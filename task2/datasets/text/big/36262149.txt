Sisterakas
{{Infobox film
| name        = Sisterakas
| image       = Sisterakas.jpg
| caption     = Theatrical movie poster
| director    = Wenn V. Deramas
| writer      = Kriz G. Gazmen Danno Kristoper C. Mariquit
| screenplay  = Kriz G. Gazmen Danno Kristoper C. Mariquit Wenn V. Deramas Joel Mercado Vic R. Kristina Bernadette Jose Marie Martina Aileen de las Alas
| cinematography = Elmer Despa
| music       = Vincent de Jesus
| editor      = Marya Ignacio
| starring    =  
| studio      =  
| distributor = Star Cinema
| released    =  
| country     = Philippines
| language    =  
| budget      = PHP 25 million
| gross       = PHP 342 million  (as of January 8, 2013 - MMFF season)     ₱393,439,711 Million  (4 weeks)  
}}

Sisterakas is a 2012 Filipino comedy parody film produced by Star Cinema and Viva Films and selected as one of the eight official entries to the 2012 Metro Manila Film Festival. The film was released nationwide in theaters on December 25, 2012. The film was directed by Wenn V. Deramas and stars an ensemble cast including Ai-Ai Delas Alas, Vice Ganda, Kris Aquino, Kathryn Bernardo, Daniel Padilla, and Xyriel Manabat.    

The film broke box-office records in the Philippines upon its nationwide release in theaters. It now holds the title of the 3rd highest-grossing Filipino film of all time domestically.

==Synopsis==
Bernadette "Detty" Sabroso is the daughter of the wealthy couple Raphael (Epi Quizon) and Kara Sabroso (Rubi-Rubi), who own a large silk factory. Raphael also has an illegitimate son, Bernard, with his former maid Maria Laurel (Beauty Gonzalez). When Maria and Bernard have nowhere else to go, Raphael takes both of them in. Unaware that they are siblings, Bernard, who is gay, and Detty become close friends, as both share their love for fashion. When Kara discovers Raphael and Maria sleeping with each other, as well as realizing that Raphael is Bernards father, she confronts them in front of Bernard and Detty, kicks them out and accidentally pushes Maria down the stairs, severely injuring her. With no assistance from the family or the other maids, Bernard swears vengeance against the Sabroso family.

Many years later, the adult Detty (Ai Ai de las Alas) and Bernard (Vice Ganda) have different lives. Following the bankruptcy of her tailor shop, Detty struggles to make ends meet for her children Kathy (Kathryn Bernardo) and Cindy (Xyriel Manabat), whom she is fighting for custody from her ex-husband. Bernard, going by the name "Bernice", owns the successful fashion brand Ponytale, which he rules with an iron fist, and lives in a large mansion with the disabled Maria (Gloria Diaz) and Angelo (Daniel Padilla), the son of Bernices deceased former maid. Detty applies for a job as executive assistant at Ponytale, unaware that Bernice is her estranged sibling. With encouragement from his mother, Bernice sees this as his long-awaited opportunity for revenge by hiring her and giving her absurd and embarrassing tasks. He also advises Angelo to pursue and date Kathy, who is his schoolmate, and then dump her to break her heart. Despite her duties becoming increasingly odd, Detty accepts and accomplishes her tasks with no complaints. Bernice begins to second-guess his intentions, but his mother advises him to continue with his plans.

At the Run Away Fashion Show, Bernice confront his rival Roselle Hermosa (Kris Aquino), who owns the equally successful La Yondelle and competes with him for a European partnership deal. When La Yondelles collection turns out to be stolen from Ponytales, Detty devises a last-minute plan to revise the fashion line. The revamped line is a success, but Bernice, angry from the stolen designs, does not thank Detty for rescuing the brand. A frustrated Detty attempts to resign, but Bernice convinces her to stay by raising her salary and increasing her work benefits.

Roselle decides to hire the Filipino-Brazilian model Marlon (Daniel Matsunaga) as her brands endorser, and requests Bernices design partner James (DJ Durano), revealed to be the mole in Ponytale, to convince Detty to join La Yondelle instead. Detty finds Marlon, who has encountered a flat tire; she repairs the tire and escorts him to the hotel where Bernice and Roselle argue over Marlon. Due to Dettys assistance, Marlon decides to sign with Ponytale. Marlon and Detty begin to show interest in each other. Marlon and Detty visit Bernices house, where Detty twists her ankle. Marlon decides to massage it, but Bernice walks in and believes that they are having oral sex, which results in him firing Marlon. Bernice and Maria confront Detty about their pasts, but Detty fails to recall the incident where Maria is paralyzed. Maria decides to do a reenactment by falling down the stairs again, which helps Detty remember who Bernice is. Detty begs for forgiveness, claiming that she was too young and didnt know what to do at the time, while Bernice counters by saying that she didnt do anything to help even in their adulthood, and fires her. Arriving home, Detty reveals to Kathy that she was fired from her job and her boss was her brother. The siblings confront each other in a shopping district, where Bernice reveals to Kathy that he used Angelo, who developed feelings for her, as part of his revenge.

Angelo approaches Kathy at their school to explain that he went through with Bernices plan because he owes him for taking him in, and that he really loves her. Roselle and James convince Detty to join La Yondelle. Bernice confronts them, where he realizes that James was the mole, and despite negotiations from him, Detty decides to remain at La Yondelle. Bernice devices an elaborate plan to scare Roselle, who falls down a flight of stairs and fractures her foot, and the latter vows revenge against Ponytale, which is facing dwindling sales. Bernices close friend Jessie (Wilma Doesnt) advices him to get back Detty and forget about his revenge scheme if he wants Ponytale to emerge successful. After a failed attempt at reconciliation with his sister, Bernice tells his mother that he wants to stop his revenge scheme, and that nothing good will come out of it.

Roselles father Simon (Tirso Cruz III) wants Detty to replace Roselle; James convinces Roselle to eliminate Detty. Detty, Kathy and Cindy are brought to an abandoned warehouse, with Bernice and Angelo in pursuit. Dettys family is ambushed by Roselle, James and several thugs, but after pleading from Kathy and Cindy, Roselle has a change of heart and begs James to call it quits. James decides to also kill Roselle to take over La Yondelle, but Bernice, who arrived with Angelo and the police, intervenes and takes a bullet for Detty. At the hospital, the siblings reconcile after blaming each other, and Bernice eventually recovers after Roselle donates blood and makes amends with Detty and Bernice. Angelo decides to pursue Kathy again. Detty rejoins Ponytale and becomes co-president, and Bernice and Detty partner with Roselle to form a new fashion line named "Sisterakas."

==Cast==

===Main===
*Ai-Ai Delas Alas as Bernadette "Detty" Sabroso-Maningas
*Vice Ganda as Bernardo "Totoy/Bernice" Laurel Sabroso
*Kris Aquino as Roxanne Celine "Roselle" Hermosa

===Supporting===
*Kathryn Bernardo as Katherine "Kathy" Maningas
*Daniel Padilla as Angelo "Gio" Samson
*Xyriel Manabat as Cindy Maningas
* Tirso Cruz III as Simon Hermosa
* Gloria Diaz as Maria Laurel
* DJ Durano as James
* Daniel Matsunaga as Marlon
* Wilma Doesnt as Jessie, Bernices Bestfriend/Personal Assistant
* Thou Reyes as Tany/Bernices Personal Assistant
* Tess Antonio as Leah
* Joey Paras as Bonbon, Dettys helper
* Joy Viado as Bing Cristobal
* Melai Cantiveros as Janet (Bernices old assistant)
* Tom Doromal as Angelos School Friend

===Special Participation===
* Maliksi Morales as Young Bernardo "Totoy" Laurel Sabroso
* Abby Bautista as Young Bernadette Sabroso
* Epi Quizon as Raphael Sabroso (father of Bernardo and Bernadette)
* Rubi Rubi as Kara Sabroso (mother of Bernadette)
* Beauty Gonzalez as Young Maria Laurel
* Christian Vasquez as the Policeman
* Luis Manzano as the Waiter

==Reception==

===Box office===
In its second day, Sisterakas doubled its lead against its closest competitor, Si Agimat, Si Enteng Kabisote, at Si Ako.
Sisterakas has turned a nationwide gross of P71.2 million at the close of screening of Metro Manila and provincial theaters.   

According to Kris Aquinos official Twitter account, she reported that Sisterakas grossed P207.3 million after its first seven days in theaters. In comparison, The Unkabogable Praybeyt Benjamin, the current highest-grossing Philippine film, grossed P200 million after eight days in theaters nationwide.    

At the closing of screening in nationwide theaters on January 2, Sisterakas had earned P265,765,433.26 in nine days. This was reported by Kris Aquino, via her official Twitter account saying she is very pleased for the positive reviews of the film. The nine-day earnings of the movie places itself at the top five all-time highest-grossing Philippine film of all time. 

According to Kris Aquino via her official Twitter account, she reported that Sisterakas grossed P283,467,640.89 in 10 days.  It officially surpassed the record of then second all-time highest-grossing Philippine of all time, No Other Woman which stars Anne Curtis. Also released by Star Cinema and Viva Films, No Other Woman has a total box-office take of P278.39 million, according to the data gathered from Box Office Mojo. Sisterakas now places itself on the second spot behind from The Unkabogable Praybeyt Benjamin in the list of the Philippines all-time box-office films. Also directed by Wenn V. Deramas and stars Vice Ganda, Praybeyt Benjamin earned P331.61 million after its local theatrical run, according from Box Office Mojo. Vice Ganda currently holds the distinction of starring in three of the five highest-grossing Filipino films of all time. 

After nearly eleven days in cinemas, Sisterakas turned in a nationwide gross of P300,263,229.84. This is confirmed by Kris Aquinos official Twitter account.  It has since then the second film to breach P300 million of gross ticket sales after Praybeyt Benjamin, which has total gross of P331.61 million after its local theatrical run. 

After almost 12 days in nationwide cinemas, Sisterakas closes the gap for all-time highest-grossing Philippine film. It has turned in a nationwide gross of P320,112,364.28 and is on set to dethrone Praybeyt Benjamin as the highest-grossing film of all-time. Starred by Vice Ganda and jointly produced by Star Cinema and Viva Films, Praybeyt Benjamin has made a grand total of P331.61 Million of gross ticket sales since its release in 2011. 

No less than Metro Manila Development Authority (MMDA) Chairman Francis Tolentino confirmed earlier today, January 8, that the Ai Ai delas Alas, Vice Ganda and Kris Aquino starrer has hit P342 million at the box office. Eric John Salut, Advertising & Promotions Head, TV Production of ABS-CBN Corporation confirmed on his Twitter account. 

===Awards===
{|| width="90%" class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2012
| rowspan="2" align="left"| Metro Manila Film Festival  Third Best Picture
| align="center"| Sisterakas
|  
|- Best Supporting Actress
| align="center"| Wilma Doesnt 
|  
|- 2013 GMMSF 2013 || Phenomenal Box-Office Kris Aquino, Vice Ganda, Ai Ai delas Alas || 
|}

==References==
 

==External links==
 

 
 
 
 
 
 