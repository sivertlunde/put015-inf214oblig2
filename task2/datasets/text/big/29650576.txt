Sheeba (film)
{{Infobox film
| name           = Sheeba
| image          = SheebaQuestarCover.jpg
| alt            =
| caption        = DVD release cover
| director       = Michael J. Jacobs
| producer       = Robin Christian
| writer         = Robin Christian
| starring       = Edward Asner Judge Reinhold Ruby Handler
| music          = Alan Williams
| cinematography =
| editing        = Jason Cox, Michael J. McLaughlin
| studio         = Questar Entertainment
| distributor    = Questar Entertainment
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Sheeba is a family film that was released on January 19, 2011.  It was directed by Michael J. Jacobs, and stars Edward Asner, Judge Reinhold and Ruby Handler.    During production, the films working title was Crab Orchard.

==Story==
Sheeba is the tale of a young boy named Clay from New York City whose parents have become estranged.  His mother (Handler) elects to move them away from his firefighter father (Reinhold) to live in the country with Clays grandfather (Asner).  Clay initially struggles to fit in and make friends in the rural community, but finds comfort and friendship with his new dog, Sheeba.  When tension between Clay and some local bullies mount, Clay finds himself in some serious trouble.  As his parents reunite, Clays father arrives to save the day, with a little help from Sheeba.

==Critical reception==
In an advance review, Home Media Magazine deemed Sheeba a "heartwarming family film", and declared "if you like dog films and family dramas with a happy ending, this is one to check out." 

Sheeba was also awarded the Dove Family Approved Seal by the Dove Foundation.

==References==
 

 
 
 
 