Luces de Buenos Aires
{{Infobox film
| name           =Luces de Buenos Aires
| image          = Lucas de Buenos Aires.jpg
| image size     =
| caption        =
| director       = Adelqui Millar
| producer       =
| writer         = 
| narrator       =
| starring       = Carlos Gardel   Sofía Bozán   Pedro Quartucci   Gloria Guzmán   Manuel Kuindós
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1931
| runtime        =
| country        =  
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} 1931  Argentine tango comedy film directed by Adelqui Millar.    

==Cast==
*Carlos Gardel as Anselmo
*Sofía Bozán as Elvira del Solar
*Gloria Guzmán as Rosita
*Pedro Quartucci as Pablo Soler
*Carlos Martínez Baena as Empresario
*Manuel Kuindós as Alberto Villamil
*Marita Ángeles as Lily
*Vicente Padula as Ciriaco
*Jorge Infante as Romualdo
*José Argüelles as Secretario 

==References==
 

==External links==
*  

 
 
 


 