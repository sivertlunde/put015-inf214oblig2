A Night in Venice (1934 film)
{{Infobox film
| name           = A Night in Venice
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = 
| writer         = Camillo Walzel (libretto)   Richard Genée (libretto)   Robert Wiene 
| narrator       = 
| starring       = Tino Pattiera   Tina Eilers   Ludwig Stössel   Oskar Sima
| music          = Johann Strauss II (operetta)   László Angyal 
| editing        = 
| cinematography = Werner Bohne 
| studio         = Hunnia-Film 
| distributor    = Kinofa  (Austria)
| released       = February 1934 (Austria)   21 March 1934 (Germany)
| runtime        = 82 minutes
| country        = Germany   Hungary
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Hungarian operetta film directed by Robert Wiene and starring Tino Pattiera, Tina Eilers and Ludwig Stössel. It is loosely based on the 1883 operetta Eine Nacht in Venedig by Johann Strauss II.
 Hunnia Studios in Budapest with three weeks of location shooting in Venice.  In common with the practice of multi-language versions at the time, the film was also made in a separate Hungarian language version Egy éj Velencében based on the same screenplay. The Hunnia Studios specialised in such co-productions during the era.  The two versions were shot simultaneously. The Hungarian version was co-directed by Wiene and Géza von Cziffra and used a separate cast of Hungarian actors. 

The film appears to have been popular with Austrian and German audiences, although its critical reception was less enthusiastic. 

==Cast==
* Tino Pattiera   
* Tina Eilers   
* Ludwig Stössel   
* Oskar Sima   
* Lici Balla    Fritz Fischer

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 