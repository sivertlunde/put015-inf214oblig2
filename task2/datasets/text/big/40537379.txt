Secrets of a Windmill Girl
{{Infobox film
  | name     = Secrets of a Windmill Girl
  | image    = "Secrets_of_a_Windmill_Girl"_(1966).jpg
  | caption  = British quad poster
  | director = Arnold L Miller Stanley A. Michael Klinger Tony Tenser
  | writer   = Arnold L Miller
  | based on = 
  | starring = Pauline Collins April Wilding Renée Houston Derek Bond Harry Fowler Peter Gordeno
| narrator = 
  | music    = Malcolm Lockyer
  | cinematography = Stanley A. Long
  | editing  = 
  | studio = Searchlight
  | distributor = Compton
  | released = 1966
  | runtime  = 
  | language = English
  | country = UK
  | budget   = 
  }}
Secrets of a Windmill Girl is a 1966 British  s by former Windmill Theatre Company performers.  It was originally released in Britain as part of a double bill with Naked as Nature Intended.  

==Cast==
* Pauline Collins as Pat Lord
* April Wilding as Linda Grey
* Renée Houston as Molly - Dresser
* Derek Bond as Inspector Thomas
* Harry Fowler as Harry
* Howard Marion-Crawford as	Richard - Producer
* Peter Gordeno as Peter
* Peter Swanwick as Len Mason Martin Jarvis as Mike, Windmill Stage Manager

==Critical reception==
TV Guide wrote, "the premise of this film is compelling, but the treatment is empty-headed" ;  and The Spinning Image asked, "and those hoping for titillation? As with so much of the sexually-themed cinema of this (British) nation, they were offered it with a moralistic angle, as if telling the audience off for their prurience."  

==References==
 

==External links==
*  
*  (from the Upstairs Downstairs webpage "Before Updown" section)

 
 

 
 