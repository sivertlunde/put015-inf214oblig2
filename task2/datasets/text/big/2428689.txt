Fashionistas
 Fashionista}} 
{{Infobox film
| name           = The Fashionistas
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = John Stagliano   
| starring       =  Evil Angel 
| distributor    = 
| released       = 
| runtime        = 276 minutes 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
The Fashionistas is a 2002  , Vol. 29/No. 2, Issue 362, February 2013, pp. 46–51.  with a length of over four and a half hours. The film was a commercial success, selling over 100,000 copies just in its first month of release.  In 2003, it set the record for most AVN Award nominations for a single title — 22. 
 AVN Special Achievement Award in 2006.  The film has had two sequels, Fashionistas Safado: The Challenge and Fashionistas Safado: Berlin, both still directed by Stagliano. 

==Plot== Fashion District of Los Angeles.  The group is trying to land a deal with Italian fetish fashion designer Antonio (Rocco Siffredi).  Antonio, who recently divorced amid highly publicized rumors of extramarital affairs, arrives in Los Angeles in search of an Sadomasochism|s/m-influenced house to partner with.  In order to grab Antonios attention, the Fashionistas crash his fashion show.  Helena wants Antonio to believe she is the creative force behind the Fashionistas, even though it is actually Jesse (Belladonna (actress)|Belladonna), her assistant.  Jesse also engages in a triangular relation with Helena and Antonio. 

==Awards and nominations==
 
{| class="infobox" style="width: 25em; text-align: left; font-size: 90%; vertical-align: middle;"
|+  Accolades received by The Fashionistas 
|-
|-
| colspan=3 |
{| class="collapsible collapsed" width=100%
! colspan=3 style="background-color: #D9E8FF; text-align: center;" | Awards & nominations
|-
|-  style="background:#d9e8ff; text-align:center;"
| style="text-align:center;" | Award
|  style="text-align:center; background:#cec; text-size:0.9em; width:50px;"| Won
|  style="text-align:center; background:#fcd; text-size:0.9em; width:50px;"| Nominated
|-
| style="text-align:center;"| AFW Awards
| 
| 
|-
| style="text-align:center;"|
;AVN Awards
| 
| 
|-
| style="text-align:center;"|
;Empire Awards
| 
| 
|-
| style="text-align:center;"|
;Ninfa Awards
| 
| 
|-
| style="text-align:center;"|
;XRCO Awards
| 
| 
|}
|- style="background:#d9e8ff;"
| style="text-align:center;" colspan="3"|
;Total number of wins and nominations
|-
| 
| 
| 
|- style="background:#d9e8ff;" References
|}
{| class="wikitable"
|-
! Year
! Ceremony
! Result
! Category
! Recipient(s)
|-
|rowspan="46"| 2003 AFW Award
|   Best Film   
|  
|-
|   Best Director – Film  John Stagliano
|-
|   Best Actress – Film  Taylor St. Claire
|-
|   Best Supporting Actress – Film  Belladonna (actress)|Belladonna
|-
|   Best Sex Scene - Film  Taylor St. Claire & Rocco Siffredi
|-
|   Best All-Girl Sex Scene - Film  Belladonna (actress)|Belladonna & Taylor St. Claire
|-
|   Best Anal Sex Scene - Film  Kate Frost & Rocco Siffredi
|-
|   Best Group Sex Scene 
|  
|-
|rowspan="22"| AVN Award
|   Best Film   
|  
|-
|   Best Director – Film  John Stagliano
|-
|   Best Actor – Film    Rocco Siffredi
|-
|   Best Actress – Film  Taylor St. Claire
|-
|   Best Supporting Actor – Film  Manuel Ferrara
|-
|   Best Supporting Actress – Film  Belladonna (actress)|Belladonna
|-
|   Best Supporting Actress – Film  Caroline Pierce
|-
|   Best Tease Performance  Belladonna (actress)|Belladonna
|-
|   Best Tease Performance  Chelsea Blue, Gia, & Taylor St. Claire
|-
|   Best Box Cover Concept 
|  
|-
|   Best All-Girl Sex Scene - Film  Belladonna (actress)|Belladonna & Taylor St. Claire
|-
|   Best Anal Sex Scene - Film  Kate Frost & Rocco Siffredi
|-
|   Best Oral Sex Scene - Film  Belladonna (actress)|Belladonna & Rocco Siffredi
|-
|   Best Oral Sex Scene – Film  Belladonna (actress)|Belladonna, Mark Ashley & Billy Glide
|-
|   Best Sex Scene Coupling – Film  Caroline Pierce & Manuel Ferrara
|-
|   Best Sex Scene Coupling – Film  Taylor St. Claire & Rocco Siffredi
|-
|   Best Group Sex Scene - Film 
|Friday, Taylor St. Claire, Sharon Wild & Rocco Siffredi
|-
|   Best Screenplay – Film  John Stagliano
|-
|   Best Art Direction - Film  Jim Malibu
|-
|   Best Cinematography  John Stagliano
|-
|   Best Editing - Film  Tricia Devereaux & John Stagliano
|-
|   Best Music  John Further, Javier, Mistress Minx & DJ Uneasy
|-
|rowspan="8"| Ninfa Award
|   Best Anal Sequence   
|  
|-
|   Best Sex Sequence 
|  
|-
|   Best Script 
|  
|-
|   Best Cinematographic Values 
|  
|-
|   Best Actor  Rocco Siffredi
|-
|   Best Actress  Belladonna (actress)|Belladonna
|-
|   Best Director  John Stagliano
|-
|   Best Film  John Stagliano
|-
|rowspan="8"| XRCO Award
|   Best Group Sex Scene   
|Friday, Taylor St. Claire, Sharon Wild & Rocco Siffredi
|-
|   Best Actor  Rocco Siffredi
|-
|   Best Actress  Belladonna (actress)|Belladonna
|-
|   Best Girl/Girl Sex Scene  Belladonna (actress)|Belladonna & Taylor St. Claire
|-
|   Best Male/Female Sex Scene  Taylor St. Claire & Rocco Siffredi
|-
|   Film of the Year 
|  
|-
|   Single Performance, Actress    Taylor St. Claire
|-
|   Sex Scene of the Year  Belladonna (actress)|Belladonna & Rocco Siffredi
|-
|rowspan="10"| 2004
| AFW Award
|   Best DVD 
|  
|-
|rowspan="5"| AVN Award
|   Best DVD   
|  
|-
|   Best Renting Title of the Year 
|  
|-
|   Best DVD Extras   
|  
|-
|   Best DVD Menus 
|  
|-
|   Best DVD Packaging 
|  
|-
|rowspan="3"| Empire Award
|   Best Overall DVD   
|  
|-
|   Best Feature DVD 
|  
|-
|   Best DVD Audio Quality 
|  
|-
|rowspan="2"| XRCO Award
|   Best DVD 
|  
|-
| 2013
|   XRCO Hall of Fame 
|  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 