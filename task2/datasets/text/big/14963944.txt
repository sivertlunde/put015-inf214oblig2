Raggedy Rose
{{Infobox film
| name           = Raggedy Rose
| image          = Poster of the movie Raggedy Rose.jpg
| image_size     =
| caption        = Theatrical release poster Richard Wallace F. Richard Jones (supervising director) Stan Laurel (asst. director)
| producer       = Hal Roach
| writer         = Carl Harbaugh Stan Laurel Leroy Scott Jerome Storm  Beatrice Van Hal Yates H. M. Walker (titles)
| starring       = Mabel Normand
| cinematography = Harry W. Gerstad Floyd Jackman
| editing        = Richard C. Currier
| studio         = Hal Roach Studios
| distributor    = Pathé Exchange
| released       =  
| runtime        = 56 minutes
| country        = United States English intertitles
}}
 silent comedy Richard Wallace.

==Cast==
* Mabel Normand as Raggedy Rose Carl Miller as Ted Tudor
* Max Davidson as Moe Ginsberg James Finlayson as Simpson Sniffle
* Anita Garvin as Janice
* Laura La Varnie as her mother
* Jerry Mandy as the chauffeur

==Plot==
Rose (Normand), who works for a junk dealer (Davidson), dreams of romance with bachelor Ted Tudor (Miller).

==Production notes==
*Oliver Hardy had been injured in a cooking accident at home where he burned his arm after a frying pan of scalding grease spilled onto it, and was still recovering when filming for Raggedy Rose began. This accident also forced Hardy to be replaced by Stan Laurel in the Hal Roach comedy Get Em Young. 

==See also==
* Stan Laurel filmography

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 