Kaliyil Alpam Karyam
{{Infobox film
| name           = Kaliyil Alpam Karyam
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Sathyan Anthikad
| producer       = Pavamani
| writer         = Sathyan Anthikad
| narrator       =  Rahman  Lizy
| music          = Raveendran Venu
| editing        =    
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Lizy in the lead roles.   


==Plot==
Vinayan comes from a rich family but he cannot stand up for modern lifestyle. His brother spends time dancing at clubs, his sister always listens to the radio, and his parents are always busy. He gets tired of life in the city and moves to a village taking a small job as a village officer. There he falls in love with a village girl but she is just the opposite of Vinayan and she wants to enjoy the luxurious life in a city. Soon they part. Vinayan continues in the village while his wife moves to the city. After a while she understands that her village life was far better and returns to her own village and the couple unites.

==Cast==
*Mohanlal as Vinu/Vinayan
*Neelima as Radha
*Jagathi Sreekumar as Vaasunni Rahman as Babu
*Sukumari Lizy as Kalpana
*Nedumudi Venu
*Mala Aravindan as Shankarankutty
*Sankaradi


==Soundtrack==
The music was composed by Raveendran and lyrics was written by Sathyan Anthikkad.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Disco disco || KS Chithra, Raveendran || Sathyan Anthikkad ||
|-
| 2 || Kannodu Kannaaya Swapnangal || K. J. Yesudas, KS Chithra || Sathyan Anthikkad ||
|-
| 3 || Manathaaril Ennum || K. J. Yesudas || Sathyan Anthikkad ||
|-
| 4 || Pattanathilennum || KS Chithra, Chorus || Sathyan Anthikkad ||
|}

==References==
 

==External links==
*  

 
 
 
 


 