Running Scared (2006 film)
 Running Scared}}
{{Infobox film
| name = Running Scared
| image = Running scared.jpg
| caption = Theatrical release poster Wayne Kramer
| producer = Andrew Pfeffer
| writer = Wayne Kramer
| starring = Paul Walker Cameron Bright Vera Farmiga Chazz Palminteri
| music = Mark Isham
| cinematography = Jim Whitaker
| editing = Arthur Coburn
| studio = New Line Cinema Media 8 Entertainment
| distributor = New Line Cinema Media 8 Entertainment
| released =  
| runtime = 122 minutes 
| country = United States Germany
| language = English Russian
| budget = $15 million 
| gross = $9.4 million 
}} crime Thriller thriller film, Wayne Kramer, and starring Paul Walker, Cameron Bright and Vera Farmiga. It was released in the United States on February 24, 2006.

==Plot== Johnny Messner) is present during a drug deal gone wrong. A trio of corrupt cops led by Rydell (Chazz Palminteri), storm in and try to steal the drugs, the money and kill everyone else but a shootout ensues, resulting in the deaths of two of the officers. Tommy looks to Joey to take the guns and get rid of them but instead, Joey goes home to his wife Teresa (Vera Farmiga), and his son, Nicky (Alex Neuberger). Nicky and his friend from next door, Oleg (Cameron Bright) secretly watch as Joey stashes the guns in the basement. 

Oleg decides to take one of the guns after Joey leaves and goes home that night to his mother Mila (Ivana Miličević) and abusive stepfather, Anzor Yugorsky (Karel Roden), the outcast nephew of Russian mob boss, Ivan Yugorsky (John Noble). Oleg uses the gun on Anzor after he becomes belligerent with him and his mother and shoots him. After hearing the gunshot, Joey goes next door to find Anzor wounded and Oleg gone. Anzor describes the gun to Joey, making him realize Oleg has one of the murder weapons and he knows he has to find Oleg and the gun before the police do.

Throughout the night, Oleg runs into many unsavory people, including a homeless man, a drug dealer, an abusive pimp named Lester (David Warshofsky) and his prostitute, Divina (Idalis DeLeon). After saving Divina from Lester, she decides to help him which is how he winds up at the same diner that Joey and Nicky are at. They are there for Joey to talk to Frankie Perello (Arthur J. Nascarella), the Italian mob boss, about Oleg and to tell him that the gun Oleg used wasn’t from the shootout. Nicky and Oleg stash the gun in the diner bathroom and when Oleg tries to flee, he is caught by the police and sent back into his stepfather’s custody. 
 child molesting serial killers. Oleg fakes an upset stomach and on the way to the bathroom finds Edeles purse by the door and takes her cell phone and calls Teresa. She prompts him to find anything with the apartments address, and promises to come get him as Dez and Edele try and force their way into the bathroom.

When Teresa arrives, she threatens her way inside and searches the whole apartment, but sees no sign of Oleg and goes to leave when she realizes something is off. She threatens Dez who eventually indicates a closet in the playroom where she finds Oleg tied up with a plastic bag on his head. After Teresa saves Oleg by doing CPR while keeping the gun at the couple, she tells him to get the other kids and leave. Dez attempts to bribe Teresa with $100,000 in diamonds if she takes only Oleg and leaves. Teresa ignores him and finds children costumes, plastic body bags, children snuff films and surgical instruments in the playroom closet. Teresa uses the couples phone to call the police. She reports gunshots, hangs up, then murders them both. 

Throughout all this, Joey has been tracking down the missing gun, and is about to finally get it back but is foiled by the presence of Tommy who takes him and Oleg to a hockey rink to meet Frankie and Ivan. Ivan kills Anzor when he refuses to kill Oleg, and Joey, about to be killed himself, says in desperation that the gun is Tommys. Tommy goes in an attempt to kill him but is killed instead by a Russian mobster where another shootout ensues. Frankie shoots Ivan during the process and goes to kill Joey when Joey reveals his FBI cover. Oleg helps distract Frankie so Joey can disarm him where he then kills Frankie. Joey and Oleg leave the hockey rink in time to see the FBI storm the building.

Joey and Oleg run into Lester, the new owner of the gun and a struggle between Joey and Lester develops that ends with Lester shooting Joey with the gun but not before Joey stabs Lester in the neck with his own knife. Joey and Oleg run and drive back to Joey’s house. Back at Olegs house, Mila thinks her son is dead and kills herself by blowing up the meth lab in their backyard. The explosion makes Teresa and Nicky come outside to investigate when they see Joey crash the car after losing consciousness. Days later, a funeral is held for Joey with Teresa, Nicky, and Oleg in attendance. They drive out to a small farm house, where Joeys car is parked in the driveway. Teresa sits on the cars bumper when Joey emerges from under the car, alive and well, having faked his own death for his protection as an undercover officer. Oleg has been adopted into the family.

==Cast==
* Paul Walker as Joey Gazelle
* Cameron Bright as Oleg Yugorsky
* Vera Farmiga as Teresa Gazelle
* Chazz Palminteri as Detective Rydell
* Karel Roden as Anzor Yugorsky Johnny Messner as Tommy "Tombs" Perello
* Ivana Miličević as Mila Yugorsky
* Alex Neuberger as Nicky Gazelle
* Michael Cudlitz as Sal "Gummy Bear" Franzone
* Bruce Altman as Dez Hansel
* Elizabeth Mitchell as Edele Hansel
* Arthur J. Nascarella as Frankie Perello
* John Noble as Ivan Yugorsky
* Idalis DeLeon as Divina 
* David Warshofsky as Lester the Pimp
* Jim Tooey as Tony
* Thomas Rosales, Jr. as Julio

==Reception==

===Box office===
Running Scared opened with $3,381,974 on 1,611 screens (for a $2,099 per theater average). It went on to make a total of $9.4 million worldwide, failing to bring back its modest budget of only $15 million.   

===Critical response===
The film received mixed reviews from film critics. It currently holds a 40% "rotten" rating on film review aggregator website  . 

Justin Chang of Variety (magazine)|Variety described Whitakers cinematography, which primarily used Steadicam and crane shots, as "  with a desaturated palette that nevertheless has a rich, grimy luster." He also noted the film had an odd plot, which was disarming given it was shot in Prague rather than somewhere that looks closer to New Jersey.  Sam Wigley of Sight and Sound said the vicious gangland depicted in the film resembles an "iniquitous fairytale realm," although it is dark, and "passes in a vertiginous blur of comic-book hyper-reality." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 