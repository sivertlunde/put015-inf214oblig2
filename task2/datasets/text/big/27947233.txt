Kousek nebe
 
{{Infobox film
| name           = Kousek nebe
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Petr Nikolaev
| producer       = Jaroslav Kucera
| writer         = Jirí Stránský
| narrator       = 
| starring       = Jakub Doubrava
| music          = 
| cinematography = Martin Duba
| editing        = 
| studio         = 
| distributor    = 
| released       = 10 February 2005
| runtime        = 88 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech romance film directed by Petr Nikolaev. It was released in 2005.

==Plot==
The film is set in a Czechoslovak Prison of 1950s. The main theme is friendship and love of unjustly senńtenced people trying to survive in hard conditions of Prison. It is a story of Luboš and Dana as thhey fall n love with each other.

==Cast==
* Jakub Doubrava - Lubos
* Tatiana Pauhofová - Dana (as Tána Pauhofová)
* Petr Forman - Bruno
* Ondrej Vetchý - Rusnák
* Pavel Zednícek - Roubal
* Vladimír Javorský - Sebek
* Karel Zima - Dráb
* Zuzana Stivínová - Dorota
* Lenka Vychodilová - Kredenc
* Josef Somr - Prisoner
* Petr Vacek - Prisoner
* Pavel Landovský - Starej
* Jan Vondrácek - Warder

==Reception==
The film has received positive reception. It gained 5 nominations for Czech Lion.      

==References==
 

==External links==
*  

 
 
 
 


 
 