Sira` Fi al-Wadi
{{Infobox film
| name           = Sira` Fi al-Wadi  صراع في الوادي 
| image          = Struggle-in-the-Valley-poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Youssef Chahine
| producer       = Gabriel Telhamy Mustafa Hasan
| writer         = Ali El Zorkani Helmy Halim
| starring       = Faten Hamama Omar Sharif Zaki Rostom Abdel Waress Assar Farid Shawki Hamdy Gheith
| music          = Fouad El Zahiri
| cinematography = Ahmed Khorshed
| distributor    = Nile Cinema
| released       = 1954
| runtime        = 125 minutes
| country        = Egypt
| language       = Arabic
| budget         = 
}}
 1954 Egyptian Egyptian Cinema centennial, this film was selected one of the best 150 Egyptian film productions. It was presented in the 1954 Cannes Film Festival under the name The Blazing Sky.      

== Plot ==
Omar Sharif plays Ahmed, an engineer whose father (played by Abdel Waress Assar) is a farmer and a farm owner. His father succeeds in improving and increasing the production of sugar cane in his farm. Taher Pasha, a wealthy land owner, who runs a competing sugar cane production facility, feels threatened by his recent production prosperity. Ahmed is in a love relationship with the pashas daughter, Amal, but as a consequence to the rivalry between both their fathers, he is compelled to hide their relationship.            
 sentenced to death. Ahmed learns of a witness of this crime, but after looking for him, he discovers that he was killed in a mysterious accident. Ahmeds father is executed and his partners son, Selim, seeks revenge for his fathers death. He searches for Ahmed for retribution. Ahmed, fearing his death, hides in an ancient temple in the desert.    
 marries her.    

== Main cast ==
*Faten Hamama as Amal
*Omar Sharif as Ahmed
*Zaki Rostom as Taher Pasha
*Abdel Waress Assar as Ahmeds father
*Farid Shawqi as himself
*Hamdy Gheith as Selim

== References ==
 

== External links ==
 
* 
 
 
 
 
 
 
 