Kaena: The Prophecy
{{Infobox film
| name           = Kaena: The Prophecy
| image          = kaena_the_prophecy_us_film_poster.jpg
| caption        = Promotional poster for US release.
| director       = Chris Delaporte Pascal Pinon
| producer       = Marc du Pontavice
| writer         = Patrick Daher Chris Delaporte Tarik Hamdine
| starring       = Kirsten Dunst Richard Harris Anjelica Huston Keith David Ciara Janson
| music          = Farid Russlan
| editing        = Bénédicte Brunet
| studio         = Xilam TVA 
| released       = June 2003 (France)  July 2004 (USA)
| runtime        = 85 min 
| country        = France/Canada
| language       = French, English
}}
  Canadian computer-generated imagery|computer-generated fantasy Film|movie. The United States release of the film is distributed by Samuel Goldwyn Films and features the voices of Kirsten Dunst, Richard Harris (in his last role before his death), Anjelica Huston, Keith David and Ciara Janson.
 Heart of Darkness. 

==Plot==
The film begins with an alien ship crash landing on a desert planet. The alien survivors, known as Vecarians, are quickly killed by the planets predatory native inhabitants, the Selenites. The ships core, Vecanoi, survives, and from it sprouts Axis, a massive tree reaching up into space.

600 years later, a race of human-like tree-dwellers have evolved living in the branches of Axis. One of them, a young girl named Kaena (voiced by Kirsten Dunst), is an adventurous daydreamer who longs to explore the world beyond the confines of her village. Kaenas inquisitiveness is opposed as heresy by the village elder, who commands his people to stay productive and toil for the villagers gods (who are, unbeknownst to them, the Selenites living in the planet below).

Led by prophetic dreams of a world with a blue sun and plentiful water, Kaena eventually defies the elder and climbs to the top of Axis. There, she encounters the ancient alien Opaz (voiced by Richard Harris), the last survivor of the Vecarian race that crash landed on the planet centuries ago. Opaz has used his technology to evolve a race of intelligent worms to serve him and help him escape the planet. Upon learning of Kaenas dreams, Opaz enlists her help in retrieving Vecanoi, which contains the collective memory of his people.

However, Vecanoi rests at the base of Axis, where the Selenites dwell. The Queen of the Selenites (voiced by Anjelica Huston) blames Vecanoi for the destruction of their planet, and has spent most of her life (and sacrificed the future of her people) attempting to destroy it.

==Cast==

===French Cast===
* Cécile de France as Kaena
* Michael Lonsdale as Opaz
* Victoria Abril as La reine
* Jean Piat as Le prêtre

===English Cast===
* Kirsten Dunst as Kaena
* Richard Harris as Opaz
* Anjelica Huston as Queen of the Selenites
* Keith David as Voxem Michael McShane as Assad
* Greg Proops as Gommy
* Tom Kenny as Zehos
* Tara Strong as Essy
* Dwight Schultz as Ilpo
* John DiMaggio as Enode
* Ciara Janson as Kamou, Roya
* Jennifer Darling as Reya
* Cornell John as Demok
* Gary Martin as The Priest
* William Attenborough as Sambo

==External links==
*  —Sony Pictures
*  
*  
*  —FilmJerk.com

 
 
 
 
 
 
 
 
 