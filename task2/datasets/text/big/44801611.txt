The Spanish Jade (1915 film)
{{Infobox film
| name           = The Spanish Jade
| image          =
| alt            = 
| caption        = 
| director       = Wilfred Lucas
| producer       = 
| screenplay     = Maurice Hewlett Louis Joseph Vance
| starring       = Betty Bellairs Wilfred Lucas Nigel De Brulier Arthur Tavares Frank Lanning Howard Davies
| music          = 
| cinematography = Enrique Juan Vallejo 
| editing        = 
| studio         = Fiction Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Wilfred Lucas and written by Maurice Hewlett and Louis Joseph Vance. The film stars Betty Bellairs, Wilfred Lucas, Nigel De Brulier, Arthur Tavares, Frank Lanning and Howard Davies. The film was released on April 11, 1915, by Paramount Pictures.  

==Plot==
 

== Cast == 
*Betty Bellairs as Manuela
*Wilfred Lucas as Osmund Manvers
*Nigel De Brulier as Don Luis 
*Arthur Tavares as Don Bartolome
*Frank Lanning as Tormillo
*Howard Davies as Gil Perez
*Lloyd Ingraham as Sebastian 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 