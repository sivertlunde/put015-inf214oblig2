Phone Call from a Stranger
{{Infobox film
| name           = Phone Call from a Stranger
| image          = PhoneCallStranger.jpg
| caption        = Original poster
| director       = Jean Negulesco
| producer       = Nunnally Johnson
| writer         = Nunnally Johnson I. A. R. Wylie
| starring       = Gary Merrill Shelley Winters Michael Rennie Keenan Wynn
| music          = Franz Waxman
| cinematography = Milton R. Krasner
| editing        = Hugh S. Fowler
| studio         = Twentieth Century-Fox
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,350,000 (US rentals) 
}}
 American drama film directed by Jean Negulesco, who was nominated for the Golden Lion at the Venice Film Festival. The screenplay by Nunnally Johnson and Ida Alexa Ross Wylie|I.A.R. Wylie, which received the award for Best Scenario at the same festival, centers on the survivor of a plane crash who contacts the relatives of three of the victims he came to know on board the flight.

==Plot== Craig Stevens) vaudevillian Sally flashbacks during the unexpected four-hour layover. They exchange home phone numbers with the idea they may one day have a reunion.  
 South Pacific Broadway and paralyzed from iron lung and feeling hopeless about her future when Eddie arrived to take her home. Marie tells Trask that despite his often obnoxious behavior, Eddie was the most decent man she had ever known, and had taught her the true meaning of love.

Maries story teaches Trask a lesson about marital infidelity and forgiveness, and he calls Jane to tell her hes returning home.

==Cast==
* Gary Merrill as David Trask
* Shelley Winters as Binky Gay
* Michael Rennie as Dr. Robert Fortness
* Keenan Wynn as Eddie Hoke
* Evelyn Varden as Sally Carr
* Warren Stevens as Marty Nelson
* Beatrice Straight as Claire Fortness
* Ted Donaldson as Jerry Fortness Craig Stevens as Mike Carr
* Bette Davis as Marie Hoke
* Helen Westcott as Jane Trask

==Production==
When Gary Merrills wife Bette Davis read the script, she suggested he ask director Negulesco if she could play the relatively small role of Marie Hoke, feeling "it would be a change of pace for me. I believed in the part more than its length. I have never understood why stars should object to playing smaller parts if they were good ones. Marie Hoke was such a part." 

The film was the third on-screen pairing of Merrill and Davis, following All About Eve (1950) and Another Mans Poison (1951).    

Producer-screenwriter Johnson originally wanted to cast Lauren Bacall as Binky Gay, but she was unavailable. 

Broadway actress Beatrice Straight made her screen debut in this film.
  Jesse White as Eddie Hoke in Crack Up, an hour-long television adaptation broadcast on the CBS anthology series The 20th Century Fox Hour in February 1956. 

==Critical reception==
In his New York Times review, Bosley Crowther said, "So slick, indeed, is the whole thing — so smooth and efficiently contrived to fit and run with the precision of a beautifully made machine — that it very soon gives the impression of being wholly mechanical, picked up from a story-tellers blueprints rather than from the scroll of life . . . that is the nature of the picture — mechanically intriguing but unreal."  

Time Out London calls it "a decent, but hardly outstanding dramatic compendium."  

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 