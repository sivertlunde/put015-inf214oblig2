A Sunday in the Country
{{Infobox film
|  name     = A Sunday in the Country image          = Sunday in the country poster.jpg writer         = Bertrand Tavernier Colo Tavernier
| starring = {{plainlist|
* Louis Ducreux
* Michel Aumont
* Sabine Azéma
* Geneviève Mnich
* Monique Chaumette
}} director       = Bertrand Tavernier producer       = cinematographer = Bruno de Keyzer
  distributor       = released   = October 2, 1984 runtime        = 90 minutes country = France |
  language = French |
  budget         = |
}} 1984 cinema French film directed by Bertrand Tavernier.

==Plot==

The story takes place during a Sunday in the late summer of 1912. Monsieur Ladmiral is a painter without any real genius and in the twilight of his life. Since the death of his wife, he lives alone with Mercedes, his servant. As every Sunday, he invites Gonzague, his son, a steady young man, who likes order and propriety, accompanied by his wife, Marie-Thérèse and their three children, Emile, Lucien and Mireille. That day, Irène, Gonzagues sister, a young  non-conformist, liberated and energetic woman, upsets this peaceful ritual and calls into question her fathers artistic choices.

==Cast==
* Louis Ducreux - Monsieur Ladmiral
* Michel Aumont - Gonzague
* Sabine Azéma - Irène
* Geneviève Mnich - Marie-Thérèse
* Monique Chaumette - Mercédès
* Thomas Duval - Emile
* Quentin Ogier - Lucien
* Katia Wostrikoff - Mireille
* Claude Winter - Madame Ladmiral
* Jean-Roger Milo - Fisherman (Le pêcheur)
* Pascale Vignal - A servant (La serveuse)
* Jacques Poitrenaud - Hector (Patron guinguette)
* Valentine Suard - Little girl (La petite fille 1)
* Erika Faivre - Little girl (La petite fille 2)
* Marc Perrone - Accordionist (Laccordéoniste)

==Awards==

===Won===
*Boston Society of Film Critics (USA)
**Best Director (Bertrand Tavernier)
**Best Foreign Language Film Cannes Film Festival (France) Best Director (Bertrand Tavernier)   
*César Awards (France)
**Best Actress in a Leading Role (Sabine Azéma)
**Best Cinematography (Bruno de Keyzer)
**Best Writing - Adaptation (Bertrand and Colo Tavernier)
*Kansas City Film Critics Circle (USA)
**Best Foreign Film
*London Film Critics Circle (UK)
**Best Foreign Language Film
*National Board of Review (USA)
**Best Actress in a Supporting Role (Sabine Azéma)
**Best Foreign Language Film
*New York Film Critics Circle (USA)
**Best Foreign Language Film

===Nominated===
*BAFTA Awards (UK)
**Best Foreign Language Film (Alain Sarde and Bertrand Tavernier) Cannes Film Festival (France)
**Golden Palm 
*César Awards (France)
**Best Actor in a Leading Role (Louis Ducreux)
**Best Actor in a Supporting Role (Michel Aumont)
**Best Director (Bertrand Tavernier)
**Best Editing (Armand Psenny)
**Best Film (Bertrand Tavernier)
*Golden Globes Awards (USA)
**Best Foreign Language Film

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 
 