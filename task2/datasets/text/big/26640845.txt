From Hell to Texas
{{Infobox film
| name           = From Hell to Texas
| image          = 
| image_size     = 
| caption        = 
| director       = Henry Hathaway
| writer         = Robert Buckner Wendell Mayes
| narrator       =  Don Murray Diane Varsi Chill Wills Dennis Hopper
| music          = Daniele Amfitheatrof
| cinematography = Wilfred M. Cline
| editing        = Johnny Ehrin
| distributor    = 20th Century Fox
| released       = June 1, 1958
| runtime        = 100 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Don Murray and Diane Varsi.    The supporting cast features Chill Wills and Dennis Hopper. It is based on the novel, The Hell-Bent Kid, by Charles O. Locke.

==Plot==
Tod Lohman is on the run from a posse. The ruthless land baron Hunter Boyd has sent men, including sons Otis and Tom, in pursuit of Tod for having killed another son.

Otis Boyd stampedes horses toward Tod, but gunshots drive them in the opposite direction and Otis is trampled instead. Tod then gets the drop on Tom Boyd and insists he did not kill their brother, but before Tod can leave, Tom Boyd shoots his horse.

On foot, Tod collapses near a river bank. He is found by kindly rancher Amos Bradley and daughter Juanita, who provide food and shelter. Juanita takes a liking to Tod, who is searching for his missing father and was brought up with Biblical lessons and principles by his mother.

Tod departs but is soon surrounded by Hunter Boyd and his men. In gratitude for not shooting Tom Boyd when he had the chance, Hunter Boyd permits a horse and a four-hour headstart to Tod, then resumes the chase.

Tod discovers that his father has died. A member of the posse shoots old man Bradley, which proves the last straw for Tod after having tried to turn the other cheek. But during a gun battle in town, Tom Boyd is engulfed by flames after a chandeliers crash. Tods instincts take over. He saves the life of Tom, whose appreciative father Hunter finally calls off the feud.

==Cast== Don Murray as Tod Lohman
*Diane Varsi as Juanita Bradley
*Chill Wills as Amos Bradley
*Dennis Hopper as Tom Boyd
*R.G. Armstrong as Hunter Boyd
*Jay C. Flippen	as Jake Leffertfinger

==Production==
Hathaway has described the editing of From Hell to Texas in an oral history conducted by Rudy Behlmer. It describes the role of Barbara McLean, then the head of the editing department at 20th Century Fox. 

==References==
 

==External links==
* 

 

 
 
 
 
 

 