Jedda
 
 
{{Infobox film
| name           = Jedda
| image          =
| image_size     =
| caption        = Charles Chauvel
| producer       = Charles Chauvel
| writer         = Charles Chauvel Elsa Chauvel
| narrator       =
| starring       = Robert Tudawali Ngarla Kunoth
| music          = Isador Goodman
| cinematography = Carl Kayser
| editing        = Alex Ezard Jack Gardiner Pam Bosworth
| studio = Charles Chauvel Productions Ltd British Lion (UK) Distributors Corporation of America (US)
| released       = 3 January 1955 (premiere) 5 May 1955 (Aust) 1956 (UK) 1957 (USA)
| runtime        = 101 mins (Aust) 61 mins (UK)
| country        = Australia
| language       = English
| budget         = £90,823 
| preceded_by    =
| followed_by    =
}}
 Charles Chauvel. Aboriginal actors (Robert Tudawali and Ngarla Kunoth) in the leading roles, and also to be the first Australian feature film shot in colour.  Jedda is seen by some as an influential film in early Australian cinema, as it set a standard for future Australian films.  It won more international attention than previous Australian films, during a time when Hollywood films were dominating the Australian cinema. The director, Charles Chauvel, was nominated for the Golden Palm Award in the 1955 Cannes Film Festival,    but lost to the American Delbert Mann for Marty (film)|Marty.

==Plot==
Jedda is an Aboriginal girl born on a cattle station in the Northern Territory of Australia. After her mother dies giving birth to her, the child is brought to Sarah McMann, the wife of the station boss. Sarah has recently lost her own newborn to illness. She at first intends to give the baby to one of the Aboriginal women who work on the station, but then raises Jedda as her own, teaching her European ways and separating her from other Aborigines.

Jedda wants to learn about her own culture, but is forbidden by Sarah. When Jedda grows into a young woman, she becomes curious about an Aboriginal man from the bush named Marbuck. This tall stranger arouses strong feelings in her. She is lured to his camp one night by a song. Marbuck abducts her and sets off back to his tribal land, through crocodile-infested swamps.
 stockman in dreaming time of tomorrow."

==Cast== Ngarla Kunoth as Jedda
*Robert Tudawali as Marbuck
*Betty Suttor as Sarah McMann 
*Paul Reynall as Joe
*George Simpson-Lyttle as Douglas McMann
*Tas Fitzer as Peter Wallis
*Wason Byers as Felix Romeo
* Willie Farrar as Little Joe
*Margaret Dingle as Little Jedda

==Development==
Charles Chauvel says the original inspiration for the film came from a meeting he had in Hollywood in early 1950 with Merian C. Cooper. Cooper encouraged the director to make a film exploiting Australian locations. Chauvel was further encouraged along these lines by Bess Meredyth who had made a number of films in Australia in the 1920s. 

Chauvel decided to make the project in the Northern Territory. With his wife Elsa he made an extensive survey of the Territory later that year with the assistance of the Commonwealth government. He undertook colour tests, intending to make Australias first colour    The Chauvels then wrote a screenplay, originally entitled The Northern Territory Story.

In 1951 Chauvel formed Chauvel Productions Ltd to make the film, with a notional capital of £500,000.   It went public in August 1951, offering 240,000 shares. 

Chauvel later claimed he turned down an American offer of $100,000 (£44,000) to finance the film because it was conditional upon Linda Darnell being cast in the lead.  Chauvels regular backers Universal Pictures did not want to invest in the movie but Chauvel managed to secure finance from various businessmen, including Mainguard Australia Ltd.  It took the Chauvels 18 months to find a suitable filming location.

==Production== Melville Island.  Ngarla Kunoth was an Arunta woman from an inland mission station; she was selected over seven other actors screen tested and was cast by July.  

The shoot took five months to complete, plus post-production work done in Sydney. Most of the scenes were shot on the Coolibah Station in the Northern Territory, as well as at Stanley Chasm, Ormiston Gorge and Mary River in the north.

The Chauvels celebrated their 25th wedding anniversary during filming.  Wason Byers, who had a small role, was arrested for stealing over £1,000 worth of cattle. 

The production process was as laborious as the colour technique used, Gevacolor, could only be processed overseas in England. The film was fragile and heat-sensitive, which was a problem as the Northern Territory had a typically hot climate; during production, the film was stored in cool caves to protect it from deteriorating. By mid 1952 the film had incurred expenses of £24,673. 
 Blue Mountains west of Sydney.  Editing and sound recorded were completed in London.

The music was written by Isador Goodman.  Elsa Chauvel, the directors wife, replaced large parts of Goodmans score with old-fashioned commercial ‘mood’ music.

==Reception==
The film has its world premiere on 3 January 1955 but did not open in Sydney until May. Its commercial reception was solid rather than sensational: Charles Chauvel Productions Ltd received £17,915 from the film in May and June 1955. The companys name was changed to Jedda Ltd to help exploit the film.  In December 1956 Jedda Ltd reported a profit of £50,454 for the year to 30 June, reducing the debit balance in the production account to £69,697.  The film had been successful in Australia but performed disappointingly overseas. 

The film was released in the UK as Jedda the Uncivilised. 

Some time after the film was completed and released in locations around the world, the film in Gevacolor was found to have faded from aging.  In 1972 the film was reproduced from original tri-separations found in London.

This was Charles Chauvels last feature film. He intended to adapt Kay Glasson Taylors novel The Wars of the Outer March  but was hired by the BBC to make the TV series Walkabout. Before he could resume work on his feature projects he died on 11 November 1959.

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
* 
*  , Murdoch University Media Communication and Culture
*  at National Film and Sound Archive
*  
*   at Australian Screen Online
*  at Oz Movies

 
 

 
 
 
 
 
 
 
 
 
 