Porco Rosso
 
{{Infobox film
| name           = Porco Rosso
| image          = Porco Rosso (Movie Poster).jpg
| caption        = Japanese release poster
| alt            = A male pig in a suit is flying with a woman next to him on his plane. To their right is the films title and below them is a plane flying in the sky - and the films credits.
| director       = Hayao Miyazaki Toshio Suzuki
| writer      = Hayao Miyazaki
| based on =  
| starring       = Shūichirō Moriyama Tokiko Kato Akemi Okamura Akio Ōtsuka
| music          = Joe Hisaishi
| cinematography = Atsushi Okui
| editing        = Takeshi Seyama
| studio         = Studio Ghibli
| distributor    = Toho
| released       =  
| runtime        = 94 minutes
| country        = Japan
| language       = Japanese
| budget         = $9.2 million
| gross          = $34 million
}} Toshio Suzuki produced the film for Studio Ghibli. Joe Hisaishi composed the music.

The plot revolves around an Italian World War I ex-fighter ace, now living as a freelance bounty hunter chasing "air pirates" in the Adriatic Sea. However, an unusual curse has transformed him to an anthropomorphic pig. Once called Marco Pagot (Marco Rousolini in the American version), he is now known to the world as "Porco Rosso", Italian for "Red Pig".

==Plot== WWI fighter ace and freelance bounty hunter, responds to an alert over an attack on a ferry liner by airborne Piracy|pirates. Having successfully defeated the airborne pirates, Porco retires to the Hotel Adriano, which is run by his long-time friend Gina.

At the restaurant of the hotel, the heads of the pirate gangs are introduced to Curtis, an arrogant and ambitious American ace who has a contract to assist them. Within time Curtis falls in love with Gina but is frustrated to see that she has affection for Porco. After successfully executing a pirating mission, Curtis tracks down Porco, who is flying to Milan to have his plane improved, and shoots him down, claiming to have killed him. Porco actually survives, but all but the fuselage of his plane has been destroyed. Porco continues his mission to Milan, much to the irritation of Gina.

Porco arrives discreetly in Milan to meet Piccolo, his mechanic. He learns that Piccolos sons have emigrated to find work elsewhere, and much of the engineering is done by his granddaughter Fio. Porco is initially skeptical of Fios abilities as a mechanic, but after seeing her dedication in the project to repair his plane he accepts her as a competent engineer. With no males to assist in the project, Piccolo calls up an all-female team to repair the plane. When Porcos plane is finished, he is unexpectedly joined by Fio on his flight home, with the justification that if the secret police arrest the team, they can say that Porco forced them to help and took Fio as a hostage. Stopping off to refuel on the way, Porco discovers that the new Fascist government is beginning to hire seaplane pirates for their own use, thus putting him out of business.

Upon returning home, Porco and Fio are ambushed by the pirates, who threaten to kill Porco and destroy his plane. Fio successfully talks them out of it, but Curtis appears and challenges Porco to a final duel. Fio makes a deal with him declaring that if Porco wins, Curtis must pay off his debts owed the Piccolos company, and if Curtis wins, he may marry her.
 This aspect of the story has parallels to Roald Dahls short story They Shall Not Grow Old. {{cite web url = http://www.roalddahlfans.com/shortstories/they.php title = Short Stories: "They Shall Not Grow Old" work = RoaldDahlFans.com accessdate = 28 May 2012
 }}  {{cite web
 |title=Porco Rosso
 |url=http://www.barbican.org.uk/artgallery/event-detail.asp?ID=12384
 |publisher=Barbican Centre
 }} 
|group=note}}

The next day, the duel is arranged and a large crowd gathers on an island to observe. With the attendants betting heavily on the outcome, the contest begins. After an indecisive dogfight between Porco and Curtis, which soon dissolves into a bare fist boxing match, Gina appears to stop the fight and to warn the crowd that the Italian air force has been alerted and are on their way. Porco barely manages to win the fight upon her arrival, and hands Fio over to her, requesting Gina to look after her. Before the plane takes off, Fio gives Porco a kiss. With the crowd gone, Porco and Curtis agree to delay the air force together. When Curtis sees Porcos face he reacts with surprise. In the end, Fio becomes president of the Piccolo company, which is now an aircraft manufacturer, Curtis becomes a famous actor, the pirates continue to attend the Hotel Adriano.

==Cast==
{| class="wikitable"
|-
! Character!! Original cast!! Disney English dub cast
|-
| Porco Rosso || Shūichirō Moriyama || Michael Keaton
|-
| Donald Curtis || Akio Ōtsuka || Cary Elwes
|-
| Madame Gina || Tokiko Kato || Susan Egan
|-
| Mamma Aiuto Gang Boss || Tsunehiko Kamijo || Brad Garrett
|- Sanshi Katsura || David Ogden Stiers
|-
| Fio Piccolo || Akemi Okamura || Kimberly Williams-Paisley
|-
| Mamma Aiuto Gang || Reizō Nomoto Osamu Saka Yuu Shimaka || Bill Fagerbakke Kevin Michael Richardson Frank Welker
|}

==Production==
  war in Yugoslavia cast a shadow over production and prompted a more serious tone for the film, which had been set in Croatia. The airline remained a major investor in the film, and showed it as an in-flight film well before its theatrical release.     Due to this, the opening text introducing the film appears simultaneously in Japanese, Italian, Korean, English, Chinese, Spanish, Arabic, Russian, French and German.

==History and politics== First World War and is shown fighting against Austro-Hungarian fighter planes in a flashback sequence. The story is set in the Adriatic Sea east coast between Dalmatian and Kvarner islands.

Porco makes statements of his being Anti-fascism|anti-fascist, quipping during one scene that "Id much rather be a pig than a fascist".

Miyazaki shed light on the political context of the making of the film in an interview with Empire (film magazine)|Empire. He reflects that the conflicts that broke out during the films production (such as those in Yugoslavia, Dubrovnik, Croatia and the islands in which the story was based) made Porco Rosso into a more complicated and difficult film. 

==Homage to early aviation==
The fictional "Piccolo" aircraft company depicted in the film may be a reference to the Italian aircraft manufacturers Caproni and Piaggio Aerospace|Piaggio.
 Caproni C-22J, Fouga Zéphyr Magister jet trainer, and shares with both of these aircraft the inclusion of a V-tail.

Additionally, the Caproni Ca.309 light reconnaissance aircraft is known under the name "Ghibli", the same name as Miyazakis and Takahatas animation studio.

In the early 1930s, Italian seaplane designers set world speed records (such as the Macchi M.C.72 designed by the Italian airplane designer Mario Castoldi). One of the test pilots killed during the attempt to set the speed record was named Bellini, the name given to Porcos pilot friend in the film.

Marco Pagot, the name of the main character, is also a homage to the Pagot brothers, pioneers of Italian animation (Nino Pagot was the author of the first Italian animated feature film, The Dynamite Brothers, and his sons Marco and Gi Pagot were Miyazakis collaborators in the production of Sherlock Hound).

Meanwhile, the character of Curtis is likely to have been named after the American aviation pioneer Glenn Hammond Curtiss who, along with the Wright Brothers, founded the Curtiss-Wright Corporation. Curtis airplane is a Curtiss R3C, which was built for the 1925 Schneider Cup race (which Porco refers to when he first meets Curtis). His character is also an oblique reference to Ronald Reagan, in that his ambitions lie not only in Hollywood, but also the Presidency. In the 1930s this would indeed have seemed remarkably ridiculous (hence Gina laughing off his ambition), though modern viewers will gain a satisfied grin from Curtis on this score. The rest of Curtis character appears to come directly from the adventure film heroes portrayed by Errol Flynn at this time — indeed, they share a jaw line — including his buccaneering derring-do, willingness to fight, and overall demeanour combined with romantic ardour.

Miyazaki revisited the theme of aviation history in his 2013 film The Wind Rises.

==Soundtrack==
{{Infobox album
| Name        = Porco Rosso
| Type        = soundtrack
| Artist      = Joe Hisaishi
| Cover       = Porco rosso.jpg
| Length      =
| Recorded    =
| Released    = 22 July 1992
| Label       = Tokuma
}}

#"The Wind of Time (When a Human Can Be a Human)" – 2:50
#"MAMMAIUTO" – 1:21
#"Addio!" – 0:37
#"The Bygone Days" – 2:16
#"A Sepia-Coloured Picture" – 0:47
#"Serbia March" – 1:03
#"Flying Boatmen" – 2:36
#"Doom (Cloud Trap)" – 1:23
#"Porco e Bella" – 1:06
#"Fio-Seventeen" – 2:04
#"The Women of Piccolo" – 2:04
#"Friend" – 3:04
#"Partnership" – 2:28
#"Madness (Flight)" – 2:39
#"To the Adriatic Sea" – 1:50
#"In Search of the Distant Era" – 2:18
#"Love at First Sight in the Wildness" – 1:11
#"At the End of Summer" – 1:26
#"Lost Spirit" – 4:11
#"Dog Fight" – 2:10
#"Porco e Bella (Ending)" – 2:35 The Time of Cherries" (sung by Tokiko Kato) – 2:52
#"Once in a While, Talk of the Old Days" (composition, lyrics, singing by Tokiko Kato, arrangement by Yoko Kanno, Junichiro Ohkuchi) – 3:56

==Reception== Time Outs top 50 animated movie list.  On Rotten Tomatoes, the film has a rating of 94% based on 16 reviews. 

==Sequel==
In 2011 Miyazaki said that he wanted to make a follow-up anime to the 1992 original film if his next few films following   with Porco also returning, albeit this time as an old pilot, reflecting Miyazakis own aging.  Miyazaki is writing the film, but Hiromasa Yonebayashi will direct.  Due to both Miyazaki and Yonebayashis departure from Ghibli and the current hiatus of the studio itself, the current status on the proposed sequel remains uncertain.

==Notes==
 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 