Wadd: The Life & Times of John C. Holmes
{{Infobox film
| name           = Wadd: The Life & Times of John C. Holmes
| image          = File:WADD FINALS.jpg
| image_size     =
| caption        = DVD release cover
| director       = Cass Paley
| starring       = John C. Holmes Ron Jeremy Larry Flynt Misty Dawn Aunt Peg Paul Thomas Anderson Mike Sager
| producer       = Cass Paley Christopher Rowland Rodger Jacobs Mike Sager
| co-producer  = Christopher Rowland
| associate producer = Rodger Jacobs
| writer           = Rodger Jacobs
| interviewer   = Rodger Jacobs
| cinematography = Willie Boudevin
| editing        = Christopher Rowland
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| studio         =
| budget         =
}}
Wadd: The Life & Times of John C. Holmes is a 1998 documentary produced and directed by Cass Paley,   about adult film icon John C. Holmes. It was the winner of Best Feature Documentary at the 1999 South by Southwest Film Festival held annually in Austin, Texas. 

== Overview == Bob Chinn. It also covers the romantic relationships of John Holmes with first wife Sharon Holmes, his underage mistress Dawn Schiller, and his second wife, the porn actress Misty Dawn.  Finally, it covers Eddie Nash and Wonderland murders, as well as Holmes death from AIDS. It is narrated principally by journalist Mike Sager, whose Rolling Stone story "The Devil and John Holmes" inspired the films Boogie Nights and Wonderland (2003 film)|Wonderland.  It has been reformatted in HD for redistribution by The Sager Group. 

Wadd stars Ron Jeremy, Larry Flynt, Misty Dawn, Aunt Peg, Mike Sager, and Paul Thomas Anderson, among others, in addition to footage from the actor.

==Interviews==
* Bunny Bleu
* Aunt Peg
* Candida Royale
* Kitten Natividad
* Misty Dawn
* Ron Jeremy John Leslie
* Don Fernando Bob Chinn
* Paul Thomas Anderson
* Larry Flynt
* Sharon Holmes
* Bill Amerson
* Mike Sager

== Critical reception ==
Owen Gleiberman of Entertainment Weekly wrote, "Often, theres a conspiracy aspect to lost legends, but terrific films do fall through the cracks. Wadd: The Life & Times of John C. Holmes, which played the Toronto film festival and then slipped away, is the ultimate searching - and shocking - exposé of the porn world." 

Richard Corliss of Time Magazine said of the film, "Like Holmes, Wadd is seedy, twisted, a bit on the long side--and creepily fascinating." 

Stephen Holden of The New York Times wrote, "Wadd leaves us to ponder the difference between Holmess special gift and, say, another stars beautiful singing voice. When you come right down to it, theyre pretty much the same thing, but distributed to different parts of the body." 

Maitland McDonagh of TV Guide commented, "Holmess story isnt pretty, but its fascinating, in no small part because the people Paley interviews offer a glimpse into a brief time when making porn was an act of rebellion that attracted a diverse and eccentric group of filmmakers and performers." 

Journalist J.C. Maçek III of PopMatters and WorldsGreatestCritic.com wrote, "The thinking viewer probably wont like Holmes per-se, but theyll have a hard time not finding sympathy for him. And, yes, there are a couple of glimpses at Holmes mighty saber for the curious and biologically interested." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 