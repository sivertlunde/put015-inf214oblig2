That Fatal Sneeze
{{Infobox film
| name           = That Fatal Sneeze
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Lewin Fitzhamon
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Thurston Harris Gertie Potter
| music          = 
| cinematography = 
| editing        = 
| studio         = Hepworth Manufacturing Company
| distributor    = 
| released       =  
| runtime        = 6 min.
| country        = United Kingdom
| language       = 
| budget         = 
| gross          = 
}}
 Thurston Harris and Gertie Potter. 

==Plot== Thurston Harris) decides to play a practical joke on his nephew (Gertie Potter) at dinner, adding large amounts of pepper to the nephews food, prompting an uncontrollable outburst of sneezing. In revenge, the nephew puts pepper on several items in his uncles bedroom, so that when the uncle gets dressed the following morning, the pepper causes a violent sneezing fit, which shakes the room and knocks over the bed.

The pepper continues to have an effect on the uncle during the day, as his sneezing causes damage to a few shops. The shopkeepers and other onlookers start chasing the uncle, who runs to a policeman to ask for help. Before he can request assistance, the uncle sneezes violently again, which knocks over the policeman and a lamppost, as well as causing damage to a house. In an attempt to escape the crowd that is pursuing him, the uncle climbs up a ladder, but this breaks when he sneezes again. The uncles sneezing continues to get worse, with one sneeze causing the world to shake. Eventually a final sneeze causes the uncle to explode.

==Production==
The film contains a number of primitive special effects, including the use of wires to move objects when the uncle sneezes, and the balancing of the camera on a rocking board to give the impression that the world is swaying in the penultimate shot. The dramatic final scene, in which the uncle explodes, was achieve by stopping the camera and replacing the actor with a small smoke-emitting device. 

==References==
{{reflist|refs=
    
}}

 
 
 
 
 

 
 