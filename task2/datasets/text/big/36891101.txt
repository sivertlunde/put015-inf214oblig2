The Worst Week of My Life (film)
 
{{Infobox film
| name           = The Worst Week of My Life
| image          = The Worst Week of My Life (film).jpg
| caption        = Film poster
| director       = Alessandro Genovesi
| producer       = Maurizio Totti
| writer         = Fabio De Luigi Alessandro Genovesi
| starring       = Fabio De Luigi Cristiana Capotondi Alessandro Siani
| music          = Pivio and Aldo De Scalzi
| cinematography = Federico Masiero
| editing        = Claudio Di Mauro
| distributor    = Warner Bros. Italy
| released       =  
| runtime        = 93 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 same name.   

==Plot==
Paolo is an advertising agent of Milan who is madly in love with the rich Margherita, of a wealthy family. The two decide to get married, and a week before the wedding, Paolo is invited by the parents and relatives of Margherita. They just want to know Paolo, as Margherita has spoken so highly of him; Paolo, however, proves to be very clumsy and awkward, resulting in catastrophic and extremely embarrassing situations. The troubles in the family also increases when Ivano, Neapolitan and friend of Paolo, comes at the parental home of Margherita as a witness of marriage.

==Cast==
* Fabio De Luigi as Paolo
* Cristiana Capotondi as Margherita 
* Monica Guerritore as Clara
* Antonio Catania as Giorgio
* Alessandro Siani as Ivano
* Gisella Sofio as the grandmother
* Nadir Caselli as Ginevra
* Chiara Francini as Simona
* Andrea Mingardi as Dino, Paolos father
* Arisa as Martina

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 