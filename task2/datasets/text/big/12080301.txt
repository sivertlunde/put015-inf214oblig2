The Hottest State
{{Infobox Film
| name     = The Hottest State
| image    = Poster_the-hottest-state.jpg
| caption  = Theatrical release poster
| director = Ethan Hawke
| writer   = Ethan Hawke
| based on =   Mark Webber Michelle Williams Laura Linney Ethan Hawke
| producer = Alexis Alexanian Yukie Kito
| editing  = Adriana Pacheco
| music    = Jesse Harris
| cinematography = Chris Norr
| distributor = THINKFilm
| released =   
| country  = United States
| runtime  = 117 minutes
| language = English
}}
The Hottest State is a 2006 drama film directed and written by Ethan Hawke, which he based on the novel of the same name that he had written and published ten years earlier, in 1996. The film debuted at the Venice Film Festival on September 2, 2006, and received a limited theatrical release in the United States on August 24, 2007. It ran for 5 weeks and grossed $137,341 internationally.  The film was subsequently issued on DVD in December 2007.

==Synopsis== Mark Webber) who falls in love for the first time with an aspiring singer, Sarah (Catalina Sandino Moreno). As their love blossoms and languishes, young William reexamines himself and his relationship with his mother Jesse (Laura Linney) and estranged father Vince (Hawke himself).

==Cast== Mark Webber as William Harding
* Catalina Sandino Moreno as Sarah Michelle Williams as Samantha
* Laura Linney as Jesse
* Ethan Hawke as Vince Daniel Ross as Young Vince
* Anne Clarke as Young Jesse

==Soundtrack==
{{Album ratings
|rev1=Allmusic
|rev1Score=  
|rev2=The A.V. Club
|rev2Score=B 
|rev3=IGN
|rev3Score=8.7/10 
|rev4=Pitchfork Media
|rev4Score=2.2/10 
}} Bright Eyes, New York Daily News. 

===Track listing===
#"Ya No Te Veria Mas (Never See You)" - 2:06 (Rocha)
#"Always Seem To Get Things Wrong" - 3:47 (Willie Nelson)
#"Somewhere Down The Road" - 2:44 (Feist (singer)|Feist) Bright Eyes)
#"The Speed of Sound" - 4:19 (Emmylou Harris)
#"It Will Stay With Us"	- 2:17 (Jesse Harris)
#"If You Ever Slip" - 2:33 (The Black Keys)	
#"Crooked Lines" - 4:28 (M. Ward)	
#"World of Trouble" - 4:35 (Norah Jones)
#"Never See You" - 5:04 (Brad Mehldau)
#"Its Alright To Fail" - 3:40 (Cat Power)
#"One Day The Dam Will Break" - 2:58 (Jesse Harris)
#"You, The Queen" - 4:17 (Tony Scherr)
#"Morning In A Strange City (Cafe)" - 2:00
#"No More" - 3:59 (Rocha)
#"Dear Dorothy" - 2:28 (Jesse Harris)
#"Never See You" - 2:46 (Rocha)
#"There Are No Good Second Chances" - 4:58

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 