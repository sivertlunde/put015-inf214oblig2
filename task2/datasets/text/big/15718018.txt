Motown High
{{Infobox Film
| name           =  Motown High
| image          =  
| image_size     = 
| caption        =  
| director       =  Barbara Hager
| producer       =   
| writer         =   
| narrator       =   
| starring       =  
| music          =   
| cinematography =   
| editing        =   
| studio         =   
| distributor    =   
| released       =   
| runtime        =   
| country        =  
| language       =   
| budget         =   
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Motown High is a documentary by Canadian director Barbara Hager about a group of talented Canadian high school students who develop a musical bond with their idol, Motown diva Martha Reeves, after they meet her on a school band trip to Detroit. The documentary was the opening film at the 2008 Victoria Film Festival.

The documentary was shot over a two-year period starting in April 2005 when the Vic High R&B Band first met Martha after she accepted an invitation from Victoria High School band teacher Eric Emde to meet the students in Detroit. Ms. Reeves was so impressed by the students dedication to the Motown sound, that she decided to mentor them after seeing them perform at Northwestern High School in Detroit.

Two months after meeting Martha in Detroit, the Canadian students invited her to Victoria, British Columbia|Victoria. She accepted and spent three days with the Vic High R&B Band—presenting a Motown workshop, revealing stories about her life as a Motown star and joining them on stage at a local nightclub.

A year later, Martha was invited to headline the Victoria International Jazz Festival and she immediately called Eric to see if the students would perform with her. For a group of 15- to 19-year-olds, the opportunity to perform with a Motown legend was both exciting and terrifying. The timing couldn’t have been worse. Four of the band’s lead singers were about to graduate, and the daily band rehearsals competed with final exams and graduation ceremonies.

Two days before the concert, Martha arrived with her musical director Alonza McKenzie and the students had to re-learn many of the songs they will perform with her because they had learned them in the wrong key or with different endings.
 Victoria High School. The documentary mixes cinema verite, performance and interview footage to build a story about dreams and how young musicians can be profoundly influenced by their mentors and teachers.

The documentary is a celebration of musical mentoring by a high school teacher and a Motown superstar who are committed to grooming these gifted students into professional musicians.

A one-hour version of the documentary, Sounds Like Motown, has been licensed to Bravo! in Canada.

==External links==

Trailer: http://www.aarrowentertainment.com

Vic High sets Motown jumpin
Michael D. Reid, Times Colonist
Published: Tuesday, October 23, 2007
* http://www.canada.com/victoriatimescolonist/news/story.html?id=1ae9b1a3-d48f-4f7f-8f8d-bad17071bf93&k=57791

 
 
 
 
 

 