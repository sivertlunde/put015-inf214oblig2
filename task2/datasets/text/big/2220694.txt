Can't Stop the Music
 
 
{{Infobox film
| title          = Cant Stop the Music
| image          = Cantstopthemusic.JPG
| alt            = 
| caption        = Theatrical release poster
| director       = Nancy Walker
| producer       = {{Plainlist|
* Allan Carr
* Henri Belolo
* Jacques Morali}}
| writer         = {{Plainlist|
* Allan Carr
* Bronte Woodard|Bronté Woodard}}
| starring       = {{Plainlist|
* Village People
* Valerie Perrine
* Bruce Jenner
* Steve Guttenberg
* Paul Sand
* Tammy Grimes}}
| music          = Jacques Morali Bill Butler
| editing        = John F. Burnett EMI Films
| distributor    = Associated Film Distribution
| released       =  
| runtime        = 124 minutes  
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $2 million
}} musical comedy independent distributor Associated Film Distribution (AFD).
 first winner of the Worst Picture Golden Raspberry Award, for it was a double feature of this and Xanadu (film)|Xanadu that inspired John J. B. Wilson to start the Razzies.   

==Plot== DJing at disco Saddle Tramps. His roommate Sam Simpson (Valerie Perrine), a supermodel newly retired at the peak of her success, sees the response to a song he wrote for her ("Samantha") and agrees to use her connections to get him a record deal. Her connection, ex-boyfriend Steve Waits (Paul Sand), president of Marrakech Records (a reference to Village People record label Casablanca Records), is more interested in getting back with her than in Jacks music (and more interested in taking business calls than in wooing Samantha), but agrees to listen to a Demo (music)|demo.
 David "Scar" Randy Jones Girl Friday Lulu Brecht (Marilyn Sokol) to attend, hoping to lure the star back. Ron White (Bruce Jenner), a lawyer from St. Louis, Missouri|St. Louis, is mugged by an elderly woman on his way to deliver a cake Sams sister sent, and shows up on edge. Brecht gets Jack high, which unnerves him when her friend Alicia Edwards brings singing cop Ray Simpson, but Jack records the quartet on "Magic Night". Ron, pawed all night by the man-hungry Brecht, is overwhelmed by the culture shock of it all and walks out.
 spend the Glenn M. leatherman climbs atop a piano for a rendition of "Danny Boy", and he and Alex Briley, the G.I. (military)|G.I. join up. Now a sextet, they get their name from an offhand remark by Rons socialite mother Norma. Rons boss, Richard Montgomery (Russell Nype), overwhelmed by the carnival atmosphere, insists the firm not represent the group, and Ron quits.
 non R-rated offerings to feature full-frontal male nudity). The group cut a demo ("Liberation") for Marrakech, but Steve sees limited appeal and Sam refuses his paltry contract. Reluctant to use her savings, they decide to self-finance by throwing a pay-party.
 chorine mother Helen (June Havoc) who show up, to hash out a contract. Initially reluctant, Helen seduces Steve with her kreplach and before long theyre negotiating the T-shirt merchandising for the Japanese market.

In the dressing room before the show, Ron, relieved to learn Sam didnt travel with Steve, proposes to her. At one point, Montgomery shows up to rehire Ron as a junior partner representing the group. Following a set by The Ritchie Family ("Give Me a Break"), the Village People make a triumphant debut ("Cant Stop the Music (song)|Cant Stop the Music").

==Cast==
 
* Steve Guttenberg as Jack Morell
* Valerie Perrine as Samantha "Sam" Simpson
* Bruce Jenner as Ron White
* Paul Sand as Steve Waits
* Tammy Grimes as Sydney Channing
* Village People:
** Alex Briley as Alex
** David Hodo as David Glenn Hughes as Glenn Randy Jones as Randy
** Felipe Rose as Felipe
** Ray Simpson as Ray
* June Havoc as Helen Morell
* Barbara Rush as Norma White
* Altovise Davis as Alicia Edwards
* Marilyn Sokol as Lulu Brecht
* Russell Nype as Richard Montgomery
* Jack Weston as Benny Murray
* Leigh Taylor-Young as Claudia Walters
* Dick Patterson as Record store manager
 

==Production== Golden Globes,  and eight Emmy Awards|Emmys.   
 Ida Morgenstern in several episodes of The Mary Tyler Moore Show and continued that role in its Spin-off (media)|spin-off Rhoda. After establishing the character, Walker directed some episodes of both series, along with episodes of other situation comedy series. Cant Stop the Music was her lone effort at film direction, as after it, Walker turned her attention back to acting in television.

===Casting=== Jack and Jill, which, like Cant Stop the Music won the Golden Raspberry Award for Worst Picture.
 punk group heavy metal group W.A.S.P.) and James Marcel (who would later find greater success with the name James Wilder). Background dancers included Perri Lister, girlfriend of Billy Idol and mother to his son, and Peter Tramm, who would go on to appear in dozens of music videos and double for Kevin Bacon in Footloose (1984 film)|Footloose.

Ray Simpsons role was originally intended for Victor Willis, the original lead singer of the Village People who quit the group during pre-production of this film.  Wanting to assert his heterosexuality amongst the gay-themed group, Willis had insisted his then-wife, Phylicia Ayers-Allen (later Phylicia Rashād), be written into the film as his girlfriend.  When he quit the group, Ayers-Allen was fired and replaced by Altovise Davis. Hofler, Robert (2010). Party Animals: A Hollywood Tale of Sex, Drugs, and Rock n Roll Starring the Fabulous Allan Carr New York: Da Capo Press. Pg. 105-118 

===Filming=== Bill Butler to direct in her place. 
 shooting began in May 1979 at the height of the disco craze. Carr took a hands-on role with the production, and personally directed and cast the male athlete extras for the "YMCA" musical sequence.  He had attempted to cast Grease star Olivia Newton-John in this film as Samantha, but after discussions between her producer, John Farrar, and Morali over who would write Newton-Johns numbers, Newton-John instead signed on to play the lead in Xanadu (film)|Xanadu. 

The bands silver and white costumes in the "Milkshake" sequence and red costumes in the finale sequences were designed by Tony- and Academy Award|Oscar-winning  designer Theoni V. Aldredge.
 Macho Man" — do not appear in the film, though in reference to the latter, Perrine wears a T-shirt emblazoned with the words "Macho Woman" as she jogs through the mens locker room at the YMCA.

Another reference to one of the bands songs, "San Francisco (Youve Got Me)" appears in the opening credits as Jack passes a group of three women with the words San Francisco printed on their T-shirts.
 MGM Studios in Culver City, California, with location shooting in New York City and San Francisco. Location shooting in New York was somewhat complicated by adjacent protests by gay activists over the William Friedkin film Cruising (film)|Cruising (starring Al Pacino), which was filming on location nearby.  The two productions were mistaken for each other more than once, with protestors disrupting the Cant Stop the Music location shoots when they intended to halt production of Cruising.  A few weeks prior to release, Jenner and Perrine hosted a TV special, Allan Carrs Magic Night, to promote the film.

===Music=== Dennis "Fergie" Frederiksen, who was the lead singer for several popular rock bands during the 1980s. London/Frederiksen also sings a second song on the soundtrack, "The Sound of the City". 
 David London
# "Samantha" - David London
# "I Love You to Death"
# "Sophistication" - The Ritchie Family
# "Give Me a Break" - The Ritchie Family
# "Liberation"
# "Magic Night"
# "YMCA (song)|Y.M.C.A."
# "Milkshake"
# "Cant Stop the Music (song)|Cant Stop the Music"

==Release== colossal failure financially, bringing in only a tenth of that in gross revenue,  and is considered one of the reasons for the downfall of AFD.

"Our timing was wrong, and in this business, timing is everything," wrote Lew Grade who invested in the movie. Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 252 

Carrs next film, Grease 2, brought in more than twice as much on its opening weekend as this film grossed in its entire run. Even though it was considered a failure, Grease 2 nearly made back its investment in the U.S. gross alone. 
 cult status camp film. New Years tradition on Australian television.   

==Critical response==
Cant Stop the Music has received very negative reviews from critics. It currently holds an 8% "rotten" score on Rotten Tomatoes.  The New York Times gave the film a scathing review, calling it "thoroughly homogenized."  Variety (magazine)|Variety magazine felt likewise, writing "The Village People, along with ex-Olympic decathlon champion Bruce Jenner, have a long way to go in the acting stakes."  Nell Minow of Yahoo! Movies called the film "an absolute trainwreck of a movie", but that it had "some hilariously campy moments."  Baskin-Robbins|Baskin-Robbins Ice Cream sold a flavor called "Cant Stop the Nuts" as part of the promotion of the film.  Gene Siskel and Roger Ebert selected the film as one of their "dogs of the year" in a 1980 episode of Sneak Previews. 

The film is listed in Golden Raspberry Award founder John Wilsons book The Official Razzie Movie Guide as one of The 100 Most Enjoyably Bad Movies Ever Made. 

===Awards and nominations===
{| class="wikitable"
|-  style="background:#b0c4de; text-align:center;"
! Award
! Subject
! Nominee
! Result
|- Young Artist Awards Best Family Music Album
|
| 
|- Golden Raspberry Awards Golden Raspberry Worst Picture
|
| 
|- Golden Raspberry Worst Actor Bruce Jenner
| 
|- Golden Raspberry Worst Actress Valerie Perrine
| 
|- Golden Raspberry Worst Supporting Actress Marilyn Sokol
| 
|- Golden Raspberry Worst Director Nancy Walker
| 
|- Golden Raspberry Worst Screenplay Bronte Woodard and Allan Carr
| 
|- Golden Raspberry Worst Original Song Jacques Morali for "Cant Stop the Music (song)|(You) Cant Stop the Music"
| 
|-
|}

==Home media==
Cant Stop the Music was released on Region 1 DVD on April 16, 2002.

==See also==
;Other films of the late 1970s during the disco craze:
* Saturday Night Fever (1977)
* Thank God Its Friday (1978) The Stud (1978) The Music Machine (1979)
* Roller Boogie (1979)
* Skatetown, U.S.A. (1979)
* Xanadu (film)|Xanadu (1980)
* Fame (1980 film)|Fame (1980) The Apple (1980)

==References==
 

==External links==
*  
*  
*  
*  
*  

   
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 