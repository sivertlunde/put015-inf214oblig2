Serena (1962 film)
{{Infobox film
| name           = Serena
| image          =
| image_size     =
| caption        =
| director       = Peter Maxwell
| producer       = John I. Phillips
| writer         = Edward Abraham Valerie Abraham Reginald Hearne Emrys Jones Honor Blackman
| music          = Johnny Gregory
| cinematography = Stephen Dade
| editing        = Allan Morrison
| studio         =
| distributor    = Butchers Film Service
| released       = 1962
| runtime        = 60 mins
| country        = United Kingdom
| language       = English
}}
Serena is a 1962 British crime thriller directed by Peter Maxwell. 

==Cast==  
* Patrick Holt as Inspector Gregory  Emrys Jones as Howard Rogers 
* Honor Blackman as Ann Rogers 
* Bruce Beeby as Sergeant Conway  John Horsley as Mr. Fisher 
* Robert Perceval as Bank Manager 
* Wally Patch as Barman 
* Gerry Duggan as Norman Cole 
* Peter Glaze as Station Booking Clerk 
* Howard Greene as River Police Sergeant 
* Reginald Hearne as Doctor 
* Lawrence James as Uniformed Constable 
* Benedicta Leigh as Policewoman Scott 
* Barry Linehan as Forensic Chemist 
* Bill Mills as Photographer 
* Frank Pettitt as Fred 
* Colin Rix as Plainclothes Detective

==References==
 

==External links==
*  

 
 
 
 
 

 
 