20,000 Leagues Under the Sea (1954 film)
 
{{Infobox film
| name = 20,000 Leagues Under the Sea
| image = 20000leaguesposter.jpg Theatrical release poster
| director = Richard Fleischer
| producer = Walt Disney  }}
| screenplay = Earl Felton
| based on =  
| starring =   Paul Smith
| cinematography = Franz Planer
| editing = Elmo Williams Walt Disney Productions Buena Vista Distribution
| released =  
| runtime = 127&nbsp;minutes
| country = United States
| language = English
| budget = $5 million   The Numbers. Retrieved: April 15, 2013. 
| gross = $28,200,000 
}}
20,000 Leagues Under the Sea is a 1954 American  . The film is adapted from Jules Vernes 19th century novel Twenty Thousand Leagues Under the Sea and is considered an early example of the steampunk genre.  This film is also Disneys fifth live-action film overall.

==Plot==
In 1868, rumors of a sea monster attacking ships in the Pacific Ocean have created apprehension and fear among sailors, disrupting shipping lanes. The United States government invites Professor Pierre M. Aronnax (Paul Lukas) and his assistant, Conseil (Peter Lorre), onto an expedition to prove or disprove the monsters existence. On board with them is the cocky master harpooner Ned Land (Kirk Douglas).

After months of searching, the "monster" is spotted. Though the ship fires at it with cannons, the monster rams the ship. Ned and Aronnax are thrown overboard, and Conseil goes in after Aronnax. The warship, burning and helpless, drifts silently and no one on board answers when the overboard passengers cry for help. The three drift in the ocean, eventually finding a strange-looking metal vessel, and realize the "monster" is a man-made "submerging boat" that appears deserted. Inside, Aronnax finds a viewing window and sees an underwater funeral.

Ned, Aronnax and Conseil then attempt to leave in their lifeboat, but the submarine crew returns to their ship, capturing the castaways. The captain introduces himself as Nemo (James Mason), master of the Nautilus (Verne)|Nautilus. He returns Ned and Conseil to the deck, while offering Aronnax, whom he recognizes for his work and research, the chance to stay. When Nemo discovers that Aronnax is willing to die with his companions, he allows Ned and Conseil to board the submarine.

Nemo takes Aronnax to the penal colony island of Rura Penthe. Nemo reveals he was once a prisoner there, as were many of his crew. The prisoners are loading an ammunitions ship. The Nautilus rams the ship, destroying its cargo and killing the crew. An anguished Nemo tells Arronax that his actions have saved thousands from death in war; he also discloses that this "hated nation" tortured his wife and son to death while attempting to force him to reveal the secrets of his work. Ned discovers the coordinates of Nemos secret island base, Vulcania, and releases messages in bottles, hoping somebody will find them and free him from captivity.

Off the coast of New Guinea, the Nautilus becomes stranded on a reef. Ned is surprised when Nemo allows him to go ashore with Conseil, ostensibly to collect specimens. Ned goes off alone to explore avenues of escape. While kneeling at a pool to drink he sees a number of human skulls on stakes. Realizing his danger, Ned runs for his life and rejoins Conseil as they are chased back to the Nautilus by cannibals. Despite remaining aground, Nemo is unconcerned and the cannibals are repelled from the ship by electrical charges circulated on its hull. Nemo is furious with Ned for not following his orders, and confines him to the submarines brig.

A warship approaches, firing upon the submarine. It descends into the depths, where it attracts the attentions of a giant squid. The electric charge fails to repel the monster, so Nemo and his men surface to dislodge the beast. Nemo is caught in one of the squids tentacles. Ned, having escaped from captivity during the struggle, jumps to Nemos rescue, saving his captors life. As a result, Nemo has a change of heart; he claims now to want to make peace with the outer world.
 slug to captain in death.

Aronnax, Conseil and Ned are confined to their cabins. The Nautiluss crew also retreat to their cabins at Nemos instructions. Ned breaks loose and manages to surface the Nautilus, hitting a reef in the process and causing the ship to begin flooding. Nemo staggers to a viewing window and watches his beloved ocean as he dies.

Aronnax tries to retrieve his journal, which contains an account of the voyage, but the urgency of their escape obliges Ned to knock him unconscious and carry him out. The companions witness Vulcania destroyed in an explosion, and Ned apologizes to Aronnax for hitting him. As the Nautilus disappears beneath the waves, Nemos last words to Aronnax echo: "There is hope for the future. And when the world is ready for a new and better life, all this will someday come to pass, in Gods good time."

==Cast==
 , Kirk Douglas, Peter Lorre, and Paul Lukas.]]
 

 

==Production==
20,000 Leagues Under the Sea was filmed at various locations in  , December 3, 2009. Retrieved: January 9, 2015.  Some of the location filming sequences were so complex that they required a technical crew of more than 400 people. The film presented many other challenges, as well. The famous giant squid attack sequence had to be entirely re-shot, as it was originally filmed as taking place at dusk and in a calm sea.    The sequence was filmed again, this time taking place at night and during a huge gale, both to increase the drama and to better hide the cables and other mechanical workings of the animatronic squid. 

Cost overruns during production made the film very expensive for a Disney production, although by no means as expensive as other recent releases:  , (narrator)."]  IMDb, May 20, 2003. Retrieved: August 18, 2010. 

==Reception==
  for the 1954 Walt Disney film.]]
Upon the films original release,  , the usually prickly critic Richard Schickel, stated that James Mason was "superbly cast as the mad inventor Captain Nemo". 

Audiences remember it primarily for its giant-squid battle sequence as well as the Nautilus itself and James Masons portrayal of Nemo.   Widescreen Museum. Retrieved: January 9, 2015.  The film currently holds an 89% approval rating at the review aggregation website  . Retrieved: July 30, 2010. 
 Treasure Island (1950), and Richard Todd, another well-known star of British films, had appeared in a Disney Technicolor live-action version of The Story of Robin Hood and His Merrie Men (1952). Mason especially was singled out for his performance of Captain Nemo. Many people who had first seen him on-screen in the film identify him most strongly with this role.   Solar Navigator (Max Energy Limited), 2013. Retrieved: January 9, 2015.    Eccentric Cinema, May 26, 2003. Retrieved: January 9, 2015. 

Modern-day film critic Steve Biodrowski said that the film is "far superior to the majority of genre efforts from the period (or any period, for that matter), with production design and technical effects that have dated hardly at all." Biodrowski also added that the film "may occasionally succumb to some of the problems inherent in the source material (the episodic nature does slow the pace), but the strengths far outweigh the weaknesses, making this one of the greatest science-fiction films ever made." 

  from 1971 to 1994 which consisted of a submarine ride, complete with the giant squid attack. For this ride, voice artist  , named Les Mystères du Nautilus, opened,  and a dark ride at Tokyo DisneySea was created in 2001. 
 second highest White Christmas), earning $8 million in box office attendance in North America  and has become a notable classic film of the Disney corporation.

===Awards and nominations===
The film won two Academy Awards and was nominated for one more.   The New York Times. Retrieved: January 9, 2015.  
* Academy Awards (1954) Best Art John Meehan, Emile Kuri) Best Special Effects (John Hench, Joshua Meador) Best Film Editing (Elmo Williams)

The films primary art designer, Harper Goff, who designed the Nautilus, was not a member of the Art Directors Union in 1954 and therefore, under a bylaw within the Academy of Motion Pictures, he was unable to receive his Academy Award for Art Direction.   Turner Classic Movies. Retrieved: January 9, 2015. 

American Film Institute recognition
* AFIs 100 Years... 100 Movies - Nominated  
* AFIs 100 Years...100 Thrills - Nominated 

==Record albums== score or sea chanties with his crew. The albums also had Nemo surviving at the end and releasing Ned, Arronax, and Conseil out of gratitude for their saving his life.  In this version, Ned, Aronnax and Consel were not shipwrecked because the Nautilus rammed the ship they were on, but because a hurricane came up. 
 LP with no accompanying booklet and no liner notes – the usual practice with most Disneyland label albums. It contained much more of the films plot, but with many of the same alterations as the first album, so this recording was technically a remake of the earlier one. The cast for the 1963 album was uncredited. Neither album listed the films credits or made any mention of the films cast.
 Toccata and Fugue in D minor is played by Nemo on the Nautiluss organ, but James Masons playing is actually dubbed by an anonymous organist.

===Official soundtrack===
{{Infobox album
| Name = 20,000 Leagues Under the Sea (Soundtrack)
| Type = Soundtrack
| Artist = Various Artists
| Cover = 
| Released = January 29, 2008
| Recorded = digital download
| Genre = Soundtrack
| Length = 1:18:23 Walt Disney
| Producer = Randy Thorton
}}
 digital album Paul Smiths original soundtrack score to 20,000 Leagues Under the Sea plus both sides of the "A Whale of a Tale" single, as well as a digital booklet companion that explores the music of the film. This was the first official release of the film score and was initially available only through the iTunes Store.    iTunes Store. Retrieved: January 9, 2015.  Intrada released the same soundtrack on CD in 2011.   Intrada. Retrieved: January 9, 2015. 

{{Track listing
| collapsed = no
| headline = Track listing
| extra_column = Artist
| total_length = 1:18:23
| title1 = Main Title (Captain Nemo’s Theme) Paul Smith
| length1 = 2:26
| title2 = Street Fight
| extra2 = Paul Smith
| length2 = 1:04
| title3 = Aboard the Abraham Lincoln / Hunting the Monster
| extra3 = Paul Smith
| length3 = 2:28
| title4 = A Whale of a Tale
| extra4 = Kirk Douglas
| length4 = 2:09
| title5 = The Monster Attacks
| extra5 = Paul Smith
| length5 = 2:21
| title6 = Deserted Sub / Burial / Captured
| extra6 = Paul Smith
| length6 = 9:14
| title7 = Fifty Fathoms / The Island of Crespo
| extra7 = Paul Smith
| length7 = 8:45
| title8 = Storm at Sea / Nemo Plays
| extra8 = Paul Smith
| length8 = 2:25
| title9 = Strange Man of the Seas
| extra9 = Paul Smith
| length9 = 4:04
| title10 = Nemo’s Torment
| extra10 = Paul Smith
| length10 = 0:59
| title11 = Justified Hate
| extra11 = Paul Smith
| length11 = 1:29
| title12 = Searching Nemo’s Cabin
| extra12 = Paul Smith
| length12 = 4:02
| title13 = Ned’s Bottles
| extra13 = Paul Smith
| length13 = 0:43
| title14 = Ashore at New Guinea
| extra14 = Paul Smith
| length14 = 2:54
| title15 = Native Drums / Back to the Nautilus
| extra15 = Paul Smith
| length15 = 3:08
| title16 = Submerge
| extra16 = Paul Smith
| length16 = 1:45
| title17 = The Giant Squid
| extra17 = Paul Smith
| length17 = 6:53
| title18 = Ambush at Vulcania
| extra18 = Paul Smith
| length18 = 4:47
| title19 = Nemo Wounded
| extra19 = Paul Smith
| length19 = 2:43
| title20 = Escape from Vulcania
| extra20 = Paul Smith
| length20 = 3:41
| title21 = Finale / Deep Is the Mighty Ocean
| extra21 = Paul Smith
| length21 = 0:56
| title22 = A Whale of a Tale (Single)
| extra22 = Kirk Douglas
| length22 = 2:11
| title23 = And the Moon Grew Brighter and Brighter (Single B-Side)
| extra23 = Kirk Douglas
| length23 = 2:35
| title24 = A Whale of a Tale
| extra24 = Bill Kanady
| length24 = 2:24
| title25 = A Whale of a Tale
| extra25 = The Wellingtons
| length25  = 2:07
| title26 = A Whale of a Tale (Reprise)
| extra26 = Kirk Douglas
| length26 = 0:11
}}

==Home media==
The film has been released on VHS and DVD. An HD version was released on iTunes. 

==Remake==
On January 6, 2009,  , a.k.a. "McG", attached to direct. The film serves as an origin story for the central character, Captain Nemo, as he builds his warship, the Nautilus.  McG has remarked that it will be "much more in keeping with the spirit of the novel" than Richard Fleischers film, in which it will reveal "what Aronnax is up to and the becoming of Captain Nemo, and how the man became at war with war itself." It was written by Bill Marsilli, with Justin Marks and Randall Wallace brought in to do rewrites.  The film was to be produced by Sean Bailey with McGs Wonderland Sound and Vision. 

McG once suggested that he wanted Will Smith for the Captain Nemo role, but he has reportedly turned down the part.   As a second possible choice, McG had mentioned Sam Worthington, whom he worked with on Terminator Salvation, though they did not ever discuss it seriously. The project was later shelved in November 2009 with McG backing out of directing. 

During the 2010   (2011), it was speculated that 20,000 Leagues Under the Sea would enter principal photography by late 2012.  In the meantime, Fincher began courting Brad Pitt to play the role of Ned Land while the film was kept on hold.  However in February 2013, it was announced that Pitt had officially turned down the role. 

In April 2013, it was announced that the Australian government will provide a one-off incentive of $20 million in order to secure the production.  Despite this, the film was put on hold again the following month due to complications in casting a lead.  On July 17, 2013, Fincher dropped out of the film to direct the adaptation of  . Child, Ben.   The Guardian, September 1, 2014. Retrieved: January 9, 2015. 

==See also==
*  , an attraction at the Magic Kingdom theme park at Walt Disney World Resort from 1971 through 1994.

==References==
Notes
 
Citations
 
Bibliography
 
* Schickel, Richard. The Disney Version: The Life, Times, Art and Commerce of Walt Disney (Third ed.). Chicago: Ivan R. Dee, 1997. ISBN 978-1-56663-158-7.
 

==External links==
 
 
*  
*   at  
*  
*  on Favorite Story: December 20, 1947

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 