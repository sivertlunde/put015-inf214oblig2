Question 7
{{Infobox film
| name = Question 7
| image_size =
| image	= Question 7 FilmPoster.jpeg
| caption =
| director = Stuart Rosenberg
| producer = Lothar Wolff
| writer = Allan Sloane
| narrator =
| starring = Michael Gwynn Margaret Jahnen
| music = Hans-Martin Majewski
| cinematography = Günther Senftleben
| editing = Georges Klotz
| distributor = Louis De Rochemont Associates (USA)
| released =  
| runtime = 106 minutes
| country = United States West Germany
| language = English
| budget =
| gross =
}}
 1961 film directed by Stuart Rosenberg and starring Michael Gwynn, Margaret Jahnen and Christian de Bresson. It won the National Board of Review Award for Best Film. It was also entered into the 11th Berlin International Film Festival.   

==Plot==
In post-war East Germany, Peter Gottfried is the son of minister Friedrich Gottfried. The Communist regime has decreed that all children of "dissidents" will be denied entry to a prestigious music conservatory. Peter is anxious to be accepted, and in order to get in he prepares to answer the seven questions required by the conservatory, the seventh of which will require him to deny his religious convictions. Before this can happen, he is invited by the Communist Party to perform at the Berlin Youth Festival. Friedrich protests, knowing that the Communists intend to use his son as a political pawn, to "prove" to the world that East Germany affords equal rights to clergymen. In the end, it is Peter himself who decides to quit the Festival and defect to the West.

==Cast==
* Michael Gwynn as Friedrich Gottfried
* Margaret Jahnen as Gerda Gottfried
* Christian De Bresson as Peter Gottfried
* Almut Eggert as Anneliese Zingler
* Erik Schumann as Rolf Starke
* Max Buchsbaum as Inspector Hermann
* John Ruddock as Martin Kraus
* Leo Bieber as Herr Rettmann
* Fritz Wepper as Heinz Dehmert
* Eduard Linkers as Otto Zingler
* Marianne Schubarth as Marta Zingler
* Philo Hauser as Barber
* Rolf von Nauckhoff as Karl Marschall
* Helmo Kindermann as Luedtke
* Manfred Fuerst as Professor Steffl

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 