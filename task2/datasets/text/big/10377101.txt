Counsellor at Law
{{Infobox film
| name           = Counsellor at Law
| image          = CounsellorAtLawPoster.jpg
| image_size     = 250px
| caption        = original poster
| director       = William Wyler
| producer       = Carl Laemmle, Jr.
| writer         = Elmer Rice
| starring       = John Barrymore
| cinematography = Norbert Brodine
| editing        = Daniel Mandell
| distributor    = Universal Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Counsellor at Law is a 1933 American drama film directed by William Wyler. The screenplay by Elmer Rice is based on his 1931 Broadway play of the same title.  

==Plot== perjure himself on the witness stand because he believed the man could be rehabilitated if freed. Rival lawyer Francis Clark Baird has learned about the incident and is threatening to expose George, which will lead to his disbarment. The possibility of a public scandal horrifies his socialite wife Cora, who plans to flee to Europe with Roy Darwin. Devastated by his wifes infidelity, George is about to leap from the window of his office in the Empire State Building when his secretary Regina, who is in love with him, comes to his rescue.   

==Cast==
  
*John Barrymore as George Simon
*Bebe Daniels as Regina Gordon
*Doris Kenyon as Cora Simon
*Isabel Jewell as Bessie Green
*Melvyn Douglas as Roy Darwin
*Onslow Stevens as John Tedesco
 
*Thelma Todd as Lillian La Rue
*Mayo Methot as Zedorah Chapman
*Vincent Sherman as Harry Becker
*Richard Quine as Richard Dwight, Jr.
*John Qualen as Breitstein
*Angela Jacobs as Goldie
 

Cast notes
*Clara Langsner, J. Hammond Dailey, Malka Kornstein, Marvin Kline, T. H. Manning, John Qualen, Angela Jacobs, Elmer H. Brown and Conway Washburn reprised their roles from the Broadway production.   on TCM.com 
*Vincent Sherman made his film debut in Counsellor at Law.  He had previously appeared in a Chicago production of the play.

==Production==
  (theatre producer), Elmer Rice (playwright) and Carl Laemmle Jr. (Universal producer) sign a contract for the film version of Counsellor at Law]]
 Broadway and in Chicago, Los Angeles, and San Francisco. Producer Carl Laemmle Jr. paid $150,000 for the screen rights, an unusually high price tag during the Great Depression, and to ensure the films success he hired Elmer Rice to adapt his own play. 

In early August 1933, Wyler met Rice in Mexico City, where he was vacationing with his family, for preliminary discussions about the script. Rice was loath to mix business with pleasure and assured the director he would begin working as soon as his holiday ended. On August 22, he shipped a first draft from his New York office to Universal Pictures. Wyler approved of the screenplay, and principal photography was slated to begin on September 8. 

Laemmle wanted to cast Paul Muni as George Simon, the role he had created on stage, but the actor declined because he feared being typecast as Jewish. Edward G. Robinson, Joseph Schildkraut, and William Powell were considered before Laemmle decide to cast against type and offer the role to John Barrymore in order to capitalize on his box office appeal. Both Wyler and Rice wanted to cast performers from the various stage productions, and although several screen tests were made, most of the roles were filled by studio contract players. Vincent Sherman, who had been in the Chicago production, was signed to reprise his small role of Harry Becker, a young radical with Communist leanings; he later became a prolific film and television director.  Another cast member, Richard Quine, then 13, similarly went on to a career as a director, writer and producer. 
 The Little Foxes in later years. 

Barrymore had signed for $25,000 per week,  and Wyler was ordered to film all his scenes as quickly as possible. What should have taken two weeks ultimately took three-and-a-half because the actor could not remember his lines. After taking twenty-seven takes to complete one brief scene, Wyler decided to resort to cue cards strategically placed around the set. Also adding to delays was Barrymores heavy drinking, which frequently gave his face a puffy appearance that required the makeup crew to tape his jowls. Between dealing with Barrymore and trying to comply with Laemmles demands to complete the film on schedule and within the allotted budget, Wyler was tense and irritable and tended to take out his frustrations on the supporting cast. 

Three months after filming began, the film opened to critical and commercial success at Radio City Music Hall on December 11, 1933. Herman, p. 119 

==Critical reception==
Mordaunt Hall of the New York Times said the film "moves along with lusty energy, the scenes being so complete that none of them seems a fraction of a minute too long. Parts of the stage work have perforce been omitted, but where this occurs Mr. Rice and the director, William Wyler, leave nothing in doubt." He praised Barrymore for giving his role "the vigor, imagination and authority one might expect" and added, "The characterization is believable and thoroughly sympathetic." 

The Hollywood Reporter said the film proved "the value of having a playwright adapt his own brainchild to the screen." It also praised Wyler for giving it "a far better tempo than the play possessed" and added, "He milks each situation and lets it go without stressing . . . many scenes which could easily have ensnared a less capable director." 

Years later, in a critique of Barrymores career, Pauline Kael described his portrayal of George Simon as "one of the few screen roles that reveal his measure as an actor." 

==References==
Notes
 

Bibliography
*Herman, Jan, A Talent for Trouble: The Life of Hollywoods Most Acclaimed Director. New York: G.P. Putnams Sons 1995. ISBN 0-399-14012-3
*Kael, Pauline, 5001 Nights at the Movies. New York: Holt, Rinehart and Winston 1984. ISBN 0-09-933550-6

==External links==
* 
* 
* 
*  at the Internet Broadway Database
* 
* 

 

 

 
 
 
 
 
 
 
 
 