Retreat, Hell!
{{Infobox film
| name           = Retreat, Hell!
| image          = Retreat, Hell!.jpg
| image_size     =
| caption        = Original film poster Joseph H. Lewis
| producer       = Milton Sperling
| writer         = Milton Sperling, Ted Sherdeman
| studio         = United States Pictures Richard Carlson Anita Louise
| music          =
| cinematography =
| editing        =
| distributor    = Warner Brothers
| released       =  
| runtime        = 94 min.
| country        = United States
| language       = English
| budget         =
| gross          = $2 million (US rentals) 
}} 1st Marine Joseph H. Richard Carlson as a veteran captain and communications specialist of World War II called up from the Marine Corps Reserves, Russ Tamblyn as a seventeen-year-old private who hides his true age to serve with the unit overseas and outdo his older brother, also a Marine, and Nedrick Young (credited as Ned Young). Also appearing in the film is Peter Julien Ortiz, a highly decorated Marine who served in the Office of Strategic Services (OSS) and appeared in various films after retiring from the military.

==Plot==
A Marine battalion is assembled from various sources and sent to Korea. The film depicts the formation and training of the battalion, the amphibious landing at the Battle of Inchon, the advance through North Korea, and the Winter Chinese Communist Offensive sends the Marines into a fighting withdrawal to the staging area at Hŭngnam Harbor "...with rifles, grenades, bayonets, our bare fists if we have to" (quoting the battalion commander).

The private (Tamblyn) goes looking for his older brother and is shown a row of dead Marines.  One of them, he discovers, is his brother.  The battalion commander (Lovejoy) is supposed to send him home per a regulation covering the last survivor of a family.  The Chinese Communist offensive puts this on hold for the moment, and he is nearly killed during the withdrawal in a snowstorm until saved by a joint American-British force.

==Production==
With the U.S. Marine Corpss fight for life at the Battle of Chosin Reservoir against the Chinese Communist Forces offensive in the winter of 1950 being anxiously followed in the news of the day, Warner Brothers submitted a proposal on 7 December 1950 to the Marines to make a film about the events.  The Marines approved the request, with former Marine Milton Sperling producing and co-writing the film for his United States Pictures division of Warners.  The Marine Corps worked closely with Sperling on the script giving it their approval in August 1951 and agreeing to six weeks of filming at Camp Pendleton where the film crew bulldozed a road and sprinkled the area with gypsum to simulate snow. The Marines also created accurate Korean villages for the film. Commandant of the Marine Corps Lemuel Shepherd estimated the value of the Marine cooperation at US$1,000,000.   The Hollywood Production Code Office originally refused to approve the title because of its ban on the word "hell", but changed their mind after requests from the Marine Corps.

The film also features the efforts of the U.S. Navy and Royal Marines.

Director Joseph H. Lewis had been hired by Warner Brothers after the success of his film Gun Crazy but had not been given any assignment until this film. During World War II, Lewis directed US Army training films about the M1 Garand rifle that were shown well into the 1960s. 

==Reception==
Variety (magazine)|Variety called it a "top-notch war drama" for the way it balanced tense action with a more human face of the war, anticipating film-making trends that would become more common twenty years later.

An oral history interview with Donald H. Eaton, a Korean War black veteran, includes a story where he says how he and several friends watched the film when it came out, and half of his friends ended up signing up for the Marine Corps. The Korean War (1950–1953) was the first war where United States troops were desegregated.

==Trivia==
"Retreat, Hell" is the motto of the  .
  
==References==
 
*Suid, Lawrence H. Guts and Glory: The Making of the American Military Image in Film, University Press of Kentucky, 2002.
*Variety editors. Variety Movie Guide, Perigee Books edition, 2000.

==External links==
* 

 

 
 
 
 
 
 
 
  
 