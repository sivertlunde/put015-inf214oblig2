The Odyssey (miniseries)
{{Infobox television film
| name        = The Odyssey
| image       = Odyssey_NBC.jpg
| caption     = Promotional poster
| director    = Andrei Konchalovsky
| based on    = Odyssey by Homer
| producer    = Nicholas Meyer Francis Ford Coppola Dyson Lovell
| writer      = Andrei Konchalovsky
| starring    = Armand Assante Greta Scacchi Isabella Rossellini Vanessa L. Williams Bernadette Peters Alan Stenson
| music       = Eduard Artemyev
| cinematography = Sergei Kozlov
| editing     = Michael Ellis Hallmark American Zoetrope
| network     = NBC
| released    = May 18, 1997 - May 19, 1997
| runtime     = 176 minutes  (2 parts) 
| country     = United States
| language    = English
}}
 adventure miniseries|television miniseries based on the ancient Greek epic poem by Homer, The Odyssey. Directed by Andrei Konchalovsky, the miniseries aired in two-parts beginning on May 18, 1997 on NBC. The series won the award for "Outstanding Directing for a Miniseries or a Special". It was filmed in Malta, Turkey, parts of England, and many other places around the Mediterranean, where the story takes place. The international all-star cast includes Armand Assante, Greta Scacchi, Irene Pappas, Isabella Rossellini, Bernadette Peters, Christopher Lee, and Vanessa L. Williams.

==Plot==

===Part 1=== Greek kingdom giant horse gods that he did it himself, which angers Poseidon (voiced by Miles Anderson) so much that he promises to make Odysseus journey home to Penelope nearly impossible, mentioning that it was he who sent the sea monster to devour Laocoön.

Odysseus and his men initially stop on an island dominated by one-eyed giants, the Cyclops|Cyclopes. A Cyclops named Polyphemus (Reid Asato) traps them in his cave intending to eat them, but Odysseus gets him drunk on wine, causing him to pass out. Then, he sharpens a tree branch into a stake and blinds the Cyclops, allowing them to escape by hiding under sheep skins when he removes the heavy stone door. Polyphemus screams for help, but Odysseus had tricked him into stating that his name was "Nobody", so the Cyclops is shouting that nobody has tricked him, arousing no suspicion. Odysseus and his men escape, but Odysseus taunts the Cyclops who asks his father Poseidon to avenge him. This makes Odysseus journey home harder.

Odysseus travels to an island where Aeolus (Michael J. Pollard) provides him with a bag of wind to help him home, instructing him to open it when he gets close to Ithaca. One of his men opens it prematurely blowing them off course. Next, they stop at the island of Circe (Bernadette Peters), a beautiful witch, who turns his men into animals and blackmails him into sleeping with her. Odysseus is told of Circes magic by Hermes (Freddy Douglas), who helps him avoid being transformed as well. Circe tells him to go to the Underworld next, and only then does Odysseus realize that he has actually been tricked by Circe, who put a spell on him so he stayed on the island for five years instead of five days. Odysseus digs his ship out of the sand and tide and sails to the Underworld.

===Part 2===
Arriving at the Underworld, Tiresias (Christopher Lee) torments Odysseus, recognizing his courage and wit, but criticizes his ego and foolishness. After Odysseus sacrifices a goat into the River Styx, Tiresias tells Odysseus on how to get home with one of the obstacles being an isle where Scylla and Charybdis lives. As he is running in terror from the underworld, he meets his mother Anticlea (Irene Papas), who committed suicide due to the pain of losing her son. She informs him that there are multiple suitors planning to marry Penelope for her money and power.
 Calypso (Vanessa L. Williams) lives and becomes her prisoner. Meanwhile, Odysseus now 15-year-old son Telemachus (Alan Stenson) tries to find his father and is told by Athena (Isabella Rossellini) to seek out one of his fellow comrades that fought with him. When Telemachus finds one of Odysseus comrades, he learns that they dont know what happened to Odysseus.
 Phaeacians girls. With help from Phaeacian King Alcinous (Jeroen Krabbé), they help Odysseus back to Ithaca. They deliver him at night while he is fast asleep, to a hidden harbor on Ithaca. Upon awakening the next morning, he finds himself on Ithaca where is reunited with Telemachus. Using a peasant disguise provided by Athena, Odysseus meets up with Penelope where she decides to hold a contest to find the person who can string Odysseus bow. After Odysseus wins the contest, Athena sheds his disguise and Odysseus is assisted by Telemachus into slaying the suitors. Once the suitors are dead, Odysseus reunites with Penelope.

==Cast==
 
* Armand Assante as Odysseus
* Greta Scacchi as Penelope
* Isabella Rossellini as Athena
* Bernadette Peters as Circe
* Eric Roberts as Eurymachus
* Irene Papas as Anticlea
* Jeroen Krabbé as Alcinous
* Geraldine Chaplin as Eurycleia
* Christopher Lee as Tiresias Calypso
* Nicholas Clay as Menelaus
* Adoni Anastassopoulos as Perimides
* Paloma Baeza as Melanthe
* Ron Cook as Eurybates
* Reid Asato as Polyphemus David Barclay as Polyphemus (face operator) Mark Hill as Orsilicus
* Pat Kelman as Elatus Antinous
* Arete (Queen Alcinous) Polites
* Katie Carr as Nausicaa
* Marius Combo as Agelaus
* Alan Cox as Elpenor
* Will Houston as Anticlus
* Oded Levy as Leocrites
* Peter Page as Philotus
* Michael J. Pollard as Aeolus
* Alan Stenson as Telemachus Stuart Thomspon as Antiphus
* Tony Vogel as Eumaeus
* Heathcote Williams as Laocoön Eurylochus
* Richard Trewett as Achilles
* Yorgo Voyagis as King Agamemnon
* Peter Woodthorpe as Mentor
* Derek Lea as Hector
* Freddy Douglas as Hermes
* Miles Anderson as Poseidon (voice)
* Alan Smithie as King Priam
* Vernon Dobtcheff as Aegyptus
* Josh Maguire (actor) as Young Telemachus
* Kresimir Novakovic as Phoenecian Sailor (uncredited) Greek Juggler (uncredited)
* Shawna Wenger as Servant (uncredited)
 

==Filming==

===Special effects===
The creature effects for this miniseries were provided by Jim Hensons Creature Shop where they used a talking animatronic pig roasting on a spit, a CGI for Scylla, a rod puppet sea slug-like sea monster that devours Laocoön, and the full-bodied version of Polyphemus.
 Jason and the Argonauts miniseries.

===Rating===
MPAA rated this film PG-13 for violent sequences and some sensuality. 

==See also==
* Greek mythology in popular culture
* List of historical drama films

 

==References==
 

==External links==
*  
*  
*  
*   at Muppet Wiki

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 