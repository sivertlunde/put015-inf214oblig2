A Few Best Men
 
 
{{Infobox film
| name = A Few Best Men
| image = AFewBestMen2011Poster.jpg
| image_size = 215px
| alt = 
| caption = Australian release poster
| director = Stephan Elliott
| producer = Antonia Barnard Gary Hamilton Laurence Malkin Share Stallings
| writer = Dean Craig
| starring = Xavier Samuel Laura Brent Olivia Newton-John Guy Gross
| cinematography = Stephen F. Windon
| editing = Sue Blainey
| studio = Quickfire Films Icon Film Distribution   Buena Vista International  
| released =  
| runtime = 96 minutes  
| country = Australia
| language = English
| budget = Australian dollar|A$14 million
| gross = $13,944,023 
}} Blue Mountains best men for his wedding.

==Plot==
When David Locking proposes to his girlfriend Mia on the same day they meet in Australia, he rounds up his three best friends to come to his wedding as best men. Soon, havoc ensues when the trio accidentally steal drugs, are chased by a mobster, and get the father-in-laws sheep stoned.

==Cast==
* Xavier Samuel as David Locking
* Laura Brent as Mia Ramme
* Olivia Newton-John as Barbara Ramme
* Jonathan Biggins as Jim Ramme
* Kris Marshall as Tom
* Kevin Bishop as Graham
* Tim Draxl as Luke
* Rebel Wilson as Daphne Ramme
* Steve Le Marquand as Ray
* Elizabeth Debicki as Maureen

==Release==
A Few Best Men premiered at the Mill Valley Film Festival in San Rafael, California on 14 October 2011.  The film was released in Australia on 26 January 2012 and in Germany on 14 June 2012.

==Soundtrack==
 
 .

# "Weightless" – Olivia Newton-John
# "The Rain, the Park and Other Things" (Lo Five Remix) – Olivia Newton-John
# "The Nips Are Getting Bigger" – The Wedding Band
# "Daydream Believer" (Chew Fu Fix) – Olivia Newton-John
# "The Pushbike Song" (Pablo Calamari Remix) – Olivia Newton John
# "Wankered" – The Wedding Band
# "Afternoon Delight" – The Wedding Band
# "A Beautiful Morning" – The Wedding Band
# "Brand New Key" (Archie Remix) – Olivia Newton-John
# "The Love Boat" (Roulette Remix) – Olivia Newton-John Live It Up" – The Wedding Band
# "Sugar, Sugar" (Chew Fu Fix) – Olivia Newton-John
# "Living in the 70s" – The Wedding Band
# "Devil Gate Drive" (Chew Fu & PVH Night Fever Remix) – Olivia Newton-John
# "Georgy Girl" – Olivia Newton-John (Roulette Remix)
# "I Think I Love You" (Chew Fu & PVH Love Hurts Remix) – Olivia Newton-John
# "Two Out of Three Aint Bad" (Lo Five remix) – Olivia Newton-John
# "Mickey (Toni Basil song)|Mickey" (Chew Fu Fix) – Olivia Newton-John
# "Weightless" (Punk Ninja Remix) – Olivia Newton-John

==Reception==
A Few Best Men was met with negative reviews, earning an approval rating of 19% on Rotten Tomatoes.
 SBS noted that the film was as "Funny as a funeral", awarding the film one star out of five, commenting that "Like a bad wedding reception, A Few Best Men is overlong by at least an hour, and the flimsy plot groans under its own weight." 
 AACTA Award Best Original Music Score.

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 