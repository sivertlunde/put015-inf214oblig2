Dead Ringers (film)
 
 
{{Infobox film
| name           = Dead Ringers
| image          = Dead ringers poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = David Cronenberg
| producer       = Marc Boyman David Cronenberg
| writer         = David Cronenberg Norman Snider
| based on       =  
| starring       = Jeremy Irons Geneviève Bujold
| music          = Howard Shore
| cinematography = Peter Suschitzky
| editing        = Ronald Sanders
| studio         = Morgan Creek Productions Telefilm Canada Mantle Clinic II
| distributor    = 20th Century Fox
| released       =  
| runtime        = 115 minutes  
| country        = Canada United States
| language       = English
| budget         = $13 million 
| gross          = $8 million 
}}
Dead Ringers is a 1988 Canadian-American psychological thriller film starring Jeremy Irons in a dual role as identical twin gynecologists. David Cronenberg directed and co-wrote the screenplay with Norman Snider; their script was based on the novel Twins by Bari Wood and Jack Geasland. The film is very loosely based on the lives of Stewart and Cyril Marcus.   

==Plot== gynecologists who jointly operate a highly successful clinical practice in Toronto that specializes in the treatment of female fertility problems. Elliot, the more confident and cynical of the two, seduces women who come to the Mantle Clinic. When he tires of them, the women are passed on to the shy and passive Beverly, while the women remain unaware of the substitution.
 abuse of prescription drugs, which he abets through his doctors authority. When Claire learns that Elliot has been taking sexual advantage of her by impersonating Beverly, she is angry and confronts them both in a bar, but later decides to continue a relationship with Beverly exclusively.
 delusions about metallurgical artist Anders Wolleck and commissions a set of bizarre "gynecological instruments" for operating on these mutant women. After Beverly assaults a patient during surgery with one of Wollecks tools, both brothers are immediately suspended from practice and put on administrative leave by the hospital board.

With their medical career now ruined, Elliot locks Beverly into the clinic and tries to clean him up, taking pills himself in order to "synchronize" their bloodstreams. When Claire returns, Beverly leaves the clinic to be with her. After recovering his sobriety, he is concerned about his brother, and goes back to the clinic. There he finds the clinic in a shambles and Elliot despondent and intoxicated. Their positions are reversed as Beverly cares for Elliot. Drugged and despairing, they celebrate their mock birthday and Elliot volunteers to be killed, "to separate the Siamese twins". Beverly disembowels Elliot on an examination couch with the same claw-like instrument of Wollecks that he had used to assault his patient in the operating room.

Shortly after, Beverly pulls himself together, leaves the clinic and calls Claire on a payphone. When she asks, "Who is this?", Beverly leaves the payphone, walks back into the clinic and dies in Elliots dead arms.

==Cast==
 
* Jeremy Irons as Beverly Mantle / Elliot Mantle
** Jonathan and Nicholas Haley as young Beverly / Elliot
* Geneviève Bujold as Claire Niveau
* Heidi von Palleske as Cary
* Barbara Gordon as Danuta
* Shirley Douglas as Laura
* Stephen Lack as Anders Wolleck
* Nick Nichols as Leo
* Lynne Cormack as Arlene
* Damir Andrei as Birchall
* Miriam Newhouse as Mrs. Bookman
* Jill Hennessy as Mimsy
* Jacqueline Hennessy as Coral
** (Jill and Jacqueline Hennessy, themselves identical twins, made their film debut as twin escorts)
 

==Production==
In his  .

Initially, Irons had two separate dressing rooms and two separate wardrobes which he would use depending on which character he was playing at the time. Soon, he realized that “the whole point of the story is you should sometimes be confused as to which is which,” after which he moved to a single dressing room and mixed the wardrobes together, and found an “internal way” to play each character differently, using the Alexander technique to give them “different energy points,” which gave them slightly different appearances. 

==Reception==
Dead Ringers won the Genie Award for Best Canadian Film of 1988.
 Grand Prix of the Belgian Syndicate of Cinema Critics.
 best Canadian film ever made.  It was named one of "The Top 10 True-Story Horror Movies of All-time!" by Bloody Disgusting. 

Although Dead Ringers closely follows the case of Stewart and Cyril Marcus, director Peter Greenaway notes that Cronenberg queried him about his film A Zed & Two Noughts for two hours before going on to make Dead Ringers eight months later.  The film has a positive rating of 83% on Rotten Tomatoes.

==See also==
* Look-alike#Film|Look-alike

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 