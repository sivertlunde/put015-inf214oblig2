Vaidehi Kalyanam
{{Infobox film
| name           = Vaidehi Kalyanam
| image          = 
| image_size     =
| caption        = 
| director       = Manivasagam
| producer       = Rajeswari Manivasagam P. S. Mani
| writer         = Manivasagam
| starring       =   Deva
| cinematography = K. B. Ahamad
| editing        = L. Kesavan
| distributor    =
| studio         = Raja Pushpa Pictures
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil drama Deva and was released on 11 July 1991.   

==Plot==

Rajamanickam ( ). In the meantime, the doctor Krishnamoorthy (Ilavarasan) comes to Rajamanickams village and Rajamanickam loans him a clinic. The school teacher Vasanthi (Rekha (South Indian actress)|Rekha) has a boy but has no husband, they live near Kalyanams house. Vasanthi advices Kalyanam to not elope with Vaidehi but to marry her with Rajamanickams blessing.

In the past, Rajamanickam was the manager of a textile company. Rajamanickam and Vasanthi had an affair. Being already married, Rajamanickam refused to marry Vasanthi. Rajamanickam asked her to become his mistress but she challenged him to become his wife.

One day, Vasanthi has an accident and Rajamanickam admits her at the hospital. Rajamanickam persits in accepting her as his mistress. Later, Vasanthi gives tuition to Vaidehi. Rajamanickam decides to arrange the wedding between Krishnamoorthy and Vaidehi. Krishnamoorthy is in fact a womanizer. What transpires later forms the crux of the story.

==Cast==
 
*R. Sarathkumar as Rajamanickam Rekha as Vasanthi
*Ramarjun as Kalyanam (Kalyanasundaram)
*Uthra as Vaidehi
*Delhi Ganesh as Chinnasamy
*Goundamani as Pachamuthu Senthil as Thangamuthu
*Ilavarasan as Krishnamoorthy
*Kavitha
*Kalaiselvan
*Pathma
*Ganga
*Chitraguptan as Thangamuthus son
*Master Ramesh as Prabhu
*Pasi Narayanan as Kandasamy
*Karuppu Subbaiah as Pazhanisamy
*Thidir Kannaiah as Boopathi
*Tirupur Ramasamy as Ramasamy
*Krishnamoorthy
 

==Soundtrack==

{{Infobox Album |  
| Name        = Vaidehi Kalyanam
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack |
| Length      = 22:24
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1991, features 5 tracks with lyrics written by Kalidasan.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Jootudhan Jootudhan || S. P. Balasubrahmanyam, K. S. Chithra || 4:31
|- 2 || Mathaalam Thatungadi || Malaysia Vasudevan, K. S. Chithra || 4:10
|- 3 || Kalyaanam Kalyaanam || Sunanda || 4:18
|- 4 || Thenthoovum Vasandham || Mano (singer)|Mano, K. S. Chithra || 4:45
|- 5 || Panjali Kiliye Kiliye || Gangai Amaran || 4:40
|}

==References==
 

 
 
 
 
 