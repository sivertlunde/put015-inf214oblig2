Japan (film)
{{Infobox film
| name           = Japan
| image          = Japan FilmPoster.jpeg
| caption        = 
| director       = Fabien Pruvot
| producer       = Ron Althoff Petr Buk Bianca Chiminello John Corser Robert Flaxman Peggy Hovsepian Fabien Pruvot Bella Vendramini
| writer         = Fabien Pruvot
| starring       = Shane Brolly Peter Fonda Bianca Chiminello Tania Raymonde Treva Etienne
| music          = Bernie Torelli
| cinematography = Ryan Nguyen
| editing        = Ken Cravens Jeff Turkali
| distributor    = Tomas Pictures
| released       = 2008
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}}
Japan  is a 2008 American independent film written and directed by Fabien Pruvot.

==Plot==
The film follows a contract killer who goes by the code name Japan. He meets a man named Alfred at a hotel, who was recently evicted from his home. The two get along and Japan befriends Alfred. Their friendship leads the film into a twist and turn ending.

==Production==
The film was shot in Los Angeles, California and Phoenix, Arizona.

== External links ==
*  

 
 
 


 