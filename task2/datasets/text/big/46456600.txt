You Have to be Beautiful
{{Infobox film
| name = You Have to be Beautiful
| image =
| image_size =
| caption =
| director = Ákos Ráthonyi
| producer =  Walter Koppel   Gyula Trebitsch
| writer =  Gustav Kampendonk   Kurt Schwabach   Fritz von Woedtke
| narrator =
| starring = Sonja Ziemann   Willy Fritsch   Anny Ondra
| music = Michael Jary   
| cinematography = Willy Winterstein     
| editing =  Klaus Dudenhöfer    
| studio = Real-Film 
| distributor = 
| released = 1 February 1951 
| runtime = 96 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
You Have to be Beautiful (German:Schön muß man sein) is a 1951 German musical comedy film directed by Ákos Ráthonyi and starring Sonja Ziemann, Willy Fritsch and Anny Ondra. The films sets were designed by art director Mathias Matthiess. It was Ondras final film apart from a brief cameo role in the The Affairs of Julie. 

==Cast==
*  Sonja Ziemann as Maria Schippe  
* Willy Fritsch as Jupp Holunder  
* Anny Ondra as Rode de Lila  
* Hardy Krüger as Juppi Holunder Jr.  
* Rudolf Platte as Enrico Zwickel   Hans Richter as Walter Schippe  
* Ursula Herking as Wirtschafterin  
* Marina Ried as Tilly 
* Willy Maertens as Arzt  
* Carl Voscherau as Schröder - Portier  
* Joseph Offenbach as Kleiner Mann  
* Bruno Fritz as Regisseur Treff  
* Kurt Meister as Inspizient  
* Michael Jary as Kapellmeister  
* Jonny Crusoe as Jim  
* Horst von Otto as Barchef  
* Wolfgang Neuss as Moritatensänger  
* Margret Neuhaus as Moritatensänger 
* Die Hiller Girls as Themselves  
* Ilka Hugo
* Das Original Hohner-Orchester as Orchestra
* Will Raabe    
* Albert Vossen 

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 
 
 
 
 
 

 