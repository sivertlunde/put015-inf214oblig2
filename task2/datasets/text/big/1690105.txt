Enlarged to Show Detail 2
{{Infobox film
| name           = Enlarged to Show Detail 2
| image          = Etsd2.jpeg
| caption        = Cover of DVD
| director       = Carter B. Smith John OGrady Chuck Ryant
| writer         =
| narrator       = Tim Mahoney Doug Martinez Chad Sexton Aaron Wills
| music          = Nick Hexum Tim Mahoney Doug Martinez Chad Sexton Aaron Wills Shaquille ONeal
| cinematography = Jo Molitoris
| editing        = BMG Music
| released       =  
| runtime        = 120 min.
| country        = United States
| language       = English
| budget         =
}} 311 in 2001. This is their second video documenting their experiences as a band promoting what they consider to be peace and a positive outlook on life.  The video carries a Parental Advisory due to explicit lyrics and large display of adult content.

Enlarged to Show Detail 2 was certified as a Gold selling video by the RIAA.

* Format: Explicit Lyrics
* Rated: NR
* Studio: Bmg/Volcano/Capricorn
* DVD Release Date: December 11, 2001
* DVD Features:
* Number of discs: 2

==Track listings==

===Disc 1 (DVD)===
#Intro 	2:25
#3-11 Day 	6:00
#The Hive 	1:56
#Tim Mahoney 	2:03
#311 in Japan 	3:23
#Nick Hexum 	2:44
#311 Milk Challenge 	4:05
#Old School 	1:41
#SA Martinez 	6:58
#Tolerance 	7:24
#Halloween 	1:38
#Chad Sexton 	2:40
#Weenie Roast 	3:21
#P-Nut 	3:39
#Small Venues 	7:28
#The Fans 	1:50
#Management & Crew 	7:39

===Disc 2 (Bonus CD EP)===
 
#Dancehall
#Bomb the Town
#Will The World (Instrumental)
#We Do It Like This
#Dreamland (Instrumental)
#Ill Be Here Awhile (Acoustic Version)

 

==External links==
* 
*  

 

 
 
 
 
 
 
 


 