Oil's Well (film)
{{Infobox Hollywood cartoon|
| cartoon_name = Oils Well
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz Bill Nolan Tom Palmer
| voice_actor = 
| musician = Bert Fiske
| producer = Walter Lantz
| distributor = Universal Pictures
| release_date = September 16, 1929
| color_process = Black and white
| runtime = 6:43 English
| preceded_by = Race Riot
| followed_by = Permanent Wave
}}

Oils Well is a short animated film starring Oswald the Lucky Rabbit, and produced by Walter Lantz Productions. It is the 2nd Lantz Oswald film and the 54th in the entire series.

==Plot==
Oswald and a lady cat are canoeing in a river. While the rabbit rows, the cat plays a guitar and dances. One day, their ride is roughen by rocks protruding from the water, resulting Oswald to lose his oar, and the cat to lose her guitar. They are, however, able to make it safely to shore. Oswald then convertstheir boat into a car where they begin moving by road.

After such a trip, Oswald takes the cat home which is a tall condo building. Before the cat goes inside, Oswald asks her if they should be married to each other as well as having a dozen children. The cat considers this a possibility and agrees with Oswald.

It then turns out that the cat also has a romantic relationship with a bloodhound who comes out of the condo. The bloodhound angrily kicks Oswald away and takes the cat inside, much to the rabbits surprise.
 getaway in the car.

Hating to lose cat, the bloodhound ties a rope around his waist and suspense himself under a passing eagle. He carries a rifle and goes forth to chase and shoot Oswald.

Oswald and the cat are still in their the car with the carriage still elevated several feet above the ground. The bloodhound opens fire, and Oswald tries to cover himself using the trombone. One of the bloodhounds bullets, however, damages a vital part in his vehicle, causing it to come down and halt.
 oil started shooting upward from where Oswald was digging. In this, Oswald and the bloodhound gave up their rivalry and became good friends. They and the cat became rich and happy.

==See also==
*Oswald the Lucky Rabbit filmography

==External links==
* 
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 