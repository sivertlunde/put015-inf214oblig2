Hell to Pay (2005 film)
{{Infobox film
| name           = Hell to Pay
| image          = Hell-to-pay-roberto gomez martin.jpg
| caption        = Original video release
| director       = Roberto Gomez Martin
| writer         =  Billy Murray 
| music          = Ryan S. Jones
| cinematography = 
| editing        = Brian Hovmand
| distributor    = 
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| gross          = 
}}
Hell to Pay is a 2005 British film by Roberto Gomez Martin, his directorial debut, about a London East Ends gangster life based on the semi-autobiographical story of Dave Courtney, who plays character of Dave Malone in the film.

==Plot== Billy Murray) to murder a rival crime boss. Dave avoids the scheme and subsequently the murder charge made against him, whilst planning to eliminate the competition and take over the lucrative London crime trade. 
 Ronnie Kray as it was given to Dave Courtney as a present just before he died in prison.

==Cast==
*Terry Stone ...  Johnny Murphy (credited as Terry Turbo)
*Dave Courtney ...  Dave Malone Billy Murray ...  Larry Malone 
*Andy Beckwith ...  Detective Inspector Beek
*Francine Lewis ...  Gangsters wife 
*JC Mac ...  Mike-stripper 
*Chico Slimani ...  Stripper
*Garry Bushell ...  One of Larry Malones goons 
*Ian Freeman ...  Cellmate
*Martin Hancock ...  Martin 
*Helen Keating ...  Helen 
*Dave Legeno ...  Big Vic 
*Trevor Mailey ...  Hood 
*Adam Saint ...  Adam John Altman ...  Policeman
*Nicholas Bateman ...  Police officer 
*Pete Conway ...  Policeman  Joanne Guest ...  Policewoman
*Charlie Breaker ... Sir Charlie Malone (Uncle)

==References==
 
 
==External links==
*  

 
 
 
 