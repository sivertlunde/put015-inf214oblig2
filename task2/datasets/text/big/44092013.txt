Harshabashpam
{{Infobox film 
| name           = Harshabashpam
| image          =
| caption        =
| director       = P Gopikumar
| producer       = KH Khan Sahib
| writer         = Kanam EJ
| screenplay     = Kanam EJ
| starring       = K. J. Yesudas Adoor Bhasi Bahadoor Janardanan
| music          = M. K. Arjunan
| cinematography = Madhu Ambatt
| editing        = K Sankunni
| studio         = Kanthi Harsha
| distributor    = Kanthi Harsha
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, directed by P Gopikumar and produced by KH Khan Sahib. The film stars K. J. Yesudas, Adoor Bhasi, Bahadoor and Janardanan in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*K. J. Yesudas
*Adoor Bhasi
*Bahadoor
*Janardanan
*K. P. Ummer
*MG Soman
*Mallika Sukumaran
*Vidhubala

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by KH Khan Sahib and Kanam EJ. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aayiram Kaathamakaleyaanenkilum || K. J. Yesudas || KH Khan Sahib || 
|-
| 2 || Ekaadashi Dinamunarnnu || Jency || Kanam EJ || 
|-
| 3 || Thaalappoliyode || K. J. Yesudas || KH Khan Sahib || 
|-
| 4 || Vellappudavayuduthu  || K. J. Yesudas || Kanam EJ || 
|}

==References==
 

==External links==
*  

 
 
 

 