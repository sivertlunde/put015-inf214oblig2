The Lady in Number 6
{{Infobox film
| name           = The Lady in Number 6: Music Saved My Life
| image          = The Lady in Number 6.jpg
| caption        = Film poster Malcolm Clarke Christopher Branch
| writer         = Malcolm Clarke 
| starring       = Alice Herz-Sommer
| music          = Luc St. Pierre
| cinematography =  Kieran Crilly
| editing        = Carl Freed
| studio         = 
| distributor    = 
| released       =  
| runtime        = 39 minutes
| country        = Canada United States United Kingdom
| language       = English
}} Academy Award-winning Malcolm Clarke.

The Lady In Number 6: Music Saved My Life tells the story of Alice Herz-Sommer, a pianist who was the worlds oldest Holocaust survivor. She discussed the importance of music, laughter, and how to have an optimistic outlook on life. Herz (1903–2014) died at age 110, one week before the 86th Academy Awards.

==Awards==
{| class="wikitable"
! colspan="5" style="background: LightSteelBlue;" | Awards
|-
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|-
| Academy Awards   March 2, 2014 Best Documentary – Short Subject
| Malcolm Clarke Nicholas Reed
|  
|-
|}

==References==
 

== Further reading ==
*    
*   The inspiration for the film.
*    

==External links==
*  
*  
* http://insidemovies.ew.com/2014/02/28/oscar-documentary-shorts-the-lady-in-number-6/
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 