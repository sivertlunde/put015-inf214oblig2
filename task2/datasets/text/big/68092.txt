Chinatown (1974 film)
 
{{Infobox film
| name               = Chinatown
| image              = Chinatownposter1.jpg
| border             = yes
| alt                =
| caption            = Theatrical poster by Jim Pearsall
| director           = Roman Polanski Robert Evans
| writer             = Robert Towne
| starring           = Jack Nicholson Faye Dunaway John Huston
| music              = Jerry Goldsmith
| cinematography     = John A. Alonzo
| editing            = Sam OSteen
| distributor        = Paramount Pictures
| released           =  
| runtime            = 131 minutes  
| country            = United States
| language           = English
| budget             = 
| gross              = $29,200,000 
}} Robert Evans production, a Paramount Pictures release, was the directors last film in the United States and features many elements of film noir, particularly a multi-layered story that is part mystery and part psychological drama.
 the best Best Original Best Drama, Best Director, Best Actor Best Screenplay. mystery films in 2008.

A sequel, The Two Jakes, was released in 1990, again starring Nicholson, who also directed, with Robert Towne returning to write the screenplay. The film failed to generate the acclaim of its predecessor.

==Plot==
A woman identifying herself as Evelyn Mulwray (Ladd)  hires private investigator J. J. "Jake" Gittes (Nicholson) to carry out surveillance on her husband, Hollis I. Mulwray (Zwerling), chief engineer for the Los Angeles Department of Water and Power. Gittes tails him, hears him publicly oppose the creation of a new reservoir, and shoots photographs of him with a young woman (Palmer), which are published on the front page of the following days paper. Upon his return to his office, Gittes is confronted by a beautiful woman who, after establishing that the two of them have never met, irately informs him she is the real Evelyn Mulwray (Dunaway) and that he can expect a lawsuit.

Realizing he was set up, Gittes figures whoever did it wants to get Mr. Mulwray, but, before he can question the husband, Lieutenant Lou Escobar (Lopez) fishes Mulwray, drowned, from a freshwater reservoir. Suspicious of murder, Gittes investigates and notices that, although huge quantities of water are released from the reservoir every night, the land is almost dry. Gittes is confronted by Water Department Security Chief Claude Mulvihill (Jenson) and a henchman (Polanski), who slashes the Jakes nose. Back at his office, he receives a call from Ida Sessions, an actress whom he recognizes as the bogus Mrs. Mulwray. She is afraid to identify her employer but provides a clue: the name of one of "those people" is in that days obituaries.

Gittes learns that Mrs. Mulwrays late husband was once the business partner of her father, Noah Cross (Huston), whom he arranges to meet for lunch at his personal club. Cross offers to double Gittess fee to search for Mulwrays missing mistress, plus a bonus if he succeeds. Gittes visits the hall of records, where he discovers that a large amount of acreage in the "northwest valley" has changed ownership. While investigating there he is attacked by angry landowners; they believe he is an agent of the water department, attempting to force them out by sabotaging their water supply.

Gittess review of the obituaries uncovers that a former resident of the Mar Vista Inn retirement home is one of the valleys new landowners, who purchased the property a week after his death. He infers that Mulwray was murdered when he learned that the new reservoir would be used to irrigate the newly-purchased properties. Evelyn and Gittes bluff their way into Mar Vista and confirm that the real estate deals are surreptitiously completed in the names of some of its residents. After fleeing from Mulvihill and his thugs, they hide at Evelyns house, where they nurse each others wounds and end up in bed together.

Early in the morning, Evelyn has to leave suddenly; she warns Gittes that her father is dangerous and crazy. Gittes follows her to a house and spies on her from the windows; she is with Mulwrays mistress. He confronts Evelyn, who confesses that the woman is her sister.

The next day, an anonymous call draws Gittes to Ida Sessionss apartment; he finds her murdered, with Escobar waiting for his arrival. Escobar pressures him because the coroners report found salt water in Mulwrays lungs, indicating that the body was moved after death. Escobar suspects Evelyn of the murder and insists Gittes produce her quickly or hell face charges of his own.

Gittes returns to Evelyns mansion. There, he discovers a pair of bifocals in her salt water garden pond and finds her servants packing her bags. His suspicions aroused, he confronts Evelyn about her "sister", whom she then claims is her daughter, Katherine. Gittes slaps her repeatedly until she cries out "Shes my sister and my daughter!", then tearfully asks Gittes if it is "too tough" for him to understand what happened with her father. She points out that the eyeglasses are not her husbands, as he did not wear bifocals.

Gittes makes plans for the two women to flee to Mexico. He instructs Evelyn to meet him at her butlers home in Chinatown, Los Angeles|Chinatown. Gittes summons Cross to the Mulwray home to settle their deal for the girl. Cross admits his intention to annex to the City of Los Angeles the northwest valley, then irrigate and develop it. Gittes produces Crosss bifocals — a link to Mulwrays murder. Mulvihill appears and confiscates the glasses, then forces Jake to drive him with Cross to the women.

When the three reach the Chinatown hiding place, the police are already there and detain Gittes. Evelyn will not allow Cross to approach Katherine, and when he is undeterred she shoots him in the arm and drives away with Katherine. As the car speeds off, the police open fire, killing Evelyn. Cross clutches Katherine and leads her away, while Escobar orders Gittes released, along with his associates. One of them urges "Forget it, Jake. Its Chinatown."

==Cast==
 
* Jack Nicholson as J.J. "Jake" Gittes
* Faye Dunaway as Evelyn Cross Mulwray
* John Huston as Noah Cross
* Perry Lopez as Lieutenant Lou Escobar
* John Hillerman as Russ Yelburton
* Darrell Zwerling as Hollis I. Mulwray
* Diane Ladd as Ida Sessions
* Roy Jenson as Claude Mulvihill
* Roman Polanski as A man with a knife
* Richard Bakalyan as Detective Loach
* Joe Mantell as Lawrence Walsh
* Bruce Glover as Duffy
* James Hong as Kahn
* Roy Roberts as Mayor Bagby
* Noble Willingham as Councilman
* Rance Howard as Irate Farmer
* Burt Young as Curly
* Belinda Palmer as Katherine Cross

==Production==

===Background=== Robert Evans The Great Gatsby (1974), but Towne felt he could not better the F. Scott Fitzgerald The Great Gatsby|novel. Instead, Towne asked for $25,000 from Evans to write his own story, Chinatown, to which Evans agreed.    * Thomson, David (2005). The Whole Equation: A History of Hollywood. ISBN 0-375-40016-8 

Chinatown is set in 1937 and portrays the manipulation of a critical municipal resource—water—by a cadre of shadowy oligarchs. It was the first part of Townes planned trilogy about the character J.J. Gittes, the foibles of the Los Angeles power structure, and the subjugation of public good by private greed.    The second part, The Two Jakes, was about another grab for a natural resource—oil—with a thicker-torsoed Gittes in the 1940s. It was directed by Jack Nicholson and released in 1990, but the second films commercial and critical failure scuttled plans to make Gittes vs. Gittes,  about the third finite resource—land—in Los Angeles, circa 1968. 

===Origins=== Santa Paula, was inundated with flood water, the result being the end of Mulhollands career.  * Reisner, Marc (1986). Cadillac Desert. ISBN 0-670-19927-3 

===Script===
Towne wrote the screenplay with Jack Nicholson in mind.  He took the title (and the exchange, "What did you do in Chinatown?" / "As little as possible") from a Hungarian vice cop who had worked in Chinatown and explained to the writer that the complicated array of dialects and gangs in Los Angeless Chinatown made it impossible for the police to know whether their interventions were helping victims or furthering their exploitation. 
 murder of his wife and unborn child in Los Angeles, was initially reluctant to return but was persuaded on the strength of the script. 

Evans wanted Cross to die and Evelyn Mulwray to survive. The producer and director argued over it, with Polanski insisting on a tragic end. "I knew that if Chinatown was to be special," Polanski said, "not just another thriller where the good guys triumph in the final reel, Evelyn had to die."  . Turner Classic Movies. Retrieved August 22, 2012.  They parted ways over this dispute and Polanski wrote the final scene a few days before it was shot. 

The original script was more than 180 pages and included a narration by Gittes; Polanski cut that and reordered the story so the audience and Gittes unraveled the mysteries at the same time.

===Characters and casting===
* "J. J. Gittes" was named after Nicholsons friend, producer Harry Gittes.
* "Evelyn Mulwray" is, according to Towne, intended to initially seem the classic "black widow" character typical of lead female characters in film noir, yet is eventually made the only selfless character in the film. Jane Fonda was strongly considered for the role; but Polanski insisted on Dunaway. 
* "Noah Cross": Towne said that Huston was, after Nicholson, the second-best-cast actor in the film and that he made the Cross character menacing, through his courtly performance. 
* Polanski appears in a cameo as the gangster who cuts Gittes nose. The effect was accomplished with a special knife which indeed could have cut Nicholsons nose if Polanski had not held it correctly.

===Filming===
William A. Fraker accepted the cinematographer position from Polanski when Paramount agreed. He had worked with the studio previously in Polanskis Rosemarys Baby (film)|Rosemarys Baby. Robert Evans, never consulted about the decision, insisted that the offer be rescinded since he felt pairing Polanski and Fraker again would create a team with too much control over the project and complicate the production. Fraker was replaced by John A. Alonzo. 

In keeping with a technique Polanski attributes to Raymond Chandler, all of the events of the film are seen subjectively through the main characters eyes; for example, when Gittes is knocked unconscious, the film fades to black and fades in when he awakens. Gittes appears in every scene of the film. 

===Soundtrack===
{{Infobox album  
| Name        = Chinatown
| Type        = film
| Longtype    =
| Artist      = Jerry Goldsmith
| Cover       = ChinatownOST.JPG
| Released    = 1974
| Recorded    =
| Genre       = Jazz, soundtrack
| Label       = Varèse Sarabande
| Producer    =
}} top 25 American film scores.  Terry Teachout of the Wall Street Journal  and filmmaker David Lynch have both praised it.  Goldsmiths score, with haunting trumpet solos, by Hollywood studio musician and MGM first trumpet Uan Rasey, was released through ABC Records and features twelve tracks at a running time just over thirty minutes.

# "Love Theme from Chinatown (Main Title)"
# "Noah Cross"
# "Easy Living"
# "Jake and Evelyn"
# "I Cant Get Started"
# "The Last of Ida"
# "The Captive"
# "The Boy on a Horse"
# "The Way You Look Tonight"
# "The Wrong Clue"
# "J.J. Gittes"
# "Love Theme From Chinatown (End Title)"

==Reception==
The film earned $17 million at the box office outside North America, which Evans said was a million dollars more than it earned in North America. THE OVERSEAS CONNECTION: TAKING STARS TO MARKET
Wilson, John M. Los Angeles Times (1923-Current File)   18 Mar 1979: o3. 

==Legacy==
Townes screenplay has become legendary among critics and filmmakers, often cited as one of the best examples of the craft.    Polanski decided the fatal final scene, changing Townes idea of a happy ending.

Chinatown brought more public awareness to the land dealings and disputes over water rights, which arose while drawing Los Angeles water supply from the Owens Valley in the 1910s.  Margaret Leslie Davis, in her 1993 book Rivers in the Desert: William Mulholland and the Inventing of Los Angeles, says the sexually charged film is a metaphor for the "rape" of the Owens Valley and notes that it fictionalizes Mulholland, while concealing the strong public support for Southern Californias water projects.

The film holds a 98% "Certified Fresh" rating on Rotten Tomatoes with 60 reviews.  Metacritic assigned a rating of 86/100 based on 10 critic reviews. 

==Awards and honors==

===Academy Awards – 1974===
The film won one Academy Award of the eleven total nomination categories:      
;Wins Best Original Screenplay – Robert Towne
;Nominations Best Picture Robert Evans Best Director – Roman Polanski Best Actor – Jack Nicholson Best Actress – Faye Dunaway Best Film Editing – Sam OSteen Best Art Direction – Richard Sylbert, W. Stewart Campbell, Ruby Levitt Best Costume Design – Anthea Sylbert Best Cinematography – John A. Alonzo Best Sound Bud Grenzbach, Larry Jost Best Music Score – Jerry Goldsmith

===Golden Globes – 1974===
;Wins Best Motion Robert Evans Best Actor in a Motion Picture – Drama – Jack Nicholson Best Director – Roman Polanski Best Screenplay – Robert Towne
;Nominations Best Actor in a Supporting Role – John Huston Best Actress – Motion Picture Drama – Faye Dunaway Best Original Score – Jerry Goldsmith

===Other awards===
* 1975 – British Academy of Film and Television Arts|BAFTA, Best Actor (Nicholson), Best Direction, Best Screenplay (male)
* 1975 – Edgar Award, Best Motion Picture Screenplay – Robert Towne
* 1991 – National Film Registry
* 2010 – Best film of all time, The Guardian 

American Film Institute recognition
* 1998 – AFIs 100 Years...100 Movies – Ranked 19th
* 2001 – AFIs 100 Years...100 Thrills – Ranked 16th
* 2003 – AFIs 100 Years...100 Heroes and Villains:
** Noah Cross – Ranked 16th Villain
** J.J. "Jake" Gittes – Nominated Hero
* 2005 – AFIs 100 Years...100 Movie Quotes:
** "Forget it, Jake, its Chinatown." – Ranked 74th
** "Shes my sister! Shes my daughter!" – Nominated
* 2005 – AFIs 100 Years of Film Scores – Ranked 9th
* 2007 – AFIs 100 Years...100 Movies (10th Anniversary Edition) – Ranked 21st
* 2008 – AFIs 10 Top 10 mystery film – Ranked 2nd

==References==
 

;Bibliography
* Easton, Michael (1998) Chinatown (B.F.I. Film Classics series). Los Angeles: University of California Press. ISBN 0-85170-532-4.
* Thomson, David (2004). The Whole Equation: A History of Hollywood. New York, New York: Alfred A. Knopf. ISBN 0-375-40016-8.
* Towne, Robert (1997). Chinatown and the Last Detail: 2 Screenplays. New York: Grove Press. ISBN 0-8021-3401-7.
* Tuska, Jon (1978). The Detective in Hollywood. Garden City, New York: Doubleday & Company. ISBN 0-385-12093-1.

==External links==
  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 