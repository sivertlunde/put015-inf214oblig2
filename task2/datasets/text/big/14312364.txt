Purple Violets
 
{{Infobox film
| name = Purple Violets
| image =
| caption = Theatrical film poster
| director = Edward Burns
| producer = Margot Bridger Edward Burns Becky Glupczynski Aaron Lubin Nicole Marra Pamela Schein Murphy
| writer = Edward Burns Patrick Wilson Edward Burns Debra Messing
| released =  
| runtime = 101 minutes
| country = United States
| language = English
| budget = $4 million 
}} Patrick Wilson, and Debra Messing.

==Plot synopsis== Patrick Wilson), a successful crime novelist. Kate (Debra Messing) is Pattis best friend since college. Shes a tough-talking schoolteacher who plays therapist to all Pattis problems, while shes got a few of her own.

Despite Brians gorgeous Tribeca loft and perfect house in the Hamptons, he longs to write works of greater literary value.  Michael Murphy (Edward Burns), his lawyer and best friend from college still carries a flame for his former girlfriend Kate, even though their relationship ended badly years ago. He tried to forget over the years but since he spotted Kate at a restaurant she becomes all he can think about. She holds a major grudge, but he will go to any length to win Kate back, knowing his feelings for her never went away. 

When Patti sells Murphy a new apartment, and Brian publishes his personal novel, these old friends reconnect in unexpected ways with surprising results.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Selma Blair || Patti Petalson
|- Patrick Wilson || Brian Callahan
|-
| Edward Burns || Michael Murphy
|-
| Debra Messing || Kate Scott
|-
| Dennis Farina || Gilmore
|-
| Max Baker || Mark
|-
| Donal Logue || Chazz Coleman
|-
| Elizabeth Reaser || Bernadette (Bernie) 
|-
| Peter Jacobson || Monroe (Cameo)
|-
| Bill Hader || Bookstore Fan (Cameo)
|}

== Soundtrack ==

Purple Violets soundtrack includes mainly songs by the Alternative rock band "The Blue Jackets" including the ballads "Do you Remember" and "You Send Shivers" played during the final scene and credits.

== Distribution ==

Purple Violets became noteworthy for being the first feature film to debut on the iTunes Store.
It was released on November 20, 2007 and costs $14.99. The movie was exclusive to Apple Inc. for one month after release. Subsequently, Purple Violets was released on DVD through the Weinstein Company.

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 