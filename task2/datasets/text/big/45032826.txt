Black Emanuelle 2
{{Infobox film
| name           = Black Emanuelle 2
| image_size     = 
| image	         = The_New_Black_Emanuelle.jpg	
| caption        = 
| director       = Bitto Albertini (as Albert Thomas)
| producer       = 
| writer         = Bitto Albertini  Mario Mariani
| narrator       = 
| starring       = Shulamith Lasri  Angelo Infanti
| music          = Don Powell
| cinematography = Guglielmo Mancori
| editing        = 
| distributor    = 
| released       = 31 July 1976
| runtime        = 91 minutes
| country        = Italy Italian
| budget         = 
}} Italian psychological drama film|drama-sexploitation film directed by Bitto Albertini. It is an unofficial sequel of  Black Emanuelle.    Manlio Gomarasca. "Nero su bianco - Emanuelle nera n° 2" in AA.VV. Al tropico del sesso, Nocturno Dossier (35). November 2005. p. 32. 

==Background==
Although following Albertinis successful 1975 sexploitation film Black Emanuelle and the allusion to the title, Black Emanuelle 2 differs greatly in plot than the first film, featuring Israeli actress Shulamith Lasri, as Emanuelle Richmond Morgan, a supermodel going through a state of amnesia and locked in a mental institution in New York.   Lasri, at her first and only film, is credited just as "Emanuelle Nera".   The lead actor, as in the first film, is Angelo Infanti, who plays a different character.  The film was shot between Rome, New York and Venice. 

==Plot== 
Emanuelle Richmond Morgan (Lasri) is an African American supermodel married to basketball star Fred Morgan (Percy Hogan). She had visited Beirut in July 1976 and fell in the centre of the Lebanese Civil War. She has been going through a state of amnesia since then, kept at a mental institution in Manhattan. Dr. Paul Gardner (Infanti) who is in charge of the clinic takes a special interest in Richmonds case and begins to personally investigate her past, starting with the photographer John Farmer (Franco Cremonini) who was with her in Beirut.

==Cast==
*Shulamith Lasri: Emanuelle Richmond Morgan (credited as Emanuelle Nera)
*Angelo Infanti: Dr. Paul Gardner
*Percy Hogan: Fred Morgan
*Dagmar Lassander: Susan Gardner
*Sharon Lesley: Sharon, Pauls niece
*Franco Cremonini: John Farmer
*Don Powell: Emanuelles father
*Pietro Torrisi: Simon
*Attilio Dottesio: The General

==Reception==
The film was generally badly received by critics. According to Manlio Gomarasca, it has an "unattractive story", in which "it is not clear if the irony that winds throughout the film is voluntary or not".  Film critic Paolo Mereghetti wrote that the film is "extremely poor", and her lead actress has "generous shapes but a rare inexpressiveness".    In its DVD Talk review, it is referred as "flat, tedious and barely thrilling" and as "a relatively daft and boring entry into the cycle".  In his DVD Verdict review, Daryl Loomis wrote that the film is "mind-numbingly dull and looks like a grouping of unconnected scenes". 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 