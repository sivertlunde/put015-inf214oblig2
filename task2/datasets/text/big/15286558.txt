The Soloist
{{Infobox Film
| name           = The Soloist
| image          = SoloistPoster.jpg
| image_size     =
| caption        =
| director       = Joe Wright
| producer       = Gary Foster Russ Krasnoff
| writer         = Susannah Grant
| based on    =  
| narrator       =
| starring       = Jamie Foxx Robert Downey, Jr. Catherine Keener Tom Hollander Lisa Gay Hamilton 
| music          = Dario Marianelli 
| cinematography = Seamus McGarvey
| editing        = Paul Tothill DreamWorks Pictures StudioCanal Participant Media Working Title Films Universal Pictures   
| released       = 24 April 2009  (US)  11 September 2009  (UK) 
| runtime        = 117 min.  
| country        = United States United Kingdom France
| language       = English
| budget         = $60 million  
| gross          = $38.3 million  
}}
 2009 United American drama film directed by Joe Wright, and starring Jamie Foxx and Robert Downey, Jr.. It is based on the true story of Nathaniel Ayers, a musician who developed schizophrenia and became homeless. The screenplay by Susannah Grant is based on the book, The Soloist by Steve Lopez.

Foxx portrays Ayers, who is considered a cello prodigy, and Downey portrays Lopez, a Los Angeles Times columnist who discovers Ayers and writes about him in the newspaper. The film was released in theatres on 24 April 2009  and on DVD and Blu-ray Disc|Blu-ray August 5.

==Plot==
In 2005, Steve Lopez (Robert Downey, Jr.) is a journalist working for the L.A. Times. He is divorced and now works for his ex-wife, Mary (Catherine Keener), an editor. A biking accident lands Lopez in a hospital.

One day, he hears a violin being played beautifully. Investigating, he encounters Nathaniel Ayers (Jamie Foxx), a homeless Schizophrenia|schizophrenic, who is playing a violin when Lopez introduces himself. During the conversation that follows, Lopez learns that Ayers once attended Juilliard School|Juilliard.

Curious as to how a former student of such a prestigious school ended up on the streets, Lopez contacts Juilliard but learns that no record of Ayers graduating from it exists. Though at first figuring a schizophrenic whos talented with a violin isnt worth his time, Lopez soon realizes that he has no better story to write about. Luckily, he soon learns that Ayers did attend Juilliard, but dropped out after two years.

Finding Ayers the next day, Lopez says he wants to write about him. Ayers doesnt appear to be paying attention. Getting nowhere, Lopez finds and contacts Ayers sister (  with the cello, until he began displaying symptoms of schizophrenia at Juilliard. Unable to handle the voices, Ayers dropped out and ended up on the streets due to the delusion that his sister wanted to kill him. Without a cello, he has resorted to playing a two-string violin.

Lopez writes his article. One reader is so touched, she sends him a cello for Ayers. Lopez brings it to him and Ayers is shown to be just as proficient as with a violin. Unfortunately, his tendency to wander puts both Ayers and the cello in danger, so Lopez talks him into leaving it at a shelter, located in a neighborhood of homeless people. Ayers is later seen playing for the homeless.

A concerned Lopez tries to get a doctor he knows (Nelsan Ellis) to help. He also tries to talk Ayers into getting an apartment, but Ayers refuses. After seeing a reaction to music played at an opera house, Lopez persuades another friend, Graham (Tom Hollander), a cellist, to rehabilitate Ayers through music. The lessons go well, though Ayers is shown to be getting a little too attached to Lopez, much to the latters annoyance. Lopez eventually talks Ayers into moving into an apartment by threatening to abandon him.

Lopez article on Ayers gains so much fame, Ayers is given the chance to perform a recital. Sadly, he loses his temper, attacks Graham and leaves. This convinces Lopez doctor friend to get Ayers help. But when Ayers learns what Lopez is up to, he throws Lopez out of his apartment and threatens to kill him.

While speaking with Mary, Lopez realizes that not only has he changed Ayers life, Ayers has changed him. Determined to make amends, Lopez brings Ayers sister to L.A. for a visit. Ayers and Lopez make up. Later, while all watch an orchestra, Lopez ponders how beneficial their friendship has been. Ayers still hears voices, but at least he no longer lives on the streets. In addition, Ayers has helped Lopezs relationship with his own family.

It is revealed in the end that Ayers is still a member of the LAMP Community, a Los Angeles-based nonprofit organization that seeks to help people living with severe mental illness, and that Lopez is learning how to play the guitar.

==Cast==
*Jamie Foxx as Nathaniel Ayers
*Robert Downey, Jr. as Steve Lopez
*Catherine Keener as Mary Weston
*Tom Hollander as Graham Claydon
*Lisa Gay Hamilton as Jennifer
*Nelsan Ellis as David Carter
*Rachael Harris as Leslie Bloom
*Stephen Root as Curt Reynolds
*Lorraine Toussaint as Flo Ayers
*Justin Martin as Young Nathaniel Ayers
*Octavia Spencer as Troubled Woman
*Jena Malone as Cheery Lab Tech
*Lemon Andersen as Uncle Tommy

==Production==
The Soloist, directed by Joe Wright, was written by Susannah Grant, based on a series of columns  written by Los Angeles Times columnist Steve Lopez, who chronicled the plight of Nathaniel Ayers, Jr., a musician with schizophrenia, and eventually was chronicled in Lopez book, The Soloist.    Grant drew elements of the story from a book written by Lopez, which was published in the spring of 2008.     The film was budgeted at $60 million, twice the budget amount of Wrights previous film, Atonement (film)|Atonement.   Production began in January 2008 and was filmed mostly in Los Angeles,  with some scenes shot in Cleveland.

==Reception==

===Critical reaction===
The film received positive to mixed reviews. On Rotten Tomatoes, the film has a rating 56%, based on 200 reviews, with an average score of 5.9/10. The sites consensus reads, "Though it features strong performances by its lead players, a lack of narrative focus prevents The Soloist from hitting its mark."    At Metacritic the film received an average score of 61 out of 100, based on 33 critics, indicating "generally favorable reviews".  
 Pride & Prejudice and Atonement (film)|Atonement.

===Box office=== 17 Again, and Obsessed (2009 film)|Obsessed, grossing $9,715,000 in 2,024 theaters with a $4,800 average per theater.  The film went on to only recover about half of its total budget bringing in $31,720,158. This was blamed on the films initial release date being postponed, as well as the films release coming one week before the 2009 summer movie season.

==Soundtrack==
The soundtrack to The Soloist was released on April 21, 2009.

{{Track listing
| collapsed       = no
| extra_column    = Artist
| total_length    = 47:47 

| title1          = Pershing Square
| length1         = 0:48
| extra1          = Dario Marianelli

| title2          = Crazy About Beethoven
| length2         = 2:01
| extra2          = Dario Marianelli

| title3          = Paper Mache World
| length3         = 1:33
| extra3          = Dario Marianelli

| title4          = A City Symphony
| length4         = 3:41
| extra4          = Dario Marianelli

| title5          = This Is My Apartment
| length5         = 1:54
| extra5          = Dario Marianelli

| title6          = There Is No Escape
| length6         = 1:36
| extra6          = Dario Marianelli

| title7          = Falling Apart
| length7         = 1:10
| extra7          = Dario Marianelli

| title8          = Four Billion Years
| length8         = 2:54
| extra8          = Dario Marianelli

| title9          = Nathaniel Breaks Down
| length9         = 5:31
| extra9          = Dario Marianelli

| title10         = Accordion Interlude
| length10        = 2:07
| extra10         = Dario Marianelli

| title11         = The Lords Prayer
| length11        = 3:12
| extra11         = Dario Marianelli

| title12         = The Voices Within
| length12        = 2:09
| extra12         = Dario Marianelli

| title13         = Sister
| length13        = 5:34
| extra13         = Dario Marianelli

| title14         = Cello Lesson
| length14        = 2:27
| extra14         = Dario Marianelli

| title15         = Mr. Ayers And Mr. Lopez
| length15        = 11:10
| extra15         = Dario Marianelli

}}


==See also==

* White savior narrative in film

==Notes==
 

==References==
 
 
*Thompson, Anne.  , Variety, October 19, 2008.

==External links==
*  
* 
* 
* 
* 
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 