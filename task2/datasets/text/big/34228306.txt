A Good Day to Die Hard
{{Infobox film
| name           = A Good Day to Die Hard
| image          = A Good Day to Die Hard.jpg
| image_size     =
| caption        = Theatrical release poster John Moore
| producer       = {{Plainlist | Alex Young
*Wyck Godfrey
}}
| screenplay     = Skip Woods
| story          =  
| based on       =  
| starring       = {{Plainlist |
*Bruce Willis
*Jai Courtney
*Sebastian Koch
*Rasha Bukvić
*Cole Hauser
*Yuliya Snigir
}}
| music          = Marco Beltrami
| cinematography = Jonathan Sela
| editing        = Dan Zimmerman
| studio         = {{Plainlist |
*Giant Pictures
*TSG Entertainment
}}
| distributor    = 20th Century Fox
| released       =   
| runtime        = 97 minutes
| country        = United States
| language       = {{Plainlist |
*English
*Russian
}} 
| budget         = $92 million   
| gross          = $304.7 million 
}} John Moore and written by Skip Woods, and starring Bruce Willis as John McClane. The main plot finds McClane travelling to Russia to get his estranged son, Jack, out of prison, but is soon caught in the crossfire of a terrorist plot. Talks of a fourth sequel to Die Hard (1988) began before the release of Live Free or Die Hard (2007), with Willis affirming that the latter would not be the last in the series, but pre-production did not start until September 2011, when John Moore was officially announced as the director. Filming began in April 2012, primarily in Budapest, Hungary.
 premiered in East and Dolby Atmos Surround Mixing and the first to also be released in IMAX theaters. The film was also the first, unlike the previous films, to be a critical disappointment, receiving mostly negative reviews,       but it nevertheless grossed over three times its budget worldwide. It is the first film produced by TSG Entertainment, since distributor 20th Century Foxs departure from Dune Entertainment upon the completion of their distribution contract at the end of 2012.

==Plot==
In Moscow, Viktor Chagarin (Sergei Kolesnikov), a high-ranking, but corrupt Russian official, plans on incriminating political prisoner, former billionaire and government whistleblower, Yuri Komarov (Sebastian Koch), without a fair trial when Komarov refuses to hand over a secret file believed to have convicting evidence against Chagarin. In a separate incident, Jack McClane (Jai Courtney) is arrested as a result of an assassination attempt but agrees to testify against Komarov for a shorter sentence. John McClane (Bruce Willis), who has not been in touch with his son for several years, learns of his sons actions and travels to Russia to help. When John finally arrives and approaches the courthouse that holds Komarov on trial, a bomb explosion orchestrated by Chagarins henchman Alik (Radivoje Bukvić) occurs in the courthouse, and Jack breaks free with Komarov. Seeing his son, John confronts him, but their dispute is cut short when Alik and his men chase them in a Ural Typhoon MRAP on the streets of Moscow. John, Jack and Komarov manage to escape.
 CIA officer in an undercover operation for the past three years. Jacks partner, Collins (Cole Hauser), demands the files location from Komarov so that the CIA can bring Chagarin down, and Komarov agrees to hand it over on condition that he and his daughter are given safe passage out of Russia. But Collins is then shot by Chagarins men and dies, as The McClanes and Komarov survive subsequent heavy gunfire and escape again. They make their way to a hotel in the city to find the key to a vault containing the file. There, they meet Komarovs daughter, Irina (Yuliya Snigir), whom they earlier planned on joining with, but John grows suspicious of her shifty behavior, and is proven correct when Alik and his men crash in and tie John and Jack up, while Komarov is taken as a hostage, and Irina confesses to snitching on them for the "millions of dollars" to be gained. Jack breaks free of his ties and kills the nearest guards using a Russian gun-knife allowing the two to kill most of the men. Alik and Irina, with Komarov still their hostage, come back on a helicopter and try to kill them, but the two manage to escape.

That night, the two steal a car full of firearms and drive to Pripyat, Ukraine, the location of the vault that houses the file. When they arrive, they find that Komarov, Irina, and Alik have already arrived, but what they dont know is that there was never a file in existence, and that the drawer with the file inside was actually the secret passage to a vault containing a large amount of weapons-grade uranium. Once inside the vault, Komarov kills Alik and calls Chagarin to tell him that he and Irina had manipulated the entire sequence of events since the beginning in an attempt to obtain the uranium and get revenge on Chagarin. Chagarin is then killed by a subordinate of the Komarovs.

At this point, John and Jack enter the vault, discover Komarovs true plot, and capture him. Irina, with another henchman, comes to her fathers aid, but before they can escape, Jack goes after Komarov, while John goes after Irina, who is escaping on a Mil Mi-25 helicopter. Irina tries to protect her father by firing the helicopters cannons at Jack, but John is able to bring the helicopter out of balance by driving a truck in the hangar section, still shackled by a chain, out of the open rear ramp of the helicopter; he is later thrown off into the building. Komarov remarks that Jack will get to watch his father die, which enrages Jack to hurl him off the rooftop into the path of the spinning helicopters rotors, killing him. As Jack reunites with John inside the building, Irina, wanting to avenge her father, rams the helicopter into the building in a suicide attack, but both father and son survive by leaping off the building and into a large pool of water as the helicopter crashes and explodes, killing Irina. John and Jack go back to New York and reconcile with Johns daughter - and Jacks sister - Lucy McClane (Mary Elizabeth Winstead) on the tarmac.

==Cast==
* Bruce Willis as John McClane, a police officer and detective on a "vacation" in Russia to find his arrested son John McClane, Jr. John "Jack" McClane, Jr., the only son of the senior McClane and a CIA operative on a mission to deliver Komarov and the supposed file.
* Sebastian Koch as Yuri Komarov, a "political prisoner" who is supposedly in possession of an incriminating file.
* Yuliya Snigir as Irina Komarov, Yuris daughter  
* Radivoje Rasha Bukvić as Alik, Chagarins main enforcer. Lucy McClane, McClanes oldest child and Jacks older sister.
* Cole Hauser as Mike Collins, a CIA operative and Jacks partner.
* Amaury Nolasco as Murphy, a NYPD detective and McClanes friend.
* Sergei Kolesnikov as Viktor Chagarin, a corrupt, high-ranking Russian official.
* Roman Luknár as Anton, the ill-fated associate of Chagarin that was killed by Jack in the beginning.
* Ganxsta Zolee as MRAP Driver
* Péter Takátsy as Prosecutor
* Pasha D. Lychnikoff as Cabbie
* Megalyn Echikunwoke as Reporter
* Melissa Tang as Lucas
* Ivan Kamaras as G-Wagon Driver
* Sophie Raworth as BBC News reporter (herself)

==Production==

===Pre-production=== John Moore was subsequently drafted in to replace him.   
 crossover between Die Hard and 24 (TV series)|24 series, with Kiefer Sutherland to reprise his role as Jack Bauer alongside John McClane.  This was never confirmed by the studio, and the films title was later revealed to be A Good Day to Die Hard—with no further mention of any involvement from the 24 series—with a release date of February 14, 2013. 

===Casting===
Bruce Willis returned as John McClane, and has expressed a desire to shoot A Good Day to Die Hard and a sixth installment in the series before retiring the character. 

When casting the role of Jack McClane, the studios considered several actors, including Liam Hemsworth and James Badge Dale,  before ultimately settling on Australian actor Jai Courtney.  Mary Elizabeth Winstead also featured in the film, reprising her role as McClanes daughter Lucy. 

Sebastian Koch played the films primary antagonist, Yuri Komarov,    while Yuliya Snigir and Cole Hauser featured as secondary characters Irina and Collins.   The cast was completed by actors Amaury Nolasco as a friend of McClane,    Pasha D. Lychnikoff as a taxi driver,  and Megalyn Echikunwoke, Anne Vyalitsyna, and Ivan Kamaras in smaller roles.  

===Shooting===
Production began in Hungary in April 2012, with the capital Budapest standing in for Moscow.  A military shooting range near Hajmáskér was used for shooting live ammunition,   while vehicular stunts were shot at the Hungaroring, a Formula One racing circuit in Mogyoród.   

In July 2012, a fire broke out on the set while shooting an aerial stunt, though no one was injured and shooting resumed after a short delay. 
 35mm Arri    cameras equipped with long lenses to capture tight close-ups, for Moore explained, "McClane is in a strange world, with little or no initial control over his environment. Hes unable to anticipate things as he normally might. Hes caught off guard, and we want the camera to mimic that surprise and confusion."  Moore also chose to create as many of the films effects on camera as possible, only using visual effects to enhance elements or paint in backgrounds.   

===Post-production===
A specially censored version was prepared for theatrical release in the United Kingdom, which was cut for language and violence in order to attain a 12A at the request of the distributors.  The U.S. version is rated R and is uncut. The films audio was mixed in Dolby Atmos surround sound.  In February 2013, director Moore began work on a directors cut, which was later released on Blu-ray.   

==Soundtrack==
{{Infobox album  
| Name       = A Good Day to Die Hard: Original Motion Picture Soundtrack
| Type       = Soundtrack
| Artist     = Marco Beltrami
| Cover      = 
| Released   =  
| Recorded   =  
| Genre      = Film score
| Length     = 
| Label      = Sony Classical
| Producer   = Marco Beltrami
| Misc       = 
}}
Marco Beltrami, who had composed the soundtrack for the previous film, Live Free or Die Hard, returned to score A Good Day to Die Hard. Beltrami again incorporates Michael Kamens material from the first three films into his score. Beltrami only had six weeks in which to write the music, and new scenes were still being shot as the music was being recorded. In the end, he wrote around 120 minutes of music, with 80 of those minutes making it into the final film.  The soundtrack album was released on February 14, 2013 digitally and in retailers by Sony Classical.  Five orchestrators were involved: Pete Anthony, Jon Kull, Dana Niu, Rossana Galante, Andrew Kinny. The orchestra was conducted by Pete Anthony.

The score was programmed by Buck Sanders, with additional music composed by Marcus Trumpp and Brandon Roberts. 

{{Track listing
| collapsed =
| all_music = Marco Beltrami
| title1 = Yuri Says, “привет”
| length1 = 2:19
| title2 = Getting Yuri to the Van 
| length2 = 2:14
| title3 = Jack Makes the Call 
| length3 = 2:53
| title4 = Everyone to the Courthouse
| length4 = 3:09
| title5 = Court Adjourned 
| length5 = 2:19
| title6 = Truckzilla (Act 1)
| length6 = 3:38
| title7 = Yippie Kay Yay, Mother Russia!
| length7 = 1:54
| title8 = Truckzilla (Act 2)
| length8 = 2:00
| title9 = Father & Son
| length9 = 1:24
| title10 = To the Safe House
| length10 = 1:51
| title11 = Regroup
| length11 = 2:30
| title12 = Leaving the Safe House 
| length12 = 1:59
| title13 = Getting to the Dance Floor
| length13 = 1:34
| title14 = Too Many Kolbasas on the Dance Floor 
| length14 = 3:53
| title15  = Whats So Funny?
| length15 = 2:30
| title16  = McClanes Get the Bird
| length16 = 3:00
| title17  = Scumbags 
| length17 = 2:05
| title18  = Entering Chernobyl 
| length18 = 4:07
| title19 = Into the Vault
| length19 = 2:17
| title20 = Rubbed Out at the Spa
| length20 = 2:07
| title21 = Sunshine Shootout 
| length21 = 1:37
| title22 = Get to the Choppa! 
| length22 = 2:59
| title23 = Chopper Takedown
| length23 = 3:26
| title24 = Its Hard to Kill a McClane
| length24 = 2:59
| title25 = Triple Vodka Rhapsody 
| length25 = 1:55
| title26 = McClanes Brain 
| length26 = 2:00
}}

==Release== 2012 Supercup 2012 USGP) promoting the release date on February 14, 2013.]] East and Southeast Asian territories on February 7.

In the United States and Canada, the film was distributed to 2,328 theaters for night showings on February 13.    Select theaters also held a one-time special marathon of all Die Hard films to lead up to A Good Day to Die Hards nationwide release,  with Bruce Willis making a personal appearance at one of these marathons in New York City to thank fans.  The film then expanded to a total of 3,553 theaters, including IMAX theaters, on February 14. 

==Reception== average score of 3.9/10. The sites critical consensus reads, "A Good Day to Die Hard is the weakest entry in a storied franchise, and not even Bruce Willis smirking demeanor can enliven a cliched, uninspired script."  By comparison, Metacritic gave the film a score of 28 out of 100, based on 40 critics, indicating "generally unfavorable" reaction.  On both websites, the film ranked lowest among the Die Hard films.

A. O. Scott of The New York Times described A Good Day to Die Hard as "a handful of extended set pieces—each more elaborate and therefore somehow less exciting than the last—linked by a simple-minded plot and a handful of half-clever lines." Though complimenting the special effects, he criticizes the direction of John Moore, the lack of style, and writes that "everything that made the first Die Hard memorable—the nuances of character, the political subtext, the cowboy wit—has been dumbed down or scrubbed away entirely."  Todd McCarthy of The Hollywood Reporter expressed similar sentiments, particularly of the direction, for which he says that Moore "has directed these sequences in a way that makes the incidents look so far-fetched and essentially unsurvivable that you can only laugh".  Kenneth Turan of the Los Angeles Times gave the film a 2/5 and remarked that it lacked "inspiration", and that the onscreen rivalry of Willis and Courtney was "more irritant than enticement."  Richard Roeper, standing in for Roger Ebert on Eberts website, rated the film one and a half stars out of four, criticizing the implausibility of the action sequences, as well as the films lack of sufficient characterization for McClane and the villains as compared to the other films in the series. He says that "McClane has been stripped of any real traces of an actual three-dimensional character," and that the film "never giv  us a chance to get the least bit involved with any of these characters." 
 The Telegraph Peter Howell of The Toronto Star remarked that Willis and Courtney made a strong estranged family duo and that the film had a nice drinking game routine going for it with how many times McClane exclaims "Im on vacation!" during the running time. 

===Box office===
A Good Day to Die Hard grossed $67,349,198 in North America and $237,304,984 in other territories for a worldwide total of $304,654,182, roughly three times its $92 million budget.
 Safe Haven.  However, for the whole 4-day Presidents Day weekend, A Good Day to Die Hard opened in first place with $28,640,657, bringing its total at that point to $36,879,773. 

Overseas, A Good Day to Die Hard grossed $10,860,000 in its first weekend. Opening in seven Asian markets at 1,182 locations a week before North Americas release (February 6–7) to take advantage of the Chinese New Year holiday, the majority of the films gross came from South Korea, with the film also setting a Fox record in Indonesia and a series record in Hong Kong. 

===Home media===
A Good Day to Die Hard was released on DVD and Blu-ray Disc|Blu-ray on June 4, 2013.  There is an extended cut that is only available on the Blu-ray version. It features a longer car chase through Moscow and some other slightly extended scenes. It also completely removes Lucy from the film.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 