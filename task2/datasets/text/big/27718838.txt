The Scarlett O'Hara War
 
 
The Scarlett OHara War is a 1980 made for TV movie directed by John Erman. It is based on the novel Moviola (novel)|Moviola by Garson Kanin. The film is set against the backdrop of late 1930s Hollywood as the search for Scarlett OHara begins to unfold.

== Synopsis ==
In 1936, Margaret Mitchells Gone With The Wind is published and it is an instant nationwide sensation. As all things in Hollywood go, the movie rights are up for grabs and every studio in Hollywood wants in on it. While having lunch at the MGM dining room, Louis B. Mayer is talking to his son-in-law David O. Selznick about the film rights. In time, Selznick establishes his own production company, Selznick International Pictures, and wants his studio to have a film that will cement both its fame and his as well.

Back at MGM, Joan Crawford is negotiating the idea of her portraying the acclaimed heroine, even getting Selznick to come back to her place to spend the night to "seal the deal." However, other actresses must be tested in order to expand possibilities. One of the first to do this is Paulette Goddard and her screen test is the most praised out of them all. Tallulah Bankhead comes down from New York and auditions for the role and although she herself is a Southerner who could easily play the part, Selznick decides to give her more tests and seek other candidates. But when Louella Parsons gets wind of this, she misinforms her radio audience that Tallulah has gotten the part, thanks to the influential power of her father William Brockman Bankhead. When this is announced, Joan Crawford throws her radio at a mirror and Paulette makes a beeline to the study of her lover, Charles Chaplin, announcing that Tallulah has gotten the part.

After this error has been cleared and the actresses have been reassured that the role is still up for grabs, the casting process continues. One day while Clark Gable and Myron Selznick are hunting, Gable mentions that he is being considered for the role of Rhett Butler. Fleming agrees that Gable would be an appropriate choice but Gable is uncertain about accepting the role because the film is to be directed by George Cukor, a womans director. Gable first tries to withdraw from the very idea but later goes forth with the role after Louis B. Mayer threatens him with a suspension. It isnt long before Gables love interest Carole Lombard is considered for the part.

One night at the Selznick lot, a party is thrown to honor the actresses who are closest to winning the role of Scarlett and entertaining such stars as Joan Bennett, Margaret Sullavan, Jean Arthur, and Miriam Hopkins. Tallulah Bankhead is there, too, sitting at the table saying to herself, "Oh God, when will it ever stop?". While at this party, George Cukor is talking with the actresses, seeing if they would be interested to star in his new film The Women (1939 film) after Gone With The Wind is filmed. When Cukor asks Bankhead if she would entertain appearing in The Women, her response is one of disbelief - why would any actress appear in a movie with "No men; at all?"  When it comes time to have dinner at the party, Tallulah and Carole, who are sitting with Selznick between them, decide to get back at the producer for putting them through this acting contest. They stand up to make an announcement, pour their soup bowls onto his head, and declare, "Frankly my dear we dont give a damn". Laughter ensues.

Later in 1938 Selznick has still not decided who hell have as his Scarlett after Paulette Goddard is denied the part for failing to verify whether she is married to Charlie Chaplin. As the burning of Atlanta scene is to begin for a test sequence, his brother Myron Selznick arrives with a new actress. When he directs Selznick to look at her, David first refuses but after more badgering finally submits. When the young actress removes her hat, he sees the beautiful Vivien Leigh and informs her that she is his Scarlett. The rest is film history.

== Cast ==
* Tony Curtis... David O. Selznick
* Bill Macy... Myron Selznick
* Harold Gould... Louis B. Mayer
* Sharon Gless... Carole Lombard
* George Furth... George Cukor Edward Winter... Clark Gable
* Barrie Youngfellow... Joan Crawford
* Carrie Nye... Tallulah Bankhead
* Clive Revill... Charlie Chaplin
* Gwen Humble... Paulette Goddard
* Jane Kean... Louella Parsons
* Marianne Taylor... Katharine Hepburn
* Gypsi DeYoung... Lucille Ball
* Vicki Belmonte... Jean Arthur
* Sheilah Wells... Miriam Hopkins
* Morgan Brittany... Vivien Leigh

== Reception, Filming Locations, and Additional Details ==
At the 1980 Emmy Awards, The Scarlett OHara War won two awards for make-up by Richard Blair and costume design by Travilla.  It was nominated in five categories; Outstanding Director in a Limited Series or Special, Outstanding Lead Actor in a Limited Series or Special (Tony Curtis), Outstanding Limited Series, Outstanding Supporting Actor in a Limited Series (Harold Gould), and Outstanding Actress in a Limited Series (Carrie Nye).  At the Golden Globe Awards in 1981 the movie was nominated for Best TV Series in the Drama category. 
 John Gilbert.   The distribution company for this film was by Warner Bros. Television. 
 starlet who auditions for the role of Scarlett.

The complete film is one of the bonus features on the Blu-ray Bonus disc (disc 2) included with the 70th Anniversary Limited Edition of "Gone With the Wind" from Warner Home Video.

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 