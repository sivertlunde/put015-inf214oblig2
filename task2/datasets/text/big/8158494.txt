Who's Minding the Mint?
{{Infobox film
|  name           = Whos Minding the Mint?
|  image          = Whos_Minding_the_Mint_1967.jpg
|  caption        = 1967 movie poster
|  director       = Howard Morris
|  producer       = Norman Maurer Harvey Bullock 
|  starring       = Jim Hutton Dorothy Provine Milton Berle  Joey Bishop  Bob Denver  Walter Brennan Victor Buono  Jack Gilford
|  music          = Lalo Schifrin
|  cinematography = Joseph F. Biroc
|  editing        = Adrienne Fazan
|  distributor    = Columbia Pictures (1967) (USA) (theatrical) GoodTimes Home Video (video/VHS)
|  released       = September 26, 1967 (US)
|  runtime        = 97 min. USA
| English
|  budget         =
|}}
 caper film. Harvey Bullock. The movie was produced by Norman Maurer for Columbia Pictures.
 the US Treasury. The term "mint" as a verb can be applied to both coins as well as paper currency.

==Synopsis==
Harry Lucas (Jim Hutton) works at the Bureau of Engraving and Printing in Washington, D.C.  He has an admirer in sweet co-worker Verna Baxter (Dorothy Provine), who tries to woo him by giving him home-cooked (yet famously inedible) fudge, but he avoids her as he doesnt feel ready for anything serious. He also has an enemy in co-worker and supervisor Samson Link (David J. Stewart), who cant understand how Harry manages to live beyond his means. Unbeknownst to Link, Harry relies  on free trials that enable him to take luxury apartments and ride in chauffeured cars, enjoying the good life, including romance with a sexy neighbor. 
 garbage disposal. Realizing what he has done, he now fears Link and an audit at the mint.

In desperation, Harry turns to Pop (Walter Brennan), a former mint employee forced into retirement just before getting the chance to operate the presses he always maintained - and now has little to live for except the company of his pregnant beagle, Inky. Pop agrees to help Harry sneak into the mint after hours and print up replacement currency.

Harry learns that they will both need help from others to break in and print the missing $50,000. One by one, he has to offer a partnership to a safecracker named Dugan (Jack Gilford); Luther (Milton Berle), a pawnbroker who can front expenses; Ralph (Joey Bishop), a public works employee who can navigate a secret passage to the mint through D.C.s sewer system; a boat Captain (Victor Buono) who can create a boat that can fit down a manhole, and an ice cream truck driver named Willie (Bob Denver) who has the means to distract the one resident on the street whose apartment overlooks the manhole. Harry ultimately winds up asking Verna to help once Pop reminds him that a professional cutter will be needed to cut the printed sheets of bills. To his surprise, she agrees to help.

Unknown to Verna, however, the other conspirators accept an offer of $2,000 apiece at first, but as they rehearse for the big night, they decide to help Harry only on the condition that he and Pop will print them a million dollars apiece.

An unexpected change at the mint forces the timing of the caper to be moved up, the group has to drop what they are doing and go in immediately. Despite the rehearsals, many things go wrong during the job, not the least of which is Ralph bringing along his straight-off-the-boat Italian cousin Mario (Jamie Farr), and Pops dog Inky going into labor. Verna is also upset when she discovers that far more money is being printed, as Harry had assured her they were only going to replace the missing $50,000.

After several setbacks, the group manages to leave with the money - over seven million dollars in all - only to have it later lost when Mario mistakenly allows uniformed garbage collectors (whom he mistakes for police) to haul away the cardboard boxes containing the bills, placing them on a barge to be dumped into the ocean. 
 scuba diving equipment.

==Production notes==
The score was composed by Lalo Schifrin. 
 US bank notes which were still in use for official transactions at the time, starting with a United States one-dollar bill|$1 bill and ending with a Large denominations of United States currency #$100,000 bill|$100,000 bill. 

Over $1,000,000 of real US currency was used in the movie but was carefully watched by armed guards.  Most of the money shown being printed was 1 and 1/2 times larger than actual US currency and had obvious printing errors so there was no chance the money could be passed as genuine. 
 Dell published a 12-cent comic book version of this movie as a tie-in.

This film was originally available on a long out-of-print VHS.  As of July 2012, Amazon.com offers the film through a press-on-demand DVD-R.

==Cast==
* Jim Hutton as Harry Lucas
* Dorothy Provine as Verna Baxter
* Walter Brennan as Pop Gillis
* Milton Berle as Luther Burton
* Joey Bishop as Ralph Randazzo
* Bob Denver as Willie Owens
* Jack Gilford as Avery Dugan
* Victor Buono as the Captain
* Jamie Farr as Mario
* David J. Stewart as Samson Link
* Jackie Joseph as Imogene Harris
* Peanuts as Inky

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 