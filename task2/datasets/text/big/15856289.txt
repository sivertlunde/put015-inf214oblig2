The Master of Ballantrae (film)
 
 
{{Infobox film
| name           = The Master of Ballantrae
| image          = The Master of Ballantrae (film) poster.jpg
| image_size     =
| caption        =
| director       = William Keighley
| producer       =
| writer         = Herb Meadow Harold Medford (add. dialogue)
| based on       =  
| narrator       =
| starring       = Errol Flynn Roger Livesey
| music          = William Alwyn
| cinematography = Jack Cardiff Jack Harris
| distributor    = Warner Bros.
| released       = 5 August 1953 (USA)
| runtime        = 90 minutes
| country        = United Kingdom English
| budget         =
| gross          = $2 million (US rentals)  1,814,822 admissions (France) 
}}
 novel of the same title. In eighteenth century Scotland, two sons of a laird clash over the family estate and a lady. 

==Plot== Anthony Steel) Jacobite rising. King George II, so that whichever side wins, the familys status and estate will be preserved. Both brothers want to go. Jamie insists on tossing a coin for the privilege and wins, despite the opposition of his fiancée, Lady Alison (Beatrice Campbell).
 Irish adventurer, Colonel Francis Burke (Roger Livesey). They return secretly to Durrisdeer to obtain money for passage to France. 

When Jamies commoner mistress, Jessie Brown (Yvonne Furneaux), sees him kissing Lady Alison, she betrays him to the British. Jamie is shot and falls into the sea. Henry becomes the heir to the estate on the presumption that Jamie is dead. Believing his brother betrayed him, a wounded Jamie and Burke take ship with smugglers to the West Indies, where they are captured by pirates led by French dandy Captain Arnaud (Jacques Berthier).

Jamie goes into partnership with Arnaud. When they reach the port of Tortugas Bay, they see a rich Spanish galleon captured by fellow buccaneer Captain Mendoza (Charles Goldner). Arnaud agrees to Jamies proposal that they steal the ship. However, once they have seized the galleon, Arnaud turns on Jamie. Jamie kills Arnaud in a sword duel and takes command. They sail for Scotland.

Jamie returns to the family estate, rich with pirate treasure, to find a celebration in progress for Henrys betrothal to Alison. Unable to contain himself, Jamie confronts his brother, despite the presence of British officers. A fight breaks out, in which Henry tries to aid Jamie. The unequal fight ends with Jamie and Burke condemned to death. Jessie helps them escape, at the cost of her own life. Alison elects to go with Jamie to an uncertain future.

==Cast==
* Errol Flynn as Jamie Durie
* Roger Livesey as Colonel Francis Burke Anthony Steel as Henry Durie
* Beatrice Campbell as Lady Alison
* Yvonne Furneaux as Jessie Brown
* Felix Aylmer as Lord Durrisdeer
* Mervyn Johns as MacKellar
* Charles Goldner as Captain Mendoza
* Ralph Truman as Major Clarendon
* Francis de Wolff as Matthew Bull, Arnauds Quarter Master
* Jacques Berthier as Captain Arnaud
* Gillian Lynne as Marianne, a dancer favored by Mendoza

==Production==
Warner Bros announced on 7 September 1950 that they would make the film, with shooting to take place in England. WALD, KRASNA BUY ZOLA STORY RIGHTS: R.K.O. Producers to Do New Film on The Human Beast --Mature Injured on Set
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   08 Sep 1950: 25.   The following year it was announced that Joe Gottesman would be producer and Herb Meadow was doing the adaptation. Drama: Hollywoods Invasion of Europe Spreading; Lanza Start Slated
Schallert, Edwin. Los Angeles Times (1923-Current File)   11 Oct 1951: B9.  

In 1952 it was announced that Errol Flynn would star and the film would be known as The Sea Rogue.  FLYNN WILL PLAY PIRATE IN ENGLAND: To Be Starred in Sea Rogue, Based on Stevenson Story, for Warners in Summer
By THOMAS M. PRYORSpecial to THE NEW YORK TIMES.. New York Times (1923-Current file)   07 Apr 1952: 20.   Anthony Steel, who had impressed in some British films, was signed to play his brother. Drama: Anthony Steel Enacts Brother in Ballantrae; Bobby Van Speeds Along
Schallert, Edwin. Los Angeles Times (1923-Current File)   05 July 1952: A7.  

The film was shot in England in 1952, with location work in Cornwall and the Scottish Highlands with the pirate sequences done in Palermo in Sicily. Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 194  MOVIELAND BRIEFS.
Los Angeles Times (1923-Current File)   06 Oct 1952: B8.  Shooting took place six days a week. FILMLAND BRIEFS
Los Angeles Times (1923-Current File)   14 July 1952: B8.  

==Reception==
The New York Times called it Flynns best swashbuckler since The Sea Hawk. Master of Ballantrae at Paramount
H. H. T.. New York Times (1923-Current file)   6 Aug 1953: 16.   "Flynn himself hasnt been served better in years," wrote the Los Angeles Times. Flynns Ballantrae Has Real Scotch Kick
Scheuer, Philip K. Los Angeles Times (1923-Current File)   6 Aug 1953: B9.  

The Washington Post called the film "a chaotic tale deserving of his   undisputed prowess." A Nifty Chemise And Errol Flynn
By Richard L. Coe. The Washington Post (1923-1954)   31 July 1953: 39. 

It was the last film Flynn made under contract to Warner Bros., ending an association that had lasted for 18 years and 35 films. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 