Get Your Man (1927 film)
 
{{Infobox film
| name           = Get Your Man
| image          =
| caption        =
| director       = Dorothy Arzner
| producer       = Adolph Zukor Jesse Lasky  B. P. Schulberg(associate producer)
| writer         = Hope Loring(scenario) Agnes Brand Leahy(continuity)
| based on       =  
| starring       = Clara Bow
| music          =
| cinematography = Alfred Gilks
| editing        = Louis D. Lighton
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Paramount Famous Lasky Corporation, and released in 1927.  The film was directed by Dorothy Arzner and stars Clara Bow, Charles Buddy Rogers and Josef Swickard. The Library of Congress holds an incomplete print of this film, missing two out of six reel#Motion picture terminology|reels.   

==Cast==
*Clara Bow - Nancy Worthington
*Charles "Buddy" Rogers - Robert de Bellecontre
*Josef Swickard - Duc de Bellecontre
*Josephine Dunn - Simone de Villeneuve Harvey Clark - Marquis de Villeneuve
*Frances Raymond - Mrs. Worthington

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 
 