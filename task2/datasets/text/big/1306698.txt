Island in the Sun (film)
{{Infobox film
| name           = Island in the Sun
| image          = Island in the Sun 1957.jpg
| image_size     =
| caption        = Film poster by Jock Hinchliffe
| director       = Robert Rossen
| producer       = Darryl F. Zanuck
| based on       =   Alfred Hayes
| starring       = James Mason Harry Belafonte Joan Fontaine Joan Collins Dorothy Dandridge Michael Rennie 
| music          = Malcolm Arnold
| cinematography = Freddie Young
| editing        = Reginald Beck
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 119 mins
| country        = United States English
| budget         = $2,250,000 
| gross          = $5,056,396 (US rentals) 
}}
 Patricia Owens, novel by Alec Waugh. The film was controversial at the time of its release.

== Plot == Patricia Owens) and is envious of his younger sister Jocelyn (Joan Collins), who is being courted by the Oxford-bound Euan Templeton (Stephen Boyd), a war hero visiting the Governor of the island, his father Lord Templeton (Ronald Squire).

David Boyeur (Harry Belafonte), a young black man emerging as a powerful politician, represents the common people and is seen by some as a threat to the white ruling class. Mavis Norman (Joan Fontaine), a woman from the elite white class, strikes up a romantic interest in Boyeur and much of the story explores the tension between these two.

There is also an interracial romance between Margot Seaton (Dorothy Dandridge), a mixed-race drugstore clerk, and Denis Archer (John Justin), aide to the Governor.
 John Williams), the head of police, investigates the murder.

A journalist named Bradshaw (Hartley Power) writes an expose revealing that Maxwells grandmother was part Black. Maxwell has decided to run for the legislature, but is jeered by the crowd, then insults everyone there.

Jocelyn learns she is pregnant, but does not wish to burden Euan with a child of mixed race. Her mother reveals that Jocelyns father was actually a white man, the result of an undisclosed affair. She and Euan board a plane to England, as do Margot and Denis, to begin new lives.

Maxwell, a broken man, contemplates suicide, then decides to go to Whittingham to confess. Mavis wishes to marry Boyeur and begin a new life of her own, but he decides the needs of the island and his people must come first.

==Characters==
* James Mason as Maxwell Fleury, the familys son, who is a successful, but hot-tempered plantation owner.
* Harry Belafonte as David Boyeur, a black politician representing the common people, who falls in love with Mavis Norman.
* Joan Fontaine as Mavis Norman, an upper class white woman, whos a member of the richest family on the island; also the lover of David Boyeur.
* Joan Collins as Jocelyn Fleury, the alluring sister of Maxwell, whos the romancer of Euan Templeton.
* Dorothy Dandridge as Margot Seaton, the beautiful Indian drug store clerk, who is pursued by governor-aide, Denis Archer.
* Michael Rennie as Hilary Carson, a retired war hero, who is assumed to be having an affair with Sylvia Fleury. Patricia Owens as Sylvia Fleury, the wife of Maxwell, who Maxwell assumes is having an affair with Hilary Carson.
* John Justin as Denis Archer, the governors aide, and the romancer of Margot Seaton.
* Stephen Boyd as Euan Templeton, the governors young son, whos also courting Jocelyn Fleury.
* Diana Wynyard as Mrs. Fleury, the mother of Maxwell and Jocelyn, whos hiding things from her children.
* Basil Sydney as Julian Fleury, the husband of Mrs. Fleury and father of Maxwell and Jocelyn, who also is hiding things. John Williams as Colonel Whittingham, the head of police, who investigates Hilary Carsons murder.
* Ronald Squire as Governor Templeton, the governor of the island, and Euans father.
* Hartley Power as Bradshaw, an American journalist visiting Santa Marta.

== Production ==
 
 
A proposal was floated in 2009 to demolish the remains of the real mansion used in the film.   The mansion is located in Farley Hill, Barbados.  The mansion was gutted by fire just after the filming of the movie and all that remains are the foundations and exterior walls of the building. 
 Carmen Jones, The King and I and The Lieutenant Wore Skirts but Otto Preminger, her lover and Carmen Jones  director,  advised her to turn down the roles. This was Dandridges first film role in three years: she was third billed, but appeared in only a supporting role. 

== Reception ==
Premiering in June 1957, Island in the Sun was a major box office success. The film earned $5,550,000 worldwide, and finished as the sixth highest-grossing film of 1957.

It was the 8th most popular movie in Britain of that year. 

== Music ==
The title song "Island in the Sun" was written by Harry Belafonte and Irving Burgie.  There are now over 40 cover versions recorded by various artist such as The Merrymen, José Carreras, Caterina Valente in German, Henri Salvador in French ("Une île au soleil") and The Righteous Brothers, just to name a few.

==References==
 

== External links ==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 