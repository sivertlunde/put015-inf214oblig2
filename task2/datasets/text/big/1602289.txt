Make Mine Music
{{Infobox film
| name = Make Mine Music
| image = Make mine music poster.png
| caption = Original theatrical release poster
| director = {{Plain list|
*Jack Kinney
*Clyde Geronimi
*Hamilton Luske
*Joshua Meador
*Robert Cormack}}
| producer = Walt Disney
| writer = {{Plain list|
*Walt Disney
*James Bordrero
*Homer Brightman
*Erwin Graham
*Eric Gurney
*T. Hee
*Sylvia Holland
*Dick Huemer
*Dick Kelsey
*Jesse Marsh
*Tom Oreb
*Cap Palmer
*Erdman Penner Harry Reeves
*Dick Shaw
*John Walbridge Roy Williams}}
| starring = {{Plain list|
*Nelson Eddy
*Dinah Shore
*Benny Goodman
*The Andrews Sisters Jerry Colonna
*Sterling Holloway Andy Russell
*David Lichine Tania Riabouchinskaya
*The Pied Pipers
*The Kings Men
*The Ken Darby Chorus}} Walt Disney Productions RKO Radio Pictures, Inc.
| released =   |ref2= }}
| runtime = 76 minutes
| country = United States
| language = English
| budget =
}}
 theatres on Walt Disney Animated Classics series.
 Second World studio was littered with unfinished story ideas. In order to keep the feature film division alive during this difficult time, the studio released six package films including this one, made up of various unrelated segments set to music. This is the third package film, following Saludos Amigos and The Three Caballeros. It received mixed to positive reviews, though its first segment, The Martins and the Coys, was panned by critics due to its overuse of violence. The film was entered into the 1946 Cannes Film Festival.   

The musical director was Al Sack. 

==Film segments==
This particular film has ten such segments.

===The Martins and the Coys=== vocal group, Hatfields and McCoys-style feud in the mountains broken up when two young people from each side fell in love. It was edited out in the NTSC home media version, while in the PAL version, it was kept.

===Blue Bayou===
This segment featured animation originally intended for Fantasia (1940 film)|Fantasia using the Claude Debussy musical composition Clair de Lune from Suite bergamasque. It featured two egrets flying through the Everglades on a moonlit night. However, by the time Make Mine Music was released Clair de Lune was replaced by the new song Blue Bayou, performed by the Ken Darby Singers. However, the original version of the segment still survives.

===All the Cats Join In=== teens were swept away by popular music.

===Without You=== Andy Russell.

===Casey at the Bat=== Jerry Colonna, reciting the poem also titled "Casey at the Bat" by Ernest Thayer, about the arrogant ballplayer whose cockiness was his undoing.

===Two Silhouettes=== ballet dancers, Tania Riabouchinskaya, moving in silhouette with animated backgrounds and characters. Dinah Shore sang the title song.

 
 Peter and the Wolf=== horns and cymbals.

===After Youve Gone=== Octet as anthropomorphized instruments (Piano, Double Bass|Bass, Drums, Clarinet, Trumpet, Trombone, Alto Sax, Tenor Sax) who paraded through a musical playground.

===Johnnie Fedora and Alice Bluebonnet=== romantic story of two hats who fell in love in a department store window. When Alice was sold, Johnnie devoted himself to finding her again. They eventually, by pure chance, meet up again and live happily ever after together, side by side. The Andrews Sisters provided the vocals. Like the other segments, it was later released theatrically. It was released as such on May 21, 1954. 

===The Whale Who Wanted to Sing at the Met=== impresario Tetti-Tatti believes this and sets out to destroy Willy, the newspapers announcing that he was going to see. Whitey, Willys seagull friend, excitedly brings Willy the newspaper, all of his friends believing that this is his big chance, so he goes out to meet the boat and sing for Tetti-Tatti. He finds them, and upon hearing Willy sing, Tetti-Tatti comes to believe that Willy has swallowed not one, but THREE singers (due to his having three uvulae), and chases him with a harpoon on a boat with three crewmen. Upon hearing the whale sing, the crewmen try to stop Tetti-Tatti from killing the whale, as they want to continue listening to him sing, even to the point of tying up Tetti-Tatti and sitting on him, however he still manages to escape and fire the harpoon gun. In the end, Willie was harpooned and killed, but the narrator then explains that Willys voice will sing on in Heaven.
Nelson Eddy narrated and performed all the voices in this segment. As Willie the Whale, Eddy sang all three male voices in the first part of the Sextet from Gaetano Donizetti|Donizettis opera, Lucia di Lammermoor.

==Cast==
{| class="wikitable"
|-
! Actor !! Role(s)
|-
| Nelson Eddy || Narrator; characters (The Whale Who Wanted to Sing at the Met)
|-
| Dinah Shore || Singer (Two Silhouettes)
|-
| Benny Goodman || Musician (All the Cats Join In/After Youve Gone)
|-
| The Andrews Sisters || Singers (Johnnie Fedora and Alice Bluebonnet)
|- Jerry Colonna || Narrator (Casey at the Bat)
|-
| Sterling Holloway || Narrator (Peter and the Wolf)
|- Andy Russell || Singer (Without You)
|-
| David Lichine || Dancer (Two Silhouettes)
|- Tania Riabouchinskaya || Dancer (Two Silhouettes)
|-
| The Pied Pipers || Singers
|-
| The Kings Men || Singers (The Martins and the Coys)
|-
| The Ken Darby Chorus || Singers (Blue Bayou)
|}

==Theatrical and Home Video Releases== Disney television programmes.

Make Mine Music was originally released on Laserdisc in Japan on October 21, 1985. It was released on VHS and DVD in the US on June 6, 2000 under the Walt Disney Gold Classic Collection line. This release did not include the segment "The Martins and the Coys", because it had "graphic gunplay not suitable for children."  In addition, "All the Cats Join In" was edited to remove the breasts on the girl who emerges from the shower. The Japanese Laserdisc includes none of the edits made to the VHS/DVD versions. No unedited release has been scheduled in America. An unedited version of the film was released on DVD in the United Kingdom in 2013, having been previously unavailable until that point.

Outside of North America, Make Mine Music has been largely unavailable on DVD and VHS. This and The Adventures of Ichabod and Mr. Toad are the only two films in the Disney Animated Classics canon never to see a release on  Region 4 DVD in Australia. Also it has never been released on digital DVD in Italy making the only Disney Canon Classic to be unavailable for Italian market.

* August 11, 2015 (Blu-ray/DVD Combo Pack - Special Edition Double Feature)

==See also==
*List of animated feature films

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 