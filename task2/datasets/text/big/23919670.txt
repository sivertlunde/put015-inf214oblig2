Cold Moon
 
{{Infobox film
| name           = Cold Moon
| caption        = A poster with the films French title: Lune froide
| image	         = Cold Moon FilmPoster.jpeg
| director       = Patrick Bouchitey
| producer       = Luc Besson Andrée Martinez
| writer         = Jackie Berroyer Patrick Bouchitey Charles Bukowski
| starring       = Jean-François Stévenin
| music          = 
| cinematography = Jean-Jacques Bouhon
| editing        = Florence Bon
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $1.4 million 
}}

Cold Moon ( ) is a 1991 French drama film directed by Patrick Bouchitey. It was entered into the 1991 Cannes Film Festival.    It is based on the Charles Bukowski short stories "The Copulating Mermaid of Venice" and "Trouble with the Battery".

==Cast==
* Jean-François Stévenin as Simon
* Patrick Bouchitey as Dédé
* Jean-Pierre Bisson as Gérard, le beau-frère de Dédé
* Jackie Berroyer as The monk
* Consuelo De Haviland as La blonde / Blonde girl
* Laura Favali as Nadine, la soeur de Dédé
* Silvana de Faria as La prostituée / Whore
* Karine Nuris as La sirène
* Roland Blanche as Laccoudé
* Jean-Pierre Castaldi as Félix, le patron du bistrot
* Dominique Maurin as Le vagabond (as Dominique Collignon Maurin)
* Bernard Crombey as Le boucher
* Patrick Fierry as Jean-Loup
* Anne Macina as La femme de la voiture
* Marie Mergey as Suzanne, la tante de Simon / Aunt Suzanne

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 