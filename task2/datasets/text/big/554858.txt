Four Rooms
 
{{Infobox film
| name           = Four Rooms
| image          = Four rooms ver2.jpg
| caption        = Theatrical release poster
| director       = Allison Anders Alexandre Rockwell Robert Rodriguez Quentin Tarantino
| producer       = Lawrence Bender
| writer         = Allison Anders Alexandre Rockwell Robert Rodriguez Quentin Tarantino Madonna David Proval Ione Skye Lili Taylor Kathy Griffin Marisa Tomei Tamlyn Tomita  Esquivel
| Rodrigo García Guillermo Navarro Phil Parmet Andrzej Sekuła
| editing        = Margie Goodspeed Elena Maganini Sally Menke Robert Rodriguez
| studio         = A Band Apart
| distributor    = Miramax Films
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       = English
| budget         = $4 million
| gross          = $4,257,354
}} anthology comedy film directed by Allison Anders, Alexandre Rockwell, Robert Rodriguez, and Quentin Tarantino, each directing one segment of the film that in its entirety is loosely based on the adult short fiction writings of Roald Dahl, especially Man from the South which is the basis for the last segment, Penthouse - "The Man from Hollywood" directed by Tarantino. The story is set in the fictional Hotel Mon Signor in Los Angeles on New Years Eve. Tim Roth plays the hotel bellhop, the main character in the frame story, whose first night on the job consists of four very different encounters with various hotel guests.

==Plot==
The film is set on New Years Eve, and starts with Sam (Marc Lawrence), the previous bellhop of the Hotel Mon Signor, briefing his replacement, Ted (Tim Roth), about the job.
 scat song "Vertigogo".

===Honeymoon Suite - "The Missing Ingredient"===
*Written and directed by Allison Anders

Ted assists a number of unusual women with their luggage, which he takes up to the Honeymoon Suite. He learns they are a coven of witches, attempting to create a potion to reverse a spell cast on their goddess, Diana (Amanda De Cadenet) 40 years ago. In order to create the potion, each witch must place an ingredient into a large cauldron in a ritual. However, one of the witches (Ione Skye) has still to retrieve her ingredient - semen - which she is told she must retrieve in one hour. The witch manages to seduce an initially reluctant Ted and has sex with him in the cauldron. After he leaves, the witches complete the ritual and Diana is seen emerging from the cauldron.

====Teds phone call with the party guest====
At the end of the segment, a guest from a hotel room party (Lawrence Bender) calls Ted at the front desk to get some ice. He is unsure about which floor the room is on, but eventually directs Ted to room 404.

===Room 404 - "The Wrong Man"===
*Written and directed by Alexandre Rockwell

Upon arriving at room 404, Ted finds himself in the middle of a fantasy hostage situation between a husband and wife. Sigfried (David Proval), the husband, maniacally accuses Ted (whom he calls Theodore) of having slept with his wife, Angela (Jennifer Beals). At gunpoint, Ted is made to participate in the scenario, with uncertainty about what is real and what is part of the fantasy. At one point, Ted is stuck in the bathroom window and the party guest from the beginning of the episode appears in the window above, uttering the word "ice" and vomiting. Eventually, Ted escapes just as a different party guest (Paul Skemp) appears, looking for room 404 and is greeted by Sigfried in the same manner as Ted was at the beginning of the episode.

====Ambiguity of room number====
It is not fully clear if these events take place in room 404 or in room 409. The party guests room is on the above floor, which suggests Sigfrieds room is indeed room 404. Adding to this uncertainty, the room door reads "40-", with a faint outline of what appears to be a missing "4" or "9". However, Sigfried answers the phone at one point, which is later revealed to be a call connecting to room 409.

===Room 309 - "The Misbehavers"===
*Written and directed by Robert Rodriguez

A husband ( , and the children have found the corpse of a dead prostitute (Patricia Vonne) stuffed under the mattress. While Ted tries to quell the chaos in the room, Sarah stabs him in the leg with the syringe when he repeatedly uses the word "whore" and Juancho accidentally sets the bedroom on fire with his cigarette. At this point, the childrens father arrives, carrying his passed-out wife, and, looking around the room, asks Ted, "Did they misbehave?"

====Teds phone call with Betty====
After the events of room 309, an unsettled Ted calls his boss, Betty (Kathy Griffin), to quit for the night, as his shift has ended.  After a prolonged conversation with Margaret (Marisa Tomei), Ted gets Betty on the phone and quits, but a call from the Penthouse comes in.  Betty reasons with Ted and convinces him to tend to their needs, due to the importance of continued Hollywood business to the hotel.

===Penthouse - "The Man from Hollywood"===
*Written and directed by Quentin Tarantino

The penthouse is currently being occupied by the famous director Chester Rush (Tarantino) and a group of his friends, which includes Angela from The Wrong Man. The party requests a block of wood, a doughnut, a ball of twine, three nails, a club sandwich, a bucket of ice, and an extremely sharp hatchet (Rush specifically requests a hatchet "as sharp as the Devil himself"). After getting acquainted with Chester and his friends, Ted is asked to take part in a challenge: Chesters friend Norman (Paul Calderón) has bet he can light his Zippo cigarette lighter ten times in a row. If he succeeds, Norman will win Chesters car, but if he fails, Normans pinky will be cut off. Ted is asked to "wield the hatchet" and cut off Normans pinky, should he fail. Ted initially tries to leave, but Chester persuades him to stay by offering $100 up front and another $1,000 if he performs his assigned role. Normans lighter fails on the first try, and Ted chops off the finger, sweeps up all the money, and leaves the penthouse. While the credits are rolling, Chester and company are seen frantically getting ready to take a screaming, agonizing Norman to a hospital. Bruce Willis also appears prominently in this scene, yet is not credited in the movie.

==Cast==

*Tim Roth as Ted the Bellhop

==="The Missing Ingredient"===
*Valeria Golino as Athena Madonna as Elspeth
*Alicia Witt as Kiva
*Sammi Davis as Jezebel
*Lili Taylor as Raven
*Ione Skye as Eva
*Amanda de Cadenet as Diana

==="The Wrong Man"===
*David Proval as Siegfried
*Jennifer Beals as Angela
*Lawrence Bender as Long Hair Yuppie Scum
*Paul Skemp as Real Theodore
*Quinn Thomas Hellerman as Baby Bellhop

==="The Misbehavers"===
*Antonio Banderas as Man
*Tamlyn Tomita as Wife
*Lana McKissack as Sarah
*Danny Verduzco as Juancho
*Patricia Vonne as Corpse
*Salma Hayek as TV dancing girl

===Bettys house===
*Kathy Griffin as Betty
*Marisa Tomei as Margaret
*Julie McClean as Left Redhead
*Laura Rush as Right Redhead

==="The Man from Hollywood"===
*Quentin Tarantino as Chester Rush
*Jennifer Beals as Angela
*Paul Calderón as Norman
*Bruce Willis as Leo (uncredited)
*Kimberly Blair as Hooker (uncredited)

==Production== 
  
The role of Ted was originally written with Steve Buscemi in mind, but he had to pass on the part due to scheduling conflicts. Buscemis Reservoir Dogs co-star Tim Roth eventually took his place.

The film was originally going to be called Five Rooms, with Richard Linklater contributing a segment, but he eventually withdrew before production began.

The reason Bruce Willis was not credited was because he violated Screen Actors Guild rules for acting in this film for no money. He appeared for fun and did it as a favor for Quentin Tarantino, and acting for free violated SAG rules. SAG agreed not to sue Willis if his name was not included in the credits.

==Crossovers between rooms==
The four segments are shown chronologically, except for "The Misbehavers", the events of which both precede and succeed the events of "The Wrong Man".

There are some connections between the four segments:
*In "The Wrong Man", Ted recalls the witches ritual in "The Missing Ingredient" with the expression "weird voodoo thing".
*Ted can be seen with the two cherries from "The Missing Ingredient" at the beginning of "The Misbehavers".
*Sarah in "The Misbehavers" calls a random room to ask a question. The man who picks up happens to be Siegfried from "The Wrong Man".
*Angela appears in both "The Wrong Man" and "The Man from Hollywood".
*When calling his boss, just before the beginning of "The Man from Hollywood", Ted recalls the events of the first three segments.

==Reception==

===Critical reception===
The film did not fare as well with critics as it received a 14% "Rotten" rating from Rotten Tomatoes.  James Berardinelli of ReelViews described the film as "one of 1995s major disappointments".  Hal Hinson of the Washington Post said the film "asserts itself as a goof so laboriously and aggressively that you almost feel pinned back in your seat".  Most reviews agree that "The Misbehavers" is the best of the segments. Tim Roth was praised by critics and audiences for his performance as Ted.

The film won a Razzie Award for Worst Supporting Actress (Madonna (entertainer)|Madonna). 

===Box office===
The film grossed $4,257,354 in only 319 theaters. 

==Soundtrack==
{{Infobox album  
| Name        = Four Rooms: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = various artists
| Cover       =
| Released    = 1995
| Recorded    =
| Genre       = Lounge music, soundtrack
| Length      = 49.20 Asylum
| Producer    = Mark Mothersbaugh Carl Plaster Combustible Edison
| Chronology  = Robert Rodriguez film soundtrack
| Last album  =
| This album  = Four Rooms (1995)
| Next album  =   (1995)
| Misc = {{Extra chronology
  | Artist = Quentin Tarantino film soundtrack
  | Type  = Soundtrack Pulp Fiction (1994)
  | This album = Four Rooms (1995) From Dusk Till Dawn (1996)
  }}
}}
{{Album ratings
| rev1 = Allmusic
| rev1score =     
| noprose   = yes
}}
The soundtrack to Four Rooms features a score composed and performed by contemporary lounge music band Combustible Edison, co-produced by Mark Mothersbaugh. Additional music is by Juan García Esquivel.  

===Track listing=== Vertigogo (Opening Theme)" (Combustible Edison)&nbsp;– 2:35
#*Tracks 2-11 from "The Missing Ingredient":
#"Junglero"&nbsp;– 1:54
#"Four Rooms Swing"&nbsp;– 2:11 Jack Keller)&nbsp;– 1:01
#"Tea and Eva In The Elevator"&nbsp;– 0:55
#"Invocation"&nbsp;– 1:26
#"Breakfast At Dennys"&nbsp;– 3:57
#"Strange Brew"&nbsp;– 0:27
#"Coven Of Witches"&nbsp;– 0:59
#"The Earthly Diana"&nbsp;– 0:36
#"Eva Seduces Ted"&nbsp;– 2:10
#*Tracks 12-17 from "The Wrong Man":
#"Hallway Ted"&nbsp;– 0:31
#"Headshake Rhumba"&nbsp;– 0:41
#"Skippen, Pukin, Siegfried"&nbsp;– 0:29
#"Angela"&nbsp;– 0:46
#"Punch Drunk"&nbsp;– 2:57
#"Male Bonding"&nbsp;– 3:06
#*Tracks 18-25 from "The Misbehavers":
#"Mariachi"&nbsp;– 0:29
#"Antes De Medianoche"&nbsp;– 2:45 Sentimental Journey" Les Brown and Ben Homer, performed by Juan García Esquivel|Esquivel)&nbsp;– 2:39
#"Kids Watch TV"&nbsp;– 2:03
#"Champagne and Needles"&nbsp;– 2:06
#"Bullseye"&nbsp;– 1:01
#"Harlem Nocturne" (Written by Earle Hagen, performed by Juan García Esquivel|Esquivel)&nbsp;– 2:30
#"The Millionaires Holiday"&nbsp;– 2:13
#*Tracks 26-29 from "The Man from Hollywood":
#"Ted-o-vater"&nbsp;– 0:39
#"Vertigogo (Closing Credits)"&nbsp;– 5:33
#"D In The Hallway Credits"&nbsp;– 0:25
#"Torchy"&nbsp;– 0:16

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 