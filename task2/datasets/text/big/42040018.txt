Le crocodile du Botswanga
{{Infobox film
| name           = Le crocodile du Botswanga
| image          = Le crocodile du Botswanga.jpg
| alt            = 
| caption        = 
| director       = Lionel Steketee Fabrice Eboué
| producer       = Alain Goldman
| writer         = Fabrice Eboué
| starring       = Fabrice Eboué Thomas NGijol Ibrahim Koma Claudia Tagbo
| music          = Guillaume Roussel
| cinematography = Stéphane Le Parc
| editing        = Frédérique Olszak
| studio         = Légende Films
| distributor    = Mars Distribution
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = $9,500,000
| gross          = $18,983,067 
}}
Le crocodile du Botswanga (Crocodile Botswanga) is a 2014 French comedy film directed by Lionel Steketee and Fabrice Eboué.

==Cast==
* Fabrice Eboué – Didier
* Thomas NGijol – Capitaine Bobo
* Claudia Tagbo – Jacqueline
* Ibrahim Koma – Leslie
* Franck de la Personne – Pierre
* Etienne Chicot – Taucard
* Amelle Chahbi – Karina
* Issa Doumbia - Soldat Issa
* Eriq Ebouaney - Lieutenant Yaya
* Hélène Kuhn - Lea

==References==
 

==External links==
*  

 
 
 
 
 

 