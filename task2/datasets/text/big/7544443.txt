Tomorrow Is Forever
{{Infobox film
| name = Tomorrow Is Forever
| image = Tomorrow is forever.jpg
| caption = Theatrical release poster
| director = Irving Pichel
| writer = Gwen Bristow (novel)  Lenore J. Coffee Richard Long Natalie Wood David Lewis
| music = Max Steiner
| cinematography = Joseph A. Valentine
| editing = Ernest J. Nims
| distributor = RKO Pictures Independent Releasing Organization (1953, re-release)
| budget =
| released =  
| runtime = 105 minutes
| country = United States
| language = English
}}    
  1946 black-and-white Richard Long and the eight-year-old Natalie Wood in their first credited acting roles.

==Plot==

The movie tells the story of Elizabeth (Colbert) and John (Welles), a married couple recently separated when John goes off to fight in World War I. When Elizabeth receives notice of Johns death, she marries another man (George Brent). John, however, is still alive, and later returns, but after being disfigured in the war he has undergone plastic surgery, making him almost unrecognizable. He has also adopted an eight-year old daughter (Wood). When he finds out that he has a son with Elizabeth, he is faced with the dilemma of whether or not to reveal his true identity.

==Cast==
*Claudette Colbert as  Elizabeth Hamilton 
*Orson Welles as  John Andrew MacDonald/Erik Kessler 
*George Brent as  Lawrence Hamilton 
*Lucile Watson as  Aunt Jessica Hamilton  Richard Long as  Drew Hamilton 
*Natalie Wood as  Margaret Ludwig 
*John Wengraf as  Dr. Ludwig 
*Sonny Howe as  Brian Hamilton  Michael Ward as  The Drew infant 
*Ian Wolfe as  Norton 
*Joyce Mackenzie as  Cherry Davis 
*Tom Wirick as  Pudge Davis 
*Lane Watson as  Hamiltons secretary  Henry Hastings as  Daniel
*John Bowes as  Dr. Jenkins

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 