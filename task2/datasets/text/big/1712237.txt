Goin' Down the Road
{{Infobox film
| name = Goin Down the Road
| image = Goindowntheroadmp.jpg
| image_size =
| caption = Promotional movie poster for the film
| director = Donald Shebib
| producer = Donald Shebib
| writer = William Fruet Donald Shebib Paul Bradley Jayne Eastwood Cayle Chernin
| music = Bruce Cockburn
| cinematography = Richard Leiterman
| editing = Donald Shebib
| distributor = Chevron Pictures
| released =  
| runtime = 90 minutes
| country = Canada
| language = English CAD 87,000 USD 78,000 
}} Paul Bradley, Jayne Eastwood and Cayle Chernin. Despite the lack of a large production budget, the movie is generally regarded as one of the best and most influential Canadian films of all time and has received considerable critical acclaim for its writing, directing and acting.

==Plot summary==
Pete and Joey drive their 1960 Chevrolet Impala from their home on Cape Breton Island in Nova Scotia to Toronto with the hope of meeting up with their relatives in the city who might be able to help them find them jobs. But their relatives hide from what they see as the pairs uncouth behaviour, and the two are set adrift in the city. The men find minimum-wage jobs at $2 an hour for a 40-hour week, still much better pay than anything they could have found back home.

They soon turn their good fortune into residency in a small apartment. Both men start romances, and Joey decides to get married when his girlfriend, Betty (Jayne Eastwood), becomes pregnant. He pursues a credit-driven lifestyle undreamt of at home with his new wife, but the larger apartment and payments on the new stereo and television start to strain their finances. He becomes desperate as their childs birth approaches and the expenses continue to mount.

Disaster strikes when Pete and Joey get laid off at the end of the summer. Unable to find steady work and with bills to pay and a baby on the way, they resort to stealing food from supermarket, a plan which results in disaster. Following this the pair decide to pawn their colour TV set for money in order to make it out to the west coast, leaving Betty and her unborn child in Toronto.

==Cast==
*Doug McGrath as Peter McGraw Paul Bradley as Joey Mayle
*Jayne Eastwood as Betty
*Cayle Chernin as Selina
*Nicole Morin as Nicole
*Pierre La Roche as Frenchie La Roche
*Don Steinhouse as Plant Co-worker
*Ted Sugar as Plant Co-worker Ron Martin as Plant Co-worker

==Social relevance==
The film reflected an important social phenomenon in post-war Canada as the economy of the eastern provinces stagnated and many young men sought opportunities in the fast-growing economy of Ontario. Although the men in the film come from Nova Scotia, the "Newfie" as an unsophisticated manual labourer was a common stereotype starting in the early 1950s as many Atlantic Canadians moved to the cities looking for work, only to find widespread unemployment and jobs that may have seemed to have attractive salaries, but made living in large cities marginal at best. Many of Torontos early housing developments (particularly Regent Park) were built to handle the influx of internal immigrants before they were eventually replaced by external immigrants from Africa, the Caribbean and Asia starting in the 1960s.
 doctor (respectively) seeking a better life in Toronto after hearing about the job openings there. Eastwood reprised her role as the pregnant girlfriend, and Andrea Martin expanded the list of characters as a French-Canadian nuclear physicist who was also seeking better opportunities outside her native province of Quebec. As in the original, the men are entranced by the big city appeal of Yonge Street, a primary commercial thoroughfare in downtown Toronto. The parody ends on a happier note, with the characters leaving Toronto to seek better opportunities in Edmonton, Alberta|Edmonton.

==Production and significance== The Grapes of Wrath but it puts the story into the present, and the story itself is not dated – the flight from rural to urban areas continues throughout the world today.
 Quebec cinema also was influenced by the realistic look of Goin Down the Road, and many successful Quebec films based on real life experiences were also critical and often commercial successes. Other Canadian filmmakers have also taken advantage of the cost savings that realism can mean to a production (such as shooting on less expensive film stock).

This film has been designated and preserved as a "masterwork" by the Audio-Visual Preservation Trust of Canada, a charitable non-profit organization dedicated to promoting the preservation of Canada’s audio-visual heritage.  The Toronto International Film Festival ranked it in the Top 10 Canadian Films of All Time four times, in 1984, 1993, 2004 and 2015.  In 2002, readers of Playback (magazine)|Playback voted it the 5th greatest Canadian film of all-time. 

The then up-and-coming singer-songwriter Bruce Cockburn composed several songs for the film, including "Goin Down the Road" and "Another Victim of the Rainbow". Cockburn refused to release the songs commercially because they represented the experiences of the movies characters and not his own.  Director Shebib was introduced to Cockburn, who was then playing in coffee houses in Toronto, by journalist Alison Gordon.

Shebib subsequently directed the 1981 film Heartaches (film)|Heartaches, starring Margot Kidder, Annie Potts and Robert Carradine in a thematically similar story about two women.

In 2010, Shebib announced that a sequel film was in production.  . Torontoist, October 20, 2010.   Down the Road Again was released in October 2011.

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 