Achamillai Achamillai
{{Infobox film
| name = Achamillai Achamillai
| image =
| caption = Official DVD Box Cover
| director = K. Balachander
| producer = Rajam Balachander Pushpa Kandaswamy
| writer = K. Balachander (dialogue)
| story = K. Balachander
| screenplay = K. Balachander
| narrator =
| starring =   
| music = V. S. Narasimhan
| cinematography = B. S. Lokanath
| editing = N. R. Kittu
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released =  
| runtime = 160 minutes 
| country = India Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
}}
 1984 Indian Rajesh and 32nd Filmfare Awards South, as well as the National Film Award for Best Feature Film in Tamil.

== Plot == Independence Day. The couple lead a happy life and work for the welfare of the village. With elections around the corner, two political parties try to garner support by enlisting Ulaganathan to their side. Initially reluctant, Ulaganathan joins one of the parties when it promises to provide him with a ministerial position. 

Ulaganathan quickly slides down the slippery slope of political intrigue and intimidation, alienating both his wife and his father. Thenmozhi returns to her fathers home to deliver her first child. Ulaganthan wins the election, but as neither of the parties receives a clear majority, the rival party bribes Ulaganathan to join them instead. When Thenmozhi returns after the birth of their child, she is disappointed over the transformation of her husband and refuses to live with him. To spite her, Ulagnathan brings home Alangaram (Pavithra) as his mistress. Alangaram and her mother slowly take control over the household, which leads to Thenmozhi leaving her family. Ulagnathans atrocities go beyond her tolerance when he plans to organise a communal riot in the village in order to safeguard himself from the critics. When Thenmozhi finds out about this, she goes to have a talk with him. But Ulaganathan defends himself by saying that political murders are not ethically wrong. The following day (Independence Day), a statue of Mahatma Gandhi is to be unveiled by Ulaganathan. During the ceremony, Ulagnathan makes a speech on his and his partys accomplishments. Thenmozhi approaches him with a garland; as she lays it on his neck, she stabs him to death. The police arrest her; at the end, Suthanthiram (Delhi Nayakar) is seen crying at the feet of Gandhis statue. 

== Cast == Rajesh as Ulaganathan
* Saritha as Thenmozhi
* Delhi Ganesh
* Pavithra as Alangaram
* Ahalaya
* Prabhakar
* Vairam Krishnamoorthy
* Veeraiah
* Charle
* Delhi Nayakar as Suthanthiram

== Production, themes and analysis ==
The film was produced by Kavithalayaa Productions, Balachanders production house. Apart from direction, he took charge of the story, screenplay, and dialogue. Cinematography and editing were handled by B. S. Lokanath and N. R. Kittu, respectively. 
 Indian political system. Its underlying theme is Party switching, a common phenomenon that exists in the countrys political system, and its impact on small-time politicians.  Balachander added a dwarf character named Suthanthiram (literally, "Freedom") to metaphorically depict that the freedom of the nation is stunted. 

== Soundtrack ==
The films score and soundtrack were composed by debutante V. S. Narasimhan, while lyrics were written by Vairamuthu and "Erode" Thamizh Inban.  The soundtrack received positive response. 

{{Infobox album  
| Name        = Achamillai Achamillai
| Type        = soundtrack
| Artist      =
| Cover       =
| Caption     =
| Released    =    
| Recorded    =
| Genre       =
| Length      = Tamil
| Label       =
| Producer    =
| Reviews     =
| Compiler    =
| Misc        =
}}
{{tracklist
| collapsed       = 
| headline        = 
| extra_column    =  Singers
| total_length    =

| all_writing     = 
| all_lyrics      = Vairamuthu, Erode Thamizh Inban
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          =  Aavaram Poovu
| note1           =  
| writer1         =  
| lyrics1         =  
| music1          =  
| extra1          =  S. P. Balasubrahmanyam, P. Susheela
| length1         =

| title2          =  Karisal Karisu
| note2           =  
| writer2         =  
| lyrics2         =  
| music2          =  
| extra2          =  Vani Jairam
| length2         =

| title3          =  Kayiala kaasu
| note3           =  
| writer3         =  
| lyrics3         =  
| music3          =  
| extra3          =  S. P. Balasubrahmanyam
| length3         =
| title4          =  Odukira Thanniyila
| note4           =  
| writer4         =  
| lyrics4         =  
| music4          =  
| extra4          =  Malaysia Vasudevan, P. Susheela
| length4         =

| title5          =  Pudhiru Poda
| note5           =  
| writer5         =  
| lyrics5         =  
| music5          =  
| extra5          =  Malaysia Vasudevan, S. Janaki
| length5         =

}}

== Reception == 10th International Film Festival of India in 1985.    Sarithas performance won her a lot of accolades and critical praise.   

A contemporary review from  ", and lauded the director for his dialogue filled with sarcasm.  The film received negative reviews for showcasing violence. Some critics were concerned about the idea that even educated people would be corrupted if they come to politics, while a few others opined that the film did not not provide a solution for the problem. 

== Legacy ==
The film is widely regarded as one of the finest that Balachander has directed.  In a 2006 interview with The Hindu, Balachander listed the film as one of his favourites.  Analysing the dominance of female characters in Balachanders films, Baradwaj Rangan listed the heroine of Achamillai Achamillai as an example. 

== Awards ==
{| class="wikitable"
|-
! Year !! Award !! Category !! Nominee !! Outcome
|- National Film Best Feature Film in Tamil || V. Natarajan (Producer) K. Balachander (Director) ||  
|- 32nd Filmfare Best Tamil Film || V. Natarajan ||  
|- Best Tamil Director || K. Balachander ||  
|- Best Tamil Actress || Saritha ||  
|}

== References ==
 

== Sources ==
 
* 
*  
 

== External links ==

*  
*  

 
 
 

 
 
 
 
 
 