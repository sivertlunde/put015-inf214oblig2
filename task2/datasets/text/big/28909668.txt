At the Edge of the World (2008 film)
{{Infobox film
| name           = At the Edge of the World
| image          = At the Edge of the World film.jpg
| caption        = Theatrical release poster
| director       = Dan Stone
| producer       = Dan Stone
| writer         = 
| starring       = Paul Watson Alex Cornelissen
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Sea Shepherd ships to hinder the Japanese whaling fleet in the waters around Antarctica. The film won Best Environmental Film at the Vancouver International Film Festival. Director and Producer Dan Stone would later produce the first season of Whale Wars. It depicts what actually went on during this excursion, with clips of beautiful scenery, news clips, whaling in action, and life on the ship. 

==Synopsis== RV Farley MY Robert Hunter, captained by Alex Cornelissen meet in the Southern Ocean. As they are docked side to side, material is transferred from the Farley Mowat to the Robert Hunter to build a new helicopter deck. After some time the Robert Hunter is able to find the Nisshin Maru and engages it. In the course, one of the Sea Shepherds small boats with two men on board gets lost. Both Sea Shepherd vessels must abandon the Nisshin Maru, which later takes part in the search for them. Finally after 9 hours, they are able to locate them and they are saved. Having lost the Nisshin Maru, the Robert Hunter later finds the Kaiko Maru, a spotter vessel for the Japanese whaling fleet. They engage the ship and during maneuvering through an ice field collide with each other, damaging both ships. As the Farley Mowat approaches, the Japanese vessel calls out a Mayday, stopping the Sea Shepherds from further engagement. As the film ends, we find out that there was a fire on the Nisshin Maru, killing one worker and ending the whaling season early due to damage to the ship.

==Cast==
*Paul Watson as Himself
*Alex Cornelissen as Himself

==Reception==
The Globe and Mail reviewer Fiona Morrow called it, "an epic tale of hunter and hunted: a Moby-Dick for the environmental age." 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 