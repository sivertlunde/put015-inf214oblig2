Sequoia (film)
{{Infobox film
| name           = Sequoia
| image          =
| alt            = 
| caption        =
| director       = Chester M. Franklin 	
| producer       = John W. Considine Jr. 	 Carey Wilson
| based on       =   Paul Hurst Willie Fung
| music          = Herbert Stothart
| cinematography = Chester A. Lyons 
| editing        = Charles Hochberg
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Carey Wilson. Paul Hurst and Willie Fung. The film was released on December 22, 1934, by Metro-Goldwyn-Mayer. {{cite web|url=http://www.nytimes.com/movie/review?res=9800E4D91E31EE32A25750C2A9649C946494D6CF|title=Movie Review -
  Sequoia - The Capitol Presents Sequoia, a Distinguished Animal Film -- Night Life of the Gods. - NYTimes.com|publisher=|accessdate=18 November 2014}}  

==Plot==
Toni and her father Matthew Martin live in the sequoia forests of California. While Toni is out walking she finds a puma, which she names Gato and a young fawn that she calls Malibu. Toni and her adopted animals become friends quickly. After several years, Toni and her father leave the woods and Gato and Malibu are returned to the wild. Later, when Toni and her father return, they find that the animals in the area have been decimated by logging and hunting. With aggressive hunting parties roaming the area, it is up to Gato and Malibu to survive. 

== Cast ==
*Jean Parker as Toni Martin
*Russell Hardie as Bob Alden
*Samuel S. Hinds as Dr. Matthew Martin Paul Hurst as Bergman
*Willie Fung as Sang Soo
*Harry Lowe Jr. as Feng Soo Ben Hall as Joe 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 