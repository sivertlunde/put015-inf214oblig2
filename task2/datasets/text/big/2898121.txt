Mr Karate
{{Infobox film
| name           = Mr. Karate
| image          = 
| caption        = 
| writer         = Raouf Tawfiq
| starring       =  
| director       = Mohamed Khan
| producer       = 
| cinematography = Kamal Abdel Aziz 
| editing        = Nadia Shoukry  Nadia Shoukry]]
| distributor    = 
| released       =  
| music          =
| runtime        = 200 minutes
| language       = Arabic
| budget         =
| gross          =
}}
 Ahmad Zaki as an originally naive out of towner who is brought down by the hardships of life in Cairo.  Broke and unemployed, Zakis character gets a stint as a garage worker before graduating to the more lucrative, albeit less dignified, profession of illicit parking helper (monadi).  After an acquaintance with a local retired karate instructor (played by Ibrahim Nasr), Zakis character is introduced to the world of martial arts through watching a series of old Bruce Lee and Jackie Chan movies which at first are picked out by his new friend (Nasrs character) but which he then picks out himself from a nearby video rental store where his love interest in the movie works (played by Nahla Salama).  What starts as a childish fascination, becomes with the help of Nasr, a new awakening for Zakis character, who develops a resolve to become defenseless no more and trains in art of karate.
 Ahmad Zaki said the only thing he regreted about Mr Karate was its title.  He said it was misleading because it led many moviegoers and critics to believe it was a lighthearted martial arts Parody|spoof, when, in fact, it was about the troubles of a simple man trying to survive the vast city that is Cairo, Egypt.  In the interview, Zaki said he had hoped for the movie to be called Cairo 90.

Soon, Zaki realizes that his newfound karate abilities allows him to help out distressed members in his community, which captures the attention of a local tycoon (who in the film remains obscure and whose exact profession - whether he is a businessman, member of parliament or even a minister - is left to the viewers imagination).
At the time of its release, the film received mixed reviews, but it did solidify Zaki as an icon and spokesperson for the average Egyptian youth.  It also allowed him to pursue more action-packed movies during the mid-and late-1990s.

==Cast==
* Ahmed Zaki as Salah
* Ibrahim Nasr
* Nahla Salama
* Zouzou Nabil

==Reception==
Mr. Karate received mixed reviews. IMDb gave it an average score of 4.8 out of 10 based on 83 different reviews.    Despite the low IMDB rating, Andeel, from Mada Masr, found it as, "an amazing mixture of cultures and references   cooperate to produce the joy you receive watching it", and, as he enjoys Tarantino movies, "an earlier experiment in the same field".   

==External links==
*  

 
 
 
 