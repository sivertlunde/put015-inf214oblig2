A Woman of Paris
{{Infobox film
| name           = A Woman of Paris
| image          = AWomanOfParis1923Cover.jpg
| alt            =  
| border         = yes
| caption        = US DVD cover
| director       = Charlie Chaplin
| producer       = Charlie Chaplin
| writer         = Charlie Chaplin Carl Miller Lydia Knott Charles K. French Adolphe Menjou
| music          = Louis F. Gottschalk (original score) Charlie Chaplin (1976 release)
| cinematography = 
| editing        = 
| studio         = Charles Chaplin Productions Regent Allied Artists (1923) (UK) Cinegate (1978) (UK) (theatrical re-release) Cinal S.A. (1980) (Spain) (theatrical re-release) Fox Video (1992) (USA) (VHS) Warner Home Video (2004 DVD) Cinegate (1984) (UK) (theatrical) Continental Home Vídeo (Brazil) (VHS) Image Entertainment (USA) (DVD)
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = Silent film  English intertitles
| budget         = 
| gross          = $634,000 (US/Canada) {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p45 
}}

A Woman of Paris is a feature-length American silent film that debuted in 1923. The film, an atypical drama film for its creator, was written, directed, produced and later scored by Charlie Chaplin. It is also known as A Woman of Paris: A Drama of Fate.  

==Plot==
Marie St. Clair and her beau, aspiring artist Jean Millet, plan to leave their small French village for Paris, where they will marry. On the night before their scheduled departure, Marie climbs down from her second-floor bedroom for a rendezvous with Jean. Her stepfather sees them strolling down a lane and locks her out of the house. When the couple returns, Jean furiously knocks on the front door and reminds the older man that hes locked out his daughter. The stepfather dismisses Jeans complaint and tells Marie, "Perhaps (Jean) will give you a bed for the night."

Jean does invite Marie to his home, but he makes it clear that he lives with his parents and that his mother will fix a bed for Marie. It turns out that Jeans parents are not thrilled with their sons romance with Marie, either. Marie goes to the train station, with Jean promising to follow her. But Jeans father has died while sitting in his chair in front of the fireplace; and, when Jean telephones Marie at the station to tell her he cant go with her to Paris, she gets on the train and makes the trip alone.

In Paris, Marie enjoys a life of luxury as the mistress of wealthy businessman Pierre Revel. One night when Marie is alone in the apartment Revel has provided for her, a friend calls and invites her to a raucous party in the Latin Quarter. The friend gives Marie the address but admits that she cant remember whether the apartment is in the building on the right or the left. Marie, arriving by taxi, enters the wrong building and is surprised to be greeted by Jean Millet. Marie tells Jean she would like for him to paint her portrait and gives him a card with her address.

Jean calls on Marie at her apartment to begin the painting. Marie notices he is wearing a black armband and asks why he is in mourning. Jean tells her his father has died. Marie asks when, and Jean replies, "The night you left."

Marie and Jean revive their romance, and Marie begins to distance herself from Pierre Revel. Pierre knows about Jean but also realizes that Marie has become fond of the luxuries she enjoys as his mistress.

Jean finishes Maries portrait; but, instead of painting her wearing the elegant outfit she chose for the sitting, he outfits her in the simple dress she wore on the night she left for Paris.

Jean proposes to Marie. Marie tells Pierre shell be leaving soon, but Pierre isnt so sure.

Jeans mother, with whom he shares the simple Paris apartment, argues with him about marrying Marie. Jean starts to leave in anger but, after opening the door, leaves the door ajar as he goes to apologize to his mother. He tells his mother the proposal was spur-of-the-moment and not serious. Marie happens to arrive unexpectedly outside Jeans apartment at that moment. A chastened Marie returns to Pierre Revel.

Jean fails to convince Marie he didnt mean what she overheard him say to his mother only to appease the older woman. One night, Jean slips a gun into his coat pocket and goes to the exclusive restaurant where Marie and Pierre are dining. Jean asks the maitre d to give Marie a note asking her to meet him one last time. Pierre sees the note and invites Jean to join them. Jean and Pierre get into a scuffle, and Jean is ejected from the dining room. Jean stands by the fountain in the restaurants foyer, pulls out the gun and fatally shoots himself.

The police carry Jeans body to his apartment. Jeans mother retrieves the gun and goes to Maries apartment. Maries maid tells her that Marie has gone to her sons studio. Jeans mother returns to the apartment and finds Marie kneeling by Jeans body and sobbing.

Jeans mother is touched by Maries display of grief. The two women reconcile and return to the French countryside, where they open a home for orphans in a country cottage.

One morning, Marie and one of the girls in her care walk down the lane to get a pail of milk. Marie and the girl meet a group of sharecroppers with a horse-drawn wagon, who offer them a ride back in the wagon. At the same time, Pierre Revel and another gentleman are riding through the French countryside in a chauffeur-driven automobile. Pierres companion asks him, "What ever happened to that Marie St. Clair?" Pierre replies that he doesnt know. Pierres automobile and the horse-drawn wagon then pass each other, heading in opposite directions.

==Cast==
* Edna Purviance - Marie St. Clair
* Clarence Geldart - Maries Father Carl Miller - Jean Millet
* Lydia Knott - Jeans Mother
* Charles K. French - Jeans Father
* Adolphe Menjou - Pierre Revel
* Betty Morrissey - Fifi
* Malvina Polo - Paulette
* Henry Bergman (uncredited) - Head Waiter
* Charles Chaplin (uncredited) - Porter

==Production==
Several things set this film apart from Chaplins other work. The first, most obvious, is that he does not appear in the film, at least not in his traditional role of the tramp. He has a brief cameo as a porter in a train station. This role was supposed to be inconspicuous and he is not even listed in the credits for it (though he precedes the film with a title card which explains that he does not appear). Most people seeing the film will not realize that it is actually Chaplin; this was intended. The other major difference between this and most of Chaplins other work is that the film is a serious drama. 

Edna Purviance plays the lead as Marie St. Clair. Chaplin had several reasons for producing this film, and one of these reasons was to help Purviance gain recognition as an actress without Chaplin at her side. Another was because he wanted to try staying behind the camera and attempt his first real drama. Despite this attempt, Edna Purviance was never able to achieve the level of success that she had in films with Chaplins Tramp at her side. However, the film did help Adolphe Menjou gain some recognition.

The film was largely inspired by Chaplins brief 1922 romance with Peggy Hopkins Joyce, whose stories of her romantic adventures in Europe provided the framework of the screenplay.

==Reception==
The public did not receive this film very well. Chaplin was very popular at this time, and many went to this film expecting to see Chaplin in his traditional role. There were two efforts made to help "ease" the public into the idea of Chaplin doing a film without Chaplin in it. On the night the film premiered, Chaplin had flyers given to those in line. The flyers essentially state that this is a deviation from his normal work, and that he hopes the public will find these deviations enjoyable. The film also contains a message at the beginning stating that Chaplin will not be appearing in the film. Some   film historians have speculated about what the publics reaction would have been if they did not know A Woman of Paris did not star Chaplin — it may have been received much differently. 

Critical response to the film was very positive, and it is credited with influencing later filmmakers. In particular, the films characters and their motivations had a complexity that was new to cinema. Chaplin biographer  . 

The films box office failure was painful for Chaplin, and after its initial release it was not seen by the public for over fifty years. Chaplin reissued the edited film with a new musical score—replacing the original score by Louis F. Gottschalk—in 1976, a year before his death. In fact, the score he composed is credited as being the final completed work of his 75-year career.

==References==
 

==External links==
*  
*  at silenthollywood.com

 

 
 
 
 
 
 
 
 
 
 