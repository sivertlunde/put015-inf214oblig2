Mysterious Mose
 
{{Infobox film
| name = Mysterious Mose
| image =
| caption =
| director = Dave Fleischer
| producer = Max Fleischer
| animator = Grim Natwick
| writer =
| narrator =
| starring = Margie Hines
| music =
| cinematography =
| editing =
| distributor = Paramount Publix Corporation
| released =  
| runtime = 5-6 minutes
| country = United States English
| budget =
}}

Mysterious Mose is a 1930 Fleischer Studios Talkartoon animated short released through Paramount Pictures. It was released in late December the same year. This film contains an early version of Betty Boop and the studios star, Bimbo (Fleischer Studios)|Bimbo.

Mysterious Mose was a song from early 1930, first performed by Walter Doyle and his Orchestra. There are numerous recordings of the song, including Cliff Perrine and his Orchestra and R. Crumb & His Cheap Suit Serenaders. It was written by Walter Doyle and recorded for Columbia in April 1930 by Rube Bloom and His Bayou Boys.

==Synopsis==
Betty is startled awake in her bed on a stormy night. She searches for the cause of the shock while she sings the song. Then, unexplainable phenomena start happening in the house. Mysterious Mose (Bimbo) appears, and sings part of the song. Bizarre cartoon creatures appear and, at first, sing and enhance Moses "mysterious" image. Quickly, however, the antics become frightful even to Mose. The film escalates into chaos, which ends when Mose bursts, revealing him having been an automaton (full of cogs and springs) the whole time.

Graveyard Jamboree with Mysterious Mose is a short film made in 1998 by film makers Seamus Walsh and Mark Caballero of Screen Novelties. The film utilizes puppetry, stop motion and silhouette animation to tell the story of an otherworldly creature preparing a celebration in a cemetery. Walsh and Caballero used the song Mysterious Mose recorded in 1930 by Harry Reser and his Radio All-star Novelty Orchestra. The film has gained world wide attention in film festivals and has retained a small cult audience.

==External links==
*  
*  
*  

 
 
 
 


 