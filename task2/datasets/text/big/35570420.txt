After the Ball (1897 film)
{{Infobox film
| name           = After the Ball
| image          = Méliès, Apres le bal (Star Film 128, 1897).jpg
| caption        = scene from the film
| director       = Georges Méliès
| producer       = Star Film Company  
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1897
| runtime        = Short
| country        = France
| language       = Silent film
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
After the Ball (French: Après le bal) is a short French silent film created and released in 1897 and directed by Georges Méliès.

It starred Jehanne dAlcy - Méliès future wife, and Jane Brady.

The films plot is a one minute scene of a servant bathing a woman, along with the scenarios before and after as the servant is helping her get undressed while revealing a few layers of clothing, bathing her, and finally covering and drying her with a robe. This is the earliest film to show nudity. 

==References==
 

==External links==
* 
*   on YouTube
*   on YouTube
 
 
 
 
 
 
 

 