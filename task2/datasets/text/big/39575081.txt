One by Two (2014 film)
 
 
{{Infobox film
| name           = One By Two
| image          = One by Two (2014 Hindi film) Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Devika Bhagat
| producer       = Amit Kapoor Abhay Deol  Sanjay Kapoor 
Vikram Khakhar Viacom 18 Motion Pictures 
| story          = Devika Bhagat
| screenplay     = Devika Bhagat
| starring       = Abhay Deol Preeti Desai
| music          = Shankar Ehsaan Loy
| cinematography = 
| editing        = Shan Mohammed
| studio         = 
| distributor    = Cynozure Networkz
| released       =  
| runtime        = 
| country        = India
| language       = Hindi Urdu
| budget         = 
| gross          =
}}
One By Two is a 2014 Hindi romantic comedy directed by Devika Bhagat.  It released on 31 January  2014 at multiplexes, showing on approximately 500 screens in India.    The film features real life couple Abhay Deol and Preeti Desai.   This is the story of Amit and Samara who meet each other while living in Mumbai  The music of the film has been composed by Shankar-Ehsaan-Loy. The film was heavily panned by critics.   

== Cast ==

* Abhay Deol as Amit Sharma
* Preeti Desai as Samara Patel
* Rati Agnihotri as Amits mother
* Jayant Kripalani as Amits father
* Lillete Dubey as Samaras mother
* Anish Trivedi as Samaras father
* Darshan Jariwala as Amits uncle
* Netarpal Singh Heera as Bunty
* Yudishtir Urs as Jonathan
* Preetika Chawla as Anika
* Tahir Raj Bhasin as Mihir
* Maya Sarao as Shaila 
* Yashika Dhillon as Shishika
* Dr. Rajesh Asthana as Mr. Reddy
* Diwakar Pundir as Ranjan Sadanah
* Shrishti Arya as Promila
* Tanvir Singh as Host of dance show
* B.K. Tiwari as ACP Salgaonkar
* Neelam Sharma as Inspector Chowgule
* Hemant Soni as Halwaldar More
* Amit Shukla as Inspector Shetty 

==Critical Reception==
The film was heavily panned by critics.  Subhash K Jha writes that One by Two is "irresistible in parts" and "never disappoints." He says there is a "winsome, bubbly bouncy and ebullient quality to this take on urban aspirations." 

== Soundtrack ==
{{Infobox album  Name = Longtype = Type = Artist = Label = Producer = Last album This album Next album = }}
{{Track listing extra_column = lyrics_credits = all_lyrics = length2 = title2 = extra2 = length4 = title4 = extra4 = length7 = title7 = extra7 = Arijit Singh}}

==Controversy==
  royalty over to the music company.    Abhay Deol took to social media and blamed T-Series for delaying the music launch of his film as well as not promoting the movie due to him and the composers not agreeing to sign the documents. This allegation was denied by T-Series who claimed that they had uploaded 3 songs from the movie on its YouTube page and claimed "In the absence thereof  , we cannot be expected to release music when there is no clarity with respect to the rights granted to the producer"   In another statement, T-Series expressed the fact that they had been assured by the company Viacom 18 that they owned all the rights (including the music rights) and that they did not understand why they were being dragged into this dispute which should be between Mr. Deol and Viacom 18.   T-Series eventually relinquished their hold on the music rights after a campaign was run by the composers and artists against the music company.    Eventually the music for the film was launched after music companies Crecendo and Unisys who took over, with the former releasing the physical copies and the latter releasing the digital copies of the music online.   The music was released 5 days before the films theatrical release.

==Boxoffice==
One By Two collected Rs 2.25-2.50 crore nett in its first week, mostly from multiplexes in  Mumbai and Delhi. Box Office India called the business "very poor". 

== References ==
 

==External links==
*  

 
 
 
 
 