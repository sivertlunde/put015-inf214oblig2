Dandam Dashagunam
{{Infobox film
| name = Dandam Dashagunam
| image = 
| caption = 
| director = K. Madesha
| producer = A. Ganesh
| writer = 
| starring = Chiranjeevi Sarja Ramya P. Ravi Shankar
| music = V. Harikrishna
| cinematography = 
| editing = 
| distributor = 
| released =  
| runtime = 
| country = India
| language = Kannada
| budget = 
| gross = 
}} action genre Surya and Jyothika starrer  Tamil film, Kaakha Kaakha. 

==Plot==
ACP Surya I. P. S. is an efficient officer who works to serve people. When he is handling a case he sees Maya. Till then he thinks he is fire and no one can touch his heart but falls in love with Maya.  He meets her again in the mall but cant talk to her. He meets her again and she comes to know that he is an officer.  After some months he sees her again in the school Maya will be working in the school as a teacher. Once one of the students of Maya will face a problem.  So she seeks help of Surya who solves the problem.  Surya will be assigned to look after the roads where the state c.m would pass. Maya sees Surya and requests him to have a cup of coffee with her but he tells no as he is on duty. Maya while crossing the road meets with an accident. Surya will take her to a nearby hospital and Maya will be out of danger. After some days Surya comes to meet Maya to her house she requests him to take her out on a ride as she was bored of staying at home. She takes him to a boat house (the place which she owns) and tells him that she likes him a lot and proposes him he first refuses and later accepts. He will be assigned to arrest a drug smuggler who is the villain of the story. He kidnaps Maya to escape from Surya but fails to do so and reaches his death.

==Cast==
* Chiranjeevi Sarja - A.C.P Surya I.P.S
* Ramya - Maya
* P. Ravi Shankar as Tamate Shiva
* Jeevi
* Thilak

==Character map of Kaakha Kaakha(2003) and its remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
| Kaakha Kaakha (2003) (Tamil cinema|Tamil) ||  Gharshana (2004) (Cinema of Andhra Pradesh|Telugu) || Force (2011 film)|Force (2010) (Bollywood|Hindi) ||  Dandam Dashagunam (2011) (Cinema of Karnataka|Kannada)
|- Suriya (actor)|Suriya Venkatesh  John Abraham   || Chiranjeevi Sarja
|-
| Jyothika ||  Asin ||  Genelia DSouza || Divya Spandana
|- Jeevan || Salim Baig || Vidyut Jamwal || P. Ravi Shankar
|}

==Soundtrack==
V. Harikrishna has composed the music for the film.

{|class="wikitable" width="70%"
! Song Title !! Singers
|-
| "Kalli Neenu" || Shaan (singer)|Shaan, Shamita Malnad
|-
| "Rasikanu Ivanu" || Anuradha Bhat
|- Karthik
|-
| "Ondooral" || Sonu Nigam
|-
| "Neenillade" || Chetan Sosca
|-
|}
 

==References==
 

 
 
 
 
 