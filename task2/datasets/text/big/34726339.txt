Pusher (2012 film)
 
 
{{Infobox film
| name           = Pusher
| image          = Pusher_2012_Quad.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster Luis Prieto
| producer       = Rupert Preston Felix Vossen Christopher Simon
| screenplay     = Matthew Read
| based on       =  
| starring       = Richard Coyle Agyness Deyn Bronson Webb Mem Ferda Zlatko Buric Orbital
| cinematography = Simon Dennis
| editing        = Kim Gaster
| studio         = Vertigo Films Embargo Films Gaumont   The Weinstein Company|RADiUS-TWC  
| released       =  
| runtime        = 89 minutes  
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $186,506 
}} crime Thriller thriller film Luis Prieto. 1996 film of the same name. The film stars Richard Coyle, Agyness Deyn, Bronson Webb, and Paul Kaye. Refn takes on the role of executive producer, with Rupert Preston of Vertigo Films and Chris Simon and Felix Vossen of Embargo Films producing. International sales are handled by Gaumont Film Company.

==Plot==
 Serbian drug duck lake. In custody, Frank is told by police that Tony has confessed to the deal, but they release him the next day. Frank quickly finds Tony and beats him savagely with a baseball bat.

Frank returns to Milo without the cash or the cocaine and requests more time to pay Milo off. Milo demands £55,000 to be paid soon. Frank scrambles to call in old debts and tries to get back in touch with Danaka, a drug mule who is scheduled to return with a half kilo of cocaine. Frank is unsuccessful and becomes increasingly on edge. Milos henchman Kagan arrives to help Frank strongarm his debtees, but they have no success. Frank finally gets in touch with Danaka, but discovers that the cocaine pack she brought back is worthless. Frank becomes desperate and robs a high-class drug party, but is still far short of the required sum.

Milos patience runs out and Frank is brought back to him. With only a few thousand pounds to his name Frank begs for more time, but Milo begins to torture him instead. Frank manages to escape and returns to Flo, asking her if she wants to run away with him to Spain using the funds he has gathered up. As Frank makes a few final arrangements, Milo calls and says that their feud has become too costly and will therefore accept a token payment to resolve it. However, we see that Milo and his henchmen are actually planning to ambush Frank when he arrives.

Frank brusquely tells Flo that their plans are called off and laughs at the thought of them running away together. Heartbroken, Flo steals all of Franks remaining cash and escapes in a taxi. Frank catches up to the taxi and the two stare tearfully at each other through the closed car window as the film ends.

==Cast==
* Richard Coyle as Frank, a mid-level drug-dealer.
* Bronson Webb as Tony, Franks cheerful partner
* Agyness Deyn as Flo, Franks girlfriend
* Zlatko Burić as Milo, a powerful Serbian drug lord. Burić also played Milo in the original Pusher.
* Mem Ferda as Hakan, Milos enforcer
* Paul Kaye as Fitz, one of Franks customers
* Shend as Meten, a nasty piece of work

 

==Filming==
The film was shot in and around Stoke Newington.

 

==Soundtrack==
The films score is provided by the British electronic band Orbital (band)|Orbital.

==Reception==
  
Pusher received mixed to positive reviews. Metacritic gives the film a weighted average score of 52% based on reviews from 19 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 