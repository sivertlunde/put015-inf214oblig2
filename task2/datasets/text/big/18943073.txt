The Master Touch
{{Infobox film
| name           = The Master Touch 
| image          = TheMasterTouch.jpg
| image_size     =
| caption        =
| director       = Michele Lupo
| producer       = Manolo Bolognini (executive producer) Marina Cicogna (producer)
| writer         = Roberto Leoni (story) Franco Bucceri (story) Nico Ducci (screenplay) Michele Lupo (screenplay) Mino Roli (screenplay)
| narrator       =
| starring       = See below
| music          = Ennio Morricone
| cinematography = Tonino Delli Colli
| editing        = Antonietta Zita
| distributor    =
| released       = 8 December 1972
| runtime        = 112 minutes 95 minutes (DVD)
| country        = Italy, West Germany
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 1972 Italian / West German crime film directed by Michele Lupo starring Kirk Douglas and Florinda Bolkan. 

The film is also known as A Man to Respect in the Philippines and the USA.

== Plot summary ==
Steve Wallace, a safe cracker, has just been released from prison.  He attempts one last burglary with the help of a circus gynmast in Germany.

== Cast ==
*Kirk Douglas as Steve Wallace
*Giuliano Gemma as Marco
*Florinda Bolkan as Anna
*Wolfgang Preiss as Miller
*Reinhard Kolldehoff as Detective Hoffman
*Romano Puppo as Millers Lieutenant
*Bruno Corazzari as Eric
*John Bartha as Murdered Security Guard
*Allen Stanley
*Vittorio Fanfoni
*Luigi Antonio Guerra

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 


 