Once Upon a Crime
{{Infobox film
| name           = Once Upon a Crime...
| image          = Once upon a crime.jpg
| caption        = The cover for Once Upon a Crime...
| writer         = Rodolfo Sonego Giorgio Arlorio Stefano Strucchi Luciano Vincenzoni Charles Shyer Nancy Meyers Steve Kluger Richard Lewis George Hamilton
| director       = Eugene Levy
| music          = Richard Gibbs
| distributor    = Metro-Goldwyn-Mayer  Entertainment Film Distributors (UK)
| released       =  
| runtime        = 94 min.
| language       = English
| producer       = Dino De Laurentiis
| cinematography = Giuseppe Rotunno
| editing        = Patrick Kennedy
| budget         = $14 million
| gross          = $8,669,847 (USA)
}}
 1992 ensemble ensemble comedy Richard Lewis, John Candy, James Belushi, Cybill Shepherd, Sean Young and Ornella Muti. The film was directed by Eugene Levy. It is the remake of the Mario Camerinis comedy Crimen (film)|Crimen.

== Plot == Richard Lewis) and Phoebe (Sean Young) met each other in Rome and are attempting to return a dachshund to the wealthy Madam Van Dougan.

Madam Van Dougan is found murdered and the interactions between Julian and Phoebe and the other couples begin to look increasingly suspicious, as Inspector Bonnard (Giancarlo Giannini) needs to unravel the clues. Over the course of the film, Augie returns to gambling, Elena has an affair and Julian sells and repurchases the dog.

== Synopsis ==
The film is often described as having "A chaotic screenplay, which results in everyone needing to shout at each other all the time". The plot is fast-moving and often involves frantic wisecracking from all the principal characters. A memorable example is Julian being asked by Bonnard to "look at this mans face" (the man being a witness). Julian takes one look at the strangely featured man and screams. Later, Augie is requested to provide an alibi for the crime which took place "Between one and one-thirty in the morning". His alibi is that he was making love to his wife, and he can be certain because "Her screams of pleasure woke up the street and someone cried out ... Its between one and one-thirty in the morning! Give that poor woman a break!  Lewis customary dry humour is ever-present, including his description of himself and Young as "Julian Peters and Phoebe? We sound like a high-wire act!"

There are some elements of physical comedy, including the facial expressions of the characters as the contents of a suitcase are revealed. A striking example, however, is Augies re-enactment of how he crept from room to room of a hotel at night.

==Cast==
* John Candy as Augie Morosco
* James Belushi as Neil Schwary
* Cybill Shepherd as Marilyn Schwary
* Sean Young as Phoebe Richard Lewis as Julian Peters
* Ornella Muti as Elena Morosco
* Giancarlo Giannini as Inspector Bonnard George Hamilton as Alfonso de la Pena
* Roberto Sbaratto as Detective Toussaint
* Joss Ackland as Hercules Popodopoulos
* Ann Way as Housekeeper
* Geoffrey Andrews as Butler
* Caterina Boratto as Madame de Senneville
* Elsa Martinelli as Carla the Agent
* Riccardo Parisio Perrotti as Customs Officer
* Eugene Levy as Casino Cashier (uncredited)
* Brad Murphy as The Fashion (uncredited)

== Reception ==
The film earned negative reviews from critics. Janet Maslin gave the film a negative review in the New York Times, saying it was not funny, and adding "As a general rule, films whose plots revolve around lost dogs are apt to be short on comic inspiration, and this one is no exception."  It currently holds a 0% rating on rotten tomatoes and was nominated for one Razzie Award, Worst Supporting Actress for Sean Young, where she lost to Estelle Getty in Stop! Or My Mom Will Shoot.

== Inspirations == thriller movies in Bollywood, adapted this movie as 36 China Town starring Shahid Kapoor and Kareena Kapoor. 36 China Town is a frame-by-frame, shot-to-shot imitation of Once Upon a Crime.

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 