Carmen (1918 film)
{{Infobox film
| name           = Carmen
| image          = Pola Negri 1.png
| image_size     =
| caption        =
| director       = Ernst Lubitsch Paul Davidson
| writer         = Prosper Mérimée (novel)   Grete Diercks   Norbert Falk   Hanns Kräly   Myron M. Stearns (English version)
| narrator       =
| starring       = Pola Negri   Harry Liedtke   Leopold von Ledebur   Wilhelm Diegelmann
| music          =
| editing        = Alfred Hansen UFA
| distributor    = UFA (Germany)   First National Pictures (US)
| released       = 17 December 1918
| runtime        = 6 reels
| country        = Germany Silent  German intertitles
| budget         =
| gross          =
}} silent drama English intertitles in the United States in 1921 under the alternative title Gypsy Blood. 

==Plot==

The story is told by a man at a camp-fire who says that it took place many years before.

Don José was a Dragoon Sergeant in Sevilla who fell madly in love with Carmen, a beautiful gypsy. For her, he killed an officer and gave up his fiancée and his career in the army, and became a smuggler. But Carmens love did not last. She left him and went to Gibraltar where she fell in love with the famous bull-fighter Escamillo. Back in Sevilla, Carmen rode triumphantly in Escamillos carriage on his way to a bull-fight. At the end of the bull-fight, José confronted Carmen and when she told him that she no longer loved him, stabbed her to death.

Back at the camp-fire seen at the beginning, the man who told the story adds that some say that Carmen did not die ′for she was in league with the Devil himself.

==Cast==
* Pola Negri as Carmen
* Harry Liedtke as Don José Navarro
* Leopold von Ledebur as Garcia
* Grete Diercks as Dolores
* Wilhelm Diegelmann as Gefängniswärter / Prison Guard
* Heinrich Peer as Englischer Offizier
* Margarete Kupfer as Carmens Wirtin - Landlady
* Sophie Pagay as Don Josés Mutter
* Paul Conradi as Don Cairo
* Max Kronert as Remendado
* Magnus Stifter as Lieutnant Escamillo
* Paul Biensfeldt as Soldat
* Victor Janson  
* Albert Venohr

==References==
 

==Bibliography==
* Eyman, Scott. Ernst Lubitsch: Laughter in Paradise. Johns Hopkins University Press, 2000.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 

 