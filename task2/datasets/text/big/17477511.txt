Nekromantik 2
{{Infobox film name = NEKRomantik 2 image = Nekromantik2.jpg image_size = caption = Film poster by Andreas Marschall director = Jörg Buttgereit producer = Manfred O. Jelinski writer = Jörg Buttgereit Franz Rodenkirchen narrator = starring = Wolfgang Müller music = cinematography = Manfred O. Jelinski editing = Jörg Buttgereit Manfred O. Jelinski studio = distributor = released =   runtime = 104 min. country = Germany language = German English English
|budget = €25.000 gross =
}}
NEKRomantik 2 is a 1991 German horror film|horror/splatter film directed by Jörg Buttgereit and a sequel to his 1987 film Nekromantik. The film is about necrophilia, and was quite controversial and was seized by authorities in Munich 12 days after its release,     an action that had no precedent in Germany since the Nazi era. Blake, Linnie. Things To Do In Germany, With The Dead.  Today, it is regarded as a cult classic.

==Setting==
The film is set in Berlin, shortly after the German reunification. The apartment of Monika is set in the former East Berlin, with exterior shots depicting a burnt-out Trabant and crumbling building façades, while other locations of the film, such as the meeting place of Mark and his friend, are set in the former West Berlin. Halle (2003), p. 291-292 

==Plot== tabloid headlines inform viewers of the source of Monikas knowledge of Rob and his activities. Kerekes (1998), p. 76-90  Meanwhile, Mark (Mark Reeder) heads to his as-yet-unspecified job, and the film then cuts back to a scene of Monika undressing Rob. Marks job is thereupon revealed to be dubbing porn films, and this scene foreshadows the next, in which Monika has sex with Robs corpse. Betty (Beatrice Manowski), Robs ex-girlfriend from the previous film, is then briefly introduced as she discovers, to her disappointment, that Robs grave has already been robbed.
 cuddling the corpse in the photos. Kerekes (1998), p. 76-90  Mark, meanwhile, makes plans to meet a friend (Simone Spörl) at the movie theater. Marks friend, however, is late, and Mark offers his ticket instead to Monika, a stranger who happens to be passing by. Kerekes (1998), p. 76-90  Monika and Mark hit it off and soon go on a carnival date, after which point Monika decides to break up with Rob. She is apparently attempting to live a normal life with a real boyfriend. Kerekes (1998), p. 76-90 

She tearfully saws the corpse of Rob into pieces and putting them into garbage bags, saving just his head and genitals. She then disposes the garbage bags. She keeps the genitals in her fridge. Kerekes (1998), p. 76-90  The next scene depicts Monika and Mark in her apartment. She is showing him a photograph album, containing images of several dead relatives, sometimes in their coffins. The two end up having sex, though the experience is less than satisfying for Monika. Mark subsequently spends the night at Monikas, and, through a morning raid of her refrigerator, Mark discovers Robs genitals. This discovery, combined with Monikas desire to photograph Mark in positions that make him appear dead, plants doubts in his mind about the relationship. He does not dare express these doubts to her. Kerekes (1998), p. 76-90  Halle (2003), p. 294  Consequently, Mark consults first his perennially tardy friend and then a drunk in a bar regarding his relationship with the perverse Monika.

Soon thereafter, Monika and her fellow necrophiliac friends have a movie night at Monikas apartment. Suggesting that she is part of a network of people with similar interests. The film they are watching depicts the dissection of a seal. Positioned on the coffee table during their viewing experience is Robs severed head. Kerekes (1998), p. 76-90  Halle (2003), p. 294  Mark undexpectedly drops by, bringing a pizza. This brings the movie night to a premature end, with Monika hiding the severed head and her friends leaving. When Mark insistently asks what Monika and her friends had been doing, she reluctantly shows him the seal video. The images both disgust and enrage Mark, who says its perverse to watch such a thing for fun, leading to a quarrel. Kerekes (1998), p. 76-90 

The couple later speak on the phone and makes plans to meet at Monikas and discuss the matter. In the meantime, Monika makes a trip to the ocean, where she contemplates what course of action to take. When Mark arrives the next day, they have make-up sex, during which Monika severs Marks head and replaces it with Robs severed head. In addition, Monika is finally shown climaxing, which suggests that she has chosen the correct lover.

Finally, in the last scene, a doctor congratulates Monika on her pregnancy.

==Soundtrack==
The soundtrack, by Hermann Kopp, Daktari Lorenz, John Boy Walton and Peter Kowalski, is neither ironic nor campy, but rather is intended to generate genuine emotional response. The serious intent of the film in general is made clear in an interview in which Buttgereit discusses an audition in which actors performed the love scene with Robs corpse: "Though they were all quite willing, none of them took it as seriously as we did."  

Furthermore, although he is commenting on the soundtrack to the original Nekromantik, Christian Keßlers observations about that films soundtrack resonate in the context of the second film as well: "The excellent soundtrack by Lorenz, Hermann Kopp, and John Boy Walton accentuates this   with a romantic leitmotif composed for a single piano that makes the gruesome environment seem like a protective case, shielding Robert from the reality that so torments him." Keßler, Christian. Last Rites - Ways of Approaching Jörg Buttgereit  

==Production==
The shooting of the film occurred in September and October 1990. The editing of the film was completed by April 1991. Kerekes (1998), p. 76-90  The film was originally planned to last 85 minutes, but the print shown at the Berlin premiere lasted 111 minutes. It was soon shortened to 104 minutes, after removing "unimportant bits and pieces" from various scenes. Reportedly, no scene of the film was completely removed. Kerekes (1998), p. 76-90  David Kerekes commented that it could stand to be further shortened, since several sequences were, in his view, protracted and tedious. Kerekes (1998), p. 76-90 
 film within a film in the movie theater scene is a parody of My Dinner with Andre (1981). In the original film, two men sit at a dinner table and philosophize for two hours. The parody film  has a nude man and woman sitting outdoors, eating numerous boiled eggs and conversing on topics of ornithology. The parody film is shot in black-and-white, maintaining an authentic look for an art film. Kerekes (1998), p. 76-90  This sequence contains most of Nekromantik 2s dialogue. Kerekes (1998), p. 76-90  Buttgereit included this parody film as a tribute to a "fascinating piece of filmmaking". Kerekes (1998), p. 76-90 

The role of Monika was not written with a specific actress in mind. The creators of the film placed an advertisement in a magazine, looking for a person for the role. There were about 40 applicants, but none was deemed satisfying. Eventually Franz Rodenkirchen recruited Monika M., a woman who he met by chance in a movie theater. Kerekes (1998), p. 76-90  He was initially impressed that this woman was a fan of Lucio Fulci films. He then observed her for a while, and liked the way she walked and expressed herself. He decided to approach her and offer her the role. Kerekes (1998), p. 76-90  According to Monika, she was already familiar with the name of Buttgereit, having already seen Der Todesking (1989). Kerekes (1998), p. 76-90 

The role of Mark required the actor Mark Reeder to smoke, but he had no intention to take up smoking for the sake of art. So, the creators revised the role to have Mark as a non-smoker. Kerekes (1998), p. 76-90 

==Critical response==
According to Randall Halle, the film can be seen as a romance film with a twist. Halle (2003), p. 291-292 

"Jörg Buttgereit is the only person in Germany who manages to dedicate himself to these darkest of subjects with this much charm", writes critic Christian Keßler.   Though some accuse the Nekromantik films of being "little more than disappointingly witless and morbidly titillating attempts to disgust the most jaded conceivable audience,  these movies are not only more thematically complex and technically sophisticated than is popularly supposed, but share a set of artistic and ideological concerns more usually associated with the canonic authors of the Young German Cinema and the New German Cinema of the turbulent years of the 1960s and 1970s". 

Though speaking of the first Nekromantik, in which a "beer-guzzling, oompah-listening fat-man" accidentally kills a man picking apples, Linnie Blakes comments are also relevant to Nekromantik 2 when she writes "As Buttgereit makes clear, then, it is neither Rob nor Betty   who has transformed the young apple-picker into a corpse. This has been accomplished by an ostensibly morally upstanding member of society who subsequently disappears from view, unpunished for his crimes. Buttgereits mission, it seems, is to embrace that corpse, and in so doing to raise the question originally posed by Alexander Mitscherlich, Director of the Sigmund Freud Institute in Frankfurt, as to why the collapse of the Third Reich had not provoked the reaction of conscience-stricken remorse one might logically expect; why, in Thomas Elsaessers words, instead of confronting this past, Germans preferred to bury it.  

==Confiscation==
In June 1991, Munich police confiscated the film, leading an interviewer to ask Buttgereit "How does it feel to be Germanys most wanted filmmaker?"   Buttgereit responded: "Im not sure how to feel. At the moment Im afraid of a police raid. But Im not really proud of it, if thats what you mean." The reason for the films seizure was that it purportedly glorified violence. According to Buttgereit, "The thing that people find offensive about Nekromantik 2 is that it doesnt accuse Monika." At a different point in the interview, Buttgereit states, "It was very important to me that the audience is on Monikas side, even with her doing these terrible things." In 1993, however, the film was officially deemed "art", thanks to an exhaustive expert opinion by film scholar Knut Hickethier.  However, Buttgereit says, "the big shops are still afraid to sell my DVDs." 

The official charge against the film was that it was "glorifying violence". The police confiscated the print of the film shown in the Werkstattkino (Workshop Cinena) in Munich. Then the local prosecutor handed the print to the authorities of Berlin, which placed the film in the index for seized videos. Videos which all known copies had to be confiscated, while the police started searching for the negatives. Caught in possession of a copy was illegal in its own right. Kerekes (1998), p. 76-90  The confiscation was actually the fourth one to occur in the Werkstattkino over a period of two years. Starting in the mid-1970s, the movie theater had depicted many controversial films, including hardcore pornography, horror films and propaganda films dating to World War II. The censoring authorities in Munich seemed to have a particular grudge for the movie theater, which seems to have influenced the fate of Nekromantik 2. Kerekes (1998), p. 76-90  In July 1992, there was also a police raid in the residence of Manfred O. Jelinski, and the policemen confiscated anything remotely related to the banned film. Kerekes (1998), p. 76-90 

The films banning was actually questionable, because no trial or hearing over the fate of the film had actually occurred. The confiscation and banning was based solely on the decisions of the censoring authorities. Kerekes (1998), p. 76-90 

==Release==
The film premiered in June 1991 in Germany on VHS.  Cult Epics released the film in a limited edition on February 10, 2015 on DVD and Blu-ray Disc in the United States.  The Blu-ray featured extensive Bonus features; including  a new Introduction by Jörg, Audio Commentary, Trailers of Jörg Buttgereits films, The Making Of, Still PHoto Gallery, new Shorts by Jörg, and the Score (original and live) of Nekromantik 2. 

==Sources==
*  
*  

==References==
 

==External links==
*  
*  
*   (German and English)

 
 
 
 
 
 
 
 
 
 
 
 
 
 