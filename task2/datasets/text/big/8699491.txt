The Confession (1970 film)
{{Infobox film
| name           = The Confession
| image          = LAveu.jpg
| image_size     =
| writer         = Jorge Semprún Artur London (book LAveu)
| starring       = Yves Montand Simone Signoret Gabriele Ferzetti
| director       = Costa-Gavras
| producer       = Robert Dorfmann Bertrand Javal
| cinematography = Raoul Coutard
| editing        = Françoise Bonnot
| distributor    =Paramount Pictures
| released       =  
| runtime        = 139 min French
| budget         =
}}
The Confession ( ) is a 1970 French-Italian film directed by Costa Gavras and stars Yves Montand and Simone Signoret.

It is based on the true story of the Czechoslovak communist Artur London, a defendant in the Slánský trial. Gavras did not intend the film as an anti-communist film but a plea against totalitarianism and particularly Stalinism. 

==Plot==

The film is about Anton Ludvik, alias Gerard, vice-minister of Foreign Affairs of Czechoslovakia. He realizes he is being watched and followed. One day, he is arrested and put into jail by an organisation that declares itself "above the ruling party" and put in solitary confinement for months without being told the reason why. Through brainwashing techniques, including sleep deprivation and being forced to walk back and forth all the time, he is slowly pressured into confessing imaginary crimes, including treason, and to repeat this confession in a public court. Years later, he meets his now demoted tormentor, who tries to downplay his role at that time.

Yves Montand lost more than 15 kilos to play his role. Montand had been shaken by the   ." 

== Cast ==
* Yves Montand – Arthur London
* Simone Signoret – Lise London
* Michel Vitold – Smola
* Gabriele Ferzetti – Kohoutek
* Jean Bouise 
* Marcel Cuvelier
* Gérard Darrieu
* Gilles Segal
* Henri Marteau
* Jean Lescot
* Michel Beaune 
* Jacques Rispal
* Michel Robin 
* Georges Aubert
* Marc Bonseignour
* Thierry Bosc
* André Cellier
* Monique Chaumette
* Jean-Paul Cisife
* Pierre Decazes
* Pierre Delaval
* Basile Diamantopoulos
* Jacques Emin
* Marc Eyraud
* Jean-François Gobbi
* Maurice Jacquemont
* William Jacques
* Jean-Pierre Janic
* Patrick Lancelot
* Guy Mairesse
* François Marthouret
* Pierre Moncorbier
* Charles Moulin
* Umberto Raho
* Paul Savatier Laszlo Szabo
* Claude Vernier
* Nicole Vervil
* Pierre Vielhescaze
* Antoine Vitez
* Jacques Marbeuf

==Awards== Best Foreign Language Film.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 