Metro Manila (film)
{{Infobox film
| name           = Metro Manila
| image          = Metro Manila Poster.jpg
| alt            =  
| caption        = 
| director       = Sean Ellis
| producer       = Mathilde Charpentier Sean Ellis
| writer         = Sean Ellis Frank E. Flowers
| starring       = Jake Macapagal Althea Vega John Arcilla
| music          = Robin Foster
| cinematography = Sean Ellis
| editing        = Richard Mettler
| studio         = Chocolate Frog Films
| sales          = Independent (World wide) ICM (US)
| distributor    = Independent (UK) Haute et Court (France) Captive (Philippines) Festival (Spain)
| released       =  
| runtime        = 115 minutes
| country        = United Kingdom
| language       = Filipino
| budget         = 
| gross          = 
}} Best Foreign Language Film at the 86th Academy Awards,    but it was not nominated.

==Cast==
* Jake Macapagal as Oscar Ramirez
* Althea Vega as Mai Ramirez
* John Arcilla as Ong
* Leon Miguel as White-eyed Man

==Plot== manipulative ways of city hucksters. Oscar lands a job in an armored truck company. He befriends his senior officer, Ong, who takes Oscar under his wing.

Soon, it becomes apparent that Ong has been waiting for someone like Oscar for a long time.

==Production==
The film was shot on location in the Philippines in 2011 with a Filipino cast and crew members. The Filipino language is used throughout the film.

==Release==
Metro Manila had its International premiere at the 2013 Sundance Film Festival  on 20 January 2013. It was also released on 17 July 2013 in France, 28 August 2013 in Belgium, 29 August 2013 in the Netherlands, and 20 September 2013 in the UK.

Metro Manila had its Philippine premiere on 9 October 2013.

Metro Manila was re-released with special screenings to raise money for the victims of Typhoon Haiyan/Yolanda that had hit the Philippines and killed close to 6000 people. Its British director, Sean Ellis said: "The people of the Philippines were tremendously supportive during the making of Metro Manila, and its only right that we should now use the film to raise money to help the victims of this terrible disaster."

==Critical Reception==
Rotten Tomatoes lists a rating of 93% based on 29 reviews as of October 2014. 

After winning the Hamburg Film Critic Award at the 2013 Filmfest Hamburg, the jury said of the film: “The themes of our times are what define this film: rural exodus and impoverishment, exploitation and poverty in the Moloch of overcrowded metropolises. Director Sean Ellis filmed this story in a language that is foreign to him - and yet still always manages to hit the right tone. He is emotional, yet never impassioned; poetic, yet never tawdry; raw without any hint of cynicism. A social drama that becomes a thriller, breathless and unstoppable. “Metro Manila” deserves to be seen by many. This film belongs in the cinema. ...” 

==Accolades==
At the 2013 British Independent Film Awards (BIFA) Metro Manila was nominated in five categories and won awards for Achievement in Production, Best Director and Best British independent film. 

 
 
{| class="wikitable"
|- style="text-align:center;"
! colspan=3 style="background:#B0C4DE;" | List of Accolades
|- style="text-align:center;"
! style="background:#ccc;" width="55%"| Award / Film Festival
! style="background:#ccc;" width="40%"| Category
! style="background:#ccc;" width="20%"| Result
|- style="border-top:2px solid gray;"
|- British Academy Film Awards 
| Best Film Not in the English Language
|  
|- 2013 Sundance Film Festival 
| Audience Award: World Dramatic
| 
|-
| Grand Jury Prize: World Dramatic
| 
|- Filmfest Hamburg  Hamburg Film Critic Award
|  
|- Polar Festival de Cognac  Grand Jury Prize
|  
|- Seminci  Espiga de Oro: Gold Spike
|  
|- Espiga de Plata: Silver Spike
|  
|- Best Director
|  
|- Best Script
|  
|- Best Cinematography
|  
|- Amazonas Film Festival  Best Director
|  
|- Best Script
|  
|- Audience Award
|  
|- British Independent Film Awards Best British Independent Film
|  
|-
| Best Director
|  
|-
| Best Supporting Actor - John Arcillia
|  
|-
| Most Promising Newcomer - Jake Macapagal
|  
|-
| Achievement in Production
|  
|- Satellite Award  Best Foreign Language Film 
|  
|- World Soundtrack Awards  Public Choice Award 
|  
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of British submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 