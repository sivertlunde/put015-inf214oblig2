Raptor (film)
{{infobox television film
| name          = Raptor
| image         = Raptor 2001 dvd.jpg
| director      = Jim Wynorski 
| producer      = Roger Corman
| starring      = Eric Roberts Melissa Brasselle Corbin Bernsen
| released      = November 6, 2001
| runtime       = 81 minutes 
}}
Raptor is a 2001  .

==Plot==
When a series of unexplained vicious animal attacks strikes his community, Sheriff Jim Tanner and his assistant Barbara trace them back to a Dr. Hyde, a former military researcher whose government funding for a dinosaur cloning project was cut.  When the Pentagon discovers Hyde obtained foreign backing to continue his experiments, they send in a strike team to save Tanner and Barbara and stop Hyde.

==Cast==
* Eric Roberts as Sheriff Jim Tanner	
* Melissa Brasselle as Barbara Phillips
* Corbin Bernsen as Dr. Frank Hyde
* Tim Abell as Captain Connelly 
* William Monroe as Captain York
* Lorissa McComas as Lola Tanner, daughter of sheriff Jim Tanner
* Teresa DePriest as Karen Konbell, assistant of Dr. Frank Hyde

==Reception== IMDb as well as a "rotten" rating on review aggregator Rotten Tomatoes of 19%. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 