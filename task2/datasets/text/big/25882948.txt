Prowl (film)
{{Infobox film
| name           = Prowl
| image          = Prowl poster.jpg
| caption        = Theatrical poster
| director       = Patrik Syversen
| producer       =  
| writer         = Tim Tori
| starring       =  
| music          = Theo Green
| cinematography = Håvard Andre Byrkjeland
| editing        = Celia Haining
| studio         =  
| distributor    = After Dark Films
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
}}
Prowl is a 2010 American horror film directed by Patrik Syversen and written by Tim Tori  and starring Courtney Hope, Ruta Gedmintas and Bruce Payne. 

==Cast==

* Courtney Hope as Amber
* Ruta Gedmintas as Suzy
* Joshua Bowman as Peter
* Bruce Payne as Bernard
* Jamie Blackley as Ray
* Oliver Hawes as Eric
* George Oliver as Runt
* Saxon Trainor as Veronica
* Perdita Weeks as Fiona
* Velizar Peev as Creature 4

==Synopsis==
The film concerns a young lady named Amber, who dreams of escaping her small town existence. She therefore persuades her friends, Peter, Suzy, Eric, Fiona and Ray to accompany her to find an apartment in the big city, Chicago. Unfortunately, the vehicle transporting Amber and her friends to Chicago breaks down and as a consequence they gratefully accept a ride in the back of a semi driven by a "blatantly untrustworthy truck driver"  named Bernard. However, Amber and her friends become concerned when Bernard refuses to stop and they discover that the cargo consists of hundreds of cartons of blood. Concern turns to panic and terror when Bernard ends up taking them into an abandoned meat-packing plant that is now a training ground for a group of bloodthirsty creatures. The leader of the creatures, Veronica, wants them to learn how to hunt human prey and has coaxed Bernard into helping her. Amber and her friends attempt to evade the creatures in order to survive.

==Production==
After Dark Films began with the shooting of the splatter film in August 2009 in Sofia, Bulgaria. After Dark produced the film in association with Dobré Films and in cooperation with Lionsgate and NBC Universal’s fantasy Channel Syfy.

==Release==
The film premiered on September 4, 2010 and the theatrical release occurred in January 2011. The DVD was released in April 2011.

==Reception==

The film has received mixed reviews. Matt Withers, who reviewed the film for JoBlo.com, praised both the cast, and the character development. He stated that Courtney Hope is a believable leading lady and that Bruce Payne shows up as a trucker in a throwaway role that he makes anything but. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 