Ungli
{{Infobox film
| name = Ungli
| image =Ungli 2.jpeg
| alt =
| caption =
| director = Rensil DSilva
| producer = Hiroo Yash Johar Karan Johar Apoorva Mehta
| screenplay = Rensil DSilva
| writer = Rensil DSilva  Milap Zaveri  (dialogues) 
| starring =  
| music = Salim-Sulaiman Sachin-Jigar Gulraj Singh Aslam Keyi Background Score:  John Stewart Eduri
| cinematography = Hemant Chaturvedi
| editing = Deepa Bhatia
| studio = Dharma Productions
| distributor = AA Films
| released =  
| runtime = 114 minutes  
| country = India
| language = Hindi
| budget = 15 Crore                                                                           | box office = 20 crore                                                         
| gross =
}}
 Hindi comedy comedy drama film directed by Rensil DSilva and produced by Karan Johar under Dharma Productions. The film stars Emraan Hashmi, Randeep Hooda, Kangana Ranaut, Neha Dhupia, Angad Bedi, Neil Bhoopalam and Sanjay Dutt.  The film was released on 28 November 2014. Mid-day gave the film 3 out of 5 stars highlighting the direction and performances. 

== Plot ==
The movie starts off with 4 friends Kalim (Angad Bedi), Gautam (Neil Bhoopalam), Maya (Kangana Ranaut) and Abhay (Randeep Hooda) starting a gang called the "Ungli" gang which fights against corruption. Their job is to bug corrupt people and teach them moral lessons quite unconventionally. They target a corrupt minister and become the most wanted people in Mumbai. Their case is the responsibility of ACP Ashok Kaale (Sanjay Dutt), whose pal Nikhil Abhyankar (Emraan Hashmi) shortly joins the gang. But soon, after he faces trouble, Kaale is compelled to seek help from the gang when Nikhil helps him expose the gangs greatest target, B.R. Dayal (Mahesh Manjrekar) and his son Anshuman. Kaale instigates a sting operation against the Mumbai police and is successful in preventing his transfer. Kaale thanks the "Ungli" gang and he assures them that the city wont need an "Ungli" gang as long as the administrations are running properly in accordance to public demands.

== Cast ==
* Emraan Hashmi as Nikhil Abhyankar
* Randeep Hooda as Abhay
* Kangana Ranaut as Maya
* Neha Dhupia as Teestha
* Angad Bedi as Kalim
* Neil Bhoopalam as Gautam
* Sanjay Dutt as ACP Ashok Kale
* Arunoday Singh as Ricky
* Raza Murad as Commissioner
* Mahesh Manjrekar as B.R. Dayal
* Kader Khan
* Shraddha Kapoor in a special appearance
* Shiv Kumar Subramaniam as DCP Shivraman
*   Charanpreet Singh as Anshuman Dayals Friend

== Reception ==
Mid-day said the film would have been "wonderful" except for the dialogues, which it said were sometimes "downright idiotic".   

== Soundtrack ==
{{Infobox album
| Name = Ungli
| Artist = Salim-Sulaiman, Sachin-Jigar, Gulraj Singh and Aslam Keyi.
| Cover =
| Border =
| Alt =
| Caption =
| Released =  November 2014
| Recorded =
| Genre = Film soundtrack
| Length = 18:53 Hindi
| Label = Sony Music
| Producer = Karan Johar
| Chronology = Salim-Sulaiman
| Type = Soundtrack
| Last album = Mardaani (2014)
| This album = Ungli (2014)
| Next album = 22 Yards (2015)
{{Extra chronology
| Artist = Sachin-Jigar
| Type = Soundtrack
| Last album = Happy Ending (2014)
| This album = Ungli (2014)
| Next album =
}}
}}

The music of Ungli has been composed by Salim-Sulaiman, Sachin-Jigar, Gulraj Singh, Aslam keyi.

The song "Dance Basanti" was the first single track starring Shraddha Kapoor and Emraan Hashmi released on 28 October 2014.  The song is sung by Vishal Dadlani and Anushka Manchanda, composed by Sachin-Jigar with lyrics written by Amitabh Bhattacharya. "Pakeezah" the second single track picturised on Emraan Hashmi and Kangana Ranaut released on 3 November 2014. The song is sung and composed by Gulraj Singh with the lyrics of Manoj Yadav. "Ungli Pe Nachalein" the title track picturised on Emraan Hashmi released on 11 November 2014, sung by Dev Negi & Shipra Goyal, written by Kumaar and composed by Aslam Keyi.

The soundtrack comprises 5 tracks.

=== Track listing ===
{{track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length =  
| music_credits = yes
| title1 = Dance Basanti
| extra1 = Vishal Dadlani, Anushka Manchanda
| music1 = Sachin-Jigar
| lyrics1 = Amitabh Bhattacharya
| length1 = 3:43
| title2 = Paakeezah
| extra2 =  Gulraj Singh
| music2 = Gulraj Singh
| lyrics2 = Manoj Yadav
| length2 = 4:25
| title3 = Ungli Pe Nachalein (Title Track)
| extra3 = Dev Negi, Shipra Goyal
| lyrics3 = Kumaar
| music3 = Aslam Keyi
| length3 = 3:39
| title4 = Aadarniya Ungli
| extra4 = Vishal Dadlani, Neeti Mohan
| lyrics4 = Manoj Yadav
| music4 = Gulraj Singh
| length4 = 3:46
| title5 = Aulia
| extra5 = Armaan Malik
| lyrics5 = Amitabh Bhattacharya
| music5 = Salim-Sulaiman
| length5 = 3:21
}}

== References ==
 

== External links ==
*  

 
 