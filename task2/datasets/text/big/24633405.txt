Harem Suare
{{Infobox film
| name           = Harem Suare
| image	         = Harem Suare FilmPoster.jpeg
| caption        = Film poster
| director       = Ferzan Özpetek
| producer       = Abdullah Baykal
| writer         = Ferzan Özpetek Gianni Romoli
| starring       = Marie Gillain
| music          = Pivio and Aldo De Scalzi
| cinematography = Pasquale Mari
| editing        = Mauro Bonanni
| distributor    = Medusa Film
| released       =  
| runtime        = 125 minutes
| country        = Turkey Italy France
| language       = Turkish Italian French
| budget         = 
}}

Harem Suare is a 1999 Turkish drama film directed by Ferzan Özpetek. It was screened in the Un Certain Regard section at the 1999 Cannes Film Festival.   

==Plot==
The old Safiye is telling her granddaughter the love that she has lived a long time ago: the beginning of 1900. The beautiful young Safiye is the favorite of the sultan, a man tormented by the crisis of the monarchy and the displacement of the Ottoman Empire in Turkey. Safiye is the most beautiful girls of the harem of the Sultan, but the girl is in love with the young and beautiful Nadir, a eunuch of the personal guard of the sultan. The two young lovers together design a good future, but the war breaks out, and the girl is forced to escape from his country. Arrived in Italy, Saiye is forced to sell her beauty, performing on stage in operas, including those of Giacomo Puccini.

==Cast==
* Marie Gillain as Safiye
* Alex Descas as Nadir
* Lucia Bosé as Old Safiye
* Valeria Golino as Anita
* Malick Bowens as Midhat
* Christophe Aquillon as Sumbul
* Serra Yılmaz as Gulfidan
* Haluk Bilginer as Abdulhamit
* Pelin Batu as Cerkez Cariye
* Nilufer Acikalin as Selma (as Nilüfer Açikalin)
* Ayla Algan as Valide
* Meriç Benlioğlu
* Cansel Elcin as Journaliste
* Başak Köklükaya as Gulbahar
* Gaia Narcisi as Aliye
* Selda Özer as Guya

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 