CyberTracker (film)
{{Infobox film
| name           = CyberTracker
| image          = 
| alt            =  
| caption        = 
| director       = Richard Pepin
| producer       = Joseph Merhi Richard Pepin
| writer         = Jacobsen Hart
| starring       = Don "The Dragon" Wilson
| music          = Lisa Popeil, Bill Montei
| cinematography = Ken Blakey
| editing        = Chris Maybach, Chris Worland
| studio         = 
| distributor    = PM Entertainment Group
| released       =   
| runtime        = 91 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}} science fiction Richard Norton, Steve Burton, Abby Dalton, and Jim Maniaci.

The film was followed by a 1995 direct-to-video sequel, Cyber-Tracker 2, also starring Wilson, Foster, Burton, and Maniaci.

==Plot synopsis==
In the near future, Eric Phillips is one of the bodyguards for Senator Robert Dilly (John Aprea), the champion of a recently implemented "computerized justice system", a product of CyberCore Corporation. It uses data of evidence to determine the guilt of accused criminals, then carries out the sentence using cyborg executioners called "Core Trackers" (Maniaci).

However, the more Phillips learns about Dilly and the CyberCore Corporations ruthless plans, the more uncomfortable he becomes and he refuses to go along with the murder of a corporate spy. This forces Dilly and CyberCore to frame Phillips with the murder and release a Core Tracker to execute him. Phillips defeats the Core Tracker but is taken by a group of underground rebels called the "Union for Human Rights". The group is secretly led by popular news journalist Connie (Foster).

While being tracked by another Core Tracker and Dillys head bodyguard (Norton), Phillips and Connie are able to break into CyberCore and steal secret files revealing that Senator Dilly is in fact a cyborg. Phillips defeats the bodyguard and yet a third Core Tracker and then infiltrates a press conference to shoot Dilly, publicly revealing his mechanical nature. This, along with everything else Connies group has discovered, causes the computerized justice system to be shut down and CyberCore to collapse.

==External links==
* 

 
 
 
 
 
 


 