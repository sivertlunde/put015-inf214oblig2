The Age of Innocence (1934 film)
{{Infobox film
| name           = The Age of Innocence
| image          = 
| image size     = 
| caption        = 
| director       = Philip Moeller
| producer       = Pandro S. Berman
| writer         = Edith Wharton (novel) Margaret Ayer Barnes (play)
| based on       = The Age of Innocence (1920 novel) The Age of Innocence (1928 play)
| narrator       = John Boles
| screenplay     = Sarah Y. Mason Victor Heerman John Boles Lionel Atwill
| music          = Max Steiner (uncredited)
| cinematography = James Van Trees
| editing        = George Hively
| distributor    = RKO Radio Pictures
| released       = September 14, 1934
| runtime        = 81 min.
| country        = United States
| language       = English
}}

The Age of Innocence (1934) is an American   with Katharine Hepburn.

The novel was made into the silent film version The Age of Innocence (1924 film) starring Beverly Bayne, and a 1993 film version The Age of Innocence (1993 film) starring Michelle Pfeiffer. The 1928 Broadway stage adaptation starred Katharine Cornell. 

==Plot summary==
  John Boles) is surprised to meet his childhood friend Ellen (Irene Dunne), beautiful and grown up and now Countess Olenska.  Olenska is the cousin of his fiancee May (Julie Haydon) and is considered scandalous by the strait-laced society of the time.  Newland, however, treats her well and sends her two dozen yellow roses. Olenska turns to Newland for advice about a possible divorce. 

==Cast==
* Irene Dunne as Countess Ellen Olenska John Boles as Newland Archer
* Lionel Atwill as Julius Beaufort
* Helen Westley as Granny Manson Mingott
* Laura Hope Crews as Augusta Welland
* Julie Haydon as May Welland
* Herbert Yost as Howard Welland
* Theresa Maxwell Conover as Mrs Archer
* Edith Van Cleve as Jane Archer
* Leonard Carey as Jasper, the Butler

==Reception==
The film was a box office disappointment. 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 