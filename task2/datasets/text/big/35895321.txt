Vida de Menina
{{Infobox film
| name           = Vida de Menina
| image          = 
| alt            =  
| caption        = 
| director       = Helena Solberg
| producer       = David Meyer
| writer         = Elena Soárez Helena Solberg
| based on       =  
| starring       = {{Plainlist|
*Ludmila Dayer
*Daniela Escobar
*Dalton Vigh
}}
| music          = Wagner Tiso
| cinematography = Pedro Farkas
| editing        = Diana Vasconcellos
| studio         = Radiante Filmes
| distributor    = Riofilme
| released       = 2003
| runtime        = 90 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
Vida de Menina is a 2003 Brazilian drama film. It was the first feature-length fiction film directed by Helena Solberg. The film is an adaptation of the book Minha Vida de Menina by Helena Morley.

It won five awards at the 2004 Gramado Film Festival. 

== Cast ==
* Ludmila Dayer as Helena Morley
* Daniela Escobar as Carolina
* Dalton Vigh as Alexandre
* Maria de Sá as Teodora
* Camilo Bevilacqua as Geraldo
* Lolô Souza Pinto as Madge
* Benjamim Abras as Teodomir
* Lígia Cortez as Iaiá

==References==
 

==External links==
* 

 
 
 
 
 
 

 