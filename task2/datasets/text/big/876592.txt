It Came from Beneath the Sea
 
{{Infobox film| name = It Came from Beneath the Sea
  | image = It Came From Beneath The Sea poster.jpg Theatrical release title lobby card
  | director = Robert Gordon
  | producer = Charles H. Schneer
  | writer = Hal Smith George Worthing Yates
  | starring = Kenneth Tobey Faith Domergue Donald Curtis
  | music = Mischa Bakeleinikoff
  | cinematography = Henry Freulich
  | editing = Jerome Thoms
  | studio      = Clover Productions
  | distributor = Columbia Pictures
  | released = July 1955 (U.S. release)
  | runtime = 79 min.
  | country = United States
  | language = English
  | budget = $150,000 
| gross = $1.7 million (US)  
  }} American black-and-white science fiction giant monster film from Columbia Pictures, produced by Sam Katzman and Charles Schneer, directed by Robert Gordon, and starring Kenneth Tobey, Faith Domergue, and Donald Curtis. The script by George Worthing Yates was designed to showcase the stop motion animation effects of Ray Harryhausen.

==Plot==
A nuclear submarine on maneuvers in the Pacific Ocean, captained by Commander Pete Mathews (Kenneth Tobey), comes into contact with a massive sonar contact. The boat is disabled but manages to free itself and return to Pearl Harbor. Animal tissue of great proportions is discovered jammed in its dive planes. A man-and-woman team of marine biologists, Lesley Joyce (Faith Domergue) and John Carter (Donald Curtis), is called in; they identify the tissue as being part of a gigantic octopus. The military authorities scoff, but are finally persuaded after receiving reports of missing swimmers and ships at sea being pulled under by a large animal. Both scientists conclude the sea beast is from the Mindanao Deep, having been forced from its natural habitat by hydrogen bomb testing in the area, which has made the giant creature radioactive, driving off its natural food supply. 

The scientists suggest the disappearances of a Japanese fishing fleet and a Siberian seal boat may be the work of the foraging sea beast. Both Pete and the Navy representatives express doubt and demand further proof. Later, as Pete assists John and Lesley, a report comes in of an attack on a French shipping boat; several men escaped in a raft. The French survivors are questioned by psychiatrists, and when the first sailors description of a creature with giant tentacles is met with skepticism, the other sailors refuse to testify. Lesley is able to convince the first sailor to repeat his story for government officials, who then have the evidence they need. The U. S. government halts all sea traffic in the North Pacific without revealing the reason. John flies out to sea to trace a missing ship, while Pete and Lesley follow up on a report of three missing people on the coast of Oregon.

The local sheriff, Bill Nash (Harry Lauter), takes Pete and Leslie to the site of the attack, where they find a giant suction cup imprint in the beach sand; they then request that John join them. Bill is later attacked along the beach by the giant octopus, right in front of the two scientists. He escapes, and together they hastily arrange for all Pacific coast waters to be mined before departing for San Francisco and the Navys headquarters. An electrified safety net is strung underwater across the entrance to San Francisco Bay to protect the Golden Gate Bridge, which has also been electrified. John takes a helicopter along the shoreline and baits the sea with dead sharks in an effort to lure the sea beast inland. Lesley demonstrates to reporters a special jet-propelled atomic torpedo, which they hope to fire at the creature, while driving it back to the open sea before detonating the weapon. Later that day, the giant octopus demolishes the underwater net, irritated by the electrical voltage, and heads toward San Francisco.
  Embarcadero and Ferry Building, which is battered by the beasts giant tentacles. When more people are attacked and killed, the Defense Department authorizes Pete to take out the submarine and fire the torpedo; John joins Pete while Lesley remains at the base. Flame throwers push the beasts giant tentacles back into the sea, but when Pete fires the jet torpedo into the creature, it grabs the submarine. Using an aqua-lung|aqualung, Pete swims up to the massive creature and places explosive charges on its huge body before being knocked out by its flailing tentacles. John then swims out and shoots at one of its eyes, forcing the beast to release the submarine; he then pulls Pete to safety. Back at the base, as the creature turns toward the open sea, the torpedo is detonated, completely destroying the giant Cephalopod. 

==Cast==
*Kenneth Tobey as Commander Pete Mathews
*Faith Domergue as Professor Lesley Joyce
*Donald Curtis as Dr. John Carter
*Ian Keith as Admiral Burns
*Dean Maddox Jr. as Admiral Norman
*Chuck Griffiths as Lieutenant Griff

==Production==
Much of the filming was done at the San Francisco Naval Shipyard, including scenes aboard a submarine, and several naval personnel were given supporting roles. Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol I: 1950 - 1957, McFarland, 1982. ISBN 0-89950-032-3.  
 Creature with the Atom Brain on a theatrical release double bill. 

To keep shooting costs low, director Robert Gordon shot inside an actual submarine, both above and under water, using handheld cameras. For a scene that takes place on a stretch of Pacific coastline, Gordon and his crew dumped several truckloads of sand onto a sound stage at Columbia, which they backed with a rear projection screen. During their scene together, Kenneth Tobey found himself sinking through the sand to the point of appearing shorter than Faith Domergue on camera, forcing him to dig himself out of the hole between every take. A more extensive love scene had been written for the characters but was literally torn out of the shooting script by Sam Katzman, to keep principal photography from going over schedule.. Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol I: 1950 - 1957, McFarland, 1982. ISBN 0-89950-032-3.  

The octopus stop-motion effects were designed and animated by Ray Harryhausen. The effects budget, however, was getting slightly out of hand, and for this reason, Sam Katzman allowed Harryhausen only enough money for animating six of the octopus eight tentacles; two were eliminated on the final shooting miniature. Harryhausen jokingly named his giant octopus "the sixtopus" (this behind-the-scenes detail was revealed years later in a science fiction magazine). For the scenes where a single tentacle is seen moving up and around the bridge superstructure, Harryhausen used a single large model tentacle instead of employing the complete animation model. Some of the bridge scenes employ a shooting miniature of a bridge support, which was then composited in post-production over live footage of the real support; this is the bridge section that the "sixtopus" is seen clinging to in the final scene. 

==See also==
* List of stop-motion films

==References==
 

==External links==
*  
*  
*  
* Chris Noeth  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 