True Tales of Slaughter and Slaying
 
{{Infobox Film name            = True Tales Of Slaughter And Slaying 
| image          = Macabre_-_True_Tales_Of_Slaughter_And_Slaying.jpg Macabre
| distributor    = Decomposed Records
| released       = July 11, 2006
| runtime        = 1:26:05
| rating         = 
| country        =   English
}}
 Macabre performing on stage in Dordrecht, Holland.
This is the bands debut DVD released for their 20th anniversary.

==Track listing==
# Zodiac
# Jack the Ripper
# Serial Killer
# The Wustenfeld Man Eater
# Dog Guts
# Fritz Haarman der Metzger
# Coming to Chicago
# The Vampire of Düsseldorf
# Acid Bath Vampire
# Ed Gein
# Dr. Holmes
# Fatal Foot Fetish & The Diary of Torture
# Scrub A Dub Dub
# Mary Bell
# The Hitchhiker
# Albert Was Worse Than Any Fish In The Sea
# Dog Guts
# Hitchhiker
# Mary Bell
# Drill Bit Lobotomy

==Extra features==
*Recorded in 5.1 surround sound
*Multi-Camera footage
*Behind the scenes & out and about with Corporate Death and Dennis The Menace
*Tracks 14-17 are bonus tracks.
*Tracks 17-20 are songs from Biebob, Vosselaar on the Dahmer Tour.
*Limited edition DVD comes complete in a full color metal box.

 
 
 
 
 


 