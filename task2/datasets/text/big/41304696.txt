See You Tomorrow, Everyone
{{Infobox film
| name           = See You Tomorrow, Everyone
| image          = 
| caption        = 
| director       = Yoshihiro Nakamura
| producer       = 
| based on       =  
| writer         = Yoshihiro Nakamura
| starring       = Gaku Hamada Kana Kurashina Kento Nagayama
| music          = 
| cinematography = Gen Kobayashi
| editing        = Toshirô Matsutake
| studio         = Dub Smoke
| distributor    = Phantom Films
| released       =  
| runtime        = 120 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
See You Tomorrow, Everyone, in Japanese   is a 2013 Japanese film directed by Yoshihiro Nakamura starring Gaku Hamada. It was released in Japan on January 26, 2013 and in the USA at the Hawaii International Film Festival on October 12, 2013. 

==Plot==
Starting in 1981, the film follows the life of Satoru Watarai (Hamada) from 12 to 30 years old.  After dropping out of Junior High School,  Watarai has resolved never to set foot outside his danchi (Japanese apartment complex).  As he watches his classmates gradually move away, he begins a daily routine of exercise, martial arts and security patrols which he believes will protect the danchi and preserve its way of life.  The film also explores Watarais struggle to build relationships, make a living and deal with the dilapidation of the complex in the wake of the collapse of Japanese asset price bubble|Japans bubble economy.

==Cast==
* Gaku Hamada as Satoru Watarai
* Kana Kurashina as Saki Ogata
* Kento Nagayama as Noriaki Sonoda
* Haru as Yuri Nagashima
* Nene Otsuka as Hina Watarai

==Reception==
In a review for the website efilmcritic.com, Jay Seaver praises Hamadas ability to reflect the characters development from a Junior High School student to a 30 year old man.   This was done without the use of make-up effects. 

==References==
 

==External links==
*  

 

 
 
 

 