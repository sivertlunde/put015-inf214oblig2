Second Time Around (film)
 
 
{{Infobox film
 | name = Second Time Around
 | image = 
 | caption = 
 | director = Jeffrey Lau
 | producer = Johnnie To
 | writer = Jeffrey Lau
 | starring = Ekin Cheng Cecilia Cheung
 | music = Chiu Tsang-Hei Anthony Chue  
 | cinematography = Johnny Koo
 | editing = Wong Wing-Ming
 | distributor = China Star Entertainment Group
 | released =  
 | runtime = 99 min.
 | country = Hong Kong Cantonese English English
 | budget = 
 }} 2002 Cinema Hong Kong film starring Ekin Cheng, Cecilia Cheung and Jonathan Ke Quan.

==Plot==
 

During a trip to Las Vegas with his best friend, gambler Sing Wong (Jonathan Ke Quan) loses all his money. The pair learn about Ren Lee (Ekin Cheng), a man who controls a way of going back in time, from a young woman.
Both his best friend and the woman die in a car accident. Sing is the sole survivor. 
Sing, now pursued by policewoman Tina Chow (Cecilia Cheung), is led by Ren Lee through various parallel universes and in this process he not only changes himself and saves his friends life but also falls in love with Tina.

==Awards==
The film won the Film of Merit prize at the 2003 Hong Kong Film Critics Society Awards.  

==References==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 
 
 

 