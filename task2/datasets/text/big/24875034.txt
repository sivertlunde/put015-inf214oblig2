Under the Mud
{{Infobox film
| name           = Under the Mud
| image          = Under The Mud Movie Poster.jpg
| caption        = 
| director       = Solon Papadopoulos
| producer       = Solon Papadopoulos, Julie Currie, Roy Boulter 
| writer         = Sophia Barlow, Roy Boulter, David Catterall, Mick Colligan, Julie Currie, Howard Davies, David Dearden, Dave Hart, Ged Hunter, Graham Mullen, Solon Papadopoulos, Natalie Southern, Tanya Taylor, Lenny Wood
| narrator       = Lenny Wood Andrew Schofield, Lisa Parry, Lauren Steele, Jasmine Mubery
| music          = Pete Wylie
| cinematography = 
| editing        = 
| distributor    = Hurricane Films
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = Pound sterling|£85,000
| gross          = 
}} fantasy sequences, and has been described by its producers as “social surrealism”. 

==Plot== Andrew Schofield), mother Sally (Lisa Parry), teenagers Paul (Dave Hart) and Paula (Lauren Steele), young children Olivia (Jasmine Mubery) and Karl (Adam Bailey), and the Potts’s part-time lodger, Magic (Lenny Wood).

On the day of Olivia’s Communion, the family is prepared for a large celebration, for which they have arranged a party in a local social club and ornamented Olivia with electrically powered angel wings.

As Joe gets drunk and his attempts to enliven the social atmosphere with jokes and banter fall flat, the cracks in the family’s relationships are revealed: Sally wonders whether she is still beautiful or appreciated; Paula, who throughout the film talks out loud to and continually confers with her imaginary friend Georgina, worries over the news that her beloved boyfriend DJ Worm (Mick Colligan) is planning to move to Ibiza without telling her; Magic tries to console her while suppressing his obvious attraction to Paula.

When Paula is entrusted with taking Olivia and Karl back to the family home, the film takes more turns for the dramatic. Paula suspects she might be pregnant and desperately attempts to find DJ Worm. As Paula is out of the house, Olivia and Karl steal a car and go for a joyride. Magic becomes frustrated in his attempts to steer Paula towards level-headedness and find the missing children. At the social club, Joe and Sally find their relationship strained to the breaking point by both their own failure to communicate and by the charms of local gangster One Dig (James McMartin), who dated Sally years ago and is still willing to offer her a wedding ring.

The film builds to a climax in which Magic is forced to confront his true feelings for Paula, Paula herself has to confront the way she is living her life, and Joe and Sally are forced to decide whether they will commit to each other or break apart their family. Along the way there are numerous larger-than-life moments, including a slapstick car chase, another chase involving mobile airplane steps and a taxi driver who claims to be clairvoyant.

==Production==
The film was written by fifteen Liverpool young people who had no previous experience in screenwriting, in conjunction with the established Liverpool production company Hurricane Films, through a community-based Internet café called Interchill.

Although initially declined funding by BBC Films and Channel 4, the project received some pilot project support from the UK Film Council, and the filmmakers managed to secure a budget of £85,000 through a grant from pharmaceutical company GlaxoSmithKline. Production began in 2004 and was completed in 2006.

==Release==
Under the Mud played at several international film festivals, including the Victoria Film Festival, Hollywood Film Festival and Cambridge Film Festival, but did not receive theatrical distribution in either the UK or abroad. Hurricane Films created a distribution arm to release the film on DVD on 2 November 2009. The film was picked up by the BBC in 2011 and was aired on BBC One on 14 January 2011.

==Reception==
National press coverage of Under the Mud was limited, due to the lack of a theatrical release, but mostly positive. Helen Walsh in The Guardian wrote that it “may be the best British film you’ll never see”,  while Robert Epstein wrote in The Independent “this is both an accomplished social project and charming fairytale” and proposed that the film plays “like a Scouse Shameless (UK TV series)|Shameless.” 

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 
 