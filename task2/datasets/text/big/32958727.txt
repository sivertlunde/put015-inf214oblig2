Everlasting Love (film)
 

{{Infobox film
| name           = Everlasting Love
| image          = EverlastingLove.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 停不了的愛
| simplified     = 停不了的爱
| pinyin         = Tíng Bù Liǎo De Ài
| jyutping       = Ting4 Bat1 Liu2 Dik1 Ngoi3 }}
| director       = Michael Mak
| producer       = Johnny Mak
| writer         = 
| screenplay     = Manfred Wong
| story          = 
| based on       = 
| narrator       = 
| starring       = Andy Lau Irene Wan Loletta Lee Ng Man-tat
| music          = Lam Miu Tak
| cinematography = Gray Hoh
| editing        = Fan Kung Ming
| studio         = Johnny Mak Production Golden Harvest
| released       =  
| runtime        = 98 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$10,435,613
}}
Everlasting Love (停不了的愛) is a 1984 Hong Kong romance drama film directed by Michael Mak and starring Andy Lau.

==Cast==
*Andy Lau as Eric
*Irene Wan as Pauline Leung Pui Kwun
*Loletta Lee as Lulu Leung
*Ng Man-tat as Bob

==Reception==
The film performed well in Hong Kong, grossing HK$10,435,613 during its theatrical run from 22 March to 11 April 1984.

===Awards===
4th Hong Kong Film Awards 
* Nominated: Best Screenplay - Manfred Wong

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 