Délice Paloma
{{Infobox Film
| name           = Délice Paloma 
| image          = 
| caption        = 
| director       = Nadir Moknèche
| producer       = Sunday Morning Productions
| writer         = Nadir Moknèche
| starring       = Biyouna
| distributor    = Les Films du Losange
| released       =  
| runtime        = 134 minutes
| country        = Algeria France
| language       = French
| budget         = € 6.50 million
| gross          = € 29 million
| screenplay     = Nadir Moknèche
| cinematography = Jean-Claude Larrieu (AFC)
| sound          = Benoît Hillebrant Jean-Pierre Laforce
| editing        = Ludo Troch
| music          = Pierre Bastaroli
}}

Délice Paloma is a 2007 French-Algerian film directed by Nadir Moknèche and starring Biyouna. It tells the story of Madame Aldjeria, her past life, her glory,  her dream, and her downfall as queen of petty dealing, the mafieuse, against the backdrop of Algiers and the Algeria of Independence to today.

==Synopsis==
 You need a building permit? You are alone one evening? Call the national benefactress, Madame  Aldjéria: she will arrange it. The one that was given the name of the country will stop at no scheming to survive in Algeria today. If they are pretty  and not too scrupulous, recruits can make a career. The latest, Paloma, made a great effect, -  especially on Riyadh, the son of Ms. Aldjéria. The re-sale of the Baths of Caracalla in Tipaza, the dream which was to allow the clan  Aldjéria to change its life will be a scam too far. 

== Cast ==
*Biyouna as Zineb Agha/Madame Aldjeria
*Nadia Kaci as Shéhérazade
*Aylin Prandi as Paloma/Rachida
*Daniel Lundh as Riyad
*Fadila Ouabdesselam as Mina
*Hafsa Koudil as Madame Bellil
*Ahmed Benaissa as Monsieur Bellil
*Nawel Zmit as Baya
*Lu Xiuliang as Mister Zhang
*Karim Moussaoui as Le mari de Shéhérazade

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 

 