The Nun (1966 film)
{{Infobox film
| name           = La Religieuse
| image          =
| caption        =
| director       = Jacques Rivette
| producer       = Georges de Beauregard
| writer         = Denis Diderot (novel) Jean Gruault Jacques Rivette
| narrator       =
| starring       = Anna Karina
| music          = Jean-Claude Éloy
| cinematography = Alain Levent
| editing        = Denise de Casabianca
| distributor    = Les Films Impéria Hanns Eckelkamp Filmproduktion Altura Films International Bela Productions Lionsgate (US)
| released       = 6 May 1966 (France)
| runtime        = 135 minutes
| country        = France
| language       = French
| budget         =
| preceded by    =
| followed by    =
}}
 French drama novel of the same title by Denis Diderot.

==Plot summary==

The Nun starts out with a young woman, named Suzanne, in a wedding gown preparing to take her vows of chastity, obedience, and poverty to make herself a nun, but she refuses at the 
last moment and instead begs her parents not to force her to take them. 

This does not work, and later Suzanne learns much about her family and her heritage - or her lack thereof. She discovers that her mother’s husband is not her father, and that her mother is shutting her up in the convent because she doesn’t want her husband to know that the girl was not his daughter. She also does not want to see her sin in the flesh, for she says bearing the girl was her only sin. The father sends the priest to convince her, who reveals her heritage, but it fell on deaf ears. Later the mother falls on her knees to beg the daughter to take the vows, explaining the story enough to make Suzanne resign herself to her fate, realizing that her mother would never give her a chance to marry because the mother did not feel she was worthy to marry and the family could not afford to marry her off. According to the mother, she did not have the bloodline to marry. She writes her mother a letter that says she will take the vows, a letter that will later be used against her in the court case she wages against the church to be released of her vows. 

Suzanne allows herself to be dressed in a wedding gown and takes the vows. She enters the convent, extremely depressed and unresponsive, unable to cope with the requirements of being a nun. She bonds to the Mother Superior, who takes her under her wing, and they have many long conversations. The Mother Superior, Mme de Moni, knows it’s a mistake to accept the girl as a nun but does not stop it, instead telling the girl to accept her fate and make the best of it. Suzanne attempts to, which is made easier by Mme de Moni’s encouragement, and does not utter more words but her body language reveals all. During this time, Suzanne’s mother dies, and Mme de Moni does as well. She bears it until the life finally drives her mad, for the new Mother Superior, Sister Sainte-Christine, mistreats her because of her rebellion as a result of her dislike of the nun’s life. She isolates her constantly and deprives her of food, forcing her to adopt a diet of bread and water. 

Suzanne then sends her friend away with a letter to a lawyer. She wants to be free and absolved of her vows under the argument that everyone around her forced her to take the vows against her will: her mother, her father, the Mother Superior, etc. The lawyer, who becomes her biggest advocate against the religious orthodoxy enslaving her, informs her that while the case is pending, she will have to stay with Sister Sainte-Christine and endure the resulting persecution, but that either she will win or be transferred. Suzanne doesn’t care, not truly understanding the depths of Sister Sainte-Christine’ cruelty. While the case pends, Suzanne suffers many mistreatments under Sister Sainte-Christine, who steals her crucifix, forbids her to eat, forbids her to pray, forbids the other sisters to interact with or speak to her, and isolates her.  She allows them to walk on the weakened, starving Suzanne after Mass. She is also whipped. They become convinced she is possessed, and Sister Sainte-Christine requests an exorcist. Officials arrive, see her mistreatment and understand that her devotion to God is not the way a possessed person would act, and investigate the mistreatment, which involves Sister Sainte-Christine’s being reprimanded. After that, Sister Sainte-Christine lessens the punishment to only isolation but still treats her coldly. 

When Suzanne discovers that the church has decided not to absolve her vows, she once again falls into a severe depression. Her lawyer apologizes and promises to keep in touch, although a church official forbids the contact. The same man later tells her that the church transferred her to another convent under the supervision of Mme de Chelles. In addition to long conversations about her thoughts and experiences, the light-hearted, fun, happy Mme de Chelles displays homosexual tendencies towards Suzanne, which Suzanne never fully grasps. She meets a monk who attempts to comfort her by saying that he was forced into religion against his will as well. They develop a relationship and he later tells her that they must escape together. Suzanne goes with him, but flees from him when he forces kisses on her as soon as they are together which implies he desires more with her. Suzanne finds refuge nearby, working as a seamstress and doing chores for women. While there, she learns that the monk was caught and faces life in prison, same as she does. She cannot bear the thought of returning. She flees the small village she has taken refuge in and winds up begging on the street. Someone gives her money and then a place to stay. They host a party, where she discovers her lawyer is dead. She later attends a costume party with the people. In the middle of it she asks God to forgive her and commits suicide by throwing herself out of the window.

==Cast==
* Anna Karina - Suzanne Simonin
* Liselotte Pulver - Mme de Chelles
* Micheline Presle - Mme de Moni
* Francine Bergé - Sister Ste. Christine
* Francisco Rabal - Dom Morel
* Yori Bertin - Sister Ste. Thérèse
* Catherine Diamant - Sister Ste. Cecile
* Christiane Lenier - Mme Simonin
* Wolfgang Reichmann - Father Lemoine

==Crew==
* Jacques Rivette - Screenwriter, Director
* Georges de Beauregard - Producer
* Jean-Jacques Fabri - Art Director
* Francoise Geissler - Editor
* Harold Salemson - Editor
* Denis Diderot - Book Author
* Jean-Claude Éloy - Composer (Music Score)
* Jean Gruault - Screenwriter
* Gitt Magrini - Costume Designer
* Denise de Casabianca - Editor
* Alain Levent - Cinematographer
* Marilù Parolini - Set Photographer 

==Awards==
* Nominated for the Golden Palm Award  at the 1966 Cannes Film Festival.   

==See also==
* La Religieuse (novel)|La Religieuse (novel)
* La religieuse (song)|"La religieuse" (song)

==References==
 

==Further reading==
*  

==External links==
*  

 

 

 
 
 
 
 
 
 
 

 