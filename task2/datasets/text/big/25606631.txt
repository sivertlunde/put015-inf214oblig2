The Apparition
 Apparition}}
{{Infobox film
| name           = The Apparition
| image          = The_Apparition_poster.jpg
| caption        = Official theatrical release poster
| director       = Todd Lincoln
| producer       = Alex Heineman Todd Lincoln Andrew Rona Joel Silver
| writer         = Todd Lincoln
| starring       = Ashley Greene Sebastian Stan Tom Felton Julianna Guill
| music          = tomandandy Daniel Pearl
| editing        = Todd Lincoln
| studio         = Dark Castle Entertainment
| distributor    = Warner Bros. Pictures (US) StudioCanal (UK)
| released       =  
| runtime        = 82 minutes 
| country        = United States
| language       = English
| budget         = $17 million 
| gross          = $9,627,492 
}} supernatural Horror horror Thriller thriller film written and directed by Todd Lincoln and starring Ashley Greene, Sebastian Stan, Tom Felton, Julianna Guill and Rick Gomez.  The film was a box office bomb and was panned by critics upon its release. The film was loosely inspired by the Philip experiment conducted in 1972.   

==Plot== parapsychological experiment, in which they stare at a drawing of a deceased man, Charles Reamer, hoping to summon his spirit. Years later, four college students, Patrick (Tom Felton), Lydia (Julianna Guill), Ben (Sebastian Stan) and Greg (Luke Pasqualino) attempt to recreate the Charles Experiment on a larger scale by using modern technology. During the experiment, something attacks the students and pulls Lydia into the wall.

Some time later, Ben and his girlfriend Kelly (Ashley Greene) are living together. One evening, they discover strange burn marks on their counters. Kelly finds both doors wide open, even though they had locked them. They decide to change the locks and install surveillance cameras. Later, Kelly finds a large amount of mold and spores on the laundry room floor while Ben finds even more in a crawl space (basement)|crawlspace. Ben gets 36 "urgent" emails from Patrick that first inform him of a new attempt at the Charles Experiment, followed by a warning that "containment failed" and finally "you are in danger".

After witnessing the apparition, the couple go to a hotel, but theyre attacked there as well. As they flee, they receive a call from Patrick and meet him. Patrick explains that the initial experiment enabled a malevolent spirit entity to enter their world, but that he has built a room surrounded by a negative current that he believes protects him from the spirit. They return to Kelly and Bens house to try a new experiment to contain the entity. During the experiment, the house begins to shake and break apart, then abruptly stops. While Kelly and Ben are outside, Patrick is pulled into the darkness and vanishes. Unable to find Patrick, they leave to get to the safety chamber in Patricks house.

Inside the room, we hear Patricks personal log being played back, including information about the members of the original experiment. Of the original six, two died, one committed suicide and the other three disappeared. After entering the safety chamber cage, Ben disappears. Kelly exits the chamber and finds Bens contorted corpse. Patricks narration explains that the entity gets stronger with each person it claims, and that it will wear its victims down until theyre too weak to resist.

With no escape, Kelly wanders around, and enters an empty Costco. She walks to the camping section, enters a tent and waits. Having given up resisting, a number of hands appear from behind and slowly grab hold of her as the movie cuts to black.

==Cast==
* Ashley Greene as Kelly
* Sebastian Stan as Ben
* Tom Felton as Patrick
* Julianna Guill as Lydia
* Rick Gomez as Mike
* Luke Pasqualino as Greg
* Anna Clark as Maggie
* Tim Williams as Office Executive
* Marti Matulis as Apparition
* John Grady as Apparition
* Suzanne Ford as Mrs. Henley

==Production==
In May 2009, Todd Lincoln was named as the director and screenwriter of the project.    actor Sebastian Stan and British actor Luke Pasqualino.  Filming began on February 26, 2010 in Berlin, Germany,  with other scenes being shot in Los Angeles.  On March 25, Greene shot some scenes in Palmdale, California.   The project marks Lincolns directorial debut.  Silver will produce with Andrew Rona and Alex Heineman. 

==Release==
The film was released on August 24, 2012, in 810 theaters.   

===Home media===
The film was released on Blu-ray Disc|Blu-ray and DVD on November 27, 2012.

==Reception==
===Critical response===
The Apparition was heavily panned by critics, and has an overall approval rating of 3% on  , Ring (film)|Ring, Paranormal Activity, and Pulse (2001 film)|Pulse. Brian Orndof from Blu-ray.com said, "The trailer for The Apparition contained more story than the picture it was promoting. In fact, I think the trailer for The Apparition is actually more of a movie than The Apparition." Mark Dujsik agreed, saying, "Its so terrible that there might actually be more tension in the numbing first act of monotonous chores than there is in anything that follows."  IndieWire called the film a "hauntingly inept chiller", feeling that the film "makes no attempts to transcend or even enliven its genre".  RedEye Chicago called the film "hilariously non-scary". 

===Box office===
The Apparition was a significant box office bomb. The film only came in at #12 on its opening weekend at the box office, with a gross of $2.84 million.  According to Box Office Mojo, "With the unusually-low theater count and a practically non-existent marketing effort, its clear Warner Bros. was trying to bury this movie, and they appear to have succeeded."  As of November 2012, it grossed $4.9 million domestically and $9.6 million worldwide.  The film still had international release dates throughout December 2012;  however, it appears not to have recouped its budget.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 