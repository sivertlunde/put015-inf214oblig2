Night Train (2007 film)
{{Infobox film
| name           = Night Train
| image          = Night_train_poster.jpg
| caption        = 
| director       = Diao Yinan
| producer       = Vivian Qu Steve Chow
| writer         = Diao Yinan Liu Dan Qi Dao
| music          = Wen Zi
| cinematography = Dong Jingsong
| editing        = Kong Jinglei
| studio         = Fonds Sud Cinema DViant Films
| distributor    = MK2 Diffusion
| released       =  
| runtime        = 94 minutes
| language       = Mandarin
| budget         = 
| film name = {{Film name| jianti         = 夜车
| fanti          = 夜車
| pinyin         = Yè chē}}
}}
Night Train is writer-director Diao Yinans second feature film.  Like his previous film, Uniform (film)|Uniform, Night Train takes place in Diaos home province of Shaanxi and was shot in and around Baoji.    
 Li Yangs Blind Mountain in competition for an award.  The film was produced by Diaos Beijing-based Ho-Hi Pictures with funding from French (Fonds Sud Cinema) and American (DViant Films) companies.

== Plot == Liu Dan), execution of female prisoners. Lonely and widowed, Wu finds herself taking the night train to a dating service in a neighboring city.
 Qi Dao), with whom she begins a relationship. It soon becomes clear that Li Jun is hiding a secret: that he is the widower of one of Wus executed prisoners. Li Jun is torn both by his attraction to Wu, but also his desire to exact some sort of vengeance. Wu, meanwhile, must consider her own safety in this new volatile relationship.

== Cast == Liu Dan as Wu Hongyan, a widow, now working on the execution  Qi Dao as Li Jun, the husband to one of Wus executed inmates, Li Jun and Wu embark on an unstable sexual relationship
* Xu Wei as Wus first date
* Wu Yuxi as Wus second date
* Wang Zhenjia as Wus girlfriend
* Men Haiyan as a new prisoner

== Reception ==

===Awards and nominations===
*2007 Warsaw International Film Festival
** Nescafe Grand Prix 
*2008 Buenos Aires International Festival of Independent Cinema
** Best Actress — Liu Dan   
** Jury Special Prize 

== References ==
 

== External links ==
* 
* 
*  from the Chinese Movie Database
*  from distributor Mk2

 
 
 
 
 
 
 
 
   
 
 


 
 