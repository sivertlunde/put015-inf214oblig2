Tai Chi Master (film)
 
 
 
{{Infobox film
| name           = Tai Chi Master
| image          = Tai Chi Master DVD.jpg
| caption        = DVD cover
| film name = {{Film name| traditional    = 太極張三豐
 | simplified     = 太极张三丰
 | pinyin         = Tàijí Zhāng Sānfēng
 | jyutping       = Taai3 Gik6 Zeong1 Sam1 Fung1}}
| director       = Yuen Woo-ping
| producer       = Jet Li
| writer         = Yip Kwong Kim Chin Siu Ho Fennie Yuen Cheung Yan Yuen Lau Shun Hai Yu Kam Kong Chow Jian-kui Sun  Wing-chung Ho
| music          = Wu Wai Lap
| cinematography = Tom Lau
| editing        = Angie Lam On-yee Golden Harvest
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
}}
Tai Chi Master ( , aka Tai-Chi Master and Twin Warriors) is a 1993 Hong Kong martial arts film directed by Yuen Woo-ping, and produced by Jet Li, who also starred in the film. The film was released in the Hong Kong on 18 November 1993.

==Plot== Junbao and Shaolin Temple as monks, studying the martial arts and generally getting into trouble. They are both expelled from the temple after Tienbo almost kills a fellow student who cheats in a fight against him. Aided in their escape by their sympathetic teacher, they receive final instructions regarding the potential paths of their different personalities, with a specific warning given to Tienbo. They both then go into the outside world to find their way in life. Meanwhile, a gang of henchmen are forcibly taking money from a local shop owner. A woman named Miss Li steals the money and returns it. Having noticed the money gone, the henchmen start to fight with Miss Li. Miss Li holds her own during the fight, but soon gets into a bit of trouble due to her being outnumbered. Junbao comes to her aid and defeats the gang. Army reinforcements arrive to break up the fight and so the trio run away to escape capture. At this moment the eunuch governor travels through the town, roughing up the locals as he does so. Tienbo realizes that he wants to be as rich and powerful as the governor, but Miss Li warns him that he has "a heart of a viper" and wonders if he could handle this power. Miss Li then shows them to a pub to get food.

Inside the pub, they find a woman named Siu Lin. Siu Lin is searching for her lost husband, during which she supports herself by playing on a sanxian that said husband gave to her as a gift. She finds him inside the pub as the new husband of the governors niece. The niece starts a battle with Sui Lin where they both seem equal, but the husband hits Siu Lin on her head with a stool causing her to collapse. Junbao helps Siu Lin by defending her against her ex-husband and the nieces guards.
 kung fu skills, the governors second in command spots them and is impressed with Tienbos abilities (and his eagerness to kow-tow to authority.) He offers him to join the army (which he readily accepts) however, Junbao is more reluctant to do so and declines going with Tienbo and so the two go their separate ways. Later, some soldiers come to the pub to collect taxes (which have increased due to the governor’s greedy nature) but Junbao and the rebels (who have stolen great valuables from the governor to give back to the poor) fight and kills them one by one. One soldier escapes alive and starts off towards the armys camp to warn them about the rebels with Junbao in pursuit. Just in front of the army encampment, Tienbo kills the soldier before he warns the rest of the army about the rebels whereabouts. Tienbo warns Junbao to stay clear of the rebels as theyll get him into trouble. Now knowing where the rebels are hiding however, Tienbo takes this unique opportunity to gain a promotion. He sets a trap for Junbao and the rebels by telling them that the army is on patrol and when would be the best time to attack them.

Junbao and Siu Lin collect all the rebels from the region, and go to the army camp (thus, falling for Tienbos trap). A big battle occurs where most of the rebels die. Tienbo captures Miss Li and Siu Lin. In the end, the only survivors are Junbao and a few rebels.

Because of the trap, the governor promotes Tienbo to lieutenant. With his new authority (and some poignant advice from the governor) Tienbo kills Miss Li, and holds Siu Lin as bait so that he can try to convert Junbao to the armys cause. This is unsuccessful and Junbao rescues her. Due to the fact that his best friend betrayed him, Junbaos mind snaps and he goes crazy for days. While recuperating in the countryside with Siu Lin, he has a sudden epiphany that leads to him regaining his mental health and inspiration for the creation of the martial art of Tai Chi Chuan.
 
While the governor is traveling to Beijing to see the empress, he encounters Junbao and Siu Lin who defeats his niece the soldiers guarding him. With the governor captured as a hostage, they go to the army camp and demand Tienbo to surrender his wealth and his power. Due to his arrogance, Tienbo declines and starts to fight Junbao thinking that he is still the inferior fighter. To Tienbos surprise, however, Junbao is now fighting using the heretofore unseen style of Tai Chi Chuan. Because the style relies less on size, power, speed and strength, Junbao is able to fend off Tienbo with ease. Seeking an additional advantage in their fight, Tienbo kills the governor in order to gain complete control over the troops surrounding them. Siu Lin intervenes and convinces them not to listen to Tienbo as he just betrayed their leader. Seeing this (and also the way Tienbo utilizes his troops as battle fodder), the troops back off and leave Tienbos fate to Junbao. After a stunning series of parries and blows by Junbao, Tienbo is defeated and ultimately killed due to his reluctance to accept fate and admit defeat.
 his own Tai Chi Chuan.

==Cast==
*Jet Li - Zhang Junbao / Shang Jun Biao / (Zhang Sanfeng)
*Michelle Yeoh - Siu Lin / Qiu Xue / (Falling Snow) Chin Siu Ho - Tienbo / Dong Tian Biao
*Fennie Yuen - Miss Li / Xiao Dong Gua (Little Melon) Cheung Yan Yuen - Reverend Ling
*Shun Lau - Master Jueyuan
*Hai Yu - Head Master
*Kam Kong Chow - Rebel
*Jian-kui Sun – Royal Eunuch Liu-jin
*Wing-chung Ho - Rascal

==Home media releases==
* Universe Laser released DVD in a near uncut state with a Cantonese soundtrack and English subtitles. US on Hollywood Pictures UK on English dub was commissioned with a new musical score and some cuts were applied.
*On 29 July 2008, Dragon Dynasty released their DVD containing a Cantonese soundtrack and the Dimension English dub (using the 1993 dub to fill in the previously cut sections). However, the Cantonese "mono" is a downmix of the 5.1 remix. 
* An Indian VCD by Diskovery contains the 1993 export English version, but under the title "Twice Deadly". It is cut, however.
* On 26 April 2010, Cine Asia was released on DVD in the United Kingdom, the film title has been changed to Tai-Chi Master from Twin Warriors.

==Sequels==
The two sequels to Tai Chi does continuing with two actors they were: Shun Lau and Hai Yu without Jet Li and Michelle Yeoh.

===Tai Chi Boxer===
{{Infobox film
| name           = Tai Chi Boxer
| image          = 
| caption        = 
| director       = Yuen Woo-ping Xinyan Zhang
| producer       = Wai-him Wong
| writer         = Yeung-ping Sze Jacky Wu Christy Chung Mark Cheng Billy Chow Darren Shahlavi
| music          = 
| cinematography = Chau Pak-ling
| editing        = Xinyan Zhang
| distributor    = Film Can Production Limited
| released       =  
| runtime        = 96 minutes
| country        = Hong Kong
| language       = Cantonese
}} Jacky Wu, Christy Chung, Mark Cheng, Billy Chow and Darren Shahlavi. The film was released in the Hong Kong on 14 March 1996. This was Jacky Wu’s first Hong Kong film appearance as Hawkman / Jackie, and Yuen Woo-ping’s final directorial of the film until 2010s True Legend.

===Cast=== Jacky Wu – Hawkman / Jackie
*Christy Chung – Rose
*Mark Cheng – Lam Wing
*Chuen-hua Chi – Da Bu Liang (Bald Villain)
*Billy Chow – Wong, Great Kick of the North
*Sibelle Hu – Jackie’s Mother
*Shun Lau – Officer Tsao (Rose’s Father)
*Darren Shahlavi – Smith
*Chiu Tam – Sung
*Hai Yu – Yeung Shan-wu (Jackie’s Father)

===DVD release=== Region 2.
 Wing Chun Iron Monkey all three films directed by Yuen Woo-ping.

==See also== Jacky Wu filmography
*Jet Li filmography
*List of Dragon Dynasty releases
*List of Hong Kong films List of Hong Kong Legends releases

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 