The Amazing Quest of Ernest Bliss
 
 
{{Infobox film
|name=The Amazing Quest of Ernest Bliss
|image=TheAmazingAdventureDVD.jpg
|image_size=
|caption=A DVD issue of the film, under the US title.
|director=Alfred Zeisler
|producer=Alfred Zeisler
|writer=John L. Balderston (writer) E. Phillips Openheim (novel)
|starring=Cary Grant
|cinematography=Otto Heller
|editing=Merrill G. White
|distributor=United Artists
|released=28 July 1936 (UK) 27 February 1937
|runtime=80 minutes (UK), 62 minutes (US)
|country=United Kingdom language =English
}}
The Amazing Quest of Ernest Bliss is a 1936 British romantic comedy film directed by Alfred Zeisler and starring Cary Grant. It is a remake of the 1920 film The Amazing Quest of Mr. Ernest Bliss.

The film was re-issued in the United States in 1937 under the title The Amazing Adventure (also alternatively Romance and Riches), and was edited down from the original UK running time of 80 minutes, to 62 minutes.  Most prints these days are the shorter one.

==Plot synopsis==
Ernest Bliss (Cary Grant) is a rich socialite suffering boredom. He makes a bet with a doctor, Sir James Aldroyd, that he can live a year without relying on any of his inherited wealth. He loses the bet for £50,000 when he has to draw money to wed poverty stricken Frances Clayton (Mary Brian), to save her from an unhappy marriage of convenience.

==Cast==
*Cary Grant – Ernest Bliss
*Mary Brian – Frances Clayton
*Peter Gawthorne – Sir James Alroyd Henry Kendall – Lord Honiton
*Leon M. Lion – Dorrington John Turnbull – Masters Arthur Hardy – Crawley
*Iris Ashley – Clare
*Garry Marsh – The Buyer
*Andreas Malandrinos – Guiseppi
*Alfred Wellesley – Montague

==External links==
* 
* 
*As   (Public Domain)

 

 
 
 
 
 
 
 
 
 
 
 

 