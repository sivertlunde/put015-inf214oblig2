All Because of the Dog
{{Infobox film
| name = All Because of the Dog
| image =
| image_size =
| caption = Fred Sauer Karl Schulz   Robert Wüllner
| writer = Dr. Lohmeyer    Max Wallner
| narrator =
| starring = Weiß-Ferdl   Julia Serda   Edith Oß   Waldemar Spann-Müller 
| music = Max Pflugmacher  
| cinematography = Georg Krause  
| editing =  Marianne Behr
| studio = Schulz & Wuellner Film
| distributor = Tobis-Sascha Film (Austria)
| released = 29 August 1935 
| runtime = 87 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
All Because of the Dog (German:Alles wegen dem Hund) is a 1935 German comedy film directed by  Fred Sauer and starring Weiß-Ferdl, Julia Serda and Edith Oß.  

==Cast==
* Weiß-Ferdl as Sebastian Neumeyer, Postassistent 
* Julia Serda as Cilly Neumeyer, seine Frau  
* Edith Oß as Anny, seine Tochter  
* Waldemar Spann-Müller as Schorschi, sein Sohn 
* Heinz Dugall as Pepi, sein Sohn  
* Peter Bosse as Der kleine Hansl, sein Jüngster  
* Otto Sauter-Sarto as Hölzinger, Postmeister  
* Trude Hesterberg as Lottchen, seine Frau 
* Dieter Borsche as Franz, beider Sohn  
* Willi Schaeffers as Pilzer, Notar   Robert Jungk as Jean, Diener  
* Irene Andor as Schmederer, Bäckermeisterin  
* Wolfgang von Schwindt as Apotheker 
* Otto Kronburger as Zimmermann, Neumeyers Kollege  
* Lucie Euler as Frau Schwiebus  
* Egon Brosig as Geier, Auktionator  
* Vera Hartegg as Zenzi  
* Else Lüders as Baumeisterin 
* Karl Harbacher as 1. Gast im Restaurant  
* Heinz Herkommer as 2. Gast im Restaurant  
* Irene Hübner as Gast bei Frau Hölzinger  
* Leo Peukert as Tierarzt  
* Norma Wellhoff as Frau mit Hund

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 