Les fils du vent
{{Infobox film
| name           = Le fils du vent
| image          = 
| caption        = 
| director       = Julien Seri
| producer       = Yves Marmion
| writer         = Julien Seri Bruno Guiblet Philippe Lyon Charles Perrière
| narrator       = 
| starring       = Charles Perrière Elodie Yung  Châu Belle Dinh
| music          = Christian Henson
| cinematography = Michel Taburiaux
| editing        = Maryline Monthieux
| distributor    = Manga Entertainment 
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

Les fils du vent (also known as The Great Challenge and Sons of the Wind: Bangkok Ninjas  {{Cite news|url= http://www.beyondhollywood.com/sons-of-the-wind-bangkok-ninjas-2004-movie-review/|title= French production "Sons of the Wind: Bangkok Ninjas" (aka "The Great Challenge") was originally released back in 2004, and is a semi sequel to the Luc Besson produced 2001 film "Yamakasi – Les Samouraïs des Temps Modernes".|accessdate=2011-06-04|location=London|work=BeyondHollywood.com
|first=James|last=Mudge}} ) is a 2004 French action film featuring the Yamakasi.

==Plot== Eurasian leader Kien (Châu Belle Dinh) attacks the foreigners while they are having some extra training in a scaffold. They battle until police arrives. Both groups can scarcely escape. Logan (Charles Perrière) runs hereby into Kiens only sibling, his sister Tsu (Elodie Yung). Logan and Tsu, who obviously feel a mutual attraction, are right from the start about to become a modern version of "Romeo and Juliet". 

Following the dangerous fight the group splits up. Only Leo (Laurent Piemontesi) and Yaguy (Guylain NGuba-Boyeke) still stick to the plan of establishing the gym. Williams (played by Williams Belle) departs to visit his grandfather in an abbey. Lukas (Yann Hnautra) wants to return home and to try to fix his marriage. Kenjee (Malik Diouf) attends a local Muay Thai school. Logan is after Tsu. 
 Triads or the Yakuza will eventually kill her and her brother. But her brother Kien is determined to get back at the Triad that refused them because of their French mother. He and his gang believe they can have a future only by serving the Yakuza. Their contact to the Yakuza is Kitano (Santi Sudaros), the Japanese son-in-law of Triad boss Wong (Burt Kwouk). 

Kitano has made his father-in-law believe he had broken up with his Yakuza family. Yet Wong still doesnt trust him. Kitano commissions Kien to kidnap Wongs little son. Tsu objects to this and saves the heir by disclosing Kitanos double game to Wong. Now Kitano is out for revenge. He abducts Tsu and blackmails Kien to turn himself in to the Yakuza. Kien is ordered to derelict industrial premises.

Logan, Kenjee, Yann, Yaguy, Leo and Williams arrive on time to free Tsu. But when they do, Wong appears with a great many Triad members and confronts the Yakuza. Logan, Tsu, the other traceurs and Tsus brother Kien find themselves in the middle of a ferocious battle. They must fight their way out before they get grinded between Triad and Yakuza. A fight ensues.

==Cast==
* Châu Belle Dinh as Kien
* Charles Perrière as Logan
* Guylain NGuba Boyeke as Yaguy
* Laurent Piemontesi as Léo
* Malik Diouf as Kenjee
* Williams Belle as Williams
* Yann Hnautra as Lucas
* Elodie Yung as Tsu
* Santi Sudaros as Kitano
* Burt Kwouk as Wong

==DVD Release==
The film was released for English-speaking countries in 2006 and under a different title  again in 2010.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 