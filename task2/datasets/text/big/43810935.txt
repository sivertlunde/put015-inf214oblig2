Bellas de noche
{{Infobox film
  | name = Bellas de noche
  | image = 
  | caption =
  | director = Miguel M. Delgado
  | producer = Guillermo Calderòn
  | writer = Francisco Cavazos Víctor Manuel Castro
  | starring = Sasha Montenegro Jorge Rivero Rosa Carmina
  | music = Gustavo Cesar Carrión
  | cinematography = Miguel Araña
  | editing = 
  | distributor = Cinematográfica Calderón SA
  | released = September 25, 1975
  | runtime = 98 minutes
  | country = México
  | language = Spanish
  | budget =
    }}
 Mexican film directed by Miguel M. Delgado. It was filmed in 1975 and starring Sasha Montenegro and Jorge Rivero. Its regarded as the film that began the saga of the Mexican sex comedies film genre if the Mexican Cinema.

==Plot==
The Boxer Germán Bronco Torres (Jorge Rivero) loses his license, and works as bouncer at the cabaret El Pirulí (The Lolipop), where he falls for the fichera Carmen (Sasha Montenegro), and befriends of the pimp Margarito Fuensanta El Vaselinas (Eduardo de la Peña), who lost a bet and has to pay to a gangsters. For 500 pesos for El Vaselinas, Bronco prepares a trap in the cabaret to the taxi driver Raul (Enrique Novi), to seduce his girlfriend, not knowing that the victim is his own sister Lupita (Leticia Perdigón). When he discovers the situation, he hits the driver and send to the prison. Raúl sold his taxi to pay bail. El Vaselinas pretends to die to get rid of his creditors. The cabaret is closed and all begin a new life.  In the adventures of these characters, appended the alcoholic woman know as La Corcholata (Carmen Salinas), a sympathetic woman trying to sneak to the cabaret, and the history of the owner of the cabaret, Don Atenógenes (Raúl Chato Padilla), and his wife, the mistress of the brothel, Maria Teresa (Rosa Carmina).

==Production== Belle de Jour. 

==Cast==
* Sasha Montenegro as Carmen
* Jorge Rivero as Germán "Bronco" Torres
* Rosa Carmina as María Teresa
* Raúl Chato Padilla as Don Atenógenes
* Leticia Perdigón as Lupita
* Carmen Salinas as La Corcholata
* Enrique Novi as Raúl
* Eduardo de la Peña as El Vaselinas
* Mabel Luna as La Muñeca
* Víctor Manuel Castro as Fabián
* Rafael Inclán as El MovidAS

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 