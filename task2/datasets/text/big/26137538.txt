Get-Rich-Quick Wallingford (1921 film)
 
{{Infobox film
| name           = Get-Rich-Quick Wallingford
| image          = Get-Rich-Quick Wallingford (1921) - 1.jpg
| image_size     = 200px
| caption        = Still with Sam Hardy on left and Doris Kenyon on right
| director       = Frank Borzage
| producer       =
| writer         = Luther Reed
| narrator       = Sam Hardy Doris Kenyon
| music          =
| cinematography = Chester A. Lyons
| editing        =
| distributor    = Paramount Pictures
| released       =   reels
| country        = United States Silent English intertitles
| budget         =
}} silent comedy film directed by Frank Borzage. The films script was adapted by writer Luther Reed from the novel "Get-Rich-Quick Wallingford" by George Randolph Chester and George M. Cohan, which expanded in turn on a series of short stories by Chester. Produced by Cosmopolitan Productions and distributed by Paramount Pictures Corporation, the film was released in seven reels on December 4, 1921.   

==Plot==
As the film opens, Blackie Daw arrives in the town of Battlesburg, Iowa. Daw has little money, but makes it known that J. Rufus Wallingford, a wealthy businessman, will be arriving in town soon and is interested in finding good investments. When Wallingford arrives, he and the townspeople hatch a scheme to build a factory, but they cannot decide what the factory should produce. Wallingford suggests carpet tacks, which he insists will interest other investors, and the townspeople agree. As the film progresses, the companys stockholders begin to doubt Wallingford who is, in fact, a con man. He is able to assuage their doubts. The establishment of the factory begins a real estate boom, and Wallingford and Daw are planning to skip town with the money theyve gained. But just before they do, a wealthy financier buys out Wallingfords interest and the factory makes a large sale of carpet tacks. As a result, Wallingford and Daw become wealthy by honest means. They both end the film by finding women to marry, Wallingford to his stenographer Fannie Jasper and Daw to Dorothy Wells, daughter of a prominent townsperson. 

==Cast== Sam Hardy - J. Rufus Wallingford
* Norman Kerry - Blackie Daw
* Doris Kenyon - Fannie Jasper
* Diana Allen - Gertrude Dempsey
* Edgar Nelson - Eddie Lamb
* Billie Dove - Dorothy Wells
* Mac Barnes - Andrea Dempsey
* William T. Hayes - G.W. Battles
* Horace James - Timothy Battles
* John Woodford - Mr. Wells
* Mrs. Charles Willard - Mrs. Dempsey
* Eugene Keith - Harkins
* William Carr - Quigg
* William Robyns - Abe Gunther
* Theodore Westman Jr. - the bellboy
* Patterson Dial - Bessie
* Jerry Sinclair - Judge Hampton
* Benny One - Wallingford’s valet

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 