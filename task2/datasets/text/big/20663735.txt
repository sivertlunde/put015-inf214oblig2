Narasimham (film)
{{Infobox Film |
| name = Narasimham
| image = Narasimham (film).jpg
| director = Shaji Kailas
| caption = DVD cover
| writer = Ranjith
| music = M. G. Radhakrishnan Aishwarya  Kanaka  Sai Kumar   Kalabhavan Mani   Narendraprasad
| producer = Antony Perumbavoor
| cinematography = Sanjeev Sankar
| editing = L. Bhoominathan 
| studio = Aashirvad Cinemas
| distributor = Swargachitra 
| released =  
| runtime = 175 minutes
| country = India
| language = Malayalam
| budget =   2 crores
| gross =   22 crores   
}}
 2000 Malayalam Malayalam action action masala Ranjith and directed by Shaji Kailas. It stars Mohanlal in the title role along with Thilakan, N. F. Varghese, Aishwarya Sivachandran|Aishwarya, Kanaka (actress)|Kanaka, Jagathi Sreekumar. This was the first movie from Aashirvad Cinemas owned by Antony Perumbavoor.

Upon release the film broke almost all the box office records becoming a record breaking grosser and the all time biggest blockbuster in Malayalam cinema at that time. The film completed 200 days in one theatre and 175 days in three theatres and grossed around   22 Crore in total making a   10 Crore profit to the producer.
Narasimham grossed around   7 crores as distributors share.  .   with Mohan Babu and Nagarjuna appeared in a guest role.

== Plot ==

Poovalli Induchoodan (Mohanlal) is sentenced for six years prison life for murdering his classmate. Induchoodan, the only  son of Justice Maranchery Karunakara Menon (Thilakan) was framed in the case by Manapally Madhavan Nambiar (Narayanan Nair) and his crony DYSP Sankaranarayanan (Bheeman Raghu) to take revenge on idealist judge Menon who had earlier given jail sentence to Manapally in a corruption case. Induchoodan,  who had achieved top rank in Indian Civil Service loses the post and Manapally Sudheeran (Saikumar (Malayalam actor)|Saikumar), Manappallys younger son, enters the list of civil service trainees. We learn in flashback that it was Ramakrishnan (E A Rajendran) the son of Moopil Nair (Narendra Prasad), who had actually killed his classmate.  Six years passes by and Manapally Madhavan Nambiar, now a former state minister, is dead and Induchoodan, who is all rage at the gross injustice meted out to him - thus destroying his promising life, is released from prison.  Induchoodan thwarts Manapally Pavithran (N F Varghese), Manappallys elder son from performing the funeral rituals of his father at Bharathapuzha. Many confrontations between Induchoodan and Manapallys henchmen follow.

Justice Menon and his wife returns to Kerala to stay with Induchoodan. There is an appearance of a girl named Indulekha (Kanaka (actress)|Kanaka), who claims to be the daughter of Justice Menon. Menon flatly refuses the claim and banishes her. Forced by circumstances and at the instigation and help of Manapally Pavithran, she reluctantly come out open with the claim. Induchoodan at first thrashes the protesters. But upon knowing the truth from Chandrabhanu(Jagathi Sreekumar) his uncle, he accepts the task of her protection in the capacity as elder brother. Induchoodan decides to marry off Indulekha to his good friend Jayakrishnan (Vijayakumar (actor)|Vijayakumar). Induchoodan has a confrontation with his father and prods him to accept mistake and acknowledge the parentage of Indulekha. Menon ultimately regrets and goes on to confess to his daughter. The very next day, when Induchoodan returns to Poovally, Indulekha is found dead and Menon is accused of murdering her. The whole act was planned by Pavithran, who after killing Indulekha, forces Raman Nair (Menons longtime servant) to testify against Menon in court. In court, Nandagopal Maarar (Mammootty), a close friend of Induchoodan and a famous supreme court lawyer, appears for Menon and manages to lay bare the murder plot and hidden intentions of other party. Menon is judged innocent of the crime by court. After confronting Pavithran and promising just retribution to the crime of killing Indulekha, Induchoodan returns to his father, who now shows remorse for all his actions including not believing in the innocence of his son. But while speaking to Induchoodan, Menon suffers a heart attack and passes away. At Menons funeral, Manapally Pavithran arrives to poke fun at Induchoodan and he also tries to carry out the postponed last rituals of his own father. Induchoodan interrupts the ritual and avenges for the death of his sister and father by severely injuring Pavithran. On his way back to peaceful life, Induchoodan accepts Anuradha, (Aishwarya (actress)|Aishwarya), the strong-willed and independent-minded daughter of Mooppil Nair,as his life partner.

== Cast ==
*Mohanlal as Poovalli Induchoodan or Achuvettan
*Thilakan as Justice Maranchery Karunakara Menon, Induchoodans father.
*N. F. Varghese as Manappally Pavithran, the elder son of Manappally Madhavan Nambiar.
*Jagathi Sreekumar as Chandrabhanu, Induchoodans uncle. Aishwarya as Anuradha, the love interest of Induchoodan. Bharathi as Induchoodans mother
*Spadikam George as Kalletti Vasudevan Kanaka as Indulekha, Menons illegitimate daughter.
*T. P. Madhavan as Raman Nair, Menons longtime clerk.
*Kalabhavan Mani as Bharathan, Induchoodans follower.
*Maniyanpilla Raju as CI Habeeb, Induchoodans friend.
*Narendra Prasad as Moopil Nair, Anuradhas father.
* Kozhikode Narayanan Nair as Manappally Madhavan Nambiar Saikumar as Manappally Sudheeran IPS, Pavithrans younger brother.
*V. K. Sreeraman as Venu Master.
*Kollam Thulasi as Public Prosecutor.
*Jagannatha Varma as Judge.
*Vijayakumar as Jayakrishnan, Induchoodans friend and jail mate.
*Bheeman Raghu as DYSP Sankaranarayanan, Pavithrans devoted officer. Augustine
*Sadiq Irshad
*Mammootty as Adv. Nandagopal Marar (Cameo)

==Box office==
Narasimham was released in 32 centres and collected a share of   3 crores from 20 days.  Upon release the film broke almost all the box office records becoming a record breaking grosser and one of the all time biggest blockbuster in Malayalam Cinema. The film completed 200 days in one theatre and 175 days in three theatres and grossed around   22 Crore in total making a   10 crore profit to the producer with a total distributor share of   7 crore. The film is considered to be an all time highest grossing movie in Malayalam Cinema when adjusted to inflation, considering the high increase in number of releasing theatres, ticket rates and money value. 
== Accolades ==
The movie was re released after 15 years on 5/12/14 in Dubai. The second release also had houseful shows.

== Legacy ==
*The films punch dialogue "നീ പോ മോനെ ദിനേശാ (Nee po mone Dinesha) has become an iconic Malayalam catchphrase.  
*The mundu worn by Mohanlal in this film, popularly known as "Narasimhahm Mundu", became a fashion trend among youngsters. 
*The film includes five songs composed by M. G. Radhakrishnan, of which "Dhaankinakka/Pazhanimala" attained a high level of popularity.
*The film was a record breaking blockbuster and was the highest grossing film of all time.
*This was the  1000th movie of Jagathy Sreekumar.

==Awards==
; Asianet Film Awards Best Film Best Director - Shaji Kailas

;National Film Academy Award
* Best Actor - Mohanlal

==Soundtrack==
{{Infobox album 
| Name        = Narasimham
| Type        = Soundtrack
| Artist      = M. G. Radhakrishnan
| Cover       =
| Background  = 
| Recorded    = 
| Released    = 2000
| Genre       = Film 
| Length      = 31:05
| Label       = Satyam Audios
| Producer    = Aashirvad Cinemas
| Reviews     = 
| Last album  = Kannezhuthi Pottum Thottu
| This album  = Narasimham Pilots
|}}
This film includes 5 songs written by lyricist Gireesh Puthenchery. The songs are composed by composer M. G. Radhakrishnan.
{| class="wikitable"
|-
! Track !! Song Title !! Singer(s) !! Other notes
|- Chorus || Mohanlals introduction song
|- Sujatha || End credit song picturised on Mohanlal and Aiswarya
|- Chorus ||  Romantic song picturised on Mohanlal and Aiswarya. Not included in DVD release.
|-
| 4 || Amme Nile || K. J. Yesudas|Dr. K. J. Yesudas || Title credit song
|- Sujatha || Not Picturised
|-
| 6 || Amme Nile || M. G. Sreekumar || Not picturised
|- Chorus || Not picturised
|-
| 8 || Pazhanimala ||  
|}

==See also==
* Mohanlal
* Aashirvad Cinemas

==References==
 

== External links ==
*  

 

 
 
 
 
 