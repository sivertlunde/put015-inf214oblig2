Paavai Vilakku
{{Infobox film
| name        = Paavai Vilakku
| image       = 
| director    = K. Somu
| writer      = A. P. Nagarajan
| screenplay  = A. P. Nagarajan
| story       = Akilan
| starring    = Sivaji Ganesan Sowcar Janaki Pandari Bai M. N. Rajam Kumari Kamala V. K. Ramasamy (actor)|V. K. Ramasamy K. Sarangkapani
| producer    = T. Vijayarangam V. K. Gopanna
| music       = K. V. Mahadevan
| cinematography = V. K. Gopanna
| editing     = T. Vijayarangam (Supervisior) K. Thurairaj
| studio      = Sri Vijayagopal Pictures
| distributor = Sri Vijayagopal Pictures
| released    = 19 October 1960
| runtime     = 170 mins
| country     =   India Tamil
| budget      = 
}}
 Tamil 1960 film directed by K. Somu. The film features Sivaji Ganesan, Sowcar Janaki, Pandari Bai, M. N. Rajam and Kumari Kamala in lead roles. The film, had musical score by K. V. Mahadevan and was released on 19 October 1960.   As the story was serialised in a weekly magazine, it had many twists and turns, and the movie is also not easy to narrate in detail. Despite the popularity of the novel , brilliant writing for the screen by A.P. Nagarajan and fine performances Sivaji came up with an excellent portrayal, well supported by Sowcar Janaki, Rajam, and Kumari Kamala., the film did not do well at the box office. 

==Plot==
The film has a novel beginning. Sivaji sits in a park with his friends, holding a copy of the novel Paavai Vilakku in his hands, Sivaji talks about its greatness to his friends, and then begins to read it.

Now the film cuts to the beginning of the story in which he plays the hero. The heroine is Sowcar Janaki, the woman he marries, and they have a child Kalyani. Kumari Kamala is a dancer to whom the hero is attracted, but for many reasons they do not marry because he already has a wife. Pandari Bai, a young widow, is also drawn to him, but she begins to treat him as her brother. M.N. Rajam is another young woman who stays with the married couple and brings up the child as her own. She too falls in love with the hero, but cannot marry him. An accident on the steps of the hero’s house ends in the death of the child. Rajam for obvious reasons is not informed of the child’s death. Later she too meets with a similar accident and when she comes to know the shocking secret about the child, she dies in the arms of her friend Janaki and the hero surrounded by all the relations.

==Cast==
*Sivaji Ganesan
*Sowcar Janaki
*Pandari Bai
*M. N. Rajam
*Kumari Kamala
*V. K. Ramaswamy (actor)|V. K. Ramasamy
*K. Sarangkapani
*S. A. Asokan
*A. Karunanidhi
*Sairam
*K. Balaji in Guest Appearance
*Prem Nazir in Guest Appearance
*Sriram in Guest Appearance
*M. R. Santhanam in Guest Appearance

==Crew==
*Producer: T. Vijayarangam & V. K. Gopanna
*Production Company: Sri Vijayagopal Pictures
*Director: K. Somu
*Music: K. V. Mahadevan Mahakavi Bharathiyar, A. Maruthakasi
*Story: Akilan
*Screenplay: A. P. Nagarajan
*Dialogues: A. P. Nagarajan
*Art Direction: C. H. E. Prarsad Rao
*Editing: T. Vijayarangam (Supervisior) & K. Thurairaj
*Choreography: K. N. Dhandayuthapani Pillai
*Cinematography: V. K. Gopanna
*Stunt: None
*Audiography: A. Samy
*Songs Recording & Re-Recording: T. S. Rangasamy
*Dance: None

==Soundtrack== Mahakavi Bharathiyar Playback singers Soolamangalam Rajalakshmi P. Suseela& L. R. Eswari.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics|| Length (m:ss)
|-
| 1 || Aayiram Kan Podhadhu || C. S. Jayaraman || rowspan=2|A. Maruthakasi || 03:24
|-
| 2 || Kaaviyama Nenjil || C. S. Jayaraman & P. Suseela || 05:08
|- Mahakavi Bharathiyar || 01:27
|-
| 4 || Naan Unnai Ninaikkaatha || P. Suseela || rowspan=5|A. Maruthakasi || 05:19
|- Soolamangalam Rajalakshmi || 03.:44
|-
| 6 || Sidhariya Sadhangaigal || P. Suseela || 02:58
|-
| 7 || Vannathamizh Pennoruthi || C. S. Jayaraman & L. R. Eswari || 04:06
|- Soolamangalam Rajalakshmi || 01:52
|}

==References==
 

==External links==
*  
 

 
 
 
 
 
 


 