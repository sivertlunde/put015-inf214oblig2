Darinda
 
 
{{Infobox film
| name           = Darinda
| image          = Darinda-Vcd.jpg
| caption        =
| director       = Iqbal Kashmiri
| producer       = Inam Shah
| writer         = Syed Noor Shaan Resham Saima Babar Ali Veena Malik
| music          = Ranaq Ali, Sajid Ali
| cinematography =
| editing        =
| released       = 2002
| runtime        =
| country        = Pakistan Urdu
| budget         =
| domestic gross =
| preceded_by    =
| followed_by    =
}}

A Pakistani Urdu film, starring Saima, Resham and Shaan (actor)|Shaan. A fusillade of machinegun bullets bursts forth as expected, Baber Kashmiris action packed thriller.

==Overview==
 
Saima, Shaan (actor)|Shaan, Momy Rana and Babar Ali play the lead in this action thriller. In Darinda, Shaan (actor)|Shaan, as usual, is the chief protagonist, while Saima makes people swoon. Having Momy and Shaan come together in any film seems to be the best formula. Most movies with them together, have done well at the box office. On top of it, you have Babar Ali also in confrontation with the other two. Babar is no less of a bankable name than the other too. It seems that this formula will soon fizzle out, considering the sort of rat race that is going on in this context. Veena is fast making her presence felt, largely because of her ability to adjust to the demands of the filmmakers and TV serials alike. In this film, she is looking as glamorous as she looks in real life too. If Veena is able to team the craft of voice manipulation and dialogue delivery, she can well manage to have a grand career in the industry.

==Cast==
* Resham Shaan
* Saima 
* Babar Ali
* Jan Rambo
* Tabinda
* Raza

==References==
 
* 

 
 
 

 