Temptation Island (2011 film)
{{Infobox film
| name = Temptation Island
| image = TemptationIsland2011.png
| caption = 
| director = Chris Martinez   
| producer = Jose Mari Abacan Lea A. Calmerin Annette Gozon-Abrogar Roselle Monteverde-Teo	
| writer =  Chris Martinez 
| starring = Marian Rivera Rufa Mae Quinto Heart Evangelista Lovi Poe Solenn Heussaff John Lapus Tom Rodriguez Aljur Abrenica Mikael Daez
| music = Von de Guzman
| cinematography = Gary Gardoce	 
| editing = Vanessa De Leon
| distributor =   
| released =  
| runtime = 115 minutes
| country = Philippines
| language = Tagalog, English
| gross = P 60 Million      
}} comedy Drama drama film Chris Martinez 1980 film of the same name. 

== Plot ==

Four beauty pageant contestants, who are vying for the coveted title of "Miss Manila Sunshine", set sail on a yacht. They ended up stranded on a deserted island after their vessel got caught up on fire. Together with an attractive young man, a maid and the gay pageant director, the four ladies found life "anything but dull.

==Production==
In 2010, Regal Films decided to remake their 80s cult hit film Temptation Island that starred  , Tom Rodriguez, and Mikael Daez. Paulo Avelino was initially one of the leading men, but later refused because of health reasons. He was replaced by Mikael Daez. 

In April 2011, production started in Paoay, Ilocos Norte. 

==Cast==
*Marian Rivera as Cristina G.
*Rufa Mae Quinto as Nympha
*Heart Evangelista as Virginia P.
*Lovi Poe as Serafina L.
*Solenn Heussaff as Pura K.
*John Lapus as Joshua
*Tom Rodriguez as Umberto
*Mikael Daez as Ricardo
*Aljur Abrenica as Alfred
*Dennis Trillo as Antonio
*Maggie Wilson as Herself
*Deborah Sun as Nenuca Kikinang
*Tim Yap as Himself
*Tessa Prieto-Valdes as Herself

== Reception ==
According to   and Harry Potter and the Deathly Hallows - Part 2.

==References==
 

 
 
 
 
 
 
 
 