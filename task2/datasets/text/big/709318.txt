The Stepford Wives (2004 film)
{{Infobox film
| name = The Stepford Wives
| image = Stepford wives ver2.jpg
| caption  = Theatrical release poster
| director = Frank Oz
| producer = Scott Rudin Donald De Line Edgar J. Scherick Gabriel Grunfeld
| writer = Paul Rudnick
| based on =  
| starring = Nicole Kidman Matthew Broderick Bette Midler Christopher Walken Roger Bart Faith Hill Glenn Close 
| music = David Arnold
| cinematography = Rob Hahn Jay Rabinowitz DeLine Pictures
| distributor = Paramount Pictures   DreamWorks Pictures  
| released = June 11, 2004
| runtime = 93 minutes
| country = United States
| language = English
| budget = $90 million
| gross =  $102,001,626	 
}} dramedy film. 1975 film of the same name; both films are based on the Ira Levin novel The Stepford Wives. While the original book and film had tremendous cultural impact, the remake was marked by infighting behind the scenes, poor reviews by many critics, and a financial loss of approximately $40 million at the box office. The film was also heavily reworked in editing and many blame that for the films failure.          

==Plot==
Joanna Eberhart (Nicole Kidman) is a successful reality television executive producer. She is fired after her latest project, a show  called I Can Do Better, where spouses choose between each other or prostitutes, results in one of the jilted men going on a shooting spree, and she has a nervous breakdown. With her husband Walter (Matthew Broderick) and their two children, they move from Manhattan to Stepford, a quiet Connecticut suburb.
 flamboyant Homosexuality|gay man who has moved to town with his long-time partner, Jerry (David Marshall Grant). Joanna, Bobbie and Roger witness an incident in which Sarah Sunderson (Faith Hill), violently dances and then collapses. A man named Mike (Christopher Walken) arrives and directs all men to crowd around Sarah so that no one can see whats going on, although Joanna sees Mike touch Sarah and she puts off sparks. After Sarah is carried away, Claire (Glenn Close), the towns leading lady, announces to Joanna that Mike essentially runs Stepford and says that shes his wife.

Joanna argues with Walter about the incident with Sarah until he loses his temper and yells at her. He tells her that her children barely know her, that their marriage is falling apart, and that shes so domineering people want to kill her. Realizing how unhappy she is, Joanna apologizes and agrees to try and fit in with the other wives. The next day, as she cleans the house and tries wearing more make-up, she talks with Bobbie and Roger, and they decide to visit Sarah. Entering the house, they hear Sarah having loud, passionate sex with her husband. Roger starts tiptoeing up the stairs to have a peek, and the women follow until they hear someone suddenly walking out of the room.
 book club, their story is a catalogue of Christmas and Chanukah collectibles and decoration tips. Meanwhile, Walter has been bonding with the Stepford Mens Association. When he wins $20 in a game from Ted, one of the Stepford Husbands, Ted summons his wife and puts a credit card in her mouth. She spits out $20 in one-dollar bills, revealing that she is a robot like the other women.
 State Senate conservative gay Republican Party (United States)|Republican.

Terrified, Joanna tells Walter that she wants to move. Walter apologizes, saying that if shes so miserable, they can leave tomorrow. She thanks him. That night, she is awakened by their robotic dog bringing her a bone. She is horrified to find that its actually a remote control, like Sarahs but labeled JOANNA. She goes online to research the women of Stepford and learns that the women used to be scientists, engineers and judges. The next morning she runs to see Bobbie, only to find that she, too, has become fawning and stupid. Joanna realizes that Bobbie isnt human any more when Bobbie fails to react to the open flame of a lit stove. Joanna tries to flee but finds that her children have been taken hostage by the men.
 microchips into their wives brains and then transplanted their minds into cloned bodies, which became the mens patient, subservient and impossibly beautiful robot mistresses. As Mike reveals Joannas new body, Walter voices his frustration at being second best to her. The men corner Joanna and Walter and force them toward the transformation room, but before Joanna enters, she makes a final appeal by asking whether the new wives really mean it when they tell their husbands that they love them. Later, Joanna appears at the grocery store, calmly purchasing groceries alongside the other wives.

With Joanna and Walter as the guests of honor, Stepford hosts a formal ball. During the festivities, Joanna distracts Mike and entices him into the garden while Walter slips away. Walter returns to the transformation room where he destroys the software that makes the women obedient. This in turn burns out all the implanted microchips, causing all the Stepford Wives to revert to their original personalities. Walter returns to the ball, where the baffled husbands are cornered by their vengeful wives. Walter reveals that Joanna never received the microchip implant; her argument during the struggle had won him over, and out of his love for and loyalty to the human being he married, he joined her plan to infiltrate Stepford by pretending to be a robot. Mike threatens Walter, but before Mike can attack, Joanna hits Mike with a candlestick, decapitating him and revealing that he himself is a robot. It is revealed that his wife Claire is a real woman and not a Stepford Wife as implied earlier.
 electrocutes herself by kissing Mikes severed robotic head.

Six months later Larry King is interviewing Joanna, Bobbie, and Roger. After their experiences in Stepford, they have all met with success; Joanna made a documentary, Bobbie wrote a book of poetry, and Roger won his state senate seat as an Independent (politician)|Independent. Joanna also notes that while her and Walters relationship isnt perfect, it is still real, and that is what is important. Before ending the interview, King asks about the mens fate. Bobbie reveals they being retrained back in Connecticut. The closing scene of the film reveals that the irate wives have taken over Stepford and forced their husbands to atone for their crimes by placing them under house arrest, making them complete many of the same domestic tasks that the men had forced the women to do.

==Cast==
* Nicole Kidman as Joanna Eberhart
* Matthew Broderick as Walter Kresby
* Bette Midler as Bobbie Markowitz
* Christopher Walken as Mike Wellington
* Roger Bart as Roger Bannister
* Faith Hill as Sarah Sunderson
* Glenn Close as Claire Wellington
* Jon Lovitz as Dave Markowitz
* Matt Malloy as Herb Sunderson
* David Marshall Grant as Jerry Harmon
* Kate Shindle as Beth Peters
* Lorri Bagley as Charmaine Van Sant Robert Stanton as Ted Van Sant  Mike White as Hank
* KaDee Strickland as Tara
* Larry King as Himself

==Production== John and Joan Cusack, originally slated to star in supporting roles, pulled out of the project and were replaced by Matthew Broderick and Bette Midler, respectively. After filming was initially completed, several changes were made to the new script, which created a number of plot holes, and the cast was called back for reshoots.    Nicole Kidman was reportedly so dissatisfied with the new screenplay that she considered pulling out of the project.

Reports of problems on-set between director Frank Oz and stars Kidman, Midler, Christopher Walken, Glenn Close and Roger Bart were rampant in the press. Oz confirmed in an interview that there was "tension on the set" and that he had "had words" with Walken. He also blamed Midler for being under a lot of stress from other projects - she "made the mistake of bringing her stress on the set". 

In an interview with Aint It Cool, Frank Ozs take on the film was "I f***ed up... I had too much money, and I was too responsible and concerned for Paramount. I was too concerned for the producers. And I didnt follow my instincts."  In recent interviews, Kidman, Broderick and producer Scott Rudin have all expressed regret for participating in the film.  

The majority of the film was shot in Darien, Connecticut, New Canaan, Connecticut and Norwalk, Connecticut.  

==Critical reception==
The film was largely panned by the critics; Rotten Tomatoes gave the film a 26%. 
* Rolling Stone said, "Buzz of troubles on the set... cant compare to the mess onscreen." 
* Entertainment Weekly said, "The remake is, in fact, marooned in a swamp of camp inconsequentiality." 
* The New York Times said "the movie never lives up to its satiric potential, collapsing at the end into incoherence and wishy-washy, have-it-all sentimentality." 
 At the Movies with Ebert and Roeper, he admitted that, while he gave the film "thumbs up," it wouldnt be "the first movie that   would defend."

Also, the films teaser won several Golden Trailer Awards, in the categories of "Summer 2004 Blockbuster" and "Most Original", as well as "Best of Show". 

==Box office==
The film was not successful; the US opening weekends gross was a respectable $21,406,781; however, sales fell off quickly and that one weekend would ultimately represent over a third of the films domestic gross of $59,484,742.  The film grossed $42,428,452 internationally; its production budget was an estimated $100 million plus a further $46 million for marketing and distribution costs. 

==See also== The Stepford Wives (1975)
* Asterisk animation - provided animation for this film

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 