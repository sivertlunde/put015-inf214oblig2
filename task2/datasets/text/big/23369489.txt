The House of Trent
{{Infobox film
| name           = The House of Trent
| image          = 
| image size     =
| caption        = 
| director       = 
| producer       = Charles Bennett
| based on = 
| narrator       =
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1933
| runtime        = 
| country        = UK English
| budget         =
| gross = 
| preceded by    =
| followed by    = British drama Norman Walker and starring Anne Grey, Wendy Barrie, Moore Marriott and Peter Gawthorne.  It follows a doctor who faces both a scandal and a moral dilemma when a patient of his dies while he is making love to a press magnates daughter. It was also released as Trents Folly. 

==Cast==
* Anne Grey ...  Rosemary John Stuart ...  John Trent
* Wendy Barrie ...  Angela
* Peter Gawthorne ...  Lord Fairdown
* Hope Davey ...  Joan
* Norah Baring ...  Barbara
* Hubert Harben ...  Editor
* Moore Marriott ...  Ferrier
* Jack Raine ...  Peter
* Dora Gregory ...  Mary
* Estelle Winwood ...  Charlotte
* Hay Plumb ...  Jury Foreman
* Victor Stanley ...  Spriggs
* Humberston Wright ...  Coachman

==References==
 

 

 
 
 
 
 
 
 


 