Sweet Home (film)
{{Infobox film
| name           = Sweet Home
| image          = Shpic.gif
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kiyoshi Kurosawa
| producer       = Juzo Itami
| writer         = Kiyoshi Kurosawa
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Nobuko Miyamoto Shingo Yamashiro Nokko Fukumi Kuroda Ichiro Furutachi
| music          = Masaya Matsuura
| cinematography = Yonezo Maeda Akira Suzuki
| studio         = Itami Productions
| distributor    = Toho
| released       =  
| runtime        = 101 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}} a video game of the same title.

== Plot ==
  possessed by the infuriated ghost of Fujin, Ichirōs wife. The team discovers a makeshift grave where a toddler is buried. The boy is Ichirō and Fujins son, who fell into the houses incinerator one day and burned alive. Since then, Fujins ghost haunts the mansion, killing any trespassers. In the end, only Kazuo, Emi, and Akiko survive, by reuniting Fujin with her beloved son, and so giving them peace. When Kazuo, Emi, and Akiko leave the mansion, it begins to collapse.

== Cast ==
*  (Shingo Yamashiro) – Director of the crew. A firm and friendly guy, but anxious and jumpy. Throughout the movie, his affections for Akiko are apparent but he does not reveal his feelings to her. Kazuo is always worried about his daughter Emi.

*  (Nokko) – Daughter of Kazuo. She lost her mother as toddler and sees Akiko as her “big sister”. Emi is a student on summer vacation. She assists her father as a guide instead of staying at home alone. Near end of the movie, she is kidnapped by the evil ghost of Mamiya and is rescued by Akiko.

*  (Ichiro Furutachi) – Photographer of the crew. He is a womaniser and stalks Asuka. When Asuka is lured away by Mamiya, he follows her.  After being fried in half by the mansions shadows, he is bludgeoned to death by panicked Asuka.

*  (Nobuko Miyamoto) – Producer. She is an extremely self-confident woman with the strongest personality of the crew and the final say. She seems to be aware of Kazuo´s feelings for her, but  pretends to be oblivious.  She is the primary heroine of the movie and saves Emi from Lady Mamiya by pretending to be Emis dead mother.

*  (Fukumi Kuroda) – Reporter. She is also a professional art restorer and therefore responsible for the preservation of the sought-out frescos. Her goal is to get famous from the documentary about Ichirō Yamamura and his paintings.  After killing Taguchi, she is overcome with emotion and does not notice a falling battle axe that smashes straight into her forehead, killing her.

*  – A famous artist who disappeared 30 years ago in his mansion. No one knows what exactly happened to him, but his death is likely connected to the suicide of his beloved wife.

*  (Machiko Watanabe) – Beloved wife of Ichirō. 30 years ago, she lost her son in an accident. After the toddler´s death, Mamiya could not cope with the loss and became insane from the idea of giving her lost son playmates. In attempts to do so, she kidnapped and killed toddlers by burning them in the incinerator. When caught red-handed by local villagers, who immediately pursue her, she commits suicide in the very furnace her son died in. Her infuriated soul is trapped in the house due to a magical memorial outside of the house. It seems that Mamiya is upset about the circumstances concerning the close proximity of her memorial and her sons grave.

== Release ==
  the same title produced by Juzo Itami and published by Capcom was also released in 1989.   According to the games director, Tokuro Fujiwara, he was able to view the film and use what he wanted to as part of the game, and that he "carefully considered how to go about bringing elements from the movie to the game screen". 

== Reception ==
  The Haunting. He said, "Despite its unsurprising plotting, Sweet Home is action-packed, thrill-packed and effects-packed, resulting in a more than entertaining haunted house ride." 

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 