Ayitham
{{Infobox film
| name = Ayitham
| image = Ayithamfilms.png
| caption = Promotional Poster
| director = Venu Nagavally Ambika Radha
| writer = Venu Nagavally
| screenplay = Venu Nagavally Ambika Radha Sukumari
| music = MG Radhakrishnan
| cinematography = Vipin Mohan
| editing = KP Puthran
| studio = ARS Productions
| distributor = ARS Productions
| released =  
| country = India  Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Ambika and Radha and Sukumari in lead roles. The film had musical score by MG Radhakrishnan.   

==Cast==
  
*Mohanlal  Ambika 
*Radha 
*nedumudi venu
*Sukumari 
*Jagathy Sreekumar  Innocent  LPR Varma Murali  Santhosh 
*Maniyanpilla Raju 
*Sukumaran  Seema 
*V. K. Ramaswamy (actor)|V. K. Ramasamy 
*Louba Schild
 

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by and ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aliveni || KS Chithra, K Omanakkutty || || 
|- 
| 2 || Ga ma ga ri ga pa  || K. J. Yesudas, BA Chidambaranath || ONV Kurup || 
|- 
| 3 || Ilamarimaan Nayane || KS Chithra || || 
|- 
| 4 || Jagadodhaarana || KS Chithra || || 
|- 
| 5 || Maaye || KS Chithra, K Omanakkutty || || 
|- 
| 6 || Nalinamizhi   || KS Chithra, K Omanakkutty || || 
|- 
| 7 || Oruvakkil Oru Nokkil || K. J. Yesudas || ONV Kurup || 
|- 
| 8 || Parama Purusha || KS Chithra, K Omanakkutty || || 
|- 
| 9 || Thankamani Anna || KS Chithra, MG Sreekumar || ONV Kurup || 
|- 
| 10 || Uyyaala Loogavaiya   || K. J. Yesudas || || 
|- 
| 11 || Uyyala loogavaiya   || KS Chithra || || 
|- 
| 12 || Vaaneedevi || KS Chithra, K Omanakkutty || || 
|}

==References==
 

==External links==
*  

 
 
 
 


 