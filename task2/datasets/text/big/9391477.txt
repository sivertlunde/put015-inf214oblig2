A Very Young Lady
 
{{Infobox Film
| name = A Very Young Lady
| image =
| image_size =
| caption =
| director = Harold D. Schuster
| producer =
| writer =
| narrator =
| starring = Jane Withers   Nancy Kelly
| music =
| cinematography =
| editing =
| distributor =
| released = June 27, 1941
| runtime =80 minutes
| country =United States
| language =English
| budget =
| preceded_by =
| followed_by =
}}

A Very Young Lady is a 1941 film comedy starring Jane Withers and Nancy Kelly. The movie was directed by Harold D. Schuster.

==Cast==
* Jane Withers as Kitty Russell
* Nancy Kelly as Alice Carter
* John Sutton as Dr. Franklin Meredith
* Janet Beecher as Miss Steele
* Richard Clayton as Tom Brighton
* June Carlson as Madge
* Charles Halton as Oliver Brixton
* Cecil Kellaway as Professor Starkweather
* Marilyn Kinsley as Susie
* JoAnn Ransom as Linda
* Catherine Henderson as Jean
* Lucita Ham as Sarah
* June Horne as Beth

==External links==
*  
 
 
 
 
 

 