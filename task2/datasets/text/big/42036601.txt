Pretty Ugly People
{{Infobox film
| image          = Pretty Ugly People.jpg
| caption        = THeatrical release poster
| director       = Tate Taylor
| producer       = Brunson Green Kurt Kelly
| co-producer    = Sonya Lunsford
| writer         = Tate Taylor
| starring       = Missi Pyle Melissa McCarthy Octavia Spencer
| music          = Lucian Piane
| cinematography = J.P. Lipa
| editing        = Justin C. Green
| studio         = Harbinger Pictures Plump Pictures
| distributor    = Gravitas Ventures
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
}}
Pretty Ugly People is a 2008 American comedy drama film written and directed by Tate Taylor, and starring Missi Pyle, Melissa McCarthy,  and Octavia Spencer.         

The film is the story of a group of friends brought together for a four day hike in the Montana wilderness by one their number, Lucy (Missi Pyle).  She is celebrating losing hundreds of pounds of weight after gastric bypass surgery.  However, as she quickly realizes while she has become thin and happy, her friends are miserable.  The films title is derived from this realization.

The film was filmed in Montana.

==Plot==

The film begins with a voice-over by Lucy (Missi Pyle) discussing calling her friend Becks (Melissa McCarthy) and insinuating she is dying. She asks Becks to rally all of the womens college friends for one final trip to see Lucy in Montana.

Orchestrated by Becks, the circle of friends meet up in an airport in Montana. Becks brings her husband Richard and introduces him, for the first time, to commodities trader George, Grammy nominated rap producer Trevor, flight attendant Austin, and State Representative Raye who also brings his wife, Mary (Octavia Spencer).  When the group is assembled a man named Sam approaches the group and tells them Lucy has sent him to drive them to the lodge where she is staying.

After arriving at the lodge, the group is greeted by a svelte, attractive blonde women.  When no one recognizes her, Lucy reveals to her friends that it is her.  Shortly thereafter, at lunch, Lucy tells her friends that she had gastric bypass surgery and she remains four pounds away from her ideal weight.

Waking the group early the next morning, she informs them that they are going on a four-day hike in which she will drop her finally four pounds of weight.  The majority of the film centers on the hiking trip the friends take.  Over the course of the trip, various aspects of the lives of all of Lucys friends are revealed.  George reveals himself as gay.  Raye and Mary are unhappy together and Mary ends up taking off her wedding ring and ending her marriage to Raye.  Similarly, Becks and Richard are equally unhappy.

The group finally makes it to the end of the trip and Lucy prepares dinner for them at night before Sam comes to pick everyone up in the bus.  Lucy tells them she believed that becoming thin would make her as happy as them, but what she realized on the trip is that none of them are happy, and that it never had anything to do with weight.

As Sam is driving the group back to the lodge on the bus, he has a heart attack and the bus goes off the road.  Sam is dead and Richard dies while waiting for help.  Everyone else survives.  In the final scenes of the movie, everyone but Raye, who has gone back to Washington and wins a Senate election, is having lunch on a beach, an inestimable time after the bus accident.  Lucy and Becks, who is pregnant, wade into the water so that Becks can toss Richards ashes to sea.

==Awards==

The film seems largely ignored by critics, however, it did well on the independent film circuit.  It was the winner of "Best Feature Film" at the Berks Madness film festival.  It took the same award at both the Berkley and Long Island film festivals.  Also, it won an "audience choice" award at the Crossroads film festival.

==Issues raised in the film==

While on its face, the film seems to be a type of black comedy, various questions centering on identity are raised in the film.

Austin, the flight attendant, remains unmarried and in many instances various individuals within the circle of friends insinuate he is gay.  On two separate occasions, Austin is called a "faggot (slang)|fag" when having confrontations with other members of the circle.  Interestingly, the belief in Austins homosexuality leads George to kiss him at one point.  However, Austin, surprised and heterosexual, pushes George away.  The films raises questions about perceptions and stereotypes regarding homosexuality.
 ebonics shit."  In watching the way in which these two characters relationship plays out, the idea of Black identity is complicated within the realm of the film.

==References==
 

==External links==
* 

 

 
 
 
 