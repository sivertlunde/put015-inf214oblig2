Sleeper (2005 film)
{{Infobox film
| name           = Sleeper
| caption        = 
| image	=	Sleeper FilmPoster.jpeg
| director       = Benjamin Heisenberg
| producer       = Jessica Hausner Bruno Wagner
| writer         = Benjamin Heisenberg
| starring       = Bastian Trost
| music          = 
| cinematography = Reinhold Vorschneider
| editing        = Karina Ressler
| distributor    = 
| released       = 19 May 2005
| runtime        = 100 minutes
| country        = Austria Germany
| language       = German
| budget         = 
}}

Sleeper ( ) is a 2005 Austrian-German drama film directed by Benjamin Heisenberg. It was screened in the Un Certain Regard section at the 2005 Cannes Film Festival.   

==Cast==
* Bastian Trost - Johannes Mehrveldt
* Mehdi Nebbou - Farid Atabay
* Loretta Pflaum - Beate Werner
* Gundi Ellert - Frau Wasser
* Wolfgang Pregler - Professor Behringer
* Charlotte Eschmann - Johannes Grandmother
* Ludwig Bang - Secret service agent
* Masayuki Akiyoshi - Fei Li
* Marco Schuler - Robert Königsbauer
* Jürgen Geißendörfer - Markus
* Dominik Dudy - LAN-student
* Andrea Faciu - Singer
* Gordana Stevic - Mrs. Stevic
* Christine Böhm - Mrs. Schmid
* Anke Euler - Animal Nurse

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 