Crazy Over Horses
{{Infobox film
| name           = Crazy Over Horses
| image_size     =
| image	         = Crazy Over Horses FilmPoster.jpeg
| caption        =
| director       = William Beaudine Jerry Thomas Tim Ryan
| narrator       =
| starring       = Leo Gorcey Huntz Hall David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       =  
| runtime        = 65 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Crazy Over Horses is a 1951 comedy film starring The Bowery Boys. The film was released on November 18, 1951 by Monogram Pictures and is the twenty-fourth film in the series.

==Plot==
Louie is owed money by a stable-owner and sends Slip and the boys over to collect the debt.  They return with a horse, My Girl, as payment.  Local gangsters want the horse and switch their horse, Tarzana, for the gangs horse.  They boys discover the ruse and the horses are switched several more times.  In the end, Sach rides the real My Girl in a horse race, beating Tarzana and the gangsters.

==Production==
This was the first film that David Gorcey was credited under using his mothers maiden name, Condon.   It is also the return of Bennie Bartlett to the gang.

This was also the last appearance of William Benedict in the series. Benedicts reason for leaving the series was that "I suddenly decided I had enough, and it was getting a little rough doing em - emotionally. There was a lot of infighting going on and I said, I dont need this". 

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Aloysius Slip Mahoney
*Huntz Hall as Horace Debussy Sach Jones
*William Benedict as Whitey
*David Gorcey as Chuck (Credited as David Condon)
*Bennie Bartlett as Butch

===Remaining cast===
*Bernard Gorcey as Louie Dumbrowski
*Gloria Saunders as Terry Flynn
*Ted de Corsia as Duke Tim Ryan as Mr. Flynn
*Allen Jenkins as "Weepin Willie"

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==References==
 

==External links==
* 
* 

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Lets Go Navy! 1951
| after=Hold That Line 1952}}
 

 
 

 
 
 
 
 
 


 