Lord Love a Duck
 
{{Infobox Film
| name           = Lord Love a Duck
| image          = Lordloveaduck.jpg 
| image_size     = 
| caption        = theatrical poster 
| director       = George Axelrod
| producer       = 
| writer         = Al Hine George Axelrod
| narrator       = 
| starring       = Roddy McDowall Tuesday Weld Lola Albright Martin West        Ruth Gordon Harvey Korman
| music          = Neal Hefti
| cinematography = 
| editing        = 
| distributor    = United Artists 1966
| runtime        = 
| country        = United States
| language       = English
| budget         = $850,000 Standing Up To the Teen-Agers
By PETER BARTHOLLYWOOD.. New York Times (1923-Current file)   15 Aug 1965: X7. 
|
}} 1966 black comedy starring Roddy McDowall and Tuesday Weld. The film was a satire of popular culture at the time, its targets ranging from progressive education to Beach Party films. It is based on Al Hines 1961 novel of the same name.

==Plot==
From his prison cell, Alan Musgrave dictates his experiences of the previous year, which he dedicated to fulfilling the unending wishes and ambitions of high school senior Barbara Ann Greene. The daughter of Marie, a cocktail waitress sinking unhappily into her forties, Barbara wants every kind of success and for everyone to love her. Signing a pact with Alan in wet cement, Barbara soon has the 12 cashmere sweaters needed to join an exclusive girls club. She drops out of school to become the principals new secretary and gets involved in church activities run by strait-laced but hyper-hormonal Bob Bernard. When Barbara Ann decides she wants Bob for her husband, Alan facilitates by keeping Bobs eccentric mother Stella, who disapproves of Barbara Ann, perpetually plastered. Then Barbara meets schlock producer T. Harrison Belmont, the King of Beach Party movies, and decides to become the biggest star that ever was. Bob refuses, however, to allow his wife to have a Hollywood screen test, so Barbara Ann decides she wants a divorce.   Since Bobs mother frowns upon divorce, Alan takes matters into his own hands to kill Bob. Although Bob proves to be almost indestructible, by graduation time Alan has him in a wheelchair. At the graduation ceremony, Alan pursues Bob with a tractor, humorously killing him and everyone else on the speakers platform. Barbara Ann goes on to Hollywood fame in her debut film "Bikini Widow", while Alan is sent to prison. 

==Cast==
*Roddy McDowall as Alan "Mollymauk" Musgrave
*Tuesday Weld as Barbara Ann Greene
*Lola Albright as Marie Greene Martin West as Bob Bernard
*Ruth Gordon as Stella Bernard
*Harvey Korman as Weldon Emmett

==Awards==
Lola Albright won the Silver Bear for Best Actress award at the 16th Berlin International Film Festival in 1966.   

==References==
 
*Hines, Al. Lord Love a Duck (Atheneum_Books|Atheneum, 1961)

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 