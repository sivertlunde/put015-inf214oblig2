Joshila
{{Infobox film
| name           = Joshila
| image          = Joshila.jpg
| image_size     =
| caption        = 
| director       = Yash Chopra
| producer       = Gulshan Rai
| writer         = Akhtar ul-Iman Akhtar Mirza Gulshan Nanda C. J. Pavri
| narrator       =  Pran Bindu Bindu Raakhee
| music          = R. D. Burman
| cinematography = Fali Mistry
| editing        = Pran Mehra
| studio         = Chandivali Studio Filmistan Studios Rajkamal Studios
| distributor    = Trimurti Films
| released       = 19 October 1973
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Joshila ( ;   Bollywood thriller film directed by Yash Chopra. The film stars Dev Anand, Raakhee and Hema Malini.

==Cast==
*Dev Anand - Amar
*Hema Malini - Shalini
*Raakhee - Sapna  Pran - Thakur sahib Bindu - Rani
*Madan Puri - Madanlal Dogra (he is killed in an encounter with a group of gundas in the hotel and hence, dev anand impersonates him)
*Manmohan Krishna - Jailor (as Manmohan Krishan)
*Padma Khanna - Manju
*A. K. Hangal - Lala Gulzarilal
*Sulochana Latkar - Mrs. Gulzarilal (as Sulochana) Sudhir - Kundan Hercules - SherSinh Master Satyajeet - Ravi (as Master Satyajit)
*I. S. Johar - Raunaq Singh
*Vikas Anand - Manohar
*Roopesh Kumar - Sapnas brother
*Iftekhar
*Jagdish Raj - (as Jagdishraj)
*Mahendra Sandhu

==Plot==
Jailor Manmohan Krishan is responsible for looking after convicts undergoing sentences of rigorous imprisonment. He has a young, beautiful and captive daughter named Shalini. Shalini is a poet, one day while reciting her poetry, she meets with a young man, who introduces himself as Amar, who also happens to be a poet himself. The two of them spend beautiful moments together and finds themselves attracted to each other. Shalini wants to find out why Amar is in jail. She is told in no uncertain terms that Amar is in jail for murder - for killing the brother of his former lover, Sapna. She also found out that this is not true and would strive to the best of her merit to get Amar release from jail because she is in love with him and would like to marry him.

==Crew==
*Director - Yash Chopra
*Story - Gulshan Nanda
*Screenplay - Akhtar Mirza, C. J. Pavri
*Dialogue - Akhtar-Ul-Iman
*Producer - Gulshan Rai
*Editor - Pran Mehra
*Cinematographer - Fali Mistry
*Art Director - R. G. Gaekwad
*Assistant Director - Ramesh Talwar (chief), Vasudev Dheer, Dilip Naik
*Assistant Editor - T. R. Mangeshkar (chief), Achyut Y. Gupte (color consultant)
*Assistant Art Director - Sudhir Gaekwad
*Stunts - Ravi Khanna, M. B. Shetty
*Makeup - Pandhari Juker
*Production Manager - R. L. Bajaj, Devdutt Bharti
*Costume and Wardrobe - Meeta Hassan, Wilmary Ventura Suresh Bhatt, P. L. Raj
*Original Music - R. D. Burman
*Music Assistant - Manohari Singh
*Lyricist - Sahir Ludhianvi
*Playback Singers - Asha Bhosle, Kishore Kumar, Lata Mangeshkar

==Music==
{|class="wikitable"
|-
!Song Title !!Singers
|-
|"Kiska Rasta Dekhe" Kishore Kumar
|-
|"Dil Mein Jo Baatein Hain" Asha Bhosle, Kishore Kumar
|- 	
|"Jo Baat Ishaaron Mein Kahi" Lata Mangeshkar
|-
|"Kaanp Rahi Main" Asha Bhosle
|-
|"Kuch Bhi Kar Lo" Kishore Kumar, Lata Mangeshkar
|-
|"Mehfil Mein Chhupaane Pade" Lata Mangeshkar
|-
|"Sharma Na Yoon" Asha Bhosle
|-
|"Sona Rupa Laayo Re" Asha Bhosle
|-
|}

==External links==
*  
*   at Yash Raj Films

 

 
 
 
 
 
 