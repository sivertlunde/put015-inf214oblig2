Li Lianying: The Imperial Eunuch
{{Infobox film name = Li Lianying: The Imperial Eunuch image =  alt =  caption =  traditional = 大太監李蓮英 simplified = 大太监李莲英 pinyin = Dà Tàijiān Lǐ Liányīng}} director = Tian Zhuangzhuang producer = Alan Chui writer = Guo Tianxiang starring = Jiang Wen Liu Xiaoqing Xu Fan Zhu Xu music = Mo Fan cinematography = Zhao Fei editing =  studio = Beijing Film Studio Skai Film Production Ltd. distributor =  released =  runtime = 110 minutes country = China language = Mandarin budget =  gross = 
}}
Li Lianying: The Imperial Eunuch, also known as The Last Eunuch, is a 1991 Chinese biographical film directed by Tian Zhuangzhuang. It tells the story of Li Lianying, a eunuch who wielded power in the waning days of the Qing Dynasty. The film was entered into the 41st Berlin International Film Festival, where it won an Honourable Mention.   

==Cast==
*Jiang Wen as Li Lianying
*Liu Xiaoqing as Empress Dowager Cixi
*Xu Fan as Consort Zhen
*Zhu Xu as Prince Chun
*Tian Shaojun
*Liu Bin
*Ding Jiali

==References==
 

==External links==
* 
* 
* 
*  at the Chinese Movie Database

 

 
 
 
 
 
 
 


 
 