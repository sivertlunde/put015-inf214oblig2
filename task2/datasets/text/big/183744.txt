Sister Kenny
{{Infobox film
| name           = Sister Kenny
| image          = SisterKennyPoster.jpg
| image_size     = 180px
| caption        = Movie Poster Jack Gage (dialogue director)
| producer       = Dudley Nichols Mary McCarthy
| starring       = Rosalind Russell Alexander Knox Dean Jagger
| music          = Alexander Tansman George Barnes
| editing        = Roland Gross
| distributor    = RKO Radio Pictures
| released       =   |ref2= }}| runtime        = 116 min
| language       = English
| budget         =
}}

Sister Kenny (1946) is a biographical film about Sister Elizabeth Kenny, an Australian bush nurse, who fought to help people who suffered from polio, despite opposition from the medical establishment.  The film stars Rosalind Russell, Alexander Knox, and Philip Merivale.
 Mary McCarthy Milton Gunzburg (uncredited) and Dudley Nichols from the book And They Shall Walk, by Elizabeth Kenny and Martha Ostenso, and directed by Dudley Nichols.

The "Sister" in the title does not refer to being a nun, rather, a rank she had held as a nurse in the Australian Army and also  a term for a senior RN (which Kenny was not).

==Plot== bush of Queensland with the head doctor at the nearest hospital, Dr. McDonnell (Alexander Knox). She informs him that she will be a bush nurse near her home instead of taking up residency at the hospital 50 miles away; her own personal experiences growing up in the bush made her want to help those not able to get to a hospital.  Dr. McDonnell is incredulous, and says she will not last six months in the bush.
 infantile paralysis has no known treatment, but "to do the best you can with the symptoms presenting themselves."  Sister Kenny notices that if the muscles were truly paralyzed, they could not tense up as they were doing.  She wraps Dorrie in hot cloth, and the girl eventually fully recovers, as do five other cases of infantile paralysis that Sister Kenny finds and treats.

Sister Kenny visits McDonnell to tell him of her new treatment, but instead of congratulating her, he tells her she may have caused much harm.  Sister Kenny is shocked, but McDonnell eventually comes to believe that her "muscle re-education" shows signs of real promise.  He takes her to Dr. Brack (Philip Merivale), the most senior doctor working on polio, who tells them both that her theories of how to treat the disease goes against the science of the past 50 years and cannot possibly be accepted as medical truth. Even when Sister Kenny brings in Dorrie to dance and do cartwheels, Brack instead contends that Dorrie never had infantile paralysis in the first place.
 to fight in World War I, and Sister Kenny follows him to the hospital in England where he is recuperating from a leg injury.

She returns to Australia and tells McDonnell she has no interest in starting a clinic, and is worn out by being constantly dismissed by the medical establishment. However, she reads in the paper about a polio outbreak in Townsville, and starts up her practice treating the children there.  Kevin returns home, Sister Kenny is going back to nursing. He is incredulous at the news, but she is adamant that she will continue her work despite of how her personal happiness will suffer

Over a decade later, Sister Kenny and Brack still stand by their respective treatments, and she decides to confront him at a symposium in front of many Orthopedic surgery|orthopedists.  Brack belittles her treatment, but she learns that McDonnell has been working on setting up a Royal Commission so that the Australian medical community will recognize her treatment. She travels around Europe spreading her cure, and is visited by Kevin, who is proud of the route she has taken in her life.  However, when she returns to Australia in 1939, she learns the Commission condemned her treatment as unprovable and recommended closing down her clinics.
 attack on Happy Birthday.

==Cast==
 
* Rosalind Russell as Elizabeth Kenny  
* Alexander Knox as Dr. McDonnell  
* Dean Jagger as Kevin Connors  
* Philip Merivale as Dr. Brack  
* Beulah Bondi as Mary Kenny  
* Charles Dingle as Michael Kenny  
* John Litel as Medical Director  
* Doreen McCann as Dorrie  
* Fay Helm as Mrs. McIntyre  
* Charles Kemper as Mr. McIntyre  
* Dorothy Peterson as Agnes

==Reception==
The film recorded a loss of $660,000 for RKO. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p215 

==Awards==
;Wins
* Golden Globe Award for Best Actress - Motion Picture Drama

;Nominations To Each His Own)

==References==
 

==External links==
*   at Classic Film Guide
*  
*  
*  

 

 
 
 
 
 
 
 
 