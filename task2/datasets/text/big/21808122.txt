Torrid Noon
 
{{Infobox film
| name           = Torrid Noon (Горещо пладне)
| image          = 
| caption        = 
| director       = Zako Heskija
| producer       = Ivan Kordov
| writer         = Yordan Radichkov
| starring       = Plamen Nakov
| music          = 
| cinematography = Todor Stoyanov
| editing        = Katya Vasileva
| distributor    = 
| released       =  
| runtime        = 89 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
}}

Torrid Noon ( , Transliteration|translit.&nbsp;Goreshto pladne) is a Bulgarian drama film released in 1966, directed by Zako Heskija. It was entered into the 1965 Cannes Film Festival.     

==Cast==
* Plamen Nakov - Aleko
* Peter Slabakov - Generalat
* Grigor Vachkov - Selyanin
* Rousy Chanev - Voynik
* Gerasim Mladenov - Chinovnik
* Vladislav Molerov - Kapitanat
* Kalina Antonova - Devoyka
* Ivan Bratanov - Selyanin
* Dimitar Panov
* Naicho Petrov - Pasazher
* Dora Staeva - Mayka
* Kyamil Kyuchukov
* Lachezar Yankov
* Aszparuh Sarjev
* Lyubomir Kirilov
* Dimitar Manchev

==References==
 

==External links==
* 

 
 
 
 
 
 
 