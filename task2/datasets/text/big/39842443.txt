Welcome Back (film)
 
 
{{Infobox film
| name           = Welcome Back
| image          =
| caption        =Film Logo
| director       = Anees Bazmee
| producer       = Firoz Nadiadwala
                   F M Khan Swiss Entertainment Pvt Ltd                 
| writer         =Anees Bazmee John Abraham Paresh Rawal Shruti Haasan Shiney Ahuja Brahmanandam
| music            = Anu Malik Anand Raaj Anand
| cinematography = Kabir Lal
| editing        = Steven Bernard
| studio         = Base Industries Group
| distributor    = 
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}} 2007 comedy Welcome (2007 film)|Welcome, and features an ensemble cast that includes Nana Patekar and Anil Kapoor reprising their roles respectively from the prequel, along with Paresh Rawal.
 John Abraham and Shruti Haasan are new additions to the cast, Earlier expected to release in December 2014, the film is now expected to release on May 29, 2015.

==Cast==
 John Abraham as Sharad Gupta 
* Shruti Hassan
* Nana Patekar as Uday Shetty
* Anil Kapoor as Majnu Bhai/Sagar Patil 
* Paresh Rawal as Dr. Ghunghroo
* Naseeruddin Shah 
* Dimple Kapadia
* Shiney Ahuja  
* Sakshi Maggo 
*Brahmanandam
* Sara Loren Special appearance in song

==Production==
 John Abraham was next signed on for the lead role, thus replacing actor Akshay Kumar from the original movie.   Katrina Kaif, who played the female lead in the original movie was also replaced by Sonakshi Sinha  however she soon dropped out citing date issues.  
 All is Well,  she opted out and was replaced by Shruti Haasan.   A debutante Sakshi Maggo from Delhi was given a role in the film. Sara Loren has been selected to perform an item song in the film. 

==Soundtrack==
The soundtrack is composed by Anu Malik & Anand Raj Anand. Anu Malik has said in an interview that he has composed two songs which have been pictured recently on the whole cast and crew. Anu Malik has sung a song in the film as said by him in a latest interview.

==References==
 

 

 
 
 
 