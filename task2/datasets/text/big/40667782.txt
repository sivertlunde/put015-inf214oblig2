Dasi Aparanji
{{Infobox film
| name = Dasi Aparanji
| image = Dasi aparanji.jpg
| caption = Theatrical poster
| director = B. N. Rao
| producer = S. S. Vasan
| starring = Pushpavalli
| country = India
| language = Tamil
}}
Dasi Aparanji (  directed by B. N. Rao and produced by S. S. Vasan. The film stars Pushpavalli, Kothamangalam Seenu, M. K. Radha, Kothamangalam Subbu, L. Narayana Rao and Sundari Bai. 

==Plot==
Aparanji, a devadasi, lives in Ujjain during the reign of King Vikramaditya. Beautiful, accomplished in the traditional 64 arts and haughty, she demands a fee of one thousand gold coins for a night of joy even from those who merely think of her! She is also pious and prays hard to go to Kailas.

Vikramaditya follows the custom of spending six months in the forest in meditation. Vijaya, a wolf in sheeps clothing who covets the throne and the lovely queen Padmavathi, accompanies the king to the forest. Both are experts in the esoteric art of entering dead bodies! Vijaya kills a parrot and sheds crocodile tears. Deeply moved, the king sheds his body, enters the parrots and brings it to life. At once, Vijaya enters the kings body and returns to Ujjain!

The parrot falls into the trap of a hunter, who sells it to Dhanapal, a rich man. The clever bird helps the man decide cases coming up before him. One day an unusual case comes up for judgment. A poor temple priest (Kothamangalam Subbu) lusts after Aparanji but he cannot afford her. So he concocts a love potion which the maid (Sundari Bai) consumes by mistake! Meanwhile, the priest awaits Aparanji and falls asleep. He enjoys her favours in an erotic dream that seems to last forever! The next morning he tells his friends about it at the temple pond and Aparanji overhears him! At once she demands her fee and as the priest has no gold she files a case before the magistrate.

The parrot gives an amazing solution. A mirror is kept in the court in which the reflection of one thousand gold coins is seen! "The poor man enjoyed you in his dream and as your fee you can take what you see in the mirror," the bird declared to the astonishment of those present. How it all ends forms the rest of the story.
==Cast==
* Pushpavalli
* Kothamangalam Seenu
* M. K. Radha
* Kothamangalam Subbu
* L. Narayana Rao
*  Sundari Bai

==Reception==
Film historian Randor Guy praised the film for "its offbeat content, brilliant photography and Pushpavalli." http://www.hindu.com/cp/2008/09/19/stories/2008091950351600.htm 

==References==
 

 
 
 