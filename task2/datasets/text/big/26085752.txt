The Utah Kid (1944 film)
{{Infobox film
| name           = The Utah Kid
| image          = The Utah Kid 1944 movieposter.jpg
| director       = Vernon Keays
| producer       = William Strohbach (supervising producer)
| screenplay     = Victor Hammond
| writer         = Lindsley Parsons
| starring       = {{plainlist|
*Hoot Gibson Bob Steele
}}
| cinematography = Harry Neumann
| editing        = {{plainlist|
*Ray Curtiss
*John C. Fuller
}}
| released       =  
| runtime        = 53 minutes
| country        = United States
| language       = English
}}
 Western film directed by Vernon Keays.

== Cast ==
*Hoot Gibson as Marshal Hoot Higgins Bob Steele as Bob Roberts
*Beatrice Gray as Marjorie Carter Ralph Lewis as Cheyenne Kent
*Evelyn Eaton as Dolores
*Mauritz Hugo as Barton George Morrell as Sheriff Dan White as Henchman Slim
*Mike Letz as Henchman Blackie
*Jameson Shade as Judge Carter

== External links ==
* 

 

 
 
 
 
 
 
 
 
 


 