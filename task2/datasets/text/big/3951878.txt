The Aggressives
 
{{Infobox film
| name           = The Aggressives
| image          = The Aggressives movie poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Taepungtaeyang
| mr             = T‘aep‘ungt‘aeyang }}
| writer         = Jeong Jae-eun
| starring       = Chun Jung-myung Kim Kang-woo Jo Yi-jin Lee Chun-hee
| director       = Jeong Jae-eun
| producer       = Kim Dong-joo
| editing        = Ji Mi-hyang Shin Mi-kyung
| cinematography =
| distributor    = Showeast
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| music          =
| budget         =
}} 2005 South Korean film about a teen, Soyo, whose parents have abandoned him to fend for himself. He becomes immersed in the local inline skating subculture, and falls in love with the girlfriend of his skating tutor. The film was screened at the 2005 Pusan International Film Festival.

==Plot==
Soyo (Chun Jung-myung) is a quiet, conscientious sixth form student. Although he doesn’t enjoy school, he attends dutifully, without a word of complaint. And then, one day, he discovers inline skating. A complete beginner, he practices at first in a hidden corner of the park. Here, he meets a group of wild skaters and immediately, one skater, Mogi (Kim Kang-woo), catches his eye. His adventurous jumps and breathtaking loops defy all laws of gravity. Mogi is without doubt the star of the group. His stunts, his style and the cool way he executes even the most daring of figures are unrivalled. Mogi’s girlfriend, Hanjoo (Jo Yi-jin), invites Soyo to join their team and he accepts enthusiastically.   

No sooner does he become a member of the skaters than his life changes completely. The loneliness Soyo sometimes felt simply disappears – as does his quiet existence. He soon makes enormous progress as a skater. Soyo’s life becomes faster and more exciting, and Mogi and Hanjoo turn out to be the kind of friends he always dreamed of having. The team are busy preparing for the world championships, in which Mogi is to take part. But then, disaster strikes. The film crew with whom Mogi is working on a commercial shoot is particularly condescending to him. He allows himself to be goaded into performing a particularly dangerous jump that ends in a bad accident. All at once, the whole team is completely absorbed with trying to scrape the money together to pay off their looming debts, and Mogi appears to have lost all interest in skating. Soyo’s faith in his new friends dwindles and the once-successful team threatens to fall apart. 

==References==
 

== External links ==
*    
*  
*  

 
 
 
 
 
 
 
 
 


 