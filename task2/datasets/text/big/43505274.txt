Una botta di vita
{{Infobox film
 | name = Una botta di vita
 | image = Una botta di vita.jpg
 | caption =
 | director =Enrico Oldoini
 | writer =  	  Alberto Sordi  Liliana Betti    Age & Scarpelli  Enrico Oldoini 
 | starring =  Alberto Sordi Bernard Blier
 | music =Manuel De Sica 
 | cinematography =   Giuseppe Ruzzolini
 | editing =   Raimondo Crociani
 | language = Italian 
| country = Italy
| released = 1988
| runtime = 92 min
 }} 1988 Italian comedy film directed by Enrico Oldoini.    

== Plot ==
Two retirees who were been abandoned by their families, Elvio Battistini and Giuseppe Mondardini, tired of spending yet another August in the city, decide to take a "shot of life". Using the car made available by Mondardini, the two decided to go to visit a friend who lives in Bordighera and then to spend a few days in Portofino.

== Cast ==

*Alberto Sordi: Elvio Battistini
*Bernard Blier: Giuseppe Mondardini
*Andréa Ferréol: Germaine
*Vittorio Caprioli: Riccardo  
*Alberto Sorrentino: Friend of Battistini 
*Elena Falgheri: Camilla
*Nerina Montagnani: Old woman

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 


 
 