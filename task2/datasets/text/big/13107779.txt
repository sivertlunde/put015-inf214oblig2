Dulhe Raja
{{Infobox film
| name           = Dulhe Raja
| image          = Dulhe_Raja.jpg
| image_size     = 
| caption        = 
| director       = Harmesh Malhotra
| producer       = Harmesh Malhotra
| writer         = Rajeev Kaul
| narrator       =  Govinda Raveena Tandon Kader Khan Prem Chopra Johnny Lever
| music          = Anand-Milind
| cinematography = Shyam Rao Shiposkar
| editing        = Govind Dalwadi
| distributor    = 
| released       = July 10, 1998
| runtime        = 155 mins
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Dulhe Raja ( ) is a 1998 Bollywood comedy film starring Govinda (actor)|Govinda.,    Raveena Tandon, Kadar Khan, Johnny Lever, Prem Chopra and  Asrani.

==Plot Synopsis==

K.K. Singhania (Kader Khan) owns a five star hotel, but a small dhaba owned by small-timer Raja (Govinda) in his premises puts his hotel business in jeopardy. Soon, the battle between K.K. Singhania (Kader Khan) and Raja (Govinda) leads to hilarious situations.

==Plot==

K.K. Singhania (Kader Khan) buys an expensive hotel "Maharaja International" from P.K. Diwani (Dinesh Hingoo). Later on, Singhania finds that a dhaba inside the hotel complex run by a petty guy Raja (Govinda) acts as a hitch to the earnings of the hotel. Hilarity starts henceforth, when every ploy used by Singhania to dislodge Rajas dhaba ends in a smoke.

Singhanias daughter Kiran (Raveena Tandon) is in love with a man named Rahul (Mohnish Behl). Rahul is financially aided by a confederate Bishambar Nath (Prem Chopra). At the same time, Raja develops a fascination for Kiran. Raja, who is already a thorn in Singhanias side, proclaims his love for Kiran before Singhania. He persistently begs Singhania for Kirans hand only to be refused by Singhania.

However, Singhania knows that Rahul is a rogue whose pursuit is to lure young women and later abandon them after exploiting their wealth. So he warns Kiran not to marry Rahul. After a strife between Singhania and Kiran, the former says that Kiran is allowed to marry any man, be he a destitute or Singhanias enemy, without his refusal but Rahul.

One day, Raja knocks down the top floor of Singhanias hotel after Singhania tries to demolish his dhaba using a municipality bulldozer. Enraged at this, Singhania vehemently declares Raja as his greatest enemy. Kiran overhears this, and decides to thwart her fathers decision that Kiran is allowed to marry any man, be he a destitute or Singhanias enemy but Rahul. She starts a mendacious love affair with Raja gaining the latters affection.

The story takes a turn when Kiran dumps Raja and declares Rahul as her ultimate man. She departs for Rahuls home, and Rahul finds it a great opportunity to acquire Singhanias entire wealth as his daughter is now in his custody. He assaults Kiran, confines her in his home and demands Singhania by phone his entire wealth and property as a pay-off for his daughters life.

Raja learns about Rahuls vicious plan and decides to rescue Kiran. He arrives at Rahuls house and hoodwinks him and his accomplices that they become hostile at one another. He beats up Rahul when Singhania arrives with some documents, apparently to transfer his entire wealth to Bishambar. Delighted at attaining Singhanias entire assets, Bishambar signs the papers without reading them, but later finds that they are not the property papers as he expected, but his confession of kidnapping Kiran and threatening her life. The papers further state his desire for his cloths and other belongings to be seized by his men forcibly. His men snatch away his possessions when police arrives and arrests Rahul, Bishambar and their associates.

In the final scene, Singhania, Kiran and Raja unite.

==Cast== Govinda  as  Raja
*Raveena Tandon  as  Kiran Singhania
*Kader Khan  as  K.K. Singhania
*Prem Chopra  as  Bishambar Nath
*Mohnish Behl  as  Rahul Sinha
*Johnny Lever  as  Baankey
*Asrani  as  Inspector Ajgar Singh
*Dinesh Hingoo  as  Diwani Seth
*Viju Khote  as  Municipal Commissioner Toddal
*Manmauji  as  Chhotu
*Guddi Maruti  as  Ajgar Singhs Wife
*Sudhir  as  SSP Nissar Khan
*Veeru Krishnan  as  Kiran Dance Teacher
*Ghanashyam Rohera  as  Police Constable
*Anjana Mumtaz  as  Mrs. Singhania, Kirans mother
*Rana Jung Bahadur  as  Pitambar Nath
*Anil Nagrath  as  Sukhiram

==Music==
Songs of the film were scored by Anand-Milind who presented some remarkably striking numbers for the album. The frisky "Suno Sasurjee Ab Zidd Chhodo" became significantly popular, followed by "Ankhiyon Se Goli Maare" picturised on Govinda and Raveena Tandon which is pretty identical to the perky romantic songs sought-after at that period, like "Ole Ole" (Yeh Dillagi) and "Husn Hai Suhaana" (Coolie No. 1). In the few years that followed 1998, the film was relayed on some Indian TV channels on which a snippet of the song "Ankhiyon Se Goli Maare" was shown as a trailer of the film. Other songs of the film are pleasant to the ears, but the aforesaid two went on to become the most popular songs of the album. Mention may be made that an instrumental version of the song "Ankhiyon Se Goli Maare" synchronizes the credit display at the beginning of the film. The lyrics were created by Sameer (lyricist)|Sameer.

===Soundtrack===
{{Track listing
| headline     = Songs
| extra_column = Playback Sameer
| all_music    = Anand-Milind

| title1  = Aayi Ban Ke Root
| extra1  = Sonu Nigam, Anuradha Paudwal
| length1 = 5:35

| title2  = Ankhiyon Se Goli Maare 	
| extra2  = Sonu Nigam, Jaspinder Narula
| length2 = 5:12

| title3  = Dulhan To Jayegi
| note3   = Dulhe Raja
| extra3  = Vinod Rathod, Anuradha Paudwal
| length3 = 5:17

| title4  = Kahan Raja Bhoj
| extra4  = Sonu Nigam, Vinod Rathod
| length4 = 8:09

| title5  = Kya Lagti Hai Hai Rabba
| extra5  = Vinod Rathod
| length5 = 5:35

| title6  = Ladka Deewana Lage
| extra6  = Udit Narayan, Anuradha Paudwal
| length6 = 6:00

| title7  = Nighahen Kyon Churaati Hai
| extra7  = Udit Narayan, Ram Shankar
| length7 = 6:16
}}

==Award==
 
|-
| 1999
| Johnny Lever
| Filmfare Award for Best Performance in a Comic Role
|  
|}

==Reception== a film of the same name by director Harmesh Maholtra. 

==Trivia== Nagina (1986), Kismat (1995) etc.

2. Johnny Lever received the Filmfare Best Comedian Award in 1999 for the movie.

3. A song from the movie- Ankhiyon Se Goli Maare, would later become the inspiration for a film of the same name by director Harmesh Maholtra.

==References==
 

==External links==
* 

 
 
 
 

 