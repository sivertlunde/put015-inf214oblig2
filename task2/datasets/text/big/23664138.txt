Adultery Tree
{{Infobox film
| name           = Adultery Tree
| image          = Adultery Tree.jpg
| caption        = Theatrical poster for Adultery Tree (1985)
| film name      = {{Film name
| hangul         =  
| hanja          =  
| rr             = Janyeomok
| mr             = Chanyŏmok}}
| director       = Jung Jin-woo 
| producer       = Jung Jin-woo
| writer         = Chi Sang-hak
| starring       = Won Mi-kyung
| music          = Han Sang-ki
| cinematography = Lee Seong-chun
| editing        = Hyeon Dong-chun
| distributor    = Woo Jin Films Co., Ltd.
| released       =  
| runtime        = 125 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Adultery Tree ( ) is a 1985 South Korean film directed by Jung Jin-woo. It was awarded Best Film at the Grand Bell Awards ceremony.     

==Synopsis==
In this historical drama, the matriarch of an aristocratic family makes the lady Yeon-jis life miserable due to her inability to bear children. The matriarch brings a surrogate mother into the family, and orders Yeon-ji to kill herself. Yeon-ji hangs herself at the hanging tree used for women who have engaged in adultery.   

==Cast==
* Won Mi-kyung 
* Kim Yong-seon
* Park Jung-ja
* Kim Hee-ra
* Jeon Moo-song
* Choe Byeong-geun
* Hong Seong-min
* Lim Hae-lim
* Choe Jae-ho
* Park Jong-sel

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 


 