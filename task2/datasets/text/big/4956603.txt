Priyamaana Thozhi
{{Infobox film
| name = Priyamaana Thozhi
| image = PriyamanaThozhi.jpg
| director = Vikraman
| producer = M. Saravanan (film producer)|M. Saravanan M. Balasubramanian M. S. Guhan B. Gurunath
| writer = Vikraman Madhavan Jyothika Sreedevi Vijayakumar Vineeth
| cinematography = S. Saravanan
| editing = V. Jaishankar
| studio = AVM Studios
| music = S. A. Rajkumar
| released = 11 July 2003
| runtime =
| country = India
| language = Tamil
}}
 Tamil film Livingston and Manivannan playing other supporting roles. The films music is composed by S. A. Rajkumar, while S. Saravanan handled the camera. The film opened simultaneously alongside the Telugu version of the film, Vasantham, in July 2003 to an average response critically and commercially.

==Plot==
Ashok (R. Madhavan|Madhavan) and Julie (Sridevi Vijaykumar) have been friends ever since childhood and they dont share any love interest. Ashok falls in love with Nandini (Jyothika), a rich girl with whom he marries, and although Julies closeness to Ashok initially irritates Nandini, she subsequently accepts it. Julie falls in love with a guy called Michael DSouza (Vineeth), a cricketer whos hoping for a place in the Indian cricket team and Ashok happens to be his main opponent.

When Ashok gets selected, Michaels father strikes a deal with Ashok that the marriage between his son and Julie will only take place if Ashok steps down and let Michael substitute him and for Ashok to cut friendship with Julie so he doesnt interfere in Michaels and Julies way, to which Ashok agrees. When Michael learns of this at the end, he restores the severed ties between himself and Ashok, and the movie ends upon a happy note.

==Cast== Madhavan as Ashok
* Jyothika as Nandini Ashok
* Sreedevi Vijayakumar as Julie
* Vineeth as Michael DSouza
* Manivannan 
* R. Sundarrajan Livingston as Kumar
* Nirosha 
* Ramesh Khanna as Kaka Ramesh
* Dharini
* Y. Vijaya as Nandinis Mother
* Kumaresan
* Subhalekha Sudhakar
* Madhan Bob
* R. Neelakantan
* Krishnamachari Srikkanth as himself (guest appearance)

==Production==
AVM Productions had agreed a deal with Vikraman to make a film for them in the 1990s but date clashes meant that they were unable to work together until 2003.  Madhavan signed the film in September 2002, and it became the first time the actor had worked with the producers or the director.  Scenes for the film were shot in Ooty, Tamil Nadu with producer Guhan often cooking for the team.
The songs were shot abroad, with places filmed where the team filmed including Australia, New Zealand and Switzerland.  
 Kaveri reprised the roles of Madhavan, Jyothika and Sridevi respectively; while V. V. S. Laxman appeared in the film in a cameo instead of Srikkanth.   The film was remade in Kannada as Hoo (film)|Hoo with V. Ravichandran.

==Release== Filmfare Best Tamil Supporting Actress Award in 2004 for her performance as Julie.

Priyamaana Thozhi became a commercial failure, the first of his career for director Vikraman. He has since struggled to recapture the success that he achieved before this film with his subsequent ventures.  

==Soundtrack==
{{Infobox album|  
  Name        = Priyamaana Thozhi
|  Type        = Soundtrack
|  Artist      = S. A. Rajkumar
|  Cover       =
|  Released    = 2003
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       = AVM Audio
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Priyamaana Thozhi (1999)
|  Next album  = 
}}
The soundtrack of the film was composed by S. A. Rajkumar. The lyrics for the film were written by Pa. Vijay & Kalaikumar.  The song "Maankuttiye was alledly lifted from Hindi song "Saawan Ka Mahina" from Hindi film Milan (1967 film)|Milan (1967).  Rajkumar reused "Katre Poongatre" as "Chanda Oh" in Kannada film Mallikarjuna (film)|Mallikarjuna. The song "Rojakkale" was reused from Rajkumars own Kannada song "O Preethige" which he had composed for Jodi (2001 film)|Jodi (2001).

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Maankutty Maankutty  Sujatha
| length1         = 
| title2          = Penne Neyum Pennaa 
| extra2          = Unni Menon, Kalpana
| length2         = 
| title3          = Kattre Poongattre 
| extra3          = K. S. Chithra 
| length3         = 
| title4          = Vaanam Enna Vaanam 
| extra4          = Hariharan
| length4         = 
| title5          = Maankutty Maankutty 
| extra5          = Sadhana Sargam
| length5         = 
| title6          = Rojakale 
| extra6          = Mahalakshmi Iyer 
| length6         = 
| title7          = Kattre Poongattre (2)
| extra7          = K. S. Chithra
| length7         = 
| title8          = Kattre Poongattre 
| extra8          = Hariharan
| length8         = 
}}

==References==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 