Questions à la terre natale
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Questions à la terre natale
| image          = 
| caption        = 
| director       = Samba Felix N’diaye
| producer       = La Huit Productions, Médiatik, Arte France
| writer         = 
| starring       = 
| distributor    = 
| released       = 2006
| runtime        = 52 minutes
| country        = France Senegal
| language       = 
| budget         = 
| gross          = 
| screenplay     = Samba Felix N’diaye
| cinematography = Raphaël Popelier
| editing        = Mariette Levy-Novion
| music          = El Hadj Ndiaye
}}

Questions à la terre natale is a 2006 documentary film.

== Synopsis ==
Where is Africa going to? After forty years of regained independence, the African continent is in turmoil and is struggling with its contradictions, between its desire to be liberated from its colonial supervision and its dependence on the goodwill and the charity of rich countries. This film is a forum through which Africans themselves are asked about the way they see their continent and its leaders.

== References ==
 

 
 
 
 
 
 
 
 


 
 