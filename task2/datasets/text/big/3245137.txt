Deewane Huye Paagal
 
 
{{Infobox film
| name           = Deewane Huye Paagal
| image          = Deewane_Huye_Paagal_movie poster.jpg
| caption        = Theatrical release poster
| director       = Vikram Bhatt
| producer       = Firoz A. Nadiadwala
| narrator       = Vivek Oberoi
| story          = 
| screenplay     = Kiran Kotrial
| starring       = Akshay Kumar Shahid Kapoor Sunil Shetty Rimi Sen Paresh Rawal
| music          = Anu Malik
| cinematography = Praveen Bhatt
| editing        = Diwakar P Bhonsle Virendra Gharse
| distributor    = Base Industries Group
| released       = 25 November 2005
| runtime        = 157 mins
| language       = Hindi
| country        = India
}}
 2005 Bollywood romance action comedy film directed by Vikram Bhatt, and produced by Firoz A. Nadiadwala. The film features Akshay Kumar, Sunil Shetty, Shahid Kapoor, Rimi Sen, Paresh Rawal along with many others. Aftab Shivdasani and Vivek Oberoi make guest appearances.

The film was released on 25 November 2005. It turned out to be a box office flop. Its plot was lifted from the 1998 American comedy film Theres Something About Mary. 

==Plot==
 

Karan(Shahid Kapoor) is a shy and nerdy canteen boy and college student, running a café with his friend Murugan (Johnny Lever), and Tanya (Rimi Sen) is a student in the same college. She is the most popular girl on campus. Karan is in love with Tanya but is too shy to tell her. After he musters some courage and rescues her step brother from some goons, he catches her attention and his eventually able to invite her for a fake birthday party. On her way there, Tanya witnesses the murder of a scientist, Khurana (Om Puri) by his evil twin brother, the underworld don Mehboob, and as a result is forced to flee the country to save her own life, since the law had been bought over by the very same killers. The scientist had stored a secret code in a stuffed toy, a parrot, now being sought by Mehboob, and he had been able to hide the toy in Tanyas car before being killed. Unawares, Tanya leaves the toy at home before fleeing the country, and the toy is later picked up by Karan, who himself is not aware of Tanyas predicament.

Three years later, still in love with Tanya, but depressed from her sudden and complete disappearance, Karan stumbles across old schoolmates of hers at a café, and discovers from them that she had been living in Dubai all this time. Karan coaxes Murugan to help him track down Tanya in Dubai. They enlist the aid of a local hustler and bounty hunter, Rocky (Akshay Kumar).

Rocky travels to Dubai, where his friend Babloo (Vijay Raaz) has already tracked down Tanya, who has morphed into a famous singer/performer named Natasha. Rocky and Babloo stake Natashas villa to make sure they are on the right track, but as soon as Rocky spots Natasha, its love at first sight. While he has Natasha under surveillance, Karan and Murugan arrive at the Dubai airport from India. Faced with a potential competitor to woo Natasha, Rocky attempts to eliminate Karan from the picture. Tanya, he informs Karan, is not the same girl he knew from three years ago, apparently she has been married twice since coming here and both her husbands died one by one and now she has three pairs of twins (six children) and is also dating an underworld don, with whom she has a further illegitimate child! Karan is heartbroken to hear all this, and decides to go back to Mumbai, and is about to board his flight, when at the last moment, he spots a picture of Tanya (as Natasha) on a club advertisement printed across an airport Taxi. He is overjoyed, and assumes that Rocky made a mistake, and ended up tracking down someone else named Tanya, not being aware that Rocky is in fact trying to deceive him.

Things are about to get more complicated, since not only has Rocky to contend with Karan, there are two other people already in her life, trying to catch her in their web of deceit. Local plumber, Sanju Malvani (Sunil Shetty) has been currying favours for Natasha by pretending to be a paraplegic on crutches. Every time Natasha meets someone she likes, Sanju eliminates the competitor using a unique trick. He edits and prints out a fake newspaper featuring an article showing that person as a crook, and comes over to Natasha and shares "the news" with her while acting all shocked himself, thus earning her admiration.

On the other hand, there is Tanyas physically disabled live-in friend Tommy (Paresh Rawal), who also has a soft spot for her. Having heard of the death of Tanyas disabled brother a year ago, Tommy has since faked being run over in a car accident and now acts like he himself is disabled and mentally challenged, thus managing to stay close to Natasha by seamlessly filling in the void left behind by her deceased brother. Nonetheless, Rocky begins his attempted seduction of Natasha. Having wired the villa, and thus overheard what Natasha craves in a man, Rocky shows up at the local shopping mall, pretending to be an architect. Natasha, who is shopping at the mall, is ambushed in the parking lot by a bunch of thugs (who it later transpires are simply mugs hired by Rocky himself). Rocky intervenes, and after seemingly beating the living daylights out of the supposed thugs (the scene is similar to Jean-Claude Van Dammes introductory action sequence from Hard Target (1993), introduces himself to Natasha, and proceeds to show-off himself as a charming professional with a heart of gold. Somewhat awed, Natasha offers Rocky an invitation for tea later in the evening, which he accepts.

When Rocky arrives at her villa later in the evening, he is confronted by several obstacles, Natashas sister Sweety (Supriya Pilgaonkar), the jealous Tommy and the familys pet dog. Rocky is easily able to charm Sweety, but has to drug the dog, who dies due to an overdose while the sisters are making Rocky a drink in the kitchen. Crisis is avoided when Rocky is able to revive the dog by electrocution using wiring from a lamp in the living room. The parties then quickly depart to the grand opening of Natashas latest album at a social event, where Rocky now comes face to face with Sanju, who puts Rocky on the spot right away, as he himself is pretending to be an architect!

With Natasha and company under surveillance, Rocky now intercepts Sanju trying to discredit him at Natashas home. Later, when the shocked Natasha confronts Rocky about him being an alleged impostor, Rocky is able to fast-talk his way out of the situation and convince the gullible Natasha that he is indeed not an architect, but a Captain on a ship, something his mother does not approve of, thus the ruse. Tommy, however behaves in a somewhat jealous manner and puts Rocky in a confusing situation, involving him jumping on Rockys back and then accusing the latter of having twisted and bitten his arm. This arouses Rockys suspicions about his supposed "condition".

The next day, using a food bill from a bar (which dropped out of Tommys pocket during his supposed accusation of Rocky) that aroused his suspicions, Rocky and Babloo track down Tommy at a local disco where, unaware that he is being followed, Tommy shows up every week, for a little fun and dance away from his daily existence as a fake disabled victim. Rocky confronts Tommy while he is on the disco floor and threatens to expose him in front of Natasha. At the end, Rocky lets Tommy go, on the condition that the latter will no longer attempt to get in his way of seducing Natasha.

The scene now shifts to the harbour, where Rocky, resplendently dressed as an immaculate ships captain, has invited Natasha over for a cruise. While he is coddling her on the harbour front, Natasha spots Karan and Murugan sitting at the grounds. Unaware that Karan is in Dubai searching for her, she is overjoyed to see an old friend from her past. She introduces Karan to Rocky, unaware that they know each other quite well already! Murugan has to be held back by Karan, as he wants to knock Rockys teeth out, having just realised that all this time Rocky was simply giving them the run-around to get his own hands on Natasha/Tanya. Karan, seeing Rocky and Natasha/Tanya all smiles and happy, simply wants to leave them alone out of his love for Tanya. He just wants her to be happy, even if it is with Rocky and not himself, much to Murugans frustration. While he tries to leave the scene, pretending that he and Murugan have a flight to catch, Natasha/Tanya convinces Karan to stay one more night in Dubai, and come over for dinner later in the day.

At the dinner party, which takes place in a club, both Karan and Rocky are shown staging elaborate dance sequences to impress Natsha/Tanya. Sanju shows up later at the party, and pulls Natasha aside to reveal his latest newspaper creation, showing Rocky as an international crook, murderer and serial killer wanted by many crime agencies the world over, whose M.O, as Sanju explains is to trap and seduce innocent girls like Natasha and then destroy their lives. Rocky overhears the conversation and while Natasha is occupied elsewhere, chases Sanju outside in the club parking lot. Sanju, fearing for his life, drops the pre tense of being crippled, throws his crutches on the ground, and runs for dear life. Later, Rocky, Sanju and Tommy are seen sitting in a bar nearby after a confrontation, exposing each other as fraudsters and having a drink together, cursing their luck at the emergence of Karan as the new love in Natashaa life.

With Rocky having bugged Natasha again, the trio now listen to Natashas views about them and find out, in a hilarious manner, that all is not what it seems, since she views Sanju as a brother like figure, Tommy as something similar to her deceased brother, and Rocky as a fraud and murderer. They then go to Natashas house, where she is having coffee with Karan. Rocky uses a gun to shoot some specially medicated pills into the drawing room to be consumed by the dog. One of them is consumed by the dog, but the other is consumed by Sweety. The effetcs are hilarious as Sweety suddenly becomes hyperactive and so does the dog. Confusion ensues as the dog attacks Karan and he ends up throwing it out the window.

The next day, Karan goes to Natashas house to propose to her and finds that she has been sent an anonymous letter, informing her that he indeed had hired Rocky to follow her. In a spate of anger, she sends him out of the house. Later, when he goes to Rockys house to confront him about this, he finds Sanju and Tommy there as well, who also reveal themselves to have been conning Natasha. Further, Sanju reveals that he had written the anonymous letter to Natasha and goes on to reveal his trick with the newspapers, disgusting Karan. Meanwhile, Mehboob and his family, including Sunny ( Mehboobs illegitimate, adopted, half South Indian, half Punjabi son, who was the result of an affair Meboobs late wife had had with their South Indian neighbour. He had been a student at the same college as Karan and Natasha and had been obsessed with marrying her ever since. Mehboob had used this obsession to find out who Natasha was and had been able to track her down) arrive at Natashas house and interrogate her about the stuffed toy. Sunny then kidnaps her. Sweety informs Sanju about this and he along with Rocky, Karan and Tommy follow them after having gotten directions from Murugan, who happened to see them from a taxi. The chase takes them to the middle of a desert, where after some confusion, it is revealed that Khurana had stored the code of a vault in the stuffed toy. The vault had carried a secret solution, two drops of which could decrease a persons age by 25 years. The night Khurana had died, he had found that his brother wanted to use the formula for his own consumption and had tried to escape in Natashas car as she had been passing by. He had slipped the toy into a package ( a birthday present she had been carrying for Karans fake birthday dinner, thus explaining how he had the toy) she had had with her at that time. Khurana used the toy to counteract his forgetfulness. Natasha sent him out of the car at first, but, undergoing a change of heart, had gone back later just in time to witness Mehboobs son Baljeet kill Khurana.

Rocky intervenes and cleverly destroys the toy after having it speak the code into Natashas ear, making sure she cannot be killed, the code being in her mind now. A gunfight ensues, involving bikes and quad bikes during which Natasha discovers Tommy and Sanjus deceit, much to her shock and disgust. After the commotion, Baljeet, wanting the solution all for himself, to sell to a Chinese buyer and make millions, kidnaps Natasha. Rocky and Sanju follow them on bikes and finally manage to catch up with them. After beating up all the men, they take Natasha to her house where a further argument ensues as to who should be more worthy of Natasha. The argument ends when Karan arrives with Raj Sinha (Aftab Shivdasani) and tells Natasha about Sanjus newspaper scams (which the latter confesses), one of which had exposed Raj as a drug dealer and addict earlier. Raj and Natasha are reunited. Wishing them luck, Karan leaves in sadness after the others are left cursing.

Natasha stops Karan as he is leaving and at first, stalls him by saying that he had forgotten his car keys. She then invites him to the wedding, to which he refuses. She says that it would seem strange if the bridegroom is not present at his own wedding. Karan realises what she means and she professes her love for him as well. This tearful union is once again being witnessed by a grumbling Rocky, Sanju and Tommy along with Sunny. Rocky impatiently tells them to stop cursing and asks them to admit to the fact that they were never worthy enough for her. He then turns to Babloo and suggests, in a whisper, that they try to find out where Karan and Tanya are going for their honeymoon and they eagerly put on a pair of earphones. The film ends as the camera pans to Tanya and Karan walking away towards the sunset, hand in hand.

At the end, the narrator says that after all the effort he had undertaken to acquire the solution, Mehboob had become too overexcited and had consumed the whole quantity instead of two drops. He had ended up becoming a baby and Sunny unwittingly became his father!!!! However he breaks the fourth wall finally, as he requests the audience not to forget him, given that he had narrated the story in its entirety.

== Cast ==
* Akshay Kumar as Rocky Hirandani
* Sunil Shetty as Sanju Malvani 
* Shahid Kapoor as Karan
* Rimi Sen as Tanya Mulchandani / Natasha	
* Paresh Rawal as Tommy	
* Vijay Raaz as Babloo (Rockys assistant)
* Johnny Lever as Murugan
* Om Puri as Scientist Khurana / Mehboob
* Suresh Menon as Veerappan Sunny Khurana
* Baljeet Singh as Baljeet Khurana
* Supriya Pilgaonkar as Sweety Aunty
* Asrani as the Blind man
* Leena as Kavita
* Vivek Oberoi as the Narrator (Sutradhar)	
* Aftab Shivdasani as Raj Sinha (Special appearance)
* Rakesh Bedi as Gullu Mulchandani
* Snehal Dabi

== Music ==
Music of the film was composed by veteran Anu Malik. The film has around five-six songs. They are listed below.
*Meri Jaane Jigar – Anu Malik
*Chakle Chakle – Anu Malik Shaan
*Tu Shaan
*Maar Sutiya – Anu Malik

==Reception==
The film received favourable reviews upon release. However, it didnt manage to succeed at the box office. It grossed Rs. 160&nbsp;million fully and was given the final verdict of flop.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 