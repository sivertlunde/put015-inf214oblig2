Melinda and Melinda
 
{{Infobox film
| name           = Melinda and Melinda
| image          = Melinda and melinda poster.jpg
| caption        =
| director       = Woody Allen
| writer         = Woody Allen
| starring       = Radha Mitchell Chiwetel Ejiofor Will Ferrell Jonny Lee Miller Amanda Peet Chloë Sevigny Wallace Shawn
| producer       = Letty Aronson
| music          = Johann Sebastian Bach Johannes Brahms Igor Stravinsky Béla Bartók
| cinematography = Vilmos Zsigmond
| editing        = Alisa Lepselter
| distributor    = Fox Searchlight Pictures
| budget         =
| released       = March 18, 2005
| runtime        = 99 minutes
| language       = English
| gross = $20,085,825
}} written and directed by Woody Allen. It was premiered at the San Sebastian International Film Festival. The film is set in Manhattan and stars Radha Mitchell as the protagonist Melinda, in two storylines; one comic, one tragedy|tragic. The film was given a limited release in the United States on March 18, 2005.

==Plot==
The premise of the film is stated by a group of four writers conversing over dinner at the beginning of the film. The question arises: Is life naturally comic or tragic? One of the four proposes a simple story (a distraught woman knocks on a door and disrupts a dinner party) and the two prominent playwrights in the group begin telling their versions of this story, one being comic and one tragic.

==Cast==
Woody Allen said in Conversations with Woody Allen that he wanted to cast Winona Ryder in the title role.  He had to replace her with Radha Mitchell because no one would insure Ryder due to her arrest for shoplifting – this would have made it impossible to obtain a film completion bond. Allen stated he was sad because he had written the part for Ryder after working with her on Celebrity (film)|Celebrity. In the same interview, he also claimed to have intended Ferrells part for Robert Downey, Jr., but, again, insurance got in the way due to Downeys history of arrests and drug abuse.

Radha Mitchell plays Melinda in both versions. Chloë Sevigny, Jonny Lee Miller, and Chiwetel Ejiofor star with her in the tragedy, while Will Ferrell and Amanda Peet star with her in the comedy. Steve Carell has a small part as Ferrells friend.
 Brooke Smith as Cassie. All three had appeared in the 1994 film Vanya on 42nd Street, directed by Louis Malle.

The cast includes:
 
*Radha Mitchell as Melinda
*Chloë Sevigny as Laurel
*Jonny Lee Miller as Lee
*Will Ferrell as Hobie
*Amanda Peet as Susan
*Chiwetel Ejiofor as Ellis
*Wallace Shawn as Sy
*Josh Brolin as Greg
*Vinessa Shaw as Stacey
*Steve Carell as Walt
*Arija Bareikis as Sally
*Matt Servitto as Jack
*Zak Orth as Peter Brooke Smith as Cassie
*Daniel Sunjata as Billy
*Larry Pine as Max
*Andy Borowitz as Doug
 

==Critical reception==
The film received mostly mixed reviews from critics. The review aggregator Rotten Tomatoes reported that the film received 51% positive reviews, based on 144 reviews.  Metacritic reported the film had an average score of 54 out of 100, based on 40 reviews.  Leonard Maltin gives the film two stars, calling it "meandering,” with "echoes... of earlier, better Allen movies". 

==Box office==
Melinda and Melinda opened on March 18, 2005, in one New York City cinema, where it grossed $74,238 in its first three days, the 21st highest limited release opening in Hollywood history.   In weekend two, it expanded to 95 theatres to gross $740,618, seeing its per screen average nosedive to $7,795.

Overseas, it grossed an additional $16,259,545, bringing its worldwide total to $20,085,825.

==Soundtrack==
 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
*  (Unofficial Woodypedia and blog)

 
 

 
 
 
 
 
 
 