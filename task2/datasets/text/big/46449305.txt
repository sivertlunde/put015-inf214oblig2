Puppylove
{{Infobox film
| name           = Puppylove
| image          =
| alt            =
| caption        =
| film name      =
| director       = Delphine Lehericey
| producer       = {{plainlist|
* Sebastien Delloye
* David Grumbach
}}
| writer         = {{plainlist|
* Delphine Lehericey
* Martin Coiffier
}}
| starring       = {{plainlist|
* Solene Rigot
* Audrey Bastien
* Vincent Pérez
}}
| music          = Soldout
| cinematography = Sebastien Godefroy
| editing        = Ewin Ryckaert
| studio         = {{plainlist|
* Entre Chien et Loup
* Liaison Cinématographique
* Box Productions
* Juliette Films
}}
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = {{plainlist|
* Belgium
* France
* Luxembourg
* Switzerland
}}
| language       = French
| budget         =
| gross          =
}}
Puppylove is a 2013  coming-of-age film directed by Delphine Lehericey, written by Lehericey and Martin Coiffier, and starring Solene Rigot, Audrey Bastien, and Vincent Pérez.  It is an international co-production of Belgium, France, Luxembourg, and Switzerland.   Rigot plays a 14-year-old girl who, after she meets her new neighbor, played by Bastien, begins to explore her sexuality.  The film premiered at the San Sebastián International Film Festival and was released 7 May 2014 in Belgium.  It won a Magritte Award for Best Original Score and was nominated for Best First Feature.

== Plot ==
Diane, a 14-year-old girl who lives with her younger brother Marc and her father Christian, is eager to explore her sexuality.  When she visits her friend Paul, the two strip naked and get into bed together.  As Paul kisses her and calls her beautiful, she tells him that she would prefer not to talk.  He apologizes, and the two try to get comfortable in various sexual positions.  Eventually, they are overcome by awkwardness and decide not to have sex.  Paul takes her back home.  There, her father catches her watching hardcore pornography, and she ignores his demands that she turn it off.  Exasperated, he leaves.

During school, Dianes friends discuss whether losing ones virginity is painful. Diane meets with Paul again, and they briefly hang out together at his house.  He requests that she stay, but she leaves for her own.  Through her window, she sees her new neighbor, Julia, as the girls father berates her.  Annoyed, Julia pulls her curtains shut.  As Diane and her family eat dinner, Julia introduces herself and requests help finding a local piano teacher.  Christian and Diane promise to help.

At school, Diane approaches Julia with the name of a piano teacher, but Julia says that only said that to meet Diane.  The two strike a friendship, and Diane invites Julia to a party.  Julia tells her parents that she is sleeping over at Dianes house, but Christian expresses no concerns with Dianes attendance.  Diane brushes off Paul and leaves to find Julia, who is about to go on a car ride with two boys.  Julia invites Diane to join her on condition that Diane does not attempt to stop Julia from doing anything.  As Julia and a boy have sex, Diane and the other boy exit the car.  After the second boy becomes aggressive and sexually harasses Diane, the girls walk home.

Diane is surprised to find her father has a woman at the house, and the noise of their sex keeps her awake at night.  In the morning, she berates her father for his irresponsible behavior, as he has not woken up Marc and gotten him prepared for school.  Diane is hurt when she sees Paul and Julia together at school.  After football practice, Diane wanders into the male showers completely naked, and the coach calls her father to pick her up.  As they drive home, Diane asks Christian whether he is sexually attracted to her, and she becomes annoyed as Christian fumbles to find an appropriate answer.

As Julia and Diane grow closer, they begin to flirt with each other and kiss.  Their families also become more friendly, and Diane goes on a vacation with Julia and her parents.  There, Julia seduces a bartender.  Although Diane initially refuses to join the two in a threesome, she later knocks on the door and watches the two have sex.  Julia encourages Diane to have casual sex with a man at a club, but after he demands that she perform oral sex on him, Diane insists that she and Julia leave.  Later, the two girls go on a trip on Christian and Marc.  Julia flirts with Christian despite Dianes discomfort.  The girls find a local man and engage in a threesome.

Julia flashes Christian, and as he stares at Julia, Diane chastises him.  After Christian becomes drunk that night, Julia begins kissing him and undressing.  Christian weakly pushes her away and tells her that it is a bad idea, but she continues her attempts to seduce him.  Christian eventually gives in, and the two have sex outside against a tree.  As Diane looks for her friend, she is disgusted to see the two having sex, and she slaps her father.  Christian apologizes to her, and as she storms off, he drives after her.  After he picks her up again, Diane asks Julia whether she would run away with her if she asked.  Disinterested, Julia says that she would not, and Diane walks into the traffic at a busy road, much to the horror of the others.  The film ends as Diane stands on the other side of the road and smiles.

== Cast ==
* Solene Rigot as Diane
* Audrey Bastien as Julia
* Vincent Pérez as Christian
* Vadim Goldberg as Marc
* Joel Basman as Paul
* Thomas Coumans as the bartender
* Jan Hammenecker as Yann
* Valérie Bodson as Catherine

== Soundtrack ==
The soundtrack was performed by the band Soldout. 

== Release ==
Puppylove premiered on 21 September 2013 at the San Sebastián International Film Festival   and was released 7 May 2014 in Belgium. 

== Reception ==
Jonathan Holland of The Hollywood Reporter wrote, "More a forum for debate than fully achieved piece of work, this intriguing, misguided film is unsettling in ways it may not itself understand." 

=== Awards ===
{| class="wikitable"
! Organization !! Award !! Result !! Ref
|-
| rowspan=2|  5th Magritte Awards || Best Original Score ||   ||  
|-
| Best First Feature ||   ||  
|}

== References ==
 

== Further reading ==
*  
*  
*  

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 