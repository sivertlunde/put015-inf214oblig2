The Swan and the Wanderer
{{Infobox film
| name           = The Swan and the Wanderer
| image          = 
| alt            =  
| caption        = Promotional movie poster for the film
| director       = Timo Koivusalo
| producer       = Timo Koivusalo
| writer         = Timo Koivusalo Juha Numminen
| starring       = Tapio Liinoja Martti Suosalo Heikki Nousiainen
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 102 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}}

The Swan and the Wanderer ( ) is a Finnish film telling the story of two very popular Finnish singer/songwriters, Tapio Rautavaara (Tapio Liinoja) and Reino Helismaa (Martti Suosalo), who worked together until their relationship got frictious for a long time. The film covers the years from 1949 to 1965, the date of Helismaas death.

The film also tells about the change in Finland during those years.

==Cast==
*Tapio Liinoja as Tapio Rautavaara
*Martti Suosalo as Reino Helismaa
*Heikki Nousiainen as Esa Pakarinen
*Ari Virta as Masa Niemi
*Matti Mäntylä as Toivo Kärki
*Tuija Piepponen as Siiri Angerkoski
*Raimo Grönberg as Olavi Virta

==External links==
* 
* 

 
 
 
 

 