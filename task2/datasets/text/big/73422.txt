The Day the Earth Stood Still
 
{{Infobox film
| name           = The Day the Earth Stood Still
| image          = Day the Earth Stood Still 1951.jpg Theatrical release poster
| director       = Robert Wise
| producer       = Julian Blaustein
| screenplay     = Edmund H. North
| based on       =  
| starring       = Michael Rennie Patricia Neal Hugh Marlowe Sam Jaffe
| music          = Bernard Herrmann
| cinematography = Leo Tover
| editing        = William H. Reynolds
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = $995,000 Solomon 1989, p. 246. 
| gross          = $1,850,000   
}}
 
 Harry Bates.  The notable score was composed by Bernard Herrmann. Gianos 1998 p. 23. 
 alien visitor named Klaatu comes to Earth, accompanied by a powerful eight-foot tall robot, Gort, to deliver an important message that will affect the entire human race. 
 

==Plot==
When a flying saucer lands in Washington, D.C., the U.S. Army quickly encircles the spaceship. A humanoid (Michael Rennie) emerges, announcing that he has come in peace. When he advances, however, he unexpectedly opens a small cylindrical device and is shot by a nervous soldier. A tall robot emerges from the saucer and disintegrates all of the soldiers weapons using an energy ray. The alien orders Gort (The Day the Earth Stood Still)|Gort, the robot, to stop, then explains that the now-broken device was simply a gift for the President of the United States|President, which would have enabled him "to study life on the other planets".

The wounded alien is taken to Walter Reed Hospital, where he reveals his name: Klaatu. He uses a salve to quickly heal his wound. Meanwhile, the military attempts to enter the spaceship, but finds it impenetrable; Gort stands outside the ship, silent and unmoving.
 Frank Conroy), that he has come with a message that must be revealed to all the worlds leaders simultaneously. Harley tells him that such a meeting in the current political climate is impossible. Klaatu suggests that he be allowed to go among humans to better understand their "unreasoning suspicions and attitudes." Harley rejects the proposal and leaves Klaatu under guard.
 dry cleaners Billy Gray). The next morning, Klaatu listens to his fellow boarders speculations about the aliens purpose.

While Helen and her boyfriend Tom Stephens (Hugh Marlowe) go out, Klaatu babysits Bobby. The boy takes Klaatu on a tour of the city, including a visit to his fathers grave in Arlington National Cemetery; Klaatu learns that most of those buried there were killed in wars. The two view the heavily guarded saucer and visit the Lincoln Memorial. Klaatu asks Bobby who the greatest person living is; Bobby suggests Professor Barnhardt (Sam Jaffe), who lives in the city. Bobby takes Klaatu to Barnhardts home, but the professor is absent. Klaatu adds a mathematical equation to a problem on Barnhardts blackboard and leaves his contact information with the suspicious housekeeper.
 atomic power. Klaatu declares that, if his message goes unheeded, "Earth will be eliminated." Barnhardt agrees to gather scientists at Klaatus saucer, suggesting that Klaatu give a demonstration of his power. Klaatu returns to his spaceship the next evening, unaware that Bobby has followed him.  Bobby sees Gort knock out two guards and "Mr. Carpenter" enter the spaceship.

Bobby tells Helen and Tom what he saw, but they do not believe him until Tom takes a diamond found in Klaatus room to a jeweller, who informs him it is "unlike any other on Earth." Klaatu finds Helen at her workplace. They take an empty service elevator which stops precisely at noon. Klaatu reveals his true identity and asks for her help. He has neutralized all electricity everywhere except where human safety would be compromised, such as hospitals and aircraft in flight.

After the blackout ends, the manhunt for Klaatu intensifies. When Tom informs the military of his suspicions, Helen breaks up with him. Helen and Klaatu take a taxi to Barnhardts home. En route, he tells her that should anything happen to him, she must go to Gort and say these words: "Klaatu barada nikto". After being spotted, Klaatu makes a break for it, but is gunned-down. Helen then heads to the saucer. Gort awakens, disintegrates the two army night guards, and advances on her. When Helen utters the three words, the robot carries her into the spaceship, then retrieves Klaatus body. Gort revives Klaatu, but he explains to Helen that this is only temporary, that the power of life and death is "reserved for the Almighty Spirit".

Klaatu and Helen emerge from the saucer after Barnhardts scientists have assembled. Klaatu declares that the people of Earth have a choice. They can join the other planets in peace, but should they threaten to extend their violence into space, "this Earth of yours will be reduced to a burned-out cinder.   We will be waiting for your answer". Klaatu and Gort then depart in their spaceship.

==Cast==
  Klaatu
* Patricia Neal as Helen Benson Billy Gray as Bobby Benson
* Hugh Marlowe as Tom Stephens Sam Jaffe as Professor Jacob Barnhardt
* Frances Bavier as Mrs. Barley Gort
* Frank Conroy as Mr. Harley
* Tyler McVey as Brady (uncredited)
 

Cast notes  Drew Pearson, and Gabriel Heatter, appeared and/or were heard as themselves in cameo roles.

Spencer Tracy and Claude Rains were originally considered for the part of Klaatu.   IMDb, 1995. Retrieved: 1 February 2015.  

==Metaphors== censor installed by the Motion Picture Association of America at the Twentieth Century Fox studios, balked at the portrayal of Klaatus resurrection and limitless power.   At the behest of the Motion Picture Association of America|MPAA, a line was inserted into the film; when Helen asks Klaatu whether Gort has unlimited power over life and death, Klaatu explains that he has only been revived temporarily and "that power is reserved to the Almighty Spirit."  Shermer 2001, pp. 74–75. 
Of the elements that he added to Klaatus character, screenwriter Edmund North said, "It was my private little joke. I never discussed this angle with Blaustein or Wise because I didnt want it expressed. I had originally hoped that the Christ comparison would be subliminal." Matthews 2007, p. 54. 

That the question even came up in an interview is proof enough that such comparisons did not remain subliminal, but they are subtle enough that it is not immediately obvious to all viewers which elements were intended to compare Klaatu to Christ. Holloway and Beck 2005, p. 135.  He was sent to Earth to provide humans with another chance. When Klaatu escapes from the hospital, he steals the clothing of a "Maj. Carpenter," carpentry being the profession Jesus learned from his father Joseph. He presents himself as John Carpenter, the same initials as Jesus Christ. His message is misunderstood, and he is killed. At the end of the film, Klaatu rises from the dead and ascends into the night sky. Other parallels include his apprehension by authorities at night, his befriending of children, his having wisdom and knowledge far beyond any human being, and people being given a sign of his power. 

==Production==
===Development===
Producer Julian Blaustein set out to make a film under the working titles of Farewell to the Master and Journey to the World that illustrated the fear and suspicion that characterized the early Cold War and Atomic Age. He reviewed more than 200 science fiction short stories and novels in search of a storyline that could be used, since this film genre was well suited for a metaphorical discussion of such grave issues. Studio head Darryl F. Zanuck gave the go-ahead for this project, and Blaustein hired Edmund North to write the screenplay based on elements from Harry Batess 1940 short story "Farewell to the Master". The revised final screenplay was completed on February 21, 1951. Science fiction writer Raymond F. Jones worked as an uncredited adviser. 

===Pre-production===
The set was designed by Thomas Little and Claude Carpenter. They collaborated with the noted architect Frank Lloyd Wright for the design of the spacecraft. Paul Laffoley has suggested that the futuristic interior was inspired by Wrights Johnson Wax Headquarters, completed in 1936. Laffoley quotes Wright and his attempt in designing the exterior: "...&nbsp;to imitate an experimental substance that I have heard about which acts like living tissue. If cut, the rift would appear to heal like a wound, leaving a continuous surface with no scar." 

===Filming=== Century City, War Department refused participation in the movie based on a reading of the script. The military equipment shown came from the Virginia Army National Guard, although one of the tanks bears the "Brave Rifles" insignia of the 3rd Armored Cavalry Regiment, then stationed at Ft. Meade. 

The robot Gort, who serves Klaatu, was played by Lock Martin, who worked as an usher at Graumans Chinese Theater and stood seven feet tall. Not used to being in such a confining, heat-inducing costume, he worked carefully when wearing the two demanding, metallic-looking, stitched-up-the-front or -back, prop suits needed for creating the illusion on screen of a seamless Gort. Wise decided that Martins on-screen shooting time would be limited to half hour intervals, so Martin, with his generally weak constitution, would face no more than minor discomfort. These segments, in turn, were then edited together into films final print. Warren 1982, p. 621. 

In a commentary track on DVD, interviewed by fellow director Nicholas Meyer, the director Robert Wise stated that he wanted the film to appear as realistic and believable as possible, in order to drive home the motion pictures core message against armed conflict in the real world. Also mentioned in the DVDs documentary interview was the original title for the movie, "The Day the World Stops." Blaustein said his aim with the film was to promote a "strong United Nations." 

===Herrmanns score===
The music score was composed by   electronic instruments (played by Dr. Samuel Hoffman and Paul Shure), two Hammond organs, a large studio electric organ, three vibraphones, two glockenspiels, marimba, tam-tam, 2 bass drums, 3 sets of timpani, two pianos, celesta, two harps, 1 horn, three trumpets, three trombones, and four tubas.  Herrmanns notable advances in film scoring included Unison organs, tubas, piano, and bass drum, staggered tritone movement, and glissando in theremins, as well as exploitation of the dissonance between D and E-flat and experimentation with unusual overdubbing and tape-reversal techniques.
==Music and soundtrack==
 
{{Infobox album  
| Name = The Day the Earth Stood Still
| Type = Film
| Artist = Bernard Herrmann
| Cover = Dtest sndtrk.jpg
| Released = 1993
| Recorded = August, 1951
| Genre = Soundtracks, Film score
| Length = 35:33
| Label = 20th Century Fox
| Producer = Nick Redman
| Last album =
| This album = The Day the Earth Stood Still
| Next album =
}}
{{Album ratings
| rev1 = AllMusic
| rev1Score =    
}}
20th Century Fox later reused the Bernard Herrmann title theme in the original pilot episode of Irwin Allens 1965 TV series Lost in Space; the music was also used extensively in Allens Voyage to the Bottom of the Sea series episode, “The Indestructible Man.” Danny Elfman noted The Day the Earth Stood Still s score inspired his interest in film composing, and made him a fan of Herrmann. 

{{tracklist collapsed = headline = Track listing title1 = length1 = 0:12 title2 = length2 = 3:45 title3 = length3 = 0:24 title4 = length4 =  2:15 title5 = length5 = 2:23 title6 = length6 = 0:55 title7 = length7 = 1:04 title8 = length8 = 1:08 title9 = length9 = 1:27 title10 = length10 = 5:58 title11 = length11 = 4:32 title12 = length12 = 0:42 title13 = length13 = 5:11 title14 = length14 = 1:42 title15 = length15 = 1:38 title16 = length16 =  0:52 title17 = length17 =  0:32 title18 = length18 = 0:30
}}
==Reception==
===Critical response===
The Day the Earth Stood Still was well received by critics and is widely regarded as one of the best films of 1951.     

The Day the Earth Stood Still was moderately successful when released, accruing US$1,850,000 in film distributor|distributors domestic (U.S. and Canada) rentals, making it the years 52nd biggest earner. Gebert 1996, p. 156.    Variety (magazine)|Variety praised the films documentary style, and the Los Angeles Times praised its seriousness, though it also found "certain subversive elements."  Bosley Crowther of The New York Times called it "tepid entertainment."  

The Day the Earth Stood Still earned more plaudits overseas: the Hollywood Foreign Press Association gave the filmmakers a special Golden Globe Award for "promoting international understanding." Bernard Herrmanns score also received a nomination at the Golden Globes.  The French magazine Cahiers du cinéma was also impressed, with Pierre Kast calling it "almost literally stunning" and praising its "moral relativism." J. Hoberman|Hoberman, J.   The New York Times, September 31, 2008. 

The Day the Earth Stood Still is ranked seventh in  , which Clarke himself co-wrote. 

The Day the Earth Stood Still holds a 94% "Certified Fresh" rating on the review aggregate website Rotten Tomatoes. 

==Legacy==
In 1995 The Day the Earth Stood Still was selected for preservation in the United States National Film Registry as "culturally, historically, or aesthetically significant".   
 100 Years...100 100 Years...100 10 Top 100 Years...100 the tenth 100 Years...100 Klaatu in 100 Years...100 100 Years of Film Scores.  In 2004, the film was selected by The New York Times as one of The Best 1000 Movies Ever Made. 

Lou Cannon and Colin Powell believed the film inspired Ronald Reagan to discuss uniting against an alien invasion when meeting Mikhail Gorbachev in 1985. Two years later, Reagan told the United Nations, "I occasionally think how quickly our differences worldwide would vanish if we were facing an alien threat from outside this world". 

===Klaatu barada nikto===
 
Since the release of the film, the phrase "Klaatu barada nikto" has appeared repeatedly in fiction and in popular culture. The Robot Hall of Fame described it as "one of the most famous commands in science fiction", 
while Frederick S. Clarke of Cinefantastique called it "the most famous phrase ever spoken by an extraterrestrial". 

Edmund H. North, who wrote The Day the Earth Stood Still, also created the alien language used in the film, including the iconic phrase "Klaatu barada nikto". The official spelling for the phrase comes directly from the script (as shown in the image to the left) and provides insight as to its proper pronunciation.

No translation was given in the film. Philosophy professor Aeon J. Skoble speculates the famous phrase is a "safe-word" that is part of a fail-safe feature used during the diplomatic missions such as the one Klaatu and Gort make to Earth. With the use of the safe-word, Gorts deadly force can be deactivated in the event the robot is mistakenly triggered into a defensive posture. Skoble observes that the theme has evolved into a "staple of science fiction that the machines charged with protecting us from ourselves will misuse or abuse their power". 
In this interpretation the phrase apparently tells Gort that Klaatu considers escalation unnecessary.

Fantastic Films magazine explored the meaning of "Klaatu barada nikto" in a 1978 article titled The Language of Klaatu. The article, written by Tauna Le Marbe, who is listed as their "Alien Linguistics Editor", attempts to translate all the alien words Klaatu used throughout the film. Le Marbe, Tauna.    Fantastic Films, Issue 1, April 1978.  In the article the literal translation for Klaatu barada nikto was "Stop Barbarism (I have) death, bind" and the free translation was "I die, repair me, do not retaliate." 
 director of the film, related a story he had with Edmund North saying North told him, "Well, its just something I kind of cooked up. I thought it sounded good".  Billy Gray, who played Bobby Benson in the film, said that he thought that the message was coming from Klaatu and that, "barada nikto must mean ... save Earth".  producer Julian Blaustein, said North had to pass a street called Baroda every day going to work and said, "I think thats how that was born." 
Film historian Steven Jay Rubin recalled an interview he had with North when he asked the question, "What is the direct translation of Klaatu barada nikto, and Edmund North said to me Theres hope for earth, if the scientists can be reached." 
 Bender from Futurama, and a tagline from Battlestar Galactica.  Klaatu, Barada, and Nikto are the names of three minor characters among the personnel on Jabba The Hutts sail barge (featured in Return of the Jedi). Sam Raimi used the three words in The Army of Darkness, in a scene where a wise man tells Ash Williams| Ash, played by Bruce Campbell, to say "Klaatu barada nikto", so he could take the book safely.

===Professor Barnhardts blackboard problem===
The blackboard problem seen in the professors office is a real set of equations describing the three-body problem fundamental to space travel.  Hence this dialog:

Professor Barnhardt: "Have you tested this theory?"

Klaatu:   "Um.. I find it works well enough to get me from one planet to another."

==Adaptations==
The film was dramatized as a  . Retrieved: February 1, 2015. 

==See also==
 
*Culture during the Cold War

==References==
Notes
 

Citations
 

Bibliography
 
* Gebert, Michael. The Encyclopedia of Movie Awards (listing of Box Office (Domestic Rentals) for 1951, taken from Variety magazine). New York: St. Martins Paperbacks, 1996. ISBN 0-668-05308-9.
* Gianos, Phillip L. Politics and Politicians in American Film. Portsmouth, New Hampshire: Greenwood Publishing Group, 1998. ISBN 0-275-96071-4.
* Holloway, David and John Beck. American Visual Cultures. London: Continuum International Publishing Group, 2005. ISBN 0-8264-6485-8.
* Matthews, Melvin E.. Hostile Aliens, Hollywood and Todays News: 1950s Science Fiction Films and 9/11. New York: Algora Publishing, 2007. ISBN 0-87586-497-X.
* Shermer, Michael. The Borderlands of Science: Where Sense Meets Nonsense. New York: Oxford University Press, 2001.  ISBN 0-19-514326-4.
* Skoble, Aeon J. "Technology and Ethics in The Day the Earth Stood Still." in Sanders, Steven M. The Philosophy of Science Fiction Film. Lexington, Kentucky: University Press of Kentucky, 2007. ISBN 0-8131-2472-7.
* Solomon, Aubrey. Twentieth Century Fox: A Corporate and Financial History. Lanham, Maryland: Scarecrow Press, 1989. ISBN 978-0-8108-4244-1.
* Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol I: 1950 - 1957. Jefferson, North Carolina: McFarland & Company, 1982. ISBN 0-89950-032-3.
 

==External links==
 
 
*  
*  
*  
*  
*  
*   at the Internet Movie Script Database
*  on Lux Radio Theater: January 4, 1954

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 