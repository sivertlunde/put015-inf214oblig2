Santo Luzbel
{{Infobox Film
| name           = Santo Luzbel 
| director       = Miguel Sabido
| producer       = Dulce Kuri Gabriel Romo Alpuche
| writer         = Miguel Sabido Julio Palaez
| starring       = Rafael Cortes Victor Perez Roberto Alvarez Agustin Aviles Ignacio López Tarso Antonio Monroy Carlos Pichardo
| music          = Musicaos de San Miguel Zincacapan Jorge Suarez
| editing        = Óscar Figueroa (film editor)|Óscar Figueroa Jara Jorge Suarez
| distributor    = IMCINE(international)
| released       = 1997
| runtime        = 101 min
| country        = Mexico
| language       = Spanish & Nahuatl
}}

Santo Luzbel is a 1997 drama film.

==Plot==
The film takes place in a remote region (Cuetzalan and Yohualichan in the State of Puebla) of present-day Mexico where a majority of the population speaks Nahuatl as their mother tongue.

The movies plot exploits a historical mistrust and conflict between Native Inhabitants of the region, represented by the Nahuatl-speaking inhabitants and the Spanish-speaking mestizo who identify themselves as being of higher social class and call themselves "Gente de razon". Intertwined in this main conflict there is a second conflict which opposes the view of a traditionalist  priest (Ignacio Lopez Tarso) who sides with the Spanish-speakers and a more liberal priest who better understands the synchretism between Roman Catholicism and Indigenous beliefs. Both priests are Roman Catholic in charge of the parishes of the neighbouring towns.

The main conflict in the film explodes when the Mayordomos of Yohualichan decide to present a Theatrical representation of an ancient text, part in Nahuatl and part in Spanish, called the "Colloquium of the Adoration of the King". The Catholic priest of Yohualichan accuses the Nahuas of blasphmey since the text is over the discussion between the Archangel Michael and Santo Luzbel, seeing them as equals instead of good & evil. The Nahuas want to perform it in full costume in the towns church, much to the dismay of the everyone else.

==Cast==
Director  	Credit
Miguel Sabido 	Director

Cast 	Credit
Rafael Cortes 	Emeterio
Victor Perez 	Melchor
Roberto Alvarez 	Cirilo
Agustin Aviles 	Agustin
Ignacio López Tarso 	Father Leopoldo Santos Higareda
Antonio Monroy 	Olegario
Carlos Pichardo 	Delfino Gonzalez

Production Credits 	Credit
Dulce Kuri 	Executive Producer
Gabriel Romo Alpuche 	Producer

Production Companies 	Credit
Mexican Film Institute 	Production Company
Producciones Nuevo Sol 	Production Company

Distribution Companies 	Credit
Desert Mountain Media 	Domestic Video Distributor

Writer 	Credit
Miguel Sabido 	Screenplay
Julio Palaez 	Screenplay

Art Department 	Credit
Julio Pelaez 	Art Director

Film Camera 	Credit
Arturo De La Rosa 	Cinematographer - cinematography
Jorge Suarez 	Cinematographer - cinematography

Film Sales Financing 	Credit
IMCINE 	Foreign Distribution Sales

===Awards===
It had 8 Academia Mexicana nomination.

===Home video===
Released on DVD in 2006. Special features include English subtitles.

==External links==
*  
*  

 
 
 
 
 


 
 