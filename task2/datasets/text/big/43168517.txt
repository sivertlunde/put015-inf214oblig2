Circus Rookies
{{infobox film
| title          = Circus Rookies
| image          =
| imagesize      =
| caption        =
| director       = Edward Sedgwick
| producer       = Louis B. Mayer Irving Thalberg
| writer         = Lew Lipton (story) Edward Sedgwick (story) Richard Schayer (writer) Robert E. Hopkins (intertitles)
| starring       = Karl Dane George K. Arthur
| music          =
| cinematography = Merritt B. Gerstad
| editing        =
| distributor    = MGM
| released       = March 31, 1928
| runtime        = 60 minutes; 6 reels(5,661 feet)
| country        = USA
| language       = Silent...(English intertitles)

}}
Circus Rookies is a lost  1928 silent film comedy produced and distributed by MGM and directed by Edward Sedgwick. It starred the comedy team of Karl Dane and George K. Arthur.  

==Cast==
*Karl Dane - Oscar Thrust
*George K. Arthur - Francis Byrd
*Louise Lorraine - La Belle
*Sydney Jarvis - Mr. Magoo (*as Sidney Jarvis)
*Fred Humes - Bimbo

unbilled
*Lou Costello -  Extra

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 