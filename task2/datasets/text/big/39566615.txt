Cold Blooded (film)
{{Infobox film
| name = Cold Blooded
| image = Cold_Blooded_-_Original_Theatrical_Poster.jpg
| caption = Theatrical release poster
| director = Jason Lapeyre
| producer =  
| screenplay = Jason Lapeyre
| starring =  
| cinematography = Alwyn Kumst
| editing = Aaron Marshall
| music = Todor Kobakov
| distributor =  
| runtime = 95 minutes
| country = Canada
| language = English
}}

Cold Blooded is a 2012 crime film written and directed by Jason Lapeyre.  The film stars Ryan Robbins, Zoie Palmer, William MacDonald, Sergio Di Zio and Huse Madhavji.  The film tells the story of a female police officer assigned to guard a diamond thief overnight in a hospital when his violent partners break into the hospital to get him.

==Plot==
A diamond heist goes very wrong, leaving one of the team of criminals dead and another arrested for his murder.  The surviving thief, Eddie Cordero, wakes up in a hospital being guarded by Constable Frances Jane.  He tries to con her into letting him go, but shes too smart for him and wont fall for his various pleas.  Later in the evening, Corderos partners in the diamond heist break into the hospital and head for his room.  In the ensuing chaos, Constable Jane ends up having to work with Cordero to survive the night while at the same time trying to keep him from escaping.

==Cast==
* Ryan Robbins as Eddie Cordero
* Zoie Palmer as Frances Jane William Macdonald as Louis Holland
* Sergio Di Zio as George Keyes
* Merwin Mondesir as Nestor Grimes
* Thomas Mitchell as Buckmire
* Husein Madhavji as Dr. Gill

== Production ==

Lapeyre was approached by producer Tim Merkel and told that Merkel had access to an abandoned hospital and needed a story to take place in one.  Merkel gathered a group of writers and gave them a tour of the hospital, and afterwards invited them to submit ideas to him for stories.  Lapeyre submitted two ideas - one for a crime story and one for a zombie story.  Merkel liked Lapeyres ideas and told him to choose the one he felt most strongly about, leading Lapeyre to choose his first love, the crime genre.   The film was shot in Toronto in a single location.  Reservoir Dogs and Elmore Leonard were influences. 

Producer Leah Jaunzems came onto the project later and helped finish raising the financing for the film.  The film was shot in December 2010, finished in 2011 and began playing festivals in 2011 and 2012.

== Release ==

Cold Blooded was an official selection at Fantastic Fest  and Fantasy Filmfest. 

It was picked up by Uncorkd Entertainment/Viva Films in the United States, who will be releasing the film in late 2013. 

The film is distributed in Canada by Astral Grove Entertainment, in Germany by OFDb Filmworks, in Japan by Zazie Films and in The Netherlands, Belgium and Luxembourg by Premiere TV.

== Reception ==
Scott Weinberg of Fearnet called it "a quick and efficient crime thriller" that does not reinvent the wheel.   Corey Mitchell of Bloody Disgusting rated the film 2.5/5 stars and criticized the believability of the plot. 

=== Awards ===

Zoie Palmer won Best Actress at the Bare Bones Film Festival,  and the film won Best Canadian Film at the Fantasia Film Festival in Montreal. 

== References ==

 

== External links ==

* 
* 

 
 
 
 
 
 