Main Tulsi Tere Aangan Ki
{{Infobox film
| name           = Main Tulsi Tere Aangan Ki
| image          = Main Tulsi Tere Aangan Ki 1978 film poster.jpg
| caption        = Film poster
| director       = Raj Khosla
| producer       = Raj Khosla
| writer         = Raj Bharti Chandrakant Kakodkar G.R. Kamath Dr. Rahi Masoom Reza Suraj Sanim
| narrator       = 
| starring       = Nutan Vinod Khanna Asha Parekh
| music          = Laxmikant-Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Raj Khosla Films
| released       = 1978
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Main Tulsi Tere Aangan Ki is a 1978 Indian film directed by Raj Khosla and Sudesh Issar.  It is based on a Marathi novel titled Ashi Tujhi Preet by Chandrakant Kakodkar.

==Plot ==
 Vijay Anand), who is in love with his mistress Tulsi (Asha Parekh) but forced to marry a strong aristocratic woman named Sanjukta (Nutan). Tulsi sacrifices her life, some time after giving birth to Rajnaths son Ajay, because she wants Sanjukta to have her husband all to herself. Rajnath and Sanjukta send Ajay to boarding school to prevent him from bearing the stigma of being an illegitimate child. Sanjukta and Rajnath have a son, Pratap. Rajnath dies in a horse-riding accident. Sanjukta makes regular visits to the boarding school to see Ajay and, when he grows up, she brings him home. Sanjukta makes Ajay (Vinod Khanna) into not only a very important man but also shields him every time and finally confesses before the public that Ajay is her husbands first son and therefore, is entitled to respect. However, her own son Pratap (Deb Mukherjee) feels slighted and becomes wayward. Some people around them also try to further damage the relations between the two brothers. However, for every sin of the younger brother, Ajay protects him and takes the blame. Sanjukta, not knowing the actual situation, gets disturbed. At one stage, she blames Ajay for every wrong thing which actually has been done by her own son. Ajay leaves the house. But soon thereafter, the situation changes and the men standing in support of Pratap feel deceived as he lets them down. In the climax, these men try to kill Pratap, but Ajay, who comes to know of this plan, rescues his brother. Then, Pratap realizes his half-brothers kindness. He surrenders to Ajay and accepts him as the elder brother. The family reunites.

== Cast ==
* Nutan as Sanjukta Chouhan
* Vinod Khanna as Ajay Chouhan
* Asha Parekh as Tulsi  Vijay Anand as Thakur Rajnath Singh Chouhan
* Deb Mukherjee as Pratap Chouhan
* Neeta Mehta as Naini

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chhap Tilak Sab Chhini Re"
| Lata Mangeshkar, Asha Bhosle
|-
| 2
| "Nathaniyan Jo Dali"
| Anuradha Paudwal, Hemlata
|-
| 3
| "Yeh Khidki Jo Band Rahti Hai"
| Mohammed Rafi
|-
| 4
| "Saiyan Rooth Gaye"
| Shobha Gurtu
|-
| 5
| "Main Tulsi Tere Aangan Ki"
| Lata Mangeshkar
|-
| 6
| "Main Tera Kya Le Jaoongi"
| Lata Mangeshkar
|-
| 7
| "Mat Ro Behna"
| Lata Mangeshkar
|}

==Awards and nominations==
* Filmfare Best Movie Award — Raj Khosla
* Filmfare Best Actress Award — Nutan
* Filmfare Best Dialogue Award - Rahi Masoom Raza
* Filmfare Nomination for Best Director — Raj Khosla
* Filmfare Nomination for Best Actress in a Supporting Role — Nutan
* Filmfare Nomination for Best Actress in a Supporting Role — Asha Parekh
* Filmfare Nomination for Best Lyrics — Anand Bakshi for "Main Tulsi Tere."
* Filmfare Nomination for Best Female Playback Singer — Shobha Gurtu for "Saiyan Rooth Gaye"
* Filmfare Nomination for Best Story — Chandrakant Kakodkar
* Nutan was nominated as both Best Actress and Best Supporting Actress.  She won the Filmfare Best Actress Award, but she and co-star Asha Parekh lost out the Best Supporting Actress Award to Reena Roy for Apnapan (1978), who refused the award saying that her role in the film was a leading role, not a supporting role. 

==Reception==

The film became a "Super Hit" at the box office. 

The title song sung by Lata Mangeshkar became an instant classic.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 