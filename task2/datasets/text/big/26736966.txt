The Witness for the Defense
{{Infobox film
| name           = The Witness for the Defense
| image          = Elsie Ferguson The Witness of Defense Film Daily 1919.png
| caption        = period advertisement
| director       = George Fitzmaurice
| producer       = Famous Players-Lasky
| based on       =  
| writer         = Ouida Bergere (scenario)
| starring       = Elsie Ferguson Warner Oland Wyndham Standing
| music          =
| cinematography = Arthur C. Miller  and/or Hal Young
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = Silent (English intertitles)
| budget         =
}}
 silent drama directed by George Fitzmaurice and starring Elsie Ferguson, Warner Oland, and Wyndham Standing.

==Production background== novel of the same title by A. E. W. Mason. Masons story was performed as a play on Broadway in 1911 and starred Ethel Barrymore. 

The film is the earliest of prolific director George Fitzmaurices to survive and is likewise the only silent film of Elsie Ferguson that remains extant. The film is also the first feature length art direction credit for William Cameron Menzies.

==Cast==
* Elsie Ferguson - Stella Derrick
* Vernon Steele - Dick Hazelwood
* Warner Oland - Captain Ballantyne
* Wyndham Standing - Henry Thresk
* George Fitzgerald - Wigney Derrick
* J. H. Gilmour - Harold Hazelwood
* Amelia Summerville - Margaret Pettifer
* Cora Williams - Teresa Derrick
* Blanche Standing - Mary Derrick
* Leslie King - Baram Singh
* Charles W. Charles - Gardner
* Mrs. Bryant - Gardners Wife
* Etienne Girardot - Richard Pettifer
* Henry Warwick - Lawyer

==Preservation status==
A print of this film was discovered in the Gosfilmofond Archive in Moscow.  

==References==
 

==External links==
 
* 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 

 