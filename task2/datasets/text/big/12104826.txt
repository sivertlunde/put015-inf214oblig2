Silver Dream Racer
{{Infobox Film
  | name = Silver Dream Racer
  | caption = 
  | image	=	Silver Dream Racer FilmPoster.jpeg
| director = David Wickes
  | producer = David Wickes Michael Billington (story) David Wickes (screenplay)
  | starring = David Essex
  | music =
  | cinematography = Paul Beeson
  | editing =
  | distributor = Rank Organisation 1980
  | runtime = UK: 111 min USA: 101 min 
  | language = English
  | budget = 
  }}

Silver Dream Racer (1980) is a motor-racing film starring British pop star David Essex and Beau Bridges. The film was produced, written and directed by David Wickes. It would become the last film to be distributed by the Rank Organisation. John Huxley. "Losses of £1.6m sound the knell for cinema production." Times   7 June 1980: 17. The Times Digital Archive. Web. 16 Apr. 2014.
 

==Plot==
Nick Freeman is an aspiring motorcycle racer. But after Nicks brother dies before he is able to test and race the new experimental motorcycle hes developed, Nick inherits the responsibility to prove his brothers design. In spite of a series of tough setbacks, including the loss of his girlfriend, Nick goes into the big race at the British Grand Prix with all his energy and concentration bent on winning. But underhanded American racer (Beau Bridges) is also among the competitors and he is determined to ruin Nicks chances. Numerous incidents happen before Nick crosses the finish line in first place.

==Cast==
 
* David Essex	 ... 	Nick Freeman
* Beau Bridges ... 	Bruce McBride
* Cristina Raines	... 	Julie Prince
* Clarke Peters	... 	Cider Jones
* Harry H. Corbett	... 	Wiggins
* Diane Keen	... 	Tina Freeman
* Lee Montague	... 	Jack Freeman Sheila White	... 	Carol
* Patrick Ryecart	... 	Benson
* Ed Bishop	... 	Al Peterson
* T. P. McKenna	... 	Bank Manager
* David Baxt	... 	Ben Mendoza
* Barrie Rutter	... 	Privateer
* Doyle Richmond	... 	Ciders Brother
* Nick Brimble	... 	Jack Davis
* Malya Woolf	... 	Mrs. Buonaguidi
* Stephen Hoye	... 	Clarke Nichols
* Richard LeParmentier	... 	Journalist
* Murray Kash	... 	1st TV Reporter
* Bruce Boa	... 	2nd TV Reporter
* Christopher Driscoll	... 	Photographer
* Leslie Schofield	... 	Reporter Robert Russell	... 	Garage Mechanic
* Morris Perry	... 	Financier
* Elisabeth Sladen	... 	Bank Secretary (as Elizabeth Sladen) Jim McManus	... 	Bike Salesman
* Antony Brown	... 	Executive
* Edward Kalinski	... 	Disco Boy
* Joanna Andrews	... 	Disco Girl Vincent Wong	... 	1st Japanese Man
* Cecil Cheng	... 	2nd Japanese Man
* David Neville	... 	Man at Bank
* Godfrey Jackman	... 	Bank Doorman
* June Chadwick	... 	Secretary
* Kate Harper	... 	1st Party Guest
* Derrie Powell	... 	2nd Party Guest
 

==Production==
This would be last feature film made by Harry H. Corbett.
 1979 Grand Prix at Silverstone Circuit|Silverstone, Northamptonshire.

Barry Hart designed the Silver Dream that was built in North Wales by his business Barton Motors and put together by fellow director and shareholder Graham Dyson.

==Reception==
The film was not a commercial success at the box office. It holds a 30% fresh rating on review aggregate site Rotten Tomatoes.
==References==
 
==External links==
*  
* 

 
 
 
 
 
 
 


 
 