Humanity Through the Ages
{{Infobox film
| name           = La Civilisation à travers les âges
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = Georges Méliès
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| cinematography = 
| editing        = 
| studio         = Star Film Company
| distributor    = 
| released       = 1908 
| runtime        = 320 meters   
| country        = France
| language       = Silent
| budget         = 
| gross          = 
}}

Humanity Through the Ages ( ), released in the US initially as Humanity Through Ages,  is a 1908 historical drama film directed by Georges Méliès. The film is an episodic narrative displaying examples of humankinds brutality, from the story of Cain and Abel through the Hague Convention of 1907.   

==Summary==
The films first ten episodes feature Cain and Abel, the Druids, Nero and Locusta, the persecution of Christians in the Roman Empire, the pillories of the Middle Ages, the Gibbet of Montfaucon, torture processes in the Middle Ages, Louis XIII, contemporary Parisian Apache (gang)|apaches, and the Hague Convention of 1907. Malthête & Mannoni, p. 229.  The Hague scene ends with the convention collapsing into chaos, with the delegates, who had convened to limit the power of armies, directly attacking each other. The eleventh and final scene, titled "Triumph," shows an Angel of Destruction hovering over a battlefield covered with dead and wounded soldiers.   

==Production== The Last Days of Pompeii, and directed in a highly serious style contrasting sharply with most of Mélièss work.  Méliès made two appearances in the film, as a druid and as a judge in the Spanish Inquisition. Malthête & Mannoni, p. 228.  The Méliès scholars Paul Hammond and John Frazer have noted that the films highly pessimistic tone is unusual among Mélièss work; both scholars have suggested that the pessimism derives from the intense commercial pressure Méliès felt from competitors during the making of the film.  

==Release and reception==
The film was released by Mélièss Star Film Company as its first film of 1908  and numbered 1050–1065 in its catalogs.  It was registered for American copyright at the Library of Congress on 7 February 1908. 

According to the film historian Jay Leyda, the film created a sensation when it was released in Russia.  Méliès reported late in life that Humanity Through the Ages was the film he felt proudest of.  It is now presumed lost film|lost. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 