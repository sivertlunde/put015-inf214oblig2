Storm in a Water Glass
{{Infobox film
| name           = Storm in a Water Glass
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Jacoby  Hermann Fellner   Josef Somlo
| writer         = Bruno Frank (play)   Felix Salten   Walter Schlee   Walter Wassermann
| narrator       = 
| starring       = Hansi Niese   Renate Müller   Paul Otto   Harald Paulsen
| music          = Stefan Weiß
| editing        = 
| cinematography = Guido Seeber   Bruno Timm
| studio         = Sascha Film   Felsom Film
| distributor    = Felsom Film
| released       = 13 March 1931
| runtime        = 
| country        = Austria   Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy Storm in a Teacup. The film is known by the alternative title The Flower Woman of Lindenau (Die Blumenfrau von Lindenau). It is notable, in part, for the small role played by Heddy Lamarr in her second film. The films art direction was by Hans Jacoby.

It premiered in Vienna on 13 March 1931, coinciding with the opening of the Sascha-Filmpalast cinema. 

==Synopsis==
An ambitious town councillor feels confident he will be elected the next mayor, but a dispute over a mongrel dog owned by a local flower seller rapidly turns into a scandal which threatens his political career.

==Cast==
* Hansi Niese as Frau Vogel - Blumenfrau 
* Renate Müller as Viktoria Thoss 
* Paul Otto as Dr. Thoss - ihr Mann 
* Harald Paulsen as Burdach - Redakteur 
* Herbert Hübner as Quilling - Zeitungsverleger 
* Grete Maren as Lisa - dessen Frau 
* Oscar Sabo as Pfaffenzeller - Magistratsdiener 
* Otto Treßleras Gerichtsvorsitzender 
* Franz Schafheitlin as Staatsanwalt 
* Hedy Lamarr as Secretary 
* Eugen Guenther as Beisitzer 1 
* Karl Kneidinger as Beisitzer 2
* Alfred Neugebauer as Kellner

==References==
 

==Bibliography==
* Barton, Ruth. Hedy Lamarr: The Most Beautiful Woman in Film. University Press of Kentucky, 2010.

==External links==
* 

 

 

 
 
 
 
 
 
 
 
 