Bawke
{{Infobox film
| name =  Bawke|
 image = |
 caption = Theatrical release poster |
 writer = Hisham Zaman|
 starring = Broa Rasol, Serdar Ahmad Saleh|
 director = Hisham Zaman |
 producer = Gudny Hummelvoll|
 composer = Øistein Boassen|
 released =  2005|
 runtime = 15 minutes |
 language = Kurdish language|Kurdish, Norwegian language|Norwegian|
}}
 short film directed by Hisham Zaman and produced in Norway.

==Plot==
Two Kurdish refugees from Iraq, a father and son, approach their goal after being on the run for a long time.  After arrival, both father and son are apprehended by the police and taken into a refugee camp. About to be deported and in order for his son to have any chance of staying, the father decides to leave his son behind in the new country. The son desperately tries to catch up with him, but when a policeman ask if he knows the kid, his answer is no.  ,
Nikolaj B. Feifer 

==Awards==
# Best Short Film (Årets kortfilm), Amanda Awards, Norway, 2005.
# Prix UIP Grimstad (European Short Film), Norwegian Short Film Festival, 2005.
# BAFTA/LA Award for Excellence, Aspen Shortsfest, Colorado, 2005.
# Youth Jury Award, Clermont-Ferrand International Short Film Festival, 2006.
# Youth Award, San Sebastián Human Rights Film Festival, 2006.
# Best Live-Action Short, Toronto Worldwide Short Film Festival, 2006.

==See also==
*Cinema of Norway

==References==
 

==External links==
* 
* , Richard Raskin, Nov. 2005.

 
 
 
 


 