A Flat (film)
{{Infobox film
| name           = A Flat
| image          = AFlat2010Film.jpg
| caption        = Promotional poster
| director       = Hemant Madhukar
| producer       = Anjum Rizvi
                   Raaj Varma Entertainment
| starring       = Sanjay Suri Jimmy Shergill Kaveri Jha Aindrita Ray Hazel Crowney Sachin Khedekar Nassar Abdulla Saurabh Dubey
| writer         = Ajay Monga, Hemant Madhukar
| music          = Bappi Lahiri
| cinematography = Manoj Shaw
| editing        = Bunty Nagi
| studio         = Anjum Rizvi Film Company Y.T Entertainment Ltd
| distributor    = 
| released       =  
| country        = India Hindi
}}
A Flat  is a Hindi thriller film, directed by Hemant Madhukar and produced by Anjum Rizvi.The film was released on 12 November 2010 under the Anjum Rizvi Film Company and Y.T Entertainment Ltd. banners.    

==Plot==
The story follows Rahul (Jimmy Shergill), a young businessman who comes back from the U.S. to patch things up with his girlfriend Preeti (Kaveri Jha). His father, Varma (Sachin Khedekar) is mysteriously murdered as he goes to find a flat for Rahul. His friend Karan (Sanjay Suri), a rich salesman, gives his old flat to Rahul. After Rahul enters the flat, his life takes an unexpected turn when the unexplainable disappearance of Preeti takes place, and finally Rahul finds himself trapped in his own flat. With no connection to the outside world, Rahul is stuck and realizes that a ghost is living in the flat with him, who wont let him go. He tries to contact Karan, but is unsuccessful. He then finds a diary in his room, which is opened by the ghost who allows him to read it.

It turns out the diary belongs to Geethika (Hazel Crowney), a young village girl living out her childhood even at an adult age. She finds Karan coming to her village to build many buildings, and Karan uses Geethikas fathers help. When her father cancells it of due to the suicide of her sister, Karan cant take the loss, and runs away with Geethika pretending to be in love with her. Back in a flashback, it is shown the two coming to foreign and getting married in the same flat Rahul is living in. Karan explains that he will return in a few days, but doesnt come back until many months. In his absence, Varma visits Geethika and takes advantage and tries to rape her. She uses self-defence, and tells him to get out. She begins to cry, only to realize she is pregnant. Karan comes back, and tells her to abort the baby because he is already married to someone else. She dies during the abortion, and Karan hides her body so nobody would blame him for her death.

The diary ends, and Rahul investigates that the ghost is Geethikas, and it wants revenge on Karan. Firstly Rahul refuses, though when severely attacked several times, he agrees. He calls Karan over, and tells him everything. He gives the flat keys back, and walks out, and looking back, Karan is now trapped in the flat with no way of getting out. Rahul walks out of the building, and looks at the window, only to see Geethikas ghost approaching Karan, and the curtains closing with Karan shouting for help. Rahul now realizes that except for Geethikas revenge, the ghosts appearance had another meaning, for him to sort out his love life with Preeti. He rings Preeti, and apologizes about every mistake he did, the two make-up and get married. 

==Cast==
* Jimmy Shergill as Rahul
* Sanjay Suri as Karan
* Hazel Crowney as Geethika
* Kaveri Jha as Preeti
* Sachin Khedekar as Varma
* Aindritha Ray as Karans Wife

==Crew==
* director       - Hemant Madhukar
* producer       - Anjum Rizvi
* writer         - Ajay Monga, Hemant Madhukar
* music          - Bappi Lahiri
* cinematography - Manoj Shaw
* editing        - Bunty Nagi
* Associate Directors - Karan Sharma, Kushal Srivastava, Mehboob

==References==
 

 
 
 
 
 
 
 
 