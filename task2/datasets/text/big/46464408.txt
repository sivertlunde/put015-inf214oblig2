Chandrettan Evideya
{{Infobox film
| name = Chandrettan Evideya
| image =Chandrettan Evideya movie poster.jpg 
| caption =Theatrical poster
| alt =
| director = Sidharth Bharathan 
| producer = Sameer Thahir Shyju Khalid Ashiq Usman
| based on = 
| story = Santhosh Echikkanam
| screenplay = Santhosh Echikkanam  Dileep   Anusree Namitha Pramod K. P. A. C. Lalitha
| music = Prashant Pillai 
| cinematography = Shyju Khalid
| editing = Bavan Sreekumar
| studio = Handmade Films
| distributor = Popcorn Entertainments (Asia Pacific release)
| released =   
| runtime = 
| country = India
| language = Malayalam
| budget = 
| gross = 
}} Dileep alongside Anusree, Namitha Pramod and K. P. A. C. Lalitha in the main roles. It is the second directorial of Sidharth. The story and screenplay is penned by noted author Santhosh Echikkanam who had teamed up with Sidharth in his directorial debut Nidra (2012 film)|Nidra (2012). The songs are composed by Prashant Pillai and cinematography is handled by Shyju Khalid.

The film released on 1 May 2015, and received good opening  at the box office and garnered positive reviews from critics. 

==Cast== Dileep as Mattannur Chandramohanan / Velkozhu Kottuvan
* Anusree as Sushama
* Namitha Pramod as Dr. Geethanjali / Vasanthamallika
* K. P. A. C. Lalitha
* Mukesh as Sekharan
* Suraj Venjaramoodu as Ilayath, astrologer
* Chemban Vinod Jose
* Vinayakan
* Veena Nair
* Dileesh Pothen
* Saju Navodaya
* Soubin Shahir sumesh

==Production== Dileep and Anusree but later she backed out because of her busy schedule and later Namitha Pramod was roped in for that role.  The film also stars KPAC Lalitha in a pivotal role. The official trailer for the movie was released on 10 April 2015. 

The songs were composed by Prashant Pillai.

== Reception ==

=== Critical reception ===
Oneindia|Filmibeat.com rated 3.0 out of 5 stars and said the film brings back the vintage Dileep in full swing. And praised the script which has all ingredients for a perfect entertainer, and the brilliant cinematography makes it a visual treat and also appreciated the songs especially Vasanthamallike and background score which perfectly sinks in with the interesting plot and flow of the screenplay. And concluded "Strongly recommended to the audience who love to watch family entertainers".  Malayala Manorama rated 3.5 out of 5 stars while appreciating the script, songs and background score especially mentioning the song Vasanthamallike, and ellaborated "The best part about the movie is that it frees Dileep, the actor, from the unrealistic comic stereotypes which have been proving to be suicidal for him for quite sometime now. The film, with a strong base on middle-class realism, treads the popular narrative line even as it carefully tries to refrain from going after the commercial formula, a genre in which the master-duo Bharathan and Padmarajan and the likes have excelled", and concluded "Chandrettan Evideya&nbsp;is for those who value familial bonds, a type which constitutes the majority of movie-goers in this part of the world. 

Metromatinee.com gave a positive review stating "The film is refreshing in the sense that after a while actor Dileep has opted out of the run of the mill. Dileep the actor comes to the fore after a while and director Sidharth Bharathan deserves a fair bit of credit for it".  The Times of India awarded 3 out of 5 stars and stated "Sidharth Bharathan steps away from his fathers shadow and displays sparks that have the potential to help him carve a niche in the industry. Nevertheless, Chandrettan Evideya is a clean, family watch and a throwback to the times when Dileep movies meant sterling, genuine entertainers.".  Indiaglitz.com opinoin was to "Watch this movie without expecting a usual Dileep fare. ‘Chandretan Evideya’ is definitely a good watch that can leave you entertained. Siddarth Bharathan has nailed it with this simple tale.".  Sify.com gave a verdict "Good" and said "Chandrettan Evideya has nothing much to offer that you have not seen before but even then, it has been presented attractively. In those times when there is very little to choose in terms of good cinema, this one could give you enough to leave the theatre with a smile. Watch this one!". 

==References==
 

==External links==
*  
*   (in Malayalam)

 
 