The Dogway Melody
The Dogway Melody is a 1930 comedy short film that recreates scenes from early musical films, particularly The Broadway Melody.  The entire cast are trained dogs with human voiceovers. It was directed by Zion Myers and Jules White and it forms part of the MGM produced series of Dogville shorts.

This film is an extra feature in The Broadway Melody Special Edition DVD released in 2005.

==Plot==
The series of short films has an all-dog cast (with human voiceovers) that recreate famous scenes from early musical films, particularly The Broadway Melody. The finale is a chorus line of dogs performing "Singing in the Rain" spoofing Cliff Edwardss original version of the song in The Hollywood Revue of 1929. Also spoofed is Al Jolsons performance of "Mammy" in The Jazz Singer. This was a part of MGMs popular series of Dogville Comedies shorts directed by Zion Myers and Jules White.

==Cast==
*Zion Myers - (voice) (uncredited)
*Jules White - (voice) (uncredited)

==External links==
*  

 
 
 
 
 
 
 
 
 


 