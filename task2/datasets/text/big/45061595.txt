Muddula Krishnaiah
 
{{Infobox film
| name           = Muddula Krishnaiah
| image          =
| caption        =
| writer         = Ganesh Patro  
| story          = Bhargav Arts Unit
| screenplay     = Kodi Ramakrishna
| producer       = S. Gopal Reddy
| director       = Kodi Ramakrishna Radha
| music          = K. V. Mahadevan
| cinematography = P. Lakshman
| editing        = K. Satyam
| studio         = Bhargav Art Productions   
| released       =  
| runtime        = 133 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Telugu film Radha in the lead roles, with music by K. V. Mahadevan.    

==Cast==
*Nandamuri Balakrishna as Krishnaiah
*Vijayashanti as Shanti Radha as Radha
*Gollapudi Maruti Rao as Appa Rao
*S. Varalakshmi as Mangalagiri Ramanamma
*KK Sarma
*Telephone Satyanarayana 
*Juttu Narasimham 
*Dham as Priest
*Kalpana Rai 
*Nirmalamma 
*Y.Vijaya

==Soundtrack==
{{Infobox album
| Name        = Muddula Krishnaiah
| Tagline     = 
| Type        = film
| Artist      = K. V. Mahadevan
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 25:22
| Label       = SEA Records
| Producer    = K. V. Mahadevan
| Reviews     =
| Last album  = Maa Pallelo Gopaludu   (1985)
| This album  =  Muddula Krishnaiah   (1986)
| Next album  = Seetarama Kalyalam   (1986)  
}}

The music was composed by K. V. Mahadevan with lyrics by C. Narayana Reddy. The soundtrack was released by SEA Records Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Andhagaada SP Balu, P. Susheela
|4:02
|- 2
|Krishnaiah Dhookaadu SP Balu
|5:24
|- 3
|Em Cheyanu SP Balu, S. Janaki
|4:03
|- 4
|Ongolu Gittha (M) SP Balu
|3:54
|- 5
|Suruchira Sundharaveni SP Balu,P. Susheela
|3:52
|- 6
|Ongolu Gittha (F)
|P. Susheela
|4:07
|}

==Others==
* VCDs and DVDs were released by Teja Videos, SHALIMAR Video Company, Hyderabad, Telangana|Hyderabad.

==References==
 

 
 
 
 


 