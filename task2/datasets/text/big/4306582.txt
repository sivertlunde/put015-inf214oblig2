The Public Eye (film)
 
{{Infobox Film 
| name           = The Public Eye
| image          = The Public Eye.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Howard Franklin
| writer         = Howard Franklin
| producer       = Sue Baden-Powell
| starring       = Joe Pesci Barbara Hershey 
| music          = Mark Isham
| cinematography = Peter Suschitzky
| editing        = Evan A. Lottman
| studio         =
| distributor    = Universal Studios
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $15,000,000
| gross          = $3,067,917
}}
The Public Eye is a 1992 neo-noir film written and directed by Howard Franklin, produced by Sue Baden-Powell, and starring Joe Pesci and Barbara Hershey. Stanley Tucci and Richard Schiff appear in supporting roles.
 New York Arthur "Weegee" Fellig, and some of the photos in the film were taken by Fellig.

==Plot==
In the 1940s, Leon "Bernzy" Bernstein is a freelance photographer for the New York City Tabloid (newspaper format)|tabloids, dedicated to his vivid and realistic work and his ability to get shots nobody else can get. He is very confident of his skills, declaring at one point, "Nobody does what I do. Nobody."

With a police radio under the dashboard of his car and a makeshift darkroom in his trunk, he quickly races to the scene of horrific crimes and accidents in order to snap exclusive photographs. He is so good at his job that he becomes known affectionately as the "Great Bernzini".
 mob is muscling in on her due to some arrangement with her late husband. Kay asks if Bernzy could investigate an individual she considers troublesome. Generally unsuccessful with women, Bernzy agrees to help. In fact, he begins falling in love with Kay.
 journalist friend FBI are gas rationing, and the government. His activities get Sal killed and place Bernzys life in great danger as he waits in hiding at a restaurant where a mob hit is about to take place.

==Cast==
* Joe Pesci as Leon "Bernzy" Bernstein
* Barbara Hershey as Kay Levitz
* Stanley Tucci as Sal
* Jerry Adler as Arthur Nabler
* Dominic Chianese as Spoleto
* Richard Riehle as Officer OBrien
* Max Brooks as Teen at Thompson Street
* Richard Schiff as Thompson Street Photographer
* Christian Stolte as Ambulance Attendant
* Jack Denbo as Photo Editor
* Timothy Hendrickson as Richard Rineman
* Del Close as H.R. Rineman
* Lisa Parthenakis Christakis -  as young bride with soldier husband the night before he sails,  in  five and dime as Bernzy looks on admiringly and longingly from the soda fountain counter

==Production==
  as "Bernzy".]] Oscar winning performance in Goodfellas (film)|Goodfellas.  It was an attempt to capitalize on his popularity at the time and help elevate Pesci from respected character actor to star status.

Director and writer Howard Franklin was unable to secure the rights to Arthur "Weegee" Felligs story.  Franklin, then, wrote the story of a Weegee-like photographer who smokes cigars and he named him Leon "Bernzy" Bernstein.  In the film, like Weegee, cops wonder if Bernzy uses a ouija board to snap his photographs and find the stories. 

According to journalist Doug Trapp, Franklin was inspired to write the screenplay after seeing a show of Felligs photos in the 1980s at the International Center of Photography in New York City. And Weegee did, in real life, have  a "soul-mate" named Wilma Wilcox, who was the woman in charge of the his estate.  But, Franklin has always denied that the film was about their relationship. 

===Filming locations===
Even though the film takes place in Manhattan, it was shot in Chicago, Illinois, Cincinnati, Ohio, and Los Angeles, California.

==Distribution==
The producers used the following tagline when marketing the film:
:Murder. Scandal. Crime. No matter what he was shooting, "The Great Bernzini" never took sides he only took pictures... Except once.

The Public Eye began filming on July 24, 1991 and completed October 28, 1991.

The film premiered at the Toronto Film Festival in September 1992.   The film also was shown at the Venice Film Festival and the Valladolid International Film Festival in Spain.
 Los Angeles. It opened wide on October 16, 1992.

The films box office performance was a disappointment.  The first weeks gross was $1,157,470 and the total receipts for the run were $3,067,917.  According to "The Numbers" box office database the film was in circulation one week (5 days) in 635 theatres.  

===Home media===
The film was released in video format on April 14, 1993 and in laserdisc on April 21, 1993.

In September 2007 an Unbox Digital Video Download was made available. The movie is also available at Amazon.com as digital Video on Demand.

It was released on DVD on January 31, 2011 in the Universal Vault Series.

==Reception==

===Critical response===
Chicago Sun-Times film critic, Roger Ebert, was especially complimentary of the film and of actor Joe Pesci, and wrote, "One of the best things about the movie is the way it shows us how seriously Bernzy takes his work. He doesnt talk about it. He does it, with that cigar stuck in his mug, leading the way with the big, ungainly Speed Graphic with the glass flashbulbs. In the movies big scene of a mob assassination, he stares death in the face to get a great picture."  Ebert said the film made him "think" a little bit of Casablanca (film)|Casablanca (1942).
 Time Out magazine liked the acting and the script, and wrote, "The main virtue of screenwriter Franklins debut as director is Pescis portrayal of Weegee, the famous low-life tabloid photographer of urban disaster, lightly concealed as Bernstein, The Great Bernzini... good dialogue, nice period recreation, great performances." 

Desson Howe, film critic for The Washington Post wrote, "Public wants to be taken for an atmospheric film noir, full of intrigue, romance and street toughness. But its all flash and no picture. Despite the usual quippy, perky performance from Pesci, as well as cinematographer Peter Suschitzkys moodily delineated images, the movie is superficial and unengaging. Its as if Life magazine|Life magazine decided to make an oldtime gangster movie." 

==Soundtrack==
The original score for the film was written and recorded by Jerry Goldsmith but he was replaced by Mark Isham at a late stage. As such, Isham had a lot of work to do and in a short time.  He found working with director Franklin a wonderful and educational experience. 
 strings and trumpet also play a major role.

==References==
 

==External links==
*  
*  
*   shooting screenplay draft for educational purposes
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 