Makalkku
 
{{Infobox film
| name           = Makalkku
| image          = Makalkku.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Jayaraj
| producer       = Alex Varghese
| writer         = Madambu Kunhukuttan   Rajan Poduwal
| narrator       = 
| starring       = Shobana  Suresh Gopi
| music          = Ramesh Narayan
| cinematography = M.J. Radhakrishnan
| editing        =    
| studio         = 
| distributor    = 
| released       = 2005
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Makalkku  is a 2005 Malayalam film, made in Cinema of India|India, by Jayaraj, and starring Suresh Gopi and Shobana. 

Noted Indian singer Adnan Sami performed a song in this movie, which became a superhit.

==Plot==
A woman gives birth to a daughter on the streets, her identity is unknown, and she is non-responsive. Her daughter, Manasi, is raised in the mental asylum, and is later adopted by a doctor in the hospital.

==Cast==
*Shobana as Killeri 
*Suresh Gopi as Dr.Warrier 
*Rehana as Manasi (as Baby Rehana)
*Shalu Menon as Bindu
*Valsala Menon as Gayathri
*Vinayakan

== Sound Track ==
The films soundtrack contains 8 songs, all composed by Ramesh Narayan. Lyrics by Kaithapram Damodaran Namboothiri, Rifat Sulthan and Anil Panachooran.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chaanchaadiyaadi"
| Gayatri Asokan
|-
| 2
| "Chaanchaadiyaadi"
| Adnan Sami
|-
| 3
| "Mukilin Makale" Manjari
|-
| 4
| "Baharon Ko Chaman" Hariharan
|-
| 5
| "Edavamaasa Perumazha"
| Dhanu Jayaraj
|-
| 6
| "Edavamaasa Perumazha"
| Balachandran Chullikkad
|-
| 7
| "Paavakali"
| Jassie Gift, Madhusree Narayanan
|-
| 8
| "Paavakali  "
| Madhusree Narayanan
|}

==Awards== Manjari for the song Mukilin makalae....
*Kerala State Film Award for Best Makeup Artist in 2004 for Renjith Ambadi

==External links==
*  

== References ==
 

 
 
 
 
 


 