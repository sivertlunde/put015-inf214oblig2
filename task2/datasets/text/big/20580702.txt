Spread (film)
{{Infobox Film
| name           = Spread
| image          = Spreadposter.jpg
| image_size     = 
| caption        = Theatrical release poster David Mackenzie
| producer       = Anthony Callie
| writer         = Jason Hall
| story          = Paul Kolsby
| narrator       = Ashton Kutcher
| starring       = Ashton Kutcher Anne Heche Margarita Levieva Sebastian Stan
| music          = John Swihart
| cinematography = Steven Poster
| editing        = Nicholas Erasmus
| distributor    = Anchor Bay Films
| released       = 14 August 2009 (US)
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $4,789,700
| gross          = $12,032,983     
 }}
 David Mackenzie. The film was released under the name L.A. Gigolo in Denmark, Finland, Norway, and Sweden, as Toy Boy in France, Germany, Italy and the Netherlands, Oh yeah in Argentina, as American Playboy in Spain and Portugal, Jogando com Prazer in Brazil, Love, Sex and Celebrity in Japan, and S-Lover in South Korea. The film was released on January 17 at the Sundance Film Festival and was released on August 14, 2009 in North American theatres. 

==Plot==
Narcissistic gigolo Nikki (Ashton Kutcher) lives in Los Angeles, drifting from one relationship to another without a steady job or even a place to live. He preys on women who can provide for him. After meeting Samantha (Anne Heche) at a club he moves in with her, using his looks and sexual prowess to keep her happy.

Before long, however, Nikki starts cheating on Sam, first with his friend Emily (Rachel Blanchard), then with Christina (Sonia Rockwell), whom he met at another party.  Emily disapproves of Nikkis free-wheeling lifestyle and has expressed a desire for a relationship, but Nikki has no real interest in her except for sex. Samantha catches Nikki with Christina, but they come to an uneasy arrangement where she will ignore his infidelity.

While Sam is out of town, Nikki meets a waitress named Heather (Margarita Levieva). He enlists his friend Harry (Sebastian Stan) to help him get Heather interested, but she doesnt fall for his charms. Although he eventually gets a date with her, she abandons him afterwards. Soon after, Heather unexpectedly shows up in Nikkis pool and they end up having sex. However, the next morning Nikki is moving Heathers car and realizes it doesnt belong to her but to her "boyfriend", after she told him she was single; Nikki throws her out in anger. However, he cant stop thinking about her, and his obsession frustrates Samantha to the point that she gets fed up and throws him out, quickly taking him back. Nikki, however, leaves on his own accord, still enchanted by Heather. 

Nikki searches for a place to stay, but has a fall out with Harry and cannot get into the parties he once did. He runs into Heather at a swanky hotel, and she admits that she was only interested in him for his house, believing him to be rich. It transpires that she is the same as Nikki, scamming rich men for money in the same way he does with women. She lets him move in with her and her stoner room-mate Eva (Ashley Johnson) and they begin dating, though Heather continues to scam and hustle, with some reluctant assistance from Nikki.

One day, an upset Heather reveals to Nikki that she just broke up with her fiancé because shes in love with him (Nikki). She further tells him her fiancés family owns the New York Rangers and that hes the one whos been paying her bills and living expenses. Nikki, who has also fallen in love with Heather, is nonetheless angry that she kept her engagement from him, and leaves the house in a huff. When he returns he only finds a note that says she has left for New York City. At Evas urging and with Harry paying for the airfare, Nikki follows her. He finds her at a plush penthouse and begs her to come back to LA with him. She refuses, telling him she cant afford to let him chase his fantasies around while she runs down the funds they would need to live. He then proposes to her, but she tells him that she is already married, breaking his heart. When told about divorce, Heather says that she cant, because she needs the luxuries and high expenses. Her husband (Hart Bochner) returns home and Heather passes Nikki off as a grocery boy, dismissing him.

Nikki returns to LA, getting an honest job delivering groceries and living with Harry. He delivers groceries to Samanthas house, where theyre picked up by the kept man who has replaced Nikki. The ending credits show Nikki feeding a mouse to Harrys African Bullfrog.

==Cast==
* Ashton Kutcher as Nikki
* Anne Heche as Samantha "Sam"
* Margarita Levieva as Heather
* Sebastian Stan as Harry
* Ashley Johnson as Eva
* Rachel Blanchard as Emily
* Eric Balfour as Sean

==Reception==
===Critical reception===
Spread received generally negative reviews from critics. At Rotten Tomatoes, based on 59 reviews, the movie rated an aggregate score of 21% positive reviews, noting that, "Despite occasional detours into surprisingly dark territory, Spread overall is an ineffectual celebration of vacuous Los Angeles high life rather than a deconstruction of it." 

===Box office===
The film was released on August 14, 2009. At the box office, the film grossed $12,032,983, of which $250,618 was from North America.  

==References==
 

==External links==
*  
*  
 
 

 
 
 
 