Little Lord Fauntleroy (1980 film)
{{Infobox film
| name     =  Little Lord Fauntleroy
| image          = LittleLordFauntleroy 1980 cover.jpg
| caption        = DVD cover
| writer         =  Frances Hodgson Burnett (novel) Blanche Hanalis (teleplay)
| starring       =  Rick Schroder Alec Guinness Eric Porter Colin Blakely Connie Booth
| director       = Jack Gold
| producer       =  Norman Rosemont
| cinematography =  Arthur Ibbetson Keith Palmer
| production_company = 
| distributor    = 
| music          = Allyn Ferguson
| released   = December 1980
| runtime        = 103 minutes
| language = English
| budget         = 
| country = United Kingdom|
| gross          = 
}} British family film directed by Jack Gold and starring Alec Guinness, Rick Schroder and Eric Porter.  It is based on the Little Lord Fauntleroy|childrens novel of the same name.

==Plot synopsis==
Young Cedric Ceddie Errol and his widowed mother live in genteel poverty in 1880s Brooklyn after the death of his father. He was the favorite son of Cedrics grandfather, the Earl of Dorincourt, because the other two were wastrels and weaklings.  But the Earl has long ago disowned his son for marrying an American. Cedrics two best friends were Mr. Hobbs the grocer (and a Democrat and anti-aristocrat) and Dick Tipton the bootblack.

After his other two sons die, leaving Cedric technically the heir to the earldom, Lord Dorincourt sends Mr. Havisham, his lawyer, to America to bring Cedric to Britian. Mr Havisham is authorized to buy expensive gifts for Cedric, but he chooses to buy an engraved gold watch for Mr Hobbs and enable Dick to buy out his drunken partner.

Mrs. Errol accompanies her son, but is not allowed to live at Dorincourt castle nor meet the Earl, although she will receive a house and a large allowance. She does not tell Cedric of his grandfathers bigotry. The Earls lawyer is impressed with the young widows dignity and intelligence, especially after she begins to take care of the poor living on the land.
 
Cedric is most grateful that his grandfather, albeit unintentionally, enabled him to help his friends, and thinks he is a kind man.  This soon wins the heart of his stern grandfather. All his tenants and nearby villagers are also taken by him, especially as he inspires his grandfather to be more caring about his tenants. Slowly thawing, the Earl hosts a grand party to proudly introduce his grandson to British society, notably his formerly estranged sister, Lady Constantia Lorridaile. Lady Constantia is impressed with both Cedric and his mother.

After the party, Havisham tells the Earl that Cedric may not be the heir apparent. An American dancer calling herself Minna Errol has approached him, insisting that her son Tom is the offspring of her late husband, the Earls second son, Bevis. Heartbroken, the Earl is forced to accept her apparently valid claim. Minna proves to be uneducated and openly mercenary. 

Fortunately for Cedric, Dick recognizes Minna from her newspaper picture, as the ex-wife of his brother Ben, Toms real father. They travel to England, confront Minna and thus disprove her claim. 

The overjoyed Earl apologizes to Cedrics mother and brings her to live with the delighted Cedric on his estate. The small family has a festive Christmas dinner with all their friends and servants.

==Cast==
 
* Rick Schroder as Cedric Errol, Lord Fauntleroy
* Alec Guinness as John Arthur Molyneux Errol, Earl of Dorincourt
* Connie Booth as Mrs. Errol, Cedrics mother
* Eric Porter as  Havisham, Dorincourts adviser
* Colin Blakely as Silas Hobbs, Cedrics friend
* Rachel Kempson as Lady Lorradaile, Dorincourts sister
* Carmel McSharry as Mary, Mrs. Errols substantive
* Antonia Pemberton as Dawson, Dorincourts substantive
* Rolf Saxon as  Dick Tipton, Cedrics friend
* John Cater as Thomas, the Butler
* Peter Copley as Reverend Muldaur
* Patsy Rowlands as  Mrs. Dibble
* Beth Isaac as Miss Smiff
* Patrick Stewart as Wilkins, the riding teacher
* Gerry Cowper as Mellon, the room service
* Edward Wiley as Ben Tipton Kate Harper as Minna Tipton
* Tony Melody as Mr. Kimsey
* Rohan McCullough as Lady Grace
* Dicon Murray as Georgie
* Ballard Berkeley as Sir Harry Lorradaile John Southworth as Higgins
* Bill Nighy as the Hunting Officer
* Norman Pitt as Lord Ashbey Delefante
 

== Distribution ==
Little Lord Fauntleroy had its premiere on television in the United States on November 25, 1980 on CBS, and was then was then released in the United Kingdom in December 1980, and a German-dubbed version was first screened in Germany on December 26, 1982. The film has since become a Christmas classic in Germany,  and is shown on the national broadcast network Das Erste every year.

==References==
 

==External links==
* 

 
 

 
 
 
 
 