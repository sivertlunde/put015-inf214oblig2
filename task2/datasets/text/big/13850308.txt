The Yolk's on You
 
  Sylvester J.Cat, and Foghorn Leghorn. It is part of Daffys Easter Egg-Citement and it shows a rare time when Foghorn Leghorn, Sylvester, and Daffy all appear together in a mix.

==Plot==
Miss Prissy is as usual late for Foghorn Leghorns egg expectation. Leghorn is disappointed in Miss Prissy for being late and not laying a normal round white egg. Leghorn warns Prissy only one last chance that if she doesnt lay a normal egg, she will be sent to the old hens farm. Foghorn Leghorn decides that Miss Prissy lays the turquoise Easter Eggs. He also tells her to think "Oval|egg-shape".

Prissy tries but she lays a golden egg instead. She rolls away the golden egg and soon Sylvester and Daffy find it. The two both try to get it for themselves including the ancient Chinese tickle torture but at the end they accidentally put the golden egg on the fresh egg farms. Daffy whispers to Sylvester to get the egg for one last chance.

This episode was Foghorns Leghorns first appearance since "False Hare" (1964), sixteen years earlier.

==References==
 

 
 
 
 
 


 