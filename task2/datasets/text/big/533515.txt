Cat's Eye (1985 film)
{{Infobox film
| name           = Cats Eye
| image          = Cats Eye (poster).JPG
| caption        = Theatrical release poster 
| writer         = Stephen King
| starring = {{Plainlist|
* Drew Barrymore
* James Woods Alan King Kenneth McMillan
* Robert Hays
* Candy Clark
}}
| director       = Lewis Teague
| producer       = Dino De Laurentiis Martha Schumacher
| music          = Alan Silvestri
| cinematography = Jack Cardiff
| editing        = Scott Conrad
| studio         = De Laurentiis Entertainment Group
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 94 min.
| country        = United States
| language       = English
| budget         = $7,000,000
| gross          = $13,086,298 (US)   
}}
 anthology horror The Ledge", Night Shift collection, and the third is unique to the film. The three stories are connected only by the presence of a traveling cat.  The cat plays an incidental role in the first two and is a major character of the third.
 Alan King, Robert Hays and Candy Clark.

==Plot== a dishevelled Christine according to its fender sticker). He hides from the dog in a delivery truck, which drives to New York City. The tomcat hears the disembodied voice of a young girl (Drew Barrymore) pleading for help because something is threatening her. The cat is then captured by an employee from Quitters, Inc.

==="Quitters, Inc."===
 Alan King) explains the clinics uniquely persuasive method: every time Dick smokes a cigarette, horrors of increasing magnitude will befall his wife and child.

Using the tomcat that Donattis assistant Junk has caught in the street, Donatti demonstrates the first of these horrors: the cat is locked in a cage and tormented (albeit harmlessly) with electric shocks. Donatti explains that if his new client should be caught with a cigarette, Dicks wife Cindy will be subjected to the same shocks while he is forced to watch. For subsequent infractions, his young daughter afflicted with down syndrome will be subjected to the shocks, then his wife raped, and after the fourth infraction, they give up (i.e. kill him). Not wanting to worry them, Dick hides the looming threat from his family.

That night, Dick is angered by the methods Quitters uses and notices an undisposed pack of cigarettes in his desk. He prepares to smoke it, but notices a set of feet in his closet, realizing Quitters Inc. is serious about their threat to ensure he is not smoking. The following day, Dick visits his daughter (also played by Drew Barrymore) and gives her a doll. Donatti is also at the school, warning Dick that if he strays the only thing his daughter would understand is that someone is hurting her because her father misbehaved. Morrison resolves to never let that happen, and Donatti hopes so for her sake.

Two weeks later, Dick is at a party where he is the only guest not smoking. He feels trapped by everyone enjoying themselves while he must stick to Donattis regimen, but resists tempatation and declines an offer to "go with the gang" and smoke.

During a stressful traffic jam, Dick loses his will and smokes after finding an old pack of cigarettes in his glovebox, not realizing he is being watched by Junk in a nearby car. After watching Cindy suffer in the electric cage, an enraged Dick attacks Donatti and Junk, allowing the tomcat to escape in the scuffle. After regaining the upper hand Donatti says he understands and forgives Dick. Dick is determined never to smoke again and tells his wife everything, after which they embrace.

Time passes, and Dick is apparently smoke-free at last, but has put on a little weight as a result of quitting. Dr. Donatti prescribes diet pills and sets a target weight for Dick. Dick jokingly asks what will happen if he strays; if a man would attack his house with a flame thrower. Donatti chuckles and says not that in mind; instead someone will cut off his wifes pinky finger. Later Dick and his wife have a dinner party with the friends that recommended Quitters, Inc., and they toast the company for a job well done. As she raises her glass, Dick discovers the friends wife is missing her little finger.

==="The Ledge"===
 Kenneth McMillan). Cressner, who will bet on anything, wins a wager that the cat will successfully cross the busy road outside his casino. He takes the tomcat home.
 gothic skyscraper. Mustang by Mike Starr) named Albert, a.k.a. "Ducky" for his T-shirt that depicts a cartoon duck.

Norris agrees. During the circumambulation attempt Cressner and Ducky appear by windows to keep Norris from "cheating", and Cressner turns on a fire hose at the halfway point, which is a roomier section of the ledge, to keep Norris from lingering.  Despite their harassment and a moment alone when Norris is hanging from a dislodged neon sign, Norris makes it back to the apartment, where Norris learns Mrs. Cressner has been dead the whole time. Cressner slyly claims he honors his bet to the letter of the law, in which the drugs have been removed; Norris can have a bagful of cash and "his wife", as evidenced by her severed head. Enraged, Norris springs upon Cressner, surprising him, while his gunman drops his firearm when tripped-up by the fleeing cat. Norris takes the gun, kills the henchman, and turns it on Cressner. Norris forces Cressner to undergo the same ordeal on the ledge, but the casino owner is less successful (running into a foot-pecking bird that hassled Norris) and falls to his death while the escaped tomcat watches.

==="General"===

The tomcat hops a freight train and travels to Wilmington, North Carolina, where it is adopted by a little girl, Amanda (Drew Barrymore), who names him General. The cat runs afoul of the girls mother (Candy Clark), who believes he will harm their pet parakeet, Polly.

Despite Amandas protests, the mother puts General out at night. As a consequence, he is unable to protect Amanda from a small, malevolent troll that he witnessed take up residence in the house. When Amanda sleeps, the troll emerges via a retractable hole in one of the walls in Amandas room. The troll slays the parakeet with a tiny dagger and then tries to steal Amandas breath. General finds a way into the house and battles the troll. After luckily wounding the cats shoulder with his dagger, the troll quickly finds itself outmatched by an enraged General. It successfully flees, leaving Amanda and her parents to discover the death of the bird. The parents are convinced that General killed Polly, but the father discovers a wound on the tomcat too large to have been caused by a parakeet. He starts to doubt the mothers belief that General slew the bird.

General is then taken to the animal shelter by the mother and is scheduled to be euthanized the next day (at the mothers request). When night falls, the tiny troll emerges from a retractable hole in Amandas wall. It uses a small rubber doorstop to wedge the childs room door shut from the inside. Again it attempts to take the sleeping girls breath. Meanwhile, at the animal shelter, a relaxed looking General is brought his last meal. But when his cage is opened, the wily feline explodes into action. He escapes and rushes back to Amandas. Determined to defend the child at any cost, General makes his way down the chimney.

General arrives in the nick of time to save Amanda, and again battles the troll, causing a great deal of noise. Knowing that he is no match for the cat, the small creature tries to flee. But General cuts off the trolls escape route by knocking down a thick hardcover storybook, covering the wall hole. Grabbing on to a bunch of mylar balloons, the troll tries to float out of the furious cats reach, landing on Amandas record player. The quick witted tomcat then uses the record player to hurl the troll into a box fan, slicing it to bits. The ruckus awakens Amandas parents, who are initially prevented by the blocked door from reaching her. Once they break into her room, the girl explains to them that General saved her from the troll. The parents are at first unwilling to believe the story until parts of the trolls dismembered corpse are discovered, as well as the tiny dagger that caused Generals wound. Amanda uses the justification that General will keep her safe in case others like her first assailant appear, and General is allowed to stay inside at night to act as a protector for Amanda. The valiant tomcat finally has a loving home.

==Cast==
*Drew Barrymore as Our Girl/Amanda/Morrisons daughter
*James Woods as Dick Morrison
*Candy Clark as Sally Ann Alan King as Dr. Vinny Donatti
*Robert Hays as Johnny Norris Kenneth McMillan as Cressner
*James Naughton as Hugh
*James Rebhorn as Drunk Businessman
*Charles S. Dutton as Dom Mike Starr as Ducky
*Frank Welker as the voice of the cat and the troll.

==Production==
 
This was Barrymores second film based on Kings writing; she had previously appeared in  .

The Police song "Every Breath You Take" is heard in a few places. Since the original version was too expensive for this low-budget production, a cover version was used.

Frank Welker provided the voices of General the Cat and the Troll. One of the cartoon characters on the balloons in Amandas room, with which the Troll uses to attempt its getaway, is Baby Kermit from Muppet Babies (whom Welker had also voiced), the others from cartoons Shirt Tales and Smurfs which Welker had done voice-work for.
 The Dead Zone on television (he clicks off the television in disgust, remarking "I dont know who writes this crap"), and Amandas mother in the last segment is reading Pet Sematary in bed. The third segment takes place in Wilmington, North Carolina, the same setting as Maximum Overdrive.

==Release==
Cats Eye received mixed to positive reviews from critics, as the film currently holds a 67% rating on Rotten Tomatoes based on 24 reviews.

The film was released theatrically in the United States by MGM on April 12, 1985.  It grossed $13,086,298 at the domestic box office. 

The film was released on DVD by Warner Home Video in 2004. 

==Awards==
The film was nominated for the International Fantasy Film Award for Best Film in 1987. Drew Barrymore was nominated for the Young Artist Award for Best Starring Performance by a Young Actress in a Motion Picture in 1986.

==See also==
*Creepshow
*List of Stephen King films

==References==
 

==External links==
* 
* 
* 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 