Felix și Otilia
{{Infobox film
| name = Felix şi Otilia
| image =
| caption =
| director = Iulian Mihu
| producer = Marin Gheoroaie
| writer = George Călinescu (novel) Ioan Grigorescu
| starring = Radu Boruzescu Julieta Szönyi
| music = Anatol Vieru
| editing = Lucia Anton
| distributor = Româniafilm
| released = March 20, 1972
| country = Romania
| runtime = 146 minutes Romanian
| budget =
}}
Felix şi Otilia (Felix and Otilia) is a 1972 Romanian drama film based on the George Călinescus 1938 novel Enigma Otiliei. The film was directed by Iulian Mihu and scripted by Ioan Grigorescu. The titular roles are played by Radu Boruzescu and Julieta Szönyi.

== Production ==
Screenwriter Ioan Grigorescu and director Iulian Mihu held "long and resultful discussions" with George Călinescu about an adaptation of the novel Enigma Otiliei. The writer said that he wanted thee film to be made in the style of The Umbrellas of Cherbourg, a musical film. He hummed a song that the screenwriter reproduced to composer Anatol Vieru. The song was played by Aurelian Andreescu and was used before the credit titles in the beginning of the film. Ioana Popescu - „De vorbă cu Ioan Grigorescu” (Ed. Artprint, București, 2010), p. 43-44. 

== References ==
 

 
 
 


 