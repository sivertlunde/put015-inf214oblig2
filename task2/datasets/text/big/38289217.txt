The March Hare (1919 film)
{{Infobox film
| name = The March Hare
| image =
| caption = Frank Miller George Clark
| writer = Guy Newall   Frank Miller
| starring = Godfrey Tearle   Ivy Duke   Will Corrie   Philip Hewland
| music = Bert Ford
| editing =
| studio = Lucky Cat
| distributor = Ideal Film Company
| released = August 1919
| runtime = 5 reels 
| country = United Kingdom
| awards =
| language = Silent   English intertitles
| budget =
| preceded_by =
| followed_by =
}} silent comedy Frank Miller and starring Godfrey Tearle, Ivy Duke and Will Corrie. The screenplay was written by Guy Newall as a vehicle for his wife Ivy Duke. 

==Cast==
* Godfrey Tearle as Guy
* Ivy Duke as Ivy
* Will Corrie
* Philip Hewland
* Lewis Gilbert
* Douglas Heathcote Percy Crawford
* Peggy Maurice John Miller

==References==
 

==Bibliography==
* Bamford, Kentom. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 
 
 
 
 
 
 


 
 