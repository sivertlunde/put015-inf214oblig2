The Virgin of Stamboul
 
{{Infobox film
| name           = The Virgin of Stamboul
| image          = The Virgin of Stamboul.jpg
| caption        = Film poster
| director       = Tod Browning
| producer       =  William Parker H. H. Van Loan
| starring       = Priscilla Dean Wheeler Oakman Wallace Beery
| cinematography = William Fildew
| editing        = Viola Mallory Universal Film Manufacturing Company
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
  and Wheeler Oakman]] 
The Virgin of Stamboul is a 1920 American drama film directed by Tod Browning and starring husband and wife team Priscilla Dean and Wheeler Oakman and  featuring Wallace Beery in a supporting role.   

==Plot==
Based upon a review in a film publication,  Sari (Dean) is a beggar girl of the streets of Stamboul, near Constantinople, who attracts the attention of Captain Pemberton (Oakman), a soldier of fortune, who has recruited the Black Horse cavalry to maintain law and order. Sari overhears him being told that her soul is as filthy as the streets, so she goes to pray in a mosque although she knows Turkish women are not allowed to enter. There she witnesses a revenge murder by a sheik (Beery), who then attempts to lure her into his harem. She defies him, and he then tries to purchase her. Pemberton returns from the desert and has determined that he loves Sari. The sheik then carries both Pemberton and Sari to his fortified camp outside the city walls. Sari escapes and gets the Black Horse cavalry to attack the camp, resulting in a battle and rescue.

==Cast==
* Priscilla Dean as Sari
* Wheeler Oakman as Capt. Carlisle Pemberton
* Wallace Beery as Ahmed Hamid (segment "Achmet Bey")
* Clyde Benson as Diplomat
* E. Alyn Warren as Yusef Bey
* Nigel De Brulier as Capt. Kassari
* Edmund Burns as Hector Baron
* Eugenie Forde as Saris Mother
* Ethel Ritchie as Resha
* Yvette Mitchell as Undetermined role

==Marketing==
To market the film, Harry Reichenbach in a publicity stunt had a "sheik" from Constantinople with his entourage check into the Majestic Hotel in New York City on March 7, 1920. Newspapers carried the story of his visit looking for an American heiress who had left with a marine just as the first teaser ads for the film were being published.  Several ads noted the films reported production budget of $500,000. 

==References==
 

==External links==
 
* 

 

 
 
 
 
 
 
 
 
 