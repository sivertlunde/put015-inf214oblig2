Naan Kadavul
{{Infobox film
| name           = Naan Kadavul
| image          = Naan kadavul.jpg
| alt            = Theatrical poster of the film
| caption        = Theatrical poster Bala
| producer       = K. S. Sreenivasan
| screenplay     = Bala Jeyamohan
| based on       =  
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Arthur A. Wilson
| editing        = Suresh Urs
| Sound recordist=Raj shekar.k
| studio         = Vasan Visual Ventures
| distributor    =  
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Tamil
| budget         =  7 crore 
| gross          = 
}}
 2009 Indian Tamil drama Tamil novel Arya and Pooja in the lead roles.

The film revolves around Rudran (Arya), who in his childhood was left in Varanasi by his parents only to become an aghori. Years later he is brought back to Tamil Nadu, his homeland where he encounters a new world of physically and mentally challenged beggars. Rudran happens to meet Amsavalli (Pooja), a blind girl being controlled by Thandavan, a local thug and his henchmen. In the end, Rudran kills Thandavan and joins his mentor in Varanasi.
 National Film Best Director Filmfare Award. It was successful at the box office as well. 

== Plot ==
For astrological reasons, a father leaves his son Rudran (Arya (actor)|Arya) in Kasi. Fourteen years later, repenting his act he goes with his daughter in search of him. He finds him there but is shocked to learn that he has become an Aghori, a cannibalistic character who gives moksha and prevents the soul from getting rebirth. Nevertheless he brings him back to Tamil Nadu. The story takes a turn here and introduces us to the world of physically and mentally challenged beggars. A world controlled by the cruel Thandavan (Rajendran). Hamsavalli (Pooja Umashankar) a blind girl is forcefully separated from her troupe and made to join the beggars. Soon she becomes another victim of Thandavans cruelty. Meanwhile, Rudran leaves his house to find his place on a small cave, and soon meets Hamsavalli, who tries her best to convince him to return home, but fails. Thandavan then takes a deal with a Malayali man of the same profession to sell some of the beggars.The Malayali guy forcefully takes the beggars away,although they want to stay with the rest of the beggars. He returns again,but this time to force Hamsavalli to marry an ugly, deformed man for 10-lakhs worth of money. Thandavan orders his men to bring Hamsavalli but they take Hamsavalli to Rudran to help her. Rudran kills the Malayali guy and is also arrested by the local Police. This agitates Thandavan and he orders the Police man to release Rudran so that he can kill him. In the mean time Thandavan also finds Hamsavalli and on one incident, Hamsavalli refuses to marry the deformative,which angers him and he runs away without marrying her. This angers Thandavan, who deeply wounds her. Rudran is shown wounded in his forehead with blood oozing out. Flashback opens where Thandavan appears face to face with Rudran. In the fight between the two, Rudran kills Thandavan. Rudrans wound in the forehead is caused due to the fight. She later goes crying to Rudran to somehow free from this curse and also prevent her from rebirth. The aghori then kills her and grants her moksha. In the final scene Rudran returns to Kasi.

== Cast == Arya as Rudran Pooja as Hamsavalli
* Rajendran as Thandavan
* Krishnamoorthy as Murugan
* Azhagan Thamizhmani as Rudrans father
* Singampuli
* Aacharya Ravi
* Rasaiya Kannan

== Production ==

After the release of Pithamagan (2003), Bala began to work on a script for a film that would star Ajith Kumar in the lead role and produced by A. M. Rathnam.  Ajith signed a contract for the film stating that he would work in the film for 150 days, and the project was titled  Naan Kadavul.  However Ratnam, the producer of the film dropped out in December 2004, opting to concentrate on his Telugu film Bangaram and his sons venture, Kedi (2006 film)|Kedi.  Early sources indicated that Cleeny, sister of actress Gopika, would play the lead role in the film although this later proved to be untrue and Meera Jasmine was selected.   As pre-production work continued, Ajith grew his hair for the role and subsequently appeared in a song in the much-delayed film, Varalaru (film)|Varalaru with the long hair he grew for Naan Kadavul, when doing patchwork.  The film was briefly shelved in August 2005 and Ajith moved on to sign other films such as P. Vasus Paramasivan, which was initially set to be produced by Bala, and Perarasus Thirupathi.  The film then re-emerged and in April 2006, Bala announced the technical crew of the film revealing that Arthur A. Wilson would be cinematographer, Krishnamoorthy as art director and that Ilaiyaraaja would score the films music. Pre-production on the film began in early 2006, with Balas assistants already scouting for ideal filming locations in the city of Varanasi.  Ajith announced that the shoot of the film would start in the city in May 2006, with the actor refusing to speculate the story of the film.  However as the film yet again failed to take off, Ajith finally pulled out of the project in June 2006 stating he could wait no longer for Bala. 
 Suriya was Arya was Saran for Bhavana was signed for the film after the success of Chithiram Pesuthadi, replacing Meera Jasmine. Ravi, director of Vignesh starrer Aacharya and Kannan, director of Raasaiyya, made their debuts as actors with this film.  Rajendran, a fight master who earlier appeared in a small role in director Balas previous film Pithamagan was selected to play main villain thus making his debut as full-fledged actor.  The film was consequently launched in June 2006 at Hotel Green Park, Chennai with P. L. Thenappans Sri Rajalakshmi Films as producers. 

The photo shoot of the film was held in August 2006 with Arya and Bhavana and images of Arya were released showing him in different postures of Yoga including Sirasasanam and Padmasanam. The films first schedule began later that month in Nazarethpettai, near Chennai.  Reports suggested that Arya was ousted from the film in November 2006 and replaced with Vikram (actor)|Vikram, but this proved untrue.  Shoots continued in Kasi and Varanasi in January 2007, with Arya opting against working in any other films til Naan Kadavul was complete.  Producer Thennapan also backed out of the film in early 2007 but Srinivasan of Vasan Visual Ventures took over swiftly. 
 Anjali and Pooja was Seeman and thought twice about accepting the film due to her commitments in a Sinhalese film, before the producer of that film released her from her contract.  

== Soundtrack ==
This film features 7 songs composed by Ilaiyaraaja. The audio was released on 1 January 2009. Lyrics have been penned by Vaali except for the track Pitchai Paathiram which has been penned by Ilayaraja himself and the title song "Maa Ganga" written by Bharath Achaarya. The song "Matha Un Kovilil" was reused from Rajas own song which he had composed for Achchani (1978).

* Maa Ganga - Kunal Ganjawala
* Om Sivoham - Vijay Prakash
* Kannil paarvai - Shreya Ghoshal
* Matha un kovilil - Srimathumitha
* Amma un pillai naan - Sadhana Sargam
* Oru kaatril alaiyum - Ilayaraaja
* Pitchai paathiram - Madhu Balakrishnan

== Release ==

=== Critical response ===
 

Rediff wrote:"Naan Kadavul is definitely worth a watch for its superb secondary characters, setting and music. Now, if only the screenplay had provided the missing punch".  Behindwoods wrote:"Naan Kadavul is not a movie that everyone can digest. It shows reality in such brutality that you wonder whether such things really do happen. It cannot be called violent; the word ‘brutal’ has to be repeated often to describe the movie. One feels the director could have toned it down a bit, it leaves one very disturbed".  Sify wrote:"Its a much darker film than the director’s previous works. Watch Naan Kadavul, because its one of those films that wont easily get out of your head long after the film is over".  Indiaglitz wrote:"In one word, Naan Kadavul is a movie to cherish and celebrate".  Actor Rajinikanth was in full praise of Naan Kadavul, citing it as an extraordinary work on screen and congratulated them for their meticulous efforts. 

=== Accolades ===
{| class="wikitable"
|-
! Award
! Category
! Nominee
! Outcome
|- 56th National Film Awards National Film Best Director Bala (director)|Bala
| 
|- Best Make-up Artist
|U.K. Sasi
| 
|- 57th Filmfare Awards South Filmfare Best Best Tamil Actress Pooja
| 
|- Filmfare Best Best Tamil Director Bala
| 
|- Filmfare Best Best Tamil Actor Arya (actor)|Arya
| 
|- Filmfare Best Best Tamil Film
|K. S. Sreenivasan
| 
|- Filmfare Best Best Tamil Supporting Actor Rajendran
| 
|- Filmfare Best Best Tamil Lyricist Ilayaraja for "Pitchai Paathiram"
| 
|- Tamil Nadu State Film Awards Tamil Nadu Best Female Character Artiste Pooja
| 
|- Tamil Nadu Best Villain Rajendran
| 
|- Tamil Nadu Best Cinematographer Arthur A. Arthur Wilson
| 
|- Vijay Awards Vijay Award Best Director Bala
| 
|- Vijay Award Best Actress Pooja
| 
|- Vijay Award Best Villain Rajendran
| 
|- Best Make up
|U.K. Sasi
| 
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 