Panithuli
{{Infobox film
| name           = Panithuli
| image          = Panithuli.jpg
| caption        = Promotional poster
| director       = Natty Kumar Dr. Jay
| producer       = Natty Kumar Kalpana Pandit
| story          = 
| screenplay     = 
| narrator       = 
| starring       = Ganesh Venkatraman Kalpana Pandit Shobana
| music          = Agnel Roman Faizan Hussain
| cinematography = Chris Eldridge C.J. Rajkumar
| editing        = N. Vasanth N. Ayappan
| studio         = Dreams On Frames House of Pandit
| distributor    = 
| runtime        = 143 minutes
| released       =  
| country        = India, United States
| language       = Tamil
| budget         = 
| gross          = 
}}

Panithuli (English: Dew Drops) is a 2012 Tamil action film directed by Natty Kumar and Dr. Jay, starring Ganesh Venkatraman, Kalpana Pandit and Shobana. The film was released on 10 August 2012.  It received a lot of negative reviews.        The film was shot in Hindi. 

==Cast==
* Ganesh Venkatraman as Shiva
* Kalpana Pandit as Maya
* Shobana as Meera
* Sreedharan Karthikeyan as Balu
* Narayan Sundararajan as Arun
* Thirumudi Thulasiraman as Rajaraman
* Natty Kumar as Veeraswamy
* Cliff Janke as Winston

==References==
 

==External links==
*  

 
 
 
 
 