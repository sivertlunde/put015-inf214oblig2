Badri (2001 film)
 
 
{{Infobox film
| name = Badri
| image = Vijay Bhumika Monal
| director = Arun Prasad P.A.|P. A. Arun Prasad
| producer = B. Sivarama Krishna
| cinematography =Jayanan Vincent
| editing = N. Hari
| studio = Sri Venkateswara Art Films
| released =  
| runtime = 160 minutes
| country = India
| language = Tamil
| music = Ramana Gogula (Soundtrack) Devi Sri Prasad (Background Score)
}} sports action Vijay in Monal are Vivek play supporting roles. The films soundtrack was composed by Ramana Gogula while the background score was composed by Devi Sri Prasad. The film released on 12 April 2001 to positive reviews and was declared super hit .

==Plot== Bhupinder Singh), who happens to be Mamathis new boyfriend. He defeats Rohit, redeems himself in front of his fathers eyes and finally accepts Janus love.

==Cast== Vijay as Sri Badrinatha Moorthy aka Badri
* Bhumika Chawla as Janaki aka Janu Monal as Mamathi Vivek as Azhagu Bhupinder Singh as Rohit Kitty as Badris father
* Malaysia Vasudevan as Janakis father
* Riyaz Khan as Vetrinath aka Vetri
* Meenakumari as Anitha Anumohan as Chitthappa
* Dhamu as Badris Friend Sanjeev as Badris Friend Pandu
* Shihan Hussaini as Vetris Coach
* Kazan Khan as Rohits Coach Alphonsa in a Special Appearance

==Production== Bhupinder Singh was also selected to reprise his role from the original version as Rohit, the antagonist. 

During the filming of a pivotal scene in the "Travelling Soldier" song, Vijay allowed a car to run over his fingers and the shot was canned with three cameras, with the scene attracting media attention. Martial arts expert Shihan Husseini helped with the production and features in the film in a guest appearance.  Other scenes were shot at Amir Mahal in Chennai. 

The film teamed up with Coca Cola for their publicity campaign after Vijay had signed on to the soft drink company as a brand ambassador.  

==Release== Sun TV. The film was given a "U" certificate by the Indian Censor Board.

==Critical Reception== Monal "needs to work on her expressions".  

==Box office==
This film ran for 100 days and proved commercial success. Vijay

==Soundtrack==
{{Infobox album  
 Name = Badri
| Type = Soundtrack
| Artist = Ramana Gogula
| Cover =
| Released = 2001
| Recorded = Feature film soundtrack
| Length = 37.91
| Label = Star Music
| Producer = Ramana Gogula
| Reviews =
| Last album = Yuvaraju (2000)
| This album = Badri (2001)
| Next album = Johnny (2003 film)|Johnny (2003)
}}
The soundtrack of the film was composed by Ramana Gogula who composed the original film and reused all the tunes making his debut in Tamil and notably remains his first and only Tamil film he had worked so far, was well received by the audience. The lyrics were penned by Palani Bharathi.

==Track list==
{{tracklist
| headline        = Track-list
| extra_column    = Artist(s)
| total_length    = 37:91
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          =  Travelling Soldier
| extra1          = Ramana Gogula
| lyrics1         = Palani Bharathi
| length1         = 04:05
| title2          =  Adi Jivunnu Jivunnu
| extra2          = Ramana Gogula, Devi Sri Prasad
| lyrics2         = Palani Bharathi
| length2         = 02:04
| title3          =  Salaam Maharasha 
| extra3          = Devan Ekambaram, Priya Himesh
| lyrics3         = Palani Bharathi
| length3         = 02:21
| title4          =  Ennoda Laila Vijay
| lyrics4         = Palani Bharathi
| length4         = 05:13
| title5          =  Kalakalakudhu Mano
| lyrics5         = Palani Bharathi
| length5         = 05:04
| title6          = Kalakalakudhu
| extra6          = Shankar Mahadevan
| lyrics6         = Palani Bharathi
| length6         = 05:04
| title7          =  Kadhal Solvadhu
| extra7          = Srinivas (singer), Sunitha Upadrashta
| lyrics7         = Palani Bharathi
| length7         = 04:34
| title8          =  Angel Vandhaaley
| extra8          = Devi Sri Prasad, K. S. Chithra
| lyrics8         = Palani Bharathi
| length8         = 04:45
| title9          =  King Of Chennai
| extra9          = Devi Sri Prasad
| lyrics9         = Palani Bharathi
| length9         = 04:17
| title10          =  Stella Maris Laara
| extra10          = Tippu (singer), Vivek (actor), Dhamu 
| lyrics10         = Palani Bharathi
| length10         = 01:44
}}

==References==
 

==External links==
*  

 
 
 
 
 
 
 