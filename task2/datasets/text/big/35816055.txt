Doctor Murek
{{Infobox film
| name           = Doktór Murek
| image          = 
| image_size     = 
| caption        = 
| director       = Juliusz Gardan
| producer       = 
| based on       = 
| writer         = Tadeusz Dołęga-Mostowicz   Juliusz Gardan
| narrator       =  Nora Ney Jadwiga Andrzejewska
| music          =  Władysław Szpilman
| editing        = 
| cinematography = Seweryn Steinwurzel
| studio         = Parlo-Film
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Poland Polish
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish drama film directed by Juliusz Gardan. It is based on two novels by Tadeusz Dołęga-Mostowicz.

==Cast==
* Franciszek Brodniewicz - doctor Franciszek Murek  Nora Ney - Arleta 
* Jadwiga Andrzejewska - Mika 
* Janina Wilczówna - Nira 
* Ina Benita - Karolka
* Lidia Wysocka - Tunka Czabran
* Aleksander Zelwerowicz - chairman Jacek Czabran 
* Mieczysława Ćwiklińska - Mrs. Czabran
* Kazimierz Junosza-Stepowski - chairman Jaźwicz
* Bronisław Dardziński - Kuzyk 
* Maria Żabczynska - Kuzyks wife
* Tadeusz Kański - Black Kazik 
* Jerzy Kaliszewski - Jurek Czolkowski 
* Stanisław Sielański - Cipak, shelter pal 
* Wanda Jarszewska - the fortune-teller

==Bibliography==
* Skaff, Sheila. The Law of the Looking Glass: Cinema in Poland, 1896-1939. Ohio University Press, 2008.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 