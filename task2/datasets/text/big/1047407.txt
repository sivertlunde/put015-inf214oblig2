Jean de Florette
 
{{Infobox film
| name           = Jean de Florette
| image          = Jean de Florette.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Claude Berri
| producer       = Pierre Grunstein Alain Poiré
| writer         = Claude Berri Gérard Brach Marcel Pagnol
| starring       = Yves Montand Gérard Depardieu Daniel Auteuil
| music          = Jean-Claude Petit Giuseppe Verdi
| cinematography = Bruno Nuytten
| editing        = Noëlle Boisson Sophie Coussein Hervé de Luze Jeanne Kef Arlette Langmann Corinne Lazare Catherine Serris
| distributor    = Orion Pictures (USA)
| released       =  
| runtime        = 120 minutes
| country        = France Italy 
| language       = French
| budget         = $17 million
| gross          = $87 million
}} period drama Manon des BAFTA award for his performance, and Yves Montand in one of the last roles before his death.

The film was shot, together with Manon des Sources, over a period of seven months. At the time the most expensive French film ever made, it was a great commercial and critical success, both domestically and internationally, and was nominated for eight César Award for Best Actor|César awards, and ten BAFTA Award for Best Actor in a Supporting Role|BAFTAs. In the long term the films did much to promote the region of Provence as a tourist destination.

==Plot  == poacher inside the house.

The property descends to the dead mans sister, Florette de Berengere, a childhood friend of Papet; who married the blacksmith in another village, Crespin, whilst Papet was recovering in a military Hospital in Africa. He writes to Grafignette—a common friend—for news on Florette, and finds that she died the same day his letter arrived. The property thereby descends to her son Jean, who is a tax collector and "unfortunately, by Gods will...hes a hunchback". To discourage the new owner from taking up residence, Ugolin breaks many tiles on the roof of the residence.

Florettes son (Depardieu) arrives with his wife Aimée and young daughter Manon, and Soubeyrans hopes of an easy takeover are soon shattered. The new owner is called Jean Cadoret, but Ugolin, in the local custom, calls him Jean de Florette. Jean makes it clear that he has no intention of selling, but plans to take up residence and live off the land. He has a grand scheme for making the farm profitable within two years, involving breeding rabbits and feeding them off Cucurbitaceae|cucurbit. Jean does not know about the nearby spring, but he knows of another, more distant spring; and the house has a cistern that can supply some water for irrigating crops. The distant spring, where an old Italian couple lives, is   away and also part of the property. Jean believes the needs of the farm can be met from here. Ugolin is discouraged, but Papet tells him to befriend Jean and gain his confidence. They also keep secret from him the fact that—while average rainfall for the surrounding region is sustainable—the area where Florettes farm lies rarely gets any of this rain. Meanwhile, the two work to turn the local community against the newcomer, who is described merely as a former tax collector from Crespin, as the deceased Pique-Bouffigue has distant cousins living in the village who know about the spring.

Jean initially makes progress, and earns a small profit from his rabbit farm. In the long run, getting water proves a problem, and dragging it all the way from the distant spring becomes a backbreaking experience. Jean asks to borrow Ugolins mule, but is met only with vague excuses. Then, when the rain does come, it falls on the surrounding area but not where it is needed. Jean loudly berates God, whom he thinks has already given him enough trouble by making him a  , or drive Jean away for good. From the money Jean buys dynamite to finish the well, but an accident occurs, and he is hit by a rock and falls into the dynamite hole. At first the injuries seem minor, but it turns out the rock fractured his spine, and when the doctor arrives he declares Jean dead. Ugolin returns with the news to Papet, who asks him why hes crying. "It is not me whos crying," he responds, "its my eyes".

Aimée and Manon are now forced to leave the farm, and Papet offers to buy them out. As the mother and daughter are packing their belongings, Papet and Ugolin make their way to where they blocked the spring, to pull out the plug. Manon follows them, and when she sees what the two are doing,  understands and gives out a shriek. The men hear it, but quickly dismiss the sound as that of a buzzard making a kill. As Papet performs a mock baptism of his nephew in the cold water of the spring, the film ends with the caption "end of part one".

==Cast==
*  , died during filming.    Montand himself died in 1991, and the two films were among the last of a cinematic career spanning forty-five years.  Having grown up in nearby Marseilles, he visited the location before filming started, and endeared himself with the locals.  BAFTA and a César Award for Best Actor|César – was a great step forward in his career. 
* Gérard Depardieu as Jean Cadoret / "Jean de Florette": Jean is a city man with a romantic idea of the countryside, yet obstinate and hard-working.  Depardieu was well established as a versatile actor even before this role.  Seemingly impervious to the great pressure on the film crew, he earned a reputation on the set for "fooling about, telling jokes, swearing at planes interrupting the shot and never knowing his lines until the camera was rolling". 
*  .  Aimée was played by Gérard Dépardieus real-life wife, Élisabeth. 

==Production==
  landscape, central to the film, can be seen in the background.]] Manon des Sources was four hours long, and subsequently cut by its distributor. The end result left Pagnol dissatisfied, and led him to retell the story as a novel.       The first part of the novel, titled Jean de Florette, was an exploration of the background for the film; a prequel of sorts. Together the two volumes made up the work Pagnol called LEau des collines (The Water of the Hills).  Berri came across Pagnols book by chance in a hotel room, and was captivated by it. He decided that in order to do the story justice it had to be made in two parts. 
 Mirabeau (65&nbsp;km to the north), while Jean de Florettes house is located in Vaugines, where the church from the film can also be found.  The market scenes were filmed in Sommières in the Gard, and the storys Les Romarins was in reality Riboux in the Var (department)|Var. 

Extensive work was put into creating a genuine and historically correct atmosphere for the film. The facades of the houses of Mirabeau had to be replaced with painted polystyrene, to make them look older, and all electric wires were put underground.  Meanwhile, in Vaugines, Berri planted a dozen olive trees twelve months before filming started, and watered them throughout the waiting period, and for the second installment planted 10,000 carnations on the farm. 
 Minister of Jack Lang. Giuseppe Verdis 1862 opera La forza del destino.

==Reception==
The film was a great success in its native France, where it was seen by over seven million people.  It also performed very well internationally; in the United States it grossed nearly five million US$, placing it among the 100 most commercially successful foreign-language films shown there. 

Critical reception for Jean de Florette was almost universally positive.  
Rita Kempley, writing for The Washington Post, compared the story to the fiction of William Faulkner. Allowing that it could indeed be "a definitive French masterwork", she reserved judgement until after the premiere of the second part, as Jean de Florette was only a "half-movie", "a long, methodic buildup, a pedantically paced tease".  
Roger Ebert of the Chicago Sun-Times commented on Berris exploration of human character, "the feeling that the land is so important the human spirit can be sacrificed to it". Ebert gave the film three-and-a-half out of four stars.     

The staff reviewer for the entertainment magazine Variety (magazine)|Variety highlighted – as other reviewers did as well – the cinematography of Bruno Nuytten (an effort that won Nuytten a BAFTA award and a César nomination).  The reviewer commended Berri particularly for the work done with the small cast, and for his decision to stay true to Pagnols original story.  Richard Bernstein, reviewing the film for The New York Times, wrote it was "like no other film youve seen in recent years". He called it an updated, faster-paced version of Pagnol, where the original was still recognisable.  The newspaper lists the film among the "Best 1000 Movies Ever Made".  
Later reviews show that the film has stood up to the passage of time. Tasha Robinson, reviewing the DVD release of the two films for The A.V. Club in 2007, called the landscape, as portrayed by Berri and Nuytten, "almost unbearably beautiful". Grading the films A, she called them "surprisingly tight and limber" for a four-hour film cycle. 

=== Awards === Best Film, Best Director Best Cinematography Best Actor for Daniel Auteuil.   BAFTA awards Best Actor Best Cinematography, Best Film Best Adapted Best Actor-category, Best Direction Best Foreign Language Film.   Best Foreign Best Foreign Golden Globes.  It was also nominated for the Golden Prize at the 15th Moscow International Film Festival.   

==Legacy== French cinema]] culture and President François Minister of Jack Lang, to promote these kinds of films through increased funding of the ailing French film industry. Berris pair of films stand as the most prominent example of this effort.   It has also been suggested that the treatment given the outsider Jean de Florette by the locals was symbolic of the growing popularity of the National_Front_(France)#Immigration|anti-immigration movement, led by politicians like Jean-Marie Le Pen. 

The two films are often seen in conjunction with Peter Mayles book  A Year in Provence, as causing increased interest in, and tourism to, the region of Provence, particularly among the British.  The films inspired a vision of the area as a place of rural authenticity, and were followed by an increase in British home ownership in southern France.  As late as 2005, the owners of the house belonging to Jean de Florette in the movie were still troubled by tourists trespassing on their property. 

Ranked No. 60 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema – 60. Jean de Florette 
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=60 
| work = Empire 
}} 
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 