The Wrong Guy
 
{{Infobox film name = The Wrong Guy image = The wrong guy.jpg caption = DVD cover director = David Steinberg producer = Jon Slan Janet E. Cuddy Martin Walters writer = Dave Foley David Anthony Higgins Jay Kogen starring = Dave Foley David Anthony Higgins Jennifer Tilly Joe Flaherty music = Lawrence Shragge cinematography = David A. Makin editing = Christopher Cooper distributor = Lions Gate Entertainment released = August 1, 1997 runtime = 92 minutes language = English budget = preceded by = followed by =
}}
 1997 Cinema Canadian comedy film directed by David Steinberg.  It was co-written by Dave Foley of The Kids in the Hall and Newsradio fame, along with David Anthony Higgins and Jay Kogen (the latter of The Simpsons writing fame). Foley also stars in the picture, along with David Anthony Higgins, Jennifer Tilly, Colm Feore and Joe Flaherty.

The script was originally inspired by a sketch Foley himself wrote back during his days with The Kids in the Hall.

==Synopsis==
 narcoleptic farm girl, Lynn Holden (Tilly).  What follows is a bizarre series of misadventures marked with slapstick routines and constant one-liner joke|one-liners.

==Cast==
{| class="wikitable"
! Actor
! Role
|-
| Dave Foley || Nelson Hibbert
|-
| David Anthony Higgins || Detective Arlen
|-
| Colm Feore || The Killer
|-
| Jennifer Tilly || Lynn Holden
|-
| Joe Flaherty || Fred Holden
|-
| Dan Redican || Ken Daly
|-
| Richard Chevolleau || Jimmy
|-
| Alan Scarfe || Farmer Brown
|-
| Kenneth Welsh || Mr. Nagel
|-
| Enrico Colantoni || Creepy Guy
|-
| Kevin McDonald || Motel Manager
|-
| Jay Kogen || Bus Driver
|-
| David Steinberg || Outpatient in Neck Brace
|-
|}

* The members of Barenaked Ladies made cameos as singing policemen.

==Awards==
* Film Discovery Jury Award (U.S. Comedy Arts Festival) - Best Screenplay Dave Foley, David Anthony Higgins, Jay Kogen 

==References==
 

==External links==
* 

 

 

 

 
 
 
 
 
 

 
 