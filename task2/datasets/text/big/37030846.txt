Diary of a High School Bride
{{Infobox film
| name           = Diary of a High School Bride
| image          =
| image_size     =
| caption        =
| director       = Burt Topper
| producer       = Burt Topper
| writer         = Burt Topper Robert Lowell Jan Englund
| based on       =
| starring       = Anita Sands
| music          =
| cinematography =
| editing        =
| studio         =
| distributor    = American International Pictures
| released       =  
| runtime        = 72 mins
| country        = United States
| language       = English
| budget         = $80,000   accessed 3 January 2012 
| gross          =
}}
Diary of a High School Bride is a 1959 film directed by Burt Topper about a seventeen-year-old high school student who gets married. American International Pictures released the film as a double feature with Ghost of Dragstrip Hollow.

The lead actor, Anita Sands, had never acted before. Girl With No Previous Roles Given Film Lead
Los Angeles Times (1923-Current File)   16 May 1959: 12.  The movie was shot over seven days.  Contemporary reviews were poor. High School Bride Remains Sophomore
Stinson, Charles. Los Angeles Times (1923-Current File)   16 Jan 1960: A6. 

Leftover sets for the film were used by Roger Corman to shoot Bucket of Blood (1959). Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p145 

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 
 
 


 