Susanne (1950 film)
{{Infobox film
| name           = Susanne
| image          = File:Susanne Poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Movie poster for Susanne
| director       = Torben Anton Svendsen
| writer         = Fleming Lynge
| based on       =    Rasmus Christiansen, Ellen Gottschalch, Erik Mørk, Astrid Villaume
| studio         = Nordisk Film
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = Denmark
| language       = Danish
}}

Susanne is a Danish film of 1950, directed by Torben Anton Svendsen and starring Astrid Villaume in the title role.

The film won three prizes at the 1950  ), Best Actress (Astrid Villaume) and Best (Danish) Film. 

==Cast==
*Erik Mørk ... Hakon Riis
*Astrid Villaume ... Susanne Drewes
*Ellen Gottschalch ... Andrea Drewes
*Lis Løwert ... Helene Drewes
*Katy Valentin ... Louise Hallenberg

==External links==
* 

==References==
 

 

 
 
 
 


 