After Alice
{{multiple issues|
 
 
}} mystery thriller thriller directed by Paul Marcus and written by Jeff Miller. The film stars Kiefer Sutherland as Detective Mickey Hayden.

== Summary ==
A troubled cop makes a discovery that really has him worried in this thriller. Mickey Hayden (Kiefer Sutherland), a police detective, is ordered to take a new look at a case hed worked on ten years ago. A brilliant but demented serial killer known as Jabberwocky went on a killing spree before dropping out of sight; Hayden was never able to track him down, and the disappointment has left Hayden with more than his share of emotional scars; the detective has since become an alcoholic in a failed attempt to cope. After a decade of silence, Jabberwocky strikes again, sending the police a note suggesting Hayden be put back on his case. But this time around, Hayden notices something different as he investigates the killings; when he comes in contact with the evidence, he has troubling psychic visions that tell him more about the murders than he ever wanted to know.

==Cast==
* Kiefer Sutherland ...  Mickey Hayden 
* Henry Czerny ...  Harvey 
* Polly Walker ...  Dr. Vera Swann  Gary Hudson ...  John Hatter 
* Ronn Sarosiak ...  Ray Coombs 
* Stephen Ouimette ...  Gideon Wood 
* Eve Crawford ...  Margaret Ellison 
* Denis Akiyama ...  Owen Gackstetter 
* Ardon Bess ...  Lenny 
* A. Frank Ruffo ...  Alvin 
* Loren Petersen ...  Alice Ellen Lurie  
* Alexander Chapman ...  Claudette  
* Martha Gibson ...  Mrs. Lurie 
* Colin Glazer ...  Tom Ellison 
* Susan Kottman ...  Mrs. Ramsey  
* Keith White ...  Hal 
* Stephanie Belding ...  Talbot 
* Finn White ...  Kid 
* Rick Bennett ...  Bartender 
* Brian Smegal ...  Detective Lewis 
* Jim Aldridge ...  Transit Cop 
* Tanya Doyle ...  Immigrant Wife 
* Jasmin Geljo ...  Immigrant Husband 
* Lily Eng ...  Secretary 
* Samantha Espie ...  Girl
 

==References==
 

 
 
 
 

 