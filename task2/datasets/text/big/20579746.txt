Maa Ka Aanchal
{{Infobox film
| name           = Maa Ka Aanchal
| image          = 
| image_size     = 
| caption        = 
| director       = Jagdev Bhambri 
| producer       = 
| writer         =
| narrator       = 
| starring       =Sanjeev Kumar and Leela Mishra
| music          = 
| cinematography =
| editing        = 
| distributor    = 
| released       = 1970
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1970 Bollywood drama film directed by Jagdev Bhambri. The film stars Sanjeev Kumar and Leela Mishra .

==Cast==
*Sanjeev Kumar   
*Anjana  
*Master Ratan   
*Abhi Bhattacharya   
*Leela Mishra   
*Johnny Whisky
==Soundtrack==
{| class="wikitable"
! Serial !! Song Title !! Singer(s)
|- 1 || "Bhanwar Mein" || Asha Bhosle
|- 2 || Mukesh
|- 3 || "Jaane Kya Haal Ho" || Asha Bhosle
|- 4 || "Maa Hai (version 1)" || Mohammed Rafi
|- 5 || "Maa Hai (version 2)" || Mohammed Rafi
|- 6 || "Maa Hai (version 3)" || Mohammed Rafi
|- 7 || "Maa Ka Aanchal" || Asha Bhosle
|- 8 || "Tere Jhuthe Waade" || Asha Bhosle
|- 9 || "Waqt Meherbaan Hai" || Asha Bhosle
|-
|}

==External links==
*  

 
 

 
 
 
 