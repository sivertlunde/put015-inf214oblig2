For Heaven's Sake (1950 film)
{{Infobox Film
| name           = For Heavens Sake
| image          = For Heavens Sake 1950.jpg
| image_size     = 
| caption        = 
| director       = George Seaton
| producer       = William Perlberg
| based on       =  
| writer         = George Seaton
| narrator       = 
| starring       = Clifton Webb Joan Bennett Robert Cummings Alfred Newman
| cinematography = Lloyd Ahern Robert L. Simpson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 minutes United States
| language       = English
| budget         = 
| gross          = $1.7 million (US rentals) 
}}

For Heavens Sake is a 1950 fantasy film starring Clifton Webb as an angel trying to save the marriage of a couple played by Joan Bennett and Robert Cummings. It was adapted from the play May We Come In? by Harry Segall.   

==Plot==
Angels Charles (Clifton Webb) and Arthur (Edmund Gwenn) try to convince a young cherub named Item (Gigi Perreau) to stop waiting to be born to Lydia (Joan Bennett) and Jeff Bolton (Robert Cummings). They are too busy acting in and directing plays respectively to start a family. They are also drifting apart, as Lydia wants to have a child, but Jeff convinces her to put their careers first.
 The Westerner) to help matters along by taking human form as "Slim" Charles, a supposedly rich Montanan, and bumping into the Boltons at the racetrack. Jeff sees a potential financial backer (an "angel" in theatrical slang) for his next play. He gets his playwright, Daphne Peters (Joan Blondell), to try to convince Charles to invest in the production. Since Charles actually doesnt have any money, this proves awkward. However, Jeffs usual backer, Tex Henry (Harry von Zell), shows up. Tex and Charles draw cards to see who will get to put up the money; Tex also makes a side bet of $10,000. Fortunately for Charles, Tex wins.

All this starts to corrupt Charles. He begins to enjoy human vices. When Daphnes former actor boyfriend, Tony Clark (Jack La Rue), shows up to reclaim his uninterested girlfriend, Charles  punches him. Charles also starts drinking and playing modern music (on his harp), much to Arthurs disapproval.

Still, Charles has not completely forgotten his mission. He arranges a lavish party to celebrate the Boltons eighth anniversary, but that does not work as planned. The Boltons decide to break up, and Charles is taken to the mental hospital when he admits that he is an angel. Luckily, when Lydia develops a sudden craving for peanuts, when she could never before even stand the smell of them, Jeff realizes that she is pregnant (with Item), and they reconcile.

==Cast==
* Clifton Webb as Charles / "Slim" Charles
* Joan Bennett as Lydia Bolton
* Robert Cummings as Jeff Bolton
* Edmund Gwenn as Arthur
* Joan Blondell as Daphne Peters
* Gigi Perreau as Item
* Jack La Rue as Tony Clark
* Harry von Zell as Tex Henry
* Tommy Rettig as Joe Blake

==Release==
In his 1950 New York Times review, critic Bosley Crowther wrote that "Mr. Webb, need we say, is an actor with an urbane sense of the grotesque and a thoroughly cultivated talent for farcical mimicry. So his broad travesty of a rancher from Gods country, whose particular line is sheep and whose weakness is wine and women, is very amusing to see. But we have to advise that the whimsies with which this picture begins and in which it dissolves at the climax are far on the sticky side — the sort of stuff that may seem poignant if youre a softie, but nauseous if youre not." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 