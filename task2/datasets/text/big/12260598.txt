Ice Age: Dawn of the Dinosaurs
 
 
{{Infobox film
| name           = Ice Age: Dawn of the Dinosaurs
| image          = Ice Age Dawn of the Dinosaurs theatrical poster.jpg
| caption        = Theatrical release poster
| director       = Carlos Saldanha  Mike Thurmeier (co-director)
| producer       = Lori Forte John C. Donkin
| screenplay     = Michael Berg Peter Ackerman Mike Reiss Yoni Brenner
| story          = Jason Carter Eaton
| starring       = {{plainlist | 
* Ray Romano
* John Leguizamo
* Denis Leary
* Queen Latifah }} John Powell
| editing        = Harry Hitner
| studio         = Blue Sky Studios 20th Century Fox Animation
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $90 million
| gross          = $886.7 million    
}} comedy adventure Ice Age series. It was produced by Blue Sky Studios  and distributed by 20th Century Fox. The film features the voices of Ray Romano, John Leguizamo, Denis Leary, Queen Latifah, Seann William Scott, Josh Peck, Simon Pegg, and Chris Wedge.

The story has  .

Despite receiving mixed reviews from critics, Dawn of the Dinosaurs ranked at the time as the   was released in 2012.

==Plot==
Ellie (Queen Latifah) and Manny (Ray Romano) are expecting their first child, and Manny is nervously obsessed with making life perfect and safe for Ellie, until the babys born, since his first experiences as a husband and father went bad when his family was killed by hunters. At the same time, Diego (Denis Leary) finds himself unable to catch a cocky gazelle (Bill Hader) he has been stalking and decides to leave the herd, believing that he is losing his predatory nature as a tiger. Sid (John Leguizamo) grows jealous of Manny and Ellie and "adopts" three apparently abandoned eggs that he finds in an icy underground cavern and call them Eggbert, Shelly, and Yoko. Manny tells him to put them back saying he would never make a good parent, but Sid instead looks after the eggs, which hatch into baby Tyrannosaurus triplets the next morning.

Although Sid tries his best to raise the three dinosaurs, their rambunctious behavior scares away all the other animals young and ruins a playground Manny built for Ellies baby, and well as the ice mobile Manny made. A female Tyrannosaurus, Momma, whose eggs Sid stole, soon returns and carries both Sid and her young underground, with Diego in pursuit. Manny, Ellie, Crash, and Eddie (Seann William Scott, Josh Peck) follow as well and discover that the icy cavern leads to a vast jungle populated by dinosaurs thought to be extinct. Here, an Ankylosaurus threatens the herd despite Diegos efforts to fend it off; they are saved from a further crowd of angry dinosaurs by an insane, one-eyed weasel named Buck (Simon Pegg).
 laughing gas, causing anyone who breathes in it to laugh uncontrollably while speaking in a high-pitched voice). Although the gas is not the actual cause of death, victims usually cannot stop laughing and thus die while trying to cross the chasm. Eventually the group manages to cross the chasm. In the meantime, Sid and Momma try to outdo each other in feeding the offspring; he loses this contest, but is soon welcomed into the family regardless. The next day, however, Sid is separated from the family and attacked by Rudy. Sid is knocked onto a loose rock slab that is floating on a river of lava and about to plummet over the falls.
 the fruit (and the codeword they had chosen for Ellie to use if she went into labor during the trip). Sid is saddened at the fact that he never had a chance to say goodbye to "his" children as he returns to the herd and learns of Peaches birth.

As they venture back to the tunnel, they are shocked to discover Rudy lurking inside of the entrance. Rudy exits the tunnel and attacks at full force; Buck lures Rudy away from the group and is nearly eaten himself, before Diego saves him at the last second. Manny, Sid, Diego, and Buck manage to ensnare Rudy and knock him unconscious, but as they begin to leave, Sid trips over one of the ropes and breaks it. Rudy quickly recovers and escapes, and is about to attack Sid when Momma arrives on the scene, charging at Rudy and knocking him off a cliff before roaring her victory. As she and her children wish Sid well, Buck – now without a purpose in life since Rudy is gone – decides to join the herd and live on the surface. However, a distant roar tells him that Rudy is still alive; he changes his mind and sends the herd home, blocking off the path to the underground jungle at the same time, so nobody else can go down there anymore. Manny and Ellie welcome Peaches into their frozen world and admit that Sid did a good job looking after Mommas children (though Manny tells Diego that he will never let Sid babysit Peaches). Diego decides to remain with the herd, while Buck stays where he wants to be: underground, battling it out with Rudy.

===Scrat and Scratte===
Like the previous Ice Age (franchise)|Ice Age films, the film opens with the saber-toothed squirrel Scrat (Chris Wedge), who does everything he can to retrieve his precious acorn. This time, he falls in love with his female counterpart Scratte (Karen Disher), with the song "Youll Never Find Another Love Like Mine" by Lou Rawls playing in the background, but an acorn appears instead after the music stops. The two fight over the acorn with Scratte is flung off the ground. As Scrat tries to save her, the two hold onto the acorn and the music starts back. Suddenly, she demonstrates her ability to glide like a flying squirrel, leaving Scrat falling to the ground. The two eventually fall in love after Scrat saves her from falling to her death in the lava river while she is unconsciousness|unconscious. He even chooses to focus his attention on her instead of the acorn until the end of the film, when he tires of her finicky nature and reverts to his old ways. The ensuing fight between the couple leads to a volcanic explosion (caused by Scratte, due to slamming the acorn into the ground like Scrat had done before, though he tried to warn her of what would happen if she did) that hurls Scrat and the acorn back to the surface, leaving Scratte trapped underground. However, as Scrat is about to enjoy his acorn, a stray piece of ice falls on him and knocks the acorn back into Scrattes hands. He screams in frustration, having lost both his acorn and Scratte.

==Voice Cast==
 
* Ray Romano as Manny
* Denis Leary as Diego
* John Leguizamo as Sid
* Queen Latifah as Ellie
* Seann William Scott as Crash
* Josh Peck as Eddie
* Simon Pegg as Buck
* Chris Wedge as Scrat
* Karen Disher as Scratte
* Bill Hader as Gazelle
* Joey King as Beaver Girl
* Jane Lynch as Diatryma Mom
* Kristen Wiig as Pudgy Beaver Mom
* Carlos Saldanha as Dinosaur Babies / Flightless Bird
* Eunice Cho as Madison (Diatryma Girl)
* Maile Flanagan as Aardvark Mom
* Clea Lewis as Start Mom
* Devika Parikh (additional voices)

==Production==
Blue Sky decided to do "more of a what-if adventure" in the third Ice Age installment, titled Ice Age: A New Beginning, "like finding the giant ape in King Kong or a Shangri-la in the middle of snow," and added the dinosaurs to the story. Character designer Peter de Sève welcomed the new plot addition, since he could not think of any other giant mammal to put into the story. The "lost world" approach led to colorful dinosaurs, because "the dinosaurs didnt have to be just brown, and you can take liberties because no one knows what color they were", according to de Sève. Rudys design was inspired by the Baryonyx because of his crocodile-like look, which de Sève considered even more menacing than the T. rex. 

The film was released in RealD Cinema where available. The release sparked some controversy when Fox announced that it would no longer pay to supply 3D glasses to theaters,  which led to a number of exhibitors threatening to only show the film in standard 2D projection. 
 Horton Hears a Who! on March 14, 2008, then online on April 7, 2008.  There are three others that have been released, with the third and fourth (which shows Buck) being the most closely resembling each other. Queen Latifah recorded a cover of the song "Walk the Dinosaur".

==Reception==
===Box office===
The film earned $196,573,705 in North America and $690,113,112 in other territories, which gives it a worldwide gross of $886,686,817 against a budget of $90 million. Worldwide, it is the   ($170.9 million). It marks the highest grossing film of the   for 2009 (after  . 

;North America
The film made $13,791,157 on its opening day in 4,099 theaters.  It reached $41,690,382 on its first weekend, marking the least-grossing first weekend for the franchise, although it had a Wednesday release and therefore burned off attendance until the weekend.     The film became 20th Century Foxs third largest 2009 release in North America behind Avatar and  . It is the third-highest-grossing animated feature of 2009. It heavily out-grossed its predecessor,   which earned $195,330,621 three years before,  to become the highest-grossing movie in the franchise, but it was way behind the two first Ice Age movies in estimated attendance.   

;Other territories
On its opening weekend it earned $151.7 million, which is the biggest opening for an animated feature  and the 13th largest of all time.  Outside North America, it is the fifteenth highest-grossing film of all time  and the second highest-grossing animated movie of all time (out-grossing Finding Nemo, later out-grossed by Frozen (2013 film)|Frozen).  Its highest-grossing market after North America was Germany ($82.2 million), followed by France and the Maghreb region ($69.2 million), and the UK, Ireland and Malta ($56.9 million).  It was the highest-grossing animated film of the year in all major countries, except Spain  and Australia.  

As of March 2012 it is the highest-grossing animated film of all time in Hungary,  Slovakia,  the Czech Republic,  Romania,  Bulgaria, where the film holds the opening-weekend record,  Finland,  Norway,  Denmark,  Estonia,  Latvia,  Lithuania,  Italy,  Greece,  Serbia and Montenegro,  Slovenia,  Croatia,  France and the Maghreb region,  the Netherlands,  Germany,  Austria,  Brazil,  Peru,  Uruguay  and Venezuela. 

===Critical response===
The film received mixed reviews from critics. As of June 27, 2011, Rotten Tomatoes reported that 45% of critics gave positive reviews based on 157 reviews with an average score of 5.4/10.  Another review aggregator, Metacritic, which assigns a normalized rating out of 100 top reviews from mainstream critics, gives the film an average score of 50 based on 25 reviews. 

However,   awarded the film 3 stars stating that the film is "much more of an emphasis on action in this nicely crafted, fast-paced sequel."  Keith Phipps of the A.V. Club graded the film a C+ claiming the sequel "throws its commitment to the era away with movie number three, a ploy sure to anger Ice Age purists everywhere."  Carrie Rickey of the Philadelphia Inquirer enjoyed the "films animation art is Seuss-imaginative", but panned "the flatness of the story and indifferent voicework all the more obvious." 

CinemaScore polls conducted during the opening weekend, cinema audiences gave Dawn of the Dinosaurs an average grade of "A-" on an A+ to F scale.   

===Home media===
Ice Age: Dawn of the Dinosaurs was released on High-definition video|high-definition Blu-ray Disc and standard DVD in North America on October 27, 2009 and in the United Kingdom on November 23, 2009. Two versions of the DVD exist: a single-disc DVD, and a "Scrat Pack" Double DVD Pack with three Scrat games.

The 3-disc Blu-ray combo pack includes a Blu-ray, the single-disc DVD, and a Digital Copy, as well as an Ice Age digital story book maker, commentary by director   and  ), and a how-to-draw Scrat tutorial with the filmmakers.

On September 21, 2010, a 3D DVD was released as a two-disc set, with the first disc being the TrioScopics 3D version and the second disc being the 2D version.

==Ice Age: Dawn of the Dinosaurs - The 4-D Experience== Roxy Theatre, at the Warner Bros. Movie World in Australia,  and since July 2012, at the Shedd Aquariums Phelps Auditorium in Chicago. 

==Video game==
 
A video game tie-in was published by Activision on June 30, 2009. 

==Sequel==
 
The fourth film, Ice Age: Continental Drift, was released in 3-D on July 13, 2012. It was directed by Steve Martino and Mike Thurmeier — the first time without Carlos Saldanha. The film takes place a few years after the events of the third film, with Peaches in her teenage years. Scrats never-ending pursuit of acorns has world-changing consequences, separating Manny, Sid and Diego from the rest, forcing them to stand up to a pirate gang, led by Captain Gutt.

==See also==
* List of animated feature-length films
* List of computer-animated films

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 