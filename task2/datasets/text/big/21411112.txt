Eden Log
{{Infobox Film
| image = Eden Log.png
| caption = Theatrical release poster
| director = Franck Vestiel
| producer = Cédric Jimenez
| writer = Franck Vestiel Pierre Bordage
| starring = Clovis Cornillac Vimala Pons
| music = Seppuku Paradigm
| cinematography = Thierry Pouget
| editing = Nicolas Sarkissian
| studio = Impéria Films
| distributor = Bac Films (France) Magnolia Pictures (United States)
| released =   
| runtime = 101 minutes
| country = France
| language = French
}} science fiction horror film directed and co-written by Franck Vestiel. The film was Vestiels first as a director, who shot the entire film using only hand-held cameras.

Reviews towards the film were mixed, which received an aggregated score of 43% from Rotten Tomatoes. In North America, it premiered at the Toronto International Film Festival on September 11, 2008.

== Synopsis  ==
A man wakes up deep inside a cave. Suffering amnesia, he has no recollection of how he came to be here nor of what happened to the man whose body he finds beside him. Tailed by a mysterious creature, he must continue through this strange and fantastic world. Enclosed, Tolbiac has no other option to reach the surface than to use Rezo Zero, secret observing cells in this cemetery-like abandoned mine. He embarks upon this journey guided by the roots of a plant, leading the way and the main subject of attention of the Rezo.

This is the story of the rise of a forgotten man, sometimes informed by the archives of abandoned laboratories, which punctuate this subterranean network and at other times by his own strangely amplified instinct. During his search for the surface, Tolbiac must uncover the secret of what once lived in this network; but has this “mystery” truly left?

== Plot (full) ==
   
In an underground cave, Tolbiac (Cornillac) wakes up disoriented, not knowing who he is, or how he got there. A dead man lies next to him.

As he stumbles around in the limited light, cold, and darkness, he comes upon a dilapidated area with damaged pieces of technology all around him. While he is trying to get a damaged panel to work, suddenly, digital phantoms appear and inform him of a paradise known as Eden Log. They play a generic welcome message for the workers, where Tolbiac learns that workers are volunteers making sacrifices with the hope of gaining citizenship, presumably for the society above ground, and that Eden Log can offer a passport. Tolbiac passes through the main entryway, but remembers nothing, even after examining several crude maps and some damaged equipment.

A second message plays, reasserting the fairness of the workers contract, though the details lack context. The workers will enter the "cycle," and by working below can contribute to the world above through a fair exchange. Their job is described as "looking after the plant," and they are promised that if they do so, the plant will "look after them." The workers message reiterates that the workers only reward is found in their drive to become part of the new society.

With no memory of recent events, or even his own name, Tolbiac begins scrounging for supplies and looking for a way out. After recovering some clothes and a harness with mounted lights, he sets out for an exit. After some wandering, he comes upon a bearded man who seems pinned against a wall by the same plant-like vines that protrude from the technology and along the tunnels. The man, clearly in great pain and mentally addled from the experience, only identifies himself as the "architect" and reveals that the plant-like vines have invaded his body and are slowly taking over. As Tolbiac approaches, the man warns him away to keep him from danger but also because something about Tolbiac makes the vines more aggressive. Just as Tolbiac is about to leave completely, he hears a loud, growling noise that seems to be approaching. The architect warns Tolbiac that the noise is "your end" and that he should kill himself immediately. When Tolbiac refuses, the architect warns him to run, but Tolbiac cannot as the noise has grown so loud that he loses consciousness.

When Tolbiac wakes up, he is in a new, yet still unfamiliar area with no sign of the Architect. As he crawls through a passage to find an exit, he quickly finds himself enclosed in a cube on a conveyor. The machinery stops suddenly, colliding Tolbiacs cube with the one ahead of him, and through the translucent glass, he sees a dead body in the next cube. Panicked, Tolbiac frees himself by swinging the cube off the hinge and crashing to the floor beneath, though this knocks him unconscious again.

This time, he finds himself on a slightly better-lit area with his first major landmark indicating that he is on Level -4. While exploring, he finds a dead technician with his hand in a palm-recognition device. After tinkering with the technology, Tolbiac activates the last recording. In it, the dead scientist apparently had an argument with the Eden Log personnel above where he refused to open up access to the "rezo" to security. It becomes clear that the levels of the plantation are sharply divided and massive. What becomes clear is that due to deteriorating and increasingly dangerous conditions in the plant, a large workers revolt was in progress, and the dead technician was instrumental in cutting off security (located on the upper levels) from the lower ones. The revolting workers demanded to know the condition of the former workers allowed to join the society on the surface, which the company refused. Prior to the security forces breaking into the lab, the technician reveals that there is a record of this information, and unless Eden Log complies, they will release it to the society above. The recording ends with the security forces breaking in and requesting additional orders on containing the situation from their superiors. Their orders were to find the architect, secure the remaining technician on Level -3 to prevent any further broadcasts, and recover the data from the labs.

Tolbiac checks the dead technicians palm activation device, but the system does not recognize him. Nevertheless, he pulls the data chip out of the device and proceeds to try finding an exit. After some maneuvering in the tunnels, Tolbiac comes up to Level -3. While avoiding the security forces, he overhears their discussion as they search for the remaining technician and the architect. The security forces have also chained up what looks like a mutated human, and they hint that a significant amount of the worker population have also changed into mutants. During a brief scuffle with the security forces, Tolbiac makes a break for the tunnels and fights off their chained mutant, climbing up a narrow passage to Level -2.

On Level -2, Tolbiac hears a strange, ambient music playing in a chamber surrounding a well-lit, but small, laboratory. Suddenly ambushed on all sides by the mutants in the dark, he is rescued by a figure in a protective suit (Vimala Pons) who uses a bright light and loud noise to fend off the mutants. Unfortunately, the light and noise are equally painful to Tolbiac. Capturing him, she uses a gas to knock him out. When he wakes up, she reveals that she is the only remaining Botanist on Level -2, who has survived by remaining isolated and careful. She initially believes Tolbiac to be infected, and progressing in mutation, because he refuses to speak. Once he does, she explains that the "rezo" is the term used in the plant to describe the area in which the workers operate as well as the workers themselves. Eden Log has been using a central living plant, to harvest power. The workers are part of this because the plants sap has energetic properties, giving the workers unusual stamina and strength. However, the more sap that theyve taken from the plant, the more toxic and hostile it became. The mutants are the workers who had been corrupted by this sap, and the botanist is both confused and amazed that Tolbiac, an apparent worker, has somehow managed to escape its effects. Seeing his confusion, she conducts a brief test to learn more about him. She connects him to the plant with a simple exam in mind, if he is healthy, the plant will try to infect him (though she says that it is more like the plant is trying to make contact), and theyll both know that he is infection-free. Better still, the botanist hopes to find out what the plant wants from the humans. During the transfusion, something goes wrong, and Tolbiac seems to be able to force something back into the plant, causing the seemingly sterile plant to sprout and become lush with growth and fruit. Surprised and scared, the Botanist disconnects him from the plant.

Shortly after, the mutants begin an assault against the botanists lab, and Tolbiac is tricked and abandoned as bait. When one of the mutants manages to get inside, Tolbiac manages to tie it up and escape. Outside, he catches up with the botanist and fights off several mutants attacking her with increased primal rage. Having established dominance, the remaining mutants back away long enough for the two to run to a working elevator.

Inside, Tolbiac is overcome by primal urges for the first time, having flashbacks of his blackout with the Architect and realizes that he had killed him. In this state, he feels part of his humanity slip away in a roar, only to calm down. He then begins to act on the lust he feels for the botanist, first acting tenderly, then brutally as he makes sexual advances on her. When they finish, Tolbiac realizes that his mental flashes during sex that depicted him brutally raping the botanist were not just in his head. While they had started with attraction, in his fugue state he raped her. Under control of himself again, Tolbiac weakly tries to tell her that he was not himself, and she reluctantly goes with him when the elevator breaks down.

Together, the two traverse several tunnels, though they remain wary of one another. Eventually, they stumble upon the location of the hidden technician on Level -2, who managed to halt his mutation at the cost of his life. Like the Architect, he underwent a critical merging with the plant in order to keep himself alive. He reveals with more detail what the botanist suspected. In addition, he confirms the ugly truth: the plant workers are never sent to the surface, they are simply exploited by Eden Log. More creatures attack, but Tolbiac manages to fight them off and protect the botanist. The two then approach the exit level, Level -1, which is where the sap is collected. Here, Tolbiac realizes fully that the term "plant" is a double meaning as the whole facility is, in fact, a power plant which uses a biological plant as its power supply.

The two proceed upward quickly through the sap collector, since the tree (and the surface), are above it. Tolbiac and the Botanist suddenly find themselves in the middle of a skirmish between mutants, who are following them from below, and the security forces who are descending. The Botanist spots the cubes which convey humans up to the tree and, without explanation, runs away from Tolbiac towards the underground levels. When he catches up to her, she states her recent revelation to the disoriented Tolbiac; the technicians discovered that the workers, who became ill and infected from using the sap in the rezo, were sent to the surface in cubes to be "cured" by the tree, but this was a lie because the energy in the sap was actually being taken from the humans in the cubes.

Immediately after, she begins convulsing and hearing a noise that Tolbiac does not, and he realizes that he has been infected all along (though somehow has managed to keep his symptoms in check).  When he raped her, he passed the contamination onto her. He apologizes, but she is too heartbroken to have come so close and survived so long to be infected now. She savagely attacks Tolbiac, but runs away before he is similarly overcome with the infections rage. Before she breaks for the tunnels, she tosses him a final data chip with recordings from before the rebellion.

Proceeding upward, Tolbiac emerges in a chamber that for the first time, he recognizes. Dodging another scuffle of security forces and mutants, he sneaks into the surveillance room and begins watching the compiled data clips and piecing all the information together. For the first time, he sees all the messages on full screens and can make out all the details. While watching all the clips, Tolbiac slowly realizes what his true identity is. He was the former captain of the guard, trusted with the full secrets of Eden Log, and asked to quell the rebellion and keep the information from getting out. With another guard, the two descended to the lowest levels where Tolbiac became contaminated and, in a rage, had killed his companion. Instead of mutating, he awoke with no memory, which is where his journey began.

When he realizes this, the computer simultaneously confirms his identity as Captain Tolbiac and his completely successful mission of having killed the architect, recovered all the lost data of the technicians, and having put down the mutants, which the guards have just accomplished working under the orders he issued before going underground. As the guards burst into the surveillance office, they immediately stop and recognize him as Captain Tolbiac.

On the surface, Tolbiac is saluted by Eden Log for a successful mission and for protecting the companys secrets as he stands in front of an animated board depicting the full process of the power harvesting procedure. Workers are sent to the lowest levels, given sap to energize them, then work until they are depleted or dead as their energy is harvested to power the city above. Once they succumb, their bodies are placed in the cubes and used to feed the tree, providing the sap for future workers.

Realizing the destruction of the trees life cycle, and now comprehending what the plant was communicating, Tolbiac knows that the Eden Log trees cycle needs to return to a natural process rather than feeding off of humans, though human beings will no longer be able to use the trees life cycle to power their cities. Using his authority, he walks into the trees main chamber and plugs himself in. Like before in the botanists lab, he causes the tree to demonstrate explosive growth, which destroys the power plant and causes a full blackout. The trees growth overruns the city where it covers everything in a lush green coating. In the final scene, the perspective focuses directly on Tolbiacs face as a single tear falls from his eye.

==Cast==
*Clovis Cornillac as Tolbiac
*Vimala Pons as Last botanist
*Zohar Wexler as Technician
*Sifan Shao as Technician
*Arben Bajraktaraj as Technician
*Abdel Kader Dahou as Guard (as Abdelkader Dahou)
*Tony Amoni as Guard
*Antonin Bastian as Guard
*Joachim Staaf as Guard
*Benjamin Baroche as Guard
*Zakariya Gouram as Guard
*Gabriella Wright
*Asha Sumputh
*Nadia-Layla Bettache
*Lavinia Birladeanu
*Mariella Tiemann
*Nadia Fina
*Alexandra Ansidei
*Liou Chou
*Olivier Dupuy

==Production== Frank Millers Daredevil (Marvel Comics)|Daredevil and Métal Hurlant.     

==Release==
The film first appeared in North America  at the Toronto International Film Festival, where it aired on 11 September and 13 September 2008. 

==Festivals==
* Grand Prix International Festival of Fantasy Film São Paulo
* Official Selection Toronto International Film Festival 2008 Midnight Madness
* Official Selection Sitges Film Festival 2008
* Official Selection Austin Film Festival 2008
* Official Selection London FrightFest Film Festival 2008
* Official Selection Brussels International Fantastic Film Festival	2008
* Official Selection Fantasy Filmfest Germany 2008
* Official Selection Trieste International Science Fiction Film Festival 2008
* Official Selection Melbourne International Film Festival 2009

==Reception== Heavy Metal".  Chris Cabin of Film Critic credited the film for its first 20 minutes of attention-grabbing action, but notes that the journey that Tolbiac takes is "more suited that of a video game programmer than a young filmmaker". Cabin wrote that the film does not emotionally connect with the viewer, and he gave the film 1.5 out of 5 stars.  Variety (magazine)|Variety remarked that the film is "bold", despite rehashing some cinematic qualities and sound design, concluding that the film "the most bang for limited bucks, with an eye-popping finale".   

==References==
 

== External links ==
* Bac Films International "Eden Log" official site - http://www.bacfilms.com/international/film/38
*  
*  
*  

 
 
 
 
 
 
 
 
 