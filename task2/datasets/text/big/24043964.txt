Mid-August Lunch
{{Infobox Film name        = Mid-August Lunch image       = Poster_large1.jpg caption     = Theatrical movie poster director    = Gianni di Gregorio writer      = Gianni di Gregorio and Simone Riccardini. starring    = Gianni Di Gregorio  Valeria de Franciscis  Marina Cacciotti  and Maria Cali. Nazan Kırılmış producer    = Matteo Garrone distributor = Zeitgeist Films released    =   country     = Italy language    = Italian
}} 2008 Italy|Italian Italian writer-director 2008 film Gomorrah was co-written by Di Gregorio. It currently is being distributed in the US by Zeitgeist Films.   

==Plot==
Gianni (Gianni Di Gregorio), a broken man with mounting condo debts, is forced to entertain his 93-year-old mother and three other feisty women during Italy’s biggest summer holiday,  Ferragosto.  The other women include the mothers of his landlord and doctor, who will forgive his debts in exchange for his service.

==Awards and nominations== David Di Donatello Awards, the Satyajit Ray Award at the London Film Festival, and the Golden Snail award at the Academy of Food and Film in Bologna.

==Cast==
* Gianni Di Gregorio
* Valeria de Franciscis
* Maria Cali
* Grazia Cesarini Sforza
* Marina Cacciotti
* Alfonso Santagata

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 

 