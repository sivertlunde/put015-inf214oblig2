Pappy's Puppy
 
{{Infobox Hollywood cartoon|
| cartoon_name = Pappys Puppy
| series = Merrie Melodies (Sylvester (Looney Tunes)|Sylvester)
| director = Friz Freleng
| story_artist = Warren Foster
| animator = Gerry Chiniquy
| image=
| caption=
| layout_artist = Hawley Pratt
| background_artist = Irv Wyner
| voice_actor = Mel Blanc
| musician = Carl Stalling Edward Selzer Warner Bros. Pictures
| release_date = December 17, 1955
| color_process = Technicolor
| runtime = 7 minutes
| movie_language = English
}}

Pappys Puppy is a 1955 Merrie Melodies short starring Sylvester (Looney Tunes)|Sylvester, Butch, and his newborn son. It was animated entirely by Gerry Chiniquy. Mel Blanc provides the vocals; however, aside from whistling, Sylvester doesnt speak in this short.

==Plot Summary==
At a hospital, Butch the bulldog paces nervously, waiting for his baby to be born. He does, and Butch faints. Back at home, Butch teaches his new son how to walk, act tough, and bite cats (by practicing on a dummy). One day, the young bulldog is playing with a ball when he wanders upon Sylvester. His training kicks in and he attacks Sylvester, who proceeds to place the small bulldog under a tin can. However, Butch catches Sylvester doing this and exacts revenge by taking Sylvester to a shed and hammering the tin can on his head. Later, Sylvester is walking along while the baby bulldog is biting at his tail. He slaps the bulldog off camera, only for Butch to catch up to him and wallop Sylvester in retaliation. After that, Sylvester reluctantly plays fetch with the baby bulldog and decides to throw the stick into a busy street, hoping the bulldog will be flattened. However, the young bulldog retrieves it successfully. Annoyed, Sylvester prepares to throw it again but Butch sees this and throws the stick into traffic himself, pointing for Sylvester to fetch the stick this time. Sylvester successfully retrieves it amidst heavy traffic, but is run over by a man on a scooter on the sidewalk anyway. Once again, Sylvester plays fetch with the young bulldog again and throws a ball into a doghouse, which Sylvester boards up when the bulldog chases the ball into it. Sylvester plans to drop a lit stick of dynamite into the open hole on the top. However, Butch once again catches Sylvester and places Sylvester over the doghouse instead; Sylvester doesnt hammer out the nails in the boards in time and explodes. In the final set piece of the cartoon, Sylvester sets up a booby trap of a dog bone hooked up to a shotgun. When Butchs son begins tugging on the bone, Butch gives Sylvester a stern look, prompting Sylvester to run over and plug the shotgun hole with his finger so the young bulldog is unharmed, getting his finger shot over and over (including once in the face) as Butchs son tugs. The stork arrives and announces Butch has even more puppies to add to his family. The cartoon ends as Sylvester chases the stork, shooting at him.

==Trivia==
The soundtrack to this cartoon (minus the title card music) can be heard on "The   Cartoons 1939–1957", free of sound effects and voices.

==External links==
*  

 
 
 
 
 
 


 