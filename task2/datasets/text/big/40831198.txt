The Christmas Candle
 
 
{{Infobox film
| name = The Christmas Candle
| image = The Christmas Candle film poster.jpg
| alt =
| caption = John Stephenson
| producer = Tom Newman Hannah Leader Steve Christian Ivan Dunleavy Huw Penallt Jones Brian Lockhart  
| writer = Candace Lee Eric Newman Max Lucado  John Hannah
| music =
| cinematography =
| editing =
| studio = Pinewood Studios
| distributor = Pinewood Studios   EchoLight Studios  
| released =  
| runtime =
| country = United Kingdom United States
| language = English
| budget =
| gross = $2,024,910 
}}
The Christmas Candle is a 2013 British/American Christmas film.

==Introduction==
It is based on  , 9/25/13  It is Susan Boyles debut on the big screen.     Boyle also contributes an original song to the film, "Miracle Hymn". 

It was shot in Gloucestershire, Worcestershire and in the Isle of Man.   

==Summary==
In the fictional village of Gladbury, every twenty-five years an angel visits the candlemaker and bestows a miracle upon whomever lights the Christmas Candle. However, shortly after the arrival of a new pastor, David Richmond in 1890, the Christmas Candle goes missing.

==Theology== Roman Catholic theology, where the main character is struggling to find his lost faith.   

==Cast==
*Hans Matheson as David Richmond
*Samantha Barks as Emily Barstow
*Lesley Manville as Bea Haddington
*Sylvester McCoy as Edward Haddington
*James Cosmo as Herbert Hopewell
*Susan Boyle as Eleanor Hopewell
*Barbara Flynn as Lady Camdon John Hannah as William Barstow
*Jude Wright as Charlie
*Emily Shewell as Orphan Sarah

==Reception== New York Daily News describing it as a "Dickens-meets-Sunday-school movie", and that it was "as artless as the setup   muddled".  The New York Post referred to it as a "throwback, made-for-TV-style film" with a "cheesy climax". The Arizona Republic judged it as "resolutely stiff and hollow".   
 The Portsmouth News gave the film 4 stars writing "Boyles performance is endearing and her stunning vocal talent continues to dazzle while the gentle chemistry between Matheson and Barks complements the piece without upstaging the films central ideas."  The Los Angeles Times wrote, "Hammy histrionics of a Hallmark movie are present, but its message of community and faith shines brighter."  The Dove Foundation awarded 5 Doves as a "Family-Approved" film. 

The film expanded from five to 390 venues for its second week. 

==References==
 

==External links==
*  
*  
*   on Facebook
*   on Twitter
*   on Twitter

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 