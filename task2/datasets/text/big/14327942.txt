Elves (film)
{{Infobox Film
| name        = Elves
| image       = 
| caption     = 
| director    = Jeffrey Mandel
| producer    = Mark Paglia
| writer      = Jeffrey Mandel
| starring    = Dan Haggerty Deanna Lund Ken Carpenter
| music       = Vladimir Horunzhy
| cinematography= Kenny Carmack
| editing     = Tom Matthies
| studio      = Action International Pictures AIP Home Video
| released    = October 24, 1989
| runtime     = 89 min
| country     = USA English
| budget      = 
| preceded_by = 
| followed_by = 
}} American horror film directed by Jeffrey Mandel and starring Dan Haggerty, Deanna Lund and Ken Carpenter. 

==Plot==
When teenager Kirsten (Julie Austin) accidentally cuts her hand during an "Anti-Christmas" pagan ritual with her friends Brooke and Amy in the woods, her spilled blood awakens an ancient demonic Christmas elf. The elf is the central figure in a modern-day Neo-Nazi plot to finally bring about the master race that Hitler had always dreamed of conquering the world with. Rather than a race of pure-blood Aryans, it is revealed that Hitler instead dreamed of a race of half-human/half-elf hybrids (it is also revealed that elves figured heavily into a pseudo-cult religion that the Nazis practiced in secret). Kirsten is also a figure in this plot as she is the last remaining pure-blooded Aryan virgin in the world, her grandfather being a former Nazi who was once involved in the plot (but is now reformed); he is also her father, as inbreeding was somehow considered crucial to maintaining a pure Aryan bloodline. Unaware of all these sinister goings-on, the non-festive Kirsten continues to sulk her way through the Christmas season as she works at the snack counter of a local department store.

Mike McGavin (Dan Haggerty) is an ex-cop who lost his badge when he lost control of his alcoholism. Jobless, penniless, and recently served a notice of eviction from his ramshackle trailer home, Mike turns to his old friend – the manager of the department store – for help, and winds up becoming the store Santa after the prior Santa is murdered by the elf. Without a proper home, Mike sneaks into the store at night to sleep in the storage room and live off the snack counter left-overs. One night, he hears Kirsten and her friends, who have also sneaked in, frolicking through the store as they wait for their boyfriends to show up for an all-night party. The shadowy Nazi group arrives instead, planning to kidnap Kirsten and find the elf so the master race can finally be made reality. With Mikes help, Kirsten escapes with her life, though her friends are not so lucky. Promptly fired for breaking into the store after hours, Mike and Kirsten are able to devote their time to unraveling the plot. After making a Christmas Eve visit to the local college library and later breaking into a professors home to demand information, Mike realizes what is afoot and sets out to protect Kirsten. Mike, Kirsten and her grandfather have a final climactic showdown with the Nazis and the elf in Kirstens home, culminating in the woods where Kirsten destroys the elf by performing a ritual involving an "elfstone" from her grandfathers study. The following morning, Kirsten huddles in the now-inexplicably destroyed forest as it begins to snow for the first time that winter. The film ends on the image of a fetus, suggesting perhaps that the plot was successful despite the elfs seeming inability to actually copulate with Kirsten before its demise.

==Cast==
* Dan Haggerty as Mike McGavin
* Julie Austin as Kirsten
* Deanna Lund as Kirstens mother
* Borah Silver as Kirstens grandfather
* Mansell Rivers-Bland as Rubinkraur
* Christopher Graham as Willy
* Laura Lichstein as Brooke
* Stacey Dye as Amy
* Winter Monk as Kurt
* Jeff Austin as Emil
* Allen Lee as Dr. Fitzgerald
* Paul Rohrer as Prof. OConner
* Ken Carpenter as Shaver
* Michael Tatlock as Hugh Reed
* Michael Herst	as Sgt. DeSoto
* Chris Hamner as Kevin
* D.L. Walker as Dave 
* James Albert as Mark
* Douglas K. Grimm as Dead Santa
* Bob Holland as Wino
* Lisa Crisp as Salesgirl #1
* Kamey Pignotti as Salesgirl #2
* Sarah Serby as Girl on Santas lap
* Kenneth Graham as Boy on Santas lap
* Jodi Blunt as Santas pretty helper
* Richard Luna as Bell ringer
* Sheila Allard	as Librarian
* Janet Fikany as Wet Body Double
* Heidi Morrow as Bitchy coed
* Deborah Aspinwall as OConnors wife
* Jessika Stratton as OConnors daughter #1
* Laura Reinsch	as OConnors daughter #2
* Marie Bishop as Rosa
* Denise Strong	as Amy under wraps
* Don Dowling as Detective #1
* Chris Begley as Detective #2

==Production== Colorado Springs in Colorado, USA. 

==Soundtrack==
The score was composed by Ukrainian jazz composer Vladimir Horunzhy. 

==Release== rated R AIP Home Video.  The film is currently unavailable on DVD.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 