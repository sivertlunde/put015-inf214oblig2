Militia (film)
{{Infobox film
| name           = Militia
| director       = Jim Wynorski
| producer       = Paul Hertzberg
| writer         = Steve Latshaw William Carson
| starring       = Dean Cain Jennifer Beals Frederic Forrest Stacy Keach
| music          = Neal Acree Rick Chadock
| cinematography = Mario DAyala
| editing        = William Daniels
| studio         = Avalanche Entertainment
| released       =  
| runtime        = 89 min
| country        = United States English
}} 2000 direct-to-video action film directed by Jim Wynorski. It stars Dean Cain, Jennifer Beals and Frederic Forrest.

==Plot== ATF agents against a militia group, the Brotherhood of Liberty. Their leader, William Fain, is captured and his family is mistakenly killed in the raid. Seven years later, other persons from the same militia group break into a chemicals corporation called Cyberdyne Systems and steal anthrax. Their plan is to use this anthrax to engineer a full-blown attack against the US government. Ethan must now use the imprisoned William to infiltrate the Brotherhood of Liberty and stop the attack against the government.

==Cast== ATF Agent Ethan Carter
*Jennifer Beals as Julie Sanders
*Frederic Forrest as William Fain
*Stacy Keach as George Armstrong Montgomery John Beck as Dep. Dir. Anderson Brett Butler as Bobbi
*Christopher Maleki as Staley

==Terminator 2 footage==
Action scenes involving stealing the anthrax and subsequent destruction of the building were taken directly from  . Not much editing has been done to conceal this matter, as Arnold Schwarzenegger is once clearly visible driving an armored van. Moreover, the Cyberdyne Systems name is well known to Terminator fans.
Another scene in the movie involving a helicopter were taken out of  .

==External links==
* 

 
 
 
 
 
 

 