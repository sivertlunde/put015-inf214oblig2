Ludwig van (film)
 
Ludwig van (full title: Ludwig van: A report;    ), Robert Filliou, and Joseph Beuys were involved in the design.  According to Gramophone (magazine)|Gramophone, "at first it’s a laugh a minute ... then Kagels film turns dark". 

==Summary== Karajan is beautiful sound Waldstein sonata Ninth Symphony.  

==Analysis== deconstructive analysis nationalist agendas; anxieties of bourgeois ideals; and the difficulties of peering through the myths to catch a glimpse of the "real" Beethoven.  Kagel uses the term Musealisierung or "Museum|musealisation" in speaking of the Beethoven cult, the term used by Theodor W. Adorno to indicate that "museums are the family sepulchres of works of art". 

==References==
 

==External links==
*   (pages 46–88)

 
 
 