The Ghost (1982 film)
 
{{Infobox film
| name           = The Ghost
| image          = 
| image size     = 
| caption        = 
| director       = Herbert Achternbusch
| producer       = Herbert Achternbusch
| writer         = Herbert Achternbusch
| starring       = Herbert Achternbusch
| music          = 
| cinematography = Jörg Schmidt-Reitwein
| editing        = Micki Joanni
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

The Ghost ( ) is a 1982 West German drama film directed by and starring Herbert Achternbusch. It was entered into the 33rd Berlin International Film Festival.   

==Cast==
* Herbert Achternbusch as Ober
* Annamirl Bierbichler as Oberin
* Werner Schroeter as Bischof
* Kurt Raab as Poli
* Dietmar Schneider as Zisti
* Josef Bierbichler as Römer / Landwirt
* Franz Baumgartner as Römer / Vertreter
* Alois Hitzenbichler as Römer / Priester
* Judit Achternbusch as Novizin
* Rut Achternbusch as Novizin
* Gabi Geist as Vertretersgattin
* Gunter Freyse as Mann
* Ann Poppel as Frau

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 