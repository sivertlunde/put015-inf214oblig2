Merry-Go-Round (2010 film)
{{Infobox film
| name           = Merry-Go-Round
| image          = MerryGoRound2010FilmPoster.jpg
| image size     =
| alt            =
| caption        = Theatrical Poster
| director       = İlksen Başarır
| producer       = 
| writer         = İlksen Başarır   Mert Fırat
| narrator       = 
| starring       = Mert Fırat   Nergis Öztürk   Zeynep Oral   Semra Çeyrekbaşı   Sercan Badur   Oğulcan Güler 
| music          = 
| cinematography = Hayk Kirakosyan
| editing        = 
| studio         =
| distributor    =
| released       =  
| runtime        = 
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          =
| preceded by    = 
| followed by    = 
}}
Merry-Go-Round ( ) is a 2010 Turkish drama film directed by İlksen Başarır, which tells the story of a family dealing with incest. The film, which is the second collaboration between writer-director İlksen Başarır and co-writer Mert Fırat following the formers award-winning directorial debut film Love in Another Language ( ), was selected for the 47th Antalya "Golden Orange" International Film Festival.   

== Plot ==
Erdem and Sevil live in a small town with their two kids, Edip and Sevgi, until they are forced to move to Istanbul to look after Sevil’s mother who has become
paralyzed. Ten years later, Edip is away at boarding school, disconnected from family life. Erdem still has the same dream: to become a successful writer...

Noticing that her daughter has suddenly become withdrawn and unhappy, Sevil begins to question some of what is going on at home, and discovers a secret behind closed doors. Erdem’s death in a traffic accident will reveal further secrets within the family. Each member of this small family is faced with truths that they will have to bear on their own for the rest of their lives.

== Release ==

=== Festival screenings ===
* 47th Antalya "Golden Orange" International Film Festival (October 9–14, 2010)
* 16th Festival on Wheels (December 3–19, 2010)   

==See also==
* Turkish films of 2010

==References==
 

 
 
 
 
 