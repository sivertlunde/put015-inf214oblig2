The Method (film)
{{Infobox film
| name           = The Method
| image          = El método2005.jpg
| caption        = 
| director       = Marcelo Piñeyro
| producer       = Gerardo Herrero Francisco Ramos
| writer         = Mateo Gil Marcelo Piñeyro
| story          = Jordi Galceran Eduardo Noriega Najwa Nimri Eduard Fernández Pablo Echarri Ernesto Alterio Natalia Verbeke Adriana Ozores Carmelo Gómez
| music          = Frédéric Bégin Phil Electric    
| cinematography = Alfredo F. Mayo
| editing        = Iván Aledo
| distributor    = Palm Pictures
| released       =  
| runtime        = 115 minutes
| country        = Argentina Spain Italy Spanish English English French French
| budget         = 
}}
The Method ( ) is a 2005 thriller film based on the play El mètode Grönholm by Jordi Galceran and directed by Marcelo Piñeyro.
 Eduardo Noriega, Najwa Nimri, Eduard Fernández, Pablo Echarri, Ernesto Alterio, Natalia Verbeke, Adriana Ozores and Carmelo Gómez, as the only eight actors in the film.

== Synopsis ==
  roles as the location of the Dekia offices.]] Torre Europa), mostly in a closed conference room.
 posing as an applicant to evaluate the others. As the applicants are discarded, the race for the position becomes more tense and sinister.
The tests reveal the weakness of each of the characters and their attitudes to company work. In the end the "mole" will be revealed and only one of them will get the desired position.

==Cast== Eduardo Noriega as Carlos, a young well prepared executive.
*Najwa Nimri as Nieves, a young well prepared executive. 
*Eduard Fernández as Fernando, an older Spanish macho.
*Pablo Echarri as Ricardo, a former Argentine unionist.
*Ernesto Alterio as Enrique, interested in personnel selection methods.
*Natalia Verbeke as Montse, the company secretary-
*Adriana Ozores as Ana, a mature executive.
*Carmelo Gómez as Julio, who valued his ethics over loyalty to the company.

== Awards ==
The film was nominated to five Goya Awards (Best Lead Actor (Fernández), Best New Actor (Echarri), Best Editing (Aledo), Best Screenplay (Gil and Piñeyro) and Best Supporting Actor (Gómez), winning in the last two categories. It also won in the Flanders International Film Festival and was nominated for Best Film in the Mar del Plata Film Festival.

==Footnotes==
Similarities have been noted between this film and the 2009 film Exam (film)|Exam.

 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 