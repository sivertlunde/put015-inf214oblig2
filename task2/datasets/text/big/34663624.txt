Rajaparambara
{{Infobox film
| name           = Rajaparambara
| image          =
| caption        =
| director       = Dr. Balakrishnan
| producer       = MP Bhaskaran
| writer         = Dr. Balakrishnan
| screenplay     = Dr. Balakrishnan
| starring       = Jayan Jayabharathi Jose Prakash Shobha
| music          = A. T. Ummer
| cinematography = PS Nivas
| editing        = G Venkittaraman
| studio         = Balabhaskar Films
| distributor    = Balabhaskar Films
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film,  directed by Dr. Balakrishnan and produced by MP Bhaskaran. The film stars Jayan, Jayabharathi, Jose Prakash and Shobha in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Jayan
*Jayabharathi
*Jose Prakash
*Shobha Raghavan
*Kuthiravattam Pappu Reena
*Sudheer Sudheer
*Vincent Vincent
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Appan Thachethu, Bharanikkavu Sivakumar and Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Devi Nin Chiriyil || K. J. Yesudas || Appan Thachethu ||
|-
| 2 || Prapanjapathma Dalangal || K. J. Yesudas || Bharanikkavu Sivakumar ||
|-
| 3 || Snehikkaan Padhichoru || S Janaki || Bharanikkavu Sivakumar ||
|-
| 4 || Viswam Chamachum || Sujatha Mohan, Chorus || Bichu Thirumala ||
|}

==References==
 

==External links==
*  

 
 
 


 