The Magnificent Matador
{{Infobox film
| name           = The Magnificent Matador
| image          = The Magnificent Matador poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Budd Boetticher
| producer       = Edward L. Alperson
| screenplay     = Budd Boetticher Charles Lang
| starring       = Maureen OHara Anthony Quinn Manuel Rojas Richard Denning Thomas Gomez Lola Albright William Ching
| music          = Raoul Kraushaar
| cinematography = Lucien Ballard
| editing        = Richard Cahoon 
| studio         = Edward L. Alperson Productions
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Magnificent Matador is a 1955 American drama film directed by Budd Boetticher and written by Budd Boetticher and Charles Lang. The film stars Maureen OHara, Anthony Quinn, Manuel Rojas, Richard Denning, Thomas Gomez, Lola Albright, William Ching and an early appearance of Stuart Whitman. The film was released on May 24, 1955, by 20th Century Fox.  

==Plot==
 

== Cast ==
*Maureen OHara as Karen Harrison
*Anthony Quinn as Luís Santos
*Manuel Rojas as Rafael Reyes
*Richard Denning as Mark Russell
*Thomas Gomez as Don David
*Lola Albright as Mona Wilton
*William Ching as Jody Wilton  Eduardo Noriega as Miguel
*Stuart Whitman as Man in the Arena
*Lorraine Chanel as Sarita Sebastian Anthony Caruso as Emiliano
*Jesus Chucho Solorzano as himself
*Joaquín Rodríguez Cagancho as himself
*Rafael Rodríguez as himself
*Antonio Velasquez as himself
*Jorge Ranchero Aguilar as himself

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 