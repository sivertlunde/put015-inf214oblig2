Effects (film)
{{Infobox film
| name           = Effects
| image          = Effects (film).jpg
| alt            = 
| caption        = Synapse Films DVD cover
| director       = Dusty Nelson
| producer       = Pasquale Buba John Harrison  (as John S. Harrison Jr.) 
| screenplay     = Dusty Nelson
| based on       = Snuff by William H. Mooney
| starring       = Joseph Pilato  (as Joseph F. Pilato)  Susan Chapek John Harrison Bernard McKenna Debra Gordon Tom Savini
| music          = John Harrison
| cinematography = Carl Augenstein  (as Carl E. Augenstein)  Toni Semple  (as Toni Semple-Nelson) 
| editing        = Pasquale Buba
| studio         = Image Works International Harmony
| distributor    = Synapse Films (DVD)
| released       = October 2005
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $55,000 (estimated)
}}
 horror film. snuff documentary with an unwilling cast and crew.

== History ==

According to director Dusty Nelson, the film purportedly screened at the Sundance Film Festival, which at the time was known as US Film Festival.   Effects screened at Three Rivers Film Festival in 2011. 

== References ==

 

== External links ==

*  
*  

 
 
 

 