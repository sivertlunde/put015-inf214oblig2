Circulation (film)
{{Infobox film
| name           = Circulation
| image          =
| alt            =
| caption        =
| director       = Ryan Harper
| producer       = Jason Mitchell
| writer         = Ryan Harper
| starring       = {{plainlist|
* Yvonne DeLaRosa
* Sherman Koltz
}}
| music          = Michael Mouracade
| cinematography = Paul Nordin
| editing        = Jason Mitchell
| studio         = Salty Dog Studios
| distributor    = Cinema Epoch
| released       =   }}
| runtime        = 88 minutes
| country        = United States
| language       = English Spanish
| budget         =
| gross          =
}} Ryan Harper.  It stars Yvonne DeLaRosa and Sherman Koltz as residents in a purgatory-like existence where people are reincarnated as animals.

== Plot ==
Opening narration from Gene, a middle-aged American, explains that he has died, entered purgatory, and has been having strange dreams and compulsions.  In this afterlife, people slowly take on the characteristics of animals, and Genes nature is that of a spider.  As he drifts aimlessly through Baja, Mexico, where he died while on vacation, he meets a Mexican woman named Ana who can speak no English.  Ana, who is taking on the characteristics of a caterpillar, died on the way to meet her boyfriend when her abusive ex-husband attempted to kidnap her.  Because he speaks no Spanish, Gene can only understand that she needs help.  Grateful to find a purpose, Gene decides to help Ana, unaware of the danger posed by her violent ex-husband, who also died in the botched kidnapping attempt.  Along the way, Gene and Ana witness much in the way of strange behavior, and they begin to give in to their own bestial compulsions: Ana begins eating leaves, while Gene becomes obsessed with wrapping everything in web-like rope.  Although ashamed at the cannibalistic ramifications, Gene ultimately begins eating corpses he has wrapped in rope.  In the end, Gene preys upon and consumes Anas ex-husband.

== Cast ==
* Yvonne DeLaRosa as Ana
* Sherman Koltz as Gene

== Production ==
Director Ryan Harper was inspired to set the film in Baja after taking a road trip there himself.  Harper, a scientist, describes himself as not spiritual, but he wanted to explore concepts of death, reincarnation, and animal instinct.  Harper recruited the cast and crew through Craigslist and other local websites. 

== Release ==
Circulation premiered at the Another Hole in the Head Film Festival in San Francisco, California.   It was theatrically released in November 2008.  It was released on DVD on December 9, 2008.   

== Reception ==
Gary Goldstein of the Los Angeles Times called it inept and muddled.   LA Weekly wrote that its originality saves it from becoming dull due to the slow pacing.   Cheryl Eddy of the San Francisco Bay Guardian called it slow-paced but enjoyable.   Jeremy Blitz of DVD Talk rated it 2.5/5 stars and called it a "moody, hard to classify film that is often confusing, most of the time intentionally."   David Johnson of DVD Verdict called it a bizarre, original film that comes close to being too pretentious. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 