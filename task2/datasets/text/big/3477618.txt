Once Upon a Girl
 
{{Infobox Film
| name           = Once Upon a Girl
| image          = Once Upon a Girl.jpg
| caption        = Theatrical release poster.
| director       = Don Jurwich
| producer       = Don Jurwich
| writer         = Don Jurwich Hal Smith Frank Welker
| music          = Martin Slavin
| cinematography = 
| editing        = 
| studio         = Concelation a Girl, Inc. Tommy J. Productions
| distributor    = Severin Films
| released       = June 20, 1976
| runtime        = 80 min USA
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Once Upon a Girl is a 1976 X-rated live-action/animated film written, produced, and directed by Don Jurwich. It was animated by a group of animators who had worked for Disney, according to the director in an interview included with the DVD release. The film was released in some theaters and did not lose money.

==Plot==
A lewd old lady claiming to be Mother Goose (Hal Smith) has been put on trial for obscenity due to telling the "true versions" of famous fairy tales.  Her evidence is presented as a collection of pornographic animated shorts, those of Jack and the Beanstalk, Cinderella, and Little Red Riding Hood.

==Cast== Hal Smith - Mother Goose (live action segments)
*Frank Welker - Jack/Fairy Godmother/Prince/additional voices
*Richmond Johnson
*Carol Piacente
*Kelly Gordon

==Home Video==

On November 14, 2006, Severin Films released Once Upon a Girl on DVD.  The DVD features the uncut version, as Severin surrendered the original X rating for an unrated video release.

==See also==
*Adult animation
*Cartoon pornography
*List of animated feature-length films

==External links==
*  
*  
*  
*  

 
 
 
 
 


 