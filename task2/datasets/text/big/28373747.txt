Fested: A Journey to Fest 7
 
{{Infobox film
| name           = Fested: A Journey To Fest 7
| image          = 
| caption        = DVD Cover by Craig Horky
| director       = Reese Lester
| producer       =  
| writer         = Reese Lester
| starring       =  
| music          = 
| cinematography =  
| editing        = Reese Lester
| studio         = 
| distributor    = Blue Elephant Media
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Fest 7 in Gainesville, FL in 2008. The movie was directed by Reese Lester about The Fest and a group of about a dozen close friends experience attending and performing at it.     

The films title is a reference to one of the people in the film, David McMaster, referring to his highly intoxicated friend as "fested". 

==Synopsis==
From the official website (used with permission): Fest 7 is a documentary film about the yearly punk music festival held in Gainesville, Florida simply called The Fest.
It was filmed over two weeks spent in Gainesville around Fest 7 in 2008. The film follows Gainesvilles SPANISH GAMBLE (formerly DIRTY MONEY) and friends on their quest to play and experience the epic weekend-long festival that has been called things like "punk rock christmas", "a big, drunk, punk rock, bearded, tattooed family reunion", and "the last true home of punk rock".
In the spirit of Penelope Spheeris Decline Of Western Civilization series, FESTED features interviews, performances, and who-knows-what from over 20 bands... 

The artwork for the DVD was designed by Craig Horky, who also designed similar artwork for Spanish Gambles debut album, "Its All Coming Down".

There are about 20 bands in the film either performing, being interviewed, or having a recorded The Falcon, Hour Of Off With Paint It Black, Spanish Gamble, Static Radio NJ, Tiltwheel (band)|Tiltwheel, and Umoja Orchestra.

Throughout the film the band Spanish Gamble plays a leading role, though at the time they were known as Dirty Money.

The film was principally shot by  . 

==Live performances==
* The Anchor
**"This Is For My Friends" The Falcon
**"The La-Z-Boy 500"
* The Flatliners
**"July! August! Reno!" Hour Of The Wolf
**"Eat You Alive" Black Flag cover song)(On DVD extras)
* The Lawrence Arms
**"Great Lakes/Great Escapes"
**"Like A Record Player"
* New Mexican Disaster Squad
**"Tightrope"
* None More Black
**"We Dance On The Ruins Of The Stupid Stage"
**"Dinners For Suckers"
**"Drop The Pop"
* OK Pilot
**"No Sleeping On The Floor"
**"Too Hot To Stop" Paint It Black
**"Past Tense, Future Perfect"
**"Atticus Finch"
**"Memorial Day"
* Spanish Gamble
**"1-2-3 Fest!" (as Dirty Money)
**"From The Corazon" (as Dirty Money)
**"Sunday Curse"
* Static Radio NJ
**"Big Man, Small Mouth" (Minor Threat cover song)
* Umoja Orchestra
**"Indocumentado"

==Recorded tracks==
* 10-4 Eleanor
**"Whispers In A Shot Glass"
**"Its Alive!"
* American Steel
**"Emergency House Party"
* Dear Landlord
**"I Live In Hell" The Falcon
**"Building The Perfect Asshole Parade"
* Fleshies
**"Meatball"
* Grabass Charlestons
**"Un-American"
* Lagrecia
**"Hey Medic"
* Lasalle (band)
**"Pretty World"
* The Lawrence Arms
**"Like A Record Player Off With Their Heads
**"I Am You" Paint It Black
**"Memorial Day"
* Spanish Gamble
**"We Are The Restless" Tiltwheel
**"Fuck You, This Place Is Dead Anyway"

==Notable musicians interviewed== Brendan Kelly The Falcon) Paint It Kid Dynamite)
* Adam Goren (Atom And His Package) Off With Their Heads)
* Chris Cresswell and Jon Darbey (The Flatliners)

==Soundtrack==
{{Infobox album
| Name        = Fested: A Journey To Fest 7
| Type        = Soundtrack
| Artist      = Various Artists
| Cover       =
| Released    =  
| Recorded    =
| Genre       = Punk rock
| Length      =
| Label       = Blue Elephant Media
| Producer    =  Reese Lester
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}} digital download.

===Track listing=== Tiltwheel
# "1-2-3 Fest!" (Live) – Spanish Gamble (FKA Dirty Money)
# "Like A Record Player" (Live) – The Lawrence Arms
# "I Live In Hell" – Dead Landlord
# "I Am You" – Off With Their Heads (band)
# "We Dance On The Ruins Of The Stupid Stage" (Live) - None More Black The Falcon LaSalle
# "Hey Medic" - Lagrecia
# "Too Hot To Stop" - OK Pilot
# "Meatball" - Fleshies
# "Tightrope" (Live) - New Mexican Disaster Squad
# "We Are The Restless" - Spanish Gamble Paint It Black
# "Its Alive!" - 10-4 Eleanor

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 