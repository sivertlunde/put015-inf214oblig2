The Beloved Vagabond (1915 film)
 
{{Infobox film
| name           = The Beloved Vagabond
| image          =
| caption        =
| director       = Edward José
| producer       =
| based on       =  
| writer         = George B. Seitz
| starring       = Edwin Arden
| music          = Darius Milhaud
| cinematography =
| editing        =
| studio         = Pathé Exchange (as Gold Rooster Plays)
| distributor    = Pathé Exchange
| released       =  
| runtime        = Six reels
| country        = United States Silent (English intertitles)
}} romantic drama film directed by Edward José and starring Edwin Arden.  Originally, prints of the film were film tinting|hand-colored. Darius Milhaud wrote the music to be played with this silent film.
 The Beloved in 1923 in 1936.

==Cast==
* Edwin Arden - Gaston de Nerac / Paragot 
* Kathryn Brown-Decker - Joanna Rushworth
* Bliss Milford - Blanquette
* Doc Crane - Asticot
* Mathilde Brundage
* Florence Deshon

==Plot==
The wealthy Gaston de Nerac (Arden) decides to live as a tramp until he falls in love.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 