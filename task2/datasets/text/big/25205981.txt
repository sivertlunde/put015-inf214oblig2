Fugitive Valley
{{Infobox film
| name           = Fugitive Valley
| image          =
| image_size     =
| caption        =
| director       = S. Roy Luby
| producer       = Anna Bell Ward (associate producer) George W. Weeks (producer) Oliver Drake (story) Robert Finkle (writer) John Vlahos (writer)
| narrator       =
| starring       =
| music          = Frank Sanucci
| cinematography = Robert E. Cline
| editing        = S. Roy Luby
| distributor    =
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Fugitive Valley is a 1941 American film directed by S. Roy Luby, one of the Range Busters series of western films.

==Cast==
*Ray Corrigan as Crash Corrigan
*John Dusty King as Dusty King
*Max Terhune as Alibi Terhune
*Elmer as Elmer, Alibis Dummy
*Julie Duncan as Ann Savage aka The Whip
*Glenn Strange as Gray
*Bob Kortman as Red Langdon Ed Brady as Dr. Steve
*Tom London as Marshal Warren
*Reed Howes as Jim Brandon
*Carl Mathews as Henchman Slick
*Edward Peil Sr. as Ed (jailer)
*Doye ODell as Jim - Whip rider

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 