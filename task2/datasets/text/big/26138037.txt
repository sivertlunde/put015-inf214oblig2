Echo Park (film)
 
{{Infobox film
| name = Echo Park
| image = Echopark-film-poster.jpg
| image_size =
| caption = DVD cover
| director = Robert Dornhelm
| producer = Walter Shenson
| writer = Michael Ventura
| narrator = Michael Bowen Cheech Marin
| music = David Anderle Aaron Jacoves
| cinematography = Karl Kofler
| editing = Ingrid Koller
| distributor = Orion Pictures
| released =  
| runtime = 88 min
| country = United States
| language = English
| budget =
| gross = $700,000 (domestic)
}} Echo Park neighborhood of Los Angeles, California. The plot follows several aspiring actors, musicians and fashion model|models.

==Cast== Michael Bowen. East of Eden.

==Plot==
May, a single mom, wants to become an actress. Her next-door neighbor August, a bodybuilder, wants to become a worthy successor to his hero, Arnold Schwarzenegger. They live in a district of Los Angeles known as Echo Park, not far from Dodger Stadium, and dream of a better life.

Jonathan, a pizza delivery boy, arrives at Mays door and is immediately smitten with her. As he entertains her young son, Henry, she goes out to pursue an acting opportunity that has come along, only to discover that it involves disrobing in private residences, delivering "strip-o-grams."

May gives that a try, August tries to meet his idol at a reception at the Austrian embassy, while Jonathan worries that the two are more than mere neighbors.

==Reviews==
Echo Park maintains an 86% fresh rating at   gave the film three out of four stars, saying that the film "has no great statement to make and no particular plot to unfold. Its ambition is to introduce us to these people and let them live with us for a while. The movie is low-key, unaffected and sometimes very funny." Roger Eberts review of  . 

==References==
 

==External links==
*   AMC
*  
*  
*  

 

 
 
 
 
 
 


 