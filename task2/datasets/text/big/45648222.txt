John Doe: Vigilante
 

 
{{Infobox film
| name           = John Doe: Vigilante
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Kelly Dolen
| producer       =  
| writer         =  
| screenplay     = Stephen M. Coates 
| story          = {{Plainlist|
* Stephen M. Coates 
* Kelly Dolen }}
| based on       =  
| starring       = {{Plainlist|
* Jamie Bamber
* Daniel Lissing
* Lachy Hulme }}
| narrator       =  
| music          = David Hirschfelder 
| cinematography = David Parker 
| editing        = 
| studio         = {{Plainlist|
* Rapidfire Entertainment 
* Screen Corporation }}
| distributor    =  
| released       =  
| runtime        = 93 Minutes
| country        = Australia 
| language       = English
| budget         = 
| gross          =  
}}

John Doe: Vigilante (also known as John Doe) is a 2014 crime thriller.

==Plot==
John Doe (Jamie Bamber) is an ordinary man who decides to take the law into his own hands due to frustration with a failing legal system. Doe exacts his justice by killing one criminal at a time, and gaining attention from the media in the process, inspiring other copycat vigilantes. 

==Cast==
* Jamie Bamber as John Doe
* Daniel Lissing as Jake
* Lachy Hulme as Ken Rutherford
* Ditch Davey as Detective James Clint
* Sam Parsonson as Murray Wills
* Paul OBrien as Officer J.Callahan
* Chloe Guymer as Chloe
* Louise Crawford as Leah
* Ben Schumann as Boy
* Fletcher Humphrys as Henry Junig
* Brooke Ryan as Mary
* Gary Abrahams as Sam Foley
* Brendan Clearkin as Adam McLeish
* Harry Pavlidis as Alan Walker
* Dennys Ilic as Photojournalist

==Reception== average rating of 3.8/10 based on 5 reviews. 
==References==
 

==External links==
*  
* http://www.imdb.com/title/tt1418754/