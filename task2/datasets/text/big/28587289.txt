Göta kanal 3: Kanalkungens hemlighet
 
{{Infobox film
| name           = Göta kanal 3:  Kanalkungens hemlighet
| image          = Gota kanal 3.jpg
| image size     = 185px
| director       = Christjan Wegner
| producer       = Anders Birkeland Göran Lindström
| writer         = Hans Iveberg
| starring       = Janne Carlsson Eva Röse Magnus Härenstam
| music          =
| cinematography = Jens Fischer
| editing        = Darek Hodor Katarina Wiklund
| distributor    =
| released       =  
| runtime        = 92 minutes
| country        = Sweden
| language       = Swedish
| budget         =
| gross          =
}}
Göta kanal 3: Kanalkungens hemlighet (English: Göta Channel 3: The Channel Kings Secret) is a 2009 Swedish film directed by Christjan Wegner.

It is a sequel to Göta kanal eller Vem drog ur proppen? and Göta kanal 2 – Kanalkampen.

== Cast ==
* Janne Carlsson as Janne Andersson
* Eva Röse as Petra Andersson
* Magnus Samuelsson as the motorcyclist
* Magnus Härenstam as Peter Black
* Sara Sommerfeld as Tanja Svensson
* Svante Grundberg as the canoeist
* Christian Rinmad as Vincent
* Rafael Edholm as Benito Eric Ericson as Henrik
*Jon Skolmen as a Norwegian

== External links ==
*  
*  

 

 
 
 
 
 
 
 


 
 