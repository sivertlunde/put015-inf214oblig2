Boulevard of Broken Dreams (film)
 
{{Infobox film
| name           = Boulevard of Broken Dreams
| image          = 
| image size     =
| caption        = 
| director       = Pino Amenta
| producer       = Frank Howson
| writer         = Frank Howson
| based on = 
| narrator       = John Waters Penelope Stewart Kim Gyngell
| music          = 
| cinematography = 
| editing        = 
| studio =  Boulevard Films
| distributor    = Hoyts
| released       = 1988
| runtime        = 
| country        = Australia English
| budget         = AU$2 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p198-200  
| gross = AU$126,359 (Australia) 
| preceded by    =
| followed by    =
}}
Boulevard of Broken Dreams is a 1988 Australian film. It was the first movie from Boulevard Films.

==Plot==
A successful Australian writer discovers he has cancer and returns home to Melbourne to be with his estranged wife and daughter.

==Production==
Producer-writer Frank Howson met Pino Amenta when discussing a possible mini series about Les Darcy. That was never made but they decided to collaborate on this film. 

Howson later said, "It was the first film wed done, and it was made with a lot of commercial requirements because we werent in the position to just make a film and hope for the best. We set out to make a film that would do well here and internationally." Paul Kalina, "Boulevard Films", Cinema Papers, November 1989 p42-43 

Among theses decisions were the ending. Originally, John Waters character was to get on a plane to Los Angeles without anyone knowing that he returned home to die. In the final film, though, he was reunited with his wife and child. 

Howson placed a great emphasis on music for the movie:
 We recorded a great deal of those songs in LA with people like Richie Havens, Dan Hill and Marc Jordan. On most Australian productions, the soundtrack tends to be done last and usually at a stage when they have almost run out of money. It suffers as a result. To me, the soundtrack is one of the most important things for the emotional balance of a film.  

==Reception==
John Waters earned an AFI Award for Best Actor and the film was popular enough for Boulevard Films to secure funding for a further five movies.  Reviews were mostly poor. 

==References==
 

==External links==
*  at IMDB
 

 
 
 

 