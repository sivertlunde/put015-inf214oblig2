Little Nellie Kelly
 
{{Infobox film
| name           = Little Nellie Kelly
| image          = Poster - Little Nellie Kelly 03.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Norman Taurog
| producer       = Arthur Freed
| writer         = Jack McGowan Based on the play by George M. Cohan
| starring       = Judy Garland George Murphy
| music          = Roger Edens William Axt
| cinematography = Ray June
| editing        = Fredrick Y. Smith
| distributor    = Metro-Goldwyn-Mayer (1940, original) Warner Bros. (2011, DVD)
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $718,000  . 
| gross          = $2,046,000 
}}
 musical comedy stage musical Broadway in 1922 and 1923.   The film was written by Jack McGowan and directed by Norman Taurog.  Its cast included Judy Garland, George Murphy, Charles Winninger and Douglas McPhail.

The film is notable for containing Judy Garlands only on-screen death scene, although she re-appears in the film as the daughter of the character who died.

A DVD of the movie was released on March 15, 2011.
 

==Plot==
In Ireland, Jerry Kelly (George Murphy) marries his sweetheart, Nellie Noonan (Judy Garland) over the objections of her neer-do-well father, Michael Noonan (Charles Winninger), who swears never to speak to Jerry again, even though he reluctantly accompanies the newlyweds to America, where Jerry becomes a policeman, and all three become citizens. Michael continues to hold his grudge against Jerry, even when Nellie dies while giving birth to little Nellie.

Years later, Jerry is now a captain on the police force, and little Nellie (also played by Judy Garland) has grown up as the spitting image of her mother. When Nellie becomes enamored of Dennis Fogarty (Douglas McPhail), the son of Michaels old friend Timothy Fogarty (Arthur Shields), the squabbling between Nellies father and grandfather intensifies, as Michael objects to the romance, and finally leaves home because of it.

Eventually, the three generations are reconciled, and Nellie and Dennis remain a couple.   

==Cast==
*Judy Garland as Nellie Noonan Kelly and as Little Nellie Kelly
*George Murphy as Jerry Kelly
*Charles Winninger as Michael "Mike" Noonan
*Douglas McPhail as Dennis Fogarty
*Arthur Shields as Timothy Fogarty
*Rita Page as Mrs. Mary Fogarty
*Forrester Harvey as Moriarity James Burke as Police Sergeant McGowan George Watts as Mr. Keevan, NYC Bar Owner

==Songs== 5th Avenue during the St. Patricks Day Parade. This song became one of Garlands biggest hits.  

Songs cut from the film include:  "Rings on Your Fingers and Bells on Your Toes" (used in Garlands later film Babes on Broadway), "Danny Boy" and "How Can You Buy Killarney".

==Production== The Wizard MGM to evaluate both Garlands audience appeal and her physical image. It was rumoured at the time that George M Cohan sold the rights expressly as a vehicle for the young Garland. The film gave 18-year-old Garland the opportunity to grow up as she is in the first half of the picture set in Ireland, in which she plays Nellie Noonan, the mother of Little Nellie Kelly. Although called a bit of Blarney, overall the film was well received and has become a classic St Patricks Day film. Critics noted "she (Judy Garland) gets prettier with each picture". 
==Box office==
According to MGM records the film earned United States dollar|USD$968,000 in the US and Canada and $1,078,000 elsewhere resulting in a profit of $680,000. 

==See also== List of Films Set in Ireland

==Notes==
 

==External links==
 
*  
*  
*  
*  
*   at the Judy Garland Online Discography
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 