Alice (2005 film)
{{Infobox Film |
  name           = Alice|
  image          =  |
  caption        = Alice (poster)|
  writer         = Marco Martins|
  starring       = Nuno Lopes Beatriz Batarda Miguel Guilherme Ana Bustorff
  director       = Marco Martins |
  producer       = Paulo Branco |
  director of photography = Carlos Lopes (Cácá)|  
  music          = Bernardo Sassetti 
| released       = 6 October 2005
| runtime        = 102 min|
  language       = Portuguese| 
  budget         = |
}}
 Portuguese film directed by Marco Martins, released in 2005. Alice stars Nuno Lopes as Mário, the father, and Beatriz Batarda as Luísa, his wife, as well as Miguel Guilherme, Ana Bustorff, Gonçalo Waddington, Carla Maciel, Laura Soveral and José Wallenstein. Alice was produced by Paulo Branco. Music is by Bernardo Sassetti.

Shot in a dark, depressive undertone, Alice unveils a Lisbon whose mists, colours, alleys and moods are strangers, despite the familiarity of the locations. All seems odd, silently cruel, as cruel is the disappearance of a child from the path shed traveled every day in (apparent) safety. The anguish is masterfully conveyed, and so is the loneliness of both parents.

== Plot ==
Its been 193 days since Alice was last seen. Every day, her father, Mário, leaves home and follows exactly the same route the day Alice disappeared. In his quest, Mário creates a video surveillance network on the places she might walk, the airport, anything that may provide a clue. 

Every day, he watches countless hours of surveillance tapes in fast-forward, expecting to get a glimpse of his child. Almost every day, hope is born. Almost every day, hope is extinguished - until one day, the search seems to be over.

== Cast ==
*Nuno Lopes - Mário
*Beatriz Batarda - Luísa
*Miguel Guilherme - 
*Ana Bustorff - Margarida
*Laura Soveral - Lurdes
*Gonçalo Waddington - Airport Security
*Carla Maciel - Mónica
*José Wallenstein - Detective
*Clara Andermatt - 
*Ivo Canelas - Ricardo 

== Awards ==
An internationally acclaimed success, Alice won and received nominations for a number of awards, including:
*Cannes Film Festival - Won Directors Fortnight Best Picture Award 2005.
*Cannes Film Festival - Won Jeunes Regards Award 2005.
*Berlin International Film Festival - Won Shooting Star Award 2006 (Nuno Lopes).
*Las Palmas de Gran Canaria International Film Festival - Won Best Director - 1st work 2006 (Marco Martins).
*European Film Awards - Nominated for the Fassbinder Award 2005.
*Mar del Plata Film Festival - Won Best Director, Best Filmography and FIPRESCI Prize 2006. Nominated for Best Film.
*Raindance Film Festival- Won Best Director - 1st work 2006 (Marco Martins).
 submitted for the 79th Academy Awards (2007) in the category Academy Award for Best Foreign Language Film.

== References ==
 

==External links==
*http://www.madragoafilmes.pt/alice/ (in Portuguese)
*   (Madragoa Filmes).
*   atEstreia Online.
*   at Cannes.
*   atCinema PTGATE.
*   at Público.
*  

 

 
 
 
 