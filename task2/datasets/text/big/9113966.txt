Searching for Monica
{{Infobox film
| name = Buscando a Mónica
| image =  BuscandoaMónica.jpg
| caption = The Railway set used in the film
| director = José María Forqué
| producer = Francisco Carcavallo Luis Giudici
| writer = Jaime de Armiñán Vicente Coello
| starring = Alberto de Mendoza Carmen Sevilla
| music = Augusto Algueró hijo Mário Clavel
| cinematography = Ricardo Younis
| editing = Julio Peña Gerardo Rinaldi
| distributor = AS Films
| released = 29 January 1962
| runtime = minutes
| country = Argentina Spanish
| budget =
| followed_by =
}} Spanish black-and-white drama directed by José María Forqué. The film premiered on 29 January 1962 in Madrid and was named El Secreto de Mónica. it was first shown in Argentina on 29 March in Buenos Aires. It starred Alberto de Mendoza and Carmen Sevilla.

==Plot summary==
A man named Antonio Montes (Alberto de Mendoza) has a car accident on a street on his way to Buenos Aires. While he waits for his car repairing, he learns that everybody in the little town where hes stuck seem to know about his wife Mónica. He knows little about her past, but the reactions of the townsfolk towards him range from laughing behind his back to practically not standing the sight of him. He wont leave the town until he could learn more about the secret past of his wife. But the former life of the young beautiful mother of his daughter involves a turbulent story.

==Cast==
*Carmen Sevilla ....  Mónica Durán
*Alberto de Mendoza ....  Antonio Montes
*Jardel Filho
*Ana Casares
*Enrique Diosdado  (as Enrique Alvarez Diosdado)
*Adolfo Marsillach
*Guillermo Battaglia
*Antonia Herrero
*Mario Baroffio
*Alma Montiel
*Orestes Soriani  (as Oreste Soriani)
*Cayetano Biondo
*Rodolfo Crespi
*Alberto Fernández de Rosa  (as Alberto F. De Rosa)
*Carlos Cotto  (as Carlos Coto)
*Elena Lucena  (as Lucy)
*Sara Yuter
*Domingo Garibotto
*Aída Villadeamigo
*Alberto Quiles
*Nonne L. de Perez
*Amelia Folsini
*Yamandú Romero  (as Yamandu Romero)
*Hector G. Corti
*Miguel Yunes
*Hector Givagnoli
*Mario Laserna
*Felipe Duarte
*Juan Laserna
*I. Del Bosco
*Eduardo Diaz
*Humberto Martinez
*Oscar Zaccari
*Rosángela Balbó
*Nelly Antonelli
*Elvira Porcel

==External links==
*  

 
 
 
 
 
 
 
 


 
 