Dead Game
 
{{Infobox film
| name           = Dead Game
| image          = Dead Game Poster.jpg
| caption        = Film poster
| director       = Edward Sedgwick
| producer       = Carl Laemmle
| writer         = 
| starring       = Hoot Gibson
| music          = 
| cinematography = Charles E. Kaufman
| editing        = 
| distributor    = 
| released       =  
| runtime        = 50 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 Western film directed by Edward Sedgwick and featuring Hoot Gibson.   

==Cast==
* Hoot Gibson as "Katy" Didd (credited as Ed "Hoot" Gibson) Robert McKim as Prince Tetlow Harry Carter as Jenks
* Laura La Plante as Alice Mason William Welsh as Harlu
* Tony West as Hiram William Steele as Sam Antone (credited as William A. Steele) Alfred Allen as Undetermined Role (uncredited)

==See also==
* List of American films of 1923
* Hoot Gibson filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 