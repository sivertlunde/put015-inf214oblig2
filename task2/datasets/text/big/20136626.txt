Chili Weather
{{multiple issues|
 
 
 
}}
{{Infobox Hollywood cartoon| cartoon_name = Chili Weather series = Merrie Melodies (Speedy Gonzales) image = caption = director = Friz Freleng story_artist = John Dunn animator = Gerry Chiniquy Virgil Ross Bob Matz Lee Halpern Art Leonardi layout_artist = Hawley Pratt background_artist = Tom OLoughlin voice_actor = Mel Blanc musician = Bill Lava producer = David H. DePatie (uncredited) studio = Warner Bros. Cartoons distributor = Warner Bros. Pictures release_date =   color_process = Technicolor runtime = 6 minutes movie_language = English
}}Chili Sylvester and Speedy Gonzales.

==Plot== Sylvester guarding the place. The hungry mice summon Speedy Gonzales to help them get food. Speedy steals some cheese, but when he goes back, Sylvester starts chasing him. They end up on a conveyor belt, where Sylvester gets shaved by some chopping blades. Speedy spreads some grease on a platform, and Sylvester skids into a vat of Tabasco Sauce. He melts a block of ice to recover. On another conveyor belt, Sylvester gets a bottle cap pressed onto his head. He pries it off with a bottle opener, but Speedy "yee-has" him into the ceiling where it gets stuck again, and Speedy hides his bottle opener. Sylvester wanders off, trying to club Speedy but unable to because of his lack of sight, and his aimless wandering lands him in a dehydrator. He emerges from the dehydrator in a miniature size (finally able to remove the bottle cap), but then Speedy, who is now larger than the cat, greets him prompting the tiny Sylvester to run away in fright.

==Edited versions==
*On CBS, the part where Sylvester chases Speedy Gonzales through a conveyor belt with cleavers was edited to remove Speedy dodging the cleavers without moving and not getting hurt (but left in Sylvester getting his fur shaven off by the cleavers).

==External links==
* 
* 

 
 
 
 
 
 
 

 