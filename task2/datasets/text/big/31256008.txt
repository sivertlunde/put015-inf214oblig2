Tesha
 
 
{{Infobox film
| name           = Tesha
| image          =
| caption        =
| director       = Victor Saville   Edwin Greenwood
| producer       = Victor Saville
| writer         = Oliver Sandys   Walter C. Mycroft   Victor Saville
| starring       = María Corda  Jameson Thomas   Paul Cavanagh   Mickey Brantford
| music          = Harry Gordon
| cinematography = Werner Brandes
| editing        = 
| studio         = British International Pictures   Burlington Films
| distributor    = Wardour Films
| released       = 24 August 1924
| runtime        = 95 minutes
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British drama film directed by Victor Saville and Edwin Greenwood and starring María Corda, Jameson Thomas and Paul Cavanagh.  A mans wife has an affair with his best friend and becomes pregnant.  The film was originally shot as a silent film but in 1929 sound was added.

==Cast==
* María Corda as Tesha
* Jameson Thomas as Robert Dobree
* Paul Cavanagh as Lenane
* Mickey Brantford as Simpson
* Clifford Heatherley as Doctor
* J.J. Espinosa as Dancemaster 
* Boris Ranevsky as Teshas Father 
* Daisy Campbell as Mrs Dobree
==References==
 

==Bibliography==
* Slide, Anthony. Fifty classic British films, 1932-1982: a pictorial record. Constable and Company, 1985.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 