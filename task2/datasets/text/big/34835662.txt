The Left-Hand Side of the Fridge
  Canadian film director Philippe Falardeau, released in 2000.

Shot in mockumentary style, the film stars Paul Ahmarani as Christophe and Stéphane Demers as Stéphane, two roommates sharing an apartment in Montreal. Christophe is an unemployed engineer, while Stéphane is a documentary filmmaker who begins filming Christophes search for work.

The film won the award for Best Canadian First Feature Film at the 2000 Toronto International Film Festival, as well as the Claude Jutra Award for the best Canadian film by a first-time director at the 21st Genie Awards. Ahmarani won the 2001 Prix Jutra for Best Actor; Falardeau was also nominated for Best Director and Best Screenplay, but did not win.

==External links==
* 

 

 
 
 
 
 
 
 

 