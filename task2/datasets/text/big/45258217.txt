Boonie Bears: Mystical Winter
{{Infobox film
| name           = Boonie Bears: Mystical Winter
| image          = Boonie Bears- Mystical Winter poster.jpg
| alt            = 
| caption        = 
| film name      = 熊出没之雪岭熊风 
| directors      = Ding Liang Liu Fuyuan
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = 
| cinematography = 
| editing        = 
| production companies = Shenzhen Huaqiang Shuzi Dongman Co.,Ltd Le Vision Pictures (Tianjin) Co.,Ltd Fantawild Holdings Inc Pearl River Pictures Co., Ltd Beijing Iqiyi Co.,Ltd Beijing LeTV Mobile Media & Technology Co. Ltd He Yi information technology (Beijing) Co., LTD Tencent Video TV.SOHU.COM You Yang（ Tian Jin） Dong Man Culture Media Co., LTD
| distributors   = Le Vision Pictures (Tianjin) Co.,Ltd Mr. Cartoon Pictures Co.,Ltd Pearl River Pictures Co., Ltd Shenzhen Huaqiang Shuzi Dongman Co.,Ltd
| released       =  
| runtime        = 96 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = US$47 million (China)   
}} Chinese animated family adventure adventure comedy film directed by Ding Liang and Liu Fuyuan. It was released on January 30, 2015. 

==Voice cast==
*Zhang Wei
*Zhang Bingjun
*Tan Xiao
*Meng Yutian
*Sun Yaodong
*Zhao Xiaoyu
*Xin Yuan
*Wan Danqing

== Box office == 
As of February 15, 2015, the film has earned over US$40.13 million in China m.   

==See also==
*Boonie Bears, the television series
* , the 2014 film based on the television series

==References==
 

 
 
 
 
 
 
 
 


 
 
 
 