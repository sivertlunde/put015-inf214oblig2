The Tunnel (2014 film)
 
{{Infobox film
| name           = The Tunnel
| image          = 
| director       = Park Gyu-taek
| producer       = Han Man-taek   Kwon Jun-hyeong
| writer         = Yu Se-mun Jeong Yu-mi Yeon Woo-jin Song Jae-rim Jeong Si-yeon Lee Si-won
| cinematography = Yu Jae-eung
| editing        = Moon In-dae
| music          = Han Jae-kwon
| distributor    = BoXoo Entertainment
| studio         = 
| country        = South Korea
| language       = Korean
| runtime        = 86 minutes
| budget         = 
| gross          = 
| released       =  
}}
The Tunnel ( ; lit. Tunnel 3D) is a 2014 South Korean horror film directed by Park Gyu-taek. 

==Plot==
A group of friends are invited to the launch party of a luxury resort. A strange man barges in, and scares everyone away by declaring that they will all be killed by a curse. They return to the resort to find the strange man watching them, and accidentally end up killing him. They decide to dispose of the body in a coal mine, but get trapped inside the dark tunnels facing the buried horrors within.

==Cast== Jeong Yu-mi as Eun-joo
* Yeon Woo-jin as Dong-jun
* Song Jae-rim as Ki-cheol
* Jeong Si-yeon as Se-hee
* Lee Si-won as Yoo-kyung
* Son Byung-ho as Mr. Kim
* Lee Jae-hee as Young-min
* Min Do-hee as Girl 
* Woohee as Hye-young 

==References==
 

== External links ==
*    
*    
*  
*  

 
 
 
 
 
 
 