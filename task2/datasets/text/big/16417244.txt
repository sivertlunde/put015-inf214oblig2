P (film)
{{Infobox film
| name           = P
| image          =
| caption        =
| director       = Paul Spurrier
| producer       = Panupong Dangdej Narongsak Vorarraitchagun Narongsak Vorrarutchaikul
| writer         = Translation Preeyaporn Chareonbutra Screenplay Paul Spurrier Story Paul Spurrier
| narrator       = Puvisate Ltd.
| starring       = Suangporn Jaturaphut John Kathrein Shaun Delaney Amy Siria
| music          = Paul Spurrier
| cinematography = Rich B. Moore Jr.
| editing        = Paul Spurrier
| distributor    = Creative Films Siam Ltd.
| released       =  
| runtime        = 110 minutes
| country        = Thailand United Kingdom
| language       = Thai
| budget         =
}}
P is a 2005 Thai horror film directed by Paul Spurrier. 

==Plot==
Whilst growing up in rural Thailand, a young orphan girl named Dau (Suangporn Jaturaphut) is taught the ways of magic by her grandmother. But when grandmother falls sick, Dau is lured to Bangkok to find work so that she can buy medicine. She finds herself working in a go-go bar, and her journey from naiveté to maturity is swift. She uses the magical skills her grandmother taught her to her advantage, but in doing so makes enemies within the bar. As her magic gets darker, and the consequences increasingly horrific, she gradually loses control, and something evil takes over. 

==Cast==
* Suangporn Jaturaphut as Dau/Aaw
* Shaun Delaney	as Rich Customer
* John Kathrein	as Customer
* Chartchai Kongsiri as Policeman
* Opal as Pookie
* Pisamai Pakdeevijit as Grandmother
* Supatra Roongsawang as New
* Narisara Sairatanee as May
* Amy Siriya as Mee

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 


 
 