The Facts of Life (film)
{{Infobox film 
| name           = The Facts of Life
| image          = Factsoflife1960.jpg
| producer       = Melvin Frank Norman Panama
| director       = Melvin Frank Norman Panama (uncredited)
| writer         = Melvin Frank Norman Panama
| starring       = Bob Hope Lucille Ball
| music          = Leigh Harline Johnny Mercer
| cinematography = Charles Lang
| editing        = Frank Bracht
| distributor    = United Artists
| released       =  
| runtime        = 103 min. English
}}
The Facts of Life is a 1960 romantic comedy starring Bob Hope and Lucille Ball as married people who have an affair. Written, directed, and produced by the longtime Hope associates Melvin Frank and Norman Panama, it was more serious than many other contemporary Hope vehicles.
 Costume Design Edward Stevenson). Best Actress – Comedy. The film features an opening animated title sequence created by Saul Bass.

==Plot==
As the yearly vacation of six neighbors, the Gilberts, Masons, and Weavers, approaches, Kitty Weaver (Lucille Ball) and Larry Gilbert (Bob Hope) find themselves frustrated with the routine. When both their spouses (Ruth Hussey and Don DeFore) are kept away from the vacation, Kitty and Larry find themselves alone in Acapulco, with the Masons (Philip Ober and Marianne Stewart) bedridden with illness. Forced together, Kitty and Larry fall in love. Once the vacation is over, however, they have difficulties in either abandoning or continuing their romance.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Bob Hope || Larry Gilbert
|-
| Lucille Ball || Kitty Weaver
|-
| Ruth Hussey || Mary Gilbert
|-
| Don DeFore || Jack Weaver
|-
| Louis Nye || Charles Busbee
|-
| Philip Ober || Doc Mason
|-
| Marianne Stewart || Connie Mason
|-
| Hollis Irving || Myrtle Busbee
|}

==Awards and nominations==
===Academy Awards===
Won:     Best Costume Edward Stevenson)
Nominated: Best Art Direction, Black and White (J. McMillan Johnson, Kenneth A. Reid, and Ross Dowd) Best Cinematography, Black and White (Charles Lang) Best Original Song (Johnny Mercer for "The Facts of Life") Best Original Screenplay (Melvin Frank and Norman Panama)

===Golden Globe Awards===
Nominated: Best Actress in a Motion Picture Musical or Comedy (Lucille Ball)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 