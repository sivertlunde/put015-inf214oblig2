Under the Lash
 
{{Infobox film
| name           = Under the Lash
| image          = Under-the-Lash-1921.jpg
| caption        = Lobby card
| director       = Sam Wood
| writer         = J. E. Nash
| based on       =    
| starring       = Gloria Swanson
| cinematography = Alfred Gilks
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 silent drama Alice and Claude Askew.   

The film is lost with no copies of it existing in any archives. 

==Cast==
* Gloria Swanson as Deborah Krillet
* Mahlon Hamilton as Robert Waring Russell Simpson as Simeon Krillet
* Lillian Leighton as Tant Anna Vanderberg
* Lincoln Stedman as Jan Vanderberg
* Thena Jasper as Memke
* Clarence Ford as Kaffir boy

==Reception==
Upon its release, Under the Lash was not well received by audiences. It was the only film released in the early 1920s starring Gloria Swanson that did not do well at the box office. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 