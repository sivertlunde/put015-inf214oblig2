Suzanne's Career
{{Infobox film
| name           = Suzannes Career
| image          =  Suzanne career 344 DVD.jpg
| caption        = DVD cover
| writer         = Éric Rohmer
| starring       = Catherine Sée Philippe Beuzen Christian Charrière Diane Wilkinson
| director       = Éric Rohmer
| editing        = Éric Rohmer
| cinematography = Daniel Lacambre
| producer       = Barbet Schroeder
| distributor    =
| released       = 1963
| runtime        = 54 min
| language       = French
| budget         =
}}
 French title is La Carrière de Suzanne. It is the second movie in the series of the Six Moral Tales.

== Plot ==
Bertrand, a timid student disapproves but still admires the lack of self-consciousness and the rude-but-efficient manners of Suzanne, one of his friend Guillaumes conquests.

== Cast ==
* Catherine Sée: Suzanne Hocquetot
* Christian Charrière: Guillaume Peuch-Drumond
* Philippe Beuzen:  Bertrand, the narrator
* Diane Wilkinson: Sophie
* Jean-Claude Biette: Jean-Louis
* Patrick Bauchau: Franck
* Pierre Cottrell: the art lover
* Jean-Louis Comolli

== External links ==
*  
*  

 

 
 
 
 

 