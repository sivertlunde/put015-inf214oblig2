Buckshot John
{{Infobox film
| name           = Buckshot John
| director       = Hobart Bosworth
| writer         = {{plainlist|
*Hetty Grey
*Charles E. van Loan
}}
| starring       = Hobart Bosworth
| cinematography = George W. Hill
| released       =  
| runtime        = 50 minutes
| country        = United States Silent with English intertitles
}}
 Western film, directed by and starring Hobart Bosworth. Prints of the film survive in the Library of Congress film archive.   

==Cast==
* Hobart Bosworth as "Buckshot John" Moran
* Courtenay Foote as Dr. Buchanan Gilmore / The Great Gilmore
* Carl von Schiller as Jimmy Dacey, a Reporter
* Helen Wolcott as Ruth Mason
* Herbert Standing as John Mason
* Marshall Stedman as Warden of states prison
* Frank Lanning as Bad Jake Kennedy
* Art Acord as Hairtrigger Jordan
* Elmo Lincoln as The sheriff (as Oscar Linkenhelt)
* Rhea Haines as Mrs. Hayden
* Arthur Allardt as Medicine Show Crowd (uncredited)
* J.F. Briscoe as Medicine show crowd (uncredited)
* Mr. Fletcher as Judge (uncredited)
* Hoot Gibson as Medicine Show Crowd (uncredited)
* Wong Ling as Hindo (uncredited)
* Martha Mattox as Medicine Show Crowd (uncredited)
* Robert Murdock as Turnkey (uncredited)
* Ray Myers as Medicine Show Crowd (uncredited)
* Joe Ray as Medicine Show Crowd (uncredited)

==See also==
* List of American films of 1915
* Hoot Gibson filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 