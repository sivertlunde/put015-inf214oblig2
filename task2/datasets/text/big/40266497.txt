Unforgiven (2013 film)
 
{{Infobox film
| name           = Unforgiven
| image          = Unforgiven (2013 film).jpg
| caption        = Film poster Lee Sang-il
| producer       = 
| writer         = David Webb Peoples Lee Sang-il
| starring       = Ken Watanabe
| music          = Taro Iwashiro
| cinematography = Norimichi Kasamatsu
| editing        = Tsuyoshi Imai
| distributor    = Nikkatsu Office Shirous Warner Bros.
| released       =  
| runtime        = 135 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}
 Lee Sang-il. It is a remake of  Clint Eastwoods 1992 western Unforgiven. The film was screened in the Special Presentation section at the 2013 Toronto International Film Festival.      

The plot closely follows the original 1992 film, but shifts the setting to Japans Hokkaido frontier during the early Meiji period. Jubei Kamata (Ken Watanabe), a former samurai, is approached by an old associate to help claim the bounty on two men who disfigured a prostitute.

==Plot==

Shortly after the start of the Meiji period, a former samurai under the Edo Shogunate, Jubei Kamata, flees from government forces on the northern Japanese island of Hokkaido. Jubei kills his pursuers and disappears, but remains infamous as “Jubei the Killer”.

Years later, in a frontier town, two brothers, Sanosuke and Unosuke Hotta, disfigure a prostitute. The local lawman, Ichizo Oishi, lets the brothers go with only minor chastisement instead of dispatching them to Sapporo to face justice. The other prostitutes gather together the reward for a bounty on the two brothers. This draws several bounty hunters, including Kingo Baba, an old associate of Jubei. One of the bounty hunters, Masaharu Kitaoji, arrives with his biographer. Armed with swords, he draws the attention of Oishi, who demands he hand over his weapons as they are banned in the town. Confronted by lawmen with firearms, Kitaoji has to accept, but is then beaten and humiliated by Oishi. The following day, Oishi throws him out of the town, but the biographer, Himeji, stays behind to write about Oishi instead.
 Ainu man who claims to have killed five men already. When the trio arrive at the frontier town, Oishi finds and recognises Jubei at the inn. He taunts, beats, and scars Jubei, but doesn’t kill him. Kingo and Goro are upstairs with the prostitutes and escape unharmed. After recuperating with the help of the prostitutes, the three men track down one of their targets and kill him. Jubei delivers the killing blow after Kingo is unable to do so himself. After this, Kingo admits that he doesn’t have the stomach for killing. Handing Jubei his rifle, he leaves the other two men to collect the bounty. Jubei and Goro return to the town to kill the other Hotta brother. Jubei sends Goro in to kill the man while he uses an outhouse. After a scuffle in which Goro is unable to get a fatal shot with his pistol, he stabs the Hotta brother with an Ainu knife. Although Jubei and Goro escape, Oishi’s men are able to track down Kingo, who is killed after a night of torture.

Jubei learns of his friend’s death when one of the prostitutes delivers the bounty. Sober for years, he drinks the last of Goro’s liquor and returns to the town to exact revenge. Before he leaves, he orders Goro and the scarred prostitute, Natsume, to deliver his share of the bounty to his children and look after them. Jubei returns to the inn and a battle ensues between him, Oishi, and the posse Oishi had put together to hunt down Jubei and Goro. Despite being wounded several times, Jubei is able to kill Oishi and several of his men, and the survivors flee. He orders Himeji, who witnessed the battle, to write about what he saw. However, Jubei knows that the Japanese authorities would be particularly harsh if they found out that Goro, who is half-Ainu, had been involved in the killing of two Japanese men. He therefore demands that Himeji leave Goro and the Ainu out the account, and threatens to kill the writer if he mentions them.

Having returned to Jubei’s farm, Natsume explains how she might be willing to settle down there with Goro, looking after Jubei’s children. She is hopeful that one day, Jubei might return. He is last seen walking alone, in the snow, somewhere in Hokkaido.

==Cast==
* Ken Watanabe as Jubei Kamata
* Kōichi Satō as Ichizo Oishi
* Akira Emoto as Kingo Baba
* Yūya Yagira as Goro Sawada
* Shiori Kutsuna as Natsume
* Eiko Koike as Okaji
* Jun Kunimura as Masaharu Kitaoji
* Yukiyoshi Ozawa as Sanosuke Hotta
* Takahiro Miura as Unosuke Hotta
* Kenichi Takito as Yasaburo Himeji
* Yoshimasa Kondo as Kihachi

==Production==
Filming took place between mid-September to late November, 2012. All filming took place in Hokkaido, Japan.

==References==
 

==External links==
*    
*  

 
 

 
 
 
 
 
 
 
 
 
 