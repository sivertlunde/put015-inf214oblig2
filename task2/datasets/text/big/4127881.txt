Train of Dreams
{{Infobox Film
| name           = Train of Dreams
| image          = 
| image_size     = 
| caption        = 
| director       = John N. Smith
| producer       = Sam Grana
| writer         = Sally Bochner John N. Smith Sam Grana
| narrator       = 
| starring       = Jason St. Armour Christopher Neil Fred Ward
| music          = 
| cinematography = 
| editing        = 
| distributor    = National Film Board of Canada 1987
| runtime        = 88 min.
| country        =   English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Train of Dreams is a 1987 Canadian film starring Jason St. Armour, Christopher Neil and Fred Ward as a popular teacher. It was directed by nominee John N. Smith.

In this documentary-style drama, a teenage delinquent teenager tries to put his life on the right track.   

==Awards==
The film won Special Jury Prize at the Chicago International Film Festival in 1987, was nominated for 4 Genie Awards in 1988. It was also won the Best Actor (Jason St. Amour) award at the Paris Film Festival in 1989.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 