Blues Busters (film)
{{Infobox film
| name           = Blues Busters
| image          = Blues Busters (1950 film).jpg
| caption        = Theatrical poster
| director       = William Beaudine
| producer       = Jan Grippo
| writer         = Charles Marion Bert Lawrence
| starring       = Leo Gorcey Huntz Hall Gabriel Dell David Gorcey William Benedict
| music          = Edward J. Kay
| cinematography = Marcel LePicard William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
}}

Blues Busters is a 1950 comedy film starring The Bowery Boys. The film was released on October 29, 1950 by Monogram Pictures and is the twentieth film in the series.

==Plot==
Sach develops an uncanny ability to sing, after having his tonsils removed, and Slip convinces Louie to turn his sweet shop into a night club, The Bowery Palace, after unsuccessfully trying to get Sach a singing job at a neighboring club, The Rio Cabana.
 Craig Stevens), the owner of the now-rival club, tries to hire him away but is unsuccessful.  Rick gets his lady friend, Lola (Adele Jergens), to get Sach to sign a contract with him, using the pretense that she is asking for his autograph.  Rick then goes after the Bowery Palaces other star, Sally Dolan (Phyllis Coates).  She, however, does not want to go because Rick is after more than just her singing talent.  She tips off Lola about what Rick is up to and Lola agrees to testify that Sachs signature was just an autograph and not a signed contract, thereby allowing him to return to the Bowery Palace.  However, by this time Sach has gone to a doctor to help cure the "tickle in his throat and he has lost the ability to sing.

==Production==
The working title of this film was The Bowery Thrush.  This is the last Bowery Boys film that Gabriel Dell appears in. Tired of taking a back seat to co-stars Leo Gorcey and Huntz Hall , he decided to quit the series. Ironically, around the time Dell was leaving, he and Huntz Hall formed a nightclub act called Hall and Dell, in which the two performed classic vaudeville routines. Dell played straight man to Hall.

==Home media== Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume One" on November 23, 2012.

==Cast==

===The Bowery Boys===
* Leo Gorcey as Terrance Aloysius Slip Mahoney
* Huntz Hall as Horace Debussy Sach Jones
* William Benedict as Whitey
* David Gorcey as Chuck
* Buddy Gorman as Butch

===Remaining cast===
* Gabriel Dell as Gabe Moreno
* Adele Jergens as Lola Stanton 
* Bernard Gorcey as Louie Dumbrowski Craig Stevens as Rick Martin
* Phyllis Coates as Sally Dolan
* William Vincent as Teddy Davis

==Soundtrack==
* "Wasnt It You?"
** Written by Ben Raleigh and Bernie Wayne
** Played on a radio and sung by Huntz Hall (dubbed by John Laurenz)

* "Joshua Fit the Battle of Jericho"
** Traditional spiritual
** Sung by Adele Jergens in a nightclub (dubbed by Gloria Wood)

* "Bluebirds Keep Singin in the Rain"
** Written by Johnny Lange and Eliot Daniel
** Published by Bulls Eye Music Inc. (ASCAP)
** Played on piano by Gabriel Dell
** Sung by Huntz Hall (dubbed by John Laurenz)
** Reprised by Huntz Hall in the nightclub (dubbed by John Laurenz)

* "Lets Have a Heart to Heart Talk"
** Written by Billy Austin, Edward Brandt and Paul Landers
** Played on piano by Gabriel Dell
** Sung by Huntz Hall (dubbed by John Laurenz)
** Reprised by Huntz Hall in the nightclub (dubbed by John Laurenz)

* "You Walk By"
** ritten by Ben Raleigh and Bernie Wayne
** Played by the orchestra with Gabriel Dell on piano
** Sung by Huntz Hall (dubbed by John Laurenz)

* "Better Be Lookin Out for Love"
** Written by Ralph Wolf and Johnny Lange
** Sung by Adele Jergens in a nightclub (dubbed by Gloria Wood)

* "Swanee River"
** Written by Stephen Foster (as Stephen Collins Foster)
** Jazzy version played by Gabriel Dell on piano
** Danced to by William Billy Benedict, David Gorcey and Buddy Gorman

* "Dixies Lan"
** Written by Daniel Decatur Emmett
** Sung by Leo Gorcey

According to a July 3, 1950 Hollywood Reporter news item, singer Bob Carroll was the singing double for Huntz Hall, but reviews credit John Laurenz as his singing double. Carrolls participation in the final film has not been confirmed. 

==References==
 

==External links==
*  
*  
*  

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958 Triple Trouble 1950
| after=Bowery Battalion 1951}}
 

 
 

 
 
 
 
 
 
 
 
 