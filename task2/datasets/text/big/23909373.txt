Good Luck!
 
{{Infobox Film

| name               = Good Luck!   
| image              = Good_Luck!.jpg
| caption            = Theatrical release poster
| director           = Aditya Datt Karan Sharma Jagdish Sharma 
| story              = Rajan Agarwal
| screenplay         = Sanjay Chhel Vibha Singh Sai Kabir
| starring           = Aryeman Ramsay Sayali Bhagat Ranvir Shorey Lucky Ali
| music              = Anu Malik 
| cinematography     = 
| editing            = 
| distributor        = Tips Music Films Om Films Pvt. Ltd.
| released           = August 8, 2008
| language           = Hindi 
| country            = India 
}}
 Indian film Karan Sharma Hollywood film, Just My Luck. The film was released on 8 August 2008.

==Synopsis==

This is a story about an singer/dancer Vicky Verma (Aryeman Ramsay). While he was a top student in his school and college days, he is not having much luck finding work, clients or projects. All he does seems to go wrong. Afterwards, Saba (Sayali Bhagat) one of the most luckiest girls in the city, meets Vicky at a masked-party and the two kiss which swaps their luck around. Vicky gets as lucky as hell, and Saba gets jinxed. After she finds out that her luck changed due to that kiss, Saba goes out looking for Vicky even though she has never seen his face because he was wearing a mask at the party when they kissed. Vicky gets signed a contract deal by Tarun Chopra (Lucky Ali), but its later cancelled when Saba finds him and kisses him to get the luck back. She goes to work, only to get fired, knowing that Vicky was going to use the luck not on him but on his little cousin who has a heart-problem. She once again kisses him to give him the luck back, and Vicky kisses his little cousin and she gets all the luck. After he does that, he ends up winning Sabas love and admiration. After Saba and Vicky get together, Vicky and Sabas lifes change and they becomes prosperous.

==Cast==

* Aryeman Ramsay as Vicky Varma; a jinxed 25-year-old singer looking for a contract deal
* Sayali Bhagat as Saba; a charmed hard-worker
* Lucky Ali as Tarun Chopra; a rich musical contract dealer
* Ranvir Shorey as Javed; a Jig lo
* Nazneen Patel as Kanchan Soni
* Archana Puran Singh in special appearance 
* Sharat Saxena in special appearance
* Viju Khote in special appearance

==External links==
* 

 
 