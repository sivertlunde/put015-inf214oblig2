The Round-Up (1965 film)
{{Infobox film
| name           = The Round-Up
| image          = 
| caption        = 
| director       = Miklós Jancsó
| producer       = 
| writer         = Gyula Hernádi
| starring       = János Görbe Zoltán Latinovits Tibor Molnár
| music          = 
| cinematography = Tamás Somló
| editing        = Zoltán Farkas
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
| gross          = 
}}

The Round-Up ( , "Outlaws") is a 1965 Hungarian film directed by Miklós Jancsó. It was well received in its home country, and was its directors first film to receive international acclaim. The film was selected to be screened in the Cannes Classics section of the 2015 Cannes Film Festival.   

==Plot== 1848 revolution Habsburg rule in Hungary, prison camps were set up for people suspected of being Kossuths supporters. Around 20 years later, some members of highwayman Sándor Rózsas guerrilla band, believed to be some of Kossuths last supporters, are known to be interned among the prisoners in a camp. The prison staff try to identify the rebels and find out if Sándor is among them using various means of mental and physical torture and trickery. When one of the guerrillas, János Gajdar, is identified as a murderer by an old woman, he starts aiding his captors by acting as an informant. Gajdar is told that if he can show his captors a man who has killed more people than himself, he will be spared. Fearing for his life, he turns in several people his captors had been looking for by name, but could not identify among the prisoners.

Eventually Gajdar becomes an outcast among the prisoners, and is murdered at night by some of his fellow inmates while in solitary confinement. The prison guards easily discover suspects, people whose cells had been left unlocked for the night, and start interrogating them with hope of finding Sándor himself. The suspects are tricked into revealing the remaining guerrillas when they are given a chance to form a new military unit out of former bandits and informed that Sándor, who was not among the prisoners, has been pardoned. However, the celebrating guerrillas are then told that those who previously fought under him, will still face execution.

==Cast==
* János Görbe as János Gajdar
* Zoltán Latinovits as Imre Veszelka
* Tibor Molnár as Kabai
* Gábor Agárdy as Torma (as Agárdy Gábor)
* András Kozák as Ifj. Kabai
* Béla Barsi as Foglár
* József Madaras as Magyardolmányos
* János Koltai as Béla Varju
* István Avar as Vallató I
* Lajos Őze as Vallató II

==Production==
The Round-Up was produced by the Hungarian state film production company Mafilm.  It had a budget of 17 million Hungarian forint|forints, or around half a million United States dollar|US$ at the exchange rates of the time.    The screenplay was written by Hungarian author Gyula Hernádi, who Jancsó had met in 1959 and who was a frequent collaborator with the director until Hernádis death in 2005. The film was shot in widescreen in black and white by another regular Jancsó collaborator, Tamás Somló.

The Round-Up does not exhibit many of Jancsós trademark elements to the degree evident later: thus, the takes are comparatively short and although the camera movements are carefully choreographed they do not exhibit the elaborate fluid style that would become distinctive in later films. The film does, though, use Jancsós favourite setting, the Hungarian puszta (steppe), shot in characteristically oppressive sunlight.  The film has little dialogue and rarely shows any emotion in its characters. It has been called by one critic as "a total absorption of content into form". 

==Critical reception== Best Foreign Language Film at the 39th Academy Awards, but was not accepted as a nominee. 
 1956 uprising against Soviet Russia.  Therefore before Jancsó was allowed to screen the film in Cannes, he had to make a declaration stating the film had nothing to do with the recent events in the country, even though he later said that "everybody knew it wasnt true".  Later in 1966, the film was released in the United Kingdom, and in 1969, it received a limited release in the United States.

The film was included in Béla Tarrs list of the 10 greatest films of all time submitted to 2012 poll of Sight & Sound    and Derek Malcolms The Century of Films, a list of 100 of the critics favorite films from the 20th century.   

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 