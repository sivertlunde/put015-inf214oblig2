Bloodthirsty Butchers (film)
{{Infobox film
| name           = Bloodthirsty Butchers
| image          = Bloodthirsty-butchers.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Andy Milligan
| producer       = William Mishkin
| writer         = Andy Milligan John Borske
| screenplay     = 
| story          =
| based on       =  
| narrator       =  Michael Cox Linda Driver Jane Helay Bernard Kaler
| music          = 
| cinematography = Andy Milligan
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
Bloodthirsty Butchers (1970)  is a horror film directed by Andy Milligan. It is an adaptation of the notorious story of Sweeney Todd.

== Plot ==
An updated version of the Sweeney Todd legend, this melodrama tells the tale of a murderous barber, Sweeney Todd, who supplies raw meat for his neighbor, Mrs. Lovett, who runs a pie shop. Amid the resulting carnage is a romantic sub-plot, although the film focuses mainly on the gore. 

== Cast==
The film stars Annabella Wood as Johanna, Berwick Kaler as Tobias, Jane Helay as Mrs. Lovett, and John Miranda as Sweeney Todd.

== Release ==
The film premiered on 23 January 1970 and was re-released in a special screening of the Milligan Mania as part of the Cinedelphia Film Festival on 10 April 2015 over Exhumed Films. 

== References ==
 

== External links ==
*  

 

 
 
 


 