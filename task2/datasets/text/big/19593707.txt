Sailors (film)
{{Infobox film
| name           = Sailors
| image          = 
| caption        = 
| director       = Arne Mattsson
| producer       = Inge Ivarson
| writer         = Lajos Lajtai Arne Mattsson Gösta Rybrant Volodja Semitjov
| starring       = Dirch Passer
| music          = 
| cinematography = Kalle Bergholm Sreco Pavlocic
| editing        = Lennart Wallén
| distributor    = 
| released       = 3 October, 1964
| runtime        = 108 minutes
| country        = Sweden 
| language       = Swedish
| budget         = 
}}

Sailors ( ) is a 1964 Swedish film directed by Arne Mattsson and starring Dirch Passer. 

==Cast==
* Dirch Passer - Sam
* Anita Lindblom - Carmen
* Åke Söderblom - Nappe von Lohring
* Nils Hallberg - Nitouche
* Elisabeth Odén - Eva
* Per Asplin - Bob
* Siv Ericks - Mrs. Plunkett
* Grynet Molvig - Pia
* Carl-Axel Elfving - Fifi
* Arve Opsahl - Månsson
* Lillevi Bergman - Mia
* Tomas Bolme - Big Man / as Bolme, Thomas
* Eric Brage - Ships officer
* Curt Ericson - Olsson
* Sven Holmberg - Second man with draft-order in El Bajo
* Olof Huddén - Swahn
* Nils Kihlberg - Söderman
* Lennart Lindberg - Berg
* Gustaf Lövås - First man with draft-order in Stockholm and El Bajo
* Marianne Mohaupt - Fia
* Cence Sulevska - Ballet dancer
* Hans Wallbom - Sören
* Birger Åsander - Second man with draft-order in Stockholm (uncredited)
* Carl-Gustaf Lindstedt - Ships Doctor (uncredited)

==External links==
* 

 
 
 
 
 