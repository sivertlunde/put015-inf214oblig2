The Prince and the Pauper (1937 film)
{{Infobox film
| name           = The Prince and the Pauper
| image          = The Prince and the Pauper (1937 film).jpg
| image_size     =
| caption        =
| director       = William Keighley William Dieterle (uncredited)
| producer       = Jack L. Warner (uncredited exec. producer) Hal B. Wallis (uncredited exec. producer)
| screenplay     = Laird Doyle Catherine Chisholm Cushing
| based on       =  
| narrator       =
| starring       = Errol Flynn Billy and Bobby Mauch Claude Rains
| music          = Erich Wolfgang Korngold George Barnes
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       =  
| runtime        = 118 minutes
| country        = United States English
| budget         =
| gross          =
}}
 1937 film novel of the same name by Mark Twain. It starred Errol Flynn, twins Billy and Bobby Mauch in the title roles, and Claude Rains.

The film was originally intended to coincide with the coronation of King George VI and Queen Elizabeth in 1936. However, its release was delayed until the following year. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 54-55 
 violin concerto was drawn from the music he composed for this film.

==Plot==
In Tudor England, two boys are born on the same day in the most different circumstances imaginable. Tom (Billy Mauch) is the son of vicious criminal John Canty (Barton MacLane), while Edward VI (Bobby Mauch) is a prince and the heir of King Henry VIII of England (Montagu Love). One grows up in poverty, hungering for something better for himself and his family, the other in isolated luxury, with a strong curiosity about the outside world.

When they are youngsters, they meet and are astounded by their striking resemblance to each other. As a prank, they exchange clothes, but the Captain of the Guard (Alan Hale, Sr.) mistakes the prince for the pauper and throws him out of the palace grounds.
 the Earl of Hertford (Claude Rains) of his identity. Everyone else is convinced that he is mentally ill. When Henry VIII dies, Hertford threatens to expose Tom unless he does as he is told. Hertford also blackmails the Captain into searching for the real prince to eliminate the dangerous loose end.

Meanwhile, Edward finds an amused, if disbelieving protector in Miles Hendon (Errol Flynn). An attempt to assassinate the boy on the instigation of the Earl of Hertford, who fears for his power if the real king lives, changes Hendons opinion of Edwards story. With Hendons help, Edward manages to re-enter the palace just in time to interrupt the coronation ceremony and prove his identity. Tom is made a ward of the new king, Hertford is banished for life, and Hendon is rewarded for his services.

==Cast==
* Errol Flynn as Miles Hendon Billy Mauch as Tom Canty Bobby Mauch Prince Edward/King Edward VI the Earl of Hertford the Duke of Norfolk
* Barton MacLane as John Canty
* Alan Hale, Sr. as the Captain of the Guard
* Eric Portman as the First Lord
* Lionel Pape as the Second Lord
* Leonard Willey as the Third Lord
* Murray Kinnell as	Hugo Hendon Archbishop
* Phyllis Barry as the Barmaid
* Ivan F. Simpson as Clemens
* Montagu Love as Henry VIII of England
* Fritz Leiber as Father Andrew
* Elspeth Dudgeon as John Canty′s mother
* Mary Field as Mrs. Canty
* Forrester Harvey as the Meaty Man
* Joan Valerie as Lady Jane Seymour
* Lester Matthews as St. John Robert Adair as the First Guard
* Harry Cording as the Second Guard Lord Warwick
* Rex Evans as a Rich Man
* Holmes Herbert as the First Doctor
* Ian MacLaren as the Second Doctor  Anne Howard as Lady Jane Grey  Lady Elizabeth
* Lionel Braham as Ruffler
* Harry Beresford as The Watch
* Lionel Belmore as the Innkeeper
* Ian Wolfe as the Proprietor

==Production== The White Angel and The Charge of the Light Brigade. Mauch Twins Rising Stars in Film Sky: Scenario Planned for 11 Year Old Veterans of Vaudeville and Radio.
Shafer, Rosalind. Chicago Daily Tribune (1923-1963)   19 Apr 1936: D4.  They announced The Prince and the Pauper as part of their line up in June 1936. WARNERS TO SHOW 60 FEATURE FILMS: 1936-37 Production Schedule Announced at Convention in Progress Here. GREEN PASTURES LISTED Seven Other Stage Successes to Be Screened -- Adaptation of Anthony Adverse Ready.
New York Times (1923-Current file)   04 June 1936: 27.  (They bought the rights to the story from Twains estate for $75,000. FACTS ON A FEW OF THE NEW PICTURES
New York Times (1923-Current file)   02 May 1937: X4. )

Patrick Knowles was cast for the role of Miles in October. SIDNEY BLACKMER WILL PLAY THEODORE ROOSEVELT ON SCREEN: Spencer Tracy Re-signed; Gets New Role
Schallert, Edwin. Los Angeles Times (1923-Current File)   21 Oct 1936: 15.  However Jack Warner then decided he wanted someone with a bigger name and asked Errol Flynn to do it. ERROL FLYNN CALLED BACK FOR ROLE IN "PRINCE AND THE PAUPER": John Ford to Direct Shirley Temple Film
Schallert, Edwin. Los Angeles Times (1923-Current File)   05 Dec 1936: A7 

==Other works==
The Prince and the Pauper is a novel by Mark Twain with Edward VI of England as the central character. This fictional narrative has been adapted to film a number of times:
* The Prince and the Pauper (1909), a two-reel short that features some of the only known film footage of Mark Twain, shot by Thomas Edison at Twains Connecticut home Hugh Ford and Edwin Stanton Porter; the first feature-length adaptation
*  
* The Prince and the Pauper (1937 film), featuring Errol Flynn as Miles Hendon and Billy and Bobby Mauch as the title characters Guy Williams as Miles Hendon, and Sean Scully in the dual roles of Prince Edward and Tom Canty. Crossed Swords, a 1977 film released in the United Kingdom as The Prince and the Pauper, starring Oliver Reed, Raquel Welch, Ernest Borgnine, George C. Scott, Rex Harrison, and Charlton Heston The Prince and the Pauper (1990 film), an animated short starring Mickey Mouse
* The Prince and the Pauper (2000 film), a television film featuring Aidan Quinn and Alan Bates Dylan and Cole Sprouse

==References==
 

==External links==
*  
*  
*  }
*  
*  

 
 

 
 
 
 
 
 
   
 
 
 
 
 
 
 