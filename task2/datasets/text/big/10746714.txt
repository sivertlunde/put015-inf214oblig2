The Plouffe Family (film)
{{Infobox Film
| name           = Les Plouffe
| image          = 
| image_size     = 
| caption        = 
| director       = Gilles Carle John Kemeny (executive producer)
| writer         = Gilles Carle Jacques Vigoureux Roger Lemelin (also novel)
| narrator       = 
| starring       = Gabriel Arcand Pierre Curzi Juliette Huot Émile Genest Serge Dupire
| music          = Claude Denjean Stéphane Venne
| cinematography = François Protat Yves Langlois
| distributor    = Ciné 360 Inc. (Canada) International Cinema (USA)
| released       =  
| runtime        = 227 min / Canada:165 min (International version) / Canada:169 min (English version) / Canada:259 min (French version)
| country        = Canada
| language       = French
| budget         = 
| gross          = 1,900,000 (Canada)
}}
The Plouffe Family ( ) is a 1981 Canadian drama film, based on Roger Lemelins novel about the titular Plouffe family, set during World War II.

==Awards== Best Achievement Best Adapted Best Performance Best Foreign Language Film at the 54th Academy Awards, but was not accepted as a nominee. 

==See also==
* List of submissions to the 54th Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 