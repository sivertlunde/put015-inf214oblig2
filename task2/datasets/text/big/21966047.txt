Delta of Venus (film)
{{Infobox film
| name = Delta of Venus
| image = Deltaofvenusposter.jpg
| image_size =
| caption = Theatrical release poster
| director = Zalman King
| producer = Evzen Kolar
| writer = Anaïs Nin Elisa M. Rothstein Patricia Louisianna Knop
| narrator =
| starring = Audie England Costas Mandylor Marek Vašut
| music = George S. Clinton
| cinematography = Eagle Egilsson
| editing = James Gavin Bedford Marc Grossman
| studio =
| distributor = New Line Cinema
| released = June 9, 1995
| runtime =
| country =   English
| budget =
| gross = $63,174 
}} erotic story collection of the same name by Anaïs Nin. The book is not autobiographical, nor does it have a frame narrative. The movie has one, about a Nin-like American who begins an affair with another expatriate American in pre–World War II Paris, and who writes erotic stories, representing her fantasies. Some of these stories/fantasies, based on those of Nin, are explored on-screen.The film was directed by Zalman King, and stars Audie England, Costas Mandylor, and Marek Vašut. NC-17 and R-rated versions of the film exist. The NC-17 rating is due to explicit sex.  The DVD release contains both versions.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 


 