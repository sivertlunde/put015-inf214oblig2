Beyond Rangoon
 
{{Infobox film
| name = Beyond Rangoon
| image = Beyond_Rangoon.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = John Boorman
| producer = John Boorman Sean Ryerson Eric Pleskow Barry Spikings
| writer = Alex Lasker Bill Rubenstein
| starring = Patricia Arquette Frances McDormand U Aung Ko Johnny Cheah Adelle Lutz
| music = Hans Zimmer
| cinematography = John Seale
| editing = Ron Davis
| studio = Castle Rock Entertainment
| distributor = Columbia Pictures
| released =  
| runtime = 99 minutes   
| country = United States  
| language = English
| budget = $23 million
| gross = $5,750,110
}} American tourist Burma (Myanmar) in 1988, the year in which the 8888 Uprising takes place. The film was mostly filmed in Malaysia, and, though a work of fiction,  was inspired by real people and real events.
 rallies with military dictators of the State Law and Order Restoration Council (SLORC), and attempt to escape to Thailand.

The film was an official selection at the 1995 Cannes Film Festival, where it was one of the popular hits of the event.   

The film may have had an impact beyond movie screens, however. Only weeks into its European run, the Burmese military junta freed Nobel Peace Prize winner  .   Suu Kyi was re-arrested a few years later, but Beyond Rangoon had already helped raise world attention on a previously "invisible" tragedy: the massacres of 1988 and the cruelty of her countrys military rulers. 

==Plot== Karen rebels. Laura has a dream where her son Danny tells her she has to let him go. Laura and her group make a harrowing river crossing into Thailand under mortar fire and reach a refugee camp. Having found a new purpose in life Laura begins helping at the camp’s hospital.

==Cast==
* Patricia Arquette - Laura Bowman
* Frances McDormand - Andy Bowman
* U Aung Ko - U Aung Ko
* Johnny Cheah - Min Han
* Adelle Lutz - Aung San Suu Kyi
* Spalding Gray - Jeremy Watt
* Tiara Jacquelina - San San, Hotel Desk Clerk
* Kuswadinath Bujang - Colonel at Hotel
* Victor Slezak - Mr. Scott
* Jit Murad - Sein Htoo
* Ye Myint - Zaw Win
* Cho Cho Myint - Zabai
* Haji Mohd Rajoli - Karen Father
* Azmi Hassan - Older Karen Boy
* Ahmad Fithi - Younger Karen Boy

==Reception==
Critical reaction was mixed. Time (magazine)|Time, Rolling Stone, and Entertainment Weekly wrote negative reviews, while the critic for The New Yorker called the film a "fearless masterpiece" and Andrew Sarris declared himself "awestruck" by the film.  The film was a financial success only in France (where it opened number one and gained 442,793 visitors), though it was screened in many European countries. Film critic Tullio Kezich compared the film to Rossellinis classic, Paisà, regretting that it was marred by certain directorial touches.  Beyond Rangoon currently holds a 38% rating on Rotten Tomatoes based on 32 reviews.

==Soundtrack==
 

Beyond Rangoon is also an original soundtrack music album that features in the film of Beyond Rangoon. The majority of the soundtrack was composed by the German composer Hans Zimmer and the album was released in 1999.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
   
   
 
 
 
 
 
 
 
 
 