The Old Mill
 
 
{{Infobox film
| name           = The Old Mill
| image          = The Old Mill poster.jpg
| caption        = Poster for The Old Mill
| alt            = Poster for The Old Mill
| director       = Wilfred Jackson
| producer       = Walt Disney
| writer         =
| narrator       =
| starring       = Walt Disney Productions
| music          = Leigh Harline
| cinematography =
| editing        = RKO Radio Pictures
| released       = November 5, 1937
| runtime        = 9 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1937 Silly RKO Radio Pictures on November 5, 1937. The film depicts the natural community of animals populating an old abandoned windmill in the country, and how they deal with a severe thunderstorm that nearly destroys their habitat. It incorporates the song "One Day When We Were Young" from Johann Strauss IIs operetta The Gypsy Baron.
 Snow White and the Seven Dwarfs. 

==Awards and accolades==
*The Old Mill won the 1937  . 
*It is ranked at IMDb as the 46th most popular short film ever.  The 50 Greatest Cartoons As Selected by 1,000 Animation Professionals.

==Homages==
===Disney California Adventure===
The Old Mill is featured in the World of Color show at Disney California Adventure. 

===Disneyland===
The three mills from the short were seen in miniature on the Storybook Land Canal Boats ride at Disneyland. Beginning December 20, 2014, they were replaced by landmarks from Walt Disney Animation Studios|Disneys Frozen (2013 film)|Frozen. The miniature windmills were put into storage by Walt Disney Imagineering. 

===Disneyland Paris=== Fantasyland at Disneyland Paris by a building resembling a Dutch windmill, which serves drinks and snacks.  

===The Simpsons===
The Old Mill was parodied in  " is heard in the scene where Homer protected the duck from the water wheel. 

==Release== Diamond Editions Blu-ray disc as a special feature. 

== Influence ==
The Old Mill was stated by Japanese director Hayao Miyazaki as his favorite Disney film. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 