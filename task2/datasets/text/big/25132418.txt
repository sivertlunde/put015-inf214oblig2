Cattle Annie and Little Britches
{{infobox film| name = Cattle Annie and Little Britches
| image = Cattle Annie and Little Britches poster.jpg
| director = Lamont Johnson
| writer = David Eyre and Robert Ward from the book by Robert Ward John Savage Rod Steiger Diane Lane Amanda Plummer Scott Glenn Buck Taylor 
| cinematography = Larry Pizer
| released = 1981
}} Western outlaws that they had read about in Ned Buntlines stories and left their homes to join the criminals. It was scripted by David Eyre and Robert Ward from Robert Wards book and directed by Lamont Johnson.

==Plot== Little Britches (Diane Lane), finds a father figure in Doolin, who in the story line coined her nickname "Little Britches". Doolins efforts to live up to the girls vision of him lead him to be carted off in a cage to an Oklahoma jail where he waits to be hanged. With the help of the girls, and the gang, Doolin escapes and rides off to safety with his men. The girls are triumphant, but they cannot escape Marshal Bill Tilghman (Rod Steiger), and are sent back East to the reformatory in Framingham, Massachusetts|Framingham, Massachusetts.

==Reaction==
The film was favorably reviewed by the critic   gives a scarily brilliant performance." 

==References==
 

 

 
 
 
 
 
 