Kamen Rider Den-O & Kiva: Climax Deka
{{Infobox film 
|  name          = Kamen Rider Den-O & Kiva: Climax Deka
| film name      = {{Film name
| kanji          = 劇場版 仮面ライダー電王&キバ クライマックス刑事（デカ）
| romaji         = Gekijōban Kamen Raidā Denō ando Kiba Kuraimakkusu Deka}}
| image          = Climaxdekapostercm3.jpg
| caption        = Promotional poster
| director       = Osamu Kaneda
| writer         = Yasuko Kobayashi
| producer       =   
| starring       =  
| music          = Toshihiko Sahashi
| cinematography = Koji Kurata
| editing        = Naoki Osada Toei
| Toei Co. Ltd 
| released       =  
| runtime        = 70 minutes
| language       = Japanese
| budget         = 
}} Kamen Rider franchise. Climax Deka is the second film to feature the characters of the television drama Kamen Rider Den-O and the first to feature the characters of Kamen Rider Kiva. It opened in theaters on April 12, 2008 as part of the  . The catchphrase for the movie is  . In its first weekend, it took the top spot at the Japanese box offices,  unseating the Japanese release of Cloverfield.  In the end, it grossed 730 million yen, over double the normal revenue for a Kamen Rider film.   佐藤健『電王』3度目映画化に「一番面白い」|accessdate=2008-09-26}}  Though it is considered the first Heisei rider teamup however the story focuses more on Den-O than Kiva.

== Plot == Wataru Kurenai and Shizuka as they are suspiciously digging up items for a new violin varnish. After a small spat with U-Ryotaro and an unintentional possession of Wataru by Momotaros, Ryotaro and Kazuya locate the base of the Evil Organization after Wataru tips them off. There, before being chased out, they find Yuto with the organizations leader, the Imagin Negataros, and a man that Kazuya recognizes as wanted criminal Seiya Kuroki. Acting on his own, Kazuya attempts to arrest Kuroki, only to be ambushed by the Clown Imagin. However, with a drunk Deneb and Naomi as a distraction, Yuto, who has been working incognito, attempts to free Kazuya. Before he can ambush Sarah with a metal bat, he is possessed by Urataros who manages to successfully woo her and wrest the gun away. Yuto kicks Urataros out as he fights her and the Clown Imagin as Kuroki arrives, with K-Ryotaro providing backup. Negataros takes his leave while activating the detonation device with everyone making it out.

At the DenLiner, after Kazuya apologises for almost getting everyone killed and explaining his reasons to be avenging his father Kazuma, Yuto reveals Negataross plans. Despite telling them that hes too strong, the DenLiner Police decide to attack at full strength together. The next day, Negataros begins to set his plan in motion with Kamen Riders Den-O and Zeronos, along with their Imagin partners, standing in his way while Suzuki arrests Kuroki after the DenLiner takes him and his posse away from the fight. However, Negataros uses the Den-O Pass to become Nega Den-O as the fight begins. As the Tarōs deal with the Fangires, Zeronos battles the Clown Imagin while Den-O dukes it out with Nega Den-O. Nega Den-O easily gains the upper hand against all four Den-O forms, but before he can finish him off, Kamen Rider Kiva arrives to turn the tables as Den-O Climax Form joins him in a Double Rider Kick.  The impact forces Negataros out of his Rider form. Refusing to accept defeat, Nega Den-O escapes in the Nega DenLiner with DenLiner and ZeroLiner in pursuit. Castle Doran joins the fight as all three giants destroy the Nega DenLiner, taking out Negataros for good. Soon after, Ryotaro takes Kazuya to 1986 to see Kazuma as he was interviewing Otoya on an investigation, giving the young detective closure. Though they succeeded in their mission to stop Negataros, Momotaros decides to keep the DenLiner Police going as he considers being a cop a cool thing. After they succeed in cornering a couple of crooks, Deneb attempts to ask for Yuto to join the DenLiner Police, failing to see the crooks they cornered escape with the DenLiner Police and Yuto chasing after them.   

== Characters ==
  Singularity Point, and the man destined to become Den-O to fight the Imagin and protect time and space.
*  : Another Singularity Point like Ryotaro,  , as she came to be called as she regressed in age due to certain events in the TV series, helps in protecting time and space while keeping the Tarōs in line. Other than her heightened physical strength, Hana also uses a bazooka as her weapon of choice.
*  : The waitress of the dining car of the DenLiner who is extremely hyper and does not get surprised easily, sticking to her belief of making only coffee despite her batch being poorly made to non-Imagin.
*  : The strange man who owns the DenLiner and has a habit of speaking in riddles when it comes to the nature of time. He becomes Chief Inspector of the DenLiner police, forming to get the Rider Pass back from Negataros.
*  : Ryotaros elder sister, Hanas future mother and owner of the Milk Dipper.
*  : A past incarnation of Airis fiance who disappeared from time and Hanas future father. He has fought alongside Ryotaro as his ally.
*  : Cassanova journalist who is always looking for a new good story to restore his magazine, sometimes revealing Imagin attacks to Ryotaro this way. A regular customer at Milk Dipper, he always tries to court Airi.
*  : A self-styled super counselor, Miura is also a Milk Dipper regular who tries to court Airi. Miura is the only minor character to be somewhat aware of the Tarōs possessions of Ryotaro, believing them to be "evil spirits".
 
*  : A shy young man who teams up with Kivat to fight the Fangire race and protect humanity from them.
*  : Called "Kivat" for short, he is a solid character with dry wit, tending to have a bitter attitude. He formed a partnership with Wataru to give him the power of Kiva.
*  : A 14-year-old girl who is able to get around the shyness of Wataru, being a motherly figure to him as he teaches her how to play the violin.
*  : Watarus father, a ladies man and a master violinist. His exploits of battle against the Fangire take place in 1986.

=== Imagin ===
The   are the main antagonist group in Kamen Rider Den-O. Only the good Imagin have survived the Junction Point while most of the evil Imagin are erased from history. The four   become members of the newly made  , a law-enforcement group to prevent changes in the past by Imagin who also survived the Junction Point and form an army under Negataros. revolver on the movie poster, but single-handedly uses a shotgun while on the Machine DenBird in the movie. Briefly possesses Wataru Kurenai by accident while attempting to possess Ryotaro, thanks to Urataros
*  : The smooth-talking Imagin that possesses Ryotaro, aiding him physically or in Rod Form to fight other Imagin. Urataros is seen using a pistol on the movie poster. Briefly possesses Yuto Sakurai to the latters initial dissatisfaction.
*  : The physically strongest of the Imagin that are linked to Ryotaro, aiding him physically or in Ax Form to fight other Imagin. Kintaros is seen using a shotgun on the movie poster.
*  : The most childish of the Tarōs Ryotaro became contracted to, aiding him physically or in Gun Form to fight other Imagin. Ryutaros is seen using a sniper rifle on the movie poster.
*  : The sole Imagin contracted to Yuto, aiding him physically or in Vega Form to fight other Imagin.

=== Fangire ===
The   are the main antagonist group in Kamen Rider Kiva. Other than the  , the others appeared in only human form before being destroyed by Zeronos Vega Form.

=== Movie-exclusive characters ===
*  : A faint-hearted rookie detective investigating the crimes committed by the Evil Organization. He seems perpetually confused by the details of Ryoutaros possession by the Taros, and R-Ryotaros, U-Ryotaros and M-Watarus antics do not help the situation.
*  : A detective in 1986 who is Kazuyas father. He died on duty, just before Kazuya was born.
*  : A criminal on the wanted list who aid Negataros in return for world domination. In the end, he is arrested by Kazuya after a fist fight.
*  : Kurokis right hand and the Clown Imagins contract holder, she is defeated by Kohana. 
*  : The villain of the movie, founder of the Evil Organization that he terms as the "Negataros Corps". He and a select few Imagin are stray Imagin who evaded the fate of the other evil Imagin when Kai ceased to exist.
*  : One of Negataross Imagin followers. His form is based on legend of the  . His contractor is a robber who is working with the Horse Fangire.
*  : One of Negataross Imagin followers. His form is based on the story The   from The Picture-Book without Pictures. His contractor is Sarah.

== Short film == pirate movie. animal movie. By the time Kivat throws in his own ideal movie, a biopic telling his life story from 22 years ago, the credits to the short start up and he bites Momotaros in frustration while the other Tarōs leave to see the movie.

== Cast ==
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :   of Run&Gun|RUN&GUN
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  ,  
*  :  
*  :  
*  :  ,  ,  

=== Voice actors ===
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :  

== Songs ==
; Opening theme
* "Climax Jump DEN-LINER form"
** Lyrics: Shoko Fujibayashi
** Composition: Shuhei Naruse
** Arrangement: LOVE+HATE
** Artist: Momotaros, Urataros, Kintaros, Ryutaros (Toshihiko Seki, Kōji Yusa, Masaki Terasoma, Kenichi Suzumura)
; Insert songs:
* "Double-Action"
** Lyrics: Shoko Fujibayashi
** Composition & Arrangement: LOVE+HATE
** Artist: Ryotaro Nogami & Momotaros (Takeru Satoh & Toshihiko Seki)
* "Double-Action Rod form"
** Lyrics: Shoko Fujibayashi
** Composition: LOVE+HATE
** Arrangement: Yōichi Sakai
** Artist: Ryotaro Nogami & Urataros (Takeru Satoh & Kōji Yusa)
* "Double-Action Ax form"
** Lyrics: Shoko Fujibayashi
** Composition: LOVE+HATE, Shuhei Naruse
** Arrangement: Shuhei Naruse
** Artist: Ryotaro Nogami & Kintaros (Takeru Satoh & Masaki Terasoma)
* "Double-Action Gun form"
** Lyrics: Shoko Fujibayashi
** Composition: LOVE+HATE, Shuhei Naruse
** Arrangement: Shuhei Naruse
** Artist: Ryotaro Nogami & Ryutaros (Takeru Satoh & Kenichi Suzumura)
* "Action-ZERO"
** Lyrics: Shoko Fujibayashi
** Composition & Arrangement: LOVE+HATE Yuichi Nakamura & Hōchū Ōtsuka)
* "Real-Action"
** Lyrics: Shoko Fujibayashi
** Composition & Arrangement: Ryo (of defspiral)
** Artist: Ryotaro Nogami (Takeru Satoh)
* "Climax Jump HIPHOP ver."
** Artist: AAA Den-O form
; Ending theme
* "Double-Action CLIMAX form"
** Lyrics: Shoko Fujibayashi
** Composition: LOVE+HATE, Shuhei Naruse
** Arrangement: Shuhei Naruse
** Artist: Momotaros, Urataros, Kintaros, Ryutaros, Deneb (Toshihiko Seki, Kōji Yusa, Masaki Terasoma, Kenichi Suzumura, Hōchū Ōtsuka)
*: "Double-Action CLIMAX form" is an arrangement of "Double-Action" that is the only quintet vocalist arrangement. Its single was released on April 16, 2008.  Shuhei Naruse arranged and composed the song.  Like "Climax Jump DEN-LINER form", the single has variant covers which designate its bonus track, one for each of the Imagin vocalists. These singles also include a DVD of the music video. A sixth single has all five Imagin on the cover, but does not have a bonus track nor a DVD.  On its first day of release, the singles reached #3 on the Oricon Daily charts.  It ended its first week on the Oricon at #4 on the Weekly Single Charts. 

== References ==
 

== External links ==
*  
*   - Kamen Rider Den-O & Kiva: Climax Deka official website

 
 
 

 
 
 
 
 