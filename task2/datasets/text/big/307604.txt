Renaissance Man (film)
 
{{Infobox film
| name           = Renaissance Man
| image          = Renaissance man poster.jpg
| caption        = Theatrical release poster
| director       = Penny Marshall
| producer       = Sara Colleton Elliot Abbott Penny Marshall Robert Greenhut
| writer         = Jim Burnstein
| starring       = Danny DeVito Gregory Hines James Remar Cliff Robertson 
| music          = Hans Zimmer Adam Greenberg George Bowers Parkway Productions Cinergi Pictures Buena Vista Pictures
| released       =  
| runtime        = 128 minutes
| country        = United States
| language       = English
| budget         = $40,000,000 (estimated) 
| gross          = $24,332,324 
}}
Renaissance Man is a 1994 American comedy film directed by Penny Marshall, and stars Danny DeVito, Gregory Hines, James Remar and Cliff Robertson. In Australia, the film is known under the title of Army Intelligence.

==Plot==
Bill Rago (DeVito) is a divorced advertising executive down on his luck. When he loses his job in   training base, Fort McClane.

Initially unenthusiastic, Rago finds that he has only six weeks to teach a group of "squeakers," or low achievers, the basics of comprehension and use of English language. Most of the soldiers are only semi-literate and equally unenthusiastic.

Unable to connect with his pupils and desperate to spark their interest, Rago quotes from his favorite play, Hamlet by William Shakespeare. They are unfamiliar with it (or even the concept of a "play") and a small initial spark of interest is generated. He casts each student as a character in a classroom reading, then takes everyone on a field trip to a live performance by professionals. He introduces them to Shakespeares Henry V as well.

Despite the disapproval of their hard-as-nails Drill Sergeant Cass (Hines), and the loss of one of the trainees, who is revealed as a drug dealer hiding under an assumed identity, Rago sets an end-of-term examination. Even the friendly Capt. Murdoch in charge of the project doesnt expect the soldiers to pass Ragos class, adding that if they fail, they will be discharged from the Army.

While on duty, on a dare from Cass in front of other men, one of the soldiers recites the St. Crispins Day Speech by King Henry V while in full combat gear in the middle of a rainstorm during a night exercise; the speech moves even the hardened Sgt. Cass. The students then all pass Ragos class, with flying colors.

Rago meets and dates Marie, another soldier in the records department, who helps him do some investigation before the bases graduation ceremony. It results in one of his students being awarded a medal his father was to have been given posthumously, after he was killed in Vietnam.

As the proud soldiers march at their passing-out parade, Rago is saluted by his "graduates." He signs on to continue teaching soldiers-in-training.

 

==Cast==
 
*Danny DeVito as Bill Rago, an advertising executive who loses his job. He applies for unemployment and is ultimately offered a temporary teaching job on a U.S. Army base. Bitter and frustrated, he finds himself unable to comprehend the strict structure of the Army.
*Gregory Hines as Sergeant Cass, a strict Drill Sergeant, who makes it clear to Rago upon their first meeting that he finds classroom teaching of this civilians "students" to be a waste of time and money, mockingly calling him "Shakespeare." In turn, Rago nicknames him "Lou".
*James Remar as Captain Tom Murdoch, an official at Fort McClane and a supporter of Ragos, who is always busy but finds himself amused by Bills initial frustrations.
*Cliff Robertson as Colonel James 
*Ed Begley, Jr. as Jack Markin, Ragos advertising partner, who does his best to defend Bill when he comes under fire for losing a business deal. Henry V with Rago and the class, he buys a copy of the play to read himself, and later recites the St. Crispins Day Speech for Sgt. Cass while on night maneuvers.
*Stacey Dash as Private Miranda Myers (Ophelia), the sole female soldier in the class. Prior to joining the army, Mirandas mother ran off with a man and left her alone. While riding a bus for two days she found herself staring at an Army recruitment sign with the slogan "Be all you can be," so she enlisted.
*Kadeem Hardison as Private Jamaal Montgomery (Ghost of Hamlets Father), a wise-cracking soldier who jokes around and taunts everyone in class.
*Richard T. Jones as Corporal Jackson Leroy (Laertes), a serious soldier (and Montgomerys best friend), who had previously been an all-star football player before an injury forced him to give up his dream. He is bitter about having been encouraged to only pursue sports, leaving him with only a handful of options. He is married and a father-to-be.
*Khalil Kain as Private Roosevelt (Nathaniel) Hobbs (Hamlet), an intelligent, mysterious soldier who intrigues Rago to encourage officials to explore his background in hopes of promoting him. While the research is being conducted, the officials uncover that he is actually a drug dealer who joined the Army with a fake identity to escape warrants for his arrest from selling crack. His discharge and arrest devastates Rago and the rest of his students.
*Peter Simmons as Private Brian Davis, Jr., a soldier needled by others for continually discussing his deceased father, who was killed in the Vietnam War before he was born. When roles for the class version of Hamlet are handed out, hes not thrilled about being cast as Queen Gertrude but accepts it after it is revealed that all parts were played by men in Shakespeares time. Rago proves this by performing a Juliet soliloquy.
*Gregory Sporleder as Private Melvin Melvin (Polonius), a redneck soldier, well known for falling asleep under any situation. It is implied that he enlisted to escape the abuse he endured from his stepfather. Upon receiving a letter, he learns that his younger siblings have begun to receive the beatings in his place. During the final exam, he notes that all royal characters in the play are either killed or commit suicide, and the only two characters who live on are "Fortinbras and Horatio... a soldier, and a student... aint that something?"
*Mark Wahlberg as Private Tommy Lee Haywood (King Claudius), a tough-talking soldier from a trailer park, who had previously worked in a mill with his father before it closed down. He enlists in the Army hoping to see the world. Due to his objection of Private Davis being cast as the Queen (because he is the King), for once he takes more teasing than Davis.
*Alanna Ubach, as Emily Rago, Bills daughter. It is Bills continuing desire to provide for her that he takes "any" job.
*Isabella Hofmann as Marie, a soldier who sparks a romantic interest in Bill.

==Filming== Fort Jackson, Point Edward, Port Huron, Michigan.

==Reaction== Private Benjamin but does not have the warmth or spirit of those films". He also wondered what Devitos character teaching Shakespeares plays had to do with the training of the military recruits. Ebert gave it a thumbs-down on his television show, but partner Gene Siskel enjoyed it as pleasant fare and gave it a thumbs-up. It currently holds a 17% rating on Rotten Tomatoes.
 The Flintstones, and The Lion King.  After failing to draw in much of an audience as a comedy, the film was marketed as a drama and re-released a few months later under the title By the Book, again without much box office success.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 