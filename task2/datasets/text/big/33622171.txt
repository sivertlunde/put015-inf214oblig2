Keep It for Yourself
{{Infobox film
| name           = Keep It for Yourself
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Claire Denis
| producer       = Philippe Carcassonne Ted Hope
| writer         = Claire Denis
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Michael James
| music          = John Lurie
| cinematography = Agnès Godard
| editing        = Dominique Auvray
 
| studio         = Allarts Good Machine
| distributor    = 
| released       =  
| runtime        = 40min
| country        = United States France Netherlands
| language       = English
| budget         = 
| gross          = 
}}

Keep It for Yourself is a 1991 black and white drama short film.

==Plot==
Sophie comes to New York from France with the intention of meeting up with a man she met a few months before. She finds herself alone in the mans apartment, and she discovers that he left town because he was scared stiff at the idea of seeing her.

==Cast==
* Sophie Simon
* Sarina Chan
* Michael James
* E.J. Rodriguez
* Jim Stark
* James Schamus
* Michael Stun
* Sara Driver
* Vincent Gallo

==Notes==
* The French director Claire Denis hired Vincent Gallo to act in several films such as the "short film Keep It for Yourself, the made-for-TV U.S. Go Home, and its follow-up feature Nénette et Boni (1996)." 
* Claire Denis preferred black faces in her movies at first. "Vincent Gallo is an old face for me - the first time I shot him was 10 years ago in a short I made in New York called Keep it for Yourself". 

==References==
 
*  
*  
*  

 
 