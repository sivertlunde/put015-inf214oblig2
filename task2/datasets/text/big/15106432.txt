Trouble the Water
 

{{Infobox film
| name           = Trouble the Water
| image          = Trouble the water.jpg
| caption        = Theatrical release poster
| director       = Tia Lessin Carl Deal
| producer       = Tia Lessin Carl Deal
| starring       = 
| music          = Davidge Del Naja Black Kold Madina
| cinematography = PJ Raval and Kimberly Rivers Roberts
| editing        = T. Woody Richman
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 2008 documentary film produced and directed by Tia Lessin and Carl Deal, producers of Fahrenheit 9/11.  Trouble the Water is a redemptive tale of a couple surviving failed levees, bungling bureaucrats, and their own troubled past and a portrait of a community abandoned long before Hurricane Katrina hit, featuring music by Massive Attack, Mary Mary, Citizen Cope, John Lee Hooker, The Roots, Dr. John and Blackkoldmadina. Trouble the Water is distributed by Zeitgeist Films and premiered in theaters in New York City and Los Angeles on August 22, 2008, followed by a national release in more than 200 theaters. It had its television premiere on HBO and has been rebroadcast on National Geographic Channel and Turner Classic Movies.  Trouble the Water is available on DVD.

==Synopsis==

Trouble the Water opens the day the filmmakers meet 24-year-old aspiring rap artist and ex-drug dealer Kimberly Rivers Roberts and her husband Scott at a Red Cross shelter in central Louisiana, then flashes back two weeks, with Kimberly turning her new video camera on herself and her neighbors trapped in their Ninth Ward attic as the storm rages, the levees fail and the flood waters rise. 

Weaving 15 minutes of Roberts ground zero footage shot the day before and the morning of the storm, with archival news segments, other home video, and verité footage they filmed over two years, director/producers Tia Lessin and Carl Deal document the journey of a young couple living on the margins who survive the storm and seize a chance for a new beginning.

Trouble the Water explores issues of race, class, and the relationship of government to its citizens, issues that continue to haunt America, years after the levees failed in New Orleans.

==Awards and nominations==
The film was nominated for an     
as well as the Grand Jury Award, The Kathleen Bryan Edwards Award for Human Rights, and the Working Films Award at the 2008 Full Frame Documentary Festival, and the Special Jury Prize at the 2008 AFI/Silverdocs Festival.
 IFC Gotham Award for best documentary and the Council on Foundation’s Henry Hampton Award. It has also been nominated for an NAACP Image award for outstanding documentary and a Producers Guild of America award.

Named best documentary of 2008 by the African-American Film Critics Association and the Alliance of Women Film Journalists, and came in 2nd place for the National Film Critics Circle Award.

===List of Awards===
* 2009 Academy Award  Nominee, Best Documentary Feature
* 2009 Gotham Independent Film Award  for Best Documentary
* 2009 NAACP Image Award Nominee, Outstanding Documentary
* 2009 Producers Guild of America For Feature Documentary (Nominated)
* 2008 Sundance Film Festival Grand Jury Prize
* 2008 Full Frame Documentary Festival Grand Jury Prize
* 2008 AFI/Silverdocs Special Jury Prize
* 2008 Council On Foundations Henry Hampton Award for Excellence In Film And Digital Media
* 2008 Working Films Award
* 2008 Kathleen Bryan Human Rights Award
* 2010 Harry Chapin Media Award
* Official Selection, 2008 New Directors/New Films (Museum of Modern Art And Film Society of Lincoln Center)

==Critical reception==
The film appeared on several critics top ten lists of the best films of 2008.     

==References==
 

==External links==
*  
* 

 
 
{{succession box
| title= 
| years=2008 Manda Bala
| after=We Live in Public}}
 

 
 
 
 
 
 
 
 
 
 
 
 