High Risk (1981 film)
{{Infobox film
| name           = High Risk
| image          = 
| image_size     = 
| caption        = 
| director       = Stewart Raffill John Daly (executive producer) Gerald Green (producer) Joseph Raffill (producer)
| writer         = Stewart Raffill (writer)
| narrator       = 
| starring       = See below
| music          = Mark Snow
| cinematography = Álex Phillips Jr.
| editing        = Tom Walls
| distributor    = 
| released       = 
| runtime        = 94 minutes 90 minutes (Argentina video)
| country        = USA, Mexico, UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

High Risk is a 1981 American/Mexican/UK adventure/heist film directed by Stewart Raffill and stars James Brolin, Lindsay Wagner, Cleavon Little, James Coburn, Ernest Borgnine and Anthony Quinn. 

==Plot==

During a period of sluggish economy and high unemployment, four friends—Stone (  to rob a drug lord. They meet with Clint (Ernest Borgnine), who supplies them with weapons and target practice. It becomes clear that with the exception of Stone, the men have no relevant training and are not taking the heist seriously.

The men are flown to a jungle. Their target is Serrano (James Coburn), a sadistic drug lord who lives in a large villa. The group observes Serrano, his staff and various visitors from a hidden vantage point. They discuss whether they would have the courage to actually kill someone, and what they will do with their share of the cash once they return home.

They infiltrate the villa and capture Serrano and his lover, and force Serrano to reveal the combination to his safe. They escape from the villa with five million dollars in cash. They travel on foot through the jungle, eventually finding horses to buy. Believing that they have been successful, the men camp for the night and celebrate. They are unaware that they are being observed by a rebel tracker. 

When they wake the next day, Serrano’s men and the local military are pursuing them in all-terrain vehicles and on horseback. They flee, but Tony and Rockney are captured by Serrano. Stone and Dan have to continue on foot and only escape their pursuers by leaping from very high rope bridge.

Serrano has Rockney beaten to force Tony to reveal the location of money. Knowing that Stone has his share of the cash and that they face better odds of escape in the open, Tony agrees to show them the rest of the money.

Serrano has them taken to a prison. They meet Olivia (Lindsay Wagner), the prisoner in the neighboring cell, who claims she was falsely arrested. Tony bribes a passing child to pull out the barred window of their cell with a tow truck. The child demands their clothes in payment also, and they are pursued virtually naked through the town. They lose their pursuers in a bordello. Dressed in womens clothes, Tony and Rockney make their way to a bus station and reunite with Olivia. They travel together to the rendezvous point.

Dan and Stone are captured by a local rebel leader, General Mariano (Anthony Quinn), who wants the stolen money for himself. They escape, then turn and track the rebels until nightfall. They are forced to kill several of the rebels before they retrieve the cash.

The rebels pursue them to the rendezvous point, where they expect to be airlifted to safety. Dan is wounded by Mariano’s tracker. In the ensuing firefight, Tony is shot trying to protect Stone.

Believing Tony is dead, the others continue to the airstrip without him. Serrano’s mercenaries have arrived ahead of them, and a three-way gun battle ensues. Serranos men subdue the rebels. The retrieval plane arrives and provides covering fire, scattering Serrano’s men. The plane lands, the team boards hurriedly, and they make their escape. They are celebrating their success when the engine stalls.

==Cast==
*James Brolin as Stone
*Anthony Quinn as Mariano
*Lindsay Wagner as Olivia
*James Coburn as Serrano
*Ernest Borgnine as Clint
*Chick Vennera as Tony
*Bruce Davison as Dan
*Cleavon Little as Rockney

==Soundtrack==
* The Rolling Stones - "(I Cant Get No) Satisfaction" (Jagger/Richards)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 