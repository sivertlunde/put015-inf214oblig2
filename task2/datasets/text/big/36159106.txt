Angels Hard as They Come
{{Infobox film
| name           = Angels Hard as They Come
| image          =
| caption        =
| director       = Joe Viola
| producer       =  
| script         = Jonathan Demme Joe Viola
| based on       =
| starring       = Scott Glenn Charles Dierkop Gary Busey
| music          =
| cinematography =
| editing        =
| studio         = New World Pictures
| distributor    = New World Pictures
| released       =  
| runtime        = 
| country        = United States English
| budget         = $180,000 Roger Corman & Jim Jerome, How I Made a Hundred Movies in Hollywood and Never Lost a Dime, Muller, 1990 p 204  or $125,000 Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 99 
| gross          = $2 million Ed. J. Philip di Franco, The Movie World of Roger Corman, Chelsea House Publishers, 1979 p 160 
}}
Angels Hard as They Come is a 1971 biker film produced by Jonathan Demme. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 30 

==Production==
Jonathan Demme met Roger Corman while doing publicity on Von Richthofen and Brown. The producer was impressed by publicity material Demme had written and asked if he was interested in writing a motorcycle movie. Demme pitched the idea of a motorcycle version of Rashomon and wrote it with Joe Viola, who directed TV commercials Demme had produced. 

==Reception==
The film was very successful with Corman saying it earned rentals of over $700,000 and returning a profit of 46% within the first year. 

==References==
 

==External links==
*  at IMDB
*  at TCMDB

 
 
 
 
 
 

 