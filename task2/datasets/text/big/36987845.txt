Le dossier Toroto
{{Infobox film
| name           = Le Dossier Toroto
| image          = Dossier_toroto_aff.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Jean-Pierre Mocky
| producer       = 
| writer         = Jean-Pierre Mocky
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Vladimir Cosma
| cinematography = Jean-Paul Sergent
| editing        = Michel Cosma
| studio         = Mocky Delicious Products
| distributor    = 
| released       =  
| runtime        = 64 minutes
| country        = France
| language       = French
| budget         = €37,000
| gross          = 
}} French film directed by Jean-Pierre Mocky.

== Cast ==
* Jean Abeillé (Professeur Toroto)
* Jean-Pierre Mocky (Le professeur Lapine)
* Romain Gontier (Riri)
* Olivier Hémon (Le Brigadier)
* Guillaume Delaunay (Vitupin 1)
* Lionel Laget (Vitupin 2)
* Emmanuel Nakach (Joseph)
* Pamela Ravassard (Marie)
* Jean Luc Atlan (Docteur Klaus)
* Anksa Kara (Irma, lintendante)
* Raphaël Scheer (Le Nonce du Pape)
* Christian Chauvaud (Marco Le chef des grosses bites)
* Idriss (Marco 2)
* Pascal Lagrandeur (Membre des grosses bites)
* Julie Baronnie (Mme Baron)
* Serge Bos (Le prêtre)
* Fabien Jegoudez (Le professeur Suédois)
* Noël Simsolo (Mr Noir / Blanc)
* Marie Noelle Pigeau (Mme Noir / Blanc)
* Mauricette Gourdon (Ida)
* Jean Christophe Herbert (Père Blanc)
* Jana Bittnerova (Femme du Brigadier)
* Cyrille Dobbels (Politique 1)
* Alain Schlosberg (Politique 2)
* Christophe Bier (Politique 3)
* Patrick Lebadezai (Politique 4)
* Michel Stobac (Colonel Pascal au cul poilu)
* Eric Cornet (Lieutenant)
* Alexis Wawerka (Moine)
* Jean Philippe Bonnet (Moine 2)
* Fabrice Colson (Robert)
* Laure Hennequart (Infirmière)
* Françoise Armelle (Chienne de Garde)
* Clément Bobo (Riri 10 ans)
* Charlotte Ciprey (Cousine Riri)
* Joelle Hélary (Dame Marriage 1)
* Valérie Lejeune (Dame Marriage 2)

== See also ==
* Cinema of France
* List of French language films

==External links==
*  

 
 
 
 

 