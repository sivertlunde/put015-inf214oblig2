My Village at Sunset
{{Infobox film   
| name           = My Village at Sunset 
| image          = My Village at Sunset poster.jpg
| image_size =
| caption        = Poster
| director       = Norodom Sihanouk
| producer       = Norodom Sihanouk
| writer         = Norodom Sihanouk
| starring       = San Chariya, Roland Eng
| music          = 
| cinematography =    
| editing        = 
| distributor    = 
| released       = 1992
| runtime        = 63 minutes
| country        = Cambodia
| language       = Khmer
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1992 Cinema Cambodian romantic drama film directed by Norodom Sihanouk.

==Plot summary==
  doctor returns from Paris to volunteer his services at a clinic in his home village in rural Cambodia, just outside of the capital city. Most of his patients are victims of land mines. A love triangle of sorts comes to pass as he falls in love with a young nurse at the clinic, while fending off the advances of his cousin (unhappily married to an amputee war hero).

==Cast==
*San Chariya   
*Roland Eng   
*Ros Sarocun

==External links==
*  

 
 
 
 


 
 