Strawberry Fields (2011 film)
{{Infobox film
| name           = Strawberry Fields
| image          =
| caption        = 
| director       = Frances Lea
| producer       = Liam Beatty Lucie Wenigerova
| writer         = Judith Johnson Frances Lea
| starring       = Anna Madeley Emun Elliott Christine Bottomley
| music          = Bryony Afferson James Stone
| release        =
| cinematography = Dave Miller
| editing        = Cinzia Baldessari
| released       =  
| runtime        = 94 minutes
| country        = England
| language       = English
| budget         = 
| gross          = 
| distributor    = BBC Films
}}

Strawberry Fields is a 2011 light psycho-drama film set in the Kent countryside of England during the summer fruit picking season. It was filmed entirely on location in Kent county,  and the background reflects the extensive fruit growing economy of Kent (sometimes called "the Garden of England").

==Plot==
The plot follows the exploits of two sisters, Gillian and Emily, recently bereaved by the death of their mother, and with a long history of a strained and troubled relationship. Both sisters have psychological problems, the results of which provide the key story lines for the film. The cast are all itinerant fruit pickers on the fictional Rymans Farm. Kev, an older Scottish picker (played by Emun Elliott) who has grandiose but unfulfilled plans for his future, and Fabio (played by Jonathan Bonnici), a headstrong and sexually demanding young Italian picker, provide the material for the underlying sexual tension of the film. At heart the principal storyline follows Gillians attempts to escape from Emily, frustrated by her own conflicting desire to care for her mentally ill sister. 

==Production==
The film was produced by Spring Pictues, working in association with Film London, BBC Films, Screen South, Screen East, MetFilm, and the Kent County Council Film Office. Additional funding was provided by the National Lottery through the UK Film Council, and the office of the Mayor of London.

==References==
 

==External links==
*  

 
 