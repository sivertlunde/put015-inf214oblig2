Viktor (film)
{{Infobox film
| name           = Viktor
| film name      = Russian: Виктор
| image          = Viktor film poster.jpg
| image size     =
| alt            =
| caption        = Viktor film poster
| director       = Philippe Martinez
| producer       =  
| writer         = Philippe Martinez
| starring       =  
| music          =  
| cinematography = Jean-François Hensgens
| editing        = Thomas Fernandez
| studio         =  
| distributor    =  
| released       =   
| runtime        = 134 miniutes
| country        =  
| language       =  
| budget         =
}}

Viktor  (  .  Starring Gérard Depardieu and Elizabeth Hurley, the film premiered September 2, 2014, in Moscow and debuted in New York City on October 24, 2014.
                  The project was filmed entirely in Russia and,  being set in Moscow,  it was filmed in locations in both Moscow and the Chechnyan capital of Grozny.      

==Production==
London-based production company Saradan Media announced May 15, 2013, that Liz Hurley and Gérard Depardieu were  about to begin filming an action film project in Russia under a working title of Turquoise and directed by Philippe Martinez.     On May 22, Saradan Media revealed that "Tourquoise" and Viktor were the same project.    Filming began in Grozny on Saturday May 18, 2013 with expectations that the film would release in late 2013.      The film actually released in Russia September 2014.   

==Synopsis==
The plot revolves around the story of a former gangster Victor Lambert (Gérard Depardieu), a professional thief specializing in the theft of works of art. After serving prison time in France for an art heist, he returns to his Russian home to investigate the circumstances surrounding the brutal murder of his son Jeremys (Jean Baptiste Fillon) three months earlier.  He enlists the assistance of his former accomplice Alexandra Ivanov (Elizabeth Hurley).

==Cast==
 
* Gérard Depardieu as Victor Lambert
* Elizabeth Hurley as Alexandra Ivanov
* Eli Danker as Souliman
* Polina Kuzminskaya as Katerina
* Evgeniya Akhremenko as Inspector Plutova
* Igor Yatsko as Police Officer Vadim
* Frédéric Ducastel as shooter, bad guy
* Jean Baptiste Fillon as Jeremy
* Pavel Klimov as Egor
* Marika Gvilava as Tatiana
* Marcello Mazzarella as Claudio Martelli
* Denis Karasyov as Anton Belinskiy  
* Kait Tenison as Herself
* Alexei Petrenko as Vetrov
* Solenn Mariani
 

==Critical response==
Rotten Tomatoes gave the film a metascore rating of 27, based upon generally poor reviews.    The film was well received in France,       but generally considered a flop in its US release, with many US sources describing Depardieus girth as making him unbelievable as an action hero. 

The Detroit News wrote "In his younger days, Depardieu had a burly charm, but the dude is 65 now and has to weigh 300 pounds. He looks ludicrous in action sequences".  The Hollywood Reporter wrote "Depardieu, age 65 and looking like he can barely move due to his massive girth..."  continuing "the film is relentlessly tedious when its not being laughable", sharing that the "latter aspect is exemplified by the supposed romantic relationship between the morbidly obese Depardieu and the gorgeous Hurley, whose adoring gazes toward her co-star demonstrate that her acting abilities have perhaps been underestimated".     San Francisco Weekly wrote "The 66-year-old Depardieu was no action hero even in his prime, and as his girth now rivals Paul Masson-era Orson Welles, were meant to believe he strikes fear into his enemies".     Film Journal International writes of the films weak points, stating "...sadly, the problem is Depardieu, who lumbers through the film looking as though hes tormented by indigestion rather than a lust for vengeance",  and toward the film,  "Viktor is surprisingly dull".  They did however speak well toward the films cinematography, expressing pleasure at the directors "delivering the genre staples &ndash; car chases, gun battles, a little torture (a reluctant informer), female flesh and endless dark alleys and mean streets &ndash; and Moscow is wall-to-wall with photo ops, from the winding Neva River to the Cathedral of St. Basil, which has made so many movie appearances it should be getting above-the-title billing".     And The Los Angeles Times wrote that seeing "the films corpulent, 65-year-old star, Gérard Depardieu, play a brash killing machine who beds the likes of the gorgeous Elizabeth Hurley is truly like entering some cinematic Bizarro world. Think Charles Durning as Dirty Harry".   The reviewer wrote the film was a "hackneyed jumble ... a fiery car chase, a couple of shootouts and an eyes-averting torture scene fill this competently shot movies action quota. But a tone-switching epilogue proves hokey — and a little spooky".     Daily Mail called the films US release a "spectacular flop".     CBS News noted that while film was "beautifully shot" and director of photography Jean-Francois Hensgens brought out "the stunning Russian landscape", however the films beauty was "brought down by the clunky dialogue and the slow, plodding plot".   

==References==
 

==External links==
*  
*  
*    (French)
*    
*    
*   at Metacritic
*  

 
 
 
 
 
 
 
 

 