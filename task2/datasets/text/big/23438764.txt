There's a Zulu On My Stoep
{{Infobox film
| name           = Theres a Zulu On My Stoep
| image          = Theres A Zulu On My Stoep.jpg
| image_size     =
| caption        = DVD cover
| director       = Gray Hofmeyr
| producer       =
| writer         = Leon Schuster Gray Hofmeyr
| narrator       =
| starring       = Leon Schuster John Matshikiza Wilson Dunster Terri Treas Michelle Bowes Peter Hugo
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1993
| runtime        =
| country        = South Africa English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Theres a Zulu On My Stoep (in other countries, Yankee Zulu) is a 1993 South African comedy film by the comedy filmmaker Gray Hofmeyr starring Leon Schuster and John Matshikiza. It has received considerable popularity and a cult status in South Africa and Eastern Europe.

==Plot==
In Apartheid South Africa, white boy Rhino Labuschagne and Zulu Mashebela were best friends until Rhino, pressured by his American girlfriend, Rowena, shoots a can off Zulus head, abruptly ending their friendship. 25 years later, Zulu has become a car thief in New York, picking up an American accent but not forgetting his roots as "the champion mud slinger of the world". The prisons warden gets Zulu deported back to Africa by the Threatened Immigrants Right-wing Defence, or TIRD.
 Sun City and split the jackpot fifty-fifty. In Sun City, Tinkie befriends Prince William, son of Prince Charles of the United Kingdom. Rhino manages to win R500 000. They are then relentlessly pursued by Diehard and Rowena, but get a make-up artist, Antonio to make Zulu into a white, neo-nazi TIRD, Baron von Mauchausen-Klarks, and Rhino into a black manservant named Moses. Zulu cannot resist going to Rowenas TIRD party in order to pickpocket.

At the party, Diehard and Rowena recognize them after some slapstick antics, but Zulu sets off Diehards intricate security system. They try to flee but are captured by Rowena. Zulu gives Rowena one half of the jackpot cheque, but Rhino has given the other to Tinkie, who has fled. The duo are imprisoned and they settle their differences by re-enacting the tincan incident, only vice versa. Diehard and Rowena take the two to a cliff called "Crocodile Gorge" and put them on a plank supported by an elephant.

Tinkie and Prince William use booby traps to defeat Rowena and severely injure Diehard. Rhino reveals to Zulu that he is Tinkies biological father from Thandi. The two talk the elephant into setting them free. The psychotic Diehard tries to execute Tinkie on a cliff, but Zulu uses his mud throwing skill to knock-out Diehard, sending him plunging off the cliff to his apparent death.

Zulu, Rhino and Tinkie then reunite while William heads off back to his father. In an epilogue, Diehard is seen climbing out of a river muttering "they do not call me Diehard for nothing!". Then his elephant sits on him.

==Cast==
*Leon Schuster as Rhino Labuschagne
*John Matshikiza as Zulu Mashabela
*Wilson Dunster as Gen. "Diehard"
*Terri Treas as Rowena Labuschagne
*Michelle Bowes as Tienkie Labuschagne
*Peter Hugo as Prince Charles
*Skye Svorinic as Prince William
*Ruan Mandelstam as Young Rhino
*Bobo Seritsani as Young Zulu
*Marie Van Deventer as Young Rowena
 

 
 
 
 
 
 