D.O.A. (1950 film)
{{Infobox film
| name           = D.O.A.
| image          = DOA1950.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Rudolph Maté
| producer       = Leo C. Popkin
| writer         = Russell Rouse Clarence Greene
| starring       = Edmond OBrien Pamela Britton Luther Adler Beverly Garland
| music          = Dimitri Tiomkin
| cinematography = Ernest Laszlo
| editing        = Arthur H. Nadel
| studio         = Cardinal Pictures
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

D.O.A., a film noir drama film directed by Rudolph Maté, is considered a classic of the genre.  The frantically paced plot revolves around a doomed mans quest to find out who has poisoned him, and why.  .  This film marks the debuts of Beverly Garland (as Beverly Campbell) and Laurette Luez. 

The film stars Edmond OBrien and Pamela Britton.

Leo C. Popkin produced D.O.A. for his short-lived Cardinal Pictures, but failed to renew the copyright in 1977,  so that it has fallen into the public domain. The Internet Movie Database shows that 22 companies offer the VHS or DVD versions, and the Internet Archive (see below) offers an online version.

==Plot==
The film begins with what a BBC reviewer called "perhaps one of cinemas most innovative opening sequences."  The scene is a long, behind-the-back tracking sequence featuring Frank Bigelow (Edmond OBrien) walking through the hallway of a police station to report his own murder. Oddly, the police almost seem to have been expecting him and already know who he is.

 
 flashback begins with Bigelow in his hometown of Banning, California, where he is an accountant and notary public. He decides to take a one-week vacation in San Francisco, but this does not sit well with Paula Gibson (Pamela Britton), his confidential secretary and girlfriend, as he does not want her to accompany him.
 Beat subculture. second opinion confirms the grim diagnosis, and the other doctor implies that the poisoning must have been deliberate. Bigelow remembers his drink tasted strange.
 Los Angeles, first meeting Miss Foster (Beverly Garland) (whose on-screen credit reads "Beverly Campbell"), the secretary, and then Mr. Halliday (William Ching), the companys comptroller, who tells him Eugene Phillips committed suicide. From there the trail leads to Phillips widow (Lynn Baggett) and brother Stanley (Henry Hart).

The key to the mystery is a bill of sale for what turns out to be stolen iridium. Bigelow had notarized the document for Eugene Phillips six months earlier. He connects Phillips mistress, Marla Rakubian (Laurette Luez), to gangsters led by Majak (Luther Adler). They capture Bigelow. Since Bigelow has learned too much, Majak orders his psychotic henchman Chester (Neville Brand) to kill him. However, Bigelow manages to escape.

Bigelow thinks Stanley and Miss Foster are his killers, but when he confronts them he finds Stanley has been poisoned too&mdash;after having dinner with Mrs.&nbsp;Phillips. He directs them to call an ambulance and tells them what poison has been ingested so that, in Stanleys case at least, prompt treatment may save his life. Stanley tells Bigelow he found evidence that Halliday and Mrs.&nbsp;Phillips were having an affair. Bigelow realizes that the theft was merely a diversion. Eugene discovered the affair and Halliday killed him.

Halliday and Mrs. Phillips used the investigation of the iridium as a cover for their crime, making it seem that Eugene Phillips had killed himself out of shame. However, when they discovered that there was evidence of his innocence in the notarized bill of sale, Halliday murdered anyone who had knowledge of the bill of sale. Bigelow tracks Halliday down and shoots him to death in an exchange of gunfire.
 flashback comes to an end. Bigelow finishes telling his story at the police station and dies, his last word being "Paula." The police detective taking down the report instructs that his file be marked "Dead on arrival|D.O.A."

==Cast==
{| class="wikitable"   
|- Pamela Britton as Paula Gibson
|- Lynn Baggett as Mrs. Phillips 
|- Henry Hart Henry Hart as Stanley Phillips 
|- Neville Brand as Chester 
|- Virginia Lee as Jeannie
|-
|}

Rest of cast:

* Jess Kirkpatrick as Sam
* Cay Forester as Sue
* Frank Jaquet as Dr. Matson
* Lawrence Dobkin as Dr. Schaefer
* Frank Gerstle as Dr. MacDonald Carol Hughes as Kitty

== Critical response ==
The New York Times, in its May 1950 review, described it as a "fairly obvious and plodding recital, involving crime, passion, stolen iridium, gangland beatings and one mans innocent bewilderment upon being caught up in a web of circumstance that marks him for death". OBriens performance had a "good deal of drive", while Britton adds a "pleasant touch of blonde attractiveness".  

In 1981 Foster Hirsch carried on a trend of more positive reviews, calling Bigelows search for his own killer noir irony at its blackest.  He wrote, "One of the films many ironies is that his last desperate search involves him in his life more forcefully than he has ever been before... Tracking down his killer just before he dies &mdash; discovering the reason for his death &mdash; turns out to be the triumph of his life."  Critic A. K. Rode notes Rudolph Matés technical background, writing,  D.O.A. reflects the photographic roots of director Rudolph Maté. He compiled an impressive resume as a cinematographer in Hollywood from 1935 (Dantes Inferno, Stella Dallas, The Adventures of Marco Polo, Foreign Correspondent, Pride of the Yankees, and Gilda, among others) until turning to directing in 1947. The lighting, locations, and atmosphere of brooding darkness were captured expertly by Mate and director of photography Ernest Lazlo.   Michael Sragow, in a review of a DVD release of the film, characterized it as a "high-concept movie before its time." 

In 2004, D.O.A. was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant."

==Production==
The shot of Edmond OBrien running down Market Street (between 4th and 6th Streets) in San Francisco was a "stolen shot," taken without city permits, with some pedestrians visibly confused as OBrien bumps into them. D.O.A. producer Harry Popkin owned the Million Dollar Theater at the southwest corner of Broadway and Third Street in downtown Los Angeles, directly across the street from the Bradbury Building, where OBriens character confronted his murderer. Director Rudolph Maté liberally used Broadway and the Bradbury Building during location shooting and included the Million Dollar Theaters blazing marquee in the background. The theater would later serve the same function when Ridley Scott filmed Blade Runner at the Bradbury Building. The Bradbury Building still exists at 304 South Broadway. 

After "The End" and before the listing of the cast, a credit states the medical aspects of this film are based on scientific fact, and that "luminous toxin is a descriptive term for an actual poison."

The bop jazz band playing at the Fishermans Club while OBriens glass is being spiked was filmed on a Los Angeles soundstage after principal photography was completed. According to  , also known as James Von Streeter. Other band members were Shifty Henry (bass), Al "Cake" Wichard (drums), Ray LaRue (piano), and Teddy Buckner (trumpet). However, rather than use the live performance, the music director went back and rerecorded the soundtrack with a big band, not a quintet as seen in the film, led by saxophonist Maxwell Davis.  Film score was composed by Dimitri Tiomkin. 

==Remakes==
D.O.A. was dramatized as an hour-long radio play on the June 21, 1951, broadcast of Screen Directors Playhouse, starring Edmond OBrien in his original role.

The film was remade in Australia in 1969 as Color Me Dead, directed by Eddie Davis.  In 1988 it was filmed again as D.O.A. (1988 film)|D.O.A., directed by Annabel Jankel and Rocky Morton, with Dennis Quaid as the protagonist. 

In 2011, the   staged a world-premiere musical based on the classic film noir. D.O.A. a Noir Musical was written and adapted by Jon Gillespie and Matthew Byron Cassi, directed by Cassi, with original jazz and blues music composed by Jaime Ramirez.  

==See also==
* List of films in the public domain

==References==
;Notes
 

== External links ==
 
 

*  
*  
*  
*  
*  
*  
*   complete film on YouTube

 
 

 

 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 