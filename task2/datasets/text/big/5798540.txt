The Hottie and the Nottie
 
{{Infobox film
| name           = The Hottie and the Nottie
| image          = Hottie and the nottie.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Tom Putnam
| producer       = Myles Nestel Victoria Nevinny Neal Ramer Hadeel Reda Executive: Paris Hilton
| writer         = Heidi Ferrer Joel David Moore Christine Lakin
| music          = David E. Russo
| cinematography = Alex Vendler
| editing        = Jeff Malmberg Paris Hilton Entertainment Purple Pictures Summit Entertainment
| distributor    = Regent Releasing
| released       =  
| runtime        = 91 minutes  
| country        = United States
| language       = English
| budget         = $9 million
| gross          = $1,596,232 
}} Joel David Moore, and Christine Lakin. Written by Heidi Ferrer and directed by Tom Putnam, the film began shooting in January 2007 and was released theatrically on February 8, 2008.
 she received that year.

==Plot==
Nate Cooper is unable to get it together with women. But he also cannot forget his first crush, the tall, attractive, blonde Cristabel Abbott, from their time in elementary school. Nate sets out for the beaches of California and meets up with his geeky best friend Arno, whose mother has an unnatural amount of information about Cristabel, and perhaps an unusual relationship with her son.

Cristabel jogs on the beach every day with many suitors trying to catch her eye, including an albino stalking|stalker. But shes still single, and there is a reason: Cristabel is still best friends with the same short, unattractive brunette girl whom Nate also knew in elementary school, June Phigg.

Nate reintroduces himself to Cristabel and they hit it off. However, Cristabel refuses to go on a date with Nate unless June has a date as well. Nate sets out to find a boyfriend for June, but guys recoil at the sight of her. One day at the Santa Monica Pier, Johann Wulrich, an attractive dentist who works as a part-time model, appears in their lives. He seems to want to do a makeover on June when he apparently sees her inner beauty. However, Nate believes that Johann is a threat to his shot for Cristabel, since Johann is almost perfect. Eventually, with June dating Johann, Cristabel finally begins dating Nate, per his original plan.

Over the next few weeks, as Nate and June become friends and she emerges from her cocoon, with her face and appearance transforming into that of an attractive woman whose beauty begins to compare with Cristabel, Nate slowly realizes that June may be the girl of his dreams. Nate tells this to Cristabel, who is happy for June. Nate then tries to find June, and finds her, telling her how he feels.

==Cast==
 
* Paris Hilton as Cristabel Abbott
** Karley Scott Collins as young Cristabel Joel David Moore as Nate Cooper
** Caleb Guss as young Nate
* Christine Lakin as June Phigg
** Alessandra Daniele as young June
* The Greg Wilson as Arno Blount
** Kurt Doss as young Arno
* Marianne Muellerleile as Mrs. Blount
* Johann Urb as Johann Wulrich
* Scott Prendergast as Randy ("Creepy Albino Stalker")
* Morgan Rusler as Stan ("Marry Me" Guy)
* Adam Kulbersh as Cole Slawsen
* Kathryn Fiore as Jane
 

==Reception==
===Box office===
Opening on February 8, 2008, The Hottie and the Nottie grossed $9,000 on opening day,  and it went on to earn $27,696 on its opening weekend, each theater averaging $249.  The Houston Chronicle determined that, based on average ticket prices, this represented an average of 35 people per theatre on its opening weekend, or an average of five per showing.  In the end, the film grossed $1,596,232 worldwide. 

For its release in the United Kingdom, the film was advertised as "The Number One Film", with small print revealing its being number 1 on the Internet Movie Databases Bottom 100. The film opened at number 32 and grossed $34,231 in 28 theaters in the UK.

===Critical response===
The film received near-universally negative reviews from critics. Review aggregator website Rotten Tomatoes reported that 4% of critics gave the film positive reviews, based on 67 reviews, concluding that "The Hottie and the Nottie is a crass, predictable, and ineptly staged gross-out comedy that serves little purpose beyond existing as another monument to Paris Hiltons vanity."  Metacritic reported the film had an average score of 7 out of 100, based on 18 reviews — indicating "overwhelming dislike." 

 , reviewing The Hottie and the Nottie, called the film "the worst movie ever made".  

John Anderson of Newsday, while panning the film, was one of a handful of critics to see merit in it. He gave the film two out of four stars and concluded that "The Hottie and the Nottie is no worse in many ways than a lot of teen-centric comedies, which generally appeal to their audience through cruelty and vulgarity."

The film featured on several worst of 2008 lists, including that of The Times,  New York Post,  The Star-Ledger,  The Tart,  and Metromix. 

===Awards and nominations===
{| class="wikitable"
|-
!Award
!Category
!Nominee
!Result
|- Golden Raspberry Razzie Award Golden Raspberry Worst Actress Paris Hilton
| 
|- Golden Raspberry Worst Screen Couple
| 
|- Joel David Moore
| 
|- Christine Lakin
| 
|- Golden Raspberry Worst Screenplay Heidi Ferrer
| 
|- Golden Raspberry Worst Picture Hadeel Reda
| 
|- Neal Ramer
| 
|- Myles Nestel
| 
|- Victoria Nevinny
| 
|- Golden Raspberry Worst Director Tom Putnam
| 
|}

==Home media==
The Hottie and the Nottie was released on DVD on May 6, 2008. The DVD includes a production Audio commentary|commentary, an actors commentary, and two featurettes.

==See also==
* List of films considered the worst

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 