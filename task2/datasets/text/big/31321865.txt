Mrs Dalloway (film)
 
{{Infobox film
| name           = Mrs Dalloway
| director       = Marleen Gorris
| producer       = Stephen Bayly
| screenplay     = Eileen Atkins
| based on       =  
| starring       = Vanessa Redgrave Natascha McElhone Michael Kitchen Lena Headey Alan Cox
| music          = Ilona Sekacz
| cinematography = Sue Gibson
| editing        = Michiel Reichwein
| released       =  
| runtime        = 97 mins
| country        = UK USA Netherlands English
}}

Mrs Dalloway is a 1997 British drama film directed by Marleen Gorris and starring Vanessa Redgrave, Natascha McElhone and Michael Kitchen.  It is an adaptation of the novel Mrs Dalloway by Virginia Woolf. It is a co-production by the United Kingdom, United States and the Netherlands.

==Plot==
Clarissa Dalloway sets out on a beautiful morning; shes shopping for flowers for her party that evening. At the same time in London, a young man is suffering from a nightmarish delayed-onset (the year is 1923) form of shell-shock. Clarissas nearly-grown daughter is distant, and preoccupied. In the course of one day, Peter, a passionate old suitor, returns from India, there is a suicide, Clarissa relives a day in her youth (and her reasons for her choice of a life with the reliable Richard Dalloway).

==Partial cast==
* Vanessa Redgrave – Mrs Clarissa Dalloway
* Natascha McElhone – Young Clarissa
* Michael Kitchen – Peter Walsh Alan Cox – Young Peter
* Sarah Badel – Lady Rosseter
* Lena Headey – Young Sally
* John Standing – Richard Dalloway
* Robert Portal – Young Richard
* Oliver Ford Davies – Hugh Whitbread
* Hal Cruttenden – Young Hugh
* Rupert Graves – Septimus Warren Smith
* Amelia Bullmore – Rezia Warren Smith
* Margaret Tyzack – Lady Bruton
* Robert Hardy – Sir William Bradshaw
* Richenda Carey – Lady Bradshaw
* Katie Carr – Elizabeth Dalloway
* Selina Cadell – Miss Kilman
* Amanda Drew – Lucy
* Phyllis Calvert – Aunt Helena

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 

 