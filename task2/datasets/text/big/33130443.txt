The Three Musketeers (1961 film)
{{Infobox film
| name           = The Three Musketeers 
| image          = Semur.jpg

| caption        = Semur-en-Auxois, one of the locations

| director       = Bernard Borderie

| producer       = Films Borderie Les Films Modernes Le Film dArt Fono Roma

| writer         = Bernard Borderie et  Jean-Bernard Luc  

| based on       =  | }}
| starring       = Gérard Barray Mylène Demongeot Perrette Pradier Georges Descrières

| music          = Paul Misraki

| cinematography = Armand Thirard

| editing        = Christian Gaudin

| distributor    = Pathé|Pathé Distribution

| released       =  

| runtime        = 186 minutes

| country        = France  Italy

| awards         = gross = 4,471,861 admissions (France)   at Box Office Story 
| language       = French

| budget         = 
}}
  novel by Alexandre Dumas, père which consists of two parts.   The script keeps close to the classic French novel. The director treats all the classic characters with respect, not making fun of any of them, although there is humour when dArtagnan rides his peculiar horse and when Planchet supplies wine for the heroes.

The films remarkable location shots were made in Bois de Boulogne, around and in the Château de Guermantes in Seine-et-Marne and in Semur-en-Auxois (department Côte-dOr).

The settings, costumes and props are very elaborate and provide the impression of historic accuracy.   Bernard Borderie and his crew demonstrated here already the qualities which later contributed substantially to the success of his series of five costume drama films about Anne Golons heroine Angelique (French series)|Angelique.  An American in Paris, Xanadu (film)|Xanadu) had provided as “dArtagnan” in an earlier adaption, the fencing in this film looks less like dancing and more dangerous. But of course Borderie also knew how to present a fist fight. When dArtagnan defends Mme Bonacieux against a couple of the cardinals thugs, the director does not only use dramatic sound effects but furthermore lets Barrays punches look more explosive by taking out frames very precisely when he is about to hit. He is also capable of making us believe an outnumbered man could really win the day if only certain circumstances are given, because in Borderies films the thugs are often so overly keen on decking the hero that they actually hinder each other to succeed.

==Cast==

* Gérard Barray as dArtagnan

* Mylène Demongeot as Milady de Winter

* Perrette Pradier as Constance Bonacieux
 Athos

*   as Porthos

*   as Aramis

* Jean Carmet as Planchet
 Count De Rochefort

*   as Cardinal Richelieu

*   as ([[Queen consort|
Queen]]) Anne of Austria

* Robert Berri as M. Bonacieux

*   as Comte de Troisville|M. de Tréville
 Louis XIII
 Duke of Buckingham
==Reception==
The film was the sixth most popular movie at the French box office in 1961. 
==External links==
* 

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 

 