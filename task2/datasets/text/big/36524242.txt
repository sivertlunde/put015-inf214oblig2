Bambai Ki Sair
{{Infobox film
| name           = Bambai Ki Sair
| image          = 
| image_size     = 
| caption        = 
| director       = Sarvottam Badami
| producer       = Sudama Productions
| writer         =
| narrator       = 
| starring       = Shobhna Samarth E. Billimoria Sabita Devi Jal Merchant
| music          = Khemchand Prakash 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1941
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1941 and directed by Sarvottam Badami for Sudama Productions.     The music direction was by Khemchand Prakash with lyrics by D. N. Madhok, Munshi Dil, B.R. Sharma and Pandit Indra.  The film starred Shobhna Samarth, Sabita Devi, E. Billimoria, Jal Merchant, Arun, Vatsala Kumtekar and Ghory. 

==Cast==
* Shobhana Samarth
* Sabita Devi
* Aroon
* Vatsala Kumtekar
* E. Bilimoria
* Jal Merchant
* Ghory

==Music==
The music was composed by Khemchand Prakash with lyrics written by D. N. Madhok, Munshi Dil, B.R. Sharma, Pandit Indra. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer 
|-
| 1
| Chhed De Sajni (Jinki Jawani Unka Zamana)
| Shobhna Samarth, Kanti Lal
|-
| 2
| Kheenchi Chitvan Chadhe Tevar
| Shobhna Samarth, Kanti Lal
|-
| 3
| Meri Ankhein Sharaabi
| Vatsala Kumtekar
|-
| 4
| Raat Pade Mat Rooth Deewani
| Kanti Lal
|-
| 5
| Woh Baat Ki Hai Tune Bas Mazaa Aa Gaya
| Vatsala Kumtekar, Kanti Lal
|-
| 6
| Tere Nainon Mein Naina Daale
| Vatsala Kumtekar
|-
| 7
| Jaaye Na Paayi Ho Hamari Gali Aay Ke
| Vatsala Kumtekar
|-
| 8
| Taare Kyun Muskayein
| Rajkumari, Kanti Lal, Vatsala Kumtekar
|-
| 9
| Gar Zindagi Mein Pyar Ho 
|
|}

==References==
 

==External links==
*  

 

 
 
 


 