Kusruthi Kuruppu
{{Infobox film
| name           = Kusruthi Kuruppu
| image          =
| caption        =
| director       = Venugopan
| producer       =
| writer         =
| screenplay     = Meena Jagathy Innocent KPAC Lalitha Johnson
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1998 Cinema Indian Malayalam Malayalam film, directed by Venugopan. The film stars Jayaram, Meena (actress)|Meena, Jagathy Sreekumar, Innocent (actor)|Innocent, and KPAC Lalitha in lead roles. The film had musical score by Johnson (composer)|Johnson.   

==Cast==
 
*Jayaram Meena
*Jagathy Sreekumar Innocent
*KPAC Lalitha
*Sankaradi
*Indrans
*Oduvil Unnikrishnan
*Paravoor Bharathan
*Sudheesh
*T. P. Madhavan
 

==Soundtrack== Johnson and lyrics was written by Gireesh Puthenchery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maanathe maamayile || K. J. Yesudas || Gireesh Puthenchery || 
|-
| 2 || Melleyen kannile (d) || K. J. Yesudas, KS Chithra || Gireesh Puthenchery || 
|-
| 3 || Melleyen kannile   || KS Chithra || Gireesh Puthenchery || 
|-
| 4 || Peelimukil Thaazhvarayil || MG Sreekumar || Gireesh Puthenchery || 
|-
| 5 || thaarmakalkkanpulla || K. J. Yesudas || Gireesh Puthenchery || 
|}

==References==
 

==External links==
*  

 
 
 

 