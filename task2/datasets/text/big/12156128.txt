Demented Death Farm Massacre
{{Infobox film
| name = Demented Death Farm Massacre
| image = Demented death farm massacre dvd cover.jpg
| caption = DVD cover for Demented Death Farm Massacre
| director = Donn Davison Fred Olen Ray
| producer = Donn Davison Fred Olen Ray
| writer = Barbara Morris Davison
| narrator = John Carradine
| starring = Ashley Brooks George Ellis Trudy Moore Mike Coolik
| music =
| cinematography = Avrum M. Fine
| editing = Avrum M. Fine
| distributor = Troma Entertainment
| released =  
| runtime = 90 minutes
| language = English
| budget = $300
}}

Demented Death Farm Massacre is a 1971 horror film directed by Fred Olen Ray and Donn Davison and features screen legend John Carradine as The Judge of Hell, who narrates the story.

==Plot==
A group of jewel thieves on the lam run out of fuel in the middle of the countryside. They wander into a backwoods farm, hoping to hide out for the time being. However, when the farmer returns home only to find the thieves taking over the house, he hatches a deadly plan.

==Cast==
*Ashley Brooks as Reba Sue Craven
*George Ellis as Harlan P. Craven
*Trudy Moore as Karen
*Mike Coolik as Kirk
*Jim Peck as Phillip
*Pepper Thurston as Susan
*Valarie Lipsey as Madame Jessabelle
*John Carradine as the Judge of Hell

==Other titles==
*Honey Britches
*Moonshiners Women
*Shantytown Honeymoon
*Hillbilly Hooker
*Little Whorehouse on the Prairie

==Release==
Producer Fred Olen Ray bought the obscure, and barely released, 1972 film Shantytown Honeymoon and shot several new scenes with John Carradine as the Judge of Hell. After a limited theatrical release, Olen Ray sold the film to Troma in 1986 under the current title who released it on VHS. Troma would also release it in early DVD packages alongside lesser known titles that had been purchased for distribution. 

The film is currently available on DVD by Troma Entertainment.

Tagline: First we plant the perversion! Then we harvest the horror!

==External links==
* 
*  – at the Troma Entertainment movie database

 
 
 
 
 

 