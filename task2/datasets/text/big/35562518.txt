Wardat
{{Infobox film
 | name = Wardat
 | image = WardatMithun.jpg
 | caption = 
 | director = Ravikant Nagaich
 | producer = Ravikant Nagaich
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Kajal Kiran Iftekhar Jagdeep Kalpana Iyer Shakti Kapoor
 | music = Bappi Lahiri
 | lyrics = Ramesh Pant
 | choreographer = 
 | released = April 24, 1981
 | runtime = 130 min.
 | language = Hindi Rs 1 Crore
 | preceded_by = 
 | followed_by = 
 }}
Wardat is a Hindi film made in 1981. The film is the sequel to Surakshaa. A genre of James Bond spy thriller (Gunmaster G9 being the code for the hero). Large locusts attack the farmers and farmlands resulting in hefty damages. The Indian Government suspects that this is the work of some terrorists and assign this case to their best agent Gunmaster G-9 alias Gopinath, who takes charge and begins the investigation, that will lead him to the dark secret world of underground scientists who are quite capable of harnessing nature to make their ends meet. 

==Cast==
* Mithun Chakraborty .. as ... Gunmaster G-9 / Gopinath  
* Kaajal Kiran .. as ... Kajal Malhotra  
* Iftekhar .. as ... Chief  
* Shakti Kapoor .. as ... Shakti Kapoor  
* Kalpana Iyer .. as ... Anuradha  
* Jagdeep .. as ... Kabadi  
* Keshto Mukherjee .. as ... Dharamdas  
* Birbal   

==Soundtrack==
The songs of the film are being composed by Bappi Lahiri, written by Ramesh Pant and Faruk Kaiser and singers are Mohammad Rafi, Usha Mangeshkar, Bappi Lahiri, Usha Uthup, Annette Pinto & Shailendra Singh

{| class="wikitable "
|-
! Track !! Title !! Singer(s) !! Music !! Lyricist !! Length
|-
| 1 || "Dekha Hai Maine Tujhko Phir" || Bappi Lahiri || Bappi Lahiri || Ramesh Pant || 05:58 
|-
| 2 || "Sara Jahan Chhod Ke Tujhe" || Mohammad Rafi, Usha Mangeshkar || Bappi Lahiri || Faruk Kaiser || 05:00
|-
| 3 || "Tu Mujhe Jaan Se Bhi Pyara Hai" || Usha Uthup, Bappi Lahiri || Bappi Lahiri || Ramesh Pant || 04:29 
|-
| 4 || "Din Ho Ya Raat Tu Pyar Kiye" || Annette Pinto, Bappi Lahiri || Bappi Lahiri || Ramesh Pant || 05:09 
|-
| 5 || "Na Main Hun Tera Na Tu Hain" || Bappi Lahiri, Usha Uthup || Bappi Lahiri || Faruk Kaiser || 07:29
|-
| 5 || "Zaalim Duniya Hum Pe" || Shailender Singh|| Bappi Lahiri || Ramesh Pant || 05:20 
 
|}

==External links==
*  

 
 
 
 
 

 