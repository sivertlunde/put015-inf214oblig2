The Eleventh Hour (1912 film)
 
{{Infobox film
| name           = The Eleventh Hour
| image          = 
| image_size     =
| caption        = 
| director       = Franklyn Barrett
| producer       = 
| writer         = 
| based on       = play by Leonard Willey
| narrator       =
| starring       = 
| music          =
| cinematography = Franklyn Barrett
| editing        = 
| distributor    = 
| studio   = Wests Pictures
| released       = 13 April 1912
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}

The Eleventh Hour is a 1912 Australian silent film. It is considered a lost film.

==Plot==
The script is based on a play "showing the adventures and vicissitudes in the life of a Girl Telegraphist". 

The action consisted of four acts:
*Act 1 – Pangs of Jealousy
*Act 2 – Bad Blood
*Act 3 – The Distress Call
*Act 4 – The Eleventh Hour

==Cast==
*Cyril Mackay
*Sidney Stirling
*Leonard Willey
*Charles Lawrence
*Loris Brown
*Irby Marshall

==Release==
The film was shot in Sydney and released in that city in 1912. It screened in London in September 1913 under the title Saved by Telegram. 

The critic from the Sydney Morning Herald said that "the story is a thrilling one, whilst the cinematographic work of Mr. Franklyn Barrett, the West expert, is particularly good." 

==References==
 

==External links==
* 
 

 
 
 
 
 
 


 