Muss 'Em Up
{{Infobox film
| name           = Muss Em Up
| image          = File:Muss Em Up lobby card.jpg
| alt            =
| caption        = Lobby card
| film name      = 
| director       = Charles Vidor Doran Cox (assistant)
| producer       = Pandro S. Berman
| writer         = Erwin Gelsey
| screenplay     = 
| story          = 
| based on       =  
| starring       = Preston Foster Margaret Callahan
| narrator       = 
| music          = Roy Webb
| cinematography = J. Roy Hunt Joseph August
| editing        = Jack Hively RKO Radio Pictures
| distributor    = 
| released       =    |ref2= }}
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Muss Em Up is a 1936 American detective drama film directed by Charles Vidor from a screenplay by Erwin Gelsey. RKO Radio pictures premiered the film in New York City on February 1, 1936, with a nationwide opening on February 14.  The film stars Preston Foster and Margaret Callahan, with a supporting cast which includes Alan Mowbray, Ralph Morgan, Big Boy Williams, and Maxie Rosenbloom.

==References==
 