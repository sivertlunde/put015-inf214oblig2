The Bowery (film)
{{Infobox film | name = The Bowery
  | image          = The Bowery poster.jpg |
  | caption        = Theatrical release poster
  | director       = Raoul Walsh|
  | producer       = Joseph M. Schenck Darryl F. Zanuck|
  | writer         = Howard Estabrook James Gleason Michael L. Simmons (novel) Bessie Roth Solomon (novel)|
  | starring       = Wallace Beery George Raft Jackie Cooper Fay Wray Pert Kelton| Alfred Newman|
  | cinematography = Barney McGill
  | editing        = Allen McNeil|
  | studio         = 20th Century Pictures
  | distributor    = United Artists
  | released       =  |
  | runtime        = 92 minutes |
  | language       = English |
  | country        = United States |
  | budget         =|
| gross = $2 million (US & Canada rentals) Everett Aaker, The Films of George Raft, McFarland & Company, 2013 p 42-43  |
}}

The Bowery is a 1933 historical film about the Lower East Side of Manhattan around the start of the 20th century directed by Raoul Walsh and starring Wallace Beery and George Raft.

==Production background==
  and Fay Wray]] Beery as Raft as Steve Brodie, the first man to jump off the Brooklyn Bridge and live, Jackie Cooper as a pugnacious child, Fay Wray (in the same year as King Kong) as the leading lady, and Pert Kelton (the first "Alice Kramden" on Jackie Gleasons The Honeymooners) as a bawdy young dance hall singer.

The film is an absorbing presentation of the views and behaviors common at the time. The movie opens with a close-up of a saloon window featuring a sign saying "Nigger Joes" in large letters (the name of an actual Bowery bar from the period).

Coopers character has a habit of throwing rocks at people in Chinatown. When Beerys character berates him for doing so, Coopers character responds, "They was just Chinks," whereupon Beery immediately softens, saying "Awww..." while affectionately mussing the boys hair. At one point, Coopers character breaks a window, knocking over a kerosene lamp and causing a lethal fire that spreads through the block.

==Cast==
 
*Wallace Beery as Chuck Connors Steve Brodie
*Jackie Cooper as Swipes McGurk
*Fay Wray as Lucy Calhoun
*Pert Kelton as Trixie Odbray
*Herman Bing as Max Herman
*Oscar Apfel as Ivan Rummel
*Ferdinand Munier as Honest Mike
*George Walsh as John L. Sullivan Carrie A. Nation

==Plot== New Yorks volunteer fire brigades, and wager $100 on which will be the first to throw water on the fire.

Although Brodie is first to arrive, he finds Connors young pal, Swipes McGurk (Jackie Cooper|Cooper), sitting on a barrel placed over the fire hydrant preventing Brodie from using it first. Connors arrives and the rival fire fighters brawl as the fire reduces the building to a smoldering ruin, presumably incinerating the crowd of Chinese trapped inside who had been screaming for help at the window. Brodie vows revenge on Connors, leading to a $500 bet that a Boxing|fighter, whom Brodie calls "The Masked Marvel," can beat "Bloody Butch" a prizefighter managed by Conners. Conners accepts, and the "Marvel" knocks out Bloody Butch with one punch. After the fight, the "Marvel" is revealed to be John L. Sullivan (George Walsh).
 call on her. They soon fall in love, and Brodie reveals his ambition to run a saloon bigger than Connors.
 dummy made temperance activist Carrie Nation and her band of women arrive at Connors saloon to tear it down with axes and hatchets. When he sees Brodie lifted in a parade after making the jump, however, Connors encourages the activists to destroy the saloon, which they do.
 war is declared against Spain, Connors enlists in an effort to get away from the Bowery, where he is no longer a big shot. When he returns to his apartment to pack, he finds that Swipes has returned and reconciles with the boy. Professional rivals of Brodies then find Connors and deceitfully tell him that Brodie did not actually jump from the bridge, showing him the dummy. Connors demands Brodie give his saloon back. Brodie denies using the dummy, and the two have a long fight on a barge in the East River to settle their differences. After Connors returns victorious, he is arrested for assault and battery with intent to kill. Brodie, however, refuses to implicate him. As Brodie recovers, Connors visits his hospital only to begin another fight, but Swipes stops them and urges them to become friends. After they shake hands, Connors dares Brodie to join him in Cuba. At a parade for departing soldiers, Connors tells Lucy to kiss Brodie goodbye, and after she does, she also kisses Connors. The men lament not being able to say goodbye to Swipes, but they soon see, to their delight, that he is hiding in an artillery box on the supply wagon just ahead of them.

==Production==
The Bowery bears some resemblances to a concurrent movie She Done Him Wrong, a film starring Mae West and Cary Grant released earlier the same year by a different studio (Paramount Pictures) featuring Wallace Beerys older brother Noah Beery, Sr. in a similar role.

Raoul Walsh had directed a groundbreaking film about the Bowery as far back as 1915.  Regeneration (1915 film)|Regeneration (1915), shot on location in the Bowery during the same year that Walsh played John Wilkes Booth in D.W. Griffiths The Birth of a Nation, was the first gangster movie and features tattered clothing on cast members far more ragged than anything seen in a more recent film.

==Reception==
The film was Twentieth Centurys second most popular movie of the year. The Year in Hollywood: 1984 May Be Remembered as the Beginning of the Sweetness-and-Light Era
by Douglas W. Churchill. Hollywood. New York Times (1923-Current file)   30 Dec 1934: X5. 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 