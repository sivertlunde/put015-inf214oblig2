Before Winter Comes
{{Infobox film
| name = Before Winter Comes
| image = Before the winter comes (movie).jpg
| image_size =
| caption = Theatrical release poster
| director = J. Lee Thompson
| producer = Robert Emmett Ginna
| writer = Andrew Sinclair (screenplay)  Frederick L. Keefe (story)
| narrator = Topol Anna Karina
| music = Ron Grainer 
| cinematography = Gilbert Taylor
| editing = Willy Kemplen
| studio =
| distributor =
| released = January 1969, London
| runtime = 107 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}}
Before Winter Comes is a 1969 British film directed by J. Lee Thompson from a screenplay by Andrew Sinclair.

==Plot==
Before Winter Comes takes place in the immediate aftermath of World War II. British Major Giles Burnside (David Niven) is assigned to an Austrian refugee camp; his mission is to send the groups of displaced civilians to either the Russian or the American zone. Burnside is a by-the-books commander but runs into trouble with the intertranslations of many different languages. However, of the refugees, Janovic (Chaim Topol|Topol) is willing to help as he can speak many languages. Janovic quickly conveys Burnsidess orders and helps the way station run smoothly. Janovic runs into romance with a lovely innkeeper, Maria (Anna Karina). But his love with her stops when he discovers her affair with Burnside. Meanwhile, Janovic is found to be a Russian deserter, and must be returned to the Russian mainland to be executed. Burnside offers to help him escape, but Janovic cant decide whether to trust his commander. 

==Cast==
* David Niven as Major Burnside Topol as Janovic
* Anna Karina as Maria
* John Hurt as Lieutenant Pilkington
* Anthony Quayle as Brigadier Bewley
* Ori Levy as Captain Kamenev John Collin as Sergeant Woody
* Karel Stepanek as Count Kerassy
* Guy Deghy as Kovacs
* Mark Malicz as Komenski
* Gertan Klauber as Russian major
* Hana Maria Pravda as Beata
* George Innes as Bill
* Tony Selby as Ted
* Hugh Futcher as Joe
* Christopher Sandford as Johnny
* Colin Spaull as Alf
* Larry Dann as Al
* Jeffry Wickham as Captain Roots
* Alysoun Austin as A.T.S. driver
* John Savident as British corporal

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 