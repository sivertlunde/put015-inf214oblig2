Madhya Venal
{{Infobox Film
| name           = Madhya Venal
| image          = Madhya Venal.jpg
| image_size     = 
| caption        = 
| director       = Madhu Kaithapram
| producer       = Jahangir Shamz
| writer         = Anil Mukhathala
| narrator       = 
| starring       = Manoj K. Jayan  Shweta Menon  Arun Bala
| music          = Kaithapram Viswanathan
| cinematography = M.J. Radhakrishnan
| editing        = Venugopal
| distributor    = Xarfnet Movies Release
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Madhya Venal is a 2009 Malayalam film produced by Jahangir Shamz under the banner of Xarfnet Movies. The movie is directed by Madhu Kaithapram starring Manoj K. Jayan and Shweta Menon.  

==Plot==
Sarojini (Swetha Menon) is a house wife and is also a social worker. In her village, majority of the villagers depend on the Khadi weaving mills for their livelihood. Sarojini too runs a mill. Her husband Kumaran (Manoj K. Jayan) is a political worker. Praveen (Arun Bala) who is an executive from a new generation bank comes to help the villagers. Without any proper documents he lends money to them. Being innocent they do not realize the trick behind Praveens business.

Sarojini and Kumaran try their level best to make the people understand Praveens intention. Meanwhile Praveen fakes love for Manikutty, the daughter of Kumaran and Sarojini, since they were against him.

==Cast==
* Manoj K. Jayan as Kumaran
* Shweta Menon as Sarojini
* Sabitha Jayaraj
* Arun Bala as Praveen Niveditha as Manikutty
* Balachandran Chullikkadu
* Augustine Irshad

==Awards== Kerala State Film Award for Best Male Playback Singer - K. J. Yesudas for "Swantham Swantham Baalyathiloode"

*  Padmarajan Puraskaram/Award  for the Best Movie of 2009 in Malayalam - Madhyavenal.

*  Kerala Film Critics Award  for Second Best Actress - Shwetha Menon for Madhyavenal in 2009.

*   Kerala Film Critics Award  for Madhyavenal - the  film with social relevance and commitment in 2009.

*   Kerala Film Critics Award  for Manoj K Jayan - Special Jury Award for Madhyavenal in 2009.

==References==
 

 
 
 


 