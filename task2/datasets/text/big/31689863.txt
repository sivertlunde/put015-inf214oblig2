On Top of the World (film)
On British comedy film directed by Redd Davis and starring Betty Fields, Frank Pettingell and Leslie Bradley.  A Lancashire mill worker mediates between management and labour during an industrial dispute. It had originally been intended as a vehicle for Bettys sister Gracie Fields. 

==Cast==
* Betty Fields - Betty Schofield
* Frank Pettingell - Albert Hicks
* Leslie Bradley - Jimmy Priestley
* Ben Field - Old Harry
* Wally Patch - Cardsharper
* Aileen Latham - Anne
* Fewlass Llewellyn - Soames
* Charles Sewell - Mr Preston

==References==
 

==Bibliography==
* Richards, Jeffrey. The Age of the Dream Palace. Routledge & Kegan, 1984.

==External links==
* 

 
 
 
 
 
 


 