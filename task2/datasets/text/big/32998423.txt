The Ticket of Leave Man (1912 film)
 
{{Infobox film
| name           = The Ticket of Leave Man
| image          = 
| image_size     =
| caption        = 
| director       = Gaston Mervale
| producer       = 
| writer         = 
| narrator       =
| starring       = Louise Lovely
| music          =
| cinematography = 
| editing        = 
| studio    = Australian Life Biograph Company
| distributor    = 
| released       = 7 November 1912
| runtime        = 4,100 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Ticket of Leave Man is a 1912 Australian silent film directed by Gaston Mervale starring Louise Lovely.

==Plot==
John Galloway serves time in prison and gets his ticket of leave. He rescues Lady Northon from some roadside ruffians, and her belief in him encourages him to reform. However his old associates, led by Yellow Rose, are keen to get him to participate in a bank robbery. John refuses, the robbery goes ahead and Yellow Rose hides some stolen papers. John is blamed and the gangs hiding place is raided. John manages to escape the police, clear his name and reunite with Lady Norton.  

==Production==
The film was the last production of the Australian Life Biograph Company and was likely made prior to May 1912 when the company shut down. 

The movie is not an adaptation of the highly popular play by Tom Taylor with the same name. 

==References==
 

==External links==
* 

 
 
 
 
 
 


 