Eeran Sandhya
{{Infobox film
| name           = Eeran Sandhya
| image          =
| image_size     =
| caption        =
| director       = Jeassy
| producer       = Rajan Joseph John Paul (dialogues) 
| screenplay     = Dennis Joseph Rahman Jose Prakash
| music          = V. S. Narasimhan
| cinematography = Anandakkuttan 
| editing        = VP Krishnan 
| studio         = Prakash Movietone 
| distributor    = Prakash Movietone
| released       =  
| country        = India Malayalam
}} 1985 Cinema Indian Malayalam Malayalam film, Rahman and Jose Prakash in lead roles. The film had musical score by V. S. Narasimhan.  {{cite web|url=http://
www.malayalachalachithram.com/movie.php?i=1683|title=Eeran Sandhya|accessdate=2014-10-21|publisher=www.malayalachalachithram.com}}   

==Cast==
 
*Mammootty as Madhavankutty
*Shobhana as Prabha Rahman as Raju
*Jose Prakash as Krishna Menon
*Santhosh as Freddy Louis
*Adoor Bhasi as Poulose
*Sankaradi as Gopalan
*Sukumari as Seetha K P A C Sunny as Police Officer T P Madhavan as Avarachan
*N F Varghese as Police Inspector Ashokan as Ashokan
*Baby Chaithanya
*Thilakan
 

==Soundtrack== 
The music was composed by VS Narasimhan and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Poovaam Manchalil Moolum Thennale || Vani Jairam, KG Markose, Krishnachandran || ONV Kurup || 
|- 
| 2 || Randilayum || Vani Jairam || ONV Kurup || 
|} 

==References== 
  

==External links== 
*   

 

 
 
 
 


 