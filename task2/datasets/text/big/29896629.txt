Santa and the Ice Cream Bunny
 
{{Infobox film
| name           = Santa and the Ice Cream Bunny
| image_size     = 
| image	=	Santa and the Ice Cream Bunny FilmPoster.jpeg
| caption        = 
| director       = Barry Mahon R. Winer 
| producer       = Barry Mahon
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = R & S Film Enterprises Inc. 
| released       = 1972
| runtime        = 95 minutes 83 minutes (DVD)
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Santa and the Ice Cream Bunny is a 1972 Childrens film|childrens fantasy film made by R. Winer to frame Barry Mahons Childhood Productions films for a Christmas release. The threadbare plot concerns Santa Clauss attempts to free his sleigh from the sands of a Florida beach, assisted by local children. Poor acting and production values have made the film a cult classic. 

== Synopsis ==

In Santas workshop in the North Pole, Santas elves sing about Christmas nearing and Santas absence. Meanwhile, on a beach in Florida, Santas sleigh has become mired in the sand, and his reindeer have flown away to escape the heat, leaving him stuck. Santa sings a song bemoaning his troubles, then falls asleep.

Several local children hear Santa calling them telepathically and run to him. Santa awakes and explains his predicament. One boy asks why Santa does not fly back to the North Pole on a plane; he explains that he cannot abandon his sleigh and needs their help pulling it out of the sand. The kids bring him several animals, including a pig, a sheep, a donkey, a horse, and a gorilla.

Meanwhile, Tom Sawyer and Huckleberry Finn watch and comment on the action from a distance.

When all the childrens attempts fail, Santa encourages them not to give up hope, and tells them a story about a girl who visits the theme park Pirates World and hears the story of "Thumbelina" as an example. A previously produced film of Thumbelina directed by Barry Mahon plays, complete with the films original credit sequence, and runs longer than its frame story. (Alternate prints of Santa and the Ice Cream Bunny use another Mahon adaptation, Jack in the Beanstalk, as Santas story. )

After the story, Santa encourages the kids to "always believe". One girl tells Santa that her dog, Rebel, can do anything. The kids leave. Santa takes off his coat and falls asleep. He wakes up and puts his coat back on when the kids return in an antique fire engine, singing about how they will help Santa. The engine is being driven by the titular Ice Cream Bunny, whom Rebel has summoned. The bunny offers to drive Santa to the North Pole and they depart. The children realize Santas sleigh is still stuck in the sand and wonder what to do before it teleports to the North Pole, waiting for Santas arrival.

== Parody ==
On December 17, 2010, the RiffTrax movie-commentary project released Santa and the Ice Cream Bunny, with their synchronous commentary, as a "Video on Demand" download. It has since been made available on DVD as well by Legend Films. 

==Home video availability==
The Thumbelina version was released on VHS by United Home Video, but is different from the theatrical version in that the Thumbelina film is shown after the Santa portion rather than during it. 

In 2011, the film was made available on DVD as a bonus feature on the  . However, just like the version shown in the RiffTrax episode, the film is cut down to a runtime of 83 minutes due to the removal of certain musical numbers.

The Jack and the Beanstalk version has not been released on video, but the included film was released on DVD by Image Entertainment in 2002. 

==References==
 

==External links==
*  
*  at RiffTrax
*  extensive recap at The Agony Booth
*  review at Cinemassacre

 
 
 
 
 
 
 
 
 
 