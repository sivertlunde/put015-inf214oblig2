Four Jills in a Jeep
{{Infobox film
| name           = Four Jills in a Jeep
| image_size     =
| image	         = Four Jills in a Jeep FilmPoster.jpeg
| caption        =
| director       = William A. Seiter
| producer       = Irving Starr
| writer         = Carole Landis (story) Froma Sand (story) Helen Logan Fred Niblo, Jr. Snag Werris
| narrator       =
| starring       = Kay Francis Carole Landis Martha Raye Mitzi Mayfair
| music          = Hugo Friedhofer Arthur Lange Cyril J. Mockridge
| cinematography = J. Peverell Marley
| editing        = Ray Curtiss
| studio         = Twentieth Century Fox
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 USO tour of Europe and North Africa during World War II.

==Summary==
When actress Kay Francis acts as mistress of ceremonies for a broadcast of the Command Performance radio show, which goes out to military troops around the world, also appearing on the show are Jimmy Dorsey and his orchestra, actresses and singers Betty Grable and Carole Landis, comedienne Martha Raye and dancer Mitzi Mayfair. After the show is over, Dorsey announces that he is going on a USO tour, and Kay reveals that she also has received permission from military officials in Washington, D.C. to gather a troupe of entertainers. The perpetually blustery Martha tells a visiting colonel from the British Foreign Office how much she would like to be with the fighting men, and he suggests that Kay form her troupe with Martha, Carole and Mitzi. The three women agree, and soon Kay has organized everything for their trip to England. After their arrival in England, American soldier Sgt. Eddie Hart drives them to the military base, where they make the best of their muddy, cold surroundings. Eddie quickly develops a crush on Martha, while Carole is attracted to American flier Capt. Ted Warren. Kay is impressed by the good manners of an English doctor, Capt. Lloyd, while Mitzi is surprised to meet her former partner and boyfriend, Lt. Dick Ryan. The women have little time for romance, however, as they travel the country with their show and work hard to entertain the troops. Carole arranges to see Ted at a Red Cross benefit given at the luxurious home of Lady Carlton-Smith, and despite the attentions paid to Carole by a general, she and Ted find time to declare their love for each other. Later, the women return to Teds camp, and Kay, Martha and Mitzi try to cheer up Carole when Ted is late returning from a mission. The men also try to entertain the hard-working performers, and together they listen to a Command Performance broadcast featuring George Jessel, Alice Faye and Carmen Miranda. Caroles anxiety is finally relieved when Ted returns safely. The couple are married shortly after, but are denied a honeymoon when the women receive orders to ship out to North Africa. Kay is bewildered by the new orders until Martha confesses that she caused the problem at the Red Cross party, when she boasted to an admiral about the entertainers determination to visit men fighting in an active war zone. Upon their arrival in North Africa, the women are nonplussed by the hard conditions, but eagerly pitch in and help the nurses. Kay is especially pleased to find Capt. Lloyd assigned to the unit and does all she can to help him, even scrubbing floors. With their tenacity and cheerfulness, the American celebrities force the gruff head nurse to admit that she was wrong in her opinion that they would be hindrances, and they top off a long day by putting on a show for the troops. During Caroles performance, the lights go out due to a power failure, but the soldiers illuminate the stage with their flashlights and Carole continues. The show is then interrupted by a German bombing raid, and the women take cover in the trenches with the soldiers. Dick and Mitzi declare their love as they duck for cover, while elsewhere, Eddie cuddles with Martha. After the all-clear is sounded, an officer tells the women that they can now leave for the rear lines, but they refuse to leave the men while they are shipping out for the front lines. Soon after, the women wave to the soldiers as they drive away.

==Production==
The working titles of this film were Command Performance and Camp Show. Before the films opening credits, an onscreen written prologue reads, "This story is based on the experiences of four of the many performers who take entertainment to Americas men in uniform in the theatres of war as well as in the camps at home. Actors who serve in this global entertainment program consider it a privilege to lighten a little the hardships endured by our fighting men and to share, in a measure, their experiences in combat zones. The producers gratefully acknowledge the work of USO-Camp Shows, Inc., the Hollywood Victory Committee and the Special Service Division of the War Department." As noted in the onscreen credits, the picture was based on the actual experiences of Kay Francis, Carole Landis, Martha Raye and Mitzi Mayfair, who, as the members of the Feminine Theatrical Task Force, entertained American and British troops on a tour of England, Ireland and North Africa. The women left the United States on 16 Oct 1942 and spent approximately three months in England, Ireland, Scotland and Wales. During their sometimes difficult tour, the women performed several shows a day for the troops. They also presented a command performance for the Queen of England.

After leaving England, the women spent approximately three weeks in North Africa, which marked the first USO tour of that area. Francis and Mayfair then returned to the United States. Landis flew back to England to join her husband. As depicted in the film, Landis met U.S. Army Air Force pilot Capt. Thomas C. Wallace in England in Nov 1942 and married him on 5 Jan 1943. The couple divorced in Jul 1945. Raye, who was "the first honorary captain created in World War II," according to a LAEx article, continued touring with other USO groups. The experience marked the beginning of Rayes long association with the USO, and her many visits to the troops during the Korean and Vietnam wars, as well as her service as a nurse, earned Raye many awards, including a Purple Heart, the USOs Distinguished Service Award and the Presidential Medal of Freedom.  

According to information in the Twentieth Century-Fox Records of the Legal Department, located at the, Landis and Edwin Seaver wrote a book about her travels while the film was in pre-production at the studio. Although  UCLA Arts—Special Collections Library, Landis material was not used in the screenplay, the studio agreed to let her use the films title as her book title for the publicity value. Her book, which was published in 1944, first appeared as a serial in SEP (18 Dec 1943–15 Jan 1944). The legal records indicate that, while the film was based on the experiences of all four performers, only Mayfair and Francis directly contributed to the screenplay. The legal records also indicate that Mayfairs agent, Lou Irwin, was the first person to suggest the idea of the film to the studio. Studio records reveal that Waldo Salt worked on an early draft of the films script, but the extent of his contribution to the completed picture has not been determined.
 Universal production Between Us Girls. Mayfair had not appeared in a feature film since the 1930 musical Paramount on Parade, and Four Jills in a Jeep marked her last screen appearance.

According to an Apr 1943 HR news item, Landis, Raye and Mayfair were also scheduled to make a short film recreating their act. Twentieth Century-Fox loaned Harold Schuster to the Army to direct the short, which was "to be exhibited only before Army groups, and is not for public showing." According to the news item, "The short opens on the set of  , from which point the girls go into their act." 
 
==Cast==
* Kay Francis as Herself
* Carole Landis as Herself
* Martha Raye as Herself
* Mitzi Mayfair as Herself
* Jimmy Dorsey and His Orchestra as Themselves John Harvey as Ted Warren
* Phil Silvers as Eddie
* Dick Haymes as Lt. Dick Ryan
* Alice Faye as Herself
* Betty Grable as Herself
* Carmen Miranda as Herself George Jessel as Himself

==Reception==
The New York Times reviewer Bosley Crowther was unimpressed, writing, "It gives the painful impression of having been tossed together in a couple of hours. All that happens, really, is a lot of dizzying about the dames and some singing and dancing by them in an undistinguished style." 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 