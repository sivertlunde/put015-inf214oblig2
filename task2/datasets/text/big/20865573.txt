D' Anothers
{{Infobox film
| name           = D Anothers
| image          = DAnothersPoster.jpg
| caption        = Theatrical Release Poster
| director       = Joyce Bernal
| producer       = Elma S. Medua Charo Santos-Concio Malou N. Santos
| writer         = Adolfo Alix Jr. Raymond Lee
| starring       = Vhong Navarro Toni Gonzaga Pokwang Roxanne Guinoo Joross Gamboa Jhong Hilario
| music          = 
| cinematography = Charlie Peralta
| editing        = Renewin Alano
| distributor    = Star Cinema
| released       =  
| run_time        = 105 minutes
| country        = Philippines
| awards         = Funniest movie of the year
| language       = Filipino language|Filipino, English
| budget         = 
| gross          = P85 million
| preceded_by    = 
| followed_by    = 
}}

D Anothers is a 2005 comedy-horror film starring Vhong Navarro and Toni Gonzaga released under Star Cinema, ABS-CBN Film Productions.

==Plot summary==
The Resurreccions ancestral   mansion.

Hesus Resurreccion (Vhong Navarro) is about to find out that he is The One but he is afraid of ghosts. When he inherits the mansion, he wants to sell it immediately but it isnt that easy since people believe the mansion is haunted house|haunted. He totally freaks out when he learns that the people he sees in the mansion are all ghosts. Hesus has no choice but to fulfill his mission as The One. As he spends more time with the ghosts, he realizes that they are not scary at all and even enjoys their company.
 key and opened the portal.

===Main Cast===
*Vhong Navarro as Hesus Resurrecion
*Toni Gonzaga as Maan Tuken

===Supporting Cast===
* John Prats as JC
* Jhong Hilario as Gorio
* Dominic Ochoa
* Roxanne Guinoo
* Neri Naig as Rachelle
* Michelle Madrigal as Mayumi
* Joross Gamboa as Xavier Mura as Vic
*Pokwang
*Jaime Fabregas as Padre Florentino
* Jill Yulo as Angelica
*Arlene Muhlach as Kimberly
*Archie Alemania as Jograd Tado as Atty. Reposo
*Bella Flores as Precious
* Pinky Amador as Mrs. Tuken
* Dennis Padilla as Mr. Resurrecion
* Marjorie Barretto as Mrs. Resurrecion
*Mosang as Lotus Feet
*Jojit Lorenzo as George
* Piolo Pascual as Himself
* Rufa Mae Quinto as Herself

==Soundtrack==
 
 
*"Don Romantiko"
*:Composed and Written by Christian Martinez
*:Published by Star Songs, Inc.
*:Performed by Vhong Navarro
*:Courtesy of Star Recording, Inc.

*"Hesus"
*:Composed and Written by Andrei Dionisio
*:Performed by Cindy Lacanilao
*:Used with permission

*"Chicken Dance"
*:Composed and Written by Christian Martinez
*:Published by Star Songs, Inc.
*:Performed by The Chickies
*:Courtesy of Star Recording, Inc.

*"I Dont Know Why (I Just Do)"
*:Composed by Roy Turk and Fred E. Ahlert
 
*"Rain"
*:Words and Music by George Canseco
*:Published by Bayanihan Music Philippines, Inc.
*:Performed by Boy Mondragon
*:Courtesy of Vicor Music Corporation

*"Huwag na Huwag Mong Sasabihin"
*:Words and Music by Kitchie Nadal
*:Performed by Kitchie Nadal
*:Courtesy of Twelve Stone Records and Music Corporation

*"Umiiyak Ang Puso"
*:Words and Music by April Boy Regino
*:Published by Soundscape Music Publishing, Inc.
*:Performed by April Boy Regino
*:From the Album "Umiiyak Ang Puso"
*:Produced by Ivory Music and Video, Inc.
 

==See also==
* Star Cinema

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 