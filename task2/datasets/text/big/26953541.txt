Tiny Furniture
{{Infobox film
| name           = Tiny Furniture
| image          = Tiny_furniture_poster.jpg
| image size     =
| caption        = Promotional film poster
| director       = Lena Dunham
| producer       = Kylie Martin Alicia Van Couvering Alice Wang
| writer         = Lena Dunham
| starring       = Lena Dunham Laurie Simmons Grace Dunham Jemima Kirke Alex Karpovsky David Call Merritt Wever Amy Seimetz
| music          = Teddy Blanks
| cinematography = Jody Lee Lipes
| editing        = Lance Edmands
| studio         = Tiny Ponies
| distributor =  IFC Films
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $65,000
| gross          = $391,674
}}

Tiny Furniture is a 2010 American independent comedy-drama film|comedy-drama written by, directed by, and starring Lena Dunham. 

It premiered at South by Southwest, where it won Best Narrative Feature,  screened at such festivals as Maryland Film Festival, and was released theatrically in the United States on November 12, 2010. Dunham’s own mother, the artist Laurie Simmons, plays Aura’s mother, while her real sister, Grace Dunham|Grace, plays Aura’s on-screen sibling. The actors Jemima Kirke and Alex Karpovsky would also appear in Dunhams television series Girls (TV series)|Girls.

==Plot==
  
Aura returns home from her Midwest liberal arts college to her artist family’s TriBeCa loft with nothing but a film studies degree, a failed relationship and a lack of direction. She takes a job as a hostess at a restaurant and falls into relationships with two self-centered men while struggling to define herself.

The film focuses on the relationship of Aura with her mother, Siri, a successful photographer. Aura reads her mothers teenage journals, perhaps for guidance or inspiration. In the films final scene, Aura confesses to having read the journals; her mother says she doesnt mind. Aura asks about her mothers many male friends at that time, and her mother responds that she had been experimenting, as Aura is doing now. That moment of understanding contrasts with the conflict between the two for most of the film. The films title refers to her mothers signature photographic subject, miniature furniture.

The film also focuses on Auras relationship with her 17-year-old sister Nadine, and Auras best friend from childhood, Charlotte. All three—Aura, Nadine, and Charlotte—are budding artists, which provides for exploration of their planned paths as well as rivalry between Aura and the other two.

==Cast==
*Lena Dunham as Aura
*Laurie Simmons as Siri
*Grace Dunham as Nadine
*Jemima Kirke as Charlotte
*Alex Karpovsky as Jed
*David Call as Keith
*Merritt Wever as Frankie
*Amy Seimetz as Ashlynn
*Garland Hunter as Noelle
*Isen Ritchie as Jacob
*Mike S. Ryan as Homeless Man

==Production==

===Filming===
The film was shot on the Canon EOS 7D.
Filming took place in TriBeCa and Lower Manhattan. The film was shot in November 2009.  Dunham says she wrote a "tight script" to which the actors were faithful. 

===Music=== Domino (Domino Kirke, and Jordan Galland), Rebecca Schiffman and Sonias Party! & The Everyones Invited Band.  The soundtrack is downloadable for free on the movie website. 

==Home media==
Tiny Furniture  was released on DVD and Blu-ray Disc in 2012 as part of the Criterion Collection. 

==Reception==
The film holds a 78% rating on review site Rotten Tomatoes, with an average rating of 6.9/10, based on 93 reviews.

===Awards=== Independent Spirit Awards. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 