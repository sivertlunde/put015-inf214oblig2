Plan 9 from Syracuse
Plan documentary by independent filmmaker Ryan Dacko about his attempt to gain attention of film producer Mark Cuban by staging a cross-country run from Syracuse, New York, to Hollywood.

Despite a lack of experience in marathon running, Dacko left from Syracuse’s Eastwood Palace Theatre on August 15, 2006.  He planned to reach Hollywood in 90 days, but problems with weather and unreliable support crews delayed his arrival until December 31, a 139-day journey.  In the course of the run, he used the Internet to urge supporters to lobby Mark Cuban to agree for a meeting.  Cuban, learning about the endeavor, responded negatively and refused to meet with Dacko. 

With this run, Dacko became the 175th person to cross America on foot (166th male, the 30th supported run, and the 201st crossing overall among other stats).

Plan 9 from Syracuse had its premiere at the B-Movie Film Festival in October 2007. Two months later, it was reviewed by Film Threat as “the best documentary of the year, bar none.”    It was screened on the U.S. film festival circuit,  winning awards for Best Picture and Best Director at the 2008 New Haven Underground Film Festival.  The film was released on DVD release on August 26, 2008, by SRS Cinema. 

==References==
 

== External links ==
* 
* 
* 
* 
* 
* 
* 
* 

 
 
 
 
 

 