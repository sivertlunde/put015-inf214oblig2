Imaginary Crimes
{{Infobox Film
| name           = Imaginary Crimes
| image          = Imaginecrimespost.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Anthony Drazan
| producer       = James G. Robinson	
| writer         = Kristine Johnson Davia Nelson
| based on       = Imaginary Crimes by Sheila Ballantyne
| starring       = Harvey Keitel Fairuza Balk Kelly Lynch Vincent DOnofrio Chris Penn Seymour Cassel
| music          = Stephen Endelman
| cinematography = John J. Campbell
| editing        = Elizabeth Kling
| studio         = Morgan Creek Productions	
| distributor    = Warner Bros.
| released       = October 14, 1994
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Imaginary Crimes is a 1994 drama film directed by Anthony Drazan, and an adaptation of Sheila Ballantynes 1982 novel of the same name.    The film stars Harvey Keitel and Fairuza Balk, and tells the story of Ray Weiler (Keitel), a widowed hustler trying to raise his two daughters in 1962 Portland, Oregon.  His eldest daughter Sonya (Balk) is a gifted student and writer who attempts to start a life for herself despite her fathers inability to change his self-destructive behaviour. Her journal entries form the basis of retrospective scenes documenting the familys story.

==Cast==
*Harvey Keitel as Ray Weiler
*Fairuza Balk as Sonya Weiler
*Kelly Lynch as Valery Weiler
*Vincent DOnofrio as Mr. Webster
*Diane Baker as Abigail Tate
*Chris Penn as Jarvis
*Seymour Cassel as Eddie
*Elisabeth Moss as Greta Weiler
*Annette OToole as Ginny Rucklehaus

==Production==
Filming took place in Portland, Oregon|Portland, Oregon, ultimately grossing only  $89,611.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 