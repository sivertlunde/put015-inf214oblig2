Face to Face (1967 film)
{{Infobox film
| name          = Face to Face
| image         = Faccia a faccia.jpg
| caption       = Japanese DVD cover for Faccia a faccia.
| director      = Sergio Sollima
| producer      = Arturo González Alberto Grimaldi
| writer        = Sergio Sollima Sergio Donati William Berger
| music         = Ennio Morricone
| cinematography= Emilio Foriscot Rafael Pacheco
| released      = November 23, 1967
| runtime       = 108 min.
| country       = Italy, Spain Spanish
}} Italian spaghetti western film written and directed by Sergio Sollima. The film stars Gian Maria Volonté and Tomas Milian, and features a musical score by Ennio Morricone.

Face to Face is the second of Sollimas three westerns, following La resa dei conti ("The Big Gundown") and predating Corri, uomo, corri ("Run, Man, Run!"), a sequel to La resa dei conti. Milian stars in a lead role in all three films.

==Plot==
Professor Brad Fletcher (Volontè), a teacher from New England, leaves his job and travels to Texas due to his ill health. After arriving in the west, he ends up taken hostage by a wounded outlaw, Solomon "Beauregard" Bennet (Milian). Fletcher helps Bennet regain his health and manages to convince Bennet that he is more valuable to him alive than dead. Through their uneasy friendship, Fletcher takes an interest in the outlaw way of life. Instead of returning home, he decides to join the "Bennets Raiders".
 William Berger), Pinkerton detective, Fletcher and Bennet find out there is a vigilante group after them. 

==Cast==
*Tomas Milian as Solomon Beauregard Bennett
*Gian Maria Volonté as Professor Brad Fletcher William Berger as Charlie Siringo
*Jolanda Modio as Maria
*Gianni Rizzo as Williams
*Carole André as Cattle Annie
*Ángel del Pozo as Maximilian de Winton
*Aldo Sambrell as Zachary Chase
*Nello Pazzafini as Vance
*José Torres as Aaron Chase
*Federico Boido as Sheriff of Purgatory City

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 


 
 