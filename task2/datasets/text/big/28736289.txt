Stranger in the House (1967 film)
 
{{Infobox film
| name           = Stranger in the House
| image          = Strangerinthehouse67.jpg
| caption        = Film poster by Arnaldo Putzu
| director       = Pierre Rouve
| producer       = Anatole de Grunwald Dimitri De Grunwald
| writer         = Pierre Rouve Georges Simenon
| starring       = James Mason Geraldine Chaplin Bobby Darrin John Scott
| cinematography = Kenneth Higgins
| editing        = Ernest Walter	
| distributor    = 
| released       =    
| runtime        = 104 minutes
| country        = UK
| language       = English
| budget         = $665,000 "ABCs 5 Years of Film Production Profits & Losses", Variety, 31 May 1973 p 3 
| gross          = $255,000 
}} remade in 1997. 

Eric Burdon & The Animals wrote and recorded the song "Aint That So" for the film.

==Plot==
A retired, scotch-swilling attorney (James Mason) resides in France who disapproved of his daughters (Geraldine Chaplin) new boy friend (Bobby Darin) but rose to the young mans defense in court when the boy is arrested on a suspicious murder charge. 

==Cast==
* James Mason as John Sawyer 
* Geraldine Chaplin as Angela Sawyer 
* Bobby Darin as Barney Teale 
* Paul Bertoya as Jo Christoforides 
* Ian Ogilvy as Desmond Flower 
* Bryan Stanyon as Peter Hawkins 
* Pippa Steel as Sue Phillips 
* Clive Morton as Colonel Flower 
* Moira Lister as Mrs. Flower  James Hayter as Harry Hawkins 
* Megs Jenkins as Mrs. Christoforides
* Lisa Daniely as Diana Sawyer 
* Ivor Dean as Inspector Colder
* Yootha Joyce as Shooting Range Girl

==Critical response==
Some critics felt that, although the casting of Chaplin and Darin was meant to appeal to younger audiences, both were nonetheless too old for their characters. Others thought that the title Cop-Out might have worked better (especially with audiences of the 1960s) without its trendy camera work and wearisome generation-gap propaganda. 

==Reception==
ABC reported a loss of $795,000 on the movie. 
==References==
 

==External links==
* 
* 

 
 
 
 
 
 