Destiny Turns on the Radio
 
{{Infobox Film
| name           = Destiny Turns on the Radio
| image          = Destiny Turns on the Radio.jpg
| image_size     = 
| caption        = VHS cover
| director       = Jack Baran
| producer       = Keith Samples Gloria Zimmerman
| writer         = Robert Ramsey Matthew Stone
| narrator       = 
| starring       = James LeGros Dylan McDermott Quentin Tarantino Nancy Travis James Belushi
| music          = J. Steven Soles
| cinematography = James L. Carter
| editing        = Raúl Dávalos
| distributor    = Savoy Pictures
| released       = April 28, 1995
| runtime        = 102 min. United States English
| budget         = 
| gross          = $1,176,982 
}}

Destiny Turns on the Radio (1995) is an American action-comedy film, directed by Jack Baran. It was the film debut of David Cross, with a small role.  

==Plot==
An incarcerated bank robber, Julian Goddard, escapes from prison. He is rescued in the desert by Johnny Destiny, a bizarre, possibly supernatural character. Destiny takes Julian to Las Vegas and the Marilyn Motel, owned by Harry Thoreau, who was Julians partner in crime. Julian searches for his girlfriend, Lucille, and the proceeds of the heist. However, Destiny has taken the money and Lucille is pregnant and shacking up with Tuerto, a mob kingpin. Her agent has convinced a record label to send a talent scout to hear her lounge singing act, but Julian’s arrival upsets her plans. As they are hunted by both the police and Tuerto’s henchmen, Destiny toys with their fate.

==Cast==
*Dylan McDermott as Julian Goddard
*Nancy Travis as Lucille
*James LeGros as Harry Thoreau
*Quentin Tarantino as Johnny Destiny
*James Belushi as Tuerto
*Janet Carroll as Escabel
*David Cross as Ralph Dellaposa
*Richard Edson as Gage
*Bobcat Goldthwait as Mr. Smith
*Barry Shabaka Henley as Dravec
*Lisa Jane Persky as Katrina
*Sarah Trigger as Francine
*Tracey Walter as Pappy
*Allen Garfield as Vinnie Vidivici

==Reception==
Destiny Turns on the Radio received negative reviews from critics, as the film holds a 13% rating on Rotten Tomatoes.

==Home video==
The film was released on VHS by Rysher Entertainment, but is out of print. It has not been released on DVD or Blu-ray.

==References==
 

==External links==
* 
* 
* 

 
 