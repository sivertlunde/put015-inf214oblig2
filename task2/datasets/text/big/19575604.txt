Venus fra Vestø
 
{{Infobox film
| name           = Venus fra Vestø
| image          = Venus fra Vestø.jpg
| caption        = Film poster
| director       = Annelise Reenberg
| producer       = Poul Bang
| writer         = Børge Müller Jerrard Tickell
| narrator       =
| starring       = Malene Schwartz
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen Ole Lytken
| editing        = Wera Iwanouw Lizzi Weischenfeldt
| studio         = Saga Studios
| distributor    = ASA Film
| released       =  
| runtime        = 116 minutes
| country        = Denmark
| language       = Danish
| budget         =
}}
 comedy war film directed by Annelise Reenberg and starring Malene Schwartz. The film is based on Jerrard Tickells book Appointment with Venus (Danish title: Operation Venus). 

==Cast==
* Malene Schwartz - Frk. Nicola Egede-Schack
* Henning Moritzen - John Morland
* Dirch Passer - Ditlev Egede-Schack
* William Knoblauch - Sognerådsformand Ole Klausen
* Ole Wegener - Radiotelegrafist Henriksen
* Jan Priiskorn-Schmidt - Søren Severinsen Arthur Jensen - Pive Ras
* Holger Hansen - Morten Jacobsen - Trawler Jack
* Gunnar Lemvigh - Kromand
* Jakob Nielsen - Kristoffer
* Jerrard Tickell - Uncle George
* Poul Thomsen - Tolk i retssal
* Karl Heinz Neumann - Kaptajn Weiss
* Inger Knoblauch
* Edith Hermansen
* Avi Sagild - Mathilde
* Varinka Wichfeld Muus
* Flemming B. Muus
* Vera Lynn - Herself
* Richard Wattis - Englishman Edward Chapman - Englishman
* Allan Blanner - Englishman
* David Collet - Englishman

==References==
 

==External links==
* 
*  featuring performance by Vera Lynn

 
 
 
 
 
 
 
 