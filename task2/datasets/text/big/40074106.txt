The Rogue Stallion
{{Infobox film
| name           = The Rogue Stallion
| image          = 
| image size     =
| caption        = 
| director       = Henri Safran
| producer       = Philip East
| writer         = Rick Maier Ysabelle Dean
| based on = 
| narrator       =
| starring       = Beth Buchanan Brian Rooney
| music          = 
| cinematography = 
| editing        = 
| studio = Grundy Productions South Pacific Pictures
| distributor    = 
| released       = 1990
| runtime        = 
| country        = Australia English
| budget         = $1 million 
| gross = 
| preceded by    =
| followed by    =
}}
The Rogue Stallion is a 1990 Australian television film about a wild horse. It was part of the South Pacific Adventure Series.
==Synopsis==
Anna Peterson (Beth Buchanan) loves horses, and the fact that her father (Bruno Lawrence) works at a racetrack in Sydney, Australia only adds to her enthusiasm. One night at dinner, her father announces that after the success of one of the stables horses in a prominent trophy race, his boss gave the employees a bonus large enough for the Peterson family to go back to her fathers ancestral home in New Zealand. Tragically the following day, there is an accident at the stables and Annas father is killed. With nothing left to keep them in Australia, Annas mother Rose (Michele Fawdon) decides to follow through on her husbands dream and moves Anna and her younger brother Mike (Andrew Shepherd) to Charlton, New Zealand.

Upon arrival, however, they find things are not quite what they expected. First, they are run off the road by Mr. Garrett (Peter McAllum), a prominent local horse breeder obsessed with hunting a beautiful brown wild stallion, as he and his men are in pursuit. Next, after walking several miles to their fathers family homestead, they discover it has fallen into disrepair and is barely livable. The night they arrive, Anna hears the wild horse out in the woods behind the house and frees it from loose wire in which is had been caught. Before she can persuade the horse to come home with her, a mysterious man appears and frightens the horse away.

Meanwhile, a local shop owner Mr. Peabody (John I. Watson), who met the Petersons on their way into town and who seems aware of a connection between them and Mr. Garrett, informs his friend of their arrival. Garretts three teenage children Tony (Dean OGorman), Ginny (Jodie Rimmer), and Wayne (Brian Rooney) overhear the conversation and discuss the rumour that John Peterson killed one of Mr. Garretts best mares years before, explaining his cold behavior towards Johns wife and children. Ginny warns the caring Wayne that he "best not get friendly with them, if you know whats good for you."

The next day, Jean Bailey (Beaver) arrives at the homestead to offer her various services, including veterinary services, if the family plans on keeping horses. Rose declines, but invites Jean to stay for coffee, during which Jean tells Rose about the feud between her husband and Garrett. She also offers to tow their stranded car. Anna volunteers to show her where the car was run off the road. On the way, Jean Bailey invites Anna to join the local Pony Club, and they discuss the wild horse. Later, while Annas mother runs into town for house supplies, she and Mike sneak off to check out the Pony Club in spite Roses expressed wishes. Anna gets into a tiff with Garrett after he attempts to whip his prize horse, Eclipse, which threw his daughter Ginny during competition.

Anna and Mike start school the following day, and find themselves the targets of a band of bullies, led by Tony and Ginny. Only Wayne goes out of his way to befriend Anna, telling her about Old Gonzalez (Jose Maria Caffarel), the mysterious man she had seen in the woods, a supposed murderer who lives in the wild. After an incident on the bus, Jean Bailey, who is also the bus driver, threatens to tell the school principal and get Ginny and Tony in detention after school, causing them to miss Pony Club. Ginny plots revenge by inviting Anna and Mike to ride Eclipse after school. During Annas turn, Tony and his friends throw stones at Eclipse, causing the horse to bolt. When Anna loses control and is thrown, Eclipse runs off into the woods, and Ginny and Tony jeeringly tell Anna to go back home to Australia. Dejected, Anna runs off after Eclipse.

In the woods, she finds the wild stallion who leads her to Eclipse, now stuck in a bog. After being saved from falling into a large trap Garretts men dug throughout the woods by Old Gonzalez, Anna and the old man pull Eclipse out with the help of the stallion, called Wild Fire (Fuego Savaje) by Gonzalez. In contrast to Gonzalezs murderous reputation, he takes Anna to his remote animal sanctuary to rest and to check Eclipse for injuries.

In the meantime, Ginny and Tony claim that Anna has stolen the horse, fueling Garretts determination to buy the Peterson homestead and drive the family out of town. Wayne is revealed to be friends with Gonzalez when he runs into Anna in the woods on his way to ask Gonzalezs help to find her. Wayne apologizes on behalf of his siblings, who he states only bullied her to get on the right side of their father, and explains his complicity in perpetuating rumours of Gonzalezs reputation as a murderer. He also tells her of his own horse riding accident during which he was injured while trying a difficult jump at his fathers urging, resulting in his current limp.

Before Wayne and Anna escort Eclipse back to town, Gonzalez tells Anna and Wayne the real story of the fight between Garrett and Annas father. John Peterson had worked for Garretts father as an assistant trainer on his horse farm. John had a way with horses and unlike the younger Garrett who was very rough with them. One day, Annas father found Garrett whipping the prize stallion, slated to win the Melbourne Cup race. After an altercation, Garrett tried to persuade his father to fire John, but he refused to fire such a skilled trainer and risk losing the race. One morning some time later, both John and the horse went missing. John later claimed the stable door had been left open and the horse had wandered off. It fell in a gully and broke two legs, ruining any hope of winning the race. The younger Garrett claimed John had tried to steal the horse, and this time his father fired John and told him to "get out of town." Gonzalez sends them off to take Eclipse back with instructions that she cannot be ridden for a minimum of two days.

As Anna and Wayne stroll back to town with Eclipse, they hear cries for help. Annas younger brother Mike, who had gone off on his own to find his missing sister, fell into one of Garretts horse traps and is seriously injured. Wildfire allows Anna to ride him into town to fetch help. Mike is taken to the hospital, where Rose declares that, in light of the unwelcoming community and the severity of Mikes injuries, they must move back to Australia. While Rose stays with Mike, Jean tries to drum up support for the family in town, encouraging everyone to stand up to Garrett. Anna goes back to Gonzalezs home to say goodbye to Wildfire, but Garrett has followed her and plans to shoot the horse, chasing it through the woods to the edge of a cliff where Wildfire leaps into the rapids, presumably dead. Gonzalez accuses him of leaving the stable door open all those years ago, declares that rather than leading the horse away John Peterson had been tracking it, and chastises Garrett for his arrogance which has caused so much hurt to those around him.

After Mike encourages Rose not to run away, the family decides to stay. The community finally rallies around them, forming a working bee to fix up the house, and refusing to be intimidated by Garrett anymore. Wayne shows up riding a horse, having regained his confidence, and invites Anna to come riding with him. When she protests she has no horse, he indicates Wildfire, still alive, at the edge of the woods, and the two ride together through the hills surrounding the homestead.

 Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p133 
 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 