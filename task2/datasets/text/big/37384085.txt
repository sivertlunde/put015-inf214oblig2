Stitches (2012 film)
 
 
{{Infobox film
| name           = Stitches
| image          = Stitches 2012 movie poster.jpg
| image_size     =
| caption        =
| director       = Conor McMahon  
| producer       =     
| writer         = Conor McMahon
| starring       = Ross Noble Tommy Knight
| music          =
| cinematography =
| editing        =
| distributor    = MPI Media Group Irish Film Board
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom Ireland English
}}

Stitches is a 2012 British-Irish horror comedy film directed by Conor McMahon and starring Ross Noble, Tommy Knight and Gemma-Leah Devereux.  The plot concerns a birthday clown returning from the dead to exact revenge upon a group of children who contributed to his death. Stitches was produced by Fantastic Films and Tailored Films in 2012 and marks the movie debut of Stand-up comedy|stand-up comedian Ross Noble.  

==Synopsis==
Richard Grindle, a clown with the stage name Stitches, makes out with a woman in his van. During sex, the woman notices an egg with a face painted on it encased in a glass tube. Stitches explains that "they" made him do it when he signed up. Stitches arrives late at Toms (Ryan Burke) birthday, and attempts to entertain the children, but they instead ridicule him. Vinny (Gerald Ahern) ties Stitches shoelaces together, causing him to trip and land on a kitchen knife, and he dies. Tom visits Stitches grave and finds a group of clowns performing a ritual, and the groups leader, The Motley, threatens Tom.

Six years later, Tom (Tommy Knight) is preparing for his sixteenth birthday. Hesitating at the idea of throwing a large party, he considers instead inviting only a few friends. Ultimately, he settles on a large gathering, and Vinny secretly distributes many more invitations over the internet. Tom, Vinny, Richie (Eoghan McQuinn) and Bulger (Thommas Kane Byrne), all of whom had been present when Stitches died, prepare the house. As the guests, including Toms childhood love Kate (Gemma Leah Devereux), arrive, Stitches comes back to life and leaves his grave.
 scalps him and removes his brain. Sarah, Pauls girlfriend, enters the attic to look for him. There, she is attacked by Stitches, and manages to fight back. As she escapes, Stitches drives an umbrella through her skull, killing her. Through his telescope, Tom sees Stitches in the house, and goes to warn Vinny of his presence. He asks Kate to leave, but her boyfriend Dan (Tommy Cullen) stops her.

Outside, Stitches attacks Richie. Richie attempts to flee but trips and falls. Stitches stabs him with his bike pump and inflates Richie. Stitches manages to pump enough air into Richie to cause him to explode. Vinny, on discovering Tom to be telling the truth, attempts to leave, but Stitches attacks them. Tom stabs him, Vinny covers him with a blanket, and they escape. Tom and Vinny rescue Kate, but Stitches knocks her unconscious as they attempt to escape. Tom tries to resuscitate her, and Vinny leaves them behind, and Stitches attempts to drown Tom in a sink. Kate awakens and throws a knife at Stitches, while Tom deduces a manner in which to defeat the clown; to kill him, they must destroy the egg he kept in his castle. Tom and Kate, pursued by Stitches, make their way to the den in the graveyard. There, Tom searches for Stitches egg amongst a collection of them. Stitches arrives, and whilst deciding which of the two to kill, Vinny ties his shoelaces together again. He trips, and drops his egg. Tom smashes it, and Stitches explodes.

Six months later, Tom has moved to a new house, and he and Kate are in his treehouse. Kate gives Tom a new telescope, and his old one is positioned so as to focus on the den in the graveyard. There, The Motley is attempting to piece Stitches broken egg back together. After the film cuts to black, Stitches catchphrase, "Everybody happy?", is heard.

==Cast==
*Ross Noble as Richard "Stitches" Grindle — An irresponsible and boring clown victimized by the children at Toms tenth birthday party, resulting to his death. A black magic clown cult made it possible for him to rise back from the dead and avenge his sudden death.

*Tommy Knight as Tom — The birthday boy whose tenth birthday party became the cause of Stitches death when the children made fun of Stitches. Because of Stitches traumatizing death, Tom was under medication. Hes had a crush on Kate since they were children.

*Shane Murray Corcoran as Vinny — Toms closest friend who has an interest on Mary. He was the one who tied Stitches shoelaces, causing him to trip with his face landing on a kitchen knife.

*Gemma-Leah Devereux as Kate — Toms childhood crush who also attended Toms tenth birthday party. She has a boyfriend, Dan, whom she doesnt like very much.

*Thommas Kane Byrne as Bulger — Toms gay friend who arranged and decorated his sixteenth birthday party. During Toms tenth birthday party, he threw an ice cream scooper on Stitches. Stitches killed him using a can opener and scooped his brain out with an ice cream scooper.

*Eoghan McQuinn as Richie — Toms friend who likes photography. During Toms tenth birthday party, he asked Stitches to make a balloon with the shape of a Stegosaurus. Stitches said that balloons for dinosaurs are extinct and that he should have a dog-shaped balloon; he also popped the balloon, shocking Stitches. He was killed by Stitches by pulling his intestines out and shaping a dog out of it and pumped his head with an air pump until it exploded.

*Roisin Barron as Sarah — A girl who picks on Tom. She has a boyfriend, Paul, who also picks on Tom. During Toms tenth birthday party, she threw an umbrella on Stitches; she was killed by Stitches by throwing an umbrella going through her head.

*Hugh Mulhern as Paul — Sarahs boyfriend who was also invited for Toms tenth birthday party. Back then, he put a drink in Stitches hat as Stitches was about to do a "Hat of Mystery" trick. During Toms sixteenth birthday party, he peed on his clown costume and was killed by Stitches by kicking his head off.

*John McDonnell as The Motley — The leader of an unknown clown cult performing some black magic ritual in a small underground den. He led the ritual to raise Stitches from the dead. After Stitches second death, he tries to raise Stitches from the dead once again by putting his broken egg back to its pieces.

*Tommy Cullen as Dan — Kates boyfriend who bores her by talking about his band.

*Lorna Dempsey as Mary

*Jemma Curran as Jenny

*Ryan Burke as Young Tom, who is only ten years of age.

==Development==
McMahon began working on Stitches after receiving a €600,000 grant from the Irish Film Board, also utilizing funding from MEDIA Europe.  Filming for the movie took place in Ireland.  Stitches premiered in Dublin, Ireland in September 2012. 

==Reception==
Critical reception for Stitches has been mixed to positive,  with entertainment.ie giving it three stars and commenting on the influence of "80s slasher flicks" on the movie.  Of the film, many critics praised Stitches for its inventiveness and Nobles performance, with Bloody Disgusting writing that the movie was "destined to become a cult classic" and carried "heavy replay value".       Dread Central in particular praised the movies kill scenes, calling them "brilliant" and remarking that they were done "with such a rigorous sense of care and detail".  Criticisms for the movie revolved around the film being "an incredibly low budget production" and the plot being "nowhere near as fun as it sounds on paper".   The Screen Daily commented that the "horror stuff is obvious, but staged with showstopping flair and buckets of blood". 

==References==
 

==External links==
* 
*  
*  
* 

 
 
 
 
 
 
 
 