Aballay (film)
{{Infobox film
| name           = Aballay
| image          = Aballay2010Poster.jpg
| caption        = Argentinian poster
| director       = Fernando Spiner
| producer       = Eduardo Carneros Fernando Spiner
| writer         = Antonio Di Benedetto Fernando Spiner
| starring       = Pablo Cedrón
| music          = 
| cinematography = Claudio Beiza
| editing        = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards,       but it did not make the final shortlist.   

==Cast==
* Pablo Cedrón as Aballay
* Nazareno Casero as Julián
* Claudio Rissi as El Muerto
* Mariana Anghileri as Juana
* Luis Ziembrowski as Torres
* Aníbal Guiser as Mercachifle
* Lautaro Delgado as Ángel
* Tobías Mitre as Julián Niño
* Horacio Fontova as Cordobés
* Gabriel Goity as Cura

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Argentine submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 


 
 