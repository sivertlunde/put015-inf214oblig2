Newlyweds (film)
{{Infobox film
| name           = Newlyweds
| image          = 
| alt            = 
| caption        = 
| director       = Edward Burns
| producer       = Edward Burns Aaron Lubin William Rexer
| writer         = Edward Burns
| starring       = Edward Burns Kerry Bishé Marsha Dietlein Caitlin Fitzgerald Max Baker Dara Coleman Johnny Solo
| music          = PT Walkley
| cinematography = William Rexer
| editing        = Janet Gaynor
| studio         = Marlboro Road Gang Productions
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $9,000 
| gross          = 
}}
Newlyweds is a 2011 American comedy/drama film written, directed and starring Edward Burns, with Kerry Bishé, Marsha Dietlein, and Caitlin Fitzgerald. Newlyweds was selected to close the 2011 Tribeca Film Festival. 

==Plot==
The honeymoon period ends quickly for Buzzy (Burns) and Katy (Fitzgerald) when Buzzys sister Linda arrives unannounced to the couples apartment looking for a place to stay. Lindas arrival complicates Buzzy and Katies marriage and forces both to re-evaluate their relationship.

==Cast==
* Edward Burns as Buzzy
* Kerry Bishé as Linda
* Marsha Dietlein as Marsha
* Caitlin Fitzgerald as Katie
* Max Baker as Max
* Dara Coleman as Dara
* Johnny Solo as Miles

==Production==
Newlyweds was shot in a faux-documentary style on location in Tribeca, New York City.  The movie cost only $9,000 to produce and was filmed entirely on a Canon 5D. 

==Soundtrack==
The soundtrack is composed by PT Walkley. 

==Reception==
The film received generally positive reviews and currently holds a 70% positive rating on Rotten Tomatoes based on 10 reviews. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 