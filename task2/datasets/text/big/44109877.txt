Neeyethra Dhanya
{{Infobox film 
| name           = Neeyethra Dhanya
| image          =
| caption        =
| director       = Jesey
| producer       = John Paul (dialogues) John Paul Murali Menaka Menaka Mukesh Mukesh
| music          = G. Devarajan
| cinematography = Anandakkuttan
| editing        = KP Puthran
| studio         = Chithrakaumudi
| distributor    = Chithrakaumudi
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Menaka and Mukesh in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Karthika as Shyama Murali as Hafiz Ali Menaka as Shobha Mukesh as Suresh
*Nahas
*Lissy Priyadarsan as Laila
*Divya Unni
*MG Soman as Captain
*Mala Aravindan as Esthappan
*Valsala Menon

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Arikil Nee Undaayirunnenkil || K. J. Yesudas || ONV Kurup || 
|-
| 2 || Bhoomiye Snehicha || P. Madhuri || ONV Kurup || 
|-
| 3 || Kunkumakkalppadavuthorum || R Usha || ONV Kurup || 
|-
| 4 || Nishaagandhi Neeyethra Dhanya || K. J. Yesudas || ONV Kurup || 
|-
| 5 || Pularikal Sandhyakal || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 