Greenhide
 
{{Infobox film name           = Greenhide image          = caption        = producer       = Charles Chauvel director  Charles Chauvel writer         = Charles Chauvel  Frank White (titles) starring  Bruce Gordon Frank Thorn Irma Deardon distributor    = released       = 20 November 1926  , National Film and Sound Archive Accessed on 4 December 2010.  runtime        = 8,000 feet country        = Australia language       = English  budget         = £3,800 
| gross = ₤1,000   
}}
 Charles Chauvel.

Only part of the film survives today.

==Plot== Bruce Gordon). steal cattle. 

==Cast==
*Elsie Sylvaney as Margery Paton
*Bruce Gordon as Greenhide Gavin
*Jules Murray-Prior as Slab Rawlins
*Irma Dearden as Polly Andrews
*Gerald Barlow as Sam Paton
*Frank Thorn as Tom Mullins
*Joe Mackaway as Phil Mackin
*Alfred Greenup as Bill Mullins
*Nell Kerwin
*George Barrett

==Production== St James Church, Sydney, the ceremony officiated by Charles brother, the Reverend John Chauvel. 
 Walloon Station in Dawson Valley, Queensland.  The production encampment, a collection of tents accommodating twenty people, was informally named "Camp Greenhide" by locals.  Interior filming took place in a studio in Brisbane.  Chauvel played a phonograph recording of "In a Monastery Garden" to induce realistic tears from Elsa Chauvel without the need to use glycerine drops. 

==Release==
Greenhide was screened throughout most of Queensland without the use of a distribution agency.  Charles and Elsa Chauvel personally transported prints of the film from town to town, and tried to convince theatre owners to replace booked American films with a local alternative. Prior to each screening, Elsa would provide a dramatic monologue and introduction. 

In Brisbane and Sydney, Greenhide screened through distributor Hoyts,  and broke records in Brisbane.  However the movie struggled to find distribution in country areas and the Southern cities. In 1927 it was reported the film still had to earn ₤6,030 to recoup all its costs, due in part to the large portion of box office earnings taken by distributors and exhibitors.  This caused Australian Film Productions to go into voluntary liquidation in 1929.  Later the liquidator left Brisbane and copies of the film were abandoned in a building and caught fire. 

Greenhide, in its original form, was 8000 feet long, but today only 2475 feet of 35mm film survive (37 mins at 18 frames per second). 

==References==
 

==External links==
*  

 

 
 
 
 