Jawani Diwani
 
 
{{Infobox film
| name           = Jawani Diwani
| image          = Jawani diwani.jpg
| image_size     = 
| caption        = 
| director       = Narender Bedi
| producer       = Ramesh Behl
| writer         = Inder Raj Anand
| narrator       = 
| starring       = Randhir Kapoor Jaya Bhaduri Balraj Sahni
| music          = R.D. Burman
| cinematography = 
| editing        = 
| distributor    = Rose Films Pvt. Ltd
| soundtrack     = Polydor
| released       = 14 July 1972
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

  1972 Bollywood movie directed by Narender Bedi, and starring Randhir Kapoor, Jaya Bhaduri, Balraj Sahni, Nirupa Roy,  as leads.
 highest grossing film at the Indian Box Office. 

== Plot ==
 
After being stereotyped in the Girl next door|girl-next-door image after her Bollywood debut film, Guddi (1971 film)|Guddi (1971), #Ba|Banerjee, p. 93  Jaya Bhaduri tried to break out of the mould with a glamourous role in the film. 
 Thakur brothers (Iftekhar) household. When they get married, the Thakur breaks all ties with them, she moves into Ravis home, where they live with his younger brother, Vijay (Randhir Kapoor). Subsequently, Vijay meets a girl, Neeta (Jaya Bhaduri), at school, with the usual problems with parents. She turns out to be the Thakurs daughter, who has been promised in marriage to Benny Sinha (Narendra Nath).

Satyen Kappu gives a comical performance as the DJ friend of Randhir Kapoor in two scene-stealing pieces: first, when he impersonates Randhir Kapoors father to meet the boys teacher, and later when he meets Jayas father.

==Cast==
*Randhir Kapoor - Vijay Anand
*Jaya Bhaduri - Neeta Thakur
*Balraj Sahni - Ravi Anand
*Nirupa Roy - Madhu Anand
*Iftekhar - Thakur
*A. K. Hangal - College Principal
*Narendra Nath - Benny Sinha Paintal - Rattan Satyen Kappu - Mamaji
*Jagdish Raj - Mr. Sharma
*Viju Khote - Mr. Gupta
*Brahm Bhardwaj - Mr. Sinha
*Lalita Kumari - Mrs. Sinha

==Crew==
*Director - Narendra Bedi 
*Writer - Inder Raj Anand
*Producer - Ramesh Behl 
*Music Director - Rahul Dev Burman
*Lyricist - Anand Bakshi
*Playback Singers - Asha Bhosle, Kishore Kumar Universal Music India Limited)

==Music==
R. D. Burman composed and produced the music for this movie
Jawani Diwani is known R.D. Burman melodious score.<!--  RD Burman was such a genius; his musical legacy will live on forever. When his music combined with the voices of either Kishore or Asha (or both), the result was electrifying. A case in point is this movie.

Kishore gives the start with the energetic and beautifully sung "Samne Kaun Aya". He later also executes the title song "Yeh Jawani" in his unique comic style (Kishore was said to have the unique ability to put himself right in the shoes of the actor for whom he was doing the singing). However, it is the combination of Kishore with Asha on "Aa Jane Ja" that creates one of the most memorable songs of Indian cinema. The picturization of the song is perfect, there is a sense of mystery in both the setting, the voice and the music. This one song will keep on ringing in your ears long after hearing it and is being remixed now over and over again by so many. COPY PASTED FROM IMDB page user review http://www.imdb.com/title/tt0155775/-->
The lyrics were written by Anand Bakshi.
Song, "Jaane Jaan Dhoondta Phir Raha" made it to #26 on the Binaca Geetmala annual list 1972. The  title of the hit film, Yeh Jawaani Hai Deewani (2013)  was taken from a hit song from the film. 

{| class="wikitable"
|-
! Songs
! Performed By

|-
|"Samne Yeh Kaun Aaya" Kishore Kumar
|-
|"Yeh Jawani Hai Deewani" Kishore Kumar
|-
|"Jaane Jaan Dhoondta Phir Raha" Asha Bhosle & Kishore Kumar
|-
|"Agar Saaz Cheda To" Asha Bhosle & Kishore Kumar
|-
|"Hai Tauba Mujhe Tune" Asha Bhosle
|-
|"Nahi Nahi Abhi Nahin" Asha Bhosle & Kishore Kumar
|}

==Notes==
 

==References==
*  
*  

== External links ==
*  

 
 
 
 


 