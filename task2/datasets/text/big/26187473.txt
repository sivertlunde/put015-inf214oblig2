The Caretaker (film)
 
{{Infobox film
| name           = The Caretaker
| caption        = A poster bearing the films alternate title: The Guest
| image	         = The Caretaker FilmPoster.jpeg
| director       = Clive Donner Michael Birkett
| writer         = Harold Pinter Robert Shaw
| music          = 
| cinematography = Nicolas Roeg
| editing        = Fergus McDonell
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         = £30,000 Alexander Walker, Hollywood, England, Stein and Day, 1974 p250 
}} play of Silver Bear Extraordinary Jury Prize.   

==Cast==
* Alan Bates as Mick
* Donald Pleasence as Mac Davies / Bernard Jenkins Robert Shaw as Aston

==Production==
The movie was made by a partnership of six people, none of whom took payment: Clive Donner, Alan Bates, Robert Shaw, Harold Pinter and Michael Birkett. 

No distributor expressed interest in funding the film, which meant it was unable to attract investment from the   

==Reception==
The film was unable to obtain a release in London until it first screened in New York.  According to Janet Moat, "the film is striking. Donner deploys a non-musical soundtrack, close-ups and two-shots to unsettling and menacing effect." 

==References==
 

==External links==
* 
*  at BFI Screenonline
*  at Haroldpinter.org

 
 

 
 
 
 
 
 
 
 
 
 