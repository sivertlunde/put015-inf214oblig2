Sagar Alias Jacky Reloaded
{{Infobox Film
| name           = Sagar Alias Jacky Reloaded
| image          = Sagar Alias Jacky Reloaded.jpg
| image_size     = 250
| caption        = Theatrical Release Poster
| director       = Amal Neerad
| producer       = Antony Perumbavoor
| writer         = S. N. Swamy
| narrator       =  Shobana Bhavana Bhavana Suman Suman Sampath Raj Rahul Dev
| music          = Gopi Sundar
| cinematography = Amal Neerad
| editing        = Vivek Harshan
| studio         = Aashirvad Cinemas Maxlab Entertainments
| released       =   (world wide release)
| runtime        = 144 min
| country        = India
| language       = Malayalam
| budget         =   
| gross          = 
}}
 Bhavana and Shobana play Jyothirmayi plays a cameo role. The film was dubbed into Hindi as Ek Hi Don.

==Plot==
Sagar Alias Jacky (Mohanlal) is a gangster who is known for solving problems among big gangs. The Chief Ministers(Nedumudi Venu) son-in-law, Manu(Manoj K Jayan) is kidnapped and the police are unable to find a clue. Manus wife Indu(Shobhana Chandrakumar|Shobhana) realises that the police have been influenced by her brother Hari(K. B. Ganesh Kumar|Ganesh) who seems to know the kidnappers. Indu decides to call her best friend Sagar to help find her husband. Sagar calls his best henchmen from all over Kerala for the job. The gang then travels to Goa where Sagars contact tells him that the kidnappers are the Rosario brothers(Sampath Raj,Sherveer Vakil) who run the most powerful gang in the Goa underworld. After considerable difficulties, Sagar and his gang locate the night club where Manu is held hostage and helps him to escape. This audacity does not sit well with the Rosarios who swear vengeance against Sagar. They kidnap one of Sagars favourite gangsters and demand ransom. Sagar responds by kidnapping two of the three Rosario brothers and demands that his man be released at once. Realising that they would never be able to defeat Sagar in a heads-on battle, the Rosario brothers make a deal with Manu and Hari to deliver Sagars current location. The Rosarios then try to assassinate Sagar but fail. Frustrated with repeated defeats, the Rosarios then approach Sagars rival, Nanthakrishna Naina(Suman (actor)|Suman). Naina supplies them with a sniper, Sheikh Imran(Rahul Dev), who is also an internationally infamous assassin. Imran is asked to assassinate Manu so that the Sagar can be framed for the murder. But the Rosarios plan backfires as Indu gives witness that Sagar was not responsible for her husbands murder.
Imran is then asked to assassinate Sagar. He manages to kill Sagars bodyguard but Sagar himself escapes. Sagar then hunts him down and kills him. The Rosarios then kidnap Sagars love interest Arati(Bhavana Balachandran|Bhavana), a news reporter. Sagar is asked to come to a place to work out a deal. When he reaches there, he finds Arati dead.
Enraged, Sagar goes to the Rosarios club and goes on a killing spree. Then he goes to Dubai where Naina is hiding on a yacht. With the help of some gangsters he plants explosives on the yacht and blows it up. The film ends when Sagar informs Indu that her husband has been avenged.

==Cast== Sagar alias Jacky
* Manoj K Jayan as Manu
* Shobana as Indu, Manus wife Bhavana as Arati Menon
* Jagathy Sreekumar as Ashok Kumar
* Nedumudi Venu as the Chief Minister of Kerala Suman as Nanthakrishna Naina
* Sampath Raj as Rosario
* Shereveer Vakil as David Rosario Ganesh as Hari
* Rahul Dev as Sheikh Imran
* Nimal (Special appearance in an Item number) Bala as the Superintendent of Police (Special appearance)
* Vinayakan as Style
* Vanitha Krishnachandran as Azars mother
* Pranav Mohanlal (Special appearance)
* Sumit Naval as Azar

==Award==
* Kerala State Film Award for Best Choreography - Dinesh Kumar

== Soundtrack ==
{{Infobox album
| Name        = Sagar alias Jacky Reloaded
| Type        = Soundtrack
| Artist      = Gopi Sundar
| Cover       =
| Background  =  
| Recorded    = 
| Released    = 2009
| Genre       = Soundtrack
| Length      =  
| Label       = Manorama Music
| Producer    = Aashirvad Cinemas
| Reviews     =  Flash
| This album  = Sagar alias Jacky Reloaded Anwar
|}}

The films soundtrack features 6 songs composed by Gopi Sundar, with lyrics by Gopi Sundar, Jofy Tharakan, Santhosh Varma and Riya Joy.

{| class="wikitable" border="1"
!Track
!Song
!Length
!Singers
!Lyrics
|- 1
|"Sagar alias Jacky Reloaded"
|3:52 min Gopi Sundar, Dr. Burn Gopi Sundar
|- 2
|"Melle Melle"
|3:48 min Punya Srinivas Jofy Tharakan
|- 3
|"Osama"
|3:26 min Suchithra
|Santhosh Varma
|- 4
|"Vennilave"
|4:33 min
|M. G. Sreekumar, Shreya Ghoshal Riya Joy
|- 5
|"Osama (Bigblast)"
|4:09 min
|Suchithra, Nitha Santhosh Varma
|- 6
|"Theme Music"
|3:21 min Gopi Sundar Gopi Sundar
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 