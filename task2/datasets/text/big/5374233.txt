Good Boy, Bad Boy
{{Infobox film
| name           = Good Boy Bad Boy
| image          = Good boy bad boy 1.jpg
| caption        = Theatrical release poster
| director       = Ashwini Chaudhary
| producer       = Raju Farooqui
| starring       =  
| music          = Himesh Reshammiya
| cinematography = Manoj Soni
| distributor    = Mukta Arts Ltd
| released       = 11 May 2007
| runtime        = 110 minutes
| language       = Hindi
| budget         = Rs. 27 crore
| gross          = Rs. 30.50 crore
}} comedy film directed by Ashwini Chaudhary, starring Tusshar Kapoor, Emraan Hashmi, Tanushree Dutta, Isha Sharvani and Paresh Rawal. Produced by Raju Farooqui under the banner of Mukta Arts Ltd, the film is the remake of 1992 film Class Act starring Kid N Play. The film was an average grosser at the box-office.

==Plot==
Raju Malhotra (Emraan Hashmi) and Rajan Malhotra (Tusshar Kapoor) study in the same college but are poles apart in everything. While Raju is a brat, poor in studies but good in sports, Rajan is a brilliant student but a zero in extracurricular activities. The college they study in has a new and strict principal Mr. Awasthi (Paresh Rawal) who wants to transform this ill-reputed college into a most flocked one. Hence he divides the students according to their merit. So while Raju is fit for C section, for poorly faring students, Rajan easily gets access to the A section with his 90% marks. But Raju falls for a girl in A-section and hence swaps his place with a hesitant Rajan who enters C-section for the first time in his life. Mr. Awasthi comes to know of this and decides to teach them a lesson by sending their names to a quiz and dance competition. So now, geeky Rajan will have to dance while brat Raju will have to answer question hurled at him. What will Raju and Rajan do? Will they accept the challenge or just scoot off?

==Cast==
* Tusshar Kapoor as Rajan Malhotra/Raju Malhotra
* Emraan Hashmi as Raju Malhotra/Rajan Malhotra
* Tanushree Dutta as Dinky Kapur
* Isha Sharvani as Rashmi Awasthi
* Paresh Rawal as Principal Diwanchand "Diwan" Awasthi
* Manu Malik as Raj

==Songs==
* "Meri Awaargi" - Himesh reshmaiya
* "Good Boy Bad Boy" - himesh reshamiya
* "Ashiqana Aalam" - himesh reshamiya
* "Dard-E-Dil" (Zubeen Garg)
* "Good Boy Bad Boy" - Remix
* "Meri Aawargi" - Remix By Dj Suketu
* "Aashiqana Aalam Hai" - Remix By Dj A_Myth
* "Dard-E-Dil" - Remix By Dj A_Myth

== External links ==
*  

 

 
 
 