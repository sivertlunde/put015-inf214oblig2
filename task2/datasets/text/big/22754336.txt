Ganesha Subramanya
{{Infobox Film
| name = Ganesha Subramanya
| image =
| image_size =
| caption =
| director = Phani Ramachandra
| producer =
| writer =
| narrator =
| starring =
| music = V.Manohar
| cinematography =
| editing =
| studio =
| distributor =
| released = 1992
| runtime =
| country = India
| language = Kannada
| budget =
| gross =
| preceded_by =
| followed_by =
}}

Ganesha Subramanya, a Kannada comedy film directed by popular director Phani Ramachandra, is a story of two brothers whove been indoctrinated by their father into remaining bachelors.

== Plot ==

Ganesha and Subramanya, two brothers, promised their late father that they would remain bachelors until they achieve something significant in their lives. Their father thinks marriage would hinder personal achievements. Ganesha works at a construction firm, while younger brother Subramanya pursues fine arts. They shun female company, recoiling at the very sight of ladies. Much to their chagrin, they find themselves being sought by the fair sex much too often. Be it the girl residing in the house opposite to theirs or the middle aged lady house-owner, they always find themselves in situations that they think could shake their principles.

Eventually, to be able to live in peace, they move out of their own house to a rented one, whose owner is obsessed with story-telling. Subramanya gets a chance to attended a specific fine arts course that he thinks would be useful to his career and has to go out of town for one year. What happens to Ganesha in Subramanyas absence, if the two brothers achieve something significant in their lives or if they change their stance on marriage, forms the rest of the story.

== Cast ==
*Anant Nag as Ganesha
*Ramesh Bhat as Subramanya
*Shivaram as their father
*Mukhyamantri Chandru as the house-owner
*Bank Janardhan as the house-broker
*Hema Chaudhary

 
 
 


 