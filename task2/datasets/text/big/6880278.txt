Black Irish (film)
{{Infobox Film 
 | name = Black irish
 | image = Black irish post.jpg
 | caption = Promotional poster for Black Irish
 | director = Brad Gann
 | writer = Brad Gann
 | starring = Michael Angarano Brendan Gleeson Tom Guiry Melissa Leo Emily VanCamp
 | producer = J. Todd Harris Kelly Crean Jeff Orenstein Mark Donadio
| distributor = Anywhere Road
 | released = July 26, 2007
 | country = United States
 | gross =  $6,900 (USA) 
 | language = English 
 | }}

Black Irish is a 2007 drama film written and directed by Brad Gann.

==Plot==
Sixteen-year-old Cole McKay’s struggle for independence is put to the test as his South Boston Irish-Catholic family implodes around him. Older brother Terry is descending into a life of drugs and crime, pregnant sister Kathleen is being sent away to cover up the shame of unwed motherhood and Cole’s father, Desmond, spends his days in a fog of alcohol and self-pity, silently torturing himself over what might have been. The one thing keeping young Cole’s head above water is his love of baseball. The movie starts with Cole practicing his pitching, when he is picked up by his family to attend the funeral of Desmonds sister.   A talented baseball pitcher, Cole overcomes self-doubt and family indifference to fight his way into the state championships. To get there however, he must make a life and death decision, one that will change the McKay family forever.

==Cast==
*Michael Angarano as Cole McKay
*Brendan Gleeson as Desmond McKay
*Tom Guiry as Terry McKay
*Melissa Leo as Margaret McKay
*Emily VanCamp as Kathleen McKay
*Michael Rispoli as Joey
*Francis Capra as Anthony
*Mark S. Cartier as Mr. Quint

==Release==
*The film premiered at the Newport Beach Film Festival on 20 April 2007. It received a limited release, opening in Los Angeles, New York City and Boston on 26 October 2007. It was released on DVD on 8 January 2008 in the US and Canada.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 