Le Parfum de la dame en noir (1949 film)
{{Infobox film
| name           =Le Parfum de la dame en noir
| image          =
| image_size     =
| caption        =
| director       =Louis Daquin
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editor       =
| distributor    =
| released       = 1949
| runtime        =
| country        = France French
| budget         =
}} French crime film directed by Louis Daquin and starring Hélène Perdrière, Serge Reggiani and Marcel Herrand. It is an adaptation of the novel The Perfume of the Lady in Black by Gaston Leroux.
 The Mystery of the Yellow Room.

==Cast==
* Hélène Perdrière - Mathilde Stangerson
* Serge Reggiani - Joseph Rouletabille
* Lucien Nat - Robert Darzac
* Michel Piccoli - Lebel
* Gaston Modot - Mathieu
* Marcel Herrand - Larsan
* Arthur Devère - Père Jacques
* Yvette Etiévant

==External links==
* 

 
 
 
 
 
 
 