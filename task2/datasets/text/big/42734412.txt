Don Quixote (1923 film)
{{Infobox film
| name           = Don Quixote
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       =  Maurice Elvey
| producer       = 
| writer         =  Sinclair Hill
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = December 1923
| runtime        = 50 minutes
| country        = United Kingdom
| language       = 
| budget         = 
| gross          = 
}} silent comedy film, directed by Maurice Elvey, based on the novel Don Quixote by Miguel de Cervantes.    The film stars Jerrold Robertshaw, George Robey, Frank Arlton, and Marie Blanche.

==Cast==
*Jerrold Robertshaw as Don Quixote
*George Robey as Sancho Panza
*Frank Arlton as Father Perez
*Marie Blanche as The Housekeeper
*Bertram Burleigh as Sanson Carrasco
*Adeline Hayden Coffin as The Duchess
*Sydney Fairbrother as Tereza
*Minna Leslie as Dulcinea Edward ONeill as The Duke

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 