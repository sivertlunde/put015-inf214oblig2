The Last Good Time
 
{{Infobox film
| name = The Last Good Time
| image = Poster of the movie The Last Good Time.jpg
| caption =
| director = Bob Balaban
| producer = Dean Silvers
| writer = Bob Balaban Richard Bausch (novel)
| starring = Armin Mueller-Stahl Olivia dAbo Maureen Stapleton Lionel Stander Adrian Pasdar Kevin Corrigan Beatrice Winde
| music = Jonathan Tunick
| cinematography = Claudia Raschke
| editing = Hughes Winborne
| distributor = The Samuel Goldwyn Company
| released =  
| runtime = 90 minutes
| language = English
| country = United States
| budget =
| gross = $65,081
}}

The Last Good Time is a 1994 drama film, released in early 1995, starring Armin Mueller-Stahl, Olivia dAbo, Maureen Stapleton and Lionel Stander in his final theatrical role. Directed by Bob Balaban, the film was based on a 1984 novel of the same name by Richard Bausch.

==Plot summary==
The film opens showing a small New York City apartment building at night, where a young couple
on the top floor are seen arguing. Charlotte (dAbo) throws a bag out of her window, with some contents landing locker key, and by the next day, Charlotte and her boyfriend Eddie (Adrian Pasdar) have apparently vacated the premises.

Focus is then shifted towards Joseph, a widowed German/Jewish violinist who lives a
simple yet structured everyday routine that includes writing in a journal about what he will wear, visiting Howard (Stander), an old friend at a nursing home who is in the final stages of Alzheimers disease, trying to make arrangements on a tax debt, buying food at a discount grocery store, and then returning home to read books and play his violin.

Charlotte returns to the apartment the following evening to recover missing contents from her bag, but notices her boyfriends car pull up while doing so and quickly rushes into the building. She asks Joseph for a towel to dry off after the heavy rain, but subsequently collapses on his apartment floor. He
gives her a blanket and pillow and lets her sleep there.

After some hesitation, Joseph lets Charlotte stay in his apartment indefinitely. Despite
the age difference, the two slowly form a platonic bond; Joseph discusses his past violin career and
interest in philosophers; Charlotte reveals that her now former boyfriend Eddie was involved in
organized crime and that the relationship was purely for financial reasons. Charlotte is additionally
impressed that Joseph – despite his age – could remember the words of philosophers, whereas her
former boyfriend – a one-time musician – could barely remember "words to his own songs."

While the two opposites slowly forge a more intimate relationship, matters begin to change dramatically for
Joseph – due to tax collectors, he is forced to withdraw his life savings of $6000 from his bank account; his
old friend Howard dies, and upon returning home with Charlotte from a bar one night, he finds his
apartment ransacked, his violin smashed and his entire life savings stolen. Frank (Kevin Corrigan), Eddies fellow gangster,
had been following Charlotte around and traced her to Josephs apartment. Joseph then gives Charlotte
the missing locker key, the item Eddie and Frank were looking for. Charlotte angrily asks Joseph why he had
this all along but never gave it to her, and Joseph explains because he didnt want her to leave.

Joseph, back to being the lonely man he was before, slowly reconciles with his elderly neighbor Ida (Stapleton),
who he introduced to Charlotte and Howard, but had also been unfriendly towards for being too inquisitive about his life.

Charlotte returns a few days later informing Joseph she sold the locker key to one of Eddies criminal associates for $7000.
She gives Joseph $6000 of that money – the sum Joseph lost – and keeps the remaining $1000 for herself to start her life over, claiming she
knows a friend in the city who owes her a favor. The two kiss and say goodbye.

The same morning, Joseph knocks on Idas door and asks if she would like to accompany him on a NYC ferry the next day. Ida is interested, and Joseph agrees to pick her up the next morning.

==Critical response==
The limited release indy film received a plurality of positive reviews from critics, earning a "89% Fresh" rating at Rotten Tomatoes. 

Additionally, the film won awards at Frances Avignon Film Festival and Long Islands Hamptons International Film Festival. 

==References==
 

==External links==
 
* 
*  
*   at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 
 
 