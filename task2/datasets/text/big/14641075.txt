Kyō Kara Ore Wa!!
{{Infobox animanga/Header
| name            = Kyō Kara Ore Wa!!
| image           =  
| caption         = Volume one of the wideban version of Kyō Kara Ore Wa!!
| ja_kanji        = 今日から俺は!!
| ja_romaji       = 
| genre           = Action genre|Action, comedy
}}
{{Infobox animanga/Print
| type            = manga
| author          = Hiroyuki Nishimori
| publisher       = Shogakukan
| demographic     = Shōnen manga|Shōnen
| magazine        = Shōnen Sunday Super  Weekly Shōnen Sunday
| first           = 1988
| last            = 1997
| volumes         = 38
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = ova Takeshi Mori   Masami Anō
| producer        = 
| writer          = 
| music           = 
| studio          = Studio Pierrot
| licensee        = 
| first           = April 1, 1993
| last            = December 21, 1996
| runtime         = 60 minutes per episode
| episodes        = 10
| episode_list    = 
}}
{{Infobox animanga/Video
| type            =  live video
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = January 8, 1993
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live video
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = July 9, 1993
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live video
| title           = Kyō Kara Ore Wa!! Nan demo ari no 17 sai
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = February 19, 1994
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live video
| title           = Kyō Kara Ore Wa!! Dengeki no 17 sai
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = January 13, 1995
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live video
| title           = Kyō Kara Ore Wa!! Guts daze 17 sai
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = April 11, 1997
| runtime         = 
}}
{{Infobox animanga/Video
| type            = live video
| title           = Kyō Kara Ore Wa!! Arashi wo yobu 17 sai
| director        = Tsutomu Kashima
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = May 9, 1997
| runtime         = 
}}
 

  is a Japanese shōnen manga|shōnen manga by Hiroyuki Nishimori. The series was initially published in Shōnen Sunday Super running monthly from 1988 volume 9 through 1990 volume 38. The series was then promoted to the weekly magazine Shōnen Sunday from 1990 volume 40 through 1997 volume 47. The first manga tankōbon was published in December 14, 1989  and last thirty-eighth on March 18, 1998. 

==Story==
Two boys, Takashi Mitsuhashi and Shinji Itō meet each other at a salon. It turns out both boys are transferring to a new school and decide to take the opportunity to reinvent themselves. No longer will they be run of the mill high school students, they will become the biggest delinquents Japan has ever seen!

==Characters==

===Nanyou High===

;  
:Main protagonist, 181 cm tall. At the beginning of the story, he was around 15-16 years old. He gave up his ordinary school life and decided to "become a delinquent from today on" (thus the title of this manga), after moving to his new home in Chiba. Being a very athletic person, he used his talent for the sole purpose of brawling and actually becomes a real delinquent. To make his appearance more noticeable, he dyed his hair blond. Being the most self-centered person in this manga, his own safety comes before anyone elses, he often sacrifices his "friends" for his cause. He is greedy and cunning. Although, beneath his devilish personality lies a softer side, which shows from time to time, especially when he is angry. His fighting style is based around his speed, as he often dodges most attacks, becoming surprised whenever he is forced to block due to being faster than most of the people he encounters. His love interest is Riko-chan.

;  
:Main protagonist, 180 cm tall (205 cm including his hair). A complete reversal of Mitsuhashi, Itous the kind of righteous person fighting for justice. Just like Mitsuhashi, Itou got transferred to Nan highs first year. Like his personality, his fighting style is also the opposite of Mitsuhashis; hes incredibly tough and often stands up from any beating, even in the most bleak of situations. While he may be as strong as Mitsuhashi, Itou is beat up much more often than Mitsuhashi due to often sacrificing himself to protect others as well as fighting fair in unfair situations or against numerable opponents. Due to his help, he wins the affection of Kyouko-chan, an ex-delinquent girl, at the start of the series.

;  
:Main heroine, the only daughter of the Akasaka style dojo. Originally she visited another senior high school than Mitsuhashi and Itou, but ultimately decided to transfer to Nan high instead to repay Mitsuhashis favor. Behind her pretty looks lies someone pretty skilled in Aikido; she can handle most delinquents on her own despite her small body size. While she used to be very popular at her old school, in Nan high she is initially considered "weird" because of her constantly hanging out with Mitsuhashi. As the manga goes on, she spends more and more time with Mitsuhashi, and he sometimes intrudes in her home/dojo, often irritating her father, who also seems to get used to Mitsuhashi. She is romantically interested in Mitsuhashi and is one of the people close to him who know his softer side. She is the only person who refers to Mitsuhashi as "San-chan" because 三 (mitsu) is also read as the number three (san).

;  
:Student of the Akasaka style dojo, and in love with Riko. 160cm tall. His sense of justice is as strong as Itous, and he is a modest person who is willing to help people in need out whenever he can. Despite his initially weak body, he never backed down to any delinquents threat. He was transferred to Nan high together with Riko, which is why everyone calls him "Ryou-kun" like Riko does. Surprisingly, he can hold his own against Mitsuhashi.

;  
:A handsome transfer student from Saitama, around 180 cm tall (he has short legs though). He originally transferred to Nan high to avoid trouble at his home place of Saitama during the Kitagawa incident, and was quickly at enmity with Mitsuhashi because of his righteous nature, even more so than Itou. When he first appeared, he seems to have an equal charm towards girls as Itou, which only grew when he defeated the duo in a match of Judo. At the end of the Kitagawa incident, he got to know Mitsuhashis true nature and became friend with them, aiding them multiple times in battle in order to repay their favor. A running gag in the manga, is whenever Itou is doing something emotional with Kyouko, he appears and witness a embarrassing scene.

;  
:A slacky delinquent from Nan high, who quickly became Mitsuhashis and Itous lackey and informant due to his nature "always succumbing to the circumstances". Even though he is no strong fighter, people respect him for the sole sake of him being able to call for Itou and Mitsuhashis help at any given time.

;  
:A delinquent and good friend with Sagawa, also being considered a lackey of Mitsuhashi and Itou.

;  
:Rikos girl friend at Nan high, with lots of screentime, but very few times they refer to her with her name.

;  
:A junior with passable strength, at least 2 years younger than Mitsuhashi. Mitsuhashi gave him the nickname of "pisshead" because he once peed on them while they were trapped in a well. When introduced, he is an overconfident middle school student and tries to defeat Mitsuhashi. However, he soon gains immense respect for Mitsuhashi after witnessing his strength and looks up to him. Mitsuhashi regards him as a bud despite his young age.

===Seiran Girl School===

;  
:The second heroine beside Riko. Although a former Yankee girl, shes very beautiful and guys try to hit on her on occasion. She becomes Itous girlfriend near the beginning of the series after he takes a beating in order to rescue her from a group of thugs. Her parents approve of her dating Itou and are just grateful that she isnt dating Mitsuhashi.

===Benibane High===

;  
:Beni highs Banchou, the eldest son of four brothers, 192cm tall. Despite his incredible strength, his intelligence isnt up to scratch. Even though he looks handsome at first glance, he cannot get a girlfriend because he often unintentionally ends up looking like a fool. Imai possesses impressive strength, usually throwing his opponents. However, he still couldnt win against many of the main antagonists. Mitsuhashi often tries to hinder Imai, especially when good things happen to him. Itou, on the other hand, usually gets along with Imai, respecting his honest efforts. He is one of the characters in this manga with the most character development, often looking for ways to better himself.

;  
:Imais lackey and best friend. He respects Imai wholeheartedly and made a promise to follow Imai his entire life. He tends to get involved in a lot of troubles with Imai, and because of his small size, hes usually the first to get picked on. Despite his lack of strength, he has a lot of spirit, which sometimes intimidates people.

;  
:An obese giant (197cm tall), who also decided to follow Imai as his lackey, after he was defeated in a fight. It is implied that he has anger issues and was thus expelled from his old school. His size makes him a formidable fighter even Imai acknowledges. He likes to refer to Imai as "aniki".

;  
:Originally from Ibaraki, Nakano transferred to Beni high due to his quarrel with the local yakuza boss son. Despite his short stature, he was able to fight on par with Itou and Mitsuhashi during their school trip to Kyoto. His original goal in going to Chiba was to defeat Mitsuhashi, although he later settles in and even joins them in their raid of Akehisa.

===Akehisa High===

;  
:Being the banchou of the delinquent high school Akehisa, he is feared among his enemies and respected amongst his fellowship. He is on par with Mitsuhashi strength-wise, but stopped with his delinquent career the moment he stepped out of his school and entered society.

;  
:The main recurring villain of this manga, and as dangerous as a rabid dog. His methods are as dirty as Mitsuhashis, however his own twisted character makes him always go a step further than Mitsuhashi. He is the only villain in this manga who managed to completely disable Mitsuhashis abilities during their last encounter by severely injuring him.

;  

===Others===

;  
:A beautiful girl who usually fights with a shinai. When she is introduced into the story, she notices her cousin Satorus bruises from Hokunei. Satoru lies and tells her that it was Mitsuhashi who did it, in order to keep her out of it. However, that only makes things worse and she tries going after Mitsuhashi and the others. Later on, she develops a crush on Imai due to his good nature. She was herself a (unwilling) delinquent and decided to change herself to a normal girl when she transferred, although it didnt last very long and she soon became respected by the other delinquents of her new school.

;  

==Media==

===Manga===

Manga series Kyō Kara Ore Wa!! is written and illustrated by Hiroyuki Nishimori and originally run in manga magazine Shōnen Sunday Super running monthly from 1988 to 1990; later it was moved to the weekly manga magazine Shōnen Sunday from 1990. The series was concluded in the 1997. The first manga tankōbon was published in December 14, 1989    and last thirty-eighth on March 18, 1998. Kyou Kara Ore Wa has sold over 33 million copies world wide.   

====Volume List====
{{Graphic novel list/header
 | OneLanguage = yes
}}
{{Graphic novel list
 | VolumeNumber    = 01
 | RelDate         = December 14, 1989 
 | ISBN            = 4-09-122401-6
 | ChapterListCol1 = 
* 001.  
* 002.  
* 003.  
* 004.  
 | ChapterListCol2 = 
* 005.  
* 006.  
* 007.  
 | Summary         = 
}}
 

===OVA===
 OVA series Takeshi Mori and Masami Anō. Each episode lasts 60-minutes. The first was released on April 1, 1993 and last on December 21, 1996.   

====Episode list====
{|class="wikitable" style="width:100%; background:#FFF;"
|- style="border-bottom: 3px solid #CCF"
! width="5%"  | #
! Title
! width="20%" | Original airdate 
|-
{{Japanese episode list
 | EpisodeNumber   = 1
 | KanjiTitle      = 今日から俺は！！　1
 | RomajiTitle     = Kyō Kara Ore Wa!! 1
 | OriginalAirDate = April 1, 1993
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 2
 | EnglishTitle    = 
 | KanjiTitle      = 今日から俺は！！　２　夕日に赤いヒキョー者
 | RomajiTitle     = Kyō Kara Ore Wa!! 2 Yūhi ni Akai Hikyo-mono
 | OriginalAirDate = May 1, 1994
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 3
 | EnglishTitle    = 
 | KanjiTitle      = 今日から俺は！！　３　日本一のワガママ男
 | RomajiTitle     = Kyō Kara Ore Wa!! 3 Nihonichi no Wagamama Otoko
 | OriginalAirDate = August 1, 1994
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 4
 | KanjiTitle      = 今日から俺は！！　４　なのにあなたは京都へいくの！
 | RomajiTitle     = Kyō Kara Ore Wa!! 4 Na no ni Anata wa  Kyōto e iku no!
 | OriginalAirDate = May 1, 1995
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 5
 | KanjiTitle      = 今日から俺は！！　５　名もなく貧しくズルッこく
 | RomajiTitle     = Kyō Kara Ore Wa!! 5 Namonaku Mazushiku Zurukkoku
 | OriginalAirDate = November 1, 1995
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 6
 | KanjiTitle      = 今日から俺は！！　６　逆襲・暴徒達のララバイ
 | RomajiTitle     = Kyō Kara Ore Wa!! 6 Gyakushü・Bōtodachi no Rarabai
 | OriginalAirDate = February 1, 1996
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 7
 | KanjiTitle      = 今日から俺は！！　７　マブダチ作戦Go！Go！Go！
 | RomajiTitle     = Kyō Kara Ore Wa!! 7 Mabudachi Sakusen Go！Go！Go！
 | OriginalAirDate = August 1, 1996
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 8
 | KanjiTitle      = 今日から俺は！！　８　道場やぶりをぶっ飛ばせ！
 | RomajiTitle     = Kyō Kara Ore Wa!! 8 Dōjō Yaburi o Buttobase!
 | OriginalAirDate = December 21, 1996
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 9
 | KanjiTitle      = 今日から俺は！！　９　ワンス・アポン・ア・タイム・イン・千葉
 | RomajiTitle     = Kyō Kara Ore Wa!! 9 Onsu・Apon・A・Taimu・In・Chiba
 | OriginalAirDate = December 21, 1996
 | ShortSummary    = 
}}
{{Japanese episode list
 | EpisodeNumber   = 10
 | KanjiTitle      = 今日から俺は！！　１０　極道のつまはじき達
 | RomajiTitle     = Kyō Kara Ore Wa!! 10 Gokudō no Tsumahajiki-dachi
 | OriginalAirDate = December 21, 1996
 | ShortSummary    = 
}}
|}

== References ==
 

==External links==
*   
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 