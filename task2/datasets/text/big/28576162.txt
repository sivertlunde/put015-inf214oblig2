The Passionate Adventure
{{Infobox film
| name           = The Passionate Adventure
| image          = 
| image_size     = 
| caption        = 
| director       = Graham Cutts
| producer       = Michael Balcon Michael Morton Frank Stayton (novel) Marjorie Daw Victor McLaglen
| music          = 
| cinematography = Claude L. McDonnell
| editing        = 
| studio         = Gainsborough Pictures
| released       = 1924
| runtime        = 
| country        = UK Silent English English intertitles
}}
 Michael Morton, with Hitchcock also credited as assistant director to Cutts. 

The Passionate Adventure is also notable as the first film released under the aegis of Michael Balcons newly formed Gainsborough Pictures.

==Plot== Marjorie Daw) and takes on the role of her unofficial protector.

This does not go down well with Vickys East End criminal element boyfriend Herb (Victor McLaglen) who becomes increasingly suspicious and jealous about her association with Adrian, until a showdown in inevitable.  Adrian uses his wits to overcome Herbs brute force, and hands him over to the police who have wanted him for some time.  With Herb in custody and Vickys safety assured, Adrian returns west to Drusilla invigorated by his East End experience and with his feelings of passion towards her evidently restored.  They embrace at the bottom of the staircase, which the appreciative Drusilla starts to climb.

==Reception==

 , reviewing the film for the London Evening Standard, wrote that ‘For absolute skill in production and for inspiration in setting, The Passionate Adventure reaches a high level, far higher than was actually entailed by the particular story Graham Cutts and his coadjutors here had to handle.

==Cast==
* Clive Brook as Adrian St. Clair
* Alice Joyce as Drusilla St. Clair Marjorie Daw as Vicky
* Victor McLaglen as Herb Harris
* Lillian Hall-Davis as Pamela
* Joseph R. Tozer as Inspector Sladen
* Mary Brough as Lady Rolis
* John F. Hamilton as Bill

==See also==
* Alfred Hitchcock filmography

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 