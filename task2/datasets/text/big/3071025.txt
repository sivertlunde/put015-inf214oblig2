Cold Showers
{{Infobox film
| name           = Cold Showers
| image          = Douchesfroidesposter.jpg
| caption        = Promotional poster
| director       = Antony Cordier
| producer       = Pascal Caucheteux Sébastien Lemercier
| writer         = Antony Cordier Julie Peyr
| starring       = Johan Libéreau Salomé Stévenin Pierre Perrier Steve Tran Florence Thomassin
| music          = Nicholas Lemercier 	
| cinematography = Nicolas Gaurin 	
| editing        = Emmanuelle Castro 
| distributor    = Bac Films 
| released       =  
| runtime        = 102 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}} 2005 Cinema French drama film directed by Antony Cordier. It was a Directors Fortnight Selection at 2005 Cannes Film Festival. The film tells the story of three teenagers, a girl, Vanessa, and two boys, Mickael and Clement, who face changes and problems over a period of three months as they enter adulthood. The film attracted attention on its release due to the full-frontal nudity of several young French actors.

==Plot==
Mickael (Johan Libereau) is from a poor   and his life is focused on his sport and on his girlfriend Vanessa (Salomé Stévenin). One of Mickaels teammates Clément (Pierre Perrier) is from a wealthy family: his father Louis Steiner (Aurelien Recoing) uses a wheelchair and his mother Mathilde (Claire Nebout) is a woman of the world and society. Louis decides to sponsor the judo team, buys them outfits, and asks Mickael to work with Clement to perfect his technique and prepare the judo team for a French championship.

Mickael and Clément relate well and while Mickael is a winning player, Clément is smarter and understands the intrinsic rules of the game better. An incident occurs that forces Mickael to take the position of a wounded team mate and in doing so he must lose eight kilos to qualify for the championship team. The struggle to lose weight (he is already in ideal physical condition) places stress on both Mickael and his family and teammates. Mickael and Vanessa include Clément in their camaraderie, a situation which evolves into a ménage à trois as the three have group sex in the after hours gym. Vanessa reacts as though this is the greatest physical feeling ever, Clément is smitten, and Mickael has troubling doubts. When the three decide to try it again in a hotel room Mickael is so conflicted that he does not join the other two, only listening to their cavorting in the bathtub feeling inferior to the smarter, wealthier Clément. But on the judo side, the team plays the championship and Mickaels delicate sense of self worth is restored for a moment. It is the manner in which the trio of teenagers resolve their antics that closes the film.

==Cast==
* Johan Libéreau as Mickael
* Salomé Stévenin as Vanessa
* Pierre Perrier as Clément
* Florence Thomassin as Annie
* Jean-Philippe Ecoffey as Gérard
* Aurélien Recoing as Louis Steiner
* Claire Nebout as Mathilde Steiner
* Denis Falgoux as the coach
* Magali Woch as Mickaels sister
* Camille Japy as coachs wife
* Dominique Cabrera as the nurse in school Sarah Pratt as the examiner
* Julie Boulanger as Valentine

==Soundtrack==
The soundtrack for this film contains songs by Julie Delpy and Galt MacDermot. The score is composed by Nicolas Lemercier. The main song of the film is called "Central Park".

==Awards and nominations==
César Awards 
* Nominated:
** Best First Film
Prix Louis Delluc
* Won
** Best First Film
Taipei International Film Festival 
* Won:
** Grand Prize
Verona International Film Festival 
* Won:
** Grand Prize 
Marseille Film Festival 
* Won:
** Critic Prize
French Press Golden Star 
* Won:
** Best First Film  French Syndicate of Cinema Critics 
* Won:
** Best Promising Actor - Johan Libéreau 
Moulins Film Festival 
* Won:
** Best Actress - Florence Thomassin 
La Ciotat Film Festival 
* Won:
** Best Actress - Salomé Stévenin

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 