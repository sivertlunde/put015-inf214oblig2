Sakhi (film)
{{Infobox film
| name           = Sakhi
| image          = Sakhi Marathi Movie.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sanjay Surkar
| producer       = Lata Narvekar Bharti Achrekar
| screenplay     = 
| story          = 
| starring       = Ashok Saraf Sonali Kulkarni Subodh Bhave Usha Nadkarni
| music          = Ashok Patki
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
Sakhi is a Marathi movie released on 16 May 2008.  Produced by Lata Narvekar along with Bharti Achrekar and, directed by Sanjay Surkar. The movie is based on the friendship between a widow and a married woman, whose friendship is misunderstood, but how they support each other through thick and thin.

== Synopsis ==
Suryakant Jagdale, a middle-aged widower, has lost his job under the Compulsory Retirement Scheme. His philanthropic ideas are met with indifferent response at his native village where he happens to save the life of Nishi Surve, a young woman attempting suicide after being abandoned by her in-laws. Surya discovers Nishis singing talent and encourages her to pursue the art.

Meanwhile, the villagers due to their conservative mentality frown upon their strange relationship. Disgusted by the unfair accusations, Surya and Nishi leave the village, go to Mumbai in pursuit of a bright future and after repeated rejections, Ravi Desai, a budding composer gives Nishis career the much needed boost. She rapidly achieves stardom while Surya faces dejection. Though Nishi whole-heartedly respects Surya, it is Ravi who takes control of Nishis life.

== Cast ==

The cast includes 
*Ashok Saraf as Suryankant Jagdale
*Sonali Kulkarni as Nishi Surve
*Subodh Bhave as Ravi Desai 
*Usha Nadkarni as Kunda Tai & others

==Soundtrack==
The music is provided by Ashok Patki.

== References ==
 
 

== External links ==
*  
*  

 
 
 


 