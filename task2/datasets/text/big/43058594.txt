Mourning Grave
{{Infobox film name           = Mourning Grave image          = MourningGrave.jpg director       = Oh In-chun producer       = Lee Jong-ho   Ju Seong-ho   Bang Mi-jeong   Lee Suk-joon writer         =   starring       = Kang Ha-neul   Kim So-eun  music          =   cinematography = Kwon Sang-jun editing        = Kim Chang-ju   Kim Woo-hyeon studio = Ghost Pictures Jupiter Films distributor    = Little Big Pictures 9ers Entertainment (international) released       =   runtime        = 90 minutes country        = South Korea language       = Korean gross          = 
}}
Mourning Grave ( ; lit. "Girl Ghost Story" ) is a 2014 South Korean mystery horror film starring Kang Ha-neul and Kim So-eun.     

==Cast==
*Kang Ha-neul as In-su
*Kim So-eun as Ghost girl
*Kim Jung-tae as Seon-il
*Han Hye-rin as Hyeon-ji
*Park Doo-sik as Hae-cheol
*Joo Min-ha as Na-ra
*Joo Da-young as Seong-hee
*Kwak Jung-wook as Ki-tae 
*Kim Yeong-choon 
*Lee Ah-hyun as Oh Mi-hee

==Original soundtrack==
{{Infobox album
| Name        = Mourning Grave:   Original Sound Track
| Type        = Soundtrack
| Artist      = MBLAQ
| Cover       = 
| Released    = June 26, 2014
| Genre       = Pop music|Pop, K-pop, soundtrack
| Label       = Ogam Entertainment
}}

{{tracklist
| extra_column= Artist
| title1          = 니가 떠난 그 자리
| note1           = The Place You Left
| extra1          = MBLAQ
| length1         = 4:09
| title2          = 니가 떠난 그 자리 (Inst.)
| note2           = The Place You Left (Inst.)
| extra2          = MBLAQ
| length2         = 4:09
}}

==Box office==
Since its domestic release on July 3, 2014, the film has grossed  , with 216,000 ticket sales. 

==International release== Cannes Film Market, Mourning Grave was pre-sold to China, Hong Kong, Taiwan, Singapore and Mongolia. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 
 