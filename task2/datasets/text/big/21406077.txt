Kathantara
 


{{Infobox Film  name = Kathantara image = caption = Movie poster for Kathantara starring = Anu Chowdhury Bhaswati Basu Rasesh Mohanty Bedaprasad Dash director = Himansu Khatua producer = Iti Samanta  writer = Himansu Khatua editor = Rabi Pattnaik  music = Swarup Nayak cinematography = Sameer Mahajan  distributor = Kadambinee Media Pvt. Ltd  released       =   runtime        =   116 minute  country = India language = Bengali 
}} English Another Story) is a 2007 Indian Oriya film directed by Himansu Khatua, a story of tribulations of 1999 Orissa cyclone.  Much before the Tsunami became a household name all over the world, the coastal belts of Orissa were hit by what has come to be known as the "Super Cyclone" that killed more than 10,000 people and rendered still more homeless.  But public memory is short lived and it is a commendable effort on the part of the director Himansu Khatua to rake up the issue in his second feature film, perhaps the only Indian film that recalls those fateful days that changed the lives of so many people within a span of just a few days and its aftermath.

==Synopsis==
 
 
 
Kathantara, a feature film in Oriya language, is an attempt to throw light on the trials and tribulations of 1999 Orissa cyclone survivors with particular emphasis on Kalpana (Anu Chowdhury), a young widow.  Kalpana’s destiny is followed from different perspective and a tale of human predicament is built around her.  The films narrative unfolds with the anniversary function of 1999 Super Cyclone that devastated coastal Orissa.  This was one of the severest cyclones of the last century.  It had caused absolute havoc, leaving thousands dead and many more thousands homeless and destitute.  The worst affected was the Bengali Hindu refugee settlement.  Aids and relief came pouring in.  So also came NGOs, media persons, politicians, and vested-interest groups.  With so much external influence the local value system got affected, corruption and amoral practices became rampant. Dipankar, a TV Journalist from Bangladesh, is engaged in documenting the cyclone anniversary and the status of the post-cyclone survivors.  With the help of an NGO group he covers the programme, and hears a great deal about Kalpana — the much publicised cyclone widow.  Dipankar takes a keen interest on her and wants to do a special feature.

The genesis of Kalpana’s ordeal is rooted in history.  Like others in that locality her father a forced migrant from East Bengal during the partition of India, struggled all his life to establish his identity, even though he was a citizen of his adopted homeland.  Kalpana born and brought up in coastal Orissa, was leading a harmonious life with her fisherman husband.  As they were dreaming to add more colours into their life, the infamous super cyclone of October 1999 strikes.  It leaves all her family member dead.  Kalpana miraculously survives.  And, with the support of her childhood friend Akshaya, she limps back to a life now full of emptiness.  The links with her past being snapped off, Kalpana feels alienated in her own native place.  A sense of dispossession engulfs her.  Kalpana tired with her own struggle and very much bitter at the meaningless publicity made out of her, firmly refuses Dipankar for an interview.  Disappointed, he returns to his own country without the story he wanted to do on Kalpana.  Kalpana lives an isolated life with an uncertain future.  She does small job with the support of local NGO and fights to safeguard her dignity from the lusts of society.  Kalpana’s life is limited to ceaseless follow up with the government officials for the death compensation package.

Dipankara makes a repeat visit without his camera unit and he just wants to meet Kalpana.  He is fascinated with her innocence, beauty and determination.  He shares with her lot of things about himself, about his people and the life out there in Dhaka.  He also learns about Kalpana and wants to put an end to her suffering.  It becomes difficult on Kalpana’s part to reconcile in settling down with a person she hardly knows.  Yet again, she is fed up with this place with bleak prospect.  The frustration of the never ending follow-up for compensation money, the anguish of being treated as women of loose virtue by village youths, and the illicit advances by Rupa’s husband makes Kalpana insecured.  Rupa, also believes how difficult it is for a single woman, especially a young and good looking widow to cope up with her present situation.  She insists upon Kalpana to settle down with Dipankar in Bangladesh — the land of her forefathers.

A brief stay in Kolkata prior to crossing the border becomes a revealing moment in Kalpana’s life.  She realises she will be a misfit in the far off urban setting of Dhaka and would suffer further alienation.  The memories of her homeland, of her childhood, of all that she had and all that she lost will haunt her for the rest of her life.  She longs to return to her village.  She also reconciles to the fact that Dipankar, in spite of all his concern and support, is just another ambitious journalist whose priority is his career.  He sees everything from the point of view of news and story.  Right now she is a hot subject for Dipankar, he can get lot of publicity if he marries her.

Ironically, the scene in her village is different-now.  The improper eviction process for the immigrants from Bangladesh is in full swing.  People are being served deportation notices to leave the country within thirty days.  There is a large scale resentment, unrest and protest-rally by the affected people to assert their rights and belongingness to the adopted homeland.  Kalpana unaware of the happenings returns to her home and to her own people.  Despite all the adversities, stigma and grim prospect she could breathe freely in the land which forever belongs to her.  But, as soon as she lands up in her village she is also served with the deportation notice.  Kalpana now determined to stay back, takes a surprise bold step to reclaim her rights to live in the land of her birth.  When she comes back to the village Kalpana realizes, quite conveniently, that Akshaya, the simple village guy who had saved her is the actual love of her life and implores him to ‘save’ her by marrying her.  This is a major letdown in her characterization and is quite a regressive moment; as a result it fails to alleviate her in the eyes of the viewers at the end.

==Cast== Oriya films; she portrays the mental agony of both a cyclone survivor and a Bangladeshi settler who has been served a notice to leave the country of her birth.  She worked hard on her look, walk, body language, dialogue delivery, and everything. She’s brilliant in every scene. 
*Bhaswati Basu as Rupa
*Rasesh Mohanty as Akshaya
*Bedaprasad Das as Deepankar
*Choudhury Bikash Das as Bikash
*Mihir Swain as the late husband of Kalpana
*Mamuni Mishra
*George Tiadi
*Surya Mohanty
*Ananta Mohapatra
*Choudhury Jayaprakash Das
*Bidyutprava Devi
*Chakradhara Jena

==Credits==
*Director – Himansu Khatua	 	
*Story – Himansu Khatua	 	
*Producer – Iti Samanta	
*Composer – Swarup Nayak	 	
*Cinematographer – Sameer Mahajan	 	
*Editor – Ravi Patnaik	 	
*Art – Amiya Maharana

==Awards==
Kathantara went on to win several awards at the 2006 Orissa State Film Awards, including the Best feature film of the year prize, and at the National Film Award for Best Feature Film in Oriya  

*Orissa State Film Awards
** Best feature film of the year 2006
** Best Director & Best Story - Himanshu Khatua
** Best Music Director - Swaroop Nayak
** Best Photography - Sameer Mahajan
** Best Audiography - Manas Chaudhry & Gitimugdha Sahani
** Best Supporting Actress - Bhaswati Basu
** Best Supporting Actor - Hadu
 National Film Award
** Best feature film in Oriya

==Critical reception==
Thus, Kathantara is brilliantly shot and passionately directed.  The real star of a film is it’s content, the script.  Kathantara is a perfect example of that, and also a perfect example of team work.  In today’s context, when we are familiar watching fast-paced films with sleek camera work and stylized editing, the film not only fulfills the above measures, but its narrative is also pretty superb.  Debaprasad Dash as a TV journalist from Bangladesh, Chaudhary Bikash Dash as an abusive husband, Rasesh Mohanty as the silent lover and Hadu, in a very small role of a street-smart villager excel in their respective roles.  Mihir Swain, Chaudhary Jay Prakash Dash, Mamuni Mishra did justice with their characters.  But the surprise packet is Bhaswati Basu as Rupa.  She’s simply superb.  Masterly editing by Ravi Shankar Pattnaik and breathtaking cimematography by Sameer Mahajan makes Kathantara a must watch film.  The film is rich in background score.  Audiography by Manas Choudhry and Gitimugdha Sahani is pretty good and art direction by Amiya Maharana is realistic.  Overall Kathantara, the first venture of Kadambinee Media, is a brilliant film. 

Kathantara seemingly looks like a very simple film but it does pack in a lot of issues and is multi-layered and rich in subtexts.  One just wished that its treatment also reflected the same richness, but unfortunately it does not.  The camera work of Sameer Mahajan does manage to capture the landscape of Orissa in all its expansive beauty.  One is particularly impressed by a sequence where in one long take the character of Rupa enters her house from outside at dusk; the camera which is placed at the door, pans left along with her as she enters her room, interacts with her husband who is lying on a bed in long shot, and he gets up and walks out of the house while the camera holds on to her in the foreground.  The outdoor and indoor matching of lights in the sequence is quite an impressive stroke and the mise-en-scene is quite noteworthy.  But otherwise, overall, the mise-en-scenes are too simplistic and the scenes too informative.  Specially the scenes inside the Kolkata hotel room; they are too rudimentary and bland, shot mostly in top angle mid-long shots.  In fact the entire film has a propensity for long takes and long and mid-long shots and is peculiarly averse to close-ups.  The ride from Howrah station to the hotel in a taxi (which does not have a meter!) fails to capture the essence of the big city; it is left to the lead characters to speak about its enormity and monotony instead.  At places the film is too loaded with dialogues, which are solely meant to convey information, quite often superfluous; and what makes matters worse is that they are stilted, specially the Bengali lines spoken by Dipankar.  Also, the actor playing Dipankar tries hard to deliver but falls far short of expectation.  With his full-sleeved dull coloured shirts tucked inside dark pleated pants, he looks like a stuffed bureaucrat and lacks the flamboyance or mannerisms of a television journalist.  In fact, he affects the entire look of the film.  The only saving grace are the two actresses even if Anu Choudhury in the role of Kalpana looks too urbane and her starched cotton printed sarees do not help matters in lending her character the required rural look that was so necessary for the film.  But her face has an elegance and sublime quality and her fine performance compensates for these shortcomings and one wishes there were at least some close-ups of her beautiful face. 

==Music==
Swarup Nayak has arranged the musical score and written the lyric for this film.

==References==
 

==External links==
*  
*  
*  
*   

 
 
 