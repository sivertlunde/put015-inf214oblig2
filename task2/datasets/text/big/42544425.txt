Pilot Premnath
 

{{Infobox film
| name           = Pilot Premnath  பைலட் பிரேம்நாத்
| image          = Pilot premnath.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = A. C. Tirulokchandar
| producer       = 
| writer         = 
| starring       = Sivaji Ganesan Malini Fonseka Thengai Srinivasan Major Sundararajan Sridevi
| music          = M. S. Viswanathan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Sri Lanka
| language       = Tamil
| budget         = 
| gross          = 
}}
Pilot Premnath is a 1978 Indian/Sri Lankan Tamil film directed by A. C. Thirulokachandar, and starring Sivaji Ganesan as the eponymous character. It was an "Indo-Sri Lankan joint-venture" produced by T. M. Menon and M. M. Saleem. http://www.thehindu.com/features/cinema/cinema-columns/pilot-premnath-1978/article5928817.ece 

==Plot==
Pilot Premnath (Sivaji Ganesan) has three children — two sons (Vijayakumar and JaiGanesh) and a blind daughter (Sridevi). He is closely attached to his children, especially his daughter. His wife (Malini) dies in an accident.

The deceased wife continues to live in Premnaths heart, and every time he returns from a flight journey, he goes to their room, and sees her in his mind’s eye. Once while talking about her in the room with his co-pilot (Thengai Srinivasan), he shows him the bundle of her letters. One letter is found, not posted. In it, the wife states that one of the three children was not born to her.
 stillborn child. Simultaneously, his friend Balu’s (Major Sundararajan) wife gives birth to a blind baby girl and to prevent tragedy in Premnath’s family, Balu substitutes his baby with Premnath’s.

The two sons find sweethearts (Jayachitra and Satyapriya) and the daughter falls in love with a young man (Prem Anand), who saves her from a thief near a shop. All get married and happiness is restored once more in the household.

==Cast==
* Sivaji Ganesan as Pilot Premnath
* Malini Fonseka as Premnaths wife
* Thengai Srinivasan as Premnaths co-pilot
* Major Sundararajan as Balu Vijayakumar and JaiGanesh as Premnaths sons
* Sridevi as Premnaths visually challenged daughter

==Production==
Pilot Premnath was entirely shot in Sri Lanka, while post production took place in Chennai. 

==Music==
The films music was composed by M. S. Viswanathan (assisted by Joseph Krishna), with lyrics written by Vaalee. 

==Reception==
Film historian Randor Guy praised the film for the "interesting storyline, the fine performances of Sivaji Ganesan, Malini Fonseca, Major Sundararajan, Sri Devi and others, the pleasing music, deft direction and impressive cinematography." 

==References==
 

 

 
 
 
 