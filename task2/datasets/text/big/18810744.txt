Emīla nedarbi
{{Infobox Film
| name           = Emīla nedarbi
| image          = 
| image_size     = 
| caption        = 
| director       = Varis Brasla
| producer       = 
| writer         = Alvis Lapiņš (writer) Astrid Lindgren (novel "Än lever Emil i Lönneberga")
| narrator       = 
| starring       = Māris Zonnenbergs-Zambergs Madara Dišlere Dace Eversa Uldis Dumpis Diāna Zande
| music          = Imants Kalniņš
| cinematography = Dāvis Sīmanis
| editing        = S. Šķila
| distributor    = Rīgas kinostudija 1985
| runtime        = 
| country        = Soviet Union
| language       = Latvian
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1985 Latvian TV-film about the Astrid Lindgren character, Emil i Lönneberga. It was produced by Rīgas kinostudija.

The film was awarded the Latvian National Film Prize Lielais Kristaps in 1985. 

==External links==
*   on the website of Latvian Television.  
* About the making of  , from www.filmas.lv  
*  

 
 
 
 
 
 
 
 
 