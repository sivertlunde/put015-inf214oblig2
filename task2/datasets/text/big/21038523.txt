Fast Food Fast Women
{{Infobox film
| name           = Fast Food Fast Women
| image          = Fast Food Fast Women (film).jpg
| image_size     = 
| caption        = 
| director       = Amos Kollek
| producer       = Hengameh Panahi
| writer         = Amos Kollek
| narrator       = 
| starring       = Anna Thomson
| music          = David Carbonara
| cinematography = Jean-Marc Fabre
| editing        = Sheri Bylander
| distributor    = 
| released       = 15 May 2000
| runtime        = 95 minutes
| country        = France
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Fast Food Fast Women is a 2000 American romantic comedy written and directed by Amos Kollek and financed by French and Italian production companies. The tag line for the film was "There are 18 million people in New York City, but only one like Bella." It was entered into the 2000 Cannes Film Festival.   

==Cast==
* Anna Thomson - Bella Jamie Harris - Bruno
* Louise Lasser - Emily
* Robert Modica - Paul
* Lonette McKee - Sherry-Lynn
* Victor Argo - Seymour
* Angelica Torn - Vitka
* Austin Pendleton - George
* Sandrine Holt - Giselle
* Valerie Geffner - Wanda
* Mark Margolis - Graham
* Judith Roberts - Bellas Mother
* Lynn Cohen - Jesse
* Salem Ludwig - Leo
* Irma St. Paul - Mary-Beth

== References ==
 

== External links ==
*  

 
 
 
 
 

 
 