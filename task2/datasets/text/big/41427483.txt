Puthiya Vaanam
{{Infobox film
| name           = Puthiya Vaanam
| image          = File:Puthiya Vaanam DVD cover.jpg
| image_size     =
| caption        = 
| director       = R. V. Udayakumar
| producer       = G. Thyagarajan V. Thamilazhagan
| writer         = R. V. Udayakumar (dialogues)
| screenplay     = R. V. Udayakumar R. M. Veerappan
| story          = Anil Sharma
| starring       =  
| music          = Hamsalekha
| cinematography = Ravi Yadav
| editing        = K. R. Krishnan
| distributor    =
| studio         = Sathya Movies
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1988 Tamil Tamil crime Rupini and Gouthami in lead roles. The film, produced by G. Thyagarajan and V. Thamilazhagan, had musical score by Hamsalekha and was released on 16 June 1988. The film was named after a song from the film Anbe Vaa (1966).   The film was a remake of Hindi film Hukumat. 

==Plot==

In the past, Jeevadurai (Jaiganesh), an honest police officer, was killed by Govindan (Vijayakumar (actor)|Vijayakumar). Thereafter, Rajaratnam (Sathyaraj), Jeevadurais son, was brought by D.I.G Pandidurai (Sivaji Ganesan). Few years later, Rajaratnam is married to Devaki (Rupini (actress)|Rupini) and they have a son. Like his father, Rajaratnam becomes a police officer. In Shanthi Nagar, Govindan who is now known as Kocha controls the city and the people cannot face him and his henchmen. His adopted father Pandidurai transfers Rajaratnam to Shanthi Nagar.

==Cast==

*Sathyaraj as M. G. Rajaratnam
*Sivaji Ganesan as D.I.G Pandidurai Rupini as Devaki
*Gouthami as Mary Vijayakumar as Kocha / Govindan Janagaraj as Pattabi
*Rajesh Kumar as Joni, Kochas son
*Nassar
*Charle
*Jaiganesh as Jeevadurai (guest appearance)
*K. Natraj Ponnambalam
*Oru Viral Krishna Rao
*Idichapuli Selvaraj
*Gundu Kalyanam
*Ennatha Kannaiya
*LIC Narasimhan as Samuel
*Sethu Vinayagam
*Baby Sujitha Kuyili
*Disco Shanti
*Anuja

==Soundtrack==

{{Infobox album |  
| Name        = Puthiya Vaanam
| Type        = soundtrack
| Artist      = Hamsalekha
| Cover       = 
| Released    = 1988
| Recorded    = 1988 Feature film soundtrack |
| Length      = 
| Label       = 
| Producer    = Hamsalekha
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Hamsalekha. The soundtrack, released in 1988, features 5 tracks with lyrics written by Muthulingam, Gangai Amaran, Ilandevan and R. V. Udayakumar.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Manidha Innum || S. P. Balasubrahmanyam || 2:49
|- 2 || Mynaa Mynaa || S. P. Balasubrahmanyam, S. P. Sailaja, Sunanda || 4:39
|- 3 || Oore Kedithavane || S. P. Balasubrahmanyam, S. P. Sailaja || 4:08
|- 4 || Oru Paadal Solgiren || S. P. Balasubrahmanyam, Malaysia Vasudevan || 4:36
|- 5 || Raakuyile || S. P. Balasubrahmanyam, K. S. Chithra || 4:58
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 