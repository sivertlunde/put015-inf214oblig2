The Perils of Pauline (1914 serial)
 
{{Infobox film
| name           = The Perils of Pauline
| image          = Perilsofpauline.jpg
| caption        = Film poster
| director       = Louis J. Gasnier Donald MacKenzie
| producer       =
| writer         = Charles W. Goddard George B. Seitz
| starring       =
| cinematography = Arthur C. Miller
| editing        =
| distributor    = General Film Company & Eclectic Film Company
| released       =  
| runtime        = 20 chapters (total of 410 minutes)
| country        = United States
| language       = Silent with English intertitles
| budget         =
| gross          =
}}
 some analyses hold that her character was more resourceful and less helpless than the classic damsel stereotype. {{Cite book first = Ben last = Singer editor = Richard Abel chapter = Female Power in the Serial-Queen Melodrama: The Etiology of An Anomaly title =  Silent Film publisher = Continuum International Publishing Group - Athlone date = February 1999 pages = 168–177 isbn = 0-485-30076-1
}} 

Pauline is menaced by assorted villains, including pirates and Indians. Neither Pauline nor its successor, The Exploits of Elaine, used the so-called "cliffhanger" format in which a serial episode ends with an unresolved danger that is addressed at the beginning of the next installment.  Although each episode placed Pauline in a situation that looked sure to result in her imminent death, the end of each installment showed how she was rescued or otherwise escaped the danger. Despite popular associations, Pauline was never tied to railroad tracks in the series, an image that comes instead from contemporary films such as Barney Oldfields Race for a Life.

The serial had 20 episodes, the first being three reels (30 minutes), and the rest two reels (20 minutes) each. After the original run, it was reshown in theaters a number of times, sometimes in edited, shortened versions, through the 1920s. Today, The Perils of Pauline is known to exist only in a shortened 9-chapter version (approximately 214 minutes), released in Europe in 1916.
 {{Cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | year = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 8
 | chapter = 1. The Girls "Who Is That Girl in the Buzz Saw?"
}} 

In 2008 The Perils of Pauline was selected by the Library of Congress for preservation in the United States National Film Registry, as being "culturally, historically, or aesthetically significant."

==Cast==
* Pearl White as Pauline
* Crane Wilbur as Harry Marvin
* Paul Panzer as Koerner / Raymond Owen
* Edward José as Sanford Marvin
* Francis Carlyle as Owens Henchman, Hicks
* Clifford Bruce as Gypsy Leader
* Donald MacKenzie as Blinky Bill
* Jack Standing as Ensign Summers
* Eleanor Woodruff as Lucille

==Plot==
The premise of the story was that Paulines wealthy guardian Mr. Marvin, upon his death, has left her inheritance in the care of his secretary, Mr. Koerner, until the time of her marriage. Pauline wants to wait a while before marrying, as her dream is to go out and have adventures to prepare herself for becoming an author. Mr. Koerner, hoping to ultimately keep the money for himself, tries to turn Paulines various adventures against her and have her "disappear" to his own advantage.

==Behind the scenes==
 
Pearl White was hesitant to accept the title role, but signed up for $250/week and lots of publicity. {{Cite book
 | last = Lahue
 | first = Kalten C.
 | title = Continued Next Week
 | publisher =
 | isbn =
 | pages = 8–10
 | chapter = 1. A Bolt From The Blue
 }} 

William Randolph Hearst was involved in plot development.  He was also present at the premiere at Loews Broadway Theatre, on 23 March 1914.  According to "The Truth About Pearl White" by Wallace E. Davis, the general release was approximately 1 April 1914. 

E. A. McManus, head of the Hearst-Vitagraph service organization, was the person who proved how successful a serial could be.  He co-operated with the largest film equipment and production company in the world at that time, a France-based company named Pathé, to produce this serial, which was Pathés first entry into the medium.  {{Cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | pages = 11–14
 | chapter = 1. Drama by Instalment
}}   George B. Seitz tried to follow the cliffhanging pattern of The Adventures of Kathlyn but each chapter was mostly self-contained. 

After retiring from law enforcement, former FBI Director William J. Flynn, through his acquaintance with the actor King Baggot (who in 1917 was considered the greatest film star in the country), became a scenario writer for the motion picture industry; producers Theodore and Leopold Wharton commissioned him to write story lines for their films. The Whartons also adapted Flynns experiences into a 20-part spy thriller titled The Eagles Eye (1918), starring Baggot. 

Surviving chapters of Pauline are noteworthy for their unintentionally funny title cards and dialogue captions, filled with misspellings, poor punctuation, terrible grammar, and odd expressions. This happened when Pathé, the theatrical distributor, exported the film to  .
 
Much of the film was shot in Fort Lee, New Jersey when many early film studios in Americas first motion picture industry were based there at the beginning of the 20th century.    Scenes were also filmed in Pittsburgh, Pennsylvania.  The term "cliffhanger" may have originated with the series, owing to a number of episodes filmed on or around the New Jersey Palisades—though it is also likely to refer to situations in stories of this type where the hero or heroine is hanging from a cliff, seemingly with no way out, until the next episode or last-minute resolution.

Pearl White performed her own stunts for the serial.  Considerable risk was involved.  In one incident, a balloon carrying White escaped and carried her across the Hudson River into a storm, before landing miles away. In another incident her back was permanently injured in a fall. 

One of the more famous scenes in the serial was filmed on the curved Ingham Run trestle in New Hope, Pennsylvania on the Reading Companys New Hope Branch (now the New Hope and Ivyland Railroad line). The trestle still stands, just off Ferry Street, and is now referred to as "Paulines Trestle". The railroad is a tourist attraction and offers rides from New Hope to Lahaska, Pennsylvania, crossing over the original trestle. {{Cite web url = http://www.newhoperailroad.com title = New Hope & Ivyland Railroad publisher = newhoperailroad.com accessdate = 2008-06-20}} 

Milton Berle (1908-2002) claimed The Perils of Pauline as his first film appearance, playing the character of a young boy, though this has never been independently verified.  The serial did mark one of the early credits for the cinematographer Arthur C. Miller, who was transferred to the project from the Pathé News department.

Pathé established an American factory and studio facility in Jersey City, New Jersey in 1910, and also established the Eclectic Film Company as a subsidiary distribution company for both its American and European products.  Although the Jersey City plant produced moderately popular comedies, dramas, and newsreels largely directed at the US market, Pauline was the first American-made Pathé effort to achieve worldwide success under the Eclectic banner.

The final peril has Pauline sitting in a target boat as the Navy opens fire.  The idea was also used in To the Shores of Tripoli (1942, Fox). 

==Sequels and remakes==
 
 parodies followed, heralding the first golden age of the American film serial.
 1933 sound The Perils updated comedy.

An abortive mixed-media musical was planned to be based on the film, called Whos That Girl?, meant to premiered by The Boys From Syracuse producer Richard York on Broadway in 1970, with a book written by Lewis Banchi and Milburn Smith, and with the planned participation of the songwriting duo Ray Evans and Jay Livingston. 

==Legacy==
The Perils of Pauline is the prime example of what author Ben Singer has called the "serial-queen melodrama,"  noted as a typification of damsel in distress cinema, as well as for its extensive use of the cliffhanger technique in film serials.

There has been a recent reassessment of Singers model in the light of broader film forms. 
 women film audiences.

The films style was later subject to nostalgic caricature in many forms (e.g. Dudley Do-Right), but the original heroine was neither as helpless as the caricatures, nor did the original film include the much-parodied "tied to railroad tracks" or "tied to buzzsaw" scenarios which appeared in later films in this vein. Even the title phrase "Perils of" was often adopted by later serials, for example, in Universals Perils of the Secret Service, Perils of the Wild, and Perils of the Yukon.

==See also==
* List of film serials
* List of film serials by studio
* List of incomplete or partially lost films

==References==
 

==External links==
 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 