Docking the Boat
{{Infobox Film
| name           = Docking the Boat
| image          = Docking the Boat.jpg
| caption        = Swedish poster.
| director       = Tage Danielsson
| producer       = Lars-Owe Carlberg
| writer         = Hans Alfredson Tage Danielsson
| starring       = Monica Zetterlund Lars Ekborg Birgitta Andersson Gösta Ekman
| music          = Lars Färnlöf Charles Redland
| cinematography = Martin Bodin
| editing        = Carl-Olov Skeppstedt
| distributor    = 
| released       = 
| runtime        = 102 min.
| rating         =
| country        = Sweden
| awards         =
| language       = Swedish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Swedish dark comedy film from 1965 directed by Tage Danielsson. The film stars Gösta Ekman, Monica Zetterlund, Hans Alfredson, Lars Ekborg and Birgitta Andersson have acquired a mild cult following in Sweden for their acting in this film. Monica Zetterlund also performed the theme song.

==Plot== eat crayfish and drink snaps, a quintessentially Swedish tradition. Some are already in the house on the island (with the food) and the rest of the group arrives by boat (bringing the snaps), but they experience great difficulties while trying to come ashore.  Their only neighbor on the island, an eccentric, Hollywood-obsessed, hot-tempered hermit doesnt make the situation better.

==Cast==
* Monica Zetterlund as Berit
* Lars Ekborg as Kalle
* Birgitta Andersson as Mona
* Gösta Ekman as Lennart
* Katie Rolfsen as Inez
* Hans Alfredson as Garbo
* Hatte Furuhagen as Walter
* Tage Danielsson as Olsson Jim Hughes as Man finding message in bottle

==External links==
* 
 

 
 
 
 
 
 
 


 
 