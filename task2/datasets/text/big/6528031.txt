Princesa (2001 film)
 
{{Infobox film
| name           = Princesa
| image          = Princesa DVD cover.jpg
| image_size     = 
| caption        = 
| director       = Henrique Goldman
| producer       = Rebecca OBrien
| writer         = Ellis Freeman Henrique Goldman
| narrator       = 
| starring       = Ingrid de Souza Cesare Bocci
| music          = Giovanni Venosta
| cinematography = Guillermo Escalon
| editing        = Kerry Kohler
| distributor    = BIM Distribuzione
| released       =      
| runtime        = 96 minutes
| country        = Italy Spain
| language       = Italian Portuguese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2001 film directed by Henrique Goldman that tells the story of a transsexual, Fernanda, who travels to Milan and works as a prostitute to finance her sex-change surgery. The film was inspired by a book of the same name written by Maurizio Janelli and Fernanda Farias de Albuquerque.

== Cast ==
* Ingrid de Souza as Fernanda
* Cesare Bocci as Gianni
* Lulu Pecorari as Karin
* Mauro Pirovano as Fabrizio
* Biba Lerhue as Charlo
* Sonia Morgan as Fofao
* Alessandra Acciai as Lidia
* Egidio Cardillo as Cliente

== External links ==
*  
*  
*  
*  
*   at Parallax Pictures

=== Reviews ===
*   at the BBC
*   at Film4
*   at The New York Times
*   at OffOffOff Film
*   at PlanetOut
*   at Total Film

 
 
 
 
 
 
 


 