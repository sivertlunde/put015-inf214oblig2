First Flight (film)
{{Infobox film
| name           = First Flight
| image          = First Flight (film) poster.jpg
| caption        =
| border         = yes
| director       = Cameron Hood Kyle Jefferson
| producer       = Maryann Garger Pilar Flynn
| writer         = Cameron Hood Kyle Jefferson
| starring       = Jeanine Meyers Jon Spinogatti James Dooley
| editing        = Marcus Taylor
| studio         = DreamWorks Animation
| distributor    = Paramount Pictures   20th Century Fox  
| released       =   |2013|01|29|DVD}}
| runtime        = 8 minutes
| country        = United States English
}}
 2006 computer-animated short film produced by DreamWorks Animation. It was written and directed by Cameron Hood and Kyle Jefferson.
 Over the Hedge. 

== Plot ==
The film tells the story of a fastidiously organized businessman, whose perspective on life is forever changed through an unexpected encounter with a tiny fledgling bird. 

==Cast==
* Jeanine Meyers as Bird
* Jon Spinogatti as Swift/Bus Driver

==Home media== Over the Hedge short film. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 