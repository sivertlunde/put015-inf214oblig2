The Frozen Ground
{{Infobox film
| name           = The Frozen Ground
| image          = The Frozen Ground poster.jpg
| alt            = Two men look in a common direction while the movies title runs through the middle of this poster
| caption        = Poster for The Frozen Ground
| director       = Scott Walker Jane Fleming
| writer         = Scott Walker
| starring       = Nicolas Cage John Cusack Vanessa Hudgens
| music          = Lorne Balfe 
| cinematography = Patrick Murguia
| editing        = 
| studio         = Grindstone Entertainment Group Cheetah Vision Court Five Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = Lionsgate
| released       =  
| runtime        = 105 minutes  
| country        = United States
| language       = English
| budget       =$19.2 million 
| gross          = $5,496,951 
 
}} mystery Thriller thriller film first feature film, based on the real-life 1980s Alaskan hunt for serial killer Robert Hansen. Hansen stalked and murdered between 17 and 21 young women, kidnapping them and taking them out to the Alaskan wilderness where he shot and buried them.    The film stars Nicolas Cage, John Cusack, Vanessa Hudgens, Katherine LaNasa, Radha Mitchell and 50 Cent. The film was released in theaters and on demand on August 23, 2013. 

==Plot==
The film opens in an Anchorage motel room in 1983, where 17-year-old Cindy Paulson (Vanessa Hudgens) is handcuffed and screaming for help. She is rescued by an Anchorage Police Department patrol officer. He takes Paulson to the hospital, and her clothes are kept for a rape kit. At an APD station, she explains to detectives that she was abducted and repeatedly raped. Because she is a prostitute and lying about her age, the detectives disbelieve her story, refusing to even look into the man she named as her abductor, Robert Hansen (John Cusack). They claim Hansen is an upstanding member of society and a family man who owns his own restaurant, three people have alibied him, and there are too many conflicting details in Paulsons story to investigate him.

The APD patrol officer who rescued Paulson is outraged that the detectives refuse to pursue Hansen. He surreptitiously photocopies information about the case and sends it to the Alaska State Police. Meanwhile, state trooper Jack Halcombe (Nicolas Cage) has been called to investigate a female body that was found in the bush, half eaten by bears. The police connect the case to other missing girls, who have disappeared after going to what they thought were legitimate photo shoots. With the secret information from the APD officer, Halcombe connects the other cases to Paulsons and starts to put together a portrait of Hansen. Paulson details how Hansen kept her captive, and that she escaped from his car when he tried to transfer her to his bush plane.

Meanwhile in Anchorage, Debbie Peters gets picked up by a man in an RV for a photo shoot. Later, Hansen eats a quiet dinner at home. His wife and children are away, and Hansen relaxes in his trophy room, casually ignoring Debbie who is chained to a post. She has urinated on the floor, and as she cleans up the mess with a towel, Hansens neighbor enters the house, trying to deliver a plate of food. Hansen warns Debbie not to scream and leaves the trophy room to greet his neighbor. Hansen then takes Debbie to the airport, where he orders her into his plane. After landing in a remote spot in the bush, Hansen frees Debbie, letting her run in a panic through the woods for a while before he shoots her with a .223 caliber Ruger Mini-14|rifle. He steals her necklace before finishing her off with a handgun.

Halcombe has a very difficult time assembling a case against Hansen, despite the mountain of evidence against him. Hansen also has a history of criminal and psychological problems. Because all of the evidence is circumstantial and Paulson is afraid of testifying, the District attorney refuses to issue a search warrant. Paulson keeps falling back into the world of stripping and prostitution, despite Halcombes efforts to keep her safe. At a strip club, while she is trying to sell lap dances, she notices Hansen who is trolling for a new victim. Paulson barely escapes from him. The encounter makes Hansen nervous, and he hires Carl Galenski to find and kill Paulson. Carl approaches Paulsons erstwhile pimp Clate Johnson (50 Cent) and offers to forgive his sizable debt if Clate turns Paulson over to him.

Halcombe stakes out Hansens house, causing Hansen to panic. Hansen gathers up evidence of his crimes, including the keepsakes from his victims, and flees with his son to the airport. He flies his plane to the bush and hides his keepsakes. Feeling that the chance to catch Hansen is slipping away, and with the victim count now at 17 girls, Halcombe forces the DA to issue a warrant. The search of Hansens house yields no evidence, not even in his trophy room. Hansen agrees to be interrogated without a lawyer, but he is not yielding any new evidence. Halcombe arrests Hansen, but unless the police find new evidence, they will be unable to hold him.

Halcombe orders a second search of Hansens house, which turns up a hidden cache of guns, including the .223 caliber rifle used in many of the murders. Under police watch at a safe location, Paulson slips out and returns to her life of prostitution. Clate picks her up and delivers her to Carl. When Clate attempts to rob Carl, Paulson uses the opportunity to escape, with Carl in pursuit. After making a call to Halcombe, Paulson is almost killed by Carl, but Halcombe rescues her just in time.

Halcombe uses a copy of a bracelet that one of the victims wore to bluff Hansen into thinking the police have found where he hid the evidence in the bush. The bracelet, combined with the sight of Paulson in the interrogation room, enrages Hansen to the point where he incriminates himself. The film ends with actual pictures of Hansens victims.

==Cast==
* Nicolas Cage – Jack Halcombe
* John Cusack – Robert Hansen
* Vanessa Hudgens – Cindy Paulson
* 50 Cent – Pimp Clate Johnson
* Radha Mitchell – Allie Halcombe
* Jodi Lyn OKeefe – Chelle Ringell
* Dean Norris – Sgt. Lyle Haugsven
* Katherine LaNasa – Fran Hansen
* Matt Gerald – Ed Stauber
* Robert Forgit – Sgt. Wayne Von Clasen
* Ryan ONan – Gregg Baker
* Kurt Fuller – D.A. Pat Clives
* Kevin Dunn – Lt. Bob Jent
* Mark Smith – Head of Security
* Gia Mantegna – Debbie Peters
* Michael McGrady – Vice Det. John Gentile
* Brad William Henke – Carl Galenski
* Bostin Christopher – Al
* Taylor Tracy – Sandy Halcombe
* Ron Holmstrom – Attorney Mike Rule

==Production==
 
The film was shot in 26 days, entirely in Alaska. Writer/director Scott Walker delayed shooting for five months so he could shoot the film on the cusp of fall into winter, so he could achieve a look and feel of the film starting with no snow and ending in the deep of winter. He has said he literally wanted the feel of the weather closing in and around the story, and freezing the case. As a result of shooting at this time of year, by the end of 26 days filming there were 3 1/2 hours less daylight per day than when filming began.

== Reception == 
The movie has received mixed reviews from critics. It currently holds a 60% rating on review site   assigns the film an average score of 37 out of 100 based on 16 reviews from mainstream critics, indicating "generally unfavorable reviews". 
 weighted average of 8/10.  Netflix audiences give the film 3.8 stars based on 1,152,400 reviews. 

==References==

 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 