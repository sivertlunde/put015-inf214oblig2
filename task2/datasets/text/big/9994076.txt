Juan Lamaglia y señora
{{Infobox film
| name           =Juan Lamaglia y señora
| image          =
| image_size     =
| caption        =
| director       =Raúl de la Torre
| producer       =
| writer         =Héctor Grossi
                Raúl de la Torre
| narrator       =
| starring       =Pepe Soriano|José Soriano
                  Julia von Grolman
| music          =Roberto Lar
| cinematography =Juan José Stagnaro
| editor         =Óscar Souto
                  Sergio Zottola 
| distributor    =
| released       = 1970
| runtime        =88 mins
| country        = Argentina Spanish
| budget         =
}}
 1970 Argentina|Argentine film written by Héctor Grossi and Raúl de la Torre and directed by Raúl de la Torre. 

It was released in Argentina on April 28, 1970.

Raúl de la Torre was awarded the Special Jury Award at the 1970 Mar del Plata Film Festival and the film itself won the Silver Condor for Best Film (Mejor Película) at the 1971 Argentine Film Critics Association Awards.

==Cast==
* Pepe Soriano|José Soriano as Juan Lamaglia
* Julia von Grolman as Señora Lamaglia

==External links==
*  

==References==
 
 
 
 
 
 
 


 