Red Blossoms
 
{{Infobox film
| name           = Azahares rojos
| image          =
| image_size     =
| caption        =
| director       = Edmo Cominetti
| producer       =
| writer         = Edmo Cominetti
| narrator       =
| starring       = Mecha Caus  Antuco Telesca
| music          =
| cinematography =
| editing        =
| distributor    = 1940
| runtime        =
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} 1940 cinema Argentine comedy film directed by and written by Edmo Cominetti. The film starred Mecha Caus and Antuco Telesca.

==Cast==
*Vicente Álvarez
*Mecha Cobos
*Justo Garaballo
*Elisa Labardén
*Mecha López
*Juan José Piñeiro
*Juan Siches de Alarcón
*Enrique Vico Carré

==Release==
The film premiered in Argentina in October 1940.

==External links==
* 

 
 
 
 
 

 
 