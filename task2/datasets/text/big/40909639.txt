Phantom (2015 film)
{{infobox film
| name           = Phantom
| image          = 
| alt            =
| caption        =  Kabir Khan Kabir Khan
| producer       = Sajid Nadiadwala
| writer         = Kabir Khan  Mohd.Salik
| starring       = Saif Ali Khan Katrina Kaif
| music          = Pritam Chakraborty
| cinematography = Aseem Mishra
| editing        = Aarif Sheikh Nadiawala Grandson Entertainment
| distributor    = UTV Motion Pictures
| released       = Scheduled for  
| runtime        = 
| country        = India
| language       = Hindi
| gross          =
}} Kabir Khan, and produced by Sajid Nadiadwala. It stars Saif Ali Khan and Katrina Kaif in lead roles. The film is about the post-26/11 attacks in Mumbai and global terrorism. The film was originally announced as having an April release, but was rescheduled for August 2015. 

==Cast==
* Saif Ali Khan
* Katrina Kaif

==Production==
 Kurdish for his role.   

Filming began on October 2013 in Beirut, Lebanon. Due to the countrys recent political turmoil, security was provided to the crew.  A few days into shooting, an accident occurred on the set where two local artists were injured after being hit by a car.  Other international locations such as Turkey are included on the filming plan.  The shooting was also done in Gulmarg in Jammu and Kashmir state (India) in January 2014.   Shooting of film is also done in Malerkotla.  

==References==
 

==External links==
*  

 
 
 
 
 

 