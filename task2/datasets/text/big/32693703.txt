Rush It
{{Infobox film
| name           = Rush It
| image          = Rush It film.jpg
| image_size     = 
| alt            = 
| caption        = VHS cover
| director       = Gary Youngman
| producer       = Robby Kenner Bob Wunderlich  
| writer         = Barbara Brenner Gary Youngman
| story          = 
| narrator       = 
| starring       = Tom Berenger Jill Eikenberry
| music          = Buzzy Linhart
| cinematography = Don Lenzer
| editing        = 
| studio         = Unicorn Pictures
| distributor    = 
| released       = 
| runtime        = 78 minutes
| country        =  
| language       = English
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} 1977 film co-written and directed by Gary Youngman. The directorial debut film also featured the first acting role in a film by Tom Berenger. 

==Plot==
A female bicycle messenger is looking for love in New York City.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Tom Berenger || Richard Moore
|-
| Jill Eikenberry || Merrill
|- John Heard || Byron
|-
| Martin Harvey Friedberg || Ernie 
|-
| Harriet Hall || Harriet  
|-
| Christina Pickles || Eve
|-
| Judith Kahan || Catherine
|}

==See also==
* Quicksilver (film)|Quicksilver

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 


 