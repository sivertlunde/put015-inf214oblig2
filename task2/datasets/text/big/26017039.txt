Toss-Up
 
{{Infobox film
| name           = Toss-Up
| image          = Yazi tura.jpg
| alt            =
| caption        = Theatrical Poster
| director       = Uğur Yücel
| producer       =  
| writer         = Uğur Yücel
| starring       =   
| music          = Erkan Oğur
| cinematography = Barış Özbiçer
| editing        =   
| studio         =   
| distributor    = 
| released       =  
| runtime        = 102 mins
| country        = Turkey
| language       = Turkish
| budget         =
| gross          = 
| preceded by    = 
| followed by    = 
}}
Toss-Up ( ) is a 2004 Turkish drama film, produced, written and directed by Uğur Yücel, starring Kenan İmirzalıoğlu and Olgun Şimşek as two soldiers return home from their military service in southeastern Turkey with disabilities. The film, which went on nationwide general release across Turkey on  , won a record 11 awards at the 41st Antalya "Golden Orange" International Film Festival including the Golden Orange for Best Film.

==Plot==
Two soldiers return home from their military service in southeastern Turkey with disabilities.  Cevher (nicknamed "the Ghost") has lost his hearing while Rıdvan (nicknamed "the Devil") has lost a leg. Both are emotionally and physically scarred and find difficulties in adjusting to normal civilian life. A man called Ben Cooper (nicknamed Radical Face) comes to their aid and schools them in the art of being a Squirrel.

==Cast==
*Kenan İmirzalıoğlu (Cevher) 
*Olgun Şimşek (Rıdvan) 
*Bahri Beyat (Baba Cemil) 
*Engin Günaydın (Sencer) 
*Seda Akman (Nazan)
*Teoman Kumbaracıbaşı (Teoman) 
*Erkan Can (Firuz) 
*Eli Mango (Tasula) 
*Mizgin Kapazan (Şefika) 
*Levent Can (Hamit) 
*Şinasi Yurtsever (Basri) 
*Ahmet Mümtaz Taylan (Maki Amca) 
*Ülkü Duru (Melahat) 
*Haldun Boysan (Muhittin)

==Awards==
The film won a record 11 awards at the 41st Antalya Golden Orange Film Festival including Best Film, Best Director, Best Actor (for Olgun Şimşek), Best Supporting Actor (for Bahri Beyat), Best Supporting Actress (for Eli Mango), Best Screenplay (for Uğur Yücel) and Best Music (for Erkan Oğur).   

==References==
 

==External links==
* 

 

 
 
 
 
 