Zohi Sdom
{{Infobox film
| name           = Zohi Sdom
| image          = 
| caption        = 
| director       = Muli Segev and Adam Sanderson
| producer       = Moshe Edri Leon Edri Avi Nir David Zilber Muli Segev Avi Nesher
| writer         = Muli Segev Asaf Shalmon David Lipschitz
| starring       = Eli Finish Dov Navon Assi Cohen
| music          = Ran Shem Tov
| cinematography = Giora Bih
| editing        = Ron Omer Yaron Shilon
| distributor    = 
| released       =  
| runtime        =
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}} comedy feature film released on August 5, 2010. It was conceived and created by the team behind the television programme, Eretz Nehederet, and directed by Muli Segev and Adam Sanderson. According to official figures, the movie was the most-watched Israeli film in the 25 years preceding it, with over half a million local tickets sold. 
 Bera the king of Sodom.

==Plot== Lot (Dov Raphael (Yuval Michael (Maor Cohen).
 yes men, who has two sons—the elder and successful Ninveh (Assi Cohen) and the younger and stupid Liam (Mariano Idelman).

When Bera accidentally finds out that Sodom is about to be destroyed and Lot saved, he concocts a plan to switch places with Lot, as the archangels set to destroy the city had never actually seen Lot. He sends his elder son to seduce Charlotte, but to his chagrin his son falls in love. Bera tells Lot that he is ill and needs to get away from his usual surroundings, so they change places. As Lot takes the role of king, he becomes conceited and forgets his plans to help the children of Sodom.

Bera finds out that Ninveh plans to betray him for the sake of Charlotte, and has Ninveh imprisoned. He instead decides to wed his younger son Liam to Charlotte. Ninveh escapes and crashes the wedding, telling the guests that there is no food, so they start to riot. He tells everyone that Sodom is about to be destroyed, and Lot and Bera make their escape with their families. On their way out they are met by Raphael and Michael, who would only let Lot pass, but both Lot and Bera pretend to be Lot. Lots wife betrays her husband and supports Bera. The angels propose splitting Lots wife in two as in Judgment of Solomon|Solomons Judgement, but even though Lot cares more about her, they assume that Bera is in fact Lot, and let his escape with Lots wife.

Sodom is then blown up with dynamite, which Abraham witnesses and signs the contract with God. However, while it makes a big blast, no damage is done, and God tells his angels that it was a ploy to swindle Abraham into signing the contract. Bera brings Abraham the money from the wedding, and the latter accepts him into the family, while Lot remains to rule Sodom. It is shown that Sodom later moves to a more lucrative real estate location—Tel Aviv.

==Cast==
{| class="wikitable"
|-
!Character!!Actor
|- Bera (Bible)|Bera, the king of Sodom Eli Finish
|- Lot (biblical Lot
|Dov Navon
|-
|Ninveh, Beras eldest son Assi Cohen
|-
|Liam, Beras younger son Mariano Idelman
|-
|Lots wife Tal Friedman
|-
|Charlotte, Lots daughter Alma Zack
|- Raphael (archangel)|Archangel Raphael Yuval Semo
|- Michael (archangel)|Archangel Michael Maor Cohen
|- God
|Eyal Kitzis
|- Abraham
|Moti Kirschenbaum
|-
|Isaac, Ishmael Mariano Idelman
|- Hagar
|Orna Banai
|}

==Reception== Keshet TV.  However, Pnai Plus praised it as one of the most well-planned Israeli comedies in recent decades, with a good humoristic flow and a high concentration of good jokes. 

==References==
{{reflist|refs=
   
   
   
}}

==External links==
* 

 
 
 
 