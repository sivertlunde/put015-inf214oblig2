Spider's Web (film)
 
{{Infobox film
| name           = Spiders Web
| image          = 
| image_size     = 
| caption        = 
| director       = Bernhard Wicki
| producer       = Peter Hahne
| writer         = Wolfgang Kirchner Joseph Roth Bernhard Wicki
| narrator       = 
| starring       = Ulrich Mühe
| music          = 
| cinematography = Gérard Vandenberg
| editing        = Tanja Schmidbauer
| distributor    = 
| released       = 21 September 1989
| runtime        = 196 minutes
| country        = Germany
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1989 West West German official submission Best Foreign Language Film, but did not manage to receive a nomination.  The film was the last ever submission by West Germany, due to German reunification in 1990, Germany competed at the 63rd Academy Awards as a single country.

The film was also entered into the 1989 Cannes Film Festival.   

== Plot ==

The film centers on young right-wing Leutnant (lieutenant) Theodor Lohse ( , joining an organization called "S II" (probably based on real-life Organisation Consul that was responsible for a number of political and anti-Semitic assassinations) where his immediate superior is Baron von Rastchuk (Armin Mueller-Stahl). Baron von Rastchuk brings Lohse in contact with former Crown Prince Heinrich in order to get Lohse employed, a favor for which the homosexual Prince demands one-time bodily obligingness from Lohse. In spite of his apparent shock and disgust, Lohse yields to the Prince out of his opportunism and willingness to please his superiors.

Lohse becomes a full-time spy for the organization, and with unprecedented, relentless opportunism and unscrupulousness he spies in on Communist plots, partakes in the organizations plans to undermine the new German democracy, and disposes of his own right-wing colleagues when he sees fit, all of which to serve his own plans of rising to the top within right-wing circles. During these activities he comes in contact with Benjamin Lenz (Klaus Maria Brandauer), a Jewish man dealing in informations on all kinds of criminal and underground political proceedings who will always sell at the highest price, be it paid by left or right-wing conspirators or the police. In spite of Lohses hatred of Jews, he finds Lenzs services useful, but soon finds himself at his mercy as Lenz through their collaboration finds out more and more about Lohses schemes and spy activities.
 putsch in Munich to count on - a man named Adolf Hitler.

==Cast==
* Ulrich Mühe - Theodor Lohse
* Klaus Maria Brandauer - Benjamin Lenz
* Armin Mueller-Stahl - Baron von Rastchuk
* Andrea Jonasson - Rahel Efrussi
* Corinna Kirchhoff - Else von Schlieffen
* Elisabeth Endriss - Anna Ullrich Haupt - Baron von Köckwitz
* Agnes Fink - Mutter Lohse
* András Fricsay Kali Son - Klitsche
* Ernst Stötzner - Günter
* Peter Roggisch - Prinz Heinrich
* Rolf Henniger - Aaron Efrussi
* Hans Korte - Geheimrat Hugenberg
* Kyra Mladeck - Frau von Köckwitz
* Hark Bohm - Dada-Künstler

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 