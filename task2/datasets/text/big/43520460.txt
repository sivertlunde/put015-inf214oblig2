Ambushed (film)
{{Infobox film
| name           = Ambushed
| image          = Ambushed.2013.MoviePoster.png
| alt            =
| caption        = Film poster
| director       = Giorgio Serafini
| producer       = {{plainlist|
* Agustin
* Gianni Capaldi
* Randy Couture
* Phillip B. Goldfine
* Dolph Lundgren
}}
| writer         = Agustin
| starring       = {{plainlist|
* Dolph Lundgren
* Vinnie Jones
* Randy Couture
}}
| music          = {{plainlist|
* Brian Jackson Harris
* Justin Raines
* Michael Wickstrom
}}
| cinematography = Marco Cappetta
| editing        = Scott Evans
| studio         = {{plainlist|
* Creamovies
* Hollywood Media Bridge
}}
| distributor    = {{plainlist|
* Voltage Pictures
* Anchor Bay Films
}}
| released       =   }}
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Ambushed (also released as Hard Rush) is a 2013 action-thriller film directed by Giorgio Serafini, written by Agustin, and starring Dolph Lundgren, Vinnie Jones, Randy Couture, Gianni Capaldi, and Daniel Bonjour.  Lundgren, Jones, and Capaldi would go on to work with Serafini in Blood of Redemption and Puncture Wounds.

== Plot ==
Mid-level drug dealers Eddie and Frank grow restless with their limited growth potential and set up a meeting with their supplier, Madsen.  Instead of negotiating, Frank impulsively murders Madsen and takes his cocaine.  Eddies excitement quickly turns to apprehension when he hears about Madsens death, but Frank talks him into bluffing their way back into drug lord Vincent Camastras good graces.  At the same time, dirty cop Jack Reiley investigates the murder and is annoyed when DEA agent Maxwell takes over the investigation.  As Frank attempts to save his relationship with his naive girlfriend, Ashley, Reiley violently bullies his informants for information, and Camastra sends assassins to kill Eddie.  After Eddie engages in a brief gunfight with the assassins, Camastra himself beats Eddie unconscious.  Ashley, who has become suspicious of Franks secretive lifestyle, is shocked when Kathy and Beverly, Eddies friends,  interrupt them to explain that Camastra has kidnapped Eddie.

Beverly, an undercover DEA agent, is in a relationship with Maxwell, who is concerned that she has gone too deep.  However, she protests when he requests that she abandon the case.  Beverly and Maxwell expand their investigation to include Reiley, but they are forbidden from taking action until they can also get Camastra.  Frustrated, they bide their time until Camastra makes his move.  Frank visits Camastra, and Eddie successfully buys them two days time to pay Camastra back.  At the nightclub Frank owns, Reiley attempts to blackmail him, only to be shot in his hand and left on the side of the road.  Unable to explain his predicament, Reiley is put on unpaid leave and learns that Internal Affairs is preparing a case against him.  Forced to abandon his plans to retire comfortably, Reiley becomes even more unstable and kidnaps Ashley.  He breaks into Eddies apartment, takes Kathy and Beverly hostage, and waits for Frank and Eddie to return.

As Maxwell and his team race to the apartment, Frank and Eddie discuss their future plans.  Frank wants to retire from the criminal life; Eddie, while wishing Frank well, says that he will stay in the business and take over.  Frank admits that he will miss the excitement, but he wants a normal life with Ashley, whom he plans to marry.  Frank, Eddie, and the DEA agents all enter the apartment within minutes of each other.  Panicked, Reiley kills several people, including Eddie and Ashley.  In the chaos, Beverly sets Kathy free, and Reiley and Frank escape.  Maxwell corners Reiley, and they face off against each other; reinforcements arrive just as Maxwell subdues Reiley.  In the last scene, Frank mourns the loss of Ashley and speculates on what he will do next, now that both his main plan and backup plan have been ruined.

== Cast ==
* Daniel Bonjour as Frank
* Gianni Capaldi as Eddie
* Dolph Lundgren as Maxwell
* Randy Couture as Det. Jack Reiley
* Vinnie Jones as Vincent Camastra
* Carly Pope as Beverly
* Susie Abromeit as Kathy
* Cinthya Bornacelli as Ashley
* Domiziano Arcangeli as Madsen

== Production ==
Capaldi, Lundgren, and Jones acted together in three of Serafinis films back-to-back.   Each film also used the same crew.   Originally titled Rush, the film was renamed to Hard Rush and finally Ambushed to avoid conflicts with other upcoming films. 

== Release ==
Ambushed was released in the UK on September 28, 2013,  and in the US on November 12, 2013. 

== Reception ==
Nav Qateel of Influx Magazine rated it B and called it "a great little movie that entertains from beginning to end" that "starts to get bogged down in side story".   Ian Jane of DVD Talk rated it 3/5 stars and called it "a fine pizza and a six pack movie" despite the cliches.   David Johnson of DVD Verdict wrote that the films main characters are too uninteresting and unlikeable. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 