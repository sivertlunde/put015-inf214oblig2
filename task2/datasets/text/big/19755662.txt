Woman's World (film)
{{Infobox film
| name           = Womans World
| image          = Womansworld.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Jean Negulesco
| producer       = Charles Brackett Richard Sale Mona Williams (story)
| starring       = Clifton Webb June Allyson Van Heflin Lauren Bacall Fred MacMurray Arlene Dahl Cornel Wilde
| music          = Cyril J. Mockridge
| cinematography = Joseph MacDonald
| editing        = Louis R. Loeffler
| distributor    = 20th Century Fox
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $2,010,000 
| gross          = $3 million (US rentals)  
}}
Womans World, also known as A Womans World, is a 1954 Technicolor drama film about corporate America. Three men compete for the top job at a large company.

==Plot== Kansas City. Elizabeth Burns (Lauren Bacall) is becoming estranged from her driven husband Sidney (Fred MacMurray) because his work is consuming him and undermining his health; she fears a promotion would eventually kill him. Jerry Talbot (Van Heflin), who has a sexy, ambitious wife, Carol (Arlene Dahl), rounds out the trio.

As time goes by, Katie is shown to be a bit of a Accident-proneness|klutz, both physically and socially. On the other hand, Elizabeth is both poised and gracious. Despite their differences, she and Katie get along well. When the couples are unexpectedly invited to spend the weekend at the estate of Giffords sister Evelyn Andrews, (Margalo Gillmore), Elizabeth generously helps Katie buy appropriate clothing on a limited budget.

==Cast==
* Clifton Webb as Ernest Gifford
* June Allyson as Katie Baxter
* Van Heflin as Jerry Talbot
* Lauren Bacall as Elizabeth Burns
* Fred MacMurray as Sidney Burns
* Arlene Dahl as Carol Talbot
* Cornel Wilde as Bill Baxter
* Elliott Reid as Tony Andrews, Giffords nephew
* Margalo Gillmore as Evelyn Andrews, Giffords sister

==See also==
* Executive Suite, a similar film released the same year, with June Allyson again playing a reluctant, but loyal wife

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 