Happy Birthday to Me (film)
 
 
{{Infobox film
| name           = Happy Birthday to Me
| image          = Happy birthday to me poster.jpg
| image_size     =
| caption        = Theatrical Poster
| director       = J. Lee Thompson John Dunning Stewart Harding Andre Link
| writer         = Timothy Bond Peter Jobin John Saxton Uncredited: John Beaird
| narrator       =
| starring       = Melissa Sue Anderson Glenn Ford Lawrence Dane Sharon Acker Frances Hyland Tracey E. Bregman Lisa Langlois
| music          = Bo Harwood Lance Rubin
| cinematography = Miklos Lente
| editing        = Debra Karen
| distributor    = Columbia Pictures
| released       =  
| runtime        = 110 min.
| country        = Canada
| language       = English
| budget         = $3,500,000 (estimated)
| gross          = $10,609,514 
| preceded_by    =
| followed_by    =
}} Canadian slasher film directed by J. Lee Thompson and starring Melissa Sue Anderson and Glenn Ford. It was released on 15 May 1981, and has since become something of a cult classic among fans of the slasher genre, with its bizarre murder methods and twisted climactic revelation. 

==Plot==
Virginia "Ginny" Wainwright is a pretty and popular high school senior at Crawford Academy. She is one of her schools "Top Ten": an elite clique which comprises the richest, most popular and most snobbish teens at the Academy. The Top Ten meet every night at the Silent Woman Tavern, a pub near Crawfords campus.

One night en route to the tavern, Top Ten member Bernadette OHara is attacked in her car by a killer whose face cannot be seen. Unable to flee, she struggles and then plays dead to catch the killer off-guard, before running to get help. She finds somebody whom she is familiar with, only to have her throat slit by this person with a razor (whom the audience still cannot see) who is actually the killer.

The Top Ten is briefly concerned when Bernadette fails to show up at the Silent Woman. They soon get over it when their argument with another customer results in one of them putting a pet mouse into his beer; mayhem ensues, and the Top Ten flee the scene.

On the way home, the Top Ten see a drawbridge going up and decide to play a game of chicken: all cars in the game must make it across before the bridge is completely raised (to allow the passing of ferries). A protesting Ginny is shoved into a car by fellow Top Ten member Ann Thomerson. Every car jumps the drawbridge save one. As the car goes over the drawbridge Ginny yells "Mother!" and makes it safely across. After the car stops Ginny runs from the vehicle into the darkness home.

On the way home she stops by her mothers grave to tell her shes popular and hangs out with the Top Ten all the time. Ginny is confronted by her father about coming home after her curfew. Unbeknownst to either of them, Top Ten member Etienne Vercoures, a French foreign exchange student has followed her home. He enters Ginnys room, steals a pair of her underwear and escapes without being seen.

Ginny shares a handful of lost, repressed memories with her on-call psychiatrist, Dr. David Faraday. She underwent an experimental medical procedure, involving surgery to restore brain tissue, after surviving a harrowing accident at a drawbridge.

As Ginny attempts to resume her normal life, her fellow Top Ten members are murdered in very vicious and violent ways: Etienne is strangled when his scarf gets caught in the spokes of his motorcycle (à la Isadora Duncan); Greg has his neck crushed while lifting weights, and yet, the killer, who always sports a pair of black gloves, is never seen. Until one night, Top-Ten member Alfred, whos had a strong crush on Ginny, follows Ginny to her mothers grave and is then gutted with a pair of garden shears by Ginny.

On the weekend of her 18th birthday, Mr. Wainwright leaves on a business trip. After a school dance, Ginny invites Steve, another member of the Top Ten, to her place for a midnight snack. She prepares shish kebabs, and feeds him, while they drink and smoke marijuana. Then, Steve unsuspectingly leaves his mouth open and Ginny shoves the kebab skewer violently down his throat; leaving little doubt that Ginny may be the killer at this point.

The following morning, Ginny is taking a shower fighting to remember everything up to that point. In flashbacks, the following is revealed. Ginnys mother, a newly inducted socialite, invited the Top Ten to Ginnys birthday celebration four years earlier. Instead, the Top Ten went to Anns party. Drunk and unstable, Mrs. Wainwright confronted the grounds-keeper. Ginnys mother learned that she had a reputation as the town whore; ergo, neither she nor her daughter were welcome at the Thomersons. This led to Mrs. Wainwright attempting to drive across a bridge that was in the process of opening. With Ginny screaming, her mother finally stopped in the middle of the bridge as each side was still raising. The car fell in between the bridge halves and Mrs. Wainwright drowned in her car, although Ginny swam to safety.

Ginny realises that she may have killed her friends after all, including Ann, who had just paid her a visit. With Ginnys 18th birthday steadily approaching, she struggles to get answers from Dr. Faraday; when he fails to provide any, she kills him with a fire poker.

Mr. Wainwright returns from his business trip, ready to celebrate his daughters 18th birthday. Entering their house, he sees blood and frantically attempts to locate Ginny. Instead, Mr. Wainwright finds Gregs girlfriend Amelia in shock in the courtyard clutching a gift, his late wifes grave (which recently has been robbed) and Dr. Faradays corpse. Entering a cottage which serves as the Wainwrights guest quarters, he makes a ghastly discovery.

The corpses of all the murdered Top Ten members are seated around the table, which has been set to look exactly as it did four years ago. The corpse of Mrs. Wainwright is seated there as well. Then Ginny enters, carrying a large cake and singing "Happy Birthday" to herself. Already distraught, Mr. Wainwright bursts into tears when his daughter casually admits to committing the murders. Ginny then slits her fathers throat with the same large knife she used to cut the cake. He never sees the real Ginny, who is sedated, seated at this table as the killers only living guest.

Ginnys doppelganger rants about having done all of this for Ginny, who then awakens to discover that the second Ginny is really Ann in disguise. Ann has been embittered by the revelation of her fathers affair with Ginnys mother. It turns out that both girls are half-sisters. Ann slaughtered the six main members of the clique, that never showed up for her birthday party, expressly for the purpose of framing Ginny – who suddenly breaks free, takes Anns knife and kills her with it.

As she stands over her half-sisters corpse, a detective walks in on Ginny and the horrifying carnage that surrounds her. He stares at Ginny and demands, "What have you done?" The audience is then left to wonder if Ginny will be cleared of the killings or arrested for them. 

We hear Ginny singing "Happy Birthday to Me" as the film ends.

==Cast==
*Melissa Sue Anderson as Virginia "Ginny" Wainwright
*Glenn Ford as Dr. David Faraday
*Lawrence Dane as Harold "Hal" Wainwright
*Sharon Acker as Estelle Wainwright
*Frances Hyland as Mrs. Patterson
*Tracey E. Bregman as Ann Thomerson
*Jack Blum as Alfred Morris
*Matt Craven as Steve Maxwell
*Lenore Zann as Maggie
*David Eisner as Rudi
*Michel-René Labelle as Etienne Vercures
*Richard Rebiere as Greg Hellman
*Lesleh Donaldson as Bernadette OHara
*Lisa Langlois as Amelia

==Pre-production== My Bloody Friday the New Year’s He Knows Prom Night Graduation Day Home Sweet regenerating frogs flashbacks and Syncope (medicine)|blackouts, yet is unsure of her role in the mayhem around her.

Although it seems to have been directly influenced by the success of Friday the 13th and Prom Night, it is worth noting that pre-production on Happy Birthday to Me had started before those films had been released, which more than hints that the huge success of Halloween was perhaps more of an influence (although the Grand Guignol elements of Friday the 13th may have convinced the makers of Happy Birthday to Me to add more gore as production geared up).

The specialized genre website   has a copy of the script purporting to be a third draft from April 1980, where the major difference is that Virginia is actually the killer, possessed by the spirit of her deceased mother. Although this ending logistically makes more sense than the ending that was filmed, the filmmakers thought that what was originally scripted was not climactic enough. Still, the majority of the film does point to this original ending, which indicates the switch came well into production. This version of the script also features a good number of scenes that were either never shot or rewritten, including some that show more clearly Alfreds love for Virginia, and Virginias difficult relationship with her father.

The script was completely reworked by screenwriting team Timothy Bond and Peter Jobin before production started. 

==Production== Cape Fear exploitative material than Happy Birthday to Me.
 Jonathan Kent The Fan Alone in the Dark (1982) with Donald Pleasence and Jack Palance could compare as old-Hollywood stars in a slasher film.

The films make-up effects were done by special effects guru Tom Burman (who replaced Stéphan Dupuis just three weeks before the cameras were due to start rolling). Dupuis later did the duties on another bigger budget Canadian slasher Visiting Hours (1982), but left the production for undisclosed reasons. Ironically, in an issue of Fangoria from 1981, Burman criticizes the level of gore in films at that time.
 Loyola College in Montreal. The draw bridge scenes were actually filmed in Phoenix, New York, just outside Syracuse, New York|Syracuse. The producers found it difficult to find the right bridge closer to the main production, as the expansion of the Highway system had made them increasingly rare. The whole town of Phoenix came to watch the dangerous stunts, where a total of fifteen cars were junked, and one stunt driver was hospitalized with two broken ankles.

The film’s ending was changed to hide the fact that the script was being rewritten so late in production.

Bo Harwood and Lance Rubin provided the films score. Syreeta, one-time wife of Stevie Wonder, provided the eerie closing track that plays over the credits. 

==Promotion==
Columbia Pictures bought the $2.5 million production for $3.5 million, following Paramount Pictures lead with buying Friday the 13th the year before. Columbia reportedly put as much money into promoting the film as it cost to make.

The promotional materials sold the odd-ball nature of the murders as the prime reason to see it, with the poster advertising “Six of the most bizarre murders you will ever see.” Whilst the death scenes are not overly gory (arguably because of censorship troubles), they are sensationalist without being mean-spirited.

Dunning and Link didnt like the advertising campaign that Columbia Pictures had planned; they thought it should have been more subtle and worried that it might put off as many people as it attracted. They were concerned that only a handful of the murders in the film were truly bizarre, and that the audience might feel cheated. 

Columbia Pictures really pushed the promotional manual for Happy Birthday to Me, which was jam packed with ideas for cinemas to promote the film. Although it is not clear how many picture houses really embraced the films promotion, some of the more colorful ideas were to stage a mini recreation of the films final scene (without the bodies), but with a butchered birthday cake with crimson candles surrounded by glittering birthday party hats; all to be set upon a fake coffin. People celebrating their own birthdays were encouraged to bring family and friends with incentives, such as t-shirts and party hats. They also suggested having a member of staff, dressed in funereal black, preventing anyone from entering the auditorium during the final ten minutes. Those in line would then be offered "a bite-sized slice of Virginias birthday cake” from the concession stand.
 radio disc jockeys to promote the film, including a special scream in. Callers would be asked questions such as "How would you react if you went to a birthday party … and you were the only person at the dinner table who was still alive?". Those with the best set of lungs would win free passes to the film. The manual also encouraged the DJs to attend dressed as funeral attendants and give each girl a white lily and each boy a blood-red carnation. 
 trailers both at the cinema and on TV. Most trailers culminate with a birthday cake being split with an axe, although an axe does not actually feature in the film itself.

===Censorship===
My Bloody Valentine had been severely cut due to the   states that the film was released “virtually uncut” because of Thompsons stature with the MPAA. In the United Kingdom, the BBFC passed the film without any cuts on April 30, 1981. 

==Release==
===Box-office===
Despite the mostly harsh critical notices Happy Birthday to Me made $10 million at the domestic box office. 

===Critical reception===
Critical reaction to the film was harsh when it was released to American screens on the May 15, 1981. Vincent Canby in the New York Times said it was, “comparatively expensive ripoff of such teen-age love-and-meat-cleaver films as Friday the 13th and Prom Night.” He went on to call it “confused” and said it was directed “without style”. James Harwood in Variety (magazine)|Variety called the film “monumentally stupid,” summing up, “In her film debut, Melissa Sue Anderson clumsily carries the suspense of whether she is or isnt the killer, with director J. Lee Thompson helping her with clouds of confusion that just get dumber and dumber until the fitful finale.” In the September 1981 issue of the British magazine Films on Screen and Video, Eric Braun stated that the film and others of its ilk were responsible for the decline in cinema-going. He said, "The systematic killing, unhappily, seems to extend to the cinemas on Ranks doomed list, which are being quite inevitably emptied by a constant diet of this kind of nonsense ... Please mourn with me the unhappy demise of many splendid halls of entertainment.” Halliwells Guide was no kinder, describing it as an “Abysmal teenage shocker which grinds on relentlessly for almost two hours.” Critic Leonard Maltin sneered that Glenn Ford had hit “rock bottom” saying that he must have been “desperate for work”. Even some of the horror critics were not kind, with the Aurum Film Encyclopedia of Horror describing it as “... a lucklustre addition to the teenage horror cycle”, but did acknowledge the climax of the film had a “gothic effectiveness”. 
 AllMovie gave the film a mixed review, writing, "Happy Birthday to Me stands out from the slasher movie pack of the early 80s because it pushes all the genres elements to absurd heights. The murders, plot twists and, especially, the last-minute revelations that are dished up in the final reel dont just deny credibility, they outright defy it." 

==DVD and Blu-ray releases==
The initial DVD release from Columbia Pictures featured a new soundtrack for the film and a new completely different cover art (not original poster art), which fans did not appreciate. Which also caused many slasher fans to beg for a re-release of the film with the original soundtrack that was heard in theatres and on the Columbia VHS.

Anchor Bay Entertainment released a new DVD with the image from the original poster artwork as the DVD cover and using the films original soundtrack. The DVD was released on 13 October 2009. 
However, some Walmart stores released it a week earlier with a huge selection of other horror movies for only $5.  
 When a Stranger Calls. This release, like the 2009 DVD, features the original 1981 audio.

==Soundtrack==
The films score varies from copy to copy. Some video releases contain the original theatrical score, whereas others contain a newer, completely different score and ending theme. The original score was written and composed by Bo Harwood and Lance Rubin, respectively.

The films ending theme, "Happy Birthday To Me" was sung by Syreeta Wright, (credited as simply Syreeta in the ending credits) and written by Lance Rubin (music) and Molly-Ann Leikin (lyrics). Subsequent releases (primarily the recent DVD copies) feature a disco track in place of this theme.

There was no official soundtrack release, however fan-made copies exist over the Internet.

==Production information== John Dunning, with the assistance of special effects man Tom Burman, Thompson "would be splashing blood ALL over the place."  Columbia video shish kebab death scenes, plus the original music score. The 2004 DVD release is the edited R-rated version with the alternate music.
*The press reported that to keep the "twist" ending a secret several endings were shot. This is untrue but helped hide the fact that while shooting, the film had no ending. The script was written with one ending that made sense to the story, but did not have a twist. So producers proceeded to film while tinkering with a twist. This explains why there is no build up. 
*The movie was not popular with most critics; film historian Leonard Maltin considered it a bomb, stating that "Glenn Ford must have been desperate for the work" and said that Ford had "hit rock bottom with this appearance."
*Lisa Langlois auditioned for the role of Ann but the role went to Tracy Bregman instead. 
*According to an interview with Lisa Langlois, her character Amelia was originally killed off. In the first cut of the film, her character got an axe to her head. Langlois was told by David Douglas (a cameraman on the set) that the film would have been rated X if they kept that scene intact. So instead, the producers decided to make her look like the sole survivor in the final release. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 