Corso: The Last Beat
 
{{Infobox film
| name           = Corso: The Last Beat
| image  = Corso- The Last Beat FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = Gustave Reininger
| producer       = 
| writer         = Gustave Reininger
| narrator       = Ethan Hawke
| music          = 
| cinematography = Harry Dawson Richard Rutkowski
| editing        = Damien LeVeck
| studio         = 
| distributor    = 
| released       =  
| runtime        = United States Italy
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
Corso: The Last Beat is a 2009 documentary film, with on-screen narration by Ethan Hawke and appearances by Patti Smith, Allen Ginsberg, William Burroughs and Gregory Corso.

==Synopsis ==
The Beat Generation’s Gregory Corso became one of four in the inner circle of the Beats, along with Jack Kerouac, Allen Ginsberg, William S. Burroughs. Corso grew up in foster homes, on the streets of Little Italy and Greenwich Village. Corso was sentenced to Clinton State Prison for stealing a $50 suit to go on a date; there, protected by Mafia inmates, Corso read his way through a three-year sentence and ended up at Harvard. He eventually met Allen Ginsberg at a dyke bar in the West Village. After Allen Ginsberg’s death, Corso goes "On the Road" to rediscover his creative Muse. From Paris to Venice, Rome to Athens, Mount Parnassus to Jim Morrison’s grave, Corso reflects on the early days of "Beats." In high humor he reveals how "The Beats" emerged in Europe, and paved the way for youth culture, the sexual revolution and even hip-hop. Corso interrupts his tour to revisit Clinton State Prison and inspire young inmates. Returning to Italy, Corso muses on his lost mother, who he believes is dead and buried in Italy. In a stunning discovery, filmmaker Gustave Reininger finds Corsos mother not dead in Italy, but alive in Trenton, N.J. Corso meets his mother on film and discovers she is the Muse he has been seeking. He also discovers that his mother left him to escape the violence and sexual abuse of his father, who lied saying she had returned to Italy. Healing from a life of abandonment, emotional deprivation and abuse, Corso finishes his road trip on the Acropolis in Greece.A revitalized Corso returns to Greenwich Village, only to discover he is dying. He faces his own death with pluck and humor, comforted by Ethan Hawke, Patti Smith and his newfound mother, Michelina.

==The making of Corso, the Last Beat==
Gregory Corso who unlike his compatriot Allen Ginsberg, avoided publicity and promotion of the "Beat" label, refused all film and biography requests.  Allen Ginsberg introduced Corso to filmmaker Gustave Reininger, and after a lengthy quiz on Gilgamesh, Heraclitus, and St. Clement of Alexandria, Corso decided to allow Reininger to make a film.  Reininger had made feature films and created network television but had never attempted non-fiction.  Corso proved a challenge.  Ginsberg offered to rein in Corso, known for his boisterous and confrontational antics, however a month after Corso agreed, Ginsberg became ill and died precipitously.  Corso, with the loss of his best friend and literary alter-ego, became nearly catatonic. Beat fans would accost him, saying "Hey, Youre the Last Beat" (Burroughs died shortly after Ginsberg).  Corso, angered would always shoot back, "Is that your myth or mine."  Reininger considered returning his funding for the film, raised privately, as Corso was non-responsive.  Reininger though had an inspiration to take Corso back to Europe, Paris in particular, where the Beats had emerged, working in a small rundown hotel on the Left Bank, on Rue Git Le Coeur.  Corso named it "The Beat Hotel." 

Once in Europe, Corso came alive and led Reininger and his film crew on a madcap tour of France, Italy and Greece.  The film is shot in the verite style, without much exposition.  Corsos street theater antics kept the films serious tone light ironic. Corsos poetry is woven into the film, with different voices. Discussions are underway with Bob Dylan to read a Corso poem, "Destiny", and with Bono to read "At Oscar Wildes Tomb."

From Paris, to Rome, to Florence, Delphi and Athens, Corso retraces the steps of him and Ginsberg and Burroughs as young self-proclaimed poets, (though unpublished.)  In Venice, Corso muses about his mother who abandoned him, and Reininger proposed finding her grave in Italy.  For a year, he quietly searched cemeteries, genealogical records and official documents.  His search turned up nothing in Europe, and relentlessly Reininger considered that perhaps the story of his mothers return was a lie told by his father.  The search resumed in America, where again from cemeteries, hospital records, church documents and anecdotal accounts, Reininger picked up the trail of the missing mother, which had gone cold 67 years ago.  Relentlessly Reininger searched and found that she was not in Italy, and certainly not dead, but 85 and living all her life in Trenton, New Jersey as a waitress, married to a short order cook. Her abandoned son was a secret withheld from her husband and two children. Corsos on screen meeting with his mother had the potential to be explosive.

Corso, about a year thereafter, developed prostate cancer, and summoned Reininger to continue filming on his death bed.  His last poker game bring great levity, a visit from Ethan Hawke, a lullaby from Patti Smith, and a final visit with his new-found mother mark the films third act.

In addition to the film, Reininger commissioned a major bibliography of Corsos letter and holdings in various public and university libraries, conducted by Bill Morgan, the archivist for Allen Ginsberg and Lawrence Ferlinghetti.

The goal of the Corso Project is to restore Corso to his literary place as one of Americas premier poets and social revolutionaries.

== Awards, recognition and distribution==
Reininger withheld the film from the distribution market during the recent recession, which saw many independent film distributors leaving the business. Instead he took the film to European festivals.  It was the audience award winner at Italys most prestigious Taormina Film Festival.  At the Dubai international Film festival it won awards in World Cinema.  It was heralded in at the Rome Festival, and a Festival Litteratura in Mantua where it won Best Film.  It was received at the Glasgow International Film Festival and then show as a work in progress at the Hamptons International Film Festival.  Distributuion is expected to be coordinated with the film adaptation of Kerouacs "On the Road," produced by Francis Ford Coppola.

== Gustave Reininger - filmmaker==
Gustave Reininger is the creator of the hit NBC TV drama series (and feature film), "Crime Story." Working with Michael Mann, Reininger crafted the 1960s period drama of the Chicago Mob and how they took Las Vegas.  It discovered Dennis Farina and David Caruso and  featured other prominent artists such as  Julia Roberts, Madonna, Dennis Haysbert, Gary Sinise, Miles Davis, Dexter Gordon, Stanley Tucci and Kevin Spacey.
While living in Paris, Reininger wrote and co-produced a French Language thriller starring Dennis Hopper, “L’Ordre et la Securite´ du Monde.” Returning to America, Reininger was a contributor to Michael Manns “Miami Vice” and “Robbery Homicide Division.” He has written for and co-produced with directors Sydney Pollack, Michael Apted, Penny Marshall, Paul Verhoeven and other notable filmmakers. 
Corso – The Last Beat,” a longitudinal, ten-year feature documentary is Reininger’s featured debut.
With Archbishop Desmond Tutu, Thich Nhat Hanh, The Dalai Lama, Deepak Chopra and Sr. Helen Prejean, Reininger is finishing commitments to a feature, “The Big Question”, which he co-wrote and co-directed.
Reininger is preparing a dramatic version of “Corso ...” along with his other dramatic film and television commitments, including “Promises to Keep” a political thriller on the U.S. invasion of Cuba at the Bay of Pigs.
Gustave Reininger was born in Kentucky, was schooled by Jesuits, and graduated from University of Chicago. He began his business career as an international investment banker with Chemical Bank (J.P. Morgan Chase.) After stints of living in Paris and London, he now resides in Los Angeles with his three children Olivia, Isabel and Haven.

==Collaborators and producers==
Steven J. Edwards, composed the score for Corso. Damien LeVeck edited and produced the film.  Donna Stillo is an executive producer.  Amanda Gill and Haven Reininger are co-producers.  Music in the film ranges from Charles Mingus to the Doors, Nina Simone, and Charlie Parker.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 