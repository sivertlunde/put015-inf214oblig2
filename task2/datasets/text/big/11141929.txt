Aakali Rajyam
 
{{Infobox film
| name           = Aakali Rajyam
| image          = Aakali Rajyam.jpg
| image_size     =
| caption        = Promotional Poster
| director       = Kailasam Balachander|K. Balachander
| starring       = Kamal Haasan Sridevi
| music          = M.S.Viswanathan
| cinematography =
| editing        =
| distributor    =
| released       = 1981
| runtime        =
| country        = India
| language       = Telugu
}}
 Telugu language film starring Kamal Haasan and Sridevi. It is a remake of K. Balachanders 1980 Tamil film Varumayin Niram Sivappu that also starred Kamal Haasan and Sridevi in the lead. The film was also remade in Hindi as Zara Si Zindagi with Kamal Haasan and Anita Raj in 1983, directed by K. Balachander himself. Kamal Haasan won Filmfare Award for Best Actor - Telugu. 

==Plot==
The story of this film is set during the late 70s and early 80s when job prospects in India were very poor. The movie dwells on the premises of social satire and through its characters (the young unemployed graduates) critically analyzes the state of Indian polity and social situation.

Kamal Hassan comes to Delhi in search of a job and finds it difficult to earn one job without bribes, recommendations or using influence. He follows the words of Mahakavi Sri Sri (Srirangam Srinivasarao). He criticizes the government for not providing jobs and health and basic amenities to the public. He and his friends share a room and they live in abject conditions such as staying hungry for more than a day and wearing torn clothes. He meets Sridevi, who is his neighbor, and soon they fall in love. As the story proceeds, misunderstandings crop up between the duo. Eventually, Kamal settles as barber and gets married to Sridevi at the end.

==Soundtrack==
* "Gussa Rangaiah Koncham Taggaiah" (Lyrics: Aathreya; Singer: P. Susheela)
* "Oh Maharshi oh Mahatma" (Lyrics: Sri Sri; Singer: S. P. Balasubramaniam)
* "Kanne Pillavani Kannulunnavani" (Lyrics: Atreya; Singers: S.P. Balasubramaniam, S. Janaki)
* "Saapatu Yetu Ledhu Pataina Paadu Brother" (Lyrics: Atreya; Singer: S.P. Balasubramaniam)
* "Tu Hai Raja Mein Hoo Rani" (Lyrics: P B Srinivas; Singer: S. Janaki)

==Awards==
* Kamal Haasan won Filmfare Award for Best Actor - Telugu (1981).

== References ==
 

 

 
 
 
 
 
 

 