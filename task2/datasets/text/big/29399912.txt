The Lovers (2013 film)
{{Infobox film
| name           = The Lovers
| image          = The Lovers (2013 film).jpg
| caption        = Film poster
| director       = Roland Joffé
| producer       = Paul Breuls Guy J Louthan Dale G Bradley Catherine Vandeleene
| screenplay     = Roland Joffé
| story          = Ajey Jhankar
| starring       = Josh Hartnett Bipasha Basu Alice Englert Tamsin Egerton Abhay Deol
| music          = Dirk Brossé
| cinematography = Ben Nott
| editing        = John Scott Bliss Media Limited Neelmudra Entertainment
| distributor    = Corsan World Sales Anchor Bay  (US)  
| released       =  
| runtime        = 
| country        = Belgium India Australia
| language       = English
| budget         = $27 million 
| gross          = 
}} romance Time time travel adventure film directed by Roland Joffé and written by Ajey Jhankar. The film stars Josh Hartnett, Bipasha Basu, Alice Englert, Tamsin Egerton and Abhay Deol in lead roles.
 first Anglo-Maratha British India, marine biologist and his wife. 

==Plot==
Great Barrier Reef, 2020. After a dangerous dive to save his wife Laura (Tamsin Egerton) trapped while exploring the wreckage of a colonial British merchant ship, Jay Fennel (Josh Hartnett), a marine archeologist lies brain dead in a Boston hospital.

Fennels dream-like coma takes us back in time to Pune in 1778. The British East India Company is invading the palaces and a young Captain named James Stewart (also played by Hartnett), who bears a striking resemblance to Fennel, is about to embark on a dangerous mission. Along the way he encounters murder, deceit, betrayal and revenge. He falls deeply in love with a Maratha warrior, Tulaja Naik (Bipasha Basu), an impossible love for which he must fight.

Only the power of a ring can transcend time and save a life. 

==Cast==
* Josh Hartnett as Jay Fennel and James Stewart
* Bipasha Basu as Tulaja Naik
* Alice Englert as Dolly
* Tamsin Egerton as Laura Fennel, Jay Fennels wife
* Andrea Deck as Ali    
* Abhay Deol as Udaji  
* Shane Briant as the governor of Bombay 
*Bille Brown   
* Claire van der Boom 
* Atul Kulkarni as Raoji  
* Milind Gunaji as Shiv
* Simone Kessell as Clara Coldstream James Mackay as Charles Stewart, Captain James Stewarts brother
* Vijay Thombre as Havaldar Desai, Captain James Stewarts right-hand man  
* Aegina de Vas as queen Jamnabai 
*Tehmina Sunny as Sonubai
* Nisha Sharma as Radha Jee
*Mahesh Jadu as the Assassin


==Production==
Singularity was produced by Paul Breuls Corsan NV, Dale G. Bradleys Limelight International Media Entertainment, Marion Wei Hans Bliss Media Ltd and co-financed by Ajey Jhankars Neelmudra Entertainment. Sales outfit Corsan World Sales, a division of Corsan NV, is handling international sales.
 Aishwarya Rai Bachchan and Vivek Oberoi.  
 romance is Bipasha Basus first English language film. She was encouraged to take up her international debut by Oscar winning actress Hilary Swank. Basu first met up with Swank at the New Seven Wonders of the World Official Declaration ceremony convened in Lisbon in 2007 and met her again at the 2011 International Indian Film Academy Awards in Toronto.  

Though originally planned for shooting partly in the United Kingdom,  production began in Queensland,  Australia, on 8 November 2010. It concluded there on the third week of December 2010.    The production then moved to India on 31 March 2011 for a further five weeks, roaming the rocky terrain of Chambal Division|Chambal, Orchha and Gwalior in the state of Madhya Pradesh.   Principal photography wrapped on 23 April 2011. 
 Cannes International Film Festival in May 2012,   but in late 2011 the production company was placed in administration and all further filming was stopped when some ten percent remained.   In June 2012, Belgian-based financing and production outfit Corsan agreed to put further money into the production, and the final scenes were to be shot in London.  In September 2012, it was reported that the filming in London was finished, while some creditors were still waiting for payment.  And in May 2013, producer Paul Breuls confirmed that the film has been finally completed and aims to launch it at an autumn festival. 

==Release== Festival International du Film de Marrakech during a master class given by Roland Joffé. 

In late April 2014, the production company Corsan announced that the film would be shown at the 67th annual Cannes Film Festival on 15 May 2014 under the new name The Lovers.  

==References==
 kdlsbeøendk

==External links==
*  
*  
*   (Corsan World Sales)
*   (Bliss Media Ltd)

 

 

 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 