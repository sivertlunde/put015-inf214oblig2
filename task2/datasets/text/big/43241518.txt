Orange Mittai
{{Infobox film
| name           = Orange Mittai
| image          = 
| caption        = 
| director       = Biju Viswanath
| producer       = Vijay Sethupathi B. Ganesh
| writer         = Biju Viswanath Vijay Sethupathi
| starring       = Vijay Sethupathi Ramesh Thilak Aashritha
| music          = Justin Prabhakaran
| cinematography = Biju Viswanath
| editing        = Biju Viswanath
| studio         = Vijay Sethupathi Productions
| distributor    = Common Man Productions
| released       =  
| country        = India
| language       = Tamil
| budget         = 
}}
 Indian Tamil Tamil feature film co-written and directed by Biju Viswanath. Produced by actor Vijay Sethupathi, the film features him in the lead role alongside Ramesh Thilak, Arumugam Bala and Aashritha. The film began production in mid-2014. 

==Cast==
* Vijay Sethupathi
* Ramesh Thilak as Sathya
* Arumugam Bala
* Aashritha as Kavya

==Production==
Actor Vijay Sethupathi launched his production, Orange Mittai, in February 2014 and signed up director Biju Viswanath to direct the venture. The film was said to feature an ensemble cast including Jayaprakash, Ramesh Thilak, Aru Bala and Aashritha in the lead roles, with Vijay Sethupathi stating he would not play a role.  

However in July 2014, Vijay Sethupathi revealed that he would play a 55 year old man in the film and promotional stills were released, replacing Jayaprakash in the character.  He is also making a debut as a dialogue writer with this film.  The team shot in Ambasamuthiram and other places surrounding Tirunelveli and Papanasam in South Tamil Nadu. 

==Release==

The official trailer of the film was released on 26 November 2014.  The trailer has two Vijay Sethupathis - one in his present day normal makeover, and another as a 55-year-old. The normal version talks about what the movie is about - a 48 hour journey in an ambulance - and his cast and crew. He says that he is playing a college student and he has even done a kuthu dance in the film. The older version encounters the normal ones talks with sarcastic comments, like most of the present day anomalous cyber bullies do.

==References==
 

==External links==
*  

 
 
 
 


 