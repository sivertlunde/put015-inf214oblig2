Delta Heat
{{Infobox film
| name           = Delta Heat
| image          = Delta Heat.jpg
| image_size     = 
| alt            = 
| caption        = 
| director       = Michael Fischa
| producer       = Richard L. Albert Rudy Cohen Uri Harkham   (executive producer) 
| writer         = Sam A. Scribner
| story          = Bruce Akiyama
| narrator       = 
| starring       = Anthony Edwards Lance Henriksen Betsy Russell
| music          = Christopher Tyng
| cinematography = Avraham Karpick Robert Gordon
| studio         = Sawmill Entertainment
| distributor    = New City Releasing
| released       = 30, December 1992 (USA)
| runtime        = 91 min.
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1992 film directed by Michael Fischa and written by Sam A. Scribner. The buddy police film was shot in New Orleans, Louisiana. 
The screenplay was originally written by Bruce Akiyama to be a television pilot, commissioned by Sawmill Entertainment, but after producer Richard L. Albert made The Forbidden Dance, he decided to hire writer Sam Scribner to expand the script to feature film length.  During production, producer Albert spent six hours in the bayous north of New Orleans convincing alligator hunter Bob Raymond to catch 40 alligators which appear in the final scene.

==Plot== Los Angeles Police Officer (Edwards) investigates the death of his partner in the swamps of Louisiana. He enlists the help of an ex-cop (Henriksen) who lost his hand to an alligator years before.

==Main cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor
! Role
|-
| Anthony Edwards || Mike Bishop
|-
| Lance Henriksen || Jackson Rivers
|-
| Betsy Russell || Vicky Forbes
|-
| Linda Dona || Tine Tulane
|-
| Rod Masterson || Crawford
|-
| Clyde Jones || Clayborne
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 