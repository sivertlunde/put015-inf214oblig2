Thudakkam
{{Infobox film
| name           = Thudakkam
| image          =
| image_size     =
| caption        =
| director       = I. Sasi
| producer       = Sherly Joseph, Sivachandvyde
| writer         = Suraj Dev
| screenplay     = I Sasi
| music          = Berny Ignatius
| released       =  
| cinematography = Utpal V Nayanar
| editing        =
| studio         = SS International
| lyrics         =
| distributor    = Saikumar
| country        = India Malayalam
}}
 Indian Malayalam Malayalam film, Saikumar in lead roles.  The film had musical score by Berny Ignatius. 

==Cast==
* Shivaj ... Kali
* Siddhique .... Sulthan Sulaiman Saikumar ... Jacob Mathew I.A.S
* K. P. A. C. Lalitha .... Devuchechi
* Salim Kumar .... Maakkan
* Sarath Das ... Rasheed
* Suja Karthika ... Gayathri
* Urmila Unni ... Adv. Lakshmi Nair
* Kulappulli Leela ... Narayaniyamma
* Jagathy Sreekumar ... Ittoop
* Bheeman Raghu ... Purushothaman
* Geethu Mohandas .... Karthika
* Thrissur Elsy
* Joseph.E.A .... Jose Vakkeel

==Soundtrack==
The music was composed by Berny-Ignatius. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics
|-  Amme ennoru || Sujatha Mohan || Kaithapram 
|-  KS Chithra, Karthik   ||  Kaithapram 
|-  Kochiyilum kandilla ||  Afsal, Chorus || Kaithapram||
|}

==References==
 

==External links==
*  

 
 
 


 