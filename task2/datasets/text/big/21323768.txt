The Kiss (2004 film)
 The Kiss  (original   film directed and written by Hilde Van Mieghem.

==Principal cast==
*Marie Vinck as Sarah Lenaerts
*Veerle Baetens as Rita
*Zakaria Bchiri as little boy
*Marc Bogaerts as ballet teacher
*Leonie Borgs as Celine Lenaerts
*Zoe Cnaepkens as Sarah Lenaerts (young)
*Axel Daeseleire as Detective
*Jan Decleir as Marcel Lenaerts
*Tom De Hoog as doctor
*Josse De Pauw as nonkel Hugo
*Hilde Van Mieghem as Denise Lenaerts

==Trivia==
*Hilde Van Mieghem and Marie Vinck are mother and daughter.

==Awards and nominations==
===Won===
Joseph Plateau Awards
*Best Actress (Vinck)

Viareggio EuropaCinema
*Best Actress (Vinck)

===Nominated===
Emden International Film Festival
*Emden Film Award - 2nd Place (Van Mieghem) 

Joseph Plateau Awards
*Best Belgian Composer (Carlens and Joris)

 
 
 
 

 