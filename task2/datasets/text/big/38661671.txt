Megan Is Missing
{{Infobox film
| name           = Megan Is Missing
| image          = File:MeganIsMissingPoster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Michael Goi
| producer       = Mark Gragnani
| writer         = Michael Goi
| starring       = Amber Perkins Rachel Quinn Dean Waite Jael Elizabeth Steinmeyer Kara Wang
| music          = 
| cinematography = Keith Eisberg Josh Harrison
| editing        = Michael Goi (uncredited)
| studio         = Trio Pictures 
| distributor    = 
| released       = May 2011
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American drama horror movie written and directed by Michael Goi.  The movie is presented by way of "found footage" and follows two teenage girls that go online to find friends but instead go missing. 

==Plot==
Megan Stewart, 14, and Amy Herman, almost 14, though polar opposites in personality, are best friends. Megan carries the front of being the most popular girl in school, but this masks a lifestyle of hard partying, drugs, alcohol, and indiscriminate sex. Amy, unpopular and socially awkward, clings to her relationship with Megan as a lifeline to social acceptance. Together, these two girls forge a deep friendship based on their mutual needs and regularly communicate by web chat cameras or cell phone. As Megan seeks friends who are different from her usual posse of hangers-on, she is introduced by a friend online to a 17 year old boy named Josh in a chat room. Megan and Josh bond quickly and Megan finds herself intrigued and attracted to him. One day, Megan goes to meet Josh in person behind a diner, and she is never seen again. Police investigate into her disappearance, but soon begin to give up after finding little to no leads and begin to assume she simply ran away.

However, Amy believes otherwise and talks online with Josh to find out if he knows about Megans fate. She finds his answers regarding Megan suspicious. After seeing security footage of Megans kidnapping, Amy tells the police about Josh and his possible involvement in Megans disappearance, which re-sparks the investigation. Josh says that he is watching her and tells her to shut her mouth or else. Soon after, however, Amy visits her favorite spot and begins to record a video diary. Right before the video cuts, someone is seen about to grab her. The police start to investigate Amys disappearance and find her video camera in a garbage can near her favorite hiding spot.

The last twenty minutes are "unedited footage" found on the camera. Josh is holding the camera and unlocks a large door in what appears to be a basement. This is where he has been hiding Amy, who is wearing nothing but her bra and underwear and is chained to the wall. He makes her eat food in a dog bowl before he violently rapes her. He comes back again later and apologizes and says he will let her go. He then shows her a barrel and tells her to get in it so she wont know where he lives when they leave. After Josh opens up the barrel, Amy runs away screaming as she sees Megans rotting corpse inside. Josh grabs Amy and forces her into the barrel along with Megans body before locking it. He loads the barrel into the car and then drives to a forest, where he digs a large hole as Amy screams and begs for her life. Josh pushes the barrel in the hole and fills it up before walking away, leaving Amy to die.

The film concludes with a clip of Megan and Amy relaxing on her bed together while talking about how one day they will meet the perfect guy and their plans for a happy future.

==Cast==
*Amber Perkins as Amy Herman
*Rachel Quinn as Megan Stewart
*Dean Waite as Josh
*Jael Elizabeth Steinmeyer as Lexie
*Kara Wang as Kathy
*Brittany Hingle as Chelsea
*Carolina Sabate as Angie
*Trigve Hagen as Gideon
*Rudy Galvan as Ben
*April Stewart as Joyce Stewart
*John Frazier as Bill Herman
*Tammy Klein as Louise Herman
*Lauren Leah Mitchell as Callie Daniels
*Jon Simonelli as Detective Simonelli
*Craig Stoa as Leif Marcus
*Debbie Ryan as Renee Rabago

==Reception==
Critical reception for Megan Is Missing was mostly negative, with the Oklahoma Gazette calling the film "annoying" and "dull".  Beyond Hollywood and DVD Verdict also panned the film, with Beyond Hollywood calling it "majorly disappointing" and DVD Verdict stating that they "  this disc had been missing from the box".   HorrorNews.net gave a more positive review, saying that the first portion of the film "really works", although they felt that the final twenty-two minutes "went a little overboard". 

==References==
 

==External links==
*  
*  

 
 
 
 