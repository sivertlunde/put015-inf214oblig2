Mildred Pierce (film)
 
{{Infobox film
| name           = Mildred Pierce
| image          = Mildred-Pierce-One-Sheet.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Michael Curtiz
| producer       = Jerry Wald
| screenplay     = Ranald MacDougall
| based on       =  
| starring       = Joan Crawford Jack Carson Zachary Scott Eve Arden Ann Blyth
| music          = Max Steiner 
| cinematography = Ernest Haller
| editing        = David Weisbart
| distributor    = Warner Bros.
| released       =  
| runtime        = 111 minutes
| country        = United States
| language       = English
| budget         = $1,453,000
| gross          = $5,638,000
}}

Mildred Pierce is a 1945 American drama film directed by Michael Curtiz and starring Joan Crawford, Jack Carson and Zachary Scott and featuring Eve Arden, Ann Blyth and Bruce Bennett, in a film noir about a long-suffering mother and her ungrateful daughter. The screenplay by Ranald MacDougall and the uncredited William Faulkner and Catherine Turney, is based upon the 1941 novel Mildred Pierce by James M. Cain. The film was produced by Jerry Wald, with studio head Jack L. Warner as executive producer.

Mildred Pierce was Crawfords first starring film for Warner Bros. after leaving MGM, and won her the Academy Award for Best Actress.

==Plot==
 
While the novel is told by a Third-person narrative|third-person narrator in strict chronological order, the film uses voice-over narration (the voice of Mildred). The story is framed by Mildreds interrogation by police after they discover the body of her second husband, Monte Beragon. The film, in Film noir|noir fashion, opens with Beragon (Zachary Scott) having been shot. He murmurs the name "Mildred" before he  dies. The police tell Mildred (Joan Crawford) that they believe the murderer is her first husband, Bert Pierce (Bruce Bennett).  Bert has already been interrogated, and confessed to the crime.  Mildred protests that he is too kind and gentle to commit murder, and goes on to relate her life story in Flashback (literary technique)|flashback.

Mildred was married to Pierce, who is presently unemployed. Bert had been a real estate partner of Wally Fay (Jack Carson).  Mildred has been supporting her family by baking and selling pies and cakes.  Bert accuses Mildred of caring more about their daughters, and making them her priority instead of her husband.  Mildred admits this, and the two decide to separate.  
 bratty social climber and aspiring pianist, and 10-year-old Kay (Jo Anne Marlowe), a tomboy. Mildreds principal goal is to provide for Veda, who longs for possessions her mother cant afford, and social status above that of her family.  She is ashamed of her mothers work as a baker.

Mildred searches for a job, but is hampered by her lack of employable skills.  She has never worked outside her home except for her small baking business.  The best she can find is as a waitress – a  fact she hides from Veda. One day, Veda gives their maid, Lottie (Butterfly McQueen), Mildreds waitress uniform, with full knowledge that its Mildreds.  Mildred confronts Veda and is forced to admit she is a waitress.  Veda treats her with derision and makes it clear that she is ashamed of her mother.

Bert arrives to take his daughters for his weekend visit.  Kay contracts pneumonia on the trip and dies.  Mildred channels her grief into work and throws herself into opening a new restaurant. With the help of her new friend and former supervisor, Ida (Eve Arden), Mildreds new restaurant is a success. Wally helps Mildred buy the property, and she expands into a chain of "Mildreds" throughout Southern California.

Mildred continues to smother Veda in affection and worldly goods.  Veda, despite her mothers love and lavish gifts, remains spoiled, selfish, disrespectful and expresses her scorn of Mildreds common background and choice of profession.  Vedas obsession with money and materialistic possessions only increases.

Veda secretly marries a well-to-do young man for his money and position, but expresses unhappiness about her marriage. Mildred and Wally agree to help her get out of the mess shes made, but Veda demands ten thousand dollars from her husband and claims shes pregnant.  Her husbands family agrees, but she smugly confesses to Mildred that she lied about her pregnancy just to get the money.  Mother and daughter argue, exchanging insults. Mildred tears up the check, is slapped by Veda, and throws Veda out of the house.

Bert invites Mildred out for the evening, but takes her to a club where Veda is performing as a lounge singer.  Bert says he couldnt bear to tell Mildred what their daughter was doing and had to show her.  Mildred begs Veda to come home, but Veda sneers and says her mother can never give her the lifestyle she wants.

Desperate to reconcile with her daughter,  Mildred coaxes Monte Beragon into a loveless marriage in order to improve her social status.  She knows Monte is a playboy with high social standing and an elegant mansion, but is virtually bankrupt.  Montes price for marriage is a one-third share of Mildreds business, so that he can settle his debts.  Mildred agrees, and Veda, eager to live out her dream as a debutante, pretends to reconcile with her mother.  

Beragon lives the life of a playboy, and takes Veda along for the ride.  Mildred continues to support everyone financially, but ends up losing her business because of Montes debts. She goes to confront Monte at his beach house, and finds her daughter in his arms.  Veda scornfully tells Mildred that Monte loves her, and will leave Mildred.  

Mildred runs out to her car in tears.  Monte shouts that he never promised to marry Veda, and wouldnt lower himself to wed a tramp like her.  A showdown ensues, and Veda shoots Monte.  Mildred hears the gunshots from her car.

Veda begs her mother to help her one more time, tearfully proclaiming her love and penitence.  Mildred refuses to rescue her daughter this time, and calls the police, but cant go through with it.

The detectives bring Veda into the interrogation room and explain theyve trapped Mildred all along.  They knew Veda committed the murder, and lead her off to jail.  Mildred tells Veda "Im sorry; I tried" but Veda, in typical careless fashion says "Dont worry about me; Ill get by" and is led away to be prosecuted.  Mildred leaves the station to find Bert waiting for her.

==Cast==
* Joan Crawford as Mildred Pierce Beragon
* Jack Carson as Wally Fay
* Zachary Scott as Monte Beragon
* Eve Arden as Ida Corwin
* Ann Blyth as Veda Pierce Forrester 
* Butterfly McQueen as Lottie
* Bruce Bennett as Albert "Bert" Pierce Lee Patrick as Mrs. Maggie Biederhof
* Moroni Olsen as Inspector Peterson
* Veda Ann Borg as Miriam Ellis
* Jo Ann Marlowe as Kay Pierce

==Comparison to the novel== thriller and a murder was introduced into the plot.  
 Depression and the Prohibition era, which were important in the novel, are absent from the screenplay.

The plot is simplified and the number of characters reduced.  Vedas training and success as a singer (including her performance at the Hollywood Bowl) were dropped in the film and her music teachers only mentioned in passing. Lucy Gessler, a key character in the novel and Mildreds good friend, is eliminated.  Ida, Mildreds boss at the restaurant where she works as a waitress, is given much of Mrs. Gesslers wise-cracking personality.
 censorship code of that time required evildoers to be punished for their misdeeds.

==Production==
The working title for Mildred Pierce was House on the Sand;   on     Ralph Bellamy, Donald Woods, George Coulouris were considered for the role of Bert, and Bonita Granville, Virginia Weidler and Martha Vickers were considered for Veda.  Scenes for the film were shot in Glendale, California and Malibu, California.  Permission had to be granted permission from the U.S. Army to shoot in Malibu due to wartime restrictions. 

Joan Crawford had been released from Metro-Goldwyn-Mayer two years prior due to a mutual agreement. Crawford campaigned for the lead role in Mildred Pierce, which most lead actresses didnt want, because of the implied age as mother of a teenage daughter. Warner Brothers and director Michael Curtiz originally wanted Bette Davis to play the title role, but she declined. Curtiz did not want Crawford to play the part. Curtiz campaigned for Barbara Stanwyck, who was working on My Reputation (1946) at the time. When he learned that Stanwyck wasnt going to be cast, he tried to obtain Olivia de Havilland or Joan Fontaine play Mildred. He approved Crawfords casting after seeing her screen test. Nevertheless there was tension on the set between the director and the star, with producer Jerry Wald acting as peacemaker.  

==Reception==

===Critical response=== Pasadena heel, a talented performance." 
 Aristotelian warhorse German Expressionistic scored by Max Steiner with a sensational bombast thats rousing even when it doesnt match the quieter, pensive mood of individual scenes, Mildred Pierce is professionally executed and moves at a brisk clip." 

Historian June Sochen (1978) argues the film lies at the intersection of the "weepie" and "Independent Woman" genres of the 1930s and 1940s. It accentuates common ground of the two: women must be submissive, live through others, and remain in the home. 

===Awards and honors===
Wins
* National Board of Review: Best Actress, Joan Crawford; 1945. Best Actress in a Leading Role, Joan Crawford; 1946.

Nominations
* Academy Awards: 1946 Best Actress in a Supporting Role, Eve Arden Best Actress in a Supporting Role, Ann Blyth Best Black-and-White Cinematography, Ernest Haller Best Picture, Jerry Wald Best Screenplay Writing, Ranald MacDougall

National Film Registry 
In 1996, the film was deemed "culturally, historically, or aesthetically significant" and selected for preservation in the United States Library of Congress National Film Registry. 

American Film Institute lists
* AFIs 100 Years...100 Heroes and Villains:
** Veda Pierce – Nominated Villain
* AFIs 100 Years...100 Movie Quotes:
** "Personally, Vedas convinced me that alligators have the right idea. They eat their young." – Nominated
* AFIs 100 Years...100 Movies (10th Anniversary Edition) – Nominated

==Adaptations==
A Mildred Pierce (TV miniseries)|five-part television miniseries of Mildred Pierce premiered on HBO in March 2011, starring Kate Winslet as Mildred, Guy Pearce as Beragon, Evan Rachel Wood as Veda and Mare Winningham as Ida. Separate actresses portray Veda at different ages, as opposed to Ann Blyth alone in the 1945 film.  Wally Fays character in the original has been changed back to the novels Wally Burgan,  and is portrayed by James LeGros. The cast also includes Melissa Leo as Mildreds neighbor and friend, Lucy Gessler, a character omitted from the Crawford version.  The film is told in chronological order with no flashbacks or voice-over narration, and eliminates the murder subplot that was added for the 1945 version.

==DVD release== Dickie Moore, Grand Hotel, The Damned Dont Cry!, and Humoresque.

The Region 1 edition is flipper single disc with "Joan Crawford: The Ultimate Movie Star" documentary and a series of trailer galleries on the reverse of the film.

==References==
Notes
 

Further reading
* Cook, Pam. "Duplicity in Mildred Pierce," in Women in Film Noir, ed. E. Ann Kaplan (London: British Film Institute, 1978), 68–82
* Jurca, Catherine. "Mildred Pierce, Warner Bros., and the Corporate Family," Representations Vol. 77, No. 1 (Winter 2002), pp.&nbsp;30–51    
* Nelson, Joyce. "Mildred Pierce Reconsidered," Film Reader 2 (1977): 65–70
* Robertson, Pamela. "Structural Irony in Mildred Pierce, or How Mildred Lost Her Tongue," Cinema Journal Vol. 30, No. 1 (Autumn, 1990), pp.&nbsp;42–54  
* Sochen, June.  "Mildred Pierce and Women in Film," American Quarterly, Jan 1978, Vol. 30 Issue 1, pp.&nbsp;3–20,  

==External links==
 
 
*  
*  
*  
*  
*  
*  

===Streaming audio===
*   on Lux Radio Theater: June 6, 1949
*   on Lux Radio Theater: June 14, 1954

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 