Clearing the Range
{{Infobox film
| name           = Clearing the Range
| image          = Clearing the Range FilmPoster.jpeg
| caption        = Film poster
| director       = Otto Brower
| producer       = M.H. Hoffman Jr.
| writer         = Jack Cunningham
| screenplay     = Jack Natteford
| starring       = Hoot Gibson Ernest Miller
| editing        = Mildred Johnston
| released       =  
| runtime        = 61 minutes
| country        = United States
| language       = English
}}
 Western film directed by Otto Brower.

== Cast ==
*Hoot Gibson as Curt "El Capitan" Fremont
*Sally Eilers as Mary Lou Moran
*Hooper Atchley as Lafe Kildare
*Robert Homans as "Dad" Moran
*Edward Peil Sr. as Sheriff Jim
*George Mendoza as Juan Conares Edward Hearn as Jim Fremont
*Maston Williams as George "Slim" Allen
*Eva Grippon as Senora Conares

== Soundtrack ==
*George Mendoza – "La Paloma"

== External links ==
* 
* 
 
 
 
 
 
 
 
 
 
 
 
 
 