Minnaminnikoottam
{{Infobox film name           = Minnaminnikoottam image          = Minnaminnikkottam.jpg director  Kamal
|writer Kamal
|producer       = Raghuram starring  Narain Indrajith Indrajith Jayasurya Roma Samvrutha Radhika
|banner         = Neehar films cinematography = Manoj Pillai distributor    = Mulakupadam release released       =   runtime        = budget         = country        = India language       = Malayalam music          = Bijibal
}} Malayalam film Radhika in the lead roles. The films score and soundtrack is composed by Bijibal. The film revolves around a group of youngsters working in an IT park.

==Plot==
Sidharth (Indrajith Sukumaran|Indrajith) and his wife Mumthas (Samvrutha Sunil) are two highly paid IT professionals who are trying to overcome the crisis of their love marriage. They have many friends; Manikkunju (Jayasurya), Abhilash (Narain (actor)|Narain), Charulatha (Meera Jasmine), Rose Mary (Roma (actress)|Roma) and Kalyani (Radhika (Malayalam actress)|Radhika). They visit Sidharthan’s flat for unwinding after work and consider their home an escape from stressful work. Some unusual things happen which change their lives. altogether. 

Abhilash and Charu is a couple who argues and fights and at the end of these simple fights, they come to the home of Sidhu and Momu for chilling out. Charu was living a life that was surrounded only by her father from when she was 5 years old. A wedding meet between the families of Abhi and Charu causes Charus father to start drinking after 15 years. This causes a major break up between Charu and Abhi. On a trip to Chennai, for Manis and Roses marriage and also for Abhis marriage with his fathers friends daughter, Charu runs away to Bangalore and later moves abroad.

The story returns from flash back when Charu leaves for Bangalore. The flashback had started when Charu read an email from Kalyani informing about her marriage. Charu attends the marriage and there she meets the old members of her "Minnaminnikootam". However, none of them shows any friendship with her. She returns to the airport and Abhi comes and sits beside her. He asks for them to go for a drive and takes her to Sidhus house where she is emotionally greeted by her friends and receives the message of surprise. Abhi was never going to marry anyone else. He was planning on marrying Charu even if he didnt have her permission.

==Cast== Narain as Abhilash
*Meera Jasmine as Charulatha Indrajith as Sidhaarth
*Jayasurya as Manikunju  Roma as Rosemary
*Samvrutha as Mumtaz a.k.a. Mummu
*Anoop Chandran as Partha Saradhi a.k.a. Paachan Radhika as Kalyani Saikumar as Charulathas father Janardhanan as Kaliparamban, Charulathas fathers friend
*Mamukkoya as Kunjikannan, Charulathas fathers friend
*T. G. Ravi as Aalikka, Charulathas fathers friend
*Balachandran Chullikkadu as Appukuttan Maash, Charulathas fathers friend
*Ambika Mohan as Abhilashs mother

==Soundtrack== composed by Bijibal, with lyrics by Anil Panachooran. 
 Sayonara Philip, T. R. Soumya    Ranjith Govind, Swetha Mohan  
* "Mizhi Tammil" &mdash; Swetha Mohan  
* "Taara Jaalam" &mdash; Afsal, Sujatha Mohan|Sujatha, Ganesh Sundaram, Cicily, Rakhi R. Nath, Raghuram;   Manjari
* Anitha

==References==
 

==External links==
* 
*http://www.nowrunning.com/Movie/?movie=5252
*http://www.123.com/movie/2008/regional/may/minnaminnekoottam/preview/index.htm
*http://www.musicindiaonline.com/music/malayalam/s/movie_name.9787
*http://sify.com/movies/imagegallery/galleryDetail.php?hcategory=13733817&hgallery=14687659

 

 
 
 