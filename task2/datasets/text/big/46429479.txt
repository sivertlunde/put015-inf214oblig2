Fly Away Solo
 
{{Infobox film
| name           = Fly Away Solo
| image          = 
| caption        = 
| film name      = Masaan
| director       = Neeraj Ghaywan
| producer       = Anurag Kashyap
| writer         = Neeraj Ghaywan Varun Grover
| starring       = {{plainlist|
*Richa Chadda
* Vicky Kaushal
*Shweta Tripathi
* Pankaj Tripathi Sanjay Mishra
*Nikhil Sahni
}}
| music          = 
| cinematography = Avinash Arun Dhaware
| editing        = Nitin Baid
| distributor    = 
| studio         = Phantom Films
| released       =  
| runtime        = 120 minutes
| country        = India
| language       = Hindi
| budget         = 
}}

Fly Away Solo (Hindi title: Masaan) is an upcoming Indian Hindi drama film directed by Neeraj Ghaywan. The directorial debut film is an Indo-French co-production produced by Manish Mundra, Macassar Productions, Anurag Kashyaps Phantom Films, Sikhya Entertainment, Arte France Cinema and Pathé|Pathé Productions.   It has been selected to be screened in the Un Certain Regard section at the 2015 Cannes Film Festival.    Ghaywan previously assisted Anurag Kashyap on Gangs Of Wasseypur.  {{cite web | title = ‘Masaan’, ‘Chauthi Koot’ make it to Cannes Film Festival |work= The Indian Express | url = http://indianexpress.com/article/entertainment/bollywood/masaan-chauthi-koot-make-it-to-cannes-film-festival/ 
|date=April 17, 2015| accessdate = 2015-04-17  }} 

==Plot==
The film set in Varanasi, follows the lives of a young orphan (Nikhil Sahni), a low-caste teenage boy (Vicky Kaushal) who falls in love with an upper-caste girl (Shweta Tripathi), her father (Sanjay Mishra) and a woman (Richa Chadha) caught in a sex scandal. Masaan is Hindi word for crematorium, where the teenage boy works. As their stories collide, in girls father who is fighting a taboo, the young orphan finds an unlikely father figure.    

==Cast==
* Richa Chadda as Devi Pathak
* Vicky Kaushal as Deepak Chaudhary
* Shweta Tripathi as Deepak Chaudhary
* Pankaj Tripathi as Safhya Ji Sanjay Mishra as Vidyadhar Pathak
* Nikhil Sahni
* Vineet Kumar Singh

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 