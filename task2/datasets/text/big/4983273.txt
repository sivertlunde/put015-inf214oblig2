Heroes of Shaolin
 
 
 
 1977 Hong Kong kung fu epic Daai miu si yue siu piu haak. The two-part story was directed by William Chang and co-written by Chang and relatively unknown kung-fu film writer Chang Hsin Yee. Like many kung fu movies from the late 1970s, the main theme of the film focuses on revenge, but the story holds a unique twist that sets it apart from many other films of the era.

==Plot details==

Part one opens with a young boy and his father flying a kite on a secluded beach. The father is soon confronted by a past enemy, who demands a rematch after an embarrassing defeat. The young boy is horror-stricken as, after his fathers defeat, the father takes his own life before the young boys eyes and rushes to avenge his death. The challenger easily defeats the boy, but rather than kill again, he allows the boy to journey with him, offering to teach the boy in martial arts so that he may one day take appropriate revenge.
The boy keeps training under him; and through a series of events, the boy has his revenge but not in the way he intended.its setted in the Yuan Dynasty in the 13th century.

==Cast==
The cast stars Yuen Biao as Sword & Knife man, Sing Chen as Tu Tashan, Jang Lee Hwang as Tien Lung and other supporting actors

==References==
* 

 
 

 