Master of the Flying Guillotine
 
 
 
 |myr=Dú bì chywánwáng dà pwò sywě dī dž|ci= |y=Duhk bei kyùhnwòhng daaih po hyut dìk jí}}
{{Infobox film name           = Master of the Flying Guillotine image          = Masterflyingguillotine.jpg alt            =  caption        = American theatrical release poster traditional = 獨臂拳王大破血滴子 simplified    = 独臂拳王大破血滴子 pinyin        = Dú Bì Quán Wáng Dà Pò Xuě Dī Zǐ jyutping      = Duk6 Bei3 Kyun4 Wong4 Daai6 Po3 Hyut3 Dik1 Zi2}} director  Jimmy Wang producer       = Wong Cheuk-hon writer         = Jimmy Wang Yu starring       = Jimmy Wang Yu Kom Kang Doris Lung Lau Kar-wing Philip Kwok music          = Frankie Chan cinematography = Chiu Yao-hu editing        = Kwok Ting-hung studio         = First Films distributor    = Epoch Entertainment (Cinema Epoch) & Pathfinder Pictures released       =   runtime        = 93 minutes country        = Hong Kong language       = Mandarin budget         =  gross          = 
}} Jimmy Wang Yu, who also wrote and directed the film. It is a sequel to Wangs 1971 film One Armed Boxer, and thus the film is also known as One-Armed Boxer 2 and The One Armed Boxer vs. the Flying Guillotine.   

==Plot== flying guillotine", which resembles a hat with a bladed rim attached to a long chain.  Upon enveloping ones head, the blades cleanly decapitate the victim with a quick pull of the chain.  The Boxers adversary is the assassin Fung Sheng Wu Chi Thai boxer, kobojutsu user.

The One-armed Boxer leaves the tournament and, using a series of traps, defeats the assassins subordinates.  Unable to directly confront the deadly assassin himself, the One-armed Boxer devises a plan that uses misdirection.  Taking advantage of the assassins blindness by using bamboo poles as a lure, each time the blind assassin throws his weapon, it becomes snagged on one of the bamboo poles effectively removing the inner blades of the assassins deadly weapon; however as it still contains a jagged outer edge it is still a formidable weapon.  The One-armed Boxer then proceeds to convert a coffin-makers shop into an elaborate trap.  Once the weapon is finally destroyed, the One-armed Boxer engages the assassin in a duel and defeats him.

==Cast== Jimmy Wang Yu as the One-armed Boxer
*Kam Kong as Fung Sheng Wu Chi
*Doris Lung as Wus daughter Thai boxer
*Lung Fei as Yakuma Indian fighter (The film gives him the name Yogi Tro Le Soung)
*Sit Hon as tournament referee
*Lau Kar-wing as fighter with a three-section staff
*Wong Fei-lung as One-armed boxers student
*Yu Chung-chiu as Wu Chang Sang
*Shan Mao as bamboo cutter
*Wang Tai-lang as Ma Wu Kung, Monkey stylist
*Shih Ting-ken as One-armed boxers student
*Lung Sai-ga as Wang Jiang
*Philip Kwok as Chang Chia Yu
*Lung Fong as Tiger Fists / nose-picking fight
*Sun Jung-chi as Daredevil Lee San
*Wong Lik as Tornado Knives Lei Kung

 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 90% of 20 surveyed critics gave the film a positive review; the average rating was 6.9/10.   Metacritic rated the film 57/100 based on eleven reviews.   Elvis Mitchell of The New York Times called it "near-great" and "a venerable example of the kung fu genre".     Kevin Thomas of the Los Angeles Times wrote, "Master of the Flying Guillotine has been called the Holy Grail of the Hong Kong martial arts movies of the 70s, and now that it has been lovingly restored and given a regular theatrical release, its easy to see why."   Joey OBryan of The Austin Chronicle rated it 2/5 stars and called it "a mess" that fails to live up to the epic brawl promised by the alternate title.   Nathan Rabin of The A.V. Club called it "a delirious kung-fu saga" that is "wild even by the genres lenient standards".  Rabin concludes, "Goofy Z-movie fun of the highest order, Master Of The Flying Guillotine needs to be seen to be believed, and even then defies belief."   Phil Hall of Film Threat rated it 1.5/5 stars and wrote, " his silly production stands as a dinky reminder of why martial arts film fell out of favor during the mid-1970s".   J. Doyle Wallis of DVD Talk rated it 4/5 stars and called it "a complete guilty pleasure that leaves you feeling high off its empty b-movie fun".     Mike Pinsky of DVD Verdict wrote that the film toys with and subverts many martial arts film cliches, which makes it surprising and entertaining. 

==Legacy== Bushido Brown. 

===Prequel===
In 1977, a prequel called Fatal Flying Guillotine was made by Hong Kong director Raymond Liu. 

==References==
 

==External links==
* 
 
* 
* 
*  at Metacritic

 

 
 
 
 
 
 
 
 
 
 
 
 