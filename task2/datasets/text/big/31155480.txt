Kusruthykuttan
{{Infobox film 
| name           = Kusruthykuttan
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = Muhammad Assam
| writer         = Thikkurissi Sukumaran Nair (dialogues)
| screenplay     = Thikkurissi Sukumaran Nair Ambika Suresh Varma Sukumari
| music          = Vijayabhaskar
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by Muhammad Assam. The film stars Thikkurissi Sukumaran Nair, Ambika Sukumaran|Ambika, Suresh Varma and Sukumari in lead roles. The film had musical score by Vijayabhaskar.    This film is inspired by the Bollywood flick Deeksha.

==Cast==
*Thikkurissi Sukumaran Nair as Madhavan Nair Ambika as Lakshmi
*Suresh Varma as Gopi
*Sukumari
*Adoor Bhasi
*Muthukulam Raghavan Pillai
*BN Nambiar
*Pankajavalli as Lakshmis mother
*Suresh Kumar

==Soundtrack==
The music was composed by Vijayabhaskar and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ammaye Kalippikkaan || S Janaki || P. Bhaskaran || 
|-
| 2 || Ammaye Kalippikkaan   || S Janaki, B Vasantha || P. Bhaskaran || 
|-
| 3 || Ammaye Kalippikkan || S Janaki, B Vasantha || P. Bhaskaran || 
|-
| 4 || Manichilambe || B Vasantha || P. Bhaskaran || 
|-
| 5 || Punnellu Koythallo || S Janaki, PB Sreenivas || P. Bhaskaran || 
|-
| 6 || Raareero Unni || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 