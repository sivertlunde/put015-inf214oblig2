The End (1978 film)
{{Infobox film
| name           = The End
| image          = The End (1978).jpg
| image size     = 225px
| caption        = Theatrical release poster
| director       = Burt Reynolds Lawrence Gordon
| writer         = Jerry Belson
| narrator       =
| starring       = Burt Reynolds Dom DeLuise Paul Williams
| cinematography = Bobby Byrne
| editing        = Donn Cambern
| distributor    = United Artists
| released       =  
| runtime        = 100 min
| country        = United States
| language       =
| budget         =
| gross          = $44,917,151 
}} black comedy-buddy film, directed by and starring Burt Reynolds.
 Pat OBrien, Paul Williams.

==Plot==

California real-estate salesman Wendell "Sonny" Lawson (Burt Reynolds) is informed by his doctor (Norman Fell) that hes dying from a rare toxic blood disease and has six months to live. Not wanting to endure the inevitable pain, nor wanting to spend his last days in a hospital bed, he decides to take his own life.
 Pat OBrien) are too absorbed in their own problems to pay him any attention. 

After a failed suicide attempt with pills, Lawson ends up at a psychiatric hospital. There he befriends a disturbed fellow patient, Marlon Borunki (Dom DeLuise), who agrees to help in Sonnys quest to end it all.  What follows are several more botched suicide attempts, including Sonny trying to hold his breath, crack open his head with his adjustable hospital bed, jump off a tower, and hang himself, all with Marlons inept attempts to help.

Sonny finally decides to escape from the hospital and do it on his own. He drives to a secluded beach, swims out into the ocean and intends to drown. But as he sinks, Sonny begins feeling guilty of abandoning his daughter. Now with a passionate will to live, Sonny swims back, making promises to God along the way that he begins to break as soon as he gets close to shore.

Relieved to be alive, Sonny discovers to his horror that Marlon has followed him to the beach, knowing Sonny would chicken out. Marlon begins shooting a gun at him, thinking he is doing his friend a favor. Narrowly escaping, Sonny is finally able to convince a befuddled Marlon that he wants to live.

The two of them happily walk along the beach, but Marlon suddenly pulls out a knife and chases Sonny down the shoreline. For one or both men, this could be The End. 

==Cast==
*Burt Reynolds as Wendell Sonny Lawson 
*Dom DeLuise as Marlon Borunki 
*Sally Field as Mary Ellen 
*Strother Martin as Dr. Waldo Kling 
*David Steinberg as Marty Lieberman 
*Joanne Woodward as Jessica Lawson 
*Norman Fell as Dr. Samuel Krugman 
*Myrna Loy as Maureen Lawson 
*Kristy McNichol as Julie Lawson  Pat OBrien as Ben Lawson 
*Robby Benson as Father Dave Benson 
*Carl Reiner as Dr. James Maneet 
*Louise LeTourneau as Receptionist 
*Bill Ewing as Hearse Driver 
*Robert Rothwell as Limousine Driver
*James Best as Pacemaker Patient

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 