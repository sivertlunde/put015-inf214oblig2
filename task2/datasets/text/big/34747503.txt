Crazy Loka
{{Infobox film
| name = Crazy Loka
| image = Crazy Loka poster.jpg
| caption = Film poster
| director = Kavita Lankesh
| producer = Confident Group
| writer = Duniya Soori
| screenplay = 
| story = 
| narrator =  Ravichandran Daisy Bopanna Harshika Poonacha
| music = Manikanth Kadri
| cinematography = A.C. Mahendran
| editing = Jony Harsha
| studio = 
| distributor = 
| released = 8 June 2012
| runtime = 
| country =  
| language = Kannada
| budget = 
| gross = 
}}
 Kannada film starring V. Ravichandran and Daisy Bopanna in the lead roles. The film is directed by Kavita Lankesh and produced by Confident Group and Shanti Pictures. Manikanth Kadri is the music director for the film.  
 

== Cast == Ravichandran as Basavaraj Kattimane
* Daisy Bopanna as Sarala
* Bharathi Vishnuvardhan
* Harshika Poonachha as Chandini 
* Vijay Suriya as Abhay Avinash as Ganapathi 
* Neenasam Ashwath as Harihara 
* Ramya (Special appearance)

== Soundtrack ==
Music composer Manikanth Kadri has composed 7 songs.  Actress Ramya has made a special appearance in the song "Ghalabe Ghalabe" performed by Chaitra H. G..

{{Infobox album  
| Name        = Crazy Loka
| Type        = Soundtrack
| Artist      = Manikanth Kadri
| Cover       = 
| Alt         = 
| Released    = 
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Anand Audio
}}
{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Ele Elege
| lyrics1 	= 
| extra1        = Rajesh Krishnan, Aishwarya Majumdar
| title2        = Dont Worry
| lyrics2 	= 
| extra2        = Manikanth Kadri, Aishwarya Majumdar, Hemanth
| title3        = Ele Elege
| lyrics3       = 
| extra3 	= Aishwarya Majumdar
| title4        = Ghalabe Ghalabe
| extra4        = Chaitra H. G.
| lyrics4 	= 
| title5        = Naavu Aaramagiddare
| extra5        = Benny Dayal, Anitha Janani
| lyrics5 	= 
| title6        = Kabab Me Haddi Tippu
| lyrics6       = 
| title7        = Ele Elege
| extra7        = Vijay Prakash
| lyrics7       =
}}
 

==References==
 

 
 
 


 