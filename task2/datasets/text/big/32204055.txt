My Girl Tisa
{{Infobox film
| name           = My Girl Tisa
| image          = 
| image_size     = 
| caption        = 
| director       = Elliott Nugent
| writer         = Allen Boretz
| narrator       = 
| starring       = Lilli Palmer Sam Wanamaker
| music          = Max Steiner
| cinematography = Ernest Haller
| editing        = Christian Nyby
| studio         = 
| distributor    = Warner Bros.
| released       = February 7, 1948
| runtime        = 95 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
My Girl Tisa is a 1948 film directed by Elliott Nugent. It stars Lilli Palmer and Sam Wanamaker.   

==Cast==
*Lilli Palmer as Tisa Kepes 
*Sam Wanamaker as Mark Denek
*Akim Tamiroff as Mr. Grumbach
*Alan Hale, Sr. as Dugan
*Hugo Haas as Tescu

==References==
 

==External links==
* 

 

 
 
 