Naveena Saraswathi Sabatham
{{Infobox film
| name           = Naveena Saraswathi Sabatham
| image          = Naveena Saraswathi Sabatham.jpg
| alt            =  
| caption        = Theatrical poster
| director       = K. Chandru
| producer       = Kalpathi S. Aghoram Kalpathi S. Ganesh Kalpathi S. Suresh
| story          = K. Chandru
| screenplay     = K. Chandru
| based on       = Jai Niveda Thomas
| music          = Prem Kumar Kannan (score)
| cinematography = Anand Jeeva
| editing        = T. S. Suresh
| studio         = AGS Entertainment
| distributor    =
| released       = 29 November 2013
| runtime        = 149 minutes
| country        = India Tamil
| budget         =  
| gross          =   http://superwoods.com/news-id-naveena-saraswathi-sabatham-18-day-collection-report-collection-17-12-139697.htm 
}}
 Tamil contemporary Sathyan and Rajkumar.    Niveda Thomas was paired opposite Jai.  The film was produced by Kalpathi S Aghoram and has cinematography by Anand Jeeva; songs were composed by Prem Kumar.  The film went on floors on November 29, 2013.The film received mixed reviews from critics,however it was above average at box office.    It is not a remake of the 1966 mythological film Saraswathi Sabatham. 

==Plot==

The Story begins in a modern heaven, where Lord Paramasivan (Subbu Panchu) gives orders to Narada (Manobala) to pick 4 people for his Thiruvilaiyadal to make the world realise the ill effects of smoking and drinking. The candidates picked are Ramarajan (Jai (actor)|Jai), a Siddha Doctor, Ganesh (VTV Ganesh), a helpless man who is the husband of a local don, Gopi (Sathyan Sivakumar), a 
future politician and son of a selfish politician and Krishna (Rajkumar), a budding actor. How Does Paramasivan carry out his Thiruvillaiyadal and how do these four overcome it forms the rest of the story.

== Cast == Jai as Ramarajan
* Niveda Thomas as Jayashree
* VTV Ganesh as Ganesh Sathyan as Gopi Rajkumar as Krishna
* Manobala as Narada Paramasivan
* Devadarshini as Parvati Devi
* Ramya as Saraswati Devi
* Master Mathusuthanan as Murugan
* Vishal Venkat
* Chitra Lakshmanan as Kovai Kamaraj
* R. S. Sivaji as Jayashrees Father
* Swaminathan as MLA Ekambaram
* Badava Gopi as Rajendran
* Rekha Suresh as Mrs. Ekambaram
* T. R. Latha as Jayshrees Grandmother
* Raj Kamal as Jayashrees Brother
* Emey as Sorna Akka
* Venkat Prabhu as Himself Sam Anderson as Kumar (cameo appearance)
 

== Production ==
The film went on floors on February 26, 2013.  Chandru shot the first scene in Villivakkam, Chennai.  Though the film is set in Chennai, the director said that a minor portion will be shot abroad.  In June 2013 VTV Ganesh left to Malaysia for the shoot of Saraswathi Sabatham with Jai.  In July, about 80% of the film shooting had been wrapped. 

=== Title controversy === 1966 mythological film starring Sivaji Ganesan, and fans stated they would not allow the usage of titles of their idol for comedy films and that they would protest in front of Jai and Sathyans houses, if the makers proceeded with the same title.  A notice, demanding that the film should be renamed, was sent to film producer Kalpathi Agoram and his brothers Ganesh and Suresh, on behalf of the Nellai City Sivaji social welfare organisation by advocate Kamaraj.  On September 25, 2013, it was announced that the film had been retitled as Naveena Saraswathi Sabatham. 

The first look poster of the film was revealed on 14 April 2013. 

== Soundtrack ==
{{Infobox album
| Name      = Naveena Saraswathi Sabatham
| Longtype  = to Naveena Saraswathi Sabatham
| Type      = Soundtrack
| Genre     = Film soundtrack
| Artist    = Prem Kumar
| Producer  = Prem Kumar Tamil
| Label     = Think Music
| Length    = 31:50
| Released  = 13 October 2013
}}

The soundtrack album was composed by Prem Kumar. The lyrics were written by Vairamuthu, Madhan Karky and Gaana Bala. The music was released on 13 October 2013. The track Kaathirundhai Anbe was released earlier as a single.

;Tracklist
{{track listing
| extra_column    = Singer(s)
| lyrics_credits  = yes
| total_length    = 31:50

| title1      = Kaathirundhai Anbe
| extra1      = Chinmayi, Nivas, Abhay Jodhpurkar
| lyrics1     = Vairamuthu
| length1     = 06:03

| title2      = Saturday Fever
| extra2      = Vijay Prakash, Sayanora Philip, UV Rap
| lyrics2     = Madhan Karky
| length2     = 04:46

| title3      = Vaazhkai Oru
| extra3      = Gaana Bala
| lyrics3     = Gaana Bala
| length3     = 04:36

| title4      = Nenjankuzhi
| extra4      = Karthik (singer)|Karthik, Pooja Vaidyanath
| lyrics4     = Vairamuthu
| length4     = 05:49

| title5      = Nenjankuzhi&nbsp;— Ver 2
| extra5      = Karthik, Pooja Vaidyanath
| lyrics5     = Vairamuthu
| length5     = 05:20

| title6      = Nenjankuzhi
| extra6      = Instrumental
| lyrics6     = 
| length6     = 05:16
}}

==Release==
The satellite rights of the film were sold to STAR Vijay. The film was given a "U" certificate by the Indian Censor Board.

==Critical Reception==
Naveena Saraswathi Sabatham received mixed reviews from critics.  Baradwaj Rangan wrote, "Very occasionally, a line or a sight gag makes you smile, like the one with the roadside idli seller with a signboard that announces “pizza” and “burger.” Otherwise, it’s all very exhausting".  Indiaglitz said, "Motive is in place perfectly; however it has taken to long to be conveyed. Revolving around four men almost all the time, the film could have avoided a few scenes that occupied time. The 1966 flick has certainly influenced Naveena Saraswathi Sabatham he arriving at its climax twist, but has got nothing in connection with it otherwise." and added, "In all, the film is a brand new concept, with a contemporary message and a revolutionary screenplay, although it ultimately ends in an old fashion. A clean entertainer by nature, Naveena Saraswathi Sabatham is old wine in a not-exactly-new but quite a fancy bottle."  Sify called it disappointing and wrote, "The film has no basic logic or reason and seems to have been made with the only intention of trying to tickle the funny bone of the viewers, without any content" and added "There is a scene in the second half of the film where Naradar tells Lord Siva that the story isnt progressing fast, people will be posting on Facebook and Twitter that the first half of the film is super while the second half is Mokkai! It sort of sums up NSS".  The Times of India gave 2.5 stars out of 5 and pointed out the same as Sify, "There is a scene in the second half in which Naradar tells Lord Siva that the story isnt progressing and by this time, people will be posting on Facebook and Twitter that the first half of the film is super while the second half is mokkai...Sadly, it is also the most profound statement in the entire film — not only on the audiences of today but also on the films we get these days, including this one, which just turns dreary, minutes after we enter the second half". 
 IANS gave 2 out of 5 stars and wrote, "The humour is stale and has been used for years now. The film meanders at a snails pace and becomes extremely tedious in the second half. He (Chandru) throws in what are supposedly a few funny incidents that hardly evoke any laughter, forget entertainment. This is not even a film you can force yourself to watch because you have paid money. Its gods way of punishing us for all the bad we have done in our lives".  Talking about the performances, Behindwoods said, "VTV Ganesh is the biggest takeaway from the movie and he moves the second half forward with his antics on the island. The portions where he speaks chaste Tamil in his hoarse voice are a riot. Jais shrill voice has been his USP all along and he comfortably delivers what was expected." and added "Sathyan and Rajkumar as the other two friends play their roles with ease and fit in with the group. Niveda Thomas appears in exactly two scenes and two songs!" 

===Box office===
NSS opened average collected   8.3 crore in first weekend at the box office.The film collected   1.30 crore in first weekend in Chennai alone,  opening at first position at the Chennai box office ahead of the other new releases.  The film first week collection was good but after that it was all downhill.

== References ==
 

==External links==
*  

 
 
 
 
 
 
 
 