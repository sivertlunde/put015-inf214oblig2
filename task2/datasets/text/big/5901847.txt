Bhuvan Shome
{{Infobox film
| name           = Bhuvan Shome
| image          = Bhuvan Shome.jpg
| image_size     = 
| caption        = 
| director       = Mrinal Sen
| producer       = Mrinal Sen Productions
| writer         = Balai Chand Mukhopadhyay
| narrator       = Amitabh Bachhan
| starring       = Utpal Dutt Suhasini Mulay
| music          = Vijay Raghav Rao
| cinematography = K. K. Mahajan
| editing        = 
| distributor    = 
| released       =   
| runtime        = 96 minutes
| country        = India Hindi
| budget         = 
}} Hindi drama film directed by Mrinal Sen. The cast includes Utpal Dutt (Mr. Bhuvan Shome) and Suhasini Mulay (Gauri, a village belle). Sen based his film on a Bengali story by Banaphool (Balai Chand Mukhopadhya).  The film is considered a landmark in modern Indian cinema. 

This was the debut film of Suhasini Mulay.

==Plot==

Bhuvan Shome, a widower and a dedicated civil servant&nbsp;— strict, uncompromising – takes a holiday which transforms him irreparably though at the same time heightening his sense of isolation.

Bhuvan Shome is a "big officer" in the Indian Railways. The background of the film is constructed in the context of a few railway ticket checkers discussing him as a strict, unreasonable officer ("afsar"). It continues with him being described, by a narrator, as a man whose "Bengali"-ness has not been affected by his travels.

His apparent age, late 50s, is an important element of his psychology.

Inspired by hunting, Bhuvan Shome is seen to take a "hunting holiday" to Gujarat. It is quite clear that his expedition is amateurish. He is portrayed as an inept "hunter" rather than a man who knows how to acquire a skill.

His encounter with the young Gouri is fortuitous because it is she who takes care of him and helps "hunt" birds. She helps him through a barren wilderness, takes him home and takes care of him. When he is made to change his clothes because otherwise the "birds will know" and fly away is probably an important part of his transformation from a strict, conformist and aging man to one of a person more open to the stimuli of his environment.

The subsequent hunting sojourn of Gouri and Bhuvan Shome is a lyrical exploration of Bhuvan Shomes transformation. He is not only enamored by the simple beauty of Gouri, but also enchanted by the sights of birds on the lake and in the sky.

His hunt is "successful," but only in a way that reflects Bhuvan Shomes limitations as a man.

Bhuvan Shome is deeply affected by Gouri, who is actually schooled more than she could expect to be in that environment.

When he returns to his office chambers, he is seen to reprieve an offending railwayman. This is a sub-plot that completes the story and context of the initial narration.

==Awards==

* National Film Award for Best Feature Film National Film Award for Best Director - Mrinal Sen
* National Film Award for Best Actor - Utpal Dutt

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 