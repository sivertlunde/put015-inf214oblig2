Kane no naru oka: Dai san hen, kuro no maki
{{Infobox film
| name = Kane no naru oka: Dai san hen, kuro no maki
| image = Kane no naru oka Dai san hen kuro no maki poster.jpg
| image_size = 
| caption = Theatrical poster for Kane no naru oka: Dai san hen, kuro no maki (1949)
| director = Keisuke Sasaki 
| producer = Shōzaburō Yamaguchi
| writer = Kazuo Kikuta Ryōsuke Saitō Sadao Nakamura
| narrator = 
| starring = Keiji Sada
| music = Yūji Koseki
| cinematography = 
| editing = 
| distributor = Shochiku
| released = November 17, 1949
| runtime = 
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1949 Japanese film and the final installment of a wartime film trilogy, directed by Keisuke Sasaki, featuring the cast from the previous two Ringing Bell Hill films.

==Production Notes==
The success of the Ringing Bell Hill trilogy spawned a weekend radio series of 15-minute episodes, produced by NHK, featuring further adventures of a demobilized soldier and a group of war orphans under his care on Ringing Bell Hill. 

==List of a film trilogy==

# 鐘の鳴る丘 第一篇 隆太の巻 (Kane no naru oka: dai-ichi hen, ryūta no maki; Slope of the Ringing Bell Hill: the first volume) (1948)
# 鐘の鳴る丘 第二篇 修吉の巻 (Kane no naru oka: dai-ni hen, shūkichi no maki; Slope of the Ringing Bell Hill: the second volume) (1949)
# 鐘の鳴る丘 第三篇 クロの巻 (Kane no naru oka: dai-san hen, kuro no maki; Slope of the Ringing Bell Hill: the third volume) (1949)

==Cast==
* Keiji Sada
* Taeko Takasugi Masao Inoue
* Michiko Namiki
* Tōdō Gekidan

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 