For a Few Dollars More
{{Infobox film
| name           = For a Few Dollars More  (Per qualche dollaro in più) 
| image          = Forafewdollarsmore.jpg
| caption        = American film poster by Mitchell Hooks
| director       = Sergio Leone
| producer       = Alberto Grimaldi
| screenplay     =   Fernando Di Leo English Version: Luciano Vincenzoni  
| story          = Sergio Leone Fulvio Morsella
| starring       = Clint Eastwood Lee Van Cleef Gian Maria Volonté
| music          = Ennio Morricone
| cinematography = Massimo Dallamano
| editing        =   Giorgio Serrallonga
| studio         = Produzioni Europee Associati (PEA) Arturo González Producciones Cinematográficas Constantin Film
| distributor    = PEA   United Artists  
| released       =  
| runtime        = 132 minutes
| country        = Italy West Germany Spain Italian English English
| budget         = $600,000 Hughes, p. 8  
| gross          = $15 million 
}} Italian spaghetti western film directed by Sergio Leone and starring Clint Eastwood, Lee Van Cleef, and Gian Maria Volonté.  German actor Klaus Kinski also plays a supporting role as a secondary villain. The film was released in the United States in 1967 and is the second part of what is commonly known as the Dollars Trilogy, following A Fistful of Dollars and preceding The Good, the Bad and the Ugly.

==Plot==
 
The Man with No Name—"Manco"— and Colonel Douglas Mortimer—the "Man in Black"—are two bounty hunters in pursuit of "El Indio," one of the most wanted fugitives in the Wild West, and his gang. El Indio is ruthless, clever, and brutal. He has a musical pocketwatch that he plays before engaging in gun duels. "When the chimes finish, begin," he says. Flashbacks reveal that El Indio took the watch from a young woman. El Indio found her with her lover (in Joe Millards novelization of the film, her newly-wed husband), killed him, and raped her; she killed herself while being raped. There is a photograph of the woman inside the cover of the watch; it was a gift from her lover.
 White Rocks. see below for an explanation).

Manco rides into White Rocks and finds Cavanagh in a saloon playing five-card draw poker. Manco kills him and his men, and collects the bounty.
 El Paso and, after butting heads, decide to team up to take down El Indio and his gang.

El Indios next target is the Bank of El Paso and its disguised safe containing "almost a million dollars." Mortimer persuades a reluctant Manco to join El Indios gang for the robbery in order to "get him between two fires." Manco rescues one of Indios friends from prison and is admitted to the gang.

El Indio and his gang invade the bank and carry off the safe, which they cant open. They ride to the small border town of Agua Caliente, where Mortimer is waiting. One of El Indios men, the hunchback Wild, recognizes Mortimer from a previous encounter in which Mortimer had deliberately insulted him. He forces a showdown and Mortimer kills him.

Mortimer then proves his worth to El Indio by cracking open the safe without using explosives. El Indio then says he will wait a month before dividing the loot to allow the furor over the bank robbery to die down and locks the money away.

Manco and Mortimer attempt to steal the bank money from El Indio, but are caught in the act. El Indios men severely beat them and tie them up. Later, El Indios right-hand man Nino, on orders from El Indio, kills their guard and releases them. El Indio informs his gang that Manco and Mortimer "got away", and sends the gang in pursuit. He intends to kill off his gang and Manco and Mortimer, while he and Nino take all the loot for themselves. However, one of the more intelligent members of the gang, Groggy, figures out what El Indio is up to, and kills Nino. Before he kills El Indio, he finds that Mortimer has already removed the stolen money from where El Indio had hidden it. El Indio convinces Groggy to join forces with him to trap Manco and Mortimer.
 
 1854 Jennings Rifle Company Volcanic Rifle). He forces El Indio to wait while he gives his own gunbelt and pistol to Mortimer, evening the odds.

"Now we start," Manco announces, and sits down while Mortimer and El Indio face off, with the watch playing again. During the standoff, Manco looks in Mortimers pocketwatch and sees the same photo as in El Indios watch. The music finishes, and Mortimer outdraws and guns down El Indio.

Mortimer takes El Indios pocketwatch. Manco gives him back the other watch and remarks on a family resemblance to the photo. "Naturally, between brother and sister," Mortimer answers. His revenge complete, Mortimer declines a share of the bounties. Manco tosses the bodies of El Indio and his men into a wagon, adding up the bounties, and finds he is short of the $27,000 total. He spins around to gun down Groggy, who had survived and was sneaking up behind him. As he leaves, he recovers the money stolen from the Bank of El Paso, though it is not clear whether he intends to return it. He then rides off into the distance with two horses towing the wagon.

==Cast==
===Leads===
* Clint Eastwood as Manco (the "Man with No Name"):
:In the English-dubbed version of the film, Eastwoods character is said to "go by the name of Manco."  "Manco" means "one-armed" and "lame of one hand" in Spanish and "limp" in Portuguese; Eastwoods character performs nearly all actions using only his left hand, to leave free his right hand, with which he draws. His behavior thus bears a joking resemblance to that of a one-armed man.

:In the original Italian version, Eastwoods characters sobriquet is "Monco", the Italian equivalent of the word "manco". Thus in many written sources,  the Man with No Name is called Monco, due to the Italian form. In any case, the English-dubbed voices of the films characters seemingly pronounce "Manco" when they refer to him.

* Lee Van Cleef as Colonel Douglas Mortimer:
:Colonel Douglas Mortimer is a rival bounty hunter, though he is much older than Eastwoods character: "almost fifty years of age." Manco, Clint Eastwoods character, travels to visit a man known as "The Prophet" early in the movie to find out all he can about his rival. "The Prophet" explains Colonel Douglas Mortimer to have "once been a great man, a soldier" and "the finest shot in the Carolinas. Now hes reduced to being a bounty killer same as you." Unlike Manco, Mortimers motivation throughout the movie is not the bounty over El Indio and his gang, but vengeance for the death of Mortimers sister many years before, who killed herself while being raped by Indio.  During an encounter with El Indio in the movie, Mortimer exclaims, "This is Colonel Mortimer, Douglas Mortimer.  Does the name mean anything to you?"  Having seen the death of Indio, Mortimer leaves all of the bounty to be collected by Manco at the end of the movie. Mortimer says to Manco, after being questioned by Manco about the bounty, "Its all for you, I think you deserve it."  Mortimer rides off alone at the end, as his purposes were then completed.

* Gian Maria Volonté as El Indio:
 
 marijuana to ease the intensity of the memory (it has been claimed that Indios use of the drug makes this the first use of marijuana by a character in a major film production).  El Indio has a gang of fourteen men who rob the bank in El Paso. Although he is otherwise completely insensitive and ruthless, the act of killing Mortimers sister distresses him to an ascertainable degree, as it lingers in his memory profoundly; notable in most Sergio Leone films is there being antagonistic or authoritarian characters having a strange psychopathic behaviour.

===Indios gang===
* Mario Brega as Niño
* Luigi Pistilli as Groggy
* Aldo Sambrell as Cuchillo
* Klaus Kinski as Wild, the hunchback
* Benito Stefanelli as Hughie (a.k.a. Luke) 
* Luis Rodríguez as Manuel
* Panos Papadopulos as Sancho Perez
* Werner Abrolat as Slim (uncredited)
* Eduardo García as Fausto (uncredited)
* Enrique Santiago as Miguel (uncredited)
* Antonio Molino Rojo as Frisco (uncredited)
* Frank Brana as Blackie (uncredited)
* José Canalejas as Chico (uncredited)
* Nazzareno Natale as Paco (uncredited)

===Other characters===
* Dante Maggio as Carpenter in cell with El Indio
* José Terrón as Guy Calloway 
* Diana Rabito as Calloways girl in tub
* José Marco as  Red "Baby" Cavanagh
* Antoñito Ruiz as Fernando, Mancos El Paso informant
* Giovanni Tarallo as Santa Cruz telegraphist
* Joseph Egger as Old Prophet
* Lorenzo Robledo as Tomaso, El Indios traitor
* Mara Krupp as Mary, hotel managers wife
* Mario Meniconi as Train conductor
* Roberto Camardiel as Station clerk
* Sergio Mendizábal as Tucumcaris bank manager
* Tomás Blanco (actor)|Tomás Blanco as Tucumcaris sheriff
* Carlo Simi as El Pasos bank manager (uncredited)
* Rosemary Dexter as Mortimers Sister (uncredited)
* Peter Lee Lawrence as Mortimers Brother-in-Law (uncredited)
* Sergio Leone as Whistling bounty hunter at the beginning of the film (voice, uncredited) 

==Production==
===Development===
 
After the box-office success of   was again approached for a starring role but he passed, citing that the sequels script was like the first film.  Instead, Lee Van Cleef accepted the role. Eastwood received $50,000 for returning in the sequel, while Van Cleef received $17,000. 

Screenwriter Luciano Vincenzoni wrote the film in nine days.  However, Leone was dissatisfied with some of the scripts dialogue, and hired Sergio Donati to act as an uncredited script doctor.   

===Production===
The film was shot in  .  The town of Agua Caliente, where Indio and his gang flee after the bank robbery, is Albaricoques, a small "pueblo blanco" on the Níjar plain.

===Post-production=== Southern accent. Gian Maria Volonté also provided his voice for the English dub of the film. However, because he did not speak English, he had to read his lines phonetically with the aid of a translator. Sir Christopher Frayling, For a Few Dollars More audio commentary. Retrieved on 1 June 2014. 

===Music=== musical score diegetic and non-diegetic moments through a reoccurring motif that originates from the identical pocket watches belonging to El Indio and Colonel Mortimer.  "The music that the watch makes transfers your thought to a different place," said Morricone. "The character itself comes out through the watch but in a different situation every time it appears." 

{{Infobox album |  
| Name        = For a Few Dollars More
| Type        = Soundtrack
| Artist      = Ennio Morricone
| Cover       =
| Released    = 1965  (Original album) 
| Recorded    =
| Genre       = Soundtrack
| Length      =
| Label       = RCA Italiana
| Producer    =
| Last album  = Se non avessi più te (1965)
| This album  = For a Few Dollars More (1965)
| Next album  = Idoli controluce (1966)
}}
A soundtrack album was originally released in Italy by RCA Italiana.  In the United States, Hugo Montenegro released a cover version as did Billy Strange and Leroy Holmes who released a cover version of the soundtrack album with the original American poster art. Maurizio Graf sang a vocal "Occhio Per Occhio"/"Eye For An Eye" to the music of the cue "Sixty Seconds to What" track that did not appear in the film but was released as a tie-in 45rpm record.

{{Tracklist
| headline    = Track listing
| all_writing = Ennio Morricone
| title1      = La Resa Dei Conti
| length1     = 3:06
| title2      = Poker DAssi
| length2     = 1:17
| title3      = Osservatori Osservati
| length3     = 2:06
| title4      = Il Vizio Di Uccidere
| length4     = 2:26
| title5      = Carillon
| length5     = 1:14
| title6      = Il Colpo
| length6     = 2:25
| title7      = Addio Colonnello
| length7     = 1:47
| title8      = Per Qualche Dollaro In Più
| length8     = 2:52
}}

==Release and reception==
For a Few Dollars More was released in Italy in December 1965 as Per Qualche Dollaro in Piu. Hughes, p.10  In the United States, the film debuted on 10 May 1967, four months after the release of A Fistful of Dollars, grossing $5 million. 

At the time of its Italian release, the film proved to be even more commercially successful than its predecessor.  By 1967, the film became the highest-grossing film of any nationality in the history of Italian cinema.  Although, it initially received mediocre reviews from critics. Bosley Crowther of The New York Times said, "The fact that this film is constructed to endorse the exercise of murderers, to emphasize killer bravado and generate glee in frantic manifestations of death is, to my mind, a sharp indictment of it as so-called entertainment in this day."  Roger Ebert of the Chicago Sun-Times described the film as "one great old Western cliché after another" and that the film "is composed of situations and not plots." 

The film has since grown in popularity, while also gaining more positive feedback from contemporary critics. The review aggregator website Rotten Tomatoes reports a 94% approval rating with an average rating of 7.8/10 based on 33 reviews. The websites consensus reads, "With Clint Eastwood in the lead, Ennio Morricone on the score, and Sergio Leones stylish direction, For a Few Dollars More earns its recognition as a genre classic." 
 Den of Geek said, "For A Few Dollars More is often overlooked in the trilogy, awkwardly sandwiched between both the original film and the best-known, but its a stunning film in its own right."  Paolo Sardinas of MovieWeb said, "Eastwood gives it his all and turns in another iconic performance along with scene stealer Lee Van Cleef, who helps make For a Few Dollars More twice as good as its predecessor." 

==In popular culture==
*Lando Buzzanca parodied the film in For a Few Dollars Less (1966). 
*An episode of Disneys Adventures of the Gummi Bears, "For A Few Sovereigns More", references the film with its title and the name and portrayal of its prime antagonist, a bounty hunter named Flint Shrubwood. The bounty hunter is hired by the villain, Duke Igthorn, to capture one of the titular characters, but captures two and releases one, saying he was only contracted for one. When Igthorn double-crosses him, he ends up facing down Shrubwood, the bag of gold between them. Shrubwood says icily, "Go ahead...take my pay," a nod to Eastwoods other signature character Dirty Harry. Igthorn flees as Shrubwood collects the gold.  

==References==
 

==Bibliography==
*  
*  

==External links==
 
* 
* 
* 
*  at the Spaghetti Western Database
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 