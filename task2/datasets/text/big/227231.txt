Peter's Friends
 
 
{{Infobox film
| name           = Peters Friends
| caption        = 
| image	         = Peters Friends FilmPoster.jpeg
| director       = Kenneth Branagh 
| producer       = Kenneth Branagh
| writer         = Rita Rudner Martin Bergman
| starring       = Kenneth Branagh Stephen Fry Hugh Laurie Imelda Staunton with Tony Slattery and Emma Thompson
| cinematography = Roger Lanser
| editing        = Andrew Marcus
| studio         = Channel Four Films
| distributor    = Samuel Goldwyn Company
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = $5 million
| gross          = $4,058,564 (US)
}} comedy film written by Rita Rudner and her husband Martin Bergman, and directed and produced by Kenneth Branagh.

The film follows six friends in an acting troupe who graduated from Cambridge University in 1982 and went their separate ways. Ten years later, Peter (Stephen Fry) inherits a large estate from his father, and invites the rest of the gang to spend New Years holiday with him. Many changes have taken place in the lives of all the friends assembled, but Peter has a secret that will shock them all.

==Plot==
It is New Years weekend and the friends of Peter (Fry) gather at his newly inherited country house. Ten years ago, they all acted together in a Cambridge University student comedy troupe. Since then they have gone in different directions and various career paths.

Peters friends are Andrew (Branagh), now a writer in Hollywood; married jingle writers Roger (Laurie) and Mary (Staunton); glamorous costume designer Sarah (Emmanuel); and eccentric Maggie (Thompson), who works in publishing. Cast in sharp relief to the six university chums are Carol (Rudner), the American TV star wife of Andrew; and loutish Brian (Slattery), Sarahs very recently acquired lover. Law plays Peters disapproving housekeeper, Vera; and Lowe, her son Paul. Richard Briers appears in a cameo role as Peters father.

Although the film is primarily a comedy, serious overtones are present from the beginning. Peters father has recently died, and so Peter plans to sell the house after this last get-together party. While Andrew and Carols troubled marriage is played mainly for laughs, Roger and Mary are recovering from a devastating personal tragedy only slowly revealed to the audience: the death of one of their children. A lonely Maggie is determined to persuade Peter they should be more than just friends, and Sarahs not as happy with her life as she appears.

The events lead to Carol leaving Andrew to go back to America which leads to him falling off the wagon and drinking again after he has been sober for over a year. Roger and Mary come to an understanding of their worry over the rest of their children. Brian leaves Sarah after she cannot decide if she loves him or, much more, wants a commitment. After a failed attempt to seduce Peter, Maggie ends up hooking up with Paul. In the climax of the film Peter is forced to break some heart-wrenching news to his friends and the real reason for his gathering of all of them for the get-together: he happens to be HIV-positive which ends the reunion (and the film) on a sour note.
 The Big Chill.

==Cast==
*Stephen Fry as Peter Morton, the eponymous character.
*Kenneth Branagh as Andrew Benson, an old friend of Peter. Now a writer in America.
*Hugh Laurie as Roger Charleston, a jingle writer, and husband of Mary. Went to university with Peter.
*Imelda Staunton as Mary Charleston, a jingle writer, and wife of Roger. Went to university with Peter.
*Emma Thompson as Maggie Chester, an eccentric publisher. Went to university with Peter.
*Alphonsia Emmanuel as Sarah Johnson, a fashion designer. Went to university with Peter.
*Rita Rudner as Carol Benson, an actress, and Andrews American wife.
*Tony Slattery as Brian, Sarahs already-married boyfriend.
*Phyllida Law as Vera, the long-serving housekeeper for Peter.

==Casting==
Most of the cast are actually old university mates or have previously collaborated in other films. Hugh Laurie, Stephen Fry, Emma Thompson and Tony Slattery attended Cambridge University and had been members of the Cambridge Footlights, a student comedy troupe similar to the one portrayed in the film, at the same time. Co-writer Martin Bergman (husband of co-writer/star Rita Rudner) also attended Cambridge and was also a member of the Footlights, albeit several years ahead of them.
 Much Ado About Nothing the following year. More than a decade later Fry, Law and Slattery would appear together in the ITV series Kingdom (2007 TV series)|Kingdom.

==Soundtrack==
The soundtrack featured many artists from the 1980s, including Tears for Fears (whose song "Everybody Wants to Rule the World" was heard over the opening credits of the film), Eric Clapton, The Pretenders, and Bruce Springsteen.

The soundtrack album did not, however, feature the casts rendition of the Jerome Kern standard "The Way You Look Tonight", as performed in the film nor the song, Orpheus on the Underground, by John Hudson, which features at the beginning and end of the film.

==Reception==

===Box office===
Peters Friends grossed over $4 million in the United States. 

===Critical response===
Peters Friends was well received by most critics and currently holds a 76% "Fresh" rating on Rotten Tomatoes. 
 The Big Those Were the Days," then it doesnt matter that weve seen the formula before. This is a new weekend with new friends." 

Conversely, online critic James Berardinelli spoke poorly of the film, giving it two-and-a-half out of a possible four stars and stating, "At its best, Peters Friends is warm, touching, and funny. At its worst, its annoying and preachy. Fortunately, there are a few more moments in the former category than in the latter."  While praising Branaghs direction and performances by the cast, Berardinelli attributed most of his discontent to the films screenplay, concluding, "This is Branaghs worst effort to date and shows, if nothing else, that no matter how talented the director and his cast, he still needs a decent screenplay. And that, ultimately, is where Peters Friends falls short." 

===Accolades=== National Board Top Ten 1992 films.
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|- Evening Standard British Film Awards
| Best Actress
| Emma Thompson
|  
|-
| Peter Sellers Award for Comedy
| Kenneth Branagh
|  
|-
| Goya Awards Best European Film
| Kenneth Branagh
|  
|-
|}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 