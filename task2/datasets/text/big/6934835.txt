The Vanishing Prairie
 
{{Infobox film
| name           = The Vanishing Prairie
| image          = Living_Prairie.jpg
| image size     = 150 pbx
| caption        = Film poster for the double-feature release of The Living Desert and The Vanishing Prairie
| director       = James Algar
| producer       = Ben Sharpsteen
| writer         = James Algar Winston Hibler
| narrator       = Winston Hibler
| starring       = Paul J. Smith
| cinematography = N. Paul Kenworthy Jr.
| editing        = Lloyd L. Richardson Walt Disney Productions Buena Vista Distribution
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
}}
 Walt Disney Productions.

The theme music was given a set of lyrics by Hazel "Gil" George. It was rechristened as "Pioneers Prayer" in Westward Ho, the Wagons!, a western film about pioneers on the Oregon Trail.

==Awards==
* Academy Award for Best Documentary Feature (1954)    
* 5th Berlin International Film Festival: Big Gold Medal (Documentaries and Culture Films)   

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 

 