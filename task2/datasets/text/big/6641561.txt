The Jesse Owens Story
 
{{Infobox television film
| name = The Jesse Owens Story
| image = The Jesse Owens Story.jpg
| image_size = 
| caption =
| format = Biography
| creator =  Richard Irving
| producer = Harve Bennett (executive producer) Harold Gast (producer) Arnold Turner (associate producer)
| writer = Harold Gast Bob Banks Tom Bosley Georg Stanford Brown LeVar Burton
| cinematography = Charles Correll
| music = Michel Legrand
| editing = Richard Bracken Robert F. Shugrue	 	
| runtime = 174 min.
| studio = Harve Bennett Productions Paramount Television
| distributor = Operation Prime Time
| country = United States
| language = English
| network = Operation Prime Time
| released = July 9, 1984
}}
The Jesse Owens Story is a 1984 biographical film about the black athlete Jesse Owens. Dorian Harewood plays the Olympic gold-winning athlete.  The drama won a 1985 Primetime Emmy Award and was nominated for two more.

==Plot summary==

 

==Cast==
* Dorian Harewood as Jesse Owens
* Dan Ammerman as Coach Robertson Bob Banks as Henry Owens
* Tom Bosley as Jimmy Hoffa
* Georg Stanford Brown as Lew Gilbert
* LeVar Burton as Professor Preston
* Bernard Canepari as Attorney Grossfeld
* Robert Chidsey as Governor Aide
* Glenn Colerider as OSU Professor Overman
* Barry Corbin as Judge
* Ronny Cox as Coach Larry Snyder
* Chris Douridas as Jerome
* Norman Fell as Marty Forkins Lynn Hamilton as Mamma Solomon
* Robert Hibbard as Police Lieutenant
* George Kennedy as Charles Charley Riley Chuck Long as Terry Carter
* Gary Moody as Angry Customer Randy Moore as Coach Cromwell
* Debbi Morgan as Ruth Solomon Owens
* Greg Morris as Mel Walker
* Tonye Patano as Laverne Owens
* Lee Ritchey as Brashears
* James Sikking as Avery Brundage
* Vic Tayback as Abe Saperstein

==External links==
* 

 
 
 
 
 
 
 
 
 


 