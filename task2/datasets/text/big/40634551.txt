Sringaravelan
 
 
{{Infobox film
| name           = Sringaravelan
| image          = Sringaravelan-poster-550.jpg
| caption        = 
| alt            =
| director       = Jose Thomas
| producer       = Jaison Elamkulam Udayakrishna Siby K. Thomas Dileep Vedhika Vedhika
| Berny Ignatius
| lyrics         = Rafeeq Ahmed
| editing        = John Kutty
| released       =  
| running time   = 170 Minutes
| Distributed By = Tricolor Entertainments & PJ Entertainments Europe
| country        = India
| language       = Malayalam
| budget         = 7.20 cr 
| gross          = 11.27 cr 
}} Dileep and Vedhicka made her debut in Malayalam cinema industry with this film.  The film released for Onam, receiving positive reviews from critics and recording as Super Hit at the box-office.. 

==Plot==
Kannan (Dileep (actor)|Dileep) is the son of a fashion weaver Ayyapanashan (Babu Namboothiri). His father has sent him for fashion designing,but Kannan wants to make more money easier. His friends are Yesudas a.k.a. Yesu (Lal (actor)|Lal),a gunda and Vasu (Kalabhavan Shajon).

Kannan tries to woo rich girls and marry them and try to make his living. Once Kannan with Vasu went to give saree to Varmaji (Nedumudi Venu), who is Ayyapanashans friend. Kannan once meets Radhu (Vedhika) and he falls love with her. Kannan accidentally stops a pooja which was being held there and is forced to stay at that place for more than two weeks. In the process Kannan gets more attracted to Radha and once he gets into her room when Kannan was trying to escape that place. Kannan expresses his love to her and Radha laughs, not taking it seriously.

DGP Joy Mathew (the villain of the story) is the father of Radha and he is a big bombay don dada who wants to hide his daughter from several enemies who seek revenge. Radha and her grandparents dont like DGP as his ″profession″ is the cause for the death of Radhas mother. When Kannan gets more attracted to Radha,she says that she loves him but due to her father and his high criminal background Radha will be unable to marry Kannan and if her father knows about this love, Kannan will be shot at sight. But Radha says that if Kannan would be able to fight her father and marry her, Radha would be ready to go with Kannan.

Kannan gets scared hearing this. But he doesnt want to lose Radha. The rest of the story tells how Kannan overcomes the DGP. Kannan had to face many circumstances and takes help of Yesu, Vasu and a kidnapper Mahalingam Baburaj. At last Kannan succeeds and is united with Radha where her father is arrested by the police. Now Kannan and Radha and everybody can live happily. The film ends.

==Cast== Dileep as Kannan Vedhika as Radhi Lal as Yesu/ Yesudas
* Kalabhavan Shajon as Vasu
* Joy Mathew as DGP (D Gopi Prasad) Baburaj as Mahalingam
* Nedumudi Venu as Radhas Grand father
* Babu Namboothiri as Kannans father
* Shammi Thilakan as Ukken Tintu 
* Ambika Mohan as Kannans mother(Savithri)
* Geetha Salaam
* Ponnamma Babu
* Sreedevi Unni
* Anju Aravind as Revathy
* Anjana Appukuttan
* Rahul Dev as Vikram( Ahujas son /The Killer)
*Sharat Saxena as Ahuja (DGPs old gang leader)

==Reception==
Breaking Movies called Sringaravelan is a typical festival entertainer aimed at masses.   looks gorgeous and thankfully doesn’t look out of place. 
At the Box Office, Sringaravelan was declared the Onam Winner. Sify.com said Dileep’s ‘Sringaravelan’ is the only Onam release which is rocking the Kerala Box-Office. 

==References==
 

== External links ==
*  
*  

 
 