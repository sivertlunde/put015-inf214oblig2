Arikkari Ammu
{{Infobox film 
| name           = Arikkari Ammu
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       = Sasikumar
| writer         = G Vivekanandan Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi Madhu Jayabharathi Kuthiravattam Pappu Suchithra Sasi
| music          = V. Dakshinamoorthy
| cinematography = C Ramachandra Menon
| editing        = K Narayanan
| studio         = Nangasseril Combines
| distributor    = Nangasseril Combines
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film, directed by Sreekumaran Thampi and produced by Sasikumar. The film stars Madhu (actor)|Madhu, Jayabharathi, Kuthiravattam Pappu and Suchithra Sasi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast== Madhu
*Jayabharathi
*Kuthiravattam Pappu
*Suchithra Sasi

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Etho etho poonkaavanathil || K. J. Yesudas, Usha Ravi || Sreekumaran Thampi || 
|-
| 2 || Paavunangi kalamorungi || P Jayachandran, Vani Jairam, Chorus || Sreekumaran Thampi || 
|-
| 3 || Panchaayathu vilakkananju || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 4 || Valliyakkante Varikka plavil || CO Anto, Usha Ravi || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 