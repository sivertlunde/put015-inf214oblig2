Play Up the Band
{{Infobox film
| name = Play Up the Band
| image =
| image_size =
| caption = Harry Hughes
| producer = Basil Humphrys   Eric Donaldson  Frank Atkinson   Katherine Strueby   Vernon Harris   Harry Hughes
| narrator =
| starring = Stanley Holloway   Betty Ann Davies   Leslie Bradley   Amy Veness
| music = Eric Spear James Wilson
| editing = Paul Capon
| studio = City Films
| distributor = Associated British Film Distributors 
| released = November 1935
| runtime = 71 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy film directed by Harry Hughes and starring Stanley Holloway, Betty Ann Davies and Leslie Bradley.
 independent company Crystal Palace, which burnt down the following year.

==Synopsis== Northern town Crystal Palace in time for the competition.

==Cast==
* Stanley Holloway as Sam Small 
* Betty Ann Davies as Betty Small 
* Leslie Bradley as Jack  Frank Atkinson as Alf Ramsbottom 
* Charles Sewell as Lord Hechdyke 
* Amy Veness as Lady Heckdyke 
* Cynthia Stock as Vera 
* Julie Suedo as Marquise de Vaux 
* Arthur Gomez as Marquis de Vaux
* Hal Gordon as Bandmaster 
* Louise Selkirk and her Ladies Orchestra as Themselves 
* The London Brass Band as Themselves

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Perry, George. Forever Ealing. Pavilion Books, 1994.
* Sutton, David R. A Chorus of Raspberries: British Film Comedy 1929-1939. University of Exeter Press, 2000.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 