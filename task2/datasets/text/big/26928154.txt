The Painted Angel
{{Infobox film
| name           = The Painted Angel (aka The Broadway Hostess)
| image          =
| image size     =
| alt            =
| caption        =
| director       = Millard Webb
| producer       =
| writer         = Forrest Halsey
| narrator       =
| starring       = Billie Dove
| music          = Alois Reiser
| cinematography = John F. Seitz Harold Young
| studio         = First National Pictures
| distributor    = Warner Bros. Pictures
| released       = December 1, 1929
| runtime        = 68 min
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

The Painted Angel is a 1929 black and white American film. The storyline is based on a story by Fannie Hurst, "Give the Little Girl a Hand" The film is known as La favorita di Broadway in Italy. The tagline was: Do you want to know the Truth about NIGHT CLUB HOSTESSES?

This film is believed lost  . The UCLA Film and Television Archives only hold seven of the original eight sound discs for the film: Vitaphone production reels #3629-3635 and 3643. In a separately filmed trailer, Billie Dove talks to the audience about the picture. In September 1928, Warner Bros. Pictures purchased a majority interest in First National Pictures and from that point on, all "First National" productions were actually made under Warner Bros. control, even though the two companies continued to retain separate identities until the mid-1930s, after which time "A Warner Bros.-First National Picture" was often used.  The film reel was 1972 m (7 reels) in length.

==Cast==
*Billie Dove - Mamie Hudler / Rodeo West
*Edmund Lowe - Brood
*George MacFarlane - Oldfield
*Cissy Fitzgerald - Ma Hudler
*J. Farrell MacDonald - Pa Hudler
*Norman Selby - Jule
*Nellie Bly Baker - Sippie Will Stanton - Joe
*Douglas Gerrard - Sir Harry
*Shep Camp - Mac Peter Higgins - Singer
*Red Stanley - Dancer

==Soundtrack==
*Help Yourself To Love
:Music by M.K. Jerome
:Lyrics by Herman Ruby
*Bride Without A Groom
:Music by M.K. Jerome
:Lyrics by Herman Ruby
*Only The Girl
:Music by M.K. Jerome
:Lyrics by Herman Ruby
*Everybodys Darling
:Music by M.K. Jerome
:Lyrics by Herman Ruby
*That Thing
:Music by M.K. Jerome
:Lyrics by Herman Ruby

==External links==
* 
*  New York Times profile
* 
*  Filmval.se

 
 
 

 