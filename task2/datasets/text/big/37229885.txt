We Are Legion
 Legion|and}}

{{Infobox film
| name           = We Are Legion: The Story of the Hacktivists 
| image          =  The movie poster for We Are Legion.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Brian Knappenberger
| producer       = Luminant Media
| writer         = Brian Knappenberger
| starring       = 
| music          = John Dragonetti
| cinematography = Lincoln Else, Dan Krauss, Scott Sinkler
| editing        = Andy Robertson
| studio         = 
| distributor    = FilmBuff
| released       =  
| runtime        = 93 min
| country        = Pakistan
| language       = English
| budget         = 
| gross          = 
}}
We Are Legion: The Story of the Hacktivists is a 2012 documentary film about the workings and beliefs of the self-described "hacktivist" collective, Anonymous (group)|Anonymous.

==Synopsis  ==
 Anonymous assumed hacktivists have Occupy movement. They see themselves as activists and protectors of free speech, and tend to rise up most powerfully when they perceive a threat to internet freedom or personal privacy.

==Documented events==

The film describes in varying detail the major events of Anonymous as a group. The following events are documented:

* Habbo raids
A frequent target for organized raids by Anonymous is Habbo, a social networking site designed as a virtual hotel. The raid pre-dates, and was not inspired by, the news of an Alabama amusement park banning a two-year-old toddler affected by AIDS from entering the parks swimming pool. Users signed up to the Habbo site dressed in avatars of a black man wearing a grey suit and an Afro hairstyle and blocked entry to the pool, declaring that it was "closed due to AIDS, " flooding the site with Internet slang, and forming swastika-like formations. When the raiders were banned, they complained of racism.
* Hal Turner raid
 
According to white supremacist radio host Hal Turner, in December 2006 and January 2007 individuals who identified themselves as Anonymous took Turners website offline, costing him thousands of dollars in bandwidth bills. As a result, Turner sued 4chan, eBaums World, 7chan, and other websites for copyright infringement. He lost his plea for an injunction, however, and failed to receive letters from the court, which caused the lawsuit to lapse.
* Project Chanology
 
On January 14, 2008, a video produced by the Church of Scientology featuring an interview with Tom Cruise was leaked to the Internet and uploaded to YouTube. The Church issued a copyright violation claim against YouTube requesting the removal of the video. In response to this, Anonymous formulated Project Chanology. Calling the action by the Church of Scientology a form of Internet censorship, members of Project Chanology organized a series of denial-of-service attacks against Scientology websites, prank calls, and black faxes to Scientology centers.
* Epilepsy Foundation forum invasion
On March 28, 2008, Wired News reported that "Internet griefers"—a slang term for people whose only interests are in harassing others—assaulted an epilepsy support forum run by the Epilepsy Foundation of America. JavaScript code and flashing computer animations were posted with the intention of triggering migraine headaches and seizures in photosensitive and pattern-sensitive epileptics. According to Wired News, circumstantial evidence suggested that the attack was perpetrated by Anonymous users, with the initial attack posts on the epilepsy forum blaming eBaums World. Members of the epilepsy forum claimed they had found a thread in which the attack was being planned at 7chan.org, an imageboard that has been described as a stronghold for Anonymous. The thread, like all old threads eventually do on these types of imageboards, has since cycled to deletion.
* Sarah Palin email hack
Shortly after midnight on September 16, 2008, the private Yahoo! Mail account of Sarah Palin was hacked by a 4chan user. The hacker, known as "Rubico", claimed he had read Palins personal e-mails because he was looking for something that "would derail her campaign." After reading through Palins emails, Rubico wrote, "There was nothing there, nothing incriminating — all I saw was personal stuff, some clerical stuff from when she was governor." Rubico wrote that he used the Sarah Palin Wikipedia article to find Palins birth date (one of the standard security questions used by Yahoo!.) in "15 seconds." The hacker posted the accounts password on /b/, an image board on 4chan, and screenshots from within the account to WikiLeaks. A /b/ user then logged in and changed the password, posting a screenshot of his sending an email to a friend of Palins informing her of the new password on the /b/ thread. However, he did not blank out the password in the screenshot. A multitude of /b/ users then attempted to log in with the new password, and the account was automatically locked out by Yahoo!. The incident was criticized by some /b/ users, one of whom complained that "seriously, /b/. We could have changed history and failed, epically."
* Operation Titstorm
 
Occurred from 8 am, February 10, 2010 as a protest against the Australian Government over the forthcoming internet filtering legislation and the perceived censorship in pornography of small-breasted women (who are perceived to be under age) and female ejaculation. Hours earlier, Anonymous uploaded a video message to YouTube, addressed to Kevin Rudd, and Seven News, presenting a list of demands and threats of further action if they were not met. The protest consisted of a distributed denial-of-service attack (DDoS) on Australian Government websites. Australian anti-censorship groups complained that the attack only hurt their cause, and Australian government members dismissed the attack and said that they would just restore the service when the attack finished. Analysis of the attacks cited their peak bandwidth at under 17Mbit, a figure considered small when compared with other DDoS attacks.
* Operations Payback, Avenge Assange, and Bradical
 
On April 2, 2011 Anonymous launched an attack on the media giant Sony, named #opsony, as a part of Operation Payback. Anonymous claims the attack a success after they took down the PlayStation Network and other related PlayStation Websites. Anonymous actions also included personal harassment of employees and their families. The PlayStation Network subsequently has had lengthy outages, although Anonymous claims that this is not due to any officially sanctioned action on their part, but may be due to sub-groups of Anonymous.
* Visa, MasterCard, PayPal DDOS Attacks
Anonymous launched several Denial-of-Service attacks on the Visa, MasterCard and PayPal companies for cutting off their services to Wikileaks.
* Arab Spring
 
The websites of the government of Tunisia were targeted by Anonymous due to censorship of the WikiLeaks documents and the Tunisian Revolution. Tunisians were reported to be assisting in these denial-of-service attacks launched by Anonymous. Anonymouss role in the DDoS attacks on the Tunisian governments websites have led to an upsurge of internet activism among Tunisians against the government. A figure associated with Anonymous released an online message denouncing the government clampdown on recent protests and posted it on the Tunisian government website. Anonymous has named their attacks as "Operation Tunisia". Anonymous successfully performed DDoS attacks on eight Tunisian government websites. The Tunisian government responded by making its websites inaccessible from outside Tunisia. Tunisian police also arrested online activists and bloggers within the country and questioned them on the attacks. Anonymouss website suffered a DDoS attack on January 5.

During the 2011 Egyptian revolution, Egyptian government websites, along with the website of the ruling National Democratic Party, were hacked into and taken offline by Anonymous. The sites remained offline until President Hosni Mubarak stepped down.
* Attack on HBGary Federal
On the weekend of February 5–6, 2011, Aaron Barr, the chief executive of the security firm HBGary Federal, announced that his firm had successfully infiltrated the Anonymous group, and although he would not hand over details to the police, he would reveal his findings at a later conference in San Francisco. In retaliation for Aaron Barrs claims, members of the group Anonymous hacked the website of HBGary Federal and replaced the welcome page with a message stating that Anonymous should not be messed with, and that the hacking of the website was necessary to defend itself. Using a variety of techniques, including social engineering and SQL injection, Anonymous also went on to take control of the companys e-mail, dumping 68,000 e-mails from the system, erasing files, and taking down their phone system. The leaked emails revealed the reports and company presentations of other companies in computer security such as Endgame systems who promise high quality offensive software, advertising "subscriptions of $2,500,000 per year for access to 0day exploits".
* Operation Sony
Anonymous announced their intent to attack Sony websites in response to Sonys lawsuit against George Hotz and, specifically due to Sonys gaining access to the IP addresses of all the people who visited George Hotzs blog as part of the libel action, terming it an offensive against free speech and internet freedom Although Anonymous admitted responsibility for subsequent attacks on the Sony websites, Anonymous branch AnonOps denied that they were the cause behind a major outage of the PlayStation Network in April 2011. However, as Anonymous is a leaderless organization, the possibility remains that another branch of the group is responsible for the outage, though screenshots of AnonOps promotion of the attack still exist.
* Operation Anti-Security
The group collaborated with LulzSec to hack the websites of a number of government and corporate sources and release information from them. As well as targeting American sites, Anonymous also targeted government sites in Tunisia, Anguilla, Brazil, Zimbabwe, Turkey, and Australia. On July 21, Anonymous released two PDFs allegedly taken from NATO.

== Critical response ==
 
We Are Legion has received mainly positive reviews. Rotten Tomatoes reports a 71% approval rating based on 14 critic reviews.  The documentary also has a rating of 66% based on 6 reviews from Metacritic. 

==Awards==
* Best Picture, Downtown Film Festival 
*Best Documentary, Fantasia Film Festival 

==References==
 

==External links==
*  
*  


 
 
 
 
 
 
 
 
 