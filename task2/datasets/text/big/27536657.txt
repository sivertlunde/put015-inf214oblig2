Comedy of Innocence
 
{{Infobox film
| name           = Comedy of Innocence
| image          = 
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       = Antoine de Clermont-Tonnerre
| writer         = Massimo Bontempelli François Dumas Raúl Ruiz
| starring       = Isabelle Huppert
| music          = 
| cinematography = Jacques Bouquin
| editing        = Mireille Hannon
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

Comedy of Innocence ( ) is a 2000 French drama film directed by Raúl Ruiz (director)|Raúl Ruiz and starring Isabelle Huppert.   

==Cast==
* Isabelle Huppert - Ariane
* Jeanne Balibar - Isabella
* Charles Berling - Serge
* Edith Scob - Laurence
* Nils Hugon - Camille
* Laure de Clermont-Tonnerre - Hélène
* Denis Podalydès - Pierre
* Chantal Bronner - Martine
* Bruno Marengo - Alexandre
* Nicolas de La Baume - Lawyer
* Jean-Louis Crinon - Taxi driver
* Valéry Schatz - Taxi driver
* Emmanuel Clarke - Yannick
* Alice Souverain - Helenes friend

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 