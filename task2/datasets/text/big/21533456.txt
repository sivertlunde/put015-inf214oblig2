The Aryan
{{Infobox film name           = The Aryan image          = Aryan poster.jpg image_size     = 190px caption        = Theatrical Poster director       = William S. Hart Reginald Barker Clifford Smith  producer       = Thomas H. Ince for Triangle Films writer         = C. Gardner Sullivan starring       = William S. Hart Gertrude Claire Charles K. French Louise Glaum Bessie Love music          = cinematography = Joseph H. August  editing        = distributor    = released       =   runtime        = 50 min. (5 reel#Motion picture terminology|reels) country        = United States language  Silent with English intertitles budget         = gross          =
|}} silent era western film|motion picture starring William S. Hart, Gertrude Claire, Charles K. French, Louise Glaum, and Bessie Love. 
 Directed by produced by Thomas H. Ince, the screenplay was written by C. Gardner Sullivan.

Although Hart was assisted by Reginald Barker and Clifford Smith, he mostly directed the movie by himself. Harts salary as both actor and director was $150 per week. 

Prints of the film survive in the Library of Congress and at the Museo del Cine in Buenos Aires, Argentina. 

==Plot==
 
A hard working miner, Steve Denton (played by Hart), has become rich from years of prospecting. He takes his fortune and leaves to visit his ill mother, Mrs. Denton (played by Claire). 

In the town of Yellow Ridge, however, he is detained by a seductive dance hall girl named Trixie (played by Glaum). Also known as "the firefly," Trixie not only cheats him out of his gold, but also conceals a message that was wired to him by his dying mother.

Learning the next day that his mother is dead, Denton is infuriated about being cheated and betrayed by Trixie, who pretended to be good, and other false friends. In his rage, he kills Trixies lover, Chip Emmett (played by Mayall), and kidnaps her. Dragging her by the hair of her head, he takes her into the desert. Enslaving Trixie in his desert hideaway, Denton turns his back on "white civilization." He hates all white men and women and assumes the leadership of a band of Indian and Mexican bandits.

Two years later, a wagon-train of Mississippi farmers who are lost and dying in the desert appeal to Denton for help. He refuses to assist them. He is secretly visited that night by Mary Jane Garth (played by Love), an innocent and virtuous young woman among the migrants who bravely confronts the Indians and Mexicans.

She pleads their cause and expresses her belief that no white man would refuse to protect a woman in distress. Deeply moved, Denton is redeemed. He guides the wagon-train out of the desert and then resumes his wanderings.

==Cast==
*William S. Hart as Steve Denton
*Gertrude Claire as Mrs. Denton
*Charles K. French as "Ivory" Wells
*Louise Glaum as Trixie, "the firefly"
*Herschel Mayall as Chip Emmett
*Ernest Swallow as Mexican Pete
*Bessie Love as Mary Jane Garth
*Enid Bennett in an undetermined role
*Jean Hersholt John Gilbert as an extra (uncredited)

==Production==
Hart was given a screenplay by the screenwriter C. Gardner Sullivan in which the hero had, according to Hart, "no motive for his hardness." He argued that the audience needed an explanation. Sullivan preferred the idea that his ruthless personality was simply a given, but eventually accepted Harts wishes. 

Hart wanted Mae Marsh for the role as Mary Jane, but Marsh was working on a D. W. Griffith movie at the time. Griffith recommended a new actress, Bessie Love. 

The movie was made at the height of Harts career, but was unusual because he played a ruthless individual described as "hard as flint." As the title suggests, the movie draws on racial ideologies of the era. Hart stated that the central character, Steve Denton, was "a white man, who, foreswearing his race, makes outlaw Mexicans his comrades and allows white women to be attacked by them."   

Hart believed this movie to be "one of the best westerns ever made." 

==References==
 

==External links==
 
* 
*  AFI Catalog of Feature Films

 
 
 
 
 
 
 