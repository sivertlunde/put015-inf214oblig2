Hunters of the Golden Cobra
{{Infobox film
| name           = Hunters of the Golden Cobra
| image          = Hunters of the golden cobra vestron vhs front.jpg
| image size     =
| caption        =
| director       = Antonio Margheriti
| producer       =
| writer         = Tito Carpi Gianfranco Couyoumdjian
| starring       = David Warbeck Luciano Pigozzi
| music          = Carlo Savina
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 92 minutes
| country        = Italy
| language       = Italian
| budget         = 
}} 1982 action film starring David Warbeck. It was directed by Antonio Margheriti, and is one of several Italian imitations of Raiders of the Lost Ark, shot in an exotic location involving the recovery of supernatural relics.   

==Critical reception==
Fantastic Movie Musings & Ramblings wrote, "the violence is a bit nastier than that of RAIDERS, and the script seems like it was thrown together without much care, but I rather like the British soldier who is paired with the hero.  Still, this one is routine at best."  

==External links==
* 
* 

==References==
 

 
 
 
 
 
 
 
 

 
 