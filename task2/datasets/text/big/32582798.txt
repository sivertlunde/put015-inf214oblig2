Khamosh Raho
{{Infobox film
| name           = Khamosh Raho
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Altaf Hussain
| producer       = Ghafoor Butt
| writer         = Pervaiz Kaleem
| starring       = Shaan Shahid Juggan Kazim
| music          = M.Arshad
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 26 december 2011
| runtime        = 
| country        = Pakistan
| language       = Urdu
| budget         = RS 15 crore
| gross          = RS 16.8 crore
}}
Khamosh Raho is Pakistani Urdu language film directed by Altaf Hussain. It stars Shaan Shahid and Juggan Kazim in lead roles. {{cite news|title=Khamosh Raho: Khamosh Raho to revive industry 
|url=http://www.nation.com.pk/pakistan-news-newspaper-daily-english-online/lahore/19-May-2011/Khamosh-Raho-to-revive-industry|accessdate=21 February 2012|newspaper=PNP |date=May 19, 2011}} 

==Cast==
* Sheraz Ghafoor
* Mariam Khan
* Shaan Shahid
* Juggan Kazim Ghulam Mohiuddin
* Naghma
* Bahar Begum
* Asif Khan
* Kinza Malik
* Chacha Kunjar

==Plot== Ghulam Mohiuddin, plays an undefeated attorney who happens to have hit the wrong note with a criminal top dog named Chandia. Mohiuddinson (Sheraz Ghafoor) is profoundly in love with the evil, sadistic villain’s daughter (Mariam Khan); the lawyer also has another son (Shaan) who runs away from home to come home two decades later only to find his  mother fall sick at his sight. Then there’s Chandia’s evil police deputy stepbrother, who wants his son married to Chandia’s daughter. {{cite news|title=Khamosh Raho: Cinema to leave you speechless 
|url=http://pnp.com.pk/2011/06/30/khamosh-raho-cinema-to-leave-you-speechless/|accessdate=21 February 2012|newspaper=PNP |date=June 30, 2011}} 

==References==
 

 
 
 

 