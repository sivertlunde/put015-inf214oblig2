Latin Lovers (1965 film)
{{Infobox film
| name           = Latin Lovers (Gli amanti latini)
| film name      =  
| image          = Gli Amanti latini.jpg
| caption        =  Mario Costa
| producer       = Thomas Sagone 
| writer         = Bruno Corbucci  Giovanni Grimaldi
| starring       = Totò Franco Franchi  Ciccio Ingrassia
| music          =Carlo Savina   
| cinematography =Alberto Fusi    
| editor       = Gianmaria Messeri  
| distributor    = Euro International Films
| released       = 1965
| runtime        = 95 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 1965 Italy|Italian Mario Costa.

==Plot==
The film is composed of five episodes in which is shown the love of the Italians in the 60s. Among the low quality of love stories, the episode stands out with Totò: Amore e morte (Love and death).

==Segment: Love and death==
Antonio Gargiulo is a poor accountant who goes to the hospital to collect medical records, suspecting that he has a serious bad health. But hes fine: really bad is the grandfather of a girl who does not give peace for the bad news. Antonio is sorry and tries to console she and offers her to exchange the medical records: so the old man, discovering to feel good, will die happy. She accepts, and at the same time falls in love with Antonio who, wanting to have fun with she, sends the clinic record in accounting, to lend him the money from the board immediately.

==Cast==
*Toni Ucci  as	  Augusto (segment "La grande conquista")
*Vittorio Congia  as	 Maurizio (segment "La grande conquista")
*Alicia Brandet  as	Ursula (segment "La grande conquista")
*Eva Gioia as	Elisabeth (segment "La grande conquista")
*Gisella Sofio  as	 Miss Beata (segment "Il telefono consolatore")
*Aldo Giuffrè as	 Arminio (segment "Il telefono consolatore")
*Lina Maryan as	 Carmelina (segment "Il telefono consolatore") (as Gara Granda)
*Antonietta Tefri   (segment "Il telefono consolatore")
*Nino Marchetti  as	  Riccardo (segment "Il telefono consolatore")
*Luigi Tosi   (segment "Il telefono consolatore")
*Daniela Surina  (segment "Il telefono consolatore") 
*Aldo Puglisi as	 Saro (segment "Lirreparabile")
*Jolanda Modio  as	  Lucia Trapani (segment "Lirreparabile")
*Enzo Garinei  as	  Fifì (segment "Lirreparabile")
*Carlo Sposito  as	 Lamico di Fifì (segment "Lirreparabile") (as Carletto Sposito)
*Armando Curcio   (segment "Lirreparabile")
*Nino Musco  (segment "Lirreparabile")
*Pietro Tordi    (segment "Lirreparabile")
*Francesco Mulé  as	 Lawyer (segment "Lirreparabile")
*Elena Nicolai  as	  Mrs Trapani (segment "Lirreparabile")
*Totò  as	 Antonio Gargiulo (segment "Amore e morte")
*Franco Franchi  as	 Franco (segment "Gli amanti latini")
*Ciccio Ingrassia  as	 Ciccio (segment "Gli amanti latini")

== External links ==
*  

 
 
 
 
 
 

 
 