Blades of Blood
{{Infobox film
| name           = Blades of Blood
| image          = Blades-of-blood-poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
| hangul         =  
| rr             = Gureumeul Beoseonan Dalcheoreom
| mr             = Gurŭmŭl Pŏsŏnan Talch‘ŏrŏm}}
| director       = Lee Joon-ik
| producer       = Jo Cheol-hyeon Lee Jeong-se
| writer         = Jo Cheol-hyeon Oh Seung-hyeon Choi Seok-hwan
| based on       =  
| starring       = Hwang Jung-min Cha Seung-won Baek Sung-hyun Han Ji-hye
| music          = Kim Soo-chul Kim Joon-seok
| cinematography = Chung Chung-hoon
| editing        = Kim Sang-beom Kim Jae-beom
| studio         = Studio Acheem, Tiger Pictures
| distributor    = SK Telecom
| released       =  
| runtime        = 107 minutes
| country        = South Korea
| language       = Korean
| budget         =
| gross          =  
}}
Blades of Blood ( ; lit. "Like the Moon Escaping from the Clouds") is a 2010 South Korean action drama film directed by Lee Joon-ik. The film is based on Park Heung-yongs graphic novel Like the Moon Escaping from the Clouds. 

== Plot ==
In the late 16th century, the Joseon kingdom is thrown into chaos by the threat of a Japanese invasion. Royal descendant Lee Mong-hak (Cha Seung-won) and legendary blind swordsman Hwang Jeong-hak (Hwang Jung-min) were once allies who dreamed of stamping out the Japanese invasion, social inequality and corruption, and creating a better world. Persecuted by the court, Lee forms a rebel army in hopes of overthrowing the inept king and taking the throne himself. Lee is willing to kill recklessly and betray former comrades to forge his bloody path to the palace. Kyeon-ja is the bastard child of a family killed by Mong-hak.  Hwang Jeong-hak saves him from an injury caused by Mong-hak.  Together the two search for Mong-hak in order to confront and kill him.  

After Mong-haks Great Alliance rebel army defeat a large government military force, Hwang Jeong-hak confronts him alone.  After a lengthy battle, Mong-haks skill proves too much and the blind swordsman falls.  Despite finding out that Japanese forces are approaching and will kill and pillage every village they encounter, the Great Alliance presses on not against the invading forces but against the government.  They advance towards Seoul, where the kings palace is located. The Emperor evacuates and the rebel army takes Seoul.  The rebels celebrate,unfortunately a few minutes later the Japanese army comes and begins massacring everybody. Kyeon-ja confronts Mong-hak while the Japanese are killing everybody and kills Mong-hak.  He then is killed by the Japanese army.

== Cast ==
*Hwang Jung-min as Hwang Jeong-hak
*Cha Seung-won as Lee Mong-hak
*Baek Sung-hyun as Kyeon-ja
*Han Ji-hye as Baek-ji
*Lee Dol-hyung as nobleman Song King Seonjo
*Song Young-chang as Han Shin-gyun
*Yeom Dong-hyun as nobleman Park
*Jung Gyu-soo as tableware maker
*Shin Jung-geun as nobleman Yoo
*Ryu Seung-ryong as nobleman Jung
*Lee Hae-young as Han Pil-joo
*Yang Young-jo as Lee Jang-gak
*Jung Min-sung as Hwang Yoon-gil
*Lee Jae-gu as Magistrate Choi
*Jung Jae-heon as executor
*Kang Hyun-joong as Daedong mob subordinate
*Han Seung-do as police chief
*Ji Il-joo as scholar
*Lee Sol-gu as prison guard
*Jo Kyung-hoon as assassin
*Choi Dae-sung as Im Chul-mins subordinate
*Park Jin-woo as Kim Sung-il
*Yeon Young-geol as public officer
*Kim Byung-oh as low public officer 4
*Shin Young-sik as nobleman
*Kim Young-hoon as executor Kim Sang-ho as Park Dol-seok
*Kim Bo-yeon as gisaengs mother
*Min-young as gisaeng
*Kim Sung-hoon as executioner

== Production ==
Actor Hwang Jung-min expressed his difficulty playing a blind character in the film. Hwang went to schools for blind people to observe their movements but stated that "it still wasn’t an easy role to play". 

== Release ==
Blades of Blood premiered on April 29, 2010 in South Korea.    It opened at number two in the box office, grossing   on 603 screens.  In total the film received 1,389,295 admissions nationwide with a domestic gross of  .  

The film had its international premiere at New York Asian Film Festival on July 8, 2010 where it was the festivals closing film.   Lee Joon-ik won the Jury Award for Best Director at the Fantasia Festival in Montreal Canada.  Blades of Blood was one of six films considered as Koreas submission for the foreign-language Oscar award. 

== Reception ==
Film Business Asia gave the film a rating of seven out of ten, praising action and characters calling it a "cut above most Korean swordplay dramas."  Variety (magazine)|Variety gave the film a mixed review, stating that "political infighting on the eve of a Japanese invasion is told with unnecessarily broad, imagistic strokes that let character-generated conflicts slip between the blades" as well as that "the film easily outdoes numerous gimmicky f/x extravaganzas, explaining its early pickup by several European and Asian distribs." 

== References ==
 

== External links ==
*  
*   at Naver
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 