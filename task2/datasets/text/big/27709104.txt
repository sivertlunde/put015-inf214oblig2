Around the World (1967 film)
 
{{Infobox film
| name           = Around The World
| image          =
| image_size     =
| caption        =
| director       = Pachhi
| producer       =
| writer         = Jagdish Kanwal (dialogue) Pachhi (screenplay)
| narrator       =
| starring       = Raj Kapoor
| music          = Shankar Jaikishan
| cinematography =
| editing        =
| distributor    =
| released       = 1967
| runtime        = 178 minutes
| country        = India
| language       = Hindi
| budget         =
| preceded_by    =
| followed_by    =
}}
 1967 directed by Pachhi.
 70mm and was extensively shot all around the world, as the title suggests.  A romcom, it featured Raj Kapoor as an Indian who travels around the world on 8 dollars. The film also starred Rajshree, Ameeta, Om Prakash, and Mehmood Ali|Mehmood.

The sum of 8 dollars refers to the number of dollars, or an equivalent foreign currency, that an Indian was allowed, by Government rules, to obtain by converting rupees into foreign currencies.  

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background:#f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dil Laga Kar Aap Se" Mukesh 
|-
| 2
| "Chale Jana Zara Thahro" Sharda
|-
| 3
| "Duniya Ki Sair Kar Lo"
| Mukesh, Sharda
|-
| 4
| "Jane Bhi De Sanam"
| Sharda
|-
| 5
| "Joshe Jawani Hai Re Hai"
| Mukesh
|-
| 6
| "Kauva Chala Hans Ki Chaal"
| Mohammed Rafi
|-
| 7
| "Yeh Munh Aur Masoor Ki Dal"
| Mubarak Begum, Sharda
|-
| 8
| "Aao Aao Aate Kyon Nahin"
| Mohammed Rafi, Manna Dey
|-
| 9
| "Chale Jana Zara Thahro"
| Sharda
|}
==References==
 

==External links==
*  

 
 
 
 
 
 

 
 