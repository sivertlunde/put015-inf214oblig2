Monday (film)

{{Infobox film
| name           = Monday
| image          = Monday_DVD_cover.jpg
| caption        = Monday DVD cover Sabu
| producer       = Bong-Ou Lee
| writer         = Sabu
| starring       =  
| music          = Kenichiro Shibuya
| cinematography = Kazuto Sato
| editing        = Kumio Onaga
| studio         =  
| distributor    =  
| released       =  
| runtime        = 100 minutes Japan
| language       = Japanese
| budget         =
| gross          =
}}
 Japanese comedy comedy thriller thriller drama FIPRESCI Award "for its austere, dark wit and keen eye for human foibles."   

==Plot==
Takagi (Shinichi Tsutsumi), a seemingly average Japanese businessman, wakes up in a hotel room but doesnt know how he wound up there. When a packet of "purification salt" falls out of his pocket, he starts having memories of a funeral and a meeting with a yakuza boss. Soon he finds out he is in deep trouble.

==Cast==
* Shinichi Tsutsumi as Koichi Takagi
* Yasuko Matsuyuki as Yuko Kirishima 
* Ren Ohsugi as Murai Yoshio 
* Masanobu Ando as Mitsuo Kondo 
* Hideki Noda as Shingo Kamiyama 
* Akira Yamamoto as Kiichiro Hanai 
* Naomi Nishida as Yuki Machida

==Reception==
The New York Times found that "Monday has some of Sabu’s sharpest satire" and "offers a lot of stylish parody as it tracks the increasingly grim trajectory of the salaryman’s lost weekend."    Derek Elley in Variety (magazine)|Variety wrote that "Sabu makes a stunning return to form with "Monday," his fourth and best movie to date."   

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 