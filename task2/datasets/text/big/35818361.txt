Caring for the Lagoon
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Caring for the Lagoon
| image          = 
| director       = Oliver Dickinson
| producer       = Anthony Dickinson Philippe de Grissac
| cinematography = Oliver Dickinson
| editing        = Oliver Dickinson
| distributor    = LVP
| released       =  
| runtime        = 52 minutes
| country        = France United Kingdom
| language       = French, Shimaore, Shibushi, English
}}
Caring for the Lagoon is a documentary directed by Oliver Dickinson about how the Mahorans of Mayotte are trying to preserve their lagoon.

The film has been selected by numerous festivals throughout the world (i.e. Al Jazeera Documentary Film Festival, Kuala Lumpur Eco Film festival, Roshd International Film Festival, Ménigoute Wildlife Film Fetsival) and has won many awards (i.e. Grand Prix of Ecology at the Warsaw FilmAT Festival 2012, Best Ecology Film Award at the Silver Lake International Film Festival 2011, Cousteau Award at the Ecollywood Film Festival 2011).

==See also==
* Ecology
* Biodiversity
* Sustainable development
* Mozambique Channel
* Indian Ocean

==External links==
*  
*  

 
 
 
 
 
 

 
 