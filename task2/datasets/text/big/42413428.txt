A Pilot Returns
{{Infobox film
| name =  A Pilot Returns
| image =
| image_size =
| caption =
| director = Roberto Rossellini
| producer = 
| writer =  Michelangelo Antonioni   Ugo Betti   Gherardo Gherardi   Rosario Leone   Margherita Maglione   Massimo Mida   Vittorio Mussolini   Roberto Rossellini
| narrator =
| starring = Massimo Girotti   Michela Belmonte   Piero Lulli   Gaetano Masier Renzo Rossellini 
| cinematography = Vincenzo Seratrice 
| editing = Eraldo Da Roma 
| studio = Alleanza Cinematografica Italiana 
| distributor = Alleanza Cinematografica Italiana 
| released = 8 April 1942
| runtime = 87 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} The White Ship (1941) and The Man with a Cross (1943). It was made with the co-operation of the Italian Air Force.  The films sets were designed by the architect Virgilio Marchi.

== Synopsis == Italian invasion Greece has fallen.

== Cast ==
* Massimo Girotti as Gino Rossati 
* Michela Belmonte as Anna 
* Piero Lulli as De Santis 
* Gaetano Masier as Trisotti 
* Elvira Betrone as Signora Rossati 
* Piero Palermini as English Official
* Giovanni Valdambrini as medico 
* Nino Brondello as Vitali 
* Jole Tinta as madre

== References ==
 

== Bibliography ==
* Bondanella, Peter. The Films of Roberto Rossellini. Cambridge University Press, 1993.
* Haaland, Torunn. Italian Neorealist Cinema. Edinburgh University Press, 2012.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 