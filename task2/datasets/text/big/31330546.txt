Tinker Tailor Soldier Spy (film)
 
 
 
{{Infobox film
| name           = Tinker Tailor Soldier Spy
| image          = Tinker, Tailor, Soldier, Spy Poster.jpg
| caption        = UK release poster
| director       = Tomas Alfredson
| producer       = {{Plainlist|
* Tim Bevan
* Eric Fellner
* Robyn Slovo
}}
| screenplay     = {{Plainlist|
* Bridget OConnor
* Peter Straughan
}}
| based on       =  
| starring       = {{Plainlist|
* Gary Oldman
* Colin Firth
* Tom Hardy
* John Hurt
* Toby Jones
* Mark Strong
* Benedict Cumberbatch
* Ciarán Hinds
}}
| music          = Alberto Iglesias
| cinematography = Hoyte van Hoytema
| editing        = Dino Jonsäter
| studio         = StudioCanal Karla Films Paradis Films Kinowelt Filmproduktion Working Title Films Canal+ Ciné+
| distributor    = StudioCanal UK
| released       =  
| runtime        = 127 minutes
| country        = United Kingdom France Germany
| language       = English
| budget         = $21 million 
| gross          = $80,630,608 	
}} espionage film of the same name by John le Carré.
 Soviet double agent at the top of the British secret service.
 Best Original Best Actor.
 Tinker Tailor Soldier Spy.

==Plot== 
  British Intelligence Communist Hungary Soviet agents. Amid the international incident that follows, Control and his right-hand man George Smiley (Oldman) are forced into retirement. Control, already ill, dies soon afterwards.

Percy Alleline (Jones) becomes the new Chief of the Circus, with Bill Haydon (Firth) as his deputy and Roy Bland (Hinds) and Toby Esterhase as close allies. They had already initiated a secretive operation called "Witchcraft" to obtain valuable Soviet intelligence. Control and Smiley had mistrusted the material produced by Witchcraft, which is being shared with the United States in exchange for valuable American intelligence.
 civil servant mole in a senior role in British Intelligence. Control had held this suspicion as well. Working outside of the Circus, Smiley chooses Peter Guillam (Cumberbatch) and retired Special Branch officer Mendel to assist his investigation and begins to interview people who left the Circus about the same time that he and Control did.

One is Connie Sachs, who had been sacked by Alleline after claiming that Alexei Polyakov, a Soviet cultural attaché in London, was a Soviet spy. Another is Jerry Westerby, who had been duty clerk on the night Prideaux was shot. Westerby says that on that night he called Smileys house for instructions, but Ann, Smileys philandering wife, had answered. Shortly after, Haydon arrived at the Circus and said that he saw the news on the tickertape at his club. Smiley realizes that Haydon must have heard the news from Ann, confirming his suspicion that the two had been having an affair.

Smiley comes home and finds Tarr hiding there. Tarr tells him that he had been sent to Istanbul to investigate a Soviet agent named Boris. Tarr found that Boris had no significance, but that Boriss wife Irina was also an operative and seemed to have information. So Tarr overstayed in Istanbul and started having an affair with Irina to gain her trust. Irina, however, knew who Tarr was, and asked to trade information—specifically, the name of a mole who existed in the top ranks of the Circus and who worked for a KGB spymaster named Karla—for a new life in the West.

Tarr sent Irinas request back to London, but the reply, coming after several hours, ignored Irinas request and ordered him to come home immediately. Tarr then finds that Boris, as well as the British station chief in Istanbul, have been killed. Tarr saw Irina captured by her employers; he was subsequently accused of defecting and of murdering the British station chief, so went into hiding.

Guillam is sent by Smiley to steal the Circus logbook for the night Tarr called: he finds the pages for that night are cut out, suggesting that Tarrs story is true.

Prideaux, who was secretly returned by the Russians but sacked from the service, is now in hiding, working as a language teacher at a boys school. Prideaux reveals to Smiley that the purpose of the Hungary mission was to get the name of the mole. Control had codenamed the suspects "Tinker" (Alleline), "Tailor" (Haydon), "Soldier" (Bland), "Poorman" (Esterhase) and "Beggarman" (Smiley himself). Prideaux tells how he was brutally interrogated under the direction of Karla and gave in, and also how he saw a blonde female prisoner, Irina, being shot in front of him. However, says Prideaux, the Soviets already knew of Controls investigation into the mole, and were only interested in finding out how far that investigation had progressed.

Smiley learns that Alleline, Haydon, Bland and Esterhase have been regularly meeting Polyakov – the "Witchcraft" source – at a safe house to get material. At every meeting, Polyakov gives these men supposedly high-grade Soviet intelligence in exchange for low-grade British material that helps him maintain his cover with the Soviets. In reality, however, one of these men is the mole, and is passing along substantive material, including American intelligence, and Polyakov is his Agent handler|handler. The material Polyakov passes along is mostly "chicken feed", with just enough substance to persuade the Americans to share information with the British.

Smiley gets the safe houses location by threatening to deport Esterhase, who was formerly Hungarian and would surely be treated as a traitor there. Smiley then sets a trap by having Tarr appear at the Paris office implying he knows who the mole is and is ready to give the name. The mole hears this, and immediately arranges with Polyakov to meet at the safe house to ask the Soviets to kill Tarr. Smiley waits at the safe house and captures the mole: Haydon.

At the Circus interrogation centre in Sarratt, Haydon reveals that he seduced Smileys wife on Karlas orders, in order to distort any suspicions Smiley may have had of Haydon. Haydon also reveals that Prideaux confided in him about Controls suspicion of a mole right before Prideaux left for Hungary, since they were close friends. The Circus makes plans to exchange Haydon back to the Soviets, but Prideaux, having learned of how Haydon betrayed him, kills him. Smiley is restored to the Circus as its chief.

==Cast==
 
* Gary Oldman as George Smiley/"Beggarman"
* Colin Firth as Bill Haydon/"Tailor"
* Tom Hardy as Ricki Tarr
* Mark Strong as Jim Prideaux
* Ciarán Hinds as Roy Bland/"Soldier"
* Benedict Cumberbatch as Peter Guillam
* David Dencik as Toby Esterhase/"Poorman" Stephen Graham as Jerry Westerby
* Simon McBurney as Oliver Lacon
* Toby Jones as Percy Alleline/"Tinker"
* John Hurt as "Control"
* Kathy Burke as Connie Sachs
* Roger Lloyd-Pack as Mendel
* Svetlana Khodchenkova as Irina
* Arthur Nightingale as Bryant
* John le Carré as Christmas Party Guest
* Christian McKay as Mackelvore
* Konstantin Khabensky as Polyakov
* Linda Marlowe as Mrs McCraig Michael Sarne Karla
* Tomasz Kowalski as Boris Stuart Graham as Minister
* Zoltán Mucsi as Hungarian agent
* Laura Carmichael as Sal
 

==Production==

===Development===
The project was initiated by Peter Morgan when he wrote a draft of the screenplay, which he offered to Working Title Films to produce. Morgan dropped out as the writer owing to personal reasons, but still served as an executive producer.  Following Morgans departure, Working Title hired Peter Straughan and his wife, Bridget OConnor, to redraft the script. Park Chan-wook considered directing the film, but ultimately turned it down.  Tomas Alfredson was confirmed to direct on 9 July 2009. The production is his first English language film.   The film was backed financially by Frances StudioCanal and had a budget corresponding to $21 million.    The film is dedicated to OConnor, who died of cancer during production.

===Casting===
The director cast Gary Oldman in the role of George Smiley, and described the actor as having "a great face" and "the quiet intensity and intelligence thats needed". Many actors were connected to the other roles at various points, but only days before filming started, Oldman was still the only lead actor who officially had been contracted.   .  . He was replaced by Toby Jones.  John le Carré appears in a cameo as a guest in a party scene. 

===Filming===
 , the exterior of "The Circus"]]
 
Principal photography took place between 7 October and 22 December 2010.    Studio scenes were shot at a former army barracks in Mill Hill, North London.  Blythe House in Kensington Olympia, West London, was used as the exterior for "The Circus."  The interior hall of Budapests Párizsi Udvar served as the location for the café scene, in which Jim Prideaux is shot.  Empress Coach Works in Haggerston was used as the location for the Merlin safe house. Other scenes were filmed on Hampstead Heath and in Hampstead Ponds, where Smiley is shown swimming, and in the physics department of Imperial College London. The exterior shots of the Islay Hotel, a run-down hotel described in the film as being near Liverpool Street station, which Smiley uses as a base, were shot in Wilkin Street, London NW5. 
 Let the Right One In. 

===Post-production=== La Mer" montage of various characters and subplots being resolved as Smiley strides into Circus headquarters to assume command, was chosen because it was something the team thought George Smiley would listen to when he was alone; Alfredson described the song as "everything that the world of MI6 isnt". A scene where Smiley listens to the song was filmed, but eventually cut to avoid giving it too much significance.  
 Licensed to Kill.

==Release==
 
The film premiered in competition at the 68th Venice International Film Festival on 5 September 2011.  StudioCanal UK distributed the film in the United Kingdom, where it was released on 16 September 2011.  The US rights were acquired by Universal Pictures, which have a permanent first-look deal with Working Title, and they passed the rights to their subsidiary Focus Features. Focus planned to give the film a wide release in the United States on 9 December 2011, but pushed it to January 2012 where it was given an 800 screen release. 

===Critical response===
Tinker Tailor Soldier Spy received generally positive reviews. Rotten Tomatoes sampled 206 reviewers and judged 83% of the reviews to be positive. The site summarised the film as "a dense puzzle of anxiety, paranoia, and espionage that director Tomas Alfredson pieces together with utmost skill".    Metacritic, which assigns a normalised rating in the 0–100 range based on reviews from top mainstream critics, calculated an average score of 85 based on 42 reviews. 

Jonathan Romney of   declared the film "a triumph" and gave it a five star rating,  as did his colleague, Sukhdev Sandhu.  Stateside, Peter Travers of Rolling Stone wrote, "As Alfredson directs the expert script by Peter Straughan and Bridget OConnor, the film emerges as a tale of loneliness and desperation among men who can never disclose their secret hearts, even to themselves. Its easily one of the years best films."  M. Enois Duarte of High-Def Digest also praised the film as a "brilliant display of drama, mystery and suspense, one which regards its audience with intelligence". 

Detractors of the film included Peter Hitchens of The Mail on Sunday, who wrote that the plot would be too baffling for viewers who had not read the book, and that the films makers had "needlessly messed it up".    David Edwards of the Daily Mirror wrote, "The big question – and one le Carré himself asked when the film was announced – is whether such a hefty novel can fit comfortably into a feature-length production. In answering this, the writers have pared things back, meaning its far pacier than the seven-part TV show. Unfortunately, the plot is every bit as bewildering with an overload of spy-speak, a few too many characters to keep track of and a final act that ends with a whimper, rather than a bang."  Writing in The Atlantic, le Carré admirer James Parker favourably contrasted Smiley with the James Bond franchise, but finds this Tinker, Tailor adaptation "problematic" compared to the 1979 BBC mini-series. He writes "To strip down or minimal ize le Carré, however, is to sacrifice the almost Tolkienesque grain and depth of his created world: the decades-long backstory, the lingo, the arcana, the liturgical repetitions of names and functions". 

===Box office===
The film topped the British box-office chart for three consecutive weeks,  and earned $80,630,608 worldwide. 

===Accolades===
{| class="wikitable collapsible collapsed" style="width:85%;"
|- List of awards and nominations
|-  style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipient(s) and nominee(s)
! Result
|- 84th Academy Academy Awards 26 February 2012 Best Actor
| Gary Oldman
|  
|- Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|- Best Original Score
| Alberto Iglesias
|  
|-
| Amanda Award 
| 17 August 2012 Best Foreign Film
| Tomas Alfredson
|  
|- American Society of Cinematographers 
| 12 February 2012 Best Cinematography in a Feature Film
| Hoyte van Hoytema
|  
|- Art Directors Guild 
| 4 February 2012
| Period Film
|  Maria Djurkovic  
|  
|- British Academy Film Awards 12 February 2012 Best Film
|
|  
|- Outstanding British Film
|
|  
|- Best Actor in a Leading Role
| Gary Oldman
|  
|- Best Director
| Tomas Alfredson
|  
|- Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|- Best Original Music
| Alberto Iglesias
|  
|- Best Cinematography
| Hoyte van Hoytema
|  
|- Best Editing
| Dino Jonsater
|  
|- Best Production Design
| Maria Djurkovic, Tatiana MacDonald
|  
|- Best Costume Design
| Jacqueline Durran
|  
|- Best Sound
|
|  
|- Outstanding British Contribution to Cinema
| John Hurt
|  
|- British Film Bloggers Circle Awards 21 February 2012
| Best Film
|
|  
|-
| Best British Film
|
|  
|-
| Best Actor
| Gary Oldman
|  
|-
| Best Director
| Tomas Alfredson
|  
|-
| Best Adapted Screenplay
|
|  
|- British Independent Film Awards 4 December 2011 Best British Independent Film
| Tinker Tailor Soldier Spy
|  
|-
| Best Director of a British Independent Film
| Tomas Alfredson
|  
|- Best Performance by an Actor in a British Independent Film
| Gary Oldman
|  
|-
| Best Technical Achievement
| Maria Djurkovic  
|  
|- Best Supporting Actress
| Kathy Burke
|  
|- BIFA Award Best Supporting Actor
| Tom Hardy
|  
|-
| Benedict Cumberbatch
|  
|- British Film Institute 4 December 2011 Top Ten Films
|
|  
|- Best Film
|
|  style="text-align:center; background: #FFB;"| 
|- Burgundy Film Critics Awards 
| 24 February 2013
| Best Foreign Film
| Tomas Alfredson
|  
|-
| Ciak doro
| 6 June 2012
| Best Foreign Film
| Tomas Alfredson
| style="text-align:center; background:lightblue;"| 
|- Chicago Film Critics Association
| rowspan="2" | 19 December 2011 Best Actor
| Gary Oldman
|  
|- Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Crime Thriller Awards
| 18 September 2012
| Best Film
| 
|  
|- Conch Awards
| rowspan="3" | 19 September 2012
| Best Film Soundtrack
| Stephen Griffiths
|  
|-
| Best Film Mix Facility
| Goldcrest Post Production
|  
|-
| Best Sound Design & Editorial Team
| Andy Shelley and Stephen Griffiths
|  
|- Denver Film Critics Society
| rowspan="2"| 11 January 2012
| Best Cast
|
|  
|-
| Best Original Score
| Alberto Iglesias
|  
|- Dublin Film Dublin Film Critics Circle Awards 23 December 2011
| Top Ten Films
|
|  
|-
| Best Film
|
| style="text-align:center; background: #FFB;"| 
|-
| Top Ten Directors
| Tomas Alfredson
|  
|-
| Best Director
| Tomas Alfredson
| style="text-align:center; background: #FFB;"| 
|-
| Top Ten Actors
| Gary Oldman
|  
|-
| Best Actor
| Gary Oldman
| style="text-align:center; background: #FFB;"| 
|- Empire Awards 25 March 2012 Best Film
|
|  
|- Best British Film
|
|  
|- Best Actor
| Gary Oldman
|  
|- Best Director
| Tomas Alfredson
|  
|- Best Thriller
|
|  
|- European Film Awards 1 December 2012 Best Actor
| Gary Oldman
|  
|- Best Production Design
|  Maria Djurkovic
|  
|- Best Cinematography
| Hoyte van Hoytema
|  
|- Best Original Score
| Alberto Iglesias
|  
|-
| European Film Award – Peoples Choice Award for Best European Film|Peoples Choice Award – Best European Film
| Tomas Alfredson
|  
|- Evening Standard British Film Awards 7 February 2012
| Best Film
|
|  
|-
| Best Actor
| Gary Oldman
|  
|-
| Best Technical Achievement
| Maria Djurkovic
|  
|-
| Alexander Walker Special Award
| John Hurt
|  
|- Golden Trailer Awards 31 May 2012
| Best Drama Trailer
|
|  
|-
| Best Thriller Trailer
|
|  
|-
| Best Independent Poster
|
|  
|-
| Best Drama Poster
|
|  
|- Georgia Film Critics Association
| rowspan="8"| 16 January 2012
| Best Film
|
|  
|-
| Best Director
| Tomas Alfredson
|  
|-
| Best Actor in a Leading Role
| Gary Oldman
|  
|-
| Best Supporting Actor
| Tom Hardy
|  
|-
| Best Ensemble Cast
|
|  
|-
| Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Best Cinematography
| Hoyte van Hoytema
|  
|-
| Best Production Design
| Maria Djurkovic
|  
|- Gotham Independent Film Awards 18 November 2011 Gotham Tribute Award
| Gary Oldman
|  
|-
| Hollywood Film Festival
| 24 October 2011 Best Composer
| Alberto Iglesias
|  
|- International Chinephile Society 22 February 2012
| Best Cast
|
| style="text-align:center; background:lightblue;"| 
|-
| Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Best Production Design
| Maria Djurkovic
| style="text-align:center; background:lightblue;"| 
|-
| Best Original Score
| Alberto Iglesias
| style="text-align:center; background:lightblue;"| 
|- International Federation International Federation of Film Critics Award 10 September 2012 Grand Prix for the best film
| Tomas Alfredson
| style="text-align:center; background: #FFB;"| 
|- International Online Film Critics Poll   20 December 2012
| Best Film – Motion Picture
|
|  
|-
| Top Ten Films
|
|  
|-
| Best Director
| Tomas Alfredson
|  
|-
| Best Actor in a Leading Role
| Gary Oldman
|  
|-
| Best Ensemble Cast
| 
|  
|-
| Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Best Cinematography
| Hoyte van Hoytema
|  
|-
| Best Production Design
| Maria Djurkovic
|  
|-
| Best Editing
| Dino Jonsäter
|  
|-
| Best Original Score
| Alberto Iglesias
|  
|- Irish Film and Television Awards 11 February 2012
| Best International Film
|
|  
|-
| Actor in a Lead Role in a Feature Film
| Ciarán Hinds
|  
|-
| International Actor
| Gary Oldman
|  
|- Italian Online Film Actors & Dubbers Award
| rowspan="5"| 1 September 2012
| Best Foreign Actor
| Gary Oldman
|  
|-
| Best Foreign Supporting Actor
| Tom Hardy
|  
|-
| Best Foreign Cast
|
|  
|-
| Best Male Dubber
| Stefano De Sando
|  
|-
| Public Choice Award for Best Performance
| Gary Oldman
|  
|-
| rowspan="5" | Italy Screenplay Prize
| rowspan="5" | 13 July 2012
| Best Film
|
|  
|-
| Top Ten Films
|
|  
|-
| Best Adapted Screenplay – International
| Bridget OConnor, Peter Straughan
|  
|-
| Special Award for Best Director
| Tomas Alfredson
|  
|-
| Special Award for Best Performance
| Gary Oldman
|  
|- Las Vegas Film Critics Society
| rowspan="5"| 13 December 2011
| Best Actor
| Gary Oldman
|  
|-
| Best Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Best Art Direction
| Maria Djurkovic
|  
|-
| Best Cinematography
|
|  
|-
| Best Editing
| Dino Jonsater
|  
|- London Film Critics Circle Award
| rowspan="7"| 19 January 2012 Top Ten Film
|
|  
|- Best Film
|
| style="text-align:center; background: #FFC;"| 
|- Best British Film
|
|  
|- Best Actor
| Gary Oldman
|  
|- Best British Actor
| Gary Oldman
|  
|- Best Screenplay
| Bridget OConnor, Peter Straughan
|  
|- Best Technical Achievement
| Maria Djurkovic
|  
|-

| Los Angeles Film Critics Association
| 11 December 2011 Best Art Direction
| Maria Djurkovic
| style="text-align:center; background:lightblue;"| 
|- Metacritic Awards
| rowspan="2"| 5 January 2012
| Best Reviewed Drama
|
|  style="text-align:center; background: #FFB;"| 
|-
| Best Reviewed Thriller
|
|  
|-
| Movie Farm Awards
| 12 February 2012
| Best Actor
| Gary Oldman
|  
|-
| Music & Sound Awards
|
| Best Original Composition in a Film
| Alberto Iglesias
|  
|- Online Film Critics Society Awards
| rowspan="3"| 2 January 2012 Best Actor
| Gary Oldman
|  
|- Best Adapted Screenplay
|
|  
|- Best Editing
| Dino Jonsater
|  
|- Online Film & Television Association
| rowspan="5"| 5 February 2012
| Best Actor
| Gary Oldman
|  
|-
| Best Adapted Screenplay
|
|  
|-
| Best Production Design
| Maria Djurkovic
|  
|-
| Best Cast
|
|  
|-
| Best Casting
| Jina Jay
|  
|-
| Palm Springs International Film Festival
| 15 January 2012 Best International Star
| Gary Oldman
|  
|-
| Phoenix Film Critics Society
| 27 December 2011
| Best Actor
| Gary Oldman
|  
|- Premio Cinema Ludus  19 November 2012
| Gran Prix for Best Film
| Tomas Alfredson
|  
|-
| Prix for Best Actor
| Gary Oldman
|  
|-
| Best European Film
| 
|  
|-
| Best European Director
| Tomas Alfredson
|  
|-
| Best European Actor
| Gary Oldman
|  
|-
| Best European Screenplay 
| Bridget OConnor, Peter Straughan
|  
|-
| Best European Technical Achievement
| Maria Djurkovic
|  
|-
| Best Producer
| Tim Bevan, Eric Fellner 
|  
|- Richard Attenborough|Richard Attenborough Regional Film Awards
| rowspan="4"| 2 February 2012
| Best British Film of the year
|
|  
|-
| Best Actor of the year
| Gary Oldman
|  
|-
| Best British Actor of the year
| Gary Oldman
|  
|-
| Best Screenplay
| Bridget OConnor, Peter Straughan
|  
|- San Francisco Film Critics Circle 25 March 2012 Best Actor
| Gary Oldman
|  
|- Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|- Satellite Award
| rowspan="3"| 18 December 2011 Best Film – Motion Picture
|
|  
|- Best Director
| Tomas Alfredson
|  
|- Best Actor – Motion Picture
| Gary Oldman
|  
|-
| Spanish Film Music Critics Awards
| 29 June 2012
| Best Spanish Composer
| Alberto Iglesias
|  
|-
| Stockholm Film Festival
| 20 November 2011 FIPRESCI Award
|
|  
|- Sydney Film Critics
| rowspan="2"| 21 December 2011
| Top Twenty Unreleased Films
|
|  
|-
| Best Unreleased Film
|
| style="text-align:center; background: #FFB;"| 
|- Total Film|Total Film Hotlist
| rowspan="3"| 3 August 2012 Hottest Film
|
|  
|- Hottest Actor
| Benedict Cumberbatch
|  
|- Hottest Actor
| Tom Hardy
|  
|-
| Venice Film Festival 10 September 2011
| Golden Lion
|
|  
|- Virgin Media Movie Awards
|| 1 March 2012
| Best Film
|
|  
|-
| Washington D.C. Area Film Critics Association 5 December 2011 Best Adapted Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| rowspan="14" | YouMovie Awards  
| rowspan="14" | 30 June 2012
| Best Film
|
|  
|-
| Best Drama Film
|
|  
|-
| Best Thriller
|
|  
|-
| Best Actor in a Leading Role
| Gary Oldman
|  
|-
| Best Supporting Actor
| Benedict Cumberbatch
|  
|-
| Best Supporting Actor
| Colin Firth
|  
|-
| Best Cast
|
|  
|-
| Best Villain
| Colin Firth
|  
|-
| Best Director
| Tomas Alfredson
|  
|-
| Best Trailer
|
|  
|-
| Best Cinematography
| Hoyte Van Hoytema
|  
|-
| Best Art Direction
| Maria Djurkovic
|  
|-
| Best Screenplay
| Bridget OConnor, Peter Straughan
|  
|-
| Best Costume Design
| Jaqueline Durran
|  
|- World Soundtrack Academy World Soundtrack 20 October 2012 Best Score of the Year
| Alberto Iglesias
|  
|- Best Composer of the Year
| Alberto Iglesias
|  
|}

==Sequel== Les Misérables film adaptation, producer Eric Fellner stated that fellow producer Tim Bevan is working with writer Straughan and director Alfredson on developing a sequel. Fellner did not specify if the sequel will be based on The Honourable Schoolboy or Smileys People, the two remaining Smiley novels in Le Carrés "Karla Trilogy". {{cite web|last=Chitwood|first=Adam|url=
http://collider.com/tinker-tailor-soldier-spy-2-sequel-eric-fellner/217164/|title=Producer Eric Fellner Talks; Says Tomas Alfredson and Screenwriter Peter Straughan are Working on it "As We Speak"|work=Collider|date=11 December 2012|accessdate=11 December 2012}} 

==References==
 

==External links==
 
 
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 