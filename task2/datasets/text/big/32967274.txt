Caught in the Net
 
 
{{Infobox film
| name           = Caught in the Net
| image          = 
| image_size     =
| caption        = 
| director       = Vaughan C. Marshall
| producer       = 
| writer         = Sheila Preston
| narrator       =
| starring       = Zillah Bateman John Mayer
| music          =
| cinematography = Tasman Higgins
| editing        = 
| studio = Advance Films
| distributor    = 
| released       = 14 July 1928 
| runtime        =
| country        = Australia
| language       = Silent film  English intertitles
| budget         = ₤2,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 145. 
| preceded_by    = 
| followed_by    = 
}}

Caught in the Net is a 1928 Australian silent film about a woman in high society starring Zillah Bateman, a British theatre star who was touring Australia at the time.  Only part of the film survives. 

==Plot==
Society girl Phyllis Weston is loved y two men, handsome Jack Stacey and villainous Robson. In a yacht race, Robson tries to sabotage Jacks boat but fails. Robson then tries to get his sister to trap Jack in a comprising situation, but is unsuccessful.
 St Kilda and a rescue from drowning at Portsea, Victoria|Portsea. 

==Cast==
*Zillah Bateman as Phyllis Weston
*John Mayer as Jack Stacey
*Charles Brown as Robson
*Peggy Farr
*Viva Vawden
*Felix St H Jellicoe
*Beverley Usher

==Production==
Advance Films had held a competition to find best new Australian story. 

The film was shot in early 1928 with exteriors filmed at Portsea.

==Release==
The film was released as a supporting feature and also was screened in the UK as a quota film. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 


 