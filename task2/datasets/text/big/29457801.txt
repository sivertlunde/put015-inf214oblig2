Age of Heroes (film)
 
{{Infobox film
| name           = Age of Heroes
| image          = Ageofheroes.jpg
| director       = Adrian Vitoria
| producer       = Lex Lutzus 
James Brown Jamie Carmichael Christopher Figg
| writer         = Ed Scates Adrian Vitoria
| starring       = Sean Bean Danny Dyer Izabella Miko
| music          = Michael Richard Plowman
| cinematography = Mark Hamilton
| editing        = Chris Gill Joe Parsons
| distributor    = Metrodome Distribution
| released       =  
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}} 30 Commando unit during World War II. The film was released in the United Kingdom in 2011. 

==Plot==
Age of Heroes follows the exploits of Corporal Rains (  and capture new radar technology from the Germans which could change the outcome of the war.

The newly formed band of brothers are joined in their task by Norwegian immigrant Steinar (Aksel Hennie) now a US Marine Lieutenant with vital knowledge of the terrain and beautiful spy Jensen (Izabella Miko). All their lethal commando skills are put to the test as they attempt to escape fierce gun battles and attain the German radar technology. The mission objective bears much resemblance to Operation Biting.   Their plan is thrown into crisis when their pick-up is delayed and they must fight their way to the border of Sweden. With the Nazis desperate to hunt them down and stop them, the commandos realise how important their mission is to the war effort, as the fate of the Allies advance into Europe rests in their hands. 

After a long trek in the snow the commandos try to resupply by returning to a previously visited farm. Its inhabitants had been shot by German units after they refused to collaborate with them. To make things worse, the Germans catch up with them at the farm, bringing with them a commando captured alive during an earlier fire fight. The Germans ask for the commandos surrender in exchange for the life of their captured friend. The commandos refuse to surrender. They open fire on the Germans, beginning an intense fire fight. Jensen and radar specialist Rollright are ordered to run for the Swedish border while the rest of the commandos provide cover for them. 

Eventually Major Jones realizes that there are too many German soldiers and that the commandos have insufficient ammunition. He orders Rains and Steinar to run for the border while two others stay to cover them. Those who remain at the farm are last seen firing on and suppressing the Germans. The Germans catch up to Rains and Steinar retreating; Steinar is shot and killed. Out of ammunition, Rains prepares to use his knife to kill himself so the Germans cannot torture him. Before he does so, Jensen and Rollright return and enable the three of them to escape. 

The last scene shows the two surviving team members and Jensen standing on a hill looking down into Sweden.

==Cast==
* Sean Bean as Jones
* Danny Dyer as Rains
* Izabella Miko as Jensen
* James DArcy as Ian Fleming
* Sebastian Street as Colonel Archer William Houston as Mac
* Aksel Hennie as Steinar
* Guy Burnet as Riley
* John Dagleish as Roger Rollright
* Stephen Walters as Brightling
* Daniel Brocklebank as RMP Sergeant Hamilton
* Rosie Fellner as Sophie Holbrook
* Eric Madsen as Teichmann
* Lee Jerrum as Dobson
* Ewan Ross as Gable
* Tom Luke Taylor as Tom
* James Godden as Corporal Jim McGodden III
* Anthony Godsell as Sergeant Anthony McGodsell VII

==Production== Gravesend Civil Defence Bunker, the former Connaught Barracks within Fort Burgoyne in Dover as well as near the village of Pluckley. 

==Release==
Age of Heroes was released in cinemas in the United Kingdom on 20 May 2011.  It was released on DVD and Blu-ray in the UK on 13 June 2011. 

==Reception==
 , Age of Heroes scores an average 40% on Rotten Tomatoes and holds an average three star rating (5.5/10) on IMDb.

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
   
 
   
 
 
 