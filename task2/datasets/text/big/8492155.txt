I Shot Jesse James
{{Infobox film
| name           = I Shot Jesse James
| image	=	I Shot Jesse James FilmPoster.jpeg
| caption        = Film poster
| director       = Samuel Fuller
| producer       = Carl K. Hittleman
| writer         = Homer Croy (article) Samuel Fuller (story and screenplay)
| narrator       =  John Ireland
| music          = 
| cinematography = Ernest Miller
| editing        = 
| distributor    = Screen Guild Productions Inc.
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = $118,000 Low-Budget Movies With POW!: Most fans never heard of director Sam Fuller, but to some film buffs he has real class. Low-Budget Movies
By EZRA GOODMANHOLLYWOOD.. New York Times (1923-Current file)   28 Feb 1965: SM42. 
| gross          = 
}}
 Robert Ford Edward Kelley John Ireland as Bob Ford.
 Criterion Collections Eclipse imprint together with The Baron of Arizona and The Steel Helmet. 

==Plot==
Bob Ford of the Jesse James gang is wounded during a bank robbery. He mends at Jesses home in Missouri for six months, although Jesses wife Zee doesnt trust him.

Cynthy Waters, an actress Bob is in love with, comes to town to perform on stage. Bob catches her speaking with John Kelley, a prospector, and is jealous. He knows that Cynthy wants to get married and settle down.

In need of money, Bob hears of the governors $10,000 reward for Jesse. He betrays his friend, shooting Jesse in the back. Bob is pardoned by the governor but receives a reward for just $500.

He spends the money on an engagement ring. Harry Kane, who manages Cynthys career, books Bob for stage appearances in which he re-enacts the shooting of Jesse. He is booed by audiences and mocked in public for his cowardly deed.

Bob goes to Colorado to try prospecting and runs into Kelley, who is rejecting offers to become Creedes town marshal. Bob wakes up one day to find both Kelley and the engagement ring missing. Cynthy arrives just as Kelley returns, having captured the rings thief. Kelley is disappointed when Cynthy accepts Bobs proposal, so he accepts the job as marshal.

Frank James, brother of Jesse, overhears a conversation in which Cynthy confides to Kelley that hes the one she truly loves. Frank makes sure that Bob learns of this, knowing Bob will make the fatal mistake of confronting Kelley face to face. In the street, Bob draws on Kelley and is shot dead. Kelley gets the girl and Frank avenges his brothers death.

==Cast==
*Preston Foster as John Kelley
*Barbara Britton as Cynthy Waters John Ireland as Bob Ford
*Reed Hadley as Jesse James
*J. Edward Bromberg as Harry Kane
*Victor Kilian as Soapy
*Tom Tyler as Frank James
*Tommy Noonan as Charles Ford (as Tom Noonan)
*Eddie Dunn as Joe, Silver King Bartender
*Margia Dean as Saloon Singer
*Byron Foulger as Silver King Room Clerk
*Jeni Le Gon as Veronica, Cynthys Maid
*Barbara Woodell as Mrs. Zee James (as Barbara Wooddell)
*Phillip Pine as Man in Saloon (as Phil Pine)
*Robin Short as Troubadour

==Production==
Fuller was paid $5,000 to direct. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 