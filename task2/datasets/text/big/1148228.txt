Calamity Jane (film)
{{Infobox film |
| name           = Calamity Jane
| image          = Calamity Jane poster.jpg
| caption        = Theatrical poster
| starring       = Doris Day Howard Keel Allyn Ann McLerie
| writer         = James OHanlon David Butler William Jacobs
| cinematography = Wilfred M. Cline
| editing        = Irene Morra  Howard Jackson
| distributor    = Warner Brothers
| released       = November 4, 1953
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2.5 million (U.S.) 
}}
 Annie Get Your Gun.
 Secret Love", Best Sound, Recording (William A. Mueller).   
 stage musical of the same name that has had a number of productions. The film was also adapted for television in 1963, with Carol Burnett in the title role.

==Plot==
 
Calamity Jane ( ), who is in Chicago. Francis Fryer points out that Adams wouldnt be seen dead in that town but Calamity is still determined to bring her. Wild Bill Hickok (Howard Keel) laughs at the idea and tells Calamity that the night Adams steps on the stage, he will come to the opening dressed as a Sioux squaw lugging a papoose.

Calamity travels to Chicago, where Adams is giving a farewell performance {she was on her way to Europe directly after the performance} Adelaid, sick of the primitive Chicago, gives her costumes to her maid, Katie Brown (Allyn McLerie), who dreams of becoming a stage singer. She tries on one of the dresses and starts to sing.  When Calamity walks in, she mistakes Katie for Adelaid. Katie takes advantage of the error and poses as Adelaid Adams to make her dream come true.

The ride back to Deadwood is rocky, as they are chased by Indians. Later, when Katie gives her first performance, Calamity says she didnt sound that way in Chicago. When Calamity tells Katie to sing out, she bursts into tears and admits that she is not Adelaid Adams. The Golden Garter falls silent. Everyone present is on the verge of rioting, but Calamity fires a shot into the air and defends Katie.  They allow Katie to carry on, and her performance wins them over.  On the balcony above, Bill Hickok, dressed as an Indian woman, ropes Calamity and hangs her high and dry.
 Black Hills of South Dakota.

At the ball, Calamity becomes increasingly jealous while watching Katie and Danny dance together. They adjourn to the garden where, as Danny holds her and moves in for a kiss, Katie can no longer resist him. Angry and betrayed, in the ballroom Calamity shoots the punch cup out of Katies hand and everybody is horrified. Back at the cabin, Calamity throws out Katies things and threatens to shoot her if she ever sees her again, but the scene ends with Calamity in floods of tears.

Calamity later confronts Katie while she is performing, and warns her to get out of town. But Katie is not intimidated.  She borrows a gun from one of the cowboys, and tells Calamity to hold up her glass. Taunting her about being too afraid, she boldly holds it up. A gunshot finally rings out, and the glass falls from Calamitys hand, but it wasnt Katie who fired; it was Bill, who lets Katie take all the credit. Humiliated, Calamity storms out. But before she can mount her horse, Bill grabs her, throws her onto his horse-drawn cart and rides off.

In the woods, Bill tries to talk some sense into Calamity, and reveals that he shot the glass out of her hand to teach her a lesson and that scaring Katie out of town would not stop Danny from loving her. Calamity is heartbroken, and reveals she was crazy about Danny (whom she had earlier saved from capture by Indians), while Bill admits that he was in love with Katie. Calamity tells Bill there wont be another man like Danny, not for her, however she and Bill end up in a passionate embrace and kiss, and she realizes it was him she loved all along. And when Bill asks her what happened to that lieutenant she was telling him about, she answers "Ive never heard of him."
 Secret Love" before she rides into town, but when she talks to the people, they just ignore her. She finds that Katie decided alone to leave for Chicago, feeling guilty about betraying her best friend. Danny is furious with Calamity for driving Katie out of town and demands she listen to him read a note Katie left. Calamity leaps back onto her horse and chases after the stagecoach, eventually catching up with it. She tells Katie she isnt in love with Danny and is marrying Bill, and the two women become friends again.

A double wedding follows. When Bill finds Calamitys gun under her wedding dress, she jokes its just in case any more actresses roll in from Chicago. The movie ends with the two happy couples riding out of town on the stage. 

==Music==
The score, with music by Sammy Fain and lyrics by Paul Francis Webster, includes:
*"Calamity Jane: The Myth, The Woman, The Legend"
*"The Deadwood Stage (Whip-Crack-Away!)"
*"I Can Do Without You"
*"Its Harry Im Planning to Marry"
*"Just Blew in from the Windy City"
*"Hive Full of Honey"
*"My Heart Is Higher Than a Hawk (Deeper Than a Well)"
*"A Womans Touch"
*"The Black Hills of Dakota" Secret Love", Billboard and Cash Box musical charts at number one.
 the album of the same name, though some of the songs from the album were re-recorded rather than taken from the soundtrack.

==Accuracy==
Though the film portrays Calamity Jane and Wild Bill Hickok as lovers, historians have found no proof that they were more than acquaintances. Jane claimed after Hickoks death that she had not only been his lover but also his wife and the mother of his child, but she offered no substantiation of her claims. Many of her contemporaries considered her a teller of tall tales who exaggerated her links to more famous frontier figures, and some insisted Hickok did not even particularly like her. But when she died decades after Hickok, friends buried her beside him at her request.

==Cast==
* Doris Day - Calamity Jane
* Howard Keel - Wild Bill Hickok
* Allyn Ann McLerie - Katie Brown
* Philip Carey - Lt. Daniel Gilmartin Dick Wesson - Francis Fryer Paul Harvey - Henry Miller
* Chubby Johnson - Rattlesnake
* Gale Robbins - Adelaid Adams
* Francis McDonald - Hank
* Monte Montague - Pete
* Bess Flowers - Officers Wife in Reception

==References==
 
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 