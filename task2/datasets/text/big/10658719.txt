Un Kannil Neer Vazhindal
{{Infobox film|
| name = Un Kannil Neer Vazhindal
| image = UnKannilNeerVazhindal.jpg
| caption =
| director = Balu Mahendra
| writer = Balu Mahendra
| story = Akhila Mahendra
| starring =  
| producer =  
| music =    Ilaiyaraaja
| cinematography = Balu Mahendra
| editing = D. Vasu
| studio = Kalakendra Movies
| distributor = Kalakendra Movies
| released = 14 June 1985
| runtime =
| country = India Tamil
| budget =
}}
 Madhavi in the lead with Y. Gee. Mahendra in a prominent role. Rajinikanth plays the role of a police officer and Madhavi as his wife.  Rajini succeeded at the box office.

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enna Desamo || K. J. Yesudas || Vairamuthu || 04:41
|-
| 2 || Ilamai Itho || Malaysia Vasudevan || Gangai Amaran || 04:21
|-
| 3 || Kannil Enna || S. P. Balasubrahmanyam, S. Janaki || Vairamuthu || 04:39
|-
| 4 || Malare Malare || S. Janaki || rowspan=2|Mu. Metha || 04:16
|-
| 5 || Moonu Vela Soru || S. Janaki || 04:46
|-
| 6 || Nethu Varai || Malaysia Vasudevan, S. Janaki || Gangai Amaran || 04:37
|}

==References==
 

 

 
 
 
 
 
 
 


 