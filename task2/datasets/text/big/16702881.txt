King (2008 film)
 
 

{{Infobox film
| name           = King
| image          = King poster.jpg
| caption        = Official Poster
| writer         = BVS Ravi  
| story          = Kona Venkat Gopimohan
| screenplay     = Sreenu Vaitla
| producer       = D.Siva Prasad Reddy
| director       = Sreenu Vaitla Nagarjuna Trisha Trisha Mamta Mohandas Srihari
| music          = Devi Sri Prasad
| cinematography = Prasad Murella
| editing        = M. R. Varma
| art director   = A. S. Prakash
| studio         = Kamakshi Movies
| distributor    =
| country        =   India
| language       = Telugu
| released       =  
| runtime        = 181 minutes
| budget         =  .
| gross          =  .
}}
 Telugu film Nagarjuna Akkineni, Trisha Krishnan|Trisha, Mamta Mohandas, Srihari in lead roles and music composed by Devi Sri Prasad. The film was dubbed into Hindi as King No. 1 and into Tamil as Pudhukottai Azhagan. The films plot-line is loosely based on the 1966 Telugu film Aastiparulu. The film recorded as Super Hit at box-office.

==Plot== Nagarjuna Akkineni) hails from a royal family, having taken over the legacy and riches after the death of his father Raja Ravi Chandra Varma (Sobhan Babu). King has a younger brother Ajay (Arjan Bajwa|Deepak), mother (Geetha (actress)|Geetha), maternal uncle (Dharmavarapu Subramanyam) and his late fathers three sisters, who are married. Their husbands are (Jaya Prakash Reddy, Krishna Bhagawan, Sayaji Shinde). The uncles steal money, which is for the workers. They make it look as though the employee Chandu is responsible. King believes and fires Chandu. Suddenly, King knew that the uncles hid the money in their room. He kept quiet because of his other family members. He didnt want to ruin his familys honor. King and his uncles have to apologize to Chandu. Kings uncles are jealous of his power. Kings brother gets kidnapped by henchmen of a rival businessman named Bhagat. King rescues his younger brother. Kings late dads sisters husbands plan to kill King indirectly. This planned day arrives and King goes away on a business trip where he meets his bodyguard Munna and another employee, Swapna (Mamta Mohandas). Before King leaves airport, this mysterious guy is after King. A few days later, King and Swapna are discussing something next to a graveyard. Same mysterious Guy from airport tries to shoot King, but misses. King chases Shooter to graveyard, but Shooter suddenly disappeared. Munna calls Kings cellphone. King tells Munna that hes in a graveyard. Munna asks, "Are you Ok?" King says, "Im ok." Unfortunately, Swapna shoots King from behind. King sees Swapnas face. King realizes that Swapna was working with Shooter. Swapna shoots King again. Kings presumed dead. Munna tells Police that King went missing. At least 1 day later, when Police takes Munna to Hospital Morgue, a Dead Body looks like King. Munna lies to Police that None of the Corpses is Kings body. 2 of the 3 evil uncles say that the 1st evil uncles responsible for Kings disappearance. 1st evil uncle escapes and its assumed that hes responsible. Mother believes that Kings safe.
 Nagarjuna Akkineni), who falls in love with a singer Shravani(Trisha Krishnan|Trisha). Shravani does not care for a wastrel like Bottu Seenu so Bottu Seenu poses as a software engineer named Sarath to impress her. Bottu Seenu meets real Sarath(Sunil). Shravanis elder brother Gnaneswar (Srihari) knows Bottu Seenu, but he would get mad at Bottu Seenu loving Shravani. Bottu Seenu pretends to be Sarath who wears tie, shirt, and pant, Srihari gets fooled too. Real sarath imposes Bottu Seenu in front of a cop by mistake. Cop mistakes Sarath as Bottu Seenu and beats him up really bad in a hilarious way. Meanwhile, the 1st evil uncle sends goons to look for King, thinking hes still alive. They run into Bottu Seenu, who thrashes them. Same 1st evil uncle proposes a deal to Bottu Seenu, and asks him to pose as King and return to the palace. After Munna informed Kings family that King disappeared, 1st evil uncle tried to escape but he got picked up by Munna. Munna gave a picture of Kings corpse, time, and day to first evil uncle. First evil uncle had nothing to do with Kings murder. Once Bottu Seenu gets access to Kings bank accounts, first evil uncle will be able to repay his debts because he borrowed much money from goons. Shravani and Srihari mistaken Bottu Seenu as Sharath again. They accompany Bottu Seenu to the palace. Swapnas none other than Chandus daughter, but Chandu committed suicide cuz nobody believed that He didnt steal the Money. Swapna and mysterious shooter were hired by Bhagath. Mysterious shooters Baba. Swapnas real names Pooja. Bhagath hires Pooja to kill Ajay. Ajay brings his girlfriend Pooja to his home. Suddenly, Bottu Seenu attacks Pooja, but few seconds later, Bottu Seenu has no idea what happened. Srihari believes that Kings spirit came to get revenge and controlled Bottu Seenu. Bottu Seenu gets Engaged to Shravani. Swapna knows that its not King whos living in the palace right now. Bhagath hires bunch of guys including Baba to dress up in black and kill Kings imposter and Ajay in palace during night. Bhagath thinks that he shouldnt trust girls including Swapna. Bottu Seenus controlled by Kings spirit and saves Ajay from Swapna. Another twist: Ajay met Swapna right before she killed King. She feels that Ajay depends on his brother too much so he told her to kill King. 3rd twist: BS/Sharath is revealed as King. He calms his bro down. Munna captures Baba. Baba reveals his motive for planning to kill Swapna too, and Swapna is Chandus daughter. Swapnas told what really happened that day, but doesnt kill KING. The father and friends who took care of BS were Munnas people. Munnas real name is Raghu rokda. KING says that Ajays mistakes unforgivable. Kings family members are feeling sad, so he decides to leave. He dies in an explosion which takes place in his car. KINGs mother leaves and doesnt want anyone to look for her. Everyone elses probably still mad at Ajay. Fourth Twists dat Kings still alive. He reveals his plan to Shravani only. Same Bhagaths henchman who kidnapped Ajay at beginning of movie, King told him: "I want you to tell me Bhagaths evil plans." After King got shot, Munna took him to hospital. Swapnas details were fake, so they couldnt find her. He decided to play this double role drama. Bhagaths henchman/Kings informer reveals the shooters working for Bhagath too but didnt find details about Swapna. When King met his evil uncles again, none of them had nothing to do with it. King realized that Ajay was behind everything. Kings informer blew up KINGs car after KING escaped. This was KINGs plan too. Informer put a bomb inside Bhagaths car too, so King talks to Bhagath on phone before Bhagath dies. King pretends to be Sarath in front of everyone else.

Final twist: Bottu Seenu/Munnas father and friends were responsible for messing up lights and pulling table with rope in Kings palace. They did it to make it look like Kings spirit came for revenge.

==Cast==
  was selected as lead heroine marking her first collaboration with Nagarjuna.]]
{{columns-list|3| Nagarjuna Akkineni as "King" Raja Chandra Pratap Varma / Bottu Seenu / Sarath
* Trisha Krishnan as Sranvani 
* Srihari as Gnaneswar 
* Mamta Mohandas as Swapna / Pooja
* Brahmanandam as Jayasurya  Sunil as Sarath  Venu Madhav 
* M. S. Narayana  AVS 
* Dharmavarapu Subramanyam  Chandra Mohan   Deepak as Ajay 
* Bharath Dhabolka as Bhagath Seth
* Kelly Dorji
* Jaya Prakash Reddy as Appaji
* Sayaji Shinde as Gopi Mohan
* Surya 
* Benerjee  Krishna Bhagawan as Kona Venkat Ajay as Coal Kittu
* Amith as Bhagath Seths son
* Supreth as Munna 
* Bharat as Appajis son
* Srinivasa Reddy as Jayasuryas PA Pruthvi Raj
* Chitram Seenu as Bottu Sennus henchmen
* Giridhar as Bottu Sennus henchmen
* Rama Chandar as Bottu Sennus henchmen
* Ranam Venu as Bottu Sennus henchmen
* Fish Venkat as Gnaneswars henchmen
* Tarzan as Rowdy
* Gundu Sudarshan 
* Gowtham Raju 
* Duvvasi Mohan as Panthulu
* Chitti as DCP  Jeeva as Mantrik 
* Narsingh Yadav 
* Raavi Kondala Rao as Saraths grandfather 
* Devadas Kanakala as Secretariat Gopal Rao 
* Ramajogayya Sastry as himself  Jenny
* Geetha as Kings mother 
* Sudha as Kings aunt
* Apoorva Kings aunt
* Rajitha as Sravanis aunt
* Surekha Vani 
* Radha Kumari as Saraths grandmother
* Srilalitha as Sravanis friend
* Chitraleka as Auchor
* Master Bharat Kumar as Master
}}

Special Appearance Heroines on "Nuvvu Ready Nenu Ready" song inspired from Hindi movie "Om Shanti Om". 
* Anushka Shetty  
* Charmy Kaur  
* Priyamani  
* Genelia DSouza 
* Sneha Ullal 
* Kamna Jethmalani

==Soundtrack==
{{Infobox album
| Name        = King
| Tagline     = 
| Type        = film
| Artist      = Devi Sri Prasad
| Cover       = 
| Released    = 
| Recorded    = 2008
| Genre       = Soundtrack
| Length      = 40:31
| Label       = Aditya Music
| Producer    = Devi Sri Prasad
| Reviews     =
| Last album  = Ready (2008 film)|Ready   (2008) 
| This album  = King   (2008)
| Next album  = Dasavathaaram   (2008)
}}
Music composed by Devi Sri Prasad. All songs are hit tracks. Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 40:31
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = King
| lyrics1 = Ramajogayya Sastry
| extra1  = Leslie Lewis, Mamta Mohandas
| length1 = 5:27

| title2  = A to Z
| lyrics2 = Ramajogayya Sastry
| extra2  = Naveen, Priya Himesh
| length2 = 4:45

| title3  = O Manmadhuda
| lyrics3 = Ramajogayya Sastry
| extra3  = Sagar,Divya
| length3 = 4:46

| title4  = Yenthapani Chestiviro
| lyrics4 = Sahiti
| extra4  = Vaddepalli Srinivas,Priya Himesh
| length4 = 5:23

| title5  = Ghanana Ghanana 
| lyrics5 = Ramajogayya Sastry 
| extra5  = Ujjayinee
| length5 = 3:38

| title6  = Nenu Nee Raja 
| lyrics6 = Ramajogayya Sastry 
| extra6  = Rahul Nambiar, Andrea Jeremiah
| length6 = 5:17

| title7  = Ghanana (Funny) 
| lyrics7 = Ramajogayya Sastry 
| extra7  = Mamta Mohandas
| length7 = 1:51

| title8  = Nuvvu Ready Nenu Ready 
| lyrics8 = Ramajogayya Sastry 
| extra8  = Shankar Mahadevan,Gopika Poornima
| length8 = 5:31

| title9  = King (The DSP Mix) 
| lyrics9 = Ramajogayya Sastry 
| extra9  = Devi Sri Prasad
| length9 = 3:51
}}

===Crew===
* Director:Sreenu Vaitla
* Story:Kona Venkat, Gopimohan
* Screenplay:Sreenu Vaitla
* Dialogues:B.V.S.Ravi
* Cinematography:Prasad Murella
* Music Director:Devi Sri Prasad
* Action: Ram Laxman

==External links==
*  

 

 
 
 
 
 
 