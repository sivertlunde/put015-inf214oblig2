L'Étudiante (film)
{{Infobox film
| name           = LÉtudiante
| image          = Létudiante.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Claude Pinoteau
| producer       = Alain Poiré
| screenplay     = {{Plainlist|
* Claude Pinoteau
* Danièle Thompson
}}
| starring       = {{Plainlist|
* Sophie Marceau
* Vincent Lindon
* Élisabeth Vitali
}}
| music          = Vladimir Cosma
| cinematography = Yves Rodallec
| editing        = Marie-Josèphe Yoyotte
| studio         = Gaumont Film Company
| distributor    = Gaumont Film Company
| released       =  
| runtime        = 103 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 1,583,100 admissions (France) 
}}
LÉtudiante ( ) is a 1988 French romantic comedy film directed by Claude Pinoteau and starring Sophie Marceau, Vincent Lindon and Élisabeth Vitali.    Written by Claude Pinoteau and Danièle Thompson, the film is about an ambitious teaching student, busy preparing for her final exams, whose studies are interrupted by a passionate affair with a jazz musician. The film was released to French theaters on October 10th, 1988 and sold 1,583,067 admissions in France. 

==Plot==
Twenty-one-year-old Valentine (Sophie Marceau) is a part-time teacher preparing for her all-important final teacher examinations. She meets Edouard (Vincent Lindon), a jazz musician who aspires to be a composer. Despite the fact that the two have different schedules and career agendas, they engage in a passionate affair. Valentine compares her relationship with Edouard to the dry dissertation of Molières The Misanthrope during her oral exams at the Sorbonne.

==Cast==
* Sophie Marceau as Valentine Ezquerra
* Vincent Lindon as Ned
* Élisabeth Vitali as Celine
* Jean-Claude Leguay as Charly
* Elena Pompei as Patricia
* Roberto Attias as Philippe
* Brigitte Chamarande as Claire
* Christian Pereira as Serge
* Beppe Chierici as Lappariteur
* Nathalie Mann as Alexandra
* Anne Macina as Laura
* Janine Souchon as La dame au chien
* Virginie Demians
* Hugues Leforestier as Patron du bouchon
* Jacqueline Noëlle
* Marc-André Brunet as Victor   

==References==
 

==External links==
*  

 
 
 
 
 

 