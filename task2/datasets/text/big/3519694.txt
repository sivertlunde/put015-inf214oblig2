The Passing of the Oklahoma Outlaws
 
  silent western film produced by the Eagle Film Company depicting the end of the outlaw gangs which operated freely during the closing days of the Twin Territories (Indian Territory and Oklahoma Territory). The movie was directed by Bill Tilghman, noted Western lawman, and filmed by Benny Kent, a pioneer movie photographer and Tilghmans neighbor in Lincoln County, Oklahoma.

==Production==
 .]]
Tilghman organized the Eagle Film Company in response to several movies which glamorized outlaws and depicted lawmen as fools. He intended to produce a move that gave a realistic portrayal of outlaws and lawmen. The Passing of the Oklahoma Outlaws, while consisting of many actual events, contains several fictional people and scenes. One of the more famous fictional characters shown is Rose Dunn, the Rose of the Cimarron. 
 101 Ranch, to act in the film. He played himself, and also enlisted Deputy U.S. Marshals Bud Ledbetter and Chris Madsen to take part in the film. Arkansas Tom Jones (Roy Dougherty), the only survivor of the Wild Bunch|Doolin&ndash;Dalton Gang, also played himself.

==Reception== city and state film censorship boards. The Chicago Board of Censors refused to issue a permit allowing the showing of the film because it featured the exploits of a band of train robbers and outlaws. 

Tilghman toured with the film, since he found as many people came to see him and his collection as came to see the film. Most of the film is available from the National Archives, although some of it is damaged and a couple of reels are missing.

==Companion book==
*Graves, Richard S. Oklahoma Outlaws: A Graphic History of the Early Days in Oklahoma; the Bandits Who Terrorized the First Settlers and the Marshals Who Fought Them to Extinction; Covering a Period of Twenty-Five Years. Oklahoma City: State Printing & Publishing Company, 1915.

==References==
 

==External links==  
 

 
 
 
 
 
 

 