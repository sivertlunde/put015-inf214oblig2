El Benny
 
{{Infobox film
| name           = El Benny
| image          = El_Benny.JPG
| caption        = Promotional poster
| director       = Jorge Luis Sánchez
| producer       = Iohamil Navarro
| writer         = Abrahán Rodríguez  Jorge Luis Sánchez Enrique Molina  Carlos Ever Fonseca
| music          = Juan Manuel Ceruto
| cinematography = José Manuel Riera
| editing        = Manuel Iglesias
| distributor    =
| released       = July 2006 (Cuba)
| runtime        =
| country        = Cuba
| awards         =
| language       = Spanish
| budget         =
}}
 Juan Formell, Haila and Orishas (band)|Orishas.
 Palm Springs International Film Festival" on 6 January 2007, and its east coast premiere at the Miami International Film Festival in March 2007.

The director, Sánchez, is distantly related to Benny Moré.

==Cast==

*Renny Arozarena: Benny Moré
*Juan Manuel Villy Carbonell: Benny Moré (singing voice) Enrique Molina: Olimpio
*Carlos Ever Fonseca: Angeluis
*Mario Guerra: Monchy
*Limara Meneses: Aida
*Isabel Santos: Maggie
*Salvador Wood: Grandfather
*Laura de la Uz: Irene
*Kike Quiñones: Pedrito
*Carlos Massola: León Arévalo
*Félix Pérez: Genaro (Bennys grandfather)

*Cheryl Zaldívar: Sofía
*Husmell Díaz: Arnulfo
*Serafín García Aguiar: Gutiérrez
*Marcela Morales: Natalia
*Rakel Adriana: Doñita
*Bárbara Hernández: Lydia (nurse)
*Jorge Ferdecaz: Olegario
*Ulyk Anello: Duany
*Mayra Mazorra: Bennys Mother

==Crew==

*Director: Jorge Luis Sánchez
*Script: Abrahán Rodríguez and Jorge Luis Sánchez
*Executive Producers: Camilo Vives, Simone Haggiag, Alexandre de Lesseps
*Producer: Iohamil Navarro
*Production Manager: Olga María Fernández
*Director of Photography: José Manuel Riera
*Editor: Manuel Iglesias
*Score: Juan Manuel Ceruto
*Production, Direction, Reorchestration and Arrangements of Benny Morés Music: Juan Manuel Ceruto
*Score: Juan Formell, Juan Manuel Ceruto and Eduardo Ramos
*Production Design: Erick Grass
*Costume Design: Nanette García
*Choreography: Isidro Rolando
*Sound Editing and Postproduction: Osmani Olivare
*Production Sound: Ricardo Istueta
*Directors Assistant: Juan Carlos Tellez
*Make Up: Magali Pompa

==External links==
 
* 
* 
* 

 
 
 
 
 
 