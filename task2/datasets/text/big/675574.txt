Rat Race (film)
 
{{Infobox film
| name           = Rat Race
| image          = Rat_Race_poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster Jerry Zucker
| producer       = Jerry Zucker Janet Zucker Sean Daniel
| writer         = Andy Breckman Dave Thomas Wayne Knight John Powell
| cinematography = Thomas E. Ackerman
| editing        = Tom Lewis Fireworks Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 112 minutes  
| country        = Canada United States United Kingdom
| language       = English
| budget         = $48 million
| gross          = $85,498,534 
}} ensemble comedy Jerry Zucker, Lanai Chapman, Dave Thomas, Dean Cain, and Kathy Bates.

The main plot revolves around six teams of people who are given the task of racing 563 miles from a Las Vegas casino to a Silver City, New Mexico train station, where a storage locker contains a duffel bag filled with two million dollars. The first person to reach the locker wins and gets to keep the money. The film has a plot similar to Its a Mad, Mad, Mad, Mad World and Scavenger Hunt.

==Plot== The Venetian Resort Hotel Casino in Las Vegas, devises a new game to entertain the high rollers who visit his hotel. Six special tokens are placed in the casinos slot machines, and the winners are gathered together and told that $2 million in cash is hidden in a duffel bag in a train station locker in Silver City, New Mexico, 563 miles southeast of Las Vegas. Each team is given a key to the locker and told to race across the desert to the train station and claim the money. Unbeknownst to the competitors, Sinclairs wealthy patrons are placing bets on who will win.

Among the racers are scheming siblings Duane and Blaine Cody, businesswoman Merrill Jennings and her estranged mother Vera, disgraced American football referee Owen Templeton, the Pear family led by opportunistic father Randy, eccentric Italian tourist Enrico Pollini, and no-nonsense Nick Schaffer.

Unable to get on the earliest flight, Duane and Blaine manage to destroy the radar with their Ford Bronco, grounding everybody else. However their car is wrecked in the sabotage, so they steal another before deciding to split up to double their chances of winning by creating a replica key. The locksmith overhears their plan, and makes off with the key, trying to escape in a hot air balloon. Duane and Blaine catch up to him, leaving the locksmith and a stray dairy cow hanging from the balloons anchor rope. The brothers later accidentally swerve their vehicle into a monster truck rally, where it is destroyed. They therefore steal a monster truck and continue on to Silver City.

Merrill and Vera crash their car thanks to malicious road directions given by a crazed squirrel saleswoman. They steal a rocket car, which races across the desert until it runs out of fuel. The women dizzily stumble onto a bus full of mental patients which eventually drives toward Silver City.

Owen is left stranded in the desert by a vengeful cab driver who lost $20,000 on his bad call in a football game. He comes across a coach bus filled with Lucille Ball impersonators going to an I Love Lucy convention and disguises himself as the driver. Just outside Silver City, the bus hits the cow dangling from the hot air balloon, swerving off the road and suffering a puncture (and eventually rolling over). Owen breaks down, reveals he is not a coach driver, and is forced to flee from the women on foot. After hiding, he steals some clothes from a scarecrow and rides a stolen horse the rest of the way.

Randy Pear deceives his family into accompanying him in the race, but they mistakenly visit a museum dedicated to the Nazi Klaus Barbie. After the Cody brothers vandalize their car, the Pears steal Adolf Hitlers staff car to continue. Randy accidentally insults a biker gang and they attack, causing the Pears to crash into a meeting for World War II veterans who believe the family to be Nazis after seeing Randy exiting Hitlers car while unknowingly resembling Hitler. After being chased away, the family tell Randy they want to stop the race, but he drugs them with sleeping pills, and bundles them into a semi-truck to reach Silver City.

Nick refuses to participate until he meets pilot Tracy Faucet, one of the few still able to fly using her non-fixed-wing helicopter, and persuades her to give him a lift. They pass over Tracys boyfriends house but spot him with his ex-girlfriend, enraging Tracy to the point that she attacks and chases him in the helicopter. It stalls out, causing her and Nick to steal her boyfriends truck. Now without a job and on the run, Tracey accompanies Nick to Silver City.

Narcoleptic Enrico falls asleep at the start of the race but awakens hours later. He rushes out of the casino to be run down by ambulance driver Zack, who is delivering a transplant heart to El Paso. Wishing to avoid trouble, Zack agrees to take Enrico to Silver City. When Zack shows off the heart, it bounces out the window and a stray dog picks it up; Enrico plays fetch with it, until the dog gets killed by an electric fence. Zack considers removing Enricos heart to replace the first, before Enrico flees onto a passing train. In despair Zack touches the electric fence, which brings the retrieved heart back to life.

All the racers reunite in Silver City, most running side-by-side on foot toward the station. Enrico arrives first by train, only to fall asleep with his key in the locker. The others show up and tackle each other to open the locker, only to find it is empty. They all run outside and find Sinclairs assistant Grisham and a call girl he hired making off with the money bag. They lose it when the locksmith ties it to the balloon, only for the three to crash their car.
 All Star" and crowd surfing in the exuberant audience.

==Cast==
;Main
 
* Rowan Atkinson as Enrico Pollini, an Italian with narcolepsy
* Whoopi Goldberg as Vera Baker, a kindhearted mother
* Breckin Meyer as Nick Schaffer, an uptight attorney
* John Cleese as Donald P. Sinclair, a Las Vegas billionaire
* Amy Smart as Tracy Faucet, a helicopter pilot
* Cuba Gooding Jr. as Owen Templeton, a disgraced football referee
* Seth Green as Duane Cody, a neer-do-well
* Vince Vieluf as Blaine Cody, Duanes unintelligible brother
* Jon Lovitz as Randy Pear, an irresponsible tourist
* Kathy Najimy as Bev Pear, Randys wife Lanai Chapman as Merrill Jennings, a businesswoman, and Veras biological daughter Dave Thomas as Grisham, Sinclairs assistant
* Wayne Knight as Zack Mallozzi, a medical transport van driver
 

;Minor
 
* Brody Smith as Jason Pear
* Jillian Marie Hubert as Kimberly Pear
* Paul Rodriguez as Gus, a cabbie
* Dean Cain as Shawn Kent, Tracys boyfriend
* Brandy Ledford as Vicky, a call girl Silas Weir Mitchell as Lloyd, the locksmith
* Colleen Camp as Rainbow House Nurse
* Deborah Theaker as a Lucy
* Gloria Allred as herself
* Kathy Bates (uncredited) as the Squirrel Lady Diamond Dallas and Kimberly Page (deleted scenes) as themselves
 

==Production notes== Best Supporting Actress for Ghost (1990 film)|Ghost).
* Rat Race is also the first film to feature three actors who had previously won   for Misery (film)).
* Lawyer Gloria Allred is featured in two scenes. The first is when she happens to be nearby when a woman slips on an overturned empty shot glass (meant for Blaine Cody) and is injured falling down a flight of stairs, prompting Allred to immediately take up the womans case against the hotel. The second is when Allred is on a hotel balcony as Enrico Pollini is hit by Zacks van, whereupon she is eager to handle Pollinis lawsuit.  Monopoly with real money. In another scene, a high roller pretends to find what they are doing immoral.
* Professional wrestler Diamond Dallas Page and his wife, Kimberly, had a cameo that was cut when test audiences failed to give his appearance any reaction. The scene is available on the DVD release.
* R&B singer, Aaliyah, was originally intended to have the part of Tracy, but she was already committed to Queen of the Damned. Natalie Portman was another contender, but the part was ultimately given to Amy Smart. 
* Vieluf lost billing when his agent attempted to secure him star billing. As a result, Vieluf was left out of all promotional material, even though his character was seen in a majority of the film. Vieluf later fired that agent. 
* Director Zuckers late mother, Charlotte, made a cameo appearance as one of the Lucille Ball impersonators. the hotelier of the same name, the presumed inspiration for Cleeses famous Basil Fawlty character from Fawlty Towers.
* This is the fourth film that actresses Goldberg and Najimy were cast in together, with their previous efforts being  . Road Trip, released the previous summer (2000).
* This film reunited Chapman and Goldberg who worked together on  .
* Meyer and Najimy would reunite on Meyers legal comedy series Franklin & Bash with Meyer playing attorney Jared Franklin and Najimy in a recurring role as Judge Sturges.

;Filming locations
* The climactic railroad station scenes in Silver City, New Mexico were filmed at the restored train station at East Ely, Nevada, a popular tourist destination, as Silver City has no real train station.
* The scene with the coin toss by Owen Templeton was filmed at McMahon Stadium in Calgary, Alberta, Canada.

==Reception==

===Box office===
Rat Race was released in both Canada and the United States on August 17, 2001 and took in USD$11,662,094 in its opening weekend at the U.S. Box office, landing at #3 behind American Pie 2 and Rush Hour 2,  and ultimately making approximately $85.5 million worldwide,    based on a budget of an estimated $48 million.

===Critical reviews=== sight gag to another, but only a handful of them are genuinely funny."  On Metacritic, which uses an average of critics reviews, the film holds 52/100, indicating "mixed or average reviews". The movie has, however, gained popularity in recent years due to its suitability as a "drinking game". The game consists of participants drinking alcoholic beverages and betting either sums of money or setting challenges for the other participants, usually to "neck" their beverage, based on the outcome of the race. The popularity of this drinking game was short lived due to the outcome of the race becoming widely known. 

==See also==
* The Amazing Race
* Its a Mad, Mad, Mad, Mad World – a comedy film with a similar plot
* Scavenger Hunt
* The Cannonball Run
* Raging Bull

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 