Grow Your Own (film)
 
 
{{Infobox Film 
| name     = Grow Your Own
| image          = Growyourowndvd.jpg
| caption        = DVD cover
| producer         = Luke Alkin  Carl Hunter  Barry Ryan
| director       = Richard Laxton 
| writer         = Frank Cottrell Boyce Carl Hunter Philip Jackson Olivia Colman Joanna Scanlan
| music         = 
| cinematography = David Luther
| editing        = Joe Walker
| distributor    = Pathé Warp Films 
| released   = 15 June 2007
| runtime        = 97 min. 
| budget         = £2.5 million   
| gross  =   £103,777   English
| country = United Kingdom
}} Philip Jackson, and Olivia Colman. The film centres on a group of gardeners at a Merseyside allotment, who react angrily when a group of refugees are given plots at the site, but after they get to know them better, soon change their minds. The film was previously known under the title The Allotment. 

==Production==
The original idea for the film came from Carl Hunters involvement with the Merseyside community group "Art in Action". With the project he had worked with a number of refugees who had taken up residence in Liverpool. The refugees were each given an allotment as part of a Liverpool City Council initiative. This led Hunter to produce a series of documentaries about the lives of the refugees entitled Putting Down Roots, they were aired as part of the "3-minute wonder" slot on Channel 4.  Frank Cottrell Boyce saw some potential in the concept and asked Hunter if he wished to work with him to turn the real life story into a film. The film was supported by North West Vision, BBC Films and the UK Film Council.  
Shooting for the film began on 14 August 2006, taking place for six weeks.  All filming took place in Merseyside, with the shoot providing numerous jobs for locals. 

==Reception==
Leigh Singer gave the film four stars, calling it "a gentle, astute, life-affirming British comedy."  Tom Hawker gave it three stars, praising Eddie Marsans performance and stating "Grow Your Own has about as much edge as a prize melon, but even if the lands been well filled, theres still plenty of fertile soil here. Occasionally melancholy, often funny, this is touching, lyrical home-grown fare."  Kevin Maher gave three stars, noting his favourite part of the film as being "a serious discussion between the gardeners about Bob the Builder."  Anthony Quinn of The Independent criticised the film citing "one could wish that this parable of difference and tolerance gladdened the heart, but its effortful comedy has quite the opposite effect", as well as expressing his distaste for the films score.  Catherine Chambers also disliked it, stating "Grow Your Owns twee optimism is sometimes a little too much to digest." 

==References==
 

==External links==
*  
* 
*  at the BBC

 
 
 
 
 
 
 
 
 