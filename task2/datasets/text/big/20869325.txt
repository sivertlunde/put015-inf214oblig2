The Tortoise and the Hare (film)
 
{{Infobox Hollywood cartoon
| cartoon_name = The Tortoise and the Hare
| series = Silly Symphonies
| image = Tobymax.jpg
| caption = Toby Tortoise and Max Hare
| director = Wilfred Jackson
| story_artist = Larry Clemmons
| animator = Hamilton Luske, Larry Clemmons, Ward Kimball, Dick Huemer, Gilles Armand Rene ("Frenchy") de Tremaudan
| musician = Frank Churchill
| producer = Walt Disney Walt Disney Productions
| distributor = United Artists
| release_date =  
| color_process = Technicolor
| runtime = 8 minutes 17 seconds
| movie_language = English
}}

The Tortoise and the Hare is an  . This cartoon is also believed to be one of the inspirations for Bugs Bunny, who first appeared in 1940.  Barrier (2003), p. 359-362 

== Summary ==
Unlike the original fable, the race is a major sporting event, as opposed to a challenge between the hare and the tortoise. Max Hare is the heavy favorite to win---hes cocky, athletic and incredibly fast. His challenger, Toby Tortoise, is teased and jeered for being sluggish and clumsy. He does seem to have the ability to stretch, which comes in handy in certain situations. Max tells Toby that he intends to play fair, but it seems obvious that Max is just out to humiliate his competition. The race begins and Max zooms off. It takes an extra nudge from the starter to get Toby going.
 even knocking a tennis ball into the camera in the process!).

Just then, Max hears the crowd cheering and sees that Tobys not far from the finish line. He bids the girls farewell and charges off, still confident that he will win easily. Toby sees Max catching up and picks up his pace by stretching his legs. In the end, the race is close. Max crosses the finish line and skids to a rough halt. Once he gets up and dusts himself off, he realizes that he lost by a "necks length." The crowd rushes to congratulate the winner: Toby Tortoise.

==Other appearances==
Toby Tortoise and Max Hare reappear in the cartoon short Toby Tortoise Returns.

The girl bunnies and the animal pedestrians make cameos in Toontown, while Toby Tortoise is on a poster in the toontown alleyway and appears during the final scene of Who Framed Roger Rabbit. Toby and Max also appear as guests in Disneys House of Mouse.

==Sources==
*  

==External links==
*  
*  

==References==
 

 
 

 
 
 
 
 
 
 
 
 