Cannibal Ferox
 
 
{{Infobox film
| name            = Cannibal Ferox
| image           = Cannibalferoxposter.jpg
| image_size      = 220px
| border          = yes
| alt             = 
| caption         = Theatrical release poster
| director        = Umberto Lenzi
| producer        = Mino Loy Luciano Martino Antonio Crescenzi
| writer          = Umberto Lenzi John Morghen Zora Kerowa Walter Lloyd Robert Kerman
| music           = Budy Maglione
| cinematography  = Giovanni Bergamini
| editing         = Enzo Meniconi
| distributor     = Grindhouse Releasing
| released        =  
| runtime         = 93 minutes 83 minutes    
| language        = Italian Spanish English
}} banned in 31 countries", some of which lifted their bans only recently.

==Plot==
The film begins in New York City. A man buys heroin from his dealer, Mike. The man meets two mobsters who are also looking for Mike, because he owes them $100,000. The mobsters shoot the man and leave him for dead.

In Paraguay, siblings Rudy and Gloria and their friend Pat prepare for a journey into the rain forest. They plan to prove Glorias theory that cannibalism is a myth. The trio encounter Mike and his partner Joe. Joe is badly wounded; Mike explains that they were attacked by cannibals. During the night, Gloria goes missing and Rudy finds a native village while looking for her.

Due to Joes injuries, the travelers decide to stay in the nearly deserted village. Mike seduces the naive Pat. In a cocaine-fueled rage, he encourages Pat to kill a native girl. She is unable to do it, so Mike kills the girl himself. In his dying moments, Joe reveals that he and Mike were responsible for the cannibals aggression. They came to the region to exploit the natives for emeralds and cocaine, taking advantage of their trust in white men. One day, while high on cocaine, Mike brutally tortured and killed their native guide in full view of the tribe. A badly charred body, previously believed to be that of a different guide, is actually this native. Mike kidnapped a native girl to lead them out of the jungle, but the outsiders were followed and attacked.

Following the latest murder, the natives hunt the outsiders. Joe dies of his wounds, and his body is found and cannibalized by the natives in full view of Rudy and Gloria, who are hiding from the natives. Mike and Pat abandon the others, but all are captured by the natives and forced into a cage. The prisoners are forced to watch Mike as he is tortured and beaten. The natives transport their prisoners to another village, but Rudy manages to escape. He is caught in a booby trap in the jungle, and his bleeding wounds attract piranhas. The natives shoot him with a poisoned dart, and he dies instantly.

Pat and Gloria are put in a hole in the ground. Mike is placed in a separate cage. A native man, whom Pat had saved from Mikes aggression, lowers a rope into the hole so the women can escape. Mike digs his way out of the cage, chases the man away and cuts the rope, preventing the women from escaping. Mike flees into the jungle, where he tries to attract the attention of a search and rescue plane, but he is recaptured. The natives sever one of his hands and drag him back to the village. The search plane lands, but the natives tell the rescuers that the outsiders canoe capsized in the river and they were eaten by crocodiles.

After the search team leaves, Pat is bound and stripped and the natives run hooks through her breasts. Gloria is forced to watch as she slowly bleeds to death. Mikes head is locked in a crude apparatus and the top of his skull is removed so that the natives can eat his living brain. During the night, the sympathetic native returns and frees Gloria. He guides her through the jungle, but falls victim to one of the natives booby traps. Gloria eventually encounters a pair of trappers, who take her to safety. Instead of telling the true story, she recounts the natives lie about the others being eaten by crocodiles.

Gloria, deeply disturbed by her experiences, returns to civilization. She publishes a book titled, Cannibalism: End of a Myth, which lies to support her theory and covers up the events of her ordeal.

==Cast== John Morghen as Mike Logan
** Frank von Kuegelgen (uncredited English dub voice) as Mike Logan
* Lorraine De Selle as Gloria Davis
* Bryan Redford as Rudy Davis Zora Kerowa as Pat Johnson
* Walter Lloyd as Joe Costolani
* Robert Kerman as Lt. Rizzo
* Meg Fleming as Myrna Stenn
* John Bartha as Brooklyn mobster
* Venantino Venantini as Sgt. Ross
* El Indio Rincon as Native who helps Gloria
* Dominic Raacke as Tim Barrett

==Release== transgressive imagery video nasties BBFC for a rating.

In the United States, Cannibal Ferox (aka Make them Die Slowly) "original, uncensored directors cut" was released by Grindhouse Releasing in the late 1990s. Grindhouse Releasing is still the sole official licensed distributor of the film in North America. On May 22, 2015, Grindhouse Releasing will release the film in a 3 Disc Blu-Ray/DVD feature, the films first time on Blu-ray Disc.  The Blu-ray featured the documentary film Eaten Alive! The Rise and Fall of the Italian Cannibal Film and 12 page booklet. 

==Critical reception==
  AllMovie called the film "revolting," but "nauseatingly effective," though noting that it is "primarily a showcase for the gory special-effects artistry of Gianetto de Rossi". 

==Music==

The films soundtrack was written and performed by Roberto Donati and Fiamma Maglione (credited under their joint alias Budy Maglione). The score was first released in 1998 on CD by Blackest Heart Media. This CD version also contains Fabio Frizzis score for Zombie. In 2014 the Cannibal Ferox soundtrack was re-released on vinyl and cassette by One Way Static Records. The vinyl edition contains four bonus tracks and extensive liner notes by Roberto Donati and the films cast and crew.

==See also==
* Cannibal boom
* Cannibal film
* Video nasty

==References==
 

==External links==
*  
*  
*  
*  
*   is available for free download at the Internet Archive
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 