All Together Now (film)
{{Infobox film
| name           = All Together Now
| image          = Beatles film 2008.gif 
| caption        = DVD release cover
| director       = Adrian Wills
| producer       = 
| writer         = 
| narrator       = 
| starring       = The Beatles, George Martin, Yoko Ono, Olivia Harrison
| music          = John Lennon, Paul McCartney, George Harrison, Ringo Starr
| cinematography = Alain Julfayan
| editing        = 
| distributor    = 
| released       =  
| runtime        =  86 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}} album of the same name. The film is dedicated to the memory of Neil Aspinall, a former assistant to the band and eventual head of Apple Corps.

==Background and release==
All Together Now recounts how the Love project came into being, borne from the personal friendship between George Harrison and Cirque du Soleil founder Guy Laliberte. George saw how the twin talents of Cirque’s artistry and The Beatles’ music could be fused into something new and totally original. The title of the DVD refers to The Beatles song "All Together Now".

The director, Adrian Wills, records early meetings between the Cirque & Apple Corps creative teams, as well as contributions from Sir Paul McCartney, Ringo Starr, Yoko Ono Lennon and Olivia Harrison discussing how The Beatles music can be used in a different way. The film touches on  the  decision to utilize the combined talents of Sir George Martin and his son Giles Martin to produce what became a 90-minute soundscape created from The Beatles multi-track recordings and how this new audio adventure was being quietly worked on in the famous Abbey Road Studios in London, England whilst the first creative ideas for the show were being formulated in Montreal, Canada.
 Las Vegas, which was completely rebuilt with a one-of-a-kind sound system and complex round staging to house the Love show. George and Giles Martin, the show’s musical directors, were involved with the Cirque du Soleil creative team, performers and backroom staff.

The DVD was released on 20 October 2008. 

The DVD won the Grammy Award for Best Long Form Music Video, in 2009, for the director, Adrian Wills, and the producers, Martin Bolduc and Jonathan Clyde. The Beatles did not receive the award as they did not perform in the video. They did however receive an award in the same category several years earlier for the Beatles Anthology Video release.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 