Je suis né d'une cigogne
{{Infobox film
| name = Je suis né dune cigogne
| image = Je suis né dune cigogne (cover).jpg
| alt = DVD cover
| caption = 
| director = Tony Gatlif
| producer = Tony Gatlif
| writer = Tony Gatlif
| starring = Romain Duris Rona Hartner Ouassini Embarek
| music = Marc Nouyrigat
| cinematography = Claude Garnier Eric Guichard
| editing = Monique Darton
| studio = Production Princes
| distributor = Mondo Films
| released =  
| runtime = 80 minutes
| country = France
| language = French
| budget =
}}
Je suis né dune cigogne ( ) is a 1999 French road movie directed by Tony Gatlif, starring Romain Duris, Rona Hartner, Ouassini Embarek, Christine Pignet and Marc Nouyrigat. Following its French release, it received mixed reviews but was nominated for a Golden Bayard at the International Festival of Francophone Film in Namur, Belgium. 
 New Wave techniques in this film.   

== Plot ==
Two French pals, one an unemployed young man named Otto (Romain Duris) living with his mother in state housing, and the other his girlfriend Louna (Rona Hartner), who is a hairdresser and has the bailiffs after her, reflect on the lack of meaning in their lives, their society and the system. In a spirit of rebellion against everything, they hit the road and what follows is an anarchic adventure. A teenage Arab immigrant named Ali (Ouassini Embarek) enters the story. Alis family tries to hide its ethnic origins by going to extreme measures in switching to French customs. 

The trio start wreaking havoc, robbing shops and stealing cars. On their way, they come across an injured stork with a broken wing. The stork speaks to them and says that it is an Algerian refugee, on its way to Germany to reunite with its family. The trio adopt the stork as their father, name it Mohammed, and forge a passport to enable the stork to cross the French–German border. 

== Casting and characterisation ==
The films four main characters represent the "most vulnerable sections" of society, in tune with Gatlifs earlier films portraying "social outcasts and racial minorities".   Otto represents the section of unemployed youth who are neither rich nor qualified, with no hopes for a job in the future. Louna represents the underpaid who are exploited by their employers. The above characters are played by the same duo, Romain Duris and Rona Hartner, who played the leading roles in Gatlifs previous film, Gadjo dilo.   The third character, the Arab immigrant, Ali (played by Ouassini Embarek), is going through an identity crisis and has run away from his family, who are trying to distance themselves from their ethnic origins by, for example, adopting French names. Ali is shown to be interested in current affairs and is also shown reading Karl Marx. The other character, the stork, represents illegal immigrants. 

The film encountered production problems due to a quarrel between Rona Hartner and Gatlif which led to her walking out midway. This resulted in her abrupt disappearance from the plot in the middle until they patched up much later. 

== Themes and analysis == New Wave" technique of early films by Jean-Luc Godard|Godard, to explore themes of border crossings and social alienation. 

=== Gatlifs take on the New Wave ===
The reviewer in Film de France remarked that with its themes like absurdity and nonconformity, making use of characters like a speaking stork,   and also its filming techniques like jump cuts and multiple exposures, the film feels like "a blatant homage to the works of Jean-Luc Godard", and the plot "looks like a crazy mélange of Godards À bout de souffle, Pierrot le Fou and Weekend (1967 film)|Weekend".  In the reviewers opinion, Gatlif has overdone these techniques, leading to the films ending up "far more substantial and worthy than a shameless appropriation of another directors technique".  ACiD Productions|ACiD remarked that with his boldness and unconventional style, Gatlif has started a new New Wave trend, which would serve as a notice for both amateur filmmakers and professional film-makers.  Chronicart remarked that the film can be placed between the worse and the better among the works inspired by Godard. Though the filming techniques are similar to Godards, the film falls short in its dealing with the unconventional themes, avoiding providing solutions, and rather ending up being a mere "passive acquiescence" reflecting on the works of revolutionaries of the era, which is far from rising up to revolt as one would expect in a Godard movie.    Time Out London was also critical of Gatlifs attempts at Godard, calling it "offbeam". 

=== Satirical elements ===
The film is packed with a number of references to "social issues and political theory", especially on the border crossings.   Yet a reviewer for Films de France found it to be not so "heavy", thanks to the unintentional flaws in the techniques used.  He observed that the film treats them using "  called this "poetic"  while Time Out London found it "woolly and unilluminating".  The word cigogne is pronounced very similarly to tsigane which is one of the words used for Romani people. There are also a number of "in-jokes and references to French cinema" which a viewer might miss in the first viewing, observed Films de France, citing scenes such as one which is a parody on an awards ceremony and one of an austere reviewer "rubber stamping films with trite stock phrases".  Chronicart found these scenes heavy because of the limitations of a work in which the director "at his pleasure distills his personal tastes". 

=== Political alienation === CRS personnel NF activists clichéd. 

== Release ==
The film was screened at the 1999 Festival International du Film Francophone de Namur, in Belgium, competing against films from Québec, France, Vietnam, Belgium, Sénégal and Egypt for the Golden Bayard award in the Best Film category, which was won by Christine Carrières Nur der Mond schaut zu.    The film received rave reviews for its rare courage in presenting disconcerting themes such as unemployment and illegal immigration.    In 2000, it was screened at the International Film Festival Rotterdam in the official section and received praise for its unconventional elements, such as the talking stork.   
 Festival Internacional de Cine de Río de Janeiro sreened the film in the non-competitive Panorama du cinéma mondial section, along with 27 other films from around the world.    Romani themes.   

== Reception ==

=== Critical reception === Godard as a failure.    ACiD Productions|ACiD gave it a positive review, lauding Gatlifs bold depiction of absurdity. Romain Duris and Rona Hartners performance was described as "beautiful" and as complemented by Ouassini Embareks, which was described as "brilliant". In summary, the reviewer suggested the film be called "The Good, the Bad and the Ugly", citing the mixed topics dealt with, and added that it takes the viewers "beyond the real, beyond the borders and everything one can imagine".   

A review by James Travers forFilms de France called it the "most unconventional" of all road movies, with its "insanely anarchic portrait of adolescent rebellion", adding that it is an "ingenious parable of social exclusion and immigration in an uncaring society". Travers also wrote that the films editing and narrative techniques turn into a plus, making it "refreshingly fresh and original", adding that the "patchwork narrative style" suits the rebellious nature of the characters very well. Owing to the unconventionality of the film, Lounas disappearance from the plot in the middle does not look very obvious, he added.  Les Inrockuptibles called it a "tragicomic fable on the notions of borders and free movement of people" and added that the films use of comedy and disjunctive narrative style is only partially successful. Though not conventionally beautiful, the  film impresses the viewers with its "energy, boldness and humor in places when it doesnt leave them stranded", the reviewer concluded. 

== References ==
 

==External links==
* 

 

 
 
 
 
 