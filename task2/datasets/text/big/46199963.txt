The Mistress of Shenstone
{{Infobox film
| name           = The Mistress of Shenstone
| image          = The Mistress of Shenstone (1921) - Ad 2.jpg
| caption        = Henry King
| producer       = Robertson-Cole Pictures Corporation
| writer         =
| based on       = novel, The Mistress of Shenstone, by Florence L. Barclay Roy Stewart
| music          =
| cinematography = J. Devereaux Jennings
| editing        =
| distributor    = Robertson-Cole Distributing Corporation
| released       = February 27, 1921
| runtime        = 6 reels
| country        = USA
| language       = Silent..English, Spanish(Spain version)
}} Henry King Roy Stewart.   

It is a surviving film but in an abridged version in a Spanish archive, Filmoteca de Catalunya. 

==Cast==
*Pauline Frederick - Lady Myra Ingleby
*Roy Stewart - Jim Airth
*Emmett C. King - Sir Deryck Brand
*Arthur Clayton - Ronald Ingram
*John Willink - Billy Cathcart
*Helen Wright - Margaret OMara
*Rosa Gore - Amelia Murgatroyd
*Helen Muir - Eliza Murgatroyd
*Lydia Yeamans Titus - Susannah Murgatroyd

==References==
 

==External links==
* 
* 
*  by Greta de Groat

 

 
 
 
 
 
 

 
 