Solo Sunny
{{Infobox film
| name           = Solo Sunny
| image          = 
| caption        = 
| director       = Konrad Wolf Wolfgang Kohlhaase
| producer       =  Dieter Wolf
| starring       = Renate Krößner
| music          = 
| cinematography = Eberhard Geick
| editing        = Evelyn Carow
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = East Germany
| language       = German
| budget         = 
}}

Solo Sunny is a 1980 East German drama film  directed by Konrad Wolf and Wolfgang Kohlhaase. It was entered into the 30th Berlin International Film Festival, where Renate Krößner won the Silver Bear for Best Actress.   

==Cast==
* Renate Krößner - Sunny
* Alexander Lang - Ralph
* Dieter Montag - Harry
* Heide Kipp - Christine
* Klaus Brasch - Norbert
* Hansjürgen Hürrig - Hubert
* Harald Warmbrunn - Benno
* Olaf Mierau - Udo
* Ursula Braun - Frau Pfeiffer
* Regine Doreen - Monika
* Klaus Händel - Bernd
* Rolf Pfannenstein - Ernesto
* Bernd Stegemann - Detlev
* Fred Düren - Doktor
* Ulrich Anschütz - Grafiker

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 


 