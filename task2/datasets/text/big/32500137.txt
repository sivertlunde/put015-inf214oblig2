Way Back Home (2011 film)
{{Infobox film
| name           = Way Back Home
| image          = WayBackHome2011.jpg
| caption        = Theatrical movie poster
| director       = Jerry Lopez Sinenen
| producer       =  
| writer         = Dan Bagas Karen Ramos Vanessa Valdez
| music          = Jessie Lasaten
| starring       =  Tonton Gutierrez
| cinematography = Julius Palomo Villanueva
| editing        = Marya Ignacio
| studio         = Star Cinema
| distributor    = Star Cinema
| released       =    
| runtime        = 120 minutes
| country        = Philippines
| language       =  
| budget         =
| gross          = P25.7 million
}} 2011 Cinema Filipino family family drama film. The film was released by Star Cinema and premiered on August 17, 2011.   retrieved on July 23, 2011 via www.pep.ph   

The film follows the story of two sisters, Ana Bartolome (Kathryn Bernardo) and Jessica Santiago (Julia Montes), who have been separated for twelve years and grew up in two totally different worlds. 

==Production== Mara Clara, ABS-CBN decided to launch the lead cast of the series to film, Bernardo and Montes. In February 2011, the cast was announced that the two main characters will be portrayed by Bernardo and Montes and supposedly together with AJ Perez and Albie Casino.   But in April 17, 2011, Perez died due to a car crash.    retrieved on May 6, 2011 via http://push.com.ph  
 Mara Clara.

==Cast and characters==
*Kathryn Bernardo as Joanna Santiago/Ana Bartolome
*Julia Montes as Jessica "Jessie" Lorraine S. Santiago
*Enrique Gil as Michael Estacio
*Sam Concepcion  as AJ Delgado
*Agot Isidro  as Amelia "Amy" Santiago
*Tonton Gutierrez as Ariel Santiago
*Lotlot De Leon as Lerma Bartolome
*Clarence Delgado as Buboy Bartolome
*Bella Flores as Lola Nita
*Ahron Villena as Jeffrey Santiago
*Jairus Aquino as Junior Bartolome
*Josh Ivan Morales as Uncle Dado
*Gilleth Sandico as Aunt Tida
*Mickey Ferriols as Bettina
*Kyle Danielle Ocampo as Young Jessica Santiago
*Veyda Inoval as Pochay
*Veronica Louise Bernardo as Young Joanna Santiago
*Cecil Paz as Yaya Minda
*Earl Christian Periquet as Young Jeffrey Santiago
*Ray An Dulay as Berto
*Kristel Fulgar as Froggy
*Katrina "Hopia" Legaspi as Yvette

==Reception==
===Launch=== Multiply site.  The official cinematic trailer was released on the July 30 episode of the noontime variety show, Happy Yipee Yehey. It was first reported that the soundtrack of the film, "Youre My Home" will be covered by Maria Aragon, but later on showed in the films music video that Angeline Quinto will sing the theme song, which was originally sang by Lea Salonga. 

===Critical response===
On a film review done by Noel Orsal of Philippine Entertainment Portal he quoted, "The typical Filipino film is at home in portraying families. It brings out one of the best dramatic scenes and emphatic dialogues primarily because relationships within the family can relate to. Conflicts and dysfunction between parent and child or among siblings would always find an audience connecting to a particular character over another." 

===Box office===
The film opened at first place with an P18 million within its first week surpassing   film Wedding Tayo, Wedding Hindi.

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 