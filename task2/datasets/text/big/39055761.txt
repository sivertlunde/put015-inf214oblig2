Good Vibrations (film)
 

{{Infobox film
| name = Good Vibrations
| image =
| caption = Theatrical release poster
| director = Lisa Barros DSa Glenn Leyburn  David Holmes Bruno Charlesworth
| writer = Colin Carberry Glenn Patterson Michael Colgan Adrian Dunbar Liam Cunningham Dylan Moran Karl Johnson
| cinematography = Ivan McCullough
| visual effects = Blacknorth
| editing = Nick Emerson
| distributor = The Works
| released =  
| runtime = 102 minutes
| country = UK/Ireland
| language = English    
}}
 David Holmes. Holmes also co-wrote the soundtrack score.   

== Plot ==
 

Terri Hooley (Richard Dormer|Dormer) is a radical, rebel and music-lover in 1970s Belfast     when the bloody conflict known as The Troubles shuts down his city. As all his friends take sides and take up arms, Terri opens a record shop on the most bombed half-mile in Europe and calls it Good Vibrations. Through it he discovers a compelling voice of resistance in the citys nascent underground punk scenes. Galvanising the young musicians into action, he becomes the unlikely leader of a motley band of kids and punks who join him in his mission to create a new community, an alternative Ulster, to bring his city back to life. 

== Cast ==
* Richard Dormer as Terri Hooley
* Jodie Whittaker as Ruth Michael Colgan as Dave Hyndman
* Karl Johnson as George Hooley
* Adrian Dunbar as Gang Leader
* Liam Cunningham as Studio Engineer
* Dylan Moran as Harp Owner
* Mark Ryder as Greg Cowen
* Killian Scott as Ronnie Matthews
* Phillip Taggart as Gordon Blair
* Diarmuid Noyes as Brian Young Andrew Simpson as Colin "Getty" Getgood
* Ryan McParland as Fangs
* Kerr Logan as Fergal Sharkey
* Demetri Goritsas as Paul McNally
* Chris Patrick-Simpson as Wolfgang Zorrer
* James Tolcher as Gang Member
* Paul Caddell as Ned
* Niall Wright as Mickey Bradley
* Una Carroll as Mrs. Sharkey (as Una Caryll)
* Dorian Dixon as Sazafrazz Bods
* Mark Asante as Soldier
* Niketa Ferguson as Beautiful German Girl
* Robert Render	as Roaring Executive
* Mary Lindsay as Marilyn
* Steven Donnelly as Rural Punk Kid
* Emma Ryan as Girl
* Joseph Donnelly as Rural Hall Manager

== Release and reception == Time Out all gave extremely favourable 4/5 reviews, with much praise for Dormers performance as Hooley. Observer film critic Mark Kermode described the film as "an absolute humdinger with real heart and soul" and later described how he was twice moved to tears watching it.    Kermode went on to call it the best film of 2013. 

The film was the winner of both the Galway Film Fleadh Audience Award and The Belfast Film Festival Audience Award and  was nominated for three Irish Film and Television Awards including Best Film, Best Actor for Richard Dormer, and Costume for Maggie Donnelly, winning Best Costume. The film received the award for best script at the 2012 Dinard Festival. The film currently holds a 94% rating on Rotten Tomatoes. The screenplay of Good Vibrations received a BAFTA nomination. 

== Music == Good Vibrations The Outcasts and "Teenage Kicks" by The Undertones, as well as Stiff Little Fingers, another Northern Irish punk band around at the same time but not released by the label. The soundtrack also includes songs by The Shangri-Las, Small Faces, David Bowie, Hank Williams and Suicide (band)|Suicide, among others.

== References ==
 

 
 
 
 