Camp (2003 film)
{{Infobox film
| name           = Camp
| image          = Camp (2003 film) Theatrical Release Poster.jpeg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Todd Graff
| producer       = Danny DeVito Michael Shamberg Stacey Sher Christine Vachon
| writer         = Todd Graff Don Dixon
| music          = Stephen Trask
| cinematography = Kip Bogdahn
| editing        = Myron I. Kerstein Jersey Films
| distributor    = IFC Films
| released       =  
| runtime        = 111 minutes  
| country        = United States
| language       = English Spanish
| budget         = 
| gross          = $2,650,356 
}}
 independent musical film written and directed by Todd Graff, about an upstate New York performing arts summer camp. The film is based on Graffs own experiences at a similar camp called Stagedoor Manor  where many scenes of the movie were filmed.

==Plot==
The film centers on the experiences of several teenagers at Camp Ovation, a summer theatre camp. The campers are a mishmash of youngsters from different backgrounds and dealing with different issues, but all feel most comfortable while performing. School is out, and the campers reunite as they prepare to board the buses to Camp Ovation. Shy, self-conscious Ellen Lucas happily greets her friends, recovering from an embarrassing situation where she desperately tried to land a prom date, and ended up asking her brother. Nerdy Fritzi Wagner attempts to befriend icy diva Jill Simmons, but ends up toting her luggage instead. Returning camper Jenna Malloran laments that her parents forced her to have her jaw wired shut in order to lose weight. Upon arrival to Camp Ovation, the kids settle in. Ellen converses with her friend Michael Flores, a gay teenager who was violently beaten by his classmates after showing up to his junior prom in drag. The campers are struck dumb with the arrival of Vlad Baumann, a handsome new camper who is, as a staff member marvels, "an honest-to-god straight boy."

Camp Ovation puts on a new play or musical every two weeks. Vlad and Ellen bond after rehearsal one day and flirt a bit. However, soon afterwards, Vlad is seduced by the conceited Jill, who later makes cruel cracks at Ellen for her weight and her inexperience with boys. Feeling guilty, Vlad comforts Ellen and the two begin to inch towards a relationship. The camp enlists a guest counselor for the summer: composer Bert Hanley, whose play "The Childrens Crusade" was a one-hit wonder many years ago. Frustrated with his lack of recent success, he is now a grumpy alcoholic and shows up two days late at Camp Ovation. He constantly drinks on the camp grounds and is very irritated by Vlad, who idolizes him and attempts to strike up conversation.

Jill shares a cabin with Fritzi, who is still catering to her every whim. However, after Fritzi is caught washing Jills underwear, Jill expresses her disgust with Fritzi and kicks her out of the cabin. The usually meek Fritzi becomes filled with borderline psychotic rage, and sabotages Jills next performance in "Company (musical)|Company" by putting Woolite in her Snapple. As Jill vomits during her performance of "The Ladies Who Lunch," Fritzi shows up in costume and takes her place mid-song. She reveals herself to be a talented and passionate actress.

Michael is rooming with Vlad, on whom he has developed a crush. Vlad convinces Michael to invite his parents to his next play, although his father is incredibly unsupportive of his homosexuality. Though his mother tells him they will come, Michael notices mid-performance that they did not. Crushed, he flees the stage. He is found later by Vlad, who explains that he too has his share of problems. Though Vlad is a seemingly normal, all-American teenager, he reveals to Michael that he suffers from an acute type of Obsessive Compulsive Disorder|OCD, for which he is medicated.

Hanley is angered after hearing Vlad play one of his songs, and begins to tell the campers that theater will only make them bitter and lonely, much like himself. Vlad follows a drunken Hanley back to his room and berates him for his cruel speech. After Hanley throws up and passes out, Vlad finds a trove of music that Hanley has written over the years but not released. During a rehearsal for the camps benefit performance, Vlad and the campers sing "Century Plant", one of Hanleys songs. Hanleys heart is lifted and his disposition changes. Shortly before the benefit, Michael sleeps with Dee, Ellens roommate, out of frustration about Vlad. When Vlad hears of this, he immediately asks Dee if it was true. Vlad and Dee end up making out on Dees bed, and Ellen walks in on them. She runs off, hurt, and refuses to talk to Vlad.

The night of the benefit concert arrives, and the campers are starstruck as famed composer Stephen Sondheim is in attendance. The dressing room atmosphere is tense, and gets even more awkward when Vlads girlfriend Julie shows up to see him. Meanwhile, in another act of revenge, Fritzi sabotoges Jills makeup, causing her to break out in boils. Jill attacks her, injuring her, and both are unfit to go onstage. To replace her, Bert cuts the wires on Jennas mouth, allowing her to sing a powerful song directed to her parents in the audience, telling them to accept her as she is.

The benefit is a hit, but Vlad, Michael, and Ellen are still arguing. Vlad and Michael meet up by the lake, where Vlad strips down to gain Michaels attention. After being set straight by Michael, Vlad admits that he is an "attention junkie", and attempts to please everyone in order to gain their good favor. He apologizes to Michael for leading him on. Ellen arrives, and Vlad explains that he still cares about her, and that his girlfriend Julie had just broken up with him. After another apology, Ellen forgives him, and the three go swimming.

During the end credits, the entire cast does an elaborate rendition of Todd Rundgrens "Want of a Nail".

==Cast==
* Daniel Letterle as Vlad Baumann
* Joanna Chilcoat as Ellen Lucas
* Robin de Jesús as Michael Flores
* Anna Kendrick as Fritzi Wagner
* Alana Allen as Jill Simmons
* Vince Rimoldi as Spitzer Don Dixon as Bert Hanley
* Tiffany Taylor as Jenna Malloran
* Sasha Allen as Dee
* Eddie Clark and Leslie Frye as Mr. and Mrs. Malloran
* David Perlow as Ben Lucas
* DeQuina Moore as DeQuina
* Steven Cutts as Shaun
* Stephen Sondheim as himself

==Production== Jersey Films, IFC Films, John Wells Productions, Killer Films, and Laughlin Park Pictures. All production took place in New York, New York, United States.   

==Soundtrack==
 
{| class=wikitable width="51%" 
|-
! width=1% |  Track # 
! width=20% | Title
! width=20% | Written By
|-
| 1 How Shall I See You Through My Tears"
|rowspan="1"| Robert Telson and Lee Breuer
|-
| 2
| "The Beat Escape"
| rowspan="1"| Jimme ONeill
|-
| 3
| "Losing My Mind"
| rowspan="1"| Stephen Sondheim
|-
| 4
| "The Size of a Cow"
| rowspan="1"| Malcom Treece, Martin Gilks, Miles Hunt, Robert Jones, Martin Bell and Paul Clifford
|-
| 5 Wild Horses"
| Mick Jagger and Keith Richards
|-
| 6
| "Skyway"
| Paul Westerberg
|-
| 7
| "Follies|Im Still Here"
| Stephen Sondheim
|-
| 8
| "Last Song on Blue Tape" Gary Lightbody
|-
| 9
| "Turkey Lurkey Time" Burt Bacharach and Hal David
|-
| 10
| "Praying Mantis" Don Dixon and Phyllis Glasgow
|-
| 11
| "Imagining You"
| David Evens and Winnie Holzman
|-
| 12
| "With You I Do" Chris Perry Adam Alexander
|-
| 13
| "And I Am Telling You Im Not Going"
| Tom Eyen and Henry Krieger
|-
| 14
| "I Believe in Us"
| Phil Galdston, Jon Lind, and Wendy Waldman
|-
| 15 The Ladies Who Lunch"
| Stephen Sondheim
|-
| 16
| "Greensleeves"
| Stephen Trask
|-
| 17
| "Moving on Up"
| Paul Heard and Mike Pickering
|-
| 18
| "I Sing for You"
| Michael Gore and Lynn Ahrens
|-
| 19
| "Generation Landslide" Michael Bruce, Neal Smith
|-
| 20
| "Century Plant"
| Victoria Williams
|-
| 21
| "On/Off"
| Gary Lightbody, Mark McClelland, and John Quinn
|-
| 22
| "Right On Be Free"
| Chuck Griffin
|-
| 23
| "The Kitchen Sink (Peties Top)"
| Tim Weil
|-
| 24
| "Heres Where I Stand"
| Michael Gore and Lynn Ahrens
|-
| 25
| "Desire"
| Tristan Avakian, Sam Slavick, Sterling Campbell, and John Naslas
|-
| 26
| "Round Are Way"
| Noel Gallagher
|-
| 27
| "The Want of a Nail"
| Todd Rundgren
|}

==Reception==

===Box office===
In the opening weekend, the film made $54,294. It came in ranking at #45, showing at only 3 theaters in the United States and averaging $18,098.    The films widest release took place in the UK where it showed in 116 theaters. It ran for 12 weeks and closed on October 16, 2003. It has grossed $1,629,862 since 2003. The film also hit several top 100 charts for films in numerous categories. It is number 83 in the genre of gay/lesbian independent films,    78 for Yearly PG-13 Movies for 2003,<ref name=There is lots of music and a genuine showstopper when Jenna sings "Heres Where I Stand" with such emotion that even her hardheaded dad gets the message.

Read more: http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2003/08/08/DD126441.DTL&type=movies#ixzz0XIraMtNn"PG-13">   and ranked 198 for the year 2003.   

===Critical response===
Camp has received mixed to positive reviews; on  , which uses an average of critics reviews, the film has a 55/100 rating, indicating "mixed or average reviews". 

Margaret A. McGurk of The Cincinnati Enquirer says "Like the prodigies on screen, Camp powers through its imperfections, with irresistible results."   
James Sullivan of the San Francisco Chronicle said in his review titled "Camp," "There is lots of music and a genuine showstopper when Jenna sings "Heres Where I Stand" with such emotion that even her hardheaded dad gets the message."   

===Award nominations===
Camp received nominations for the following awards:   
* 2004, Nominated for Artiors Award for Best Casting for Feature Film, Independent, Bernard Telsey
* 2004, Nominated for Independent Spirit Award for Best Debut Performance, Anna Kendrick
* 2004, Nominated for Golden Satellite Award for Best Original Score, Stephen Trask, and Best Original Song, Bob Telson and Lee Breuer (For the song “How Shall I See You Through My Tears”)
* 2003, Nominated for Grand Jury Prize for Dramatic at the Sundance Film Festival, Todd Graff

==References== 
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 