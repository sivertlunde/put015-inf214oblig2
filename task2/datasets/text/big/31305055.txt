Esther Waters (film)
{{Infobox film
| name           = Esther Waters
| image          = 
| image_size     = 
| caption        = 
| director       = Ian Dalrymple Peter Proud
| producer       = Ian Dalrymple Peter Proud
| writer         = 
| starring       = Dirk Bogarde
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1948
| runtime        = 
| country        = UK English
| budget         = 
| gross          = 
}} George Moore.

==Cast==
* Kathleen Ryan - Esther Waters
* Dirk Bogarde - William Latch
* Cyril Cusack - Fred
* Ivor Barnard - John Randall
* Fay Compton - Mrs Barfield
* Margaret Diamond - Sarah
* George Hayes - Journeyman
* Morland Graham - Ketley
* Mary Clare - Mrs. Latch
* Pauline Jameson - Hospital Nurse
* Shelagh Fraser - Margaret
* Margaret Withers - Grover
* Julian DAlbie - Squire Barfield
* Nuna Davey - Matron
* Beryl Measor - Mrs. Spires
* Barbara Shaw - Mistress
* Archie Harradine - Singer
* Duncan Lewis - Butcher

==Production==
The movie was Dirk Borgardes first film as a leading man. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 