City of Life and Death
{{Infobox film
| name = City of Life and Death
| image = CityofLifeandDeath.jpg
| alt = 
| caption = Film poster
| film name = {{Film name| traditional = 南京！南京！
| simplified = 南京! 南京!
| pinyin = Nánjīng! Nánjīng!}}
| director = Lu Chuan
| producer = Zhao Haicheng Yang Xinli Han Xiaoli Jiang Tao Liang Yong
| writer = Lu Chuan Liu Ye John Paisley
| music = Liu Tong
| cinematography = Cao Yu He Lei
| editing = Teng Yu
| studio = Media Asia Entertainment Group China Film Group Stellar Megamedia Group Jiangsu Broadcasting System Chuan Production Film Studio Media Asia Distribution Ltd. China Film Group
| released =  
| runtime = 133 minutes
| country = China
| language = Mandarin, English, German, Japanese
| budget = US$12 million
| gross =
}}
City of Life and Death is a 2009 Chinese drama film written and directed by Lu Chuan, marking his third feature film. The film deals with the Battle of Nanjing and its aftermath (commonly referred to as the "Rape of Nanking" or the "Nanking Massacre") during the Second Sino-Japanese War. The film is also known as Nanking! Nanking! or Nanjing! Nanjing!.
 Chinese Film Bureau announced in September that the film would be delayed to an early 2009 release.  The film was eventually released on April 22, 2009 where it became a box office success, earning 150 million yuan (approximately US$20 million) in its first two and a half weeks alone. 

==Plot== Republic of China. What followed is historically known as the Nanking Massacre, a period of several weeks wherein massive numbers of Chinese prisoners-of-war and civilians were killed by the Japanese military.

After some commanders of the National Revolutionary Army flee Nanking, a Chinese soldier Lieutenant Lu Jianxiong and his comrade-in-arms Shunzi attempt to stop a group of deserting troops from leaving the city, but as they exit the gates, they are captured by Japanese forces that have surrounded the city.

As the Japanese comb the city for enemy forces, Superior Private (later Sergeant) Kadokawa Masao and his men are attacked by Lu Jianxiong and a small unit of both regular and non-regular soldiers, who fire at them from buildings. Lu and his companions are eventually forced to surrender as more Japanese troops arrive. The Chinese prisoners-of-war are systematically escorted to various locations to be mass executed. Shunzi and a boy called Xiaodouzi survive the shootings and they flee to the Nanking Safety Zone, run by the German businessman and Nazi Party member John Rabe and other Westerners. Thousands of Chinese women, children, elderly men and wounded soldiers take refuge in this safety zone. However, the zone is forcefully entered several times by bands of Japanese soldiers intent on making sexual advances on female refugees. Due to the repeated intrusions, the women are urged to cut their hair and dress like men to protect themselves from being raped. A prostitute called Xiaojiang refuses to do so, saying that she needs to keep her hair in order to earn a living.

Meanwhile, Kadokawa develops feelings for a Japanese prostitute named Yuriko, and struggles to come to terms with the omnipresent violence around him with his own conflicting impulses. Despite his feelings of alienation, Kadokawa brings Yuriko candy and gifts from Japan, and promises to marry her after the war.

Rabes secretary Tang Tianxiang and a teacher named Jiang Shuyun manage the daily operations of the safety zone. Even though he is in a privileged position, Tang is still unable to protect his young daughter from being thrown out of a window by a Japanese soldier, and his sister-in-law from being raped. When the Japanese officer Second Lieutenant Ida Osamu demands that the refugees provide 100 women to serve as "comfort women", Rabe and Jiang tearfully make the announcement to the community. Xiaojiang and others volunteer themselves, hoping that their sacrifice would save the refugees.

Kadokawa meets Xiaojiang and brings her rice but witnesses another soldier raping her as she lies almost lifeless. Later, many "comfort women", including Xiaojiang, die from the abuse, and Kadokawa sees their naked bodies being taken away. Kadokawa feels further estranged when he witnesses Ida shooting Tangs sister-in-law May, who has become insane.

Rabe receives an order to return to Germany because his activities in the safety zone are detrimental to diplomatic ties between his country and Japan. Tang and his wife are allowed to leave Nanking with Rabe. However Tang changes his mind at the last moment and trades places with a Chinese soldier pretending to be Rabes assistant, saying that he wants to stay behind to find May. Mrs Tang reveals that she is pregnant before bidding her husband farewell. Not long after Rabe left, Ida has Tang executed by firing squad.

The Japanese disband the safety zone and start hunting for Chinese men who were previously soldiers, promising that these men would find work and receive pay if they turn themselves in. However, men deemed to be soldiers are huddled into trucks and sent for execution. Shunzi, who survived the earlier mass killings, is physically checked and initially deemed to be a non-combatant, but is later recognised by a Japanese soldier and brought onto a truck as well.

After Minnie Vautrin and other Westerners plead with the Japanese, Ida permits each refugee to choose only one man from the trucks to be saved. Jiang Shuyun rescues a man, pretending to be his wife, and then returns for Shunzi, claiming that he is her husband, while Xiaodouzi acts as their son. Kadokawa sees through Jiangs ruse but does not expose her. Despite this, another Japanese soldier points her out to Ida and the three of them are captured. Shunzi is taken away again, this time together with Xiaodouzi. Jiang, knowing that she will be raped, asks Kadokawa to kill her. He grants her her wish and shoots her, much to his comrades surprise.

Afterwards, Kadokawa looks for Yuriko and learns that she has died after leaving Nanking. He says that she was his wife and requests that she be given a proper funeral. As the Japanese perform a dance-ritual to celebrate their conquest of Nanking and honour their war dead, Kadokawa reveals his emotional turmoil over what he has done and witnessed.

Kadokawa and another Japanese soldier march Shunzi and Xiaodouzi out of town to be executed, but to their surprise, Kadokawa releases them instead. Kadokawa tells the other soldier, "Life is more difficult than death." The soldier bows to show his respect for Kadokawas decision and walks away. Kadokawa then weeps before shooting himself to escape from his guilt.

In the end credits, it is revealed that Mrs Tang lived into old age, as did Ida Osamu, and that Xiaodouzi is still alive today.

== Cast ==
  Liu Ye as Lu Jianxiong
* Gao Yuanyuan as Jiang Shuyun
* Fan Wei as Tang Tianxiang (Mr. Tang)
* Qin Lan as Madam Zhou (Mrs. Tang)
* Nakaizumi Hideo as Kadokawa Masao
* Jiang Yiyan as Jiang Xiangjun (Xiaojiang)
* Yao Di as Zhou Xiaomei (May) John Paisley as John Rabe
* Kohata Ryu as Ida Osamu
* Zhao Yisui as Shunzi
* Liu Bin as Xiaodouzi
* Miyamoto Yuko as Yuriko
* Beverly Peckous as Minnie Vautrin
* Sam Voutas as Durdin
* Aisling Dunne as Grace the missionary 
* Kajioka Junichi as Mr. Tomita
* Liu Jinling
* Zhao Zhenhua
 

== Production==
Filming began in Tianjin in October 2007,    working under a budget of 80 million yuan (US$12 million), and was produced by the China Film Group, Stella Megamedia Group, Media Asia Entertainment Group and Jiangsu Broadcasting System. 
 Chinese censors, Film Bureau did require some minor edits and cuts, including a scene of a Japanese officer beheading a prisoner, a scene of a woman being tied down prior to being raped, and an interrogation scene of a Chinese soldier and a Japanese commander. 

==Release==
City of Life and Death was released on 535 film prints and 700 digital screens on April 22 grossing an estimated US$10.2 million (70 million yuan) in its first five days. This made it the second biggest opener in 2009, after the second part of   grossed US$1.26 million (8.6 million yuan) in 2004.

Despite its success, City of Life and Death created controversy upon its release in mainland China. In particular, some criticised the films sympathetic portrayal of the Japanese soldier Kadokawa. The film was nearly pulled from theaters, and Lu even received online death threats to both himself and his family.   

The film received a limited U.S. release in May, 2011. 

==Awards== Achievement in Directing (Lu Chuan) and Achievement in Cinematography (Cao Yu). The film also won Best Director (Lu Chuan) and Best Cinematographer (Cao Yu) Awards at the 4th Asian Film Awards in 2010. The film won Best Cinematography (Cao Yu) at the 46th Golden Horse Film Awards and was nominated for Best Visual Effects. At the Oslo Film Festival in 2009, the film also won Best Film Prize.

==Critical reception==
City of Life and Death has received positive reviews. The film received a 91% approval rating from critics based on 47 reviews on aggregator site Rotten Tomatoes, with an average score of 8.4/10.   

Kate Muir of The Times gave the film five out of five stars, describes the film as "harrowing, shocking and searingly emotional", and states "the picture has the grandeur of a classic. It should be witnessed."  Derek Elley of Variety (magazine)|Variety states "at times semi-impressionistic, at others gut-wrenchingly up close and personal, Nanjing massacre chronicle City of Life and Death lives up to hype and expectations."  Maggie Lee of The Hollywood Reporter states the film is "Potently cinematic and full of personal stylistic bravura." 

Betsy Sharkey of the Los Angeles Times describes the film as "Truly a masterpiece in black and white and pain" and the film contains "some of the most affectingly choreographed battle scenes to be found, with Lu Chuan a master at moving from the micro of a face to the macro of a city in ruins."  Karina Longworth of IndieWire describe the film "manages to convey the total horror of the Japanese atrocities from the perspective of both perpetrators and victims, all with exceptional nuance, sensitivity and sadness" and the film "has the feel of a lost post-War foreign classic, a masterwork implicating the viewer in the horrors of bearing witness." 

Michael OSullivan at  The Washington Post gave it three out of four stars, elucidating it as "...a muscular, physical movie, pieced together from arresting imagery and revelatory gestures, large and small." 

== See also ==
 
* 
*Dont Cry, Nanking John Rabe
*Nanking (2007 film)|Nanking

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 