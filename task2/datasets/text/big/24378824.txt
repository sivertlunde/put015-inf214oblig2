The Cosmonaut
{{Infobox film
| name           = The Cosmonaut
| image          = Cine Callao, pre-estreno de la película EL COSMONAUTA, Madrid, 14 de mayo de 2013.JPG
| alt            =  
| caption        = Premiere in Madrid in May 2013
| director       = Nicolás Alcalá
| producer       = Carola Rodríguez Bruno Teixidor
| writer         = 
| starring       = Katrine De Candole Leon Ockenden Max Wrottesley
| music          = Joan Valent
| cinematography = Luis Enrique Carrión
| editing        = Carlos Serrano Azcona Nicolas Alcalá
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Spain
| language       = English
| budget         = €860,000
| gross          = 
}}
The Cosmonaut (El Cosmonauta) is a Spanish science-fiction film, directed by Nicolas Alcala and produced by Carola Rodriguez and Bruno Teixidor. It premiered in May 2013. The first feature-length project of Riot Cinema Collective is notable for its use of crowdfunding and Creative Commons license in its production.

==Plot== Star City race against the clock to beat the Americans into space takes place.

Stas and Andrei will witness first-hand the political plots, the fights for power and the successes and failures of the Soviet Union in some of the greatest achievements of the 20th century. They will meet Yulia, a communications technician, with whom they will strike up a strong friendship, always close to love but never consummated.

===Influences===
The main influences behind The Cosmonauts conception and filmmaking approach are the works of some of the worlds most renowned  , and Marina Tarkovskaya (Tarkovky’s sister).

The fictional Program K, whose hummingbird icon is one of the main images used in the project’s promotion, and which also gives its name to the virtual community of producers of the film, is based on the real Soviet projects that attempted to place a Soviet cosmonaut on the Moon.

==Production==
The idea behind The Cosmonaut emerges from two main sources: a) the conversations between Nicolás Alcalá,and Bruno Teixidor (the main creative behind the project’s visual looks) about Conspiracy (crime)|conspiracies, the Lost Cosmonauts theories and the Soviet space program; and b) the poetry book Poetics for Cosmonauts, written by Henry Pierrot. Throughout the time it was developed, the main plot core underwent a lot of changes, although some key elements remained through all the process, mainly the principal plot element (the return of an astronaut to Earth after a long trip to the Moon). After the reading of Poetics for Cosmonauts, the script changed to its present form. The preproduction for the short film started in October–November 2008.

===2009===
In January 2009, the project underwent its most radical changes: the film went from being a short film released and produced in a traditional way, to a feature-length film funded through Crowdfunding and released for free under a Creative Commons Attribution-ShareAlike License. 

====Crowdfunding====
Inspired by productions such as A Swarm of Angels and Artemis Eternal, The Cosmonaut is the first Spanish feature film that makes use of the crowdfunding financing method. There were two ways to get involved in The Cosmonaut’s production:  
* first, as a regular producer. From an initial contribution of 2 euros, a contributor may be listed as a “producer” in the film credits, receiving a welcome pack, and a ticket for the drawing of one of the cosmonauts’ suits that will be used in the film. Further investments could be used to buy merchandising items in the film’s online store. 
* And secondly, as a movie investor. From an initial investment of 100 euros, a contributor can own a percentage of the films profits. 
Being a cinematographic project whose spread is based mostly in the use of social networks, and synergy between audience and creators, the network material of the movie is centered mostly in the creation of a fan community. Producers, regardless of their monetary participation in the film, form part of Programme K, a social group in which they can relate between each other, follow the development of the film, and take advantage of special perks and draws for Programme K members. 

====Release====
The film, when finished, will be released on the Internet  available for download and in High-definition video|HD. Also, besides the full movie on HD, all raw footage shot during the movies filming will be uploaded too. As the film is licensed under a Creative Commons Attribution-ShareAlike License, the users will be able to download, lend, re-cut or use the film footage in any way they wish. This way, the audience not only will be allowed, but also encouraged by the producers themselves, to create new versions of the film and other derivative works. 

===2010===
Between December 2009 and June 2010, the Riot Cinema team worked in modernizing the previous business model. On 2 June 2010, the new model, called “The Plan”, was launched in a press conference by the people in charge of the company. It provides the project with some developments. These developments increase the project approach and infrastructure, keeping its previous features, but, in general, taking them a step further. 

====Crowdfunding and financing==== private financing, crowdfunding, Sponsor (commercial)|sponsorization, and distribution presales. Even though the new three-staged model (in which the first stage would be the first year of the project) keeps crowdfunding and private investment as funding means, their role in the project varies: economically, crowdfunding carries less weight than in the first draft of the project. Nevertheless, its use is kept due to the weight such a completely developed infrastructure has in keeping and strengthening a fan community. Regarding private investment, it keeps the same operational system, but the owned profits per invested money ratio vary depending on the project stage. According to Riot Cinema data, of the €860,000 the project will cost, it is expected that 6.5% be financed through crowdfunding and/or merchandising, 21% trough private investment, 32% through sponsorship, and 40.5% through presales distribution (approximate figures). 

====Yuris Night====
One of the examples of the application of the new sponsorship option was available in the party carried out during the 2010 Yuris Night. Since Yuri Gagarin is a kind of mascot of the project, the people in charge of the film worked in collaboration with LEEM for months in order to join the world initiatives celebrated in Yuris Night. It also happened to celebrate the achievement of 2000 producers of the film. 

====Distribution and transmedia====
In the same way, the movie distribution model has also changed. The project has suffered more noticeable changes in its distribution channels and in the final completion of the project. Regarding distribution, the new plan’s objective is the simultaneous premiere in all release windows. The philosophy behind this decision is to allow the user to choose the format he wants to watch the movie. Different particularities have been adopted for each release window, taking each mean into account, but making some changes in the traditional way of distribution. For instance, the release on the Internet will still be totally free and in HD, while the TV release will have an alternative ending for that channel. In the same way, the people in charge of the movie have designed a special presentation in cinemas to encourage people to go the theaters. This is called “The Cosmonaut Experience”, and it merges performance elements with recreational interaction with the audience, classic screening and a traveling tour, in order to provide the audience, according to the creators, with an added "experience". This tour of screenings will not replace the traditional screening in cinemas, but it will be done at the same time. 
 franchises such The Matrix or Lost (TV series)|Lost, The Cosmonaut will be a transmedia project with ramifications in different fields. It is planned to make a series of webisodes, mobile content, or even an Alternate reality game|ARG, among others. Each series of content will show its own perspective of a story included in the film. It won’t be necessary to watch them in order to understand the given facts, but it will provide some data and information about The Cosmonaut world that will complement each other. 

===2011===
An update of the business model ("The Plan 2") was presented in October 2011, once shooting was finished. 

The Cosmonaut is one of the most successful crowdfunding projects in history with about €300,000. 

====Save The Cosmonaut====
After having raised €120,000 during the first two years, the decision was taken to shoot with that money together with €120,000 more contributed by a Russian co-producer. One week before shooting, when all the tickets were purchased and all the reservations made, the co-producer had to withdraw. A desperate campaign was then launched requesting the community to help save the movie. 

More than 600 people contributed a total of €131,000 in 3 days, beating all the world crowdfunding records in such a short period of time , and allowing the film to be shot. With such a huge success, the possibility of investing was kept during the entirety of shooting. A streaming window was created so the investors could watch the shooting they had made possible live. 

Lánzanos, the Spanish crowdfunding platform that collaborated during the whole campaign, faced, together with The Cosmonaut’s team, what they called "their most beautiful failure of design": the raising status bar exceeded 173%, going beyond the box it was embedded in. The same happened on the film website.

==Media coverage and reception==
After over a year of promotion, the project has received certain degree of attention from the national media in Spain. There have been reports about the film in various of the non-" , in the news program of   and web based media dedicated articles to the film, between them some of the most influential blogs of Spain. Some examples of this include the digital version of El País,  the daily newspaper with the biggest print run in the country, or Microsiervos,  one of the most visited blogs in Spanish language|Spanish. The media reaction wasnt limited to Spain: some English blogs wrote about the project as well. 

===CosmoNauts concert=== indie scene of Madrid played in front of an audience that, according to the organization, reached 400 people between the two days, with the emerging filmmaker Nacho Vigalondo as the master of ceremonies. The concert, held in Madrid in the Sala Heineken,  wasnt able to fill the seating capacity of the concert hall, mainly due to schedule hardships (the concert was held in the middle of a working week, and at the end of the month), and the organizations inexperience. While it was a mild economic failure, the organizers of the concert ascribe the subsequent promotional success of the movie and the later peak of sales in the online store to the festival, and, therefore, consider it a success. 

===Travelling at zero gravity===
The Cosmonaut team, in collaboration with Improba Spain (the collective of followers of Improv Everywhere in Spain), carried out a promotional flashmob on 29 January 2010, in Puerta del Sol, Madrid. The object of the event was to simulate a moon landing in downtown Madrid, and it had a remarkable attendance, even some of the passers-by joined the performance.

== Critics ==
The crowdfunding movie project "The Cosmonaut" has suffered a serious PR drawback    due to a public relationship problem related to the director and writer, Nicolas Alcala. In his response (which he made public) to a resume from a man named "Carlos", Alcala made some very peculiar remarks about the structure of the email and its purpose. Thus, with the intention of humiliating this man, he made comments, such as: "If you are a fishmonger, tell us, we have a job for you." (it can be read in Spanish here ). Spanish social media responded very harshly to the public comment, with multiple reactions across the web,      and thousands of angry responses on their blogs, leading to a public declaration from the director   where he made a public apology for his comments.

==See also==
* Crowdsourcing

==References==
 

==Further reading==
*"The Creative Crowds Greatest Hits". Wired (magazine)|Wired. April 2010

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 
 