The Memorial Gate for Virtuous Women
 
{{Infobox film
| name           = The Memorial Gate for Virtuous Women
| image          = The Memorial Gate for Virtuous Women.jpg
| caption        = Theatrical poster for The Memorial Gate for Virtuous Women (1962)
| director       = Shin Sang-ok 
| producer       = Shin Sang-ok
| writer         = Hwang Sun-won
| starring       = Choi Eun-hee Shin Young-kyun
| music          = Jeong Yun-ju
| cinematography = Jeong Hae-jun
| editing        = Kim Yeong-hie
| distributor    = Shin Films
| released       =  
| runtime        =
| country        = South Korea
| language       = Korean
| budget         =
| gross          =
| film name      = {{Film name
 | hangul         =  
 | hanja          =  
 | rr             = Yeolnyomun
 | mr             = Yŏllyŏmun
 | context        =}}
}} 1962 South Korean film directed by Shin Sang-ok. It was chosen as Best Film at the Grand Bell Awards.      It was also entered into the 13th Berlin International Film Festival.   

==Synopsis==
A melodrama based on a novel. A widow in an aristocratic family has an affair with a servant and bears him a son. The widows in-laws drive the servant and his son away. As a man, the widows son comes to visit her, but, bound by the custom that she must remain celibate after her husbands death, she cannot acknowledge him. 

==Cast==
* Choi Eun-hee 
* Shin Young-kyun
* Kim Dong-won
* Han Eun-jin

==References==
 

==Bibliography==
*  
*  
*  
*  

  
 
  
 
 

 
 

 
 
 
 
 
 

 