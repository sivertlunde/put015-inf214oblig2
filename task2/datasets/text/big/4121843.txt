Grbavica (film)
 
{{Infobox film
| name        = Grbavica
| image       = Grbavica film.jpg
| caption     = Grbavica film poster
| writer      = Jasmila Žbanić Barbara Albert
| starring    = Mirjana Karanović Luna Mijović
| director    = Jasmila Žbanić
| producer    = Tanja Aćimović
| distributor = Dogwoof Pictures
| released    =  
| runtime     = 107 min. Bosnian
| website=  
}} Bosniak women by Serbian troops during the war.  It was released in the United Kingdom as Esmas Secret: Grbavica, and in USA as Grbavica: Land of My Dreams.

The film shows, through the eyes of the main character Esma, her teenage daughter Sara, and others, how everyday life is still being shaped by the Yugoslav wars of the 1990s. The film was an international co-production between companies from Bosnia and Herzegovina|Bosnia, Austria, Croatia and Germany; it received funding from the German television companies ZDF and Arte. Grbavica received an enthusiastic response from critics, earning a 98% rating on Rotten Tomatoes, a website that aggregates professional critiques.
 Bosnia & Herzegovinas official entry for the Best Foreign Language Film at the 79th Academy Awards.

==Background==
The title refers to a neighbourhood of Sarajevo Esma lives in which was one of the most traumatized neighborhoods in the city.   

According to the director,
:"...in 1992 everything changed and I realized that I was living in a war in which sex was used as part of a war strategy to humiliate women and thereby cause the destruction of an ethnic group. 20,000 women were systematically raped in Bosnia during the war."
 derogatory term which some of the population of Sarajevo (mostly Bosnian Muslims and Croats) used for the besieging Serb troops. The director claims that she avoided the word Serb on purpose in order to avoid imposing collective guilt on an entire ethnic group. 

==Plot==
Single mother Esma lives with her 12-year-old daughter Sara in post-war Sarajevo. Sara wants to go on a school field trip and her mother starts working as a waitress at a nightclub to earn the money for the trip. 
Sara befriends Samir, who, like Sara, has no father. Both of their fathers allegedly died as war heroes. Samir is surprised to find out that Sara does not know the circumstances of her father’s death. Samir’s own father was massacred by Chetniks near Žuč when he refused to leave the trench he was defending. Whenever Sara and her mother discuss this delicate topic, however, Esma’s responses are always vague. 
The situation grows more complicated when students who can provide a certificate proving that they are the offspring of war heroes can go on the field trip free. Esma explains to Sara that her father’s corpse was never found and that she has no certificate. She promises to try to obtain the document. Secretly, however, Esma attempts to borrow the money Sara needs from her friend Sabina, her aunt and her boss. 
Sara is haunted by a nagging feeling that something is not right. Shocked and bewildered when she discovers she is not mentioned as the child of a war hero on the list of pupils going on the school trip, Sara lashes out at a classmate, explaining that her father was massacred on the front near Žuč when he refused to desert his trench. At home, however, she confronts her mother and demands to know the truth. Esma breaks down and finally admits painfully that she was raped at a prisoner camp and forced to have the child, Sara, that resulted from this violation. All at once, Sara realizes she is the child of a Chetnik. And yet, this discovery also brings her closer to her mother and helps overcome her trauma. In the end, Sara leaves for the field trip, waving to her mother until the last moment. On the bus, the students sing a popular song about Sarajevo ("Land of My Dreams"), and Sara joins in.

==Cast==
* Mirjana Karanović as Esma Halilović
* Luna Mijović as Sara 
* Leon Lučev as Pelda 
* Kenan Ćatić as Samir 
* Jasna Ornela Berry as Sabina   Dejan Ačimović as Čenga 
* Bogdan Diklić as Šaran 
* Emir Hadžihafizbegović as Puška 
* Ermin Bravo as Professor Muha 
* Semka Sokolović-Bertok as Peldas Mother 
* Maike Höhne as Jabolka 
* Jasna Žalica as Plema 
* Nada Džurevska as Aunt Safija 
* Emina Muftić as Vasvija 
* Dunja Pašić as Mila

==Distribution==
In the UK, the film opened in selected cinemas in London and Sheffield on 15 December 2006.

While it has been screened in Sarajevo, it is unlikely that the film will be shown in cinemas in the Republika Srpska entity of Bosnia and Herzegovina, as the major film distributor "doubts its commercial viability".  It has, however, been shown in Serbia itself.

==Reception==
As of September 2013 the film has the rating of 7,2/10 based on 3.959 watchers.   

==Awards==
;Wins
*Golden Bear - Best Film - 56th Berlin International Film Festival 2006 
*Peace Film Award - Berlin Film Festival 2006 
*Prize of the Ecumenical Jury - Berlin Film Festival 2006
*Kosomorama Award - Best Film
*Reykjavik Film Festival - Best Film
*AFI Film Festival - Narrative grand jury prize
*Brussels European Film Festival - Prize TV Canvas for Best Film and Award Best Actress (to Mirjana Karanović)
*Ourence Film Festival - Award Best Actress (to Mirjana Karanović)
*Portland International Film Festival - Audience Award
*Thessaloniki Film Festival - Woman & Equality Award
*Bosnian-Herzegovinian Film Festival in New York - Audience Award

;Nominations
*Sundance Film Festival - Grand Jury Prize
*European Film Award - Best Film and Best Actress

==See also==
* List of submissions to the 79th Academy Awards for Best Foreign Language Film

==References==
 

== External links ==
* 
* 
* 
* 
* 
* , at the YouTube.

 

 
 
 
 
 
 
 
 
 
 