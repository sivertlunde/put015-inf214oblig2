White Comanche
{{Infobox film name          = White Comanche image         = Whcompos.jpg caption       = Original film poster director      = José Briz Méndez Gilbert Kay writer  Frank Gruber José Briz Méndez Manuel Gomez Rivera Robert I. Holt starring      = William Shatner Joseph Cotten producer  Sam White Philip N. Krasne| Vincente Gomez music         = Jean Ledrut cinematography = Francisco Fraile released      = 1968 runtime       = 93 min. distributor   = language      = country       = Spain budget        =
}}
 1968 paella western starring William Shatner in two roles.
 John Wilsons book The Official Razzie Movie Guide as one of the The 100 Most Enjoyably Bad Movies Ever Made. 

==Plot==
Drifter Johnny Moon (William Shatner) is frequently attacked as he is mistaken for his twin brother Notah who leads Comanche war parties in attacks on the white population whilst he is having visions on peyote.   Johnny travels to a Comanche encampment where he challenges his brother to a fight to the death in the town of Rio Honcho.

When Johnny rides into Rio Honcho he finds the town is at boiling point between two warring factions with only Sheriff Lomax (Joseph Cotten) keeping the peace.  One of the factions discovers Johnny’s prowess with his six gun and tries to hire him.  Johnny says he will give his answer in four days, after the climax with his brother.

==Cast==
*William Shatner as Johnny Moon / Notah
*Joseph Cotten as Sheriff Logan
*Rosanna Yanni as Kelly
*Perla Cristal as White Fawn
*Mariano Vidal Molina as General Garcia
*Luis Prendes as Grimes
*Barta Barri as Mayor Bolker
*Vicente Roca as Ellis
*Luis Rivera as Kah To
*Victor Israel as Carter

==Production== NBC network to buy the film to show on television. 

==Notes==
 

== External links ==
*  

 
 
 
 
 

 