Something Big
 
{{Infobox film
| name = Something Big
| image = Poster of the movie Something Big.jpg
| caption =
| director = Andrew V. McLaglen
| producer = James Lee Barrett Andrew V. McLaglen
| writer = James Lee Barrett Ben Johnson
| music = Burt Bacharach (title song) Marvin Hamlisch
| cinematography = Harry Stradling Jr.
| editing =
| distributor = National General Pictures
| released = January 1, 1971 (U.S. release)
| runtime = 108 min
| language = English
| budget =
}} American motion Western comedy, the film stars Dean Martin, Honor Blackman and Brian Keith.

==Production== Don Knight), Dovers brother, is a partner in Bakers banditry.

To achieve his "something big," Baker must deal with the vulgar but lonely bandit Johnny Cobb (Albert Salmi) and his ruthless sidekick Angel Moon (Robert Donner). 
 Army command in the territory. In a situation parallel to Bakers, Morgans wife Mary Ann (Honor Blackman) is arriving from the East to accompany him home on his retirement. Former football star Merlin Olsen appears in his second on-screen role as Sergeant Fitzsimmons.

Produced at the end of the 1960s and at the height of the Vietnam War, the movie is a revisionist western, with a subtext involving the "civilizing" influence of women on the American frontier. The lighthearted title song, which sets the picaresque tone for the movie, was written by Burt Bacharach (music) and Hal David (lyric) and sung by Mark Lindsay.
 Native Americans is rather negative, depicting them to be drunk and cowardly in a critical battle scene. This itself is a revision of the noble savage stereotype of earlier Hollywood westerns.

McLaglan started his career in western movies as an assistant director to John Ford.

The horses in this film were used by a farmer in Durango, Mexico, named Francisco Berumen.

==Plot summary==
 Ben Johnson) that the notorious bandit Joe Baker (Dean Martin) is planning "something big" but is thwarted at learning details. 

It is revealed that Baker is planning to attack and rob a bandit hoarde just across the border in Mexico. The treasure being well guarded, Baker makes a deal with the scurrilous Johnny Cobb to purchase a Gatling gun in exchange for a woman. Baker receives a letter from his fiancé Dover informing him of her imminent arrival, which sets a deadline on the achievement of his "something big."

Bakers gang holds up a series of stagecoaches, but in each one he is unable to find a woman suitable for Cobb and lets the passengers go unmolested. He is finally able to find a worthy candidate, who turns out to be Colonel Morgans wife Mary Ann. She quickly learns to like Baker because he treats her with respect.

The abduction of his wife enrages Morgan, who sets off with a patrol to rescue her and capture Baker.

Cobb and sidekick Angel Moon meet the trader Malachi Morton (David Huddleston) in the desert to buy the gun, which has been quietly stolen from a federal arsenal. When the trader demands more, Moon hurls his knife into Mortons chest, instantly killing him. They take the gun. Before they can meet Baker, however, they are accosted by Morgan and his scout Bookbinder, who agree to let the bandits go if they reveal Bakers location.

Bakers fiancé Dover arrives at the fort and installs herself in Morgans quarters. Hearing of her arrival, Baker agrees to meet her in the desert. She gives him an ultimatum to go home with her immediately or she will marry someone else.

The night before the supposed rendezvous with Cobb to purchase the gun, Baker realizes he is in love with Mary Ann. He attempts to kiss her, but she rebuffs him, stating she is love with her husband. Nevertheless, Baker tells Tommy that he intends to take the gun from Cobb without giving Mary Ann to him.

Morgan and his scout, with Cobb, Moon and the gun in tow, arrive at Bakers hideout. Angel Moon is killed when he attempts to kill Baker. Morgan proceeds to beat up Baker for stealing his wife. He refuses to give Baker the gun, but his wife reminds him that he is now officially retired and no longer has the authority to seize the gun as federal property.

Cobb realizes he is not going to get his woman and breaks down. Tommy realizes that there is a solution, namely that a pair of lonely women (ostensibly prostitutes, played by Joyce Van Patten and Judi Meredith) would certainly welcome Cobbs presence. Cobb proceeds to their house, and although it is across the border in Texas, where he is a wanted outlaw, the women roughly throw him off his horse and gleefully drag him across.
 Apache allies they have previously paid with whiskey. They are informed that the notorious bandit, named Emilio Estevez, is now a monk, who greets Baker in a garden. Baker suspects a ruse and pulls open the monks robe, revealing a pistol. A gun battle erupts in which Estevez is shot dead. The Apaches, who arrive drunk, and sustain casualties, quickly flee.

Baker is able to mount the wagon with the Gatlin gun and goes on a killing rampage, mowing down men by the dozen from the rooftops of the town until the remainder flee. Baker finds the bandits treasure in the town church, but as his men celebrate their riches, he is haunted by his fiancés words and the sight of the crucifix on the wall.

Back at the fort, Morgan receives an emotional farewell from his assembled troops. Baker, Dover, Morgan and Mary Ann board the stagecoach to return to the East. As they ride out, Bakers men ride alongside and salute him, adorned like kings. Baker climbs out on top of the stagecoach and celebrates with them.

==Credits==
===Production===

*Andrew V. McLaglen  - producer and director
*James Lee Barrett - producer and screenplay 

===Cast===

*Dean Martin as Joe Baker
*Brian Keith as Colonel Morgan
*Carol White as Dover McBride
*Honor Blackman as Mary Anna Morgan Ben Johnson as Jesse Bookbinder
*Albert Salmi as Jonny Cobb Don Knight as Tommy McBride
*Joyce Van Patten as Polly Standall
*Judi Meredith as Carrie Standall
*Denver Pyle as Junior Frisbee
*Merlin Olsen as Sergeant Fitzsimmons
*Robert Donner as Angel Moon
*Harry Carey, Jr. as Joe Pickins
*Francisco Berumen as Farmer

==Reception==
Vincent Canby of The New York Times found it perversely humorous: "Like being stuck on a subway, or bearing witness to a mugging on the other side of the street, watching Something Big is a group experience of a contemporary, if secondary, order. ... Mr. Martin grins his way through it, wearing an extremely handsome, well-cut suede coat. He is the centerpiece of a fiction that occasionally recalls the sentimentality of John Ford, with mock seriousness, as well as the inane cheeriness of those TV Westerns whose heroes never die, but just go into reruns." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 