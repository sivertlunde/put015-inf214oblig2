Teenage Wolfpack
{{Infobox film
| name           = Teenage Wolfpack
| image_size     =
| image	=	Teenage Wolfpack FilmPoster.jpeg
| caption        =
| director       = Georg Tressler
| producer       = Wenzel Lüdecke (producer) Artur Brauner (associate producer)
| writer         = Will Tremper (screenplay) Will Tremper (story) Georg Tressler (screenplay)
| narrator       =
| starring       = See below
| music          = Martin Böttcher
| cinematography = Heinz Pehlke
| editing        = Wolfgang Flaum
| distributor    = Distributors Corporation of America (US)
| released       = 1956
| runtime        = 97 minutes 89 minutes (USA)
| country        = West Germany
| language       = German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Teenage Wolfpack (German: Die Halbstarken) is a 1956 German film directed by Georg Tressler. The film is also known as Wolfpack in the United Kingdom.

== Plot summary ==
 

== Cast ==
*Horst Buchholz as Freddy Borchert
*Karin Baal as Sissy Bohl
*Christian Doermer as Jan Borchert
*Jo Herbst as Günther
*Viktoria von Ballasko as Mutter Borchert
*Stanislav Ledinek as Antonio Garezzo
*Mario Ahrens as Mario
*Manfred Hoffmann as Klaus
*Hans-Joachim Ketzlin as Willi
*Kalle Gaffkus as Kudde
*Wolfgang Heyer as Woelfi
*Paul Wagner as Vater Borchert
*Eduard Wandrey as Pepe Garezzo
*Friedrich Joloff as Theo
*Ruth Mueller as Rita
*Egon Vogel as Prillinger
*Gudrun Kruger
*Ingrid Kirsch
*Oskar Lindner
*Marion Lebens
*Editha Horn
*Heinz Palm
*Paul Bladschun

== Soundtrack ==
* Mister Martins Band - "In Chicago"
* Mister Martins Band - "Sissy Blues"
* Mister Martins Band - "Mister Martins Hop"
* Mister Martins Band - "Swing Party"

== External links ==
* 
*  (English dialogue)
*  (German dialogue)

 
 
 
 
 
 
 
 


 