Doc Savage: The Man of Bronze
{{Infobox film
| name = Doc Savage: The Man of Bronze
| image =Doc_savage_the_man_of_bronze_dvd_cover.jpg
| caption = DVD Cover Michael Anderson
| producer = George Pal
| writer = Joe Morhaim George Pal Lester Dent   (novel) 
| narrator = Paul Frees
| starring = Ron Ely Don Black (lyrics) Frank De Vol
| cinematography = Fred J. Koenekamp Thomas J. McCarthy
| distributor = Warner Bros.
| released =  
| runtime = 112&nbsp;minutes
| country = United States
| language = English
| budget =
}} pulp hero Michael Anderson, Around the World in 80 Days.

== Plot == Fortess of Native American Mayan god The Fabulous Five", his brain trust, at his side.  
 Mayan tribe that disappeared 500 years ago.  However, Don Rubio Gorro (Bob Corso) of the local government informs Doc that all records to the land transaction are missing. Doc receives unexpected help from Gorros assistant, Mona Flores (Pamela Hensley), who saw the original papers and offers to lead Doc and his friends to the land claim.  
 plague that fighting styles and forces Seas to release his friends, whom Doc then treats with a special antidote.  Seeing their leader captured, the Captains men try to escape with the gold, but exploding dynamite causes the pool of gold to erupt, covering the henchmen, including Don Rubio Gorro, in molten metal.  Freed from Captain Seas, Chief Chaac (Victor Millan) offers the gold and land grant to Doc, who replies, "I promise to continue my fathers work ... his ideals. With this limitless wealth at my disposal, I shall be able to devote my life to the cause of justice."

Doc Savage returns to the United States and performs acupuncture brain surgery on Captain Seas to cure him of his criminal behavior.  Later, during Christmas season, Doc Savage encounters the former supervillain, who is now a bandleader for the Salvation Army, flanked by his former paramours Adriana and Karen.  Arriving back at his penthouse headquarters from shopping, Doc hears an urgent message about a new threat that could cost millions of lives.  Doc Savage leaps into action and speeds to his next adventure.

== Cast ==
  Clark "Doc" Savage Jr. (pictured) Major Thomas J. "Long Tom" Roberts Colonel John "Renny" Renwick Lieutenant Colonel Andrew Blodgett "Monk" Mayfair Professor William Harper "Johnny" Littlejohn Brigadier General Theodore Marley "Ham" Brooks Paul Wexler as Captain Seas
* Pamela Hensley as Mona Flores
* Bob Corso as Don Rubio Gorro
* Federico Roberto as El Presidente Don Carlos Avispa
* Janice Heiden as Adriana
* Robyn Hilton as Karen
* Victor Millan as King Chaac
* Paul Frees as Narrator (uncredited)

Other noteworthy casting included:
* Cult actor Robert Tessier as one of Captain Seas’ henchmen.
* Cult actor Michael Berryman as Juan Lopez Morales, Hidalgos chief Coroner#Artistic depictions|coroner.
* Legendary stuntman Dar Robinson as the would-be Mayan assassination|assassin. Mayan shaman Kulkan, also appeared in episodes "The Ultimate Duel" and "Perils of Tanga" of the 1966 TV series NBC series Tarzan (TV series)|Tarzan starring Ron Ely.
* Grace Stafford, the wife of animation producer Walter Lantz, played an elderly woman who was helped across the street by a Boy Scout near the end of the film. George Pal and Lantz were good friends, and Lantz’s most famous creation, Woody Woodpecker, often made a cameo appearance in Pals George Pal#Trivia|films.  Ms. Stafford provided the voice for Woody Woodpecker.

== Production background ==
 

=== Goodson-Todman aborted 1966 Film ===
As co-creator of Doc Savage, author Lester Dent retained the radio, film, and television rights to the character as part of his contract with Street and Smith Publications, publishers of the Doc Savage pulp magazine. Although Dent succeeded in launching a short-lived radio program, he was never able to interest Hollywood in a Doc Savage film. Upon Dents death in 1959, his widow, Norma Dent, acquired the radio, film, and television rights to Doc Savage.

The production team of Mark Goodson and Bill Todman announced the intention to produce a Doc Savage film to cash in on the popularity of the re-issued pulp novels by Bantam Books and the James Bond craze sweeping the movies.
 The Thousand-Headed Man, with Chuck Connors as Doc, for a 1966 release.
 western Ride Beyond Vengeance.
 movie tie-in Gold Key, with cover artwork by James Bama, remains to mark this aborted film undertaking.

=== George Pals 1975 theatrical film === Voyage to the Bottom of the Sea.  However, to make a franchise work, the first movie had to be successful.  Pal originally contacted Steve Reeves for the role of Doc but when filming was about to begin a Hollywood writers strike put the film on hold with Reeves and the original director replaced. 
 The Man The Green The Mystic Mullah.
 Paul Wexler, Mystery of the Sea. This should not be confused with the Canadian television character Captain Flamingo.
  Michael Anderson Dam Busters, Operation Crossbow. Around the World in Eighty Days, the Best Picture of 1956.

Although there were reports that Pal planned to do location filming in Central America, principal photography was confined to southern California.  Scenes involving the fictitious Eastern Cranmoor Building in New York City were filmed underneath the clock tower of the art deco Eastern Columbia Building in downtown Los Angeles.
 handlebar moustache.

Pamela Hensley made her film debut in this motion picture.

Darrell Zwerling and Federico Roberto appeared in another 1930s nostalgia film, the 1974 film noir retro classic Chinatown (1974 film)|Chinatown, with Zwerling playing Hollis Mulwray, the murdered water commissioner.
 a rare War of the Worlds as well as performing the narration and other voice work for the 1960 fantasy film Atlantis, the Lost Continent, both produced by George Pal.
 Cord Model Lockheed L-12A Amelia Earhart, the 1976 CBS-TV adventure series Spencers Pilots and the 1877 TV movie The Amazing Howard Hughes in addition to Doc Savage: The Man of Bronze. It is currently registered with the tail number N12EJ to the Runyan family of Vancouver, WA.

=== Thematic issues & faithfulness to the source material ===
 
Doc Savage: The Man of Bronze was visually faithful to the novels and characters, which included such elements as:
* Docs trilling (although it sounded like bells tinkling rather than a more lifelike trilling (such as a cat purring)
* Bickering between Monk and Ham
* Rennys signature expletive "Holy Cow!"
* Rennys love of slamming his fists through solidly constructed doors or door-panels.
* Monks pet pig, Habeas Corpus Fortress of Solitude headquarters on the 86th floor of the Empire State Building.
* The Crime College and Docs brain surgery techniques to remove the criminal element from crooks hed captured, making them incapable of committing further crimes.
* Docs dramatic descent down a skyscraper elevator shaft
* Docs daily two-hour exercise regime
* Doc standing on an automobiles running board in hot pursuit (see photo, right)
* Johnnys use of long, obscure words when simple words would suffice.
* Johnnys signature expletive "Ill be superamalgamated!"
* A plethora of retro gadgetry such as heat detector, globes of fire-fighting chemicals (extinguisher globes), phonograph-based recording machine, remote-controlled aircraft, a ray gun disguised as a cigarette lighter, lightweight bullet-proof vests, miniaturized Scuba set|SCUBA-type underwater breathing gear, and the Whizzer, a prototype helicopter—all his inventions and designs.
 Empire State tutelage of Escape From The Fabulous Five in a high-security German POW camp in 1918.  Finally, the opening titles used the same typography (pictured) used in the Bantam Books reprints of the Doc Savage pulp novels.

Debate continues who was responsible for the   or George Pal and his production team.  Among the many examples of over-the-top camp include Don Rubio Gorro (Bob Corso) rocking himself to sleep in an adult-sized baby crib, with Beautiful Dreamer as its musical cue, the animated twinkle in the eye of Doc Savage (Ron Ely) at the beginning of the film and later when Doc tells Mona (Pamela Hensley) that she was a brick.  "La Cucaracha", played by a flute was used in an up-tempo musical cue, during the attempted escape of Captain Seas henchmen from the Valley of the Vanished, and finally, an applause soundtrack was added following Docs recitation of his personal code.

The film is also remembered for its theme song arranged by   and composer Marvin Hamlisch had achieved when they used the ragtime music of Scott Joplin for the 1975 caper film The Sting. Both Sousa and Joplin were turn-of-the-century contemporaries, and their music were not contemporaneous to the period that these 1930s nostalgia films were set.  The credit acknowledging Sousas score has the letters "USA" of his last name highlighted in red, white, and blue.

== Reception and reaction ==
 
  Murder on the Orient Express, and other 1930s nostalgia films released during this period.  Even Pals trademark visual effects and matte paintings were mediocre, with the depiction of the Green Death as animated serpentine vapors being particularly unconvincing (pictured).
 parade in Atlanta, Georgia.

Reviews were scathing, with Daily Variety noting: "Execrable acting, dopey action sequences, and clumsy attempts at camp humor mark George Pals Doc Savage as the kind of kiddie film that gives the G rating a bad name."
 blockbuster box office of Jaws (film)|Jaws.

== Sequel and future productions ==

A sequel,  , was announced at the conclusion of The Man of Bronze.  Based on a screenplay by Joe Morhaim, and according to contemporary news accounts, it had been filmed in the Lake Tahoe area simultaneously with the principal photography for the first Doc Savage. However, people associated with the production of the film have said only some publicity shots were taken at Tahoe. No filming was done.
 Death in The Feathered Octopus.  However, due to the poor reception of the first film, Doc Savage: The Arch Enemy of Evil was scrapped.

=== Other productions ===
Another script was written by fantasy author Philip José Farmer, and included a meeting between Doc and a retired Sherlock Holmes in 1936, but it was never filmed.
 The Mummy, Castle Rock Entertainment announced plans to produce a new Doc Savage movie with Warner Brothers and Universal Studios.  Film-makers Frank Darabont and Chuck Russell would supervise the production, based on a screenplay by Brett Hill and David Johnson, with Arnold Schwarzenegger attached to play Doc. However, with Schwarzenegger serving as governor of California, and both Darabont and Russell involved in other projects, this proposed production came to nothing and has since been shelved.
 Street and pulp heroes, The Avenger, and Doc Savage.  A screenplay was supposedly written by Siavash Farahani but since then, no other news has surfaced with regards to this script.  

In 2010, it was announced that Shane Black is set to direct and write a new film adaptation, set in the pulps native time of the 1930s, for Original Film and Sony Pictures. 

== Awards == Golden Scroll for Best Fantasy Film from the Academy of Science Fiction, Fantasy & Horror Films.

== Home video ==

Doc Savage: The Man of Bronze was initially released by Warner Home Video in a clamshell box, at the time denoting a family film, with cover art designed to capitalize on the success of Raiders of the Lost Ark.  The film was also released onto Laserdisc and there is  a DVD available in Germany (with German and English language).

In March 2009, the movie was   within the United States as part of Warner Brotherss   manufacture-on-demand DVD service, at a price of $19.95 for a DVD containing the movie presented in its original aspect ratio, and its trailer in 4:3.

==References==
 
 Will Murray. The Bronze Gazette (Vol. 1, No.   1) March 12, 1992.

"The Doctor is in!  Doc Savage" by Michael A. Beck. Baby Boomer Collectibles (April 1996)

“The Bronze Age” by Will Murray from James Van Hise, ed., Pulp Heroes of the Thirties, 2nd edition (Yucca Valley, CA: self-published, 1997).

  (New York, NY: Bantam Books, 1975).

== External links & sources ==
*  
*  
;1975 film
*   ThePulp.Nets Doc Savage movie page
*   Hidalgo Trading Company website – Doc Savage: The Man of Bronze page
*   Doc Savage The Man of Bronze – On-line Movie Graphic Novel
*   Moria: Doc Savage Movie Page
*   Weird Sci-Fi – Doc Savage Movie Review
*   Listing of George Pal Papers, 1937–1986
*   Daily Variety review – January 1, 1975
*   Obscure Facts about George Pal
*   DVD Late Show: Twenty MIA Movies I Want on DVD
*   Cinema.de (German)
*   Doc Savage The Supreme Adventurer
*   @  
;Sequels & possible remakes
*   Mania.com Development Hell – Doc Savage
*   Mania.com – Sam Raimi finds more superheroes?
*   Wold Newton Universe
*   A Lester Dent Bibliography by Will Murray
*   Philip Jose Farmers Basement: Original Manuscripts
*   Aint It Cool Cool News – July 7, 1999
*    Doc Savage Bibliography – Movies

 
 
 

 
 
 
 
 
 
 
 
 
 
 