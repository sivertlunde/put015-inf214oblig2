Cactus Flower (film)
{{Infobox Film
|  name           = Cactus Flower
|  image          = Cactus_Flower_1969.jpg
|  writer         = Abe Burrows (play), Pierre Barillet (Fleur de cactus),  Jean-Pierre Grédy (Fleur de cactus), I. A. L. Diamond (screenwriter)
|  starring       = Walter Matthau, Ingrid Bergman, Goldie Hawn
|  music          = Quincy Jones
|  director       = Gene Saks
|  producer       = Mike Frankovich|M. J. Frankovich
|  cinematography = Charles Lang
|  distributor    = Columbia Pictures
|  released       = December 16, 1969
|  runtime        = 103 min.
|  country        = United States
|  language       = English 
|  awards         = 1969 Academy Award for Best Supporting Actress for Goldie Hawn
|  budget         = $3 million
|  gross          = $25,889,208 
}} Oscar for Broadway stage play written by Abe Burrows, which in turn was based upon the French play Fleur de cactus by Pierre Barillet and Jean-Pierre Grédy. The film was the seventh highest grossing film of 1970.  

The film has been remade a few times.  An unauthorized Hindi version titled Maine Pyaar Kyun Kiya?, starring Salman Khan, Sushmita Sen and Katrina Kaif, was released in 2005. An English language remake, Just Go With It, starring Adam Sandler and Jennifer Aniston, was released in 2011. An Egyptian version titled Nos Saa Gawaz (Half-Hour Marriage), starring Rushdy Abaza, Shadia and Adel Imam, was released in 1969.

==Plot==
 mouth to mouth resuscitation, which evolves into a kiss after Toni regains consciousness.

Tonis failed suicide attempt stems from her despondency following a romantic breakup. Her lover, Julian, ended the relationship by announcing he had a wife and three children. Unknown to Toni, Julian is not married. Upon learning of Tonis suicide attempt, Julian decides to marry Toni, but he needs a wife to divorce in order to sustain his earlier lie. Julian asks Stephanie Dickinson, his long time assistant, to pose as his wife. At first unwilling, she ultimately relents, since she has long had a crush on her employer. 

Toni senses Miss Dickinsons feelings for Julian and asks Julian to help Miss Dickinson find another man. Ultimately Julians friend Harvey, Señor Arturo Sánchez, and Igor all become embroiled in Julians scheme. Toni suspects Julians untrustworthiness and leaves him for Igor. Julian finally falls in love with Miss Dickinson.

The prickly cactus Miss Dickinson keeps on her desk in the office gives the film its name. Like Miss Dickinson, the cactus thrives in the driest of settings.

Ultimately, however, both the cactus and Miss Dickinson "bloom."

==Cast==

{| class="wikitable"
|- bgcolor="#CCCCCC" align="center"
| Actor || Role || Other notes
|-
| Walter Matthau || Dr. Julian Winston || a dentist
|-
| Ingrid Bergman || Stephanie Dickinson || Dr. Winstons assistant
|-
| Goldie Hawn || Toni Simmons || Dr. Winstons girlfriend
|-
| Jack Weston || Harvey Greenfield || a friend and patient of Dr. Winston
|-
| Rick Lenz || Igor Sullivan || a writer and Tonis neighbor
|-
| Vito Scotti || Señor Arturo Sánchez || a diplomat and patient of Dr. Winston
|-
| Irene Hervey || Mrs. Durant || a patient of Dr. Winston
|-
| Eve Bruce || Georgia || a date of Harvey Greenfields
|-
| Irwin Charone || Record Store Manager || Tonis employer
|-
| Matthew Saks || nephew || one of Mrs. Dickinsons nephews
|}

==Reception== eighth highest Howard Thompson of The New York Times stated that "both the expansive scenario of I. A. L. Diamond and the flexible direction of Gene Saks open up and even ventilate the story".  Roger Ebert declared that "the chemistry works" and "the movie is better than the play".  

In her first major film role, Goldie Hawn, once described as the "dizzy cream puff who is constantly blowing her lines  ",  was praised for being "a natural reactress; her timing is so canny that even her tears run amusingly".  Hawns performance in Cactus Flower won her the Academy Award for Best Supporting Actress, her sole Oscar to date.

==Awards==

Goldie Hawn won two awards for her supporting role:
* Academy Award for Best Supporting Actress - Goldie Hawn
* Golden Globe Award for Best Supporting Actress - Motion Picture - Goldie Hawn

In addition, there was a nomination for Ingrid Bergman and an additional one for Goldie Hawn:
* BAFTA Award for Best Actress in a Leading Role - Goldie Hawn
* Golden Globe Award for Best Actress - Motion Picture Musical or Comedy - Ingrid Bergman

Screenwriter I. A. L. Diamond was nominated for the 1969 Writers Guild of America Award for Best Comedy Adapted from Another Medium, the only one of his ten screenplay nominations that was not for a screenplay that he co-wrote with Billy Wilder.

==See also==
* List of American films of 1969

==Notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 