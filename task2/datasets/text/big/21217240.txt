The Graduates
 
 
 

{{Infobox Film
| name           = The Graduates
| image =Poster of the movie The Graduates.jpg
| director       = Ryan Gielen
| producer       = Ryan Gielen Matthew Gielen Joshua Davis Caitlin Marshall
| writer         = Screenplay: Ryan Gielen
| starring       = Rob Bradford Blake Merriman Nick Vergara Michael Pinnacchio
| music          = Songs: The New Rags Plushgun Mad Tea Party Sonia Montez The Kudabins
| cinematography = Mufit Umar
| editing        = Ryan Gielen
| released       = July 31, 2008
| runtime        = 94 minutes
| country        = United States
| language       = English
}}
The Graduates is a 2008 American coming-of-age teen comedy film directed and written by Ryan Gielen.  The film is produced by Ryan Gielen, Matthew Gielen, Josh Davis and Caitlin Marshall. The film tells the story of Ben (played by Rob Bradford), Andy (played by Blake Merriman), Mattie (played by Nick Vergara), and Nickie (played by Michael Pennacchio) recent highschool graduates who go to Ocean City, Maryland for Senior Week or Beach Week. 

==Plot==
The film explores the life of 18-year-old Ben, shortly after earning his degree from an unnamed high school in Maryland. Ben is heading to Senior Week to hang out with friends, party, and chase the hottest girl in high school, Annie (played by Stephanie Lynn).

The movie begins outside a liquor store in Columbia, Maryland|Columbia, located between Baltimore and Washington, D.C. in Howard County. Ben is talking to himself when Bens older brothers friend Brian (played by Brian Seibert) overhears him, which embarrasses Ben. Bens older brother Josh (played by Josh Davis) gets in the car and hands Ben a bag of liquor. They then drive to pick up Bens three friends, Andy, Mattie and Nickie.

After picking up Bens three friends, the group heads off to Ocean City. During the trip to Ocean City the group has discussions about sex, which highlights the groups limited understanding of sex and their naivete.

Upon arriving in Ocean City the group gets drunk and goes to a party. After entering the party, Josh tells the boys "the secret" about female body language. Later at the party Ben talks to Megs (played by Laurel Reese), his best friend. As they talk, Annie enters the party. Her entrance completely distracts Ben. Ben then sits down next to Annie and flirts with her. As Ben and Annie begin to kiss, Nickie gets into a fight, which causes Annie to leave and ends the party.

The next day, the group goes out on the town, has fun at the beach, and cruises the boardwalk. They end up back at Bens condo and play Asshole (game)|Asshole. Andy becomes president and declares that Nickie must toast him. After a brief verbal spat, Ben becomes Vice President and calls for a confession. After a descriptive story from Josh about losing his virginity, the boys stumble into an awkward conversation about divorce.

Throughout the rest of the week the viewer meets many more characters that come in and out of the story line.

==Cast==
*Rob Bradford as Ben
*Blake Merriman as Andy
*Nick Vergara as Mattie
*Michael Pennacchio as Nickie
*Stephanie Lynn as Annie
*Josh Davis as Josh
*Max Lodge as Stuart
*Laurel Reese as Megs
*Brian Seibert as Brian
*Rachel Kiri Walker as Rachel
*Zak Williams as Mike "The Resonator" Resniski
*Josh Folan as Jesse Swomley
*Meghan Miller as Mellisa
*Christine Utterberg as Dee
*Logan Tracey as Trish
*Katy Wright-Mead as Chelsea
*Dennis Hill as Steven Wright
*Nadeya Ward as Beth-Anne
*Tyler Ramage as Dan
*Brandon Kenny as Partygoer #4
*Lena Hall as Inga
*Lauren Toub as Bridget
*Tom Cryan as Tom

==Music==
*The Film has three original composed songs by Seth Freeman.
*The New Rags have three songs on the soundtrack.

==Awards and honors==
Directorial Discovery Award: Rhode Island Film Festival http://www.film-festival.org/awards08.php 

==Miscellany==
*This is the first film to be shot entirely on location in Ocean City, Maryland. Additionally, "Violets are Blue" (1986) starring Kevin Kline, was partially shot in Ocean City, Maryland. 

*The film is Zak Williams, son of famous actor Robin Williams, feature film debut. 

==References==
 

==External links==
 
*  
*  
*   at Film School Rejects
*   at Show Me Your Indies
*   in The Washington Post
*   in The Charleston Post and Courier

   
{{succession box
| title = Rhode Island Film Festival Directorial Discovery Award 1 
| years = 2008
}}
 
 
 

 
 
 
 
 
 
 
 
 
 