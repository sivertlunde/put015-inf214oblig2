Waar
 
 
{{Infobox film
| name           = Waar
| image          = Waar (film poster).jpg
| caption        = Theatrical release poster
| director       = Bilal Lashari(خواتین میں چھاتی) 
| producer       = Hassan Waqas Rana 
| writer         = Hassan Waqas Rana
| starring       = Shaan Shahid Shamoon Abbasi Ali Azmat  Ayesha Khan  Meesha Shafi
| music          = Amir Munawar   
| cinematography = Bilal Lashari
| editing        = Bilal Lashari
| studio         = MindWorks Media  Off Road Studios
| distributor    = ARY Films   Mandviwalla Entertainment
| released       =   }} 
| runtime        = 130 minutes   
| country        = Pakistan
| language       = English, Urdu
| budget         =    
| gross          =     }} 2013 Urdu-English Pakistani action war on attack on a Police Academy at Lahore in 2009. 

== Plot== tribal region of Pakistan, led by Ehtesham Khattak (played by Hamza Ali Abbasi) and coordinated by his sister, Javeria Khattak (played by Ayesha Khan), an intelligence officer. Ehtesham and Javeria come to know of a major terrorist attack that can only be countered with the help of Major Mujtaba.

Major Mujtabas family was assassinated by Ramal (played by Shamoon Abbasi), a spy of India spy agency Research and Analysis Wings (RAW) operative. Major Mujtaba wants to take revenge from Ramal. Recognizing Ramals through his actions and tactics, Major Mujtaba is able to counter his attacks. Mulla Siraj, a Taliban working with Ramal, operating from a fort in the tribal area gives Ramal two bombs which Ramal is going to plant somewhere in Pakistan. Planned by Laxmi, a spy of RAW, terrorists conduct an attack on a police training center in order to divert the attention of the security agencies aimed at watching any suspicious activity as they are reported that a big terrorist activity is imminent. Out of the two bombs, one is loaded in a vehicle and Ehtesham drives this vehicle away to dispose the bomb off and is killed in the explosion. Second bomb is planted in Jinnah Convention Centre, Islamabad but Major Mujtaba counters this attack and saves the country from another deadly terror incidence. He takes his revenge by killing Ramal. His powerful words are "Good wins over evil in the end" and this happens.

== Cast ==
* Shaan Shahid  as Major Mujtaba Rizvi, a retired Pakistan Army Officer
* Shamoon Abbasi  as Ramal, Indian spy agency Research and Analysis Wings operative
* Meesha Shafi  as Laxmi, Indian spy agency Research and Analysis Wings operative
* Ali Azmat  as Ejaz Khan, politician
* Hamza Ali Abbasi as Ehtesham Khattak, O/C Field Operations CTG 
* Ayesha Khan  as Javeria Khattak, an intelligence officer. Chief INTEL. and COM. Analyst CTG  and Ehteshams sister
* Hassan Waqas Rana as Taha Ali, director CTG
* Bilal Lashari as Ali (Sniper)
* Kamran Lashari - as Asher Azeem, DG Internal Security or Head of Security Wing
* Nadeem Abbas Rana
* Batin Farooqi - Militant
* Uzma Khan - Mujtabas Wife
* Waseem Badami a news anchor Naseer Afridi (cameo)

== Production ==
  (right) along with Bilal Lashari during the film shooting.]]

The films title "Waar" is an Urdu language word meaning "to strike".    Waar is primarily an English language film with some dialogues in Urdu. According to the producer, Hassan Waqas Rana, it was considered dubbing the movie in Urdu but the idea was dropped as it would have compromised the lead role played by Shaan Shahid. The story has been inspired from real events and highlights the aspect of terrorism in Pakistan. It was written by Hassan Waqas Rana.

=== Casting and crew ===
Initially, it was reported that Tom Delmar who has worked as stunt director in a number of Hollywood movies would direct the film,    later Lashari was chosen as the director who was already working with Rana on another project.It is Bilal Lasharis debut as a director, who has already directed music videos    and assisted Shoaib Mansoor in the film Khuda Kay Liye.  Ali Azmat and Meesha Shafi both of whom are known for singing made their acting debut in the film.    Originally Ali Azmats and Ayesha Khans roles were limited to guest appearance that were later expanded to full roles.    Hamza Ali Abbasi who intended to work as assistant director on the film was cast as an actor.   

=== Filming ===
The film is produced by MindWorks Media and includes four hundred visual effects.  It took three years for the film to complete.    Locations for the filming included Lahore, Islamabad, Karachi, Swat Valley, Istanbul, Turkey and Rome, Italy. It was reported in the media that the film was shot in collaboration with the Inter-Services Public Relations (ISPR), the media wing of Pakistan Army  but the director Bilal Lashari denied any such collaboration saying that the confusion might have arisen as MindWorks Media worked on a documentary film The Glorious Resolve with ISPR at the same time when Waar was being filmed.    The films budget was variously said to be PKR: 170&nbsp;million and PKR: 200&nbsp;million.    

== Release and promotion ==
 The films release date was changed repeatedly and finally it was released on 16 October 2013 coinciding with Eid al-Adha in Pakistan.  It was initially scheduled to release on 6 September 2013.    The first theatrical trailer of the movie was launched in January 2012    while the second in January 2013.    One of the trailers was viewed more than 500,000 times the same month, making it one of the top five videos of YouTube.  The film was dubbed as the most anticipated film in the history of the Pakistani Cinema.    When the film could not make it to the screens in a considerable time after the release of trailers, the critics dubbed it as another project that will get shelved.  Shamoon Abbasi, the main antagonist, cited the lack of resources for filming as one of the reasons for its delay.    The film premiered on 10 October at Karachi    and on 14 October 2013 at Rawalpindi/Islamabad.    The film was given adults-only rating by the Sindhs provincial censor board for use of obscene language and violence.  Waar was released in about forty five theaters across the country.    The film was world TV premiered on 14 August 2014 on ARY Digital.  

Though initially reported to be distributed by Warner Bros..,  the film was distributed by ARY Films and Mandviwalla Entertainment.     The film was released in twenty five countries.  Waar was released in the United Arab Emirates (UAE)    cinemas on 12 December, where the movie actors graced the red carpet at the Grand Cinema, Wafi City.     It was released in cinemas across the UK on 17 January 2014.    The movie released in cinemas throughout Australia on 15 May 2014.   It was expected to release in India as well but so far it has not been possible because of portrayal of India supporting terrorism in Pakistan.

== Reception ==

=== Box office ===
Waar opened on the first day of Eid al-Adha on 42 screens, the widest release ever, across Pakistan with 100% occupancy and it broke records with capacity audiences. It earned   in its first day, thus breaking the previous record of   held by Chennai Express.   It earned a total of   till Friday night, breaking all previous records of Eid collections.  Waar collected   in its extended first week of nine days  and added another  , thus making a total of   in thirteen days.  The film managed to collect   in its 4th week but was still behind Syed Noors 1998 film Choorian (1998 film)|Choorian, which earned   and then on its 36th day of screening, it broke the record held by Choorian (1998 film)|Choorian.     Movie had collected   in 7 Weeks. In its 8th Week movie got advantage of ban of Hindi films in Pakistan and collected   to take its total to  .  In its 9th Week movie saw a huge competition in the form of Dhoom 3 but still added another  .  Movie continued its steady run in next Weeks and ended its run around   becoming biggest grosser in Pakistan at that time. 

===Critical response===
{{Album ratings rev1 = IMDB rev1Score =      rev2 = DAWN News rev2Score =      rev3 = Skotato rev3Score =     
}}
The film received positive reviews from critics and became the highest grossing film in Pakistan of all time. In the IMDb Waar is the 26th highest rated feature film with more than 5,000 votes.  Because since 2012 films of the Top 250 need at least 25,000 votes, Waar is not part of Top 250 yet as it has only about 22,000 votes for now.     

Rafay Mahmood for The Express Tribune gave the movie three out of five stars and commended the cinematography, editing and sound design but viewed critically the story and some performances. According to the review Waar is a "piece of pointless propaganda (and) is going to further confuse an already puzzled nation about Pakistan’s outlook on counter-terrorism. In the long run, it will prove to be a great feature for Pakistani cinema but a damaging one for intellect.   
 Dawn gave Daily Times talks about the message given by the movie that highlights the "Pakistani perspective on the menace of terrorism", while acclaiming the story, direction, action sequences and individual performances.    Rubban Shakeel of Skotato gave Waar a 3.5/5 stars, calling it one of the best action films on Pakistan.    On Skotato, too, Umer Ali called Waar "A Ray of Hope". 

Because of the film story, Waar has been critically reviewed in India. Unlike other Pakistani films, Waar received a wider and ever higher coverage than its contemporary Indian movies in the Indian media.   Although hundreds of Indian movies have provided with a negative portrayal of Pakistan, very few Pakistani movies like Waar has shown India as a force behind terrorism attacks in Pakistan. However, due to the so fame the movie has gained internationally, Waar has been received critically by Indian media and politicians.  However, Indian film director Ram Gopal Varma praised the film, saying he was "stunned beyond belief" and congratulated Bilal Lashari.  

== Music ==
The films music that took almost two years to get completed was composed by Amir Munawar   while Qayaas and Umair Jaswal have contributed a song each. Here is the list of soundtracks:

*Inquilaab (Vocal: Umair Jaswal)
*Saathi Salaam (Vocals: Sawan Khan Manganiyar & Clinton Cerejo)
*Mauje naina (Vocals: Bianca Gomes, Shadab Faridi & Altamash Faridi)
*Halaak
*Khayal

==Accolades==
ARY Film Awards 2014

Won: Technical Award
*Best Action: Hassan Waqas Rana (producer) 
*Best Cinematography: Bilal Lashari

Won: Viewers Choice
*Best Film: Hassan Waqas Rana (producer) 
*Best Director: Bilal Lashari 
*Best Actor: Shaan Shahid 
*Best Actress: Aisha Khan 
*Best Supporting Actor: Hamza Abbasi 
*Best Supporting Actress: Meesha Shafi 
*Best Star Debut Female: Aisha Khan 
*Best Actor in a Negative Role: Shamoon Abbasi

== Sequel ==
On 7 December 2013 ARY Films and MindWorks Media joined for the production of Waar 2, sequel of Waar.    Waar 2 will be shot in Pakistan, UK, Russia, Turkey and the former Yugoslavia. 

==Controversy==

Owner and CEO of Mindworks Media, Hassan Waqas Rana was recently booked under an FIR (First Information Report) with the Pakistani police by director of Waar, Bilal Lashari for keeping Waars profits for himself. Apparently he transferred all cinema earnings to his personal account. The court dismissed Hassans pre-arrest bail and police is currently searching the suspect in Lahore and Islamabad. 

==See also==
* List of highest-grossing Pakistani films
* List of Pakistani films of 2013

== References ==
 

== Further reading ==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

== External links ==
*  
*  
*  
*  
*  
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 