Karmayogi (2012 film)
{{Infobox film
| name           = Karmayogi
| image          = Karmayogi 2011.jpg
| alt            = 
| caption        = Film poster
| director       = V. K. Prakash
| producer       = Vachan Shetty Sajitha Prakash
| writer         = Belram Mattannoor
| based on       =   Indrajith Nithya Menon Padmini Kolhapure Saiju Kurup
| music          = Ousepachan
| cinematography = R. D. Rajasekhar
| editing        = Beena Paul
| studio         = Trends Add Films Innostorm Entertainment Group Creative Land Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
| narrated by    = Renjith
}}
Karmayogi is a 2012 Malayalam film directed by V. K. Prakash, starring Indrajith Sukumaran|Indrajith, Nithya Menon, Padmini Kolhapure, Saiju Kurup, Ashokan (actor)|Ashokan, Thalaivasal Vijay and Manikuttan. The film is an adaptation of Shakespeares Hamlet.  Indrajith plays the protagonist in the film.

==Plot==
The film tells the story of Rudran Gurukkal, the lone male descendant of the Chathothu family. The family represents the Yogi community, in which Lord Shiva is believed to have been born. Rudran is haunted by a strange kind of destiny. This forms the crux of the story. 

==Accolades==
* Karmayogi was screened in the competition section at the 42nd International Film Festival of India (IFFI), in Panaji, Goa in 2011.
* Karmayogi was screened in the Malayalam Cinema Today section at the 16th International Film Festival of Kerala (IFFK), in Thiruvananthapuram, Kerala in 2011.   
* Risabava won the 2011 Kerala State Awards for dubbing for his work in the film.

==Cast== Indrajith as Rudran Gurukkal
* Nithya Menon
* Padmini Kolhapure as Mankamma, Rudrans mother 
* Saiju Kurup Ashokan
* Thalaivasal Vijay
* Manikuttan

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 

 