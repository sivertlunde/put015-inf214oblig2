A Message from Mars (1913 film)
:For the 1903 New Zealand film version of the play, see A Message from Mars.
{{Infobox film
| name           = A Message From Mars
| image          = 
| image_size     =
| caption        = 
| director       = J. Wallett Waller
| producer       = Nicholas Ormsby-Scott
| writer         = Richard Ganthony J. Wallett Waller
| based on       = play A Message From Mars by Richard Ganthony
| narrator       =
| starring       = 
| music          =
| cinematography = 
| editing        = 
| studio         = United Kingdom Photoplays
| distributor    = 
| released       = July 1913
| runtime        = 68 minutes
| country        = United Kingdom English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
A Message From Mars (1913 in film|1913) is a British science fiction film. In September 2014, the British Film Institute announced that they were putting the restored feature film online on their website.
 tinting and toning of the film. According to the BFI website, the plot (see below) is very similar to that of the Charles Dickens novella A Christmas Carol.  

==Cast== Charles Hawtrey as Horace Parker
*E. Holman Clark as Ramiel
*Crissie Bell as Minnie Templer
*Frank Hector as Arthur Dicey
*Hubert Willis as a tramp 
*Kate Tyndale as Aunt Martha
*Evelyn Beaumont as Bella 
*Eileen Temple as Mrs. Claremce
*R. Crompton as the God of Mars
*B. Stanmore as the wounded man
*Tonie Reith as the wounded man’s wife

==Plot==
Horace Parker (Hawtrey) is a wealthy young man who is exceedingly selfish and self-centered. Not only is he a miser, but he also expects his friends (and everyone else) to conduct their lives according to his personal convenience.

Parker is engaged to Minnie Templer (Bell), but Minnie has discovered Parkers selfishness and she is on the brink of calling off the engagement.

However, on Christmas Eve, a messenger from Mars comes to Earth to show Parker the error of his ways. The two of them become invisible and eavesdrop on all the terrible—and true—things Parkers friends and family are saying about him.

==Taglines==
"A fantastical photo-drama, in four parts."

==See also==
*List of incomplete or partially lost films

==References==
 

==External links==
* 
* 
* 

 
 
 

 
 