The Angels Wash Their Faces
{{Infobox film 
| name            = The Angels Wash Their Faces
| image           = Poster of The Angels Wash Their Faces.jpg|
| caption         = Theatrical release poster|
| director        = Ray Enright| Max Siegel|
| writer          = Michael Fessier Niven Busch Robert Buckner|
| starring        = Ann Sheridan Billy Halop Bernard Punsly Leo Gorcey Huntz Hall Gabriel Dell Bobby Jordan Ronald Reagan Bonita Granville Frankie Thomas Henry ONeill Eduardo Ciannelli|
| music           = Adolph DeLitsch|
| cinematography  = Arthur L. Todd	|
| editing         = James Gibson	|
| distributor     = Warner Bros. Pictures|
| released        =   |
| runtime         = 86 minutes| 
| language        = English|
| budget          = 
}} The Dead End Kids.

==Plot==

Gabe Ryan (Frankie Thomas) is released from reform school and it taken to a new house by his sister Joy (Ann Sheridan) to start a new life where no one knows of his past.  However, Gabe immediately joins a local gang, the Beale Street Termites,  where he meets up with William Kroner (Bernard Nedell), a local gangster.  William accuses him of starting a fire at one of his properties, and Alfred Martino (Eduardo Ciannelli), the actual arsonist, uses this opportunity to frame Gabe for any fire.  He decides to torch one of his apartment complexes so that he can collect the insurance money.  Unfortunately, one of the kids, Sleepy (Bernard Punsly) is killed in the fire.

Patrick Remson (Ronald Reagan), the Assistant District Attorney, tries to prove Gabes innocence.  His motives are not only to prove Gabes innocence, but also to get closer to his sister.  Joy has devoted her life to helping Gabe and neglects her other interests, which was rallying against city government corruption, which pleases Martino.  However, it is all for naught as Gabe is found guilty and sentenced to prison.

The other boys, led by Billy (Billy Halop), decide to do something to help Gabe.  Billy runs for "boy mayor" and wins.  He has Kroner arrested for a small infraction and sends him to jail.  While there, Billy and the rest of the gang interrogate him and try to make him admit that Gabe is innocent.  He does not cave in, that is until he is shown proof that his accomplices, Martino and the fire chief, are planning to skip the country.  He confesses and Martino and the chief are arrested and sent to prison.

==Production==
It was filmed under the title, The Battle of City Hall. It was changed to reference the title of the unrelated movie Angels with Dirty Faces. 

==Cast==
===The Dead End Kids===
*Billy Halop as Billy Shafter
*Bobby Jordan as Bernie Smith
*Leo Gorcey as Leo Finnegan
*Gabriel Dell as Luigi Batteran
*Huntz Hall as Huntz Gartman
*Bernard Punsly as Luke Sleepy Arkelian

===Additional cast===
*Ronald Reagan as Patrick Remson
*Ann Sheridan as Joy Ryan
*Frankie Thomas as Gabe Ryan Margaret Hamilton as Miss Hannaberry
*Marjorie Main as Mrs. Arkelian
*Grady Sutton as Gildersleeve (mayors secretary)
*William Hopper as Photographer (uncredited)

==References==
 

== External links ==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 