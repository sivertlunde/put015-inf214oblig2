That Night in London
{{Infobox film
| name           = That Night in London
| image          = 
| image_size     = 
| caption        = 
| director       = Rowland V. Lee
| producer       = Alexander Korda
| writer         = Dorothy Greenhill   Arthur Wimperis
| narrator       = 
| music          = Peter Mendoza Robert Martin
| editing        = Stephen Harrison
| starring       = Robert Donat   Pearl Argyle   Miles Mander   Roy Emerton
| distributor    = Paramount Pictures
| released       = August 1932
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British crime film directed by Rowland V. Lee and starring Robert Donat, Pearl Argyle, Miles Mander and Roy Emerton. 

==Synopsis==

A young bank clerk steals £500 and goes on a spree. 

==Cast==
* Robert Donat - Dick Warren
* Pearl Argyle - Eve Desborough
* Miles Mander - Harry Tresham
* Roy Emerton - Captain Paulson
* Graham Soutten - Bert
* Laurence Hanray - Ribbles

==References==
 

==External links==
* 

 
 

 
 
 
 
 


 
 