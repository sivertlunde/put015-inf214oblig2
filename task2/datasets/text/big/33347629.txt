A Boy and His Samurai
{{Infobox film
| name           = A Boy and His Samurai
| image          = ABoyAndHisSamurai2010Poster.jpg
| alt            = 
| caption        = Film poster
| director       = Yoshihiro Nakamura
| producer       = 
| writer         = 
| screenplay     = Yoshihiro Nakamura
| story          = 
| based on       =  
| narrator       = 
| starring       = Ryo Nishikido   Rie Tomosaka   Fuku Suzuki   Hiroki Konno   Keisuke Horibe   Hitomi Satō   Shiori Kutsuna   Yūji Nakamura   Jun Inoue
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2010 Japanese time travel comedy film, directed by Yoshihiro Nakamura.

==Plot==
Based on a manga by Gen Araki,  the film chronicles the adventures of a samurai who accidentally travels through time from Edo-era Japan to present-day Japan where he meets a single working mother and her young son.

==Cast==
* Ryo Nishikido - Kijima Yasube
* Rie Tomosaka - Hiroko Yusa
* Fuku Suzuki - Tomoya Yusa
* Jun Inoue - Tonoma Tomoharu (teacher)
* Keisuke Horibe - Shirozaki
* Hiroki Konno - Tanaka  
* Hitomi Satō - Yoshie Hiraishi
* Yūji Nakamura - TV presenter 
* Shiori Kutsuna

==References==
 

==External links==
*    
*  

 

 
 
 
 
 


 