Charlie Chan's Secret
{{Infobox film
| name           = Charlie Chans Secret
| director       = Gordon Wiles
| image	         = Charlie Chans Secret FilmPoster.jpeg John Stone
| writer         = Robert Ellis
| starring       = Warner Oland
| music          = Samuel Kaylin
| cinematography = Rudolph Mate
| distributor    = 20th Century Fox
| released       =  
| runtime        = 72 minutes
| country        = United States
}}
 
Charlie Chans Secret is the tenth Fox-produced film in the Charlie Chan series with Warner Oland as the detective.

== Plot ==
Alan Colby, heir to a vast fortune, reappears after a seven-year absence, only to be murdered  before he can claim his inheritance. The Lowells have been living off the Colby fortune, and now someone is trying to kill Henrietta Lowell, matriarch of the family. Among the suspects are:
*Fred and Janice Gage, who live off the Lowell (Colby) fortune, which would have gone to Alan Colby, the murdered man
*Prof. Bowen, who is paid handsomely by the Lowells for his valuable psychic research
*Mr. Phelps, the executor of the Lowell estate
*Ulrich, the caretaker who had a longstanding grudge against Alan Colby
*Henrietta Lowell, the aunt of Alan Colby who wants to continue psychic research.

Charlie Chan is called to investigate Alan Colbys murder where clues include:
*A clock deliberately set to the wrong time
*The old house has secret passageways
*A medieval dagger used to commit the murder
*A rifle rigged to fire by itself

== Cast ==
*Warner Oland as Charlie Chan
*Rosina Lawrence as Alice Lowell
*Charles Quigley as Dick Williams, Alices fiancé
*Henrietta Crosman as Henrietta Lowell, Alan Colbys aunt
*Edward Trevor as Fred Gage, Henriettas son-in-law
*Astrid Allwyn as Janice Gage, Henriettas daughter
*Herbert Mundin as Baxter the butler, who provides comic relief throughout the film
*Jonathan Hale as Warren Phelps, the Lowells lawyer
*Egon Brecher as Ulrich, caretaker
*Jerry Miley as Allen Colby, the murdered heir
*Arthur Edmund Carewe as Prof. Bowen, the Lowells psychic researcher
*Gloria Roy as Carlotta, medium, Mrs. Bowen
*Ivan Miller as Morton, police inspector

==Production== public domain copyright notice on original prints.

==See also==
*List of films in the public domain in the United States

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 