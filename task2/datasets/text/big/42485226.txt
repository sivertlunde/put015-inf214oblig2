Lan Kwai Fong 3
 
 

{{Infobox film
| name           = Lan Kwai Fong 3
| image          = Lan Kwai Fong 3 poster.jpg traditional = 喜愛夜蒲3 simplified = 喜爱夜蒲3 jyutping = hei2 ngoi3 je6 pou4 3}}
| alt            = 
| caption        = Lan Kwai Fong 3 theatrical poster
| director       = Wilson Chin
| producer       = Ng Kin-Hung
| writer         = 
| screenplay     = Au Cheuk-Man
| story          = Wilson Chin
| narrator       = Christine Ng Jason Chan Whitney Hui Alex Lam Ava Yu Jeana Ho Christine Ng Celia Kwok Charles Ying
| music          = Ronald Ng
| cinematography = Choi Man-Lung Mak Hoi-Man Yip Wai-Ying
| editing        = Matthew Hui Wai-Kit
| studio         = Mei Ah Entertainment Local Production Ltd.  
| distributor    = Mei Ah Entertainment Golden Village Pictures
| released       =  
| runtime        = 93 minutes
| country        = Hong Kong
| language       = Cantonese 
| budget         = 
| gross          = HK$7,899,509 
}}
 blue comedy Lan Kwai Jason Chan, Alex Lam, Christine Ng, Celia Kwok, and Charles Ying.    The movie is much darker than the previous two by depicting drugs and rape in club scenes. 

==Plot==
The story follows four women who hit the Hong Kong night-life district of Lan Kwai Fong, looking for excitement and love. Jeana (from the first movie) is now a model in an open, non-committed relationship with Jacky (also from the first movie), and continues with her party-hard night lifestyle. Her friend Sara, feeling neglected by her fiance, looks for excitement by hitting the clubs again. Papa is an ugly duckling who works as an infants school teacher by day and rushes home to go clubbing at night, hoping to find a guy who will love her. Jolie is a rich orphan who is looking to have fun, having just come back from England during her university holidays. 

Jeana and Papa take Sara out to the hottest club at Lan Kwai Fong, Club Magnum, to help her forget about her boyfriend Shin, who would rather hang out with his ex-girlfriend then celebrate his birthday with her. At Club Magnum they meet up with Jeanas boyfriend Jacky, who has managed Club Magnum since Ah Gongs club went out of business. Jacky introduces them to May, the owner of the club, who then introduces them to her partying younger cousin Jolie. Jolie immediately reminds Jacky of his ex-girlfriend Mavis, about whom he still feels guilty. May, knowing Jolie likes to party hard, gives Jacky the task of looking after Jolie. She hopes the two can start a relationship, since his current girlfriend Jeana will soon be leaving for Taiwan for work. Jacky hesitantly agrees to babysit Jolie only because she reminds him of Mavis. 

Meanwhile Jeana pushes Sara to forget about her boyfriend Shin by telling her to party harder and introduces her to her Korean friend Kim. Sara is instantly attracted to Kim but because she is engaged to Shin she stops the two from going past a platonic friendship. During a yacht party thrown by May, Jeana, knowing of the rough patch in Sara and Shins relationship, tries to seduce Shin and suggest Sara, Jolie and she go on a get away to Seoul, South Korea, to have fun with Kim. On the day they are to depart to Seoul Jeana backs out of the trip with only Sara and Jolie going. While Sara is in Korea, Jeana uses images of Sara having fun with Kim to coax Shin into having sex with her. Coincidentally, while in Korea, Kim is able to convince Sara to give in to her sexual desires and have sex with him. With Kim and Sara leaving Jolie behind at the club, Jolie gets gang raped by two Korean men she just met. At the police station to find out what happened to Jolie, Sara finds out Kim has a wife and was only using her for a no strings attached fling. 

Papa, who has the body but not the face, is often called pork chop (ugly girl) by all the men at the club. Being ignored by guys, she sits by herself cutting paper patterns while waiting for a guy too drunk to notice her looks to come along and have a one night stand with her. She is introduced to one of Shins friends, Parker, who takes an interest in her. The two eventually have sex one night, with Papa leaving early in the morning thinking Parker will think of it as a one night stand the next day. Parker waits outside Papas workplace the next day to tell her he is truly serious about their relationship.

May sends Jacky to Korea to take care of Jolie until she recovers from her horrific incident. In Korea Jolie tells Jacky she knows that she reminds him of his ex-girlfriend Mavis and that hes not obliged to take care of her. Once back in Hong Kong Jacky shows Jolie that he wants to take care of her, not because he has to but because he wants to. With Jeana leaving, Jacky and Jolie eventually become a couple and manage Club Magnum together. 

==Cast==
===Main cast===
*Whitney Hui as Jolie
*Ava Yu as Sara
*Jeana Ho as Jeana Jason Chan as Jacky
*Alex Lam as Shin
*Christine Ng as May
*Celia Kwok as Papa
*Charles Ying as Parker
*Lee Shi-Min as Kim (voice dubbing by Z.O. Chen Zhi-Ming)

===Guest star===
*Michael Tse as Yee Gor
*Justin Lo as himself
*Kelvin Kwan as Rain Alex Fong as guy at the gym
*Timmy Hung as guy at the gym
*Louis Cheung as preaching homeless guy
*Gregory Wong as Sean
*Bob Lam as Sau Jung

==Casting==
*Dada Chan was supposed to be the lead actress of the film, but backed out of the film the day before filming was to start. Whitney Hui, who was only to have a guest role, became the films new female lead. Since Hui is a TVB contract artiste and former Miss Hong Kong runner up she was not given any explicit sex scenes in the movie to protect her image. Instead, most of the explicit scenes originally intended for Dadas character were rewritten for Ava Yu and Jeana Hos characters. With Chan no longer part of the movie some of the cast were reshuffled and scripts rewritten. Yu was originally to play party girl Jolie while Chan was to reprise her role as Cat. 
*Gregory Wong reprises his role as Sean from the first movie in a guest spot to take a dig at Dada Chan for backing out of the film at the last minute. His character Sean mentions that his wife Cat has run out on him.
*Z.O. Chen Zhi-Ming, who plays Steven and was the lead actor in the first movie, does the voice dubbing for Korean actor Lee Shi-Mins character Kim in the 3rd movie. Z.O. was chosen to dub the character Kims voice because of his non-fluent pronunciation of Cantonese.
*Michael Tses guest spot as Yee Gor is in homage to the character he played in the Young and Dangerous film series.

==Soundtrack==
No official soundtrack was released for the movie. 
Music featured in the movie are:
*Wonderland by 24HERBS ft. Janice Vidal
*Wooh Yeah by EO2
*Feel The Bass by JW
*Do Me More (Juicy Girl) - JW
*Wave Your Hands by Gregory Fitzgerald
*Hit The Dancefloor by Donnie Dragon, Jae Hoon Shin, Mobile Mansion

==Released==
Mei Ah Entertainment released a trailer for Lan Kwai Fong 3 in November 2013.  

The film premiered in Hong Kong on December 20, 2013 and it was given a wider release on January 2, 2014.  

The film was a box office disappointment and the least profitable of the Lan Kwai Fong series.

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 