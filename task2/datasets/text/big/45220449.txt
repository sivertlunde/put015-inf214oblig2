Handsworth Songs
  riots in Handsworth and 1985 Brixton riot|London. The production company was the Black Audio Film Collective,  who also wrote the screenplay.  With cinematography by Sebastian Shah and music by Trevor Mathison, there were voice-overs by Pervais Khan, Meera Syal, Yvonne Weekes, Sachkhand Nanak Dham and Mr. McClean.  A full list of cast and credits appears on the BFI Screenonline. 

==Background==
Handsworth Songs was commissioned by   (BFI). The production company used their now renowned methods of intermixing newsreel, still photos and a sound mosaic, creating an experimental multi-layered narrative. It gives accounts of those involved in or observing the 1985 riots and more significantly their personal reflections.

Viewers create their own interpretation of narrative through navigation of the multi-faceted material presented, which is a direct response to the fragmented presentation of the story of the riots.    Ann Ogidi, the author, sees it as a journey as if wandering through an art gallery with images of the 1950s. 
 sexist film material. Their work includes Signs of Empire (1989), Images of Nationality and Handsworth Songs (1986).   
 Diasporic peoples, rather than simply Black people. This is almost a marketing strategy to help define where they might be shown. It analyses the limits of definition of an audience. 

==References==
 

==External links==
* Ann Ogidi,  , BFI Screen Online.
*   at IMDb.

 
 
 
 
 
 
 