Sarah Palin: You Betcha!
 
{{Infobox film
| name           = Sarah Palin: You Betcha!
| caption        = 
| image	=	Sarah Palin- You Betcha! FilmPoster.jpeg
| director       = Nick Broomfield Joan Churchill 	
| producer       = Marc Hoeferlin Cassian Elwes
| starring       = Sarah Palin
| cinematography = 
| editing        = Michael X. Flores
| distributor    = Freestyle Releasing
| studio         =  Channel 4 in association with Awakening Films
| released       = September 11, 2011 (2011 Toronto International Film Festival|TIFF)
| runtime        = 90 minutes
| country        = 
| language       = English
| budget         = 
}}

Sarah Palin: You Betcha! is a 2011 documentary film about Sarah Palin. Directed by Nick Broomfield and Joan Churchill, the film was produced by Marc Hoeferlin and Cassian Elwes. Shani Hinton, Sophie Watts and Gregory Unruh executive produced. The documentary premiered at the 2011 Toronto International Film Festival. $31,120 was raised through Kickstarter for the films distribution and advertising.  The film received a limited release in the United States on September 30, 2011. 

==See also==
The Undefeated (2011 film), another documentary about Sarah Palin exploring the events of her Wassila mayorship, Alaskan governship and run for the vice presidential office.

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 