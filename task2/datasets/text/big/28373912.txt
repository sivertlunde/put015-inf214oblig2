Jan Žižka (film)
 
{{Infobox film
| name           = Jan Žižka
| image          = 
| caption        = 
| director       = Otakar Vávra
| producer       = 
| writer         = Milos Václav Kratochvíl Otakar Vávra
| starring       = Zdenek Stepánek
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}} 
Jan Žižka is a 1955 Czechoslovak film directed by Otakar Vávra. The film starred Zdenek Stepánek. 

==Cast==
* Zdenek Stepánek as Jan Zizka z Trocnova
* Frantisek Horák as Jan Zelivský
* Karel Höger as Václav IV, czech king
* Vlasta Matulová as Zofie, czech queen
* Ladislav Pesek as Miserere, zany
* Jan Pivec as Zikmund Lucemburský, roman Emperor
* Václav Voska as Cenek from Vartenberk
* Vítezslav Vejrazka as Václav from Dubé
* Gustav Hilmar as Jan from Chlum

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 