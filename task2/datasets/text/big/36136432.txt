Fox Farm (film)
{{Infobox film
| name           = Fox Farm
| image          = 
| image_size     = 
| caption        = 
| director       = Guy Newall
| producer       = 
| writer         = Warwick Deeping (novel)   Guy Newall 
| narrator       =  Cameron Carr
| music          = 
| cinematography = Hal Young
| editing        = 
| studio         = George Clark Productions
| distributor    = Stoll Pictures 
| released       = 1922
| runtime        = 
| country        = United Kingdom Silent  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British silent silent drama film directed by Guy Newall and starring Newall, Ivy Duke and Barbara Everest. It is based on the 1911 novel Fox Farm by Warwick Deeping. A farmers wife becomes obsessed with the high life, and abandons him after he loses his sight.  It was made at Beaconsfield Studios. 

==Cast==
* Guy Newall - James Falconer 
* Ivy Duke - Ann Wetherall 
* Barbara Everest - Kate Falconer  Cameron Carr - Jack Rickerby 
* A. Bromley Davenport - Sam Wetherall 
* Charles Evemy - Slim Wetherall 
* John Alexander - Jacob Boase

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.
* Warren, Patricia. British Film Studios: An Illustrated History. Batsford, 2001.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 