Fareb (2005 film)
 

{{Infobox film
| name = Fareb
| image = Fareb_poster.jpg
| caption =
| director = Deepak Tijori
| producer = Sanjeev Jaiswal, Rajesh Singh Suresh Maurya (executive producer)
| writer = Girish Dhamija (dialogue) Bijesh Jayarajan (story, screenplay)
| starring = Shilpa Shetty Shamita Shetty  Manoj Bajpai.
| music = Anu Malik
| cinematography = Manoj Soni
| editing = Asif Khan
| distributor = Dayal Creations
| released =  
| runtime = 129 minutes
| country = India Hindi
| budget =
| gross =
}} romantic thriller Shilpa and Shamita Shetty and Manoj Bajpai. The music is composed by Anu Malik.

==Plot==
The film opens with the murder of business magnate Amit Singhania (Bakul Thakker) who lives a wealthy lifestyle along with his wife, Ria Singhania (Shamita Shetty), in a spacious bungalow. He runs a company, Spykar Jeans. Ria is asked to co-operates with the Police, who suspect her for foul play as she is the sole proprietor of Amits business and wealth. The police also suspect Amits subordinate, Siddharth Sardesai (Parmeet Sethi), who may have killed Amit, who was also having an affair with Sonia Sharma (Sonia Kapoor), who works for an ad agency run by Diwakar. Ria meets with Aditya (Manoj Bajpai), who also works for the same ad agency, and is attracted to him, both get have sex, but Aditya, who is married to Neha (Shilpa Shetty), and has a school-going son, Arya, decides to break off this relationship much to the chagrin of Ria, who decides to teach Aditya a lesson - and she does this by acquiring the ad agency where Aditya works, and is all set to turn Adityas life upside down. Aditya confronts her and returns home in a drunken state with blood on his clothes. Shortly thereafter Aditya is arrested by Police Inspector Kelly - and the charge is murder - that of Ria Singhania. A frantic Neha hires Advocate Milind Mehta (Milind Gunaji) - but will hiring a lawyer ensure the release of Aditya especially when the Police know that he had the motive and was possibly the last person to have been with Ria?

==Cast==
* Shilpa Shetty as Neha A. Malhotra
* Manoj Bajpai as Aditya Adi Malhotra
* Shamita Shetty as Ria A. Singhania
* Parmeet Sethi as Siddharth Sardesai
* Milind Gunaji as Advocate Milind Mehta
* Kelly Dorji as Inspector Kelly
* Shawar Ali as Plainclothes Police Officer
* Gagan Gupta		
* Sonia Kapoor as Sonia Sharma
* Firdosh Mewawala		
* Hemant Pandey	 Anup Shukla	
* Shaikh Sami		
* Bakul Thakker		
* Homi Wadia as Public Prosecutor Saxena

==External links==
*  

 
 
 
 
 

 