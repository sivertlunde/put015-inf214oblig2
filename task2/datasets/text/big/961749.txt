Overdrawn at the Memory Bank
{{Infobox film
| name = Overdrawn at the Memory Bank
| image = Overdrawn.jpg
| caption = VHS cover for Overdrawn at the Memory Bank Douglas Williams
| executive producer = Geoffrey Haines-Stiles
| producer =Robert Lantos Stephen J. Roth John Varley (short story) Corinne Jacker (teleplay)
| starring = Raúl Juliá Linda Griffiths Wanda Cannon Donald C. Moore Louis Negin Chapelle Jaffe Jackie Burroughs Maury Chaykin
| music = John Tucker
| cinematography =Barry Bergthorson
| editing =Rit Wallis
| distributor =Public Broadcasting Service New World Video (VHS and Laserdisc)
| released =  
| runtime = 83 min.
| country = United States
| awards = English
| budget =
}}
 New Jersey The Lathe of Heaven  and Between Time and Timbuktu. 
 John Varley short story. The production was not a critical success and was satirized by Mystery Science Theater 3000 (MST3K) in 1997, complete with a spoof of a public television pledge drive.

Overdrawn at the Memory Bank was not shot on film, but rather was videotaped, with extensive use of chroma key and blue screen special effects. Pixelation artifacts are clearly visible in many of the effects.

==Plot== sex change; and, with the computer unable to return him to his body, Fingals mind must be kept active by storing it in Novicorps central computer – the HX368, which controls everything from finances to the weather – until his body is located. His mind can only be maintained in such a way for a limited time before it is destroyed, forming one of the central plot points of the film.
 reported to Majority shareholders force Novicorps Chairman (the films main antagonist) to divert resources to keep Fingal alive and find his body. Apollonia (Linda Griffiths), a computer controller, is assigned to locate Fingal and keep him from hacking into Novicorps mainframe. With Apollonias help, Fingal creates a virtual world where he encounters characters from Casablanca, including a version of Humphrey Bogarts character, Rick (played also by Raul Julia). Over time he grows bored (while only minutes pass in the real world, days pass in the virtual one) and plots to bring down Novicorps finances without being removed and, thus, killed. Apollonia tries to keep Fingal out of trouble, placing her in opposition with Novicorps leaders, especially when she finds herself falling in love with Fingal and develops a conflict of interest. With Apollonias considerable help, Fingal eventually "interfaces" with the mainframe and defeats his antagonists. He also returns to his body, which has been discovered before undergoing the aforementioned sex change operation. Finally corporeal and reunited with his accomplice, Fingal and Apollonia experience a traditional happy ending, with Fingal having taken complete control of the HX368. After ordering bonuses and stocks for every employee, committing Novicorps Chairman to a month of "compulsory rehab" via doppeling and changing both his and Apollonias identity to Rick and Ingrid (respectively), the characters from Casablanca, Fingal, who by now has absolute and total access to and control of the system, vows to fight against the dystopian government. The film ends with the new couple walking out the door and, now free from Novicorps oppression, talk about opening a club on the other side of town: Ricks Place.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 