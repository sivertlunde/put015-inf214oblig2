Ashes to Honey
{{Infobox film
| name           = Ashes to Honey
| image          = Ashes to Honey poster.jpg
| caption        = Original poster
| director       = Hitomi Kamanaka
| producer       = Shūkichi Koizumi
| writer         =
| starring       =
| music          =
| cinematography =
| editing        =
| studio         = Group Gendai
| distributor    =
| released       =  
| runtime        = 116 minutes
| country        = Japan
| language       = Japanese
| budget         =
}}
 , (literally "Humming of Bees and Rotation of the Earth") is a Japanese  . 

==Content==
The documentary covers the long struggle of the residents of Iwaijima island in the Inland Sea of Japan to prevent the construction of a nuclear power plant across the bay.    It compares the situation to Sweden, where models of sustainable energy are being explored.

==Production==
Kamanaka began filming the documentary in 2008 and completed it in 2010. The 2011 Tōhoku earthquake took place right during the films first Tokyo screening. 

==Reception==
In a poll of critics at Kinema Junpo, Ashes to Honey was selected as the fifth best documentary of 2011. 

==See also==
*List of books about nuclear issues
*List of films about nuclear issues
*Rokkasho Rhapsody

==References==
 

==External links==
*  (in English)
* 

 

 
 
 
 
 
 
 
 
 
 


 
 