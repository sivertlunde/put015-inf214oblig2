The Ship from Shanghai
{{Infobox film
| name           = The Ship from Shanghai
| image          = The Ship from Shanghai poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Charles Brabin
| producer       = 
| screenplay     = John Howard Lawson
| based on       =  
| starring       = Conrad Nagel Kay Johnson Carmel Myers Holmes Herbert Zeffie Tilbury
| music          = William Axt
| cinematography = Ira H. Morgan
| editing        = Grant Whytock 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

The Ship from Shanghai is a 1930 American action film directed by Charles Brabin and written by John Howard Lawson. The film stars Conrad Nagel, Kay Johnson, Carmel Myers, Holmes Herbert and Zeffie Tilbury. The film was released on January 31, 1930, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==	 
*Conrad Nagel as Howard Vazey
*Kay Johnson as Dorothy Daley
*Carmel Myers as Viola Thorpe
*Holmes Herbert as Paul Thorpe
*Zeffie Tilbury as Lady Daley
*Louis Wolheim as Ted
*Ivan Linow as Pete Jack McDonald as Reid 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 