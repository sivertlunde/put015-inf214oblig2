Tiger Love (1924 film)
{{Infobox film
| name           = Tiger Love
| image          = 
| alt            = 
| caption        = 
| director       = George Melford
| producer       = Adolph Zukor
| screenplay     = Manuel Penella Howard Hawks Julie Herne
| starring       = Antonio Moreno Estelle Taylor G. Raymond Nye Manuel Caméré Edgar Norton David Torrence Snitz Edwards
| music          = 
| cinematography = Charles G. Clarke
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by George Melford and written by Manuel Penella, Howard Hawks and Julie Herne. The film stars Antonio Moreno, Estelle Taylor, G. Raymond Nye, Manuel Caméré, Edgar Norton, David Torrence and Snitz Edwards. The film was released on June 30, 1924, by Paramount Pictures.  

Tiger Love is a lost film. 
 
==Plot==
 

== Cast ==
*Antonio Moreno as The Wildcat
*Estelle Taylor as Marcheta
*G. Raymond Nye as El Pezuño
*Manuel Caméré as Don Ramon
*Edgar Norton as Don Victoriano Fuestes
*David Torrence as Don Miguel Castelar
*Snitz Edwards as Te Hunchback
*Monte Collins as Father Zaspard

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 

 