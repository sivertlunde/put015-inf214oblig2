Rhapsody in August
{{Infobox film name       = Rhapsody in August image      = RHAPSODY IN AUGUST.JPG director   = Akira Kurosawa producer   = Hisao Kurosawa writer     = Akira Kurosawa starring   = Sachiko Murase  Richard Gere music      = Shinichirō Ikebe studio  Shochiku Films Ltd. Shochiku Films Ltd. released   = May 25, 1991 runtime    = 98 minutes country    = Japan language   = Japanese and English music      = Shinichirô Ikebe budget     =
|}} atomic bombing American film star Richard Gere appears as Suzujiros son Clark.

==Plot== postwar Japan, Japanese economic miracle and provide most of the dialogue in the film.

Kanes grandchildren come to visit her at her rural home on Kyūshū one summer while their parents visit a man who may or may not be Kanes brother in Hawaii.  Like most children, they are bored out of their minds, find her cooking to be disgusting, and escape to the urban environment of Nagasaki the first chance they get.  While in Nagasaki the children visit the spot where their grandfather was killed in 1945 and become aware of the atomic bombing for the first time in their lives.  They slowly come to have more respect for their grandmother and also grow to question the United States for dropping the Bomb.  

In the meantime they receive a telegram from their American cousins, who turn out to be rich and offer the parents a job managing their pineapple fields in Hawaii.  Matters are complicated when, in their response, the grandchildren mention the attack, which infuriates their parents.  To smooth things over, one of the Japanese-Americans (Clark) travels to Japan to be with Kane for the anniversary.  While there, Kane and the grandchildren reconcile with Clark over the bombing.

== Cast ==
*Sachiko Murase as Kane (The Grandmother)
*Hisashi Igawa as Tadao (Kanes Son)
*Narumi Kayashima as Machiko (Tadaos Wife)
*Tomoko Otakara as Tami (Tadaos Daughter)
*Mitsunori Isaki as Shinjiro (Tadaos Son)
*Toshie Negishi as Yoshie (Kanes Daughter)
*Hidetaka Yoshioka as Tateo (Yoshies Son)
*Choichiro Kawarazaki as Noboru (Yoshies Husband)
*Mieko Suzuki as Minako (Yoshies Daughter)
*Richard Gere as Clark (Kanes Nephew)

== Reception ==
Rhapsody in August received mixed reviews on its release in 1991.  

Some critics made much of the fact that the film centered on the films depiction of the atomic bombing as a war crime while omitting details of Japanese war crimes in the Pacific War.  When Rhapsody premiered at the 1991 Cannes Film Festival,    one journalist cried out at a press conference, "Why was the bomb dropped in the first place?"  At the Tokyo Film Festival, critics of Japanese militarism said Kurosawa had ignored the historical facts leading up to the bomb. Japanese cultural critic Inuhiko Yomota commented:

"Many critics, myself included, thought Kurosawa chauvinistic in his portrayal of the Japanese as victims of the war, while ignoring the brutal actions of the Japanese and whitewashing them with cheap humanist sentiment." 

Kurosawas response was that wars are between governments, not people, and denied any anti-American agenda. 

== About the Japanese title ==
The Japanese title (八月の狂詩曲 Hachigatsu no rapusodī) is also known as Hachigatsu no kyōshikyoku.  "八月" means August, and "狂詩曲" means rhapsody. Both are Japanese kanji words. "狂詩曲" is usually pronounced "kyōshikyoku." When this film released in Japan, 1991, Kurosawa added furigana "ラプソディー rapusodī" to the word "狂詩曲" contrary to the standard usage of Japanese.    So the correct romanization of the official Japanese title is Hachigatsu no rapusodī. But, often, the Japanese title has been cited without the furigana in various media. This is the reason why the misreading Hachigatsu no kyōshikyoku has become more widely known than the correct pronunciation.

==See also==
*Atomic bombings of Hiroshima and Nagasaki
*Hibakusha
*Japanese-American

==References==
 

==External links==
* 
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 
 