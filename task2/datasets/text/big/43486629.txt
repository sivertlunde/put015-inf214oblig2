Triumph in the Skies (film)
 
 
{{Infobox film
| name           = Triumph in the Skies
| image          = TriumphintheSkiesfilm.jpg
| alt            = 
| caption        = Film poster
| director       = Wilson Yip Matt Chow
| producer       = Tommy Leung
| writer         = 
| starring       = Louis Koo Sammi Cheng Francis Ng Julian Cheung Charmaine Sheh Amber Kuo   Océane Zhu   Dean Liu
| music          = 
| cinematography = 
| editing        =  Media Asia Shaw Brothers Pictures
| distributor    = Media Asia Distributions
| released       =  
| runtime        = 100 minutes
| country        = Hong Kong   China
| language       = Cantonese   Mandarin   English
| budget         = 
| gross          = US$24.59 million (China)   
}}

Triumph in the Skies ( ) is a 2015 Hong Kong-Chinese film adaptation of Triumph in the Skies series directed by Wilson Yip and Matt Chow and starring Louis Koo, Sammi Cheng, Francis Ng, Julian Cheung, Charmaine Sheh, Amber Kuo, Océane Zhu and Dean Liu. It was announced on July 2014 that there would be a movie sequel to the successful series. Production started on 6 August 2014.   The film was released on February 19, 2015. 

==Synopsis==
Young pilot Branson (Louis Koo) recently takes over Skylette, his father’s aviation empire, only to realize his old flames Cassie (Charmaine Sheh) is a flight attendant there. Several years ago, he was forced to break up with her and move to New York to take care of his fathers business. To this day, the two continue to harbor feelings for each other but decide to keep them bottled up.

In an effort to rebrand the airline, Branson invites rock idol TM (Sammi Cheng) to star in an upcoming commercial and appoints Sam (Francis Ng) as her flying consultant. Incongruent in both tastes and experience, this odd couple gets off on the wrong foot. As the shoot progresses, however, they slowly discover each others merits, developing a strong mutual attraction. 

Jayden (Julian Cheung) has left Skylette Airline to become a pilot for private jets. He meets the young and vivacious Kika (Amber Kuo) during a flight and assumes her to be wayward and shallow. But they turn out to have a lot in common and start falling madly in love. At the height of their romance, Jayden realizes almost too late the secret behind her recalcitrance… 

Each of the three relationships comes with its own setbacks. As long as one can accept the imperfection of things, finding happiness—however fleetingly—is a blessing in itself. 

==Cast==
* Louis Koo as Branson Cheung (張春亮)
* Sammi Cheng as TM Tam (譚夢)
* Francis Ng as Samuel "Sam" Tong (唐亦琛)
* Julian Cheung as Jayden "Captain Cool" Koo (顧夏陽)
* Charmaine Sheh as Cassie Poon (潘家詩)
* Amber Kuo as Kika Sit (薛健雅)
* Océane Zhu as Winnie
* Dean Liu
* Kenneth Ma as Roy Ko (高志宏)
* Elena Kong as Heather Fong (方芮嘉)
* Jun Kung
* Ma Jin
* Liao Jingsheng

==Box office==
As of March 8, 2015, the film has earned over US$24.59 million at the Chinese box office.   

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 