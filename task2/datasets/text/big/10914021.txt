Nuovomondo
{{Infobox film
| name = Nuovomondo (Golden Door)
| image = Nuovomondo.jpg
| image_size =
| caption =
| director = Emanuele Crialese
| producer = Bernard Bouix  Tommaso Calevi  Alexandre Mallet-Guy  Luc Besson  Fabrizio Mosca
| writer = Emanuele Crialese
| starring = Charlotte Gainsbourg   Vincenzo Amato   Francesco Casisa   Filippo Pucillo  Vincent Schiavelli   Mohamed Zouaoui (Voice)
| music = Antonio Castrigano
| director of photography = Agnes Godard
| editing = Maryline Monthieux
| distributor = Miramax
| released =  
| runtime = 120 minutes 
| country = Italy
| language = Italian
| budget =
}}
Nuovomondo, literally "new world" and also known as Golden Door, is a 2006 drama film based on a familys migration from Italy to New York, United States|U.S.A. at the beginning of the 20th century. The film, written and directed by Emanuele Crialese, opens on location in Sicily and concludes in the United States. (The Ellis Island sequences were shot in a Rome studio and in Buenos Aires, Argentina.)  Vincent Schiavelli, whose character was originally planned to play a major part, died during the filming, forcing his role to become a supporting character.

The movie premiered at the Venice Film Festival on September 8, 2006 to critical praise and seven nominations (with six wins) at the festival. Many praised the directors focus on sound and visual composition. Others focused on the lack of typical iconography of the time (such as the Statue of Liberty). 
Martin Scorsese was involved in the marketing,  and introduced the film at the 2007 Tribeca Film Festival.

== Plot ==
At the turn of the 20th century, the poor Mancuso family (headed by the widowed Salvatore, Vincenzo Amato), from Sicily, Italy, emigrates to the United States. They dream of the land of opportunity, where giant vegetables are grown, people swim in milk, and coins fall from the sky. Salvatore takes his family, as well as his old mother, Fortunata (Aurora Quattrocchi). While on the ship to America, many men find the British Lucy (Charlotte Gainsbourg) attractive. For administrative reasons, Lucy wants to marry Salvatore on arrival on Ellis Island. He agrees; he understands that she is not in love with him yet, but expects that will come.

At Ellis Island, the families undergo extensive and humiliating physical and psychological examinations and questioning. Pietro (Filippo Pucillo) is about to be sent back for being Mute (disorder)|mute, and Salvatores mother for insufficient intelligence.

== Critical response ==
  
The film received generally favorable reviews from critics. The review aggregator Rotten Tomatoes reported that 72% of critics gave the film positive reviews, based on 74 reviews.  Metacritic reported the film had an average score of 74 out of 100, based on 22 reviews. 

Ann Hornaday of The Washington Post    and Wesley Morris of The Boston Globe  named it the 7th best film of 2007.

== Awards ==
The film won six awards at the Venice Film Festival, including two awards for Best Film (the CinemAvvenire and Pasinetti Awards), along with the FEDIC, SIGNIS, Silver Lion, and UNICEF awards. It was also nominated for a Golden Lion award.
The film was also nominated for a European Film Award, for the Film of the Year.
The film was Italys submission for the 79th Academy Awards, but was ultimately passed over in final nominations.

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 
 