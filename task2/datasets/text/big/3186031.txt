Kyon Ki
{{Infobox film
| name           = Kyon Ki क्योंकि  
| image          = Kyonki.jpg
| caption        = Theatrical release poster
| director       = Priyadarshan
| producer       = Sunil Manchanda
| story          = Priyadarshan
| screenplay     = Priyadarshan Neeraj Vora
| starring       = Salman Khan Kareena Kapoor Rimi Sen Jackie Shroff Sunil Shetty
| music          = Himesh Reshammiya S. P. Venkatesh (Background Score)
| cinematography = Tirru
| editing        = Arun Kumar N. Gopalakrishnan
| distributor    = Orion Pictures MAD Entertainment Ltd.
| released       =  
| runtime        =
| country        = India Hindi
| budget         = 
| gross          = 
}} One Flew Over the Cuckoos Nest. 

==Plot==
The film tells the story of a young man named Sir Anand (Salman Khan), who was deeply in love with a girl named Maya Memsaab (Rimi Sen), who was intentionally thrown into a swimming pool by her fiance for the sake of getting back at her pranks. Apparently, she drowned. After Mayas death, Anands life is completely shattered and he becomes insane.

He is brought to Sir Viv Richards Mental Sanatorium. His brother requests the doctors to admit him immediately. However, they insist on evaluating his mental condition before taking any decision. On being asked a few questions, Anand does not show any signs of insanity, and the doctors conclude that there is nothing wrong with him. They decline to admit him to the hospital. Just then, Anand spots a housefly on the table. He first tries to capture it in his palm, but it evades him. While Anands elder brother is arguing with doctors to admit him, Anand suddenly becomes very violent, picking up a club and using it to hit everything the fly lands on. He finally manages to kill the fly. This convinces the doctors that he is indeed insane, and they finally agree to admit him.

Anand is admitted to Sir Richards Mental Sanatorium, where he tries to makes friends with other inmates. One of them claims to have bought the Taj Mahal, another claims to have a tree growing sweets, and so on. This sets the scene for the entry of the chief doctor, Dr. Khurana (Om Puri). Dr Khurana is livid that a nurse had come into the operation theatre even though he had forbidden it. He yells at the matron and refuses to accept an explanation for the action of the nurse.

At the mental asylum, there are also other two main doctors, Dr. Sunil (Jackie Shroff) and Dr. Tanvi Khurana (Kareena Kapoor), Dr. Khuranas daughter.

Sunil turns out to be Anands close childhood friend. Anand makes a commotion every day and behaves like a child. This upsets Tanvi and she almost removes Anand from the asylum. One day when she finds out about his past, she feels bad for him. She apologizes to him and starts spending time with him. They soon become good friends and she falls in love with him. Tanvi starts to help cure him and one day he is completely cured. He is normal now, and remembers everything. Anand decides to leave the asylum. As soon as Tanvi finds out, she runs to Anand and expresses her love to him. He is surprised, but he appreciates her love and decides to stay. He falls in love with Tanvi too. They both are happy with each other.But there is a problem: Tanvi is engaged to someone by the name of Karan (Sunil Shetty).

When Tanvis father, Dr. Khurana finds out about her love for Anand he is angered. He tells Tanvi to forget about Anand and leave him. Tanvi refuses, and continues to love Anand. At that time Karan comes back to India. At first, Karan is unaware of Tanvi and Anands love for each other. When Karan finds out, he tells Tanvi to go and live with Anand. Meanwhile, at the asylum Anand explodes into a violent rage against Dr. Khurana, which results in him being lobotomized by Khurana. Sunil and Tanvi arrive at the asylum in order to smuggle Anand out so that he and Tanvi can run away together, but it is too late as Sunil finds out about Anand being lobotomized. Realising that Anand is much better off dead, Sunil kills his neurologically disabled friend by suffocating him with a pillow. A concluding scene is shown in which Tanvi is now a patient at the asylum and has become insane and mentally unbalanced due to the shock of Anands death. Tanvi is shown wearing the same patient number shirt that Anand was wearing when he was mentally ill.

==Cast==
* Salman Khan  as  Anand
* Kareena Kapoor  as  Dr. Tanvi Khurana
* Rimi Sen  as  Maya
* Om Puri  as  Dr. Khurana
* Jackie Shroff  as  Dr. Sunil
* Suniel Shetty  as  Karan, Special Appearance Manoj Joshi  as  P K Narayan
* Anil Dhawan  as  Deepak
* Sulabha Arya  as  Mrs. Shobhna Mathur
* Asrani  as  Asylum patient
* Arun Bakshi  as  Asylum patient Javed Khan  as  an Asylum patient
* Kurush Deboo  as  Munna, Idiot
* Atul Parchure
* Nagesh Bhonsle
* Shaurya Chauhan

==Reception==
 Garam Masala, Malayalam films. While Garam Masala became a huge success, Kyon Ki flopped.

==Soundtrack==
{{Infobox album |  
 Name = Kyon Ki
| Type = Soundtrack
| Artist = Himesh Reshammiya
| Cover =  2005
| Recorded =  Feature film soundtrack
| Length = 
| Label =  T-Series
| Producer = Himesh Reshammiya
| Reviews =

| Last album = Koi Aap Sa  (2005)
| This album = Kyon Ki  (2005)
| Next album = Vaah! Life Ho Toh Aisi  (2005)
}}

{{Track listing
| heading      = Songs
| extra_column = Playback
| all_lyrics   = Sameer
| all_music    = Himesh Reshammiya

| title1  = Aa Jeele Ek Pal Mein
| extra1  = Alka Yagnik, Udit Narayan
| length1 = 4:44

| title2  = Dil Keh Raha Hai
| extra2  = Kunal Ganjawala
| length2 = 5:06

| title3  = Dil Keh Raha Hai
| note3   = Remix
| extra3  = Kunal Ganjawala
| length3 = 4:41

| title4  = Dil Ke Badle Sanam
| extra4  = Alka Yagnik, Udit Narayan
| length4 = 4:17

| title5  = Jhatka Maare
| extra5  = Udit Narayan, Shaan (singer)|Shaan, Kailash Kher
| length5 = 6:20

| title6  = Kyon Ki Itna Pyar
| extra6  = Alka Yagnik, Udit Narayan
| length6 = 5:56

| title7  = Kyon Ki Itna Pyar
| note7   = II
| extra7  = Radha, Udit Narayan
| length6 = 5:56

| title8  = Kyon Ki Itna Pyar
| note8   = Female
| extra8  = Alka Yagnik
| length7 = 5:42
}}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 