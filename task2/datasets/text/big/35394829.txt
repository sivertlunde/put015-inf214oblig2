Snow White: A Deadly Summer
{{Infobox film
| name           = Snow White: A Deadly Summer
| image          = Snow_White_A_Deadly_Summer.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD Cover
| director       = David DeCoteau
| producer       = 
| writer         = 
| screenplay     = Barbara Kymlicka 
| story          = 
| based on       =
| narrator       = 
| starring       = Shanley Caswell Maureen McCormick Eric Roberts
| music          = Harry Manfredini
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2012
| runtime        = 85 min
| country        = United States
| language       = English
| budget         = $1,000,000
| gross          = 
}}
Snow White: A Deadly Summer is a low budget horror film. The film was released straight to DVD and digital download on March 20, 2012.   

==Plot==
Eve doesnt like her stepdaughter Snow getting in her way of being fully loved by her husband. Snows stepmother talks to a reflection of herself which tells her what to do. With her reflections persistence, Eve sends Snow to a camp for juvenile delinquents where they are killed off. As they are killed off Snow has dreams that show her the killings and give her clues into who the killer is and why it is doing what it does, but she has to be careful. Not everyone in the camp is as trustworthy as they seem and those who run it are hiding something.

==Cast==
* Shanley Caswell as Snow
* Maureen McCormick as Eve
* Eric Roberts as Grant
* R.J Cantu as Bob
* Tim Abell as Hunter
* Chase Bennett as Cole
* Chelsea Rae Bernier as Lauren
* Camille Cregan as Mya
* Eileen Dietz as Lyla
* Aaron Jaeger as Sean
* Jason-Shane Scott as Mark
* Kelsey Weber as Sara
* Hunter Ansley Wryn as Erica
* Carolyn Purdy-Gordon as Dr.Beckerman
* Patrick Lewey as Jason

==Production==
Maureen McCormick, who was in The Brady Bunch, had her first starring role after a long time. McCormick said that her only problem was having to pretend that she hated the main actress, Shanley Caswell.  The film has Snow White in the title because the main characters name is Snow and her evil stepmother wants her dead.   

==DVD release==
The DVD was released in widescreen with a Dolby 2.0 Stereo mix. The special features are a commentary with the director and two cast members, production photos, and the films trailer.   

==Reception==
A Dread Central review said that the film could have been called Snow White and the Seven Delinquents or Snow White: A Deadly Dullness.  Dawn Hunt of DVD Verdict thinks that the film fails most spectacularly on any expectations that viewers may have from Snow White being in the title. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 