Speedy (film)
{{Infobox film
| name           = Speedy
| image          = Speedy4.jpg
| caption        = Poster
| director       = Ted Wilde
| producer       = Harold Lloyd
| writer         = Albert DeMond (titles)
| starring       = Harold Lloyd Ann Christy Bert Woodruff Babe Ruth
| music          = Carl Davis (recent) Don Hulette (1974) Don Peake (1974 additional music)
| cinematography = Walter Lundin
| editing        = Carl Himm
| studio         = Harold Lloyd Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = Silent film  English intertitles
| budget         = 
}} 
Speedy is a 1928 silent film that was one of the films to be nominated for the short-lived Academy Award for Best Director of a Comedy. The film stars famous comedian Harold Lloyd in the eponymous leading role, and it was his last silent film to be released in theatres.
 John Grey (story), J.A. Howe (story), Lex Neal (story), and Howard Emmett Rogers (story) with uncredited assistance from Al Boasberg and Paul Girard Smith. The film was directed by Ted Wilde, the last silent film to be directed by him, and was shot in both Hollywood, and on location in New York City.

The plot revolves around Harold Speedy Swifts attempts to save the last horse-drawn streetcar in New York. The film contrasts the speed of life of the contemporary city with the pace of yesteryear, represented by this non-motorized mode of transport. Yankees star Babe Ruth plays one of Speedys hapless passengers.

There is a scene in the film where Speedy is seen giving the finger to himself while looking in a distorted mirror which may be the earliest motion picture depiction of that gesture.  

== Gallery ==
  An original US poster
 

== See also ==
* Harold Lloyd filmography
* List of United States comedy films

== External links ==
*  
*  
*  
*   at Virtual History
*   at Archive.org
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 