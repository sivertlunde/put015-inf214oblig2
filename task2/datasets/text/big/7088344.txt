The End of Silliness?
 

{{Infobox film
| name = The End of Silliness?!? More Really Silly Songs!
| image = veggietales_dvd_end.jpg
| caption = 2000 Lyrick Studios VHS cover
| writer = Mike Nawrocki Big Idea, Inc.
| director = Mike Nawrocki
| distributor = Lyrick Studios
| producer = Jon Gadsby
| starring = Phil Vischer, Mike Nawrocki, Lisa Vischer, Jim Poole, Mike Sage
| music = Kurt Heinecke
| editor = John Wahba Adam Frick
| released =  
| runtime = 32 minutes
| language = English
}}
"The End of Silliness?!? More Really Silly Songs!" is the eleventh episode and the second sing-along video in the VeggieTales animated series. It was released on VHS on November 23, 1998 by Word Entertainment, and again on April 11, 2000 by Lyrick Studios. It includes the two new Silly Songs since the release of Very Silly Songs! as well as a selection of other songs from previous VeggieTales episodes. It also features the previously unreleased Silly Song "The Yodeling Veterinarian of the Alps".

==Plot==
In the previous episode, Archibald Asparagus replaced Silly Songs with Larry with "Love Songs with Mr. Lunt" because one of Larrys silly songs, "The Song of the Cebu", was so silly and confusing. In The End of Silliness Larry, feeling depressed about the change, goes to Jimmys ice cream parlor (modeled after Edward Hoppers famous 1942 painting Nighthawks).  Jimmy does not fully grasp the situation, and tries unsuccessfully to cheer Larrys spirits by showing him past Silly Songs. Archibald later appears with his wife. At first he tries to avoid being recognized by Larry, but eventually he reveals himself and introduces Mr. Lunts love song, "His Cheeseburger." Now understanding the reason for Larrys distress, Jimmy becomes angry at Archibald and says that he would feel the same way if he were in Larrys position. Archibald attempts to defend his actions by saying he was only trying to uphold the shows standards, but Jimmy only continues to glare at him. Archibald apologizes and reads Larry a petition from millions of fans who believe that the misunderstanding from "The Song of the Cebu" should be forgiven. Archibald then tells Larry he can perform Silly Songs again. Overjoyed, Larry is given the honor of releasing a new song and the episode concludes with "The Yodeling Veterinarian of the Alps."

==Songs==
# Veggie Tales Theme Song (1998 version, from Wheres God When Im S-Scared?|Wheres God When Im S-Scared?!) Josh and the Big Wall)
# Promised Land (from Josh and the Big Wall)
# Good Morning George (from Rack, Shack & Benny|Rack, Shack and Benny)
# Thankfulness Song (from Madame Blueberry)
# Keep Walking! (from Josh and the Big Wall) Dave & the Giant Pickle)
# Salezmunz Rap (from Madame Blueberry)
# His Cheeseburger (Love Songs with Mr. Lunt) (from Madame Blueberry) God Made You Special!.)

==External links==
*  
* User review at  

 

 
 
 


 