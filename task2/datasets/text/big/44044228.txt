The Eyes of the World
{{Infobox film
| name           = The Eyes of the World
| image          = 
| alt            = 
| caption        =  Henry King
| producer       = Sol Lesser
| screenplay     = Brewster Morse Clarke Silvernail 
| based on       =  
| starring       = Eulalie Jensen Hugh Huntley Myra Hubert Florence Roberts Una Merkel Nance ONeil
| music          =  
| cinematography = John P. Fulton Ray June
| editing        = Lloyd Nosler 
| studio         = Inspiration Pictures
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Henry King and written by Brewster Morse and Clarke Silvernail. The film stars Eulalie Jensen, Hugh Huntley, Myra Hubert, Florence Roberts, Una Merkel and Nance ONeil. The film was released on August 30, 1930, by United Artists.   

==Plot==
 

== Cast ==
*Eulalie Jensen as Mrs. Rutledge 
*Hugh Huntley as James Rutledge 
*Myra Hubert as Myra
*Florence Roberts as The Maid
*Una Merkel as Sybil
*Nance ONeil as Myra
*John Holland as Aaron King
*Fern Andra as Mrs. Taine
*Frederick Burt as Conrad La Grange
*Brandon Hurst as Mr. Taine
*William Jeffrey as Bryan Oakley

==Preservation status==
*This film is now considered a lost film. 

==See also==
*List of lost films

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 