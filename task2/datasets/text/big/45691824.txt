The Money Habit
{{Infobox film
| name           = The Money Habit
| image          =
| caption        =
| director       = Walter Niebuhr
| producer       =  Alice Ramsey
| starring       = Clive Brook   Annette Benson   Nina Vanna 
| cinematography = Baron Ventimiglia
| editing        = 
| studio         = Commonwealth Films 
| distributor    = Granger Films
| released       = January 1924 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent crime film directed by Walter Niebuhr and starring Clive Brook, Annette Benson and Nina Vanna.  It was based on a novel by Paul M. Potter. A mans mistress helps him con a financier into buying a worthless oil well.

==Cast==
*  Clive Brook as Noel Jason  
* Annette Benson as Diana Hastings 
* Nina Vanna as Cecile dArcy  
* Warwick Ward as Varian  
* Fred Rains as Marley 
* Eva Westlake as Duchess  
* Philip Hewland as Mr. Hastings  
* Muriel Gregory as Typist 
* Kate Gurney as Mrs. Hastings  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 
 
 

 