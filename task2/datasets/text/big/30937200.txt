Locker (film)
{{multiple issues|
 
 
}}
 
 
{{Infobox film name            = Locker image           =  caption         =  director  Jonathan Burton  Braydn Michael producer        = Braydn Michael writer          = Braydn Michael starring        = Jeremy Kewley Lincoln Younes Braydn Michael Billy Pope music           = Jeremy Palmer cinematography  = Jonathan Burton editing         = Jonathan Burton distributor     = Video Education Australasia released        =   runtime         = 24:00 country         = Australia language        = English budget          = Australian dollar|A$2,500 (estimated)  (US$2.300)
}}

Locker is an Australian short film which follows the emotional journey of a teenager suffering depression and loss.

==Synopsis==

Faced with the recent loss of his mother, a father who is not coping and has become aggressive towards him, a difficult time at school, and a younger brother who doesn’t understand, Tim Kelly (Lincoln Younes) has become confused, lonely and withdrawn.

Forced to reconsider the path before him, Tim cleans out his locker and leaves school, and looks set to leave home and maybe take a further tragic step.

Through jumps in time we reveal the series of events that lead to Tim’s decision, and how Tim is saved from his destructive path by the unspoken love of his brother and father, and help from another troubled boy who ultimately becomes a friend.

==Production==
 Jonathan Burton for Young Blaze Films. It was financed by the Youth Foundations Victoria in Association with Geelong Community Telco and Geelong Central Rotary. It was distributed by Video Education Australasia Pty Ltd and released on 15 September 2009.

==See also==
*List of Australian films
*http://www.vea.com.au/Product.aspx?id=4318

==External links==
* http://www.imdb.com/title/tt1756600/
* http://www.youngblazefilms.com/frameset.html
* http://www.vea.com.au/Product.aspx?id=4318
* https://www.facebook.com/#!/group.php?gid=198729685050

 
 
 
 


 

 
 