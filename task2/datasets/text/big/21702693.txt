Airlock, or How to Say Goodbye in Space
{{Infobox Film
| name           = Airlock, or How To Say Goodbye In Space
| image          =
| image_size     =
| caption        =
| director       = Chris Boyle
| producer       = Laura Tunstall
| writer   = Michael Lesslie
| starring       = Michael Sheen Steven Waddington Derek Jacobi
| music          = Mathieu Ranson
| cinematography =
| editing        = Jerry Chater
| studio         = Abel and Baker Films
| distributor    =
| released       = 2007
| runtime        = 14 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 British science fiction film.

This film was nominated for the Shine Award at the Bradford International Film Festival 2008. 

==Plot==
On Christmas Eve, 1976, NASA astronauts Adam Banton (Michael Sheen|Sheen) and Carl Ackland (Steven Waddington|Waddington) are trapped in a capsule, hundreds of miles from Earth.  The two pioneers of the Space Race await the call that will determine their survival.  As their oxygen dwindles they receive a transmission from their President. It is not what they expect.

==Cast==
* Michael Sheen - Adam Banton
* Steven Waddington - Carl Ackland
* Derek Jacobi - President

==Reference list==
 

==External links==
* 
* 

 
 
 
 
 
 


 