Some Like It Hot
 
 
{{Infobox film
| name           = Some Like It Hot
| image          = Some Like It Hot poster.jpg
| image_size     =
| caption        = Theatrical poster
| director       = Billy Wilder
| producer       = Billy Wilder
| based on       = story by Robert Thoeren Michael Logan
| screenplay     = Billy Wilder I. A. L. Diamond Joe E. Pat OBrien
| music          = Adolph Deutsch
| cinematography = Charles Lang
| editing        = Arthur P. Schmidt
| studio         = Mirisch Company
| distributor    = United Artists
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = United States dollar|$2,883,848
| gross          = $40 million
}}
 drag in Pat OBrien, Joe E. Brown, Joan Shawlee  and Nehemiah Persoff. The film was filmed in black and white, even though color films were increasing in popularity.

==Plot==
 Disguised as women renaming themselves Josephine and Daphne, they board a train with the band and their male manager, Bienstock. Before they board the train, Joe and Jerry notice Sugar Kane, the bands vocalist and ukulele player.

Joe and Jerry become enamored of Sugar and compete for her affection while maintaining their disguises. Sugar confides that she has sworn off male saxophone players, who have stolen her heart in the past and left her with "the fuzzy end of the lollipop". She has set her sights on finding a sweet, bespectacled millionaire in Florida. During the forbidden drinking and partying on the train, Josephine and Daphne become intimate friends with Sugar, and have to struggle to remember that they are supposed to be girls and cannot make a pass at her.

Once in Miami, Joe woos Sugar by assuming a second disguise as a millionaire named Junior, the heir to Shell Oil, while feigning disinterest in Sugar. An actual millionaire, the much-married aging mamas boy Osgood Fielding III, tries repeatedly to pick up Daphne, who rebuffs him. Osgood invites Daphne for a champagne supper on his yacht. Joe convinces Daphne to keep Osgood occupied onshore so that Junior can take Sugar to Osgoods yacht, passing it off as his. Once on the yacht, Junior explains to Sugar that, due to psychological trauma, he is impotent and frigid, but that he would marry anyone who could change that. Sugar tries to arouse some sexual response in Junior, and begins to succeed. Meanwhile, Daphne and Osgood dance the tango ("La Cumparsita") till dawn. When Joe and Jerry get back to the hotel, Jerry explains that Osgood has proposed marriage to Daphne and that he, as Daphne, has accepted, anticipating an instant divorce and huge cash settlement when his ruse is revealed. Joe convinces Jerry that he cannot actually marry Osgood.

The hotel hosts a conference for "Friends of Italian Opera", who are actually mobsters. Spats and his gang from Chicago recognize Joe and Jerry as the witnesses to the Valentines Day murders. Joe and Jerry, fearing for their lives, realize they must quit the band and leave the hotel. Joe breaks Sugars heart by telling her that he, Junior, has to marry a woman of his fathers choosing and move to Venezuela. After several chases, Joe and Jerry witness additional mob killings, this time of Spats and his crew. Joe, dressed as Josephine, sees Sugar onstage singing that she will never love again. He kisses her before he leaves, and Sugar realizes that Joe is both Josephine and Junior.

Sugar runs from the stage at the end of her performance and is able to jump into the launch from Osgoods yacht just as it is leaving the dock with Joe, Jerry, and Osgood. Joe tells Sugar that he is not good enough for her, that she would be getting the "fuzzy end of the lollipop" yet again, but Sugar wants him anyway. Jerry, for his part, comes up with a list of reasons why he and Osgood cannot get married, ranging from a smoking habit to infertility. Osgood dismisses them all; he loves Daphne and is determined to go through with the marriage. Exasperated, Jerry removes his wig and shouts, "Im a man!" Osgood simply responds, "Well, nobodys perfect."

==Cast==
*Marilyn Monroe as Sugar "Kane" Kowalczyk, a ukulele player and singer
*Tony Curtis as Joe/"Josephine"/"Shell Oil Junior"
*Jack Lemmon as Jerry (Gerald)/"Daphne" 
*George Raft as "Spats" Colombo, a mobster from Chicago Pat OBrien as Detective Mulligan
*Joe E. Brown as Osgood Fielding III
*Nehemiah Persoff as "Little Bonaparte," a mobster
*Joan Shawlee as Sweet Sue, the bandleader of "Sweet Sue and Her Society Syncopators" Dave Barry as Mister Beinstock, the band manager for "Sweet Sue and Her Society Syncopators" Billy Gray as Sig Poliakoff, Joe and Jerrys agent in Chicago
*Barbara Drew as Nellie Weinmeyer, Poliakoffs secretary
*George E. Stone as "Toothpick" Charlie, a gangster who is killed by "Spats" Colombo
*Mike Mazurki as Spatss henchman Harry Wilson as Spatss henchman
*Edward G. Robinson Jr. as Johnny Paradise, a gangster who kills "Spats" Colombo
*Beverly Wills as Dolores, a trombone player, and Sugars apartment friend

==Soundtrack==
{| class="wikitable"
|- Song 
!  style="text-align:center; width:150px;"|Performer(s)
!  style="text-align:center; width:300px;"|Note(s)
|-
|"Runnin Wild (1922 song)|Runnin Wild"
| style="text-align:center;"| Marilyn Monroe
|
*Played during the opening credits
*Performed also a capella by Tony Curtis
|-
|"Liebesträume"
|style="text-align:center;"| -
|
*Performed on the organ at Mozzarellas Funeral Parlour
|- Sugar Blues"
|style="text-align:center;"| -
| Orchestra
|-
|"Down Among the Sheltering Palms"
| style="text-align:center;"| -
|
*Performed by Society Syncopators
|- Randolph Street Rag"
| style="text-align:center;"| -
| style="text-align:center;"| 
|-
|"I Wanna Be Loved by You"
| style="text-align:center;"| Marilyn Monroe
| style="text-align:center;"| 
|-
|"Park Avenue Fantasy (Stairway to the Stars)"
| style="text-align:center;"| -
| style="text-align:center;"| 
|-
|"Im Thru with Love"
| style="text-align:center;"| Marilyn Monroe
|style="text-align:center;"| 
|-
|"Sweet Georgia Brown"
| style="text-align:center;"| -
|style="text-align:center;"| 
*Played in the back room of the Funeral Parlour
|- By the Beautiful Sea"
| style="text-align:center;"| -
|
*Performed by Society Syncopators
|- Some Like It Hot"
|style="text-align:center;"| -
| Orchestra
|- La Cumparsita"
|style="text-align:center;"| -
|style="text-align:center;"| 
|-
|"Sweet Sue, Just You"
| style="text-align:center;"| -
|style="text-align:center;"| 
*In the score after "I Wanna Be Loved By You"
|-
|"For Hes a Jolly Good Fellow"
| style="text-align:center;"| -
|style="text-align:center;"| 
*Sung by all at the gangsters meeting
|-
|"Sugar Blues / Runnin Wild"
|style="text-align:center;"| -
| Orchestra
|-
|}

== Production ==

===Pre-production=== Michael Logan from the French film Fanfares of Love. Curtis, T. and Vieira, M. (2009). Some Like It Hot. London: Virgin Books, p.13  However, the original script for Fanfares of Love was untraceable, so Walter Mirisch found a copy of the German remake Fanfaren der Liebe. He bought the rights to the script and Wilder worked with this to produce a new story.    Some Like It Hot is often seen as a remake of Fanfares of Love, as both films follow the story of two musicians in search of work.  Some Like it Hot (York Film Notes) 

The studio hired Barbette (performer)|Barbette, a famous female impersonator, to coach Lemmon and Curtis on gender illusion for the film. 

Marilyn Monroe worked for 10% of the gross in excess of $4 million, Tony Curtis for 5% of the gross over $2 million and Billy Wilder 17.5% of the first million after break-even and 20% thereafter. Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 170 

===Style===
With regards to sound design, there is a ‘strong musical element,’   Some Like it Hot (York Film Notes)  in the film, with the soundtrack created by Adolph Deutsch. It has an authentic 1920s jazz feel using sharp, brassy strings to create tension in certain moments, for example whenever Spats gangsters appear. In terms of cinematography and aethsetics, Billy Wilder chose to shoot the film in black and white as Jack Lemmon and Tony Curtis in full costume and make up looked ‘unnaceptably grotesque,’ in early colour tests.  Some Like it Hot (York Film Notes) 

==Casting==
Tony Curtis was spotted by Billy Wilder while he was making the film Houdini (film)|Houdini (1953),  as he thought he would perfect for the role of Joe. "I was sure Tony was right for it" explained Wilder - "because he was quite handsome, and when he tells Marilyn that he is one of the Shell Oil family, she has to be able to believe it". 

According to York Film Notes, Billy Wilder and I.A.L. Diamond didnt expect such a big star as Marilyn Monroe to take the part of Sugar  Some Like it Hot (York Film Notes)  in face Wilder admits that "Mitzi Gaynor was who we had in mind. The word came that Marilyn wanted the part. and then we had to have Marilyn". 

==Reception==
Some Like it Hot received widespread critical acclaim. It was voted as the top film on AFIs "100 Years... 100 Laughs" poll in 2000. 

By 1962, the film had grossed $14 million in the US. 

Roger Ebert wrote about the movie, "Wilders 1959 comedy is one of the enduring treasures of the movies, a film of inspiration and meticulous craft."  http://www.rogerebert.com/reviews/great-movie-some-like-it-hot-1959  New Yorkers John McCarten referred to the film as "a jolly, carefree enterprise".   The Guardians Richard Roud claims that Wilder comes "close to perfection" with the film.  

Rotten Tomatoes reports a score of 96%, with an average score of 8.9 out of 10.  

Despite critical accolades, at the time of release it was one of the only American films to receive a "C" or Condemned rating by the Roman Catholic Churchs Legion of Decency. 

=== Awards and nominations ===

{| class="wikitable"
|- Date of ceremony Award
! Category
! Recipients and nominees Result
|- August 23 - September 6, 1959 
| Venice Film Festival
|style="text-align:center;"| Golden Lion
| style="text-align:center;"|Some Like It Hot ||  
|- National Board December 1959   National Board National Board of Review Awards
| style="text-align:center;"|  
| style="text-align:center;"|Some Like It Hot  ||  
|- February 6, 1960  
| Directors Guild of America Award Directors Guild Outstanding Achievement in Feature Film  Billy Wilder ||  
|- 13th British 1960  British Academy Film Awards Best Film from any Source
| style="text-align:center;"|Some Like It Hot||  
|- Best Foreign Actor Jack Lemmon||  
|- March 10, 1960   Golden Globe Awards Golden Globe Best Actor in a Motion Picture – Comedy or Musical Jack Lemmon ||  
|- Golden Globe Best Actress in a Motion Picture – Comedy or Musical Marilyn Monroe||  
|- Golden Globe Best Motion Picture – Musical or Comedy
| style="text-align:center;"|Some Like It Hot  ||  
|- April 4, 1960 
| rowspan="6"| Academy Awards Academy Award Best Director    Billy Wilder  ||  
|- Academy Award Best Actor Jack Lemmon||  
|- Academy Award Best Cinematography – Black-and-white Charles Lang, Jr.||  
|- Academy Award Best Art Direction – Black-and-white Ted Haworth  (Art Direction) , Edward G. Boyle  (Set Decoration) ||  
|- Academy Award Best Costume Design - Black and white   
| style="text-align:center;"|Orry-Kelly  ||  
|- Academy Award Best Adapted Screenplay  
| Billy Wilder, I. A. L. Diamond ||  
|- Writers Guild May 6, 1960   Writers Guild of America Awards Writers Guild Best Written Comedy Billy Wilder, I.A.L. Diamond  || 
|- Laurel Awards|September 28, 1960    Laurel Awards Top Female Comedy Performance Marilyn Monroe (2nd place)||  
|- Top Male Comedy Performance Jack Lemmon (2nd place)||  
|- Top Comedy
| style="text-align:center;"|Some Like It Hot (3rd place)||  
|- 1960  
| Bambi Awards Best Actor - International Tony Curtis (2nd place)||  
|-
|}

==Adaptations==
An unsold television pilot was filmed by Mirisch Productions in 1961 featuring Vic Damone and Tina Louise. As a favor to the production company, Jack Lemmon and Tony Curtis agreed to film cameo appearances, returning as their original characters, Daphne and Josephine, at the beginning of the pilot. Their appearance sees them in a hospital where Jerry (Lemmon) is being treated for his impacted back tooth and Joe (Curtis) is the same O blood type. 
 musical play Tony Roberts and Cyril Ritchard, with book by Peter Stone, lyrics by Bob Merrill, and (all-new) music by Jule Styne. A 1991 production of this show in London featured Tommy Steele and retained the original title. In 2002, Tony Curtis performed in a stage production of the film. He portrayed the character originally played by Joe E. Brown.

==See also==
* Cross-dressing in film and television
* Bollywood remake Rafoo Chakkar
* List of films considered the best

==References==
 

==Further reading==
*Curtis, Tony. The Making of Some Like It Hot, Wiley & Sons, Hoboken NJ, 2009. ISBN 978-0-470-53721-3.
*Maslon, Laurence. Some Like It Hot: The Official 50th Anniversary Companion, New York, HarperCollins, 2009. ISBN 978-0-06-176123-2.

==External links==
 
 
* 
* 
* 
* 
*Roger Eberts  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 