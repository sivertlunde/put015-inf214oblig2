Chanthaly
{{Infobox film
| name           = Chanthaly
| image          = Theatrical poster Chantaly (2012 Lao Horror Movie).jpg
| caption        = Theatrical release poster
| director       = Mattie Do
| producer       = {{Plain list |
* Mattie Do
* Christopher Larsen
* Douangmany Soliphanh}}
| screenplay     = Christopher Larsen
| starring       = {{Plain list |
* Amphaiphun Phimmapunya
* Douangmany Soliphanh
* Soukchinda Duangkhamchan
* Khouan Souliyabapha
* Soulasath Souvanavong
* Mango}}
| cinematography = Christopher Larsen
| editing        = Christopher Larsen
| released       =   
| runtime        = 98 minutes     
| country        = Lao PDR Lao
}}
 Laos    and the first Lao feature film directed by a woman.       Chanthaly has been screened at the 2012 Luang Prabang Film Festival  and the 2013 Fantastic Fest.   Pop singer Amphaiphun Phimmapunya stars in the leading role as Chanthaly, alongside Douangmany Soliphanh and Soukchinda Duangkhamchan.  

==Plot==
A young girl, raised alone by her overprotective father sequestered in their home in Vientiane, Chanthaly suspects that her dead mothers ghost is trying to deliver a message to her from the afterlife. After a change in the medication treating her hereditary heart condition causes the hallucinations to cease, Chanthaly must decide whether or not to risk succumbing to her terminal illness to hear her mothers last words." 

==Cast==
* Amphaiphun Phimmapunya as Chanthaly
* Douangmany Soliphanh as Father
* Soukchinda Duangkhamchan as Thong
* Khouan Souliyabapha as Bee
* Soulasath Souvanavong as Keovisit
* Mango as Moo

==Production==
The entire film was shot at director Mattie Dos house in Vientiane, Laos.   

==References==
 

==External links==
*  
*  
*  

 
 
 