The Dead Zone (film)
{{Infobox film
| name           = The Dead Zone
| image          = The Dead Zone.jpg
| caption        = Theatrical poster
| alt            =
| director       = David Cronenberg
| producer       =  
| based on       =  
| screenplay     = Jeffrey Boam
| starring = {{Plainlist|
* Christopher Walken Brooke Adams
* Tom Skerritt
* Herbert Lom
* Anthony Zerbe
* Colleen Dewhurst
* Martin Sheen
}}
| music          = Michael Kamen
| cinematography = Mark Irwin
| editing        = Ronald Sanders Dino De Laurentiis Company Lorimar Film Entertainment
| distributor    = Paramount Pictures   De Laurentiis Entertainment Group  
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $10,000,000 (estimated)
| gross          = $20,766,616 (Domestic)
}}
 horror thriller novel of Brooke Adams, Tom Skerritt, Herbert Lom, Anthony Zerbe, Colleen Dewhurst and Martin Sheen.
 Johnny Smith television series of the same name in the early 2000s, starring Anthony Michael Hall.

==Plot== Castle Rock, Brooke Adams). After having a headache following a ride on a roller-coaster, Johnny politely declines when Sarah asks if he wants to spend the night with her.

As he drives home through stormy weather, Johnny has a car accident that leaves him in a coma. He awakens under the care of neurologist Dr. Sam Weizak (Herbert Lom) and finds that five years have passed and that Sarah has married and had a child.

Johnny also discovers that he now has the psychic ability to learn a persons secrets (past, present, future) through physical contact with them. As he touches a nurses hand, he has a vision of her daughter trapped in a fire. He also sees that Weizaks mother, long thought to have died during World War II, is still alive and that a reporters sister killed herself.

As news of his "gift" spreads, Johnny is asked by a sheriff (Tom Skerritt) for help with a series of murders, but he wants to be left alone and therefore declines. Sarah visits with her infant son and the two have sexual interactions. Having a change of heart, Johnny agrees to help the sheriff and through a vision at the crime scene, he discovers that the sheriffs own deputy is the killer.  Before they can arrest him, the deputy kills himself. Johnny is then shot by the mans mother, who in turn is killed by the sheriff.

A disillusioned Johnny, now barely able to walk, moves away and attempts to live a more isolated life. He takes on tutoring jobs for school children, working from home until a wealthy man named Roger Stuart (Anthony Zerbe) implores him to come visit his son. Johnny and the boy, Chris, quickly form a friendship but, after seeing a vision of a boy falling through ice and drowning during a hockey game, Johnny warns Stuart not to let the boy go. Stuart ignores him, but Chris believes him and stays home. Two boys die during the trip, proving Johnny right. Johnny then realizes he has a "dead zone" in his visions, where he can actually change the future.
 US Senatorial candidate whom Sarah is volunteering for. Johnny shakes Stillsons hand and has a vision of him becoming President of the United States and ordering a nuclear strike against Russia that brings on a nuclear holocaust. He seeks Weizaks advice, asking, by way of example, if he would have killed Adolf Hitler when he had the chance, knowing in advance the atrocities Hitler would commit. Weizak replies that he would have no choice but to kill him. Johnny leaves Sarah a letter, telling her that what he is about to do will cost him his life, but it would be a sacrifice he is willing to make.

Johnny loads a rifle and takes aim at Stillson at a rally held in a church. His shot misses the target, but Stillson grabs Sarahs baby and holds him as a human shield. A photographer snaps a picture just as Johnny is shot by a security guard. Confronted by an angered Stillson, a fatally wounded Johnny grabs his hand. He now foresees Stillsons reputation and political ambitions being ruined after the photograph of his cowardly act is published in the media for all to see, and Stillson will shoot himself. Johnny says to Stillson everything will be over, as Sarah watches Johnny die.

==Cast==
* Christopher Walken as Johnny Smith Brooke Adams as Sarah Bracknell
* Tom Skerritt as Sheriff Bannerman
* Herbert Lom as Dr. Sam Weizak
* Anthony Zerbe as Roger Stuart
* Colleen Dewhurst as Henrietta Dodd
* Martin Sheen as Greg Stillson
* Nicholas Campbell as Deputy Frank Dodd
* Simon Craig as Chris Stuart

==Development==

Lorimar Film Entertainment began developing The Dead Zone film adaptation. Producer Carol Baum gave the book to screenwriter Jeffrey Boam and asked him to write a screenplay. "I saw it had great possibilities and agreed to do it," Boam said.    He developed a script with director Stanley Donen, who left the project before the film reached production at Lorimar.     Lorimar eventually closed its film division after a series of box-office failures, and soon after producer Dino De Laurentiis bought the rights to The Dead Zone. He initially disliked Boams screenplay and asked King to adapt his own novel.     De Laurentiis reportedly rejected Kings script as "involved and convoluted.",    however David Cronenberg, who ultimately directed the film, said that he was the one who decided not to use the script, finding it "needlessly brutal".  De Laurentiis rejected a second script by Andrei Konchalovsky, eventually returning to Boam.  The film was finally on track to be made when De Laurentiis hired producer Debra Hill to work with Cronenberg and Boam. 

Boam abandoned Kings parallel story structure for The Dead Zones screenplay, turning the plot into separate episodes. Boam told writer Tim Lucas in 1983, "Kings book is longer than it needed to be. The novel sprawls and its episodic. What I did was use that episodic quality, because I saw The Dead Zone as a triptych."  His script was revised and condensed four times by Cronenberg, who eliminated large portions of the novels story,    including plot points about Johnny Smith having a brain tumor.  Cronenberg, Boam, and Hill had script meetings to revise the screenplay page by page. Boams "triptych" in the screenplay surrounds three acts: the introduction of Johnny Smith before his car accident and after he awakes from a coma, a story about Smith assisting a sheriff track down the Castle Rock Killer, and finally Johnny deciding to confront the politician Stillson. Boam said that he enjoyed writing the character development for Smith, having him struggle with the responsibility of his psychic abilities, and ultimately give up his life for the greater good. "It was this theme that made me like the book, and I particularly enjoyed discovering it in what was essentially a genre piece, a work of exploitation," he said. In Boams first draft of the screenplay, Johnny doesnt die at the end, but rather has a vision about the Castle Rock Killer, who is still alive and escaped from prison. Cronenberg insisted that this "trick ending" be revised. Boam submitted the final draft of the screenplay on November 8, 1982. 

King is reported to have told Cronenberg that changes the director and Boam made to the story "improved and intensified the power of the narrative." 

==Production==
The film was shot in the Greater Toronto Area and Regional Municipality of Niagara of Cronenbergs native Ontario, Canada where some of its temporary props and structures are still in place, such as the gazebo which still stands in the small town of Niagara-on-the-Lake, where most of the in-town shots were filmed. The so-called Screaming Tunnel, located in nearby Niagara Falls, Ontario, was also used as the backdrop for one scene. The school where Johnny teaches in the beginning of the film is Summitview P.S., located in Stouffville, Ontario.

According to a David Cronenberg interview on the DVD, The Dead Zone was filmed during a relentless deep freeze in Southern Ontario which lasted for weeks, creating an authentic atmosphere of subzero temperatures and icy snow-packed terrain, which made for great natural shooting locations in spite of its being almost too cold for cast and crew to tolerate at times. Canadas Wonderland, a theme park which which is 30&nbsp;km north of Torontos city limits (and was eventually purchased by Dead Zone distributor Paramount years after the release of the film), was also used as a filming location.

In an interview on the Dirty Harry DVD set, director John Badham said that he was attached to direct the film at one stage, but pulled out as he felt the subject matter was irresponsible to show on screen.

The music soundtrack, composed by Michael Kamen, was recorded by The National Philharmonic Orchestra, London at the famous EMI Abbey Road Studios. Michael Kamen conducted the recording sessions; the orchestra was contracted and led by Sidney Sax.  This is the only Cronenberg film since The Brood (1979) for which Howard Shore did not serve as composer.

== Reception ==
 
The Dead Zone was granted very favorable reviews, holding a 90% "Fresh" rating on Rotten Tomatoes. 

==See also==
* List of American films of 1983
* The Dead Zone (TV series)|The Dead Zone (TV series), a television series also based on the novel. The Ned Zone", a segment of the Simpsons "Treehouse Of Horror XV" episode that parodies the novel and film. Ed Glosser, Trivial Psychic", a Saturday Night Live sketch featuring Christopher Walken that parodies the film.

== References ==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 