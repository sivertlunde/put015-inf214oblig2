Mr. Popper's Penguins (film)
{{Infobox film
| name               = Mr. Poppers Penguins
| image              = Mr.PoppersPenguinsTeaser.jpg
| caption            = Theatrical release poster Mark Waters John Davis
| based on           =  
| screenplay         = Sean Anders John Morris Jared Stern
| starring           = {{Plain list |
* Jim Carrey
* Carla Gugino
* Madeline Carroll
* Clark Gregg
* Angela Lansbury
}}
| music              = Rolfe Kent
| cinematography     = Florian Ballhaus
| editing            = Bruce Green Davis Entertainment Company Dune Entertainment Twentieth Century Fox
| released           =  
| runtime            = 94 minutes
| country            = United States
| language           = English
| budget             = $55 million   
| gross              = $187,361,754  
}} comedy family Mark Waters, John Davis, same name. The film was originally slated for a release on August 12, 2011, but was moved up to June 17, 2011.   

==Plot==
 
Thomas "Tom" Popper (Jim Carrey) is a divorced real-estate entrepreneur whose father traveled around the world during his childhood. He learns that his father has died during an adventure to Antarctica and discovers that his fathers will asks for his son to be given a souvenir from his last adventure. The next week, a crate containing a gentoo penguin (Captain) shows up at his doorstep. Eventually, five more penguins arrive. Popper intends to give them away but changes his mind when his children, Janie and Billy (Madeline Carroll and Maxwell Perry Cotton), think that the animals are Billys birthday present. That night, Popper meets with New York zookeeper Nat Jones (Clark Gregg), who asks for the penguins. Popper tries to stall him by asking him to collect the penguins another time. Jones agrees but not before warning that the conditions in Poppers apartment are not good enough to raise penguins. Popper is forced to pay the board of his apartment to keep the penguins in his apartment and lies to his arrogant neighbor Kent (David Krumholtz) about there being no animals in his home.

At work, Popper is given the task of buying Tavern on the Green, an old restaurant where he used to eat with his father as a child, with the intent of tearing it down and building a new development in its place. However, its elderly owner, Selma Van Gundy (Angela Lansbury), will only sell it to a person of true value. Popper has unsuccessful meetings with her, and she refuses to sell to him. Having the penguins around helps Popper become closer to his children. He also begins dating their mother, Amanda Popper (Carla Gugino), again. The penguins eventually lay three eggs. Two eggs hatch, and one doesn’t. Popper becomes obsessed with seeing the last egg hatch, losing his job in the process when he angers his bosses for not focusing on the tavern. 

Upon realizing from Jones that the egg can’t be hatched, Popper feels he is not capable of raising the penguins and donates them to Jones for the New York zoo. He then is re-hired by his bosses and refocuses on purchasing the Tavern on the Green. His children and ex-wife are disappointed by his decision. The next day, his ex-wife prepares to go to Africa with her boyfriend and leave the kids in his care. 

Popper finds a lost letter from his father telling him to hold his children close and love them. Having a change of heart, Popper asks his children and ex-wife to help him get the penguins back from the zoo. He and his family interrogate Jones about the penguins, which Jones claims were separated and traded to other zoos. Popper finds that Jones is lying. He frees the penguins from a cooler in the office and locks Jones inside. When preparing to leave, Popper and his family notice that Jones has escaped and security guards are looking for them. Popper and his family get away with the penguins and flee to the tavern with Jones in pursuit. 

Upon seeing how Popper had reunited his family and rescued the penguins, Van Gundy agrees to sell him the restaurant. Rather than tear it down, Popper orders that the restaurant should be renovated and reopened. Jones arrives with the police to arrest Popper for theft of the penguins. After Popper explains the story, the officers agree to spare him from prison if they determine who the penguins love. Jones holds up a sardine for the penguins, but they go to Popper. The police officers arrest Jones instead. Van Gundy protects Mr. Popper from arrest due to her friendship with New Yorks Mayor, thus leaving Popper in custody of the penguins.

Popper and his family travel to Antarctica with the penguins, allowing them to live with their own kind and promising to visit. Poppers first penguin, Captain, is revealed to have laid another egg. Popper tells his children that theyll visit when the baby is born.

==Cast==
* Jim Carrey as Thomas "Tom" Popper, Jr.
** Henry Keleman as Young Tom Popper, Jr.
** Dylan Clark Marshall as Younger Tom Popper, Jr.
* Carla Gugino as Amanda Popper
* Madeline Carroll as Janie Popper
* Maxwell Perry Cotton as Billy "Bill" Popper
* Angela Lansbury as Selma Van Gundy
* Desmin Borges as Daryl
* Philip Baker Hall as Mr. Franklin
* Dominic Chianese as Mr. Reader
* Clark Gregg as Nat Jones
* Ophelia Lovibond as Pippi
* Jeffrey Tambor as Mr. Gremmins
* David Krumholtz as Kent
* James Tupper as Rick
*Frank Welker as The Voice of Captain, Nimrod, Lovey, Loudy, Bitey, Stinky & Other Penguin Offspring

==Production== Mark Waters was chosen to direct. Filming began in October 2010, and finished in January 2011. On September 21, 2010, it was confirmed that Carla Gugino joined the cast as Toms former wife Amanda Popper.  Rhythm and Hues Studios did the penguin animations for certain shots. The musical score was ambitious, with music playing nearly throughout. It was written by Rolfe Kent, and orchestrated by Tony Blondal. It was recorded at the scoring stage at 20th Century Fox in Century City, Ca. with a 78 piece orchestra. An online Museum Slide Game created by designer Mark Kavanaugh as promotional material for the film is still accessible. 

==Reception==
===Box office === Green Lantern Super 8. The opening was at the high end of 20th Century Foxs expectations, which was predicting a mid to high teens opening. In its second weekend, the film faced competition from Cars 2 and dropped 45% to $10.1 million and ranked in fifth place. Over the four-day Independence Day holiday weekend, it ranked in eighth place after dropping 34% to $6.7 million. The film has earned $68,224,452 domestically and $119,137,302 in foreign countries, grossing a total of $187,361,754 worldwide.

===Critical response===
The film received mixed reviews from critics, with a Rotten Tomatoes rating of 48%, based on 136 reviews, as of August 2012. The sites consensus reads "Blandly inoffensive and thoroughly predictable, Mr. Poppers Penguins could have been worse—but it should have been better."
 

==Home media==
Mr. Poppers Penguins was released on Blu-ray and DVD on December 6, 2011. It includes a short film called Nimrod and Stinkys Antarctic Adventure.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 