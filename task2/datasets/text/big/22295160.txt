Eat the Peach
{{Infobox film name = Eat the Peach director = Peter Ormrod writer = Peter Ormrod, John Kelleher starring = Eamon Morissey, Catherine Byrne, Niall Toibin, Bernadette ONeill country = Ireland   United Kingdom language = English
}}
Eat the Peach is a 1986 British-Irish comedy film, directed by Peter Ormrod. The title derives from the  T.S.Eliot poem The Love Song of J. Alfred Prufrock. It was written by Peter Ormrod with John Kelleher. It is a film about eccentricity and companionship and was part financed by Channel Four.

==Plot==
 Eamon Morissey), Wall of Death -  a high walled barrel-like tank where centrifugal force keeps the rider up in the air circling. Straight away Vinnie makes diagrams, and measures - and clears a patch of land near his house. His wife, Nora (Catherine Byrne - Alice More in the series The Tudors), protests and goes back to her mother with their little girl, Vicky. Its a new kitchen she wants, not a Wall of Death. The men however, continue with the work and sinking tree posts into the ground and putting up a huge cylindrical construction. They become energetic and resourceful. Vinnie believes his Wall of Death will be a source of income - that people will buy tickets to stand on a gallery around the top of the rink and watch him and Arthur give their daring performances. Nora returns.

The film is based on actual events: a true story of two brothers-in-law Connie Kiernan and Michael Donoghue living in Granard, County Longford (Ireland). They build a wall of death in their back garden for fun. The director, Peter Ormrod,  had seen a huge, wooden tank just off the road when he was looking for items for Irish television.

==Critical reception== Juno and the Paycock." 

==Filming==
Eat The Peach was filmed on location in Newcastle, Dublin|Newcastle, County Dublin.
Motorcycle stunts were performed by riders from Messhams Wall Of Death. 

==References==
 

==External links==
* 

 
 
 
  
 
 
 