The Worthless (film)
{{Infobox film
| name           = The Worthless
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Mika Kaurismäki
| producer       = 
| writer         = Aki Kaurismäki Mika Kaurismäki
| starring       = Matti Pellonpää Pirkko Hämäläinen Juuso Hirvikangas
| music          = Anssi Tikanmäki
| cinematography = Timo Salminen
| editing        = Antti Kari
| studio         = Villealfa Filmproductions
| distributor    = Finnkino
| released       =  
| runtime        = 119 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}} Finnish film directed by Mika Kaurismäki, who also co-wrote the film with his brother Aki Kaurismäki. It is a road movie about two men and a woman driving around the country as they are being chased by a group of criminals and the police. 
 Best Direction for the film. 

== Cast ==
* Matti Pellonpää as Manne
* Pirkko Hämäläinen as Veera
* Juuso Hirvikangas as Harri Salminen
* Esko Nikkari as Hagström
* Jorma Markkula as Mitja
* Asmo Hurula as Väyry
* Ari Piispa as Vasili
* Aki Kaurismäki as Ville Alfa
* Aino Seppo as Tiina
* Veikko Aaltonen as Juippi
 

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 