The Old Lady and the Pigeons
{{Infobox film
| name           = The Old Lady and the Pigeons
| image          = The Old Lady and the Pigeons poster.jpg
| caption        = Theatrical release poster
| director       = Sylvain Chomet
| producer       = Didier Brunner Bernard Lajoie
| writer         = Sylvain Chomet
| music          = Jean Corti
| editing        = Chantal Colibert Hélène Girard
| studio         = Les Armateurs
| distributor    = Les Grands Films Classiques
| released       =  
| runtime        = 24 minutes
| country        =  
| language       = English
| budget         = 
}}
The Old Lady and the Pigeons ( ) is a 1997 animated short film written and directed by Sylvain Chomet. It tells the slightly surreal story of a starving policeman who dresses up as a pigeon and tricks an old lady into feeding him. The film was produced through the French company Les Armateurs with support from companies in Canada, Belgium and the United Kingdom. It was Chomets debut film and won several awards including the Grand Prix at the Annecy International Animated Film Festival.

==Plot==
An emaciated Parisian policeman discovers an old lady who feeds pigeons in the park excessively. After having a nightmare ending in giant pigeon-men pecking at his stomach, the policeman constructs a pigeon mask, which he wears to the old ladys home. She welcomes him inside and, despite his rude behavior, allows him to gorge himself. As weeks pass, the policeman grows increasingly fat. As he goes up flights of stairs to the womans home each day, he passes a maid sweeping the floor.

Eventually, the policeman discovers the old ladys other pet: the woman who swept the floor, dressed as a cat. When the old lady spots the police man, she begins to pursue him with a large pair of shears. He tries to remove his fake pigeon head, to reveal himself as human, but it has become stuck around his neck. As the old lady is cornering him, the policeman falls out the window and onto the street, among a small group of pigeons. In the final scene the policeman, skinny once more and without his pigeon suit, is seen behaving like a pigeon in front of the Eiffel Tower.

==Dialogue==
In Jacques Tati|Tati-esque fashion, the bulk of the film is without any dialogue at all. And what there is in English: supplied by overweight American tourists in the opening and closing scenes.

==Production== National Center of Cinematography. The backgrounds were designed by Nicolas de Crécy, who had studied with Chomet and previously collaborated on comics projects. The team produced the films first four minutes at the Folimage studios in Bourg-lès-Valence. They then attempted to use the finished footage to attract more investors, but failed. In 1993 Chomet relocated to Canada in hope of a fresh start; however, Brunner suddenly managed to pre-sell the film to the BBC and several other broadcasters, and production could continue.  Five years after production started the film was completed. 

==Reception==
The film competed at the 1997 Annecy International Animated Film Festival. On 27 May 1998 it was released theatrically in France through Les Grands Films Classiques. It was screened together with the animated short film Bobs Birthday, directed by Alison Snowden and David Fine.  Bernard Génin reviewed The Old Lady and the Pigeons for Télérama and called Chomets directing "brilliant". He also complimented Crécys background art, and wrote: "Paris streets, cozy interiors, characters puffy faces &ndash; each shot is a beauty! Yes, the traditional, handmade cartoon can still surprise us." 

===Accolades=== BAFTA Award 1998 César Awards, and the United States Academy Award for Best Animated Short Film at the 70th Academy Awards.   

{| class="wikitable" border="1" align="center"
|-
! Event !! Award !! Outcome
|- Best Animated Short Film ||  
|-
| 1999 Algarve International Film Festival || Grand Prize of the City of Portimão ||  
|-
| rowspan="2" | 1998 Angers European First Film Festival || Audience Award, Short Film ||  
|-
| European Jury Award, Short Film ||  
|-
| 1997 Annecy International Animated Film Festival || Grand Prix, Best Animated Short ||  
|-
| 50th British Academy Film Awards || Best Animated Short Film ||  
|-
| 1997 Cartoon Forum || Cartoon dor ||  
|-
| César Awards 1998 || Best Short Film, animation ||  
|- Best Animated Short ||  
|-
| 1998 Hiroshima International Animation Festival || Grand Prize ||  
|-
| 1997 World Animation Celebration || Grand Prize ||   
|}

==References==
;Notes
 
;Bibliography
*  

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 