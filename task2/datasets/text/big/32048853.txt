Swing Shift Maisie
{{Infobox film
| name           = Swing Shift Maisie
| image_size     = 225px
| image	         = Swing Shift Maisie FilmPoster.jpeg
| alt            = The Girl in Overalls
| caption        = 
| director       = Norman Z. McLeod
| producer       = George Haight
| writer         = Wilson Collison Robert Halff Mary C. McCall Jr.
| narrator       =  James Craig Jean Rogers
| music          = Lennie Hayton
| cinematography = Harry Stradling Sr.
| editing        = Elmo Veron
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 87 minutes
| country        =  
| language       = English
| budget         = 
| gross          = 
}}
 James Craig and Jean Rogers.

==Plot== James Craig). Breezy gets Curley a job at the airplane company where he works and offers to use his influence on Maisies behalf too. However, she insists she can get a job there on her own merits. She lands on the swing shift at the aircraft manufacturing factory. Breezy does, however, get her a room at Maw Lustvogels (Connie Gilchrist) boarding house. Despite her initial dislike for him, Maisie starts falling in love.

Maisie stops a suicide attempt by failed actress Iris Reed (Jean Rogers), who lives across the hall, and persuades her to get a job in the same place. Her kindness backfires on her when Breezy is more attracted to Iris. The two soon become engaged.

When Breezy gets his wish to join the Air Force, he asks Maisie to look after Iris while he is away being trained. Maisie finds this a difficult task, as Iris turns out to like men altogether too much. When Iris gets fired, she uses her feminine charms to get Judd Evans, a factory clerk, to pay the rent for an apartment in his building. She skips out without paying Maw the back rent she owes, even though Maisie gives her $20 to do so, and she has just received a $100 money order from Breezy.

Maisie finds Judd comforting Iris after another "suicide attempt". She orders Iris to tell Breezy that she no longer loves him when he comes home on leave to marry her. However, Iris secretly accuses Maisie of suspicious behavior to the factorys security department. While Maisie is being questioned, Iris and Breezy drive to Yuma  to get married. Maisie cleverly confesses to being a saboteur and implicates Iris and Breezy as fellow agents. They are stopped by the police, but let go after Breezy produces his identification. However, Breezy learns that Maisie is being held, even though Iris told him that she left to attend the funeral of an aunt. Having learned of Iriss shady character, Breezy goes back to Maisie. She is initially unwilling to take him back, but eventually gives in.

==Cast==
* Ann Sothern as Maisie Ravier James Craig as Brian "Breezy" McLaughlin
* Jean Rogers as Iris Reed
* Connie Gilchrist as Maw Lustvogel
* John Qualen as Horatio Curley
* Kay Medford as Ann Wilson
* The Wiere Brothers as Harry, Herbert and Sylvester Smith / Schmitt
* Jacqueline White as Grace Betty Jaynes as Ruth
* Frederick Brady as Judd Evans (as Fred Brady)
* Marta Linden as Emmy Lou Grogan
* Celia Travers as Helen Johnson
* Donald Curtis as Joe Peterson

==References==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 