Rip, Sew and Stitch
{{Infobox Film |
  | name           = Rip, Sew and Stitch
  | image          = RipSewStitchTITLE.jpg
  | caption        = 
  | director       = Jules White Felix Adler Harold Brauer
  | cinematography = Ray Cory  Edwin Bryant
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 32"
  | country        = United States
  | language       = English
}}
Rip, Sew and Stitch is the 150th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

== Plot == Harold Brauer), they think that catching him might end their financial woes. Hargan conveniently ducks into their shop and leaves a suit jacket with a safe combination in its pocket. After quietly sneaking back into the shop while the Stooges are elsewhere searching for clues, Hargan snatches a handful of suit jackets in hopes of retrieving the combination. He then later returns with his henchmen, and a wild mêlée follows. The Stooges end up getting the reward to pay off their debts and, with a stroke of luck, wind up with the crooks bankroll as well.
 ) in Rip, Sew and Stitch.]]

==Production notes==
Rip, Sew and Stitch is a remake of 1947s Sing a Song of Six Pants, using ample stock footage.    A double is used for Harold Brauer in the new footage. 

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 