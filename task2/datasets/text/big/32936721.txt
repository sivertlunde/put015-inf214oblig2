Wir Wunderkinder
{{Infobox film
| name           = Wir Wunderkinder
| image          = Wir_Wunderkinder_poster.jpg Robert Graf.
| director       = Kurt Hoffmann
| producer       =   Rolf Thiele
| writer         =     based on a novel by   Robert Graf
| music          = Franz Grothe
| cinematography =  
| editing        = Hilwa von Boro
| studio         = Filmaufbau GmbH
| distributor    = Constantin Film Verleih
| released       =  
| runtime        = 107 minutes
| country        = West Germany
| language       = German
| budget         =
| gross          =
}}
 Robert Graf. The black-and-white film is also known in English as Arent We Wonderful?.

==Plot==
The film recounts the lives of two schoolmates, Hans Boeckel and Bruno Tiches from the fictional town of Neustadt an der Nitze, against the backdrop of German history in the first half of the 20th century. It is told by a narrator (Wolfgang Neuss) who is supported by Wolfgang Müller with music. Through their presentation and discussion of events in the film these two provide a running commentary on political and social issues between the acts of the movie and link them together with explanations and songs.
 Emperor William II. Hans is caught and punished, but Bruno stows away, causing the balloon to crash far from its intended goal. However, he tells a tall tale of his trip to Leipzig and his meeting with the Kaiser, for which he is congratulated and awarded a gift. This marks a trend for the future: While hard-working Hans has to work struggle for everything in life, carefree Bruno is lucky and seems to succeed without even trying.
 inflation of 1923, Hans has to sell newspapers to pay his way through university at Munich. Bruno calls on him and asks for some food and money, having quit his job as a "slave of the Jewish capital" to join up with the Bewegung of Adolf Hitler. Hans falls in love with fellow student Vera von Lieven. However, she becomes ill and has to go to Switzerland for her health. Hans finishes his PhD and becomes a journalist with a Munich newspaper. Bruno joins the NSDAP and makes a career as a Nazi.

During a Fastnacht party in 1932, Hans meets Kirsten, a student from Denmark. After the Nazis seize power, Siegfried Stein visits Hans and asks him for help. Stein wants to know whether he should flee across the border immediately or wait for a passport. Hans promises to talk to Bruno, who is now in a position of authority in the party and lives in a large house confiscated from a Jewish doctor. However, he finds that Bruno is quite unwilling to help a Jew.
 proper fashion. Asked for help, Bruno is willing to oblige, but only on condition that Hans joins the party. Hans refuses.

In 1939, Kirsten returns to Munich where Hans works as an assistant at a bookstore. They go to Denmark together and get married, but when war starts, Hans has to return to Germany and she accompanies him. The final scene from the Nazi years shows Bruno in 1944 giving a "holdout" speech.

With the war over, Hans returns to Neustadt, having been a POW. He, Kirsten and their two children live in quite poor conditions while Bruno, who has changed his surname to "Anders", makes a comfortable living from his black market activities. The lot of the Boeckel family only improves when Siegfried Stein, now a member of the occupation forces, gets Hans a job at a newspaper in Munich.

Finally, in the period known as the Wirtschaftswunder, Bruno has risen to the rank of Generaldirektor (head of company) while Hans works as a journalist. When he writes a critical article, referencing Brunos Nazi past, Bruno visits Boeckels boss and threatens to organise an advertising boycott of the paper unless Hans retracts the story. Hans refuses and Bruno storms out. Finally abandoned by his luck, he falls into an empty elevator shaft.

== Cast ==
*Hansjörg Felmy as Hans Boeckel Robert Graf as Bruno Tiches
*Johanna von Koczian as Kirsten Hansen
*Wera Frydtberg as Vera von Lieven
*Elisabeth Flickenschildt as Mary Meisegeier
*Ingrid Pan as Doddy Meisegeier
*Ingrid van Bergen as Evelyne Meisegeier
*Jürgen Goslar as Schally Meisegeier
*Tatjana Sais as Mrs. Häflingen
*  as Mrs. Roselieb
*Michl Lang as Anton Roselieb
*Wolfgang Neuss as Narrator
*  as Hugo
*Peter Lühr as Chief editor Vogel
*Hans Leibelt as Mr. Lüttenjense
*Lina Carstens as Mrs. Vette
*Pinkas Braun as Siegfried Stein
*Ernst Schlott as Dr. Sinsberg
*Ralf Wolter as Bathroom waiter
*Horst Tappert as Teacher Schindler
*Franz Fröhlich as Fruit salesman
*Ludwig Schmid-Wildy as Old man
*Karl Lieffen as Obmann Wehackel
*Otto Brüggemann as Dr. Engler
*Helmuth Rudolph as Baron von Lieven
*Karen Marie Løwert as Mrs. Hansen
*Emil Hass Christensen as Mr. Hansen
*Michael Burk as Cabaret artist
*Rainer Penkert as Cabaret artist
*Fritz Korn as Cabaret artist
*Lisa Helwig 

==Production==
This satire  is based on the novel Wir Wunderkinder by Hugo Hartung, first published in 1957. Günter Neumann and Heinz Pauck wrote the script. Kurt Hoffmann directed it and it was produced by Hans Abich and Rolf Thiele. 
  Roman Theatre) and Sicily.   

==Release==
The film premiered on 28 October 1958 at the Sendlinger Tor-Lichtspiele in Munich. 

==Reception==
The film received numerous awards. Most notably, it won the   in  1958 and won both the Great Price at the International Film Festival at Acapulco and a Golden Medal at the 1st Moscow International Film Festival in 1959.    

Robert Graf won the Filmband in Silber as Best Newcomer at the Deutscher Filmpreis in 1959 for his role as Bruno. The Verband der Deutschen Filmkritik (Association of German Film Critics) awarded its price for Best Actress to Johann von Koczian for this movie.  
 
More recently, Wir Wunderkinder was selected by academics and critics as one of the "Most Important German Movies", for which a total of 132 films from the period 1895 to 2008 have been chosen.   

The Lexikon des internationalen Films calls the film "an imaginative, cabaret-critical-satirical cross-section of four decades of German history", but "a typical movie of the Adenauer era with a basically positive attitude despite all the criticism".   

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 