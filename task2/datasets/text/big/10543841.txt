Pleasure Pill
{{Infobox Film
| name           = Pleasure Pill
| image          =
| image_size     = 
| caption        = Sgrin Media Agency
| director       = 
| producer       = Kate Crowther (producer), Jane Dauncey (executive producer)  
| writer         = 
| narrator       = 
| starring       = Sharon Morgan Duncan Betts
| music          = Neil White 
| cinematography = Rory Taylor    
| editing        = Mike Hopkins  
| distributor    = 
| released       = Wales 2002
| runtime        = 3 minutes
| country        = United Kingdom
| language       = English language
| budget         = 
}} HTV and the UK Film Council in 2002.

The 3-minute short film is the story of Kath, a bored middle-aged housewife, who is given a new medication with unusual side-effects - it causes spontaneous sexual arousal at the most embarrassing moments.

The film stars Sharon Morgan and Duncan Betts.

== Film Festival Screenings ==
Toronto Britpics, Commonwealth Film Festival, Curzon Serious About Shorts, Clermont-Ferrand Short Film Festival, International Film Festival of Wales, Durango International Film Festival (Colorado, USA), Tiburon International Film Festival (CA, USA).

==External links==
* 

 
 
 
 
 
 
 

 
 