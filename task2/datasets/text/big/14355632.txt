Dreamboat (film)
 
{{Infobox film
| name           = Dreamboat
| image_size     = 
| image          = Dreamboat FilmPoster.jpeg
| caption        = 
| director       = Claude Binyon
| producer       = Sol C. Siegel
| based on       =  
| writer         = Claude Binyon
| narrator       = 
| starring       = Clifton Webb Ginger Rogers Anne Francis Jeffrey Hunter
| music          = Cyril J. Mockridge
| cinematography = Milton R. Krasner James B. Clark
| distributor    = 20th Century Fox
| released       = July 26, 1952
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2 million  
}}

Dreamboat is a 1952 comedy film starring Clifton Webb as a college professor with a mysterious past.

==Plot==
  and Clifton Webb in Dreamboat]]
The respectable lives of Professor of English literature Thornton Sayre (Clifton Webb) and his daughter Carol (Anne Francis) are severely disrupted when it is revealed that he was once a matinee idol known as "Dreamboat". His films are being shown on a television show hosted by his former costar Gloria Marlowe (Ginger Rogers). The college administrators clamor for his resignation, but President Mathilda May Coffey (Elsa Lanchester) requests and is given discretionary power to decide what to do. In private, she admits to Thornton that she had been one of his biggest fans. 

Thornton hastily leaves for New York to get an injunction against the show, taking Carol along. There they meet Sam Levitt (Fred Clark), the man responsible for airing the movies. While Sam and Gloria try to get Thornton to change his mind, Sam has underling Bill Ainslee (Jeffrey Hunter) show Carol the sights. Undaunted, Thornton eventually gets his injunction, but his life is irreparably changed. He is fired after spurning Coffeys advances, and Bill and Carol have fallen in love and are planning to get married.
 Sitting Pretty - a real film starring Clifton Webb. Gloria then reveals to Thornton that she has bought his contract and is now his boss.

==Cast==
* Clifton Webb as Thornton Sayre / "Dreamboat" / Bruce Blair
* Ginger Rogers as Gloria Marlowe
* Anne Francis as Carol Sayre
* Jeffrey Hunter as Bill Ainslee
* Elsa Lanchester as Dr. Matilda Coffey
* Fred Clark as Sam Levitt Paul Harvey as Harrington Ray Collins as Timothy Stone
* Helene Stanley as Mimi
* Richard Garrick as Judge Bowles

==Music== standard Poinciana The Bridges of Madison County.

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 