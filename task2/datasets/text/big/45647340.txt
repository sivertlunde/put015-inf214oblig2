The Cold Deck (film)
{{Infobox film
| name           = The Cold Deck
| image          =
| caption        =
| director       = William S. Hart
| producer       = New York Film Corporation
| writer         = J.G. Hawks (story & screenplay)
| starring       = William S. Hart Mildred Harris
| music          =
| cinematography = Joseph H. August
| editing        =
| distributor    = Triangle Films
| released       = November 1917
| runtime        = 70 minutes
| country        = USA
| language       = Silent..English
}}
The Cold Deck is a 1917 silent film Western directed by and starring William S. Hart.

The film is incomplete at the Library of Congress. 

==Cast==
*William S. Hart - "Level" Leigh
*Mildred Harris - Alice Leigh
*Edwin N. Wallock - Black Jack
*Sylvia Breamer - Rose Larkin (*as Sylvia Bremer)
*Charles O. Rush - Ace Hutton
*Alma Rubens - Coralie
*Joe Knight - Vigilante Chief

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 