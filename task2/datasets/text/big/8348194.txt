We Were Young
{{Infobox film
| name           = We Were Young
| image          =
| caption        = DVD cover
| director       = Binka Zhelyazkova
| producer       = Binka Zhelyazkova
| writer         = Hristo Ganev
| starring       = Dimitar Buynozov
| music          = Simeon Pironkov
| cinematography = Vasil Holiolchev
| editing        =
| distributor    = Boyana Film
| released       =  
| runtime        = 110 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         =
}}

We Were Young ( ) is a 1961 Bulgarian drama film directed by Binka Zhelyazkova and written by Hristo Ganev. The film is centres on the Bulgarian resistance to Nazism in Sofia during the Second World War.

==Plot==
Based in the days of the Bulgarian Resistance during Second World War the film deals with a young anti-fascist underground group combat operating in Sofia.  Veska and Dimo are central characters. Their love affair ends because they are apprehended by the police and jailed. Veska is tortured to death and Dimo is burnt alive cruelly. The little photographer girl, Tzveteto, also meets with a violent death, as do all other members of the group. Soon afterwards another group of courageous youth is being formed. Some of them will perhaps not live to enjoy their youth.

==Cast==
*Dimitar Buynozov as  Dimo
*Rumyana Karabelova as  Veska
*Lyudmila Cheshmedzhieva as  Tzveta
*Georgi Georgiev-Getz as  Mladen
*Emilia Radeva as  Nadya
*Anani Yavashev as  Slavcho
*Georgi Naumov
*Ivan Trifonov
*Dimitar Panov
*Ivan Bratanov
*Dora Stoyanova

==Release and acclaim==
The film premiered on 13 March 1961 in Bulgaria. The film premiered in Russia in July 1962 at the 2nd Moscow International Film Festival and won the Golden Prize (1959–1967) for director Binka Zhelyazkova.    Zhelyazkova was also nominated as director of best picture at the Grand Prix Film Festival in 1961 in film|1961.

==Box office success==
A reported 2,303,354 admissions were recorded for the film in cinemas throughout Bulgaria.

==See also==
*List of Bulgarian films

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 
 