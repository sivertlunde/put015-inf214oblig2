Judgment Deferred
{{Infobox film
| name           = Judgment Deferred
| image          = "Judgment_Deferred".jpg
| alt            = 
| caption        = Australian daybill poster
| film name      =  John Baxter
| producer       = John Baxter 
| writer         = 
| screenplay     = Barbara K. Emary Walter Meade Geoffrey Orme
| story          = 
| based on       = 
| narrator       =  Hugh Sinclair  Helen Shingler Abraham Sofaer Joan Collins
| music          = Kennedy Russell Arthur Grant
| editing        =  Vi Burdon
| studio         =  Group 3
| distributor    = Associated British Film Distributors (UK)
| released       = February 1952 (UK)	
| runtime        = 88 min
| country        = Britain
| language       = English
| budget         = 
| gross          =  
}} John Baxter Hugh Sinclair, Helen Shingler and Abraham Sofaer.  The film is a remake of the directors earlier film, Doss House (1933), and features an early performance from Joan Collins. It was the first production from Group 3, a company formed to encourage new young British film-makers (which later produced The Brave Dont Cry, Conflict of Wings, The Angel Who Pawned Her Harp and several other low-budget features).      

==Plot==
With the assistance of a journalist a group of refugees and down and outs try and unmask the criminal who has framed one of their number as a drug dealer.

==Selected cast== Hugh Sinclair - David Kennedy 
* Helen Shingler - Kay Kennedy 
* Abraham Sofaer - Chancellor 
* Leslie Dwyer - Flowers 
* Joan Collins - Lil Carter 
* Harry Locke - Bert 
* Elwyn Brook-Jones - Coxon 
* Marcel Poncin - Stranger  Martin Benson - Pierre Desportes
* Bud Flannagan - Himself
* Bransby Williams - Dad
* Michael Martin Harvey - Martin
* Harry Welchman - Doc  
* Wilfred Walter - Judge
* Maire ONeill - Mrs. OHalloran film "

==Critical reception== amateur theatricals."  TV Guide found the film "captivating mainly because of the novelty of the story and the many strange characters that are introduced."   Sky Movies wrote "this one occasionally creaks under the strain of its longish running time but offers some striking tableaux, especially within the weird court held by a crowd of criminals, eccentrics and jobless that in some ways recalls the jury that proved the nemesis of Peter Lorre in Fritz Langs classic thriller M (1931 film)|M." 

==References==
 

==External links==
* 

 
 
 
 
 
 

 