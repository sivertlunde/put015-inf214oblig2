They Died with Their Boots On
{{Infobox film
| name           = They Died with Their Boots On
| image          = Diedbootson.jpg
| border         = yes
| alt            =
| caption        = Theatrical release poster
| director       = Raoul Walsh
| producer       = {{Plainlist|
* Hal B. Wallis
* Robert Fellows
}}
| screenplay     = {{Plainlist|
* Æneas MacKenzie
* Wally Kline
* Lenore J. Coffee
}}
| starring       = {{Plainlist|
* Errol Flynn
* Olivia de Havilland
}}
| music          = Max Steiner
| cinematography = Bert Glennon William Holmes Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =   1947 (France)
| runtime        = 140 minutes
| country        = United States
| language       = English
| budget         = $1,357,000 
| gross          = $2.55 million   2,151,959 admissions (France) 
}}
 western film directed by Raoul Walsh and starring Errol Flynn and Olivia de Havilland. Written by Æneas MacKenzie and Wally Kline, the film is a highly fictionalized account of the life of General George Armstrong Custer, from the time he enters West Point military academy, through the American Civil War, and finally to his death at Little Big Horn. The battle against Chief Crazy Horse is portrayed as a crooked deal between politicians and a corporation that wants the land Custer gave to the Indians. Despite its historical inaccuracies, the film was one of the top-grossing films of the year. They Died with Their Boots On was the eighth and final film collaboration between Errol Flynn and Olivia de Havilland.
 Sea Hawk, colorized in the early 1990s. This version was released on VHS tape in 1998.  Only the black-and-white version of this film has been released on DVD.

==Plot== West Point Military Academy, his wooing of Elizabeth "Libby" Bacon (Olivia de Havilland) who becomes his loving wife, and his participation in the American Civil War and the Battle of Little Big Horn.

Custer enters West Point and quickly establishes himself as a troublemaker, after showing up in an outfit he designed himself that made him appear as a visiting officer. After he is almost kicked out of West Point for the misunderstanding, he signs up as a cadet, and stacks up demerits for pranks, unruliness, and disregard of rules. When the Civil War breaks out his class at West Point is graduated early, including Custer who graduates at the bottom of the class, and is ordered to report to Washington, D.C.

Custers relationship with Libby Bacon begins at West Point, when he is walking a punishment tour around the campus. On punishment, he is not allowed to talk, but he is approached by Libby who is looking for directions. As soon as his punishment is over, he runs after her, and tells her he will meet her at her front porch that evening. Because of his orders to travel to Washington, Custer misses his meeting with her.

Once in Washington, Custer befriends General Winfield Scott (Sydney Greenstreet) who aids him in being placed with the 2nd Cavalry. He becomes a war hero after disregarding his superiors orders in a crucial battle and successfully defending a bridge for the infantry to cross. He is awarded a medal while recovering in hospital after a shot to the shoulder, then gets leave to go home to Monroe, Michigan. He meets Libby again but angers her father, who had been a butt of his joking at a bar earlier in the day.  Custer returns to his regiment.  Due to a miscommunication from the Department of War, he is promoted to the rank of Brigadier General. He takes command of the Michigan Brigade at the Battle of Gettysburg, wins the day, and many victories follow him all the way to Appomattox Court House National Historical Park|Appomattox.

Upon returning home to Monroe as a hero, Custer marries Libby and they set up a house together. However, Custer is bored with civilian life and has begun to drink. Libby visits Custers friend General Scott and asks him to assign Custer to a regiment again. He agrees, and Custer is given a Lt. Colonels commission in the Dakota Territory, where he will ultimately be involved in the Battle of Little Big Horn, later also called "Custers Last Stand".
 Arthur Kennedy), is running the bar in town, as well as the General Store which is providing firearms to the local Native Americans. Furious, Custer shuts down the bar and teaches his troops a song, "Garryowen (air)|Garryowen", which brings fame to the 7th Cavalry. They have many engagements with Dakota leader Crazy Horse (Anthony Quinn). Crazy Horse wants peace but wants a treaty to protect the Black Hills. Custer and Washington sign the treaty. The new treaty is almost bankrupting Sharps trading posts so they spread a rumor of discovery of rich gold deposits in the area, to get Euro-American settlers to stream into the Black Hills. Custer and his troops will permit no infraction of the treaty. However, Sharp gives the troops each a bottle of liquor right before they are supposed to report, and they embarrass Custer by riding past Commissioner Taipe (a politician in league with the Sharps) while drunk. Custer hits Taipe in anger and is relieved of his command.

On the train home, Custer hears from Libby about Sharps attempts to start a gold rush in the Black Hills, a plan that would bring lots of business to Sharps shipping line. Outraged, Custer takes the information to the U.S. Congress, but they ridicule him. When news arrives that the presence of gold miners has led to open conflict between Native Americans and U.S. troops, Custer appeals to President Ulysses S. Grant who restores to him command of the 7th.

On the day of "Custers Last Stand", Custer realizes that a group of infantry will march into a valley where thousands of Native Americans stand ready to fight them. Knowing the infantry wont have a chance, he says a tearful goodbye to Libby and leads his battalion into the battle to save the infantry. Arrows fly and horses trample across the valley, and all are killed, including Sharp, who had elected to ride with the regiment to, as Custer puts it, "Hell or glory. It depends on ones point of view", and who admits with his last breath that Custer may have been right about glory and money when he said that "at least you can take   with you." Custer himself is finally downed by a gunshot from Crazy Horse.

Custer is portrayed as a fun-loving, dashing figure who chooses honor and glory over money and corruption. Though his "Last Stand" is probably treated as more significant and dramatic than it may have actually been, Custer follows through on his promise to teach his men "to endure and die with their boots on." In the movies version of Custers story, a few corrupt white politicians goad the western tribes into war, threatening the survival of all white settlers in the West. Custer and his men give their lives at Little Bighorn to delay the Indians and prevent this slaughter. A letter left behind by Custer absolves the Indians of all responsibility.

==Cast==
* Errol Flynn as George Armstrong Custer
* Olivia de Havilland as Elizabeth Bacon Custer Arthur Kennedy as Ned Sharp
* Charley Grapewin as California Joe
* Gene Lockhart as Samuel Bacon
* Anthony Quinn as Crazy Horse
* George P. Huntley Jr as Lt Queens Own Butler
* Stanley Ridges as Maj. Romulus Taipe
* John Litel as Phillip Sheridan|Gen. Phillip Sheridan
* Walter Hampden as William Sharp
* Sydney Greenstreet as Winfield Scott|Lt. Gen. Winfield Scott
* Regis Toomey as Fitzhugh Lee
* Hattie McDaniel as Callie
* Minor Watson as Senator Smith
* Joseph Crehan as President Ulysses S. Grant
Louis Zamperini was an extra in this movie. He was the true life hero of "Unbroken"

==Production==

===Filming=== Santa Fe Trail, released the previous year, in which Flynn portrayed Jeb Stuart and Ronald Reagan played Custer, also featuring Olivia de Havilland as Flynns leading lady.

In September 1941, during filming, Flynn collapsed from exhaustion.   

Three men were killed during the filming. One fell from a horse and broke his neck. Another stuntman had a heart attack. The third, actor Jack Budlong, insisted on using a real saber to lead a cavalry charge under artillery fire. When an explosive charge sent him flying off his horse, he landed on his sword, impaling himself. 

===Custers Last Stand sequence===
Although the rest of the film was shot in various locales throughout southern California, the film makers had hoped to capture this climactic sequence near the location of the actual Battle of Little Bighorn.  Due to scheduling and budget constraints, however, the finale of the film was relegated to a rural area outside of Los Angeles.
 Filipino extras.

===Soundtrack=== Irish soldiers. English soldier, Silver River Rocky Mountain, The Searchers starring John Wayne.

==Reception==
They Died with Their Boots On grossed $2.55 million for Warner Bros. Pictures in 1941, making it the studios second biggest hit of the year.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 