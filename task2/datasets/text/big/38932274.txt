Sama (film)
{{Infobox film
| name           = Sama
| image          = Sama_poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Néjia Ben Mabrouk
| producer       = 
| writer         = 
| screenplay     = Néjia Ben Mabrouk
| story          = 
| narrator       = 
| starring       = Fatma Khwmiei, Mouna Noureddine, Basma Tajine
| music          = François Gaudard
| cinematography = Marc-André Batigne
| editing        = Moufida Tlatli
| studio         = SATPEC (Tunisia), No Money Company (Belgium), ZDF (Germany)
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Tunisia
| language       = Arabic
| budget         = 
| gross          = 
}}
 1988 Tunisian feature film directed by Néjia Ben Mabrouk.

==Plot==
The mother (Mouna Noureddine) of the young protagonist Sabra (Fatma Khemiri) gives her daughter an oval stone to protect her from men until her wedding day. The mother keeps the stone locked in a jewelry box, symbolizing Sabra being locked in society against her consent.   Living in the male-dominated Tunisian society, Sabra seeks an education in fear of being reduced to societys traditional roles. Flashbacks to her childhood reveal how her mother taught her to be wary of men. After fighting her way past societal barriers, Sabra makes it into university and studies for her exams by candlelight. However, when her professor gives her a failing grade, Sabra decides that she has to leave. Using her mothers support, Sabra continues her studies exiled in Europe. 

==Production==
Ben Mabrouk planned on making a documentary for her debut, but encountered some problems at Brussels. She found that many Tunisians did not want to talk openly about their problems.   From 1979 to 1980, Ben Mabrouk wrote the script for Sama.  Ben Mabrouk looked for foreign producers from Algeria, Belgium, and France, all of which rejected her script. Finally, she received production support from the German company ZDF and SATPEC.   Sama was filmed in 1982, but a dispute with the SATPEC delayed the films release for six years,  later described as "petty bureaucratic red-tape,"  Concerning the filming, one reviewer found that it was "shot grittily in locations which look authentic, skimping on lighting to the point where it is hard to distinguish between details on the screen." 

==Critical response==
Sama premiered at the Carthage Film Festival, causing a stir among audiences, particularly the female members who protested the films open portrayal of private practices.   One reviewer noted that the film broke "an incredible taboo" by Tunisian standards.   Ben Mabrouk won the Caligari Prize at the 1989 International Forum of New Cinema of the Berlin International Film Festival for directing Sama.  Today, the Arabic emancipatory cinema dauteur consider this film a classic.  

Marie-Pierre Macia praised the films presentation of its main protagonist, saying "The Trace is content to introduce us to a complex protagonist—it does not insist we identify with her although her story captivates and moves us."    A review from Variety noted some of Sama s shortcomings, saying that they could be attributed to "the name of the production company (No Money) and the time it took to shoot his film." The reviewer called it "a typical portrait of a womans condition in a male-dominated society," and "first and foremost a militant tract." 

==Themes==
The same reviewer from Variety thought that the film is "bound to attract the attention of ethnographers and socio-political activists."  Hayet Gribaa examined the contrast between the knowledge seeking Sabra and her illiterate mother and their similarities as they try to respect each other.  Simone Mahrenholz praised the flashbacks to Sabras childhood, saying they "show us in painful clarity the conditioning of girls."   The imagery of Sama is divided between the hostile school and streets, and the protective interior of the home; both respectively defined as the male and female environments. 

==References==
{{Reflist|refs=
   

   

   

   

   
}}

==External links==
* 

 
 
 
 