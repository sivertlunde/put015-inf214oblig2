Polish Blood
{{Infobox film
| name = Polish Blood
| image =
| image_size =
| caption =
| director = Carl Lamac
| producer =  Robert Leistenschneider   Carl Lamac   Anny Ondra Leo Stein
| narrator = Hans Moser   Iván Petrovich
| music = Jára Benes   
| cinematography = Otto Heller   Otto Martini  
| editing =    Ella Ensink    
| studio = Elektra Filmfabriken   Ondra-Lamac-Film 
| distributor = Lux Film (Austria)
| released =24 October 1934 
| runtime = 
| country = Austria   Czechoslovakia   Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Hans Moser Leo Stein. The films sets were designed by art directors Bohumil Hes and Stepán Kopecký. A separate Czech language version was also released.

==Cast==
*   Anny Ondra as Helena Zaremba   Hans Moser as Jan Zaremba  
* Iván Petrovich as Graf Bolko Baransky  
* Margarete Kupfer as Jadwiga Kwasinskaja 
* Hilde Hildebrand as Wanda Kwasinskaja  
* Rudolf Carl as Bronio von Popiel  
* Paul Rehkopf as Dymscha, Gutsverwalter  
* Karl Platen as Constanty 
* Helmut Heyne   
* Franz Marner   
* Alfred Frey  

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009.

== External links ==
*  

 
 
 
 
 
 
 
 
 

 