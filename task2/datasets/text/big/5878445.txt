X2000
 
 
{{Infobox film
| name           =  
| image          = X2000 poter.jpg
| image size     =
| border         =
| alt            =
| caption        = Films poster
| director       = François Ozon
| producer       = Olivier Delbosc Marc Missonnier
| writer         = François Ozon
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       = Denise Aron-Schropfer Bruno Slagmulder Lucia Sanchez Flavien Coupeau Lionel Le Guevellou Olivier Le Guevellou
| music          =
| cinematography = Pierre Stoeber
| editing        = Dominique Petrot
| studio         = Fidélité Productions
| distributor    =
| released       =  
| runtime        = 8 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}} short film directed by François Ozon.

==Plot==
A naked man (Bruno Slagmulder) wakes up in a luxury loft, which is a residential building in an unidentified European city, after a particularly wild New Years Eve party of the year 2000. He finds a naked woman (Denise Aron-Schropfer) in his bed, and obviously he does not recognize or remember her. He walked naked through the apartment and discovers a pair of partygoers – two young boys, identical twins (Lionel Le Guevellou and Olivier Le Guevellou), in a sleeping bag, hugging each other. He looks out the window and recognizes a man (Flavien Coupeau) and woman (Lucia Sanchez) making love in the apartment across the street, while the woman who was sleeping beside him wakes up, and takes a bath. While he looks at what is happening across the street, he falls off the table he was sitting on, and lands on the floor, breaking a glass. This noise wakes up the twins. He goes into the kitchen to throw the pieces of broken glass away, and discovers ants underneath the garbage can. He goes into the bathroom and tells the women in the bath about the plague of ants in the kitchen.

==Cast==
* Denise Aron-Schropfer as the woman
* Bruno Slagmulder as the man 
* Lucia Sanchez as a lover 
* Flavien Coupeau as a lover 
* Lionel Le Guevellou as a twin 
* Olivier Le Guevellou as a twin

== Reception ==
In the year 1999, the film won two awards at International Short Film Festival Oberhausen:
* Interfilm Award (awarded to François Ozon)
* Jury Prize for Best Short Film (awarded to François Ozon)

==See also==
* A Summer Dress

== External links ==
*  
*  

 

 
 
 
 
 


 
 