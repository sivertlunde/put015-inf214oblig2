Pallikoodam (film)
 
{{Infobox film
| name = Pallikoodam
| image = pallikodam_movie_poster.jpg
| director = Thangar Bachchan
| producer =  Viswas Sundar
| official site =  Narain Sneha Sneha Sriya Seeman Thangar Bachchan
| cinematography = Thangar Bachan
| music = Bharadwaj
| released = 10 August 2007
| camera = Thangar Bachan
| country = India Tamil
}}
 Tamil feature Seeman and the director himself form the cast. Narain plays Vetrivel, a collector, while Sriya Reddy is a nurse, Sneha is a teacher named Kokila and Seeman plays Muthu. The film deals with four different periods - 1978, 1983, 1991 and 2004.

==Plot==
Vetrivel, a son of a potter, is the most intelligent of the lot and loves Kokila who is the daughter of a rich Jamindar who owns the school land. And their common friends are Muthu and Kumaraswami. Kumaraswami, very bad at studies, avoids school; however, his father makes all efforts to send him back to school and succeeds in doing so.

A young and beautiful Doctor, Jhansi (Shreya) arrives at the village and befriends the foursome. Jhansi slowly becomes the godfather figure to all the four. And when Muthu loses his father and becomes an orphan, Jhansi takes him into her custody. Vetrivel and Kokilas love is made public and the villagers blame Jhansi. They throw her out of the village and Muthu accompanies her.  
 
What follows is the disentanglement of the plot on the plight of Jhansi, Muthu, Kumaraswami, Vettivel, and Kokila. Eventually the rest of the story and the climax also unravel whether the school escapes the wrath of the landowner.

==Cast== Narain as Vetrivel Sneha as Kokila
* Shreya Gupta as child Kokila
* Sriya Reddy as Jhansi Seeman as Muthu
* Thangar Bachchan as Kumarasamy
* Meenal

==Box office==
*The film grossed $1 million at the box office.
*The movie is based on the novel Kalavupona Pozhudhugal (Stolen Moments) by Thangar Bachchan himself.
* Producer Viswas Sundar has sent a two-hour international copy of the film with English and French sub-titles to Canada.

==External links==
*  
 
 

 
 
 
 
 


 