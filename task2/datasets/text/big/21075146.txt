When Brendan Met Trudy
{{Infobox film
| name           = When Brendan Met Trudy 
| image          = When Brendan Met Trudy.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Kieron J. Walsh
| producer       = Lynda Myles
| writer         = Roddy Doyle
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Peter McDonald Flora Montgomery
| music          = Richard Hartley
| cinematography = Ashley Rowe
| editing        = Scott Thomas
| studio         = Deadly Films 2 
| distributor    = 
| released       = 2001 in film  
| runtime        = 95 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
}}

When Brendan Met Trudy is a 2001 film directed by first time director Kieron J. Walsh and written by Roddy Doyle. The story is about a Dublin schoolteacher who falls in love with a mysterious young woman.

==Plot== Peter McDonald) is a shy, reserved teacher who takes his profession seriously.  Away from the classroom, he has a love of films and classical music. One night, after practising with his church choir, he meets Trudy (Flora Montgomery), a bright, witty and free-spirited woman whom he believes is a Montessori teacher. Despite the differences in their personalities, the two begin a relationship. Brendan is unaware that his new girlfriend is actually a burglar, and is shocked when Trudy asks him to prove his love by helping her on one of her jobs. Brendan is torn between his feelings for Trudy, and the desire to do what is right. Throughout his relationship with her, Brendan slowly begins to discover himself, and realises that there is more to life than music and movies.

==Cast== Peter McDonald as Brendan  
* Flora Montgomery as Trudy
* Maynard Eziashi as Edgar
* Marie Mullen as Mother 
* Pauline McLynn as Nuala 
* Don Wycherley as Niall 
* Eileen Walsh as Siobhán
* Barry Cassin as Headmaster  Robert ONeill as Dylan

== Production ==
The film received funding from The Irish Film Board. 

== Reception ==
The film received mixed reviews. Review aggregation website Rotten Tomatoes gives the film a score of 61% based on reviews from 62 critics. The sites  consensus was "The references to other films are rather overdone, and the direction is uneven". {{cite web 
| title = When Brendan Met Trudy (Stolen Nights) (2000)
| url = http://www.rottentomatoes.com/m/when_brendan_met_trudy/ 
| work = Rotten Tomatoes 
| publisher = Flixster 
}} 
Metacritic gives the film a score of 53% based on reviews from 25 critics.
 {{cite web 
| title =  When Brendan Met Trudy  
| url = http://www.metacritic.com/movie/when-brendan-met-trudy 
| work = Metacritic
| publisher = CBS Interactive
}} 

Reviewing for the BBC, George Perry gives the film 3/5 stars. {{cite web 
| date = 14 May 2001
| author = George Perry
| title = When Brendan Met Trudy (2001)
| url = http://www.bbc.co.uk/films/2001/04/27/when_brendan_met_trudy_2001_review.shtml 
| work = BBC 
}} 

Peter Bradshaw describes the film as "ordinary piece of work from Roddy Doyle" a standard rom-com, witheringly calling it "dull enough to qualify as an honorary British film". 
 {{cite web 
| date = 25 May 2001
| author = Peter Bradshaw
| title = When Brendan Met Trudy	
| work = The Guardian
| url = http://www.guardian.co.uk/film/2001/may/25/culture.reviews1 
}} 

Roger Ebert gave the film 3/4 stars. Ebert says you will likely enjoy the film more if you get the film references but whether you do the film still works. {{cite web 
| date =  March 9, 2001 
| author = Roger Ebert
| title = When Brendan Met Trudy 
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20010309/REVIEWS/103090305 
| work = Chicago Sun Times
}} 

== References ==
 

== External links ==
*  
*   original trailer

 

 
 
 
 
 
 


 
 