Life Begins Anew
{{Infobox Film
| name           = Life Begins Anew
| image          = 
| image size     = 
| caption        = 
| director       = Mario Mattoli
| producer       = 
| writer         = Aldo De Benedetti Mario Mattoli
| narrator       = 
| starring       = Alida Valli   Fosco Giachetti   Eduardo De Filippo   Carlo Romano
| music          = Ezio Carabella
| cinematography = Ubaldo Arata
| editing        = Fernando Tropea
| studio         = Excelsa Film 
| distributor    = Minerva Film
| released       = 21 September 1945
| runtime        = 84 minutes
| country        = Italy 
| language       = Italian
| budget         = 
| preceded by    = 
| followed by    = 
}}
Life Begins Anew ( ) is a 1945 Italian drama film directed by Mario Mattoli and starring Alida Valli, Fosco Giachetti and Eduardo De Filippo. It was the second most popular Italian film during 1945-46 after Roberto Rossellinis Paisan.   

==Production== on location neorealist style, which he largely rejected.  Interior scenes were shot at the Palatino Studios in Rome. The film was important in Giachettis transition from one of the leading stars of the Fascist era into a figure acceptable to post-war audiences. This was partly achieved through retaining his previously strongly masculine persona which was adapted to the new conditions. Alida Valli, also a celebrated Fascist period star, received a boost from the film and went to Hollywood the following year.

==Synopsis==
An Italian serviceman returns home to Rome after spending some time in a British Prisoner of War camp during the Second World War. He finds that his wife, during the desperate war years, has become a prostitute to pay for medicine to keep their son alive. He is at first outraged and pushes her away, but after being given wise advice by a friendly neighbour, he decides to reconcile with her. 

==Cast==
* Alida Valli - Patrizia Martini
* Fosco Giachetti - Paolo Martini
* Eduardo De Filippo - Il professore
* Carlo Romano - Croci
* Aldo Silvani - Il giudice istruttore
* Nando Bruno - Scorcelletti, il camionista
* Anna Haardt - La baroness Magda Huberth
* Maria Donati - Maria
* Ughetto Bertucci - Righetto
* Maurizio Ceselli - Sandrino Martini, figlio di Patrizia e Paolo

==References==
 

== Bibliography ==
* Bayman, Louis (ed.) Directory of World Cinema: Italy. Intellect Books, 2011. 
* Gundle, Stephen. Mussolinis Dream Factory: Film Stardom in Fascist Italy. Berghahn Books, 2013.
* Hipkins, Danielle & Plain, Gill (eds.) War-torn Tales: Literature, Film and Gender in the Aftermath of World War II. Peter Lang, 2007.

==External links==
* 

 

 
 
 
 
 
 
 
 
 