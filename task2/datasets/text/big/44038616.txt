Chuvanna Sandhyakal
{{Infobox film
| name = Chuvanna Sandhyakal
| image =
| caption =
| director = KS Sethumadhavan
| producer = MO Joseph
| writer = Balu Mahendra Thoppil Bhasi (dialogues)
| screenplay = Thoppil Bhasi Lakshmi Mohan Sharma Sam
| music = G. Devarajan
| cinematography = Balu Mahendra
| editing =
| studio = Manjilas
| distributor = Manjilas
| released =  
| country = India  Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan and produced by MO Joseph. The film stars Adoor Bhasi, Lakshmi (actress)|Lakshmi, Mohan Sharma and Sam in lead roles. The film had musical score by G. Devarajan.   
 
==Cast==
  
*Adoor Bhasi  Lakshmi 
*Mohan Sharma 
*Sam
*Sankaradi 
*Bahadoor 
*MG Soman 
*Manibala Meena 
*Paravoor Bharathan  Sujatha 
*Vidhubala 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Achyuthaananda || P. Leela || Vayalar || 
|- 
| 2 || Ithihaasangal Janikkum || Sreekanth || Vayalar || 
|- 
| 3 || Kaalindi Kaalindi || K. J. Yesudas || Vayalar || 
|- 
| 4 || Nightingale || P Jayachandran || Vayalar || 
|- 
| 5 || Poovukalkku Punyakaalam || P Susheela || Vayalar || 
|- 
| 6 || Vritham Kondu Melinjoru || P. Madhuri || Vayalar || 
|}

==References==
 

==External links==
*  

 
 
 


 