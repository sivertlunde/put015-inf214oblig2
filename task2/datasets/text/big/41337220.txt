Rigor Mortis (film)
 
 
{{Infobox film name = Rigor Mortis  image = Rigor Mortis poster.jpg alt =  caption =  traditional = 殭屍 simplified = 僵尸 pinyin = Jiāng shī  jyutping = Goeng1 Si1}} director =  Juno Mak producer =     Bernard Lai writer = Philip Yung   Jill Leung   Juno Mak starring = Anthony Chan  Kara Hui  Lo Hoi-pang  Paw Hee-ching music = Nath Connelly cinematography =  editing = David Richardson studio = Great Sound Creation for Kudos Film distributor = Fortissimo Films released =   runtime = 101 minutes country = Hong Kong language = Cantonese budget = HK$15,000,000 gross = HK$16,781,408 
}}
 Anthony Chan, Billy Lau and Richard Ng.   Additionally, Chung Fat, who starred in Encounters of the Spooky Kind, is also featured.   

== Plot ==
Formerly successful actor Chin Siu-ho, star of Mr. Vampire, becomes depressed and suicidal when his wife leaves him.  He moves into a dilapidated apartment building, where he meets Uncle Yin, the buildings long-time, friendly supervisor.  Yin performs a welcoming ceremony, and Chin insists that Yin take a gratuity saying that, "Itll become necessary,".  After listening to a recorded message left by his young son, Chin attempts to hang himself.  His action draws the attention of twin ghosts who haunt the apartment, and they possess his body.  Lau, a retired vampire hunter, bursts into the room, cuts down the noose, and exorcises the spirits from Chins body.  Alerted by the commotion, his neighbors gather at the doorway before Lau disperses them.

Later, Chin visits Laus restaurant, where Lau explains that he has taken up cooking glutinous rice, an ingredient used in fighting jiangshi.  Chin thanks Lau for saving his life, but Lau tells him to save his gratitude for a time when he can save Chins life without the threat of suicide.  Chin then meets several of his neighbors, including Yang Feng, a traumatized woman with mental instability and her albino son, Pak. The audience is also introduced to seamstress Auntie Meiyi and her husband Uncle Tung.  Later that night, in a dream, Tung tells Meiyi that he must leave her for several days and requests that she wait for him.  In reality, Tung has fallen down the stairs and died.  Black magic practitioner Gau discovers the body and preserves it for Meiyi, who requests that he resurrect her husband. They begin the process of preserving and gradually resurrecting Tungs corpse while keeping it concealed in her bathroom.

Chin later discovers that Yin has been providing shelter for Yang and Pak, who are connected to the ghostly twins.  Yin explains with the aid of a flashback, that in life, the twins were students being tutored by Yangs husband in apartment 2442. There, he assaulted and raped one of the sisters and upon discovering the brutal rape, the other twin stabs Yangs husband to death. After a scuffle, the final image depicts the dead tutor surrounded by the corpses of the twins; one dead on the floor next to him, the other hung from the ceiling fan. After hearing this, Chin attempts to seek help from Lau in permanently ridding the apartment of the twins, but Lau refuses and claims to simply be a cook now. Before leaving Laus apartment, Chin remarks that if he truly was just a cook, he wouldnt have kept all the exorcism memorabilia from his Father, notably the large, round talisman mounted on the wall.  Chin then turns to Gau, and the two summon the twins who proceed to possess Chin once again.  Hearing the sounds of Gau and Chin fighting, Lau investigates and joins the fray.  Together, Gau and Lau are able to exorcise the ghosts and bind them into a sturdy cabinet.  Lau rebukes Gau for his use of Chin as bait and advises him to burn the cabinet.  Gau promises to do so, and they leave Chin to recover.

Unsatisfied with her husbands lack of progress, Meiyi seeks further help from Gau, who counsels her to continue waiting.  Gau eventually relents and suggests an alternative to the crows blood she has been feeding Tung every night: the blood of a virgin. While they are discussing their options, Yin arrives to investigate the disappearance of Tung.  Afraid that Yin suspects the truth, Meiyi murders him by bashing his head in.  Later, Lau requests that she watch over Pak for a bit.  When Pak requests to use the bathroom, she hesitates only briefly before seizing the opportunity and locks him in the room with Tung. While the child is horrifically murdered, Meiji stands on the other side of the door, tears streaming down her face.  In a cut scene, the talisman in Laus room activates, signaling the presence of a powerful evil. Now risen as a jiangshi, a disfigured Tung haunts the apartment complex. His first victim, a mortally wounded Gau reveals to Lau that he thought he needed the ghosts to resurrect Tung.

Distraught, Yang searches the apartment building for her missing son.  When she finds the rattling cabinet, she assumes he is inside and hastily she opens it, unintentionally and unknowingly releasing the twins ghosts.  After searching a bit more she encounters Pak, now a specter. Filled with rage and sorrow, she slits her left wrist to lure out the jiangshi. She attempts to destroy him with a makeshift weapon, but ultimately, Tung kills her. Moments later, Chin shows up with a Molotov cocktail and sets Tung aflame, seemingly defeating him.  However, the twins arrive and quickly possess Tungs corpse.  Now more powerful, he easily impales Chin and leaves him for dead.  Chin manages to free himself despite being heavily injured and finds Lau. He begs that Lau give him enough strength for one last fight.  Initially reluctant, as it might risk Chins life, Lau relents and sets a trap for Tung.  With his vampire hunting tools, Lau weakens Tung enough for Chin to finally destroy him.  As Tung slowly crumbles to ash, Meiyi joins him by slitting her throat.  Chin and Lau, wounded by the battle, both collapse.

In the final sequence, it is shown what would have happened had there been no supernatural elements. Chin is again depicted moving into the apartment complex, but it is less run-down, and the residents are less eccentric: Yin does not interact with him, Yang and Pak are depicted as happy and friendly neighbors, and Meiyi is a widow who is shown admiring a picture of Tung.  Chin successfully commits suicide, and an inept Lau discovers him too late to save him.  At the morgue, Chins son who is actually an adult and not a child as previously depicted, identifies the body to the medical examiner, Dr. Gau.

==Cast==
* Chin Siu-ho as Chin Siu-ho Anthony Chan as Yau
* Kara Hui as Yang Feng
* Chung Fat as warlock Gau
* Lo Hoi-pang as janitor Uncle Yin, or Grandpa
* Richard Ng as Uncle Tung
* Paw Hee-ching as Auntie Meiyi
* Morris Ho as Pak
* Billy Lau as the cook

==Release==
Rigor Mortis premiered at the Venice Film Festival. 

==Reception==
Rotten Tomatoes, a review aggregator, reports that 58% of twelve surveyed critics gave the film a positive review; the average rating was 6.1/10.   Metacritic rated it 53/100 based on eight reviews.   Clarence Tsui of The Hollywood Reporter called it "a lavish, heavy-handed retreading and reinvention of Hong Kong and Japanese horror-film tropes, saved from clinical inhumanity by its veteran cast."   Justin Chang of Variety (magazine)|Variety described it as a "flashy, incoherent and virtually scare-free Hong Kong horror exercise".   Daniel M. Gold of The New York Times called it a "relentlessly creepy film" that uses less comedy than Mr. Vampire.   Martin Tsai of the Los Angeles Times wrote, "The film supplies a succession of hyper-stylized and potent set pieces without ever establishing any sort of internal logic." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 