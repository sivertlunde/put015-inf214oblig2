Bunny and the Bull
{{Infobox film
| name           = Bunny and the Bull
| image          = Bunny-bull-poster.jpg
| caption        = Theatrical release poster Paul King
| producer       = Mary Burke Mark Herbert Robin Gutch
| writer         = Paul King
| narrator       = 
| starring       = Edward Hogg Simon Farnaby with Verónica Echegui Noel Fielding Julian Barratt Richard Ayoade Love
| cinematography = John Sorapure
| editing        = Mark Everson Warp X Wild Bunch Film4 Regional Screen Yorkshire UK Film Council
| distributor    = Optimum Releasing
| released       =  
| runtime        = 101 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = $81,010
}} Paul King. The Mighty Boosh and Garth Marenghis Darkplace; the film is made in a similar style and has guest appearances from stars of those series.

==Plot==
Stephen is an agoraphobic recluse who has not left his flat in nearly a year. Each of his days are structured around carefully planned routines, but one day his routine is disrupted when he discovers an infestation of mice in the kitchen cupboards. He contemplates leaving the flat to buy mouse traps, but can not bring himself to open the door, so he reminisces about the events leading up to his current situation, with objects around the flat triggering flash backs.

One year earlier, after being "friend zoned" by the woman he loved, Stephen went on a sightseeing holiday across Europe with his friend Bunny, who is addicted to gambling. They visit several bizarre museums, but Bunny finds them all boring and is more concerned with seducing women. At a seafood restaurant in Poland, Stephen and Bunny meet Eloisa, a Spanish waitress who has recently left her boyfriend and plans to return to Spain for an upcoming fiesta. Bunny wins the restaurants delivery car in a bet, and Eloisa accompanies them on their journey towards Spain. They stop at a hostel in Switzerland, where Stephen plans to ask Eloisa on a date, only to discover that she has already started a sexual relationship with Bunny.

When they arrive in Spain, Eloisa is reunited with her brother Javier, an aspiring matador. Bunny decides he wants to fight a bull, so asks Javier to teach him everything he knows about bull-fighting. Javier shows him a prized matador suit he keeps in a display cabinet, which Bunny later steals and subsequently loses in a bet. Elsewhere, Eloisa falls in love with Stephen and they sleep together, but the next morning Bunny explains about the lost matador suit and urges Stephen to come with him as he flees Spain. Eloisa finds out about the suit and breaks up with Stephen. On the train ride home, Stephen angrily confronts Bunny about his recklessness, but he misinterprets Stephens frustrations as a challenge, so that night Bunny sneaks into a field and attempts to fight a bull. The bull kills him, and Stephen is left traumatised. When he returns home, Stephens guilt develops into agoraphobia, and he has remained in his flat ever since.

In the present day, Stephen imagines a conversation with the ghost of Bunny, who asserts that his death was not Stephens fault and urges him to talk to Eloisa. This conversation gives Stephen the confidence he needs to overcome his anxiety, so he contacts Eloisa and gains enough courage to leave the flat.

==Cast==
*Edward Hogg as Stephen Turnbull
*Simon Farnaby as Bunny
*Verónica Echegui as Eloisa
*Noel Fielding as Javier
*Richard Ayoade as Museum Curator
*Julian Barratt as Atilla
*Rich Fulcher as Captain Crab (voice)

==Production== The Mighty Boosh and Garth Marenghis Darkplace respectively on television, uses a similar surreal, nonsensical, and occasional dark style in the film. Like many projects made by ex-contributors of Garth Marenghi and Boosh, the film features several other ex-contributors such as Noel Fielding, Julian Barratt, Richard Ayoade, and Simon Farnaby, along with animation sequences by Nigel Coan. King admits "I’ve basically cast it off my speed-dial." 

==Reception==
Bunny and the Bull received mostly positive reviews.
Empire (magazine)|Empire rated the film at four stars (out of a maximum five), describing it as "charmingly crafted and willfully daft". The film was compared to Michel Gondrys 2004 film Eternal Sunshine of the Spotless Mind in theme and appearance, with the exception that Bunny and the Bull "manages to tramp down its whimsy with a rich vein of very silly, very British comedy".  Charles Watson of underground film magazine Slant said it was "a daft tale of wit and woe, with recognisable actors that go straight into their comfort zones making it as crafty and clever as possible." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 