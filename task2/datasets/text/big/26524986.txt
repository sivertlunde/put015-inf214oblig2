Love in Another Language
{{Infobox film
| name           = Love in Another Language 
| image          = LoveAnotherLanguageFilmPoster.jpg
| alt            = 
| caption        = Theatrical poster
| director       = İlksen Başarır
| producer       = Murat Şenöy   Burhan Bengi
| writer         = İlksen Başarır   Mert Fırat
| starring       = Saadet Aksoy   Lale Mansur   Mert Fırat
| music          = Uğur Akyürek
| cinematography = Hayk Karaosman
| editing        = Arzu Volkan
| studio         = Tonaj Productions
| distributor    = Medyavizyon
| released       =  
| runtime        = 
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = $771,785
}}
Love in Another Language ( ) is a 2009 Turkish drama film directed by İlksen Başarır, starring Mert Fırat as a young deaf man who falls in love with a call-center worker. The film, which went on nationwide general release on  , won awards at film festivals in Antalya, Bursa and Ankara as well as the 3rd Yeşilçam Awards.

==Production==
The film was shot on location in Istanbul, Turkey.   

== Plot ==
Onur, who has been deaf since birth, works as a librarian. His father had left him and his mother when he was seven, and Onur has always blamed himself for this. Although being able to speak, he has chosen to stay silent because of the pitying looks of the people around him. At his friend Vedat’s engagement party, he meets Zeynep, who later finds out about Onur’s hearing disability, but is not bothered by it. She is forced by her overbearing father to leave home and gets a job at a call-center. Having to speak on the phone all day to people she doesn’t know, Zeynep finds peace with Onur, who she communicates with perfectly without speaking...

== Release ==

=== General release ===
The film opened in 38 screens across Turkey on   at number nine in the Turkish box office chart with an opening weekend gross of $103,877.   

=== Festival screenings ===
* 46th Antalya Golden Orange Film Festival   
* 4th Bursa International Silk Road Film Festival      
* 21st Ankara International Film Festival   

==Reception==

===Box Office===
The film twice reached number nine in the Turkish box office chart and has made a total gross of $771,785. 

===Awards===
* 46th Antalya Golden Orange Film Festival   
** City Council Jurys Special Award: İlksen Başarır (Won)
* 4th Bursa International Silk Road Film Festival   
** Best Actress Award: Saadet Aksoy (Won)
** Turkish Film Critics Association (SİYAD) jury prize
* 21st Ankara International Film Festival (11–21 March 2010)      
** Best Leading Actor Award: Mert Fırat (Won)
** Best Leading Actress Award: Saadet Aksoy (Won)
* 3rd Yeşilçam Awards (March 23, 2010)   
** Best Actor: Mert Fırat (Won)

== See also ==
* 2009 in film
* Turkish films of 2009

==References==
 

==External links==
*   for the film
*   discussion with the director
*  

 
 
 
 