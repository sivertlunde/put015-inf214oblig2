Zombie King and the Legion of Doom
{{Infobox film
| name           = Zombie King and the Legion of Doom
| image          = Zombie King and the Legion of Doom.jpg
| alt            =
| caption        =
| director       = Stacey Case
| producer       = {{plainlist|
* Steve Salomos
* Bill Marks
}}
| writer         = {{plainlist|
* Bill Marks
* Sean K. Robb
}}
| starring       = {{plainlist|
* Jules Delmore
* Raymond Carle
* Jennifer Thom
* Rob Etcheverria
* Jim Neidhart
}}
| music          = Stephen Skratt
| cinematography = Adam Swica
| editing        = {{plainlist|
* Jason Winn Bareford
* Sandy Pereira
}}
| studio         =  El Zorrero
| distributor    =
| released       =   }}
| runtime        =
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
Zombie King and the Legion of Doom, also known as Enter... Zombie King and Zombie Pool Party, is a 2003 Canadian comedy horror lucha film directed by Stacey Case, written by Bill Marks and Sean K. Robb, and starring Jules Delmore, Raymond Carle, Jennifer Thom, and Rob "El Fuego" Etcheverria as masked Mexican wrestlers (luchadores) who fight zombies.

== Plot ==
Luchadores Ulysses and Blue Saint discover that Tiki is wrestling zombies.  Their worries are put to rest when Tiki reveals that the zombies have been domesticated.  However, Tikis zombies are soon blamed for a wave of violent zombie attacks.  The luchadores research the attacks and are led to the lair of the Zombie King, who seeks to conquer the world.

== Cast ==
* Jules Delmore as Ulysses
* Jennifer Thom as Mercedes
* Raymond Carle as Blue Saint
* Rob "El Fuego" Etcheverria as Tiki
* Sean K. Robb as Mister X Sinn Bodhi as Zombie King
* Jason Winn Bareford as Murdelizer
* Jim Neidhart as Sheriff Logan

== Production ==
Shooting took place in Toronto over a three week period in December 2002.  Professional wrestlers were hired to train the actors, but they did not have enough time.  Instead, the wrestlers were brought in as actors and stuntmen.  The film was then rewritten to show off the abilities of the wrestlers.  The Zombie King was originally slated to be played by George A. Romero, but Romero had to bow out due to health concerns.  Wrestler Sinn Bodhi replaced Romero.  Rob "El Fuego" Etcheverria, who played Tiki, also worked as the stunt coordinator.  In order to properly emulate lucha films, the crew intentionally added continuity errors and poor dubbing.  The film was not meant as a parody, though. 

== Release ==
On November 21, 2003, the film premiered in Toronto, Ontario. 

== Reception ==
Meredith Renwick of Canoe.ca called the film "a high-camp, low-budget melange of masked wrestlers, hokey dialogue, spurting blood and gratuitous nudity" that is more fun than bigger-budgeted high concept films.   Mike Adair of Exclaim! wrote that the fight scenes are surprisingly simple, but the action scenes are energetic enough to make up for it.  Adair also stated that the film will appeal to any fan of B movies.   Jon Condit of Dread Central rated the film 2.5/5 stars and called the film "goofy fun" that is recommended only for fans of lucha films.   David Johnson of DVD Verdict called the film "a fun, gory, amusing zombie romp".   Writing in The Zombie Movie Encyclopedia, Volume 2, academic Peter Dendle said that the film is not a cinematic masterpiece, but it is still amusing. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 