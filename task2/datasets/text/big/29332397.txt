The Don (1995 film)
{{multiple issues|
 
 
}}

{{Infobox film
 | name = The Don
 | image = The Don film.jpg
 | caption = DVD Cover
 | director = Farogh Siddique
 | producer = Salim
 | writer = Anand S. Vardhan
 | dialogue = 
 | starring = Mithun Chakraborty Sonali Bendre Jugal Hansraj Aashif Sheikh Prem chopra
 | music = Dilip Sen-Sameer Sen
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = April 28, 1995
 | runtime = 135 min.
 | language = Hindi Rs 4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1995 Bollywood|Hindi-language Indian feature directed by Farogh Siddique for producer Salim, starring Mithun Chakraborty, Sonali Bendre, Jugal Hansraj and Aashif Sheikh.

==Plot==

The Don is the story of a comman man, later become a Don, who always stands for justice and poor against all odds and evil minds. His sister is yet to forgive him as he only murdered her fiancee.

==Cast==

*Mithun Chakraborty
*Sonali Bendre 
*Jugal Hansraj 
*Aashif Sheikh
*Prem Chopra
*Kader Khan
*Sadashiv Amrapurkar   
*Raza Murad 	 ... 	
*Rana Jung Bahadur	
*Goga Kapoor 
*Mushtaq Khan 
*Kunika 
*Guddi Maruti

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1 
| "Dekha Jo Tumhe Yeh Dil" 
| Kumar Sanu
|-
| 2
| "Dil Ko Jo Maanu To"
| Mohammed Aziz, Sadhana Sargam
|-
| 3
| "Pum Pum Pum" Abhijeet
|-
| 4
| "Rajaai Maa To Garmi Lage"
| Udit Narayan, Kavita Krishnamurthy
|-
| 5
| "Teri Chaahat Mein Dil Yeh Deewana Hua"
| Kumar Sanu, Sadhana Sargam
|-
| 6
| "The Don"
| Mohammed Aziz
|}

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?id=The+Don

==External links==
*  

 
 
 
 
 
 


 