Turn Back the Clock (film)
{{Infobox Film
| name           = Turn Back the Clock
| image          = Turn Back The Clock film.jpg
| image_size     = 
| caption        = 
| director       = Edgar Selwyn
| producer       = Harry Rapf
| writer         = Edgar Selwyn Ben Hecht
| narrator       = 
| starring       = Lee Tracy Mae Clarke
| music          = Herbert Stothart    
| cinematography = Harold Rosson  Frank Sullivan 
| distributor    = Metro-Goldwyn-Mayer
| released       = August 25, 1933 (United States|U.S.)
| runtime        = 78 25"
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Turn Back the Clock (1933 in film|1933) is an MGM comedy-drama film directed by Edgar Selwyn, written by Edgar Selwyn and Ben Hecht, and starring Lee Tracy (while under contract to Metro-Goldwyn-Mayer). The film also stars Mae Clarke. The Three Stooges featuring Curly Howard appear in an uncredited straight role as wedding singers. There is a small gag wherein Lee Tracy asks the Stooges to sing Tonys Wife, a song the Stooges have not heard of; Moe Howard then asks "Tonys wife? Who is she?"

Although they are not credited as the Three Stooges (indeed, they receive no screen credit at all), this marks the first time the trio appeared as a group on film without their former leader, Ted Healy. They would launch their long-running film-shorts career a few months later.

==Plot Summary==
On March 23, 1933, Joe Gimlet (Lee Tracy) is a middle-aged cigar store owner, runs into his childhood friend, banker Ted Wright.  While having dinner, with Joe and his wife Mary, Ted requests that Joe and Mary invest $4,000 in savings in Teds company.  Joe is excited by the idea, but Mary refuses to part with their savings.
Angered by her reluctance, Joe gets drunk and declares to Mary that he should have married the wealthy Elvina.  After drunkenly leaving the apartment, he is hit by a car.
 
Joe wakes to discover that he is a young man again.  After scaring his mother with talk of the future, he decides to keep his past life to himself. 
Joe goes to his soda jerk job where me meets Elvina.  They soon become engaged.  The engagement announcement crushes his girlfriend Mary, and his mother who reminds him that money does not buy happiness.
   
After the wedding, Joe becomes rich due to his knowledge of the future.  Meanwhile Mary and Ted, Joe’s old friend, get married.

Remembering the post war problems, Joe pledges one million dollars to help returning vets. His wife is enraged.  President Woodrow Wilson hails Joe as a hero and offers him a job as the head of the War Industry Board.  Elvina openly mocks him, but they refuse to divorce to avoid scandal.

Years pass and on 1929, Joe goes into the cigar store and sees Ted working there.  At dinner with Ted and Mary Joe offers Ted the chance to invest $4,000 in a venture.  Mary approves the idea, because she believes in Joe.

The venture does not go forward because Joe is ruined by the stock market crash.  He divorces Elivinia.  His bank employees plunder the bank and Joe is to be held responsible.  
It is now March 6, 1933, the date of the car accident.  Joe now must live his life with no knowledge of the future.

Joe finds Mary and begs her to run away with him.  Mary tells him she cannot leave her husband.  Joe is pursued by a horde of police officers and brought into custody.
At that moment Joe wakes up in the hospital room with his life returned as it was.  He tells his wife he wouldn’t change a thing about their life together.

==Cultural references==
This film features a fictitious version of Henry Ford named Henry Cord asking Lee Tracy to accompany him in his $300 motor car.

==Cast==
* Lee Tracy as  Joe Gimlet 
* Mae Clarke as  Mary Gimlet / Mary Wright 
* Otto Kruger as  Ted Wright  George Barbier as  Pete Evans 
* Peggy Shannon as  Elvina Evans Wright / Elvina Evans Gimlet 
* C. Henry Gordon as  Dave Holmes 
* Clara Blandick as  Mrs. Gimlet, Joes Mother
* Three Stooges as Wedding Singers (uncredited)

==See also==
*The Three Stooges filmography
* List of films featuring time loops

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 