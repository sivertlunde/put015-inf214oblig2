On Our Own (film)
{{Infobox film
| name           = On Our Own
| image          =
| image_size     =
| caption        =
| director       = Lyman Dayton
| producer       = Sharon Baker
| writer         = Lyman Dayton Eric Hendershot
| narrator       =
| starring       = Leigh Lombardi Sam Hennings
| music          = Merrill Jenson Sam Cardon
| cinematography = T.C. Christensen
| editing        = 
| distributor    =
| released       = 1988
| runtime        =
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}On Our Own is a 1988 family film, from Feature Films for Families, about four children, Mitch, Kate, Travis and Lori. After being abandoned by their father and with the death of their mother they are forced to move into a childrens home. Feared they will have to be separated, they escape and run away.

==Story==
They escape to their old home where Mitch collects all the money he can find, (9 dollars), and his Uncle Jacks address, thinking Jack will know where their father is. They also manage to get their dog back. They then get into their mothers old car and drive away, with neighbors spotting them.

The police are on their trail. They stop at a gas station but with only $1.25 left, (after buying food earlier), they drive away without paying leaving a note saying that theyll pay later. Mitch later falls asleep at the wheel and gets the children stranded in the desert. Peggy Williams happens to drive past and notices them, she stops and asks if theyre OK. They lie in order to get a ride with her. She stops at a gas station to call the police after she suspects theyre lying, Mitch takes the wheel of Peggys car and drives away.

They successfully manage to escape but they badly damage Peggys car radiator. The four of them are hungry and stop at a store where Travis wins money on a slot machine with a quarter he picked up from the shop floor. While the children celebrate about the huge amount of money, two gangsters threaten them and insist the money is theirs, because the quarter was theirs. While they are fighting, Peggy (who managed to get a lift) saves them, by using a toy gun to bluff the gangsters into leaving.

The children tell her the truth, and as they drive on together, they form a close bond, especially Peggy and little Lori. They continue to drive the next day but Peggys car finally packs in, (because of the flummoxed radiator). They get a lift with a lorry driver who drops them off at their uncles home.

They discover Uncle Jack is not their biological uncle, but rather a friend of their fathers. He is an exceedingly wealthy pilot. He welcomes them in and introduces himself, his creepy girlfriend and her son Rhett. Rhett shows the children the animals but gets in a fight with Mitch. During the fight, Rhett grabs Mitch around the neck and tries shoving his face into a pile of horse manure and tells Mitch to "eat it!" Fortunately for Mitch, Kate jumps in and gets Rhett off Mitch before hes forced, head first, into the horse manure. Rhett gets a bloody nose from Mitch, then declares, "If I wasnt bleeding all over the place Id clean your plow. Ill be back." Travis, in defense of his brothers close call with the horse manure, grabs a chunk of "green mud" and slings it onto Rhetts back. Rhett subsequently goes and tattles on them to his mother, and as shes hugging Rhett, she gets manure on her hands. Peggy takes the children to a motel for the night. Afterward, Jack reveals to Peggy that the childrens father is in prison, and Mitch overhears them. The next day, they stop at a café, when suddenly the police arrive. While the police confront Peggy, Mitch, Kate, Lori and Travis escape on a bus, which Mitch drives. Peggy then gets in a taxi and follows them, as do the police.
 a plane suddenly appears and lands in front of the bus, forcing them to stop. The pilot is Jack, who asks if he can become the childrens guardian. After the hearing takes place, he is allowed to do so. 

The movie also centers around a conversation between Peggy and her mother about the incident. Peggy and Jack are getting married. Mitch has to work four Saturdays in the gas station and both he and Kate have to do 40 hours of community service. The last shot of the movie is of a wedding cake Peggys mother prepared while they were on the phone. This footage was clearly filmed after-the-fact at the request of the distributor, resulting in occasionally contradicting messages throughout the film. This is most likely the factor that led to the directors use of a pseudonym in the opening credits. 

The Osmonds (known then as The Osmond Boys), performed the main theme, also called "On Our Own", and other incidental songs and music.

==External links==
* 

 
 