Devathalara Deevinchandi
{{Infobox film
| name           = Devathalara Deevinchandi
| image          =
| image_size     =
| caption        =
| director       = Kommineni Seshagiri Rao
| producer       =
| writer         = Kommineni Seshagiri Rao (story and screenplay), Jandhyala Subramanya Sastry (dialogues)
| narrator       = Chandra Mohan Giri Babu Jayamalini Murali Mohan Allu Ramalingaiah
| music          = K. Chakravarthy
| cinematography = P. Deva Raj
| editing        =
| studio         =
| distributor    =
| released       = 1977
| runtime        =
| country        = India Telugu
| budget         = Rs. 9.75 lakhs
}}
Devathalara Deevinchandi is a Telugu film directed by Kommineni Seshagiri Rao.  His debut film was hit and resulted in many successful films later. 

It is remake of Nagin (1976 film)|Nagin, a highly successful 1976 Hindi film, which in turn was inspired by François Truffauts film The Bride Wore Black, based on Cornell Woolrichs novel of the same name. 

==The plot==
The story revolves around five close friends: Mohan (Ranganath), Kubera Rao (Haribabu), Muralikrishna (Chandramohan), Giri (Eswara Rao), and Ravindra (Madala Rangarao). Kubera Rao knows about a hidden treasure in the Nallamala forest. The friends go to forest to find the treasure in a Naga Temple. They hurt a Nagadevatha during a ritual. Kubera Rao soon dies of a snake bite.  Nagadevatha who takes human form as Phani (Jayamalini) and takes revenge on the five friends.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Narsimhachary || Muralikrishna
|-
| Giribabu || Upendra
|-
| Jayamalini || Phani
|-
| Eswara Rao || Giri
|-
| Madala Ranga Rao || Ravindra
|-
| Haribabu || Kubera Rao
|-
| Murali Mohan || Police Inspector
|-
| Baby Padma
|-
| Prabha || Savitri
|-
| Mada Venkateswara Rao || Driver
|-
| Allu Ramalingaiah
|-
| Ramaprabha
|- Ranganath || Mohan
|-
| Sakshi Ranga Rao
|-
| Baby Varalakshmi || Nagalakshmi
|-
| Vijayalakshmi
|}

==Soundtrack==
* Amma Oka Bomma Naanna Oka Bomma - Meekenduku Ee Roju Kopamocchindi (Singer: Baby Geetha; Cast: Baby Varalakshmi)
* Konaloki Vastaavaa Kotta Chotu Choopistaanu (Singer: P. Susheela; Cast: Jayamalini)
* Nagulachavitiki Nagendraswaami Puttanindaa Paalu Posemu Tandri (Singer: P. Susheela; Cast: Prabha)
* O Cheli Manohari Nee Kosame Nenunnadi (Singers: S.P. Balasubrahmanyam, P. Susheela; Cast: Chandramohan, Jayamalini)
* Srisaila Malleeswaraa - O Kaalahastiswaraa (Devatalara, Deevinchandi) (Singer: P. Susheela; Cast: Prabha)

==Boxoffice==
The film ran for more than 100 days in many centers across Andhra Pradesh.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 


 