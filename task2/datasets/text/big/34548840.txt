Prey (1977 film)
 
 
 
{{Infobox film
| name = Alien Prey
| image = Prey (1977 film).jpg
| caption =
| director = Norman J. Warren
| producer = Terence Marcel David Wimbury
| screenplay = Max Cuff
| story = Quinn Donoghue Barry Stokes Glory Annen Sally Faulkner Sandy Chinney
| music = Ivor Slaney
| cinematography = Derek V. Browne Alan Jones
| studio = Tymar Film Productions
| distributor = Premier Releasing
| released = 1977
| runtime = 85 minutes United Kingdom
| language = English
}}
 British science-fiction Glory Annen, Barry Stokes, Sally Faulkner and directed by Norman J. Warren. 

==Plot==
A bloodthirsty alien lands on Earth and assumes the identity of a young man. The alien is then befriended by a lesbian couple who lives nearby.

==Cast==
Cast in credits order: Barry Stokes as Anders
* Sally Faulkner as Josephine
* Glory Annen as Jessica
* Sandy Chinney as Sandy
* Eddie Stacey as 1st Policeman
* Jerry Crampton as 2nd policeman

==Edits==
* In the UK, in 1977, Premier Releasing submitted the film for certification to the British Board of Film Classification (BBFC) for a cinema release. The film was certified "X", precursor of the "18" certificate after unspecified cuts had been made. The submitted running time was 85 minutes.
* In Canada, in 1984,  Studio One Film and Video Inc released an unrated 85 minute edition on VHS cassette in the NTSC format under the title Alien Prey.
* Subsequently, further editions of the film were shortened by about 4 minutes. Using the Studio One edition as a reference, the deleted scenes and clips are: at 00:30:00, Jessica talks to Anders about being scared, Josephine talks about music, in the bedroom Jessica and Josephine argue about Anders, and finally in the kitchen Jessica and Anders talk. This sequence is three and a half minutes. At 00:36:15 a 10 second shot of Anders sitting in a chair is deleted, at 01:15:00 a door opening is shortened by 1 second, at 01:21:15 a 10 second sequence of Anders eating flesh and Josephine running down a corridor is deleted.  
* In the UK, the BBFC has certified a shortened version of the film "18" for home entertainment on three occasions: in 1986, Stablecane Ltd issued a video after 10 seconds of cuts had been made; in 1995, Arthouse Productions Ltd issued a video after 11 seconds of cuts had been made; in 2004, Anchor Bay Entertainment UK Ltd issued an extended cut of the film as part of the Norman J. Warren Coffin-Box DVD Collection but the four minutes of deleted scenes were not part of this edition.
* In the USA, Salvation Films, using its Redemption label has issued unrated editions of the film on DVD on two occasions, in 2004 and in 2009. Both editions are of the shortened version.
* In France, in 2006, NEO Publishing issued the film on DVD in the PAL format under the title, Le Zombie venu DAilleurs. Again, this is the shortened version despite NEO claiming it to be of 85 minutes duration.

==References==
 

==External links==
* 
*  Official U.K. website
*  Company website
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 