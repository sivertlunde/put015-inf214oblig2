Johnny Handsome
{{Infobox film |
| name = Johnny Handsome 
| image = Johnny_handsome.jpg
| image_size =
| caption = original film poster Walter Hill
| screenplay = Ken Friedman John Godey
| producer = Charles Roven Scott Wilson Lance Henriksen Morgan Freeman
| music = Ry Cooder
| cinematography = Matthew F. Leonetti
| editing = Donn Aron Carmel Davies Freeman A. Davies
| studio = Carolco Pictures The Guber-Peters Company
| distributor = TriStar Pictures|Tri-Star Pictures
| runtime = 94 min.
| released = September 29, 1989
| country = United States
| language = English
| budget = $20 million
| gross = $7,237,794  334,941 admissions (France) 
}}
 American Crime Walter Hill John Godey. The music for the film was written, produced and performed by Ry Cooder, with four songs by Jim Keltner. 

==Plot ==
John Sedley is a man with a disfigured face, mocked by others as "Johnny Handsome." He and a friend are double-crossed by two accomplices in a crime, Sunny Boyd and her partner Rafe, and a judge sends Johnny to jail, where he vows to get even once he gets out.

A surgeon named Fisher is looking for a guinea pig so he can attempt an experimental procedure in cosmetic surgery. Johnny, figuring he has nothing to lose, is given a new, normal-looking face (making him unrecognizable to the people who knew him) before he is released back into society.

Lt. Drones, a dour New Orleans law enforcement officer, is not fooled by Johnnys new look or new life, even when Johnny lands an honest job and begins seeing Donna McCarty, a respectable woman who knows little of his past. The lieutenant tells Johnny right to his changed face that, on the inside, Johnny is still a hardened criminal and always will be.

The cop is correct. Johnny cannot forget or forgive his sworn vengeance against Sunny and Rafe, joining them for another job, which ends violently for all.

==Cast==
*Mickey Rourke - John Sedley a.k.a. Johnny Handsome/Johnny Mitchell
*Ellen Barkin - Sunny Boyd
*Elizabeth McGovern - Donna McCarty
*Morgan Freeman - Lt. A.Z. Drones
*Forest Whitaker - Dr. Steven Fisher
*Lance Henriksen - Rafe Garrett Scott Wilson - Mikey Chalmette
*David Schramm - Vic Dumask
*Yvonne Bryceland - Sister Luke
*Peter Jason - Mr. Bonet
*J.W. Smith (actor)|J.W. Smith - Mr.Stathansom
*Jeffrey Meek - Earl (as Jeff Meek)
*Allan Graf - Bob Lemoyne
*Ed Zang - Prestige Manager
*John P. Fertitta - Prestige Salesman (as John Fertitta)
*Raynor Scheine - Gun Dealer Edward Walsh - Judge

==Production==
The novel was published in 1972. The New York Times called it "part psychological novel, part action thriller" which "moves like a runaway train, and is as expert an example of the genre as anybody is going to come across." Criminals At Large
By NEWGATE CALLENDAR. New York Times (1923-Current file)   21 May 1972: BR30.   The Chicago Tribune called it "wholly engrossing". Books Today: Primed for Crime
Cromie, Alice Chicago Tribune (1963-Current file); May 25, 1972; ProQuest Historical Newspapers: Chicago Tribune pg. B6 

Film rights were bought that year by 20th Century Fox who announced the film would be produced by Paul Heller and Fred Weintraub for their Sequota Productions Company. MOVIE CALL SHEET: Director Koch to Return Murphy, Mary Los Angeles Times (1923-Current File); Jul 29, 1972;
ProQuest Historical Newspapers: Los Angeles Times pg. B8  However the film was not made.

The project was reactivated in 1987. Richard Gere was going to star with Harold Becker to direct. OUTTAKES: THE SEQUEL MONOPOLIZING ELVIS
Matthew CostelloPat BroeskeDavid FoxJohn WilsonCraig Modderno. Los Angeles Times (1923-Current File)   08 Mar 1987: L82.  Eventually Al Pacino signed to play the lead. By February 1988 Becker was out as director, replaced by Walter Hill.  CALENDAR: OUTTAKES Packaging Problems
Los Angeles Times (1923-Current File)   21 Feb 1988: K22.  Then Pacino dropped out and Mickey Rourke was cast instead. Cinefile
Klady, Leonard. Los Angeles Times (1923-Current File)   14 Aug 1988: K32. 

Walter Hill later claimed he turned down the movie four times:
 No studio wanted to make it, and I didnt think any actor would be willing to play it. I wasnt sure the audience would buy the gimmick of the plastic surgery. Its an old-fashioned melodramatic device. Then about a year ago, I decided to do it. First, I figured that Hollywood is based on melodrama anyway and, second, I thought up a way to present the story in a way that resisted histrionics. More importantly, I found an actor who could play Johnny and not make it risible. Someone who understood the pitfalls of the thing. The main thing is that motion pictures have conditioned us to expect psychological realism. This is a drama in a different category. Its about moral choices... I knew I was on very thin ice. If you let any histrionics in, it will fall apart. You have to trust the drama of the whole rather than an individual scene. And thats antithetical to most actors. They want to know, Wheres my big moment? When do I get to cry and scream? Mickey understood that.   accessed 6 Feb 2015  
"Im drawn to characters where theres no happy ending, where things arent rosy in the end," said Rourke. "Its not a happy world and there arent easy, sappy endings to life." 

Hill said the film was reminiscent of 1940s film noir:
 You have the doomed character, and audiences back then were more comfortable with it. You can imagine John Garfield having a lot of fun with something like this... But this one has a hard road commercially, and Id like to see it have a chance to find an audience that will be interested. Some people like the movie and others are really offended by it. Thats fine with me. I like movies that stir things up a little.  
"The audience is invited to anticipate the drama rather than be surprised," added Hill. Polish filmdoms bittersweet future
Kehr, Dave. Chicago Tribune (1963-Current file)   17 Sep 1989: E2.  
Shooting took place in November 1988 in New Orleans, where Hill had previously made Hard Times and Southern Comfort. 

Ellen Barkin said she wanted to play her role because her character, Sunny "is one of the great female villains. I dont know if Ive ever seen a female villain so evil. Sunnys just mean, thats all there is to it. And the great thing about Sunny in this movie is they just let her be bad. With women they always want to give explanations: she had such a terrible childhood or something. They cant just let women be bad... Sex is just one of the tools Sunny uses to get what she wants. And what she wants is money! Pure greed!" 

==Release==
The film premiered in September 1989 at the Toronto Film Festival. It also screened at the Venice Film Festival.

The film was not a box office success. 

==Home video release==
After the films theatrical run, the film was released on videocassette and laserdisc in 1990 by International Video Entertainment. In 2002, the film was finally released on DVD, but without any bonus material and was shown in only a full frame presentation.

In 2010 the film was released on Blu-ray through Lions Gate Entertainment in its original widescreen presentation.
==Legacy==
In 2008, Slant Magazine published a review of the Mickey Rourke film The Wrestler which commented on the similarities between that and Johnny Handsome: 
 There is a moment, early on in the film, when he staggers down the street, through a bleak New Jersey morning, a great hulk of a man, too big for his clothes. His face looks battered and puffy, and suddenly, out of nowhere, I got an acute and clear memory of his performance as the deformed criminal in 1989s Johnny Handsome. In the opening shots of that film, "Johnny Handsome" skulks down the street; his face has a ballooning forehead, a bulbous nose, a cleft palate. We know it is Mickey Rourke because he is the star of the film, but we cannot tell it is him. The story of that film, of "Johnny Handsome" getting an operation on his face that leaves him looking like, well, a young and handsome Mickey Rourke, is the reverse of what we have seen happen in Mickey Rourkes real life. It is one of those odd art-meeting-biography truths. In The Wrestler, Mickey Rourkes actual face looks like the makeup-job he had done in that movie almost 20 years ago, and its a strange, tragic thing to contemplate.  

==References==
 

==External links==
*  
*  
*  
*  
*  by Roger Ebert
 

 
 
 
 
 
 
 
 
 