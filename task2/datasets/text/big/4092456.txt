The Hound of the Baskervilles (1981 film)
 
{{Infobox film name           = The Hound of the Baskervilles image          = Sobaka_dvd.jpg image_size     = caption        = DVD Cover art director       = Igor Maslennikov producer       = Lenfilm based on       =   screenplay     = Igor Maslennikov Yuri Veksler narrator       = starring       = Vasily Livanov Vitaly Solomin Borislav Brondukov music          = Vladimir Dashkevich cinematography = Dmitri Dolinin Vladimir Ilyin editing        = distributor    = released       =   runtime        = 147 minutes country        = Soviet Union language       = Russian budget         =
}}
 Solomin duo as Holmes and Watson, the film stars the internationally acclaimed actor/director Nikita Mikhalkov as Sir Henry Baskerville and the Russian movie legend Oleg Yankovsky as Jack Stapleton.

== Cast ==
* Vasily Livanov as Sherlock Holmes
* Vitaly Solomin as Dr. Watson
* Rina Zelyonaya as Mrs. Hudson
* Borislav Brondukov as Inspector Lestrade
* Irina Kupchenko as Beryl Stapleton
* Nikita Mikhalkov as Sir Henry Baskerville
* Alla Demidova as Laura Lyons
* Sergey Martinson as Mr. Frankland
* Oleg Yankovsky as Stapleton
* Aleksandr Adabashyan as Barrymore Svetlana Kryuchkova as Mrs. Barrymore
* Evgeny Steblov as Dr. Mortimer

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 