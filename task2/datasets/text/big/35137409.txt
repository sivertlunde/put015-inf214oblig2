Etter Rubicon
{{Infobox film
| name           = Etter Rubicon
| image          =
| image_size     =
| caption        =
| director       = Leidulv Risan
| producer       = Dag Alveberg
| writer         = 
| starring       = Sverre Anker Ousdal Toralv Maurstad
| music          = Geir Bøhren  Bent Åserud
| cinematography = 
| editing        =
| production_company = Filmeffekt
| distributor    = 
| released       = 10 September 1987
| runtime        = 93 minutes
| country        = Norway
| awards         = 
| language       = Norwegian
| budget         = 
| preceded_by    =
| followed_by    =
}}

Etter Rubicon is a 1987 Norwegian thriller film directed by Leidulv Risan and starring Sverre Anker Ousdal and Toralv Maurstad. It was produced by Dag Alveberg and the film company Filmeffekt. The film is a political commentary about the Cold War and follows the aftermath of two children dying after a military exercise off the coast of Nordland and Troms. 

==References==
 

 
 
 
 
 
 


 