Windfall (2010 film)
Windfall is a 2010 documentary film directed by Laura Israel about the reaction of residents in rural Meredith, New York (in Delaware County, New York) to a proposal to place numerous wind turbines in their community to harness wind power.

==Summary==
The film begins in 2004, when energy companies approached several property owners in Meredith, offering cash payments to allow the long-term placement of wind turbines standing over 400 feet tall on their land. The documentary portrays Meredith residents as deeply divided over the idea. Some believe the economic and energy benefits are worth investigating. Others are concerned about the towers being an eyesore, loss of property values, or posing a variety of hazards such as collapse, accumulation of ice which is then flung from the turbines in large chunks, or health problems attributed to low frequency noise. Residents of Lowville, New York are also interviewed, expressing regret at installing wind turbines in their community.

After an often rancorous debate, officials in Meredith ultimately decided against authorizing the use of wind turbines.

The film is composed mostly of interviews with Meredith residents. Also included are excerpts of news broadcasts, films of city council meetings, and computer-animated segments.

==Reactions==
Roger Ebert gave the film 3 out of 4 stars, writing that the film "left me disheartened. I thought wind energy was something I could believe in. This film suggests its just another corporate flim-flam game." He notes that there is doubtless a legion of wind-power activists and lobbyists who would counter-argue the points made in Windfall, but asks "How many of them live on wind farms?"  In a review for the New York Times, Andy Webster writes that Israels film tends to "overheat" but raises important questions:  "The quest for energy independence comes with caveats. Developers’ motives must be weighed, as should the risks Americans are willing to take in their own backyard." 

Wind energy advocate Mike Barnard panned the film for what he alleges are many factual errors and biases. 

==See also==
*Environmental impact of wind power

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 


 