Black on White (film)

{{Infobox film
| name           = Black on White
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jörn Donner
| producer       = Arno Carlstedt, Jörn Donner
| writer         = Jörn Donner
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Jukka Virtanen, Lasse Mårtenson
| music          = 
| cinematography = Esko Nevalainen
| editing        = Jörn Donner
| studio         = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}} Finnish drama film directed by Jörn Donner. The film stars Donner himself as a refrigerator salesman named Juha Holm who starts an affair with a young female hitchhiker named Maria (played by Kristiina Halkola).

The film was very controversial for its sex scenes, at the time the most daring in Finnish film history. According to film historian Peter von Bagh " he arena of conflict here, as in Donners subsequent films, is the bed, wheresoever it might be. The point of departure is a family portrait: an ideal image of happiness, a miniature of affluent Finland. The protagonist borders on burnout, and the camera follows the drama of the other disintegrating characters and relationships as if in a laboratory experiment." 

==References==
 

==External links==
* 

 
 
 
 
 
 