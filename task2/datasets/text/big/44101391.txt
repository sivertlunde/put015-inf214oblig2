Kaanathaya Penkutty
{{Infobox film 
| name           = Kaanathaya Penkutty
| image          =
| caption        =
| director       = KN Sasidharan
| producer       =
| writer         = Babu Mathews
| screenplay     = Babu Mathews
| starring       = Jayabharathi Bharath Gopi Mammootty Jagathy Sreekumar
| music          = Jerry Amaldev
| cinematography = V Aravindakshan
| editing        = Venugopal
| studio         = Kuleena Movie Makers
| distributor    = Kuleena Movie Makers
| released       =  
| country        = India Malayalam
}}
 1985 Cinema Indian Malayalam Malayalam film,  directed by KN Sasidharan. The film stars Jayabharathi, Bharath Gopi, Mammootty and Jagathy Sreekumar in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
 
*Jayabharathi as Bhavani
*Bharath Gopi as Devadas Menon
*Mammootty as Ram Mohan
*Jagathy Sreekumar as Vasu
*Thilakan as Matthai
*Manavalan Joseph as Tea Shop Owner Kuttettan
*Achankunju as Achankunju
*Alummoodan as Abdu
*VK Sreeraman as Circle Inspector Nambiar
*Ramachandran as S.P.
*Sandhya as Mini
 

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Sebastian Paul. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chellam Chellam || KB Sujatha, Preethi, Renuka, Elizabeth, Molly, Nadin || Sebastian Paul || 
|}

==References==
 

==External links==
*  

 
 
 

 