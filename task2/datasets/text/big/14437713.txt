Pigs (2007 film)
{{Infobox film
 | name = Pigs
 | image = Pigs VideoCover.jpeg
 | director = Karl DiPelino
 | producer = Chris Ragonetti
 | writer = Karl DiPelino Chris Ragonetti
 | starring = Jefferson Brown Darryn Lucio Melanie Marden Christopher Elliott
 | music = John Jamieson
 | cinematography = Gurjeet Mann
 | editing = Karl DiPelino
 | distributor = ThinkFilm
 | released =  
 | runtime = 85 minutes
 | country = Canada
 | language = English
}}
Pigs is a 2007 Canadian teen comedy directed by Karl DiPelino. The title refers to the slang meaning of the word pig, an Egotism|egoist; someone who disregards others feelings and acts out of self-interest.

==Plot==
Ladies man and soon-to-be college graduate Miles is a player who keeps journals to record his "conquests". His friend Cleaver thinks Miles should accept a bet: can Miles sleep with enough girls before he graduates, to complete the alphabet (using the first letter in the girls surnames)? Having already conquered a number of girls with different first-letters in their surnames, Miles stands a good chance of being able to succeed. It all boils down to whether he can find and sleep with a girl whose surname starts with an X. Miles roommate Ben, however will prove to be a problem for Miles, since he has a crush on Gabrielle, Miles target.
At the end of the bet, Cleaver tries to push Miles even harder, since the pot has grown to 30,000 dollars. Miles, however, has fallen for Gabrielle, who is different from every girl hes ever met. They start dating, and Ben becomes more jealous. Ben decides to ruin Miles chances of sleeping with Gabrielle by telling Gabrielle about the bet, even though Miles has clearly explained that Gabrielle means more to him than the money.

==Cast==
*Jefferson Brown .... Miles
*Darryn Lucio .... Cleaver
*Melanie Marden .... Gabrielle "X"
*Christopher Elliott .... Ben
*Kelly Cunningham .... Wendy "P"
*Katharine Jane Reid .... Fran
*Tyrone Greenidge .... Big Eddie
*Derek Cvitkovic .... Silvio
*Ted Neal .... Tommy
*Heidi Rayden .... Michelle Noonan
*Leslie Ferreira .... Stacy Usher/Swanson
*Sarah Scheffer .... Vicky
*Subeena Ishaq .... Rebecca Stinson
*Kim Allan .... Party Girl
*Yo Mustafa .... Waiter
*Chris Traps .... Struggler on Cutting Room Floor

==External links==
* 

 
 
 
 


 
 