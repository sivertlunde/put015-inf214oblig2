Gentleman (1993 film)
 
 
 

{{Infobox film name = Gentleman image = Gentleman 1993 poster.jpg caption = Poster director = Shankar
|producer = K. T. Kunjumon writer = Balakumaran  (Dialogue)  screenplay     = Shankar story          = Shankar starring = Arjun Madhoo Senthil Vineeth music = A. R. Rahman cinematography = Jeeva
|editing = B. Lenin V. T. Vijayan studio = K. T. Kunjumon|A. R. S. Film International distributor = A. R. S. Film International released =   runtime = 160 mins country = India language = Tamil awards = budget = gross =
}} 1993 Tamil Tamil Action thriller film Arjun and Madhoo in the lead roles.
 blockbuster at the box office. 
 Hindi as The Gentleman in 1994 by Mahesh Bhatt starring Chiranjeevi.

==Plot==
A story begins with a swashbuckling thief, also a master of disguise, who not only targets jewellers but also government agencies is being tracked down by Azhagar Nambi (Charan Raj), an efficient Police officer, who takes things a bit too personal. The story cuts to Krishnamoorthy alias Kicha (Arjun Sarja|Arjun), a respected brahmin and runs a home-based pappadam business.

Susheela (Madhoo), one of Kichas many female employees, who has a crush on Kicha is constantly devouring for his attention. Adding to her woes, the arrival of her naughty and playful cousin Sugandhi (Subhashri), adds a love triangle to the plot. Sugandhi also forms a crush on Kicha, especially after being saved by him from some molesting goons. But Kicha reveals to her that he has no such feeling for her and wants her to find a suitable mate. Sugandhi steals Kichas ring as souvenir. An intermittent comedy track featuring Mani and played by the famous comic duo Goundamani and Senthil respectively.

After several unsuccessful attempts at nabbing the thief, a disgraced Azhagar Nambi shaves his head and left with a ring mark on his face, after a scuffle with the thief. On investigating he later realises that the mark was formed by a ceremonial ring worn by Brahmin priests, but to his vain finds that the specific design of this ring is uncommon to Brahmin priests but rather resembles a mangalsutra. Both intrigued and frustrated, Azhagar Nambi is forced by his parents into getting married and by coincidence to Sugandhi, from whom he gets the particular ring he was tracking and finds out that it belongs to Kicha.

In an attempt to trap and arrest Kicha, Azhagar Nambi plots an attempt at Kichas house where they were invited for wedding diner, hosted by Kicha, where he shoots Kicha, but the latter narrowly escapes with bullet injury in his hand, along with Mani. They were followed by Susheela to their hide out, where Susheela finds the duo, and fabercasts Kicha for his false deed. Then, Kicha reveals his flashback about his student life as a district level topper along with his best friend Ramesh (Vineeth), and even then they both were denied their desired Medical college seats due to bribery, leading to tragic death of his mother (Manorama (Tamil actress)|Manorama) and his best friend Ramesh (Vineeth). And since then, he became a thief to build a college of his own, where he desires to make education available to the deserved, with out any difference to poor, rich, or any caste.

In order to fund the final stages of the college building, Aware of the police trap, Kicha makes his final attempt to success, during which he is arrested by Azhagar Nambi. The climax of the movie is set in the courtroom, where Kicha demands the Chief minister of the state should come to the court room, who was the then educational minister who demanded the bribe from him. He is exposed to the public and a youth who is aspired by the ideologies of Kicha kills the minister planting a bomb on himself. Its later shown that Kicha serves his sentence in jail and after his release he and susheela inaugurate the medical college for the poor students.

==Cast== Arjun as Krishnamoorthy (Kicha)
* Madhoo as Susheela (Susi)
* Goundamani as Mani
* Charan Raj as Azhagar Nambi
* Subhashri as Sugandhi
* Senthil as Babloo
* Vineeth as Ramesh Manorama as Kichas mother
* M. N. Nambiar as Rameshs father
* Rajan P. Dev
* Ajay Rathnam
* Prasanna
* Omakuchi Narasimhan
* Chokkalinga Bhagavathar
Special appearances in "Chikku Bukku Rayile" song
* Prabhu Deva
* Gouthami Tadimalla
* Raju Sundaram
* Raghava Lawrence
* G. V. Prakash Kumar

==Production==
The film marked the directorial debut of Director Shankar, former assistant of S. A. Chandrasekhar who went to become one of the famous directors in Tamil cinema.

Sarathkumar was initially approached for the lead role but since he was busy with other projects he was replaced by Arjun. 

==Soundtrack==
{{Infobox album
|  Name        = Gentleman
|  Type        = Soundtrack
|  Artist      = A. R. Rahman
|  Cover       = Gentleman Audio Cover.jpg
|  Caption     = Front Cover
|  Background  = Gainsboro
|  Released    = 1993
|  Recorded    = Panchathan Record Inn
|  Genre       = Soundtrack
|  Length      =  23:53 Pyramid
|  Producer    = 
|  Reviews     = 
|  Last album  = Pudhiya Mugam (1993)
|  This album  = Gentleman (1993)
|  Next album  = Kizhakku Cheemayile (1993)
}}
The score and soundtrack of the movie was composed by A. R. Rahman and lyrics penned by Vairamuthu and Vaali (poet)|Vaali. The soundtrack became a chartbuster upon release and was highly acclaimed. Gentleman marked the beginning of a famed collaboration between A. R. Rahman and director S. Shankar. The film and soundtrack were dubbed and released in Telugu with the same name. The lyrics were penned by Rajashri for this version.

The choreography was done by Prabhu Deva who also made an appearance in the song "Chikku Bukku Rayile" written by Vaali.
 Filmfare Best Sujatha got the Tamil Nadu State Film Award for Best Female Playback for her soulful rendition of the track "En Veettu Thotathil".

A Gentleman instrumental adaptation album was subsequently released on Lahari and became a run away success. The track "Ottagathai Kattiko" soundtracked a BBC fashion show.

{{tracklist
| headline = Tamil version (Original)
| extra_column = Singer(s)
| title1 = En Veetu Thotathil Sujatha
| length1 = 4:05
| title2 = Usalampatti Penkutti
| extra2 = Shahul Hameed, Swarnalatha
| length2 = 4:41
| title3 = Chikku Bukku Rayile
| extra3 = Suresh Peters, G. V. Prakash Kumar
| length3 = 5:24
| title4 = Parkathey
| extra4 = Minmini, Srinivas (singer)|Srinivas, Noel James
| length4 = 4:29
| title5 = Ottagathai Kattiko
| extra5 = S. P. Balasubrahmanyam, S. Janaki
| length5 = 5:15
}}

{{tracklist
| headline = Telugu version (Dubbed)
| extra_column = Singer(s)
| title1 = Kontegaadni Kattuko
| extra1 = S. P. Balasubrahmanyam, S. Janaki
| length1 = 5:15
| title2 = Chikubuku Raile
| extra2 = Suresh Peters, G. V. Prakash Kumar|G. V. Prakash
| length2 = 5:24
| title3 = Naa Inti Mundunna Sujatha
| length3 = 5:15
| title4 = Maavele Maavele
| extra4 = Minmini, Srinivas (singer)|Srinivas, Noel James
| length4 = 4:29
| title5 = Mudinepalli
| extra5 = Shahul Hameed, Malgudi Subha, Swarnalatha
| length5 = 4:40
}}

==Awards==
*Filmfare Award for Best Tamil Film - K. T. Kunjumon Filmfare Best Shankar
*Filmfare Filmfare Best Music Director Award - A. R. Rahman
*Tamil Nadu State Film Award for Best Actor - Arjun Sarja
*Tamil Nadu State Film Award for Best Director - Shankar
*Tamil Nadu State Film Award for Best Music Director - A. R. Rahman

==References==
 

==External links==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 