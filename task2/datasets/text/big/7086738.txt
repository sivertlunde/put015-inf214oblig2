Rogue (film)
{{Infobox film name = Rogue image = Rogueposter2007.jpg image_size = caption = Official poster for Rogue director = Greg McLean producer = Matt Hearn David Lightfoot Greg McLean writer = Greg McLean narrator = starring = Michael Vartan Radha Mitchell Sam Worthington John Jarratt music = Frank Tetaz cinematography = Will Gibson editing = Jason Ballantine studio = Village Roadshow Pictures Emu Creek Pictures distributor =Dimension Films released =         runtime = 99 minutes country = Australia language = English budget =   gross = $4,623,570 preceded_by = followed_by =
}}
  2007 Cinema Australian independent independent horror film about a group of tourists in Australia who fall prey to a giant, man-eating crocodile. Rogue was released in Australia on 8 November 2007.   
 Wolf Creek. It was produced by David Lightfoot and Matt Hearn and made on a budget of  .  The film was inspired by the true story of Sweetheart (crocodile)|Sweetheart, a giant Australian crocodile that attacked boats in the late 1970s, although in real life, Sweetheart was never responsible for an attack on a human.

==Plot==
While carrying out his research as a travel journalist, cynical American Pete McKell (Michael Vartan) joins a group of tourists on a crocodile watching river cruise in Kakadu National Park of Australias Northern Territory, led by wildlife researcher Kate Ryan (Radha Mitchell). After a run-in with two locals, Neil (Sam Worthington) and Collin (Damien Richardson), the cruise winds to a close and Kate prepares to return the group to base.
 Robert Taylor) spots a flare in the distance, and Kate tells the group that they must investigate to determine whether someone needs rescuing. A few miles up river, they come across a half-sunken wreck when suddenly, something crashes into the tourists boat, creating a crack in the side. Kates only choice is to run it ashore on a small island in the middle of the river. The group disembarks and begins discussion of what to do when Everett is suddenly pulled into the water by an unseen predator and killed. Kate comes to the conclusion that they are in the heart of a large crocodiles territory and that it will be more aggressive than usual. The tourists realize that by nightfall, the tide will start to rise and in a matter of hours, their small island will be submerged.

Neil and Collin arrive soon after, and as they near the island, an unseen force, smashes their boat and sinks it. Neil manages to swim to the island safely, but Collin disappears.
 Geoff Morrell) becomes impatient and aggressive and attempts to get himself and his daughter Sherry (Mia Wasikowska) across with Mary Ellen still on the line. While trying to secure the rope, Neil is attacked by a crocodile and killed. The tree holding the rope breaks and the three on the line fall into the water. They manage to swim back to the island, but as Allen crawls up the beach, the gigantic 23-foot crocodile suddenly lunges out of the water, rips off his right arm, and throws him into the middle of the river, where he is dragged under and killed.
 Stephen Curry) is skeptical of the idea, but Russell (John Jarratt) agrees to try. Lacking bait, everyone suggests using Kates dog (called Kevin), but they decide to use two dead birds that Neil and Collin had shown them earlier. Kate hooks the birds onto the boats anchor while Pete secures the other end of the rope to a boulder and throws the bait out into the river. After a long wait, the anchor is suddenly grabbed and pulled and the group makes a break for the far shore. Russell assists Sherrys mother, Elizabeth (Heather Mitchell), due to her inability to swim. Pete tries to stop the boulder from being pulled over as Kate swims across behind the group. The crocodile suddenly lets go of the hook and bait, seizes Kate, and drags her underwater. Pete hurriedly makes the swim across the river with Kevin in tow, and heads off into the bush to meet up with the others.

As day breaks, Pete is crossing a stream when Kevin runs away. Pete chases the dog into a cave and falls down a narrow chute into a larger cave, where he sees Neils corpse. He realizes that the cave is the crocodiles lair, and to his surprise, he finds Kate alive, but badly injured and unconscious. He attempts to carry her out but has to hide when he hears the crocodile returning and Kevin is devoured. Finishing its meal of Kevin, the crocodile enters the cave and falls asleep. Trying to reach the entrance carrying Kate, Pete wakes the crocodile, and it makes several attempts to kill both him and Kate as he retreats into the narrow confines of the cave. Finally, after a long fight in which he gets tossed around and is severely bitten on the hand, Pete makes one desperate last stand. He braces a broken log against a large boulder with the sharp end pointing out towards the crocodile. The crocodile lunges at him, and Pete successfully impales it through the head killing it. He escapes from the cave with Kate to join the surviving tourists and waiting paramedics.

As the credits roll, the camera zooms in on a newspaper article detailing Petes heroic battle with the crocodile and rescue of Kate.

==Location== Katherine Gorge Gilderoy in Victorias Yarra Valley. 

==Cast==
*Michael Vartan as Pete McKell
*Radha Mitchell as Kate Ryan
*Sam Worthington as Neil Stephen Curry as Simon
*Celia Ireland as Gwen
*John Jarratt as Russell
*Heather Mitchell as Elizabeth Geoff Morrell as Allen
*Mia Wasikowska as Sherry
*Caroline Brazier as Mary Ellen Robert Taylor as Everett Kennedy
*Damien Richardson as Collin

==Reception==

===Box office===
Rogue debuted in the Australian box office on 11 November 2007 making  . After 11 weeks in the nations cinemas it left making A$1.8 million. It was released in the United States on 25 April 2008 and in its first weekend made  . It remained in theatres for three more days before making an exit on a low US$10,452. As of 8 August 2008, Rogue has made A$3,475,708 worldwide.

===Reviews===
  Wolf Creek. The film holds a rare 100% approval rating from 11 critics on Rotten Tomatoes.  Melbournes Herald Sun critic Leigh Paatsch gave the film three out of five stars stating that, "If you must see at least one killer croc movie before you die, it may as well be this polished little Australian schlocker".  Sydney Morning Herald critic Sandra Hall gave the movie three and a half out of five stars writing that, " ts almost elegant. Its only disadvantage is it conjures up inevitable comparisons with Jaws (film)|Jaws...a benchmark the film has no hope of achieving". 

==Accolades==
{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (2008 AFI Awards)  AACTA Award Best Visual Effects Andrew Hellen
| 
|- Dave Morley
| 
|- Jason Bath
| 
|- John Cox
| 
|- Australian Screen Editors Avid Award for Best Editing on a Feature Film Jason Ballantine
| 
|- AWGIE Awards|AWGIE Award Best Original Feature Film Greg McLean
| 
|- Fangoria Chainsaw Award Best Limited-Release/Direct-to-Video Film
| 
|- Sitges Film Festival Best Film Greg McLean
| 
|-
|}

==DVD release==
Rogue was released on DVD in Australia on 29 May 2008.  The DVDs special features include "The Making of Rogue" documentary, four featurettes, and a theatrical trailer. The US and UK DVDs feature an additional audio commentary. As  of 2013, Rogue has been released on Blu-ray in Canada and the UK. The Canadian disc features the film only, whilst the UK disc includes all of the aforementioned extras, bar the trailer. 

==See also==
* Cinema of Australia
* List of killer crocodile films Sweetheart

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 