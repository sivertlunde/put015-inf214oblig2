The Herd (1978 film)
{{Infobox film
| name           = The Herd
| image          = 
| image size     = 
| caption        = 
| director       =  
| producer       = Yılmaz Güney
| writer         = Yılmaz Güney
| starring       =  

| music          =  
| cinematography = İzzet Akay
| editing        = Zeki Ökten
| distributor    = TurkishFilmChannel
| released       =  
| runtime        = 129 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
}}

The Herd ( ) is a 1978 Turkish drama film, written, produced and co-directed by Yılmaz Güney with Zeki Ökten during Güneys second imprisonment, featuring Tarık Akan as a peasant, forced by a local blood feud to sell his sheep in far away Ankara. The film, which went on nationwide general release on  , was screened in competition at the 30th Berlin International Film Festival, where it won Interfilm and OCIC Awards, the Locarno International Film Festival, where it won Golden Leopard and Special Mention, was scheduled to compete in the cancelled 17th Antalya Golden Orange Film Festival, for which it received 6 Belated Golden Oranges, including Best Film and Best Director, was awarded the BFI Sutherland Trophy and was voted one of the 10 Best Turkish Films by the Ankara Cinema Association.

==Awards==
*17th Antalya Golden Orange Film Festival
**Belated Golden Orange for Best Film (won) The Enemy)
**Belated Golden Orange for Best Music: Zülfü Livaneli (won)
**Belated Golden Orange for Best Actress: Melike Demirağ (won, shared with Güngör Bayrak for The Enemy) The Sacrifice, shared with Aytaç Arman for The Enemy)
**Belated Golden Orange for Best Supporting Actor: Tuncel Kurtiz (won)
*  (won)

==External links==
* 
*  Distributor page for the film

 

 

 
 
 
 
 
 
 
 
 
 

 
 