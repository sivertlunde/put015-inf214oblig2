Soul Assassin
{{Infobox film
| name           = Soul Assassin
| image          = 
| caption        = 
| director       = Laurence Malkin
| producer       = Laurence Malkin
| writer         = Laurence Malkin Chad Thumann
| starring       = Kristy Swanson Skeet Ulrich Rena Owen
| music          = Alan Williams
| cinematography = Lex Wertwijn
| editing        = Robert Brown
| distributor    = Columbia Pictures
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = $7,000,000
| gross          = Unknown
}}

  thriller film starring Rena Owen, Kristy Swanson and Skeet Ulrich. The film is about a young security agent, Kevin Burke, who is employed by a multinational banking firm. He intends to ask his girlfriend Rosalind to marry him. His girlfriend, however, is brutally murdered by a hitman. Kevin then swears to avenge Rosalinds death. 

==Cast==
* Kristy Swanson as Tessa Jansen
* Skeet Ulrich as Kevin Burke
* Rena Owen as Karina
* Katherine Lang as Rosalind
* Derek de Lint as Karl Jorgensen

==References==
 

 


 