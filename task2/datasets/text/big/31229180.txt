Your Cheatin' Heart (film)
{{Infobox film
| name           = Your Cheatin Heart
| image          = 
| alt            = 
| caption        = 
| director       = Gene Nelson
| producer       = Sam Katzman
| writer         = Stanford Whitmore
| narrator       =  George Hamilton Susan Oliver Red Buttons
| music          = Fred Karger
| cinematography = Ellis W. Carter
| editing        = Ben Lewis
| studio         = Four-Leaf Productions
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 min
| country        = United States English
| budget         = 
| gross          = $2,500,000 (US/ Canada rentals) 
}}
 1964 Musical musical directed George Hamilton, Susan Oliver and Red Buttons.

==Plot summary==
The film begins with young Hank Williams trying to earn money by pitching a snake-oil cure-all to the gullible, capping his spiel by picking up his guitar and singing. In the crowd is The Drifting Cowboys, a group of touring country-western musicians who happen to be passing through.  They invite Hank Williams to join their group, and music history is made.  Memphis-born George Hamilton is winningly natural as Williams in the musical biopic of the short-lived but forever influential “Hillbilly Shakespeare.” It’s a deeply felt story, made with the assistance of his widow Audrey and featuring timeless songs (Long Gone Lonesome Blues, I Can’t Help It and  .      The end scene (when the audience is notified that Williams has passed away while on the way there) is a powerful one, as one audience member stands up umprompted and begins to sing "I Saw the Light". Others stand up quickly and join him, as the spotlight shines on the stage where Hank should be.  (This was similar to what actually happened after Williams died, as Hawkshaw Hawkins and several musicians began singing "I Saw The Light", and the crowd joined in, thinking at first that the announcement was an act, but when Hawkins and company began singing, the crowd realized it was no act.)

==Versions== MGM Musical Film to be produced in Black and White.
 SuperStation WTBS Hank Williams, Sr.s Death.

==Cast== George Hamilton as Hank Williams
* Susan Oliver as Audrey Williams
* Red Buttons as Shorty Younger Fred Rose
* Shary Marshall as Ann Younger Rex Ingram as Teetot
* Chris Crosby as Sam Priddy
* Rex Holman as Charley Bybee
* Hortense Petra as Wilma, the Cashier
* Roy Engel as Joe Rauch
* Donald Losby as Young Hank Williams
* Kevin Tate as Boy Fishing.

==Production==

===Development=== Jeff Richards and June Allyson in the lead roles. Frank Ross to Sponsor Joan Caulfield on TV; Garmes Produces Fear Jailhouse Rock. Cheatin Heart Named for Presley; Holdens Rival British Favorite
Schallert, Edwin. Los Angeles Times (1923-Current File)   August 12, 1957: C11.  However Colonel Tom Parker refused. Paul Gregory became attached as producer and wanted Steve McQueen for the lead. Looking at Hollywood: Plan Film About Your Cheatin Heart Composer
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   November 18, 1959: b5. 
 Nick Adams but he turned it down as well. According to George Hamilton, MGM then "revised their concept of the film as quickie drive-in fare that might sell some records in the South and maybe to some crossover Beverly Hillbillies fans."  They assigned their film to producer Sam Katzman who specialized in low budget fare.

===Casting George Hamilton===
Parker was friendly with actor George Hamilton, who had been a fan of Williams music since his youth, knew every song Williams had written and could also play the guitar. Hamilton was under contract to MGM but says the studio "didnt see their stock company preppy playboy playing a drug addict honky tonk crooner".  Hamilton Portrays Singer But Only Mouths Songs
Los Angeles Times (1923-Current File)   November 17, 1964: C9. 

Parker introduced Hamilton to Hank Williams widow Audrey. The two of them got along well and Audrey lobbied on Hamiltons behalf. Hamilton says that "Audrey wanted the movie to happen, especially to make her son Hank William Jr a singing star the same way she had pushed Big Hank to stardom." Hamilton p 181  The idea was that Hank Williams Jr would dub the singing in the movie and release the soundtrack album under his name; Hamilton wanted to perform the songs himself - "that was the key to the character" - but knew the only way he would get the part was to agree to be dubbed.  With Audreys support, Hamilton got the part, his signing being announced in November 1963. Riddle Sound Is Ring-a-Dingy
By Eddie Gallaher. The Washington Post, Times Herald (1959-1973)   November 17, 1963: G9. 

Paula Prentiss was at one stage attached as female star. Looking at Hollywood: Paula Prentiss Lands Big Film Role
Hopper, Hedda. Chicago Tribune (1963-Current file)   March 16, 1964: b3. 

===Shooting===
Filming started April 1964.  Hamilton says Sam Katzman ran a tight ship. "Jungle Sam cracked the whip, whacked the cane and the whole film was in the can right on time. But he gave me free rein creatively and our director... brought in something memorable, and even Sam knew it." 

==Release==
According to Hamilton, "the movie made me a hero in the South, but because it was a small film, it didnt get the exposure it deserved in the rest of the country." Hamilton p 183 

However among the films fans were Colonel Tom Parker and Hamiltons later girlfriends Lynda Bird Johnson and Alana Hamilton. 

==DVD==
Your Cheatin Heart was released on DVD November 9, 2010, by Warner Archive as a MOD (Manufacture On Demand) disc via Amazon (Black and White version). The colorized version has never been released on any form of home video, but DVD-Rs of it frequently show up on websites specializing in bootlegs of rare movies.

==References==
 
* George Hamilton & William Stadiem, Dont Mind If I Do, Simon & Schuster 2008

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 