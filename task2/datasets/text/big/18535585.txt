Contract (2008 film)
 
 
{{Infobox film
| name           = Contract
| image          = Contract Hindi Movie.jpg
| director       = Ram Gopal Varma   
| writer         = Prashant Pandey Zakir Hussain Ajay Bijli Praveen Nishchol Sanjeev K Bijli
| distributor    = PVR Pictures
| studio         = PVR Pictures
| cinematography =
| editing        =
| released       = 2008
| runtime        = Hindi
| music          = Amar Mohile, Bappi Tutul, Sana Kotwal
}}
 Hindi film by Ram Gopal Varma, where the story revolves around an ex-commando man, Aman, whose wife and daughter are killed in a terrorist attack.    A top undercover cop then approaches Aman to collaborate with him to hunt the mastermind of these terrorist attacks Sultan, who is planning another big attack on the city of Mumbai.    With meticulous planning, Aman infiltrates the underworld gangs to reach Sultan, the chief mentor.

==Music==
* Maula Khair Kare - Shilpa Rao, Sukhwinder Singh
* Aa Khushi Se Khudkhushi Kar Le - Shaan, Sunidhi Chauhan
* The Heart Of Contract - Instrumental
* Jeene Ka - Runa Rizvi, Ravi Shankar
* Badalon Pe - Shaan
* Saathiya - Adnam Sami, Tulsi Kumar
* Har Kafan - Abhishek Nailwal, Runa Rizvi
* Twinkle Twinkle - Amitrajit Bhattacharee, Devika Verma, Rajana Verma, Trishe
* Khallas - Asha Bhosle, Sudesh Bhosle, Sapna Awasthi
* Hai Aag Yeh - Sunidhi Chauhan
* Take Lite - Jiah Khan

==Cast and crew==
* Amruta Khanvilkar... Divya
* krishna ... Amaan
* Kishor Kadam ... Dara
* Sumeet Nijhawan ... R.D.
* Upendra Limaye ... Goonga
* Amruta Subhash ... Goongas Wife
* Vibha Cheebur ... Commissioner
* Raaj Gopal ... RAW Agent 2
* Sakshi Gulati ... Iya Zakir Hussain ... Sultan
* Brajesh Jha ... Zahwari
* Yasir Khan ... Allwyn
* Jai Tari... Bhansal

==References==
 

 

 
 
 
 
 
 


 