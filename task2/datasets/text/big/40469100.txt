Khatron Ke Khiladi (2001 film)
{{Infobox film
| name           = Khatron Ke Khiladi
| image          = KKKhiladi.JPG
| image_size     =
| caption        = DVD Cover
| director       = Imraan Khalid
| producer       = Sanjay Chaturvedi
| writer         =  
| narrator       =
| starring       = Mithun Chakraborty   Raj Babbar Pooja Gandhi Sudesh Berry Puru Raajkumar Ronit Roy
| music          = Ram Shankar
| cinematography = 
| editing        =  
| distributor    = 
| released       = May 4, 2001
| runtime        = 150 min.
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 2001 Bollywood|Hindi-language Indian feature directed by Imraan Khalid for producer Sanjay Chaturvedi, starring Mithun Chakraborty and Raj Babbar in the lead role along with a galaxy of supporting stars.

==Cast==

*Mithun Chakraborty
*Raj Babbar
*Pooja Gandhi
*Ronit Roy
*Puru RaajKumar
*Sudesh Berry
*Rutika Singh
*Shweta Menon
*Ishrat Ali
*Kiran Kumar
*Aushim Khetrapal
*Vishwajeet Pradhan
*Asha Sachdev
*Rami Reddy
*Amit Pachori
*Tej Sapru
*Sheel Jahangira

==Soundtrack==

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Lyrics
|-
| 1
| "Khatro Ke Khiladi"
| Vinod Rathod, Mohammed Aziz
| Ibrahim Ashq 
|-
| 2
| "O Jameela" Abhijeet
|Gunvantraaj 
|-
|}

==References==
 
*http://www.bollywoodhungama.com/moviemicro/cast/id/539690

==External links==
* 

 
 
 
 
 
 