The Fatal Mallet
 
{{Infobox film
| name = The Fatal Mallet
| image = The Fatal Mallet.jpg
| image size= 180px
| caption =  Mabel Normand, Mack Sennett, and Charles Chaplin in The Fatal Mallet (1914)
| director =  Mack Sennett
| producer = Mack Sennett
| writer = Mack Sennett
| starring = Charles Chaplin Mabel Normand Mack Sennett
| music = Frank D. Williams
| editing =
| distributor = Keystone Studios
| released =  
| runtime = 18 minutes English (Original titles)
| country = United States
| budget =
}}
  1914 United States|American-made motion picture starring Charles Chaplin and Mabel Normand.  The film was written and directed by Mack Sennett, who also portrays one of Chaplins rivals for Normands attention (Sennett and Normand were offscreen lovers  during this period).

The Fatal Mallet is one of more than a dozen early films that writer/director/comedienne Mabel Normand made with Charles Chaplin; Normand, who had written and directed films before Chaplin, mentored the young comedian.

==Synopsis==
Three men will fight for the love of a charming girl. Charlie (in famous tramp guise) and one other suitor (unusually played by Mack Sennett himself) teams up against the third, and play dirty, throwing bricks and using a mallet. However, Charlie double-crosses his partner, thus losing his trust and the girl in the end.

==Cast==
* Charles Chaplin - Suitor
* Mabel Normand - Mabel
* Mack Sennett - Rival suitor
* Mack Swain - Another rival

==See also==
* List of American films of 1914
* Charlie Chaplin filmography

==External links==
* 
* 
*  

 
 
 
 
 
 
 
 
 
 


 