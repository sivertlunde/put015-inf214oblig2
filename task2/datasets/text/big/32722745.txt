La-La Loco Baby
{{Infobox film
| name           = La-La Loco Baby
| image          = 
| caption        = Release poster
| director       = Miklos Philips
| producer       = Miklos Philips Anna West	 
| writer         = Stephen Serpas	
| starring       = Chris Moir Satomi Okuno Rene Rivera Ilia Volok Jeremy Fultz Olya Milova
| music          = Eduardo Queiroz
| cinematography = Ulf Soderqvist
| editing        = Miklos Philips	 
| distributor    = Point Zero Pictures
| released       =  
| runtime        = 25 minutes
| country        = United States
| language       = English
| budget         = 
}} American short action comedy film starring Chris Moir, Satomi Okuno, Ilia Volok and Rene Rivera as "Diesel" and is based on the life of Roberto Vilme known as Baby Lala.

==Plot==
Mitch, a rookie LA cop is having nightmares and meets Keiko a girl he had believed to be a figment of his dream. An irresistible Japanese naïf and runaway daughter of a Yakuza boss, Keiko reveals surprising skills that save the day. Against all odds, together they ace Mitchs jaded sergeant Diesel, and bring down the erudite though brutal Kazimir, a disillusioned Russian mobster.

==References==
 

==External links==
* 
* 

 
 