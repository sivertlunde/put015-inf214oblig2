When Marnie Was There
 
 
{{Infobox film
| name           = When Marnie Was There
| image          = Omiode no Marnie poster.jpg
| alt            = 
| caption        = Japanese poster for When Marnie Was There
| film name      = {{Infobox name module
| kanji          = 思い出のマーニー
| romaji         = Omoide no Mānī
| translation    = Memories of Marnie
}}
| director       = Hiromasa Yonebayashi Toshio Suzuki
| writer         = Keiko Niwa Masashi Andō Hiromasa Yonebayashi
| screenplay     = 
| story          = 
| based on       =  
| starring       = Sara Takatsuki Kasumi Arimura
| narrator       =  
| music          = Takatsugu Muramatsu
| cinematography = Junji Yabuta Atsushi Tamura or Jun Tamura Hidenori Shibahara
| editing        = Rie Matsubara
| studio         = Studio Ghibli
| distributor    = Toho
| released       =  
| runtime        = 103 minutes
| country        = Japan
| language       = Japanese
| budget         = Japanese yen|¥1.15 billion (United States dollar|US$10,523,124)
| gross          = Japanese yen|¥3.63 billion  (US$30,563,937)
}}

 
  is a 2014 Japanese anime film written and directed by Hiromasa Yonebayashi, produced by Studio Ghibli, and based on the novel When Marnie Was There by Joan G. Robinson.  It was released on 19 July 2014.  It was the final film for Studio Ghibli before they announced that the film division is taking a short hiatus after The Tale of the Princess Kaguya, and the retirement of Hayao Miyazaki a year before the film was released.  This is potentially the last feature film to be released by Studio Ghibli. 
The film was released on Blu-ray and DVD in Japan on 18 March 2015. 

==Plot==
Anna Sasaki is a 12-year-old girl who lives in Sapporo with foster parents, Yoriko and her husband. One day at school she collapses from an asthma attack, so her parents send her to spend the summer with Setsu and Kiyomasa Oiwa, relatives of Yoriko, in Kushiro, Hokkaido (town)|Kushiro, a rural, seaside town where the air is clear.

Anna sees an abandoned mansion, dilapidated and overgrown, across a seaside marsh and wades across the marsh to investigate it. She looks around, wondering why it seems so familiar to her, but gets trapped there by the rising tide. Toichi, a taciturn old fisherman, rescues her with his rowboat. On the way back across the water, Anna sees the house for a moment in good repair and well-lit. When she returns to the Oiwas, Setsu tells her that the mansion used to be a vacation home for some foreigners, but that it has been empty for a long time. That night and on other nights, Anna has a dream of seeing a blond girl in the mansion, having her hair brushed by an old woman.

On the night of the Tanabata festival, Anna discovers a rowboat by the shore and rows it across the marsh to the mansion, where she meets Marnie, the blond girl. Anna tells Marnie about her dreams, but Marnie assures Anna that she is not dreaming now. The two agree to keep their meeting secret, and they meet again on several evenings.

One evening, Marnie invites Anna to a party at the mansion. The house is filled with guests. Marnie disguises Anna as a local flower girl to get her into the party. While there, Anna sees Marnie dancing with a boy named Kazuhiko. Later some townspeople find Anna asleep by the post office. The next day, Anna returns to the mansion, but it appears abandoned and dilapidated again.

One week later, while sitting on the shore sketching in a sketchbook, Anna meets Hisako, an older woman who paints pictures of the marsh and the mansion. Hisako comments that Annas sketches look like a girl whom she knew when she was young, that her name was Marnie and she used to live in the mansion. Hisako also tells Anna that the mansion is being renovated because someone is moving in. Anna runs to the mansion, where she meets a girl named Sayaka. Sayaka has discovered Marnies diary hidden in the mansion. The diary includes an account of the party with the flower girl. Several pages are missing at the end of the diary.

The next day, Marnie reappears. Marnie tells Anna about how her parents are always traveling abroad, how they leave her in the mansion with her nanny and two maids, and how the nanny and maids abuse her physically and psychologically. Meanwhile, Sayaka has found the missing pages from Marnies diary, which include passages about Kazuhiko and a nearby abandoned feed silo. Sayaka and her brother head for the silo, thinking Anna may be there, and find her unconscious and feverish along the path.

When Anna recovers from her fever, Sayaka shows her the missing pages and a painting Hisako gave to Marnie. They seek out Hisako, who tells them Marnies story. Marnie eventually married Kazuhiko and had a daughter named Emily. Kazuhiko died while Emily was young; Marnie was institutionalized; and Emily was sent to boarding school. After Marnie was released, Emily blamed Marnie for abandoning her. Emily eventually ran away and had a daughter herself, but she and her husband were killed in a car accident when their daughter was one year old. Marnie tried to raise her granddaughter, but died herself a year later. The granddaughter was placed in foster care.

At the end of the summer, Yoriko goes to Kushiro to take Anna home. She gives Anna a photograph of the mansion and says it belonged to Annas grandmother. When Anna sees Marnies name written on the back, she realizes that she is Marnies granddaughter.

==Cast==
*  as  
*  as  
*  as   
*  as  
*  as Elderly  
*  as  
*  as  
*  as  
*  as  
*  as  
*  as  
*  as  
*  as  

===English dub cast=== Vanessa Williams, Grey Griffin, Ava Acres, and Raini Rodriguez have been announced as the voice cast for the upcoming English dub. 

==Music==

===Fine on the Outside===
{{Infobox single
| Name           = Fine on the Outside
| Cover          = fineontheoutside_cover.jpg
| Artist         = Priscilla Ahn
| Album          =  Digital download
| Released       =  
| Recorded       = 
| Genre          = Folk/Pop
| Length         = 11:43 Yamaha Music Communications
| Writer         = Priscilla Ahn
| Producer       = 
| prev           = 
| prev_no        = 
| track_no       = 
| next           = 
| next_no        = 
}}
"Fine on the Outside" is a single by American recording artist and musician Priscilla Ahn. It features the title song, "Fine on the Outside", the theme song of the 2014 Ghibli movie, "When Marnie was There",  as well as "This Old House", the theme song of the "When Marnie Was There x Yohei Taneda Exhibition" that was held at the Edo-Tokyo Museum from 27 July 2014 to 15 September 2014.  

====Background====
Priscilla Ahn, in common with the protagonist, saw herself as friendless and alone, and turned to music and other interests as a way to compensate. "...would literally sit on my bed and look out the window at night at the moon, and wonder if I was loved... if anyone would miss me if I was gone."  This led her to writing "Fine on the Outside" in 2005, but she had never released it due to the lyrics being too personal, and because she didnt want to have to change the song in any way to make it fit in with her other albums. A big fan of Ghibli, she read the original novel "When Marnie was There" after the announcement they were working on a movie adaptation, saw herself in Anna, and eventually decided to submit the song to Ghibli. The movies producer Yoshiaki Nishimura contacted her soon afterwards saying how much he loved the song, and it was later officially chosen as the theme song. 

====Release====
It was released in Japan as a CD single and a digital single on 2 July 2014. 

====Track listings====
#"Fine on the Outside"  – 4:12
#"This Old House" – 3:19
#"Fine on the Outside (Original Karaoke)"  – 4:12

===Just Know That I Love You===
{{Infobox album
| Name           = Just Know That I Love You
| Cover          = anatanokotogadaisuki_cover.jpg
| Artist         = Priscilla Ahn
| Album          =

| Released       =  
| Recorded       = 
| Genre          = Folk/Pop
| Length         = 36:00 Yamaha Music Communications
| Type        = studio
| Producer       = 
| prev           = 
| prev_no        = 
| track_no       = 
| next           = 
| next_no        = 
}}
"Just Know That I Love You", known as   in Japan,  is an album by American recording artist and musician Priscilla Ahn. It features theme songs, "Fine on the Outside" and "This Old House" that were featured on her previous single, as well as other "When Marnie Was There"-inspired songs written by Ahn. 

====Release====
It was released on CD in Japan, and in 113 countries worldwide (including Japan) as a digital download on the iTunes Store on 16 July 2014. 

====Track listings====
#"Fine on the Outside"  – 4:12
#"Deep Inside My Heart"  – 3:43
#"Pretty Dress"  – 2:23
#"I See You"  – 3:58
#"Marnie"  – 3:07
#"This Old House" – 3:18
#"With You"  – 3:45
#"Youre A Star"  – 3:45
#"Waltzing Memories"  – 3:31
#"I Am Not Alone"  – 4:12

===When Marnie Was There Soundtrack Music Collection===
{{Infobox album
| Name           = When Marnie Was There Soundtrack Music Collection
| Type           = Soundtrack
| Cover          = whenmarniewastheresoundtrack_cover.jpg
| Artist         = Takatsugu Muramatsu
| Album          =

| Released       =  
| Recorded       = 
| Genre          = Soundtrack
| Length         = 1:11:18 Studio Ghibli Tokuma Japan Communications
| Producer       = 
| prev           = 
| prev_no        = 
| track_no       = 
| next           = 
| next_no        = 
}}
"When Marnie Was There Soundtrack Music Collection", known as   in Japan,  is a two-disc soundtrack and image song album that was released on CD in Japan and in 113 countries worldwide (including Japan) as a digital download on the iTunes Store on 16 July 2014.  The first "Image Song" disc features music composed to express the personality of the characters and feel of places in the film. The second disc features all the background music for the film, as well as its ending theme song, "Fine On The Outside".

====Track listings====

===== Disc 1 =====
{{Track listing
| total_length  = 21:50
| title1 =  
| length1= 3:36
| title2 =  
| length2 =3:43
| title3 =  
| length3 = 3:38
| title4 =  
| length4 = 4:37
| title5 =  
| length5 = 2:23
| title6 =  
| length6 = 3:54
}}

===== Disc 2 =====
{{Track listing
| total_length  = 49:29
| title1 =  
| length1= 1:40
| title2 =  
| length2 =1:42
| title3 =  
| length3 = 2:00
| title4 =  
| length4 = 2:09
| title5 =  
| length5 = 0:23
| title6 =  
| length6 = 0:57
| title7 =  
| length7 = 0:43
| title8 =  
| length8 = 0:39
| title9 =  
| length9 = 0:59
| title10 =  
| length10 = 0:47
| title11 =  
| length11 = 3:25
| title12 =  
| length12 = 1:47
| title13 =   
| length13 = 1:14
| title14 =  
| length14 = 1:45
| title15 =  
| length15 = 2:22
| title16 =  
| length16 = 1:57
| title17 =  
| length17 = 1:14
| title18 =  
| length18 = 0:37
| title19 =  
| length19 = 2:43
| title20 =  
| length20 = 1:21
| title21 =  
| length21 = 3:38
| title22 =  
| length22 = 0:57
| title23 =  
| length23 = 0:46
| title24 =  
| length24 = 2:52
| title25 =  
| length25 = 3:13
| title26 =  
| length26 = 1:26
| title27 =  
| length27 = 1:58
| title28 = Fine On The Outside
| note28 = Words and music written by Priscilla Ahn
| length28 = 4:14
}}

==Reception==
The film opened at third place, grossing Japanese yen|¥379 million during its opening weekend in Japan.  By its fourth weekend, it had earned ¥2.08 billion,  made an additional ¥930 million in its next two weekends,  and had a total of ¥3.63 billion by its eighth weekend. 

==Release==
The film was released in Japan on 19 July 2014. It was released on Blu-Ray and DVD in Japan on 18 March 2015. On 14 January 2015, GKIDS announced that they will be distributing the film for a North American release.  The film was premiered at the New York International Childrens Film Festival on February 27, 2015. 

==References==
 

==External links==
 
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 