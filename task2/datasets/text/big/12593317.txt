Plácido
 
{{Infobox Film
| name           = Plácido
| image          = Plácido (film).jpg
| caption        = Spanish film poster
| director       = Luis García Berlanga
| producer       = Alfredo Matas
| writer         = Luis García Berlanga Rafael Azcona José Luis Colina José Luis Font Cassen Amparo Soler Leal Xan Das Bolas Félix Dafauce
| music          = Miguel Asins Arbó
| cinematography = Francisco Sempere
| editing        = José Antonio Rojo
| distributor    = 
| released       = 3 November 1960
| runtime        = 85 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}} Best Foreign Language Film.    It was also entered into the 1962 Cannes Film Festival.   

==Cast== Cassen - Plácido Alonso (as Casto Sendra Cassen)
* José Luis López Vázquez - Gabino Quintanilla
* Elvira Quintillá - Emilia
* Manuel Alexandre - Julián Alonso
* Mario Bustos - (as Mario de Bustos)
* María Francés
* Mari Carmen Yepes - Martita (as Carmen Yepes)
* Jesús Puche - Don Arturo
* Roberto Llamas
* Amelia de la Torre - Doña Encarna de Galán
* Juan G. Medina
* José María Caffarel - Zapater
* Xan das Bolas - Rivas
* Laura Granados - Erika
* Juan Manuel Simón

==See also==
* List of submissions to the 34th Academy Awards for Best Foreign Language Film
* List of Spanish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 