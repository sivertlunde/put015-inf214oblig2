God's Little Acre (film)
{{Infobox film
| name           = Gods Little Acre
| producer       = Sidney Harmon
| image	         = Gods Little Acre FilmPoster.jpeg
| director       = Anthony Mann
| writer         = Erskine Caldwell
| based on       = Gods Little Acre
| screenplay     = Philip Yordan Ben Maddow
| starring       = Robert Ryan Aldo Ray Jack Lord Tina Louise Buddy Hackett
| music          = Elmer Bernstein
| cinematography = Ernest Haller
| editing        = Richard Meyer
| studio         = Security Pictures
| distributor    = United Artists
| country        = United States
| released       =  
| runtime        = 110 minutes
| language       = English}}
 novel of the same name.    It was directed by Anthony Mann and shot in black and white by master cinematographer Ernest Haller.
 Marxist insurrection southern United millworkers trying to gain control of the factory equipment on which their jobs depended.
 Red Scare, working without credit was the only way he could successfully submit screenplays. When it was first released, audiences under eighteen years of age were prohibited from viewing what were perceived to be numerous sexy scenes throughout, though in recent decades the films scandalous reputation has diminished. After decades of neglect, the film was restored by the UCLA Film and Television Archive under the supervision of master restorer Robert Gitt. As part of Gitts restoration, Yordans name was replaced by Maddows in the main title roll.

==Plot== Georgia during the Great Depression. While Ty Ty searches for gold on his farm, his son-in-law Will (Aldo Ray) cheats on his wife Rosamund (Helen Westcott) with Griselda (Tina Louise). Ty Ty has been digging for gold on his land for 15 years. Pluto Swint (Buddy Hackett) arrives to announce hes running for sheriff. Swint is invited to come around back where Darlin Jill (Fay Spain) is taking a bath in an outdoor bathtub positioned near a handpump and spigot. She asks him to pump some more water (the camera never dips lower than the top of the tub). He is asked to keep his eyes closed, but takes a pleasurable peek.

Ty Ty spends most of his time digging holes, constantly searching for the treasure his grandfather left him. Consequently the farm has suffered from years of neglect. He could have turned a profit any time by buying and planting seed in the fields, but he thinks the gold is just another dig away. (In the book, Ty Ty remarks to Swint that gold nuggets have been found on his land here and there, but never more than a nugget or two. In the movie, his constant searching for buried gold appears to be making his farm worth less with each coming year.) The seductive lure of easy gold a mere shovel or two away is actually leading him to squander his inheritance. If his real treasure lies in his daughters, the viewer is invited to gauge the depth of that genealogical reserve, inasmuch as Darlin Jill appears to be a woman of equally easy morals.

In the belief that having an albino with him in his quest for treasure will bring him great fortune, Ty Ty transports and wrongfully imprisons Dave Dawson (Michael Landon), a man with white hair and pink eyes, demanding that he help him locate the buried treasure. Ty Ty reasons that it is not actually wrongful detainment, because the albino at any time could lead him to the treasure and thereby earn his release. In anticipation of a potential find he changes the location of a plot of land allocated to the church in a tithing relationship (known as Gods Little Acre, hence the title), in order to keep any gold found there for himself.

He moves the plots marker, a large, ramshackle cross, to the side of the house. but lo and behold that is exactly where the divining rod in the hands of the albino comes to rest, supposedly indicating where the gold is buried. However, seeing that the divining rod was pulling the albino towards the side of the house, Ty Ty had again pulled the marker out of the ground explaining that God told him to move it, thereby absolving him from giving any gold found in this new spot to the church. He replants the marker farther away on the edge of a pond for it "to be cooler". In the middle of the night Will leaves his house, followed by Griselda. He breaks open the mills gates and enters the property. At first she distracts him from his purpose and they kiss. He then escorts her back to the gates and asks the growing (but in the film relatively silent) crowd to restrain her. He reenters and turns on the power, and the machines reactivate to the cheers of the crowd.  

Hearing the rioters assembly and the mills power turned on, the caretaker comes from an inside office and shoots Will for trespassing. He staggers outside and the crowds cheers turn to silence. They carry his body back to his house. Griselda enters to tell Rosamund the bad news, but the other cries out that she already knows what has happened. Implicitly, at this point the tide in the county elections turns irreversibly. The populist Pluto Swint is elected sheriff, replacing the incumbent. The Walden family squabble after Wills funeral, particularly over Griseldas actions at the mill, and the numerous affections which she attracts from the other sons. The family decides to give up searching for the gold. The film ends with the family  contentedly plowing for the first time in years. Ty Ty finds the blade of an old shovel in the ground, and speculates about whether the gold might lie in that spot. As he begins digging again, the camera pans to the pond and the final resting place of the marker for Gods Little Acre.

==Cast==
* Robert Ryan as Ty Ty Walden, a widower
* Aldo Ray as Will Thompson
* Tina Louise as Griselda Walden (Bucks wife)
* Buddy Hackett as Pluto Swint
* Jack Lord as Buck Walden
* Fay Spain as Darlin Jill Walden (Ty Tys unmarried daughter)
* Vic Morrow as Shaw Walden
* Helen Westcott as Rosamund Thompson (Wills wife)
* Lance Fuller as Jim Leslie Rex Ingram as Uncle Felix
* Michael Landon as Dave Dawson, the albino

==Copyright registration and renewal==
Gods Little Acre was registered (LP 10695) for copyright 9 May 1958 by Security Pictures, Inc. as copyright owner  and renewed under RE 304-495 25 September 1986 by MPH Films as proprietor of copyright in a work made for hire.  The black and white version is Public Domain, the colorized version is Copyright. 

==Notes==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 