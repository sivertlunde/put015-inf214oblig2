A Sort of Homecoming (film)
{{Infobox film
| name           = A Sort of Homecoming
| image          = 
| caption        =
| director       = Maria Burton
| producer       = Marcus Lyle Brown Yvette Marie Brown
| writer         = Lynn Reed
| starring       = Laura Marano Parker Mack Katherine McNamara Michelle Clunie Kathleen Wilhoite
| music          = Andrew Morgan Smith
| cinematography = Arlene Nelson
| editing        = Lai-San Ho Susan Vaill
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} directorial debut. The films stars Laura Marano, Parker Mack, Katherine McNamara, Michelle Clunie and Kathleen Wilhoite. The film was released March 14, 2015.

==Plot==
The protagonist, Amy (Michelle Clunie), returns to her home town in Louisiana from her career in New York upon request of her high school debate coach. She recalls her final year, the politics surrounding debate competitions and her relationship with Nick (Parker Mack), her debate partner.

==Production==
Production for the movie began in February and while a few scenes were shot New Orleans, the majority were shot in Lafayette, Louisiana, making it the second Lafayette-based feature film completed by Holbrook Multi Media this year.

==Cast==
* Laura Marano as Young Amy
* Katherine McNamara as Rose
* Kathleen Wilhoite as Annie
* Jaqueline Fleming as Val	
* Michelle Clunie as Adult Amy
* Lara Grice as Amys mother
* Wayne Pére as Adam
* Ashlynn Ross as Melanie
* Shayne Top] as Dylan
* Morganna May as Susan Levine
* Lance E. Nichols as Hal
* Parker Mack as Nick
* Ritchie Montgomery as Amys Father
* Marcus Lyle Brown as Keith
* Jim Gleason as Bill Tarrity

==Awards and nomination==
{|class="wikitable sortable"
|- Year   Award
!scope="col"|Category Nominee
!scope="col"|Result.
|- 2012
|WorldFest Houston Dramatic Original
|Lynn Reed
|  
|- 2015
|Remi Awards Rising Actress
|Laura Marano
|   
|}

==References==
 

==External links==
*  

 
 
 
 
 
 