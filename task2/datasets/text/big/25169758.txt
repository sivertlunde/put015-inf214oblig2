Mommy 2: Mommy's Day
{{Infobox Film
| name           = Mommy 2: Mommys Day
| image          = 
| caption        = 
| director       = Max Allan Collins
| producer       = Steven Henke
| writer         = Max Allan Collins
| starring       = Patty McCormack Sarah Jane Miller Rachel Lemieux Paul Petersen Gary Sandy Brinke Stevens Michael Cornelison Mickey Spillane Arlen Dean Snyder
| music          = Richard Lowry
| cinematography = Phillip W. Dingeldein
| editing        = Phillip W. Dingeldein	
| distributor    = M.A.C. Productions
| released       =  
| runtime        = 87 min
| country        = United States
| language       = English
| budget         = 
}}
 thriller Mommy (1995 film)|Mommy starring Patty McCormack once again as the psychotically obsessed mother who is trying to reunite with her daughter.

==Plot==

In this sequel to 1995s Mommy, Mrs. Sterling (Patty McCormack), who had been convicted of murder and sentenced to death, escapes execution when she is wounded in a prison breakout attempt. While she is recovering, a physician convinces the courts that she would be able to live in a halfway house if she takes a violence-suppressing drug that he designed. Although her parole forbids contact with her daughter (who is now living with her sister and brother-in-law), she violates her parole by secretly visiting her one night. When people who are trying to keep her from seeing her daughter start turning up murdered, a local cop starts investigating her.

==Cast==

*Patty McCormack ...  Mommy/Mrs. Sterling
*Sarah Jane Miller ...  Jolene Jones
*Rachel Lemieux ...  Jessica Ann 
*Paul Petersen ...  Paul Conway 
*Gary Sandy ...  Sgt. Anderson 
*Brinke Stevens ...  Beth 
*Mickey Spillane ...  Attorney Neal Ekhardt 
*Arlen Dean Snyder ...  Lt. March 
*Michael Cornelison ...  Dr. Price 
*Todd Eastland ...  Jerry 
*Paula Sands ...  Paula Sands 
*Del Close ...  Warden 
*Laurence Coven ...  Dr. Stern

==External links==
* 

 
 
 
 


 