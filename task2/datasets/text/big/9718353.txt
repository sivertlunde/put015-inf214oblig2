The Browning Version (1994 film)
 
 
{{Infobox film
| name           = The Browning Version
| image          = Browning version.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Mike Figgis
| producer       = Ridley Scott Mimi Polk Gitlin
| writer         = Terence Rattigan (play) Ronald Harwood
| narrator       = 
| starring       = Albert Finney Greta Scacchi Matthew Modine Julian Sands
| music          = Mark Isham
| cinematography = Jean-François Robin
| editing        = Hervé Schneid
| distributor    = Paramount Pictures
| released       = 8 June 1994
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = $7 million
| gross          = $487,391 
| preceded_by    = 
| followed_by    = 
}} 1948 play under the same name in 1951.

==Plot==
Andrew Crocker-Harris (Albert Finney) is a veteran teacher of Greek and Latin at a British Public school. After nearly 20 years of service, he is being forced to retire on the pretext of his health, and perhaps may not even be given a pension. He is disliked or ignored by the other teachers and while his pupils fear his relentlessly strict discipline, they are bored by his dictatorial but dreary and un-inspiring teaching methods. His younger wife Laura (Greta Scacchi), whom he has sexually and emotionally neglected, is unfaithful, and now lives to wound him any way she can. She is having an affair with Frank (Matthew Modine), an eager, young American science teacher who is highly popular with his pupils, much more lenient with class-room rules yet is able connect with the students. In his final class, Andrew, whilst reading from a Greek play, finally shows some genuine passion about the subject, giving a glimpse at the teacher he could have been. Andrews nervous new replacement Tom (Julian Sands) expresses his awe at the ironclad control that the former exerts over his classes, but Andrew advises his young colleague not to follow his example.

As his retirement at the end of the school term draws near, Andrew is approached by a quiet and sensitive pupil named Taplow who has detected the unhappiness and loneliness of his teacher and makes an attempt to reach out to him, saying that Andrews Latin teachings have inspired him. Taplow gives Andrew a gift- a rare copy of an early edition of the Browning Version- the 1877 translation by Robert Browning of Aeschylus|Aeschylus’ ancient play Agamemnon (play)|Agamemnon. Touched by this gesture, Andrews emotional guard begins to be let down for the first time. Increasingly aware of Andrews isolation, Frank feels guilty about the affair with Laura and ends the relationship. Shortly before the end-of-term school assembly in which Andrew will make his farewell speech, Laura tells her husband that she wants their marriage to end and that she intends to leave him.

The schools senior staff want Andrew to make his speech first, to be followed by the farewell speech of a younger, more popular teacher who is leaving to pursue a career as a cricket player. But Andrew insists on going second, even though the headmaster angrily says that it will give the ceremony an anti-climax. To the surprise of everyone, including Laura who has lingered to watch the event, Andrews speech is highly emotional and revelatory, apologising for his failures both as a teacher and as a person. Moved by the speech, the pupils and staff give Andrew a huge applause.

Andrew, as a parting gesture of gratitude, tells Taplow that he has organised a place for him in Franks science class which the pupil had been eager to join. Laura has a newfound sense of respect for her husband and the two part on good terms. As he watches Laura drive away, Andrew sadly but calmly faces the next phase of his life.

==Filming location==
The interior and exterior scenes in The Browning Version were filmed at Milton Abbey School and Sherborne School, two boys independent schools in Dorset, in southern England.  

==Awards==
Nominations: Cannes Film Festival (1994)    
*Best Screenplay, BAFTA Awards (1995)
Wins:
*Best Actor (Albert Finney), Boston Society of Film Critics Awards (1994)

==Cast==
*Albert Finney as Andrew Crocker-Harris
*Greta Scacchi as Laura Crocker-Harris
*Matthew Modine as Frank Hunter
*Julian Sands as Tom Gilbert
*Michael Gambon as Dr. Frobisher
*Ben Silverstone as Taplow
*Jim Sturgess as Bryant (as James Sturgess)
*Joseph Beattie as Wilson
*Mark Bolton as Grantham
*Tom Havelock as Laughton
*Walter Micklethwait as Buller
*Jotham Annan as Prince Abakendi
*David Lever as David Fletcher
*Bruce Myers as Dr. Rafferty
*Maryam dAbo as Diana
*Peter Hetherington as Williams
* Nick Cracknell as Nick
* Trevor Slack as Boy

==Reception==
The Browning Version received positive reviews from critics, as the film holds an 82% rating on Rotten Tomatoes.

==References==
 

==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 