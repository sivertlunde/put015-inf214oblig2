Inside I'm Dancing
 
 
{{Infobox film
| name           = Rory OShea Was Here
| image          = Rory_oshea_was_here.jpg
| image_size     = 215px
| alt            =  
| caption        = US release poster
| director       = Damien ODonnell
| producer       = James Flynn Juanita Wilson
| screenplay     = Jeffrey Caine
| story          = Christian OReilly
| starring       = James McAvoy Steven Robertson Romola Garai Brenda Fricker
| music          = David Julyan
| cinematography = Peter J. Robertson
| editing        = Fran Parker WT 2  Productions Universal Pictures Momentum Pictures
| released       =  
| runtime        = 104 minutes   
| country        = Ireland United Kingdom France
| language       = English
| budget         =
| gross          = $1,226,577  
}} disabled young men who pursue physical and emotional independence in direct defiance of "protective" institutional living and their societys prevailing standards and attitudes, especially pity.

The production was filmed in Dublin and Wicklow and involved production companies Working Title Films and StudioCanal with assistance from the Irish Film Board (Bord Scannán na hÉireann).
 physical disabilities, leading some to criticise it for casting non-disabled actors in to disability roles. This criticism is especially poignant given that the character of Michael is portrayed as having a rather pronounced version of athetoid cerebral palsy when in fact the actor who portrays him is entirely physically normal.

==Plot==
 
 
Michael Connolly is a 24-year-old with cerebral palsy who is a long-term resident of the Carrigmore Residential Home for the Disabled run by the formidable Eileen. His life, mundane and schedule-driven by the institutions authorities, is transformed when the maverick Rory OShea, who suffers from Duchenne muscular dystrophy, suddenly moves in. Michael is stunned to discover that fast talking Rory, prone to lewd and/or overt jokes at unpredictable intervals, and who can move only his right hand, can understand his almost unintelligible speech. Rorys dynamic and rebellious nature soon sparks a flame in Michael, introducing him to the wider world outside of Carrigmore.

On a day out in Dublin led by Carrigmore collecting for "the needs of the disabled", Rory leads Michael astray, sneaking off to a local pub with their donation bucket, charming a group of girls at a corner table at which sits Siobhán; later they see Siobhán again while traversing a neighbouring nightclub, which they get into only by Michael citing Irish and EU Disability Discrimination Law text to the Bouncer (doorman)|bouncer.

Rory has meanwhile repeatedly failed in his application for the Independent Living Allowance; he is always denied that on the grounds of irresponsibility and poor judgement, but told to reapply in six months. Inspired by his example Michael applies for the allowance himself; with the help of Rory as his interpreter Michael gets the allowance but they struggle to find an apartment that is both wheelchair accessible and affordable. Rory convinces Michael to visit his estranged father who, out of guilt, gives them the money and property they need to set up on their own.
 joyride with local kids in a stolen car, crashing it, subsequently getting briefly detained by the police.

Siobhán invites Rory and Michael along to a costume party; but after Siobhán rejects Michaels advances she decides to bring in Peter, a qualified Personal Assistant to replace herself. Michael is distraught over Siobháns departure from their lives, and considering suicide; Rory finds him on the edge of the James Joyce Bridge. Michael jokingly complains the edge is too high for him to throw himself off, and Rory talks him out of it, reminding him he has a future and to enjoy it.

Later, Michael finds Rory in his bed struggling to breathe, and calls for an ambulance. His disease having progressed, Rory is given only a few days to live. Michael visits Siobhán and with her help goes to the review board on behalf of Rory to argue his case, another chance having come up after six months.  The board initially refuse, restating the same arguments, however Michael responds that "the right must exist independently of its exercising" and as a gesture the board approve Rorys independent living allowance in principle – but before they can get to the hospital to tell Rory the news, he has already died.

The film ends with Michael and Siobhan attending Rorys funeral. Michael hears in his mind Rorys words "Well, then, are we going out?" and we then see Michael heading out on his own after saying goodbye to Siobhan.

==Cast==
 
* James McAvoy as Rory Gerard OShea, a rebellious young man with Duchenne muscular dystrophy
* Steven Robertson as Michael Connolly, a 24-year-old long-term resident with cerebral palsy
* Romola Garai as Siobhán, a young woman they employ as a caregiver
* Brenda Fricker as Eileen, a domineering care worker at Michaels care home
* Gerard McSorley as Fergus Connolly, Michaels father who abandoned him at birth Tom Hickey as Con OShea, Rorys father
* Alan King as Tommy
* Ruth McCabe as Annie
* Anna Healy as Alice
* Deirdre OKane (Cameo appearance|cameo) as a caregiver applicant

== Production ==
Writer Christian OReilly came up with the story after working with Martin Naughton, and Dermot Walsh, on a campaign for the Centre for Independent Living in Dublin.   

==Music==
The film features Johnny Cashs cover version of "Hurt (Nine Inch Nails song)|Hurt". Siobhan asks Rory "Were you born like this ... dodgy hair and shit taste in music?" and plays the Cash song for him. 
 Elbow
* "Frontier Psychiatrist" – The Avalanches
* "Hurt" (Trent Reznor of the Nine Inch Nails) – Johnny Cash
* "Look of Love" – Dusty Springfield
* "Easy" – Groove Armada

==Critical response==
 
Review aggregation website Rotten Tomatoes gives the film a score of 49% based on 69 reviews, with a rating average of 5.8 out of 10. 
{{cite web
| url = http://www.rottentomatoes.com/m/rory_o_shea_was_here/
| title= Rory OShea Was Here (2005)
| work = Rotten Tomatoes
}} 
The sites general consensus is that "The dramatic aspects of Rory OShea Was Here veer into mawkish, formulaic sentiment, which undercuts the characters individuality." {{cite web
| url = http://www.rottentomatoes.com/m/rory_o_shea_was_here/?critic=creamcrop 
| title= Rory OShea Was Here (2005)
| work = Rotten Tomatoes
}} 

Metacritic, which assigns a weighted average score gives the film 59% based on 23 reviews. 
Neil Smith gave the film 4/5 stars but felt casting non-disabled actors undermined the film.  {{cite news
| date = 13 October 2004
| author = Neil Smith
| title = BBC – Films – Inside Im Dancing
| url = http://www.bbc.co.uk/films/2004/10/07/inside_im_dancing_2004_review.shtml
| archiveurl =
| archivedate =
}}
 
 
 inclusion communities have mixed opinions on the film.  It is not always cited, for example, as a top pick in film festival events led by and for disabled people. These critics sometimes imply that the film does not quite go far enough in lampooning and dismantling pity/heroism attitudes, and that some sectors of the film (mostly in relation to camera angles and music) actually play into those attitudes, possibly without realising it.  Similarly to other critics, critics with disabilities also point out that there was no readily apparent reason to exclude actors with disabilities from consideration from the two lead roles. Neither of the two lead actors is actually physically disabled, when in fact at a minimum, it would have been a relatively easy exercise for the filmmakers to have at least located one young, skilled actor with actual cerebral palsy to play the role of Michael.

==References==
 
* http://www.bbc.co.uk/films/2004/10/07/inside_im_dancing_2004_review.shtml

==External links==
*   official site, archived copy from 2006 provided by the Internet Archive
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 