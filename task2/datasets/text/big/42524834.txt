Jauja (film)
 
{{Infobox film
| name           = Jauja
| image          = Jauja poster.jpg
| caption        = Theatrical release poster
| director       = Lisandro Alonso
| producer       = {{Plainlist|
* Ilse Hughan
* Andy Kleinman
* Viggo Mortensen
* Sylvie Pialat
* Jaime Romandia
* Helle Ulsteen}}
| writer         = {{Plainlist|
* Fabian Casas
* Lisandro Alonso}}
| starring       = Viggo Mortensen
| music          = Viggo Mortensen 
| cinematography = Timo Salminen 
| editing        = {{Plainlist|
* Gonzalo del Val
* Natalia López}}
| distributor    = NDM
| released       =  
| runtime        = 110 minutes  
| country        = {{Plainlist|
* Denmark
* Argentina
* France
* Mexico
* Netherlands
* United States}}
| language       = {{Plainlist|
* Danish
* Spanish}}
| budget         = 
| gross          =
}}
Jauja is a 2014 Danish drama film directed by Lisandro Alonso. It competed in the Un Certain Regard section at the 2014 Cannes Film Festival       where it won the FIPRESCI Prize. 

==Cast==
* Viggo Mortensen as Gunnar Dinesen
* Ghita Nørby
* Viilbjørk Malling Agger
* Adrian Fondari
* Esteban Bigliardi
* Brian Patterson

==Reception==
Slant magazines Jake Cole gave the film three and-a-half stars and described it as "refreshingly absurdist". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 