Kamome Shokudo
{{Infobox Film
| name           = Kamome Shokudō
| image          = Visit-suomi-2009-05-by-RalfR-062.jpg
| caption        = 
| director       = Naoko Ogigami
| producer       = Mayumi Amano
| writer         = Naoko Ogigami Jarkko Niemi, Markku Peltola
| music          = Tatsuro Kondo
| cinematography = Tuomo Virtanen
| editing        = Shinichi Fushima
| distributor    = Nippon Television Network Corporation 2006
| runtime        = 102 min. Finnish
| budget         = 
| preceded_by    = 
| followed_by    = 
| tv_rating      = 
}}
 2006 comedy Japanese film director Naoko Finnish capital Helsinki, and follows a Japanese woman who sets up a diner serving Japanese food in the city, and the friends she makes in the process.

Cast members include: Hairi Katagiri (Midori), Satomi Kobayashi (Sachie, the shopkeeper), Masako Motai (Masako), Markku Peltola, Tarja Markus (Liisa), and Jarkko Niemi (Tommi).

==Plot==
Sachie is a Japanese woman living alone in Helsinki, who is trying single-handedly to establish a new cafe serving Japanese-style food.  However, it has no customers.  Eventually a young Finnish anime enthusiast comes for coffee and becomes the cafes first regular, though as her first customer he gets to eat and drink there for free.

Midori is a Japanese woman who has just arrived in Finland for an indefinite time and without any definite plans.  She and Sachie happen to meet in a bookstore and she starts to help out in the cafe. Later, Masako, another Japanese woman on her own, turns up.  Her baggage has been lost by an airline, and before long she too starts to work in the cafe. Over the course of the film, the cafe gradually gains more customers, and the Japanese women make more friends with the local people. 
 
==Reception==
It was the 5th Best Film at the 28th Yokohama Film Festival. 

==Tourist attraction==
The film was filmed in a real cafe in downtown Helsinki, at the address Pursimiehenkatu 12, its real name being Kahvila Suomi (Finland Cafe). However, the original decor of the cafe was taken out for the filming, replaced by Finnish designer furniture. The original interior was returned afterwards. A poster from the film remains in the window of the cafe and it has become a popular tourist attraction for Japanese tourists. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 