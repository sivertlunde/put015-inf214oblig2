Heart's Desire (1917 film)
{{Infobox film
| name           = Hearts Desire
| image          =
| alt            = 
| caption        =
| director       = Francis J. Grandon
| producer       = 
| screenplay     = Shannon Fife Eve Unsell
| starring       = Marie Doro Alan Roscoe Mario Majeroni Jean Del Val Helen Dahl Harry Lee
| music          = 
| cinematography = Larry Williams 
| editing        = 
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent film directed by Francis J. Grandon and written by Shannon Fife and Eve Unsell. The film stars Marie Doro, Alan Roscoe, Mario Majeroni, Jean Del Val, Helen Dahl and Harry Lee. The film was released on April 30, 1917, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Marie Doro as Fleurette
*Alan Roscoe as Paul Le Roque 
*Mario Majeroni as Henri Le Roque
*Jean Del Val as Jacques 
*Helen Dahl as Helen St. Simon
*Harry Lee as Le Roques Secretary
*Gertrude Norman as Mother Mathilde
*Ida Darling 	
*Eddie Sturgis

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 