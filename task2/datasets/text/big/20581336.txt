Shehzada
 
 

{{Infobox film
| name           = Shehzada
| image          = Shehzada.jpg
| image_size     =
| caption        = 
| director       = K. Shankar
| producer       = 
| writer         =
| narrator       = 
| starring       =Rajesh Khanna and Raakhee
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 8 September 1972
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1972 Bollywood drama film directed by K. Shankar, set during the British Raj. The film stars Rajesh Khanna and Raakhee as the lead pair and supporting cast included Veena, Pandari Bai and Karan Dawan.The music was given by R.D.Burman.

==Cast==
*Rajesh Khanna ...  Rajesh
*Raakhee ...  Chanda Veena ...  Rajlaxmi
*Karan Dewan ...  Ratan
*Pandharibai ...  Janki Sunder ...  Nekiram
*Mohan Choti ...  Nandu
*Madan Puri ...  Chandas maternal uncle
*P. Jairaj
*Sonoo Arora
*Gulshan Bawra
*Badri Prasad ...  Police Inspector Halam
*Varalakshmi 
*Shreeram Shastri

==Reception==

Shehzada grossed 19&nbsp;million in 1972 at the box-office and was declared "Semi-Hit". 

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Rajendra Krishna
| all_music       = Rahul Dev Burman

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Rimjhim Rimjhim Dekho Baras Rahee Hai
| note1           =
| writer1        =
| lyrics1         =
| music1          =
| extra1          = Kishore Kumar, Lata Mangeshkar, Rajesh Khanna
| length1         = 3.20

| title2          = Naa Jaeeyo, Naa Jaeeyo, Chhod Ke Naa
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Kishore Kumar, Lata Mangeshkar, Rajesh Khanna
| length2         = 3.20

| title3          = Thokar Mein Hai Meree Saaraa Zamaanaa
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar
| length3         = 4.17

| title4          = Pariyon Kee Nagaree Se Aayaa Chunnoo
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Kishore Kumar
| length4         = 4.14

| title5          = Ho Tere Atharoo Chun Levaan Main
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Mohammad Rafi
| length5         = 5.00 
}}

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 