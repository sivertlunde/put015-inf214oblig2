Thunderbolt and Lightfoot
{{Infobox film name = Thunderbolt and Lightfoot image = Thunderbolt_and_Lightfoot_movie_poster.jpg caption = Theatrical release poster, version #2  director = Michael Cimino producer = Robert Daley writer = Michael Cimino starring = Clint Eastwood Jeff Bridges music = Dee Barton cinematography = Frank Stanley editing = Ferris Webster studio = The Malpaso Company distributor = MGM (2000, DVD) Twilight Time (under license from MGM) (2014, Blu-ray DVD) released =   runtime = 115 minutes country = United States language = English budget = $4 million Hughes, p.166  gross = $25,000,000 
}} Geoffrey Lewis.

==Plot==
As a young neer-do-well named Lightfoot (Jeff Bridges) steals a car, an assassin attempts to shoot a minister delivering a sermon at his pulpit. The preacher escapes on foot. Lightfoot, who happens to be driving by, inadvertently rescues him by running over his pursuer and giving the preacher a lift.

Lightfoot eventually learns that the "minister" is really a notorious bank robber known as "The Thunderbolt" (Clint Eastwood) for his use of a 20 millimeter cannon to break into a safe. Hiding out in the guise of a clergyman following the robbery of a Montana bank, Thunderbolt is the only member of his old gang who knows where the loot is hidden.

After escaping another attempt on his life by two other men, Thunderbolt tells Lightfoot that the ones trying to kill him are members of his gang who mistakenly thought Thunderbolt had double-crossed them. He and Lightfoot journey to Warsaw, Montana to retrieve the money hidden in an old one-room schoolhouse. They discover the schoolhouse has been replaced by a brand-new school standing in its place.
 Geoffrey Lewis)—and driven to a remote location where Thunderbolt and Red fight each other, after which Thunderbolt explains how he never betrayed the gang.

Lightfoot proposes another heist—robbing the same company as before—with a variation on the original plan, since Lightfoot inadvertently killed their electronics expert, Dunlap, in his rescue of the fleeing preacher. In the city where the bank is located, the men find jobs to raise money for needed equipment while they plan the heist.

The robbery begins as Thunderbolt and Red gain access to the building. Lightfoot, dressed as a woman, distracts the Western Union offices security guard, deactivates the ensuing alarm, and is picked up by Goody. Using an anti-tank cannon to breach the vaults wall, as they did in the first heist, the gang escapes with the loot. They flee in the car, with Red and Goody in the trunk, to a nearby drive-in movie in progress. Upon seeing a shirt tail protruding from the cars trunk lid, the suspicious theater manager calls the police and a chase ensues. Goody is shot and Red throws him out of the trunk onto a dirt road, where he dies.

Red then forces Thunderbolt and Lightfoot to stop the car. He pistol-whips them both, knocking them unconscious, and kicks Lightfoot violently in the head. Red takes off with the loot in the getaway car but is again pursued by police, who shoot Red several times, causing him to lose control of the car and crash through the window of a department store, where he is attacked and killed by the stores vicious watchdogs.

Escaping on foot, Thunderbolt and Lightfoot hitch a ride the next morning and are dropped off near Warsaw, Montana, where they stumble upon the one-room schoolhouse—now a historical monument on the side of a highway—moved there from its original location in Warsaw after the first heist. As the two men retrieve the stolen money, Lightfoots behavior becomes erratic as a result of the beating.

Thunderbolt buys a new Cadillac convertible with cash, something Lightfoot said he had always wanted to do, and picks up his waiting partner, who is gradually losing control of the left side of his body. As they drive away celebrating their success with cigars, Lightfoot, in obvious distress, tells Thunderbolt in a slurred voice how proud he is of their accomplishments, and slumps over dead.

Thunderbolt snaps his cigar in half (as there no longer is a celebration), and with his dead partner beside him, he drives off down the highway into the distance.

==Cast==
*Clint Eastwood as Thunderbolt
*Jeff Bridges as Lightfoot
*George Kennedy as Red Leary Geoffrey Lewis as Eddie Goody
*Gary Busey as Curly
*Catherine Bach as Melody
*Jack Dodson as Bank Manager
*Gregory Walcott as Car Salesman
*Vic Tayback as Construction Company Owner
*Bill McKinney as Crazed Car Driver w/raccoon and rabbits
*Burton Gilliam as Welder
*Beth Howland as Bank Managers Wife
*Dub Taylor as Gas Station Attendant
*Claudia Lennear as Secretary
*June Fairchild as Gloria

==Production==

===Development & Screenplay=== speculation with Eastwood in mind.  Due to the great financial success of Dennis Hoppers Easy Rider, road pictures were a popular genre in Hollywood. Eastwood himself wanted to do a road movie. Eliot, p. 154  Agent Leonard Hirshan brought the script to Eastwood from fellow agent Kamen. Eliot, p. 153  Reading it, Eastwood liked it so much that he originally intended to direct it himself. However, on meeting Cimino, he decided to give him the directing job instead, giving Cimino his big break and feature-film directorial debut.  Cimino later said that if it was not for Eastwood, he never would have had a career in film.  Cimino patterned Thunderbolt after one of his favorite 50s films, Captain Lightfoot. 

===Shooting=== Robert Daley Great Falls Fort Benton, Augusta and Fort Benton, Wolf Creek, Great Falls,  and Hobson, Montana|Hobson. St. Johns Lutheran Church in Hobson was used for the opening scene.

Eastwood did not like to do any more than three takes on any given shot, according to co-star Bridges. "I would always go to Mike and say I think I can do one more. I got an idea. And Mike would say I gotta ask Clint. Clint would say, Give the kid a shot." Jeff Bridges|Bridges, Jeff (actor); Okun, Charles ("Cimino production manager, 64-78"); Willem Dafoe|Dafoe, Willem (narrator); Epstein, Michael (director). (2004). Final Cut: The Making and Unmaking of Heavens Gate.  . Viewfinder Productions.  Charles Okun, first assistant director on Thunderbolt, added, "Clint was the only guy that ever said no. Michael said OK, lets go for another take. It was take four, Clint would say No we got enough. We got it.   And if   took too long to get it ready,   would say, Its good, lets go."  

==Release==
Thunderbolt was released on May 23, 1974. The film grossed $9 million on its initial theatrical release and eventually grossed $25 million overall. Eliot, p. 155  The film did respectable box office business, and the studio profited, but Clint Eastwood vowed never to work with the movies distributor United Artists again due to what he felt was bad promotion of it.  Hughes, p.170  According to author Marc Eliot, Eastwood perceived himself as being upstaged by Bridges. 

Given that for Eastwood this was an offbeat film, Franks Wells of Warner Brothers refused to back Malpaso in the production, leaving him to turn to United Artists and producer Bob Daley. McGilligan (1999), p. 239  Eastwood was unhappy with the way that United Artists had produced the film and swore "he would never work for United Artists again", and the scheduled two film deal between Malpaso and UA was cancelled. McGilligan, p. 241 

==Reception==
Jay Cocks of Time (magazine)|Time called the film "one of the most ebullient and eccentric diversions around."  Leonard Maltin gave the film three out of four stars, describing it as "Colorful, tough melodrama-comedy with good characterizations; Lewis is particularly fine, but Bridges steals the picture." 

Thunderbolt has an 86% "Fresh" rating on Rotten Tomatoes. The RT consensus is "This likable buddy/road picture deftly mixes action and comedy, and features excellent work from stars Clint Eastwood and Jeff Bridges and first-time director Michael Cimino."  Thunderbolt has since become a cult film.  

As a result of this film and Ciminos TV commercial work, producer Michael Deeley would approach Cimino to direct and co-write the Oscar-winning The Deer Hunter (1978).  

==Accolades==
Jeff Bridges received the films only nomination for an Academy Award for Best Supporting Actor.  Eastwoods acting performance was noted by critics to the extent that Clint himself believed it was Oscar worthy. McGilligan (1999), p. 240 

==Analysis== structural paradigm describes a tripartite series of events: natural order followed by disturbance followed by a restoration of natural order. Bliss, p. 153 

==Original soundtrack== Paul Williams.

==DVD==
Thunderbolt and Lightfoot was released to DVD by MGM Home Video on June 13th, 2000 as a Region 1 widescreen DVD and also by Twilight Time on February 11th, 2014 as a Region 1 widescreen Blu-ray DVD.

==See also==
*Clint Eastwood in the 1970s

==References==

===Footnotes===
 

===Bibliography===
* : Newmarket Press. ISBN 1-55704-374-4.
*Bliss, Michael (1985). "Two For The Road". Martin Scorsese and Michael Cimino (Hardcover ed.). Metuchen, NJ: Scarecrow Press. pp.&nbsp;151–165. ISBN 0-8108-1783-7.
*Carducci, Mark Patrick (writer); John Andrew Gallagher|Gallagher, John Andrew (editor) (1989). "Michael Cimino". Film Directors on Directing (Paperback ed.). Westport, CT: Praeger Publishers. ISBN 0-275-93272-9.
*Michael Deeley|Deeley, Michael (April 7, 2009). Blade Runners, Deer Hunters, & Blowing the Bloody Doors Off: My Life in Cult Movies. New York, NY: Pegasus Books LLC. ISBN 978-1-60598-038-6.
*Marc Eliot (author)|Eliot, Marc (October 6, 2009). American Rebel: The Life of Clint Eastwood (1st ed.). New York, NY: Rebel Road, Inc. ISBN 978-0-307-33688-0.
*Hughes, Howard (2009). Aim for the Heart. London: I.B. Tauris. ISBN 978-1-84511-902-7.
*McGilligan, Patrick (1999). Clint: The Life and Legend. London: Harper Collins. ISBN 0-00-638354-8.

==Further reading==
*Wood, Robin (2003). "From Buddies to Lovers". Hollywood from Vietnam to Reagan (Revised and Expanded ed.). New York, NY: Columbia University Press. ISBN 978-0-231-12966-4.

==External links==
* 
*  on MichaelCimino.Fr
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 