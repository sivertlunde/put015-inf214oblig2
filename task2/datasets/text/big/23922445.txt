Assault! Jack the Ripper
{{Infobox film
| name = Assault! Jack the Ripper
| image = Assault! Jack the Ripper.jpg
| image_size = 
| caption = Theatrical poster for Assault! Jack the Ripper (1976)
| director = Yasuharu Hasebe 
| producer = Ryōji Itō
| writer = Chiho Katsura
| narrator = 
| starring = Tamaki Katsura Yutaka Hayashi
| music = Hajime Kaburagi
| cinematography = Masaru Mori
| editing = 
| distributor = Nikkatsu
| released = July 7, 1976
| runtime = 71 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
| preceded_by = 
| followed_by = 
}}
 1976 Japanese film in the Nikkatsus Pink film#Second wave (The Nikkatsu Roman Porno era 1971–1982)|Roman porno series ("Violent Pink" genre). It was directed by Yasuharu Hasebe and has in the lead roles Tamaki Katsura and Yutaka Hayashi.

==Synopsis==
A young couple engage in a rampage of murder, rape and mayhem in order to stimulate their sexual appetites. 

==Cast==
* Tamaki Katsura 
* Yutaka Hayashi
* Yuri Yamashina
* Natsuko Yashiro
* Rei Okamoto
* Naomi Oka
* Luna Takamura

==Critical reception==
Assault! Jack The Ripper! was the second of three "Violent Pink" films Yasuharu Hasebe directed in 1976, between Rape! and Rape! 13th Hour.  Of the three, Hasebe, named  Rape! as his favorite. Screenwriter Chiho Katsura, however, thought that Assault! Jack The Ripper! was the best, and this is the one that had the best critical reception. Hasebe, p. 40.  Some critics interpreted the film as an allegorical comment on the state of Japanese cinema in the mid-1970s. Hasebe recalled, "...it wasnt my intention. I dont like to play those games within cinema. If such a thing exists in the movie, then it was a conspiracy between Producer Itō and scripter Katsura." 

Robert Firsching at Allmovie writes, "Hasebe strikes the perfect balance between blood and sex." He notes, however, that the directors next entry in the "Violent Pink" genre, Rape! 13th Hour would go too far, and almost single-handedly stop Nikkatsu from producing films of this sort. 
 Japan Academy Prize. The Weissers, in their Japanese Cinema Encyclopedia: The Sex Films, write that Katsura was the critics favorite to win, but that the extreme nature of the film and her performance alienated many voters. Weisser, p. 52. 

==Availability==
Assault! Jack the Ripper was released on DVD in Japan on March 21, 2007, as part of Geneon Universal Entertainment|Geneons seventh wave of Nikkatsu Roman porno series.   Mondo Macabro released the film with English subtitles on Region 0 on October 28, 2008. {{cite web |first=Michael Den |last=Boer| date=2008-10-19 |url=http://10kbullets.com/reviews/a/assault-jack-the-ripper-mondo-macabro/|title=
Assault! Jack the Ripper (Mondo Macabro) |accessdate=2009-08-09|publisher= }}  Among the extras included on the disk are an interview with Japanese cinema writer Jasper Sharp, and the Roman Porno episode of the British television program Mondo Macabro (TV programme)|Mondo Macabro. 

==Bibliography==

===English===
*  
*  
*   
* Yasuharu Hasebe|Hasebe, Yasuharu. (1998). Interviewed by Thomas and Yuko Mihara Weisser in Tokyo, 1999, in Asian Cult Cinema, #25, 4th Quarter, 1999, p.&nbsp;32-42.
*  
*  
*  

===Japanese===
*  
*  
*  
*  
*  

==Notes==
 

 

 
 
 
 
 
 