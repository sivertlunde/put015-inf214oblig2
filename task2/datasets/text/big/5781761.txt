The Diary of Ellen Rimbauer (miniseries)
 
{{Infobox television film name = The Diary of Ellen Rimbauer image = DiaryofEllenRimbauerDVD.jpg caption = DVD cover director = Craig R. Baxley producer = Thomas H. Brodek writer = Stephen King (characters) Ridley Pearson (novel and teleplay) starring = Lisa Brenner Steven Brand Tsidii Le Loka  music = Gary Chang cinematography = João Fernandes (cinematographer)|João Fernandes editing = Sonny Baskin distributor = ABC Lions Gate Entertainment (US DVD) Warner Home Video (worldwide DVD) released =   runtime = 88 minutes language = English budget = 
}}
 Rose Red (2002). Directed by Craig R. Baxley, the film stars Lisa Brenner as Ellen Rimbauer, Steven Brand as John Rimbauer, and Tsidii Le Loka as Sukeena.

==Synopsis== Rose Red.

The plot revolves around the construction of the Rimbauer mansion, Rose Red, in Seattle, Washington, tracing a series of mysterious accidents throughout the mansions early history which cause it to become haunted house|haunted. Set at the turn of the 20th century, the stately, sinister mansion is constructed by powerful Seattle oil magnate John Rimbauer (Steven Brand) as a wedding present for his timid, submissive young bride, Ellen (Lisa Brenner). Rimbauer uses much of his wealth to build the mansion in the Tudor neo-Gothic style and situate it on   of woodland. The site previously was a Native American burial ground. The house appeared cursed even as it was being constructed: three construction workers are killed on the site, and a construction foreman is murdered by a co-worker.

Shortly after her marriage to Rimbauer, Ellen begins keeping a diary in which she confesses her anxieties about her new marriage, expresses her confusion over her emerging sexuality, and contemplates the nightmare that her life is becoming. At first impressed by her husbands extravagance, Ellen eventually hates and fears John, especially when learning unsavory facts about his past. Meanwhile, the number of individual hauntings in the mansion increases, possibly including ghosts of the many people close to John who have mysteriously vanished. Ellen interprets the eerie manifestations as a warning that she, too, may some day disappear without a trace.

Ellen and her maid and confidant Sukeena continue to live in the house after Johns death. Ellen believes that if she continues to build the house, she will never die. She uses nearly all of her inherited fortune to continually add to the home over the next several decades, enlarging it significantly. Mysterious disappearances continue: Deanna Petrie, an actress friend of Ellens, and Sukeena both disappear over the next few years. Ellen herself disappears in 1950. Though supernatural means, the house continues to grow, with new rooms, hallways, and staircases appearing overnight.

== External links ==
* .

 
 

 
 
 
 
 
 