Marunnattil Oru Malayali
{{Infobox film 
| name           = Marunnattil Oru Malayali
| image          =
| caption        =
| director       = AB Raj
| producer       = TE Vasudevan
| writer         = V Devan S. L. Puram Sadanandan (dialogues)
| screenplay     = S. L. Puram Sadanandan
| starring       = Prem Nazir Adoor Bhasi Prameela Sankaradi
| music          = V. Dakshinamoorthy
| cinematography = P Dathu
| editing        = BS Mani
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by AB Raj and produced by TE Vasudevan. The film stars Prem Nazir, Adoor Bhasi, Prameela and Sankaradi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Cast==
 
*Prem Nazir
*Adoor Bhasi
*Prameela
*Sankaradi
*Paul Vengola
*Alummoodan
*N. Govindankutty
*Nellikode Bhaskaran
*Pala Thankam
*Paravoor Bharathan
*Philomina
*S. P. Pillai Sadhana
*Vijayasree
 

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Asoka Poornima || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Govardhana Giri || S Janaki || Sreekumaran Thampi || 
|-
| 3 || Kaali Bhadrakali || P Jayachandran, P. Leela, Chorus || Sreekumaran Thampi || 
|-
| 4 || Manassilunaroo || K. J. Yesudas, S Janaki || Sreekumaran Thampi || 
|-
| 5 || Swargavaathilekaadashi || P. Leela || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 