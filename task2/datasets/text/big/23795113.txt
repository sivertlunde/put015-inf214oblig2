Love for Sale (film)
{{Infobox film
| name = Love For Sale
| image = Love_For_Sale.jpg
| director = Russ Parr
| producer = David Eubanks
| writer = Russ Parr  Kelly Neilon  Charles Stewart Jr
| starring = Jackie Long  Jason Weaver  Mýa  Melyssa Ford
| distributor =
| released = October 21, 2008
| runtime = 106 mins.
| country = United States English
| budget =
| website =
}}

Love For Sale is a 2008 romantic comedy film starring Jackie Long, Jason Weaver, Mýa, and Melyssa Ford. The film was directed by Russ Parr and released to DVD on October 21, 2008.

==Plot==
A hapless delivery man learns why sometimes too much of a bad thing can be detrimental to your personal well being after getting caught up with an older woman who sends his life spinning out of control. Trey (Jackie Long) may be working hard, though he just cant seem to catch a break. Hes got no money for school, and his recent attempt to win over the girl of his dreams (Mýa) resulted in nothing less than complete and total embarrassment. But while a seductive cougar (Melyssa Ford) may be more than happy to pay for Treys "personal delivery services," his ideal situation is about to backfire in a way that the roving Romeo could have never seen coming.

==Cast==
* Jackie Long as Trey
* Jason Weaver as Vince
* Mýa as Keiley
* Melyssa Ford as Katherine
* Essence Atkins as Candace
* Tanjareen Martin as Dedra
* Angell Conwell as Sida Richard Lawson as Uncle Mac

==DVD release==
The film was released on DVD October 21, 2008.

==External links==
*  

 
 
 
 
 
 


 