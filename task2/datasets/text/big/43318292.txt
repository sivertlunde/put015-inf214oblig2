The Ways of Sin
{{Infobox film
| name = The Ways of Sin 
| image =
| image_size =
| caption =
| director = Giorgio Pastina
| producer =
| writer =  Grazia Deledda (novel)   Giorgio Pastina 
| narrator =
| starring = Jacqueline Laurent   Leonardo Cortese   Carlo Ninchi   Ada Dondini 
| music = Mario Labroca 
| cinematography = Giuseppe La Torre
| editing = Mario Serandrei
| studio =   Ilaria Film  ENIC
| released = 19 October 1946 
| runtime = 90 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical drama film directed by Giorgio Pastina and starring Jacqueline Laurent, Leonardo Cortese and Carlo Ninchi. The film is a melodrama set in Sardinia at the beginning of the twentieth century. It is based on a novel by Grazia Deledda. The film was shot in the Apennine Mountains rather than Sardinia. 

==Cast==
* Jacqueline Laurent as Ilaria 
* Leonardo Cortese as Don Roberto  
* Carlo Ninchi as Don Sebastiano Pinna  
* Ada Dondini as Eufemia Pinna 
* Laura Gore as Carla Pinna  
* Gualtiero Tumiati as Don Salvatore  
* Andrea Checchi as Rocco  
* Lauro Gazzolo as Il farmacista 
* Umberto Sacripante as Fausto, Il pastore 
* Michele Riccardini as Il sacerdote 
* Nino Pavese as Il brigadiere
* Franco Coop as Il notaio  
* Dante Maggio as La guardia carceraria  
* Rinalda Marchetti as Lamante di Don Sebastiano 
* Amalia Pellegrini as La domestica dei Pinna 
* Aldo Silvani as Il giudice istruttore

== References ==
 
 
==Bibliography==
* Urban, Maria Bonaria. Sardinia on Screen: The Construction of the Sardinian Character in Italian Cinema. Rodopi, 2013.

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 