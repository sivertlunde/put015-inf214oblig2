Dieu a besoin des hommes
 
{{Infobox film
| name           = Dieu a besoin des hommes
| image          = 
| caption        = 
| director       = Jean Delannoy
| producer       = Paul Graetz
| writer         = Jean Aurenche Pierre Bost Henri Queffelec
| starring       = Pierre Fresnay
| music          = 
| cinematography = Robert Lefebvre
| editing        = James Cuenet
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         = 
}}

Dieu a besoin des hommes (God Needs Man) is a 1950 French drama film directed by Jean Delannoy. At the 1st Berlin International Film Festival it won the Special Prize for an Excellent Film Achievement.   

==Cast==
* Antoine Balpêtré - Le père Gourvennec, un pêcheur
* Lucienne Bogaert - Anaïs Le Berre
* Charles Bouillaud - Le gendarme
* Jean Brochard - Labbé Kerhervé, le recteur de Lescoff
* Jean Carmet - Yvon
* Andrée Clément - Scholastique Kerneis
* Marcel Delaître - M. Kerneis
* Jean dYd - Corentin Gourvennec
* Pierre Fresnay - Thomas Gourvennec
* Daniel Gélin - Joseph le Berre
* Marcelle Géniat - La mère Gourvennec
* René Génin - Le père dYvon
* Jérôme Goulven - Le brigadier
* Daniel Ivernel - François Guillen
* Germaine Kerjean - Mme Kerneis
* Cécyl Marcyl - La vieille
* Albert Michel - Le Bail
* Jean-Pierre Mocky - Pierre
* Pierre Moncorbier - Un pêcheur
* Raphaël Patorni - Jules
* Fernand René - Yves Lannuzel
* Madeleine Robinson - Jeanne Gourvennec

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 