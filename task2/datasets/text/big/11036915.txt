Mi Amigo
 
{{Infobox film
 | name = Mi Amigo
 | image = Mi_amigo.jpg
 | caption = Mi Amigo DVD cover
 | director = Milton Brown
 | writer = Milton Brown
 | starring = Josh Holloway Tom Everett Ed Bruce Channon Roe
 | distributor = Azalea Film Corporation
 | runtime = 89 minutes
 | language = English released = 2002 country = United States
 | }}
 2002 comedy, western genre film.

==Plot==
Their friendship strained by love of the same woman, cowboy buddies Bobby Ray Burns and Pal Grisham return to hometown Cherub, Texas, to renew old ties and settle an old debt. In the thirty years since the pair split up, Bobby Rays guitar picking and singing have won him gold records, while Pals good looks have earned him silver-screen stardom—and the hand of the woman who divided them, Kitty ODonnell. Known once around old Cherub as The Three Musketeers, Bobby Ray, Pal, and Kitty come home to confront the past in this warm-hearted, cowboy valentine to friendship, love, and fidelity.

Even as Cherub welcomes them back as celeb-heroes, Bobby Ray seeks to involve Pal in breaking into the town bank to make an anonymous deposit of $211.63 — repayment of a loan drawn on a whiskey-drinking night, years gone, that saw the Musketeers go their separate ways. Complications ensue as they learn that the bank, much of the town, and Pals ancestral ranch have fallen into the hands of antagonist, Cale Chason.

==Cast==
*Josh Holloway as Younger Pal Grisham
*Burton Gilliam as Pal Grisham
*Ed Bruce as Bobby Ray
*Tom Everett as  Older Cale
*Jo Harvey Allen as  Kitty ODonnell
*Channon Roe as Younger Bobby Ray
*Jackie Schell as Younger Kitty ODonnell Jack Armstrong as Younger Cale Francisco Gonzalez as Thug and Director of Photography

==External links==
*  
*  

 
 
 
 
 
 
 


 