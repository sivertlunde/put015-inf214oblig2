Dragon Blade (film)
 
 
{{Infobox film
| name           = Dragon Blade
| image          = DragonBladefilm.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 天將雄獅
| simplified     = 天将雄狮
| pinyin         = Tiān Jiāng Xióng Shī
| jyutping       = Tin1 Zeong3 Hung4 Si1 
| translation    = "Celestial General, Heroic Army"}} Daniel Lee 
| producer       = 
| writer         = 
| screenplay     = Daniel Lee
| story          = 
| based on       = 
| narrator       =  Xiao Yang Lorie Pester
| music          = 
| cinematography = 
| editing        =  Shanghai Film Group  Sparkle Roll Cultural Media Beijing Cultural Assets Chinese Film & Television Fund Home Media and Entertainment Shenzhen Tencent Video Culture Communication   
| distributor    = SFC Film   Intercontinental Film Distributors (HK) Ltd.   Golden Network Asia   
| released       =   }}
| runtime        = 127 minutes
| country        = China   Hong Kong 
| language       = Mandarin English
| budget         = US$65 million   
| gross          = US$120 million (China) 
}} historical action Daniel Lee and starring Jackie Chan. In the film, Chan plays Huo An, the commander of the Protection Squad of the Western Regions during the Han Dynasty. Dragon Blade was released in IMAX 3D on 19 February 2015, the first day Chinese New Year holiday period.    

==Plot==
  Protection Squad of the Western Regions of the Han Dynasty. They are tasked by the emperor to maintain stability between the warring 36 nations in the Silk Road region.

Huo An and his subordinates are framed for smuggling gold and are sent to the Wild Geese Gate, a frontier town of the Han Dynasty and undergo forced labor. At the frontier town the many nations and people that represented labor force were distrustful and did not assist each other in the completion of the restoration of the Wild Geese Gate.

Meanwhile, in the west a Roman general named Lucius (Cusack) flees with his legion to the east. He smuggles Publius (Waite), heir of the Roman consul for the Parthian Empire to save him from death and a ploy by his older brother, Tiberius (Brody), who schemes to takeover the position as heir to the Consul seat for himself. Lucius arrives at outskirt of the Wild Geese Gate, with plans to assault the frontier town with his legion. Their aim was to find a place for the legions to rest. In anticipation of the confrontation, Hou An takes the armor plate and sword of the previous commanding officer of the Wild Geese Gate and goes out to meet Lucius. Lucuis and Hou battle in an evenly matched sword play. Before the match ends with a clear winner, a sandstorm approaches threatening the romans and all others who are outside of the city walls. Huo realizing this, plus seeing Publius in a very dire state, reluctantly offers to allow the romans refuge.

During their stay, Lucius, after admiring the sword of the previous commanding officer of the Wild Geese Gate, tells Huo the plight of Publius and his legions.  The next day, the current commanding officer of the Wild Geese Gate receives a royal edict to complete the restoration work of the fortress within 15 days. The commanding offers deems this to be impossible and pleads Huo An to enlist more men from China to help with the construction efforts. Huo An instead asks the legionnaires for their expertise and assistance to complete the task. In line with Huo Ans mentor, the previous commanding offer, the reconstruction effort got all different people and nations to work together as one and a peace is achieved in what was a chaotic, and separated city. Because of the success of the reconstrucition and a general feeling of brotherhood the young Publius appoints Huo as a First Centurion, presenting him with a Roman gladius.

During this time Tiberius (Brody) and his army, who have pursued Lucius along the Silk Road, approaches the Wild Geese Gate. Huo An then informs Lucius he will go to his allies to recruit an army from his long lost friend to fight against Tiberius, but is ultimately betrayed by one of his surbodinates and delayed, while Lucius is imprisoned and tortured.  Huo manages to escape, but his wife is killed protecting one of her escaping students. Help arrives in the form of a group of tribal warriors whom Huo previously encountered.

Huo learns about Luciuss fate and attempts to rescue Lucius by pretending to surrender to Tiberius. He finds Lucius, whom Tiberius has blinded in spite, and Lucius entrusts his legion to Hou An shortly before pleading Hou An to end his misery in which reluctantly does so. A climatic battle ensues with Tiberiuss Army and Huo Ans allies. Huo Ans army and allies were almost completely defeated before a Parthian Army arrives to the field. They accuse Tiberius of murdering his father and blinding the rightful heir to the throne. Huo An and Tiberius engages in one last epic mano-a-mano battle.  Tiberius then stabbed himself after losing and in his dying breath silently sings the Roman Legion anthem.

==Cast==
*Jackie Chan as Huo An (霍安) 
*John Cusack as Lucius
*Adrien Brody as Tiberius
*Lin Peng    as Moon
*Mika Wang  as Xiu Qing (Huo Ans Wife)
*Choi Siwon as Yinpo Xiao Yang
*Wang Taili
*Sammy Hung as Sun
*Jozef Waite as Publius (The Roman Prince)
*Yoo Seung-jun Lorie Pester Parthian Queen
*Feng Shaofeng as Huo Qubing (Han Chinese General)
*Vanness Wu  as Christian
*Karena Lam
*Sharni Vinson as The Roman Queen
*Harry Oram as General Statius
*Philippe Joly as Lucius Deputy General Paullus  
*James Lee Guy as Eugene
*Alexey Surovtsev as Roman Soldier

==Production== Daniel Lee attended and announced the film to be released in IMAX 3D on 19 February 2015, the first day of the Chinese Lunar New Year.  Aside from Hengdian, production took place in Dunhuang and the Gobi Desert. 

For the role of the Roman general, Lucius, American actor Mel Gibson was initially rumored to be taking the role. During a press conference, neither Chan nor Lee confirmed Gibsons participation and Chan only said,
  }}

Ultimately, however, the role went to John Cusack. Dragon Blade was filmed with a budget of US$65 million.    The  film was financed by Sparkle Roll Media Corporation, Huayi Brothers Media Corporation, Shanghai Film Group, Home Media & Entertainment Fund, Tencent Video and the Beijing Cultural Assets Chinese Film and Television Fund.    A signing ceremony for the launching of Beijing Cultural Assets Chinese Film and Television Fund also occurred there as Dragon Blade is the first film to receive an investment from the fund.   International distribution of the film outside of China was  handled by Golden Network Asia. 

==Box office==
The film was a commercial success in its native country, China.  It opened Thursday, February 19 in China and grossed US$18.7 million on the opening day.  Through Sunday, February 22, it had a 3-day opening weekend total of US$33 million, topping the Chinese box office    (US$54.8 million from Thursday - Sunday)    from 132,874 screenings and 8.14 million admissions.    Through its opening week it earned US$72 million.    The film fell to number three the following weekend, earning US$45.9 million (down 19%).    As of March 15, 2015, Dragon Blade has earned US$120 million in China alone.   

==See also==
*Jackie Chan filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 