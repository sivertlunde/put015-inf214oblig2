Roots of Blood
{{Infobox film
| name           = Roots of Blood thumb
| alt            = Roots of Blood DVD cover
| caption        = DVD cover for Raíces de sangre in English
| film name      = Raíces de sangre 
| director       = Jesús Salvador Treviño 
| producer       =  
| writer         = Jesús Salvador Treviño
| screenplay     = 
| story          = 
| based on       = 
| starring       = Richard Yñiguez Malena Doria Adriana Rojo Ernesto Gómez Cruz Pepe Serna Roxana Bonila-Giannini
| narrator       = 
| music          = Sergio Guerrero 
| sound          = Sigfrido Garcia
| cinematography = Rosalío Solano
| editing        = Joaquin Ceballos 
| studio = CONACINE (Corporación Nacional Cinematográfica)
| distributors   = Azteca Films (foreign theatrical distributor)  Madera CineVideo (VHS tape)  Spanishmultimedia (DVD) 
| released       =         
| runtime        = 94 minutes 
| country        = Mexico
| language       = Spanish (also English on DVD )
| budget         = 
| gross          =  
}}

Raíces de sangre {{Cite AV media
| first       = Jesús Salvador
| last        = Treviño
| author-link = Jesús Salvador Treviño
| year        = 2001
| title       = Raices de sangre (VHS tape, 2001)
| trans-title = Roots of Blood
| medium      = VHS tape
| language    = es
| url         = https://www.worldcat.org/title/raices-de-sangre/oclc/52138242
| format      =
| time        =
| location    = Madera, CA, US
| publisher   = Compañía Oxxo, Inc., and Corporación Nacional Cinematográfica (CONACINE) (Mexico), Distributed by Madera CineVideo
| id          =
| isbn        =
| oclc        = 52138242
| archive-url = https://web.archive.org/web/20150403050617/https://www.worldcat.org/title/raices-de-sangre/oclc/52138242
| archive-date= 2015-04-03
| dead-url    = no
| access-date = 2015-04-03
| quote       = A dramatization of the struggle of Mexican families to earn an acceptable life on one side of the border or the other, and to organize against exploitive employers who play off the Mexican workers south of the border against the fear of the undocumented workers north of the border.
| ref         =
}}  {{Cite AV media
| people      = Treviño, Jesús Salvador
| year        = 2007
| orig-year   = 1976
| title       = Raices de sangre = Roots of blood (DVD video, 2007)
| trans-title = not playable in Mexico (Region 4))
| language    = Spanish and English
| url         = https://www.worldcat.org/title/raices-de-sangre-roots-of-blood/oclc/190772413
| format      = Color, NTSC
| time        =
| series      = Latin cinema collection.
| location    = Los Angeles, CA, US
| publisher   = Desert Mountain Media, and Corporación Nacional Cinematográfica (CONACINE) (Mexico), Distributed by Spanishmultimedia
| id          =
| isbn        =
| oclc        = 190772413
| asin        = B00D6196UA
 
| archive-url = https://web.archive.org/web/20150403050517/https://www.worldcat.org/title/raices-de-sangre-roots-of-blood/oclc/190772413
| archive-date= 2015-04-03
| dead-url    = no
| access-date = 2015-04-03
| quote       = A dramatization of the Latino experience in America, chronicling the creation of an international union of garment workers in a Texas border town. A group of chicanos, known as "Barrio Unido", bravely struggle to bring to light instances of prejudice and brutality on the part of both exploitative businesses and the police.
| ref         = Mexican movie written and directed by Jesús Salvador Treviño released in 1978 in Mexico and other countries. {{Cite news
| author      = Staff
| date        = 2010
| title       = Raices de Sangre (1978) Credits details
| url         = http://www.nytimes.com/movies/movie/40109/Raices-de-Sangre/details
| newspaper   = The New York Times
| archive-url = https://web.archive.org/web/20150403050245/http://www.nytimes.com/movies/movie/40109/Raices-de-Sangre/details
| archive-date= 2015-04-03
| dead-url    = no
| access-date = 2015-04-02
| quote        = 
}}   According to some sources, it had a wide release on May 30, 1979, {{Cite web
| url         = https://www.rottentomatoes.com/m/raices-de-sangre/
| title       = Raices De Sangre (1979)
| author      = Staff
| date        = 2015
| website     = Rotten Tomatoes
| publisher   = Flixster, Inc.
| access-date = 2015-04-26
| archive-url = https://web.archive.org/web/20150426175943/http://www.rottentomatoes.com/m/raices-de-sangre/
| archive-date= 2015-04-26
| dead-url    = no
}}  but other sources show it was playing in US theaters as early as August 1978. {{Cite news
| last1        = Victoria (theater)
| first1       = 
| date         = 1978-08-27
| title        = The Victoria Advocate (theater advertisement)
| trans-title  = Roots of Blood
| url          = https://news.google.com/newspapers?nid=861&dat=19780827&id=qWAdAAAAIBAJ&sjid=qVoEAAAAIBAJ&pg=1502,6890699
 
| format       = 
| language     = 
| page         = 34
| newspaper    = The Victoria Advocate
| location     = Victoria, TX, US
| archive-url  =  
| archive-date = 
| dead-url     = no
| access-date  = 2015-04-05
| via          = Google news archive
| subscription = 
| quote        = 
}} 

The film deals with labor relations and tensions between Mexican and Chicano workers.  It has received praise as a foundational work in Latin American cinema.

== Theme, setting, tone and plot summary ==

 

The theme concerns worker relations along the US&ndash;Mexico border. {{Cite news
| last         = Fountain
| first        = Clarke
| date         = 2010
| title        = Raices de Sangre (1978) Review Summary
| url          = http://www.nytimes.com/movies/movie/40109/Raices-de-Sangre/overview
| newspaper    = The New York Times
| archive-url  = https://web.archive.org/web/20150403050418/http://www.nytimes.com/movies/movie/40109/Raices-de-Sangre/overview
| archive-date = 2015-04-03
| dead-url     = no
| access-date  = 2015-04-02
| quote        = The factory workers in the border towns on the U.S./Mexico border in this movie have fallen between the cracks of the guidelines which apply to either nation concerning worker safety.
}} 

The movie is set in Socorro, Texas, along the border between Mexico and the US.  Chicano and Mexican workers interact with each other and with management in a garment factory.
 labor tension and attempts at organizing the workers, a deadly riot occurs.

== Cast ==

* Richard Yñiguez as Carlos Rivera
* Malena Doria as Hilda Gutierrez
* Adriana Rojo as Rosamaria Mejía
* Ernesto Gómez Cruz as Román Carvajal
* Pepe Serna as Juan Vallejo
* Roxana Bonila-Giannini as Lupe Carrillo 
* León Singer as Rogelio
* Enrique Muñoz as Adolfo Mejía
* Roger Cudney as factory foreman Alvarado

== Reception, criticism and legacy ==

For the 20th annual Ariel Awards of the Mexican Academy of Film Arts and Sciences in 1978, Treviño was nominated for Argumento original (Best Original Story) and Ópera prima (Best First Work) for Raíces de sangre. {{Cite web
| url         = http://www.academiamexicanadecine.org.mx/ver_ariel.asp?tipo=ariel&idPersona=3638
| title       =  Ariel > Ganadores y nominados > Jesús Salvador Treviño   1978]
| language    = Spanish
| trans-title = Ariel > Winners and nominees > Jesús Salvador Treviño > XX   1978
| last        =
| first       =
| date        =
| website     = The Mexican Academy of Film Arts and Sciences http://www.academiamexicanadecine.org.mx
| publisher   =
| archive-url = https://web.archive.org/web/20150403094345/http://www.academiamexicanadecine.org.mx/ver_ariel.asp?tipo=ariel&idPersona=3638
| archive-date= 2015-04-03
| dead-url    = no
| access-date = 2015-04-25
| quote       =
}} 

In 1983, professor of Chicano/Latino Studies Alejandro Morales|Dr. Alejandro Morales reviewed and analyzed the literature of three of Treviños major 1970s works, stating about Raíces de sangre that it is "an antidote to Hollywood productions such as Boulevard Nights and Walk Proud, which focus on stereotypical images of Chicano gangs". {{Cite journal
| last        = Morales
| first       = Alejandro
| author-link = Alejandro Morales
| date        = May–December 1983
| title       = Expanding The Meaning Of Chicano Cinema: Yo Soy Chicano, Raices De Sangre, Seguin
| url         = http://www.jstor.org/stable/25744065
| journal     = The Bilingual Review / La Revista Bilingüe
| publisher   = Bilingual Review Press
| location    = Arizona State University, Tempe, AZ, US
| volume      = 10
| issue       = 2/3
| pages       = 121&ndash;137
| bibcode     = 
| doi         = 
| archive-url =  
| archive-date= 
| dead-url    = no
| access-date = 2015-04-04
| quote       = ... he Chicano or non-Chicano viewer cannot totally reject the obtuse meaning but is instead attracted to it in an interrogative posture.  Among other qualities this is what makes Treviños films artistically and socially valuable. (p. 136)
}}    He criticizes the film noting that, " he film projects...a confusing vision of life on the Mexican&ndash;United States border."     Morales further notes the symbolism of the fence dividing the border, and the Chicanos and the Mexicans, at the end of the film and observes that "...the criminals go unpunished and not even an educated Harvard lawyer will change the exploitative conditions that exist on the border."  
 International Film Festival in Valladolid, Spain. {{Cite web
| url          = http://www.dga.org/Events/2009/12-December-2009/Latino-Committee-Hosts-Jesus-Salvador-Trevino-Full-Circle.aspx
| title        = Jesus Salvador Trevino Full Circle (A Latino Committee Event)
| author       = The Directors Guild of America and Byron Gamarro
| date         = 2009-11-02
| website      = dga.org
| publisher    = Directors Guild of America, Inc.
| location     = Los Angeles, CA, US
| archive-url  = https://web.archive.org/web/20150326202514/http://www.dga.org/Events/2009/12-December-2009/Latino-Committee-Hosts-Jesus-Salvador-Trevino-Full-Circle.aspx
| archive-date = 2015-03-26
| dead-url     = no
| access-date  = 2015-03-26
| quote        = On Monday, November 2  , the Latino Committee presented a special evening in honor of DGA National Board Member Jesús Treviño. Entitled, Jesús Salvador Treviño “Full Circle”, the event was a celebration of Treviño as a ground-breaking director and as an activist and pioneer.
}} 

In his 2014 book, Latino Image Makers in Hollywood, author Frank Javier Garcia Berumen describes Treviños Raíces de sangre as foundational to Chicano cinema. {{Cite book
| last        = Berumen
| first       = Frank Javier Garcia
| author-link = 
| date        = 2014
| title       = Latino Image Makers in Hollywood: Performers, Filmmakers and Films Since the 1960s
| url         = https://books.google.com/books?id=6OpZBAAAQBAJ&pg=PT107&lpg=PT107&source=bl&ots=XNBWhqcYuy&sig=wEL3fb_TpkKitoDV99w-kuQqlkw&hl=en&sa=X&ei=XTo8VamOD4HZsAWf0YCgDg&ved=0CC0Q6AEwAg#v=onepage&q&f=false
| publisher   = McFarland & Co Inc
| location    = Jefferson, North Carolina, US
| pages       = 
| bibcode     = 
| doi         = 
| isbn        = 978-07-8647-432-5
| oclc        = 881518442
| asin        = 
| archive-url = https://web.archive.org/web/20150426014910/https://books.google.com/books?id=6OpZBAAAQBAJ&pg=PT107&lpg=PT107&source=bl&ots=XNBWhqcYuy&sig=wEL3fb_TpkKitoDV99w-kuQqlkw&hl=en&sa=X&ei=XTo8VamOD4HZsAWf0YCgDg&ved=0CC0Q6AEwAg#v=onepage&q&f=false
| archive-date= 2015-04-26
| dead-url    = no
| access-date = 2015-04-25
| quote       = This book documents historical and socio-economic factors that created Latino and Latina images and stereotypes, beginning with the conquest of the American continent by Europeans followed by colonization and new nation-states in the early 1800s. These concepts were incorporated into literature of the 19th century and then into the motion picture art form in the 1890s 
}} 

== See also ==

 
 
* Cinema of Mexico
* Latin American culture
* List of Latin American films
* List of Mexican submissions for the Academy Award for Best Foreign Language Film
* Maquiladora
 

== References ==

 

== External links ==

*  
*  
 
  British Film Institutes Film and TV Database
*  
*  
*  

 
 

 
 
 