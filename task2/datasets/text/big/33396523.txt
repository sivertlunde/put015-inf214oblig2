Mera Yaar Mera Dushman
{{Infobox film
 | name = Mera Yaar Mera Dushman
 | image = MeraYaarfilm.jpg
 | caption = VCD Cover
 | director = Anil Ganguly
 | producer = Askari Jafri
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Rakesh Roshan Ardhendu Bose Zarina Wahab Bindiya Goswami A.K. Hangal Deven Verma
 | music = Bappi Lahiri
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  1987 (India)
 | runtime = 140 min.
 | language = Hindi Rs 4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1987 Hindi Indian feature directed by Anil Ganguly, starring Mithun Chakraborty, Ardhendu Bose, Zarina Wahab, Bindiya Goswami, A.K. Hangal and Deven Verma.

==Plot==

Mera Yaar Mera Dushman is a crime thriller, featuring Mithun Chakraborty and Zarina Wahab in lead roles, well supported by Rakesh Roshan, Ardhendu Bose, A.K. Hangal, Bindiya Goswami and Deven Verma.
Frame by frame of a murder plot, unraveled by a boxing champion, played by Mithun, who is the brother of Ramesh Deo, the Union Leader killed.
The story is on Narcotics and their illegal smuggling and involvement of the staff of big industries, who are harassed by rising prices and low wages.The story opens with Zarina Wahab being forced into narcotics smuggling by her brother-in-law Sujit Kumar, due to her wheel chair ridden sister Beena Banerjee.Meanwhile Rakesh Roshans father makes him take over his business, much to the chagrin of A.K. Hangal and Sujit Kumar who embroil him in the murder of Union leader Ramesh Deo by switching off the lights.A.K.Hangal has a factory to prepare drugs with the use of Acids.Rakesh Roshans good business is used to en route drugs. Mithun is in love with Bindiya Goswami who is the sister of Rakesh Roshan.She helps him to catch the actual killer by guiding him to the place where her brother is imprisoned by Sujit Kumar with drugs.Will they know who killed Ramesh Deo and who fired the second bullet ?

==Cast==
*Mithun Chakraborty
*Rakesh Roshan
*Ardhendu Bose
*Zarina Wahab
*Bindiya Goswami
*A.K. Hangal
*Deven Verma
*Beena Banerjee
*Dina Pathak
*Ramesh Deo

==References==
* http://ibosnetwork.com/asp/filmbodetails.asp?...Mera+Yaar+Mera+Dushman

==External links==
*  

 
 
 
 

 