Some Kind of Hero
{{Infobox film
|  name=Some Kind of Hero
|  image= Some kind of hero movie poster.jpg
|  caption = The movie poster for Some Kind of Hero.
|  writer=Robert Boris  James Kirkwood Jr.
| starring       = {{plainlist|
* Richard Pryor
* Margot Kidder
* Ray Sharkey
}}
|  director=Michael Pressman King Baggot
|  editing=Christopher Greenbury Patrick Williams
|  distributor=Paramount Pictures
|  released=April 2, 1982
|  runtime=97 min.
|  country=   English
|  movie_series=
|  awards=
|  producer=Howard W. Koch
|  budget=$8 million
|  gross=$23,671,186}}

Some Kind of Hero is a 1982 film starring Richard Pryor as a returning Vietnam War veteran having trouble adjusting to civilian life. Soon he is involved in an organized crime heist. It co-stars Margot Kidder and was directed by Michael Pressman.

Although James Kirkwood and Robert Boris are jointly credited with the screenplay, in fact the script was Boris’ rewrite of Kirkwood’s adaptation of his novel. Originally intended to be a straight drama, the studio insisted that Pryor perform comedic scenes as well.

==Plot==
Eddie Keller is one of the last POWs to be brought home from Vietnam, after several years of torture and deprivation at the hands of the Vietcong. During his captivity, he resists signing a "confession" admitting to war crimes repeatedly, but finally consents to save the life of another prisoner.

Having returned home, Eddie finds the world has moved on without him. His wife has fallen in love with someone new, and had a daughter, just after he became a POW. His mother has suffered a stroke, and requires constant (and expensive) medical attention. Eddie is initially called a hero when he is finally released, but when his signed confession is discovered (and no one can track down the other prisoner he tried to save), his veterans benefits are suspended pending further investigation.

Eddie tries to integrate back into society, but finds himself stopped at every turn. The Army refuses to help, he cannot find a job, and he is running out of options. The only bright spot in his life is Toni, a high-priced prostitute who picks Eddie up at a bar. Despite Tonis profession, the two begin a romance.

While trying to secure a loan, Eddie is witness to a bank robbery. He begins to plot a way to gain the funds he needs to provide for his mother, and also to avenge himself on a system that abandoned him in Vietnam, then turned him into a traitor.

Eddie plans to hold up a bank, but fails repeatedly in his efforts to embark on a life of crime. Eventually, he succeeds in stealing a briefcase full of bonds, which he arranges to sell to a mobster for a large sum of cash. The mobsters plan to kill Eddie and take the bonds. Eddie turns the tables on the mobsters, escaping with the cash and the bonds with Toni.

==Cast==

*Richard Pryor as Eddie Keller
*Margot Kidder as Toni Donovan
*Ronny Cox as Col. Powers
*Lynne Moody as Lisa
*Paul Benjamin as Leon
*Olivia Cole as   Jesse  Matt Clark as  Mickey

==Reception==

The movie gained mixed reviews.  

==See also==
* List of American films of 1982

==References==
 

== Further reading==

* Egan, Sean (2011) “Ponies & Rainbows: The Life of James Kirkwood” Bearmanor Media, ISBN 1-59393-680-X

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 