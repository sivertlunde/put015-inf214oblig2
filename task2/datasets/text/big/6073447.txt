Children of the Corn 666: Isaac's Return
 
{{Infobox film| name = Children of the Corn 666: Isaacs Return
 | image = ChildrenOfTheCorn666DVD.jpg
 | caption = Region 2 Dvd Cover
 | director = Kari Skogland
 | producer = Bill Berry Jeff Geoffray Walter Josten
 | writer = Tim Sulka John Franklin John Franklin Nancy Allen
 | music = Jonathan Elias Vladimir Horunzhy Terry Huud
 | cinematography = Richard Clabaugh
 | editing = Troy Takaki
 | distributor = Dimension Films Buena Vista Home Entertainment 
 | released = October 12, 1999 (USA)
 | runtime = 82 minutes
 | language = English
 | country = United States
}}
 John Franklin, from the first Children of the Corn film.

==Plot==
 
Hannah, the first child born of the original Gatlin corn cult, visits the town of Gatlin to find her real mother. On the way, she picks up a street preacher by the name of Zachariah whose car broke down.  He tells her about her name, and then vanishes. After crashing into a corn field a lady sheriff suddenly appears and takes Hannah to a hospital in town . Once there, she finds out Isaac was not killed by "He Who Walks Behind The Rows", but instead went into a coma. The hospital appears to be filled with strange patients who speak of a prophecy involving Hannah and Isaac before the scene changes.

After she leaves the hospital and resumes her journey, she is nearly driven off the road by a mysterious truck. After pulling into a strange motel, she almost steps on a dead crow and then is startled by a smiling young boy (Daniel L Nicoletti) who  suddenly appeared in a chair next to her car.  In the motel office, she meets a pair of romantically-involved teenagers, a girl and her boyfriend, Matt. She then checks into the motel. The next morning as Hannah is leaving the motel, a small crowd gathers around her car, fascinated by her. Meanwhile, the scene changes and it is revealed that Isaac has awakened from his long 19 year sleep (in which He Who Walks Behind the Rows left him in at the end of the first film) and that he has a son. Hannah returns to the hospital where she begins having visions in the empty hallway before Gabriel appears behind her. He shows her to the record-room so she can look for her birth-certificate. While theyre in the dark, she is almost killed when Jake tries to split her head open with an axe. Gabriel leaves Hannah alone to take Jake back to his room. While alone, Hannah finds a scythe pinning what she thinks is her birth-certificate into the wall.

In the middle of the night, a strange woman tries to touch Hannah while she is lying in bed, but leaves once she realizes Hannah is awake. Hannah recognizes her truck as the one that drove her off the road. She follows the truck until dawn into the middle of the corn-field, but before she can follow the person she runs into Jesse: another strange teenager carrying a machete. He tells her the owner of the truck is Rachel Colby, the same name on the birth-certificate. In her motel-room, Hannah discovers the words "GET OUT OR DIE!" written in what appears to be blood in the shower. Back at the church, Rachel confronts Isaac. It is revealed that Rachel is the wife of Amos (Children of the Corn) and she believes her daughter to be dead. When she leaves, Isaac tells how she will be punished for her betrayal. Rachel later talks with Dr. Michaels, who just wants to move on beyond the ideas of cults and sacrifices. He tells Rachel to do whatever she needs to do to try to stop Isaac.

Dr. Michaels comes back to the hospital, he finds Jake has clogged the sink and caused water to go all over the floor and is now muttering mindlessly on his knees. Isaac steps out of the shadows, displaying supernatural power. Michaels stands, unafraid of him, warning him to leave Hannah alone, exposing himself as the one who took Hannah away from the town. Isaac pulls a sparking electric cord from the wall and drops it on the wet floor, electrocuting Michaels. Later, Isaac approaches the son, who is revealed to be Matt. He is proud in the belief that his son will carry on his legacy. Matt, though, seems less than enthusiastic. While Gabriel talks with Isaac in the cornfield, Hannah is meanwhile driven off the road again, this time by a drunken Matt. After she yells at him, he hands her a shovel and tells her he is a descendant of Isaac and for her to trace her lineage. Hannah begins digging up the grave of Baby Colby, Rachels apparently dead child. As she digs, the bloody dead body falls right above her head hanging from a tree; however, this turns out to be only an illusion. Rachel is in the graveyard with her and warns her that theres no going back if she continues. Hannah then says shell only leave if Rachel tells her the truth. Rachel denies her this, calling it "repulsive". As Hannah prepares to open the casket, a desperate Rachel tells her the prophecy: "The firstborn daughter of the children will return on the eve of her nineteenth-birthday to find out who she is and He-Who-Walks-Behind-The-Rows will awaken".
     
Hannah opens the casket to see it is empty and Rachel knew Hannah was her child and wasnt dead. She tells a distraught Hannah that Isaac wants Hannah to make a new, "pure" race. Hannah accuses her of insanity and runs away. In the corn-fields, all of the cult has gathered to celebrate as Matt is branded as the first of the chosen. While Hannah is walking through the corn-fields that night, she is surrounded by children, one of whom injects her with a sedative. Hannah wakes up surrounded by the cult-members and a blazing bonfire. They place a crown of corn-husks on her head and brand her hand like they did Matt. They begin to perform a union-ceremony between her and Matt, but Hannah escapes. They try to catch her, activating the irrigation-system and driving motorcycles through the fields. Rachel appears before the cult-members, claiming Isaac is a fraud. In the fields, Matts girlfriend attempts to help Hannah escape, because she wants to be with Matt. They are soon cornered by the cult-members who capture them. It turns out that Gabriel was on the bike Hannah was put on and takes her out of the cornfields. Matts girlfriend, though, isnt so lucky and Isaac orders Matt to kill her. Matt refuses and Isaac, cursing his son, splits her in half with Jesses blade. With that, Matt runs into the corn-field.

Gabriel tends Hannahs wounds in a barn, helps her bathe and kisses her. They begin to have intercourse at the exact moment the clock strikes twelve. Matt then soon appears in the barn, Hannah asks about her mother and, after not receiving any answer, leaves to look for her. While in the barn, Gabriel shows Matt his collection of all of the farming tools of the original children and promises that Matt will be with his girlfriend. He leaves the barn with Hannah and Matt impales himself on a scythe. In the hospital, Rachel is being held in the basement and Hannah is being led to her by visions of her being beaten by Cora. In the hallway she meets Jake, who warns her of "a false-prophet, sheep’s-clothing, raving-wolves!" while Gabriel kills Jesse with his supernatural-power. Hannah now confronts Isaac, who now believes himself to be He-Who-Walks-Behind-The-Rows and has gone mad with power. Gabriel storms down the halls of the hospital and, when Cora tries to shoot him, with a flick of his wrist and the word bang, has her kill herself. He then confronts Isaac and exposes that he was the firstborn child of the children and that Isaac denied him his birthright in favor of his own son.

Gabriel tells Hannah to kill Isaac, but listening to her mother, Hannah does not. Gabriel goes on to explain how everything that has happened has gone according to his plan. He then levitates Isaac with his power and reveals himself to be He-Who-Walks-Behind-The-Rows". He restrains Isaac to the ground and stabs him with the broken end of a lead pipe. Rachel stabs Gabriel and together, they flee from the hospital. Gabriel, though, is healed almost instantly and begins to set off explosions which kills Jake. Rachel and Hannah are then seen walking down the road, with Hannah now pregnant with the child of He-Who-Walks-Behind-The-Rows.

==Cast==
*Natalie Ramsey as Hannah Martin John Franklin as Isaac Chroner
*Paul Popowich as Gabriel Nancy Allen as Rachel Colby
*Stacy Keach as Dr. Michaels
*Gary Bullock as Zachariah
*Rebuka Hoye as Lisa
==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 