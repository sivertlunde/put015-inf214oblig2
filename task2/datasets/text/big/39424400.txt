We Who Are About to Die
{{Infobox film
| name           = We Who Are About to Die
| image size     = 
| image	=	
| caption        =  James Anderson (assistant)
| producer       = Edward Small
| writer         = John Twist
| based on       = book by David Lansom
| starring       = Preston Foster Ann Dvorak John Beal
| music          = 
| cinematography =  Arthur Roberts
| distributor    = RKO
| studio         = RKO
| released       = January 8, 1937
| runtime        = 
| country        = USA
| language       = English
| budget         = 
}}
We Who Are About to Die is a 1937 film. It was based on a book by David Lansom, who was trialled four times for murdering his wife before being set free. 

==Plot==
A man is arrested for murdering his wife. His lover and a sleuth work to free him.

==Production==
Lansom was hired by producer Edward Small to work on the script. Lamsons Book, "We Who Are About to Die," Will Be Produced as Film: Author Will Adapt; Beal May Be Starred Story of Father Damiens Experiences in Leper Colony Considered; Alex Esway of England to Seek Players Here
Schallert, Edwin. Los Angeles Times (1923-Current File)   11 Apr 1936: 7. 

==References==
 

==External links==
*  at TCMDB
*  at IMDB
 

 
 
 
 
 
 


 