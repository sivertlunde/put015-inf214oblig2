El Ausente
 Ausente}}
{{Infobox film |
  name     =       El Ausente
  image          = |
  caption        = Screenshot|
  director       = Rafael Filipelli|
  producer       = Rafael Filipelli |
  writer         = Rafael Filipelli|
  starring       = Verónica Castro|
  music          = Jorge Candia|
  cinematography = Andrés Silvart| Diego Gutierrez|
  distributor    = |
  released       = 1989|
  runtime        = |
  country        = Argentina|
  language       = Spanish |
  budget         = |
  followed_by    = |
}} Argentine film directed and written by Rafael Filipelli. The film starred Verónica Castro, Daniel Greco and Ana María Mazza

==Plot==
Raúl Salas is the general secretary of a major union in Córdoba, Argentina.

==Cast==
*Ricardo Bertone
*Verónica Castro Julia
*Alejandro Cuevas
*Daniel Greco ....  Lencinas
*Hugo Guzzo
*Miguel Angel Iriarte
*Ana María Mazza ....  Elena
*Pepe Novoa
*Omar Rezk ....  Raúl Salas
*Beatriz Sarlo
*Andrés Silvart
*Roberto Suter ....  Muñiz
*Omar Viale ....  Rios

==Release and acclaim==
The film premiered in 1989.

==External links==
*  

 
 
 
 
 

 
 