A Yank at Oxford
 
{{Infobox film
| name           = A Yank at Oxford
| image          = A-Yank-at-Oxford-1938.jpg
| image_size     = 220px
| caption        = Theatrical release poster Jack Conway
| producer       = Michael Balcon Michael Hogan Angus MacPhail Frank Wead John Paddy Carstairs
| narrator       = Robert Taylor Lionel Barrymore Maureen OSullivan Vivien Leigh Edmund Gwenn Edward Ward
| cinematography = Harold Rosson
| editing        = Margaret Booth Charles Frend
| studio         = MGM-British Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 102 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,374,000 "The Eddie Mannix Ledger." Margaret Herrick Library, Center for Motion Picture Study, Los Angeles. 
| gross          =  $2,736,000 
}}
 directed by Jack Conway Leon Gordon. produced by Robert Taylor, Lionel Barrymore, Maureen OSullivan, Vivien Leigh and Edmund Gwenn.
 romantic lead Waterloo Bridge (1940).  Before this film, Taylor was seen as the "romantic love interest" and thus as a 1930s equivalent to Rudolph Valentino, with men therefore starting to doubt Taylors masculinity. His casting in this film (by Mayer) was a successful attempt to put paid to such doubts, and dramatically boosted his reputation with both men and women. 

==Plot== Cardinal College, Griffith Jones), Wavertree (Robert Coote), and Ramsey (Peter Croft) on the train to Oxford. Annoyed, they trick Lee into getting off the train at the wrong stop.  Lee, however, does make his way to Oxford where the students attempt to trick him again, this time into thinking that he is getting a grand reception. Seeing through the deception, he follows the prankster impersonating the Dean and after chasing him is thrown off and ends up kicking the real Dean of Cardinal (Edmund Gwenn) before retreating.

Lee considers leaving Oxford but stays on after being convinced by Scatters (Edward Rigby), his personal servant. Lee meets Elsa Craddock (Vivien Leigh), a married woman who "helps" the new campus students, and starts a relationship with Paul Beaumonts sister Molly (Maureen OSullivan). Lee makes the track team and just when he begins to fit in, he is hazed for pushing Paul out of the way during a track meet when asked to rest. In a fit of anger, Lee goes to a local bar and finds Paul in a private booth with Elsa. He starts to fight with Paul when Wavertree comes in and warns them of campus officials coming. Lee and Paul run and when they are almost caught by one of the campus officials Lee punches him. Wavertree tells his friends that he saw Paul throw the punch and it is Paul who gets in trouble for hitting the official. He is scorned for saying it was Lee who punched him and Lee is soon the favorite of Pauls old friends. Molly begins to see him again, but Lee still feels poor for what has happened between her and Paul.
 the boat race.

==Cast==
  Robert Taylor as Lee Sheridan
* Lionel Barrymore as Dan Sheridan
* Maureen OSullivan as Molly Beaumont
* Vivien Leigh as Elsa Craddock
* Edmund Gwenn as Dean of Cardinal Griffith Jones as Paul Beaumont
* C.V. France as Dean Snodgrass
* Edward Rigby as Scatters
* Morton Selten as Cecil Davidson, Esq.
* Claude Gillingwater as Ben Dalton
* Tully Marshall as Cephas  
* Walter Kingsford as Dean Williams  
* Robert Coote as Wavertree
* Peter Croft as Ramsey  
* Noel Howlett as Tom Craddock
* Ronald Shiner as bicycle repairman (uncredited)
* Jon Pertwee as extra (uncredited, his first film)
 

==Production==
A Yank at Oxford was MGMs first British production, with MGM head Louis B. Mayer taking a personal interest in casting.  He visited the set several times. British playwright Roland Pertwee was one of several uncredited writers, and F. Scott Fitzgerald also spent three weeks working on the script, touching up rough points and adding bits of dialogue. Mayer and Balcon later got into a fight on set, within earshot of Vivien Leigh and Maureen OSullivan, that led to Balcon resigning as the producer. Looney, Deborah.  Turner Classic Movies. Retrieved: 27 January 2015. 

To the surprise of other actors, Taylor was able to do all of the physical scenes himself, especially running and rowing.  He had competed in track and field as a student at Doane College. Kral, E. A.   Nebraska History,  Volume 75, Issue 4, Winter 1994, pp. 280–290.} 

At first, Mayer was reluctant to cast the then little known Vivien Leigh in the role of Elsa Craddock, until persuaded by  , Leigh felt judged by Maureen OSullivan, whom she had befriended years earlier at school, because OSullivan was happily married and Leigh was in the midst of an affair with Laurence Olivier and awaiting word of a divorce from her first husband, Leigh Holman. Therefore, the relationship was "strained." Also Leigh had developed a foot problem whereupon she asked to go to London to seek treatment. As Leigh was preparing to leave, the wardrobe department cut a hole in her shoes so that her toe would be at ease. 

According to Leigh, she was forced to pay for her own shoes and demanded that MGM help her make some of the payments. On the other hand, MGM said that they bought all of Leighs shoes and she didnt have to pay a penny on the film. Due to the dispute, her manager, Alexander Korda, sent Leigh a message stating that if her behavior did not improve, he would not renew her contract. Leighs behavior did shape up and her contract was renewed. 
 Gone with Hollywood talking about the great English actress he had worked with and suggested to Selznick, who was still searching for his Scarlett OHara, that they ought to look at her. 
 

==Reception==
A Yank at Oxford was reviewed by Frank S. Nugent in The New York Times as a "pleasant spoof." He noted, "...   turns out to be an uncommonly diverting show. It cant be the story, for weve read the one about the old college spirit before. ... It must be the accents, the caps and gown, the cycles and the remarkably credible chaps Metro hired to play dean and tutor, scout and students. When the camera turns upon them you can jolly well smell the fog, you know." 

The film review in Variety (magazine)|Variety concentrated on Taylors appeal. "Robert Taylor brings back from Oxford an entertaining rah-rah film which is full of breathless quarter-mile dashes, heartbreaking boat race finishes and surefire sentiment—Metros first British-made film under Hollywood supervision and with Hollywood principals and director." 

A Yank at Oxford and its sequel, the 1942 A Yank at Eton, portrayed the British in a mainly positive light, and set the scene for other films that were popular in both the United States and the United Kingdom during the war years.   The film was later parodied in the Laurel and Hardy film A Chump at Oxford (1940) and remade as Oxford Blues (1984) .

===Box office===
According to MGM records the film earned $1,291,000 in the US and Canada and $1,445,000 elsewhere resulting in a profit of $513,000. 

==See also==
* Lionel Barrymore filmography

==References==
Notes
 

Bibliography
 
* Capus, Michelangelo. Vivien Leigh: A Biography. Jefferson, North Carolina: McFarland & Company, 2003. ISBN 978-0-7864-1497-0.
* Glancy, H. Mark. When Hollywood Loved Britain: The Hollywood British Film 1939-1945. Manchester, UK: Manchester University Press, 1999. iSBN 978-0-7190-4853-1.
* John Russell Taylor|Taylor, John Russell. Vivien Leigh. London: Elm Tree Books, 1984. ISBN 0-241-11333-4.
* Vickers, Hugo. Vivien Leigh: A Biography. London: Little, Brown and Company, 1988 edition. ISBN 978-0-33031-166-3.
* Alexander Walker (critic)|Walker, Alexander. Vivien: The Life of Vivien Leigh. New York: Grove Press, 1987. ISBN 0-8021-3259-6.
* Wayne, Jane Ellen. Robert Taylor. New York: Warner Paperback Library, 1973. ISBN 978-0-446-76103-1.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 