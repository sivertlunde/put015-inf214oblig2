Good (film)
{{Infobox Film
| name           = Good
| image          = Goodposter08.jpg
| image_size     =  Theatrical film poster
| director       = Vicente Amorim
| producer       = Miriam Segal
| writer         = C.P. Taylor John Wrathall
| narrator       = 
| starring       = Viggo Mortensen Jason Isaacs Jodie Whittaker
| music          = Simon Lacey Andrew Dunn
| editing        = John Wilson
| distributor    =
| released       =  
| runtime        = 96 min.
| country        = United Kingdom Germany Hungary
| language       = English
| budget         = 
| gross          = 
}} stage play of the same name by C. P. Taylor and starring Viggo Mortensen, Jason Isaacs and Jodie Whittaker. It was directed by Vicente Amorim and was first shown at the Toronto International Film Festival on 8 September 2008.

==Plot== German literature professor in the 1930s, who is reluctant at first to accept the ideas of the Nazi Party. He is pulled in different emotional directions by his wife, his mother, his mistress (Whittaker) and his Jewish friend (Isaacs). Eventually Halder gives in to Nazism in order to advance his career. He is granted an honorary position in the SS, due to his writings in support of euthanasia. His involvement in the party makes his relationship with his Jewish friend more and more fraught. Finally, Halder finds himself working for Adolf Eichmann. Under the pretext of work he engineers a visit to a concentration camp where he imagines that he sees his emaciated friend. Seeing inmates arriving and the suffering of those at the camp he realizes what his deeds have accomplished.

==Cast==
*Viggo Mortensen - John Halder 
*Jason Isaacs - Maurice Israel Glückstein
*Jodie Whittaker - Anne 
*Steven Mackintosh - Freddie 
*Mark Strong - Philipp Bouhler 
*Gemma Jones - Halders Mother
*Anastasia Hille - Helen (Halders wife)	 
*Paul Brennan - Clerk
*Steven Elder - Adolf Eichmann

==Production== Broadway in 1982. “I was simply overwhelmed by the play, and knew immediately I would do whatever was necessary to produce the film adaptation”, Segal has stated.

In 2003, 22 years after the plays premiere, she finally secured the rights. Her former classmate, Jason Isaacs, signed on to be one of the film’s executive producers, and Viggo Mortensen, who had been very impressed by the play when visiting London as a young actor in 1981, agreed to play the lead. The film was shot entirely on location in Budapest in 2007.

==Critical reception==

The film was poorly received by critics and its release was limited.  It currently holds a 34% Rotten rating on RottenTomatoes,  but a 6.1/10 rating by users on IMDb. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 