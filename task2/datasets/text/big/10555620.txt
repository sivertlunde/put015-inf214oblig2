Rough Sea at Dover
{{Infobox film
| name           = Rough Sea at Dover
| image          = Rough Sea at Dover 1896 Birt Acres Robert W Paul.webm
| image_size     =
| caption        = Full film
| director       = Birt Acres Robert W. Paul
| producer       = Birt Acres Robert W. Paul
| writer         =
| starring       =
| cinematography = Birt Acres
| editing        =
| distributor    =
| released       =  
| runtime        = 39 seconds
| country        = United Kingdom Silent
| budget         =
| gross          =
}}
 British Short short black-and-white silent documentary documentary  film directed and produced by Birt Acres and Robert W. Paul.

Paul and Acres shot the film in mid-1895, originally with the intention of showing it via kinetoscope. Projected, the film premiered on 14 January 1896 at the Royal Photographic Society in Hanover Street, London. This was the first public screening of a film in Britain and followed a month after the first such screening by the Lumière Brothers in Paris. Following its successful screening in London the film was taken to the United States where it was shown on April 23, 1896 at Koster and Bials Music Hall in New York with a series of American movies made by the Thomas Edison|Edisons company.

Despite its simple nature, Rough Sea at Dover became one of the most popular and widely-screened early British films. 

==Content==
The film consists of two distinct shots of different locations which have been edited together. It is not possible to tell with certainty whether these were edited together in the original release version. The first shot is of the rough sea as heavy waves crash against Admiralty Pier in   a film made by Acres late in 1895. 

==Current status==
Given its age, this short film is available to freely download from the Internet. It has also featured in a number of film collections including The Movies Begin - A Treasury of Early Cinema, 1894-1913. 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 