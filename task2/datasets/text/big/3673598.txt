Calendar Girl (1947 film)
{{Infobox film
| name           = Calendar Girl
| image          = File:Calendar Girl lobby card.jpg
| image_size     = 
| caption        = Lobby card
| director       = Allan Dwan
| producer       = Allan Dwan (associate producer) Richard Sale (screenplay)
| starring       = See below
| music          = Nathan Scott
| cinematography = Reggie Lanning Fred Allen
| distributor    = Republic Pictures
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Richard Sale, with a cast featuring Jane Frazee, Gail Patrick, Victor McLaglen, Irene Rich and Franklin Pangborn. This was Patricks last film before retiring from acting in the wake of her marriage.

==Plot== Boston who come to Greenwich Village in 1900, one to become a famous artist, the other to become a famous composer. The composer falls in love with the girl next door, but she is intrigued by his friend, who has secrets he feels he doesnt need to share with her.

==Cast==
*Jane Frazee as Patricia ONeill William Marshall as Johnny Bennett
*Gail Patrick as Olivia Radford Kenny Baker as Byron Jones
*Victor McLaglen as Matthew ONeill
*Irene Rich as Lulu Varden James Ellison as Steve Adams
*Janet Martin as Tessie
*Franklin Pangborn as Dilly Dillingsworth
*Gus Schilling as Ed Gaskin
*Charles Arnt as Capt. Olsen
*Lou Nova as Clancy
*Emory Parnell as The Mayor

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 