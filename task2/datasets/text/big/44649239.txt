Darwin (2015 film)
{{Infobox film
| name           = Darwin
| image          = 
| alt            = 
| caption        = 
| director       = Alain Desrochers
| producer       = Jason Ross Jallet   B. P. Paquette   Susan Schneir   Kim  Berlin
| writer         = Robert Higden
| starring       = Molly Parker   Nick Krause   Juliette Gosselin   Jordyn Negri   Daniel DiVenere
| music          = 
| cinematography = Tobie Marier-Robitaille
| editing        = Éric Drouin
| studio         = Nortario Films   Suki Films
| distributor    = Suki International
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Darwin (formerly titled ESC) is an upcoming Canadian science fiction film directed by Alain Desrochers and written by Robert Higden. The film stars Molly Parker, Nick Krause, Juliette Gosselin, Jordyn Negri, and Daniel DiVenere.         

Principal photography on the film commenced June, 2014 in Greater Sudbury, Ontario, Canada, and wrapped-up July, 2014. Nortario Films, based in Greater Sudbury, is producing the film with Montreal-based Suki Films, and the film was financed by Telefilm Canada, the Northern Ontario Heritage Fund Corporation, the Ontario Media Development Corporation, and the Harold Greenberg Fund. It is scheduled for release summer, 2015.   

== Plot ==
Set in an oppressive future where everyone connects via the computer, Darwin tells the adventure of a young man (portrayed by Nick Krause) who is forced out into the real world and discovers truths about that world and his own life that he never dreamed of.   

== Cast ==
* Nick Krause as Darwin
* Molly Parker as Lilllian
* Juliette Gosselin as Dara
* Jordyn Negri as Beth
* Daniel DiVenere as Steven

== Release ==
Suki International has set the film for a summer 2015 release.  

== References ==
 

== External links ==
*  

 
 
 


 
 