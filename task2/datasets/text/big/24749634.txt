Fast Track: No Limits
 
 
{{Infobox film
| name = Fast Track: No Limits
| image =
| caption =
| director = Axel Sand
| producer = Hermann Joha
| writer = Lee Goldberg Andrew Walker Joseph Beattie Alexia Barlier
| music = Dirk Leupolz
| cinematography = Axel Sand
| editing = Martin Rahner ProSieben Media AG   Maverick Entertainment Group  
| released = 21 February 2008  
| runtime = 95 minutes
| country = Germany
| language = English
}}
 Andrew Walker.

The film was shot in English, with American, British, Canadian, French, and German actors, on location in Berlin, Germany. Cars for the film were supplied by BMW, Audi, and Subaru. It aired first on ProSiebenSat.1 in February 2008, and was released theatrically in China by Beijing Time Entertainment. The film has been broadcast on television and released on DVD in dozens of countries, and it was released on DVD in the United States by Maverick Entertainment Group.

==Plot==
For four young people; Katie, Mike, Eric, Nicole; speed is a way of life. But the four soon come to realize that living life in the fast lane carries a very high price. On the streets debts are not settled with cash, they are settled in blood. Each character has a different motive and a different goal but when they get behind the wheel they are all the same. It is the speed that connects them, but the one thing they all have in common will be the one thing that tears them apart.

The film starts with bank giving Katie, owner of Carls Garage warning about the debt on her garage. They then order pizza for lunch which is to be delivered by Mike, a pizza delivery guy. On his way to delivery his way is blocked by Nicoles broken down car and then by a police chase between Eric, a police officer and Wolf, the bank robbers wheel man making him run out of time for delivering pizzas on.

On the roads Katies Subaru is driven by a mysterious Phantom driver in the illegal racing events thereby earning her extra money for paying her debts. Meanwhile in garage Mike sees Nicoles BMW which he steals to prove he is a good racer to Katie by participating and winning in illegal racing. There he races against Wolf and ultimately wins the race but wrecks the BMW.

Next day when Nicole sees what has become of her husbands car, Mike apologizes saying that a car can be fixed but one cant buy what it feels like after winning. This earns Katie Nicoles contract for building her a luxury sports car. Meanwhile Mike is hired by Gargolov, a crime lord as a wheel man in place of Wolf.

Eric is investigating the bank robberies and figures out that Wolf is the former wheel man of Gargolov and also comes to know that Mike is the new one. He investigates Mikes background and comes to know that he is a wanted convict.

As ordered Nicoles new sports car is made by Katie on which Mike teaches her how to race, drive and drift.

In the proceeding later Mike and Eric becomes friends and races against Wolf for the last time where Wolfs car crashes at the end of the race thereby killing him.

==Cast==
*Erin Cahill as Katie Reed Andrew Walker as Mike Bender Cassidy
*Joseph Beattie as Eric Marc Visnjic
*Alexia Barlier as Nicole Devereaux
*Nicholas Aaron as Wolverine
*Pasquale Aleardi as Gregor Gargolov
*Maurice Roëves as Schmitty
*Jack Bence as Rainer
*Tim Dantay as Neubeck
*Ill-Young Kim as Markus
*Shaun Prendergast as Heinrich
*Carl Stück as Porsche Thief
*Anna Tkatschenko as Race Diver

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 