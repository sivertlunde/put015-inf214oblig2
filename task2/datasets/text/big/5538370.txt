The Sinister Urge (film)
{{Infobox film
| name = The Sinister Urge
| image = The Sinister Urge.jpg
| caption = Theatrical release poster
| director = Ed Wood
| producer = Ed Wood
| writer = Ed Wood
| narrator = James "Duke" Moore Jean Fontaine Carl Anthony Dino Fantini Jeanne Willardson Harvey B. Dunn Reed Howes Fred Mason Conrad Brooks
| music = Manuel Francisco William C. Thompson
| editing = John Soh
| distributor = Headliner Productions
| released =  
| runtime = 71 min
| country = United States English
}}

The Sinister Urge is a 1960 crime drama film that was written and directed by Ed Wood. The film was featured in episode 613 of the cult television show Mystery Science Theater 3000.

== Synopsis ==
The film revolves around a series of murders of young women. The opening scene features an unnamed blonde woman (Betty Boatner) running along a mountain road, wearing only her undergarments. She is clearly scared for her life. She enters a city park and manages to reach a telephone booth. Her attempt to escape her assailant ends in failure, when the man also reaches the booth. He quickly kills her and leaves her corpse next to a lake. The corpse is soon discovered by visitors of the park. Craig (2009), p. 200-216  The scene shifts to a police station, where Police Lieutenant Matt Carson (Kenne Duncan) receives a phone report on the latest murder. He instructs subordinate Officer Kline (Fred Mason) to gather information from the witnesses who called in the discovery of the body. Then he summons his associate Sergeant Randy Stone (Duke Moore) and they head out towards the crime scene. They take note of the youthful appearance of the victim and suspect a connection to the "smut picture racket". Craig (2009), p. 200-216 

The next scene is more light-hearted in nature, taking place in the film studio of pornographic director Jaffe (Harry Keatan), an elderly man who is somewhat elfin in appearance. Jaffe is currently shooting a scene involving three women posing for the camera. He fusses over the details of the scene he is shooting, in a scene both depicting and mocking his artistic pretensions. Craig (2009), p. 200-216  His superior Johnny Ryde (Carl Anthony) interrupts him to bring orders from their boss, Gloria Henderson (Jean Fontaine). She wants their inventory moved to a safer location. Jaffe promises to do so after completing of his shooting. He never actually finishes, since Carson leads a police raid into the studio. All participants in the shooting are arrested, and the inventory of films passes into police custody. Craig (2009), p. 200-216 

Back at the police station, Carson and Stone are berated by their superior, who demands quicker action in exposing the racket. Afterwards the two officers are visited by local businessman Mr. Romaine (Harvey B. Dunn), who demands to know why are taxpayers money wasted in persecuting harmless deviances such as pornography. In his view they should be preoccupied with more serious crimes and with the gangs of young hoodlums roaming the streets. In response, Carson claims that the dirty picture racket is connected to major crimes. He then reveals images of the murder victims, and explains the connection between them working for the racket and their violent deaths. He then asks the shocked Romaine if he has a daughter. As he exits the door to leave, the visibly shaken Romaine explains that he has two daughters. Craig (2009), p. 200-216 

The next scene takes place in the suburban house of Gloria Henderson. Gloria is visited by Johnny Ryde, who informs her of the recent police raid. The conversation shifts to Shirley, a person permanently silenced by Dirk. This is the first time the name of the killer is revealed in the film. The conversation also reveals that Shirley had learned too much about the inside operations of Glorias organization and was attempting to blackmail them. Johnny states that he is worried about Dirk himself, since he clearly enjoys using his knife. Craig (2009), p. 200-216  The conversation next moves to another operation of the pornography ring. They have been peddling bondage photos to high school kids, but these young customers have started demanding newer material. Craig (2009), p. 200-216 
 street fight attracting onlookers. Inside Jake, the owner, finds time to negotiate the purchase of more smut pictures from Janet, an agent of the pornography ring. He complains, however, about the inventory of photos he already has. Claiming that copies are already owned for everyone from ages 7 to 70, so finding new customers is hard. Craig (2009), p. 200-216  The fight outside attracts the attention of the police, so Jake and others are arrested. The two street fighters are revealed to be low-level smut peddlers, who were fighting over turf area. Craig (2009), p. 200-216 

Later, Janet leads three other women into attacking Clauson, a distributor of smutty pictures who was embezzling funds of the organization. The man ends up hospitalized. Craig (2009), p. 200-216  The scene shifts back to Glorias house. Gloria and Johnny are watching 16 mm films, projected onto a screen, and conversing. Gloria points that the increasingly unstable Dirk is both aroused and triggered to kill by viewing pornography. Johnny claims that he can still keep the killer under control. Craig (2009), p. 200-216  Meanwhile, Dirk has returned to the park. He is flirting with a woman and the two kiss. But then his lust turns to murderous rage. He strips off her clothes and then stabs her to death. Craig (2009), p. 200-216 

In the police station, Carson and Stone start discussing the fates of the women victimized by pornography. Mary Smiths from Everywhere, USA who graduate at the top of their class, were once great in a school play, and come into Hollywood seeking stardom. Craig (2009), p. 200-216  The scene shifts to following the path of one Mary Smith (Jeanne Willardson). The inexperienced actress is rejected by film studios and talent agents, before offered work by Johnny Ryde. Who introduces himself as a director, without explaining the kind of films he directs. With childlike naivety, Mary agrees to work hard for him. She receives money for her expenses long before filming starts. She first becomes economically dependent on her employers, then she resigns into posing for revealing pictures. Craig (2009), p. 200-216 

The scene shifts from the fate of Mary Smith to the exterior of Glorias house, where Dirk is seen lurking. He breaks into the house and gets his hands into more smutty pictures. By this point, he is clearly addicted to both porn and killing. Later he returns to the park and finds another victim, a young woman who was feeding the ducks. Craig (2009), p. 200-216  This time the murder has repercussions for him. He leaves behind photos which he had touched with his bare hands. Through his fingerprints, the police discover the identity of the killer. The identity of the killer is leaked to the press, making the headline of the Hollywood Chronicle.  Glorias own overlords in "the organized crime|Syndicate" then order her to remove Dirk permanently, as he is an unruly employee who is drawing negative attention for their organization. Craig (2009), p. 200-216 

Gloria and Johnny next argue over the best way to accomplish this. Johnny suggests an indirect way of killing Dirk, by sending on an errand in an old car with faulty brakes. In other words, arranging a fatal "accident" for him. As if aroused by the idea, the two partners then share a passionate embrace. Craig (2009), p. 200-216 Dirk survives his "accident" by leaping out of the car, then comes to Glorias house seeking vengeance. He finds it empty and them manages to ambush an arriving Johnny. Johnny temporarily manages to convince his assailant that they could work together to topple Gloria off her leadership position. Dirk then murders Johnny, before being shot in the dark by Gloria, who assumed that she was shooting Johnny. The film ends with Glorias disbelief that she shot the wrong man as the police arrest her. Craig (2009), p. 200-216 

==Production and analysis==
In 1959, Ed Wood completed a screenplay titled The Racket Queen. Producer Roy Reid of Headliner Productions was willing to fund the project, though Wood had to revise his script in early 1960. The result was The Sinister Urge, which was filmed primarily in July 1960. Craig (2009), p. 200-216  The film project was influenced by a box office hit of the time, Psycho (1960 film)|Psycho (June 1960) by Alfred Hitchcock. Both films were about sexually motivated psychopaths, and Reid and Wood likely aimed to capitalize on the similarity of their concepts. Craig (2009), p. 200-216 

Rob Craig suggests that the film can be seen as an early entry in a new subgenre of   (1981) by Andrea Dworkin. Craig (2009), p. 200-216 
 voyeur was Peeping Tom (1960). Craig (2009), p. 200-216 

Craig suggests that Officer Kline serves as a stand-in for Officer Kelton, a recurring character in Wood films. The main difference between the two characters being that Kelton served as a comic relief, while Kline seems humorless--perhaps because comic relief would seem out of place in a film about violent sexual death. Craig (2009), p. 200-216 

The inventory of films captured in the police raid is represented by the image of a motion picture editing room, containing numerous film cans. Craig suggests that the scene may depict the actual editing room where Wood edited his films. Craig (2009), p. 200-216 

The film includes a fight scene Wood shot for his unfinished project Hellborn, a.k.a. Rock and Roll Hell. The scene is edited to include footage of Dino Fantinis character observing the events and some additional dialogue audio to connect the scene to the films plot. The same fight scene was also used in another of Woods films, Night of the Ghouls. Craig (2009), p. 200-216 
 The Young Marrieds, were to some degree pornographic. Craig (2009), p. 200-216 

The   (1954), Bride of the Monster (1955), The Violent Years (1956), and Plan 9 from Outer Space (1959). Craig (2009), p. 200-216 

Craig finds that the film functions well as an "engaging and coherent" melodrama, as a work of social criticism, and as a treatise against the exploitation of women. All this was accomplished with a Skid row budget of 20,000 dollars. Craig (2009), p. 200-216  He notes this was the swan song for cinematographer William C. Thompson, who was losing his eyesight. For Wood himself, it was his last mainstream work as both writer and director. He would subsequently write screenplays for other exploitation films and direct pornographic films. Craig (2009), p. 200-216 

==Cast==
* Kenne Duncan as Lt. Matt Carson James "Duke" Moore as Sgt. Randy Stone
* Carl Anthony as Johnny Ryde
* Dino Fantini as Dirk Williams
* Jean Fontaine as Gloria Henderson
* Conrad Brooks as Connie
* Harvey B. Dunn as Mr. Romaine
* Harry Keatan as Jaffe
* Reed Howes as Police Inspector
* Fred Mason as Officer Kline
* Ed Wood as rival pornographic distribution worker in fight
* Vic McGee as Syndicate man
* Jeanne Willardson as Mary Smith

==See also==
* Edward D. Wood, Jr. filmography

==References==
 
* The Haunted World of Edward D. Wood, Jr. (1996), documentary film directed by Brett Thompson
* Rudolph Grey, Nightmare of Ecstasy: The Life and Art of Edward D. Wood, Jr. (1992) ISBN 978-0-922915-24-8

== Sources ==
*  

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 