Not Any Weekend for Our Love
{{Infobox film
| name           = Pas de week-end pour notre amour
| image          = 
| caption        = 
| director       = Pierre Montazel
| producer       = Les Films Gloria
| writer         = Pierre Montazel
| starring       = Luis Mariano Jules Berry
| music          = Roger Lucchesi
| cinematography = 
| editing        = 
| distributor    = U.F.P.C.
| released       = 23 January 1950 (France)
| runtime        = 96 minutes
| country        = France
| awards         = 
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 French film from 1950, directed by Pierre Montazel, written by Pierre Montazel, and starring by Luis Mariano. The film was also features Louis de Funès. 

== Cast ==
* Luis Mariano: Franck Reno, the barons son, singer
* Jules Berry: Baron Richard de Valirman
* Louis de Funès: Constantin, the Baron de Valirmans servant
* Maria Mauban: Margaret Duval, journalist
* Denise Grey: Gabrielle, the wife of Alvarez, Francks mother
* Bernard Lajarrige: Christian, Francks brother
* Jean Ozenne: Bertrand Touquet, Lauries father
* Jean Carmet: the pianist

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 