Ever Since Eve
{{Infobox film
| name           = Ever Since Eve
| image size     = 
| image	=	Poster - Ever Since Eve (1937) 01.jpg
| caption        = 
| director       = Lloyd Bacon
| producer       = Earl Baldwin (uncredited)
| writer         = Gene Baker (story) Margaret Lee (story) Earl Baldwin Lillie Hayward Lawrence Riley Brown Holmes (uncredited dialogue) Robert Montgomery
| music          = 
| cinematography =  William Holmes
| distributor    = Warner Bros.
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Robert Montgomery.

==Plot==
Marge Winton (Marion Davies) is fed up with having to quit job after job to avoid the advances of lecherous bosses. When she goes to the employment agency, she is surprised to discover that she is too beautiful for one position. So she gives herself a makeover, hiding her blond curls under a dark, severe wig, putting on glasses, and wearing a drab, unflattering dress.

The disguise works. Book publisher Abigail Belldon (Louise Fazenda) hires her as a secretary for lazy writer Freddy Matthews (Robert Montgomery). Freddy would rather go out and party with his girlfriend Camille Lansing than start on his novel. Abigail has already sold the film rights, and the deadline for delivering the book to the film studio is fast approaching. She figures a plain secretary will be one less distraction.

Despite his initial displeasure at Marges appearance, Freddy gives in and accepts her. However, Camille keeps taking up too much of Freddys time and attention, and Marge begins to fall for him as well. Thus, Marge has plenty of reason to try to sabotage their relationship. When this is discovered, she quits.

A complication arises when Freddy decides to rehire her. He shows up at her apartment unexpectedly and sees her without her disguise, so she has to pretend to be her roommate Sadie (Patsy Kelly). They spend the entire evening and part of the morning getting acquainted.

With the deadline only days away, however, Marge pretends to go out of town for a couple of weeks. The plan backfires. Instead of writing, Freddie goes after her. Camille finds out and follows as well. Marge has no choice but to show up at the hotel, registering first as the plain secretary, then as Sadie, juggling her two personas to keep Freddie in the dark. She finally gets an outline from him for the last few chapters, which she uses to finish the novel on her own.

Since he gave the outline to "Sadie", and she had no opportunity to give it to Marge, Freddie finally realizes that they are one and the same. He decides to marry her anyway.

==Cast==
* Marion Davies as Marjorie "Marge" Winton Robert Montgomery as Freddie Matthews
* Frank McHugh as Mike McGillicuddy, a writer of girls books under the nom de plume "Mabel DeCraven"
* Patsy Kelly as Sadie Day
* Allen Jenkins as Jake Edgall, Sadies dimwitted plumber boyfriend
* Louise Fazenda as Abigail Belldon
* Barton MacLane as Al McCoy, Jakes boss
* Marcia Ralston as Camille Lansing
*Fredrick R. Clark as Alonzo, Freddies suave African American butler
* Arthur Hoyt as Mr. Cuddleton
* Mary Treen as Employment Clerk

== External links ==
 
*  
*  
*  

 

 
 
 
 
 
 