Asso (film)
{{Infobox film
| image =Asso (film).jpg
| director = Castellano & Pipolo
| writer =Castellano & Pipolo
| starring = Adriano Celentano  Edwige Fenech  Mario & Vittorio Cecchi Gori
| music          = Detto Mariano
| cinematography = Danilo Desideri
| editing         = Antonio Siciliano
| studio = Intercapital
| distributor= Cecchi Gori Home Video  Ízaro Films
| released =  
| runtime = 90 minutes
| country = Italy
| language = Italian
}}
 Italian comedy film, directed by Franco Castellano and Giuseppe Moccia and released in 1981. The film stars Adriano Celentano, Edwige Fenech, Renato Salvatori, Sylva Koscina, Pippo Santonastaso, Gianni Magni and Elisabetta Viviani. It was filmed in Italy.

==Plot==
Asso, an ace poker player, has just married his long-time love, Silvia, the most coveted woman in his neighborhood. For her sake, he promises to give up poker, but the unexpected arrival of a notorious player in town and his wifes permission push him into one last game, which he wins. But as he tries to return home the following morning, he is apprehended by the Killer, one of his closer acquaintances, who has been hired to do away with Asso himself.

Shot several times, Asso dies but returns as a ghost out of worry for leaving his wife behind without any means of supporting herself. Because of their close bond, Silvia can see and hear him, but to anyone else Asso is invisible. After managing to convince his wife that he really is dead, he tries to get her remarried so she wont end up on the streets, and his choice falls on the feeble but rich banker Morgan. Morgan is of course delighted to win the attention of such a beautiful lady, but this invites the jealousy of Bretella, Assos rival over Silvias hand. As it turns out, Bretella had commissioned the Killer to assassinate Asso and now tries to hire him again to finish off Morgan. Shaken by remorse over killing Asso, the Killer refuses and even promises to go to the police, whereupon Bretella kills him and decides to do the job himself.

However, after getting a short leave of absence from Hell, the Killer reveals Bretellas true intentions to Asso, who rushes to Morgans protection. While he cannot prevent Bretella from kidnapping the banker, he does prevent Morgan from getting killed and gets Bretella arrested. In his desperation, however, Morgan has started praying to his dead wife for salvation, and once rescued, he revokes his courtship of Silvia due to what he perceives to be his wifes grace. Alone once more (save for Assos company), Silvia returns to the gambling den where she met Asso for the first time, only to encounter a young card player who is a dead look-alike of Asso. Once she falls for him, Asso fades away and goes to Heaven, where God himself (Celentano) invites him to a round of poker and, following Assos defeat, a rematch in about three thousand years.

==Cast==
* Adriano Celentano as Asso
* Edwige Fenech as Silvia
* Renato Salvatori as Bretella
* Sylva Koscina as Enrichetta
* Pippo Santonastaso as Morgan
* Gianni Magni as The Killer
* Elisabetta Viviani as The Tourist
* Dino Cassio as The Commissioner
* Sandro Ghiani as The Cop
* Raffaele di Sipio as The Choreographer
* Gianni Musy as The Speaker
* Gerry Bruno as Ettore Bruno

==Reception==

===Critical response===
Based on 606 user votes, IMDB grades the movie at 6.2. 

==References==
 
 

==External links==
*   at Internet Movie Database
*   at Rotten Tomatoes

 

 
 
 
 
 