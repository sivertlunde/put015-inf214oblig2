Pudsey: The Movie
{{Infobox film
| name           = Pudsey The Dog: The Movie
| image          = Pudsey_Movie_poster.jpg
| caption        = UK theatrical release poster Nick Moore
| producer       = Simon Cowell  Paul Rose
| based on       = The Dog from the dancing duo Ashleigh and Pudsey
| starring       = David Walliams Jessica Hynes Jim Tavaré John Sessions Izzy Meikle-Small
| cinematography = Ali Asad
| music          = Simon Woodgate
| editing        = Daniel Farrell
| studio         = Syco Entertainment Infinite Wisdom Studios Vertigo Films Roar Films
| distributor    = Vertigo Films Cathay-Keris Films (Singapore) Silverline Multimedia (Philippines)
| released       =  
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
| budget         = £2.5 million 
| gross          = £2,549,705   
}} 2014 British family comedy Nick Moore, Paul Rose and starring Pudsey the Dog, one half of the dancing duo Ashleigh and Pudsey, voiced by David Walliams.   Other stars include Jessica Hynes, John Sessions and Jim Tavaré. The film was made by Vertigo Films and Syco Entertainment and was released in the United Kingdom on 18 July 2014, after it was originally set to be released in December the previous year.    

==Plot==
The film is about 2012 Britains Got Talent winner Pudsey the dog on an adventure.  Pudsey uses his ability to walk on his hind legs and knock people over to save the day. Pudsey starts as a dog in a movie set, but he ends up fired after causing havoc. Pudsey runs away in disgrace and catches a bus, and there are several school kids on there. When the bus stops, Pudsey runs down the street. Some bullies from the bus bully some other kids. Pudsey runs back and knocks the bullies over. The bullied kids take Pudsey home with them. They are siblings and their names are Molly, Tommy and George. Tommy does not speak much. The kids mum gets rid of Pudsey so the family can move house after the dad died. Molly, George and Tommy take Pudsey to a lady who looks after dogs. But the lady turns out to be evil after she says she is going to dye him pink and do horrible things to him like she has done to some poodles. Pudsey, because he is specially trained, opens the door and runs away, and jumps into a white van. The poodles escape too. The white van turns out to be the van that is moving the siblings things, so Pudsey is moved to Chuffington with the family.

==Cast==
*David Walliams as Pudsey
*John Sessions as Thorne
*Jessica Hynes as Gail
*Izzy Meikle-Small as Molly
*Malachy Knights as Tommy
*Luke Neal as Farmer Jack
*Amanda Holden as Someone
*Lorraine Kelly as Cat (fired due to the original actor being withdrawn)
*Dan Farrell as Ken and Finn
*Jim Tavare as The Dog Catcher

==Production==
In January 2013, it was announced that Simon Cowell would produce Pudsey The Dog: The Movie. The film, which was released in the UK on 18 July 2014, follows Pudsey and his siblings Molly, George and Tommy as they move to the village of Chuffington-on-Sea with their mother Gail (Jessica Hynes) and set out to save the village from their landlord Mr Thorne (John Sessions) and his cat Faustus. Pudsey is voiced by comedian David Walliams.

==Soundtrack==
* Pudsey: Hes Got The Love - Performed by Echobass
* Things Are Getting Better - Performed by Echobass
* Breaking It Down - Performed by Echobass Norman Warren
* All Music - Composed by Simon Woodgate

==Release==
Pudsey the Dog: The Movie was released in cinemas in the UK on August 18 2014.
===Home media===
Pudsey the Dog: The Movie was released on DVD on November 10 2014 in the UK.

==Reception==
===Box office===
In its first week, the film grossed £446,000, finishing outside of a Top 5 led by Dawn of the Planet of the Apes with £8.7 million. This was described by the BBC as a "flop".   
===Critical reception===
Pudsey: The Movie was critically panned. On  , Mark Kermode said "nothing can explain (or excuse) the sheer skull-scraping ugliness of this relentlessly tacky Britains Got Talent spin-off... If you paid to see this, you would feel duty-bound to demand your money back; I saw it for free and still wanted a refund."  David Edwards of the Daily Mirror called the film a "cheap and cheerless embarrassment" with a "thin and familiar" plot and said it "deserves to be scraped from the lawn, and dropped in the bin." 

==See also==
 
*List of films with a 0% rating on Rotten Tomatoes

==References==
 

==External links==
* 
*Pudsey the Dog: The Movie at the Internet Movie Database
*Pudsey the Dog: The Movie at Rotten Tomatoes

 
 
 
 
 
 