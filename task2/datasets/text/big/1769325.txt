Doctor in the House
 
 
 
 
{{Infobox film
|  name           = Doctor in the House
|  image          = Doctor in the House poster.jpg
| caption        = British original cinema poster
|  director       = Ralph Thomas Richard Gordon Ronald Wilkinson
| based on       =   
| cinematography = Ernest Steward
|  starring       = Dirk Bogarde Muriel Pavlow Kenneth More Donald Sinden Kay Kendall James Robertson Justice Donald Houston
|  producer       = Betty E. Box
| studio = Rank Organisation GFD (UK) Republic Pictures (US)
|  released       =  
|  runtime        = 87 min
|  country        = United Kingdom
|  language       = English
|  budget         = £120,000 
}} Richard Gordon and Ronald Wilkinson, is based on the novel by Gordon, and follows a group of students through medical school.
 six sequels, Doctor in the House.

It made Dirk Bogarde one of the biggest British stars of the 1950s. Other well-known British actors featured in the film were Kenneth More, Donald Sinden and Donald Houston. James Robertson Justice appeared as the irascible chief surgeon Sir Lancelot Spratt, a role he would repeat in many of the sequels.

==Plot==
The story follows the fortunes of Simon Sparrow (Dirk Bogarde), starting as a new medical student at the fictional St Swithins Hospital in London. His five years of student life, involving drinking, dating women, and falling foul of the rigid hospital authorities, provide many humorous incidents.

When he has to leave his first choice of lodgings to get away from his landladys amorous daughter (Shirley Eaton), he ends up with three amiable but less-than-shining fellow students as flatmates:
*Richard Grimsdyke (Kenneth More). A relative had left him a small but adequate annuity while he remains in medical school, so he sees to it that he flunks each year.
*Tony Benskin (Donald Sinden), an inveterate woman chaser. rugby fanatic.
Towering over them all is the short-tempered, demanding chief surgeon, Sir Lancelot Spratt (played by James Robertson Justice in a manner quite unlike Gordons original literary character), who strikes terror into everyone.

Simons friends cajole him into a series of disastrous dates, first with a placidly uninterested "Rigor Mortis" (Joan Sims), then with Isobel (Kay Kendall), a woman with very expensive tastes, and finally with Joy (Muriel Pavlow), a nurse at St Swithins. After a rocky start, he finds he likes Joy a great deal. Meanwhile, Richard is given an ultimatum by his fiancée Stella (Suzanne Cloutier) – graduate or she will leave him. He buckles down.

The climax of the film is a rugby match with a rival medical school during Simons fifth and final year. After St Swithins wins, the other side tries to steal the school mascot, a stuffed gorilla, resulting in a riot and car chase through the streets of London. Simon and his friends are almost expelled for their part in this by the humourless Dean of St Swithins (Geoffrey Keen). When Simon helps Joy sneak into the nurses residence after curfew, he accidentally falls through a skylight. This second incident gets him expelled, even though he is a short time away from completing his finals. Sir Lancelot, however, has fond memories of his own student days, particularly of the Deans own youthful indiscretion (persuading a nurse to reenact Lady Godivas ride). His discreet blackmail gets Simon reinstated. In the end, Richard fails (as does Tony), but Stella decides to enroll at St Swithins herself so there will be at least one doctor in the family. Simon and Taffy graduate.

==Cast==
 
*Dirk Bogarde as Simon Sparrow
*Muriel Pavlow as Joy Gibson
*Kenneth More as Richard Grimsdyke
*Donald Sinden as Tony Benskin
*Kay Kendall as Isobel
*James Robertson Justice as Lancelot Spratt
*Donald Houston as Taffy Evans
*Suzanne Cloutier as Stella
*George Coulouris as Briggs
*Jean Taylor Smith as Sister Virtue
*Nicholas Phipps as Magistrate
*Geoffrey Keen as Dean
*Martin Boddey as Lecturer at pedal machine
*Joan Sims as "Rigor Mortis"
*Gudrun Ure as May
*Harry Locke as Jessup
*Cyril Chamberlain as Policeman
*Ernest Clark as Mr Parrish
*Maureen Pryor as Mrs. Cooper George Benson as Lecturer on drains
*Shirley Eaton as Milly Groaker
*Eliot Makeham as Elderly Examiner
*Joan Hickson as Mrs. Groaker
*Brian Oulton as Medical equipment salesman
*Shirley Burniston as Barbara
*Mark Dignam as Examiner at microscope
*Felix Felton as Examiner
*Lisa Gastoni as Jane
*Felix Felton as Examiner (uncredited)
*Wyndham Goldie as Examiner (uncredited)
*Douglas Ives as Sprogett
*Anthony Marlowe as Paul
*Geoffrey Sumner as Forensic Lecturer
*Amy Veness as Grandma Cooper
*Mona Washbourne as Midwifery sister
*Mary Chapman as Girl (uncredited) Richard Gordon as Anaesthetist (uncredited)
*Fred Griffiths as Taxi driver (uncredited)
*Joan Ingram as Woman (uncredited) Noel Purcell landlord at the doctors pub (uncredited)
*Bruce Seton as Police driver (uncredited)
*Richard Wattis as Medical book salesman (uncredited)
*Carol White as Bit Role (uncredited)
 

==Awards==
*Won, 1955 BAFTA Film Award for Best British Actor, Kenneth More
*Nominated, 1955 BAFTA Film Award, Best British Film
*Nominated, 1955 BAFTA Film Award, Best British Screenplay, Nicholas Phipps
*Nominated, 1955 BAFTA Film Award, Best Film from any Source

==Production== Rank executives that people would go to a film about doctors, and that Bogarde, who up to then had played spivs and World War Two heroes, had sex appeal and could play light comedy. They got a low budget, and were only allowed to use available Rank contract artists.

St Swithins hospital is represented by the front of University College London.

Kenneth More had just made Genevieve (film)|Genevieve (1953) when he signed to appear in the cast, but that Genevieve had not been released yet. Accordingly his fee was only £3,500. 

==Sequels==
  Michael Craig and Leslie Phillips, and the other two with Phillips, as well as a successful television series from London Weekend Television.

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 