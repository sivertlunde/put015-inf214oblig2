Troublesome Night 3
 
 
{{Infobox film
| name = Troublesome Night 3
| image =
| caption =
| film name = {{Film name| traditional = 陰陽路之升棺發財
| simplified = 阴阳路之升棺发财
| pinyin = Yīn Yáng Lù Zhī Shēng Guān Fā Cái
| jyutping = Jam1 Joeng4 Lou6 Zi1 Sing1 Gun1 Faat3 Coi4}}
| director = Herman Yau
| producer = Nam Yin
| writer = Kenneth Lau Chang Kwok-tse
| starring = 
| music = Mak Jan-hung
| cinematography = Joe Chan
| editing = Chan Kei-hop
| studio = Wins Entertainment Ltd. Nam Yin Production Co., Ltd.
| distributor = China Star Entertainment Group
| released =  
| runtime = 99 minutes
| country = Hong Kong
| language = Cantonese
| budget =
| gross = HK$5,730,755
}}
Troublesome Night 3 is a 1998 Hong Kong horror comedy film produced by Nam Yin and directed by Herman Yau. It is the third of the 19 films in the Troublesome Night film series.

==Plot==
The film consists of three loosely connected segments based on the central theme of a mortuary. Mortician Shishedo is grief-stricken when his favourite pop star, Lam Wing-si, is killed in a car accident. Lams face was badly marred and Shishedo takes her place in the coffin by disguising himself as the deceased singer. He disappears and leaves his colleagues to deal with the ghostly aftermath. In the next segment, Gigi wants a memorial service for her late mother, who had hanged herself. The ghost of Gigis mother is displeased when the greedy morticians try to trick her daughter into using their services, and she haunts them. The last segment involves a mortician named Hung, who commits suicide after her boyfriend dumps her because of her job. Her boyfriend faces retribution when her ghost returns to haunt him and he dies in an accident later.

==Cast==
* Louis Koo as Cheng Lik
* Vincent Kok as Policeman
* Lee Kin-yan as Man
* Lee Lik-chi as Bill Chan
* Helen Poon as Mrs Wong
* Simon Lui as Chan Tai-cheung
* Fennie Yuen as Hung
* Rain Lau as Ann
* Allen Ting as Shishedo
* Frankie Ng as Sun Kwai
* Emotion Cheung as Trump
* Chin Kar-lok as Rock
* Natalie Wong as Elaine
* Michael Tse as Daviv
* Wallis Pang as Beauty Chans sister
* Lo Mang
* Shing Fui-On as Loanshark Wah
* Law Lan as Gigis mother
* Christine Ng as Gigi Cheung
* Vicky Hung as Davivs new girlfriend
* Oliveiro Lana as Beauty Chan
* Lee Kim-wing
* Hui Si-man

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 
 