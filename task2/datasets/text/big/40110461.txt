Red Ant Dream
{{Infobox film
| name = Red Ant Dream
| film name =  
| image =
| caption = Red Ant Dream
| director = Sanjay Kak
| writer = Sanjay Kak Tarun Bhartiya
| cinematography = Sanjay Kak Ranjan Palit Setu Pandey
| editing = Tarun Bhartiya
| music = Word Sound Power / Delhi Sultanate & Chris McGuiness
| released =  
| runtime = 120 minutes
| country = India Punjabi
}} Maoist movement in India. The documentary has been screened in various cities in India since May 8, 2013.

== Synopsis == Maoist insurgency Bastar in Chhattisgarh, tribals fighting against industrialists in Niyamgiri in Odisha, and protestors acting in memory of the Leftist revolutionary Bhagat Singh in Punjab.  This film shows the people of these regions resisting what they believe to be oppression.      Red Ant Dream was financed by funds given by an International Documentary Film Festival Amsterdam Fund grant and a prize from the Busan International Film Festival. 

==Reviews==
Sheikh Saaliq, editor of The Vox Kashmir, in one of his reviews in The Express Tribune said, "This film is a saga of heroism and sacrifice in the face of a brutal enemy camouflaged as the world’s “largest democracy”. Red Ant dream is a moving meditation."   

== References ==

 

== External links ==
*  
*  

 
 
 
 
 

 