Handphone (film)
{{Infobox film
| name           = Handphone
| image          = Handphone.jpg
| director       = Kim Han-min
| producer       = Heo Tae-gu Kim Min-gi  Heo Chang Eom Ju-yeong
| writer         = Kim Mi-hyeon
| starring       = Uhm Tae-woong Park Yong-woo
| music          = Kim Jun-seong
| cinematography = Park Sang-hun
| editing        = Kim Sun-min
| distributor    = 
| released       =  
| runtime        = 137 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
Handphone ( ) is 2009 South Korean crime film.  

==Plot==
Talent manager Seung-min sees Yoon Jin-ah, a rising actress, as his one last hope to turn his life around. Just as Jin-ah is on the path to stardom, he receives a threat from her former lover and gets her sex clip on his phone. Seung-min tracks down the culprit and retrieves the tape but ends up losing his phone. He realizes there is one last evidence of the sex tape on his phone and anxiously looks for it. Yi-gyu, who found Seung-mins phone, calls Seung-mins wife and asks her to come pick it up. On the night the phone was supposed to be returned, Yi-gyu doesnt show up. Now Yi-gyu is the one holding the leverage. Seung-min tries to do everything possible to get back his phone but Yi-gyus demands are escalating to the point of no return.  

==Cast==
*Uhm Tae-woong as Oh Seung-min 
*Park Yong-woo as Jung Yi-gyu
*Park Sol-mi as Kim Jeong-yeon
*Lee Se-na as Yoon Jin-ah
*Kim Nam-gil as Jang Yoon-ho (Jin-ahs ex-boyfriend)
*Hwang Bo-yeon as Kim Dae-jin (Seung-mins assistant) 
*Park Gil-su as Choi (loan shark) 
*Kim Gu-taek as Chois bodyguard 
*Kim Yu-seok as Han Joon-soo (attorney) 
*Ju Jin-mo as Captain Kim 
*Seo Woo as Yi-gyus sister 
*Kim Jong-seok as wedding toastmaster 
*Kim Gu-ra as radio DJ who interviews Jin-ah
*Jeon Bae-su as sound engineer at radio station 
*Bong Man-dae as movie director 
*Choi Ju-bong as man who lost dog in the store 
*Lee A-rin as mart female employee 2 
*Jun In-kul as employee of management company Kwak Byung-kyu as lead detective

==Unofficial remakes==
Three South Indian films have been made whose plots have close resemblance to Handphone, but they are not officially credited as remakes. The films are Chaappa Kurish (lit. "Heads or Tails"; Malayalam; 2011), Puli Vaal ("Tail of Tiger"; Tamil language|Tamil; 2014) and Only Vishnuvardhana (Kannada language|Kannada, 2012).  

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 