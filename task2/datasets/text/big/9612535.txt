The Lightning Warrior
{{Infobox film
| name           = The Lightning Warrior
| image          = File:The Lightning Warrior poster.jpg
| caption        = Poster of chapter 12
| director       = Benjamin H. Kline Armand Schaefer
| producer       = Nat Levine
| writer         = Ford Beebe Wyndham Gittens Colbert Clark
| narrator       = Pat OMalley Georgia Hale Theodore Lorch
| music          = Lee Zahler Tom Galligan Ernest Miller William Nobles
| editing        = Wyndham Gittens Ray Snyder
| distributor    = Mascot Pictures
| released       =  
| runtime        = 12 chapters (250 min)
| country        = United States English
| budget         =
}} Mascot Serial movie serial starring Rin Tin Tin in his last role. It is regarded as one of the better Mascot serials. A number of the productions outdoor action sequences were filmed on the rocky Iverson Movie Ranch in Chatsworth, Los Angeles, California|Chatsworth, Calif., known for its huge sandstone boulders and widely recognized as the most heavily filmed outdoor shooting location in the history of the movies.

==Plot== mysterious masked figure, is leading an Indian uprising to drive local settlers off their land.  The Wolfman kills Jimmy Carters father and Alan Scotts brother, which leads the two heroes and Alans dog Rinty, to hunt down and defeat the villain.

==Cast==
*Rin Tin Tin as "Rinty", known as The Lightning Warrior to the local Indians
*Frankie Darro as Jimmy Carter, set to defeat The Wolfman after the villain kills Jimmys father
*Hayden Stevenson as Carter
*George Brent as Alan Scott, government agent investigating The Wolfman and his brothers death Pat OMalley as Sheriff A. W. Brown
*Georgia Hale as Dianne
*Theodore Lorch as Pierre La Farge
*Lafe McKee as John Hayden
*Frank Brownlee as Angus McDonald
*Bob Kortman as Wells, one of the Wolfmans henchmen
*Dick Dickinson as Adams, one of the Wolfmans henchmen
*Yakima Canutt as Ken Davis and Deputy
*Frank Lanning as Indian George/Jim
*Bertee Beaumont as Pioneer Woman 
*Helen Gibson as Pioneer Woman

==Chapter titles==
# Drums of Doom
# The Wolf Man
# Empty Saddles
# Flaming Arrows
# The Invisible Enemy
# The Fatal Name
# The Ordeal of Fire
# The Man Who Knew
# Traitors Hour
# Secret of the Cave
# Red Shadows
# Painted Faces
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = pp. 205
 | chapter = Filmography
 }} 

==References==
 

==External links==
* 
* 
* 
*  

 
{{succession box  Mascot Serial Serial  The Galloping Ghost (1931 in film|1931)
| years=The Lightning Warrior(1931 in film|1931)
| after=The Shadow of the Eagle (1932 in film|1931)}}
 

 

 
 
 
 
 
 
 
 
 