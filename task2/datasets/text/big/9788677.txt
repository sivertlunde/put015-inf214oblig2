Prisioneros de la tierra
{{Infobox film
| name           = Prisioneros de la tierra
| image          = Prisionerosterra39.jpg
| image_size     =
| caption        = Poster
| director       = Mario Soffici
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Argentina Sono Film S.A.C.I 1939
| runtime        =85 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded_by    =
| followed_by    =
}} Argentine drama film directed by Mario Soffici. The film premiered in Buenos Aires. The film is often cited as one of the greatest in the history of Argentine cinema, and established Soffici as a "social" filmmaker.  It was awarded by the Municipality of Buenos Aires as the best film of the year.

==Cast==
*Homero Cárpena		
*Raúl De Lange		
*Roberto Fugazot		
*Elisa Galvé		
*Ángel Magaña		
*Pepito Petray		
*Francisco Petrone	
*Félix Tortorelli	
*Manuel Villoldo

==References==
 
 
==Further reading==
*   
==External links==
*  
 

 
 
 
 
 
 
 
 
 