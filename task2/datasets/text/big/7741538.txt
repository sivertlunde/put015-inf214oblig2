The Inspector General (film)
{{Infobox film
| name           = The Inspector General
| image          = Inspector General.jpg
| image_size     =
| caption        = DVD cover
| director       = Henry Koster
| producer       = Jerry Wald, Sylvia Fine
| based on       =  
| writer         = Harry Kurnitz Philip Papp Ben Hecht
| narrator       =
| starring       = Danny Kaye Walter Slezak Elsa Lanchester Alan Hale Sr. Barbara Bates Gene Lockhart
| music          = Johnny Green Sylvia Fine
| cinematography = Elwood Bredell
| editing        = Rudi Fehr
| distributor    = Warner Bros.
| released       =  
| runtime        = 100 minutes
| country        = United States English
| budget         =
| gross          = $2.2 million (US rentals) 
}}
 musical comedy Rhys Williams. Original music by Sylvia Fine and Johnny Green.

==Premise== The Inspector General. The plot is re-located from the Russian Empire into an unspecified corrupted region of a country that suddenly finds itself under the supervision of the First French Empire.

==Plot== Gypsies led by Yakov (Walter Slezak) escapes from a travelling medicine show after he innocently lets slip that the elixir theyre selling is a fraud. Tired and hungry, he wanders into the small town of Brodny and whilst trying to sample the contents of a horses feedbag, hes arrested as a vagrant and sentenced to hang the next day by a corrupt police chief (Alan Hale Sr.), desperate to prove his efficiency.   

The town is run by a corrupt Mayor (Gene Lockhart), whose employees and councillors are all his cousins and equally corrupt and incompetent, but they are frightened when they learn that the Inspector General is in their neighborhood, and probably in disguise. The band of officials and the mayor want to protect their town and their lives, so, acting foolishly they seal off every road to keep the inspector from entering their town. They mistake Georgi for the Inspector and ply him with food and drink whilst plotting to have him killed. Yakov wanders into the small town and convinces Georgi to stay on as an inspector general and accept the bribes the officials so willingly throw at him. Of course, Yakov wants to seize Georgis misfortune and turn it into a new start for his own life.

Meanwhile, hearing tales of his legacy and courageous efforts the mayors wife instantly takes a liking to Georgi, hoping he will fall in love with her and whisk her away from the mayor and his lack of attention to her. However Georgi has fallen in love with a servant and wishes to marry her.

Naturally, their plans go awry and Georgi, despite his innocence, discovers how corrupt they really are. And when the real Inspector arrives suddenly, he also realizes that Georgi is the most honest fellow hes met since leaving Budapest. The Inspector General names Georgi the new Mayor of Brodny and presents him the mayoral gold chain, having taken it from the old mayor saying, "Well put something else around your neck." Yakov becomes the new chief of police and Georgi gets the girl of his dreams.

==Cast==
* Danny Kaye as Georgi
* Walter Slezak as Yakov
* Barbara Bates as Leza
* Elsa Lanchester as Maria
* Gene Lockhart as The Mayor Alan Hale as Kovatch
* Walter Catlett as Colonel Castine Rhys Williams as Inspector General

==Soundtrack==
  Golden Globe Best Motion Picture Score for his work on the film. Kayes wife Sylvia Fine wrote the original songs "The Inspector General" and "Happy Times", both sung by Kaye in the film.  Happy Times was, in fact, the working title of the film.

==DVD release==
 
The Inspector General is one of a number of major Hollywood productions from the 1940s and 1950s that have lapsed into the public domain in the United States.    The last copyright holder was United Artists Television (later MGM and Turner Entertainment).

==See also==
* List of films in the public domain

==References==
 

==External links==
 
*  
*  
*  
*  

*  

 
 

 
 
 
 
 
 
 
 