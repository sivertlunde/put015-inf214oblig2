The Big Punch
 
{{Infobox film
| name           = The Big Punch
| image          = The Big Punch.jpg
| caption        = Theatrical poster for The Big Punch (1921)
| director       = John Ford
| producer       = 
| writer         = John Ford Jules Furthman Barbara Bedford
| music          = 
| cinematography = Frank B. Good
| editing        =  Fox Film Corporation
| released       =  
| runtime        = 50 minutes
| country        = United States Silent English English intertitles
| budget         = 
}}
 western film directed by John Ford.    Its survival status is classified as unknown,  which suggests that it is a lost film. In France, the film was known under the title Un homme libre (meaning a free man).

==Plot==
As summarized in a film publication,    Buck (Jones) consents to study for the ministry, and before leaving attempts to convince his worthless brother Jed (Curtis) to sober up and stay home with their mother (Lee) during Bucks absence. On the eve of his leaving Buck is implicated in a murder committed by Jed and his gang. Buck serves two years and upon his release completes his study for the ministry before returning home. People ridicule him and laugh at the "jailbird minister," as they call him. During one of his services, his brother and two pals enter the church to hide from the prison officials who are after them. Buck shields them, and they later come to his aid when Flash McGraw (Siegmann), the owner of a dance hall, has lured a Salvation Army girl (Bedford) to his room, and Buck has to fight the whole gang. A girl who believes McGraw is "throwing her over" reveals that McGraw "framed" the murder charge on Jed and his pals. This gives the men their freedom and clears Buck, leaving him free to marry the Salvation Army girl.

==Cast==
* Buck Jones as Buck Barbara Bedford as Hope Standish Jack Curtis as Jed
* George Siegmann as Flash McGraw Jack McDonald as Friend of Jeds
* Al Fremont as Friend of Jeds Jennie Lee as Bucks Mother
* Edgar Jones as The Sheriff Irene Hunt as Dance Hall Girl
* Eleanore Gilmore

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 