Tess of the d'Urbervilles (1913 film)
{{infobox_film
| title          = Tess of the dUrbervilles
| image          =
| imagesize      =
| caption        =
| director       = J. Searle Dawley
| producer       = Famous Players Film Company Daniel Frohman
| writer         = Thomas Hardy (novel)
| starring       = Mrs. Fiske
| music          =
| cinematography =
| editing        =
| studio         = Famous Players Film Company
| distributor    = States Rights
| released       = September 1, 1913
| runtime        = 5 reels
| country        = United States
| language       = Silent (English intertitles)
}} novel of the same name and was one of the first feature films made. It was directed by J. Searle Dawley and stars Mrs. Fiske, reprising her famous role from the 1897 play.  An Adolph Zukor feature production after securing the services of top American actress Mrs. Fiske. 

A fragment of this film is said to exist.   


==Cast==
*Mrs. Fiske - Tess Durbeyfield
*Raymond Bond - Angel Clare
*David Torrence - Alec DUrberville
*John Steppling - John Durbyfield
*Mary Barker - Mrs. Durbeyfield James Gordon - Crick
*Maggie Weston - Mrs. Crick
*Irma La Pierre - Marian
*Katherine Griffith - Mrs. DUrberville
*Franklin Hall - Parson Clare
*Camille Dalberg - Mrs. Clare
*J. Liston - Parson Tringham
*Boots Wall - Reta
*Caroline Darling - Izz
*Justina Huff - Liza Lou
*John Troughton - Jonathan

==Reception== city and state film censorship boards. In 1917 the Chicago Board of Censors issued the film, due to its subject matter, an "adults only" permit. 

==See also== Tess of the dUrbervilles (1924) Tess of the DUrbervilles (2008 TV)

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 