Catfish (film)
{{Infobox film
| name = Catfish
| image = Catfish film.jpg
| image_size = 
| alt = 
| caption = Theatrical release poster
| director = Henry Joost Ariel Schulman
| producer = Andrew Jarecki Marc Smerling Henry Joost Ariel Schulman
| starring = Nev Schulman Henry Joost Ariel Schulman  Stephen Fogarty
| cinematography = Henry Joost Ariel Schulman Nev Schulman
| editing = Zachary Stuart-Pontier Rogue Pictures
| distributor = Universal Pictures
| released =  
| runtime = 87 minutes  
| country = United States
| language = English
| budget = 
| gross = $3,479,614 
}}
Catfish is a 2010 American  .

==Plot==
Young photographer  .

For a documentary, Ariel and Henry Joost film Nev as he begins an online relationship with Megan. She sends him MP3s of song covers she performs for him, but Nev discovers that they are all taken from performances on YouTube. He later finds evidence that Angela and Abby have lied about other details of Abbys art career. Ariel urges Nev to continue the relationship for the documentary, although Nev seems reluctant to continue. The siblings decide to travel to Michigan in order to make an impromptu appearance at the Pierces house and confront Megan directly. As they arrive at the house, Angela takes some time to answer the door, but is welcoming and seems happy to finally meet Nev in person. She also tells him that she has recently begun chemotherapy for uterine cancer. After leaving multiple messages while trying to call Megan, she drives Nev and Ariel to see Abby herself. While talking with Abby and her friend alone, Nev learns that Abby never sees her sister and rarely paints.

The next morning, Nev wakes up to a text message from Megan saying that she has had a long-standing alcohol problem, and has decided to check into rehab and cannot meet him, which is confirmed by one of Megans Facebook friends, but Nev realizes that this is likely another lie from Angela. After meeting with the family back at their house, Angela admits that the pictures of Megan were of a family friend, that her daughter Megan really is in rehab downstate and that Angela had really painted each of the paintings that she had sent to Nev. Nev thus realizes that while believing he was talking to Megan, it was really Angela posing as her with an alternate Facebook account and mobile phone. As he sits for a drawing, Angela confesses that the various Facebook profiles were all maintained by her, but that through her friendship with Nev she had reconnected with the world of painting, which had been her passion before she sacrificed her career to marry Vince—who has two severely mentally disabled children who require constant care. Through a conversation with Vince himself, the siblings learn that Angela had told him (falsely) that Nev was paying for her paintings, and that he had encouraged her to seize the opportunity to have him as a patron.

Vince, talking with Nev, tells a story. He says that when live cod were shipped to Asia from North America, the fishs inactivity in their tanks resulted in only mushy flesh reaching the destination, but fishermen found that putting catfish in the tanks with the cod kept them active, and thus ensured the quality of the fish. Vince talks of how there are people in everyones lives who keep us active, always on our toes and always thinking. It is implied that he believes Angela to be such a person.

Some time after, Nev receives a package labeled as being from Angela herself; it is the completed drawing that she labored over during their meeting, although Nev seems ambivalent in his feelings about it.

On-screen text then informs the viewer that Angela did not have cancer, there was no Megan at Dawn Farms, and she doesnt know the girl in the pictures. Over the course of their nine-month correspondence, Angela and Nev exchanged more than 1,500 messages. It was revealed later on that the girl in the pictures was Aimee Gonzales, a professional model and photographer, who lives in Vancouver, Washington with her husband and two children. In October 2008, two years subsequent to the events, Ronald, one of Vinces twin sons, has passed away. Angela deactivated her 15 other profiles and changed her Facebook profile to a picture of herself, and now has a website to promote herself as an artist. Nev is still on Facebook and has more than 732 friends, including Angela.

==Production==
To portray Megan and her family, Angela used pictures that Gonzales had posted on Facebook. The documentarys filmmakers compensated Gonzales for her involuntary appearance in "Catfish," and she participated in publicity for the film.    A photograph Angela described as a son, Alex, is that of rapper Joshua Paul Liimatta, also known as "The Sisu Kid".   

==Authenticity==
In an interview, Schulman related that some viewers believe Catfish to be a fake documentary or a hoax.

Kyle Buchanan at Movieline questions why the filmmakers would begin obsessively documenting Nevs online relationship so early on, and argues that it is highly improbable that media-savvy professionals like the Schulmans and Joost would not use the Internet to research Megan and her family before meeting them.  Others have also questioned the trios decision to begin filming, as well as the seemingly improbable coincidence of them catching everything of importance to the story on film as it happens. It has also been pointed out that the groups supposed movements in Catfish are not documented in their public blog postings at the time. 

Since the movies release, Angela has been interviewed by ABCs 20/20 (US television series)|20/20  and the Los Angeles Times has spoken with neighbors familiar with her family.  In the summer of 2011, The Mining Journal ran a two-part profile  of Angela in connection with the North of the 45th Parallel 2011 exhibition at the DeVos Art Museum on the campus of Northern Michigan University. 

==Release== Rogue Pictures unit of Relativity Media acquired Catfish in a bidding war with Paramount Pictures, after Brett Ratner endorsed the film.    Catfish was released on Blu-ray and DVD on January 4, 2011. 

==MTV series==
 
The Schulmans teamed up with MTV to produce a reality television series similar to the idea of the documentary but which focuses on the lives of others who have been entangled in an online relationship with another person.  It premiered on November 12, 2012. 

==Reception==
The film was well received by critics; it holds an 81% rating on Rotten Tomatoes, the sites consensus being "Catfish may tread the line between real-life drama and crass exploitation a little too unsteadily for some viewers tastes, but its timely premise and tightly wound mystery make for a gripping documentary".   

Time (magazine)|Time magazine did a full-page article, written by Mary Pols in a September 2010 issue, saying "as you watch Catfish, squirming in anticipation of the trouble that must lie ahead―why else would this be a movie?―youre likely to think this is the real face of social networking." 

At the   called Catfish "jaw-dropping" and "crowd-pleasing" but said that it "will require clever marketing in order to preserve the surprises at its core." DeFore, John. " " The Hollywood Reporter, 28 January 2010.  Kyle Buchanan of   of the Chicago Sun-Times referred to these questions as a "severe cross-examination" and states that "everyone in the film is exactly as the film portrays them." Ebert, Roger." " Chicago Sun-Times, 22 September 2010. 

Total Film described the film as: "Funny, unsettling and thoroughly engrossing... the end result is a compulsive, propulsive study of relationships virtual and real". 

Very Aware said of the film: "All of the above information doesnt prove that the film is entirely fake. What it does prove is that much of the film has been recreated, and its possible that they did it in such a way to create a story where there might not have been one to begin with". 
 the fourth installment.

==Lawsuits==
The film itself has been the subject of two lawsuits. Relativity Media has concluded that due to these lawsuits, the film will never be profitable.  Both of these lawsuits have to do with songs used within the film not being attributed to their creators.

==References==
 

==External links==
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 