Impy's Island
{{Infobox film
| name = Impys Island
| image =
| caption =
| director = Reinhard Klooss   Holger Tappe
| producer = Ambient Entertainment GmbH Max Kruse   Sven Severin
| starring = Wigald Boning   Anke Engelke   Florian Halm   Christoph Maria Herbst   Kevin Iannotta   Ulrike Johanssen   Stefan Krause   Oliver Pocher   Domenic Redl   Frank Schaff   Klaus Sonnenschein   Wolfgang Völz James Dooley
| cinematography =
| editing =
| distributor =
| released = 2006
| runtime = 87 minutes
| country = Germany
| language = German
| budget =
}}
 Max Kruse.

==Plot summary==
On a magical tropical island called Tikiwoo, a fun-loving group of misfit animals and people make a marvelous discovery: a baby dinosaur frozen since prehistoric times. Little Impy, as they call him, is loving his new family and ready to explore the strange new world. but when a king from a faraway country vows to capture the lovable baby dino for his private collection, all the inhabitants of Impys island must join together to save their new friend.

==Cast==
The film stars the voice talents of:
* Wigald Boning as Professor Habakuk Tibatong
* Anke Engelke as Wutz
* Florian Halm as Diener Sami
* Christoph Maria Herbst as Doctor Zwengelmann
* Kevin Iannotta as Tim Tintenklecks Pinguin Ping Schuhschnabel Schusch
* Domenic Redl as Urmel Waran Wawa
* Klaus Sonnenschein as King Pumponell
* Wolfgang Völz as Elephant seal|See-Elefant Seele-Fant

For the American release, the voice talents are the following:
* Lisa Ortiz as Impy the Impodochosaurus Zoe Martin as Ping the king penguin
* Maddie Blaustein as Shoe the shoebill
* Sean Schemmel as King Pumponell the 55th Charlotte Mahoney as Peg the house pig
* Pete Bowlan as Sami
* Michael Sinterniklaas as Professor Horatio Tibberton
* Jimmy Zoppi as Monty the monitor lizard
* Alan Smithee as Dr. Zonderburgh
* P.J. Battisti|P.J. Battisti, Jr. as Tim Michael Alston Baley as Solomon the elephant seal

==Sequel==
 

*Elisabeth Williams as Impy the Impodochosaurus
*Veronica Taylor as Babu the panda Billy Beach as Barnaby, Monty the monitor lizard, Small Sheik
*Lisa Ortiz as Ping the king penguin
*Madeleine Blaustein as Shoe the shoebill
*Kayzie Rogers as Peg the house pig Michael Alston Baley as Solomon the elephant seal
*Wayne Grayson as Professor Horatio Tibberton Pete Zarustica as Eddie
*Tricia Diaz as Miss Lee
*Roland Hemmo as Otto the bulldog Marc Thompson as the whale

==See also==
*List of animated feature films
*List of computer-animated films

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 

 