The Bourne Legacy (film)
 
 
{{Infobox film
| name = The Bourne Legacy
| image = The Bourne Legacy Poster.jpg
| caption = Theatrical release poster
| director = Tony Gilroy
| producer =  
| screenplay =  
| story =  
| based on =  
| starring = {{Plain list|
* Jeremy Renner
* Rachel Weisz
* Edward Norton
* Stacy Keach
* Dennis Boutsikaris
* Oscar Isaac
* Joan Allen
* Albert Finney
* David Strathairn
* Scott Glenn
}}
| music = James Newton Howard
| cinematography = Robert Elswit John Gilroy
| studio =   Universal Pictures
| released =  
| runtime = 135 minutes 
| country = United States  
| language = English
| budget = $125 million   
| gross = $276.1 million 
}} action thriller series of The Bourne Legacy, the actual screenplay bears little resemblance to the novel. Unlike the novel, which features Jason Bourne as the principal character, the film centers on agent Aaron Cross (played by Jeremy Renner), an original character. In addition to Renner, the film stars Rachel Weisz and Edward Norton.
 The Bourne Ultimatum (2007).

In The Bourne Legacy, Aaron Cross is a member of a black ops program called Operation Outcome whose subjects are genetically enhanced. He must run for his life once former CIA Treadstone agent Jason Bournes actions lead to the public exposure of Operation Treadstone and its successor Operation Blackbriar.

Filming was primarily in New York, with some scenes shot in the Philippines, South Korea, Pakistan and Canada. It was theatrically released on  , 2012, in the United States. It received mixed reviews, with critics praising the story and Renners performance but critics seemed disappointed with Matt Damons absence and the shaky camera work that was also in the second and third films.

== Plot == CIA lost Department of black operation program using experimental pills known as "chems" to enhance the physical and mental abilities of their users. Aaron is assigned to Alaska for a training exercise, where he must survive weather extremes and traverse rugged terrain in order to arrive at a remote cabin. The cabin is operated by an exiled Outcome operative, Number Three, who informs Aaron that he has broken the mission record by two days.   
 FBI and Senate Select CIA Director Deputy Director Pamela Landy, Blackbriar supervisor Noah Vosen and Treadstone medical director Dr. Albert Hirsch.
 Air Force supersoldier program LARX.

 , an unmanned aerial vehicle (UAV) like the one featured in Legacy]]
 drone to biochemist Dr. Marta Shearing as the sole survivor.  Meanwhile, other Outcome agents are eliminated when their handlers give them poisoned chems.
 genetically modified Private First roadside bomb in the Iraq War) and that his recruiter added 12 points to his IQ, enabling Aaron to meet the United States Armys requirements. Without his enhanced intelligence, Aaron believes they stand no chance of survival. Aaron and Marta travel to Manila, where the chems are manufactured, to try to infect him with another virus so he will not need the blue chems. 

Aaron and Marta bluff their way into the chem factory. Marta injects Aaron with the live virus stems. Byer alerts the factory security, but Aaron and Marta evade capture. Byer orders LARX-03, a chemically-brainwashed supersoldier, to track down and kill them. Aaron recovers from the flu-like symptoms but hallucinates about his Outcome training.  Police surround their shelter while Marta is buying medicine; she warns Aaron by screaming. Aaron rescues her and steals a motorcycle. They are pursued by both the police and LARX-03. After a lengthy chase through the streets and marketplaces of Manila to Marikina, they lose the police, but not the assassin. Both Aaron and LARX-03 are wounded by bullets. LARX-03 is killed when Marta causes his motorcycle to crash into a pillar. Marta persuades a Filipino boatman to help them escape by sea. They sail away, while back in New York, Vosen lies to the Senate that Landy committed treason by trying to sell Treadstone secrets to the press and by assisting Jason, the only reason why Blackbriar existed. 

== Cast ==
 
* Jeremy Renner as Aaron Cross/PFC. Kenneth James Kitsom
* Rachel Weisz as Dr. Marta Shearing/June Munroe
* Edward Norton as Colonel Eric Byer
* Stacy Keach as Admiral Mark Turso
* Dennis Boutsikaris as Terrence Ward
* Oscar Isaac as Outcome 3
* Joan Allen as CIA Deputy Director Pamela Landy
* Albert Finney as Dr. Albert Hirsch
* Neil Brooks Cunningham as Dr. Dan Hillcott
* David Strathairn as Noah Vosen 
* Scott Glenn as CIA Director Ezra Kramer
* Donna Murphy as Dita Mandy
* Željko Ivanek as Dr. Donald Foite
* Corey Stoll as Vendel
* Shane Jacobson as Mackie
* Elizabeth Marvel as Dr. Connie Dowd
* Louis Ozawa Changchien as LARX-03 Corey Johnson as Ray Wills
* John Arcilla as Joseph
* Lou Veloso as The Captain
* Paddy Considine as Simon Ross (archive footage)
* Robert Christopher Riley as Outcome 6
 

== Production ==
Universal Pictures originally intended The Bourne Ultimatum (film)|The Bourne Ultimatum to be the final film in the series, but development of another film was under way by October 2008.    George Nolfi, who co-wrote The Bourne Ultimatum, was to write the script of a fourth film, not to be based on any of the novels by Robert Ludlum.  Joshua Zetumer had been hired to write a parallel script&mdash;a draft which could be combined with another (Nolfis, in this instance)&mdash;by August 2009 since Nolfi would be directing The Adjustment Bureau that September.   Matt Damon stated in November 2009 that no script had been approved and that he hoped that a film would begin shooting in mid-2011.    The next month, he said that he would not do another Bourne film without Paul Greengrass, who announced in late November that he had decided not to return as director.  In January 2010, Damon said that there would "probably be a prequel of some kind with another actor and another director before we do another one just because I think were probably another five years away from doing it." 

However, it was reported in June 2010 that Tony Gilroy, who co-wrote each of the three previous Bourne films, would be writing a script with his brother, screenwriter Dan Gilroy, for a fourth Bourne film to be released sometime in 2012.   That October, Universal set the release date for The Bourne Legacy for August 10, 2012,   Tony Gilroy was confirmed as the director of the film, and it was also announced that the Jason Bourne character would not be in The Bourne Legacy. 
 James Bond. You cant do a prequel. You cant do any of those kinds of things, because there was never any cynicism attached to the franchise, and that was the one thing they had to hang on to."   

Gilroy "never had any intention of ever coming back to this realm at all—much less write it, much less direct it. Then I started a really casual conversation about what we could do in a post-Jason Bourne setting. I was only supposed to come in for two weeks, but the character we came up with, Aaron Cross, was so compelling."     After watching The Bourne Ultimatum again, Gilroy called his brother, screenwriter Dan Gilroy, and said,  The only thing you could do is sort of pull back the curtain and say theres a much bigger conspiracy. So we had to deal with what happened in Ultimatum as the starting point of this film. Ultimatum plays in the shadows of Legacy for the first 15 minutes—they overlap." 

In speaking about the films storyline, Gilroy drew a distinction between the fictional programs in the Bourne film series:
   }}

Although a large part of the film was set in and around Washington, D.C., the real D.C. appears only in aerial   used as Shearings house was unable to accommodate the weight of equipment and crew, so it was used only for exterior shots, and all interior scenes were filmed on a Kaufman Astoria soundstage.   The scenes set in the "SteriPacific" factory in Manila were actually filmed in the New York Times printing plant in Queens.   

  in the Philippines served as a filming location, and in the film, was used as escape route from Manila.]] Manila  and in the paradise bay of El Nido, Palawan in the Philippines.  Several train scenes at Garak-Market Station on Seoul Subway Line 3 and nearby areas in Seocho-daero 77-Gil (1308 Seocho 4-dong), Seocho-gu and Gangnam-gu, Seoul, South Korea were used in some scenes.  The Kananaskis Country region of Alberta, Canada was used for the scenes set in Alaska. 

Gilroy said that "there are three deleted scenes—we just mixed them and color corrected them   but what I like about it is all three scenes happen in the movie. One of thems referred to and theyre completely legitimate parts of our story, they absolutely happen in our film, we just didnt have time to show them to you so theres nothing off to the side. I think theyll be on the straight-up DVD." 

The film portrays Cross and Shearing as traveling nonstop from New York JFK Airport to Manila on board an American Airlines Boeing 747-400. That particular 747 model was introduced in 1989; American Airlines has never flown one;   American Airlines has never served Manila as a destination; no commercial airline has ever flown from JFK to Manila nonstop with passenger service; the distance between the two cities exceeds the maximum range of any model 747.  In spite of these continuity lapses, American Airlines was actively involved in the production of the film in cooperation with NBCUniversal, and contributed its own airline employees and a Boeing 777-200 for the interior terminal and cabin shots at Terminal 8 of JFK International Airport.  The airline also heavily co-marketed the film throughout post-production.

== Release ==

=== Critical response ===
The Bourne Legacy received mixed reviews from critics. On Rotten Tomatoes, the film holds a rating of 55%, based on 215 reviews, with an average rating of 5.8/10. The sites consensus reads, "It isnt quite as compelling as the earlier trilogy, but The Bourne Legacy proves the franchise has stories left to tell—and benefits from Jeremy Renners magnetic work in the starring role."  On Metacritic, the film received a score of 61 out of 100, based on 42 critics, which indicates "generally favorable reviews".  

Lisa Schwarzbaum of Entertainment Weekly gave the film an A-, commenting that "Gilroy, who as a screenwriter has shaped the movie saga from the beginning, trades the wired rhythms established in the past two episodes by Paul Greengrass for something more realistic and closer to the ground. The change is refreshing. Jason Bournes legacy is in good hands." 

Peter Debruge of  ized, with an abrupt change of frontman and a resulting dip in personality." 

  of The Hollywood Reporter commented on his review that "the series legacy is lessened by this capable but uninspired fourth episode." 

=== Box office performance === The Bourne Identity. Studio research reported that audiences were evenly mixed among the sexes.  The film grossed $113,203,870 in North America and $162,940,880 in foreign countries, bringing the films worldwide total to $276,144,750. 
 Pasay City, Metro Manila, Philippines on  , five days ahead of its opening date in North American theaters. 

== Soundtrack ==
{{Infobox album  
| Name        = The Bourne Legacy: Original Motion Picture Soundtrack
| Type        = film
| Artist      = James Newton Howard
| Cover       =
| Released    =  , 2012
| Recorded    = 2012
| Genre       =
| Length      = 63:34
| Label       = Varèse Sarabande
| Producer    =
| Last album  = Snow White and the Huntsman
| This album  = The Bourne Legacy
| Next album  = After Earth
}}
{{Album ratings
| rev1 =  
| rev1score =  
| rev2 =  
| rev2score =  
| rev3 =  
| rev3score =  
| rev4 =  
| rev4score =  
| rev5 =  
| rev5score =  
| rev6 =  
| rev6score =  
}}
The soundtrack to The Bourne Legacy as composed by James Newton Howard was released digitally on   by Varèse Sarabande Records.  

== Home media ==
The Bourne Legacy was released on DVD and Blu-ray on December 11, 2012 in the United States and Canada. 

==Sequel== Perfect Storm Entertainment and the studio announced an August 14, 2015 release date.  On May 9, 2014, Andrew Baldwin was brought in to re-write the film.  On June 18, 2014, the studio pushed back the film from August 14, 2015, to July 15, 2016.  On January 6, 2015, the studio pushed back the release date to July 29, 2016.  In November 2014, Damon confirmed that he and Greengrass will return. 

==See also==
 
*List of films featuring surveillance
*List of films featuring drones

== References ==
 

== External links ==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 