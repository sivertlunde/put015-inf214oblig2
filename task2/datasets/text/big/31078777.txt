A South Sea Bubble
 
 
{{Infobox film
| name           = A South Sea Bubble
| image          =
| caption        =
| director       = T. Hayes Hunter 
| producer       = Michael Balcon   S.C. Balcon
| writer         = Angus MacPhail   Alma Reville   Roland Pertwee
| starring       = Ivor Novello  Benita Hume   Alma Taylor   Annette Benson
| music          =  James Wilson
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       = July 1928
| runtime        = 100 minutes
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent comedy comedy adventure film directed by T. Hayes Hunter and starring Ivor Novello, Benita Hume and Alma Taylor.  A group of adventurers head to the Pacific Ocean to hunt for buried treasure. It was made at Islington Studios. 

==Cast==
* Ivor Novello as Vernon Winslow
* Benita Hume as Averil Rochester
* Alma Taylor as Mary Ottery
* Annette Benson as Lydia la Rue
* Sydney Seaward as William Carpenter
* S.J. Warmington as Frank Sullivan
* Ben Field as Isinglass
* Harold Huth as Pirate
* John F. Hamilton as Tony Gates
* Mary Dibley as Olive Barbary

==References==
 

==Bibliography==
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 