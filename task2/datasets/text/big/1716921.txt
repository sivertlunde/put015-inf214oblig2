Amazon Women on the Moon
{{Infobox film
| name           = Amazon Women on the Moon
| image          = amazon_women_on_the_moon.jpg
| caption        = Theatrical release poster
| director       = Joe Dante Carl Gottlieb Peter Horton John Landis Robert K. Weiss
| producer       = John Landis Robert K. Weiss
| writer         = Michael Barrie Jim Muholland
| music          = Ira Newborn
| cinematography = Daniel Pearl Malcolm Campbell Universal Pictures
| released       =  
| runtime        = 85 min.
| country        = United States English
| budget         = $5 million
| gross          = $548,696
}}

Amazon Women on the Moon is a 1987  , Carl Gottlieb, Peter Horton, John Landis and Robert K. Weiss.
 spoof of science fiction movies from the 1950s that borrows heavily from Queen of Outer Space (1958) starring Zsa Zsa Gabor, itself a movie that recycles elements of earlier science fiction works such as Cat-Women of the Moon (1953), Fire Maidens from Outer Space (1955) and Forbidden Planet (1956). 
 Steve Forrest, William Marshall, Joe Pantoliano, Robert Picardo and Roxie Roker.

Other notable people in the cast included voice actors Corey Burton and Phil Hartman, talk show host Arsenio Hall, adult film actress Monique Gabrielle, science fiction writer Forrest J. Ackerman, B movie stars Lana Clarkson and Sybil Danning, musician B. B. King, radio personalities Roger Barkley and Al Lohman, composer Ira Newborn, director Russ Meyer, model Corinne Wahl, comedian Andrew Dice Clay, Firesign Theater member Phil Proctor and independent film actor Paul Bartel.

John Landis had previously directed The Kentucky Fried Movie (1977), which employed a similar sketch anthology format.

==Plot== Steve Forrest) battle exploding volcanoes and man-eating spiders on the moon. Waiting for the film to resume, an unseen viewer begins channel surfing — simulated by bursts of white noise — through late night cable, with the various segments and sketches of the film representing the programming found on different channels. The viewer intermittently returns to channel 8, where Amazon Women continues to resume airing before faltering once more.

These segments feature: 
* Arsenio Hall as a man who nearly kills himself in a series of mishaps around his apartment;
* Monique Gabrielle as a model who goes about her daily routine in Malibu, California, completely naked;
* Lou Jacobi as a man named Murray, zapped into the television, wandering throughout sketches looking for his wife;
* Michelle Pfeiffer and Peter Horton as a young couple having trouble with eccentric doctor Griffin Dunne delivering and then concealing their newborn baby;
* Joe Pantoliano as the presenter of a commercial recommending stapling carpet to a bald head as a hair loss prevention measure;
* David Alan Grier and B. B. King in a public-service appeal for "blacks without soul";
* Rosanna Arquette as a young woman on a blind date, employing unusual methods of investigation to reveal the qualifications of Steve Guttenberg; In Search of...; Archie Hahn roasted at his funeral by a variety of people, including Steve Allen, Henny Youngman and even his own wife; William Marshall as the leader of the Video Pirates, who hijack an MCA Home Video ship, uncover a vast amount of videotapes and laserdiscs, and promptly begin illegally bootlegging the media;
* Ed Begley, Jr. as the son of the Invisible Man, having trouble with his formula; First Lady who is also a former prostitute|hooker;
* Matt Adler as a sexually frustrated teenager trying to purchase a pack of condoms, with unexpected results;
* Marc McClure renting a personalized date video that spills over into real life; social diseases" in the style of Reefer Madness.

===Alternative versions===
An alternate version of the "Pethouse Video" sketch was filmed for the television broadcast of the film, with Monique Gabrielle in lingerie instead of appearing naked throughout the segment. However, most European television broadcasts of the film retained the original theatrical version. Bullshit or Not? was retitled Baloney or Not? for the television version.

The American television edit, in addition to the alternative "Pethouse Video" sketch, features an additional bridging sequence between the death of Harvey Pitnik and his subsequent celebrity roast. In it, the mortician successfully cons Pitniks widow into having the celebrity roast as part of the funeral, and her performance gets such strong positive feedback, it becomes a continuing performance series lasting for weeks.

The DVD release features an unreleased sketch titled "The Unknown Soldier", starring Robert Loggia. Some television broadcasts of the film featured the sketches "Peter Pan Theater" and "The French Ventriloquists Dummy", which were not present in the theatrical version.

==Cast==
 
 
"Mondo Condo" (directed by John Landis):
* Arsenio Hall (credited as Arsenio) as Apartment Victim.
"Pethouse Video" (directed by Carl Gottlieb):
* Donald F. Muhich as Easterbrook.
* Monique Gabrielle as Taryn Steele.
"Murray in Videoland" (directed by Robert K. Weiss):
* Lou Jacobi as Murray.
* Erica Yohn as Selma.
* Debby Davison as Weatherperson.
* Rob Krausz as Floor Manager.
* Phil Hartman as Baseball Announcer.
* Corey Burton as Anchorman.
"Hospital" (directed by Landis):
* Michelle Pfeiffer as Brenda Landers.
* Peter Horton as Harry Landers.
* Griffin Dunne as Doctor Raymond.
* Brian Ann Zoccola as Nurse.
"Hairlooming" (directed by Joe Dante):
* Joe Pantoliano as Sy Swerdlow.
* Stanley Brock as Customer.
"Amazon Women on the Moon" (directed by Weiss):
* Corey Burton as TV Announcer. Steve Forrest as Capt. Steve Nelson.
* Robert Colbert as Blackie.
* Joey Travolta as Butch.
* Forrest J Ackerman as U.S. President.
* Sybil Danning as Queen Lara.
* Lana Clarkson as Alpha Beta.
"Blacks Without Soul" (directed by Landis):
* David Alan Grier as Don No Soul Simmons.
* B.B. King as Himself. William Bryant (credited as Bill Bryant) as Male Republican.
* Roxie Roker as Female Republican.
* Le Tari as Pimp.
* Christopher Broughton as Fan Club President.
"Two I.D.s" (directed by Peter Horton):
* Rosanna Arquette as Karen.
* Steve Guttenberg (credited as Steven Guttenberg) as Jerry.
"Bullshit or Not" (directed by Dante):
* Henry Silva as Himself.
* Sarah Lilly as Prostitute.
"Critics Corner" (directed by Dante):
* Roger Barkley (credited as Barkley) as Herbert.
* Al Lohman (credited as Lohman) as Frankel. Archie Hahn as Harvey Pitnik.
* Belinda Balaski as Bernice Pitnik.
* Justin Benham as Pitnik Boy.
* Erica Gayle as Pitnik Girl.
"Silly Pâté" (directed by Weiss):
* Corey Burton as Announcer.
* T. K. Carter as Host.
* Phil Proctor as Mike.
* Ira Newborn as Fred.
* Karen Montgomery as Karen.
 
"Roast Your Loved One" (directed by Dante): Archie Hahn as Harvey Pitnik.
* Belinda Balaski as Bernice Pitnik.
* Justin Benham as Pitnik Boy.
* Erica Gayle as Pitnik Girl.
* Bryan Cranston as Paramedic #1.
* Robert Picardo as Rick Raddnitz.
* Rip Taylor as Himself.
* Slappy White as Himself. Jackie Vernon as Himself.
* Henny Youngman as Himself.
* Charlie Callas as Himself.
* Steve Allen as Himself.
"Video Pirates" (directed by Weiss): William Marshall as Pirate Captain.
* Tino Insana as Mr. Sylvio.
* Donald Gibb as Graceless Pirate.
* Frank Collison as Grizzled Pirate.
* Bill Taylor as Gruesome Pirate.
"Son of the Invisible Man" (directed by Gottlieb):
* Ed Begley, Jr. as Griffin.
* Chuck Lafont as Trent.
* Raye Birk as Vanya.
* Pamla Vale as Woman in Pub.
* Larry Hankin as Man in Pub.
* Garry Goodrow as Checker Player.
* Roger La Page as London Bobby.
"French Ventriloquists Dummy" (directed by Dante):
* Phil Bruns as Manager.
"Art Sale" (directed by Gottlieb):
* John Ingle as Felix Van Dam.
"First Lady of the Evening" (directed by Weiss):
* Angel Tompkins as First Lady. Terry McGovern as Salesman.
* Michael Hanks as Announcer.
"Titan Man" (directed by Weiss):
* Matt Adler as George.
* Kelly Preston as Violet.
* Ralph Bellamy as Mr. Gower.
* Howard Hesseman as Rupert King.
* Steve Cropper as Customer. Christopher Wolf (credited as Chris Wolf) as Mascot Bip.
"Video Date" (directed by Landis):
* Marc McClure as Ray.
* Russ Meyer as Video Salesman.
* Corinne Wahl as Sharri.
* Andrew Dice Clay as Frankie.
* Willard E. Pugh as Speaking Cop.
"Reckless Youth" (directed by Dante):
* Carrie Fisher as Mary Brown.
* Paul Bartel as Doctor.
* Herb Vigran as Agent.
* Tracy Hutchinson as Floozie.
* Mike Mazurki as Dutch.
* Frank Beddor as Ken.
 

==Critical reception==
The majority of critical opinion agreed that the quality was inconsistent throughout the film. Variety (magazine)|Variety called it "irreverent, vulgar and silly...   some hilarious moments and some real groaners too."  Roger Ebert in the Chicago Sun-Times felt that the exercise was somewhat unnecessary: "Satirists are in trouble when their subjects are funnier than they are."   

Janet Maslin of the New York Times, in a largely positive review, described the film as "an anarchic, often hilarious adventure in dial-spinning, a collection of brief skits and wacko parodies that are sometimes quite clever, though theyre just as often happily sophomoric, too."   

Certain portions of the film were singled out for praise. "The funniest episode probably is Son of the Invisible Man, directed by Carl Gottlieb, in which Ed Begley, Jr. plays a man who thinks he is invisible but is not", wrote the Chicago Sun-Times.  "The films best sight gags come from Robert K. Weiss, who deserves kudos for the inspired idiocy of his Amazon Women segments", was the opinion of the New York Times. 

In a retrospective article for   look as trapped as she does in her skit with Thirtysomething (TV series)|thirtysomething s Peter Horton, or Joe Pantoliano and Arsenio Hall as unfunny as they are in their skits." 

Amazon Women on the Moon has a rating of 64% on Rotten Tomatoes, based on 11 reviews, indicating a mixed critical response. 

==See also==
* The Kentucky Fried Movie (1977), a similarly formatted anthology comedy from John Landis
* Disco Beaver from Outer Space (1978)
* UHF (film)|UHF (1989)

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 