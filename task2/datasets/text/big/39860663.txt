Let Freedom Ring (1939 film)
{{Infobox film
| name           = Let Freedom Ring
| image          = 
| image size     = 
| alt            = 
| caption        =  Jack Conway
| producer       = Harry Rapf
| writer         = Ben Hecht
| based on       = 
| screenplay     = 
| narrator       = 
| starring       = Nelson Eddy Virginia Bruce Victor McLaglen
| music          = Arthur Lange Sidney Wagner
| editing        = Fredrick Y. Smith
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 87 min.
| country        =   English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 1939 in Western directed by Jack Conway, starring Nelson Eddy and Virginia Bruce.

==Plot summary==
  Edward Arnold), launches a newspaper aimed at combatting Knoxs tactics. Logan is disowned by his wealthy family and frozen out by his society friends. But with the help of woman-of-the-people Maggie Adams (Virginia Bruce), Logan sticks to his guns and perseveres. 

==Cast==
* Nelson Eddy as Steve Logan
* Virginia Bruce as Maggie Adams
* Victor McLaglen as Chris Mulligan
* Lionel Barrymore as Thomas Logan Edward Arnold as Jim Knox
* Guy Kibbee as David Bronson Charles Butterworth as The Mackerel
* H.B. Warner as Rutledge
* Raymond Walburn as Underwood
* Dick Rich as Bumper Jackson
* Trevor Bardette as Gagan
* George Gabby Hayes as Pop Wilkie (as George F. Hayes)
* Louis Jean Heydt as Ned Wilkie
* Sarah Padden as Ma Logan Eddie Dunn as Curly

==References==
 

== External links ==
*  
*  
*  
 Jack Conway

 
 
 
 
 
 
 


 