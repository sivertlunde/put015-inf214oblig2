Rajasthan (film)
 
{{Infobox film
| name           = Rajasthan
| image          = 
| image_size     =
| caption        = 
| director       = R. K. Selvamani
| producer       = N. Lakshmi
| story          = R. K. Selvamani
| screenplay     = R. K. Selvamani Sarathkumar Vijayashanti
| music          = Ilaiyaraaja
| cinematography = Saroojpaadhi
| editing        = V. Udhayashankar
| distributor    =
| studio         = 
| released       = 1999
| runtime        =
| country        = India Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1999 Telugu Sarathkumar and Vijayashanti in lead roles whilst Brahmanandam, Prakash Raj and Devan play supporting roles.  The film was later dubbed into Tamil under the same name, with some scenes re-shot with local actors. 

==Cast== Sarathkumar as Hariharan
*Vijayashanti as Gayathri
*Manivannan
*Prakash Raj Prithviraj
*Manjula Manjula
*Brahmanandam Ali
*Vadivelu
*Devan

==Soundtrack==
; Telugu
*Chilaka Chilaka
*Jai Jawan
*Kova Billa
*Mama Mama
*Swargamlo

; Tamil
*Aajaji
*Jai Jawan
*Machan
*Sirakadikkira
*Sorgathil

==References==
 

==External links==
 

 
 
 
 
 
 
 
 


 