Crayon Shin-chan: Unkokusai's Ambition
{{Infobox film
| film name = {{Film name| kanji          = クレヨンしんちゃん 雲黒斎の野望
| romaji         = Kureyon Shinchan: Unkokusai no Yabō}}
| name           = Crayon Shin-chan: Unkokusais Ambition
| image          =
| caption        = 
| director       = Mitsuru Hongo
| producer       = 
| writer         = Yoshito Usui
| starring       = Akiko Yajima Miki Narahashi Keiji Fujiwara Satomi Kōrogi
| music          = 
| cinematography = 
| studio         = Shin-Ei Animation
| editing        = 
| distributor    = Toho
| released       =  
| runtime        =
| country        = Japan
| language       = Japanese
| gross          = 
}}
  is a 1995 anime film. It is the 3rd film based on the popular comedy manga and anime series Crayon Shin-chan. The film was released in theatres on April 15, 1995 in Japan. 

The film was produced by Shin-Ei Animation, the studio behind the TV anime.

==Story==
The scene opens with Time Patrol machine T2327X traveling through time. A disintegration is detected in Japan in 1570 AD, during the Warring States Period. Snow storm time ring is a patrol of the future members of the 30th century. The patrolling officer suspects a crime and tries to contact headquarters. In spite of being unable to do so, the officer nevertheless heads off to investigate the happenings in that region. Suddenly, the time machine meets with a mysterious space-time torpedo assault, and is forced to conduct an emergency landing. The emergency landing location is deep underground, below the backyard of the Nohara family, under Shiros doghouse.

Due to the failure of the time machine, it and the officer are stuck deep underground. In order to contact the Noharas, the officer takes control of Shiros body. She informs the Noharas of the disturbance that occurred in the Warring States Period. Despite the initial reluctance of the Noharas to participate in the investigation, they agree later on, when Shinnosuke decides to do so, and when they are assured that their schedule will not be affected in any way. The three of them wear a field jersey dress (time suit) and board the disposable time machine, an emergency three-seater sphere type, and travel to the 16th century Warring States Period.

They land on the historical equivalent of present-day Kasukabe. An army of Unkokusais minions attempts to attack them, but a mysterious swordsman intervenes, and fends them off. The swordsman reveals that he is Fubukimaru, the heir of the castle Kasukabe, which was destroyed due to Unkokusais attacks. Fubukimarus parents, the lord and lady of the castle, were killed by Unkokusai, and her sister was kidnapped by him. She recounts the prophecy, "When the castle fell into crisis, 3 heroes and a dog appeared, to save everyone from crisis and ruin", and seeks the cooperation of the Nohara family and Shiro, believing them to be the "3 heroes and a dog". At first, the Noharas are reluctant, but eventually agree to help, mostly due to the fact that one of Unkokusais minions stole a bag from Misae, which happened to contain the savings bank passbook and the legal seal(Hanko (stamp)#Japanese usage|Hanko), and they wish to reclaim it.

As the Nohara family proceed toward their destination -Unkokusais castle-one of Unkokusais henchmen, Matatabi the cat-assassin, appears, with the aim to kill them. Fubukimaru attempts to stop him, but is at first unsuccessful and gets fatigued. furious even fall into crisis to lose us, a universal wearing of Shinnosuke jersey you can use in crying had been installed in the "cuesta Help!" "help function"   by, succeeded in warding off the cat by the support of Shinnosuke Roh proceed. Immediately after that, in turn silver when your subordinates Diana is also black cloud appeared, Misae and Hiroshi her by a mysterious force but marbles wind up being transformed into.

White (ring), how can snowstorm round defeat when the black cloud and really Shinnosuke. What can protect the history.

==Character==
*Fubukimaru Kasuga: 15-year-old swordsman Nohara family met in the Warring States period. Kasuga is a memento of the castle was destroyed. Are traveling toward the castle when the black cloud to help the sister that was trapped when the black cloud. There is also considerable arm of the sword, which overthrew many soldiers in the castle black cloud on board the "engine" of the most favorite horse is a tit of the country. Initially, any person claiming a man had to be found in the middle story were actually women. Depiction of the conflict, such as "not when forced to man "himself" at strategic points in her story suggests. After Shinnosuke was defeated when the black cloud, and lord of the castle Kasukabe. Replied, but when that get picked up by Shinnosuke "not wife of Ora?" And with "! Refuse." For the takeover of the castle was supposed to be black cloud 斎 by then there was no history, circle snowstorm is my understanding that "dream" when the black cloud events defeated. In the original comics is set and has a completely different movie version, but was convinced that women will arbitrarily and Shinnosuke and Hiroshi (Hiroshi says were male and Shinnosuke And I big hear).
It should be noted that, in this volume is Anime cartoon, Yoshi says strike has emerged as the hero of "Round snowstorm ninja boy" of the original book.
*Snow Storm Ring: Time patrol personnel time management of the Asian branch office 30th century. Pilot of patrol boat P23-27X. In that time, crime control, such as modification history. Two types of patrol suit the time and move to double the power and defenses, at a rate of 20 times the normal called "Dona suit on" Powered Suit have. Noticed that the abnormality occurs in the Warring States period, but was headed to the Warring States Period is the site of Kasukabe, crash landing in the basement of the home field attacked at that time. Ask because I no longer trapped, and to contact family through the body of the white field, and want to resolve the incident of the Warring States period, entrusting to time to suit them. Appeared before the family settled in the field after the Warring States period, was on the verge of invasion by the desperate situation of Hieru-Jokoman of in the modern world, along with the family to climb down to the Jokoman after helping them. After killing the Jokoman in cooperation with the Nohara family, the family thanked the deep field, went back to the future back to the original time zone of the family. Had time for back to early than the time of the original In this case, the family field is ("(ring), I pretty irresponsible child that says," Misae) have been compromised and their near-miss when going to the past. "Ring Snow Storm" her name would be "round snowstorm" when applied to Chinese characters. Feel depiction was seen even when the circle and face to face blizzards, snowstorm round against her "and others I do not think" and. In the manga version (Misae is to Shinnosuke for a different reason for the time slip Harisen that was able to jump into when you rip a punishment), and does not appear. Of pineapple that have been modified from figure gag after the final battle was Hairegu sexy.
*Hieru-Jokoman: A.k.a "Stinkpoop", History buffs in the future of human identity when the black cloud that appeared in the anime version (according to the business card out of himself, the title is "man of culture (history) and trend creator, art director, members of the association four-dimensional world history"). Quite tall with a squinty eye and when the difference between black cloud, long limbs. Hyohyoto personality was pointless. While said in his "hate violence" and, it is to tease you like dogs and children, at the Battle of Shinnosuke are fighting with us and enjoy from beginning to end. Confrontation and Shinnosuke after taking over the Kasukabe of the Warring States period, attacked the circle snowstorm and the Nohara family alone the next one subordinate of three decimal Roh cat, waxes soul, silver tail, has boarded the castle later in the story. Although Shinnosuke fight with the "issue you a bloodbath" robot, losing to Shinnosuke was transformed into an adult with the power of time suit, Kasuga be recaptured the castle. However, after losing to Shinnosuke has fled to the modern world, was converted into a world bizarre hijack a modern and "President" Japan Now, the culture of the wind during the Edo period, Sengoku has spread to Japan. In the final battle is also a rematch and Shinnosuke boarded the robot of the castle black cloud, in front of the family love of the family field, the action beam gun Kantamurobo was fired at the end of the battle with the Nohara family boarded the Kantamurobo with the help of a ring died leaving the words "GAME ... OVER ..., TIMEUP · · ·" is shot down in that. One of the enemy cornered the home field of the enemy character in the movie version.
*Matabinekonoshi ("Catnip killer of Cats"): When the black cloud of subordinates in the anime version. First person. Accomplishment sake, that are followed by delirium love. Showed off the arm of a master of Iai snowstorm or more round. He is one who murdered Fubukimarus mother, was invited to inform the circle snowstorm frenzy about it actually, are cut by hand by a circle of Shinnosuke borrowed snowstorm has turned into a cockroach, and fell in the rape fields. The future is unknown whether human.
*Tamashiro ("White balls"): When the black cloud of subordinates in the cartoon version. Women have been dressed like a. Misae has robbed the waist pouch. But in stick fighting and confrontation Hiroshi Shinnosuke (Akita specialty Shinnosuke clung to stick Kiritanpo retreat would be divided into three bars of round Emono snowstorm when the favorite has been taken). But also appeared at the castle after the black cloud, the art of generalization in the mirror bounce shot by aiming to when black clouds Hiroshi, has become a Bichiguso When light rays. Is thought that women themselves born with, it is round snowstorm was due to upbringing as a man. Men still like a love interest, has been dumped after Shinnosuke circle snowstorm "I am (even though the bride of Shinnosuke), but Kamawanu" You say, in turn, Oneisan of man "to Shinnosuke Yea had been refused with ". In the original comics would kill the magic by setting the Harisen dedicated, the knot hair (from Hiroshi and Shinnosuke was suspected because the circle snowstorm had been coming out with a man before that, however) has become women (younger sister) become a fan fan killed Ma.

==See also==
* Crayon Shin-chan
* Yoshito Usui

==References==
 

==External links==
*  

 
 

 
 
 
 
 