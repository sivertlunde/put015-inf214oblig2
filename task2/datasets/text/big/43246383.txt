Alpha and Omega: The Legend of the Saw Tooth Cave
{{Infobox film
| name            = Alpha and Omega: The Legend of the Saw Tooth Cave
| image           =
| caption         = Theatrical release poster Richard Rich Steve Moore Ken Katsumoto Daniel Engelhardt Susan Gelb
| writer          = Tom Kane
| based on        = Characters created by Steve Moore Ben Gluck Dee Dee Greene Blackie Rose Lindsay Torrance Dennis Cox
| music           = Chris P. Bacon
| cinematography  = Daryn Okada
| editing         = Louis La Praix Joe Campana
| studio          = Crest Animation Productions
| distributor     = Lionsgate Films
| released        =  
| runtime         = 45 minutes
| country         = United States
| language        = English
}} Alpha and Richard Rich. The film was released on iTunes on September 23, 2014    and had its DVD premiere on October 7, 2014.      

==Plot==
In a dark forest, a female wolf is being chased by other wolves as she hides in a bush. It is implied that she is killed when she tries to lead them away. In the bush, the glowing eyes of her pup are seen.

After this prologue, Winston and Tony are training the future alpha, including Claudette and Stinky. Runt appears but Winston tells him he is supposed to be in Omega school. Kate, Humphrey, and Lilly arrive. Kate tells Runt that Lilly plans to go on a field trip to Shadow Forest with the Omegas. As they enter Shadow Forest, a porcupine named Floyd appears to tell them they must leave, but the wolves ignore him. They come across the Saw Tooth Cave just as it gets windy around the cave, Lilly mistakes it for a windstorm. Humphrey tells the others they must leave, but Runt climbs a tree to take a better look only to fall from the tree when a ghost appears before him.

Runt, Humphrey, and Lilly return to the den and Eve asks them why they were late. Humphrey states Runt had an "incident," but Runt insists he is fine. Kate thinks a different field trip would be better, but Humphrey assures her that the forest is a good place to explore. After waking up from a nightmare, Tony decides to tell them what happened when he went there. When he was younger, Tony had gone on his first hunt with Lyle and Link in Shadow Forest. When a squirrel they had been chasing hid in the cave, Tony told the twins to go in the cave and look for it. The twins do so, but turn back when the wind became stronger. Tony went in the cave himself, only to get chased away by the ghost. He ends his story by stating that he never went into the forest again. Kate tells everyone that it is getting late and they return to their dens.

Kate and Humphrey are worried about the pups growing up as they prepare to sleep. Runt has a nightmare about the Saw Tooth Cave and sees the silhouette of a female wolf. He wakes up screaming from his dream and tells his siblings that he has to go back while convincing them to come with him. They agree and the next day, the pups set off back to the haunted forest. There, they meet two female porcupines, Frieda and Fran, who insult and forbid them to enter. The pups pass anyway, but as they approach Saw Tooth Cave, they are attacked and chased away by the ghost. They narrowly escape it. However, Claudette returns without Stinky. They look for him, but do not find him at first. Runt, in a tree, looks at Saw Tooth Cave and sees the same wolf he saw in his dream, realizing the ghost is protecting her life. The wolf points out where Stinky is - sliding down the edge of a cliff.

Runt and Claudette rescue him before returning to the den, where Kate and Humphrey are upset about the pups lying to them. Kate tells them to stay away from the forest, but the pups are fed up with them being overprotective. Tony encounters Lyle and Link again, who disagree with Omegas and pups exploring the forest due to it being "off-limits." Tony tells them that it is against his wishes, but the twins decide to venture in the forest anyway.

The pups set off again, this time followed by Kate and Humphrey. Kate offers to go on a family walk, but Claudette tells them they are grown up and want to explore by themselves. They hesitantly agree, but later Humphrey tells Marcel and Paddy to keep an eye on the pups. However, it does not take long for the pups to realize they are being followed, so they split up. Paddy follows Stinky and Marcel follows Runt and Claudette. Runt sneaks off to the Saw Tooth Cave to speak with the wolf. Runt goes in anyway to cheer her up, only for the ghost to appear. When he mentions he has been to Rabbit Poo Mountain, the wolf pulls him in the cave. When Runt asks for her name, she tells him to return the next day. At the same time, Lyle and Link are in Shadow Forest and encounter Freida and Fran, who scare them deeper into the woods. The twins approach the cave, only to be chased away again by the ghost. Lyle and Link tell the pack what happened, but no one believes.

The twins decide to go back in order to chase out the ghost, believing they will be hailed as "heroes". Runt hears this and they ask him if he saw anything unusual. Runt simply replies he has seen "something strange down by the river" before returning to the Saw Tooth Cave. He warns the wolf that some members of his pack are after her. This wolf, whose name is Daria, tells Runt her past and that she needs Runt to help her return to Rabbit Poo Mountain. Through a flashback, it is revealed that Daria was born genetically blind and the Head Wolf saw her "useless" and decided to kill her. Her mother saved her - thus breaking the rule of the pack - and sacrificed herself to protect Daria. She was found by Floyd who took Daria in to raise her. This also reveals that the ghost is her mother, who protects Daria from outsiders. As Lyle, Link, and other wolves come near the cave, Runt leads Daria and Floyd away. The other wolves enter the cave but Freida and Fran chase them away.

Humphrey wakes up to see Runt missing and with Kate, sets off to search for him while Runt helps Daria escape the forest. Kate and Humphrey encounter Freida and Fran, whom they ask about Runt. But they refuse to speak unless Kate and Humphrey both promise to help Runt. They do, but the porcupines agree to tell them only if they beg. Runt, Daria, and Floyd stop at a bridge to rest. Humphrey and Kate catch up with them the next day wanting to help before they arrive at Rabbit Poo Mountain. Kate leads Daria to the female wolves in the pack, but her mother is not there. The Head Wolf recognizes Kate and Daria, and he follows them when they leave. Lois, recognizing Daria as her sister, follows them as well to tell Daria that their mother was killed and that the packs laws never changed.

The Head Wolf and two others find them near Shadow Forest, where Kate and Humphrey fight two of them while the Head Wolf follows Runt and Daria into the forest. Daria narrowly escapes and the ghost attacks the Head Wolf, killing him. Kate and Humphrey catch up to see this as Daria thanks Runt for helping her. Freida and Fran are leading Winston and Eve to Shadow Forest just as the others arrive. Runt introduces Daria to his grandparents and tells them that the ghost was only protecting Daria, also saying that they must accept her or they will "continue to be haunted." Winston and the others agree and knowing that her daughter will finally be safe, Darias mothers spirit leaves Shadow Forest forever. Kate and Humphrey tell Runt that they are proud of him and the film comes to a close with all of them returning home with Daria as the newest member of their pack while the sun shines over Shadow Forest for the first time in many years.

==Cast==
*Gina Bowes as Daria / Freida / Fran
*Ben Diskin as Humphrey
*Kate Higgins as Kate / Stinky / Lilly Dee Dee Greene as Runt
*Dennis Cox as The Head Wolf
*Blackie Rose as Floyd
*Lindsay Torrance as Claudette
*Jojo Blue as Lois Larry Thomas as Winston
*Bill Lader as Tony
*Solomon Gartner as Lyle / Link
*Cindy Robinson as Eve Chris Smith as Marcel / Paddy

==Production== Richard Rich showing two short clips from the storyboards of both films.

==Critical reception== title = url =  work = publisher = accessdate =24 January 2015}} 

== Sequels ==
  is planned for release to DVD in August 2015    and three more sequels have been announced for Lionsgate to distribute, the first of which is intended for a release in early 2016, and will follow the "misadventures of Alpha Kate and Omega Humphrey and their three wolf pups, Claudette, Stinky and Runt, as they learn life lessons in the great outdoors". 

== References ==
 

== External links ==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 