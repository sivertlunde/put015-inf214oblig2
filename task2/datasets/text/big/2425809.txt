Mississippi Masala
{{Infobox film name = Mississippi Masala image = Mississippi_masala.jpg caption = Theatrical release poster director = Mira Nair producer = Studio Canal Souss writer = Sooni Taraporevala starring = Denzel Washington Roshan Seth Sarita Choudhury music = L. Subramaniam cinematography = Edward Lachman editing = Roberto Silvi distributor = The Samuel Goldwyn Company (USA) released =  : 17 January 1992 United States: 5 February 1992 runtime = 118 minutes country = United States United Kingdom language = English budget = $8 million gross = $7,308,786
}} romantic drama film directed by Mira Nair, based upon a screenplay by Sooni Taraporevala, starring Denzel Washington, Sarita Choudhury, and Roshan Seth. Set primarily in rural Mississippi, the film explores interracial romance between African Americans and Indian Americans in the United States.
 USD at the box office.

==Plot== expelled Asians Ugandan Indians residing in Kampala. It then moves forward to 1990. After spending a few years in England, Jay, Kinnu, and Mina have relocated in 1987 to Greenwood, Mississippi|Greenwood, Mississippi to live with family members who own a chain of motels there. Kinnu obtains work in a liquor store and Mina works for the motel of her extended family there (the Monte Cristo). She falls in love with Demetrius (Denzel Washington), a local African American self-employed carpet cleaner. The respective families erupt in turmoil after the pair are discovered and confronted by members of the Indian family during a clandestine weekend of pleasure in Biloxi, Mississippi|Biloxi. Ultimately, the two families cannot come to terms with the interracial pair, who flee the state together in Demetriuss van. After a brief return to Kampala to attend a court proceeding on the disposition of his confiscated Ugandan house, Jay relinquishes his long-nurtured dream of returning to Uganda, the place he considered home.

==Production== Ocean Springs. The Uganda scenes were filmed in Kampala, Uganda, including in Nairs home.   

==Cast==
* Denzel Washington - Demetrius Williams
* Sarita Choudhury - Mina
* Roshan Seth - Jay
* Sharmila Tagore - Kinnu
* Charles S. Dutton - Tyrone Williams
* Joe Seneca - Williben Williams
* Ranjit Chowdhry - Anil
* Joseph Olita - Idi Amin
* Mohan Gokhale - Pontiac
* Mohan Agashe - Kanti Napkin
* Tico Wells - Dexter Williams
* Yvette Hawkins - Aunt Rose
* Anjan Srivastav - Jammubhai (as Anjan Srivastava)
* Mira Nair - Gossip 1
* Rajika Puri - Gossip 2
* Richard Crick - Hotel Customer

==Awards==
*1993 NAACP Image Award for Outstanding Actor in a Motion Picture - Denzel Washington
* 1991 São Paulo International Film Festival - Mira Nair, Critics Special Award Golden Osella Best Original Screenplay - Mira Nair and Sooni Taraporevala; Golden Ciak/Best Film - Mira Nair

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 