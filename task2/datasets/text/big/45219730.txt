Balondu Bhavageethe
{{Infobox film 
| name           = Balondu Bhavageethe
| image          =  
| caption        = 
| director       = Geethapriya
| producer       = Geetha Srinath M. S. Karanth
| story          = Narayana Rao (Based on Novel)
| writer         = K. S. Sathyanarayana Geethapriya (dialogues)
| screenplay     = Geethapriya
| starring       = Srinath Anant Nag Saritha C. R. Simha
| music          = Hamsalekha
| cinematography = B. N. Haridas
| editing        = Suresh Urs
| studio         = Vijayeshwari
| distributor    = Vijayeshwari
| released       =  
| runtime        = 121 min
| country        = India Kannada
}}
 1988 Cinema Indian Kannada Kannada film, directed by Geethapriya and produced by Geetha Srinath and M. S. Karanth. The film stars Srinath, Anant Nag, Saritha and C. R. Simha in lead roles. The film had musical score by Hamsalekha.  

==Cast==
 
*Srinath
*Anant Nag in Special Appearance
*Saritha
*C. R. Simha
*Ramesh Bhat
*Shivaram
*Shimoga Venkatesh
*M. S. Karanth
*Umashree
*Padma Kumuta
*Anitharani
*Baby Rekha
*Baby Sangeetha
 

==Soundtrack==
The music was composed by Jairam. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Yauvvana Ondu || S. P. Balasubrahmanyam || R. N. Jayagopal || 04.17
|-
| 2 || Beeso Gaali || Malaysia Vasudevan, Vani Jayaram || Chi. Udaya Shankar || 05.06
|-
| 3 || Bala Sangeetha || Malaysia Vasudevan || Chi. Udaya Shankar || 03.23
|-
| 4 || Ide Roopa || Malaysia Vasudevan || Chi. Udaya Shankar || 04.39
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 

 