The Good and the Bad
Le Bon et les Méchants is a French film directed by Claude Lelouch and released in 1976 in film|1976.

==Synopsis==
From 1935 to 1945, the happiness of a couple turns to sadness when their  Traction Avant is used by the gang des Tractions Avant.

==Details==
*Director: Claude Lelouch
*Musique : Francis Lai, songs by Jacques Dutronc
*Length: 120 minutes
*Release date: 19 January 1976

==Starring==
*Jacques Dutronc : Jacques
*Marlène Jobert : Lola
*Jacques Villeret : Simon
*Bruno Cremer : Linspecteur Bruno Deschamps
*Brigitte Fossey : Dominique Blanchot
*Jean-Pierre Kalfon : Henri Lafont
*Alain Mottet : commissaire Blanchot
*Marie Déa : Mme Blanchot
*Serge Reggiani : chef de la Résistance
*Stéphane Bouy : Bony
*  Georg Marischka  : général allemand
*Philippe Léotard : vendeur de Citroën
*Alain Basnier : fils Blanchot
*Valérie Lagrange : Françoise
*  Claudio Gaia  : Claudio De Souza
*  Arlette Emmery  : Arlette
*Anne Libert  : sa copine
*  Hans Verner  : officier allemand
*  Oskar Freitag  : allemand
*  Otto Frieber  : allemand
*  Michel Fortin  : truand
*  Jean Luisi  : truand
*  Michel Charrel  : truand
*  André Falcon  : maire
*Etienne Chicot : lieutenant
*  Michel Peyrelon  : présentateur du défilé de mode
*  Gérard Dournel  : supérieur
*José Luis de Vilallonga : homme du couple dévalisé
*  Mme de Vilallonga  : femme du couple dévalisé
*  Jean Mermet  : vendeur de traction
*  Adrien Cayla-Legrand  : Charles De Gaulle
*  Gérard Sire  : voix narrateur/commentateur actualités/speaker radio (non crédité)
*  Tony Roedel  : officier allemand (non crédité)
*  Jean-Yves Gautier
*  Marie-Pierre De Gérando
*Jack Berard
*  René Morard
*Lisette Bercy
*Jean Bessière
*  Maurice Illouz
*  Serge Lahssen
*  Jacques Marty
*  Jacques Saint-Hilaire
*  Roland Neunreuther  : cascadeur.

== Details ==
* The film is projected in sepia tone|sepia.

==See also==
* 

 
 

 
 
 
 

 