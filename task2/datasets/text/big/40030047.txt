Hours (2013 film)
{{Infobox film
| name           = Hours
| image          = Hours2013Poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Eric Heisserer
| producer       = Peter Safran
| writer         = Eric Heisserer
| starring       =  
| music          = Benjamin Wallfisch
| cinematography = Jaron Presant
| editing        = Sam Bauer
| studio         =  
| distributor    = Pantelion Films  (Through Lionsgate Films|Lionsgate) 
| released       =  
| runtime        = 97 Minutes
| country        = United States
| language       = English
| budget         = $4 million  
| gross          = $783,997 (Foreign)  
}} South by Southwest Film Festival. It went on general release on December 13, 2013, in which it was considered a posthumous release after Paul Walkers death on November 30, 2013.

== Plot ==
In 2005, before Hurricane Katrina strikes New Orleans, Nolan Hayes, rushes his wife Abigail Hayes to the emergency room as she is in labor five weeks early. The doctor explains to Nolan that his wife gave birth to a baby girl but lost her life in the process due to liver failure. Nolan refuses to accept her death and is in grief. He then learns that his newborn baby needs to be attached to a ventilator for the next two days before she can breathe on her own. The only problem is that the hurricane is beginning to flood the hospital and the ventilator cant be moved.

When the hurricane becomes too strong, everyone in the hospital, including the doctors and nurses, evacuate the building, leaving only Nolan and his baby (that he names Abigail, after her mother) in the empty hospital. One of the nurses promises to bring back help and leaves, because Nolan is unwilling to abandon his baby. The power then goes out and Nolan is forced to find a way to keep his baby alive. Somewhere in a room, he finds a generator that has a crank to manually charge the battery on the ventilator. He also finds some more I.V.s for the baby to keep her nourished, and some food and drinks for him to stay alive as well. The charge on the incubator is very short and only lasts up to two to three minutes, and with each charge the battery life gets shorter. Using only that amount of time, Nolan tries to juggle trying to get help outside of the hospital and rushing to help his baby before her timer runs out. Sometimes, Nolan sits down with his baby and tells the story about how he met her mother, to help keep himself preoccupied. (The two met after stopping a bank robbery together.)

After this, Nolan finds a stray dog hiding in the hospital and takes care of it. He names the dog Sherlock while also having illusions of talking with his late wife. Nolan goes to the rooftop to find helicopters flying around the building. When he tries to signal one, criminals distract it by shooting at it, demanding to be rescued first. This leaves Nolan furious, but he has to return to his daughter to charge the battery before he can do more. Nolan manages to also find an ambulance on a flooded street outside. He tries to signal for help on a walkie-talkie, but has no time to complete the distress call as he has to return to the room to charge the battery.

Nolan attempts to find a spare battery, but has no luck. After running back upstairs and charging the battery, he returns and finds a generator in a flooded basement room, but it is ruined from water, and almost electrocutes him. Nolan barely returns to the room in time to charge the battery again. Despite being awake for over 36 hours without power and running low on food, (his hand cut from cranking the generator), Nolan continues to come up with more clever ways to charge the battery (i.e. using his foot and later a rod). He also plays games with Sherlock, like playing fetch with him and sharing his lunch meat given to him by one of the hospital cooks.

Looters soon start to break into the hospital and steal food and drugs. One comes in Nolans room and tries to steal food but gets attacked by Sherlock, who runs him off. Nolan later realizes that this man had robbed and killed the nurse who was bringing back supplies (as she promised earlier). Since he hasnt slept in almost two days, Nolan takes a shot of some adrenaline to keep himself awake. Two more looters later break in the hospital and try to steal drugs to get high with and sell. When Nolan finds that they are both armed with guns, Nolan takes two shots of the adrenaline and sneaks up on one, injecting him with it and giving him a fatal overdose. Nolan takes his Marlin 336 lever-action rifle and surprises the other thug, who has discovered his baby. Nolan shakes his head "no" while holding the rifle on him, trying to get him to leave his child alone. When the man pulls his gun on Nolan, he is shot and killed.

Nolan is now so exhausted he cannot crank it with his hand any longer. Nolan has to use both his hands to slowly crank the handle, but breaks it off accidentally. Nolans attempts to fix the crank generator fails, so he gives his child mouth-to-mouth resuscitation to keep her alive. Before he can do any more, Nolan passes out from shock, stress, and exhaustion. Nolan then hears the ventilator beeping; it is running out of power. However, he is too weak to get back up. Nolan accepts that he has failed his child. But Sherlock comes to his aid and brings two paramedics with him to save Nolan, and drag him out. When Nolan wakes up, the paramedics hear his baby crying and run off to find it. Abigail has finally learned to breathe on her own. The paramedics give Nolan his baby and the two embrace, with Nolan crying tears of joy as they are brought to safety.


== Cast ==
* Paul Walker as Nolan Hayes
* Génesis Rodríguez as Abigail Hayes
* TJ Hassan as Jeremy
* Nick Gomez as Jerry
* Judd Lormand as Glenn
* Michelle Torres as Hurricane Katrina Victim
* Kerry Cahill as Nurse Shelly
* Yohance Myles as Dr. Edmonds
* Natalia Safran as Karen
* Elton LeBlanc as Paramedic
* Tony Bentley as Doctor
* Emily D. Haley as Patient
* Christopher Matthew Cook as Lenny
* Cynthia LeBlanc as Head Nurse
* Lena Clark as Lucy

== Reception ==
On Rotten Tomatoes, a review aggregator, the film holds a 61% rating, with an average score of 5.2/10, based on 28 reviews.  On Metacritic, the film has a score of 55 (out of 100), based on 16 reviews.  Variety (magazine)|Variety film critic Joe Leydon wrote that Hours  "ingeniously simple setup is cunningly exploited for maximum suspense."  Film critic Steve Pulaski of Influx Magazine stated, "Hours works because, once more, it proves a little story can go a long way thematically and that bigger, deeper themes can surface when there’s small-scale filmmaking at hand."  Pulaski also edited the review to include a brief note about the passing of Walker at the conclusion of his review of the film, adding "if only I knew when watching Hours I was watching one of the last performances by a great actor."

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 