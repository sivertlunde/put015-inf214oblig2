Guru Brahma
{{Infobox film
| name           = Guru Brahma
| image          =
| caption        =
| director       = Veerappa Maralavadi
| producer       = Bhanu
| writer         = Veerappa Maralavadi
| screenplay     = Veerappa Maralavadi Sukanya Lokesh Sumithra
| music          = Hamsalekha
| cinematography = Kabir Lal
| editing        = K Balu
| studio         = Sri Ranga Creations
| distributor    =
| released       =  
| country        = India Kannada
}}
 1992 Cinema Indian Kannada Kannada film, Sumithra in lead roles. The film had musical score by Hamsalekha.  

==Cast==
 
*V. Ravichandran Sukanya
*Lokesh Sumithra
*Ramakrishna
*Brahmavar
*Ramesh Bhat
*Mukhyamantri Chandru
*Ashalatha
*Jyothi
*Mandeep Roy
*Shani Mahadevappa
*Rathnakar
*Agro Chikkanna
*B K Shankar
*Bharath Bhagavathar
*Go Ra Bheema Rao
*Nanjundaiah
 

== Soundtrack ==
The music was composed and lyrics for the soundtrack penned by Hamsalekha. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = no
| collapsed = no
| title1 =  Varuna Varuna
| extra1 = Mano (singer)|Mano, K. S. Chithra
| lyrics1 =
| length1 =
| title2 = Deepa Deepa Roopa
| extra2 = K. J. Yesudas, K. S. Chithra
| lyrics2 =
| length2 =
| title3 = Hetthu Hotthu Mutthu
| extra3 = K. J. Yesudas, Mano, Manjula Gururaj
| lyrics3 =
| length3 =
| title4 = O Prema Prema
| extra4 = Mano, Rajesh Krishnan
| lyrics4 =
| length4 =
| title5 = Maduve Maduve
| extra5 = K. J. Yesudas, Mano, K. S. Chithra
| lyrics5 =
| length5 =
|}}

==References==
 

==External links==
* 

 
 
 
 


 