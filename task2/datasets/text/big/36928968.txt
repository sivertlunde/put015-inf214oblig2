Oba: The Last Samurai
 
{{Infobox film
| name           = Oba: The Last Samurai
| image          = 
| caption        = 
| director       = Hideyuki Hirayama
| producer       = 
| writer         = Takuya Nishioka Gregory Marquette Cellin Gluck
| starring       = Yutaka Takenouchi
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 128 minutes
| country        = Japan
| language       = Japanese English
| budget         = 
}}

 , also known as Battle of the Pacific, is a 2011 Japanese World War II Pacific War drama film directed by Hideyuki Hirayama.    

==Plot== holds out for 512 days before surrendering on 1 December 1945, having lasted three months after Japans capitulation following the bombing of Hiroshima and Nagasaki. Ōba marches down from the mountain with his remaining survivors singing a song of departure to fallen comrades and presents his sword to the American commander in a formal and dignified manner, the last organized resistance of Japanese forces of the Second World War.

==Cast==
* Yutaka Takenouchi as Captain Sakae Oba Sean McGowan as Captain Lewis
* Mao Inoue as Chieko Aono
* Takayuki Yamada as Toshio Kitani
* Tomoko Nakajima as Haruko Okuno
* Yoshinori Okada as Saburo Bito
* Sadao Abe as Suekichi Motoki
* Daniel Baldwin as Colonel Pollard
* Treat Williams as Colonel Wessinger
* Toshiaki Karasawa as Horiuchi

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 