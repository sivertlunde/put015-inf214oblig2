Incendiary Blonde
{{Infobox film
| name           = Incendiary Blonde
| image          = Incendiary blonde.jpg
| image_size     = 
| caption        =  George Marshall
| producer       = Joseph Sistrom Buddy G. DeSylva Frank Butler Ken Englund James Edward Grant
| starring       = Betty Hutton Arturo de Córdova Charles Ruggles Albert Dekker Barry Fitzgerald
| music          = Robert Emmett Dolan John Leipold
| cinematography = Ray Rennahan
| editing        = Archie Marshek
| distributor    = Paramount Pictures
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
}}
 George Marshall and loosely based on a true story, the picture stars actress Betty Hutton in the titular role.

The music was written by Robert Emmett Dolan and nominated for an Academy Award for "Best Music, Scoring of a Musical Picture."

==Plot==
A tomboy named Mary Louise "Texas" Guinan lands a job with a Wild West show after proving she can ride a bucking bronco. The rodeos new owner is Romero "Bill" Kilgallen, who doubles Texass pay after the attention she gets from saving a toddlers life from a runaway wagon at a show.

Tim Callahan comes along, looking for a job as the shows press agent by promising not to tell what he has found out, that Texass "heroism" was a staged act, with a midget pretending to be the endangered child.

Texas sends money home to her impoverished family. Tim falls in love with her, but she prefers Bill, unaware that he is legally bound to an institutionalized wife. Tim ends up marrying Texas and promoting her new career on stage in New York.

Bill tries making movies in Hollywood, but things go badly. A gangster acquaintance, Joe Cadden, takes control of Nick the Greeks nightclub in New York and ends up making Texas his headliner there. Her fame grows, but a feud develops between Cadden and two other racketeers, the Vettori brothers, that leads to bloodshed and threats against Texas and Tim.

Bill saves her life, but is arrested and sentenced to jail. His own wife passes away, making him free to marry again, but Texas has discovered that she has an inoperable condition, and that she will die before Tim can get out of prison.

==Cast==
* Betty Hutton..............Texas Guinan Arturo de Cordova.....Bill Romero Kilgannon
* Charles Ruggles.........Cherokee Jim
* Barry Fitzgerald.........Michael "Mike" Guinan
* Albert Dekker............Joe Cadden
* Eduardo Ciannelli......Nick the Greek
* Bill Goodwin..............Tim Callahan
* Mary Philips...............Bessie Guinan
This was the final film for character actor Bud Jamison. Jamison, best known for his work with slapstick comedy team The Three Stooges (Disorder in the Court), died suddenly in September 1944.

==Production==
The film was announced in 1942 and was originally to have co-starred Hutton and Alan Ladd. SCREEN: Hedda Hoppers HOLLYWOOD
Los Angeles Times (1923-Current File)   23 Nov 1942: 14. 

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 


 
 