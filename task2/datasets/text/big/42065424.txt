Pixels (2015 film)
{{Infobox film
| name           = Pixels
| image          = PixelsMovieTeaserPoster.jpg
| caption        = Teaser poster Chris Columbus
| producer       = Adam Sandler Chris Columbus Allen Covert Mark Radcliffe
| screenplay     = Tim Herlihy Timothy Dowling
| story          = Tim Herlihy
| based on       =   Brian Cox Ashley Benson Jane Krakowski
| music          = Henry Jackman
| cinematography = Amir Mokri
| editing        = Hughes Winborne
| studio         = Happy Madison Productions 1492 Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = $110 million   
| gross          = 
}} 3D  science fiction action comedy comedy film Chris Columbus of the Brian Cox, Ashley Benson, and Jane Krakowski.

Principal photography on the film began on June 2, 2014 in Toronto, Ontario, Canada. The film is scheduled to be released on July 24, 2015.

==Plot== classic arcade President William retrogamers (Peter Dinklage and Josh Gad) to defeat the aliens with various technology similar to those used in games.

==Cast==
* Adam Sandler as Sam Brenner, a former 1980s video game champion, Williams childhood best friend, and the leader of the team. 
* Kevin James as President William Cooper, the President of the United States, Sams childhood best friend, and a member of the team. 
* Josh Gad as Neil Jackson, a conspiracy-theory-obsessed, sad-sack genius with really bad social skills who is also a member of the team. Unlike the rest of the group, he was a kid in the 1980s. 
* Peter Dinklage as Frank "The Crank" Evans, Sams brash former video game-playing nemesis and a member of the team. 
* Michelle Monaghan as Vanessa "Van" Pattern, a unique weapons developer and specialist for the military, a member of the team.  Brian Cox as Admiral Porter, a military heavyweight. 
* Ashley Benson as Lady Lisa, a beautiful warrior from the fictional 1980s video game Dojo Quest. 
* Jane Krakowski as Carolyn Cooper, the First Lady and the wife of William. 
* Sean Bean as Army General.
* Denis Akiyama as Toru Iwatani, the creator of Pac-Man.  Thomas Ridgewell as Fred, Dr. Johns assistant 
* Chris Parnell as Dr. John Mario Hughes, a good-hearted scientist.

==Production==
===Development=== Chris Columbus Sony Pictures and Sandlers Happy Madison banner.    Characters from classic arcade games such as Space Invaders, Pac-Man, Frogger, and Donkey Kong, among several others, have been licensed for use in the film. 

===Casting=== Brian Cox joined the cast, hell play military heavyweight Admiral Porter.    On July 1, Ashley Benson was added to the cast of the film to play Lady Lisa, a beautiful warrior from the fictional 80s video game Dojo Quest.    On July 9, Jane Krakowski joined the cast of the film to play the First Lady to James President Cooper.   

On October 20, during an interview with Film-News.co.uk, actress Michelle Monaghan said, "Yeah I adore Adam. We had the best time filming Pixels for 12 hours a day for three months straight, Adam Sandler, Josh Gad, Peter Dinklage and myself, its a comedy and those guys just know how to roll; their timing is impeccable."   

===Filming===
{{multiple image
 | align     = right
 | direction = horizontal
 | width     = 150
 | image1    = Pixels - NY Subway Entrance - Side View.JPG
 | caption1  = Movie prop for Pixels in downtown Toronto
 | image2    = Pixels - NY Subway Entrance - End View.JPG
 | caption2  = Prop for NY Subway entrance has no stairs
 }}
On March 25, 2014, Ontario Media Development Corporation confirmed that the film would be shot in Toronto from May 28 to September 9 at Pinewood Toronto Studios.   While principal photography on the film commenced in Toronto, Canada on June 2, 2014, using downtown streets decorated to resemble New York City.  On July 29, the filming was being taken place outside of Markham, Ontario|Markham, Ontario.    Filming was also done in Rouge Park area, extras were dressing in costume at Markhams Rouge Valley Mennonite Church.  On August 4, actors Josh Gad, Peter Dinklage and Ashley Benson were spotted in Toronto during the filming scenes for the film on Bay Street, which was transformed into a city block in Washington, D.C. and littered with wrecked vehicles and giant holes in the pavements.    The Ontario Government Buildings was doubled to transform into a federal office building in Washington. Actors were aiming at aliens which could not been seen but will be added later with CGI. 

On August 26, 2014, filming took place in Cobourg.  The filming was completed in three months, with 12 hours of shooting a day. 

==Release==
The film was originally scheduled to be released on May 15, 2015,   but on August 12, 2014, the release date was pushed back to July 24, 2015.   

=== Marketing ===
The first trailer was released on March 17, 2015.  The trailer received 34.3 million global views in 24 hours, which is Sonys biggest, breaking the previous record held by The Amazing Spider-Man 2 with 22 million views in 2014. 

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 