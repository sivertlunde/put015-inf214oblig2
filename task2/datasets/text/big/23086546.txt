Love Cavalcade
{{Infobox film name = Cavalcade damour image = Cavalcade d amour.jpg director = Raymond Bernard producer = Arnold Pressburger  music = Roger Desormière   Arthur Honegger   Darius Milhaud     cinematography = André Germain   Robert Lefebvre  editing = Charlotte Guilbert    
|country= France
|language= French  studio = Pressburger Films
|released= 17 January 1940 (Paris)
|runtime= 100 min 
}} French film directed by Raymond Bernard and written by Jean Anouilh.

==Plot==
Three episodes show how the owners of a certain French castle experience dramatic issues with their love interests. The plot spans three centuries.

==Cast== Claude Dauphin : Léandre, Hubert & Georges 
*Michel Simon :  Diogène, Monseigneur de Beaupré & Lacouret 
*Janine Darcey : Julie 
*Simone Simon : Juliette  
*Corinne Luchaire : Junie
* Saturnin Fabre : Lacouret
* Alfred Baillou : Un comédien
* Charles Vissières : Le maître dhôtel
* Marcel Melrac : Lemployé du gaz
* Jacques Castelot : Un danseur
* Pierre Labry : Le baron de Maupré
* Trubsky : Le marquis de Longuyon
* Henri Richard : Anthelme
* Christian Argentin : Le chapelin
* Henri Monteux : Joseph
* Hubert Daix : an actor
* Blanchette Brunoy : Léonie de Maupré
* Dorville : father of Junie
* Léon Larive : cook
* Milly Mathis : nurse

==Music==
Music for the film was composed by Roger Désormière, Arthur Honegger, and Darius Milhaud. Milhaud later adapted his music for La cheminée du roi René for wind quintet.

==References==
 

==External links==
* 
*  at AllRovi

 
 
 
 
 
 
 
 
 

 
 