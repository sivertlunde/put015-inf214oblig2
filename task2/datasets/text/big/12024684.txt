Between Love and Hate
 
 
 
{{Infobox film name           = Between Love and Hate image          = Between Love and Hate film poster.jpg film name      = {{Film name hangul         =  ,   hanja          =  , 그 참을 수 없는 가벼움 rr             = Yeonae, geu chameul su eobmneun gabyeoum mr             = Yŏnae, kŭ ch‘amŭl su ŏmnŭn kapyŏum}} director       = Kim Hae-gon producer       = Kim Jeong-su   Jo Jin-man writer         = Kim Hae-gon starring       = Kim Seung-woo Jang Jin-young music          = Lee Han-na cinematography = Choi Gi-youl editing        = Kyung Min-ho distributor    = Cinema Service released       =   runtime        = 125 minutes country        = South Korea language       = Korean budget         =   gross          =   
}}
Between Love and Hate (also known as The Unbearable Lightness of Dating) is a 2006 South Korean film starring Kim Seung-woo and Jang Jin-young, and is the directorial debut of screenwriter Kim Hae-gon. Jangs performance won her Best Actress at the 2006 Korean Film Awards.  This would be Jang Jin-youngs final film before her death almost 3 years later.

== Plot ==
Young-woon works in his mothers restaurant, and is more interested in having a good time with his friends than settling down with his fiancée. He allows himself to be seduced by bargirl Yeon-ah, and the two embark on a tumultuous love-hate relationship. But when Young-woons mother finds out about the affair and pushes him into marrying his fiancée, he is forced into choosing between the two women.

== Cast ==
* Kim Seung-woo as Young-woon
* Jang Jin-young as Yeon-ah
* Sunwoo Yong-nyeo as Young-woons mother Kim Sang-ho as Director Jeon
* Nam Sung-jin as Joon-hee
* Jung Soo-hyung as Min-gu
* Oh Jung-se as Tae-gu
* Oh Dal-su as Hak-yi
* Tak Jae-hoon as Joon-yong

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 


 
 