One Water
{{Infobox film
| name           = One Water
| image          = One Water (documentary film) poster art.jpg
| caption        = A Global Film About Our Changing Relationship to Water
| producer       = Sanjeev Chatterjee
| director       = Sanjeev Chatterjee, Ali Habashi
| writer         = Sanjeev Chatterjee
| starring       = 
| music          = Thomas Sleeper
| editing        = Ali Habashi
| distributor    = 
| released       =  
| runtime        = 68 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
One Water is a 2008 documentary film directed by Sanjeev Chatterjee and Ali Habashi. The film premiered at the 2008 Miami International Film Festival on 22 February 2008.

==Film== freshwater crisis. At the heart of the project was the motion picture "One Water."
 Dalai Lama, Robert Kennedy Jr., Felipe Fernández-Armesto and Oscar Olivera - among others.  Donna E. Shalala, president of the University of Miami also appears in the film as narrator.  

The original score for "One Water" was recorded in Moscow by the Russian National Orchestra.

In 2006, "One Water" received major support from the John S. and James L. Knight Foundationas part of its establishment of the Knight Center for International Media at the School of Communication, University of Miami.

The central idea behind the project was the making of a documentary that would spark the creation of further media on the subject of water around the world. World Water Day (22 March 2009). This partnership has also launched the International Water Journalism website   that is receiving water related multimedia articles produced by journalists around the world.

In early 2009, an international television version of the film was created with a script written by Sanjeev Chatterjee and narrated by Hollywood star Martin Sheen Sheen was brought to the project by executive producer, Sam L Grogg who formerly collaborated with Sheen on the theatrical motion picture DA. This version of the documentary will air on Discovery Networks Planet Green in August 2010.

==Theme==
The documentary One Water assesses the deficiency of potable, safe water on a global scale. The movie highlights how communities across the world are dramatically suffering from a lack of our most valuable resource, thanks to drought, pollution and other factors. It becomes apparent that although this issue demands an immediate, workable solution, the problem is not easily resolved. The filmmakers also discuss the vitality of water on parallel spiritual and physical levels.

==Water crisis==
 
 

A 2006 United Nations report stated that "there is enough water for everyone", but that access to it is hampered by mismanagement and corruption. 

The UN World Water Development Report (WWDR, 2003) from the World Water Assessment Program indicates that, in the next 20 years, the quantity of water available to everyone is predicted to decrease by 30 percent. 40 percent of the worlds inhabitants currently have insufficient fresh water for minimal hygiene. More than 2.2 million people died in 2000 from waterborne diseases (related to the consumption of contaminated water) or drought. In 2004, the UK charity WaterAid reported that a child dies every 15 seconds from easily preventable water-related diseases.

==Awards==
*Winner, Best of Festival, Columbus International Film and Video Festival, 2009.
* Winner, Best Cinematography Award and Winner, Honorable Mention, Documentary Feature, Fargo Film Festival, 2009.
* Short listed for Awards Jury at 7th Annual Wild and Scenic Environmental Film Festival, Nevada City, CA,   Jan. 10, 2009.
* Winner, Best Documentary at Foyle Film Festival, The Northern Ireland International Film Festival, 2008,  Derry, United Kingdom.
* Winner of Best Feature Environmental Preservation 5th Annual Artivist Film Festival & Awards, 2008, Los Angeles, CA
* Winner, Best Film on Sustainable Development at Festival Cinemambiente 2008, Turin, Italy.
* Winner, Honorable Mention Inspirational Excellence Award, Documentary Competition, at Bayou City Inspiration Film Festival, 2008, Houston, Texas.
* Winner of the AMAP Prize,  EcoVision Film Festival, 2007 Palermo, Italy.
* Official Program Selection, Agua i Cine, 4th World Water Forum, 2006, Mexico City, Mexico  (22 min version)
* Winner of Documentary Award of Excellence and Best of Competition in Technical Merit Broadcast Education Association, Festival of Media Arts, 2004 (22 min version)

==Other Selected Screenings==
* INFOBAE - IV Pan-American Internet Communication Encounter, 9 December 2008, Buenos Aires, Argentina.
* Fort Lauderdale International Film Festival, October 15–11 November 2008, Fort Lauderdale, FL.
* Vancouver International Film Festival, September 25–10 October 2008, Vancouver, BC, Canada.
* Highway Africa Conference, Rhodes University, September 2008, Grahamstown, South Africa.
* Salzburg Academy for Media and Global Change, August 2008, Salzburg, Austria.
* International Forum of Organic Agriculture Movements (IFOAM) Organic World Congress, June 18–20, 2008, Modena, Italy.
* Expo Zaragoza, June 14–14 September 2008, Zaragoza, Spain.
* Miami International Film Festival, 22 February 2008, Miami, FL.

==See also==
* 1998 Klang Valley water crisis
* China water crisis
* California Water Wars
* Nature documentary
* Political cinema

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 