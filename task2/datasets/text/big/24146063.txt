The Beat (2003 film)
{{Infobox film
| name           = The Beat
| image          = the beat 2003 film.jpg
| image_size     =
| alt            =
| caption        = The Beat official DVD cover.
| director       =  Brandon Sonnier  Kenneth Arnold (executive)
| writer         = Brandon Sonnier
| narrator       =
| starring       =  
Rahman Jamaal 
Jazsmin Lewis 
Gregory Alan Williams 
Brian McKnight 
Steve Connell 
Jermaine Williams
| music          = Rahman Jamaal Chris Westlake
| cinematography = Graham Futerfas
| editing        = John Randle
| studio         = Symbolic Entertainment Tripped Out Productions
| distributor    = Allumination FilmWork LLC
| released       = 2005
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
The Beat is a 2003 drama written and directed by Brandon Sonnier and was screened at the  2003 Sundance Film Festival.  At 20 years of age, Sonnier was the youngest director to have a feature film debut at the Sundance Film Festival.  The previous record was held by Robert Rodriguez for El Mariachi.  With limited release on Showtime, Black STARZ, and Encore in 2005, the film became an underground hip hop cult-classic, supported by a cast of internationally recognized rap artists such as Chino XL, Tak (Styles of Beyond) and Coolio, as well as Def Poetry Jams Steve Connell.  The films star, Rahman Jamaal also wrote and produced the films hip hop soundtrack.

==Plot==
Philip Randall Bernard a.k.a. "Flip" (Rahman Jamaal) is faced with the decision to join the police force, or attempt to live his dream of becoming a Hip Hop/spoken word artist. The film shows both of Flips possible futures.

==Cast==
Rahman Jamaal 
Jazsmin Lewis 
Gregory Alan Williams 
Brian McKnight 
Steve Connell 
Jermaine Williams

==External links==
New York Times Move Review    

==References==
 

 
 
 
 
 
 


 