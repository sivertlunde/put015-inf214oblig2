Aashirwad
{{Infobox film
| name           = Aashirwad
| image          = Aashirwad.jpg
| image_size     = 
| caption        = 
| director       = Hrishikesh Mukherjee
| producer       = Hrishikesh Mukherjee   N.C. Sippy 
| writer         = Story:  
| narrator       =  Sanjeev Kumar Sumita Sanyal
| music          = Vasant Desai Gulzar
| cinematography = T.B. Seetaram
| editing        = Hrishikesh Mukherjee
| distributor    = 
| released       = 1968
| runtime        = 146 min.
| country        = India
| language       = Hindi
| budget         = 
}} 1968 Bollywood Sanjeev Kumar.

The film is notable for its inclusion of a Rap music|rap-like song performed by Ashok Kumar, "Rail Gaadi".   

==Plot summary==
 

Jogi Thakur (Ashok Kumar) is a simple man of high principles. He is a resident son-in-law who, with his wife, has been bequeathed property and estates by his father-in-law. He breaks his marriage to an autocratic landlady (Veena) when he finds out that on her wifes order, the chief accountant of the estate has cunningly obtained his signatures on an order that the houses of the poor be burnt. He leaves home, vowing never to return as long as he lives, leaving his daughter Neena behind. He moves to Mumbai where he makes a living by entertaining children in a park(the famous song "Rail Gaadi", said to be Indias first rap number). He is specially fond of a girl whose name, incidentally, is Neena (played by baby Sarika), too.  Unfortunately the girl takes ill and dies.  

He then returns to his village, Chandanpur, where he finds that the daughter of one of his villager friends, Baiju, has been abducted.  He rushes to the place where she is about to be raped by the estates cunning chief accountant, and kills him to protect the girl.  The villagers make up a phony story to save him, but he opts to tell the truth in the court and is jailed. There, he starts tending to the garden and composes philosophical poems. The doctor at the jail, Dr. Biren (Sanjeev Kumar) takes a special liking to him. Coincidentally, Neena, Jogi Thakurs daughter is set to be married with the doctor.  Jogi Thakur finds this out by chance as he is tending to the garden outside doctors room and overhears their conversations.  He also learns that his daughter hates criminals.  And so he shields his face from her on the few occasions that they meet. Unfortunately, he takes ill just as he is granted pardon by the government for his good behaviour.  The doctor,  has come to think of him as a father figure.  He tells Jogi Thakur that the day he will be out of jail will be the eve of his marriage.  Jogi Thakur is taken by the desire to see his daughter being wedded, and hurries to see her.  However, he does not want anyone to recognize him.  Finally he joins the beggars who have gathered for a treat for the marriage, where his daughter and son-in-law are serving food.  He manages to give his blessings to her and hurries out.  However, as he collapses on the road, he is recognized and people gather around him.  The news reaches his daughter who rushes to the spot to meet with her father at his last moment.

Adorned with such wonderful songs such as Rail Gaadi Rail Gaadi Chhuk Chhuk Chhuk Chhuk Chhuk Chhuk Chhuk, Beechwale Station Bole Ruk Ruk Ruk Ruk Ruk Ruk Ruk Ruk, sung by Ashok Kumar himself, Ek Tha Bachpan by Lata Mangeshkar (sung by Jogi Thakurs daughter), and finally the memorable Jeevan Se Lambe Hai Bandhu Yeh Jeevan Ke Raste by Manna Dey (sung by the bullock-cart driver who carries Jogi Thakur near his village in the night), the movie is an emotional journey through a mans life who stands by his principles only to be overcome by love for his daughter in the end.

==Cast==
* Ashok Kumar as Shivnath Joggi Thakur Choudhary
* Sanjeev Kumar as Doctor Biren
* Sumita Sanyal as Neena Bittu S. Choudhary
* Veena as Leela S. Choudhary
* Sajjan as Ramdas
* Harindranath Chattopadhyay as Baiju Dholakia 
* Padma Kumari as Rukmini
* Bipin Gupta
* S.N. Banerjee as Mohan
* Amar Kumar
* Brahm Bhardwaj
* Leela Desai
* Abhi Bhattacharya	as Jailor
==Soundtrack==

The music of the film was by Vasant Desai, with lyrics by Gulzar.
* "Ik tha bachpan" - Lata Mangeshkar
* "Rail ghadi chuk chuk chuk chuk" - Ashok Kumar
* "Jeevan se lambhe hain bandhu yeh jeevan ke rastey" - Manna Dey
*"Rim-Rim Jim Barsay Sawariya" 

==Awards==
*1969 Filmfare Best Actor Award for Ashok Kumar
*1969 National Film Award for Best Actor for Ashok Kumar
*1969 National Film Award for Best Feature Film in Hindi

== References ==

 
==External links==
*  
* 

 
 

 
 
 
 
 