Jani Gal
{{Infobox film|
 name =  Jani Gal|
 image = |
 caption = Jani Gal film poster |
 writer = Ibrahim Ahmad, Khosro Sina   Jamil Rostami|
 starring = Nezar Selami, Renas Voria |
 director = Jamil Rostami |
 producer = Suli Film|
 released =  July, 2007| min |
 language = Kurdish language|Kurdish|
 }} Iraqi film directed by Iranian director Jamil Rostami . It is based on a celebrated novel by Kurdish author Ibrahim Ahmad and Film written by Iranian screenwriter khosro Sina. It was Iraqs submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.        

==Plot== flashback structure. When Juamers wife goes into labor, he runs to get the midwife but is caught in the midst of a clash between Kurdish protestors and Iraqi police. He is mistakenly arrested, tortured and sentenced to ten years imprisonment. After his release, Juamer sets out to find his loved ones but he discovers that his wife died, without medical help, on the same day that he was arrested.   

==Awards==
#Best International Filmmaker Award, Boston Film Festival, 2008.
#Columbine Award, Moondance International Film Festival, 2008.

==See also==
 
*Cinema of Iraq
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 