Happiness Is (film)
 
{{Infobox film
| name           = Happiness Is
| image          = Happiness Is.jpg
| alt            =  
| caption        = 
| director       = Andrew Shapter
| producer       = 
| writer         = 
| narrator       =
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Happiness Is is a 2009 documentary film that the examines the different interpretations of the "pursuit of happiness" in America.  The film features interviews with notable personalities such as His Holiness The Dalai Lama, Willie Nelson and John Mellencamp as well as happiness authors Daniel Gilbert, Gretchen Rubin and Darrin McMahon. Its the second film from director and writer Andrew Shapter.

==Plot==
Happiness Is is a road trip documentary that explores Americans “pursuit of happiness”.    It is the second film from Andrew Shapter, director of the popular music documentary, Before the Music Dies and his first with producer, Tracy Marino, who is also a producer on his next film, The Teller and The Truth. Happiness Is examines Americas constant struggle to find more happiness as it breaks down the many interpretations of what Thomas Jefferson really meant when he wrote the words "Life, Liberty and the pursuit of happiness".   

==References==
 

==External links==
*  

 
 
 
 
 

 