Spook Busters
{{Infobox film
| name           = Spook Busters
| image          = Spook Busters.jpg
| caption        = Theatrical poster
| director       = William Beaudine
| producer       = Jan Grippo Tim Ryan
| starring       = Leo Gorcey Huntz Hall Bobby Jordan William Benedict Gabriel Dell
| music          = Edward J. Kay
| cinematography = Harry Neumann William Austin
| distributor    = Monogram Pictures
| released       =  
| runtime        = 68 minutes
| language       = English
| budget         =
}}
Spook Busters is a 1946 film starring the comedy team of The Bowery Boys.  It is the fourth film in the series.

==Plot==
All of the boys have just graduated from school where they learned exterminating, except for Sach who flunked out.  They set up their new business in a corner of Louies Sweet Shop and quickly get a job to remove ghosts from an old abandoned mansion.  Upon arrival they discover weird events taking place, such as lights turning on when a match is lit, and a disappearing organ.  Soon they discover that these events are not the actions of ghosts, but of a mad scientist who is conducting illegal experiments in the basement.

Upon encountering the scientist, Sach quickly becomes part of the experiment when the scientist wants to take part of his brain out and put it into a gorilla.  A fight ensues and, after the cops arrive and apprehend the criminals, the boys find themselves at the police station telling the story of what happened.  Louie then calls them and tells Slip that the mouse in his store "had puppies" and the boys quickly leave the police station to go to their next job.

==Production==
Gabriel Dell makes his first appearance of the series, playing an old member of the gang who just returned from a stint in the Navy and newly married to a French woman.

The film was made under the working title Ghost Busters. 

==Cast==

===The Bowery Boys===
*Leo Gorcey as Terrance Slip Mahoney
*Huntz Hall as Sach
*Bobby Jordan as Bobby
*William Benedict as Whitey
*David Gorcey as Chuck

===Remaining cast===
*Gabriel Dell as Gabe
*Bernard Gorcey as Louie Dumbrowski
*Douglass Dumbrille as Dr. Coslow
*Tanis Chandler as Mignon
*Maurice Cass as Dr. Bender

==Home media==
Released on VHS by Warner Brothers on September 1, 1998.
 Warner Archives released the film on made to order DVD in the United States as part of "The Bowery Boys, Volume Two" on April 9, 2013.

==References==
 

== External links ==
*  

 
{{succession box
| title=The Bowery Boys movies
| years=1946-1958
| before=Bowery Bombshell 1946
| after=Mr. Hex 1946}}
 

 
 

 
 
 
 
 
 
 
 
 
 