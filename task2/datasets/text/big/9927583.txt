Two Cars, One Night
 
 
{{Infobox film
| name           = Two Cars, One Night
| image          = 
| caption        = 
| director       = Taika Waititi
| producer      = Vanesser Alexander Catherine Fitzgerald Ainsley Gardiner
| writer         = Taika Waititi
| narrator       = 
| starring       = Rangi Ngamoki Hutini Waikato
| music          = Craig Sengelow
| cinematography = Adam Clark
| editing        = Owen Ferrier-Kerr
| distributor    = 
| released       = 
| runtime        = 
| country        = New Zealand
| language       = English Māori language|Māori
| budget         = 
}}

Two Cars, One Night is a short film, 11 minutes in length, written and directed by Taika Waititi.   

Released in 2004, the film is about two boys and a girl meeting in the carpark of a rural pub in Te Kaha, New Zealand. What at first seems to be a relationship based on rivalry soon develops into a potential friendship.

==Awards== Live Action Short Film, Academy Awards AFI Film Festival
*2004 Panorama Short Film Award, Berlin International Film Festival
*2004 Hamburg Short Film Award, Hamburg International Short Film Festival
*2004 Award of the Theatre Owners, Oberhausen International Short Film Festival
*2004 Short Film Competition Award, Seattle International Film Festival
*2004 Best Short Film Performance, NZ Film and TV Awards
*2004 Best Short Film Screenplay, NZ Film and TV Awards
*2004 Best Technical Contribution to Short Film, NZ Film and TV Awards

==References==
 

==External links==
* 
* 

 
 
 
 
 


 
 