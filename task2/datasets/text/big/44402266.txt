The Ghost of Twisted Oaks
{{infobox film
| director = Sidney Olcott
| producer = Sid Films
| writer = Pearl Gaddis
| starring = Valentine Grant Florence Walcott James Vincent
| distributor = Lubin Manufacturing Company
| released = November 11, 1915
| runtime = 3000 ft
| country = United States
| language = Silent English intertitles
}}
The Ghost of Twisted Oaks is an American silent film produced by Sid Films and distributed by Lubin Manufacturing Company. It was directed by Sidney Olcott with Valentine Grant and Florence Walcott in the leading roles.

==Cast==
* Valentine Grant - Mary Randall
* Florence Walcott - Her Mother
* James Vincent - Jack Carlton
* Arthur Donaldson -

==Production notes==
The film was shot in Jacksonville, Fla.

==External links==
* 
*    at website dedicated to Sidney Olcott

 
 
 
 
 
 
 
 


 