Terminus (1961 film)
 
  British Transport documentary directed by John Schlesinger which presents a "fly-on-the-wall" look at an ordinary day at Waterloo Station in London. Along with most British Transport Films, it was produced by Edgar Anstey. It was nominated for a BAFTA Film Award for Best Documentary and, for a time, the Academy Award for Best Documentary Feature before being disqualified after it was discovered that the film was first released prior to the eligibility period. Original music was by Ron Grainer.

Many of the "reportage" shots were actually staged. Schlesinger makes a cameo appearance as a passing, umbrella-carrying business man and one of the main characters in the film, a tearful and apparently lost five-year-old, Matthew Perry, was abandoned deliberately by his mother Margaret, an actress relative of Schlesinger. Other characters, including handcuffed convicts and a confused elderly woman, were actors. 

== References ==
 

== External links ==

*  

 

 
 
 
 
 
 


 