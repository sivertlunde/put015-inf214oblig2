Der Hund von Baskerville (1929 film)
{{Infobox film
| name           = Der Hund von Baskerville
| image          =
| caption        =
| director       = Richard Oswald
| producer       = Fred Lyssa
| writer         = Arthur Conan Doyle (novel)   Georg C. Klaren  Herbert Juttke
| starring       = Carlyle Blackwell   Alexander Murski   Livio Pavanelli   Fritz Rasp
| music          = 
| cinematography =  Frederik Fuglsang
| editing        = 
| studio         = Erda-Film
| distributor    = Süd-Film
| released       = 28 August 1929
| runtime        = 
| country        = Germany 
| awards         =
| language       = Silent   German intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent mystery film directed by Richard Oswald and starring Carlyle Blackwell, Alexander Murski, Livio Pavanelli. The film is an adaptation of the Sherlock Holmes novel The Hound of the Baskervilles by Arthur Conan Doyle. It was the last Sherlock Holmes adaptation in the silent film era.   

==Production==
Richard Oswald had penned an earlier adaptation of Conan Doyles tale for the 1914 serial Der Hund von Baskerville. This version was not a remake of that serial but was a straight adaptation of the source material. 

The British-based American actor Carlyle Blackwell was hired to play Holmes, as he was "suitably Britannic". 

==Cast==
*  Carlyle Blackwell as Sherlock Holmes 
* Alexander Murski as Lord Charles Baskerville 
* Livio Pavanelli as Sir Henry Baskerville 
* George Seroff as Dr. Watson
* Betty Bird as Beryl 
* Fritz Rasp as Stapleton 
* Valy Arnheim as Barrymore 
* Alma Taylor as Mrs. Barrymore 
* Carla Bartheel as Laura Lyons 
* Jaro Fürth as Dr. Mortimer  Robert Garrison as Falkland

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 