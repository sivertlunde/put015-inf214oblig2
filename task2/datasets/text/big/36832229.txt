Stardust on the Sage
{{Infobox film
| name           = Stardust on the Sage
| image          = Stardust_on_the_Sage_Poster.jpg
| border         = yes
| caption        = Theatrical release poster
| director       = William Morgan
| producer       = Harry Grey
| screenplay     = Betty Burbridge
| story          = {{Plainlist|
* Dorrell McGowan
* Stuart E. McGowan
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette William Henry
* Edith Fellows
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = Bud Thackery
| editing        = Edward Mann
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 65 minutes Magers 2007, p. 213. 
| country        = United States
| language       = English
| budget         = $86,378 
| gross          = 
}} Western film William Henry, and Edith Fellows.     Written by Betty Burbridge, based on a story by Dorrell and Stuart E. McGowan, the film is about a singing cowboy who helps his fellow ranchers against a corrupt mine superintendent looking to steal the mine in which theyve invested. The film soundtrack features two duets by Autry and Fellows, "When the Roses Bloom Again" and "Ill Never Let You Go, Little Darlin". The final scene includes an innovative audience sing-along medley of the classics "You Are My Sunshine", "Home on the Range", and "Deep in the Heart of Texas", with Burnette conducting and the words appearing at the bottom of the screen. Magers 2007, p. 214. 

==Plot==
The Rawhide Mining Company is looking for investors to support their hydraulic mining operations in the area. Radio promotions have drawn the interest of local ranchers. Singing cowboy and cattleman Gene Autry (Gene Autry) opposes the company and warns his fellow ranchers not to invest in it—too many local mines have already failed. After saving teenager Judy Drew (Edith Fellows) by stopping her runaway carriage, Gene meets her sister, Nancy Drew (Louise Currie), who runs the local radio station which features a show sponsored by the mining company. Nancy is pleased to see "progressive ranchers" supporting the mining company and tries to elicit Genes endorsement, but he remains unimpressed.
 William Henry), who also manages Genes Western Packing Company. Jeff is relying on investment money from the ranchers after he embezzled the Cattlemens Association payment to the packing company in order to keep the mine in operation. Mine superintendent Dan Pearson (Emmett Vogan) reminds Jeff that if the mine stops production, the Atlas Mining Equipment Company will take possession. Jeff plans a party where he intends to persuade the ranchers to invest in the mining company. Wanting to support her brother, Nancy offers Gene a ride to the party and fakes a flat tire, preventing him from attending.

The next day, after talking with the ranchers, Gene realizes what Nancy was up to and goes to see her. After secretly recording their conversation, Nancy edits Genes words and broadcasts the altered conversation making it seem as if Gene is endorsing the mining stock. Later, Gene hears the edited conversation broadcast on the radio and is furious—especially when he learns that the ranchers have followed his "advice" and purchased the stock. When Gene and his sidekick Frog Millhouse (Smiley Burnette) go after Nancy, they encounter three armed thugs who just robbed Nancy and Pearson of the payroll. When Nancy and Pearson arrive, Nancy offers to show Gene the mine to prove that it is a good investment. Pearson offers to take the thugs to jail.

Unknown to Gene and the others, Pearson owns the Atlas Mining Equipment Company and wants the mine to fail so he can become the owner. Pearson in fact hired the thugs to steal the payroll hoping the unpaid miners would walk off the job. Later, Gene becomes suspicious of Pearson when he claims the thugs escaped. Meanwhile, Tom, feeling guilty about his actions, prepares a suicide note and is prevented from shooting himself by Gene who arrives just in time with Nancy. After Jeff confesses, Nancy persuades Gene that the only way to save Jeff from prison is for the mine to succeed. Knowing its the only way to get back the Western Packing Companys money, Gene begins promoting the mine to the ranchers, assuring them he is investing in the stock.

While the town gathers for Genes jamboree dance to promote the mining company, Gene is ambushed by Pearsons henchmen on the trail and held captive. When Gene doesnt show up at the dance, Pearson convinces the guests to leave. Judy breaks into a song and Gene arrives to join her on stage—having escaped Pearsons henchmen—and the two sing a beautiful duet. Judy has had a crush on Gene since he saved her from the runaway carriage. After Gene gives his word that he is an investor in the mine, the people rush to invest their money as well.

Still set on sabotaging the mine, Pearson orders his men to empty the dam supplying the power for the hydraulic equipment, then goes to the radio station and tells Nancy that the water supply gave out. When Nancy isnt looking, Pearson switches on the radio microphone and the whole town hears him lie that he warned Gene that the water would dry up and that Gene still wanted to sell the stock for his own profit. Feeling double-crossed, Nancy and the angry townspeople confront Gene, who rushes off to the mine when he learns about the water. After fighting Pearsons henchment, he comes up with a plan to replenish the reservoir by blowing up an abandoned mine shaft to let the water through.

Back in town, Gene picks up a box of explosives, just as the townspeople close in. With Judys help, Gene escapes with the box of explosives. After setting the dynamite in place, Pearson and the townspeople arrive and try to arrest Gene, but Frog accidentally sets off the explosion, which replenishes the reservoir and provides the necessary water for the mining operation. One of Pearsons henchmen confesses that Pearson was behind the sabotage. Soon after, the mine is fully operational, and Gene and Frog broadcast a show from Nancys radio station and lead the audience in a sing-along. Magers 2007, pp. 213–214. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse William Henry as Jeff Drew
* Edith Fellows as Judy Drew
* Louise Currie as Nancy Drew
* Emmett Vogan as Dan Pearson
* George Ernest as Curly
* Vince Barnett as Haskins
* Betty Farrington as Mrs. Haskins
* Roy Barcroft as Henchman Murphy
* Tom London as Henchman MacGowan
* Champion as Genes Horse (uncredited)    

==Production==
===Casting===
In 1942, Edith Fellows appeared in two Gene Autry films, Heart of the Rio Grande and Stardust on the Sage, which highlighted her fine singing voice. Born March 20, 1923, Fellows became a child star in the 1930s.    Best known for playing orphans and street urchins, Fellows was an expressive actress with a good singing voice.    She made her screen debut at the age of five and had her first credited role in a feature film with The Rider of Death Valley (1932). By 1935, she had appeared in over twenty films. Her performance opposite Claudette Colbert and Melvyn Douglas in She Married Her Boss (1935) won her a seven-year contract with Columbia Pictures,  the first such contract offered to a child. 
 Pennies from Heaven (1936) won her critical acclaim. Her acting career was interrupted in the 1940s by serious personal problems. In the 1980s, she returned to acting with sporadic roles, mainly in television series. Between 1929 and 1995, she appeared in over seventy films and television shows.   

===Filming and budget===
Stardust on the Sage was filmed March 24 to April 8, 1941. The film had an operating budget of $86,378 (equal to $ }} today), and a negative cost of $87,830. 

===Filming locations===
* Agoura Ranch, Agoura, California, USA
* Los Angeles River Basin, Los Angeles, California, USA    

===Stuntwork===
* George DeNormand
* Mary Ann Jackson (Edith Fellows stunt double)
* Bert LeBaron
* Jack Montgomery
* Tex Terry (Smiley Burnettes stunt double)
* Nellie Walker (Louis Curries stunt double)
* Joe Yrigoyen (Gene Autrys stunt double)  

===Soundtrack===
* "Perfidia" (Milton Leeds, Alberto Domínguez) by Edith Fellows with George Ernest (guitar)
* "Good Night Sweetheart" (Ray Noble, Jimmy Campbell, Reginald Connelly) by Gene Autry
* "Youll Be Sorry" (Gene Autry, Fred Rose) by Gene Autry
* "Wouldnt You Like to Know?" (Smiley Burnette) by Smiley Burnette while performing magic tricks
* "When the Roses Bloom Again" (Nat Burton, Walter Kent) by Gene Autry and Edith Fellows at the party
* "Ill Never Let You Go, Little Darlin" (Jimmy Wakely) by Gene Autry and Edith Fellows on a radio broadcast
* "You Are My Sunshine" (Jimmie Davis, Charles Mitchell) by Gene Autry, Smiley Burnette, and others
* "Home on the Range" (Daniel E. Kelley, Brewster M. Higley) by Gene Autry, Smiley Burnette, and others
* "Deep in the Heart of Texas" (Don Swander, June Hershey) by Gene Autry, Smiley Burnette, and others    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 