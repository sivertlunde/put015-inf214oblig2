Shogun and Little Kitchen
 

 
 
{{Infobox film
| name           = Shogun and Little Kitchen
| image          = ShogunandLittleKitchen.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 伙頭福星
| simplified     = 伙头福星
| pinyin         = Huǒ Tóu Fú Xīng
| jyutping       = Fo2 Tau4 Feok1 Sing1 }}
| director       = Ronny Yu
| producer       = Laura Fau
| writer         = 
| screenplay     = Raymond To James Yuen
| story          = 
| based on       = 
| starring       = Yuen Biao Leon Lai Ng Man-tat Maggie Shiu Monica Chan
| narrator       = 
| music          = Richard Yuen
| cinematography = David Chung Andrew Lau
| editing        = David Wu
| studio         = Hoventin Films
| distributor    = Gala Film Distribution
| released       =  
| runtime        = 93 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$9,687,943
}}

Shogun and Little Kitchen is a 1992 Hong Kong comedy film directed by Ronny Yu and starring Yuen Biao, Leon Lai, Ng Man-tat, Maggie Shiu and Monica Chan.

==Plot== Jimmy Wang Yu) is very cold due to his hatred of his father. On the day of his birthday, Fung gets into an argument with his father and runs away from home,. Afterwards, Fung was robbed and beaten into a coma by a crock. Uncle Bo then brings the collapsed Fung to Tai Ping Fong for him to heal.

At Tai Ping Fong, Fung experiences love and care by others. There, Fung also develops a relationship with Bos daughter Maggie (Maggie Shiu). However, Maggie was angry after she discovers Fungs true identity. Furthermore, Bos uncle Tang Tai Chi (Yuen Biao), whom recently arrived to Hong Kong from Beijing, was poached by a catering company due to his amazing cooking and acrobatic skills. Bo feels betrayed by Chi and they fell out. Later, Bos building was also acquired by the Lam Enterprises. Because Bo was reluctant to sell the building, Lam Chung Yuens assistant Raymond (Lam Lap Sam) hires triad thugs to force Bo to sign the contract.

==Cast==
*Yuen Biao as Tang Tai Chi
*Leon Lai as Lam Fung
*Ng Man-tat as Uncle Bo
*Maggie Shiu as Maggie Ko Tin Oi
*Monica Chan as Jacqueline Kwok Jimmy Wang Yu as Lam Chung Yuen
*Bryan Leung as Mr. Tong Ching
*David Lui as Chow Wai Cheung
*Josephine Koo as Wong Yu Ying
*Lam Lap Sam as Raymond Chan
*Hau Woon Ling as Bos Neighbor
*Fong Yue as Shanghai Woman
*Tang Cheung as Wah
*Leung Gam San as Bill
*Jim James as Uncle Bos customer
*Hui Sze Man as Wedding guest
*Ernest Mauser as Uncle Bos customer
*Leung Sam as Bos tenant
*Cheung Siu as Bos tenant
*Leung Kei Hei as Uncle Bos customer
*Chan Man Hiu as Auntie Eight
*Kent Chow as Chong
*Jack Wong as Bills thug
*San Sin as Bing
*Hoh Wan as Bos tenant
*Lam Chi Tai

==Reception==
===Critical===
Love HK Film gave the film a positive review and writes "Standard formula and annoyingly cute subplots drag the film down, but Yuen Biaos athletic stuntwork makes the movie a fun, if not crucial viewing experience. The film doesnt challenge you in any way, but its really not meant to. Undemanding fun." 

===Box office===
The film grossed HK$9,687,943 at the Hong Kong box office during its theatrical run from 19 March to 13 April 1992 in Hong Kong.

==See also==
*Yuen Biao filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 

 
 
 
 
 
 
 
 
 
 