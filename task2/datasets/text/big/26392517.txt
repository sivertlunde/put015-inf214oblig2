Crazy Moon (film)
 
{{Infobox Film
| name = Crazy Moon
| caption =
| image	=	Crazy Moon FilmPoster.jpeg
| director = Allan Eastman
| producer = Franco Battista Tom Berry Stefan Wodoslawsky
| writer = Tom Berry Stefan Wodoslawsky
| starring = Kiefer Sutherland Vanessa Vaughan
| editing = Franco Battista
| country=  Canada   USA
| language= English
| released = December 1987
| runtime = 90 min
}}
Crazy Moon is a 1987 film written by Tom Berry and Stefan Wodoslawsky, directed by Allan Eastman, starring Kiefer Sutherland and Vanessa Vaughan.

==Plot==
A rich but slightly odd teenager has various adventures with his older brother leading him astray. One day while stealing a mannequin from a clothes store, he falls for the deaf female employee. The story follows their relationship as each of them learn from the others strengths and weaknesses.

==Cast==
*Kiefer Sutherland as Brooks 
*Vanessa Vaughan as Anne 
*Peter Spence as Cleveland 
*Ken Pogue as Alec 
*Eve Napier as Mimi  Sean McCann as Annes Father 
*Bronwen Mantel as Annes Mother 
*Terri Hawkes as Pamela

==External links==
* 

 
 


 