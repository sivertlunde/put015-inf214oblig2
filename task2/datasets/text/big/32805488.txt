The 13th Man
 

{{Infobox film
| name           = The 13th Man
| image_size     =
| image	=	The 13th Man FilmPoster.jpeg
| caption        =
| director       = William Nigh
| producer       = Trem Carr (executive producer) Lon Young (associate producer)
| writer         = John W. Krafft
| narrator       =
| starring       = See below
| music          = Abe Meyer
| cinematography = Paul Ivano
| editing        = Russell F. Schoengarth
| studio         = Monogram Pictures
| distributor    = Monogram Pictures
| released       = 1937
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The 13th Man is a 1937 American mystery film directed by William Nigh and starring Weldon Heyburn, Inez Courtney and Selmer Jackson. The film is also known as The Thirteenth Man. It was the first film released by the relaunched Monogram Pictures after the studio withdrew from a merger with Republic Pictures.

==Plot summary==
Swifty Taylor, a journalist with the Globe Times, hunts for the underworld figure responsible for the killing of a crusading district attorney, murdered by curare-laced dart while attending a prizefight, and of a reporter who had got too close to the truth. Meanwhile, Taylors secretary, Julie, hopes to persuade him to settle down and marry her.

==Cast==
*Weldon Heyburn as A. "Swifty" Taylor
*Inez Courtney as Julie Walters (Swiftys secretary)
*Selmer Jackson as Andrew Baldwin (Globe Times publisher)
*Matty Fain as Louis Cristy (nightclub owner)
*Milburn Stone as Jimmy Moran (Globe Times reporter)
*Grace Durkin as Alice Moran (Baldwins secretary)
*Robert Homans as Police Lt. Tom OHara
*Eadie Adams as Stella Leroy (nightclub singer)
*Sidney Payne as "Legs" Henderson (fighter)
*Dewey Robinson as Romeo Casanova (radio singer / gym operator) William Gould as Dist. Atty. Robert E. Sutherland
*Warner Richmond as George Crandall the Bookie
*Eddie Gribbon as Iron Man (Swiftys bodyguard)

==Soundtrack==
* Eadie Adams - "My Topic of Conversation" (Written by Joseph Myro and Milton Royce)

==External links==
* 
* 

 
 
 
 
 
 
 
 


 