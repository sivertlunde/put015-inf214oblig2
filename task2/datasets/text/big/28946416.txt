After Tomorrow
{{Infobox film
| name           = After Tomorrow
| image          = After-tomorrow-1932.jpg
| caption        = movie poster
| director       = Frank Borzage
| producer       = 
| writer         = Story:  
| narrator       = 
| starring       = Charles Farrell Marian Nixon
| music          = Hugo Friedhofer (uncredited) 
| cinematography = James Wong Howe
| editing        = Margaret Clancey Fox
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

After Tomorrow is a 1932 American film directed by Frank Borzage, and starring Charles Farrell, Marian Nixon, Minna Gombell and William Collier, Sr..

==Plot==
Peter Piper and his girlfriend Sidney Taylor have been engaged for a long time, but the economic situation of the Great Depression and the selfish demands of their respective mothers have delayed the marriage. They imagine their future together "after tomorrow" in the lyrics of their favorite song. Sidneys mother thinks only of her own needs, while clinging Mrs Piper cant bear the thought that her son will one day leave her, and does her best to break up Sidney and Peters relationship.

==Cast==
* Charles Farrell ...  Peter Piper
* Marian Nixon ...  Sidney Taylor
* Minna Gombell ...  Else Taylor
* William Collier, Sr. ...  Willie Taylor
* Josephine Hull ...  Mrs. Piper

==External links==
* 

 

 
 
 
 
 
 
 
 


 