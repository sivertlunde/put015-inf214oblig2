Les maîtres fous
{{Infobox Film
| name           = Les maîtres fous
| image          = Les_maitres_fous_title_still.jpg
| image_size     =
| caption        =
| director       = Jean Rouch
| producer       =
| writer         = 
| narrator       =
| starring       = 
| cinematography =
| music          =
| editing        =
| distributor    =
| released       = 1955
| runtime        = 36 minutes
| country        =
| language       =
| budget         =
| gross          =
}}
 French film director and ethnologist. It is a docufiction, his first ethnofiction, a genre he is considered to have created.

==Historical background==
 British Colonial administrators. The participants performed the same elaborate military ceremonies of their colonial occupiers, but in more of a trance than true recreation. 

The Hauka movement, according to some anthropologists was a form of resistance that began in Niger, but spread to other parts of Africa.  According to some anthropologists, this pageant, though historic, was largely done to mock their authority by stealing their powers.   Hauka members were not trying to emulate Europeans, but were trying to extract their life force – something "entirely African".

This stance has been heavily criticized by anthropologist James G. Ferguson who finds this imitation not about importing colonialism into indigenous culture, but more a way to gain rights and status in the colonial society.  The adoption of European customs was not so much a form of resistance, but to be "respected by the Europeans." Ferguson 

Les maîtres fous offended both colonial authorities and African students alike.  Indeed, the film was so controversial that it was banned first in Niger, and then in British territories including Ghana.   The film was considered offensive to colonial authorities because of the Africans blatant attempts to mimic and mock the "white oppressors".  On the other hand, African students, teachers, and directors found the film to perpetrate an "exotic racism" of the African people. 

==See also==

* Visual anthropology
* Ethnographic film
* Ethnofiction
* Docufiction

==References==
 
{{cite paper
  | author = James G. Ferguson
  | title = Of Mimicry and Membership: Africans and the "New World Society"
  | publisher = American Anthropological Society
  | year = 2002
  | format = Paper}}

== Related articles ==
*    – a web site devoted to the study of Jean Rouchs films
*   at Savage Minds (notes and queries in anthropology)
*   – article at Documentary (Educational Resources)
*   – article by Prerana Reddy
*   – article by Anna Grimshaw
*   – article by Natalie Mildbrodt
*   – article by Lorraine Mortimer

==External links==
*   (fr) at French Diplomacy
*  
*   (Society of Visual Anthropology)

 
 
 
 
 
 
 
 
 