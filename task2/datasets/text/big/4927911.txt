Volver
 
{{Infobox film
| name           = Volver
| image          = Volver Poster.jpg
| caption        = Theatrical release poster
| director       = Pedro Almodóvar 
| producer       = Esther García   (producer)  Agustín Almodóvar   (executive) 
| writer         = Pedro Almodóvar
| starring       = Penélope Cruz Carmen Maura Lola Dueñas Blanca Portillo Yohana Cobo Chus Lampreave 
| music          = Alberto Iglesias 
| cinematography = Jose Luis Alcaine
| editing        = José Salcedo
| distributor    = Sony Pictures Classics 
| released       =  
| runtime        = 121 minutes
| country        = Spain
| language       = Spanish
| budget         = €9.4 million
| gross          = $84,021,052
}}
Volver ( , meaning "to go back") is a 2006 Spanish drama film written and directed by Pedro Almodóvar. Headed by actress Penélope Cruz, the film features an ensemble cast also starring Carmen Maura, Lola Dueñas, Blanca Portillo, Yohana Cobo, and Chus Lampreave. Revolving around an eccentric family of women from a wind-swept region south of Madrid, Cruz plays Raimunda, a working-class woman forced to go to great lengths to protect her 14-year-old daughter Paula. To top off the family crisis, her mother Irene comes back from the dead to tie up loose ends.

The plot originates in Almodóvars earlier film The Flower of My Secret (1995), where it features as a novel which is rejected for publication but is stolen to form the screenplay of a film named The Freezer. Drawing inspiration from the Italian neorealism of the late 1940s to early 1950s and the work of pioneering directors such as Federico Fellini, Luchino Visconti, and Pier Paolo Pasolini, Volver addresses themes like sexual abuse, loneliness and death, mixing the genres of farce, tragedy, melodrama, and magic realism. Set in the La Mancha region, Almodovars place of birth, the filmmaker cited his upbringing as a major influence on many aspects of the plot and the characters.

Volver was one of the films competing for the  .    The films premiere was held on March 10, 2006, in Puertollano, Spain, where the filming had taken place. Penélope Cruz was nominated for the 2006 Academy Award for Best Actress, making her the first Spanish woman ever to be nominated in that category.

==Plot==
Raimunda (Penélope Cruz) and Soledad (Lola Dueñas) are sisters who grew up in Alcanfor de las Infantas, a small village in La Mancha, but now both live in Madrid. Their parents died in a tragic fire three years prior to the beginning of the film. The events which occurred on the night of the fire are only gradually revealed, and are central to the plot.

Sole returns to the village for the funeral of her elderly and dementia-stricken Aunt Paula (Chus Lampreave). Aunt Paulas neighbour Agustina (Blanca Portillo) confesses to Sole that she has heard Paula talking to the ghost of Soles mother Irene (Carmen Maura). Sole encounters the ghost herself, and when she returns to Madrid, she discovers that the ghost has stowed away in the trunk of her car. Irene has brought luggage, intending to stay with her daughter for a while, and Sole, though frightened, agrees to let her mother stay with her: Sole operates a hair salon in her apartment, and Irene will assist her, posing as a Russian woman to hide her true identity. Sole tries to determine why her mothers ghost has returned to Earth, asking her if she left anything undone in her life. Irene says that she does have issues to resolve, relating to the questions of why Raimunda hates her and why she is afraid to reveal herself to Raimunda.

Meanwhile Raimunda and her daughter Paula (Yohana Cobo) have a different death to cope with. Paulas father Paco (Antonio de la Torre) attempts to rape her, claiming that he is not really her father, and Paula stabs him in self-defense. Raimunda quickly hides the corpse in the deep-freezer of a nearby unused restaurant. The owner of the restaurant building, Emilio (Carlos Blanco), is out of town and entrusted Raimunda with the keys so that she can show it to prospective tenants. When members of a film crew happen upon the restaurant, Raimunda strikes a deal to cater for them, and suddenly finds herself back in the restaurant business.

Raimunda reveals to Paula that Paco was not, in fact, her biological father, and promises to tell her the whole story at a later time. Agustina is diagnosed with terminal  , 180 kilometres away. While staying in Soles apartment, Paula meets her grandmothers ghost and grows close to her. The next night, Agustina comes to the restaurant to renew her request to Raimunda to ask her mothers ghost about her own mothers whereabouts. She reveals two startling secrets: that Raimundas father and Agustinas mother were having an affair and that Agustinas mother disappeared on the same day that Raimundas parents died.
 sexually abused her, resulting in the birth of Paula; thus Paula is Raimundas daughter and also her sister. Raimunda had been angry with her mother for never noticing and ending this abuse. Irene tells Raimunda that she had never understood Raimundas anger and distance until her Aunt Paula told her about what her husband had done to her daughter, and Irene became furious with herself when she found out.

Irene explains that, due to her husbands abuse of Raimunda, she started the fire that killed him. The ashes that had been presumed to be Irenes were, in fact, the ashes of Agustinas mother, the woman with whom Irenes husband was having an affair. After the fire, Irene wandered for several days in the countryside, until she decided that she wanted to turn herself in. But first, she wanted to say goodbye to her sister Paula, who had lost the ability to look after herself and with whom Irene had been living prior to setting the fire that killed her husband. Paula, who was living in the past due to her senility, welcomed Irene home as if nothing had happened, and Irene stayed, caring for her sister and expecting that the police would come soon to arrest her. Due to the superstitious and closed nature of the community, however, the police never came and the residents, who are accustomed to tales of the dead returning, explained the rare sightings of Irene as "un fantasma", a ghost.

The film ends with the family reunited at Aunt Paulas house. Irene reveals her presence to Agustina, who believes her to be a ghost. Irene pledges to stay in the village and care for Agustina as her cancer worsens, saying to Raimunda that it was the least that she could do after killing Agustinas mother. In the last scene Raimunda visits her mother at Agustinas house. The two embrace and tell one another that they now have time to repair their relationship.

==Cast==
* Penélope Cruz as Raimunda, a mother living in Madrids suburbs
* Carmen Maura as Irene, the mother of Raimunda and Sole
* Yohana Cobo as Paula
* Blanca Portillo as Agustina
* Lola Dueñas as Soledad ("Sole")
* Chus Lampreave as Tía Paula Antonio de la Torre as Paco
* María Isabel Díaz as Regina
* Carlos Blanco as Emilio
* Neus Sanz as Inés

==Production==

===Origins=== Puerto Rican man who opts to kill his mother-in-law in hopes of reuniting with his beloved wife, who left him and broke off contact, at her mothers funeral. Owning a restaurant, he leaves it in his neighbours care, when he is about to kill his victim.  Fascinated by the story and its background, Almodóvar decided on incorporating elements of it into the screenplay of The Flower of My Secret, making it the plot of a movie-within-the-movie based on the main characters novel in the film.  While working on the script for Volver, he would however settle on outlining the role of the neighbour Raimunda, as the films central character, while Emilio, the Puerto Rican, eventually became a supporting role only.   

Almodóvar says of the story that "it is precisely about death...More than about death itself, the screenplay talks about the rich culture that surrounds death in the region of La Mancha, where I was born. It is about the way (not tragic at all) in which various female characters, of different generations, deal with this culture."   

===Casting=== Live Flesh Italian neorealism films from the 1950s, many of them starring Sophia Loren and Claudia Cardinale, to study "the Italian maggiorate" that Almodóvar envisioned for her performance in the film.  Cruz, who had to wear a prosthetic bottom while filming, noted the role of Raimunda as "the best gift an actress can get."   
 borderline character" of Irene as a "very complicated  ." 

==Reception==
 .]]

===Box office===
In the US alone, the film had made $12,897,993 (15.4% of total) at the box office after 26.4 weeks of release in 689 theatres. The box office figure from the rest of the world is somewhere in the region of $71,123,059 (84.6% of total) according to Box Office Mojo. The total worldwide gross is estimated at $84,021,052. 

As of January 22, 2007, the film had grossed $12,241,181 at the Spanish box office. 

===Critical reception===
The film received rave reviews when it was released in Spain. Fotogramas, the countrys top film magazine, gave it a five-star rating.  Upon its U.S. release, A. O. Scott made it an "NYT Critics Pick" and wrote: 
 To relate the details of the narrative&mdash;death, cancer, betrayal, parental abandonment, more death&mdash;would create an impression of dreariness and woe. But nothing could be further from the spirit of Volver which is buoyant without being flip, and consoling without ever becoming maudlin. Mr. Almodóvar acknowledges misfortune&mdash;and takes it seriously&mdash;from a perspective that is essentially comic. Very few filmmakers have managed to smile so convincingly in the face of misery and fatality: Jean Renoir and Billy Wilder come immediately to mind, and Mr. Almodóvar, if he is not yet their equal, surely belongs in their company. Volver is often dazzling in its artifice&mdash;José Luis Alcaines ripe cinematography, Alberto Iglesiass suave, heart-tugging score&mdash; but it is never false. It draws you in, invites you to linger and makes you eager to return. 

Roger Ebert gave it his top rating of   (four stars out of 4), calling it "enchanting, gentle, transgressive" and notes "Almodovar is above all a director who loves women&mdash;young, old, professional, amateur, mothers, daughters, granddaughters, dead, alive. Here his cheerful plot combines life after death with the concealment of murder, success in the restaurant business, the launching of daughters and with completely serendipitous solutions to (almost) everyones problems." 

As of 2012, the film has a "Certified Fresh" rating from critics at Rotten Tomatoes, scoring a 92% based on 147 "fresh" reviews out of 160 critics, with the general consensus being "Volver catches director Pedro Almodóvar and star Penélope Cruz at the peak of their respective powers, in service of a layered, thought-provoking film. This magical tragicomic melodrama may be Almodovars most restrained work to date, but it still features his trademarks: a strong attention to color and detail, a celebration of the trials and tribulations of women, and, of course, the inestimable Carmen Maura. The lovely Penélope Cruz hasnt shone more brightly as she does here." 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2006. 

*2nd - Marjorie Baumgarten, The Austin Chronicle
*3rd - Glenn Kenny, Premiere (magazine)|Premiere
*3rd - Kevin Crust, Los Angeles Times
*3rd - Richard Corliss, Time (magazine)|Time magazine
*3rd - Philip Martin, Arkansas Democrat-Gazette Salon
*4th - Peter Travers, Rolling Stone
*4th - Ray Bennett, The Hollywood Reporter
*5th - Desson Thomson, The Washington Post
*6th - Claudia Puig, USA Today
*6th - Scott Tobias, The A.V. Club
*7th - Kenneth Turan, Los Angeles Times
*8th - A. O. Scott, The New York Times
*8th - Keith Phipps, The A.V. Club
*8th - Kirk Honeycutt, The Hollywood Reporter
*8th - Stephen Holden, The New York Times
*9th - Shawn Levy, The Oregonian
*10th - David Ansen, Newsweek
*10th - Lou Lumenick, New York Post
General top ten
*Carina Chocano, Los Angeles Times
*Carrie Rickey, The Philadelphia Inquirer
*Joe Morgenstern, The Wall Street Journal
*Liam Lacey and Rick Groen, The Globe and Mail

===Awards and nominations===
:(Awards won are in bold) Best Screenplay Best Actress &mdash; which was shared by the six stars of the film. 

Cruz was nominated for   for Penélope Cruz as well as Best Foreign Language Film. 

*19th European Film Awards (4/6):
**Best Actress (Penélope Cruz)
**Best Cinematographer (José Luis Alcaine)
**Best Composer (Alberto Iglesias)
**Best Director (Pedro Almodóvar)
**Best Film
**Best Screenwriter (Pedro Almodóvar) 59th Cannes Film Festival (2/2): 
**Best Actress (Penélope Cruz, Carmen Maura, Lola Dueñas, Blanca Portillo, Yohana Cobo and Chus Lampreave)
**Best Screenplay (Pedro Almodóvar)
*60th British Academy Film Awards (0/2):
**Best Actress (Penélope Cruz)
**Best Foreign Language Film
*12th Empire Awards (1/1):
**Best Actress (Penélope Cruz)
*79th Academy Awards (0/1):
**Best Actress (Penélope Cruz)
*12th Critics Choice Awards (0/2):
**Best Actress (Penélope Cruz)
**Best Foreign Language Film 19th Chicago Film Critics Association Awards (0/2):
**Best Actress (Penélope Cruz)
**Best Foreign Language Film
*32nd César Awards (0/1):
**Best Foreign Film 78th National Board of Review Awards (1/1):
**Best Foreign Language Film
*11th Satellite Awards (1/4):
**Best Foreign Language Film
**Best Actress - Drama (Penélope Cruz)
**Best Director (Pedro Almodóvar)
**Best Screenplay - Original (Pedro Almodóvar)
*21st Goya Awards (5/14):
**Best Actress (Penélope Cruz)
**Best Director (Pedro Almodóvar)
**Best Film
**Best Original Score (Alberto Iglesias)
**Best Supporting Actress (Carmen Maura)
**Best Cinematography (José Luis Alcaine)
**Best Costume Design (Sabine Daigeler)
**Best Make-Up and Hairstyles (Massimo Gattabrusi and Ana Lozano)
**Best Production Design (Salvador Parra)
**Best Production Supervision (Toni Novella)
**Best Screenplay - Original (Pedro Almodóvar)
**Best Sound
**Best Supporting Actress (Lola Dueñas)
**Best Supporting Actress (Blanca Portillo) 7th Vancouver Film Critics Circle Awards (1/1):
**Best Foreign Language Film
*64th Golden Globe Awards (0/2):
**Best Actress - Drama (Penélope Cruz)
**Best Foreign Language Film
*13th Screen Actors Guild Awards (0/1):
**Best Actress (Penélope Cruz)

==Music== Saint Etienne.

==References==
 

==External links==
*   and   from a 4 August 2006 interview about Volver with Pedro Almodóvar and Penélope Cruz at the National Film Theatre
*  
*  
*  
*  
*  }}
*   from moviegrande.com

 
 
{{succession box Cannes Film Festival Prix du scénario| years=2006
| before=The Three Burials of Melquiades Estrada The Edge of Heaven}}
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 