Lost (1983 film)
{{Infobox film
| name           = Lost
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Al Adamson
| producer       = Andy and Beverly Innes Corey Allen
| writer         = Don Buday
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = Gary Graver
| editing        = 
| studio         = American National Enterprises
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Lost is a 1983 Low-budget film|low-budget movie directed by Al Adamson.  It stars Sandra Dee, Gary Kent and Jack Elam. This was Sandra Dees last film role. 

==Plot== Don Stewart), put to sleep she runs off, getting lost in the mountains.

An initial search party is unable to locate the missing girl. Buddy is chased by a cougar, jumps into a river injuring herself, and is eventually found by Mr. Newsome (Jack Elam), a reclusive mountain man. After initial distrust, Buddy lets the man care for her injury and listens to his stories through the evening. The next day Jeff starts looking for Buddy using a rented airplane, eventually finding her. Happy for her rescue, they return home together. 

==Cast==
 
*Sandra Dee as Penny Morrison
*Gary Kent as Stokes (also Stunt coordinator)
*Jack Elam as Mr. Newsome
*Ken Curtis as Wyatt Cosgrove, the Morrisons neighbor
*Sheila Newhouse as Buddy Morrison Don Stewart as Jeff Morrison

==Production==
Lost was filmed on-location in Utah and Colorado.

==References==
 

==External links==
* 
* 

 


 