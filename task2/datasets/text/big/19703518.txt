Perkins' 14
 
{{Infobox film
| name           = Perkins 14
| image          = Perkins fourteen.jpg
| caption        = Promotional film poster
| director       = Craig Singer
| producer       = {{Plainlist|
* Jeremy Donaldson
* Matthew Kuipers
* Christopher Milburn}}
| screenplay     = Lane Shadgett (screenplay)
| starring       = {{Plainlist|
* Patrick OKane
* Shayla Beesley
* Mihaela Mihut
* Michale Graves}}
| music          = 
| cinematography = Alexandru Durac
| editing        = 
| distributor    = After Dark Horrorfest
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Perkins 14  is a 2009 horror film originated by Jeremy Donaldson, written by Lane Shadgett, and directed by Craig Singer. August 28, 2008,  , Retrieved 10-09-2008  The film is produced by Jeremy Donaldson, Matthew Kuipers, and Christopher Milburn.  by Joseph B. Mauceri, August 19, 2008,  , "PERKINS 14 in Post Productionin in NYC", Retrieved 10-09-2008  By Tatiana Siegel, May 7, 2008,  , "After Dark to produce scare pair: Faithless, Perkins 14 to screen at Horrorfest", Retrieved 10-09-2008  The film was released theatrically nationwide January 9, 2009. The DVD which includes the 10 making-of webisodes from the Massify Ghosts in the Machine Competition was released on March 31, 2009.

== Plot ==
 
Ten years after officer Dwayne Hopper’s son disappeared, the last of 14 victims in a string of local unsolved disappearances, his suspicions are aroused by prison inmate Ronald Perkins (Richard Brake). Dwayne searches Perkins’ house and discovers a collection of torture videos featuring the missing victims from the past. In a fit of rage, Dwayne kills Perkins. But things get complicated when a wave of carnage sweeps the town, with reports of Dwayne’s own son amongst the marauding psychopaths.

== Cast ==
* Patrick OKane as Dwayne Hopper 
* Shayla Beesley as Daisy Hopper 
* Mihaela Mihut as Janine Hopper
* Michale Graves as Eric Ross  
* Gregory OConnor as  Grodsky  
* Katherine Pawlak as Felicity
* Richard Brake as Ronald Perkins 
* Craig Robert Young as Mitch James
* Trey Farley as Scott 
* CJ Singer as Kyle Hopper
* Sean Farragher as Pete
* Josh Davidson as The Stranger 

== Production ==
The film was developed online at Massify.com. Writers uploaded story concepts and actors submitted audition videos.  The winning idea was submitted by Jeremy Donaldson.    Filmed on locations in Romania,  and produced by After Dark Films, and Massify in association with FanLib.

==Release==
Perkins 14 premiered at the After Dark Horrorfest on January 9, 2009. It would later have its DVD premiere on March 31 the same year. 

==Reception==

 

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 