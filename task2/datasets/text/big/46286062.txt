Hey, Let's Twist!
{{Infobox film
| name           = Hey, Lets Twist!
| image          = Hey, Lets Twist! poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Greg Garrison
| producer       = Martha Vera Romm 
| screenplay     = Hal Hackady
| starring       = Joey Dee Jo Ann Campbell Teddy Randazzo Kay Armen Zohra Lampert Dino Di Luca
| music          = Henry Glover
| cinematography = George Jacobson 	
| editing        = Arline Garson Sidney Katz
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Hey, Lets Twist! is a 1961 American musical film directed by Greg Garrison and written by Hal Hackady. The film stars Joey Dee, Jo Ann Campbell, Teddy Randazzo, Kay Armen, Zohra Lampert and Dino Di Luca. The film was released on December 31, 1961, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Joey Dee as himself
*Jo Ann Campbell as Piper
*Teddy Randazzo as Rickey Dee
*Kay Armen as Angie
*Zohra Lampert as Sharon
*Dino Di Luca as Papa
*Hope Hampton as herself
*Richard Dickens as Rore 
*The Peppermint Loungers as themselves

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 