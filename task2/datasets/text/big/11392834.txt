Patita (1953 film)
 
{{Infobox film
| name           =Patita
| image          = Patita1953.jpg
| image_size     =
| caption        = 
| director       =Amiya Chakrabarty
| producer       =Amiya Chakrabarty
| writer         = 
| narrator       =  Agha  Lalita Pawar
| music          =Shankar Jaikishan
| cinematography = 
| editing        = 
| distributor    = 
| released       = January 1, 1953 
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Patita (A fallen woman) is a 1953 Hindi film produced and directed by  , "Hai Sab Se Madhur Geet" by Talat Mahmood, "Kisi Ne Apna Bana Ke" by Lata Mangeshkar,  "Mitti se Khelte Ho" by Lata Mangeshkar and the duet "Yaad Kiya Dil Ne Kahaan Ho Tum" by Hemant Kumar and Lata Mangeshkar.

==Plot==
Radha (Usha Kiran) is a 19 year old poor girl living in a one-room shanty with her handicapped and bed-ridden father Sadhu Ram (Krishnakant). She is reduced to begging for providing food for the two of them and is hounded almost daily by Bhiku Chacha (C.S.Dubey), the right hand man of their shantys owner Manohar (Shivraj), who is a rich and unscrupulous businessman as they owe almost one hundred and fifty rupees in rent for over a year to him.One day whilst begging she meets the young and charming Nirmal Chander (Dev Anand) who chastises her for begging and hands her a visiting card of his friend who he informs her is a kind man and will provide her with work.In fact, Nirmal himself is the owner of a textile mill and the contact he provides Radha with is that of his factory manager. She goes to the said person and is hired as a daily labourer in Nirmals mill. Nirmal visits the shanty of Sadhu Ram, where both father and daughter are overjoyed with happiness and gratitude at his kindness.Eventually, Nirmal falls in love with the simple,pretty Radha and proposes marriage to her. She readily accepts and her happiness knows no bounds,but in a cruel twist of fate,she comes home to find Bhiku Chacha and his men aided with orders from Manohar, about to throw them and their few belongings from the shanty for not being able to repay the entire rent in time. She goes to Manohar to beg for some more time and mercy but he overcome with lust at the sight of the helpless young girl, traps her in his room and forcibly rapes her, and then lets her know that he forgives all the amount they owe him in return. 
She is heartbroken and ashamed and comes home a mental wreck unable to confide in either her father or Nirmal. She stops going to work, starts distancing herself from her suitor and roams around alone till the point when she becomes sick and faints and a neighbourhood woman checks her to reveal to her father that she is quite a few months pregnant. Her father is shocked and passes away in grief to hear this. Radha,being the traditional Bharatiya Naari, goes to commit suicide from a cliff because clearly abortion is immoral and confiding in her rich.modern boyfriend doesnt even come to her traditional head filled with shame and guilt.
However, here she meets the happy go lucky chap Mast Ram (Agha (actor)|Agha) who talks her into giving up the idea of suicide and takes her to live with him in his house,which he calls the Mast Mahal. It is a self-created ancient piece of architectural wonder that is supported by the bare trunk of a tree at a precarious angle and has stumps of felled trees in place of chairs and tables.Mast Ram himself is an orphan and a product of sex outside marriage, hence much chastised by society is the diametrically opposite of Radha.He seems genuinely happy to be alive, have four working limbs and is a man easily pleased with two square meals a day, or sometimes even not, and always has a song or two on his lips. He takes to Radha like an elder brother and it is in his house, her son Chandu is born. Mast Ram advises Radha to give up her infant son to an orphanage run by a Swami Ji for the betterment of the both of them but Radha vehemently refuses, and decides to work and struggle to bring up her son, as he is all she has left now. She goes back to her old place of work and this time ends up finding out the owner of the mill to be Nirmal. He is angry and rude to her for running out on him earlier without any explanation and insults her. But afterwards, he has a change of heart and follows her to Mast Mahal where he finds out about the infant. Radha tells him everything honestly and angry and embarrassed at himself, he goes up to Manohar and beats him up in his frustration. Afterwards, he offers marriage to Radha once again as he truly believes that whatever happened was not due to any fault of hers and that she has suffered enough. Nirmal is a modern, educated and charming man and Radha accepts his proposal but has apprehensions about how his mother will take to her, considering she has such a scandalous event in her past.
Nirmal tells her that in time his mother will accept the situation and love her as much as he does, and it is in the best of all their interest to hide the presence of her son from his mother now. He arranges for her son and Mast Ram to live in a mansion owned by his father and now vacant alongside an ayah for the infant. Life is good for sometime for the young couple but Radha keeps on bringing up the topic of letting Mother (Lalita Pawar) know the truth and Nirmal keeps on delaying it for the right time. Then one day,a disrupt breaks out between the mill workers in a factory located in another city, Nirmal is forced to leave town to manage the situation there, and Manohar who has been harbouring vengeance for being thrashed up by Nirmal, tips off his mother about her daughter-in-law spending way too much time in their old mansion. She follows Radha one day, arrives on her breast feeding the child, and Radha in fear of being caught red handed tells her that the child is not Nirmals. She thinks that Radha has duped her son and therefore ruined the social standing and prestige of her family and throws her out of her house.Radha writes a letter to Nirmal for him to return quickly, and in his harrowed sense of mind,he rash drives and crashes his car to turn over a bridge into the sea. 
With the news of Nirmals death,Radha is beyond grief and goes to his mother, who still refuses to accept her.Also seeking his chance now, Manohar offers Radha the proposition of becoming his kept with her child, and tries to force himself upon her. Finally, the weepy,poor,helpless Radha has had enough and taking down a rifle from the mansion, shoots Manohar dead at point-blank. She is arrested by the Police and a trial starts against her. At this point, Nirmals spirit tells his mother that he knew all along about Radhas innocence and plight and married her nevertheless and that she never wanted to hide anything from his mother in the first place. His mother, overcome with grief and sorrow, goes over to the court to testify on behalf of Radha and delivers a passionate speech. Radha is acquitted, all charges against her are dropped, and she is accepted by Nirmals Mother along with her child to come and live with her. At this point, the story takes a dramatic twist.Nirmal comes over to his place and tells the stunned Radha,Mast Ram and his mother that he had never died although his car was damaged and he had planned to behave like a spirit by hiding under beds and other places to deliver his "spirit oracles" so that his mother would realise the pain of losing a loved one and accept Radha into her household. Everyone is happy now,and the family is reunited to live happily ever after.

==Cast==
*Dev Anand ...  Nirmal Chander
*Usha Kiran ...  Radha (as Usha Kiron) Agha ...  Mastram
*Lalita Pawar ... Nirmals Mother
*Shivraj ...  Manohar (Landlord)
*Krishnakant ...  Sadhu Ram (Radhas Father)
*C.S. Dubey ...  Bhiku Chacha (as Chandra Shekhar)
*Sonny Abraham  (as Sonny)
*Tiwari ...  Orphanage man
*Baby Chand  (as Baby Chandu)

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Hai Sab Se Madhur Woh Geet"
| Talat Mahmood 
|-
| 2
| "Yaad Kiya Dil Ne"
| Hemant Kumar, Lata Mangeshkar
|-
| 3
| "Andhe Jahan Ke Andhe Raste"
| Talat Mahmood
|-
| 4
| "Kisine Apana Bana Ke Mujhko"
| Lata Mangeshkar
|-
| 5
| "Mitti Se Khelte Ho Bar-Bar Kisliye"
| Lata Mangeshkar
|-
| 6
| "Tujhe Apne Paas Bulati Hai"
| Talat Mahmood
|}
==References==
 

== External links ==
*  

 
 
 
 
 