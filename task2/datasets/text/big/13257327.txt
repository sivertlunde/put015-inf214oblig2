Love Crazy
 
{{Infobox film
| name           = Love Crazy
| image          = Love crazy poster.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster Jack Conway
| producer       = Pandro S. Berman David Hertz William Ludwig Charles Lederer
| starring       = William Powell Myrna Loy Gail Patrick Jack Carson David Snell
| cinematography = William H. Daniels
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = $889,000  . 
| gross          =  $2,060,000 
}}
Love Crazy is a 1941 screwball comedy film pairing William Powell and Myrna Loy as a couple whose marriage is on the verge of being broken up by the husbands old girlfriend and the wifes disapproving mother.

==Plot==
Architect Steve Ireland (William Powell) and his wife Susan (Myrna Loy) eagerly look forward to their fourth wedding anniversary, but her mother Mrs. Cooper (Florence Bates) shows up and puts a damper on their plans for the evening. She sends Steve downstairs to mail her insurance premium. 

He runs into his old girlfriend Isobel Kimble Grayson (Gail Patrick) and learns that she has just moved into the apartment building, just one floor below. On the way up, the elevator gets stuck. While they are getting out, Steve is struck several times in the head and becomes woozy. Isobel takes him to her apartment to recover. Though she is now also married, she makes it clear that she would not mind renewing their relationship, but Steve is hopelessly in love with his wife.

When he returns to his apartment, he neglects to mention his encounter with Isobel, but Mrs. Cooper finds out and tells her daughter, putting Steve in an awkward spot. For revenge, Susan calls Isobels husband Pinky (Donald MacBride) and suggests that they pretend that they are seeing each other. He agrees, but Susan goes to the wrong apartment, that of world champion archer Ward Willoughby (Jack Carson). He is puzzled, but has no objection to being romanced by a beautiful woman. When Susan learns her mistake, she has difficulty extricating herself from Willoughbys apartment. They are seen by Steve and Isobel, resulting in much confusion. Things are finally cleared up, but then Susan is led to believe that Steve was alone with Isobel in her apartment while she was out running an errand for her mother. 

Susan decides to get a divorce, despite Steves pleas. She hides in Arizona with her meddling mother. Willoughby follows, to better his acquaintance with Susan. The night before the divorce hearing, Steves lawyer, George Renny (Sidney Blackmer), spots Susan at a party and tells his client. Steve crashes the gathering, but is unable to change Susans mind. A chance remark by Steve gives Renny an idea - a divorce can be delayed if one of the parties is insane. Steve does his best to act nutty, even pushing his mother-in-law into the pool. However, he had been so eccentric in the past, that everyone (with the exception of one older man) just believes he is drunk. 

Nonetheless, Renny gets the divorce judge to agree to a thirty-day delay to have Steve examined by the city lunacy commission. When he realizes that he has gone too far, Steve tries to convince the members that he is sane, but the head of the board, Dr. Klugle (Vladimir Sokoloff), turns out to be the only person Steve hoodwinked at the party. As a result, he is committed to a sanitarium.

Steve escapes by tricking the head of the rest home, Dr. Wuthering (Sig Ruman). He returns to his apartment building one step ahead of the police, who now consider him a homicidal maniac. Steve dodges Willoughby and hides with Isobels help. He then disguises himself as his "sister" by putting on some of Isobels clothes and shaving his mustache. He finally reaches Susan, only to have Mrs. Cooper and Willoughby show up soon afterwards. When Mrs. Cooper inadvertently confirms Steve just talked to Isobel at a cafe, Susan finally believes her husband.

==Trivia==
The character, Dr. Klugle, is referred to as looking like, "General Electric Whiskers." This may be a reference to Annibale Bergonzoli.

==Cast==
* William Powell as Steve Ireland
* Myrna Loy as Susan Ireland
* Gail Patrick as Isobel Kimble Grayson
* Jack Carson as Ward Willoughby
* Florence Bates as Mrs. Cooper
* Sidney Blackmer as George Renny
* Sig Ruman as Dr. Wuthering, the head of the sanitarium (as Sig Rumann)
* Vladimir Sokoloff as Dr. David Klugle
* Donald MacBride as Pinky Grayson
* Sara Haden as Cecilia Landis
* Kathleen Lockhart as Mrs. Bristol
* Fern Emmett as Martha, the Irelands maid
* Elisha Cook, Jr. as Elevator Operator
==Box Office==
According to MGM records the film earned $1,335,000 in the US and Canada and $725,000 elsewhere resulting in a profit of $514,000. 
==References==
 
==External links==
*  
*  
*  

 
 


 
 
 
 
 
 
 
 

 