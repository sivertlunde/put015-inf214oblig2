Ratty (film)
{{Infobox Film
| name = Råttis
| image = Ratty poster.jpg
| caption =
| producer =
| writer = Lennart Gustafsson
| starring = 
| director = Lennart Gustafsson
| distributor =
| released = 19 December 1986
| runtime = 84 min. Swedish
| music = 
| editing = 
| budget = 
| box office = 
| preceded_by = 
| followed_by = 
}} Swedish animated feature film directed by Lennart Gustafsson about rats experiencing young love. The film features several song numbers and is on the border to what could be called a musical film|musical.

It was the fifth Swedish animated feature film to ever be released, not counting Per Åhlins early films that were only partly animated.

==Plot== club "Ratz."

==Reception==
The film met mixed reviews in the Swedish press. It was praised for the amount of details and its distinguished style, but criticized for lacking in dramatic pulse and energy. " ." (in Swedish) Swedish Film Institute 

==References==
 

== External links ==
* 

 
 
 
 
 