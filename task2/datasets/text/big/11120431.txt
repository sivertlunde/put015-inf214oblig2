Action in the North Atlantic
{{Infobox film
| name           = Action in the North Atlantic
  | image          = Action in the North Atlantic - 1943 - poster.png
  | image_size     = 
  | caption        = 1943 Movie Poster
| director       = Lloyd Bacon Byron Haskin Raoul Walsh
| producer       = Jerry Wald   
| writer         = John Howard Lawson Guy Gilpatric (story)
| narrator       = 
| starring       = Humphrey Bogart Raymond Massey
| music          = Adolph Deutsch George Lipschultz (uncredited)
| cinematography = Ted D. McCord
| editing        = George Amy
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $2.6 million Thomas Schatz, Boom and Bust: American Cinema in the 1940s Uni of California Press, 1999 p 218 
}}
Action in the North Atlantic is a 1943 American war film directed by Lloyd Bacon, featuring Humphrey Bogart and Raymond Massey as sailors in the U.S. Merchant Marine in World War II. 

==Plot==
The oil tanker mastered by Capt. Steve Jarvis (Raymond Massey) is sunk in the north Atlantic Ocean by a German U-boat. He and the first officer, his friend Joe Rossi (Humphrey Bogart), make it to a lifeboat along with other crewmen. When the U-boat crew starts filming their plight they respond with rude gestures and are rammed. The men swim to a raft and are rescued after 11 days adrift. 
 convoy carrying vital war supplies to the Soviet port of Murmansk. 
 seaplanes find the freighter and attack. Both are shot down, but the second crashes into the bow. Steve is shot in the leg during the battle; Joe has to take command. The U-boat sights the ship again and hits her with a torpedo. Joe orders the men to set fires and make smoke so that it appears as if the ship is sinking. When the submarine surfaces to finish her off, Joe rams and sinks it. The Seawitch then limps into Murmansk to a warm Russian welcome.

==Cast==
*Humphrey Bogart as First Officer Joe Rossi
*Raymond Massey as Captain Steve Jarvis
*Alan Hale, Sr. as Alfred "Boats" OHara Julie Bishop as Pearl ONeill
*Ruth Gordon as Sarah Jarvis
*Sam Levene as "Chips" Abrams
*Dane Clark as Johnnie Pulaski
*Peter Whitney as "Whitey" Lara
*Dick Hogan as Cadet Ezra Parker
In addition, Robert Mitchum appeared in a one-line role.

==Production== Bill Collins Presents the Golden Years of Hollywood, the ships sets were built in halves on two sound stages, with the tanker sinking sequence shot first.  
  
Director Lloyd Bacons contract with Warner Brothers expired during production. Jack L. Warner wanted to wait until the film was finished before entering discussions about a new contract, but Bacon wasnt willing to continue without one. Warner fired him and brought in Byron Haskin to complete filming, which ran 45 days over schedule. 

According to a news item in the Hollywood Reporter on June 24, 1943, copies of Action in the North Atlantic were provided to the Merchant Marine schools for use in training when the War Shipping Administration judged that technical and educational material in the film  would "aid considerably the training program." The studio donated three prints for official use at the U.S. Merchant Marine Academy in Kings Point, New York, and at cadet basic schools in San Mateo, California, and Pass Christian, Mississippi. 

This film has a famous back-story; watching their stunt men performing a dive off a burning ship, Bogie and Massey, both a bit intoxicated (being off-duty), started making bets on which stunt man was braver...one thing led to another, until the stars, themselves, made the dive.

Authentic models of German and Soviet airplanes were used in the film, and all dialogue involving non-Americans was in the native tongue of the speaker, both rarities in movies of this era.

==Awards==
The film received an Academy Award nomination for Best Writing (Best Original Story).   

==References==
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 