The Night of Love
{{Infobox film
| name           = The Night of Love
| image          = The-night-of-love-1927.jpg
| caption        = Film poster
| director       = George Fitzmaurice
| producer       = Samuel Goldwyn
| writer         = Lenore J. Coffee (screenplay) Edwin Justus Mayer (intertitles)
| narrator       =
| starring       = Ronald Colman Vilma Bánky George Barnes Thomas E. Brannigan
| editing        = Grant Whytock
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       =  
| runtime        = 89 minutes
| country        = United States
| language       = English
| budget         =
}}

The Night of Love (1927 in film|1927) is a drama film, produced by Samuel Goldwyn, released by United Artists, and starring Ronald Colman, Vilma Bánky, and Montagu Love. The screenplay by Lenore J. Coffee is based on the play by Pedro Calderón de la Barca.

==Cast==
* Ronald Colman ... Montero
* Vilma Bánky ... Princess Marie
* Montagu Love ... Duke de la Garda
* Natalie Kingston ... Donna Beatriz John George ... Jester
* Bynunsky Hyman ... Bandit
* Gibson Gowland ... Bandit
* Eugenie Besserer ... Gypsy (uncredited)

==Censorship==
The film was banned in 1927 by Eric W. Hamilton, a Hong Kong censor, who stated that "The subject matter is absolutely immoral and the depiction in many cases is indecent."  

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 


 