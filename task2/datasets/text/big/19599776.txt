The Bachelor (1955 film)
{{Infobox film
| name           = The Bachelor
| image          = The Bachelor (1955 film).jpg
| image size     =
| caption        =
| director       = Antonio Pietrangeli
| producer       =
| writer         =
| narrator       =
| starring       = Alberto Sordi Nino Manfredi Fernando Fernán Gómez Virna Lisi (uncredited)
| music          =Angelo Francesco Lavagnino
| cinematography =Gianni Di Venanzo
| editing        = Eraldo Da Roma
| distributor    =
| released       = 1955
| runtime        = 89 min
| country        = Italy Spain
| language       = Italian
| budget         =
| preceded by    =
| followed by    =
}} 1955 film directed by Antonio Pietrangeli, starring Alberto Sordi and Nino Manfredi. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Plot==
The accountant Paolo Anselmi is a confirmed bachelor who finds himself having to do the best man for a friends business partner. Paul left the old apartment to his friend and his wife moved to a small guesthouse where he met a woman, a young hostess. The relationship between the two begins to get serious, but he does not want commitment, then the girl gets moved to another city. Paul regain his freedom, but loneliness grips him. The company of the other bachelors bored, if the night feels bad and is only the mother continues to insist on getting married. The bachelor is convinced and begins to seek a wife. Among the "candidates" have many defects insurmountable, jealousy, family, bulky, but finally manages to convince him is Miss Carla who becomes his companion despite their initial meetings have always ended in fights furious.

==Cast==
* Alberto Sordi - Paolo Anselmi
* Sandra Milo - Gabriella, hostess
* Nino Manfredi - Peppino
* Madeleine Fischer - Carla Alberini
* Anna Maria Pancani - Lisa
* María Asquerino - Catina
* Fernando Fernán Gómez - Armando
* Pina Bottin - Anna, laundrys director
* Attilio Martella - Michele
* Giovanni Cimara - Mario
* Franca Mazzoni - Norma
* Alberto De Amicis - Antonio
* Francesco Mulé - Cosimo
* Antonella De Luca - Vanda
* Lilli Panicalli - Claudia

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 