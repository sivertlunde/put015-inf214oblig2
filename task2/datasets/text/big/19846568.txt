Piccadilly Zero Hour 12
{{Infobox film
| name           = Piccadilly Zero Hour 12
| image          = 
| caption        = 
| director       = Rudolf Zehetgruber
| producer       = Eberhard Meichsner
| writer         = Francis Durbridge Rudolf Zehetgruber
| starring       = Helmut Wildt
| music          = 
| cinematography = Hans Jura
| editing        = Liselotte Cochius
| distributor    = 
| released       = 31 December 1963
| runtime        = 
| country        = West Germany 
| language       = German
| budget         = 
}}

Piccadilly Zero Hour 12 ( ) is a 1963 West German crime film directed by Rudolf Zehetgruber and starring Helmut Wildt.

==Cast==
* Helmut Wildt - Mike Hilton
* Hanns Lothar - Jack Bellamy
* Ann Smyrner - Ruth Morgan
* Klaus Kinski - Whitey
* Karl Lieffen - Lee Costello
* Pinkas Braun - Sir Cunningham
* Ilja Richter - Der junge Edgar Wallace
* Marlene Warrlich - Della
* Camilla Spira - Pamela
* Rudolf Fernau - Craddock
* Stanislav Ledinek - Sammy
* Albert Bessler
* Dieter Eppler - Slatterly
* Herbert Grünbaum
* Toni Herbert
* Erik Radolf
* Conny Rux - Big John
* Arthur Schilsky
* Kurt Zips - Donovan

==External links==
* 

 
 
 
 
 
 


 
 