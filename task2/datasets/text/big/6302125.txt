Viy (1967 film)
 
{{Infobox film
| name           = Viy
| image          = Viy dvd.jpg
| caption        = DVD cover
| director       = Konstantin Yershov Georgi Kropachyov
| producer       = 
| writer         = Screen writers:  
| starring       = Leonid Kuravlev Natalya Varley Alexei Glazyrin Vadim Zakharchenko Nikolai Kutuzov
| music          = Karen Khachaturian
| studio = Artistic Association "LUCH"
| distributor = Mosfilm
| cinematography = Viktor Pishchalnikov Fyodor Provorov
| editing        = R. Pesetskaya Tamara Zubova
| released       = 1967
| runtime        = 78 minutes
| country        = Soviet Union
| language       = Russian
| budget         = 
}}
 story of the same name.

==Plot==
 
A seminary student must survive three nights in prayer guarding the deceased witch maiden while she, along with an army of hellish demons, try to lure him out of his Holy Ring of Chalk.

The movie follows the original tale in a somewhat loose fashion, but manages to retain the majority of the images and action.

==Cast==
* Leonid Kuravlev as Khoma Brutus
* Natalya Varley as Pannochka
* Alexei Glazyrin
* Vadim Zakharchenko
* Nikolai Kutuzov
* Pyotr Vesklyarov
* Dmitri Kapka
* Stepan Shkurat
* G. Sochevko
* Nikolai Yakovchenko
* Nikolai Panasyev

==Production==
 
 
Some of the witch scenes and the ending where Viy appears were toned down due to technological limitations as well as then current restrictions on Soviet film production. The directors were able to avoid the previous restrictions by using what was considered a folk tale.

==Release==
 
 
 
This was officially the first Soviet-era horror film released in the USSR.

==Remake==
 

A modern version starring Jason Flemyng has been in production for several years and has gone through several different deadlines, but is planned for release in 2012.  The 1990 Serbian version of the film, called "A Holy Place" has run on the Fantasia Festival 2010. 

==References==
{{reflist|refs=

   

}}

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 