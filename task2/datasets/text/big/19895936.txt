In the King of Prussia
{{Infobox film
| name           = In the King of Prussia
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Emile de Antonio
| producer       = Emile de Antonio
| writer         = Emile de Antonio
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Jackson Browne, Graham Nash
| cinematography = 
| editing        = Mark Pines
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = USA
| language       = English
| budget         = $275,000
| gross          = 
}}

In the King of Prussia is a 1983 film directed and written by Emile de Antonio.  The film reconstructs the events of the 1980s "Plowshares Eight." The group of anti-war activists were charged with the September 1980 destruction of nose cones designed for nuclear warheads at the Re-Entry Division of the General Electric Space Technology Center in King of Prussia, Pennsylvania.    The members of the Plowshares Eight, including Daniel Berrigan and Philip Berrigan, played themselves while actors played the roles of jurors, lawyers and police; Martin Sheen played the role of the judge in this shot-on-video feature. 

==Production==
De Antonio had previously worked with Daniel Berrigan on his film In the Year of the Pig. The Berrigans asked de Antonio to film the trial for their actions in King of Prussia, which de Antonio initially refused because he did not share their religious beliefs. However, he changed his mind and agreed shortly before the trial. Martin Sheen had been a friend of de Antonios since the controversy surrounding de Antonios previous film, Underground (1976 film)|Underground. Sheen offered to help with de Antonios new project and accepted the role of Judge Samuel Salus II. 

For this film, de Antonio shot and edited the footage on video, then converted to 35mm film.

==Exhibition==
In the King of Prussia premiered in New York City on February 13, 1983. The film played at the Berlin International Film Festival that year.
 MPI Home Video. 

==References==
 

==External links==
* 

 
 
 
 
 


 