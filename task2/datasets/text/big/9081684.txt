Adolf Hitler: My Part in His Downfall (film)
{{Infobox film
| name           = Adolf Hitler: My Part in his Downfall
| image          = Adolf Hitler - My Part in His Downfall.jpg
| caption        = Theatrical release poster.
| director       = Norman Cohen Greg Smith Johnny Byrne Norman Cohen
| starring       = Jim Dale Arthur Lowe Bill Maynard Tony Selby Geoffrey Hughes Spike Milligan Pat Coombs Windsor Davies Bob Todd
| music          = Wilfred Burns Ed Welch Spike Milligan
| cinematography = Terry Maher
| editing        = Tony Lenny
| distributor    = United Artists
| released       =  
| runtime        = 102 min.
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
}}

Adolf Hitler: My Part in His Downfall is a 1973 film adaptation of the   of Spike Milligans autobiography. It starred Jim Dale as the young Terence "Spike" Milligan. Spike played the part of his father, Leo Milligan. 

==Plot==
London, 1940. Aspiring jazz musician Terence "Spike" Milligan reluctantly obeys his call-up and joins the Royal Artillery regiment at Bexhill-on-Sea, where he begins training to take part in World War II. But along the way Spike and his friends get involved in many amusing - and some not-so amusing - scrapes.

==Basis==
The film is based on the first volume of Milligans war memoirs.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 
 