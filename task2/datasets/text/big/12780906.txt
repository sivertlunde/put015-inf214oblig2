Audace colpo dei soliti ignoti
{{Infobox film
| name           = Audace colpo dei soliti ignoti
| image          = Audace colpo dei soliti ignoti.jpg
| image size     =
| caption        = Film poster
| director       =  Nanni Loy
| producer       = Franco Cristaldi
| writer         = Agenore Incrocci Furio Scarpelli Nanni Loy (dialogue)
| narrator       =
| starring       =  Vittorio Gassman, Renato Salvatori and Claudia Cardinale.
| music          = Piero Umiliani
| cinematography = Roberto Gerardi
| editing        = Mario Serandrei
| distributor    = Titanus
| released       = December 1959 (Italy)
| runtime        = 105 minutes
| country        = {{plainlist|
*Italy
*France }} Italian
| budget         =
}}
 1959 Italy|Italian comedy crime film directed by Nanni Loy. The film stars Vittorio Gassman, Renato Salvatori and Claudia Cardinale.

It is the sequel to Mario Monicellis I soliti ignoti.

==Plot==
A Milanese gangster contacts Peppe (Gassman); he has identified him and his accomplices as the perpetrators of the bungled attempt at the Madonna street pawn shop.

His offer is to reunite the same men for a daring robbery in Milan, where the local offices of football betting pool Totocalcio shift the weekly revenue on Sunday afternoon via a common car with just an accountant and a driver in it.
The gang would have to travel north from Rome disguised among the supporters of A.S. Roma going to Milan for a football match, commit the robbery and then flee to Bologna via a souped-up car there to rejoin the returning sport fans.

The Milanese seems tough and smart and his proposal sounds very inviting for the small-time crooks who all have their problems trying to lead an "honest" life, but things will go differently.

==Cast==
{|class=wikitable
!Actor!!Role
|- Vittorio Gassman Peppe er pantera
|- Renato Salvatori||  Mario Angeletti
|- Claudia Cardinale Carmela Nicosia
|- Nino Manfredi||Ugo Nardi, named "Piede Amaro"
|- Vicky Ludovici Floriana
|- Riccardo Garrone Riccardo Garrone Il milanese
|- Tiberio Murgia Ferribotte
|- Carlo Pisacane Carlo Pisacane Capannelle
|- Gianni Bonagura Totocalcio accountant
|- Gina Amendola  (as Luigina Amendola)|| Imma
|- Clara Bindi  (as Clara Bini)|| Luisella
|- Elena Fabrizi Lella
|- Mauro Lemma Gianni
|- Gastone Moschin||Bookseller
|- Elvira Tonelli   ||
|- Toni Ucci Totocalcio driver

|}

==Release==
Audace colpo dei soliti ignoti opened in Rome in December 1959.    It was shown later in Paris in August 1962 with the title Hold-up la milanaise. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 