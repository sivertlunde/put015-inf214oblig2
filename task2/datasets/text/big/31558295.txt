Pak! Pak! My Dr. Kwak!
{{Infobox film
| name           = Pak! Pak! My Dr. Kwak!
| image          = MyDrKwakofficialposter.png
| caption        = Theatrical movie poster
| director       =  Tony Y. Reyes
| producer       =   Antonio Tuviera   Marivic Sotto  Orlando Ilacad
| writer         = 
| eproducer      = 
| aproducer      =
| starring       =  
| cinematography = 
| editing        =
| studio         =APT Entertainment M-Zet Films OctoArts Films Star Cinema
| distributor    = Star Cinema
| released       =  
| runtime        =
| country        = Philippines
| language       =  
| budget         =
| gross          = P72.31 Million      
}} 2011 Cinema Filipino comedy film starring Vic Sotto and Bea Alonzo and produced by OctoArts Films, M-Zet Films, and APT Entertainment and released by Star Cinema. It is Vic Sottos comeback to Star Cinema after 14 years.


==Synopsis==
Angelo  dreams of creating a medicine that could cure his father’s illness. With some knowledge about medicinal herbs, he makes a living as a faith healer. Everyone believes him, except Cielo Delos Santos, a doctor who swears to do everything to uncover Angelo’s secret.

Things get a little complicated when Angelito (Zaijian Jaranilla), an exiled angel comes down to earth to help Angelo become a better man. As the three of them try to achieve their own goals, they realize that they need each other more than they are willing to admit.

==Main Cast==
*Vic Sotto  as Angelo The Great Pak Healer/Ka Olegna
*Bea Alonzo as Cielo Delos Santos
*Zaijan Jaranilla as Saint Angelito
*Xyriel Manabat as Mae-C 
*Pokwang as Pining
*Wally Bayola as Phil
*Jose Manalo as James

===Supporting cast===
*Paolo Ballesteros as Anton
*Joonee Gamboa as Juan
*Dexter Doria as Ester
*Victor Basa as Ricky
*Ryan Yllana as Paeng
*Jon Avila as Marcus
*Peque Gallaga as San Pedro (Saint Peter)
*Johnny Revilla as Doctor John Fuentes
*Thou Reyes as Doctor Reyes
*Paw Diaz as Doctor Diaz
*Charles Christianson as Doctor Charles
*Romeo Rivera as Don Ramon
*Ace Veloso as Ace

===Special Participation===
*Anjo Yllana as Doctor Yllana
*Joey de Leon as Doctor Joey
*Toni Rose Gayda as Sister Mary

==Reception==
The film has been graded B by the Cinema Evaluation Board of the Philippines.   The film had a total gross of P72.31 million.  


==Trivia==
*All of the hospital scenes were shot at the World Citi Medical Center in Aurora Boulevard, Quezon City.
*It is Vic Sottos comeback to Star Cinema after 14 years.
*This is Tomas Gonzales only (first and last) movie before recently died last March. However the following month had been premiered the movie.
*The characters names of Phil and James are derived from the name of Philippine Football Team Azkals.

==References==
 



 
 
 
 
 
 
 
 