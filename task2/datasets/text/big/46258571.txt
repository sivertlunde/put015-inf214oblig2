Dui Purush
 
 
{{Infobox film
| name           = Dui Purush
| image          = Dui_Purush_(1945).jpg
| image_size     = 
| caption        = 
| director       = Subodh Mitra
| producer       = New Theatres
| writer         = 
| narrator       = 
| starring       = Chhabi Biswas Chandravati Sunanda Banerjee Ahindra Choudhury
| music          = Pankaj Mullick
| cinematography = Sudhin Majumdar Yusuf Mulji
| editing        = Subodh Mitra
| distributor    =
| studio         = New Theatres, Ltd.
| released       = 30 August 1945
| runtime        = 110 minutes
| country        = India Bengali
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
Dui Purush (Two Generations) is a 1945 Bengali language epic family drama film directed by Subodh Mitra.  Produced by New Theatres and adapted from Tarashankar Banerjis  novel and popular stage play, it had screenplay by Binoy Chatterjee. The editing was done by Mitra, who was popularly known as Kachi Babu, and cited as one of the best editors in the film industry.    Music director was Pankaj Mullick with lyrics by Sailen Roy. The cinematograpers were Sudhin Majumdar and Yusuf Mulji. The cast included Chhabi Biswas, Chandravati , Sunanda Banerjee, Ahindra Choudhury, Tulsi Chakraborty, Jahar Ganguly, Naresh Mitra. 
 National Film Award for Best Film.    The film also won awards for Best Music for Pankaj Mullick, and Best Actress Award for Chandravati at the Bengal Film Journalists Association – Best Actress Award. It won five more awards at the 9th Annual BFJA Awards.   
 
Dui Purush involves three generations, and has the traditional theme of a poor social worker pitted against a rich Zamindar (Landowner). 

==Plot==
Nutubehari, called Nutu, is a poor but educated social worker who sticks to his lifes ideals and fights against the wealthy Zamindar for peoples rights. He falls in love with Kalyani, the Zamindars daughter. The enraged Zamindar has Nutu put in jail. When he gets out of jail, he finds Kalyani has been married off to someone else. Nutu marries Bimla, and starts a school in the village. He and Bimla have a son called Arun. Kalyanis husband dies and her in-laws throw her out of the house along with her infant daughter. She comes back to the village and takes shelter with Nutubehari who lets her run the school.

Years pass and Nutubehari has a court case against the Zamindar when he fights for one of the villagers. He has become a lawyer by now, joined politics and amassed great wealth. He has also let go of his ideals. His son, Arun is dismayed at the changes in his father. Arun also rebels against his arranged marriage as he now loves Kalyanis daughter Mamta (Latika). Finally, Nutu sees the error of his ways and reconciles with his former self.

==Cast==
* Chhabi Biswas as Nutubehari
* Chandravati as Bimla
* Sunanda Debi as Kalyani
* Ahindra Choudhury as the Zamindar
* Jahar Ganguly as Sushobhan
* Latika Bandyopadhyay as Mamta, Kalyanis daughter
* Naresh Mitra as Gopi Mitra
* Tulsi Chakraborty
* Sailen Chowdhury
* Naresh Bose
* Puru Mullik
* Rekha Mullik

==Review==
The review by Baburao Patel in Filmindia praised Chhabi Biswas as Nutubehari. Biswas had originally played the same role for the stage version. Chandravati as Bimla "competes successfully" with Biswas and her acting was commended for restraint. Ahindra Choudhary was stated to be "disappointing". The screenplay came in for harsh criticism, being called "utterly clumsy". The fact that an important character in the play, Sushobhan, played by Jawahar Ganguly on screen, was turned into a "frivolous" character, and the tragic death of Nutbehari avoided and the character allowed to live in the film also came in for censure.   

==Remake==
The film was remade in 1978 with the same title, Dui Purush, and was directed by Sushil Mukherjee. It starred Uttam Kumar as Nutbihari, Supriya Choudhury as Bimla, and Lily Chakraborty as Kalyani.   

==Awards== National Film Award for Best Film.   

Dui Purush won the following awards at the 9th Annual BFJA Awards:    Best Indian Films
* Bengal Film Journalists Association – Best Actress Award: Chandravati 
* Bengal Film Journalists Association – Best Art Director: Soren Sen
* Bengal Film Journalists Association – Best Music Director Award: Pankaj Mullick 
* Bengal Film Journalists Association – Best Lyricist Award: Sailen Roy 
* Bengal Film Journalists Association – Best Cinematographer Award: Sudhin Majumdar
* Best Audiographer: Loken Bose

==Soundtrack==
The music was appreciated by the public and won awards for both the composer Pankaj Mullick, and lyricist, Sailen Roy.

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| "Ae Bijoyi Deene"
|-
| 2
| "Behag Jodi Na Hay Raji"
|-
| 3
| "Prabhu Tomar Ashishe"
|-
| 4
| "Tomar Ae Gaaner Hawa"
|-
| 5
| "Orey Aamar Gaan Kothay"
|-
| 6
| "Tomar Oi Surer"
|}

==References==
 

==External links==
* 


 
 
 