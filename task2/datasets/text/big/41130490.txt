No Hay Pan
 

{{Infobox film
| name           = No Hay Pan
| image          = No Hay Pan official movie poster.jpg
| alt            = 
| caption        = Official Movie Poster
| director       = Macarena Monrós
| producer       = Iván Nakouzi
| writer         = Pamela Muñoz
| starring       = Raúl Palma, Marta Méndez
| music          = Eduardo Ortíz, Martín Pérez
| cinematography = Mauricio Palacios
| editing        = Jasmín Valdés
| studio         = 
| distributor    = 
| released       =  
| runtime        = 19 minutes
| country        = Chile
| language       = Spanish
| budget         = 
| gross          = 
}}
No Hay Pan is a Chilean short film, directed by Macarena Monrós. The plot deals with issues such as capitalism, modernity, the loss of heritage and traditions. It was produced by Iván Nakouzi and premiered on August 21 of the year 2012, at the Santiago International Film Festival (SANFIC) in Chile.  On 24 January 2013 it was internationally premiered at the International Film Festival Rotterdam (IFFR). 

== Plot==
Luis (Palma, Rául) is the owner of a little grocery store that suffers a sudden downturn when a supermarket sets up in its neighborhood and he is told that hell no longer be provided with bread. The man faces a crossroads: closing the store, or finding another bread supplier to keep his business open.

== References ==
 

== External links ==
 

 