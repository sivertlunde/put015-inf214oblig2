The Fire Alarm
 
 
{{Infobox Hollywood cartoon
|series=Looney Tunes
|image=Ham and Ex.jpg
|caption=Screenshot Jack King
|story_artist=
|animator=Paul Smith Ben Clopton
|voice_actor=Billy Bletcher Bernice Hansen
|musician=Norman Spencer
|producer=Leon Schlesinger
|studio=Warner Bros. Cartoons
|distributor=Warner Bros. The Vitaphone Corporation
|release_date=March 9, 1936 (USA)
|color_process=Black and white
|runtime=6 minutes
|movie_language=English Boom Boom
|followed_by=The Blow Out
}} Ham and Ex in their first and only film as star characters, but also co-starring their Uncle Beans the Cat|Beans.

==Plot==
Ham and Ex have been sent to the fire station by their mother Lizzie so Uncle Beans can babysit them. Ham and Ex have an ambition of becoming firefighters and play around with their Uncles fire engine and equipment. Annoyed by their fiddling around, Beans has them seated on a bench, but when his back is turned, Ham and Ex accidentally set off the fire alarm, waking the other firemen, only to realise on their return it was a false alarm.

Beans has Ham and Ex grounded in the sleeping quarters. Once hes left, Ham and Ex jump out of their beds and down the fire pole. Then they get in the fire engine and drive off through the station wall into the street, to Beans horror. Beans runs after them as they clumsily crash and bump their way around the city. Finally they drive back to the station and hop back into bed just as Beans returns. He notices they appear to be asleep before he can belt them. As Beans exits the room, Ham and Ex toss a firemans boot at him. Entirely fed up with their bad behaviour, Beans drags them to one end of the bed and spanks them.

==See also==
* Looney Tunes and Merrie Melodies filmography (1929–1939)

==External links==
*  
*  

 
 
 
 
 
 
 
 


 