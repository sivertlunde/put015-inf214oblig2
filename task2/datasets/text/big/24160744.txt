Breaking Point (1976 film)
{{Infobox film
| name           = Breaking Point
| image          = Breaking_Point_(1976_film).jpg
| image_size     =
| caption        =
| director       = Bob Clark
| writer         = Stanley Mann Roger Swaybill
| narrator       =
| starring       = Bo Svenson Robert Culp John Colicos
| music          =
| producer       = Bob Clark
| cinematography =
| editing        = Stan Cole
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
Breaking Point is a 1976 film directed by Bob Clark. 

==Plot==
Vincent Karbone is a leading construction magnate in Philadelphia and a suspected leader of one of the citys most notorious criminal gangs. Several of his thugs are on trial, and the key witness is Michael, a mild-mannered judo instructor with a wife and kids. Karbone will stop at nothing to keep the muscles of his organization out of prison, including striking at Michaels family to keep him from testifying.

==Cast==
* Bo Svenson as Michael McBain
* Robert Culp as Frank Sirrianni
* John Colicos as Vincent Karbone
* Belinda Montgomery as Diana McBain
* Linda Sorenson as Helen McBain Stephen Young as Peter Stratas

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 