Fashions for Women
{{Infobox film
| name           = Fashions for Women
| image          = 
| alt            = 
| caption        = 
| director       = Dorothy Arzner
| producer       = B. P. Schulberg Jesse L. Lasky Adolph Zukor Percy Heath Herman J. Mankiewicz Léopold Marchand George Marion, Jr.
| starring       = Esther Ralston Raymond Hatton Einar Hanson Edward Martindel William Orlamond Agostino Borgato
| music          =
| cinematography = H. Kinley Martin  Marion Morgan
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent Percy Heath, Herman J. Mankiewicz, Léopold Marchand and George Marion, Jr.. The film stars Esther Ralston, Raymond Hatton, Einar Hanson, Edward Martindel, William Orlamond and Agostino Borgato. The film was released on March 26, 1927, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Esther Ralston as Céleste de Givray and Lola Dauvry
*Raymond Hatton as Sam Dupont
*Einar Hanson as Raoul de Bercy
*Edward Martindel as Duke of Arles
*William Orlamond as Roue
*Agostino Borgato as Monsieur Alard
*Edward Faust as Monsieur Pettibon
*Yvonne Howell as Mimi
*Maude Wayne as The Girl
*Charles Darvas as Restaurant Manager 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 