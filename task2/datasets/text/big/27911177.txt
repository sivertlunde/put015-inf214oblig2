U-Turn (1973 film)
 
{{Infobox film
| name           = U-Turn
| image          = 
| caption        = 
| director       = George Kaczender
| producer       = George Kaczender
| writer         = George Kaczender Douglas Bowie
| starring       = David Selby
| music          = 
| cinematography = Miklós Lente
| editing        = George Kaczender
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 99 minutes
| country        = Canada
| language       = English
| budget         = 
}}

U-Turn, also known as The Girl in Blue, is a 1973 Canadian drama film directed by George Kaczender. It was entered into the 23rd Berlin International Film Festival.   

==Cast==
* David Selby - Scott Laithem
* Maud Adams - Paula / Tracy
* Gay Rowan - Bonnie William Osler - Professor Bamberger
* Diane Dewey - Holly Michael Kirby - Kippie
* Walter Wakefield - Old man
* Don Arioli - Sidewalk artist
* Valda Dalton - Bingo woman Guy Martin - Policeman
* Michel Maillot - Good Humour man
* Hanka Posnanska - Flower woman
* George R. Robertson - Tennis pro
* Elsa Pickthorne - Georgette
* Donald Ewer - Les Turnbull

==References==
 

==External links==
* 

 
 
 
 
 
 