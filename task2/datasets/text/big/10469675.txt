Caprice (1967 film)
{{Infobox film
| name           = Caprice
| image          = Caprice (film) poster.jpg
| caption        = original film poster
| producer       = Aaron Rosenberg Martin Melcher
| director       = Frank Tashlin
| writer         = Jay Jayson Frank Tashlin
| starring       = Doris Day Richard Harris
| music          = 
| cinematography = Leon Shamroy
| editing        = 
| distributor    = 20th Century-Fox
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget = $4,595,000 
| gross          = $4,075,000 
}}
Caprice is a 1967 comedy-thriller film directed by Frank Tashlin starring Doris Day and Richard Harris. This film and In Like Flint (1967) were the last movies made in CinemaScope, with most studios moving to Panavision and other widescreen processes. 

==Plot==
 
In the Swiss Alps, a skier is shoots and kills another skier down a mountain slope. Cut to a woman picking up a newspaper in Paris, the headline story telling of yet another Interpol agent killed in Switzerland.

That woman is Patricia Foster (Doris Day), an industrial designer for Femina Cosmetics, owned by her boss, Sir Jason Fox (Edward Mulhare). She gets caught trying to sell a secret Femina cosmetics formula for an underarm deodorant to a rival company, May Fortune, owned by Matthew Cutter (Jack Kruschen). After her arrest, her subsequent release, and her firing from Femina, Patricia is hired by Cutter to work for him in their Los Angeles operation, but she nonetheless states she will not divulge any of Feminas other secrets as part of this employment. However, this selling of secrets was a scheme devised by Sir Jason for Patricia to steal a secret formula for a new water-repellent hairspray from Cutter, the formula invented by Dr Stuart Clancy (Ray Walston), May Fortunes head chemist.

Patricia is wooed by Cutters right hand man, Christopher White (Richard Harris), who drugs her with truth serum to get her to divulge Feminas other secrets to him, which he passes along to Cutter. Patricia, however, was aware of what Christopher was trying to do; she only pretended to be drugged and passed along false information.

While she cant get the formula from Cutter or Clancy, Patricia learns that the formula can be obtained through analysis of a lock of hair from the one person known to have used the hairspray, a beautiful Asian woman who ends up being Clancys secretary, Su Ling (Irene Tsu). As Patricia goes to work trying to cut off a lock of Su Lings hair, Christopher spots her. Patricia is unable to get that lock of hair, but Christopher tells her that he too is truly working for Sir Jason, and has the proof to back-up his claim.

Later, just before a lunch rendezvous at a restaurant with Patricia at which they will discuss how to proceed in getting the hairspray formula, Christopher is seen setting up surveillance, which includes speaking to Cutter via hidden microphone, Cutter stating that it will be good to see an undercover agent of Sir Jasons caught red-handed trying to steal his formula. Patricia sees what Christopher is doing, although she is assured by Sir Jason that Christopher indeed does work for him and not Cutter. Patricia still does not trust Christopher as she enters the lunch date with him. She does whatever she can to thwart the surveillance that Christopher has set up, much to Christopher and Cutters chagrin.

At a date with one of his regulars named Miranda (Lisa Seagram) - a May Fortune model who Patricia earlier seemed to recognize - Christopher learns that Patricia Foster is not the real name of the woman who May Fortune just hired.

Patricia spots Su on a movie date. Patricia sits behind her to cut off a lock of her hair, but instead is hit on secretly by Sus date, Barney (Michael J. Pollard). An altercation ensues when Patricia hits Barney.

At The Times newspaper archives, Christopher is looking through old clippings of a story concerning a man named Robert Fowler being murdered in the Swiss Alps while skiing.

At Sus apartment, Patricia finds her unconscious on the floor, seemingly drugged. She takes a small sample of a black powder lying on the coffee table, which she believes was used to drug Su and which she later gives to Sir Jason to have analyzed for her. Patricia searches and finds a bottle of the hairspray, which she takes with her. Christopher catches her, blackmailing her that he will tell Sir Jason that her real name is Felippa Fowler. Then Dr Clancy arrives, while Patricia and Christopher try to escape without he finding out they were the ones in Sus apartment. A chase ensues through the apartment building, the police and Clancy both trying to nab whoever was in Sus apartment, but Patricia and Christopher are eventually able to escape.

At a later rendezvous in the middle of the ocean on a small outboard-motor boat, Patricia admits to Christopher that her real name is indeed Felippa Fowler, and that her father - the man featured in the news clipping - was murdered in Switzerland while on the trail of a narcotics ring. Before his death, he had divulged that the head of the narcotics ring was discovered to be a woman, most probably the person who killed him. Although finding her fathers killer is her main objective, Patricia is still devoted to Sir Jason, to whom she plans to give the bottle of hairspray. Purposely breaking the bottle upon hearing this news - but noticing that the bottle is an expensive Swiss one not used by Cutter - Christopher in turn tells Patricia that Clancy has Swiss connections - that he is married to a Swiss woman still living in Switzerland, and has children who go to school there.

Patricia goes to Switzerland and finds at the address for Clancys family, a woman named Madame Piasco (Lilia Skala) - Clancys mother-in-law - who is the actual cosmetics expert who had long ago come up with the formula for herself to protect her hair for skiing. She gives a bottle to Patricia for free, as Madame Piasco states that May Fortune now has the distribution rights and it will soon be on the market in America.

Patricia goes skiing on the same hill where her father was killed, and comes under fire from a masked skier. She is rescued by Christopher, who arrives in a helicopter in the nick of time. Patricia believes it is Clancy who tried to kill her, while Christopher thinks it couldnt be him. He seems unconcerned about the news about the hairspray or Clancys mother-in-law, which makes Patricia realize that neither Christopher nor Sir Jason were ever after the hairspray. Christopher tells her the story: that Clancy used to be the chief cosmetics chemist for Sir Jason, but was an utter failure at it, and that Sir Jasons true goal was to discover and hire the true chemical mastermind behind Clancy, whose identity he still doesnt know. Regardless, Patricia plans to give the hairspray solely to Sir Jason, which was her agreed mission. Christopher and Patricia profess their love for each other, but Christopher requests one more job from her concerning Sir Jason.

Out in the middle of a snowy mountaintop, Christopher has secretly placed a microphone on Patricia and secretly films her while she goes to speak to Clancy off in the distance. She offers Clancy a job with Femina as head chemist, with an illegal under-the-table bonus. Clancy declines as he says that Cutter already knows about Madame Piasco as the chemical mastermind, and if Cutter doesnt care, he is happy where he is. Cut to Cutters office where he is watching the film that Christopher took of Clancy and Patricias encounter. He tells Christopher that this footage, which he plans to broadcast, will ruin Sir Jason forever. But Cutter is angry that Christopher has not kept Patricia under confinement for her illegal role in the matter; he in return states that they can nab Patricia as she leaves Sir Jasons.

Patricia is visiting with Sir Jason, who tells her that he was able to convince Madame Piasco to come and work for him, which Patricia finds incredible. Sir Jason also tells her about the analysis of the black powder, which contains a powerful narcotic. Patricia in turn tells him that she already had the powder analyzed herself, and this analysis conducted by Sir Jason was a test purely to see if he would tell the truth or a lie, the latter of which would implicate him in her fathers murder. Sir Jason also tells her that Christopher had her encounter with Clancy filmed, that Cutter plans to broadcast it to the world to ruin him, and that the Paris police after being shown the film, are now after her for bribery. He states that Christophers motivation is purely financial, as he collects from both sides. Patricia is heartbroken about this news concerning Christopher.

As the police are ready to arrest Patricia outside Sir Jasons apartment, Christopher snatches her away. He admits that he works for Interpol. Later, Patricia is able to tell Christophers Interpol colleagues that the narcotics were smuggled as May Fortune face powder, which was perfectly harmless until incinerated, but then turned into a powerful hallucinogen. None of them believes that Cutter is smart enough to be the head of the narcotics ring.
 
Donning a microphone to Interpol while searching through Cutters Paris office, Patricia tries to tell the cleaning lady who enters the office she need not clean here. The cleaning lady is revealed to be Clancy in disguise, with a gun - he being the mysterious woman killer. As Patricia and Clancy move through the building, they scuffle, a gunshot is heard; Clancy is shot, and tumbles over the railing to his death several stories below. Sir Jason, the co-conspirator of the narcotics ring, arrives wielding a gun against Patricia. With Clancy dead, Sir Jason will have the cosmetics market all to himself as the narcotics will have been found in May Fortune containers, of which Cutter is totally unaware. Sir Jason manages to force Patricia into a helicopter and take off. Christopher shoots and kills him from a distance, leaving a frightened Patricia alone in the air. She somehow manages to fly the helicopter back to Paris and land it atop the Eiffel Tower.

Patricia and Christopher live happily ever after.

==Cast==
* Doris Day: Patricia Foster
* Richard Harris: Christopher White
* Ray Walston: Dr Stuart Clancy
* Edward Mulhare: Sir Jason Fox
* Jack Kruschen: Matthew Cutter
* Lilia Skala: Madame Piasco
* Michael J. Pollard: Barney
* Irene Tsu: Su Ling
Arthur Godfrey, who played the father of Doris Day in Tashlins previous comedy, The Glass Bottom Boat, plays her father once again but is only seen via photograph.

==Reception==
The 20th Century-Fox release was not a box-office success, failing to place in the Top 20 movies for 1967.
Film critic Leonard Maltin’s review of the film was quite negative. He gave the film zero stars and said the film was a “terrible vehicle for Day.”

In her memoir, Day recounts an argument she had with her manager-husband Martin Melcher over the script for Caprice, unaware he had signed her name to the contracts before she had the chance to say no. On the DVD commentary, authors Pierre Patrick and John Cork discuss the ways the screenplay was rewritten, ostensibly to please the star. They speculated that recent interest in the films mixture of slapstick, satire, and adventure—coupled with its Mod design—has acquired renewed respect from film buffs and, possibly, from Day herself.

==Music==
The buttery  title theme sung by Doris Day was released as the flip-side to her final single release on the Columbia Records label, the A-side being a more uptempo number, "Sorry."

==Adaptations==
The screenplay by Jay Jayson and Tashlin was novelized by Julia Withers and was published in paperback by Dell in February, 1967.

==Home media==
Initially only released on VHS in the UK, the movie was eventually released in a deluxe edition Region 1 DVD in January 2007 in widescreen and includes several extra features.

==Cast==
* Doris Day as Felippa Fowler aka Patricia Foster
* Richard Harris as Christopher White
* Ray Walston as Dr. Stuart Clancy
* Jack Kruschen as Matthew Cutter
* Edward Mulhare as Sir Jason Fox
* Irene Tsu as Su Ling
* Lilia Skala as Madame Piasco
* Michael J. Pollard as Barney

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 