The Man with the Claw
{{Infobox film
| name = The Man with the Claw 
| image =
| image_size =
| caption =
| director = Nunzio Malasomma
| producer = 
| writer = Aldo Vergano   Rudolph Cartier   Egon Eis   Otto Eis   Nunzio Malasomma
| narrator = Carlo Fontana   Elio Steiner   Vasco Creti
| music = Felice Montagnini 
| cinematography = Carlo Montuori
| editing = Guy Simon
| studio = Società Italiana Cines 
| distributor = Societa Anonima Stefano Pittaluga
| released = November 1931
| runtime = 80 minutes
| country = Italy Italian
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Carlo Fontana and Elio Steiner. It was made at the Cines Studios in Rome. The film is one of several regarded as a possible precursor to the later giallo genre. 

==Cast==
* Dria Paola as La dottoressa Renata Vigo  Carlo Fontana as Pietro Kruger 
* Elio Steiner as Gastel 
* Vasco Creti as Il commissario 
* Carola Lotti as Gina Rappis  Carlo Lombardi as Carlo Lopez 
* Carlo Gualandri as Rappis 
* Gino Viotti as Alberti 
* Augusto Bandini 
* Giuseppe N. Bellini 
* Fedele Gentile 
* Lucia Parisi

== References ==
 

== Bibliography ==
* Moliterno, Gino. A to Z of Italian Cinema. Scarecrow Press, 2009.

== External links ==
* 

 
 
 
 
 
 
 


 