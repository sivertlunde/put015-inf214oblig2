Karma (1986 film)
{{Infobox film
| name           = Karma
| image          = Karmafilm86.jpg
| image_size     =
| caption        = Poster
| director       = Subhash Ghai
| producer       = Subhash Ghai
| writer         = Sachin Bhowmick Subhash Ghai Kader Khan
| narrator       =
| starring       = Sridevi Dilip Kumar Naseeruddin Shah Jackie Shroff Anil Kapoor Anupam Kher Nutan Poonam Dhillon
| music          = Laxmikant-Pyarelal
| cinematography = Kamalakar Rao
| editing        = Waman Bhonsle Gurudutt Shirali
| studio         =
| distributor    = Mukta Arts Ltd. Lousanne Films
| released       = 8 August 1986
| runtime        = 194 mins
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Karma (  film directed by Subhash Ghai and featuring an ensemble cast including Dilip Kumar, Nutan, Jackie Shroff, Anil Kapoor, Naseeruddin Shah, Sridevi and Anupam Kher. The film reunites Subhash Ghai and Dilip Kumar after the success of their last film together Vidhaata (1982). The film also marked the first time Dilip Kumar was paired with veteran actress Nutan.

==Plot summary==
Rana Vishwa Pratap Singh (Dilip Kumar) is an ex- high ranking police officer who is in charge of a jail that successfully reforms criminals.  

One day he is informed that the head of a major terrorist organisation  "Dr Dang (Anupam Kher)" has been captured and will be secretly held in his prison due to its remote location

On arrival at the prison Dang states he expects a cell akin to a hotel room.  He also makes it clear that through bribes and favours he will be released very shortly.   The following day Dang assaults a prison warden during an argument about the condition of his cell. On hearing this, Singh goes to Dangs cell and slaps him, which is highly insulting in Indian culture.  Dang then vows to personally take revenge on Singh.

While Singh is away , Dangs army successfully locate the prison and free him.  They then set about destroying the prison and killing everyone including Singhs family who reside nearby.  However, Singhs wife is spared. 

After a period of mourning Singh decides to embark on a mission to bring Dang and his terrorist organisation to justice. Singhs plan is to train an elite unit, the members of which will be selected from death row.  He believes the offer of freedom  will motivate the prisoners to swap a life of crime for one of honour.  The mission is approved by the Indian government and Singh successfully recruits Baiju Thakur (Jackie Shroff), Johnny/Gyaneshwar (Anil Kapoor), and a former terrorist Khairuddin Chishti (Naseeruddin Shah).

Under the guise of being forest rangers , Singh and his recruits set up base near the Indian border near where they believe Dr Dangs compound is located.  Singh and his three soldiers form a strong family bond during their training. However Baiju and Johnny fall in love with some local women and make several attempts to elope. Each time they are recaptured by Singh who firmly reminds them that they can either continue with the mission or return to death row.   Khairuddin pleads with Singh to let Baiju and Johnny leave as he believes they have been reformed.  Singh is not pleased and reminds Khairuddin that he granted them life on the basis that they would fight to the end.  As far as he is concerned they are here to complete the mission only.

The mission takes a setback when Singh is shot by the terrorists whilst protecting his three soldiers who are locked in a nearby building.    Singh is taken to a hospital and placed on life support.  The three soldiers are distraught and realise they need to complete the mission for Singh.   They then receive intelligence on the exact location of Dr Dangs compound which they successfully infiltrate. With the help of  hostages (who are Indian army soldiers) who have been held captive by the terrorists,  they set about destroying the compound and killing the terrorists. It soon becomes apparent that the destruction of the heavily guarded munitions centre is the only way the terrorists organisation can be defeated.   Khairuddin hatches a plan that involves driving a truck filled with explosives into the heart of the building.  Baiju and Johnny argue that the plan is too dangerous and they would all be killed in the process. However Khairuddin wants to sacrifice himself so that his friends can enjoy their new found freedom. Baiju and Johnny are forcilby removed off the truck by Khairuddin who then goes on to destroy the munitions centre at the cost of his life.  Johnny and Baiju return to the main compound and kill the remaining terrorists bringing the battle to its conclusion. Singh, having made a swift recovery, joins the battle and kills Dr.Dang thus completing the mission.

The two surviving soldiers are awarded  bravery medals and a posthumous award is granted to Khairuddin. 
Singh and the two soldiers form a long lasting friendship.

==Cast==
* Dilip Kumar as Rana Vishwa Pratap Singh as Dada Thakur
* Nutan as Rukmini
* Naseeruddin Shah as Khairuddin Chishti
* Jackie Shroff as Baiju Thakur
* Anil Kapoor as Johnny/ Gyaneshwar
* Anupam Kher as Dr Michael Dang
* Sridevi as Radha  
* Poonam Dhillon as Tulsi (Johnnys girlfriend)
* Dara Singh as Dharma (Dada Thakurs friend)
* Shakti Kapoor as Jagga / Jolly Dharmas brother
* Tom Alter as Rexson (Michael Dangs henchman)
* Shashi Puri as Anil (Vishwanaths son)
* Satish Kaul as Sunil (Vishwanaths son) Beena .... Sunils wife
* Jugal Hansraj .... Rana Vishwa Pratab Singhs youngest and surviving son (child artist).
* Mukri .... Chhote Khan
* Vinod Nagpal .... Minister Tripathi
* Dan Dhanoa .... Indian Army officer in BSOs prison Bindu .... Radhas Aunty
* Satyanarayana Kaikala .... Kittam Kittu

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Karma (Introduction)"
| Arun Bakshi
|-
| 2
| "Mera Karma Tu"
| Manhar Udhas, Mohammad Aziz, Suresh Wadkar  
|-
| 3
| "Aye Watan Tere Liye"
| Mohammad Aziz, Kavita Krishnamurthy
|-
| 4
| "Aye Sanam Tere Liye"
| Dilip Kumar, Mohammad Aziz, Kavita Krishnamurthy
|-
| 5
| "Maine Rab Se Tujhe"
| Manhar Udhas, Anuradha Paudwal
|-
| 6
| "De Daru"
| Kishore Kumar, Mahendra Kapoor, Manhar Udhas 
|-
| 7
| "Na Jaiyo Pardes"
| Kishore Kumar, Kavita Krishnamurthy
|-
| 8
| "Aye Mohabbat Teri Dastan"
| Anuradha Paudwal
|}

== External links ==
* 

 

 
 
 
 


 