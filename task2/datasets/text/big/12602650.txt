Arranged (film)
{{Infobox Film

 | name = Arranged
 | caption = 
 | director = Diane Crespo   Stefan Schaefer
 | producer = Diane Crespo   Stefan Schaefer
 | writer = Stefan Schaefer  Yuta Silverman
 | starring = Zoe Lister-Jones Francis Benhamou
 | music = Sohrab Habibion Michael Hampton
 | cinematography = Dan Hersey
 | editing = Erin Greenwall
 | distributor = Cicala Filmworks
 | released = March 11, 2007
 | runtime = 90 minutes
 | country = United States
 | awards =  English Hebrew Hebrew Arabic Arabic
 | budget = 
 | preceded_by = 
 | followed_by = 
 | image = Arranged VideoCover.png
}}
 Orthodox Jewish woman from Borough Park, Brooklyn and her friendship with a young Muslim woman. Schaefer co-directed and produced the film with his long-time collaborator Diane Crespo.

==Plot summary== public school in Brooklyn. Over the course of the year they learn that they share much in common, not least of which is that they are both going through arranged marriages.

==Cast==
The cast includes Zoe Lister-Jones, Francis Benhamou, Marcia Jean Kurtz, John Rothman, Mimi Lieber and Daniel London.

==Festivals and distribution==
The film premiered at the 2007 South by Southwest Film Festival, won top prizes at The Berkshire International Film Festival, The Brooklyn International Film Festival, The Washington Jewish Film Festival, The Palm Beach Jewish Film Festival, and The Skip City International D-Cinema Film Festival in Japan. The film is distributed in North America by New York-based Film Movement. Internationally, the film is sold by Visit Films, and has been distributed in more than forty countries.

== External links ==
* 
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 
 