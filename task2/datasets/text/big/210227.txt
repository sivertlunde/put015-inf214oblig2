The Pumpkin Eater
 
 
{{Infobox film
| name           = The Pumpkin Eater
| image          = The Pumpkin Eater .jpeg
| image_size     =
| caption        = Theatrical poster
| director       = Jack Clayton James Woolf
| writer         = Harold Pinter Penelope Mortimer   (novel) 
| narrator       =
| starring       = Anne Bancroft Peter Finch James Mason
| music          = Georges Delerue
| cinematography = Oswald Morris Jim Clark
| distributor    = Royal Films International
| released       =  
| runtime        = 118 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross = $1,200,000 
| preceded_by    =
| followed_by    =
}}
The Pumpkin Eater is a 1964 British drama film starring Anne Bancroft as an unusually fertile woman and Peter Finch as her philandering husband.
The film was adapted by Harold Pinter from the 1962 novel of the same name by Penelope Mortimer, and was directed by Jack Clayton. The title is a reference to the nursery rhyme Peter Peter Pumpkin Eater.

== Plot ==
The story revolves around Jo Armitage (Anne Bancroft|Bancroft), a woman with an ambiguous number of children from three marriages, who becomes negative and withdrawn after discovering that her third (and current) husband, Jake (Peter Finch|Finch), has been unfaithful to her. After a series of loosely related events in which Jakes infidelity is balanced by his reliability as a breadwinner and a father, Jo and Jake take a first tentative step toward reconciliation.

Most of the story is based on two issues:  Jos predilection for childbearing and Jakes extramarital affairs. The question of Jos fertility is first broached by her psychiatrist. He suggests that she may feel uncomfortable with the messiness or vulgarity of sex, and that she may be using childbirth to justify it to herself. This does not prevent her from becoming pregnant again, but she follows suggestions by Jake and her doctor that she have an abortion and be sterilised, and she seems happy after the operation.

Meanwhile, signs accumulate that Jake has been having affairs while pursuing a successful career as a screenwriter. The first indication is about a woman who lived with the Armitage family for a while. Jake reacts irrationally and unconvincingly to Jos questioning after the children tell her the woman fainted into Jakes arms. The second sign comes from Bob Conway (James Mason|Mason), an acquaintance who alleges an affair between his wife and Jake during production of a film in Morocco. Finally, Jake admits some of his infidelities under heated interrogation by Jo. After venting her frustration by furiously assaulting him, she retaliates by having an affair with her second husband. This elicits a similar coldness from Jake.

In the films finale, Jo spends a night alone in the windmill (near the converted barn she had lived in with her second husband and children) that the couple have been renovating. The following morning, Jake and their children arrive at the windmill with food. Seeing how happy her children are with Jake, Jo indicates her acceptance of him by sadly but graciously accepting a can of beer from him, a gesture which echoes another scene in the windmill from a happier time in their marriage.

==Cast==
* Anne Bancroft – Jo Armitage
* Peter Finch – Jake Armitage
* James Mason – Bob Conway
* Janine Gray – Beth Conway
* Cedric Hardwicke – Mr. James, Jos father
* Rosalind Atkinson – Mrs. James, Jos mother Alan Webb – Mr. Armitage, Jakes father Richard Johnson – Giles
* Maggie Smith – Philpot
* Eric Porter – Psychiatrist
* Cyril Luckham – Doctor Anthony Nicholls – Surgeon
* John Franklyn-Robbins – Parson
* John Junkin – Undertaker
* Yootha Joyce – Woman at hairdressers
* Frank Singuineau - King of Israel

==Reception== Fellini comment is being made on our society, but if this is indeed so then the glee and the ambivalence are significantly more telling, and certainly more apparent, than any clarity of focus." 
 Pumpkin Eater flashback technique is confusing in the early stages. Jack Claytons direction gets off to a slow, almost casual start, but the pace quickens as the drama becomes more intense." 
 The French The Servant. 

==Awards== Best Actress BAFTA Award Best Actress Mary Poppins). 1964 BAFTA Award for Best British Screenplay. 

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 