What Remains: The Life and Work of Sally Mann
{{Infobox Film
| name           = What Remains: The Life and Work of Sally Mann
| image          = What Remains Sally Mann Poster.jpg
| image_size     = 
| caption        = 
| director       = Steven Cantor
| producer       = Steven Cantor Daniel Laikind Pax Wassermann Mandy Stein Suzanne Georges
| writer         = 
| narrator       = 
| starring       = Sally Mann
| music          = Billy Coté Mary Lorson
| cinematography = Paul Dokuchitz
| editing        = Pax Wassermann Sari Gilman
| studio         = Stick Figure Productions
| distributor    = HBO Documentary Films
| released       =    (DVD released in 2006) 
| runtime        = 80 minutes
| country        = United States
| language       = English   
| budget         = 
| gross          = 
}}
What Remains: The Life and Work of Sally Mann is a 2005 film directed and produced by Steven Cantor,    which documents the photography and story of photographer Sally Mann at her Virginia farm home. The movie documents the photographers progression from a child to a mother, and the struggles Mann faces through her public and private life.   

The movie garnered quite favorable reviews from publications such as The New York Times. 

==See also==
* 

== References ==
 

 
 
 
 
 
 

 