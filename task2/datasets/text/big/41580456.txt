Cynthia (film)
{{Infobox film
| name           = Cynthia
| image_size     =
| image	         = 
| caption        =
| director       = Robert Z. Leonard
| producer       = Edwin H. Knopf Charles Kaufman Buster Keaton
| based on       =  
| starring       = Elizabeth Taylor Mary Astor George Murphy
| music          = Bronislau Kaper
| cinematography = Charles Edgar Schoenbaum Irvine "Cotton" Warburton
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 98 min.
| country        = United States English
| budget         = $1,318,000  . 
| gross          = $1,648,000 
}}
 Charles Kaufman, was based on the play The Rich, Full Life by Viña Delmar. 

==Plot summary==
In school, baseball hero Larry Bishop impresses a girl, Louise, and they fall in love. Both coincidentally have dreams of traveling to Vienna, Austria someday to continue their education, Larry in medicine, Louise in music.

When she becomes pregnant, Louise and Larry marry and move to his hometown in Illinois, a small town called Napoleon. He takes a job in Dingles hardware store and they raise a daughter, Cynthia, who has chronic health problems and is quite frail. Fifteen years later, the Bishops are having trouble making ends meet, cant afford to buy a home and no longer have any illusions about the adventurous lives they intended to lead.

Dr. Fred Jannings has been the familys physician since Cynthias birth, and strongly recommends against her doing any strenuous activities. Louise ignores this advice and lets Cynthia take a role in the school musical, but her health fails, causing Larry to be angry with his wife.

Cynthia falls for a classmate, Ricky Latham, in the meantime. But as the bills and worries mount, Larry loses his patience and his job one day after his boss, Dingle, objects to his coming late to work. In the end, though, the family unites to embrace the future, satisfied when Larrys boss comes back, hat in hand, asking him to return to his job.

==Cast==
* Elizabeth Taylor as Cynthia Bishop
* Mary Astor as Louise Bishop
* George Murphy as Larry Bishop
* S. Z. Sakall as Professor Rosenkrantz
* Gene Lockhart as Dr. Fred Jannings
* Spring Byington as Carrie Jannings James Lydon as Ricky Latham
* Scotty Beckett as Will Parker
* Anna Q. Nilsson as Miss Brady
* Minerva Urecal as Maid
* Morris Ankrum as Mr. Phillips
* Kathleen Howard as McQuillan
* Shirley Johns as Stella Regan
* Harlan Briggs as J.M. Dingle Will Wright as Gus Wood

==Reception==
According to MGM records the film earned $1,206,000 in the US and Canada and $442,00 elsewhere, resulting in a loss of $280,000. 

==References==
 

==External links==
*  
*   at TCMDB
*  
*  

 

 
 
 
 
 
 
 
 
 

 