Mere Dad Ki Maruti
{{Infobox film
| name              = Mere Dad Ki Maruti
| image             = Mere Dad Ki Maruti promotional poster.jpg
| alt               = 
| caption           = Theatrical release poster
| director          = Ashima Chibber
| producer          = Yash Raj Films and Y-Films
| story             = Neeraj Udhwani
| screenplay        = Neeraj Udhwani Pooja Desai Ashima Chibber
| dialogues         = Ishita Moitra
| starring          = Saqib Saleem Rhea Chakraborty Ram Kapoor Prabal Panjabi Ravi Kishan Sachin Gupta
| cinematography    = Adil Afsar
| editing           = Antara Lahiri
| studio            = Y-Films
| distributor       = Yash Raj Films ShowMaker Riser Pictures
| released          =  
| runtime           = 101 mins.
| country           = India
| language          = Hindi
| budget            =  
| gross             =    
}}
Mere Dad Ki Maruti (English: My dads Maruti) is a 2013 Bollywood comedy film directed by Ashima Chibber, featuring Saqib Saleem, Ram Kapoor and Prabal Panjabi in lead roles. The film takes its plot from the 2000 Hollywood film Dude, Wheres My Car?. It also features newcomer Rhea Chakraborty in a pivotal role.  It was well received by critics and audiences and did quite well at the box office. It was declared as a hit in Punjab and semi hit in Delhi and was declared as average to flop in rest of the India but due to low business and not much profit but still a good trend, It was declared average overall and flop overseas. The sequel of the film is also announced by Saqib Saleem

==Plot==
The plot is set in Chandigarh, where a wedding is about to take place at the Khullar House, and father Tej Khullar (Ram Kapoor) has bought a new Maruti ERTIGA car as a wedding gift for his daughter & would-be son-in-law. A few days before the wedding, his college going son, Sameer (Saqib Saleem) who the father thinks is good for nothing, secretly takes the car out to impress the hottest girl in college, Jasleen (Rhea Chakraborty). After dropping Jasleen at her hostel and in a drunk state, he loses the car by accidentally giving the keys to a person who Sameer thinks is a valet. As soon as he realises that the valet doesnt have the keys and he had lost his car, he and his best friend Gattu (Prabal Panjabi) go on an all night search to find the car.

The next day they get an Ertiga model (test drive) to the house to show Sameers father that the car was still in the garage. They also meet a car dealer Pathan (Ravi Kishen) and agree to purchase a stolen Ertiga from him. Pathan  promises them to give the car the very next day. To ensure that there is a car in the garage in the night, they take another Ertiga on rent from a Jat family and bring it home. The day before the wedding, Sameer returns Ertiga and in the night goes to meet Pathan along with Gattu and Jasleen. While at Pathans place, a police raid happens and the three barely manage to escape. Pathans assistant calls Sameer telling him to take the car but its a trap set by the police and Sameer ends up in jail.

On the morning of the wedding, Sameer explains to the cops what happened but the cops insist on meeting his father. Sameer informs his sister who sends her fiancee to bail out Sameer. While this is happening, a cop informs the police control room that an Ertiga is being stolen by a gang using a tow truck. A chase ensues and police catch the thieves and hand the car back to Sameer.

After the wedding ceremony, Sameer manages to get the car back decorated and behaves as nothing has happened. But a few minutes later, Gattu brings another Ertiga to Sameers home and so does Jasleen, leaving everyone shocked and confused as there are three Ertigas.
Sameer then confesses to his father in the presence of everyone what happened over the past few days. His dad loses his temper, chides him but then forgives him and all are happy.

The audience comes to know that Gattu had won a contest where the 1st prize was a Maruti Ertiga and since Jazleen had saved a Jat family members life by taking him to the hospital on time, the Jat family gifts the car to her in gratitude. 

==Cast==
* Saqib Saleem as Sameer
* Rhea Chakraborty as Jasleen
* Ram Kapoor as Tej (Sameers father)
* Prabal Panjabi as Gattu
* Ravi Kishan as Pathan
* Benazir Shaikh as Tanvi
* Karam Rajpal as Daljeet

==Music== Sachin Gupta with lyrics penned by Kumaar and Anvita Dutt Guptan. The film also has a second music album which includes only background scores and is one of the first to have two such albums. 

{{track listing
| extra_column = Singer(s)
| title1 = Punjabiyan Di Battery
| extra1 = Mika Singh & Yo Yo Honey Singh
| length1 =
| title2 = Main Senti Hoon
| extra2 = Jaspreet Jasz & Shalmali Kholgade
| length2 =
| title3 = Haaay
| extra3 = Panjabi MC
| length3 =
| title4 = Hip Hip Hurrah
| extra4 = Sonu Kakkar
| length4 = 
| title5 = Mere Dad Ka Mash Up
| extra5 = DJ Shadow, DJ Raamji
| length5 = 
| title6 = Mere Dad Ki Maruti
| note6 = 
| extra6 = Diljit Dosanjh, Sachin Gupta
| length6 = 
}}

==References==
 

==External links==
*  
*  

 
 
 
 
 