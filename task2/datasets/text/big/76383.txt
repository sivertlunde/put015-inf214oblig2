The Ten Commandments (1956 film)
 
{{Infobox film
| name           = The Ten Commandments
| image          = 10Command56.jpg
| image_size     = 220px
| border         = yes
| alt            = 
| caption        = Original theatrical release poster
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille Henry Wilcoxon  
| screenplay     = Æneas MacKenzie Jesse L. Lasky, Jr. Jack Gariss Fredric M. Frank The Holy Scriptures
| narrator       = Cecil B. DeMille
| starring       = Charlton Heston Yul Brynner Anne Baxter Edward G. Robinson Yvonne De Carlo Debra Paget John Derek
| music          = Elmer Bernstein
| cinematography = Loyal Griggs,  
| editing        = Anne Bauchens
| studio         = Motion Picture Associates
| distributor    = Paramount Pictures
| released       =  
| runtime        = 220 minutes   
| country        = United States
| language       = English
| budget         = $13 million 
| gross          = $122.7 million   
}} religious epic biblical story Egyptian prince Sir Cedric Hardwicke as Seti I|Sethi, Nina Foch as Bithiah, Martha Scott as Jochebed|Yochabel, Judith Anderson as Memnet, and Vincent Price as Baka, among others.
 1923 silent film of the same title, and features one of the largest sets ever created for a film.  At the time of its release on November 8, 1956, it was the most expensive film made. 
 Best Picture, Best Performance The King film of seventh most adjusted for inflation.
 greatest films Ten Top Ten"—the best ten films in ten American film genres—after polling over 1,500 people from the creative community. The film was listed as the tenth best film in the epic genre.  

==Plot== Rameses I Egypt has Hebrew males Yochabel saves her infant son by setting him adrift in a basket on the Nile. Bithiah, the Pharaohs daughter, who had recently lost her husband and the hope of ever having children of her own, finds the basket and decides to adopt the boy even though her servant, Memnet, recognizes the child is Hebrew and protests.
 Hebrew God.

Moses institutes numerous reforms concerning the treatment of the slaves on the project, and eventually Prince Ramesses II|Rameses, Mosess "brother", charges him with planning an insurrection, pointing out that the slaves are calling him the "Deliverer". Moses defends himself against the charges, arguing that he is simply making his workers more productive by making them stronger and happier and proves his point with the impressive progress he is making. Rameses is later charged by Sethi with finding out whether there really is a Hebrew fitting the description of the Deliverer.

  learns from Memnet that Moses is the son of Hebrew slaves]]
Nefretiri learns from Memnet that Moses is the son of Hebrew slaves. Nefretiri kills Memnet but reveals the story to Moses only after he finds the piece of Levite cloth he was wrapped in as a baby, which Memnet had kept. Moses goes to Bithiah to learn the truth. Bithiah evades his questions, but Moses follows her to the home of Yochabel and thus learns the truth, while also meeting his true brother, Aaron, and sister, Miriam.

Although Moses feels no real change from this revelation, he spends time working amongst the slaves to learn more of their lives. Nefretiri urges him to return to the palace to help his people when he becomes pharaoh, to which he agrees after he completes a final task. The master builder Baka steals Lilia, who is engaged to the stonecutter Joshua. Joshua rescues Lilia by causing a commotion in Bakas stables. Joshua strikes Baka in the process and gets captured; he is then whipped by Baka for this insult.  Moses strangles Baka and frees Joshua, confessing to Joshua that he too is Hebrew.  The confession is witnessed by the ambitious Hebrew chief overseer Dathan. In exchange for his freedom, riches and Lilia, Dathan tells about this to Rameses, who then arrests Moses. Brought in chains before Sethi, Moses explains that he is not the Deliverer, but would free the slaves if he could. Bithiah tells her brother Sethi the truth about Moses, and Sethi reluctantly orders his name stricken from all records and monuments, and Rameses is declared the next Pharaoh. Rameses, well aware of his fathers (and Nefretiris ) devotion to Moses, decides not to execute him. He instead banishes Moses to the desert, where Nefretiri will never know if he survives, or perhaps finds another love. He also tells him that Yochabel had died after delivering a robe of Levite cloth for Moses.
 Sephora in the land of Midian]]
Moses makes his way across the desert, nearly dying of hunger and thirst before he comes to a well in the land of Midian. At the well, he defends seven sisters from Amalekites who try to push them away from the water. Moses finds a home in Midian with the girls father Jethro (Bible)|Jethro, a Bedouin sheik, who reveals that he is a follower of "He who has no name", whom Moses recognizes as the God of Abraham. Moses impresses Jethro and the other sheiks with his wise and just trading, and marries Jethros eldest daughter Zipporah|Sephora.

While herding sheep in the desert Moses finds Joshua, who has escaped from the copper mines of Ezion-Geber that he was sent to after the death of Baka. Moses sees the burning bush on the summit of Mount Sinai and hears the voice of God. God charges Moses to return to Egypt and free His chosen people. In the meantime, in Egypt, Sethi dies, his last word being Mosess name. Before he dies, he hands over the reins to his son Ramseses, who becomes Rameses II.
 Jannes does the same with his staves, but Moses snake devours his. Rameses decrees that the Hebrews be given no straw to make their bricks, but to make the same tally as before on pain of death. As the Hebrews prepare to stone Moses in anger, Nefretiris retinue rescues him. He spurns her when she attempts to renew her relationship with him by saying that he is on a mission and is also married.
 exodus from Egypt with Dathan, reluctantly, also among them.

Rameses spends the next three days begging Seker to call life back into the body of his son. Nefretiri goads him into such a rage that he arms himself and gathers the elite Egyptian forces and pursues the former slaves to the shore of the Red Sea. When the people see the Egyptian troops heading for them, they beg Moses to save them. With Gods help, he puts out a pillar of fire. Held back by this pillar, the Egyptian forces helplessly watch as Moses parts the waters. As the Hebrews race over the seabed, the pillar of fire then dies down and the army follows them in hot pursuit. The Hebrews make it to the far shore as the waters close on the Egyptian army, drowning every man and horse, except Rameses, who looks on in despair. All he can do is return to Nefretiri, confessing to her, "His god is God".

The former slaves camp at the foot of Sinai and wait as Moses again ascends the mountain with Joshua. During his absence, the Hebrews lose faith. Urged by Dathan, they build a golden calf as an idol to take back to Egypt, hoping to win Rameses forgiveness. They force Aaron to help fashion the gold plating. The people indulge their most wanton desires in an orgy of sinfulness, except for a few still loyal to Moses, including Sephora and Bithiah. 
 

High atop the mountain, Moses witnesses Gods creation of the stone tablets containing the Ten Commandments. When Moses finally climbs down and meets Joshua, they both behold their peoples iniquity. Moses hurls the tablets at the idol in a rage. The idol explodes, and Dathan and his followers are killed. After God forces them to endure forty years exile in the desert to kill off the rebellious generation, the Hebrews are about to arrive in the land of Canaan. An elderly Moses, who is not allowed to enter the promised land, because of his disobedience to God at the waters of Strife,   appoints Joshua to succeed him as leader, says a final good bye to Sephora, and goes forth to his destiny.

==Cast==
 
* Charlton Heston as Moses Rameses II Nefretiri
* Edward G. Robinson as Dathan Sephora
* Debra Paget as Lilia
* John Derek as Joshua Sir Cedric Sethi
* Nina Foch as Bithiah Yochabel
* Judith Anderson as Memnet
* Vincent Price as Baka
* John Carradine as Aaron
* Olive Deering as Miriam
* Babette Bain as Little Miriam  Jannes
* Frank de Kova as Abiram
* Henry Wilcoxon as Pentaur Jethro
* Donald Curtis as Mered Hur Ben Caleb
* H. B. Warner as Amminadab
* Julia Faye as Elisheba Lisa Mitchell as Jethros daughter (Lulua)
* Noelle Williams as Jethros daughter
* Joanna Merlin as Jethros daughter 
* Pat Richard as Jethros daughter
* Joyce Vanderveen as Jethros daughter
* Diane Hall as Jethros daughter
* Abbas El Boughdadly as Rameses Charioteer Fraser Heston as The Infant Moses
* John Miljan as The Blind One Francis J. McDonald as Simon Rameses I
* Paul De Rolf as Eleazar Woodrow Strode as King of Ethiopia
* Tommy Duran as Gershom
* Eugene Mazzola as Amun-her-khepeshef|Rameses Son
* Ramsay Hill as Korah
* Joan Woodbury as Korahs Wife Princess Tharbis
 

==Production==

===Writing===
The final shooting script was written by Æneas MacKenzie, Jesse L. Lasky, Jr., Jack Gariss, and Fredric M. Frank.  It also contained material from the books Prince of Egypt by Dorothy Clarke Wilson, Pillar of Fire by Joseph Holt Ingraham, and On Eagles Wings by Arthur Eustace Southon.  Henry Noerdlinger, the films researcher, consulted ancient historical texts such as the Midrash Rabbah, Philos Life of Moses, and the writings of Josephus and Eusebius in order to "fill in" the missing years of Moses life,  and as the films last opening title card states, "the Holy Scriptures."

The expression "the son of your body" for a biological offspring is based on inscriptions found in Mehus tomb. 

===Casting=== New Kingdom, Old Kingdom Fraser (born February 12, 1955), appeared as the infant Moses and was three months old during filming. 
 Joan Evans, Jane Griffiths, Audrey Hepburn, Jean Marie, Vivien Leigh, Jane Russell, and Joan Taylor were considered for the part.  DeMille liked Audrey Hepburn but dismissed her because of her figure, which was considered too slim for the characters Egyptian gowns.  Anne Baxter (who was considered for the part of Moses wife) was cast in the role. 
 Judith Ames, Laura Elliot, Rhonda Fleming, Rita Gam, Grace Kelly, Jacqueline Green, Barbara Hale, Allison Hayes, Frances Lansing, Patricia Neal, Marie Palmer, Jean Peters, Ruth Roman, Barbara Rush, and Elizabeth Sellers were considered for the part of Zipporah|Sephora.  Grace Kelly, DeMilles first choice, was unavailable.  DeMille was "very much impressed" with Yvonne De Carlos performance as a "saintly type of woman" in Metro-Goldwyn-Mayer|MGMs Sombrero (film)|Sombrero.       He "sensed in her a depth, an emotional power, a womanly strength which the part of Sephora needed and which she gave it."  Sephora is the Douay–Rheims Bible|Douay–Rheims version of the name of Zipporah. 

Merle Oberon and Claudette Colbert were considered for the role of Bithiah before DeMille chose Jayne Meadows (who declined) and finally cast Nina Foch, on the suggestion of Henry Wilcoxon, who had worked with her in Scaramouche (1952 film)|Scaramouche. 

For the role of Memnet, Flora Robson was considered and Bette Davis was interviewed (DeMilles casting journal also notes Marjorie Rambeau and Marie Windsor)  but DeMille chose Judith Anderson after screening Alfred Hitchcocks Rebecca (1940 film)|Rebecca.) 
 The Egyptian,  a rival production at the time.  Several exceptions to this are the casting of John Carradine and Mimi Gibson (in credited supporting roles) and Michael Ansara and Peter Coe (in uncredited minor roles), who appeared in both films.

===Art direction===
  Samson and David and Bathsheba, and others.

Jesse Lasky Jr., a co-writer on The Ten Commandments, described how DeMille would customarily spread out prints of paintings by  , demonstrated the LDS manner of performing such ordinations, and DeMille liked it.

Pharaoh is usually shown wearing the Pschent|red-and-white crown of Upper and Lower Egypt or the nemes royal headdress. For his pursuit of the Israelites, he wears the blue Khepresh helmet-crown, which the pharaohs wore for battle.
 The Egyptian were bought and re-used for The Ten Commandments. As the events in The Egyptian take place 70 years before the reign of Rameses II, an unintentional sense of continuity was created.

An Egyptian wall painting was also the source for the lively dance performed by a circle of young women at Setis birthday gala. Their movements and costumes are based on art from the Tomb of the Sixth Dynasty Grand Vizier Mehu. 

Some of the films cast members, such as Anne Baxter|Baxter, Debra Paget|Paget, John Derek|Derek, and Nina Foch|Foch, wore brown contact lenses, at the behest of DeMille, in order to conceal their light-colored eyes which were considered inadequate for their roles.    Paget once said that, "If it hadnt been for the lenses I wouldnt have got the part."  However, she also said that the lenses were "awful to work in because the Kleig lights heat them up".  When DeMille cast Yvonne De Carlo as Zipporah|Sephora, she was worried about having to wear these contact lenses; she also believed that her gray eyes were her best feature. Katherine Orrisons audio commentary for The Ten Commandments 50th Anniversary Collection DVD (2006)  She asked DeMille to make an exception for her. He agreed, expressing the idea that De Carlos role was special, and that Moses was to fall in love with her. 

===Special effects===
The special photographic effects in The Ten Commandments were created by   filming, and combined scenes shot on the shores of the Red Sea in Egypt, with scenes filmed at Paramount Studios in Hollywood of a huge water tank split by a U-shaped trough, into which approximately 360,000 gallons of water were released from the sides, as well as the filming of a giant waterfall also built on the Paramount backlot to create the effect of the walls of the parted sea out of the turbulent backwash.  All of the multiple elements of the shot were then combined in Paul Lerpaes optical printer, and matte paintings of rocks by Jan Domela concealed the matte lines between the real elements and the special effects elements.  Unlike the technique used by ILM for Raiders of the Lost Ark and Poltergeist (1982 film)|Poltergeist of injecting poster paints into a glass tank containing a salt water inversion layer, the cloud effects for the The Ten Commandments were formed with white Britt smoke filmed against a translucent sky backing, and colors were added optically. Mandell, Paul R. (April 1983) "Parting the Red Sea (and Other Miracles)". American Cinematographer, pp. 125-126.  Striking portraits of Charlton Heston as Moses and three women in front of menacing clouds were photographed by Wallace Kelly, A.S.C. in Farciot Edouart’s process (rear projection) department, in what are still considered unforgettable scenes.  DeMille used these scenes to break up the montage, framing his subjects like a Renaissance master.  An abundance of blue screen spillage or "bleeding" can be seen, particularly at the top of the superimposed walls of water, but rather than detracting from the shot, this (unintentionally) gives the scene an eerie yet spectacular appearance. The parting of the Red Sea sequence is considered by many to be one of the greatest special effects of all time. 
 parting of the Red Sea. It was eventually revealed that footage of the Red Sea was spliced with film footage (run in reverse) of water pouring from large U-shaped trip-tanks set up in the studio backlot.       

==Release==
{{multiple image
 
| align     = right 
| direction = vertical
| width     = 150

 
| image1    = Charlton Heston and Yul Brynner at the New York premiere of The Ten Commandments.JPG
| width1    = 150
| alt1      = 
| caption1  = Charlton Heston and Yul Brynner at the New York premiere

 
| image2    = Anne Baxter at the New York premiere of The Ten Commandments.JPG
| width2    = 150
| alt2      = 
| caption2  = Anne Baxter at the New York premiere

 
| image3    = Yvonne De Carlo and Bob Morgan at the New York premiere of The Ten Commandments.JPG
| width3    = 150
| alt3      = 
| caption3  = Yvonne De Carlo and Bob Morgan, her husband, at the New York premiere

 
| header            = 
| header_align      =  
| header_background = 
| footer            = 
| footer_align      =  
| footer_background = 
| background color  = 
}} premiered at roadshow basis with reserved seating until mid-1958, when it finally entered general release.    It was re-released in 1966 and 1972, and one more time in 1990 with a restored print. 

==Reception==

===Box office=== theater rentals (the distributors share of the box office gross) of $31.3 million in North America and $23.9 million from the foreign markets, for a total of $55.2 million (equating to approximately $122.7 million in ticket sales).  It was hugely profitable for its era, earning a net profit of $18,500,000,  against a production budget of $13.27 million (the most a film had cost up to that point). Reported budgets:
*  . "... a record $13,266,491".
*  . "$13,272,381". 
 Gone with the Wind at the box office in the North American territory,  and mounted a serious challenge in the global market—the worldwide takings for Gone with the Wind were reported to stand at $59 million at the time.    Gone with the Wind would be re-released the following year as part of the American Civil War Centennial, and reasserted its supremacy at the box office by reclaiming the US record.  Also at this time, Ben-Hur (1959 film)|Ben-Hur—another biblical epic starring Charlton Heston released at the end of 1959—would go on to eclipse The Ten Commandments at the box office.   A 1966 reissue earned $6,000,000,    and further re-releases brought the total American theater rentals to $43 million,  equivalent to gross ticket sales of $89 million at the box office.  Globally, it ultimately collected $90,066,230 in revenues up to 1979. 
 The Sound of Music (1965), and E.T. the Extra-Terrestrial (1982) have generated higher grosses in constant dollars. 

===Critical response===
{{Quote box class =   title =  quote = As Mr. DeMille presents it in this three-hour-and-thirty-nine-minute film, which is by far the largest and most expensive that he has ever made, it is a moving story of the spirit of freedom rising in a man, under the divine inspiration of his Maker. And, as such, it strikes a ringing note  today. source = Bosley Crowther for The New York Times    align = left width = 30% border =  fontsize =  bgcolor =  style =  title_bg =  title_fnt =  tstyle =  qalign =  qstyle =  quoted =  salign =  sstyle = 
}}
The Ten Commandments received generally positive reviews after its release, although some reviewers noted its divergence from the biblical text. Bosley Crowther for The New York Times was among those who lauded DeMilles work, acknowledging that "in its remarkable settings and décor, including an overwhelming facade of the Egyptian city from which the Exodus begins, and in the glowing Technicolor in which the picture is filmed&mdash;Mr. DeMille has worked photographic wonders."  Variety (magazine)|Variety described the "scenes of the greatness that was Egypt, and Hebrews by the thousands under the whip of the taskmasters" as "striking," and believed that the film "hits the peak of beauty with a sequence that is unelaborate, this being the Passover supper wherein Moses is shown with his family while the shadow of death falls on Egyptian first-borns."   

The films cast was also complimented. Variety called Charlton Heston an "adaptable performer" who, as Moses, reveals "inner glow as he is called by God to remove the chains of slavery that hold his people."  It considered Yul Brynner "expert" as Rameses, too.  Anne Baxters performance as Nefretiri was criticized by Variety as leaning "close to old-school siren histrionics,"  but Crowther believed that it, along with Brynners, is "unquestionably apt and complementary to a lusty and melodramatic romance."  The performances of Yvonne De Carlo and John Derek were acclaimed by Crowther as "notably good."  He also commended the films "large cast of characters" as "very good, from Sir Cedric Hardwicke as a droll and urbane Pharaoh to Edward G. Robinson as a treacherous overlord." 

Leonard Maltin, a contemporary film critic, gave the film four out of four stars and described it as "vivid storytelling at its best... parting of the Red Sea, writing of the holy tablets are unforgettable highlights."   

Rotten Tomatoes retrospectively collected 32 reviews and gave the film a rating of 91%, with the sites consensus stating: "Bombastic and occasionally silly but extravagantly entertaining, Cecil B. DeMilles all-star spectacular is a muscular retelling of the great Bible story." 

===Accolades=== Academy Award Best Color Best Color Best Color John Jensen, Best Film Best Motion Best Sound Recording (Paramount Studio Sound Department and sound director Loren L. Ryder).  Paramount submitted the names of Yvonne De Carlo, John Derek, and Debra Paget for the supporting player categories (even though they received star billing in the film) at the 29th Academy Awards,    but the actors did not receive nominations.
 The Film Best Performance by an Actor in a Motion Picture - Drama    and later won the Fotograma de Plata Award for Best Foreign Performer in 1959.   

Yul Brynner won the National Board of Review Award for Best Actor for his performance as Ramesses II|Rameses. 

Cecil B. DeMille won many special awards for the film. He received, among others, the Los Angeles Herald-Examiner|Los Angeles Examiner Award,    the Boxoffice (magazine)|Boxoffice Blue Ribbon Award for the Best Picture of the Month (January 1957),  the Photoplay Achievement Award,  and The Christian Heralds Readers Award for the Picture of the Year (1957). 
 Sir Cedric Hardwicke, Nina Foch, and Martha Scott also received awards for their acting.       

The film was also included in several of the annual top ten film lists, such as those featured in The Film Daily and Photoplay. 

;American Film Institute recognition
*AFIs 10 Top 10 &ndash; #10 Epic film
*AFIs 100 Years...100 Movies (10th Anniversary Edition) &ndash; Nominated
*AFIs 100 Years...100 Cheers &ndash; #79
*AFIs 100 Years of Film Scores &ndash; Nominated
*AFIs 100 Years...100 Movie Quotes:
**"Oh, Moses, Moses, you stubborn, splendid, adorable fool!" &ndash; Nominated
*AFIs 100 Years...100 Heroes and Villains:
**Moses &ndash; #43 Hero
*AFIs 100 Years...100 Thrills &ndash; Nominated
*AFIs 100 Years...100 Movies &ndash; Nominated

==Popularity==
Critics have argued that considerable liberties were taken with the biblical story of Exodus, compromising the films claim to authenticity, but neither this nor its nearly four-hour length has had any effect on its popularity. In fact, many of the supposed inaccuracies were actually adopted by DeMille from extra-biblical ancient sources, such as Josephus, the Sepher ha-Yashar, and the Chronicle of Moses. Mosess career in Ethiopia, for instance, is based on ancient midrashim.  For decades, a showing of The Ten Commandments was a popular fundraiser among revivalist Christian Churches, while the film was equally treasured by film buffs for DeMilles "cast of thousands" approach and the heroic but antiquated early-talkie-type acting.

==Home media==
 
The Ten Commandments has been released on  ,    the third edition (50th Anniversary Collection) was released on March 21, 2006 as a three-disc set with the 1923 version and special features,    and the fourth edition (55th Anniversary Edition) was released on DVD again in a two-disc set on March 29, 2011 and for the first time on Blu-ray Disc|Blu-ray in a two-disc set and a six-disc limited edition gift set with the 1923 version and DVD copies.  In 2012, the limited edition gift set won the Home Media Award for Best Packaging (Paramount Pictures and JohnsByrne).   

==Television broadcast== ABC network Easter holidays, and for much of that period ABC has aired it on Easter Sunday. 

Unlike many lengthy films of the day, which were usually broken up into separate airings over at least two nights, ABC elected to show the entire film in one night and has done so every year it has carried The Ten Commandments with one exception; in 1997, ABC elected to split the movie in two and aired half of it in its normal Easter Sunday slot, which that year was March 30, with the second half airing as counterprogramming to the other network offerings on April 1, which included CBS coverage of the NCAA Mens College Basketball National Championship Game. 
 Central and Mountain zones often air the film live with the Eastern time zone feed to keep their late local news as close to its regular time as possible, since both areas Big Three affiliates air their late news at 10:00 pm local time.

In more recent years, ABC chose to air other programming on Easter night and instead aired The Ten Commandments the night before as part of its Saturday night lineup, with the broadcast starting at 8:00 pm Eastern. In 2015, for the first time in several years, the network returned to airing the film on Easter Sunday night, which fell on April 5. 
 high definition for the first time, which allowed the television audience to see it in its original VistaVision aspect ratio.

;Ratings by year (between 2007 and 2014)
{| class="sortable wikitable" style="text-align:center;"
|-
! Year
! Airdate
! Rating
! Share
! Nielsen ratings|Rating/Share (18–49)
! Viewers (millions)
! Rank (timeslot)
! Rank (night)
|-
|align=left| 2007
|align=left| April 7, 2007 TBA
| 7.87
| TBA
| TBA
| TBA
| TBA
|-
|align=left| 2008
|align=left| March 22, 2008
| 4.7
| 9
| 2.3/7
| 7.91
| 1
| 1
|-
|align=left| 2009
|align=left| April 11, 2009
| 4.2
| 8
| 1.7/6
| 6.81
| 1
| 1
|-
| align="left" | 2010 
|align=left| April 3, 2010 TBA
| TBA
| 1.4/5
| 5.88
| 2
| 3
|-
| align="left" | 2011 
|align=left| April 23, 2011
| rowspan=2|1.6/5
| 7.05
| 1
| 1
|-
|align=left| 2012   
|align=left| April 7, 2012
|6.90 TBA
|TBA
|-
| align="left" | 2013 
|align=left| March 30, 2013
| 1.2/4
| 5.90
| 2
| 2
|-
| align="left" | 2014 
|align=left| April 19, 2014
| 1.0/4
| 5.87
| 1
| 1
|-
| align="left" | 2015 
|align=left| April 5, 2015
| 1.4/5
| 6.80
| TBA
| TBA
|}

==See also==
* The Ten Commandments (1923 film)|The Ten Commandments (1923 film)
* List of films featuring slavery
* Van Orden v. Perry

==References==
 

;Bibliography
*  
*  
*  
*  
*  
*  
*  
*  

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 