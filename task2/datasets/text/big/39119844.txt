Night of the Living Carrots
{{Infobox film
| name           = Night of the Living Carrots
| image          = Night of the Living Carrots poster.jpg
| caption        = 
| director       = Robert Porter
| producer       = Lisa Stewart
| writer         = Bill Riling
| story          = 
| based on       = 
| starring       = Seth Rogen David Kaye Hugh Laurie Kiefer Sutherland
| music          = Matthew Margeson
| cinematography =
| editing        = Bret Marnell
| studio         = DreamWorks Animation
| distributor    = Nintendo Video
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| italic title   = force
}}
Night of the Living Carrots is a 2011 Halloween short animated film, based on  , a mutated carrot has spawned hundreds of zombie carrots taking control of the subjects mind. Dr. Cockroach determines that the only way to defeat them and free their victims is for B.O.B. to eat all of the carrots.
 video service    - the first was released on October 13, 2011,  and the second five days later.  To a general audience it was released on August 28, 2012, as a part of the Shreks Thrilling Tales DVD    and DreamWorks Spooky Stories Blu-ray.   

==Plot==
 
In a theater, B.O.B. introduces the story in a manner parodying that of typical horror films, beginning with a recap of the events of Mutant Pumpkins from Outer Space, and tells the audience to be prepared for a story guaranteed to give them nightmares.

The scene then shifts to the twist ending of the previous special. The Zombie Carrot emerges and charges at the camera but is stopped short by a gate slamming into it. Carl Murphy announces to the children of the Modesto suburbs that a costume contest was about to start and that the winner got their weight in candy. B.O.B., dressed as a pirate, takes interest and comes inside but takes all the candy meant for the contest. Outside, he hears a strange voice and is initially frightened by the zombie carrot, but he mistakes it for a child in a costume (saying that he thought the zombie carrot was a real carrot). Believing the carrot would win the costume contest, he throws it inside where it immediately bites Carl, turning him into a zombie.

All the guests flee the Murphy house and not long after, the carrot is blasted by Dr. Cockroachs scanner. Carl then snaps out of his zombie state, unsure of what went on. Doc theorizes that the carrot was contaminated by the mutant pumpkins and that the curse could only be lifted by eliminating the infected carrot. However, the remains of the carrot replicate themselves into more zombie carrots, reinfecting Carl in the process. Going against Docs advice, B.O.B. uses the scanner to blast the carrots and before long, all three monsters are completely surrounded.

At that moment, General Monger arrives but he too falls victim to the zombie carrots. Doc, Link and B.O.B. retreat inside the house to create a barricade. B.O.B.s bungling leads to Missing Link getting infected and both Doc and B.O.B. retreat to Susans old room. There, a stray zombie carrot seemingly infects B.O.B. leaving only Doc. B.O.B. however is seen conversing with a lava lamp and Doc realises that B.O.B. was immune, specifically because B.O.B. has no brain. Doc then theorizes that the only way to save the day is that B.O.B. must eat all the carrots.

This causes B.O.B. to have a flashback revealing why he has a fear of carrots; when he was young, B.O.B. was force-fed carrot puree by General Monger. Back in the present, the infected Link and the zombie carrots break through the door and Doc, also infected, rises menacingly. B.O.B. is unwilling to eat the carrots and escapes through the window to the roof, taking four stuffed toys with him as "new friends" (one of which is an Alex toy from Madagascar). But when he sees the zombified Monger, Doc and Link blundering about, he willingly decides to eat the zombie carrots.

Before long, B.O.B. eats every zombie carrot, rendering him grotesquely obese. He is about to eat a "Nutty Buddy Butter Bar" (a parody of Butterfinger) for dessert when he sees that Monger, Link and Doc are still zombies. B.O.B. is puzzled why they are still in that state when he sees the original zombie carrot appear. B.O.B. is unable to pursue it due to the weight hed put on. He does however see the zombie carrot picking up the Nutty Buddy Butter Bar and B.O.B. warns the carrot not to open it. The carrot does open it however, causing B.O.B. to chase down on the carrot like a seal, the carrot ending up in B.O.B.s mouth as a "chocolate-covered carrot." Link, Monger and Doc snap out of their zombie-like states, the latter congratulating B.O.B. for breaking the curse. They all hug B.O.B. causing him to burp out an orange cloud.

Back in the theater, B.O.B. ends the story saying that he saved the day because "those silly little carrots had absolutely no effect on me." He reminds the audience, "eat your veggies, or they just might eat you" before leaving the stage but soon after his shadow on the curtain morphs into one of a zombie carrot.

==Cast==
* Seth Rogen as B.O.B.
* Kiefer Sutherland as General W.R. Monger James Horan as Dr. Cockroach
* David Kaye as The Missing Link
* Julie White as Wendy Murphy
* Jeffrey Tambor as Carl Murphy

==References==
 

==External links==
*  
*  

 
 
 

 
 
 