That Dangerous Age
 
 
{{Infobox film
| name           = That Dangerous Age If This Be Sin
| image          = That Dangerous Age - Film Poster.jpg
| image_size    = 225px
| caption        = United States Theatrical release poster
| director       = Gregory Ratoff 
| producer       = Gregory Ratoff
| writer         = 
| starring       = Myrna Loy 
| cinematography = 
| music          = 
| studio = 
| distribution   = British Lion Films
| released       = 5 September 1949
| runtime        = 
| country        = United Kingdom English
| gross = £176,577 (UK)  
}} British romance film directed by Gregory Ratoff and starring Myrna Loy, Roger Livesey and Peggy Cummins.  It is an adaptation of the play Autumn by Ilya Surguchev.   It was released as If This Be Sin in the United States. 

==Plot==
A lady on the Isle of Capri, neglected by a husband who works too much, strikes up a romance with another man.

==Cast==
* Myrna Loy - Lady Cathy Brooke
* Roger Livesey - Sir Brian Brooke
* Peggy Cummins - Monica Brooke
* Richard Greene - Michael Barcleigh
* Elizabeth Allan - Lady Sybil
* Gerard Heinz - Doctor Thorvald
* Jean Cadell - Nannie
* G.H. Mulcaster - Simmons
* Margaret Withers - May Drummond Ronald Adam - Prosecutor
* Wilfrid Hyde-White - Mr Potts
* Henry Caine - Mr Nyburg
* Patrick Waddington - Rosley
* Edith Sharpe - Angela Caine George Curzon - Selby Robert Atkins - George Drummond
* Phyllis Stanley - Jane
* Daphne Arthur - Margot
* Martin Case - John Barry Jones - Arnold Cane
* Louise Lord - Ellen
* Nicholas Bruce - Charles
* William Mervyn - Nicky

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 


 