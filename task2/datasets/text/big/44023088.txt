Hello Darling (1975 film)
{{Infobox film
| name           = Hello Darling
| image          =
| caption        =
| director       = AB Raj
| producer       = RS Sreenivasan
| writer         = VP Sarathy MR Joseph (dialogues)
| screenplay     = VP Sarathy
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Sankaradi
| music          = M. K. Arjunan
| cinematography = PB Mani
| editing        = BS Mani
| studio         = Sree Sai Productions
| distributor    = Sree Sai Productions
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by AB Raj and produced by RS Sreenivasan. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Sankaradi in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir as Venu
*Jayabharathi as Syamala
*Adoor Bhasi as Padmarajan
*Sankaradi as Pachu Pilla
*Mallika Sukumaran as Leela Meena as Kochunarayani
*Rani Chandra as sumithra Sudheer as Rajesh
*Sreelatha Namboothiri as Latha
*Bahadoor as Appukuttan
*Jagathy Sreekumar as Vijayan
*Alummoodan as Harshan Pilla
*Manavalan Joseph as Mahadevan
*Jose Prakash as Krishna Kumar
*Paravoor Bharathan as Sekhar Khadeeja as Kamalabhai
*Prathapachandran as Police officer
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Anuraagame Anuraagame || K. J. Yesudas || Vayalar ||
|-
| 2 || Bahar Se Koy || KP Brahmanandan, Sreelatha Namboothiri || Vayalar ||
|-
| 3 || Dwaarake || P Susheela || Vayalar ||
|-
| 4 || Kaattin Chilamboliyo || K. J. Yesudas || Vayalar ||
|-
| 5 || Nineteen Seventy Five || P. Madhuri || Vayalar ||
|-
| 6 || Nineteen Seventy Five || K. J. Yesudas || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 