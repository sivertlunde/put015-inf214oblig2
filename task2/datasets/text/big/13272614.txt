Salsa (1988 film)
{{Infobox Film
| name           = Salsa
| image          = Salsaposter.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Boaz Davidson
| producer       = Yoram Globus
| writer         = Boaz Davidson Eli Tavor Tomás Benitez Shepard Goldman Robi Rosa Rodney Harvey Michael Bishop Wojciech Kilar
| cinematography = David Gurfinkel
| editing        = Alain Jakubowicz
| distributor    = Cannon Film Distributors
| released       = May 7, 1988
| runtime        = 97 minutes
| country        =  
| language       = English
| budget         = $6 million
| gross          = $8,892,589
}}
 Puerto Rican salsa dancing Robi Rosa, Rodney Harvey, and Angela Alvarado. It earned a Razzie Award nomination for Rosa as Worst New Star.

==Synopsis==
In a nightly escape from his day job as a mechanic, Rico (Robi Rosa) enters his true element: the wild exuberance of the East L.A. "La Luna" salsa club. Dreaming of making himself and Vicky (Angela Alvarado), his girlfriend the "King and Queen of Salsa", Rico pours all his energy into winning La Lunas Grand Salsa Competition. But when Luna (Miranda Garrison), the clubs gorgeous owner sets her sights on making Rico her dance partner, Rico must decide what drives him, his ambition or his heart.

==Cast== Robi Rosa - Rico
*Rodney Harvey - Ken
*Angela Alvarado - Vicky
*Magali Alvarado - Rita
*Daniel Rojo - Orlando
*Moon Orona - Lola
*Humberto Ortiz - Beto
*Miranda Garrison - Luna
*Tito Puente - Himself
*Celia Cruz - Herself Wilkins - Himself
*Robert Gould - Boss
*Loyda Ramos - Mother
*Leroy Anderson - Himself
*Roxan Flores - Nena
*Deborah Chesher - Sister
*Kenny Ortega - Himself
*Joanna Garcia - Waitress
*Chain Reaction - Himself Marisela Esqueda - Herself
*Willie Colón - Himself
*Michael Sembello - Himself
*Mavis Vegas Davis - Himself
*Mongo Santamaría - Himself
*Charlie Palmieri - Himself Jellybean Benitez - Himself
*Constance Marie- dancer

==Soundtrack== Wilkins
# "Chicos y chicas" - Mavis Vegas Davis
# "Cali Pachanguero" - Grupo Niche
# "Your Love" - Laura Branigan Good Lovin" - Kenny Ortega, Chain Reaction, The Edwin Hawkins Singers
# "Under My Skin" - Robby Rosa
# "Oye Como Va" - Tito Puente I Know" - Marisela, The Edwin Hawkins Singers Spanish Harlem" - Ben E. King
# "Puerto Rico" - Bobby Caldwell, Marisela Esqueda, Michael Sembello, Wilkins Vélez|Wilkins, Mongo Santamaría, Charlie Palmieri, The Edwin Hawkins Singers

==External links==
*   at MGM.com
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 

 