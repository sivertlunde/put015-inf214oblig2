Our Friend, Martin
{{Infobox film
| name        = Our Friend, Martin VHS cover
| writer      = Dawn Comer (story) Chris Simmons (story) Sib Ventress Deborah Pratt
| director    = Rob Smiley  Vincenzo Trippetti
| producer    = Andy Boron Andy Heyward Phillip Jones Robby London Michael Maliani Judith Reilly Janice Sonski
| studio      = DIC Entertainment (original) DHX Media (DVD/Blu-ray)
| distributor = CBS/Fox Video (VHS) Shout! Factory (DVD/Blu-ray)
| released    =  
| country     = United States
| runtime     = 60 min.
| language    = English
}} 1999 animation|animated American civil rights movement. Two friends travel through time, meeting Dr. King at several points during his life. It featured an all-star voice cast and was nominated for an Emmy award in 1999 for Outstanding Animated Program (For Programming More Than One Hour). It marks John Travoltas voice acting debut. It was the final release under the CBS/Fox Video name.

==Plot== Caucasian boys Randy (Lucas Black) and Kyle (Zachary Leigh) and a Latino girl Maria (Jessica Garcia), visit a museum, dedicated to Martin Luther King, Jr.. Randy and Miles explore Martins bedroom, and are caught by the museums curator Mrs. Peck (Whoopi Goldberg), who winds up an old watch. The boys hold Martins baseball glove and the two are transported back to 1941 and encounter a 12 year old Martin (Theodore Borders) playing with his friends, Sam (Adam Wylie) and Skip Dale, until their mother (Ashley Judd) arrives and reprimands her sons for integrating with the "colored people|coloreds".  Martin explains to Miles and Randy that Mrs. Dales hatred of black people stems from the fact she regards them as "different", but violence would only worsen things. 
 waiting rooms. They later have dinner with Martins family and while he goes to do shut in rounds with his father (James Earl Jones), the boys travel forward 11 years and meet Martin (LeVar Burton), who by now is in his 20s and works as a minister at the church. He is holding a meeting about the Montgomery Bus Boycott, set off after Rosa Parks, a black seamstress refused to give up her seat on a bus and was put in prison for it. As a result, no black adults or children will ride the buses. Just then, Turner (Samuel L. Jackson), Martins friend alerts him his house is on fire. He races home, but fortunately his wife Coretta Scott King and newborn daughter Yolanda escape unscathed. 

Turner announces that in retaliation, they will attack the perpetrators with bricks, guns, Molotov cocktails and knives, but Martin stops him, reminding the crowd of Gandhi peacefully standing his ground to exile the British colonies from India and of Jesus teaching love for his enemies. Miles and Randy then travel to the Birmingham riot of 1963 and witness demonstrators squirt black protesters with hoses and set German Shepherds on them. The boys are later transported back to the museum and join their class back at school. The following day, Miles and Randy tell Miss Clark about the events prior to Martins work and later the class watch a DVD of National Geographics Inside WWII. After the class leaves, Maria and Kyle decide to investigate for themselves how Randy and Miles got the information. When the boys arrive at the museum, Mrs. Peck lets them stay but warns them that when one messes with the past, this can affect the present. 

Maria and Kyle follow the two boys in and catch them in Martins bedroom.  The four children are then transported to the March on Washington Movement and meets Martin who is in his 30s (Dexter Scott King) and a young Miss Clark, who at this time is a member of the movement and not yet married. When they return, Miles discovers that Martin was Assassination of Martin Luther King, Jr.|murdered. The children travel back to 1941 and bring the 12 year old Martin back to the present. This results in the world being different, i.e. Randy and Kyle are racist and no longer Miles friends, Maria works as a maid and cant speak English, his high school is segregated and named after Robert E. Lee and he and his mother (Angela Bassett) live in poverty, as she no longer has a job. Miles realizes the error of his ways and must sacrifice his plans to rescue Martin. Miles bids a tearful farewell, but he realizes he still has Martins watch and begs for him to come out of his house. Martin returns to his time, where he is shot and killed at his hotel. This results in the world reverting to normal and Miles receives an A+ on his history test, allowing him to progress to 7th grade. He and his friends then vow to continue Martins work. At the end of the film, Mrs. Peck closes the door to Martins bedroom as the credits roll.

==Production==
While the film was released on VHS, and with co-distribution rights with CBS/Fox Video (which in turn was also distributed by 20th Century Fox Home Entertainment and CBS), the film was also distributed by DiC Entertainment. Andy Heyward, Michael Maliani, and Robby London assumed the position to producing the series, and making a deal with 20th Century Fox Home Entertainment & CBS/Fox Video to distribute the series.

==Main Roles==
Miles Woodman- A carefree, wise cracking 12-year-old, African-American baseball fan who is having trouble in school. His teacher Mrs. Clark is threatening to make him repeat 6th grade if he doesnt start raising up his grades for history class. During a class field trip to the Martin Luther King, Jr. museum, he and his best friend Randy discover that the museum holds magic. With the help of Martins old watch and a few of his belongings, he and Randy travel back in time throughout Martins life (from when he is 12 to about 34). In one of the later points of his time travel, they find a younger version of Mrs. Clark, who at that time was not yet married and involved in Martins movement. After his adventures with his friends, Miles learns that Martin was shot. To thwart this, he takes the young Martin into the present.

By taking young Martin out of his time, an alternate timeline has been created. As Martin was unable to do his Civil Rights work, Randy and Kyle are racists and are no longer friends with Miles, Maria can no longer speak to Miles in English and works as a maid, his parents no longer own a business and they are poor, and his middle school has become a segregated white school. Miles then must sacrifice his plans to save Martin. Young Martin goes back to his own time and thanks Miles for being his friend, leaving him the watch. Young Martin grows up and eventually dies as he did in the original timeline, the world returning to what it is today. Miles learns from this experience and gets an A+ on his report, earning him a promotion to Grade 7.

Randy Smith-  A 12-year-old Caucasian skater boy, who talks with a Dixie accent. Miless best friend and partner in his adventures through time.  In the alternate timeline, he and Kyle are best buds and pick on black kids like Miles.

Maria Ramirez (although on Mrs. Clarks computer her last name is Alverez)- A very intelligent 10-year-old Hispanic girl (she said she skipped two grades) who is a little bit of a snob, but is the first to realize that even though they are different, she, Miles, Randy and Kyle can all be friends. In the alternate timeline, she is unable to speak English and works as a maid, scrubbing the floors of the school. The principal comments on her and the other Hispanic maid, saying that "their kind" are lazy enough without being bothered.

Kyle Langon- A Caucasian 13-year-old bully of the story, but later a friend of Miles, Randy and Maria. He often wears shirts that are small and tear easily. In the alternate timeline, he is a racist bully to black kids and is best friends with Randy. He get his "lousy" attitude from his dad. By accidentally getting grabbed into one of the time travel adventures, he sees firsthand the effects of bigotry and resolves to turn over a new leaf.

==Cast==
* Edward Asner - Mr. Harris
* Angela Bassett - Miles mother  
* Lucas Black - Randy  
* Theodore Borders - Martin at age 12  
* LeVar Burton - Martin at age 26 
* Jessica Garcia - Maria  
* Danny Glover - Train Conductor  
* Whoopi Goldberg - Mrs. Peck  
* Samuel L. Jackson - Turner 
* James Earl Jones - Daddy King  
* Ashley Judd- Mrs. Dale  
* Richard Kind - Mr. Willis  
* Dexter King - Martin at age 34  
* Yolanda King - Christine King  
* Zachary Leigh - Kyle 
* Robert Richard - Miles
* Susan Sarandon - Mrs. Clark  
* John Travolta - Kyles father 
* Jaleel White - Martin at age 15  
* Adam Wylie - Sam Dale 
* Oprah Winfrey - Coretta Scott King   Chihuahua 
* Nicole Palacio - Parker Marie
* Jess Harnell - Reporter #1/Demonstrator 
* Joe Lala - Reporter #2/Demonstrator  
* John Wesley - Man/Demonstrator  
* Elizabeth Primm - Old Woman/Demonstrator  
* H.P Shimono - Additional Voices.

==Soundtrack==
Motown Records released a soundtrack from this film that featured a cover of "Aint No Mountain High Enough" by Debelah Morgan, which combined the Marvin Gaye/Tammi Terrell and Diana Ross versions.

==See also==
* African-American Civil Rights Movement in popular culture

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 