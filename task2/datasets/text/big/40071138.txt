Get That Man
{{Infobox film
| name           = Get That Man
| image	         = get_that_man_poster.jpg
| caption        = DVD release poster
| director       = Spencer Gordon Bennet
| producer       = Spencer Gordon Bennet Lester F. Scott, Jr.
| writer         = Betty Burbridge
| narrator       = Leon Ames
| music          = Lee Zahler
| cinematography = James S. Brown, Jr.
| editing        = Ethel Davey
| distributor    =
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Get That Man is a 1935 American drama film directed by Spencer Gordon Bennet, from a screenplay by Betty Burbridge. It stars Wallace Ford as Jack Kirkland, a taxi driver who discovers he closely resembles of a murdered heir to a fortune. Ford also plays the murdered heir, John Prescott.

Due to a failure to renew copyright, it is now in the public domain. 

==Cast==
*Wallace Ford as Jack Kirkland/John Prescott
*Finis Barton as Diane Prescott
*E. Alyn Warren as Jay Malone Leon Ames as Don Malone
*Lillian Miles as Fay Prescott
*Laura Treadwell as Mrs. Prescott
*William Humphries as Mr. Brownlee
*Johnstone White as Mr. Joyce

==Release==
Originally released on July 11, 1935, Get That Man appears regularly on many public domain DVD compilations. It was released as a standalone disc on July 26, 2011 by Alpha Video.  

===Reception=== Hal Erickson graded Get That Man with two out of five stars.   

==References==
 

==External links==
* 
* 
*  

 
 
 
 
 
 

 