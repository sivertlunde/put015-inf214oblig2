Nothing but the Truth (2008 South African film)
 
 

{{Infobox film
| name           = Nothing but the Truth 
| image          = 
| caption        = 
| director       = John Kani
| producer       = Jazz Spirit Production, Odélion Films
| writer         = 
| starring       = John Kani, Rosie Motene, Motshabi Tyelele, Warona Seane, Esmeralda Bihl
| distributor    = 
| released       = 2008
| runtime        = 81
| country        = South Africa
| language       = 
| budget         = 
| gross          = 
| screenplay     = John Kani
| cinematography = Jimmy Rob, Marius Va Graan
| sound          = Rick Mc Namee
| editing        = Megan Gil, Jackie Le Cordeur
| music          = Neil Solomon
}}

Nothing but the Truth  is a 2008 film. The movie is adapted from a widely popular one-man show performed by actor and director John Kani.

== Synopsis ==
In New Brighton, South Africa, 63-year-old librarian Sipho Makhaya is getting ready to receive the body of his brother Themba, recently deceased while in exile in London and a hero of the Anti-Apartheid Movement. Nothing but the Truth investigates the contrast between those blacks who remained in South Africa and risked their lives to lead the fight against apartheid and those who returned victoriously after living in exile.

== Awards ==
* Écrans Noirs (Yaundé) 2009
* Fespaco (Uagadugú) 2009
* Festival de Cine de Harare 2009

== References ==
 
*  

 
 


 