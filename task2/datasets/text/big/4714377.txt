Sympathy for the Underdog
{{Infobox film
  | name           = Sympathy for the Underdog
  | image          = Sympathy for the Underdog Poster.jpg
  | caption        = Japanese release poster
  | director       = Kinji Fukasaku
  | producer       = Koji Shundo Toru Yoshida
  | writer         = Kinji Fukasaku Fumio Konami Hirō Matsuda
  | starring       = Koji Tsuruta Noboru Ando
  | music          = Takeo Yamashita
  | cinematography = Hanjiro Nakazawa
  | editing        = Osamu Tanaka Toei
  | released       = January 12, 1971
  | runtime        = 93 min
  | country        = Japan Japanese
  }}
Sympathy for the Underdog, known in Japan as  , is a 1971 Japanese yakuza film directed and co-written by Kinji Fukasaku and starring Koji Tsuruta and Noboru Ando. It is director Fukasakus (Battles Without Honor and Humanity, Battle Royale) last film featuring Koji Tsuruta. Complex (magazine)|Complex named it number 8 on their list of The 25 Best Yakuza Movies.  Home Vision Entertainment released the movie on DVD in North America in 2005. 

==Summary==
Gunji is a yakuza boss whose gang is driven out of Yokohama by a powerful rival from Tokyo. After serving ten years in prison, Gunji collects what is left of his loyal members in order to start over his small organization. However after setting up their new operation in Okinawa, the large yakuza organization from Tokyo that was responsible for their previous downfall and Gunjis imprisonment, comes to the island in a grand procession to gain control of the territory. This leads to a bloody and cold confrontation that is the climax of the film.

==Cast==
*Koji Tsuruta as Masuo Gunji
*Noboru Ando as Noburo Kudo
*Asao Koike as Ozaki
*Hideo Murota as Shark
*Harumi Sone as Gunshot
*Tsunehiko Watase as Susumu Seki
*Toru Yuri as Old Man
*Asao Uchida as Eisaku Oba
*Tadao Nakamura as Shigeru Kaizu
*Kaku Takashina as Kusakabe
*Rinishi Yamamoto as Hadelma
*Tomisaburo Wakayama as Yonabal
*Kenji Imai as Mad Dog Jiro
*Kenjiro Morokado as Gushken
*Akiko Kudo as Terumi

==Production==
Set and filmed in Okinawa, Sympathy for the Underdog has similarities to actual real-life events. It was not until several months after the film was released that America gave control of Okinawa back to the Japanese. But yakuza fled to the prefecture in the late 1960s in anticipation of the new business opportunities created once US forces withdrew. This ultimately led to the Yamaguchi-gumi, the largest criminal organization in the country, leading a ten year war in Okinawa against other gangs. However, this was only just starting when the film went into production. Macias, Patrick, Sympathy for the Underdog DVD booklet, 2004, Home Vision Entertainment. Retrieved 2014-08-22 

Inspired by movies about the French Foreign Legion, "and stories about people who cross national borders and ended up fighting in foreign wars," Fukasaku originally wanted to make a film about yakuza that end up in Vietnam. But stated this ultimately proved "impossible."  Fukusaku biographer Sadao Yamane stated that Sympathy for the Underdog was originally developed as a sequel to Japan Organized Crime Boss, a film released shortly before it and which also stars Tsuruta and Ando, until the director saw The Battle of Algiers. It was then that, Yamane thinks, Fukasaku decided to make a film about "foreigners" and "resistance groups" within a yakuza film.   

==Influence==
This film has inspired Takeshi Kitanos 1993 Sonatine (1993 film)|Sonatine. Several story elements, including a famous okinawan theme, were later used in Sonatine.

==References==
 

==External links==
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 