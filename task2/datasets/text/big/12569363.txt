Caravan 841
 
{{Infobox film
| name           = Caravan 841
| image          = Caravan-1-.jpg
| image_size     =
| caption        =
| director       = Zion Rubin
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 2001
| runtime        = 52 minutes
| country        = Israel Hebrew with English subtitles
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}} 2001 Israeli film made by Zion Rubin that follows Moshe, a little Jewish boy from Ethiopia who was sent to Israel to escape from his war-torn homeland. While Moshe waits for his mother to meet him in Israel, life gives him one disappointment after the another, but he works through them with the help of  a strict Rabbi and a saxophone player who owns a night club.

==Summary==
Caravan 841 tells a premature coming of age story of a boy who is forced to realize all too soon that the world is cruel and unfair. The rabbi and saxophone player take a sincere interest in Moshe and serve as kindhearted role models who guide him in different directions. While the Rabbi encourages him to pray, study, and live dutifully, his musician friend, Walter, gives him opportunities to laugh, dance and make music—leaving Moshe to choose between them and find his own path.

Orphaned and lonely, Moshe desperately wants something stable to believe in, but, after being repeatedly hurt, hes instinctively distrustful. The Rabbi give Moshe a small box in which to collect his coins and tells him to trust in God; God, he says, will give him all he needs. Walter, on the other hand, tells Moshe to trust in himself. Walter recognizes Moshes strong-will, bravery and independence. He knows that a little boy who grew up in Ethiopia and lost his father to murderers in Sudan already knows a great deal about the world and simply needs to trust in the knowledge he has.

Caravan 841 explores what life in Israel as an outsider is like. Although hes Jewish, Moshe obviously wasnt raised with a thorough understanding of the culture. Jewish prayers and traditions are slightly foreign to him. At one point, the Rabbi decides Moshe should be circumcised, and, unfamiliar with the practice, Moshe runs away terrified.

At the beginning of the film, one of Moshes few friends sings Bob Marley| Bob Marleys lyrics, as he dances about, “Ive gotta run like a fugitive to live the life I live...Im going to be iron, like a lion in Zion.” With big innocent eyes and long skinny legs, Moshe appears vulnerable, but his inner strength of character is repeatedly tested, suggesting that the little boy has a lions spirit.

==Reception== San Francisco International Film Festival.

==See also==
Other Israeli feature films:
*Time of Favor
*Kedma (film)|Kedma
*Alila
*Kippur

==References==
{{Citation
  | last =
  | first =
  | title = Caravan 841
  | date =
  | year = 2006
  | url =http://www.desertfilmsociety.com/archive/2006screenings.html
  | accessdate = August 2 }}

==External links==
* 
* 
* 

 
 
 
 