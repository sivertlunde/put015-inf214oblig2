How Czar Peter the Great Married Off His Moor
How Czar Peter the Great Married Off His Moor ( , Skaz pro to, kak tsar Pyotr arapa zhenil) is a 1976 film directed by the Russian filmmaker Aleksandr Mitta. The film features Vladimir Vysotsky as the protagonist Ibragim Hannibal who is the North African godson of the Czar Peter. Also starring in the film are Aleksei Petrenko as Peter the Great, and Irina Mazurkevich as Natasha Rtishcheva.      It is an adaptation of Aleksandr Pushkins book Peter the Greats Negro (1827) The music for the film was done by the composer Alfred Shnitke. In 1976, the film was the sixth most popular film in the Soviet Union, being seen 33,100,000 times.

==Plot==
The beginning of the film contains some animations depicting Ibragims acquisition from his native land and eventually we find him in the courts of Paris.  He gets himself in trouble when a French countess, whom he had romances with, bears a black child.  Ibragim is challenged to a duel which is played out humorously in a sped up timeframe. He leaves Paris shortly thereafter to return to his home in St. Petersburg where his godfather, and czar of Russia Peter the Great enthusiastically awaits his return.  Upon arriving Ibragim expresses he is done with love, as his lover denounces him as a savage and his son is never fated to know his father. In an attempt to help Ibragim, as well as tie him into the nobility of Russia, Peter announces Ibragim and Natasha Rtishcheva are to be wed, to the despair of the family and the her current suitor.  Ibragim however, refuses the marriage on the grounds that he believes Natasha is in love with another.  This angers the czar who in turn begins to alienate both Ibragim and Natashas family from his favor.  The conundrum plays out in humorous and dramatic ways and eventually resolves in a pleasant and light-hearted manner.

==Cinematography==
The film is in color and makes use of animation at times, as well as a combination of animated scenes and live actors.  The colors are lively and in a few instances the timescale is sped up, making light of otherwise dire situations. Also Mitta makes use of point-of-view angles, for example when a nervous mariner crosses a long, elevated plank in order to reach Peter the camera looks down at his feet as he nervously crosses.  The shot shows the drop below and the characters feet cautiously making each step, poking fun at the hesitation.

==References==
 

 
 
 

 