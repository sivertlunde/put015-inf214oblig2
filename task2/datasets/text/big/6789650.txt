Moolaadé
 
{{Infobox film
| name        = Moolaadé
| image       = Moolaadé (film).jpg
| caption     =
| director    = Ousmane Sembène
| writer      = Ousmane Sembène
| starring    = Fatoumata Coulibaly
| producer    = Ousmane Sembène   Thierry Lenouvel
| distributor =
| music       = Boncana Naiga
| released    =  
| cinematography = Dominique Gentil
| editing     = Abdellatif Raïss
| country     = Senegal / France / Burkina Faso / Cameroon / Morocco / Tunisia
| runtime     = 120 minutes French / Bambara
}}
Moolaadé ("magical protection") is a 2004 film by the  .
The film argues strongly against the practice, depicting a village woman, Collé, who uses moolaadé (magical protection) to protect a group of girls. She is opposed by the villagers who believe in the necessity of female genital cutting, which they call "purification".

== Historical Context ==
 
To outsiders, the act known as "female genital cutting" is often shocking. Practitioners surgically remove part or all of the female genitals. Traditionally, it is cut with an iron sheet or a knife, then sutured back with needlework or a thorn. There is no anesthetic during the whole process, and the disinfection is not thorough. Gynecologist Dr Rosemary Mburu of Kenya estimate that as many as 15% of circumcised girls die of the excessive loss of blood or infection of the wound.  Even the girls who survive the circumcision will endure lifelong pain.  The lower part of the body is always in pain while having sexual intercourse, urinating, or even working. This affects their ability to achieve orgasm or sexual pleasure of any kind and might also affect fertility.
:
"Although the precise origins of Female Genital Cutting are unclear, the practice dates back to antiquity (Lightfoot-Klein 1989) Female Genital Cutting can be traced back as far as the second century B.C., when a geographer, Agatharchides of Cnidus, wrote about Female Genital Cutting on the western coast of the Red Sea (modern-day Egypt) (Mackie 1996). Based on the current geographic locations of Female Genital Cutting, the practice appears to have originated there with infibulation and spread southward and westward while diminishing to clitoridectomy (Mackie 1996). Some surmise that female genital cutting is rooted in the Pharaonic belief in the bisexuality of the gods (Meinardus 1967; Assaad 1980). According to this belief, mortals reflected this trait of the gods-every individual possessed both a male and a female soul. The feminine soul of the man was located in the prepuce of the penis; the male soul of the women was located in the clitoris. For healthy gender development, the female soul had to be excised from the man and the male soul excised from the women. Circumcision was thus essential for boys to become men; and girls, women (Meinardus 1967, 388-89)" 
In the world, there are approximately 130,000,000 women who accept the female circumcision excision, that is about 2,000,000 women everyday. It is mainly popular in Africa, but the procedure is also practised on the Arabian Peninsula, mainly in Iraq and Yemen.  The country in which it is most prevalent is probably Egypt, followed by Sudan, Ethiopia and Mali. In recent years, Egypt has introduced legislation to abolish female circumcision but the practice is still widely carried out.

The circumcision is sometimes done by the girls mother and female relatives, moreover, the father must stand at the side of the door to protect this work symbolically. The young girl sits on a chair, restrained by other women. Then an old woman opens hers lip of vulva, fixes with the needle in the one side?, lets the clitoris entire dew come out?. Then glans clitoridis are cut with a kitchen knife, then the other parts of the clitoris. There is also a woman who is responsible for wiping off the blood unceasingly, and the girls mother puts in the finger in the incision to pull out the entire organs. When the mother excises the clitoris, she will cut flesh away from the bone, even around the lip of vulva. Then the mother will use her finger to dig everywhere in the wound, simultaneously letting another woman touch the wound, to make sure that all organs have excised cleanly with no part remaining. Afterwards, the girls mother will also cut away the entire lip of vulva. At this moment, the brutal second part officially starts. After the previous steps, the girl will most likely have lost consciousness more than once, so the women use medicines to revive the girl. Other neighbor women discretely supervise the mother as she carries out her work. Because some girls are unable to withstand the severe pain, they sometimes try to nip the tongue to commit suicide. Therefore there is a woman to inspect girls mouths carefully. They scatter pepper on the tongue to let it retract immediately between lips. After the surgery is completed, the mother will uses locust tree acicular  to suture the both sides of the remaining genitalia, leaving only a very small aperture to drain the menstruation. The smaller this hole is, the more highly the girl will be valued.
:
There is no simple answer to the question why people do it. It is practised by different ethnicities, also religions, including Muslims, Christians, and Jews, as well as followers of traditional African religions. "A good example of variation is found in the survey search done by Rushman and colleagues in Sudan. Reporting on a sample of 1,804 female and 1,787 male respondents, the authors found that answers to the questions of why female circumcision was practised (where more than one reason could be given) varied a great deal. The majority of men (59percent) said it was a "good tradition" (42 percent), but only 28 percent of men gave that reason. Substantial number of men (28 percent) and women (19 percent) said it promoted cleanliness, while relatively few thought it promoted fertility (1 percent of women and 2 percent of men). Surprisingly, only about one-tenth explicitly mentioned protecting virginity and preventing immorality (10 percent of women, 11 percent of men), and even fewer said it "increases chances of marriage" (9 percent of women and 4 percent of men). Quite a few (13 percent of women and 21 percent of men) mentioned the increase in the pleasure of the husband as a reason. Similar results are reported for Somalia in a study by Dirie and Lindmark (1991), with religion playing a major role in peoples justifications for female circumcision. Allowing respondents just one choice of reason to justify female circumcision, they found that of the 290 female interviewees in their survey (of medium to high socioeconomic status), 70 percent stated "religion", 20 percent said "to remain virgin in order to get married", and 10 percent said “tradition”."  

==Plot==
 
The film is set in a colourful Burkina Faso village dotted with termite mounds, and a mosque made from clay that resembles a gigantic hedgehog. The village is a symbol of green Africa, a time capsule that nonetheless is not immune to the influences of the outside and ‘modern’ world. http://archive.is/20121124092949/http://www.irenees.net/en/fiches/documentation/fiche-documentation-579.html 

Collé is the second of her husbands three wives. She is the most intelligent, humorous, charming, and is also loved most by her husband, who is portrayed as a temperate enlightened man. Her nubile daughter, Amasatou, has become engaged, although she has not undergone female genital cutting, considered a prerequisite for marriage in the local tradition. Collé opposes this practice. This has led the elders in the village, women as well as men, to despise her daughter. Amasatou herself unceasingly requests to have her genitals cut to secure her social status and marriage acceptance, but Collé remains unmoved. She is willing to protect not only her daughter from the life-threatening genital cutting but also four little girls who join her to refuse the practice. Collé draws a symbolic line, the colorful rope Moolaadé, a "magical protection," across the gate of the familys premises. Moolaadé prevents the women elders who carry out the practice, and who have been searching for the girls, from entering the house.

In the beginning, the first wife seems to be against Collés plan to protect the girls. However, later they become closer and she tells Collé that she also opposes female genital cutting. She feared making it known, but has been helping her all along, without anyones notice.

While facing her daughters request to be circumcised, Collé explains that she does not want her daughter to end up on the same road she travelled. Her first reason is that it has too many indefinite outcomes, some of which can be fatal. An even bigger reason is that Collé had two unsuccessful pregnancies before Amasatou, which caused her great physical and emotional pain and were almost fatal. In a flashback, there is a scene of her and her husband having sexual intercourse which it clear is causing her physical pain. He falls asleep, while she is unable to do so because the sexual intercourse brings unbearable pain for her rather than pleasure. She keeps biting her ring finger, symbol of her marriage, and dares not say a single word even when her finger bleeds. At dawn, she is still awake to wash her body, as well as her blood off the bed sheet.

If Collé represents African women who awaken to resist patriarchal control, then her daughters fiance Ibrahima, a rich, upstanding, and open-minded young man living in France, one of Africas former colonizers, who returns, filmically representing the enlightened elite educated abroad who is welcomed home and observes the barbaric tradition of his village home. His knowledge, money and technologies such as television are appreciated.  He witnesses a funeral of two little girls, who desperately drowned themselves in a well to avoid the mutilation of their genitals. The girls relatives are sad, but the incident does not lead the villagers to question the tradition. Ibrahima is shocked and worried by this scene which keeps fermenting in his mind. Meanwhile, Ibrahimas father wants him to renounce his engagement to Amasatou, and marry his innocent eleven-year-old cousin instead, who has already undergone female genital cutting. Ibrahima refuses to do so, recognizing such an act as child abuse, and visits Amasatous house despite what the villagers say. He confirms her as his fiancée, regardless of her "impure" status according to the local tradition.

The African womens most important daily entertainment, besides sitting together and chatting under the tree shadow enjoying the cool air, is enjoying the radio which transmits news of the world and the music. Some incidents including Ibrahimas revolt against his father on the engagement and Collés protection of the five little girls, including her own daughter, from the life-threatening female genital cutting in the village causes the elders to think that the atmosphere is bad. Collés husband has lost the ability to control his own wife and the elders insist that he beat her with a leather whip in the presence of the community to restore order. The elders want her to utter the magic word so they can take away the four little girls from her protection but, no matter how hard her husband whips her, she endures, refusing to give her tormenters the satisfaction of a scream or cry. Opposite groups of women shout to her to revoke or to be steadfast, but no woman interferes. When she is on the verge of collapse, the merchant steps out and stops the whipping.

The womanizing merchant is called Mercenaire by people in the village. He is a war veteran who has become a merchant. When he converses with Ibrahima, he accuses him, his father, and his uncle of pedophilia and is suddenly no longer concerned about the money that he could possibly get from the rich young man. He is bringing all the plastic junk to the village; the junk is brightly and boldly colored as the magnificent costumes the people wear in Africa. He sells his stuff at extremely high prices (he even raises the prices when Ibrahima came to pay for his dads bill). Later he is hunted out of the village and, when out of sight, murdered. 

During the whipping, one of the four girls mother steals her daughter from Collés house and sends her to get her genitals cut, although the little girl screams and tries to resist. The girl dies as a result of the cutting and her mother regrets her previous support of it. The other mothers all see the tragedy happen and thus change their minds and begin opposing genital cutting.

From the mens point of view, the radio is a bad influence on the women because it teaches them things from the outside world, such as the idea of equality. Therefore, the elders decide that all the radios in the village must be confiscated and burned. Although all the radios are supposed to be burned, some are hidden by the women of the village. The women are united because of the pain caused by the genital cutting. They are all mourning, they are all awakened, they seize the blade and pursue the genital cutters, shouting, “No more genital cutting!” Ibrahima stands up to his father, says he is not going to listen to him, and announces that he is going to marry Amasatou because he is proud of her. The end of the movie is the smoke of the burning radios, which speaks both to speaking out and repression of speech.

==Cast==
* Fatoumata Coulibaly as Collé Gallo Ardo Sy, the second wife who protects the girls from the female genital cutting.
* Maimouna Hélène Diarra as Hadjatou
* Salimata Traoré  as Amasatou
* Dominique Zeïda  as Mercenaire
* Mah Compaoré  as Doyenne des Exciseuses
* Aminata Dao  as Alima Bâ
* Stéphanie Nikiema  as Mah
* Mamissa Sanogo  as Oumy
* Rasmane Ouedraogo  as Ciré Bathily
* Ousmane Konaté  as Amath Bathily
* Bakaramoto Sanogo  as Abdou
* Modibo Sangaré  as Balla Bathily
* Joseph Traoré  as Dugutigi
* Théophile Sowié  as Ibrahima (as Moussa Théophile Sowié)
* Balla Habib Dembélé  as Sacristain (as Habib Dembélé)
* Gustave Sorgho  as Bakary
* Cheick Oumar Maiga  as Kémo Tiékura
* Sory Ibrahima Koïta  as Kémo Ansumana (as Ibrahima Sory Koita)
* Aly Sanon  as Konaté
* Moussa Sanogo  as Konaté fils
* Naky Sy Savane  as Sanata (as Naki Sy Savane)
* Marie Yameogo  as Exciseuse (as Marie Augustine Yameogo)
* Mabintou Baro  as Exciseuse
* Tata Konaté  as Exciseuse
* Fatoumata Sanogo  as Exciseuse
* Madjara Konaté  as Exciseuse
* Fatoumata Konaté  as Exciseuse
* Fatoumata Sanou  as Nafissatou
* Mariama Souabo  as Jaatu
* Lala Drabo  as Saaiba
* Georgette Paré  as Niassi
* Assita Soura  as Seymabou
* Alimatou Traoré  as Binetou
* Edith Nana Kaboré  as Ibatou
* Maminata Sanogo  as Coumba
* Sanata Sanogo  as La Reine mère
* Mafirma Sanogo  as Fify

==Reception==

=== Critical ===
This movie received positive reviews over all. Review aggregate Metacritic assigned the film 91/100 based on 26 Critics, in which 24 are positive and 2 are mixed. On the review aggregate Rotten Tomatoes, 99% of 72 critics gave the film a positive review, with an average score of 8.5/10.
Roger Ebert of the Chicago Sun-Times called it "for me the best film at Cannes 2004, a story vibrating with urgency and life. It makes a powerful statement and at the same time contains humor, charm and astonishing visual beauty". 
Dana Stevens of The New York Times found "To skip Moolaade would be to miss an opportunity to experience the embracing, affirming, world-changing potential of humanist cinema at its finest."  
Desson Thomson of Washington Post said "Moolaade, in short, is a movie to rock the soul". 
Kevin Thomas of Los Angeles Times, said "Theres such a rich sense of the fullness of life in Moolaadé that it sustains those passages that are truly and necessarily harrowing". 
Melissa Levine of Dallas Observer said "Its not easy to pull off a good morality tale. Thats why Moolaad, the new film from 81-year-old Senegalese writer-director Ousmane Sembene, feels like such an exceptional success. Its moral center is painfully clear, but so is its humanity".  

However, Kirk Honeycutt of The Hollywood Reporter gave a 60 and announced that that "As drama the film mostly serves to illustrate the two sides of this crucial social debate in Africa".  Phil Hall of Film Threat gave only 40/100 and found the movie "Achieves the impossible in taking a genuine socio-political tragedy and turning it into an anvil drama which will fray the patience of the most sympathetic audiences". 

===  Box Office  ===
"Moolaadé" took in $11,982 on its opening weekend. In 41 weeks, the total domestic is $215,646, foreign $218,907. 

==Accolades==
Prominent American film critic Roger Ebert was a big supporter of the film, naming it one of his top ten of the year, and later adding it to his list of great movies.
{| class="wikitable"
|-
! Year !! Award !! Category !! Winner/Nominee !! Result
|- 2004
|rowspan=2|2004 Cannes Film Festival Prix Un Certain Regard
|
| 
|- Prize of the Ecumenical Jury
| special mention   
|- European Film Awards Screen International Award Ousmane Sembène
| 
|- Marrakech International Film Festival Special Jury Award Ousmane Sembène
| 
|- Golden Star Ousmane Sembène
| 
|- National Society of Film Critics Awards National Society Best Foreign Language Film
|
| 
|- Political Film Society Awards, USA Political Film Award for Democracy
|
| 
|- Political Film Award for Human Rights
|
| 
|- 2005
|Cinemanila International Film Festival Best actress Fatoumata Coulibaly
| 
|- Image Awards Outstanding Independent or Foreign Film
|
| 
|-
|Pan-African Film Festival jury award Ousmane Sembène
| 
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 