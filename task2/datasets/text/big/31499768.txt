Footnote (film)
 
{{Infobox film
| name           = Footnote
| image          = Footnote (poster art).jpg
| caption        = International festival poster
| director       = Joseph Cedar
| producer       = David Mandil Moshe Edery Leon Edery
| writer         = Joseph Cedar
| starring       = Shlomo Baraba Lior Ashkenazi
| music          = Amit Poznansky
| cinematography = Yaron Scharf
| editing        = Einat Glaser Zarhin
| studio         = United King Films Movie Plus
| distributor    = United King Films
| released       =  
| runtime        = 107 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = $2,007,758   
}}
Footnote ( , Transliteration|translit.&nbsp;Hearat Shulayim) is a 2011 Israeli drama film written and directed by Joseph Cedar, starring Shlomo Baraba and Lior Ashkenazi.  The plot revolves around the troubled relationship between a father and son who teach at the Talmud department of the Hebrew University of Jerusalem. 
 Best Screenplay 84th Academy Awards for Best Foreign Language Film.     On 18 January 2012, the film was named as one of the nine shortlisted entries for the Oscars.    On 24 January 2012, the film was nominated for an Academy Award in the category of Best Foreign Film,    but lost to the Iranian film A Separation.

==Plot== philologist who researches the different versions and phrasings of the Jerusalem Talmud.  He and his son Uriel (Lior Ashkenazi) are both professors at the Talmudic Research department of the Hebrew University of Jerusalem.

Uriel, a young charismatic academic, is extremely popular with the departments students and the general public, and is also recognized by the establishment when he is elected member of the Israel Academy of Sciences and Humanities. The father, on the other hand, is a stubborn old-school purist in his research methods. He is unpopular, unrecognized, and frustrated by his would-be lifetime research achievement having gone unfulfilled, as a rival scholar, Prof. Yehuda Grossman (Micah Lewensohn), published similar results one month ahead of Eliezer. Eliezer is also highly critical of the new methods of research used by his son and other modern researchers, as he considers them superficial. His ambition is to be recognized by being awarded the Israel Prize, but he is disappointed every year when he does not win it. His nature and the lack of recognition have made him bitter, anti-social, and envious of his sons popularity.
 Minister of Education. She tells Eliezer that he was elected this years laureate of the Israel Prize and congratulates him.

The following day Uriel is summoned to an urgent meeting with the Israel Prize committee. Uriel is told that an error had occurred and that in fact it was he, not his father, who was awarded the Israel Prize. The committee wishes to discuss ways to correct the error, but Uriel objects, saying the revelation would devastate his father. Uriel and the head of the committee, Grossman, argue over the issue until Uriel loses his temper and shoves Grossman. Regretting his outburst, Uriel relents, and asks that the committee permit him to break the news to his father personally. During the meeting Uriel says he has been submitting his fathers name for the Israel Prize every year, and accuses Grossman of blocking that and other ways of recognizing Eliezer. According to Grossman, Eliezer never published anything significant in his career, and his only claim to fame is being mentioned as a footnote in the work of a more famous scholar.

Uriel goes to the National Library to break the news to his father but finds him raising a toast to winning the prize with colleagues. Unable to break the news, he once again meets with Grossman, asking that the prize be given to Eliezer. Grossman relents but with two conditions: Uriel must write the committees recommendation and Uriel can never be a candidate for the prize. Uriel agrees.

Uriel writes the recommendation text, picking and choosing every word carefully; at the same time Eliezer, finally recognized, is interviewed by the newspaper Haaretz, during which he denounces the scientific and academic validity of Uriels research.

When the interview is published, Uriel is angry but keeps his secret. Later, though, he whispers the secret to his mother. She does not disclose the truth to anyone else.

During preparations for a television interview, Eliezer is struck by a word in the Israel Prize committees recommendation. He flees the television studio and returns to his study. He examines the word, cross-checking its published uses, and realizes that the text must have actually been written by Uriel. Eliezer also reconstructs his phone conversation with the Minister of Education, realizing she had addressed him by his last name only. He concludes that the minister thought she was talking to his son when she broke the news about the Israel Prize.
 Jerusalem International Convention Center to prepare for the ceremony; Eliezer is stressed and distracted.  The movie ends a moment before the laureates are called to the stage.

==Cast==
* Shlomo Bar Aba as Eliezer Shkolnik
* Lior Ashkenazi as Uriel Shkolnik
* Alisa Rosen as Yehudit Shkolnik
* Alma Zack as Dikla Shkolnik
* Daniel Markovich as Josh Shkolnik
* Micah Lewensohn as Yehuda Grossman
* Yuval Scharf as Noa the reporter
* Nevo Kimchi as Yair Fingerhut
* Yona Elian as Yuli Tamir, Minister of Education (voice)

==Production==
Director  . 

The film marked the return to cinema after 20 years for Shlomo Bar Aba, a stage comedian, in the role of the father. Bar Aba prepared his character for six months. Lior Ashkenazi, who was raised in a secular home, took Talmud classes at the Hebrew University and let his beard grow for eight months. 

==Release== Best Screenplay Award. It was released in Israel on 2 June through United King Films.  North American distribution rights for the film were acquired by Sony Pictures Classics. 

==Box office==
As of 8 July 2012, the film has grossed $2,007,451, in North America. 

==Critical reception==
The film received high critical acclaim. At  .  A. O. Scott, film critic for The New York Times, called Footnote the fourth best film of 2012. Wrote Scott: "This Israeli film takes what might have been a trivial anecdote — a committee accidentally awards a prize to the wrong scholar — and turns it into a tragicomic opera with a great deal to say about Zionism, academia, family life and the way language functions as a bridge between the sacred and the profane." 

==See also==
* Culture of Israel
* Cinema of Israel
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Israeli submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 