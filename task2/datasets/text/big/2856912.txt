Zurdo
{{Infobox film
| name=Zurdo
|image= Zurdo (2003 film) poster.jpg
|director=Carlos Salces
|writer=Blanca Montoya,  Carlos Salces
|starring=Alex Perea, Arcelia Ramírez, Alejandro Camacho, Blanca Salces
|producer=Blanca Montoya, Mónica Lozano
|distributor=Altavista Films
|runtime=110 min.
|language=Spanish
|}} fantasy adventure directed by Carlos Salces from the same company that produced Amores Perros.

Translated from Spanish language|Spanish, the title means "Lefty." It is also a nickname of Rafael Nadal due to him being a left-handed player.

== Storyline ==

The story is of a young boy called Lefty, a gifted marble player who lives in an urban housing complex. One day a stranger arrives claiming to know the best marble player in the world and challenges Lefty to a contest. The news sends the townspeople into a frenzy and they put all their hopes and dreams into their little hero winning much more than a simple game of marbles. With success well within his grasp, Lefty is faced with a dilemma when a corrupt local official comes along to sabotage his chances.

== Contributing artists ==
 Ariel (Mexican Oscar) for his work, he also has released the soundtrack to the film in the form of an album.

==External links==
* 

 
 
 
 
 


 
 