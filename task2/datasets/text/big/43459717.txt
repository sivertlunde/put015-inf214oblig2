Electric Boogaloo: The Wild, Untold Story of Cannon Films
{{Infobox film
| name = Electric Boogaloo: The Wild, Untold Story of Cannon Films
| image = Electric_boogaloo_poster.jpg
| caption =
| director = Mark Hartley
| producer = {{Plainlist|
* Veronica Fury
* Brett Ratner}}
| writer = Mark Hartley
| starring = {{Plainlist|
* Menahem Golan
* Yoram Globus}} Garry Richards
| music = Jamie Blanks
| editing = {{Plainlist|
* Jamie Blanks
* Sara Edwards
* Mark Hartley}}
| studio = {{Plainlist|
* Film Victoria
* Screen Australia}} RatPac Entertainment (North America)
| released =  
| runtime = 106 minutes  
| country = Australia
| language = English
| budget =
| gross = 
}}
Electric Boogaloo: The Wild, Untold Story of Cannon Films is a 2014 Australian documentary film written and directed by Mark Hartley. It focuses on the story of The Cannon Group.

==Interviewees==
Interviewees include Olivia dAbo, John G. Avildsen, Martine Beswick, Richard Chamberlain, Bo Derek, Lucinda Dickey, Michael Dudikoff, Robert Forster, Elliott Gould, Tobe Hooper, Just Jaeckin, Dolph Lundgren, Franco Nero, Molly Ringwald, Robin Sherwood, Catherine Mary Stewart, Alex Winter, and Franco Zeffirelli.

==Production==
The film was partly funded by Brett Ratners RatPac Entertainment. Other investors included the Melbourne International Film Festival’s Premiere Fund, Film Victoria and Screen Queensland. 

==Release==
The film had its world premiere in August 2014 at the Melbourne International Film Festival,  and was shown in October 2014 at the BFI London Film Festival.  2015 screen as part of the Film Comment Selects tribute to Cannon Films on 20 February in the Walter Reade Theater. 

==See also==
*  , a 2008 film by Hartley
*  , a 2014 documentary film

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 