Boh Ngau
 
{{Infobox film
| name           = Boh Ngau   (The Champions)
| image          = Boh-ngau-film-the-champions.jpg
| caption        = Theatrical release poster
| director       = Brandy Yuen
| producer       = Raymond Chow
| screenplay     = Brandy Yuen
| starring       = Biao Yuen
| music          = 
| cinematography = Kwun Wah Ma   Kenneth Song
| editing        = Yao Chung Chang
| distributor    = 
| released       = 1985 
| runtime        = 
| country        = 
| language       = Chinese
| budget         = 
| gross          = 
}} football movie starring Yuen Biao.

==Synopsis==
In this sports comedy film, Biao is taunted by rival football teams. At first he is just a ball boy and a punch bag for leading villain Dick Wei. But after too many beatings, Biao decides to play for another local team and go against Dick Wei in a one off football at the end. The film also tackles problems of jealousy, corruption and bullying in sports. 

==Cast==
*Biao Yuen as Lee Tong
*Moon Lee as Fanny
*Dick Wei as King
*Eddy Ko as Tongs Uncle
*Kwok Keung Cheung as Suen
*Tien Lung Chen
*Wah Cheung as Kings soccer player
*Tien Hsi Tang as Team Hsin Sheng manager

==Awards==
*In 1985, the film was nominated for the Hong Kong Film Award for Best Action Choreography for Chun Yeung Yuen and Shun-Yee Yuen at the 3rd Hong Kong Film Awards. 

==References==
 

==External links==
* 

 
 


 
 