Halloween II (2009 film)
{{Infobox film name           = Halloween II image          = Halloween2009.jpg caption        = Theatrical poster director       = Rob Zombie producer       = Malek Akkad Andy Gould Rob Zombie writer         = Rob Zombie based on       =   starring       = Malcolm McDowell Tyler Mane Sheri Moon Zombie Brad Dourif Danielle Harris Scout Taylor-Compton  music          = Tyler Bates cinematography = Brandon Trost editing        = Glenn Garland studio         = Dimension Films Spectacle Entertainment Group Trancas International Films     distributor    = The Weinstein Company released       =   runtime        = 101 minutes country        = United States language       = English budget         = $15 million gross          = $39.4 million
}} 2007 reboot Michael Myers as he continues his search for Laurie so that he can reunite with his sister. The film sees the return of lead cast members Malcolm McDowell, Scout Taylor-Compton, and Tyler Mane, who portray Dr. Loomis, Laurie Strode, and Michael Myers in the 2007 film, respectively.

For Halloween II, Zombie decided to focus more on the connection between Laurie and Michael, and the idea they share similar psychological problems. Zombie wanted the sequel to be more realistic and violent than its 2007 predecessor. For the characters of Halloween II, it is about change. Zombie wanted to look at how the events of the first film affected the characters. Zombie also wanted to show the connection between Laurie and Michael, and provide a glimpse into each characters psyche. Filming primarily took place in Georgia, which provided Zombie with a tax incentive as well as the visual look the director was going for with the film. When it came time to provide a musical score, Zombie had trouble finding a place to include John Carpenters original Halloween theme music. Although Carpenters theme was used throughout Zombies 2007 film, the theme was only included in the final shot of this film.

Halloween II was officially released on August 28, 2009 in North America, and was met with a negative reception from critics. On October 30, 2009 it was re-released in North America to coincide with the Halloween holiday weekend. The original opening of the film grossed less than the 2007 remake, with approximately $7 million. The film would go on to earn $33,392,973 in North America, and $5,925,616 in foreign countries giving Halloween II a worldwide total of $39,318,589. The film was released on DVD and Blu-ray, with a theatrical version and directors cut of the film offered.

==Plot==

 
 Deborah Myers Michael Myers shock  Sheriff Brackett Annie (Danielle traffic accident, white horse.

Michael appears at the hospital, and begins murdering everyone he comes across on his way to Laurie. Trapped in a security outpost at the gate, Laurie watches as Michael tears through the walls with an ax, but just as he tries to kill her, Laurie wakes up from the dream. It is actually one year later and Laurie is now living with the Bracketts. Michael has been missing since last Halloween—still presumed dead—and Laurie has been having recurring nightmares about the event. While Laurie deals with her trauma through therapy, Dr. Loomis has chosen to turn the event into an opportunity to write another book. Meanwhile, Michael has been having visions of Deborahs ghost and a younger version of himself, who instructs him that with Halloween approaching it is time to bring Laurie home; so he sets off for Haddonfield.

As Michael travels to Haddonfield, Laurie begins having hallucinations that mirror Michaels, which involve a ghostly image of Deborah and a young Michael in a clown costume. In addition, her hallucinations also begin to include her acting out Michaels murders, like envisioning herself taping Annie to a chair and slitting her throat while dressed in a clown outfit—similar to how a young Michael murdered Ronnie White. While Laurie struggles with her dreams, Loomis has been going on tour to promote his new book, only to be greeted with criticism from people who blame him for Michaels actions and for exploiting the deaths of Michaels victims. When his book is released, Laurie discovers that she is really Angel Myers, Michaels long lost sister. With the truth out, she decides to go to a party with Mya (Brea Grant) and Harley (Angela Trimbur) to escape how she is feeling. Michael appears at the party and kills Harley, then makes his way over to the Brackett house and stabs Annie repeatedly.
 
When Laurie and Mya arrive they find Annie bloodied and dying. Michael kills Mya and then comes after Laurie, who manages to escape the house. While Laurie manages to flag down a passing motorist, Sheriff Brackett arrives home and finds his daughter dead. Laurie gets into the motorists car, but before they can escape Michael kills the driver and flips the car over with Laurie still in it. Michael takes the unconscious Laurie to an abandoned shed he has been camping out in. Laurie awakens to a vision of Deborah, and a young Michael, ordering her to say "I love you, mommy".

The police discover Michaels location and surround the shed. Loomis arrives and goes into the shed to try to reason Michael into letting Laurie go. Inside, he has to inform Laurie, who believes that the younger Michael is holding her down, that no one is restraining her and that she must maintain her sanity. Just then, Deborah instructs the older Michael that it is time to go home, and Michael grabs Loomis and kills him by slashing his face and stabbing him in the chest. Stepping in front of a window while holding Loomiss body, Michael is shot twice by Sheriff Brackett and falls into the spikes of some farming equipment. Apparently released from the visions, Laurie walks over and tells Michael she loves him, then stabs him repeatedly in the chest and finally in the face. The shed door opens and Laurie walks out, wearing Michaels mask. As she pulls the mask off, the scene transitions to Laurie in isolation in a psychiatric ward, grinning as a vision of Deborah dressed in white stands with a white horse at the end of her room.

==Production==

===Development===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 95%; background:#F0F8FF; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"Dont feel hindered by any of the rules weve had in the past. I want this to be your vision and I want you to express that vision."
|-
|style="text-align: left;" |— Producer Malek Akkad speaking to writer/director Rob Zombie. 
|}
In 2008, at the 30 Years of Terror Convention, Halloween producer Malek Akkad confirmed that a sequel to Rob Zombies 2007 film was in the works. French filmmakers Julien Maury and Alexandre Bustillo were in negotiations to direct the sequel in November 2008,  but on December 15, 2008 Variety (magazine)|Variety reported that Rob Zombie had officially signed on to write and direct the Halloween sequel.  In an interview, Zombie expressed how the exhaustion of creating the first Halloween made him not want to come back for a sequel, but after a year of cooling down he was more open to the idea.  The writer/director explained that with the sequel he was no longer bound by a sense of needing to retain any "John Carpenter-ness", as he "felt free to do whatever".  Producer Malek Akkad said the original intention, when they believed Zombie was not returning, was to create a "normal sequel".    Akkad and his Trancus producing company hired various writers to come up with drafts for a new film, but none worked. Akkad and the Weinstein brothers then turned to Bustillo and Muary, whose film Inside (2007 film)|Inside had recently been bought for distribution by the Weinstein Company. According to Akkad, the producers really wanted Zombie to return, as Akkad felt that there was something "lost in the translation" when the French filmmakers took over the project.  After his work on the 2007 remake, Zombie had earned the trust of Akkad, who told him to ignore any rules they had set for him on the previous film. Akkad said that he wanted Zombie to move the franchise away from some of its established rules. 

===Characters===
For the sequel, Tyler Mane, Malcolm McDowell, Scout Taylor-Compton, Danielle Harris, Sheri Moon Zombie, and Brad Dourif returned to the roles of Michael Myers, Dr. Loomis, Laurie Strode, Annie Brackett, Deborah Myers, and Sheriff Brackett, respectively. Daeg Faerch, who portrayed a young Michael Myers in the 2007 remake, was set to reprise his role for Halloween II. By the time production was getting started for the sequel Faerch had grown too big for the part. Zombie had to recast the role, much to his own dismay, because Faerchs physical maturity did not fit what was in the script. Although Faerch is not in the sequel, the first trailer for Halloween II contained images of Faerch. Zombie pointed out that those images were test shots done and were not intended to be in either the trailer or the film. 
{|class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 95%; background:#F0F8FF; color:black; width:27em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"As Laurie is Michaels sister, Im playing it like hes clearly insane and so is she, but her insanity doesnt manifest itself in the same way... Shes slipping into insanity throughout the whole movie."
|-
|style="text-align: left;" |— Zombie describing Lauries psychological state. 
|}
Taylor-Compton described her character as having "these bipolar moments",    where her emotions are spontaneously changing from points of happiness to agitation. The actress stated that Zombie wanted to see Laurie Strode travel into "these really dark places".  Taylor-Compton clarified that when the film starts Laurie is still not aware that Michael is her older brother, and as the film progresses more and more pieces of information are given to her and she does not know how to deal with them. The actress explained that the darkness brewing inside Laurie is manifested externally, generally through her physical appearance and the clothes she chooses to wear—Zombie characterized the look as "grungy". 

Zombie further described Laurie as a "wreck", who continually sinks lower as the film moves forward.  Even Sheriff Brackett goes through changes. Brackett, who receives more screen time in this film, allows Laurie to move in with him and his daughter after the events of the first film. Zombie explained, "Hes old, hes worn out, hes just this beat-down guy with these two girls he cant deal with."  Zombie characterized Loomis in the sequel as more of a "sellout",    who exploits the memories of those who were killed by Michael in the 2007 film. Zombie explained that he tried to channel Vincent Bugliosi, a lawyer who prosecuted Charles Manson and then wrote a book about it, into Loomiss character for the sequel; noting that he wanted Loomis to seem more "ridiculous" this time.  As for Michael Myers, the character is given almost an entirely new look for the film, which is being used, according to Taylor-Compton, to illustrate a new emotion for the character as he spends much of his time trying to hide himself.  Zombie said that of all of the characters that return in the sequel, Michael is the only one that does not change: "All the other characters are very different. Laurie; Loomis; theyre having all kinds of problems in their life, but Michael just moves along. Michael is no different; hes exactly the same as he was ten years old and he killed everybody   He has no concept of the world around him, so he can never be affected by it."   

===Filming===
With a $15,000,000 budget, {{cite news|author=Ben
Fritz|url=http://latimesblogs.latimes.com/entertainmentnewsbuzz/2009/08/movie-projector-the-final-destination-halloween-ii-splitting-horror-audience.html|title=Movie projector: The Final Destination, Halloween II splitting horror audience|publisher=Tribune Company|work=Los Angeles Times|date=August 27, 2009|accessdate=August 29, 2009}}  production began on February 23, 2009 in Atlanta, Georgia.  Zombie acknowledged that filming in Georgia provided certain tax breaks for the company, but the real reason he chose that location was because the other locations he was planning to use were still experiencing snowy weather. For him, Georgias landscapes and locations provided the look that he wanted for his film.    During production, Zombie described the sequel as being "Ultra gritty, ultra intense and very real"    and said that he was trying to create almost the exact opposite of what people would expect.  Known for filming multiple sequences during production of his films, Zombie filmed an alternate ending to Halloween II. In the alternate ending, Loomis and Michael crash through the shed the police have surrounded, and out into the open air. As Loomis grasps at Michaels mask, and pleads for him to stop, Michael stabs him in the stomach, telling him to "Die!". 

===Music===
For the sequel, Zombie only used John Carpenters original theme music in the final scene of the film, though the director admits that he and music composer Tyler Bates tried to find other places to include it. According to Zombie, Carpenters music did not fit with what was happening in the film; whenever he or Bates would insert it into a scene it "just wouldnt feel right" to the director.  Zombie also used popular culture songs throughout the film, with "Nights in White Satin" appearing the most prominently. Zombie chose songs that he liked, and that would enhance a given scene within the film.  An official soundtrack for the film was released on August 25, 2009.  In addition, an album featuring the music of psychobilly band Captain Clegg and the Night Creatures was released in conjunction with Halloween II on August 28, 2009. Captain Clegg and the Night Creatures is a fictional band that appears in Halloween II.   Nan Vernon, who recorded a new version of the song "Mr. Sandman" for the end credits of the 2007 remake, recorded a cover of "Love Hurts". 

==Release==
Dimension Films released Halloween II in North America on August 28, 2009 to 3,025 theaters.    Following that, the film was released in the United Kingdom on October 9, 2009.    Dimension re-released Halloween II in North America on October 30, 2009 to coincide with the Halloween holiday,    across 1,083 theaters.    The film was released on DVD and Blu-ray on January 12, 2010; the theatrical cut and an unrated directors cut, which Zombie says is "very different from the theatrical version," are available.    

===Box office===
On its opening day, the film grossed an estimated $7,640,000,  which is less than the $10,896,610 Zombies 2007 remake pulled in during the same weekend of August.  By the end of its opening weekend, Halloween II had grossed $16,349,565.    That weekend earned more than the entire box office performances of   ($11,642,254),   ($14,400,000), and   ($15,116,634), in unadjusted dollars.    The film dropped 64.9% in its second weekend, only grossing $5,745,206 and slipping from third to sixth place. Grossing just $2,114,486 in its third weekend, Halloween II dropped out of the box office top ten to fourteenth place.    The re-release of the film was intended to take advantage of the Halloween holiday, but the film only brought in approximately $475,000.  By the end of his theatrical run, Halloween II grossed a total of $33,392,973 in North America, and an additional $5,925,616 overseas for a worldwide total of $39,318,589.  Compared to the other Halloween films, the 2009 sequel sits in fourth place, just behind the original Halloween. 

===Critical reception=== average score Time Out believed the hospital scene at the start of the film "  in just about every respect". Time Out stated that Comptons portrayal of Laurie Strode showed an "intense, nontrivializing dedication to the role" that kept interest, while the storyline of Dr. Loomiss egocentricity hinders the overall storyline. Time Out also said that Zombie hurt the film by trying to show how "violence lingers with, and perverts, all who are touched by it", but then undercutting himself with "carnivalesque" violence.  Although the New York Post  s Kyle Smith did not believe the character of Laurie Strode was a balance for Michael Myers or Dr. Loomis, he agreed the ghostly images of Deborah Myers were a "relief from the blood-streaked brutality" of Michaels murders. 
 Daily News, The Things We Do for Love", is "terrifically odd" throughout the film. Neumaier also said that the imagery of Deborah Myers and the "ethereal white horse" were a "nice visual relief" from Michaels violent attacks. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 