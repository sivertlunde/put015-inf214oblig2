Grass Labyrinth
{{Infobox film
| name           = Grass Labyrinth
| image          = 
| image_size     = 
| caption        = 
| film name          =  
| director       = Shūji Terayama
| producer       = 
| writer         = 
| narrator       = 
| starring       = Juzo Itami
| music          = 
| cinematography = 
| artdirector    = 
| editing        = 
| studio         = 
| distributor    = Toei Company (Japan)
| released       =  (France)  (Japan)
| runtime        = 50 minutes
| country        = France Japan
| language       = Japanese
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}}
  is a Japanese film directed by Shūji Terayama which was released in France in 1979 and in Japan in 1983.

==Plot==
A surreal excursion into a young mans subconscious as he searches for the words to a tune that his mother may have sung to him as a child. The dreamlike images culminate in a scene of a girls naked body covered with calligraphic characters. 

==Cast==
*Hiroshi Mikami as Akira (as a boy)
*Takeshi Wakamatsu as Akira (as a man)
*Keiko Niitaka as Mother
*Juzo Itami as Principal / Priest / Old man
*Miho Fukuya as Girl
*Masaharu Satō

==Release==
Grass Labyrinth was originally one of the installments in a French movie package called Private Collections, the other two sections being directed by Walerian Borowczyk and Just Jaeckin, both associated with avant-garde films with strong sexual content. Glass Labyrinth was the longest of the three and was later (1983) released as a separate film in Japan.   

==Awards and nominations==
8th Hochi Film Award  
* Won: Best Actor - Juzo Itami

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 