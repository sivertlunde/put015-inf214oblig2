Chinna Cinema
{{Infobox film
| name           = Chinna Cinema
| image          = 
| alt            = 
| caption        = 
| film name      = Chinna Cinema
| director       = AK Kambhampati
| producer       = Jyothy 
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Jersey plots
| released       = 2013
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}}

  
 
}}

Chinna Cinema is a romantic comedy film starring Komal Jha and music by Praveen Lakkaraju.   

==Plot==
Ramu (Arjun Kalyan) lands in the USA with the dream of making lots of money with little effort. He stays with his friends and works at a local Indian grocery store owned by B. Jay (Mahesh Sriram) and operated by B. Jays brother-in-law S. Jay (Karthik Srinivas). He takes an instant liking towards Janaki (Sumona Chanda) when he sees her the first time. Ramu is motivated to earn and send more money to his father Narayana (Surya) for building a new home. The new home is actually for an orphanage ("Raamaalayam") that is run by an old man, Bapiraju (Dr. M. Balayya). 

==Reception==
Chinna Cinema is a predictable fare that has revenge plot - often seen in Telugu films and doesnt offer anything new. The element of surprise is missing from the narration and it meanders aimlessly. 

==References==
 

 
 
 
 
 


 
 