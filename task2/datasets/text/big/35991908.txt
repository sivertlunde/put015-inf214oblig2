Tall Man Riding
{{Infobox film
| name           = Tall Man Riding
| image          = Tall_Man_Riding_Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = David Weisbart
| writer         = 
| screenplay     = Joseph Hoffman
| based on       =  
| starring       = {{Plainlist|
* Randolph Scott
* Dorothy Malone
* Peggie Castle
}}
| music          = Paul Sawtell
| cinematography = Wilfred M. Cline
| editing        = Irene Morra Warner Bros. Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1.4 million (US) 
}} Western film directed by Lesley Selander and starring Randolph Scott, Dorothy Malone, and Peggie Castle. Based on the novel Tall Man Riding by Norman A. Fox, the film is about a cowboy seeking revenge against a ranch owner for publicly whipping him years earlier and for breaking up his relationship with the ranch owners daughter.   

==Cast==
* Randolph Scott as Larry Madden
* Dorothy Malone as Corinna Ordway
* Peggie Castle as Reva, Pearlos Palace entertainer
* William Ching as Rex Willard
* John Baragrey as Sebo Pearlo
* Robert Barrat as Tucker Ordway
* John Dehner as Ames Luddington Paul Richards as The Peso Kid
* Lane Chandler as Hap Sutton
* Mickey Simpson as Deputy Jeff Barclay
* Joe Bassett as Will
* Charles Watts as Al, Pearlos Palace bartender Russ Conway as Marshal Jim Feathergill
* Holly Bane as Tom   

==Production==

===Filming locations===
* Dijon Street, Warner Brothers Burbank Studios, 4000 Warner Boulevard, Burbank, California, USA
* French Ranch, Hidden Valley Road, Thousand Oaks, California, USA 
* Iverson Ranch, 1 Iverson Lane, Chatsworth, Los Angeles, California, USA 
* Janss Conejo Ranch, Thousand Oaks, California, USA 
* Universal Studios, 100 Universal City Plaza, Universal City, California, USA (Six Points western town)   

===Soundtrack===
* "Oh, He Looked Like He Might Buy Wine" (Ray Heindorf and Sammy Cahn)
* "It Looks Like a Big Night Tonight" (Egbert Van Alstyne and Harry Williams)
* "As the Brass Band Played" (Ray Heindorf and Jack Scholl)   

==References==
 

==External links==
*  
*  
*  
 
 
 
 
 
 
 
 
 
 