The Naked Country
 
{{Infobox film
| name           = The Naked Country
| image          = 
| image size     =
| caption        = 
| director       = Tim Burstall
| producer       = Ross Dimsey
| writer         = Ross Dimsey Tim Burstall
| based on = novel by Morris West
| narrator       = John Stanton Rebecca Gilling Ivar Kants
| music          = 
| cinematography = 
| editing        = 
| studio =  
| distributor    = Dumbarton Films
| released       = 1985
| runtime        = 
| country        = Australia English
| budget         =AU$2.75 million David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p72-73 
| gross = 
| preceded by    =
| followed by    =
}}
The Naked Country is a 1985 Australian film. It was based on the 1957 novel by Morris West.

==Production==
In 1982 Ross Dimsey was finishing his contract as head of the Victoria Film Corporation and was looking for a project to do. Robert Ward of Filmways introduced him to the novel. Along with Mark Josem of Filmways and solicitor Bill Marshall they formed a company called Intronide to buy the rights to The Naked Country, which had been optioned several times but not made. Ewan Burnett, "Burstall and Dimsey Go West", Cinema Papers, May 1985 p48-49 

Dimsey did a treatment which focused on the affair between the police officer and the station owners wife. Audio interview with Ross Dimsey, Naked Country DVD Special Features, Umbrella Entertainment, 2005  He then approached Tim Burstall, with whom he had worked several times, to direct. Burstall agreed on the proviso that he helped write the script.  Tim Burstall:
 I was a hired gun there; I didnt choose the material. He   wrote it in 1945 and it really was a potboiler. It had the rudiments, the beginnings of land rights issues, but I took this and converted it absolutely into a land rights thing. To me the problem was I thought the business of Stanton versus the aborigines was okay. But there were indigestible lumps in the script like the adultery. And the Ivar Kants character was too melodramatic. I had to make him a mercenary from South Africa.  
The story was originally set in the Kimberley region but filming there would have been logistically impossible, so it was set in Queensland.  

Ten percent of the budget was provided by the Queensland Film Corporation. The films complete budget of $2.75 million was over subscribed by more than $1 million at the end of the 1983-84 financial year and the movie had a 30% combined foreign and Australian pre sale when production started.  The movie was shot in and around Charters Towers.  Dimsey later estimated that buying the rights to first day of filming took three months. 

==Reception==
The film performed poorly at the Australian box office but did better overseas, particularly in France and Germany, and Ross Dimsey says it got most of its money back. Dimsey says he regrets pulling back on the romantic plot and he felt the movie was a little old fashioned. He felt it was made under circumstances that were too rushed and the script had not been developed. 
==References==
 

==External links==
*  at IMDB
 
 
 
 
 
 