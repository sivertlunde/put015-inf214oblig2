Era Bator Sur
{{Infobox Film
| name           = Era Bator Sur
| image          = 
| image_size     = 
| caption        = 
| director       = Dr Bhupen Hazarika
| producer       =  BP Films
| writer         = 
| narrator       = 
| starring       = Phani Sarma  Bishnu Rabha  Balraj Sahani
| music          = Bhupen Hazarika|Dr. Bhupen Hazarika
| cinematography = 
| editing        = 
| distributor    = 
| released       = 30 November 1956
| runtime        = 
| country        =   Assamese
| imdb_id        = 
}} 1956 directed by Bhupen Hazarika    and produced by BP Films.
The film was choreographed by Priyambada Patel.

==Background==
Era Bator Sur is the first film of Dr Bhupen Hazarika. Dr. Bhupen Hazarika was then an active member of Indian people’s Theater Association and in the fifties almost all the workers of I. P. T. A. were influenced by the sorrow and happiness, struggle and hope of the common mass and made themselves associated with creative works related to folk music and culture of the common Assamese people. The film was a result of one such effort. The story and the music of Era Bator Sur reflects the emotional rising of the people of that era. The theme of this movie is based on characters belonging to tea labourers society of Assam. The exploitations carried out by one class of the society also finds importance in the movie.

==Cast   ==
*Phani Sarma
*Bishnu Prasad Rabha
*Balraj Sahni
* Bijoy Shankar
* Iva Achao
* Tassaduk Yusuf
* Anil Das 
* Punya Dowerah etc.

==See also==
*Jollywood

==References==
 

==External links==
* 

 
 
 
 


 