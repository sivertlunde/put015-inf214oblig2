The Glory Guys
  
{{Infobox Film
| name           = The Glory Guys
| image          = Glorguypos.jpg Frank McCarthy
| director       = Arnold Laven
| writer         = Sam Peckinpah James Caan Michael Anderson, Jr.
| music          = Riz Ortolani
| cinematography = James Wong Howe
| titles         = Format Productions
| distributor    = United Artists Levy-Gardner-Laven
| released       =  
}}
 James Caan, and Michael Anderson, Jr. The films screenplay was written by Sam Peckinpah long before the 1965 film was made.  The director was Arnold Laven.  Riz Ortolani composed the score and the title song.

Though a fictionalized Western based on George Armstrong Custers 7th Cavalry Regiment at the Battle of the Little Big Horn, the film is almost a generic war story covering the enlistment, training, and operational deployment of a group of recruits that could take place in any time period.

The large-scale film was made in Durango, Mexico, with large numbers of mounted extras and the final battle scene choreographed on   of land.  

The titles were drawn by Joseph Mugnaini for Format Productions.  Cover versions of the title song were done by Al Caiola and sung by Frankie Laine.

==Cast==
*Tom Tryon as Capt. Demas Harrod
*Harve Presnell as Scout Sol Rogers
*Senta Berger as Lou Woddard
*Michael Anderson Jr. as Trp. Martin Hale
*James Caan as Trp. Anthony Dugan
*Slim Pickens as Sgt. James Gregory
*Erik Holland as Trp. Clark Gentry Adam Williams as Trp. Lucas Crain
*Andrew Duggan as Gen. Frederick McCabe
*Peter Breck as Lt. Bunny Hodges
*Laurel Goodwin as Beth Poole
*Jeanne Cooper as Mrs. Rachael McCabe
*Robert McQueeney as Maj. Oliver Marcus
*Wayne Rogers as Lt. Mike Moran
*Michael Forest as Fred Cushman

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 