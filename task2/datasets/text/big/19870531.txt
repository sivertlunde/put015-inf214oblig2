Modern Vampires
{{Infobox film
| name           = Modern Vampires
| image          = Modern Vampires.jpg
| caption        =
| director       = Richard Elfman
| producer       = Chris Hanley Brad Wyman
| writer         = Matthew Bright
| music          = Michael Wandmacher Danny Elfman
| cinematography = Robin Brown
| editing        = Larry Bock
| starring       = Casper Van Dien Natasha Gregson Wagner Rod Steiger Kim Cattrall Natasha Lyonne Craig Ferguson Natasha Andrejchenko Udo Kier Gabriel Casseus Robert Pastorelli Conchata Ferrell
| distributor    = 
| released       = 
| runtime        = 91 minutes
| country        = United States
}} 1998 black horror/comedy film that was released Oct 19, 1999 straight to video, written by Matthew Bright and directed by Richard Elfman.

Most of the storyline is about a vampire named Dallas, a young WW2 veteran who was banished by "The Count" (the films interpretation of Count Dracula, who is mostly portrayed as a mob-boss type figure). Dallas is given a chance to visit America for the first time in years while unknowingly being pursued by Dr. Fredrick Van Helsing (portrayed as a former Nazi doctor) who hires a group of street thugs to help him do his dirty work. At the same time, we are introduced to Nico, a sultry vampire posing as a prostitute who lures unsuspecting victims to their doom.

==Plot==
Dallas (Casper Van Dien) is not a very discriminating vampire. Years ago, in an effort to save the life of Hans, the crippled son of famed Viennese vampire hunter Doctor Frederick Van Helsing, he turned him into a vampire, without permission from The Count (Robert Pastorelli). Its later revealed that he turned young and waifish Nico (Natasha Gregson Wagner), who has since been on a killing spree across Los Angeles earning her the nickname "Hollywood Slasher", which infuriates The Count, who orders her immediate death. Even with both Van Helsing and The Count on his tail and after 20 years in exile, Dallas cant resist returning to L.A. to pay a visit to his dearest friends — beautiful Ulrike (Kim Cattrall), urbane Vincent (Udo Kier), artist Richard (Craig Ferguson) and Richards ever-pregnant wife Panthia (Natasha Andrejchenko). To celebrate Dallas return, the five of them decide to spend the night clubbing in one of The Counts many vampire bars. Unfortunately, The Count is also there. When he sees Dallas is back in town, he gives him three days to leave or else.

Dr. Van Helsing (Rod Steiger), leader of Van Helsings Institute of Vienna, also has his eye on the band of merrymaking vampires. "Its an infestation" he concludes, and realizes that he cant handle the extermination alone. So, he puts an ad in the newspaper looking for a "strong and brave young man whos not afraid to get his hands dirty in a cause that is holy." What he gets is Crips member Time Bomb (Gabriel Casseus), who doesnt believe in vampires but is willing to put a stake through anyones heart for cold hard cash. The pair succeed in killing Vincent.

After killing Vincent, Van Helsing tells Time Bomb that 20 years ago his son Hans became good friends with Dallas who promised to save his life from a fatal genetic disorder by turning him into a vampire. He eventually discovered on his own by finding out his wife was secretly visiting Hans late night at a family mausoleum. He then killed Hans with a stake through the heart, and mentions that after killing Hans, his wife committed suicide out of grief. Van Helsing makes it abundantly clear that he will destroy Dallas.

Dallas is feeling protective (and a little curious) about his protegee Nico, so he drives around Hollywood Boulevard that night until he spots her. After sharing a bit of each others blood, Dallas reveals who he is and explains that the vampire community is out to get her. Theyre afraid that Nico will get herself arrested and then the whole world will know that vampires exist. Dallas forces her to move from the junked oil tanker in which she sleeps and to come live with Richard and Panthia who clean her up and take her shopping for some new clothes. Panthia develops a mother-like complex towards Nico and starts treating her like a daughter.

When they pick another victim at a late night coffee shop, Nico makes a human friend named Rachel (Natasha Lyonne). But, when they return to Richard and Panthias house, they find Ulrike sitting on her car hood, crying. She has discovered Vincents decaptiated body. Knowing that this was Van Helsings handywork, Dallas theorizes that hes closing in on them. Dallas then decides to take Nico to see her mothers house since Nico doesnt remember much about her past during her human life. This turns out to be a bad idea, as Nico has him kill her verbally and sexually abusive stepfather as payback for all that "sex shit" he did to her when she was little. Nico exchanges heated words with her neglectful mother, who she claims never loved her. As they leave the trailer park, they are attacked by four of The Counts henchmen. Dallas and Nico manage to shoot three of them and set fire to the fourth.

Dallas then tells his side of the story to Nico - Originally Dallas was ordered to kill Van Hellsing, but he ended up befriending Hans and decided not to. After saving Hans from the genetic disorder, he hoped that his fathers attitude would change towards the vampire community, but it didnt. Van Helsing became vengeful towards Dallas and killed Hans. The Count then banished Dallas as punishment for not carrying out his order to kill him.

The next night, while Richard, Panthia, and Ulrike are sitting around the living room and listening to music, Nico gets a call from her new friend Rachel and Invites Nico out to a small party with some delinquent teenagers, and Nico is eager to go. Meanwhile, Dallas pays a visit to The Count and begs to be allowed to train Nico. The Count will have none of it and advises Dallas to deliver Nico to him or he and everyone he cares about "will die screaming".

Having come to the conclusion that even two vampire hunters will not be enough against the vampire infestation, Van Helsing and Time Bomb bring in three more Crips members - Soda Pop (Victor Togunde), Lil Monster (Cedrick Terrell), and Trigger (Flex Alexander). While Nico and Dallas are out, Van Helsing and his four employees storm the house and drive stakes through Richard and Panthia. They tie Ulrike to a bedpost, but she taunts the Crips into having sex with her before Van Helsing finally stakes her, too. Little do they know that to have sex with a vampire turns a person into a vampire. Suddenly, Nico walks in, horrified at the carnage. She attacks Trigger, who pulls a gun and tries to get off a shot. The bullet hits Rachel instead. Nico grabs the gun and shoots Trigger. Time Bomb subdues Nico with a rope of garlic, just as Dallas returns home and comes face to face with Van Helsing. In exchange for letting Nico go, Dallas offers to lead Van Helsing to The Count. So, Van Helsing, Time Bomb, Trigger, Lil Monster, Soda Pop, Nico, Rachel and Dallas pile into Van Helsings van and head for The Counts house. Unfortunately, The Counts henchmen intercept them and take Nico and Van Helsing to The Count, where he hooks up Nico to a blood-draining machine and places Van Helsing in a box with only his head sticking out. Just as The Count is about to crack open Van Helsings skull with a hammer, Dallas drives the van through the wall of the club. Dallas and the Crips, now vampires, come out shooting. Many bullets and stakes later, The Count and his henchmen are destroyed, Nico is set free, Rachel has been made into a vampire, and Nico, Rachel, and Dallas have decided to move to New York. In the closing scene, a pajama-clad Van Helsing runs down an alley, screaming for forgiveness from his son. As he utters apologies to God and his son Hans, he screams for help, revealing his new vampire fangs in front of two policemen.

==Cultural references== The Outsiders.
*In the movie, vampirism is viewed as more of a sexually transmitted infection than a simple bite to the neck.

==Differences between R Rated and Unrated Versions==
*The DVD version is unrated and contains more explicit gore than the edited R-rated VHS release.
*In the Spanish video and DVD version by Manga Films, the title does not appear onscreen during the opening credits.

==Reception==
The movie has received third place Best International Film award at the Fant-Asia Film Festival in 1997. 

==See also==
*Vampire film

==References==
 

==External links==
* 
* 

 

 
 
 
 
 