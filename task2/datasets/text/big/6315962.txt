The Making of an American
{{Infobox film
| name           = The Making of an American
| image          = File:Advertisement_for_1920_silent_film_The_Making_of_an_American.jpg
| alt            = 
| caption        = Advertisement for film in Educational Film Magazine
| film name      = 
| director       = Guy Hedlund
| producer       = Worcester Film Corporation
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| starring       = Emile De Verny
| narrator       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = State of Connecticut Department of Americanization Worcester Film Corporation
| distributor    = Worcester Film Corporation
| released       = 1920
| runtime        = 14 minutes
| country        = United States Silent
| budget         = 
| gross          = 
}} assimilate immigrants into mainstream culture, especially by encouraging them to learn the English language. It was produced for the State of Connecticut Department of Americanization by the Worcester Film Corp., a company founded in 1918 in Worcester, Massachusetts.The film was rediscovered and preserved by Northeast Historic Film (NHF) a regional moving image archive in New England. 

==Synopsis of film==
An enterprising young Italian who comes to America is forced to take a position as a day laborer, which is far below his ability and standard of living, solely because he cannot speak English. Even a laborer, however, must know the language of the country where he is employed, as Pete soon found to his cost. An unattended freight elevator, a sign in English that he could not read, a struggle of an instant, and then the hospital. It was a sadder and wiser man who came out a few weeks later. When he passed the post office, and a saw a sign in several languages calling upon foreigners to learn English, and to attend night school, he was prepared for the message that was destined to change the entire course of his life. Night school for Pete was the result. Anyone familiar with such work will experience anew the keen realization of what it means to the newcomer, the crowded roomful of eager listeners, trying so hard, following so patiently and docilely, the enthusiastic teachers efforts, in short, the making of Pete. He now is able to secure a suitable position and rises rapidly.
:Education Film Magazine (1920)
===Cast===
*Emile De Verny - Italian immigrant

==Production and background== industrial workers 28mm film stock, an extremely rare format that was primarily used for educational film distribution from 1913 to 1923. 

In 2005, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

== See also ==
*Immigration to the United States of America
*Melting pot
*Salad bowl (cultural idea)

==References==
 

== External links ==
* 
*  
* 
* 

 
 
 
 
 
 
 

 