Elvis: That's the Way It Is
{{Infobox film
| name           = Elvis: Thats the Way It Is
| image          = Thats the Way It Is.jpg
| image_size     =
| caption        =
| director       = Denis Sanders
| producer       =
| writer         =
| narrator       =
| starring       = Elvis Presley
| music          = 
| cinematography = Lucien Ballard
| editing        = Henry Berman
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Las Vegas during August 1970. It was his first non-dramatic film since the beginning of his film career in 1956, and the film gives a clear view of Presleys return to live performances after years of making films.

==Overview==
The original concept as devised by technical advisor Colonel Tom Parker was in view of Elviss triumphant return to live performances was a closed circuit television presentation of one show.
 International Hotel in Las Vegas, there are several other parts to the film:

*The opening credits sequence contains footage of Elvis show at Arizona Veterans Memorial Coliseum in Phoenix on September 9, 1970. This was the first show of Elvis first tour in 13 years. Little Sister", "Words (Bee Gees song)|Words", "Thats All Right Mama", and "The Next Step Is Love." The rehearsal sequences were filmed during late July 1970.
*Later rehearsals show Elvis in Las Vegas with his back-up vocalists The Sweet Inspirations, Millie Kirkham and The Imperials, preparing songs such as "You Dont Have to Say You Love Me" and "Bridge Over Troubled Water".
*There is also a session of rehearsals that takes place in the Showroom Internationale of the International Hotel (now known as the LVH – Las Vegas Hotel and Casino) in Las Vegas. Together, Elvis and the entire group run through songs from "Mary In the Morning" to "Polk Salad Annie". These rehearsals took place on August 7, 1970. Radio Luxembourg DJs Tony Prince and Peter Aldersley are on hand to lead the festivities. A tandem bicycle owned by Elvis is raffled off to a lucky fan in the audience. Additionally, various musicians are seen performing their own versions of Elvis songs.

===Onstage in Las Vegas===

The Elvis Summer Festival at the International Hotel began on August 10, 1970, and the MGM film crew was on hand to film this show as well as the evening and midnight performances of August 11, 12 and 13. He sings many well-known songs, including several of those that he had been seen rehearsing earlier in the film. The songs are:
 Tiger Man
*Thats All Right Mama
*Ive Lost You 
*Love Me Tender (song)
*Patch It Up
*Youve Lost That Lovin Feelin
*I Just Cant Help Believin  Tiger Man 
*Sweet Caroline
*Heartbreak Hotel One Night
*Blue Suede Shoes
*All Shook Up
*Polk Salad Annie
*Bridge Over Troubled Water 
*Suspicious Minds 
*Cant Help Falling in Love
*Dont Be Cruel
*You Dont Have to Say You Love Me
 George Hamilton, Juliet Prowse and Xavier Cugat) arriving for opening night of the show.

===Shows/Rehearsals===

* July 14 rehearsal (M.G.M. Stage 1, Culver City, California)
* July 15 rehearsal (M.G.M. Stage 1, Culver City, California)
* July 24 rehearsal (R.C.A. studios, Hollywood, California)
* July 29 rehearsal (M.G.M. studios, Culver City, California)
* August 4 rehearsal (Convention Center, International Hotel, Las Vegas, Nevada)
* August 7 stage rehearsal
* August 10 stage rehearsal
* August 10 opening night show
* August 11 dinner show
* August 11 midnight show
* August 12 dinner show
* August 12 midnight show
* August 13 dinner show
* September 9 (Arizona Veterans Memorial Coliseum, Phoenix)

==Cast==
*Elvis Presley
*The Imperials (vocals)
**Terry Blackwood
**Armond Morales
**Joe Moscheo
**Jim Murray
**Roger Wiles
*The Sweet Inspirations (vocals)
**Estell Brown
**Myrna Smith
**Sylvia Shemmell Ann Williams
*TCB Band
**Joe Guercio (orchestra conductor)
**James Burton (lead guitar) Glen D. Hardin (piano, Rhodes piano) Charlie Hodge (acoustic guitar, harmony vocals)
**Jerry Scheff (bass guitar)
**Ron Tutt (drums) John Wilkinson (rhythm guitar) Richard Davis Joe Esposito
*Felton Jarvis
*Millie Kirkham (vocals)
*Del Sonny West
*Red West

==Reception==

The concert film reached #22 on the Variety National Box Office Survey on its original theatrical release in 1970.

==Soundtrack album==
 

A soundtrack album was released in conjunction with the film. It was Presleys first full-length film soundtrack album since 1968s Speedway (film)#Soundtrack|Speedway, and the last as his final film, 1972s Elvis on Tour, did not have a soundtrack release. It was only a partial soundtrack release, as it did not include all of the live performances, and substituted studio recordings for some of the songs performed in the film (some of these recordings can actually be heard playing during the film itself).

A Special Edition was released in 2001 in conjunction with the Special Edition of the movie. This consisted of three discs:

* Disc one   : Original album plus bonus songs
* Disc two   : 12 August Midnight Show
* Disc Three : Unreleased Live songs and rehearsals

However the following shows are also available on CD:

* One Night in Vegas (10 August Opening Show)
* Live in Las Vegas (Disc 2) (11 August Midnight Show)
* The Wonder Of You (13 August)

==2001 version==
In 2001, a new version of Thats the Way it Is was compiled. The new version eliminated much of the documentary and non-Elvis content of the original in favor of adding additional performances of Elvis rehearsing and in concert. The final film runs 12 minutes shorter than the original, but contains more music, although several performances included in the original film are omitted (most notably the concert performance of "I Just Cant Help Believin", even though the new version of the film features footage of Presley rehearsing the song and being concerned about remembering its lyrics on stage).

The special edition was released on January 19, 2001, when this new version made its worldwide debut on the cable network, Turner Classic Movies and produced by award winning producer Rick Schmidlin.

In August 2007 a two-disc DVD "special edition" was released by Warner/Turner that has both the reworked version plus the original cut. The original, however, has only a mono soundtrack  . The DVD also includes approximately 35 minutes of additional performances and other footage that was not included in either edition.

==2014 version==

A Two Disc Special Edition Premium Digibook was announced for release in August 12, 2014. 
With thousands of feet of materials including sequences added to capture with greater intimacy Elvis performances and his creative process behind-the-scenes, the previously released Special Edition is now being made available as a Blu-ray two disc Special Edition Premium Digibook. Denis Sanders (Shock Treatment) directed this "rockumentary." Academy Award® nominated Lucien Ballard (The Wild Bunch) was the cinematographer.
Disc 1 (DVD) 2001 Special Edition and Special Features:
- Patch It Up: The Restoration of Elvis: Thats The Way It Is
- 12 Outtakes - song/nonmusical sequences 
- 1970 Original Theatrical Version
Disc 2 (BD) 1970 Original Theatrical Version and Special Features:
- 12 Outtakes - song/nonmusical sequences

==See also==
*List of Elvis Presley films
*Elvis Presley discography

==External links==
*  Website dedicated to Elvis Presleys Movies.
* 
* 
* Elvis performing I Just Cant Help Believin from Elvis: Thats the Way It Is  
* Elvis performing Bridge Over Troubled Water from Elvis: Thats the Way It Is  

===DVD reviews===
*  by Ian Jane at  , August 27, 2007.

 

 
 
 
 
 
 
 
 
 
 
 