Warming Up (1928 film)
 
{{Infobox film
| name           = Warming Up
| image          =
| caption        =
| director       = Fred C. Newmeyer
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Ray Harris (screenplay) Sam Mintz (story) George Marion, Jr. (intertitles)
| starring       = Richard Dix Jean Arthur
| music          = Gerard Carbonara
| cinematography = Edward Cronjager
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States English intertitles; also sound version with music and sound effects only
| budget         =
| gross          =
| awards         =
}}
Warming Up (1928 in film|1928) is a baseball film starring Richard Dix and Jean Arthur, directed by Fred C. Newmeyer, and released by Paramount Pictures in the Movietone sound system as Paramounts first sound film. 
 silent version sound version. The sound version had synchronized music and sound effects without dialogue. 

The film featured several major league baseball players as themselves.

==Cast==
*Richard Dix 	... 	Bert Tulliver
*Jean Arthur 	... 	Mary Post Claude King 	... 	Mr. Post
*Philo McCullough	... 	McRae
*Billy Kent Schaefer ... 	Edsel
*Roscoe Karns ... 	Hippo
*James Dugan  ... 	Brill
*Mike Donlin ... 	Veteran Baseball Player/Himself
*Mike Ready	  ... 	Himself Chet Thomas  ... 	Himself
*Joe Pirrone   ... 	Himself Wally Hood   ... 	Himself
*Bob Murray  ... 	Himself
*Truck Hannah ... 	Himself

==Plot==
Bert Tulliver (Dix), a pitcher for a baseball team in a small town, is given the opportunity to try out for a team in the big leagues. Unfortunately, he incurs the enmity of McRae (McCullough), the leagues leading home-run hitter. In addition, Bert falls for the team owners daughter Mary (Arthur), who McRae has designs on.

==Preservation status==
According to notes on the Wikipedia entry for Richard Dix, this film is now considered a lost film.

==See also==
*List of lost films
*Movietone sound system

==References==
 

==External links==
*  
* 

 

 
 
 
 