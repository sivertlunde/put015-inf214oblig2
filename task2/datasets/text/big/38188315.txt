5 June (film)
{{Infobox film
| name           = 5 June 
| image          =
| image_size     =
| caption        =
| director       = Fritz Kirchhoff 
| producer       = Walter Ulbrich
| writer         =  Walter Ulbrich
| narrator       =
| starring       = Carl Raddatz  Joachim Brennecke   Karl Ludwig Diehl   Gisela Uhlen
| music          = Georg Haentzschel
| cinematography = Walter Pindter
| editing        = Walter Wischniewsky UFA
| distributor    =  UFA
| released       = 25 August 1942
| runtime        = 99 minutes
| country        = Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} invaded France. on location Minister of Vichy government of France. 

==Main cast==
* Carl Raddatz as Feldwebel Richard Schulz 
* Joachim Brennecke as Gefreiter Eickhoff 
* Karl Ludwig Diehl as Generalmajor Lüchten 
* Gisela Uhlen as Luise Reiniger  Paul Günther as Hamann 
* Ernst von Klipstein as Oberleutnant Lebsten 
* Gerhard Geisler as Stabsfeldwebel Eickhoff  Hans Richter as Norbert Nauke 
* Josef Kamper as Klawitter 
* Werner Völger as Retzlaff

==References==
 

==Bibliography==
* Eltin, Richard A. Art, Culture, and Media Under the Third Reich. University of Chicago Press, 2002.
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945.University of California Press, 1999.
* Hull, David Stewart. Film in the Third Reich: A Study of the German Cinema, 1933-1945. University of California Press, 1969

==External links==
* 

 
 
 
 
 
 
 
 
 


 