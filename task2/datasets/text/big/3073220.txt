Accattone
{{Infobox film name = Accattone image = Accattone.jpg caption = Promotional Poster director = Pier Paolo Pasolini producer = Alfredo Bini Cino Del Duca writer = Sergio Citti Pier Paolo Pasolini starring = Franco Citti Franca Pasut Silvana Corsini music = Johann Sebastian Bach cinematography = Tonino Delli Colli editing = Nino Baragli studio    = Arco Film distributor = Brandon Films released =   runtime = 120 minutes country = Italy language = Italian budget = 
}} Boys of Life and A Violent Life.  It is Pasolinis first film as director, employing what would later be seen as trademark Pasolini characteristics; a cast of non-professional actors hailing from where the movie is set, and thematic emphasis on impoverished individuals.
 Le notti di Cabiria and considered cinema to be writing with reality. The word "Accattone" is a slang term mainly used for beggars, referring to people who never do well, who are lazy, and who rarely hold down a job. 

Accattone is a story of pimps, prostitutes and thieves, the same topic as his novels. Peasant culture is celebrated, in contrast to Italys postwar economic reforms. Pasolini’s choice of topics was scandalous, as was his blurring of the lines between the sacred and the profane. Although Pasolini tried to distance himself from Italian neorealism|neorealism, the film is considered to be a kind of second neorealism, with one critic believing it "may be the grimmest movie" hed ever seen. 

==Plot==

Vittorio (Franco Citti), nicknamed "Accattone" (meaning beggar in Italian), leads a mostly serene life as a pimp until his prostitute, Maddalena, is hurt by his rivals and sent to prison. Finding himself without a steady income, and not much inclination for working himself, he discovers the naive Stella and tries to lure her into prostituting herself for him. She is willing to try, but when a client begins pawing her she cries and gets out of the car. Accattone tries to support her, but gives up on honest labor after one day, and following a bizarre vision of Pasolinis own death, is killed in a traffic accident when he tries to evade the police on a stolen motorcycle.

==Cast==

*Franco Citti as Vittorio "Accattone" Cataldi
*Franca Pasut as Stella
*Silvana Corsini as Maddalena
*Paola Guidi as Ascenza
*Adriana Asti as Amore
*Luciano Conti as Il Moicano
*Luciano Gonini as Piede DOro
*Renato Capogna as Renato
*Alfredo Leggi as Papo Hirmedo
*Galeazzo Riccardi as Cipolla
*Leonardo Muraglia as Mammoletto
*Giuseppe Ristagno as Peppe
*Roberto Giovannoni as The German
*Mario Cipriani as Balilla
*Roberto Scaringella as Cartagine
*Silvio Citti as Sabino
*Monica Vitti (uncredited) as Ascenza (voice)

==Awards== BAFTA Award for Best Foreign Actor in 1963 for his title role. 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 