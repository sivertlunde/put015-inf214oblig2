We Joined the Navy
{{Infobox film
| name           = We Joined the Navy
| image          = 
| image_size     = 
| caption        = 
| director       = Wendy Toye
| producer       = Daniel Angel
| writer         = 
| narrator       = 
| starring       = Kenneth More
| music          = 
| cinematography = Otto Heller (CinemaScope)
| editing        = 
| studio         = Dial   Associated British Picture Corporation
| distributor    =  Associated British Picture Corporation|Warner-Pathé Distributors
| released       = 29 November 1962 (London premiere)
| runtime        = 106 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}

We Joined the Navy is a 1962 British comedy film produced by Daniel M. Angel and directed by Wendy Toye which stars Kenneth More, Lloyd Nolan, Joan OBrien, Derek Fowlds, Graham Crowden, Esma Cannon and John Le Mesurier. It was based on the novel of the same name by John Winton.

The film was shot on location in Villefranche-sur-Mer, near Cannes and at ABPC Elstree Studios. 

In addition to the credited cast, there were uncredited cameos from Michael Bentine, Sidney James and Dirk Bogarde (in a gag reference to his Doctor series role, Simon Sparrow).

==Cast==
* Kenneth More as Lieutenant Commander Robert Badger
* Lloyd Nolan as Vice Admiral Ryan
* Joan OBrien as Lieutenant Carol Blair
* Mischa Auer as Colonel & President
* Jeremy Lloyd as Dewberry Jr.
* Dinsdale Landen as Bowles
* Derek Fowlds as Carson
* Denise Warren as Collette
* John Le Mesurier as George Dewberry Sr.
* Lally Bowers as Mrs. Cynthia Dewberry, his wife
* Laurence Naismith as Admiral Blake
* Andrew Cruickshank as Admiral Filmer
* Walter Fitzgerald as Admiral Thomas John Phillips as Rear Admiral
* Ronald Leigh-Hunt as Commander Royal Navy
* Arthur Lovegrove as Chief Petty Officer Froud
* Brian Wilde as Petty Officer Gilors David Warner as Sailor Painting Ship
* John Barrard as Consul

==DVD==
A Region 2 DVD of We Joined the Navy was released by Network on 16 February 2015. The disc features a 2.35:1 anamorphic transfer.
==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 