Andala Rakshasi
{{Infobox film
| name           = Andala Rakshasi
| image          = Andala Rakshahi Poster.jpg
| caption        = Theatrical release poster
| director       = Hanu Raghavapudi Sai Korrapati S. S. Rajamouli   
| writer         = varun prabhas
| screenplay     = varun prabhas
| starring       = Naveen Chandra Rahul Ravindran Lavanya Tripathi chandravyas
| music          = Radhan
| cinematography = Murali G.
| art director   = S.Ramakrishna
| editing        = Chandrasekhar G. V. 
| studio         = Varahi Chalana Chitram SVC Cinemas 14 Reels  (overseas)    
| released       =  
| runtime        = 
| country        = India
| language       = Telugu
| budget         = 
| gross          = 
}} Telugu romance film written and directed by Hanu Raghavapudi as his first directorial effort. The film, produced by Sai Korrapati and S. S. Rajamouli under Vaaraahi Chalana Chitram banner debuts Naveen Chandra, Rahul Ravindran and Lavanya Tripathi.  The soundtrack of the film was composed by Radhan and cinematography was handled by Murali. The film was released worldwide on August 10, 2012. 

==Plot==
“Love is the irresistible gravity between hearts that pulls them into the game of fate.”

The story of Andhala Rakshasi is set during the days of 1991. It is centre-staged on three pure hearts Surya, Gowtham & Midhuna and is about falling in love, forgetting and waiting for love. The story celebrates love as well as exposes its dark side of tormenting its patrons in pursuit of it. The three characters resemble the three celestial bodies Sun, moon and Earth. Set in motion by the force of love, the three individuals can neither come close nor cut off but are destined to revolve around the dear ones. In this pursuit of pure love they will be thrown to destinies that they never imagined.

The story revolves around Midhuna (Lavanya), a beautiful small town girl, Gautham (Rahul), a part-time guitarist, and Surya (Naveen), a scrap artist. Gautham and Surya fall in love with Midhuna. She means everything to them but they do not know each other.

Gautham, the only son of a rich dad, working as a part-time guitarist in a local pub falls in love with Midhuna at first sight for her innocence and follows her. Later he goes out of town for a while. In Gauthams absence, Midhuna meets with a fatal accident and Gauthams father helps to cure her, with a marriage proposal in return. Later Gautham reaches the hospital,  finds out her name and takes care of her. Mithuna suffers four months in coma. He also finds out Midhuna is in love with another guy, Surya, who died in the same accident. After a few months, on the marriage day of herself and Gautham, Midhuna asks for sometime to forget Surya. Gautham takes her to his house at a hill station. Midhuna is unable to forget the memories of Surya. All the pain suffers Gautham. After many days, Mithuna starts to have feelings for Gautham regarding the respect and the true love of him for her. she asks Gautham to marry her. He feels very happy to hear that but fate plays a twist in their lives. Gautham discovers that Surya is alive and living distant to Midhuna. Why?

A few months ago, Surya, a street guy, falls in Love with Midhuna at first sight while travelling in a bus. Surya makes several attempts to gain Midhunas love and finally succeeds. Soon after, Midhuna meets with a car accident and her parents are unable to bear the cost of the treatment. Gautham father arrives in the picture and makes a marriage proposal. Midhunas father requests Surya to leave for her life. Surya who has a heart break revolves on the roads of Anantagiri. After knowing the truth, Gautham tells Midhuna that Surya is alive. She gets emotional but refuses to marry him. Gauthams father plans to kill Surya for the love of his son. Gautham comes to know the plan and tells his father that he is getting killed instead of Surya, in the hands of paid murderers on the telephone. Gautham sacrifices his life for Midhuna after getting stabbed and hitting a vehicle. Before he dies, he feels Midhuna is walking away in front of his car. Midhuna feels heartbreak for the second time. Later Midhuna and Surya meets each other . Gauthams love succeeds when Midhuna asks Surya for time to forget Gautham. Later it is revealed that the dead man at graveyard is Gauthams father who was missing for a while.

==Cast==
* Lavanya Tripathi as Midhuna
* Naveen Chandra as Surya
* Rahul Ravindran as Gautham
* C.V.L. Narasimha Rao Pragathi
* Vijay

==Production== SVC Cinemas.  Later through his Twitter account, Rajamouli announced that he bought a stake in the film and would now act as the co-producer for it.   On July 31, it was reported that the film was censored by the Central Board of Film Certification and was given a U/A certificate.     The overseas theatrical distribution rights of the film were sold to 14 Reels Entertainment and they collaborated with Ficus for the release.  The television rights were bought by Maa TV and was telecast on September 25, 2012 at 7pm.

==Soundtrack==
{{Infobox album
| Name = Andala Rakshasi
| Type = Soundtrack
| Artist = Radhan
| Cover =
| Released = June 2, 2012
| Recorded = 2012 Feature film soundtrack
| Length = 22:56 Telugu
| Label = Vel Records
| Producer = Radhan
}}

The soundtrack of the film was composed by Radhan. The audio of the film was launched in Hyderabad on June 2, 2012 through Vel Records.  The album consists of six songs, which are penned by Rakendu Mouli, Krishnakanth, Vasishta Sharma and Lakshmi Bhupal. Reviewer from IndiaGlitz reviewed the album and said "Andala Rakshasi could have been a better album by cutting short the length of at least three songs.  The songs are situation-based, and like all songs belonging to this category, they could work in the film, more than the audio. Even so, this is a good offering from a team of fresh singing, writing and musical talent."  Kathik from Milliblog reviewed the album and praised Radhan for his efforts. 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 22:56
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Sound Of Vel
| note1           = 
| lyrics1         = 
| extra1          =
| length1         = 00:29
| title2          = Yemito
| note2           =  
| lyrics2         = Rakendu Mouli
| extra2          = Haricharan 
| length2         = 04:28
| title3          = Manasu Palike
| note3           = 
| lyrics3         = Rakendu Mouli
| extra3          = Rakendu Mouli
| length3         = 03:48
| title4          = Ne Ninnu Chera 
| note4           = 
| lyrics4         = Krishnakanth
| extra4          = Ranjith (singer)|Ranjith, Veena Ghantasala
| length4         = 03:29
| title5          = Ye mantramo
| note5           = 
| lyrics5         = Vasishta Sharma
| extra5          = Bobo Shashi
| length5         = 02:47
| title6          = Manasa Marchipo
| note6           = 
| lyrics6         = Lakshmi Bhupal
| extra6          = Sathya Prakash, Bhargavi Sridhar
| length6         = 04:28
| title7          = Vennante 
| note7           = 
| lyrics7         = Krishnakanth
| extra7          = Ranjith
| length7         = 03:27
}}

==References==
 

==External links==
*  

 
 
 
 