Wonderwall (film)
 
 
{{Infobox film
| name           = Wonderwall
| image          = Wonderwall (film).jpg
| image size     = 225px
| caption        = DVD cover
| director       = Joe Massot
| producer       = Andrew Braunsberg
| story          = Gérard Brach
| screenplay     = Guillermo Cabrera Infante
| starring       = Jane Birkin Jack MacGowran Iain Quarrier
| music          = George Harrison The Remo Four Eric Clapton
| cinematography = Harry Waxman
| editing        = Rusty Coppleman
| distributor    = Compton-Cameo Films
| released       = 17 May 1968 (Cannes Film Festival|Cannes) 12 January 1969 (UK)
| runtime        = 85 minutes
| country        = UK
| language       = English
| budget         =
| gross          =
}}
 cameo by The Fool, who were also set designers for the film. 

The film is best remembered for its soundtrack, composed by then-Beatle George Harrison.

==Plot==
The reclusive, eccentric scientist Oscar Collins (Jack MacGowran) has for next-door neighbours a pop photographer (Iain Quarrier) and his girlfriend/model Penny Lane (Jane Birkin). Discovering a beam of light streaming through a hole in the wall between them, Collins follows the light and spots Penny modelling for a photo shoot. Intrigued, he begins to make more holes, as days go by and they do more photo sessions. Oscar gradually becomes infatuated with the girl, and feels a part of the couples lives, even forsaking work to observe them. When they quarrel and the couple split, Penny takes an overdose of pills and passes out, but Oscar comes to her rescue.

==Cast==
  
*Jack MacGowran as Prof. Oscar Collins
*Jane Birkin as Penny Lane
*Irene Handl as Mrs. Peurofoy
*Richard Wattis as Perkins
*Iain Quarrier as Young Man
 
*Beatrix Lehmann as Mother
*Brian Walsh as Photographer
*Sean Lynch as Riley
*Bee Duffell as Mrs. Charmer
*Noel Trevarthen as Policeman
 

Cast notes:
*Suki Potier and Anita Pallenberg had small roles uncredited in the film as girls at a party. 

==Soundtrack==
  Beatle George Harrison, whom Massot approached specially for the project. Harrison had never done a film soundtrack, and told Massot he did not know how to do it, but when Massot promised to use whatever Harrison created, Harrison took the job.
 Indian music, Bombay in January 1968, then a selection of rock and other musical styles, at De Lane Lea Studios in London, England. Timing the segments with a stopwatch as he watched the unfinished film, Harrison built up a varied musical programme. The soundtrack album (Wonderwall Music), the first "solo" Beatles record, was released in November 1968 by Apple Records as the companys first LP. Wonderwall Music also appeared on compact disc in 1992, during reissues of the Apple catalogue, being reissued again in 2014.

 

==Release history==
The film premiered at the Cannes Film Festival on 17 May 1968, with George Harrison, his wife Pattie Boyd|Pattie, Ringo Starr, his wife Maureen Cox|Maureen, and the cast members of the film in attendance. The premiere in London was on 12 January 1969. The film won an award, but did not gain a proper distribution deal, and its showings were limited, leading some writers to mistakenly state that the film "was so poor, it was never seen by anyone".  A print did finally appear on the American midnight movies circuit in the 1970s, and on home video in the 1980s and 1990s, all of rather low technical quality.

In 1998, thirty years after the films release, and with Massot an established film director, he decided to restore and re-release his first film. Harrisons search for master recordings turned up a lyrical song, "In the First Place" by the Remo Four, which he hadnt submitted the first time around, believing Massot wanted only instrumental music. "In the First Place" was released as a single in 1999. Harrison is believed to have not only produced it, but sung and played on it, although he asked to be credited only as producer. Massot was happy to include the song in the restored film, which was released to critical acclaim the second time around, and got a distribution deal.

The restored version of Wonderwall, both in theatrical and Directors Cut versions, is currently available on DVD through Shout! Factory in the United States, with bonus features.  

==References==
Notes
 

==External links==
* 
* 
*  at TCM.com


 
 
 
 
 
 
 