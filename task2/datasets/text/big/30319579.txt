Agnidevan
{{Infobox Film |
  name           = Agnidevan |
  image          = Agnidevan.jpg |
  writer         = P. Balachandran | 
  starring       = Mohanlal, Revathi Menon|Revathi, Devan (actor)|Devan, Bharath Gopi, Jagadish|
  director       = Venu Nagavalli|
  producer       = | Alwin Antony
  released       = 1995|
  runtime        = 159 mins.|
  language       = Malayalam |
  music          = M. G. Radhakrishnan |
  cinematography = V. Jayaram|
  editing        = K. Narayanan|
  runtime        = 159 minutes|
  budget         = |
  followed by    = |
}}

Agnidevan (Malayalam : അഗ്നിദേവന്‍  n film in Malayalam written by P. Balachandran and directed by Venu Nagavalli, starring Mohanlal and Revathi Menon|Revathi. The Leadership over the family newspaper, causes the family to split apart and have problems. This is the main theme of the film, which is of the musical/drama genre. It is one of the successful film of the year along with the films Music.

== Plot ==
 Devan has a clinical, business oriented attitude to growing the newspaper industry that he heads, the younger brother Mohanlal feels very emotional and passionate about the craft of writing and reporting. He gives everything to enriching his relationships and cannot understand how to approach it with business goals because writing, in itself, marks tradition, legacy and so much more in their family of distinguished writers. How the younger brother won his legacy is the rest of film.

==Cast==

*Mohanlal as Aniyankuttan (Ravi Varma)
*Revathi as  Sudarshana Devan as  Appu (Rama Varma)
*Bharath Gopi as K. K. Menon
*Jagadish as  Murukan
*Mohan Jose as Company manager
*Rohini Hattangadi as Aniyankuttans Grandmother
*Beena Antony as Vasudeva Warriers Daughter
*P. Balachandran#Filmography|P. Balachandran as Edakka
*T. P. Madhavan

== Songs ==

The film includes songs written by lyricist Gireesh Puthenchery and composed by M. G. Radhakrishnan.  The songs became chartbusters especially Nilavinte Neela Bhasmam
Sung by M. G. Sreekumar is a hit.

{| class="wikitable"
|-
! Track Name !! Singers !!  Length
|-
| "Nilavinte Neela" || M. G. Sreekumar ||| 04:32
|-
| "Oru Poovithalin" || M. G. Sreekumar ||| 04:32
|- Mano ||| 03:45
|-
| "Sama Gana" || M. G. Sreekumar, K. S. Chitra ||| 04:38
|}

== References ==
 

==External links==
*   at the Malayalam Movie Database

 
 
 
 