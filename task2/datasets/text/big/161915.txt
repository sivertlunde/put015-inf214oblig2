I Never Sang for My Father

{{Infobox film
| name           = I Never Sang for My Father
| image          = I Never Sang for My Father poster.jpg
| image_size     =
| caption        = film poster
| director       = Gilbert Cates
| producer       = Gilbert Cates Robert Anderson
| starring       = Melvyn Douglas Gene Hackman Dorothy Stickney Estelle Parsons Elizabeth Hubbard Lovelady Powell
| cinematography = Morris Hartzband George Stoetzel
| editing        = Angelo Ross
| distributor    = Columbia Pictures
| released       =  
| runtime        = 92 min English
| music          = Al Gorgoni Barry Mann
| awards         =
| budget         =
}}
 American film, based on a play by the same name, which tells the story of a widowed college professor who wants to get out from under the thumb of his aging father yet still has regrets about his plan to leave him behind when he remarries and moves to California. It stars Melvyn Douglas, Gene Hackman, Dorothy Stickney, Estelle Parsons, Elizabeth Hubbard, Lovelady Powell and Conrad Bain.
 Robert Anderson from his play and directed by Gilbert Cates.
 Best Actor Best Actor Best Writing, Screenplay Based on Material from Another Medium.

==Plot summary==
Gene Garrison (Gene Hackman) is a widowed New York college professor who is in a long distance relationship with a woman in California. Gene wants to marry and move to California, where his girlfriend has her medical practice and is raising her children. His mother is sympathetic to Genes dreams of moving, although aware of the toll it could take on his father (Melvyn Douglas|Douglas). Family tension exists as his father disowned Genes sister (Estelle Parsons|Parsons), for marrying a Jew. When his mother suddenly dies, Genes plans are thrown into disarray. Gene has lived in the shadow of his towering father, who is expecting Gene to stay and watch over him. Gene must decide for himself if hell stay to care for  his father or finally move on with his life.

==Cast==
* Melvyn Douglas as Tom Garrison
* Gene Hackman as Gene Garrison
* Estelle Parsons as Alice
* Dorothy Stickney as Margaret Garrison
* Elizabeth Hubbard as Doctor Margaret Peggy Thayer
* Lovelady Powell as Norm
* Daniel Keyes as Dr. Mayberry
* Conrad Bain as Rev. Sam Pell Jon Richards as Marvin Scott
* Nikki Counselman as Waitress
* Carol Peterson as Nurse #1
* Sloane Shelton as Nurse #2
* James Karen as Mr. Tucker (old age home director)
* Gene Williams as Dr. Jensen (state hospital director)

==Production notes==
Director Gilbert Cates had been one of the producers of the original stage play.

The film was shot in various locations, including Southern California and the Great Neck - Douglaston area of New York. Applauded by critics and viewers, the film (and play)
predicted the coming of the sandwich generation, in this case, grown children and other family members helping their elderly parents who are up in age The Savages and Away from Her.

==References==
 

==External links==
*  
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 


 