Ice Kings
 

Ice Kings is an award winning documentary film by Craig E. Shapiro.  The film chronicles Mount Saint
Charles Academy hockey teams historic streak of 26 consecutive state
titles.

==Synopsis==

Ice Kings  is a 2006 documentary film that was produced, written and directed
by Craig E. Shapiro. The film captures the story of Rhode Island’s Mount Saint Charles Academy high school hockey team and their historic streak of 26 consecutive state titles. The film tells the incredible stories of
the young men who kept the streak alive and recaps the tale of the unlikely
public school that knocked the perennial kings from their throne. 
 football is in Texas."   

The film then turns its attention to the man behind the legacy, former Mount Saint Charles Academy rink manager turned coach, Bill Belisle. At that time Mount, a perennial contender for the state title had lost its luster, but with Belisles tough grit and hard nosed approach their legacy began. "It was said he knew how to fix the Zamboni, the soda machines, and the team."  Tales from former players and parents put a face on the historic streak and help explain how one man could will generation after generation to win.

Narrated by CBS Sports Bill Macatee, Ice Kings is an inspiring look at dedication, sacrifice, and one school’s remarkable willingness to win. Mark Patinkin  of the Providence Journal states, “High school hockey is a metaphor for the state, and the film gets you thinking about our mill-town heritage, and how the quest for pride and identity plays out here each season on the ice.”

==Critical reception==

In 2006, Ice Kings was screened at a number of film festivals and received critical acclaim. The film premiered at the 2006 Rhode Island International Film Festival,  where it won the Audience Award.

“Every so often, you come across a creation that captures part of Rhode
Island¹s soul. I just watched a movie that did that”
-Mark Patinkin, Providence Journal 

“Shapiros Ice Kings is a great sports story told well, and one that
deserves space in any hockey fans DVD collection”
- Eric McErlain, The Sporting News 

“A nifty story, even for non fans”
- Jay Seaver, efilmcritic 

==NHL Players in Ice Kings==
  Ed Lee and many others.

==The filmmaker==

Craig E. Shapiro most recently produced and directed a retrospective of Andre Agassis career for The Tennis Channel, entitled Andre Agassi|AGASSI, Between the Lines. The 90 minute special was chosen by Tennis Week Magazine as one of the 10 best TV moments from 2007.
In 2004, he started Magic Garden Productions,  a full service production company
specializing in Sports and Entertainment non-fiction programming.
 
From 2004 through 2006, Magic Garden was instrumental in creating the
Countdown Series for HBO Sports, a series of 30 minute documentary specials that preview HBOs biggest live boxing matches.
 
Craig has produced and directed features on Kate Winslet and Alicia Keys for the Glamour Magazine Woman of the Year Award Show, which aired on NBC. In addition, Shapiro produced and directed The Maxim Magazine Hot 100 for VH1, as well as VH1’s I Love the 70’s, I Love the 80’s and ESPN|ESPN’s groundbreaking documentary series, "The Life".

==References==
 

 
 
 
 
 
 