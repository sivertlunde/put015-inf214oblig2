Tough Guys Don't Dance (film)
{{Infobox film
| name = Tough Guys Dont Dance
| image = Tough guys dont dance ver2.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Norman Mailer
| producer = Menahem Golan Yoram Globus
| screenplay = Norman Mailer Robert Towne  
| based on =  
| starring = Ryan ONeal Isabella Rossellini Debra Sandlund Wings Hauser
| music = Angelo Badalamenti John Bailey
| editing = Debra McDermott Zoetrope Studios Golan-Globus
| distributor = Cannon Films
| released =  
| runtime = 109 minutes   
| country = United States
| language = English
| budget = $5 million
| gross = $858,250
}} crime Mystery mystery Comedy-drama|comedy-drama film written and directed by Norman Mailer based on his novel of the same name. It is a murder mystery/film noir piece that was scorned by audiences and critics alike. It was screened out of competition at the 1987 Cannes Film Festival.   

The script had revisions done by  , the Murder Inc. honcho, and his gorgeous girlfriend greet three champion boxers in the Stork Club. Costello demands that each, in turn, dance with the woman, and each nervously complies. The last, Willie Pep, suggests that Mr. Costello dance. Costello replied, "Tough guys dont dance."

==Plot==
Writer Tim Madden, who is prone to blackouts, awakens from a two-week bender to discover a bloodbath in his car,then sees a womans severed head in his marijuana stash, and the new Provincetown, Massachusetts police chief, Luther Regency, shacked up with his former girlfriend Madeleine. 

Flashing back, Madden remembers the time when he encouraged Madeline to swing with a Lil Abnerish couple from down South, the fundamentalist preacher Big Stoop and his Daisy Mae-ish wife, Patty Lareine, whose ad Tim had come across in Screw magazine. It is on the trip back that the car crash occurs, Madeline incensed along the way that Tim has so enjoyed Patty Lareines charms.

Except for his father, who is dying of cancer, Tim suspects everyone, including Patty Lareine, multi-millionaire prep-school pal Wardley Meeks III — and even himself — of murder. Patty Lareine had left Big Stoop, married Wardley, left him in a messy divorce which netted her a rich cash settlement, and in turn married Tim, whom she fancied. Patty Lareine disappears, and Tim goes on his fatal bender that has left his memory in shards after receiving a letter from Madeline informing him that her husband is having an affair with his wife.

Tim remembers his assignation in the local taverns parking lot with the blond porn star Jessica Pond, while her effete husband Lonnie Pangborn watched from the sidelines, distraught. It was Jessicas head in the Hefty bag with his grass, but soon another head turns up in his marijuana stash, that of Patty Lareine.

We eventually learn that she and Wardley, a bisexual skewed towards the gay side, had been involved in a massive cocaine deal, one that also involved Pond and Pangborn, who are also missing.

==Cast==
* Ryan ONeal as Tim Madden
* Isabella Rossellini as Madeleine Regency
* Debra Sandlund as Patty Lareine
* Wings Hauser as Captain Alvin Luther Regency
* John Bedford Lloyd as Wardley Meeks III
* Lawrence Tierney as Dougy Madden
* Penn Jillette as Big Stoop
* Frances Fisher as Jessica Pond
* R. Patrick Sullivan as Lonnie Pangborn John Snyder as Spider
* Stephan Morrow as Stoodie
* Clarence Williams III as Bolo
* Kathryn Sanders as Beth
* Ira Lewis as Merwyn Finney
* Ed Setrakian as The Lawyer

==Box office==
The film was a box office Bomb, making only $858,250,  nearly a fifth of its $5 million budget. 

==Reception==
Tough guys dont dance has received negative reviews.It currently holds a 39% "rotten" rating at Rotten Tomatoes. 

Hal Hinson of the Washington Post said that the film was "hard to classify; at times you laugh raucously at whats up on the screen; at others you stare dumbly, in stunned amazement." Roger Ebert, in a 2½ star review in the Chicago Sun-Times praised the cinematography, the Provincetown setting, and said that the relationship between Tim and Dougy was the best aspect of the film, but also had to say that "what is strange is that Tough Guys Dont Dance leaves me with such vivid memories of its times and places, its feelings and weathers, and yet leaves me so completely indifferent to its plot. Watching the film, I laughed a good deal."

However, the film had at least two supporters. Jonathan Rosenbaum of the Chicago Reader, said "Norman Mailers best film, adapted from his worst novel, shows a surprising amount of cinematic savvy and style." Also, "He translates his high rhetoric and macho preoccupations (existential tests of bravado, good orgasms, murderous women, metaphysical cops) into an odd, campy, raunchy comedy-thriller that remains consistently watchable and unpredictable--as goofy in a way as Beyond the Valley of the Dolls. Where Russ Meyer featured women with oversize breasts, Mailer features male characters with oversize egos, and thanks to the juicy writing, hallucinatory lines such as "Your knife is in my dog" and "I just deep-sixed two heads" bounce off his cartoonish actors like comic-strip bubbles; even his sexism is somewhat objectified in the process." Vincent Canby of the New York Times said that "Not the high point of the Mailer career, but its a small, entertaining part of it."

In the years since the films release on video, the film has become a cult classic in bad film circles. Channel 4 Film said "The overkill is strangely compelling and Mailers disregard for taste and convention ensure his film is a massive but spectacular and unmissable folly." The film apparently got enough of a following for Metro-Goldwyn-Mayer, who owns much of Cannon Films|Cannons film library, to release an anamorphic widescreen DVD of the film on September 16, 2003. The disc contained an interview with Norman Mailer, a tour of Provincetown and the films film trailer|trailer.

This film also includes the unofficially-proclaimed "worst line reading ever", wherein ONeals character Tim Madden reads a note from his ex-girlfriend Madeline informing him that his wife was having an affair with her husband, whereupon he exclaims "Oh man! Oh God! Oh man! Oh God! Oh man! Oh God! Oh man! Oh God! Oh man! Oh God!".  This scene has become a popular internet meme. Norman Mailer, in an interview featured on the DVD release of the film, said that he was counseled to cut the ending of the scene due to ONeals poor performance, but kept it in because he thought the poor line-reading actually added something to the picture. ONeal, who had been friendly with Mailer, turned on him as he felt his reputation could be jeopardised by the scene (ONeal had been nominated for an Academy Award for Best Actor several years previously).

==Awards==
{| class="wikitable"
|-
! Group !! Award !! Result
|-
|rowspan="5"| Independent Spirit Awards 1988
|- John Bailey)
|  
|-
| Best Feature (Menahem Golan and Yoram Globus)
|  
|-
| Best Female Lead (Debra Sandlund)
|  
|-
| Best Supporting Male (Wings Hauser)
|  
|}
 
{| class="wikitable"
|-
! Group !! Award !! Result
|- Golden Raspberry Awards 1988
|-
| Worst Director (Norman Mailer)
|  , tied with Elaine May for Ishtar (film)|Ishtar
|-
| Worst Actor (Ryan ONeal)
|  
|-
| Worst Actress (Debra Sandlund)
|  
|-
| Worst New Star (Debra Sandlund)
|  
|-
| Worst Picture (Menahem Golan and Yoram Globus)
|  
|-
| Worst Screenplay (Norman Mailer)
|  
|-
| Worst Supporting Actress (Isabella Rossellini, also for Siesta (film)|Siesta)
|  
|}

==References==
 

==Discography==

The CD soundtrack composed and conducted by Angelo Badalamenti is available on Music Box Records label ( ).

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 