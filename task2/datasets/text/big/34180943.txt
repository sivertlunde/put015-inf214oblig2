Mahakshathriya
 
 
{{Infobox film
| name = MAHAKSHATRIYA
| image = 
| caption = 
| director = Rajendra Singh Babu
| producer = Lahari
| released = 1994
| runtime =
| language = Kannada
| music = Hamsalekha
| lyrics = Hamsalekha
| Photography = 
| starring = Vishnuvardhan (actor)|Vishnuvardhan, Sonu Walia, Ramkumar, Sudha Rani, Shankar Ashwath, B C Patil. 
}}
Mahakshatriya ( , The soldier) is a 1994 Kannada movie starring Vishnuvardhan (actor)|Vishnuvardhan, Sonu Walia, Ramkumar, Sudha Rani, Shankar Ashwath, B. C. Patil. It was directed by Rajendra Singh Babu and produced by Lahari.  Mahakshatriya got censored on 27 May 1994 and got a "U" certificate.  This film won Filmfare award.

==Cast== Vishnuvardhan 
* Sonu Walia
* Ramkumar
* Shankar Ashwath
* B. C. Patil 
* Sudha Rani

==Plot==
The story revolves around a young man who is short tempered but good at heart. Hes in prison for committing a murder. The prison officer helps him in becoming a good man by doing good deeds.

==Soundtrack==

This film is well known for the hugely popular philosophical song "Ee Bhoomi Bannada Buguri", sung by SP Balasubrahmanyam. Hamsalekha composed the lyrics and music for this song. This song has also been picturised very well in the film.

* "Ee Bhoomi Bannada Buguri" - S. P. Balasubramanyam
* "Tavare Kendavare" - K. S. Chitra, S. P. Balasubramanyam
* "Yenaithi Olage Yenaithi" - Malgudi Subha, C. Ashwath
* "Chumbana Chumbana" - K. S. Chitra
* "O Prema O Prema" - K. S. Chitra, S. P. Balasubramanyam

==External links==
*  

== References ==
 

 

 
 
 
 
 

 