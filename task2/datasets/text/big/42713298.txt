A Shot at Dawn
{{Infobox film
| name =  A Shot at Dawn
| image =
| image_size =
| caption =
| director = Alfred Zeisler
| producer =  Erich Pommer 
| writer =  Harry Jenkins  (play)   Rudolph Cartier   Egon Eis   Otto Eis
| narrator =
| starring = Ery Bos   Genia Nikolaieva   Karl Ludwig Diehl   Theodor Loos
| music = Bronislau Kaper   
| cinematography = Werner Bohne   Konstantin Irmen-Tschet  
| editing =  Erno Hajos       UFA
| distributor = UFA
| released =  18 June 1932 
| runtime = 73 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} jewel theft.  A separate French language version was also released.

==Cast==
* Ery Bos as Irene Taft  
* Genia Nikolaieva as Lola  
* Karl Ludwig Diehl as Petersen  
* Theodor Loos as Bachmann  
* Fritz Odemar as Dr. Sandegg  
* Peter Lorre as Klotz  
* Heinz Salfner as Joachim Taft  
* Gerhard Tandar as Müller IV  
* Kurt Vespermann as Bobby 
* Ernst Behmer as Gas Station Attendant  
* Curt Lucas as Holzknecht 
* Hermann Speelmans as Schmitter  

== References ==
 

== Bibliography ==
* Bock, Hans-Michael & Bergfelder, Tim. The Concise CineGraph. Encyclopedia of German Cinema. Berghahn Books, 2009. 
* Youngkin, Stephen.  The Lost One: A Life of Peter Lorre. University Press of Kentucky, 2005.

== External links ==
*  

 

 

 
 
 
 
 
 
 
 
 