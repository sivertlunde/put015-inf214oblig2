East Lynne (1931 film)
{{Infobox film
| name           = East Lynne
| image          = Eastlynne1931.jpg
| image_size     =
| caption        = 1931 film
| director       = Frank Lloyd
| producer       =  Tom Barry Bradley King
| starring       = Ann Harding Conrad Nagel Clive Brook Cecilia Loftus
| music          = Richard Fall  
and Carli Elinor  
| cinematography = John F. Seitz
| editing        = Margaret Clancey  
| distributor    = Fox Film Corporation
| production dates = Nov. 17, 1930 - early January 1931
| released       =  
| runtime        = 102 minutes, 9,188 ft., or 10 reels  
| country        = United States English
| budget         = 
}}
 Ellen Woods eponymous 1861 Tom Barry and Bradley King and directed by Frank Lloyd. The film is a melodrama starring Ann Harding, Clive Brook, Conrad Nagel and Cecilia Loftus. Only one copy of the film is known to exist, although, somehow, bootleg DVD copies exist, minus the final scene. 

This print is in good shape, although several frames have an "X" on them, indicating they were to be removed in the film editing stage. One frame has a "crosshairs" on it while several frames have ink marks.

People may view the film at University of California Los Angeless Instructional Media Lab, Powell Library, after arranging an appointment.
 
==Synopsis==
The trophy wife of a stodgy man of wealth yearns for a more interesting life.

==Cast==

*Ann Harding as Lady Isabella
*Clive Brook as Captain William Levison
*Conrad Nagel as Robert Carlyle (named changed from Archibald Carlyle in the book)
*Cecilia Loftus as Cornelia Carlyle
*Beryl Mercer as Joyce
*O.P. Heggie as Lord Mount Severn
*  as Barbara Hare
*David Torrence as Sir Richard Hare
*  as Dodson, the Butler (uncredited)
*  as Doctor
*  as William as a child
*  as William as a boy

==Awards==
Academy Award for Best Picture ( nominated )

== References ==
 

== External links ==
* http://bestpicturederby.blogspot.com/2009/02/east-lynne-1931.html
* http://movies.nytimes.com/movie/review?res=9F0DEFDC143AEE3ABC4951DFB466838A629EDE
* http://www.tcm.com/tcmdb/title.jsp?stid=73829&atid=5339
*  

 

 
 
 
 
 