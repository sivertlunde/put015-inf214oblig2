Ye Happy Pilgrims
{{Infobox Hollywood cartoon|
| cartoon_name = Ye Happy Pilgrims
| series = Oswald the Lucky Rabbit
| image = 
| caption = 
| director = Walter Lantz
| story_artist = 
| animator = Manuel Moreno George Grandpre Lester Kline Verne Harding Fred Kopietz Victor McLeod
| voice_actor = 
| musician = James Dietrich
| producer = Walter Lantz
| studio = Walter Lantz Productions
| distributor = Universal Pictures
| release_date = September 3, 1934
| color_process = Black and white
| runtime = 7:58 English
| preceded_by = The Dizzy Dwarf 
| followed_by = Sky Larks
}}

Ye Happy Pilgrims is a short animated film by Walter Lantz Productions, starring Oswald the Lucky Rabbit. In a reissue, the film goes with the title The Happy Pilgrims. {{cite web
|url=http://lantz.goldenagecartoons.com/1934.html
|title=The Walter Lantz Cartune Encyclopedia: 1934
|accessdate=2012-01-11
|publisher=The Walter Lantz Cartune Encyclopedia
}} 

==Plot==
The cartoon is set in the 1600s where the ship Mayflower lands in Plymouth Rock in the United States. Oswald is portrayed as one of the passengers on board.

Oswald makes a living as a woodcutter. His closest colleague is a tall man wearing a helmet. The two are good friends until they spot a young woman whom they both have affection for.

One day the tall man tries to hand a love letter to the young woman at her house but is too shy to do so. He then asks Oswald to pass it for him. Oswald confidently approaches the recipient at the door, and presents the note. The flattered young woman, however, thinks the words on the letter were Oswalds as she takes in and relentlessly offers kisses to the rabbit. The tall man is most surprised.

At a site not too faraway, the tall man sits around, upset on how things did not go his way at the young womans house. He then develops a grudge towards Oswald, and plans to get rid of his colleague.

Days later, a feast is held in some open grounds. Oswald and the young woman are among the attendees. While the guests are having their meal, the event gets raided by a pack of Native Americans who are led by none other than the tall man. The natives, after noticing food on the table, suddenly decide to have a friendly meal with the guests instead of making chaos. And the tall man opts to just kiss the young woman, and no longer intends to take out Oswald. But before she and the tall man could kiss each other, a big beagle shows up in between just to join the feast. Bothered by this, the tall man splats a pie onto the big beagles face.

==References==
 

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 

 