Murder Unveiled
{{Infobox film
| name = Murder Unveiled
| image = Murder Unveiled VideoCover.jpeg
| director = Vic Sarin
| producer = Debra Beard Hugh Beard Rob Bromley Greg Laikin John Ritchie
| writer = S. Bennett M.R. Smith
| starring = Anita Majumdar Chenier Hundal Hassani Shapi Lushin Dubey Sanjay Talwar Vinay Pathak Vik Sahay Michael Benyaer Cedric De Souza
| music = Daniel Seguin
| cinematography = Gregory Middleton
| editing = Alison Grace
| distributor = CBC Home Video
| production_company = CBC Home Video
| released =  
| runtime = 94 min
| country = Canada English
}}
 2005 Cinema Canadian television true story of the Jaswinder Kaur Sidhu murder.  The film was screened at the Asian Festival of First Films on November 26, 2005 and was first aired on CBC on February 6, 2006.

==Cast==
*Anita Majumdar as Davinder Samra
*Chenier Hundal	as Surinder Singh
*Hassani Shapi as Jaipal
*Lushin Dubey a Kuldeep Samra
*Sanjay Talwar as Inspector Darshan Singh
*Vinay Pathak as Inspector Gurpal Badash
*Cedric De Souza as Mohan
*Vik Sahay as Bindri
*Michael Benyaer as Ashu
*Zena Darawalla as Jasminder
*Parm Soor as Vikram Samra
*Veena Sood as Magistrate
*Tony Ali as Doctor
*Sooraj Jaswal as Jaggi
*Rajinder Singh Cheema as Suitors #2 Father
*Balinder Johal as Suitors #2 Mother
*Stephen Park as Immigration Officer
*Bill MacDonald	as Business Man #1
*Jim Francis as Senior Immigration Officer
*Manoj Sood as Lawyer
*Vir Singh Pannu as Jhagi
*Gurpreet Singh Sekhon as Ashus Thug
*Tegan Moss as Veronica
*Jag Cheema as Suitor #3
*Jai Sondhi as Parmveer
*Tara Gerewal as Amarjeet
*Taminder S. Singh as Dharamleen
*Nina Tiwana as Grandmother
*Paya Choudhry as Surindas Mother
*Kamal Tiwari as Nirmal Bakshi
*Mylène Dinh-Robic as Host
*George Gordon as Judge
*Sanjay Madhav as Vic Prasad
*Dev Parmar as Prisoner

==Plot==
In India, a young woman is kidnapped, and her young male companion beaten within an inch of his life. He is working class Sikh, Surinder Singh; she is his wife, the former Davinder Samra, a Canadian Sikh whom he met when she visited India a year earlier for her cousins wedding. For both, it was love at first sight. However, Davinder comes from a traditional Sikh family, who made their fortune in Canada. Her parents, who knew nothing of Surinder when Surinder and Davinder eloped, were seeking a suitable husband for her. As the story unfolds leading to the kidnapping/beating and the subsequent investigation by the local police and Crime Investigation Division, the power of money and of Sikh family honor is shown.

==Awards==
* 2005 Best Actress Award at the 2005  
* 2006   

==References==
 

==External links==
* 

 

 

 
 
 
 
 
 
 
 


 