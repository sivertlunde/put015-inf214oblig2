Puthiya Mukham
 
{{multiple issues|
 
 
}}

{{Infobox film
| name        = Puthiya Mukham
| image       = Puthiya Mukham.jpg
| caption     = Poster Prithviraj Bala Bala  Priyamani  Meera Nandan
| director    = Diphan
| producer    = Anil Mathew S. Murugan
| writer      = M. Sindhuraj
| studio      = Bethestha Productions
| distributor = Time Ads Release
| cinematography= Bharani K. Dharan
| editing     = Samjith Mhd
| country     = India
| released    =  
| runtime     = 137 minutes
| language    = Malayalam
| music       = Deepak Dev
| budget      = 
| gross       = 
}} Malayalam action film directed by Diphan and starring Prithviraj Sukumaran. The shooting locations were Kochi, India|Kochi, Palakkad, and Malaysia. The music was 
composed by Deepak Dev, and the cinematography was by Bharani K. Dharan, who mainly works in Telugu cinema. The film was a commercial success and was one of the successful films of 2009,yet lacked positive reviews regarding creativity. The film was dubbed in Tamil with same name and it was remade in Kannada as Ziddi with Prajwal Devaraj.

==Plot==
Krishnakumar(Prithviraj Sukumaran) is a resident of an agraharam in Kalpathi. Along with his studies, he pursues a career as a percussionist and teaches the mridangam to local students. He is in love with a girl—the daughter of a family friend. Their relationship is not only approved, but supported by both families.
 Kochi (Ernakulam) to enroll at an engineering college. The villain - Sudhi (Bala (actor)|Bala) - is a senior student and the youngest of three brothers (the others being Sai Kumar and Shammi Thilakan); he pursues a real estate business and routine crimes associated with it. In all his villainous endeavours on the campus he is supported by his brothers.

The main support for the villain family from the system is a police officer (Vijayaraghavan (actor)|Vijayaraghavan). The story takes its form when Anjana (Priyamani), the daughter of the police officer, joins the engineering college. Sudhi is in love with her and to win her love plays the gentleman, concealing and temporarily stopping all his villainous activities.

Kichu, as is expected, wins the heart of the girls in the college from almost day one. Sudhi suspects that Anjana is attracted to Kichu. So he moves fast and sends his brother with an official proposal to Anjanas family. Her parents accept the proposal and force the reluctant daughter to agree to it. But she stipulates a condition: there will be no marriage immediately. Sudhi has to wait for four years (until the course ends) for the marriage; she will have her freedom for these four years. The freedom of Anjana and the way she exercises it make jealous and suspicious. He wants to scare Kichu out of the college. He arranges an attack on him at the college hostel. Kichu is severely wounded. At the end of the attack, when apparently he receives a blow on his head, Kichu appears to turn mad and attacks his own friend who tried to rescue him.

He is admitted to a hospital and it is revealed that he has what the doctor in the movie says a Flashback Phenomenon. He had to witness the death of his younger brother, while young, hit by a bus, and traumatic incidents like this will turn him violent. His prearranged marriage has been disapproved by his would-bes family following this. His own family doesnt want him to go to the college anymore. But his father (Nedumudi Venu) supports him and urges him to go back to the college and asks him to win his life.

What happens after this is an unbelievable change in the character of Kichu. A hitherto simpleton now turns a super hero. The villain welcomes him back at the college with an attack. Kichu, the now fearless super hero, gives a difficult-to-believe show of stunts. Sudhis brothers turn in for help. Kichu sends the eldest brother Sai Kumar to hospital, mortally wounded. He is put in the ICU for observation and is reportedly in a coma. The villains ally, Vijaya Rakhavan, arrests Kichu and takes him to the police station. Here Kichu learns that he is going to be transported to the sub jail where his murder is planned. Kichu turns violent and storms the police station. He is hit by a policeman from behind and he apparently falls unconscious, but it is only an act. He is rushed to the hospital, where he makes his escape.

The villains kidnap Anjana. In the final stunt at a construction site of a multistoried building, Kichu beats numerous stuntmen and tells Sudhi the truth about Kichu and how he wants to see Sudhi become a good person towards Kichu. But Sudhi, who is still angry, denies and tells Kichu that he doesnt want a life. So Kichu lets go of his hand and Sudhi dies. Kichu walks away with Anjana as the credits start to roll.

==Cast==
*Prithviraj Sukumaran as Krishnakumar a.k.a. Kichan Bala as Sudhi
*Priyamani as Anjana Sivaraman
*Meera Nandan as Sreedevi
*Sudheesh as Varghese
*Oviya as Meera
*Nedumudi Venu as Kichans father Vijayaraghavan as Shivaraman, father of Anjana
*Jagadish as Kichans brother-in-law
*Sona Nair as Anu, Kichans sister Saikumar as Mahi, Sudhis elder brother
*Shammi Thilakan as Giri, Sudhis brother
*Kalashala Babu as Sreedevis father
*Anil Murali as Sreedevis brother
*Guinness Pakru
*Joy John Antony as Kuruvila

==Soundtrack==

Songs in the movie were composed by Deepak Dev. The songs "Picha Vecha Naal" (Shankar Mahadevan) and "Thattum Muttum Thalam" (Jassie Gift, Deepak Dev, Sindhuja) were chart toppers on Malayalam television/radio stations. 
 Best Music Best Male Playback Singer Award for the song "Picha Vecha Naal".

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (min:sec) ||Lyrics ||Notes
|-
| 1 || "Kaane Kaane" ||   || Plays with the opening credits
|-
| 2 || "Picha Vacha Naal" ||   ||
|-
| 3 || "Thattum Muttum Thalam" ||   ||
|- Kaithapram ||
|-
| 5 || "Rahasyamay" ||   ||
|}

==References==
 

==External links==
*  
* http://www.cinepicks.com/malayalam/gallery/puthiya-mukham/
* http://www.flickr.com/photos/prithviz/sets/72157615784348214/
* http://www.strikersncrew.blogspot.com/

 

 
 
 
 
 
 