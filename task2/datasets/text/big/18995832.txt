Rabbit Without Ears
 
{{Infobox film
| name           = Rabbit Without Ears  (Keinohrhasen) 
| image          = Keinohrhasen.jpg
| image_size     = 
| caption        = German promotional picture
| border         = yes
| director       = Til Schweiger
| producer       = Til Schweiger Thomas Zickler
| writer         = Til Schweiger Anika Decker
| narrator       = 
| starring       =Til Schweiger Nora Tschirner
| music          = Stefan Hansen Dirk Reichardt Mirko Schaffer
| cinematography = 
| editing        = Charles Ladmiral 
| distributor    = Warner Bros.;
 SevenOne International
| released       =  
| runtime        = 115 minutes
| country        = Germany
| language       = German
| budget         = €4,200,000
| gross          = $74,000,000
}}

Rabbit Without Ears (German title: Keinohrhasen, lit: No Ear Rabbits), is a 2007 German  romantic comedy film, written, produced and directed by Til Schweiger. Co-written by Anika Decker, and starring Nora Tschirner and himself, the story of the film revolves around yellow press reporter Ludo and his ex-classmate Anna, who reconvene after years when he is sentenced to 300 hours of community service at her day-care facility.
 Bambi and received a nomination for the Audience Award at the European Film Awards 2008. 

A sequel, entitled Rabbit Without Ears 2 was released on 3 December 2009.

== Plot ==
Ludo Decker (Schweiger) is a Berlin based yellow press reporter.  With his photographer Moritz, his daily routine is to spy on celebrities for the tabloid Das Blatt. He also uses his work for frequent sexual contacts with his objects of interest.

When heavyweight boxer and celebrity Wladimir Klitschko is about to toast his fiancee Yvonne Catterfeld at their engagement party, Ludo and Moritz are on the scene to report about it. Ludo breaks through a glass dome of the party venue, falls into the cake and is subsequently sentenced to 300 hours of community service at a daycare center.

There he meets Anna Gotzlowski (Tschirner), the centers manager. Anna grew up in the same neighborhood as Ludo and used to be picked on and mocked by him.  She gets her revenge by appointing him to perform humiliating tasks.  Ludo cannot fight this as his parole is at stake. Still, he is interested in one-night stands, and has an affair with a childs mother who frequents the facility.
 German Film Awards ceremony with Vogel, describing her as the most beautiful woman around, he is fired by his editor.

In the end, Ludo realizes that he feels more for Anna than he had previously thought. During a childrens festival at a local theatre, he crashes a performance and swears his love to Anna, who sits in the audience with the children. After his discharge from the tabloid paper, he takes up work at Annas facility.

==Cast==
* Til Schweiger as Ludo Decker, a well-known yellow press reporter and libertine who is sentenced to three hundred hours of social work in a local day-care facility run by his ex-classmate Anna Gotzlowski.    Ludo is Latin for "I play". His character was conceptualized as a "comedic tour de force" for Schweiger.   
* Nora Tschirner as Anna Gotzlowski, Ludos ex-classmate and head of a day-care facility for children, who initially harbours an improbable resentment of Ludo, having not overcome his teasing over 20 years ago.  Schweiger created the role of Anna specifically with Tschirner in mind, citing her "one of the best screen actresses in Germany."  paparazzo and friend of Ludo. 
*   as Miriam Steinfeld, a colleague and friend of Anna. 
* Barbara Rudnik as Lilli Decker, Ludos elder sister.
*   as Lollo, Lillis son.
* Emma Tiger Schweiger as Cheyenne-Blue, a kid at the day-care facility. Lilli Camille Schweiger as Sacha, another kid at the day-care facility. Luna Marie Schweiger as young Anna Valentin Florian Schweiger as young Ludo tabloid Bild-Zeitung|Das Blatt
* Jürgen Vogel as himself
* Christian Tramitz as Annas date.
* Wladimir Klitschko as himself.
* Yvonne Catterfeld as herself.
* Wolfgang Stumph as a taxi driver.
* Armin Rohde as Bello.
* Fahri Ogün Yardım as Mucki.
*   as a taxi driver.
*   as a judge.
*   as Ludos attorney.
*   as a hotel worker.
*   as Michi Nußbaumer, a folk musician.
* Nina Proll as Daniela Berg, Michis partner.
*   as Mandy, a headline-making "Ministerluder".

==Soundtrack==
===Music from the Motion Picture album===
{{Infobox album  
| Name        = Keinohrhasen
| Type        = Soundtrack
| Longtype    =
| Artist      = Various artists
| Cover       = Keinohrhasen soundtrack.jpg
| Released    = 7 December 2007
| Recorded    =
| Genre       = Pop Rock Film music
| Length      = 68:47
| Label       = Universal Music
| Producer    = 
| Last album  =
| This album  =
| Next album  =
}}{{Album reviews
| rev1 = CDStars
| rev1Score =   
}}

Track listing
# "Mr. Brightside" (Jacques Lu Conts Thin White Duke Mix)  (The Killers)  – 8:48 Deepest Blue"  (Deepest Blue)  – 3:24
# "Is It Love"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 2:18 Rea Garvey)  – 1:47
# "Springtimes"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 2:56
# "Lifeline"  (Angels & Airwaves)  – 4:16
# "Autumn Leaves"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:42
# "Everybodys Changing"  (Keane (band)|Keane)  – 3:35
# "Looking for Atlantis"  (Prefab Sprout)  – 4:03
# "Rain"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:18 I Still Remember"  (Bloc Party)  – 4:20
# "Liquid"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:43
# "Everythings Magic"  (Angels & Airwaves)  – 3:51
# "Some Time"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:59
# "Sad Song"  (Au Revoir Simone)  – 4:07 Apologize  (Timbaland presents OneRepublic)  – 3:04
# "A Rainy Day in Vancouver"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:38
# "Rocket Brothers"  (Kashmir (band)|Kashmir)  – 5:27
# "Perfect Circle"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 2:57
# "Cheyenne Blue"  (Stefan Hansen, Dirk Reichardt, Mirko Schaffer)  – 1:40
# "Der Zauberlehrling"  (Junge Dichter & Denker)  – 3:54

===Charts===

{| class="wikitable"
|-
! Chart (2007) !! Peak position
|- Austrian Albums Chart    13
|- European Top 100 Albums    14
|- Media Control German Albums Chart  2
|- Swiss Music Swiss Albums Chart  46
|}

===Year-end charts===
{| class="wikitable sortable"
|-
! Chart (2008)
! Rank
|- Media Control German Albums Chart  17
|}

===Certifications===
{| class="wikitable"
|-
! Country Certifications  sales thresholds) 
|- Media Control Germany
|Platinum 
|}

==The title==
While doing handicraft at the daycare centre, Ludo sews a stuffed rabbit without ears. As Anna criticises him for it, he points at the fact that another rabbit made by the child Cheyenne-Blue has no ears as well. Anna thereupon claims that it is a Keinohrhase, a no-ears rabbit, which is able to hear through its nose. This scene was also used for the first teaser trailer.

A rare earless rabbit born in February 2012 at a zoo in Limbach-Oberfrohna, Saxony was named "Til (rabbit)|Til", in reference to this films title and its director.  The baby rabbit was accidentally crushed when a television cameraman stepped on him, an event that was briefly the subject of international media coverage.   

==American remake==
In January 2010, Schweiger sold the rights for the US remake of Keinohrhasen to Newmarket Entertainment; as he stated he hopes to direct the film and to engage Ben Affleck as leading man. 

==Awards==
* 2008: Goldene Leinwand mit Stern for more than 6 million viewers
* 2008: Bogey Awards in gold for 3 million viewers within 30 days
* 2008: Ernst Lubitsch Prize
* 2010: Bronze Palm Award at the Mexico International Film Festival 

== References ==
 

== External links ==
*  
*    

 

 
 
 
 
 
 
 