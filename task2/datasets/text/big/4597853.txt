The Devil Wears Prada (film)
 
{{Infobox film
| name = The Devil Wears Prada
| image = The Devil Wears Prada main onesheet.jpg
| border = yes
| caption = Theatrical release poster
| director = David Frankel
| producer = Wendy Finerman
| screenplay = Aline Brosh McKenna
| based on =  
| starring = Meryl Streep Anne Hathaway Emily Blunt Stanley Tucci Simon Baker Adrian Grenier Theodore Shapiro
| cinematography = Florian Ballhaus
| editing = Mark Livolsi
| distributor = 20th Century Fox
| released =  
| runtime = 109 minutes
| country = 
 United States
| language = English
| budget = $35 million 
| gross = $326,551,094 
}} novel of screen adaptation fashion magazine editor Miranda Priestly, played by Meryl Streep. Emily Blunt and Stanley Tucci co-star, as co-assistant Emily Charlton and art director Nigel, respectively.
 directed the Oscar bid, Golden Globe Best Actress in a Comedy or Musical. Blunt also drew favorable reviews and nominations for her performance, as did many of those involved in the films production.

The film was well received by both film critics and the public and became a surprise summer box-office hit following its June 30 North American release. The commercial success and critical praise for Streeps performance continued in foreign markets with the film leading the international box office for most of October. Likewise, the U.S. DVD release was the top rental during December. The film finished in 2006s Top 20 both in the U.S. and overseas and grossed over $300 million, mostly from its international run.

Although the movie is set in the fashion world, most  ; retrieved January 10, 2007.  Wintour later overcame her initial skepticism, saying she liked the film and Streep in particular. Walters, Barbara; December 12, 2006;  ; "The 10 Most Fascinating People of 2006"; retrieved from abcnews.go.com December 18, 2006. 

==Plot== journalist fresh Miranda Priestly (Meryl Streep), the icy editor-in-chief of Runway fashion magazine. Andy plans to put up with Mirandas bizarre and humiliating treatment for one year in hopes of getting a job as a reporter or writer somewhere else.

At first, Andy fumbles with her job and fits in poorly with her gossipy, fashion-conscious coworkers, especially Mirandas senior assistant Emily Charlton (Emily Blunt).  However, with the help of art director Nigel (Stanley Tucci), who loans her designer clothes, she gradually learns her responsibilities and begins to dress more stylishly to show her effort and commitment to the position.  She also meets attractive young writer Christian Thompson (Simon Baker), who offers to help her with her career.  As she spends increasing amounts of time at Mirandas beck and call, problems arise in her relationships with her college friends and boyfriend Nate (Adrian Grenier), a chef working his way up the career ladder.

Miranda is impressed by Andy and allows her to be the one to bring the treasured "Book," a mock-up of the upcoming edition, to her home, along with her dry cleaning. She is given instructions by Emily about where to leave the items and is told not to speak with anyone in the home. Andy arrives at Mirandas home only to discover that the instructions she received are vague and she is unsure what to do. As she tries to figure out what to do and begins to panic, Mirandas twins falsely tell her she can leave the book at the top of the stairs just as Emily has done on many occasions. At the top of the stairs, Andy interrupts Miranda and her husband having an argument. Mortified, she leaves the book and runs out of the home.
 Harry Potter book for her daughters and if Andy cannot find a copy, she will be fired. Andy desperately attempts to find the book, nearly gives up, but ultimately obtains it through Christians contacts.  She surprises Miranda by not only finding the book but having copies sent to the girls at the train station, leaving no doubt that she accomplished Mirandas "impossible" task, thus saving her job.

One day, Andy saves Miranda from being embarrassed at a charity benefit, and Miranda rewards her by offering to take her to the fall fashion shows in Paris instead of Emily. Andy hesitates to take this privilege away from Emily, but is forced to accept the offer after being told by Miranda that she will lose her job if she declines. Andy tries to tell Emily on her way to work; however, Emily is hit by a car and Andy has to break the bad news while visiting her in the hospital.

When Andy tells Nate she is going to Paris, he is angered by her refusal to admit that shes become what she once ridiculed, and they break up. Once there, Miranda, without makeup, opens up to Andy about the effect Mirandas impending divorce will have on her daughters.  Later that night, Nigel tells Andy that he has accepted a job as creative director with rising fashion star James Holt (Daniel Sunjata) at Mirandas recommendation.  Andy finally succumbs to Christians charms, and after spending the night with him, learns from him about a plan to replace Miranda with Jacqueline Follet as editor of Runway. Despite the suffering she has endured at her bosss behest, she attempts to warn Miranda.

At a luncheon later that day, however, Miranda announces that it is Jacqueline instead of Nigel who will leave Runway for Holt. Nigel remarks to Andy that, though disappointed, he has to believe that his loyalty to Miranda will one day pay off. Later, when Miranda and Andy are being driven to a show, she explains to a still-stunned Andy that she was grateful for the warning but already knew of the plot to replace her and sacrificed Nigel to keep her own job. Pleased by this display of loyalty, she tells Andy that she sees some of herself in her. Andy, repulsed, says she could never do that to anyone. Miranda replies that she already did, stepping over Emily when she agreed to go to Paris. When they stop, Andy gets out and throws her cell phone into the fountain of the Place de la Concorde, leaving Miranda, Runway, and fashion behind.
 Boston restaurant. Andy is disappointed, but her hope is rejuvenated when he says they can work something out. At the films conclusion, she is interviewing for a job at the New York Mirror. The interviewer reveals that Miranda told him she was by far her biggest disappointment, but that he would be an idiot not to hire her. In the last scene, Andy, dressed casually but with a bit more style, gives a call to Emily in Mirandas office and offers her all the clothes she received in Paris. Emily accepts the offer and tells the new assistant that she has some very large shoes to fill. After that, Andy looks over and sees Miranda getting into her car across the street. They exchange looks and Miranda gives a soft smile once inside the car. Miranda then looks at the driver and says with attitude, "Go".

== Cast ==
* Meryl Streep as Miranda Priestly: The editor-in-chief of Runway. Feared by her staff and many in the fashion world, and powerful enough that she can discard a $300,000 photo shoot with impunity and lead a designer to redo an entire collection with the pursing of her lips. Nevertheless, she cares a lot about her twin daughters.
*   graduate and aspiring journalist who, despite no real knowledge of fashion, is hired as the junior personal assistant to the powerful and demanding editor-in-chief of Runway magazine, Miranda Priestly.
* Emily Blunt as Emily Charlton: Mirandas haughty senior assistant, who tolerates her bosss rudeness and insults so that she may accompany her to Paris for Fall Fashion Week.
* Stanley Tucci as Nigel: Art director for Runway and the only person at the magazine Andrea feels she can trust, despite his sometimes cutting remarks about her wardrobe and weight.
*   books Miranda requests for her daughters. He hints he could help her with her journalistic aspirations.
* Adrian Grenier as Nate Cooper: Andreas boyfriend, a chef at a Manhattan restaurant who eventually breaks up with her due to the strain her job places on their relationship, but later reconcile after she leaves the magazine, telling her after he has accepted a job as a chef at Boston that they can work something out.
* Tracie Thoms as Lily: Andreas close friend, who runs an art gallery.
* Rich Sommer as Doug: A college friend of Andrea, Nate and Lily who works as a corporate research analyst. He also shows an extensive knowledge of fashion, Runway magazine, and Miranda Priestly.
* Daniel Sunjata as James Holt: An up-and-coming designer.
* David Marshall Grant as Richard Sachs, Andreas father.
* Tibor Feldman as Irv Ravitz: The board chairman of Elias-Clarke (fictional version of Condé Nast), the company that publishes Runway.
* Rebecca Mader as Jocelyn.
* Gisele Bündchen as Serena: An editorial staffer at Runway and friend of Emilys.
* Alyssa Sutherland as Clacker: An editorial staffer at Runway.
* Ines Rivero as Clacker at elevator
* Stephanie Szostak as Jacqueline Follet

=== Cameos ===
* Valentino Garavani
* Giancarlo Giammetti
* Carlos de Souza
* Charlene Shorto
* Bridget Hall
* Lauren Weisberger (uncredited) as the twins nanny
* Robert Verdi as a fashion journalist in Paris who interviews Miranda
* Heidi Klum
* Ivanka Trump

== Production ==
Director   Florian Ballhaus and costume designer Patricia Field, drew heavily on their experience in making Sex and the City.

=== Preproduction ===

=== Filming ===
 ]]
Principal photography took place over 57 days in New York and Paris between October and December 2005. Frankel, David (2006). Commentary track on The Devil Wears Prada  . USA: 20th Century Fox.  The films budget was $35 million.   at boxofficemojo.com, retrieved September 15, 2006. 
 camera during some of the busier meeting scenes in Mirandas office, to better convey the flow of action, and slow motion for Andreas entrance into the office following her makeover. A few process shots were necessary, mainly to put exterior views behind windows on sets and in the Mercedes where Miranda and Andrea are having their climactic conversation. 

==== Acting ====
Streep made a conscious decision not to play the part as a direct impression of Wintour, January 31, 2007;  ;  s; her coat-tossing on Andreas desk Weisberger, 201.  and discarded steak lunch Weisberger, 150–51.  are retained from the novel. Streep prepared by reading a book by Wintour protégé Liz Tilberis and the memos of Vogue editor Diana Vreeland. She lost enough weight during shooting that the clothes had to be taken in. 

Hathaway prepared for the part by volunteering for a week as an assistant at an  ; retrieved January 10, 2007.  Streep applied this philosophy to everyone else on set as well, keeping her distance from the cast and crewmembers unless it was necessary to discuss something with them. 

She also suggested the editorial meeting scene, which does not advance the plot but shows Miranda at work without Andrea present. McKenna, Aline Brosh (2006). Commentary track on The Devil Wears Prada  . USA: 20th Century Fox.  It was also her idea that Miranda not wear makeup in the scene where she opens up to Andrea and worries about the effect on her daughters of her divorce becoming public knowledge.  Both Hathaway and Emily Blunt lost weight for their roles, with Hathaway later recounting that "Emily Blunt and I would clutch at each other and cry because we were so hungry." 

==== Costuming ====
Frankel, who had worked with  ; retrieved January 10, 2007. 
 wig and forelock she wore as well as the clothes the two spent much time poring over look-books for. 
 Bill Blass, French, Serena; June 21, 2006; "The $1 Million Wardrobe"; The New York Post, 41–43  a look she describes as "rich-lady clothes."  She didnt want people to easily recognize what Miranda was wearing. Patricia Field|Field, Patricia (2006). Commentary track on The Devil Wears Prada  . USA: 20th Century Fox. 
 
She contrasted Andrea and Emily by giving Andrea a "textbook" sense of style, without much risk-taking, that would suggest clothing a fashion magazine would have on hand for shoots.  Much of her high-fashion wardrobe is Chanel, with some Calvin Klein thrown in for good measure.  Blunt, on the other hand was "so on the edge shes almost falling off."    For her, Field chose pieces by Vivienne Westwood and Rick Owens, to suggest a taste for funkier, more "underground" clothing.  After the films release, some of the looks Field chose became popular, to the filmmakers amusement.  Finerman, Wendy. (2006). Commentary track on The Devil Wears Prada  . USA: 20th Century Fox. 

Tucci praised Fields skill in putting ensembles together that were not only stylish but helped him develop his character:
 
She just sort of sits there with her cigarette and her hair, and she would pull stuff&nbsp;— these very disparate elements&nbsp;— and put them together into this ensemble, and youd go, "Come on, Pat, you cant wear that with that." Shed say, "Eh, just try it on." So youd put it on, and not only did it work, but it works on so many different levels&nbsp;— and it allows you to figure out who the guy is. Those outfits achieve exactly what I was trying to achieve. Theres flamboyance, theres real risk-taking, but when I walk into the room, its not flashy. Its actually very subtle. You look at it and you go, "That shirt, that tie, that jacket, that vest? What?" But it works. Lamphier, Jason; " "; Out (magazine)|Out; retrieved January 9, 2007. 
 
He found one Dries van Noten tie he wore during the film to his liking and kept it. 

==== Production design ====
After touring some offices of real fashion magazines,   to the August 2004 cover of that magazine.  , retrieved from imdb.com December 24, 2006. 

She even chose separate computer wallpaper to highlight different aspects of Blunts and Hathaways character: Pariss Arc de Triomphe on Blunts suggests her aspirations to accompany Miranda to the shows there, while the floral image on Andys suggests the natural, unassuming qualities she displays at the outset of her tenure with the magazine. For the photo of Andrea with her parents, Hathaway posed with her own mother and David Marshall Grant.  One of the purported Harry Potter manuscripts was later sold at auction for $586 on eBay, along with various clothing used in the film, to benefit Dress for Success, a charity which provides business clothing to help women transition into the workforce.  ; retrieved from Ebay|ebay.com January 18, 2007. 

==== Products ====
Aside from the clothing and accessories, some other well-known brands are conspicuous in the film. Apple computers are used in the Runway offices, consistent with many real publishing companies. San Pellegrino mineral water are seen in the Runway offices.
* Mitel IP telephones are used in the office of RUNWAY Magazine - including both reception desks outside the office of Miranda Priestly (played by Meryl Streep), Magazine Editor.
* Miranda drinks coffee from a nearby Starbucks. RAZR V3 in silver, same as Nigels.
* The two are frequently driven around in Lincoln Town Cars and Mercedes-Benz W221|Mercedes-Benz S-Class S550 (without vehicle registration plate) sedans. Porsche 911 Carrera Cabriolet.
* Andrea gives her friend a Bang & Olufsen phone

 , home to Elias-Clarke in the film]]

==== Locations ====

===== New York ===== lobby of Elias-Clarkes headquarters.
* The Runway offices are partially corridors in the neighboring Fox building and partially sets. 
* The Elias-Clarke cafeteria is the one at the Reuters office in Manhattan. 
* Nate and Andys apartment is on the Lower East Side. 
* The restaurant Nate works at (and where Andrea, Doug and Lily eat dinner on occasion) is in TriBeCa. 
* The Smith & Wollensky and its kitchen were used. 
* The Calvin Klein showroom is used in the deleted scenes. Frankel, David and Livolsi, Mark; commentary on deleted scenes on The Devil Wears Prada  . USA: 20th Century Fox. 
* Holts studio is a loft used by an actual designer. 
* The American Museum of Natural History was used for the exterior of the museum benefit, while the lobby of one of the Foley Square courthouses is used for the interior. 
* The Priestly townhouse is on the Upper East Side and belongs to a friend of Finermans. It had to be dressed on short notice after another one could not be used. 
* The Amtrak train the twins are taking is going up the Hudson River at Haverstraw Bay.
* Streep exits her limousine, supposedly in Paris, at 77th Street and Central Park West. New York Sun. 
* The cafe where Andy apologises to Nate was the Mayrose at 920 Broadway (near the Flatiron Building), which has since closed.  On its site is the Brio restaurant.

===== Paris =====
The crew were in Paris for only two days, and used only exteriors. Streep did not make the trip. 

* The fountain Andy throws her phone into is on the Place de la Concorde. W Hotel 

=== Post-production ===

==== Editing ====
Mark Livolsi realized, as McKenna had on the other end, that the film worked best when it focused on the Andrea-Miranda storyline. Accordingly, he cut a number of primarily transitional scenes, such as Andreas job interview and the Runway staffs trip to Holts studio. He also took out a scene early on where Miranda complimented Andrea. Upon reviewing them for the DVD, Frankel admitted he hadnt even seen them before, since Livolsi didnt include them in any prints he sent to the director. 

Frankel praised Livolsi for making the films four key Montage (film)|montages—the opening credits, Mirandas coat-tossing, Andreas makeover and the Paris introduction—work. The third was particularly challenging as it uses passing cars and other obstructions to cover Hathaways changes of outfit. Some scenes were also created in the editing room, such as the reception at the museum, where Livolsi wove B-roll footage in to keep the action flowing. 

==== Music ====
 
Composer   ("  ("Our Remains," Andrea picks up James Holts sketches for Miranda; Bittersweet Faith, Lilys art show), Azure Ray ("Sleep," following the breakdown of her relationship with Nate), Jamiroquai ("Seven Days in Sunny June," Andrea and Christian meet at James Holts party) among others. Frankel had wanted to use "City of Blinding Lights" in the film since he had used it as a soundtrack to a video montage of Paris scenes he had put together after scouting locations there.  Likewise, Field had advocated just as strongly for "Vogue." 
 archiveurl = archivedate = March 12, 2007|deadurl=yes}} 

== Pre-release and marketing == opening credit clappers seen type for soundtrack and DVD covers as well. 
 trailer of scenes and images strictly from the first three minutes of the film, in which Andrea meets Miranda for the first time, to be used at previews and film festivals until they could create a more standard trailer drawing from the whole film. But, again, this proved so effective with early audiences it was retained as the main trailer, since it created anticipation for the rest of the film without giving anything away. 

== Reception == Valentino can be seen behind and between Stanley and Lisa Tucci, and Beatrice Borromeo is partly visible to Stanley Tuccis left.]]

=== Critical response ===
Metacritic reported the film had an average score of 62 out of 100, based on 40 reviews. {{cite web
|url= http://www.metacritic.com/film/titles/devilwearsprada
|title=The Devil Wears Prada : Reviews
|accessdate=February 21, 2008
|publisher=Amazon.com | work=Metacritic
}} 
The film holds a 76% fresh rating on  , retrieved June 30, 2006 
 ; retrieved May 26, 2009 
"Wintour should be flattered by Streeps portrayal," agreed Jack Mathews in the  ; retrieved June 30, 2006   

 ; retrieved June 30, 2006.  J. Hoberman, Edelsteins onetime colleague at  ]." Hoberman, J.; June 27, 2006;  ; The Village Voice; retrieved June 30, 2006. 

Blunt, too, earned some favorable notice. "  has many of the movies best lines and steals nearly every scene shes in," wrote Clifford Pugh in the  ; retrieved December 24, 2006.  Other reviewers and fans concurred. Elliott, Michael; undated;  ; christiancritic.com; retrieved December 24, 2006.    Commentators on post by Slezak, Michael; August 11, 2006 et seq.;   popwatch; retrieved from ew.com December 24, 2006.   ; retrieved December 19, 2006.  while   Canada, who felt the film needed more of the nastiness others had told her was abundant in the novel. Baldassare, Angela; undated; " "; sympatico.msn.ca; retrieved January 7, 2007,   

 ; July 3, 2006; " "; The New Yorker; retrieved January 7, 2007  Reactions to Hathaways performance were not as unanimous as for many of her costars. Denby said "she suggests, with no more than a panicky sidelong glance, what Weisberger takes pages to describe."  On the other hand, to Baldassare, she "barely carrie  the load." 

==== Depiction of fashion industry ====
Some media outlets allowed their present or former fashion reporters to weigh in on how realistic the movie was. Their responses varied widely. Booth Moore at  ; retrieved from telegraph.co.uk December 22, 2006. 
"If they want a documentary, they can watch the  ; retrieved January 18, 2007.  Another newspaper fashion writer, Hadley Freeman of  ; retrieved January 10, 2007. 

Charla Krupp, the executive editor of SHOP, Inc., says "Its the first film Ive seen that got it right ...   has the nuances of the politics and the tension better than any film&nbsp;— and the backstabbing and sucking-up."  Joanna Coles, the editor of the U.S. edition of Marie Claire, agreed:
  Yves Saint Laurent that costs three times their monthly salary. Its also accurate in its understanding of the relationship between the editor-in-chief and the assistant. 
 
Ginia Bellefante, former fashion reporter for  ; retrieved January 15, 2007.  Her colleague Ruth La Ferla found a different opinion from industry insiders after a special preview screening. Most found the fashion in the movie too safe and the beauty too overstated, more in tune with the 1980s than the 2000s. "My job is to present an entertainment, a world people can visit and take a little trip," responded Field. 

=== Commercial ===
On its opening weekend, the film was on 2,847 screens. It grossed $27 million, second only to the much bigger-budget Superman Returns, and added $13 million more during the first week. This success led Fox to add 35 more screens the next week, the widest domestic distribution the film enjoyed. Although it was never any weeks top-grossing film, it remained in the top 10 through July. Its theatrical run continued through December 10, shortly before the DVD release.  , retrieved from boxofficemojo.com January 8, 2007. 
 Mamma Mia Alice in Get Smart in 2008, domestically). 
 , on whom Miranda is supposedly based, was at first skeptical of the film but later came to appreciate it.]]

=== Anna Wintour ===
 ; retrieved January 10, 2007.  But in an interview with  ; retrieved January 10, 2007. 

=== International ===
Weisbergers novel had been translated into 37 different languages, giving the movie a strong potential foreign audience. It would ultimately deliver 60 percent of the films gross.

The Devil Wears Prada topped the charts on its first major European release weekend on October 9, after a strong September Oceania and Latin America opening. It would be the highest-grossing film that weekend in Britain, Spain and Russia, taking in $41.5 million overall. Bresnan, Conor; October 9, 2006; " "; boxofficemojo.com; retrieved January 8, 2007.  Continued strong weekends as it opened across the rest of Europe helped it remain atop the overseas charts for the rest of the month. Bresnan, Conor; October 16, 2006; " ; boxofficemojo.com; retrieved January 8, 2007.  Bresnan, Conor; October 23, 2006; " "; boxoffficemojo.com; retrieved January 8, 2007.  Bresnan, Conor; October 30, 2006; " "; boxoffficemojo.com; retrieved January 8, 2007.  By the end of the year only its Chinese opening remained; it was released there on February 28, 2007.

Most reviews from the international press echoed the domestic response, heaping praise on Streep and the other actors, but calling the whole film "predictable."  ; retrieved January 10, 2007.   ; retrieved January 10, 2007.  In  ; retrieved July 4, 2008. 

In most markets the title remained unchanged; either the English was used or a translation into the local language. The only exceptions were Argentina, Chile, Colombia, Ecuador, Mexico and Venezuela, where it was El diablo que viste Prada and El diablo se viste a la moda. In Poland, the title was Diabeł ubiera się u Prady which roughly means "The Devil dresses (itself) at Prada" rather than "The Devil Wears Prada."In italian the title was ″Il diavolo veste Prada" which roughly means "The devil wears Prada". In Turkey, the title was "Şeytan Marka Giyer," roughly translated as "The Devil Wears Brand-Names." In Romania, the title was "Diavolul se îmbracă de la Prada," which roughly means "The Devil Dresses itself from Prada", the same construction being found in the French title, "Le Diable shabille en Prada". The Japanese version is titled "プラダを着た悪魔", which translates as "The devil wearing Prada".

=== Awards and nominations === Quill Variety (magazine)|Variety Blockbuster Book to Film Award. A committee of staffers at the magazine made the nominations and chose the award winner. Editor Peter Bart praised both works.
 
The Devil Wears Prada is an energetically directed, perfect-fit of a film that has surprised some in the industry with its box-office legs. It has delighted the country, much as did Lauren Weisbergers book, which is still going strong on several national bestseller lists   
  similar recognition. American Film Institute;  , retrieved December 19, 2006. 
 nominations were announced on December 14, 2006. The film itself was in the running for Best Picture (Comedy/Musical) and Supporting Actress (for Blunt). Streep later won the Globe for Best Actress (Musical/Comedy). Hollywood Foreign Press Association; January 16, 2007;  ; retrieved January 16, 2007. 
 nominated Streep for Best Actress as well. Screen Actors Guild; January 4, 2007;  , retrieved from sagawards.org January 4, 2007. 
Four days later, at the  ; retrieved January 10, 2007.  McKenna earned a nomination from the Writers Guild of America Award for Best Adapted Screenplay on January 11, 2007. Writers Guild of America Award,  ; retrieved January 11, 2007. 
 2006 nominations; Blunt, Field, McKenna and Streep were all among the nominees, as were makeup artist and hairstylists Nicki Ledermann and Angel de Angelis. British Academy of Film and Television Arts; January 12, 2007;  ; retrieved July 4, 2008. 

On January 23, 2007, Streep received her 14th  . Streep played along with a stern expression before smiling. Dehnhart, Andy; February 26, 2007;  ; MSNBC; retrieved February 26, 2007. 

=== Proposed television series ===
The success of the film led to a proposed, but unrealised, American dramedy series that was in contention to air for the 2007–08 television season on Fox Broadcasting Company|Fox. It was to be produced by Fox Television Studios.

The series was to be based on the book and 2006 film (which was produced by 20th Century Fox), but with the premise adjusted for the confines of a traditional half-hour or one-hour dramedy with a single camera set-up. Fox TV president Angela Shapiro-Mathes told Variety (magazine)|Variety: "The TV series will not be exactly like the movie or the book. The reason you loved the book and the reason you loved the movie was these were characters you really cared about in a world you wanted to learn more about. You cant read that book and not feel that the two characters are ones that you want to keep following. Its something you can get really passionate about."  The project never got to the pilot stage, and was shelved.

== Home media   ==
The DVD was released on December 12, 2006 and has, in addition to the film, the following extras:    editor Mark Livolsi, Field, screenwriter Aline Brosh McKenna, producer Wendy Finerman and cinematographer Florian Ballhaus.
* A five-minute   allowing the viewer to compare the actual shot with the blooper. The many shots of actors touching their noses are, Rich Sommer says, a game played to assign blame for ruined takes. Rich Sommer|Sommer, Rich; January 3, 2007; " "; retrieved January 16, 2007. 
* Five featurettes
** "Trip to the Big Screen", a 12-minute look at the films pre-production, discussing the changes made from the novel, how Frankel was chosen to direct and other issues.
** "NYC and Fashion", a look at the real New York fashion scene and how it is portrayed in the film.
** "Fashion Visionary Patricia Field", a profile of the films costume designer.
** "Getting Valentino", covering how the designer was persuaded to appear as himself in the film.
** "Boss from Hell", a short segment on difficult, nightmarish superiors like Priestly.
* Fifteen deleted scenes, with commentary from Frankel and Livolsi available (see below).
* The theatrical trailer (film)|trailer, and promotional spots for the soundtrack album and other releases.

Closed captions in French and Spanish are also available. The DVD is available in both full screen and widescreen versions. Pictures of the cast and the tagline "Hell on Heels" were added to the red-heel image for the cover. It was released in the UK on February 5, 2007.
 subtitle pop-up trivia track that can be watched by itself or along with the audio commentary. Bracke, Peter M.; December 11, 2006; " "; highdefdigest.com; retrieved January 18, 2007. 

=== Reception ===
Immediately upon its December 12 release, it became the top rental in the USA. It held that spot through the end of the year, adding another $26.5 million to the films grosses. To date the film has made $95,886,194 in DVD sales.  ; retrieved from boxofficemojo.com January 8, 2007.  The following week it made its debut on the DVD sales charts in third position. The Digital Entertainment Group;  ; retrieved from digitalentertainment.info January 8, 2007. 

=== Deleted scenes ===
Among the deleted scenes are some that added more background information to the story, with commentary available by the editor and director. Most were deleted by Livolsi in favor of keeping the plot focused on the conflict between Miranda and Andrea, often without consulting Frankel. 

Frankel generally approved of his editors choices, but differed on one scene, showing Andrea on her errand to the Calvin Klein showroom. He felt that scene showed Andreas job was about more than running personal errands for Miranda. 

== See also ==
 
*The September Issue; a 2009 documentary film which follows Anna Wintour prior to the release of the September 2007 Vogue issue.

== Works cited == The Devil Wears Prada, Broadway Books, New York 2003, ISBN 0-7679-1476-7

== References ==
 
 

== External links ==
 
*  
*  
*  
*  
*  
*   (includes full gag reel).
*   part of the BAFTA Screenwriters on Screenwriting series.

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 