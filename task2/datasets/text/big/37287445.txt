Evidence (1929 film)
{{Infobox film
| name           = Evidence
| image          =
| imagesize      =
| caption        =
| director       = John G. Adolfi
| producer       = 
| writer         = J. duRocher MacPherson (play) L. duRocher MacPherson (play) J. Grubb Alexander (screenplay) De Leon Anthony (titles)
| starring       = Pauline Frederick
| music          = Rex Dunn
| cinematography = Barney McGill
| editing        =
| distributor    = Warner Brothers
| released       =  
| runtime        = 79 minutes
| country        = United States
| language       = English
}}
Evidence is a 1929 sound horror/mystery film produced and distributed by the Warner Brothers. It is based on the 1914 Broadway play Evidence by J. duRocher MacPherson and L. duRocher MacPherson. This early talkie was directed by John G. Adolfi and starred Pauline Frederick and Lowell Sherman. While this film is  lost film|lost, its soundtrack, recorded by the Vitaphone process, survives.   

==Cast==
*Pauline Frederick - Myra Stanhope William Courtenay - Cyril Wimborne
*Conway Tearle - Harold Courtenay
*Lowell Sherman - Norman Pollock
*Alec B. Francis - Harbison
*Freddie Burke Frederick - Kenyon Wimborne
*Madeline Seymour - Mrs. Debenham
*Ivan F. Simpson - Peabody
*Myrna Loy - Native Girl
*Lionel Belmore - Innkeeper

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 