I Nostalgos
{{Infobox book 
| name         = I Nostalgos Η Νοσταλγός
| image        = 
| caption      = 
| author       = Alexandros Papadiamantis
| language     = Greek
| genre        = Short Novel
| publisher    = 
| release_date =1894
| media_type   = 
| pages        = 
| isbn         = 
| dewey        = 
| congress     = 
| oclc         = 
| preceded_by  = 
| followed_by  = 
}} Greek essayist and novelist Alexandros Papadiamantis. The novel was written in 1894.  In 2005 the novel was adapted for the cinema by the director Eleni Alexandraki and in 2013 was adapted for the theatre by the director Dimos Abdeliodis.    

==Plot==
Anna married with a much older husband decides to return in the island where she was born. He persuades Matthios, a young shepherd, to help her to escape from her husband and return to her island. Matthios is secretly in love with Anna and during their travel he hopes to keep Anna forever. But anna is homesick only for the place that she was born. Her husband worried, follows her to persuade her to turn back. 

==The film==
{{Infobox film
| name           = I Nostalgos Η Νοσταλγός
| image          =
| caption        =
| director       = Eleni Alexandraki
| producer       =
| writer         = Alexandros Papadiamantis (novel) Eleni Alexandraki (screenplay)
| starring       = Olia Lazaridou
| music          = Nikos Papazoglou
| cinematography = Hristos Asimakopoulos Vassilis Kapsouros
| editing        = Georgios Triantafyllou
| distributor    =
| released       =  
| runtime        = 87 minutes
| country        = Greece
| language       = Greek
| gross          =
}}
I Nostalgos is a Greek film released in 2005. It was directed by Eleni Alexandraki.  The screenplay is based in homomymous novel of Alexandros Papadiamantis of 1894. The film stars Olia Lazaridou and it is shot in Nisyros. The film carries the story from Skiathos, the homeland of Alexandros Papadiamantis, to Nisyros.  It won one award in Greek State Film Awards for the best music.

===Cast===
*Olia Lazaridou as Anna
*Giorgos Tsoularis as Mathios
*Spyros Stavrinidis as Pantelis Monahakis
*Maria Mina as Annas Mother

===Awards===
{| class="wikitable"
|+ List of awards and nominations
! Award !! Category !! Recipients and nominees !! Result
|- Greek State 2005 Greek Best Music||Nikos Papazoglou|| 
|}

==References==
 

==External links==
*  Ἡ Νοσταλγός (1894)
* 

 
 