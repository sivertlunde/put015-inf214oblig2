Children of War (2014 film)
{{Infobox film
| name           = Children of War
| image          = 
| alt            = 
| caption        = 
| director       = Mrityunjay Devvrat
| producer       = Soumya Joshi Devvrat
| writer         = 
| starring       = Riddhi Sen Rucha Inamdar Victor Banerjee Farooque Shaikh Pavan Malhotra Indraneil Sengupta Raima Sen Tilotama Shome Shatrunjay Devvrat
| music          = Sidhant Mathur (Songs) Ishaan Chhabra (Score)
| cinematography = Fasahat Khan
| editing        = Apurva Asrani
| studio         = 
| distributor    = 
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Hindi
| Website        = http://www.childrenofwar.in
| budget         = 
| gross          = 
}} Bangladesh Liberation War. 

==Cast==
*Riddhi Sen as Rafiq
*Rucha Inamdar as Kausar 
*Victor Banerjee as Baba
*Pavan Malhotra as Malik 
*Indraneil Sengupta as Aamir
*Raima Sen as Fida Tilottama Shome as Bhitika
*Shatrunjay Devvrat as Young Aamir
*Joy Sengupta as Sudipto
*Purnendu Bhattacharya as Abbu
*Ganga Chakravorthy as Ammi
*Roopa Ganguly as Raziya
*Aweree Chaurey as Durga (Old Woman) 
*Chandra Sengupta as Old Man
*Rupsa Mondal as Mariyam
*Bhushan Vidhate as Raziyas Husband
*Harish Chhabra as Soldier Bull 1
*Devendra Chauhan as Soldier Bull 2
*Vidya Bhushan as Old Pandit
*Sanjeeta Mukherjee as Mad Woman
*Ketan Rathod as Mojids Servant 
*Guddu Gupta as Ashraf 
*Debashish Mondol as Captured Razakaar
*Prodip Ganguly as Sheikh Mujibur Rahman 

==Production==
While writing the films script, Devvrat researched the topic thoroughly, which included interviews with various journalists, war veterans, and refugees.   Filming for The Bastard Child was supposed to take place in Bengal, but the film was forced to relocate after only 8 days of shooting due to problems with the Federation of Cine Technicians and Workers of Eastern India, as there were issues with the amount of local hands hired to work on the movie.    Filming also took place around Delhi and Haryana, and overall filming for The Bastard Child occurred mostly at night.    In 2013 Farooq Sheikh was confirmed to be performing in the film, only for rumors to surface that he be replacing Sabyasachi Chakraborty in the movie.    Devvrat addressed these rumors in June 2013, stating that Chakraborty was still part of the movie but that his original role had been split into two different characters due to the director wanting to avoid further potential issues with the Federation of Cine Technicians and Workers of Eastern India.  The production for The Bastard Child experienced further issues with the films release, as the Indian Motion Picture Producers Association did not approve of the movies title, which included the word "bastard".    Devvrat responded to the criticism over the title, stating that he did not intend for the word to be seen as offensive. 

Of her role in the film, Raima Sen felt that it was "one of the toughest roles she has portrayed on screen in the recent past." 

==Reception & Awards==
Critical reception so far has been positive.  Live Mint gave a positive review for the movie, which they felt contained "beautifully filmed grief and devastation".    Page3 Bollywood also gave a positive review, praising the films acting and musical score.  Subhash K. Jha also gave a positive review, he stated "It is impossible to believe that this war epic has been directed by a first-time filmmaker. How can a virgin artiste conceive such a vivid portrait of the rape of a civilization? 

The Government of Bangladesh has recognised the film and has recently adopted a proposal recognising 1971 war rape survivors as freedom fighters

1.	‘ Extraordinary Film’ – River to River Film Festival, Florence Italy 2014

2.	‘ Extraordinary Film’ – River to River Film Festival, Rome Italy 2014

3.	‘Director’s Choice’ presented by Hansal Mehta – MAMI 2014 
           With only five other films ( Bandit Queen, Anand, Parinda, Black Friday and Junoon)

4.	‘Breaking Boundaries’ – IFFI Goa 2014 (International Film Festival of India.)

5.	Best Cinematography  - Jagran International Film Festival 2014

6.	Best Negative Lead – Jagran International Film Festival 2014

7.	‘Best Topical Content’ – Global Bioscope Film Festival 2014

8.     ‘Best Debut Director’ – International Film Awards, Jakarta 2015

9.     ‘Best Debut Director’ – Patna International Film Festival 2015

10.   ‘Official Selection’ – Colortape International Film Festival, Australia 2015

==References==
 

==External links==
*  
*   - Official Movie Website

 
 