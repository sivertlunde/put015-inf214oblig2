The Day Christ Died
{{Infobox film
| name = The Day Christ Died
| image = The Day Christ Died.jpg
| caption =
| director = James Cellan Jones
| writer =
| starring =
| music = Laurence Rosenthal
| cinematography = Franco Di Giacomo
| editing =
| producer =
| distributor = CBS
| released =  
| runtime =
| awards =
| country = United States
| language = English
| budget =
}} 1980 American TV movie directed by James Cellan Jones. The collaborative production by 20th Century Fox and CBS-TV dramatizes the last 24 hours of Jesus Christs life and is based on Jim Bishops 1957 book of the same name.  Bishop, who did not accept the script adaptation, had his name removed from the credits, called the film "cheap revisionist history", and even tried unsuccessfully to change the films title.  The Day Christ Died was filmed in Tunisia, at a cost of USD$2.8 million.    It was broadcast by CBS-TV on Wednesday, March 26, 1980. 

== Cast ==
*Chris Sarandon     as	Jesus Christ
*Colin Blakely	as	Caiaphas
*Keith Michell	as      Pontius Pilate Herod
*Barrie Judas
*Jay O. Sanders	as	Simon Peter Mary
*Delia Boccardo	as	Mary Magdelene Claudia (Pontius Pilates wife) John

==References==
 

==External links==
* 
*  Full video (duration 2:08:45).
*  Video in four parts.

 
 
 
 
 
 
 
 
 
 