Aloha from Hawaii Via Satellite
 
{{Infobox film
| name           = Aloha from Hawaii via Satellite
| image          = Aloha from Hawaii Via Satellite.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Marty Pasetta
| producer       = Marty Pasetta
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Elvis Presley
| music          = 
| cinematography = 
| editing        = Stephen McKeown
| studio         = Pasetta Productions
| distributor    = RCA
| released       =  
| runtime        = 85 minutes
| country        = 
| language       = English
| budget         = $2.5 million   
| gross          =
}}
 music concert that was headlined by Elvis Presley, and was broadcast live via satellite on January 14, 1973. The concert took place at the Honolulu International Center (HIC) in Honolulu (now known as the Neal S. Blaisdell Center) and aired in over 40 countries across Asia and Europe (who received the telecast the next day, also in primetime). Despite the satellite innovation, the United States did not air the concert until April 4, 1973 (because the concert took place the same day as Super Bowl VII). Viewing figures have been claimed by the promoters at over 1 billion viewers worldwide, although the accuracy of these figures has been debated. The show was the most expensive entertainment special at the time, costing $2.5 million. 

== Background ==
On July 8, 1972, inspired by a recent visit made by U.S. President  . Guralnick/Jorgensen, Elvis: Day by Day, p. 312.  As the show had already been planned prior to this upset, the original shows, now set for November, would still go ahead but without being filmed. Guralnick/Jorgensen, Elvis: Day by Day, p. 316. 
 Las Vegas Our World broadcast was the first live, international, satellite television production, which was broadcast worldwide on June 25, 1967, which included entertainment artists such as Maria Callas and headlined by The Beatles) - Aloha From Hawaii was the first live satellite concert to be with a single performer. Two weeks after the Las Vegas press conference Parker received a letter from Honolulu Advertiser columnist Eddie Sherman. Guralnick (1999), p. 478.  Sherman had read in news accounts that there was to be no charge for admittance to the concerts, instead a donation for charity was required. He suggested to Parker that, as Presley had recorded, and was still performing, the song "Ill Remember You," which had been written and composed by Kui Lee, the donations could go to the Kui Lee Cancer Fund that had been set up following the death of the songwriter in 1966.  Seeing the chance to publicize Presleys charitable nature once again, Parker eagerly agreed. 
 Long Beach in mid-November, and he had found it to be "boring" and lacking in any physical excitement. Guralnick (1999), p. 479.  He approached Parker with ideas about the broadcast, including a runway that led out from the stage so Presley could get closer to his audience.  Parker insisted that the ideas were useless, and that Presley would agree that they were useless.  Pasetta, however, decided to approach Presley about the ideas anyway and was pleasantly surprised to find that he would be happy to do whatever Pasetta felt was best for the show.  This was another example of the ever-growing rift between Presley and his manager. Although Presley would later dismiss Parker, whom he never truly befriended and towards whom he was openly hostile in his later years, the two would reconcile professionally shortly thereafter, and Parker would remain Presleys manager for the rest of the artists life.

Presley performed three shows over November 17 and 18 in Honolulu, the dates originally planned for the satellite broadcast,  and gave a press conference on November 20 to promote the satellite special.  He also announced officially that it would now be in aid of the Kui Lee Cancer Fund. 

Presley arrived in Hawaii again on January 9, a day after his 38th birthday, to begin rehearsals.  He had lost twenty-five pounds for the show Guralnick (1999), p. 481.  and was confident after news that his record sales were increasing and that Elvis on Tour had been nominated for a Golden Globe.  Rehearsals were held at the Hilton Hawaiian Village while the main set was being constructed. Guralnick (1999), p. 482.  Although there were several technical problems, the rehearsals were an overall success. 

== Broadcast ==
 
Presley taped a January 12 rehearsal concert as a fail-safe in case anything went wrong with the satellite during the actual broadcast – however, nothing went wrong during the January 14 broadcast. For both shows, Presley was dressed in a white "American Eagle" jumpsuit designed by Bill Belew. The broadcast was directed by Marty Pasetta, who was then in charge of directing the Oscar ceremonies.

Audience tickets for the January 14 concert and its January 12 pre-broadcast rehearsal show carried no price. Each audience member was asked to pay whatever he or she could afford. The performance and concert merchandise sales raised $75,000 for the Kui Lee Cancer Fund in Hawaii.

===Viewing figures===
Elvis Presley Enterprises give the figure that between 1 billion and 1.5 billion people watched the one hour broadcast live, a debatable figure as the total population of the countries receiving the broadcast was 1.3 billion. 

==Set list==
 Also Sprach Zarathustra", performed by the Joe Guercio Orchestra)

*"See See Rider"
*"Burning Love"
*"Something (Beatles song)|Something"
*"You Gave Me a Mountain"
*"Steamroller Blues" My Way" Love Me"
*"Johnny B. Goode"
*"Its Over" 
*"Blue Suede Shoes"
*"Im So Lonesome I Could Cry"
*"I Cant Stop Loving You" Hound Dog" What Now My Love"
*"Fever (Little Willie John song)|Fever" Welcome to My World"
*"Suspicious Minds"

*Elvis introduces J. D. Sumner|J.D. Sumner & the Stamps Quartet, the Sweet Inspirations, Kathy Westmoreland, members of the TCB Band, and the Joe Guercio Orchestra

*"Ill Remember You"
*"Long Tall Sally"/"Whole Lotta Shakin Goin On"
*"An American Trilogy"
*"A Big Hunk o Love"
*"Cant Help Falling in Love"

*Closing vamp performed by the TCB Band and the Joe Guercio Orchestra

After the concert was over, Presley returned to the empty arena with the TCB Band to record five songs for the American airing of the show, including  " "No More" and "Ku-U-I-Po". 

== Soundtrack album ==
 
The album containing the music from the concert was a blockbuster hit, becoming Presleys first chart-topping album in the US since the soundtrack to Roustabout in 1965.  The original release of the album, however, did not include the five post-show performances. The album would be Presleys final number 1 album  during his lifetime. 
 Billboard Billboard album chart, and remains the biggest-selling release in the format. 

Presley was accompanied by:

Vocalists
*J. D. Sumner|J.D. Sumner & the Stamps Quartet (Ed Enoch, Bill Baize, Donnie Sumner, Richard Sterban, JD Sumner) Priddle, Mel.  , AllExperts, May 15, 2007. Retrieved December 23, 2011. 
*The Sweet Inspirations (Myrna Smith, Sylvia Shemwell, Estelle Brown)
*Kathy Westmoreland
His TCB Band
*James Burton (lead guitar)
*John Wilkinson (rhythm guitar)
*Ronnie Tutt (drums)
*Jerry Scheff (Fender bass)
*Glen Hardin (piano/keyboards) Charlie Hodge (vocals/acoustic guitar)
Joe Guercio Orchestra 
*Joe Guercio was Elviss conductor and musical director and, with a few exceptions, the orchestra consisted of local musicians contracted for this particular engagement. The brass players were Patrick Houston, Thomas Porrello, Gary Grant and Forrest Buchtel on  ; and William Konney and Beverly LeBeck on cello. Rounding out the orchestra were Frank Strazzeri on Hammond organ and Dean Appleman on percussion. Houston, Porrello, Harrell and Strazzeri had toured with Elvis in 1972 and were brought to Hawaii for the show, as were Buchtel and Corimby. Harvey Ragsdale was the Hawaiian contractor who hired local musicians for the orchestra.   

== DVD releases ==
  The 68 Dolby Digital digitally remastered master tapes.
 Easter Eggs" and can be found by pressing a hidden button in the menu.

A bronze statue of Elvis was unveiled in front of Neal Blaisdell Center Arena in Honolulu.  The statue was sponsored by TV Land channel.

== Charts and certifications ==
 
 
{|class="wikitable sortable" border="1" Chart (2004) Peak position
|- Australian Top 40 Music DVDs  1
|- Austrian Top 10 Music DVDs  7
|- Belgium (Flanders) Top 10 Music DVDs  2
|- Belgium (Wallonia) Top 10 Music DVDs  1
|- Ireland Top 20 DVDs  17
|- Japanese DVDs Chart  117
|- Netherlands Top 30 Music DVDs  2
|- New Zealand Top 10 Music DVDs  6
|- Norwegian Top 10 DVDs  2
|- Spanish Top 20 Music DVDs    6
|- Swedish Top 20 DVDs  1
|-
|}
{|class="wikitable sortable" border="1" Chart (2006) Peak position
|- Italian Top 20 Music DVDs  14
|-
|}
 
 
 
 
 
 
 
 
 
 

 

== See also ==
*Aloha from Hawaii Via Satellite (album)

== References ==
*  
*  
  

== External links ==
* 
*  
*  all you need to know about this live performance

 

 
 
 
 
 
 
 

 