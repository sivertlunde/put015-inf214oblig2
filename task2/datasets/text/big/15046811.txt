Loss (film)
 
{{Infobox film
| name           = Loss
| film name      =  
| image          = loss_movie_2008.jpg
| caption        = Theatrical poster Maris Martinsons Maris Martinsons Maris Martinsons Raimondas Paškevičius
| starring       = Valda Bičkutė Kostas Smoriginas Dalia Micheleviciute Andrius Mamontovas Daiva Tamošiūnaitė-Budrė
| music          = Andrius Mamontovas
| cinematography = Gints Berzins, LGC Maris Martinsons
| distributor    = Forum cinemas
| released       =  
| runtime        = 95 minutes
| country        = Lithuania
| language       = Lithuanian
| budget         = 2.1 million litas
| gross          =
}}
 Latvian film Maris Martinsons. submission for the Academy Award for Best Foreign Language Film in the 81st Academy Awards, becoming the first Lithuanian feature film ever to be submitted for the Academy Awards.

==Awards==
International premiere of Loss was held in   and Best Music for Andrius Mamontovas. It is the first Lithuanian feature film to be screened in the competition  of A class festival and awarded prestigious prizes.

2009 "Loss" was awarded with the Latvian Film Prize for the BEST FILM.

==Synopsis==
Loss is a story about a Priest (starring Andrius Mamontovas) in Ireland who meets a woman Valda (starring Valda Bičkutė) from his native Lithuania only to find out her mysterious identity is closely connected with a darkest secret of his past.

Desperate for a better life Valda left Lithuania for Ireland, hoping to find a new purpose in life but failed to fulfill a promise to come back she made to her son. Now a boy is adopted by  Nora (starring Dalia Michelevičiūtė), a childless ex-wife of the desperate businessman Ben (starring Kostas Smoriginas) whose life is falling from his hands. Caught in a struggle with her husband’s mistress Laima (starring Daiva Tamošiūnaitė-Budrė) she must fight not only for her company but also for the custody of a child who now is her only hope in life.
 Six Degrees of Separation and Feodor Dostoevsky|Dostoevsky’s line “the tears of that one tortured child”. Loss delves into the harsh realities of six people’s lives whose destinies are brought together after a tragic accident that happened over two decades ago.

==Style== Irish folk rhythms.

==Production==
Loss has been shot in Lithuania and Ireland. The exterior scenes were shot in Wicklow town, Powerscourt Waterfall, etc.

==Box office==

Total box office of this film from exhibition Lithuania is 594 763 Lt.

==External links==
*  
* 
* 

 
 
 
 