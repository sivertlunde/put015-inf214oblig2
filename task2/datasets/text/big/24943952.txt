Black Rodeo
{{Infobox film
| name           = Black Rodeo
| image          =
| image_size     =
| alt            =
| caption        =
| director       = Jeff Kanew
| producer       =
| writer         = Jeff Kanew
| narrator       = Woody Strode
| starring       = Muhammad Ali
| studio         =
| distributor    = Cinerama Releasing
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Black Rodeo is a 1972 documentary by filmmaker Jeff Kanew. 

==Overview==
Black Rodeo captures the events surrounding the first-time performance of an all African-American rodeo in Harlem, New York City.  The documentary depicts that the people who attended the rodeo were awed to find African-American men and women actively involved in skills like bronc riding, calf roping and brahma bull riding.  

Actor Woody Strode attended the rodeo and appears in the film as its narrator. He imparts a number of stories that show the participation of blacks in the development of the American Old West. 
 125th Street (the main street in Harlem), trades friendly verbal jabs with the cowboys and straps on chaps and rides a bull. 

The rodeo events in the film are set to the music of Aretha Franklin, Ray Charles, Little Richard, Lee Dorsey, Sammy Turner, Little Eva and other R&B acts. 

The film was released by Cinerama Releasing in the spring of 1972.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 