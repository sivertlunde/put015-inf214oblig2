List of horror films of 2013
 
 
A list of horror films released in 2013 in film|2013.

{| class="wikitable sortable" 2013
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Alpha Girls
| Tony Trov, Johnny Zito|| Ron Jeremy, Nikki Bell ||   || 
|-
!   | Arcana (film)|Arcana
| Yoshitaka Yamaguchi|| Tao Tsuchiya, Masataka Nakagauchi, Kaito (actor)|Kaito||   || 
|-
!   | Bad Milo! Comedy horror 
|-

!   | Bunshinsaba 2
| Ahn Byeong-ki|| Xin Zhilei, Park Han-byul, Zhang Haoran||   || 
|-
!   | Carrie (2013 film)|Carrie
| Kimberly Peirce|| Chloë Grace Moretz, Julianne Moore, Gabriella Wilde||   ||  
|-
!   | The Chrysalis Qiu Chuji||Sandrine Pinna, Ren Quan, Lee Wei|| || 
|-
!   | Curse of Chucky Don Mancini || Fiona Dourif, Danielle Bisutti, Brennan Elliott|| || 
|- The Complex
| Hideo Nakata|| Atsuko Maeda, Hiroki Narimiya, Masanobu Katsumura||   ||  
|- The Conjuring Vera Farmiga, Patrick Wilson, Ron Livingston, Lili Taylor|| || 
|- Dark Skies Scott Stewart|| Josh Hamilton, Dakota Goyo||   ||  
|-
!   | Dark Touch Marina de Van|| Marie Missy Keating, Marcella Plunkett, Padraic Delaney||  || 
|-
!   | The Deadly Strands Leon Dai, Zak Di, Kong Qianqian|| || 
|-
!   | The Demons Rook Ashleigh Jo Sizemore, James Sizemore, John Chatham|| || 
|-
!   | Discopath
| Renaud Gauthier|| || ||  
|- Evil Dead Jane Levy, Shiloh Fernandez, Lou Taylor Pucci||  ||  
|-
!   | Fish %26 Cat
| Shahram Mokri || Babak Karimi, Saeed Ebrahimifar, Siavash Cheraghipoor||  ||
|-
!   |   Eduardo Rodriguez Sean Power||  || 
|-
!   | Frankensteins Army
| Richard Raaphorst || Karel Roden, Joshua Sasse, Robert Gwilym|| || 
|-
!   | Hatchet III
| B.J. McDonnell|| Danielle Harris, Kane Hodder, Zach Galligan||   ||  
|-
!   |  
| Tom Elkins|| Chad Michael Murray, Abigail Spencer, Katee Sackhoff||   ||  
|-
!   | Horror Stories 2
| Jeong Beom-Sik, Kim Hwi, Kim Seong-ho, Min Gyoo-dong|| || || 
|-
!   | I Spit on Your Grave 2
| Steven R. Monroe || Jemma Dallender, Joe Absolom, Yavor Baharoff||  ||  
|-
!   | Its a Beautiful Day (film)|Its a Beautiful Day Kim Kkot-bi, Nanako Ohata, Adam LaFramboise||  || 
|-
!   | Killer Toon
| Kim Yong-kyoon ||  Lee Si-young, Um Ki-joon, Hyeon-u|| || 
|-
!   | The Last Exorcism Part II Ashley Bell, Julia Garner, Spencer Treat Clark||  ||  
|-
!   | Lift to Hell
| Ning Jingwu || Blue Lan, Chrissie Chau, Tse Kwan-ho|| || 
|- Long Weekend
| Taweewat Wantha|| Chinawut Indracusin, Sheranut Yusananda, Sean Jindachot||  ||  
|-
!   | Mama (2013 film)|Mama
| Andres Muschietti|| Jessica Chastain, Nikolaj Coster-Waldau, Megan Charpentier||    ||   
|- Midnight Train
| Zhang Jiangnan|| Huo Siyan, Kara Hui, Calvin Li||   ||  
|-
!   | Miss Zombie
| Sabu (director)|Sabu|| Ayaka Komatsu, Makoto Togashi, Riku Onishi||   ||  
|-
!   | Mysterious Island 2 Rico Chung||Deng Jiajia, Julian Chen, Wei Yuhai|| || 
|- 
!   | Pee Mak Banjong Pisanthanakun||Mario Comedy horror 
|-
!   | Possession (2013 film)|Possession Meryll Soriano, Baron Geisler, Dennis Trillo|| ||  
|- Rigor Mortis Juno Mak||Chin Siu-ho, Kara Hui, Nina Paw|| || 
|-
!   | The Supernatural Events on Campus Guan Er||Zhao Yihuan, Wang Yi, Li Manyi|| || 
|-
!   | Tales from the Dark 1 Simon Yam, Tony Leung Ka-fai, Susan Shaw|| || 
|-
!   | Tales from the Dark 2 Fala Chen, Chan Fat-kuk, Teddy Robin|| || 
|-
!   | Texas Chainsaw 3D John Luessenhop||Alexandra Daddario, Scott Eastwood, Tania Raymonde|| || 
|-
!   | V/H/S/2
|Various||Various|| || 
|- We Are What We Are
| Jim Mickle || Bill Sage, Ambyr Childers, Julia Garner|| || 
|- Willow Creek
| Bobcat Goldthwait || Alexie Gilmore, Bryce Johnson||  || 
|-
!   | Witching and Bitching
| Álex de la Iglesia || Mario Casas, Hugo Silva, Carmen Maura||  || 
|- World War Z Chris LaMartina||Paul Fahrenkopf, Aaron Henkin, Nicolette le Faye|| ||   
|-
|}

==References==
 

==External links==
*  

 
 
 

 
 
 
 