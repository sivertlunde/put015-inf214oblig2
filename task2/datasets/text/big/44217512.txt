Street of Sinners
{{Infobox film
| name           = Street of Sinners
| image          = Street of Sinners poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = William Berke
| producer       = William Berke 
| screenplay     = John McPartland 	
| story          = Philip Yordan George Montgomery Geraldine Brooks Nehemiah Persoff Marilee Earle William Harrigan Stephen Joyce
| music          = Albert Glasser
| cinematography = J. Burgi Contner 	
| editing        = Everett Sutherland 	
| studio         = Security Pictures
| distributor    = United Artists
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 George Montgomery, Geraldine Brooks, Nehemiah Persoff, Marilee Earle, William Harrigan and Stephen Joyce.   The film was released in September 1957, by United Artists.

==Plot==
 

== Cast == George Montgomery as John Dean Geraldine Brooks as Terry
*Nehemiah Persoff as Leon
*Marilee Earle as Nancy
*William Harrigan as Gus
*Stephen Joyce as Ricky
*Clifford David as Tom
*Diana Millay as Joan
*Andra Martin as Frances 
*Danny Dennis as Short Stuff
*Ted Erwin as Sergeant #1
*Melvin Decker as Tiny
*Lou Gilbert as Sam
*Barry McGuire as Larry
*Elia Clark as Boy
*William Kerwin as Tom
*Jack Hartley as Fire captain
*Billy James as Joey
*Liza Balesca as Sams wife
*Eva Gerson as Tinys mother
*John Holland as Harry
*Bob Duffy as Motor cop
*Joey Faye as Pete
*Fred Herrick as Sergeant #2
*Charlie Jordan as Customer
*John Barry as Utility bartender
*Wolfe Barzell as Tinys father Stephen Elliott as Bit part

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 