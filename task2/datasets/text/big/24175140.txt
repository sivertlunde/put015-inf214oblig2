Stacy's Knights
{{Infobox film
| name           = Stacys Knights
| image          = Poster of the movie Stacys Knights.jpg
| image_size     =
| caption        = Jim Wilson Jim Wilson (executive producer) Michael Blake
| narrator       =
| starring       = See below
| music          = Norton Buffalo Joao Fernandes (credited as Raul Lomas)
| editing        = Bonnie Koehler
| distributor    =
| released       = June 1983
| runtime        = 100 minutes
| country        = USA
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Jim Wilson.

The film is also known as Double Down, The Touch (American video title), Winning Streak (British video title).

== Plot summary == cheating dealer to stop her and eventually they have Will killed. In retaliation, Stacy recruits a team of players and trains them to win at the game. The team returns to the casino, with Stacy in disguise, to avenge Wills murder by winning a large amount of money.

== Cast ==
*Kevin Costner as Will Bonner
*Andra Millian as Stacy Lancaster
*Eve Lilith as Jean Dennison Mike Reynolds as Shecky Poole
*Garth Howard as Mr. C.
*Ed Semenza as The Kid
*Don Hackstaff as Lawyer
*Loyd Catlett as Buster
*Cheryl Ferris as Marion
*Gary Tilles as Rudy
*Roge Roush as Rollin
*John Brevick as Floor Boss
*Robin Landis as Bourbon Drinker
*Shashawnee Hall as Recruit
*Robert Conder as Recruit
*Frederick Hughes as Recruit
*Steve Noonan as Make-Up Man
*David Brevick as Rejected Recruit
*Steve Kopanke as Video Tech #1
*Jim Kosub as Video Tech #2
*Ray Whittey as Frisker
*Roy Reeves as Dealer
*Tena Knox as Dealer
*Joanne Lisosky as Dealer
*Theresa Thompson as Dealer
*Mark Conrad as Dealer
*Pete Borsz as Dealer
*Dennis Pflederer as Security Man
*Jay Conder as Pit Boss
*John Coinman as Stiff

== Soundtrack ==
* "Good Things" (Written by John Lawrence, co-writer with J. Lawrence, Ken Hilton)

== External links ==
* 

 
 
 
 
 
 
 


 