Too Much, Too Soon
 

{{Infobox film
| name           = Too Much, Too Soon
| image          = File:Too Much, Too Soon - Poster.jpg
| image_size     =
| caption        = 1958 Theatrical Poster
| director       = Art Napoleon
| producer       = Henry Blanke
| writer         = Diana Barrymore (book) Gerold Frank (book) Art Napoleon Jo Napoleon
| narrator       =
| starring       = Dorothy Malone Errol Flynn Ernest Gold
| cinematography = Carl E. Guthrie
| editing        =
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 121 min.
| country        = United States English
| budget         =
}} 1958 biographical Ernest Gold and the cinematography by Carl E. Guthrie. Diana died in 1960, two years after the release of this film.

It stars Dorothy Malone and Errol Flynn (as John Barrymore), with Efrem Zimbalist Jr., Ray Danton, Neva Patterson, Murray Hamilton and Martin Milner.

==Plot==

Fourteen-year-old Diana Barrymore is being raised by her domineering mother, a poet. Her father, the famed actor John Barrymore, has not laid eyes on Diana for 10 years, but they share an evening on his boat before John abandons her again.

At 18, Diana has become an actress and has a steady boyfriend, Lincoln Forrester. When a Hollywood contract comes her way, Dianas mother warns her not to live with John, now a washed-up alcoholic.

She finds her father living in a nearly empty mansion, having sold or pawned his belongings to pay his bills. He keeps a bald eagle in a cage indoors and has a servant, Gerhardt, who must physically knock out John to put him to bed.

Dianas famous name gains her some publicity, but her performances are panned. Her new husband, actor Vince Bryant, is away a lot, so Diana turns to drink and leaves Vince for tennis player John Howard. When her father dies alone, a penniless and often drunk Diana and her husband move in with her mother, who can only stand so much before making them leave.

After marrying again, this time to recovering alcoholic Bob Wilcox, she discovers after her mothers death that she has been left no inheritance. Diana takes demeaning jobs, including a striptease. She becomes violent and is hospitalized. Her only hope at salvation is an offer to write her memoirs, and old friend Linc returns to her life, offering some badly needed kindness.

==Cast==
* Dorothy Malone as Diana
* Errol Flynn as John Barrymore
* Efrem Zimbalist, Jr. as Vincent Bryant - in real life this was Bramwell Fletcher
* Martin Milner as Lincoln Forrester
* Neva Patterson as Dianas mother Blanche Oelrichs
* Ray Danton as John Howard  Robert Wilcox 
*Robert Ellenstein as Gerold Frank
*Martin Milner as Lincoln Forrester

==Original book==
{{infobox book |  
| name          = Too Much Too Soon
| title_orig    = 
| translator    = 
| image         = 
| caption = 
| author        = Diana Barrymore Gerold Frank
| illustrator   = 
| cover_artist  = 
| country       =  English
| series        = 
| genre         = autobiography
| publisher     = Henry Holt & Co.
| release_date  = 1957
| english_release_date =
| media_type    = 
| pages         = 380
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
The movie was based on the best selling 1957 autobiography, by Barrymore and Gerold Frank. Frank had previously worked on Ill Cry Tomorrow, a popular book about another alcoholic celebrity, Lillian Roth. 

"Theres no message, I didnt set out to point a moral," said Barrymore. "But writing it has been a cleansing process. Its like psychiatry in a way." BIOGRAPHY IS TALE OF GIRL WITH THREE LIVES: Diana Barrymores Story: The Glitter and Bitter
Pauley, Gay. Los Angeles Times (1923-Current File)   23 Apr 1957: A2.  

When the book was published the New York Times called it "an extremely skilful piece of work, a craftsmans product aimed at a mood and a market that spell big business. It is a book for the mass audience... as an artisan, Mr Frank is no slouch." For Diana, Nothing Failed Like Success: Nothing Like Success
By ELIZABETH JANEWAYFrom "Too Much, Too Soon.". New York Times (1923-Current file)   07 Apr 1957: 255.   The Washington Post thought the book "fails to touch the heart even though it spins a recognisably sad story." Two Ladies Sad Bouts With the Bottle
-- GLENDY CULLIGAN.. The Washington Post and Times Herald (1954-1959)   07 Apr 1957: E7.   Louella Parsons said the book "told too much too loudly." Too Much May Be Just That, All Right
By Louella Parsons. The Washington Post and Times Herald (1954-1959)   07 Apr 1957: H7.  

The book became a best seller. BEST SELLING Books IN THE MIDWEST
Chicago Daily Tribune (1923-1963)   02 June 1957: b3.   Heinrich Heines Background Told
Los Angeles Times (1923-Current File)   09 June 1957: F5. 

By the time the book came out Diana Barrymore tried to reactivate her acting career and was seeing a psychiatrist but she had not given up drinking. 
==Production==
There was film interest in the book early on - Ill Cry Tomorrow had been a box office hit and Diana Barrymore had been fictionalised in a popular movie, The Bad and the Beautiful (1952) (the character played by Lana Turner).  In December 1956, even before the book had been published, Warner Bros took an option on the film rights for a reported minimum of $100,000. OF PEOPLE AND PICTURES: COLLEGIAN
By A.H. WEILER. New York Times (1923-Current file)   16 Dec 1956: X7.   In January it was announced that Gerold Frank would work on the script in collaboration with Irving Wallace, and that Irving Rapper would direct and Henry Blanke would produce.  By June however it was reported that the film was having "script problems" with the script two months overdue. Roz Has a Gala Birthday Celebration
The Washington Post and Times Herald (1954-1959)   12 June 1957: D6.   In August, Warners said that Art and Jo Napoleon would write and direct the movie. 
===Casting===
Originally, Carroll Baker, who had just made a big impression with Baby Doll (1956) and was under contract to Warners, was to star as Diana.  Frederic March was mentioned as a possible John Barrymore.  However, Baker refused to play the role, and Warner Bros put her on suspension FILM BODY RULES ON OSCAR WINNER: Mysterious Author of Brave One Must Identify Himself to Claim the Award Two Join Brando Firm
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   12 Apr 1957: 22.   and refused to let her make The Brothers Karamazov (1958) at MGM. WARNERS TO HOLD ACTRESS TO PACT: Studio Halts Deal Between Carroll Baker and M-G-M for Karamazov Movie Maria Schell Sought
By THOMAS M. PRYOR Special to The New York Times.. New York Times (1923-Current file)   03 May 1957: 20.  

Natalie Wood, also under contract to Warners, was mentioned as a possibility for the lead, Tracy Set for Ten North Frederick
The Washington Post and Times Herald (1954-1959)   10 Apr 1957: B8.  as was Anne Baxter.  Finally in August 1957 it was announced Dorothy Malone, who had recently won an Oscar for Written on the Wind would play Diana Barrymore. DOROTHY MALONE IN FILM BIOGRAPHY: Oscar Winner Is Cast as Diana Barrymore--Paul Douglas Gets New Role Giulletta Masina to Co-Star Of Local Origin Special to The New York Times.. New York Times (1923-Current file)   21 Aug 1957: 22.   Malone never met Diana Barrymore. TRAGEDIES TAKE TOLL: Bad Girl Dorothy Wants Comedy Role Dorothy Malone Yearns for Change-of-Pace Film Roles
Scott, John L. Los Angeles Times (1923-Current File)   20 Apr 1958: E1.   (She was invited to the set but declined. Too Soon Star Lauded
Los Angeles Times (1923-Current File)   14 May 1958: B7.  )

Gene Wesson was mentioned as auditioning for the part of John Barrymore.  Jo Van Fleet was discussed for the part of Michael Strange. Errol Wants to Make Up Again
Dorothy Kilgallen:. The Washington Post and Times Herald (1954-1959)   19 Sep 1957: C10.  

By September 1957 Errol Flynn had signed to play John Barrymore.  Errol Flynn was a friend of John Barrymores and the film was the first he had made for Warner Bros in a number of years. 

Flynn flew back into Hollywood to make the movie and was arrested only a few days later for public drunkenness, stealing an off duty policemans badge and trying to kiss a girl. Flynn denied he was drunk and was released from jail on bail after an hour. 
===Shooting===
Warner Bros recreated John Barrymores yacht and house for the film. A Hollywood mansion that used to be owned by Madge Kennedy and Pola Negri was hired for the latter. HOLDEN TO REVIVE PRODUCTION UNIT: Star Will Reactivate Toluca Films With Two Stories-- Wilde to Do Maracaibo Cornel Wilde Active
Special to The New York Times.. New York Times (1923-Current file)   04 Sep 1957: 41.  

A number of characters in the movie were fictionalised due to legal reasons - for instance first husband Bramwell Fletcher was turned into "Vincent Bryant". Tony Thomas, Rudy Behlmer & Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 216  Real names were used for her last two husbands, despite their unsympathetic portrayls - John Howard had been arrested on white slavery charges   and Robert Wilcox was dead. Howard later became a car salesman and threatened to sue Warner Bros. 

Ray Danton, who played Howard, a tennis professional, received tennis coaching from Tony Trabert. Peyton Place Thoroughly Dissects Small-Town Life
Schallert, Edwin. Los Angeles Times (1923-Current File)   13 Dec 1957: B16.  

==Reception==
===Critical===
The New York Times said the film was "not bad, just ineffectual... undaring and even unsurprising. Gone is most of the endless soiled linen that aggressively flapped through Miss Barrymores best-selling autobiography - and, with it, its left wallops, perhaps the books only real substance... Mr Flynn steals the picture lock, stock and keg. It is only in the scenes of his savage disintegration, as the horrified girl looks on, that the picture approaches real tragedy." Diana Barrymores Story at 2 Theatres
Thompson, Howard. New York Times (1923-Current file)   10 May 1958: 19.  

The Los Angeles Times called the film a "depressing affair, one that never should have been considered... it doesnt stick to the facts... it is not good storytelling, either in structural form or characterisation... For all his capturing of Johns surface mannerisms, some of the physical appearance and, most effortlessly, his way with a bottle, Flynn is not the great profile and great actor of our time. I resented him in the part." Barrymore Biography Ill Advised: Barrymore
Scheuer, Philip K. Los Angeles Times (1923-Current File)   16 May 1958: A8.  

The Washington Post called it "a sorry film" in which Errol Flynns performance "may seem to have at least dazzling vitality , but its about as dishonest a portrait of the volatile actor as youre likely to find." So Soon, So Little!
By Richard L. Coe. The Washington Post and Times Herald (1954-1959)   16 May 1958: B6.  

The Chicago Daily Tribune called it "a sordid, unattractive tale, poorly written and badly acted." Film About Barrymore Deadly Dull: "TOO MUCH, TOO SOON"
TINEE, MAE. Chicago Daily Tribune (1923-1963)   10 June 1958: a6.  

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 