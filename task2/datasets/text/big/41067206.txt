Medora (film)
{{Infobox film
| name           = Medora
| image          = Medora-film-poster.jpg
| caption        = Theatrical release poster
| director       = Andrew Cohn and Davy Rothbart
| producer       = Rachel Dengiz, Davy Rothbart, Andrew Cohn, Rachael Counce
| writer         = 
| starring       = 
| music          = Bobby Emmett   Patrick Keeler
| cinematography = Rachael Counce
| editing        = Vanessa Roworth, Andrew Cohn, Mary Manhardt
| studio         = 
| distributor    = Beachside Films, Olive Productions, Seven34 Films
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Medora is a 2013 documentary film by Andrew Cohn and Davy Rothbart about a small town basketball team based in Medora, Indiana, called the Medora Hornets, the towns 70-students high school basketball team which is on a losing streak of many seasons, just unable to keep up with larger schools in the area league, much as Medora itself struggles to barely survive.

The directors were initially interested in the team after an article in The New York Times about the team. For the full season 2010-2011, they took more than 600 hours of footage about the team and the lives and struggles of its team members.    The post-production costs were financed through the crowdfunding site Kickstarter.  The 82-minute resulting documentary has been released in November 2013.

==Festivals==
Medora had its premiere during the South by Southwest Film Festival in March 2013. It was also an official selection during the Full Frame Documentary Film Festival in 2013, and played over 20 other festivals worldwide throughout the year.

Critics agree that Medora is not just another basketball documentary. Variety (magazine)|Variety magazine said that the filmmakers Andrew Cohn and Davy Rothbart "deliver a bleakly potent portrait of life in an economically devastated Middle American town." The film also received glowing reviews in The New York Times, the Village Voice, Entertainment Weekly, and Time, among others, and currently scores a 92% "Fresh" rating on film website Rotten Tomatoes.

==Cast==
;Basketball players
*Dylan McSoley
*Rusty Rogers
*Robby Armstrong
*Chaz Cowles
*Logan Smith
*Corey Hansen
*Logan Farmer

;Others (including coaches)
*Justin Gilbert (Head Varsity coach)
*Rudie Crane (Junior Varsity Coach/ Varsity Assistant)
*Dennis Pace (athletic director)
*Josh Deering
*Hannah Elkins (cheerleader)
*Monika "Mo" Bevers (cheerleader)
*Denny Temple (cheerleading coach)
*Dr. John Reed (Superintendent)
*Mr. Brad McCammon (Principal)

==Soundtrack==
{{Infobox album  
| Name        = Medora, Original Sountrack from the Motion Picture
| Type        = Soundtrack
| Artist      = Bobby Emmett and Patrick Keeler
| Cover       = 
| Released    = November 19, 2013 (Digital Download)
| Recorded    =
| Genre       = 
| Length      =
| Label       = ABCKO Records
| Producer    =
| Reviews     = 
}}
Medora, Original Soundtrack from the Motion Picture is being released by ABCKO Records on November 19, 2013.  The album contains 18 tracks, including 10 original score tracks by Bobby Emmett and Patrick Keeler.

The album features the films original music composed by Bobby Emmett, as well as tracks by Joe Lapaglia, Dabrye, Kadence, Harlan, Chris Bathgate and The Press Delete.

;Tracklist
#"Consolidated Blues" (1:07)
#"Shadow of Love" (2:27)
#"Old Medora" (2:43)
#"Rusty Kicked Out" (3:14)
#"Rusty Picks Up Mom" (3:18)
#"Never Take Me Alive" (1:50)
#"Shaking Ghosts" – Joe Lapaglia (4:40)
#"Encoded Flow" – Dabrye feat. Kadence (2:53)
#"Moment to Myself" – Harlan (2:52)
#"The Real World" – Chris Bathgate (3:59)
#"Almost Home" – Joe Lapaglia (4:20)
#"Ride to Columbus" (2:55)
#"Columbus Game (2:36)
#"The Firetruck" (2:17)
#"Strawflowers Waltz" – Chris Bathgate (4:55)
#"Small Town" – The Press Delete (2:39)
#"Crothersville Game" (2:26)
#"Lighthouse Game" (1:51)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 