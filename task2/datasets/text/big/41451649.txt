Ilavarasan
{{Infobox film
| name           = Ilavarasan
| image          = 
| image_size     =
| caption        = 
| director       = Senthilnathan
| producer       = V. Thamilazhagan V. Selvam V. Thangaraj
| writer         = Erode Soundar  (dialogues) 
| story          = Radha Veerannan
| screenplay     = Senthilnathan
| starring       =   Deva
| cinematography = M. Kesavan
| editing        = K. R. Krishnan
| distributor    =
| studio         = Sathya Movies
| released       =  
| runtime        = 105 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil drama Sukanya in Deva and was released on 15 January 1992. 

==Plot==

Mirasu (Senthilnathan) killed his elder brother Selvanayagam (Nizhalgal Ravi) and Selvanayagams wife (C. R. Saraswathi), he tried to kill his mistress Lakshmi (Kavitha) for the inheritance. The innoncent Lakshmi was then sent in jail for Selvanayagams murder and gave birth in jail, with the help of a police officer, she escaped from jail with her son Vijay.
 Sarath Kumar) saves them. Vijay, who comes from another village, introduces himself as an orphan. Poongodu falls in love with Vijay and he gets married with her. Finally, Vijay clashes with Mirasu. What transpires later forms the crux of the story.

==Cast==

*R. Sarathkumar as Vijay Sukanya as Poongodu
*Goundamani
*Kavitha as Lakshmi
*Senthilnathan as Mirasu
*Senthamarai
*Venniradai Moorthy as Kanakku
*Nizhalgal Ravi as Selvanayagam
*C. R. Saraswathi as Selvanayagams wife
*Swamikannu
*Bayilvan Ranganathan
*Ashwani Kumar
*Vadivelu
*Paal
*M. Rajkumar
*Kokila as Kannatta
*Babitha
*V. R. Thilakam
*Vasantha
*Sharmili
*Vasanthi
*Suseela Patti
*Baby Pushpa
*V. Thamilazhagan

==Soundtrack==

{{Infobox Album |  
| Name        = Ilavarasan
| Type        = soundtrack Deva
| Cover       = 
| Released    = 1992
| Recorded    = 1991 Feature film soundtrack |
| Length      = 23:45
| Label       =  Deva
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Deva (music director)|Deva. The soundtrack, released in 1992, features 5 tracks with lyrics written by Muthulingam, Na. Kamarasan, Piraisoodan and Kadhalmathi.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Aaniponne Asaimuthe || K. S. Chithra || 5:07
|- 2 || Kaatuparavaigal || S. P. Balasubrahmanyam, K. S. Chithra || 5:04
|- 3 || Kangalilthoodhuvidu || S. P. Balasubrahmanyam, K. S. Chithra || 4:24
|- 4 || Thaaye Nee || K. S. Chithra || 4:26
|- 5 || Yaaro Neeyaaro ||  K. J. Yesudas || 4:44
|}

==References==
 

 
 
 
 
 