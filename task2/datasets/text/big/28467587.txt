The Deer Hunt
{{Infobox film name           = The Deer Hunt image          = image size     = alt            = caption        = director       = Fadil Hadžić producer       = writer         = Fadil Hadžić narrator       = starring       = Boris Dvornik  Silvana Armenulić  Ivo Serdar  Miha Baloh music          = Nikica Kalogjera cinematography = editing        = studio         = distributor    = released       = 10 January 1972 runtime        = country        = Yugoslavia language       = Serbo-Croatian budget         = gross          =
}} Yugoslav film directed by Fadil Hadžić. It was released in 1972.

==Cast==
*Boris Dvornik - Konobar Zeljo
*Silvana Armenulić - Pjevacica Seka
*Ivo Serdar - Recepcionar
*Aleksander Krošl (as Sandi Krošl) - Ivan Susnjar
*Miha Baloh - Nacelnik milicije
*Franjo Majetić - Brico
*Mate Ergović - Joza Vikulic
*Fabijan Šovagović - Zdravko
*Sanda Langerholz - Susnjareva sestra
*Zvonko Lepetić - Isljednik Andrija
*Adem Čejvan - Kosta
*Relja Bašić - Advokat Janjic
*Ilija Ivezić - Provokator
*Tonko Lonza - Doktor
*Ljubo Kapor - Uhapsenik
*Ivo Fici
*Franjo Fruk - Zeljeznicar
*Mirko Svec - Cinovnik u banci
*Vinko Lisjak - Gospon Maresic
*Marija Aleksić - Sankerica Marica
*Velimir Keković - Milicajac
*Marija Geml - Nacelnikova tajnica
*Jagoda Kralj - Vikuliceva kcer
*Dane Georgijevski
*Tomislav Lipljin - Gost u restoranu
*Dobrila Biser - Recepcionareva zenska
*Ante Kraljević
*Ljudevit Gerovac - Stranka kod brijaca
*Ivan Lovriček
*Zvonko Zungul
*Lena Politeo - Stranka kod odvjetnika

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 