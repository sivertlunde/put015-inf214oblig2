El Pisito
 
{{Infobox film
| name           = El Pisito
| image          = El-pisito.jpg
| image_size     =
| caption        = Spanish theatrical release poster
| director       = Marco Ferreri Isidoro M. Ferry
| producer       = Eduardo Manzanos Brochero Alberto Soifer
| writer         = Marco Ferreri Rafael Azcona
| narrator       =
| starring       = Mary Carrillo José Luis López Vázquez Concha López Silva Ángel Álvarez María Luisa Ponte
| music          = Federico Contreras
| cinematography = Francisco Sempere
| editing        = José Antonio Rojo
| distributor    =
| released       = June 15, 1959
| runtime        = 87 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}
 Spanish comedy film directed by Marco Ferreri. The Spanish Ministry of Culture forced the producers to sign the film as co-directed by Spaniard Isidoro M. Ferry. It was co-written by famous Spanish screenwriter Rafael Azcona, who collaborated with Ferreri throughout his career.

The film was a huge flop when it was released in Spain, but nowadays is a cult classic. The film had troubles with Spanish censorship.

In the film, Marco Ferreri and Carlos Saura appear in cameos.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 