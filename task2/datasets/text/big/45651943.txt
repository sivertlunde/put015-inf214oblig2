The Heart (1955 film)
{{Infobox film
| name     = The Heart
| image    =
| director = Kon Ichikawa
| producer = Masayuki Takagi
| writer   = Keiji Hasebe Katsuhito Inomata Soseki Natsume  (novel)  Masayuki Mori   Michiyo Aratama
| cinematography = 
| music    = 
| studio = Nikkatsu
| country  = Japan
| language = Japanese
| runtime  = 120 min
| released =  
}}
The Heart is a 1955 Japanese drama film directed by Kon Ichikawa. It is based on the novel Kokoro by Natsume Sōseki.

== Cast == Masayuki Mori - Nobuchi
* Michiyo Aratama - Nobuchis wife
* Tatsuya Mihashi - Kaji
* Shōji Yasui - Hioki
* Tanie Kitabayashi - Hiokis mother
* Akiko Tamura - Widow
* Mutsuhiko Tsurumaru - Hiokis father
* Tsutomu Shimomoto - Hiokis elder brother
* Masami Shimojō - Broker
* Akira Hisamatsu - Travelling monk
* Tomoko Naraoka - Kume
* Zenji Yamada - Nobuchis uncle
* Keiji Itami - Kajis father
* Kiyoshi Kamoda - Doctor

== External links ==
* 

 
 
 
 
 

 