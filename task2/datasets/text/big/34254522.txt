Vijetha Vikram
 
 
{{Infobox film
| name           = Vijetha Vikram 
| image          =
| caption        =
| writer         = Satyanand  
| screenplay     = S.S. Ravichandra
| producer       = T. Tirupathi Reddy
| director       = S.S. Ravichandra Venkatesh  Farah Chakravarthy
| cinematography = Mahindar
| editing        = Kotagiri Venkateswara Rao
| studio         = Samyutha Arts
| distributor    = 
| released       =  
| runtime        = 2:24:52 
| country        =   India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood  Venkatesh and Farah played the lead roles and music composed by K. Chakravarthy|Chakravarthy.
  The film was flop at the box-office. 

==Plot==
Rudra Bhupathi (Rao Gopal Rao) a dictator of an estate, treats his entire villagers as slaves. Vikram (Daggubati Venkatesh|Venkatesh) a young and energetic guy enters to the estate who always faces against Rudra Bhupathis wickedness and gets closer to villagers. Usha (Farah (actress)|Farah) Rudra Bhupathis only daughter also likes Vikrams attitude and both of them fall in love. A mad woman Bharathi (Sumithra (actress)|Sumitra) roams all over the estate without recognizing herself, one day Vikram comes to know that mad woman is his mother and there is some suspicious link between her madness and Rudra Bhupathi. Meanwhile Shishupal (Betha Sudhakar|Sudhakar) grandson of adjacent estate owner comes from foreign and he wants to marry Usha. Shishupal wants to remove Vikram from his way, so he plans an attack on Vikram in that quarrel Bharathi is injured and gets her memory back. Vikram asks his mother actually what happened then she reveals their past.

Vikrams father Pratap Rao (Ranganath (actor)|Ranganath) was a forest officer who always comes in between to the Rudra Bhupathis illegal activities in the estate, that why he killed him and also tried to kill Bharathi and Vikram in that attack Bharathi is injured keeping Vikram in safe-zone, but she lost her memory. Listening to all this now Vikram decides to take revenge against Rudra Bhupathi. At the same time Shishupal rapes a innocent village girl Gowri (Poornima (Telugu actress)|Poornima) and keeps the blame an Vikram, every one believes it including Usha becauce Gowri lost her conscious and not able to recognize the person who raped her. Usha gets ready to marry Shishupal, at the time marriage Rudra Bhupathi comes to know that Shishipal double crossed him, who is cheat and not the original grandson of estate owner. Simultaneously Gowri comes to conscious ans reveals the entire truth and sacrifices her life to protect Vikram. Shishupal & his men attacks on Rudra Bhupathi & villagers and kidnaps Usha, Vikram protects her and sees end of Shishupal even Rudra Bhupathi admits his mistake and movie ends with marriage of Vikram & Usha.

==Cast==
  Venkatesh as Vikram Farah as Usha
*Rao Gopal Rao as Rudra Bhupathi
*Nutan Prasad as Bangaruraju Sudhakar as Shishupal
*Suthi Veerabhadra Rao as S.P. Pithal Rallapalli as Bhajagovindam Ranganath as Forest Officer Pratap Rao
*Vankayala Satyanarayana as Vikrams grandfather
*Chidatala Appa Rao as Villager Poornima as Gowri Sumitra as Bharathi
*Y.Vijaya as Bangari
 

==Soundtrack==
{{Infobox album
| Name        = Vijetha Vikram
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 22:28
| Label       = Lahari Music Chakravarthy
| Reviews     =
| Last album  = Bharatamlo Arjunudu   (1987) 
| This album  = Vijetha Vikram   (1987)
| Next album  = Muddai   (1987)
}}
Music composed by K. Chakravarthy|Chakravarthy. Music released on Lahari Music Company. 
{|class="wikitable"
|-
!S.No!!Song Title !! Singers !! lyrics !!length
|- 1
|Premalo Paddavuga SP Balu, P. Susheela Veturi Sundararama Murthy
|4:34
|- 2
|Ettu Ettu Ettu SP Balu,P. Susheela Veturi Sundararama Murthy
|4:43
|- 3
|Kasipatnam Chooddamante SP Balu,P. Susheela Veturi Sundararama Murthy
|4:42
|- 4
|Look At Me SP Balu, S. Janaki Veturi Sundararama Murthy
|4:32
|- 5
|Gorinta Poddullo SP Balu,P. Susheela Vennelakanti
|3:57
|}

== References ==
 

 
 
 
 


 