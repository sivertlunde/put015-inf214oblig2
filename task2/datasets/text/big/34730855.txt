Apio Verde
 
{{Infobox film
| name           = Apio verde
| image          =
| caption        =
| director       = Francesc Morales
| producer       = Francesc Morales Camilo Klein Catherine Mazoyer Jorge Gaete
| writer         = Francesc Morales
| starring       = Catherine Mazoyer Cristian Gajardo Catalina Aguayo Sonia Mena Teresita Reyes Alejandro Trejo Gregory Cohen Jenny Cavallo Carolina Paulsen
| music          = 
| cinematography = Alex Leyton
| editing        = Francesc Morales
| studio         = Klein Producciones Efecto Moral Films
| distributor    = 
| released       =  
| runtime        = 82 minutes
| country        = Chile
| language       = Spanish
| budget         =
| gross         =
}}
Apio verde is 2013 Chilean thriller film written and directed by Francesc Morales, starring Catherine Mazoyer, Cristian Gajardo, Catalina Aguayo, Sonia Mena, Teresita Reyes, Alejandro Trejo, Gregory Cohen, Jenny Cavallo, Carolina Paulsen, among others. The film focuses on how a normal woman is turned into a psychotic as shes told the child shes expecting has anencephaly, a disease that wont allow him to live once hes born. As she lives in Chile, one of the few countries where therapeutic abortion is illegal in any situation, she must wait until the natural birth. The film was released in Chile in July 2013.

Morales said recently to   would be considered illegal, not even when the life of the mother is in danger. The realism is very important in this film, which is why the story is based on the lives of many real people and it also shows how politics and religion get involved in a subject like this. Yet the movie is not a drama, but a thriller that states how this is not only an attempt on human rights but also a situation that always leads to a violent resolution.”

==Plot==
A couple is told their child has a disease that wont allow him to live outside the mothers womb. It wont matter how much physical or mental pain they feel; as therapeutic abortion is illegal on Chile, theyll have to wait until the natural birth.

==External links==
*  
*   at Screen International
*   at Fangoria
*   at Twitch Film
*   at Shock Till You Drop


== See Also ==
* Cinema of Chile


 
 
 
 
 
 
 
 