Markandeya (1935 film)
{{Infobox film|
  name           = Markandeya|
  image          = Markandeya1935.jpg |
  image_size = 200px |
  caption        = |
  director       = K. Ramnoth, Murugadasa|
  writer         = |
  starring       = Master V. N. Sundaram,  Rajapalayam Kuzhandaivelu Bhagavathar,  K. B. Srinivasan,  ‘Lady Bhagavathar M. S. Kanna Bai, S. N. Kannamani  |
  producer       =  Vel Pictures|
  distributor    = |
  music          = |
  cinematography = K. Ramnoth|
  editing = |
  released   = 1935|
  runtime        = | Tamil }}

Markandeya was a 1935 Tamil film directed by K. Ramnoth and Murugadasa. The film is based on the mythological story of Markandeya.

==Plot==

Mrikandu rishi and his wife Marudmati worshipped Shiva and sought from him the boon of begetting a son. As a result he was given the choice of either a gifted son, but with a short life on earth or a child of low intelligence but with a long life. Mrikandu rishi chose the former, and was blessed with Markandeya, an exemplary son, destined to die at the age of 16.

Markandeya grew up to be a great devotee of Shiva and on the day of his destined death he continued his worship of Shiva in his aniconic form of Shivalingam. The messengers of Yama, the god of death were unable to take away his life because of his great devotion and continual worship of Shiva. Yama then came in person to take away Markandeyas life, and sprung his noose around the young sages neck. By accident of fate the noose mistakenly landed around the Shivalingam, and out of it, Shiva emerged in all his fury attacking Yama for his act of aggression. After defeating Yama in battle to the point of death, Shiva then revived him, under the condition that the devout youth would live forever.

==Production==

The film was produced by Vel Pictures of Madras owned by M. T. Rajen. Markandeya was the first Tamil film produced by him. It had a total of 36 songs. The film was shot at Hindu pilgrimage sites of Thiruvannamalai, Chidambaram and Kanchipuram and got considerable acclaim for its cinematography. The film fared well at the box office.

==Cast==

V. N. Sundaram (who later became a playback singer) played the title role of Markandeya while Kuzhandaivelu Bhagavathar played the sage Mrikanduand Kanna Bai played Markandeyas mother. Stage actor Kannamani played the role of a gypsy girl.

==References==

*  

 
 
 
 
 


 