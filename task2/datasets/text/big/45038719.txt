Thayigintha Devarilla
{{Infobox film 
| name           = Thayigintha Devarilla
| image          =  
| caption        = 
| director       = Y. R. Swamy
| producer       = Lakshmi Vajramuni
| writer         = K. S. R. Murthy
| screenplay     = M. D. Sundar Jayanthi Manjula Manjula Lokanath
| music          = Rajan-Nagendra
| cinematography = S S Lal
| editing        = P Bhakthavathsalam
| studio         = Vajrameshwara Pictures
| distributor    = Vajrameshwara Pictures
| released       =  
| country        = India Kannada
}}
 1977 Cinema Indian Kannada Kannada film, Manjula and Lokanath in lead roles. The film had musical score by Rajan-Nagendra. 

==Cast==
 
*Srinath in Special Appearance Jayanthi
*Manjula Manjula
*Lokanath
*Jayakumar
*Seetharam
*Joker Shyam
*B. S. Narayana Rao
*Bhadrachalam
*Bhatti Mahadevappa
*Hanumanthachar
*Vajramuni
*Somu
*N. S. Rao
*Balachandra Pai
*B. H. Manohar
*Rangegowda
*Siddalingappa
*Srinivas
*Kunigal Ramanath
*S. K. Ramu
*Halasingachar
*Raghavendra Raju
*B. Jayashree
*Pramila Joshai
*Shanthamma
*Vani Chandra
*Pramila
*Udayashree
*Swapna
*Baby Rekha
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || Chi. Udaya Shankar || 03.22
|- Janaki || Chi. Udaya Shankar || 03.20
|-
| 3 || Nee Thanda Bhaagya || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar || 03.22
|}

==References==
 

==External links==
*  

 

 
 
 
 


 