The Adventures of the Elektronic
 
{{Infobox film
| name           = The Adventures of the Elektronik
| image          =
| image_size     =
| caption        =
| director       = Konstantin Bromberg
| producer       =
| writer         = Yevgeni Veltistov
| narrator       =
| starring       =
| music          = Yevgeni Krylatov
| cinematography =
| editing        =
| studio         = Gosteleradio Odessa Film Studio
| distributor    =
| released       = 1979
| runtime        = 215 min
| country        = Soviet Union
| language       = Russian
| budget         =
| gross          =
}}
  Soviet adventure adventure TV miniseries directed by Konstantin Bromberg.

Novel and screenplay by Yevgeni Veltistov. His fiction novels "Elektronic - the boy from the suitcase" (1964) and "Ressi - an elusive friend" (1971) were adapted into three film series by Bromberg. The TV premiere was on 2 May 1980. The film achieved a cult status among the Soviet kids.

== Plot summary ==
A robot named Elektronic escapes from Professor Gromovs laboratory. The robot looks exactly like Sergey (Serezha) Syroezhkin, the boy from the magazine cover, which was chosen by Gromov as a model to construct Elektronic.

By coincidence, the double meets its prototype. 6-grader Serezha cunningly suggests that Elektronik should impersonate him - go to school instead of him and even live in his home.  His plan works, as no one can tell the difference between them.  Serezhas teachers delight in a very gifted pupil, who suddenly shows unbelievable talents in math, gymnastics, drawing and even singing. Sergeys parents do not suspect his trick and are glad of their pseudo-sons progress.

However, eventually the boy realizes that as the robot takes over "his" life, he may be out of business...

At the same time, "somewhere abroad", a gang of criminals operates. It is headed by a criminal authority known as Stump. He tells Urrie, who is the best in their gang, to find and kidnap Elektronic. They want to organize a "crime of the century" with the use of his extraordinary abilities.

== Cast ==
* Yuri Torsuyev as Syroyezhkin
* Vladimir Torsuyev as Elektronic
* Vasili Skromny as Gusev Oksana Alekseyeva as Maika
* Maksim Kalinin as Korolkov
* Dmitri Maksimov as Smirnov
* Evgeny Livshits as Ryzhikov/Chizhikov
* Valeriya Soluyan as Kukushkina
* Vladimir Basov as Stump
* Nikolai Karachentsov as Urrie
* Nikolai Grinko as Professor Gromov
* Yelizaveta Nikishchikhina as Masha, Gromovs assistant
* Yevgeny Vesnik as Tarator, the math teacher
* Maya Bulgakova as Schools headmistress
* Nikolai Boyarsky as Rostik, the gymnastics teacher
* Roza Makagonova, the singing lessons teacher
* Nataliya Vasazhenko, Syroyezhkins mother
* Yuri Chernov, Syroyezhkins father
* Lev Perfilov as Lyug, Stumps gangster
* Gennadi Yalovich as Bree, Stumps gangster

==Production==
 
Originally, it was planned that both Syroyezhkin and Elektronic roles would be played by just one boy. But the director of the film decided to simplify the filming process by using twins.  His assistants screened a hundred twins throughout most of the Soviet Union. One of the casting days took place in the winter, when the temperature was below zero, nobody came except for the Torsuyev brothers. It was they who were cast in leading roles. In test filmings, Yuri played the role of Elektronic and Vladimir of Syroyezhkin, but the roles were later changed by the director. During the filming period the kids grew up too fast, and it was necessary to make new costumes from time to time.

===Music===
Music for the film were written by Yevgeni Krylatov. The songs were performed in the film by Yelena Kamburova, Yelena Shuenkova  and the chorus; no actors, except for Karachentsov and Basov, were afforded an opportunity to sing (The lyrics were written by Yuri Entin).

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 