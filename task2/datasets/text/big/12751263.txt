A Lawyer Walks into a Bar
{{Infobox film
| name           = A Lawyer Walks Into a Bar...
| image          = A Lawyer Walks into a Bar.jpg
| image_size     =
| caption        = 
| director       = Eric Chaikin
| producer = Tasha Oldham Eric Chaikin Robert Shapiro Nancy Grace Len Jacoby Eddie Griffin Michael Ian Black Sam Garrett Tricia Zunker Donald Baumeister Magda Madrigal Megan Meadows Cassandra Hooks Chad Troutwine
| music          = Daniel Fickle
| cinematography = Stephanie Martin
| editing        = Deborah Barkow
| distributor    = Camels Back Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
 the California Bar exam, and the obsession America has with its legal system. The film has garnered many positive reviews including coverage from The Wall Street Journal, The Dallas Morning News, Premiere (magazine)|Premiere.com and on nationally syndicated television show At the Movies with Ebert & Roeper.   February 23, 2007    

== Film content ==

This film highlights the trials and tribulations of trying to pass the State Bar of California exam and follows six wanna-be lawyers struggling to prepare and actually pass the test with the lowest national pass rate.  One of the test takers in the documentary is Donald Baumeister, an ex-Marine, who had failed the California bar 41 times and is shown in the movie gearing up for attempt #42. A working father, Baumeister never had the time to properly study for the exam. 

This movie portrays a common perception of American life – that, in the time of trouble, whether it be civil or criminal, Americans depend on lawyers to navigate the tough waters of today’s judicial system. 

===Festival accolades===

A Lawyer Walks Into a Bar... has won awards at many independent film festivals including the Grand Jury Prize at AFI Dallass Texas Competition, and Boulder Colorados Toofy Fests Best Narrative Feature.  

===A focus on minorities===

A Lawyer Walks Into a Bar... is a movie that not only portrays the fight of the average law school student, but also of the minority and not so fortunate law student trying to pass and be admitted to a major state bar.  The movie follows Magda Madrigal as she attends Peoples College of Law so that she can take the bar and be an advocate for people in her local Hispanic community. 

===Cast of characters===
Meagan Meadows and Cassandra Hooks are shown passing the July 2006 Bar examination in the movie. http://members.calbar.ca.gov/search/member_detail.aspx?x=245033   Unfortunately for the rest of those followed for the July 2006 Bar Exam, Donald Baumeister failed the bar for the 42nd time and Sam Garrett, who failed it for the third time. Magda Madrigal did not take the July 2006 exam because the State Bar determined that she had not taken enough units at Peoples College of Law, an unaccredited law school.

The movie does follow-up with the subsequent exam and shares that Tricia Zunker successfully passed the February 2007 California Bar,  but neither Donald Baumeister, Sam Garrett nor Madga Madrigal passed at that time.

While not shown in the movie, the California Bar shows that Ms. Madrigal did pass the July 2009 Bar Exam. http://members.calbar.ca.gov/fal/Member/Detail/264637  Sam Garrett passed the New Jersey Bar Exam in May 2009 and the California Bar Exam in February 2011. He was admitted to the California Bar on October 27, 2011. 

===Where are they now?===
Mr. Baumeister is an Educational Consultant at Integrative Medical Institute of Orange County as well as an instructor at California State University, Los Angeles and Chapman University. 

Mr. Garrett is vice-president of Bienvenidos Immigration Services in Los Angeles.  

Ms. Hooks is a senior associate at the law firm of Alston & Bird in Los Angeles. 

Ms. Madrigal is an attorney at the Eviction Defense Network in Los Angeles. 

Ms. Meadows currently works at the PR firm of Gallagher & Gallagher in Los Angeles. 

Ms. Zunker is in solo practice with the Zunker Law Group in Santa Monica. 

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 