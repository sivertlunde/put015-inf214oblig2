Jurassic Shark
 

{{Infobox film
| name           = Attack of the Jurassic Shark  (aka: Jurassic Shark) 
| image          = Jurassic Shark DVD cover.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD cover
| film name      = 
| director       = Brett Kelly
| producer       = Anne-Marie Frigon
| writer         = David A. Lloyd Trevor Payer
| starring       =  
| music          = Christopher Nickel
| cinematography = Amber Peters
| editing        = 
| studio         =  
| distributor    = Brett Kelly Entertainment
| released       =  
| runtime        = 75 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Attack of the Jurassic Shark (also called Jurassic Shark) is an 2012 Canadian independent action horror film project directed by Brett Kelly.    

==Plot==
A megalodon (prehistoric shark) is accidentally awakened by an oil companys explorations, trapping a group of art thieves and a group of young female college students on an abandoned island in the middle of a lake.  The two groups band together to fight off the monsters attacks and fall victim to the shark one by one until only two of the girls are left, who manage to defeat the shark. However, another shark is revealed to be alive as it kills two fisherman who are nearby.

==Cast==
* Emanuelle Carriere as Jill
* Christine Emes as Tia
* Celine Filion as Kristen
* Angela Parent as Barb
* Duncan Milloy as Rich
* Phil Dukarsky as Doug
* Kyle Martellacci as Mike
* Joshua Gilbert Crosby as Jack
* Kevin Preece as Jerry
* Jurgen Vollrath as Dr. Lincoln Grant
* George Hudson as Luke
* Kala Gray as Brittany
* Sherry Thurig as Scientist
* Jody Haucke as Chairman
* Darren Stevens as Fisherman 1
* Ian Quick as Fisherman 2
* Kimberly Wolfe as Beer Girl

==Reception== VOD or Jurassic Park Megalodon feature, Attack of the Jurassic Shark". He offered "when I say this movie is one of the worst pieces of cinema that my eyes had the misfortune of viewing, I truly mean it."  He explained "I love terrible horror movies, I think they’re hilarious and while this one had a few of those moments, it couldnt live up to its potential." He noted that viewer expectations would be subverted by only "average girls in bikinis, terrible prop work, awful CG (for the most part), pathetic death sequences, a flying shark and a really long, dialogue-free walking montage." 

Dread Central spoke negatively about productions first efforts at a film trailer (promotion)|trailer, calling it "underwhelming" as "a trailer for a killer shark movie with very little shark in it". Comparing the early trailer to the later, they expanded by writing "the differences between the two trailers should be a lesson to indie filmmakers not to release the first trailer for their movie to the public before they’re really ready to show off their movie goods."    

JoBlo spoke somewhat more positively about the later trailer which included footage of the purported megalodon and predicted the film "has potential to become something special."     After DVD release, they found the film and premise to be "ridiculous shit". 
 CGI effects making its shark look cartoon#Animation|cartoonish.  They expanded it was "Amazing how bad the effects are here," and that the film was "an exercise in how not to make a low budget flick".   

Julian Thome of Zelluloid described the movie as "a disappointment itself", while mainly criticizing the acting, the story and the effects. He also mentioned the film is too long for its 70 minutes.   

==References==
 

==External links==
*   at the Internet Movie Database
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 