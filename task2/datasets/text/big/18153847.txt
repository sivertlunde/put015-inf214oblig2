Charley and the Angel
 
{{Infobox Film
| name           = Charley and the Angel
| image          = Charley and the Angel.jpg
| caption        = 
| director       = Vincent McEveety
| producer       = Bill Anderson
| writer         = Will Stanton (book author) Roswell Rogers (film writer) Kathleen Cody Kurt Russell
| music          = Buddy Baker
| cinematography = Charles F. Wheeler
| editing        = Bob Bring Ray de Leuw Walt Disney Productions Buena Vista Distribution
| released       = March 23, 1973
| runtime        = 93 min.
| country        = United States
| awards         = Golden Globe (nomination)
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Disney Family comedy film Will Stanton.

==Plot== Chicago Worlds Kathleen Cody) elope with a young man named Ray (Kurt Russell), who seems untrustworthy.

Charley is visited by a shabby-looking angel (Harry Morgan) who appears visible only to him.  The angel tells Charley that his time will soon be up, and the shopkeeper decides to become religious, patch relations with his family, sell his business, and do the best he can to be a good father and husband before he dies.  Charleys angel appears intermittently throughout the film, occasionally helping Charley, and occasionally causing mischief.  The angel reveals his name as Roy Zerney.
 insane whenever he speaks to, or looks for, the lingering angel who is visible only to him. When Charley tries to take money from his account in the bank, he learns from the banker Ernie (Edward Andrews) that the bank will be closing for a while and may be in danger of foreclosure.  He must loan money to son-in-law Ray, and to his friend Pete (George Lindsey).  Business tightens, and Charley is running out of time and money.
 Model T, booze by Felixs request, and they are kidnapped and forced to drive away when the Chicago gangsters responsible for the operation are trying to flee the city.  Charley personally chases them in the abandoned gangsters car, dodging gunfire, and the police catch him presuming he is the criminal.  While in prison, Roy tells Charley that today will probably be his last day on Earth.  However, Charleys thoughts are still of his boys.

When he returns home in the evening, Leonora and Ray return for an untimely visit just as the gangsters occupy his house and intend to take Charleys wife as another hostage.  Charley defies them and defends his wife and kids with his own life.  The fight ends when Charley and Ray, with the assistance of a timely appearance by Pete, succeed in defeating the gangsters and delivering them to the police.  In the course of the fight, Charley was shot at point-blank range but miraculously receives no wound.
 bank examiner has approved the banks credibility and that it will be reopening tomorrow.  Pete has also returned to repay his debt.

Charley, satisfied with the turn of events in his final day, says goodbye to his family and expects that he will still die, but Roy appears and reveals that the eleventh-hour decision in Heaven was to let Charley live.  Roy physically intervened and pulled the bullet from the air, thus nullifying the prophecy and clarifying to Charley that he will live on, with an enriched outlook.

==Cast==
* Fred MacMurray: Charley Appleby
* Harry Morgan: The Angel (Roy Zerney)
* Cloris Leachman: Nettie Appleby Kathleen Cody: Leonora Appleby
* Scott Kolden: Rupert Appleby
* Vincent Van Patten: Willie Appleby
* Kurt Russell: Ray Ferris
* George Lindsey: Pete
* Edward Andrews: Ernie
* Mills Watson: Frankie Zuto
* Richard Bakalyan: Buggs
* Barbara Nichols: Sadie
* Kelly Thordson: Policeman
* Liam Dunn: Dr. Sprague
* Larry D. Mann: Felix
* George OHanlon: Police Chief

==Reception==
Actress Cloris Leachman was nominated for a Golden Globe in the category of Best Actress in Musical/Comedy.  The film was re-edited for television on the Walt Disney anthology television series in 1977.

==External links==
*  
*  
*  
*  
*  
*  at Rotten Tomatoes
*  brief description

 

 
 
 
 
 
 
 
 