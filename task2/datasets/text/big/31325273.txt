Unraveled (film)
 
{{Infobox film
| name           = Unraveled
| image          = 
| caption        =
| director       = Marc H. Simon
| producer       = Miranda Bailey Steven Cantor Terry Clark Daniel Laikind Matthew Makar Marc H. Simon
| writer         = Marc Dreier
| music          = Chris Hajian
| cinematography =
| editing        = Christina Burchard Alyse Ardell Spiegel
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Marc Dreier, who was arrested for orchestrating a massive fraud scheme that netted hundreds of millions of dollars from hedge funds.  Set during his house arrest, the film recounts Driers struggle to prepare for the possibility of life imprisonment with first-person flashbacks of his actions. 

The film also show Driers attempts to grasp his tragic unraveling. The film was  produced and directed by Marc H. Simon of Stick Figure Productions. Other producers include Matthew Makar, Steven Cantor, and Miranda Bailey.

==External links==
*  

 
 
 
 
 
 


 