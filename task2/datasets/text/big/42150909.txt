Don't Fence Me In (film)
{{Infobox film
| name = Dont Fence Me In
| image =
| image_size =
| caption = John English
| producer = Donald H. Brown   Armand Schaefer
 | writer = Dorrell McGowan   Stuart E. McGowan   John K. Butler
| narrator = Robert Livingston
| music = Mort Glickman   Morton Scott   R. Dale Butts William Bradford
| editing = Charles Craft   Richard L. Van Enger    
| studio = Republic Pictures
| distributor = Republic Pictures
| released = October 20, 1945
| runtime = 71 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Dont Fence Me In is a 1945 American western film directed by  John English (director)| John English and starring Roy Rogers, George "Gabby" Hayes and Dale Evans. The film was part of the long-running series of Roy Rogers films produced by the Hollywood studio Republic Pictures. 

==Plot== title tune, composed by Cole Porter.

==Cast==
* Roy Rogers as Himself 
* Trigger (horse) as Himself 
* George Gabby Hayes as Gabby Whittaker, aka Wildcat Kelly  
* Dale Evans as Reporter Toni Ames  Robert Livingston as Jack Chandler 
* Moroni Olsen as Henry Bennett, aka Harry Benson  
* Marc Lawrence as Clifford Anson  
* Lucile Gleason as Mrs. Prentiss  
* Andrew Tombes as Cartwright   Paul Harvey as Gov. Thomas  
* Tom London as Ben Duncan, Sheriff of Twin Wells  
* Douglas Fowley as Jack Gordon  Steve Barclay as Tracy  
* Edgar Dearing as Chief of Police  
* Bob Nolan as Bob

==References==
 

==Bibliography==
* Hurst, Richard M. Republic Studios: Beyond Poverty Row and the Majors. Scarecrow Press, 2007. 
==External links==

* 

 
 
 
 
 
 
 
 

 