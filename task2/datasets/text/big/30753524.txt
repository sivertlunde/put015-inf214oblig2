Give Us Wings
{{Infobox film
| name           = Give Us Wings
| image_size     =
| image	=	Give Us Wings FilmPoster.jpeg
| caption        =
| director       = Charles Lamont
| producer       = Ken Goldsmith (associate producer) Robert Lee Johnson (screenplay) Eliot Gibbons (story)
| narrator       =
| starring       = Billy Halop Huntz Hall Gabriel Dell Bernard Punsly Bobby Jordan Shemp Howard
| music          = Charles Previn (musical director) H.J. Salter (conductor) Frank Skinner (composer: stock music) Paul Van Loan (composer: stock music) John Boyle Frank Gross
| distributor    = Universal Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
}} Universal film starring the Dead End Kids and the Little Tough Guys.

==Plot==
The Dead End Kids work  as airplane mechanics in the National Youth Administration Work Program plant. Feeling that they have enough knowledge of planes, they feel the urge to want to become pilots. The boys are hired by crop dusting operator Arnold Carter to become pilots. Upon being hired, York (Carters manager) feels that the boys are far too inexperienced to fly, and assigns them to ground work. When Carters company falls beind in their contracts, the Dead End Kids are forced to learn the ropes of flying. Eventually, York agrees that all of the boys are ready to become pilots, except for Rap. Rap, who becomes terrified of flying after witnessing a recent plane crash, still has the urge to become a pilot in order to be on the payroll with his friends. Carter convinces Rap to take part in a dangerous crop dusting job, which winds up killing him. York, mechanic friend Buzz, and the rest of the Dead End Kids go after Carter, eventually turning him over to the authorities. In the end, York agrees to permanently hire the boys.

==Cast==
===The Dead End Kids===
*Billy Halop as Tom
*Huntz Hall as Pig
*Gabriel Dell as String
*Bernard Punsly as Ape
*Bobby Jordan as Rap

===The Little Tough Guys===
* Harris Berger - Bud
* Billy Benedict - Link

===Additional cast===
*Wallace Ford as Mr. York
*Anne Gwynne as Julie Mason
*Victor Jory as Mr. Arnold Carter
*Shemp Howard as Buzz Berger (a.k.a. Whitey)
*Milburn Stone as Tex Austin

==Production== Universal Dead End Kids/Little Tough Guys series, and signed on to Monogram Pictures to costar with fellow Dead End Kid Leo Gorcey in the East Side Kids series. Jordan would return to Universal to replace Billy Halop in Keep Em Slugging, the final Dead End Kids film for Universal.

* Though his character is referred to as "Buzz" throughout the film, the credits for this film state that Shemp Howards character name was "Whitey".

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 