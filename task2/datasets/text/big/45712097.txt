Riders of the Timberline
{{Infobox film
| name           = Riders of the Timberline
| image          = Riders of the Timberline poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = J. Benton Cheney  William Boyd Brad King Victor Jory Eleanor Stewart J. Farrell MacDonald Anna Q. Nilsson
| music          = John Leipold
| cinematography = Russell Harlan
| editing        = Fred R. Feitshans Jr. 
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 59 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, Brad King, Victor Jory, Eleanor Stewart, J. Farrell MacDonald and Anna Q. Nilsson. The film was released on September 17, 1941, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*Andy Clyde as California Carlson Brad King as Johnny Nelson
*Victor Jory as Baptiste Deschamp
*Eleanor Stewart as Elaine Kerrigan
*J. Farrell MacDonald as Jim Kerrigan
*Anna Q. Nilsson as Donna Ryan
*Tom Tyler as Henchman Bill Slade Edward Keane as Preston Yates
*Hal Taliaferro as Ed Petrie
*Mickey Eissa as Larry
*The Guardsmen Quartet as Singing Lumbermen

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 