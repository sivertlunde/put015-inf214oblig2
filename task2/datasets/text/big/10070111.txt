Shabazi (film)
 
{{Infobox Film |
  name         = Shabazi|
  image              = SHABAZIDVD.jpg|
  writer             = Yaky Yosha|
  starring           = Amos Lavie Michal Zuarets Nathan Zehavi Eyal Geffen|
  director           = Yaky Yosha|
  producer           = Dorit Yosha|
  movie_music        = |
  distributor        = Yaky Yosha Ltd. |
  released           = 1997 (Israel) |
  runtime            = 90 minutes |
  language     = Hebrew |
  budget         = |
  music          = |
  awards         = |
}} Yaky Yoshas eighth feature film. It was based on the TV drama series "Night Fare."

==Plot==
Danny lives in a one bedroom apartment in "Shabazi", a run down neighborhood crumbling over the heads of its inhabitants – dirt poor day laborers, junkies and the homeless, who find refuge in abandoned buildings. 

One rainy evening Danny almost runs over Gideon, an army comrade, who is now a homeless junkie. Gideon tries to tell Danny about some sinister scam that is going on around in the ‘hood, but hes too high, too scared and too confused.  

The next day Gideon is found murdered, but the police have no real interest in another dead junkie. Danny decides to track down the killer himself.

What appears at first to be a drug war soon reveals itself as a vast cooperation between outlaws and men of law. 
Danny manages to expose the big shots behind the scene, the scumbags who murdered his once brother in arms. 

Danny’s crusade is over; He removes his temporary armor and goes back to his one bedroom apartment in "Shabazi" – a glorious knight...

== External links ==
*  

 

 
 
 
 
 


 