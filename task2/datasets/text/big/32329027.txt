Sadie Love
{{infobox film
| name           = Sadie Love
| image          = Billie Burke Sadie Love Film Daily 1919.png
| imagesize      =
| caption        =
| director       = John S. Robertson
| based on       =  
| writer         = Clara Beranger (scenario)
| starring       = Billie Burke
| music          =
| cinematography = 
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 
| country        = United States
| language       = Silent English intertitles
}}
 1919 American silent comedy film distributed by Paramount Pictures (as Famous Players-Lasky Corporation) and directed by John S. Robertson. It is based on a 1915 stage play of the same name by Avery Hopwood  and stars Billie Burke in the title role.  In the play Marjorie Rambeau played the Burke part.   

==Anecdote==
Hedda Hopper reflecting back on the making of this film stated that Billie Burke always wanted a dressing room to herself and was reluctant to change clothing with the other female cast members. Hopper seemed to not understand(or perhaps was jealous) that Burke was the star of the film (as well as a star on Broadway) and was due the personal dressing room accorded a star. 

==Cast==
*Billie Burke - Sadie Love James Crane - Count Luigi Pallavichi
*Helen Montrose - Princess de Marabole
*Hedda Hopper - Mrs. James Wakeley
*Jed Prouty - James Wakeley
*Shaw Lovett - Mumford Crewe
*Mrs. Margaret A. Wiggin - Mrs. Warrington
*May Rogers - Celeste Charles Craig - Butler
*Ida Waterman - Aunt Julia

==References==
 

==External links==
*  
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 