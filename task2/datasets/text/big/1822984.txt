Sorrell and Son
 
{{Infobox film
| name           = Sorrell and Son
| image          =
| caption        =
| director       = Herbert Brenon Ray Lissner (assistant)
| producer       = Joseph Schenck
| writer         = Warwick Deeping (novel Sorrell and Son) Elizabeth Meehan (adaptation) Herbert Brenon (scenario)
| starring       = H. B. Warner Anna Q. Nilsson Norman Trevor Nils Asther Mary Nolan
| cinematography = James Wong Howe
| editing        = Marie Halvey
| distributor    = United Artists
| released       =  
| runtime        = 10 reels; 9,000 feet
| country        = United States
| language       = Silent film (English intertitles)
}} Warwick Deeping, Sorrell and Son, which became and remained a bestseller from its first publication in 1925 throughout the 1920s and 1930s.
 
The screenplay was adapted by Elizabeth Meehan. It was written and directed 
by Herbert Brenon. Filming took place in England.

==Remake== Sorrell and Son with H.B. Warner repeating his role as Stephen Sorrell, and as a British television serial in 1984.

==Preservation status==
The 1927 version was considered a lost film for many years. However, in 2005. the Academy Film Archive premiered a partially restored print. 

== Cast ==
* H.B. Warner as Stephen Sorrell
* Anna Q. Nilsson as Dora Sorrell
* Mickey McBan as Kit (as a child)
* Carmel Myers as Flo Palfrey
* Lionel Belmore as John Palfrey
* Norman Trevor as Thomas Roland
* Betsy Ann Hisle as Molly (as a child)
* Louis Wolheim as Buck
* Paul McAllister as Dr. Orange
* Alice Joyce as Fanny Garland
* Nils Asther as Christopher Kit Sorrell Mary Nolan as Molly Roland

==See also==
*List of rediscovered films

==References==
 

== External links ==
*  
*  
*  
*  
*   at Virtual History

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 