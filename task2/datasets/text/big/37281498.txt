Going Sane
 
 
{{Infobox film
| name           = Going Sane
| image          = 
| image size     =
| caption        =  Michael Robertson
| producer       = Tom Jeffrey
| writer         = John Sandford
| based on = 
| narrator       = John Waters Linda Cropper Judy Morris
| music          = 
| cinematography = Dean Semler Brian Kavanagh
| studio = Sea Change Films
| distributor    = 
| released       = 30 July 1987
| runtime        = 89 mins
| country        = Australia English
| budget         = A$2.1 million 
| gross = 
| preceded by    =
| followed by    =
}}
Going Sane is a 1987 Australian comedy starring John Waters. It was one of several films made in the 1980s where Waters plays a character who has a mid life crisis. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p110  In 1994 when asked to name his worst movie, Waters said Going Sane was the one "that failed to achieve its brief more than any other." 

==Production==
Filming started 15 July 1985. 

==References==
 

==External links==
* 

 
 
 
 


 