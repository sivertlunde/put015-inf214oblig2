The Boys Didn't Wear Hair Gel Before
{{Infobox film
| name           = The Boys Didnt Wear Hair Gel Before
| image          = Los Muchachos de antes no usaban gomina37.jpg
| image size     = 200px
| caption        = Screenshot
| director       = Manuel Romero
| producer       =
| writer         = Mario Bernard    Manuel Romero
| narrator       =
| starring       =  Florencio Parravicini   Mecha Ortiz   Santiago Arrieta   Irma Córdoba 
| music          = Alberto Soifer 
| cinematography =Francisco Múgica 
| editing        = Francisco Múgica 
| studio         = Lumiton 
| distributor    =Lumiton 
| released       = 31 March 1937 
| runtime        = 90 minutes
| country        = Argentina
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} historical drama film directed and written by Manuel Romero and starring  Florencio Parravicini, Mecha Ortiz and Santiago Arrieta.  Older and wiser, a man recount an romance he had with a young woman in turn-of-the-century Buenos Aires. He was eventually forced to give her up and marry a girl from a wealthy family.
 was remade in 1969.

==Cast==
* Florencio Parravicini as 	Ponce - Mocho
* Mecha Ortiz as La Rubia Mireya
* Santiago Arrieta	 as	Alberto
* Irma Córdoba	 as	Camila Peña
* Martín Zabalúa	 as	Carlos Rosales
* Niní Gambier	 as	Inés Rosales
* Alfonso Pisano	 as	Euclides García Fuentes
* Mary Parets as	Lucy Rosales
* Pedro Laxalt as	Jorge Rosales
* Aurelia Musto	 as	Sra.Rosales
* Hugo del Carril	 as	El cantor
* María Vitaliani	 as	Sra. Peña
* Amalia Bernabé	 as	Amiga de Camila
* Fernando Borel	  as Jorge Newbery
* Eduardo de Labar	 as Hansen
* Homero Cárpena	 as Guapo Salinas
* Malisa Zini	 as Amiga de Camila
* Isabel Figlioli	 as Mujer del cabaret
* Jorge Lanza	 as Amigo de Rivera
* Carlos Enríquez	 as	Vendedor de globos Osvaldo Miranda	 as 	Amigo de los Rosales
* Delfina Fuentes		
* Nelly Quirós	
* Andrés Labrano		 Roberto Blanco	
* Roberto Páez
* José de Ángelis

== References ==
 

== Bibliography ==
* Rist, Peter H. Historical Dictionary of South American Cinema. Rowman & Littlefield, 2014. 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 