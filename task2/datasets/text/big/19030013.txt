Trucks (film)
 

{{Infobox film|
| name = Trucks
| image = Trucks_film.jpg
| director = Chris Thomson
| producer = Robert Simonds
| writer = Stephen King Brian Taggert
| starring = Timothy Busfield Brenda Bakke Aidan Devine Roman Podhora Jay Brazeau Rick Skene Brendan Fletcher
| released = October 29, 1997
| runtime = 95 minutes English
| country = Canada
| music = DawnField Entertainment( VHS/DVD )
| distributor = Trimark Pictures
| awards =
| budget =
}} Canadian and American television Horror film directed by Chris Thomson. It is based on Stephen Kings short story "Trucks (short story)|Trucks", which also serves as the source material for the earlier film Maximum Overdrive, the only film directed by King.

==Synopsis==

In a small American town, the town residents live peacefully in a small community. But one day, trucks mysteriously start to come to life and develop their own minds, but they end up prompting violence, starting to destroy houses and kill people, and the rest of the townspeople have to figure out a way how to destroy them all before they kill all the residents of the town.

== Cast ==
* Timothy Busfield as Ray Porter
* Brenda Bakke as Hope Gladstone
* Aidan Devine as Trucker Bob
* Roman Podhora as Thad Timmy
* Jay Brazeau as Jack
* Brendan Fletcher as Logan Porter
* Amy Stewart as Abby Timmy
* Victor Cowie as George "Georgie"
* Sharon Bajer as June Yeager
* Jonathan Barrett as Brad Yeager
* Rick Skene as Trucker Pete
* Don Granberry as Sheriff
* Barbara Lee Edwards as TV Reporter
* Gene Pyrz as Refrigerator Truck Driver
* Kirk Harper as Lino
* Harry Nelken as Phil
* David Brown as Gas Station Customer in Car
* Chuck Robinson as Lunar town Bus Driver
* Len Wagner as Toxic Worker 2
* Erich Hicks as Toxic Suit Killer with Axe

==References==

 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 