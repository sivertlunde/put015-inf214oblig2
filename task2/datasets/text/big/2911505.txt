Castle of Blood
{{Infobox film
| name = Danza macabra Castle of Blood
| image = Danza macabra poster.jpg
| image_size =
| caption = Italian film poster for Castle of Blood
| director = Antonio Margheriti
| producer = Leo Lax Marco Vicario
| writer = Sergio Corbucci Giovanni Grimaldi
| narrator =
| starring = Barbara Steele Georges Rivière
| music = Riz Ortolani
| cinematography = Riccardo Pallottini
| editing = Otello Colangeli
| distributor = Woolner Brothers Pictures Inc (US)
| released =  
| runtime = 87 min
| country = Italy Italian
| budget =
}} Italian horror film directed by Antonio Margheriti, using the pseudonym Anthony M. Dawson. This film is also known as Coffin of Terror, Danse macabre, Dimensions in Death, La Lunga notte del terrore, Terrore, The Castle of Terror, The Long Night of Terror, Tombs of Horror, and Tombs of Terror.

== Cast ==
*Barbara Steele as Elisabeth Blackwood
*Georges Rivière as Alan Foster
*Margarete Robsahm as Julia
*Arturo Dominici as Dr. Carmus
*Silvano Tranquilli as Edgar Allan Poe
*Sylvia Sorrente as Elsi
*Giovanni Cianfriglia as  Herbert
*Umberto Raho as  Lord Thomas Blackwood
*Salvo Randone as  Lester 
*Benito Stefanelli as  William

== Plot ==
A journalist challenges Edgar Allan Poe on the authenticity of his stories, which leads to him accepting a bet from Lord Blackwood to spend the night in a haunted castle on All Souls Eve. Ghosts of the murdered inhabitants appear to him throughout the night, re-enacting the events that lead to their deaths. It transpires that they need his blood in order to maintain their existence. Barbara Steele plays a ghost who attempts to help the journalist escape.

== Release== credits which refer to a non-existing short story by Poe. Moreover Silvano Tranquilli plays Poe recounting the end his story "Berenice (short story)|Berenice" at the films beginning.

== Critical reception ==
AllRovi|Allmovies review of the film was favorable, calling it an "eerie and effective early horror film". 

==Remake==
In 1971, director Antonio Margheriti remade Castle of Blood as Web of the Spider. In this version Poe was played by Klaus Kinski, and the movie claimed to be based on a different non-existent Poe story called "Night of the Living Dead".

==Biography== 
* 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 

 
 