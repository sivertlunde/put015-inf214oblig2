Barsaat Ki Ek Raat
 
 
{{Infobox film|
  name           = Barsaat Ki Ek Raat|
  image          = barsaat ki ek raat.jpg|
| director       = Shakti Samanta
| producer       = Shakti Samanta
| writer         = Shaktipada Rajguru
| based on       =  
| narrator       =
| starring       = Amitabh Bachchan Raakhee Amjad Khan
| music          = Rahul Dev Burman
| cinematography =
| editing        =
| distributor    = Shakti Films
| released       =  
| runtime        = 142 mins
| country        = India Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Barsaat Ki Ek Raat  ( ;  ) is a 1981 Bollywood film starring Amitabh Bachchan, Raakhee, Amjad Khan and Utpal Dutt. It was directed by Shakti Samanta. Barsaat Ki Ek Raat was filmed in two languages; the Bengali version, titled "Anusandhan", was the top grossing Bengali movie ever for a number of years. The story was adopted from the novel "Anushandhan" by Shaktipada Raajguru.

==Characters==
* Amitabh Bachchan  as  Abhijit
* Raakhee  as  Rajni  (Tamosha in Bengali version)
* Amjad Khan  as  Kaaliram
* Abhi Bhattacharya  as  Rajnis father
* Utpal Dutt  as  Sahuji, Kaalirams father
* Prema Narayan  as  Village belle Asit Sen  as  Police inspector
* Sujit Kumar
* Nimu Bhowmick

==Story==
In a small village in Darjeeling, Sahuji (Utpal Dutt) the merchant has weaved a web of corruption in every layers of the social fabric. He supplies materials of inferior quality to the tea garden, and then bribes the accountants to pass his bills. When one of the
managers, called "Boro Babu", (Abhi Bhattacharya), stands up, Sahuji pays the workers to go into strike against this Boro Babu. And he is also involved in rampant smuggling of goods across the border, and everyone from the local jeweler to the local police inspector are part of his intricate web.

While the father has created a position of influence by spreading corruption, his son Kaaliram (Amjad Khan) has ushered in a reign of terror. He goes to local bars, drinks, and doesnt pay. Anyone standing in his way gets beaten up mercilessly, either by him or his thugs. After getting sufficiently intoxicated, he then indulges in carnal desire by forcefully taking away any of the unmarried village girls for a night of merriment. If the poor girls parents try to fight back, their house is set on fire. Desperate villagers make a plea to the owner of the tea garden, who calls (presumably) the higher ups in police force and they promise to send someone.
 Asit Sen), always comes to his rescue and prevents him from being sent to jail. However, Kaalis frustration grows.

In the meanwhile, Abhijit has met Rajni (Raakhee), the blind daughter of the Boro Babu, and fell in love with her. In the first of the two important Barsaat Ki Raats (which means rainy night) in the story, Kaali makes a bold attempt to molest Rajni, but Abhijit comes to her rescue. And when the police inspector again attempts to drag his feet, Abhijit erupts in anger, shows his identity as a very high level police officer, throws Kaali into jail and suspends the police officer also.

Second half of the story finds Abhijit, now promoted, in a quaint little village named Sonarpur, with Rajni as his wife, and awaiting the arrival of their first child. In the meanwhile, Kaalia gets out of jail, and finds the whereabouts of Abhijit and Rajni. He sends Abhijit away from home by making a fake call for help, and then, in the second significant Barsaat Ki Raat of the story, he enters Abhijits home and kills Abhijits unborn child.

A clue found outside the house ties Kaali with the incident and Abhijit runs out to take his vengeance. His police jeep was sent off the road by Kaalis truck, and he was presumed dead. But, in a typical Hindi movie formula, he comes back in a disguise, participates in a song and dance with a village belle (Prema Narayan), and then brings Kaalia to justice. However, as he is about to drag the handcuffed Kaalia to the police station, Sahuji, in an attempt to shoot Abhijit, ends up killing his son.

==Music==
The film has two solos by Lata Mangeshkar, a romantic duet by Lata and Kishore, the mocking of Kaaliram after the drum competition sung in the form of a titular song by Kishore ("KKaliram Ka Dhol"), and then the melody of "Manchali O Manchali" sung by Kishore and Asha.

Some of the scenes have been in filmed in Darjeeling. The knife shown in the poster is the "Kukri|Khukri".

===Tracklist===
{{Track listing
| headline     = Songs
| extra_column = Playback
| all_lyrics   = 
| all_music    = Rahul Dev Burman

| title1 = Apne Pyar Ke
| extra1 = Kishore Kumar, Lata Mangeshkar

| title2 = Haye Wo Pardesi
| extra2 = Lata Mangeshkar

| title3 = Kaliram Ka Dhol
| extra3 = Kishore Kumar

| title4 = Manchali O Manchali
| extra4 = Kishore Kumar, Asha Bhosle

| title5 = Nadiya Kinare Pe
| extra5 = Lata Mangeshkar
}}

==External links==
*  
*  

 
 
 
 