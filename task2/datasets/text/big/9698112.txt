Aanch
{{Infobox film
| name = Aanch
| image =  Aanch.jpg
| starring = Suchindra, Sharbani Mukherjee, Nana Patekar, Paresh Rawal
| director = Rajesh Singh
| producers =  B L Saboo & Poonam Jhawer
| cineamatography = Damodar Naidu
| released = November 28, 2003
| screenplay = Kamal Pandey
| runtime = 160 min. Hindi
| music = Sanjeev Darshan
| awards = 
}}
Aanch is a 2003 Hindi movie directed by Rajesh Kumar Singh.    Nana Patekar, Paresh Rawal, Suchindra and Sharbani Mukherjee played the lead roles.     Its an epic love story set against the background of a crime ridden some parts of rural Uttar Pradesh. Sanjeev-Darshan scored the music for the movie.  

==Plot==
Nana Patekar and Paresh Rawal belong to two different villages. They hate each other for reasons best known to them. Suchindras father (residing in Pareshs village) and Sharbanis brother (a resident of Nanas village) decide to get them Suchindra and Sharbani Mukherjee married.
The preparations to the marriage ceremony begin and neither the boy nor the girl knowing what the other looks like. In the meanwhile, the two villages get ready to confront each other in case of a quarrel. The wedding ceremony is underway when the revelry between the two village head starts and the guns roar. The bride and the groom come to the town. They are happy to escape the brutal world of rural India to find their own careers and future. They first bump into each other in the train, then in the college, completely unaware of all facts. For Suchindra, its love at first sight, but Sharbani doesnt respond to his overtures. Later she packs her bags and heads straight for her village. In the meanwhile, Suchinder learns that Sharbani is his legally wedded wife, and he decides to get her back from the clutches of two warring villages.

==Cast==
{|class="wikitable"
|-
!Actor/Actress !! Character
|- Arun Bakshi
|Latas father
|- Suchindra Bali Diwakar
|- Poonam Jhawer Lata K. Thakur
|- Ayesha Jhulka Devangi M. Thakur
|- Akhilendra Mishra Shambhu
|- Sharbani Mukherjee|Sharbani Mukherji Vidya
|- Nirmal Pandey Kirti Thakur
|- Nana Patekar Mahadev Thakur
|- Vishwajeet Pradhan
|Jawahars son
|- Deepraj Rana|Deep Raj Rana Shiva
|- Paresh Rawal Jawahar Pandit
|- Rakhi Sawant Singer & Dancer at Wedding
|- Raghuvir Yadav Chilkona
|-
|}

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s) !! notes
|-
| 1
| "Dil Ke Arma Tarse"
| Sonu Nigam, Mahalakshmi Iyer Tune copied from Middle eastern singer Amr Diabs  hit song El alem Allah from his 2000 international hit album Tamally Maak.
|-
| 2
| "Lehron Se Khalen Hai Payal"
| Alka Yagnik, Kumar Sanu
|-
| 3
| "Sada Suhagan"
| Anuradha Paudwal, Chanadana Dixit
|-
| 4
| "Tapki Jaye" Richa Sharma
|-
| 5
| "Mera Dil Chura"
| Udit Narayan
|-
| 6
| "Sun Meri Rani"
| Nana Patekar, Poonam Jhawar
|}

== External links ==
 

== References ==
 

 
 
 


 