Sobar Upore Tumi
{{Infobox film
| name = Sobar Upore Tumi
| image  = sobar upore tumi.jpg
| image size  = 200px
| caption = VCD Cover
| director = F I Manik
| producer = Kishor Shah
| writer = Chotko Ahmed
| starring = Shakib Khan Swastika Mukherjee Victor Banerjee Uzzol  Suchorita   Misha Sawdagor
| distributor = Keishan Chalochitra 
| music = Devendranath Alluddion Ali
| cinematography = Mostafa Kamal
| editing = Thofik Hossion Choudhury
| released = 
| runtime = 150 minutes
| country = Bangladesh India Bengali
}}

Sobar Upore Tumi ( ) is a Bangladeshi Bengali language film. The film, directed by Bangladeshi filmmaker F I Manik, is a romantic family story. It stars Shakib Khan, Swastika Mukherjee, Victor Banerjee, Uzzol, Suchorita and others. In India this film was released under the title Aamar Bhai Aamar Bon. It is an Indian and Bangladesh joint production.

==plot==
The story is as Mr. Chowdhury (Uzzol) a businessman lived with his wife Mamata (Suchorita) and children Rahul (Shakib Khan) and Ria. An accident totally changed his life. Mamata could not bear the accident and was admitted into a mental asylum. Ruahul and Ria became homeless by the conspiracy of Mr. Chowdhurys sister. Mr. Chowdhury though lost his family. In such a condition, Rahul and Ria started their struggling in life. Rahul and Ria grew up and Rahul took a job as a driver of Biplab babu. Mithila (Swastika Mukherjee) was the only child of Biplab (Victor Banerjee). She initially did not like Rahul for his attitude, but later she fell in love with him. Mithila helped Rahul a lot to establish his own business. Ultimately, Rahul became successful and with the help of his well-wishers, he found his parents. In this way, everything ended on a happy note.

==Cast==
*Shakib Khan as Rahul
* Swastika Mukherjee as Mithila
* Victor Banerjee as Mithilas Father
* Uzzol as Rahuls Father
* Suchorita as Rahuls Mother
* Aliraj
* Suyab Khan
* Sayontoni Lisa
* Misha Sawdagor
* Ahmed Sarif
* Shiva Shanu
* Kabila
* Nasrin
* Rasheda Chowdhary
* Raju Sarkar

==Crew==
* Producer: Kishor Shah
* Story: Chotko Ahmed
* Screenplay: Chotko Ahmed
* Director: F I Manik
* Script: Mohammad Rafikuzzaman
* Dialogue: Chotko Ahmed
* Cinematography: Mostafa Kamal
* Editing: Thofik Hossion Choudhury
* Music: Devendranath and Alluddion Ali
* Distributor: Keishan Chalochitra Ltd.

==Music==
{{Infobox album
| Name = Sobar Upore Tumi
| Type = Album
| Artist = Devendranath  Alluddion Ali
| Cover =
| Background = Gainsboro |
| Released = 2009 (Bangladesh)
| Genre = Soundtrack/Filmi
| Length =
| Label =
| Producer =Eagle Music
| Reviews =
| Last album = 
| This album = Sobar Upore Tumi (2009)
| Next album = 
|}}
Sobar Upore Tumis music was directed by Devendranath (India) and Alluddion Ali (Bangladesh).

===Soundtrack===
{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Trac !! Song !! Singers !! Performers !! Notes
|- 1
|Mem saheb nondini 
| Shakib Khan and Swastika Mukherjee
|
|- 2
|Tumi amoni ekjon
|
| Shakib Khan and Swastika Mukherjee
|
|- 3
|Amar ek hata surjo
|
|
|
|- 4
|Pagri mathai pore Sonu Nigam
| Shakib Khan
|
|- 5
|Kisher Shahrukh kiser salman
| Kabila and Nisrin
|
|- 6
|Ke tumi sai
|
|
|
|}

==Home media==
Sobar Upore Tumis copyrights take G-Series and made as VCD, DVD and cassette marketed/distributed in Bangladesh. For India the films copyrights take Angel and made as VCD, DVD and cassette.

==References==
 

 
 
 
 
 
 