Television Talent
{{Infobox film
| name = Television Talent
| image =
| image_size =
| caption = Robert Edmunds
| producer = R. Howard Alexander
| writer =  R. Howard Alexander   Robert Edmunds 
| narrator =
| starring = Richard Goolden   Hal Walters   Polly Ward
| music = 
| cinematography = 
| editing = 
| studio = Alexander Films 
| distributor = Ambassador Film Productions
| released = November 1937
| runtime = 56 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Robert Edmunds and starring Richard Goolden, Hal Walters and Polly Ward. The film was a quota quickie.  A music teacher takes part in a talent contest. 

==Cast==
* Richard Goolden as Professor Langley 
* Gene Sheldon as Herbert Dingle  
* Hal Walters as Steve Bagley 
* Polly Ward as Mary Hilton

==References==
 

==Bibliography==
*Chibnall, Steve. Quota Quickies: The Birth of the British B Film. British Film Institute, 2007.
*Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
*Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  

 
 
 
 
 
 

 