Kiss Me (2014 film)
 
{{Infobox film
| name           = Kiss Me
| image          = Kiss Me 2014 film.png
| alt            = Movie poster 
| caption        = 
| director       = Jeff Probst
| producer       = {{Plainlist|
*Jeff Probst
*Katty Wallin-Sandalis
*Jhon J. Kelly
}}
| screenplay     = Elizabeth Sarnoff
| starring       = {{Plainlist|
*Emily Osment
*Sarah Bolger
*Missi Pyle
*Jenna Fischer
*John Corbett
}}
| music          = BC Smith
| editing        = Eric Potter
| studio         = MysticArt Pictures
| distributor    = Studio Universal
| released       = 5 July 2014
| runtime        =  minutes
| country        = United States
| language       = English 
}}
Kiss Me  is an American romantic drama film. It stars Emily Osment and Sarah Bolger and was directed by Jeff Probst.

==Plot==
The movie features two 15-year-old girls, Shelby (Emily Osment) and Zoe (Sarah Bolger). Zoe is diagnosed with scoliosis and has to wear a brace for a few years, which will lead to problems in her life. She is attracted to an older man, photographer Chance (John Corbett). Shelby, on the other hand, has problems at home.  Her father is an alcoholic and is abusive towards her and her mother. Shelby and Zoe cross paths at school, and, through the physical and emotional growing pains in their lives, they test the limits of their relationship and start a friendship that will change their lives forever. When Shelbys mom finds out, everything explodes.

==Cast==
*Emily Osment as Shelby
*Sarah Bolger as Zoe
*Missi Pyle as Pam
*Jenna Fischer as Vera
*John Corbett as Chance
*Jes Macallan as Erica
*Rita Wilson as Edith Steven Weber as Arthur
*Currie Graham as Dr. Craig
*Davenia McFadden as Nurse Sylvie

==Production==
===Casting=== Steven Weber, Davenia McFadden and John Corbett joined the cast. It was also announced that Irish actress Sarah Bolger and Emily Osment were cast in the lead roles.  

===Filming===
The film was shot in Los Angeles in early February 2012.

==See also==
*Emily Osment
*Sarah Bolger

==References==
 

==External links==
*  

 
 
 
 
 


 