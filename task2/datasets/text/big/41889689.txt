Tbilisi, I Love You
{{Infobox film
| name           = Tbilisi, I Love You
| image          = Tbilisi, I Love You movie poster.jpg
| alt            =
| caption        = Theatrical release poster Nika Agiashvili Irakli Chkhikvadze Levan Glonti Alexander Kviria Tako Shavgulidze Kote Takaishvili Levan Tutberidze.
| producer       = David Agiashvili Nick Agiashvili Emmanuel Benbihy Jason Speer
| writer         = Nick Agiashvili Emmanuel Benbihy Tristan Carné Levan Glonti Sandro Kakulia Archil Kikodze Alexander Kviria Tako Shavgulidze Kote Takaishvili Levan Tutberidze	
| starring       = 
| music          = Niaz Diasamidze
| editing        = Irakli Chkhikvadze Levan Kukhashvili Nodar Nozadze Rusudan Pirveli
| studio         = Storyman Pictures
| distributor    = 
| released       =  
| runtime        = 
| country        = Georgia
| language       = 
| budget         = 
| gross          = 
}}
Tbilisi, I Love You is an anthology film starring an ensemble cast of actors of various nationalities and part of Emmanuel Benbihy’s Cities Of Love franchise that started with Paris, je taime and New York, I Love You, has set to be released in Georgia on February 20.

The file consists a series of 10 short films written and directed by natives of Georgia, and take on a personal narrative about the republic’s capital city. Malcolm McDowell and Ron Perlman both feature. McDowell’s vignette centres on an actor who reluctantly agrees to a one-month shoot in Tbilisi and develops a love affair with the city. Perlman’s short features him as a nameless American motorcyclist who rides through Tbilisi’s remote areas with a woman named Freedom.

A festival schedule is being determined.

==External links==
*  
*  

 

 
 
 
 
 
 
 

 