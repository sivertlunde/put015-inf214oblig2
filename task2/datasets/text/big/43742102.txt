Kangal (film)
{{Infobox film
| name           = Kangal கண்கள்
| image          =
| image_size     =
| caption        =
| director       = Krishnan-Panju
| producer       =
| writer         = N. V. Rajamani
| screenplay     = N. V. Rajamani
| starring       = Sivaji Ganesan Pandaribai S. V. Sahasranamam J. P. Chandrababu
| music          = S. V. Venkatraman
| cinematography = R. R. Chandiran
| editing        = Devarasan
| studio         =
| distributor    = Motion Pictures Team
| released       =  
| country        = India Tamil
}}
 1953 Cinema Indian Tamil Tamil film, directed by Krishnan-Panju. The film stars Sivaji Ganesan, Pandaribai, S. V. Sahasranamam and J. P. Chandrababu in lead roles. The film had musical score by S. V. Venkatraman.  

==Cast==
*Sivaji Ganesan
*Pandaribai
*Mynavathi
*S. V. Sahasranamam
*J. P. Chandrababu
*V. K. Ramasamy (actor)|V. K. Ramasamy

==Soundtrack==
The music was composed by S. V. Venkatraman.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Alu Ganam Aanaal Moolai Gaali || J. P. Chandrababu|| Kambadasan || 02:35 
|- 
| 2 || Inba Veenaiyai Meetudhu || M. L. Vasanthakumari || Kambadasan || 03:08 
|- 
| 3 || Paadi Paadi Dhinam  || Jikki ||  || 03:14
|}

==References==
 

==External links==
*  

 
 
 
 


 