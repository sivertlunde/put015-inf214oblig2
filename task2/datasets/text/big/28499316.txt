Ranczo Wilkowyje
 
{{Infobox television film
| name = Ranczo Wilkowyje
| image = 
| caption = 
| director = Wojciech Adamczyk
| producer  = 
| writer = Robert Brutter Jerzy Niemczuk
| starring  = Ilona Ostrowska Paweł Królikowski Radosław Pazura Artur Barciś Dorota Chotecka Franciszek Pieczka
| music  = Krzesimir Dębski
| cinematography = Janusz Sosnowski
| editing = Marek Król TVP
| studio = Studio A
| released =    
| runtime = 96 minutes
| country  = Poland
| language  = Polish/English
| budget  =
| gross =
| followed_by = 
}}

Ranczo Wilkowyje is a Polish film released in 2007 directed by Wojciech Adamczyk, which follows the story of television series produced by TVP Ranczo(TV series)|Ranczo.

The film was shown in cinemas and is available for sale on DVD.

==Additional Information==

* Term began shooting on July 17, 2007. The photos were made in Latowicz and Jeruzal.
* English language has been replaced by a voice-over and Polish subtitles.

==Cast==
{| class="wikitable"
|-
! Actor !! Role
|- Ilona Ostrowska || Lucy Wilska
|- Cezary Żak || Priest Piotr Kozioł
|- Cezary Żak || Paweł Kozioł
|- Violetta Arlak || Halina Kozioł
|-
|Paweł Królikowski || Jakub "Kusy"
|- Marta Chodorowska || Klaudia Kozioł
|- Bogdan Kalus || Tadeusz Hadziuk
|- Piotr Pręgowski Piotr Pręgowski || Patryk Pietrek
|- Franciszek Pieczka || Stanisław "Stach" Japycz
|- Sylwester Maciejewski || Maciej Solejuk
|- Katarzyna Żak || Dorota Solejuk 
|- Marta Lipińska || Michałowa
|- Grzegorz Wons || Andrzej Więcławski
|- Dorota Chotecka || Krystyna Więcławska
|- Artur Barciś || Arkadiusz Czerepach 
|- Piotr Ligienza || Fabian Duda 
|- Wojciech Wysocki || Doktor Mieczysław Wezół 
|- Arkadiusz Nader || Policjant Stasiek 
|- Magdalena Waligórska || Wioletka 
|-
|Radosław Pazura || Louis
|- Danuta Borsuk || Mother of the bride
|- Filip Bobek || Flower deliverer
|}

 
 

 