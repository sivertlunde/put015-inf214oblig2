Malayali Mamanu Vanakkam
{{Infobox film
| name           =Malayali Mamanu Vanakkam
| image          = 
| image size     =
| caption        =
| director       = Rajasenan
| producer       = Kalliyoor Sasi B. Rakesh
| writer         = 
| narrator       = Prabhu Roja Roja Kalabhavan Mani
| music          = 
| cinematography = 
| editing        =
| distributor    = Cinema Cinema
| released       = 2002
| runtime        =
| country        = India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Malayali Mamanu Vanakkam is a Malayalam language film directed by Rajasenan starring Jayaram, Prabhu and Roja as the leads. It was released in 2002. 

==Synopsis==
Malayalimamanu Vanakkam movie tells Anandakuttans mother (Srividya) wants her daughter Anandavalli(Shobha Mohan) to be with the family for Anandakuttans (Jayaram) marriage with Revathy (Suja Karthika). So he is on his way to Tamil Nadu in search of his sister and her husband Muniyandi (Kalabhavan Mani).

But Muniyandi wants to take his revenge against Anandakuttans father who had cut one his legs. Anandakuttan finds out his sister and she sends her daughter Parvathy (Roja) with Anandakuttan home. Parvathy starts falling in love with Anandakuttan. In the mean time Muniyandi, who is Thiruppathy Perumal now. 

==Cast==
* Jayaram as Anandakuttan/Kuttan/Kuttu Prabhu as Periyakulam Kannayya/Kannayya Roja as Parvathy/Paru
* Kalabhavan Mani as Thiruppathy Perumal/Muniyandi
* Jagathy Sreekumar as Keshu/Keshu Ammavan
* Suja Karthika as Revathy
* Srividya as Anandakuttans mother
* Shobha Mohan as Anandavalli
* Oduvil Unnikrishnan
* Sudheesh as Lodge receptionist
* Machan Varghese as Chinnayya (Kannayyas helper)

In the films dubbed Tamil version, Gounder Veetu Maapillai, a separate comedy track featuring Vadivelu was shot and added.

==References==
 

 
 
 


 