Arike
{{Infobox film
| name           = Arike
| image          = Arike (movie poster).jpg
| alt            = 
| caption        = Film poster
| director       = Shyamaprasad
| producer       = N. B. Vindhyan
| story          = Sunil Gangopadhyay
| screenplay     = Shyamaprasad Dileep Samvrutha Sunil Mamta Mohandas
| music          = Ouseppachan
| cinematography = Azhagappan
| editing        = Vinod Sukumaran 
| studio         = Picture Perfect
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Arike ( ) is a 2012 Malayalam romantic comedy film   by Shyamaprasad, starring Dileep (actor)|Dileep, Samvrutha Sunil and Mamta Mohandas. The film, based on a Bengali short story by Sunil Gangopadhyay, is produced under the banner of  Picture Perfect and has script by Shyamaprasad himself, cinematography by Alagappan, editing by Vinod Sukumaran and music by Ouseppachan.

The film, a romantic comedy, is about the shades of love that vary in a relationship woven around three individuals.

==Plot==
The film tells the story of Shantanu (Dileep (actor)|Dileep), a researcher in Linguistics, and his two friends, Kalpana (Samvritha) and Anuradha (Mamta). Anuradha is the sensitive type who has gone through certain bitter experiences in her teenage, where her cousin approached her with love, which she later realised that it was just his lust for her. Kalpana is slightly capricious and loves being in love. Anuradha wants to play Cupid and bring Shantanu and Kalpana together; she knows that they care a lot for each other, but for some reason or the other are hesitant to take the next step.

Kalpanas parents are against her marrying Santanu and they try to bring proposals from their own (Brahmin) community. At one point Kalpanas aunt tricks her into meeting a guy, Sanjay, who they hope Kalpana will like and get married to. While they travel with Sanjay in his car, they meet with an accident, which causes some scars on Kalpanas face and gave her a disfigured toe. After the accident Kalpana backs out from the relationship with Santanu and give-in to her parents wishes to marry Sanjay. Anuradha feels very bad for Santanu and tries to console him. It is then that Santhanu realizes that Kalpana had actually not loved him at all. Though his actual love was near him, so close, he failed to recognise it.

==Cast== Dileep as Shantanu
* Samvrutha Sunil as Kalpana
* Mamta Mohandas as Anuradha
* Ajmal Ameer as Sanjay Shenoy
* Urmila Unni as Alaga Devi, Kalpanas mother
* Vineeth as Balu Innocent as Ananthanaraya Pai, Kalpanas father
* Madampu Kunjukuttan as Guruji
* Dinesh Panicker as Subramanya Pai
*Valsala Menon as Balus Mother
* Chithra Iyer as Sujatha
* Sreenath Bhasi as Anjan
* Prakash Bare as Vinayan
* Chandramohan as Anuradhas Father
* Kozhikode Narayanan Nair as Balus Father

==Production==
===Adaptation===
Shyamaprasad has adapted several classics on celluloid so far. Arike is the latest in that list and is based on a Bengali short story by Sunil Gangopadhyay, whose novel was also the base for the directors Ore Kadal.    Says the director, "I don’t think adapting stories from other languages is an issue at all, because human emotions are universal and can be told without linguistic barriers. I am fond of adapting literature, since writing a story is not my forte and I do not believe in concocting a story for the sake of filmmaking."    He also adds that Basu Chatterjee, noted Bollywood director who portrayed the middle-class in many films, proved to be his guiding force while making Arike. 

===Themes===
Like all of Shyamaprasads previous films, Arike too is about the quest for love.  Talking about the theme of Arike, the director says, "There is an obsessive quest for love in each one of us. We have illusions and inhibitions about love, yet we have an irrepressible thirst for it. We all want to find happiness in love. But whether we get that happiness is the question which I want to delve through this film."  

Compared to the dark themes he took up earlier, Arikes tone is relatively lighter. Shyamaprasad says, "In Ore Kadal, the subject had an shadow of gloom and depression, Ritu (film)|Ritu was about the aspirations and attitude of the young working population. Arike  has a breezy feel, and the story is told in a lighthearted and realistic way. I have also taken efforts to avoid cliches such as rain, sea, train that are common motifs in romantic films." 

===Casting and filming===
The film has only a handful of characters. Shyamaprasad wanted to use live sound recording and hence he chose actors who could emote dialogues perfectly while shooting itself.  Shyamaprasad says he selected Dileep to play the protagonist because he feels the actor has the look and feel of the character.  This is the first time Shyamaprasad is casting Dileep in the lead role, though the actor had earlier done a role in the directors 1998 film Kallu Kondoru Pennu.       The films production started in late November 2011.    The director had given all the actors a copy of the script so that they could practice their parts well. 

The film uses sync sound recording or live sound recording. Shyamaprasad brought in Sohel Sanwari from Bollywood to record the sound. Says Shyamaprasad, "This is something I’ve always wanted to do. The early talkies had speech recorded live but the change to outdoor locations made carrying recording cameras cumbersome. Added to this was the fact that technicians and stars started travelling and this started the trend of dubbing. I feel that we lose 50 to 60 per cent of the performance when we post-synchronise voices. Take for example a scene on the seashore — our body, voice modulation and mannerisms are in sync with the surroundings. Dubbing in a studio means that a lot of vocal nuances get lost!"    Lead player Dileep describes the sync sound recording experience, "I had to memorise the entire dialogues beforehand and deliver them at one go after umpteen rehearsals. It was a very novel experience for me and I personally feel that it increases one’s memory power."  Samvrutha Sunil says, "This is my first film where our voices have been recorded live during our respective performances. The sync sound recording actually threw open many challenges - one, you cant console yourself for a not-up-to-the-mark performance saying that you can cover it up with a good dubbing and two, you have to byheart the dialogues thoroughly as there is no prompting."   

The film was shot completely from Kozhikode. It was Shyamaprasads first film to be filmed from the city. He says it was his emotional relationship with the city and its calm nature which helped live recording to a great extent, that inspired him to choose Kozhikode as the main locale.  Filming was completed in late December 2011.  Cinematographer Alagappan says he has experimented a lot with the film, and the film was made in a special "visual tone." The film was shot using a Sony F3 Camera.    Sakhi Thomas, who worked with Shyamaprasad in Ritu and Elektra (2010 film)|Elektra, was the costume designer for this film. 

==Soundtrack==
{{Infobox album   Name        = Arike Type        = Soundtrack Artist      = Ouseppachan Cover       =  Caption     = Album cover Released    = June 11, 2012 Recorded    =  Genre  Feature film soundtrack Length      = 56:32 (inc. Karaoke) Label       =  Universal Music International Producer    =  Reviews     =  Last album  = Thiruvambadi Thamban  (2012) This album  = Arike (2012) Next album  = Kalikalam (2012)
}}

All songs are composed by Ouseppachan. All lyrics are by Shibu Chakravarthy.

{| class="wikitable"
|-
! Song !! Length !! Artist(s) 
|-
| "Varavayi Thozhi Vadhuvayi" || 5:15 || Nithyashree Mahadevan
|- 
| "Shyam Hare" || 6:19 || Swetha Mohan
|-
| "Iravil Viriyum" || 5:19 || Mamta Mohandas 
|-  Karthik
|-
| "Ee Vazhiyil" || 6:45 ||  ) 
|}

==References==
 

== External links ==
*  

===Reviews===
*   ( )
*  
*   ( )
*   ( )
*   (Verdict: Average)
*   (Verdict: Above Average)

 

 
 
 
 
 
 
 
 