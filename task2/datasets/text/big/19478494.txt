Touched by Love
{{Infobox film
 | name = Touched by Love
 | image_size = 
 | caption = 
 | director = Gus Trikonis
 | producer = Michael Viner
 | writer = Lena Canada (story "To Elvis with Love") Hesper Anderson
 | starring = Diane Lane Deborah Raffin
 | music = 
 | cinematography = 
 | editing = 
 | distributor = Sony Pictures (domestic)
 | released = October 31, 1980
 | runtime = 95 minutes
 | country = Canada
 | language = English
 | budget = 
 | gross = 
 | preceded_by = 
 | followed_by = 
 | image= Touched by Love VideoCover.png
}}
 1980 drama film about a therapist who tries a novel approach with a girl afflicted with cerebral palsy; she has her charge become a pen pal with the girls favorite singer, Elvis Presley. The film was based on the real-life reminiscences of Lena Canada.
 Razzie for Worst Actress for her performance. Razzie nomination - Hesper Anderson for the worst screenplay.

==Cast==
*Diane Lane as Karen
*Deborah Raffin as Lena Canada
*Michael Learned as Dr. Bell
*John Amos as Tony
*Cristina Raines as Amy
*Mary Wickes as Margaret
*Clu Gulager as Don Fielder

== External links ==
*  
*  

 
 
 
 
 
 
 