Violins at the Ball
 
{{Infobox film
| name           = Violins at the Ball
| image          = Les violons du bal.jpg
| caption        = Film poster
| director       = Michel Drach
| producer       =
| writer         = Michel Drach
| narrator       =
| starring       = Jean-Louis Trintignant
| music          =
| cinematography = William Lubtchansky
| editing        =
| distributor    =
| released       =  
| runtime        = 110 minutes
| country        = France
| language       = French
| budget         =
}}
 Best Actress.   

==Cast==
* Jean-Louis Trintignant – Lui (Michel)
* Marie-José Nat – Elle (La femme et la mère de Michel) / Michels wife
* Gabrielle Doulcet – La grand-mère
* David Drach – Lenfant
* Nathalie Roussel – La soeur de Michel / Michels Sister
* Christian Rist – Le frère de Michel et le contestataire / Michels Brother
* Yves Afonso – Le cameraman
* Yvon Yak – Le premier producteur
* Noëlle Leiris – La comtesse
* Luce Fabiole – La boulangère
* Malvina Penne – La réfugiée rousse
* Paul Le Person – Le premier passeur
* Guy Saint-Jean – Le second passeur
* François Leccia – Le jeune passeur
* Guido Alberti – Le producteur italien
* Hella Petri – La femme du producteur

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 