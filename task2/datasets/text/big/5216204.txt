The Bakery Girl of Monceau
{{Infobox film
| name           = The Bakery Girl of Monceau
| image          = Bakery girl monceau VDV 343.jpg
| caption        = DVD cover
| writer         = Éric Rohmer
| starring       = Barbet Schroeder Claudine Soubrier Michèle Girardon
| director       = Éric Rohmer
| cinematography =  
| producer       = Georges Derocles Barbet Schroeder
| distributor    =
| released       =  
| runtime        = 23 minutes
| language       = French
| budget         =
}}
 French title is La Boulangère de Monceau. The film was the first of Rohmers Six Moral Tales (Contes moraux), which consisted of 2 shorts and 4 feature films.

== Cast ==

* Barbet Schroeder:  young man
* Bertrand Tavernier: young mans voiceover narration
* Claudine Soubrier: Jacqueline
* Michèle Girardon: Sylvie
* Fred Junk: Schmidt
* Michel Mardore: Client

== Plot ==

...a man hesitates between two women...

The main character is played by Barbet Schroeder, but was dubbed by Bertrand Tavernier, whose voice Rohmer judged more appropriate for the very literary voice-over.

The structure of the film is about a man who sees and falls in love with a woman he passes by in the street, but he doesnt know how to talk to her. When he finally speaks to her, she disappears and he begins a daily search for her in the area that he has seen her in. During his search, he begins a habit of stopping in a bakery to buy a snack. Over time, the "girl" in the bakery becomes interested in him and he starts flirting with her, and, eventually, convinces her to go on a date with him. Before the date begins, however, he runs into the woman that he was originally searching for and is forced to choose.

== Narrative structure ==

The narrative structure of The Girl at the Monceau Bakery and the other five "Moral Tales" begins with the main character (a man), who is committed to a woman, meeting and being tempted by a second woman, but renouncing her for the first woman.

== Meaning of "Moral" ==

According to Rohmer:
 
My intention was not to film raw events, but the narrative that someone makes of them.  The story, the choice of facts, their organization... not the treatment that I could have made them submit to. One of the reasons that these Tales are called "Moral" is that physical actions are almost completely absent: everything happens in the head of the narrator.
 

Most of the film is told through the narrator.

Using the word "moral" does not mean that there is a moral in the story.  According to Rohmer:  
So Contes Moraux doesnt really mean that theres a moral contained in them, even though there might be one and all the characters in these films act according to certain moral ideas that are fairly clearly worked out. 
 

Also, Rohmer said:
 
They are films which a particular feeling is analyzed and where even the characters themselves analyze their feelings and are very introspective. Thats what Conte Moral means.
 

== Themes ==

* Guilt - That all are guilty and none are completely innocent.
* Love, desire, and free will - This is marked by the conflict within the protagonist. The conflict is the debate of being in anothers arms.  They "prefer to emphasize the possibility of choice rather than the activity of it."
* "Courtly Love" - According to James Monaco, there is an "inborn suffering" for the protagonist caused by "excessive meditation upon the beauty of the opposite sex."  It is a "love based in idleness."

== Setting ==

In each of the Six Moral Tales, Rohmer only filmed during the time and in the place that the film was set.  There was no use of sets.

== Sound ==

There is no nondiegetic music, only what is played in the background as part of the setting (i.e., cars, people walking by, music playing in the background at a party).  Also, there is an emphasis on dialogue and frequent use of voice-over narration.  The Girl at the Monceau Bakery contains no music, and the only sound that interrupts the sounds in the background is that of the narrator.

== External links ==

*  

*  

*  

== References ==
 

 

 

 
 
 
 
 
 
 