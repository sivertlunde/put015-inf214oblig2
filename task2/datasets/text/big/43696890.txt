Kuntilanak 3
 
{{Infobox film name = Vishwas Fincap image = K3 poster.jpg image_size = 200px caption =  director = Rizal Mantovani producer = Raam Punjabi writing = Ve Handojo   Rizal Mantovani starring = Julie Estelle Imelda Therinne Laura Antoinette Mandala Shoji Reza Pahlevi Ida Iasha Irene Racik Salamun Cindy Valerie Robby Kolbe Laudya Cynthia Bella distributor = MVP Pictures released =   runtime = 90 minutes language = Bahasa Indonesia country = Indonesia budget = }}

Kuntilanak 3 (English: The Chanting 3) is an Indonesian horror film directed by Rizal Mantovani. The film is the third in a trilogy, preceded by Kuntilanak (film)|Kuntilanak and Kuntilanak 2. The film stars Julie Estelle, Imelda Therinne and Laudya Cynthia Bella. The film was released on March 13, 2008.

== Cast ==
{{columns-list|3|
* Julie Estelle as Samantha
* Imelda Therinne as Asti
* Laura Antoinette as Petra
* Mandala Shoji as Darwin
* Reza Pahlevi as Herman
* Ida Iasha as Mega N. Widjoko
* Irene Racik Salamun as Mrs. Putri
* Cindy Valerie as Yenny
* Robby Kolbe as Rimson
* Laudya Cynthia Bella as Stella
}}
== External links ==
*   
*  
*  
*   
*  

 
 
 
 
 
 
 
 