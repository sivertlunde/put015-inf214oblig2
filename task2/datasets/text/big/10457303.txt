The Conformist (film)
{{Infobox film
| name           = The Conformist (Il conformista)
| image          = Original movie poster for the film The Conformist.jpg
| caption        = Original Italian poster
| director       = Bernardo Bertolucci
| producer       = Giovanni Bertolucci Maurizio Lodi-Fe
| story          = Alberto Moravia
| screenplay     = Bernardo Bertolucci
| studio = Mars Film Marianne Productions Maran Film
| starring       = Jean-Louis Trintignant Stefania Sandrelli Dominique Sanda Enzo Tarascio Gastone Moschin
| music          = Georges Delerue
| cinematography = Vittorio Storaro
| editing        = Franco Arcalli
| distributor    = Paramount Pictures
| released       =  
| runtime        = 111 minutes
| country        = Italy France West Germany Italian French French
}} political drama West German film companies.

Bertolucci makes use of the 1930s art and decor associated with the   drawing rooms and the huge halls of the ruling elite. 

==Plot==
The film opens with Marcello Clerici (Jean-Louis Trintignant) in Paris finalizing preparations to assassinate his former college professor, Luca Quadri (Enzo Tarascio). It frequently returns to the interior of a car driven by Manganiello (Gastone Moschin) as the two of them pursue the professor and his wife.
 blind friend, his plans to marry, his somewhat awkward attempts to join the Fascist secret police, and his visits to his morphine-addicted mother at the familys decaying villa and his unhinged father at an insane asylum.

In one of these flashbacks we see him as a boy during World War I, who finds himself isolated from society by his familys wealth. He is socially humiliated by his schoolmates until he is rescued by chauffeur Lino (Pierre Clémenti). Lino offers to show him a pistol and then makes sexual advances towards Marcello, which he partially responds to before grabbing the pistol and shooting wildly into the walls and into Lino, then flees from the scene of what he assumes is a murder.
 confession in order for her parents to allow them to marry, even though he is an atheist. He agrees, and in confession admits to the priest to having committed many sins, including his homosexual experience with Lino, the consequent murder, premarital sex, and his absence of guilt for these sins. Marcello admits he thinks little of his new wife but craves the normality that a traditional marriage with children will bring. The priest is shocked &mdash; apparently more by Marcellos homosexuality than the murder &mdash; but quickly absolves Marcello once he hears that he is currently working for the Fascist secret police.

Now married, Marcello finds himself ordered to assassinate his old friend and teacher, Professor Quadri, an outspoken anti-Fascist intellectual now living in exile in France. Using his marriage as a convenient cover he takes Giulia on their honeymoon to Paris where he can carry out the mission.

While visiting Quadri he falls in love with Anna - the professors young wife - and actively pursues her. Although it becomes clear that she and her husband are aware of Marcellos Fascist sympathies and the danger he presents to them she seems to accept his advances, as well as forming a close attachment to Giulia, toward whom she appears to make sexual advances as well, possibly for Marcellos benefit. Giulia and Anna dress extravagantly and go to a dance hall with their husbands where Marcellos commitment to the Fascists is tested by Quadri. Manganiello is also at the dance hall, having been pursuing Marcello for some time and is doubtful of his intentions. Marcello returns the gun that he has been given and secretly gives Manganiello the location of Quadris country house where the couple plan to go the following day.

Even though Marcello has warned Anna not to go to the country with her husband and has apparently persuaded her that she should leave her husband and stay with him she does make the car journey. On a deserted woodland road Fascist agents conspire to stop Quadris car with a false accident. When he attempts to help a stricken driver he is attacked and stabbed to death by several men who appear from the woods. Anna sees her husband murdered and realising the danger to herself runs to Marcellos car for help. When Anna sees that the passenger in the rear of the car is Marcello, she begins to scream uncontrollably, then runs off into the woods. Marcello merely watches without emotion as she is pursued through the woods and finally shot to death.

The ending of the film takes place in 1943 during the fall of Benito Mussolini and the fascist dictatorship, Marcello now has a small child and is apparently settled in a conventional lifestyle. He is called by Italo, his blind friend and former Fascist, and asked to meet on the streets. While walking with Italo, they overhear a conversation between two men and Marcello recognizes one of them as Lino, who attempted to seduce him when he was a boy. Marcello publicly denounces Lino as a homosexual, Fascist, and for participating in the murder of Professor Quadri and his wife. While in this frenzy he also denounces his friend Italo. As a crowd sweeps past taking Italo with them Marcello is left alone, unaccepted by the people of the new partisan political movement, and having spurned his former friend. He sits near a small fire and stares intently behind him at the young man Lino was previously talking to.

==Cast==
* Jean-Louis Trintignant as Marcello Clerici
* Stefania Sandrelli as Giulia
* Gastone Moschin as Manganiello
* Enzo Tarascio as Professor Quadri
* Fosco Giachetti as Il colonnello
* José Quaglio as Italo
* Dominique Sanda as Anna Quadri
* Pierre Clémenti as Lino
* Yvonne Sanson as Madre di Giulia
* Giuseppe Addobbati as Padre di Marcello
* Christian Aligny as Raoul
* Carlo Gaddi as Hired Killer
* Umberto Silvestri as Hired Killer
* Furio Pellerani as Hired Killer

==Themes==
 
 bureaucrat dehumanised dysfunctional upper childhood sexual trauma. He accepts an assignment from Benito Mussolinis secret police to assassinate his former mentor, living in exile in Paris. In Trintignants characterization, Clerici is willing to sacrifice his values in the interests of building a supposedly "normal life." 

According to the political philosopher Takis Fotopoulos, The Conformist (as well as Rhinoceros (play)|Rhinoceros by Eugène Ionesco|Ionesco) is "a beautiful portrait of this psychological need to conform and be Norm (sociology)|normal at the social level, in general, and the political level, in particular." 

In 2013, Interiors, an online journal concerned with the relationship between architecture and film, released an issue that discussed how space is used in a scene that takes place on the Palazzo dei Congressi. The issue highlights the use of architecture in the film, pointing out that in order to understand the film itself, it’s essential to understand the history of the EUR district in Rome and its deep ties with fascism. 

==Production==
The filming locations included Gare dOrsay and Paris, France; Ponte SantAngelo|Sant Angelo Bridge and the Colosseum, both in Rome. 
 German films of the 1920s and 1930s, such as in Leni Riefenstahls Triumph of the Will and Fritz Langs Metropolis (1927 film)|Metropolis. 

The drama was influential to other filmmakers: the image of blowing leaves in The Conformist, for example, influenced a very similar scene in The Godfather, Part II (1974) by Francis Ford Coppola. 

==Distribution==
The film premiered at the 20th Berlin International Film Festival on 1 July 1970,  where it competed for the Golden Bear. However, due to the row over the participation of Michael Verhoevens anti-war film o.k. (film)|o.k., the festival was closed down three days later and no prizes were awarded.   

The film opened simultaneously in Italy and the United States on 22 October 1970. The first American release of the film was trimmed by five minutes compared to the Italian release; the missing scene features a group of blind people having a dance. They were restored in the 1996 reissue. 
 Paramount Home Entertainment on 5 December 2006. The DVD includes: the original theatrical version (runtime 111 minutes); The Rise of The Conformist: The Story, the Cast featurette; Shadow and Light: Filming The Conformist featurette; The Conformist: Breaking New Ground featurette.

In 2011 the Cineteca di Bologna commissioned a 2K restoration of The Conformist, supervised by Storaro himself (and approved by Bertolucci), 
   which screened in the Cannes Classics series on May 11, 2011, in conjunction with the presentation of an honorary Palme dOr to Bertolucci.  
   The restoration was done by Minerva Pictures-Rarovideo USA and LImmagine Ritrovata (laboratory of the Cineteca di Bologna).   In 2014 the digital restoration was released theatrically by Kino Lorber in North America, and is set for a Blu-ray release by Rarovideo USA on November 25, 2014. 

==Critical response==
Vincent Canby, film critic for The New York Times, liked Bertoluccis screenplay and his directorial effort, and wrote, "Bernardo Bertolucci...has at last made a very middle-class, almost conventional movie that turns out to be one of the elegant surprises of the current New York Film Festival...It is also apparent in Bertoluccis cinematic style, which is so rich, poetic, and baroque that it is simply incapable of meaning only what it says...The movie is perfectly cast, from Trintignant and on down, including Pierre Clementi, who appears briefly as the wicked young man who makes a play for the young Marcello. The Conformist is flawed, perhaps, but those very flaws may make it Bertoluccis first commercially popular film, at least in Europe where there always seems to be a market for intelligent, upper middle-class decadence." 

Recently, critic James Berardinelli wrote a review and heralded the films look. He wrote, "Storaro and Bertolucci have fashioned a visual masterpiece in The Conformist, with some of the best use of light and shadow ever in a motion picture. This isnt just photography, its art &mdash; powerful, beautiful, and effective. Theres a scene in the woods, with sunlight streaming between trees, thats breathtaking to behold &mdash; and all the more stunning because of the brutal events that take place before this background." 

More recently, Kevin Thomas, Los Angeles Times staff writer, said, "In this dazzling film, Bertolucci manages to combine the bravura style of Fellini, the acute sense of period of Visconti and the fervent political commitment of Elio Petri — and, better still, a lack of self-indulgence...The Conformist," which memorably costars Dominique Sanda as a sexually ambiguous beauty, is not merely an indictment of fascism — with some swipes at ecclesiastical hypocrisy as well — but also a profound personal tragedy. 

The review aggregator Rotten Tomatoes reported that 100 percent of critics gave the film a positive review, based on thirty-nine reviews. 

==Awards==
Wins Berlin Film Festival: Interfilm Award - Recommendation and Journalists Special Award, Bernardo Bertolucci; 1970.
* David di Donatello Awards: David; Best Film, Maurizio Lodi-Fe; 1971.
*  ; 1972.
* National Society of Film Critics Awards: NSFC Award; Best Cinematography, Vittorio Storaro; Best Director, Bernardo Bertolucci; 1972.
* Satellite Awards: Satellite Award: Best Classic DVD; 2006.

Nominations
* Berlin Film Festival: Golden Berlin Bear, Bernardo Bertolucci; 1970.
*  , Bernardo Bertolucci; 1972.
* Golden Globes: Golden Globe; Best Foreign-Language Foreign Film Italy; 1972.

==Discography==
The CD soundtrack composed by Georges Delerue is available on Music Box Records label.

==References==
 

==External links==
 
 
*  
*  
*  
*  
*   at RAI International
*   (this scene is reviewed in documentary Visions of Light)
*   at DBCult Film Institute (Italian)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 