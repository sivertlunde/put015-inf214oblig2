The Mercenary (film)
 
{{Infobox film
| name           = The Mercenary
| image          = The Mercenary.jpg
| caption        = Spanish poster for The Mercenary
| director       = Sergio Corbucci
| producer       = Alberto Grimaldi Francesco Merli
| writer         = Giorgio Arlorio Adriano Bolzoni Sergio Corbucci Franco Solinas Sergio Spina Luciano Vincenzoni
| starring       = Franco Nero Jack Palance Tony Musante Giovanna Ralli
| music          = Ennio Morricone Bruno Nicolai
| cinematography = Alejandro Ulloa
| editing        = Eugenio Alabiso
| studio         = Produzioni Europee Associati
| distributor    = United Artists
| released       =  
| runtime        = 110 minutes
| country        = Italy
| language       = Italian
}}
The Mercenary ( ), also known as A Professional Gun, is a 1968 spaghetti western film directed by Sergio Corbucci. The film stars Franco Nero, Jack Palance, Tony Musante and Giovanna Ralli, and features a musical score by Ennio Morricone and Bruno Nicolai. The film takes place during the Mexican Revolution and is a well-known example of the "Zapata Western" subgenre.

The Mercenary was released the same year as Corbuccis more popular western,  . 

==Plot== Mexican peasant, Polish mercenary, arrives in Mexico and makes a deal with two Garcia brothers to take their silver safely across the border. Curly (Palance) sees the three men talking and tracks down the brothers to find out what they hired Kowalski for, after which Curly kills the two.

When Kowalski arrives at a mine to meet one of the Garcia brothers, he finds the man along with many others dead and meets Paco and his revolutionaries instead. Soon after, Colonel Alfonso Garcias (Eduardo Fajardo) Mexican army troops attack. Kowalski agrees to help Paco fight them, but only for money as he is a professional soldier. With the help of Kowalski and his machine gun, the revolutionaries defeat the army. Kowalski then leaves the group, but gets ambushed by Curly. Soon Pacos group arrives and kills Curlys men. Although Curly swears to revenge, they let him go after stripping him of his clothes. Paco then hires Kowalski to teach him how to lead a revolution.

The revolutionaries travel from town to town robbing money, guns and horses from the army. They also release a prisoner named Columba (Ralli) and she joins the group. After Paco stays in one town to protect the people despite Kowalski telling him that they can not match the army sent to capture them, Kowalski leaves the group again. Paco and his men later admit their defeat and go after Kowalski. Kowalskis price has doubled now, but the two make another deal. After the revolutionaries take over a town by defeating a whole regiment, Paco betrays Kowalski and takes him prisoner, confiscates all the money he has paid him, and marries Columba. When General Garcias army, along with Curly, attack them, Paco realizes he can not manage the situation on his own and decides to set Kowalski free, but ends up locked up himself while Kowalski escapes. Columba frees Paco and the two also manage to escape before Curly finds them.

After six months, Kowalski attends a circus performance and notices that Paco managed to get away after all, as he is one of the circus clown|clowns. After the performance is over, Curly enters the arena and his men capture Paco. Kowalski then shoots Curlys men and gives him and Paco both a rifle and a bullet, so that the two can have a fair duel. After Paco kills Curly, Kowalski takes him as a prisoner and heads to the headquarters of the 51st Regiment to collect the reward offered for his head. Columba sees this, manages to get before them to the headquarters and then pretends that she wants to betray Paco by telling where the two are. 

When the army troops find the pair, Kowalski also finds himself arrested as there is now even a bigger reward for his head. The two are then sentenced to death by firing squad. However, Columba now executes her plan and with the help of two machine guns, Paco, Kowalski and Columba manage to escape. Kowalski suggests Paco that they team up to make a lot of money, but Paco claims that his dream is in Mexico referring to the revolution. The two part ways, and we see Garcia and four soldiers about to ambush Paco. Kowalski cuts them all down with his rifle from a nearby hillside, and then yells to Paco to "keep dreaming, but with your eyes open."

==Cast==
* Franco Nero - Sergej Kowalski
* Tony Musante - Paco Roman
* Jack Palance - Ricciolo (Curly)
* Giovanna Ralli - Columba
* Eduardo Fajardo - Alfonso Garcia
* Franco Giacobini - Pepote Alvaro De Luna - Ramon
* Raf Baldassarre - Mateo
* Remo De Angelis - Hudo

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 