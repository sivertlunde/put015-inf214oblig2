Modhi Vilayadu
{{Infobox film
| name           = Modhi Vilayadu
| image          = Modhi Vilayadu Poster.jpg
| alt            =  
| caption        =  Saran
| producer       = Murali Manohar Saran
| Santhanam Mayilsamy
| music          = Colonial Cousins
| cinematography = A. D. Karun
| editing        = V. T. Vijayan
| studio         = 
| distributor    = Mediaone Global Entertainment Ltd. Gemini Industries & Imaging Private Ltd.
| released       =  
| runtime        = 
| country        = India Malaysia 
| language       = Tamil
| budget         = 
| gross          = 
}}
Modhi Vilayadu is a Tamil language film released on 24 July 2009. It stars Vinay Rai and Kajal Aggarwal in the lead roles. The film opened to mixed reviews with Indiaglitz.com rating it highly while Behindwoods said "The game lacks excitement". The film was rated with mixed reviews and fared poorly at the box office

The Films plot involving heroine is based on the korean movie 100 Days with Mr. Arrogant.

==Plot==
Rajan Vasudev (Kalabhavan Mani) is an intolerable tycoon who runs the OPM Group of companies spread across the globe. Uday (Vinay Rai) is his only son, a spoilt kid who drives a Ferrari and hangs out with his constant companion Madan (Yuva) and Kaduku (Santhanam) along with a bodyguard (Alse Ram).

Uday meets Eashwari (Kajal Aggarwal) a student and in a funny turn of events makes her his maid servant. Soon Madan falls in love with Eashwari who has fallen for Uday.

An assassin is hired by rivals of Rajan Vasudev to knock off Uday the only heir to the business empire. But in a bizarre twist, the killer accidentally kills Madan, and Rajan is heartbroken as we find that it was actually Madan who was his real son, while Uday was just a ‘Benami’.

A heart broken Rajan abandons Uday who is left in the streets overnight as he is not needed any longer. The rest of the plot is how Uday with the help of Chanakyan (VMC Haneefa), turns the table on his foster dad and gets back his wealth.

==Cast==
*Vinay Rai as Udhay Vasudev
*Kajal Aggarwal as Easwari Lakshmiram
*Kalabhavan Mani as Rajan Vasudev
*V. M. C. Haneefa as Chanakya Santhanam as Kadukku Yuva as Madhan
*Amit Dhawan as Iqbal
*Mayilsamy
*Raviprakash as Lakshmiram

== Music ==
The film has music of Hariharan - Lesle for the first time in Tamil. The audio launch of this movie held on April 28. 
=== Soundtrack ===

{| class="wikitable"
|-
! Song title !! Singers !! Length (m:ss) !! Description 
|- Hariharan || 4:37 || 
|-
| Latcham Varthaigal || Ranjani || 3:08 || 
|- Shaan || 3:26 || 
|- Lesle Lewis & Achu || 3:44 || 
|- Lesle Lewis || 4:28 || 
|-
| Paathi Kadhal || Sunitha Sarathy, Bombay Jayashree || 3:44 || 
|}

==References==
 

==External links==

 

 
 
 
 
 