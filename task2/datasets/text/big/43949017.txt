Under the Red Sea
{{Infobox film
| name           = Under the Red Sea
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = 
| producer       = Sol Lesser Hans Hass
| writer         = Bill Park
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = Les Tremayne
| music          = Bert Grund
| cinematography = Hans Hass Lottie Berl
| editing        = Robert Leo RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =   }}
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Under the Red Sea is a 1952 American documentary film about the attempts of Austrian marine biologist Hans Hass to record the sounds of marine animals in the Red Sea. It was co-produced by Thalia Productions and RKO Radio Pictures, which released the film on October 2, 1952. The film was narrated by Les Tremayne.

==References==
 

 
 
 
 
 

 