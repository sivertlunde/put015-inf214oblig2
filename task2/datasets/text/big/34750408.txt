Muna Moto
{{Infobox film
| name         = Muna Moto
| director     = Jean-Pierre Dikongué Pipa
| producer     = Cameroun Spectacles
| writer       = Jean-Pierre Dikongué Pipa
| starring     = Philippe Abia Arlette Din Beli Gisèle Dikongue-Pipa David Endene
| music        = Georges Anderson
| sound editor = Ambroise Ayongo
| photography  = JP Delazay, JL Léon
| editing      = Andrée Davanture
| distributor  = Marfilmes
| released     =  
| runtime      = 89 minutes
| country      = Cameroon Duala and Basaa
}}
 1975 drama film directed by Jean-Pierre Dikongué Pipa.

==Synopsis==
Ngando and Ndomé are in love. Ngando wishes to marry Ndomé but her family reminds him that the traditional dowry must be settled. Unfortunately, Ngando is poor and unable to fulfil the tradition. Ndomé is pregnant and bears his child. According to the village tradition, she must take a husband, at least one who can afford to pay the dowry. The villagers decide that Ndomé should marry Ngando’s uncle, who has already three sterile wives. In despair, the young man kidnaps his daughter upon the day of the traditional feast. An African Romeo and Juliet story.

The film is part of the Les Étalons de Yennega 1972-2005 collection, launched by   and  .

==Festivals==
*   (2011)
*   (2008)
* Official Selection Venice Film Festival, Italy (1975)
* São Paulo International Film Festival, Brazil

==Awards== First prize (Étalon de Yennenga) and First Prize International Catholic Organization of Cinema at FESPACO - Panafrican Film and Television Festival of Ouagadougou, Burkina Faso (1976)
* First Prize Festival International du Film de lEnsemble Francophone, Switzerland (1975)
* Silver Tanit at Journées cinématographiques de Carthage, Tunis (1976)
* George Sadoul Prize, France (1975)

==See also==
*  
* 
*  in Cinefrance

==External links==
*http://www.imdb.com/title/tt0073417/
*  in Africultures.
*  in 20.mai


 
 

 