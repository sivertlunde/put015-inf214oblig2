Exists (film)
 
{{Infobox film
| name           = Exists
| image          = Exists_Movie_Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      =  Eduardo Sánchez
| producer       =  
| writer         = Jamie Nash
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       =  
| music          = Nima Fakhrara
| cinematography = John Rutland
| editing        =   
| studio         =  Lionsgate 
| released       =    
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Eduardo Sánchez.  The film had its world premiere on March 7, 2014 at South by Southwest and stars Chris Osborn and Samuel Davis as two brothers hunting for the legendary Sasquatch.   Following the darker psychological tone of Sánchezs previous film, Lovely Molly, the film returns to the Creature Features|creature-feature horror of Altered (film)|Altered, also written by Jamie Nash. 

==Plot==
Brothers Brian and Matt, Matts girlfriend Dora, and their friends Todd and Elizabeth are on their way to the brothers uncle Bobs cabin for a fun filled weekend, with Brian filming all the escapades with hand held cameras and go pros.  After stopping off to buy some fireworks, they continue on their way as day turns to night.  As they are driving along, they suddenly hit something and stopped to investigate.  With only a tail light busted, they begin to search for whatever they hit, assuming it was a deer.  Suddenly they hear loud, almost screaming noises coming from the nearby woods, and quickly decide to get back in the car and push on.  

Reaching a tree that has been knocked across the road, they continued on foot and finally reached the cabin, that has seen better days.  After finding a wild pig in the cabin, the group decided to spend the night in the car, where Brian and Matt again hear the loud screaming noises, to which Brian questions whether or not it was something their uncle saw many years before but Matt quickly silences his brother, telling him he doesnt want to hear anything about it.

That morning, Brian finds the front passenger side a little damaged, but also finds blood and a tuft of hair/fur.  The group then heads down to a lake and have some fun, filming themselves the entire time.  Cut back to camp, Brian follows Todd and Elizabeth into the woods, filming them about to engage in sex, when suddenly Brian films a big, dark bipedal figure running along a nearby ridge near Todd and Elizabeth, to which he screams out to them, but in turn only angers them, not believing his claims.  He then heads off into the woods alone after arguing with Matt, talking about his uncles story and because of whatever he saw, never returned to his cabin.  He then hears noises nearby and is determined to get whatever is out there on film and puts several Go Pro cameras around the cabin to further assist him.

That night, Brian who fell asleep outside in a hammock, is suddenly awoken by noises that are close by.  Upon investigating, he is scared by Matt and Todd, who shoot him up with paintball guns.  Suddenly, loud scream like roars are heard, which scare the men back into the cabin. Everyone is clearly frightened by whatever is outside, but try to remain calm, even when Matt tells them his theory that it is in fact Bigfoot.  The Go Pro camera outside reveal a large figure approaching the cabin and ascending the stairs onto the porch. The group are even further frightened when whatever is outside begins pushing on the walls and doors.  When silence falls, Matt goes to a window to see if he can see anything, and when he turns on the night vision on his camera, sees a large hairy creature looking back before roaring out and disappearing from view.

That morning, they decide to leave.  Upon reaching the car, they find it completely destroyed and quickly return to the cabin.  Dora tells them that they should just wait until Brian and Matts uncle come and they can leave with him, but the brothers reveal that their uncle doesnt allow anyone to come out here, so they stole the key and in fact their uncle has no idea they are out here, which angers the others.  Matt decides to take a bike and ride out until he can get reception on his cell phone and call for help.  When he is finally able to reach his uncle, he sees the large dark figure standing in the middle of the path, blocking his way through and flees, heading back to camp with the creature giving chase.  Matt hits a downed tree and is thrown from his bike, fracturing his leg.  He continues to try and get away, but is quickly knocked to the ground by the creature which approaches him before cutting to black.

Back at the cabin, the others move the furniture to block the windows and doors.  They then find a latch which pulls up to reveal a basement access where they find a gun.  They then are shown outside the cabin, waiting for Brians return when they hear the creature nearby hooting and hollering, then suddenly hurls Matts destroyed bike onto the porch, causing them to retreat inside.  When they see nothing out there, Elizabeth says that it is waiting for nightfall.  Dora is distraught over Matt when they suddenly hear the creature outside, and suddenly the generator goes out.  Suddenly the creature breaks through a window in an attempt to get inside and Todd shoots it before the group retreat to a bedroom, blocking the door.  They hear the creature inside wreaking the cabin, and suddenly begins trying to get into the bedroom.  Todd shoots at the door, and the creature retreats. Suddenly it beaks through another window, grabbing Elizabeth by the shoulder and slamming her several times into the wall before lifting her off the ground and throwing her across the room to the floor.  The group quickly retreat and hide in the basement. They try and stay quiet but the creature again finds them and Todd shoots it again, causing it to retreat.  

In the morning, it is revealed that Elizabeth did not survive the night, and the others leave on foot. Brian talks the others into taking a short cut to save time, and take shelter under a small foot bridge when night falls. The creature is heard nearby, and Todd enraged and grieving over Elizabeth, begins screaming and shooting blindly, wanting to kill the creature for killing his friends.  The creature begins hurling large rocks at the group and they run.  A short time later, they hear Matt screaming somewhere nearby and go searching and find a tunnel.  Brian goes in and finds the creatures den, along with Matt.  The creature appears and Brian shoots it then quickly drags his brother out to the others.  They head to a abandoned trailer that they passed nearby and receive a call from their uncle, who is now at the cabin.  Todd heads out with the fireworks and sets them off in an attempt to alert their uncle to their location when suddenly the creature appears out of the smoke and chases Todd back to the trailer. Todd trips and the creature catches him, hurling him against and tree and then the trailer, killing him.  The creature then begins to push the camper towards the edge of a hill and sends it over, with Brian, Matt and Dora inside.

When Brian finally comes to, he discovers that Matt is dead and upon finding Dora, she dies as well as the creature is shown jumping from the top of the hill onto the trailer.  Brian flees out of a broken window and the creature gives quick chase.  Brian comes to the edge of another hill and quickly jumps as the creature approaches. Upon his landing, he fakes dead as the creature quickly descends upon him, roaring in his face, pushing him back and forth and hitting the ground near his head. The creature then grabs his leg and drags him off.

Sometime later, Brian jumps up gasping for air as it is revealed he is lying next to the bodies of Todd, Matt, Dora and Elizabeth. In his panic, he turns and sees a small dark figure lying in a shallow hole.  Upon investigating, it is shown to be a small bigfoot. The large one appears and knocks him into the hole, pushing his head closer and closer. He realizes that must have been what they hit the night they arrived at the cabin and hysterically apologizes for what they had done.

Suddenly a shot is heard and the creature flees as Brians uncle calls out.  He grabs Brian and they quickly flee, Brian apologizing for not listening to him.  As they near his uncles truck, the bigfoot appears and tackles Uncle Bob to the ground, seemingly killing him.  Brian picks up his uncles gun and runs as the creature gives chase.  He turns and points at the creature, begging it to stop.  The creature stops a few feet away from him, breathing heavily as Brian continues to apologize for killing its young.  Brian lays down the gun and turns and sits on the small bridge, saying hes done and this is his final video awaiting the creature to finish him off.  The creature, after a while, just turns and disappears into the woods, leaving Brian on the bridge as it begins to rain as he lays the camera down facing his uncles truck.

==Cast==
*Chris Osborn as Brian Dora Madison Burge as Dora
*Roger Edwards as Todd
*Denise Williamson as Elizabeth
*Samuel Davis as Matt
*Brian Steele as Sasquatch
*Jeff Schwan as Uncle Bob (as J.P. Schwan)
*George P. Gakoumis Jr. as Firework Salesman (as George Gakoumis)
*Stefanie Sanchez as 911 Operator

==Release==
Exists premiered on 7 March 2014 as part of the South by Southwest Film Festival  and was released on February 3, 2015.  Entertainment One set the United Kingdom release, on DVD and Blu-ray for the April 6, 2015. 

==Reception==
 
Critical reception for the film has been mostly negative.

Fearnet praised Exists for its editing and score, as they felt that it would help make the movie appeal to people who would not normally like found footage films.  The Austin Chronicle gave a predominantly positive review and commented that while the film held several similarities to Sánchezs The Blair Witch Project, "he evades all those bear traps, returning tension and pathos to the monster movie." 
It currently has a 35% "Rotten" on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  
*   
 
 
 
 

 