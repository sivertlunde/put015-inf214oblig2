Shorts (2009 film)
{{Infobox film
| name           = Shorts
| image          = Shortsposter.jpg
| image_size     =
| caption        = Theatrical release poster
| alt            =
| director       = Robert Rodriguez Elizabeth Avellán
| writer         = Robert Rodriguez
| starring       = Jon Cryer William H. Macy Leslie Mann James Spader Jimmy Bennett Kat Dennings Jolie Vanier
| narrator       = Jimmy Bennett
| music          = George Oldziey Robert Rodriguez Carl Thiel
| cinematography = Robert Rodriguez
| editing        = Ethan Maniquis Robert Rodriguez Imagenation Abu Dhabi Media Rights Capital Troublemaker Studios Lin Pictures
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 89 minutes  
| country        = United States United Arab Emirates
| language       = English
| budget         = $20 million
| gross          = $29 million 
}}
 2009 3D family comedy comedy fantasy Imagenation Abu Dhabi, distributed by Warner Bros. Pictures and stars Jimmy Bennett, Jake Short, Kat Dennings, Trevor Gagnon, Devon Gearhart, Jolie Vanier, Rebel Rodriguez, Leo Howard, Leslie Mann, Jon Cryer, William H. Macy, and James Spader. It was released in the United States on August 21, 2009.  A Nintendo DS video game of the same name was announced on June 23, 2009, with a prospective July release date in advance of the movie. 

==Plot==
Before the main movie, a pair of twins (Cambell Westmoreland and Zoe Webb) decide to have a staring contest. The game throughout the course of the movie as a running gag. Both brother and sister lose because their mom snapped her fingers in front of their faces, causing both to blink at the same time.  As they wondered who won the game they began a new round, then the film begins. Toby "Toe" Thompson (Jimmy Bennett) begins telling the story from his point of view and explaining The Black Box Inc, where all the adults work. It is this company that creates the "Black Box", a small cube that can fill any technological need and which every adult at Black Falls works at. Toe goes to school only to be bullied by his worst enemy Helvetica "Hel" Black (Jolie Vanier) and her brother Cole (Devon Gearhart). Toe speculates in front of everyone that Hel loves him, which infuriates Hel. As Toe walks home Cole and his gang start throwing rocks at Toe, one of which is the rainbow colored wishing rock. Toe wishes for friends which come in the form of small aliens. Toe takes the aliens to school, which leads to a series of events ending with Toe and Hel falling from the schools roof with broken arms. The aliens leave, but Toe keeps the rock. One alien says Toe has friends all around, but he doesnt know it. The Wishing Rock appears for the first time in front of the Shorts House. The three brothers Loogie (Trevor Gagnon), Laser (Leo Howard), and Lug (Rebel Rodriguez) use the rock to wish for a giant fort. The brothers start an argument on who gets to wish for something and the rock ends up falling into the forts "canyon", which Loogie filled with snakes and alligators. The brothers escape with the rock and try to decide what to do with it. Lug and Loogie prefer to use it for fun, but Laser wants to use it wisely. To solve the dilemma, Loogie wishes that one of them was super smart but, as he was not more specific, the power is given to their nearby infant sister (portrayed by Bianca Rodriguez and voiced by Elizabeth Avellan) who convinces the boys to lose the rock into a pile of dirt, where Cole finds it the next day to throw it at Toe. Toes older sister Stacey (Kat Dennings) arrives to tutor "Nose" Noseworthy (Jake Short), Toes old germaphobic friend. Nose picks his nose against his fathers wishes and accidentally flicks the booger into his fathers radioactive work study. Toe and Loogie, now friends, arrive at Noses house in time to see Noses booger mutate into a giant monster. They leave as quickly as possible, but Toe gets caught by the booger. Nose rescues him by saying that if the booger mutant should eat his friend, Nose will eat the boogers friend (another booger), and the two run out of the house as soon as the booger mutant gives in to Noses commands. After helping Noses father (William H. Macy) capture the monster outside of the house, Toe and Loogie take the rock and leave. Mrs. and Mr. Thompson (Leslie Mann and Jon Cryer) are having trouble with their relationship, as Mr. Carbon Black (James Spader) forces them to compete in making a new sales idea for the black box. Mrs. Thompson goes to look at Toes phone, and finds the rock on the floor in his room. She takes the rock just as Toe wakes up. She and Mr. Thompson go to a Black Box Inc. costume party. While on the patio of Mr. Blacks house, she wishes that she and her husband were closer, due to the fact that earlier, Mr. Thompson texted her hugs and kisses, while literally standing right next to her. They become mutated together into a two-headed person and try to hide from Mr. Black. He finds them and invites them inside (thinking theyre wearing a costume). Meanwhile, Toe realizes that Mrs. Thompson took the rock and rushes to the costume party, despite Helvetica warning him against coming. Hel finds the rock as Toe arrives to try and explain its power. Toe is interrupted by Cole, who dumps him in the garbage. An infuriated Hel wishes Cole into a dung beetle before losing the rock. The rock is found by Mr. Black who, unaware of its power, accidentally wishes for all of the employees to go for each others throats. In the chaos, Toe retrieves the Rock and wishes everyone back to normal including his parents. Hel takes the stone, wishes off her casts and escapes before Toe throws the rock as far as he can, where it lands at the Noseworths house. After the events of episode 4, Toe and Loogie contemplate what to do with the rock. They are ambushed by the rest of the kids and they all fight over the rock until it winds up in the hands of Mr. Black. Mr. Black wishes that he was the most powerful thing in the world. He turns into a giant Black Box and the children team up to stop him. Soon, the infant tells them that the rock is starting to feel misused, as shown by the fact that the rock starts undoing all of their wishes. She also tells them the rock could destroy the Earth because of all of the petty wishes they make. Together they get rid of it and all of the events that happened because of the wishes disappear. Mr. and Mrs. Thompson state they plan to work together to Mr. Black who agrees. He later reverts the Black Box into the Green Box, suggesting it to be more environmental. Mr. Noseworthy and son lose their germaphobia, Mr. and Mrs. Thompson become closer, and last it is suggested that Hel and Toe will marry in the future. All the children become friends and walk off while stating they wished their story became a Hollywood movie, breaking the 4th wall and concluding the movie.

==Cast==
* Jon Cryer as Mr. Bill Thompson, Tobys father who works at Black Box, Inc.
* Jimmy Bennett as Toby "Toe" Thompson.
* Leslie Mann as Mrs. Jane Thompson, Tobys mother who also works at Black Box, Inc.
* Kat Dennings as Stacey Thompson, Tobys older sister. germophobe scientist who doesnt like Contamination.
* Jolie Vanier as Helvetica Black, a girl bully who secretly has a crush on Toby.
* Devon Gearhart as Colbert "Cole" Black, Carbons son and captain of the football team. Hes Helveticas brother who cares deeply for her and is the leader of his own gang which Helvetica sometimes leads.
* James Spader as Mr. Carbon Black, a greedy man who invented the black box and founded the Black Box Community.
* Trevor Gagnon as Loogie Short, a boy who found the wishing rock. When he had the wishing rock he wished for unreasonable stuff. He flirted with Tobys older sister throughout the movie.
* Rebel Rodriguez as Lug Short, Loogies older brother. He is the most video game obsessed and easily angered of the three Short brothers.
* Leo Howard as Laser Short, Loogies older brother. He seems to be the smartest of the three Short brothers.
* Bianca Rodriguez as the Short brothers infant sister, simply referred to as "(the) Baby". She is voiced by Elizabeth Avellan
* Jake Short as "Nose" Noseworthy, a germophobe scientist like his father.
* Racer Rodriguez as Bully #1, a member of Coles gang.
* Rocket Rodriguez as Bully #2, also a member of Coles gang.
* Alejandro Rose-Garcia as John, Staceys boyfriend.
* Cambell Westmoreland as Blinker #1
* Zoe Webb as Blinker #2

==Production== Imagenation Abu Dhabi, Media Rights Capital and Troublemaker Studios are studios for the film, it stars Jon Cryer, Jimmy Bennett, Leslie Mann, Kat Dennings, William H. Macy, Jolie Vanier, Devon Gearhart, James Spader, Trevor Gagnon, Rebel Rodriguez, Leo Howard and Jake Short.

==Release==
The film was released in cinemas in 21 August 2009 in the USA and it was released on DVD in November 24, 2009 by Warner Home Video.

==Reception==
Reviews for the film have been mixed. The film holds a rating of 45% on  , and Julie & Julia. Overall, the film was a minor success at the box office, grossing about $20.9 million in the United States and Canada, and about $28.9 million worldwide. It is considered a financial disappointment compared to other films that Robert Rodriguez has directed, making only a third of what the Spy Kids films had made.

==Awards==
{| class="wikitable sortable"
|+ List of awards and nominations
! Award
! Category
! Nominee
! Result
|-
| rowspan="2" | Young Artist Awards 2010
| Best Performance in a Feature Film - Young Ensemble Cast || Jimmy Bennett, Jake Short, Devon Gearhart, Leo Howard, Jolie Vanier, Trevor Gagnon ||  
|-
| Best Performance in a Feature Film - Leading Young Actress || Jolie Vanier ||  
|}

==Video game==
A Nintendo DS video game of the same name was announced on June 23, 2009, with a prospective July release date in advance of the movie.

==Songs==
* Spy Ballet - Robert Rodriguez
* Summer Never Ends - Jimmy Bennett

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 