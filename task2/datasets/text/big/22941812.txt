Furia (film)
 
{{Infobox Film
| name           = Furia
| image          = Furia.jpg
| image_size     = 
| caption        = French film poster for Furia
| director       = Alexandre Aja
| producer       = Alexandre Arcady 
| writer         = Alexandre Aja Grégory Levasseur
| based on       = short story Graffiti by Julio Cortázar   
| narrator       = 
| starring       = 
| music          = Brian May    
| cinematography = Gerry Fisher  
| editing        = Pascale Fenouillet
| studio         = Alexandre Films  Canal+  France 2 
| distributor    = Bac Films  1999
| runtime        = 90 min
| country        = France
| language       = French
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1999 French romantic drama film directed by Alexandre Aja, who co-wrote screenplay with Grégory Levasseur, adapted from the science fiction short story "Graffiti" by Julio Cortázar.

==Cast==
*Stanislas Merhar as  Théo 
*Marion Cotillard as  Elia 
*Wadeck Stanczak as  Laurence 
*Pierre Vaneck as  Aaron 
*Carlo Brandt as  Freddy 
*Laura del Sol as  Olga 
*Jean-Claude de Goros as  Tonio 
*Etienne Chicot as  Quicailler

==Soundtrack==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 


 
 