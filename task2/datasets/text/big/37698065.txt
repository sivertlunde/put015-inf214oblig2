Thalaivaa
 
 

{{Infobox film
| name           = Thalaivaa
| image          = Thalaivaa film official poster.jpg
| caption        = Promotional poster
| director       = A. L. Vijay
| producer       = S. Chandraprakash Jain
| writer         = A.L.Vijay
| starring       =  
| music          = G. V. Prakash Kumar
| cinematography = Nirav Shah Anthony
| studio         = Sri Mishri Productions
| distributor    = Vendhar Movies
| country        = India
| language       = Tamil
| released       = 8 August 2013  (France, Singapore and Canada)  9 August 2013  (Worldwide) 
| budget         =   
| gross         =    
}}
 gangster Thriller thriller  Vijay and Amala Paul in the lead roles, Abhimanyu Singh as the main antagonist, as well as Sathyaraj, N. Santhanam|Santhanam, Ponvannan, Rajiv Pillai and Ragini Nandwani in supporting roles.  The film, produced by Chandraprakash Jain features background score and soundtrack composed by G. V. Prakash Kumar with cinematography handled by Nirav Shah released on 9 August 2013 worldwide to positive reviews. The film was a moderate hit at box office.

==Plot==
Thalaivaa is about Vishwa (Vijay (actor)|Vijay), a dancer in Australia who lives his good life engaging in dance competitions and managing his water distribution business with his partner and childhood friend Logu (N. Santhanam|Santhanam)and his dance team members. His father Ramadurai (Sathyaraj) is a don in Mumbai and the people’s leader for Mumbai Tamilians, a fact that Vishwa is unaware of. His Fathers arch enemy is Bhima Bhai (Abhimanyu Singh). His father keeps him away from India throughout his life and Vishwa is under the impression that his father is a businessman. He comes across Meera (Amala Paul), the daughter of one of his customers, and slowly falls in love with her. She joins his dance team and they win a major competition. Vishwa then asks for Meera’s hand in marriage from her father (Suresh), who wishes to discuss the matter formally with his father. So, Vishwa, Meera and her father head to Mumbai to meet Ramadurai. When they reach Mumbai, Vishwa realises that his father is no ordinary man but a powerful don and this is where Vishwa’s life changes forever as he discovers that Meera and her father are in fact CBI who are trying to arrest his father Ramadurai. In the meantime, Bhima Bhai devises a plot to kill Ramadurai and he gets killed in the bomb blast. Vishwa is completely devastated after the death of his father. He helps his comrades in danger but that results in the comrade getting grievously injured. Vishwa then gets attacked by assassins but he kills all of them. Police arrest him but nobody came as a witness. As a result , Vishwa is freed. He dons the title of Thalaivaa and starts helping people using his friends. During a riot in Mumbai, Bhima causes mass agitation and as a result many people are grievously injured. All the evidence goes against Vishwa, but he finds a tape recorded by a cameraman and publicises it. Bhima is put in jail. Vishwa kills the corrupt chief minister of that state and puts the blame on Bhima. At last Bhima escapes and kills every single one of his comrades, including a girl named Gowri (Ragini Nandwani). In the end, a great fight occurs in which Vishwas uncle Ranga(Ponvannan) stabs Vishwa and escapes. He tells his son about what he did in the car. Angered,
his son drives the car into a petroleum truck, thus killing both.
 
Vishwa kills Bhima, but Meera acts quickly and shoots the corpes, implying that she killed them, thus saving Vishwa from jail time. At last, Meera resigns her police job and marries Vishwa, who quickly follows in his fathers footsteps as the new leader.

==Cast== Vijay as Vishwa Ramadurai (Vishwa Bhai)
* Amala Paul as A.C.P Meera Narayanan
* Sathyaraj as Ramadurai (Anna)
* Abhimanyu Singh as Bhima Bhai
* Nassar as Ratnam
* Rajiv Pillai as Raju Santhanam as Logu
* Ragini Nandwani as Gowri Udhaya as Video Kumar
* Ponvannan as Ranga (Chitappa)
* Y. Gee. Mahendra as Lawyer Radhakrishnan
* M. R. Kishore Kumar Suresh as Meeras Father
* Manobala
* Subbu Panchu as Ravi Kiran Rekha as Ganga Ramadurai
* Ravi Prakash as Kesav Sam Anderson (cameo appearance)
* G. V. Prakash Kumar (special appearance) Silva (special appearance)
* Sridhar (special appearance) Dinesh Kumar (special appearance)
* Sathish Krishnan

==Production==

===Casting===
  Udhaya has been selected to feature in a prominent role.  G. V. Prakash Kumar will be the music director while Nirav Shah will crank the camera. R.K. Naguraj has been enrolled as art director. Rumors about a Hollywood star portraying a singer, were refuted by Vijay. 

===Filming===
The muhurta of the film was held in Chennai on 16 November 2012.  The first schedule began in Mumbai from 23 November 2012.  The title song "Thalaivaa Thalaivaa" was shot in Mumbai with 500 junior artists.  A major portion of the film was shot in Australia.  The first look and title was revealed on 14 January 2013. Additional posters were released on 26 January 2013. 
 Bondi Beach in New South Wales.  It was revealed that Vijay would portray a leader of a dance group based in Australia, performing dance styles of different genres including tap dancing with Sathish (June Ponal song fame), Karthik (Ungalil yaar Prabhu Deva winner) & their dance schoolmates.  

==Music==
{{Infobox album Name = Thalaivaa Type = Soundtrack
|Artist= G. V. Prakash Kumar Cover = Thalaivaa Audio Cover.jpg Caption = Front cover Released = 21 June 2013  Genre = Feature film soundtrack Length = 31:25 Language = Tamil
|Label = Sony Music India Producer = G. V. Prakash Kumar Last album = Udhayam NH4 (2013) This album = Thalaivaa (2013) Next album Raja Rani (2013) Reviews =
}} Vijay alongside Sony Music. 

The album received positive reviews from the audience.Behindwoods rated it 3.5/5, stating "A true for the fans, high on energy and drama". 
Oneindia said GV Prakash Kumar has composed the songs keeping new-age audience in mind. Thalaivaa album simply cannot be ignored.  Indiaglitz said "Thalaivaa is all about energy and jive. Ranging from foot tapping to soul lifting, the album is elevating in all respects. For a crowd puller like Vijay, this album is aptly scored to be cheery and soulful. In all, GV Prakash has redefined music crafted for dance and rated 3.75 / 5 – Energy packed album. Get up and dance." 

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    = 31:25
| all_lyrics      = Na. Muthukumar
| title1          = Tamil Pasanga
| extra1          = Benny Dayal, Sheezay.Psycho Unit
| length1         = 5:06
| title2          = Yaar Indha Saalai Oram
| extra2          = G. V. Prakash Kumar, Saindhavi
| length2         = 5:12
| title3          = Vaanganna Vanakkanganna Santhanam
| length3         = 5:31
| title4          = Sol Sol
| extra4          = Vijay Prakash, Abhay Jodhpurkar, Megha
| length4         = 5:46
| title5          = The Ecstacy of Dance
| extra5          = Kiran, Chennai Symphony
| length5         = 2:08
| title6          = Thalapathy Thalapathy
| extra6          = Haricharan, Pooja Vaidyanath, Zia Ulhaq
| length6         = 5:36
| title7          = Thalaivaa Theme
| extra7          = G. V. Prakash Kumar
| length7         = 2:46
}}

==Release== Sun TV for a record sum of  .  The films running length is 3 hours 2 minutes and was given a "U" certiticate by the Indian Censor Board. The film has been released on Friday, 9 August 2013 worldwide.

==Reception==
The film opened with positive reviews from critics."    rated 3/5 stating that the film is a full-baked product. Sify stated "Thalaivaa is good and the movie was racy and shorter".  Behindwoods.com gave the movie 2.75/5 commenting "A different Vijay offering with enough impactful scenes, which takes its time to build up." .Relatively, greatandhra.com gave the movie 3.85/5 stating that the film is a typical entertainer  Metromasti.com gave the movie 3/5 stars stating, "Thalaiva which runs for 185 minutes has very intelligent scenes and is expected to be a complete class hit entertainer movie of the year".  . Metromasti.com (9 August 2013). Retrieved on 29 December 2013.  AP Herald.com gave 3/5 stating that "the film just entertains the audience". 
 Vijay is Vijay and Santhanam and Amala Paul are glorified in the first half of the movie and second half is good with twists and revenge drama.  MovieCrow rated 3/5 stating that the movie is a typical entertainer.  APlive.net gave 3/5 and commented, Thalaivaa is old wine in new bottle".  Bookmyshow rated the film 3/5 stating that "Overall, Thalaivaa is a very good movie to watch, not just for movie fanatics down south but all across the nation. If it’s the one thing you learn from this flick is – If you’re your father’s son, you will follow his footsteps! ". 

==Box office==

===India===
In Chennai, the film had a good opening by fetching   in its opening day.  In Malaysia, the film overall grossed $1,902,959 ( .80&nbsp;million).  "Thalaivaa" is doing good business in Tamil Nadu.  Thalaivaa grossed more than   during the first weekend in Tamil Nadu. "Thalaivaa" had a thunderous opening and topped the Kerala box office during the Eid weekend (9–11 August) raking in  , even though four Malayalam films and one Hindi film – "Chennai Express" were released during that period.  From a total of 131 screens in kerela,the opening has been excellent and the first weekend gross is expected to be above 3 crores. The first day gross of   perfectly set the pace for constant improvements over the weekend.It must be noted that lot of audiences from Tamil Nadu are frequenting theatres in the TN – Kerala border to catch a glimpse of Thalaivaa. The Eid festival period has given a huge boost to the Kerala box-office and Thalaivaa stands on top. Distributors are confident that the movie’s overall collections will end up being as big as Thuppaki and Pokkiri in Kerala.  . behindwoods.com.  At the end of the third weekend,Thalaivaa grossed   5 crore in Kerela. 
 Vijay Strikes Vijay star power has worked big time in Bangalore.Bangalore City has seen an extraordinary opening for Thalaivaa with occupancies ranging in the 85 to 90% range.In Karnataka,Thalaivaa has earned Rs 5.7 crores (both Tamil and Telugu versions) at Box Office in the first week. The distributors share is  , says a source. In Bangalore, the Tamil movie had done great business and it is expected to have collected more than Rs  .In the following weekend, the occupancy for the recently released Thalaivaa was close to 80% in most of the multiplexes. And the single screens ran into packed house in the second weekend. Multiplexes such as Cinemax, Gopalan and PVR witnessed cent percent collections in the weekend  After the first day, 4 extra screens were added and trade pundits opine that the opening is better than the other recent big release Singam 2.A lot of fans had travelled from Tamil Nadu to see the movie, thereby adding to the craze in Bangalore City. 

The Telugu dubbed version, Anna, after a terrific day one opening share of 1 crore, settled at around   to   for the overall weekend share. Anna opened in around 275 screens, making it Vijay(actor)|Vijays biggest release in Andhra. The Tamil version of the movie is being screened in 3 properties in Hyderabad City. The overall feedback from audiences seem to be positive. 

===Overseas===
Thalaivaa opened well at International markets,especially in USA and UK. http://www.cinemalead.com/news-id-thalaivaa-usa-and-uk-box-office-report-box-office-analysis-12-08-133080.htm  In UK From the paid previews on Thursday till Saturday, Thalaivaa’s gross in the UK is   approximate from 34 locations and it is seen as a very impressive opening.  starrer Thalaivaa has been declared a Hit in UK.In UK "Thalaivaa" made an impressive start earning   (£1,21,249) from 36 screens in the opening weekend. The film has made higher collections than Suriyas "Singam 2" in the UK. "Singam 2" earned around   in the first week of its release. But Vijay(actor)|Vijays "Thalaivaa" crossed "Singam 2" collections in its debut weekend.The film continued its dream run at the UK box office even in its second weekend. Its total UK collections are now pegged at  . 

At the end of the first weekend, the US gross of Thalaivaa from 44 reported locations is 212,000 USD  . A record total of 70 locations are screening Thalaivaa and the total gross for the first weekend is expected to be in the 250,000 USD range  Thalaivaa fared well in Malaysia too.Thalaivaa has maintained exceptionally well in its second weekend in Malaysia.The film has become ninth highest grosser of all time in Malaysia box office in just 10 days collection.In its second weekend the film has collected   (MYR 579,153) on 60 screens and average working out per screens   (MYR 9,654).  Thalaivaa collected around   in just 10 days at the Malaysian box office.  The overall box office collection of the movie is estimated around 74.5 Crores 

==Awards==
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipient
! Result
|- Vijay Awards Vijay Award Favourite Hero Vijay (actor)|Vijay
| 
|- Vijay Award Entertainer of the Year Vijay (actor)|Vijay
| 
|- Vijay Award Favourite Film
|Thalaivaa
| 
|-
|rowspan="1"|Techofes#Awards                     Favourite Actor Vijay (actor)|Vijay
| 
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 