First Monday in October (film)
{{Infobox film name = First Monday in October image = First-monday-in-october-movie-poster-1981.jpg director = Ronald Neame based on =   Robert Edwin Lee}} starring = Walter Matthau Jill Clayburgh Barnard Hughes producer = Paul M. Heller Martha Scott movie_music = Ian Fraser distributor = Paramount Pictures released =   runtime = 98 minutes country = United States language = English gross =  $12,480,249 
}} play of Robert E. Lee, and directed by Ronald Neame.  Walter Matthau (for which he was nominated for a Golden Globe Award for Best Actor – Motion Picture Musical or Comedy) and Jill Clayburgh (for which she was nominated for a Golden Globe Award for Best Actress – Motion Picture Musical or Comedy) performed the principal roles.
 James Stephens The Paper Chase. 

The film was scheduled for a February 1982 release; President Ronald Reagans appointment of Sandra Day OConnor as the first female Supreme Court justice on July 7, 1981 forced the films release a month after the presidential nomination.

==Plot==
At the start of the story, the death of Associate Justice Stanley Moorehead has created a vacancy on the United States Supreme Court.  The new appointee turns out to be Ruth Loomis, a staunch conservative, who is confirmed as the first female US Supreme Court Justice.  She and Associate Justice Daniel Snow, a committed liberal and many years older than Loomis, clash intellectually on just about every judicial issue before them.  One case involves a pornographic film and arguments about freedom of speech.  With time, the two characters develop a liking and respect for each other.

==Cast==
* Walter Matthau (Associate Justice Daniel Snow)
* Jill Clayburgh (Judge, later Associate Justice, Ruth Loomis)
* Barnard Hughes (Chief Justice James Jefferson Crawford)
* Jan Sterling (Christine Snow) James Stephens (Mason Woods)
* Joshua Bryant (Bill Russell)
* Wiley Harker (Associate Justice Harold Webb)
* F.J. ONeil (Associate Justice Waldo Thompson)
* Charles Lampkin (Associate Justice Josiah Clewes)
* Lew Palter (Associate Justice Benjamin Halperin)
* Richard McMurray (Associate Justice Richard Carey)
* Herb Vigran (Associate Justice Ambrose Quincy)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 