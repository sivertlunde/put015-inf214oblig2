Sigaram
{{Infobox film
| name           = Sigaram
| image          = 
| image_size     = 
| caption        = 
| director       = Ananthu
| producer       = Rajam Balachander Pushpa Kandaswamy
| writer         = Ananthu
| starring       =  
| music          = S. P. Balasubrahmanyam
| cinematography = Raghunatha Reddy
| editing        = Ganesh Kumar
| studio         = Kavithalayaa Productions
| distributor    = Kavithalayaa Productions
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1991 Tamil Tamil drama Rekha and Radha in lead roles. The film, produced by  Rajam Balachander and Pushpa Kandaswamy, had musical score by S. P. Balasubrahmanyam and was released on 11 January 1991. 

==Plot==

Damodar (S. P. Balasubrahmanyam) is a famous music director and playback singer who won many awards. Gnanam (Nizhalgal Ravi), the man behind his success, works hard for his breakthrough and he considers Damodar as his enemy.

Krishna (Anand Babu), Damodars son, is a drunkard and spoils his life by drinking alcohol. Krishna was in love with Aparna (Ramya Krishnan) but they had not the same view, so they were separated.

Sukanya (Rekha (Tamil actress)|Rekha), Damodars wife, dies by falling from stairs. Damodar falls ill and Dr. Priya (Radha (actress)|Radha) takes care of him. Priya was Damodars fan and they were in love when they were young.

Gnanam steals Damodars records and becomes one of the top music directors. Damodar gets well and meanwhile Priya talks to Aparna. Aparna was cheated by her friend and she attempted suicide but she cannot forget Krishna. Gnanam apologizes to Damodar for his misdeeds. Krishna gets married with Aparna and Damodar and Priya also get married.

==Cast==

*S. P. Balasubrahmanyam as Damodar Rekha as Sukanya Radha as Priya
*Anand Babu as Krishna
*Ramya Krishnan as Aparna
*Nizhalgal Ravi as Gnanam
*Charle as Madhavan
*Lalitha Kumari
*Delhi Ganesh as Sachidhanandam
*Nassar in a guest appearance
*Pyramid Natarajan in a guest appearance Mano in a guest appearance
*Babloo Prithviraj in a guest appearance

==Soundtrack==

{{Infobox album |  
| Name         = Sigaram
|  Type        = soundtrack
|  Artist      = S. P. Balasubrahmanyam
|  Cover       = 
|  Released    = 1991
|  Recorded    = 1990 Feature film soundtrack
|  Length      = 45:35
|  Label       = 
|  Producer    = S. P. Balasubrahmanyam
|  Reviews     = 
}}

The film score and the soundtrack were composed by playback singer S. P. Balasubrahmanyam. The soundtrack, released in 1991, features 15 tracks with lyrics written by Vairamuthu.   

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || Agaram Ippo || K. J. Yesudas || 5:11
|- 2 || Mano || 1:16
|- 3 || Itho Itho En || S. P. Balasubrahmanyam, K. S. Chithra || 4:39
|- 4 || Jannalil || S. P. Sailaja || 5:29
|-2 suggestions: mdashfix whitespacefix
 5 || Muthamma Ennai || S. N. Surendar, S. P. Sailaja || 4:21
|- 6 || Nithiyathil Erupeerum || S. P. Balasubrahmanyam || 1:26
|- 7 || Panchali Katharukiral || M. Balamuralikrishna || 2:27
|- 8 || Petrathaithanai || S. P. Sailaja || 1:19
|- 9 || Puliku Piranthavane || S. P. Balasubrahmanyam || 1:49
|- 10 || Sangeethame Sannedi || Instrumental || 4:28
|- 11 || Unnai Kanda Pinpu  (female) || K. S. Chithra || 3:01
|- 12 || Unnai Kanda Pinpu  (male) || S. P. Balasubrahmanyam || 2:28
|- 13 || Vannam Konda  (solo)  || S. P. Balasubrahmanyam || 5:05
|- 14 || Vannam Konda  (duet)  || S. P. Balasubrahmanyam, S. P. Sailaja || 1:53
|- 15 || Chorus || 0:43
|}

==References==
 

 
 
 
 