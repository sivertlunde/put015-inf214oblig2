The Valley (2014 film)
 
{{Infobox film
| name           = The Valley
| image          = 
| caption        = 
| director       = Ghassan Salhab
| producer       = 
| writer         = Ghassan Salhab
| starring       = Carol Abboud
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 135 minutes
| country        = Lebanon
| language       = Arabic
| budget         = 
}}

The Valley ( ; al-wadi) is a 2014 Lebanese drama film written and directed by Ghassan Salhab. It was selected to be screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival and received its world premiere on 4 September 2014.   

==Cast==
* Carol Abboud as Carole
* Fadi Abi Samra as Marwan
* Aouni Kawas as Hikmat
* Carlos Chahine as Accident Man
* Rodrigue Sleiman as Armed Man
* Ahmad Ghossein as Armed Man
* Mounzer Baalbaki as Ali
* Yumna Marwan as Maria

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 