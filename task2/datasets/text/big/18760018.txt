Difficult Years
 
{{Infobox film
| name           = Difficult Years
| image          = Anni-difficili-zampa_locandina.jpg
| caption        = Film poster
| director       = Luigi Zampa
| producer       = Domenico Forzari Folco Laudati
| writer         = Sergio Amidei Vitaliano Brancati Franco Evangelisti Enrico Fulchignoni Arthur Miller
| starring       = Umberto Spadaro
| music          = Nino Rota
| cinematography = Carlo Montuori
| editing        = Eraldo Da Roma
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Difficult Years ( ) is a 1948 Italian drama film directed by Luigi Zampa and starring Umberto Spadaro,    adapted from the 1946 short story Vecchio con gli stivali ("Old Man in Boots"), by the Sicilian author Vitaliano Brancati.

==Cast==
* Umberto Spadaro as Aldo Piscitello
* Massimo Girotti as Giovanni
* Ave Ninchi as Rosina
* Delia Scala (as Odette Bedogni) as Elena
* Ernesto Almirante as Grandpa
* Milly Vitale as Maria
* Enzo Biliotti as The Baron
* Carlo Sposito (as Carletto Sposito) as Riccardo
* Loris Gizzi as The Fascist minister
* Aldo Silvani as The pharmacist

==References==
 

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 