All In (film)
 
{{Infobox film
| name           = All In
| caption        = 
| image	=	All In FilmPoster.jpeg
| director       = Marcel Varnel
| producer       = Michael Balcon
| writer         = Philip Merivale (play)  Brandon Fleming (play)  Val Guest Leslie Arliss
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ralph Lynn Gina Malo Jack Barty Claude Dampier
| music          = Louis Levy
| cinematography = Arthur Crabtree
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Gaumont British Distributors
| released       = November 1936 
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} sports comedy film directed by Marcel Varnel and starring Ralph Lynn, Gina Malo and Garry Marsh.  The owner of a racing stables has high hopes of winning The Derby, but fate intervenes. It is also known by the alternative title Tattenham Corner.

The film was made at Islington Studios by Gainsborough Pictures and based on a play by Philip Merivale and Brandon Fleming.

==Cast==
* Ralph Lynn as Archie Slott 
* Gina Malo as Kay Slott 
* Jack Barty as Tingaling Tom 
* Claude Dampier as Toop 
* Sydney Fairbrother as Genesta Slott 
* Garry Marsh as Lilleywhite 
* Robert Nainby as Eustace Slott 
* O. B. Clarence as Hemingway 
* Gibb McLaughlin as Reverend Cuppleditch 
* Glennis Lorimer as Kitty 
* Fewlass Llewellyn as Dean of Plinge

==References==
 

==Bibliography==
* Cook, Pam. Gainsborough Pictures: Cassell, 1997,
* Low, Rachael. The History of the British Film, 1929-1939. Film Making in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1929-1939. British Film Institute, 1986.

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 