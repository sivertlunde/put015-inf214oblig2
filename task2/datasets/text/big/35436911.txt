Janam Kundli
 
{{Infobox film
| name           = Janam Kundli
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Tariq Shah
| producer       = Ibrahim Khan Hiralal N. Patel	
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Jeetendra Vinod Khanna Reena Roy Anu Agarwal
| music          = Anand-Milind
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Tariq Shahand produced by Ratan Jain. It stars  Jeetendra, Vinod Khanna, Reena Roy and Anu Agarwal in pivotal roles.

==Cast==
* Jeetendra...Ravi Kapoor
* Vinod Khanna...Randhir Junior Mehra
* Reena Roy...Rita R. Mehra
* Anu Agarwal...Kiran M Prasad / Kiran R. Kapoor
* Paresh Rawal...Wong Lee / Ching Lee
* Anupam Kher...Mahendra Prasad
* Dinesh Hingoo...D.H.
* Satish Kaul...Rajiv Sodhi

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Pyar Ho Gaya"
| Udit Narayan, Alka Yagnik
|-
| 2
| "Lalla Tera Ghar"
| Sudesh Bhosle, Arun Bakshi
|-
| 3
| "Agar Barsaat Na Hoti" Poornima
|-
| 4
| "Dil Deewana Mane Na"
| Abhijeet Bhattacharya|Abhijeet, Poonima
|-
| 5
| "Love Love Love"
| Udit Narayan, Sadhana Sargam
|-
| 6
| "Cham Cham Chandni"
| Arun Bakshi, Poornima
|}
==External links==
* 

 
 
 
 

 