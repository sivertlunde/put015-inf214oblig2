Crazy House (1940 film)
{{Infobox Hollywood cartoon|
| cartoon_name        = Crazy House
| series              = Andy Panda 
| image               = Crazy House1.jpg
| caption             = 
| director            = Walter Lantz or Alex Lovy (unconfirmed)
| story_artist        = 
| animator            = Alex Lovy LaVerne Harding Les Kline (all uncredited)
| voice_actor         = Bernice Hansen Dick Nelson (both uncredited)
| musician            = Frank Marsales (uncredited)
| producer            = Walter Lantz
| studio              = Walter Lantz Productions Universal Pictures
| release_date        = September 23, 1940 (United States|U.S.)
| color_process       = Technicolor
| runtime             = 9 mins.
| movie_language      = English
| preceded_by         = 100 Pygmies and Andy Panda Knock Knock
}} Michael Barrier) or by Lantz himself (as he claimed himself). The cartoon was released on September 23, 1940.

==Plot==
Andy and his father seek shelter from a flood in an abandoned building, which in reality is a fun house, filled with such things as hidden practical jokes, a noisy merry-go-round and a dancing floor.

==Notes==
* The cartoon is recognized as Lantzs first fully independent film. The use of Technicolor produced some clashes between Lantz and the management at Universal Pictures, whom didnt like that their animation department was offering an apparently better output than their features, so they decided to cut in early 1940 Lantzs weekly advance, forcing the producer to shut down the studio for a while. Crazy House was produced during the closure. 

==References==
 

 
 
 
 
 
 
 
 
 
 
 
 
 