Fog Over Frisco
{{Infobox film
  | name = Fog Over Frisco
  | image = Fog Over Frisco Film poster.jpg
  | image_size = 
  | caption = Theatrical poster
  | director = William Dieterle
  | producer = Henry Blanke (uncredited) Robert Lord (uncredited) George Dyer (story) Robert N. Lee Eugene Solow Donald Woods Alan Hale William Demarest
  | music = Leo F. Forbstein (music supervision)
  | cinematography = Tony Gaudio
  | editing =
  | distributor = Warner Bros.-First National Pictures
  | released = June 2, 1934
  | runtime = 68 min
  | language = English
  | budget =
  | country = USA
}} American drama film directed by William Dieterle. The screenplay by Robert N. Lee and Eugene Solow was based on the short story The Five Fragments by George Dyer.

==Plot== brokerage firm security bonds for crime boss Jake Bello (Irving Pichel). 
 Donald Woods) and photojournalist Izzy Wright (Hugh Herbert).

==Principal cast==
*Bette Davis ..... Arlene Bradford Donald Woods ..... Tony Sterling
*Margaret Lindsay ..... Val Bradford
*Hugh Herbert ..... Izzy Wright
*Lyle Talbot ..... Spencer Carlton
*Irving Pichel ..... Jake Bello Alan Hale ..... Chief C.B. OMalley
*William Demarest ..... Spike Smith
*Arthur Byron ..... Everett Bradford

==Background== Of Human Bondage, accepted the relatively small role of Arlene in the hope her cooperation would convince Jack Warner to lend her to the rival studio for the film. Her ploy worked, and when Warner received word about her dynamic performance in Bondage, he elevated her to top billing in Frisco.    
 Warner Brothers remade as Spy Ship in 1942.

It was released on DVD in July 2010.

==Critical reception==
In his review in The New York Times, Mordaunt Hall described the film as a "ruddy thriller" and opined, "What   lacks in the matter of credibility, it atones for partly by its breathless pace and its abundance of action. As the story of murder and robbery passes on the screen it scarcely gives the spectator time to think who might be the ring-leader of the band of desperadoes." 

Time (magazine)|Time stated, "Brisk to the point of confession, Fog Over Frisco is not the best of Director William Dieterles pictures." 
 Film historian William K. Everson called this "the fastest film ever made". {{cite book 
|last1=Lodge 
|first1=Jack
|authorlink1= 
|last2=Taylor
|first2= John Russell
|authorlink2= John Russell Taylor
|last3= Kermode
|first3= Mark
|authorlink3= Mark Kermode
|others= 
|title=1930-1990 Hollywood: Sixty Great Years 
|year=1992 |publisher=PRION, an imprint of Multimedia Books Limited 
|location=London
|isbn=1-85375-074-3 
|page=27 }} 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 