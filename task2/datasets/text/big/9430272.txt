The Monster and the Girl
{{Infobox film
| name = Monster and the Girl
| image = The-Monster-and-the-Girl.jpg
| caption = Monster and the Girl film poster
| director = Stuart Heisler
| producer = Jack Moss
| writer =Stuart Anthony
| starring =Ellen Drew Robert Paige
| music = Gerard Carbonara
| cinematography = Victor Milner
| editing = Everett Douglas
| distributor = Paramount Pictures
| released =  
| runtime = 65 min
| language = English
| budget =
}} horror black-and-white film released by Paramount Pictures, on a low budget.

==Plot==
A gangster named Scot Webster (Philip Terry) attempts to save his sister, Susan (Ellen Drew) from the clutches of rival gangster W.S Bruhl (Paul Lukas). When one of Bruhls gang members catches Scot in Bruhls rented room, one of Scots aides is killed by a gunman. The man tosses him the gun and disappears. Scot is tried and executed. A scientist (George Zucco) salvages his brain and transplants it into a gorilla. Using the strength of his new, bestial body, Webster begins stalking the gangsters to exact his revenge.

==Cast==
*Ellen Drew as Susan Webster
*Robert Paige as Larry Reed
*Paul Lukas as W. S. Bruhl
*Joseph Calleia as Deacon
*Onslow Stevens as J. Stanley McMasters 
*George Zucco as Dr. Perry Rod Cameron as Sam Daniels
*Phillip Terry as Scot Webster 
*Marc Lawrence as Sleeper
*Gerald Mohr as Munn
*Bud Jamison as Jim

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 