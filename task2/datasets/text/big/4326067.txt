Desert Blue
{{Infobox Film
| name           = Desert Blue
| image          = Desert blue.jpg
| caption        = Theatrical release poster
| director       = Morgan J. Freeman
| producer       = {{plainlist|
* Michael Burns
* Nadia Leonelli
* Andrea Sperling
}}
| writer         = Morgan J. Freeman
| starring       = {{plainlist|
* Brendan Sexton III
* Kate Hudson
* Casey Affleck
* Sara Gilbert
* Christina Ricci John Heard
}}
| music          = Vytas Nagisetty
| cinematography = Enrique Chediak
| editing        = Sabine Hoffmann
| distributor    = Samuel Goldwyn Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}} John Heard.  

== Plot ==
A rising Hollywood starlet (Hudson) becomes "marooned" in a small desert town while on a roadtrip with her father. There, she gets to know the towns rather eccentric residents, including one (Ricci) whose hobby is pipe bombs and another (Sexton) who is trying to carry out his fathers dream of building a waterpark in the desert.

==Cast==
{{columns-list|3|
* Casey Affleck  as Peter Kepler
* Brendan Sexton III as Blue Baxter
* Kate Hudson as Skye Davis
* Christina Ricci as Ely Jackson John Heard as Prof. Lance Davidson
* Ethan Suplee as Cale
* Sara Gilbert as Sandy
* Isidra Vega as Haley Gordon
* Peter Sarsgaard as Billy Baxter
* Rene Rivera as Dr. Gordon
* Lee Holmes as Deputy Keeler
* Lucinda Jenney as Caroline Baxter
* Jerry Agee as Insurance Agent
* Daniel Von Bargen as Sheriff Jackson 
* Richmond Arquette as Truck Driver
* Michael Ironside Agent Frank Bellows
* Nate Moore as Agent Red
* Ntare Guma Mbaho Mwine as Agent Green
* Aunjanue Ellis as Agent Summers
* Fred Schneider as KBLU Radio DJ (voice)
* Liev Schreiber as Mickey Moonday (voice)
*  MacDaddy Beefcake as Telly Clems (voice)
}}

==Soundtrack==
The soundtrack features songs by The Candyskins, Rilo Kiley, Janis Ian, and others.

==Production==
Scenes were filmed in Goldfield, Nevada and Tonahpah, Nevada to portray the fictional small town. 

==Reception== U Turn, saying that it is the "herbal tea" version of the latter.   Lisa Schwarzbaum  of Entertainment Weekly rated it C and described the setting as "yet another indie drama set in a burg reminiscent, by way of aggressive eccentricity, of TVs Northern Exposure." 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 