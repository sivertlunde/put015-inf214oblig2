Arthur? Arthur!
{{Infobox film
| name           = Arthur? Arthur!
| image          = 
| caption        = 
| director       = Samuel Gallu
| producer       = 
| writer         = Julian Symons
| screenplay     = 
| story          = 
| based on       =  
| starring       = Shelley Winters Donald Pleasence Terry-Thomas Tammy Grimes Harry Robertson
| cinematography = 
| editing        = Peter R. Hunt
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

Arthur? Arthur! is a 1969 British comedy film directed by Samuel Gallu and starring Shelley Winters, Donald Pleasence and Terry-Thomas.  The plot follows a dull and unsuccessful inventor who begins to develop a second identity as a man about town with a completely different life.

==Cast==
* Shelley Winters - Hester Green
* Donald Pleasence - Arthur Brownjohn / Sir Easonby E Mellon
* Terry-Thomas - Clennery Tubbs
* Tammy Grimes - Lady Joan Mellon
* Rafiq Anwar - Majordomo
* Judith Arthy - Patricia Parker Michael Bates - Mr - Harrington
* Peter Bayliss - Doctor Hubble
* Joan Benham - Mrs Payne
* Mike Carnell - Postman
* Erik Chitty - Uncle Ratty Margaret Courtenay - Clare Brownjohn
* Frank Crawshaw - Dustman
* Mark Eden - Jack Parker
* Robin Ellis - Ames
* Angela Grant - Cynthia (as Angie Grant)
* Basil Henson - Coverdale
* Raymond Huntley - George Payne
* Stanley Lebor - Analyst
* Garry Marsh - Golfer
* Keith Marsh - Lillywhite
* Harry Shacklock - Attendant
* Jeffrey Sirr - Waterboy
* Patsy Smart - Miss Bonnamie
* Oliver Tobias - Peter Bobo Jackson
* Margery Withers - Susan
* Victor Brooks - Minor part

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 