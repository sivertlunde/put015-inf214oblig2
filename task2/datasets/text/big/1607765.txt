Prefontaine (film)
{{Infobox film
| name           = Prefontaine
| image          = Prefontaineposter.jpg
| caption        = Theatrical release poster Steve James
| producer       = Mark Doonan Peter Gilbert Jon Lutz Irby Smith
| writer         = Steve James Eugene Corr
| starring       = Jared Leto R. Lee Ermey Ed ONeill
| music          = Mason Daring
| cinematography = Peter Gilbert
| editing        = Peter Frank
| studio         = Hollywood Pictures Buena Vista Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = $589,304
}}
 Steve James and Eugene Corr, and directed by James. Prefontaine tells the story from the point of view of Bill Dellinger, played by Ed ONeill, the assistant coach who was with him day-to-day, and Nancy Alleman, the runners girlfriend at the time of his death.

==Plot==
Steve Prefontaine, a Coos Bay, Oregon student, is too small to play most sports but becomes a talented distance runner. He enrolls at the University of Oregon in 1969, and meets fellow Oregon Ducks track and field athletes Pat Tyson and Mac Wilkins. With coaches Bill Bowerman and Bill Dellinger, "Pre" wins three national cross-country championships and four consecutive 5k run|5,000-meter runs, breaking the U.S. record in the latter. Prefontaine gains fame as an aggressive runner who likes to be out front from the start, rather than biding his time until a strong finish.

Prefontaine accompanies other top American runners including Frank Shorter and Jeff Galloway to the 1972 Munich Olympics, where they witness the terrorist attacks of the Munich Massacre which interrupt and almost cancel the games. In the 5,000-meter, after leading with only 150 meters to go, three different runners including the winner, Finlands Lasse Viren, pass Prefontaine and he does not win a medal.
 MG convertible flips while driving. After his death, the Amateur Sports Act of 1978 gives athletes more control over their sports governance.

==Cast==
* Jared Leto as Steve Prefontaine
* Ed ONeill as Bill Dellinger
* R. Lee Ermey as Bill Bowerman
* Amy Locane as Nancy Alleman
* Breckin Meyer as Pat Tyson
* Lindsay Crouse as Elfriede Prefontaine
* Brian McGovern as Mac Wilkins
* Kurtwood Smith as Curtis Cunningham
* Laurel Holloman as Elaine Finley

==Production==
The majority of the film was shot at the University of Puget Sound campus in Tacoma, Washington. Peyton Field was redecorated to resemble Hayward Field at the University of Oregon. For the role of Steve Prefontaine, Jared Leto immersed himself in the runners life, meeting with members of the family and friends.    He bore a striking resemblance to the real Prefontaine, also adopting the athletes voice and upright running style.  The transformation was so complete that when the runners sister, Linda, first saw him in character, she broke down and cried.  Prefontaine explores American athletes amateur status and the conditions and lack of resources these athletes had to endure in their attempts to compete with the worlds top athletes, who were provided all they needed to train and compete at a top level, while dealing with the pressure from their American fans who expected nothing but the best from them.

==Critical reception==
Upon its premiere at the 1997 Sundance Film Festival, Prefontaine was positively received by critics and audiences, who greatly praised the film for Letos acting and James direction.  Review aggregator Rotten Tomatoes reports that 59% of critics gave the film a positive review, based on 27 reviews with an average score of 6.4 out of 10.  On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film has a rating score of 56 based on 15 reviews, indicating mixed or average reviews.  Film critics Gene Siskel and Roger Ebert praised the film giving it "two thumbs up".

==See also==
* Without Limits, another film based on Prefontaines life.

==References==
 

==External links==
*  
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 