The Assassin (2015 film)
{{Infobox film
| name           = The Assassin
| image          = Theassassinshuqi.jpg
| alt            = 
| caption        = 
| director       = Hou Hsiao-Hsien
| producer       = 
| writer         = Hou Hsiao-Hsien Chu Tien-wen Zhong Acheng Xie Haimeng
| starring       = Shu Qi Chang Chen Satoshi Tsumabuki
| music          = 
| cinematography = Mark Lee
| editing        = 
| studio         = 
| distributor    = Tartan Films
| released       =  
| runtime        = 
| country        = Taiwan
| language       = 
| budget         = Renminbi|CN¥90 million (United States dollar|US$14.9 million) 
| gross          = 
}}
The Assassin (Chinese: 聶隱娘) is an upcoming Taiwanese martial arts film directed by Hou Hsiao-Hsien. It has been selected to be screened in the main competition section at the 2015 Cannes Film Festival.   

==Plot==
The film is set during the mighty Tang Dynasty-period in Chinese history.   

==Cast==
* Shu Qi as Nie Yinniang
* Chang Chen as Tian Jian
* Satoshi Tsumabuki
* Ethan Juan

==Production==
 
The film received several subsidies from the Taiwanese government: in 2005 of New Taiwan dollar|NT$15 million (United States dollar|US$501,000), in 2008 of NT$80 million (US$2.67 million) and in 2010 of NT$20 million (US$668,000).   However, over the production, Hou encountered various budget problems; thus over half of the films final budget came from China, a first for Hous films.  As of September 2012, its budget was Renminbi|CN¥90 million (US$14.9 million).     

Reflecting on the films long shooting period and a much bigger budget than his previous films, Hou stated "I havent shot a movie in six or seven years. Its really a whole new world for me because the market is now so big, because of China. So the scale is much bigger, and that makes every detail different, so now even I have to adjust my scale." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 

 