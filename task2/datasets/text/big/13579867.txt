Awaargi
{{Infobox film
| name           = Awaargi
| image          = Awaargi.jpg
| image_size     =
| caption        = Promotional Poster
| director       = Mahesh Bhatt
| producer       = P. Raghnunath
| writer         = Suraj Sanim
| narrator       =  Govinda Anupam Kher Paresh Rawal
| music          = Anu Malik
| cinematography = Nadeem Khan
| editing        = Dimpy Bahl
| distributor    = Swaraajya Shree Movies
| released       = 26 January 1990
| runtime        = 149 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}} 1990 Bollywood drama film directed by Mahesh Bhatt starring Anil Kapoor, Meenakshi Sheshadri and Govinda (actor)|Govinda. The film is considered to have featured Anil Kapoors and Meenakshi Sheshadris best performances. Over the years it has gained immense critical praise and is considered a classic now. 

Like several other 1980s films by Mahesh Bhatt, Awaargi, containing serious and realistic content, belongs to the arthouse cinematic genre, known in India as parallel cinema. On its release, it received critical acclaim.   

==Story==
Lalla (Anupam Kher) is one of the underground dons in Bombay, and has several men to carry out all kinds of criminal activities for him. One of them is Azad (Anil Kapoor), who he uses to scare people, and Azad has never failed him. Then Azad goes and rescues a prostitute named Meena (Meenakshi Sheshadri) from Ranubhai (Avtar Gill), who owes allegiance to rival don, Bhaus (Paresh Rawal), group and invites his ire and anger. Azad refuses to submit to anyone, and assists Meena meet her goal - which is to be a famous singer. He entrusts her to a music director by the name of Dhiren, and sits back and watches as Meena becomes popular day by day. Azad has fallen in love with Meena, but is unable to tell this. What Azad does not know that Dhiren too has fallen in love with Meena, and it appears that Meena is also attracted to him. Before Azad can pursue this any further, he must content with Bhaus hoodlums - who have been instructed to kill him under any circumstances.

==Cast==
*Anil Kapoor as  Azaad
*Govinda as Dhiren Kumar 
*Meenakshi Sheshadri as  Meena 
*Anupam Kher as  Lala Jamal Khan 
*Paresh Rawal  as  Bhau 
*Avtar Gill as  Ranubhai the Pimp
*Deepak Qazir as  Saigal 
*Satish Kaushik as  Azaads Friend

==Music==
The music of Awaargi was by Anu Malik. The lyrics were penned by Anand Bakshi.
 
{| border="2" cellpadding="5" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
| Chamakte Chand Ko Ghulam Ali Ghulam Ali
|-
| Baali Umer Ne Mohammad Aziz, Lata Mangeshkar
|-
| Aye Mere Saathiya Mohammad Aziz, Lata Mangeshkar
|-
| Daak Babu Aya Asha Bhonsle
|-
| Mujre Wali Hoon Anuradha Paudwal
|}

== References == 
 

== External links ==
*  

 

 
 
 
 

 
 