The Apostle
 
 
{{Infobox film name            = The Apostle image           = Apostleposter.jpg caption         = Theatrical release poster director        = Robert Duvall producer        = Steven Brown Rob Carliner Robert Duvall writer          = Robert Duvall starring        = Robert Duvall Farrah Fawcett Miranda Richardson Billy Joe Shaver Billy Bob Thornton June Carter Cash Todd Allen Walton Goggins music           = David Mansfield cinematography  = Barry Markowitz editing         = Stephen Mack distributor     = October Films released        =   runtime         = 134 min. country         = United States language        = English budget          = $8,000,000 The Numbers,    gross           = $19,868,354 (domestic)   
}} John Beasley, Farrah Fawcett, Billy Bob Thornton, June Carter Cash, Miranda Richardson and Billy Joe Shaver also appear. It was filmed on location in and around Saint Martinville and Des Allemands, Louisiana with some establishing shots done in the Dallas|Dallas, Texas area by a second unit before principal photography began.

The film was screened in the Un Certain Regard section at the 1998 Cannes Film Festival.    For his performance, Duvall was nominated for the Academy Award for Best Actor.

== Plot == charismatic Pentecostal preacher. His wife Jessie (Fawcett) has begun an adulterous relationship with a youth minister named Horace. She refuses Sonnys desire to reconcile, although she assures him that she will not interfere with his right to see his children. She has also conspired to use their churchs bylaws to have him removed from power. Sonny asks God what to do but receives no answer. Much of the congregation sides with Jessie in this dispute. Sonny, however, refuses to start a new church, insisting that the one which forced him out was "his" church. At his childs Little League game, Sonny, in an emotional fit, attacks Horace with a bat and puts him into a coma. Horace later dies.

A fleeing Sonny ditches his car in a river and gets rid of all identifying information. After destroying all evidence of his past, Sonny rebaptizes himself and anoints himself as "The Apostle E. F." He leaves Texas and ends up in the bayous of Louisiana, where he convinces a retired minister named Blackwell (Beasley) to help him start a new church. He also begins a dating relationship with a local radio stations employee (Richardson).

With Sonnys energy and charisma, the church soon has a faithful and racially integrated flock. Sonny even succeeds in converting a racist construction worker (Thornton) who shows up at a church picnic intent on destruction. While at work in a local diner, Sonny sees his new girlfriend out in public with her husband and children, apparently reconciled. Sonny walks out, vowing never to return there.

Jessie hears a radio broadcast of the Apostle E. F. and calls the police on Sonny. The police show up in the middle of an evening service but allow Sonny to finish it while they wait outside. In the poignant finale, Sonny delivers an impassioned sermon before telling his flock that he has to go. In the final scene, Sonny, now part of a chain gang, preaches to the inmates as they work along the side of a highway.

== Cast ==
* Robert Duvall – Euliss F. Sonny Dewey – The Apostle E.F.
* Farrah Fawcett – Jessie Dewey
* Billy Bob Thornton – Troublemaker
* Billy Joe Shaver – Joe
* June Carter Cash – Mrs. Momma Dewey Sr.
* Miranda Richardson – Toosie
* Walton Goggins – Sam
* Todd Allen – Horace
* Lenore Banks – Female Sonny Supporter John Beasley – Brother C. Charles Blackwell
* Brett Brock – Helper

== Production ==
Duvall wrote the script in the 1980s but could not find a studio willing to film it. He eventually decided to direct and finance it himself. It was first screened at the Toronto International Film Festival. Thirty minutes into the screening, studio executives began leaving the theater to wheel and deal outside; October Films gained the distribution rights that night. The film went on to have a $21.3 million worldwide theatrical gross, with a combined production and advertising budget of $8 million. 

In counterpoint to its volatile subject matter, the film has a restrained visual style and employs a large number of non-professional actors to enhance its realism.

== Soundtrack ==
{{Infobox album  
| Name        = Music from and Inspired by the Motion Picture The Apostle
| Type        = soundtrack
| Longtype    =
| Artist      = various
| Cover       =
| Cover size  = 0
| Released    = February 10, 1998
| Recorded    = Country Contemporary Contemporary Christian Southern Gospel
| Length      = 50:30 Rising Tide
| Producer    = Robert Duvall Scott Greenstein  (executive producers) 
| Last album  =
| This album  =
| Next album  =
}}
{{Album ratings
| rev1      = AllMusic
| rev1Score =   
| rev2      = 
| rev2Score = 
}} contemporary Christian artist Steven Curtis Chapman, were recorded especially for the film.

The soundtrack won the 1998 Grammy Award for Best Southern, Country, or Bluegrass Gospel Album.

The songs, "I Will Not Go Quietly" by Chapman, "Two Coats" by Loveless and "Im a Soldier in the Army of the Lord" by Lovett were released on a soundtrack album that was supplemented with more exclusive songs "inspired by" (but not included in) the film. The additional tracks include works by Johnny Cash, Emmylou Harris (in a duet with Robert Duvall), the Carter Family, the Gaither Vocal Band and the Sounds of Blackness.

=== Track listing ===
#"I Will Not Go Quietly" (composed and performed by Steven Curtis Chapman) – 3:46
#"Two Coats" (traditional, performed by Patty Loveless) – 3:21
#"Im a Soldier in the Army of the Lord" (traditional, performed by Lyle Lovett) – 3:29
#"Softly and Tenderly" (composed by Will Lamartine Thompson, performed by Rebecca Lynn Howard) – 3:05
#"There Is a River" (composed by David Sapp, performed by the Gaither Vocal Band) – 4:24 In the Garden" (composed by C. Austin Miles, performed by Johnny Cash) – 3:16
#"I Love to Tell the Story" (traditional, performed by Emmylou Harris and Robert Duvall) – 3:45 Terry Smith, performed by the Carter Family) – 3:15
#"Victory Is Mine" (traditional, performed by the Sounds of Blackness) – 3:32
#"There Is Power in the Blood" (traditional, performed by Lari White) – 5:19
#"There Aint No Grave Gonna Hold My Body Down" (composed by Brother Claude Ely,   performed by Russ Taff) – 4:54 Gary Chapman and Wynonna Judd) – 3:47
#"Softly and Tenderly (Reprise)" (composed by Will Lamartine Thompson, performed by Dino Kartsonakis) – 4:37

=== Chart performance ===
{| class="wikitable"
! Chart (1998)
! Peak position
|-
| U.S. Billboard Top Christian Albums
| align="center"| 4
|-
| U.S. Billboard Top Country Albums
| align="center"| 21
|-
| U.S. Billboard 200
| align="center"| 175
|}

== Reception ==

Review aggregator Rotten Tomatoes has a 91% fresh rating for the film based on 45 reviews, with an average score of 8.1/10.   Roger Ebert gave it four out of four stars and called the film "a lesson in how movies can escape from convention and penetrate the hearts of rare characters." 

== References ==
 

== External links ==
*  
*  
*   at the   list
* The New York Times Critics Pick (A. O. Scott)  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 