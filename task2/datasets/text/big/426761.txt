Wet Hot American Summer
{{Infobox film
| name = Wet Hot American Summer
| image = Wet hot american summer.jpg
| alt =
| caption = Theatrical release poster
| director = David Wain
| producer = Howard Bernstein
| writer = David Wain Michael Showalter
| starring = See #Cast|Cast Theodore Shapiro Craig Wedren
| cinematography = Ben Weinstein
| editing = Meg Reticker
| distributor = USA Films
| released =  
| runtime = 97 minutes
| country = United States
| language = English
| budget = $1.8 million  
| gross = $295,206   
}} satirical comedy The State), Marguerite Moreau, Paul Rudd, Molly Shannon, Christopher Meloni, Elizabeth Banks, Michael Ian Black, Bradley Cooper, Amy Poehler, Zak Orth and A.D. Miles. The film takes place during the last day at a fictional Jewish summer camp in 1981, before closing for the summer.
 cult following.  Netflix will produce an eight-episode prequel in 2015 starring most of the original cast members, including Bradley Cooper, Janeane Garofalo, Paul Rudd, Amy Poehler, Elizabeth Banks and David Hyde-Pierce, among numerous others.

==Plot==
On August 18, 1981, Camp Firewood, a summer camp located near Waterville, Maine, is preparing for its last day of summer camp, which means counselors have one last chance to have a romantic encounter with another person at Camp Firewood. The summer ultimately culminates in a talent show.

Beth (Janeane Garofalo), the camp director, struggles to keep her counselors in order—and her campers alive—while falling in love with Henry (David Hyde Pierce), an astrophysics associate professor at the local college (actually Colby College). Henry has to devise a plan to save the camp from a piece of NASAs Skylab, which is falling to Earth.
 veteran and camp chef, can help Coop win Katie—with some help from a talking can of vegetables (voiced by H. Jon Benjamin).

All the while, Gary (A.D. Miles), Genes unfortunately chosen apprentice, and J.J. (Zak Orth) attempt to figure out why McKinley (Michael Ian Black) hasnt been with a woman;  Victor (Ken Marino) attempts to sleep with the resident loose-girl Abby; and Susie (Amy Poehler) and Ben (Bradley Cooper) attempt to produce and choreograph the greatest talent show Camp Firewood has ever seen.

==Cast==
 
* Janeane Garofalo as Beth
* David Hyde Pierce as Professor Henry Newman
* Michael Showalter as Gerald "Coop" Cooperberg/Alan Shemper
* Marguerite Moreau as Katie
* Paul Rudd as Andy
* Zak Orth as J.J.
* Christopher Meloni as Gene
* A.D. Miles as Gary
* Molly Shannon as Gail von Kleinenstein
* Gideon Jacobs as Aaron
* Ken Marino as Victor Pulak
* Joe Lo Truglio as Neil
* Michael Ian Black as McKinley Dozen
* Liam North as Arty
* Bradley Cooper as Ben
* Amy Poehler as Susie
* Marisa Ryan as Abby Bernstein
* Elizabeth Banks as Lindsay
* Gabriel Millman as Caped Boy
* Kevin Sussman as Steve
* Kevin Thomas Conroy as Mork Guy
* Madeline Blue as Cure Girl
* Cassidy Ladden as Mallrat Girl
* Nina Hellman as Nancy
* Peter Salett as Guitar Dude
* Judah Friedlander as Ron Von Kleinenstein
* H. Jon Benjamin as Can of Mixed Vegetables  
* Samm Levine as Voice of Arty   
* David Wain as Paco   
* Jacob Shoesmith-Fox as Burt "Moose" Flugelman
* Jared Reiter as Roger the Master Broom Balancer
 

==Production==
===Background=== Indian Summer Dazed and Confused and Do the Right Thing&mdash;"films that take place in one contained time period that have lots of different characters."  

===Development===
The films financing took three years to assemble; in a June 2011 interview, Wain revealed the films budget was $1.8 million; he noted that during the 2001 Sundance Film Festival,  the film had been promoted as costing $5 million, in an attempt to attract a better offer from a distributor.  

===Filming===
Principal photography lasted 28 days, and it "rained on all of them";   Exterior shots were filmed catch-as-catch can, and, in many interior scenes, rain seen outside turns into sun as soon as characters step outside. The actors breath can be seen in most outdoor scenes,  and even in some indoor ones because of the cold. The film was shot at Camp Towanda in Honesdale, Pennsylvania  and is rated R for adult humor, language, and sexual content.

===Music===
 
As the film is set in the early 1980s, the films soundtrack features songs from many popular bands of the era, most notably Jefferson Starship, Rick Springfield, Loverboy, and Kiss (band)|KISS.

==Release== premiered at participation for theatrical release limited to fewer than 30 cities: 
Toronto,
Los Angeles, San Francisco,
Chicago, Champaign, IL,
Dallas, Austin, TX, Abilene, TX,
Washington D.C.,
Seattle, Bellingham, WA, Langley, WA,
Cleveland,
Salt Lake City,
Atlanta, Athens, GA,
Chapel Hill, NC, Tryon, NC,
Lafayette, IN,
Boston,
Providence, RI,
Wilton, NH,
Madison, WI,
Columbia, MO,
Minneapolis,
Portland, OR and
Nashville, TN.

==Reception==
The film received negative reviews from critics. Rotten Tomatoes gives the film a score of 31% based on 65 reviews. Metacritic gives a rating of 42% based on reviews from 24 critics. 

Notably, Roger Ebert rated the film with one star out of four, and despised it so much that his review took the form of a sarcastic tribute to Allan Shermans "Hello Muddah, Hello Fadduh". 

In contrast, Entertainment Weekly  s Owen Gleiberman awarded the film an "A" and named it as one of the ten best films of the year. Newsweeks David Ansen also lauded it, calling it a "gloriously silly romp" that "made me laugh harder than any other movie this summer. Make that this year."  Numerous other critics have praised the film as a witty pop satire and it has gone on to achieve a cult following. 

Kristen Bell stated on NPR on September 2, 2012, that this was her favorite film of all time, having watched it "hundreds of times". NPR host Jesse Thorn said on the April 29, 2014, episode of Bullseye with Jesse Thorn|Bullseye, "When someone has an open enough heart to accept this silliness - and thats what its about for me, an open heart - if someones heart is open to Wet Hot American Summer, they love it. And thats when I know that me and them, weve got an unbreakable bond. Together forever. Like camp counselors." 

==Home video==
The film was released in both VHS and DVD formats on January 15, 2002.   Wain has tried to convince Universal Studios to do either a 10th anniversary home video re-release with extra features, or perhaps a Blu-ray Disc|Blu-ray release, but Universal has repeatedly rejected the idea.  

==Upcoming Netflix Miniseries==
 

On January 9, 2015, Deadline.com reported that an 8 episode miniseries based on the film had begun shooting for release on Netflix. Almost the entire cast of the original film will be returning, along with Showalter and Wain. 

==Anniversary celebrations== Stella in San Francisco Comedy Festival with much of the original cast. 

==References==
{{Reflist|30em|refs=
    -->
   
   
}}

==External links==
 
*  
*  
*  
*  
*  
*   from Kittenpants|Kittenpants.org

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 