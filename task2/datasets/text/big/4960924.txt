Circus World (film)
 
{{Infobox film
| name           = Circus World
| image          = Circus World.jpg Frank McCarthy
| director       = Henry Hathaway
| producer       = Samuel Bronston studio          =Samuel Bronston Productions
| writer         = Ben Hecht Julian Halevy James Edward Grant John Smith Richard Conte
| music          = Dimitri Tiomkin
| cinematography = Jack Hildyard
| editing        = Dorothy Spencer
| distributor    = Paramount Pictures (US) Rank Organization (UK)
| released       =  
| runtime        = 135 minutes
| country        = United States
| language       = English
| budget         = $9 million (estimate)
| gross = $1,550,000 (US/ Canada) 
| followed_by    = 
}}
Circus World (released as The Magnificent Showman in the United Kingdom) is a 1964 drama film starring John Wayne, Claudia Cardinale and Rita Hayworth. It was directed by Henry Hathaway and produced by Samuel Bronston, with a screenplay by Ben Hecht, Julian Halevy and James Edward Grant, from a story by Philip Yordan and Nicholas Ray.
 Best Song Best Actress award.

==Plot== Buffalo Bill Cody, bought a bankrupt circus in 1885 and successfully rebuilt it into a combination three ring and Wild West extravaganza, mixing Wild West Show acts with conventional circus acts in a winning combination. He has successfully toured the United States for more than a decade. Now that the century is about to turn, he wants to take his show to Europe.
 John Smith), also attempts to dissuade Masters, but to no avail. Masters buys a freighter, renames her the Circus Maximus, and the show sails for Europe.

At Lisbon, the first port of call, the Circus Maximus capsizes at the pier and puts the show in the toilet. Masters has to release most of his performers, board out his animals, and go back to performing an act for the Ed Purdy Wild West Show, a staple on the European circus circuit. Down but not out, Masters doesnt waste the disaster. "While touring Europe at Ed Purdys expense," as Masters puts it, he, Cap, Steve and Toni scout acts that will enable Masters to relaunch the Matt Masters Circus bigger and better than ever. 

His first new hire is Emile, a French animal trainer who has a spectacular act involving lions who lie down on him in the ring. Masters offers to take him on if he will switch from lions to tigers (Masters has many tigers but few lions). The trainer is adamant that he does not want to change to working with tigers; his wife persuades him to do so. By the time the circus is ready to relaunch, Emile has so adapted to working with a different variety of big cat that when a couple become ill,  he demands that Masters hire a doctor for "HIS tigers." 

His second new act is Jojo the Clown and the Wire-Dancing Ballerina (Richard Conte and Katharyna respectively). They have a unique act; Jojo is dressed as a clown but walks the high wire over a cage full of lions while coaching the Ballerina (his daughter Giovanna) as she dances on the wire. Backstage, Masters discovers that Jojo is an old acquaintace - Aldo Alfredo, brother-in-law of his lost love Lili Alfredo. Despite his reservations at hiring a possible enemy Masters takes the act on and neither he nor Aldo admit to Toni, who is Aldos niece, that they have met before; or that Jojo the Clown is her uncle.

The third addition to the performers roster is Margo Angeli, an artist of the high trapeze, coincidentally where Toni wants to work instead of in the Wild West show or as part of Clown Alley, where Matt has her working in the show. In reality, Margo is the vanished Lili Alfredo, haunted by the guilt of having been caught up in a love triangle, blaming herself for the death of her flyer husband who had fallen - or did he miss Aldos catch on purpose after learning he was part of a love triangle? She had run away from the world of the circus and kept on running, finding solace first in the Church and then in the bottle.

As Masters had hoped, the lure of her daughter brought Lili out of hiding. She speaks to her daughter during a performance of Ed Purdys Wild West Show without identifying herself, and Masters spots her. The two have an intense confrontation in a bar, ending with Masters slamming a full bottle of brandy down in front of Lili and telling her that she needs to decide whether the booze or her child is more important to her;but that if she doesnt pull herself together, as far as hes concerned she is dead to him. Lili quits drinking and goes into training to seek a position in the new Matt Masters Circus, then in winter quarters near Lisbon.

Meanwhile, Toni has fallen for Steve and he for her, despite a difference in their ages of at least a decade. Matt has to come to terms with the fact that his adopted little girl is a woman grown, with a mind of her own. "Margos" reappearance helps, and she is secretly amused by Tonis attempts to pair her off with Matt. Matts doing pretty well on his own, as Toni observes with pleasure. She loves her adopted father and wants him to be happy.

Inevitably, the truth comes out. The afternoon of the rehearsal for the first show of the circus season in Vienna, with Masters demanding of his performers the same show they will put on that evening, Toni finds a poster of The Flying Alfredos in her wagon living quarters with "Suicide" daubed on it in red. She also finds a newspaper clipping of the Flying Alfredos that allows her to identify "Margo" as her mother. There is a stormy confrontation with many passionate, hateful words on Tonis part between her, Lili and Matt; and Matt has to tell her that he was the second man in the love triangle. Toni curses both of them and runs out, just before a bugle call summons the show to Dress Rehearsal.

The rehearsal opens to empty seats with Grand Parade, with the performers marching in behind the flags of the nations whose citizens are in the show: the United States, Great Britain, France, Imperial Germany, Switzerland, Iceland, Sweden, Italy, and many more. Partway through Grand Parade, a fire breaks out in Wardrobe and spreads to the Big Top. Fast action by Lili, Matt, Steve, Toni, Cap and Aldo prevent injury to the circus performers and manage to save about half of the tent from the flames. The one positive thing to come out of the fire is a rapprochement between Toni and Lili.

Matt somehow obtains permission from the Emperor to set up the circus in the grounds of the Imperial Palace. The show is a smash success, with a new act headlining: Lili and Toni Alfredo performing a swing-over routine fifty feet in the air. Ultimately Matt, Lili, Toni and Matts new partner and Tonis fiance, Steve, taking bows to the applause of the people and the Crown.

==Production notes==
===Background===
Circus buffs will recognize the headlining routine at the films climax as the routine Lillian Leitzel performed at one time.

===Technical===
The films music score was by Dimitri Tiomkin and the cinematography by Jack Hildyard. The film was made in Super Technirama 70, but was advertised on posters as being presented in Cinerama. 

===Cast===
This was Waynes last film before his lung cancer operation.  It was speculated that at the time this film was made, Rita Hayworth may have already been suffering the early stages of Alzheimers disease. She was often late and had trouble remembering her lines and it was reported she was often drunk and abusive to those on the set. John Wayne had previously looked forward to working with her, but it was said he came to despise her behavior.  David Niven was originally cast as Cap Carson, but withdrew from the film.  Frank Capra was originally going to direct, but quit during pre-production over disagreements with Waynes favored screenwriter, James Edward Grant.  Rod Taylor was going to play Steve McCabe but pulled out just prior to filming and was replaced by John Smith. 

==Cast==
* John Wayne as Matt Masters
* Rita Hayworth as Lili Alfredo
* Claudia Cardinale as Toni Alfredo
* Lloyd Nolan as Cap Carson
* Richard Conte as Aldo Alfredo John Smith as Steve McCabe
* Katharyna as Giovana
* Katherine Kath as Hilda
* Wanda Rotha as Mrs. Schuman
* Kay Walsh as Flo Hunt 
* Francois Calepides as Ringmaster   
* Margaret MacGrath as Anna
* Miles Malleson as Billy Hennigan  
* José María Caffarel as Barcelonas Mayor 
* Robert Cunningham as Ringmaster  
* Hans Dantes as Emile Schuman 
* Katherine Ellison as Molly (uncredited)  
* Margaret Fuller as Woman with Binoculars (uncredited)  Moustache as Bartender (uncredited)  
* Sydna Scott as Undetermined Role (uncredited)
* George Tyne as Madrid Bartender (uncredited)
* Víctor Israel as unknown (uncredited)

==Home Media==
It has never been released in the U.S.A., but has been released on an all Region DVD, which works on most all DVD players. 

==In popular culture==
The films production was the setting for a novel by the Spanish author   which was originally published in 1999 with the Spanish title Noticias del mundo real (News from the Real World). It is about two young Spanish law students in 1964 who search the city of Barcelona for actor John Wayne, who has not returned to the hotel for another day of the films shooting. 

==Trivia==
While filming a scene where the main tent catches fire, John Wayne was almost killed when the set collapsed. As he was "fighting" the fire, Wayne was to be cued by the assistant director when to leave before the set was to collapse in flames. Either Wayne didnt hear the assistant director, or the a.d. mistimed it (it was never determined which), but the flaming set began to collapse before Wayne got out. He escaped with just seconds to spare before the entire set fell down.

==See also==
*John Wayne filmography

==References==
 

==External links==
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 