Joy Ride (2000 film)
{{Infobox film
| name           = Joy Ride
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Martin Rengell
| producer       = Claudia Wick
| writer         = Martin Rengell
| narrator       = 
| starring       = Andri Zehnder Sebastian Hölz Edward Piccin Stephan Krauer Claudia Knabenhans
| music          = 
| cinematography = Marco Barberi
| editing        = Bernhard Lehner
| studio         = Abrakadabra Films Schweizer Fernsehen Teleclub AG Annazarah Films
| distributor    = Frenetic Films
| released       =  
| runtime        = 90 minutes
| country        = Switzerland
| language       = Swiss German
| budget         = 
| gross          = 
}}

Joy Ride (in Swiss German: Usfahrt 
{{cite web
| url         = http://www.crew-united.com/index.asp?show=projectdata&ID=47052
| title       = Joy Ride (2000)
| work        = crew united: Das Netzwerk der Film- und Fernsehbranche
| publisher   = crew united
| location    = Munich
| language    = German
| accessdate  = 2009-12-17
| quote       = Arbeitstitel: Usfahrt 
Fremdsprachiger Titel: Joy Ride

}} drama feature film written and directed by Martin Rengel that followed Lars von Triers Dogme 95 manifesto. It is classified as the 14th dogme movie. {{cite web
|url=http://www.filmbug.com/dictionary/dogme95.php
|title=Dogme 95
|publisher=Filmbug
|accessdate=15 December 2009}}  Joy Ride employs a very realistic, near-documentary style, with a story based on the homicide of a 19-year-old girl in Zürich, Switzerland in 1992. {{cite web
|url=http://www.cineman.ch/movie/2000/JoyRideDogma14/review.html
|title=Die Tat ist ohn warum
|last=Gubser
|first=Stefan
|language=German
|accessdate=15 December 2009}}      The incident attracted a great deal of media coverage in Switzerland.

==Background==
The magazine Der Schweizerische Beobachter|Beobachter reported on the incident which gave rise to the  "Joy Ride" story. 

According to the report, the clique no longer tolerated the murdered girl and they had assaulted her by singeing her hair and bending her fingers back.  On March 14, 1992, the male members of the clique, who had spent the afternoon smoking marijuana, decided "heute Abend passiert es. Andrea muss weg." (Tonight it happens/has to happen. Andrea must leave.) Patrick and Thomas were allegedly the dominant force in the discussion; Roman, who drove the car for the commission of the crime, claims to have been unconvinced that her transgression warranted murder. Roman, a compulsive gambler and alcoholic at odds with his parents, was an old friend of Thomas but their association had recently terminated. Three weeks before the murder, Roman met Thomas again and the pressure to regain his approval clearly contributed to his decision to assist in the murder. 

As the clique drove back from an evening at a local pub, Patrick took his belt off and tied it around Andreas neck. She managed to shake him off, but Thomas ordered Roman to turn off the main road and they assaulted her again. Roman stopped the car and urged the other men to stop; they hesitated, but as another car full of potential witnesses passed them Thomas decided that they could not leave her alive.  Roman stopped the car at the parking lot of restaurant Girenbad. 

The police report stated that Roman now placed his hand on Andreas belly and shouted "Die ist ja tot!" (She really is dead!).  Roman wanted to drive the car over a slope to kill himself and the others, but Thomas succeeded in calming him a bit, and the three drove on to dispose of the corpse by a nearby creek. They removed some of her clothing in attempt to imply rape as the motive for the crime.

They then drove to Thomas home, spent the night together and bragged about the incident to a friend. On Sunday they decided that the corpse should be moved to somewhere more secure. They returned to the creek to pick up Andreas body and drove aimlessly towards Kyburg with the corpse in the cars trunk. Eventually they tossed her body into a hidden gully. However, on Monday, workers on the road stumbled across her body and the perpetrators of the murder were arrested. All eventually confessed to their part in the crime. 

The 20-year-old Roman was sentenced to 30 months in an Arbeitserziehungsanstalt (work education institute). Thomas (21), received a sentence of 14 years of jail, Patrick (23) 16 years. The parents and brother of the victim were awarded  300,000 Swiss francs in compensation to be paid by the three perpetrators.

===Style===

Amateur actors play nearly all of the principal roles and a good majority of the filming locations are original sites, as the Dogme style demands. Joy Ride violates some of the rules of Dogme, but it is not unusual for productions employing its principles to fail to adhere strictly to one or two of the stringent rules. One clear transgression is the importing of a soundtrack onto the final cut, violating the rule whereby all sound must be naturally recorded.  {{cite news
|url=http://www.liberation.fr/cinema/0101409188-une-viree-en-enfer
|title=Une virée en enfer
|last=Bayon
|date=4 October 2002
|publisher=Liberation
|language=French
|accessdate=15 December 2009}}
 

== Storyline ==
A clique of young people spend their time driving around the city, drinking in trendy nightspots and smoking joints. An outsider, Sandra (Claudia Knabenhans), who takes up with the clique, falls in love with one of its members, Daniel (Andri Zehnder). Their relationship remains diffuse and directionless, provoking tension and threatening to cause a schism within the group. 

==Partial cast==
* Andri Zehnder as Daniel
* Sebastian Hölz as Andi
* Claudia Knabenhans as Sandra
* Stephan Krauer as Max
* Edward Piccin as Bruno
* Charlotte Schwab as Sandras mother
* Jaap Achterberg as Sandras father
* Elisabeth Niederer as Daniels mother
* André Jung as Daniels stepfather

==References==
 

== External links ==
*  

 
 
 
 
 