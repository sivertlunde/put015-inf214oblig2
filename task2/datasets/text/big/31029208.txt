Las tres viudas de papá
{{Infobox film
| name           =Las tres viudas de papá
| image          = 
| image size     =
| caption        =
| director       =Miguel Zacarías
| producer       =Manuel Sereijo
| writer         = 
| narrator       =
| starring       =Sara García
| music          = 
| cinematography = 
| editing        =
| distributor    = Grovas y Cía
| released       =3 October 1942
| runtime        =
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1942 Mexico|Mexican comedy film directed by Miguel Zacarías.       
It stars Sara García.    Produced by Manuel Sereijo, it was distributed by Grovas y Cía.    

The film was one of three successive Zacarías comedies starring Chato Órtín and Sara García.    
It was the fourth sequel to Zacaríass Los enredos de papá, and maintained the same quality as the first three.   
The others were Papá se desenreda and Papá se enreda otra vez.   

The film premiered on 3 October 1942 in Mexico City. 
It was included in the 1959 list of shows censored by the Mexican League of Decency.   

==Cast==
*Carolina Barret		
*Antonio R. Frausto
*Sara García	 		
*Carlos López Moctezuma 		
*Chel López 		
*Miguel Montemayor 	 		
*Leopoldo Chato Ortín
*Blanca Rosa Otero 		
*Virginia Serret 		
*Virginia Zurí   

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 