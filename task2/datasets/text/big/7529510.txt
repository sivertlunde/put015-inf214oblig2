Thamizh
{{Infobox film
| name = Thamizh
| image = "Thamizh,_2001".jpg
| caption = Movie Poster Hari
| producer = Amutha Durairaj
| writer = Hari Prashanth Simran Simran Nassar Vadivelu
| music = Bharadwaj Priyan
| editing = V. T. Vijayan
| studio = Deivanai movies 
| released =  
| country = India
| runtime =
| language = Tamil
}}
 2002 Tamil Tamil language Hari and Prashanth and Simran in the leading roles, while Nassar, Vadivelu and Ashish Vidyarthi among others portrayed supporting roles. Featuring music composed by Bharadwaj (music director)|Bharadwaj, Thamizh opened to positive reviews upon release in April 2002.

==Plot==
Thamizh (Prashanth (actor)|Prashanth) leads a happy life with his mother (Manorama (Tamil actress)|Manorama) and sister-in-law (Urvasi). His brother(Livingston (actor)|Livingston) is working in Kuwait and Thamizh too dreams of joining him there. Meenakshi (Simran (actress)|Simran), their tenant and Thamizh fall in love. When the goons of Periyavar (Ashish Vidyarthi) injure Thamizhs niece, he stops Periyavars car on the road and questions him. He then beats up one of Periayavars goons when insulted. This makes him Periyavars target and though he tries to withdraw from the violence, he is forced to join forces with Rathnam (Nasser), Periyavars sworn enemy and eventually defeats him.

==Cast== Prashanth as Thamizh Simran as Meenakshi
*Nassar as Rathnam Livingston as Thamizhs brother
*Ashish Vidyarthi as Periyavar
*Vadivelu as Moorthy Manorama as Thamizhs mother Urvashi as Kalaichelvi
*Delhi Ganesh as Meenaskis father
*K. S. Ravikumar as Inspector
*"Minnal" Deepa as Moorthys Sister

==Production==
The film marked the debut of director  . http://www.rediff.com/movies/2002/may/15pra.htm  For his role, Prashanth worked out at the gym, grew a beard and began smoking cigarettes to get into character. Shooting commenced in Chennai and proceeded in locations like Mumbai, Kolkata and Delhi. Scenes were also shot in Sikkim, reportedly becoming the first time that a Tamil film shot there. A few scenes were also later picturised on Prashanth, Charlie, Vadivelu, and Crane Manohar in Karaikudi.  Prashanth had performed a lengthy dialogue in front of Madurai Meenakshi Temple which gained accolades from the onlookers.  Prashanth shot action scenes for the film through pain after he had injured his knee during the making of Majunu (2001). 
 Vijay starrer Thamizhan created confusion with the producer of both films unable to accommodate any changes.  The film, made at a cost of 20 million rupees, failed to get a distributor before release due to competition from other films, so producer Amudha Durairaj marketed the venture herself. 

==Release==
The film gained positive reviews upon release, with a critic noting the "debutant director has woven an action packed entertainer and has etched out the roles well".  Another critic wrote "When one leaves the theatre, one gets the satisfaction of watching a good film. This feeling has been rare in recent lives. Thamizh has turned out as the thirst quencher".  Malathi Rangarajan of The Hindu noted "Thamizh is backed by a strong storyline and a significant end   and the positive twist is appealing." 

The film became a box office success despite opening with little publicity, with positive word of mouth significantly helping the films prospects. 

==Soundtrack==
{{Infobox album |  
| Name        = Thamizh
| Type        = soundtrack
| Artist      = Bharadwaj
| Cover       = 
| Released    = 2002
| Recorded    = 2002 Feature film soundtrack |
| Length      = 
| Label       = The Best Audio
| Producer    = Bharadwaj
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Bharadwaj. The soundtrack, released in 2002, features 7 tracks .

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)
|-  1 || Azhakana ||  
|- 2 || Kaathelenum || Harish Raghavendra 
|- 3 || Kannukkull || Unni Krishnan, Swarnalatha 
|- 4 || Penne || Manikka Vinayagam
|- 5 || Rosappu || Yugendran, Anuradha Sriram 
|- 6 || Mano 
|- 7 || Sujatha 
|}

==References==
 

 

 
 
 
 
 
 
 
 
 