Matthew and Son (film)
{{Infobox film
| name           = Matthew and Son
| image size     = 
| image	=	
| caption        = 
| director       = Gary Conway
| producer       = Damien Parer
| writer         = Christine Schofield Marcus Cole
| based on       = short story by Bert Deling Michael Aitkens
| starring       = Paul Cronin Darius Perkins Paula Duncan
| music          = 
| cinematography = 
| editing        = 
| distributor    = Channel Ten
| studio         = Television House Films
| released       = 5 August 1984
| runtime        = 75 mins
| country        = Australia
| language       = English
| budget         = $800,000 
}}
Matthew and Son is a 1984 Australian TV movie about a single parent who is a police surgeon. It was filmed May to June 1984 and is a pilot for a TV series that never eventuated. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p106 

A young Nicole Kidman appears in a small role.

==Production==
The film was one of several projects Paul Cronin made when at Channel Ten, and was done for Johnny Youngs company. Cronin:
 It was based on the life of Dr John Birrell, the police surgeon, who was instrumental in the .05 and seatbelt legislation. A fascinating man - I met him, and he was a great character to play. Ten Brisbane, Ten Melbourne and Ten Adelaide committed to a series - they had a 7:30 timeslot allocated for it and all - and the only fly in the ointment was Ten Sydney. They would not commit. The telemovie rated well when it did go to air. It was a very, very well constructed show, but Johnnys company collapsed after that, and I left channel Ten.  

==References==
 

==External links==
*  at IMDB

 
 
 


 