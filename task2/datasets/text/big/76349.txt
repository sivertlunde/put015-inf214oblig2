Shock Corridor
{{Infobox film
| name           = Shock Corridor
| image          = ShockCorridor Poster.jpg 
| image_size     = 
| border         = 
| alt            = 
| caption        = German theatrical poster
| director       = Samuel Fuller
| producer       = Samuel Fuller
| writer         = Samuel Fuller
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Peter Breck Constance Towers Gene Evans James Best
| music          = Paul Dunlap
| cinematography = Stanley Cortez
| editing        = 
| studio         = 
| distributor    = Allied Artists Pictures Corporation (USA)
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Shock Corridor is a 1963 film, directed and written by Samuel Fuller.  The film tells the story of a journalist who gets himself committed to a mental hospital in order to track an unsolved murder. 

==Plot==
Journalist Johnny Barrett (Peter Breck) thinks that the quickest way to a Pulitzer Prize is to uncover the facts behind a murder at a mental hospital. He convinces an expert psychiatrist to coach him to appear insane; this involves relating imaginary accounts of incest with his "sister", who is impersonated by his striptease|exotic-dancer girlfriend (Constance Towers). Barrett convinces the authorities and is locked up in the institution where the murder took place. While pursuing his investigation, he is disturbed by the behavior of his fellow inmates. 

The three witnesses to the murder were driven insane by the stresses of war, bigotry or fear of nuclear annihilation.    
 brainwashed into becoming a Communist. Stuart was ordered to indoctrinate a fellow prisoner, but instead the prisoners unwavering patriotism reformed him. Stuarts captors pronounced him insane and he was returned to the US in a prisoner exchange, after which he received a dishonorable discharge and was publicly reviled as a traitor. Stuart now imagines himself to be Confederate States of America General J.E.B. Stuart. 
 first Negro white nationalist dogma.

* Boden was an atomic scientist scarred by the knowledge of the devastating power of intercontinental ballistic missiles.  He has regressed to the mentality of a six-year-old child.
 shock treatment. Barrett begins imagining that his girlfriend really is his sister, and experiences many other symptoms of mental breakdown. He learns the identity of the killer and violently extracts a confession from him in front of witnesses and writes his story. The damage to his mind is irreparable, however, and he never leaves the hospital.

==Cast==
*Peter Breck	 ... 	Johnny Barrett
*Constance Towers	... 	Cathy
*Gene Evans	... 	Boden
*James Best	 ... 	Stuart
*Hari Rhodes	... 	Trent Larry Tucker	... 	Pagliacci
*Paul Dubov	... 	Dr. J.L. Menkin
*Chuck Roberson	... 	Wilkes
*Neyle Morrow	... 	Psycho John Matthews	... 	Dr. L.G. Cristo
*Bill Zuckert	... 	Swanee Swanson John Craig	... 	Lloyd
*Philip Ahn	... 	Dr. Fong

==Historical importance==
In 1996, Shock Corridor was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==References in film==
* In The Naked Kiss (1964), another film directed by Fuller, and starring Towers, the theater outside the bus station is playing Shock Corridor.  The Dreamers (2003), the main character is watching Shock Corridor at the beginning.

==Notes==
 

==External links==
* 
*  
*  at Rotten Tomatoes

==See also==
*Mental illness in film
*One Flew Over the Cuckoos Nest (film)

 

 
 
 
 
 
 
 
 
 
 
 
 
 