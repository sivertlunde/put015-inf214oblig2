Ooruku Nooruper
{{Infobox Film
| name           = Ooruku Nooruper
| image          = 
| image_size     = 
| caption        = 
| director       = B. Lenin
| producer       = Lakshmanan Suresh
| writer         =  
| narrator       = 
| based on       =  
| starring       = Hans Kaushik, G.M. Sundhar
| music          = Arvind Jayashankar
| cinematography = Alphonse Roy
| editing        = Suresh Urs
| distributor    = National Film Development Corporation of India
| released       = 2001
| runtime        = 97 min.
| country        = India Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Tamil film by film editor B. Lenin. The screenplay was based on a 1979 novel of the same name by Jayakanthan.   

==Synopsis==
The story follows Balan (Kaushik), a young artist who becomes disillusioned by the current political situation of his society. He joins the revolutionary organization Ooruku Nooruper, accidentally kills a priest and is sentenced to death. The film examines various issues related to capital punishment.

==Accolades==

===National Film Awards===
* National Film Award for Best Direction – Ooruku Nooruper (2002).   
* National Film Award for Best Feature Film in Tamil – Ooruku Nooruper (2002). 

==References==
 

==External links==
*http://www.nfdcindia.com/view_film.php?film_id=128&categories_id=6
* 

 

 
 
 
 
 
 
 


 