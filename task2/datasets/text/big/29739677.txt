Khuda Kasam
{{Infobox film
| name           = Khuda Kasam
| image          = KhudaKasam2010Film.jpg
| caption        = Promotional poster
| director       = K C Bokadia
| producer       = Amit Kumar Bokadia Malook Nagar Tabu Farida Jalal Govind Namdeo Ashish Vidyarthi Mukesh Rishi Daisy Bopanna
| writer         = K C Bokadia
| music          = Aadesh Shrivastava
| cinematography = 
| editing        = 
| studio         = Sangeeta Pictures
| distributor    = 
| released       =  
| country        = India
| language       = Hindi
}}
Khuda Kasam  is a Hindi action film, directed by K C Bokadia and produced by Amit Kumar Bokadia and Malook Nagar. The film was released on 26 November 2010 under the Sangeeta Pictures banners. The film was earlier titled "The Challenge".    

==Plot==
Sunny Deol plays Hussain, an honest Muslim truck driver who is forced to take on the system when he is falsely implicated in a crime. Neetu Singh (Tabu (actress)|Tabu) is a police officer who is investigating the murder of the Chief Minister. While investigating the murder she falls prey to some criminal who play mind games with her. What happens next is revealed at the climax. The movie has political and social theme as its backdrop.   

==Cast==

*Sunny Deol Tabu
*Farida Jalal
*Govind Namdeo
*Ashish Vidyarthi
*Mukesh Rishi
*Daisy Bopanna

==References==
 

==External links==
*  
*  

 
 
 


 