My Yiddishe Momme McCoy
{{Infobox film
| name           = My Yiddish Momme McCoy
| image          =
| image_size     =
| caption        =
| director       =Bob Giges
| producer       = Bob Giges
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1991
| runtime        = 20 min.
| country        =   English   Yiddish
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 1991 by Bob Giges about his 90-year-old Jewish grandmother who fell in love and married an Irish people|Irish-Catholic named Bernie McCoy. Interfaith marriage has always been an issue in the Jewish community, but in the early twentieth-century it was particularly taboo. My Yiddish Momme McCoy addresses the prejudices the couple faced and the challenges that their divergent cultures presented them.

==Summary== Orthodox parents. She was an obedient daughter who speaks about her parents with great respect, admiring their dedication to their religious observance and wanting to live a life of which they approved. However, in her youth, Belle took a secretarial job under McCoy and soon fell for his romantic advances. McCoy fought and was injured in the First World War, and, from his hospital bed, he wrote Belle 52 love letters. When McCoy came home, they began an affair, but Belle was adamant that she could never marry him because doing so would directly oppose her parents expectations.
 conversion process to enter the Jewish faith. After watching the twenty-eight-year-old man undergo circumcision without anesthesia, Belle’s father said, “He could murder a man, and I would forgive him.”

Not everyone accepted their marriage. Belle’s uncle was disgusted that his niece had married a gentile. Instead of joining the married couple in celebration, her uncle sat shiva (Judaism)|shiva, a Jewish mourning ritual that typically accompanies a funeral.

The documentary explores how conflicts due to their diverging cultural backgrounds, were constantly emerging, despite McCoys conversion. While Belle insisted on keeping a kosher home so her parents could eat at her house, McCoy would forget and mix up the meat and milk pots. He would also attend Catholic services from time to time and give his children Christmas presents, but Belle insisted his Christian activities were cultural rather than religious. “As far as Im concerned,” she says, “he lived and died a Jew.”

Old photographs of the couple and an extended interview with Belle - during which she occasionally stops to sing Yiddish songs - are combined to tell the story of a Belle Demners long and happy marriage.
 
 
 

==See also==
*Jewish intermarriage
*Conversion to Judaism
*marriage
*anti-Semitism
*race relations
*racism
*Judaism

==External links==
* 
* 

 
 
 
 
 
 
 
 

 