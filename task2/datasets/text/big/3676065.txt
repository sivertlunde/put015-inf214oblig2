Fire Over England
 
 
{{Infobox film
| name           = Fire Over England
| image          = Fire-over-england-1937.jpg
| image size     = 220px
| caption        = Film poster
| director       = William K. Howard
| producer       = Erich Pommer Alexander Korda
| based on       =  
| screenplay     = Clemence Dane Sergei Nolbandov
| starring       = Laurence Olivier Vivien Leigh Flora Robson Leslie Banks
| music          = Richard Addinsell
| cinematography = James Wong Howe
| editing        = Jack Dennis
| distributor    = United Artists
| released       =  
| runtime        = 92 min.
| studio         = London Film Productions
| country        = United Kingdom English
| budget         =
}}
 Fire Over Gone with the Wind. The film is a historical drama set during the reign of Elizabeth I focusing on Englands victory over the Spanish Armada. 

==Plot==
During the reign of Queen Elizabeth I, England is concerned by the impending arrival of the Spanish Armada. In 1588, relations between Spain and England are at breaking point. With the support of Queen Elizabeth I (Flora Robson), English privateers such as Sir Francis Drake regularly capture Spanish merchantmen bringing gold from the New World.
 Lord Treasurer, Lord Burleigh Robert Dudley, Earl of Leicester (Leslie Banks). Burleighs beautiful granddaughter Cynthia (Vivien Leigh) is one of Elizabeths ladies-in-waiting, and the ageing queen is plagued by jealousy of the girls attractiveness and vitality.

In a sea battle between the Spanish, led by Don Miguel (Robert Rendel), and the English, led by his old friend Sir Richard Ingolby (Lyn Harding) the English are captured. Miguel allows Richards son Michael (Laurence Olivier) to escape. Michael washes ashore on Miguels estate, and his wounds are tended to by Miguels daughter Elena (Tamara Desni), who quickly becomes enamoured of the handsome Englishman. As the months pass, Michael recovers and laments being apart from Cynthia, his sweetheart, but is nonetheless impressed by Elenas charms. 

Miguel brings Michael the sad news that Sir Richard, his father, has been executed as a heretic. The grieving Michael denounces his rescuers and flees to England in a small fishing boat. When he is granted an audience with the Queen he urges her to fight the Spanish menace by whatever means necessary, and swears undying loyalty to her. Elizabeth is flattered by the young mans fervent devotion and later has an opportunity to take advantage of his offer of service when Hillary Vane (James Mason), an Englishman spying for Spain, is killed before the names of his English co-conspirators can be uncovered.

Michael, disguised as Vane, goes to the court of King Philip II of Spain (Raymond Massey) to get the letters that will set into motion a plan to assassinate Elizabeth. At the palace Michael meets Elena.  Her father has been killed by the English and she is now married to Don Pedro (Robert Newton), the palace governor. Elena keeps Michaels identity a secret as long as she can, but finally must tell her husband out of loyalty to him. 

Philip sees through Michaels disguise and orders his arrest. Pedro helps him escape so that it will not be discovered that his wife aided a heretic. While Michael is returning home, the Spanish Armada sails against England and Elizabeth addresses her army in Tilbury. Michael meets her there and reveals the names of the traitors. Elizabeth knights Michael before confronting the six traitors, inviting them to fulfill their plot and kill her. Overwhelmed with shame, they agree to accompany Michael on a mission to deploy fire ships in a night attack on the Armada, massed off the coast of England.

The tactic succeeds, and Elizabeth allows Michael and Cynthia to be wed.

==Cast==
 
* Flora Robson as Queen Elizabeth I of England
* Raymond Massey as King Philip II of Spain the Earl of Leicester
* Laurence Olivier as Michael Ingolby
* Vivien Leigh as Cynthia Lord Burleigh
* Tamara Desni as Elena
* Lyn Harding as Sir Richard Ingolby
* George Thirlwell as Mr. Lawrence Gregory
* Henry Oscar as the Spanish Ambassador
* Robert Rendel as Don Miguel
* Robert Newton as Don Pedro
* Donald Calthrop as Don Escobal Charles Carson as Admiral Valdez
* James Mason as Hillary Vane, an English traitor
 

==Production==
With the working title of Glorianna, principal photography took place at Denham Studios, where a large water tank was used to launch the model ships representing the Spanish Armada and the English naval defenders. Originally  Conrad Veidt was to star, but Alexander Korda saw the production as a star vehicle for Vivien Leigh, who was under contract to Korda.   Turner Classic Movies. Retrieved: 2 February 2015.  Along with the historical drama that was portrayed, Fire Over England was also a costume romance that served to showcase Leigh and Olivier, a real-life romantic couple. 

==Reception==
Fire Over England was the first British film to have its U.S. premiere at Los Angeles. In the review in Variety (magazine)|Variety, the comment was "This is a handsomely mounted and forcefully dramatic glorification of Queen Bess. It holds a succession of brilliantly played scenes, a wealth of choice diction, pointed excerpts from English history and a series of impressive tableaux." The League of Nations Committee on Motion Pictures awarded the 1937 Cinema Medal of Honor to Fire Over England. 

==See also==
* List of films in the public domain

==References==
Notes
 

Bibliography
 
* Vermilye, Jerry. The Great British Films. New York: Citadel Press, 1978. ISBN 978-0-8065-0661-6.
 


==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 