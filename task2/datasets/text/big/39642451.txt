No 73, Shanthi Nivasa
{{Infobox film
| name       = No 73, Shanthi Nivasa
| image      = No 73, Shanthi Nivasa.jpg
| director   = Sudeep
| producer   = Kiccha Creations
| story      = 
| screenplay = 
| starring   =   Ramani Bharathwaj
| cinematography = B. S. Kemparaj
| editing    = Rangaraj
| banner     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| running time = 157 min
| budget     = 
| gross      = 
| website    =  
}}
 Kannada musical-family-drama film directed by Sudeep featuring Sudeep, Anu Prabhakar, Deepa in the lead roles. The film features background score and soundtrack composed by Bharathwaj and Produced by Kiccha Creations. The film released on 15 June 2007.  This film is inspired by Hindi movie Bawarchi and Tamil movie Navagraham. 

==Cast==
* Sudeep ... Raghu (Cook) Vishnuvardhan ... Guest Role
* Shivrajkumar ... Narrator (Guest Role)
* Anu Prabhakar ... Neetha
* Deepa Bhaskar ... Radha Master Hirannayya ... Kailasanatha
* Srinivasa Murthy ... Ramanath
* Ramesh Bhat ... Kashinath
* Vaishali Kasaravalli ... Seethadevi Chitra Shenoy ... Shobha Arun Sagar ... Dance Guruji
* Komal ... Vishwanath urf Gandharva Rohit ... Chintu Deepu ... Arun     and others.

==Soundtrack==
{{Infobox album
| Name = #73, Shanthi Nivasa (2007)
| Type = soundtrack Ramani Bharathwaj Kannada
| Label = Anand Audio
| Producer = Kiccha Creations
| Cover = 
| Released    =  January 01, 2009 Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}} 

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Geeya Geeya Thirugo Bhoomi
| lyrics1 	= K. Kalyan
| extra1        = S. P. Balasubrahmanyam
| length1       = 
| title2        = Preethi Endare Heegene
| lyrics2 	= K. Kalyan
| extra2        = Rajesh Krishnan
| length2       = 
| title3        = Thayata Thayata Kalyani
| lyrics3 	= K. Kalyan
| length3       = 
| title4        = Adaddella Olledaytu Arun Sagar Master Hirannayya
| lyrics4       = K. Kalyan
| length4       =
| title5        = Hrudaya Hrudaya Srinivas
| lyrics5       = V. Nagendra Prasad
| length5       = 
| title6        = Preethi Endare Heegene
| lyrics6 	= K. Kalyan Ramani Bharathwaj
| length6       = 
| title7	= Geeya Geeya Thirugo Bhoomi (Remix)
| lyrics7 	= K. Kalyan
| extra7        = S. P. Balasubrahmanyam
| length7       = 
| title8        = Ondu Olle Katheya Heluve
| extra8        = Sudeep and Shivrajkumar
| lyrics8       = K. Kalyan
| length8       = 
| title9        = Bandu Nodu Ramani Bharathwaj
| lyrics9       = K. Kalyan
| length9       = 
}}

==References==
 

==External links==
*  

 