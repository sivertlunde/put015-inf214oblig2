When the Evening Bells Ring (1951 film)
{{Infobox film
| name = When the Evening Bells Ring
| image =
| image_size =
| caption =
| director = Alfred Braun
| producer = Willie Hoffmann-Andersen 
| writer =  Hans Scheffel   Werner P. Zibaso  
| narrator =
| starring = Willy Birgel   Maria Holst   Paul Hörbiger   Hans Holt
| music = Willy Schmidt-Gentner 
| cinematography = Georg Krause 
| editing =  Erwin Marno    
| studio = Apollo-Film 
| distributor = Gloria Film
| released = 21 December 1951
| runtime = 88 minutes
| country = West Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} silent film of the same title.

==Cast==
* Willy Birgel as Albrecht von Finke 
* Maria Holst as Gloria Römer 
* Paul Hörbiger as Lehrer Storm  
* Hans Holt as Michael 
* Julia Fjorsen as Rosemarie 
* Käthe Haack as Frau Brenda  
* Peter Voß as Gutsherr von Brenda  
* Rudolf Platte
* Hilde Körber
*Alfred Braun
* Aribert Wäscher 
* Hilde Sessak 
* Otto Gebühr

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 


 