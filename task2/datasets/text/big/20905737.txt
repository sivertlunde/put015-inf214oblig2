Everlasting Moments
{{Infobox film
| name           = Everlasting Moments
| image          = Everlasting moments poster.PNG
| caption        = US theatrical poster
| director       = Jan Troell
| producer       = Thomas Stenderup
| writer         = Niklas Rådström Jan Troell Agneta Ulfsäter-Troell
| narrator       = Callin Öhrvall
| starring       = Maria Heiskanen Mikael Persbrandt Jesper Christensen
| music          = Matti Bye
| cinematography = Jan Troell Mischa Gavrjusjov
| editing        = Niels Pagh Andersen
| distributor    = Sandrew Metronome (Nordic countries) IFC Films (US) Icon Film Distribution (UK)
| released       =  
| runtime        = 131 minutes
| country        = Sweden
| language       = Swedish Finnish
| budget         = $7 million   
| gross          = 
}}
Everlasting Moments ( ) is a 2008 Swedish drama film directed by   and As White as in Snow, which are both set around the same period. 
 Best Foreign Best Foreign Language Film at the 81st Academy Awards, but wasnt selected as one of the final nominees.

==Cast==
* Maria Heiskanen as Maria Larsson
* Mikael Persbrandt as Sigfrid Larsson
* Jesper Christensen as Sebastian Pedersen Emil Jensen as Englund
* Ghita Nørby as Miss Fagerdal
* Hans Henrik Clemensen as Mr. Fagerdal
* Amanda Ooms as Mathilda
* Antti Reini as Captain
* Birte Heribertsson as Aunt Tora
* Claire Wikholm as Grandmother Karna
* Nellie Almgren as Maja age 7-9
* Callin Öhrvall as Maja age 14-22
* Ann Petrén as Ida
* Maria Lundqvist as Miss Petrén
* Sanna Persson as Månse-Lotta
* Maria Kulle as Aunt Anna
* Hans Alfredson as Prison Guard
* Livia Millhagen as Ingeborgs Mother
* Max Eskilsson as Sven age 14-17
* Rickard Nygren as Police
* Alexander Kathy as Anton Nilson
* Lukas Wägbo as Algot Rosberg

==Production== The Emigrants in the early 1970s.  Agneta Ulfsäter-Troell, Jan Troells wife, started doing research and interviews in 1986 with Maja Larsson, Maria Larssons daughter, who was a cousin to Ulfsäter-Troells father. During her research she found Marias pictures, which were used as inspiration for the pictures seen in the film. The material wasnt organized, but when a person at the Swedish Film Institute heard about the story and how Jan Troell was interested in turning it into a film, an early process for a manuscript was started. 

The first official meeting took place in early 2004. Troell said that the casting choice of Maria Heiskanen and Jesper Christensen, both of whom had starred in Troell films previously, had always been "obvious."  Two years before filming started, Troell met Mikael Persbrandt at a film festival in Sweden and started to imagine him in the role of Sigfrid. Persbrandt then contacted Troell himself and persuaded him into giving him the role.  A major difference between the film and the actual story is that the real Maria Larsson lived in Gothenburg, while the film takes place in Malmö, where Jan Troell himself comes from. 

Filming took place between 26 February and 1 June 2007 in Malmö and  . Retrieved on 12 February 2009. 

==Release==
The film premiered as part of the Masters selection at the 2008 Toronto International Film Festival.  On 24 September the same year it was released in Sweden.  IFC Films aqcuired the American distribution rights and gave the film a limited release on 6 March 2009. At its peak it was running in 30 theatres during the same weekend.  It was released in the United Kingdom on 22 May 2009 through Icon Film.  A US DVD and Blu-ray was released in June 2010 through The Criterion Collection, as part of a collaboration between The Criterion Collection and IFC Films. 

==Reception==
The film has a 91% "fresh" rating at Rotten Tomatoes based on 97 reviews, with an average rating of 7.5 out of 10.  The average rating from 21 reviews collected at the Swedish-language site Kritiker.se was 4.0 out of 5. 

==Awards and nominations==
In addition to the awards and nominations, Everlasting Moments was also Swedens submission for the Academy Award for Best Foreign Language Film at the 81st Academy Awards.  It was among the nine films that made it to the January shortlist, but wasnt selected as one of the final nominees. 

{| class="wikitable"
|-
! Award !! Category !! Name !! Outcome
|-
| rowspan="1"| 66th Golden Globe Awards || Best Foreign Language Film || ||  
|- Best Film || Thomas Stenderup ||  
|- Best Actress || Maria Heiskanen ||  
|- Best Actor || Mikael Persbrandt ||  
|- Best Supporting Actor || Jesper Christensen ||  
|-
| Special Achievement - Best Music || Matti Bye ||  
|- Best Direction || Jan Troell ||  
|- Best Supporting Actress || Amanda Ooms ||  
|- Best Screenplay || Niklas Rådström, Jan Troell and Agneta Ulfsäter-Troell ||  
|-
| Best Cinematography || Jan Troell and Mischa Gavrjusjov ||  
|-
| rowspan="3"| Valladolid International Film Festival || Best Actress || Maria Heiskanen||  
|-
| Best Director of Photography || Jan Troell and Mischa Gavrjusjov ||  
|-
| Golden Spike || Jan Troell ||  
|}

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 