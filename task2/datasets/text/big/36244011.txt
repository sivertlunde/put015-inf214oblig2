Re-encounter
{{Infobox film name           = Re-encounter image          = File:Re-encounter_poster.jpg director       = Min Yong-keun   producer       = Shim Hyun-woo  writer         = Min Yong-keun  starring       = Yoo Da-in Yoo Yeon-seok music          = Kim Myung-jong Sung Ji-young Hong Ye-young cinematography = Na Hee-seok   editing        = Wong Su-ahn studio         = Secret Garden distributor    = Indiestory  released       =   runtime        = 107 minutes country        = South Korea language       = Korean budget         =   gross          =  film name      = {{Film name hangul         = 혜화,동  rr             = Hyehwa, Dong  mr             = Hyehwa, Tong}}
}} indie film written and directed by Min Yong-keun.  Starring Yoo Da-in and Yoo Yeon-seok, it is a coming of age story about two young people who fall in love but lose contact when the girl becomes pregnant and the boy leaves her to go to Canada; they meet again after five years to look for their child, who they believe has been adopted.

It premiered at the 15th Busan International Film Festival in 2010,  and was released in theaters on February 17, 2011.

==Plot==
When precocious teenager Hye-hwa realizes that she is pregnant, the assertive young woman seems to have everything under control. But her convictions come crashing down when her loving, docile boyfriend Han-soo disappears without a word, apparently having been exiled to Canada by his mother.

Five years down the road, Hye-hwa’s spunky attitude and fondness for colorful manicures have been replaced by a fixation with rescuing abandoned dogs when shes not grooming the creatures for a living. Mothering her widowed bosss son provides her some relief; she is wise and weathered far beyond her 23 years. The fragile equilibrium maintained by her routine lifestyle breaks, however, after an unwarranted re-encounter with Han-soo.

At first Hye-hwa refuses her exs approach, but her heart drops when he informs her that their child is actually well and alive — contrary to her understanding that the baby girl had died hours after birth. Han-soo explains that their daughter had been given up for adoption by their own grandmothers. Unable to help herself, Hye-hwa goes along with him in trying to track the baby down, leading to tragic consequences.    

==Cast==
*Yoo Da-in - Hye-hwa
*Yoo Yeon-seok - Han-soo 
*Park Hyuk-kwon - veterinarian Jung-hun
*Choi Hee-won - Na-yeon 
*Son Young-soon - Hye-hwas mother 
*Park Seong-yeon - Han-soos sister 
*Kil Hae-yeon - Han-soos mother 
*Kim Joo-ryung - Hwa-young 
*Kim Tae-in - Kindergarten director 
*Kwon Oh-jin

==Reception== indie film with unknown actors and a production budget of just   ( ), Re-encounter drew favorable reviews from critics. It attracted a little more than 10,000 viewers, which is a dream figure in the world of Korean indie films, where low production budgets, minimal promotion and short runs in theaters are the norm.   

Re-encounter was sponsored in part by the Korean Film Council and the Seoul Film Commission, who covered half of the films production costs. The films production team later made headlines for donating all of the proceeds from ticket sales to film and arts organizations such as the Seoul Independent Film Festival and MediACT. 

==Awards==
*2010 15th Busan International Film Festival: Best Director, Korean Cinema Today: Vision section - Min Yong-keun
*2010 36th Seoul Independent Film Festival: Independent Star Award - Yoo Da-in
*2010 36th Seoul Independent Film Festival: Kodak Award
*2010 36th Seoul Independent Film Festival: Best Film 
*2011 31st  
*2012 13th Asian Film Festival (Tours, France): Best Performance by an Actress - Yoo Da-in
*2012 13th Asian Film Festival (Tours, France): Jury Award

==References==
 

==External links==
*   at Naver  
*   at IndieStory
*  
*  
*  

 
 
 
 
 
 