Peregrina (film)
{{Infobox film
| name           =Peregrina 
| image          = 
| image size     =
| caption        =
| director       =Chano Urueta
| producer       =  Agustín Yáñez (story), José Luis de Celis (screenplay)
| writer         = 
| narrator       =
| starring       =  Jorge Mistral, Lilia del Valle, Carlos López Moctezuma  Jorge Pérez
| cinematography = Víctor Herrera
| editing        = Jorge Bustos	
| distributor    = 
| released       = 9 August 1951 (Mexico)
| runtime        = 114 min 
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 1951 Mexico|Mexican film. It was directed by 
Chano Urueta.

==Cast==
* Jorge Mistral	 ...	Miguel Obregón
* Lilia del Valle	 ...	Mariana Oneill
* Carlos López Moctezuma	 ...	Don Remigio
* Charles Rooner	 ...	Don Tomás Oneill
* Nora Veryán	 ...	Lupe
* Joaquín Cordero	 ...	Marcos Obregón
* Maruja Grifell	 ...	Bruna
* Manuel Trejo Morales		
* José Pardavé	 ...	Margarito

==External links==
*  

 
 
 
 

 