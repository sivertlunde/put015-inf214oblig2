Lonesome Jim
{{Infobox film
| name           = Lonesome Jim
| image          = Lonesome Jim DVD cover.jpg
| caption        = DVD cover
| director       = Steve Buscemi
| producer       = Jake Abraham, Galt Niederhoffer, Celine Rattray Daniela Taplin Lundberg Gary Winick
| writer         = James C. Strouse
| starring       = Casey Affleck Liv Tyler Kevin Corrigan Mary Kay Place Seymour Cassel Mark Boone Junior
| music          = Evan Lurie
| cinematography = Phil Parmet
| editing        = Plummy Tucker
| studio         = Plum Pictures
| distributor    = United States IFC Films International Lions Gate Entertainment
| released       =   (Sundance Film Festival)
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $500,000
| gross          = $174,815
}} depressed aspiring novelist who moves back into his parents home after failing to make it in New York City. Liv Tyler also stars as a good-hearted nurse who finds contentment through encouraging optimism in Jims glum world.

Lonesome Jim premiered at the 2005 Sundance Film Festival where it was nominated for the Grand Jury Prize  but it lost the award to Ira Sachs Forty Shades of Blue. 

==Plot==
Jim (Casey Affleck) is a perennially gloomy 27 year-old aspiring novelist from Goshen, Indiana who moved to New York City in hopes of finding success with his writing. After two years of barely making a living as a dog walker, he defeatedly decides to move back home to his parents house in Goshen.

Jims 32 year-old brother Tim (Kevin Corrigan) is a recently divorced father of two young girls whose business recently failed. Tim has moved back into his parents home and he works in the ladder factory thats owned and operated by their pessimistic father Don (Seymour Cassel) and overly cheerful mother Sally (Mary Kay Place). Jim has no interest in the family business and he resists pressure from Don to start working there.

Jim meets Anika (Liv Tyler), a nurse, in a bar and they end up having sex in a hospital bed, though Jim finishes embarrassingly early.

After an argument between the two brothers on whose life has been more pathetic so far, Tim, having previously made repeated unsuccessful attempts to commit suicide, drives his car into a tree in hopes of ending his life; he is gravely injured and hospitalized. Jim now finally gives in to Dons pressure to work in the factory by taking over Tims duties. He also takes over Tims job as the coach of a girls basketball team; the team, which has not scored a single point in the last 14 games, includes both of Tims daughters.

While visiting Tim is in the hospital, Jim runs into Anika, who works in another department.  They arrange a date, but on arriving to collect her, he discovers she is a single mother. Their relationship progresses, however.  Anika is sympathetic to Jims problems, and she decides to stand by him in encouragement even when he tries to convince her that its in her best interest to not be around him.

At the ladder factory, Jim encounters his uncle Stacy (Mark Boone Junior), who prefers to be called "Evil."  Jim seeks advice from Evil about premature ejaculation, and they become friends of a sort.  Evil offers Jim some recreational drugs and asks Jim to open a checking account for him so he can pay for things by mail.  Evil gives Jim $4,000, saying it is saved-up birthday and Christmas presents.
 drug dealer, but Jim cannot persuade him to confess.  Evil points out that Jim  will be implicated if he tries to report Evil, as he has opened an account with Evils cash and will test positive for drug use.  Eternal optimist that she is, Sally finds happiness in her new surroundings and makes friends with her fellow prisoners.  Eventually she is released on bail.

Despite working a job he hates and feeling responsible for his mothers imprisonment, Jim slowly allows his monumental depression to be dismantled by Anika and finds himself believing that life is worth living. Jim invites Anika and her son to move to New Orleans with him, but after Jim vacillates, she is offended and this seems to be off the cards.  Jim finally decides to leave town for New Orleans by himself, leaving a note for his parents promising not to take their love for granted again and revealing Evil as the drug dealer.  Anika shows up at the bus station to say good-bye, but seems to Jim miss the opportunity to reconcile with her.  Jim departs on the bus, but as Anika drives home with her son, Jim is seen running after them, luggage in hand.

==Cast==
* Casey Affleck as Jim
* Liv Tyler as Anika
* Kevin Corrigan as Tim
* Mary Kay Place as Sally
* Seymour Cassel as Don
* Mark Boone Junior as Stacy a.k.a. "Evil"
* Jack Rovello as Ben

==Production==
The film was originally a part of a deal with  , February 3, 2005. Accessed October 28, 2008.  More money was saved by recording the entire film onto a mini-DV digital video camera rather than a film camera.  

==Critical reception and box office==
During its theatrical run, Lonesome Jim never earned back its initial budget of $500,000; instead, the film grossed less than $155,000 domestically and less than $175,000 worldwide.  , Box Office Mojo. Accessed October 28, 2008. 

The film received mixed reaction from film critics. The aggregate review websites Rotten Tomatoes and Metacritic record a rating of 60 percent  and 54/100  respectively as of October 28, 2008. Film critic Roger Ebert of the Chicago Sun-Times awarded the film three stars out of four,  and it also received "Two thumbs up" on the film review television program At the Movies (U.S. TV series)|At the Movies with Ebert & Roeper co-hosted by Richard Roeper.  Mathew Turner of View London proclaimed "Lonesome Jim is one of the years best films, thanks to a superb script, terrific performances and Buscemis assured direction". 
Peter Travers of Rolling Stone awarded it three stars out of four, calling the film a "deadpan delight" and proclaiming "I cant recall having a better time at a movie about depression".  Critic Christopher Campbell declared the film "hilarious throughout. By far it is the funniest thing I saw during the   festival". 

Stephen Holden of The New York Times did not give the film a very favorable review, criticizing the films sense of humor by calling it "only as broad as the Mona Lisas smile" and criticizing Afflecks portrayal of Jim.  Lisa Schwarzbaum of Entertainment Weekly awarded the film a grade of C minus, writing that director Steve Buscemi "is stymied here by the inertia of his material". 

==See also==
 
* 2005 in film
* Cinema of the United States
* List of American films of 2005

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 