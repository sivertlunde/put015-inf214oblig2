The Hard Way (1943 film)
 
{{Infobox film
| name           = The Hard Way
| image          = The Hard Way 1943 movie poster.jpg
| caption        = Theatrical Poster
| director       = Vincent Sherman
| producer       = Jerry Wald
| writer         = Jerry Wald
| screenplay     = Daniel Fuchs Peter Viertel
| starring       = Ida Lupino Dennis Morgan Joan Leslie
| music          = Heinz Roemheld
| cinematography = James Wong Howe Thomas Pratt
| distributor    = Warner Bros.
| released       =  
| runtime        = 109 minutes
| country        = United States English
| budget         =
}}
 musical drama film directed by Vincent Sherman. The film was based on a story by Irwin Shaw which was reportedly
based on Ginger Rogers relationship with her first husband, Jack Pepper (whom she married in 1928 at age 17) and her own mother, Lela.  

==Plot==
 
Helen Chernen (Ida Lupino) is an ambitious woman, determined to once and for all become rich. She pushes her younger sister Katie (Joan Leslie) into a marriage with singer/dancer Albert Runkel (Jack Carson). Katie has no interest in the man, but is desperate to leave the poor conditions she and her sister live in, a dirty steel town. Runkels partner Paul Collins  (Dennis Morgan) sees what Helens real intentions are and tries to stop her from breaking Runkels heart.

Now living in wealthier surroundings, Helen tries to make a start of Katies career. She is able to put her on a Broadway play. Katie soon becomes a successful singer and actress, while Collins and Runkels act flounders. Runkel cant bear the idea of his wife having more success than himself. Also annoyed with Helens efforts to destroy his marriage, he eventually kills himself.

Meanwhile, Katies popularity rises to her head and she becomes a wild party girl. Her behavior eventually costs her her career. They later meet up with Paul, who is now a successful band leader. He falls in love with Katie and they start a relationship. However, things get complicated when Helen reveals she is in love with him as well. 

==Cast==
{| class="wikitable"   
|- Joan Leslie as Katie Chernen 
|- Jack Carson as Albert Runkel
|-
|}

* Gladys George as Lily Emery
* Faye Emerson as Ice Cream Parlor Waitress
* Paul Cavanagh as John "Jack" Shagrue
* Dolores Moran as Young Blonde (uncredited)

==Production==
Both Bette Davis and Ginger Rogers were initially offered the role of Helen, but both declined. Ida Lupino was then cast. Shaw wanted Howard Hawks or William Wyler to direct the film, but since they were busy with other projects producer Jerry Wald hired Vincent Sherman. Portions of a documentary film by Pare Lorentz were used to represent the mining town of Green Hill. To achieve a more realistic feel during the scenes that took place in Green Hill, neither Lupino nor Leslie wore makeup. The films first and last scenes were added at Jack Warners insistence that Lupino appear more glamorous in the opening scene.  

==Soundtrack==
*"I Love to Dance"
**(1942) (uncredited)
**Written by M.K. Jerome and Jack Scholl
**Played during the opening credits and at the end
**Sung by Gladys George at rehearsal with piano accompaniment
**Reprised at a show and sung and danced by Joan Leslie (dubbed by Sally Sweetland) and chorus
**Sung on a record by Leslie
**Played as background music often 

*"Am I Blue?"
**(1929) (uncredited)
**Music by Harry Akst
**Lyrics by Grant Clarke
**Sung by Dennis Morgan and Jack Carson in their vaudeville act
**Reprised by Joan Leslie (dubbed by Sally Sweetland)
**Played as background music often 

*"Tip Toe Through the Tulips with Me"
**(1929) (uncredited)
**Music by Joseph Burke
**Lyrics by Al Dubin
**Sung by Dennis Morgan and Jack Carson in their vaudeville act 

*"Youre Getting To Be a Habit With Me"
**(1932) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Played on a juke box in the ice cream parlor 

*"For You"
**(1930) (uncredited)
**Music by Joseph Burke
**Lyrics by Al Dubin
**Played on piano by Dennis Morgan and danced by Joan Leslie
**Played as background music 

*"(You May Not Be an Angel, But) Ill String Along With You"
**(1934) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung by Dennis Morgan and Jack Carson at a vaudeville show and danced by Joan Leslie 

*"Shuffle Off To Buffalo"
**(1932) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung and danced by Jack Carson and Joan Leslie (voice dubbed by Sally Sweetland) at a vaudeville show
**Played as background music 

*"Forty-Second Street"
**(1932) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Played as background music 

*"Shes A Latin From Manhattan"
**(1935) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Sung and danced by Jack Carson and Joan Leslie (voice dubbed by Sally Sweetland) in a nightclub 

*"I Get a Kick Out of You"
**(1934) (uncredited)
**Music and lyrics by Cole Porter
**Played offscreen by the nightclub band 

*"Lullaby of Broadway"
**(1935) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Played offscreen by the nightclub band 

*"About a Quarter to Nine"
**(1935) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Played on piano and danced by chorus girls at rehearsal 

*"Jeepers Creepers"
**(1938) (uncredited)
**Music by Harry Warren
**Lyrics by Johnny Mercer
**Sung by a chorus at a show 

*"My Little Buckaroo"
**(1937) (uncredited)
**Music by M.K. Jerome
**Lyrics by Jack Scholl
**Sung by a chorus in a montage 

*"With Plenty of Money and You"
**(1936) (uncredited)
**Music by Harry Warren
**Lyrics by Al Dubin
**Song by a chorus in a show during a montage 

*"You Must Have Been a Beautiful Baby"
**(1938) (uncredited)
**Music by Harry Warren
**Lyrics by Johnny Mercer
**Song by a chorus in a show during a montage 

*"Begin the Beguine"
**(1935) (uncredited)
**Music and lyrics by Cole Porter
**Played on a record 

*"Night And Day"
**(1932) (uncredited)
**Music and lyrics by Cole Porter
**Played by the band at the Embassy Club 

*"Goodnight, My Darling"
**(1942) (uncredited)
**Written by M.K. Jerome and Jack Scholl
**Played by the band at the Oakmont Lodge and
**Sung by Dennis Morgan

*"Theres a Small Hotel"
**(1936) (uncredited)
**Music by Richard Rodgers
**Lyrics by Lorenz Hart
**Played as background music at a theater

==Awards==
Ida Lupino was awarded a New York Film Critics Circle Award for Best Actress for her role in the film.

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 