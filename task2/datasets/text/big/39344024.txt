Big Calibre
{{Infobox film
| name           = Big Calibre
| image          = File:Bob Steele big calibre.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Title card
| film name      = 
| director       = Robert N. Bradbury
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Bob Steele
| music          = 
| cinematography = 
| editing        = 
| studio         = Supreme Pictures
| distributor    = 
| released       = March 8, 1935
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 horror Western Western film Bob Steele as Bob ONeill, a stockgrower who, seeking vengeance for his murdered father, goes after the murderer, crazed scientist Otto Zenz (Bill Quinn).

==Plot==
Rancher Bob ONeills father is gassed to death by lunatic Otto Zenz. In a bid to avenge his father, Bob tracks the scientist down, and they eventually have a showdown in the dry plains.   

==Reception==
Film critic Bob Magers considers Big Calibre to be one of Steeles finer films. 

==See also==
* Bob Steele filmography

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 
 