Soccer Dog: The Movie
 
 
 
{{Infobox film
| name           = Soccer Dog: The Movie
| image          = SoccerDogTheMovie.jpg
| caption        =
| director       = Tony Giglio
| producer       = John H. Brister Tony Giglio
| writer         = Daniel Forman James Marshall Jeremy Foley Burton Gilliam Sam McMurray
| music          = Victoria Dolceamore
| cinematography = Christopher Duddy
| editing        = Alan Z. McCurdy
| distributor    = Columbia TriStar Home Video
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
}}

Soccer Dog: The Movie is a 1999 film that follows a dog who has an uncanny ability to play Association football|soccer.

==Plot==
The movie begins with a now grown-up Alden (James Marshall) telling the story of his childhood in an orphanage, and how his parents set him on the steps of the Pelton Orphan home. He says that soccer was the only thing that made his life easier. When he was too old to stay in the orphanage, he went out and married his wife Elena (Olivia dAbo). Alden misses his days of soccer, so he reasons that the only way to enjoy it was to have a son to play it with. Alden and Elena go to the Pelton Orphanage, where Alden has to face the mean headmaster. He and Elena adopt Clay (Jeremy Foley). They bring Clay home and Clay isnt really into the spirit of things. Meanwhile, the setting shows  little boy playing with his dog, Kimble (who actually turns out to be Lincoln) in the park, then throws the ball over the fence of a construction site. The evil dogcatcher, Damon Fleming (Billy Drago) catches Kimble and takes him to the pound. The next day, Alden learns that Clay doesnt know how to play soccer. Clay goes to practice is confused by his coach, Coach Shaw, who doesnt really know anything about soccer. He tells the team he wants 100% from each of them from each quarter, and Sonny, whose father is a rich fat guy with two bodyguard, points out that in soccer there are no quarters, just two halves. Coach Shaw get irritated and tells Sonny that there is no A in team, then spells it T-E...go take a lap. (Coach Shaw has a nasty habit of telling his players to go take a lap when he says something wrong and he knows they are right.) Behind the scenes, when the dogcatcher goes to take a head count of the dogs, he finds Kimble isnt there and finds he has escaped through a sewage hole in the floor under his mat. Kimble is seen running away and removing the orange tag on his right leg. After that, the coach is explaining that soccer is a game of hand eye coordination. Clay raises his hand to ask a question, but is deterred by Vince, (Kyle Gibson) who tells him not to bother and that daddy has a feeling baby was dropped on his head when he was a baby. Meanwhile, the dogcatcher sets out to find Kimble and, while eating a blade of grass, finds the orange tag Kimble tore off. On the other hand, Clay is walking back home from practice when Vince catches up with him. they talk a little, but Clay doesnt really understand Vince because he calls people baby and calls himself daddy. Thats when Clay spots Kimble digging in a flower bed and calls to him, and Kimble comes. later that night, at dinner, Elena wants Clay to take violin lessons, and she and Alden sort of argue. Clay excuses himself to go feed Kimble, who he has named Lincoln and hid in his bathroom. Alden thinks that Clay is not feeling welcome, so he comes into Clays bedroom to find he was hiding a dog. In the middle of the soccer season, Sonnys dad thinks that Vince is getting too hot and that if he keeps showing off, Sonny was to give him a food poisoning pill. Sonny drops it in Vinces drink and Vince cant play. Clays dog, Lincoln, runs out on the field and the Coach appoints him to play. The referee tells this really old lady to check the book of soccer rules, which is a 7 inch tall book. The rules state that Lincoln can play for the team. Lincoln scores a goal minutes afterwards. At the end of the season, the team wins the championship. Lincolns original owners take their dog back, but Clay gets a puppy at the end that looks exactly like Lincoln, the dogcatcher disappears, and Alden becomes the coach of the Stampede.

==Sequel==
The movie generated one sequel, 2004s  .

==Cast== James Marshall &ndash; Alden
* Olivia dAbo &ndash; Elena Jeremy Foley &ndash; Clay
* Sam McMurray &ndash; Coach Shaw
* Burton Gilliam &ndash; The Mailman
* Billy Drago &ndash; Damon Fleming the Dog Catcher
* Kyle Gibson &ndash; Vince
* Randall May- Sexist P.E. Teacher
* Evan Matthew Cohen &ndash; Sonny
* Brocker Way &ndash; Berger
* Bill Capizzi &ndash; Vito
* Wilfred P. Rosa &ndash; the surprised Puerto Rican guy on the bleachers

==External links==
*  
*  

 

 
 
 
 
 
 
 
 