The Silence of the Hams
{{Infobox Film
| name           = The Silence of the Hams
| image          = The_silence_of_the_hams_poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Ezio Greggio
| producer       = Julie Corman Ezio Greggio
| writer         = Ezio Greggio
| narrator       = Ezio Greggio
| starring       = Ezio Greggio Dom DeLuise Billy Zane Joanna Pacuła Charlene Tilton Martin Balsam
| music          = Parmer Fuller
| cinematography = Jacques Haitkin
| editing        = Robert Barrere Andy Horvitch
| studio = Silvio Berlusconi Productions 30th Century Wolf
| distributor    = October Films 1994 
| runtime        = 81 minutes
| country        = Italy / United States English
| budget         = $3,000,000
}} The Silence of the Lambs and Psycho (1960 film)|Psycho.
 Michael Jacksons Thriller, and tongue-in-cheek references to the then-current state of American politics (such as a fight scene between Presidents George H. W. Bush and Bill Clinton). As a curiosity, there is a Mel Brooks cameo in this film, who made a number of well regarded parodies (Blazing Saddles, Young Frankenstein, Spaceballs).

==Plot==
The film follows rookie detective Jo Dee Fostar (Billy Zane), on his first case. The case involves a serial killer, wanted for over 120 murders. In order to find the killer, he must enlist the help of convicted murderer Dr. Animal Cannibal Pizza (Dom Deluise). However, during the investigation, his girlfriend, Jane Wine (Charlene Tilton), is asked by her boss to take a large sum of money to the bank. Instead of doing this, she leaves town with the money. While hiding, she decides to rest at the Cemetery Motel, which is later revealed to be a cemetery named Motel after its owner, Antonio Motel. Jo must then enlist the help of Det. Balsam (Martin Balsam) and Dr. Pizza to not only find the murderer, but his missing girlfriend. All of this takes the cast on many adventures at the Cemetery Motel. In the final confrontation, most characters are revealed to be somebody else in disguise.

==Cast==
*Ezio Greggio - Antonio Motel
*Dom DeLuise - Dr. Animal Cannibal Pizza
*Billy Zane - Jo Dee Fostar
*Joanna Pacuła - Lily Wine
*Charlene Tilton - Jane Wine
*Martin Balsam - Det. Martin Balsam
*Stuart Pankin - Insp. Pete Putrid
*John Astin - The Ranger Tony Cox - Jail Guard
*Mel Brooks - Checkout Guest (Uncredited)
*Phyllis Diller - Old Secretary
*Shelley Winters - Mrs. Motel (The Mother)
*Bubba Smith - Olaf
*Larry Storch - Sergeant
*John Carpenter - Trench coat man
*Eddie Deezen - Cameraman
*Pat Rick - President Bill Clinton

==Critical response==
The Silence of the Hams has a 0% "rotten" rating on review aggregate Rotten Tomatoes based on eight reviews. 

  criticised it for "a script staggeringly bereft of humour or invention, and a clumsy, amateurish direction that seems largely concerned with focusing on Charlene Tiltons breasts". Darren Bignell, "Silence of the Hams", Empire Online, http://www.empireonline.com/reviews/reviewcomplete.asp?FID=132614 

==References==
 

== External links ==
*  
*   at Rotten Tomatoes
*  

 
 
 
 
 
 
 
 
 