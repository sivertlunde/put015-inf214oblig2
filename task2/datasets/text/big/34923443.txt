Fighting Back (1982 film)
 

{{Infobox film
| name           = Fighting Back
| image          = Fighting Back Poster.jpg
| caption        = Fighting Back Poster
| director       = Lewis Teague
| producer       = Perm Presentations
| screenplay     = Thomas Hedley Jr. David Zelag Goodman
| starring = {{plainlist|
* Tom Skerritt
* Patti LuPone
* Michael Sarrazin
* Yaphet Kotto
}}
| music          = 
| cinematography =
| editing        = Nicholas C. Smith
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $3,335,948 USD
}}

Fighting Back (UK title: Death Vengeance) is a 1982 vigilante film written by Thomas Hedley Jr and David Zelag Goodman. The film was directed by Lewis Teague and takes place on the streets of Philadelphia starring Tom Skerritt, Patti LuPone, and Michael Sarrazin.

== Plot == deli in town. While driving with his wife, Lisa (Patti LuPone), DAngelo comes across a pimp known as Eldorado (Pete Richardson) brutalizing one of his prostitutes. Johns wife confronts the pimp and the pimp chases the DAngelos, ramming his car into the back of the DAngelos vehicle, injuring Lisa and killing their unborn baby. Johns mother Vera (Gina DeAngles) is assaulted in the neighborhood.

John decides to make a stand, organizing a neighborhood patrol composed of regular citizens who are also fed up with the crime in their neighborhood. They call themselves The Peoples Neighborhood Patrol, or “PNP” for short. The PNP has their own uniforms consisting of blue hats and vests that have a PNP logo on them, a headquarters to take phone calls, along with vehicles all containing their PNP logo and is led by John and his best friend Vince Morelli (Michael Sarrazin), a police officer. After DAngelos house is burglarized and their dog is killed, the film cuts to the reporters studio footage of Newark, New Jersey ten years after the 1967 Newark riots, self-defense classes in Beverly Hills, various target practice sessions and the Guardian Angels on patrol in New York City. With Vinces help, the police allow the PNP to patrol the neighborhood. However, the PNP seem to operate with no regard for the law and do as they please. To make their first stand and to introduce themselves to the neighborhood, the group goes to a dirty bar in town known for being a hot spot for criminals, which Eldorado and his men are known patrons of. John casually walks into the bar with the rest of the PNP behind him. John confronts the bartender (Allan Graf), trying to get answers as to who is responsible for mugging his mother. Things turn violent when the bartender laughs in John’s face, triggering an all-out brawl, but the PNP come out on top.

John and the PNP start gaining media attention, and the neighborhood starts to rally behind the PNP. The group starts taking out pimps, drug dealers, muggers, and thieves. The PNP operates above the law. John D’Angelo does what he wants, and his actions are seen as racial discrimination by a small portion of the African-American community. John D’Angelo meets with Ivanhoe Washington (Yaphet Kotto), a black leader of a similar vigilante movement. Ivanhoe presents John with the two men who mugged his mother, one of whom is white while the other is black. John beats up the black man, proving Ivanhoe’s point that John is guilty of discrimination.

With widespread media attention, John decides to run for councilman in the upcoming election. Just when things are looking good for the city, tragedy strikes when Vince is gunned down and killed at the hands of Eldorado and his men. In retaliation, John  organizes a large scale attack on the park where Vince was killed.  All members of the PNP head to the park, where they demand that everyone in the park clear out. When their demands are ignored, the PNP takes action, and starts clearing out the park by way of brute force. A large brawl soon erupts, and police arrive on the scene not long after. John spots Eldorado and chases after him; during the chase John is tackled and arrested by police. Eldorado manages to get away. While meeting with Police Commissioner (Ted Ross), John is given permission to take out Eldorado, waiting patiently on the roof above Eldorados vehicle. When Eldorado and his men enter the car, John drops a grenade though the vehicles roof. The grenade explodes, killing everyone inside the car.

John ends up winning the election, and a large celebration with family and friends takes place inside his deli. The PNP have cleaned up the neighborhood, and crime is no more. The final scene shows children playing in the very same park that was once occupied by criminals.

== Cast ==
*Tom Skerritt as John DAngelo
*Patti LuPone as Lisa DAngelo
*Michael Sarrazin as Vince Morelli
*Yaphet Kotto as Ivanhoe Washington
*David Rasche as Michael Taylor
*Lewis Van Bergen as Laz Burkofsky
*Earle Hyman as  Police Officer
*Ted Ross as Police Commissioner
*Frank Sivero as Frank Russo
*Pat Cooper as Harry Janelli 
*Gina DeAngelis as Vera D’Angerlo
*Allan Graf as Carl-The Bartender
*Donna de Varona as Sara Rogers
*Jonathan Adam Sherman as Danny D’Angelo
*Pete Richardson as Eldorado
*Joseph Rangno as Mike Pelyk
*Sal Richards as Bill Gallo Jim Moody as Lester Baldwin

== Production == Kensington section of Philadelphia, Pennsylvania

== Box Office ==
Opening the weekend of May 23, 1982, the film brought in $1,624,381 USD

== References ==

*  Fighting Back
*  Fighting Back
*  Fighting Back

== External links ==
 
*  at Yahoo Movies
*  at IMDB
*  at Nytimes

 

 
 
 
 
 