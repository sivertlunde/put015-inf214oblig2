Yesterday (1981 film)
{{Infobox film
| name = Yesterday
| image =
| image size =
| alt =
| caption =
| director = Lawrence Kent
| producer = Don Carmody André Link Lawrence Nesis
| writer = John Dunning Bill La Mond
| narrator =
| starring = Vincent Van Patten Claire Pimparé
| music = Paul Baillargeon
| cinematography = Richard Ciupka
| editing = Debra Karen
| studio =
| distributor = Levy Films
| released =  
| runtime = 95 min
| country = Canada English
| budget =
| gross =
| preceded by =
| followed by =
}}
Yesterday is a 1981 film directed by Lawrence Kent, starring Vincent Van Patten and Claire Pimparé. It is a love story of a young soldier maimed in Vietnam, who allows his former girlfriend to believe hes dead to spare her anguish.

==Cast==
* Vincent Van Patten - Matt Kramer
* Claire Pimparé - Gabrielle Daneault
* Nicholas Campbell - Tony
* Jack Wetherall - Moose
* Jacques Godin - Mr. Daneault
* Marthe Mercure - Mrs. Daneault
* Gerard Parkes - Professor Saunders
* Daniel Gadouas - Claude Daneault
* Cloris Leachman - Mrs. Kramer
* Eddie Albert - Bart Kramer

==References==
 

==External links==
*  
*  

 
 
 


 