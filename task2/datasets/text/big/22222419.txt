Mother Was a Rooster
{{multiple issues|
 
 
}}

{{Infobox Hollywood cartoon|
|cartoon_name=Mother was a Rooster
|series=Merrie Melodies (Foghorn Leghorn)
|director=Robert McKimson
|animator=Ted Bonnicksen Warren Batchelder Keith Darling George Grandpre
|voice_actor=Mel Blanc
|musician=Milt Franklyn
|producer=
|distributor=Warner Bros.
|release_date=October 20, 1962
|color_process=Technicolor
|runtime=7 mins
|movie_language=English
}} Bill Lava would take over as composer for Looney Tunes cartoons starting with Good Noose until the cartoon departments closure in 1969.

==Plot== he laid the egg as a prank. Foghorn awakens and falls for that. When the egg doesnt immediately hatch, Barnyard Dawg decides to speed up the process by bonking Foghorn on the head with a mallet. The egg hatches an ostrich chick, to which Foghorn immediately warms up to as his own son. Foghorn proudly shows off his "son" to Barnyard Dawg as a gesture of goodwill, but Barnyard Dawg insults the ostrich. The ostrich buries his head in the ground in shame.

After an attempt to get back at Barnyard Dawg fails, the plot shifts to Foghorns attempts to bond with his son, showing him how to play various sporting activities such as baseball and football. Despite these efforts to build the birds self-esteem and forget Barnyard Dawgs maliciousness, the dog continually and unmercifully mocks the ostrich. The ostrich buries his head with each insult, agitating Foghorn even more. Finally Foghorn has enough of the bullying and decides to defend his sons honor in a boxing match.

The bout takes place in a makeshift ring, contained beneath the farms wooden water tower. When Barnyard Dawg decides to cheat, Foghorn decides to forget the rules and — using a loose floor plank as a catapult — hurls his foe into the bottom of the water tank. Barnyard Dawg returns the favor, and the process repeats several times until the tank becomes dislodged and crashes on top of the ring, leaving both Foghorn and Barnyard Dawg with their heads buried in the ground. The ostrich who had been watching the match remarks: "Theyve left me all alone. Where did everybody go?"

==Succession==
 
{{succession box
|before=The Slick Chick Foghorn Leghorn cartoons
|years=1962
|after=Banty Raids}}
 

==References==
* Friedwald, Will and Jerry Beck. "The Warner Brothers Cartoons." Scarecrow Press Inc., Metuchen, N.J., 1981. ISBN 0-8108-1396-3.

==External links==
* 

 
 
 