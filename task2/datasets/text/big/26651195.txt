Ups and Downs (1937 film)
{{Infobox film
| name = Ups and Downs
| image =
| image size =
| caption = Roy Mack Vitaphone Corporation
| writer = Jack Henley Cyrus Wood
| starring = Hal Le Roy June Allyson
| music = Sammy Cahn Saul Chaplin Cliff Hess
| cinematography = Ray Foster
| editing = Bert Frank
| distributor = Warner Bros.
| released = 1937
| runtime = 21 minutes
| country = United States
| language = English
| budget =
}}
 Roy Mack.  

The film was made in New York City, and was Bronx native June Allysons first film for a major studio. 

==Synopsis==
An elevator operator Harry Smith (Hal Le Roy), who works in a luxury hotel, courts the hotel presidents daughter June Dailey (June Allyson). She is engaged to another, but when her fiance leaves on a business trip, Harry asks her to join him for dinner.
 stock ticker machine.

==Cast==
* Hal Le Roy as Harry Smith
* June Allyson as June Daily
* Phil Silvers as Charlie
* Fred Hillebrand
* Alexander Campbell
* Reed Brown, Jr.
* Toni Lane as herself (singer)
* The Deauville Boys as themselves (singers)

==Home media==
Ups and Downs appears as a special feature on the 2005 DVD of the film Stage Door. 

==References==
 

==External links==
* 

 
 
 
 
 

 