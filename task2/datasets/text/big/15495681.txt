Sangram (1993 film)
 
{{Infobox film
| name           = Sangram 
| image          = Sangram_(1993_film).jpg
| caption        =
| director       = Lawrence DSouza
| producer       = Lawrence DSouza
| writer         = Jalees Sherwani
| starring       = Ajay Devgn Ayesha Jhulka Karishma Kapoor Amrish Puri
| music          = Nadeem-Shravan
| cinematography = Lawrence DSouza
| editing        = 
| distributor    = 
| released       = 18 June 1993
| runtime        = 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}
 Hindi movie directed by Lawrence DSouza and starring Ajay Devgn, Karishma Kapoor, Ayesha Jhulka, Amrish Puri. Other cast members include Danny Denzongpa, Reema Lagoo, Asrani, Laxmikant Berde, Anjana Mumtaz, Tej Sapru, Vikas Anand, Satyendra Kapoor, Avtar Gill and Dinesh Hingoo.

==Cast==
* Ajay Devgn...Raja S. Singh Kanwar 
* Ayesha Jhulka...Palavi K. Singh 
* Karishma Kapoor...Madhu S. Singh 
* Amrish Puri...Thakur Surajbhan Singh Kanwar 
* Danny Denzongpa...Thakur Shamsher Singh Rana 
* Reema Lagoo...Mrs. Shamsher Rana 
* Asrani...Hindi Professor Bishveshwar Trivedi 
* Laxmikant Berde...Teji

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Udte Badal Se Poocha"
| Alka Yagnik
|-
| 2
| "Bheegi Huyee Hain Raat"
| Kumar Sanu, Kavita Krishnamurthy
|-
| 3
| "Dil Mein Mohabbat Hain"
| Kumar Sanu, Alka Yagnik
|- 
| 4
| "I Am Sorry"
| Mukul Agarwal, Alka Yagnik
|- 
| 5
| "Jeetega Wohi Jisme Hai Dam"
| Kumar Sanu, Sunanda
|- 
| 6
| "Beshak Tum Meri Mohabbat Ho"
| Kumar Sanu, Alka Yagnik, Sunanda
|- 
| 7
| "Sajana Ban Ke Phiru"
| Alka Yagnik, Sunanda
|}

== External links ==
*  

 
 
 
 


 