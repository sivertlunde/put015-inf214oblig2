The Old Man and the Sea (1958 film)
{{Infobox film
| name           = The Old Man and the Sea
| image          = The Old Man and the Sea (1958 film).jpg
| image_size     =
| alt            =
| caption        = Henry King Fred Zinnemann (Uncredited)
| producer       = Leland Hayward
| based on       =  
| writer         = Peter Viertel
| narrator       = Spencer Tracy   
| starring       = Spencer Tracy
| music          = Dimitri Tiomkin
| cinematography =  , Tom Tutwiler Underwater photography: Lamar Boren 
| editing        = Arthur P. Schmidt, Folmar Blangsted
| distributor    = Warner Bros.
| released       =  
| runtime        = 86 minutes   
| country        = United States
| language       = English
| budget         = $5 million   
| gross          = 
}}
 1958 film nominated for novella of the same name by Ernest Hemingway, and the film was directed by John Sturges. Sturges called it "technically the sloppiest picture I have ever made." 
 best color cinematography.

==Plot summary==
 
Spencer Tracy is the Old Man, a Cuban fisherman who tries to haul in a huge fish that he catches far from shore. 
He has gone 84 days without a catch - his only friend, a young boy (Felipe Pazos), is barred by his father from accompanying him to sea. On the 85th day the old man hooks a huge marlin. For three days and nights he battles the fish as a trial of mental and physical courage—and the ultimate test of his worth as a man.

==Cast==
In addition to Tracy, the cast included the following:
* Felipe Pazos Jr., the boy
* Harry Bellaver, Martin
* Don Diamond, café proprietor
* Don Blackman, arm wrestler
* Joey Ray, gambler Mary Hemingway, tourist
* Richard Alameda, gambler
* Tony Rosa, gambler
* Carlos Rivero, gambler
* Robert Alderette, gambler
* Mauritz Hugo, gambler
* Ernest Hemingway, tourist in café  

==Production== bluescreen compositing technology invented by Arthur Widmer that combined actors on a soundstage with a pre-filmed background." 
 Cabo Blanco Fishing Club in Peru. Mr. Glassell acted as special advisor for these sequences."  

Felipe Pazos Jr., who played the role of the boy in the film, is the son of the Cuban economist and revolutionary, Felipe Pazos.

==Music==
Veteran film composer  .

==Reception==
Bosley Crowther of The New York Times wrote: 
 Credit Leland Hayward for trying something off the beaten track in making a motion-picture version of Ernest Hemingways The Old Man and the Sea, and credit Spencer Tracy for a brave performance in its one big role. Also credit Dimitri Tiomkin for providing a musical score that virtually puts Mr. Tracy in the position of a soloist with a symphony. And that just about completes a run-down of the praiseworthy aspects of this film. 
Among the films short-comings, Crowther notes, is that "an essential feeling of the sweep and surge of the open sea is not achieved in precise and placid pictures that obviously were shot in a studio tank. There are, to be sure, some lovely long shots of Cuban villages and the colorful coast...But the main drama, that of the ordeal, is played in a studio tank, and even some fine shots of a marlin breaking the surface and shaking in violent battle are deflated by obvious showing on the process screen." 

Time (magazine)|Time noted that "the script follows the book in almost every detail" and called the novel a fable "no more suitable for the screen than The Love Song of J. Alfred Prufrock"; Tracy was
"never permitted to catch a marlin" while on location, so the "camera could never catch him at it" and the result is "Sturges must cross-cut so interminably&mdash;fish, Tracy, fish, Tracy&mdash;that Old Man loses the lifelikeness, the excitement, and above all the generosity of rhythm that the theme requires. 

Hemingway was pleased with the film. According to Leland Hayward, the films producer, Hemingway said it had "a wonderful emotional quality and   is very grateful and pleased with the transference of his material to the screen. He thought Tracy was great (in light of his quarrels with him this is quite a compliment) ... the photography was excellent ... the handling of the fishing and mechanical fish very good. Had some minor dislikes ... but all in all he was terribly high on the picture and pleased with it." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
   
 
 
 
 
 
 
 