Misunderstood (1984 film)
{{Infobox Film
| name           = Misunderstood
| image          = Misunderstood poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Jerry Schatzberg producer = Tarak Ben Amar
| writer         = Barra Grant
| narrator       = 
| starring = {{Plainlist|
* Gene Hackman
* Henry Thomas
* Rip Torn
* Huckleberry Fox
* Maureen Kerwin
* Susan Anspach
}}
| music          = Michael Hoppé
| cinematography = Pasqualino De Santis
| editing        = Marc Laub
| distributor    = MGM/UA Entertainment Co.
| released       = March 30, 1984
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Misunderstood is a 1984 film directed by Jerry Schatzberg, based on the 19th-century novel by Florence Montgomery. This film stars Henry Thomas as a young boy who struggles with family, friends, and relationships after his mothers death.

It is a remake of the 1966 Italian version, Misunderstood (1966 film)|Incompreso, which starred Anthony Quayle.

==Cast==
* Gene Hackman as Rawley
* Henry Thomas as Andrew
* Rip Torn as Will
* Huckleberry Fox as Miles
* Susan Anspach as Lilly
==Production==
Shooting took place over four months in 1982. The director shot two endings, and wanted to use the more tragic one. The producer, Tarak Ben Ammar, then assembled his own more upbeat version of the film, adding extra flashbacks. This was then changed by MGM/UA. At the Movies
Maslin, Janet. New York Times (1923-Current file)   23 Mar 1984: C6.  
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 


 