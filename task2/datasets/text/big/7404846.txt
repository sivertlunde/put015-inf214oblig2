Impressionen unter Wasser
{{Infobox film
| name           = Impressionen unter Wasser
| image          = Impressionenposter.jpg
| image_size     =
| caption        =
| director       = Leni Riefenstahl
| producer       =
| writer         =
| narrator       =
| starring       =
| music          = Giorgio Moroder Daniel Walker
| cinematography = Horst Kettner Leni Riefenstahl
| editing        = Leni Riefenstahl Odeon Pictures Bavaria Film International
| released       =  
| runtime        = 45 minutes
| country        = Germany German
| budget         =
}}

Impressionen unter Wasser (Underwater Impressions aka Impressions of the deep) is a documentary film released in 2002. It was directed by Leni Riefenstahl.

After the premiere of her film Tiefland (film)|Tiefland in 1954, for decades it was generally thought this would be Riefenstahls last film. However, a few days before her 100th birthday, saw the release of Impressionen unter Wasser (45 minutes, with an introduction by Riefenstahl) premiered in Berlin 48 years after Tiefland. {{cite web|url=http://news.bbc.co.uk/1/hi/entertainment/1745806.stm| title=Hitlers filmmaker to release new film date =7 January 2002}} 

==Production history==
Impressionen unter Wasser is the final result of 30 years of underwater cinematography. In 1983 she described her intentions for the eventual film; "Underwater films are either scientific, like Jacques Cousteaus, or sensational, like the Hollywood shark films. But there are none like this one we plan." 

Segments of her extensive marine footage were first shown to the public in the 1993 documentary, The Wonderful, Horrible Life of Leni Riefenstahl which followed her recent expedition.

===Release===
Impressionen unter Wasser had its premiere at Delphi am Zoo, Berlin on 14 August 2002, a week before the celebrations for Lenis centenary birthday. It was broadcast the following day on the French/German Arte channel, together with an interview with Riefenstahl and an airing of the
original 1932 version of Riefenstahls Das Blaue Licht. It was also featured at the 2003 Maui Independent Film Festival and the 2004 Oporto International Film Festival (Fantasporto).    The film was released on DVD in April 2003.

==Synopsis==
The marine-based documentary was mainly shot around Papua New Guinea, and is a collection of footage directed by Riefenstahl between the 1970s and 2000.  Other film locations included the Maldive Islands the Seychelles, Kenya, Tanzania, Indonesia, the Red Sea, the Cocos Islands (Pacific) and the Caribbean Sea (Cuba).  Riefenstahl dismissed claims that the film was a comeback; "Impressionen Unterwasser certainly isnt a comeback. I was always active and continue to be so. My film shows the beauty of the underwater world. I hope it will touch the viewers conscience as it illustrates just what the world will lose when nothing is done to stop the destruction of our oceans. I once said that I am fascinated by the beautiful and the living. I seek harmony and, under water, I have found it." 

Riefenstahl also contributed an introduction, discussing the work and her marine journeys.   

==Reception== Time Out magazine held a neutral view point, puzzled by the lack of commentary, as is characteristic of Riefenstahl films but was displeased by the Moroder-Walker score.  Other critics compared the film to her famous 1938 film Olympia (1938 film)|Olympia in that it reflects her ultimate pursuit of beauty.  Contributing to the book 501 Directors, Dr. Ernest Mathijs, writer and film scholar cited the documentary as "  final example of her creative genius."   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 