Runningshaadi.com
 
{{Infobox film name            = Runningshaadi.com image           = Runningshaadi.png director        = Amit Roy  writers         = Navjot Gulati & Amit Roy producer        = Shoojit Sircar Crouching Tiger Motion Pictures studio            = Rising Sun Films music           = Abhishek-Akshay & Zeb release        = about to release country         = India language        = Hindi
}} Bihari and a Sardarji geek stand up and create a platform providing “High Take Social Service“ to Indian couples who want to spend their lives with their soul mates.

The Shoojit Sircar production stars Amit Sadh and Taapsee Pannu and will be released to an audience of over 1,100 screens with music from an assembled list of performers.

==Plot==
A 23-year-old Ram Bharose and his teenaged whiz-kid Sardar befriend sarabjeet Sidhaana A.K.A. ‘Cyberjeet’ and strike upon an ingenious brainwave of starting a website that helps young couples in Amritsar elope and get married. The website is called ‘runningshaadi.com’ and it offers a comprehensive array of services to facilitate runaway weddings. The website becomes an instant hit with young, love-struck couples facing pressure from their parents and the site becomes a rage in small town Amritsar. While they are successful in getting many couples together, the underlying story of Runningshaadi.com lends itself to an unexpected twist. What happens with the site, along with the absurdities of making couples elope, is the catch of the film that lends an humorous joy ride.

==Cast==
* Amit Sadh as Ram Bharose:
A matric-fail migrant Bharose used to work at Singh n Singh bridals as the owners right-hand man. Being close to the family, he also becomes a confidant to the owner’s daughter Nimmi. Although he is illiterate he is very smart and business minded. His background makes his character comic. He is a dependable yet implusive lad, who despite facing rejections from his love has the desire and conviction to be with her, though he remains confused by her behaviour.

* Taapsee Pannu as Nimmi:
A loud-spoken, typical Punjabi girl, who lives on her own terms. She wants to enjoy her life without boundaries. She continuously has her share of squabbles with Cyberjeet. She finds herself asking Bharose for favors as she finds him to be a very dependable guy. Her character is that of Amritsars Patakha Queen.

* Arsh Bajwa as sarabjeet Sidhana (Cyberjeet):
A teenage whiz kid, who idolizes Mark Zuckerberg, Bill Gates, and Steve Jobs. His life revolves around his computer and his idols. He is a street-smart computer geek who, with the help of his friend Bharose, creates a one of its kind portal. Being a good friend, he is also very protective of Bharose and stands with him through thick and thin.

==Music==
The music of Shoojit Sircar’s films have always received high critical and commercial success. His film Vicky Donor was not just commercially successful but its audio too was hugely popular. The music of his film Madras Cafe is very unconventional as well.

The music from Running Shaadi.com has been created by an assembled team taking the feel of mixed cult. The music is by Abhishek-Akshay & Zeb and features the voices of singers like Bappi Da, Papon, & Labh Janjua.

==See also==
* Shaadi.com

 