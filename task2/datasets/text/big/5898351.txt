La Cérémonie
{{Infobox film
| name           = La Cérémonie
| image          = Laceremonieposter.jpg
| director       = Claude Chabrol
| producer       = Marin Karmitz
| writers        = Claude Chabrol Caroline Eliacheff
| starring       = Isabelle Huppert Sandrine Bonnaire Jacqueline Bisset Jean-Pierre Cassel Virginie Ledoyen Valentin Merlet Serge Rousseau
| music          = Matthieu Chabrol
| cinematography = Bernard Zitzermann
| editing        = Louisette Hautecoeur
| released       = August 30, 1995 (France)
| runtime        = 112 minutes
| country        = France / Germany
| language       = French
| budget         = €5,800,000
| gross          = $10,882,920   
}}
 1995 film by Claude Chabrol, adapted from the novel A Judgement in Stone by Ruth Rendell. The film echoes the case of Christine and Lea Papin, two French maids who brutally murdered their employers wife and daughter in 1933, as well as the 1947 play they inspired, The Maids by Jean Genet. 

== Plot ==

La Cérémonie tells the story of a young woman, Sophie Bonhomme ( ) and Georges, the parents, who have no children together, but one each from previous marriages. Gilles is Catherines and her ex-husbands son. He is a lonely teenager who loves reading and has a passion for arts in general. Melinda is Georges and his late wifes daughter. She studies at a university and only spends the weekends at home, where she invites her boyfriend Jérémie. The household chores are excessive for Catherine – who owns her own art gallery – so she requires a maids help and hires Sophie. Throughout the film Sophie avoids using the dishwasher, refuses to take driving lessons, buys fake eyeglasses, and has trouble giving a cashier the correct change. The viewer finds out later that Sophie is illiterate and has a history of violence since she is believed to have killed her handicapped father, or at least not to have rescued him from the fire she might have set in his house.

Once in the small village, Sophie meets Jeanne (Isabelle Huppert), the postmistress, who occasionally works in a charity and reads a lot. However, Jeanne proves to be a bad influence on the maid since she is jealous and aggressive towards a lot of people, including the Lelièvres, whose mail she vandalises. She also has a violent history. The two friends meet regularly either for a charity project (that they end up ruining) or for a film; television being Sophies main pastime. Sophie is treated rather nicely at the Lelièvres, who mean well towards her, but their patronizing attitude and the affection they have for each other create a feeling of jealousy and frustration both in Sophie and in Jeanne. This frustration reaches its climax when Georges fires Sophie for attempting to blackmail Melinda, who found out about her illiteracy. It is what triggers the climax of the film, which sees Sophie and Jeanne seize Georges’ shotguns and murder the family who were watching an opera on television.

Jeanne leaves the crime scene and is killed in a car accident by the priest who had fired her from the charity she worked for. Sophie, for her part, walks away from the house after having wiped their fingerprints off the guns, making her way through the police squads at the accident. The end credits begin with the music of the opera that is being played back by a policeman on Melindas tape-recorder, which Jeanne stole and put in her car. At the end of the credits, the gunshots can be heard on the tape and then the voices of Jeanne and Sophie, constituting evidence against them. Chabrol presents an ambiguous view of culture and class conflict in this film, which he jokingly called "the last Marxist film." 

==Reviews==
La Cérémonie received an overall rating of 92% from Rotten Tomatoes (22 fresh and 2 rotten reviews). 

On 17 April 2012, Roger Ebert added La Cérémonie to his list of "Great Movies". 

==Awards and nominations==
*César Awards (France) Best Actress &ndash; Leading Role (Isabelle Huppert) Best Actor &ndash; Supporting Role (Jean-Pierre Cassel)
**Nominated: Best Actress &ndash; Leading Role (Sandrine Bonnaire) Best Actress &ndash; Supporting Role (Jacqueline Bisset) Best Director (Claude Chabrol) Best Film Best Writing (Claude Chabrol and Caroline Eliacheff)
 Los Angeles Film Critics (USA) Best Foreign Language Film (Claude Chabrol)

*National Society of Film Critics (USA) Best Foreign Language Film

*Satellite Awards (USA) Best Motion Picture &ndash; Foreign Language

*Toronto International Film Festival (Canada)
**Won: Metro Media Award (Claude Chabrol)

*Venice Film Festival (Italy)
**Won: Volpi Cup &ndash; Best Actress (Sandrine Bonnaire and Isabelle Huppert)
**Nominated: Golden Lion (Claude Chabrol)

==See also==
*Isabelle Huppert filmography

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 