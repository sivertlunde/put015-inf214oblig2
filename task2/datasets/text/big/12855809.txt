The Honor of the Force
 
{{Infobox film
| name           = The Honor of the Force 
| image          = 
| image_size     = 
| caption        =  Frank Griffin
| producer       = Arthur Hotaling
| writer         = Frank Griffin	
| narrator       = 
| starring       = C.W. Ritchie
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  English intertitles
| budget         = 
}}
 silent comedy film featuring Oliver Hardy. The film was released on 22 September 1913.

==Plot==
A crooked policeman joins Big Tuckers gang of grafters and helps to break into his own captains house to steal an incriminating paper. The captain interrupts them at work and is stunned in the fight, but a ruse planned meticulously by him results in the thieves being captured.

==Cast==
* C.W. Ritchie - Officer Bradley
* Royal Byron - Nora Malone (as Roy Byron)
* Raymond McKee - Gyp, the Dip
* Oliver Hardy - Fattie (as Babe Hardy)

==See also==
* List of American films of 1914
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 

 