Woodcutters of the Deep South
 
{{Infobox film
| name           = Woodcutters of the Deep South
| image          = 
| image_size     = 
| caption        = 
| director       = Lionel Rogosin
| producer       = Lionel Rogosin
| writer         =
| narrator       = Lionel Rogosin (uncredited)
| starring       = 
| music          = 
| cinematography = Lionel Rogosin, Louis Brigante
| editing        = Louis Brigante
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
}}

Woodcutters of the Deep South is the sixth and final feature-length film produced and directed by American independent filmmaker Lionel Rogosin. The film looks at workers who organize to resist exploitation by pulpwood corporations.  

Rogosin made one more short film, Arab Israeli Dialogue, released in 1974, but for the next 25 years he struggled to find support for a number of unrealized projects.

==References==
 

== External links ==
*  


 
 
 
 
 
 


 