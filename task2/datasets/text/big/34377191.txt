Trance (2013 film)
 
 
{{Infobox film
| name           = Trance
| image          = Trance2013Poster.jpg
| image_size     = 250px
| alt            =
| caption        = UK release poster by Empire Design
| director       = Danny Boyle
| producer       = Christian Colson
| screenplay     = {{Plain list |
*Joe Ahearne John Hodge
}}
| story          = Joe Ahearne
| starring       = {{Plain list |
*James McAvoy
*Vincent Cassel
*Rosario Dawson
}} Rick Smith
| cinematography = Anthony Dod Mantle Jon Harris
| production companies= {{Plain list |
*Cloud Eight
*Decibel Films
*Pathé
*TSG Entertainment
*Ingenious Media Indian Paintbrush
}}
| distributor    = Fox Searchlight Pictures
| released       =   
| runtime        = 101 minutes  
| country        = {{Plain list |
*United Kingdom
*United States
*France 
}}
| language       = English
| budget         = $20 million    
| gross          = $24.3 million 
}} John Hodge from a story by Ahearne. The film stars James McAvoy, Vincent Cassel, and Rosario Dawson. The world premiere of the film was held in London on 19 March 2013. 

==Plot== Witches in the Air, from his own auction house. When the gang attacks during an auction, Simon follows the house emergency protocol by packaging the painting. The gang leader Franck at gunpoint takes the package from him. Simon attacks Franck, who strikes him a blow to the head that leaves him with amnesia. When Franck gets home, he discovers that the package contains only an empty frame. After ransacking Simons apartment and trashing his car, the gang kidnaps and unsuccessfully tortures him. But he has no memory of where he has hidden the painting. Franck decides to hire a hypnotherapist to try to help him remember. 
 memory fugue, believed the woman was Elizabeth, recalling that she had made him forget her. The gang tries violently to force Simon to remember where he put the painting, and that ends the hypnotic episode.
 Witches in the Air (1798), the painting stolen in the film.  In reality it is part of the collection of the Museo del Prado.]] To help Simon recover from the violence, Elizabeth stays overnight in his apartment. In the morning, Simon dreams of Elizabeths having used a brain-scan behaviour-conditioning technique to erase an obsession that he had for her. Elizabeth tells Franck about that.

  Nude Maja (ca. 1795), a painting Simon discusses in the film.]]For the next step to recover the painting, Elizabeth tells Franck that she will sexually seduce Simon. Simons feelings for Elizabeth recur, gently this time. At the same time, Franck and Elizabeth have unanticipated sex, and she steals his pistol from his bedside drawer. Nate, a gang member, sees them together and warns Simon, who confronts Elizabeth with it. She responds by touching his erotic mindspot, related to Goyas Nude Maja.

Then Simon has a dream where Franck and his associates plan to kill him, but he kills them instead. In the dream, he remembers that the painting is in a red car in a certain car park, calls Elizabeth and tells her.  Elizabeth takes the car keys and goes to get the painting, leaving Francks pistol for Simon. Simon awakens to find Franck in his apartment. Nate and his associates intercept Elizabeth and bring her there. As she kisses Simon, Elizabeth secretly passes three bullets into his mouth. Franck takes Simon to get the painting, but Simon stops him with a fire extinguisher, and then with the pistol, now loaded with the three bullets. In the apartment, the gang prepares to rape Elizabeth. Taking Franck back into the apartment, Simon shoots the other three gang members. He takes the car keys from Elizabeth, and loads the pistol with the remaining three bullets. He takes Elizabeth to get the painting. She tells him to let Franck come with them. He leads them to a parking garage where the painting is.

They collect the red car and drive it to a safe warehouse. During the trip, Elizabeth reveals that Simon was previously a client of hers. He had a gambling addiction he wanted to fix. They started an affair. She found his erotic mindspot. He became obsessed with her, and eventually abusive. She re-directed the hypnosis to make him forget her. This led him back into his gambling addiction, which caused him to go in debt and to try to pay it off by stealing a painting, with the help of Franck. Simon recalls that, after the heist, when he was hit by the red car, and mistook the female driver for Elizabeth, he strangled her.

At the warehouse, in the cars trunk, Elizabeth finds the painting and the body of the female driver. Simon, having at last remembered his past and wanting to forget, douses the car in fuel with Franck zip-tied to the steering wheel, sets it on fire and tells Elizabeth to run away with the painting. She runs away but promptly returns driving a truck which she drives into Simon, pinning him against the other car, and ultimately sending Simon, and the car Franck is trapped in, into the river. 

Franck manages to escape, while it is implied that Simon drowns. The scene cuts to Franck swimming in his apartment while thinking of the event. He gets out of the pool and receives a package. He opens the package and finds an iPad that plays a video of Elizabeth talking about the painting, which is now hanging in her apartment. She reveals that when she hypnotized Simon to make him forget her, she also hypnotized him to go back into his gambling addiction. When Simon would try to steal a painting to pay off his debt, he would instead give the painting over to Elizabeth. This explains why Simon took the painting away from Franck at the beginning and the text message he received before being hit by the car, which is revealed to be from Elizabeth telling Simon to deliver the painting to her. Elizabeth gives Franck the option to forget the ordeal, and a button for an app called "Trance" appears as the video ends. Franck is shown debating whether to press the button, and the screen cuts to black.

==Cast== Storm on the Sea of Galilee (1633), a painting Simon uses to introduce and frame the film, drawing attention to the central self-portrait of Rembrandt, who calmly stares out at the viewer while chaos rages around him.]]  
 
* James McAvoy as Simon
* Vincent Cassel as Franck
* Rosario Dawson as Elizabeth
* Danny Sapani as Nate
* Matt Cross as Dominic
* Wahab Sheikh as Riz
* Mark Poltimore (7th Baron Poltimore) {{cite web url        = http://artdaily.com/news/62787/Sotheby-s-to-sell-original-receipt-for-Goya-painting-in-Danny-Boyle-s-art-heist-movie-Trance#.U6A_f1Oa98F title      = Sothebys to sell original receipt for Goya painting in Danny Boyles art heist movie Trance author     =    date       = 24 May 2013 publisher  = Royalville Communications, Inc accessdate = 17 June 2014
                    }}  as Francis Lemaitre 
* Tuppence Middleton as Young Woman in Red Car
* Simon Kunz as Surgeon
* Michael Shaeffer as Security Guard #1  
* Tony Jayawardena as Security Guard #2  
* Vincent Montuel as Handsome Waiter  
* Jai Rajani as Car Park Attendant  
* Spencer Wilding as 60s Robber  
* Gursharan Chaggar as Postman
* Edward Rising as 60s Auctioneer

==Production==
===Development=== John Hodge – marking the fifth motion picture collaboration between Hodge and Boyle.   

===Casting===
In May 2011, Michael Fassbender was cast as Franck but dropped out due to scheduling conflicts.   Colin Firth was considered for the part before Cassel was cast.       Scarlett Johansson, Melanie Thierry, and Zoe Saldana were considered for the role that went to Dawson.    

McAvoy, who accepted the role in 2011, said that he almost turned down the part, while reading the script, because Simon seemed to be a victim, which didnt interest him. He told NPRs reporter Laura Sullivan, "And then I got about 15 or 20 pages in, and I started to sense that something else was coming in the character. And then something else did come. And then about every 10 pages, something else came. Until at the end, I was hunching at the bit, as we say in Scotland... It just means I was desperate...I was hungry to play this part." 

===Filming=== opening ceremony of the 2012 Summer Olympics in London. Post-production was then picked up again in August 2012. 

Boyle said that this is "the first time I put a woman at the heart of a movie."  He also said that he originally intended to set the movie in New York City,  but it was filmed in London and in Kent instead, as Boyles Olympic ceremony duties meant he had to stay in the UK. 

===Music=== Underworld would The Beach Opening Ceremony, I hardly knew what day of the week it was. I took a month off work, off music, off everything. Exactly one month and three days after we said goodbye in the stadium, I received a text from Danny that said, Do you ever want to hear from me again workwise and would that go as far as having a chat about Trance... Questions, questions. Two Minutes later I was on board."    The soundtrack album for Trance was released in the United Kingdom on 25 March and in the United States on 2 April 2013.  

When asked by an interviewer about the secret of their 17-year-old creative partnership, Boyle joked, "Hes cheap." Then, answering seriously, he said that they both like electronic music and that he doesnt prescribe a sound for a scene, but lets Smith follow his own instincts. 

==Release== teaser trailer and an extended version of an alternate ending at South by Southwest on 9 March 2013.     The entire film could not be screened at the festival, as is usually done, because the producing studio Pathé owned the rights to the world premiere, which was held 10 days later.   The film was released on 27 March 2013 in the United Kingdom,  with a United States release date on 5 April 2013. 

==Reception==
The film received mostly positive reviews from critics.   writer Michael OSullivan describes Boyle as "playing fast and loose with reality." 

On Metacritic, which assigns a weighted mean rating out of 100 based on reviews from film critics, the film has a rating score of 61% based on 37 reviews. 

Empire (film magazine)|Empire magazine in its review gave the film 4 out of 5 and called the film  "a dazzling, absorbing entertainment which shows off Danny Boyles mastery of complex storytelling and black, black humour."  Empire also ranked it 27 in its top 50 films of 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 