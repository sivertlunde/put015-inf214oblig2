Leaving Normal (film)
{{Infobox film
| name           = Leaving Normal
| image          = leavingnormal.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Edward Zwick
| producer       = Lindsey Doran
| writer         = Ed Solomon
| starring       = {{Plainlist|
* Christine Lahti
* Meg Tilly
}}
| music          = W. G. Snuffy Walden
| cinematography = Ralf D. Bode
| editing        = Victor DuBois
| studio         = {{Plainlist|
* Mirage Entertainment 
* Universal Pictures
}}
| distributor    = Universal Pictures
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,514,114
}} road film directed by Edward Zwick and starring Christine Lahti and Meg Tilly. Written by Ed Solomon, the film is about the cross country adventure of two women and the hardships and characters they encounter.    

==Plot==
Darly Peters (Christine Lahti) is a brassy waitress and former stripper who used to use the stage name Pillow Talk. Darly is on her way to Alaska to claim a home being built for her and return to the family she abandoned eighteen years earlier. She meets Marianne Johnson (Meg Tilly), a quiet waif who just walked out on her abusive husband. Darly allows Marianne to tag along as they journey across country to Alaska. 

Along the way, they meet a collection of colorful characters, including a strange-talking waitress named 66 (Patrika Darbo), and Walt (James Gammon), a road guy who recognizes Darly as the former Pillow Talk and wants to pay her big money for sex. 

The women finally make it to Alaska, where Darly finds that the house she was expecting to find has never been built. The two set up in a house trailer and, with the Alaskan wilderness as a backdrop, they begin to reevaluate their lives.

==Cast==
* Meg Tilly as Marianne Johnson
* Christine Lahti as Darly Peters
* Lenny Von Dohlen as Harry Rainey
* Maury Chaykin as Leon "Crazy-As" Pendleton
* Patrika Darbo as 66
* Eve Gordon as Emily Singer
* James Eckhouse as Rich Singer
* Brett Cullen as Kurt
* James Gammon as Walt Lachlan Murdoch as Marshall
* Robyn Simons as Sarah
* Ken Angel as Nuqaq
* Darrell Dennis as Clyde
* Barbara Russell as Izuzu Mother
* Ahnee Boyce as Izuzu Judy Marc Levy as Dave Peter Anderson as Spicy
* Gordon Tootoosis as Hank Amaruk
* Rutanya Alda as Palmer Hospital Nurse
* Ray Godshall as Mort Tom Heaton as Alec Deirdre OConnell as Ellen 
* Gordon Tipple as Danny
* John Bear Curtis as Michael
* Paul Jarrett as Charlie
* Ed Solomon as Jerk in Bar Andrew Johnston as Nearest Guy
* Benjamin Ratner as Next Nearest Guy  Timothy Webber as Spicys Friend
* Sam Bob as Delivery Guy
* Marlane OBrien as Clara
* Brenda McDonald as Motel Clerk   

==Production==
;Filming locations
* Britannia Beach, British Columbia, Canada 
* Vancouver, British Columbia, Canada 
* Yoho National Park, British Columbia, Canada   

==Reception==
;Critical response
The film received mixed-to-negative reviews. On the aggregate review site Rotten Tomatoes, the film received a 25% favorable rating from critics.   

==References==
 

==External links==
* 
*   at Film.com

 

 
 
 
 
 