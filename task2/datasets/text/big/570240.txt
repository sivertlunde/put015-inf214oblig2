All Monsters Attack
 
{{Infobox film
| name           = All Monsters Attack
| image          = Godzillas Revenge 1969.jpg
| caption        = Original poster
| director       = Ishirō Honda
| producer       = Tomoyuki Tanaka
| writer         = Shinichi Sekizawa
| starring       = Tomonori Yazaki   Kenji Sahara   Hidemi Ito   Eisei Amamoto   Sachio Sakai   Kazuo Suzuki   Haruo Nakajima
| music          = Kunio Miyauchi
| cinematography = Sokei Tomioka
| editing        = Masahisa Himi
| studio      =  Toho
| distributor    = Toho  UPA (animation studio)|UPA/Maron Films (USA)
| released       =  
| runtime        = 69 minutes
| country        = Japan
| language       = Japanese
| Followed by    = Godzilla vs Hedorah (1971)
}}
All Monsters Attack, released in Japan as  , is a 1969 Japanese Kaiju film produced by Toho. Directed by Ishirō Honda, the film starred Tomonori Yazaki, Eisei Amamoto, and Kenji Sahara. The tenth film in the Godzilla series, this was also the first film specifically geared towards children. While credited with the special effects work, Eiji Tsuburaya was not directly involved with the production of this film. The "Special Effects Supervised by" credit was given out of respect, since he was still the head of the Visual Effects Department. The effects were handled by Ishirō Honda himself, with assistance from Teruyoshi Nakano.
 Night of the Big Heat" (aka Island of the Burning Damned).

==Plot== battle three Kamacuras and Ebirah, a giant sea monster. Ichiro is then chased by a rogue Kamacuras and falls into a deep cave, but luckily avoids being caught by Kamacuras. Shortly afterwards, Ichiro is rescued from the cave by Godzillas Son, Minilla. Ichiro quickly learns that Minilla has bully problems too, as he is bullied by a monstrous ogre known as Gabara.

Ichiro is then awoken by Shinpei who informs him that his mother must work late again. Ichiro goes out to play, but is then frightened by the bullies and finds and explores an abandoned factory. After finding some souvenirs (tubes, a headset, and a wallet with someones license), Ichiro leaves the factory after hearing some sirens close by. After Ichiro leaves, two Bank Robbers (played by Sachio Sakai and Kazuo Suzuki) who were hiding out in the factory learn that Ichiro has found one of their drivers licenses and follow him in order to kidnap him.
 some invading jets. Then in the middle of Godzillas fights, Gabara appears and Minilla is forced to battle him, and after a short and one-sided battle Minilla runs away in fear. Godzilla returns to train Minilla how to fight and use his own atomic ray. However, Ichiro is woken up this time by the Bank Robbers and is taken hostage as a means of protection from the authorities.

Out of fear and being watched by the thieves, Ichiro calls for Minillas help and falls asleep again where he witnesses Minilla being beaten up by Gabara again. Finally, Ichiro helps Minilla fight back at Gabara and eventually Minilla wins, catapulting the bully through the air by a seesaw-like log. Godzilla, who was in the area watching comes to congratulate his son for his victory, but is ambushed by a vengeful Gabara. Godzilla easily beats down Gabara and sends the bully into retreat, never to bother Minilla again. Now from his experiences in his dreams, Ichiro learns how to face his fears and fight back, gaining the courage to outwit the thieves just in time for the police to arrive and arrest them. The next day, Ichiro stands up to Sanko and his gang and wins, regaining his pride and confidence in the process. He also gains their friendship when he plays a prank on a billboard painter.

==English version==
 .]]
  gives instructions to Minilla (Marchan the Dwarf) and Ichiro (Tomonori Yazaki), during filming.]]

The film was dubbed in English and released in North America on December 8, 1971 by Maron Films as Godzillas Revenge on a double bill with Island of the Burning Damned. Maron Films later re-released the movie on a double bill with War of the Gargantuas.

There are some minor alterations between the Japanese version and the English dubbed version:

* All Japanese-speaking dialogue is dubbed to English, by using English-speaking voice actors.

* The Japanese version featured a vocal song over the opening credits (Kaiju no Māchi or March of the Monsters), sung by Risato Sasaki and the Tokyo Childrens Choir, and issued on the Japanese label, Crown Records. While the English dubbed version features a jazzy instrumental entitled "Crime Fiction", composed by Ervin Jereb.

* In the original Japanese version, Minilla was voiced by voice actresses, Midori Uchiyama and Michiko Hirai respectively. In the English dubbed version, Minilla is renamed "Minya", and he is given a cartoony male voice.

* In the Japanese version, the two thieves names were never mentioned in the film, but their names were shown on the credits. In the English dubbed version, the leader is given the name "Roy," but his partners name is never mentioned in the film.

==Cast==
 
{| class="wikitable"
|-
!Character
!  Japanese actor dubbing (1971)
|- Ichiro Miki Tomonori Yazaki
|
|- Kenkichi "Tack" Miki Kenji Sahara
|
|- Shinpei Inami Eisei Amamoto
|
|- Sachiko
|Hidemi Ito
|
|- Sanko Gabara (Mitsukimi-bully) Junichi Ito
|
|- Bank Robber Senbayashi Sachio Sakai
|
|- Bank Robber Okuda Kazuo Suzuki
|
|- Minilla
|Marchan the Dwarf (suit actor)   Midori Uchiyama   (voice)    Michiko Hirai   (voice) 
|
|- Godzilla
|colspan="2" Haruo Nakajima
|- Gabara
|colspan="2" Yasuhiko Kakuko   Yû Sekida  (voice) 
|-
|Sachikos Mother Yukiko Mori  (uncredited) 
|
|- Landlady of the Inn Yoshiko Miyata  (uncredited) 
|
|-
|}

===English dubbing staff===
*Dubbing director: 
*Dubbing studio: UPA (animation studio)|UPA/Maron Films 
*Media: Film|Cinema/TV/VHS/DVD

==Box office==
In Japan, the film sold 1,480,000 tickets. This was over a million tickets less than the previous Godzilla film, Destroy All Monsters (and it was the first Godzilla film to sell less than 2 million tickets).

==DVD releases==
 Classic Media

* Released: Original Japanese version with English dubbed version as part of the Toho Master collection; originally supposed to be released in September 2007 but was made an "exclusive" to the Godzilla Toho Master Collection Box Set in November 2007, this and Terror of Mechagodzilla, it was released separately on April 29, 2008. It was also released in a box set, called the Godzilla Collection, which also included Terror of Mechagodzilla.

* Region 1

==Stock Footage== Manda and Anguirus make an appearance; stock footage from Son of Godzilla when Godzilla fights the Kamacuras and Kumonga and when he teaches Minilla how to breathe fire; and stock footage from Godzilla vs. the Sea Monster when Godzilla fights Ebirah, the giant Condor and when he destroys the Red Bamboo jets.

== References ==
 

== External links ==
*  
*  
*  
*  


 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 