Goldstein (film)
{{Infobox film
| name           = Goldstein
| image          = Goldstein1964.jpg
| image_size     =
| caption        = DVD cover
| producer       = Zev Braun Philip Kaufman
| director       = Philip Kaufman Benjamin Manaster
| writer         = Philip Kaufman Benjamin Manaster
| narrator       =
| starring       =
| music          = Meyer Kupferman
| cinematography = Jean-Phillippe Carson
| editing        = Adolfas Mekas
| studio         = Montrose Film Productions Braun Entertainment Group
| distributor    = Altura Films International
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}} 1965 film co-directed by Philip Kaufman and Benjamin Manaster,  and produced by Kaufman and Zev Braun. The cast featured a number of actors from The Second City comedy troupe. The film won the Prix de la Nouvelle Critique at the Cannes Film Festival. 

==Cast==
*Lou Gilbert 
*Ellen Madison
*Tom Erhart 
*Ben Carruthers
*Charles Fischer
*Severn Darden  Anthony Holland 
*Nelson Algren	(as himself)
*Jack Burns
*Mike Turro 
*Viola Spolin
*Del Close

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 