Gun Glory
{{Infobox film
| name           = Gun Glory
| image          = 
| caption        =  Roy Rowland
| producer       = 
| writer         = 
| based on       = 
| starring       = Stewart Granger Chill Wills Rhonda Fleming
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 1957
| runtime        = 88 mins
| country        = United States
| language       = English
| budget         = $1,707,000  . 
| gross         = $2,550,000 
}}
Gun Glory is a 1957 Western feature film starring Stewart Granger.

==Plot==
Tom Early rides into a Wyoming town where he once lived with his wife and son. In the general store, owner Wainscott is annoyed when he believes clerk Jo is flirtatious with Early.

At his old ranch, Early finds his wifes grave and his 17-year-old son, Tom Jr., embittered by his fathers having abandoned them.

Jo takes a job as housekeeper at Earlys ranch. She resists the advances of Tom Jr., whose resentment of his father grows. When they attend church, Wainscott turns the preachers congregation against them, insinuating Jo is living there in sin.

Townspeople need help, though, when gunmen working for the villainous cattleman Grimsell ambush one of their own. A posse is formed, but by the time Early gets there, the preacher is dying and Tom Jr. is wounded.

Tom uses TNT to start a rockslide, stampeding Grimsells cattle and killing some of his men. In a showdown, Early fights with Gunn, one of Grimsells men, and just in the nick of time, Tom Jr. comes to his rescue. They return home to a relieved Jo.

==Cast==
*Stewart Granger as Tom Early
*Rhonda Fleming as Jo
*Chill Wills as Preacher Steve Rowland as Tom Early, Jr.
*James Gregory as Grimsell
*Jacques Aubuchon as Sam Wainscott
*Arch Johnson as Gunn
*William Fawcett as Martin
*Carl Pitti as Joel
*Lane Bradford as Ugly
*Rayford Barnes as Blondie
*Ed Mundy as Ancient

==Production==
The film was made towards the end of Grangers contract with MGM and he felt they assigned him to this low-budget film to punish him for not renewing with the studio. 

==Reception==
According to MGM records, the film earned $1,125,000 in the US and Canada and $1,425,000 overseas, making a loss of $265,000. 

In France, it recorded admissions of 889,516.   at Box Office Story 

==References==
 

==External links==
*  
*  
 
 
 
 
 