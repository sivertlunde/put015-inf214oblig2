Neal of the Navy
 
{{Infobox film
| name           = Neal of the Navy
| image          = Lillian Lorraine 1.jpg
| caption        = Moving Picture World, 1915 William Bertram W. M. Harvey
| producer       =
| writer         = Douglas Bronston William Hamilton Osborne
| starring       = William Courtleigh, Jr. Lillian Lorraine
| cinematography =
| editing        =
| distributor    = Balboa Amusement Producing Company
| released       =  
| runtime        = 14 episodes
| country        = United States Silent English intertitles
| budget         =
}}
 adventure film William Bertram and W. M. Harvey. The film is considered to be lost film|lost.    Neal of the Navy was the first use of a mans name in the title of a serial. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | year = 1971
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | pages = 36
 | chapter = 2. The Perils of Success
 }} 
 

==Plot==
A former Annapolis cadet is thrown out of the Naval Academy for cheating on an exam. He was framed, but he must enlist in the Navy to clear himself. Meanwhile he and his sweetheart search for a buried treasure on Lost Island, which everyone is after.

==Cast==
* William Courtleigh, Jr. as Neal Hardin
* Lillian Lorraine as Annette Illington
* William Conklin as Thomas Illington
* Ed Brady as Hernandez
* Henry Stanley as Ponto
* Richard Johnson as Joe Welcher
* Charles Dudley
* Helen Lackaye as Mrs. Hardin
* Bruce Smith as Captain John Hardin
* Lucy Blake
* Philo McCullough (unconfirmed)

==See also==
* List of film serials
* List of film serials by studio
*List of lost films

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 