The Hound of the Deep
 
{{Infobox film
| name           = The Hound of the Deep
| image          = 
| director       = Frank Hurley
| producer       = Frank Hurley
| writer         = Frank Hurley
| starring       = Eric Bransby Williams   Lillian Douglas   Jameson Thomas
| music          = 
| cinematography = Frank Hurley   Walter Sully
| studio         = Stoll Pictures
| distributor    = J.C. Williamson Films
| country        = Australia   United Kingdom
| editing        = 
| released       = 6 November 1926 (Aus)  February 1927 (UK)
| budget         = £5,000
| runtime        = 4,960 feet Silent
}} Australian silent silent drama film directed by Frank Hurley and starring Eric Bransby Williams, Lilian Douglas and Jameson Thomas.

Unlike many Australian silent films, a copy of it survives today.

==Plot==
Under the terms of his uncles will, John Strong (Eric Bransby Williams) must go to Thursday Island and find a pearl within two years or the Reuben Strong pearling station and his great wealth will revert to another, Black Darley (Jameson Thomas). Eventually Strong finds the pearl, defeats Darley and discovers romance with the daughter (Lillian Douglas) of an island trader (W.G. Saunders). 

==Cast==
*Eric Bransby Williams as John Strong 
*Lilian Douglas as Marjorie Jones 
*Jameson Thomas as Black Darley 
*W. G. Saunders as Cockeye Jones  Molly Johnson as Lady Cynthia 
*Dallas Cairns as Mr. Bullyer

==Production== Jungle Woman.  

Hurley and his crew left Sydney in August 1925 and travelled to Thursday Island where they shot The Hound of the Deep.

==Release==
Reviews generally praised the photography but had reservations about the story. 

The film was released in Britain as Pearl of the South Seas.

==References==
 

==External links==
*  in the Internet Movie Database
*  at National Film and Sound Archive
*  at Australian Screen Online

 
 
 
 
 
 
 


 
 