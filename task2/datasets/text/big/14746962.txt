Husbands Beware
{{Infobox Film |
  | name           = Husbands Beware
  | image          = Husbandsbeware 1sht.jpg
  | caption        = 
  | director       = Jules White Felix Adler Clyde Bruckman Lou Leonard Maxine Gates Christine McIntyre Dee Green Emil Sitka Doris Houck Nancy Saunders
  | cinematography = Henry Freulich 
  | editing        = Anthony DiMarco
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 00"
  | country        = United States
  | language       = English
}}

Husbands Beware is the 167th short subject starring American slapstick comedy team The Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Lou Leonard and Maxine Gates), and discover to their horror after the vows that the girls are a couple of battle axes. After being kicked out from their place, the new bridegrooms vow revenge on Shemp for introducing them. Later, Shemp has a voice lesson with student, Fanny Dinkelmeyer since he is a music teacher. He then discovers that he has to marry a woman within seven hours to receive $500,000 from his dead Uncles will.

After some searching, the Stooges finally find Shemps student, Ms. Dinkelmeyer. Several of Shemps old ex-girlfriends all arrive at the Justice of the Peaces office, wreaking havoc in an attempt to marry Shemp for his money. But Shemp weds the harridan, just under the deadline, and then discovers that there is no dead Uncle or will, and that everything was Moe and Larrys revenge because Shemp let Moe and Larry marry his two overweight sisters and got divorced. Shemp gets so angry, he takes out a gun and shoots Moe and Larry in the rear end as they try to flee.
  utters the catchphrase "Hold hands, you lovebirds!" in Husbands Beware]]

==Production notes==
The second half of Husbands Beware is stock footage from 1947s Brideless Groom. {{cite book | last = Solomon| first = Jon| authorlink = Jon Solomon| coauthors =| title = The Complete Three Stooges: The Official Filmography and Three Stooges Companion| publisher = Comedy III Productions, Inc| date = 2002| location = | pages = 477
| url = http://www.amazon.com/Complete-Three-Stooges-Filmography-Companion/dp/0971186804/ref=sr_1_1?ie=UTF8&s=books&qid=1201570359&sr=1-1 | doi =| id = | isbn = 0-9711868-0-4}}  In the new footage of the wedding sequence, a double stands in for Dee Green (Fanny Dinkelmeyer). The new scenes were filmed on May 17, 1955. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 