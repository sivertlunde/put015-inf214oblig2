Kaleldo
 
{{Infobox film
| name           = Kaleldo
| image          = Kaleldo_poster.jpg
| image size     = 
| alt            = 
| caption        = Film Poster
| director       = Brillante Mendoza
| producer       = 
| writer         = Boots Agbayani Pastor
| narrator       = 
| starring       = Johnny Delgado Cherry Pie Picache
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Viva Films
| released       = 2006
| runtime        = 
| country        = Philippines
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Kapampangan term for summer.

== Plot ==
The story is set in Guagua, Pampanga|Guagua, Pampanga a decade after the eruption of Mt. Pinatubo which ravaged the province with lahar. It follows the lives of Rodolfo "Mang Rudy" Manansala, a woodcarver, and his three daughters and their relationships with the people close to them in the span of seven summers. Grace, the youngest daughter, marries the mamas boy Conrad, and has to face the reality of leaving the ancestral house, to which she is deeply rooted, to go and live with her in-laws. Much against her will, to the point of staging an escape, she yields to the dictates of tradition. Yet she manages to cope up with married life, and on the fourth year of her marriage gets pregnant with her second child. Lourdes, the middle daughter and married to the weakling Andy Pineda with whom she has a daughter, goes into an illicit affair with a bank manager, for which reason Mang Rudy succumbs to a heart attack and becomes bedridden. For a while numbed by the infidelity of his wife, Andy later realizes his insignificance and retaliates by beating up and nearly killing Lourdes. Lourdes atones for her guilt by ministering to Andys wounds after he joins the rituals of the flagellants during the Holy Week. However, they both decide to separate. Jess, the eldest daughter, is a lesbian whose bitter luck in life is being unwanted by her own father. Yet she serves Mang Rudy to the hilt after he gets bedridden and makes the common sense of allowing her girlfriend Rowena to move in at the Manansala house, not so much for their own convenience as lovers but for Rowena to help in looking after Mang Rudy and in helping out with the household chores. But when a conflict arises between Lourdes and Rowena, Mang Rudy sends Rowena away. A year later, Mang Rudy succumbs to a second attack and when he dies several months later, Rowena sets foot on the ancestral house once more to pay her last respects. At the wake, Rowena comes to the realization that, like Andy, she never belonged and finally decides to leave Jess. Rowena gets married while Jess, along with her sisters, moves on with life. The story is told in three segments, with each segment told from the point of view of the three daughters. Attached to each segment are social occasion popularly observed in the province of Pampanga in the Philippines, with a symbolic motif for each, represented by the elements sun, fire, water, air, blood, moon and earth. With Rowenas wedding at the end of the film, as in Graces wedding at the start, a full cycle of life transpired.

== Cast ==
{| class="wikitable"
|-
! Main Cast
! Role
|-
| Johnny Delgado
| Mang Rudy
|-
| Cherry Pie Picache
| Jesusa (Jess)
|-
| Angel Aquino
| Lourdes
|-
| Juliana Palermo
| Grace
|-
| Allan Paule
| Andy
|-
| Criselda Volks
| Rowena
|-
| Lauren Novero
| Conrad
|}
Also to appear in the film are Miguel Faustmann, Liza Lorena, Rita Magdalena, Coco Martin, Ama Quiambao and Gamaliel Viray.

== Awards ==
Won 2007 Durban International Film Festivals Best Actress (Cherry Pie Picache)

It was Nominated in 2007 FAMAS Awards for Best Actor (Johnny Delgado), Best Actress (Cherry Pie Picache), Best Art Direction (Benjamin Padero), Best Cinematography (Odyssey Flores), Best Cinematography (Odyssey Flores), Best Director (Brillante Mendoza), Best Editing (Mark Philipp Espina), Best Picture, Best Story (Brillante Mendoza and Boots Agbayani Pastor) and Best Supporting Actor (Allan Paule).

In the 2007 Gawad Urian Awards it received the nominations for Best Actress (Pinakamahusay na Pangunahing Aktres) (Angel Aquino and Cherry Pie Picache), Best Cinematography (Pinakamahusay na Sinematograpiya) (Odyssey Flores), Best Direction (Pinakamahusay na Direksyon) (Brillante Mendoza), Best Editing (Pinakamahusay na Editing) (Mark Philipp Espina), Best Music (Pinakamahusay na Musika) (Jerrold Tarog), Best Picture (Pinakamahusay na Pelikula), Best Production Design (Pinakamahusay na Disenyong Pamproduksiyon) (Benjamin Padero), Best Screenplay (Pinakamahusay na Dulang Pampelikula) (Boots Agbayani Pastor), Best Sound (Pinakamahusay na Tunog) (Albert Michael Idioma), Best Supporting Actor (Pinakamahusay na Pangalawang Aktor) (Lauren Novero and Allan Paule).

In the 2007 Jeonju International Film Festival held in Korea it won the critics prize for the Network for the Promotion of Asian Cinema (NETPAC) which was received by director Brillante Mendoza.

==Sources==
  at the Internet Movie Database
 
 

 

 
 
 
 
 