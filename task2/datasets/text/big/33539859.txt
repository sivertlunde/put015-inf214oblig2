Three Steps Above Heaven
 
{{Infobox film
| name           = Three Meters Above The Sky
| image          = ThreeStepsAboveHeave2010Poster.jpg
| alt            =  
| caption        = Film poster
| director       = Fernando González Molina
| producer       = Francisco Ramos Álex Pina Mercedes Gamero Daniel Écija
| writer         = Federico Moccia
| screenplay     = Ramon Salazar
| story          = 
| starring       = Mario Casas María Valverde
| music          = Manel Santisteban
| cinematography = Daniel Aranyó
| editing        = 
| studio         = Zeta Cinema, Antena 3 Films, Charanga Films, Globomedia
| distributor    = Warner Bros
| released       =  
| runtime        = 118 minutes
| country        = Spain
| language       = spanish
| budget         = $3,000,000
| gross          = $9,955,766,61
}} Spanish film based on the novel of the same name by Federico Moccia.

==Plot==
The film starts with Hugo H Olivera voicing his thoughts about the ongoing trial where hes being prosecuted for assault on his mothers boyfriend. Let off with a fine of six thousand euros and a warning that further offenses would land him in prison, he walks out of the courtroom, switching his formal coat to a leather jacket and rides off on a Triumph Thruxton 07. He sees Babi Alcázar for the first time at a traffic intersection, on her way to school. He is immediately attracted to her as she sticks her face out for air and whistles before calling her a Dog-Face. He rides up to her car window and takes her hand. A stunned Babi manages to give him the finger before driving off.

Hugo, preferred to be known as simply H, leads a somewhat lawless life. Involved in street racing (without helmets, no less) and other chest-beating contests, he is clearly somewhat of a leader in the biker clique. Overthrowing Chino, another biker portrayed as his rival, in a contest of chin-ups, H and a few friends decide to crash a rich kids party. There he sees Babi again, and upon approaching her, promptly gets a milk shake thrown on his face. The bikers and the rich kids fight, and in the chaos, H picks Babi up and jumps into the pool. An angry Babi calls him a brute and curses him for ruining her Valentino dress. H winks and tells her to tell Valentino that the dress looks much better wet. Meanwhile, Babis best friend, Katina has her weekly allowance of fifty euros stolen by Hs best friend, Pollo. Despite requests, Pollo refuses to return her money and offers to buy her lunch when she says shell go hungry. Upset, she calls him a half-wit and strides off tearfully, leaving him speechless. The gate crashers leave when they get wind of the cops being informed. However, they follow Babi and her ex, Chico, who was the narc. They give up when Chico manages to throw one off his bike, but H catches up and beats him up for ruining the party for people who know how to have fun. Babi panics and stops a car for help. When the man, Mr. Santamaria tries to pull H off Chico. Both the Santamarias and Chico drive off fearfully, leaving Babi behind. H rides her home, but they are seen by Babis mother, who disapproves of H and his motorcycle.

The following day, Katina tells Babi that Pollo and her are seeing each other, and that he had invited her to see him race. She requests Babi to cover up for her. However, she has to go the Siamese races herself when Katinas mother calls her up and is not able to get through her. H teases her, and she snaps that he wouldnt be so cocky in court. He merely smiles and replies that the day Im up in the court, this nice girl here do anything to save me because by then shell be crazy about me. Provoked by his remarks and attitude, she decides to ride behind Chino. Borrowing Katinas belt, she buckles up facing backwards. Before the race could end, the police arrive and Babi is nearly arrested, but H intervenes and picks her up and drops her off at a place to hide while he could shake the cops off his tail. The cops, however, get a good shot of them zooming off. When H comes back, Babi comes out of her hiding place and is covered in manure. H asks her to take off her clothes if she wants to get onto the bike and offers her his jacket. Despite warnings not to turn around while she strips, H adjusts his rear-view mirror to enjoy the show. She threatens to cover his face with manure.

Dropping her at her place, H approaches her seductively and whispers in her ear if she would report him. Getting a firm yes in answer, he kisses her cheek softly and teasingly moves onto her lips. Just as she readies herself for a liplock, H draws back and teases her, saying that even though she calls him a pig, bastard, brute, she still wants him to kiss her. Babi slaps him before striding off.The papers carry their picture under the headlines of illegal street-racing. H makes a poster of the picture and glues it to her ceiling in her absence. Babi receives a picture of H waiting in a nightclub from Katina, with the message Look whos waiting for you. She laughs as her gaze falls onto the poster. Babi reaches the nightclub, lets herself loose with a vodka pineapple and gyrates to the music. H joins her, and they exchange Dog Face and Brute before having their first kiss. Babi returns home, exhilarated, and is confronted by her mother about the newspaper picture. When she lies on being asked about drinking, she is slapped. Her mother, regretting hitting her daughter, asks her husband to do something about the situation.Babi is grounded, but decides to skip school to spend the day with H. They, however, are seen by Babis teacher. As they ride to the beach, H sees his mother in a car with a boyfriend and seemingly loses control. driving off at an insane speed as Babi screams at him to stop. When he does, he vents out his anger on a pile of trash cans. She asks him about the woman in the car and he tells her that she is his mother. The story comes spilling out about his mothers infidelity, and Hs distance from his parents. Babi manages to calm him down and they spend a magical day at the beach.Coming back home, she fakes her mothers signature on the absence slip. Her mother introduces her to the new neighbour, Gustavo and asks her to show him around. Babi refuses, lying that she was to go the cinema with her girlfriends. Her mother realizes, but doesnt stop her. Babi and Katina go back to the racing scene, where Babi is confronted by Mara, a girl H was sleeping with previously. She brandishes Babis scarf and comments on its strength, saying that H used it to tie her to the bed the other day. Babi snatches the scarf from her and a fight ensues. Babi wins, leaving Mara bloody-lipped. Struck by guilt, she sobs on Hs shoulder, while he consoles her that he would never cheat or hurt her. To celebrate her first war victory, he takes her to a tattoo parlor where she gets an H inked onto her pelvis.

Worried about their daughter getting out of their hand, Claudio, Babis father meets up with H. H reassures him that his daughter is a good influence him, and puts his mind at ease.  Babi submits the absence slip with a fake signature, and her teacher immediately calls her mother up and threatens her with expulsion. In order to avoid that, her parents donate a lot of money, to the tune of 80 thousand dollars to the school. Her mother tells her to stay away from Katina. Upset, Babi escapes her home when H climbs in from her window and takes her to a pool with Katina and Pollo. They discuss sex, and H tells her that he is in no hurry. Under the excuse of a sleepover with Katina, she gains permission from her father to go out with H. He blindfolds her and drives her over to the beach house, the one she loved to "own" when she was a kid. They spend the night there, and Babi loses her virginity. H, for the first time, tells her that he loves her, and that he is happier from her to three steps above heaven. Knowing that she was facing problems with her teacher, H pays the teacher a visit and asks her to treat Babi well. When the teacher refuses to be threatened, H and Pollo kidnap her dog, Pepito. Babis mother cuts her allowance off and she has to babysit for money. H brings Pollo and Pepito around to take care of the kid while they spend time alone. On the other hand, they are interrupted when Pollo brings more of his friends in and start partying. Babi gets upset and throws everyone out. In order to make up, H cooks up a romantic dinner and calls her up. She reacts angrily, accusing his friends of stealing one of her mothers rings. H, outraged, reaches the biking scene and confronts Chino, beating him senseless before Mara stops him and returns the ring. H gives her the ring back, and she lies convincingly in the court the next day. Embarrassed and upset, her mother accuses her of humiliating their family when they had promised to help the Santamarias. Babi shakes her off, saying that she doesnt want to live her mothers lousy life before riding off with H. She tells him that she wont lie for him again; if this continues, they would be through. H promises to change for her.The results come out, and Babi gets through with the best grade, though a bit confused as she was expecting to fail. 

She decides to thank her teacher when she sees her boxing her stuff, and Pepito. Stunned, she leaves and breaks up with H for threatening her teacher and messing her life up. Her mother plans her birthday party, and an unenthusiastic Babi readies herself for the evening with Katinas help, who promises to drop by after racing with Pollo.H, despite promising to be at the race for Pollo, turns up at the party to reconcile with Babi. Babi welcomes him with open arms and they dance. Gustavo, who is interested in Babi, accidentally spills a drink over her and tries to help even though she protests. H, irritated asks him to stay away, and when he reacts, punches him in the face. Babi gets upset and helps Gustavo up, and H is led away by Claudio. Daniela, Babis sister, gets a call that Katina and Pollo has had an accident.Babi and H rush to the site, where Katina is in a state of shock, begging H to find Pollo because they wouldnt allow her to see him. H pushes his way through the crowd and finds Pollo being zipped up in a body-bag and gets hysterical, hitting the bike. Babi tries to comfort him, but ultimately leaves. H runs after her, and she screams at him not to touch her. She accuses him of not caring about anything or anyone, not even himself, and blames him for Pollos death, saying that he killed him. H, beyond rational thinking, slaps her across the face. Stunned and hurt, Babi walks off, and H doesnt stop her.Many days later, Babi is shown to have had moved on, dating Gustavo now, and not keeping in touch with Katina. Katina shows up at Hs house on Christmas, detached and depressed, and gives him a framed picture of him and Pollo at a pool. Touched, he thanks her and asks her to stay for dinner. She declines, asking him if he was still in touch with Babi. He shakes his head, and she tells him to forget her as she is with a different crowd now.H decides to try for the last time, and calls at her house phone from the phone box outside her home. Babis mother answers and realizes who it is. She asks him to hold on when he asks for her, and purposefully lets him hear that Babi and Gustavo are together and going away for a day. She asks him to take care of her daughter, and he assures her he will. Rafaela hangs up, and H sees Babi and Gustavo coming out in a car. Babi sees him, and is disturbed.H goes back and sits outside his building, crying. His brother asks him if he is okay, and H breaks down in his arms.H heals, and takes his brother to the beach where Babi and he spent their first date. He tells him of his plans of going to London for work, an idea his brother agrees as a good one. H recounts his time with Babi, knowing that he would, indeed, never feel as though he were Three Steps Above Heaven again.

==Differences between the Italian and the Spanish film==
* In the Italian version the first encounter between Step and Babi happens while he is sitting with Pollo on a bench and see go Babi in a car noticing her, while in the Spanish version is equal to the book; Hache, on his bike, note in the traffic Babi and approaching to the his car asking her to take a ride together.   
* In the Italian version the illegal racing not show scenes of use of sticks and kicking during the races as happens in the novel, while in the Spanish version are shown similar scenes.
* In the Spanish version is shown who Hache has sex with Mara as explained in the book, while in the Italian version this does not happen.
* In the Italian version is absent the scene where Step called Babi for organize its first day saw at school, while in the Spanish version this scene is shown and is similar a that of the book.
* In the Italian version Step is less violent, while in the Spanish version Hache shows himself equal to the book and is explained that his shots are due to trauma of the mothers relationship with another man.
* In the Italian version the quarrel between Babi and Madda is completely different from the one narrated in the book, while in the Spanish version is similar.
* In the Italian version not shown the fight between Step and the Sicilian, while in the Spanish version is shown and is similar a that described in the novel.
* In the Italian version is absent the scene where Step, after the death of Pollo, called at the home of Babi asking if she is at home, while in the Spanish version this scene is shown.

==Cast==
* Mario Casas - Hugo "H"
* María Valverde - Babi
* Álvaro Cervantes - Pollo
* Marina Salas - Katina
* Diego Martín - Alejandro
* Lucho Fernández - Chino
* Andrea Duro - Mara
* Nerea Camacho - Dani
* Pablo Rivero - Gustavo
* Cristina Plazas - Rafaela
* Clara Segura - Forga
* Jordi Bosch - Claudio
* Joan Crosas - Padre Hache

==External links==
* 


 
 
 
 