King and the Clown
{{Infobox film
| name           = King and the Clown
| image          = The King and the Clown movie poster.jpg
| caption        = Promotion poster for King and the Clown
| film name      = {{Film name
 | hangul         =    
 | hanja          =  의  
 | rr             = Wang-ui Namja
 | mr             = Wangŭi Namja}}
| director       = Lee Joon-ik
| producer       = Jeong Jin-wan Lee Joon-ik
| writer         = Choi Seok-hwan 
| based on       =  
| starring       = Kam Woo-sung Jung Jin-young Lee Joon-gi
| music          = Lee Byung-woo
| editing        = Kim Sang-beom Kim Jae-beom
| cinematography = Ji Kil-woong
| studio         = Cineworld Eagle Pictures
| distributor    = Cinema Service CJ Entertainment (international)
| released       =  
| runtime        = 119 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| gross          =  
}} 2005 South Korean historical drama film, starring Kam Woo-sung, Jung Jin-young and Lee Joon-gi. It was adapted from the 2000 stage play, Yi ("You") about Yeonsangun of Joseon, a Joseon dynasty king and a court clown who mocks him. It was released on 29 December 2005, runs for 119 minutes; and distributed domestically by Cinema Service and internationally by CJ Entertainment.
 the sixth highest grossing film in South Korea.

==Cast==
* Kam Woo-sung as Jang-saeng
* Jung Jin-young as King Yeonsan
* Lee Joon-gi as Gong-gil
* Kang Sung-yeon as Jang Nok-su
* Yoo Hae-jin as Yuk-gab

== Synopsis == King Yeonsan, two male street clowns and tightrope walkers, Jangsaeng (Kam Woo-sung) and Gong-gil (Lee Joon-gi), are part of an entertainer troupe. Their manager prostitutes the beautiful Gong-gil to rich customers, and Jangsaeng is sickened by this practice. After Gong-gil kills the manager in defense of Jangsaeng, the pair flee to Seoul, where they form a new group with three other street performers.
 royal court, including the king and his new concubine Jang Noksu. After they are arrested for treason, Jangsaeng makes a deal with Choseon, who turns out to be one of the kings servants, either to make the king laugh at their skit or to be executed. They perform their skit for the king, but the three minor performers are too terrified to perform well. Gong-gil and Jangsaeng barely save themselves with one last joke at the king, who laughs and then makes them part of his court. The king falls for the effeminate Gong-gil, whom he calls to his private chambers often to perform finger puppet shows. Jangsaeng becomes jealous of this relationship. Meanwhile, the king becomes more and more unstable. He makes the clowns perform a skit depicting how his mother, the favorite concubine of the former king, was forced to take poison after being betrayed by other jealous concubines. The king then slaughters these concubines at the end of the play. Jangsaeng asks Gong-gil to leave with him and the gang at once before the king kills them too during one of his homicidal fits. Gong-gil, who initially sympathized with the king, begs the tyrant to give him his freedom but the king refuses.

The kings main concubine, Jang Noksu, becomes enraged by the attention the king has been lavishing on Gong-gil. The council members try to have him killed during a hunting trip, resulting in the death of one of the members of the street performing team. Days after the hunting trip, the king forcibly kisses Gong-gil. Then, Jang Noksu tries to have him jailed by having flyers run in Gong-gils handwriting insulting the king severely. Jangsaeng takes the blame for the crime for which Gong-gil has been falsely accused and is imprisoned.

Choseon secretly releases Jangsaeng, telling him that he should forget Gong-gil and leave the palace. But Jangsaeng ignores the advice and returns to walk on his tightrope across palace rooftops, this time openly and loudly mocking the king. The king shoots arrows at him while Gong-gil tries in vain to stop him. Jangsaeng falls and is caught, and has his eyes seared with burning iron as punishment before being thrown into prison again. Gong-gil attempts suicide, but his life is saved by the palace doctors. The king has Jangsaeng walk his tightrope blind. As Jangsaeng tells the story of his and Gong-gils trials and tribulations while balancing on the rope, Gong-gil runs out to join him. Gong-gil asks Jangsaeng what he would like to return as in his next life and Jangsaeng replies that he would still choose to be a clown. Gong-gil answers that he too would return as nothing else but a clown.

Throughout the film, the tyranny of the king and corruption of his courts is revealed. At the very end there is a popular uprising resulting in an attack on the palace, and as people storm the court, Jangsaeng and Gong-gil jump up from the rope together, and Jangsaeng tosses away his fan. The last scene is a happy one where Jangsaeng and Gong-gil appear to be reunited with their clowning troupe, including the friend who died earlier during the hunting incident. The whole company jokes, sings and dances, as they all walk away cheerfully into the distance.   

==Background==
The film was adapted from the Korean stage play Yi, written by Kim Tae-woong, centered around Gong-gil, the feminine actor. It was based on a small passage from the Annals of the Joseon Dynasty that briefly mentions the kings favorite clown, Gong-gil, whereas Jang-saeng is a fictional character. In the Joseon Dynasty, "Yi" was what the king called his beloved subjects. Since first staged in 2000, the play has won numerous awards, including the best stage play of the year, best new actor (for Oh Man-seok) and top 3 best plays of the year by the National Theater Association of Korea, and best stage play for 2001 by the The Dong-a Ilbo#Arts|Dong-A Art Foundation.  

The movie is referred to by various titles. It is sometimes known as The Kings Man (the literal English translation of the Korean title). In Chinese, the title is "王的男人" or "王和小丑", and in Japanese, it is known as "王の男". It is also known as The Royal Jester in English, as the movies English translator found it more fitting than the original title. 

==Reception==
 
In South Korea, the film sold a total of 12.3 million tickets, including 3,659,525 in Seoul, in its four months of screening, which ended on 18 April 2006.  It ranked first and grossed   in its first week of release  and grossed a total of   after 12 weeks of screening.  Its worldwide grossed is  . 

The low-budget film, made for  , surpassed the 10 million viewer mark on 2 February, received good reviews and positive word-of-mouth for being well-made with great acting. Leading to its commercial success, which is remarkable, considering its focus on traditional arts with   and Silmido (film)|Silmido, who have both surpassed 10 million viewers.  

This film was chosen by the Korea Film Council-appointed committee as South Koreas submission for the 2006   and Time (2006 film)|Time for its overall aesthetic and commercial quality.  

The film propelled the then unknown Lee Joon-gi into Asia-wide stardom. 

==International release==
* Taiwan: May 7, 2006
* Singapore: June 22, 2006
* Canada: September 7, 2006 (Vancouver/Toronto Film Festival)
* Japan: October 21, 2006 (Tokyo Film Festival) / December 9, 2006 (theatrical release)
* Shanghai: October 28, 2006
* United Kingdom: October 29, 2006 (London Film Festival)
* South Africa: November 14, 2006 (Cape Town Film Festival)
* New Zealand: December 1, 2006 (Film Festival)
* United States: January 3, 2007 (Los Angeles)
* Italy: March 30, 2007 (Florence Film Festival)
* France: April 1, 2007 (Deauville Film Festival) / 23 January 2008 (theatrical release)

==Awards and nominations==
;2006 Baeksang Arts Awards 
* Best New Actor - Lee Joon-gi
* Jury Prize
* Nomination - Best Film
* Nomination - Best Actor - Kam Woo-sung
* Nomination - Best Director - Lee Joon-ik
* Nomination - Best Screenplay - Choi Seok-hwan

;2006 Chunsa Film Art Awards
* Best Actor - Kam Woo-sung
* Best Supporting Actor - Jang Han-seon

;2006 Grand Bell Awards
* Best Film
* Best Actor - Kam Woo-sung
* Best Director - Lee Joon-ik
* Best Supporting Actor - Yoo Hae-jin
* Best Screenplay - Choi Seok-hwan
* Best Cinematography - Ji Kil-woong
* Best New Actor - Lee Joon-gi
* Nomination - Best Supporting Actress - Kang Sung-yeon
* Nomination - Best Art Direction - Kang Seung-yong
* Nomination - Best Costume Design - Shim Hyeon-seop
* Nomination - Best Editing - Kim Jae-beom and Kim Sang-beom
* Nomination - Best Music - Lee Byung-woo
* Nomination - Best Sound - Choi Tae-young

;2006 Blue Dragon Film Awards
* Best Music - Lee Byung-woo
* Nomination - Best Film
* Nomination - Best Actor - Kam Woo-sung
* Nomination - Best Director - Lee Joon-ik
* Nomination - Best Supporting Actor - Yoo Hae-jin
* Nomination - Best Supporting Actress - Kang Sung-yeon
* Nomination - Best Art Direction - Kang Seung-yong
* Nomination - Best Visual Effects - Lee Yun-seok
* Nomination - Best New Actor - Lee Joon-gi

;2006 Korean Film Awards
* Best New Actor - Lee Joon-gi
* Nomination - Best Film
* Nomination - Best Director - Lee Joon-ik
* Nomination - Best Supporting Actress - Kang Sung-yeon
* Nomination - Best Art Direction - Kang Seung-yong

;2007 Deauville American Film Festival
* Jury Prize

==Original soundtrack==
# "가려진" - "Veiled" by Jang Jae-hyeong (Jang-sengs theme)
# "프롤로그 - 먼길" - "Prologue - Long Roads"
# "각시탈" - "Gak-shi Tal (Mask of a Woman)"
# "돌아올 수 없는" - "Cannot Return"
# "너 거기 있니? 나 여기 있어." - "Are you over there? I am over here."
# "세상속으로" - "Into the World"
# "위험한 제의 하나" - "Dangerous Suggestion Number One"
# "행복한 광대들" - "The Happy Clowns"
# "내가 왕이 맞느냐" - "Am I the King or not"
# "위험한 제의 둘" - "Dangerous Suggestion Number Two"
# "꿈꾸는 광대들" - "The Dreaming Clowns"
# "수청" - "Serve Maiden"
# "인형놀이" - "Playing with Dolls"
# "연정" - "Romantic Emotions"
# "그림자놀이" - "Playing with Shadows"
# "피적삼의 울음소리" - "The Cry of Rags"
# "광대사냥" - "Clown Hunt"
# "광대의 죽음" - "Death of a Clown"
# "어서 쏴" - "Shoot Now"
# "질투" - "Envy"
# "장생의 분노" - "The Fury of Jang-Seng"
# "내가 썼소" - "I wrote it."
# "애원" - "Plea"
# "장생의 외침" - "The Yell of Jang-Seng"
# "눈먼장생" - "Jang-Seng the Blind"
# "자궁속으로" - "Into the Womb"
# "반정의 북소리" - "Ban-Jeongs Sounds of Drumming"
# "반허공" - "Mid-air"
# "에필로그 - 돌아오는 길" - "Epilogue - The Homeward Road"
# "반허공" Guitar Version - "Mid-air" Guitar Version

==See also==
*List of lesbian, gay, bisexual, or transgender-related films by storyline

== References ==
 

== External links ==
*  
*  
*  
*    

 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 