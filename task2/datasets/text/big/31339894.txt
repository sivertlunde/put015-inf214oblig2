Love Inventory
{{Infobox film 
| name           = Love Inventory  aka Reshimat Ahava Love Inventory film poster
| caption        = Film poster David Fisher
| sound          = Amos Zipori  David Fisher  David Fisher  Gideon Fisher  Ronel Fisher  Amnon Fisher 
| music          = Amnon Fisher   Doron Shenkar
| cinematography = Itzik Portal
| editing        = Tali Halter-Shenkar	
| release date   = 2000
| runtime        = 90 minutes French subtitles German subtitles English subtitles
}} David Fisher David Fisher. This is the First film in the family trilogy   created by director David Fisher followed by Mostar Round-Trip (2011) and Six Million and One (2011).

== Plot summary ==
  David Fisher feels that his family has grown apart and that his siblings are focused on their careers and relationships with their spouses and children. Fisher believes that a search for their sister, who was allegedly taken from their parents at birth, will help them bond.
Fisher and his four siblings, whose parents were Holocaust survivors, set out on a journey that deals with both family dynamics and the history and establishment of the State of Israel. The siblings become amateur detectives, searching for any evidence that might lead them to their sister. 

It is a story of five siblings looking for a lost sister who end up finding themselves. 

== Production ==
The film was shot on    in Europe and on 73 Public Broadcasting Service (PBS) channels in the USA  in the framework of the Independent Lens series.  
The film had limited theatrical release in Israel  and in German speaking countries in Europe by  .  The film is distributed to institutions and individuals in the USA by the  .

In 1999 before the film was released, and as part of the production of the film, an imaginary composite portrait of the missing sister was published in the daily newspaper Yedioth Aharonoth.  The portrait was composed of the common characteristics of the five Fisher siblings and was issued as an attempt to draw the attention of the missing sister, if she was indeed alive as was suspected by the Fishers.

Additional production credits:
 Aya Minster - Script Editor.
 Etay Elohev - Sound.
 Eli Taragan - Sound.
 The films theme song entitled   was composed and performed by Amnon Fisher.

== Festivals and Awards ==
The film won the following awards:
* The Wolgin award for best documentary at the Jerusalem International Film Festival 2000. 
* The Ophir Award by the Israeli Film Academy for best documentary in 2000. 
* The DocuNoga award for best documentary 2000. 
* The Merit award at the Taiwan International Documentary Festival 2002. 

The film was selected and screened at the following International film festivals:
* Jerusalem International Film Festival 2000 (World Premiere). 
* Berlin International Film festival Forum 2001 (International Premiere). 
* Taiwan International Documentary Festival 2002. 
* Hong Kong International Film Festival 2001. 
* Chicago International documentary festival 2001. 
* Worldfest Film Festival, Huston. 
* Input Conference 2001. 
* DokFest Munich Film Festival. 
* Boston International Film Festival. 
* Bangkok International Film Festival 2001. 
* CineManila Film Festival. 
* Various Jewish Film Festivals.    

== Critical Reception ==
* 
* 
* 

==References==
 

== External links ==
*   at  
*   at ARTE
*   at ITVS
*   at Independent Lens/PBS
*   at National center for Jewish films
*   at British Film Institute
*  
*   In Hebrew
*   at Fisher Features

 
 
 
 
 
 
 
 