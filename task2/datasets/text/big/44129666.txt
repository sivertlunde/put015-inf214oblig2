Chillu
{{Infobox film 
| name           = Chillu
| image          =
| caption        =
| director       = Lenin Rajendran
| producer       =
| writer         = Lenin Rajendran
| screenplay     = Lenin Rajendran
| starring       = Rony Vincent Shanthi Krishna Venu Nagavally Sukumari Jagathy Sreekumar Adoor Bhasi Nedumudi Venu
| music          = MB Sreenivasan
| cinematography = Vipin Das
| editing        = Ravi
| studio         = Hayyath Movies
| distributor    = Hayyath Movies
| released       =  
| country        = India Malayalam
}}
 1982 Cinema Indian Malayalam Malayalam film, directed by Lenin Rajendran. The film stars Ront Vincent, Shanthi Krishna, Venu Nagavally, Sukumari, Jagathy Sreekumar, Adoor Bhasi and Nedumudi Venu in lead roles. The film had musical score by MB Sreenivasan.   

==Cast==
*Rony Vincent
*Shanthi Krishna
*Venu Nagavally
*Sukumari
*Jagathy Sreekumar
*Adoor Bhasi
*Nedumudi Venu
*Jalaja
*Kanakalatha
*Anitha

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup, Kavalam Narayana Panicker and Edasseri. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chaithram chaayam chaalichu || K. J. Yesudas || ONV Kurup || 
|-
| 2 || Mannu || Venu Nagavally || Kavalam Narayana Panicker || 
|-
| 3 || Oru vattam koodiyen || K. J. Yesudas || ONV Kurup || 
|-
| 4 || Oru vattam koodiyen || S Janaki || ONV Kurup || 
|-
| 5 || Pokkuveyil ponnuruki  || K. J. Yesudas || ONV Kurup || 
|-
| 6 || Poothappaattu || Balachandran Chullikkad || Edasseri || 
|}

==References==
 

==External links==
*  

 
 
 

 