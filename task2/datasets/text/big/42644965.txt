Lucrezia Borgia (1922 film)
{{Infobox film
| name           = Lucrezia Borgia 
| image          =
| caption        =
| director       = Richard Oswald 
| producer       = 
| writer         = Harry Scheff (novel)   Richard Oswald
| starring       = Conrad Veidt   Liane Haid   Albert Bassermann   Paul Wegener
| music          =  
| cinematography = Carl Drews   Karl Freund   Károly Vass (cinematographer)|Károly Vass   Frederik Fuglsang
| editing        = 
| studio         = Richard-Oswald-Produktion  UFA
| released       = 20 October 1922 
| runtime        = 96 minutes
| country        = Germany
| awards         =
| language       = Silent   German intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent historical sets needed.

==Cast==
* Conrad Veidt as Cesare Borgia  
* Liane Haid as Lucrezia Borgia  
* Albert Bassermann as Papst Alexander VI Rodrigo Borgia  
* Paul Wegener as Micheletto  
* Heinrich George as Sebastiano  
* Adolf E. Licho as Lodowico  
* William Dieterle as Giovanni Sforza, Herr von Pesaro 
* Lothar Müthel as Juan Borgia  
* Alphons Fryland as Alfonso, Prinz von Arragon 
* Kathe Oswald as Naomi  
* Alexander Granach as ein Gefangener  
* Anita Berber as Gräfin Julia Orsini  
* Lyda Salmonova as Diabola, Tierbändigerin  
* Mary Douce as Florentina  
* Max Pohl as Fratelli, Waffenschmied  
* Adele Sandrock as Die Äbtissen  
* Wilhelm Diegelmann as Wirt  
* Philipp Manning as Diener Cesares  
* Hugo Döblin as Diener Cesares  
* Ernst Pittschau as Manfredo  
* Clementine Plessner as Fratellis Frau  
* Viktoria Strauß as Rosaura  
* Tibor Lubinszky as Gennaro, Page

== References ==
 

==Bibliography==
* Elsaesser, Thomas. Weimar Cinema and After: Germanys Historical Imaginary. Routledge, 2013.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 