Dead Man's Burden
{{Infobox film
| name           = Dead Mans Burden
| image          = Dead_Mans_Burden_Poster.jpg
| caption        =
| director       = Jared Moshe
| producer       = Veronica Nickel
| writer         = Jared Moshe
| starring       = Clare Bowen David Call Richard Riehle
| music          = H. Scott Salinas
| cinematography = Robert Hauer
| editing        = Jeff Israel
| studio         = Illuminaria Productions Stick! Pictures
| distributor    = Cinedigm Entertainment Group
| released       =  
| country        = United States
| runtime        = 93 minutes
| language       = English
| budget         =
}} Western film directed by Jared Moshe. The film premiered on June 16, 2012 at the Los Angeles Film Festival and stars Clare Bowen and Barlow Jacobs as two siblings that reunite over the death of their father and a potential sale of their land. 

==Synopsis== Civil War and the death of Marthas father. Theyre given hope for a better life when a mining company shows interest in purchasing their Homestead (buildings)|homestead, but things become tense when Marthas brother Wade (Barlow Jacobs), who defected to the Union Army returns home after hearing of their fathers death- unaware that Martha herself was the one who brought about his demise.

==Cast==
*Barlow Jacobs as Wade McCurry
*Clare Bowen as Martha Kirkland
*David Call as Heck Kirkland
*Joseph Lyle Taylor as E.J. Lane
*Richard Riehle as Three Penny Hank
*Jerry Clarke as Sheriff Deacon
*Adam OByrne as Archie Ainsworth
*Travis Hammer as Ben Ainsworth
*Luce Rains as Joe McCurry
*William Sterchi as WC Claymore

==Production==
When writing the script for Dead Mans Burden, Moshe wrote with the intent to cast the film with lesser known actors, as he didnt want "to bring in a big-name actor who didn’t look like they belong in that period."    Moshe did not use storyboards, as the film had a tight budget and couldnt afford a storyboard artist, instead watching other Western films for inspiration and working closely with cinematographer Robert Hauer.  Filming took place in New Mexico over an 18 day period, where the cast experienced freak storms that forced the cast to walk to the set but did not delay filming. 

==Reception==
Critical reception has been mixed to positive and the film holds a rating of 76 on Metacritic (based on 12 reviews) and 76% on Rotten Tomatoes (based on 17 reviews).   Common elements praised in the film was Moshés choice in cast, which Variety lauded as a highlight.  However, in their mostly positive review IndieWire remarked that the lack of major stars and the choice to film a Western (which they saw as a "mostly defunct genre") could jeopardize commercial prospects.  The New York Daily Newss review was more mixed, as they felt that the "verbose period film has the complicated plot and tight pacing of a cable TV drama, which is then squashed into an indie-film paradigm." 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 

 