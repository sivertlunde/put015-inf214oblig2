Paprika (1933 Italian film)
{{Infobox film
| name           = Paprika
| image          = 
| caption        = 
| director       = Carl Boese
| producer       = Ferruccio Biancini Viktor Klein
| writer         = Oreste Biancoli Bobby E. Lüthge
| starring       = Vittorio De Sica
| music          = 
| cinematography = Reimar Kuntze
| editing        = Fernando Tropea
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
}}

Paprika is a 1933 Italian comedy film directed by Carl Boese and starring Vittorio De Sica.    A German-language version Paprika (1932 film)|Paprika and a French version Paprika (1933 French film)|Paprika were also made.

==Cast==
* Renato Cialente - Paolo
* Vittorio De Sica
* Gianfranco Giachetti - Urbano
* Eva Magni - Lida Bonelli
* Elsa Merlini - Ilonka
* Sergio Tofano - Checco
* Enrico Viarisio - Massimo Bonelli

==References==
 

==See also==
* Italian films of 1933

==External links==
* 

 

 
 
 
 
 
 
 
 