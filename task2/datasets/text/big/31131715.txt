School for Randle
{{Infobox film
| name           = School for Randle
| image          =
| image_size     =
| caption        =
| director       = John E. Blakeley
| producer       = John E. Blakeley
| writer         = Harry Jackson   Frank Randle   John E. Blakeley
| narrator       =
| starring       = Frank Randle   Dan Young   Alec Pleon   Terry Randall
| music          = Fred Harris Ernest Palmer
| editing        = Dorothy Stimson
| studio         = Mancunian Films
| distributor    = Mancunian Films
| released       = 1949
| runtime        = 89 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
School for Randle is a 1949 British comedy film directed by John E. Blakeley and starring Frank Randle, Dan Young and Alec Pleon.  A school caretaker turns out to be the father of one of the pupils. When she runs away from home to pursue a career on the stage, he goes to persuade her to come back to school. The title is a reference to the Richard Brinsley Sheridan play The School for Scandal. It was made at the Manchester Studios, and was one of a string of cheaply made, but commercially successful films starring Randle during the era.

==Cast==
* Frank Randle as Flatfoot Mason
* Dan Young as Clarence
* Alec Pleon as Blockhead
* Terry Randall as Betty Andrews
* Hilda Bayley as Mrs. Andrews
* Frederick Bradshaw as Mr. Andrews
* Jimmy Clitheroe as Jimmy
* Maudie Edwards as Bella Donna
* John Singer as Ted Parker
* Elsa Tee as Miss Weston

==References==
 

==External links==
* 

 
 
 
 
 
 
 



 
 