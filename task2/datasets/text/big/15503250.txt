The Red Ace
 
{{Infobox film
| name           = The Red Ace
| image          = The Red Ace.jpg
| caption        = 
| director       = Jacques Jaccard
| producer       = 
| writer         = Jacques Jaccard
| starring       = Marie Walcamp Lawrence Peyton
| music          = 
| cinematography = 
| editing        =  Universal Film Manufacturing Co.
| released       =  
| runtime        = 16 episodes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 adventure film serial directed by Jacques Jaccard. An incomplete print which is missing four chapters survives in the film archive of the Library of Congress.   

==Cast==
* Marie Walcamp - Virginia Dixon
* Lawrence Peyton - Sergeant Sidney Winthrop (as Larry Peyton)
* L. M. Wells - Pierre Fouchard
* Bobby Mack - Patrick Kelly, Messenger
* Charles Brinley - Steele Heffern
* Harry Archer - Dr. Hertzman
* Noble Johnson - Little Bear
* Yvette Mitchell - Red Fawn
* Nellie Allen - Bertha Schriver
* Miriam Shelby - Kate

==See also==
* List of film serials
* List of film serials by studio
* List of incomplete or partially lost films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 