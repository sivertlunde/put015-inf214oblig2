Bellyful
{{Infobox film
| name           = Bellyful
| image          = Le Conte du ventre plein.jpg
| caption        = Poster.
| director       = Melvin Van Peebles
| producer       = Jean-Pierre Saire, Yves Pasquier
| eproducer      =
| aproducer      = Ron van Eeden, F. Jansen, Emmanuel Paulin, Stéphane Rollot, Eric Stanley
| writer         = Melvin Van Peebles
| starring       =
| music          = Melvin Van Peebles
| cinematography = Philippe Pavans de Ceccatty
| editing        = Catherine DHoir
| distributor    = Euripide Distribution
| released       = June 28, 2000
| runtime        = 105 minutes
| country        = United States France
| awards         =
| language       = French English
| budget         =
| preceded by    =
| followed by    =
}}

Le Conte du ventre plein (international English title: Bellyful) is a 2000 film written and directed by Melvin Van Peebles.

== Plot summary ==
 liberal do-gooders in late 60s France. With orders piling up at their bistro, The Full Belly, Loretta and Henri, self-described "pillars of the community," hire Diamantine as a waitress in order to give a poor black orphan a chance at a better life. At home, they tell their trusting, new live-in employee that shes "one of the family," yet in town they encourage widespread disapproval of her. When they convince her to carry an extended joke to full term - pretending shes pregnant - Diamantine, and a slightly shady friend of the couple, Jan, become entangled in an elaborate charade.

==External links==
* 
*   at Allmovie

 

 
 
 
 
 
 
 
 
 
 
 


 