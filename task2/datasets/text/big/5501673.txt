Gunn (film)
{{Infobox film
| name          = Gunn
| image         = GunnPoster.jpg
| caption       = Theatrical release poster
| director      = Blake Edwards
| screenplay    = William Peter Blatty Blake Edwards
| story         = Blake Edwards Craig Stevens Laura Devon
| producer      = Owen Crump Blake Edwards
| music         = The Gordian Knot Henry Mancini Philip Lathrop
| distributor   = Paramount Pictures
| released      = 28 June 1967
| runtime       = 94 minutes English
| budget        =
}}
 1967 mystery Craig Stevens. It featured the same lead role and actor from the 1958-1961 television series Peter Gunn, and a well-known Henry Mancini theme. The characters of Gunns singing girlfriend Edie Hart, club owner "Mother" and friendly Police Lieutenant Jacoby were played by different actors. It was followed 20 years later by the TV remake starring Peter Strauss.

==Plot==
A gangster named Scarlotti once saved private detective Peter Gunns life, but now Scarlottis been killed and Fusco intends to take over the towns crime syndicate.

Gunn is determined to find out who the killer is, and soon he and Lt. Jacoby are convinced that Fusco himself must be behind it. Fusco denies it and gives a deadline to Gunn, to solve the murder or end up dead himself.

==Cast== Craig Stevens as Gunn
* Laura Devon as Edie
* Edward Asner as Jacoby
* Albert Paulsen as Fusco
* Helen Traubel as Mother
* Regis Toomey as the Bishop
* J. Pat OMalley as Tinker
* Sherry Jackson as Samantha

==Production== The Exorcist. The film was originally titled —but then only advertised as—Gunn...Number One!; no sequels followed.

Although the complete television series is available on DVD, the film version of Gunn has never been released on home video in any format.

==Notes==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 