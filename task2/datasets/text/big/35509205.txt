Night Eyes 2
{{Infobox film
| name = Night Eyes 2
| image = Night_Eyes_2_cover_art.jpg
| image_size = 200px
| caption = Film poster
| director = Rodney McDonald
| producer = Ashok Amritraj, Andrew Stevens
| writer = Simon Levy, Michael W. Potts
| starring = Shannon Tweed Andrew Stevens
| music = Timothy Leitch	
| cinematography = Kent L. Wakeford
| editing = David H. Lloyd
| distributor = Turner Home Entertainment
| released =  
| runtime = 97 minutes
| language = English
| budget =
}} thriller film. It stars Andrew Stevens and Shannon Tweed. The film is a sequel to Night Eyes which was released two years earlier.

==Synopsis==
Will Griffith (Andrew Stevens) is hired to secure and protect the mansion of South American diplomat Héctor Mejenes (Richard Chaves), following attempts on his life. However, his wife Marilyn (Shannon Tweed) ends up attracted to Will.

==Cast==
* Andrew Stevens – Will Griffith
* Shannon Tweed – Marilyn Mejenes
* Richard Chaves – Héctor Mejenes
* Tim Russ – Jesse Younger
* Richard Chaves – Hector Mejenes
* Geno Silva – Luis
* John OHurley – Detective Turner
* Julian Stone – Safecracker
* Tessa Taylor – Vivian Talbot

==Filming==
The movie was filmed within a month, starting on April 29, 1991, and finishing in May 25, 1991.  It was filmed in Los Angeles, California. 

==References==
 

== External links ==
*  

 

 
 
 

 