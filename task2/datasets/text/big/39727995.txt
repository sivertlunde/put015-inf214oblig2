The Page from the Dalmasse Hotel (1933 film)
{{Infobox film
| name           = The Page from the Dalmasse Hotel
| image          = 
| image_size     = 
| caption        = 
| director       = Victor Janson  Karl Schulz   Robert Wüllner
| writer         = Maria Peteani (novel)   Walter Wassermann
| narrator       =  Hans Junkermann   Trude Hesterberg
| music          = Eduard Künneke
| editing        = Roger von Norman
| cinematography = Hugo von Kaweczynski
| studio         = Schulz & Wuellner Film 
| distributor    = Terra Film
| released       = 23 November 1933
| runtime        = 83 minutes
| country        = Germany
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German comedy Hans Junkermann. novel of 1958 Austrian film.

==Cast==
* Dolly Haas as Friedel Bornemann 
* Harry Liedtke as Baron Arthur von Dahlen  Hans Junkermann as Baron von Potten 
* Trude Hesterberg as Mrs. Wellington 
* Gina Falckenberg as Miss Mabel Wellington 
* Luise Stösel as Käthe Petersen 
* Vera Witt as Frau Petersen 
* Walter Steinbeck as Dr. Köppnitz 
* Ida Krill as Die Leuteköchin  Hans Richter as Der Page Ottokar 
* Otto Grüneberg as Der Page Paul  Tommy Thomas as Der Liftchef 
* Hans Adalbert Schlettow as Graf Tarvagna 
* Erich Fiedler as Sekretär Spöhn 
* Maria Reisenhofer as Baronin von Dahlen 
* Martha Ziegler as Mädchen bei den von Dahlens 
* Albert Hörrmann as Ein Kommissar

==References==
 

==Bibliography==
* Hake, Sabine. German National Cinema. Routledge, 2008.

==External links==
* 

 
 
 
 
 
 
 
 


 