L'udienza
 
{{Infobox film
| name           = Ludienza
| image          = Ludienza.jpg
| caption        = Film poster
| director       = Marco Ferreri
| producer       = Franco Cristaldi
| writer         = Marco Ferreri Rafael Azcona Dante Matelli
| starring       = Enzo Jannacci
| music          =Teo Usuelli
| cinematography = Mario Vulpiani
| editing        = Giuliana Trippa
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Italy France
| language       = Italian French
| budget         =
}}

Ludienza is a 1972 Italian-French drama film directed by Marco Ferreri. It was entered into the 22nd Berlin International Film Festival.   

==Cast==
* Enzo Jannacci - Amedeo
* Claudia Cardinale - Aiche
* Ugo Tognazzi - Aureliano Diaz
* Michel Piccoli - Padre Amerin
* Vittorio Gassman - Principe Donati
* Alain Cuny - Padre gesuita
* Daniele Dublino - Il cardinale spagnolo
* Sigelfrido Rossi
* Irena Oberberg - Luisa (as Irene Oberberg)
* Maalerer Bergier - Cardinale tedesco (as Man Lerer Bergier)
* Dante Cleri
* Luigi Scavran
* Giuseppe Ravenna - Segretaria
* Mario Jannilli - Una guardia svizzera
* Enzo Mondino
* Attilio Pelegatti - (as Attilio Pellegatti)
* Bruno Bertocci - Cardinale Osta

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 