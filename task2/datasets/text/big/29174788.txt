Khichdi: The Movie
{{Infobox film
| name           = Khichdi: The Movie
| image          = Khichdi,_The_Movie.jpeg
| alt            =  
| caption        = 
| genre          = Comedy
| based on       = Khichdi (TV series)|Khichdi and  Instant Khichdi
| director       = Aatish Kapadia
| producer       = Jamnadas Majethia
| story          = Aatish Kapadia
| starring       = Supriya Pathak Anang Desai Rajeev Mehta Jamnadas Majethia Nimisha Vakharia
| music          = Raju Singh
| narrator       = Kesar Majethia Markand Soni
| cinematography = Sanjay Jadhav
| editing        = Hemal Kothari
| studio         = Hats Off Productions
| distributor    = Fox Star Studios Hats Off Productions
| released       = 1 October 2010
| runtime        = 120 Minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}

Khichdi: The Movie is a 2010 Bollywood comedy film, directed by Aatish Kapadia. It is the first film by Hats Off Productions and stars Anang Desai, Supriya Pathak, Rajeev Mehta, Nimisha Vakharia and Jamnadas Majethia in the lead roles. The film is based on the STAR India TV series Khichdi (TV series)|Khichdi and Instant Khichdi. It is the first film in the history of Hindi cinema to be based on a television series. 

==Plot==
 
The film starts with two kids Jacky (Markand Soni) and Chakki (Kesar Majethia), narrating the story of their hilariously madcap Parekh family to their fellow classmates on a camp. When Chandrakant Sheth (Arvind Vaidya), father of Himanshu (Jamnadas Majethia) and Hansa (Supriya Pathak) passes away, he manages to tell the Parekh family that his last wish is to have his son Himanshu married. Now, Himanshu has one ambition since childhood, that is to have a memorable, legendary love story. It is all together another matter that he is not in love with anyone and neither is he capable of getting any sensible girl to like him. But as Babuji (Anang Desai) often wisely says, "for every idiotic Praful (Rajeev Mehta), there is an even more idiotic Hansa made", his prophecy turns out to be true. Parminder (Kirti Kulhari), Himanshus neighbor, falls in love with Himanshu for no logical reason and the 2 get engaged to get married. That is when the story line takes a turn – Himanshu realizes on the eve of the wedding that the impending ceremony would mean impending doom to his desire to have a legendary love story, as nothing interesting has happened in his love story to make it memorable. It was a simple non happening take with a boring happy ending. No resistance, no conflict, no separation, no sad song, no climax, nothing. So, in order to make the Himanshu–Parminder love story a legendary one, Himanshu, along with Babuji, Praful, Hansa and Jayshree (Nimisha Vakharia), sets out to stall the wedding. And then begins their journey to create all those missing stages in Himanshus love story in order to make it legendary. However, in this hilariously hare brained process, they make a mess of everything and how at the incredibly ridiculous climax Himanshu eventually manages to win his love back, makes up the rest of the story of the film.

== Cast ==
*Anang Desai as Tulsidas Parekh a.k.a. Babuji
*Rajeev Mehta as Praful Tulsidas Parekh
*Supriya Pathak as Hansa Praful Parekh
*Jamnadas Majethia as Himanshu Chandrakant Sheth
*Nimisha Vakharia as Jayshree Parekh
*Kirti Kulhari as Parminder (Himanshus love interest)
*Gireesh Sahedev as Parminder
*Farah Khan as herself (Cameo appearance|cameo) – special appearance
*Arvind Vaidya as Chandrakant Sheth
*Dinyar Contractor as Judge
*Satish Shah as Ishwar (cameo)
*Deven Bhojani as hospital patient (cameo)
*Paresh Ganatra as Advocate Pandit (cameo)  
*Kesar Majethia as Chakki Praful Parekh (narrator)
*Markand Soni as Jacky Bharat Parekh (narrator)

Note: Vandana Pathak, who originally portrayed Jayshree in Khichdi (TV series)|Khichdi and Instant Khichdi, was approached to play the same role in the film as well and accepted. However, due to her prior commitments, she was unable to do so; hence, Nimisha Vakharia was offered the role, and she accepted.

==Box office==
The film performed well considering that it faced stiff competition from Anjaana Anjaani.  The film grossed Rs.6 crores nett and was declared below average. 

==Awards and nominations==
 6th Apsara Film & Television Producers Guild Awards

* Nominated: Apsara Award for Best Performance in a Comic Role — Jamnadas Majethia
 2011 Zee Cine Awards
* Nominated: Zee Cine Award for Best Performance in a Comic Role — Jamnadas Majethia 

;2011 Filmfare Awards
* Nominated  Best Actor In A Supporting Role – Female — Supriya Pathak

==See also== Khichdi (TV Series) Instant Khichdi (TV Series)

== References ==

 

== External links ==
*  
*  
*  

 

 
 
 
 
 