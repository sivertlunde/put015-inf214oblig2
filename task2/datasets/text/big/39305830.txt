Clash by Night (1963 film)
{{Infobox film
| name           = Clash by Night
| image          =
| caption        =
| director       = Montgomery Tully
| producer       = Maurice J. Wilson
| screenplay     = Montgomery Tully Maurice J. Wilson
| based on       =  
| starring       = Terence Longdon Jennifer Jayne Harry Fowler
| music          = John Veale
| cinematography = Geoffrey Faithfull
| editing        = Maurice Rootes
| distributor    =
| released       = 1963
| runtime        = 75 mins
| country        = United Kingdom
| language       = English
| budget         =
}}
Clash by Night (released in the US as Escape by Night) is a 1963 British crime thriller directed by Montgomery Tully and starring Terence Longdon. 

==Cast== 
* Terence Longdon as Martin Lord 
* Jennifer Jayne as Nita Lord 
* Harry Fowler as Doug Roberts 
* Alan Wheatley as Ronald Grey-Simmons 
* Peter Sallis as Victor Lush 
* John Arnatt as Inspector Croft 
* Hilda Fenemore as Mrs. Peel 
* Arthur Lovegrove as Ernie Peel 
* Vanda Godsell as Mrs. Grey-Simmons  Richard Carpenter as Danny Watts 
* Mark Dignam as Sydney Selwyn  Robert Brown as Mawsley 
* Stanley Meadows as George Brett  Tom Bowman as Bart Rennison  Ray Austen as The Intruder
* William Simons as one of the guards left outside the barn (uncredited)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 
 