Steppenwolf (film)
{{Multiple issues|
 
 
}}

{{Infobox Film
| name           = Steppenwolf
| image          = Steppenwolf_official_release.jpg
| image_size     = 
| caption        = 
| director       = Fred Haines
| producer       = 
| writer         = Fred Haines Novel by Hermann Hesse
| narrator       = 
| starring       = Max von Sydow Dominique Sanda Pierre Clementi
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1974 
| runtime        = 106 min. USA
| English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1974 film 1928 novel of the same name. The film made heavy use of visual special effects that were cutting-edge at the time of its release. It follows the adventures of a half-man, half-animal individual named Harry Haller, who in the Germany of the 1920s, is depressed, resentful of his middle class
station, and wants to die not knowing the world around him. He then meets two strange people who introduce him to life and a bizarre world called the Magic Theater.

==Film rights==
The film took seven years of complicated pre-production for its producers Melvin Abner Fishman and Richard Herland. Fishman, a student of Jung and alchemy, wanted the film to be "the first Jungian film"  .  and built up relationships with the Hesse family that allowed the film rights of the book to be released. Herland raised the finance.

==Director==
Directors Michelangelo Antonioni and John Frankenheimer, as well as the actor James Coburn were all touted to direct the film. In the end, the film was directed by its screenwriter, Fred Haines. 

==Casting==
Although Walter Matthau, Jack Lemmon and Timothy Leary were all proposed as playing the main role of Harry Haller, the role eventually went to Max von Sydow. In the other principal parts, Pierre Clementi played Pablo and Dominique Sanda took the role of Hermine. Although the film is in English, none of the principal actors were native English speakers.

==Reception==
Finally, the rights to the finished film were entirely given over to Peter Sprague, its financier. A "marketing disaster"  followed, this included the colour of the prints coming out incorrectly. The film has remained little seen.

==References==
 

==External links==
* 

 
 
 
 
 


 