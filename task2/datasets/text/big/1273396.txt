Surviving Christmas
{{Infobox film
| name           = Surviving Christmas
| image          = Surviving Christmas_poster.JPG
| image size     =
| caption        = Promotional poster Mike Mitchell
| producer       = Betty Thomas Jenno Topping Jeffrey Ventimilia Joshua Sternin
| story          = Deborah Kaplan Harry Elfont Josh Zuckerman Bill Macy Jennifer Morrison Udo Kier David Selby Stephanie Faracy Stephen Root
| music          = Randy Edelman
| cinematography = Peter Lyons Collister Tom Priestley Jr. Craig McKay
| studio         = LivePlanet Tall Trees Productions
| distributor    = DreamWorks Pictures
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} directed by Mike Mitchell and starring Ben Affleck, James Gandolfini, Christina Applegate and Catherine OHara.
 DreamWorks SKG released the film towards the end of October. This was due to it being advanced from December 2003 to avoid clashing with Afflecks other film, Paycheck (film)|Paycheck. Surviving Christmas received negative reviews and was a box office failure.  It was released on DVD on December 21, 2004, just two months after it had its theatrical release. 

==Plot==

Drew Latham (Ben Affleck) is a wealthy advertising executive. Just before Christmas, he surprises his girlfriend Missy (Jennifer Morrison) with first class tickets to Fiji. She is horrified that he would want to spend Christmas away from his family. Citing the fact that Drew has never even introduced her to his family, she concludes that he will never get serious about their relationship and dumps him. Drew has his assistant send her a Cartier bracelet to apologize. Desperate not to spend Christmas alone, Drew calls all of his contacts to find a place to stay on Christmas, but he is not close enough to anyone to be invited.

He tracks down Dr. Freeman (Stephen Root) at the airport, hoping to squeeze in a therapy session. The hurried doctor tells him to list all of his grievances and then burn them at his childhood home. The house is now occupied by the Valcos, who wonder what Drew is doing on their front lawn. When he sets his grievances on fire, Tom Valco (James Gandolfini) sneaks up behind him and knocks him out with a shovel. After he comes to, Drew explains what he was doing and asks for a tour of the house. Thrilled to see his old room, Drew impetuously offers Tom $250,000 to let him spend Christmas with the Valcos. Tom accepts, and Drews lawyer draws up a contract that requires the Valcos to pose as his family.

The next day, Drew forces the family to go out and buy a tree together, requiring Tom to wear a Santa cap in public. While they are trimming the tree, the eldest child Alicia (Christina Applegate) arrives for the holidays and is stunned by Drews presence. He suggests that she could portray the maid, since she was an unexpected addition to the scenario. At dinner, Drew writes a script for the family to read at the table. He hires a local actor to play the part of his grandfather, whom he calls Doo-dah (Bill Macy). 
 Josh Zuckerman) sledding the next day. After crashing at the bottom of a hill, he moves into kiss Alicia, who sneezes instead. Recovering back home from their growing colds, Alicia shares a childhood memory with Drew about an old tree that was coated in ice during a storm. Tom asks Drew to leave because he was planning on divorcing his wife Christine (Catherine OHara). Instead, Drew encourages the couple to indulge themselves. Tom buys a Chevelle SS, which he had when he was in high school, and Christine goes to a photographer for some glamor shots. 

One evening, Drew takes Alicia to the old tree of her childhood, which he has had covered in ice again. She is touched by the gesture, but Drew overdoes it, bringing in a full pageant production to surround the tree. Disgusted by his lack of restraint, Alicia demands that he leave. Meanwhile, Missy was won over by the bracelet, and when Drews assistant informed her that he was spending Christmas with his family, Missy visits the Valcos house with her parents. Drew promises the Valcos an extra $75,000 if they will play along for the evening, and they agree to pretend to be his family. 

The visit between the two families steadily descends into chaos, culminating with everyone seeing Christines glamor shots manipulated into pornography on Brians computer. Missys parents storm out, and Drew informs her that their relationship is over. Alicia finally draws out of Drew the truth about his family. His father left them when he was just four, and every Christmas, his mother would work a double shift at the diner to make extra money. He would spend Christmas Day alone and visit his mom at the end of the night, and she would give him an adult stack of pancakes. He repeated the ritual every year until he was 18, and he has never been in a diner since. His mother died when he was in college.

Drew returns to his apartment to spend Christmas alone. Tom visits him to collect his money, and the two decide to go watch the actor who played Doo-Dah perform in the local production of A Christmas Carol. At the play, Tom and Christine decide not to divorce. Drew and Alicia make up outside the theater, and the film closes with everyone eating in a diner together.

==Cast==
*Ben Affleck &ndash; Drew Latham
*James Gandolfini &ndash; Tom Valco
*Christina Applegate &ndash; Alicia Valco
*Catherine OHara &ndash; Christine Valco Josh Zuckerman &ndash; Brian Valco
*Bill Macy &ndash; Doo-Dah/Saul
*Jennifer Morrison &ndash; Missy Vanglider
*Udo Kier &ndash; Heinrich
*David Selby &ndash; Horace Vanglider
*Stephanie Faracy &ndash; Letitia Vanglider
*Stephen Root &ndash; Dr. Freeman
*Sy Richardson &ndash; Doo-Dah Understudy
*Tangie Ambrose &ndash; Kathryn
*John B.J. Bryant &ndash; Cabbie
*Peter Jason &ndash; Suit
*Ray Buffer &ndash; Arnie
*Phill Lewis &ndash; Levine the Lawyer

== Reception == Razzie Awards, Jersey Girl) and Worst Screenplay.

Writing in Entertainment Weekly, Lisa Schwarzbaum panned, "Really, critics and audiences ought to turn thoughts and wallets discreetly away from Surviving Christmas, ignoring the sight as if Santa had just stepped in droppings from Donner and Blitzen."  In The New York Times, Stephen Holden concluded, "This is a film that perversely refuses to trust its own comic instincts. Perhaps out of a fear of not having enough jokes, it throws in extra subplots and unnecessary characters to keep the pace frantic and the action muddled." 

==See also==
*2004 in film

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 