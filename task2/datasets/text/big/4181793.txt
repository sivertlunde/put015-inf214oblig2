Der bewegte Mann
{{Infobox film
| name           = Der bewegte Mann
| image          = 
| image_size     = 
| caption        = 
| director       = Sönke Wortmann
| producer       = Bernd Eichinger
| writer         = Sönke Wortmann Ralf König (comic)
| narrator       = 
| starring       = Til Schweiger Katja Riemann Joachim Król
| music          = Torsten Breuer
| cinematography = Gernot Roll
| editing        = Ueli Christen
| distributor    = Neue Constantin Film (Germany) Orion Classics (USA)
| released       = 1994
| runtime        = 93 min
| country        = Germany German
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 German film comedy directed by Sönke Wortmann and starring Til Schweiger, Rufus Beck, Joachim Król and Katja Riemann. 

The film developed from the comics "Der bewegte Mann" and "Pretty Baby" by Ralf König. 

==Plot==

Axel (played by Til Schweiger) has just been dumped by his girlfriend Doro (Katja Riemann), and needs to find a new place to live. He meets Walter (Rufus Beck), a transvestite who teaches straight men how gay men think. Walter talks Axel into joining him and some friends at a gay party afterwards, and tries to convince Axel to move in with him. At the party, Axel decides instead to move in with Walters best friend, Norbert (Joachim Król), whose boyfriend has just left him. Later, at Axel and Doros apartment, Norbert tries to seduce Axel while they browse old photos.  Just when Norbert has shed all his clothes, Doro shows up at the door, and Axel hastily hides Norbert.  Doro explains to Axel that shes expecting his child and wants to give their relationship a second chance.  She is not amused to discover a naked man in the wardrobe, but Axel manages to convince her that nothing has happened.  Excited about fatherhood and eager to return to Doro, Axel forgets about his new friendship with Norbert.

But soon Axel discovers a downside to the pregnancy: he no longer finds Doro sexy.  Despite his engagement to her, he decides to stray when he encounters Elke, a former girlfriend. They are trying to find a place to have sex when Axel bumps into Norbert again. At first Norbert is angry with him for having left without a word, but Axel claims its only because Doro was upset. Axel convinces Norbert to lend his apartment for the tryst with Elke. A few days later at Norberts apartment, Elke gives Axel a mind-altering drug, and leaves him sitting naked on a table. Meanwhile Doro has learned that Axel went to Norberts apartment, and she thinks that Axel is going to have sex with Norbert. She confronts Norbert at his apartment and upon entering, she sees Axel naked and unable to speak and starts to go into labor. In the bathroom Norbert finds Elke and Norberts not-so-gay boyfriend playing in the tub.  As Norbert drives her to the hospital, he attempts to explain everything on the way.  Doro forgives Norbert and they become friends, but her relationship with Axel is in question.

== Cast ==
* Til Schweiger - Axel Feldheim
* Katja Riemann - Doro Feldheim
* Joachim Król - Norbert Brommer
* Rufus Beck - Walter alias Waltraut
* Armin Rohde - Metzger
* Martina Gedeck - Jutta
* Kai Wiesinger - Gunnar
* Monty Arnold - Monty
* Martin Armknecht - Lutz Mike Reichenbach - Rambo-Fan

==References==
 

==See also==
*List of German language films
*List of lesbian, gay, bisexual or transgender-related films

==External links==
*  at the Internet Movie Database

 
 
 
 
 
 
 
 
 
 
 
 