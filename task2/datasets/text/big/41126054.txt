Bhoothnath Returns
 
 
{{Infobox film
| name = Bhoothnath Returns
| image = Bhoothnath returns.jpg
| caption = Theatrical release poster
| director = Nitesh Tiwari
| producer = Bhushan Kumar Krishan Kumar Renu Ravi Chopra
| writer = Nitesh Tiwari Piyush Gupta Vivek Sharma Sanjay Mishra Brijendra Kala Usha Nadkarni
| music =  Songs:  
| distributor = B R Films
| duration = 155 minutes 
| released =  
| country = India Hindi
| budget =   
| gross =   
}} supernatural comedy film directed by Nitesh Tiwari and produced by Bhushan Kumar. A sequel to 2008 film Bhoothnath, the film revolves around Bhoothnath (Amitabh Bachchan) who is mocked in a Bhoothworld for his inability to scare children before being sent back to Earth to redeem himself. The film was released on 11 April 2014, and declared as one of the highest grossers according to Box Office India.    

Election Commission of India demanded tax-free status for Bhoothnath Returns, stating, "The state governments should support the strong social message that emanates from the film. Granting tax-free status to this film would make people aware about their rights as voters like making voter ID cards or not treating polling day as holiday".  The Uttar Pradesh Government has declared tax-free status to film on 30 April 2014.

==Plot==
The plot is linked with the previous film Bhoothnath. When Bhoothnath, although his real name in the two films is Kailash Nath, (Amitabh Bachchan) enters the Bhoothworld, (a ghosts world) he is mocked and questioned about his abilities as a ghost as he was unsuccessful to scare any child. To redeem himself, he returns to Earth where he tries his best to scare children but is unsuccessful with his attempts. However, a boy named Akhrot (Parth Bhalerao) can see him in his true form. Akhrot is not scared of Bhoothnath, but helps him to scare a few children. In return, he asks Bhoothnath to act scared of him and to run away from a haunted house while he chants mantras in front of his friends, so that his friends can accept him in their cricket team. Bhoothnath decides to help him further by helping him earn money by giving ghosts living in under-construction high rises peace by fulfilling the wishes they were still staying back on Earth as ghosts for. In the process, they come to know why the ghosts died, and help their families get insurance money by scaring corrupt insurance officers. In the process, he gets to know about Bhau (Boman Irani), a former criminal who is now a corrupt politician. After having seen the amount of corruption in India and encouraged by Akhrot, Bhoothnath decides to contest the upcoming elections. Soon, the rivalry between Bhoothnath and Bhau heats up, to the extent that Bhoothnath sacrifices his powers to win. One day before the elections, a Parivartan rally is to be held by Bhoothnath to encourage a high turnout on election day. Bhaus men threaten to kill Akhrot unless he prevents the rally from happening, but Akhrot refuses and is ready to face the consequences. On the day of the rally, Bhaus men injure Akhrot grievously for his non-compliance, which makes Bhoothnath leave his rally to be with Akhrot in the hospital. He goes to the Bhoothworld and begs for Akhrots life. They tell him that if Bhoothnath wins the election, they will spare Akhrots life. Meanwhile, Bhau uses Bhoothnaths absence at the rally to his advantage and tries to influence the audience to vote for him. However, on election day, the majority of the public voted for Bhoothnath. The next day, Bhootnath sees Akhrots heartbeat rising and realises that he won the election. The film ends with Akhrot regaining consciousness and everybody celebrating the success of Bhoothnath.

==Cast==
*Amitabh Bachchan as Khailash Nath (Bhoothnath)
*Boman Irani as Bhausaheb
*Parth Bhalerao as Akhrot
*Usha Jadhav as Bindia Pathak (Akhrots Mother) Sanjay Mishra as Mishti Baihud
*Brijendra Kala as Ridhima Koyla and Joy Dens (double role)
*Usha Nadkarni as Lolita Singh
*Kurush Deboo as Psychiatrist
*Shahrukh Khan as Aditya Sharma (cameo)
*Anurag Kashyap as Himself (cameo)
*Ranbir Kapoor as Himself (cameo) 

==Soundtrack==
The music of film was scored by Ram Sampath and Meet Bros Anjjan, along with Palash Muchhal. Yo Yo Honey Singh also composed one promotional song for the film which remarked as its first collaboration with Amitabh Bachchan. 

{| class="wikitable"
|-
! Song !! Singer !! Composer !! Lyrics !! length
|-
| Party To Banti Hai || Meet Bros Anjjan and Mika Singh || Palash Muchhal and Meet Bros Anjjan || Kumaar || 4:19
|-
| Har Har Gange || Aman Trikha || Ram Sampath || Kunwar Juneja || 3:13
|-
| Party with the Bhoothnath || yo yo Honey Singh || Yo Yo Honey Singh || Yo Yo Honey Singh ||5:21
|-
| Sahib || Rituraj Mohanty || Ram Sampath || Munna Dhiman || 4:21
|-
| Dharavi Rap || Parth S. Bhalerao & Anish || Ram Sampath || Nitesh Tiwari || 2:32
|-
|- Lonely Thing || Yo Yo Honey Singh and Mika Singh  || Yo Yo Honey Singh and Mika Singh || Kumaar ||3:39
|-
| Har Har Gange (Remix) || Aman Trikha || Ram Sampath remixed by DJ Kiran || Kunwar Juneja ||3:38
|} 

==Critical reception==
Taran Adarsh from Bollywood Hungama gave the movie 3.5 stars and said that "Bhoothnath Returns is made with noble intentions and the message it conveys resonates in the second hour".  Suprateek Chatterejee from First Post called it "a must watch for kids" and found that its "shamelessly manipulative moments are tempered with deliberate humour".  A Filmfare reviewer found that the "strength of this film is in its writing" and praised Amitabh Bachchan for his performance whilst giving the movie 3 stars.  Neha Gupta of Bollywood3 stated that "Bhootnath Returns has some extremely light-hearted moments which will tickle your funny bone and some excellent performances from the actors involved".  Rohit Khilnani of India Today, gave the movie 3.5/5 stars and said, "There is no doubt that Bhoothnath will entertain cine-goers. Its entertainment, Bachchan style!" 

== Box office ==
Bhoothnath returns collected   domestic and    overseas(as of 20 April 2014). Satellite rights of the film is sold to Sony for   ; made it a good profitable venture for producers. http://www.bollywoodhungama.com/movies/features/type/view/id/6460 

The film that released in over 1,300 theatres saw a "grand" opening of 93%%  collecting  —  on its first day with stiff competition from the film Disco Singh.  With a growth of 60% over its first day collection, it collected   on its second day.  After the completion of the films first weekend, it managed a "fair" collection of  .  It collected around    and  —   on its first Monday and Tuesday respectively, totalling   over the first five days of its release.    After a good growth on Monday, it saw a slide in its collections over the next few days to end up collecting   in its first week. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 