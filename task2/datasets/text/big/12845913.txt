The Last Frontier (serial)
 
 
{{Infobox film
| name           = The Last Frontier
| image_size     =
| image	         = The Last Frontier FilmPoster.jpeg
| caption        =
| director       = Spencer Gordon Bennet Thomas Storey
| producer       = Fred McConnell
| writer         = Karl R. Coolidge Robert F. Hill George H. Plympton Arthur Rohlsfel Courtney Ryley Cooper (novel)
| narrator       = William Desmond Joe Bonomo Pete Morrison LeRoy Mason
| music          = Edward Snyder Gilbert Warrenton
| editing        = Tom Malloy
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 213 minutes (12 chapters)
| country        = United States English
| budget         =
| gross          =
}}
The Last Frontier is a 12-chapter serial, distributed by RKO Radio Pictures in 1932. The serial starred Lon Chaney, Jr. as the Zorro-esque hero The Black Ghost. Dorothy Gulliver was the leading female star. The total running time of the serial is 213 minutes. 

==Plot==
The outlaw "Tiger" Morris attempts to drive setters off their land in order to acquire the local gold deposits.  A crusading newspaper editor, Tom Kirby, becomes the masked vigilante, The Black Ghost, to stop him.

==Cast==
* Lon Chaney, Jr. as Tom Kirby, the editor of the local newspaper and the masked vigilante The Black Ghost
* Dorothy Gulliver as Betty Halliday
* Ralph Bushman as Jeff Maitland William Desmond General George Custer Joe Bonomo as Joe, one of Morris henchman.  Listed as "Kit Gordon" in the credits.
* Pete Morrison as Hank, one of Morris henchman
* LeRoy Mason as Buck, Morris spearpoint heavy (chief henchman)
* Yakima Canutt as Wild Bill Hickok
* Mary Jo Desmond as Aggie Kirby
* Slim Cole as Uncle Happy
* Richard Neill as Leige "Tiger" Morris, outlaw
* Judith Barrie as Rose Maitland
* Claude Payton as Colonel Halliday
* Ben Corbett as Bad Ben, one of Morris henchman
* Frank Lackteen as Chief Pawnee Blood
* Fritzi Fern as Mariah

==Production==
The Last Frontier was RKOs only serial. {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut  
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury 
 | origyear = 1973
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | pages = 360
 | chapter = 14. The Villains "All Bad, All Mad"
 }} 

The Black Ghost (Lon Chaney Jr) is a Zorro-style character. 

==Chapter titles==
# The Black Ghost Rides
# The Thundering Herd
# The Black Ghost Strikes
# The Fatal Shot
# Clutching Sands
# The Terror Trail
# Doomed
# Facing Death
# Thundering Doom
# The Life Line
# Driving Danger
# The Black Ghosts Last Ride
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 206–207
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 