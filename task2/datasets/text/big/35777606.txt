Maryan (film)
 
 
 
{{Infobox film
| name           = Maryan
| image          = Poster of Mariyaan.jpg
| caption        = Film Poster
| director       = Bharat Bala
| producer       = Viswanathan Ravichandran
| writer         = Bharat Bala  Joe DCruz
| screenplay     = Bharat Bala Sriram Rajan
| starring       = Dhanush Parvathi Menon
| music          = A. R. Rahman
| cinematography = Marc Koninckx
| editing        = Vivek Harshan Aascar Film Pvt. Ltd Aascar Film Pvt. Ltd
| released       =   
| country        = India
| language       = Tamil
| budget         = 
| Box office     = 100 crores
| runtime        = 153 minutes
| gross          =  
}}
 Tamil action thriller film directed by Bharat Bala starring Dhanush alongside Parvathi (actress)|Parvathi.  Produced by Venu Ravichandran, the film has music and background score composed by A. R. Rahman  and cinematography by Marc Koninckx.  Dialogues in the film are penned by Joe D Cruz|R. N. Joe D Cruz. The film revolves around a story of human survival adapted from a newspaper article of a real-life crisis event, when three oil workers from Tamil Nadu were kidnapped and taken hostage in Sudan by mercenaries.    The film is an emotional journey of a common man to an unknown place with the hope to come home and lead a better life.    The film released in Auro 3D sound format  on selective screens in India and worldwide on 19 July 2013.    The film received positive critical reception in India.    At the Box Office of USA, the film had the biggest opening amongst all Indian films releasing on the same date.    However, in Tamil Nadu the film was declared as an average grosser. 

==Plot==
Maryan Joseph (played by Dhanush) is a fisherman in a village named Neerody. He has an auspicious bond with sea and proudly claims himself as "Kadal Raasa" (King of Ocean). There is Panimalar (played by Parvathi Menon|Parvathi) who falls in love with Maryan and doesn’t shy away in confessing it to him. Maryan is loved and longed by Panimalar but sadly her feelings are not reciprocated. The more Maryan tries to keep Panimalar away from him, the closer she tries to get. This eventually leads to Maryan falling for Panimalar. Once,
Panimalar is caught in unfortunate circumstances and to support her financially, Maryan is forced to take up employment on contract basis for two years in Sudan. He then befriends Saami (Jagan) and is his only companion for two years.
He successfully completes his tenure and packs bags in jubilation to return to his ladylove, but tragedy strikes in the form of Sudanese terrorists, who end up kidnapping Maryan and two of his co-workers, demanding money for their freedom. One of his co-worker gets killed by the head terrorist (Christophe Minie.) After 21 days in captivity, Maryan escapes with Saami and runs for his life. He gets separated from Saami, who gets killed later on, and gets lost in the desert and suffers from dehydration and confronts cheetahs as his mirage .After finding the coast he fights the terrorist who catches up with him and escapes. He then returns to his village where his love is waiting for him.

==Cast==
* Dhanush as Maryan Vijayan Joseph Parvathy as Panimalar
* Appukutty as Sakkarai
* Uma Riyaz Khan as Seeli
* Salim Kumar as Thomayya
* Vinayakan as Theekkurissi Jagan as Sami
* Imman Annachi as Kuttyandi
* Ankur Vikal as Bappan
* Hari Krishnan
* Christopher Minnie
* Dagbeh Tweh
* Barry Mydou

==Production==

===Development===
  Tamil it translates to "A man who never dies". As per the film script, it implies that the spirit of the man never dies. 
 LURD Military child soldiers and subsequently chose them as well as the cinematographer of that film, Marc Koninckx, to be a part of Maryan. 

A press release from March 2013 along with a poster revealed the films outline as "A young man is faced with adversity. Life deals him immense challenges, and forces him to struggle to thrive. But he does, nevertheless, fighting to survive by thriving on the sheer undying spirit of the human will to survive. Along his thrilling journey is a tryst with adventure, a smattering of drama and a gritty tale that shows the power of love in extreme circumstances. And that is the story of Maryan."  In an interview with Zee News, Bharatbala quoted, "Even though it (Maryan) is about the fight for survival, it deals with several other emotions such as love, separation and struggle. The story recounts a beautiful journey about the separation of the protagonist from his loved ones and his struggle to reunite". 

===Filming===
As the films story takes place in Sudan and in Kanyakumari, India,  Bharatbala noted that filming had to take place in two contrasting terrains. The sandy rugged locations in Africa were shot in Liberia, made to look like Sudan, and several other portions were shot in a month. Subsequent close-up scenes featuring supporting cast members from Africa were filmed in India. The first schedule of the film began in Namibia, with scenes including a fight sequence supervised by Dilip Subarayan, being canned.  The song "Nenje Ezhu" was shot in Namibia#Coastal Desert|Namibias Coastal Deserts featuring the leading duo.    Dhanush also shared screen space with a cheetah. 

The final schedule of the film started in   with help from the   team.    Certain arid land scenes were also shot at Rann of Kutch in Gujarat.
 Auro format was done at composer A. R. Rahmans AM Studios making it the first Tamil film to mix native sounds in the format.    Bharatbala claimed, one would find three different contrasting uses of the Auro 3D sound in the film. First, in the fishing village and the feel of the ocean surface, then deep into the desert and the sonic sounds and textures of the ocean, 50–55 feet under the water.  He revealed through Twitter that the films digital intermediate works were completed by 9 July 2013. Scenes during the making of the movie were placed with the end credits of the film. 

==Soundtrack==
  mastered for iTunes version.  Radio Mirchi South Top 20 charts, whereas "Sonapareeya" peaked at #3 and "Innum Konjam Neram" at #18.  

The soundtrack album was entitled as "Tamil Album of Year" in iTunes’ Best of 2013. {{cite web|url=http://timesofindia.indiatimes.com/entertainment/tamil/movies/news-interviews/What-connects-Rahman-and-Dhanush-to-Apple/articleshow/27776712.cms|title=
What connects Rahman and Dhanush to Apple?|publisher=The Times of India|accessdate=23 December 2013}} 

==Controversies==
The third teaser as well as a promotional film poster depicting a smoking Dhanush   were criticized by former Union Minister of Health and Family Welfare of India, Anbumani Ramadoss. He claimed that the scenes prioritize smoking in the movie and hence it is a punishable offense to appear smoking in cinemas. Following this, reports claimed that the actor was in discussions with the director for the removal of such contents from the film. 

==Marketing==
The first official poster of the film was released on 28 March 2013.    A thirty-second teaser featuring only Dhanush only released on 29 March 2013.    On 31 March 2013, another 40-second teaser with a musical score was released that showed Dhanush with a spear in his hand, diving into deep sea in a breath and hunting on the seabed.  The third teaser of 30 seconds was released on 4 April 2013. It depicted the actor fighting and smoking.    The teaser of the song "Nenje Ezhu" was released on 26 April 2013. It crossed over three lakh views on YouTube in less than two days.  A. R. Rahman extended an invitation through YouTube for the premiere of the song "Nenjae Ezhu" on 3 May 2013. 

The official trailer was released on 1 May 2013.  After the trailer garnered nearly seven lakh views in one day on Sony Indias music channel, it was re-released as an upgraded Vevo version.  On 19 June 2013 a press meet headed by the director claimed that the film utilised the Auro 3D sound technology. 
 Jaya TV studios that was aired live on 23 June 2013. 

==Release==
The satellite rights of the film were secured by Jaya TV. Initially set to release on 31 May 2013,  due to post-production and re-recording works, the release date was changed to 21 June 2013. There was a scarcity of scrutiny theaters for the certification process so, the film was censored on 17 June 2013.  The film was given a "U/A" certificate by the Indian Censor Board for its theme and some violence in the climax. However, it was re-censored to exempt itself from entertainment tax and hence, was given a "U" certificate.  The release date of 21 June 2013 was cancelled to avoid clash with Dhanushs Hindi debut film Raanjhanaa and delayed process for censoring.  The film was eventually released on 19 July 2013. 

The estimate number of screens of the film in Tamil Nadu are 350. As of July 2013, 27 screens out of these were equipped with the Auro 3D surround sound. The release in remaining India would be 100.  Maryan wasnt released in Andhra Pradesh whereas out of 50 screens all over Karnataka, the film released on 39 screens in Bangalore, 3 in Mysore, 2 in Mangalore and rest in other areas of the state.  In Kerala, distribution rights were acquired by Sagara Entertainment, releasing the film in about 79 screens.   

Bharat Creations secured the distribution rights of the film in the United States and North America.    The screens in USA were 25 whereas 6 in Canada. The film released in UK in cities, precisely Feltham, Ilford, Staples , Crawley, & Wandsworth. 

A special screening of the film was held at Mumbai on 11 August 2013. 

===Critical reception===
The film received generally positive reviews upon release. The films technicalities, performances of the lead actors and the score were applauded by the critics. 
 One India Entertainment summarized, "Dhanushs sufferings on-screen, bursting and agony during emotional scenes have come out well. The movie is a must watch for Dhanushs fans." He gave the film 3.5 stars out of 5.  S. Sarawasthi of Rediff gave the film 3 stars out of 5 and claimed, "Mariyaan is a beautiful love story. However, what brings the film down a notch is the pace, especially the second half, which seems to drag quite a bit. And though the film may not appeal to all, it is definitely a must watch."  Critical review board at Behindwoods gave the verdict, "A movie with breathtaking visuals, heart-stopping music, terrifically talented cast set on a very leisurely paced narrative." They rated 3 on a scale of 5  Baradwaj Rangan of The Hindu noted, "A wonderful romance, but needed to be more."  Vivek Ramz of in.com summarized, "Mariyaan is a realistic and touching love saga and definitely deserves a watch! Better watch it in Auro 3D screen to experience it best." He rated the film 3.5 stars out of 5. 

==Box office==
;International
At the Box Office of USA,the film had the biggest opening amongst all Indian films releasing on the same date.  In its opening weekend, at the UK and Ireland Box Office the film has collected £13,675 on 5 screens.  The film earned approximately   in the second weekend. The total collections, one month estimate was  .At the Australian box office, the film raked in approximately   at the end of the second weekend. 

;Domestic
The film netted approximately   in first weekend from nearly 35 screens and overall 531 shows in Chennai and suburbs. 85% average occupancy was recorded for these days.   After 10 days, Maryan grossed around   in Chennai City and its vicinity.  In Coimbatore, Neelagiri, Erode and Tirupur, it crossed the   mark by 10 days. By end July 2013, a total of 55 screens were screening the movie. In Kanchipuram two theaters alone, the film grossed  .  Overall, the weekdays in the first week had 80% theatre occupany.

In the second week number of weekend shows in Chennai were 306 and average theatre occupancy was 65%. The collections for the weekend grossed  .During the subsequent weekdays, number of shows in Chennai were 600 with theatre occupancy 60% and the film grossed  . The second weekend experienced a drop compared to the first, but prime multiplexes reported near full houses. All over Karnataka, especially in Bangalore City the film opened with a mixed response and second weekend’s occupancy was less than 40% at the multiplexes.    

In the third week number of weekend shows in Chennai were 117 and average theatre occupancy was degraded to 55%. The collections for the weekend grossed  . For the following weekdays, number of shows in Chennai were 372 with theatre occupancy 40% and the film grossed  . 

After a month of the film release, weekend shows in the same city were 39 and average theatre occupancy further dipped to 40%. The collections for the weekend grossed  . For the subsequent weekdays, number of shows in Chennai were 132 with theatre occupancy 30% and the film grossed  . 

==Accolades==
{| class="wikitable" style="width:99%;"   
|+ Positive awards and nominations
|-
! width="23%" scope="col" | Distributor
! width="15%" scope="col" | Date announced
! width="25%" scope="col" | Category
! width="23%" scope="col" | Recipient
! width="8%" scope="col" | Result
! width="2%" scope="col" | Reference
|-
| rowspan="5" |  Edison Awards 
| rowspan="5" |  January 2014 
|  Best Actor 
|  Dhanush 
| rowspan="5"  
| align="center" rowspan="5" | 
|-
| Best Music Director 
|  A. R. Rahman 
|-
| Best Playback Singer - Male 
|  Yuvan Shankar Raja    
|-
| Best DOP (cinematography) 
|  Marc Koninckx 
|-
| Best Choreographer 
|  Bharathi Raghuraam 
|- Filmfare Awards 
| rowspan="4" |  12 July 2014 
|  Best Actor (Critics choice) 
|  Dhanush 
|  
| align="center" rowspan="4" | 
|-
| Best Music Director 
|  A. R. Rahman 
|rowspan="3"  
|-
| Best Playback Singer - Male 
|  Yuvan Shankar Raja    
|-
| Best Actress (female) 
|  Parvathy 
|}

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 