The Third Generation (1979 film)
 
{{Infobox film
| name           = The Third Generation
| image          = Die Dritte Generation, film poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Rainer Werner Fassbinder
| producer       = Rainer Werner Fassbinder
| writer         = Rainer Werner Fassbinder
| narrator       = 
| starring       = Hanna Schygulla Margit Carstensen Eddie Constantine Bulle Ogier Harry Baer
| music          = Peer Raben
| cinematography = Rainer Werner Fassbinder
| editing        = Juliane Lorenz
| distributor    = New Yorker Films (USA)
| released       = 13 May 1979
| runtime        = 105 mins.
| country        = West Germany
| language       = German
| budget         = DEM 800,000 (estimated)
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 1979 West German film, a black comedy about terrorism, written, directed and cinematographed by Rainer Werner Fassbinder. The plot follows an ineffectual cell of underground terrorists who plan to kidnap an industrialist.

==Plot==
P.J. Lurz, an industrialist with an office in a Berlin high-rise, informs his American headquarters that the company has difficulty selling its security-related computer systems to the West German government in  , The World as Will and Representation. With these words, Susanne sets an ambiguous covert plot into motion, alerting the members of the terrorist cell of an upcoming meeting. They are: August Brem, the ringleader; Susannes composer husband Edgar; feminist history professor Hilde Krieger; Petra Vielhabor, a housewife who is constantly arguing with her banker husband Hans; and Rudolf Mann, a clerk in a record store.
 
P.J. Lurz is informed by Gerhard Gast, the inspector-general of the police, that he is being watched and is under police protection. Gast has also arrived to pick up Susanne, his daughter-in-law. En route to their home, Susanne and Gerhard stop at a hotel room and have sex. They have been carrying on an affair with sado-masochistic undertones.
The Gast family has dinner together: Gerhardt, Susan, her husband Edgar, the caustic grandfather, the delusional pianist grandmother and the young couple’s small son. During dinner, Grandpa Gast tells Edgar that every generation needs a war.

The terrorists gather at Rudolf’s large apartment, but August is annoyed by the presence of Rudolf’s roommate Ilse Hoffman, a drug addict. August sees her as a threat to their secret activities. Bored and with not much to do, the group spend their time playing Monopoly (game)|Monopoly. They eagerly await the arrival of a new contact. His name is Paul; he arrives from training camps in Africa where he has gained experience. Paul is assigned to live with Hilde. He rapes her, however by the following day they have become a couple.
 Bakunin makes him the object of jokes. Franz fails to find a job but reconnects with his drug addict girlfriend Ilse.

Times are tense and get even worse when Paul is gunned down by the authorities at a restaurant. Edgar witnesses his death and sees his father, Officer Gast, at the scene. Paul’s death scares the members of his gang. In order to finance their activities, Petra and some of the other terrorists rob the very bank in which Petra’s husband works. While they are escaping, Petra shoots and kills her husband. They frantically change their looks and names and flee from their homes. August gives out paper squares to the group. Some have a mark and some don’t. Petra, Rudolf, and Hilde get the marks and have to break into an office at night in order to steal the new identities. Rudolf is so scared that he pees in his pants and the others laugh at him. The joke is short-lived because Franz finds Ilse dead of a drug overdose.

Bernhard is interrogated by Officer Gast as to their whereabouts. Bernhard genuinely does not know but gets curious and follows August undetected. He sees Lurtz give money to August in order to finance the terrorist activities. After Pauls death, the terrorists believe that there is a traitor among them. August makes the others think that it was Franz. August sets up Franz by telling him where Ilse is buried. He then calls the authorities and gets him killed. August also does the same to Petra when she is instructed to place a bomb and gets intercepted and killed by the police. Bernhard is caught by Officer Gast at the cemetery when he tries to warn Franz that it is a set-up and tell him not to go to Ilse’s grave. Bernhard tells Officer Gast what he saw at the Japanese restaurant and after an argument Bernhard falls down a long flight of stairs and is killed. Taking advantage of the carnival season, the remaining terrorists wearing costumes kidnap PJ Lurtz. He is videotaped in a basement. He still believes that all is part of his secret plan and smiles to the camera.

==Cast==
 
 
* Eddie Constantine as P. J. Lurz
* Hanna Schygulla as Susanne Gast
* Margit Carstensen as Petra Vielhaber
* Bulle Ogier as Hilde Krieger
* Volker Spengler as August
* Raúl Gimenez as Paul
* Hark Bohm as Gerhard Gast
* Günther Kaufmann as Franz Walsch
 
* Vitus Zeplichal as Bernhard von Stein
* Udo Kier as Edgar Gast
* Harry Baer as Rudolf Mann
* Y Sa Lo as Ilse Hoffmann
* Jürgen Draeger as Hans Vielhaber
* Claus Holm as Father Gast
* Lilo Pempeit as Mother Gast
 

==Production==
The Third Generation was made right after Fassbinder achieved wide international critical and commercial success with The Marriage of Maria Braun. Watson, Rainer Werner Fassbinder, p. 168  It was produced by Fassbinders production company Tango Films for an estimated amount of 800.00 DEM. Watson, Rainer Werner Fassbinder, p. 163  It was shot in Berlin from November 1978 to January 22, 1979 in the winter of 1978 -1979, which is when the action takes place. 

The large cast was formed by actors from Fassbinders regular troupe: Hanna Schygulla, Margit Carstensen, Volker Spengler, Harry Baer and Günther Kaufmann among others.  It also includes two international stars Eddie Constantine, who had worked with Fassbinder earlier in the directors career and Bulle Ogier, who did not speak German. Her dialogue was translated to her native French by Juliane Lorenz, who worked as assistant director and editor having also a cameo role as a job counselor.  Some of the actors also worked behind the scenes: Harry Baer was executive producer, Raúl Gimenez was production designer, Volker Spengler was the art director.

==Reception==
The Third Generation premiered on 13 May 1979 at the Cannes Film Festival. It competed in the Un Certain Regard section .     American and French critics praised the film as the festivals most exciting. The French daily Le Figaro called it: "An effective, cinematic exercise in style and one of the most frightening political films".

The film was released in West Germany in September 1979. It was received without enthusiasm. Some critics praised the films mordant political humor, but  mostly it received negative reviews. A critic called it as crazy as teaming Jerry Lewis with Robert Bresson. The political theme of the film aroused controversy. At a screening in Hamburg, the projectionist was beaten unconscious, while in Frankfurt an incensed mob threw acid at the screen. There were also death threats. However, The Third Generation is now considered by film critics to be one of Fassbinders best films. At the Rotten Tomatoes website it has an 83% fresh rating.

==References==
 

==Bibliography==
* Thomsen, Christian Braad.   Fassbinder: Life and Work of a Provocative Genius . University of Minnesota Press, 2004, ISBN 0-8166-4364-4
*Watson, Wallace Steadman.  Rainer Werner Fassbinder: Film as Private and Public Art. University of South Carolina Press, 1996, ISBN 1-57003-079-0

==External links==
* 
*  
* 

 

 
 
 
 
 
 
 
 