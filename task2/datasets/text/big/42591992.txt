Mother Earth (film)
{{Infobox film
| name = Mother Earth 
| image =
| image_size =
| caption =
| director = Alessandro Blasetti
| producer = 
| writer = Goffredo Alessandrini   Camillo Apolloni   Gian Bistolfi   Alessandro Blasetti
| narrator =
| starring = Leda Gloria   Sandro Salvini   Isa Pola   Olga Capri 
| music = Francesco Balilla Pratella   Pietro Sassoli 
| cinematography = Giulio De Luca   Carlo Montuori
| editing = Carlo José Bassoli   Alessandro Blasetti 
| studio = Società Italiana Cines
| distributor =Societa Anonima Stefano Pittaluga
| released = April 1931 
| runtime = 90 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Fascist Italys urban values.

==Cast==
* Leda Gloria as Emilia 
* Sandro Salvini as Il duca Marco 
* Isa Pola as Daisy 
* Olga Capri as La governante della tenuta 
* Vasco Creti Nunzio, Il massaro, padre di Emilia 
* Carlo Ninchi as Il commandatore Bordani 
* Franco Coop as Silvano, un contadino
* Raimondo Van Riel as Un contadino nella taverna 
* Giorgio Bianchi as Il cortegiatore di Daisy 
* Ugo Gracci as Laiutante del massaro
* Umberto Sacripante as Il contadino idiota

== References ==
 

== Bibliography ==
* Reich, Jacqueline & Garofalo, Piero. Re-viewing Fascism: Italian Cinema, 1922-1943. Indiana University Press, 2002.
 
== External links ==
*  

 

 
 
 
 
 
 


 