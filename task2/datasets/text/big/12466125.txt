Spider-Man (1977 film)
{{Infobox television film
| name           = Spider-Man
| image          = Poster of The Amazing Spider-Man.jpg
| alt            = 
| caption        = United Kingdom Theatrical release poster
| director       = E.W. Swackhamer
| producer       = Edward J. Montagne
| writer         = Alvin Boretz Stan Lee  
| screenplay     = 
| story          = 
| based on       = Spider-Man by Stan Lee Steve Ditko
| starring       = Nicholas Hammond Lisa Eilbacher
| music          = Johnnie Spence
| cinematography = Fred Jackman
| editing        = Aaron Stell
| studio         = Danchuck Productions
| network    = CBS
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} The Amazing Spider-Man. It was directed by E.W. Swackhamer and stars Nicholas Hammond and Thayer David.

==Plot== Peter Parker, a freelance photographer for the Daily Bugle, is bitten by a radioactive spider and discovers he has gained superpowers, such as super-strength, agility and the ability to climb sheer walls and ceilings. When a mysterious Guru places people under mind-control to rob banks and threatens to have 10 New Yorkers commit suicide at his command unless the city pays him $50 million, Peter becomes the costumed hero Spider-Man to stop the crooks fiendish scheme. Things take a bad turn when the villain hypnotizes Peter Parker into being one of the ten people to jump off a building on command. 

==Cast==
*Nicholas Hammond&nbsp;– Spider-Man|Spider-Man / Peter Parker David White&nbsp;– J. Jonah Jameson
*Michael Pataki&nbsp;– Captain Barbera Joe "Robbie" Robertson
*Lisa Eilbacher&nbsp;– Judy Tyler Aunt May Parker
*Robert Hastings&nbsp;– Monahan
*Ivor Francis&nbsp;– Professor Noah Tyler
*Thayer David&nbsp;– Edward Byron

==Release==
The film premiered on CBS on September 14, 1977. It was later shown theatrically overseas. It was then released on VHS in 1980.

==References==
 
 

==External links==
* 

 
 
 
 

 
 
 
 
 
 
 
 

 
 