Shake, Rattle & Roll 11
 
{{Infobox Film | name           = Shake, Rattle & Roll 11
| image          = ShakeRattleRoll11movie_poster.png
| image_size     =
| caption        = 
| director       = Jessell Monteverde    Rico Gutierrez   Don Michael Perez
| producer       = Lily Monteverde   Roselle Monteverde-Teo
| writer         = 
| narrator       = 
| starring       =  Ruffa Gutierrez Mark Anthony Fernandez Maja Salvador Rayver Cruz Zoren Legaspi Iya Villania Jennica Garcia Mart Escudero
| music          = 
| cinematography = 
| editing        =  Regal Entertainment, Inc. Regal Multimedia, Inc.
| released       = December 25, 2009        
| runtime        = 
| country        = Philippines
| language       = Tagalog   English
| budget         = 
| gross          = P76.3 million
}}
 horror Suspense suspense film Regal Entertainment, Inc. and Regal Multimedia, Inc. It was an official entry in the 2009 Metro Manila Film Festival. The film received the Festival Award for the Best Make-up. 

==Synopsis==

===Diablo===
Claire (Maja Salvador), an unfaithful and troubled intern doctor was assigned to treat a young girl with a deadly flu virus. Upon entering the room, the girl mistreats Claire when she tries to treat her. Before leaving, the girl begs Claire that she needs a priest to cure her, but Claire didnt believe her and assures that she is sick. The girl angrily shouts at her and tells Claire that she was possessed by a demon until she was taken control by the demon. The girl begins to attack Claire, but she attempts to escape and helplessly watches the girl fell out of the hospital to her death before the girl shifts her face to Claires.

Claire lived an unhappy life after her family were murdered by thieves and lives with her religious aunt Beth. Before she take some sleep, She began to remember her family and suffer a nightmare of the girl patient. The next morning she began to act strange, hearing mysterious voices and seeing ominous visions around her before she watch Ronnie (Mark Anthony Fernandez), Claires former boyfriend who is now a priest, doing mass. She even tries to convince her boyfriend Jake on her story before she mistreats him. Later before Jake leaves, he accidentally hits a girl but turns out to be the girl patient and kills Jake.

Dr. Yulo visits Beth and explains on Claires condition. Beth explains to her on Claires behavior and attitude as Dr. Yulo assures that she was emotionally repressed. When Dr. Yulo enters her room and explains to Claire on her behavior, She lost control of herself that she was possessed by a Diablo and attacks Dr. Yulo but escapes as Beth watches in horror. Beth calls for Ronnie with Fr. Paul to have an exorcism for Claire but the entrance was blocked by a large mound of catholic statues.

Before arriving at the house, they are attacked by a possessed Claire but Fr. Paul manage to preach the prayers before Claire collapses. Ronnie began to exorcise the demon on Claires body, but failed on his attempts. The demonized Claire attacks Ronnie but he manage to save himself. The Diablo begins insulting him on his relationship with Claire but Ronnie began to tell Claire to fight herself. Claire manage to save herself from the demon and begs Ronnie to save her. The Diablo began to release from Claire until shards of wood and glass fly towards Ronnie, pinning him to the wall. The Diablo vanishes from Claire but Ronnie dies from his injuries as Claire mourns to his death. 

Claire became heartbroken & guilty after Ronnie is gone and thus changing her life. Later, Claire is now a doctor and begins treating a little girl. The little girls father appears whom Claire saw Ronnies deforming doppelganger in a vision as she screams.

===Ukay-Ukay===
Kayla (Ruffa Gutierrez) plans on her engagement with Harold (Zoren Legaspi). While driving with her best friend and fashion designer Basti (John Lapus), Kayla runs over a woman. As they get out of the car, they find the woman gone but instead sees a secondhand wedding veil before they leave. The next day as she visits a local boutique, she finds a secondhand wedding gown and decides to buy it. Finding it damaged, she requests Basti to fix it. Before preparing to fix it, the gown comes to life and kills a seamstress. While Kayla and Harold have their dinner at a restaurant, she sees an old man (Miguel Faustman) watching her before he leaves. Later, Kayla begins to suffer nightmares about a ghost and an old house. The next day, the gown attacks and kills Basti.

After the gown is delivered, Kayla starts to have her nightmares again. After hearing about Bastis death, the old man from the restaurant warns Kayla to stop the wedding but she disregards his warning. Kayla notices the gown and decides to return it to the store. She then later becomes suspicious of the gown that killed Basti and caused her nightmares earlier. Harold disregards this and decides to sleep with Kayla. However, when night falls, the gown comes back and attacks Harold & Kayla, but they manage to burn it.

While searching for a church, Kayla notices a house similar to the one in her dreams and decides to go inside with Harold. While investigating the house, she happens to notice a painting wherein the woman is wearing the same wedding gown that haunts her. Before the couple could leave, the old man appears to them. Kayla, although mad at first at the old man for seeing him again, explains to the old man of their current situation. The old man tells her the story of the gown. The gown was formerly owned by Lucia (Megan Young). At her wedding, Lucia waited for a long time for her groom, Joaquin. She eventually discovered that he married another woman and Lucia was left heartbroken. Out of anger, she killed Joaquins bride and attempted to kill Joaquin but he accidentally stabs her instead. Before dying, she cursed herself and her gown that she will kill every bride related to Joaquins bloodline. Joaquin stops being involved in women, however, he had a relationship with a lady named Juanita. Before they ended their relationship, Juanita gave birth to a boy who is revealed to be Kaylas father. The old man reveals himself to be Joaquin and that he is Kaylas grandfather. Kayla is the first woman that was related to Joaquins bloodline. Joaquin urges her to stop the wedding because the curse will never stop.

While designing a new wedding gown, Kayla becomes suspicious on her new gown, thinking it was the same one that will kill her since and has an uncanny resemblance to the old gown, but changes her mind and decides to wear it. At the peak of their wedding, Lucia possesses Kayla and attempts to kill Harold but Joaquin arrives at the church and began apologizing to Lucia on what he did to her, begging her to take him instead. Lucia kills Joaquin and takes his soul and herself to the afterlife.

The wedding continued and Kayla was engaged to Harold. As the guests clap for Kayla and Harold, the gown appear and walking towards them.

===Lamanglupa===
Sheila (Jennica Garcia) was interrogated by a policeman (Archi Adamos) and a social worker (Julia Clarete) on the incident at the forest. The social worker tries to help her on what happened, much to the policemans anger as Sheilas story was explained.

She and her mean boyfriend Ryan (Mart Escudero), her best friend Chari (Bangs Garcia) and her childhood friend Archie (Rayver Cruz) are on their way to the forest on camping with their friend Lia (Iya Villania) and troublesome twins Kiko (Felix Roco) & Pong (Dominic Roco). The twins cause trouble in the forest which destroys the anthills much to Lias warning and dislikes. As the friends were sleeping, When Kiko begins strolling at night in the forest, he is captured by a figure. The next morning, they witnessed that he is missing and decide to look for him. When attempting to find him, Lia tries to warn Pong and his brother about the creatures who live in the anthills. They enrage when their homes were destroyed and seek their revenge. When searching for him, Archie, Lia and Pong hears Kikos voice in a cave but they cant enter because the cave is deep. Lia and Archie will return to the campsite to get some climbing gear while Pong stays at the cave. Chari was separated from Ryan and Sheila where she was chased by a figure. She gets lost and attempts to run but she fell on the mud. When Chari attempts to get up, a creature attacks and kills her. After Sheila and Ryan find her mutilated body, They attempt to escape and met with Lia and Archie. After a brief argument, Archie & Sheila find Pong and climb down the cave. When entering the cave, they find Kiko and tried to help him. After Kiko dies, the creature who reveals to be a "Lamanglupa", appears and attacks the group as Archie fights the creature. Pong is killed by the creature as Sheila and Archie escape. After separating from the others, another creature appears and kills Ryan as Lia attempts to escape but the creature catches and kills her. Archie is killed, allowing Sheila to escape. Before Sheila gets the car keys and began to leave, she was attacked by the two creatures but Sheila survived and escape before she reaches to the police earlier.

None of them dont believe her story and the policeman assures that she committed murder. As she remains unresponsive, Sheila screams in pain, collapses on the floor and dies. When the policeman look at her body, a creature emerges from her stomach, revealing she gave birth to a "lamanglupa".

==Cast==

===Diablo===
*Mark Anthony Fernandez as Fr. Ronnie 
*Maja Salvador as Claire
*Gina Alajar as Tita Beth 
*Joem Bascon as Fr. Paul 
*Alex Castro as Jake
*Irma Adlawan as Dr. Yulo
*Janice de Belen as Nurse
*Paolo Bediones as Newscaster

===Ukay-Ukay===
*Ruffa Gutierrez as Kayla 
*Zoren Legaspi as Harold
*John Lapus as Basti 
*Megan Young as Lucia
*Miguel Faustmann as Joaquin
*Carlo Guevarra as Young Joaquin 
*Philip Lazaro as Fashion Designer

===Lamang Lupa===
*Jennica Garcia as Sheila
*Rayver Cruz as Archie 
*Mart Escudero as Ryan
*Iya Villania as Lia
*Dominic Roco as Pong
*Felix Roco as Kiko
*Bangs Garcia as Chari 
*Archi Adamos as Policeman 
*Julia Clarete as Social Worker

==Reception==
 

==Awards & Nominations==
{| border="2" cellpadding="3" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
| Award-Giving Body || Award || Work || Results
|- Best Picture || rowspan=2|Shake, Rattle & Roll 11 ||  
|- Best Make-up ||  
|- Best Film Director(s) || Rico Gutierrez, Don Michael Perez, Jessel Monteverde ||  
|- Best Supporting John Lapus ||  
|- Best Original Story || Gina Marie Tagasa, Elmer Gatchalian, Renato Custodio ||  
|- Best Cinematography || Juan Lorenzo ||  
|-
|}

==References==
 

 

 
 
 
 
 
 