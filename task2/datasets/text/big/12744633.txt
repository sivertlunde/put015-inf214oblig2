Parlor, Bedroom and Bath
{{Infobox film
| name = Parlor, Bedroom and Bath
| image = Parlor, Bedroom and Bath.jpg
| caption = Swedish theatrical poster to Parlor, Bedroom and Bath
| director = Edward Sedgwick
| producer = Buster Keaton
| writer = Charles William Bell (play) Mark Swan (play)
| starring = Buster Keaton Charlotte Greenwood
| music =
| cinematography = Leonard Smith
| editing = William LeVanway
| art director = Cedric Gibbons
| distributor = Metro-Goldwyn Mayer
| released =  
| runtime = 73 minutes
| language = English
| budget =
}} same name, Hamilton West and Mark Swan, which opened on Broadway in New York City on Christmas Eve, 1917 and ran for 232 performances. 
 1920 film version, was believed to have been part of the production team of this film, though he was not a member of the cast.

The film is now in the public domain and can be freely downloaded from the internet.

==Plot== Reginald Denny) wants to get married to Virginia Embrey (Sally Eilers). However, Virginia refused to marry unless her older sister, the hard-to-please Angelica (Dorothy Christy) gets married first. Angelica, in turn, finds every man she knows too dull and predictable, and for this reason prefers to stay single. Jeff then tries to make Angelica interested in the mild-mannered and timid Reggie Irving (played by Keaton) passing him off as a notorious playboy to intrigue her. He asks his friend Polly to teach Reggie "how to treat a woman right", but he turns out to be a disastrous learner.

==Cast (in credits order)==
*Buster Keaton as Reginald Irving
*Charlotte Greenwood as Polly Hathaway Reginald Denny as Jeffrey Haywood
*Cliff Edwards as Bell Hop
*Dorothy Christy as Angelica Embrey
*Joan Peers as Nita Leslie
*Sally Eilers as Virginia Embrey
*Natalie Moorhead as Leila Crofton
*Edward Brophy as Detective
*Walter Merrill as Frederick Leslie
*Sidney Bracey as Butler

==See also== List of films in the public domain

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 