A River Runs Through It (film)
{{Infobox film
| name           = A River Runs Through It
| image          = A_river_runs_through_it_cover.jpg
| caption        = original movie poster
| image_size     =
| director       = Robert Redford
| producer       = Jake Eberts Robert Redford Patrick Markey
| screenplay     = Richard Friedenberg
| based on       =  
| narrator       = Robert Redford (uncredited)
| starring       = Craig Sheffer Brad Pitt Tom Skerritt Brenda Blethyn Emily Lloyd
| music          = Mark Isham
| cinematography = Philippe Rousselot
| editing        = Robert Estrin Lynzee Klingman
| distributor    = Columbia Pictures(USA) Pathé(UK)
| released       =  
| runtime        = 123 minutes
| country        = United States English
| gross          = $43,440,294 
}} 
 1992 United American film A River Runs Through It (1976) written by Norman Maclean (1902–90), adapted for the screen by Richard Friedenberg.
 Missoula in come of age in the Rocky Mountain region during a span of time from roughly World War I (1917–18) to the early days of the Great Depression (1929–41), including part of the Prohibition era (1919–33).   

The film won an Academy Award for Best Cinematography in 1993 and was nominated for two other Oscars, for Best Music, Original Score and Best Adapted Screenplay. The film grossed $43,440,294 in US domestic returns.   

==Plot summary==
A River Runs Through It is the true story about two boys, Norman (Craig Sheffer) and Paul (Brad Pitt), growing up in 1920s Missoula, Montana with their father, a Presbyterian minister. Much of the film is about the two boys returning home after becoming troubled adults. A common theme in the film is the mens love of fly fishing for trout in the Blackfoot River and how it impacted their lives. The film is told from Normans point of view.

==Cast==
* Craig Sheffer as Norman Maclean
* Brad Pitt as Paul Maclean
* Tom Skerritt as Reverend Maclean
* Brenda Blethyn as Mrs. Maclean
* Emily Lloyd as Jessie Burns
* Edie McClurg as Mrs. Burns
* Joseph Gordon-Levitt as Young Norman
*  Vann Gravage as Young Paul Maclean
* Nicole Burdette as Mabel
* Susan Traylor as Rawhide
* Michael Cudlitz as Chub
* Rob Cox as Conroy
* Buck Simmonds as Humph
* Stephen Shellen as Neal Burns

==Production==

===Filming===
  Livingston and Boulder Rivers.  The waterfall shown is Granite Falls in Wyoming.      The church scenes were filmed in the Redeemer Lutheran Church in Livingston.  

An article published in the Helena Independent Record in July 2000 and based on recollections of people who knew both brothers noted a number of specifics about the Macleans — notably various chronological and educational details about Paul Macleans adult life — that differ somewhat from their portrayal in the film and novella. 

===Music===
Mark Isham, who would go on to compose the scores to most Robert Redford-directed films, composed the musical score for the film. Originally, Elmer Bernstein was hired to score the film. However, after Redford and Bernstein disagreed over the tone of the music, Bernstein was replaced by Isham.  Rushed for time, Isham completed the score within four weeks at Schnee Studio of Signet Sound Studios in Hollywood, CA. Upon release, the music was met with positive reviews earning the film both nominations for Grammy and Academy awards. The A River Runs Through It (Original Motion Picture Soundtrack) was released on October 27, 1992. 

In some home video releases of the film, Elmer Bernstein is credited as the films composer despite his score being rejected during post-production.

==Release==
===Critical reception===
 
Released on October 9, 1992, the film grossed $43,440,294 in US domestic returns.  The film achieved holds an 83% rating on Rotten Tomatoes based on polled critical reviews. The sites consensus reads: "Tasteful to a fault, this period drama combines a talented cast (including a young Brad Pitt) with some stately, beautifully filmed work from director Robert Redford."  Much of the praise focused on Pitts portrayal of Paul, which has been cited as his career making performance. 

===Awards=== Academy Awards Best Cinematography Best Music, Best Writing, Golden Globes, Best Director - Motion Picture, but did not win.

==References==
 

==External links ==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 