Tomfoolery (film)
{{Infobox film
| name =  Tomfoolery 
| image =
| image_size =
| caption =
| director = Willi Forst 
| producer =  Fritz Klotsch 
| writer =  Jochen Huth    Willi Forst 
| narrator =
| starring = Renate Müller   Jenny Jugo   Anton Walbrook   Heinz Rühmann
| music = Peter Kreuder   
| cinematography = Werner Bohne   Theodore J. Pahle   Hans Wolff     
| studio = Cine-Allianz 
| distributor = 
| released =12 June 1936   
| runtime = 94 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Tomfoolery (German:Allotria) is a 1936 German comedy film directed by Willi Forst and starring Renate Müller, Jenny Jugo and Anton Walbrook.  It premiered at the Gloria-Palast in Berlin on 12 June 1936. A pair of friends fall in love with the same woman, before realizing they are really in love with two other women. Racing to his romantic interest, one of the friends (Heinz Rühmann) takes by chance part in the Monaco Grand Prix.

Joseph Goebbels remarked:  "Quite energetic and lively. But its overdone and therefore not totally satisfying. Less would be more." (original: „Sehr flott und mit viel Tempo. Aber übertrieben an Effekten, und darum nicht ganz befriedigend. Weniger wäre mehr.“)

== Cast ==
* Renate Müller as Viola 
* Jenny Jugo as Gaby 
* Anton Walbrook as Philip 
* Heinz Rühmann as David 
* Hilde Hildebrand as Aimée 
* Heinz Salfner as Gabys Vater 
* Will Dohm as Theodor 
* Norma Wellhoff as Stewardess 
* F.W. Schröder-Schrom as Schiffspassagier an Violas Tisch 
* Julia Serda as Schiffspassagierin an Violas Tisch 
* Toni Tetzlaff as Mädchen bei Gaby 
* Ingeborg Peter as Gabys Freundin 
* Erich Dunskus as Zollbeamter 
* Alfred Karen as Schiffspassagier bei der Überfahrt 
* Ferdinand Robert as Schiffspassagier bei der Überfahrt 
* Günther Vogdt as Schlagersänger der Schiffkapelle 
* Margarethe von Ledebur as Angestellte im Schönheitssalon 
* Fritz Draeger as Assistant des Rundfunkreporters 
* Gretel von Schrabich as Angestellte im Modesalon 
* Werner Bernhardy as Barman bei der Modeschau 
* Gustav Mahncke as Gast bei der Modeschau (as Gustav Mahnke) 
* Lieselotte Aureden as Gast bei der Modeschau 
* André Saint-Germain as Französischer Rundfunkreporter 
* Theodor Thony as Französischer Rennbahnhelfer 

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 
 