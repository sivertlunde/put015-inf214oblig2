This Way Please
{{Infobox film
| name           = This Way Please
| image          =
| caption        =
| director       = Robert Florey
| producer       =
| writer         = 
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = October 7, 1937
| runtime        = 73 mins.
| country        = United States
| language       = English 
| budget         =
}}
 Charles Rogers, a popular singer from the days of vaudeville entertainment.

According to historian Martin Grams, this motion picture was the introduction of Mary Livingstone (Mrs. Jack Benny) and Jim and Marian Jordan (radios Fibber McGee and Molly).  This was also Betty Grables first motion picture under contract with Paramount Studios.

==Plot==
Betty Grable plays young Jane Morrow, who applies for the job of a theater usherette, and encounters her matinée idol.  After he takes a liking to her, he arranges for her to audition in front of an audience. Jane is a hit, making her idol less favorable. Jane soon finds herself engaged to another man, so a battle of romantic wits ensues.

== Cast ==
 Charles Buddy Rogers as Brad Morgan
* Betty Grable as Jane Morrow
* Ned Sparks as Inky Wells Jim Jordan as Fibber McGee
* Marian Jordan as Molly McGee
* Porter Hall as S.J. Crawford
* Lee Bowman as Stu Randall
* Cecil Cunningham as Miss Eberhardt
* Mary Livingstone as Maxine Barry
* Wally Vernon as Bumps

==External links==
* 
 

 
 
 
 
 
 
 


 