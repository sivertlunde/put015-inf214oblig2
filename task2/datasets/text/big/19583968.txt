The Triumphs of a Man Called Horse
{{Infobox film
| name          = Triumphs of a Man Called Horse John Hough
| producer      = Derek Gibson Donald P. Borchers
| writer        = Jack DeWitt Dorothy M. Johnson Ken Blackwell Carlos Aured
| starring      = Richard Harris
| music         = Georges Garvarentz
| cinematography+ John Alcott John Cabrera
| editing       = Ray Watts
| country       = United States
| distributor   = Jensen Farley Pictures
| released      =  
| runtime       = 86 min.
| language      = English
}} John Hough A Man Called Horse (1970) and The Return of a Man Called Horse (1976).

==Synopsis==
Now in his 60s, Morgan is the chief of the Sioux and is much hated by white men because "manys a white mans died from the tricks he taught the Sioux." However, the government calls Horse to peace talks. But someone with a rifle assassinates him and his men after the meeting.

His son (Michael Beck), who was raised among the Sioux but was sent away to school in the East, returns to deal both with white settlers encroaching on the Sioux lands and with his own people who want to go to war. He also meets an attractive young woman who happens to be a Crow, the traditional enemy of the Sioux. In the end, it is revealed that Horse was murdered by a phony preacher who wanted to start a war so he could get the Siouxs lands. Horses son and his Crow girlfriend have a traditional showdown with the preacher and his head honcho. At the end, Horses son sees the triumphant spirit of his father in full chief regalia.

==Production==
The movie was filmed on location in Mexico in May 1982.

==Reception==
  (1970) and The Return of a Man Called Horse (1976) cashes in on the popularity of its predecessors. Richard Harris appears briefly as the "Man Called Horse", an aging Englishman who has headed a Sioux tribe for 30 years... The film is anything but a triumph." 
 
==References==
 
==External links==
* 

 

 
 
 
 
 
 
 
 
 


 