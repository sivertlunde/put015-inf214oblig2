For Eternal Hearts
{{Infobox film
| name           = For Eternal Hearts
| image          = For Eternal Hearts film poster.jpg
| caption        = Theatrical poster
| film name      = {{Film name
 | hangul         =    
 | rr             = Byeolbit sogeuro
 | mr             = Pyŏlpit sok ŭro}}
| director       = Hwang Qu-duk
| producer       = Jo Seong-gyu Jo Eun-un Hwang Qu-duk
| writer         = Hwang Qu-duk Kim Min-sun Cha Soo-yeon
| music          = Go Byeong-jun Kim C
| cinematography = Ko Myeong-woo
| editing        = 
| distributor    = Sponge Entertainment
| released       =  
| runtime        = 103 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = United States dollar|$45,398
}} 2007 South Korean film.

== Plot ==
College professor Su-young recounts to his class the tale of his first love. As a student in the 1980s, he meets a wild and eccentric girl who he names "Pippi", and is crushed when she later jumps to her death from a window. But soon after she magically reappears, and his life becomes increasingly surreal and bizarre.

== Cast ==
* Jung Kyung-ho ... Su-young (student) Kim Min-sun ... Pippi
* Cha Soo-yeon ... Su-ji
* Jung Jin-young ... Su-young (professor)
* Kim C
* Jang Hang-seon
* Lee Soo-na
* Song Seung-hwan

== Release ==
For Eternal Hearts premiered as the opening film of the 11th  , which opened on October 20, 2007. 

== References ==
 

== External links ==
*    
*  
*  
*  

 
 
 
 
 
 
 
 


 
 