Mr. Mouse Takes a Trip
 
{{Infobox Hollywood cartoon
| cartoon name      = Mr. Mouse Takes a Trip
| series            = Mickey Mouse
| image             = Mr. Mouse Takes a Trip.jpg
| image size        =
| alt               =
| caption           = Theatrical release poster
| director          = Clyde Geronimi {{cite encyclopedia
 | last = Smith
 | first = Dave
 | authorlink = Dave Smith (archivist)
 | encyclopedia =  
 | title = Mr. Mouse Takes a Trip
 | edition = 1st
 | year = 1996 Hyperion
 New York
 | isbn = 0-7868-8149-6
 | pages = 336
}} 
| producer          = Walt Disney John Sutherland
| story artist      =
| voice actor       =  Billy Bletcher Walt Disney Lee Millar
| musician          = Leigh Harline Oliver Wallace
| animator          = Ray Abrams Clyde Geronimi Ed Love Kenneth Muse Marvin Woodward
| layout artist     =
| background artist = Jim Carmichael Walt Disney Productions RKO Radio Pictures
| release date      =   (USA)}} 
| color process     = Technicolor
| runtime           = 8 minutes
| country           = United States
| language          = English
| preceded by       = Plutos Dream House
| followed by       = The Little Whirlwind
}}
Mr. Mouse Takes a Trip is a 1940 American animated   as Mickey, Lee Millar as Pluto, and Billy Bletcher as Pete. 
 Pluto traveling Pete plays conductor intent on enforcing the rule.

==Synopsis== Walt Disney Productions headquarters). They board a west-bound train, but are both immediately kicked off by the conductor, played by Pete, because dogs are not allowed (side gag reveals Plutos luggage to contain bones).  Pete then rambles off the trains destinations and forces his watch to tell him when the train is ready to leave.  When the watch does show its time for the train to go, Pete calls "All aboard!"

Mickey, at this point, decides to smuggle Pluto onboard by squeezing the dog inside his suitcase. At first, the handle breaks, making Mickey almost leave Pluto behind, but Mickey recovers the suitcase and manages to make it aboard just as the train is clearing the platform. Later, Pluto barks, wanting to be let out.  Mickey scolds him for nearly arousing Pete, but manages to take Pluto out and unsquash Pluto.  The freedom is only short lived as Pete is coming through the train to collect tickets, forcing Mickey to squash Pluto back into position in the suitcase. After biting "OK" in Mickeys tickets, Pete sees Mickeys suitcase containing Pluto in the seat and forcefully throws it into an overhead baggage net. This causes Pluto to bark, making Pete suspicious.

He then recognizes Mickey, who is trying to hide behind a large newspaper and make it look like the barking was coming from him. Understanding that Pluto has been stowed in the suitcase, Pete menacingly asks Mickey if he is alone. He then makes up a story about owning a little cat whod cry when he was all alone and screams a loud "MEOW!" (with his face look like a real cat) at the suitcase causing the dog to leap out. Realizing his covers been blown, Pluto ducks back into the suitcase, but Pete has already figured it out.  Before he can catch the pair, Mickey and Pluto run away and a chase ensues on board the train.

Mickey and Pluto first hide in a sleeping car where Pete mistakenly intrudes on a female passenger and gets assaulted. Pete then stumbles into another bed where Mickey and Pluto (disguised as babies) are hiding.  Pete apologizes for the intrusion, but quickly catches on after realizing he was covering up Plutos tail.  Just as Mickey and Pluto are gloating that theyd fooled Pete, Pete bursts in and threatens to beat them to a pulp, but the sudden darkness (from the train running through a tunnel) allows the pair to escape, leaving Pete to beat the mattress to a pulp (and a brief entanglement with the springs).
 Indian chief with Pluto as his papoose, but Pete eventually sees through their disguises right after Pluto bites his hand.

While Mickey and Pluto are next to an open window, Pluto is caught on a passing mail hook which whisks him outside the train. Mickey runs after him through the train, and is just barely able to grab Pluto as he exits the last car. Pete throws their luggage out after them and they fall to the ground from the mail hook. Mickey looks up at the station sign and is pleasantly surprised that they have already arrived at their destination &ndash; Pomona (Amtrak station)|Pomona.

==Voice-over footage==
  (left) and Billy Bletcher recording voice-overs for Mr. Mouse Takes a Trip]]
Mr. Mouse Takes a Trip is unique among the classic Disney shorts in that film footage exists of the voice-over session, which included Walt Disney and Billy Bletcher. According to film historian Leonard Maltin, the footage was not known to exist and only discovered (as of 2004) "not too many years ago." 
 easter egg.  Edited portions of the footage were also seen in earlier releases. 
 

==Adaptations== verse and was illustrated by Tom Wood. {{cite book
| last       = Gerstein
| first      = David
| authorlink = David Gerstein
| title         = Walt Disneys Mickey and the Gang: Classic Stories in Verse
| year      = 2005
| publisher = Gemstone Publishing
| location  = Timonium, Maryland|Timonium, MD
| isbn      = 1-888472-06-5
| page      = 246
}}  

In October 1940, a prose version of Mr. Mouse Takes a Trip was printed in the first edition of Walt Disney Comics and Stories. This five-page version is a closer retelling of the film, with the added detail that Mickey is heading to an "important meeting" in Pomona which he cant be late for. Pluto comes along only because he would get lonely if he stayed home alone.

In 2010, the film inspired the Italian comic story "Topolino, Pluto e la gita in montagna," or "Mickey, Pluto, and the Trip to the Mountain." The story, published in the May edition of Extralarge XL Disney, is 25 pages and written and illustrated by Enrico Faccini 

==Releases==
*1940 &ndash; Original theatrical release Super 8) 
*c. 1980s &ndash; "A Disney Vacation" (TV)
*1984 &ndash; "Cartoon Classics - Limited Gold Edition: Mickey" (VHS)
*1986 &ndash; "Adventures with Mickey" (TV) The Ink and Paint Club, episode # 1.43: "On Vacation" (TV)
*1998 &ndash; "The Spirit of Mickey" (VHS)
*2004 &ndash; " " (DVD)
*2006 &ndash; " " (DVD)
*2009 &ndash; "  Volume 1: Mickey and the Beanstalk" (DVD)
*2009 &ndash; Have a Laugh! (TV)
*2010 &ndash; "Have a Laugh!: Volume 2" (DVD)

==Trivia==
*This cartoon was featured in Disneys Magical Mirror Starring Mickey Mouse.
*Pete says "MEOW!" like a real cat (with fangs and Whiskers)
*Audio from Pete was used in Get A Horse! ("Its you!", "I used to have a little cat once!", "Why you little...!")

==Notes==
 

==External links==
* 
* 

 

 
 
 
 
 
 