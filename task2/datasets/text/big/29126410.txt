The Beggar's Deceit
{{Infobox film
| name           = The Beggars Deceit
| image          =
| image_size     =
| caption        =
| director       = Cecil Hepworth
| producer       =
| writer         =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = Hepworth Manufacturing Company
| released       = November 1900 (UK) http://www.imdb.com/title/tt2002820/?ref_=fn_al_tt_1 
| runtime        = 58 seconds
| country        = United Kingdom
| language       =
}} 1900 British short film directed by Cecil Hepworth.  The film is a comedy sketch shot from a static camera position, with the composition divided into thirds: on the left the beggar, in the centre the pavement and pedestrians, and to the right the road and vehicle traffic.

==Synopsis==
A legless beggar with a sign around his neck saying "cripple" pushes himself slowly and laboriously on a trolley along the pavement, soliciting alms from sympathetic passers-by.  A policeman gradually approaches from the distance.  Feeling suspicious, he taps the beggar on the shoulder, whereupon the beggar leaps up in a panic and runs away on his perfectly functional legs.  The policeman trips over the trolley before recovering his footing and setting off in pursuit. 

==References==
 

 
 
 
 
 
 
 
 
 


 
 