Rendu Pondatti Kaavalkaaran
{{Infobox film
| name           = Rendu Pondatti Kaavalkaaran
| image          = 
| image_size     =
| caption        = 
| director       = Relangi Narasimha Rao
| producer       = P. Balaraman
| writer         = Relangi Narasimha Rao B. Shankar  (dialogues) 
| starring       =  
| music          = Raj-Koti
| cinematography = Sarath
| editing        = Murali-Ramaiah
| distributor    =
| studio         = Sree Productions
| released       =  
| runtime        = 130 minutes
| country        = India
| language       = Tamil
}}
 1992 Tamil Tamil comedy film directed by Relangi Narasimha Rao. The film features Anand Babu in a dual role, Rohini (actress)|Rohini, Vaidehi and Devipriya in lead roles. The film, produced by P. Balaraman, had musical score by Raj-Koti and was released on 7 February 1992.   

==Plot==

Krishnan (Anand Babu) is an honest police constable who has two wives Rukmini (Rohini (actress)|Rohini) and Satyabhama (Vaidehi). However his two wives hate each other. Krishnan is transferred to another city because of his honesty and the fact that he often refuses to do his superiors drudgery.

In the past, Krishnan got married with Rukmini who thought that he was a Sub-inspector of police. When she realized his real job, she left him alone. In the meantime, Krishnan and his neighbour Satyabhama fell in love with each other. They had sexual intercourse. Then Satyabhama begged Rukmini to help her and Satyabhama got married with Krishnan.

Fed up of the transfers, Krishnan throws away his police uniform and  becomes a pickpocket. Anand, Krishnans look-alike, takes his identity and becomes a police constable. Soon, Anand must juggle between Krishnans two wives while Krishnan must manage Anands girlfriend Lalitha (Devipriya).

==Cast==

*Anand Babu as Krishnan and Anand Rohini as Rukmini
*Vaidehi as Satyabhama
*Devipriya as Lalitha Janagaraj as Muthusamy Senthil
*Y. Vijaya as Mangalam Sri Lakshmi
*Loose Mohan
*Omakuchi Narasimhan
*Tyist Gopu

==Soundtrack==

{{Infobox album |  
| Name        = Rendu Pondatti Kaavalkaaran
| Type        = soundtrack
| Artist      = Raj-Koti
| Cover       = 
| Released    = 1992
| Recorded    = 1992 Feature film soundtrack
| Length      = 19:10
| Label       = 
| Producer    = Raj-Koti
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Raj-Koti. The soundtrack, released in 1992, features 4 tracks with lyrics written by Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Ennikko || S. P. Balasubrahmanyam, Minmini|| 4:40
|- 2 || Kanna Kanna || S. P. Balasubrahmanyam, Minmini || 4:45
|- 3 || Police Police Police Mama || S. P. Balasubrahmanyam, Minmini, K. S. Chithra || 4:55
|- 4 || Sundakkai Kuzhambu || K. S. Chithra, Minmini || 4:50
|}

==References==
 

 
 
 
 