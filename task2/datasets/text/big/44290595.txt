The Frontiersmen
{{Infobox film
| name           = The Frontiersmen
| image          = The Frontiersmen poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lesley Selander
| producer       = Harry Sherman 
| screenplay     = Norman Houston Harrison Jacobs 
| story          = Clarence E. Mulford William Boyd William Duncan Clara Kimball Young
| music          = Gerard Carbonara John Leipold
| cinematography = Russell Harlan
| editing        = Sherman A. Rose 	
| studio         = Harry Sherman Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western film William Boyd, William Duncan and Clara Kimball Young. The film was released on December 16, 1938, by Paramount Pictures.  

==Plot==
 

== Cast ==		 William Boyd as Hopalong Cassidy
*George "Gabby" Hayes as Windy Halliday 
*Russell Hayden as Lucky Jenkins
*Evelyn Venable as June Lake
*Charles Anthony Hughes as Mayor Judson Thorpe  William Duncan as Buck Peters
*Clara Kimball Young as Mrs. Amanda Peters
*Emily Fitzroy as Teacher that Quits Dickie Jones as Artie Peters
*John Beach as Henchman Quirt
*Roy Barcroft as Henchman Sutton
*The Robert Mitchell Boy Choir as The Crockett Schools Chorus Boys

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 