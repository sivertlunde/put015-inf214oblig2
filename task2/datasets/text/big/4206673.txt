Black Caesar (film)
 
{{Infobox film
  | name        = Black Caesar
  | image       = Black caesar.jpg
  | caption     = Theatrical release poster
    | writer      = Larry Cohen
  | starring    = Fred Williamson Gloria Hendry
  | director    = Larry Cohen
  | producer    = Larry Cohen Benjamin Fisz Kenneth Rive
| studio = Larco
  | distributor = American International Pictures
  | released    = February 7, 1973
  | runtime     = 87 minutes
  | language    = English
  | country     = United States
  | music       = James Brown
  | awards      =
| gross = $2 million (US/ Canada rentals)  Samuel Z Arkoff & Richard Turbo, Flying Through Hollywood By the Seat of My Pants, Birch Lane Press, 1992 p 203 
}} written and directed by Little Caesar. musical score Black Caesar) by James Brown (with heavy input from his bandleader Fred Wesley), his first experience with writing music for film. A sequel titled Hell Up in Harlem was released in late 1973. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 25 

==Plot== Italian mobsters of New York City and begins to establish a criminal empire, keeping a ledger book of all his dealings as leverage over his business associates, including McKinney. He meets and falls in love with a singer named Helen (Gloria Hendry) and marries her. She is unhappy as he is violent and rapes her. Eventually his enemies conspire with her, leading to an attempt on his life that leaves him shot and wounded. Killing his would-be assassins, he returns to his office to retrieve the ledger book. McKinney meets him there, and attempts to humiliate him before killing him. Tommy overpowers McKinney and beats him to death. Retrieving the ledger, a badly wounded Tommy returns to the house where he grew up, but a street gang attacks, robs and, presumably, kills him.

==Cast==
*Fred Williamson as Tommy Gibbs
*Gloria Hendry as Helen
*Art Lund as McKinney
*DUrville Martin as Reverend Rufus
*Julius Harris as Mr. Gibbs
*Minnie Gentry as Mama Gibbs
*Philip Roye as Joe Washington
*William Wellman Jr. as Alfred Coleman
*James Dixon as Bryant
*Val Avery as Cardoza
*Patrick McAllister as Grossfield
*Don Pedro Colley as Crawdaddy
*Myrna Hansen as Virginia Coleman
*Omer Jeffrey as Tommy as a boy
*Mike Anthony Jones as Joe as a boy

The Harlem film sequence was directed by James Signorelli, later to go on to producing films on Saturday Night Live.
 rap musicians The Alchemist for Prodigy (rapper)|Prodigys album Return of the Mac. The song is also sampled on Nas album Gods Son on "Get Down".
 Public Enemys Burn Hollywood Burn"; when the cinema announces the movie to be Driving Miss Daisy, guest rapper Big Daddy Kane suggests leaving, saying "I got Black Caesar back at the crib." Kane makes another reference to the movie in his song "How U Get A Record Deal".

Larry Cohen originally wrote this film with Sammy Davis Jr. in mind for the title role, but after the success of Shaft (1971 film)|Shaft, the film was retooled for Fred Williamson. 

==Reception==

The movie received a mostly positive reception. 

In 2009 Empire (magazine)|Empire named it eighteenth in a poll of the "20 Greatest Gangster Movies Youve Never Seen* (*Probably)".

==Release on DVD & HD==
* In 2001 it was released on DVD.
* In 2010 it was digitized in High Definition (1080i) and broadcast on MGM HD.

== References ==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 