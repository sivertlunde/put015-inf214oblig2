Belle van Zuylen – Madame de Charrière
 
{{Infobox film
| name           = Belle van Zuylen - Madame de Charrière
| image          = 
| image_size     = 
| caption        = 
| director       = Digna Sinke
| producer       = 
| writer         = Digna Sinke
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       =September 16, 1993
| runtime        = 
| country        = Netherlands Dutch
| budget         = 
}}
 Belle van Zuylen – Madame de Charrière  is a 1993 Dutch historical film, directed by Digna Sinke. 

A biography of Isabelle de Charrière and her friendship with Benjamin Constant.

This film got the Main Award of the 43rd International Filmfestival Mannheim-Heidelberg in 1994.

==Cast==
*Will van Kralingen	... 	Belle van Zuylen
*Laus Steenbeke	... 	Benjamin Constant
*Kees Hulst	... 	Mr. de Charrière
*Patty Pontier	... 	Henriette
*Carla Hardy	... 	Germaine de Stael
*Marieke van Leeuwen	... 	Charlotte
*Krijn ter Braak	... 	Mr. Suard
*Kitty Courbois	... 	Mrs. Saurin
*Truus te Selle	... 	Mrs. Pourrat
*Arthur Boni	... 	Vicar Chaillet
*Ed Bauer	... 	Mr. Du Peyrou
*Mirjam de Rooij		
*Eric Corton

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 
 