The Wool Cap
{{Infobox film
| name           = The Wool Cap
| image          = TheWoolCap.jpg
| caption        = Videotape Cover
| director       = Steven Schachter
| producer       = Elaine Frontain Bryant   Irene Litinsky   William H. Macy   Frances Croke Page   David A. Rosemont   Steven Schachter
| writer         = Jackie Gleason (story)   William H. Macy (teleplay)   Steven Schachter (teleplay)
| starring       = William H. Macy   Keke Palmer   Don Rickles   Catherine OHara   Ned Beatty
| music          = Jeff Beal
| cinematography = Guy Dufaux
| editing        = Paul Dixon Turner Network Paramount Home Entertainment
| released       =  
| runtime        = 90 Minutes
| country        = United States
| language       = English

}}
 American cable cable television movie, an updated and Americanized version of the 1962 feature film Gigot (film)|Gigot starring Jackie Gleason, who wrote the original story.
 mute and alcoholic superintendent of a dilapidated New York City apartment building. He becomes the unwilling parent figure for Lou, a young girl temporarily left in his care by a woman who fails to return for her. The two and Gigots pet monkey struggle to make it through the winter in his ramshackle basement apartment while he tries to find someone willing to take her in.

The film, a co-production of Viacom Productions and 20th Century Fox Television, was shot in Atlanta, Georgia and Montréal, Québec, Canada. Director by Schachter, it starred Macy as Charlie and Keke Palmer as Lou, with Don Rickles, Ned Beatty, Cherise Boothe, Julito McCullum, and Catherine OHara in supporting roles.

The film premiered on November 21, 2004 on Turner Network Television|TNT. It has been released on videotape and DVD.

Fox owns the international ancillary rights, while North American ancillary rights are now in the hands of CBS Television Studios. Paramount Pictures handles video distribution for the CBS library, though this particular program was released on DVD before the Viacom/CBS split.

==Plot==
In New York City, Charles Gigot (William H. Macy) is an alcoholic, disgruntled hearing-mute and superintendent of an apartment building with eccentric tenants.  One day, he meets Lou (Keke Palmer), a young girl who lives with her mother Arlene (Cherise Booth) and her boyfriend, Bernard (Edward Yankie).  When Gigot suspects that their apartment may have been broken into, he investigates, only to encounter two thugs looking for the couple.  His presence creates a diversion that allows Lou, who had been in hiding, to escape to the basement with her mother.  Despite Gigots protestations, Arlene leaves Lou in Gigots care for an indefinite time while she sorts things out with Bernard.

Annoyed at having his solitude compromised, Gigot attempts to ignore Lou completely, but her brash and back-talking attitude makes her unbearable.  Lou is equally uncomfortable with Gigots spartan style of living – he has no food in the apartment, drinks frequently, and is emotionally distant.  In fact, his only friends seem to be his pet monkey, Grace, and Gloria (Catherine OHara), a middle-aged prostitute.  Wanting to find a relative with whom Lou can live, he learns from the apartments lease that Lou has an aunt named Cheryl who lives in Philadelphia.  They travel via bus to Cheryls house, but Gigot is unsuccessful in convincing her to take Lou, learning that Arlene and Cheryl hate one another and that Cheryl does not consider Lou a relative.  Realizing that finding Arlene is the only solution to the dilemma, Gigot and Lou research one of her contacts, who sends them to an apartment building in the city.  Gigot goes in alone and discovers that the place is a crack house and that Arlene has become a drug addict.  Seeing a dead-end, Gigots only option is to return home with Lou.  Things become even more complicated, however, when he is robbed at a diner and is forced to perform a sideshow featuring Grace and Lou to raise enough money for the bus tickets.

During this course of events, Gigot and Lou bond and she comes to live with him permanently.  Lous influence causes Gigot to stop drinking and to help her with her education (he realizes that she is almost illiterate and two years older than the rest of her class).  They develop a father-daughter relationship with Lou helping Gigot grieve when local thugs poison Grace in an act of revenge for Gigot disposing of drugs belonging to one of their members.  Eventually, policemen visit the apartment building and inform Gigot that Arlene died from an overdose.  He attempts to shield Lou from the truth, but is forced to tell her while on a trip to a theme park.  Her grief prompts Gigot to seek out a friend named Clarence (Tyrone Benskin) who served with him in Vietnam and has since become a foster parent for multiple children without homes.  Clarence and his wife, Bess, however, announce that they are retired, leaving Gigot with no other choice but to become a foster parent himself after Child Protective Services arrives to claim Lou.  The process comes to a halt, however, when Gigots jail-time prevents him from being a candidate and a proposal of marriage to Gloria is refused.  At this point, Gigots only chance is his parents, whom he hasnt seen in twenty-eight years.  Hoping that they will officially adopt Lou so that he may take care of her, Gigot pays them a visit and is shocked to learn that his mother died years ago and his father (Ned Beatty) has since remarried.  During an argument, it is revealed that Gigots downward spiral into alcoholism and virtual isolation were a result of post-traumatic stress from his time in Vietnam.  The titular wool cap, which he wears throughout the film, belonged to his younger sister who died in a car accident while Gigot was at the wheel, drunk and high.  Hurt by Gigots indifference throughout the years, his father rejects him and his request to adopt Lou.

Meanwhile, being a foster chlid is hard on Lou, who falls into a life of waywardness.  She is eventually arrested for shop-lifting and put into juvenile hall where she refuses to see Gigot, having given up hope on ever finding a home.  Depressed and utterly forlorn, Gigot attempts to start afresh on Christmas Day by letting go of the past (symbolized by throwing his sisters wool cap into a river) and visiting a church, where he breaks down crying.  On coming home, he finds his stepmother and father waiting for him at the apartment building.  After a brief hesitation, Gigot and his father embrace, a sign that they will begin to repair their relationship.  Later that evening, Gigot visits Lou at juvenile hall and introduces her to his father.  Using sign language, he tells her that he would like to adopt her.  In a subtle imitation of Gigot, she becomes speechless and wordlessly accepts.

The film jumps forward a year.  It is winter again and Gigot has since become the manager of his fathers business.  He goes to pick up Lou from school where she has become an excellent student.  The two are seen driving off together and laughing, happy to have to have found one another.

==Nominations==
*2005 Emmy Award for Outstanding Lead Actor in a Miniseries or a Movie (William H. Macy)
*2005 Emmy Award for Outstanding Made for Television Movie
*2005 Emmy Award for Outstanding Single-Camera Picture Editing for a Miniseries or a Movie
*2005 Emmy Award for Outstanding Single-Camera Sound Mixing for a Miniseries or a Movie 
*2005 Golden Globe Award for Best Performance by an Actor in a Mini-Series or a Motion Picture Made for Television (William H. Macy)
*2005 Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Television Movie or Miniseries (Keke Palmer) 
*2005 Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Television Movie or Miniseries (William H. Macy)
*2005 Writers Guild of America Award for Best Adapted Long Form for Television (William H. Macy and Steven Schachter)
*2005 Young Artist Award for Best Performance in a TV Movie, Miniseries or Special by a Leading Young Actress (Keke Palmer)
*2005 Image Award for Outstanding Actress in a Television Movie, Mini-Series or Dramatic Special  (Keke Palmer)

==External links==
*  

 
 
 
 
 
 
 
 
 
 