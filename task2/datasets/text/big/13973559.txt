What a Crazy World
 
 
{{Infobox film
| name = What a Crazy World
| image = What_a_crazy_world.jpg
| caption = Soundtrack Album Cover
| director = Michael Carreras
| writer = Alan Klein
| editor = Max Benedict
| studio = Capricorn  
| distributor = Associated British Picture Corporation|Warner-Pathé Distributors
| released = 1963
| budget = £9,000 
| runtime = 88 min.
| country = UK
| language = English
}}

What a Crazy World is a 1963 film directed by Michael Carreras from a script by Carreras and Alan Klein, from the latters stage play.  An unemployed working class lad Alf Hitchens has an on-off relationship with his girlfriend Marilyn, whilst trying to sell a song he has written. Cheery pop musical featuring a number of late 1950s and early 1960s pop acts, including an appearance by Freddie and the Dreamers. Some comedy scenes are not politically correct from a modern perspective. Michael Ripper can be seen in several amusing cameo roles bemoaning the "bleeding kids" he encounters.

== Cast ==
*Joe Brown (singer) – Alf Hitchens
*Susan Maughan – Marilyn
*Marty Wilde – Herbie Shadbolt
*Harry H. Corbett – Sam Hitchens
*Avis Bunnage – Mary Hitchens
*Michael Ripper – The Common Man
*Monte Landis – Solly Gold
*Michael Goodman – Joey Hitchens
*Jessie Robins – Fat Woman
*Freddie and the Dreamers – Frantic Freddie and The Dreamers

On Network Video July 2014 with the original theatrical trailer.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 