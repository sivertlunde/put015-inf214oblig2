Arms and the Man (film)
{{Infobox film
| name           = Arms and the Man
| image          =
| caption        =
| director       = Franz Peter Wirth
| producer       = Peter Goldbaum Harry R. Sokal
| writer         = Eberhard Keindorff George Bernard Shaw Johanna Sibelius
| starring       = O. W. Fischer
| music          =
| cinematography = Klaus von Rautenfeld	
| editing        = Claus von Boro
| distributor    =
| released       =  
| runtime        = 100 minutes
| country        = West Germany
| language       = German
| budget         =
}} play by George Bernard Shaw. It was nominated for the Academy Award for Best Foreign Language Film.    It was also entered into the 1959 Cannes Film Festival.   

==Cast==
* O. W. Fischer – Hauptmann Bluntschli
* Liselotte Pulver – Raina Petkoff
* Ellen Schwiers – Louka
* Jan Hendriks – Leutnant Sergius Slivitzna
* Ljuba Welitsch – Katharina
* Kurt Kasznar – Petkoff
* Manfred Inger – Nicola
* Horst Tappert
* Hans Clarin

==See also==
* List of submissions to the 31st Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 
 