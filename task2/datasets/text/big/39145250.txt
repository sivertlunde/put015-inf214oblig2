Anjuman (2013 film)
{{Infobox film
| name           = Anjuman
| image          = Anjuman film.jpg
| alt            = 
| caption        = Theme Image
| director       = Yasir Nawaz
| producer       = Tarang Housefull
| writer         = 
| based on       =  
| editing        = Wajahatullah Khan
| studio         = Mastermind
| distributor    = Geo Films
| released       =
| runtime        = 110 minutes
| country        = Pakistan
| language       = Urdu
| budget         = 
| gross          = 
}}

Anjuman is a Pakistani romantic drama film directed by Yasir Nawaz. It is a remake of the same name released in 1970, starred by Waheed Murad and Rani (actress).  The film is produced by Tarang Housefull. Film stars Imran Abbas Naqvi, Sara Loren, Iffat Rahim, Alyy Khan and Sohai Ali Abro.  This movie was written by Zanjabeel Asim Shah.

In 2013, Anjuman won three Tarang Housefull Awards and was nominated for four categories.

==Plot==
The film is a tragedy based on a tawaif (courtesan) Anjuman, who born from a rich noble father and a courtesan mother. After her fathers death she and her mother was abandoned by her paternal family. Her mother works as a courtesan to bring her up and wishes oneday her daughter will join the same business like her. Anjuman hides her identity and tries to overcome her situation from higher education. She joins university with her family given name "Anjum Hayat Khan" and started a respectful normal life. She always dreams to leave "kotha" after her university education and want to change her ill fate. In the University, she secretly falls in love with Asif, a classmate from noble birth. Asif was already enganged to his cousin and going to marry her soon. One day, some university boys along with Asif, get to know Anjumans real identity during a dance show and they publish her reality in front of the whole university campus. Anjuman leaves university with humiliation for good and decides to join her mother dark profession. Suddenly, a wealthy guy comes to her life as a client and this guy in fact is the elder brother of Asif. In order to save his elder brothers marriage, Asif decides to visit Anjumans "kotha" in an "exchange" demanded by Anjuman. Asif initially hates Anjuman but later develop affection for her when he came to know because of him, Anjuman have to quit her study and join this profession. At the end, Asif tells everything to his family and decides to marry Anjuman to save her pride and give her a social respect.

== Cast ==
* Imran Abbas Naqvi as Asif
* Sara Loren as Anjuman
* Alyy Khan
* Iffat Rahim
* Sohai Ali Abro
* Farha Nadir

== Soundtrack ==
{{Infobox album  
| Name = Anjuman
| Type = Soundtrack
| Artist = Sunidhi Chauhan
| Cover =
| Genre = 
| Director =Yasir Nawaz Urdu
| Length =  
}}

Anjuman songs are sung by Sunidhi Chauhan, composed by Erphan Qayyum. 

{{Track listing
| headline = Tracklist
| extra_column    = Singer(s)
| total_length    = 
| title1          = Aap Dil Ki Anjuman Mein
| extra1          = Sunidhi Chauhan
| length1         = 5:23
| title2          = Dil Dharke Main Tum Se
| extra2          = Sunidhi Chauhan
| length2         = 5:38
}}

=== Awards ===
{| class="wikitable" style="width:100%"
! year !! Award !! Category !! Recipient(s) !! Result
|-
| rowspan="8" | 2013
| rowspan="8" | Tarang Housefull Awards
| Best Telefilm
| Yasir Nawaz
| rowspan="4"  
|-
| Best Actress in a Leading Role
| Sara Loren
|-
| Best Writer
| Zanjabeel Asim Shah
|-
| Best Song
| Dil Dharke Main Tum Se
|-
| Best Song
| Aap Dil Ki Anjuman Mein
| rowspan="4"  
|-
| Best Actor in a Leading Role
| Imran Abbas Naqvi
|-
| Best Supporting Actor (Male)
| Alyy Khan
|-
| Best Supporting Actor (Female)
| Iffat Rahim
|}

== References ==
 

 
 
 
 
 
 
 