London River
 
{{Infobox film
| name           = London River
| image                  = London River.jpg
| caption                = Theatrical poster
| writer                 = Olivier Lorelle Rachid Bouchareb
| starring               = Brenda Blethyn Sotigui Kouyaté
| director               = Rachid Bouchareb
| producer               = Rachid Bouchareb Jean Bréhat Bertrand Faivre Matthieu de Braconier
| Locataire appartement  = Salah Mohamed-Mariche	
| cinematography         = Jérôme Alméras
| editing                = Yannick Kergoat
| released               =  
| runtime                = 87 minutes
| country                = United Kingdom French
| music                  = Armand Amar
| budget                 = 
}}
London River is a 2009 British drama film, written and produced by France|Franco-Algerian film director Rachid Bouchareb. Starring Brenda Blethyn and Sotigui Kouyaté, it centres on the journey of two people searching for their children after the 7 July 2005 London bombings.

== Plot ==
In July 2005, British Protestant war widow Elisabeth Sommers (Blethyn), who is a Guernsey farmer, and Francophone African Muslim Ousmane (Kouyaté) are strangers who meet in London. She is searching for her daughter, and him for his son, following the London bombings. Neither is close to their missing child. They fear that the daughter and son were killed in the bombings. They discover that they were a couple who lived together in a flat in London who planned to travel to France (camembert land), but were killed by Hasib Hussain when the bus they were travelling on exploded in Tavistock Square.
 Finsbury Park, in particular Blackstock Road.

==Cast==
* Brenda Blethyn — Elisabeth Sommers
* Sotigui Kouyaté — Ousmane
* Roschdy Zem — Landlord
* Francis Magee - Inspecteur anglais
* Sami Bouajila - Imam
* Roschdy Zem - Le Boucher
* Marc Baylis - Edward
* Bernard Blancan - Ouvrier forestier
* Diveen Henry — Female inspector

==Critical reception==
The film premiered at the 59th Berlin International Film Festival on February 10, 2009.    

The film received mixed reviews. The   couldnt have done much better. This is a film no Londoner should miss: humane, stunningly acted, it will be a gross injustice if it doesnt win a prize from Tilda Swintons Berlin jury".   

Kaleem Aftab, writing in The Independent called London River the "most talked about film at the Berlin Film Festival", but argued that "it was only   Kouyates performance that lifted an otherwise dull and predictable film that avoided any meaningful discussion about the effect of the terrorist attack around which the story was shaped".   

Having seen the Berlin premiere, Variety (magazine)|Varietys Jay Weissberg stated that the film "trumpets political correctness far more loudly than this intimate drama can stand. Though the ending proves effective, Bouchareb and his co-scripters employ simplistic stereotypes and obvious counterpoints that shouldnt need to be spelled out so literally. Still, with its heart in the right place and the majestic presence of Malian thesp Sotigui Kouyate, the pic will get a decent international run before heading to its originally skedded home on the smallscreen".   
 Days of Glory) brings great sensitivity to the fictionalized tale, which goes a step beyond the obvious in its description of Englands multiracial society scarred by deep-seated prejudice but capable of change. Without glossing over the tales hard edges, the film ends on a positive note of ethnic tolerance that should make it more accessible to audiences".     

==Awards== Silver Bear Award for Best Actor at the 59th Berlin International Film Festival for his performance in the film.  The film also won a Special Mention by the Ecumenical Jury. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 