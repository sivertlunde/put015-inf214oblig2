Lawstorant
{{Film infobox title            = Lawstorant original title   =  podpis grafiki   =  type of the film = komedia kryminalna rok produkcji    =  Premiere  2005
 country          = Poland language  polski
 how long?        = 104 minuty director         = Mikołaj Haremski idea             = Tadeusz Porębski main actors Henryk Gołębiewski music            = Jacek Łągwa sponsor          = Edbud photos           = Zdzisław Najda scenery          = Natasza Eichelkraut-Galimska costiumes        = Natasza Eichelkraut-Galimska montage          = Jarosław Kamiński (montażysta)|Jarosław Kamiński sponsored by     = Edward Mazur (polski przedsiębiorca) Edward Gierwiałło
 |wytwórnia        =  distribution     = ITI Film Studio
 |budżet           =  poprzednik       =  kontynuacja      =  nagrody          =  www              = 
}}
 Edward Mazur

== Fabule ==

The film takes place in Warsaw. The protagonist of the film is Fragles (Michal Wisniewski) whos a gambler and is ruining his life - he was a trader -  and is forced to earn a large sum of money. He enters into business with Lawstorant, who planning life interest (in the role played by Zbigniew Buczkowski). Lawstorant, dreams of a beautiful arrangement of life, fascinated by the director of the bank. Fragles with Lawstorant lead a company with goal to make a fortune. Their plans crosses the head of a local criminal group - Caruso (Slawomir Orzechowski), demanding a share in the profits , and second Lawstorant asked the bank for a loan in the amount of four million dollars. Terrified man faces a dilemma to risk their lives or be gangsters, and thus implicate his woman to prison, to which it is bound. 

== Actors ==

* Zbigniew Buczkowski as Lawstorant
* Michał Wiśniewski as Fragles
* Jolanta Mrotek as Monika
* Jerzy Trela as Notary
* Sławomir Orzechowski as "Caruso"
* Ireneusz Czop as "Dags"
* Tomasz Sapryk as Kazio Traczyk Henryk Gołębiewski as Bawół
* Bohdan Gadomski as Lawstorants neighbour
* Małgorzata Socha as Jola
* Małgorzata Pieczyńska as Joanna Burska
* Joanna Liszowska as Marta
* Bohdan Łazuka as Sylwek
* Paweł Koślik as policeman
* Sławomir Sulej as policeman
* Sebastian Domagała as Lutek

== References ==

 

 
 