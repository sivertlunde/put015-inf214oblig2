The Sword and the Rose
{{Infobox film
| name           = The Sword and the Rose
| image          = The Sword and the Rose poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ken Annakin
| producer       = Perce Pearce Walt Disney
| writer         = Lawrence Edward Watkin (screenplay) Charles Major (novel "When Knighthood was in Flower")
| narrator       = 
| starring       = Glynis Johns James Robertson Justice Richard Todd Michael Gough Jane Barrett Peter Copley Ernest Jay Jean Mercure D. A. Clarke-Smith Gérard Oury Fernand Fabre Gaston Richer Rosalie Crutchley Bryan Coleman
| music          = Clifton Parker
| cinematography = Geoffrey Unsworth
| editing        = Gerald Thomas Walt Disney Productions
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 92 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = $1 million (US) 
| preceded_by    = 
| followed_by    = 
}}
 Mary Tudor, a younger sister of Henry VIII of England. It was Disneys third completely live-action film.
 When Knighthood Treasure Island (1950) and The Story of Robin Hood and His Merrie Men (1952).  In 1956, it was broadcast on American television in two parts under the original book title.

== Plot == Charles Brandon. She convinces her brother King Henry VIII to make him his Captain of the Guard. Meanwhile, Henry is determined to marry her off to the aging King Louis XII of France as part of a peace agreement. Marys longtime suitor the Duke of Buckingham takes a dislike to Charles as he is a commoner and the Duke wants Mary for himself. However, troubled by his feelings for the princess, Brandon resigns and decides to sail to the New World. Against the advice of her lady-in-waiting Lady Margaret, Mary dresses up like a boy and follows Brandon to Bristol. Henrys men find them and throw Brandon in the Tower of London. King Henry agrees to spare his life if Mary will marry King Louis and tells her that when Louis dies she is free to marry whomever she wants. Meanwhile, Mary asks the Duke of Buckingham for help but he only pretends to help Brandon escape from the Tower, really planning to have him killed while escaping. The Duke thinks he is drowned in the Thames, but he survives.
 Francis makes it clear that he will not return Mary to England after the kings death, but keep her for himself. When she goes to him for help, the Duke of Buckingham tells Lady Margaret that Brandon is dead and decides to go "rescue" Mary himself. Lady Margaret discovers that Brandon is alive and learning of the Dukes treachery they hurry back to France. Louis dies and the Duke of Buckingham arrives in France to bring Mary back to England. He tells her that Brandon is dead and tries to force her to marry him. Charles arrives in time, rescues her and kills the Duke. Mary and Brandon are married and remind Henry of his promise to let her pick her second husband. He forgives them and makes Charles Duke of Suffolk.

== Cast == Mary Tudor - Glynis Johns King Henry VIII -  James Robertson Justice
* Charles Brandon, 1st Duke of Suffolk -  Richard Todd Duke of Buckingham -  Michael Gough
* Lady Margaret  -  Jane Barrett
* Sir Edwin Caskoden -  Peter Copley
* Lord Chamberlain  -  Ernest Jay Louis XII  -  Jean Mercure
* Cardinal Wolsey  -  D. A. Clarke-Smith
* Dauphin of France  -  Gérard Oury
* DeLongueville  -  Fernand Fabre
* Antoine Duprat -  Gaston Richer Queen Katherine -  Rosalie Crutchley
* Earl of Surrey - Bryan Coleman Princess Claude - Helen Goss

==Production==
At the end of 1948, funds from Walt Disney Pictures stranded in foreign countries, including the United Kingdom, exceeded $ 8.5 million. Walt Disney decided to create a studio in Britain, Walt Disney British Films Ltd. or Walt Disney British Productions Ltd. in association with RKO Pictures and started production of Treasure Island (1950). With the success of Robin Hood and His Merrie Men (1952), Disney wanted to keep the production team to make a second film; he chose The Sword and the Rose inspired by the novel When Knighthood Was in Flower (1898) by Charles Major. This team consisted of the director Ken Annakin, producer Douglas Pierce, writer Lawrence Edward Watkin, and the artistic director Carmen Dillon. 

At the beginning of production, Annakin and Dillon went to Burbank, Disney Studios in order to develop the script and set the stage with storyboards, a technique used by Annakin on production of Robin Hood . During this step, each time a batch of storyboards was finished, it was presented to Walt Disney who commented and brought his personal touch. Annakin was granted great freedom with the dialogue.

Walt Disney came to oversee the production of the film in the UK from June to September 1952. The team spent several months researching period details to make the film more realistic. Working in pre-production had helped reduce the need for natural settings in favor of studio sets designed by Peter Ellenshaw. Ellenshaw painted sets for 62 different scenes in total. According to Leonard Maltin, Ellenshaws work was such that it is sometimes impossible to tell where the painting ends and reality begins.
 The Wonderful World of Disney. 

==Analysis==

Leonard Maltin surmised that The Sword and the Rose is historically equivalent to Pinocchio (1940) although it remains primarily a dramatic entertainment featuring costumed actors. However, it was greeted coolly in the UK mainly because of its historical approximations despite reviews from The Times that said that Mary had "remarkably alive moments" and James Robertson Justices King Henry had "a royal air". On the other side of the Atlantic in the United States the New York Times reviewed the film as "a time consuming tangle of mild satisfaction". Despite these criticisms, the team responsible for the film was reassembled for another film Rob Roy, the Highland Rogue. 

Peter Ellenshaws work on set allowed him to get a "lifetime contract" with the Disney studio. He moved to the United States after the shooting of 20,000 Leagues Under the Sea (1954 film)|20,000 Leagues Under the Sea (1954).

Douglas Brode draws a parallel between The Sword and the Rose and Lady and the Tramp (then in production) in which two female characters of noble lineage are enamored of a poor male character.

Steven Watts sees The Sword and the Rose and Rob Roy as showing the Disney studios concern for individual liberty fighting against powerful social structures and governments. He is joined in this opinion by Douglas Brode. Brode sees the film and the ball scene, not as a conservative, but as an incentive to "dance crazes" (as the   would similarly upset the American nation. 

==Historical inaccuracies== English attempts Eleanor Percy is eliminated entirely from the story. 
King Henry is portrayed as a middle aged and corpulent figure, although at the time he was only 23. His wife Catherine of Aragon is also shown as a brunette although she was a redhead. 

==Game==
An unrelated Amiga computer game has the same name.

==See also==
*Cultural depictions of Henry VIII of England

==References==
 

==External links==
*   
*  

 

 
 
 
 
 
 
 
 
 
 
 
 