Kabhi Khushi Kabhie Gham...
 
 
 
{{Infobox film name = Kabhi Khushi Kabhie Gham... image = KabhiKhushiKabhiGham_Poster.jpg caption = Theatrical release poster director = Karan Johar producer = Yash Johar story = Karan Johar screenplay = Karan Johar Sheena Parikh starring =  Amitabh Bachchan Jaya Bachchan Shah Rukh Khan Kajol Hrithik Roshan Kareena Kapoor music = Songs: Jatin Lalit Sandesh Shandilya Aadesh Shrivastava Background score: Babloo Chakravarty
|cinematography= Kiran Deohans editing = Sanjay Sankla studio = Dharma Productions distributor = Yash Raj Films released = 14 December 2001 runtime = 210 minutes    country = India language = Hindi budget =      gross =     
}}
 Sameer and Anil Pandey. The background score was composed by Babloo Chakravarty. The film tells the story of an Indian family, which faces troubles and misunderstandings over their adopted sons marriage to a girl belonging to a lower Social class|socio-economic group than them.

Development of the film began in 1998, soon after the release of Karans debut film Kuch Kuch Hota Hai (1998). Principal photography began on 16 October 2000 in Mumbai and continued in London and Egypt. Initially scheduled to release during the Diwali festivities of 2001, the film eventually released in India, United Kingdom and North America on 14 December 2001. In 2003, it became the first Indian film to be given a theatrical release in Germany. Upon release, the film met with mixed reviews from film critics and received polarising reactions to Karan Johars "larger-than-life" directorial style. Certain academicians criticised the portrayal of a "morally corrupt" British society against an "idealist" Indian society.

Made on a budget of  , Kabhi Khushi Kabhie Gham... emerged as a major commercial success, both domestically and internationally, with a lifetime gross of  . Outside India, the film was the highest grossing Indian film ever, until its record was broken by Karans next directorial, Kabhi Alvida Naa Kehna (2006). Kabhi Khushi Kabhie Gham... won several awards at popular award ceremonies the following year, including five Filmfare Awards.

==Plot==
Yashvardhan "Yash" Raichand (Amitabh Bachchan) is a rich businessman, living in India with his wife Nandini (Jaya Bachchan) and two sons Rahul and Rohan. Rahul is the elder son and was adopted by Yash and Nandini when he was first born. At the age of eight, he learns of this and as a result feels obliged to them. This truth is known to everyone in the Raichand household, except Rohan.

Years later, a grown-up Rahul (Shahrukh Khan) comes across the vivacious Punjabi-speaking Anjali Sharma (Kajol). The two become friends and eventually fall in love. However, their love is forbidden, because Anjali is from a working-class background. Yash and Nandini soon send Rohan off to a Boarding School, somewhere all the males in their family have attended. Yash announces his desire for Rahul to marry Naina (Rani Mukherji), the daughter of his closest friend. Naina learns however that Rahul is in love with Anjali, and encourages him to follow his heart. When Yash comes to know of Rahuls love for Anjali, he is enraged and hurt. Rahul promises his father that he will not marry Anjali. When he goes to tell her that he cannot marry her, he is shocked to discover that her father (Alok Nath) has died. Unable to see the woman he loves in pain, he decides to marry her despite his fathers hostility. Yash learns of the marriage and is furious. He disowns Rahul, saying he no longer considers him his son. Nandini and Rahul share a tearful goodbye. Nandini sends Sayeeda (Farida Jalal), Rahul and Rohans nanny, to watch over her son so that he will never feel apart from a mothers love. Rahul visits Rohan in the Boarding School and tells him that he is leaving home forever. He begs Rohan to never ask where he went or why he left and also asks that he take care of Nandini.
 Indian National Anthem. Later, Krish recites advice which Rohan had given him. Rahul had given Rohan this advice ten years back, so he now realises that Rohan is his brother.

Rohan begs Rahul to come back home, but he refuses as he believes his father will never accept him. Pooja convinces Rohan to invite his parents to London and have them meet Rahul, hoping this will encourage them to patch things up. Rahul and Nandini are overjoyed to see one another, however Rahul still refuses to talk to his father. Yash learns about Rohans true intentions and is livid. Soon, Yash receives a phone call and learns that his mother is on her death bed; her last wish being for Yash, Rahul and Rohan to light her funeral pyre together. Although Yash does not want this, the entire family attends the funeral together. Nandini now picks up the courage to tell Yash that she thought he was wrong for disowning Rahul, whom he had brought up with so much love. Rohan and Pooja now talk to Rahul and convince him to talk to Yash. Yash now acknowledges his mistake and shares tearful greetings with Rahul and allows him and Anjali to come back to the house. With the family reunited, Rohan and Pooja get married and the family also has a belated celebration of Rahul and Anjalis wedding.

==Cast==
* Amitabh Bachchan as Yashvardhan "Yash" Raichand, a New Delhi-based businessman. Fiercely dominating, he insists on controlling the life of his wife and sons. Amitabh Bachchan was Karans first choice for the portrayal of Yash Raichand. Karan added, "As I wrote the film, I realised that Yashvardhan Raichand is the backbone of the film and I could only see one actor playing the role&nbsp;— Amitabh Bachchan." Bachchan, on his part, agreed to do the film without a script narration.    Karan mentioned that he was initially scared to direct a star of the stature of Bachchan, but the latter "soon became an actor instead of a superstar". 
* Jaya Bachchan as Nandini Raichand, Yashs wife. She shares a close bond with her sons, but remains in the shadow of her husband. According to Karan Johar, Jaya Bachchan was the "obvious" choice for the character of Nandini, and added that her "acting prowess and stature" were the other reasons for him preferring to cast her.  The film also marked the return of Amitabh and Jaya Bachchan together on screen after a gap of 18 years.  Of her character, she said that Nandini was an extension of her own self. She elaborated, "My personal feelings towards Shah Rukh are similar to what I was supposed to portray in the film. Theres something about him that makes me want to mother him." She added that she modelled her character on Karans mother, Hiroo Johar, who "is a very emotional and sentimental person." 
* Shah Rukh Khan as Rahul Raichand, Yash and Nandinis adopted son. He feels indebted to his parents and tries to fulfill all their wishes. However, he falls in love with Anjali and invites the wrath of Yash. When Karan offered the role to Shah Rukh, he immediately agreed to do it and accommodated his dates, despite having several other commitments.  Khan described the character of Rahul by saying, "I love the vulnerability and the honesty in his eyes. He has the appeal of a boy next door. Besides, his intensity and ability to convey emotions without words is amazing."  Punjabi woman living in the Chandni Chowk area of Delhi. Belonging to a lower socio-economic group than the Raichand family, she is not accepted by Yash as his daughter-in-law. Karan was initially hesitant to cast Kajol in the film, as he felt that she would refuse the offer due to her recent marriage. Kajol, however, was moved to tears during the script narration and agreed to do the film.  In an interview to Filmfare, Kajol said, "One tiny fact that Karan forgot to mention during his narration was just how much Punjabi my character spoke in the film. I nearly died when I saw the lines of dialogue on the first day of shooting." However, she learnt the right pronunciation and diction with the help of Yash Johar and the crew members. 
* Hrithik Roshan as Rohan Raichand, Yash and Nandinis biological son. He wants his elder brother, Rahul, to return home. Karan signed Roshan to play the character of Rohan after watching a rough cut of the latters debut film, Kaho Naa... Pyaar Hai (2000).  Roshan described his character as a "buffer" in a film that primarily focused on Amitabh Bachchan and Shah Rukh Khan. 
* Kareena Kapoor as Pooja "Poo" Sharma, Anjalis younger sister. She is a sophisticated girl, who helps Rohan in his plan to reunite Rahul with his parents. After spotting Kapoor at a party organised by Bombay Times, Karan decided to cast her immediately for the role of the glamorous diva, Poo.  Kapoor stated that in her opinion, Kabhi Khushi Kabhie Gham... was primarily Kajols film, and that her own character was a supporting one. In order to prepare for her role, she worked hard on her dancing skills, as she didnt want audiences to know that she couldnt dance well.  Sony Music during the promotional activities led to her discovery.   
* Farida Jalal as Sayeeda / Daijan (DJ), Rahul and Rohans nanny. On the insistence of Nandini, she follows Rahul and Anjali to London.
* Simone Singh as Rukhsaar, Sayeedas daughter and Anjalis best friend
* Alok Nath as Bauji Sharma, Anjali and Poojas father
* Kavish Majumdar as the younger Rohan Raichand
* Malvika Raaj as the younger Pooja Sharma 
* Aryan Khan (Shah Rukh Khans real-life son) as the younger Rahul (special appearance)
* Jugal Hansraj as Rohans friend (special appearance)

Achala Sachdev and Sushma Seth were cast as Yash and Nandinis mothers, respectively. The film also featured Johnny Lever as Haldiram (a shopkeeper in Chandni Chowk), Himani Shivpuri as Haldirams wife, Jibraan Khan as Krish Raichand (Rahul and Anjalis son), Amar Talwar as Mr. Kapoor (Yashs friend and Nainas father). Ramona Sunavala, Jeroo Writer and Vikas Sethi feature as Poos friends Sonya, Tanya and Robbie, respectively. Additionally, Ashutosh Singh features as Ashfaque, Rukshaars husband. Shilpa Mehta, Shashikala and Parzan Dastur were cast as Ashfaques mother, grandmother and nephew, respectively.

==Production==

===Development===
 
After the success of Karans debut film, Kuch Kuch Hota Hai (1998), he began work on a story dealing with the concept of "generations". The idea initially revolved around two daughters-in-law. However, on the advice of filmmaker Aditya Chopra, who thought that the male characters would be too weak, Karan decided to tweak the story-line to make it the story of two brothers.   
 Kabhi Kabhie (1976). On being inspired by the classic, Karan quoted, "What appealed to me was the fact that the love story stretched out across generations. It began with youth and went on as the people grew older. You could say that Kabhi Kabhie is the starting point for my new film, that I am inspired by it. But the film, I am sure, will be very different. It will look different, feel different."  Similarly, Karan added an extra "e" to the second Kabhi in the title of his film, due to numerological reasons.  In an interview with The Times of India, Karan dispelled comparisons with Kuch Kuch Hota Hai and said that while his debut film was "frothy and bubble-gummish", this one was "more classy and sophisticated". He added that there would be "plenty of drama" in this film too, but handled more maturely. 

Before principal photography could begin, Karan and the contracted costume designers (Manish Malhotra, Shabina Khan and Rocky S.) shopped in several locations of United States|USA, London, Milan, and New Delhi to get the right look for each of the cast members.  Additionally, Karan had only one expectation from the contracted actors; to "look good and do their job". He did not organise any rehearsals for them, except for a scene involving a climatic encounter between Amitabh Bachchan and Roshan.  Additional production people hired included choreographer Farah Khan, production designer Sharmishta Roy and cinematographer Kiran Deohans. 

===Filming===
The first schedule of the film began in Mumbai on 16 October 2000, with the picturisation of the song "Bole Chudiyan" involving Roshan, Kapoor, Khan and Kajol. Amitabh and Jaya Bachchan joined the schedule on 20 October. Due to the immense stress caused by the presence of these actors, Karan fainted on the sets.  However, he continued directing the rest of the song while lying in bed. 

For the first half of Kabhi Khushi Kabhie Gham..., the production design team lead by Sharmishtha Roy, recreated Chandni Chowk in a studio at Film City of Mumbai.  In order to lend authenticity, the team took several pictures of the original area and also shopped in the various alleys of Chandni Chowk. Roy later won Filmfare Award for Best Art Direction her work.  The inside of a palatial mansion was developed from scratch in the same studio to double as the home of the Raichand family.  In order to lend authenticity to the house of the multi-multimillionaires, several expensive paintings were hung from the walls.  A total of 18–19 elaborate sets were constructed by Roy, as Karan wanted the look of the film to be "larger-than-life". 

  Bluewater Complex, Kent, Blenheim Palace, St Pauls Cathedral and the banks of River Thames.  The outdoor scenes of the Raichand family mansion were shot at Waddesdon Manor.  The crew faced enormous difficulties while filming an emotional scene between Jaya Bachchan and Khan at the Bluewater Complex, as a massive crowd had gathered there to watch them at work. The situation, eventually, got worse and the complex officials asked them to wrap up the shoot within two hours.   
 Pyramids of Giza in the city of Cairo in Egypt.  Due to the lighting conditions, the crew could shoot only between 7 and 9&nbsp;am in the morning. As a result, the song took several days to film.  In addition, Kajol suffered from a minor injury while filming for the song, as she had experienced a bad fall. 
 

==Soundtrack== Sony Music on 16 October 2001.    Explaining the album, Karan Johar said, "I wanted music that had all kinds of tunes&nbsp;— pop, romantic, bhangra (music)|bhangra – but one sound. It had to be larger than life." He added that Jatin-Lalit came up with three "haunting melodies", while Shandilya and Shrivastava came up with the pop and bhangra songs, respectively.  A legal suit was filed against Johar for using the song "Its Raining Men" in the film without obtaining prior permission. 

Upon release, the soundtrack of Kabhi Khushi Kabhie Gham... emerged as a major success by selling 2.5 million units within 30 days.  Additionally, it became the largest selling album of the year in India.  Writing for Rediff, Sukanya Varma praised most of the compositions, while being critical of the song "Say Shava Shava" due to the "overdose of Punjabi emotions". She summed up by saying, "The music of K3G has a presence. Hate it or love it, you certainly wont ignore it."  Planet Bollywood gave it 8 of 10 stars, calling "Suraj Hua Maddham" the best song, and the best reason to buy the album.  In 2002, Sony released another album titled Klub K3G, featuring remixes by Indian electronic music producers Akshai Sarin, Harshdeep Sidhu, Prempal Hans and others. 

{{track listing
|headline=Kabhi Khushi Kabhie Gham... Album: Track listing
|extra_column=Singer(s)
|music_credits=yes
|lyrics_credits=yes
|total_length=56:01
|title1=Kabhi Khushi Kabhie Gham
|extra1=Lata Mangeshkar
|music1=Jatin Lalit
|lyrics1=Sameer
|length1=7:55
|title2=Bole Chudiyan Kavita K. Subramaniam, Udit Narayan, Amit Kumar
|music2=Jatin Lalit
|lyrics2=Sameer
|length2=6:50
|title3=You Are My Soniya
|extra3=Alka Yagnik, Sonu Nigam
|music3=Sandesh Shandilya
|lyrics3=Sameer
|length3=5:45
|title4=Suraj Hua Maddham
|extra4=Alka Yagnik, Sonu Nigam
|music4=Sandesh Shandilya
|lyrics4=Anil Pandey
|length4=7:08
|title5=Say Shava Shava
|extra5=Alka Yagnik, Sunidhi Chauhan, Udit Narayan, Sudesh Bhonsle, Aadesh Shrivastava, Amitabh Bachchan
|music5=Aadesh Shrivastava
|lyrics5=Sameer
|length5=6:50
|title6=Yeh Ladka Hai Allah
|extra6=Alka Yagnik, Udit Narayan
|music6=Jatin Lalit
|lyrics6=Sameer
|length6=5:28
|title7=Kabhi Khushi Kabhie Gham&nbsp;— Sad (Part 1)
|extra7=Sonu Nigam
|music7=Jatin Lalit
|lyrics7=Sameer
|length7=1:53
|title8=Deewana Hai Dekho
|extra8=Alka Yagnik, Sonu Nigam, Kareena Kapoor
|music8=Sandesh Shandilya
|lyrics8=Sameer
|length8=5:46
|title9=Kabhi Khushi Kabhie Gham&nbsp;— Sad (Part 2)
|extra9=Lata Mangeshkar
|music9=Jatin Lalit
|lyrics9=Sameer
|length9=1:53
|title10=Soul of K3G
|extra10=Instrumental
|music10=Sandesh Shandilya
|lyrics10=
|length10=2:18
|title11=Vande Mataram
|extra11=Usha Uthup, Kavita K. Subramanium
|music11=Sandesh Shandilya
|lyrics11=Bankim Chandra Chattopadhyay
|length11=4:15
}}

==Release and reception==
Initially scheduled for a theatrical release during the Diwali celebrations of 2001, Kabhi Khushi Kabhie Gham... released a month later on 14 December.  Due to the long duration of the film, theatres screened three shows daily, instead of four. Additionally, due to a massive rush in advance bookings, several theatres increased their ticket prices. 

The use of "Jana Gana Mana" by Rabindranath Tagore during the film was met with criticism from a certain section of the audiences, and politicians of the Bharatiya Janata Party, for being "out-of-context" and "insulting the national pride".  Subsequently, a writ was issued against Dharma Productions in the Allahabad High Court by a petitioner based in Uttar Pradesh.  However, the court did not entertain the complainants petition. 

===Critical reception===

====India====
 
In India, Kabhi Khushi Kabhie Gham... met with polarised reactions from film critics. While certain critics praised the visual richness and the performances of the cast, certain others were negative about the lengthy run time and criticised the script strength and inconsistencies. Khalid Mohamed of The Times of India applauded the film and reviewed, "K3G is the complete commercial banquet delivered with fabulous finesse by Karan Johar. So, go indulge yourself. Cry your heart out and surprisingly, youll feel lifes finally alive and kicking in Mumbais dream world."  Taran Adarsh of Bollywood Hungama gave the film 4.5 out of 5 stars. He praised the emotional sequences, as well as the choreography, production design, costumes, and cinematography. He added that Karan Johar was the real star, for creating many memorable sequences.    Rakesh Budhu of Planet Bollywood gave the film 8 out of 10 stars, saying "Dharma Productions has kept its promise in giving us a lovable film to remember in coming times." He pointed out several flaws in the script, but added that the positive aspects of the film managed to outweigh the negative ones. He quoted, "K3G is one heck of an entertainer and was worth the wait".    In the film review section of his book Bollywood: An Insiders Guide, Fuad Omar showered overwhelming praise on the film and called it a "masterpiece from the first frame to the last". In summary he said, "Overall Kabhi Khushi Kabhie Gham... is without a doubt the most enthralling, entertaining, emotional and complete vision and definition of Hindi cinema I have ever seen. It is simply the perfect Hindi film." 

Contrary to the positive reviews, Anjum N., writing for Rediff.com|Rediff, said that despite an extraordinary cast and a big budget, "Karan Johar disappoints." He praised Amitabh and Jaya Bachchans performance and noted Hrithik Roshan for holding his own against the veteran actors. However, in summary he called the film "a bad remix of Mohabbatein and Kuch Kuch Hota Hai".    Writing for The Hindu, Ziya Salam praised Kajols performance and Karans ability to "keep the viewer occupied". She commented, "Watch Kabhi Khushi Kabhie Gham... not because of the hype which preceded its release but because in these meagre times not many have come up with better fare. The film at least partially redeems the hope surrounding it. Again, just like its name. Some joy, some disappointment."    Namarata Joshi of Outlook (magazine)|Outlook gave a mixed review and said that while the film "makes you laugh and cry alternately", the shenanigans were nevertheless "fake and affected" and "monochromatic despite the profusion of colours". 

====Overseas====
Overseas too, the reviews were mostly mixed, with several critics praising the technical production details of the film, while being somewhat less enthusiastic about the story line. Shamaila Khan of BBC gave the film 9 out of 10 stars and praised the performances of Khan, Kajol and Kapoor. She summed up by saying, "(K3G is) a well made film, with some magical moments (hilarious and weepy) and possibly the worlds best looking family!"   
Derek Elley of Variety (magazine)|Variety said that it "is a highly enjoyable, often dazzlingly staged vehicle dragged down by a sluggish final half-hour". He also praised the cinematography, and the picturisation of the song, "Say Shava Shava".    Corey K. Creekmur, of the University of Iowa, said that there were many ignored or illogical plot points and inconsistencies between the moral messages meant to be portrayed and the manner in which they came off on screen. Overall, he called the film a letdown.   

===Box Office===
Upon release, Kabhi Khushi Kabhie Gham... broke all opening records. The film opened to around   net collections in its first weekend in India, with the first week total at around  . The domestic opening week collections were 70% higher than the previous record and never before had opening records been eclipsed by such large margins. It also set new records for the second and third weeks, by collecting   and   respectively.    The film went on to become the second highest grosser of 2001 domestically, netting   in India, and earning "Blockbuster" status. 

The film was released in around 125 prints in the overseas markets, grossing a total of $8 million at the end of its theatrical run there.     It performed very well in the United Kingdom, with a gross of $689,000 in its opening weekend. It thus debuted at the third position at the British box-office.  The total earnings of the film reached over $3.2 million in the UK.  The film also had the biggest opening ever for a Bollywood film in North America, with a gross of $1.1 million in 73 screens.  However, according to a report by Rediff.com|Rediff, the numbers were so high that the official reporting agency did not believe it, and asked for evidence that could not be furnished until after the reporting deadline had passed. If reported on time, the film would have opened at the number 10 spot in the North American box-office.    However, according to figures from Box Office Mojo, the film debuted at the 32nd place at the American box office during the week of 4 January 2002.    It eventually gathered a total of $2.9 million there.  Additionally, in 2003, the film became the first from India to be given a theatrical release in Germany.   

Kabhi Khushi Kabhie Gham... earned a worldwide gross of  .  Internationally, the film was the highest grossing Indian film ever, until its record was broken by Johars next directorial, Kabhi Alvida Naa Kehna (2006). When adjusted for inflation, the film is the fifth highest grosser worldwide, with an adjusted gross of  .  Box Office India called it "one of the true worldwide blockbusters of Hindi cinema". 

===Accolades===
Kabhi Khushi Kabhie Gham... received fifteen nominations at the Filmfare Awards, ultimately winning five awards.   In an interview with Filmfare, Karan Johar said that he wasnt dejected to have not won too many awards at the ceremony, as he felt that Lagaan was "a classic" and deserved to win. 

The film won several awards at the International Indian Film Academy Awards (IIFA),   and some at the Zee Cine Awards  and Screen Awards  ceremonies, among others. Among the cast members, Kajol and Jaya Bachchan won several awards in the Best Actress and Best Supporting Actress category, respectively. At the 13th annual Valenciennes International Film Festival, the film won five major awards, including three Best Film awards and Best Actress for Kajol. 

{|class="wikitable"
|-
! Award
! Category
! Nominee
! Result
|- Filmfare Awards Filmfare Award Best Actress Kajol
|rowspan="5"  
|- Filmfare Award Best Supporting Actress Jaya Bachchan
|- Filmfare Award Best Dialogue Karan Johar
|- Filmfare Award Best Art Direction Sharmishta Roy
|- Filmfare Award Best Scene of the Year 
|Kabhi Khushi Kabhie Gham...
|- Filmfare Award Best Film
|Kabhi Khushi Kabhie Gham...
|rowspan="10"  
|- Filmfare Award Best Director Karan Johar
|- Filmfare Award Best Actor Shahrukh Khan
|- Filmfare Award Best Supporting Actor Amitabh Bachchan
|- Filmfare Award Best Supporting Actor Hrithik Roshan
|- Filmfare Award Best Supporting Actress Kareena Kapoor
|- Filmfare Award Best Music Direction Jatin Lalit
|- Filmfare Award Best Lyricist Anil Pandey ("Suraj Hua Maddham")
|- Filmfare Award Best Lyricist Sameer (lyricist)|Sameer ("Kabhi Khushi Kabhie Gham")
|- Filmfare Award Best Male Playback Sonu Nigam ("Suraj Hua Maddham")
|- Screen Awards Screen Award Best Actress Kajol
|rowspan="17"  
|- Screen Award Jodi No. 1 Shahrukh Khan & Kajol
|- Valenciennes International Film Festival Best Film (Special Jury Award)
|Kabhi Khushi Kabhie Gham...
|- Best Film (Audience Jury Award)
|Kabhi Khushi Kabhie Gham...
|- Best Film (Student Jury Award)
|Kabhi Khushi Kabhie Gham...
|- Best Actress Kajol
|- Bleu Nord Award Jatin Lalit, Sandesh Shandilya, Aadesh Shrivastava
|- International Indian IIFA Awards IIFA Award Best Supporting Actress Jaya Bachchan
|- IIFA Award Best Male Playback Sonu Nigam ("Suraj Hua Maddham")
|- IIFA Award Best Dialogue Karan Johar
|- IIFA Award Best Art Direction Sharmishta Roy
|- IIFA Award Best Background Score Babloo Chakravarty
|- IIFA Award Best Sound Recording Anil Mathur and Nakul Kante
|- IIFA Award Best Costume Design Manish Malhotra
|- IIFA Award Best Makeup Mikey Contractor
|- Zee Cine Awards Zee Cine Best Playback Singer&nbsp;– Male Sonu Nigam ("Suraj Hua Maddham")
|- Zee Cine Special Award for Outstanding Performance&nbsp;– Female Kajol
|}

==Analysis== Hindu audiences to participate in the Darśana|darshan along with the characters. 

Author Rajani Mazumdar compared Kabhi Khushi Kabhie Gham... to Hum Aapke Hain Koun..! (1994) and added that the film dealt with themes of family and moral values through a "spectacular stage that moves across global locations".  She further stated that the buildup to the story was juxtaposed with the backdrop of two contrasting places&nbsp;– the Raichand home and the interiors of Chandni Chowk. While the Raichand house is described as "expensive, almost like a museum", Chandni Chowk is shown as a world of crowds, chaos and festivities. She also made a note of the use of frontal camera angles in order to ensure that the "spectators eye travels throughout the interior expanse". 
 extradiegetic sequence that allowed Rahul and Anjali to be physically intimate "in ways that they could not in the real world of the film." He added that the characters conveyed a plethora of emotions not through extensive dialogue but through the exchange of glances, which were demonstrated by extreme close-ups on their eyes. 

Writing for the book Movie Blockbusters, Andrew Willis commented that the film was specifically written to appeal to the Indian diaspora. He explained that the film was aimed at invoking nostalgia among the large section of Non-resident Indian and Person of Indian Origin|NRIs in Canada, United Kingdom and North America.  In the second half of the film, Rahul and Anjali move to London, where they enjoy an affluent lifestyle, among several non-Indian neighbours and friends. However, there is a perpetual dissatisfaction among them, especially Anjali, in living away from home.  Additionally, she dresses up in a traditional sari and performs the duties of a loyal housemaker. She also frets about her son and younger sister being "too influenced" by Western culture. The film, thus tries to form an emotional connection with the expatriate Indian audiences.  

Certain academics, however, criticised the portrayal of Britishers as "morally corrupt". Several sequences convey a "culturally conservative" and "idealistic image" of India, while maintaining that the diaspora living in Britain lead a life of "involuntary exile". Western ideology is, however, equated with economic success, with emphasis on Western consumerism such as Starbucks and Burger King, while being critical of the society at large.  However, another academic observed that Rohan was the only character in the film who could navigate multiple cultural spaces with ease. He seems totally at ease both at his ancestral home in India and in London. Additionally, he strikes a chord with the diaspora by arranging for the local schoolchildren in London to sing "Jana Gana Mana", the Indian National Anthem.  Though the tagline for the film was "Its all about loving your parents",  some analysts noted that the film dealt with the concept of fathers trusting and loving their children. 

==Other media==
During the production and filming process, a book entitled The Making of Kabhi Khushi Kabhie Gham was written by Niranjan Iyengar. It features materials and interviews concerning the producer, director, photographer, art director, cast and crew that Iyengar gathered over an 18-month period during the production of the film. The book was released a few days prior to the theatrical release of the film.      
 theatrical trailer, and several television promos. 

==References==
;Footnotes
 
;Bibliography
* {{Citation
|surname1=Omar
|given1=Fuad
|year=2006
|title=Bollywood: An Insiders Guide
|publisher= Lulu.com
|ISBN=1847280099
}}.
* {{Citation
|surname1=Bhattacharya Mehta
|given1=Rini
|surname2=Pandharipande
|given2=Rajeshwari
|year=2010
|title=Bollywood and Globalization: Indian Popular Cinema, Nation, and Diaspora: Anthem South Asian Studies
|publisher= Anthem Press
|ISBN=1843318334
}}.
* {{Citation
|surname1=Mazumdar
|given1=Ranjani
|year=2007
|title=Bombay Cinema: An Archive of the City
|publisher= University of Minnesota Press
|ISBN=0816649421
}}.
* {{Citation
|surname1=Mazur
|given1=Eric
|year=2011
|title=Encyclopedia of Religion and Film
|publisher= ABC-CLIO
|ISBN=0313330727
}}.
* {{Citation
|surname1=Stringer
|given1=Julian
|year=2003
|title=Movie Blockbusters
|publisher= Routledge
|ISBN=0415256089
}}.
* {{Citation
|surname1=Hirji
|given1=Faiza
|year=2010
|title=Dreaming in Canadian: South Asian Youth, Bollywood, and Belonging
|publisher= UBC Press
|ISBN=0774817984
}}.
* {{cite journal
|last1=Punathambekar|first1=Aswin
|author1-link=
|title=Bollywood in the Indian-American Diaspora: Mediating a transitive logic of cultural citizenship
|journal=International Journal of Cultural Studies
|volume=8
|issue=2
|pages=151–173
|publisher=SAGE Publications
|location=
|date=
|year=2005
|url=http://www-personal.umich.edu/~aswinp/K3G_IJCS_June05.pdf issn =
|doi=10.1177/1367877905052415
|mr=
|zbl=
|ref=harv
}}
* {{Citation
|surname1=Eckstein
|given1=Lars
|year=2008
|title=Multi-Ethnic Britain 2000+: New Perspectives in Literature, Film and the Arts
|publisher= Rodopi
|ISBN=9042024976
}}.
* {{Citation
|surname1=Oonk
|given1=Gijsbert
|year=2007
|title=Global Indian Diasporas: Exploring Trajectories of Migration and Theory
|publisher= Amsterdam University Press
|ISBN=9053560351
}}.
 

==Further reading==
* {{Cite book
|surname1=Iyengar
|given1=Niranjan
|year=2002
|title=The Making of Kabhi Khushi Kabhie Gham
|publisher= Dharma Productions (in association with) India Book House, India
|ISBN=81-7508-338-7
}}

==External links==
 
*  
*   site at Dharma Productions
*   site at Yash Raj Films
*  
*  
*  
*  
*   at Bollywood Hungama Rediff

 
 

 
 
 
 
 
 
 
 
 