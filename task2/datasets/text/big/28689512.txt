A Girl of London
 
 
{{Infobox film
| name           = A Girl of London
| image          =
| caption        = Henry Edwards
| producer       =
| writer         = Douglas Walshe
| screenplay     =
| story          =
| based on       = Ian Hunter Harvey Braban G.H. Mulcaster
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = United Kingdom
| language       =
| budget         =
| gross          =
}} Henry Edwards Ian Hunter and Nora Swinburne.  Its plot concerns the son of a member of parliament, who is disowned by his father when he marries a girl who works in a factory. Meanwhile he tries to rescue his new wife from her stepfather who operates a drugs den. It was based on a novel by Douglas Walshe. 

==Cast==
* Genevieve Townsend ...  Lil Ian Hunter ...  Peter Horniman
* Harvey Braban ...  George Durran
* G.H. Mulcaster ...  Wilson
* Nora Swinburne ...  Vee-Vee
* Edward Irwin ...  Lionel Horniman
* Bernard Dudley ...  Lawton
* Nell Emerald ...  Mother

==References==
 

==Bibliography==
* Low, Rachael. The History of British Film, Volume 4 1918–1929. Routledge, 1997.

==External links==
*  

 

 
 
 
 
 
 
 
 
 


 