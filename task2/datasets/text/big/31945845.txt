Kamen Rider OOO Wonderful: The Shogun and the 21 Core Medals
{{Infobox film
| name           = Kamen Rider OOO Wonderful: The Shogun and the 21 Core Medals
| film name = {{Film name| kanji = 劇場版 仮面ライダーオーズ　WONDERFUL　将軍と２１のコアメダル
| romaji = Gekijōban Kamen Raidā Ōzu Wandafuru: Shōgun to Nijū-ichi no Koa Medaru}}
| image          = OOO-Gokaiger.jpg
| alt            =  
| caption        = Film poster for both Kamen Rider OOO Wonderful: The Shogun and the 21 Core Medals and  
| director       = Takayuki Shibasaki Toei
| writer         = Yasuko Kobayashi
| narrator       = Jōji Nakata Shu Watanabe Ryosuke Miura Riho Takada Asaya Kimijima Hiroaki Iwanaga Ken Matsudaira Miki Sakai
| music          = Kōtarō Nakagawa
| cinematography = Koji Kurata
| editing        =  Toei
| Toei Co. Ltd
| released       =  
| runtime        = 65 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| italic title = force
}}
  is the film adaptation of the 2010 & 2011  .  The catchphrases for the movies are   and  .

The Shogun and the 21 Core Medals features a trip to the past where the cast of Kamen Rider OOO meets up with Tokugawa Yoshimune (portrayed by Ken Matsudaira) to battle the powerful alchemist Gara (portrayed by Miki Sakai) while learning the origins of the Core Medals. The film will also introduce Kamen Rider OOOs  .   The film will also feature the debut of the lead character of Kamen Rider Fourze.

==Synopsis==
Leading an expedition in Thuringia, Germany, Kousei Kougami was unearthing the resting place of one of the First OOOs alchemist to obtain legendary Lost Medals. However, upon the seal being undone, millions of Cell Medals erupt into a tower while creating a magical barrier that causes it the entire sit to flip over to Japan. This attracts Eiji Hinos attention as he and Ankh investigate. Arriving as Knights emerge from the tower after it dragged Kougami and Erika Satonaka in, Akira Date arrives as Eiji and him become Kamen Riders OOO and Birth to battle the Knights. The fight eventually attracts the other Greeds attention before a new figure arrives and takes the Core Medals from all sides. Attempting to kill a boy that Kamen Rider OOO saves, the figure revealing his intent to become the New OOO of a new world before taking the Taka Medal and forcing everyone out with a barrier set up. Taking the boy to the Cous Coussier and learning his name to be Shun Wakaba, Eiji asks the boy why he was there before Shintaro Goto reveals their enemy to be known as Gara, one of the alchemists who created the Core Medals.  Following Ankh when he senses something like Gara, Eiji and company find a strange girl called Belle as she presents 5 million yen to obtain Cell Medals from those she tricks so Gara can use the weight of human desire to execute his doomsday plan.

Once enough Cell Medals are gathered, Garas magic device activates with Date and Shintaro watching Eiji and the others end up in the Edo era where he attempts to keep the peace between the present and past peoples. But without warning, the Nue Yummy appears and attacks Eiji. Unable to become OOO, Eiji uses the Birth Driver to become Kamen Rider Birth to fight the Nue Yummy before he takes Ankhs Kujaku and Condor Medals and takes his leave. Though the fight caused the past people to demonize Eiji and others, Tokugawa Yoshimune manages to calm everyone down.  Back in the present as another area of Japan flipped over for the Mezoanic Era, Kougami talks Gara into going after Eiji to use his desire to speed up the doomsday countdown. Later that night in Edo Japan, Eiji learns from Shun that his mother, Satsuki, was taken earlier when the tower appeared and become a vessel for Garas soul while assuring the boy that his mother loves him.

The next day, forced to use Ankhs main Taka Medal, Eiji becomes Kamen Rider OOO to fight the Nue Yummy and his platoon of Knights with Hina backing him up. Things seemed hopeless until Tokuda arrives to Kamen Rider OOOs aid along with the Edo residents. Once the Knights are dealt with, Yoshimune gives Kamen Rider OOO a set of Core Medals belong to the Tokugawa family. Using them, OOO becomes Kamen Rider OOO Burakawani Combo to destroy the Nue Yummy. As Yoshimune takes his leave, Belle arrives to offer Eiji the choice to return to his time with everyone else fading from existence. Though agreeing on the condition that he brings his "family", Eiji manages to trick Belle into saving everyone. As a shocked Belle explodes, Eijis infinite desire overloads Garas device to the alchemists horror as Eiji and friends return to their time.
 Kamen Rider Fourze. With Fourze declaring Kamen Rider OOO a friend, the two Kamen Riders double team Gara and knock him into his tower. Negating his transformation, Gentaro Kisaragi gives Kamen Rider OOO his handshake before running off to Amanogawa High School. However, managing to take Eijis purple Core Medals to complete the seven sets of Core Medals in his possession, Gara absorbs them to become Gara Dragon. Kamen Riders OOO and Birth are powerless against Gara until the Greeed arrive and Uva gives the former his Core Medals so he can assume Gatakiriba Combo. Then receiving more Core Medals from the Greeed, each Gatakiriba clone changes into Kamen Rider OOOs seven other Combo forms. With Births Breast Cannon, the eight OOOs use their Scanning Charges in synch to destroy Gara with the area raining Cell Medals. As Shun and Satsuki take their leave, Eiji talks Ankh into holding Hinas hand as she holds his hand as well. Remembering what he said, Eiji realizes that he, Hina, and Ankh are a family as they all walk home.

==Internet spin-off films==
To promote the movie, Toei released a series of Internet clips under the collective title  .

==Cast==
*  :  
*  ,  :  
*  :  
*  :  
*  :  
*  :  
*  :  
*  :   Hyper Hobby, October 2010 
*  :   
*  :    
*  :  
*  :   
*  :  
*  :  
*  ,  :  
*  :  
*  :  
*  :  
*  :   
*  :  
*  :  
*  :  
*  :  
*  :   Theatrical pamphlet 
*   
*   
*  :  
*  :  

===Suit actors===
* Kamen Rider OOO, Kamen Rider Fourze, Kamen Rider Birth (Eiji):   
* Kamen Rider Birth (Date):   
* Gara Inhumanoid Form, Nue Yummy:   
* Uva:   
* Kazari:   
* Gamel:   
* Mezool:   

==Songs==
;Theme song
* 
**Lyrics: Shoko Fujibayashi
**Composition & Arrangement: Shuhei Naruse Shu Watanabe & Ryosuke Miura) Matsuken Samba song series that blend traditional-style Japanese vocals with Brazilian samba music and use several Brazilian Portuguese words and phrases in the lyrics.

==References==
 

==External links==
*  &  
* 

 

 
 
 
 
 
 
 
 