A Place for Lovers
 
{{Infobox film
| name           = A Place for Lovers
| image          = Place for lovers.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Vittorio De Sica
| producer       = Arthur Cohn Herschell Gordon Lewis Carlo Ponti Peter Baldwin Ennio De Concini Tonino Guerra Cesare Zavattini
| starring       = Faye Dunaway Marcello Mastroianni
| music          = Manuel De Sica Lee Konitz
| cinematography = Pasqualino De Santis
| editing        = Adriana Novelli
| studio         = Compagnia Cinematografica Champion Les Films Concordia
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 88 minutes  
| country        = France Italy
| language       = English
}}
A Place for Lovers ( ,  ) is a 1968 French-Italian  . The film was released on DVD in 2009 by Atlantic Film AB.

==Plot==
The rich Julia is tired of living because she knows she is suffering from a malignant cancer. When the woman leaves for her last holiday in Cortina dAmpezzo, she meets the young and vital Valerio. The two fall in love instantly, but Julia does not reveal her secret to Valerio. When Valerio finds out that she is sick and dying, he decides to pretend to know nothing, continuing his love affair with Julia to the end.

==Cast==
* Faye Dunaway as Julia
* Marcello Mastroianni as Valerion
* Caroline Mortimer as Maggie
* Karin Engh as Griselda
* Yvonne Gilbert as Marie
* Mirella Pamphili as Party guest
* Esmeralda Ruspoli as Attorneys wife
* Enrico Simonetti as Party entertainer
* David Archell
* Martha Buckman

==Soundtrack==
 .

==Reception==
The film opened to generally negative reviews. Roger Ebert of the Chicago Sun-Times called it the "most godawful piece of pseudo-romantic slop Ive ever seen!", and Charles Champlin of the Los Angeles Times referred to it as "the worst movie I have seen all year and possibly since 1926."  Mark Deming wrote in the New York Times that Dunaway and Mastroianni were romantically involved during the filming of Amanti but that little of their personal chemistry can be seen in the movie.  Years later, A Place for Lovers was included as one of the choices in the book The Fifty Worst Films of All Time.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 