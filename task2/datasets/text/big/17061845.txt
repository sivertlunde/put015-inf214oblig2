Business and Pleasure
{{Infobox film
| name           = Business and Pleasure
| image	         = Business and Pleasure FilmPoster.jpeg
| caption        = Film poster David Butler
| producer       = Al Rockett
| writer         = William M. Conselman Arthur F. Goodrich Booth Tarkington Gene Towne
| starring       = Will Rogers Jetta Goudal
| music          = Ernest Palmer
| editing        = Fox Film Corporation
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
}}
 David Butler, starring Will Rogers and featuring Boris Karloff.

==Plot==
Earl Tinker (Will Rogers) goes on a Mediterranean cruise and finds that a business rival has a femme fatale in pursuit.

==Cast==
* Will Rogers as Earl Tinker
* Jetta Goudal as Madame Momora
* Joel McCrea as Lawrence Ogle
* Dorothy Peterson as Mrs. Tinker
* Oscar Apfel as P.D. Weatheright
* Vernon Dent as Charles Turner
* Boris Karloff as Sheik
* Mitchell Lewis as Hadj Ali
* Jed Prouty as Ben Wackstle
* Cyril Ring as Arthur Jones
* Peggy Ross as Olivia Tinker

==See also==
* List of American films of 1932
* Boris Karloff filmography

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 