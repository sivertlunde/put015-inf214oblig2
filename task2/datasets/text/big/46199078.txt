All the Sad World Needs
{{Infobox film
| name           = All the Sad World Needs 
| image          =
| caption        =
| director       = Hubert Herrick 
| producer       = 
| writer         = Kenelm Foss 
| starring       = Lauri de Freece   Joan Legge   Lennox Pawle 
| cinematography = 
| editing        = 
| studio         = British Actors Film Company
| distributor    = Stoll Pictures
| released       = August 1918
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Hubert Herrick and starring Lauri de Freece, Joan Legge and Lennox Pawle. 

==Cast==
*   Lauri de Freece as Peep ODay  
* Joan Legge as Rhoda Grover 
* Lennox Pawle as George Grover  
* Adelaide Grace as Miss Flint  
* Cyprian Hyde as Ernest Hanbury  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 
 
 
 
 
 
 

 