Enakkul Oruvan (2015 film)
{{Infobox film
| name           = Enakkul Oruvan
| image          = Enakkul Oruvan poster.jpg
| alt            =  
| caption        = Enakkul Oruvan
| director       = Prasad Ramar
| producer       = C. V. Kumar Pawan Kumar Siddharth Deepa Sannidhi Srushti Dange
| music          = Santhosh Narayanan
| cinematography = Gopi Amarnath
| editing        = Leo John Paul
| studio         = Thirukumaran Entertainment, Abi TCS Studios
| distributor    = Y Not Studios, Radiance Media, Dream Factory 
| released       =  
| runtime        = 136min
| country        = India
| language       = Tamil
| budget         =  
| gross          =   
}} Tamil psychological Pawan Kumar, Siddharth and Deepa Sannidhi in the lead roles, with Santhosh Narayanan composing the films music. The film was released on March 6, 2015. The film received positive reviews. The movie didnt witness a strong opening at the box office despite Siddarths presence and ended up being a below average earner at the box office.

==Plot==
The plot is non-linear, and the end scene of the film shows the real beginning of the story.The plot starts with the protagonist being in a state of coma and continuing only on life support. The film, from the beginning, tells two stories of the same person, one in colour and the other in black and white. 

A detective from Mumbai crime branch starts investigating the incident that caused the protagonists (Siddharth (actor)|Siddharth) current state. The Detective going through his belongings stumbles upon some scribbled notes and a mysterious pill. Meanwhile police capture two suspects and interrogate them for information.

The story starts with Vicky who is from a village and working as an Usher/“torch-shiner” in a movie theatre owned by Durai (Aadukalam Naren). Vicky suffers from insomnia. On one of his sleepless nights Vicky is contacted by a drug dealer who gives him Lucia pills as a solution to his sleeping problems. This drug is said to have the capability to help one dream the life they want but has the side-effect, that on discontinuation the same dreams shall turn into a nightmare.

With the help of the Lucia pills, Vicky starts seeing himself as being a successful actor called Vignesh surrounded by people he knows in the real world. Film actor Vigneshs life is depicted in black-and-white. Durai, the theater owner where Vicky works, had directed a film in his youth produced by a thug, who now wants Durai to sign off his theatre for his debt. In the black-and-white life, Vignesh and Durai gets a number of death threats from a person. More events from the dream are followed by similar events in reality as Vicky falls in love with Divya (Deepa Sannidhi) who is a model in the dream and a waitress in reality.

In the dream, Vignesh and Divya hit it off easily at the beginning but later friction arises when Vignesh expresses his discontent on Divya working in film industry. Meanwhile in reality, Vicky struggles to win Divya’s heart, who rejects him first due to his low salary and falls later for his humble character. Divya’s attempts to get Vicky educated and lead him to earn a better job turns out to be futile and also results in death of Durai in the hands of goons. Frustrated, Vicky chooses to leave Divya and continue to work in the theatre, despite the threats. Vicky redecorates the theatre and releases Durai’s film to the audience. He later reconciles with Divya, who agrees to marry him.

In the dream, Durai is kidnapped, and Vignesh rescues him with the help of police. In the process, all of the goons are either arrested or killed except for the contract killer. Vignesh feels Durai is not safe with him and decides to fire him. Vignesh breaks up with Divya and sends off all his servants so as to be alone. Later, a visually disturbed Vignesh finds himself in a thrashed room. Vignesh gets hold of the torch and through its light sees the “torch-shiner” Vicki inspecting the screen of the theatre.

Divya misses Vignesh and tries to get back with him but Vignesh is in his own shell and shuts out everyone from his life. Divya receives an unexpected call from Vignesh some days later and he asks her to meet him. He tells her that he cannot decide if the present moment is a dream or reality but he is ultimately happy. He says that he should not confused his dreams and real life. Asking Divya to close her eyes, Vignesh jumps off the roof to the confusion of everyone present, including the contract killer waiting to kill him.

At present, the detective with the help of the suspects, performs a sting operation on the drug dealer and learns more about the Lucia pills. Meanwhile, Divya is caught in an attempt to kill Vignesh by pulling him off life support. During her interrogation, the whole investigating team along with Divya watches a re-run of an interview of the film star Vignesh. In the interview, Vignesh expresses his dissatisfaction of the stardom and reveals that he is actually color blind from an accident in his childhood. He tells the interviewer that he dreams of being a normal person and in fact in one of his dreams he is a “torch-shiner” who gets no attention from people and is happily in love with the dream of his life.
 
The detective is now convinced that Vignesh is living in his own versions of reality and chose to be in his dream over the real world by attempting suicide. Divya cries near a comatose Vignesh in the hospital and simultaneously Vicki is shown happily married to Divya and have a daughter. Vigneshs health begins to deteriorate and the doctors attempt to defibrillate him. Simultaneously, Vicki, aware and alive in his dream world finds everything disappearing around him and is killed by a shot to the head. Vignesh wakes up from his coma to see the astonished doctors and Divya and Durais relieved faces.

== Cast == Siddharth as Vicky / Vignesh
*Deepa Sannidhi as Divya
*Aadukalam Naren as Durai
*Srushti Dange
*John Vijay
*Yog Japee Mahadevan
*Ajay Rathnam as Police
*Ramdoss Uday Mahesh
*Amit Bhargav
* Vettri

==Production== Pawan Kumar, Siddharth to play the lead role in the film, while newcomer Prasad Ramar, co-writer of Pizza (2012 film)|Pizza was assigned the task of directing the venture.  
 1984 film of the same name.  

==Soundtrack==
The soundtrack was composed by Santhosh Narayanan, who had worked on the film score of the original film Lucia (2013 film)|Lucia. The soundtrack features five tracks while lyrics were written by Muthamil, Ganesh Kumar and Vivek. The album was launched on 8 September 2014 with actors Samantha Ruth Prabhu, Kushboo and Vijay Sethupathi being present at the event.

The album received very positive reviews upon release, with critics lauding the composers consistency.  The score too was praised. IBN Live wrote, "Santhosh Narayanan is the centerpiece of the film. What a marvelous job he has done! His music is an emotion in itself. For a puzzle like film of this kind, his music is pitch-perfect. It removes the clutter from the screenplay. The best thing Lucia missed out on is Santhosh Narayanans music".  Rediff wrote, "The unconventional music that is the trademark of Santhosh Narayanan is perfect for this intriguing half dream and half realistic plot". 

*Prabhalamagave - Siddharth
*Endi Ippadi - Santhosh Narayanan
*Poo Avizhum - Pradeep Kumar
*Kuttipoochi - Manikka Vinayagam
*Yaar - Dhibu Ninan Thomas

==Release== Zee Thamizh. 

==Critical reception==
The Times of India gave 3.5 stars out of 5 and wrote, "This film doesnt quite capture that films (Lucia) ambitiousness and visual pizzazz but still makes for a solid effort, especially for a debut film".  Baradwaj Rangan wrote, "It’s a thrillingly mad conceit, a Möbius-strip movie with a superb slap-on-the-forehead twist. Imagine the little boy in Cinema Paradiso all grown up and starring in a Christopher Nolan head-scratcher written by Charlie Kaufman, and you’ll have something like Enakkul Oruvan. Prasad Ramar...sticks fairly close to the template of the original but a lot is lost in the execution...Enakkul Oruvan lacks the intimate texture and delicacy of Lucia. When you open up a niche, crowd-sourced film into something more mainstream, you are going to get more… “commercial.” Watching Enakkul Oruvan, you’re left with the feeling of waking up from a dream and coming face-to-face with harsh Kollywood reality".  Indo-Asian News Service gave 3 stars and wrote, "While Enakkul Oruvan mostly remains faithful to the original, it lacks its indie spirit and somehow still feels commercial. The film wouldve worked with someone with no image and stardom, but Siddharth doesnt disappoint".  Deccan Chronicle gave the same rating and wrote, "Despite few shortcomings, (Enakkul Oruvan) culminates into a climax that may possibly leave you melted and concentrated. It is the apotheosis of the intense".  Rediff gave 3 stars out of 5, calling it "an engaging and admirable effort by the debutant director, definitely worth watching".  Sify wrote, "Enakkul Oruvan is a bold and unique attempt with sharp performances and a tight script, it is 136 mints well spent. It is sans the normal commercial aspects for a Tamil movie and the icing on the cake is the intelligent screenplay by Pawan Kumar which is well supported by top notch cinematography and music". 

==Box Office==
The movie grossed 9 Crores worldwide with 6 Crores coming from Tamil Nadu, 2 Crores from the rest of India, and 1 Crore from various overseas markets.  The movie witnessed an average opening  and ended up being a below average earner at the box office. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 