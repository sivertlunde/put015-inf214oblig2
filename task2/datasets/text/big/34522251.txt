Convict 99 (1919 film)
{{Infobox film
| name           = Convict 99
| image          = 
| caption        = 
| director       = G. B. Samuelson 
| producer       = G. B. Samuelson
| writer         = Robert Leighton Marie Connor Leighton
| starring       = Wee Georgie Wood Daisy Burrell Wyndham Guise 
| music          = 
| cinematography = 
| editing        = 
| studio         = G. B. Samuelson Productions
| distributor    =  
| released       =  
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
  magazine dated May 10–17, 1919]]
 silent motion picture of 1919 produced and directed by G. B. Samuelson and starring Daisy Burrell, C. M. Hallard, Wee Georgie Wood, and Wyndham Guise. It was written by Robert Leighton and Marie Connor Leighton.

A comedy, the film consists of six reels.  It premiered at a Trade Show in April 1919. 

Convict 99 was at the Gaiety Cinema, Singapore, in February 1920, when it was advertised as -  

It was shown in Singapore again in November 1921, this time at the Empire. 

==Cast==
* Wyndham Guise  – Mr Lucas (mill owner)
* Daisy Burrell – Geraldine Lucas (mill owners daughter)
* Wee Georgie Wood – James (office boy)
* Tom Coventry  – Hewett (time-keeper)
* Ernest A. Graham – Lawrence Gray (manager)
* C. M. Hallard – Ralph Vickers (assistant manager) 
* Arnold Bell – Warder Gannaway

==Notes==
 

==External links==
*  at BFI Film & TV database

 

 
 
 
 
 
 
 
 