Mondo Keyhole
{{Infobox film
| name           = Mondo Keyhole
| image          = Mondo_Keyhole.jpg
| image_size     = 
| caption        =  John Lamb
| writer         = 
| narrator       = 
| starring       = Nick Moriarty John Lamb
| music          = The Psychedelic Psymphonette
| cinematography = Jack Hill
| editing        = Jack Hill
| studio         = Lamb-Garden
| distributor    = Art Films International
| released       = 1966
| runtime        = 70 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Mondo Keyhole is a 1966 film directed by Jack Hill in his directorial debut. A sexploitation drama, it covers themes such as the pornography business, heroin|drug-taking, rape, martial arts and revenge. 

==Plot==
Howard Thorne is a rapist in Los Angeles: he meets women at work and at parties or he sees them walking down the street, and he follows them, terrifies them, and assaults them. He also dreams about these assaults, and hes unclear how much of what hes done is real and how much is fantasy. He ignores his heroin-using wife, Vicki, who tries everything she can think of to get his sexual attention. Howard and Vicki go separately to a costume party where she learns the full truth about his nature and where he is stalked by one of his recent victims. Individualized versions of Hell await Howard and Vicki.

==Cast==
*Nick Moriarty as Howard Thorne
*Adele Rein as Vicky
*Cathy Crowfoot as The Crow
*Carol Baughman as Carol
*Christopher Winters as Vampire

==References==
 

==External links==
* 

 

 
 
 
 
 

 