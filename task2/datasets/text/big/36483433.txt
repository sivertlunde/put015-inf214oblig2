Divergent (film)
 
{{Infobox film
| name = Divergent
| image = Divergent.jpg
| image_size = 
| border = Yes
| alt = Lead characters Tris and Four stand above a futuristic Chicago.
| caption = Theatrical release poster
| director = Neil Burger
| producer = {{Plain list | 
* Douglas Wick
* Lucy Fisher
* Pouya Shabazian
}}
| screenplay = Evan Daugherty Vanessa Taylor
| based on =  
| starring = {{Plain list |
* Shailene Woodley
* Theo James
* Ashley Judd
* Jai Courtney Ray Stevenson
* Zoë Kravitz
* Miles Teller
* Tony Goldwyn
* Maggie Q
* Kate Winslet 
}}
| music = Junkie XL Hans Zimmer (score producer)
| cinematography = Alwin H. Küchler
| editing = {{Plain list |
* Richard Francis-Bruce
* Nancy Richardson
}}
| studio = {{Plain list |
* Red Wagon Entertainment
* Summit Entertainment
}}
| distributor = {{Plain list | Lionsgate
* Summit Entertainment
}}
| released =  
| runtime = 139 minutes  
| country = United States
| language = English
| budget = $85 million  
| gross =  $288.7 million   
}} 2014 American science fiction novel of Ray Stevenson, Zoë Kravitz, Miles Teller, Tony Goldwyn, Ansel Elgort, Maggie Q and Kate Winslet.   The story takes place in a dystopian post-apocalyptic version of Chicago  where people are divided into distinct factions based on human virtues. Beatrice Prior is warned that she is Divergent and thus will never fit into any one of the factions and soon learns that a sinister plot is brewing in her seemingly perfect society.

Development of Divergent began in March 2011 when Summit Entertainment picked up the film rights to the novel with Douglas Wick and Lucy Fishers production company Red Wagon Entertainment. Principal photography began on April 16, 2013 and concluded on July 16, 2013, with reshoots taking place from January 24–26, 2014. Production mostly took place in Chicago.

Divergent was released on March 21, 2014 in the United States. The film received mixed reviews from critics, with praise going to its solid performances, but criticized for its generic execution and predictable, unoriginal plot with several critics making negative comparisons to The Hunger Games (film series)|The Hunger Games, which shares a similar story and characters. Despite this, the film was a financial success as it reached the #1 spot at the box office during its opening weekend. Since its release, the film has grossed over US$288 million worldwide against its budget of US$85 million.   It was released on DVD and Blu-ray Disc|Blu-ray on August 5, 2014.   

A sequel,  , was released on March 20, 2015 in the United States. 

==Plot==
In a futuristic dystopian Chicago, the society is divided into five Factions (Divergent)|factions: Abnegation (the selfless), Amity (the peaceful), Candor (the honest), Dauntless (the brave), and Erudite (the intelligent). The Factionless have no status or privilege in society. When children reach the age of 16, they are subject to a serum-induced psychological test which recommends their best-suited faction, then are allowed to choose any faction as their permanent faction at the subsequent Choosing Ceremony.
 Ray Stevenson). Beatrice takes her test with a Dauntless woman named Tori (Maggie Q) as her proctor. Her results show attributes of multiple factions, meaning she is Divergent. Tori records her results as Abnegation and warns her to keep the true result a secret, telling her that since Divergents can think independently and the government therefore cannot control them, they are considered threats to the existing social order.

The next day at the Choosing Ceremony, Beatrices brother Caleb (Ansel Elgort) chooses Erudite, while after some hesitation, Beatrice chooses Dauntless. After the ceremony, Beatrice meets Christina (Zoë Kravitz), Al (Christian Madsen), and Will (Ben Lloyd-Hughes), three other initiates from other factions who also chose Dauntless. The initiates commitment and fearlessness are immediately tested, and Eric (Jai Courtney), a brutal Dauntless leader, makes it clear that anyone who does not meet the high expectations will be expelled from the faction. Beatrice is the first to volunteer for a leap of faith from a tall building into a dark hole, and when Four (Theo James), a transfer initiates instructor, asks her name, she decides to shorten it to "Tris".

Tris initially struggles in Dauntless training, ranking far below the cutoff after the first evaluation, but slowly improves. Eric matches her against her nemesis - Peter (Miles Teller) in a fight, though Tris is soundly defeated and ends up in the hospital. When she discovers that she will miss the final test, she breaks out of the hospital and joins the rest of the initiates, secures her teams victory, and makes the cut.

In the next phase of training, the initiates are subject to psychological simulations where they face their worst fears. Triss divergence allows her to excel at the tests, defeating them in unusual ways, but Four warns her to hide her abilities and solve the challenges as a normal Dauntless.

Tris visits her brother Caleb, who tells her that Erudite is planning to overthrow Abnegation and become the ruling faction. On her return to Dauntless headquarters, Tris is attacked by Al and two others before being rescued by Four. The next day Al pleads with Tris for her forgiveness but she refuses and calls him a coward. He later kills himself rather than face failure.

To prepare her for her final test, Four takes Tris into his own fear simulations, where she learns that his real name is Tobias and he is the son of Marcus Eaton. After the simulation, they kiss. Tris then passes her test and is officially initiated into the Dauntless. The rest of the Dauntless are then injected with a serum supplied by Erudite which is supposedly for tracking, but is actually for mind control.

The next morning, the Dauntless prepare to execute the orders of the Erudite. Divergents are unaffected by the new serum, so Tris must blend in to avoid suspicion. She finds Four, who reveals himself to be a Divergent. While the Dauntless move to raid the Abnegation, Tris and Four separate from the group and attempt to locate Triss parents, but Eric sees that Four is not under control and captures the two. Four is taken into custody while Tris is ordered to be executed. Her mother Natalie (Ashley Judd) appears and saves her but is shot and killed as they try to escape.

Tris finds her father in hiding with Caleb, Marcus, and several Abnegations. The group sneaks into the Dauntless headquarters, where Tris encounters Peter and forces him to lead them to Erudites control center. Her father sacrifices himself in a shootout, and Tris goes in alone to find Four, who is now under stronger mind control and attacks her. Using her knowledge of his fears, she manages to wake him from the mind control and the two enter the central control room, where Erudite leader Jeanine (Kate Winslet) is about to have the Dauntless army execute the entire Abnegation faction. Tris uses a sample of the mind control serum on her to force her to cancel the program. The group escapes the compound and boards a train out of the city.

==Cast==
  portrays Beatrice Prior.]]
 
  Beatrice "Tris" Prior Tobias "Four" Eaton Natalie Prior Eric Coulter Ray Stevenson Marcus Eaton Christina
* Peter Hayes Andrew Prior Caleb Prior Tori Wu Max
* Jeanine Matthews Will
* Al
* Molly Atwood
 

==Production==

===Pre-production===
In March 2011, Summit Entertainment picked up the rights for Divergent with Douglas Wick and Lucy Fishers production company Red Wagon Entertainment. 
Neil Burger was announced as the director on August 23, 2012.  Evan Daugherty, who co-wrote the screenplay with Vanessa Taylor, said, "I get hung up on the toughness of the movie but of equal importance is the love story between Tris and Four. Its inherently and inextricably linked to Triss character journey. There will be plenty of sexual tension and chemistry, but its important that all of that stuff doesnt just feel like its thrown in, but that it all helps Tris grow as a character."  Daugherty further added, "Its tricky because the book is a very packed read with a lot of big ideas. So, distilling that into a cool, faithful two-hour movie is challenging. Not only do you have to establish five factions, but you have to acknowledge that theres a sixth entity, which is the divergent, and you also have the factionless. So theres a world that really has to be built out for the big screen… the movie is going to do it a little more efficiently."  Author Veronica Roth said about the script of the film: "Reading a script is a really interesting experience. Id never read a script before. I was really impressed by how closely it stuck to the general plot line of the book." 
 The Hunger Games. Analyst Ben Mogil said, "Divergent is more similar to Hunger Games in that the company owns the underlying economics (i.e. production) and the budget (at $80  ) is more manageable." 

===Casting===
  was cast as Tobias "Four" Eaton, winning the role over several other considered actors.]]
On October 22, 2012 it was announced that Shailene Woodley had landed the lead role of Beatrice "Tris" Prior.  Lucas Till, Jack Reynor, Jeremy Irvine, Alex Pettyfer, Brenton Thwaites, Alexander Ludwig and Luke Bracey were all considered for the role of Tobias "Four" Eaton.   On March 15, 2013, it was announced that Theo James had been cast as Four.   

Though James was 10 years older than the character when cast, Roth praised his casting: "I was sure within seconds: this was Four, no question. Theo is able to capture Fours authority and strength, as well as his depth and sensitivity." She also mentions the chemistry between him and Shailene: "He is a perfect match for Shailenes incredibly strong presence as Tris. Im thrilled!"  The producers said about his casting: "We took our time to find the right actor to fill the role of Four, and Theo is definitely the perfect fit. Veronica has crafted a truly iconic character in Four and we cannot wait to begin production and bring him and this story to life for millions of fans around the world." 
 Ray Stevenson, Jai Courtney and Aaron Eckhart were announced to be in talks to join the cast on March 15, 2013.  Stevenson and Courtney joined the cast as Marcus Eaton and Eric.  That same day, Miles Teller was cast as Peter.  Kate Winslet was announced to be in talks on January 24, 2013.  Later, it was confirmed that she would portray Jeanine Matthews.  Talking about playing the negative character for the first time, Winslet said, "Im no idiot. The idea went through my head that I have never played a baddie before, I was almost kind of surprised." Since Winslet joined filming late, she used that distance from her co-stars to appear aloof on the first day of her shoot. "I wanted to break it and say, Its OK, Im really fun. I promise. But I thought, just for today, Id let them think that I am a complete bitch."    Winslet was five months pregnant during the shooting of the film and her pregnancy was concealed using various props. According to Burger, "We had to be very strategic in the way that we shot her. She always had some sort of file or case in her hand that was sort of protecting, you know, her bump."   On March 25, 2013, Ben Lamb was cast as Edward, Ben Lloyd-Hughes as Will, and Christian Madsen as Al.  In April 2013, Ashley Judd and Tony Goldwyn joined the cast as Natalie and Andrew Prior, Beatrices parents.  

===Filming===
 .]]
 17th Church L tracks Michigan Avenue.  Scenes were also filmed in the University of Chicagos Joe and Rika Mansueto Library. 
 Wells Street in downtown Chicago.  In late June, filming took place at 1500 S Western Avenue and 600 S. Wells Street, Chicago.  In the last schedule, filming moved to the Chicago River and continued at Lake Street Bridge, Wacker Drive and LaSalle Street Bridge.  Filming wrapped on July 16, 2013.    On January 24, 2014, additional filming began in Los Angeles, finishing two days later, which officially became the last day of shooting. 

===Post-production===
Post-production work began after filming wrapped.  On July 18, Summit and Lionsgate issued a joint statement announcing the film would be released in IMAX format: "Were delighted to continue our successful collaboration with IMAX, with whom we have already partnered on the global blockbuster Hunger Games and Twilight Saga franchises, and were especially pleased that we can introduce our newest young adult franchise, Divergent, to the movie going public in the premium IMAX format that celebrates its status as a special and memorable event." 

==Music==
 
The score for Divergent was composed by Junkie XL and executive produced by his longtime mentor Hans Zimmer. Randall Poster served as the films music supervisor.   The original soundtrack was released on March 11, 2014, while the Original Score of the film was released on March 18, 2014, by Interscope Records.  

==Distribution==
===Marketing===
The first image of Shailene Woodley as Beatrice "Tris" Prior was revealed by Entertainment Weekly on April 24, 2013.  A few seconds sneak preview footage of the film was shown at the 2013 Cannes Film Festival.  On June 7, Entertainment Weekly released a still of Theo James (Four) showing the Dauntless initiates around their new headquarters.  The magazine released several more stills on July 19.  On July 16, USA Today released the first image of Kate Winslet as Jeanine Matthews. 

On July 18, 2013 Summit held a sold-out San Diego Comic-Con panel in Hall H. Shailene Woodley, Theo James, Maggie Q, Zoe Kravitz, Ansel Elgort, Ben Lloyd-Hughes, Amy Newbold, Miles Teller, Christian Madsen, director Neil Burger, and author Veronica Roth attended the panel and answered fan questions, along with the showing of exclusive film clips. 
 MTV Video trailer on November 13, 2013.   On February 4, 2014 Shailene Woodley and Theo James released the final trailer for the film during their appearance on Jimmy Kimmel Live!. 

The marketing campaign for the film cost at least $50 million. 

===Release===
On its first day of advance ticket sales, the film sold half of its tickets. 

===Home media=== Peter Hayes Edward was released on July 30, 2014. 

==Reception==

===Box office===
Divergent earned $150,947,895 in North America, and $137,800,000 in other countries, for a worldwide total of  $288,747,895.  Calculating in all expenses, Deadline.com estimated that the film made a profit of $71.87 million. 

On its opening weekend, the film attained the No. 1 spot in domestic and worldwide box office rankings by Rentrak.   The film grossed $4.9 million in late night screenings, on Thursday March 20, 2014.    On its opening day, the film grossed $22.8 million in the United States (including the Thursday night gross).    Divergent accumulated $54,607,747 from 3,936 theaters at an average of $13,873 per theater, on its opening weekend in the United States and Canada    and grossed $1.7 million internationally from four territories, with a worldwide total of  $56,307,747.      

===Critical response===
 Divergent received mixed reviews from critics. Review aggregator   gives the film a score of 48 based on reviews from 38 critics, indicating "mixed or average reviews". {{cite web | url = http://www.metacritic.com/movie/divergent| title=Divergent: Reviews (2014) | publisher = CBS Interactive | work = Metacritic | accessdate = 2014-03-17 Harry Potter The Hunger Games. Andrew Barker of Variety Film Reviews|Variety said, "Unlike the “Harry Potter” series’ tangible, fully dimensional Hogwarts or “The Hunger Games’” colorfully variegated districts, “Divergent’s” vision of new Chicago doesn’t have much to distinguish it from a standard-issue post-apocalyptic pic."  Peter Travers of Rolling Stone wrote that, "At least The Hunger Games spawned two terrific movies and a breakthrough star in Jennifer Lawrence. Onscreen, Divergent ignites only indifference."  However, Peter Debruge of Variety considered it a much better adaptation writing that, "  it shares a fair amount of DNA in common with “The Hunger Games,” it ranks as far superior." 
 The Playlist, "Woodley makes for more than uncertain enough of a hero to add detail and meaning to the implosion of this world", adding that "theres little artifice to her performance, and the mundane honesty of her reactions create a believability that the world would otherwise lack." Speaking of Jamess performance, Gilchrist adds that he "manages the considerable accomplishment of seeming like a real grown-up man" and that he "makes the characters transformation from hardass to collaborator seem natural, if inevitable".    Drew McWeeny at HitFix said, "it helps that   got Woodley and James in the leads.   like a walking empathy battery, wide-open emotionally, easy to read and enormously appealing", also adding that James is "incredibly natural onscreen". 
 The Hunger At the Movies gave praise to director Neil Burger, stating that he "handles the action with aplomb". 

The Playlists Todd Gilchrist gave it a mixed review saying that it has "great ideas ... and some terrific character work, but its given such uneven attention, alternately languished upon and glossed over".  IGNs Matt Patches gave it 5.8 out of 10, citing its similarities to other young adult films and contrived Dauntless plot line. He praised lead actors Theo James and Shailene Woodleys performances, judging that they "add personality and physicality to the limp script theyre acting out".  Scott Mendelson of Forbes magazine echoed these sentiments, arguing that despite Woodleys excellent performance, "the generic story reduced a large portion of the mythology to irrelevancy". Mendelson believed that the film would please the novels fanbase. 

===Accolades===
{| class="wikitable sortable"
|+ List of awards and nominations
! Year
! Award
! Category
! Recipients
! Result
|- 2014
| MTV Movie Awards Favorite Character
| Beatrice "Tris" Prior
|  
|- Teen Choice Awards 
|  
| Divergent
|  
|-
|   Theo James
|  
|-
|   Shailene Woodley
|  
|- Choice Movie Villain Kate Winslet
| 
|- Choice Breakout Star Ansel Elgort
| 
|- Choice Breakout Star Theo James
| 
|- Young Hollywood Awards
| Fan Favorite Actor – Male Theo James
|  
|-
| Fan Favorite Actor – Female Shailene Woodley
|  
|-
| Best On-Screen Couple Shailene Woodley/Theo James
|  
|-
| Best Cast Chemistry – Film
|Divergent
|  
|- Favorite Flick
|Divergent
|  
|- 2015
| Broadcast Film Critics Association Award  Best Actress in an Action Movie
| Shailene Woodley
|  
|- 41st Peoples Choice Awards|Peoples Choice Awards 
| Favorite Action Movie
| Divergent
|  
|-
| Favorite Action Movie Actress
| Shailene Woodley
|  
|-
| Favorite Movie Duo
| Shailene Woodley/Theo James
|  
|-
|}

==Sequels==
 
On May 7, 2013, Summit Entertainment revealed that a sequel based on Insurgent (novel)|Insurgent was in the works. Brian Duffield, writer of Jane Got a Gun, was hired to write the script.  The sequel is scheduled to be released on March 20, 2015. The third film, based on the novel Allegiant (novel)|Allegiant, was initially announced as a single film scheduled for release on March 18, 2016.   In December 2013, it was announced that Neil Burger will not return to direct the sequel, due to him still working on the first film.  On February 11, 2014, it was announced that Robert Schwentke would take Burgers place for the next installment.    Principal photography for The Divergent Series: Insurgent began in Atlanta on May 27, 2014  and concluded on September 6, 2014. 

On April 11, 2014, Summit Entertainment announced that the third film would split into two films titled The Divergent Series: Allegiant – Part 1 and The Divergent Series: Allegiant – Part 2, with Part 1 scheduled to be released on March 18, 2016 and Part 2 scheduled to be released on March 24, 2017.  On December 5, 2014, it was announced that Robert Schwentke would return to direct Part 1. 

==References==
 

==Readings==
*  // Summit Entertainment ( )

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 