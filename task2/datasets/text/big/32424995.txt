Haunted (1991 film)
{{Infobox film
| name           = Haunted (1991)
| image          = Haunted Poster.jpg
| alt            =
| caption        = Original theatrical release poster
| director       = John Magyar Warren Chaney (2nd Unit Director) Darlene Staffa (Assistant Director)
| producer       = Warren Chaney Beverly Wilson (Executive Producer) David Sanders (Associate Producer)
| screenplay     = Warren Chaney
| starring       = Gabe Caravello Hunter Lee Hughes Parrish Nelson Quincy Starnes  Trevor Tellepsen James Gale
| music          = Ted Mason
| cinematography = Craig Bailey
| editing        = Connie Schell Herbert Craig
| studio         = Sandpiper Productions
| distributor    = Intercontinental Releasing Corporation Metro-Goldwyn-Meyer
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $2,700,000 (estimated) 
| gross          = $6.7 million
}}

Haunted is a children’s comedy-horror film produced by Sandpiper Productions in 1989 and 1990 and released in 1991. It was written and produced by Warren Chaney and directed by John Magyar. Beverly Wilson was the Executive Producer for the film, which stars Gabe Caravello, Hunter Lee Hughes, Parrish Nelson, Quincy Starnes, Trevor Tellepsen and James Gale.  

Haunted tells the story of five children playing hooky from school who become unexpectedly trapped in a haunted castle where they encounter Dracula, the Werewolf, Hunchback and Frankensteins monster.  Turner Classic Movies (Overview)    

The picture was shot on location in  , the movie was once more re-released in 1996 and then faded away.  Internet Movie Database (Distribution)    

==Plot==
  in Sandpiper Productions HAUNTED (1991)]]
 gothic mansion as the Frankensteins monster and a hunchbacked figure unload a coffin from a parked black hearse. The hunchback accidentally drops the coffin in the mud causing muffled cursing to emanate from the coffin’s interior.

Across town within a city’s dark streets a woman takes a shortcut through an alleyway. Suddenly, she hears footsteps behind her and quickens her pace, eventually breaking into a run. She stumbles, falling against wet garbage before colliding with a tall-cloaked figure.

Nearby, two teenagers Reggie Helsley (Steve Chizmadia) and Jessica Stuart (Nicole Feenstra) hear the woman’s scream and rush to help. Upon arriving, they see nothing but as they leave, a lifeless arms drops unseen across a fallen garbage bin.
 

In the early morning hours, three children wait with parked bikes at the entrance to the old mansion. Doc (Hunter Lee Hughes), Skeet (Gabe Caravello), and Curly (Trevor Tellepsen), are joined by two other children, Speck (Quincy Starnes) and Angela (Parrish Nelson). The children discuss plans to sneak away from school during a planned fire drill, meet up again at the old house and spend the night. Each has convinced their parents they are staying overnight with a friend.

A shadowed figure watches from the darkness of the upper windows as the children exit on their bikes. A car pulls into the driveway and Realtor, John Helsley (Jack Adams) steps from the vehicle to exchange his “For Sale” sign with one that says, “Sold”. A second vehicle arrives and a very agitated dark-haired man (James Gale) leaps out. Learning the house has sold, the man screams hysterically, causing the realtor to quickly toss his signs into the trunk and drive away. From the window, unseen hands close the drapes.

The five young truants slip away from school during the fire drill while across town – the dark-haired man rushes into a police station demanding to be locked up. He claims that he will change into a werewolf with the evening’s full moon. The police ignore him whereupon he become violent and is locked in a cell. Meanwhile the children having entered the old mansion find themselves unexpectedly locked in and unable to escape. Soon they encounter the hunchback and Frankensteins monster.

With no way out, the children bravely separate to search for exits. Skeet accidentally falls through a hidden entrance into a cavernous-like room where he discovers a coffin adjacent to a stack of recently signed real estate documents. Skeet learns that Curly’s Dad was the realtor who sold the old house to a Count Vlad Drakulya. Abruptly the candlestick crashes to the floor and the coffin swings open. Count Vlad steps from the coffin motioning for Skeet to come forward. Vlad transforms into a large bat, causing Skeet to run hysterically through the dark halls.
  in Sandpiper Productions HAUNTED (1991)]]

The sounds of a howling wolf echo as the full moon rises over the city jail. The police chief and his assistant rush to the cellblock where the dark-haired man was incarcerated only to discover the prisoner’s cell door ripped from its hinges and their prisoner gone.

Elsewhere in the old mansion, the other children escape the hunchback and rejoin Skeet who has escaped the flying bat. Skeet relates his misadventures and Doc, the most knowledgeable of the gang, informs them that Vlad is the real name for Dracula. The Frankensteins monster discovers them causing them to flee. However, they abruptly run into the dark-haired man. The man tries to help the children escape, but is soon confronted by the hunchback. As they struggle, the man transforms into a werewolf who throws off the hunchback and begins stalking the children.

Reggie discovers a note written by his little brother. In the note, Curly tells Doc of the plans to spend the night in the haunted mansion. Jessica, whose brother is missing as well, joins Reggie as they rush to the old manor in search of the missing children.

The Frankenstein’s monster nearly captures Skeet and at the same time the hunchback discovers the others. All rush up an old staircase only to encounter Dracula blocking their passage. The vampire revolves to face the werewolf who rushes headlong toward him. Dracula raises his cape and transforms into a bat as the werewolf leaps over the stairwell snatching the bat from the air and crashing to the floor below.  The monster reaches for Skeet and stumbles, falls, and bounces down the long winding stairway. As the children recoil, Curly’s and Jessica burst through large French doors leading from the outside into the large ballroom where the children are cornered.

The Frankensteins monster rushes Reggie but screams in pain at the sounds of gunfire. Curly’s dad – the realtor, John Helsley, stands at the entrance with a large hunting rifle. As the creature staggers, Helsley fires repeatedly. The monster crashes to the floor as the father spins and fires his weapon, dropping the clubfooted hunchback wildly swinging an axe at the children. Curly rushes into his fathers arms, who explains that he was carrying the hunting rifle the brothers gave him for Father’s Day.

A huge bat settles behind Helsley, transforming into Dracula. The vampire’s long arm sweeps the man and boy aside like feathers. The creature takes several quick steps and seizes Jessica. The twang of an archery bow reverberates as Dracula screams in anguish and twists around with a wooden arrow protruding from his back. The vampire collapses to the floor and crumbles into dust.

Sirens signal arriving police whom the father had phoned on his way to the old manor. Helsley exits with his sons along with Doc and Speck. Skeet and Angela turn to leave when something springs at Skeet causing him to faint. Regaining consciousness Skeet is startled once more as a small black cat leaps onto his chest. Skeet’s eyes widen and he faints again.

Angela nods her head and quips, “My hero!”

A bat flies through the doorway toward an upper window as the picture fades to black for the final credits.

==Cast==
 
* Gabe Caravello … Skeet
* Hunter Lee Hughes … Doc
* Parrish Nelson … Angela
* Quincy Starnes … Speck
* Trevor Tellepsen  … Curly
* James Gale …  the Wolf Man
* Nicole Feenstra … Jessica
* Steve Chizmadia … Reggie
* David Markwarkdt … Count Dracula
* David Sanders … The Monster
* Jack Adams … Mr. Helsley
* Tana Herrington … woman in alley
 
* John Magyar … date
* Mark Sevier … police Chief
* Jim Bolinger … desk clerk
* David Folwell … deliveryman
* Nuccia Pignataro … Woman of “ill-repute”
* Interrogating Officer … Chief Mike Barry
* Mark Gerald … policeman
* Jay Jackson … policeman
* Debbie McLaughllin … bystander in police station
* Lisa Florence … bystander in police station
* Maria McCullough … bystander in police station
* Laura Goulas … bystander in police station
 
* Matt Bennett … bystander in police station
* Cecilia Bennett … bystander in police station
* Jimmy Smith … bystander in police station
* Archie Bennett … bystander in police station
* Michele Deguerin … bystander in police station
* Shelly Newman … bystander in police station
* Phil Hardage … bystander in police station
* Julie Brownlee … bystander in police station
* Sherri Atkins … bystander in police station
* Cara Wedel … bystander in police station
* Rene Bennett … bystander in police station
* Carol Ann Smith … bystander in police station
 

==Reviews==
 
Reviews were mostly favorable. Lawrence Sailes writes in a Turner Classic Movie Review, “Surprisingly for an overworked genre, Haunted is most enjoyable – even for an adult.”  However James Packard, writing for Silver Screen Reviews says, “…the film is so enjoyable that one wishes the production had a greater budget together with a more experienced director.” He went on to say, “There is a lot that is up there on the screen and the picture is very entertaining.” 

==Production==
Haunted was the second of a series of children’s films aimed at what Sandpiper Productions had estimated would be a future television market for 3-D films.  Although the 3-D process developed by Ashai, Tec, Ltd. was reported to be excellent for television, the filming process was burdensome. Each film setup took many hours and hundreds of takes in order to achieve a stabilized 3-D picture. The new camera systems weighed approximately 800 pounds and required a five-person camera team to operate.  

Haunted is recognized as the film debut for future film star, Hunter Lee Hughes. Hughes would later star in  such films as Winner Takes All, Armenia, Project: X, and Tweakers.  

The film was also the film debut for two more future actors, Steve Chizmadia and Nicole Feenstra. Chizmadia portrayed Curly’s brother, Reggie and Feenstra was Reggie’s girlfriend, Jessica. Chizmadia would later act in I Come in Peace and Celebrity Ghost Stories while Feenstra would appear in Dark Room Theatre and The Indictment.  

Haunted marked actor James Gales second entry into film but his first starring role as the films Wolfman. He would go on to leading roles in many films including The Return of the Texas Chainsaw Massacre, Good Girl, Bad Girl and The Grand Inquisitor. 

==Distribution==

Haunted (1991 film) was distributed by Intercontinental Releasing Corporation (IRC) in 1991 and for reasons unexplained, once again in 1993.  The picture was later acquired for continuing distribution by Metro-Goldwyn-Meyer in 1998.  

==References==
 

==External links==
* Internet Movie Database: "Haunted" (1991)  
* Turner Classic Movies Haunted (1991)  
* Yahoo! Movies Haunted (1991)  
* Haunted (1991) opening footage located at  

 

 
 
 
 
 
 
 
 
 
 
 
 