One or the Other of Us
{{Infobox film
| name           = One or the Other of Us Einer von uns beiden
| image          = Einer von uns beiden.jpg
| caption        = Film poster
| director       = Wolfgang Petersen
| producer       = Luggi Waldleitner Ilse Kubaschewski
| writer         = Manfred Purzer based on the novel by -ky (Horst Bosetzky)
| starring       = Klaus Schwarzkopf Jürgen Prochnow Elke Sommer Ulla Jacobsson
| music          = Klaus Doldinger
| cinematography = Charly Steinberger
| editing        = Traude Krappl
| distributor    = Roxy Film Divina-Film
| released       =  
| runtime        = 105 minutes
| country        = West Germany
| language       = German
| budget         = 
| gross          = 
}}

One or the Other of Us ( ) is a 1974 West German film directed by Wolfgang Petersen. It was Petersens first theatrical feature film, and was based on the novel of the same name by Horst Bosetzky, published anonymously under his pseudonym -ky. The film is a psychological thriller and focuses on the intense conflict between a university professor and a blackmailer. The film features Klaus Schwarzkopf and Jürgen Prochnow as the two main characters and won two Deutscher Filmpreis|Bundesfilmpreise.

==Plot==
  and Klaus Schwarzkopf]] plagiarized his DM and additional monthly payments of 1,500 DM. Kolczyk initially agrees, but vows to Ziegenhals that "only one of us will survive". Looking for a way to fight back, Kolczyk tries to gather information about Ziegenhals from Miezi (Elke Sommer), a prostitute and a housemate and friend of Ziegenhals. As Miezi has saved enough money she is thinking of leaving her profession. At the same time her violent ex-boyfriend and former pimp, Kalle Prötzel (Claus Theo Gärtner), has been released from prison; he murders her and steals her savings. During the investigations the police finds out that Miezi had an appointment with Kolczyk and discovers the payments Kolczyk makes to Ziegenhals. Being the main suspects in the murder investigation only increases the hatred between Kolczyk and Ziegenhals, who fight with all means available. Kolczyk, learning that Ziegenhals thinks that Kolczyk wants to kill him, starts playing with Ziegenhals mind. For example, he sends him a package with an alarm clock, making Ziegenhals think that he has received a mail bomb. Ziegenhals meanwhile befriends Kolczyks daughter Ginny (Kristina Nel) and makes her his lover. Although this forces the both to have to pretend in public that they are friends, the final disaster cannot be averted.

==Cast==
* Klaus Schwarzkopf as Professor Rüdiger Kolczyk
* Jürgen Prochnow as Bernd Ziegenhals
* Elke Sommer as Miezi
* Ulla Jacobsson as Reinhild Kolczyk
* Kristina Nel as Ginny Kolczyk
* Anita Kupsch as Sekretary Beate Blau Walter Gross as Grandfather Melzer
* Fritz Tillmann as Dr Sievers
* Berta Drews as Mother Braats
* Claus Theo Gärtner as Kalle Prötzel

==Production and distribution==
One or the Other of Us was Petersens first theatrical feature film. Petersen had already directed several high profile films for television, including four episodes for the  .   

The film made its theatrical debut on 22 February 1974 at the Gloria-Palast in Berlin. It was first showed on television on 12 February 1982 on ZDF. The film was released on VHS in 1998 and on DVD in 2008 in Germany. 

==Awards and honors==
One or the Other of Us won two Bundesfilmpreise in 1974: 

* Filmband in Gold for Best New Director (Wolfgang Petersen)
* Filmband in Gold for Best Cinematography (Charly Steinberger)
 official submission Best Foreign Language Film, but did not manage to receive a nomination.  

==See also==
* List of submissions to the 47th Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

==Bibliography==
*  

==External links==
*  
*   at    

 

 
 
 
 
 
 
 
 