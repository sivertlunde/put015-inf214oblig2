Dr. Alien
{{Infobox film
| name           = Dr. Alien
| image_size     =
| image	         = Dr. Alien FilmPoster.jpeg
| caption        =
| director       = David DeCoteau
| producer       = David DeCoteau John Schouweiler
| writer         = Kenneth J. Hall Billy Jacoby
| music          = Reg Powell Sam Winans
| cinematography = Nicholas Von Sternberg
| editing        = Tony Malanowski
| distributor    = Paramount Home Video (VHS) Full Moon Entertainment (DVD)
| released       =  
| runtime        = 90 min
| country        = United States English
| budget         =
| gross          =
| website        =
}} Billy Jacoby, and Olivia Barash. Directed by David DeCoteau, the film was also released under the titles I Was a Teenage Sex Maniac and I Was a Teenage Sex Mutant. The alternative titles explain the movies plot.

The films plot centers on an unpopular honors student named Wesley Littlejohn (Billy Jacoby), who becomes involved in an experiment headed by his new, sexy biology teacher, Ms. Xenobia (Judy Landers). As part of the experiment, Wesley becomes a chick magnet whenever a phallic-like stalk emerges from his head. However, this threatens to alienate the girl he really cares about, Leanne (Oliva Barash), and Wesley begins to suspect Xenobias motives, which may have something to do with the fact that she isnt from Earth.

==Plot==
Dr. Ackerman (Troy Donahue) is the high school biology teacher.  Leaving school one night he crashes his car after encountering a UFO.

Wesley Littlejohn is a geek who is in Dr. Ackermans class, along with his opportunistic friend Marvin (Stuart Fratkin).  Wesley is the stereotypical geek who is bullied at school and relies on Marvin to defuse situations with the bullies.  He is also in love with a girl named Leanne, but is too shy to make any kind of approach to her.

Instead of Dr. Ackerman teaching biology that day, a sexy, blond substitute teacher named Ms. Xenobia arrives and informs the class she will be teaching for the time being.  All the males in the class are stunned at having such a sexy woman as a teacher, apart from Wesley.  Looking for a participant in an experiment, all the males in class volunteer but Ms. Xenobia selects Wesley and asks him to attend after school has finished.

Wesley goes back to the class after school, where he encounters Drax, Ms. Xenobias assistant (Raymond OConnor).  Ms. Xenobia arrives and explains that the experiment involves being injected with a vitamin supplement and observing Wesleys reaction.  Wesley does not want to participate and Ms. Xenobia agrees, before injecting him with the "supplement" when his back is turned.  A phallic-like stalk starts to emerge from Wesleys head as a result. Becoming sexually excited by the sight of the stalk,  Ms. Xenobia proceeds to remove her lab coat, revealing a white lingerie underneath. She then has sex with Wesley (who is oblivious to his surroundings) with Drax recording the findings.  After the "experiment" Wesley is sent home.

At home Wesleys parents and brother start to see changes in him. However,  He becomes more confident and assured, which allows him to get closer to Leeane.  However, after the experiment, there is a tendency for the stalk to emerge at the most inconvenient times, usually when there are women around.  This leads to several sexual encounters, notably with a former bullys girlfriend and the female gym teacher Buckmeister (Edy Williams).  Each time these encounters happen, Wesley is left with amnesia and he starts to suspect Ms. Xenobia has given him something more than a vitamin supplement.  Challenging Ms. Xenobia on the experiments, he is injected again with the serum by Drax and again has sex with Ms. Xenobia.

Away from school, Wesley becomes the singer of a heavy metal band called the Sex Mutants (before he played classical piano). He also goes on a date with Leanne, although it is interrupted by the stalk which leads Leanne to become passionate with him before "waking up" and accusing Wesley of trying things on too fast. Wesley protests his innocence later on and invites her to a gig that the Sex Mutants are playing at.
 Ginger Lynn Allen and Linnea Quigley).  After the show has finished, Wesley is backstage with the Poon Tangs when Leanne arrives.  Shocked that Wesley appears to be flirting with the girls, she runs off and Wesley follows her.  Outside the venue, Wesley encounters Ms. Xenobia, who is wearing a silver spacesuit. Ms. Xenobia informs Wesley that she and Drax are aliens from the planet Alterion who had been experimenting on him to help their race, as the males of their species lost the ability to procreate and Ms. Xenobia was tasked with finding the cure.  With humans having a similar biology to her species, she and Drax landed on earth (inadvertedly causing the demise of Dr. Ackerman) and began her experiments with Wesley. When a skeptical Wesley does not believe this, Ms. Xenobia then proceeds to peel off her face, revealing that  she is a blue-skinned alien with a large head rather than a sexy woman.  This causes Wesley to panic, throw Ms. Xenobia into a garbage can, and flee.

Wesley returns to the school (encountering Drax) where he blows up the biology lab, destroying Ms. Xenobias serum. Meanwhile, Ms. Xenobia starts to shoot up the music venue with a ray gun. Wesley arrives with the last batch of serum and threatens to drop it unless Ms. Xenobia stops her violent spree. When Ms. Xenobia refuses, Wesley destroys the serum, shocking her enough that she stops shooting.

Despondent that the chances of saving her race are over, Drax appears with a stalk growing from his head; he had taken some of the serum because he was in love with Ms. Xenobia.  This allows Ms. Xenobia to have a change of heart and she apologizes for all the hassle she caused Wesley. The film ends as Ms. Xenobia and Drax head home to their planet, while Wesley continues his new career with the Sex Mutants and his relationship with Leeane.

==Cast==
* Billy Jacoby as Wesley Littlejohn
* Judy Landers as Ms. Xenobia
* Olivia Barash as Leeanne
* Troy Donahue as Dr. Ackerman
* Stuart Fratkin as Marvin
* Arlene Golonka as Mrs. Littlejohn
* Jim Hackett as Mr. Littlejohn Bobby Jayne as Bradford Littlejohn
* Raymond OConnor as Drax
* Edy Williams as Buckmeister Ginger Lynn Allen as Rocker Chick #1
* Linnea Quigley as Rocker Chick #2
* Michelle Bauer as Coed #1

==External links==
*  

 

 
 
 
 
 
 
 