Spencer Halpin's Moral Kombat
 
{{Infobox film
 | name=Spencer Halpins Moral Kombat
 | image=Moral_kombat.jpg
 | director=Spencer Halpin
 | producer=Spencer Halpin Ramy Katrib Steven Kent Skylar James David Walsh Jack Thompson Dave Grossman Phil ONeil Marie Sylla Ed Williams Richard Ow Henry Jenkins Jason Della Rocca Doug Lowenstein Vince Broady
 | music=Mark Petrie
 | cinematography=Jeff Orsa
 | editing=Lakan de Leon
 | distributor=
 | released= 
 | runtime=80 minutes
 | country=United States
 | language=English
 | budget=
}} American independent filmmaker. The title of the film is in part a reference to the Mortal Kombat (series)|Mortal Kombat series of video games, which are notable for their extreme violence. The title was changed from Moral Kombat to Spencer Halpins Moral Kombat to avoid the risk of a lawsuit.   

== Overview == First Amendment.

The film makes extensive use of green screen technology to blend the subject matter being discussed (games, characters, archival news footage) in the background, while keeping the interviewee actively in the shot.

The trailer was uploaded to YouTube on December 27, 2006 and was met with overwhelming concern and dismay from gamers around the globe who felt it sensationalized the topic and serves as a rhetoric piece for anti-games and anti-gamer activists and political figures. The trailer was viewed over 100,000 times in the coming weekend and spurred debate on scores of websites.

Interviewed in the film are leading politicians, journalists, academics, consumer advocates and special interest group executives, game developers, retailers, trade associations and publishers. The interviewees among others, include: Dr. David Walsh, Jack Thompson, Dr. Michael Rich, Lorne Lanning, Greg Fischbach, Dean Takahashi, Jeff Griffiths, John Marmaduke, Pamela Eakes, Andy McNamara, Greg Ballard, Bob McKenzie, American McGee, Hal Halpin (Spencer Halpins brother), Lt. Col Grossman, Phil ONeil, Marie Sylla, Ed Williams, Richard Ow, Dr. Henry Jenkins, and Jason Della Rocca.

== Trailer feedback  ==
The films objectivity has been debated and challenged heavily since the release of the trailer, with many video responses to it being posted on YouTube. Notable among the responses are a series of videos that point out statements made by Jack Thompson, each with a web location leading back to the statement itself. Many respondents believe that Mr. Thompsons participation in the movie may rob it of its credibility.

According to Spencer Halpin, the trailer was not targeted at gamers, but rather the "42 year old women" who were concerned about video game violence issues, but didnt fully understand them. Professor Henry Jenkins recently released an online review of the complete film, applauding it as a balanced representation of both sides of the debate. {{cite web |url=http://www.henryjenkins.org/2007/11/moral_kombat.html 
|title=Why You Should See Spencer Halpins Moral Kombat (Part One) |accessdate=2007-11-30 |author= |last=Jenkins |first=Henry |authorlink=Henry Jenkins |coauthors= |date=2007-11-29 |year= |month= |work= |publisher= |pages= |language= |doi= |archiveurl= |archivedate= |quote= }} 

== Awards ==
Moral Kombat won a Director’s Choice Award from the Dallas Film and Video Festival in the Meta Media category. Festival Director and Founder, Bart Weiss, in summarizing the documentary for attendees said, “This is not an easy ‘Nasty video games are bad and are the cause of the destruction of man’ type of video.  It is thoughtful, very thorough, and it definitely presents both sides of the argument. The film is incredibly well-produced and directed, melding the interview into hi-res images from the games.  Indeed, by the end of the film you are exhausted by the barrage of these graphics, many of them violent.  The effect is, well, the effect of these images.  For someone like me who is not a gamer (well I play basketball, thats real ball, not a Wii game), the effect is disconnecting and without playing gives you an idea of what its about.  The film makes you feel what video games really are while youre hearing about their effects; its very powerful and a must-see. Many documentaries have footage that shows you what the subject is about very few make you feel it like this one does. As a festival that is very much interested in the effects of media on our psyche, this is a must see.”      

== References ==

 

== External links ==
*  
*  

 
 
 
 
 