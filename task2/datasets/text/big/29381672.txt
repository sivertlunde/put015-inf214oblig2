Kaali Topi Laal Rumaal
{{Infobox film
 | name = Kaali Topi Lal Rumaal 
 | image = KaaliTopiMithun.jpg
 | caption = DVD Cover
 | director = B. Vijay Reddy
 | producer = Lal Purswani Anwar Khan
 | dialogue = 
 | starring = Mithun Chakraborty
 | music = Dilip Sen-Sameer Sen
 | lyrics = Shyam Anuragi
 | associate director = 
 | art director = 
 | choreographer = 
 | released =  15 September 2000
 | runtime = 125 min.
 | language = Hindi Rs 4.0 Crores
 | preceded_by = 
 | followed_by = 
 }}
Kaali Topi Lal Rumaal is an action Hindi film made in 2000. A revenge drama, with Mithun in the lead role There is another Hindi movie named Kali Topi Laal Roomal made in 1959, which has song "Lagi Chhoote Na" sung by Lata Mangeshkar and Mohammad Rafi.

==Plot==

 

==Cast==

*Mithun Chakraborty	... 	Commando Vijay
*Rituparna Sengupta	... 	Mohini / Kamini (as Ritu Parnasen Gupta)
*Kamal Sadanah	... 	Raja
*Zeenat Ibrahim	... 	Shalu
*Farida Jalal	... 	Shalus mom
*Mohan Joshi	... 	Master D.D.
*Jay Kalgutkar	... 	Tony (as Jai Kalgutkar)
*Satyendra Kapoor	... 	Tilu
*Johnny Lever	... 	Nanhey (as Jhony Lever)
*Ram Mohan	... 	Major (as Ram-Mohan)
*Alok Nath	... 	Bishamber
*Ranjeet	... 	Mogha
*Chandrashekhar	... 	Fakir
*Tej Sapru	... 	Indian Army Officer
*Birbal	 ... 	Gangu
*Brahmachari	... 	Jailor (as Brahamchari)
*Gurbachchan Singh	... 	D.D.s truck driver (as Gurbachan)

==References==
 
*http://www.dvddhamaka.com/movie_detail_Kaali_Topi_Laal_Rumaal_3170_0.htm

==External links==
* 

 
 
 
 
 
 