Adventures in Babysitting
 
{{Infobox film
| name = Adventures in Babysitting
| image = Adventures In Babysitting.jpg
| caption = Theatrical release poster Chris Columbus	
| producer = Debra Hill Lynda Obst
| writer = David Simkins
| starring = Elisabeth Shue Maia Brewton Keith Coogan Anthony Rapp
| music = Michael Kamen
| cinematography = Ric Waite
| editing = Fredric Steinkamp William Steinkamp
| studio = Touchstone Pictures Silver Screen Partners III Buena Vista Pictures
| released = July 1, 1987
| runtime = 102 minutes
| country = United States
| language = English 
| budget = $7 million
| gross = $34,368,475
}} Chris Columbus directorial debut), Oak Park and Chicago|Chicago, Illinois, with much of the action taking place in the city itself, the film was shot primarily in Toronto.

==Plot== flat tire and they are picked up by a kind tow-truck driver, "Handsome" John Pruit, who offers to pay for the tire when Chris realizes she left her purse at the Andersons. En route, Pruit gets a call from his boss Dawson with evidence that his wife is cheating on him, and he rushes to his house to confront the infidelity. In the ensuing fight, Chris mothers car is damaged and they hide in a Cadillac, which is then car-jacked by a thief named Joe Gipp.

Reaching their hideout, Joe is confronted for bringing witnesses to their car theft operation and second-in-command Graydon ignores them when they say they wont go to the police. They are detained in an upstairs office and escape through the ceiling, they make their way across the rafters and out onto the roof. They are chased by the thieves and enter a blues club where the band on stage wont let them leave until they sing the blues. Chris, Brad, Sara and Daryl recount their events so far that night to the cheers of the audience and are allowed to leave, successfully losing Greydon and his leader Bleak. Meanwhile, Brenda is having trouble of her own, after her glasses are stolen and she doesnt have money for a hot dog, she mistakes a sewer rat for a kitten and is appalled when pest control points it out.
 on the lam for his earlier attacks and tells the kids he replaced the window to the car, but they need to find $50 to pay Dawson for the tire. The kids come across a fraternity house party and Chris meets and becomes attracted to Dan Lynch, who manages to get them $45. He takes them to Dawsons Garage and drops them off. When they find Dawson, his blond hair and holding a sledge hammer leads Sara to believe he is her hero Thor (Marvel Comics)|Thor. He coldly denies them their car for the $5 shortage, but when Sara offers him her toy Thor helmet, her generosity changes his mind and he lets them go. Meanwhile, Joe Gipp told Bleak about their troubles and are waiting to follow them. The kids find the restaurant where Mike was supposed to take Chris to and discover he is there with another girl. Sara slips away on her own to view a toy store while Chris yells at Mike. Brad stands up for his friend while Daryl kicks Mike over a food cart, ruining the dinner. Meanwhile, Sara is spotted by Bleak and Graydon chases her to an office building where she goes into hiding on a renovated floor; the others note her disappearance and follow close behind, accidentally coming across the Andersons party. After Sara climbs out one of the open windows and slides down the building, Chris spots her and they run upstairs to help her.

After pulling Sara from outside the window, Joe and Bleak confront them, but Joe knocks his boss out, giving him a Playboy Magazine that Daryl had stolen to replace one he had lost earlier, which had important notes that the criminals wanted. The kids hurriedly pick up Brenda from the bus station and rush home, narrowly avoiding the Andersons on the way. Once back home, the kids go upstairs while Chris dismisses Brenda and cleans up the mess left earlier, settling in place just as the Andersons return. Everything back to normal, Chris tells Sara that the night was probably her last babysitting gig, but all of them agree that it was the best night of their lives (so far) and that Brad and Chris are comfortable just remaining close friends. After Chris leaves, Dan arrives with one of Saras missing skates, he says he needs a babysitter and is disappointed when Chris said shes retired, he then confesses that the babysitter was for him. With Saras encouragement from the bedroom window, Chris and Dan laugh and kiss as Brad closes the blinds.

In a scene after the closing credits, Graydon is seen still stuck on the building ledge.

==Cast==
* Elisabeth Shue as Chris Parker
* Maia Brewton as Sara Anderson
* Keith Coogan as Brad Anderson
* Anthony Rapp as Daryl Coopersmith
* Penelope Ann Miller as Brenda
* Bradley Whitford as Mike Todwell
* Calvin Levels as Joe Gipp Vincent Phillip DOnofrio as Dawson
* George Newbern as Dan Lynch, a college student
* John Ford Noonan as "Handsome" John Pruitt
* John Davis Chandler as Bleak
* Ron Canada as Graydon, Bleaks second in command
* Albert Collins as himself; a player in a Chicago Blues club  
* Lolita Davidovich as Sue Ann
* Clark Johnson as Black Gang Leader
* Kirsten Kieferle as Cecily Plexer

==Production== Disney film Chris Columbus. It was released as A Night on the Town in the United Kingdom. In the bus station scene with Brenda, the actor who portrayed the hot dog salesman originally improvised his line as "then I dont have a fucking wiener!" Columbus loved the line, but executives at Disney wanted to keep the picture more family-friendly, so the line was removed (although the word "fuck" is used twice in the final cut ). This scene was added to stretch out the bus station segments. It was not in the original script.

Bradley Whitford, who played Mike Todwell in the film, was actually 27 years old at the time of shooting in 1987. He was uncomfortable with the age difference, but Columbus put him at ease by allowing him to use his own Camaro in the movie. The car is later seen with Whitfords actual license plate "SO COOL."

Valerie Bertinelli was one of the actresses who auditioned for the role of Chris Parker. 

==Release==

===Home media===
Adventures in Babysitting has been released on VHS, LaserDisc, DVD and Blu-ray formats.  In the United States, it received a VHS release by Touchstone Home Video on July 14, 1992.   It was released on DVD for the first time on January 18, 2000 by Touchstone Home Video.   A 25th anniversary edition Blu-ray was released August 7, 2012.

Although it may still be referred to as A Night on the Town on television airings in the United Kingdom, it has now been released in its original title.  The VHS was released on October 21, 2002 in the United Kingdom by Cinema Club, it received a 15 certificate by the BBFC  for strong language and sexual references, it was previously released in an edited PG certificate for family viewing.  It was released on DVD in the United Kingdom on May 31, 2004, again uncut like the 15 certificate VHS, it has been reduced to a 12 certificate. 

==Reaction==

===Critical response===
The film has received positive reviews, and it currently earns a "fresh" 76% approval rating on Rotten Tomatoes based on 24 reviews. 

===Box office===
The movie was a box office success, earning more than $34 million on its $7 million budget. 

==Television pilot==
Adventures in Babysitting was adapted into an American television pilot  of the same name for CBS in 1989. It starred Jennifer Guthrie (who would later co-star on Parker Lewis Cant Lose with Maia Brewton) as Chris, Joey Lawrence as Brad, Courtney Peldon as Sara, Brian Austin Green as Daryl, and Ariana Mohit as Brenda. It was not picked up to series.

==Remake==
A remake was reportedly planned for release in 2012.      Raven-Symoné was going to star in the remake, currently titled Further Adventures in Babysitting,  but decided to withdraw due to other projects. Miley Cyrus was also rumored to be attached to the project, but later denied involvement.   

According to Variety (magazine)|Variety, Tiffany Paulsen was writing the script.  It was presumed that the remake was scrapped due to years of inactivity. However, on January 9, 2015, Disney Channel announced that the remake would go forward, with Sabrina Carpenter and Sofia Carson starring as competing babysitters. 
In 2011, Jonah Hill starred in the film The Sitter, which was loosely based off the same plot and storyline, but with more modernized content. 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 