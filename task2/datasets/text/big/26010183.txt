Marriage Counselor Tora-san
{{Infobox film
| name = Marriage Counselor Tora-san
| image = Marriage Counselor Tora-san.jpg
| caption = Theatrical poster for Marriage Counselor Tora-san (1984)
| director = Yoji Yamada
| producer = 
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Rie Nakahara
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 102 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1984 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Rie Nakahara as his love interest or "Madonna".  Marriage Counselor Tora-san is the thirty-third entry in the popular, long-running Otoko wa Tsurai yo series.

==Synopsis==
In Shibamata, Tokyo, Tora-sans family prepares for a wedding. Meanwhile, the traveling Tora-san meets an old acqaintance in Iwate Province. Tora-san refuses to drink with him, afraid that the acquaintance, now settled and married, will again become attracted to Tora-sans wandering existence. Tora-san becomes attracted to a female barber, but must break off their relationship so that she too can live a secure life. She instead gets into an abusive relationship with a motorcyclist.      

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Rie Nakahara as Fūko Kogure
* Tsunehiko Watase as Tony
* Jun Miho as Akemi
* Shimojo Masami as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajiros aunt)
* Gin Maeda as Hiroshi Suwa
* Hidetaka Yoshioka as Mitsuo Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama

==Critical appraisal==
Stuart Galbraith IV writes that Marriage Counselor Tora-san is a better entry in the Otoko wa Tsurai yo series, though it differs considerably from other films in the series, both formally and thematically. Tora-sans wandering life is portrayed harshly, rather than light-heartedly, and his familys stable life is shown in the more positive light. Galbraith concludes, "With its darker tone and atypical structure, Tora-sans Marriage Counselor is like a breath of fresh air following a series of recent, somewhat repetitive entries."  The German-language site molodezhnaja gives Marriage Counselor Tora-san three and a half out of five stars.   

==Availability==
Marriage Counselor Tora-san was released theatrically on August 4, 1984.  In Japan, the film has been released on videotape in 1987 and 1996, and in DVD format in 2005 and 2008. 

==References==
;Notes
 

;Bibliography
;;English
*  
*  
*  

;;German
*  

;;Japanese
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 