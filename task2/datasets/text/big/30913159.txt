Psychosis (film)
{{Infobox film
| name = Psychosis (2010)
| image = Psychosis-poster.jpg
| caption = 
| alt = 
| director = Reg Traviss
| producer = Patrick Fischer
| writer = Reg Traviss
| starring = Charisma Carpenter Paul Sculfor Ricci Harnett Justin Hawkins Ty Glaser Katrena Rochell Slaine Kelly Axelle Carolyn
| distributor = Lionsgate
| released =  
| runtime = 89 minutes
| country = United Kingdom
| language = English
| budget = $1 million
}} Michael Armstrong. Its a remake of the "Dreamhouse" episode from the movie anthology Screamtime.   Fangoria 

The film was released in the United Kingdom in July 2010 and 11 January 2011 in the USA. The film was budgeted on $1 million.

== Plot ==
In 1992, a group of young Anarchy | Anarchists seeking to preserve the local wildlife are brutally murdered. The killer is later found collapsed by a river due to wounds hed sustained while attempting to kill the lone surviving anarchist. 

The movie flashes forward 15 years as successful crime novelist Susan moves into a nearby house with her husband David, who have purchased the house in hopes of it helping her with her writing. Susan is quickly made uneasy as she witnesses the houses gamekeeper Peck having enthusiastic sex in the woods and then later exposing himself to her. She also begins to witness strange visions in the house, all surrounding bloody bodies, the killer from earlier in the film, and people who appear one moment and disappear the next. Its later revealed that Susan had previously suffered a mental breakdown due to seeing and hearing things that were never there, which was part of the reason for the houses purchase. 

After David leaves for a "business trip" (quickly revealed to be an excuse to indulge in incredibly raunchy affairs), Susan is drugged and taken advantage of sexually by Peck. The next day Susan confides in the local priest about her past mental illnesses and her fears of her new home. The priest has a psychic examine the house, only for the psychic to declare that there are no presences currently in the house. 

Immediately after the psychic and the priest are escorted outside by her husband, Susan witnesses a series of brutal murders involving all of the people she had earlier seen in her visions throughout the film. These visions end up destroying what little sanity Susan has left, resulting in her accidentally killing Peck as he was checking up on her. Upon discovering what she has done, Susan is sent to a mental institution to spend the rest of her days. 

The movie then shows that Susan had been channeling her visions into her latest book, which has become an instant bestseller. David is shown receiving the money from her book and its implied that he had married Susan only for her money and that Pecks actions were done in an attempt to get blackmail material for the divorce. David then goes back to the house one last time to finalize the sale to its new owner, where we are then shown all of the people Susan had witnessed during her stay in the house. He is then gruesomely murdered by the killer Susan had been seeing all along (who had been incarcerated but escaped), revealing that her visions had never been due to insanity, rather because shed been having visions of the future murders that would happen in the house.

== Cast ==
*Charisma Carpenter as Susan
*Paul Sculfor as David
*Ricci Harnett  as Peck
*Justin Hawkins as Josh
*Ty Glaser as Emily 
*Bernard Kay as Reverend Swan
*Richard Raynesford as  Charles   
*Sean Chapman as  Detective Sergeant  
*Katrena Rochell as  Helena 
*Slaine Kelly as Kirsty  
*Axelle Carolyn as Michele (the characters voice is performed by a different actress)
*Josh Myers as Snake

==Reception==
Psychosis has received mostly negative reviews from critics,  with Scott Weinberg of Fearnet recommending the movie as a "sleeping aid" to viewers and HorrorNews.net saying the film was "unoriginal, boring, and confusing as hell at times".   Reelfilm reviews wrote that while the film has a "reasonably competent sense of style", ultimately it was "impossible to label Psychosis as anything more than a fleetingly captivating yet thunderously misguided piece of work."  Fangoria also panned the film, stating "PSYCHOSIS is a terribly boring film with an ending that doesn’t reward viewers for undertaking it’s gruelingly sluggish pace." 

Eye For Film positively reviewed the movie, calling Psychosis "a rather stately, old-fashioned feeling film".  Dread Central wrote that "Psychosis is definitely worthy of a watch, and in the end its only its pacing issues that keep it from rising about the good level into greatness." 

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 