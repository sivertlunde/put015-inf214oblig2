Buona Sera, Mrs. Campbell
{{Infobox film
| name           = Buona Sera, Mrs. Campbell
| image          = BuonaSeraMrsCampbell.jpg
| caption        = Original poster
| director       = Melvin Frank
| producer       =  
| writer         =  
| starring       =  
| music          = Riz Ortolani
| cinematography = Gábor Pogány
| editing        = Bill Butler
| distributor    = United Artists 
| released       = December 1968
| runtime        = 108 minutes
| country        = United States
| language       = English
| gross = $2.5 million  (US/ Canada rentals) 
}}
 American comedy film starring Gina Lollobrigida and directed by Melvin Frank, who co-wrote the original screenplay with Denis Norden and Sheldon Keller.
 2008 movie adaptation. 

==Plot  ==
   Italian woman GIs (a corporal, a sergeant, and a lieutenant) in the course of ten days, Cpl. Phil Newman (Phil Silvers), Lt. Justin Young (Peter Lawford), and Sgt. Walter Braddock (Telly Savalas). By the time she discovers she is pregnant, all three have moved on and she, uncertain of which is the father, convinces each of the three (who are unaware of the existence of the other two) to support "his" daughter, Gia, financially.

To protect her reputation, as well as the reputation of her unborn child, Carla has raised the girl to believe her mother is the widow of an army air force captain named Eddie Campbell, a name she borrowed from a can of soup (she is very fond of Campbell Soup Company|Campbells soups).

The film opens twenty years after the end of World War II in the village of San Forino, where the three ex-airmen attend a unit-wide reunion of the 293rd Squadron of the 15th AF in the village where they were stationed. The men are accompanied by their wives, and in the Newmans case, three obnoxious children. Carla is forced into a series of comic slapstick situations as she tries to keep them—each one anxious to meet his daughter (Janet Margolin) for the first time—from discovering her secret, while at the same time trying to keep Gia from running off to Paris to be with a much older married man who will take her to Brazil.

When confronted, Mrs. Campbell admits she does not know which of the three men is Gias father. She challenges the men by asking them what kind of father each would have been, particularly because they have never been there for all the small but important life events of their daughter. Provoked by this, the potential fathers talk to Gia and insist that she cannot run off. The "fathers" cease the support payments, and the Braddocks, who cannot have children of their own, agree to take care of Gia while she studies in the U.S.

==Cast  ==
* Gina Lollobrigida as Carla Campbell
* Phil Silvers as Phil Newman
* Peter Lawford as Justin Young
* Telly Savalas as Walter Braddock
* Shelley Winters as Shirley Newman Marian Moses as Lauren Young
* Lee Grant as Fritzie Braddock
* Janet Margolin as Gia Campbell
* Naomi Stevens as Rosa Philippe Leroy as Vittorio
* Giovanna Galletti as The Contessa

==Musical score==
A soundtrack album was released by United Artists Records.

* Buona Sera, title song
** Sung by Jimmy Roselli
** Music by Riz Ortolani
** Lyrics by Melvin Frank

* San Forino March
** Music and Lyrics by Andrew Frank

* The Army Air Corps Song
** Music by Robert Crawford

* In the Mood
** Written by Joe Garland and Andy Razaf

* Moonlight Serenade
** Music by Glenn Miller

==Critical response==
In his review in the Chicago Sun-Times, Roger Ebert described the film as "a charming reminder of what movie comedies used to be like . . . It depends on the traditional strong points of movie comedy: well-defined situation, good dialog, emphasis on characters . . . director Melvin Frank holds the story together and makes it work. A lot of the credit goes to the real comic ability of Telly Savalas (the best of the three would-be fathers) and Shelley Winters, who plays Phil Silvers wife. Miss Lollobrigida is good, too, projecting the kind of innocence that is necessary if the situation isnt going to seem vulgar." 
 Howard Thompson paternity begins to sound like a tired, old burlesque joke. The finale is as dull as the opening chapter is sprightly." 

==Awards and nominations==
* Golden Globe Award for Best Motion Picture Actress in a Musical or Comedy (Gina Lollobrigida) - nominated
* Golden Globe Award for Best Original Song ("Buona Sera, Mrs. Campbell") - nominated
* Writers Guild of America Award for Best Written American Original Screenplay - nominated
* David di Donatello for Best Actress (Gina Lollobrigida), winner

==See also==
* List of American films of 1968

==References==
 

==External links==
*  
*  
*  
*   filming locations at Movieloci.com

 

 
 
 
 
 
 
 
 