Malamaal Weekly
 
 
{{Infobox film
| name           = Malamaal Weekly
| image          = Malamaalweeklyposter.jpg
| caption        = Theatrical release poster
| director       = Priyadarshan
| producer       = Suresh Balaje 
| story          = Priyadarshan
| screenplay     = Priyadarshan
| starring       = Paresh Rawal Om Puri Ritesh Deshmukh Reema Sen Rajpal Yadav
| music          = Uttankk V. Vorra
| narrator       = Naseeruddin Shah
| cinematography = Sameer Arya
| editing        = N. Gopalakrishnan Arun Kumar
| distributor    = Percept Picture Company Sahara One
| released       =  
| runtime        = 160 min.
| country        = India
| language       = Hindi
| awards         =
| budget         =  
| followed_by    =
| gross          =   
}}

Malamaal Weekly is a 2006 Bollywood comedy film starring Paresh Rawal and Om Puri and written and directed by Priyadarshan. The storyline has similarities with Waking Ned Devine.

The film was rated poorly by most critics in India but was a surprise hit among cinema-goers. The film was remade in Telugu as Bhagyalakshmi Bumper Draw, in Kannada as Dakota Picture and in Malayalam as "Aamayum Muyalum".

==Plot== Hindi for rich).

Lilaram (Paresh Rawal) is the only educated man in the village. He has the job of intermediary between the lottery organisation and the village, for which he receives a commission whenever a villager wins; thus, he has a relatively good but volatile income. One day he reads the winning lottery numbers and realises that one of the tickets has won the top prize of one crore (about $220,000, a relative fortune in rural India). He devises a plan to obtain the winning ticket and present it to the commission as his own. He hosts a dinner (mortgaging his wifes beloved pet goat to the Thakurs wife to pay for it) and invites all the villagers who play the lottery, but the man he is looking for does not turn up. By elimination he deduces that the winner is Anthony (Innocent Vincent), the town drunk, and reasons that he didnt turn up because he knew that he had won the top prize. Hoping to at least extract his commission, he goes to Anthonys house, and finds him dead, the winning ticket clutched in his hand and a happy expression on his face.

Lilaram attempts to pry the ticket from Anthonys fingers but is thwarted by Anthonys body in rigor mortis. Lilaram eventually succeeds in freeing it with a knife; at this point Ballu (Om Puri), the local dairy farmer, enters the house and discovers him standing over Anthonys corpse with what appears to be the murder weapon in his hand. Lilaram tells Ballu the truth and convinces him to remain silent in exchange for sharing the lottery winnings between them.
 Arbaaz Khan), who is on his way to the village to interview Anthony.

==Cast==
* Paresh Rawal as Lilaram aka Leela
* Om Puri as Balwant aka Ballu
* Riteish Deshmukh as Kanhaiya
* Reema Sen as Sukhmani
* Sudha Chandran as Karamkali
* Rajpal Yadav as Bajbahadur aka baje
* Shakti Kapoor as Joseph
* Innocent as Anthony Arbaaz Khan as Jayesh Agarwal or the lottery inspector
* Asrani as Chokheylal, Kanhaiyas father
* Rasika Joshi as Mary
* Anand Ingle as local vaidya
* Rakhi Sawant, special appearance in the song "Kismat Se Chalti Hai"
* Sona Nair

==Reception==
The Hindustan Times was broadly positive about Malaamal Weekly, with two reviewers awarding it two and three stars but lauding "the sheer pleasure" of Rawal and Puris comic performances.  Most reviewers, however, were more negative. The BBC gave the film two out of five stars, citing a "weak script" and saying that the humour of Waking Ned had been "lost in translation." {{cite web| url=http://www.bbc.co.uk/films/2006/03/13/malamaal_weekly_2006_review.shtml
| accessdate=2006-05-11| title=Malaamal Weekly| first=Poonam
| last=Joshi| date=3 March 2006
| publisher=BBC}}  Molodezhnaja.ch concurred, repeatedly complaining that the film was too long, had only one song and the rest consisting of "repetitive scenes, long, no, endless dialogue and a poor finale," giving it 2.5 stars on the basis of "a few laughs and solid casting." {{cite web
| url=http://www.molodezhnaja.ch/malamaalweekly.htm| accessdate=2006-05-11| title=Malaamal Weekly
| first=Marco| date=7 May 2006| publisher=Molodezhnaja|language=de}} 
It did at least give the single song credit for "breaking up the otherwise monotonous events"; rediff.com described it as "the worst Bollywood song ever" in addition to summing the film up as "simply pathetic." 

Despite the poor reviews, the movie was a surprise hit at the box office. As of April 2006, the film was the top grosser in Delhi {{cite news| title=Top grossers of 2006, so far| date=11 April 2006 publisher = Hindustan Times}}  and had made Rupee|Rs. 120 million (about $2.6 million) overall.  Priyadarshan shrugged off the poor reviews, claiming "My films have never been given good reviews in Mumbai. Id get seriously worried about my films the day theyre reviewed favourably." 

==Sequel==
A sequel, which will be directed by Priyadarshan, under the title of Malamaal Weekly 2. It will star Paresh Rawal, Om Puri, Dr.Rajeev Pillai and Rajpal Yadav; the rest of the cast are yet to be chosen. The music company is yet to be finalised; the studio will be Percept Picture Company.

==References==
 

==External links==
*   at Indiafm.com
*  

 

 
 
 
 
 
 
 