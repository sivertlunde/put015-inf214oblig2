Buried Alive (2007 film)
{{Infobox film
| name           = Buried Alive
| image          = buried-alive2007.jpg
| caption        =
| director       = Robert Kurtzman
| producer       = David Greathouse Deborah Del Prete Gigi Pritzker
| writer         = Art Monterastelli
| starring       = Tobin Bell Terence Jay Leah Rachel Erin Michelle Lokitz Lindsey Scott Steve Sandvoss and Germaine De Leon
| music          = Terence Jay
| cinematography = Thomas Callaway
| editing        = Cari Coughlin
| distributor    = Dimension Extreme
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Buried Alive is a 2007 horror film starring Terence Jay, Leah Rachel, and Tobin Bell, about a group of college students stalked by a vengeful spirit during a New Mexico road trip. It was a straight-to-DVD release distributed by Dimension Extreme.  This film was a Blockbuster Exclusive.

==Plot==
 
  as Lester in Buried Alive.]]

The film opens with Rene (Leah Rachel) in the bath, with her cousin, Zane (Terence Jay) also in the bathroom, and they have a conversation. Suddenly, he pushes her underwater, and she begins to drown. While looking up at his face from under the water, she sees the visage of an old woman above her. She then wakes up, and we find out that she has fallen asleep and begun to drown. Her boyfriend Danny (Steve Sandvoss) wakes her up, and the two make out.

The scene then switches to Zane, who has enlisted the help of a nerdy guy called Phil (Germaine De Leon) to research his family history. Zane decides to visit his family home, bringing Phil, Rene, Danny, and her two sorority pledges, Julie "Cow" (Lindsey Scott) and Laura "Dog" (Erin Michelle Lokitz). The two pledges have been forced to dress up as animals (cow and dog respectively) and are being brought to Zanes family home as an initiation. On the way to the cabin, Zane constantly sees an old woman on the side of the road, culminating in her appearance in the middle of the road, which causes him to almost crash. He decides she is a hallucination, and continues on to his familys land.

Lester (Tobin Bell), the caretaker of the house, has been living in a trailer on the land, and searching for gold. It appears that he has found some, but does not tell anyone when they arrive. The college students marvel at his stuffed oddities, as he has become an amateur taxidermist. He warns the group not to go into the subcellar, or to go outside after dark. Zane does not take his warnings seriously, and they continue on to the house. 

Settling in, Rene enslaves Julie and Laura and forces Phil to explain how he knows so much about Rene and Zanes family history. Phil goes outside to get a signal on his mobile phone, but is cut in half by a ghost armed with an axe.

Thinking that Phil is sitting in the car talking on his phone, the five remaining teens decide to learn more about the family history. After that, Rene decides to have a little fun with Julie and Laura by setting them a task; they must run to Lesters caravan and bring back one of his stuffed animals as proof they were there. They must wear one item of clothing and it cant be an overcoat. Julie chooses to wear her pants and Laura chooses to wear her boots. Laura accomplishes the task, but Julie doesnt because she sprains her ankle. Rene says that she was once given the task but had to go three blocks, down fraternity row, and that she chose to wear a ski mask.

Rene decides to give Julie one more task, and thats to take off her clothes, except for her underwear, and be blindfolded. Rene takes Zanes belt and uses it on Julie as a test of trust. Soon, Rene and Laura depart, leaving Julie standing there. Danny decides to go to get Phil but finds out that hes dead just before his face is cut off. When the lights go out, Zane goes out to check the machine to find its still functioning normally, but the cable was cut. Zane finds Dannys faceless body, as do Rene and Laura. Zane, Rene and Laura dash back inside the house, thinking Lester is behind Dannys murder, but they find Lester dead as well.

They try to escape with their car, but find it has been sabotaged. They grab Lesters keys and Laura dashes to Lesters caravan to get his truck. While Zane and Rene are still in the house, Zane gets locked in another room and Rene is knocked unconscious with the spirit scratching the words "Sins Of The Father" on her back. Zane finally breaks through and kills the woman, but whilst barricading them in a room, the woman appears and knocks Zane unconscious.

Laura returns with Lesters truck and the woman is about to slaughter Laura, but disappears after seeing a tattoo thats similar to the necklace Rene had throughout the movie. Laura escapes and Rene and Zane wake to find themselves in some kind of box. The old woman quickly grabs the necklace and drops the gold ring taken by the caretaker in the beginning of the movie. Rene and Zane scream in fear as the old woman buries them alive.

==Cast==
* Tobin Bell as Lester
* Leah Rachel as Rene
* Terence Jay as Zane
* Erin Michelle Lokitz as Laura
* Lindsey Scott as Julie
* Steve Sandvoss as Danny
* Germaine De Leon as Phil

==Reception==
 
 
Buried Alive was met with negative reviews, earning Rotten Tomatoes an approval rating of 17%. David Nusair of Film Reel Reviews quoted that "aside from the admittedly effective casting of Tobin Bell from Saw (2004 film)|Saw as a crazy old coot, Buried Alive is conspicuously lacking in overtly positive elements."

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 