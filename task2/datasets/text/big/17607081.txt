An Unforgettable Summer
 
{{Infobox film
| name           = An Unforgettable Summer
| image          = An-unforgettable-summer-poster.jpg
| caption        = Poster of the film in its French version
| director       = Lucian Pintilie Constantin Popescu Paul Bortnovschi
| writer         = Petru Dumitriu (book) Lucian Pintilie (screenplay)
| starring       = Kristin Scott Thomas Claudiu Bleonţ Olga Tudorache George Constantin
| music          = Anton Şuteu
| cinematography = Călin Ghibu
| editing        = Victoriţa Nae
| distributor    = MK2 Productions
| released       = 1994 in film
| runtime        = 81 minutes
| country        = Romania France Romanian (main) French (additional English (additional dialogue)
| gross          = $65,352  
}} 1994 drama drama film French co-production British actress Macedonian origin ethnic Bulgarian locals. The film shows her failed attempt to rescue Bulgarians held hostage by the Romanian soldiers, and who are destined for execution. An Unforgettable Summer also stars Claudiu Bleonţ as Captain Dumitriu and Marcel Iureş as Ipsilanti, a general whose unsuccessful attempt to seduce Von Debretsy and the resulting grudge he holds against the couple account for Dumitrius reassignment.

Completed in the context of the Yugoslav wars, the film constitutes an investigation into the consequences of xenophobia and state-sanctioned repression, as well as an indictment of a failure in reaching out. It is thus often described as a verdict on the history of Romania, as well as on problems facing the Balkans at large, and occasionally described as a warning that violence could also erupt in a purely Romanian context.

Released by MK2 Productions, An Unforgettable Summer was financed by the Council of Europes Eurimages fund for continental cinema. In the United States and elsewhere, it was made available on limited release. Other actors credited in secondary roles include George Constantin as General Tchilibia, Răzvan Vasilescu as Colonel Turtureanu, Olga Tudorache as Madame Vorvoreanu, Cornel Scripcaru, Carmen Ungureanu, Dorina Lazăr, Mihai Constantin and Ioan Gyuri Pascu.

==Plot== communist sympathizer, and who irritates the officials by shouting out insults and mooning them through a window. During the latter scene, John Simon notes, Land Forces officers are shown staring up "in mixed horror and admiration at the familiar globe whose owner they promptly identify."  As she is beaten up by the soldiers, Erzsi continues to defy her aggressors by shouting up revolutionary slogans coined under the Hungarian Soviet Republic. 
 Hungarian aristocracy, and that she is held in contempt by the local notabilities.  In parallel, Ipsilanti himself is shown to be not just a military commander, but also as a prince. 

Film historian Anne Jäckel describes the story as dealing with "the slow descent into Hell of two honest, liberal people." Jäckel, p.105  The two persons are the short and  ,  , at  ; retrieved May 25, 2008  French critic Sylvie Rollet argues that this attempt to "tame the world" by erecting "frontiers" is a central aspect of An Unforgettable Summer. 

While Petre Dumitriu is motivated by his pursuit of discipline, his wife preserves her sophistication, reading the works of  s: unknown attackers through rocks into the Dumitrius house, while the vegetables she planted in the garden prove unpalatable. Confronted with these signs, Von Debretsy still attempts to make the best of the situation;  s by the military, are made to work on the garden. Their labor brings immediate improvement to the crop, and, upset by their condition, Marie-Thérèse decides to pay them out of her own pocket, serves them tea and eventually befriends them. In a scene that provided the original title for Petru Dumitrius book chapter ("The Salad"), she invites Ipsilanti and other officers to dinner, and they are all shown to be enjoying the salad provided by Bulgarian labor. However, the episode also renews tensions between Ipsilanti and his Hungarian host, when she expresses her appreciation of her servants work and attempts to intervene on their behalf. 

As a result of one Macedonian incursion, during which border guards are killed, Dumitriu is ordered to round up and execute a number of his Bulgarian prisoners. Horrified by this random reprisal, Marie-Thérèse strives to have them pardoned and released, but her plea only serves to irritate her husbands superiors. Her husband alienates his superiors further when he asks for the execution order to be ratified through official channels, whereas they would prefer an extrajudicial killing.  The resulting toll on Dumitrius career means that they are forced to leave Southern Dobruja, shortly before which the captains colleagues make public their resentment of the couple. The captain feels dishonored when an angry General Tchilibia draws a comparison between Von Debretsy and the prostitute Erzsi and stresses that, as Hungarians, both women are natural suspects in Romania. It is a result of this that Dumitriu decides to commit suicide, unable to decide whether to shoot himself in the mouth or in the temple, and ultimately falling of the stool (which his mare had chewed on) and weeping uncontrollably. This episode, Simon points out, was not present in the original text, and was invented by Pintilie to underline the degradation his character undergoes in order to survive.  In what is one of the closing scenes, Marie-Thérèse is stoned by those Bulgarian women whose husbands had been executed. 

==Production==
Together with its predecessor  , March 2003 (republished by  )  An Unforgettable Summer is one of the directors main films of the 1990s. During the previous decade, his work had been  , November 4, 1994  Caryn James,  , in The New York Times, November 11, 1994  Jäckel, p.106  The New York Times chronicler Caryn James writes: "His return   has added a significant name to the list of world film makers." 
 Romanian Ministry French Ministry Constantin Popescu. 
 John Simon, English education American film John Simon Oxford accent Scott Thomas uses in her Romanian-language lines, and to allow the actress to express herself in English during several scenes. 

==Political themes==

===An Unforgettable Summer and Yugoslavia=== lyrical and far more accessible." 
 Communist Yugoslavia and the onset of the Yugoslav wars: Lucian Pintilie once stated that he had been inspired by this outcome when filming on location,  and continued to refer to it in later interviews. 

According to Jäckel, An Unforgettable Summer has a prophetic role to play within the Balkan context, one she equates with that of  , Philadelphia, 2003, p.245. ISBN 1-56639-995-5 

In 1999, Jäckel noted: "  despair at the absurdity of destiny, and her powerlessness to change the situation, seem more relevant today than in 1993, when the film was made."  Concluding that Pintilies message also displays criticism of "liberal incomprehension" for Balkan realities,  she argues that the Western worlds intervention during situations of crisis, "after first denying and then ignoring the existence of evil", bears resemblance to what Von Debretsy is attempting.  In James Berardinellis view: "The basic impotence of the characters only emphasizes the real-world difficulties faced by peacemakers." 

===Romanian historical setting===
Like Balanţa, An Unforgettable Summer is also seen as a comment on   and   productions of the 1990s, in particular films by Radu Mihăileanu.  Caryn James argues that, through its references to the Hungarian Soviet Republic and its impact in Romania, the film can serve as a guide to the start of communism, just as Balanţa is a depiction of its outcome.  Through the means of dialogues in the film, the viewer is informed that communism has had an actual impact on Marie-Thérèse: her father allowed the Hungarian revolutionaries to split up his estate, but, for all his generosity, was killed by them. 
 Fort Apache, western by John Ford. 
 internationalist stage interwar realities.  According to Doinel Tronaru, the book chapter on which the film was based was itself controversial, and seen by many as "indigestible".  Tronaru writes: "the film was made during the bloody Yugoslav conflict, but Pintilie is telling us   not to tune out believing we are somehow better, that, if need be, we could be just as bloodthirsty as our shunned neighbors." 

Pintilie indicates that his interest was in showing the violent intrusion of an "oppressive mass" of Romanians into a world peopled by Bulgarian peasants, describing the latter as "natural cultivators of that land, with a special genius for vegetables—innocent ones, without any political or national consciousness." A second area of interest was the Bulgarian governments manipulation of Macedonians living in the region, resulting in "a lucrative bloody tension on the border" and "a political and historical crime that the Romanian and Bulgarian governments are building together."  Elsewhere, he had indicated: "In An Unforgettable Summer the Bulgarian peasants have no consciousness of ethnic difference: they are executed simply because examples must be set."  The Macedonian brigands are a mysterious presence throughout the film, and their actual ethnicity, unlike their loyalty to the Bulgarian state, is never specified.  According to Simon, they are themselves multi-ethnic,  while Pintilie states that they include resettled  , and is unable to speak Bulgarian language|Bulgarian. 

Although the film develops on these themes of oppression, both the narrator and Pintilie look back on the age of Greater Romania with a dose of nostalgia.  Simon writes: "What was the most horrible summer in the life of a young mother, driving her to drink and wasting away, was for her small son the most unforgettably lovely season of his life."  Variety argues: "The frantic crescendo of the final sequence has an eerie resonance, as the narrators final remarks reframe all that has gone before in a different, deeply ironic light."  Discussing this aspect, Pintilie stated: "Maybe Im even a bit perverse to begin the film in a light and playful way: people fall into the trap of thinking its not a serious film." 
 Nazi German troops began rounding up and executing members of various communities, the region did not experience "racial tension".  Despite his fondness for the interwar period, the director added: "I believe that an artist should not be a hostage to his own political convictions. If the Romanians are shown as intolerant, at least once, it has to be discussed. Each person, in this ethnic madness, must clean his own doorstep." 

==Impact and legacy== Colonel Chabert Before the Macedonian director Milcho Manchevski, noting that, for all the difference in setting and approach, they deal with similar subjects.  He writes: "If nothing else, these two pictures taken together underline the unhappy truth that, in the Balkans, little has changed over the past seventy years." 

An Unforgettable Summer was the first major production to star Scott Thomas, and the last film to star   for his performance in the role of Colonel Turtureanu, "an opportunistic soldier who has no compunctions about anything the military life may require", and comments favorably on the soundtrack composed by Anton Şuteu and on Paul Bortnovschis production design. 

Discussing Captain Dumitrius suicide attempt, John Simon writes: "It is visually stunning and emotionally shattering, but it may be a bit too theatrical. Still, with such writing, directing, and acting—most prominently from Kristin Scott-Thomas and Claudiu Bleonţ as the Dumitrius, but also from the rest—what is a small faux pas? This film resonates in the memory, insistently and inspiredly."  Simon also discusses the cinematography, arguing that Călin Ghibus use of lighting manages to convey the "almost unearthly beauty at sunset", which helps viewers understand why Scott Thompsons character uses Mount Fuji as her preferred metaphor for the place.  Of this aspect, James notes: "Like its heroine, the films serene and beautiful appearance masks a powerful conscience." 

The overall positive reception offered by the critics did not materialize in significant box-office success or international awards: An Unforgettable Summer unsuccessfully completed for the  . 

Speaking in 1994, Pintilie indicated that he was considering a sequel to the film, also based on Dumitrius writings. Planned for 1996, it was to depict an aging and jealous Marie-Thérèse, who intervenes in her sons love life and chases away women who fancy him. 

==Notes==
 

==References==
*Anne Jäckel, "Too Late? Recent Developments in Romanian Cinema", in Wendy Everett (ed.), Critical Studies. The Seeing Century: Film, Vision and Identity, Rodopi Publishers, Amsterdam, 2000. ISBN 90-420-1494-6

==External links==
*  

 

 
 
 
 
 
 
 
 