In the Shadow of the Raven
{{Infobox film
| name           = Í skugga hrafnsins
| image          =  
| caption        = 
| director       = Hrafn Gunnlaugsson
| producer       = Christer Abrahamsen
| writer         = Hrafn Gunnlaugsson
| narrator       = 
| starring       = Reine Brynolfsson Tinna Gunnlaugsdóttir Egill Ólafsson Sune Mangs Kristbjörg Kjeld
| music          = Harry Manfredini  Hans-Erik Philip
| cinematography = Tony Forsberg
| editing        = Hrafn Gunnlaugsson
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = Iceland
| language       = Icelandic language|Icelandic, German
| budget         = ISK 200,000,000
}} 
 
In the Shadow of the Raven (orig. Í skugga hrafnsins ( )) is the title of a 1988 film by Hrafn Gunnlaugsson, set in Viking Age Iceland.

In the Shadow of the Raven is the second film of the legendary Raven Trilogy (also known as the Viking Trilogy) that consists of three Viking films:

* 1) When the Raven Flies (1984) - (original Icelandic title: Hrafninn flýgur) - usually known as simply The Raven or Revenge of the Barbarians.
* 2) In The Shadow of the Raven (1987) - (original Icelandic title: Í skugga hrafnsins).
* 3) Embla (2007) - (original Icelandic title: White Viking|Hvíti víkingurinn) - the directors cut of The White Viking.

== External links ==
*  
*  
*  
*   at  

 

 
 
 
 
 
 
 
 

 
 