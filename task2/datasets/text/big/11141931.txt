Slicked-up Pup
{{Infobox Hollywood cartoon
|cartoon_name=Slicked-up Pup
|series=Tom and Jerry
|image=Slicked-Up_Pup_Title.JPG
|caption= title card
|director=William Hanna Joseph Barbera
|story_artist=William Hanna Joseph Barbera 
|animator=Ed Barge Kenneth Muse Irven Spence Ray Patterson
|voice_actor=Daws Butler
|musician=Scott Bradley 
|producer=Fred Quimby
|distributor=Metro-Goldwyn-Mayer
|release_date=September 8, 1951
|color_process=Technicolor
|runtime=6:19 English
|preceded_by=His Mouse Friday
|followed_by=Nit-Witty Kitty
}}
Slicked-up Pup is a 1951 American one-reel animated cartoon and is the 60th Tom and Jerry cartoon directed by William Hanna and Joseph Barbera and produced by Fred Quimby. The cartoon was scored by Scott Bradley and animated by Ed Barge, Kenneth Muse, Irven Spence and Ray Patterson.  It features the second appearance of both Spike and Tyke together.

==Plot==
Spike had  bathed Tyke so that he is nice and clean. However, Spike is horrified when through the constant chases of Tom and Jerry, Tyke ends up getting dirty by falling into a mud puddle. Spike is extremely disappointed in Tom and scolds him that Tyke is dirty, and demands Tom had better clean him up. Tom quickly rushes off with the muddy pup and returns almost instantly with Tyke cleaned up. Spike issues Tom an ultimatum: the cat had better keep Tyke clean before Spike comes back, or Spike will make him suffer the consequences by tearing him limb after limb ("Do I make myself clear?"). Tom grudgingly agrees to look after the pup and ensure that the pup had best be clean until Spike returns, but Jerry of course is being ready to sabotage this.

As Tom sits down on the same wooden platform that Tyke is lying on, one of the wooden planks catapults Tyke into the air, and Tom narrowly saves Tyke from falling into the same muddy puddle. Tom overhears Jerrys laughter and chases after him. Jerry quickly stops the cat, and challenges him to a game of tic-tac-toe on Tykes back. Tom wins and resumes chasing Jerry, before suddenly realizing what hes done, and promptly returns to Tyke to rub off his pencil marks. Jerry hurls a tomato at Tom, but Tom quickly ducks so that it avoids him. Realizing that it will hit Tyke instead he yelps with fear and Tom rushes back and stands directly in front of the pup so that the tomato does hit him after all.

The chase resumes until Tyke ends up with a jar of ink spilled on him. Tom panics after seeing Tyke covered in ink and attempts to rub the ink off but refuses. Tom grabs some paint tins, painting Tyke first white, then gray, but Jerry pushes the paint containers so that Tom ends up dipping his paintbrush into a variety of different colors. Tyke has now been painted a multi-colored mess of reds, blues, greens and yellows. Horrified, Tom grabs a hose so that he can wash the paint off with water, but it is Jerry that connects the other end of the hosepipe to a large container of tar. Out of the hosepipe comes thick, black, sticky tar that makes Tyke dirty. Tom sees that Spike is approaching, and decides he had better act quickly. Tom spots a pillow hanging on a washing line, and stuffs Tyke into it and takes him out, which leaves Tyke covered in feathers. He then places a red glove on Tykes head and a clothes peg on his mouth, so that Tyke crudely resembles a chicken of sorts. Spike is surprisingly fooled and walks off. However, Tyke removes the peg from his mouth and bites Toms tail. Tom hysterically screams in pain and alerts Spike, causing him to turn and investigate.

Tom rushes off and hides in the laundry room, putting Tyke inside a washing machine. But Tom is too late to do anything; just as he is adding soap flakes, Spike stops him rattily and takes Tyke out of the washing machine. Catching on to what had happened to Tyke, an angry Spike pours the box of soap flakes over Toms head and puts a soap bar in his mouth, before shoving Tom in the washing machine and slamming its door on him. Now, Tom ends up taking a shower around the washing machine as Spike and Tyke together look on. Both of them are joined by Jerry, who waves at the cat while the cartoon closes.

==References==
 

==External links==
* 
* 

 

 
 