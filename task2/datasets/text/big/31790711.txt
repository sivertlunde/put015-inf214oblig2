Escape Me Never (1935 film)
{{Infobox film
| name           = Escape Me Never
| image          = 
| image size     = 
| caption        = 
| director       = Paul Czinner
| producer       = Herbert Wilcox
| writer         = Margaret Kennedy (play) Robert Cullen Carl Zuckermann
| narrator       =  Hugh Sinclair Griffith Jones Penelope Dudley-Ward
| music          = William Walton
| cinematography = Sepp Allgeier Georges Périnal Freddie Young
| editing        = David Lean British and Dominions
| distributor    = United Artists
| released       = 1935
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded by    =
| followed by    = 
}} Hugh Sinclair Griffith Jones. Escape Me the 1947 Warner Bros. version. The score is by William Walton. Bergner was nominated for the Academy Award for Best Actress for her performance, but lost out to Bette Davis. British readers of Film Weekly magazine voted the 1935 Best Performance category in a British movie to her. 

==Cast==
* Elisabeth Bergner as Gemma Jones Hugh Sinclair as Sebastian Sanger Griffith Jones as Caryl Sanger
* Penelope Dudley-Ward as Fenella McClean
* Irene Vanbrugh as Lady Helen McClean
* Leon Quartermaine as Sir Ivor McClean
* Lyn Harding as Herr Heinrich
* Rosalinde Fuller as Teremtcherva

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 