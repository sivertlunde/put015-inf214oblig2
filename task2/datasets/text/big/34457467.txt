Mr. Gilfil's Love Story
 
{{Infobox film
| name           = Mr. Gilfils Love Story
| image          =
| caption        =
| director       = A.V. Bramble
| producer       = 
| writer         = George Eliot (novel)   Eliot Stannard
| starring       = Robert Henderson Bland Mary Odette Peter Upcher   Dora De Winton
| music          = 
| cinematography = 
| editing        = 
| studio         = Ideal Film Company
| distributor    = Ideal Film Company
| released       = 1920 
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent drama film directed by A.V. Bramble and starring Robert Henderson Bland, Mary Odette and Peter Upcher.  It was based on the short story Mr. Gilfils Love Story from George Eliots 1857 work Scenes of Clerical Life. A chaplain to an aristocratic British family falls in love with their Ward (law)|ward, a young Italian woman, who he marries. Tragedy strikes when she dies only a few months later leaving him in a state of grief.

==Cast==
*  Robert Henderson Bland - Maynard Gilfil  
*  Mary Odette - Caterina  
*  Peter Upcher - Anthony Wybrow  
*  Dora De Winton - Lady Clevere  
*  A. Harding Steerman - Sir Christopher Chever  
*  Aileen Bagot - Beatrice Asscher  
*  Norma Whalley - Lady Asscher  
*  John Boella - Signor Sarti  
*  Irene Drew - Dorcas  
*  Robert Clifton - Knott

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 