The Last Dinosaur
{{Infobox Film
| name           = The Last Dinosaur image           =Poster of the movie The Last Dinosaur.jpg
| director       = Alexander Grasshoff Shusei Kotani
| producer       = Arthur Rankin, Jr. Jules Bass Noboru Tsuburaya
| writer         = William Overgard
| starring       = Richard Boone Joan Van Ark Steven Keats
| music          = Maury Laws
| cinematography = Masaharu Ueda
| editing        = Minoru Kozono Yoshitami Kuroiwa Tatsuji Nakashizu
| studio      =  Rankin/Bass  Tsuburaya Productions
| distributor    = Toho
| released       = 11 February 1977
| runtime        = 92 minutes (USA)   106 Minutes (Japan)
| country        =   Japan    
| language       = English   Japanese (dubbed)
}} Nancy Wilson, and arranged and conducted by Bernard Hoffer. The title should really be pluralised as there are several dinosaurs in the film that could qualify as the last one.

==Plot== Tetsu Nakamura); and Frankie Banks (Joan Van Ark) a Pulitzer Prize winning photographer selected by the Press Pool. Maston, being his sexist self, goes on about how hes never taken a woman on his journeys and that hes not going to change now. But with some clever persuasion at a dinner for the crew, Frankie manages to convince Thrust to allow her on the expedition.

Upon arriving at the isolated valley, they notice flying Pteranodons.  Once they raft to shore, the group is almost stampeded by a "ceratopsian" (though in reality, the beast in question in not actually a dinosaur but resembles the lesser-known extinct mammal Uintatherium). After setting up camp, Maston, Chuck, Bunta, and Frankie go out looking for the dinosaur while Dr. Kawamoto remains back at the camp. The party locates the Tyrannosaurus, and after theyre chased, Thrust tries to shoot it, but his gun jams. Bunta spears it and is able to turn it away.  

Afterward, the Tyrannosaurus is fishing in a stream, and comes across the camp.  He destroys the camp and  kills Dr. Kawamoto by crushing him under his (overly) enormous foot.  Then he attacks the Polar Borer and throws it ashore to a canyon containing a bone field. While he continues his attack on the Polar Borer, a Triceratops unearths in the canyon and the two clash. After a fierce battle, the Tyrannosaurus is able to kill the Triceratops by biting an ugly gash in its neck.

The group returns to their destroyed camp and notice Dr. Kawamoto is gone, as well as the Borer. They mistakenly assume the borer was sunk. Enraged, Thrust vows to kill the dinosaur. After a few months, the group is now living in a cave. Thrust creates a crossbow out of Dr. Kawamotos items and uses tent pegs for arrows. They have a number of encounters with some cavemen in the area, but are able to turn them away with the crossbow. They also befriend a cavewoman (Masumi Sekiya) who has held onto Frankies purse (she lost after the ceratopsian attack) and Thrusts scope from his broken rifle. While Hazel (the name they give the cavewoman) helps Frankie wash her hair, the T. rex returns.  Frankie is able to make it to a cave, with the Tyrannosaurus trying to get in. Thrust, Bunta, and Wade are able to turn the animal with a large rock to his tail. Thrust decides to kill the dinosaur once and for all with a catapult.

Once built, they wait for the dinosaur. Out hunting, Chuck finds the Borer and says its still operable. However, Thrust doesnt want to leave until they kill the Tyrannosaurus. Nevertheless, Chuck and Frankie leave camp to get the Borer fixed and leave, while Thrust and Bunta remain. Once it is launched back in the water, Frankie goes back to convince the others to leave with them one last time. While tracking the dinosaur, Bunta is eaten. Frankie and Thrust use the catapult on it, but the boulder hit the dinosaur square on the head, but only knocks it over temporarily, not killing it. It then rampages and destroys their catapult.

Finally Chuck arrives and says they have to leave now or theyll be stuck there forever.  Frankie pleads with Thrust to go with them and to leave the dinosaur, as its the "last one."  Thrust replies "So am I..." and so they leave without him. Now its just Thrust (and Hazel) in this primeval world with "The Last Dinosaur."

==Production==
Unlike other bigger budgeted movies that have used state of the art effects (i.e.: Stop Motion, puppets, etc.) for the dinosaurs, this movie uses the cheaper "man in a suit" method, much like the Godzilla movies of the 1960s and 1970s. (The sound department have even borrowed Godzillas trademark roar and occasionally mixed it into the T. Rexs cry.) The "ceratopsian" (Uintatherium), as well as the Triceratops were done through the "two guys in a horse-suit" technique.  The scale (size) of the Tyrannosaurus also changes literally from scene to scene, in some cases it appears to be over 40–50 feet tall (when it attacks the borer) and can carry it in its mouth, when the Polar Borer is easily well over 10 feet in diameter.  However, they do correctly state in the beginning of the movie that a Tyrannosaurus Rex is 20 feet high and 40 feet long.

The suit of Tyranosaurus was created by Tsuburaya Production, and was converted from Ururu of Tokusatsu Anime Dinosaur War Aizenborg. Also while the film featured mostly an English speaking cast a Japanese dub was created for the television release in Japan. The Japanese theatrical release as well as the Japanese laser disc used the English voice cast with Japanese subtitles.

==Release==

On May 22, 2009, Toho Video released the movie on DVD for the first time ever. The DVD contains both English and Japanese audio tracks as well as an audio commentary in Japanese. This is the uncut Japanese release therefore it runs the full 106 minutes, rather than the 92 minute length of all the US release.

The DVD also contains a 13-minute interview with visual effects director Kazuo Sagawa, a photo gallery (which includes storyboards, production designs, and behind-the-scenes photos), it also contains a 15-minute behind-the-scenes production reel narrated by Kazuo Sagawa, and the original Japanese release trailer. For more info on special features and description of the DVD please referrer to the link below.

In the U.S., Warner Home Video released the movie on DVD through their Warner Archive Collection as a "made to order" DVD on March 22, 2011.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 