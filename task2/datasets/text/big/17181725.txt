Utsab
{{Infobox film
| name     = Utsab
| image          = Utsab_DVD_Cover.jpg
| caption = DVD cover of the film.
| director = Rituparno Ghosh
| writer = Rituparno Ghosh
| starring       = Madhabi Mukherjee Mamata Shankar Prasenjit Chatterjee Rituparna Sengupta Dipankar Dey Arpita Pal
| producer        = Sunil  Sharma
| distributor    = Shemaroo Entertainment
| cinematography = Abhik Mukhopadhyay
| editing = Arghya Kamal Mitra
| released   = 23 March 2000
| runtime        = 119 minutes Bengali 
| music = Debajyoti Mishra
}}
 Bengali drama film directed by Rituparno Ghosh and stars Madhabi Mukherjee, Mamata Shankar, Rituparna Sengupta, Prasenjit Chatterjee, Dipankar Dey and Arpita Pal. The film focus on the various emotional currents passing among family and relatives underneath the supposedly festive occasion of Durga Puja.

==Plot==
This is a family drama which is portrayed on a background of Durgapuja, West Bengals biggest "Utshob" (Festival). The story is about a cultured Bengali family, different members of which have gathered in native house on the occasion of Durga puja. Bhagbati (Madhabi Mukherjee) has four children;  two sons Asit (Pradip Mukherjee),Nishit (Bodhisattva Mazumdar) and two daughters Parul (Mamata Shankar)and Keya (Rituparna Sengupta). It is the festival time and all the children’s are at the main house to celebrate the festival. Meanwhile, Shishir (Dipankar Dey) a relative who is also a big real estate agent is interested in buying the house. Most of the family members want to sell the house. None of the sisters are looking forward to the family reunion as they all are more concerned about their own problems. Furthermore like any large family, every member of the family has his own personal past, skeletons and demons to confront.

==Cast==
* Madhabi Mukherjee as Bhagabati 
* Mamata Shankar as Parul 
* Rituparna Sengupta as Keya 
* Prasenjit Chatterjee as Arun
* Arpita Pal as Shompa
* Pradip Mukherjee as Asit
* Bodhisattva Mazumdar as Nishit

==Awards== National Film Awards (India) Best Director - Rituparno Ghosh    

==References==
 

==External links==
* 

 

 
 
 
 
 

 