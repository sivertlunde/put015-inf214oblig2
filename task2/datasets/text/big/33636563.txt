Janghwa Hongryeon jeon (1956 film)
{{Infobox film name           = The Story of Jang-hwa and Heung-ryeon  image          = File:Janghwa_Hongryeon_jeon_(1956)_poster.jpg caption        = 
| film name = {{Film name hangul         =    hanja          =   rr             = Janghwa Hongryeon jeon  mr             = Chang-hwa hong-ryŏn jŏn}} director       = Jeong Chang-hwa producer       = Kyeong-jun Kim writer         = Seong-min Kim  starring       = Kyeong-hie Lee  Yeong-ran Seo  music          = Baek-bong Jo cinematography = Yeong-sun Kim editing        = Chang-hwa Jeong released       =   runtime        =  country        = South Korea language       = Korean budget         =  gross          = 
}}

Janghwa Hongryeonjeon (literally The Story of Janghwa and Hongryeon) is 1956 South Korean horror film. The film is based on a popular Korean fairy tale "Janghwa Hongryeon jeon" which had been adapted into film versions in Janghwa Hongryeon jeon (1924 film)|1924, Janghwa Hongryeon jeon (1936 film)|1936, 1956, Dae Jang-hwa Hong-ryeon jeon|1962, Janghwa Hongryeon jeon (1972 film)|1972, A Tale of Two Sisters|2003, and The Uninvited (2009 film)|2009.

==Synopsis==
Janghwa Hongreonjeon is film based on a popular Korean fairy tale which had been previously filmed in 1924, 1936, and later filmed again in 1962, and Janghwa Hongryeon jeon (1972 film)|1972.  Director Kim Jee-woon used the story as the basis of his 2003 film, A Tale of Two Sisters.

==Cast==
* Kyeong-hie Lee
* Yeong-ran Seo
* Sok-Yang Choo
* Geum-seong Seok
* Wol-yeong Seo

==References==
 

==External links==
*  

 
 
 
 


 
 