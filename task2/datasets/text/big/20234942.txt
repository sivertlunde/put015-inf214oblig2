With Babies and Banners: Story of the Women's Emergency Brigade
{{Infobox film
| name           = With Babies and Banners: Story of the Womens Emergency Brigade
| image          =
| caption        =
| director       = Lorraine Gray
| producer       = Anne Bohlen Lyn Goldfarb Lorraine Gray
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 45 minutes
| country        = United States
| language       = English
| budget         =
| italic title   = force
}}
 Best Documentary Feature.      

This film was one of the first to put together archival footage with contemporary interviews of participants and helped spur a series of films on left and labor history in the US utilizing this technique. The film was also important in helping bring into view the history of American women being active in the public sphere, particularly in union and labor actions. The film was, further, ground breaking because it was produced and directed by  women.

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 