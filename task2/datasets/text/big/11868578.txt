Kadhalil Vizhunthen
{{Infobox film
| name           = Kadhalil Vizhunthen
| image          = Kadhalil Vizhunthen Poster.jpg
| caption        = Theatrical release poster
| director       = P. V. Prasad
| writer         = P. V. Prasad Nakul Sunaina Livingston Sampath Raj
| distributor    = Sun Pictures
| banner         = Atlantic Cinemas
| cinematography = S.D. Vijay Milton
| music          = Vijay Antony
| editing        = V. T. Vijayan
| producer       = S. Umapathi
| released       =  
| runtime        = 188 minutes
| country        = India
| language       = Tamil
| budget         =
}}
Kadhalil Vizhunthen (  and Sunaina. The film has music by Vijay Antony, cinematography by S.D. Vijay Milton and editing by V. T. Vijayan. The shooting for the film started in August 2007 and the film was released in September 2008.
 Sun Network Indian Premier League 2011 held at Chennai, India.
 Tamil lyrics as "Unakkena Naan Yenakkena Nee" displeased many of the films audience. The film was remade in Oriya as Diwana.

==Plot==
The story focuses on the life of Sabha (Nakul). He is the football crazy son of an alcoholic and lives in the slums. However, he lives a comfortable life and plays football with his mates as he completes his college career. He meets Meera (Sunaina) after an accident and later develops a liking for her. He slowly starts to love her but is afraid to express his love since she is rich and would react hastily. However, he later does confirm his love to her and she accepts. When Sabha leaves for a football match with his college, he promises that he will propose to her as soon as he gets back.Sabha returns from his match to only find out that Meera has died.
Sabha is shocked and he slips into a psychotic depression, travelling around with Meeras dead body, still believing she is alive. To make matters even worse, the police are also after him. The film ends as he commits suicide after he kills Meeras uncle (suresh), who murdered her in the first place in order to gain her wealth.

==Cast==
* Nakul as Sabha
* Sunaina as Meera
* Sampath Raj as Anbu Selvan Livingston as Train TTR

==Production==
Nakul, who was one of the heroes in Boys (2003 film)|Boys made his comeback through this film and he has reduced over 20 kilograms to play the lead role in the film.  A stunt sequence was shot in Mettupalayam and Nakul performed it without any dupe.  The film was shot at locations in Chennai, Ooty, Thalakkonam, Muthumalai and Kodaikanal.  The film marked the foray of the Sun Network into film distribution through their newly founded production company Sun Pictures.

==Soundtrack==
{{Infobox album
| Name       = Kadhalil Vizhunthen
| Longtype   = to Kadhalil Vizhunthen
| Type       = Soundtrack
| Artist     = Vijay Antony
| Cover      = Kadhalil Vizhunthen Audio Cover.jpg
| Caption    = Front Cover
| Border     = yes
| Alt        = 
| Recorded   = 2007
| Genre      = Film soundtrack
| Length     =  Tamil
| Label      = Junglee Music
| Producer   = Vijay Antony
| Last album = Pandhayam (2008)
| This album = Kadhalil Vizhunthen (2008)
| Next album = Budhivanta (2008)
}}

The soundtrack has nine tracks, composed by Vijay Antony except track Unakena Naan by the director himself. The lyrics were written by Thamarai, P.V.Prasad, Nepolian and Priyan. The song "Nakka Mukka" met with tremendous response from the youth and was on everyones lips. The soundtrack met with critical acclaim and made Vijay Antony famous. The song "Naaka Mukka" would eventually go on to be featured in the Bollywood film, The Dirty Picture

Tracklist
{{track listing
| extra_column    = Singer(s)
| total_length    = 39:01

| title1          = Doley Megha
| length1         = 4:17

| title2          = Nakku Mukka - Male
| extra2          = Vijay Antony 
| length2         = 4:56

| title3          = Solladi Tippu 
| length3         = 5:08

| title4          = Thozhiya
| extra4          = Harish Raghavendra, Sri Charan
| length4         = 4:47

| title5          = Enna Sonnen
| extra5          = Nitish Gopalan
| length5         = 1:09

| title6          = Un Thalai Mudi
| extra6          = Karthik (singer)|Karthik, Nithish Gopalan, Maya
| length6         = 5:27

| title7          = Nakku Mukka - Female
| extra7          = Nakul (actor)|Nakul, Chinnaponnu
| length7         = 3:17

| title8          = Unakkena Naan
| extra8          = Vijay Antony, Sakthi Sri Gopalan
| length8         = 3:44

| title9          = Kadavul Padaitha
| extra9          = Vijay Antony, Fathima
| length9         = 6:16
}}

==Release==
The film was lying in the cans for one year and Sun Pictures bought the film making it their first film.  The film opened alongside Sakkarakatti at the box office on September 26, 2008 with the media hyping that it was a battle of two films with debutants and successful soundtracks. 

The success of the film led Nakul and Sunaina reunite again in Maasilamani  which too was promoted by Sun Pictures and became successful. PV Prasad started another project "Eppadi Manasukkul Vanthai" for he was held in cheating case and the film was finally released in 2012. 

===Critical reception===
Indiaglitz wrote: "It is a feel-good movie with strong portrayals".  Sify wrote: "obsessive love story that turns out to be dreary and long drawn out".  Rediff wrote: "Kadhalil Vizhundhen achieves at least in part, what other love-stories fail to do -- to bring a genuine lump in your throat at times". 

==References==
 

==External links==
*  

 
 
 