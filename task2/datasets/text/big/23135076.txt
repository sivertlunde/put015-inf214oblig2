We Work Again
{{Infobox Film
| name           = We Work Again
| image          = 
| image_size     = 
| caption        = 
| director       =  Work Projects Administration
| writer         = 
| narrator       = 
| starring       =
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 
| runtime        = 15 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Work Projects Administration to promote its efforts at finding work for African-Americans during the Great Depression. Various jobs are shown, including construction, demolition, "domestics", school teaching, factory work and food preparation.

We Work Again has gained considerable attention because it includes the only known footage of the famous Voodoo Macbeth|all-black version of Macbeth staged by Orson Welles in 1936. This footage consists of the last few minutes of the play. 

A copy of the film is preserved by the National Archives and Records Administration. The film has also been released on DVD on the 4-disc box set Treasures from American Film Archives, compiled by the National Film Preservation Foundation.  It is one of the very few films of its genre ever to be released on DVD or any other home format. The film is in the public domain and as such has appeared on several websites, including the Internet Archive.

== Cast ==
 ]]
*Juanita Hall as Choir Director
*Maurice Ellis as Macbeth
*Charles Collins as Macduff
*Eric Burroughs as Hecate
*Zola King as Voodoo Macbeth Witch
*Josephine Williams as Voodoo Macbeth Witch
*Wilhelmina Williams as Voodoo Macbeth Witch

== Soundtrack ==
*"Ezekial Saw the Wheel" (Conducted by Juanita Hall)

==References==
 

==See also==
*Treasures from American Film Archives

== External links ==
* 
* 
*   — National Film Preservation Foundation


 
 
 
 
 
 
 
 
 
 
 
 


 