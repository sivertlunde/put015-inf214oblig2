Scandal at the Fledermaus
Scandal German musical film directed by Herbert Selpin and starring Viktor de Kowa, Maria Andergast and Adele Sandrock. 

==Cast==
* Viktor de Kowa - Viktor Kendal 
* Maria Andergast - Mary Hill 
* Adele Sandrock - Lady Bethy Malison 
* Heinz Salfner - Sir Thomas Berkham 
* Ernst Dumcke - Sir Anthony Garring 
* Alfred Abel - Patrick, Butler 
* Erich Fiedler - Jonny Dunn 
* Eliza Illiard - Evelyne Dixon 
* Max Gülstorff - Die Exzellenz 
* Roma Bahn - Lady X 
* Eva Tinschmann - Lady Y 
* Hedi Heising - Luise 
* Helmut Weiss - Diener 
* Ernst Stimmel - Kanzleidiener 
* Horst Teetzmann - Boy 
* Fred Goebel - Gast 
* Achim von Biel - Gast 
* Alfred Pussert - Gast

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Rentschler, Eric. The Ministry of Illusion: Nazi Cinema and Its Afterlife. Harvard University Press, 1996.

==External links==
* 

 

 
 
 
 
 


 