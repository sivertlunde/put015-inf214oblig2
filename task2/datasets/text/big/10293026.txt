Monster Man (film)
 Monster Man}}
{{Infobox film
| name = Monster Man
| image  = Monster man poster.jpg
| alt = 
| caption = Theatrical release poster Michael Davis
| producer = Larry Rattner
| writer = Michael Davis
| starring = Eric Jungmann Justin Urich Aimee Brooks Michael Bailey Smith
| music = John Coda
| cinematography = Matthew Irving
| editing = Kevin D. Ross
| studio = Dream Entertainment Lions Gate Films
| released =  
| runtime = 88 minutes
| country = United States
| language = English
| budget = 
}}
 comedy horror Michael Davis and stars Eric Jungmann, Justin Urich, Aimee Brooks, and Michael Bailey Smith.

==Plot==
 
Adam (Eric Jungmann) and Harley (Justin Urich) drive across the country so Adam can tell his ex-girlfriend he still loves her before she gets married. After an encounter with a hearse, the two stop at a pub. They see a monster truck rally on TV and Harley mocks the people watching it. As they are driving away, a giant monster truck drives them off the road. Later on, they have to siphon gasoline from an abandoned RV. However, it is revealed that the RV has a mutilated body inside and the RV is surrounded by truck tracks that form a pentagram. They later stop at a gas station and see the same monster truck. Adam sees the strange-looking driver and Harley urinates in the cab of the monster truck before they speed away. At a hotel, Adam and Harley wake up with roadkill in their beds; they find a hitchhiker named Sarah (Aimee Brooks) sleeping in the backseat when they get to the car. Sarah eventually has sex with Adam. 

Later, they witness the monster truck run over a man, and they meet a man missing an arm who tells them the creature in the monster truck takes peoples limbs, but lets the victims live. Afterwards, the three drive through a ghost town with many scarecrows. They find a diner at the end of the town and begin to eat, but Adam finds they are eating human flesh. They panic and run away. After being chased by the man in the monster truck, their car is destroyed. Adam, Harley, and Sarah run into the woods and the man follows them. He finally catches up with them and shoves Harley into a tree, supposedly killing him, and kidnaps Sarah. Adam follows him to a shack covered with pentagrams, severed limbs, and a man whose entire middle section has been crushed. Adam finally finds Sarah and tries to escape, but Sarah knocks him unconscious. Sarah and the man, Bob, are brother and sister. They tie Adam to a table while the "corpse" with the crushed midsection, Fred, explains that Bob accidentally ran him over, crushing his midsection and sending Bob through the windshield. Sarah says she stitched Fred up and used black magic to bring him back to life. Fred explains that they can use other peoples limbs to add to his own body as long as the donor stays alive and they could only transfer entire bodies if the body was prepared correctly. 

Sarah says that they needed someone easy to prepare and Adam was the right person. Previous events begin to make sense to Adam. Everything that happened to Adam and Harley before was preparing Adam so that Fred could have his body: stepping into a pentagram with a mutilated body, sleeping with roadkill, Sarah having sex with him, and eating human flesh. Adam manages to escape and kills Sarah by slashing her throat. He also cuts Fred in half when he gets up and starts to attack him. Meanwhile, Bob locks the door. When Adam tries to leave, Bob swallows the key. After a fight, Adam blinds Bob with a pencil. Fred trips Adam, who gets a nose bleed. Bob senses the blood and tells Adam that he can track him now with his own blood. Adam quickly thinks and leads a trail of blood to a blade that is stuck in a door. The plan works and Bob is impaled by the blade. Adam reaches into Bobs impaled stomach and takes the key that was swallowed. 
 playing dead and apologizes to Adam for everything he done to him since the beginning of the film, he offers adam to drive Bobs monster truck to which he happily agrees. Adam then runs over Bob with the monster truck simultaneously for hours . in the end, Adam thanks Harley for not only his help but the whole trip calling him a good friend, when Harley mentions about finishing the trip to Betty anns wedding, Adam gives up on getting Betty-Ann to fall in love with him, with that Harley then decides to get some food with Adam and drive away with Bobs truck, after driving away, the mutilated remains of Bob are seen saying "You cant kill me."

==Cast==
* Eric Jungmann as Adam
* Justin Urich as Harley
* Aimee Brooks as Sarah
* Michael Bailey Smith as Monster Man/Brother Bob
* Joe Goodrich as Brother Fred

==Reception==
Monster Man received mixed reviews; it currently holds a 60% fresh rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 