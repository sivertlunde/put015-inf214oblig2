Return to Life
 
{{Infobox film
| name           = Return to Life
| image          = ReturnToLife.jpg
| caption        = Film poster
| director       = Georges Lampin André Cayatte Henri-Georges Clouzot Jean Dréville
| producer       = Jacques Roitfeld
| writer         = Charles Spaak
| starring       = 
| music          = 
| cinematography = Nicolas Hayer Louis Page
| editing        = 
| distributor    = 
| released       =  
| runtime        = 120 minutes
| country        = France
| language       = French
| budget         = 
}}

Return to Life ( ) is a 1949 French drama film directed by Georges Lampin, André Cayatte, Henri-Georges Clouzot and Jean Dréville. It was entered into the 1949 Cannes Film Festival.     

==Cast==
* Paul Azaïs – the captain
* Bernard Blier – Gaston 
* Jean Brochard - the Hotel tender
* Léonce Corne - Virolet 
* Janine Darcey - Mary 
* Max Elloy - the old barman
* Louis Florencie - the chief of police
* Paul Frankeur - the maire
* Jacques Hilling - a soldier 
* Louis Jouvet - Jean Girard 
* Léon Larive - Jules, the guard
* Héléna Manson - Simone
* Marie-France Plumer - Aunt Berth
* Noël-Noël - René
* Jeanne Pérez - the mother
* François Périer - Antoine
* Serge Reggiani - Louis, the husband of the young German
* Patricia Roc - Lieutenant Evelyne 
* Noël Roquevert - the commander
* Maurice Schutz - the old man

==See also==
* Henri-Georges Clouzot filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 