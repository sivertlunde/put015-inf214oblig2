La Linea (film)
 
{{Infobox film
| name = La Linea
| image = La Linea (Promo 3).jpg
| director = James Cotten
| producer = Geoffrey G. Ross R. Ellis Frazier
| executive producer = Robert Rodriguez
| writer = R. Ellis Frazier Kevin Gage Danny Trejo Gary Daniels Joe Morton  and Armand Assante
| music = 
| cinematography = Miguel Bunster
| editing = 
| distributor = IFA Distribution (US)
| released =  
| runtime = 
| country = United States
| language = English
| budget = 
| gross =   
}}
La Linea ( ) is a 2009 action-crime film directed by James Cotten. La Linea features an ensemble cast that includes Ray Liotta, Armand Assante, Valerie Cruz, Esai Morales, and Andy García.

==Plot==
Veteran assassin Mark Shields (Ray Liotta) is hired to track down the head of an elusive drug cartel centered in Tijuana, Mexico. Shields takes the assignment in a weary daze, as he is fresh off a case that claimed the life of a woman he continues to see in his mind.

Meanwhile, Javier Salazar (Andy García), the head of the cartel Shields is assigned to, is dying. Salazar hands over his position to his nephew, the cocky Pelon (Esai Morales). Pelon takes charge with a different agenda, however, planning to change the cocaine being transported to heroin from Afghanistan.
 Kevin Gage), an old friend, to help in an assassination attempt. Pelon is leaving one of his warehouses when he is attacked not only by Shields and Wire, but a different group set up by a contractor named Anthony (Bruce Davison). A shootout ensues, ending in Anthonys team being forced to withdraw. Shields aborts the operation, but Wire is kidnapped.

Pelon sees Padre Antonio (Armand Assante) after the shootout. Then he goes to talk to Salazar, who tells his nephew that their operation wont ever be in jeopardy if the Americans simply keep taking out the leaders. Salazar believes that someone will always be there to take the seat of the fallen leader.

Shields is mugged but is found by a local woman named Olivia (Valerie Cruz), who takes him into her home. Shields stays until he recovers, then leaves. He later finds out that Wire was tortured and killed.

Pelon is attacked once more by Shields, and is kidnapped. Shields takes Pelon to Salazars home. It is revealed that Salazar faked his illness, hired Shields and set up an elaborate twist for his nephew. Pelon is executed by Padre Antonio, another dupe set up by Salazar. Shields then gives his pay to Olivia (local women) and boards a cab. On a ride to the airport Shields realizes that he was shot sometime when he was kidnapping Pelon, and he slowly lays down in the back of the cab and dies.

==Cast==
*Andy García as Javier Salazar
*Ray Liotta as Mark Shields
*Esai Morales as Pelon
*Jordi Vilasuso as Diablo
*Danny Trejo as Mario
*Bruce Davison as Anthony
*Valerie Cruz as Olivia
*Michael DeLorenzo as Pablo Kevin Gage as Wire
*Armand Assante as Padre Antonio
*Joe Morton as Hodges
*Victoria Elizabeth as Ana
*Kahlil Joseph as Omar

== External links ==
*  
*  
*  

 
 
 
 
 


 