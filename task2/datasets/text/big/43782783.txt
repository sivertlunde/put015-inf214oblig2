The Guide (film)
 
{{Infobox film
| name           = The Guide
| image          = The Guide poster.jpg
| caption        = International poster
| director       = Oles Sanin
| producer       = Maxim Asadchiy Igor Savychenko Oles Sanin
| writer         = Oles Sanin Alexander Irvanets Irene Rozdobudko Paul Wolansky
| starring       = Stanislav Boklan Jeff Burrell Anton Sviatoslav Greene Oleksandr Kobzar Jamala Iryna Sanina
| music          = Alla Zahaikevych
| cinematography = Serhiy Mykhalchuk
| editing        = Denys Zakharov
| studio = Pronto Film
| distributor    = B&H Film Distribution
| released       =  
| runtime        = 122 minutes
| country        = Ukraine
| language       = Ukrainian Russian English
| budget         = 
}}
 Best Foreign audiodescripted version for blind people. 

==Synopsis== Soviet Ukraine, American engineer Michael Shamrock arrives in Kharkiv with his ten-year-old son, Peter to help "build socialism". He falls in love with an actress Olga who has another admirer, Commissar Vladimir.

Under tragic circumstances, the American is killed and his son is saved from his pursuers by a blind bard (kobzar). With no other chance to survive in a foreign land, the boy becomes his guide.

==Cast==
* Stanislav Boklan as Ivan Kocherga
* Jeff Burrell as Michael Shamrock
* Anton Sviatoslav Greene as Peter Shamrock
* Oleksandr Kobzar as Comrade Vladimir
* Iryna Sanina as Orysia
* Jamala as Olga

== Awards and nominations ==
{| class="wikitable sortable"
|-
! Year !! Award !! Category !! Nominee !! Result
|- 2014
| Odessa International Film Festival 
| Best Actor
| Stanislav Boklan
|  
|-
| Jury Prize for Cinematography
| Sergiy Mykhalchuk
|  
|-
| Grand Prix
| Oles Sanin
|  
|-
| 2014
| Warsaw International Film Festival 
| Grand Prix
| Oles Sanin
|  
|}

==See also==
* List of submissions to the 87th Academy Awards for Best Foreign Language Film
* List of Ukrainian submissions for the Academy Award for Best Foreign Language Film
* Persecuted bandurists

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 


 
 