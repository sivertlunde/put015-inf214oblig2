Merry Mavericks
{{Infobox Film |
  | name           = Merry Mavericks|
  | image          = Merry mavericks 1sht.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Edward Bernds| Paul Campbell|
  | cinematography = Allen G. Siegler | 
  | editing        = Edwin H. Bryant |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       =   |
  | runtime        = 15 46"
  | country        = United States
  | language       = English
}}

Merry Mavericks is the 133rd short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Old West, Peaceful Gulch is not so peaceful as Morgan (Don C. Harvey) and his roughnecks have run the sheriff out of town. In attempt to bring normalcy back to their little town, some of the sheriffs posse concoct a scheme to trick Morgan and his hombres into thinking that there are three famous marshalls headed into town to bring back law and order. 
 Native American chief (John Merton). The trio soon find that the ghost is none other than one of Morgans men. Shemp knocks out the henchman and dons the costume for himself. He soon runs into Moe and Larry who have been captured by Morgan. Still disguised, Shemp knocks out everyone in the room with his hatchet and the boys are heroes once again.

==Production notes==
Merry Mavericks was filmed June 13-16, 1950, but not released until September 1951.    It is a partial remake of Phony Express, using minimal stock footage from the original.   

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 