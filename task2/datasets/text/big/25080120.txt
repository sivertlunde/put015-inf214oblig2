Return from the Ashes
{{Infobox film
| name           = Return from the Ashes
| image          = 
| image_size     = 
| caption        = 
| director       = J. Lee Thompson
| writer         = Julius J. Epstein (screenplay) Hubert Monteilhet (novel)
| narrator       = 
| starring       = Maximilian Schell Ingrid Thulin Samantha Eggar
| music          = John Dankworth
| cinematography = Christopher Challis Russell Lloyd
| distributor    = United Artists
| released       = November 19, 1965
| runtime        = 105 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Return from the Ashes is a 1965 British drama film directed by J. Lee Thompson. The film stars Ingrid Thulin, Herbert Lom, Maximilian Schell, and Samantha Eggar. It is based on a novel by French crime writer Hubert Monteilhet, adapted for film by prolific screenwriter Julius J. Epstein.

==Plot==
Shortly before the Nazi invasion of France, Dr. Michele Wulf (Thulin) encounters the younger Stanislaus Pilgrin (Schell) over a game of lightning chess, not being aware that Pilgrin is a chess master.  She becomes intrigued with the fortune-hunting Pilgrin and the two begin a liaison.  Upon the Nazi invasion, in order to protect Michele, who is Jewish, Stan marries her, to no avail it turns out when the Gestapo arrests her and sends her to a concentration camp.  

Sometime after the war, Michele returns under the identity of Mme. Robert and encounters her colleague, plastic surgeon Dr. Charles Bovard (Lom), who at first does not recognize her because of her disfigured state. She undergoes plastic surgery to restore some of her looks and then by chance encounters Stan, who believes her to have died during the war.  Surprised at the resemblance, Stan tells her that Micheles step-daughter Fabi (Eggar) cannot inherit her step-mothers estate because no body was ever produced.  He asks "Mme. Robert" to impersonate Michele and she agrees.  Upon moving back into her own house, Michele quickly becomes aware that Fabi, now a beautiful woman, resents her for her former neglect, and what is worse, is now Stans lover.  Eventually, Michele reveals herself and insists on resuming her relationship with her husband.  

While taking a bath after consuming alcohol and barbiturates, the jealous Fabi tells Stan her plan for killing her step-mother.  Stan will go to another city on the pretext of attending a chess championship.  He will set up a gun to go off when Michele opens her safe.  After establishing his alibi, he will call her and tell her that he has put a gift for her in the safe.  When he hears the shot, he will know she is dead, and he can return home to adjust the scene to make the death appear a suicide.

Though it is not clear if Fabi is serious, the amoral Stan drowns her in the bathtub in such a way that it looks like an accident related to an overdose, and then carries out her plan.  When he returns, he discovers what appears to be Micheles body, but is caught by the police in the act of manipulating the scene. Charles, who has loved Michele all along, had entered the room just as she was opening the safe and caused her to step aside to avoid the bullet.

==Response==
The movie would get a mixed response over the years, ranging from "good" to "implausible."

==Cast==
* Ingrid Thulin as Dr. Michele "Mischa" Wolf
* Samantha Eggar as Fabienne Wolf
* Maximilian Schell as Stanislaus Pilgrin
* Herbert Lom as Dr. Charles Bovard

==External links==
* 

 

 
 
 
 