Barfuss
{{Infobox film
| name           = Barfuss
| image          = Barfuss film.jpg
| image size     =
| caption        = German film poster
| director       = Til Schweiger
| producer       =
| writer         = Story By Stephen Zotnowski Screenplay By Til Schweiger Jann Preuss Nika von Altenstadt Stephen Zotnowski
| narrator       =
| starring       = Til Schweiger Johanna Wokalek
| music          = Ray Collins Hot Club (title song Barefoot)
| cinematography =
| editing        =
| studio         = Filmstiftung Nordrhein-Westfalen Mr. Brown Entertainment barefoot films
| distributor    = Buena Vista International
| released       = 2005
| runtime        = 118 minutes
| country        = Germany German
| budget         =
| preceded by    =
| followed by    =
}}

Barfuss (    by Robert M. Pirsig.

An American remake, Barefoot (film)|Barefoot was released in 2014.

==Plot==
The life of Nick Keller (Til Schweiger) can hardly be called well sorted. He stumbles from one temporary job to the next, and he has very serious problems with Heinrich, his rich and influential stepfather, as well as with his brother Viktor. Nick’s latest temporary job is as a cleaner in a psychiatric clinic, where he prevents the barefooted patient Leila (Johanna Wokalek|Wokalek) from committing suicide just as he is being fired from this latest employment. 

Leila’s story is also complex. The first nineteen years of her life she had been confined at home by her mother. She has been hospitalized in the clinic after her mothers death, but is desperate to leave. However, emotionally Leila is still a child.  For example, everything that she is told, she takes literally; and she dislikes physical contact with strangers. The unexpected consequence of Nick’s saving Leila from hanging herself is that Leila secretly follows her saviour, in her nightdress and once again barefooted; and she appears in front of his door that night.
After Leila adamantly refuses go back to the clinic, she and Nick go on a road trip together in order to attend his brother’s wedding to Nicks ex-girlfriend. During the trip the relationship between the two deepens significantly. However, after serious disputes with his family, Nick once again tries to hospitalize Leila.  As a result, he has to confess to himself that he has fallen in love with her.

Nick is then arrested for attempted kidnapping, and Leila is brought back to the clinic. Nick pretends to have mental problems so he can go into the clinic with Leila, but only after Leila once again attempts suicide does her doctor admit him. The last scene shows them together shopping in a supermarket some months later, after their release from the clinic.

== Cast ==
{| class="wikitable"
! Actor || Role
|-
| Til Schweiger || Nick Keller
|-
| Johanna Wokalek || Leila
|-
| Nadja Tiller || Frau Keller
|-
| Michael Mendl || Heinrich Keller
|-
| Steffen Wink || Viktor Keller
|-
| Alexandra Neldel || Janine
|-
| Imogen Kogge || Dr. Blöchinger
|-
| Janine Kunze || Sarah Sommer
|-
| Stefanie Stappenbeck || Jessica
|-
| Axel Stein || truck driver
|-
| Markus Maria Profitlich || car dealer 1
|- Mark Keller || car dealer 2
|-
| Éric Judor || Lhomme
|}

== Critical response == Bambi as the best German movie of 2005.

== External links ==
*    
*  

 

 
 
 
 
 
 


 
 