Super Rhino
{{Infobox Film 
| name = Super Rhino
| image = Super Rhino poster.jpg
| caption = 
| director = Nathan Greno
| writer =  Nathan Greno
| producer = Clark Spencer
| starring = {{Plainlist | Mark Walton
* Miley Cyrus
* Susie Essman
* Malcolm McDowell
* Sean Donnellan
}} John Powell
| editing = Shannon Stein
| studio = Walt Disney Pictures Walt Disney Animation Studios Walt Disney Studios Motion Pictures
| released =  
| runtime = 4 minutes 
| language = English
}}

Super Rhino is a 2009   and is a spin-off to the fictional TV show featuring Bolt.

Super Rhino was included in the 2009 DVD and Blu-Ray release of Bolt. 

==Cast== Mark Walton as Rhino
* Miley Cyrus as Penny
* Susie Essman as Mittens
* Malcolm McDowell as Dr. Calico
* Sean Donnellan as Pennys TV Dad
* Randy Savage as Thug

==Synopsis== Bolt have been captured by the evil Dr. Calico, suspended above a pool of lava (Calico is going to lower Penny and Bolt into the lava to kill them), inside a heavily guarded warehouse on an island in the middle of nowhere - a base which is impenetrable to both people and dogs. Pennys father watches the events from his lab through a secret camera imbedded in Bolts collar and worries that he cannot save her. Discovering that no man or dog can break into Dr. Calicos base, he turns to Rhino, who is watching TV in the background. In order to save Penny and Bolt, Rhino is put through the same procedure as Bolt to give him super-powers.

Rhino uses his newly found powers to fly across the sea and crash land outside the armed base encased in his Hamster ball|ball. Rhinos battle with the guard is witnessed by Penny, Bolt and Dr. Calico from the inside, with helicopters and cars crashing into the side of the building. As all the noise subsides a single knock on the door is heard and not receiving an answer Rhino uses his heat-vision to create an opening in the wall.

On the inside Rhino uses his eye-beams to defeat the armed guards and commandeers a flying missile, riding it like a surfboard to aim at Dr. Calico, though the missile misses. Instead Rhino uses his super squeek ability to finish the villain off. With Penny and Bolt saved Rhino walks away.
 The Best of Both Worlds"; the theme song from Hannah Montana (by co-star Miley Cyrus), where it is revealed that Rhino has been dreaming his adventures all along. Mittens wakes him up, telling him that shes not a fan of his singing.

==See also== List of Disney Animated Shorts and Featurettes
* Walt Disney Animation Studios

==References==
 
*  
*  
*  

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 