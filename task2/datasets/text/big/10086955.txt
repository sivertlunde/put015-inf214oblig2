Naked Tango
{{Infobox film 
  | name           = Naked Tango
  | director       = Leonard Schrader
  | producer       = David Weisman
  | writer         = Leonard Schrader
  | starring       = Vincent D’Onofrio Mathilda May Esai Morales Fernando Rey
  | music          = Thomas Newman
  | cinematography = Juan Ruiz Anchía
  | editing        = Debra McDermott
  | distributor    = New Line Cinema
  | released       = August 23, 1991
}}
 1991 film written and directed by Leonard Schrader. It starred Vincent DOnofrio, Mathilda May, Esai Morales and Fernando Rey. 

==Plot summary==
Returning by ship to Buenos Aires, a young woman escapes her elderly husband by swapping places with a woman committing suicide. She believes her new life will be that of an arranged marriage but finds it is in fact a ruse to cause her to work in a brothel. The film plays on the association of tango with brothels and clearly alludes to the practices of the Zwi Migdal white-slavery and prostitution ring that was active in Buenos Aires early in the twentieth century and was dissolved thanks to Raquel Liberman. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 