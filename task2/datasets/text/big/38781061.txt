Hope (2013 film)
{{Infobox film name           = Hope image          = File:Wish_poster.jpg director       = Lee Joon-ik producer       = Byun Bong-hyun   Seong Chang-yeon   Kim Yong-dae writer         = Jo Joong-hoon   Kim Ji-hye starring       = Sol Kyung-gu   Uhm Ji-won   Lee Re music          = Bang Jun-seok cinematography = Kim Tae-gyeong editing        = Kim Sang-beom   Kim Jae-beom distributor    = Lotte Entertainment released       =   runtime        = 122 minutes country        = South Korea language       = Korean budget         =  gross          =   film name      = {{Film name hangul         =   hanja          =  rr             = So-won mr             = Sowŏn}}
}}
Hope ( ), also known as Wish, is a 2013 South Korean film directed by Lee Joon-ik, starring Sol Kyung-gu, Uhm Ji-won and Lee Re.  It won Best Film at the 34th Blue Dragon Film Awards.            

The film is based on a true story, the infamous Nayoung Case in 2008, in which an 8-year-old girl called "Na-young" in the Korean press, was raped and beaten by a drunk 57-year-old man in a public toilet. The court sentenced the man to only 12 years in prison, which caused outrage in the country due to the terrible brutality of the crime and the mans history of physical and sexual violence.   

==Plot==
On her way to school, a young girl named So-won (which literally means "wish" or "hope" in Korean language|Korean) gets sexually assaulted by a drunk older male stranger. As a result, she suffers multiple internal injuries and has to undergo a major surgery, but her emotional wounds are equally difficult to heal. Their happy family shattered, her parents Dong-hoon and Mi-hee go through feelings of pain and rage. From the trauma of that day, So-won refuses to see or talk to her father, so Dong-hoon hides beneath the costume of his daughters favorite cartoon character and becomes a "guardian angel" at her side. Thanks to the love of those around her, So-wons condition gradually improves. At the sight of So-won slowly finding stability and laughter, her family also begins to change and enters a new phase in their lives, trying to find hope in the midst of their sorrow and despair.

==Cast==
*Lee Re as Im So-won
*Sol Kyung-gu as Im Dong-hoon, So-wons father
*Uhm Ji-won as Kim Mi-hee, So-wons mother
*Kim Hae-sook as Song Jung-sook, psychiatrist Kim Sang-ho as Han Gwang-sik, Dong-hoons best friend
*Ra Mi-ran as Young-seoks mother and Gwang-siks wife
*Yang Jin-sung as Do-kyung, police officer
*Kim Do-yeob as Han Young-seok

==Production== Battlefield Heroes (2011). He returned two years later with Hope, casting top actor Sol Kyung-gu in one of the lead roles.  Lee said he wanted to "make a happy movie that begins with a tragedy. With this heartbreaking material, I wanted to make the film as happy as possible. I am going to present a human drama where hope blooms at the edge of unhappiness and desperation, after a series of ordeals and hardships."    He maintained that he made the film to "encourage Na-young and other victims of sex crimes," and that instead of other films with a similar subject matter that focus on sensationalist aspects, like the crime itself, Hope is about "what happens after, and is more about showing how life is good and worth living, emphasizing how the community rallied around the victim." 

Actress Uhm Ji-won had previously declined the film two years before, thinking she wasnt talented enough or ready to take on the intense emotions her role (as So-wons mother) required. But when Sols wife and her close friend Song Yun-ah sent the script to her, Uhm said, "I started to think that this movie was supposed to come to me. I felt that someone should tell this valuable story. And I gained the courage to think that I could try it."   
 stayed in character as So-wons father, wearing his characters clothes all the time.  The film wrapped on June 24, 2013 in Busan; the entire cast attended the final day of filming.

==Reception== public holiday, Hope recorded 210,000 admissions, the daily highest during its screening period. It increased its ticket sales by chalking up a 16% increase from its October 3 tally of 187,804 admissions.  By its second week, it had reached 2.4 million admissions ( ), demonstrating it had strong legs by dropping only 8% and 29% in its second and third weekends.    It stayed on the box office top ten in its third week, adding 290,000 to its modest hit status, with 2.67 million admissions.   At the end of its run, its total admissions stood at 2,711,003, with a gross of  .

Hope was sold to five Asian countries—Japan, Hong Kong, Singapore, Malaysia and Indonesia at the Asian Film Market during the 18th Busan International Film Festival. Hong Kong-based major financing and distribution company EDKO commented on the film as "a warm and considerate account of a highly controversial subject matter." 

It was invited to be the opening film of the 8th Festival du Film Coréen à Paris ("Korean Film Festival in Paris") in 2013,  and shortly after, was also shown at the London Korean Film Festival. 

Hope was the surprise winner for Best Film at the 34th Blue Dragon Film Awards, defeating blockbusters Miracle in Cell No. 7, Snowpiercer and The Face Reader. It also took Best Screenplay and Best Supporting Actress for Ra Mi-ran. In her acceptance speech, a tearful Ra said, "I want to say this to the many girls out there who are in suffering like So-won—its not your fault. Its OK. Please cheer up."  Ra again won at the KOFRA Film Awards (organized by the Korean Film Reporters Association),  and Uhm Ji-won won Best Actress at the Korean Association of Film Critics Awards. 

Apart from accolades from critics and audiences for "its focus on family, heartfelt emotion and feel-good message,"  the film has also drawn criticism for "trying to turn one of the most infamous news stories in recent years into a maudlin piece of mass entertainment,"  and that "it is too idealistic in depicting how the family overcomes the assault,"  with some people questioning whether it was right to profit from such a horrible event, no matter the intentions of the filmmaker. 

==Awards and nominations==
{| class="wikitable"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
| rowspan=9| 2013 34th Blue Dragon Film Awards    
| Best Film || Hope ||  
|-
| Best Director || Lee Joon-ik ||  
|-
| Best Actor || Sol Kyung-gu ||  
|-
| Best Actress || Uhm Ji-won ||  
|-
| Best Supporting Actress || Ra Mi-ran ||  
|-
| Best Screenplay || Jo Joong-hoon, Kim Ji-hye ||  
|-
| Best Music || Bang Jun-seok ||  
|-
|   33rd Korean Association of Film Critics Awards    
| Best Actress || Uhm Ji-won ||  
|-
|   Korean Film Actors Association  
| Top Film Star Award || Uhm Ji-won ||  
|-
| rowspan=17| 2014
| rowspan=2|   5th KOFRA   Film Awards    
| Best Film || Hope ||  
|-
| Best Supporting Actress || Ra Mi-ran ||  
|-
| rowspan=2|   19th Chunsa Film Art Awards 
| Best Director || Lee Joon-ik ||  
|-
| Best Screenplay || Jo Joong-hoon, Kim Ji-hye ||  
|-
|   4th Beijing International Film Festival   
| Best Supporting Actress || Lee Re ||  
|- 50th Baeksang Arts Awards  
| Best Film || Hope ||  
|-
| Best Director || Lee Joon-ik ||  
|-
| Best Actor || Sol Kyung-gu ||  
|-
| Best Actress || Uhm Ji-won ||  
|-
| Best Supporting Actress || Ra Mi-ran ||  
|-
| Best New Actress || Lee Re ||  
|-
| Best Screenplay || Jo Joong-hoon, Kim Ji-hye ||  
|-
|   44th Giffoni Film Festival 
| Gryphon Award for Best Film (Generator +18) || Hope ||  
|-
| rowspan=4|   51st Grand Bell Awards 
| Best Film || Hope ||  
|-
| Best Director || Lee Joon-ik ||  
|-
| Best Actress || Uhm Ji-won ||  
|-
| Best Supporting Actress || Ra Mi-ran ||  
|-
|}

==Background==
 
 anus and intestines, which initially required her to wear a colostomy bag to replace her missing organs.   Cho was arrested three days after the incident; he was a habitual sex offender with 17 prior crimes, and had spent three years in prison for rape in 1983. 
 Daum calling Supreme Court maximum security prison in North Gyeongsang Province.
 minor (taping exhibiting a appellate division compensation to Na-young. 

==References==
 

==External links==
*    
*  
*  
*  

 

 
 
 
 