Ninja III: The Domination
{{Infobox Film | name = Ninja III: The Domination
 | image = Ninja III The Domination.jpg
 | caption = Original 1984 theatrical poster
 | director = Sam Firstenberg 
 | producer =  
 | writer = James R. Silke 
| starring = {{plainlist|
* Sho Kosugi
* Lucinda Dickey
* Jordan Bennett
}}
 | music =  
 | cinematography = Hanania Baer
 | editing =
 | distributor =  
 | released =  
 | runtime = 92 min.
 | country = United States
 | language = English
 | budget =
 | gross = $7,610,785
}}

Ninja III: The Domination is a 1984 film directed by  , and the second being Revenge of the Ninja, which are not directly related to one another in terms of storyline.

==Synopsis==
A telephone linewoman who teaches aerobics classes      is possessed by an evil spirit of a fallen ninja when coming to his aid.  The spirit seeks revenge on those who killed him and uses the female instructors body to carry out his mission.  The only way the spirit will leave the aerobic instructors body is through combat with another ninja.

==Home Media==
Shout! Factory released the film on Blu-ray Disc/DVD Combo Pack on June 11, 2013. 

==Soundtrack==
*Dave Powell – Body Shop
*Dave Powell – Starting Out Right
*Dave Powell – Welcome to the Party
*Misha Segal and Udi Harpaz – Synthesizer Score

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 