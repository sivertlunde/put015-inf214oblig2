Cup-tie Honeymoon
{{Infobox film
| name           = Cup-Tie Honeymoon
| image          =
| caption        =
| director       = John E. Blakeley
| producer       =
| writer         = John E. Blakeley and Arthur Mertz
| narrator       = Sandy Powell Dan Young Betty Jumel Patricia Phoenix
| music          =
| cinematography =
| editing        =
| distributor    = Mancunian Films
| released       =  
| runtime        =
| country        = United Kingdom
| language       = English
| budget         =
}}
Cup-Tie Honeymoon was the first motion picture to be filmed at the Dickenson Road Studios by the Mancunian Film Corporation in 1948, themed around football (soccer)|football.

==Plot summary==

A business mans son (Powell) has to choose between playing for his fathers team and their rivals in a football match. He does the right thing and romantically impresses his fathers secretary.

==Cast== Sandy Powell as Joe Butler 
* Dan Young as Cecil Alistair 
* Betty Jumel as Betty 
* Pat McGrath as Eric Chambers 
* Violet Farebrother as Mary Chambers 
* Frank Groves as Jimmy Owen 
* Joyanne Bracewell as Pauline May 
* Vic Arnley as Grandad 
* Harold Walden as Himself
* Barry K. Barnes as Grumpy customer  
* Pat Phoenix as Mrs. Butler (credited as Patricia Pilkington)
* Bernard Youens as Coalman (uncredited)
* David Edwin Vivian Coker Callan as Policeman

==Production==
Filmed in Rusholme, Manchester, much of the shooting took place on local streets and at the nearby Maine Road stadium.

==Cultural impact== 1948 football season.

==External links==
* 
* 

 
 
 

 
 