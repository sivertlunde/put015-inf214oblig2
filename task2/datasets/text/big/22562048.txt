Karin Daughter of Ingmar
 
{{Infobox film
| name           = Karin Daughter of Ingmar
| image          = 
| caption        = 
| director       = Victor Sjöström
| producer       = 
| writer         = Screenplay:  
| starring       = Victor Sjöström Tora Teje
| music          = 
| cinematography = Gustaf Bode Henrik Jaenzon
| editing        = 
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Sweden
| language       = Silent with Swedish intertitles
| budget         = 
}}
 silent drama film directed by Victor Sjöström.    It is the second part in Sjöströms large-scale adaption of Selma Lagerlöfs novel Jerusalem (novel)|Jerusalem, following Sons of Ingmar from the year before, and depicting chapter three and four from the novel. The critical reception was however unenthusiastic and Sjöström decided to not direct any more parts. Eventually the suite was finished by Gustaf Molander in 1926. 

==Cast==
* Victor Sjöström as Ingmar
* Tora Teje as Karin Ingmarsdotter
* Bertil Malmstedt as Lill-Ingmar
* Tor Weijden as Halfvor
* Nils Lundell as Eljas Elof Ersson
* Carl Browallius as Eljas father
* Josua Bengtsson as Eljas friend
* Nils Ahrén as Berger Sven Persson
* Olof Ås as Inspector Eric Gustafson as Innkeepers Son
* Emil Fjellström as Stark-Ingmar
* Paul Hallström

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 