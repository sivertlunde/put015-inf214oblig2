The Kreutzer Sonata (2008 film)
 
{{Infobox film
| name           = The Kreutzer Sonata 
| image          = Poster of the movie The Kreutzer Sonata.jpg
| caption        =  Bernard Rose
| writer         = 
| starring       = Danny Huston Elisabeth Rohm Anjelica Huston
| music          = 
| cinematography = 
| editing        = 
| distributor    = Axiom Films  
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Bernard Rose eponymous novella by Leo Tolstoy.

It is Roses second collaboration with Danny Huston (after Ivans XTC) and his third adaptation of a work by Leo Tolstoy, following 1997s Anna Karenina and 2003s Ivans XTC.

"The Kreutzer Sonata" is the name commonly given to Ludwig van Beethovens Violin Sonata no. 9 in A major. Director Bernard Rose had previously directed Immortal Beloved, a film on the life of Ludwig van Beethoven, in which there is a major (and pivotal) scene that features a performance of the Kreutzer Sonata.

==External links==
* 

 
 

 
 
 
 
 
 
 


 