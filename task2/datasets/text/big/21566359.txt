The Haunted House of Horror
{{Infobox film
| name = The Haunted House of Horror
| image = Horror house LC.jpg
| caption = Michael Armstrong
| producer =Tony Tenser executive Louis M. Heyward
| script = Michael Armstrong additional material Gerry Levy (as "Peter Marcus"
| music = Reg Tilsley
| starring = Frankie Avalon Jill Haworth Dennis Price
| cinematography = Jack Atcheler
| editing = Peter Pitt studio = Tigon British Film Productions American International Pictures AIP (USA)
| Tagline = Behind its forbidden doors an evil secret hides!
| released = July 1969 (UK) 15 April 1970 (U.S.) 20 February 1970 (West Germany) 20 February 1970 (Norway)
| runtime = 92 min
| country = United Kingdom English
| budget = £80,000 John Hamilton, Beasts in the Cellar: The Exploitation Film Career of Tony Tenser, Fab Press, 2005 p 130-134, 186-188 
}} Michael Armstrong who would go on to direct Mark of the Devil.

==Plot==
In swinging London, a group of twenty-something friends are attending a rather dull party, and they decide to gather for kicks at an old supposedly haunted mansion where one of their number used to play as a child.  Among the group is American ringleader Chris, his bored partygirl girlfriend Sheila, promiscuous Sylvia who has her eye on handsome two-timing Gary, and his "good girl" date Dorothy.  Also tagging along are nervous, heavy-set Madge and her sarcastic, hot-tempered boyfriend Peter, and sweet faced Richard (who suggested going to the mansion), and his friend Henry. They are all followed by Paul Kellet, Sylvias older jealous married ex-boyfriend.

They have fun exploring the mansion, even holding a seance before separating one by one by candlelight on the moonlit night.  Sylvia, frightened by the mansion, leaves and hitchhikes toward home, but Kellet hangs behind at the mansion. While all the partiers are alone, Gary is brutally knifed; his body is discovered by the panic-stricken Dorothy and the others. Since some of them have a criminal record, Chris convinces the group to leave Garys body far from the home and to pretend that Gary left and no one knows where he went. They are all shaken by Chris assertion that one of them must be the murderer.

During the next few weeks, the survivors are possessed by tension and guilt, and after Gary is reported missing, they are further shaken by questioning from the police.  Kellet confronts Sylvia, learning that she may have lost a lighter that could link them at the mansion.  He returns there where he is also killed.

Dorothy calls the survivors together to ask to confess.  However, Chris convinces them to return to the house to discover who among them is the killer before they all succumb to a gruesome death. Meanwhile, Sylvia is visited by the police again, and she discloses the location of the house after learning of Kellets disappearance. At the mansion, Dorothy becomes hysterical, prompting several of the group to depart, leaving just Chris, Sheila, and Richard. While Sheila is out of the room, Richard recounts a tale of how he was locked in a basement for three days as a child and how he has a paralyzing fear of the dark or anyone he suspects will lock him away.  Despite Chris efforts, he is also knifed and Sheila is frantically chased around the mansion.  Just as Richard is about to strike, the moon goes behind a cloud, bringing about his reversion to childhood and fear of the dark, thus saving Sheila as the police arrive.

==Cast==
*Frankie Avalon as Chris
*Jill Haworth as Sheila
*Dennis Price as Inspector
*Mark Wynter as Gary
*George Sewell as Kellett
*Gina Warwick as Sylvia Richard OSullivan as Peter
*Carol Dilworth as Dorothy Julian Barnes as Richard
*Veronica Doran as Madge
*Robin Stewart as Henry
*Jan Holden as Peggy
*Clifford Earl as Police Sergeant
*Robert Raglan as Bradley Nicholas Young as Party Guest

==Film Locations==
*Bank Hall, Bretherton, Lancashire, England, UK;
*Birkdale Palace Hotel, Southport, Merseyside, England, UK;
*Carnaby Street, Soho, London, England, UK
 .]]

==Film Notes==
*The screenplay by Michael Armstrong was originally entitled The Dark and had been written in 1960 when Armstrong was fifteen.   accessed 13 April 2014  John Trevelyan who recommended it to Tony Tenser of Tigon Films.
*Tenser set up the film with American International Pictures, who wanted it made in England, where it was cheaper to film than the US.
*AIP insisted a role be written for Boris Karloff who was very ill at the time. Accordingly Armstrong created the role of a wheelchair-bound detective. However Karloff was too ill to play it and Dennis Price took the role instead. AIP also insisted the two lead characters be Americanised to be played by American actors and for more sex scenes to be added. 
*   accessed 13 April 2014  
*Armstrong originally wrote the part of Richard for Peter McEnery but later rewrote it for David Bowie; he was so keen on Bowie he wrote a number of cabaret scenes in early drafts specifically for him. However once Avalon was cast it was considered that Bowie would clash with Frankie Avalon. He was replaced by Noel Janus but objections by Equity led to him being replaced with Julian Barnes (who had originally been cast as Henry).  
*Heyward wrote additional scenes for the film, to the dismay of Armstrong. Tenser tried to arrange it so that two versions would be made, Armstrongs and Heyward, but there was not enough money so a fourth draft was written which cobbled together all the drafts. 
* The films tagline was "Behind its forbidden doors an evil secret hides!"
===Shooting===
* Some exterior scenes where shot at Bank Hall in Bretherton
* The Interior Scenes where shot at the Birkdale Palace Hotel, Southport   accessed 13 April 2014 
===Additional Footage===
*Sam Arkoff and Jack Nicholson of AIP hated the original cut (which Armstrong says includes Heywards scenes) and requested new scenes. Armstrong wrote these and handed them to line producer Gerry Levy. Levy ignored Armstrongs scenes and wrote his own additional material, including a romance between Gina Warwick and a new character played by George Sewell. Levy also added two additional killings, a musical number in the opening scene and a revised closing exposition.    accessed 13 April 2014 
*Armstrong says among the scenes missing in the final cut of the movie were a low story between Gary and Sylvia, "twisted sexual meanderings of the characters", satire of the youth scene and a homosexual subplot.  

==Reception==
 
The films box office performance was reasonable. 

==References==
 

==External links==
*  
*   at TCMDB

 
 
 
 