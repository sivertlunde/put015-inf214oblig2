Mouna Geethangal
{{Infobox film
| name = Mouna Geethangal
| image = Mounageethangal.jpg
| image_size = 200 px
| caption = DVD cover
| director = K. Bhagyaraj 
| producer = K. Gopinathan
| writer = K. Bhagyaraj
| narrator =
| starring =  
| music = Gangai Amaran
| cinematography = B. S. Pasavaraj
| editing = T. Rajasekar
| studio = Bagavathy Creations
| distributor = Bagavathy Creations
| released = 23 January 1981
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
| amg_id =
}}
 Indian Tamil Hindi as Ek Hi Bhool in the same year. It was also remade in Telugu as Sathyabhama with Chandramohan and in Kannada as Mane Devru with Ravichandran.

==Plot==
Bhagyaraj and Saritha are a married couple. Bhagyaraj is a graduate and works as an administrator in a company. Their marriage comes to an end when Bhagyaraj betrays Saritha for her widowed friend. Bhagyaraj confesses the truth as he gets drunk, and tries to explain himself, and even her family tries to convince her to condone this one little mistake, but Saritha cannot forgive him his unfaithfulness and they break up. After their divorce, Saritha finds out that she is pregnant. She moves into her new home. Five years later, they meet each other in a bus. Coincidentally, Bhagyarajs new house is close to Sarithas in the same neighborhood. Now when theyre neighbours, Bhagyaraj tries to captivate her again and return his family and wife. He gets close to his son . Though his son speaks well with bhagyaraj, saritha dislikes it, telling her (also his) son not to talk with him. As Days passes by saritha keeps on thinking about good things he did to her when he was with her. In a  controversy between him and saritha, bhagyaraj slaps her saying he gives 30 days to return to hi, though she refuses it, later she realises her faults, falls again in love with her. In climax bhagyaraj marries another girl . Changed up saritha comes there, begs him not to Marry another girl than her,he refuses her requests and marries. Saritha dares to die with her son. Then the clerk arrives there telling whole thing was a drama and bhagyaraj is still clean. Movie ends up with saritha walking back to his house.

==Soundtrack==
The music composed by Gangai Amaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || "Daddy Daddy" || Malaysia Vasudevan, S. Janaki || Muthulingham || 4:02
|-
| 2 || "Masamo Margazhi Maasam" || Malaysia Vasudevan, S. Janaki || Kannadasan || 4:14
|- Vaali (poet)|Vaali || 4:51
|-
| 4 || "Mookuthi Poo Melae" (Sad) || S. P. Balasubrahmanyam || 4:37
|}

==References==
 

==External links==
*  

 

 
 
 
 
 