From Beyond the Grave
 
 
{{Infobox film
| name           = From Beyond the Grave
| image          = Frombeyondgraveposter.jpg
| image_size     =
| caption        = Belgian poster Kevin Connor
| producer       = Max Rosenberg Milton Subotsky
| writer         = Raymond Christodoulou Robin Clarke David Warner
| music          = Douglas Gamley
| cinematography = Alan Hume
| editing        = John Ireland
| distributor    = Amicus Productions
| released       = 1974
| runtime        = 97 minutes
| country        = United Kingdom
| language       = English
}} anthology horror Kevin Connor, produced by Milton Subotsky and based on stories by R. Chetwynd-Hayes
 Torture Garden Tales from The Vault of Horror (1973). Ed. Allan Bryce, Amicus: The Studio That Dripped Blood, Stray Cat Publishing, 2000 p 126-137 

Originally filmed as The Undead, it is also known as The Creatures, Tales from Beyond the Grave, and Tales from the Beyond.

==Plot==
Four customers purchase (or take) items from Temptations Limited, an antiques shop whose motto is "Offers You Cannot Resist". A nasty fate awaits those who cheat the shops Proprietor (Peter Cushing).

The Gatecrasher
 David Warner) purchases an antique mirror for a knockdown price, having tricked the Proprietor into believing it is a reproduction. When he takes it home, Charlton holds a séance at the suggestion of his friends, and falls into a trance. He finds himself in a netherworld where he is approached by a sinister figure (Marcel Steiner). The figure appears to stab him, and Charlton awakes screaming.
Later, the figures face appears in the mirror and orders Charlton to kill so that he can "feed". Charlton butchers people until the apparition is able to manifest himself outside of the mirror. The figure then explains that Charlton must do one more thing before the figure can walk abroad and join the others like him. The figure says he will take Charlton "beyond the ultimate", and persuades Charlton to kill himself by impaling himself on a knife.
The mirror stays in Charltons flat for years after his death, until the latest owner also decides to hold a séance. Once the séance starts, Charltons hungry spectre appears in the mirror.

An Act of Kindness

Christopher Lowe (Ian Bannen) is a frustrated middle management drone trapped in a loveless marriage with Mabel (Diana Dors). Bullied by his wife, and shown no respect by his son, he befriends Jim Underwood (Donald Pleasence) an old soldier now scratching out a living as a match and shoe lace seller.
In an effort to impress, Lowe tells Underwood that he is a decorated soldier. To back up this lie, he tries to persuade the Proprietor to sell him a Distinguished Service Order medal. When the Proprietor asks that Lowe provide the certificate to prove he had previously been awarded the medal, Lowe steals the medal. Underwood is impressed by the medal, and asks Lowe to come to his house for tea. Once there he meets Underwoods daughter, Emily (Angela Pleasence). Over time Lowe is seduced by Emilys frankly rather creepy charms, and they start an affair.
Emily then produces a miniature doll of Mabel, and holds a knife to it. She asks Lowe to order her to do his will. Lowe agrees that she should cut the doll. When she does, a drop of blood appears from its mouth. A disturbed Lowe dashes home to find Mabel dead. Underwood and Emily then appear at Lowes home, and walk in to the sound of the wedding march. John OFarrell) and Jim Underwood attend the wedding. When the time comes to cut the cake, Emily asks all present whether they wish her to. They all agree and Emily brings the knife down, but rather than cut the cake, she cuts into the head of the decorative groom on top. Blood pours out of it, and Lowe falls on to the table, dead. Underwood and Emily explain to Lowes son that they always answer the prayers of a child "in one way or another".

The Elemental

Reggie Warren (Ian Carmichael) is a somewhat pompous business man who enters Temptations Ltd and puts the price tag of a cheaper snuff box in the one he wants to buy, whilst out of sight. The Proprietor sells him the box at the altered price, bidding him farewell with a cheery "I hope you enjoy snuffing it."

On the train home, an apparently batty old clairvoyant/white witch, Madame Orloff (Margaret Leighton) disturbs Warren whilst he reads his paper, advising him he has an Elemental on his shoulder. Warren dismisses her, but has cause to call on her services when his dog disappears and his wife Susan (Nyree Dawn Porter) is attacked and choked half to death by an unseen force.
Orloff exorcises the Elemental from Warrens home, and all seems well—even the dog returns. Later though the Warrens hear noises up stairs, and Reggie heads up to investigate. He is knocked down and falls to the foot of the stairs, unconscious. When he awakes, he finds Susan possessed by the elemental. She/It says Reggie tried to deny her life, and kills him before cackling and having a smashing time walking through the front door.

The Door

William Seaton (Ian Ogilvy) is a writer who purchases an ancient ornate door from the Proprietor. He is unable to meet the Proprietors asking price, but agrees a reduced price with him. When the Proprietor goes to the back of the shop to note Seatons details, he leaves the till open. After Seaton leaves, the Proprietor starts counting the money in the till. Jack Watson), an evil occultist who created the door as a means to trap those who entered through it, so that Sinclair can take their souls and live forever.
Seaton escapes, but when he tries to leave his house he finds that the doors influence has spread, and he and Rosemary are trapped. In a trance, Rosemary is unable to stop herself from opening to the door and entering the room, where she is incapacitated by Sinclair. Sinclair carries her through the doorway, mocking Seaton by asking him to follow, as two souls are better than one.
Seaton starts to smash the door with an axe, and the room and Sinclair start to crumble. Seaton tries to rescue Rosemary, but is attacked by Sinclair. Seaton has Rosemary continue axing the door, and manages to break free. They continue demolishing the door, destroying the room and turning Sinclair to a skeleton and then dust when they break the door from its hinges. Back at the shop, the Proprietor finishes counting and finds all the money present and correct.

Between each of the segments, a shady character (Ben Howard) is seen to be casing the shop. In the end, he enters and persuades the Proprietor to hand him two loaded antique pistols. He then tries to rob the Proprietor, who refuses to hand him any money and walks towards the thief. The thief shoots, but finds bullets cannot stop the Proprietor. Terrified, the thief staggers back, is hit by a swinging skeleton, falls into what appears to be a combination of a coffin and an iron maiden, and is spiked to death. "Nasty", the Proprietor says. The Proprietor then welcomes the viewer as his next customer, and explains he caters for all tastes, and that each purchase comes with "a big novelty surprise".

==Cast==
*Peter Cushing as Antique Shop Proprietor
*Donald Pleasence as Jim Underwood
*Angela Pleasence as Emily Underwood
*Ian Bannen as Christopher Lowe
*Diana Dors as Mabel Lowe John OFarrell as Stephen Lowe
*Nyree Dawn Porter as Susan Warren David Warner as Edward Charlton
*Marcel Steiner as Mirror Demon
*Ian Ogilvy as William Seaton
*Ian Carmichael as Reggie Warren
*Lesley-Anne Down as Rosemary Seaton Jack Watson as Sir Michael Sinclair
*Margaret Leighton as Madame Orloff
*Wendy Allnutt as Pamela
*Rosalind Ayres as Prostitute/Edwards first victim
*Tommy Godfrey as Mr. Jeffries
*Ben Howard as Burglar

== Critical reception ==

AllRovi|Allmovies review of the film was generally favourable, writing, "The last of the Amicus anthologies is a fun, old-fashioned example of the form." 

== References ==

 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 