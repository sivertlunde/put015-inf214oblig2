Waves '98
{{Infobox film name           = Waves 98
| image          = director       = Ely Dagher writer         = Ely Dagher producer = Ely Dagher
|co-producer = Nina Najjar Christina Farah music          = Zelig Sound Animation = Ely Dagher Laure Escafadals Chadi Aoun editing        =  distributor    =  released       =   runtime        = 15 minutes country        = Lebanon, Qatar language       = Arabic budget         =  gross          = 
}}
 short animated film written and directed by Lebanese filmmaker Ely Dagher, nominated for the Short Film Palme dOr at the 2015 Cannes Film Festival. 

==Plot==
Waves 98 tells the story of Omar, a high-school kid living in the northern suburb of Beirut in the late 1990s, struggling with his disillusionment with his social bubble.  On a quiet afternoon, on a terrace looking over the city he notices something strange, a sort of Giant golden animal protruding from between the buildings that draws him and leads to his discovery of a special part of the city.  The film is a meditiation on Daghers relationship to Beirut, his hometown, since he has left it for Brussels. 

==Awards==
*Cannes Film Festival, Short Films Competition, Official Selection - Palme DOr, Nominee (2015)

==Reception== Hors La Vie in 1991.  
The film was shortlisted as one of 8 from a list of 4550 short film submissions. The Palme DOr will be awarded by Abderrahmane Sissako, in a ceremony on May 24, 2015. 

==References==
 

==External links==
* 
* 

 
 
 
 