To Sir, with Love
 
 
 
{{Infobox film
 | name = To Sir, with Love
 | image = To-sir-with-love-movie-poster-1967.jpg
 | caption = UK theatrical release poster
 | director = James Clavell
 | producer = James Clavell
 | based on =  
 | writer = James Clavell Christian Roberts Lulu
 | music = Ron Grainer
 | cinematography = Paul Beeson
 | editing = Peter Thornton Columbia British Productions
 | distributor = Columbia Pictures
 | released =  
 | runtime = 105 min
 | country = United Kingdom
 | language = English
 | budget = $640,000
 | gross = $42,432,803   
}} To Sir, With Love (1959).
 To Sir With Love", sung by Lulu (singer)|Lulu, reached number one on the U.S. pop charts for five weeks in the autumn of 1967 and ultimately was Billboard magazine|Billboard magazines No. 1 pop single for that year. The movie ranked number 27 on Entertainment Weekly s  list of the  .
 released nearly three decades later, with Poitier reprising his starring role.

==Plot==
Mark Thackeray (Poitier), an unemployed engineer, applies for a teaching position at the North Quay Secondary School in the tough East End of London. He comes from British Guyana via California.
 Christian Roberts) and Pamela Dare (Judy Geeson), their antics progress from disruptive behaviour to distasteful pranks. Thackeray retains his calm manner but a turning point comes one morning when he discovers one of the female pupils has mischievously left a used sanitary towel burning in the classroom grate. He loses his temper, then informs them that from now on they will be treated as adults and allowed to discuss issues of their own choosing for the remainder of the term.

Thackeray wins the class over, except for Denham, who continues to bait him. Thackeray suggests a class outing to a museum, which turns out to be a success. He loses some of this new-found support when he defuses a potentially violent situation between Potter (Chris Chittell) and a gym teacher, Mr Bell. In class, he demands that Potter apologise directly to Bell for the incident even if he believes Bell was wrong. The group refuse to invite Thackeray to the class dance, and when Seales (Anthony Villaroel, the only black pupil in the class) mother dies, the class takes up a collection for a wreath but refuses to accept Thackerays donation. At this point, the headmaster advises him that he feels "the adult approach" has failed; future class outings are cancelled, and Thackeray is to take over the boys gym classes. Meanwhile Thackeray receives an engineer job offer in the mail.

He starts to win the pupils back after he beats Denham in a boxing match, but tells him that he has genuine boxing ability and suggests that Denham teach boxing to the younger pupils next year. Denham expresses his admiration for Thackeray to his fellow pupils, Thackeray wins back their respect and is invited to the class dance.

At the dance Barbara Pegg (Lulu (singer)|Lulu) announces a "ladies choice" dance and Pamela singles out Thackeray as her partner. The class present him with a gift, while Lulu sings the film theme. Thackeray is too moved for words and retires to his classroom.

Two youths rush into the classroom, and upon seeing Thackeray they begin mocking his gift and joking that they will be in his class next year. Thackeray realises that he has a job to do and he tears up the job offer letter, signifying that he is going to stay on at the school. He realises how affectionate he feels towards the children and understands he can never part from them.

==Cast==
 
* Sidney Poitier as Mark Thackeray Christian Roberts as Bert Denham
* Judy Geeson as Pamela Dare
* Suzy Kendall as Gillian Blanchard
* Ann Bell as Mrs. Dare
* Geoffrey Bayldon as Theo Weston
* Faith Brook as Grace Evans
* Patricia Routledge as Clinty Clintridge
* Chris Chittell as Potter
* Adrienne Posta as Moira Joseph Lulu as Barbara "Babs" Pegg
* Edward Burnham as Florian
* Rita Webb as Mrs. Joseph
* Marianne Stone as Gert
* Michael Des Barres as Williams
* The Mindbenders
 

==Reception== The Blackboard Jungle; unlike that earlier film, Crowther says "a nice air of gentility suffuses this pretty color film, and Mr. Poitier gives a quaint example of being proper and turning the other cheek. Although he controls himself with difficulty in some of his confrontations with his class, and even flares up on one occasion, he never acts like a boor, the way one of his fellow teachers (played by Geoffrey Bayldon) does. Except for a few barbed comments by the latter, there is little intrusion of or discussion about the issue of race: It is as discreetly played down as are many other probable tensions in this school. To Sir, with Love comes off as a cozy, good-humored and unbelievable little tale." 

Halliwells Film and Video Guide describes it as "sentimental non-realism" and quotes a Monthly Film Bulletin review (possibly contemporary with its British release), which claims that "the sententious script sounds as if it has been written by a zealous Sunday school teacher after a particularly exhilarating boycott of South African oranges". 

The Time Out Film Guide says that it "bears no resemblance to school life as we know it" and the "hoodlums miraculous reformation a week before the end of term (thanks to teacher Poitier) is laughable".  Although agreeing with the claims about the films sentimentality, and giving it a mediocre rating, the Virgin Film Guide asserts: "What makes   such as enjoyable film is the mythic nature of Poitiers character. He manages to come across as a real person, while simultaneously embodying everything there is to know about morality, respect and integrity." 
 Up the Down Staircase, appeared.
 review aggregate eighth highest grossing picture of 1967 in the US. Poitier especially benefited from that films success considering he agreed on a mere $30,000 fee in exchange for 10% of the gross box office and thus arranged one of the most impressive payoffs in film history. In fact, although Columbia insisted on an annual cap to Poitier of $25,000 to fulfill that percentage term, the studio was forced to revise the deal with Poitier when they calculated they would be committed to 80 years of those payments. 

==Soundtrack==
{{Infobox album 
| Name        = To Sir, with Love
| Type        = soundtrack
| Artist      = various
| Cover       = To-sir-with-love.jpg
| Released    = 1967
| Recorded    = Traditional pop
| Length      =
| Label       = Fontana Records (UK)
| Producer    =
| Misc        = {{Singles
  | Name           = To Sir, with Love
  | Type           = soundtrack To Sir With Love
  | Single 1 date  = 1967
  }}
}}
The soundtrack album features music by Lulu (singer)|Lulu, The Mindbenders, and incidental music by Ron Grainer. The original album was released on Fontana Records. It was re-released onto CD in 1995. AllMusic rated it three stars out of five. 
 Cash Box Top 100 number-one single for three weeks.  To Sir Lulu
# School Break Dancing "Stealing My Love from Me" - Lulu
# Thackeray meets Faculty, Then Alone
# Music from Lunch Break "Off and Running" - The Mindbenders
# Thackeray Loses Temper, Gets an Idea
# Museum Outings Montage "To Sir, with Love" - Lulu
# A Classical Lesson
# Perhaps I Could Tidy Your Desk
# Potters loss of temper in gym
# Thackeray reads letter about job
# Thackeray and Denham box in gym
# The funeral
# End of Term Dance "Its Getting Harder all the Time" - The Mindbenders
# To Sir With Love - Lulu

==Accolades==

===Awards===
Laurel Awards
*Sleeper of the Year: 1968 
*Female New Face – 2nd place: Judy Geeson – 1968 

===Nominations===
Directors Guild of America
*Outstanding Directorial Achievement in Motion Pictures: James Clavell – 1967 
Laurel Awards Christian Roberts – 1968 
10th Annual Grammy Awards
*Best Original Score from a Motion Picture or Television Show: 1968 

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 