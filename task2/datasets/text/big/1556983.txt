Just Another Missing Kid
{{Infobox film
| name           = Just Another Missing Kid
| image          = 
| caption        = 
| director       = John Zaritsky
| producer       = Glenn Sarty John Zaritsky
| writer         = 
| starring       = 
| music          = 
| cinematography = John Griffin
| editing        = Gordon McClennan
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Canada
| language       = English
| budget         = 
}}
Just Another Missing Kid is a 1981 documentary film about the search for a missing teenager and directed by John Zaritsky.
 the fifth estate reported that Hatch had resumed his drifting. He died in 2000. 

Originally produced by CBC Television for the documentary news program the fifth estate, it was broadcast on CBC television to much acclaim in 1981. It was released in theatres in the United States in 1982. In Canada it won an ACTRA Award for the best television program and garnered a number of international awards, including the Academy Award for Best Documentary Feature for 1982.   

In this film, Zaritsky broke new ground for documentaries by having the interview subjects recreate their actions for the camera.  This caused some controversy as some critics and filmmakers felt these recreations did not make it a true documentary.  In later years Zaritsky himself agreed the technique should not be used.  However it has since been widely used by other documentary filmmakers.
 Into Thin Air. The film starred Ellen Burstyn as the mother and was aired in 1985.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 

 