The Winter of Our Discontent (film)
{{Infobox television film
| name           = The Winter of Our Discontent
| image          = TheWinterofOurDiscontent1983.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| genre          = Drama
| director       = Waris Hussein
| producer       = R.W. Goodwin
| screenplay     = Michael De Guzman	
| based on       =  
| narrator       = 
| starring       = Donald Sutherland Teri Garr Tuesday Weld
| music          = Mark Snow
| cinematography = Robbie Greenberg
| editing        = Fred A. Chulack
| studio         = Hallmark Hall of Fame Productions Lorimar Television
| distributor    = CBS
| released       =  
| runtime        = 105 minutes
| network        = CBS
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} novel of the same name by John Steinbeck.

==Plot== Italian immigration|immigrant (played by Michael V. Gazzo). His wife (Teri Garr) and kids want more than what he can give them because of his lowly position. 

He finds out that the immigrant that owns his store is an illegal alien, turns him into the Immigration and Naturalization Service, and receives the store by deceiving the immigrant. Ethan continues to have feelings of depression and anxiety brought about by his uneasy relationship with his  wife and kids, risky flirtation with Margie Young-Hunt (Tuesday Weld), and consideration of a bank robbery scheme.

==Cast==
* Donald Sutherland as Ethan Hawley
* Teri Garr as Mary Hawley
* Tuesday Weld as Margie Young-Hunt
* Michael V. Gazzo as Marullo
* Richard Masur	as Danny
* E.G. Marshall as Mr. Baker
* Kirk Brennan as Allen Hawley
* Amanita Hyldahl as Ellen Hawley
* Nan Martin as Mrs. Baker
* Macon McCalman as Reverend Sloane

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 

 