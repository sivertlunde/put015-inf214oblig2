Aishwarya (film)
 
 
{{Infobox film
| name           = Aishwarya
| image          = Aishwarya Official DVD Poster.jpg 
| alt            =  
| caption        = 
| director       = Indrajit Lankesh
| producer       = N. Kumar
| writer         = B. A. Madhu   (dialogues ) 
| screenplay     = 
| story          = 
| starring       = Upendra  Deepika Padukone  Daisy Bopanna  Kishan Shrikanth
| music          = Rajesh Ramanath
| cinematography = 
| editing        =  
| studio         = 
| distributor    = 
| released       = 15 September 2006
| runtime        = 147 mins 
| country        = India
| language       = Kannada
| budget         =  30&nbsp;million
| gross          =  50 million
}}

Aishwarya is a 2006   movie by the name Ghajini (2005 film) and main plot is from Telugu blockbuster hit Manmadhudu (2002 film) starring Nagarjuna and Sonali Bendre.

==Plot==
Abhishek Hegde (Upendra) had first fallen in love with a young lady Anjali (Daisy Bopanna) who was a model. 
As a struggling model, Anjali finds herself in a fortunate position where she is mistaken to be girlfriend to Abhishek and gets many new offers. Abhishek tries to confront her on seeing the news about his affair with her, but falls in love with her instead. Anjali dies in an accident when the two declare their love and are hugging, Abhisheks car hits a lorry. As Anjalis family were not in favour of her decision of marrying Abhishek, he is informed that Anjali has married the person she has been engaged to and did not care to tell him that Anjali has actually died and visit him, even when he was in hospital recovering from injuries after the accident. Abhisheks grandfather had faked an invitation card to Anjalis wedding only to keep him away from further mental trauma. This angers Abhishek and he starts to hate women with the generalisation that all women are like Anjali. After a long gap Aishwarya (Deepika Padukone) joins the company he works in which is owned by his uncle as an assistant manager. After a stay for a few days in the office, she gets Abhishek fired and she takes the position of manager. Abhishek now not knowing what to do talks to his aunt, but is interrupted by his uncle, who is mean to him. Finally, he takes the job of assistant manager as that is the only job available. Within a few days they assigned a task at Vienna. The two travel to Europe on business for ten days and learn from each other. Aishwarya transforms Abhishek by her assertive and fun-loving nature. They fall in love with each other but dont express it. Abhishek finds out that Aishwarya is getting engaged as decided by her family to a very strict family from Mandya. Aishwarya is aware of Abhisheks feelings for her. Both of them finally speak out their love. Now, Abhishek, to save his love, drives to reach Aishwarya. At that moment, Aishwarya will be on a boat travelling to the place where she has to get marry. Abhishek reaches there and shouts out for Aishwarya, and she responds by jumping into the river. They both reunite and share their moment of joy by telling how much they love each other. The movie ends on a happy note and they are married by the end.

==Cast==
* Upendra as Abhishek Hegde
* Deepika Padukone as Aishwarya
* Daisy Bopanna as Anjali
* Komal
* Kishan Shrikanth Ramakrishna
* Sadhu Kokila Sharan
* Ramesh Bhat
* Doddanna
* Om Prakash Rao

==Crew==
* Director – Indrajit Lankesh
* Producer – N Kumar
* Written – B A Madhu
* Music By – Rajesh Ramanath

==Box office==
Aishwarya got a tremendous opening across Karnataka upon release.  The film got a grand opening both in single screens and multiplexes as well. The film broke several opening box office records by generating a nett share of  1.5 crore and a total gross of  20&nbsp;million during its first week,  out of which more than  10&nbsp;million came from BKT (Bangalore, Kolar, Tumkur) alone,  once again proving Upendras strong foothold in the BKT region after his earlier record breaking opener Omkara (2004 film)|Omkara which had also collected  10&nbsp;million nett from the BKT region in its first week.  After its amazing performance at the box office during its first 2 weeks, the film was already declared a big hit.  Aishwarya went on to complete 50 days of run at 30 centres across Karnataka  and 75 days of run in Bangalore, and was a commercial success, although it could not reach expectations. Aishwarya grossed a total of  50&nbsp;million in its entire run of 75 days and was among the top 5 hit films of 2006. 

==Soundtrack==

{{tracklist
| extra_column    = Singers
| title1          = Aishwarya Aishwarya
| extra1          = Kunal Ganjawala
| length1         = 
| title2          = Dhoni Dhoni Karthik
| length2         = 
| title3          = Manmatha
| extra3          =Anusha Mani
| length3         = 
| title4          = Yella OK maduve Yaake?
| extra4          = Upendra
 | length4         = 
| title5          = Hudugi Hudugi
| extra5          = Kunal Ganjawala
| length5         = 
}}

==External links==
*  
*  
*  
*  
*  

==References==
 

 
 
 
 
 
 