Alag
{{Infobox film
|  name          = Alag |
  image          = Alag movieposter.jpg|
  caption        = Theatrical release poster |
  writer         = Ashu Trikha Sanjay Masoom Tagore Almeda|
  starring       = Akshay Kapoor Dia Mirza Arjun Rampal|
  director       = Ashu Trikha Jim Mulligan|
  producer       = Subi Samuel |
  distributor    = Subi Samuel Movies Pvt. Ltd. |
  released       = 16 June 2006 |
  runtime        = |
  country        = India|
  language       = Hindi |
  music          =   Aadesh Shrivastava |
  budget         =    |
}}
 2006 Bollywood American film Powder (film)|Powder starring Sean Patrick Flanery.

==Synopsis==

Widower Hemant Rastogi lives in scenic Mahabaleshwar, seemingly alone. One night he has a heart attack and passes away. When the Police search his residence they find Tejas, Hemants only son, in the basement of the house. Tejas has spent his entire life in the basement and as a result of this is extremely sensitive to sunlight. The Police ask Purva Rana, head of P.R. Institute (an institute for the rehabilitation of young criminals) to look after Tejas, whose only experience of other people up until this point has been with his father and the books he provided for him. Tejas starts showing signs of Telekenesis, and is shunned by the other boys in the institute, resulting in the near fatal accident of a security guard and the death of a fellow student. Tenzin redeems himself in the eyes of Purvas wealthy father, Pushkar, when he is able to wake his wife, Gayetri Rana, from a coma-like condition. It looks like Tejas has been accepted into the Rana household, but he is subsequently harassed by doctors and scientists wishing to perform experiments on him. When Tejas and Pushkar both refuse to be part of these experiments, Tejas is abducted and held in a glass chamber by Dr. Richard Dyer, who wants to control his mind for his own benefit. Purva realises that Tejas has been abducted, and is fatally injured by Dr. Dyer in a rescue attempt. At the sight of this, Tejas anger causes his powers to surge, shattering the glass cage and killing Dr. Dyer brutally and Purva gets electrocuted. Tejas successfully brings back her to life by shock therapy using both his hands. The film ends with Purva and Tejas driving away in sunset.

== Cast ==
* Dia Mirza ... Purva Rana
* Akshay Kapoor ... Tejas Rastogi
* Mukesh Tiwari ... Mr. Singh
* Sharat Saxena ... Inspector
* Yatin Karyekar ... Hemant Rastogi (Tejas father)
* Beena Banerjee ... Gayatri Rana 
* Jayant Kripalani ... Pushkar Rana
* Tom Alter as Dr. Dyer

===Cameos in order of their appearances===
* Arjun Rampal ... Special Appearance
* Shahrukh Khan ... Special Appearance
* Preity Zinta ... Special Appearance
* Sushmita Sen ... Special Appearance
* Abhishek Bachchan ... Special Appearance
* Karan Johar ... Special Appearance
* Bipasha Basu ... Special Appearance
* Priyanka Chopra ... Special Appearance
* Bobby Deol ... Special Appearance
* Lara Dutta ... Special Appearance

== External links ==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 