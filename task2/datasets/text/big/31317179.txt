Shadows and Lies
{{Infobox film
| name           = Shadows & Lies
| alt            =  
| image	=	Shadows and Lies FilmPoster.jpeg
| caption        = 
| director       = Jay Anania
| producer       = Piers Richardson Sophia Lin
| writer         = Jay Anania
| starring       = James Franco Julianne Nicholson Josh Lucas
| music          = John Medeski
| cinematography = Daniel Vecchione
| editing        = Jay Anania
| studio         = 
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 
| country        = 
| language       = English
| budget         = 
| gross          = 
}}

Shadows & Lies (also known as In Praise of Shadows and William Vincent) is a 2010 romantic drama film starring James Franco and Julianne Nicholson. Filming took place in New York City, New York. The film had a release at the Tribeca Film Festival in April 2010.

==Plot==
William Vincent (Franco) returns to New York after four years in exile to save the girl he loves from the same dangerous crime syndicate that brought them together.

==Cast==
*James Franco  ...  William Vincent
*Julianne Nicholson  ...  Ann
*Josh Lucas  ...  Boss
*Martin Donovan  ...  Victor
*Emily Tremaine ... Cindy

==External links==
*  

 
 
 
 
 
 


 