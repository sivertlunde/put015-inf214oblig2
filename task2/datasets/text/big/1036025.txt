House of Flying Daggers
 
 
{{Infobox film
| name = House of Flying Daggers
| image = House of Flying Daggers poster.JPG
| alt = 
| caption = Theatrical release poster traditional = 十面埋伏
 | simplified = 十面埋伏
 | pinyin = Shí Miàn Mái Fú
 | jyutping = Sap6 Min6 Maai4 Fuk6}}
| director = Zhang Yimou
| producer = William Kong Zhang Yimou
| writer = Li Feng Peter Wu Wang Bin Zhang Yimou
| starring = Andy Lau Zhang Ziyi Takeshi Kaneshiro
| music = Shigeru Umebayashi
| cinematography = Zhao Xiaoding
| editing = Long Cheng
| studio = Edko Films Elite Group Enterprises Zhang Yimou Studio Beijing New Picture Films
| distributor = Edko Films   Sony Pictures Classics  
| released =  
| runtime = 119 minutes
| country = China Hong Kong Mandarin
| budget = $12 million
| gross = $92.9 million
}}
House of Flying Daggers is a 2004 wuxia film directed by Zhang Yimou and starring Andy Lau, Zhang Ziyi and Takeshi Kaneshiro. Unlike other wuxia films, it is more of a love story than purely a martial arts film.
 whiteout background. Another scene uses bright yellow as a color theme. The costumes, props, and decorations were taken almost entirely from Chinese paintings of the period, adding authenticity to the look of the film . 

The film opened in limited release within the United States on 3 December 2004, in New York City and Los Angeles, and opened on additional screens throughout the country two weeks later.

The film was chosen as Chinas entry for the Academy Award for Best Foreign Language Film for the year 2004; but was not nominated in that category though it was nominated for the Academy Award for Best Cinematography.

== Plot ==
In 859 CE, the once great Tang Dynasty is in decline. Numerous rebel groups have formed, the largest of which is the House of Flying Daggers, based in Fengtian county. The Flying Daggers steal from the rich and give to the poor, gaining the support of the locals.

The local authorities manage to kill the leader of the Flying Daggers, but the rebel group only becomes stronger, due to a mysterious new leader.  Leo (Andy Lau) and Jin (Takeshi Kaneshiro), two police captains, are ordered to kill the new leader within ten days.

In order to accomplish this, they arrest Mei (Zhang Ziyi), a blind dancer who is suspected of being the daughter of the old leader of the Flying Daggers. While Mei is incarcerated, Jin and Leo decide to let her go to track the mastermind; Jin will pretend to be a lone warrior called Wind, and break her out of prison. This will gain her trust, and hopefully, Jin will be led to the headquarters of Flying Daggers. The plan works, but Mei and Jin fall in love on the way. They are followed at a distance by Leo; Jin and Leo meet secretly to discuss their plans. Jin jokes about his seduction of the girl; Leo warns him sternly against getting involved.

To add authenticity to the deception, Leo and his men ambush the pair: the fight is, however, a fake. Further on, they are attacked again, but this time their assailants are apparently for real: Jin and Mei battle for their lives, being saved only by the intervention of an unseen knife-thrower. Furious, Jin confronts Leo: Leo explains that he has reported the matter up the chain of command and his general has taken over the pursuit. Jin realizes that he is now expendable.

Once again, Jin and Mei are attacked by the Generals men. They are hopelessly outnumbered; at the last minute they are saved when the House of Flying Daggers reveal themselves. Jin and Leo are captured and taken to their headquarters. At this point, a number of surprising revelations are made. Mei is not blind, nor is she the old leaders daughter - she was merely pretending to be. Yee (Song Dandan), the Madam of the Peony Pavilion, pretends to be Nia, the new leader of the House of Flying Daggers; however, when Leo confronts her, she admits that the real Nia would not reveal herself so easily. Leo is in fact an undercover agent for the House of Flying Daggers, which has engineered the whole chain of events in order to draw the General into a decisive battle. Furthermore, Leo is in love with Mei: he has waited for three years for her whilst working undercover.

Mei, however, cannot bring herself to love Leo: over the last few days she has fallen for Jin. Leo is enraged and tries to rape Mei, but she is saved by her superiors, who embed a dagger in Leos back. However, she is punished by being ordered to kill Jin. Instead, Mei takes him away then frees him from his bonds before they make love in the field. Jin then begs Mei to flee with him, but she is torn between her love and her duty to the House, as well as guilt over Leo; Jin leaves alone.

Mei finally decides to ride after Jin, but is ambushed by Leo who is embittered by her rejection and consumed by jealousy for Jin. Mei, not realizing that Leo has thrown two daggers stuck together, only manages to ward off one before the other strikes her in the chest. As Mei lies dying, Jin returns to find Leo, and they begin an epic battle of honor and revenge, fighting from autumn to winter. As Leo and Jin battle, soldiers close in on the House of Flying Daggers headquarters. Mei, regaining consciousness, grabs the dagger in her chest and threatens to pull it out and to throw it in order to kill Leo if Leo kills Jin with his throwing dagger; in doing so Mei would sacrifice her own life, as it would enable the blood to flow and cause her to bleed to death. Jin begs her not to do it, willing to die rather than let her be killed. Infuriated, Leo throws his arm out as if to throw a knife at Jin, leading Mei to rip the dagger out of her own heart and throw it, not at Leo but instead in an attempt to deflect Leos attack and save Jin. However, all her dagger does is deflect a droplet of blood, as Leo never let go of his dagger. Leo stumbles off into the blizzard, broken over having caused Meis death in face of her apparent refusal to do the same to him. A grief-stricken Jin cradles Meis lifeless body, singing the song originally sung by Mei at the beginning of the film in the Peony Pavilion. It is left ambiguous as to whether the House of Flying Daggers survived the soldiers assault or not.

==Cast==
* Andy Lau — Captain Leo ( )
* Zhang Ziyi — Mei ( )
* Takeshi Kaneshiro — Captain Jin ( )
* Song Dandan — Yee ( )

== Production ==
Anita Mui was originally cast for a major role, which was to be her final film appearance.  She died of cervical cancer before any of her scenes were filmed. After her death on 30 December 2003, director Zhang Yimou decided to alter the script rather than find a replacement. The film is dedicated to her memory.

To prepare for her role, for two months Zhang Ziyi lived with a blind girl who had lost her sight at the age of twelve because of a brain tumor. Takeshi Kaneshiro injured his leg when he went horse-back riding. As a result, Yimou had Kaneshiro spend two scenes sitting or kneeling down in order to alleviate the pain, which was stated in Zhang Yimous audio commentary.
 Hutsul Region National Park), such as the scene in the snow or birch forests. The film team spent 70 days on location (September–October 2003), based in Kosiv.  However, the often noted bamboo forest sequences were filmed in China. It snowed so early (October) that the filmmakers had to change the script and the film.  They did not want to wait because the leaves were still on the trees. Zhang Yimou was very happy with how it turned out, however, because it set the perfect tone. 
 wuxing color-theory in both a deliberate and ironic manner.

== Literary origins ==
  Li Yannian ( ):
 

         ，          。
         ，          。
               。
         。

{| style="border: solid 2px;" cellpadding="5"
|- Traditional Chinese Simplified Chinese
|-
||
{{lang|zh-Hant|北方有佳人，絕世而獨立。 
一顧傾人城，再顧傾人國。 
寧不知傾城與傾國。 
佳人難再得。}}
||
{{lang|zh-Hans|北方有佳人，绝世而独立。  
一顾倾人城，再顾倾人国。  
宁不知倾城与倾国。  
佳人难再得。}}
|- Pinyin transcription English translation
|-
|style="font-family:Arial Unicode MS; font-family /**/:inherit;"|
{{transl|zh|ISO|Běifāng yǒu jiārén, juéshì ér dúlì. 
Yí gù qīng rén chéng, zài gù qīng rén guó. 
Nìng bù zhī qīng chéng yǔ qīng guó. 
Jiārén nán zài dé.}}
||
In the north there is a beauty; surpassing the world, she stands alone. 
A glance from her will overthrow a city; another glance will overthrow a nation. 
One would rather not know whether it will be a city or a nation that will be overthrown. 
As it would be difficult to behold such a beauty again.
|-
|}

== Release ==
=== Box office ===
House of Flying Daggers opened in North America on 3 December 2004 in 15 theatres.  It grossed US$397,472 ($26,498 per screen) in its opening weekend.  The films total North American gross is $11,050,094.

The film made an additional US$81,751,003 elsewhere in the world, bringing its total worldwide box office gross to $92,801,097.  It was also the third highest grossing foreign language film in the North America market in 2004. 

=== Critical reception ===
House of Flying Daggers debuted in May at the 2004 Cannes Film Festival    to enthusiastic receptions.    The film reportedly received a 20-minute standing ovation at its Cannes Film Festival premiere. 

The film received widespread critical acclaim.     At film review aggregation website Metacritic, the film received an average score of 89%, based on 37 reviews.   Rotten Tomatoes gives the film a "Certified Fresh" score of 88% based on reviews from 160 critics.  Metacritic also ranked the film at the end of the year as the 6th best reviewed film of 2004. 

Phil Hall of   praised the director Zhang Yimous use of color in the film as "simply the best in the world" and described the film as: "the slow-motion trajectory of a small bean, hurled from a police captains hand, is a spectacular thing. Its a stunning, moving image, like a hummingbird caught in action."   While Kevin Thomas of the Los Angeles Times praised the film by stating: "House of Flying Daggers finds the great Chinese director at his most romantic in this thrilling martial arts epic that involves a conflict between love and duty carried out to its fullest expression." 

  of the  s best films of the 2000s.  and ranked #77 in Empire (magazine)|Empire magazines "The 100 Best Films Of World Cinema" in 2010. {{cite web 
| title = The 100 Best Films Of World Cinema – 77. House of Flying Daggers 
| url = http://www.empireonline.com/features/100-greatest-world-cinema-films/default.asp?film=77
| work = Empire 
}} 
 

== Accolades ==
=== Won === Boston Film Critics
** Best Cinematography (Zhao Xiaoding)
** Best Director (Yimou Zhang)
** Best Foreign Language Film (China/Hong Kong) Los Angeles Film Critics
** Best Foreign Language Film (China/Hong Kong)
* Motion Picture Sound Editors
** Best Sound Editing in Foreign Features
* National Board of Review
** Outstanding Production Design
* National Society of Film Critics 
** Best Director (Yimou Zhang)
** Best Cinematography (Zhao Xiaoding) 
* Satellite Awards
** Best Cinematography (Zhao Xiaoding) 
** Best Visual Effects

=== Nominations ===
* 24th Hong Kong Film Awards
** Best Asian Film
* Academy Awards
** Best Cinematography (Zhao Xiaoding)
* Academy of Science Fiction, Fantasy & Horror Films
** Best Actress (Zhang Ziyi)
** Best Costumes (Emi Wada)
** Best Director (Zhang Yimou)
** Best Fantasy Film
* BAFTA Awards
** Best Achievement in Special Visual Effects (Angie Lam, Andy Brown, Kirsty Millar & Luke Hetherington)
** Best Cinematography (Zhao Xiaoding)
** Best Costume Design (Emi Wada)
** Best Editing (Long Cheng)
** Best Film not in the English Language (William Kong & Zhang Yimou)
** Best Make Up/Hair (Lee-na Kwan, Xiaohai Yang & Siu-Mui Chau)
** Best Performance by an Actress in a Leading Role (Zhang Ziyi)
** Best Production Design (Huo Tingxiao)
** Best Sound (Tao Jing & Roger Savage) Golden Eagle Awards Best Foreign Language Film
* London Film Critics Circle
** Film of the Year
** Director of the Year (Zhang Yimou)
** Foreign language film of the year
* Satellite Awards
** Best Art Direction/Production Design (Zhong Han) 
** Best Costume Design (Emi Wada) 
** Best Film Editing (Long Cheng) 
** Best Motion Picture - Foreign Film (China)
** Best Sound (Editing & Mixing) (Jing Tao)
* Broadcast Film Critics Association Awards
** Best Foreign-Language Film
* Online Film Critics Society Awards
** Best Cinematography (Xiaoding Zhao) 
** Best Editing (Long Cheng) 
** Best Foreign Language Film (China) 
* European Film Awards
** Best Non-European Film - Prix Screen International

==Soundtrack==
{{Infobox album Name        = House of Flying Daggers Type        = Soundtrack Artist      = Shigeru Umebayashi Cover       = HouseofFlyingDaggersSoundtrack.jpg Released    =   Recorded    =  Genre       = Stage & Screen Classical Length      = 49:37 Label  Sony   Producer    = Shigeru Umebayashi
}}
The soundtrack was produced and created by Shigeru Umebayashi, featuring vocals by Zhang Ziyi and Kathleen Battle . It was released in Hong Kong on 15 July 2004 by the films production company and distributor Edko Films. The US version was released by Sony Music Entertainment on 7 December 2004.

# "Opening Title" - 0:58
# "Beauty Song" (佳人曲) - 2:32 (Zhang Ziyi)
# "The Echo Game" - 1:17
# The Peonyhouse - 1:22
# "Battle in the Forest" - 3:26
# "Taking Her Hand" - 1:14
# "Leos Eyes" - 1:51
# "Lovers-Flower Garden" - 2:19
# "No Way Out" - 3:59
# "Lovers" - 1:54
# "Farewell No. 1" - 2:42
# "Bamboo Forest" - 2:36
# "Ambush in Ten Directions" (十面埋伏) - 2:01
# "Leos Theme" - 2:36
# "Mei and Leo" - 3:06
# "The House of Flying Daggers" - 1:27
# "Lovers-Mei and Jin" - 4:21
# "Farewell No. 2" - 2:49
# "Until The End " - 2:55
# "Title Song Lovers" - 4:12 (Kathleen Battle)

== See also ==
  List of historical drama films of Asia

== References ==
 

== External links ==
* 
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 