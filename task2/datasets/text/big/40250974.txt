Gabrielle (2013 film)
 
 
{{Infobox film
| name           = Gabrielle
| image          = Gabrielle (2013 film).jpg
| caption        = Film poster
| director       = Louise Archambault
| producer       = 
| writer         = Louise Archambault
| starring       =  
| music          = 
| cinematography = Mathieu Laverdière
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Canada
| language       = French
| budget         = 
}}

Gabrielle is a 2013 Canadian drama film directed by Louise Archambault.    It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.    The film was first shown at the Locarno International Film Festival on 12 August 2013  where it won the Audience Award. 

The film stars Gabrielle Marion-Rivard as Gabrielle, a young woman with Williams syndrome who participates in a choir of developmentally disabled adults,  and begins a romantic relationship with her choirmate Martin (Alexandre Landry) while the choir prepares for an upcoming engagement singing backing vocals for Robert Charlebois. 
 Best Foreign Language Film at the 86th Academy Awards,       but it was not nominated. The film was also a finalist for Best Canadian Film at the Toronto Film Critics Association Awards 2013, alongside The Dirties and the eventual winner, Watermark (film)|Watermark. 

The film garnered six  , 13 January 2014.  It won the awards for Best Picture and Best Actress. 

==Cast==
* Gabrielle Marion-Rivard as Gabrielle
* Alexandre Landry as Martin
* Mélissa Désormeaux-Poulin as Sophie
* Vincent-Guillaume Otis as Rémi
* Benoit Gouin as Laurent
* Sébastien Ricard as Raphael
* Marie Gignac as Martins mother
* Isabelle Vincent as Gabrielles mother
* Robert Charlebois as Himself

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 