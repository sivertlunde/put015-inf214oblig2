Saajan Ka Ghar
{{Infobox film
| name           = Saajan Ka Ghar
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Surendra Kumar Bohra	 
| producer       = Nutan Surendra Bohra Farooq
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Rishi Kapoor Juhi Chawla
| music          = Nadeem-Shravan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Surendra Kumar Bohra. It stars Rishi Kapoor and Juhi Chawla in pivotal roles. 
 

==Cast==
* Rishi Kapoor...Amar Khanna
* Juhi Chawla...Laxmi Khanna
* Deepak Tijori...Suraj Dhanraj
* Farheen...Kiran
* Anupam Kher...Mr. Dhanraj
* Kader Khan...Uncle
* Bindu (actress)|Bindu...Mrs. Dhanraj
* Shubha Khote...Mrs. Kamla Khanna
* Alok Nath...Mr. Ram Khanna aka Ramji
* Johnny Lever...Dilip K. Bose / Champa Bose
* Anjana Mumtaz...Shanti Dhanraj
* Beena Banerjee...Gita Dhanraj
* Tej Sapru...Teja

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Apni Bhi Zindagi Mein"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Babul De Do Dua"
| Suresh Wadkar, Alka Yagnik
|-
| 3
| "Bojh Se Gamon Ke"
| Alka Yagnik
|-
| 4
| "Chandi Ki Dori"
| Alka Yagnik
|-
| 5
| "Dard Sahenge Kuchh Na Kahenge"
| Manhar Udhas, Sadhana Sargam
|-
| 6
| "Main Karti Hoon Tujhe Pyar"
| Kumar Sanu, Alka Yagnik
|-
| 7
| "Nazar Jeedhar Jeedhar Jaye"
| Kumar Sanu, Alka Yagnik
|-
| 8
| "Rab Ne Bhi Mujh Pe Sitam"
| Alka Yagnik
|-
| 9
| "Sawan Aaya Badal Chhaye"
| Kumar Sanu, Sadhana Sargam
|}

==References==
 

==External links==
* 

 
 
 
 

 