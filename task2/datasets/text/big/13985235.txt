Slow Days
{{Infobox Film
| name           =Slow Days (Ajde, dan... prođi...)
| image          = 
| image_size     = 
| caption        = 
| director       = Matija Kluković
| producer       = Irena Marković
| writer         = Matija Kluković
| narrator       = 
| starring       = Višnja Pešić Filip Šuster Marija Kohn Petra Težak Nina Benović
| music          = Darko Marković Blamaž Electroniq
| cinematography = Bojana Burnać
| editing        = 
| distributor    = 
| released       = 2006
| runtime        = 102 minutes
| country        = Croatia Croatian
| HRK 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 indie film. Set in the Croatian capital Zagreb and its satellite town, Velika Gorica, it follows the lives of over twenty individuals. It was directed, written, and edited over a period of three years by the young Matija Kluković and starred, among others, Višnja Pešić, Filip Šuster, Marija Kohn, Petra Težak, and Nina Benović.

In Croatia, the film received critical acclaim for its independent style of film-making, terrific cast of non-actors combined with professional actors, and black and white cinematography by Bojana Burnać, the first female cinematographer in Croatian film history.  It won the Golden Pram award at Zagreb Film Festival and internationally premiered at the International Film Festival Rotterdam in 2007.

==References==
 

==External links==
*    
*  
*  
*  

 
 

 