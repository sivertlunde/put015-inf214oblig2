List of Telugu films of 2000
 
  Tollywood (Telugu Hyderabad in the year 2000 in film|2000.

== Released films ==

=== January–June ===

{| class="wikitable"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening 
! style="width:13%;"|Title 
! Director !! Cast !! Genre !! Notes 
|- January!
| rowspan="6"   style="text-align:center; background:#FFA07A; textcolor:#000;"|  J A N
| rowspan="1" style="text-align:center;"| 1  Laya || Drama || Comedy || 
|-
| rowspan="1" style="text-align:center;"| 7 
| Annayya (2000 film)|Annayya|| Mutyala Subbayya || Chiranjeevi, Ravi Teja, Soundarya, Simran (actress)|Simran, Sharath Babu || Drama / Action || 
|-
| rowspan="1" style="text-align:center;"| 13  Raasi || Romance || 
|-
| rowspan="2" style="text-align:center;"| 14 
| Vamsoddarakudu || Sharath || Balakrishna Nandamuri, Sakshi Shivanand, Ramya Krishna, Krishnam Raju || Action ||
|- Simran || Drama || 
|-
| rowspan="1" style="text-align:center;"| 15
| Sammakka Sarakka|| Dasari Narayana Rao || Suman (actor)|Suman, Ramya Krishna, Roja (actress)|Roja, Jayapradha, Indraja, Brahmaji || Comedy ||
|-
|- February!
| rowspan="6" valign="center" align="center" style="background:#D8BFD8; textcolor:#000;" |  F E B
| rowspan="1" style="text-align:center;"| 4
| Kshemanga Velli Labanga Randi || Raja Vannem Reddy || Srikanth (Telugu actor)|Srikanth, Rajendra Prasad, Roja Selvamani|Roja, Brahmanandam, Ramya Krishna, Kovai Sarala, Prakash Raj || Comedy ||
|-
| rowspan="2" style="text-align:center;"| 10  Rami Reddy ||  Action ||
|-
| Handsup|| Siva Nageswara Rao || Nagendra Babu, Jayasudha, Brahmanandam || Comedy ||
|-
| rowspan="1" style="text-align:center;"| 18 
| Chala Bagundi || E V V Satyanarayana|E.V.V.Sathyanarayana || Srikanth (Telugu actor)|Srikanth, Malavika (Tamil actress)|Malavika, Vadde Naveen, Asha Saini || Comedy || 
|-
| rowspan="1" style="text-align:center;"| 25
| Nishabda Ratri || Sahadeva Reddy || Kaushal, Raksha || Thriller ||
|-
| rowspan="1" style="text-align:center;"| 26 
| Manasu Paddanu Kaani || K. Veeru || Venu Thottempudi, K. Vishwanath, Raasi (actress)|Raasi, Ramya Krishna || Drama ||
|-
|- March!
| rowspan="4" valign="center" align="center" style="background:#98FB98; textcolor:#000;" |  M A R
| rowspan="1" style="text-align:center;"| 3
| Ravanna (film)|Ravanna || B. Gopal || Rajasekhar (actor)|Rajasekhar, Sanghavi, Soundarya, Krishna Ghattamaneni|| Drama ||
|-
| rowspan="1" style="text-align:center;"| 10 Roja || Action ||
|-
| rowspan="1" style="text-align:center;"| 16  Prema || Drama ||
|-
| rowspan="1" style="text-align:center;"| 31 
| Balaram|| Raviraja Pinisetty || Srihari, Raasi (actress)|Raasi, Vineeth, Maheshwari || Action ||
|-
|- April!
| rowspan="4"  valign="center" align="center" style="background:#FFDEAD; textcolor:#000;" |  A P R
| rowspan="1" style="text-align:center;"| 5 
| Nuvvu Vastavani|| Pratap || Akkineni Nagarjuna, Simran (actress)|Simran, Isha Koppikar || Romance || 
|-
| rowspan="1" style="text-align:center;"| 14 
| Yuvaraju || Y. V. S. Chowdary || Mahesh Babu, Simran (actress)|Simran, Sakshi Shivanand || Romance ||
|-
| rowspan="1" style="text-align:center;"| 20
| Badri (2000 film)|Badri || Puri Jagannadh || Pawan Kalyan, Ameesha Patel, Renu Desai, Prakash Raj  || Masala || 
|-
| rowspan="1" style="text-align:center;"| 28
| Madhuri || Mouli || Abbas (actor)|Abbas, Anjana  || Romance ||
|-
|- May!
| rowspan="5"    valign="center" align="center" style="background:#edafaf; textcolor:#000;" |  M A Y
| rowspan="1" style="text-align:center;"| 5 
| Pasupu Kumkuma || M Raghupati Reddy || Sijju, Jackie, Anjani Thakkar || Drama / Romance ||
|-
| rowspan="1" style="text-align:center;"| 13 
| Manoharam || Gunashekar || Jagapathi Babu, Laya (actress)|Laya, Prakash Raj || Drama ||
|-
| rowspan="1" style="text-align:center;"| 18 
| Yuvakudu || Karunakaran || Sumanth, Bhoomika Chawla, Jaya Sudha || Romance || 
|-
| rowspan="1" style="text-align:center;"| 26  Raasi || Drama || 
|-
| rowspan="1" style="text-align:center;"| 26
| Nagulamma || KSR Das  || Babloo Prithviraj, Maheshwari || Drama ||
|-
|- June!
| rowspan="8"   valign="center" align="center" style="background:#d9c1b0; textcolor:#000;" |  J U N
| rowspan="1" style="text-align:center;"| 1  Vijayakumar || Drama || 
|-
| rowspan="1" style="text-align:center;"| 8  Sai Kumar, Manya || Action ||
|-
| rowspan="2" style="text-align:center;"| 9 
| Hindustan||  Hari Babu || Naresh (actor)|Naresh, Bhanupriya || Drama  ||  
|-
| Real Story || Venkataramana Reddy || Prakash Raj, Sandhya, Brahmanandam || Drama ||
|-
| rowspan="1" style="text-align:center;"| 16  Teja || Uday Kiran, Reema Sen || Romance || 
|-
| rowspan="1" style="text-align:center;"| 22 Raasi || Action ||
|-
| rowspan="1" style="text-align:center;"| 23
| Okkadu Chalu || || Rajasekhar (actor)|Rajasekhar, Suresh (actor)|Suresh, Rambha (actress)|Rambha, Sanghavi || Action ||
|-
| rowspan="1" style="text-align:center;"| 30 Suman || Action ||
|}

=== July–December ===

{| class="wikitable" style="width:100%;"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening 
! style="width:19%;"| Title 
! Director !! Cast !! Genre !! Notes 
|- July!
| rowspan="6"   style="text-align:center; background:	#d0f0c0; textcolor:#000;"| J U L
| rowspan="1" style="text-align:center;"| 7  Manya  || Romance ||
|-
| rowspan="2" style="text-align:center;"| 14 
| Maa Pelliki Randi || Muppulaneni Siva || J. D. Chakravarthy, Sakshi Shivanand || Romance ||
|-
| Pellam Vachindi || Krishna Reddy || Suman (actor)|Suman, Manichandana, Radhika Chowdhary || Romance ||
|-
| rowspan="1" style="text-align:center;"| 21 
| Goppinti Alludu|| E. V. V. Satyanarayana || Balakrishna Nandamuri, Simran (actress)|Simran, Sanghavi || Drama || 
|-
| rowspan="2" style="text-align:center;"| 27 
| Pelli Sambandham || K. Raghavendra Rao || Akkineni Nageshwara Rao, Sumanth, Sakshi Shivanand, Sanghavi || Drama ||
|-
| Kouravudu || V Jyotikumar || Nagababu, Ramya Krishna || Drama ||
|-
|- August!
| rowspan="8"   style="text-align:center; background:#c2b280;"| A U G
| style="text-align:center;"| 4 
| Sardukupodam Randi || S. V. Krishna Reddy || Jagapathi Babu, Soundarya, Asha Saini, M. S. Narayana, Jayasudha || Comedy || 
|-
| rowspan="3" style="text-align:center;"| 11 
| Manasunna Maharaju || Muthyala Subbaiah || Rajasekhar (actor)|Rajasekhar, Laya (actress)|Laya, Asha Saini || Drama ||
|-
| 9 Nelalu || Kranthi Kumar || Vikram (actor)|Vikram, Soundarya || Drama / Romance  || 
|- Prema || Drama ||
|-
| rowspan="2" style="text-align:center;"| 18 Rambha || Romance || 
|-
| Manasichanu || Pramod Kumar || Ravi Teja, Manichandana || Romance || 
|-
| rowspan="2" style="text-align:center;"| 25 
| Vyjayanthi || K S Nageswara Rao ||  Vijayashanti, Babloo Prithviraj || Action ||
|-
| February 14th Necklace Road || Seenu  || Suman (actor)|Suman, Bhanupriya  || Thriller ||
|-
|- September!
| rowspan="7"   style="text-align:center; background:wheat; textcolor:#000;"|S E P
| rowspan="2" style="text-align:center;"| 1  Raasi ||  Comedy ||
|- Manya || Romance ||
|-
| rowspan="1" style="text-align:center;"| 14  
| Ninne Premista || R. R. Shinde || Akkineni Nagarjuna, Srikanth (Telugu actor)|Srikanth, Soundarya || Romance || 
|-
| rowspan="2" style="text-align:center;"| 15 
| Rayalseema Ramanna Chowdary || Ravi Raja Pinisetty || Mohan Babu, Jaya Sudha, Priya Gill || Action ||
|-
| Ayodhya Ramayya || Chandra Mahesh || Srihari, Bhanupriya ||  Action ||
|-
| rowspan="1" style="text-align:center;"| 22 
| Tensionlo Tension || D Ranga Rao || Ajay, Manichandana || Romance ||
|-
| rowspan="1" style="text-align:center;"| 29 
|Azad (film)|Azad || Tirupathy Swami || Akkineni Nagarjuna, Raghuvaran, Soundarya, Shilpa Shetty, Prakash Raj || Action || 
|-
|- October!
| rowspan="5"   style="text-align:center; background:#ffb7c5;"| O C T
| rowspan="1" style="text-align:center;"| 4 
| Vamsi (film)|Vamsi|| B. Gopal || Mahesh Babu, Namrata Shirodkar, Krishna Ghattamaneni || Action || 
|-
| style="text-align:center;"| 7 
| Jayam Manade Raa || N. Shankar || Daggubati Venkatesh|Venkatesh, Soundarya || Drama ||
|-
| rowspan="1" style="text-align:center;"| 13
| Nuvve Kavali|| Vijayabhaskar || Tarun Kumar, Richa Pallod, Saikiran || Romance || 
|-
| style="text-align:center;"| 20 Raasi || Comedy drama || 
|-
| style="text-align:center;"| 27
| NTR Nagar || Babji || Dupes, Manichandana || Drama ||
|-
|- November!
| rowspan="8"   style="text-align:center; background:#ace1af;"| N O V
| rowspan="2" style="text-align:center;"| 3
| Sri Sai Mahima || Ashok Kumar || Om Prakash Rao, Jayasudha || Drama ||
|-
| Sri Srimati Satyabhama || S. V. Krishna Reddy  || Rahman (actor)|Rahman, Vijayashanti || Comedy ||
|-
| rowspan="3" style="text-align:center;"| 9 
| Baachi || Puri Jagannadh || Jagapathi Babu, Neelambari, Kota Srinivasa Rao, Prakash Raj || Masala || 
|-
| Devullu || Kodi Ramakrishna || Ramya Krishna, Raasi (actress)|Raasi, Srikanth (Telugu actor)|Srikanth, Laya (actress)|Laya, Rajendra Prasad || Fiction || 
|- Manya || Romance ||
|- 
| rowspan="2" style="text-align:center;"| 16  Sunil || Drama ||
|-
| Anta mana Manchike || K. Veeru || Rajendra Prasad, Rachana Banerjee, Asha Saini || Comedy ||
|-
| style="text-align:center;"| 23 Urvashi || Action || 
|-
|- December!
| rowspan="8"   style="text-align:center; background:#b8860b;"| D E C
| rowspan="2" style="text-align:center;"| 1 
| Maa Annayya || Raviraja Pinisetty || Rajasekhar (actor)|Rajasekhar, Meena Durairaj, Deepti Bhatnagar || Drama ||
|-
| Subhavela || Ramana || Ravikanth, Anasuya || Romance ||
|-
| rowspan="1" style="text-align:center;"| 8 
| Uncle ||  Raja Sekhar || Tarun, Pallavi ||  Drama ||
|-
| rowspan="2" style="text-align:center;"| 21 
| Sakutumba Saparivara Sametam || S. V. Krishna Reddy || Akkineni Nageshwara Rao, Srikanth (Telugu actor)|Srikanth, Suhasini Maniratnam, Jayalakshmi || Drama ||
|-
| Tirumala Tirupathi Venkatesha || Sathibabu || Ravi Teja, Meka Srikanth|Srikanth, Roja (actress)|Roja, Maheshwari || Comedy || 
|-
| style="text-align:center;"| 22 
| Unmadi || T Krishna || Surya, Sishwa, Ranjani || Thriller ||
|-
| rowspan="2" style="text-align:center;"| 29 
| Chalo Assembly || Narayana Murthy || Narayana Murthy, Usha || Action ||
|-
| Rojuko Roja || GJ Raja || Raja Vikram, Rashmita, Swapna Sri || Romance ||
|-
|}

==External links==
*  

 
 
 

 
 
 
 