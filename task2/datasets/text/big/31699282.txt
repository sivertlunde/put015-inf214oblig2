Dead Awake (2010 film)
{{Infobox film
| name           = Dead Awake
| image          = Dead Awake.jpg
| alt            =  
| caption        = 
| director       = Omar Naim
| producer       = Lucas Jarach Nesim Hason
| writer         = 
| starring       = Nick Stahl Rose McGowan Amy Smart
| music          = John Hunter
| cinematography = David A. Armstrong
| editing        = Miklos Wright
| studio         = First Look Studios
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = 
| budget         = $3.5 million
| gross          = 
}}
Dead Awake is a 2010 mystery film starring Nick Stahl, Rose McGowan, and Amy Smart. The film was previously titled Dylans Wake.

==Plot==
Dylan (Nick Stahl|Stahl) tries to unravel the answer to a decade long mystery by staging his own funeral and examining who shows up. The problem is, Dylan might actually be dead.

==Cast==
* Nick Stahl as Dylan
* Rose McGowan as Charlie
* Amy Smart as Natalie
* Ben Marten as Steve
* Shane Simmons as David
* Scott Byerly as himself

==Production==
Filming took place in the Fall of 2009, in Des Moines, Iowa, and was released in December 2010 to mixed reviews. Mark Olsen of The Los Angeles Times said in his review; "Though the performers gamely try to make the most of what little they have to work with, the film is murky to look at and unfocused in its storytelling. "Dead Awake" is a deadly snore." Positive marks came from the NYC Movie Guru, who called the film "an intriguing blend of mystery, suspense, drama and supernatural thrills that slightly loses steam as its trust in the audience’s intelligence wanes."

==External links==
*  
*  
*  

 
 
 
 
 
 

 