Luboml: My Heart Remembers
 
{{Infobox film
| name           = Luboml: My Heart Remembers
| image          = Luboml cover.jpg
| image_size     = 
| caption        = 
| director       = Aaron Ziegelman 
| producer       = Eileen Douglas and Ron Steinman 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2003
| runtime        = 57 mins.
| country        = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Luboml: My Heart Remembers is a 2003 documentary film produced by Eileen Douglas and Ron Steinman and funded by The Aaron Ziegelman Foundation. It compiles survivor interviews, archival photographs and film footage to reconstruct a sense of life in Liuboml|Luboml, one of the five thousand small shtetls (Jewish market towns) that were destroyed by the Holocaust. The film, explores the vibrant Jewish life that –once central to European Jewry – is now forever lost. Made in 2003, Luboml: My Heart Remembers was called a “must see gem” by The Forward.

==Film content==

The documentary exposes what the Nazis destroyed, resurrecting the past to depict a tightly-knit community. “Every place you went, people knew you,” says an old man.  The town’s citizens worked together at the market square at the center of town, and they prayed together at the Great Synagogue. Like many shetlach that dotted Europes landscapes, the people of Luboml shared both a religious tradition and sincere concern for each other. Every area of life, from work to holidays, from weddings to funerals, is described with colorful anecdotes and lively details, by the few who survived.

Of the 8,000 Jews who lived in and around Luboml only 51 survived the Holocaust, and today, not a single Jew lives there. Luboml as it once was – a simple peaceful society – has been uprooted, and before this film, only existed in memories.

Isolated from urban societies, Luboml was a quaint blend of emerging technology and old fashioned practices.  Most citizens lived without refrigerators or running water.  “We always had a cow and a garden in front,” one survivor reflects.

But the citizens didn’t think of themselves as primitive. They were proud of the new cinema house and electric lights in the towns streets that went up in the idyllic period before they knew what danger lay ahead. At night, after dinner, all the youths would slip out of their homes to go on long walks together, guided by those electric lights.

The documentary suggests that the Jewish population was so full of concern for one another that economics became somewhat irrelevant.  When one family didn’t have enough money to provide for themselves, neighbors would anonymously donate food.  One survivor reflects, “We didn’t know if we were rich or poor.  Most people were in the same class,” while another chuckles, “I could have been poor, but I didn’t know it.”

But Judaism, which had brought the townspeople together, became the reason for their destruction.  The seeds of anti-Semitism in Luboml, which had played out in name-calling and child’s teasing before the war, morphed into a serious threat.  The Nazis entered the peaceful market town in tanks.  Fear and helplessness over came the town.  One survivor wrote, “Only if one’s hands have been amputated can you understand at all what a Polish Jew is going through.”  A lucky few fled from Poland.

Luboml makes it clear that despite all the time that has passed since its destruction, those survivors who once lived in Luboml still remember their hometown dearly.  “Every night, before I fall asleep” mourns one survivor who fled Poland during the war, “I am in Luboml.  Still today, I am in Luboml.”

The documentary serves as a detailed portrait of shtetl life that was destroyed. As one survivor recalls, “It was warm. It was beautiful. It was happy. All that is gone.”

The documentary film is available in DVD and VHS tape formats.

==Filmmakers interview==

Filmmaker Aaron Ziegelman says of his experience in Luboml:
 "I was a teenager when I received the news that my whole family in Luboml was killed. My first reaction was Im glad I got out in time and I went about my teenage activities. It was only years later that I truly understood the enormity of the tragedy and I would say that I felt guilty about my own initial reaction. Of course, the reality was that the whole world was silent including most of the Jewish community in America."

Of his inspiration to make Lubomls history known:
"I believe the first instinct came after seeing the movie Schindlers List. I wanted to know about the people in the camps before they were brought there. The idea wasalso reinforced when I started having the Luboml Book translated from Yiddish into English. I realized that this was a good starting point to tell the story of life before death and destruction."

Of making the documentary:
"The part of the film shot in Luboml was exciting and emotional. Reliving my childhood, going to places that I remembered and the scene at the mass grave was very profound. I kept thinking of my uncle and aunt and their two daughters. These girls were my playmates and I saw them  . I imagined the terror they must have had being marched, all together, to their deaths. I refrained from voicing these feelings during the film because I knew that I would break down."

==Geography==
 
Luboml is situated 466&nbsp;km west of Kiev, Ukraine, in a region known as Volyn′. Belarus is to its north, and Poland to its west. But because of its location on the border, Luboml has a long history—dating back to the 11th century—of changing rule. The territory of Volyn′ first belonged to Kievan Rus, then Poland, then Lithuania, then Poland, then Ukraine.

==External links==
*  

 
 
 
 
 