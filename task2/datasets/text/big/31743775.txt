Denmark (film)
{{Infobox film
| name           = Denmark
| image          = Denmark_2010_film_theatrical_poster.jpeg
| caption        = Denmark theatrical poster
| alt            
| director       = Daniel Fickle Mark Smith Adam Shearer
| screenplay     = Daniel Fickle Mark Smith Courtney Eck
| starring       = Pily Portland Cello Project
| cinematography = Daniel Fickle  Mark Smith Veronica Wood
| editing        = Jesse Salsberry
| studio         = Two Penguins Productions
| released       =  
| runtime        = 6 minutes
| country        = United States
| language       = English
}}
 

 
Denmark is a 2010 short film co-written and directed by Daniel Fickle and scored by Gideon Freudmann of The Portland Cello Project.  Utilizing puppetry and hand-built sets the film tells a story about Pily, a crustacean of mixed origin, who builds a rocket ship to escape his underwater home when it becomes threatened by pollution.

Denmark premiered at the Aladdin Theatre on May 15, 2010 in Portland, Oregon. The film achieved critical success thereafter largely through established film festivals and numerous features on websites. 

==Plot==
Pily lives a pastoral life at the bottom of Oregon|Oregons Willamette River. He tends to his underwater crops in solitude and proves to be resourceful. His home is built from flotsam and sunken debris.

Seemingly content in the world he has built for himself, Pily is actually addled by a premonition, a pre-knowledge that an invasive element is going to displace him. To prepare for the worst Pily devises an escape plan. He builds a rocketship.

When Pilys anxiety gives way to the reality of an oil spill his rocketship is ready except for one part thats essential to achieve liftoff. Pily goes ashore where he finds the missing part and returns to initiate his escape. Once airborne Pily is confronted with another challenge and reaches for a solution that doesnt exist.

==Back story==
"Denmark" is the title of the first track from The Portland Cello Projects album, A Thousand Words. The song was written by cellist Gideon Freudmann to honor the loss of a loved one who lost a battle to cancer; the song is a love letter and an inspired response against the indiscriminate nature of fate.

When director Daniel Fickle was approached by The Portland Cello Project to create a music video for Denmark he chose to treat the song as a score for a screenplay that was yet to be written. As a result the film evolved from a music video into a narrative short. 

The film uses a stark form of humor to resonate the interplay of alienation, turmoil, and other emotions that are associated with reconciling loss. Recognizing that laughter has long been a way to cope with lifes irreducible realities, the creators of Denmark, the film, use humor as a narrative device to mollify anguish and convey empathy.

==Production design== Mark Smith, Rory Brown, Daniel Fickle and Andrew Brown. The design team was initially tasked with building an underwater set; however, Pilys body being less dense than water and subject to the force of buoyancy they decided to create an underwater environment by shooting the film through a 50 gallon aquarium. The set included a stage where upstage, center stage and downstage could be removed to accommodate the needs of the puppeteers. Pilys home was wood framed, sculpted by mesh wire and covered with a mixture of mud, sand and straw. The home was segmented like the stage to meet the requirements of the directors shot list and Pilys movements. The interior of Pilys home consists of items collected from various thrift stores in Portland, Oregon. Pilys crops were made from wax and his rocketship was crafted out of foam board.

==Pily==
Jason Miranda and Bill Holznagel built the puppet based on Fickles design and were the hands behind Pilys performance in Denmark. The puppeteers/entertainers used traditional means to manipulate Pilys movements; Marionette bars, strings and wires. They also employed glove puppet techniques to express emotions from Pilys eyes.

==Official selections== SXSW
*Cinequest Film Festival
*Palm Beach International Film Festival
*Ashland Independent Film Festival
*Byron Bay Film Festival
* Sene
*Zero Film Festival
* Ferndale Film Festival
*Ann Arbor Film Festival Olympia Film Festival
* Canada International Film Festival
*Atlanta Film Festival
*Bahamas International Film Festival
* Los Angeles Cinema Festival of Hollywood
*Chicago International REEL Shorts Festival
* Vancouver DSLR Film Festival
* California Independent Film Festival
*Science Fiction Fantasy Short Film Festival
*  Santa Catalina Film festival
*Buffalo Niagara Film Festival
*Victoria Film Festival
* Desert Dust Cinema Short Film Festival Athens Video Art Festival
*New Media art festivals|Alpha-Ville Festival Park City Music Film Festival
*  Albuquerque Film Festival
*  Fantadia International Multivision Festival
*  California International Shorts Festival
*New Orleans Film Festival
*  Crested Butte Film Festival
*  Milwaukee Film Festival
*  Sapporo International Short Film Festival
*  Imagine Science Film Festival
*Malibu Film Festival
*  Radar Hamburg International Independent Film Festival
* Philadelphia Film & Animation Festival Henson International Festival BAM
*Imagine Science Film Festival Dublin 

==Awards==
*Royal Reel Award: Canada International Film festival 2011

*Best Music Video: Los Angeles Cinema of Hollywood 2010

*Gold Medal for music in a short film: Park City Film Music Festival 2011

*Best Music Video: Radar Hamburg International Independent Film Festival 2011 

== References ==
 
 

== External links ==
* 

 
 