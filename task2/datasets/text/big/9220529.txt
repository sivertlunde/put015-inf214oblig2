The Great Moment (1921 film)
 
{{Infobox film
| name           = The Great Moment
| image          = Great moment545.jpg
| image_size     = 190px
| caption        = Theatrical release poster
| director       = Sam Wood
| producer       = Jesse L. Lasky Monte Katterjohn (scenario)
| story          = Elinor Glyn
| starring       = Gloria Swanson Alec B. Francis Milton Sills
| cinematography = Alfred Gilks
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = Silent English intertitles
| budget         =
| gross          =
}}
 silent drama lost though a fragment still exists and is preserved at the BFI National Archive.  

==Plot== Russian Romani people|Gypsy, fears that his daughter will follow in her mothers footsteps and arranges a marriage with her cousin, whom she does not love. During a trip to Nevada with her father, she meets engineer Bayard Delavel, who saves her life when she is bitten by a snake; when her father finds her with Bayard in his cabin, he forces them to marry. Believing that Nadine does not love him, Delavel leaves her and prepares to sue for divorce. In Washington, Nadine is reconciled with her father and agrees to marry Hopper, a millionaire; she meets Delavel on the night of her engagement ball, however, and the lovers are reunited.

==Cast==
* Gloria Swanson - Nada and Nadine Pelham
* Alec B. Francis - Sir Edward Pelham
* Milton Sills - Bayard Delaval
* Frank Butler (writer)|F.R. Butler - Eustace 
* Raymond Brathwayt - Lord Crombie
* Helen Dunbar - Lady Crombie
* Julia Faye - Sadi Bronson Clarence Geldert - Bronson 
* Ann Grigg - Blenkensop
* Arthur Stuart Hull - Howard Hopper

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 


 