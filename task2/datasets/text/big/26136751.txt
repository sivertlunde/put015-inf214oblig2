It's My Turn (film)
{{Infobox film
| name           = Its My Turn
| image          = Its My Turn film.jpg
| image_size     =
| alt            =
| caption        = Movie Poster
| director       = Claudia Weill
| producer       = Martin Elfand Jay Presson Allen
| writer         = Eleanor Bergstein
| narrator       =
| starring       = Jill Clayburgh Michael Douglas Charles Grodin Patrick Williams Bill Butler
| editing        = Byron Buzz Brandt James Coblentz Marjorie Fowler David Bretherton
| studio         =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Its My Turn is a 1980 romantic comedy-drama film starring Jill Clayburgh, Michael Douglas, and Charles Grodin.

The film was directed by Claudia Weill and written by Eleanor Bergstein. The producers of Its My Turn cut out an erotic dancing scene from Bergsteins screenplay, which sparked her to go on and write a new script that would become the 1987 hit film Dirty Dancing.

The films title track, played during the final credits--Its My Turn (song)|"Its My Turn"—was sung by Diana Ross, with music by Michael Masser and lyrics by Carole Bayer Sager. It was released as a single and became a top ten hit on the Billboard (magazine)|Billboard Hot 100, peaking at number nine.

==Plot==
Kate Gunzinger is a mathematics professor at a Chicago university. She lives with a man named Homer in a comfortable but not terribly passionate relationship.

Kate travels to New York for a job interview and to attend the wedding of her widowed father. She meets the brides son, Ben Lewin, a former professional baseball player.

Ben is married, but a relationship develops with Kate. He takes her to Yankee Stadium for an old-timers day ceremony and eventually they have an affair. When they part, Kate goes back to Chicago and breaks up with Homer, not knowing what the future holds.

The first scene shows Kate Gunzinger in a lecture giving a correct proof of the snake lemma from homological algebra. 

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Jill Clayburgh || Kate Gunzinger
|-
| Michael Douglas || Ben Lewin
|-
| Charles Grodin || Homer
|-
| Beverly Garland || Emma
|-
| Steven Hill || Jacob
|-
| Dianne Wiest || Gail
|- Daniel Stern || Cooperman
|}

==Critical reception==
From Roger Ebert of The Chicago Sun-Times gave the film 2 stars out of 4:
 

The film was nominated for a Razzie Awards for Worst Screenplay for Eleanor Bergstein, but lost the award to Cant Stop the Music. 

== References ==
 
 

== External links ==
 
* 
* 
* 
 

 
 
 
 
 
 
 
 