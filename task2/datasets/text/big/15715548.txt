Demons in the Garden
{{Infobox film
| name           = Demons in the Garden
| image          = Demonios en el Jardín.jpg
| caption        = Spanish film poster
| director       = Manuel Gutiérrez Aragón
| producer       = Luis Megino
| writer         = Rafael Azcona Fernando Trueba
| narrator       =
| starring       = Ángela Molina, Ana Belén,   Encarna Paso,  Imanol Arias,   Eusebio Lazaro
| music          = Javier Iturralbe
| cinematography = José Luis Alcaine
| editing        = José Salcedo
| distributor    =
| released       =  
| runtime        = 97 minutes
| country        = Spain
| language       = Spanish
| budget         =
}} FIPRESCI Prize.   

==Plot==
In a small Spanish village, Gloria, the imperious head of a troubled family, is devoted to her two sons. The brothers, Óscar and Juan, have frequent violent disputes. Óscar, the oldest, and his steel-willed mother run the familys grocery store called el Jardín (The Garden), which does a brisk business in the black market. When Óscar marries Juans lover, Ana, Juan, the beloved handsome second son decides to better his fortunes in Madrid joining the Nationalist Army. Ana, who loves him, is heartbroken. Juan also leaves behind his impoverished cousin, Ángela, pregnant with his child. She moves to the countryside and gives birth to a boy, Juanito.

Years later, Óscar and Ana remain childless. Gloria wants to bring her illegitimate (and incestuous) grandson and only heir, into the family circle. When Juanito is stricken with rheumatic fever while searching for his father, Ángela agrees to let him stay at el Jardin. Everyone spoils the boy, and he becomes close to Ana. However, he misses his mother and she returns to live with the family. One day Juanito hears that his father will pass with Franco’s retinue; he finally meets him and discovers he is only a waiter in the service of Franco’s personal bodyguard.

The years move on and Juanito has recovered from the rheumatic heart condition, he is totally disenchanted with his father. He learns that his father is visiting Ana, Óscar’s wife. Because Juan now needs money, Ana steals one thousand pesetas from the family safe, the profits from the black-marketing the family practiced to survive during the war.

Gloria discovers the theft but blames it upon Ángela, who is dragged off to jail by the local Civil Guards. Gloria discovers the truth and urges Juan to return home to marry Ángela so that the family will have a legitimate heir. Ángela, however, refuses to marry the callow Juan, who is still in love with Ana. Meanwhile, Óscar and Juans sibling rivalry continues, and they fight fiercely in their own love–hate relationship, first over Ana, then Ángela.

The following day is Juanito’s "saints day," celebrated by the entire family. During the festivities, Juan tries to seduce first Ángela and then Ana in the hay barn. But Ángela is so enraged by Juans behavior that she shoots him in the shoulder with a hand gun. The scene is discovered by a shocked Gloria, but in the middle of the tumult, the entire family is gathered by the local photographer for a family portrait in front of el Jardín. They throw a jacket over Juans shoulders to cover up the blood from the bullet wound, and the film ends in a freeze frame of the photograph, offering no conclusions, but exposing all the hidden hypocrisy within this petite bourgeois family.

==Cast==
* Ángela Molina – Ángela
* Ana Belén – Ana
* Encarna Paso – Gloria
*Imanol Arias – Juan
* Eusebio Lazaro – Óscar
* Alvaro Sánchez Prieto – Juanito

==References==
 

==Notes==
* Schwartz, Ronald, The Great Spanish Films: 1950–1990, Scarecrow Press, London, 1991, ISBN 0-8108-2488-4

==External links==
* 

 

 
 
 
 
 
 
 
 
 