Return Home
 
 
{{Infobox Film name           = Return Home image          =  caption        =  producer       = Cristina Pozzan director       = Ray Argall  writer         = Ray Argall starring       = Dennis Coard Frankie J. Holden Ben Mendelsohn music          =  cinematography = Mandy Walker editing        = Ken Sallows distributor    =   released       = 2 August 1990 (Australia) 3 August 1990 (USA) runtime        = 87 minutes country        = Australia language       = English  budget         = A$350,000 David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p121 
| gross = A$60,000 
}}
 AFI Award for Best Director in 1990 and Frankie J. Holden was nominated for Best Actor in a Lead Role. 

==Cast==
*Dennis Coard as Noel
*Frankie J. Holden as Steve
*Ben Mendelsohn as Gary
*Mickey Camilleri as Judy
*Rachel Rains as Wendy
*Gypsy Lukewood as Clare
*Ryan Rawlings as Wally
*Paul Nestor as Brian
*Alan Fletcher as Barry

==Production==
The film was funded by the Australian Film Commission and Film Victoria with no private investment. It was shot in Adelaide over six weeks on 16mm but the AFC agreed to blow it up to 35mm. 

Argall and the cast rehearsed for four weeks prior to filming.  He said he worked on the script for seven years prior to the shoot. 

==Box Office==
Return Home grossed $60,000 at the box office in Australia,. 

==See also==
*Cinema of Australia

==References==
 

==Further reading==
* 

==External links==
* 
*Return Home at the  

 
 
 
 
 
 
 
 


 
 