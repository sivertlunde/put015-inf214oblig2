Handsome Harry
{{Infobox film
| name = Handsome Harry
| image = Handsome Harry.jpg
| caption = 
| director = Bette Gordon
| producer = Jamin OBrien Marilyn Haft Eric Goldman Jamey Sheridan
| writer = Nicolas T. Proferes
| starring = Jamey Sheridan
| music = Anton Sanko Jumaane Smith
| cinematography = Nigel Bluck
| editing = Keiko Deguchi
| studio = Worldview Entertainment
| distributor = Paladin Emerging Pictures Screen Media Films
| released =  
| runtime = 93 minutes
| country = United States
| language = English
| budget = $1 million
}} American film written by Nicholas T. Proferes and directed by Bette Gordon. The first project produced by Worldview Entertainment, it stars Jamey Sheridan, Steve Buscemi and Aidan Quinn. It premiered at the 2009 Tribeca Film Festival {{cite web |url=http://www.tribecafilm.com/news-features/tribeca-takes/Bette_Gordon_on_Handsome_Harry.html |title=
Tribeca Takes: Bette Gordon on Handsome Harry |accessdate=2010-04-07|work=New York City}}  and was released theatrically in 2010 by Paladin/Emerging Pictures and on DVD/VOD by Screen Media Films.

==Plot==
Handsome Harry is the story of Harry Sweeney’s journey to find forgiveness from an old Navy friend.  One day Harry (Jamey Sheridan) gets a call from an old Navy buddy, Kelly (Steve Buscemi), who is on his deathbed. At first, Harry wants nothing to do with Kelly, but soon memories and guilt overcome him and he goes on a journey to confront his old friends. First he goes to meet Kelly in a Philadelphia  hospital. Kelly asks Harry to seek forgiveness from David on his behalf. Kelly dies in the hospital the next day. Harry then goes on to meet the rest of his Navy buddies to find the truth about what actually happened the night they assaulted David together. Somebody dropped a generator on Davids hand that night, but Harry could not recollect who it was. In time it was revealed that Harry and David were having an affair back in the Navy. Kelly found Harry and David in a sexually compromising position in the shower. In fear of repercussion, Harry turned on David. Kelly and rest of the gang including Harry got drunk and assaulted David. It was in the end revealed that Harry was the one who dropped the generator on David, maiming him for life.

==Cast==
* Jamey Sheridan as Harry Sweeney
* Campbell Scott as David Kagan
* Steve Buscemi as Thomas Kelly
* Aidan Quinn as Professor Porter John Savage as Peter Reems
* Titus Welliver as Gebhardt Karen Young as Muriel
* Bill Sage as Pauly
* Mariann Mayberry as Judy Rheems

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 


 