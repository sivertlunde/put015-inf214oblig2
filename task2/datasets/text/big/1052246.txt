Topsy-Turvy
 
 
 
{{Infobox film
| name            = Topsy-Turvy
| image           = Topsy Turvy.jpg
| caption         = Film poster
| director        = Mike Leigh
| producer        = Simon Channing Williams
| writer          = Mike Leigh
| starring        = Jim Broadbent Allan Corduner Timothy Spall Lesley Manville
| music           = Sir Arthur Sullivan W. S. Gilbert Carl Davis Dick Pope
| editing         = Robin Sales
| studio = Thin Man Films
| distributor     = Pathé Distribution (UK) October Films (US)
| released        =  
| runtime         = 160 minutes
| country         = United Kingdom
| language        = English
| budget          = $20 million
| gross           = $6,201,757 
}} musical drama film written and directed by Mike Leigh and stars Allan Corduner as Sir Arthur Sullivan and Jim Broadbent as W. S. Gilbert, along with Timothy Spall and Lesley Manville. The story concerns the 15-month period in 1884 and 1885 leading up to the premiere of Gilbert and Sullivans The Mikado. The film focuses on the creative conflict between playwright and composer, and the decision by the two men to continue their partnership, which led to the creation of several more famous Savoy Operas between them.

The film was not released widely, but it received very favourable reviews, including a number of film festival awards and two design Academy Awards. While considered an artistic success, illustrating Victorian era British life in the theatre in depth, the film did not recover its production costs. Leigh cast actors who did their own singing in the film, and the singing performances were faulted by some critics, while others lauded Leighs strategy.

==Plot==
On the opening night of Princess Ida at the Savoy Theatre in January 1884, composer Sir Arthur Sullivan (Allan Corduner), who is ill from kidney disease, is barely able to make it to the theatre to conduct. He goes on a holiday to Continental Europe hoping that the rest will improve his health. While he is away, ticket sales and audiences at the Savoy Theatre wilt in the hot summer weather. Producer Richard DOyly Carte (Ron Cook) has called on Sullivan and the playwright W. S. Gilbert (Jim Broadbent) to create a new piece for the Savoy, but it is not ready when Ida closes. Until a new piece can be prepared, Carte revives an earlier Gilbert and Sullivan work, The Sorcerer.
 a popular exhibition of Japanese arts and crafts in Knightsbridge, London.  When the katana sword he purchases there falls noisily off the wall of his study, he is inspired to write a libretto set in exotic Japan. Sullivan likes the idea and agrees to compose the music for it.

Gilbert, Sullivan and Carte work to make The Mikado a success, and many glimpses of rehearsals and stressful backstage preparations for the show follow: Cast members lunch together before negotiating their salaries. Gilbert brings in Japanese girls from the exhibition to teach the ladies chorus how to walk and use fans in the Japanese manner. The principal cast react to the fittings of their costumes designed by William Charles John Pitcher|C. Wilhelm. The entire cast object to Gilberts proposed cut of the title characters Act Two solo, "A more humane Mikado," which persuades the playwright to reconsider. The actors face first-night jitters in their dressing rooms. Finally The Mikado is ready to open. As usual, Gilbert is too nervous to watch the opening performance and paces the streets of London. Returning to the theatre, however, he finds that the new opera is a resounding success.

==Cast==
* Jim Broadbent as W. S. Gilbert
* Ron Cook as Richard DOyly Carte, owner of the Savoy Theatre
* Allan Corduner as Sir Arthur Sullivan
* Eleanor David as Fanny Ronalds, Sullivans mistress
* Dexter Fletcher as Louis, Sullivans butler
* Vincent Franklin as Rutland Barrington, who plays Pooh-Bah
* Lesley Manville as Lucy "Kitty" Gilbert, Gilberts wife Richard Temple, who plays the Mikado Martin Savage as George Grossmith, who plays Ko-Ko
* Dorothy Atkinson as Jessie Bond, who plays Pitti-Sing
* Louise Gold as Rosina Brandram, who plays Katisha
* Shirley Henderson as Leonora Braham, who plays Yum-Yum
* Kevin McKidd as Durward Lely, who plays Nanki-Poo Helen Lenoir, Cartes indispensable assistant 
* Cathy Sara as Sybil Grey, who plays Peep-Bo
* Andy Serkis as John DAuban, choreographer
* Naoko Mori as Miss "Sixpence Please"
* Michael Simkins as Frederick Bovill, who plays Pish-Tush
* Sukie Smith as Clothilde, Sullivans maid
* Ashley Jensen as Miss Tringham, a member of the chorus
* Mark Benton as Mr. Price, a member of the chorus
* Steve Speirs as Mr. Kent, a member of the chorus
* Sam Kelly as Richard Barker
* Alison Steadman as Madame Leon 
* Katrin Cartlidge as the madame of a Paris brothel
* Brid Brennan as a mad beggar woman

==Depiction of Victorian society==
 ]]
Film professor Wheeler Winston Dixon wrote that the film "uses the conventions of the biographical narrative film to expose the ruthlessness and insularity of the Victorian era, at the same time as it chronicles, with great fidelity, the difficulties of a working relationship in the creative arts. ... Topsy-Turvy is an investigation into the social, political, sexual and theatrical economies of the Victorian era". 
 destruction of the British garrison at Khartoum by the Muhammad Ahmad|Mahdi; a private salon concert; a conversation about the use of nicotine by women; Sullivans visit to a French brothel; and Gilbert being accosted outside the theatre on opening night by a beggar. The film also depicts the Savoy Theatre as having electric lighting; it was the first public building in Britain &ndash; and at the time one of the few buildings there of any kind &ndash; to be lit entirely by electricity. Another scene shows an early use of the telephone. These scenes, some based on historical incidents, depict different aspects of Victorian society and life at the time. 

==Production==
Topsy-Turvy was filmed at  .  The films budget was $20,000,000. 

==Reception==
In the United Kingdom, the film grossed £610,634 in total and £139,700 on its opening weekend.  In the United States, the film grossed $6,208,548 in total, and $31,387 on its opening weekend.  The film has an 89% rating at Rotten Tomatoes,  and a 90 at Metacritic, indicating that critical reception was overall positive.  According to Janet Maslin, Topsy-Turvy is "grandly entertaining", "one of those films that create a mix of erudition, pageantry and delectable acting opportunities, much as Shakespeare in Love did: {{cite web| title= With Gilbert and Sullivan, Dreaming Up a Second Act
| url= http://movies.nytimes.com/movie/review?res=9907E0DD103EF931A35753C1A96F958260 | work= Critics Pick| date= 2 October 1999| authorlink= Janet Maslin| first=Janet |last= Maslin | publisher= The New York Times| accessdate=2011-07-16}} 
:Topsy-Turvy ... is much bigger than their story. Its aspirations are thrilling in their own right. Mr. Leighs gratifyingly long view of life in the theatre (Gilbert has a dentist who tells him Princess Ida could have been shorter) includes not only historical and biographical details but also the painstaking process of creating a Gilbert and Sullivan production from the ground up. The film details all this with the luxury of a leisurely pace, as opposed to a slow one. 
 Dick Popes photography, Eve Stewarts production design and Lindy Hemmings costumes", with "great music orchestrated by Carl Davis."    For Roger Ebert, it was "one of the years best films." 

Topsy-Turvy ranks 481st on Empire magazine|Empire s 2008 list of the 500 greatest movies of all time. 

==Awards and honours== Best Art Best Original Sleepy Hollow American Beauty, respectively.

The film also won Best Make Up/Hair at the BAFTA Awards, and was nominated for Best British Film, Best Actor in a Leading Role (Jim Broadbent), Best Supporting Actor (Timothy Spall) and Best Original Screenplay. Broadbent also won the Volpi Cup for Best Actor at the Venice Film Festival, and the film was nominated for the Golden Lion at the same festival.
 awards for Best Film (shared with Spike Jonzes Being John Malkovich) and Best Director from the National Society of Film Critics, and for Best Picture and Best Director at the 1999 New York Film Critics Circle Awards.  

==Home media==
A digitally restored version of the film, released on DVD and Blu-ray by The Criterion Collection in March 2011, includes an audio commentary featuring director Mike Leigh; a new video conversation between Leigh and musical director Gary Yershon; Leighs 1992 short film A Sense of History, written by and starring actor Jim Broadbent; deleted scenes; and a featurette from 1999 including interviews with Leigh and cast members.  

==References==

===Notes===
 

===Bibliography===
*  
*  
*Sragow, Michael.  , Salon.com, 23 December 1999

== External links ==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 