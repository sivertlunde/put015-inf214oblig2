In This Our Life
{{Infobox film
| name           = In This Our Life
| image          = In This Our Life poster.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = John Huston
| producer       = Hal B. Wallis Howard Koch Based on the novel by Ellen Glasgow
| narrator       = 
| starring       = Bette Davis Olivia de Havilland Charles Coburn George Brent
| music          = Max Steiner
| cinematography = Ernest Haller William Holmes
| studio         = 
| distributor    = Warner Bros.
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Howard Koch Pulitzer Prize-winning novel of the same title by Ellen Glasgow. The cast included the established stars Bette Davis and Olivia de Havilland as sisters and the romantic and life rivals. Raoul Walsh also worked as director, taking over when Huston was called away for a war assignment after the United States entered World War II, but he was uncredited.

Completed in 1942 after the US had joined the war, the film was disapproved in 1943 for foreign release by the wartime Office of Censorship, because it dealt truthfully with racial discrimination as part of its plot.

==Plot==
In  ) and Stanley (Bette Davis). The movie opens with the young women as adults. Asa Timberlake has recently lost his piece of a tobacco company to his former partner William Fitzroy (Charles Coburn), his wifes brother.  Roy, a successful interior decorator, is married to Dr. Peter Kingsmill (Dennis Morgan). Stanley is engaged to progressive attorney Craig Fleming (George Brent). The night before her wedding, Stanley runs off with Roys husband Peter. Fleming becomes and stays depressed, but Roy soon decides to keep a positive attitude. After Roy divorces Peter, he and Stanley marry and move to Baltimore.

Roy encounters Fleming again after some time, and she encourages him to move on with his life. They soon begin dating. Roy refers a young black man, Parry Clay (Ernest Anderson), to Fleming, and he hires him to work in his law office while he attends law school. Clay is the son of the Timberlake parents family maid, Minerva Clay (Hattie McDaniel).

William Fitzroy, Lavinias brother and Asas former partner in a tobacco business, doted on his niece Stanley and gave her expensive presents and money, but was very upset when she ran off. He says he will throw Fleming some of his legal business if he agrees to stop representing poor (black) clients. When Fleming refuses, Roy Timberlake is impressed and decides to accept him in marriage.

In Baltimore, Stanley and Peters marriage suffers from his heavy drinking and her excessive spending. Peter commits suicide. Shaken, Stanley returns to her home town with Roy.  After she recovers, Stanley decides to win back Fleming. While discussing her late husbands life insurance with Fleming at his office, Stanley invites him to join her later for dinner. When he fails to come to the restaurant, she becomes drunk. While driving home, she hits a young mother and her young daughter, severely injuring the woman and killing the child, and, in a panic, Stanley drives away.

The police find Stanleys car abandoned with front-end damage and go to question her.  Stanley insists she had loaned her car to Parry Clay the night of the accident. Minerva Clay tells Roy that her son was home with her all evening. Stanley refuses to admit her responsibility although Roy arranges for her to see Clay at the jail (he is being held as a suspect).  Later Fleming tells her he has already questioned the bartender at the restaurant and knows Stanley left drunk. Fleming plans to take Stanley to the district attorney, but she escapes to her uncles house and pleads for his help. Having just discovered he has only six months to live, Fitzroy is too distraught to do anything. The police arrive at Fitzroys house and Stanley leaves; in trying to escape, she crashes her car and is killed.

== Cast ==
 
* Bette Davis ..... Stanley Timberlake Kingsmill
* Olivia de Havilland ..... Roy Timberlake Fleming
* George Brent ..... Craig Fleming
* Dennis Morgan ..... Peter Kingsmill
* Frank Craven ..... Asa Timberlake
* Billie Burke ..... Lavinia Timberlake
* Charles Coburn ..... William Fitzroy
* Ernest Anderson ..... Parry Clay
* Hattie McDaniel ..... Minerva Clay Lee Patrick ..... Betty Wilmoth
* Mary Servoss  ...  Charlotte Fitzroy  
* William B. Davidson ..... Jim Purdy  
* Edward Fielding  ...  Dr. Buchanan   John Hamilton ..... Inspector   William Forrest ..... Forest Ranger

==Production== racist attitudes in the society.  Recommended by the director John Huston, the screenwriter Howard Koch believed he had to tone down these elements to satisfy the current Motion Picture Production Code.    In his review of the completed film, the critic Bosley Crowther said it was "moderately faithful" to the novel and praised its portrayal of racial discrimination. 

Bette Davis, eventually cast as Stanley Timberlake despite her desire to play the "good sister" Roy,    was unhappy with the script. "The book by Miss Glasgow was brilliant," she later recalled. "I never felt the script lived up to the book."  Nor did Glasgow. "She minced no words about the film," Davis said. "She was disgusted with the outcome. I couldnt have agreed with her more. A real story had been turned into a phony film." 
 Arthur Farnsworth had been admitted to a Minneapolis hospital with severe pneumonia. Her friend Howard Hughes arranged a private plane, but her flight took two days because of being grounded by fog and storms. Almost immediately, studio head Jack L. Warner cabled her to demand her return to the film. Due to his pressure and her concern for her husband, Davis own health declined.  Her doctor ordered her to return to Los Angeles by train to get some rest before returning to work. 
 National Board of Review Award for his performance.
 War Department.  The studio used Raoul Walsh to complete the film, although he received no screen credit. Walsh and Davis immediately clashed, and she refused to follow his direction or reshoot completed scenes.  She developed laryngitis and stayed off the set for several days. After she returned, the producer Hal B. Wallis frequently acted as mediator between Davis and Walsh, who threatened to quit.

Because of the delays, the film was not done until mid-January 1942, well over schedule. The first preview was highly negative, with audience comments especially critical of Davis hair, makeup, and wardrobe, the elements which she had controlled.  Preparing for Now, Voyager, Davis disregarded the comments.  She thought the film was "mediocre," although she was glad that the role of Parry Clay was "performed as an educated person. This caused a great deal of joy among Negroes. They were tired of the Stepin Fetchit vision of their people."  When the US wartime Office of Censorship reviewed the film in 1943 prior to foreign release, it disapproved the work because, "It is made abundantly clear that a Negros testimony in court is almost certain to be disregarded if in conflict with the testimony of a white person." 

==Critical reception==
Bosley Crowther of the New York Times called it "neither a pleasant nor edifying film." He felt "the one exceptional component of the film" is the "brief but frank allusion to racial discrimination" which "is presented in a realistic manner, uncommon to Hollywood, by the definition of the Negro as an educated and comprehending character. Otherwise the story is pretty much of a downhill run." He added, "Director John Huston, unfortunately, has not given this story sufficient distinction . . . The telling of it is commonplace, the movement uncomfortably stiff. Olivia de Havilland gives a warm and easy performance as the good sister who wins out in the end . . . But Miss Davis, by whom the whole thing pretty much stands or falls, is much too obviously mannered for this spectators taste . . . It is likewise very hard to see her as the sort of sultry dame that good men cant resist. In short, her evil is so theatrical and so completely inexplicable that her eventual demise in an auto accident is the happiest moment in the film."   

Variety (magazine)|Variety noted, "John Huston, in his second directorial assignment, provides deft delineations in the varied characters in the script. Davis is dramatically impressive in the lead but gets major assistance from Olivia de Havilland, George Brent, Dennis Morgan, Billie Burke and Hattie McDaniel. Script succeeds in presenting the inner thoughts of the scheming girl, and carries along with slick dialog and situations. Strength is added in several dramatic spots by Hustons direction." 

==DVD release== The Old Maid, All This, and Heaven Too, Watch on the Rhine, and Deception (1946 film)|Deception.

==References==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 