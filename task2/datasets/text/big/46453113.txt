L'Astragale
 

{{Infobox film
| name           = LAstragale
| image          = LAstragale poster.jpg
| caption        = Film poster
| director       = Brigitte Sy
| producer       = Paulo Branco
| screenplay     = Serge Le Péron Brigitte Sy 
| based on       =  
| starring       = Leïla Bekhti   Reda Kateb   Esther Garrel
| music          = Béatrice Thiriet 
| cinematography = Frédéric Serve  
| editing        = Julie Dupré 
| studio         = Alfama Films   France 3 Cinéma
| distributor    = Alfama Films 
| released       =  
| runtime        = 96 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

LAstragale is a 2015 French drama film directed by Brigitte Sy. It is the second film adaptation of the 1965 semi-autobiographical novel LAstragale by Albertine Sarrazin, after Guy Casarils LAstragale (1968). 

== Cast ==
* Leïla Bekhti as Albertine Damien
* Reda Kateb as Julien
* Esther Garrel as Marie
* Jocelyne Desverchère as Nini
* India Hair as Suzy
* Jean-Charles Dumay as Roger
* Jean-Benoît Ugeux as Marcel
* Louis Garrel as Jacky
* Delphine Chuillot as Catherine
* Zimsky as Riton 
* Billie Blain as Coco 
* Brigitte Sy as Rita
* Suzanne Huin as Marilyne
* Yann Gael as Etienne

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 

 