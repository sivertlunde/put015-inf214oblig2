Himmat (1970 film)
{{Infobox film
| name           = Himmat
| image          = Himmat 1970 film poster.JPG
| caption        = Theatrical poster
| director       = Ravikant Nagaich
| producer       = P. Mallikharjuna Rao
| writer         = V.D. Puranik   S.M. Abbas (dialogues) Mumtaz Prem Chopra Aruna Irani
| music          = Laxmikant Pyarelal   Anand Bakshi (Lyrics)
| cinematography = Ravikant Nagaich
| editing        = N.S. Prakasam Prasad Productions Pvt. Ltd.
| released       = 1970
| runtime        = 152 minutes
| country        = India
| language       = Hindi
}}
 1970 Indian Mumtaz in Asit Sen and Jagdeep.

==Plot==
Movie Concept  "once a criminal, never a criminal". Raghu is released from prison And want to live the simple life. But police always suspect. One day Raghu is stuck in a case. How will he prove that he is innocent ?

==Cast==
* Jeetendra as Raghu Mumtaz as Malti
* Prem Chopra as Boss
* Naaz as  Banthu
* K.N. Singh as Inspector Mathur Asit Sen as Dhaniram
* Jagdeep as Tiger
* Tun Tun as Tigers mom
* Aruna Irani as Rita
* Lakshmi as Club Dancer 
* Brahm Bhardwaj as Raju
* Sachin as Young Raghu Agha as Sethji
* Praveen Paul
* Joshi
* Pratibha
* J.N. Anand
* Kiran
* Nadir
* Akhtar
* Raja Rao
* Lata Arora
* Jayadev

==Crew==
* Director = Ravikant Nagaich
* Producer = P. Mallikharjuna Rao	
* Cinematography = Ravikant Nagaich
* Art Director = S. Krishnarao
* Costume Designer = Bhanu Athaiya, Surya Rao
* Makeup Department = Ganesh, Geeta, Kuppuswamy, Akhtar Mercy, Jayanti Ramprasad, Khatoon Rijal and Connie Rodriqes
* Special Effects = Syed Galib (titles writer), V. Madan Mohan (special effects)

==Soundtrack==
The lyrics written by Anand Bakshi are composed into songs by the musical duo Laxmikant Pyarelal.
{{Track listing
| extra_column = Singer(s)
| title1 = Man Jayiye Man Jayiye Bat Mere Dil Kee Jan Jayiye | extra1 = Asha Bhosle, Mohammad Rafi  | length1 = 04:42
| title2 = Hai Shukr Ki Tu Hai Ladka | extra2 = Mohammad Rafi  | length2 = 04:00
| title3 = Himmat Kare Insan To Kya Ho Nahi Sakta | extra3 = Mohammad Rafi  | length3 = 04:58
| title4 = Main Hun Akeli | extra4 = Asha Bhosle | length4 = 04:56
| title5 = Das Gayi Nagin Si Zulf Teri Bas Jan Nikal Gayi Meri | extra5 = Lata Mangeshkar, Mohammad Rafi  | length5 = 04:31
| title6 = Jaal Bichhe Hain Nigahon Ke | extra6 = Asha Bhosle | length6 = 04:05
| title7 = Hip Hip Aao Piyo Peene Ka Zamana Aaya | extra7 = Lata Mangeshkar, Mohammad Rafi  | length7 = 03:08
}}

==See also==
* Jeeo Aur Jeene Do
* Raksha (film) 
* Jawab (1970 film) 
* Kachche Heere

==References==
 

==External links==
 

 
 
 


 