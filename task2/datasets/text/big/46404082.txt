Brotherly Love (2015 film)
{{Infobox film
| name           = Brotherly Love
| image          = BrotherlyLoveMoviePoster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jamal Hill 
| producer       = Charles "Charlie Mack" Alston Yaneley Arty Ron Robinson Shelby Stone
| writer         = Jamal Hill
| starring       = Keke Palmer Cory Hardrict Quincy Brown
| music          = Chris Paultre 
| cinematography = Matthew Joffe
| editing        = Jessica Hernández Todd Wolfe Flavor Unit Films Javaci Films Electric Republic
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 89 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = $379,185 
}}
 Flavor Unit Films.  Brotherly Love was distributed by Freestyle Releasing and had a limited theatrical release in the United States on April 24, 2015. 

==Synopsis== Overbrook High School, Brotherly Love follows star high school basketball player Sergio Taylor (Hill) as he struggles navigating the fame that comes with being a star athlete. Sergios older brother June (Cory Hardrict|Hardrict) saw his own basketball dreams fade away when he turned to the streets to provide for his family after the death of their father. Segios twin sister Jackie (Keke Palmer|Palmer) saw her ambitions of having a music career side tracked after unexpectedly falling in love with Chris (Quincy Brown|Brown).  

==Cast==
*Keke Palmer as Jackie
*Cory Hardrict as June
*Quincy Brown as Chris
*Eric D. Hill, Jr. as Sergio
*Romeo Miller as Sean
*Logan Browning as Trina
*Macy Gray as Mrs. Taylor
*Malik Yoba as Coach
*Faizon Love as Uncle Ron Jay Lewis as Peanut

==References==
 

==Internal links==
*French language:  

==External links==
*  
*  
*  

 
 
 
 
 
 