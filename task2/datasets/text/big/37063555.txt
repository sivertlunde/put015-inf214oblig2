In the Shadow (2012 film)
 
{{Infobox film
| name           = In the Shadow
| image          = 
| caption        = 
| director       = David Ondříček
| producer       = 
| writer         = Marek Epstein Misha Votruba David Ondříček
| starring       = Ivan Trojan
| music          = Jan P. Muchow Michal Novinski
| cinematography = Adam Sikora
| editing        = Michal Lánsky
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Czech Republic
| language       = Czech
| budget         = CZK 70,000,000
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Ivan Trojan as Captain Hakl
* Sebastian Koch as Major Zenke
* Soňa Norisová as Jitka
* Jiří Štěpnička as Pánek
* David Švehlík as Beno
* Marek Taclík as Bareš
* Filip Antonio as Tom
* Martin Myšička as Jílek
* Miroslav Krobot as Kirsch
* Halka Třešňáková as Translator

==Awards==
In the Shadow won nine Czech Lions at the 2012 awards.   

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Czech submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 