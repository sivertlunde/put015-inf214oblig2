The King's Jester
{{Infobox film
| name =  The Kings Jester
| image =
| image_size =
| caption =
| director = Mario Bonnard
| producer =  
| writer =   Victor Hugo (play)   Tomaso Smith   Carlo Salsa   Mario Bonnard   Michel Simon
| narrator =
| starring = Michel Simon   María Mercader   Rossano Brazzi   Doris Duranti
| music = Giuseppe Verdi (opera)   Giulio Bonnard  
| cinematography = Ubaldo Arata 
| editing = Dolores Tamburini     
| studio = Scalera Film 
| distributor = Scalera Film 
| released = 25 October 1941 
| runtime = 92 minutes
| country = Italy Italian 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Kings Jester (Italian:Il re si diverte) is a 1941 Italian historical film directed by Mario Bonnard and starring Michel Simon, María Mercader and Rossano Brazzi. The film is an adaptation of the play Le roi samuse by Victor Hugo and uses music from Verdis later opera Rigoletto.  It is set at the court of Francis I of France in the Sixteenth century.

==Cast==
* Michel Simon as Rigoletto  
* María Mercader as Gilda 
* Rossano Brazzi as Il re Francesco Iº  
* Doris Duranti as Margot 
* Paola Barbara as La duchessa di Cosse  
* Elli Parvo as La zingara  
* Carlo Ninchi as Il conte di Saint Vallier  
* Juan de Landa as Sparafucile   Loredana as Diana Di Saint Vallier 
* Franco Coop as Il duca di Cosse  
* Corrado Racca as Signor De Brion
* Giulio Battiferri as Uno dei rapitori di Gilda  
* Oreste Bilancia as Un cortegiano  
* Gildo Bocci as Il Gran Visir  
* Ruggero Capodaglio as Un cortegiano  
* Stanis Cappello as Un cortegiano 
* Renato Chiantoni as Il terzo zingaro 
* Gemma DAlba as Signora De Pardaillan 
* Alfredo De Antoni as Un cortegiano  
* Liana Del Balzo as La marchesa che nasconde letà  
* Cesare Fantoni as Il sergente Roland 
* Oreste Fares as Il servitore di Saint Vallier  
* Adele Garavaglia as Costanza, la governante  
* Fausto Guerzoni as Il primo zingaro 
* Guido Morisi as Il duca De La Tour  
* Giacomo Moschini as Signor De Pardaillan  
* Giovanni Onorato as Il secondo zingaro  
* Amina Pirani Maggi as Berarda, governante di Gilda  
* Cesare Polesello as Il quarto zingaro  
* Evaristo Signorini as Un cortegiano  
* Aldo Silvani as Il boia  
* Edda Soligo as Unamica della duchessa di Cosse 
* Edoardo Toniolo as Il poeta Marot

== References ==
 

== Bibliography ==
* Fawkes, Richard. Opera on Film. Duckworth, 2000. 
* Klossner, Michael. The Europe of 1500-1815 on Film and Television. McFarland & Co, 2002.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 