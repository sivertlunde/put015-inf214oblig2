Viva la Muerte (film)
{{Infobox film
| name           = Viva la muerte
| image          = 1971 French theatrical release poster for Arrabals film Viva la muerte.jpeg
| director       = Fernando Arrabal
| producer       = Hassen Daldoul Jean Velter
| writer         = Fernando Arrabal
| starring       = Mahdi Chaouch Anouk Ferjac Núria Espert
| music          =
| cinematography = Jean-Marc Ripert
| editing        = Laurence Leininger
| studio         = Isabelle Films Satpec
| distributor    = Alliance Releasing Corporation
| released       =  
| country        = France Tunisia
| runtime        = 90 minutes French
}} communist by cult popularity as a midnight movie. The opening credits sequence features drawings by acclaimed artist, actor and novelist Roland Topor.

==Synopsis==
When Fandos fascism|fascist-sympathizing mother turns his father into the authorities as a suspected communist, Fando (Mahdi Chaouch) is told that his father was executed. In truth the father is actually just imprisoned and eventually begins to search for him, constantly imagining what his father might be up to or what might have happened to him. 

==Cast==
*Anouk Ferjac as La Tante
*Núria Espert as La Mère
*Mahdi Chaouch as Fando
*Ivan Henriques as Le Père
*Jazia Klibi as Thérèse
*Suzanne Comte as La Grand-mère
*Jean-Louis Chassigneux as Le Grand-père
*Mohamed Bellasoued as Colonel
*Víctor García  as Fando - 20 ans

==Reception==
Allmovie gave Viva la Muerte four stars, remarking that the films extreme visuals would make it "not for the faint of heart".  The New York Times gave the film a mostly positive review, stating that while it was "no perfect movie, it seems to me inescapably a major work." 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 

 
 