La Anam
 
 
{{Infobox film
| name           = La Anam  لا أنام 
| image          = La-Anam-DVD.jpg
| image_size     =
| caption        = La Anam DVD cover
| director       = Salah Abouseif
| producer       = Abdelhalim Nasr Ittihad Elfananeen
| writer         = Ihsan Abdel Quddous El Sayed Bedeir Saleh Gawdat
| narrator       =  cast below
| music          = Fouad El-Zahry
| cinematography = Mahmoud Nasr Abdelhalim Nasr Rotana (DVD)
| released       = November 31, 1957
| runtime        = 127 minutes
| country        = Egypt Arabic
| budget         = 
}}

La Anam   ( , Sleepless) is a 1957 Egyptian melodrama film. The film follows the intricate story of Nadia Lutfi, a daughter of divorced parents who suffers from Electra complex, which drives her to intervene in her fathers relationships.   
 Egyptian film novel with Egyptian film by the cinema committee of the Supreme Council of Culture in Cairo,       starred Faten Hamama and Yehia Chahine, along with five other prominent Egyptian actors. It was released on DVD as part of the Egyptian Cinema Classics film collection.    La Anam is one of the ten first Egyptian colored films.   

== Plot ==

Faten Hamama plays Nadia Lutfi, a young woman who belongs to an aristocratic, upper-class family. After her parents divorce each other, her father wins custody of her. She lives with her father (Yehia Chahine) and over the years develops a very close and strong relationship with him to the extent of being sexually attracted towards him, an obsessive behavior known as Electra complex.          

At the age of 18, she plunges into a premature relationship with Mustafah (Imad Hamdi), a writer and journalist who is quite older a bit than she is. Meanwhile, her father meets and falls in love with another woman, Safia (Myriam Fakhr Eddine). He decides to marry her, which Nadia is forced to accept. However, her father gradually starts spending more time with his new wife, which galls and displeases Nadia who does not receive an equal amount of love and attention anymore. This compels and drives her to step in and ruin their relationship. Now seeing Safia as her enemy, Nadia plots for revenge from her stepmother. Nadia accuses Safia of having an affair with Aziz (Omar Sharif), Nadias young uncle.   

To convince her father, Nadia had to ask her friend Kawthar (Hind Rostom), a voluptuous and licentious woman, to seduce her father. Nadias successful conspiracy results in a divorce between Safia and her father. Not only that, Nadias father decides to marry her. Enticed by the wealth of Nadias father, she agrees to marry him. Nadia is devastated when she discovers that Kawthar is having an affair with another young man, Samir (Rushdy Abaza), realizing that Kawthar is only living with her father for his fortunes.   

Mustafah, who had secretly admired Safia, proposed to her after her divorce with Nadias father. Nadia conceals the truth from her father, worried that such a revelation might strike and shatter him. Her father finds about Samir. Nadia, finding no escape route, lies and tells her father that he is her fiancé. Kawthar nefariously adds that Nadia and Samir are getting married, which Nadia is forced to accept. On the night of her wedding, Nadia shocks her father and the wedding attendants with the startling truth, leaving her father traumatized.  

== Cast ==
 ) in La Anam]]

*Faten Hamama as Nadia Lutfi
*Yehia Chahine as Nadias father
*Mariam Fakhr Eddine as Safia
*Imad Hamdi as Mustafah
*Hind Rostom as Kawthar
*Rushdy Abaza as Samir
*Omar Sharif as Aziz

== Reception ==
La Anam premiered at the Cinema Miami theater in Cairo on November 31 , 1957  and was met with a lot of success and recognition in Egypt and the Arab-speaking world. This was mainly due to the star-studded cast and the fact that it was one of the earliest colored Egyptian films. The film has maintained its popularity and has been selected amongst the best Egyptian films in 1996 by the Egyptian Film association.  However, due to its controversial topic, it wasnt frequently broadcast on television. {{cite web | url =http://fineartfilm.com/index.php?main_page=product_info&cPath=87_100&products_id=262&zenid=c11c48d241c3efad0e7e3077c2052871
 | title =Sleepless DVD cover | accessdate =March 14, 2007 | publisher=Fine Art Film | language =Arabic}} 

== Trivia ==
*The well-known Egyptian actress Nadia Lutfi got her stage name from Nadia Lutfi in La Anam.    

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 