Buckaroo Bugs
 
{{Infobox Hollywood cartoon
| series            = Looney Tunes (Bugs Bunny)
| image             = Buckaroo Bugs title card.png
| caption           = Title Card Robert Clampett
| story_artist      = Lou Lilly
| animator          = Manny Gould|M. Gould   Robert McKimson (uncredited)   Rod Scribner (uncredited)
| layout_artist     = 
| background_artist = 
| editor            = 
| voice_actor       = Mel Blanc Robert C. Bruce (uncredited)
| musician          = Carl W. Stalling
| producer          = Leon Schlesinger Leon Schlesinger Productions
| distributor       = Warner Bros. Pictures 
| release_date      = August 26, 1944 (USA)
| color_process     = Technicolor
| runtime           = 9 minutes
| movie_language    = English
}}
Buckaroo Bugs is a Warner Bros. Looney Tunes theatrical cartoon short released in August 1944, introducing Bugs Bunny to Looney Tunes and directed by Robert Clampett.

==Plot==
The film is set in a small town of the "San Fernando Alley" ( , which is another World War II reference. 
 Take It or Leave It). He then kisses him and blows out the candle.

==Availability==
The cartoon has been released on VHS in anonymous Bugs Bunny collections, and is also featured on the   of the Looney Tunes Golden Collection DVD set, released on October 30, 2007 and the Looney Tunes Platinum Collection Volume Two DVD.

==Production details==
 
*This was the last time the cartoon release bore Leon Schlesingers name, as he sold his cartoon studio to Warner Bros. around the time of its release. 
*While only Manny Gould was credited as an animator, Robert McKimson and Rod Scribner also aided in the process.
*The older version of Bugs Bunny would be used again in the next Bugs short, The Old Grey Hare.

== Sources ==
*  
*  

==References==
 

==External Links==
 

 
{{succession box 
| before = Hare Force  Bugs Bunny Cartoons 
| years  = 1944
| after  = The Old Grey Hare
}}
 

 
 
 
 
 
 
 
 