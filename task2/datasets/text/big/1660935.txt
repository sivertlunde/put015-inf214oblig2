Wintertime
 
 
{{Infobox film
| name           = Wintertime
| image          = HenieWintertime.jpg
| caption        =
| writer         = E. Edwin Moran Jack Jevne Lynn Starling Arthur Kober (story)
| starring       = Sonja Henie Jack Oakie Cesar Romero
| director       = John Brahm
| producer       = William Le Baron
| music          = Leo Robin Nacio Herb Brown
| cinematography = Joseph MacDonald Glen MacWilliams
| editing        = Louis R. Loeffler
| distributor    = Twentieth Century-Fox
| released       =  
| runtime        = 82 min.
| country        = United States
| language       = English
}}
 
Wintertime is a 1943 Twentieth Century-Fox musical film starring Sonja Henie and Cesar Romero, and featuring Woody Herman and His Orchestra. 

==Plot==
Norwegian millionaire Ostgaard (S.Z. Sakall) and his niece Nora (Sonja Henie) believe they will be staying at a posh resort in Canada, but it turns out owner Skip Hutton (Jack Oakie) and partner Freddy Austin (Cornel Wilde) are in debt and barely holding off foreclosure.

Nora schemes to get her uncle to invest in hotel improvements. She also falls for Freddy, although hes busy spending time with magazine photographer Marion Daly (Carole Landis), trying to gain publicity for the resort.

When more money is needed, Nora is offered a chance to skate in New York in a revue. But due to a legal technicality, she cannot enter the United States unless she is married to an American citizen, so handsome Brad Barton (Cesar Romero) gladly volunteers.

==Cast==
* Sonja Henie as Nora Ostgaard
* Jack Oakie as Skip Hutton
* Cesar Romero as Brad Barton
* Carole Landis as Marion Daly
* S.Z. Sakall as Uncle Ostgaard
* Cornel Wilde as Freddy Austin
* Woody Herman and His Orchestra as Themselves Helene Reynolds as Flossie
* Don Douglas as Jay Rogers
* Geary Steffen as Jimmy, Sonjas Skating Partner

==Songs==
* "I Like It Here"  ... (performed by Cesar Romero and Helene Reynolds)
* "Jingle Bells" ... (performed by Woody Herman and His Orchestra)
* "Wintertime" ... (performed by Woody Herman and His Orchestra)
* "We Always Get Our Girl" ... (performed by Woody Herman and His Orchestra)
* "Dancing in the Dawn" ... (performed by Woody Herman and His Orchestra)
* "Later Tonight" ... (performed by Woody Herman and His Orchestra)

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 


 