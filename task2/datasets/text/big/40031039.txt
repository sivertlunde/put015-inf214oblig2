Mary Magdalene (1914 film)
 
{{Infobox film
| name           = Mary Magdalene
| image          =
| caption        =
| director       = Arthur Maude
| producer       =
| writer         =
| starring       = Arthur Maude Constance Crawley
| cinematography =
| editing        =
| studio         = Kennedy Features Warner Features (USA)
| released       =  
| runtime        = Three reels (about 12 minutes per reel)
| country        = United States Silent English English intertitles
| budget         =
| gross          =
| website        =
| amg_id         =
}}
  (center) in the 1914 film Mary Magdalene. ]]
Mary Magdalene is a 1914 silent film that is loosely based on the 1910, three-act play of the same name by Belgian playwright Maurice Maeterlinck.

==Plot==
The story takes place in Capernaum and Jerusalem during the two years leading up to the crucifixion of Jesus Christ, and follows the lives of Judas Iscariot (Arthur Maude) and Mary Magdalene (Constance Crawley), who indulge in their own selfish pursuits and care little about the plights of others. But when Mary hears Jesus preach an outdoor sermon, she learns the power of Christs love and abandons her reckless ways to become one of his most ardent followers.      

==Production== Mary Magdalene color film. Maeterlincks mistress Georgette Leblanc, who had appeared in the title role of the original production of play, was retained to star in the movie.  Although Urban planned an Autumn 1913 release in London, and actual filming did take place in Europe, a final product never reached the screen. Instead, Aubrey Kennedy of Kennedy Features in Los Angeles, California, released his own version of Mary Magdalene in February 1914 that was filmed at cameraman James Crosbys J.A.C. Studio near downtown Los Angeles.    

Kennedy Features sought investors (state-right buyers) for the film by advertizing that Mary Magdalene was "a stupendous production that will create a sensation wherever shown."     One theater ad that appeared after the 4 February 1914 release of the film even billed it as "the most magnificent melodramatic feature America has ever seen."  Although reviews of the film were generally positive,   and it was still being shown in theaters as late as November 1915,  little mention of Mary Magdalene appears in motion picture trade journals such as The Moving Picture World and Motography in the months after the film was released. 

==Cast==
*Constance Crawley as Mary Magdalene
*Arthur Maude as Canis Proculas
*William Nigh as Syrius Superbus Joe Harris as Judas Iscariot
*Jefferson Osborne 

==Notes and references==
 

==External links==
* 

 
 
 
 
 