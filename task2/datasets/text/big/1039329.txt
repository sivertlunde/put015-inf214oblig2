Blank Check (film)
{{Infobox film
 | name = Blank Check
 | image = Blank Check film poster.jpg
 | caption = Theatrical release poster
 | director = Rupert Wainwright
 | producer = Gary Adelson Craig Baumgarten Tony Shimkin
 | writer = Blake Snyder Colby Carr Michael Lerner
 | music = Nicholas Pike
 | cinematography = Bill Pope
 | editing = Hubert de la Bouillerie Jill Savitt
 | studio = Walt Disney Pictures Buena Vista Pictures
 | released = February 11, 1994 
 | runtime = 94 minutes
 | language = English
 | budget = $13 million
 | gross = $30.5 million (domestic)  
}}
 Michael Lerner and was released by Walt Disney Pictures.

==Plot==
The story begins when convicted bank robber Carl Quigley (Miguel Ferrer) escapes from prison. Soon after his prison break, Quigley enters a warehouse and recovers $1,000,000 he had hidden there sometime before his arrest (although it is unclear exactly how he illegally obtained the money). The film then cuts to 10-year-old Preston Waters (Brian Bonsall), whose father (James Rebhorn) works as an investor for a living and is very frugal with money—so much so that when he is given a blank check from his grandmother for his birthday, his dad fills it out for only $11.00. Plus, when invited to a friends birthday party at an amusement park, he only has enough tokens to go on the kiddie rides.

After Quigley visits bank president Edward Biderman (  that he has presumably purchased with the stolen money, while Preston was riding out of the banks parking lot. Pressed for time as he sees a police car patrolling the area, Quigley gives Preston a signed blank check and tells him to give it to his dad so they can buy him a new bike. Instead, Preston fills out the check for $1,000,000 by printing it on his computer. He goes to the bank the next day and is directed to Bidermans office by a teller (as the teller could not cash a check that size herself). Thinking that Preston is Quigleys assistant, Juice, Biderman cashes his check with $1,000,000 from a safe behind a painting.

As Preston is leaving the bank, the real Juice enters Bidermans office with another check for $1,000,000. Realizing that Biderman mistook Preston for Juice, the trio begins a frantic search for Preston. Meanwhile, he embarks on an extreme shopping spree over the course of 6 days, buying a castle-style house (by outbidding Quigley using the voice box on his computer over the phone) along with many other expensive items (limousine service, go-kart track, water slide, etc.). He spends $999,667.83 of the original $1,000,000. Preston covers himself by saying he is making these purchases for a millionaire known as "Macintosh" (named after Prestons computer) who lives in the castle house.

The entire time, Preston was being investigated by FBI agent Shay Stanley (Karen Duffy) (working undercover as a teller at Bidermans bank and Prestons love interest) for money laundering, as the bills he was using to make his purchases were Bidermans watermarked ones. At a birthday party Preston throws for Mr. Macintosh that forced him into debt (it was actually his birthday) leaving only $332.17 in his account, he is forced into a showdown with Quigley, Juice, and Biderman. After the trio manage to capture him and demand to know what happened with the money, he admits Macintosh is a false name, to which Biderman suggests that Quigley can use Prestons purchases and the Macintosh name to give himself a new identity. 

When the trio is confronted by the FBI at Prestons castle house, Quigley claims to be Macintosh. However, with the FBI knowing that Mr. Macintosh had been using the watermarked bills, they arrest Quigley, Juice, and Biderman. Preston and Shay share a sweet kiss before parting ways. After Preston gets home his family throws him a birthday party. His father apologizes for being so harsh to him when it came to money.

==Cast==
*Brian Bonsall &ndash; Preston Waters
*Karen Duffy &ndash; Shay Stanley
*Miguel Ferrer &ndash; Carl Quigley aka Mr. Macintosh
*Tone Lōc &ndash; Juice Michael Lerner &ndash; Edward Biderman
*James Rebhorn &ndash; Fred Waters
*Jayne Atkinson &ndash; Sandra Waters
*Michael Faustino &ndash; Ralph Waters
*Chris Demetral &ndash; Damien Waters
*Rick Ducommun &ndash; Henry
*Maxwell Strachan &ndash; Quincy Carmichael
*Alex Zuckerman &ndash; Butch
*Alex Morris &ndash; Riggs
*Debbie Allen &ndash; Yvonne
*Mary Chris Wall &ndash; Betty Jay 
*Angee Hughes &ndash; Woman in Parking Lot

==Production== The Rattler Power Surge, were filmed in this movie. 

==Reception==
Reviews from critics were negative, with The Los Angeles Times stating that what was "missing from this film is any trace of the joy in simple pleasures. Preston isnt a very imaginative child; hes a goodies gatherer."  Janet Maslin of The New York Times said that it "looks like the best bet for family audiences in a season short on kiddie-oriented entertainment. And its a movie that no parents in their right minds should let children see."  The Chicago Tribune stated that " ith its contrived plot, its MTV-inspired montages and its blatant shilling for products, it is film as hard sell, and it comes with a decidedly suspect warranty. Its mercantile instincts are so primary it looks like an infomercial."  It currently holds a 13% approval rating on Rotten Tomatoes based on 8 reviews.

===Box Office=== The Getaway with $5.4 million in its opening weekend.  In total, the film went on to gross $30.5 million domestically in North America.

== References ==
 

== External links ==
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 
 