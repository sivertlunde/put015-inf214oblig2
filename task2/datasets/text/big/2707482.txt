Die Supernasen
{{Infobox film
| name           = Die Supernasen
| image          = 
| image_size     = 
| caption        = 
| director       = Dieter Pröttel
| producer       = Karl Spiehs Executive producer Otto Retzer Erich Tomek
| writer         = Thomas Gottschalk Mike Krüger
| narrator       = 
| starring       = Mike Krüger Thomas Gottschalk
| music          = Gerhard Heinz
| cinematography = Fritz Baader Otto Kirchhoff
| editing        = Eva Pavlikova Claudia Wutz
| distributor    = Astro Distribution
| released       = 8 September 1983
| runtime        = 88 minutes
| country        = Germany
| language       = German
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 West German comedy film featuring Thomas Gottschalk and Mike Krüger. The name of the movie, "The Supernoses", refers to the fact that both of them have big noses.

The two have worked together in several very popular movies. They were mostly college-movies in a German setting - two naughty young men in Bavaria, with lots of beautiful girls and funny complications. Their current popularity keeps their old films alive, like this film, Die Supernasen.

== External links ==
* http://www.supernasen-fans.de/ - German Fansite
*  

 
 
 
 
 
 
 
 


 
 