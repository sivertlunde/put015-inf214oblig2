Style (2004 film)
{{Infobox film
| name           = Style
| image          = Style04.jpg
| image_size     = 200px
| caption        =
| director       = Mite Ti
| producer       =
| writer         = Tar Yar Min Wai
| narrator       =
| starring       =
| music          =
| cinematography = Mg Thura  Mg Kyi Sein (A Twe A Yae)
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Myanmar Myanmar
| budget         =
}}
 2004 Burma|Burmese romantic comedy drama film directed by Mite Ti. 

==Plot==
Maung Nay Toe is a very privileged child, the only son of a wealthy merchant family residing in Taunggyi. As his parents were very disciplined, he grew up well-behaved, being polite in speech and manner and sheltered from some of the harsher realities of life. Maung Nay Toe eventually leaves home and arrives in  One day, Maung Nay Toe goes to town to record a song and becomes infatuated with a girl named Ma Wah Saw Nge, the daughter of a retired school principal and equally privileged but shy, and also a snob. She gives him an ultimatum that unless he gives up his  singing and returns to his wealthy family, their love life would come to an end.

Meanwhile, Maung Yin Maung, having met a girl name Khayt at his training course, also falls in love, but they eventually drift apart due to stark differences in their personality traits and attitude.

Maung Nay Toe and Maung Yin Maung, now both suffering knock backs in love, begin to feel depressed and briefly suffer from insomnia. When they eventually get some sleep they have dreams which reflect their desires - to become acceptable to their ex-girlfriends and rekindle their relationships. What they do not know is that the girls also regret their actions and eventually they became lovers again. 
 

==Cast==
 
 
*Lwin Moe
*Yar Zar Nay Win
*Soe Myat Nander
*Eaindra Kyaw Zin
* Zarganar
*May Thinzar Oo
*Tun Tun Win
*Mos
*Phoe Phyu
*King Kong
 
*Hnge Pyaw Gyaw
*Nyaun Nyaun
*Htet Htet
*Pyay Ti Oo
*Pwin
*Nwe Nwe San
*Khin Than Nwe
*Hla Myo Thinzar Nwe
*Khin Lay Nwe
 

==Release== Bago and San Pya in Myeik, Burma|Myeik, Tanintharyi Division.

==References==
 

 
 
 
 
 

 