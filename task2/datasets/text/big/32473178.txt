Finding Kind
 

{{Infobox film
| name           = Finding Kind
| image          =
| caption        =
| director       = Lauren Parsekian
| producer       = Lauren Parsekian Molly Stroud
| writer         =
| starring       = Lauren Parsekian Tetia Stroud
| music          = Andrew Skrabutenas
| cinematography = Christopher Hamilton
| editing        = Vegard H. Sorby
| studio         =
| distributor    = IndieFlix
| released       =   
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Finding Kind is a 2011 American documentary film directed by Lauren Parsekian.  It follows two friends traveling across America exploring the topic of how women can be mean to other women.

In May 2011, filmmakers Lauren Parsekian and Molly Stroud went on a tour of the U.S., showing the documentary to school students and encouraging them to fill out apology cards for someone theyve bullied or to write descriptions of how they themselves have experienced bullying. 

==Cast==
* Debra Parsekian ... Herself
* Lauren Parsekian ... Herself
* Molly Stroud ... Herself
* Tetia Stroud ... Herself

==References==
 

==External links==
*  
*   

 
 

 