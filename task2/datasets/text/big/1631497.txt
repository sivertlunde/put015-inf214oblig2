Hard Target
 
 
{{Infobox film
| name           = Hard Target
| image          = HardTarget 1993 poster.jpg
| image_size     = 215px
| alt            = Film poster with a gradient background fading from black to blue. In the middle is the head of an arrow with the character Chances reflection in it. At the top of the poster is the name "Van Damme" in capital letters. At the bottom left corner is the films title, production staff and cast and catch slogan stating "Dont Hurt What You Cant Kill".
| caption        = Theatrical release poster
| director       = John Woo
| producer       = James Jacks Sean Daniel Daryl Kass Sam Raimi
| writer         = Chuck Pfarrer
| starring       = Jean-Claude Van Damme Lance Henriksen Yancy Butler Arnold Vosloo Wilford Brimley
| music          = Graeme Revell
| cinematography = Russell Carpenter
| editing        = Bob Murawski
| studio         = Alphaville Films Renaissance Pictures Universal Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = $19,500,000 Elder, 2005. p.95 
| gross          = $74,189,677   	 
}} Chinese film 1932 film adaptation of Richard Connells 1924 short story, The Most Dangerous Game.
 Universal Pictures was nervous on having Woo direct a feature, and sent in director Sam Raimi to look over the films production and to take Woos place as director if he were to fail. Woo went through several scripts finding mostly martial arts films with which he was not interested. After deciding on Chuck Pfarrers script for Hard Target, Woo wanted to have actor Kurt Russell in the lead role, but found Russell too busy with other projects. Woo then went with Universals initial choice of having Jean-Claude Van Damme star. Woo got along with Van Damme during filming and raised the amount of action in the film as he knew that Van Damme was up for it.
 R rating that Universal wanted. Woo made dozens of cuts to the film until the MPAA allowed it an R rating. On its initial release, Hard Target was a financial success but received mixed reviews from film critics. Critics found Hard Target to have good action scenes but noted the weak script and poor quality acting from Jean-Claude Van Damme.

==Plot== New Orleans, a homeless veteran named Douglas Binder is the target of a hunt. He is given a belt containing $10,000 and told that he must reach the other side of town where he would then win the money and his life. Hunting him are the hunt organizer Emil Fouchon, a businessman named Mr. Lopaki who has paid $500,000 for the opportunity to hunt a human, Fouchons lieutenant Pik Van Cleaf, and mercenaries including Stephan and Peterson. Binder fails to reach his destination and is shot by three crossbow bolts. Van Cleaf retrieves the money belt.
 merchant seaman union dues are in arrears he reluctantly allows Natasha to hire him as her guide and bodyguard during her search. Meanwhile, Chances homeless friend Elijah Roper is the next to participate in Fouchons hunt, and is also killed.

Natasha discovers that her father distributed fliers for a seedy recruiter named Randal who has been secretly supplying Fouchon with homeless men with war experience and no family ties. Natasha questions Randal about her fathers death, but they are discovered by an eavesdropping Van Cleaf. Fouchon and Van Cleaf beat Randal to punish him for sending them a man with an interested family. New Orleans detective Mitchell is reluctant to investigate Binders disappearance until his charred body is discovered in the ashes of a derelict building. The death is ruled accidental, but Chance searches the ruins and finds Binders dog tag, which was pierced by one of the crossbow bolts. Van Cleafs thugs suddenly ambush Chance and beat him unconscious to scare him and Natasha out of town. When he recovers, he offers Mitchell the dog tag as evidence that Binder was murdered. With the investigation getting closer, Van Cleaf and Fouchon decide to relocate their hunting business and begin eliminating "loose ends". The medical examiner who had been hiding evidence of the hunt and Randal are both executed. Mitchell, Natasha and Chance arrive moments later at Randalls office and are ambushed by Van Cleaf and several of his men. During the shootout Mitchell is shot in the chest and killed. Chance kills a handful of the mercenaries and escapes with Natasha. Fouchon and Van Cleaf assemble their mercenary team and five paid-for hunters to continue the chase.

Chance leads Natasha to his uncle Douvees house deep in the bayou, and enlists his help in defeating the men. Chance, Natasha, and Douvee lead the hunting party to a warehouse of old Mardi Gras float (parade)|floats, and kill Fouchons men one by one. Van Cleaf is finally shot to death by Chance in a shoot out. In the end only Fouchon is left, but he holds Chance at bay by taking Natasha hostage and stabbing Douvee in the chest with an arrow. Chance charges him, attacking with a flurry of blows, and then drops a grenade in his pants. Fouchon attempts to dismantle the grenade, but fails and dies in the explosion. Chance, Natasha, and Douvee now make their way out of the warehouse.

==Cast==
  received a Saturn Award for Best Supporting Actor for his portrayal of Emil Fouchon in Hard Target. ]] Force Recon Marine. After Boudreaux saves Natasha Binder, he is hired by her to help search for her missing father.
* Lance Henriksen as Emil Fouchon, a wealthy sportsman who hunts homeless former soldiers for sport. After finding that he is being investigated by Chance and Natasha, Fouchon sends out his gang led by Pik Van Cleaf to ambush them.
* Arnold Vosloo as Pik Van Cleaf, a collaborator of Fouchon who takes part in his sport of hunting men. He leads the crew of men who are sent out to murder Chance and Natasha. Van Cleafs surname is a reference to actor Lee Van Cleef. Sharrett, 1999. p.408 
* Yancy Butler as Natasha "Nat" Binder, a young woman who comes to New Orleans to search for her father, whom she has not seen since she was seven years old. When Natasha is attacked by thugs, she is saved by Chance Boudreaux who agrees to help her find her father.
* Kasi Lemmons as May Mitchell, a police detective who works in the office while the police are on strike. Mitchell helps Natasha by ordering another autopsy when they show her the pierced dog tags that her father had.
* Chuck Pfarrer as Douglas Binder, Natashas father who has moved to New Orleans. After Natasha finds that three weeks have passed since she has heard from her father, she goes to New Orleans to find that he has been homeless and has been murdered by Emil Fouchons crew.
* Willie C. Carpenter as Elijah Roper, Chance Boudreauxs friend who is also homeless.
* Wilford Brimley as Uncle Douvee, Chance Boudreauxs uncle who lives deep in the Bayou. Chance and Natasha take shelter at his home as well as have him help during the films final shoot out.
* Sven-Ole Thorsen and Jules Sylvester as Stephan and Peterson, Fouchons mercenaries.
* Eliott Keener as Randal Poe
* Robert Apisa as Mr. Lopacki
* Douglas Forsythe Rye and Mike Leinert as Frick and Frack
* Marco St. John as Dr. Morton
* Joe Warfield as Zenan

==Production==

===Development=== Universal Pictures The Killer. English on a large scale project. They hired American director Sam Raimi to oversee the films production and to have him on standby if Woo was not able to fulfill his role as a director. Heard, 1999. p.125  Raimi was very excited to work with Woo as he was fan of his Hong Kong films. Raimi was also confident in Woos directorial skills, stating that "Woo at 70% is still going to blow away most American action directors working at 100%." 
 science fiction Andrew Davis was interested in the script, but ultimately turned it down. Hall, 1999. p.166  Woo read Pfarrers script for Hard Target appreciating that it was a "simple but powerful story, with a lot of feeling underneath. For a good action film you need a solid structure. Chuck gave me that".  Woo also stated that the story is "less John Woo" but the visual aspect would be "very John Woo". Elder, 2005. p.111  Pfarrer wrote the script originally basing it on the film The Naked Prey. After the script did not turn out Pfarrer worked on a script influenced by the film Aliens (film)|Aliens that became the basis for his comic Virus (comics)|Virus. The final attempt was a script based on The Most Dangerous Game. Pfarrer had the story take place in New Orleans to give an explanation of Jean-Claude Van Dammes accent. 

===Pre-production=== Drop Zone  and Fast Money.  Actor Lance Henriksen accepted the role of Emil Fouchon stating he was great fan of Woo, noting that his earlier films "were so creative, so balletic, and had this incredible philosophy in them. The violence was only a container for the philosophy". 

===Filming===
Hard Target had 74 days of production time and was shot on location in New Orleans, including sequences shot in the French Quarter.  Heard, 1999. p.122   Hard Target was put on a tight schedule by Universal that allowed only 65&nbsp;days of shooting time. This put a lot of pressure on Woo. Heard, 1999. p.124  
Woo was also pressured by Universal to tone-down the violence and body count that they had seen in his Hong Kong films.  As Woo had not mastered the English language yet, it took time for the cast and crew to get used to working with him. When Woo could not explain what he wanted with a shot to cinematographer Russell Carpenter, he would resort to simple statements such as "this will be the Sam Peckinpah shot" to get his message across to Carpenter.  Actor Lance Henriksen recalled that it was a gradual process that led everyone involved to start seeing the film as a John Woo film rather than a Jean-Claude Van Damme film.  Producer James Jacks recalled that Woo was not "the most powerful person on the set but as far as I was concerned, he was certainly the most respected". Heard, 1999. p.118 
 camera dollies were nicknamed by the crew as "the Woo-Woo Choo-Choo". Elder, 2005. p.103  Russell Carpenter found difficulty in filming the huge gunfight scenes. Carpenter specifically noted the Mardi Gras parade warehouse by recollecting that "just the lighting for a space like that, with all those strange shapes and shadows was difficult enough, but John then added the further complication of wanting the scene shot from several angles at once—often with more than one of the cameras moving".  Producer James Jacks supported this style of filming finding it the most economical way to shoot these types of action scenes. Heard, 1999. p.123 

===Post-production=== Kodo drummers R rating by Universal Pictures. When submitting the film to the Motion Picture Association of America (MPAA), it was judged as too violent and intense for an R rating and received an NC-17 rating.  Woo re-edited the film six times for the MPAA as they never indicated what specific scenes they found objectionable. Heard, 1999. p.128  During this editing period, Van Damme went with his own editor to make his own edit of the film. Van Dammes version excises whole characters to insert more scenes and close-ups of his character Chance. Heard, 1999. p.127  When asked about this edit, Van Damme replied that "People pay their money to see me, not to see Lance Henriksen".  The MPAA accepted the film after Woo had made 20 cuts to the film. Scenes cut include the opening chase sequence and the Mardi Gras warehouse sequence.  Heard, 1999. p.129  A non-action scene that is cut from the film is a romantic scene between Chance and Natasha. Heard, 1999. p.130 

==Release==

===Theatrical run===
Hard Target was tentatively scheduled to open in July 1993. Elder, 2005. p.109  Hard Target was released August 20, 1993 in the United States making it the first film by an Asian director to be released by a Hollywood studio.       

Hard Target did well in the box office, being the second highest grossing film release of the week at the American box office on its initial release. Hard Target also became the 49th highest grossing film in the United States in 1993.     Hard Target made domestic ticket sales of $32,589,677 (worldwide sales were $74,189,677). 

===Home media===
Hard Target was released on Laserdisc and VHS in 1994. In the United States, the film was the 14th highest selling laserdisc and the 46th most rented VHS film of 1994. {{cite journal
| date         =  January 7, 1995 |  title       = Top Laserdisc Sales Billboard
| volume      = 107
| issue       = 1
| pages       = 60, 65
| publisher   = Nielsen Business Media, Inc.
| issn        = 0006-2510
| url         = http://books.google.com/?id=ugsEAAAAMBAJ&pg=PA60#v=onepage&q=
| accessdate  = September 21, 2009
}} Street Fighter, Sudden Death The Quest.  

A longer 116-minute copy of the film has not been released officially, but has been found as a bootleg. This copy is a poor-quality videocassette dub and has a burned-in time code in the corner indicating that the film was not meant for public viewing. Heard, 1999. p.131  However, the European and Australian DVD releases restore the violent footage missing from the Region 1 DVD (that was cut for an R rating), making them the versions closest to Woos original cut. 

==Reception==

===Critical reception===
{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#FFFFE0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" |"Swishing, whooshing, lovingly photographed weaponry. Nasty gurgles from the wounded. The clonking sound of a head hitting wrought iron. A bullet in the eye. These are the cornerstones of "Hard Target," one of this summers few super-bloodthirsty action films, and the one that actually stood a chance of rising above its genre."
|-
| style="text-align: left;" |—Janet Maslin, writing for The New York Times 
|} Siskel & The Killer" and "Hard Boiled|Hard-Boiled." Van Damme and the directors reputation should ensure initial commercial kick on the way to solid if not spectacular box office".    Desson Thompson of The Washington Post wrote that "When Van Damme isnt duking it out with the English language, scriptwriter Chuck Pfarrer is filling Henriksens mouth with villainous pseudo-profundities. Even in a second-rate action picture like this, and despite Henriksens commendable efforts, theyre painful to listen to&nbsp;... Woos creative presence is practically stifled. There are some flashes of his deliriously wild style—a slow-motion moment here, a well-chosen freeze-frame there. He also introduces American audiences to his taste for unique motorcycle stunts and very, very loud car explosions. But these Wooisms are disappointingly minimal".    Lance Henriksen received a Saturn Award for Best Supporting Actor for his portrayal of Emil Fouchon in the film.    In 1997, Woo looked back on Hard Target stating that it was "in some ways, quite a troublesome movie to make, but Im rather happy with the way the action scenes turned out". {{cite journal
| author      = Nilsson, Thomas
|date=March 1997
| title       = Q&A with John Woo: Violence is Golden Black Belt
| volume      = 35
| issue       = 3
| pages       = 140
| issn        = 0277-3066
| url         = http://books.google.com/?id=CtoDAAAAMBAJ
| accessdate  = 27 July 2009
}} 

===Box office=== The Fugitive, came in first place during that weekend grossing $18,148,331.    Hard Target   s revenue dropped by 50% in its second week of release, earning $5,027,485. For that weekend, the film fell to third place, even with an increased screening count of 1,999 theaters. The Fugitive, remained unchallenged in first place grossing $14,502,865 in box office revenue.    During its final week in release, Hard Target opened in a distant eleventh place with $1,270,945 in revenue. For that particular weekend, Striking Distance starring Bruce Willis made its debut, opening in first place with $8,705,808 in revenue.  The film went on to top out domestically at $32,589,677 in total ticket sales through a 5-week theatrical run. Internationally, the film took in an additional $41,600,000 in box office business for a combined worldwide total of $74,189,677.  For 1993 as a whole, the film worldwide would cumulatively rank at a box office performance position of 23. 

==Notes==
 

===References===
* {{Cite book
 | last= Hall
 | first= Kenneth E.
 | title= John Woo: The Films McFarland
 |year= 1999
 |isbn= 0-7864-0619-4
}}
* {{cite book
| last      = Heard
| first     = Christopher
| title     = Ten Thousand Bullets: The Cinematic Journey of John Woo
| publisher = Doubleday Canada
| year      = 1999
| isbn      = 0-385-25731-7
| url       = http://books.google.com/?id=PhoKAAAACAAJ
}}
* {{cite book
| last      = Elder
| first     = Robert K.
| title     = John Woo: Interviews
| publisher = University Press of Mississippi
| year      = 2005
| isbn      = 1-57806-776-6
| url       = http://books.google.com/?id=eAKd70UbGoMC
}}
* {{cite book
| last      = Sharrett
| first     = Christopher
| title     = Mythologies of violence in postmodern media
| publisher = Wayne State University Press
| year      = 1999
| isbn      = 0-8143-2742-7
| url       = http://books.google.com/?id=QbLo8jYT4UQC
}}

==See also==
 
* American films of 1993
* List of action films of the 1990s

==External links==
 

*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 