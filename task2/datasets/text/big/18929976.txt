My Mom's New Boyfriend
{{Infobox film
| name           = My Moms New Boyfriend
| image          = Mymomsnewposter.jpg
| alt            =  
| caption        = DVD poster
| director       = George Gallo
| producer       = Avi Lerner Julie Lott Gallo Richard Salvatore
| writer         = George Gallo Trevor Morgan
| music          = Chris Boardman
| cinematography = Michael Negrin
| editing        = Augie Hess
| studio         = Millennium Films
| distributor    = Millennium Films
| released       =  
| runtime        = 97 minutes
| country        = Germany United States
| language       = English
| budget         = $25 million
| gross          = 
}}

My Moms New Boyfriend is a 2008 romantic comedy film starring Colin Hanks, Antonio Banderas, Selma Blair, and Meg Ryan.   
 limited theatrical release worldwide. However it was released straight-to-DVD in the United States on June 17, 2008. It was released under the title My Spy in the UK and Australia.

==Synopsis==
The film begins with Tommy Lucero (Antonio Banderas) being caught by French police following a foiled robbery at a museum. Tommy seems confident he will not remain in police custody very long.

The film then shifts its focus to Henry Durand (Colin Hanks). Henry had to care for his mother, Marty (Meg Ryan), when his father died in jail. Now a grown man and an FBI agent, Henry leaves his overweight mother to go work on a case.

Upon returning three years later, he finds his mother has lost a considerable amount of weight and is dating several men, one half her age, having turned over a new leaf after a passerby dropped a coin in her coffee cup, mistaking her for a homeless person. Henry announces to his mother that he has recently become engaged to another FBI agent, Emily (Selma Blair). Henry finds himself in a very uneasy state with his newly transformed mother and begins to be extremely protective about her. He confides his worries in his fiancée who does not find anything unusual in Martys behavior.

While taking a walk with Henry and Emily, Marty is hit in the head by a toy helicopter being flown by Tommy. Tommy asks to take the trio to dinner at an old Albanian restaurant as an apology. Henry, who is still protective of his mother, grudgingly concedes to go when he notices how much his mother and Emily want to.

Romance sparks between Marty and Tommy. Henry, meanwhile, is informed by his FBI superiors that they expect Tommy and two accomplices are going to attempt to steal a sculpture currently on display at a local museum. Along with his fellow agents, Henry spies on his mother around the clock, albeit reluctantly, after the FBI chief implied that he may be shipped to Alaska if he doesnt cooperate. Many uncomfortable situations arise for Henry as he has to listen into the conversations his mother has with Tommy.

While stealing the sculpture, Tommy is betrayed by his gang members and shot twice in the chest. He survives due to wearing a ballistic vest, but is later caught by Henry and Emily. Tommy reveals to them that his real name is Tomas Martinez and he is with the CIA. He has been working undercover trying to apprehend a gang of thieves who are stealing art to fund terrorism. He is concerned the gang will attempt to kill Marty as they know he has been romantically involved with her and she may know about their operation.

The trio reaches the Durand house in time to rescue Marty from the gang. The film ends with the gang members being taken into custody while Tommy kisses Marty and Henry kisses Emily.

==Cast==
* Antonio Banderas as Tommy Lucero / Tomas Martinez
* Meg Ryan as Martha "Marty" Durand
* Colin Hanks as Henry Durand
* Selma Blair as Emily Lott Trevor Morgan as Eddie
* Keith David as FBI Chief Conrad
* John Valdeterro as Agent Fedler
* Eli Danker as Jean-Yves Tatao
* Tom Adams as Niko Evangelatos
* Enrico Colantoni as Enrico the Chief
* Marco St. John as Inspector Laborde
* Aki Avni as Agent Randle
* Mark Meade as Special Agent Wagner

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 