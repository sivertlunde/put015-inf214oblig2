Devil Fetus
 
 
{{Infobox film
| name           = Devil Fetus
| image          = Devil-fetus-poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Lau Hung-chuen
| producer       = Lo Wei
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Shirley Lu Lau Dan Eddie Chan
| music          = 
| cinematography = Lau Hung-chuen 
| editing        = 
| studio         = Lo Wei Motion Picture Co.
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$ 3,920,445
}} Hong Kong film directed by Lau Hung Chuen.

==Production== The Thing. 

==Style==
John Charles, author of The Hong Kong Filmography 1977–1997 referred to the film as part of Hong Kongs "early 80s gross-out cycle" of horror films.   

==Release==
Devil Fetus premiered on 7 September 1983 in Hong Kong. It grossed a total of HK$ 3,920,445.  Part of the films more gruesome moments were cut by Hong Kong censors. 

==Reception==
Author John Charles referred to the film as "silly but amusing" and stated that the special effects in the film "are too ambitious for the budget". 

==Notes==
 

==External links==
*  
*  

 
 
 
 


 
 