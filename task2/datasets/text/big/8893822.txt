The Prodigal Planet
{{Infobox film
| name           = The Prodigal Planet
| image          = 
| alt            =  
| caption        = 
| director       = Donald W. Thompson
| producer       = {{Plainlist|
*Peg Courter
*Russell S. Doughten Jr.}}
| writer         = {{Plainlist|
*Russell S. Doughten Jr
*Donald W. Thompson
*William Wellman Jr}}
| starring       = {{Plainlist|
*William Wellman Jr
*Lynda Beatie
*Terri Lynn Hall
*Thom Rachford}}
| music          = Bob Jenkins
| cinematography = James L. Berry
| editing        = Wes Phillippi
| studio         = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Prodigal Planet is a 1983 film by Russell S. Doughten and directed by Donald W. Thompson.  It is the fourth in a series of at least five films (one still in production) based on an evangelical interpretation of Bible prophecy and the rise of the Antichrist.  Unlike the other films in the series that were filmed in Iowa, this one was filmed in New Mexico. 

==Plot== nuclear war mutants as a result of the nuclear exchanges. After rescuing one of them, Jimmy, David leads him to Christ. Jimmy later bravely dies to save the others from Jerry, and leaves Jerry their temporary captive.

Connie Wright (played by Terri Lynn Hall) is a government agent pretending to be a Christian. She rescues David from his internment at UNITE, then tries to get David to reveal the believers secret hideout. Along the way, they rescue Linda and her daughter, Jody.  Linda is a scientific researcher, brilliant, but terrified. Jody is a spoiled brat who, after being told off by Jimmy, begins to change.  She even starts to slowly accept Davids preaching. The same cannot be said of Linda, who is too rational a scientist to accept Davids faith. But Linda is actually evidence of divine providence, because her scientific specialty is radiation. So as they travel through the war-ravaged nation, Lindas knowledge keeps them alive and provides crucial guidance. She feels guilty, though, because she was part of the team that helped create the mutant doomsday people.

David suspects Linda of being a spy, since it seems that government agents always know where they are. But Linda is the only trustworthy one: Jody is discovered to have been transmitting their position inadvertently, and Connie did so deliberately. Connie is later picked up by a senior UNITE officer (dubbed "General Goon" by David in Image of the Beast). They are soon killed as their van goes out of control and runs into a train. At the end of the movie, Jody accepts Christ as her Savior, while Linda still thinks about the matter, or at least she does not yet openly receive Christ on camera. Meanwhile, a badly wounded and sobbing Jerry is shown in the ruins of the UNITE military base, which is then destroyed by explosions, but not before he rips off his UNITE armband in disgust.

== Continuity error==

*The ending to the previous movie appears to be retconned as it showed David about to be placed on the guillotine to be executed. However at at the start of this movie, hes escorted down a hall and already rescued by Connie before he even gets outside.

==References==
 

== External links ==
*  

 

 
 
 