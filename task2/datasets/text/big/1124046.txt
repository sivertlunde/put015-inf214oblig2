21-87
 
 
{{Infobox film
| name           = 21-87
| image          = 
| alt            =  
| caption        = 
| director       = Arthur Lipsett Colin Low Tom Daly
| writer         = 
| screenplay     = 
| story          = 
| starring       = 
| music          = 
| cinematography = 
| editing        = Arthur Lipsett
| studio         = 
| distributor    = National Film Board of Canada
| released       =   
| runtime        = 9 minutes 33 seconds
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} abstract collage film created by Arthur Lipsett that lasts 9 minutes and 33 seconds.

The short film, produced by the National Film Board of Canada, is a collage of snippets from discarded footage found by Lipsett in the editing room of the National Film Board (where he was employed as an Animation|animator), combined with his own black and white 16mm footage which he shot on the streets of Montreal and New York City, among other locations.

==Influence==
21-87 has had a profound influence on director  ,   and the feature it inspired,  " itself is said to have been inspired by the short film;   and in  , Princess Leias prison cell on the Death Star is numbered 2187.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 

 
 