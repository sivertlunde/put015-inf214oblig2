The Eruption of Mount Pelee
 
{{Infobox film
| name           = The Eruption of Mount Pelee
| image          = Éruption volcanique à la Martinique Méliès.jpg
| image_size     = 250px
| alt            =
| caption        = A frame from the film
| film name      = 
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = 
| cinematography = 
| editing        = 
| studio         = Star Film Company
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = Silent
| budget         = 
}}

Éruption volcanique à la Martinique, released in the United States as The Eruption of Mount Pelee and in Britain as The Terrible Eruption of Mount Pelée and Destruction of St. Pierre, Martinique, is a 1902 French short silent film directed by  , which destroyed the town of Saint-Pierre, Martinique. 

==Summary==
Mount Pelée looms over the town of Saint-Pierre. Fire and smoke rises from the crater; then lava begins pouring down the sides of the mountain. The village is soon engulfed in smoke and flames.

==Production==
The film is one of the most frequently cited examples  of   and the most complex one of all, The Coronation of Edward VII.    Stylistically, the film is reminiscent of the dioramas popular in the 19th century, which offered simulated views of places and events that would otherwise be inaccessible to spectators.  Mélièss table-top miniature models recreate the eruption in a "storybook illustration" style highly indebted to Romanticism. 

Academic opinion is divided on the exact method Méliès used to create the eruption. The Méliès descendant and film scholar Jacques Malthête hypothesized that a type of   was used (as Méliès did four years later to create an eruption of  ; Mélièss granddaughter, Madeleine Malthête-Méliès, indicated that starch was poured down the model to simulate lava, and that pieces of paper and unseasoned wood were burned to create smoke; and the Méliès expert John Frazer suggested that the model was made of cardboard and paper and that "the eruption   created by a combination of flashing lights, powdered chalk, and cinders."   

According to the film historian Pierre Lephrohon, the poet Guillaume Apollinaire once asked Méliès himself how he made The Eruption of Mount Pelee. Méliès said simply: "By photographing cinders and chalk." Apollinaire remarked to a friend who was with them: "Monsieur and I have the same occupation, we enchant ordinary materials."   

==Release and other versions== lost until 2007, when a copy was found and restored by the Filmoteca de Catalunya. 

  notes that Zeccas version aims for academic realism in its style, creating an effect markedly different from Mélièss deliberately Romanticized portrayal. 

The   stayed at the Edison studio in Orange, New Jersey to recreate the eruption using a studio model. A dozen clips of Smiths real-life footage, and all three of Porters films simulating different stages of the eruption, were sold by the Edison Company in July 1902; the catalogue encouraged exhibitors to combine the real and faked films to "make a complete show in themselves."  According to the film historian Lewis Jacobs, the crew that created the Edison version found their own unique way to simulate the eruption: they exposed a barrel of beer to direct sunlight and waited for it to explode. 

==References==
 

==External links==
* 
* 
*  on YouTube

 

 
 
 
 
 
 
 
 