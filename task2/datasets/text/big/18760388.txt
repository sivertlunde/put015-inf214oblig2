The White Line
 White line}}
 
{{Infobox film
| name           = The White Line
| image          = Cuori senza frontiere gina lollobrigida luigi zampa 006 jpg iwck.jpg
| caption        = Film poster
| director       = Luigi Zampa
| producer       = Bianca Lattuada Carlo Ponti
| writer         = Clare Catalano Piero Tellini Stefano Terra
| starring       = Gina Lollobrigida
| music          = 	Carlo Rustichelli
| cinematography = Carlo Montuori
| editing        = Eraldo Da Roma
| distributor    =
| released       =  
| runtime        = 87 minutes
| country        = Italy
| language       = Italian
| budget         =
}}

The White Line ( ) is a 1950 Italian drama film directed by Luigi Zampa and starring Gina Lollobrigida.   

==Cast==
* Gina Lollobrigida - Donata Sebastian
* Raf Vallone - Domenico
* Erno Crisa - Stefano
* Cesco Baseggio - Giovanni Sebastian
* Enzo Staiola - Pasqualino Sebastian
* Ernesto Almirante - The Grandfather
* Gino Cavalieri - The Priest
* Fabio Neri - Gaspare
* Mario Sestan - Lampadina
* Antonio Catania - Acquasanta
* Giordano Cesini - Cacciavitte
* Callisto Cosulich - Soviet Officer
* Tullio Kezich - Yugoslav Lieutenant
* Piero Grego - US Army Sergeant
* Gianni Cavalieri - Pentecoste

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 