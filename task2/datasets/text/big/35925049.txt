Kumaré
{{Infobox film
| name           = Kumaré
| image          = Kumare promotional poster.jpg
| alt            = 
| caption        = 
| director       = Vikram Gandhi
| producer       = Bryan Carmel Brendan Colthurst
| writer         = 
| starring       = Vikram Gandhi Purva Bedi Kristen Calgaro
| music          = Alex Kliment
| cinematography = Kahlil Hudson
| editing        = Adam Barton Nathan Russell 	
| studio         = 
| distributor    = Kino Lorber
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $132,160 
}}
Kumaré is an American 2011 documentary film directed by Vikram Gandhi.
 Indian accent and growing out his hair and beard. In the film, Kumaré travels to Arizona to spread his made-up philosophy and gain sincere followers.  

Kumaré premiered at the 2011 South by Southwest Film Festival (SXSW), where it received the festivals Feature Film Audience Award for Best Documentary Feature.    Gandhi came up with the idea of a fictional guru while recording another documentary film about yogis and their followers. 

==Reception==
Kumaré received fair reviews upon release.  Many movie reviewers criticized Gandhis deception as immoral, but partially forgave Gandhi for realizing that the experiment had grown out of his control.  Many compared the character of Kumaré and the deception by Gandhi to Sacha Baron Cohens Borat character and film.  Those who liked the movie, like Stephen Holden of the New York Times and Roger Ebert of the Chicago Sun-Times, praised the films message of "finding the guru within."  

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 