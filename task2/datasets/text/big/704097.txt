Box-Office Bunny
{{Infobox film name        = Box Office Bunny image       = Box Office Bunny title card.png caption     = Title card director    = Darrell Van Citters producer    = Kathleen Helppie-Shipley writer      = Charles Carney starring    = Jeff Bergman music       = Hummie Mann distributor = Warner Bros. released    = January 3, 1990 country     = USA
}}

Box-Office Bunny, released in 1990, is a 4-minute  . This was Warner Brothers first Bugs Bunny theatrical release since 1964. Sandler (1998), pp. 21–22  It was issued to commemorate Bugs 50th anniversary. It is included as a special feature on the DVD for The Looney Looney Looney Bugs Bunny Movie.

Jeff Bergman voiced the three main characters, thus becoming the first person besides Mel Blanc, who had died a year before the cartoon was released, to have the honor of providing Bugs and Daffys voices.

The short was directed by Darrell Van Citters, who went on to direct the first two "Hare Jordan" Bugs Bunny/Michael Jordan commercials for Nike, Inc.|Nike.

==Background==

In the late 1980s, Warner Bros. Animation started producing new theatrical animated shorts, featuring the Looney Tunes characters. The Duxorcist (1987) and The Night of the Living Duck (1988) were well-received individually. Both were then incorporated to the compilation film Daffy Ducks Quackbusters (1988).  They marked a return to prominence for fictional character Daffy Duck.  They were followed by Box-Office Bunny, the first theatrical short featuring Bugs Bunny since 1964. 

According to director   (1990). The under-performance of the feature film at the box-office is thought to have negatively affected the fate of the short. 
 Richie Rich (1994), Carrotblanca and The Amazing Panda Adventure (1995), Superior Duck to Carpool (film)|Carpool (1996), and Pullet Surprise to Cats Dont Dance (1997).  Staffers involved in the production of several of these shorts reportedly suspected that the studio already knew that these feature films were "hard-to-market" films. From a marketing perspective, the shorts could then be used to attract additional viewers to the cinema. Sandler himself, however, suspected that Warner Bros. was simply not particularly interested in generating publicity for the animated shorts. 

== Plot == Elmer and Bugs dance to a techno song with the aid of sticky bubble gum.]]
 usher Elmer Fudd attempts to drive him away. Meanwhile, Daffy finds the admission fee of the multiplex to be too high for his tastes. He instead uses his library card to force open a door and sneak inside.    
 free rider ending card in their attempt to escape. 

==Cast==
*  , Daffy Duck, Elmer Fudd
*   in movie (uncredited)
* Tress MacNeille: Actress in movie (uncredited)

==Reception==
In his review of the film, Charles Solomon praised the film as "funny, fast-paced, brightly colored" and managing to capture the essence of the Bugs-Elmer-Daffy films by Chuck Jones without  directly copying them. He found fault, however, with the concept of ending the film "at just over five minutes". In result, all the action and gags have to be contained in too short a space. There is no real resolution. 

==Sources==
*  

==References==
 

==External links==
*  

 
{{succession box |
before= Spaced Out Bunny | Bugs Bunny Cartoons |
years= 1990 |
after= Carrotblanca|}}
 
 

 
 
 
 
 
 
 