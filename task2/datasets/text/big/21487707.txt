Toronto Stories
{{Infobox Film
| name           = Toronto Stories
| image          = 
| caption        =
| director       = Sook-Yin Lee Sudz Sutherland David Weaver Aaron Woodley
| producer       = Daniel Bekerman Suzanne Cheriton Jennifer Jonas David Weaver Tony Wosk
| writer         = Sook-Yin Lee Sudz Sutherland David Weaver Aaron Woodley
| starring       = Carly Pope Gil Bellows Sook-Yin Lee K. C. Collins Lisa Ray Ricardo Hoyos Ellora Patnaik
| music          = E.C. Woodley
| cinematography = Sammy Inayeh
| editing        = Kathy Weinkauf
| studio         = New Real Films
| distributor    = Christal Films
| released       =  
| runtime        = 89 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}

Toronto Stories is a film in four segments bound together by a young boy, lost in an unknown city. After the prologue, the four segments are directed by different people: "Shoelaces" by Aaron Woodley, "The Brazilian" by Sook-Yin Lee, "Windows" by David "Sudz" Sutherland, and "Lost Boys" by David Weaver.

==Synopsis== Pearson International and the many diverse faces that populate the city’s landscape, a young boy, presumably of African descent, arrives at a customs desk unaccompanied and with no papers. He is taken into the custody of the immigration office, but when a back is turned he is drawn by curiosity into the throng of the airport. He then makes his way onto an express bus and into the city alone. An amber alert is issued signaling that a child has gone missing.
That same day two children embark on a quest to find a reported monster living beneath the picturesque neighbourhood of Cabbagetown. Over the course of that day and into the night, they share a number of profound experiences involving love, death and their very first kiss. In Kensington Market a lonely woman and a young man who has never been in love come up against their fundamental differences in their search for understanding and connection. Alton and Doug reunite by chance on the streets of Toronto.  These former partners in crime have to reevaluate their toxic relationship while staring down the barrel of a policemans gun. A broken man fallen from grace and now inhabiting the streets and alleys around Union Station spots the lost boy, but when he approaches the authorities his mental illness causes the credibility of his discovery to be questioned. 

==Cast==
* Carly Pope ... Roshanna
* Gil Bellows ...  Henry
* Sook-Yin Lee ...  Willia
* K. C. Collins ...  Alton Morris
* Lisa Ray ...  Beth
* Ricardo Hoyos ...  Jacob
* Ingrid Hart ...  Chantal
* Samantha Weinstein ...  Cayle
* Tygh Runyan ...  Boris
* Toka Murphy ...  Boy
* Olivia Palenstein ...  Jacobs Mother
* Cameron Kennedy ...  Zach Frank Moore ...  Stevenson
* Mike McPhaden ...  Father
* Michael Rhoades ...  Inspector Kane
* Judy Sinclair ...  Shelagh
* Joris Jarsky ...  Doug Shannon
* Shauna MacDonald ...  Lowry
* Daniel Park ...  Asian Man
* Ellora Patnaik ...  Caroline
* Elva Mai Hoover ...  Dog Walker Lady
* Louise Naubert ...  Eastern European Lady
* Maxwell McCabe-Lokos ...  Eddi
* Richard Leacock ...  Police officer Bell
* Stephen R. Hart ...  Greely
* Carly Street ...  Immigration Officer
* Ajit Zacharias ...  Indian Man
* Julian Richings ...  Leather Jacket
* James Lafazanos ...  Night Manager
* Gene Mack ...  Perry

==Release==
Toronto Stories premiered at the Toronto International Film Festival (TIFF). It then went on to screen at the Possible Worlds Film Festival and the Kingston Canadian Film Festival. It was released in select theatres on December 12, 2008.

==Reception== Hogtown as In Between Days."
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 