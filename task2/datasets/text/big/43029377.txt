Margo the Mouse
{{Infobox television
| show_name       = Margo the Mouse
| image           =  
| caption         =
| runtime         = 10 minutes  
| creator         = Eugeniusz Kotowski
| company         = Studio Filmów Rysunkowych
| distributor     = Facets Video
| composer        = Alojzy Mol
| country         = Poland
| language        = No dialogue TVP Polonia
| first_aired     =   
| last_aired      =   
| num_seasons     = 1
| num_episodes    = 13
| list_episodes   = #Episodes
}}

Margo the Mouse ( ) is a Polish animated series created and directed by Eugeniusz Kotowski. The animated series was made between 1976 and 1983, and was produced by Otokar Balcy.

Margo the Mouse stars a little Mouse called Margo (Myszka in the original Polish version) who lives in a big tree with her neighbours, a squirrel and a mole. It aired as part of the container TV animated series TVP Polonia.

==Description==

It is the story and adventures of Margo, a little mouse who lives in a large tree made chalet. Small Margo has as neighbors Adelina, a squirrel who lives on the top floor; and Jurand, a dormouse that lives in the basement.

However, all is not happiness for Margo. The little mouse must deal with some enemies. One day, Adelina goes to visit his mother because she is sick and Margo must look after the house. Then, at night, a sullen owl settles in Adelinas house illegally. So Margo seek ways to take the owl out of the house of her friend.

On another occasion, Margo prevents a beggar rat from occupying her home without her permission. Like while hiking should avoid being eaten by a stork. Although Margo has good friends like Anna, a sparrow; or Christophe, a grasshopper who likes to play the violin.

With her friends, Margo lives many adventures.

==Distribution==

Telewizja Polska broadcast the animated series sporadically for seven years.  In the 80s, Margo the Mouse was sold for international distribution. In several European countries, the series was released direct to video, as in Spain and France where the series was titled La ratoncita Tina (The Little Mouse Tina) and Jeannette, la petite souris (Jeannette, the little mouse) respectively and was made feature film with a combination of all episodes.

In United Kingdom, Margo the Mouse was released by GM Distribution with English subtitles. In United States and Canada, Margo the Mouse was distributed by Facets Video with name The Adventures of Little Mouse although series is titled Margo the Mouse was aired on PBS in the United States. 
 Spanish and French edition. Moya Iyubov The Humpbacked Horse of soviet Soyuzmultfilm animation studio are part of the same collection.  

==Episodes==

{| class="wikitable"
|-
! Number !! Polish title !! English title
|-
| 1 || Myszka nad wodą || Margo over the Water
|-
| 2 || Myszka na wycieczce || Margo on a Trip
|-
| 3 || Wyprawa do lasu || Expedition to the Forest
|-
| 4 || Imieniny cioci || Adelinas Aunt
|-
| 5 || Myszka i mucha || Margo and the Fly
|-
| 6 || Myszka i samochód || Margo and the Car
|-
| 7 || Awantura z kretem || The Row of Mole
|-
| 8 || Koncert świerszcza || The Concert of Cricket
|-
| 9 || Myszka i bocian || The Mouse and the Stork
|-
| 10 || Myszka i kot || Margo and the Cat
|-
| 11 || Myszka i włóczęga || Margo and the Tramp
|-
| 12 || Myszka i sowa || Margo and Owl
|-
| 13 || Zimowa wycieczka || Winter trip
|-
|}

==References==
 

== External links ==
*   (In Polish)
*   (In English)
*   (In Polish)
*   at the Internet Movie Database
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 