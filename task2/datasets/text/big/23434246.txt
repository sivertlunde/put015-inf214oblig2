Underground (1928 film)
{{Infobox film
| name           = Underground
| image          =
| caption        =
| director       = Anthony Asquith
| producer       = Harry Bruce Woolfe
| writer         = Anthony Asquith
| starring       = Brian Aherne  Elissa Landi   Cyril McLaglen   Norah Baring
| music          = 
| cinematography = Stanley Rodwell
| editing        = 
| studio         = British Instructional Films
| distributor    = Pro Patria Films
| released       = 1928
| runtime        = 84 minutes 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} British silent silent drama film directed by Anthony Asquith and starring Brian Aherne, Elissa Landi, Cyril McLaglen, and Norah Baring. The film examines the lives of ordinary Londoners and the romance between them, set on and around the London Underground.

==Production== Elstree and on location in London including scenes shot at Lots Road Power Station in Chelsea, London|Chelsea.  The film was based on an original scenario written by Asquith. 

==Synopsis==
An electrician and a porter both fall in love with a shop girl they meet on the London Underground. 

==Restoration==
Underground was restored in 2009 under the auspices of the British Film Institute (BFI). In 2011, composer and well-known silent film accompanist Neil Brand wrote a completely new score for the film, which was then premiered by the BBC Symphony Orchestra at the Barbican Centre in London. 

==Cast==
* Brian Aherne as Bill 
* Elissa Landi as Nell 
* Cyril McLaglen as  Bert, Power station worker 
* Norah Baring as Kate, Seamstress

==References==
 

==Bibliography==
* Ryall, Tom. Anthony Asquith. Manchester University Press, 2005.

==External links==
* 
* 
*  
*  
*   (with video clip)

 

 
 
 
 
 
 
 
 
 
 
 


 