Chandragupta (film)
 
 
{{Infobox film
| name           = Chandragupta
| image          = 
| image_size     =
| caption        = 
| director       = A. R. Kardar
| producer       = East India Film Company
| writer         = 
| narrator       = Nazir Gul Hamid Sabita Devi Mazhar Khan Dhiraj Bhattacharya
| music          = K. C. Dey
| cinematography = 
| editing        = 
| studio         = East India Film Company 
| distributor    =
| released       = 1934
| runtime        = 
| country        = British India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1934 Hindi/Urdu Nazir as Chandragupta. The cast included  Gul Hamid, Sabita Devi, Mazhar Khan, Dhiraj Bhattacharya, Vasantrao Pehalwan.    

The story, a historical, involved the founder of the Mauryan Empire, Chandragupta Maurya, and his Machiavellian Brahmin advisor/Minister Chanakya. The film was a commercial success at the box-office and proclaimed Kardar as a "talented film-maker".    

==Cast== Nazir
* Gul Hamid
* Sabita Devi
* Mazhar Khan
* Dhiraj Bhattacharya
* Vasantrao Pehalwan

==Soundtrack==
The music director was K. C. Dey.    

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| "Basa Le Apne Man Mein Preet"
|-
| 2
| "Keh Raha Hai Aasman Yeh Sab Sama Kuchh Bhi Nahin"
|-
| 3
| "Lat Uljhi Suljha Ja Balam"
|-
| 4
| "Maadho Ne Bajai Kaisi Bansuri"
|-
| 5
| "Prem Ke Bas Mein Hai Sansar"
|- 6
| "Piya Ke Darshan Bin Mor Ab Nahin Pawat Sukh Nain"
|-
| 7
| "Teenon Lok Mein Chha Rahi Mahima Aprampar"
|}

==References==
 

==External links==
*  

 

 
 
 
 