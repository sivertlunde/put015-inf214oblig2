Singh is Bling
{{Infobox film
| name           = Singh is Bling
| image          = Singh-is-bling-posters.jpg
| caption        = First look poster
| director       = Prabhu Deva
| producer       = Ashvini Yardi
| starring       = Akshay Kumar Lara Dutta Amy Jackson Yo Yo Honey Singh
| studio         = Grazing Goat Pictures
| released       = 2nd October 2015
| country        = India
| language       = Hindi
| budget         = 
| gross          =
| occupation     = 
}}

Singh is Bliing is an upcoming Bollywood action comedy film directed by Prabhu Deva and produced by Grazing Goat Pictures. The film features Akshay Kumar, Yo Yo Honey Singh, Lara Dutta and Amy Jackson in lead roles. The film started filming on April 3 2015 in Patiala, Singh is Bliing is currently expected to release on 2 October 2015.   

==Cast==
* Akshay Kumar as Mr.Singh 
* Vivek Oberoi
* Amy Jackson
* Diljit Dosanjh
* Lara Dutta
* Sonu Sood
* Rati Agnihotri as Mr. Singh Mother
* Arfi Lamba
* Karamjit Anmol
* Yo Yo Honey Singh

==Production==

===Development===
In early 2014, it was announced that actor Akshay Kumar and director Prabhu Deva would be reuniting for another film after their previous blockbuster, Rowdy Rathore. Soon after, it was confirmed that the film is titled Singh Is Bliing and is to be produced by Grazing Goat Pictures. A tentative release date of 31 July 2015 was included, only to be later changed to 2 October 2015.   

===Casting===

 
After Akshay Kumar was signed on for the lead role, Kriti Sanon was signed on for the female lead. After training for the role, Sanon eventually quit the role and opted out for the film, and was then replaced by Amy Jackson.  In March 2015, it was revealed that Lara Dutta   is hoping to make a comeback to films with Singh Is Bling.    

==References==

 

== External links ==
*  

 
 
 
 


 