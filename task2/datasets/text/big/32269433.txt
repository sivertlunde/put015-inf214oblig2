Main Tera Dushman
{{Infobox film
| name = Main Tera Dushman
| image =
| writer =
| starring = Sunny Deol Jackie Shroff Jayapradha
| director = Vijay Reddy
| producer =
| music = Laxmikant-Pyarelal
| lyrics =
| released = 1989
| language = Hindi
}}

Main Tera Dushman is an Indian film directed by Vijay Reddy and released in 1989. The movie stars Sunny Deol, Jackie Shroff, Jayapradha, Sridevi. 

==Cast==
* Sunny Deol ... Gopal
* Jackie Shroff ... Kishan Srivastav
* Sridevi ... Jugni
* Jaya Prada ... Jaya 
* Kiran Kumar ... Inspector Kiran Kumar
* Kulbhushan Kharbanda ... Jwalaprasad (Jugnis dad)
* Anupam Kher ... Thakur Dayalu

==Plot==
Honest and diligent Forest Officer Kishan Srivastav (Jackie Shroff) and his wife Jaya (Jaya Pradha) come to the rural area of Ramgarh, and upset the criminal activities of Thakur Dayalu (Anupam Kher), a corrupt Police Inspector Kiran Kumar (Kiran Kumar) and their croonies, which include capture and death of wild life, so that Dayalu can decorate his palatial home. Kishan attempts to disrupt their criminal activities, he finds that Dayalu Singh has by-passed him and complained to his superiors, thereby getting him dismissed from employment and being framed and imprisoned for a murder. Apparently, this is not the only time that Dayalu has got away with murder. He had also killed Gopal (Sunny Deol) in the presence of his to-be bride, Jugni (Sridevi), making her lose her sanity. Dayalu frames Jugnis dad, Jwalaprasad (Kulbhushan Kharbanda) for the murder of Gopal. And with Jaya all by herself, Dayalu turn his lustful eyes on her

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
|  1
| "Ye Tera Haathi"
| Manhar Udhas, Kavita Krishnamurthy
|- 
| 2
| "Ae Babu Hum Aaye"
| Kavita Krishnamurthy
|- 
| 3
| "Jugni Aayee Dhulhan Banke"
| Kavita Krishnamurthy
|- 
| 4
| "Jinhe Chahiye Daulat Rabba"
| Shabbir Kumar, Anuradha Paudwal
|-
| 5
| "Saare Jahan Ke Samne"
| Shabbir Kumar, Alka Yagnik
|-
| 6
| "Baje Mera Bichhua"
| Udit Narayan, Anuradha Paudwal
|}

==External links==
*  

 
 
 
 

 