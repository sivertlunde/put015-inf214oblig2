Five Golden Hours
{{Infobox film
| name           = Five Golden Hours
| image          = Five Golden Hours poster.jpg
| image size     = 225px
| alt            = 
| caption        = theatrical release poster
| director       = Mario Zampi
| producer       = Mario Zampi Hans Wilhelm
| screenplay     = 
| story          = 
| based on       = 
| starring       = Ernie Kovacs Cyd Charisse George Sanders
| music          = Stanley Black
| cinematography = Christopher Challis
| editing        = Bill Lewthwaite
| studio         = 
| distributor    = Columbia Pictures
| released       =   
| runtime        = 90 minutes
| country        = Italy United Kingdom
| language       = English
| budget         = 
| gross          =
}}
 Hans Wilhelm, starring Ernie Kovacs, Cyd Charisse and George Sanders, and featuring Dennis Price and John Le Mesurier.

==Plot==
Aldo Bondi (Kovacs) is a professional pallbearer and mourner in Rome who lives well off the extravagant gifts given to him by the rich widows he comforts.  When he falls for the supposedly penniless Baroness Sandra (Charisse) &ndash; who is actually a rich "black widow" whose husbands all die &ndash; he concocts a Ponzi scheme to bilk three widows by taking money from them, telling them that he will invest it during the "five golden hours" between the closing of the stock exchange in Rome, and the opening of the New York Stock Exchange. However, the Baroness absconds with the cash, leaving Bondi in hock to the widows.  He attempts to kill them, but the scheme fails and he pretends to have gone insane.  In the sanatarium, his roommate is another debtor feigning madness, Mr. Bing (Sanders).

One of the three widows dies, leaving Bondi a fortune, which he can only have if he continues to be insane, otherwise the inheritance is to go to a monastery &ndash; so Bondi makes a deal with the brothers to split the money.  He returns to Rome, where Mr. Bing makes contact with Baroness Sandra and, for a fee, tells her that Bondi is now rich.  Sandra and Bondi get married, and soon he is her seventh dead husband.

==Cast==
  
* Ernie Kovacs as Aldo Bondi
* Cyd Charisse as Baroness Sandra
* George Sanders as Mr. Bing
* Kay Hammond as Martha
* Dennis Price as Raphael
* Clelia Matania as Rosalia
* John Le Mesurier as Doctor Alfieri  
* Finlay Currie as Father Superior
 
* Reginald Beckwith as Brother Geronimo
* Avice Landone as Beatrice
* Sydney Tafler as Alfredo Martin Benson as Enrico
* Bruno Barnabe as Cesare
* Ron Moody as Gabrielle
* Leonard Sachs as Mr. Morini
 

==Production==
Five Golden Hours was filmed in two versions, one for English-language release, and another, released as Cinque ore in contanti, for Italian consumption.  In the Italian version, some of the smaller roles were taken by Italian actors.  Location shooting for the film took place in Bolzano, Italy and the surrounding area. 

Kovacs cited the picture as his favorite among his own films.

The film was the last directed by Mario Zampi. 

==References==
Notes
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 