The Valiant (1962 film)
{{Infobox film
| name           = The Valiant
| image          = "The_Valiant"_(1962).jpg
| image_size     = 
| caption        = British quad poster
| director       = Roy Ward Baker
| producer       = Jon Penington
| writer         = Play LEquipage au complet Robert Mallet Adaptation: Giorgio Capitani Franca Caprino Robert Mallet Willis Hall Keith Waterhouse 
| narrator       = 
| starring       = John Mills Ettore Manni Roberto Risso
| music          =  Christopher Whelen
| cinematography = Wilkie Cooper
| editing        =  Lea Pardo John Pomeroy
| studio         = BHP Euro International Film  (EIA)
| distributor    = United Artists Corporation  (UK)
| released       = 4 January 1962
| runtime        = 100 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Robert Shaw, and Liam Redmond. It is based on the Italian manned torpedo attack which seriously damaged the two British battleships Valiant and Queen Elizabeth and the oil tank Sagona at the port of Alexandria in December 1941.
 Princess Marina, Duchess of Kent. 

==Plot==
Alexandria December, 1941. Two Italian frogmen are captured under suspicion of placing a mine under the HMS Valiant. They are brought onto the ship for questioning.

==Cast==
*Captain Robert Morgan - 	John Mills
*Luigi Durand de la Penne - 	Ettore Manni
*Emilio Bianchi - 	Roberto Risso Robert Shaw
*Surgeon Commander Reilly - 	Liam Redmond
*The Admiral - 	Laurence Naismith
*Commander Clark - 	Ralph Michael Colin Douglas
*Bedford - 	John Meillon
*Turnbull - 	Moray Watson
*Norris - 	Dinsdale Landen
*Reverend Ellis - 	Patrick Barr
*Medical Orderly - 	Charles Houston
*Agony Payne - 	Gordon Rollings
*One Of The Tea Drinkers - 	Brian Rawlinson
*Sailor Saying Manners - 	Angus Lennie
*Sailor Trevor Williams in  communications with DiverStratford Johns
*Sailors - 	Bryan Pringle and Brian Blessed

== See also == HMS Valiant
*Luigi Durand de la Penne
*Raid on Alexandria (1941)
*The Silent Enemy
My father Trevor Williams took an active role with regards to the captive Italians, what he told me was a call went out for anyone who could speak Italian for there was nobody on board to communicate with the captured Italians. The only one on board who could communicate was my father. He told the officer that he did not speak Italian or French but could write French, so whoever was in charge got my father to write a question using French and one of the Italians could understand what was being written and replied also writing  French and my father translated.  If anyone out there would like to contact me please do so. Posted by son Alan Williams
In fact, the Italians that were captured warned the commander but they refused to believe they were about to be defeated. They Italians were returned to the captured quarters and fortunately, survived the blast.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 