MTV Movie Awards Reloaded
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The MTV Movie Awards Reloaded
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Joel Gallen
| producer       = Joel Silver (uncredited)
| writer         = The Wachowski brothers (uncredited)
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Seann William Scott Justin Timberlake Keanu Reeves Wanda Sykes Will Ferrell Andy Dick Laurence Fishburne Randall Duk Kim Don Davis (uncredited)
| cinematography = 
| editing        = 
| studio         = Silver Pictures (uncredited) Tenth Planet Productions (uncredited)
| distributor    = Warner Bros. Pictures MTV
| released       =   
| runtime        = 10 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 2003 for the 2003 MTV Movie Awards. Half of the film is made by Tenth Planet Productions, and the other half is archival footage from The Matrix Reloaded, which was released in theaters at that time. The unedited version is on the DVD version of The Matrix Reloaded.

== Plot ==
The movie starts with Justin (Justin Timberlake) and Seann (Seann William Scott) taking two girls into their apartment. As they are about to go in, the key doesnt fit. And suddenly the Keymaker (Randall Duk Kim) shows up and realises they are the ones. He opens the door with the key around his neck. He opens the door and when Justin and Seann enter the door, they are transported to Zion, where (as in the film) a party is held at the temple. They learn from an extremely flamboyant dancer (Andy Dick) that Zion is the last human city and the robots are coming to kill them. They also learn that Morpheus is having a huge orgy at his place with machines. Justin refuses, but when Seann tries to stay hes pulled by his ear out. They come through a door and they see that Seann has a really kinky suit made of leather. Seann runs through a door marked Men and Justin goes through another door into a park. He sees Neo talking to the Oracle (Wanda Sykes). After a discussion about NSync and the robot, Stiffler comes. He sticks his hand into and transforms her into a clone of him. And just then an army of Stiffler clones comes out and fights Justin as a parody of the "Burly Brawl" scene.

In place of the "Burly Brawl" music during the fight scene with the clones, this parody uses the song "Rock Your Body". He finally defeats Stiffler by punching him and turning him back into Seann. They get into an argument and are transported to the Architect (The Matrix)|Architects TV Room. The Architect (Will Ferrell) addresses about the connection between the Matrix and the MTV Movie awards and how he has no idea what hes talking about. He also talks about how Neo was supposed to host the Awards but since it was a full-time job and he was a bit distracted (with Trinity). He gets into an argument with Neo and yells at him. He shows them two doors, one door leads to the MTV Awards and the other leads to Trinity. Justin and Seann leave through one door and when Neo is about to leave through the other, the Architect jumps on him and they both smash through the door.

== Cast ==
*Justin Timberlake - Himself
*Seann William Scott - Himself/Stiffler
*Andy Dick - Zion Dancer
*Randall Duk Kim - Keymaker
*Wanda Sykes - Oracle
*Will Ferrell - The Architect
*Keanu Reeves - Neo (Archive Footage)
*Laurence Fishburne - Morpheus (Archive Footage)
*Carrie-Anne Moss - Trinity (Archive Footage, On a TV Screen)

 
 

 
 
 
 
 
 