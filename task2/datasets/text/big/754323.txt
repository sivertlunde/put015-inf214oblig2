Josie and the Pussycats (film)
 
{{Infobox film
| name           = Josie and the Pussycats
| image          = josie-pussycats-2001-movie.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = {{Plainlist|
* Harry Elfont
* Deborah Kaplan}}
| producer       = {{Plainlist|
* Marc E. Platt Kenneth Edmonds}}
| writer         = {{Plainlist|
* Harry Elfont
* Deborah Kaplan}}
| based on       =  
| starring       = {{Plainlist|
* Rachael Leigh Cook
* Tara Reid
* Rosario Dawson
* Alan Cumming
* Parker Posey
* Gabriel Mann}}
| music          = John Frizzell
| cinematography = Matthew Libatique
| editing        = Peter Teschner
| studio         = {{Plainlist|
* Archie Comics Edmonds Entertainment Group Marc Platt Productions}}
| distributor    = {{Plainlist| Universal Pictures  
* Metro-Goldwyn-Mayer  }}
| released       =  
| runtime        = 98 minutes  
| country        = {{Plainlist|
* United States
* Canada}}
| language       = English
| budget         = $39 million   
| gross          = $14.9 million 
}} musical comedy Universal Pictures Archie Josie comic of the same name, as well as the Josie and the Pussycats (TV series)|Hanna-Barbera cartoon. The film stars Rachael Leigh Cook, Tara Reid, and Rosario Dawson as the Pussycats, with Alan Cumming, Parker Posey, and Gabriel Mann in supporting roles. The film received mixed reviews and was a box office bomb.

==Plot==
Wyatt Frame (Alan Cumming) is a record executive with record label MegaRecords. The label, headed by the trendy and scheming Fiona (Parker Posey), creates a series of trendy pop bands. Conspiring with the United States government, they add subliminal messages under the music to get teens to buy their records and follow "a new trend every week". The Governments motive is to help build a robust economy from the "wads of cash" teenagers supposedly earn from babysitting and minimum wage jobs. When a member of Wyatts wildly successful boy band, Du Jour, uncovers one such message and asks Wyatt about it aboard their private jet, Wyatt and the pilot (Harry Elfont) parachute out of the plane, leaving it to crash and kill the band members.

Wyatt lands just outside the town of  ), drummer Melody Valentine (Tara Reid), and bassist/backup vocalist Valerie Brown (Rosario Dawson). Because they are struggling financially, the Pussycats accept Wyatts lucrative record deal despite its implausibility. They are flown to New York City where they are renamed "Josie and the Pussycats", much to the girls discomfort. All goes well and their first single climbs rapidly to the top of the charts, but Valerie grows increasingly frustrated that all media attention is focused on Josie rather than the band as a whole. Melody, too simple to notice the undue attention Josie receives, uses her uncanny behavioral perception and becomes suspicious of Fiona and Wyatt.
 gig by mixing board to make the subliminal track audible, but she is caught by Fiona.

MegaRecords have organized a giant pay-per-view concert, whereby they plan to unleash their biggest subliminal message yet. They force Josie to perform solo on stage by holding Melody and Valerie hostage. The badly injured members of Du Jour—who survived by grounding their plane, but landed in the middle of a Metallica concert where they were severely beaten by Metallica fans—appear just in time to stop Wyatt and Fiona from launching the message. In the resulting fight, Josie destroys the machine used to generate the messages. The new subliminal message is revealed not to promote the band, the label, or a corporate sponsor, but to make Fiona universally popular. Fiona suffers a breakdown and reveals that she had been a social outcast in high school. Wyatt reveals that his appearance is a disguise—that he went to the same high school as Fiona, but was a persecuted and unpopular albino. Fiona and Wyatt immediately fall in love. The government agents colluding with Fiona arrive, but because the conspiracy is exposed, they arrest Fiona and Wyatt as scapegoats to cover-up the governments involvement in the failed scheme.

Josie, Valerie, and Melody perform the concert together, and for the first time their fans are able to judge the band on its merits, free of subliminal persuasion.  Alan M arrives and confesses his love for Josie on stage, and she returns his feelings. The audience roars their approval as the film comes to a close.

==Cast==
 
 
;The Pussycats
* Rachael Leigh Cook as Josie McCoy, the Pussycats red hair|red-headed lead vocalist, guitarist and bandleader
* Tara Reid as Melody Valentine, the bands sweet but absent-minded blonde drummer
* Rosario Dawson as Valerie Brown, the bands brainy brunette songwriter, bassist, and backup vocalist
* Gabriel Mann as Alan M, a folk guitarist and the "sexiest man in Riverdale", and Josies romantic interest manager
* twin sister, who is chasing Josies love interest, Alan M
 
;MegaRecords
* Alan Cumming as Wyatt Frame, a manipulative promoter who recruits and manages young bands for MegaRecords.  He is the manager for Du Jour and the Pussycats, and goes to great lengths to prevent exposure of the conspiracy
* Parker Posey as Fiona, the devious MegaRecords CEO who conceived the scheme to use subliminal messages to manipulate teens spending habits Tom Butler as Agent Kelly, the government agent who collaborates with Wyatt and Fiona in the scheme
 
;Du Jour
* Alex Martin as Les
* Donald Faison as DJ
* Seth Green as Travis
* Breckin Meyer as Marco
 
;Cameos
* Serena Altschul as herself
* Carson Daly as himself
* Aries Spears as the other Carson Daly
* Eugene Levy as himself
* Kenneth "Babyface" Edmonds as The Chief
* Justin Chatwin as Teenage fan
* Russ Leatherman as Moviefone|Mr. Moviefone
* Harry Elfont (director cameo) as Lex the pilot
 

==Production== Sonic the Hedgehog also appears in Archie Comics), Motorola, Starbucks, McDonalds, Gatorade, Snapple, Evian, Target Corporation|Target, Aquafina, America Online, Pizza Hut, Cartoon Network (which has aired the cartoon series on many occasions), Revlon, Kodak, Puma AG|Puma, Advil, List of Procter & Gamble brands#Other current brand details|Bounce, and more. None of the advertising was paid promotion by the represented brands; it was inserted voluntarily by the filmmakers. 

==Reception==
 
The film grossed $14,866,015 at the U.S. box office, less than its production budget, an estimated $39 million, resulting in a domestic box office bomb. 

 
The film received mixed reviews from critics. Based on the Hanna-Barbera series of the 70s, critics felt it (and other movies like it based on cartoons) did not work on screen. The film holds a 53% "Rotten" rating at Rotten Tomatoes, based on an average of 114 reviews, holding the consensus "This live-action update of Josie and the Pussycats offers up bubbly, fluffy fun, but the constant appearance of product placements seems rather hypocritical."  On Metacritic, the film scores a 47 out of 100, based on 29 critics, indicating "mixed or average reviews". 

Film critic Roger Ebert gave the film one-half of a star out of a possible four, commenting that "Josie and the Pussycats are not dumber than the Spice Girls, but theyre as dumb as the Spice Girls, which is dumb enough." 

Evaluating the film for the Onion A.V. Club in 2009, Nathan Rabin writes that it is "funny, sly and sweet and "a sly, sustained spoof of consumerism". He rates the film as a "secret success". 

==Home media==
When released on VHS and DVD on November 20, 2001, a "Family-Friendly" PG-rated version was released as well: this version omitted a great deal of the profanity and sexual references.

==Soundtrack==
  Sony Music Playtone Records on March 27, 2001, Music from the Motion Picture Josie and the Pussycats was well-received, certifying a gold album with 500,000 copies despite the films critical and commercial failure. Cooks singing voice was provided by Kay Hanley of the band Letters to Cleo.

# "3 Small Words" – Josie and the Pussycats (2:53)
# "Pretend to Be Nice" – Josie and the Pussycats (3:50)
# "Spin Around" – Josie and the Pussycats (3:17)
# "You Dont See Me" – Josie and the Pussycats (3:42)
# "Youre a Star" – Josie and the Pussycats (2:04)
# "Shapeshifter" – Josie and the Pussycats (3:01)
# "I Wish You Well" – Josie and the Pussycats (2:55)
# "Real Wild Child" – Josie and the Pussycats (1:52)
# "Come On" – Josie and the Pussycats (3:17)
# "Money (Thats What I Want)" – Josie and the Pussycats (2:28)
# "Du Jour Around the World" – Du Jour (2:56)
# "Backdoor Lover" – Du Jour (3:40)
# "Josie and the Pussycats Theme" – Josie and the Pussycats (1:43)

==See also==
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 