Burning Country
{{Infobox film
| name           = Burning Country
| image          = 
| image_size     = 
| caption        = 
| director       = Heinz Herald 
| producer       = 
| writer         = Maximiliane Ackers 
| narrator       = 
| starring       = Marie Wismar   Ernst Deutsch   Kurt Vespermann   Albert Steinrück
| music          = 
| editing        =
| cinematography = Willy Rothe
| studio         =  
| distributor    = 
| released       = 10 March 1921
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent adventure film directed by Heinz Herald and starring Marie Wismar, Ernst Deutsch and Kurt Vespermann. It premiered in Berlin on 10 March 1921. 

==Cast==
* Marie Wismar as Frau Valevski
* Ernst Deutsch as Vikar Benedikt 
* Kurt Vespermann as Karl 
* Albert Steinrück as General Braticzek 
* Maximiliane Ackers as Marie, Karls Braut 
* John LaGatta as Wladislaus 
* Lyda Salmonova as Mascha 
* Geo Bergal as Heinrich 
* Maxi Ackers   
* Albert Bassermann   
* Hugo Döblin   
* John Gottowt   
* Albrecht Viktor Blum

==References==
 

==Bibliography==
* Grange, William. Cultural Chronicle of the Weimar Republic. Scarecrow Press, 2008.

==External links==
* 

 
 
 
 
 
 


 
 