Five Across the Eyes (film)
{{Infobox film
| name           = Five Across the Eyes
| image          = 
| alt            = 
| caption        = 
| director       = Greg Swinson Ryan Thiessen
| producer       = Rick Stroud Greg Swinson Ryan Thiessen
| writer         = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Anchor Bay Entertainment
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Five Across the Eyes is a suspense/horror film directed by Greg Swinson and Ryan Thiessen and produced by Trauma One Entertainment. It was shot in 2005 in Morristown, Tennessee, and neighboring Greene County, Tennessee.

== Plot ==
On their way home from a football game, five high school girls get lost in an area known as "the eyes". An ensuing accident with a parked SUV in a store parking lot causes the students to panic and flee the scene. A vehicle with one headlight starts to follow them until the driver traps them on a deserted road. The driver thinking that they wrecked her marriage forces the five girls from their vehicle, forces them to strip and urinate on their clothing at gunpoint. After letting them go the driver continues to pursue them throughout the film and performs several violent and sexual acts on them. The girls also discover that in the back of the drivers truck there are dead bodies. They get their revenge however, when they stab the driver multiple times with a screwdriver and light her corpse on fire. They eventually return to the store to find that the driver killed everyone in the store and drove off. The movie ends with the girls drive off into the night, one of them puking and then another girl who is driving tells her not to mess up her mothers car.

== Cast ==
* Jennifer Barrnet as Stephanie
* Angela Brunda as Caroline
* Danielle Lilley as Jamie
* Sandra Paduch as Isabella
* Mia Yi as Melanie
* Veronica Garcia as The Driver

== Production == MiniDV camcorders.

== Film festivals ==
The film was chosen to be part of the "Fantastic Fest 2007". 

== Alternate titles ==
* Perdidas (Spain)
* Perdidas en la noche (Latin America)
* Perdidas na escuridão (Portugal)
* 5 Vite AllInferno (Italy)

== Reviews ==
* America JR   
* About.com  
* Pop Syndicate   
* Den of Geek   

== References ==
 

== External links ==
*  
*  
*  , Eat My Brains. May 2008 (Retrieved November 8, 2008)
*Lines, Craig  , Den of Geek. (Retrieved November 8, 2008)

 
 
 
 
 
 
 
 