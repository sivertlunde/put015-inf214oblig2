Kapus Kondyachi Goshta
{{Infobox film
| name           = Kapus Kondyachi Goshta
| image          = Kapus Kondyachi Goshta.jpg
| caption        = Kapus Kondyachi Gostha
| director       = Mrunalini Bhosale  
| producer       = Ravindra alias Nitin Bhosale
| production company = wenkateshwara films international  
| writer         = Prasad Namjoshi 
| starring       = Makarand Anaspure  Samidha Guru  Bharat Ganeshpure   Gauri Konge   Mohini Kulkarni   Netra Mali
| cinematography =   Wasim Maner
| editing        =  Santosh Gothoskar 
| released       = 
| runtime        = 111 minutes
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
 Marathi movies. It features  Makarand Anaspure,   Samidha Guru  and   Bharat Ganeshpure  in the lead roles. Supporting roles are played by Gauri Konge, Mohini Kulkarni and  Netra Mali . The film is produced by Ravindra alias Nitin Bhosale . The background score was composed by Shailesh Dani and the cinematography was handled by Wasim Maner.

The movie received a lot of awards and appreciations with the support of All Lights Film Services (ALFS),  a leading Film Festival Consultancy.

==Plot==

“Kapus Kondyachi Goshta” a story of triumph over tragedy, where the strong will of simple village girl wins over all odds to become an inspiration for many.

In this land of cotton growers, the heartrending farmers suicides are a continuing tragedy in Maharashtra. It is these grim circumstances that unfold the story of Joyti along with her three sisters as they stand against village head and ever-fickle villagers.

This is a true story that chronicles the victory over social ostracisation, poverty and hardship.

The film inspires the audience while giving them a deep insight into the lives and struggles of a women in heart of India

==Cast==
*Makarand Anaspure  As Lawyer
* Samidha Guru as Jyoti
* Bharat Ganeshpure as Dada Patil
* Gauri Konge as Ratna
* Mohini Kulkarni as Chanda
* Netra Mali as Bali

==Festival screenings==
The film was an official selection for the following film festivals:
* Dadasaheb Phalke Film Festival 2014 - International Competition section. 
* International Indian Film Festival of Queensland in Brisbane - International Competition section.
* Gwinnett Center International Film Festival,USA  - International Competition section.
* 9th Seattle South Asian Film Festival,  USA - International Competition section.
* Silk Road International Film Festival,China - International Competition section.

==Oscar Contention==

The movie is competing for Oscar     Race 2015.  And the movie has been screened at Laemmle Theatres, CA, Los Angeles.

The Academy of Motion Picture, Arts and Sciences unleashed the names of three hundred and twenty three movies which made its way to the final list of contention.  From that the movie KAPUS KONDYACHI GOSHTA got Contention in 87th Oscar for ‘Best Picture Category.

==Accolades==
The film has received the following accolades:
; Dada Saheb Phalke Film Festival
* 2014: Best Film - Women  Empowerment - Mrs.Mrunalini Bhosale  
; International Indian Film Festival of Queensland in Brisbane
* 2014: Best Feature Film - Kapus Kondyachi Goshta  
;Sahyadri Cine Awards 2014
* 2014: Best Feature Film - Kapus Kondyachi Goshta
;Maharashtra State Film Awards 2014
* 2014 : Best Actress Award - Samidha Guru

==References==
 

==External links==
* 
* 
* 

 