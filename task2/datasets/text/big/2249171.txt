Free Zone (film)
{{Infobox film 
| name     = Free Zone 
| image          = Freezoneamosgital.jpg 
| caption        = 
| alt            = 
| writer         = Amos Gitai  Marie-Jose Sanselme
| starring       = Natalie Portman  Hiam Abbass  Hanna Laslo 
| director       = Amos Gitai 
| cinematography = Laurent Brunet 
| producer       = Nicolas Blanc  Michael Tapuah  Laurent Truchot 
| distributor    = Bac Films 
| released       =  
| runtime        = 93 minutes
| country        = Israel
| language       = English Hebrew
| music          = 
| awards         = 
| budget         = $1,234,567   
}}
  with Hana Laszlo and Natalie Portman on the set of Free Zone, 2005]] Spanish production Palestinian Arab actress Hiam Abbass, and Israeli-American actress Natalie Portman. Border Trilogy.

The film made its debut at the 2005 Cannes Film Festival on May 19, 2005.  It was released in Israel on June 9, 2005, and then appeared at numerous other film festivals throughout the rest of the year, having a limited release on December 16, 2005 in the United States.

== Plot   == Saudi border Palestinian woman who serves as the contact for Hannas husbands black market activities. The three women set off on a tense journey to retrieve Hannas money.
 bookended by a rendition of the traditional Passover song "Had Gadia", performed by Chava Alberstein. The song "Ain Ani" by Shotei Hanevua also plays in the taxi at the end of the film.

==Cast==
* Natalie Portman as Rebecca. Partly Israeli, partly American, she left New York to live in Jerusalem but has no family of her own there.
* Hanna Laslo as Hanna. After being expelled from Sinai Peninsula|Sinai, she established with her husband in the Negev.
* Hiam Abbass as Leila. A Palestinian Arab, rejected by her own son for her modern mores.
* Carmen Maura as Mrs. Breitberg. The mother of Julio. In the final cut, she only appears in a superimposed flashback.
* Makram Khoury as Samir, the American. A Palestinian orphan who was a refugee in Texas. Now living as a car trader in a Free Zone oasis.
* Aki Avni as Julio. Rebeccas former fiancé of Spanish people|Spanish-Jewish origin. They separated after he told her that he had sex with a Palestinian refugee during a military operation.
*Liron Levo as military officer in the border with Jordan .

==Reception==
Free Zone received negative reviews from critics. Review aggregator Rotten Tomatoes reports that 26% of critics have given the film a positive review based on 46 reviews, with an average score of 5/10.   

==Awards== Best Actress award, and the film was nominated for the Palme dOr.    The president of the jury said afterward he considered honoring all three Free Zone actresses with an award for best ensemble acting.

==Controversy== Orthodox Jews who were praying there while the crew filmed a kissing scene between Natalie Portman and Israeli actor Aki Avni. The scene was not included in the final cut of the film.   

==References==
 

==External links==
*   official site (interview with director)
*  
*   on Free Zone site
*   on NataliePortman.com fansite
 
 
 
 
 
 
 
 
 