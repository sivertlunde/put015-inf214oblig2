We Married Margo
{{Infobox Film
| name= We Married Margo
| image=We Married Margo poster.jpg
| caption=We Married Margo promotional poster William Dozier William Dozier Tom Arnold Jillian Johns Sal Catalano Margaret Easley
| director=J. David Shapiro
| cinematography=Jeff Baustert Richard Bennett Shawn Clement Dan Schloss Glenn Schloss KOAN
| released=January 25, 2000 (Slamdance Film Festival)
| runtime=85 minutes
| country=United States
| language=English
| movie_series=
| awards=
| budget=
| gross=
| producer=Karen Shapiro Nancy Shapiro Pamela Shapiro Schloss Stephen Zakman
| followed_by=
}}
We Married Margo is a 2000 American  , January 25, 2000. Accessed August 12, 2008.  The film was awarded the Audience Award for Comedy Film of the Year at the 2000 The Comedy Festival (formerly known as the US Comedy Arts Festival) and was nominated for the Grand Jury Prize at the Slamdance Film Festival the same year.

== Plot == William Dozier), a California surfer. However, this marriage does not last long either and they are quickly divorced. 

Jake and Rock were previously introduced to each other by Margo and, after they are both divorced from her, they form a friendship and live as roommates. The two have very little in common except for the fact that they married and divorced the same woman. They become best friends, even though they constantly irritate each other, and they decide to write a film based on their experience of being married to Margo. During their work on the project, they meet many more people just like them whose lives were impacted by Margo.

== Cameos ==
The film featured   game Six Degrees of Kevin Bacon.

The following is a partial list of cameo appearances in the film:
* Kevin Bacon Tom Arnold
* Maurice Benard
* Dan Cortese
* Cindy Crawford
* Erik Estrada
* Julie Moran
* Rob Moran
* Mark OMeara
* Payne Stewart
* Victoria Tennant

==See also==
* 2000 in film
* Cinema of the United States
* List of American films of 2000

== References ==
 

== External links ==
*  
*  

 
 
 
 