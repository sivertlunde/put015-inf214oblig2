Zachariayude Garbhinikal
{{Infobox film
| name           = Zachariayude Garbhinikal
| image          = Zachariayude Garbhinikal.jpg
| alt            = 
| caption        = Promotional poster
| director       = Aneesh Anwar
| producer       = Sandra Thomas Thomas Joseph Pattathanam
| writer         = Aneesh Anwar Nizam
| starring       = {{Plainlist| Lal
* Rima Kallingal
* Sanusha Geetha
* Asha Sarath
* Sandra Thomas
* Aju Varghese}}
| music          = Vishnu Mohan Sithara Sharreth Background Score: Prashant Pillai
| cinematography = Vishnu Narayan
| editing        = Renjith touchriver
| studio         = Friday Cinema House
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Zachariayude Garbhinikal (English: Pregnant Ladies of Zacharia) is a  , Kerala.
 Conquest of Paradise". The film had a relatively new composer duo, Vishnu and Sharath, son and nephew of musician Mohan Sithara.

==Plot==
The film narrates the incidents in the life of a gynaecologist and five women who come into his life each with unconventional/illegitimate pregnancies. Lal acts as the gynaecologist and Asha Sharreth does the role of doctors wife. Sanusha plays the role of an eighteen-year-old girl who refuses to abort her child and reveal the childs father. In the end, she gives her baby to the gynaecologist and his wife who dont have children of their own. After some months, she visits the doctors house and leaves him a book. And from reading one of the story in the book that she specially marked he figures out that she bore the child of her father.  Rimas character is a nurse, while Geetha plays an elderly woman who gets pregnant through  artificial insemination.  Aju Varghese appears as Ajmal who has strong feelings for Fatima. However, due to their height difference, he is unable to confide his love for her. Sandra Thomas plays the role of another expectant woman who bears her boyfriends child which she wants to conceive only if her husband dies and Joy Mathew acts as her husband. 

==Cast== Lal as Dr. Zacharia
* Sanusha as Zaira
* Asha Sarath as Zacharias wife
* Rima Kallingal as Fatima Geetha as Sister Jasmine Jennifer
* Aju Varghese as Ajmal aka Aju, Fatimas lover 
* Sandra Thomas as Anuradha Shanavas as Zairas father
* Devi Ajith as Zairas Mother
* Joy Mathew as Anuradhas husband
* Unni Rajan P. Dev as Iqbal aka Ikku Raveendran
* Vijay Menon as Doctor
* Shivaji Guruvayoor as Sis. Jasmines relative
* Rizabawa
* Kochu Preman
* Ponnamma Babu
* Subi

==Controversy==
The film courted controversy for showing an animation in the climax scene based on Padmarajans short story, "Moovanthy". Following a complaint against the makers for copyright infringement, the court had issued an order prohibiting the satellite telecast of the movie. 

==References==
 

 
 
 
 