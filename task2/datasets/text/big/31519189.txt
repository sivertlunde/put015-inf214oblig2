Song of Scheherazade
{{Infobox Film
| name           = Song of Scheherazade
| image          = Song of Scheherazade 1947 poster.jpg
| image_size     = 300px
| caption        = 1947 Theatrical Poster
| director       = Walter Reisch
| producer       = Edward Kaufman Edward Dodds
| writer         = Walter Reisch
| narrator       = 
| starring       = Yvonne De Carlo   Jean-Pierre Aumont   Eve Arden   Brian Donlevy   Charles Kullman (as Charles Kullmann)   Elena Verdugo   Phillip Reed   John Qualen   George Dolenz
| music          =  Nikolai Rimsky-Korsakov   Miklós Rózsa
| cinematography = Hal Mohr   William V. Skall Frank Gross
| studio         = Universal International Pictures
| distributor    = Universal International Pictures
| released       = 1947
| runtime        = 
| country        = USA
| language       = English
| budget         =  gross = 2,802,722 admissions (France) 
| preceded_by    = 
| followed_by    = 
}}
 1947 American musical film directed by Walter Reisch.  It tells the story of an imaginary episode in the life of the Russian composer Nikolai Rimsky-Korsakov (Jean-Pierre Aumont), in 1865, when he was a young naval officer on shore leave in Morocco. It also features Yvonne De Carlo as a Spanish dancer named Cara de Talavera, Eve Arden as her mother, and Brian Donlevy as the ships captain. Charles Kullman (credited as Charles Kullmann), a tenor with the Metropolitan Opera, plays the ships doctor, Klin, who sings two of Rimsky-Korsakovs melodies.

== Plot ==
 symphonic suite but in the film is a ballet), with the tacit support of his captain.  There he meets Cara de Talavera and her mother, and romantic events and complications ensue.  He has to leave to return home to Russia, where his ballet is staged, but Cara unexpectedly turns up as one of the dancers, and they are reunited.  

== Soundtrack and choreography ==

The film contains much colourful music and dancing.  The choreography was by   (sung by Charles Kullman); Flight of the Bumblebee from The Tale of Tsar Saltan; "Hymn to the Sun" from The Golden Cockerel; Capriccio Espagnol, and Scheherazade (Rimsky-Korsakov)| Scheherazade. 

==Cast==
* Jean-Pierre Aumont – Nikolai Rimsky-Korsakov
* Yvonne De Carlo – Cara de Talavera
* Eve Arden -  Madame de Talavera
* Brian Donlevy – Captain Vladimir Grigorovich
* Charles Kullman – Dr Klin (as Charles Kullmann)
* Elena Verdugo - Fioretta
* Phillip Reed – Prince Mischetsky
* John Qualen - Lorenzo
* George Dolenz – Pierre, the head waiter
* Terry Kilburn - Midshipman Lorin
* William Ching - Midshipman
* Molio Sheron - Basso Robert Kendall - Hassan (uncredited)
* Chester Conklin – sailor (uncredited)

==Production==
The film was originally known as Heat Wave Rainer Seeking Gala Trail to Hollywood
Schallert, Edwin. Los Angeles Times (1923-Current File)   04 Sep 1945: A2.   then Fandango. Rimsky-Korsakov for Film: Hollywood Letter
By Frank Daugherty Special to The Christian Science Monitor. The Christian Science Monitor (1908-Current file)   19 Apr 1946: 5.  

==Release==
The filmmakers expected censorship problems with Yvonne de Carlos costumes so submitted them all beforehand for approval. However the censor found issue with Eve Ardens costumes, requiring some of her scenes to be re-shot. NEW HOLLYWOOD STRIKE: Keep It Decent HOLLYWOOD BRIEFS Musical "Wilderness" Time Out From Fishing
By THOMAS F. BRADY. New York Times (1923-Current file)   20 Oct 1946: X1.  
==References==
 

==External links==
* 


 
 
 
 
 
 
 
 
 