Song of the South
 
{{Infobox film
| name           = Song of the South
| image          = Song of south poster.jpg
| image_size     = 215px
| alt            =
| caption        = 1946 theatrical release poster
| director       = Harve Foster Wilfred Jackson
| producer       = Walt Disney George Stallings Joel Chandler Harris Johnny Lee Nick Stewart Paul J. Smith  
| cinematography = Gregg Toland
| editing        = William M. Morgan
| studio         = Walt Disney Productions
| distributor    = RKO Radio Pictures
| released       =   |ref2= }}
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = US$2.125 million Solomon, Charles (1989), p. 186. Enchanted Drawings: The History of Animation. ISBN 0-394-54684-9. Alfred A. Knopf. Retrieved February 16, 2008. 
| gross          = US$65 million 
}} RKO Radio folk tales anthropomorphic Brer montage themes, and has become widely used in popular culture. The film inspired the Disney theme park attraction Splash Mountain.

The films depiction of black former slaves, and of race relations in Reconstruction Era|Reconstruction-Era Georgia, has been controversial since its original release, with a number of critics — at the time of its release and in later decades — describing it as racist.  Consequently it has never been released in its entirety on home video in the United States.   

==Setting== plantation in before the slavery was still in force in the region, the setting is the later Reconstruction Era, after slavery was abolished. {{Cite book
  | last = Moses
  | first = Robert
  | authorlink =
  | title = AMC Classic Movie Companion
  | publisher = Hyperion
  | series =
  | volume =
  | edition =
  | year = 1999
  | location = New York
  | pages = 440
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-7868-8394-4}}  {{Cite book
  | last = Cohen
  | first = Karl F.
  | authorlink =
  | title = Forbidden Animation: Censored Cartoons and Blacklisted Animators in America
  | publisher = McFarland & Company, Inc.
  | series =
  | volume =
  | edition =
  | year = 1997
  | location = Jefferson, North Carolina
  | pages = 64
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-7864-2032-4}}  {{Cite book
  | last = Kaufman
  | first = Will
  | authorlink =
  | title = The Civil War in American Culture
  | publisher = Edinburgh University Press
  | series =
  | volume =
  | edition =
  | year = 2006
  | location = Edinburgh
  | pages =
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-7486-1935-6}}  {{Cite book
  | last1 = Langman
  | first1 = Larry
  | authorlink1 =
  | last2 = Ebner
  | first2 = David 
  | title = Hollywood’s Image of the South: A Century of Southern Films
  | publisher = Greenwood Press
  | series =
  | volume =
  | edition =
  | year = 2001
  | location = Westport Connecticut, London
  | pages = 169
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-313-31886-7}}  {{Cite book
  | last1 = Snead
  | first1 = James A.
  | authorlink1 =
  | last2=MacCabe
  | first2=Colin
  | last3=West
  | first3=Cornel 
  | title = White Screens, Black Images: Hollywood from the Dark Side
  | publisher = Routledge
  | series =
  | volume =
  | edition =
  | year = 1994
  | location = New York
  | pages = 88, 93
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-415-90574-5}}  {{Cite book
  | last = Watts
  | first = Jill
  | authorlink =
  | title = Hattie McDaniel: Black Ambition, White Hollywood
  | publisher = Amistad/Harper Collins
  | series =
  | volume =
  | edition = First
  | year = 2005
  | location = New York
  | pages =
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-06-051491-4 }}  {{Cite book
  | last = Field
  | first = Douglas
  | authorlink =
  | title = American Cold War Culture
  | publisher = Edinburgh University Press
  | series =
  | volume =
  | edition =
  | year = 2005
  | location = Edinburgh
  | pages =
  | language =
  | url =
  | doi =
  | id =
  | isbn = 0-7486-1923-2}}     Harris original Uncle Remus stories were all set after the  ; Uncle Remus is free to leave the plantation at will; black field hands are sharecroppers, etc. Walt Disney Presents "Song of the South" Promotional Program, Page 7. Published 1946 by Walt Disney Productions/RKO Radio Pictures. 

==Plot==
Seven-year-old Johnny is excited about what he believes to be a vacation at his grandmothers Georgia plantation with his parents, John Sr. and Sally. When they arrive at the plantation, he discovers that his parents will be living apart for a while, and he is to live in the country with his mother and grandmother while his father returns to Atlanta to continue his controversial editorship in the citys newspaper. Johnny, distraught because his father has never left him or his mother before, leaves that night under cover of darkness and sets off for Atlanta with only a bindle. As Johnny sneaks away from the plantation, he is attracted by the voice of Uncle Remus telling tales "in his Old Time|old-timey way" of a character named Brer Rabbit. Curious, Johnny hides behind a nearby tree to spy on the group of people sitting around the fire. By this time, word has gotten out that Johnny is gone and some plantation residents, who are sent out to find him, ask if Uncle Remus has seen the boy. Uncle Remus replies that hes with him. Shortly afterwards, he catches up with Johnny, who sits crying on a nearby log. He befriends the young boy and offers him some food for the journey, taking him back to his cabin.

As Uncle Remus cooks, he mentions Brer Rabbit again and the boy, curious, asks him to tell him more. After Uncle Remus tells a tale about Brer Rabbits attempt to run away from home, Johnny takes the advice and changes his mind about leaving the plantation, letting Uncle Remus take him back to his mother. Johnny makes friends with Toby, a little black boy who lives on the plantation, and Ginny Favers, a poor white neighbor. However, Ginnys two older brothers, Joe and Jake (who resemble Brer Fox and Brer Bear from Uncle Remuss stories, one being slick and fast-talking, the other big and a little slow), are not friendly at all; they constantly bully Ginny and Johnny. When Ginny gives Johnny a puppy, her brothers want to drown it. A rivalry breaks out among the three boys. Heartbroken because his mother wont let him keep the puppy, Johnny takes the dog to Uncle Remus and tells him of his troubles. Uncle Remus takes the dog in and delights Johnny and his friends with the fable of Brer Rabbit and the Tar-Baby, stressing that people shouldnt go messing around with something they have no business with in the first place.

 ), Uncle Remus (James Baskett), Johnny (Bobby Driscoll) and Toby (Glenn Leedy)]]
Johnny heeds the advice of how Brer Rabbit used reverse psychology on Brer Fox and begs the Favers Brothers not to tell their mother about the dog, which is precisely what they do, only to get a good spanking for it. Enraged, the boys vow revenge. They go to the plantation and tell Johnnys mother, who is upset that Uncle Remus kept the dog despite her order (which was unknown to Uncle Remus). She orders the old man not to tell any more stories to her son. The day of Johnnys birthday arrives, and Johnny picks up Ginny to take her to his party. Ginnys mother has used her wedding dress to make her daughter a beautiful dress for the party. On the way there, however, Joe and Jake pick another fight. Ginny gets pushed, and ends up in a mud puddle. With her dress ruined, the upset Ginny refuses to go to the party. Johnny, enraged with the way Joe and Jake treat Ginny, attacks them. Uncle Remus breaks up the fight, and while Johnny goes to comfort Ginny, Uncle Remus scolds Joe and Jake, telling them not to pester Johnny and Ginny anymore. Johnny doesnt want to go either, especially since his father wont be there. Uncle Remus discovers the two dejected children and cheers them by telling the story of Brer Rabbit and his "Laughing Place".

When Uncle Remus returns to the plantation with the children, Sally meets them on the way and is angry at Johnny for not having attended his own birthday party. Ginny mentions that Uncle Remus told them a story and Sally draws a line, warning him not to spend any more time with Johnny. Uncle Remus, saddened by the misunderstanding of his good intentions, packs his bags and leaves for Atlanta. Seeing Uncle Remus leaving from a distance, Johnny rushes to intercept him, taking a shortcut through the pasture, where he is attacked and seriously injured by the resident bull. While Johnny hovers between life and death, his father returns and reconciles with Sally, but Johnny calls for Uncle Remus, who has returned amidst all the commotion. Uncle Remus begins telling a tale of Brer Rabbit and the Laughing Place, and the boy miraculously survives.

Johnny, Ginny and Toby are next seen skipping along and singing while Johnnys returned puppy runs alongside them. Uncle Remus is also in the vicinity, and he is shocked when Brer Rabbit and several of the other characters from his stories appear in front of them and interact with the children. Uncle Remus rushes to join the group, and they all skip away singing.

==Cast==
* James Baskett as Uncle Remus
* Bobby Driscoll as Johnny
* Luana Patten as Ginny Favers
* Glenn Leedy as Toby
* Ruth Warrick as Sally
* Lucile Watson as Grandmother
* Hattie McDaniel as Aunt Tempy
* Erik Rolf as John
* Olivier Urbain as Mr. Favers
* Mary Field as Mrs. Favers
* Anita Brown as Maid
* George Nokes as Jake Favers
* Gene Holland as Joe Favers

===Voices=== Johnny Lee as Brer Rabbit
* James Baskett as Brer Fox and Brer Bear|Brer Fox
* Nick Stewart as Brer Fox and Brer Bear|Brer Bear
* Roy Glenn as Brer Frog

==Animation== laughing place"]]

There are three animated segments in the movie (in all, they last a total of 25 minutes). These animated sequences were later shown as stand-alone cartoon features on television. Each of these segments features at least one song that is heard in the various versions of Splash Mountain.
* " "
* "Brer Rabbit and the Tar Baby": about 12 minutes, interrupted with a short live-action scene about two thirds of the way into the cartoon, including the song "How Do You Do?"
* "Brer Rabbits Laughing Place": about 5 minutes and the only segment that doesnt use Uncle Remus as an intro to its main story, including the song "Everybodys Got a Laughing Place"

The last couple of minutes of the movie contain animation, as most of the cartoon characters show up in a live-action world to meet the live-action characters as they all sing "Zip-a-Dee-Doo-Dah", and in the last seconds of the movie, the real world is slowly merged into an animated variation as the main protagonists walk off into the sunset.

==Songs== spiritual ("All I Want").

The songs are, in film order, as follows:

* "Song of the South" Written by Sam Coslow and Arthur Johnston; performed by the Disney Studio Choir
* "Uncle Remus Said" Written by Eliot Daniel, Hy Heath, and Johnny Lange; performed by the Hall Johnson Choir
* "Zip-a-Dee-Doo-Dah" Written by Allie Wrubel and Ray Gilbert; performed by James Baskett
* "Zip-a-Dee-Doo-Dah" (reprise) Performed by Bobby Driscoll
* "Who Wants to Live Like That?" Written by Ken Darby and Foster Carling; performed by James Baskett
* "Let the Rain Pour Down" (uptempo) Written by Ken Darby and Foster Carling; performed by the Hall Johnson Choir
* "How Do You Do?" Written by Robert MacGimsey; performed by Johnny Lee and James Baskett
* "How Do You Do?" (reprise) Performed by Bobby Driscoll and Glenn Leedy
* "Sooner or Later" Written by Charles Wolcott and Ray Gilbert; performed by Hattie McDaniel
* "Everybodys Got a Laughing Place" Written by Allie Wrubel and Ray Gilbert; performed by James Baskett and Nick Stewart
* "Let the Rain Pour Down" (downtempo) Written by Ken Darby and Foster Carling; performed by the Hall Johnson Choir
* "All I Want" Traditional, new arrangement and lyrics by Ken Darby; performed by the Hall Johnson Choir
* "Zip-a-Dee-Doo-Dah" (reprise) Performed by Bobby Driscoll, Luana Patten, Glenn Leedy, Johnny Lee, and James Baskett
* "Song of the South" (reprise) Performed by the Disney Studio Choir
 Midnight Special", Civil War folk song "Zip Coon", that is considered racist as it plays on an African American stereotype.    

==History and production==
Walt Disney had long wanted to make a film based on the Uncle Remus storybook, but it was not until the mid-1940s that he had found a way to give the stories an adequate film equivalent in scope and fidelity. "I always felt that Uncle Remus should be played by a living person", Disney commented, "as should also the young boy to whom Harris old Negro philosopher relates his vivid stories of the Briar Patch. Several tests in previous pictures, especially in The Three Caballeros, were encouraging in the way living action and animation could be dovetailed. Finally, months ago, we took our foot in hand, in the words of Uncle Remus, and jumped into our most venturesome but also more pleasurable undertaking." 

Disney first began to negotiate with Harris family for the rights in 1939, and by late summer of that year he already had one of his storyboard artists summarize the more promising tales and draw up four boards worth of story sketches.    In November 1940, Disney visited the Harris home in Atlanta. He told Variety (magazine)|Variety that he wanted to "get an authentic feeling of Uncle Remus country so we can do as faithful a job as possible to these stories."  Roy Oliver Disney had misgivings about the project, doubting that it was "big enough in caliber and natural draft" to warrant a budget over $1 million and more than twenty-five minutes of animation, but in June 1944, Disney hired Southern-born writer Dalton Reymond to write the screenplay, and he met frequently with King Vidor, whom he was trying to interest in directing the live-action sequences. 

Production started under the title Uncle Remus.   Filming began in December 1944 in Phoenix, where the studio had constructed a plantation and cotton fields for outdoor scenes, and Disney left for the location to oversee what he called "atmospheric shots".  Back in Hollywood, the live action scenes were filmed at the Samuel Goldwyn Studio.

===Writing=== Walt Disney Productions to work with Reymond and co-writer Callum Webb to turn the treatment into a shootable screenplay.  According to Neal Gabler, one of the reasons Disney had hired Rapf to work with Reymond was to temper what Disney feared would be Reymonds "white Southern slant".
 Rapf was a minority, a Jew, and an outspoken Left-wing politics|left-winger, and he himself feared that the film would inevitably be Uncle Tomish. "Thats exactly why I want you to work on it," Walt told him, "because I know that you dont think I should make the movie. Youre against Uncle Tomism, and youre a radical."  

Rapf initially hesitated, but when he found out that most of the film would be live-action and that he could make extensive changes, he accepted the offer.  Rapf worked on Uncle Remus for about seven weeks. When he got into a personal dispute with Reymond, Rapf was taken off the project.  According to Rapf, Walt Disney "ended every conference by saying Well, I think weve really licked it now. Then hed call you the next morning and say, Ive got a new idea. And hed have one. Sometimes the ideas were good, sometimes they were terrible, but you could never really satisfy him."  Morton Grant was assigned to the project.  Disney sent out the script for comment both within the studio and outside the studio. 

===Casting=== Johnny Lee Oscar in 1948.   After Basketts death, his widow wrote Disney and told him that he had been a "friend indeed and   certainly have been in need". 

Also cast in the production were child actors Bobby Driscoll, Luana Patten, and Glenn Leedy (his only screen appearance). Driscoll was the first actor to be under a personal contract with the Disney studio.  Patten had been a professional model since age 3, and caught the attention of Disney when she appeared on the cover of Womans Home Companion magazine.  Leedy was discovered on the playground of the Booker T. Washington school in Phoenix, Arizona, by a talent scout from the Disney studio.  Ruth Warrick and Erik Rolf, cast as Johnnys mother and father, had actually been married during filming, but divorced in 1946.   Hattie McDaniel also appeared in the role of Aunt Tempy.

===Direction=== Bambi on ice, it made for one of the most memorable scenes in the film." 

==Release== Fox Theater in Atlanta.  Walt Disney made introductory remarks, introduced the cast, then quietly left for his room at the Georgian Terrace Hotel across the street; he had previously stated that unexpected audience reactions upset him and he was better off not seeing the film with an audience. James Baskett was unable to attend the films premiere because he would not have been allowed to participate in any of the festivities, as Atlanta was then a racially segregated city.  The film grossed $3.3 million at the box office. 
 Snow White King Features on October 14, 1945, more than a year before the film was released. Unlike the Snow White comic strip, which only adapted the film, Uncle Remus ran for decades, telling one story after another about the characters, some based on the legends and others new, until it ended on December 31, 1972.  Apart from the newspaper strips, Disney Brer Rabbit comics were also produced for comic books; the first such stories appeared in late 1946. Produced both by Western Publishing and European publishers such as Egmont Publishing|Egmont, they continue to appear. 

In 1946, a Giant Golden Book entitled Walt Disneys Uncle Remus Stories was published by Simon & Schuster. It featured 23 illustrated stories of Brer Rabbits escapades, all told in a Southern dialect based on the original Joel Chandler Harris stories.

===Critical response===
Although the film was a financial success, netting the studio a slim profit of $226,000,    some critics were less enthusiastic about the film, not so much the animated portions as the live-action portions. Bosley Crowther for one wrote in The New York Times, "More and more, Walt Disneys craftsmen have been loading their feature films with so-called live action in place of their animated whimsies of the past, and by just those proportions has the magic of these Disney films decreased", citing the ratio of live action to animation at two to one, concluding that is "approximately the ratio of its mediocrity to its charm".  However, the film also received positive notice. Time magazine called the film "topnotch Disney".  In 2003, the Online Film Critics Society ranked the film as the 67th greatest animated film of all time.   

===Controversies===
The film has received much critical attention for its handling of race. Cultural historian Jason Sperb describes the film as "one of Hollywoods most resiliently offensive racist texts", Jason Sperb, Disney’s Most Notorious Film, University of Texas Press (2012), ISBN 0292739745, ISBN 978-0292739741; reviewed in John Lingan,  , Slate Jan. 4, 2013 (accessed Aug. 21, 2013)  noting as Gabler, Stephen Watts and Thomas Cripps all had previously, the films appearance in the wake of the Double V Campaign during World War II ("victory over fascism abroad, victory over racism at home".)

Early in the films production, there was concern that the material would encounter controversy. Disney publicist Vern Caldwell wrote to producer Perce Pearce that "the negro situation is a dangerous one. Between the negro haters and the negro lovers there are many chances to run afoul of situations that could run the gamut all the way from the nasty to the controversial." 

When the film was first released, Walter Francis White, the executive secretary of the National Association for the Advancement of Colored People (NAACP), telegraphed major newspapers around the country with the following statement, erroneously claiming that the film depicted an antebellum setting:
 The National Association for the Advancement of Colored People recognizes in "Song of the South" remarkable artistic merit in the music and in the combination of living actors and the cartoon technique. It regrets, however, that in an effort neither to offend audiences in the north or south, the production helps to perpetuate a dangerously glorified picture of slavery. Making use of the beautiful Uncle Remus folklore, "Song of the South" unfortunately gives the impression of an idyllic master-slave relationship which is a distortion of the facts. 
 
 Negro dialect. 
 Gone with the Wind; Jezebel (film)|Jezebel] assumed it must also be set during the time of slavery." Based on the Jensen and Springarn memos, White released the "official position" of the NAACP in a telegram that was widely quoted in newspapers.  The New York Times Bosley Crowther, mentioned above, made a similar assumption, writing that the movie was a "travesty on the History of the Southern United States#Antebellum Era .281781.E2.80.931860.29|antebellum South." 

In the same vein, Time magazine, although it praised the film, also cautioned that it was "bound to land its maker in hot water", because the character of Uncle Remus was "bound to enrage all educated Negroes and a number of  ".  Adam Clayton Powell, Jr., a congressman from Harlem, branded the film an "insult to American minorities   everything that America as a whole stands for."    The National Negro Congress set up picket lines in theaters in the big cities where the film played, with its protesters holding signs that read "Song of the South is an insult to the Negro people" and, lampooning "Jingle Bells", chanted: "Disney tells, Disney tells/lies about the South."  Jewish newspaper Bnai Brith Messenger of Los Angeles considered the film to be "tall  with the reputation that Disney is making for himself as an arch-reactionary".

At the same time, however, some black press had mixed reactions on what they thought of Song of the South. While Richard B. Dier in The Afro-American was "thoroughly disgusted" by the film for being "as vicious a piece of propaganda for white supremacy as Hollywood ever produced," Herman Hill in The Pittsburgh Courier felt that Song of the South would "prove of inestimable goodwill in the furthering of interracial relations", and considered criticisms of the film to be "unadulterated hogwash symptomatic of the unfortunate racial neurosis that seems to be gripping so many of our humorless brethren these days."   
 The Hays Office had asked Disney to "be certain that the frontispiece of the book mentioned establishes the date in the 1870s"; however, the final film carried no such statement. 

===Academy Award recognition=== Paul J. Smith, and Charles Wolcott was nominated in the "Scoring of a Musical Picture" category, and "Zip-a-Dee-Doo-Dah", written by Allie Wrubel and Ray Gilbert, won the award for Best Song at the 20th Academy Awards on March 20, 1948. 

A special Academy Award was given to James Baskett "for his able and heart-warming characterization of Uncle Remus, friend and story teller to the children of the world in Walt Disneys Song of the South". Bobby Driscoll and Luana Patten in their portrayals of the children characters Johnny and Ginny were also discussed for Special Juvenile Awards, but in 1947 it was decided not to present such awards at all. 

===Re-releases===
Song of the South was re-released in theatres several times after its original Walt Disney Pictures/RKO Pictures premiere, each time through  ; in 1980 for the 100th anniversary of Harris classic stories; and in 1986 for the films own 40th anniversary and in promotion of the upcoming Splash Mountain attraction at three of Disneys theme parks.

===Home media===
Disney Enterprises has avoided making the complete version of the film directly available on home video in the United States because the frame story was deemed controversial by studio management. Film critic Roger Ebert, who normally disdained any attempt to keep films from any audience, supported the non-release position, claiming that most Disney films become a part of the consciousness of American children, who take films more literally than do adults. However, he favored allowing film students to have access to the film. 

Over the years, Disney has made a variety of statements about whether and when the film would be re-released.       In March 2010, Disney CEO Robert Iger stated that there were no plans to release the movie on DVD, calling the film "antiquated" and "fairly offensive".  On November 15, 2010, Disney creative director Dave Bossert stated in an interview, "I can say theres been a lot of internal discussion about Song of the South. And at some point were going to do something about it. I dont know when, but we will. We know we want people to see Song of the South because we realize its a big piece of company history, and we want to do it the right way." 
 Alice in Alice in Wonderland film.

The film has been released on video in its entirety in various European, Latin American, and Asian countries—in the UK it was released on PAL VHS tape in 1982 and again in 1991, and in Japan it appeared on NTSC VHS, Beta, and laserdisc in 1985 then again on laserdisc in 1990 with subtitles during songs. (On a side note under Japanese copyright law it is now in the public domain.) 

A NTSC laserdisc was released out of Hong Kong for the Chinese rental market by Intercontinental Video Ltd which has been the exclusive distributor of Walt Disney Studios since 1988. This release appears to have been created from a PAL videotape, and has a 4% faster running time because of its PAL source, and thus also suffers from "frame ghosting".    

While most foreign releases of the film are literal translations of the English title (Canción del Sur in Spanish, Mélodie du Sud in French, Melodie Van Het Zuiden in Dutch, Sången om södern in Swedish, A Canção do Sul in Portuguese, and Etelän laulu in Finnish), the German title Onkel Remus Wunderland translates to "Uncle Remus Wonderland", the Italian title I Racconti Dello Zio Tom translates to "The Stories of Uncle Tom",    and the Norwegian title Onkel Remus forteller translates to "Storyteller Uncle Remus."   

Despite the films lack of an official home video release directly to consumers in the United States, audio from the film—both the musical soundtrack and dialogue—were made widely available to the public from the time of the films debut up through the late 1970s. In particular, many Book-and-Record sets were released, alternately featuring the animated portions of the film or summaries of the film as a whole. 

==References in other Disney media== newspaper strip Disney comic Big Bad Wolf stories, although here, Brer Bear was usually cast as an honest farmer and family man, instead of the bad guy in his original appearances.

Brer Rabbit, Brer Fox and Brer Bear appeared as guests in  . Brer Bear and the Tar-Baby also appear in the film Who Framed Roger Rabbit. Brer Bear can be seen near the end while the Toons are celebrating finding the will. The Tar-Baby can briefly be seen during the scene driving into Toon Town.
 James Avery, who previously voiced Brer Bear and Brer Frog in the Walt Disney World version of Splash Mountain. This is the Brers first major appearance in Disney media since House of Mouse in 2001 and their first appearance as computer-generated characters.

==See also==
 
* Coonskin (film)|Coonskin, a 1975 live-action/animated film featuring Brother Rabbit and Brother Bear
*  

==References==
 

==Further reading==

* Jason Sperb, Disneys Most Notorious Film: Race, Convergence, and the Hidden Histories of Song of the South. Austin, TX: University of Texas Press, 2012.

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 