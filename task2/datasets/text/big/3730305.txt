Bed of Roses (1933 film)
{{Infobox film
| name           = Bed of Roses
| image          = Bed_of_Roses_Poster.jpg 
| image size     =
| caption        = original film poster Charles Kerr (assistant)
| producer       =
| writer         =
| narrator       =
| starring       = Constance Bennett Joel McCrea Pert Kelton
| music          =
| cinematography =
| editing        =
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 67 min.
| country        = United States English
| budget         =
}}
Bed of Roses (1933) is a Pre-Code comedy film directed by Gregory La Cava and starring Constance Bennett.  The picture was released by RKO Radio Pictures.

==Plot and production background== The Music Man) portray a pair of rollickingly wanton prostitutes who occasionally get hapless male admirers drunk before robbing them.  After being released from a Louisiana jail they head downriver on a steamboat.  Lorry steals $60, and when she is cornered, she jumps ship.  She is rescued by cotton barge skipper Dan (Joel McCrea), but she robs him, too.  In New Orleans, she tricks publishing magnate Stephen into getting drunk and blackmails him into lavishly providing for her.  She returns to the cotton barge and repays Dan his "loan" and they fall in love.   Stephen threatens to expose her past, causing her to leave him but not to return to Dan, whom she had agreed to marry.  When Stephen cannot persuade her to return to him, he realizes that she really does love Dan, and he effectuates their reunion.

The bawdy film is laced with witty dialogue, some of which was scripted by La Cava, as well as scenes that would never be allowed after the adoption of the Production Code in July 1934.

==Cast==
*Constance Bennett as Lorry Evans
*Joel McCrea as Dan John Halliday as Stephen Paige
*Pert Kelton as Minnie
*Samuel S. Hinds as Father Doran (as Samuel Hinds)
*Franklin Pangborn as Floorwalker Tom Francis as Salesman

==References==
 

==External links==
*  
* 

 

 
 
 
 
 
 
 
 
 
 
 


 