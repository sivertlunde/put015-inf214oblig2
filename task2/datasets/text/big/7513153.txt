It's Nice Up North
 
{{Infobox film
| name           = Its Nice Up North
| image          = Its Nice Up North.jpg
| caption        = DVD cover of Its Nice Up North
| director       = Graham Fellows
| producer       =
| eproducer      =
| aproducer      =
| writer         = Graham Fellows Shetland
| music          =
| cinematography = Martin Parr
| editing        =
| distributor    =
| released       = 2006
| runtime        = 79 min.
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 documentary made by comedian Graham Fellows as his alter ego John Shuttleworth.

It was filmed by photographer Martin Parr and edited by Fellows on his laptop on a very low budget.
 Shetland Islands to test his theory that the further north in Great Britain you go the nicer people are, Shetland being the most far north part of the UK. He meets various Shetland people in unrehearsed situations. Many assume him to be a real person and not a comic creation, though some scenes are acted, particularly parts with famous local tour guide Elma Johnson.

It had a limited theatrical release in some art-house and community cinemas around the UK in 2006, including some screenings in Shetland, with Fellows answering questions after the showing. The film was released on DVD in the same year.
The film has also been shown on Sky Arts.

==External links==
* 

 
 
 
 
 
 
 


 
 