Auto Raja (1982 film)
{{Infobox film
| name = Auto Raja
| image = 
| caption = 
| director = K. Vijayan
| producer = Santosh Art Films
| story = M. D. Sundar
| dialogues= 
| screenplay= 
| starring = Vijayakanth Jaishankar Gayathri
| music = Ilaiyaraaja 1 Song  Shankar Ganesh 5 Songs
| cinematography = 
| editing = 
| released = 1982
| country = India
| language = Tamil
}}
 1982 Tamil Tamil  Indian feature directed by Kannada film of same name.

== Cast ==
*Vijayakanth
*Jayasankar
*Gayatri

==Soundtrack==
{{Infobox album Name  Auto Raja Type     = film Artist   = Illayaraja and Shankar Ganesh Cover    = Released =  Music    = Illayaraja and Shankar Ganesh Genre  Feature film soundtrack Length   = 25:30 Label    = EMI
}}

The soundtrack was composed by Shankar Ganesh and Maestro Ilaiyaraja composed 1 song. A total of six songs were composed of which all the six were included in the soundtrack.  The song "Malare Ennenna" reused from the track of original Kannada film. Ilayarajas composition "Sangathil Paadaatha" remains one of the famous tracks.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Lyrics !! Music
|- 1 || Pulavar Puthumaipithan|| Illayaraja
|- 2 || Malare Enna Kolam... || S. P. Balasubrahmanyam || Shankar Ganesh
|- 3 || Kanni Vannam... || S. P. Balasubrahmanyam, Vani Jairam || Shankar Ganesh
|- 4 || Oru Vargappuratchi... || T. M. Soundararajan || Shankar Ganesh
|- 5 || Kattilarai... || S. Janaki || Shankar Ganesh
|- 6 || Chorus || Shankar Ganesh
|}

==References==

 

 
 
 
 
 
 


 