Flor de Liza
{{Infobox film
| show_name = Flor de Liza
| image =
| format = Drama
| Production Co = seven Stars productions
| director = Nick Lizaso
| written By = Jose N. Carreon
| executive_producer = Jesse Ejercito
| starring = Julie Vega, Janice De Belen
| theme_music_composer = George Canseco
| opentheme = "Flor de Liza" by Kuh Ledesma
| country = Philippines English
| first_aired = June 4, 1981
| website =
}}

Flor de Liza is a Philippine film series produced by Seven Stars Productions launched in 1981 that stars Julie Vega and Janice De Belen.  It is a crossover between the TV series Flordeluna (played by Janice De belen)  and  Anna Liza (played By Julie Vega).

==Synopsis==
Flor de Liza is a story of two different girls who has the same dad. Flor who was born in a wealthy family and Liza who was born on an average society and who grew up without knowing and seeing her dad.

Until Lizas dad decided to look for her and asked her to be with her but refused to because she doesnt want to leave her mother.

Liza only decided to leave her Mom when her father suddenly died on an accident. She was forced to move with Flors family and live with them, but it turned out that Flors Mom doesnt want Liza to be friends with her daughter.

But nobody could tell them what to do, and they became best of friends. But Flors Mom will do everything just to keep Liza away from Flor...

==Cast==
===Main cast===
*Julie Vega as Liza
*Janice De Belen as Flor
*Daria Ramirez as Lizas Mom
* Baby Delgado as Flors Mom
* Anita Linda
* Eddie Rodriguez

==Trivia==
This movie is the first collaboration of Julie Vega and Janice De Belen, two of the most famous childstars of their time who were stars in rival shows Anna Liza and Flordeluna respectively, which inspires the title of the movie.

 