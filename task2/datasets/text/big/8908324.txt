Amalia (1936 film)
{{Infobox film
| name           = Amalia
| image          = Amaliaposter.jpg
| image size     =
| caption        =
| director       = Luis Moglia Barth
| producer       =
| writer         = Luis Moglia Barth based on a novel by José Mármol
| narrator       =
| starring       = Herminia Franco
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1936
| runtime        = Argentina
| language       = silent film Spanish intertitles
| budget         =
| preceded by    =
| followed by    =
}} Argentine silent Amalia of the same name. The film starred Herminia Franco. 

==Main cast==
*Herminia Franco as Amalia
*Floren Delbene
*Miguel Gómez Bao
*Ernesto Raquén
*Herminia Mancini
*Nélida Franco
*Delia Cobedo

==Supporting cast==
*Francisco Bastardi
*Delia Codebó
*Alfredo Gobbi
*Carlos Perelli
*José Ruzzo
*Juan Sarcione
*Marino Seré
*Juan Siches de Alarcón
*Antonia Volpe

==See also==
*Amalia (1914 film)|Amalia (1914 film)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 