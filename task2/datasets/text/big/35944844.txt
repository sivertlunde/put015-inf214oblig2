Lovely Molly
{{Infobox film
| name           = Lovely Molly
| image          = Lovely-molly-poster.jpg
| image_size     = 
| caption        = American film poster Eduardo Sánchez Jane Fleming, Gregg Hale, Mark Ordesky Jamie Nash
| story          = Eduardo Sánchez
| narrator       = 
| starring       = Gretchen Lodge Johnny Lewis Alexandra Holden Lauren Lakis
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = May 18, 2012
| runtime        = 100 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = $18,464 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Eduardo Sánchez.  The film initially had a working title of The Possession but was later changed to Lovely Molly.  The plot follows newlyweds Molly and Tim as they move into the brides childhood home, where painful memories soon begin to haunt Molly.

==Plot==
The film opens with a video recording of Molly apologizing for her actions and attempting to slit her throat with a knife. The date is October 16, 2011, and much of the movie is revealed in flashback via video. The film proceeds with the events during the wedding of Tim (Johnny Lewis) and Molly (Gretchen Lodge), after which the couple move into Mollys childhood home with the help of Mollys sister, Hannah (Alexandra Holden). 

Not long after the wedding, the couple are startled in their sleep when the alarm system buzzes. They call the police but the officer finds no evidence of forced entry. Shortly afterwards, Tim, a truck driver, must leave town for a few days, leaving Molly, a recovering heroin addict, alone. The film is then interspersed by images from a video camera where someone, most likely Molly, wanders the house, and reveals a hidden underground shrine in the shed. While in her childhood room, she hears something in her closet, although it is not revealed who is inside. Upon his return, Tim discovers Molly naked and staring blankly into one corner of her bedroom. She seems delusional and later cannot remember telling him "he is alive." 

Molly begins to record a little girl and her mother who live not far from her house. One night, Molly hears a noise from the backdoor and tries to lock it, but is suddenly pushed back by an unknown force. Shortly afterwards, at work, Molly hears someone whispering her name. A man begins to quietly sing the traditional song "Lovely Molly", accompanied by the sound of hooves. The next day, Molly is summoned by her boss, who shows Molly the surveillance camera footage, which seems to show Molly being sexually assaulted by an unknown force. Molly becomes hysterical and is sent home with her sister. 

Mollys condition turns worse, filming and talking to a dead deer, and attempting to seduce the local pastor. Fearing for her safety, Tim takes Molly to the doctor, but is advised she is healthy physically. That night, Molly attacks Tim and bites his lips, nearly pulling them off before fleeing. Terrified, Tim calls Hannah, who urges him to seek medical care. In the basement, Hannah finds that Molly has been keeping a rotting deer, which she stabs repeatedly, screaming that their father told her how Hannah killed him. Hannah replies that she was protecting herself and the young Molly from their father, and begs her to come stay with her and her son Peter. Molly refuses and then begins giggling, saying that if she does, her father will gut Peter from belly to throat. 

More video is shown, spying on the young girls family. The video pans and a distraught Tim sits with the mother on the couch. It is clear they are familiar, and the mother begins oral sex on Tim. A car pulls up the drive and it is revealed to be Pastor Bobby: Molly emerges from the house naked and the two embrace. Shortly afterwards, Pastor Bobby is seen in the bathtub, dead and covered in blood and bites. Tim goes back to the house and finds the video camera left playing on the bed, showing a recording of himself with the neighbour. Molly strikes him across the head with a bat, drags him downstairs, and kills him by stabbing him in the head with a screwdriver, the same way Pastor Bobby was killed. Another video edit shows the police in the woods digging, pulling away to reveal the body of the young girl. The mother sobs, and Molly slips away. Afterwards, Molly lies naked and sobbing on the floor until her attitude abruptly changes and she shows a half smile. She walks out of the backdoor naked towards a tall figure with glowing eyes, a horse head and body of a man (resembling the demon Orobas).

In the aftermath, the couples house is up for sale. There is no clear answer as to Mollys whereabouts or her disappearance, but time has clearly passed. In their old bedroom, Hannah finds the family photo album on the floor. Flipping though the pages, she sees that their fathers face has been covered with horse heads from his frame collection. Hearing a noise from the closet, she opens it and in a trance-like state reaches her arm towards something unseen, similar to Mollys discovery earlier.

==Cast==
*Gretchen Lodge as Molly 
*Johnny Lewis as Tim
*Alexandra Holden as Hannah Ken Arnold as Samuels
*Lauren Lakis as Lauren

==Production==
  Dahlgren Chapel being one of the locations filmed. 

==Soundtrack== Roud 454), a traditional emigration ballad that has been recorded by artists such as The Stanley Brothers (in 1961), Norma Waterson (in 1977), and Nic Jones (in 1980). 

==Reception==
 
Critical reception for Lovely Molly was mixed, with the film holding a 41% "rotten" rating on Rotten Tomatoes, based on 32 ratings.  Lovely Molly received positive reviews from the New York Times and the A.V. Club,  with the latter praising Gretchen Lodges performance and favorably comparing the film to Roman Polanskis Repulsion (1965).  Little White Lies also praised both Lodges "transformative performance" and Sánchezs "challengingly ambiguous" tone,  while Toronto.com wrote that "there’s just enough of the right stuff in Lovely Molly – a pervasive sense of dread, an implacably evil presence and a doomed heroine – to leave a haunting and lasting impression." 

The Globe and Mail panned the film, writing that "Some of the shock effects in Lovely Molly are successfully disorienting, but too many of its ideas are reductive and histrionic."  The Newark Star-Ledger also criticized the film, claiming that it represented "stuff we’ve seen before."  The Guardian gave the film two stars, stating that "the film presents us with too many unearned revelations, and it unravels." 

==References==
 

==External links==
* 
*  
 

 
 
 
 