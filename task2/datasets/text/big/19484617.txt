Panserbasse
{{Infobox Film
| name           = Panserbasse
| image          = Panserbasse.jpg
| image_size     =
| caption        = Front cover of the Danish VHS video
| director       = Lau Lauritzen, Jr. Alice OFredericks
| producer       = 
| writer         = Lau Lauritzen, Jr. Alice OFredericks
| narrator       = 
| starring       = Ib Schønberg
| music          = 
| cinematography = Carlo Bentsen
| editing        = Edith Schlüssel
| distributor    = 
| released       = 1 October, 1936
| runtime        = 99 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Panserbasse is a 1936 Danish film directed by Lau Lauritzen, Jr. and Alice OFredericks.

==Cast==
* Knud Almar
* Carl Carlsen
* Victor Cornelius Carl Fischer
* Aage Fønss
* Ingeborg Gandrup
* Ellen Jansø Arthur Jensen
* Gunnar Lauring - Slubberten
* Lau Lauritzen, Jr.
* Albert Luther
* Karen Lykkehus
* Connie Meiling - Lille Connie
* Carl Viggo Meincke
* Carola Merrild
* Poul Reichhardt - Mekanikeren
* Ib Schønberg - Gadebetjent Peter Basse
* Lis Smed
* Erika Voigt
* Alex Zander

==External links==
* 

 
 

 
 
 
 
 
 
 