Pimpin' Pee Wee
{{Infobox film
| name           = Pimpin Pee Wee
| image          = 
| image size     =
| caption        = 
| director       = Brian Trenchard-Smith
| producer       = Joel Roodman & Paul Bronfman
| writer         = 
| based on = 
| narrator       =
| starring       = Whitney Anderson Leslie Augustine James Moses Black
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 2010
| runtime        = 
| country        = US English
| budget         = $500,000 
| gross = 
| preceded by    =
| followed by    =
}}
Pimpin Pee Wee is a 2009 comedy film. It was a sequel to Porkys (1982). 

==Plot==
Three college student friends have adventures over the summer.

==Cast==
*Adam Wylie as Pee Wee
*Russ Hunt as Meat

==Production==
In 1994, a company called Lontano Investments purchased all rights to Porkys. In 2001 Lontano signed a contract with Mola Entertainment giving the latter the exclusive right to produce a Porkys film in exchange for 1.5% of the budget of the picture, less option fees. The agreement also gave Mola the right to produce another sequel if it satisfied the conditions of its deal, including completing the first picture within five years and paying the purchase fee in whole. This agreement was subsequently amended a number of times, extending the time and increasing the purchase fee from 1.5% of the budget to 2.5%. Mola originally wanted to make a film with a budget of around $10 million but were unable to raise the finance. They decided to make a film in 2009 for under $1 million before its rights expired. It was originally entitled Porkys: The College Years and was later renamed Pimpin Pee Wee.   accessed 4 February 2013 

"As time was running out they thought let’s whip up this cheap one and bury it," said Brian Trenchard-Smith, who was hired to direct four weeks before shooting began. 

Shooting took place over 15 days on location in Canyon Country, California, and at a studio in Simi Valley, California, starting in October. The director says the film was "designed as an homage to the 80′s sex-comedies where there are raging hormones that cause characters to defy logic and moral scruples in the search for the Holy Grail, which is to get laid. A desperate attempt to get laid."   accessed 6 February 2010  He says his aim was to do an update that did not lose the essence of the original.

==Release==
The movie was briefly released via video on demand but was not released on DVD.  Mola then claimed they had fulfilled their obligations under their contract with Lontano and retained rights to make another sequel. However, Lontano subsequently claimed that the film required a budget of $10 million to satisfy the arrangement. A lawsuit ensued, affecting plans by Howard Stern to make his own remake as Mola maintained it held rights on Porkys until 1 April 2014. 

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 