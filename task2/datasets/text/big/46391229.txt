Kaviyam
{{Infobox film
| name           = Kaviyam
| image          =
| image_size     =
| caption        =
| director       = M. Ponraj
| producer       = Manjil Pradhan
| writer         = M. Ponraj
| starring       =  
| music          = M. Ponraj
| cinematography = Dharma
| editing        = R. T. Annadurai
| distributor    =
| studio         = Aarthi Film Circuit
| released       =  
| runtime        = 135 minutes
| country        = India
| language       = Tamil
}}
 1994 Tamil Tamil drama film directed by M. Ponraj. The film features newcomer Ruban George and Nandhini in lead roles, with newcomers Salomon, Thamizhan, M. Ponraj and Venniradai Moorthy playing supporting roles. The film, produced by Manjil Pradhan, had musical score by the director M. Ponraj and was released on 18 February 1994 to mixed reviews.    

==Plot==

The film begins with the cancellation of a wedding. The bride Shanthi (Nandhini) refuses to marry the groom Kumar (Salomon) who wanted a large dowry and she humiliates him. Shanthi and Kumar are doctors in the same hospital. Shanthi challenges him to get married with a way better groom who will not ask her any dowry. Raja (Ruban George) is a villager who comes to the city searching work. In town, Raja befriends with Vaithi (M. Ponraj) and Vaithi accommodates him in his lodge. One night, Raja finds an old man who get struck by lightning and he brings that old man to his daughter Shanthi but the old man dies on the way. Thereafter, Raja becomes a traffic police and he falls in love with Shanthi. When Raja expresses his love to Shanthi, she rejects it. Shanthi slowly develops a soft corner for the kind-hearted Raja. What transpires later forms the crux of the story.

==Cast==

 
*Ruban George as Raja
*Nandhini as Shanthi
*Salomon as Kumar
*Thamizhan as Santhanam
*M. Ponraj as Vaithi
*Venniradai Moorthy
*Thayir Vadai Desikan
*Kambar Jayaraman as Shanthis grandfather
*Brinda
*Beena
*Sulakshana
*Kanal Kannan
 

==Soundtrack==

{{Infobox album |  
| Name        = Kaviyam
| Type        = soundtrack
| Artist      = M. Ponraj
| Cover       = 
| Released    = 1994
| Recorded    = 1994 Feature film soundtrack
| Length      = 23:22
| Label       = 
| Producer    = M. Ponraj
| Reviews     = 
}}

The film score and the soundtrack were composed by the film director M. Ponraj. The soundtrack, released in 1994, features 5 tracks with lyrics written by Salomon.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Thittikkum Maragathame || Mano (singer)|Mano, Chorus || 3:50
|- 2 || Kasu Panam || Malaysia Vasudevan, Chorus || 5:09
|- 3 || Nee Iravu || S. P. Balasubrahmanyam || 4:45
|- 4 || Maalai Nilavae || Mano, S. Janaki || 5:05
|- 5 || Unnuruvam || Mano || 4:33
|}

==References==
 

 
 
 
 