Earthbound (1981 film)
{{Infobox film
| name           = Earthbound
| image          =
| image size     =
| alt            =
| caption        =
| director       = James L. Conway Michael Fisher Bill Cornford (associate producer) Charles Sellier (executive producer)
| writer         = Michael Fisher
| narrator       = Todd Porter Christopher Connelly Meredith MacRae
| music          = Bob Summers Paul Hipp Michael Spence
| studio         = Taft International Pictures
| distributor    = Taft International Pictures
| released       = January 30, 1981
| runtime        =  94 min.
| country        =   English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Earthbound is a 1981 American science fiction film.

==Plot summary==

When a family of aliens is stranded in the Midwest after their spaceship crashes, a kind innkeeper takes them in. Their peace is threatened by a government agent who wants to assure that the aliens cannot intermingle with humankind.

==Cast==
* Burl Ives	... Ned Anderson Todd Porter ... Tommy Anderson Christopher Connelly ... Zef
* Meredith MacRae ... Lara
* Joseph Campanella ... Conrad
* Marc Gilpin ... Dalem
* Mindy Dow	... Rosie
* Elissa Leeds ... Teva
* Peter Isacksen ... Willy
* John Schuck ... Sheriff De Rita
* Joey Forman ... Madden
* Stuart Pankin ... Sweeney
* H.M. Wynant ... Dave
* Jesse Bennett ... Gold Rush man #2
* Cindy Bertagnolli ... Unknown Extra
* Allen Tatomer ...Extra, Local Posse,Townie
* Scottie Anderson ...Extra, Local Posse, Townie

==External links==
*  
*  
*  

 
 
 
 
 
 


 
 