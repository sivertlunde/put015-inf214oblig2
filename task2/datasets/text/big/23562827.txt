Boris Godunov (1986 film)
 
{{Infobox film
| name           = Boris Godunov
| image          =
| caption        =
| director       = Sergei Bondarchuk
| producer       =
| writer         = Sergei Bondarchuk Alexander Pushkin
| starring       = Sergei Bondarchuk
| music          = Vyacheslav Ovchinnikov
| cinematography = Vadim Yusov
| editing        = Lyudmila Sviridenko
| distributor    =
| released       =  
| runtime        = 141 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}

Boris Godunov ( ) is a 1986 Soviet drama film directed by and starring Sergei Bondarchuk. It was entered into the 1986 Cannes Film Festival.   

==Cast==
* Sergei Bondarchuk as Boris Godunov
* Alyona Bondarchuk as tsarevna Xenia Godunova
* Gennady Mitrofanov as fool for Christ
* Roman Filippov as Patriarch Job of Moscow
* Valeri Storozhik as prince Dmitry Kurbsky
* Yuri Lazarev as Gavrila Pushkin
* Vladimir Sedov as Afanasy Pushkin
* Georgi Burkov as Barlaam
* Vadim Aleksandrov as Misael
* Irina Skobtseva as hostess of korchma
* Kira Golovko as mamka of Xenia
* Lyudmila Korshakova as tsaritsa Maria Skuratova-Belskaya Theodore Godunov
* Henryk Machalica as Jerzy Mniszech
* Olgierd Łukaszewicz as Mikołaj Czernikowski
* Marian Dziędziel as Adam Wiśniowiecki Vladimir Novikov as Semyon Godunov

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 