Roja
 
{{Infobox film
| name = Roja
| image = Roja Poster.jpg
| caption = Film poster
| director = Mani Ratnam
| writer = Mani Ratnam Sujatha Rangarajan
| story = Mani Ratnam
| screenplay = Mani Ratnam
| starring = Arvind Swamy Madhoo
| producer = K. Balachander  Pushpa Kandaswamy
| cinematography = Santosh Sivan
| editing = Suresh Urs Pyramid
| released =   
| runtime = 139 minutes Tamil
| country = India
| music = A. R. Rahman
| gross = 
}}
 1992 Tamil cinema|Tamil-language Indian political drama-romance film co-written and  directed by Mani Ratnam. It stars Arvind Swamy and Madhoo in the lead roles. The film was subsequently dubbed in Hindi, Marathi language|Marathi, Malayalam and Telugu language|Telugu.
 National Film Best Film terrorist attacks across the world. It is the first in Ratnams trilogy of films that depict human relationships against a background of Indian politics, including Bombay (film)|Bombay (1995) and Dil Se.. (1998). {{cite web |url= http://www.cinescene.com/names/maniratnam.html
|title= FROM THE HEART – The Films of Mani Ratnam
|accessdate= 2011-04-04 |author= Pat Padua |publisher= cinescene.com}} 
 soundtrack were 10 Best Soundtracks" of all time listed by TIME (magazine)|TIME magazine, issued in 2005.   

==Plot== cryptologist working RAW of India, goes smoothly. Unknown to her and her family, Roja’s sister is in love with the son of her paternal aunt.

When Rishi wishes to speak to Roja’s sister alone, she gathers enough courage to convey this and politely asks him to reject her in front of her parents, to which he obliges. To everyone’s surprise Rishi requests Rojas hand in marriage instead. Being unaware of her sisters love affair, Roja is not willing to accept Rishis proposal as she believes that he is the best match for her sister but she marries Rishi, and the couple go to live in Madras while her sister is married to her aunts son.

Initially Roja does not like what Rishi did, but when she learns of her sisters love affair and consequent rejection of Rishi, she apologizes and starts seeing him in a new light. Love blossoms, and life is blissful for the couple for a short while. Meanwhile, due to the illness of his Boss, Rishi is assigned a special assignment at an army communications center in Baramulla. The couple find themselves in a beautiful yet alien land. Rojas world turns upside down when Rishi is abducted by terrorists whose agenda is to separate Kashmir from India and to free their leader Wasim Khan from judicial custody.

Faced with the daunting task of rescuing her husband, Roja runs from pillar to post, pleading with politicians and the military for help. Further complicating matters is the communication gap: She cant speak their language, and they cant speak hers. Meanwhile Rishi, held captive by a group of terrorists led by Liaqat (Pankaj Kapur), an  associate to Wasim Khan. Rishi tries to reason with the terrorists, about their misdirected motive for the liberation of Kashmir. Liaqat’s sister shows a little compassion towards him. Initially, when Roja’s efforts fail, the Indian government denies any negotiations with the terrorists for the release of Rishi in the media.

The angered terrorists burn an Indian flag. Rishi risks his life to put out the fire and shows the terrorist how much the country means to him, a regular citizen. When Liaqat’s younger brother, who with few other youths from his village are sent across the border to Pakistan for training, is shot down by the Pakistan Army, Liaqat’s strong belief is shaken, but he still manages to convince himself of the cause. Consequently, Roja’s efforts to apprise the politicians of her suffering and pain are successful as a minister pities her and offers to help.

Much to the chagrin of Colonel Rayappa, the government decides to release Wasim Khan in exchange for  Rishi. Rishi, not wanting to be used as a pawn to release a dangerous terrorist, gets help from the sympathetic Liaqat’s sister and escapes — with Liaqat and his men chasing him. Colonel Rayappa, Roja and other Army officers get to the hostage exchange spot with Wasim Khan, but Liaqat does not show up. This leads Roja to think that Rishi is dead. Army locks Wasim Khan up in the prison.

Rishi has managed to get close to the exchange spot on his own after evading the terrorists. During his escape, Rishi kill two terrorists. Liaqat catches up with him and holds him at gun point. Rishi reasons with Liaqat further and convinces him that his war is immoral. Liaqat lets Rishi go and he goes to the exchange spot. Liaqat escapes from the Indian Army. Rishi and Roja are united once again.

==Cast==
*Arvind Swamy as Rishikumar
*Madhoo as Roja Colonel Rayappa Janagaraj as Achu Maharaj
*Pankaj Kapoor as Liaqat
*Shiva Rindani as Wasim Khan
*Vaishnavi as Shenbagam
*Sathyapriya as Rishikumars mother
*Raju Sundaram (Item number – Rukkumani Rukkumani)
*Professor Chandramohan as Rishikumars boss & RAW official
*Vathsala Rajagopalan

==Production== Aishwarya was initially offered the female lead, but declined it due to date issues. The role went to Madhoo.  Ratnam had planned to shoot Roja in Kashmir, but extreme terrorism there forced him to shoot the film in other hill stations resembling it.   Shooting locations included Ooty,  Manali, Himachal Pradesh.    The films cinematographer Santosh Sivan said that a lot of images were written in at the script level. Even in the Kashmir sequences, the audience only sees the snow when Roja sees it for the first time. These things were written into the script.  The song "Chinna Chinna Aasai" was shot at Hogenakkal Falls in Dharmapuri and in the Banatheertham falls in Courtallam.  

==Soundtrack==
 

==Awards==
The film has won the following awards since its release:    Madhoos performance took her close to winning the National Film Award for Best Actress, but she eventually lost to Dimple Kapadia. 
 National Film Awards (India) Silver Lotus Award – Best Music Director – A. R. Rahman Silver Lotus Award – Best Lyricist – Vairamuthu
* Won – Nargis Dutt Award for Best Feature Film on National Integration

1993 Filmfare Awards South
* Won – Filmfare Best Movie Award (Tamil) – Roja
* Won – Filmfare Best Music Director Award (Tamil) – A. R. Rahman

1993 Tamil Nadu State Film Awards (India)
* Won – Tamil Nadu State Film Award for Best Film
* Won – Tamil Nadu State Film Award for Best Director – Mani Ratnam
* Won – Tamil Nadu State Film Award for Best Cinematographer – Santosh Sivan
* Won – Tamil Nadu State Film Award for Best Music Director – A. R. Rahman
* Won – Tamil Nadu State Film Award for Best Female Playback – Minmini
* Won – Tamil Nadu State Film Award Special Prize – Madhoo

1993 Shantaram Awards
* Won – Best Director – Mani Ratnam
 Moscow International Film Festival (Russia)
* Nominated – Golden St. George (Best Film) – Mani Ratnam

Bite the Mango Film Festival (United Kingdom) 
* Featured screening and premiere – Roja

Wangfujing Film Festival (Beijing) 
* Special screening – Roja

Indian Film Week (Moscow) 
* Screening in the category of "From the classics to the contemporary" – Roja

==See also== Border
*Dil Se..
*LOC Kargil
*Mission Kashmir Lakshya
*Yahaan

==References==
 

==Further reading==
*  

==External links==
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 