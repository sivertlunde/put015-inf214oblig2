California Suite (film)
{{Infobox film
| name           = California Suite
| image          = CaliforniaSuitePoster.jpg
| image_size     =
| caption        = Original poster by Nick Cardy
| director       = Herbert Ross
| producer       = Ray Stark
| writer         = Neil Simon
| narrator       =
| starring       = Maggie Smith Alan Alda Jane Fonda Walter Matthau Bill Cosby Richard Pryor Elaine May Michael Caine
| music          = Claude Bolling
| cinematography = David M. Walsh
| editing        = Michael A. Stevenson
| studio         = Rastar
| distributor    = Columbia Pictures
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $42,913,571 
}}
 play of the same title. Similar to his earlier Plaza Suite, the film focuses on the dilemmas of guests staying in a suite in a luxury hotel. Maggie Smith won the Academy Award for Best Supporting Actress for her performance in the movie.

==Plot==
In Visitors from New York, Hannah Warren is a Manhattan workaholic who flies to Los Angeles to retrieve her teenaged daughter Jenny after she leaves home to live with her successful screenwriter father Bill. The bickering divorced couple is forced to decide what living arrangements are best for the girl.

In Visitors from London, Diana Barrie is a British actress and a first-time nominee for the Academy Award for Best Actress, an honour that could jumpstart her faltering career, although she knows she doesnt have a chance of winning. She is in deep denial about the true nature of her marriage of convenience to Sidney Cochran, a once-closeted antique dealer who has become increasingly indiscreet about his sexual preference. As she prepares for her moment in the spotlight, her mood fluctuates from hope to panic to despair.

In Visitors from Philadelphia, conservative middle-aged businessman Marvin Michaels awakens to discover a prostitute named Bunny - an unexpected gift from his brother Harry - unconscious in his bed. With his wife Millie on her way up to the suite, he must find a way to conceal all traces of his uncharacteristic indiscretion.
 mixed doubles tennis match.

==Production==
The film was shot on location at The Beverly Hills Hotel, the Dorothy Chandler Pavilion at the Los Angeles Music Center, and along Rodeo Drive.

Diana and Sidneys arrival at the Academy Awards was actually shot during the arrivals for the 50th Academy Awards in April, 1978. This may explain the muted response from a real-life crowd unfamiliar with the names "Diana Barrie" and "Sidney Cochran."

The California-themed paintings seen in the opening credits are by pop artist David Hockney.

While the play featured two actors and two actresses each playing several roles, the film features a different actor for each role.

==Cast==
*Maggie Smith ..... Diana Barrie (*Winner Academy Award)
*Alan Alda ..... Bill Warren
*Jane Fonda ..... Hannah Warren
*Michael Caine ..... Sidney Cochran
*Walter Matthau ..... Marvin Michaels
*Elaine May ..... Millie Michaels
*Herb Edelman ..... Harry Michaels
*Denise Galik ..... Bunny
*Richard Pryor ..... Dr. Chauncey Gump
*Bill Cosby ..... Dr. Willis Panama
*Gloria Gifford ..... Lola Gump
*Sheila Frazier ..... Bettina Panama
*Dana Plato ..... Jenny Warren

==Critical reception==
Vincent Canby of the New York Times called it "the most agreeably realised Simon film in years" and added, "Here is Mr. Simon in top form, under the direction of Herbert Ross, one of the few directors . . . who can cope with the particular demands of material that simultaneously means to be touching and so nonstop clever one sometimes wants to gag him. It all works in California Suite, not only because the material is superior Simon, but also because the writer and the director have assembled a dream cast." 

Variety (magazine)|Variety observed, "Neil Simon and Herbert Ross have gambled in radically altering the successful format of California Suite as it appeared on stage. Instead of four separate playlets, there is now one semi-cohesive narrative revolving around visitors to the Beverly Hills Hotel . . . The technique is less than successful, veering from poignant emotionalism to broad slapstick in sudden shifts." 

Time Out New York described the film as "quick and varied comedy, highly suited to Neil Simons machine-gun gag-writing" and added, "Fonda provides the film with its centre, giving another performance of unnerving sureness. Also on the credit side is a bedroom farce of epic proportions from Matthau and May. The other vignettes are a bit glum." 

Channel 4 said, "Its an expertly crafted slick movie that sets up each of its coconuts and knocks them over with a sure eye, but ultimately its emotional sushi rather than satisfying catharsis." 

==Awards and nominations== Same Time, Next Year for the Golden Globe Award for Best Actress – Motion Picture Musical or Comedy. She was nominated for the BAFTA Award for Best Actress in a Leading Role but lost to Jane Fonda in The China Syndrome.
 Academy Award Midnight Express. He also was nominated for the Writers Guild of America Award for Best Adapted Screenplay.
 George Gaines Heaven Can Wait.
 Coming Home.

==DVD release==
The film was released on DVD on Region 1 DVD on January 2, 2002. It is in anamorphic widescreen format with audio tracks in English and French and subtitles in English, French, Spanish, Portuguese, Chinese, Korean, and Thai. There are no bonus features.

==See also== Plaza Suite
*London Suite (play)

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 