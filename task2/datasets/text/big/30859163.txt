Dig!
{{Infobox film
| name           = Dig!
| image          = Digposter.jpg
| caption        = Theatrical release poster
| director       = Ondi Timoner
| producer       = Ondi Timoner
| writer         = Ondi Timoner
| starring       = Anton Newcombe Courtney Taylor-Taylor
| cinematography = Vasco Nunes David Timoner Ondi Timoner
| editing        = Ondi Timoner Interloper Films Celluloid Dreams
| distributor    = Palm Pictures
| released       =  
| runtime        = 107 min.
| country        = United States
| language       = English
}}

Dig! is a documentary film directed by Ondi Timoner, and produced by Timoner, Vasco Nunes and David Timoner. Compiled from seven years of footage, it contrasts the developing careers and love–hate relationship of the bands The Dandy Warhols and The Brian Jonestown Massacre and the bands respective frontmen Courtney Taylor-Taylor and Anton Newcombe. It won the Documentary Grand Jury Prize at the 2004 Sundance Film Festival.
== Cast ==

; The Brian Jonestown Massacre

* Anton Newcombe
* Joel Gion
* Matt Hollywood Jeffrey Davies Peter Hayes Dean Taylor

; The Dandy Warhols

* Courtney Taylor-Taylor
* Peter Holmström
* Zia McCabe
* Brent DeBoer
* Eric Hedford

; Additional cast

* David LaChapelle
* Genesis P-Orridge – commentator
* Adam Shore – commentator

== Band member reactions ==

Taylor-Taylor, Newcombe and Warhols guitarist Peter Holmstrom have all criticized the film as being unfair in its portrayal of Newcombe and The Brian Jonestown Massacre.        On The Brian Jonestown Massacres official website the film was denounced as reducing several years of hard work to "at best a series of punch-ups and mishaps taken out of context, and at worst bold faced lies and misrepresentation of fact."   "-type storyline. 

The story also leaves one with the impression, by using Courtney as a voice over, that The Brian Jonestown Massacre were no longer a band. There was no update regarding Anton at the time of release.

== Critical reception ==

The film was generally very well-received critically. It currently has an approval rating of 90% on review aggregator website Rotten Tomatoes, based on 69 reviews. 
  gave the film five stars out of five. 

Allmovie, while giving the film a generally positive review, criticized the films emphasis, writing "DIG! isnt as concerned with differences in the groups musical styles (few songs are heard for more than a few bars at a time) as it is with personalities and interpersonal conflict. In this regard, it echoes the purportedly superficial concerns of the fickle industry it depicts, and its not entirely clear whether this is Timoners intent   In the end, the music should matter more than it apparently does." 

PopMatters gave the film a mixed review, commenting that "The film is less effective at conveying the genius of Anton Newcombe than the madness, possibly because the latter only requires a camera and Anton himself" but ultimately called it "fascinating" as a "behind-the-music-scenes glimpse". 

== References ==

 

== External links ==

*  

 
 

 
 
{{succession box
| title= 
| years=2004 The Corporation Why We Fight}}
 

 
 
 
 
 
 
 