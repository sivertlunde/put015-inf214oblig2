The Snake Pit
 
{{Infobox film
| name           = The Snake Pit
| image          = Snakepit1948 62862n.jpg
| producer       = Robert Bassler Anatole Litvak Darryl F. Zanuck
| screenplay     = Millen Brand Arthur Laurents (uncredited) Frank Partos
| based on       =   Mark Stevens Leo Genn Celeste Holm
| director       = Anatole Litvak Alfred Newman
| cinematography = Leo Tover
| editing        = Dorothy Spencer
| distributor    = 20th Century Fox
| released       = November 4, 1948
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         = 
| gross = $4.1 million (US/Canada rentals)  }}
 American drama Mark Stevens, Lee Patrick.   Based on Mary Jane Wards 1946 Autobiographical novel|semi-autobiographical novel of the same name, the film tells the story of a woman who finds herself in an insane asylum and cannot remember how she got there.

The novel was adapted for the screen by Millen Brand, Arthur Laurents (uncredited) and Frank Partos.

==Plot== hears voices out of touch with reality that she doesn’t recognize her husband Robert (Mark Stevens).

Dr. “Kik” (Leo Genn) works with her, and flashbacks show how Virginia and Robert met a few years earlier in Chicago. He worked for a publisher who rejected her writing, and they bumped into each other again in the cafeteria. Occasionally she continued to drop by the cafeteria so they get to know each other.
 shock treatment Leif Erickson) as well as childhood concerns. The film shows her progress and what happens to her along the way.

The mental hospital is organized on a spectrum of "levels." The better a patient gets, the lower level she is able to achieve. Virginia moves to the lowest level (One), but there she encounters Nurse Davis (Helen Craig) who is the only cruel nurse in the hospital. Jealous of Dr. Kiks professional interest in Virginia, and in her eyes excessive concern, Nurse Davis is so severe with Virginia that she goads her into an outburst which results in her being expelled from first level in a straight jacket. 

Despite this setback, Dr Kiks care continues to improve Virginias mental state. Over time, Virginia gains insight and self-understanding, and is able to leave the hospital. The film also depicts the bureaucratic regimentation of the institution, the staff&nbsp;— some brutal and ignorant, some kindhearted&nbsp;— and relationships between patients, from which Virginia learns as much as she does in therapy.

==Cast==
{| border="1" cellpadding="4" cellspacing="0"
|- bgcolor="#CCCCCC"
!  Actor/Actress || Character
|- Olivia de Havilland || Virginia Stuart Cunningham
|- Mark Stevens Mark Stevens || Robert Cunningham
|- Leo Genn || Doctor Mark Kik
|- Celeste Holm || Grace
|- Glenn Langan || Doctor Terry
|- Helen Craig Helen Craig || Nurse Davis
|- Leif Erickson Leif Erickson || Gordon
|- Beulah Bondi || Mrs. Greer
|- Lee Patrick Lee Patrick || Asylum Inmate
|- Howard Freeman || Dr. Curtis
|- Natalie Schafer || Mrs. Stuart
|- Ruth Donnelly || Ruth
|- Katherine Locke || Margaret
|- Celia Lovsky || Gertrude
|- Frank Conroy Frank Conroy || Dr. Jonathan Gifford
|- Minna Gombell || Miss Hart
|- Betsy Blair || Hester
|}

==Production==
Gene Tierney was the first choice to play Virginia Stuart Cunningham, but was replaced by Olivia de Havilland when Tierney became pregnant.
 electric shock treatments. When permitted, she sat in on long individual therapy sessions. She attended social functions, including dinners and dances with the patients. In fact, when, after the films release, columnist Florabel Muir questioned in print whether any mental institution actually "allowed contact dances among violent inmates," she was surprised by a telephone call from de Havilland, who assured her she had attended several such dances herself.  Much of the film was filmed in the Camarillo State Mental Hospital in California.

==Reception==
The critics were generally kind, with   wrote: "Its seething quality gets inside of you." On the other hand, Herman F. Weinberg, a noted psychiatrist, was unimpressed. He wrote, "A film of superficial veracity that requires a bigger man than Litvak; a good film with bad things in it."   

The film has come under fire from some womens rights authors for a seeming misportrayal of Virginias difficulties and the implication that accepting a subservient role as a wife and mother is part of her "cure".  Other film analysts view it as successful in conveying Wards view of the uncertainties of post-WWII life and womens roles. 

===Censorship=== British censor required a foreword added to the movie that explained to the audience that everyone in the movie was an actor&nbsp;— and that conditions in British hospitals were unlike those portrayed in the film. 

==Awards== Academy Award Best Sound Best Actress Best Director, Best Music, Best Picture Best Writing, Screenplay.   

The film also won the International Prize at the Venice Film Festival in 1949, where it was cited for "a daring inquiry in a clinical case dramatically performed." 

==Impact== Daily Variety wrote "Wisconsin is the seventh state to institute reforms in its mental hospitals as a result of The Snake Pit. Clooney, p. 144 

Publicity releases from 20th Century Fox claimed that twenty-six of the then forty-eight states had enacted reform legislation because of the movie. This is a very difficult claim to verify because few of the bills introduced, regulations changed or funding increases implemented specifically mentioned The Snake Pit as a motivating factor. 

==Other adaptations==
The Snake Pit was dramatized as an hour-long radio play on the April 10, 1950 broadcast of Lux Radio Theater, with de Havilland reprising her film role.

==See also==
*Mental illness in films

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 