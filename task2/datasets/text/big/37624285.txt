Unsettled Land
{{Infobox film
| name           = Unsettled Land
| image          = Unsettled Land Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film poster
| director       = Uri Barbash
| producer       = Ludi Boeken Katriel Schori Ben Elkerbout
| writer         = 
| screenplay     = Benny Barwash Eran Preis
| story          = 
| based on       = 
| narrator       = 
| starring       = Kelly McGillis John Shea Christine Boisson
| music          = Misha Segal
| cinematography = Amnon Salomon
| editing        = Tova Asher
| studio         = Belbo Films
| distributor    = GSO (France) Hemdale Film (US) J. L. Bowbank (Canada)
| released       =  
| runtime        = 109 minutes
| country        = Israel Netherlands
| language       = Hebrew English
| budget         = $2,000,000 Judd Neeman, "Israeli Cinema," in O. Leaman, ed., Companion Encyclopedia of Middle Eastern and North African Film (Routledge, 2001), p. 270. 
| gross          = 
}} Israeli drama directed by Uri Barbash.

The film premiered in the International Competition at the Tokyo International Film Festival and was selected to be shown at the Israel Film Festival in New York.  It also won awards for Best Cinematography (Amnon Salomon) and Best Art Direction (Eitan Levy) at the Israel Film Center. 

==Plot== utopian vision and reality.

As Miri Talmon has noted, the film "makes a clear intergenerational connection between the ‘pioneers’ and the shattering of the dream, which is the experience that the audience and the individual filmgoer faced at the end of the eighties.” 

==Cast==
*Kelly McGillis as Anda
*John Shea as Marcus
*Christine Boisson as Sima
*Arnon Zadok as Amnon Robert Pollock as Yulek
*Sinai Peter as Daniel
*Neta Moran as Temma
*Amos Lavi as Muhammed
*Boris Rimmer as Avrum
*Alan Bergreen as Natan
*Martha Arden as Rosa
*Rachel Amran as Maha
*Avi Gouetta as Salim Assistant 2
*Yossi Ashdot as Shepard

==Critical reception==
Despite its rather large budget and Hollywood stars, the film did poorly at the box office and garnered poor reviews. 

==References==
 

==External links==
*  
*  


 
 
 
 