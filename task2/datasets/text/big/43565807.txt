Driving with Selvi
{{Infobox film
| name           = Driving with Selvi
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Elisa Paloschi
| producer       = Elisa Paloschi
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Ken Myhr
| cinematography = Elisa Paloschi
| editing        = David Kazala
| studio         = Eyesfull
| distributor    = 
| released       =  
| runtime        = 78 minutes
| country        = Canada Kannada
| budget         = 
| gross          = 
}}
 taxi driver, child marriage. The film was directed by Elisa Paloschi, who also acted as producer for the project.

==Background==
Paloschi met Selvi in 2004 in India when she was tourist in Mysore, India. After Paloschi volunteered with Odanadi, an organization dedicated to rehabilitate people affected by human trafficking, they asked her to shoot a short film for their organization. Paloschi became "enamored" with Selvi, and ended up following her over the next 10 years to film the documentary.   

Chicken & Egg Pictures had financially backed Paloschi with her project, as had GoodPitch2 India, a subsidiary of the BRITDOC Foundation.          Funding for post-production costs had been solicited through Indiegogo, a crowdfunding website.      

==Plot==
The official DWS website gives the following summation of the film: 

 "Driving with Selvi follows the inspiring story of Selvi, South India’s first female taxi driver, as she takes control of both the steering wheel and her future during a courageous transformation from abused child bride to empowered working mother.
 violent child marriage and the victim of a life set against her. Over the 9 years filmed, we see a remarkable transformation as Selvi finds her voice and refuses to surrender to expectations – learning to drive, starting her own taxi company and working on the front lines of reproductive health education.

Instead of adhering to the patriarchal codes that oppress and restrict women, Selvi’s bold determination defies and challenges them. Will she hold steadfast on her road to economic empowerment? Will she remarry? After being unwanted and abused by her own mother, will she be able to break the vicious cycle of devaluing girls so prevalent in her society?"    

==Reception==
Paloschi has stated "I didn’t set out to make films that would send a message, but I gradually realised that I was drawn to people who had a spark in their soul and a powerful impact on those around them."   

Indie Wire called DWS an "inspiring story".    The Toronto Film Scene echoed that it was inspirational, also writing "Her growth and transformation is an inspiring one as she changes from an eighteen-year-old soft-spoken runaway, to a woman finding her voice and refusing to surrender to the limitations that society attempts to force upon her."   

Paloschi was included as a delegate because of her work on DWS for the Canadian-Indian Copro Treaty signing, an international co production treaty for film between the two countries. His Excellency David Johnston headed the delegation.      

==References==
 

==External links==
Driving with Selvi   
Driving with Selvi on   
Driving with Selvi on   

 
 
 
 
 