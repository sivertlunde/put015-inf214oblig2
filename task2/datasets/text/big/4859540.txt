Lord of the Beans
 
{{Infobox film
| name           = Lord of the Beans
| image          = Lord of the beans.jpg
| alt            = 
| caption        = DVD cover
| director       = Mike Nawrocki
| producer       = David Pitts
| writer         = Phil Vischer
| starring       = Phil Vischer Mike Nawrocki Lisa Vischer Joe Spadaford Tim Hodge Charlotte Jackson Ally Nawrocki
| music          = Kurt Heinecke
| editing        = J. Chris Wall
| studio         = Big Idea Productions
| distributor    = Word Entertainment
| released       =   (christian)  
| runtime        = 52 minutes
| country        = United States
| language       = English
| budget         = $250,000
}} twentieth episode in the VeggieTales animated series, and the first since An Easter Carol to feature only one story. Subtitled "A Lesson in Using Your Gifts", its goal is to teach viewers how they can use their talents to bring joy to others rather than merely bringing glory or satisfaction to themselves. Its also the first video to have the Silly Song after the 30-minute mark of the video. This video has also been the subject of a clue on "Jeopardy!" ("What is "Lord Of The Beans"?") Also, there was no directors commentary recorded for this video. 

The story used to illustrate the lesson is an overt parody of J. R. R. Tolkiens The Lord of the Rings. The parody takes character names and places directly from the source and changes them to fit the VeggieTales context (noted below). In spite of this, the plot takes many unexpected turns, and recognizable scenes are freely reinterpreted to create humor or underscore the videos message.

==Plot==
 Randalf in Shire for Billboy Baggypants a strange bean to produce a birthday cake. Returning to his home, he finds Randalf waiting in his living room. Randalf remarks on Billboys impressive height, his fine clothing, and his luxuriously appointed home, knowing they have come from the bean, and warns his friend of using such things lightly. Billboy concedes that the bean has given him almost everything he could want. He then announces that he is leaving the Shire and bequeathing everything he owns, including the bean, to his nephew Frodo Baggins|Toto.

Waiting for Toto, Randalf relates that Billboy has departed and draws his attention to the bean. Toto is curious as to why he would want a bean and Randalf describes the origin of a magical bean that could produce clothing, consumables, and small kitchen appliances, and also change your appearance. After verifying the beans authenticity from an inscription left after warming it in the fire, Toto is unsure about accepting such a gift and tries to pass it off to Randalf. However, Randalf explains that one cannot choose his gifts and must determine for himself how they should best be used.
 Elf Legolas|Leg-O-Lamb; Dwarf Gimli Mountains of Elders have blowing raspberries. This gets them sentenced to detention on a platform forever until an Eagle "saves" them and they escape.
 Blue Gate, Land of Woe. After opening the door, they realize that Toto is the only one small enough to fit through and he proceeds alone. However, the remainder of the group soon learns that a group of Uruk-hai|Sporks, minions of the evil Saruman|Scaryman, are after Toto in order to seize the bean for their master. The fellowship then goes in pursuit.
 Flobbit like Keebler Elf, the well, bringing water back to the Land of Woe and restoring it to its fertile and beautiful state.

==Songs==

This episode includes many songs - some of which are listed below:
*"VeggieTales Theme Song" - Bob the Tomato, Mom Asparagus, Dad Asparagus, Junior Asparagus, Larry the Cucumber
* "A Little More of This" - Archibald Asparagus
* "The Legend of the Bean" - Mr. Nezzer and Junior Asparagus
* "Will I Know What Its For?" - Junior Asparagus
* "Ive Got a New Umbrella" - Umbrella Guy
* "My Baby Elf" - Larry the Cucumber and Miss Achmetha
* "I Finally Know What Its For" - Junior Asparagus, Archibald Asparagus, Mr. Nezzer, Larry the Cucumber, Mr. Lunt, Pa Grape, Jimmy & Jerry Gourd, Townspeople and the Sporks
*"What We Have Learned" - The singers (however the sporks stole the song)
*"Its About Love" - Wynonna Judd

==References==
 

==External links==
*  
*   from families.com

 
 
 
 
 
 