Welcome Zindagi
{{Infobox film
| name = Welcome Zindagi
| image = Welcome_Zindagi.jpg
| caption = Tittle Look Poster 
| director = Umesh Ghadge
| starring = Swapnil Joshi , Amruta Khanvilkar
| producer = Ajit Satam, Sanjay Ahluwalia & Bibhas Chhaya
| writer   = Ganesh Matkari, Srijit Mukherjee
| released =  
| runtime  = 135 minutes
| language = Marathi
| country = India
| Co-Producer = Sanjiv Ahluwalia
| Associate Producer = Firoz Khan Ponkh
| Story =  Srijit Mukherjee
| Screenplay & Dialogues = Ganesh Matkari
| Director of Photography = Prasad Bhende
| Production Designer = Minar Tamhane
| Art Director = Trupti Tamhane, Vijay Kadali
| Costume Designer = Ashley Rebello
| Editor = Pranav Mistry
| Music = Amit Raj, Pankaj Padghan, Soumil & Siddarth
| Sound Designers = Jitendra Singh
| Choreographers = Rajesh Bidwe, Santosh Palwankar
| Publicity Design = Marching Ants
| D.I. = Prime Focus
| Visual Promotions = Aditya Joshi
}}

Welcome Zindagi (Marathi: वेलकम जिंदगी) is a Marathi language film.  Welcome Zindagi is a dark comedy about a girl who wants to end it all, and a boy who will change her perception of life. The movie is a dark comedy about life and death. 

==Plot==
Meera, a young media professional, finds herself cornered every step of the way, as she tries to live a normal life. She feels ignored by her father, betrayed by her fiancé, faces trouble at work. The only escape route she can think of, is taking her own life with a massive dose of sleeping pills. 

Enters Anand Prabhu, who stops her at a crucial moment, although he has no plans of making her change her mind. His advice instead, is to go about it systematically. Anand is the founder of ‘Happy Ending Society’, an organization that believes that every person has a right to decide his , or her own fate. Anand gladly offers to make Meera an expert in various ways of ending her life by enrolling her in a 3-day suicide camp, organized by Happy Ending. 

While Meera is busy making a choice between life and death surrounded by the like-minded students and over enthusiastic staff, Meera’s father Dr Rajwade leaves no stone unturned to find his estranged daughter.

What happens next? Will Meera finally succeed in carrying out her wish? Does Anand have an ulterior motive in keeping Meera at the Suicide camp? Will Meera reunite with her father? And what is the truth behind ‘Happy Ending Society’? 

Walking a thin line between comedy and drama, Welcome Zindagi delivers a poignant message in the garb of entertainment. 
==Cast==
* Swapnil Joshi
* Amruta Khanvilkar  
* Mohan Agashe
* Satish Alekar
* Prashant Damle
* Bharti Achrekar
* Rajeshwari Sachdev
* Vivek Lagoo
* Pushkar Shrotri
* Murli Sharma
* Urmila Kanitkar-Kothare
* Jayant Wadkar  

& in a Special Appearance Mahesh Manjrekar.

==Crew==
* Producer : Ajit Satam, Sanjay Ahluwalia & Bibhas Chhaya
* Co-Producer : Sanjiv Ahluwalia
* Associate Producer : Firoz Khan Ponkh
* Director : Umesh Ghadge
* Story : Srijit Mukherjee
* Screenplay & Dialogues : Ganesh Matkari
* Director of Photography : Prasad Bhende
* Production Designer : Minar Tamhane
* Art Director : Trupti Tamhane, Vijay Kadali
* Costume Designer : Ashley Rebello
* Editor : Pranav Mistry
* Music : Amit Raj, Pankaj Padghan, Soumil & Siddarth
* Sound Designers : Jitendra Singh
* Choreographers : Rajesh Bidwe, Santosh Palwankar
* Publicity Design : Marching Ants

==References==
 

==External links==
*  
*  

 
 
 

 