I Know Who Killed Me
{{Infobox film
| name = I Know Who Killed Me
| image = Who killed me post.jpg
| caption = Theatrical release poster
| director = Chris Sivertson David Grace
| writer = Jeff Hammond
| starring = Lindsay Lohan Julia Ormond Neal McDonough Brian Geraghty Garcelle Beauvais 
| music = Joel McNeely
| cinematography = John R. Leonetti
| editing = Lawrence Jordan
| studio = 360 Pictures
| distributor = TriStar Pictures
| released =  
| runtime = 105 minutes
| country = United States
| language = English
| budget = $12 million
| gross = $9,669,758
}} thriller film The Parent Trap.

The films story revolves around a student who was abducted and tortured by a sadistic serial killer. She manages to make it out alive but after she regains consciousness in the hospital she insists that her identity is that of another woman.
 Jack and Worst Actress Worst Screen Couple for both characters she portrayed.

==Plot==
The quiet suburb of New Salem is being terrorized by a serial killer who abducts and tortures young women, holding them captive for weeks before murdering them. Aubrey Fleming (Lindsay Lohan), a pianist and aspiring writer, appears to be his latest victim when she disappears during a night out with her friends. She is later seen bound and gagged on an operating table as her hands are exposed to dry ice. As the days tick by, the special FBI Task Force convened to track the killer begins to lose hope of finding her before its too late.

Late one night, a driver discovers a young woman by the side of a deserted road, disheveled and critically injured. The girl is rushed to the hospital, where Aubrey’s distraught parents, Susan (Julia Ormond) and Daniel (Neal McDonough), wait by her side as she slips in and out of consciousness. When she is finally able to speak, she shocks everyone by claiming to be a down-on-her luck stripper named Dakota Moss, who has never heard of Aubrey Fleming. Convinced Aubrey is suffering from post traumatic stress disorder, her doctors, parents, and law enforcement officials can only wait for rest and therapy to restore her memory. But after returning to her parents’ suburban home, she continues to insist she is not who they think she is, despite bearing bizarre wounds identical to those of the serial killers previous victims, which include her hand and half of her leg cut off.

The FBI agents are further mystified when they search Aubrey’s laptop and discover a short story about a girl with an alter ego named Dakota. When Dakota begins to suspect she may be Aubreys identical twin sister, Susan shows her a video of her pregnancy ultrasound clearly revealing there was only one fetus in her womb. Confused and terrified, Dakota starts seeing visions of a menacing figure slowly butchering his captive. Convinced time is running out both for Aubrey and herself, Dakota confronts Daniel with a shocking truth that leads them on a frantic hunt for the killer.

Aubrey and Dakota are indeed twins, born to Virginia Sue Moss, a crack addict. Moss gave birth to them the same time the Flemings had their own child, who died in the incubator. Daniel Fleming quietly raises one as his own daughter, paying Virginia over the years by mail. Dakota finds the envelopes and attempts to find her sister, when she suffers sympathetic resonance from her twins wounds, and is found by the highway. It turns out the two are stigmatic twins, with a psychic connection that lets them share pain, communicate, and even share experiences, which explains some of Aubreys stories.

After investigating the grave of Aubreys recently murdered friend, Jennifer Toland (Stacy Lynn Gabel), Dakota finds a blue ribbon from a piano competition, with a message from Jennifers (and Aubreys) piano teacher, Douglas Norquist (Thomas Tofel). Dakota realizes Norquist murdered Jennifer and abducted Aubrey after they expressed intentions to quit their piano lessons, taking off their fingers, arm, and a leg in a twisted act of retribution. Dakota and Daniel confront Norquist, and Daniel dies in the process, but Dakota cuts off Norquists hand and delivers a fatal blow to his neck. She then finds Aubrey where Norquist buried her alive and frees her. The movie ends with Aubrey and Dakota lying together on the ground, looking out into the night.

==Cast==
* Lindsay Lohan as Aubrey Fleming/Dakota Moss
* Julia Ormond as Susan Fleming
* Neal McDonough as Daniel Fleming
* Brian Geraghty as Jerrod Pointer Garcelle Beauvais-Nilon as Agent Julie Bascome
* Spencer Garrett as Agent Phil Lazarus
* Kenya Moore as Jazmin
* Gregory Itzin as Dr. Grehg Jameson
* Thomas Tofel as Douglas Norquist
* Rodney Rowland as Kenny Scaife
* Michael Adler as Dr. Alex Dupree
* Paula Marshall as Marnie Toland
* Brian McNamara as Fred Toland
* Stacy Lynn Gabel (uncredited) as Jennifer Toland
* Michael Papajohn as Jacob K./Joseph K.
* Art Bell as himself
* Jessica Rose as Marcia

==Production==
 
Lindsay Lohan actually took pole-dancing lessons to prepare for her role as a stripper.

Filming dates took place between December 2006 and March 2007. Principal photography was mostly held in California. Filming was stalled in January 2007 when Lohan entered rehab, but was resumed in February. Lohans DUI arrest in late July 2007 prevented her from doing promotion for this movie.

==Reception==

===Box office===
The film grossed $9 million worldwide on a $12 million budget. 

===Critical reception===
The film was not screened in advance for critics. I Know Who Killed Me has received extremely negative reviews from critics.   , which indicates "Overwhelming dislike". Razzie nominations, Battlefield Earth Jack and Jill won ten awards.

Despite this the film did garner some positive reviews. Fangoria (magazine)|Fangoria praises the films imaginative use of color, saying " he director and his visual team bathe the film in deep blues and reds, a welcome departure from the dirty green, sodium-lit palette of similarly themed horror fare, and the end result is simply a beautiful, eye-popping visual treat, so stylized that one cant help recalling Dario Argento|Argentos approach to Suspiria."  The Radio Times also alluded to the director "recalling the style of Dario Argento" in a "twisty, perversely fascinating psycho thriller."  The horror-movie website Bloody Disgusting|BloodyDisgusting.com gave the film a glowing review and suggested that, "Lohans continual issues with drugs/alcohol/DUI’s/rehab/on-set bitchiness" were part of a "whirlwind of media frenzy" that was unnecessary and "irrelevant to the movie". The film itself was "a more-than-pleasant surprise, well-filmed, well-acted, especially by Lohan herself, and a surprisingly intriguing and gruesome little thriller."  Boston Globe critic Ty Burr compared the film favorably to Brian de Palmas Sisters (1973 film)|Sisters and Body Double, as well as the works of David Lynch. 

==Home media== Region 2 DVD was released January 28, 2008 with different cover art showing a close-up of Lohan, in red, doing her pole-dance at the strip club. 

==Soundtrack==
{{Infobox album  
| Name        = I Know Who Killed Me
| Type        = Film score
| Artist      = Joel McNeely
| Cover       = Music fro the film I Know who killed me.JPG
| Released    = July 31, 2007
| Recorded    = 2007
| Genre       = Film soundtrack
| Length      = 1:35:09
| Label       = Varèse Sarabande
| Producer    =
}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =     
| rev2      = 
| rev2Score = 
}} score for I Know Who Killed Me, composed by Joel McNeely, was released on July 24, 2007.  Despite the films critical and commercial failure, the score itself (which had drawn comparisons to the television mystery scores by Billy Goldenberg) received almost unanimously positive reviews from film music critics, with James Southall of Movie Wave calling it an "unexpectedly classy score seems to go beyond the call of duty"  and Clark Douglas of Movie Music UK rating it 5 stars and calling it "one of the years best scores, a must-have for those who are willing to take a trip into a deep, dark, and sometimes terrifying musical world". 

The score was subsequently nominated as Best Original Score for a Horror/Thriller Film by the International Film Music Critics Association. 

# "Prelude for a Madman"
# "Duality"
# "Fairytale Theme"
# "A Daughter Is Dead"
# "End of Innocence/Aubrey Is Gone"
# "A Mothers Grief"
# "Search for Aubrey"
# "The Bus Stop"
# "Spontaneous Bleed"
# "Going Home"
# "Jennifers Room"
# "Some People Get Cut"
# "Investigating Stigmata"
# "The Mirror"
# "The Graveyard"
# "I Know Who Killed Me"
# "The House"
# "Dad Dies"
# "Death of Norquist"
# "Prelude/Reunited"
# "Valse Brillante, Op. 34, No. 2 in A Minor"

===Unreleased tracks===
The following songs appeared in the movie, but didnt appear on the soundtrack: Trans Am - "Obscene Strategies" VietNam - "Step On Inside"
* The Sword - "Freya (song)|Freya"
* Dead Meadow - "Dusty Nothing" Architecture in Helsinki - "Maybe You Can Owe Me"
* Out Hud - "How Long"
* Awesome Color - "Hat Energy"
* Melvins - "A History of Bad Men"

==Awards and nominations==
At the 28th Golden Raspberry Awards ceremony, the film won eight awards from nine nominations, was the big winner of the evening, receiving eight awards for a new Razzie record.
{| class="wikitable"
|-
! Year !! Ceremony !! Category !! Nominee !! Result
|- 2008
|rowspan="9"|28th Golden Raspberry Awards Worst Picture
|
| rowspan=8  
|- Worst Actress  (as Aubrey Fleming)  Lindsay Lohan
|-
| Worst Actress  (as Dakota Moss) 
|- Worst Screen Couple 
|- Worst Remake or Rip-off  (of Hostel (2005 film)|Hostel, Saw (2004 film)|Saw and The Patty Duke Show) 
|
|- Worst Director
| Chris Sivertson
|- Worst Screenplay
| Jeff Hammond
|-
| Worst Excuse for a Horror Movie
|
|- Worst Supporting Actress
| Julia Ormond
| rowspan=3  
|- 2010
| 30th Golden Raspberry Awards Just My Luck) 
| Lindsay Lohan
|-
| Worst Picture of the Decade
|
|}

==References==
 

==External links==
*  
*  
*  
*  

   

{{Succession box
| title=Golden Raspberry Award for Worst Picture
| years=28th Golden Raspberry Awards
| before=Basic Instinct 2
| after=The Love Guru
}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 