Déclassée
{{infobox film
| name           = Déclassée
| image          = Declassee.jpg
| caption        = 1925 lobby poster
| director       = Robert G. Vignola First National
| based on       =  
| writer         = Bradley King Charles E. Whittaker
| starring       = Corinne Griffith
| music          = Leo Fall
| cinematography = Tony Gaudio
| editing        = Cyril Gardner
| distributor    = First National Pictures
| released       =  
| runtime        = 80 mins.
| country        = United States
| language       = Silent  English intertitles
}}
 silent drama film of manners produced and released by First National Pictures in association with Corinne Griffith as executive producer. Griffith also stars in the production directed by Robert G. Vignola which is based on the 1919 play by Zoë Akins that starred Ethel Barrymore. A print of this film resides in the British Film Institute with a trailer surviving at the Library of Congress.   
 talkie in 1929 called Her Private Life starring Billie Dove. This latter version is now considered a lost film.

==Cast==
*Corinne Griffith - Lady Helen Haden
*Lloyd Hughes - Ned Thayer
*Clive Brook - Rudolph Solomon
*Rockliffe Fellowes - Sir Bruce Haden
*Lilyan Tashman - Mrs. Leslie
*Hedda Hopper - Lady Wildering
*Bertram Johns - Sir Emmett Wildering
*Gale Henry - Timmins
*Louise Fazenda - Mrs. Walton
*Eddie Lyons - Mr. Walton
*Mario Carillo - Hotel Manager
*Paul Weigel - Henri
*Clark Gable - extra

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 