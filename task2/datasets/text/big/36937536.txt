Vathikuchi
 
 
{{Infobox film
| name           = Vathikuchi
| image          = Vathikuchi.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Kinslin
| producer       = A. R. Murugadoss
| writer         = Kinslin
| starring       =  
| music          = Ghibran
| cinematography = R. B. Gurudev
| editing        = Praveen K. L. N. B. Srikanth
| studio         = AR Murugadoss | A.R Murugadoss Productions
| distributor    = Fox Star Studios
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          =
}}
 Tamil urban Anjali in Jagan appear in other pivotal roles.  The film, which has music composed by M. Ghibran, opened in March 2013 to a positive response from critics and performed well at the box office. 

==Cast==
*Dhileban as Sakthi Anjali as Meena
*Sampath Raj as Benny
*Jayaprakash as Gangariya
*Jagan as Vanaraj
*Sathish
*Akhil Kumar as Golicha
*Saranya Ponvannan as Sakthis mother
*Pattimandram Raja as Sakthis father
*V.C.R Vatsan as Praveen
*Ravi Mariya Sriranjini as Meenas mother
*Bala Singh Rajashree
*Angana Roy

==Production==
The film was first announced in January 2012, when it was revealed that Murugadosss brother Dhileepan "The Spark" would make his debut as an actor in a venture to be co-produced by Fox Star Studios.  Murugadoss chose the films title as reference to the song Vathikuchi Pathikadhuda... from his first directorial venture Dheena. 
 Anjali was signed up by producer A R Murugadoss for his second successive production.  Impressed with M Ghibrans score in Vaagai Sooda Vaa, both Murugadoss and Kinslin decided to sign him on for the film.  

==Release==
The satellite rights of the film were sold to STAR Vijay. 

===Critical reception===
Vathikuchi received mixed to positive reviews from critics. Vivek Ramz from in.com rated it 3 out of 5 and stated that Vathikuchi is more brawny than brainy. He added that Director Kinslin shows promise in his debut film with different treatment for this somewhat cliched story. His way of storytelling is realistic at times with very grounded level approach towards a common man issue. The problem with the film is that he has introduced too many characters and made few simple situations more complex which works at times and at other times backfired. Finally concluding that its not a bad film at all and can be watched once!  Tikkview.com gives 3/5 for Vathikuchi. and their review says that "Usual story line.We may seen this story in many movies.But narration style is really good."  Indiaglitz says "Economic ride jarred by minor speedbrakers, yet reaches the destination on time!".  Behindwood remained neutral with 2.5/5 saying "Vathikuchi has equal moments that will interest you as well as make you restless in your seat" 

==Soundtrack==
{{Infobox album|  
| Name = Vathikuchi
| Type = Soundtrack
| Artist = M Ghibran
| Cover  = Vathikuchi soundtrack cover.jpg
| Released =   Feature film soundtrack
| Length =  Tamil
| Label = Sa Re Ga Ma
| Producer = M Ghibran
| Reviews =
| Last album =Vaagai Sooda Vaa (2011)
| This album =Vathikuchi (2013)
| Next album =Kutti Puli (2013)
}}
The films score and soundtrack are composed by M Ghibran. The soundtrack was released on 13 February 2013, to positive reviews.  

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length =  
| lyrics_credits = yes
| title1 = Kuru Kuru
| lyrics1 = Na. Muthukumar
| extra1 =  Sundar Narayana Rao
| length1 = 04:30
| title2 = Amma wake me
| lyrics2 = Pa. Vijay & Shabir
| extra2 =  Shruthi Ravi, Anita & Shabir
| length2 = 04:28
| title3 = Kanna Kanna
| lyrics3 = Arivumathi
| extra3 = Sundar Narayana Rao
| length3 = 04:54
| title4 = Ari Unnai
| lyrics4 = Yugabharathi
| extra4 =  Shabir & Saisharan  
| length4 =  04:11
| title5 = Theme Music
| lyrics5 = M Ghibran
| extra5 =  M Ghibran
| length5 = 03:45
}}

Indiaglitz.com gave 4 out of 5 and wrote that, "Even if this album is not a sensation, everybody is bound to carry the repertoire to their shower, and hum these numbers all through the day, for these songs, certainly leave a lasting impression. Four voices, four lyricists, few strings, some beats and keys, and one young man behind them all - this is yet another musical magic from Ghibran" 

==References==
 

==External links==
*  

 

 
 
 
 