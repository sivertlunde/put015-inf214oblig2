Tillie's Punctured Romance (1928 film)
 
{{Infobox film
| name           = Tillies Punctured Romance
| image          = Poster - Tillies Punctured Romance (1928) 01.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = A. Edward Sutherland
| producer       = Al Christie(aka Christie Film Company)
| writer         = Monte Brice Keene Thompson
| narrator       =  Tom Kennedy Babe London
| music          = 
| cinematography = Charles P. Boyle William Wheeler
| editing        = Arthur Huffsmith	 	 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 57 minutes
| country        = United States English
| budget         =
}} 1914 Charles Chaplin film aside from sharing the same title. However, Chester Conklin and Mack Swain appear in both films.

==Cast==
W.C. Fields	 ... 	Ringmaster 
	Louise Fazenda	... 	Tillie, a Runaway 
	Chester Conklin	... 	Circus Owner 
	Mack Swain	... 	Tillies Father 
	Doris Hill	... 	Heroine 
	Grant Withers	... 	Hero  Tom Kennedy	... 	Property Man 
	Babe London	... 	Strong Woman 
	Billy Platt	... 	Midget (billed as William Platt) 
	Michael Raffetto	... 	 (uncredited)

== External links ==
*  

 

 
 
 
 
 
 
 
 


 