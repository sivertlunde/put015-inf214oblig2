Paathshala (2014 film)
{{Infobox film
| name           = Paathshaala
| image          = PaatshalaTelugu.jpg
| alt            = 
| caption        =  Mahi V. Raghav
| producer       = Rakesh Mahankali Pavan Kumar Reddy Mahi V. Raghav
| starring       = Nandu Shashank Sirisha Anu Priya
| music          =Rahul Raj 
| cinematography = Sudheer Surendran
| editing        = Shravan
| studio         = Moonwater Pictures.
| distributor    = 
| released       =  
| runtime        = 
| country        = India Telugu
| budget         = 
| gross          = 
}}
 Telugu film Mahi V. Raghav.  The original score & soundtrack of the movie are composed by Rahul Raj.

The movie released on 10 October 2014 and met with predominantly positive reviews.

==Pre-production and Casting Call==

Paathshala successfully marks an innovative edge, in the Telugu film industry, in terms of selection procedure wherein the cast and crew would be finalised through the medium of social networking site, Facebook. The contest hosted on Facebook managed to get more than 15,000 followers within seven days of the launch with 10,000 applicants. 

==Music==
All music is composed, arranged and produced by Rahul Raj. The soundtrack CD featuring a total of five songs and twelve background score excerpts released on July 28, 2014.

The songs were received well,   but it was the background score that was unanimously praised by all reviewers, as a major plus point, crediting it to have elevated the movie to a different level.           

==Soundtrack==
{{Infobox album <!-- See
Wikipedia:WikiProject_Albums -->
| Name       = Paathshala 
| Type       = soundtrack
| Artist     = Rahul Raj
| Cover      =
| Alt        =
| Released   =   
| Recorded   =
| Genre      =
| Length     =  
| Label      =
| Producer   = Rahul Raj
| Last album = Kadavul Paathi Mirugam Paathi  (2014)
| This album = Paathshala  (2014)
| Next album = Fireman (2015 Film)|Fireman  2015
}}  arranged and produced by Rahul Raj.
 
Original songs
{{Track listing
| extra_column = Performer(s)
| title1 = Friendship Anthem| extra1 =Sooraj Santhosh, Elvis Don Raja
| title2 = Merise Merise | extra2 =  Rahul Raj
| title3 = Sooryodhayam | extra3 =  Najim Arshad
| title4 = Swecha Varsham (Freedom Song) | extra4 =  Rahul Raj
| title5 = Shoonyamai  | extra5 =  Nikhil Mathew
}}
 
Original background score
{{Track listing
| extra_column = Performer(s)
| title1 = The Journey | extra1 = Rahul Raj
| title2 = Energy of Paathshala | extra2 =  Rahul Raj
| title3 = End of Love | extra3 = Rahul Raj feat. Francis (Solo Violin), Josy (Woodwinds)
| title4 = Love Letter | extra4 = Rahul Raj, Abhirami Suresh, Najim Arshad
| title5 = Yela Yela | extra5 = Rahul Raj, Shreemani (Lyrics)
| title6 = Salma Warangal | extra6 =  Rahul Raj
| title7 = Fatboy Dance| extra7 = Rahul Raj, Shreemani (Lyrics)
| title8 = Love Blooms| extra8 = Rahul Raj
| title9 = Aadi in Despair | extra9 = Rahul Raj
| title10 = Childhood Love | extra10 = Rahul Raj
| title11 = Route to Godavari | extra11 = Rahul Raj
| title12 = Temple Violins | extra12 = Rahul Raj
}}

==Film Release and Reception==
The movie released on 10 October 2014 and met with predominantly positive reviews. A review on International Business Times quoted "Audience Call it New-Age Tollywood Movie. Raghavs interesting concept, lead actors performances, Rahul Rajs music, Sudheer Surendrans cinematography and its exotic locales are the main highlights of the film, according to viewers.".  123telugu.com said "First half of the film is entertaining, and showcases the fun elements in student life. Locations showcased and production values in the film are stunning. One dance episode featuring a poor kid, has been executed quite emotionally. Paathshala is also a brilliant film technically. Camerawork is quite stunning, as each and every visual looks top notch. Music is decent, and goes with the mood of the film. But it is Rahul Raj’s absorbing background score, that takes the film to a decent level. Dialogues are written in a simple way, and will connect to the youth well." 
 Mani Ratnams Hrishikesh Mukherjees Anand (1971 film)|Anand and Kasinathuni Viswanath|K. Viswanaths Shankarabharanam.

== References ==
 

 
 
 