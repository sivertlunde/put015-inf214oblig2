Heaven Sword and Dragon Sabre (1978 film)
 
 
{{Infobox film name = Heaven Sword and Dragon Sabre image = Heaven Sword and Dragon Sabre (1978 film).jpg caption = DVD cover art traditional = 倚天屠龍記 simplified = 倚天屠龙记 pinyin = Yǐ Tiān Tú Lóng Jì}} director = Chor Yuen producer = Run Run Shaw story = Louis Cha screenplay = Chor Yuen music = Joseph Koo Wong Jim Frankie Chan cinematography = Wong Chit editing = Chiang Hsing-lung Yu Siu-fung studio = Shaw Brothers Studio distributor = released =   runtime = 101 minutes (Part 1) 98 minutes (Part 2) country = Hong Kong language = Mandarin budget = gross =
}} Louis Chas novel The Heaven Sword and Dragon Saber.

==Cast==
* Derek Yee as Zhang Wuji
* Ching Li as Zhao Min
* Candice Yu as Zhou Zhiruo
* Cheng Lai-fong as Xiaozhao
* Wen Hsueh-erh as Yin Li / Zhuer
* Wong Yung as Yang Xiao
* Ngai Fei as Yin Liting
* Lau Wai-ling as Ji Xiaofu
* Karen Chan as Yang Buhui
* Lo Lieh as Xie Xun
* Tin Ching as Cheng Kun / Yuanzhen
* Cheung Ying as Zhang Sanfeng
* Wang Lai as Miejue
* Teresa Ha as Daiqisi
* Cheng Miu as Yin Tianzheng
* Norman Tsui as Wei Yixiao
* Wai Wang as Fan Yao
* Ku Kuan-chung as Song Qingshu
* Tang Tak-cheung as Mo Shenggu
* Yeung Chi-hing as Shuobude
* Chiang Nan as Peng Yingyu
* Keung Hon as Iron-crowned Taoist
* Chan Shen as He Taichong
* Hung Ling-ling as Ban Shuxian
* Hon Kwok-choi as Kongjian
* Woo Wang-dat as Kongwen
* Ng Man-tat as Hu Qingniu
* Cheung Wai-yee as Wang Nangu
* Lee Sau-kei as Zhu Changling
* Helen Poon as Zhu Jiuzhen
* Chung Kwok-yan as Shi Huolong
* Yue Wing as Ruyang Prince
* Ka Wa as Cloud messenger
* Austin Wai as Wind messenger
* Kara Hui as Moon messenger

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 