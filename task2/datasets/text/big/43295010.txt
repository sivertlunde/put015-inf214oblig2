The Eternal Mother (1917 film)
{{Infobox film
| name           = The Eternal Mother
| image          =
| caption        =
| director       = Frank Reicher
| producer       = Metro Pictures
| writer         = Sidney McCall(novel:Red Horse Hill) Mary Murillo(scenario)
| starring       = Ethel Barrymore
| music          = George Webber
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent..(English titles)
}}
The Eternal Mother is a lost  1917 silent film drama directed by Frank Reicher and stars Ethel Barrymore. The picture is taken from a novel, Red Horse Hill, by Sidney McCall, an alias for Mary McNeill Fenollosa. 

==Cast==
*Ethel Barrymore - Maris
*Frank Mills - Dwight Alden
*J. W. Johnston - Lynch (as Jack W. Johnston)
*Charles Sutton - Minister (as Charles W. Sutton)
*Kaj Gynt - Kate
*Louis Wolheim - Bucky McGhee (as Louis R. Wolheim)
*Maxine Elliott Hicks - Felice
*J. Van Cortlandt - Butler

==References==
 

==External links==
* 

 
 
 
 
 


 