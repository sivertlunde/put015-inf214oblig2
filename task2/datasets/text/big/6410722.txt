Earthlings (film)
 
{{Infobox film
| name = Earthlings
| image = Earthlings (film).jpg
| alt = 
| caption = Festival poster
| director = Shaun Monson
| producer = Shaun Monson Nicole Visram  Brett Harrelson  Babak Cyrus Razi  Maggie Q  Persia White 
| narrator = Joaquin Phoenix
| music = Moby Brian Carter Natalie Merchant Gabriel Mounsey Barry Wood
| cinematography = Mark M. Rissi
| editing = Shaun Monson
| distributor = Nation Earth
| released =  
| runtime = 95 minutes
| country = United States
| language = English
}}
Earthlings is a 2005 American documentary film about humanitys use of other animals as pets, food, clothing, entertainment, and for scientific research. The film is narrated by Joaquin Phoenix, features music by Moby, was directed by Shaun Monson, and was co-produced by Maggie Q, all of whom are practicing Veganism|vegans.   

==Plot== animal profession, Earthlings includes footage obtained through the use of hidden cameras to chronicle the day-to-day practices of some of the largest industries in the world, all of which rely on animals. It draws parallels between racism, sexism, and speciesism.

==Production==
The film started off as footage that writer, director, and producer Shaun Monson had shot at animal shelters around Los Angeles in 1999. Monson originally shot the footage for PSAs on spaying and neutering pets but what he saw moved him so much that he turned it into a documentary. The film would take another six years to complete because of the difficulty of obtaining footage within these industries. 

==Promotion==
Joaquin Phoenix commented on the documentary, "Of all the films I have ever made, this is the one that gets people talking the most. For every one person who sees Earthlings, they will tell three."  Philosopher Tom Regan remarked, "For those who watch Earthlings, the world will never be the same."  

==Accolades== premiered at the Artivist Film Festival, (where it won Best Documentary Feature), followed by the Boston International Film Festival, (where it won the Best Content Award), and at the San Diego Film Festival, (where it won Best Documentary Film, as well as the Humanitarian Award to Phoenix for his work on the film).

==See also==
* Abolitionism (animal rights)
* Animal testing
* Cruelty to animals
* Animal rights
* Animal rights and the Holocaust
* Our Daily Bread (2005 film)|Our Daily Bread (film)
* Unity (film)|Unity (film)
*  

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 