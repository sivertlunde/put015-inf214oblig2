Hoodwink (1981 film)
 
{{Infobox film
| name           = Hoodwink
| image          = HOODWINK1981.jpg
| caption        = Promotional poster
| director       = Claude Whatham
| producer       = Pom Oliver Errol Sullivan
| screenplay     = Ken Quinnell John Hargreaves Dennis Miller
| music          = Cameron Allan
| cinematography = Dean Semler
| editing        = Nicholas Beauman
| distributor    = 
| released       = 5 November 1981
| runtime        = 89 min
| country        = Australia
| language       = English
| budget         = AU $950,000 David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p253-255 
| gross          = 
}} John Hargreaves Award for Best Actress in a Supporting Role.

==Plot==
Martin Stang, a bank robber (Hargreaves) finds himself behind bars and decides to pursue another con job; his escape. He does this by attempting to convince prison authorities that he is blind and no longer poses a threat to society. Along his journey he befriends a sexually-repressed clergymans wife, Sarah (Davis). The pair become intimate during Martins day release but his con is complicated when he reveals to Sarah that he is not in fact blind. 

==Cast== John Hargreaves as Martin Stang
*Judy Davis as Sarah Dennis Miller as Ralph
*Wendy Hughes as Lucy
*Max Cullen as Buster
*Paul Chubb as Reid
*Wendy Strehlow as Martins sister
*Michael Caton as Shapley
*Colin Friels as Robert
*Geoffrey Rush as Detective 1
*Lex Marinos as Detective 2
*John W Pear as Eye Specialist
*Dasha Blahova as Eye Specialists Wife

==Production==
The film is based on the true story of Carl Synnerdahl, a convict who posed as a blind man to get a lighter sentence and had been forced to keep up the deception.  He told his story to literary agent Rosemary Cresswell, who was doing some work for the Department of Corrective Services, who in turn told the story to producer Errol Sullivan. Several directors were approached to make the movie but turned it down, including Bruce Beresford, Michael Thornhill, Phillip Noyce and Esben Storm. Eventually British director Claude Whatham was imported, which was highly controversial because the movie was made with funds from the Australian tax payer. 

==Release==
The film was not a large success at the box office. However Carl Synnerdahl was released from prison on Errol Sullivans bond after serving 21 years in prison and he remarried and had three children. 

===Awards and nominations===
Australian Film Institute Awards

*Australian Film Institute Award for Best Actress in a Supporting Role – Judy Davis (won) John Hargreaves
*Australian Film Institute Award for Best Actor in a Supporting Role – Max Cullen
*Australian Film Institute Award for Best Direction – Claude Whatham
*Australian Film Institute Award for Best Screenplay – Ken Quinnell
*Australian Film Institute Award for Best Achievement in Editing – Nicholas Beauman
*Australian Film Institute Award for Best Costume Design – Ross Major
*Australian Film Institute Award for Best Achievement in Sound – Gary Wilkins/Andrew Steuart/Peter Fenton

==References==
 

==External links==
*  
*  at Oz Movies
 
 
 
 
 