Twice Born
 
{{Infobox film
| name           = Twice Born
| image          = Venuto al mondo.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sergio Castellitto
| producer       = Sergio Castellitto Roberto Sessa
| writer         = 
| screenplay     = Sergio Castellitto Margaret Mazzantini
| story          = Margaret Mazzantini
| based on       =  
| narrator       = 
| starring       = Penélope Cruz Emile Hirsch
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Entertainment One
| released       =  
| runtime        = 127 minutes
| country        = Italy Spain
| language       = English Italian Bosnian
| budget         = €13,000,000
| gross          = 
}}

Twice Born ( ) is a 2013 film directed by Sergio Castellitto. It is based on a novel by Margaret Mazzantini. 

==Cast==
* Penélope Cruz as Gemma
* Emile Hirsch as Diego
* Adnan Haskovic as Gojko
* Saadet Aksoy as Aska
* Pietro Castellitto as Pietro
* Mira Furlan as Velida
* Jane Birkin as Psychologist
* Sergio Castellitto as Giuliano
* Branko Djuric as Doctor
* Isabelle Adriani as Giornalista

==Plot==
Oft-married Gemma visits Sarajevo with her only child, Pietro. The two of them had escaped the city sixteen years ago, just days after his birth during the Bosnian War.  Diego, her second husband and Pietros father, remained behind and later died. As they travel with her wartime friend Gojco, she tries to repair her relationship with Pietro, asking her third husband (by phone) if she should tell Pietro that she did not give birth to him.  Gemma is later stunned by the revelation that Pietros real mother, Aska, is still alive and married to Gojco.  Aska reveals that, contrary to Gemmas long held belief, Diego was not Pietros father, as she had been a sex slave to a garrison of the Serb Volunteer Guard.  Gemma must face loss, the cost of war and the redemptive power of love.

==Production==
The film was shot over 15 weeks in digital using the Arri Alexa system. 

==Release==
The film had its world premiere at the 2012 Toronto International Film Festival. 

==Reception==
 
The film received negative critic reviews. It holds an 18% rating on Rotten Tomatoes based on 22 reviews.

About the film, The Hollywood Reporter wrote, "Dripping with floridly phony dialogue that no actor should be forced to speak, this paternity mystery uses the Bosnian conflict as the manipulative backdrop to a preposterously overwrought and overlong melodrama."  Variety added that the film had "little to offer beyond some pitiful twists."  Screen International went on to write, "director Sergio Castellitto’s adaptation of Margaret Mazzantini’s novel leaves no cliché unturned, yearning for big emotions that are consistently flattened by the lumbering storytelling." 

==References==
 

==External links==
*  
*  
*  

 
 
 


 