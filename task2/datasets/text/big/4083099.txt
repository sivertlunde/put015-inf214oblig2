Alone in the Dark (1982 film)
{{Infobox film
| name           = Alone in the Dark
| image          = Alone in the dark ver1.jpg
| caption        = Promotional film poster
| director       = Jack Sholder
| producer       = Robert Shaye, Benni Korzen
| writer         = Jack Sholder, Robert Shaye, Michael Harrpster
| starring       = Jack Palance, Donald Pleasence, Martin Landau Dwight Schultz Erland Van Lidth
| music          = Renato Serio
| cinematography = Joseph Mangine
| editing        = Arline Garson
| distributor    = New Line Cinema
| released       =  
| runtime        = 92 min.
| country        = United States English
| budget         = Unknown
| gross          = Unknown
}}

Alone in the Dark is a 1982 slasher film directed by Jack Sholder. It was the directors debut film and was an early production of New Line Cinema. The films tagline is: "Theyre out ...for blood! Dont let them find you—"

==Plot== psychiatric haven. paranoid former POW Frank Hawkes, pyromaniac preacher Byron Sutcliff, obese child molester Ronald Elster, and homicidal maniac John "Bleeder" Skaggs) initially treat Dr. Potter with mixed hostility.  At their first meeting, Hawkes nearly explodes at Dr. Potter as he leaves.

The men on the 3rd floor wake up and begin to carry out their plan. Sutcliffe and Elster kill Curtis and the four escape in a night doctors car. They drive to a store in the middle of a raid to pick up new clothes and weapons. Skaggs kills an innocent bystander by splattering out his insides and runs away, the others take the murdered mans van and drive off.

Dan arrives at the hospital to learn from Bain about the four escaped patients and of the people they killed. 
Dan arrives home with Nell, Toni and Tom when they see cops all over the house. Lyla is fine as she told them about Ronald who was there, but the police apparently do not know about the murdered Bunky and Billy. 
Bain is told by the telephone operator that the phone line to the Potter residence is out of order. 
Dan tries to scream to the men outside and tells them that he did not murder Dr. Merton, but he gets no reply. Suddenly, Barnetts dead body is thrown by Ronald through a window, and the group stacks furniture against it as Hawkes shoots another bolt through the broken window. Toni thinks she sees another window open and when she goes to close it, a bloody body jumps through the window at her. But she only imagined it.

Dan runs back inside when he hears the screaming and grabs Tom away from his sister. Lyla hands her mother the knife and Nell stabs Tom in the stomach, killing him. Suddenly, Preacher comes out of the basement and Dan struggles with him. Dan manages to twist the large knife out of Preachers hand, and stabs him the chest with it then throws the fatally wounded Preacher back into the basement. As Dan, Nell, Lyla and Toni gather together for comfort, Hawkes appears standing in the kitchen doorway with his crossbow aimed right at them. "Its not just us crazy ones who kill", says Hawkes. Suddenly, the electricity comes back on and Hawkes sees Dr. Merton interviewed in a news report on television. Evidently upset, Hawkes breaks the TV and leaves the house and runs off into the night.

Hawkes walks through the town and passes the nightclub where the Sic F*cks are playing. He enters the club and beats up the doorman outside who insults him. In the club, Hawkes watches the punk rock band perform, when a girl whacked out on drugs walks up to Hawkes and says something to him. He pulls out his .45 caliber gun and points it at her neck. She looks at it and laughs, and so does Hawkes.

==Cast==
*Jack Palance as Frank Hawkes
*Donald Pleasence as Dr. Leo Bain
*Martin Landau as Byron Preacher Sutcliff
*Dwight Schultz as Dr. Dan Potter Erland van Lidth as Ronald Fatty Elster
*Deborah Hedwall as Nell Potter
*Lee Taylor-Allan as  Toni Potter
*Phillip Clark as Tom Smith
*Elizabeth Ward as Lyla Potter
*Brent Jennings as Ray Curtis
*Frederick Coffin as Jim Gable
*Phillip Clark as Skaggs aka The Bleeder

==Production==
While writing the screenplay for Alone in the Dark, Jack Sholder was inspired by the writings of R D Laing|R. D. Laing, who theorized that psychotics were actually people having difficulty adapting to an already psychotic world. The character of Dr. Leo Bain was supposed to be something of a parody of Laing.

Jack Sholders original idea for the film was to have the story be about mental patients escaping during a blackout in NYC and the mafia being used to stop them. Due to the low budget it was re-envisioned to take place on a smaller scale outside of New York.

It was producer Robert Shaye that actually came up with the idea of the character of The Bleeder. Shaye liked the idea of a crazed murderer who always hid his face and was revealed later in the film.

The scene where Ronald Elster grabs Bunky by the throat and lifts her off the floor was done without any special effect. Erland Van Lidth (Elster) was an incredible weight lifter and actually seized Carol Levy by the neck and lifted her for the shot.

Makeup effects artist Tom Savini was brought in specifically to create the horrific monster apparition that Toni has. Savini achieved the startling effect by covering an actor in a concoction of soap and Rice Krispies.

In the script Jack Palances character was supposed to kill the driver outside the Haven. However Palance refused to do the scene saying it was not necessary for him to be seen killing someone for the audience to know that he was a dangerous character. The scene was never shot.

The film was one of the first horror films to be made with Dolby Stereo sound. The advanced sound level would often blow out the speaker systems in older theaters while the movie was being screened.

==Music==
In the original script the punk band that Toni drags Dan and Nell to see was named Nicky Nothing and the Hives. When The Sic F*cks, an actual punk group, landed the gig as the punk band for the film their real name was liked so much that they kept it for the film.

The first scene at Stumps with The Sic F*cks performing was shot silently without the music. The band and audience had to mimic their performances during the filming and the song Chop Up Your Mother had to be dubbed in later on.

One of the members of The Sic F*cks ran into star Jack Palance years later in the streets of New York. He said to Palance that he was one of The Sic F*cks in the film and Palance replied, "We were all sick fucks in that movie."

==Reception== Friday the 13th. However, since then the film has gained notoriety as being one of the more intelligent slasher-themed films of the 80s.  Allmovie called the film "one of the best (and most subversive) entries in the 80s slasher boom". 

The film was released on DVD by Image Entertainment for the first time in 2005.

In 2008 the indie rock band Lithium Walkers did a tribute song to the film entitled "Alone in the Dark". The groups drummer cites this as one of his favorite films. The song is part of an album called Midnite Matinee, a series of songs named after 80s horror films.

During his interview for the documentary Behind the Curtain Part II (2012), writer/director Jack Sholder said that he considered Alone in the Dark to be one of his most under-appreciated films and one of his favorites. 

The film currently holds a 71% "Fresh" score at Rotten Tomatoes, based on 7 reviews. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 