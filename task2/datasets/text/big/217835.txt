Buffalo '66
{{Infobox film
| name = Buffalo 66
| image = Buffalo sixty six ver1.jpg
| alt = 
| caption = Theatrical release poster
| director = Vincent Gallo
| producer = Chris Hanley
| screenplay = Vincent Gallo Alison Bagnall
| story = Vincent Gallo
| starring = Vincent Gallo Christina Ricci Ben Gazzara Mickey Rourke Rosanna Arquette Jan-Michael Vincent Anjelica Huston 
| music = Vincent Gallo
| cinematography = Lance Acord
| editing = Curtiss Clayton
| studio = Cinépix Film Properties Muse Productions Lions Gate Films
| released =  
| runtime = 120 minutes
| country = United States
| language = English
| budget = 
| gross = $2,375,097 
}}
Buffalo 66 is a 1998 comedy-drama film that is writer-director Vincent Gallos full-length motion picture debut. Gallo and Christina Ricci star in the lead roles and the supporting cast includes Mickey Rourke, Rosanna Arquette, Ben Gazzara, and Anjelica Huston. Gallo also composed and performed much of the music for the film.

Empire Magazine|Empire listed it as the 36th-greatest independent film ever made.  It was filmed in and around Gallos native Buffalo, New York. The film makes extensive use of British progressive rock music in its soundtrack, notably King Crimson and Yes (band)|Yes.

==Plot==
Having just served five years in prison for a crime he did not commit, Billy Brown (Vincent Gallo) kidnaps a young tap dancer named Layla (Christina Ricci) and forces her to pretend to be his wife. Layla allows herself to be kidnapped and it is clear she is romantically attracted to Billy from the start, but Billy all the while is compelled to deal with his own demons, his loneliness and his depression.

The subplot of Billy seeking revenge on the man indirectly responsible for his imprisonment, Scott Wood, is a reference to a former Buffalo Bills kicker, Scott Norwood, who missed the game-winning field goal in Super Bowl XXV against the New York Giants in 1991. 

==Cast==
* Vincent Gallo (John Sansone, young) as Billy Brown
* Christina Ricci as Layla
* Anjelica Huston as Jan Brown
* Ben Gazzara as Jimmy Brown
* Kevin Corrigan as Goon
* Mickey Rourke as The Bookie
* Rosanna Arquette as Wendy Balsam
* Jan-Michael Vincent as Sonny
* Kevin Pollak and Alex Karras as TV sportcasters
* John Rummel as Don Shanks
* Bob Wahl as Scott Wood
* Penny Wolfgang as The Judge

==Production==
Gallo had difficulties working with his cast and crew. Gallo and Christina Ricci reportedly did not get along on the set. He called her a "puppet" who did what she was told. {{cite news
| url = http://www.signonsandiego.com/uniontrib/20050118/news_lz1c18pubeye.html
| title =  Truth or consequences
| author = Tiffany Lee-Youngren
| work = San Diego Union Tribune
| date = 2005-01-18
| accessdate = 2008-04-15
}}  Ricci vowed to never work with Gallo again. {{cite news
| url = http://www.contactmusic.com/news-article/ricci.s-traumatic-gallo-memories
| title = Riccis Traumatic Gallo Memories
| work = Contactmusic.com
| date = 2004-07-13
| accessdate = 2013-10-13
}}  She also resented the comments he made about her weight three or four years after filming. {{cite web
| url = http://www.timeout.com/film/features/show-feature/2921/
| title = Christina Ricci interview
| author = Dave Calhoun Time Out
| accessdate = 2008-04-15
}} 
Anjelica Huston also did not get along with him,  and Gallo claimed Huston caused the film to be turned down by the Cannes Film Festival.    It was director Stéphane Sednaoui that suggested to Gallo to use cinematographer Lance Acord, Although Acord was widely credited with the films distinct visual look Gallo has claimed credit for designing most of the films cinematography.  He also publicly disparaged Acord, saying "This guy had no ideas, no conceptual ideas, no aesthetic point of view."  

==Reception==
Buffalo 66 received mostly positive reviews. Review aggregator Rotten Tomatoes reports that 78% of critics have given the film a positive review, based on 40 reviews, achieving a "Certified Fresh" classification, with an average rating of 7/10.  At Metacritic it has a rating score of 68, indicating "generally favorable reviews." 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at Česko-Slovenská filmová databáze|ČSFD
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 