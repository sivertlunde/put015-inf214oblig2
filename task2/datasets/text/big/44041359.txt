Srishti (film)
{{Infobox film
| name = Srishti
| image =
| caption =
| director = KT Muhammad
| producer =
| writer = KT Muhammad
| screenplay = KT Muhammad Vijayan Adoor Bhavani
| music = MS Baburaj
| cinematography = K Ramachandrababu
| editing =
| studio =
| distributor =
| released =   
| country = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, Vijayan and Adoor Bhavani in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  
*Chowalloor Krishnankutty
*Ravi Alummoodu Vijayan 
*Adoor Bhavani 
*PK Vikraman Nair
*Surasu 
*Thrissur Elsy 
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aayiram Ponpanam || LR Eeswari, Cochin Ibrahim || ONV Kurup || 
|- 
| 2 || Lahari Madakalahari || LR Eeswari, Cochin Ibrahim || ONV Kurup || 
|- 
| 3 || Nithyakaamuki Ninne || S Janaki || ONV Kurup || 
|- 
| 4 || Srishtithan Soundarya || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 


 