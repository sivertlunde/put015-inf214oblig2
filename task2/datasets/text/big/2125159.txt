Wangan Midnight
{{Infobox animanga/Header
| name            = Wangan Midnight
| image           =  
| caption         = Manga vol. 1
| ja_kanji        = 湾岸ミッドナイト
| ja_romaji       = Wangan Middonaito
| genre           = Action film|Action, Racing, Drama film|Drama, speed, car tuning
}}
{{Infobox animanga/Print
| type            = manga
| author          = Michiharu Kusunoki
| publisher       = Shogakukan Kodansha
| demographic     = Seinen manga|Seinen
| magazine        = Big Comic Spirits Young Magazine
| first           = 1990
| last            = 2008
| volumes         = 42
| volume_list     =
}}
{{Infobox animanga/Print
| type            = manga
| title           = Wangan Midnight: C1 Runner
| author          = Michiharu Kusunoki
| publisher       = Kodansha
| demographic     = Seinen manga|Seinen
| magazine        = Young Magazine
| first           = 2009
| last            = 2012
| volumes         = 12
| volume_list     =
}}
{{Infobox animanga/Video
| type            = TV Series
| director        = Tsuneo Tominaga
| producer        = Keiichi Tsuchiya
| writer          =
| music           =
| studio          = A.C.G.T
| network         = Animax
| first           = June 8, 2007
| last            = September 13, 2008
| episodes        = 26
| episode_list    = List of Wangan Midnight episodes
}}
 

  is a Japanese manga series written and illustrated by Michiharu Kusunoki. It was first serialized in Shogakukans Big Comic Spirits since 1990, but was later serialized in Kodanshas Young Magazine, in which Initial D is also serialized. In 1999, it won the Kodansha Manga Award for general manga. 

The series has been adapted into several live action feature films, video games, and an anime television series. The anime began airing in Japan on June 8, 2007 on the anime satellite television network Animax, produced by OB Planning.   

The Wangan Midnight manga ended with volume 42 but has been followed with a new arc called Wangan Midnight C1 Runner (湾岸ミッドナイト C1ランナー). It ended in July 2012, thus ending the entire Wangan Midnight series. Years before Wangan Midnight was created, Michiharu Kusunoki worked on a similar series known as Shakotan Boogie.

== Story ==
The story gets its roots from the street racing that occurs on Tokyos Shuto Expressway     Bayshore Route. Wangan (literally bayshore in Japanese) is the endonym of this longest, straightest road in the entire country.  There is also road traffic to contend with, including a fair number of heavy trucks. Because of this, the action is inherently hazardous, and wrecks are common. Blown engines are also a frequent hazard, especially with the extremely high power engines.
 Porsche 964 Fairlady Z L28 engine, bored and stroked to 3.1&nbsp;L and fitted with twin turbos. He also finds that one of the cars previous owners shares his first and last name, and had been killed in a horrible crash on the Wangan few years ago. Every person who takes possession of the Z ends up spinning out of control and crashing, as if the car is rebelling against its driver. This gives the blue Z the nickname "The Devil Z".

The manga follows Akios various encounters with people on the Wangan, and his maniac try to control the car, though the central plot revolves around his constant battle with the BlackBird for Wangan superiority.

The new story arc, Wangan Midnight C1 Runner, features the new adventures of the new main character, Shinji Ogishima (which debuted in the last chapters of the original story), and his friend, Nobu Setoguchi, both are part of "GT Cars" project which is in dispute and conflict, and must settle these problems by driving Mazda RX-7s around Shuto Expressway, and meeting with Tatsuya Shima.

== Characters ==
 
*  
 Fairlady Z Tatsuya Shima, Reina Akikawa. His day job is a waiter for a night club, but was later fired for showing up late too often due to late night racing. He is regularly exposed to the active social scene at the heart of Tokyo.  He was already an accomplished racer with his old car, but behind the wheel of the Devil Z, Akio is nearly invincible. He is very adaptable and can learn to drive virtually anything.

:  

*  

: Reina is a very attractive model who also co-hosts a driving television show named "Drive Go Go!" where the latest automotive trends are discussed. She finds Akio on the Wangan and becomes obsessed with him and the Devil Z, driving her to tune her car and even steal the Devil Z one night but is stopped by Akio in the GTR. After getting her Nissan Skyline GT-R (R32) tuned and modified, she discovers a previously unknown aptitude for street racing, something that does not sit very well with her manager. She is very sociable, and has lots of acquaintances, but has no real boyfriend.  While she is an idol, she is quite modestly dressed. Like most of the women in the series, Reina is very sensitive and bursts into tears easily in some volumes and one episode of the anime, although she calms down considerably in later volumes. In the manga, her hair is curly, but in later volumes of the manga (and the anime), she only has straight hair.

:  

*  .

: He is the main rival of Akio Asakura, as Akio is the only one who regularly gives him a challenge. He drives a Porsche (in fact, it is a RUF CTR "YellowBird", renamed "BlackBird" because of its color) in the anime and manga, and a Nissan 350Z in the English version of the Maximum Tune games up until 3DX+ and a fictional Gemballa 3.8 RS in the japanese version. The car has   stock. Tatsuya becomes annoyed with Akio becoming faster all the time so he hires Jun Kitami, the man who built the Devil Z, to tune the Porsche. Kitami tunes the suspension and adds a second turbo and boosts the car to   and beyond to try and keep up with the Devil Z. He is an extraordinarily skilled driver and has only been in one wreck during the course of the manga, when he swerved to avoid two drunk men on the road, only to swerve again to avoid a taxi and end up hitting a pole. He knows the limits of his vehicle and will never risk disaster by pushing it beyond its safe limits. This cost him a couple of battles, but he always learns from his failures and comes back even tougher. He is a very skilled surgeon and uses his large salary to fund his Porsche.

:  

== Media ==
=== Manga ===
* Japanese Release - 42 Volumes (1990 - 2008)
* C1 Runner Japanese Release - 12 Volumes (2008–2012)
* Taiwanese release - 38 Volumes
* Chuang Yi release - 42 Volumes
* C1 Runner Chuang Yi release - 11 volumes (ongoing)

The first Wangan Midnight volume was released in Japan in 1990, and has never been translated into any other languages during its publication run, and the final volume was published in mid-2008. However, Taiwan-based Sharp Point Press has obtained license for translating the manga into Chinese languages, but this was stopped after volume 38 due to expired license. There have been no announcements as to a new distributor in Taiwan for continuing or restarting the manga.

The C1 Runner arc started in 2008 shortly after the main story and ended in July 2012, thus ending the entire Wangan Midnight manga series.
 PS3 game of the same name did not help improving the sales of the manga. Despite this, the C1 Runner arc is currently translated into English as well.

=== Movies ===
The series was adapted into a series of direct-to-video movies in 1994, 1997 and again in 2000. They accurately depict the manga, with the exception of Akio being in his thirties. Many Japanese viewers hold the films in high regard for their realistic portrayal of the racing with the cars having the speed they have in the manga.
* Wangan Midnight (湾岸MidNight), 1991
* Wangan Midnight II (湾岸ミッドナイトII), 1993
* Wangan Midnight III (湾岸ミッドナイトIII), 1993
* Wangan Midnight 4 (湾岸ミッドナイト4), 1993
* Wangan Midnight Special Directors Cut Complete Edition (湾岸ミッドナイトスペシャル ディレクターズカット完全版), 1994
* Wangan Midnight Final: GT-R Legend - Act 1 (湾岸ミッドナイト FINAL ~GT-R伝説 ACT1~), 1994
* Wangan Midnight Final: GT-R Legend - Act 2 (湾岸ミッドナイト FINAL ~GT-R伝説 ACT2~), 1994
* Devil GT-R Full Tuning (魔王GT-R チューニングのすべて), 1994
* Showdown! Devil GT-R (対決!魔王GT-R), 1994
* Wangan Midnight S30 vs. Gold GT-R - Part I (新湾岸ミッドナイト S30vsゴールドGT-R Part I), 1998
* Wangan Midnight S30 vs. Gold GT-R - Part II (新湾岸ミッドナイト S30vsゴールドGT-R Part II), 1998
* Wangan Midnight Return (湾岸ミッドナイト リターン), 2001
* Wangan Midnight The Movie (湾岸MidNight Movie), 2009

=== Video games ===
The series has been adapted into a series of   feature trance music soundtracks composed and produced by Yuzo Koshiro. The main objective in the Wangan Midnight and Wangan Midnight R and Maximum Tune is, as the main player, beat all the characters and cars including the Devil Z and Blackbird driven by Akio and Shima from the manga and anime. In the Maximum Tune series, players can also tune their car or modify its appearance upon victory, depending on the game mode being selected.

On release, Famitsu magazine scored the first PlayStation 2 version of the game a 30 out of 40. 
 Arcade (Namco System 246)) Arcade (Namco System 246))
* March 21, 2002: Wangan Midnight (PlayStation 2) Arcade (Sega Chihiro|Chihiro)) Arcade (Sega Chihiro|Chihiro)) Wangan Midnight (PlayStation 3) Arcade (System N2))
* September 2, 2007: Wangan Midnight Portable (PlayStation Portable) Arcade (System N2)) Arcade (System N2))
* 2011: Wangan Midnight: C1 Runner (Mobile phone) Arcade (System N2|SystemES1)) Arcade (System N2|SystemES3))

=== Anime ===
 
At the 2007 Tokyo Anime Fair, OB Planning (Initial D) announced the production of an animated series based on the manga.    Aired on a pay-per-view channel of Animax in June 2007,  the series was co-produced by OB Planning, A.C.G.T., and Pastel under the direction of Tsuneo Tominaga and consists of twenty-six episodes. The series was released on DVD with the thirteenth volume reaching 29th on the Oricon sales chart for Japanese animation DVDs in November 2008. The Japanese opening theme for the series is Lights and Anymore by TRF (band)|TRF, the opening features clips of Akios Devil Z also featuring the 3 main characters and also features Akios Devil Z racing against Shimas Blackbird. The closing theme is Talkin bout good days by Mother Ninja. The animation is similar to that of Initial D Fourth Stage and Fifth Stage.   

=== Wangan Midnight: The Movie (2009) ===
 
  Yuichi Nakamura Wangan as a stand-in for the expressway racing scenes. Like the previous anime television series and video features, Wangan Midnight: The Movie will center on Akio Asakuras "Devil Z" and the rivalry with "Black Bird". 

It appears that a sequel may be planned, as after the ending credits in a bonus scene, Shima is seen to be leaving work when he hears the sound of a Z we dont know if its Akios car or not, followed by a fade out message saying Z will be back.

=== Video game references to other media ===
In all Wangan Midnight games that are published by Bandai Namco, some vans trucks and cars have the Pac-Man (character)|Pac-Man character on it. Also, Pac-Man is used as one of the decals when you tune your car after the race. In Maximum Tune 4, a level features two "Racer Wannabees" driving a Toyota AE86 and a yellow modified Mazda RX-7 which resemble Takumi Fujiwaras AE86 and Keisuke Takahashis FD3S which both drivers in Maximum Tune 4 & 5 are the same impersonaters of Keisuke and Takumi during the later Project D from Initial D Fourth Stage in the anime series.

== See also ==

=== Related manga and anime ===

==== Published by Kodansha ====
* Shakotan Boogie
* Initial D

=== Others ===
* Freeway Speedway Wangan

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 