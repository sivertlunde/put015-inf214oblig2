Mr Bones
 
{{Infobox film
| name           = Mr. Bones
| image          =
| caption        = DVD cover
| director       = Gray Hofmeyr
| producer       = Anant Singh Helena Spring
| writer         = Leon Schuster Gray Hofmeyr Greg Latter David Ramsey Robert Whitehead Jane Benney
| music          = Julian Wiggins
| distributor    = Videovision Entertainment Distant Horizon
| released       =  
| runtime        = 107 minutes
| country        = South Africa
| language       = English
| box office     = $3,900,000}}

Mr. Bones is a 2001 slapstick comedy film made and set in South Africa.  Leon Schuster, well-known figure in the South African film industry, starred in the title role. He also created the story and co-wrote the screenplay. The film sets African "tradition" in opposition to forces of ambition and greed in contemporary South Africa, and plays on reversals of racial stereotypes for its humour. 

The film grossed R33 million, making it the highest grossing South African film of all time, until it was beaten by its sequel,   (grossing R35 million), itself only surpassed by Titanic (1997 film)|Titanic in South Africas box-office history. 

==Plot==
The film begins in Kuvukiland, an undeveloped kingdom somewhere in Africa.  Mr. Bones arrives as a baby, the sole survivor of an airplane crash that happens nearby.  He grows older and becomes the bone-throwing prophesier for the kingdom.  King Tsonga, ruler of Kuvukiland, longs for a male child to be heir to the throne.  After having seventeen children, all of them girls, he gives up hope, until he remembers fathering a boy decades before in Sun City.  He immediately sends Mr. Bones to find the future prince.
 drops a wild boar on him.  He quickly recovers, but is held against his will in a local hospital.  Without his coach, Vince Lee plays terribly, until he meets the eccentric Mr. Bones, who believes him to be the actual prince, and gives him a lucky streak. Vince nearly wins the game until Mr. Bones remembers his mission and stops a perfect putt.  Vince retires in disgrace, but meets a local singer, Laleti, afterwards, whom he is stricken with.  Mr. Bones notices this, and by impersonating her, he kidnaps Vince.

The next day, Wild Bull manages to escape from the hospital and goes on a search for Vince, along with Laleti and her mother.  Enraged by Vince Lees performance, and by the fact that everyone had gone missing, the casino owner mounts a search for them in a helicopter, along with two of his henchmen.  After a series of comical mishaps, they all meet near Kuvukiland.  Mr. Bones introduces Vince to King Tsonga, but after discovering that Vince is terrified of animals, King Tsonga disowns him, and prepares to die.  The casino owner quickly locates Vince and Laleti, and attempts to kill them both, but Vince escapes.  He finds Laleti tied to a tree with a lion about to eat her, and overcoming his fear, he chases the lion away.  King Tsonga sees this, and decides that he doesnt want to die.  Soon after, the casino owner reappears, but Mr. Bones, with the help of an elephant, causes the helicopter to crash.  King Tsonga proclaims that Vince is his son, but asks Mr. Bones to throw his prophesy bones once more just to be sure.  As Mr. Bones does this, Wild Bull arrives upon the scene, and it is confirmed that he is the actual prince.

The film closes with Mr. Bones, King Tsonga, and Wild Bull watching from Kuvukiland as Vince Lee, now married to Laleti, wins the US Open golf tournament.

==Credits==

===Writing credits (in alphabetical order)===
*Gray Hofmeyr: screenplay 
*Greg Latter:   screenplay 
*Leon Schuster:   screenplay 
*Leon Schuster:   story

===Cast (in credits order)===
*Leon Schuster ....  Mr. Bones 
*David Ramsey ....  Vince Lee 
*Aldrovandra Cotton .... Trader Viking
*Faizon Love ....  Pudbedder  Robert Whitehead ....  Zach Devlin 
*Jane Benney ....  Laleti

===Remainder of cast (alphabetically listed)===
*Fem Belling ....  Helicopter Pilot 
*Fats Bookholane .... King Tsonga  
*Zack Du Plessis ....  Farmer 
*Ipeleng Matlhaku .... Lindiwe
*Jerry Mofokeng .... Sangoma
*Craig Morris ....  Future Son-in-Law 
*Septula Sebogodi .... Young King Tsonga 
*Muso Sefatsa .... Boy Tsonga 
*Keketso Semoko .... Laletis Mother 
*Ryan Joel Govender .... Bones Son
*Adam Woolf ....  Young Bones

==References==
 

==External links==
*  
* 

 
 
 