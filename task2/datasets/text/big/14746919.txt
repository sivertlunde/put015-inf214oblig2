Hot Ice (1955 film)
 
 
{{Infobox Film |
  | name           = Hot Ice
  | image          = Hoticestooges.jpg
  | caption        = 
  | director       = Jules White Jack White Kenneth MacDonald George Lloyd
  | cinematography = Fred Jackman Jr.
  | editing        = Anthony DiMarco
  | producer       = Jules White
  | distributor    = Columbia Pictures
  | released       =  
  | runtime        = 16 08"
  | country        = United States
  | language       = English
}}

Hot Ice is the 165th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Kenneth MacDonald). With dreams of becoming genuine detectives, the trio head for Squid McGuffys cafe asking for the whereabouts of Dapper. They manage to convince everyone at the restaurant that they are actually police.

While searching several rooms above the cafe, the Stooges stumble on Dappers moll (Christine McIntyre), who hastily hides the Punjab diamond in a candy dish. The boys refuse to leave, suspecting Dapper will eventually show his face. While killing time, Shemp starts to flirt with the moll, and manages to swallow the ice along with some mints from the candy dish. The gal nearly has a nervous breakdown but quickly discovers the Stooges are not actually real detectives. She calls in Dapper and his henchman Muscles (Cy Schindell) and frantically try to pry the diamond out of frazzled Shemp.

After all else fails, Dapper decides to cut him open. Moe and Larry are locked in a closet by Muscles while Shemp is tied down on a close by desk-turned-operating table. As luck would have it, there happen to be a bag of tools in the closet, which Moe and Larry use to saw their way out of the closet, and right into a gorillas cage on the other side of the wall. The gorilla knocks Moe, Larry, Dapper and Muscles cold. The beast, however, befriends Shemp, and helps him cough up the diamond. Moments later, Shemp explains how he managed to swallow to diamond by actually doing so again.

==Production notes==
Hot Ice is a remake of 1948s Crime on Their Hands, using ample recycled footage. In addition, the Scotland Yard sequence was recycled from 1948s The Hot Scots. 

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 
 
 