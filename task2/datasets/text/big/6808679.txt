Manga Latina: Killer on the Loose

{{Infobox film
| name           = Manga Latina: Killer on the Loose
| image          = MangaLatina-killerposter.png
| caption        = Manga Latina: Killer on the Loose poster
| director       = Henrique Vera Villanueva
| producer       = Antony Blakey Jose Luis Gil Matthew Joynes Louie Porco Henrique Vera Villanueva
| developer      = Yat-Chi Lau, Kathryn Klassen, Henrique Vera Villanueva 
| writer         = Daniel Brandt Hernandez Kathryn Klassen Henrique Vera Villanueva
| starring       = Kerry Shale Luis Soto Laurence Bouvard
| music          = Roberto C. López Maxime Lepage
| cinematography = Eric Heroux
| editing        = Christian Roy
| studio         = Manga Latina Productions
| distributor    = H2V Distribution
| released       =  
| runtime        = 75 minutes
| country        = Canada United Kingdom
| language       = English
| budget         =
}}
Manga Latina: Killer on the Loose is a 2006 dark comedy animation independent film by Manga Latina Productions, distributed by H2V Distribution, and directed by Henrique Vera Villanueva. It premiered at midnight on April 28, on the Providence Latino Film Festival, and tells the story of Víctor La Cruz and his friends from El Barrio, in a satirical quest to stop a bloody serial killer.

==Plot== gory homicides begin to terrorize Víctors community, and the news of a "killer on the loose" are quickly spread. The media warns about the killers habit of dismembering victims to the point of leaving them unrecognizable, and that the killer is a master of disguise with more than 300 victims.

The authorities, on the other hand, dont really seem to care much about the wellbeing of El Barrios inhabitants. But the rebellious Víctor, far from dismayed by this lack of government interest, decides he will investigate the matter personally, as according to him, these are not just dead people anymore - theyre dead Latinos. After consulting El Godparent|Padrino, a santería-practising wise man, Víctor teams up with his delusional and odd friends in order to stop the killer, "Manga Latina" style.

Víctors group, besides of Chucho, consists of Bello (beautiful), thin and paranoid, who is sure that the killer is after him for no particular reason; Ricky, a pothead and green-haired man, who is worried that the killer will get him because of his lightheadness; and the Sisters C.C., Claudia and Camila, who just join in to protect El Barrio. Nevertheless, all they seem to be able to do is hang around Rafas cafeteria, where coffee is always free no matter what, waiting for the latest news on the killers attacks, and driving around Chuchos car with a megaphone telling people to stay home.
 British musician, both of whom make life impossible for the other inhabitants from El Barrio (one with his deformed piñatas, and the other with his loud annoying music).

==Cast==
* Víctor La Cruz - Kerry Shale
* El Chucho - Luis Soto
* Bello / Ricky - Julian Littman
* Claudia / Camila - Laurel Lefkow
* Rafa - Carlos Riera
* Anchorman - Vincent Marcelo
* Pepita Channing - Laurence Bouvard
* Felipa - Lorelei King
* Manconeti - Paul Hicky Bob Sherman

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 