Street Scenes
 
 
{{Infobox film
| name           = Street Scenes
| image          =
| image size     =
| caption        =
| director       = Martin Scorsese
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    = 1970
| runtime        = 75 min.
| country        = U.S.A. English
| budget         =
| preceded by    =
| followed by    =
}} Kent State/Cambodia Incursion Protest in Washington, D.C. The numerous camera operators do impromptu interviews with the protestors and the spectators. The New York protest turns violent as protestors were attacked by construction workers who supported the war. The Washington protest is peaceful. At the end, Scorsese, Harvey Keitel, Jay Cocks and Verna Bloom discuss the events and the current state of world affairs. Oliver Stone was one of the many camera operators.

==External links==
* 
*  

 

 
 
 
 
 
 
 
 

 
 
 