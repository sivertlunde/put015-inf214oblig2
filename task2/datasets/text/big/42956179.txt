Just to Get a Rep
{{Multiple issues|
  }}
{{Infobox film
| name           = Just to Get a Rep
| image          =
| caption        =
| director       = Peter Gerard
| producer       = Peter Gerard
| writer         = 
| narrator       =
| starring       = Futura 2000 Mode 2 Charlie Ahearn Afrika Bambaataa Chaz Bojorquez Grandmaster Caz Henry Chalfant Martha Cooper Kaves Man One Percee-P Blade Comet Zephyr Steven Hager Stay High 149 Tracy 168
| music          = Cousin Cole
| cinematography =
| editing        =
| distributor    = Accidental Media
| released       =  
| runtime        = 52 minutes
| country        = United Kingdom United States
| language       = English
| budget         =
}}

Just to Get a Rep is a documentary film directed by Peter Gerard. It premiered at Edinburgh International Film Festival in 2004 and played over a dozen international film festivals. {{cite news
 | last = Maynard
 | first = Melissa
 | title = Writing on the walls
 | pages = 1
 | newspaper = The Missourian
 | location = Columbia, Missouri
 | date = 23 February 2006
 | url = 
}}  The film was released internationally in 2007. The film covers the history of graffiti art and its relationship with hip-hop, from 1970s New York City to the international graffiti culture in the early 2000s.

== Synopsis ==
The film is about the origins of graffiti and hip-hop as told by some of New Yorks graffiti pioneers as well as contemporary artists from Europe and the US. Graffiti art as we know it today started in Philadelphia and the Bronx and became a worldwide culture when the media and art world featured graffiti and its artists in newspapers, books and movies. Just to Get a Rep examines how these influences affected the culture and married it with rap, breakdancing and DJing under the term "hip-hop" coined by Afrika Bambaataa. {{cite web
| title = Accidental Media website
| url = http://accidental.tv/rep.php
| publisher = Accidental.tv
| accessdate = 27 July 2014 }} 

== Release ==
Just to Get a Rep was premiered at the Edinburgh International Film Festival in 2004 {{cite news
 | last = 
 | first =
 | title = Mirrorball: Short Docs
 | pages =
 | newspaper = The List Magazine
 | location = Edinburgh
 | date = 19 August 2004
 | url = 
}}  and played international festivals for more than two years. Just to Get a Rep was first broadcast on television in 2007. {{cite news
 | last = Larrochelle
 | first = Jean-Jacques
 | title = Just to Get a Rep 20.45 FRANCE Ô
 | pages =
 | newspaper = Le Monde
 | location = Paris
 | date = 14 January 2007
 | url = 
}}  After broadcasts in France, Australia and Russia and a limited DVD release in Japan, Just to Get a Rep was released via video on demand in September 2009 from the films website with "pay-what-you-feel" pricing. {{cite news
 | last = Miller
 | first = Hollie
 | title = Download & Give a Rep
 | pages =
 | newspaper = Scotcampus
 | location = Glasgow
 | date = December 2009
 | url = 
}}  Peter Gerard was invited to Dok Leipzig in October 2009 to give a presentation on the "pay-what-you-feel" sales strategy. {{cite web
| title = Accidental Media News
| url = http://accidental.tv/news9.html
| date = 29 October 2009
| publisher=Accidental.tv
| accessdate = 27 July 2014 }} 
The Special Edition DVD was released in March 2010 and it was added to Amazon Instant Video in early 2014.

==References==
 

==External links==
* 
*  

 
 
 