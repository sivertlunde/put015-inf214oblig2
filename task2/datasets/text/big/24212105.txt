Stories from the Kronen
 
{{Infobox film
| name           = Stories from the Kronen
| image          = Historias del kronen.jpg
| image size     = 
| caption        = Film poster
| director       = Montxo Armendáriz
| producer       = Alfred Hürmer   Claudie Ossard   Elías Querejeta
| writer         = Montxo Armendáriz José Ángel Mañas
| starring       = Juan Diego Botto  Jordi Mollà  Núria Prims   Aitor Merino
| music          =
| cinematography = Alfredo F. Mayo
| editing        = Rosario Sáinz de Rozas
| distributor    =
| released       = 29 April 1995
| runtime        = 95 minutes
| country        = Spain
| language       = Spanish
| budget         =
}}

Stories from the Kronen ( ) is a 1995 Spanish drama film directed by Montxo Armendáriz. Starring Juan Diego Botto and Jordi Mollà, the film is an adaptation of José Angel Mañas eponymous novel. It was entered into the 1995 Cannes Film Festival.   
== Plot ==
Carlos, Roberto, Pedro and Manolo form a group of friends in their early twenties with a lot of time on their hands during their summer vacation. The point of encounter for the group of friends is a bar called the Kronen, where Manolo, the least affluent among them, works as a bar tender and is the singer of a rock band. Carlos is the leader of the pack. He is handsome, selfish, amoral and hedonistic. He is in into a restless pursuing of pleasure: drinking, using heavy drugs, partying and having sex. Nothing seems to stop him. At Kronen Carlos rekindles a relationship with  Amalia, an ex-girlfriend whose current boyfriend is coincidentally out of town. Amalia joins the group attracted by Carlos good looks and charm. Roberto is Carlos best friend and sidekick. He plays the drums in the band where Manolo sings. More serious and with more scrupulous than Carlos, Roberto has repressed homosexual feeling towards his best friend. 

When they go to see Roberto’s favorite film,  , Roberto is aroused when he sees Carlos  sexually playing in the dark with his girlfriend. Pedro is the weakest member of the group. He has serious health issues, he lost a kidney and is a diabetic, being the most vulnerable, Carlos takes advantage of him and bullies him constantly, making fun of his delicacy.

An argument at Kronen between Pedro and a stranger is resolved by trying between them who can stand for more time to be hanging dangerously from a bridge over a highway. The incident is stopped by the police and Carlos and Pedro are detained. Carlos’s father is a lawyer and he gets the two friends out of jail soon. Family relationships are not important for Carlos. During his parents wedding anniversary Carlos’s sister despair with her brother lack of interest for family relationships and wild behavior, sleeping by day and partying heavily by night. Carlos relates only to his old an ailing grandfather, who had served as something of a mentor to him, but he dies.

To celebrate his birthday, Pedro invites his friends to his house for a party, but he dies as the result of having been forced by Carlos to drink a bottle of scotch.

==Cast==
* Juan Diego Botto - Carlos
* Jordi Mollà - Roberto
* Núria Prims - Amalia
* Aitor Merino - Pedro
* Armando del Río - Manolo
* Mercedes Sampietro - Carloss mother
*  André Falcon  - Carloss grandfather
* Josep Maria Pou - Carloss father
* Cayetana Guillén Cuervo - Carloss siter
*  Diana Gálvez - Silvia
*  Iñaki Méndez - Miguel
== Bibliography ==
*D’Lugo, Marvin:   Guide to the Cinema of Spain, Greenwood Press, 1997. ISBN 0-313-29474-7 

==References==
 

==External links==
* 

 
 
 
 
 
 