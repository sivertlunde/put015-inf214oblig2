Final Destination 2
 
 
{{Infobox film
| name= Final Destination 2
| image= Final destination two.jpg
| caption = Theatrical release poster
| director = David R. Ellis
| producer = Warren Zide Craig Perry
| screenplay = J. Mackye Gruber Eric Bress
| story    = J. Mackye Gruber Eric Bress Jeffrey Reddick
| based on =  
| starring = Ali Larter A. J. Cook Michael Landes 
| music = Shirley Walker
| cinematography = Gary Capo
| editing = Eric Sears
| studio = Zide/Perry Productions
| distributor = New Line Cinema
| released =  
| runtime = 90 minutes
| country = United States 
| language = English
| budget = $26 million   
| gross = $90,426,405   
}}
 Final Destination and the second installment of the Final Destination|Final Destination film series.

After the financial success of Final Destination, New Line Cinema contacted Reddick regarding plans for a sequel. Since the original films crew was unavailable, New Line replaced most of the production team. {{cite web|last=Muze Inc.|first=CD Universe|title=Final Destination 2 Blu-ray promotional score composed by Shirley Walker was also released on September 30, 2003.    
 Best Horror Film.   

==Plot== Kimberly Corman Shaina McKlank, Dano Estevez, Frankie Whitman. Evan Lewis, Nora Carpenter Kat Jennings, Rory Peters, Isabella Hudson, Eugene Dix, Thomas Burke from entering the highway. While Thomas questions Kimberly the pileup occurs; Shaina, Dano, and Frankie are killed by a speeding truck, but Kimberly is saved by Thomas at the last second.
 curse of Flight 180. Later, a chain reaction causes a fire in Evans apartment which he narrowly escapes; but when Evan slips the escape ladder falls and impales his eye, killing him. Thomas researches the survivors of Flight 180, and discoverers that Alex Browning was killed by a falling brick. Kimberly visits Clear Rivers, the last survivor of Flight 180, who is now a voluntary inmate at a psychiatric ward. Clear refuses to help, but while arguing with Kimberly she deduces that the survivors are dying in reverse, and warns Kimberly to look out for "signs" of death. Upon arriving home, Kimberly has a vision of a flock of pigeons attacking her. She and Thomas rush to save Nora and Tim, but they arrive too late and Tim is crushed by a glass pane at the dentist. Clear decides to help, and introduces Kimberly and Thomas to mortician William Bludworth, who tells them that "new life" can defeat Death. This leads them to believe that if Isabellas baby is born it will ruin Deaths plan and they will all be safe.

Isabella is accused of driving a stolen van and taken into custody while the other survivors reunite for safety. After Nora is decapitated by malfunctioning elevator doors, Eugene attempts to kill himself, but cant since its not his turn to die. The group leaves to find Isabella, who has gone into labor at the police station, while the policeman on duty rushes Isabella to the hospital in her van. Along the way, they discover that they have all cheated death twice; had it not been for the survivors of Flight 180 they would all be dead, which explains why the survivors are dying in reverse. Since Thomas saved Kimberly from being hit by the truck earlier, she is last on Deaths list.
 PVC pipes, Brian Gibbons, Jaws of Life Kats rescuer accidentally activates the airbag, and her head is impaled by a pipe protruding from her headrest. Her cigarette falls from her hand into a gasoline leak leading to the news van, causing the van to explode, and sending a barbed wire fence flying through the air, which trisects Rory.

Kimberly, Clear, and Thomas rush to the hospital, and Kimberly has another forewarning of List of Final Destination characters#Ellen|Dr. Ellen Kalarjian "strangling" Isabella. After Thomas immobilizes Dr. Kalarjian, Kimberly and Thomas witness Isabella give birth and assume they have cheated death. However, Kimberly has another vision of someone with bloody hands in a submerging van, and realizes that Isabella was not on Deaths list.
 Michael Corman to celebrate their survival. There, they learn of Brians deterrence from Death, when his father tells them he was almost hit by a van, but Rory saved him. The group then see a malfunctioning barbecue grill blow Brian to pieces, and one of his arms lands on his mothets plate, as she screams in horror.

==Cast==
* Ali Larter as Clear Rivers
* A. J. Cook|A.J. Cook as Kimberly Corman Thomas Burke Evan Lewis James Kirk Tim Carpenter Nora Carpenter Kat Jennings Rory Peters Eugene Dix Isabella Hudson
* Tony Todd as William Bludworth|Mr. Bludworth Shaina McKlank Dano Estevez Frankie Whitman Michael Corman Brian Gibbons
* Enid-Raye Adams as List of Final Destination characters#Ellen Kalarjian|Dr. Kalarjian

==Production==
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "We could have made no other movies and the first one still would have been a satisfying experience. But when we were given the opportunity to make a sequel, we jumped at it.”
|- Final Destination.   
|}

===Development=== James Wong, The One and Willard (2003 film)|Willard.    Instead, New Line hired second unit director and stunt coordinator David R. Ellis as director and writing partners Eric Bress and J. Mackye Gruber as co-writers.     "Second unit is like an extension of directing, you’re doing big action sequences on film and it was just something I was going after. Once I got the offer from New Line and we got a good script, it was kind of a natural transition," Ellis professed.       "I wanted our film to be able to stand alone but I watched Final Destination to see what they did that was so successful. I tried to use some of that while trying to keep a stand-alone feel for our movie. I kind of took what worked and tried to improve on it," Ellis added.   "We wanted to take what the first film did effectively, and add levels and layers that would come out through the characters. When we first started writing this, we were trying to think, How can we make Death just a total badass? and to be perfectly honest, the first crack we ever took at this script had to be reined in!" Bress uncovered.    On the other hand, producers Craig Perry and Warren Zide from Zide/Perry Productions also returned and helped on financing the film.    

{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#ffffe0; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "We’ve brought back Clear (Larter) in an interesting way and we just felt that to bring both of them back would make you wonder too much just what the hell they’d been doing for the past few months. Are they locked in a vault somewhere? The story would get bogged down and the characters wouldn’t grow. We had an option on Devon to return, so money wasn’t an issue. With Clear, Ali’s character knows everything about the enemy that she and Kimberly are facing and she’s the perfect mentor for Kimberly. Ali’s character has incarcerated herself into a mental institution for her own protection so she’s the main link to the original film.”
|-
| style="text-align: left;" | — Craig Perry on developing the cliffhanger of Final Destination. 
|}

===Casting===
 
The protagonist of the prior film, Alex Browning (Devon Sawa), was killed off-screen in the film.      Rumors indicated that Sawa had a contract dispute with New Line concerning the deduction of his salary;   however, Perry resolved the issue with the statement that "it had everything to do with narrative, and nothing to do with money or Devon’s unwillingness to come back."  Despite this, New Line reinstated Ali Larter to reprise her character as Clear Rivers.       "When New Line asked me to come back, I thought it was great. They showed me the script and let me have some input, and it was really terrific," Larter revealed.  Larter indicated that Clear "  gotten to a hardened place and tucked herself inside because she has felt so much pain in her life. By having herself committed to a mental hospital, she has created a safehouse so that Death cant get her."   Tony Todd also resumed his character as mortician William Bludworth.   "It’s the same character that we saw before that the audience loved," Ellis expressed. 

The role of Kimberly Corman was given to Canadian actress  , was cast as Thomas Burke.  Landes defined him as "a real nice, decent guy who comes across this huge car accident   who is very intrigued to begin with" and as "the guy who bumps into the girl and he goes nuts as her protector."     Ellis pointed out that he "just wanted to find someone whos young and who can relate to these kids. It wasnt an older guy, but still strong enough and yet sensitive.   brought this really good balance to his part."  Landes was cast a day after his audition, which caused flight schedule problems on his departure two days after and cancellation of his appointments.  
 James Kirk Tim Carpenter, Nora Carpenter, Officer Thomas Rory Peters, Eugene Dix, Kat Jennings. Evan Lewis.]]
 House of the Dead, was appointed as Rory Peters.  Cherry characterized Rory as "a very opposite of me whose arc goes from, I dont really care at all, to Oh my God, this is really happening!"  In the script, Bress said that Rory was his favorite character to write since "hes great comic relief, hes got a drug problem, hes funny, and hes all that." Bress bragged that "Cherry is awesome, awesome casting cause hes just so funny and the way he delivers his lines. Its like Oh yeah. Thats good! Thats better!" "What I think was surprising on him was that from all of the humor hes involved and his sort of itchiness that he has with Kat, there is some moments where he reveals how vulnerable he really is and so the shield comes down and right in there youre really becoming sympathetic to Rory. You like him at first cause hes the funny guy, but then you care about him because you realize that theres a place that humor comes from that we all share," Perry appended. 

 ) as farmer Brian Gibbons.  Novice actors Sarah Carter, Alejandro Rae, and Shaun Sipos were hired as Kimberlys friends Shaina McKlank, Dano Estevez, and Frankie Whitman correspondingly.  Andrew Airlie portrayed Kimberlys father Michael Corman, while Enid-Raye Adams appeared as Dr. Ellen Kalarjian. 

  replaced Greenwood Lake, New York in the film for the lake sequence. The scene was shot in two different locations, the other in a private pool in Campbell River, British Columbia. ]]

===Filming===
Likewise the first film, the film was shot in and around Vancouver Island. "We know the arena, we know the people up there and lets be honest, there’s an enormous economic incentive to shoot there. We were very fortunate to get   Justis Greene, who’s been working as a line producer up there for 30 years and who was able to get us the best crew working in town right now. It was advantageous to us on so many levels, that it was just the right place to go for this movie," Perry stated as the reason for the films production in British Columbia.  British Columbia Highway 19 was utilized as Route 23.  The Plaza of Nations was used as a stand-in for Ellis Medical Complex, the site of Tims death.   
 Campbell River and Okanagan Lake respectively, though it is depicted in Greenwood Lake, New York.     "We shot part of it at the lake where it was 37° cold, which is beyond an ice cream headache. And the second stuff we shot in a big huge tank where we filmed all the underwater sequence, that was in a 93° pool," Landes clarified. Cook and Landes performed their own stunts in both sequences.   "My biggest fear is being trapped in a car underwater. So it was kind of cool to face my fear and all that," Cook divulged.  "We took a couple of lessons with a scuba guide to be comfortable being underwater and breathing off the regulator," Landes enunciated. 

To avoid confusion with Larter and Carter, Cook was required to dye her hair brown for the role.    "I like being a chameleon. It opens you up for so much more in this work. You dont get typecast," Cook professed.  Landes also denied plots regarding Thomas and Kimberlys relationship. "Its kind of a protected relationship, more like a brother/sister thing than a love interest. They didn’t want to go there, I guess. They did go that way in an earlier draft of the script but they didn’t want it to seem like the cop is like lascivious or something. So what they have now, hopefully, is a little bit of chemistry and you get the idea that through tragedy something good will come. So it ends in an optimistic way, that maybe they can be together but there’s no real love story," Landes articulated. 
 composited to be presented among the continuity of the film.   ]]

===Effects===
Digital Dimension took charge of the visual effects of the film. CG supervisor Jason Crosby pointed out that their studio was mainly selected for the highway sequence after the crew realized real logs only bounced about an inch off the road when dropped from a logging truck. "They were concerned about how they would make the shot happen, not knowing if CG would work. The timing was great because we had just finished a test shot of our CG logs bouncing on the freeway. We sent a tape to Vancouver and after seeing it the crew was convinced that any of the log shots could be done with CG," Crosby indicated. "It began with R&D on the log dynamics. Scripts were written to help manage the dynamic simulations with real world numbers for gravity, density, etc. The results were remarkably similar to the original logs shot in Vancouver, a testament to the accuracy of the software and the data we had collected, however the logs still lacked the jumping through windshields killer instinct we were looking for. By tweaking the parameters, we coaxed the logs into a much livelier role." Senior technical director James Coulter added creative 3D tracking on shots with fast pans, motion blur, and filters such as dust, mist, slabs of bark, broken chains, and other debris. Digital artist Edmund Kozin manipulated high resolution photos which were carefully stitched together to achieve realistic texture amongst the 22 CG logs of the film. Hair shaders were also inputed for splintering and frayed wood looks for the logs. Physics such as speed and height of the logging truck, length and width of the logs, type of wood and density of a Douglas-fir were also considered. 

In spite of this, there are no CG cars incorporated in the actual film. "It was a possibility in the beginning so we did some dynamics tests using the logs as rigid bodies to hit cars with soft body deformers on them, but when they were done shooting they didn’t end up needing any CG cars," Crosby avowed.  Lifecasts of the actors were also used for all of the death scenes, including the highway setting.   Landes experienced claustrophobia during the procedure.  Fake blood was also used, but CG blood was also shown. 

===Music===

====Soundtrack==== Highway to Incubus (during Evans house fire) and I Got You by (hed) Planet Earth (during Rorys party). 

====Score====
{{Infobox album
| Name = Final Destination 2: Original Motion Picture Score
| Type = Film
| Artist = Shirley Walker
| Cover = 
| Released = September 30, 2003
| Recorded = 
| Genre = Promotional score
| Length = 31:08
| Label = Warner Bros. Records Inc.
| Producer = 
| Chronology  = 
| Last album  =   (2000)
| This album  = Final Destination 2: Original Motion Picture Score (2003)
| Next album  = Willard (2003 film)|Willard: The Original Motion Picture Score (2003)
}}
Final Destination 2: Original Motion Picture Score was the promotional film score conducted by Daytime Emmy-winning composer Shirley Walker.          Though it was not officially released, it was made available alongside Willard (2003 film)|Willard: The Original Motion Picture Score on September 30, 2003. 

The score received positive reviews among commentators. Robert Koehler of Variety (magazine)|Variety applauded that "Shirley Walkers score displays a thorough understanding of horror jolts."    Pete Roberts of DVDActive admired the score as "top notch."    Anthony Horan of DVD.net Australia told readers to "crank up the volume and prepare for a sonic feast".    Nonetheless, Chris Carle of IGN Movies noted that "while the score is nothing youll remember, it gets the job done."   

{{Track listing
| headline = Final Destination 2: Original Motion Picture Soundtrack
| title1          = Main Title
| length1         = 2:48
| title2          = Kimberlys Lake Premonition 
| length2         = 2:03
| title3          = Blow-out 
| length3         = 1:44
| title4          = Coincidence - Kimberly Remembers Mom 
| length4         = 2:27
| title5          = Killer Kayak 
| length5         = 1:11
| title6          = Noras Turn - Eugene Freaks 
| length6         = 3:40
| title7          = Kimberly Goes to See Clear 
| length7         = 1:51
| title8          = Kimberly Sees Dr. Kalarjan
| length8         = 0:47
| title9          = Ba Bye Kat & Mustang 
| length9         = 1:19
| title10         = Dad And Kimberly 
| length10        = 0:45
| title11         = Pigeons
| length11        = 2:39
| title12         = Eugenes Oxygen 
| length12        = 2:54
| title13         = New Life 
| length13        = 1:59
| title14         = 2 Left 
| length14        = 4:21
| title15         = We Did It 
| length15        = 0:40
| total_length    = 31:08
}}

==Release==

===Box office=== thriller film The Recruit, which debuted on the same day, starred Al Pacino and Colin Farrell, and cleared $16,302,063 domestically.  The film dropped to #5 in the next weekend and descended to #7 in its third weekend during Washingtons Birthday.       The film dropped out of the top-ten list in its fourth weekend until its last screening in 42 theaters in its sixteenth weekend, grossing $27,585 and placing in #65.       Final Destination 2 grossed $46,961,214 in the United States and Canada on its total screening and produced $43,465,191 in other territories, having an overall gross of $90,426,405 internationally. 

In comparison with its precursor, Final Destination ranked #3 on its opening weekend with net worth of $10,015,822, which is $6 million less than the first weekend of Final Destination 2 at #2.    The previous film received $53,302,314 domestically throughout its 22-week run, $64 million more than its sequels gross all through its 16-week presentation.   Final Destination amassed $59,549,147 in other countries and $112,802,314 overall, getting $16 million and $22 million more than Final Destination 2 in that order.  

===Home media===
The film was released on  , on top of accounts in relation to the   and neurological examination under neurophysiologist Dr. Victoria Ibric.   Other featurettes of the DVD include the interactive game Choose Your Fate, the music videos of Middle of Nowhere by The Blank Theory and Seven Days a Week by The Sounds, the theatrical trailers of this film and its antecedent, in addition to informative trivias provided throughout the featurettes.   

==Reception==

===Critical response=== average rating normalized rating out of 100 to reviews from mainstream critics, the film holds an average score of 38 based on 25 reviews.  On June 14, 2010, Nick Hyman of Metacritic included Final Destination 2 in the websites editorial 15 Movies the Critics Got Wrong, denoting that "the elaborate suspense/action set pieces from the first two films are more impressive than most."   

{{quote box width = 35em border = 1px
|align=left
|bgcolor=#90ee90 fontsize = 85% quote = "Death, that reliable old nemesis, is back for more in "Final Destination 2", which suffers from the same rancid dialogue and acting problems as the original but with a much funnier pulse. The real progenitor here is less the previous pic than the sick-funny horror cinema of George Romero. Unlike several recent horror items trembling at the thought of an R rating, this sequel goes for the bloody gusto, and will have the target older teen and horror auds eating it up, with steady return customers in vid." salign = left source =— Robert Koehler of Variety (magazine)|Variety reacting negatively to the films premise and positively for its comedic and horrific themes." 
}}
Negative evaluations condemned the films plot, acting, and screenplay. Roger Ebert of Chicago Sun-Times groused that "perhaps movies are like history, and repeat themselves, first as tragedy, then as farce."     James Berardinelli of ReelViews stressed that "the movie mandates complete gullibility and vacuous attention in order to work on any level."    Claudia Puig of USA Today groused on how  that "there is an audience for a movie in which innocent people suffer hideous accidental deaths is troubling enough, but a group of creative people chose to direct their energies on this repulsive spectacle   simply provokes disgust."    Justine Elias of The Village Voice asserted that "this risible thriller is merely a sadistic series of misread premonitions and vile murders."    David Grove of Film Threat stated that "  wasn’t much scared by anything in Final Destination 2 which is silly and illogical."    Jeff Vice of Deseret News censured to "not even get into the awful script or the numbingly awful performances";    while Bruce Fretts of Entertainment Weekly reviled "everything else about the film is also deadly."   
 Scream movies Evil Dead franchise, but like those pictures it recognizes the close relationship between fright and laughter, and dispenses both with a free, unpretentious hand."    C. W. Nevius of San Francisco Chronicle conveyed its "funnier than the original."    Maitland McDonagh of TV Guide pronounced "if this is your idea of fun, step right up."    William Arnold of Seattle Post-Intelligencer found it as "a series of Grand Guignol skits played for mean-spirited laughs."    Marc Savlov of Austin Chronicle admired how "it is surprisingly good fun for the current crop of horror films, reasonably well-plotted and full of jaw-dropping, white-knuckle scares. That said, it’s most definitely not for the squeamish nor the easily offended."    Nev Pierce of BBC saw that "its simple, but effective";    whereas Sheila Norman-Culp of The Atlanta Journal-Constitution proclaimed that "what Final Destination did for the fear of flying, Final Destination 2 does for the fear of driving."   

{{quote box width = 35em border = 1px
|align=right
|bgcolor=#ffb6c1 fontsize = 85% quote = "The survivors of the highway accident arent nearly as likeable as the survivors of Flight 180. Most of them are morons or jerks, making it difficult to care about their struggle with death. Only Cooks Kimberly elicits the slightest bit of sympathy while Larter clearly rules this movie. While everyone else is merely a hastily assembled caricature without depth, Clear has a well defined background and history thanks to the first film. Final Destination fans will see her as the heroic franchise veteran." salign = left source =— Andrew Manning of Radio Free Entertainment on positively differentiating Cook and Larter among the cast of Final Destination 2.   
}}
Amongst the cast ensemble, Carson, Cherry, Cook, Landes, Larter, and Todd were prominent amidst the analysis for their performances as Eugene, Rory, Kimberly, Thomas, Clear, and Bludworth respectively. Koehler of Variety (magazine)|Variety said that "Carson as skeptical Eugene energizes what had been a rote conception on the page"; "Cherry offered some dry comic balance"; Larter was "casted little light"; Todd was squandered by his "single, distinctly flat scene"; and "the generally awful thesping, led by Cook, whose blurry grasp of emotions betrays Ellis apparent disinterest in his actors."  Grove of Film Threat panned Cook, uttering that "she’s no great actress, but she’s a real looker" and teased that "since when did a horror movie suffer from having two dumb blondes as leads", the other actress referring to Larter, whom "spends the whole movie looking miserable with her frigid acting."  Dustin Putman of TheMovieBoy.com commented how "Cook is serviceable as the premonition-fueled Kimberly, but doesnt evoke enough emotion in the scenes following the brutal deaths of her close friends."    Robin Clifford of Reeling Reviews stated that "Cook was strident as the catalyst that sparks events with her premonitions of disaster and her fervent desire to cheat the Reaper" whereas Larter was "giving the smart-ass edge her character needs";    while Brett Gallman of Oh, The Horror! claimed that Larter "is again the bright spot", along with Cook and Landes who were "serviceable as leads" and Todd "whose purpose has still yet to be revealed in the franchise."   

  in 2003.    ]]

===Accolades===
Like its predecessor, the film was nominated for the  , another New Line film.    

The highway scene was regarded by Grove of Film Threat as "a monument to smashed cars, flying objects and scorched metal" and Garth Franklin of Dark Horizons as "utterly spectacular."     Michael Allen of Yahoo! Voices mentioned it as "one of the best car crash sequences in movie history". {{cite news|last=Allen|first=Michael|title=Final Destination 2 Ends with a Bang Rather Than a Whimper
|url=http://voices.yahoo.com/final-destination-2-ends-bang-rather-than-a-2672314.html?cat=40|accessdate=May 24, 2012|work=Yahoo! Voices|date=February 15}}  Anne Billson of Guardian.co.uk exclaimed it as "one of the most terrifying sequences Ive ever seen, all the more effective for being grounded in reality; few drivers havent felt that anxious twinge as the badly secured load on the lorry in front of them starts to wobble."   

  
The Route 23 pileup scene was included in the lists of best car crashes or disaster scenes by Screen Junkies,    Made Man,    Unreality Magazine, {{cite web|last=Unreality Magazine|first=Unreality|title=7 Awesome Movie "Death By Car" Scenes
|url=http://unrealitymag.com/index.php/2011/04/04/7-awesome-movie-death-by-car-scenes/|accessdate=May 24, 2012}}  All Left Turns,    Chillopedia,    Filmstalker,    io9,    UGO Entertainment, {{cite web|last=IGN Entertainment|first=UGO Entertainment|title=The Most Badass Vehicular Homicides
|url=http://www.ugo.com/movies/the-most-badass-vehicular-homicides-final-destination-2|accessdate=May 25, 2012}}  Filmcritic.com, {{cite web|last=AMC|first=Filmcritic.com|title=Top TenTop Ten Most Memorable Movie Car Crashes 
|url=http://www.filmcritic.com/features/2010/08/top-ten-movie-car-crashes/|accessdate=May 25, 2012}}  and   (#28), James Eldred of Bullz-Eye.com (#20), and Jeff Otto of Bloody Disgusting (#9).         

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 