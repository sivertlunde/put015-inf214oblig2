A Canção da Primavera
{{Infobox film
| name           = A Canção da Primavera
| image          =
| caption        =
| director       = Igino Bonfioli Cyprien Segur
| producer       = Igino Bonfioli Odilardo Costa
| writer         = Aníbal Matos
| starring       =
| cinematography = Igino Bonfioli
| editing        = Luiz de Barros
| distributor    = Bonfioli Filmes
| released       =  
| runtime        =
| country        = Brazil
| language       = Silent
| budget         =
}} 1923 Brazilian silent romantic drama film directed by Igino Bonfioli and Cyprien Segur.

The film premiered in Rio de Janeiro on July 24, 1923.

==Plot==
The film is set in Minas Gerais in the 19th century, as it the story of a powerful farmer Luiz Roldão (Castro Vianna) who attempts to force a marriage between his son Jorge (Odilardo Costa) and Rosita, from the Bentos family, to unite their families for economic reasons. However, Jorge falls in loves with Lina (Iracema Aleixo).

==Cast==
*Ari de Castro Viana as Luiz Roldão 
*Odilardo Costa as Jorge 
*Iracema Aleixo as Lina 
*Naná Andrade as Lili 
*Lucinda Barreto as Rosita 
*Osiris Colombo as Padre Belisário 
*Clementino Dotti as Dr. Carlos 
*Alberto Gomes as Juca Barbeiro 
*Nina Gomes as Salustiana

==External links==
*  

 
 
 
 
 
 


 
 
 