Savage Harvest (1994 film)
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Savage Harvest|
image          = Savage Harvest.jpg|
director       = Eric Stanze |
writer         = |
starring       = Lisa Morrison Ramona Midgett William Clifton DJ Vivona |
producer         = |
music         = |
cinematography = |
distributor    = |
released   = |
runtime        = 71 minutes |
language = English |
}}
Savage Harvest is a 1994 horror film directed by Eric Stanze, and was the first feature film released under the Wicked Pixel Cinema banner.

==Plot==
Six young people are caught in a night of hell in the wilderness as demons begin taking over their bodies. They must decipher a message left by a Cherokee elder before the long "Trail of Tears" march if theyre to get through the night alive.

==Reception==
Chad Connolly of The Movies Made Me Do It commented that "While I wouldnt go so far as to call Savage Harvest an "excellent horror movie", I do have to say that its one of the better ones that Ive seen dealing with this subject matter", and that it contains "some damned fine gore sequences".   William R. Colby of KillingBoxx.com said that "it isnt nearly as polished as some of   other films", but that "the effects work is above average and gorehounds will appreciate the fun bloodletting that occurs."  Michael Scrutchin of Flipside Movie Emporium gave the film a B+ and said, "This one outshines many of its bigger budgeted peers in terms of pure style and fun. You get all the flesh-eating, throat-ripping, screwdriver-stabbing, chainsaw-roaring entertainment you could possibly want."   
 


==References==
 

==External links==
* 
* 

 
 
 
 


 