The House of Rain
{{Infobox film
| name =   The House of Rain 
| image =
| image_size =
| caption =
| director = Antonio Román
| producer = 
| writer = Wenceslao Fernández Flórez    Pedro de Juan    Antonio Román
| narrator =
| starring = Luis Hurtado   Blanca de Silos   Carmen Viance   Nicolás D. Perchicot
| music = José Muñoz Molleda      Heinrich Gartner 
| editing = Juan J. Doria 
| studio = Hércules Films
| distributor = Hércules Films
| released = 4 October 1943 
| runtime = 88 minutes
| country = Spain Spanish 
| budget =
| gross =
| preceded_by =
| followed_by =
}}
The House of Rain (Spanish:La casa de la lluvia) is a 1943 Spanish drama film directed by Antonio Román and starring Luis Hurtado, Blanca de Silos and Carmen Viance. It is set in Galicia (Spain)|Galicia. 

==Cast==
*    Luis Hurtado as Fernando Amil  
* Blanca de Silos as Lina  
* Carmen Viance as Teresa Amil  
* Nicolás D. Perchicot as Elías Morell 
* Rafaela Satorrés as Marina 
* Manuel de Juan as Loquero 1  
* Antonio Bayón as Criado  
* Luis Latorre as Director del manicomio  
* Antonio L. Estrada as Loquero 2
* José Ocaña as Ventero

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008.

== External links ==
* 

 

 
 
 
 
 
 
 
 

 