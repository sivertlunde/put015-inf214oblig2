The Virgin Spring
 
{{Infobox film
| name           = The Virgin Spring
| image          = Jungfruk%C3%A4llan.jpg
| caption        = Original poster
| director       = Ingmar Bergman
| producer       = Ingmar Bergman Allan Ekelund
| writer         = Ulla Isaksson
| starring       = Max von Sydow Birgitta Valberg Gunnel Lindblom Birgitta Pettersson
| music          = Erik Nordgren
| cinematography = Sven Nykvist
| editing        = Oscar Rosander
| distributor    = Janus Films  
| released       =  
| runtime        = 89 minutes
| country        = Sweden Swedish
| budget         = 
| gross          = $700,000 (USA) Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 231 
}} Best Foreign exploitation horror The Last House on the Left.   

==Plot== medieval Sweden, Norse deity Odin. Along their way through the forest on horseback, Ingeri becomes frightened when they come to a  stream-side mill and the two part and Karin sets out on her own.

Ingeri encounters a Odin|one-eyed man at the stream-side mill. When Ingeri asks about his name he enigmatically responds he has none "in these days". The man tells Ingeri that he can see and hear things others can not. When the man makes sexual advances towards her and promises her power, Ingeri flees in terror. Meanwhile, Karin meets three herdsmen (two men and a boy) and invites them to eat her lunch with her. Eventually, the two older men rape and murder Karin while Ingeri watches, hidden, from a distance. The two older men then leave the scene with Karins clothing. The younger (a boy) is left with the body and to watch the goats, but he takes the situation poorly and quickly becomes sick with guilt.

The herders then, unknowingly, seek shelter at the home of the murdered girl. Her parents, played by Max von Sydow and Birgitta Valberg, discover that the goat herders murdered their daughter when the goat herders offer to sell Karins clothes to her mother. After they fall asleep, the mother locks the trio in the chamber. Ingeri returns to the farm and breaks down in front of the father; she tells him about the rape and confesses that she secretly wished for Karins death out of jealousy. She does not mention her encounter with the one-eyed man. In a rage, the father stabs one of the herders to death with a strange dagger before killing the other two with his bare hands. 

The next day, the parents set out to find their daughters body with the help of Ingeri. Her father vows that, although he cannot understand why God would allow such a thing to happen, he will build a church at the site of his daughters death. As her parents lift her head from the ground, a spring begins to flow from where she was lying. Ingeri then begins to wash herself with the water and Karins parents clean their daughters muddied face.

==Cast==
* Max von Sydow – Töre
* Birgitta Valberg – Märeta
* Gunnel Lindblom – Ingeri
* Birgitta Pettersson – Karin
* Axel Düberg – Thin Herdsman
* Tor Isedal – Mute Herdsman
* Allan Edwall – Beggar
* Ove Porath – Boy
* Axel Slangus – Bridge Keeper
* Gudrun Brost – Frida
* Oscar Ljung – Simon

==Themes==
 
The Virgin Spring contains a variety of themes (many of them focusing on the religious aspects of the film), including Christianity, Paganism, Norse mythology, vengeance, the occult, questioning of religious faith, sexual innocence, justice, and the nature of evil. The film poses many moral questions to its audience, primarily concerning the revenge enacted by the parents of Karin, and if it was justified. Threads of nihilism also run within the film, primarily displayed in the lack of human sympathy that is found in the herdsmen, and their unashamed rape, abuse, and ultimate murder of an innocent young girl. The story of The Three Living and the Three Dead, to which the film is indebted, was very common in the Middle Ages, and formed the basis for many texts and images, including the Dance of Death, and Geoffrey Chaucers Pardoners Tale.

The film is based on the 13th-century Swedish ballad, Töres döttrar i Wänge. In the ballad, it is not one but three daughters that are slain by the herdsmen, and the springs gush soon after they are beheaded. The three herdsmen are all adults, and the last one is left alive by the father. "Karin" is the mothers name rather than the daughters, and Ingeris character has no dialogue.
 redemption within the story, in which Karins father, Töre, pleads to God for forgiveness for his vengeful actions, subsequently proclaiming he will build a church on the site of his daughters murder. He also remarks his confusion toward God for the events that have unfolded over the past day, and asks why God would allow such horrendous things to happen to his people.

==Controversy== banned showings of the film because of the rape scene. Janus Films v. City of Fort Worth, 354 S.W.2d 597 (1962), and the Texas Supreme Court upheld the ban, 358 S.W.2d 589 (Tex. 1962).

==Home media==
The Virgin Spring was released in the Criterion Collection on 26 January 2006, and was the 321st entry into the Criterion series. The film was completely re-mastered with a high definition transfer that was approved by director Ingmar Bergman.

==Reception==
Rotten Tomatoes, a review aggregator, reports that 94% of 16 surveyed critics gave the film a positive review; the average rating is 8.1/10. 

===Awards and nominations===
The Virgin Spring won the following awards:
* Academy Award for Best Foreign Language Film 1961 
* A Special Mention for Ingmar Bergman in the Cannes Film Festival 1960 La Vérité)
* Kinema Junpo Award for Best Foreign Language Film and Foreign Film Director (Ingmar Bergman)-1962
It was also nominated for the following categories:
* Golden Palm at the 1960 Cannes Film Festival    1961 Academy Awards

==See also==
* Middle Ages in film The Last House on the Left is a 1972 horror film (remade in 2009) written and directed by Wes Craven, the story of which closely follows The Virgin Spring.
* List of historical drama films
* List of submissions to the 33rd Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

== References ==
 

==External links==
*  
*  
*  
*  
*  
*  
*  , also known as  Herr Töres döttrar – a version of the old ballad, in Swedish

 
 
{{succession box Golden Globe for Best Foreign Film
| years=1961
| before=Black Orpheus Through a Glass Darkly}}
 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 