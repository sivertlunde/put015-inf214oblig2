My Brother the Serial Killer
 

{{Infobox film
| name           = My Brother the Serial Killer
| image          = 
| alt            = 
| caption        = 
| director       = David Monaghan
| producers      =  
| writer         =  
| screenplay     = 
| story          =  Glen Rogers
| starring       = Anthony Meoli as profiler
| narrator       = Clay Rogers
| music          = Davey Ray Moor
| cinematography =  
| editing        = Murray North
| studio         = 
| distributors    = Investigation Discovery (USA)
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}
 Glen Rogers, 1994 murders of Nicole Brown Simpson and Ronald Lyle Goldman. 

==Synopsis==
My Brother the Serial Killer chronicles the background of Rogers and looks into prior assertions that he had murdered over 70 people.   As it investigates claims by the Rogers family that Glen Rogers was behind the Goldman-Simpson murders, the documentary includes a filmed interview of Glens brother Clay, wherein Clay asserts that his brother confessed his involvement.     Rogers family stated that he had informed them that he had been working for Nicole in 1994 and that he had made verbal threats about her to them. Rogers would later speak to a criminal profiler about the Goldman-Simpson murders, providing details about the crime and remarking that he had been hired by O. J. Simpson to steal a pair of earrings and potentially murder Nicole.   

==Reception==

===Reaction from Goldman and Brown families===
The families of Nicole Simpson and Ron Goldman expressed anger at the documentarys premise, with both families dismissing the claims by the Rogers family.    Kim Goldman accused Investigation Discovery of irresponsibility, also stating that no one had informed her of Rogers claims that he had been involved in her brothers death. 

Investigation Discoverys President Henry Schlieff replied that the documentarys intention was not to prove that Rogers had committed the crimes, but to "give viewers new facts and let them make up their own minds" and that he believed that Simpson was guilty of the murders.  Schlieff also commented that the movie did not point out any inconsistencies with the claims or evidence against Rogers because "ID viewers are savvy enough to root them out on their own." 

===Critical response=== Variety gave My Brother the Serial Killer a positive review, praising the documentary for "eschewing cheesy shock effects in favor of incisive commentary from family, law enforcement, press and even victims friends".  IndieWire criticized the documentary as not making much sense and for using the viewpoint of Clay Rogers rather than through one of someone more distanced from Rogers or the crime. 

==References==
 

==External links==
*   at the Internet Movie Database

 
 
 
 
 