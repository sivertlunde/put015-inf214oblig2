Apoorva Sangama
{{Infobox film
| name           = Apoorva Sangama
| image          = Apoorva Sangama cover.jpg
| image_size     = 
| caption        = 
| director       = Y. R. Swamy
| producer       = Y. R. Swamy Naveen Kumar
| writer         = 
| screenplay     = Y. R. Swamy
| narrator       = Rajkumar Shankar Ambika T. N. Balakrishna Thoogudeepa Srinivas Vajramuni
| music          = Upendra Kumar
| cinematography = B. C. Gowrishankar
| editing        = P. Bhaktavatsalam
| studio         = 
| distributor    = Sri Kalikamba Creations
| released       = 1984
| runtime        = 151 minutes
| country        = India
| language       = Kannada
| budget         =
}} Kannada film Ambika in lead roles. The music of the film was composed by Upendra Kumar. The film was a remake of Hindi film Johny Mera Naam.

==Cast== Rajkumar
* Shankar Nag Ambika
* Balakrishna (Kannada actor)|T. N. Balakrishna
* Thoogudeepa Srinivas
* Vajramuni
* Pandari Bai
* M. S. Umesh
* Roopa Chakravarty
* Janakamma
* Lavanya
* Baby Sonal

==Soundtrack==
{| class="wikitable"
|-
! # !! Title !! Singer(s)
|-
| 1 || "Aralide Thanu Mana" || Rajkumar (actor)|Rajkumar, S. Janaki
|-
| 2 || "Ninnegintha Indu Chenna" || Vani Jayaram
|-
| 4 || "Bhagya Ennale Punya" || Rajkumar, Ramesh
|-
| 4 || "Bangaari Nanna Vayyari" || Rajkumar
|-
| 5 || "Thara O Thara" || Rajkumar, S. Janaki
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 


 