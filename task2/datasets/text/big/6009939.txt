Interstellar (film)
 
{{Infobox film
| name = Interstellar
| image = Interstellar film poster.jpg
| alt = A ringed spacecraft, revolves around a wormhole, here depicted as a reflective sphere.
| caption = Theatrical release poster
| director = Christopher Nolan
| producer = {{Plainlist|
* Emma Thomas
* Christopher Nolan
* Lynda Obst
}}
| writer               = {{Plainlist|
* Jonathan Nolan
* Christopher Nolan
}}
| starring             = {{Plainlist|
 
* Matthew McConaughey
* Anne Hathaway
* Jessica Chastain
* Bill Irwin
* Ellen Burstyn
* Michael Caine
 
}}
| music                = Hans Zimmer
| cinematography       = Hoyte van Hoytema Lee Smith
| production companies = {{Plainlist|
* Legendary Pictures Syncopy
* Lynda Obst Productions
}}
| distributor          = {{Plainlist|
* Paramount Pictures  
* Warner Bros. Pictures  
}}
| released             =   
| runtime              = 169 minutes  
| country              = {{Plainlist|
* United Kingdom   
* United States 
}}
| language             = English
| budget               = $165 million   
| gross                = $672.7 million 
}} epic science Syncopy and with Lynda Obst through Lynda Obst Productions. Caltech theoretical physicist Kip Thorne, whose work inspired the film, was an executive producer and acted as scientific consultant. Warner Bros., Paramount Pictures, and Legendary Pictures co-financed the film.
 anamorphic 35 IMAX 70 Double Negative created additional digital effects.
 Best Visual Best Original Best Sound Best Sound Best Production Design.  The film also received several other awards and nominations particularly for its visual effects, cinematography, musical score, and the performance of Mackenzie Foy.

==Plot==
  
 pilot and NASA astronaut binary coded coordinates in the dust. These direct them to a secret NASA installation. 

There they meet one of Coopers college professors, Dr. John Brand, who reveals that a wormhole, apparently created by an advanced alien intelligence, has opened near Saturn and leads to new planets in another galaxy that may offer hope for humanitys survival. NASA had already sent twelve scientists through the wormhole to identify potentially habitable worlds, many orbiting a supermassive black hole named Gargantua. Three of the missions are sending back positive signals: Miller, Edmunds, and Mann. Brand recruits Cooper to command and pilot the spacecraft Endurance to recover the scientists complete data on the planets, while he continues work on solving the use of gravity for propulsion, to send humanity on space stations to one of the habitable planets ("Plan A"). As "Plan B", Endurance carries 5,000 frozen human embryos to repopulate the race on a new world. Coopers decision to accept the multi-year mission devastates and angers Murphy, who refuses to talk to him.

Coopers Endurance crew consists of Brands daughter, biotechnologist Amelia; scientists Romilly and Doyle; and robots TARS and CASE. They travel two years to Saturn, then the wormhole quickly sends them to the distant galaxy. They decide to head first to Millers planet, intending to stop there only briefly, as its proximity to Gargantua causes severe gravitational time dilation; each hour spent on the surface will cost seven Earth years. Cooper, Amelia, and Doyle descend to the planet, finding it covered by a shallow ocean, and Miller dead. As Amelia attempts to recover the data, an enormous wave hits, killing Doyle and waterlogging the ships engines, delaying their departure.
 singularity inside Gargantua.

Low on fuel, Endurance can only visit one more planet before returning to Earth. Amelia suggests Edmunds planet due to her romantic feelings for him, but the rest choose Manns planet as Mann is still transmitting. They discover Manns planet is perpetually cold, covered with glaciers, and has a poisonous atmosphere. Mann, who always knew "Plan B" was the missions true goal, faked data about his planets viability so Endurance would rescue him, and now plans to kill them and return through the wormhole. Mann breaks Coopers spacesuit visor, leaving him to die, and flees to Endurance on their shuttle; Romilly is killed by a bomb Mann set. Amelia rescues Cooper using a cargo shuttle, and they arrive at Endurance in time to witness Mann attempting to dock manually, but the imperfectly sealed airlock violently depressurizes, killing Mann and damaging Endurance. As Endurance spins wildly toward the planet, Cooper manages to dock the cargo shuttle and use its engines to get Endurance under control and keep it in orbit.
 slingshot Endurance around Gargantua on a course toward Edmunds, while another 51 years will pass on Earth. Cooper and TARS detach their shuttles into the black hole, sacrificing themselves to collect data on the singularity, and propel Amelia and CASE faster by reducing the ships mass. As the shuttle falls into the black hole, gravitational forces rip it apart, ejecting Cooper into the interior, which resembles a tesseract. Cooper determines this to be five-dimensional space, created not by an alien intelligence but by a future human civilization, and compressed into a three-dimensional structure in order to help Cooper understand that gravity can be exerted across the dimensions to communicate. From within the tesseract, Cooper sees into Murphs bedroom at various points in her life, and is able to interact with items in it, and comes to realize he was Murphys "ghost". After trying in vain to change the past, he reestablishes communications with TARS and realizes he can encode TARSs data from the singularity into a Morse code message, which he delivers by manipulating the second hand of a watch he gave Murphy before he left. When the adult Murphy retrieves the watch, she realizes what Cooper is doing and records the data, which enables her to solve Brands quantum gravity equation and execute Plan A after all.

After Cooper is finished, the tesseract collapses, and he finds himself traveling back through the wormhole. He awakens aboard a NASA space habitat, similar to an ONeill Cylinder. Cooper reunites with Murphy, now elderly and on her deathbed after having led humanitys exodus. Satisfied that Cooper has kept his promise to return, she convinces him to leave rather than watch her die, and find Amelia. He takes TARS with him in a NASA shuttle, and heads through the wormhole toward Edmunds planet, where Amelia and CASE have set up base camp after finding the planet suitable for human settlement.

==Cast==
{{multiple image
| direction = horizontal
| width1     = 148
| width2     = 155
| footer    = Matthew McConaughey and Anne Hathaway star as Cooper and Dr. Amelia Brand respectively.
| image1    = Matthew McConaughey - Goldene Kamera 2014 - Berlin.jpg
| alt1      = Matthew McConaughey
| image2    = Anne Hathaway 2014 (cropped).jpg
| alt2      = Anne Hathaway
}}
;Endurance crew NASA astronaut and engineer.
* Anne Hathaway as Dr. Amelia Brand, a biotechnologist and the daughter of Dr. John Brand.
* David Gyasi as Dr. Romilly
* Wes Bentley as Dr. Doyle
* Bill Irwin as robots TARS   and CASE  
* Josh Stewart as robot CASE  

;On Earth
* Jessica Chastain as Murphy "Murph" Cooper
:* Mackenzie Foy as young Murphy
:* Ellen Burstyn as elderly Murphy
* Michael Caine as Dr. John Brand
* Casey Affleck as Tom Cooper, Murphys brother
:* Timothée Chalamet as young Tom
* John Lithgow as Donald, Coopers father-in-law
* Leah Cairns as Lois Cooper, Toms wife
* Topher Grace as Getty, a NASA doctor and Murphys boyfriend
* David Oyelowo as Toms and Murphys school principal
* Collette Wolfe as Ms. Hanley, Murphys teacher
* William Devane as Williams, a NASA board member
* Elyes Gabel as the NASA Administrator
;In space
* Matt Damon as Dr. Mann

==Themes==
 
Interstellar explores a apocalyptic and post-apocalyptic fiction|post-apocalyptic future where civilization has regressed and mankind is in danger of extinction.  A blight destroying all food sources has humanity on a path to starvation.

On the surface, the basic theme is Doctor Brands plans to save the species, saying "we were not meant to save the Earth; we were meant to leave it."  His plans for space colonization include "Plan A" of launching a gigantic space station by defying gravity (rather than via rocket propulsion), and a much less ambitious "Plan B" of sending embryos to other planets to restart humanity.

Underlying this scientific/salvation effort is a theme regarding deception and utilitarianism during a crisis.  It is revealed that Doctor Brand concealed that Plan A was not viable, but he knew that the collective effort for self-preservation of Plan A was the only way to keep the scientists doing the incidental work on the more viable Plan B.  He lies to Cooper and the Endurance team, knowing it unlikely that they would leave knowing Plan B is the main goal.  Cooper, an astronaut who has previously experienced a dangerous event during a much less ambitious space mission, continues telling Murph that he is certain to return to her.

Another part of the movie has to do with forgiveness. A significant part of the movie has to do with someone forgiving or asking to be forgiven. This includes Cooper asking Murph forgiveness for leaving and many other scenes.

As Coopers ship takes off, Brands voice-over recites Dylan Thomas poem "Do not go gentle into that good night". The poem is also quoted three other times by various characters. 

Film critic Richard Roeper says that one of the beautiful things about the movie is the "overriding message about the powerful forces of the one thing we all know but cant measure in scientific terms: Love."  The hero decides to leave the daughter he loves to take a mission to find a new hospitable planet.  All the time he keeps thinking of the sacrifice he made by leaving his daughter.  At the end of the movie, as he enters a black hole he discovers that humans from the future have been trying to help them out all along.  In the black hole he moves through time and space until he communicates with her in the past by affecting the physics of a watch.  He finds her because he searches for her through the power of love.  The New Yorker says that "The Nolans take us into the farthest mysteries of space-time, where, they assure us, love joins gravity as a force that operates across interstellar distances." 

==Production==
 
*Christopher Nolan&nbsp;– director, producer, writer
*Jonathan Nolan&nbsp;– writer
*Emma Thomas&nbsp;– producer
*Lynda Obst&nbsp;– producer
*Hoyte van Hoytema&nbsp;– cinematographer
*Nathan Crowley&nbsp;– production designer
*Mary Zophres&nbsp;– costume designer Lee Smith&nbsp;– editor
*Hans Zimmer&nbsp;– music composer Paul Franklin&nbsp;– visual effects supervisor
*Kip Thorne&nbsp;– consultant, executive producer
 
<!--
Interstellar is directed by Christopher Nolan and produced by Nolan, Emma Thomas, and Lynda Obst. Nolan co-wrote the screenplay with his brother Jonathan Nolan.
-->

===Development and financing===
The premise for Interstellar was conceived by film producer Lynda Obst and theoretical physicist Kip Thorne, who collaborated on the 1997 film Contact (1997 US film)|Contact and had known each other since Carl Sagan once set them up on a blind date.     Based on Thornes work, the two conceived a scenario about "the most exotic events in the universe suddenly becoming accessible to humans", and attracted filmmaker Steven Spielbergs interest in directing.  The film began development in June 2006, when Spielberg and Paramount Pictures announced plans for a science fiction film based on an eight-page treatment written by Obst and Thorne. Obst was attached to produce the film, which Variety (magazine)|Variety said would "take several years to come together" before Spielberg directed it.     By March 2007, Jonathan Nolan was hired to write a screenplay for the film, titled Interstellar. 
 Syncopy and Lynda Obst Productions.  The Hollywood Reporter said Nolan will earn a salary of   against 20% of what Interstellar grosses.  To research for the film, Nolan visited NASA as well as the private space program SpaceX. 

Though Paramount and Warner Bros. are traditionally rival studios, Warner Bros., who released Nolans   in exchange for the stake in Interstellar.   

===Writing and casting=== The Dust Bowl for inspiration, Christopher contacted director Ken Burns and producer Dayton Duncan, requesting permission to use some of their featured interviews in Interstellar.   
 True Detective, Nolan invited the actor to visit him at his home.
 Miss Julie in Northern Ireland, and a script was delivered to her.  Matt Damon was cast in late August 2013 in a supporting role and filmed his scenes in Iceland. 

===Filming=== anamorphic 35mm and IMAX film photography.    Cinematographer Hoyte van Hoytema was hired for Interstellar, as Wally Pfister, Nolans cinematographer on all of his past films, was working on his directorial debut, Transcendence (2014 film)|Transcendence.  IMAX cameras were used for Interstellar more than for any of Nolans previous films. To minimize the use of computer-generated imagery, the director had practical locations built, such as the interior of a space shuttle.  Van Hoytema retooled an IMAX camera to be handheld for shooting interior scenes.  Some of the films sequences were shot with an IMAX camera installed in the nosecone of a Learjet.   

Nolan, who is known to keep details of his productions secret, strove to ensure secrecy for Interstellar. The Wall Street Journal reported: "The famously secretive filmmaker has gone to extreme lengths to guard the script to ... Interstellar, just as he did with the blockbuster Dark Knight trilogy."  As one security measure, Interstellar was filmed under the name Floras Letter,    Flora being one of Nolans four children with producer Emma Thomas. 

  was used as a filming location for Interstellar, doubling for Manns planet.]]
The films principal photography was scheduled to last for four months.    It began on  , 2013, in the province of Alberta.  Towns in Alberta where filming took place included Nanton, Alberta|Nanton, Longview, Alberta|Longview, Lethbridge, Fort Macleod, and Okotoks. In Okotoks, filming took place at the Seaman Stadium and the Olde Town Plaza.  For a cornfield scene, production designer Nathan Crowley planted   of corn that would be destroyed in an apocalyptic dust storm scene,  intended to be similar to storms experienced during the Dust Bowl in 1930s United States.  Additional scenes involving the dust storm and McConaugheys character were also filmed in Fort Macleod, where the giant dust clouds were created on location using large fans to blow Cellulose fiber|cellulose-based synthetic dust through the air.  Filming in the province lasted until  , 2013, and involved hundreds of extras as well as approximately   members, most of them local. 

Filming also took place in  .   While filming a water scene in Iceland, actress Anne Hathaway almost suffered hypothermia because the dry suit she was wearing had not been properly secured. 
 Westin Bonaventure Culver City, and a private residence in Altadena, California|Altadena.  Filming concluded in December 2013, and Nolan started editing the film for its release in 2014.  Production completed with a budget of  ,   less than what was allotted by Paramount, Warner Bros., and Legendary Pictures.   

===Production design===
{{multiple image
| align     = right
| direction = horizontal
| footer    = The Endurance spacecraft (left) is based on the International Space Station (right).
| image1    = Interstellar film - Endurance spacecraft.jpg
| width1    = 167
| alt1      = 
| image2    = S130e012150.jpg
| width2    = 150
| alt2      = 
}}
Interstellar features three spacecraft: the Ranger, the Endurance, and the Lander. The Rangers function is similar to the  : "Its a real mish-mash of different kinds of technology. You need analogue stuff as well as digital stuff, you need back-up systems and tangible switches. Its really like a submarine in space. Every inch of space is used, everything has a purpose." Lastly, the Lander transports the capsules with colonization equipment to planetary surfaces. Crowley compared it to "a heavy Russian helicopter". 

The film also features two robots, CASE and TARS. Nolan wanted to avoid making the robots   voiced and physically controlled both robots, but his image was digitally removed from the film and his voicing for CASE was replaced. 

===Sound design and music===
  Richard King supervised the process.  Christopher Nolan said he sought to mix the films sound to take maximum advantage of current sound equipment in theaters.  Nolan paid close attention to designing the sound mix, for instance focusing on what buttons being pressed with astronaut-suit gloves would sound like.  The studios website said that "The sound on Interstellar has been specially mixed to maximize the power of the low end frequencies in the main channels as well as in the subwoofer channel."  Nolan deliberately intended some dialogue to seem drowned out by ambient noise or music, causing some theaters to post notices emphasising that this effect was intentional and not a fault in their equipment. 

Composer  s) need to go by the wayside, the big drums are probably in the bin."  Zimmer also said that Nolan did not provide him a script or any plot details for writing music for the film and instead gave the composer "one page of text" that "had more to do with   story than the plot of the movie".  Nolan has stated that he said to Zimmer: "I am going to give you an envelope with a letter in it. One page. Its going to tell you the fable at the center of the story. You work for one day, then play me what you have written", and that he embraced what Zimmer composed. Zimmer conducted   sessions for Interstellar, which was three times more than for Inception. The soundtrack was released on November 18, 2014. 

===Visual effects===
 Double Negative, Paul Franklin said the number of effects in the film was not much greater than in Nolans The Dark Knight Rises or Inception, but that for Interstellar, they created the effects first, so that digital projectors could be used to display them behind the actors, rather than having the actors perform in front of green screens.  Ultimately the film contained 850 visual effect shots at a resolution of 5600 x 4000 lines: 150 shots that were created in camera using digital projectors, and another 700 were created in post production. Of those, 620 were presented in IMAX, while the rest were anamorphic.   

The Ranger, Endurance, and Lander spacecraft were created using miniature effects by production designer Nathan Crowley in collaboration with effects company New Deal Studios, as opposed to using computer generated imagery, as Nolan felt they offered the best way to give the ships a tangible presence in space. Created through a combination of 3D printing and hand sculpting, the scale models earned the nickname "maxatures" by the crew due to their immense size; the 1/15th scale miniature of the Endurance module spanned over 7.6 m (25 feet), while a pyrotechnic model of a portion of the craft was built at 1/5th scale. The Ranger and Lander miniatures spanned 14 m (46 feet) and over 15 m (50 feet), respectively. The miniatures were large enough for Hoyte van Hoytema to mount IMAX cameras directly onto the spacecraft, thus mimicking the look of NASA IMAX documentaries. The models were then attached to a six-axis gimbal on a motion control system that allowed an operator to manipulate their movements, which were filmed against background plates of space using VistaVision cameras on a smaller motion control rig.  New Deal Studios miniatures were used in 150 special effects shots. 

==Influences==
 The Mirror (1975) influenced "elemental things in the story to do with wind and dust and water". 

Nolan compared Interstellar to   (1983) as an example to follow, and screened it for the crew before production.  To emulate that film, he sought to capture reflection on the Interstellar astronauts visors. For further inspiration grounded in real-world space travel, the director also invited former astronaut Marsha Ivins to the set.  Nolan and his crew studied the IMAX NASA documentaries of filmmaker Toni Myers for visual reference of spacefaring missions, and sought to emulate the look of their use of IMAX cameras in the enclosed spaces of a spacecraft interior. 

The setting of the farm in the Midwest was inspired by Clark Kents upbringing in Man of Steel.  Outside of films, Nolan drew inspiration from the architecture of Ludwig Mies van der Rohe. 

==Scientific accuracy==
 , theoretical physicist, served as consultant and executive producer.]]
Theoretical physicist Kip Thorne was a scientific consultant for the film to ensure the depictions of wormholes and relativity were as accurate as possible. "For the depictions of the wormholes and the black hole," he said, "we discussed how to go about it, and then I worked out the equations that would enable tracing of light rays as they traveled through a wormhole or around a black hole—so what you see is based on Albert Einstein|Einsteins general relativity Einstein field equations|equations."   
 supermassive rotating theoretical equations to the artists, who then wrote new CGI rendering software based on these equations to create accurate computer simulations of the gravitational lensing caused by these phenomena. Some individual frames took up to 100 hours to render, and resulted in 800 terabytes of data. The resulting visual effect provided Thorne with new insight into the effects of gravitational lensing and accretion disks surrounding black holes, which led to the publication of two scientific papers.   

Christopher Nolan was initially concerned that a scientifically accurate depiction of a black hole would not be visually comprehensible to an audience and would require the effects team to unrealistically alter its appearance. However, Nolan found the finished effect to be understandable, provided that he maintained consistent camera perspectives. "What we found was as long as we didnt change the point of view too much, the camera position, we could get something very understandable".    Despite the comprehensibility of Coopers final transition to the singularity of the   makes an estimation of the experience definitionally impossible to predict. However, with the interpretation that Cooper falls within the tesseract prior to being subject to the maximum threshold for human survival of the tidal forces, it is highly likely for Cooper to survive the crossing of the event horizon and his time within Gargantua.

The portrayal of what a wormhole would look like is considered scientifically correct. Rather than a two-dimensional hole in space, it is depicted as a sphere, showing a distorted view of the target galaxy.   The accretion disk of the black hole was described by Thorne as "anemic and at low temperature—about the temperature of the surface of the sun," allowing it to emit appreciable light, but not enough gamma radiation and X-rays to threaten nearby astronauts and planets. 

  "Gargantua". This simulation is approximately what a person would see in the vicinity of a black hole, according to  equations of general relativity. All of the light shown here comes from the horizontal accretion disc. Gravity bends light from the back of the black hole to form the apparent vertical ring.]]

Early in the process, Thorne laid down two guidelines: "First, that nothing would violate established physical laws. Second, that all the wild speculations... would spring from science and not from the fertile mind of a screenwriter."  Nolan accepted these terms as long as they did not get in the way of the making of the movie. At one point, Thorne spent two weeks trying to talk Nolan out of an idea about a character traveling faster than light before Nolan finally gave up.   According to Thorne, the element which has the highest degree of artistic freedom is the clouds of ice on one of the planets they visit, which are structures that probably go beyond the material strength that ice would be able to support. 
 Astrobiologist David Grinspoon points out that even with a voracious blight it would have taken millions of years to draw down the atmospheres content of oxygen. He also notes that the ice clouds should have been pulled down by gravity and the planet orbiting the black hole had sunlight in the film when it should not.  However, as Thorne mentioned above, this kind of rotating black hole has an accretion disk that has a temperature similar to that of the sun, so that the emission of light reaching the planet is likely due to such an energetic/radiating accretion disk of matter approaching the black holes event horizon.  Additionally, a neutron star is mentioned in the movie by Cooper as part of the system.

Astrophysicist Neil deGrasse Tyson has explored the science behind the ending of Interstellar.  He concludes that it is theoretically possible to interact with the past, and that "we dont really know whats in a black hole, so take it and run with it."   

Dr. Michio Kaku praised the film for its scientific accuracy and has said Interstellar "could set the gold standard for science fiction movies for years to come." Likewise, Timothy Reyes, a former NASA software engineer, said, "Thornes and Nolans accounting of black holes and wormholes and the use of gravity is excellent." {{cite web|url=
http://mashable.com/2014/11/08/science-of-interstellar/|title=Why scientists are in a love-hate relationship with Interstellar|publisher=Mashable}} 

Lawrence Krauss has called the science in Interstellar "miserable", and used the blight as an example. 

==Marketing==
The teaser trailer for Interstellar debuted  , 2013 and featured clips related to space exploration, accompanied by a voiceover by Matthew McConaugheys character of Cooper.  The theatrical trailer debuted  , 2014 at the Lockheed Martin IMAX Theater and was made available online later that month. For the week ending   it was the most-viewed movie trailer, with over   views on YouTube. 

Christopher Nolan and McConaughey made their first appearances at San Diego Comic-Con International|Comic-Con in July 2014 to promote Interstellar. In the same month, Paramount Pictures launched a complex interactive Interstellar website. It reported that online users uncovered a star chart related to the Apollo 11 moon landing. 

In October 2014, Paramount partnered with Google to promote Interstellar across multiple platforms.    The films website was relaunched to be a digital hub hosted on a Google domain.    The website collected feedback from film audiences, and linked to a mobile app.  The app featured a game in which players could build solar system models and use a flight simulator for space travel.  The Paramount-Google partnership also included a virtual time capsule compiled with user-generated content to be available in 2015. The initiative Google for Education will also use the film as a basis for promoting lesson plans for math science in schools around the United States. 

Paramount is providing a   about the making of the film, on  , 2014.  On November 7, 2014, W. W. Norton & Company released The Science of Interstellar, a book by Kip Thorne. 

On November 18, 2014 Wired (magazine)|Wired released a tie-in online comic titled Absolute Zero, written by Christopher Nolan and drawn by Sean Gordon Murphy. The comic serves as a prequel to the film following Mann. 

==Release==

===Theatrical run===
Prior to Interstellar s public release, Paramount CEO Brad Grey hosted a private screening on  , 2014 at an IMAX theater in Lincoln Square, Manhattan.  Paramount then showed Interstellar to some of the industrys filmmakers and actors in a first-look screening at the California Science Center on  , 2014.    On the following day, the film was screened at the TCL Chinese Theatre in Los Angeles, California for over   of the Screen Actors Guild. Actors McConaughey, Chastain, and Hathaway appeared afterward for a Q&A session.  The film officially premiered on  , 2014 at the TCL Chinese Theatre in Los, Angeles, California.    It premiered in Europe on  , 2014 at Leicester Square in London. 

  group = "nb"}} A   IMAX projector was installed at the TCL Chinese Theatre in Los Angeles, California to display the format. The films wide release expanded to theaters that show it digitally.    Paramount Pictures is distributing the film in North America, and Warner Bros. will distribute it in the remaining territories.  The film was released in over 770 IMAX screens worldwide, which was widest global release  in IMAX cinemas,   until surpassed by Universal Pictures Furious 7 (810 IMAX theaters). 

Interstellar is an exception to Paramount Pictures goal to stop releasing films on film stock and to distribute them only in digital format.  According to Pamela McClintock of The Hollywood Reporter, the initiative to project Interstellar from film would help preserve an endangered format,  an initiative supported by Christopher Nolan, J. J. Abrams, Quentin Tarantino, Judd Apatow, Paul Thomas Anderson, and other filmmakers.  McClintock reported that several theater owners saw the initiative as "backward", as nearly all theaters in the United States have been converted to digital projection. 

===Home media===
Interstellar was released on home video on March 31, 2015 in both the United States and United Kingdom.    It topped the home video sales chart in its opening week  and for a total of two weeks. 

==Reception==

===Box office===
As of March 19, 2015, Interstellar has earned $188 million in North America and $484.7 million in other territories for a worldwide total of $672.7 million, against a production budget of $165 million.  Calculating in all expenses, Deadline.com estimated that the film made a profit of $47.16 million.  
 tenth highest-grossing film of 2014.  Interstellar is the fourth film to gross over $100 million worldwide from IMAX ticket sales. It trails Avatar (2009 film)|Avatar, The Dark Knight Rises and Gravity (2013 film)|Gravity in total IMAX box office revenue.   
 Pacific Rim ($45.2 million).    
 13th most successful film and 3rd most successful foreign film in South Korea with 9.1 million admissions trailing only behind Avatar (2009 film)|Avatar (13.3 million admissions) and Frozen (2013 film)|Frozen (10.3 million admissions).  The film closed down its theatrical run in China on December 12, 2014 (on Friday, 31 days after its initial release) with a total revenue of $122.6 million.       In total earnings, its largest markets outside of North America and China are South Korea ($73.4 million), the UK, Ireland and Malta ($31.3 million), and Russia and the CIS ($19 million). 
 Big Hero World War Fandango reported that pre-sales for Interstellar were outpacing Christopher Nolans earlier film Inception (2010 film)|Inception, as well as Dawn of the Planet of the Apes, released earlier in 2014.
 2014 US widely released Big Hero 6 ($15.8 million).  The film played 52% male and 75% over 25 years old. 

In its opening weekend the film earned $47,510,360  from 3,561 theaters, debuting in second place after a neck-and-neck competition with   and dropped 39% earning $29.12 million for a two weekend total of $97.8 million.   It earned $7.4 million from IMAX theatres from 368 screens in its second weekend.   In its third week, the film earned $15.1 million and remained at #3, below newcomer   and Big Hero 6. 

===Critical response===


Interstellar received generally positive reviews from critics. It has a score of 72% on  , another review aggregator, the film has a score of 74 out of 100 on based on 46 critics, indicating "generally favorable reviews". 
 At the Movies rated the film four and a half stars out of five, praising the films ambition, effects and 70mm IMAX presentation, though criticizing the sound for being so loud as to make some of the dialogue inaudible. Conversely, cohost Margaret Pomeranz rated the film three out of five, as she felt the human drama got lost amongst the films scientific concepts.  Henry Barnes of The Guardian scored the film three out of five stars, calling it "a glorious spectacle, but a slight drama, with few characters and too-rare flashes of humour." 
 Time Out London also granted the film a maximum score of five stars, stating that it is "a bold, beautiful cosmic adventure story with a touch of the surreal and the dreamlike".  New York Post critic Lou Lumenick deemed Interstellar "a soulful, must-see masterpiece, one of the most exhilarating film experiences so far this century."  Richard Roeper of Chicago Sun-Times awarded the film a full four stars and wrote, "This is one of the most beautiful films I have ever seen&nbsp;— in terms of its visuals, and its overriding message about the powerful forces of the one thing we all know but cant measure in scientific terms. Love." 
 The Telegraph felt Interstellar was "agonisingly" close to a masterpiece, highlighting the conceptual boldness and the "deep-digging intelligence" of the film.  Todd McCarthy, reviewing for The Hollywood Reporter, said, "This grandly conceived and executed epic tries to give equal weight to intimate human emotions and speculation about the cosmos, with mixed results, but is never less than engrossing, and sometimes more than that."  In his review for The Associated Press, Jake Coyle praised the film for its "big-screen grandeur", while finding some of the dialogue "clunky". He further described it as "an absurd endeavor" and "one of the most sublime movies of the decade".   Scott Mendelson of Forbes listed Interstellar as one of the most disappointing films of 2014, stating that the film has a lack of flow, loss of momentum following the climax, clumsy sound mixing, and "thin characters" despite seeing the film twice in order to "give it a second chance". Mendelson writes that Interstellar "ends up as a stripped-down and somewhat muted variation on any number of go into space to save the world movies." 
 David Brooks concludes that Interstellar explores the relationships among "science and faith and science and the humanities" and "illustrates the real symbiosis between these realms."  Wai Chee Dimock, in the Los Angeles Review of Books, writes that Nolans films are "rotatable at 90, 180, and 360 degrees," and that "although there is considerable magical thinking here, making it almost an anti-sci-fi film, holding out hope that the end of the planet is not the end of everything, it reverses itself, however, when that magic falls short, when the poetic license is naked and plain for all to see. In those moments, it suddenly dawns upon us that the ocean that rises up 90 degrees and comes at us like a wall is not just a special effect on some faraway planet, but a scenario all too close to home."   

==Accolades==
 

==See also==
  Black holes in fiction
* Bootstrap paradox
* Interstellar spacecraft
* Interstellar travel
* List of films featuring drones
* List of time travel science fiction
* Wormholes in fiction

==Notes==
 

==References==
 

==Further reading==
* 
* 
*MacKay, John. "  (preliminary notes)"

==External links==
 
 
*  
*  
*  
*  
*  
<!--
*  
-->

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 