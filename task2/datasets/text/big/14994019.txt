A Fine, Windy Day
{{Infobox film
| name           = A Fine, Windy Day
| image          = A Fine, Windy Day.jpg
| caption        = Poster to A Fine, Windy Day (1980)
| film name = {{Film name
 | hangul         =  
 | rr             = Barambuleo joheun nal
 | mr             = Param purŏ choŭn nal }}
| director       = Lee Jang-ho
| producer       = Lee Woo-suk
| writer         = Lee Jang-ho Choi Il-nam
| starring       = Ahn Sung-ki Kim Seong-chan Lee Yeong-ho
| music          = Kim Do-hyang
| cinematography = Seo Jeong-min
| editing        = Kim Hee-su
| distributor    = Dong A Exports Co.
| released       =  
| runtime        = 113 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
A Fine, Windy Day ( ) is a 1980 South Korean film written and directed by Lee Jang-ho.

==Plot==
The lives of three young working-class male friends are followed in the film. Chun-shik works at a barbershop where he is in love with Miss Yu, a co-worker. Gil-nam, a hotel worker, is in love with Jin-ok, who works at a hair salon. Duk-bae, the most innocent of the trio, works at a Chinese restaurant and is torn between his affections for a factory-worker and Myung-hi, a wealthy girl. Together over drinks, the three young men talk over their lives and their aimless thoughts about the future. At the end of the film they are separated when Chun-shik is arrested for assault, and Gil-nam leaves to begin his military service. 

==Cast==
* Lee Yeong-ho... Chun-shik (barbershop worker)
* Ahn Sung-ki... Duk-bae (Chinese restaurant worker)
* Kim Seong-chan... Gil-nam (hotel worker)
* Im Ye-jin... Choon-soon (Chun-shiks younger sister)
* Kim Bo-yeon... Miss Yu (barbershop worker)
* Yu Ji-in...
* Choi Bool-am... Kim He-jang
* Kim Hee-ra... Female Chinese restaurant worker
* Park Won-sook... Female Chinese restaurant owner
* Kim In-moon... Man at the outdoor food stand
* Kim Young-ae... Woman at the outdoor food stand

==Awards==
;Grand Bell Awards (1980) 
* Best Director: Lee Jang-ho
* Best New Actor: Ahn Sung-ki
* Best Screen Adaptation

;Baeksang Arts Awards (1981)
* Grand Award: Lee Jang-ho
* Best Production: Lee Woo-suk
* Best Performance by a New Actor: Kim Seong-chan

==Notes==
 

==Bibliography==
*  
*  
*  

 
 
 
 

 