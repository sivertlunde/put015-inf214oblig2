On Her Majesty's Secret Service (film)
 
 
{{Infobox film
| name = On Her Majestys Secret Service
| image = On Her Majestys Secret Service - UK cinema poster.jpg
| border = yes Frank McCarthy
|alt=A man in a dinner jacket on skis, holding a gun. Next to him is a red-headed woman, also on skis and with a gun. They are being pursued by men on skis and a bobsleigh, all with guns. In the top left of the picture are the words FAR UP! FAR OUT! FAR MORE! James Bond 007 is back!
| starring = George Lazenby Diana Rigg Telly Savalas Bernard Lee Gabriele Ferzetti Ilse Steppat
| screenplay =  
| based on =   Michael Reed
| director = Peter R. Hunt
| producer = Harry Saltzman Albert R. Broccoli John Barry John Glen
| studio = Eon Productions
| distributor = United Artists
| released =  
| runtime = 140 minutes 
| country = United Kingdom
| language = English
| budget = $7 million
| gross = $82,000,000
}}
 James Bond 1963 novel You Only James Bond. During the making of the film, Lazenby decided that he would play the role of Bond only once.
 Blofeld (Telly Catherina von Contessa Teresa di Vicenzo (Diana Rigg).

This is the only Bond film to be directed by Peter R. Hunt, who had served as a film editor and second unit director on previous films in the series. Hunt, along with producers Albert R. Broccoli and Harry Saltzman, decided to produce a more realistic film that would follow the novel closely. It was shot in Switzerland, England and Portugal from October 1968 to May 1969. Although its cinema release was not as lucrative as its predecessor You Only Live Twice, On Her Majestys Secret Service was still one of the top performing films of the year. Critical reviews upon release were mixed, but the films reputation has improved over time, although reviews of Lazenbys performance continue to vary.

==Plot== Teresa "Tracy" di Vicenzo, invites Bond to her hotel room to thank him. The next morning, Bond is kidnapped by several men while leaving the hotel, who take him to meet Marc-Ange Draco, the head of the European crime syndicate Unione Corse. Draco reveals that Tracy is his only daughter and tells Bond of her troubled past, offering Bond a personal dowry of one million pounds if he will marry her. Bond refuses, but agrees to continue romancing Tracy under the agreement that Draco reveals the whereabouts of Ernst Stavro Blofeld, the head of SPECTRE.
 M at British Secret Service headquarters, heads for Dracos birthday party in Portugal. There, Bond and Tracy begin a whirlwind romance, and Draco directs the agent to a law firm in Bern, Switzerland. In Bern, Bond investigates the office of Swiss lawyer Gumbold, and learns that Blofeld is corresponding with London College of Arms genealogist Sir Hilary Bray, attempting to claim the title Comte Balthazar de Bleuchamp.
 brainwashed to distribute bacteriological warfare agents throughout various parts of the world.

Bond tries to trick Blofeld into leaving Switzerland so that MI6 the British Secret Service can arrest him without violating Swiss sovereignty; Blofeld refuses and Bond is eventually caught by henchwoman Irma Bunt. Blofeld reveals that he identified Bond after his attempt to lure him out of Switzerland, and tells his henchmen to take the agent away. Bond eventually makes his escape by skiing down Piz Gloria while Blofeld and many of his men give chase. Arriving at the village of Lauterbrunnen, Bond finds Tracy and they escape Bunt and her men after a car chase. A blizzard forces them to a remote barn, where Bond professes his love to Tracy and proposes marriage to her, which she accepts. The next morning, as the flight resumes, Blofeld sets off an avalanche; Tracy is captured, while Bond is buried but manages to escape.

Back in London at Ms office, Bond is informed that Blofeld intends to hold the world at ransom by threatening to destroy its agriculture using his brainwashed women, demanding amnesty for all past crimes, and that he be recognised as the current Count de Bleuchamp. M tells 007 that the ransom will be paid and forbids him to mount a rescue mission. Bond then enlists Draco and his forces to attack Blofelds headquarters, while also rescuing Tracy from Blofelds captivity. The facility is destroyed, and Blofeld escapes the destruction alone in a bobsled, with Bond pursuing him. The chase ends when Blofeld becomes snared in a tree branch and injures his neck. Bond and Tracy marry in Portugal, then drive away in Bonds Aston Martin. When Bond pulls over to the roadside to remove flowers from the car, Blofeld (wearing a neck brace) and Bunt commit a drive-by shooting of the couples car; Tracy is killed in the attack.

==Cast== James Bond&nbsp;– MI6 agent, codename 007. Tracy di The Avengers,  where she played Emma Peel from 1965–68. )}} You Only Live Twice. Both Broccoli and Hunt felt Pleasence was unsuited to the more physical side of the Blofeld role in On Her Majestys Secret Service. 
* Gabriele Ferzetti as Marc-Ange Draco – Head of the Union Corse, a major crime syndicate and Tracys father (uncredited voice by David de Keyser).    the novel.  On Her Majestys Secret Service was Steppats last role: she died on 22 December 1969, four days after the film premiered.  M – Head of the British Secret Service. This was the sixth of eleven Eon-produced Bond films in which Lee played the role of Admiral Sir Miles Messervy, from Dr. No (film)|Dr. No in 1962 to Moonraker (film)|Moonraker in 1979.
* Lois Maxwell as Miss Moneypenny – Ms secretary. Maxwell played Moneypenny in fourteen Eon-produced Bond films from Dr. No in 1962 to A View to a Kill in 1985; On Her Majestys Secret Service was her sixth appearance. George Baker Sir Hilary Bray – Herald in the London College of Arms, whom Bond impersonates in Piz Gloria. Baker also provided the voice of Bray while Bond was imitating him.  Grunther – Blofelds brutish chief of security at Piz Gloria. In his role as a stuntman, Borienko was one of the people assisting with  Lazenbys audition: Lazenby accidentally broke his nose, which helped him get the part of Bond.  Shaun Campbell – 007s colleague who tries to aid Bond in Switzerland as part of Operation Bedlam. Campbell has been called the films "Official Sacrificial Lamb".  Q – From Russia with Love in 1963 until The World Is Not Enough in 1999.
* Virginia North as Olympe – Dracos female assistant. Nikki van der Zyl provided the uncredited voice for Olympe,  making On Her Majestys Secret Service her sixth Bond film in succession. 

===Blofelds Angels of Death===
The Angels of Death are twelve beautiful women from all over the world being brainwashed by Blofeld under the guise of allergy or phobia treatment in order to spread the Virus Omega.  A number appeared in the representative styles of dress of their particular nation. Their mission is to help Blofeld contaminate and ultimately sterilise the worlds food supply.

* Julie Ege as Helen, a Scandinavian girl. Ege was a former Miss Norway who also starred in a number of Hammer Film Productions. 
* Jenny Hanley as an Irish girl.
* Anouska Hempel as an Australian girl. The Avengers, Purdey in The New Avengers. )}} Catherina von Schell as Nancy, a Hungarian girl at the clinic whom Bond seduces. Casino Royale.

==Production== rights dispute You Only Live Twice.   
 The Man The Saint.  After You Only Live Twice was released in 1967, the producers once again picked up with On Her Majestys Secret Service. 
 Peter Hunt, Chitty Chitty Michael Reed.  Hunt was focused on putting his mark – "I wanted it to be different than any other Bond film would be. It was my film, not anyone elses."  On Her Majestys Secret Service was the last film on which Hunt worked in the series. 

===Writing===
Screenwriter Richard Maibaum, who worked on all the Bond films bar You Only Live Twice, was responsible for On Her Majestys Secret Service s script.  Saltzman and Broccoli decided to drop the science fiction gadgets from the earlier films and focus more on plot as in From Russia With Love. )}} Peter Hunt asked Simon Raven to write some of the dialogue between Tracy and Blofeld in Piz Gloria, which was to be "sharper, better and more intellectual";  one of Ravens additions was having Tracy quoting James Elroy Flecker.    When writing the script, the producers decided to make the closest adaptation of the book possible: virtually everything in the novel occurs in the film  and Hunt was reported to always enter the set carrying an annotated copy of the novel. 
 From Russia the theme from Goldfinger (film)|Goldfinger.   

===Casting=== John Richardson, Dutchman Hans De Vries, American Robert Campbell, Englishman Anthony Rogers and Australian George Lazenby. 

Broccoli and Hunt eventually chose Lazenby after seeing him in a Frys Chocolate Cream advertisement.  Lazenby dressed the part by sporting several sartorial Bond elements such as a Rolex Submariner wristwatch and a Savile Row suit (ordered, but uncollected, by Connery), and going to Connerys barber at the Dorchester Hotel.  Broccoli noticed Lazenby as a Bond-type man based on his physique and character elements, and offered him an audition. The position was consolidated when Lazenby accidentally punched a professional wrestler, who was acting as stunt coordinator, in the face, impressing Broccoli with his ability to display aggression.  Lazenby was offered a contract for seven films; however, he was convinced by his agent Ronan ORahilly that the secret agent would be archaic in the liberated 1970s, and as a result he left the series after the release of On Her Majestys Secret Service in 1969. 
 The Avengers—was George Baker was offered the part of Sir Hilary Bray. Bakers voice was also used when Lazenby was impersonating Bray,  as Hunt considered Lazenbys imitation not convincing enough.  Gabriele Ferzetti was cast as Draco after the producers saw him in an Italian mafia film, but Ferzettis heavy accent also led to his voice being dubbed over. 

===Filming===
 
  , Switzerland.|alt=A restaurant in a snowy environment. Mountains can be seen in the background.]]
Principal photography began in the Canton of Bern, Switzerland, on 21 October 1968, with the first scene shot being an aerial view of Bond climbing the stairs of Blofelds mountain retreat to meet the girls.    The scenes were shot atop the now famous revolving restaurant Piz Gloria, located atop the Schilthorn near the village of Mürren. The location was found by production manager Hubert Fröhlich after three weeks of location scouting in France and Switzerland.  The restaurant was still under construction, but the producers found the location interesting,  and had to finance providing electricity and the aerial lift to make filming there possible.  Various chase scenes in the Alps were shot at Lauterbrunnen and Saas-Fee, while the Christmas celebrations were filmed in Grindelwald, and some scenes were shot on location in Bern.  Production was hampered by weak snowfall which was unfavourable to the skiing action scenes. The producers even considered moving to another location in Switzerland, but it was taken by the production of Downhill Racer.  The Swiss filming ended up running 56 days over schedule.  In March 1969, production moved to England, with Londons Pinewood Studios being used for interior shooting, and Ms house being shot in Marlow, Buckinghamshire. In April, the filmmakers went to Portugal, where principal photography wrapped in May.   The pre-credit coastal and hotel scenes were filmed at Hotel Estoril Palacio in Estoril and Guincho Beach, Cascais,  while Lisbon was used for the reunion of Bond and Tracy, and the ending employed a mountain road in the Arrábida|Arrábida National Park near Setúbal.    Harry Saltzman wanted the Portuguese scenes to be in France, but after searching there, Peter Hunt considered that not only were the locations not photogenic, but were already "overexposed".   

  area in which the ski sequences were shot.|alt=A view of mountain slopes, heavily laden with snow.]] John Glen, parachute harness rig below a helicopter, allowing scenes to be shot on the move from any angle.  The bobsledding chase was also filmed with the help of Swiss Olympic athletes,  and was rewritten to incorporate the accidents the stuntmen suffered during shooting, such as the scene where Bond falls from the sled. Blofeld getting snared with a tree was performed at the studio by Savalas himself, after the attempt to do this by the stuntman on location came out wrong.  Glen was also the editor of the film, employing a style similar to the one used by Hunt in the previous Bond films, with fast motion in the action scenes and exaggerated sound effects. 

The avalanche scenes were due to be filmed in co-operation with the Swiss army who annually used explosions to prevent snow build-up by causing avalanches, but the area chosen naturally avalanched just before filming.  The final result was a combination of a man-made avalanche at an isolated Swiss location shot by the second unit,  stock footage, and images created by the special effects crew with salt.  The stuntmen were filmed later, added by optical effects.  For the scene where Bond and Tracy crash into a car race while being pursued, an ice rink was constructed over an unused aeroplane track,  with water and snow sprayed on it constantly. Lazenby and Rigg did most of the driving due to the high number of close-ups. 

{{Quote box
| quote ="One time, we were on location at an ice rink and Diana and Peter were drinking champagne inside. Of course I wasnt invited as Peter was there. I could see them through the window, but the crew were all outside stomping around on the ice trying to keep warm. So, when she got in the car, I went for her. She couldnt drive the car properly and I got in to her about her drinking and things like that. Then she jumped out and started shouting hes attacking me in the car! I called her a so-and-so for not considering the crew who were freezing their butts off outside. And it wasnt that at all in the end, as she was sick that night, and I was at fault for getting in to her about it. I think everyone gets upset at one time."|width=33%|source=George Lazenby {{cite episode
| title = De vergeten 007
| url = http://www.geschiedenis24.nl/andere-tijden/afleveringen/2006-2007/De-vergeten-007.html
| series = Andere Tijden
| serieslink = http://www.anderetijden.nl/
| network = VPRO
| station = Nederland 2
| city = Amsterdam
| airdate = 19 November 2002
}} }} being cropped for television. 
 Diamonds Are Forever, and that his original intentions were concluding the film with Bond and Tracy driving off following their wedding, saving Tracys murder for the pre-credit sequence of Diamonds Are Forever. The idea was discarded after Lazenby quit the role. 
 underground rail system  after Bonds conversation with Sir Hilary Bray was overheard.  

===Music===
  John Barry;  it was his fifth successive Bond film. Barry opted to use more electrical instruments and a more aggressive sound in the music – "I have to stick my oar in the musical area double strong to make the audience try and forget they dont have Sean&nbsp;... to be Bondian beyond Bondian." 

Barry felt it would be difficult to compose a theme song containing the title "On Her Majestys Secret Service" unless it were written operatically, in the style of Gilbert and Sullivan.  Leslie Bricusse had considered lyrics for the title song  but director Peter R. Hunt allowed an instrumental title theme in the tradition of the first two Bond films. The theme was described as "one of the best title cuts, a wordless Moog synthesizer|Moog-driven monster, suitable for skiing at breakneck speed or dancing with equal abandon." 
 Burt Bacharachs Nina also featured in the film in several scenes. 

The theme, "On Her Majestys Secret Service", is used in the film as an action theme alternative to Monty Normans "James Bond Theme", as with Barrys previous "James Bond music#"007 Theme"|007" themes. "On Her Majestys Secret Service" was covered in 1997 by the British big beat group, the Propellerheads for the Shaken and Stirred album.  Barry-orchestrator Nic Raine recorded an arrangement of the escape from Piz Gloria sequence and it was featured as a theme in the trailers for the 2004 Pixar animated film The Incredibles. 

==Release and reception==
On Her Majestys Secret Service was released on 18 December 1969  with its premiere at the Odeon Leicester Square in London.  Lazenby appeared at the premiere with a beard, looking "very un-Bond-like",  according to the Daily Mirror. Lazenby claimed the producers had tried to persuade him to shave it off to appear like Bond, but at that stage he had already decided not to make another Bond film and rejected the idea.  The beard and accompanying shoulder-length hair "strained his already fragile relationship with Saltzman and Broccoli".  As On Her Majestys Secret Service had been filmed in stereo, the first Bond film to use the technology, the Odeon had a new speaker system installed to benefit the new sounds. 

Because Lazenby had informed the producers that On Her Majestys Secret Service was to be his only outing as Bond and because of the lack of gadgets used by Bond in the film, few items of merchandise were produced for the film, apart from the   ceremony, losing out to Jon Voight. 

===Box office===
The film topped the North American box office when it opened with a gross of $1.2 million.  The film closed its box office run with £750,000 in the United Kingdom (the highest-grossing film of the year),  $64.6 million worldwide,  half of You Only Live Twice s total gross,  but still one of the highest-grossing films of 1969.  It was one of the most popular movies in France in 1969, with admissions of 1,958,172.  Nonetheless this was a considerable drop from You Only Live Twice.  After re-releases, the total box office was $82,000,000 worldwide. 

===Contemporary reviews===
The majority of reviews were critical of either the film, Lazenby, or both, while most of the contemporary reviews in the British press referred to George Lazenby at some point as "The Big Fry", a reference to his previous acting in  J. S. Fry & Sons|Frys Chocolate  advertisements.  Derek Malcolm of The Guardian was dismissive of Lazenbys performance, saying that he "is not a good actor and though I never thought Sean Connery was all that stylish either, there are moments when one yearns for a little of his louche panache."    For all the criticism of Lazenby, however, Malcolm says that the film was "quite a jolly frolic in the familiar money-spinning fashion".  Tom Milne, writing in The Observer was even more scathing, saying that "I&nbsp;... fervently trust (OHMSS) will be the last of the James Bond films. All the pleasing oddities and eccentricities and gadgets of the earlier films have somehow been lost, leaving a routine trail through which the new James Bond strides without noticeable signs of animation." 

  critic AH Weiler also weighed in against Lazenby, saying that "Lazenby, if not a spurious Bond, is merely a casual, pleasant, satisfactory replacement." 
 Alexander Walker in the London Evening Standard who said that "The truth is that George Lazenby is almost as good a James Bond as the man referred to in his film as the other fellow. Lazenbys voice is more suave than sexy-sinister and he could pass for the other fellows twin on the shady side of the casino. Bond is now definitely all set for the Seventies."  Judith Crist of New York Magazine also found the actor a strong point of the movie, stating that "This time around theres less suavity and a no-nonsense muscularity and maleness to the role via the handsome Mr. Lazenby". 

The feminist film critic  , and who actually dares to think that one woman who is his equal is better than a thousand part-time playmates."    Haskell was also affected by the films emotional ending: "The love between Bond and his Tracy begins as a payment and ends as a sacrament. After ostensibly getting rid of the bad guys, they are married. They drive off to a shocking, stunning ending. Their love, being too real, is killed by the conventions it defied. But they win the final victory by calling, unexpectedly, upon feeling. Some of the audience hissed, I was shattered. If you like your Bonds with happy endings, dont go." 

===Reflective reviews===
Critical response to On Her Majestys Secret Service remains sharply divided. Film critic James Berardinelli summed this up in his review of the movie: "with the exception of one production aspect,   is by far the best entry of the long-running James Bond series. The film contains some of the most exhilarating action sequences ever to reach the screen, a touching love story, and a nice subplot that has agent 007 crossing (and even threatening to resign from) Her Majestys Secret Service. The problem is with Bond himself&nbsp;... George Lazenby is boring, and his ineffectualness lowers the pictures quality. Lazenby can handle the action sequences, but thats about all he masters." 

The American film reviewer Leonard Maltin has suggested that if it had been Connery in the leading role instead of Lazenby, On Her Majestys Secret Service would have epitomised the series.  On the other hand, Danny Peary wrote, "Im not sure I agree with those who insist that if Connery had played Bond it would definitely be the best of the entire Bond series&nbsp;... Connerys Bond, with his boundless humor and sense of fun and self-confidence, would be out of place in this picture. It actually works better with Lazenby because he is incapable of playing Bond as a bigger-than-life hero; for one thing he hasnt the looks&nbsp;... Lazenbys Bond also hasnt the assurance of Connerys Bond and that is appropriate in the crumbling, depressing world he finds himself. He seems vulnerable and jittery at times. At the skating rink, he is actually scared. We worry about him&nbsp;... On Her Majestys Secret Service doesnt have Connery and its impossible to ever fully adjust to Lazenby, but I think that it still might be the best Bond film, as many Bond cultists claim."  Peary also described On Her Majestys Secret Service as "the most serious", "the most cynical" and "the most tragic" of the Bond films. 

Brian Fairbanks differed in his opinion of Lazenby, saying that "OHMSS gives us a James Bond capable of vulnerability, a man who can show fear and is not immune to heartbreak. Lazenby is that man, and his performance is superb."  Fairbanks also thought On Her Majestys Secret Service to be "not only the best Bond, it is also the last truly great film in the series. In fact, had the decision been made to end the series, this would have been the perfect final chapter." 

The filmmaker Steven Soderbergh writes that "For me theres no question that cinematically On Her Majestys Secret Service is the best Bond film and the only one worth watching repeatedly for reasons other than pure entertainment&nbsp;... Shot to shot, this movie is beautiful in a way none of the other Bond films are"  

Rotten Tomatoes gives the film an 81% rating based on 43 reviews.  IGN ranked On Her Majestys Secret Service as the eighth best Bond film,  Entertainment Weekly as the sixth,  and Norman Wilner of MSN, as the fifth best.  The film also became a fan favourite, seeing "ultimate success in the home video market".  In September 2012 it was announced that On Her Majestys Secret Service had topped a poll of Bond fans run by 007 Magazine to determine the greatest ever Bond film. Goldfinger came second in the poll and From Russia With Love was third. 

==See also==
 
* Outline of James Bond

==References==
 

==Sources==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

==External links==
 
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 