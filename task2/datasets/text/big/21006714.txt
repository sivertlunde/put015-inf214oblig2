The Warrior and the Wolf
{{Infobox film name = The Warrior and the Wolf image = The Warrior and the Wolf poster.jpg director = Tian Zhuangzhuang writer = Tian Zhuangzhuang
| based on =   producer = William Kong starring = Maggie Q Tou Chung-hua Joe Odagiri cinematography = Wang Yu editing = Wenders Li distributor = EDKO Film released =   runtime = 100 min. language = Mandarin
|country= China budget =
}} Chinese historical historical action film directed by Tian Zhuangzhuang. It tells the story of the battle between two ancient warriors. It is Tians latest directorial effort since 2006s The Go Master.

Filming began in fall 2008 in Chinas northwest Xinjiang Autonomous Region.    The film was released in late 2009. 

The Warrior and the Wolf is being co-produced by companies from Hong Kong (Edko Films), Singapore (MediaCorp Raintree Pictures) and China (Perfect World Culture).

== Plot ==
General Zhang is the commander of a northern borders army camp which repels the attacking barbarians every year. When snowfall make the support of the base with supplies impossible, the troops return home.

Lu Shenkang is a newly recruited soldier who was a shepherd, has no courage and tries to flee the army on several occasions. Lu bonds with General Zhang and when the barbarians attack and take him hostage, Lu exchanges him with a captured tribal prince.

The wounded General Zhang returns home earlier and Lu stays in charge.

When the winters snow arrives, he leads his men on the trip home. Due to the harsh weather they take refuge in the Harran tribes village where Lu takes a hut and  woman for himself.

Shortly before the soldiers leave the village to continue their return home, the woman tells Lu that legend says that being intimate with outsiders leads to them becoming wolves.

The army marches on and are attacked by wolves. A following sand storm kills all but Lu who returns to the village.

Years later the barbarians surrender to Imperial China and General Zhang returns to the region to deliver the courts messages.

Two of his men shoot at wolves and are found dead in the morning.

General Zhang is bitten to death by two wolves after riding out to a deserted fortification.

== Cast ==
*Maggie Q - woman of the Harran tribe; actress Maggie Q was cast in the lead role in September 2008 as a replacement for Tang Wei, who was originally to take the role. Due to Tangs role in Ang Lees sexually explicit Lust, Caution (film)|Lust, Caution, however, she was banned from acting in mainland productions by the State Administration of Radio, Film, and Television. 
*Tou Chung-hua  - General Zhang
*Joe Odagiri  - Lu, a soldier who was a shepherd

== Reception ==
Critical reception was generally poor. Perry Lam writes in Muse (Hong Kong magazine)|Muse Magazine, The original story is both a heartfelt tribute to physical love as a life force and an angry condemnation of the aggressive instincts of human beings... To transport the story to ancient China, and, through the novelty of casting, to have a Japanese actor play the role of a Chinese warrior and an American Vietnamese play a Chinese widow, is not to challenge the imagination but to impoverish it. 

== References ==
 

== External links ==
*  
*   from the Chinese Movie Database

 

 
 
 
 
 
 
 
 
 