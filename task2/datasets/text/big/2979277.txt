Bandidas
 
{{Infobox film 
| name = Bandidas
| image = Bandidas (movie poster).jpg
| caption = Theatrical release poster
| director = Joachim Rønning Espen Sandberg
| producer = Luc Besson
| writer = Luc Besson Robert Mark Kamen
| starring = Salma Hayek Penélope Cruz Steve Zahn Sam Shepard Dwight Yoakam Ismael East Carlo
| music = Éric Serra
| cinematography = Thierry Arbogast
| editing = Frédéric Thoraval
| studio = EuropaCorp Canal+
| distributor = 20th Century Fox
| released = 3 February 2006
| runtime = 93 minutes
| country = France Mexico United States
| language = English Spanish
| budget = $35,000,000
| gross = $18,381,890
}} Western action Norwegian directors Joachim Rønning and Espen Sandberg and produced and written by Luc Besson.  It tells the tale of two very different women in mid-19th century Mexico who become a bank robbing duo in an effort to combat a ruthless enforcer terrorising their town.

This is the first movie that Cruz and Hayek starred in together. It was a co-production among France, the United States and Mexico. 

==Plot==
Set in Mexico during the 1850s, the film tells the story of Sara Sandoval (Salma Hayek) and María Álvarez (Penélope Cruz). María is an uneducated, poor farm-girl whose caring father is being forced off his land by a cruel U.S. land baron, named Tyler Jackson (Dwight Yoakam). Sara is the highly educated, wealthy daughter of the arrogant owner of the nearby properties, and has recently returned from Europe where she attended numerous grade schools and colleges in England, Spain, and France for several years. In one fell swoop, both Marías and Saras fathers fall under attack by the baron, (Saras father is killed, Marías is shot but survives) giving him free rein in the nearby territories. As an act of revenge, María and Sara team up to become bank robbers, stealing and giving back to the poor Mexicans who had lost their lands.

At first, the two are catty and quick to fight over the smallest matters over their different backgrounds, but under the tutelage of famed bank robber Bill Buck they learn to trust each other. During a training session, the two women are testing their strength by hanging off a cliff over a wide river, and at the last moment María swallows her pride and tells Sara she cant swim. María nearly drowns but is saved by Sara, and it is then that the two women put aside their differences and agree that, while they are not friends yet, they can at least work together as partners. María turns out to be a crack shot and, while Sara can barely hold a gun, she shows that she is an expert with throwing knives.

Angered by the recent attacks by the newly infamous Bandidas, Jackson brings in a specialist criminal investigator named Quentin Cooke. It doesnt take long for Sara and María to hear of this, and they quickly capture Cooke and seduce him to help them. He has already figured out that Saras father was murdered and therefore discovers that his employer is actually a criminal.

Now there are three robbers, each playing their part in bigger, more ambitious heists. As time goes on, the girls compete for Quentins affections, which he rebukes because he is engaged. In a move to make the money they have stolen useless, Jackson moves the gold that backs the money on a train up towards U.S. territories. Midway, he decides to steal the gold, betraying the Mexican government. The Bandidas manage to hunt him down, but when they get their chance to kill him, they cant, feeling it would make them no better than him. Jackson manages to draw his gun and almost gets a shot off at María but Sara shoots first, finishing the villain off. In the end Quentin ends up with his fiancée and María and Sara ride off into the sunset, their eyes set on Europe where the banks are, according to Sara, "bigger".

==Cast==
* Salma Hayek as Sara Sandoval
* Penélope Cruz as María Álvarez 
* Steve Zahn as Quentin Cooke 
* Dwight Yoakam as Tyler Jackson 
* José María Negri as Padre Pablo
* Audra Blaser as Clarissa Ashe
* Sam Shepard as Bill Buck  
* Ismael East Carlo as Don Diego Sandoval
* Edgar Vivar as bank manager

 

==Release and reception==
Bandidas earned $18,381,890 worldwide  including $3,153,999 in Mexico and $2,380,000 in Russia. The film earned mixed to positive reviews, with a 62% out of 13 reviews on Rotten Tomatoes. 

==See also==
* Viva Maria!

== References ==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
  
 
 
 
 
 