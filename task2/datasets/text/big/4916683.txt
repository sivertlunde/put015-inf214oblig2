Ong Bak 2
{{Infobox film
| name           = Ong Bak 2: The Beginning
| image          = Ong Bak 2.jpg
| image_size     = 
| caption        = Thai Poster
| director       = Tony Jaa Panna Rittikrai
| producer       = Prachya Pinkaew Tony Jaa Panna Rittikrai Akarapol Techaratanaprasert
| writer         = Ek Iemchuen Nonthakorn Thaweesuk Tony Jaa Panna Rittikrai
| narrator       = 
| starring       = Tony Jaa/nur
| music          = Terdsak Janpan
| cinematography = Nattawut Kittikhun
| editing        = Nonthakorn Thaweesuk Saravut Nakajud
| distributor    = Sahamongkol Film International
| released       =  
| runtime        = 98 minutes
| country        = Thailand Thai
| budget         = $8,000,000
| gross          = $8,936,663

}}
Ong Bak 2: The Beginning (องค์บาก 2) is a 2008 Thai  . Initially claimed to be a   to its predecessor. Unlike its predecessor however, which had a contemporary and realistic setting, Ong Bak 2 is actually set in 15th century   in 2010, which they did. As well as the different historical setting to Jaas previous films, Ong Bak 2 has taken a notably grittier and bloodier direction.

The plot of Ong Bak 2 revolves around Tien (Jaa), the son of Lord Sihadecho, a murdered nobleman in old Siam. As a spirited and unyielding youth, Tien resists savage slave traders and, moments from death, is rescued by a man known as Chernang. Chernang is a renowned warrior and leader of the Pha Beek Khrut, a group of bandits, and Chernang realizes unsurpassed physical potential in the young Tien and takes Tien under his wing. The Pha Beek Khrut are a group of expert martial artists specialising in combat styles from all over Asia, and Tien is trained to unify these different fighting systems, and grows into the most dangerous man alive. As Tien becomes a young man he goes on a lone mission of vengeance against the vicious slave traders who enslaved him as a youth, and also the treacherous warlord, Lord Rajasena, who murdered his father and who has an entire army protecting him.

== Plot == Boromarajatiraj II Kingdom of Gods for several months. The king sent his son, Prince Indraracha to rule the kingdom.

At the new kingdom, Lord Sihadecho is a provincial ruler, and a gallant and noble warrior of a formally great dynasty. His son, Tien, a spirited and unyielding youth, aspires to be just like his father, but is forced to undergo dance lessons instead much to his disdain. Meanwhile, the treacherous and power-craving Lord Rajasena, a former city administrator of the capital city, plots to seize total control of all Asia and has amassed the greatest army in Asia. Rajasena sends out vicious assassins to murder Lord Sihadechos family and his loyal soldiers. The only survivor from this massacre is Tien, who manages to escape with deep vengeance in his heart.
 muay boran and krabi krabong, Japanese kenjutsu and ninjutsu, Malay silat, and various Chinese martial arts. He also learns the use of weapons such as the ninjatō, katana, jian, dao (sword)|dao, talwar, nunchaku, rope dart, and three-section staff.   

Now a young man and with all these martial arts heavily instilled, becoming the greatest warrior to ever live, Tien (Tony Jaa) is eager to quench the vengeance in his heart by killing the slave traders, which he does. He then goes on to kill Lord Rajasena by posing as a dancer during a celebration.  Returning to the Pha Beek Khrut, Tien is mystified to find their village deserted. Suddenly, he finds himself confronted by wave after wave of masked assassins, the same ones hired by Lord Rajasena to destroy his original home. As the fight progresses Tien is too enraged to notice that the masked villains are none other than his Pha Beek Khrut comrades though their individual combat styles are glaringly recognizable. As Tien tries to defeat the masked assassins
he climbs on an elephant but then Bhuti Sangkha a.k.a. The Crow Ghost (Dan Chupong uncredited) appears and kicks Tien off of the elephant. Bhutis nature is unknown and he has a small role in the film. Then he takes the elephant away. At last confronting their leader, Tien finds they have been surrounded by Rajasenas army, which is led by the tyrant, himself. Lord Rajasena reveals he had survived thanks to an armored tunic concealed beneath his state robes. Chernang unmasks and admits to his part in killing Lord Sihadecho, as he was in league with Rajasena. Chernang explains that he must carry out Rajasenas orders, or his family (the Pha Beek Khrut) will be killed. As Tien reluctantly fights Chernang, Chernang pins him to the ground, once again calls Tien his son and asks him to take his life in payment for killing his father. Chernang then forces Tiens blade to snap and slash across his throat, taking his life.

The film ends on a cliffhanger with Tien, after defeating dozens of Rajasenas warriors, being finally overwhelmed by hundreds more. Rajasena orders Tien to be taken away to be slowly tortured to death. It is unclear whether Tien survives, and if he does, how it is so. An extremely ambiguous and vague voice-over explains that Tien "may find a way to cheat death again", and shows him with a fully-grown beard (which he does not have in the film) standing in front of a scarred golden Buddha statue, perhaps indicating reincarnation.

== Cast ==
* Tony Jaa as Tien
* Nirut Sirichanya as Master Bua (as Nirut Sirijanya)
* Sorapong Chatree as Chernang
* Sarunyoo Wongkrachang as Lord Jom Rajasena
* Santisuk Promsiri as Nobleman Lord Sihadecho
* Primorata Dejudom as Pim
* Natdanai Kongthong as Young Tien
* Prarinya Karmkeaw as Young Pim
* Patthama Panthong as Lady Plai
* Petchtai Wongkamlao as Mhen
* Dan Chupong as Bhuti Sangkha / Crow Ghost (uncredited)
* Supakorn Kitsuwon as Guard in Golden Armour (uncredited)
* Tim Man as Black Ninja
* Jaran Ngamdee
* Somdet Kaew-ler
* Kaecha Kampakdee (as Gaesha Kumpakdee)

== Production ==
Shooting of the film began in October 2006. It was released in Thailand on December 5, 2008.    In July 2008, rumor surfaced that Tony Jaa had disappeared from the production set. Prachya Pinkaew commented to the press that Tony Jaa had disappeared from the set for almost two months, leaving the film unfinished; and that the delay caused more than 250 million baht damage due to the breach of contract with the Weinstein Company who had also canceled the contract. Later in an interview with the press, Tony Jaa stated that the production was on hiatus because Sahamongkol Film could not release the obligated funding for the film. Sources within Ayara Film, the subsidiary of Sahamongkol Film that handled Ong Bak 2 production, stated that no more funding came from Sahamongkol after it took over the budget and management role from Tony Jaa from May 2008 to July 2008. {{cite web  | last = | first =
yaporn | title = จา พนมเผยตัวแล้ว อ้างเครียดเหตุหมดงบ ปรัชญาอัดทำกองถ่ายเสียหายกว่า 250 ล้าน | publisher = Matichon Online | date = 2008-07-25  | url = http://matichon.co.th/news_detail.php?id=42547&catid=8 | accessdate = 2008-07-25 }} 

Tony Jaa and the owner of Sahamongkol Film later made a joint press conference stating that the production and funding would continue after several concessions were agreed upon between Tony Jaa and Sahamongkol. Famed Thai action choreographer and Jaas mentor Panna Rittikrai was brought onto the project in the capacity of director to help complete the film.    In addition, Rittikrai added martial artist Dan Chupong to the cast. 

An international trailer for the movie was released during filming, showing the fictional setting in which Tony Jaas character is being rescued in the jungle by a group of martial artists of various styles, and trained to unify these different systems. However, production still encountered financial problems as it came to a close. In order to complete the production on time, the filmmakers decided to end Ong Bak 2 with a cliffhanger ending, and then continue the story in a sequel, Ong Bak 3, which was announced to begin production for a 2009 release.   

== Distribution ==
Worldwide distribution and sales rights to Ong Bak 2 were purchased by The Weinstein Company in March 2006. A little over a year later, Harvey Weinstein visited Bangkok and renegotiated a deal in which Sahamongkol Film International bought back most of the rights to the film, except for North America, which The Weinstein Company retains. {{cite web  | last = Frater  | first =
Patrick  | title = Weinsteins loosen Thai films rights | publisher = Variety  | date = 2007-05-09  | url = http://www.variety.com/article/VR1117964547.html?categoryid=19&cs=1
  | accessdate = 2007-06-11 }}  At the 2007 Cannes Film Festival market, Sahamongkol sold some rights to Germany-based Splendid Films. {{cite web  | last = Frater  | first =
Patrick  | title = Splendid takes Ong Bak 2 | publisher = Variety  | date = 2007-05-17  | url = http://www.variety.com/index.asp?layout=Cannes2007&jump=story&id=1061&articleid=VR1117965135&cs=1 | accessdate = 2007-06-11 }} 

On 10 February 2009, it was announced that the Wagner/Cuban Companies’ Magnolia Pictures acquired the U.S. distribution rights for Ong-Bak 2 under their Magnet label. The deal was negotiated by Tom Quinn, Senior Vice President of Magnolia, with Gilbert Lim of Sahamongkol Film International. 

== Critical reception ==
The film currently holds a 48% Rotten rating on Rotten Tomatoes based on 67 reviews. 

== Thailand reception ==
Despite political turmoil in the films native Thailand, in its opening weekend (8 December 2008) Ong Bak 2 grossed about 58 million baht ($2.06 million), according to Variety Asia Online, and was number one at the Thai box office. {{cite web
|url=http://varietyasiaonline.com/content/view/7650/1/ 
 |title=VarietyAsiaOnline
 |publisher=VarietyAsiaOnline
 |date=
 |accessdate=2010-08-16}}  Ong Bak 2 did better at the Thai box office than Tony Jaas previous film, Tom-Yum-Goong (film)|Tom-Yum-Goong.   

== Home video == dubbings were released throughout Asia, South America, Australia and New Zealand in the months shortly after the films premiere in its native Thailand. The film was released for the European Film Market on 6 February 2009.  The United States version was released on February 2, 2010, {{cite web
|last=Pollard
 |first=Mark
 |url=http://www.kungfucinema.com/magnolias-ong-bak-2-dvd-and-blu-ray-details-11821
 |title=Magnolia’s ‘Ong Bak 2′ DVD and Blu-ray details
 |publisher=Kungfucinema.com
 |date=2009-11-24
 |accessdate=2010-08-16}}   although it is already available in English language version. A bootleg all-region-compatible version with English subtitles of Ong-Bak 2 was internationally released April 2, 2009 on DVD, although this version is not as yet widely available. There are no significant reviews, such as on Rotten Tomatoes, yet.

== Sequel ==
  Khmer Palace was completed and seen by the press. {{cite web
|url=http://www.daradaily.com/th/news/newsdetail.php?newsid=9803 |title="เสี่ย"สั่งลุยหนัง"หนังองค์บาก3":: daradaily.com :: เว็บแรกสู่โลกดารา :: ข่าวดารา ปาปาราซซี่ |publisher=daradaily.com
 |date=
 |accessdate=2010-08-16}}  The studio hoped to have the film in theaters in late 2009.

Sia Jieang, an Executive of Sahamongkol, stated the film would feature more fights between Tony Jaa and Dan Chupong (the uncredited actor behind the mysterious, enigmatic and deadly "crow ghost" in Ong Bak 2, the only enemy who really gets the drop on Tien in the film). {{cite web
|url=http://thaifilmjournal.blogspot.com/2008/12/sia-jiang-there-will-be-ong-bak-3.html
 |title=Wise Kwais Thai Film Journal: News and Views on Thai Cinema: Sia Jiang: There will be an Ong-Bak 3 
 |publisher=Thaifilmjournal.blogspot.com |date=2008-12-18 
 |accessdate=2010-08-16}} 

==Video game==
Ong Bak Tri is being developed by Studio Hive and will be published worldwide by Immanitas Entertainment for PC, smartphones, PlayStation Network, and Xbox Live Marketplace. It will be a 2.5D side-scrolling brawler with "intense fighting action, impressive free-running sequences, and highly cinematic quick-time action events," according to the press release. The game, like the second and third films, is set in ancient Thailand. No official release date has been announced.  

== References ==
 

== External links ==
*     
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 