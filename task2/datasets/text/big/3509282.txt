Dulhan Hum Le Jayenge
{{Infobox film
| name           = Dulhan Hum Le Jayenge दुल्हन हम ले जायेंगे
| image          = Dulhan Hum Le Jayenge.gif
| image_size     = 
| caption        = 
| director       = David Dhawan
| producer       = Gordhan Tanwani
| writer         = Rumi Jaffrey
| narrator       = 
| starring       = Salman Khan Karisma Kapoor Om Puri Paresh Rawal Anupam Kher Kashmira Shah Farida Jalal Himani Shivpuri Johnny Lever
| music          = Himesh Reshammiya
| cinematography = Harmeet Singh
| editing        = David Dhawan
| distributor    = Yash Raj Films (India) Eros Entertainment (Middle East)
| released       = March 23, 2000
| runtime        = 146 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Dulhan Hum Le Jayenge ( ,   romantic comedy film starring Bollywood actors Salman Khan and Karisma Kapoor in the lead roles. 
The film did very well at the box office with a good opening and collected   according to box office india and was declared a Hit.  

==Plot==
Sapna (Karisma Kapoor) has been brought up by her three doting, but eccentric uncles. Uncle no. 1 (Paresh Rawal) is a Hindu religious fanatic, while Uncle. No. 2 (Om Puri) is a former wrestler who is very in to fitness, and makes Sapna do intense workout routines daily. Uncle no. 3 (Anupam Kher) is in to fashion and western music. The uncles all wish for her to marry, but their differences clash, as each of them want the boy to have the same interests as them. Sapna is tired of dancing to her uncles tunes all her life, and wishes to take a group trip to Europe. However, when she expresses her wishes to her nanny, Mary (Himani Shivpuri), her uncles fire the nanny for giving Sapna such foolish ideas. However, Mary works for another family, and tells them of Sapnas plight. She shares Sapnas photo with Raja (Salman Khan). He is pleased with her photo and is determined to make her his bride. Meanwhile, Sapna looks to a last resort and tries to run away, but Uncle no. 3 catches her and volunteers to take her to the airport himself. On her travels through Europe, Raja creates nothing but trouble for her, but circumstances separate them from the rest of the tour group, and Raja saves Sapnas life. They fall in love, and wish to marry once they return to India, but Raja must first impress all three of her uncles. Through a series of comic events, Raja wins their hearts. He and Sapna happily marry in the end.

== Cast ==
*Salman Khan ...  Raja Oberoi 
*Karisma Kapoor ...  Sapna
*Kader Khan ...  Mr. Oberoi  
*Johnny Lever ...  Tour Manager Chirkund/Chirkunda
*Om Puri ...  Bhola Nath 
*Paresh Rawal ...  Prabhu Nath 
*Anupam Kher ...  Vicky Nath 
*Kashmira Shah ...  Lovely 
*Farida Jalal...  Mrs. Oberoi 
*Himani Shivpuri ...  Mary 
*Deepak Tijori ...  Smuggler 
*Dara Singh ...  Sapnas grandfather
*Usha Bachani...  Smugglers girlfriend

==Songs==
{{Infobox album |  
 Name = Dulhan Hum Le Jayenge |
 Type = Soundtrack |
 Artist = Himesh Reshammiya |
 Cover = |
 Released = 2000 |
 Recorded = | Feature film soundtrack |
 Length = 40:52|
 Label =  Tips Industries|Tips|
 Producer = Himesh Reshammiya | 
 Reviews = |
 Hello Brother   (1999) |
 This album = Dulhan Hum Le Jayenge (2000) |
 Next album = Kurukshetra (2000 film)|Kurukshetra  (2000) |
}}

{{Album ratings
| rev1 = Planet Bollywood
| rev1Score =    
}}

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s)!! Length

|-
| 1
| "Dulhan Hum Le Jayenge" Sunita Rao
| 05:24
|-
| 2
| "Mujhse Shaadi Karogi"
| Kumar Sanu, Alka Yagnik, Suresh Wadkar & Shankar Mahadevan, 
| 04:37
|-
| 3
| "Chhamiya"
| Alka Yagnik, Sonu Nigam
| 05:43
|-
| 4
| "Dheere Dheere Chalna"
| Alka Yagnik, Sonu Nigam
| 05:45
|-
| 5
| "Tera Pallu" 
| Sonu Nigam, Alka Yagnik
| 05:33
|-
| 6
| "Hai Na Bolo"
| Alka Yagnik, Kumar Sanu
| 02:11
|-
| 7
| "O Mr Raja"
| Alka Yagnik, Sonu Nigam
| 06:41
|-
| 8
| "Pyar Dilon Ka Mela"
| Alka Yagnik, Sonu Nigam
| 04:58
|}

==References==
 

==External links==
* 

 

 
 
 
 
 