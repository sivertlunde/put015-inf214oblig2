Silent Cry (film)
  British thriller Julian Richards, Craig Kelly and released in 2003 in film|2003.

== Synopsis ==
The main character, Rachel, is devastated to learn her newborn has died shortly after birth, but she begins to suspect the baby has been abducted. Certain that theres only one way to find out the truth, Rachel returns to the hospital. Here she encounters the menacing Dennis Betts and in an attempt to flee from him, she ís forced to hide in a car belonging to Daniel Stone, a hospital porter. Initially reluctant to help, Daniels conscience eventually gets the better of him. The plot thickens further with the death of Rachels best friend Annie and the discovery that Dennis Betts is actually a policeman, with his own very personal reasons for pursuing Rachel. As Rachel and Daniel race through Londons nightscape, desperate to stay one step ahead of Betts, every discovery unleashes further hell, extending way beyond the disappearance of Rachels baby. Their only solid lead seems to be Joanne, a young prostitute, whose own baby provides a link. But with Betts systematically eliminating anyone in his way, a further web of conspiracy unfolds and Rachel and Daniel are led to her old family doctor, Robert Barrum 

== Release ==
 Channel 5, Germany by ZDF with the title Albtraum ohne Ende, Eastern Europe and Latin America by HBO.

Silent Cry is available on DVD in North America where it was released by MVD,  Australia where it was released by DV1 Entertainment, Germany where it was released by Starmedia with the title Schrei in der Dunkelheit and Sweden where it was released by Horsecreek Entertainment with the title Allt Har Ett Pris

== Awards ==

*Gold Remi Award - WorldFest-Houston International Film Festival
*Best Actress - Love Is Folly Film Festival, Vanya, Bulgaria.
*Best Actress - Bruxelles Film Festival, Belgium.

== Festivals ==

*Welsh International Film Festival - Cardiff, UK
*Fantasporto Film Festival - Portugal
*WorldFest-Houston International Film Festival - USA
*Commonwealth Film Festival - Manchester, UK
*Fantasy Filmfest - Germany
*Malibu Film Festival - USA
*Cairo International Film Festival - Egypt
*Valencia International Film Festival - Spain
*Bradford Film Festival - UK

==References==
 

== External links ==
* 

 
 
 
 
 
 

 