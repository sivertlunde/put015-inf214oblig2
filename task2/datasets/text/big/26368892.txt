His Lordship
 
{{Infobox film
| name           = His Lordship
| image          = HisLordship.jpg
| image_size     = 
| caption        = Bert serenades his girl Lenina
| director       = Michael Powell Jerome Jackson
| writer         = Oliver Madox Hueffer (novel) Ralph Smart
| narrator       = 
| starring       = Jerry Verno Janet McGrew Ben Welden Polly Ward
| music          = Richard Addinsell with song by Leslie Holmes and Clay Keyes Direction, Maurice Winnick
| cinematography = Geoffrey Faithfull
| editing        = Arthur Seabourne   
| distributor    = United Artists
| released       = 5 December 1932 (UK)
| runtime        = 79 min.
| country        = UK
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Quota quickie.

==Plot==
Cheerful Cockney Bert Gibbs inherits a title from his father and becomes Lord Thornton Heath. But then he meets up with movie star Ilya Myona and when his mother asks about her, Bert implies they are engaged. After some adventures with some dubious Russian types Berts girl Lenina eventually wins him back.

==Cast==
* Jerry Verno as Bert Gibbs aka Albert Lord Thornheath
* Ben Welden as Washington Roosevelt Lincoln
* Polly Ward as Leninia
* Peter Gawthorne as Ferguson, the Butler
* Muriel George as Mrs. Emma Gibbs Michael Hogan as Comrade Curzon

==Subsequent history== safety film NFT in camp classic.

==References==

===Notes===
 

===Bibliography===
 
* Chibnal, Steve. Quota Quickies : The Birth of the British B Film. London: BFI, 2007. ISBN 1-84457-155-6
* Powell, Michael. A Life in Movies: An Autobiography. London: Heinemann, 1986. ISBN 0-434-59945-X.
 

==External links==
* 
* 
* 
*  reviews and articles at the  

 

 
 
 
 
 
 
 
 


 