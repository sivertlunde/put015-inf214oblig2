Keerthi Chakra (film)
{{Infobox Film 
| name = Kirti Chakra 
| image = Kirtichakra_film.jpg
| caption = Official theatrical poster
| director = Major Ravi
| writer = Major Ravi
| starring = Mohanlal Jiiva  Gopika Prakash Raj Ramesh Khanna Cochin Haneefa Biju Menon  Lakshmi Gopalaswamy 
| cinematography = Thiru
| producer = R. B. Choudary
| music = Joshua Sridhar Super Good Films Super Good Films
| released = 
| runtime = 144 min
| country = India Malayalam 
| budget =  12 crore
| gross =  22 crore
}} insurgency in the Indian controlled region of Kashmir. The political situation of the conflict is portrayed from an Indian perspective in the film.
 Tamil as Aran.       and some scenes were added for Prakash Raj in Tamil version.

== Plot ==
Major Mahadevan (Mohanlal) is an officer deputed to the National Security Guards (NSG). He has been assigned to counter-terrorism duties in Kashmir and commands an elite group of NSG commandos. He calls back his buddy Havildar Jaikumar (Jiiva) from his honeymoon because he has information about major activities of the militants; he feels Jaikumar is needed for the team. The commandos raid a mosque in civilian dress and seize a big collection of arms and explosives. Later, during an operation focused on some cottages near the Dal Lake, a commando in the team is killed and an outraged Jaikumar shoots down a captured terrorist who challenges the commandos. A human rights activist (Shwetha Menon) learns about this and threatens to file a complaint with the government. She later understands the situation and the sincerity of the commandos and decides not to proceed with the complaint.

Meanwhile, a group of terrorists from Afghanistan and Pakistan conspire to hit the Hazratbal Shrine with a missile; they think this will create unrest in the region and that the Indian army will be blamed. They hijack a house that offers a good vantage point on the shrine and plant the missile there. Some of them rape three girls in the house in front of their father and shoot a person who protests. A military patrolling vehicle hears the gunshot; as they approach the house, they are fired upon. The patrolling team intimates the NSG, and Mahadevan and his team immediately join the battle. After a long operation, the team kills the terrorists, frees the civilian hostages and disarms the missile. But a civilian informs that the chief of the terrorists is not to be seen among the corpses, and the commandos start a combing operation in the house. They locate the terrorist and Mahadevan recognises the man as the one who killed his wife and daughter. The terrorist is killed but he manages to shoot at Mahadevan. Mahadevan is covered by Jaikumar, who gets hit by the bullet and dies. Jaikumar is awarded the Kirti Chakra posthumously.

== Production ==
Major Ravi, a resigned officer of the Indian army, planned a film on Kashmir militancy based on real-life incidents while he was under duty in counter-terrorism activities in Punjab & Kashmir. He wrote the film which reveals the actual mode of working of Islamic terrorist groups within India: their recruitment, training, target choosing, terror strikes, propaganda, and how Indian armed forces tackle the attacks via its special services forces. The movie develops with this backdrop and tries to convey the dedication and selfless service defence personnel exhibit while on duty.
Ravi got R. B. Chaudhary of Super Good Films, a major production house in Tamil Nadu, to produce the film.    The film was planned to release a dubbed version in Tamil Nadu as well. Chaudharys son Jiiva was cast in a major role in the film. A few additional scenes were shot for the Tamil version, Aran (film)|Aran. The filming was predominantly done in Srinagar and Pollachi. 

==Cast==

{| class="wikitable" border="1" st#le="width:50%;"
|- "
! Actor !! Role
|- Mohanlal || Major Mahadevan
|-
| Jiiva || Havildar Jaikumar
|- Lakshmi Gopalaswamy||Mahadevans wife
|-
| Gopika || Jaikumars wife
|-
| Biju Menon || Gopinath
|-
| Santhosh Jogi || Military Officer
|- Spadikam George||Krishnakumar
|- Cochin Hanifa||Nairettan
|- Ramesh Kanna||Chinna Thambi
|- Shweta Menon||Human Rights Activist
|- Saikumar (Malayalam Dutta
|- Kashmiri girl
|- Shammi Thilakan||Hari
|- Kannan Pattambi||Tea boy Appukuttan (cameo)
|}

==Reception==
Kirti Chakra received generally positive reviews. The Rediff.com reviewer describes the casting of Mohanlal as "near perfect" and comments, "though people may complain that his bulky physique doesnt suit the role of a commando… Mohanlal overcomes this shortcoming by the agility and the emotional roundness he gives to his character."  He also thought Major Ravi was "successful in keeping the feel of the film authentic most of the time." On the negative side, he found that "there are certain times when the narrative lapses into the stereotypical terrain." He also thought the film "lacks in pace" and that "except for the main characters; the others are cardboardish." The reviewer wraps up his review by commenting that "the effort that went behind making Kirtichakra is commendable."  
The Indiaglitz.com review calls the film one that parallels national standards. The reviewer goes on to state that "proving to be an effective story teller and a crafty film maker, the film and the director holds the viewers till the end with very little short comings."    The major actors  and the cinematographer were also praised. The reviewer did not find too many negative points about the film and described it as a "technically sound, visually striking flick largely based on reality." 

Kirti Chakra was a box-office success. The film got a great opening and the film went on to become a hit.  However, Aran (film)|Aran - the Tamil version, which had additional footage of Prakash Raj, Jiiva|Jeeva, comedy tracks with Ganja Karuppu and song montages featuring Jeeva and the lead actress Gopika, met with a poor at the box-office, with the additional scenes being lamented as the reason for the failure. 

==Awards==

===Kerala State Film Awards=== Best Screenplay - Major Ravi

===Asianet Film Awards=== Best Feature Film on National Integration Best Actor - Mohanlal Best Director - Major Ravi Best Star Pair - Jiiva and Gopika

== Soundtrack ==
The films soundtrack contains 4 songs, all composed by Joshua Sridhar and Lyrics by Gireesh Puthenchery.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Pooncholai Kiliye"
| Karthik, Asha G. Menon
|-
| 2
| "Kaveri Nadiye"
| Karthik, Asha G. Menon
|-
| 3
| "Mukile Mukile"
| M. G. Sreekumar
|-
| 4
| "Khuda Se Mannath"
| Kailash Kher
|}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 