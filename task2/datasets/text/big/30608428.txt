Eve (1968 film)
 
{{Infobox film name     = Eve image    = "Eve"_(1968_film).jpg caption  =  director  Robert Lynn producer       = Oliver A. Unger Harry Alan Towers screenplay     = Harry Alan Towers
| based on      = a story by Harry Alan Towers starring       =  Robert Walker Jr.  Fred Clark  Herbert Lom  Christopher Lee Celeste Yarnall music          = Malcolm Lockyer cinematography =  Manuel Merino editing        =  Allan Morrison   released       = July 1968	(New Orleans, Louisiana) (USA) (premiere) studio         =  distributor    = Anglo-Amalgamated Film Distributors  (UK) Commonwealth United Entertainment  (US) runtime        = 94 min. country        = Spain | UK | Liechtenstein | USA language       = English
}} Robert Lynn and Jeremy Summers and starring Robert Walker Jr., Fred Clark, Herbert Lom, Christopher Lee, and introducing Celeste Yarnall as Eve. When the director quit midway through filming, Spanish horror film director Jesus Franco was brought in to finish the job.  The film was a co-production between Britain, Spain, Liechtenstein and the United States, and location scenes were filmed in Brazil.  It was also released as Eva en la Selva, The Face of Eve (in the UK), Eve in the Jungle, or Diana, Daughter of the Wilderness. 

==Plot==
  Amazon jungle runs across a lost young woman named Eve,  who is worshipped as a goddess by jungle natives. Eve is also being pursued by a showman who wants her for his freak show; by the natives who now want to kill her for helping a white man; and by an explorer, Eves grandfather, who wants to silence her.   

==Cast==
* Celeste Yarnall - Eve
* Robert Walker Jr. - Mike Yates
* Herbert Lom - Diego
* Christopher Lee - Colonel Stuart
* Fred Clark - John Burke
* Rosenda Monteros - Conchita
* Maria Rohm - Anna
* José María Caffarel - José
* Ricardo Díaz - Bruno

==Critical reception==
TV Guide called it a "very poorly done story of a Tarzaness" ;  while Dave Sindelar wrote in Fantastic Movie Musings and Ramblings, "its a dull affair, especially during the long middle section where the hero returns to civilization, and any interest it does generate is more due to the presence of several familiar faces (Herbert Lom, Christopher Lee, Fred Clark) than anything that actually happens. One fun thing to do in the movie is to keep track of how many characters die as a result of their own monumental stupidity; I count at least three."  

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 


 
 