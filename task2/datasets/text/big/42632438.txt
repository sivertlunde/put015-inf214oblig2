Kappal
{{Infobox film
| name           = Kappal
| image          = Kappal.jpg
| alt            = 
| caption        = Kappal Movie Poster
| director       = Karthik G. Krish
| producer       = {{Plainlist|
* P.L.Arulraj
* Senthil
* Sudhan
* Jayaram
* Thiagarajan
* Umashankar
* Guruprasad
* Mike Dolphin
}}
| writer         = Karthik G. Krish
| starring       = {{Plainlist| Vaibhav
* Sonam Bajwa Karunakaran
* VTV Ganesh
* Arjunan
}}
| music          = Natarajan Sankaran
| cinematography = Dinesh Krishnan Anthony
| studio         = I Studios Entertainment
| distributor    = S Pictures
| released       =  
| runtime        = 
| country        = India
| language       = Tamil
| budget         = 
| gross          = 
}} Tamil comedy Vaibhav and Karunakaran appear in supporting roles. The film was released by Shankars S Pictures on December 25, 2014. 

==Cast== Vaibhav as Vasu
* Sonam Bajwa as Deepika Karunakaran as Kanaga Sabapathi
* VTV Ganesh as Nelson
* Arjunan as Kalyana Sundaram
* Venkat Sundar as Pattabi
* Karthik Priyadarshan as Venky
* Robo Shankar as Seenu Anna
* Rajan Iyer as Deepikas father
* Raviraj as Venkys father
* George Vishnu
* Theni Murugan as Tea master
* Boys Rajan
* Swathi Shanmugam as Gayathri
* Latsumi
* Vineetha
* Preethi Kitchappan

==Plot==

The story starts with Vasu (Vaibhav Reddy|Vaibhav) narrating his story from childhood. He has four close friends (Karunakaran (actor)|Karunakaran, Arjunan, Venkat Sundar, Karthik Priyadarshan) and they hero worship Seenu Anna (Robo Shankar) for his dedication to friendship and one day he advises to them that marriage will spoil friendship and all the friends take this seriously, except for Vasu, who always has a liking for girls, and he is forced to promise not to marry lifelong along with his friends. After failing many attempts of falling in love at school and college (his friends spread rumor that Vasu is gay), Vasu decides to leave to Chennai so that he would get an opportunity to love a girl. He stays with Nelson (VTV Ganesh) who has affair with many rich and beautiful girls and he persuades him to trap a rich girl citing that middle class and college girls will have bigger competition and high expectations but super rich girls have a very less expectations and many guys do not try to date them assuming its impossible to woo them. Also he hands over the pub membership card to Vasu so that he can get to see many rich girls. During a visit to pub, arrives Deepika (Sonam Bajwa) in Audi car but with a male friend who collapses due to over drinking. Vasu falls in love with her instantly and he helps her lift the friend and also accompanies her to the pub and befriends her. But next day when he visits her home she is unable to recall about him since she was drunk the previous night and rejects his love advances. After all the cop thrashing and goons threatening episode, Deepika falls in love with him. Meanwhile his friends come to know about his love affair and threaten him to break up with her. But when he acts like committing suicide they act as if they accept his love but secretly plan to irritate Deepika and break their love. Deepika gets annoyed by their behavior and soon they manage to create a rift between both and the duo get separated. Deepika gets depressed and decides to marry a family friend who keeps proposing her from the beginning. Also, Vasu is very depressed after the breakup, and soon his friends decide that the only way they can see their friend happy is to patch him up with his lady love, and they plot many master plans to stop the wedding, and finally the wedding is stopped. Deepika then gets married to Vasu and they lead a happy life. Vasu also keeps in touch with his friends forever.

==Production== Shankar in Jai or Sivakarthikeyan for the role, before selecting Vaibhav Reddy|Vaibhav.  The film began its shoot in March 2013 with scenes shot in Chennai with the lead cast. Initially Ashrita Shetty had signed up to portray the films lead actress but delays meant that she was replaced by newcomer Sonam Bajwa.  Natarajan Sankaran replaced Santhosh Narayanan as the films music composer,  although the latter agreed to sing a song for the films soundtrack. 
 Karthik in Shankar bought the film and made plans to release it, marking a comeback to film production after a four-year hiatus. 

==Soundtrack== Vijay and Vikram being present at the event.  Kappal songs topped in FM station like radio mirchi in top 5 for 6 weeks and Suryan FM top 10 and held 2nd place in the iTunes chart for 2 weeks, especially Kadhal Cassatta become the most favorite song in all stations.  

{{tracklist
| headline        =Kappal (Original Motion Picture Soundtrack)
| extra_column    = Singer(s)
| total_length    = 21:26
| lyrics_credits  = yes
| title1          = Oru Cup Acid
| lyrics1         = Madhan Karky
| extra1          = Deepak 
| length1         = 04:06
| title2          = Kaadhal Cassata
| lyrics2         = Madhan Karky
| extra2          = Sathyaprakash, Saindhavi
| length2         = 04:11
| title3          = Ekkachakkamaai 
| lyrics3         = Kabilan
| extra3          = Alphons Joseph, Ankita Mathew
| length3         = 04:13
| title4          = Friendship
| lyrics4         = Kabilan
| extra4          = Anthony Daasan
| length4         = 03:31
| title5          = Kaali Pasanga
| lyrics5         = Karthik G Krish
| extra5          = Santhosh Narayanan
| length5         = 03:22
| title6          = Ilayarajas Ooru Vittu Ooru Vandu Remix 
| lyrics6         = Gangai Amaren
| extra6          = Sriram Parthasarathy
| length6         = 02:55
}}

==Critical reception==
The Hindu wrote, "Kappal fulfils its only purpose — to be funny...here’s actor Vaibhav’s big breakthrough as a solo hero. This Kappal sails strong for the most part".  Sify called Kappal a "watchable fun ride with witty dialogues and good writing".  Rediff gave the film 2.5 stars out of 5 and wrote, "There may be flaws, but there is no denying that Karthik G Krish’s Kappal is thoroughly entertaining. But do remember to leave your thinking cap at home".  The Times of India gave 3 stars out of 5 and wrote, "Karthik G Krish, is not after heavy philosophy but entertainment. So, he constructs the film as a comedy that is often silly, at times offensive but overall funny".  silverscreen.in wrote, "Kappal is like that old joke, which only gets better with age. It also sustains the good spirit that the riotous pattimandram (debate talk show) of Solomon Pappaiah offers on festive mornings".  The New Indian Express wrote, "Directed by a debutant, it’s meant to be a total comic entertainer. But then it depends on each one’s perception of what they feel are genuine comic moments"...going on to add that the film was "comedy probably targeted at the lowest common denominator in the audience". 

==Box office==
The film is reported to have grossed around   in TN after the first 11 days. 

==References==
 

==External links==
*  

 
 
 
 
 
 