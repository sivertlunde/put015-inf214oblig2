Goodbye, Mr. Chips (1939 film)
 
{{Infobox film
| name           = Goodbye, Mr. Chips
| image          = Goodbye, Mr. Chips (1939 film) poster.jpg
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Sam Wood
| producer       = Victor Saville
| screenplay     = {{Plainlist|
* R. C. Sherriff
* Claudine West
* Eric Maschwitz
}}
| based on       =  
| starring       = {{Plainlist|
* Robert Donat
* Greer Garson
* Terry Kilburn
}}
| music          = Richard Addinsell
| cinematography = Freddie Young
| editing        = Charles Frend
| studio         = {{Plainlist| MGM
* Denham Studios
}}
| distributor    = MGM
| released       =  
| runtime        = 114 minutes
| country        = United Kingdom
| language       = English
| budget         = $1,051,000  . 
| gross          = $3,252,000 
}}
 romantic drama James Hilton, MGM at Denham Studios, Goodbye, Mr. Chips was voted the 72nd greatest British film ever in the BFI Top 100 British films poll.

==Plot==
In 1928, Mr Chipping (Robert Donat), a retired schoolteacher of 83, is kept home by a common cold|cold. He falls asleep and his school teaching career is related in Flashback (narrative)|flashback.
 German teacher, Max Staefel (Paul Henreid), saves him from despair by inviting him to share a walking holiday to his native Austria. While mountain climbing, Chipping encounters Kathy Ellis (Greer Garson), a feisty English suffragette on a cycling holiday. They meet again in Vienna and dance to the Blue Danube Waltz. This piece of music is used as a leitmotif, symbolising Chippings love for her. Chipping remarks that the Danube appears blue, but only to those who are in love. On another part of the same boat, as Kathy looks at the river, she notices that it is blue. Even though Kathy is considerably younger and livelier than Chipping, she loves and marries him. They return to England, where Kathy takes up residence at the school, charming everyone with her warmth.
 Latin puns. As the years pass, Chips becomes a much-loved school institution, developing a rapport with generations of pupils; he teaches the sons and grandsons of many of his earlier pupils.

In 1909, when pressured to retire by a more "modern" headmaster, the boys and the board of governors of the school take his side of the argument and tell him he can stay until he is 100, and is free to pronounce Cicero as SIS-er-ro, and not as KEE-kir-ro.
 the First Roll of Honour every Sunday the names of the many former boys and teachers who have died in battle. Upon finding out that Max Staefel has died fighting on the German side, Chips also reads out his name in chapel.

He retires permanently in 1918. He is on his deathbed in 1933 when he overhears his friends talking about him. He responds, "I thought you said it was a pity, a pity I never had children. But youre wrong. I have! Thousands of them, thousands of them – and all boys."

==Cast==
*   come out of a puddle."
* Greer Garson as Katherine. Garson was initially offered a contract for MGM in 1937, but refused all the minor parts she was offered until she was given this role.
* Lyn Harding as Dr John Hamilton Wetherby Doctor of Divinity|D.D. (Cantab.), headmaster of Brookfield when Chips first arrives.
* Paul Henreid as Max Staeffel Master of Arts|M.A. (Oxon.), the German master.
* Terry Kilburn as John Colley, Peter Colley I, II and III, several generations of pupils from the same family taught by Mr. Chips
* John Mills as Peter Colley as an adult Scott Sunderland as Sir John Colley David Croft as Perkins - Greengrocers boy (uncredited)
* David Tree as Mr Jackson Bachelor of Arts|B.A. (Cantab.), new history master at Brookfield. 
* Simon Lack as Wainwright (uncredited)
* Judith Furse as Flora
* Milton Rosmer as Chatteris
* Frederick Leister as Marsham
* Louise Hampton as Mrs. Wickett
* Austin Trevor as Ralston
* Edmond Breon as Colonel Morgan
* Jill Furse as Helen Colley

==Filming locations== Denham in Buckinghamshire. Around 200 boys from Repton School stayed on during the school holidays so that they could appear in the film. 

==Score==
Richard Addinsells score for the film has been included in a CD of his work. The liner notes of the CD include the lyrics for the Brookfield School song which is heard over the beginning cast credits as well as throughout the film itself. The lyrics in the body of the film are all but unintelligible, but per the notes, the lyrics are as follows: 
:Let the years pass but our hearts will remember,
:Schooldays at Brookfield ended too soon.
:Fight to the death in the mire of November,
:Last wicket rattles on evenings in June,
:Grey granite walls that were gay with our laughter,
:Green of the fields where our feet used to roam.
:We shall remember, whate’er may come after,
:Brookfield our mother and Brookfield our home.
==Box Office==
According to MGM records the film earned $1,717,000 in the US and Canada and $1,535,000 elsewhere resulting in a profit of $1,305,000. 
==Academy Awards and nominations== Outstanding Production, Best Director, Best Writing, Best Film Best Sound. Gone with When Tomorrow Comes.)

{| class="wikitable" border="1"
|-
! Award !! Result !! Nominee
|- Outstanding Production
|   Gone with the Wind (Selznick International Pictures (David O. Selznick, producer)) 
|- Best Director
|   Gone with the Wind 
|- Best Actor
|  
| Robert Donat
|- Best Actress
|   Gone with the Wind 
|- Best Writing, Screenplay
|   Gone with the Wind 
|- Best Film Editing
|   Gone with the Wind 
|- Best Sound, Recording
|   When Tomorrow Comes 
|-
|}

==1969 remake==
Goodbye, Mr. Chips (1969 film)|Goodbye, Mr. Chips was remade as a musical in 1969, starring Peter OToole and Petula Clark.

==Notes==
 

==External links==
*  
*  
*  
*  
Streaming audio 
*   on Lux Radio Theater: November 20, 1939 
*   on Hallmark Playhouse: September 16, 1948  NBC University Theater: July 9, 1949 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 