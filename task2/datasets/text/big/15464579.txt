July Days (film)
{{Infobox film
| name           = July Days
| image          =
| image_size     =
| caption        =
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Hal Roach H. M. Walker
| narrator       = Jack Davis William Gillespie
| music          =
| cinematography =
| editing        =
| distributor    = Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

July Days is the 16th Our Gang short subject comedy released. The Our Gang series (later known as "The Little Rascals") was created by Hal Roach in 1922, and continued production until 1944.

==Plot== Mickey immediately falls in love with the familys daughter, Mary Kornman|Mary, and tries whatever he can to gain her affections. He tries taking her for a ride on his goat-powered wagon, and later dresses up as a knight. In the interim, the village blacksmith, "Dad" Anderson (Richard Daniels), receives a lucrative contract to produce a creation of his: a sail-propelled scooter. The gang is lucky enough to get a hold of a few of these scooters, and happily sail down the city streets.
 

==Notes== Mary and Jackie are Jack is the neighborhood bully.

When the television rights for the original silent Pathé Exchange Our Gang comedies were sold to National Telepix and other distributors, several episodes were retitled. This film was released into TV syndication as Mischief Makers in 1960 under the title "Puppy Love". About two-thirds of the original film was included.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey Jack Davis as Jack
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Ernie Morrison as Ernie
* Dinah the Mule as Herself

===Additional cast===
* Julia Brown as Girl with freckles
* Richard Daniels as Dad Anderson William Gillespie as Businessman

==See also==
* Our Gang filmography

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 


 