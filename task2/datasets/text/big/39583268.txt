C/O Sir
   
{{Infobox film
| name           = C/O Sir
| image          =
| image_size     =
| border         =
| alt            =
| caption        =
| director       = Kaushik Ganguly
| producer       = Jaspreet Kaur
| writer         =
| screenplay     =
| story          =
| based on       =  
| narrator       = See below
| music          = Raja Narayan Deb
| cinematography = Sirsha Ray
| editing        = Bodhaditya Banerjee
| studio         =
| distributor    =
| released       =   
| runtime        =
| country        = India
| language       = Bengali
| budget         =
| gross          =
}} Bengali romantic thriller film directed by Kaushik Ganguly and produced by Jaspreet Kaur.    The film tells story of a blind school teachers life and his struggle to prevent the school being sold.       The lead character is played by Saswata Chatterjee. 

== Plot == retinal disorder. He must now rely on his sense of hearing. After becoming irritable and helpless due to the situation, how he tries to stop the school being sold and how he tackles the situation himself (as his friends are turning foes) becomes the issue of the story.   

== Cast ==
 
* Saswata Chatterjee as Jayabrata Ray, a blind teacher, whose only solace in life is his school and his cottage, in which he resides. Due to sudden loss of eyesight, Jayabrata is becoming unsure about who are his real friends and well-wishers. He relies on his hearing power, and respects and trusts the most Mr. Das, who was Jayabratas teacher in the same school, besides being a friend of his father.
* Indraneil Sengupta as Ranabir, Jayabratas school friend and a property lawyer in Kolkata. Jayabrata seeks his help to save his school and his cottage. He later turns out to be Jayabratas foe, taking advantage of his blindness and trying to sneak away with all the money. The purpose of his double-game is never clearly told (maybe it was for the huge amount of money). Nevertheless, he is arrested in the end of the film.  
* Raima Sen as Susmita Gupta/Sushmita Gupta/Reshmi Singh, an advocate who was sent by Ranabir to look after the legal matters of Jayabratas school. She is later revealed to be Ranabirs girlfriend, working under a false identity to con Jayabrata, as per the instructions of Ranabir. Later she feels guilty and confesses everything to Jayabrata. She is also arrested in the end of the film.
* Sabyasachi Chakraborty as the Headmaster in Jayabratas school. A straightforward man, he first agreed to sell the school to Mr. Ghosh. As Jayabratas cottage is within the school premises, he tries to convince Jayabrata to sell it. When refused, he takes a strict step and fires Jayabrata. Later, upon sensing his danger, he goes to warn Jayabrata against Sushmita. In the end, he drops the idea of selling the school.
* Kunal Padhy as Mr. Ghosh, an NRI interested in buying the school and rebuilding it as an international educational institute. 
* Sudipta Chakraborty as Meghna Chatterjee, Jayabratas colleague and well-wisher. She is the first one to know about Sushmitas secret identity and report it to the Headmaster. She cares for Jayabrata and often visits him and cooks food for him.
* Kaushik Ganguly as Mr. Niogi, advocate of the schools governing body. He was also involved in the moneymaking plot with Ranabir. Although it is never shown in the film, perhaps he is the one who murdered Mr. Das, the respected old teacher of Jayabrata. He is also arrested in the end of the film.
* Anjan Mahato as Tipu, a small role as advocate assistant to Mr. Niogi of the schools governing body, perhaps helped police in arrest of Mr. Niogi, in the end of the film.
* trisankar Bera as school student

== Production ==

Director Kaushik Ganguly said in 2012 that he had planned the film "for a very long time".  The film was based in part on some of his personal experiences, including eight years when he worked as a school teacher and his observations of friends who are visually challenged.  He wrote a script for the film a long time before he started work on it, and he initially offered the role of Jayabrata Ray, the blind school teacher, to Prasenjit Chatterjee. Ultimately, he discarded his original script and started over with a new script and cast.   He told an interviewer, "I always pen the final draft of my film after having finalized the actors." 
 Dow Hill School. The school scenes were shot in Dow Hill School and Victoria Boys School. Gangulys wife Churni Ganguly was a student of Dow Hill School and Gangulys father-in-law and Churni Gangulys father was a teacher of Victoria Boys School. 

==Soundtrack==
{{Infobox album
| Name = C/O Sir
| Longtype = to C/O Sir
| Type = Soundtrack
| Artist = Raja Narayan Deb
| Cover =
| Border = yes
| Alt =
| Caption =
| Released = 2013
| Recorded = Feature film soundtrack
| Length =  
| Language = Bengali
| Label =
| Producer =
| Reviews =
| Last album = Kidnapper (2013 film)|Kidnapper (2013)
| This album = C/O Sir (2013)
| Next album = Ashchorjo Prodip (2013)
}}

The music of C/O Sir was composed by Raja Narayan Deb, and the lyrics were penned by Srijato.    

===Track listing===
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    = 9:59

| all_writing     =
| all_lyrics      =
| all_music       =

| writing_credits = no
| lyrics_credits  = no
| music_credits   = no

| title1          = Chere Jaoar Gaan
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Rupankar Bagchi
| length1         = 2:58

| title2          = Themey Jai
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Arijit Singh
| length2         = 4:03

| title3          = Sleeping Piller Raat
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Bonnie Chakraborty
| length3         = 2:58 Title4 = Dekchi Setai singer = Anupam Roy}}

== Release and reception== The Telegraph and In.com praised the acting and photography but found the story disappointing, particularly in the second half.     On In.com, Amina Khatoon gave the film an overall rating of 2.5 out of 5. Khatoon said the film would "fall short of expectations" in comparison with the "mammoth success" of Gangulys previous film, Shabdo.    In The Telegraph, Barun Chanda said the film was Gangulys "most commercial film ... so far" and suggested that it demonstrated the directors "new-found maturity and confidence".  In the Times of India, reviewer Jaya Biswas gave it three stars out of five, praising the cinematography and saying the film was "worth a watch", while faulting the plot for a "slacken  pace in second half as the director tries to tie too many loose ends in haste, making the climax predictable" and describing the music as "just about okay". 

==Awards and Nominations==

;Zee Bangla Gaurav Samman
*Best Supporting Actor – Female – Sudipta Chakraborty (Nominated)
*Best Music Director – Raja Narayan Deb (Nominated)
*Best Playback Singer – Male (Film) – Bonnie Chakraborty for Neel Sleeping Piller Raat (Nominated)

== See also ==
* Black (2005 film)|Black

== References ==
 

==External links==
**  

 

 
 
 
 