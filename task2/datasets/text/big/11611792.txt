I'm Dangerous Tonight
 
{{Infobox film
| name           = Im Dangerous Tonight
| image          = Im-dangerous-tonight.jpg
| alt            =
| caption        = Promotional poster
| director       = Tobe Hooper
| producer       = Bruce Lansbury Boris Malden Philip John Taylor Michael Weisbarth
| screenplay     = Bruce Lansbury Philip John Taylor
| story          = Cornell Woolrich Madchen Amick Corey Parker Daisy Hall R. Lee Ermey Natalie Schafer Anthony Perkins Dee Wallace
| music          = Nicholas Pike
| cinematography = Levie Isaacks
| editing        = Carl Kress
| studio         = MCA Television Entertainment (MTE)
| distributor    = USA Network
| released       = 8 August 1990  (original airing) 
| runtime        = 100 min.
| country        = United States
| language       = English
}}
 horror television film, directed by Tobe Hooper and starring Mädchen Amick. It made its debut on the USA Network on 8 August 1990, before being released on home video. Loosely inspired by a novella by Cornell Woolrich, the film revolves around a cursed Aztec ceremonial cloak that possesses anyone who wears it. Young college student Amy (Amick) decides to make a dress out of the cloth. Once she dons the dress, she falls under the spell and becomes a remorseless killer.

== DVD release ==

Universal has yet to announce any plans to release the film onto DVD, as of 9 April 2012.

== External links ==

*  
*  

 

 
 
 
 
 

 