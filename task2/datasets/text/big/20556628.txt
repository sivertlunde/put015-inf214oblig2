Sabar Uparey
 
{{Infobox film
| name           = Sabar Uparey
| image          = Sabar_Uparey_DVD_cover.png
| image_size     = 200px
| border         = 
| alt            = Sabar Uparey DVD cover
| caption        = Sabar Uparey DVD cover
| director       = Agradoot
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  1955
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
}}
 Bengali film noir directed by Agradoot, based on A.J. Cronins 1953 novel, Beyond This Place. It starred acclaimed actors Uttam Kumar and Suchitra Sen in leading roles. Chhabi Biswas, Pahadi Sanyal and Nitish Mukherjee also play important roles in the movie.

==Plot==
Prashanta Chatterjee (Chhabi Biswas) is sentenced to life imprisonment after the alleged murder of Hemangini, a lady based in Krishnanagar whom he is accused of loving and then dumping and murdering heinously.

Twelve years later, his only son Shankar (Uttam Kumar), who lives with his mother (Sobha Sen) in Patna, comes to Krishnanagar to prove his fathers innocence and bring the actual perpetrators to justice. There he falls in love with Rita (Suchitra Sen), a girl who too had a life history of injustice. Together, they secretly conjure up various evidences against the actual criminal, a government lawyer(Nitish Mukherjee) who originally fought the case against his father 12 years back. In all this, they get the help of Ritas journalist brother (Pahadi Sanyal) who, through his articles and friends in the Press, gathers public momentum to finally reopen the case.

Shankar presents the case himself and through various evidences and tact presentations, successfully pleads in favour of his father and proves the involvement of the actual criminal, the government lawyer, who killed Hemangini to benefit from a huge insurance in her name. Prashanta Choudhury is acquitted. Meanwhile with 12 years of wretched life in the prisons and the thought that his wife and child had perished due to poverty, he seems to lose his mental balance. However when brought in the midst of his family, sense and wellness returns to him. He thanks Ritas brother for his immense help and asks his sisters hand for his son Shankar.

==Cast==
*Uttam Kumar - Shankar Chatterjee
*Suchitra Sen - Rita
*Chhabi Biswas - Prashanta Chatterjee, Shankars father
*Pahadi Sanyal - Ritas brother, a journalist
*Nitish Mukherjee - Government lawyer
*Sobha Sen - Shankars mother
*Jayashree Sen - Bina
Art Director : Satyen Roy Chaudhury

==Soundtrack==
{{infobox album
| Name          = Sabar Uparey
| Type            = Soundtrack
| Artist           = Robin Chatterjee
}}
{{Track listing
| headline         = Songs
| extra_column           = Playback
| all_lyrics        = Gouri Prasanna Majumdar
| all_music           = Robin Chatterjee

| title1          = Ghum Ghum Chand
| extra1          = Sandhya Mukherjee
| length1         = 3:18

| title2          = Janina Phurabe Kobe
| extra2         = Sandhya Mukherjee
| length2         = 3:26
}}
 

==References==
 

==External links==
*  
*   at Upperstall.com

 

 
 
 
 
 


 