Happy Ever Afters
 
{{Infobox Film
| name     = Happy Ever Afters
| image    = Happy Ever Afters.jpg
| director = Stephen Burke
| producer = Leslie McKimm
| writer   = Stephen Burke Tom Riley Jill Murphy Leroy Harris Peter Byrne Lenny Hayden Eamonn Hunt Jonathan White  Jade Yourell
| cinematography = Jonathan Kovel
| distributor = Columbia Pictures
| released =  
| country  = Ireland
| language = English
| gross = €63,847   
}}
Happy Ever Afters is an Irish film written and directed by Stephen Burke. The film was first shown at the Pusan International Film Festival in South Korea on 10 October and released on 21 October 2009 in France.

==Synopsis==
Two weddings collide when both receptions are held at one hotel.

==Plot==

The film opens with us being introduced to the four main characters, Maura a down on her luck single mother, Molly her daughter as well as Freddie and his soon to be bride Sophie. Freddie is remarrying Sophie after a recent divorce . The reason for the divorce is not initially disclosed. Meanwhile Maura is shown to be marrying an illegal immigrant Wilson for which she will receive €9,000. Both weddings afters are being held in the same hotel where all 4 of the characters interact.

Maura is shown to be in debt and facing eviction from her house which lead her to the rash decision to marry Wilson. While her daughter Molly is unaware of the scam and believes that her mother truly loves Wilson and she is getting a new father. Freddie is shown to be a nice guy who has OCD esque habits while his bride Sophie is very image conscious and selfish. 

Throughout the film Freddie and Mauras paths keep crossing, leading to Sophie wrongly assuming that the two are involved in an illicit affair. To complicate matters two immigration officers arrive to the wedding reception to investigate Wilson and Maura. Eventually Molly learns that her mother is involved in a scam and has no feelings for Wilson.

Due to continually seeing Freddie and Maura, Sophie believes her suspicions of an affair to be true and flees the wedding. Sophies dad a selfish bully, loses his cool and attempts to assault Freddie believing him to be at fault for his daughters sudden disappearance. Meanwhile Sophie is shown to be in Dublin in a pub with some working class girls who support her decision to run away as they believe Freddie to be a lecherous villain. Sophie gets drunk with her new friend while Freddie is frantically trying to find her.

Freddie at the same time is trying to keep his marriage afloat. Throughout the film it is insinuated that one of the reasons for the marriage breakdown originally was the brides mental health. However, it is revealed that Freddie had a nervous breakdown as he couldnt deal with Sophie. Facing ruin and a pending divorce Freddie tries to kill himself by throwing himself off the top floor of the hotel. However just as he is about to jump, Maura steps in and talks him down from the ledge of the hotel roof. When he returns to the wedding both wedding parties have joined together and he meets a drunken returning Sophie.

They finally talk together and realise that their marriage is over and agree to part amicably. Freddie realises he has feelings for Maura and chases after her. Maura has left the hotel with Wilson and Molly. Maura believes that Freddie has reunited with Sophie when he appears and convinces her otherwise. They unite and kiss. Maura then manages to convince the immigration detectives that her marriage to Wilson is real. Wilson gives Maura her €9,000 euro and then leaves with his African girlfriend.

The film ends with Freddie uniting with Maura as Sophie leaves happily. Sophies bully father is taken away in a straight jacket for trying to kill Freddie with an axe. Freddie and Maura decide to take Freddies honeymoon together with Molly as the film ends.

==Main cast==
* Sally Hawkins as Maura Tom Riley as Freddie
* Sinead Maguire - Molly
* Tina Kellegher - Karen
* Deirdre Molloy - Niamh Jill Murphy - Geraldine
* Simon Delaney - Dessie Leroy Harris - Niamhs Oldest Boy
* Cillian Byrne - Niamhs Twins
* Cian Byrne - Niamhs Twins Peter Byrne - Repo Man
* Lenny Hayden - Eviction Man
* Eamonn Hunt - Mr. Butler
* Jonathan White - Freddies Priest
* Jade Yourell - Sophie

==Box office==
This film grossed €63,847 as announced on 17 January 2010. 

==External links==
*  

==References==
  

 
 
 
 