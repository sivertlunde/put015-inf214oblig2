Garm Wars: The Last Druid
{{Infobox film
| name           = Garm Wars: The Last Druid
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       = Mamoru Oshii
| producers      = Makoto Asanuma Mitsuhisa Ishikawa Tetsu Fujimura Lyse Lafontaine
| writer         =  
| screenplay     = Mamoru Oshii Geoffrey Gunn
| story          = Mamoru Oshii
| based on       =  
| starring       = 
| narrator       =  
| music          = Kenji Kawai
| cinematography = Benoit Beaulieu
| editing        = Atsuki Sato
| production companies = Production I.G Lyla Films
| distributor    =  
| released       =  
| runtime        = 102 minutes
| country        = Canada Japan
| language       = English
| budget         = 
| gross          =  
}} live action/animated science fiction adventure film directed by Mamoru Oshii.         

==Cast==
*Lance Henriksen as Wydd 
*Kevin Durand as Skellig   
*Melanie St-Pierre as Khara22 and Khara23 
*Jordan Van Dyck 
*Summer H. Howell as Nascien 
*Andrew Gillies 
*Dawn Ford 
*Patrizio Sanzari 
*Martin Senechal 

==Reception==
On The Hollywood Reporter, Deborah Young said that "spectacular visuals set to Kenji Kawai’s heavenly choirs lend a dreamy atmosphere to an incomplete extravaganza."  On Twitch Film, Christopher OKeeffe said that "while   there is undoubtedly an impressive visual flair thats quite stunning at times" the film "from start to finish   feels like a prequel, a tie-in to flesh-out a TV or game series."  On Variety.com, Peter Debruge called the film "a visually stunning yet impenetrable hybrid blend of live-action and CG visuals." 

==References==
 

==External links==
*   
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 

 