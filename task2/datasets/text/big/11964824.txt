Abhiyum Naanum
 
 
{{Infobox film
| name           = Abhiyum Naanum
| image          = Abhiyum_naanum_landscape.jpg
| caption        = Promotional Poster
| director       = Radha Mohan
| producer       = Prakash Raj
| writer         = Radha Mohan Aishwarya Prithviraj Prithviraj
| Vidyasagar
| cinematography = Preetha Jayaraman
| editing        = M. Kasivishwanathan
| distributor    = Duet Movies
| released       =  
| runtime        =
| country        = India
| awards         =
| language       = Tamil
| budget         =
| gross          =
}}
 Tamil feature Aishwarya and Vidyasagar scored Telugu as Father of the Bride. As of 2010, a Hindi remake was reported to have been planned. 

==Plot== Prakash Raj) meets Sudhakar (Prithviraj Sukumaran) and tells him about daughter Abhi (Trisha Krishnan). Raghuraman is the loving husband of Anu (Aishwarya Sivachandran) and the worrisome as well as caring father of his only daughter Abhi. Together they live a calm and content life in Ootacamund|Ooty. Also living with them is "Ravi Shastri" (Elango Kumaravel) who was earlier a beggar and was adopted by Abhi. Raghuraman and Abhi become best friends. Over the years as Abhi grows up, Raghuraman has the joy of being a part of her life. When Abhi gets accepted in a prestigious college in New Delhi, Raghuraman is shocked. Although the thought of being separated from his daughter for two whole years is heartbreaking, Raghuraman wearily accepts. After returning from college, Abhi tells her parents that she has fallen in love with a young man in Delhi. Anu is perfectly all right with the idea, but Raghuraman is angry and scared. He shouts at Abi and does not talk to her properly. Anu later informs Raghuraman that their future son-in-law will be arriving by flight and staying with them for a while. As soon as Raghuraman meets his future son-in-law, Joginder Singh (Ganesh Venkatraman), he is taken aback because Joginder is a Sikh. After many conflicts with Abhi and Anu, Raghuraman finally realizes that Joginder is self-sacrificing and quite talented, more so when on one occasion the Prime Minister calls him for advice. Abhi and Joginder get married and leave for Delhi. Anu and Abhi get teary-eyed, but Raghuraman stays calm. This is very surprising, because he was the one that had cried on his daughters first day to school. Raghuraman tells Sudhakar that he still stays in touch with his daughter and often gets to see her. Sudhakar understands how Raghuraman feels, because he has a baby daughter himself. The movie ends with a phone call from Abhi.

==Cast==
* Prakash Raj as Raghuraman
* Trisha Krishnan as Abhi Raghuraman
* Ganesh Venkatraman as Joginder Singh Aishwarya as Anu Raghuraman
* Elango Kumaravel as Ravi Shastri Prithviraj / Jagapati Babu as Sudhakar (Telugu version) (Special appearance)
* Neha Bhasin as Mallu (Special appearance)

==Soundtrack==
{{Infobox album |  
| Name       = Abhiyum Naanum
| Type       = Soundtrack Vidyasagar
| Cover      = 
| Released   = 2008
| Recorded   = 
| Genres     = World Music
| Length     =
| Label      = Think Music Vidyasagar
| Last album = Mahesh, Saranya Matrum Palar (2008)
| This album = Abhiyum Naanum (2008)
| Next album = Jeyam Kondaan (2008)
}}

The songs and background music have been composed by Vidyasagar (music director)|Vidyasagar. 

{| class="wikitable" style="width:60%;"
|-
! Song !! Singers
|-
|Ore Oru Oorilae || Kailash Kher
|-
|Pachhai Kaatre || Sadhana Sargam
|-
|Vaa Vaa En Devadhai || Madhu Balakrishnan
|-
|Moongil Vittu || Madhu Balakrishnan
|-
|Azhagiya Kili || S.P.Balasubramanyam
|-
|Chinnamma Kalyanam || Kailash Kher
|-
|Sher Punjabi || Rehan Khan
|}

==Release==
The satellite rights of the film were sold to Kalaignar TV|Kalaignar.

==Reception==
Abhiyum Naanum received positive to mixed reviews from critics. Pavithra Srinivasan of Rediff said, "For fathers who love their daughters, this is your pick. Definitely worth a watch." and rated the film 3 out of 5.  IndiaGlitz said, "Though the story unfolds in a slow pace towards the climax and there are few scenes which reminds one of watching a soap show, Abhiyum Nanum stands out for it is a quality entertainer which can be watched by the whole family".  Malathi Rangarajan of The Hindu said, "Watch it for its natural treatment".  Sify said, "Frankly speaking, the Prakash Raj- Radha Mohan combos Abhiyum Naanum is nowhere in the league of their previous oeuvre Mozhi. With a touching title like that, one would have thought the director would have a more solid script, but somehow it fails to strike a chord like their earlier film." but added that "If you are still looking for a different kind of cinematic experience, then, it’s worth a look."  Behindwoods rated the film 2 out of 5, saying "Abhiyum Naanum – sparkles in bits and pieces" and that it was "likely to suffer commercially". 

==References==
 

==External links==
*  

 
 

 
 
 
 