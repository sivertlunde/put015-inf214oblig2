Watch Out (film)
{{Infobox film
| name           = Watch Out
| image          = Watch out (movie poster).jpg
| caption        = 
| director       = Steve Balderson
| producer       = 
| based on       =  
| writer         = Steve Balderson
| starring       = Matt Riddlehoover
| music          = Rob Kleiner David Bagsby
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Watch Out is a 2008 film directed by Steve Balderson (Firecracker) based on the novel by Joseph Suglia and starring Matt Riddlehoover. Though the story is set in Benton Harbor, Michigan, the film was shot guerrilla-style, without permits, in March and April on location in Wamego, Kansas.

Watch Out premiered at the Raindance Film Festival in London, where it was nominated for Best International Feature. It was released theatrically in the "Stop Turning Me On" world tour in New York (Coney Island Film Festival), Nashville, Chicago, Washington D.C (Reel Affirmations Festival), Seattle (Lesbian & Gay Film Festival), San Francisco, Asheville, Charlottesville (Virginia Film Festival), Kansas City, Lawrence KS, Austin (Alamo Drafthouse), and Los Angeles.

== Festivals ==
* 2008 Raindance Film Festival - Nominee Best International Feature
* 2008 8th Annual Coney Island Film Festival
* 2008 Reel Affirmations Film Festival
* 2008 Seattle Lesbian & Gay Film Festival
* 2009 Boston Underground Film Festival

== External links ==
*  
*  

 
 
 
 
 
 


 