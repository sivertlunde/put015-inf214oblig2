Donald and the Wheel
Donald Disney animated short, directed by Hamilton Luske and released in 1961. It is an educational-based film, and features a considerable amount of musical vocals.

The film was most-recently re-released on  .

==Story== Max Smith). The elder is trying to explain the importance of the wheel to his son. They observe a caveman (portrayed by Donald Duck) trying to haul his supply sled up a hill and into a cave. Donald is then chased out of the cave by a tiger. He gets away, but the tiger tumbles down a hill wrapped around a rock. The spirits tell Donald that this should be the inspiration for his invention of the wheel.
 buggies and carriages of the 19th century, culminating with the invention of the steam locomotive and the Industrial Revolution.
 gramophone and classic ballet a wheel, and that the sun, Moon, and planetary orbits act as wheels.

After seeing into the future, Donald appears overwhelmed and bewildered, and decides against inventing the wheel. He claims it is "too much trouble" and does not want to bear the enormous responsibility. The Spirits of Progress accept that Donald may not be the true inventor of the wheel, but that "somebody did".

==Music==
 Buddy Baker, who also composed Donald in Mathmagic Land and sung by The Mellomen.

==External links==
* 
 
 
 
 
 
 
 

 