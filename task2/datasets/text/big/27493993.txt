A Deadly Secret (film)
 
 
{{Infobox film name = A Deadly Secret image = A Deadly Secret (film).jpg caption = DVD cover art traditional = 連城訣 simplified = 连城诀}} director = Mou Tun-fei producer = Run Run Shaw story = Louis Cha screenplay = Ni Kuang starring = Ng Yuen-chun Liu Lai-ling Jason Pai Shih Szu Elliot Ngok music = Eddie H. Wang cinematography = Law Wan-sing editing = Chiang Hsing-lung Cheung Siu-hei studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 90 minutes country = Hong Kong language = Mandarin budget = gross = HK$1,473,133
}} Louis Chas novel of Yueh Hua.

==Cast==
* Ng Yuen-chun as Di Yun
* Liu Lai-ling as Qi Fang
* Jason Pai as Ding Dian
* Shih Szu as Ling Shuanghua
* Elliot Ngok as Ling Tuisi
* Cho Tat-wah as Wan Zhenshan
* Wei Hung as Yan Daping
* Tong Kam-tong as Qi Zhangfa
* Tik Wai as Wan Gui
* Ngai Fei as Bu Yuan
* Kwan Fung as Mei Niansheng
* Jim Sam as Baoxiang

==See also==
*Deadly Secret
*Lian Cheng Jue (TV series)

==External links==
* 
* 
*    on Baidu Baike

 
 
 
 
 
 
 

 