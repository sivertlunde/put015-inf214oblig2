Keane of Kalgoorlie
 
{{Infobox film
| name           = Keane of Kalgoorlie, or a Story of the Sydney Cup
| image          = 
| image_size     = 
| caption        =  John Gavin
| producer       = Herbert Finlay Stanley Crick
| writer         = Agnes Gavin Arthur Wright
| narrator       =  John Gavin Agnes Gavin
| music          = 
| cinematography = Herbert Finlay
| editing        = 
| studio = Crick and Finlay
| distributor    = 
| released       = September 1911<ref name=
"mildura">  
| runtime        = 4,000 feet  or 3,000 feet 
| country        = Australia
| language       = Silent film
| budget         = 
}} Arthur Wright, adapted from the novel by Wright.
 John and Agnes Gavin and is considered a lost film.

==Synopsis==
Sydney clerk Frank Keane is in love with Tess Moreton, who is desired by Gregory Harris. Keane farewells Tess at Circular Quay in order to go west to seek his fortune. Harris arranges with Harold Rose to send a telegram to Tess saying that Keane has married a Kalgoorlie barmaid. Tess believes this and marries Harris.

Ten years later Keane and Tess meet again. Keane has become rich and wants to enter his horse in the Sydney Cup. He meets Harold Rose, who confesses the truth about falsifying the telegram, then shoots himself. Harris robs Roses dead body and is spotted by a dosser, who blackmails him.

Tess overhears Harris and the blackmailer discussing a plan to abduct Keanes horse and prevent it from winning the race. With Tess help, Keane ensures his horse wins the race. However Harris then accuses him of the murder of Rose. However at a trial, a stolen watch proves his innocence, and Harris is arrested while Keane is freed. Harris kills himself and Keane and Tess get married. 

==Cast== John Gavin as Frank Keane
*Agnes Gavin as Tess Murton
*Alf Scarlett
*Charles Woods as Dave Dyeball

==Original novel==
{{Infobox book|  
| name          = Keane of Kalgoorlie: A Tale of the Sydney Cup
| title_orig    =
| translator    =
| image         =
| caption = Arthur Wright
| illustrator   =
| cover_artist  =
| country       = Australia
| language      = English
| series        =
| genre         = 
| publisher     = Sunday Times Newspaper Company
| release_date  = 1907 
| english_release_date =
| media_type    =
| pages         =
| isbn          =
| preceded_by   =
| followed_by   =
}}
Arthur Wrights story was first published in the Referee in 1906.  He expanded it into a novel which was published the following year by the Sunday Times Company.   

===Plot===
Frank Keane is in love with Tess Morton but they have no money, so he decides to leave his home in Sydney and seek his fortune in the Kalgoorlie goldfields. Two years later he and his friends, including Harold Ross, strike it rich. But then Frank receives a letter informing him that Tess has married the villainous Gregory Harris.

Eight years later Harris has fallen into financial trouble, stolen from Tess, served a stint in gaol. Frank has become very wealthy and returns to Sydney. Frank learns from Harold Ross why Tess married Harris: Frank and his friend needed money on the goldfields to keep digging. Harris promised Ross £100 if he would forge Franks handwriting and send a letter from the goldfields claiming that Frank had married a barmaid; Ross did this and a heartbroken Tess then married Harris. Ross asks Frank for some money to help him but Frank refuses. Ross is later found dead in the Domain, having been murdered by Harris.

Frank and Tess are reunited and visit the Randwick Races together, where they are spotted by Harris. Harris is spending money he has stolen from Ross.

Harris tries to destroy Keanes horse, which is running against one of his own in the Sydney Cup. When Harris dies, he contrives things so that Keane is blamed for his death. Eventually however Keanes horse wins the race and he is proved innocent and reunited with Tess.

===Reception===
The book was popular with readers and launched Wrights career. 

A critic from the Advertiser compared the book to the works of Nat Gould and said it "makes no claim to any high order of merit, yet it i. as readable as many sensational stories of a more pretentious character... the story is written by one who has evidently seen a good deal of the darker side of life and is well acquainted with the turf, two important qualifications for the production of a work intended only to amuse... ought to find many readers." 

==Theatre adaptation==
{{Infobox play
| name       = Keane of Kalgoorlie
| image      = 
| image_size = 
| caption    = 
| writer     = Edward William OSullivan Arthur Wright
| characters = 
| setting    = 
| premiere   = 18 April 1908 
| place      = Haymarket Hippodrone, Sydney, Australia
| orig_lang  = English
| subject    = 
| genre      = Melodrama
}}

===Development===
Wright thought the novel had possibilities as a play and approached Edward William OSullivan, who had written the successful plays Cooee and Eureka Stockade for his opinion. OSullivan was enthusiastic and encouraged Wright to write the story up as a play, offering to rewrite it for him. This was done, although OSullivan later said "it did not require much alteration, as Mr. Wright had the true dramatic instinct."  OSullivan recommended the play to theatre entrepreneur E. J. Cole in early 1908. 

===Productions===
The production debuted at the Sydney Hippodrome on 18 April 1908 and starred W.H. Ayr as Frank and Amy Sherwood as Tess. 

The critic from the Sunday Times said that the play:
 Is full of local color, from the opening scene, enacted in the Domain during the Commonwealth festivities, to the running of the Sydney Cup at Randwick. Some realistic Incidents are portrayed, including tho burning of Keanes stables, and an attempt on the life of the Sydney Cup favorite: the trial of the hero on a false charge at the Central Criminal Court; a two-up school, in which the game is followed by a police raid; a house in Woolloomooloo, where Keane is imprisoned, and tho final settling at Randwick. Tho story is an interesting one, and being well interwoven with a sporting element, should appeal to a large section of playgoers.  
The Sydney Morning Herald was more guarded, stating that:
 The production was far from well mounted, but it was well acted, and with better stage dressing would prove a decided draw. Although occasionally ones sonse of probabilities was shocked during the stage unfolding of the story, on tho whole the play proved one of coherence, reasonably dramatic situations, and humour... The scenes in which boxing and two up wore presented were by no means convincing. The boxers gave a very poor display, devoid of cleverness, and the two-up episode and raid was a weak parody of the real thing... The authors must have boon gratified with, the storm of approval that arose near the conclusion of the drama from the great audience.  
The Evening News said the play "presents several stirring situations in a story which is fairly well developed." 

Public response appears to have been enthusiastic  and there were further productions throughout the rest of the decade.  

==Film production==
Wright later said he suggested the story might make an ideal film to John Gavin, who specialised in making films about bushrangers, "believing that a racing film would be welcomed as a change... It would be a shame to mention the price offered   and accepted, but as I thought the picture would enhance the sale of the book, as the drama had done, I had no kick comings." 

Production of the film was announced in April 1911, with Stanley Crick and Herbert Finlay producing the movie in Sydney.  
 

There are sequences involving a boxing match and game of two-up, and the climax has Keane winning the Sydney Cup. During shooting racing authorities would not allow Randwick Racecourse to be used, so footage of a race in Western Australia, the Perth Cup, was used instead.  Authorities also refused permission to film at Darlinghurst Gaol, location for an important scene where the hero is released from prison. In order to get the shot, Gavin snuck into the prison along with tradesmen, then was filmed by his crew when exiting. Gavin also "stole" a shot involving Keane greeting a real-life well known barrister, without the barristers knowledge. 

==Reception==
According to Arthur Wright the film was released to "big business":
 Wherever it was shown, box-office returns were a record for the times (prices 6d. and 3d. mostly). It packed the big tent show run, by the Mclntyres at North Sydney, where it was extensively advertised as- A great Australian sporting story by Nat Gould. This, of course, in my modesty, I objected to, and dodgers were sent out with a correction, and the lecturer, Mr. Woods, who played the part of Dave Dyeball, the Domain Dosser, in the picture, apologised for the mistake; and made a eulogistic reference to the young Australian author who was really responsible. Yes, someone made money out of K of K, and there should be money to be made again under the altered conditions with a talkie, with the racecourse as a background. The author gained but little from his offspring, except experience and a measure of amusement from participation in what was a new diversion.  

==References==
 

==External links==
* 
*  at AustLit
*  at AustLit Trove
*Complete text of original short story at The Referee from 1906-07 –  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  ,  
 
 

 
 
 
 
 