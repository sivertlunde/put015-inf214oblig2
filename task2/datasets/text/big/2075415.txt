Aventure Malgache
{{Infobox film
| name           = Aventure malgache
| image          =
| image_size     =
| caption        =
| director       = Alfred Hitchcock
| producer       =
| writer         = Jules François Clermont (story) Angus MacPhail  (uncredited)
| narrator       =
| starring       = "Paul Clarus", Paul Bonifas
| music          = Benjamin Frankel
| cinematography = Günther Krampf
| editing        =
| distributor    = Milestone Films
| released       = 1944
| runtime        = 30 minutes 32 minutes (American 1999 DVD)
| country        = UK
| language       = French
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 French directed Ministry of Malagasy Adventure in English.
 Claude Dauphin.  Some sources claim the film is based on the real-life activities of Jules François Clermont, who wrote and starred in the film under the name "Paul Clarus".  In September 2011, The Daily Telegraph published an article noting that writer and actor Claude Dauphin had collaborated with Hitchcock to recount his own experiences of operating an underground radio station in Nazi occupied France.    

== Plot summary == Vichy official, Resistance on the island of Madagascar during the Second World War. The events on Madagascar are shown in flashback (narrative)|flashback.
 Vichy water Scotch and soda water.

== Cast ==
*Paul Bonifas as Michel - Chef de la Sûreté (Leader of the Sûreté)
*Paul Clarus as Himself
*Jean Dattas as Man behind Michel, reading a telegram
*Andre Frere as Pierre
*Guy Le Feuvre as General
*Paulette Preney as Yvonne

==Home media== Bon Voyage, on DVD and VHS. 

==See also==
*List of World War II short films

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 

 
 