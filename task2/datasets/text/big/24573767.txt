Love's Blindness
 
{{Infobox film
| name           = Loves Blindness
| image          =
| caption        = John Francis Dillon
| producer       =
| writer         = Elinor Glyn
| starring       = Pauline Starke Antonio Moreno Lilyan Tashman
| cinematography =
| editing        =
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
}}
 1926 silent silent film John Francis Dillon. The film stars Pauline Starke, Antonio Moreno and Lilyan Tashman. Written by Elinor Glyn, the film was produced under the direct supervision of the author. The film is considered lost with just a fragment surviving at the BFI National Film & Tv Archive in London.   

==Plot==
Hubert Culverdale (Antonio Moreno) is the hard-up and hard-to-please milord who marries luscious Vanessa Levy (Pauline Starke) for financial reasons only.

==Cast==
* Pauline Starke - Vanessa Levy
* Antonio Moreno - Hubert Culverdale, 8th Earl of St. Austel
* Lilyan Tashman - Alice, Duchess of Lincolnwood
* Sam De Grasse - Benjamin Levy
* Douglas Gilmore - Charles Langley
* Kate Price - Marchioness of Hurlshire
* Tom Ricketts - Marquis of Hurlshire
* Earl Metcalfe - Col. Ralph Dangerfield
* George Waggner - Oscar Issacson
* Rose Dione - Mme. De Jainon
* Ned Sparks - Valet

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 