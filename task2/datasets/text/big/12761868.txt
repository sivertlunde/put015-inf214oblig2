Kuruvi
{{Infobox film
| name =Kuruvi
| image = Kuruvi poster.jpg
| caption =  Dharani
| writer     = Babu Sivan   (dialogue) Dharani
| Dharani
| producer   = Udhayanidhi Stalin Vijay Trisha Suman Vivek Vivek Ashish Vidyarthi Saranya Ponvannan Vidyasagar
| cinematography = Gopinath
| editing =DonMax Red Giant Movies
| released =  
| Country = India
| Run time = 170 minutes
| language = Tamil
| budget   =
| gross    =

}} Tamil Thriller action thriller Suman in Malavika played Telugu as Dopidi And Hindi As Jo Jeeta Wohi Baazigar. The movie performed average Grosser at the box office.

==Plot==
Vetrivel (Vijay (actor)|Vijay) is a car racer, who lives with his large family in Chennai. In the introduction scence, thalapathy Vijay participate in a car race with a old wrecked car.In the final lap the car accelerator breaks and Vetrivel(Vijay) hold the accelerator with his teeth.His father, Singamuthu (Manivannan) had gone to Kadapa in Andhra Pradesh for work at a colliery and never returned, prompting Vetrivel and his family to think he is dead. Vetrivel comes to know that a gentleman named Koccha (Suman (actor)|Suman) owes his father a huge sum of money. Finding that Koccha is in Malaysia, Vetrivel and his friend Pons (Vivek (actor)|Vivek) fly to Malaysia, as Kuruvi, the trade jargon for low-level contraband carriers. He steps into the group at a time when internal rivalry and problems are surfacing in Kocchas family. Kocchas sister Devi (Trisha Krishnan) has refused to marry the formers business associate Konda Reddys (Ashish Vidyarthi) brother, the boy approved by her family members. Irritated and preoccupied, nobody wants to devote any time or attention to resolve Vetrivels problem. Vetrivel is abused, harassed, suffers more ill-treatment and is thrown out of Kocchas place without any help rendered. These callous actions enrage the young man. Totally enraged, with a do-or-die attitude, deciding to get his work done no matter who gets hurt, Vetri becomes a veritable suicide machine and launches into a terrific scuffle with the dangerous gang. Determined to return only after the issue is solved to his satisfaction, motivated by an urge to reveal the truth, Vetrivel conceals himself in Kocchas palace-like residence. He returns to India after stealing a large diamond that was in Kocchas possession.

Devi follows Vetrivel and comes to India too, having fallen in love with Vetrivel. Knowing that Vetrivel has taken his diamond Koccha and his gang visit Vetrivels house and threaten his family. After confronting Koccha, Vetrivel comes to know that his father in not dead, but is being held as bonded labor along with many innocent people in Kadapa. Singamuthu had discovered diamonds at the collieries but refused to allow Koccha and Konda Reddy to illegally mine the diamonds, saying that the diamonds should go to the government. He had been held in Kadapa since.

Vetrivel goes to Kadapa and discovers a slave camp run by Koccha and Konda Reddy at the collieries, where a womanising rowdysheeter, Kadapa Mohan, is torturing the inmates.
He jumped from a tall buliding into a train which is almost a mile distance like a kuruvi(Sparrow). Vetrivel first encounters Koccha in Kadapa and throws him into a moving train, paralysing him. Then he single-handedly takes on Kadapa Mohan and Konda Reddy, killing them. When the paralysed Koccha sees Konda Reddys dead body, he recovers and attempts to shoot Vetrivel, but is then arrested by a special task force led by Raj (Nassar). Singamuthu and the others who were imprisoned in the slave camp, are finally freed.

==Cast==
  Vijay as Vetrivel
* Trisha Krishnan as Devi, Vetrivels love interest Vivek as Ops, Vetrivels sidekick Suman as Koccha
* Manivannan as Singamuthu, Vetrivels father
* Ashish Vidyarthi as Konda Reddy, Kocchas associate
* Nassar as Raj, the general of the police encounter squad targeting businesses (cameo appearance) Malavika in a cameo appearance in the song "Dandaana Darna"
* Saranya Ponvannan as Kocchas wife and Devis sister-in-law
* Subbaraju as a Rival Racer (Cameo)
* Nivetha Thomas as Vijays Sister Chitti Babu as Kochas assistant
* G. M. Kumar as Bhai
* T. K. Kala as Vetrivels mother
* Cell Murugan Pawan as Soori, Konda Reddys brother
* Ilavarasu
* G. V. Sudhakar Naidu as Kadapa Raja
* Lollu Sabha Jeeva as Bujjibabu
* Birla Bose as Hotel manager
* Chaplin Balu as one of the slaves
* S. N. Lakshmi as Vetrivels Grandmother
* Narsing Yadav as inspector
* "Paruthiveeran" Sujatha as Vetrivels Aunt
* Kalpana Shree as Vetrivels Sister
* Vimal as one of the slaves (uncredited)
* Vidharth as one of the slaves (uncredited)
 

==Production==
It was announced in 2007 that Udhayanidhi Stalin, son of M. K. Stalin and grandson of Tamil Nadu Chief minister M. Karunanidhi announced that he would produce films under his production banner Red Giant Movies.  The film marks the comeback of Dharani and the announced project titled "Kuruvi" would more or less feature same cast and crew of directors previous film Ghilli. It was rumoured that "Kuruvi" was titled because lead hero was named as Guru V which proved false.  Kuruvi or Pura is used as a code word for illegal immigrants. 
 Trisha was selected to pair with Vijay for fourth time.  Vadivelu was initially approached for the film but he was replaced by Vivek. 

A stunt scene was picturised with Vijay taking on few men who belong to the African mafia in a huge bar-cum-disco set erected for this purpose. In Kuruvi, the director has used a chopper to shoot an entire song sequence to give it a feel of grandeur.  Huge set was erected at MGR film city for major portion of the film. The team had even erected another set nearby in Sriperumbudur for shooting a song sequence, which features some models from Bangalore and Mumbai. Sadly, rain seems to have played spoilsport, which held up the shooting for two days – by the time the call sheet of the models ended.  A fight was picturised at AVM Studios with a grand set resembling a bar. 

==Soundtrack==
{{Infobox album |  
| Name       = Kuruvi
| Type       = Soundtrack Vidyasagar
| Cover      =
| Released   = 2008
| Recorded   =
| Genres     = World Music
| Length     =  
| Label      = Ayngaran Music An Ak Audio Vidyasagar
| Last album = Mulla (film)|Mulla (2008)
| This album = Kuruvi (2008)
| Next album = Neelathaamara (2009)|Neelathaamara (2009)
}}
The soundtrack is composed by Vidyasagar. Kuruvi’s audio launch was held at the Little Flower Convent on 16 April 2008 in Chennai sans the usual fanfare despite the buzz surrounding the movie. The low key affair, held in the school for hearing and speech impaired, witnessed minimal star participation. As a novel attempt, the audio was Kuruvi released to senior journalists from renowned dailies like Daily Thanthi and The Hindu. Gangadaran from Daily Thanthi and S.R. Ashokkumar from The Hindu released the album to the students of the academy. Representatives from other media houses like Sun TV also participated in the event. Vijay and Sangeetha, Vikram, Vishal, Trisha, producer Udhayanithi and his wife, and director Dharani are the other celebrities spotted at the event.  Behindwoods wrote:"Vidyasagar seems to have reinvented himself as a fairly noisy composer here! The metallic twang that accompanies the songs is not too pleasant. Maybe the plot calls for such treatment, but you come away wondering whether Vidyasagar heartily relished doing this".  Rediff wrote:"Vidyasagar music has sheen no soul".  Millblog wrote:"Kuruvi is no Gilli, but does have its own quirky charm".  The song "Mozha Mozhannu" created controversy because the name of freedom fighter "Thillaiyadi Valliyamma" was mentioned in the song. 

=== Track listing ===
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| No. || Song || Singers || Length (min:sec)
|-
| 1|| "Happy New Year" || Vaaisan, Dr. Burn, Emcee Jesz, Sunidhi Chauhan || 04:05
|-
| 2|| "Dandaana Darna" || Sangeeth halthipur || 03:40
|-
| 3|| "Thaen Thaen Thaen" || Udit Narayan, Shreya Ghoshal || 03:38
|-
| 4|| "Palaanadhu Palaanadhu" || Vidyasagar (music director)|Vidyasagar, S. Rajalakshmi || 04:05
|-
| 5|| "Kuruvi Kuruvi" || Benny Dayal, Praveen Mani, Dr. Burn, Vetri Boys, Suvi Suresh || 02:00
|- Kay Kay, Anuradha Sriram || 03:55
|}

==Release==
The satellite rights of the film were secured by Kalaignar TV|Kalaignar. The film was given a "U" certificate by the Indian Censor Board.

==Reception==
The film was made at a cost of  16 crore and opened to Mixed reviews and was a Average Grosser at the box office. Behindwoods rated the movie with 2.0 out of 5 stars and called "If you go to see Kuruvi with lowered expectations, you may end up liking it. But those who are looking for big build-up sequences and large Ghilli-like payoffs – stay away.".  Sify wrote:"To give Dharani his due, Kuruvi is watchable in parts, for die-hard fans of Vijay. For ordinary viewers, the film lacks a basic story and stretches ones patience for nearly three hours".  Rediff wrote:"Kuruvis got everything to appeal to Vijay fans – but Dharani the director is lost in this melee of fist-fights and elementary comedy".  Indiaglitz wrote:"Kuruvi, a racy thriller loaded with romance and action, is a sure treat for Vijay fans".  Hindu wrote:"Kuruvi tries its best to fly high, only that the effort isn’t enough because earlier Ghilli had soared much higher. Of course, Dharani keeps up the momentum and things move fast to the midway point". 

==References==
 

==External links==
* 

 
 

 
 
 
 
 