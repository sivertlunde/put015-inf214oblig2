Charlie Chan at the Race Track
{{Infobox film
| name           = Charlie Chan at the Race Track
| director       = H. Bruce Humberstone
| image	         = Charlie Chan at the Race Track FilmPoster.jpeg
| image size     = 190px John Stone
| writer         = Lou Breslow Saul Elkins
| starring       = Warner Oland
| music          = Samuel Kaylin Harry Jackson
| distributor    = 20th Century Fox
}}
Charlie Chan at the Race Track is the 12th film in the 20th Century Fox-produced Charlie Chan series starring Warner Oland in the title role.

== Plot ==
When a prominent racehorse owner winds up dead-allegedly kicked to death by his prized stallion, Charlie Chan is called in to investigate.  But when the indomitable detective discovers evidence of foul play, hes soon hot on the hooves of an international gambling ring with an evil plot to turn the racetracks of the world into a trifecta of terror!

== Cast ==
*Warner Oland as Charlie Chan
*Keye Luke as Lee Chan
*Helen Wood as Alice Fenton Thomas Beck as Bruce Rogers
*Alan Dinehart as George Chester Gavin Muir as Bagley
*Gloria Roy as Catherine Chester
*Jonathan Hale as Warren Fenton George Irving as Major Kent
*Max Wagner as Joe

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 

 