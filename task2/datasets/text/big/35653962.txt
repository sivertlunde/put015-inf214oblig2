The Iron Mistress
{{Infobox film
| name           = The Iron Mistress
| image          = The Iron Mistress.jpg Gordon Douglas
| producer       = Henry Blanke James R. Webb
| based on       =  
| starring       = Alan Ladd Virginia Mayo
| music          = Max Steiner
| cinematography = John F. Seitz
| editing        = Alan Crosland Jr.
| language       = English
| country        = United States
| studio         = Warner Bros.
| runtime        = 110 minutes
| gross          = $2.9 million (US rentals) 
| released       = 19 November 1952
}}
 Western film starring Alan Ladd as Jim Bowie. It ends with his marriage to Ursula de Veramendi and does not deal with Bowies death at the Battle of the Alamo. 

It was the first film Ladd made at Warner Bros after spending a decade at Paramount Pictures.

==Plot summary==
 
The Iron Mistress is based on the life of American frontiersman Jim Bowie (Alan Ladd).

==Cast==
* Alan Ladd as Jim Bowie
* Virginia Mayo as Judalon de Bornay
* Joseph Calleia as Juan Moreno Ursula Veramendi, Bowies wife and daughter of Juan Martín de Veramendi 
* Alf Kjellin as Philippe de Cabanal
* Douglas Dick as Narcisse de Bornay
* Tony Caruso as "Bloody Jack" Sturdevant Ned Young as Henri Contrecourt
* George Voskovec as John James Audubon
* Richard Carlyle as Rezin Bowie
* Robert Emhardt as Gen. Cuny
* Donald Beddoe as Dr. Cuny

==Production==
Alan Ladd broke his hand on the last day of filming. Alan Ladd Breaks Hand
Los Angeles Times (1923-Current File)   29 May 1952: 4. 

==References==
 

==External links==
*  
*   at TCMDB
*  
*  
*  

 
 
 
 
 
 
 
 


 