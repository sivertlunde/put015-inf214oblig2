The Incredible Mr. Limpet
{{Infobox film
| name           = The Incredible Mr. Limpet
| image          = TIML poster.jpg
| caption        = Theatrical poster
| director       = Arthur Lubin
| producer       = John C. Rose
| writer         = Theodore Pratt (novel) Joe DiMona Jameson Brewer John C. Rose
| starring       = Don Knotts Carole Cook Jack Weston Frank Perkins
| cinematography = Harold E. Stine
| editing        = Donald Tait
| distributor    = Warner Bros.
| released       =  
| runtime        = 99 minutes
| country        = United States English
| budget         = Unknown
}}
The Incredible Mr. Limpet is a 1964 American live action/animated film|live-action/animated adventure film from Warner Bros. Variety Film Reviews|Variety film review; January 22, 1964, page 6.  It is about a man named Henry Limpet who turns into a talking fish resembling a tilefish and helps the United States Navy|U.S. Navy locate and destroy Nazi submarines. Don Knotts plays the title character. The live action was directed by Arthur Lubin, while the animation was directed by Robert McKimson, Hawley Pratt, and Gerry Chiniquy. Music includes songs by Sammy Fain, in collaboration with Harold Adamson, including "I Wish I Were a Fish," "Be Careful How You Wish," and "Deep Rapture."

== Plot ==
The story begins September 1941 just before the attack on Pearl Harbor. Shy bookkeeper Henry Limpet loves fish with a passion. When his friend George Stickle enlists in the United States Navy, Limpet attempts to enlist as well, but is rejected. Feeling downcast, he wanders down to a pier near Coney Island and accidentally falls into the water. Inexplicably, he finds he has turned into a fish. Since he never resurfaces, his wife, Bessie, and George assume he has drowned.

The fish Limpet, complete with his signature pince-nez spectacles, discovers a new-found ability during some of his initial misadventures, a powerful underwater roar, his "thrum". He falls in love with a female fish he names Ladyfish, and makes friends with a misanthropic hermit crab named Crusty.
 Battle of the Atlantic. In his final mission, he is nearly killed when the Nazis develop a "thrum" seeking torpedo, and is further handicapped by the loss of his spectacles. He manages to survive using Crusty as his "navigator", and sinks a number of U-boats by redirecting the torpedoes.  After the battle, he swims to Coney Island to say goodbye to Bessie (who has now fallen in love with George) and get a replacement set of glasses. He then swims off with Ladyfish.

In the films coda, set in the modern times of 1964, George (now a high ranking naval officer) and the Admiral are presented with a report that Mr. Limpet is still alive and working with porpoises.  The two men travel out to sea to contact Mr. Limpet and offer him a commission in the United States Navy.

== Cast ==
* Don Knotts as Henry Limpet
* Carole Cook as Bessie Limpet
* Jack Weston as Machinists Mate 2nd Class (PO2) George Stickle
* Andrew Duggan as Harlock
* Larry Keating as Admiral P.P. Spewter
* Oscar Beregi, Jr. as Nazi admiral Charles Meredith as Fleet Admiral
* Elizabeth MacRae as Ladyfish (voice)
* Paul Frees as Crusty (voice)

This was the last film of Larry Keating and Charles Meredith. Both Keating and Meredith died not long after it was finished.

==Home video release== high definition on Blu-ray Disc.

==Film notes== Warner Theatre in Morgantown, West Virginia.

Both Don Knotts and Elizabeth MacRae (Limpet and Ladyfish) were employed in Andy Griffiths Mayberry franchises, respectively as deputy Barney Fife and Lou-Ann Poovie, Gomer Pyles girlfriend in the later seasons of Gomer Pyle, U.S.M.C..

During World War I and World War II, there was a mine known as a limpet, a type of naval mine attached to a target by magnets named because of their superficial similarity to the limpet, a type of mollusk. "Das Limpet" was the German Navys identification of Don Knotts character.
 USS Alfred A. Cunningham was the naval ship featured in this film. USS Galveston USS Los Angeles in the film.  The Los Angeles was offered for use at the time of pre-production planning, but was decommissioned in the fall of 1963, before principle filming began.  Here lies a double continuity error, in that the Los Angeles was not commissioned until the fall of 1945, and the Galveston had been converted to a guided missile cruiser, and clearly shows her 1960s configuration with large radars and missile launchers in place of her removed gun turrets.

==Remake== Steve Rudnick and Leo Benvenuti were hired as writers for a remake of The Incredible Mr. Limpet.  By 1997, Jim Carrey entered negotiations to star in the title role,  and was confirmed in February 1998 with Steve Oedekerk hired as the writer and director.   Knotts was aware of plans for the remake, which he wrote about in his autobiography, and offered his support. Roughly $10 million was spent on animation tests to digitally map Carreys Motion capture|motion-captured human face onto a fishs body, which produced disastrous results.  By March 1999, Oedekerk left the project following creative differences,  while Carrey followed suit in July.  In April 2000, Warner Bros. hired Beavis and Butt-head creator Mike Judge as director and co-writer, with Robin Williams, Chris Rock, Mike Myers, and Adam Sandler in consideration for the lead role. Filming was set to begin early 2001.    

In June 2009, it was announced that Enchanted (film)|Enchanted director Kevin Lima was attached to direct.  In 2010, it was reported that Zach Galifianakis was in talks of the lead role.  In March 2011, Richard Linklater entered negotiations to helm the project,  and was announced as the director in January 2014.  That same month, Femke Wolting and Tommy Pallotta had begun working on the design and animation on the project while Galifianakis will reportedly play the lead character.  On July 8, 2014, it was announced that Jon Hamm, Danny McBride, Sarah Silverman, Kevin Hart, Josh Gad, Keegan Michael Key, and Jordan Peele entered talks for various roles in the film.  On August 4, Linklater left the project to concentrate on his next film Thats What Im Talking About. 

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 