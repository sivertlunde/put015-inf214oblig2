June R
{{Infobox film
| name = June R
| image = 
| director = Revathy S Varmha
| writer =  Suriya (Cameo)
| producer = A. Ansari 
| music = Sharreth
| studio = Ideawold 1 Celluloid Pvt Ltd
| editing = Jyothi Jayamaruthi
| cinematography = Madhu Ambat
| released =  
| runtime = 120 minutes
| country = India
| language = Tamil
| budget = 1.8 Cr. 
}}
June R is a 2006 Tamil drama film directed by Revathy Varmha. The film stars Jyothika in the title role with Kushboo, Saritha and Biju Menon in supporting roles. The films soundtrack are composed by Malayalam composer Sharreth, while Madhu Ambat was the cinematographer. The film opened in February 2006 and received positive reviews from critics.

==Plot== Suriya who plays a cameo ), a rich client of Junes advertising agency falls in love with her who consoles her on this uncompensable loss and takes her along with him, just the way her mother Rajalakshmi wished.

==Cast==
*Jyothika as June R
*Kushboo as Amudha
*Saritha as Rajalakshmi
*Biju Menon as Arun Suriya as Raja (guest appearance)
*Siddharth Venugopal as cameo appearance (uncredited)

==Production== Tabu and Kareena Kapoor would play the lead roles in the film.  Actress Jyothika suggested that the film should be made in Tamil first and production began and Kushboo was roped in for a key role. Saritha for another senior role in the film.  Biju Menon, a Malayalam actor, was also picked to play a leading role in the film.  The team also hoped to release the film in Telugu simultaneously. 

==Release== Suriya with some minor changes to the script to build up the films appeal.   The film was caught up when Kushboos films were temporarily banned after her comments on pre-marital sex angered Indian political parties. 

The film gained poor reviews with Indiaglitz.com citing "that the debutant director Revathy Verma has tried to flesh out the emotions of a girl longing for a motherly touch. But has ended up giving a movie with many loose-ends."  Another critic cited that "June R is a worst movie of late. It is a good example of how not to make a move. It is easy to pinpoint where the problem lies. It is in all spheres: screenplay, storyline, flow, and script."  Sify.com noted that the film "turned out to be a damp squib" adding that "first and foremost the debutant director has no clue about filmmaking and the whole affair looks amateurish and shoddy." 

Revathy Varmha began remaking the film in Hindi as Aap Ke Liye Hum in 2009  The film would  star Jaya Bachchan, Ayesha Takia Azmi, Ranvir Shorey and Raveena Tandon. R. Madhavan was also signed up for a large sum for the film but left the project after a single schedule. The film remains stuck and unreleased. 

==Soundtrack==
{{Infobox album|  
  Name        = June R
|  Type        = Soundtrack
|  Artist      = Sharreth
|  Cover       =
|  Released    = 10 September 2005
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = June R (2005)
|  Next album  = 
}}
The soundtrack of the film was composed by Sharreth, being his second project in Tamil after Magic Magic 3D (2003). The audio of June R was launched at Green Park Hotel in Chennai on 10 September 2005 by Balamurali Krishna, with singer Usha Uthup also in attendance. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Mazhaye Mazhaye Hariharan 
| length1         = 
| title2          = Puthu Puthu 
| extra2          = Usha Uthup, K. S. Chithra 
| length2         = 
| title3          = Anbe Anbe 
| extra3          = Gayathri Varma 
| length3         = 
| title4          = Eano Eano
| extra4          = Sharreth  
| length4         = 
| title5          = Mazhaye Mazhaye Sujatha 
| length5         = 
| title6          = Puthu Puthu (2)
| extra6          = Usha Uthup, K. S. Chithra 
| length6         = 
}}

==References==
 

 
 
 