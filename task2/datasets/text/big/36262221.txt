Commitment (film)
{{Infobox film
| name           = Commitment 
| image          = Commitment_poster.jpg
| director       = Park Hong-soo  
| producer       = Park Eun-gyeong   Lee Deok-jae   Lee Seong-hun   Choe Ji-yun
| writer         = Kim Soo-young Choi Seung-hyun   Han Ye-ri   Kim Yoo-jung 
| music          = Noh Hyeong-woo
| cinematography = Kim Gi-tae
| editing        = Kim Sang-beom   Kim Jae-beom
| distributor    = Showbox/Mediaplex   (South Korea)  Well Go USA Entertainment  (United States) 
| released       =  
| runtime        = 113 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =    
}}
Commitment ( ; lit. "Alumnus" or "The Graduate") is a 2013 South Korean spy thriller film starring Choi Seunghyun (also known as T.O.P (entertainer)|T.O.P from the K-pop boyband Big Bang (South Korean band)|BIGBANG).  He plays the teenage son of an ex-North Korean agent who is tasked to kill a North Korean assassin in South Korea in order to save his younger sister.    
 division of the peninsula and the Korean War, historical events that the teenage characters did not directly experience that nevertheless change their lives and destinies.  

==Plot== Choi Seung-hyun) forced labor camp. Their fathers superior, high-ranking military official Colonel Moon (Jo Sung-ha) proposes a deal to Myung-hoon: if he goes down to the South as a "technician" (an assassin) and finishes what his father had failed to accomplish, he and his sister will be released from the prison camp. Myung-hoon accepts the deal and undergoes two years of intense training.

Myung-hoon finally arrives in South Korea under the guise of a  , and Myung-hoon quickly becomes a liability and must ultimately cope with Colonel Moons treachery.

==Cast== Choi Seung-hyun – Ri Myung-hoon / Kang Dae-ho (cover identity used in South Korea)  
* Han Ye-ri – Lee Hye-in 
* Kim Yoo-jung – Ri Hye-in, Myung-hoons younger sister  National Intelligence Service agent
* Jo Sung-ha – Moon Sang-chul, North Korean senior colonel
* Jung Ho-bin – "Big Dipper," North Korean agent
* Kwak Min-seok – North Korean agent, Myung-hoons adoptive father in South Korea
* Kim Sun-kyung – North Korean agent, Myung-hoons adoptive mother in South Korea; cover identity as pharmacist
* Dong Hyun-bae – one of the bullies 
* Kim Min-jae – North Korean agent
* Lee Joo-shil – North Korean agent
* Park Ji-il – South Korean agent
* Kang Bit
* Park Sung-woong – Ri Young-ho, Myung-hoon and Hye-ins father (cameo)

==Release==
The film was released in South Korea on November 6, 2013, opening at number 2 in the box office.  On its opening weekend, it sold 689,600 tickets, grossing  .   In total, Commitment grossed   with 1,048,254 tickets sold nationwide.  

Following its pre-sales deal to eight Asian countries (Japan, Singapore, Thailand, Vietnam, Taiwan, Hong Kong, Malaysia and Indonesia),  distribution company Well Go USA acquired the North American rights to the film and gave it a limited theatrical release on December 6, 2013.   Commitment played in a total of 22 theaters and grossed   on its opening weekend.   In total, the film grossed   during its North American run. 

European distributor Splendid Film also released Commitment as Silent Assassin in German language|German-speaking territories in 2014. 

==References==
 

==External links==
*    
*  
*  
*  

 
 
 