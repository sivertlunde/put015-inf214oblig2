Blue Jeans (1917 film)
 
:see also Blue Jeans (disambiguation)

{{Infobox film
| name           = Blue Jeans
| image          = Blue Jeans.jpg
| imagesize      =
| caption        =
| director       = John Hancock Collins
| producer       =
| writer         =
| cinematography =
| editing        =
| distributor    = Metro Pictures
| released       =  
| runtime        = 7 reels (approximately 70 minutes)
| country        = United States
| language       = Silent film (English intertitles)
}}
 1890 melodramatic play by Joseph Arthur that opened in New York City to great popularity. The sensation of the play was a dramatic scene where the unconscious hero is placed on a board approaching a huge buzz saw in a sawmill, later imitated to the point of cliche.  

Prints survive at several archive houses. 

== Cast ==
* Viola Dana as June
* Robert D. Walker as Perry Bascom
* Sally Crute as Sue Eudaly
* Clifford Bruce as Ben Boone Henry Hallam as Colonel Henry Clay Risener Russell Simpson as Jacob Tutwiler
* Margaret McWade as Cindy Tutwiler
* Augustus Phillips as Jack Bascom

==References==
 

==External links==
*  
 

 
 
 
 
 
 
 


 