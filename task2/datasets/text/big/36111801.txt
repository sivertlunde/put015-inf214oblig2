Simmasanam
{{Infobox film
| name           = Simmasanam
| image          = 
| image_size     =
| caption        = 
| director       = Eeshwar
| producer       = Tamil Fathima Thangamma Fathima
| story          = 
| screenplay     =  Mantra Radhika Chaudhari
| music          = S. A. Rajkumar
| cinematography = Rajarajan
| editing        = G. Jayachandran
| distributor    =
| studio         = Tamilannai Cine Creation
| released       = 4 August 2000
| runtime        =
| country        = India
| language       = Tamil
| preceded_by    =
| followed_by    =
| website        =
}}
 2000 Tamil Ambika and Viji.  

==Cast==
*Vijayakanth as Sakthivel and Thangarasu Manthra
*Radhika Chaudhari
*Kushboo
*Viji as Kannamma Ambika
*Manorama Manorama
*Radharavi Senthil
*Vaiyapuri
*R. Sundarrajan
*Rajan P. Dev Thyagu

==Production==
Vijaykants friend and confidant, Ibrahim Rowther, has produced the film for Tamilannai Cine Creation. It introduces a new director, Eashwaran, who has also penned the story-dialogue and screenplay, apart from wielding the megaphone. Shooting for the film is on at locations in Chennai, Ooty, Pollachi, Udumalai, and Chalakkudi, among other places. A song was picturised in the lush green valley around Tirumurthy Hills, with rows of eucalyptus trees, palms, fields and a stream flowing through. Village elder Vijaykant and wife Khushboo sat watching as one hundred dancers pirouetted to the music. Ambika, Viji, Radha Ravi, Thyagu, Rajan P Deo, Fatima Babu, Vatsala, and R.Sundararajan were present. The song, written by Vairamuthu, had been sung by S.P.B. and set to music by S.A. Rajkumar. Cranking the camera was Rajarajan, Vijaykants favourite cinematographer. A marriage scene and a fight were also filmed. Choreographing the fight scene was fight master Rocky Rajesh. Three cameras were used to shoot the scene from different angles. About one thousand junior artistes were used for the scene. The fight is reportedly one of the highlights of the film. Computer graphics play an important part too. 

==Reception==
A critic from The Hindu noted "The story is not new but the screenplay which has all the ingredients to sustain the interest of the average viewer, with political overtones in dialogues, has been skilfully woven by director Eswar".  The film failed at the box office but was later dubbed and released in Telugu as Bhupathy Naidu.

==References==
 

 
 
 
 


 