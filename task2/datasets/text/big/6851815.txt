Jimmy and Judy
{{Infobox film
|name=Jimmy and Judy
|image=
|image_size=
|caption=
|director=Randall Rubin Jon Schroder
|producer=Randall Rubin Ed Sanders Gregory Scanlan Jon Schroder
|writer=Randall Rubin Jon Schroder
|narrator=
|starring=Edward Furlong Rachael Bella
|music=Benoit Grey
|cinematography=Ben Kufrin
|editing=Theodore Kent
|distributor=
|released= 
|runtime=99 minutes
|country=United States
|language=English
|budget=$1 million
|gross=
|preceded_by=
|followed_by=
}}
Jimmy and Judy is an 2006 independent film starring Rachael Bella as Judy and Edward Furlong as Jimmy. It is written and directed by Randall K. Rubin and Jon Schroder. The film was shot on hand-held video in the Cinéma vérité style.

==Plot==
A teenage outcast road movie, Jimmy and Judy follows a of a pair of outsiders who fall in love and out of control as they travel across an American landscape dotted with hypocrisy, materialism, drugs and violence.

The film focuses on the classic themes such as adolescent rebellion, love, and anger. Jimmy and Judy are a modern day Bonnie and Clyde: destructive young lovers who leave the comfort of their suburban community in rural Kentucky in search of a better life.

The film is presented in the form of a video diary from the point of view of the main characters.

==Cast==
{| class="wikitable"
|- bgcolor="CCCCCC"
! Actor !! Role
|-
| Edward Furlong || Jimmy Wright
|-
| Rachael Bella || Judy
|- William Sadler || Uncle Rodney
|-
| Chaney Kley || Dinko
|-
| A. J. Buckley || Buddy
|}

==Release and awards==
Won Best Feature at the 2006 San Francisco Independent Film Festival.

Won the MySpace.com award for Best Feature at the 2006 Newport Beach Film Festival.

Was released theatrically September 15, 2006 by Outsider Pictures.

==External links==
* 
* 

 
 
 


 