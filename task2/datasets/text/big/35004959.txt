El-Banate Dol
 

{{Infobox film
| name           = El-Banate Dol
| image          = 
| caption        = 
| director       = Tahani Rached
| producer       = Studio Masr
| writer         = 
| starring       = 
| distributor    = 
| released       = 2006
| runtime        = 68 minutes
| country        = Egypt
| language       = 
| budget         = 
| gross          = 
| screenplay     = Tahani Rached
| cinematography = Nancy Abdel-Fattah
| editing        = Mona Rabie
| music          = Tamer Karawan
}}

El-Banate Dol is an Egyptian 2006 documentary film.

== Synopsis ==
This documentary follows in the tracks of teenage girls living on the streets of Cairo, Egypt, a universe of violence, but also of freedom. These girls own a surprising strength, mixing necessary laughter with the toughness needed to survive day after day. Their existence, their lives and the codes they follow challenge the social model. Their days are sown with threats; sometimes it is the police and their raids, other times, the kidnappings carried out by their street fellows as well as the fights that sometimes get out of hand. Yet, through their dances, laughter and acrobatics these women and mothers offer us a glimpse which reminds us that they are still children.

== Awards ==
* 2006 Festival Cinéma Montpellier
* 2006 Jurnées Cinématrographiques de Carthage
* 2007 Ismailia International Film For Documentary & Short Films
* 2007 Doclisboa – Best Feature Documentary

== References ==
 

== External links ==
*  

 
 
 
 
 
 


 
 