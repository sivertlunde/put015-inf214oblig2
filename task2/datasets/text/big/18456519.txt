The Storm Warriors
 
 
{{Infobox film name = The Storm Warriors image = Storm-riders-2-movie3.jpg caption = Teaser poster director = Danny Pang Oxide Pang producer = Pang Brothers story = Ma Wing-shing starring = Aaron Kwok Ekin Cheng Nicholas Tse Charlene Choi Simon Yam Kenny Ho studio = Universe Entertainment Sil-Metropole Organisation|Sil-Metropole Chengtian Entertainment distributor = Universe Films Distribution Co. Ltd. released =   country = Hong Kong language = Cantonese budget =  gross = US$5,668,356 
}}
The Storm Warriors ( ) is a 2009 Hong Kong film produced and directed by the Pang brothers. It is the second live-action film adaptation of artist Ma Wing-shings manhua series Fung Wan, following the 1998 film The Storm Riders. The Storm Warriors is based on Fung Wan s Japanese Invasion story arc The Death Battle. Ekin Cheng and Aaron Kwok respectively reprise their roles as Wind and Cloud, who this time find themselves up against Lord Godless (Simon Yam), a ruthless Japanese warlord bent on invading China. The film is a co-production between Universe Entertainment and Sil-Metropole Organisation.
 Golden Harvest. The Pangs aimed on creating a big-budgeted film involving visual effects and stated that The Storm Warriors would not be a direct sequel to its 1998 predecessor. Principal photography for The Storm Warriors began in April 2008 and ended in July; filming took place in three studios located in Bangkok, Thailand. The film is notable for being the first Chinese language film shot in Chroma key|bluescreen. During post-production, effects artists worked on scenes involving computer-generated imagery, focusing on the films setting and backgrounds.

The Storm Warriors was released theatrically in Hong Kong on 10 December 2009. 

==Synopsis==
 
The evil Japanese warlord Lord Godless desires to conquer China. He threatens the General in command into surrender, takes the emperor captive and subdues a large number of martial arts masters using his special poison. Among the captives are Cloud who surrendered out of concern for Chu Chu, and the martial arts legend Nameless. Lord Godless gives them two choices, they can either surrender and work under him, or  they will be executed. None of the masters accept the offer. Nameless then breaks free of his chains, revealing that Ghostly Tiger, Nameless assistant sneaked in earlier to give all the masters the antidote to the poison. Cloud and Wind land direct blows on Lord Godless with no noticeable effect, and even though several other martial arts masters combine their powers to attack Lord Godless, they lose the contest of strength and are killed. Nameless uses his special attack on Lord Godless, using his chi to launch all the captured masters swords at Lord Godless, Lord Godless takes the attack head on and blocks all the swords while Nameless suffers serious internal injuries because he overtaxed himself while not fully recovered from the poison. Some other martial artists raid the prison on horseback and Nameless, Cloud, Wind and Chu Chu manage to escape, while Lord Godless sends his son, Heartless and two lieutenants, Earth and Sky, after Nameless with an army, with orders to destroy any martial arts schools along the way who will not surrender.

Nameless is badly injured and will not be able to recover his full strength to fight with Lord Godless for a significant period of time, so he sends Cloud and Wind to find Piggy Kings elder brother, Lord Wicked, who is the only person who can fight Lord Godless now. They go to the mountain where he has hid himself from the outside world but are unsuccessful in persuading him to help, until Winds lover Second Dream shows up and aids in persuading Lord Wicked. Lord Wicked comes out of his cave, revealing that he had chopped off his own arms years ago. This was his attempt to stop killing innocent people because he had lost control of himself in his younger days while practicing evil kung-fu. He kicks some pebbles at Cloud and Wind to test how they react, Cloud crushes the pebbles totally while Wind only catches them intact. Lord Wicked states that Cloud is too aggressive and will not be able to handle training in the evil way, while Wind has more control and might be able to return to normal. Lord Wicked then starts training Wind in evil arts while Cloud is summoned to meet Nameless at Lin Yi Temple.

At Lin Yi Temple, Cloud fights a disguised Nameless, who states that Cloud has massive potential, able to accurately learn his styles in a short period of time, and trains him, eventually creating a new sword style which Nameless names "Ba", inventing a new character partially resembling the characters "Cloud" and "Sword". Lord Godless discovers where Nameless, Wind and Cloud have been hiding and dispatches Earth and Sky to kill Nameless while sending a few warriors after Wind. At the temple, Cloud easily defeats Earth and Sky in a single blow, but as Lord Wicked and the others are fighting off the warriors that Lord Godless sent, Second Dream gets injured and Wind interrupts his training to come out of the cave and save her, killing the warriors in the process. Afterward he leaves with his training unfinished, despite it supposedly not being possible to leave the pool yet. Cloud arrives to find Wind gone, and Lord Wicked sends Cloud after Lord Godless as he is trying to find the Dragon Tomb which contains a secret that deals with the destiny of China.

Lord Godless meanwhile has taken the Emperor and his family as hostages and is searching for the Dragon Tomb. The Emperor refuses to help him, but Heartless is able to find out which path into the tomb is the correct one through the use of his chi that generated a massive wind when he picked the correct passage. Once at the end of the path, Heartless starts threatening the Emperor for more information on the Dragon Tomb, but as he refuses to speak, Heartless starts killing his wives and sons. Cloud arrives, and the guards which were Imperial Guards that were forced to surrender, switch sides instantly and pull the Emperor out of harms way. Cloud and Heartless exchanges blows, Heartless backs off after the first exchange, and Cloud and Lord Godless starts fighting, Cloud initially has the upper hand but is unable to injure Lord Godless due to his near invincible armor. In the fight, a pile of rocks is blown away, revealing the entrance to the real Dragon Tomb, which contains a human skeleton sitting upright. Lord Godless almost kills Cloud in the next exchange of blows, but Wind, who has been thoroughly possessed by the evil arts, arrives and fights against first Lord Godless, then Cloud, and then both of them, eventually killing Lord Godless by cutting off his arm from the underside of his armor, which was not as well protected and stealing the Dragon Bone, the secret of the tomb.

As Cloud chases after Wind, the Emperor begs Cloud to retrieve the Dragon Bone as it is crucial to the destiny of China. Second Dream eventually finds Wind at the secret place she had written to him in a letter about, but Wind just sits there with minimal response, and eventually the Emperors guards arrive to try and take the Dragon Bone back. However as the General struggles with Second Dream over the dragon bone, Heartless, which had escaped from the previous battle, grabs the bone and heavily injures the General, as the General and Heartless struggle for the bone, it breaks into two and Wind gets fully taken over by the evil spirit again. Cloud arrives and fights Wind to try and bring him back, but fails as Wind eventually wounds Second Dream and also kills Chu Chu. In a final gambit, Cloud hurls his sword at Wind, cutting him on the forehead and Wind miraculously regains control. The cliff side has taken serious damage in the battle however, and a large section collapses, causing Wind and Second Dream (who walks over to check on her beloved) to fall off the cliff. Cloud is not in the section of the cliff that collapses. However, Cloud jumps off the cliff as well, manages to fall faster than the Wind and Second Dream, and exerts a force to push Wind and Second Dream back to safety. In doing so, Cloud plummets into the depths, but there is no scene of him dying. Sitting safely on the stable part of the cliff, Wind laments about why Cloud did not kill him earlier when Cloud had the chance. The movie ends abruptly at this stage. 

==Cast==
 
*Aaron Kwok reprises his role as Striding Cloud, an unruly pugilist who is wanted by the Chinese Emperor. He is kidnapped by the villain, Lord Godless and inadvertently uncovers Godless secret plan to invade China. Kwok was "ecstatic" about acting in a sequel to The Storm Riders, despite realizing how difficult the filming process would be. He prepared for the role by exercising and weight training. On the first day of filming, Kwok was injured right in front of many reporters who were covering the act but he did not sustain any serious injury. Kwok described Cloud as "a very complex character, and people will see a more developed version of him here. He is more mature this time, and more human too. You see him as a real human character, not just a comic character." 

*Ekin Cheng reprises his role as Whispering Wind, a kindhearted and trustworthy pugilist. He is willing to sacrifice himself in order to save the life of his friend, Cloud. He undertakes the path of evil to improve his prowess in martial arts in the fastest time possible to get rid of the Japanese invaders. Cheng reprises his role from the first film and decided to grow back his former long-hair extensions. He was excited about reprising his role and never thought that he would be able to reprise his role after appearing in the first film: "Ive always liked the character, especially when I was reading the comics. I had wanted to make a Fung Wan movie even before The Storm Riders was made. I never thought I would be able to do it again 10 years later!" 

*Simon Yam as Lord Godless, an evil warlord from Japan who desires to invade China. He captures and imprisons a large group of pugilists, including the Chinese Emperor. The character is based on the comic series Japanese Invasion story arc, The Death Battle. During filming, Yam faced physical difficulties on the set. He had to perform with a blue and green screen, and dealt with the sets high temperature levels: "I have been acting for many years, I think this is the only time I had to change ten pairs of underwear daily. Everyday I was soaked. During the shoot I lost five pounds." Yam expressed that early in the shoot he had cramps. In order to replenish his energy he had to drink salt water.   

*Charlene Choi as Second Dream, the only daughter of martial arts legend "The King of Dao (sword)|Broadsword". She is well trained in using the broadsword as a child and inherits the great skills of her father. She aids Wind when Cloud is kidnapped by Lord Godless. The Storm Warriors marks Choi and Ekin Chengs fifth feature film collaboration as actors: "He and I have played lovers, couples, friends and siblings. Although we have played lovers many times before, this time felt very fresh perhaps due to the costume and the comic book background."   

*Nicholas Tse as Heartless, the oldest son and competent assistant of Lord Godless.

*Tiffany Tang as Chu Chu, the daughter of Kirin Arm who accompanied Cloud on his nomadic journey. Tang replaces Shu Qi, who played Chu Chu in the first film.

*Kenny Ho as Nameless, a martial arts legend who is regarded as the greatest enemy to Lord Godless. He pins his hopes on Wind and Cloud to save the world.

*Lam Suet as Piggy King, an ugly-looking and womanising pugilist who likes to fool around. He is entrusted by Nameless to lead Wind and Cloud to Lord Wicked.

*Kenny Wong as Lord Wicked, the senior of Piggy King. He is a reclusive and renowned martial arts master who broke his arms in order to relieve himself from evil power. Wind and Cloud seek his help to improve their skills.
 Patrick Tam as the Chinese Emperor, who is kidnapped by Lord Godless.

==Production== Oxide Pang Golden Harvest after the latter companys ten-year production rights to the Fung Wan franchise expired.   

The film is based on Fung Wans Japanese Invasion story arc The Death Battle, which follows Wind and Cloud as they fight against a Japanese warlord.    The Pangs have stated that The Storm Warriors is not a direct sequel to The Storm Riders, but more of a stand-alone film with a separate storyline.   

It was announced in April 2008 that the Pangs had changed the films English title from The Storm Riders II to The Storm Warriors in order to avoid copyright conflicts.  The name change was made to head off any potential copyright disputes with the comics current license holder who is no longer associated with the films financiers. 

===Casting===
Ma Wing-shing was very concerned about the films casting. He was especially adamant of Ekin Cheng and Aaron Kwok reprising their respective roles. Ma also dismissed that the actors might be too old for the film. He commented on the actors and their roles: "My concept of Cloud was someone with a tortured soul, and Aaron resembles the actual comic Cloud more today than he did 11 years ago. He is less baby-faced, and looks more mature and manly now...As for Ekin, he was more carefree in the first movie, but it will be more challenging for him this time around because he has to turn evil."     

===Filming===
Ekin Cheng and Aaron Kwok, along with the Pang brothers, executive producer Daniel Lam and the films crew celebrated production of the film with a traditional Thai blessing ceremony. Filming for The Storm Warriors began in April 2008 and ended in July 2008.    Principal photography for the film took place entirely in three studios occupying   in Bangkoks Pak-Gret district. Filming consisted mainly of a Thai production crew, as the Pangs decided to shoot the film in Thailand, rather than in their native Hong Kong: "We wanted to shoot the whole film...because we trust them, and because its probably cheaper than shooting in Hong Kong, even though we have to fly in some of the props and costumes."    Members of the media and the films distributors across Asia were invited to visit the shooting location. They also invited to view a fight scene between leading actors Ekin Cheng and Aaron Kwok as it was being filmed. 

===Design and effects=== CGI shots involving the films settings and background were created for the film.   

==Release==
The Storm Warriors was released theatrically in Hong Kong and Australia on 10 December 2009.    

===Marketing=== theatrical trailer for the film premiered on the films official website and in cinemas on 17 June 2009.    The films official website was launched in Beijing on 3 August 2009.     

==Awards and nominations==
29th Hong Kong Film Awards
* Nominated: Best Art Direction (Yee Chung Man and Lau Man Hung)
* Nominated: Best Costume Make Up Design (Yee Chung Man and Dora Ng Li Lo)
* Nominated: Best Action Choreography (Ma Yuk Sing)
* Nominated: Best Sound Design (Ken Wong, Phyllis Cheng and Lam Siu Yu)
* Won: Best Visual Effects (Ng Yuen Fai, Chas Chau Chi Shing and Tam Kai Kwan)

==See also==
*The Storm Riders
*Storm Rider Clash of the Evils
*Wind and Cloud
*Wind and Cloud 2

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 