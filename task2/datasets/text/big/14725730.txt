Tuesday in November
{{Infobox Film
| name           = Tuesday in November
| image          = 
| caption        = 
| director       = John Houseman
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = Virgil Thomson
| cinematography = 
| editing        = 
| distributor    = Office of War Information
| released       = 1945
| runtime        = 17 minutes
| country        = USA
| language       = English
}}
 1944 United States presidential election produced by the Office of War information for overseas distribution. It is meant to explain how the democratic process in America works.

The film begins with a small town schoolteacher who takes the day off to supervise the local election committee with representatives of the two major parties. Their first visitor is the local milkman. The camera stops at the booth curtains, because every election in America is secret, but then takes an "imaginary" look about what goes on inside the voting booth. 

The film then follows standard civics book descriptions of the three branches of government, checks and balances, and the political parties. The narrator notes two previous war time elections, 1864 and 1916, and the vigorous debate over important issues that has gone on in the country for this contest.

== See also ==
*List of Allied Propaganda Films of World War 2

== External links ==
*  
*  

 
 

 