West of the Pecos (1945 film)
{{Infobox film
| name           = West of the Pecos
| image          =
| image_size     =
| caption        =
| director       = Edward Killy
| producer       = Sid Rogel, Herman Schlom
| writer         = Zane Grey (novel), Norma Houston (screenplay)
| narrator       =
| starring       = Robert Mitchum Barbara Hale
| music          = Paul Sawtell
| cinematography = Harry J. Wild
| editing      = Roland Gross
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
}} Western film directed by Edward Killy and starring Robert Mitchum and Barbara Hale.  The movie is the third film version of Zane Greys novel.

==Plot==
Colonel Lambreths health is poor, so daughter Rill persuades him to leave his Chicago meat-packing business behind and move to their Texas cattle ranch. Her fiance, lawyer Clyde Corbin, stays behind.

On the trail, a couple of cowboys, Pecos Smith and sidekick Chito Rafferty, pull up to demand driver Tex Evans pay their back wages. After they ride off, the stagecoach is attacked by bandits. Circling back, the cowboys are told by the mortally wounded Tex that he was shot by Sam Sawtelle.

The stage proceeds to town with Jeff Slinger at the reins. Rill, harassed in town, tucks her hair into her hat and disguises herself as a boy to be left alone.

Brad Sawtelle, brother of Sam, organizes a posse of vigilantes to find Texs killer. Pecos gets to Sam first and shoots him.

The colonel and Rill get lost en route to their ranch. Pecos and Chito assist them and are offered jobs. Chito tries to woo the Lambreths maid, Suzanne, but where Rill is concerned, Pecos still doesnt know shes a woman. Corbin comes to Texas and senses that Rill is now in love with someone else.

Brad and his men believe Pecos to be an accomplice in the stagecoach robbery and murder. Pecos proves that the one responsible was Slinger, who is shot dead by Brad. A marshal places Brad under arrest and Rill and Pecos finally get to know each other better.

==Cast==
* Robert Mitchum as Pecos Smith
* Barbara Hale as Rill Lambeth Richard Martin as Chito Rafferty
* Thurston Hall as Colonel Lambeth
* Rita Corday as Suzanne
* Russell Hopton as Jeff Slinger Bill Williams as Tex Evans Bruce Edwards as Clyde Corbin Harry Woods as Brad Sawtelle
* Perc Launders as Sam Sawtelle
* Bryant Washburn as Doc Howard Philip Morris as U.S. Marshal
* Martin Garralaga as Don Manuel

==Release==
The film was popular and earned $151,000 in profits. It was Mitchums final job before his service in the army. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p203 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 