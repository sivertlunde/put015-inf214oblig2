Enemies of Women
{{Infobox film
| name           = Enemies of Women
| caption        =
| image	=	Enemies of Women FilmPoster.jpeg
| director       = Alan Crosland
| producer       = Cosmopolitan Productions
| based on       =  
| writer         =  John Lynch (scenario) Pedro de Cordoba
| music          = William Frederick Peters
| cinematography = Ira H. Morgan
| editing        =
| distributor    = Goldwyn Pictures
| released       =  
| runtime        = 105 min 
| country        = United States Silent English intertitles
}}
 silent Romance romantic drama Pedro de Cordoba, and Paul Panzer. The film was produced by William Randolph Hearst through his Cosmopolitan Productions. Pre-fame actresses Clara Bow and Margaret Dumont have uncredited bit roles.

The film is based on the novel of the same title by Vicente Blasco Ibáñez.

A print of the film at the Library of Congress is believed to be incomplete.  Missing reels 3 and 9 of 11 total. 

==Cast==
*Lionel Barrymore as Prince Lubimoff
*Alma Rubens as Alicia Pedro de Cordoba as Atilio Castro
*Gareth Hughes as Spadoni
*Gladys Hulette as Vittoria William H. Thompson as Colonel Marcos
*William Collier, Jr. as Gaston
*Mario Majeroni as Duke DeDelille
*Betty Bouton as Alicias maid
*Jeanne Brindeau as Madame Spadoni
*Ivan Linow as Terrorist
*Paul Panzer as Cossack
*Clara Bow as Girl Dancing on Table (uncredited)
*Margaret Dumont as French Beauty (uncredited)

==See also==
*List of partially lost films
*Lionel Barrymore filmography

==References==
 

==External links==
*  
* 
*  (*if page wont download click on the   link, then return and click)
* (*if page doesnt load click on the   link then return and click)

 

 
 
 
 
 
 
 
 
 
 


 
 