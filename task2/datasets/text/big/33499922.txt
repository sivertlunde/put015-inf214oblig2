Vs (film)
{{Infobox film
| name           = Vs
| alt            =  
| image          = Vs FilmPoster.jpeg
| caption        =  Theatrical Poster
| director       = Jason Trost
| producer       = Jason Trost Lucas Till
| writer         = Jason Trost
| starring       = Jason Trost Lucas Till James Remar
| music          = George Holdcroft
| cinematography = Amanda Treyz
| editing        = Jason Trost
| studio         = GRINDFIST
| distributor    = Image Entertainment
| released       =  
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = $20,000
| gross          = 
}} independent superhero film starring Jason Trost, James Remar, and Lucas Till. Filming took place in Los Angeles, California. Vs premiered in Toronto, Ontario, at the Toronto After Dark Film Festival on October 26, 2011   Retrieved 2011-10-22 

The film received mixed reaction, praising the acting and storyline, but criticized the limited story and narrative.

==Plot==
Vs centers on four superheroes (Charge, Cutthroat, Shadow, and The Wall) who awaken in a seemingly abandoned town to find themselves stripped of their powers, and at the mercy of their abductor and Arch Nemesis, Rickshaw (Remar). They are shortly thrust into a series of psychological challenges where the stakes include the lives of a town full of kidnapped innocent civilians as well as their own.

The film opens with Charge/John (Trost), Cutthroat/Ben (Till), The Wall/Charlie (Valmassy), and Shadow/Jill (Merkley), waking up in a seemingly abandoned town, all bearing strange injection marks on their wrists. They soon discover television sets through which their nemesis, Rickshaw, whom the group thought Charge had defeated some time ago, explains to them that he has staged a game all across town with innocents lives at stake, and that he has also taken away their powers. To prove this, he executes a civilian near Cutthroats location, then instructs the heroes to head to a meeting ground. Once there, the four have a brief personal reunion, in which the group realizes that Charge still retains some of his abilities, before Rickshaw interrupts them, giving them new orders. Charge assumes command of the group almost unanimously, with the exception of Cutthroat, who appears disgruntled with the others decision.

The group finds themselves in a hardware store, where Rickshaw instructs each of them to choose a weapon, then divides them into teams of two and orders them to a deathmatch with two of his goons in different parts of the town; Charge and Cutthroat head off to the lumber mill, while The Wall and Shadow go to the scrap yard. In the mill, Charge and Cutthroat find a group of civilians strapped to explosives with the fuse already lit, and Charge faces Sledgesaw, a goon of Rickshaws, while Cutthroat tries to defuse the explosives. Charge defeats his opponent and they succeed in cutting the fuse, only for Rickshaw to remotely detonate the device moments after Charge saves Cutthroat and himself by pulling him away from the civilians. While surveying the wreckage, Cutthroat laments in his inability to save the civilians, stating that if he had his power of super speed, he could have saved them. 

Meanwhile, in the scrap yard, The Wall and Shadow are attacked by Manpower, who wields a flamethrower against them. Once it runs out of fuel, The Wall charges him, only to be promptly taken down without his superpower (presumably some form of invincibility, which also renders him immune to physical pain). Shadow tries to help, but without her own power of invisibility, she is unable to prevent Manpower from stabbing The Wall in the stomach. Just as he turns to her, Charge and Cutthroat arrive and defeat Manpower. Charge then informs the group that The Wall only has moments to live, as his wounds are not treatable. He instructs Shadow, who presumably had an intimate relationship with him, to stay with The Wall until the end while he explains to Cutthroat that he has a plan to defeat Rickshaw and that Rickshaw does not care if they win or not, but will execute civilians merely to affect them; he is proven right when, moments after The Walls death, he executes the trapped civilians and then gives the group the coordinates for a new, "Bonus Round".

The group arrives at a cabin where three civilians are held, and they discover three coffins with their names on them, as well as a single gun. Rickshaw appears and tells them that he will spare a civilian for each of them that commits suicide at their respective coffin, but Charge takes the gun and executes the hostages himself, much to the others horror. He argues that Rickshaw would kill them anyway, and that he did it so none of the others would be tempted to take their own lives. Cutthroat tells Shadow that he trusts Charge to have a plan, noting that "...failure is just not an option to him." It is revealed through flashbacks that the four were close friends at some point, that Charge was the one who had urged the rest to become superheroes after they obtained their powers, and that Charge and Cutthroat were working as a team. It is also suggested that Charge and the Shadow had feelings for one another, but never acted on them. Rickshaw congratulates Charge on his actions, then orders them to head to a bar.

Once there, they find two guns next to the TV set. Rickshaw appears and reveals he has Cutthroats sister hostage, then orders Cutthroat to kill the other two in order to save her. Charge and Shadow try to reason with him, but he attacks Charge, bitter about how he was always treated as a sidekick by him. While fighting, he picks up a pair of knives, throwing one onto Shadows shoulder and slashing Charge across the chest with the other, before Charge drives it into his chest. Cutthroat dies, and, soon after, Rickshaw executes his sister. Charge and Shadow grieve briefly before Charge reveals that he loves Shadow, and that, unlike the other three who each got a power and agelessness from "that thing   fell out of the sky", he himself never had any special powers, which is why he had been constantly training and why he has aged over the time they had been apart. He reveals that he can locate Rickshaw and kill him, as he can see them but not hear them, but he needs the location of the next TV set to do so.

When they arrive at the location of the next TV set, Rickshaw orders one to kill the other. Charge tells Shadow that she needs to shoot him in a way that would make it seem as if he died, then he would find and kill Rickshaw while he was distracted monitoring her. She does, unwillingly, and heads out towards the "Last Round". Meanwhile, Charge triangulates his position, then storms his hideout. Rickshaw, shocked to see him alive, is shot by Charge with a shotgun, but manages to activate a failsafe device right before he dies. Charge tells Shadow to head out of town, but she frees the civilians of the Last Round and then finds his map, where he had marked Rickshaws location. She finds him and helps him to his feet, and they leave the hideout as the timer counts down, with little more than a minute left.

In a post-credits scene, a brief shot shows Cutthroats eyes suddenly opening.

==Cast==
*Jason Trost as John / Charge
*Lucas Till as Ben / Cutthroat
*James Remar as Rickshaw
*Sophie Merkley as Jill / Shadow
*Lee Valmassy as Charlie / The Wall
*Sean Whalen as Manpower
*Brian Taylor as Man
*Nick Principe as Sledgesaw

==Development==
Jason Trost (co-writer/director and star of the film  ) and a scenery-chewing James Remar (Dexter (TV series)|Dexter), the grinning villain at the centre of this superhero horror flick."    Retrieved 2011-10-22 

==References==
 
 

==External links==
*   at Facebook
*  
*   at Superhero Hype!

 
 
 
 
 
 