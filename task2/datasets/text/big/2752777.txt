Dragon Eye Congee
{{Infobox film
| name           = Dragon Eye Congee: A Dream of Love
| image          = Dragoneye.jpg
| caption        = 
| director       = Chang Kuo-fu
| producer       = Lee Hsing
| writer         = Bo Yang
| starring       = Fann Wong Shaun Tam Ivy Yin Hsin
| music          = 
| cinematography = 
| editing        = 
| distributor    = Lees Production
| released       =  
| runtime        = 93 minutes
| country        = Taiwan Cantonese
| budget         = NT$5 million
| preceded_by    = 
| followed_by    = 
}}
Dragon Eye Congee: A Dream of Love (龍眼粥 / 龙眼粥) is a 2005 romance film set in the 1960s. Produced by the respected veteran of Taiwan film Lee Hsing and directed by award-winning young director Allen Chang, this romance film stars Singapores Fann Wong, Hong Kongs Shaun Tam and Taiwans Ivy Yin Hsin.

== Cast ==
* Fann Wong
* Shaun Tam
* Ivy Yin Hsin

==Story==

Based on a short story of the same name written in the 1950s by the Taiwanese intellectual and democracy activist Bo Yang, Dragon Eye Congee tells the story of a second-generation Taiwanese American, Shaun Tam, who, since childhood, has repeatedly dreamt about the same woman in the same scenes, complete with a haunting melody and the fragrant smell of rice congee with dried longan.

He is totally mystified about the significance of the dream until he comes to Taiwan for the first time on a business trip and stumbles upon an old house and a woman played by Fann Wong, resembling those in his dreams. Eventually, he realizes that the woman was his lover in a previous lifetime in Taiwan.

==Crew==
Produced by veteran Taiwanese director Lee Hsing of Lees Production Ltd (李行工作室有限公司), he came out of retirement to produce his first film in 20 years (since 1984), funding NT$2 million.  The director of the film is Allen Chang Kuo-Fu, the winner of the Golden Horse Award Best Short Film in 2000.

==Trivia==
This film was nominated for five categories at the inaugural Asian Festival of First Films 2005, held in Singapore.  This film also participated at the Golden Horse Awards Film Festival 2005.

== External links ==
*  
*  

 

 
 
 
 
 


 
 