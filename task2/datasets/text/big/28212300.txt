Yeh Saali Zindagi
 
 
{{Infobox film
| name           = Yeh Saali Zindagi
| image          = Yeh Saali Zindagi Poster.jpg
| caption        = Theatrical release poster
| director       = Sudhir Mishra
| producer       = Prakash Jha
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Irrfan Khan Arunoday Singh  Chitrangada Singh Aditi Rao Hydari
| music          = Nishat Khan Abhishek Ray
| cinematography = Sachin Kumar Krishnan
| editing        = 
| studio         = 
| distributor    = Cine Raas Entertainment Pvt. Ltd.
| released       =   
| runtime        = 134 Minutes
| country        = India
| language       = Hindi
| budget         =   80&nbsp;million
| gross          =   107.5&nbsp;million 
}}

Yeh Saali Zindagi is a 2011 Hindi Romantic thriller directed by Sudhir Mishra. The film stars Irrfan Khan, Chitrangada Singh, Arunoday Singh and Aditi Rao Hydari in the lead roles.    The film was released on 4 February 2011  to positive critical acclaim and was moderately successful at the box office. 

==Plot==
Arun (Irrfan Khan) is a Chartered Accountant who works for Mr. Mehta (Saurabh Shukla) and helps him run his illegal business. Arun is in love with Priti (Chitrangada Singh), but she falls for Shyam (Vipul Gupta) who was introduced to her by Arun.
 Yashpal Sharma), falls out of favour of Minister Verma (Anil Sharma), he is sent to jail and humiliated and beaten everyday on orders of the minister. Satbeer (Sushant Singh) is a dishonest cop who works for Bade and helps him in the jail. Kuldeep (Arunoday Singh) who is also in jail with Bade is released and goes to his wife Shanti (Aditi Rao Hydari). Bades brother, Chote (Prashant Narayanan) and his gang of members, Kuldeep, Tony, Chacha, Guddu and others plan Bades release by kidnapping Vermas to be son-in-law, Shyam and daughter, Anjali (Madhvi Singh). Things do not go as planned and they end up kidnapping Shyam and his girlfriend Priti. To ensure Shyams safety, Priti agrees to liaise between Verma and the gangsters, not knowing that Arun had been following them since the abduction. She meets Anjali, Verma and Shyams father. However, Mr. Verma, who comes to know about Shyams affair with Priti refuses to negotiate Bades release. Arun, in the meantime, transfers illegal and hawala money into the bank accounts of Vermas son and threatens him with leaking this information to media if he does not ensure Bades release. However, it comes to light that Chote wants to get Bade out only to get details of his bank accounts and plans to kill him. Learning of this, Bade gives the details of his foreign bank accounts to Kuldeep.

Upon being rescued from the prison, Chote unexpectedly comes from Georgia and meets Bade. In the meantime, police reach the spot and in the cross fire, Bade escapes from Chote and is helped by Kuldeep and his gang. However, he gets shot in the back and later commits suicide. With Bade dead and Chote looking for him, Kuldeep decides to leave the country and asks Shyams father for a ransom of   15 crores. Priti, under the guise of calling Mr. Singhania, calls Arun. Arun reaches the spot and tells Priti that he transferred   170&nbsp;million into her account which should be used by her to free Shyam and herself and live a happy life. Priti, now realises that she loves Arun.

Priti brings the money to a port from where Kuldeep plans to escape. Chote also reaches there, but Satbeer double crosses him and Kuldeep kills Chote, to avenge the murder of his father. However, one of the bullets hits a railing and richotes, and hits Arun. The gangsters leave the spot and Priti confesses to Arun that she loves him.

==Cast==
*Irrfan Khan as Arun
*Chitrangada Singh as Priti
*Arunoday Singh as Kuldeep
*Aditi Rao Hydari as Shanti
*Saurabh Shukla as Mehta
*Sushant Singh as Satbeer
*Vipin Sharma as Tony Yashpal Sharma as Bade
*Prashant Narayanan as Chote
*Vipul Gupta as Shyam
*Tarun Shukla as Guddu
*Madhvi Singh as ministers daughter

==Reception==

===Critical reception===
The film received positive acclaim from critics. Nikhat Kazmi from the Times of India gave it 4.5/5 saying "Dark, devious and different, Yeh Saali Zindagi is brain-and-brawn drama". 

Sukanya Verma from Rediff.com rated it 3.5/5 calling it a "A twisted entertainer" and further stated that "Yeh Saali Zindagi rocks, its wonderfully unrestrained and entertaining with its mouthful of zingers penned by Mishra and Manu Rishi". 

Rajeev Masand from IBNLive gave it 3 out of 5 stating that "Yeh Saali Zindagi surprises you, it takes its time to unfold, but its a delicious little treat if you muster the patience for it". 

Kaveree Bamzai from India Today gave it 3 out of 5 and said "Its a delightful little trifle that is individual fun, and so well acted". 
 DNA also gave it 3 out of 5 saying that "Yeh Saali Zindagi, is chaotic but enjoyable, just like its hatke title Mishra presents a thrilling and hatke account of two men and the lengths they go to for love". 

Mayank Shekhar from Hindustan Times gave the film 3 out of 5. 
On the review aggregator website ReviewGang, the film has received a 6.5/10 rating.  On Rotten Tomatoes website Yeh Saali Zindagi has received a 75% fresh rating. 

===Box office===
Yeh Saali Zindagi opened well at the box office scoring  45.0&nbsp;million net over its first weekend. {{cite web|title=Yeh Saali Zindagi Picks Up at the Box Office Patiala House and grossed just  3 crore net. It finished its lifetime collections at  107.5&nbsp;million net. {{cite web|title=Yeh Saali Zindagi Has Steady Second Weekend
|url=http://www.boxofficeindia.com/boxnewsdetail.php?page=shownews&articleid=2539&nCat=box_office_news|publisher=Box Office India|date=15 February 2011}}   It was declared "Below Average" by Boxoffice-India. 

==Soundtrack==
The music has been composed by sitarist Nishat Khan and has lyrics penned by Swanand Kirkire.The Yeh Saali Zindagi song has been composed and sung by Abhishek Ray.
{{Infobox album  
| Name        = Yeh Saali Zindagi
| Type        = Soundtrack
| Artist      = Nishat Khan
| Lyricist    = Swanand Kirkire
| Cover       =
| Released    =
| Recorded    =
| Genre       = Film Soundtrack
| Length      = 39:19
| Label       = T-Series
| Producer    =
| Last album  =  
| This album  = Yeh Saali Zindagi (2010)
| Next album  = 
}}
{{tracklist
| headline        = Track listing
| extra_column    = Artist
| extra_credits   = yes
| title1          = Dil Dar Ba Dar
| note1           = 
| extra1          = Shilpa Rao & Javed Ali
| length1         = 5:54
| title2          = Ishq Tere Jalwe
| note2           = 
| extra2          = Shilpa Rao & Javed Ali
| length2         = 5:37
| title3          = Kaise Kahein Alvida
| note3           = 
| extra3          = Javed Ali
| length3         = 4:15
| title4          = Sararara
| note4           = 
| extra4          = Sukhwinder Singh
| length4         = 5:21
| title5          = Sararara
| note5           = 
| extra5          = Javed Ali
| length5         = 5:21
| title6          = Yeh Saali Zindagi
| note6           = 
| extra6          = Sunidhi Chauhan, Kunal Ganjawala & Shilpa Rao
| length6         = 5:01
| title7          = Yeh Saali Zindagi (Bonus Song)
| note7           = 
| extra7          = Abhishek Ray
| length7         = 3:28
| title8          = Yeh Saali Zindagi (Female Version)
| note8           = 
| extra8          = Sunidhi Chauhan & Shilpa Rao
| length8         = 5:02
}}

==References==
 

==External links==
* 

 
 
 
 
 
 
 