The Girl Reporter
 
{{Infobox film
| name           = The Girl Reporter
| image          = File:The Girl Reporter Thanhouser.jpg
| caption        = A surviving film still
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = 
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama produced by the Thanhouser Company. The film follows two sweethearts, May and Will, who are reporters for the Daily Wave newspaper. Will leaves the newspaper to work as a secretary to Blake, the commissioner of public works. Blake takes a bribe and blames Will and fires him. May sets out to clear his name and becomes Blakes new secretary. May investigates and clears Wills name while proving Blakes corruption. The film was released on August 16, 1910 and saw a wide national release. The film received mixed responses from critics who liked the acting, but found issues with the staging and the plausibility of the plot. The film is presumed lost film|lost.

== Plot ==
Though the film is presumed lost film|lost, a synopsis survives in The Moving Picture World from August 20, 1910. It states: "May Merrill and Will Marshall are sweethearts and both reporters on the Daily Wave. Will leaves the paper to accept a position of private secretary to Blake, commissioner of public works. Shortly after Will takes up his new work Blake is threatened with exposure and punishment on his charge of accepting a bribe. In order to save himself, Blake makes it appear that Will is the guilty party. May is sent to investigate the matter for the Wave. When she discovers that Will is accused, she determines to devote all of her time to clearing him, and with this end in view, she applies for the vacant position of private secretary to Blake. Assisted by Pete, faithful office boy from the Wave, who follows her to her new position, May does some clever detective work and, clearing Will, manages to fix the guilt where it belongs, on the shoulders of Blake." 

== Production ==
The writer of the scenario is unknown, but it was most likely Lloyd Lonergan. He was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions.  The film director is unknown, but it may have been Barry ONeil. Film historian Q. David Bowers does not attribute a cameraman for this production, but at least two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    Though the roles of the actors are unknown, it is likely that numerous other character roles and persons appeared in the film. Bowers states that most of the credits are fragmentary for 1910 Thanhouser productions.    The cast could include either or both leading ladies of the company, Anna Rosemond and Violet Heming.   One of the more prominent leading male actors was Frank H. Crane.  
 The Girl Reporter by the Crystal Film Company in 1913.  Another girl reporter would be featured A Columbus Day Conspiracy by the Thanhouser Company. 

== Release and reception == Washington state,  and Illinois.  The film was also shown by the Province Theatre of Vancouver, British Columbia in Canada within days of its release. 

The film received positive attention by film critics and seems to have been well-staged because a reviewer of The Moving Picture News states, "This film points a moral and adorns a tale. Clean as a hounds tooth, sweet as a nut. Full of life. The tale of the triumph of a courageous girl, backed by an inimitable office boy, and the downfall of the grafter are well planned and well rendered. The copy room set our mind flying back to our own cub days. By the way, the paper on the wall of the traction company office is somewhat weird. A good, healthy, vigorous production in every sense."  The New York Dramatic Mirror reviewer said the acting was good, but found fault with the plot itself. The reviewer states, "This film story has melodramatic interest, and the acting is good, but the means by which some of the incidents are brought about will not stand the acid test. For instance, if a political boss wants to shake down a traction company for $5,000, would he submit the proposition in writing? ... The scenes in the newspaper reporters room would have been more convincing in the first scene if the staff had been more busy in writing copy instead of waving their arms and moving about."  The term "traction company" refers to a streetcar company.

== References ==
 

 
 
 
 
 
 
 
 
 