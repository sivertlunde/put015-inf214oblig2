Summer in Bethlehem
 

{{Infobox film
| name           = Summer in Bethlehem
| image          = Summer in Bethlahem.jpg
| image_size     =
| caption        = VCD cover
| director       = Sibi Malayil
| producer       = Siyad Koker Ranjith
| story          = Venu Nagavally
| narrator       = Janardhanan
| Vidyasagar
| lyrics         = Girish Puthenchery
| editing        = L. Bhoominathan
| cinematography = Sanjeev Shankar
| studio         = Kokers Films
| distributor    =  Anupama Release & PJ Entertainments UK
| genre          =
| released       =  
| runtime        = 143 minutes
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Ranjith and directed by Sibi Malayil. It stars Suresh Gopi, Jayaram, and Manju Warrier with a cameo appearance by Mohanlal. The music was composed by Vidyasagar (music director)|Vidyasagar.
 estate known as Bethlehem Estates in Chandragiri.

==Plot==
Ravishankar (Jayaram), an unsuccessful investor, stays with Dennis (Suresh Gopi), his friend who is a big success in farming business. Dennis owns a vast farmland named Bethlehem, and hundreds of cows, in a valley. Ravi is a fun-loving, jovial chap, who has fabricated stories of his success to his parents and relatives. 

On a vacation, Colonel C.R. Menon (Janardhanan (actor)|Janardhanan), his grandfather arrives with his grandchildren to spend a couple of days at the farmhouse of Ravishankar. He successfully makes them believe that he is the real owner of Bethlehem, and Dennis is his partner. Dennis, who is an orphan, was happy to meet a huge family and welcomes them with full heart. Colonel Menon has a plan to get Ravishankar married to any one of his granddaughters. Ravishankar is a bit confused about whom he should choose. For past few months, he had been receiving a mail from one among these girls expressing her love for him. Ravi and Dennis decides to find out the one behind this game. Meanwhile, a few days after their arrival at Bethlehem, Abhirami (Manju Warrier), another granddaughter of Menon, who is studying at Bangalore arrives alone. She seems to be too upset and worried. But within short time, she returns to normalcy and enjoys the colors of vacation. They play pranks on Ravi and Dennis and both enjoys them well.

Once, Dennis and Ravi asked everyone to assemble in the yard to play something for entertainment. When everybody came there, Ravi told he will sing a few lines of a song and anyone has to complete it. When he finish singing nobody completed it reasoning that how can they sing a song they havent ever heard and Abhirami say that she have heard this song from their cottage.

One day Dennis told Abhirami that, Ravi has been receiving those mails from one of the girls.
Abhirami questions all the girls and finds that it is from one of the two of the whole girl gang. 

As time passes, Dennis feels a special attraction for Abhirami. 

Colonel Menon and his wife decide to get Ravi married to Abhirami. She snubs them by declaring that she is in love with Dennis. Her decision creates a panic among all, who believes that Dennis is a failed businessman who is living at the expense of Ravishankar. Ravi shocks everyone by revealing that he is nothing and is the original owner of Bethlehem is none other than Dennis. Abhirami meets Dennis who is completely broken down after the chaos at the house. Dennis, at the same time, is happy at heart knowing about Abhiramis love for him. But she shocks him by saying that she was just using his name to escape the marriage. She tells Dennis that she is in love with Niranjan  , a Naxal revolutionary in southern Karnataka. Niranjan, whom she met at Bangalore, is now in jail, convicted of killing Brijesh Mallaya, a landlord and his family. He has got capital punishment and is waiting for the gallows. He is to be executed in a couple of days; Abhirami has taken a vow not to marry anyone other than Niranjan. 

But at home, upon the compulsion from Ravi, the family decides to get Abhirami married to Dennis. The night before marriage, Abhirami requests Dennis to take her to jail to meet Niranjan as he is getting executed the next day. Dennis takes her to Niranjan (Mohanlal), who is a completely changed man. Remorseful Niranjan regrets the violent ways he adopted in class war and the crimes he did to obtain a classless society. In jail, Niranjan became more spiritual and advises Abhirami to forget him and accept Dennis as her husband. Abhirami refuses to take his words, but Niranjan forces Dennis to tie the mangalsutra to Abhirami, which she has brought with her. Dennis obeys Niranjan, marries Abhirami at the jail, and Niranjan witnesses it with tearful eyes. He goes back to his cell happily by wishing them all success. A few days later, the family returns from Bethlehem, while Ravi jokingly reminds everyone that they should be back here next year to welcome junior Dennis. The train departs slowly and a girls hand reaches out the rapidly receding coach window, holding the kitten which was sent as a gift. Ravishankar takes off running to find out who it is, but he catches only a cryptic message that teases him to follow and discover her identity.

==Cast==
* Suresh Gopi as Dennis
* Jayaram as Ravishankar
* Mohanlal as Niranjan (Special appearance)
* Manju Warrier as	Abhirami Janardhanan as Colonel C.R. Menon
* Kalabhavan Mani as Monai
* Sukumari as Ravishankars grandmother Manjula as Aparna Rasika as Jyothi
* Sreejaya as Devika
* Sadiq Krishna
* Reena
* Augustine
* Sujitha
* Dhanya Menon
* Kripa Mayoori as Gayathri

==Remake Controversy== Fiddler On The Roof and Come September.  The matter was later settled.

==Trivia==

* This was the second script written by Renjith for Sibi Malayil after Maya Mayooram. Later they joined again for Ustaad in 1999.
* The movie is also famous for the cameo of actor Mohanlal as a convict, who is in the green mile.

==Soundtrack==
{{Infobox album  
| Name       = Summer in Bethlehem
| Type       = soundtrack Vidyasagar
| Cover      = SummerinBethlehem.jpg
| Alt        = 
| Released   = October 1998
| Recorded   = 1998 Varshaa Vallaki, Chennai Feature film soundtrack
| Length     = 
| Label      = Sargam 
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}
 Vidyasagar and written by Gireesh Puthenchery. The soundtrack had seven songs. The soundtrack became one of the biggest hits in Kerala.

{|class="wikitable" width="60%"
! Track !! Song Title !! Singer(s) !! Raga
|-
| 1 || Oru Raathri Koodi || K. J. Yesudas|Dr. K. J. Yesudas, K. S. Chithra || Abheri
|- Sujatha || Kapi 
|-
| 3 || Marivillin Gopurangal || Biju Narayanan, Srinivas ||
|- Chorus || Vrindavana Saranga
|-
| 5 || Oru Raathri Koodi || Dr. K. J. Yesudas || Abheri
|-
| 6 || Poonchillamel || K. S. Chithra, Chorus ||
|-
| 7 || Kunnimani Koottil || M. G. Sreekumar, K. S. Chithra ||
|-
| 8 || Confusion Theerkaname || M. G. Sreekumar || Shanmukhapriya
|-
| 9 || Oru Raathri Koodi || K. S. Chithra || Abheri
|}

== References ==
 

==External links==
*  
*   at Oneindia.in
 

 
 
 
 
 