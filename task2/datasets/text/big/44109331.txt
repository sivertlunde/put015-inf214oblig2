Aadhi Thaalam
{{Infobox film 
| name           = Aadhi Thaalam
| image          =
| caption        =
| director       = Jayadevan
| producer       = K Prasannakumar
| writer         = Jayadevan
| screenplay     = Jayadevan
| starring       =
| music          = Navas
| cinematography = S Balraj
| editing        = C Mani
| studio         = Karthika Movies
| distributor    = Karthika Movies
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, directed by Jayadevanand produced by K Prasannakumar.  The film had musical score by Navas.   

==Cast==

 

==Soundtrack==
The music was composed by Navas and lyrics was written by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Hridayam oru chashakam || KS Chithra || Poovachal Khader || 
|-
| 2 || Thottaal viriyum || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 


 