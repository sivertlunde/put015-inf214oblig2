The Castilian
{{Infobox Film
| name           = The Castilian
| image          = El_valle_de_las_espadas-213946089-large.jpg
| image_size     = 
| caption        = 
| director       = Javier Seto
| producer       = Sidney W. Pink
| writer         = Paulino Rodrigo Luis de los Arcos Javier Setó
| screenplay     = Paulino Rodrigo Luis de los Arcos Javier Setó
| based on       = Poema de Fernán González
| starring       = Cesar Romero Frankie Avalon
| music          = José Buenagú	
| cinematography = Mario Pacheco
| editing        = Richard Mayer Margarita de Ochoa	
| studio         = Producciones Cinematográficas M.D. S.L.
| distributor    = Warner Bros. (USA)
| released       = 1963
| runtime        = 120 mins
| country        = Spain
| language       = English
| budget         = 
| gross          = 
}}
 Spanish Reconquista.

This film was directed by Javier Setó. Filmed in Spain; exteriors filmed in Burgos and Peñafiel (Valladolid).

==Plot summary== 
Don Sancho (Broderick Crawford) is a despotic 10th century Spanish king who, in cahoots with the invading Moors, has banished handsome Castilian nobleman Fernán González (Espartaco Santoni). With the surreptitious aid of Don Sanchos daughter, Sancha (Tere Velázquez), Fernán González assembles an army to march against the Moors. 

==Cast==
* Cesar Romero as "Jerónimo"
* Frankie Avalon as "Jerifán"
* Broderick Crawford as "Don Sancho"
* Alida Valli as "Reina Teresa"
* Espartaco Santoni as "Fernán González"
* Tere Velázquez as "Sancha"
* Fernando Rey as "Ramiro II of León"

==Production==
The film was known as The Valley of the Swords. Jane Fonda Heads Hannos Doll Cast
Los Angeles Times (1923-Current File)   19 June 1962: C7.  
==References==
 

==See also==
* list of historical drama films

==External links==
*  
*   at TCMDB
*  

 
 
 
 
 
 

 
 