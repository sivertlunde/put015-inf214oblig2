A Painting of Roses
{{Infobox film
| name           = Um Quadro de Rosas
| image          = 
| caption        = 
| director       = Miguel Ribeiro
| producer       = 
| writer         = Miguel Ribeiro
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| music          = 
| cinematography = Miguel Ribeiro
| editing        = Miguel Ribeiro
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Portugal
| language       = Portuguese
| budget         = 
| gross          = 
}}

A Painting of Roses ( ) is a short documentary by Miguel Ribeiro.

It was produced in 2003 by Bookcase, an independent Portuguese film company.

==Festival and awards==
* Café das Imagens, Videoteca de Lisboa, 2003
* Mostra de Vídeo Português, Videoteca de Lisboa, 2004
* XI Caminhos do Cinema Português, Coimbra, 2004
Best documentary - video category
* Art Festival at Palazzo Venezia – International Exhibition of Art films and Documentaries, Roma, 2004
* X Festival Internacional de Cinema e Vídeo de Ambiente da Serra da Estrela, Seia, 2004
* FIKE, Festival Internacional de Curtas-Metragens de Évora, 2004
* IMAGO, V Festival Internacional de Cinema e vídeo Jovem, Fundão, 2004
* Pärnu International Documentary and Anthropology Film Festival, Estónia, 2004
* Jovens Criadores 2004, Clube Português de Artes e Ideias, 2004
* 71st MIFED, - International Cinema and Multimedia Market, Milão, 2004
Award EMERGING EUROPEAN FILMMAKERS
*NOVIDAD Film Fest, Covilhã, 2004
Best documentary
* IX Festival Internacional de Curtas-metragens de Teerão, 2004
* Jornadas  lugar d’arte- mostra de vídeo, cine-teatro Baltazar Dias, Funchal, 2005
* VI European Cinema Festival. Lecce, 2005
* XII Bienal de Jovens Criadores da Europa e do Mediterrâneo, Nápoles, 2005
* V Mostra Audiovisual "El Mes + Corto", Cáceres, Badajoz, Evora e Lisboa, 2006

==External links==
*  
*    

 
 
 

 