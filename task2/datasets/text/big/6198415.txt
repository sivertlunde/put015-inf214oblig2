Metropolis (2001 film)
{{Blacklisted-links|1=
*http://tezukainenglish.com/?q=node/100
*:Triggered by  \btezukainenglish\.com\b  on the global blacklist|bot=Cyberbot II|invisible=false}}
{{Infobox film
| name = Metropolis
| image = Metropolisanime poster.jpg
| alt = 
| caption = Theatrical release poster
| director = Rintaro
| producer = Shinji Komori Masao Maruyama Satoshi Yamaki
| writer = Katsuhiro Otomo
| starring = Kei Kobayashi Yuka Imoto Kōki Okada Tarō Ishida Kōsei Tomita
| music = Toshiyuki Honda
| cinematography = Hitoshi Yamaguchi Madhouse
| distributor = Toho
| released =   
| runtime = 113 minutes
| country = Japan
| language = Japanese
| budget = $15 million
| gross = $4,035,192  (North America)  
}}
 silent film Madhouse with conceptual support from Tezuka Productions.

==Plot==
In the futuristic city of Metropolis humans and robots coexist. Robots are discriminated against and segregated to the citys lower levels. A lot of Metropolis human population is unemployed and deprived, and many people blame the robots for taking their jobs.

Duke Red, the unofficial ruler of Metropolis, has overseen the construction of a massive skyscraper called the Ziggurat, which he claims will allow mankind to extend its power across the planet. A wayward robot disrupts the Ziggurats opening ceremony, only to be shot down by Rock, Duke Reds adopted son and the head of the Marduk Party, a vigilante group whos aim is to calm anti-robot sentiments. Private detective Shunsaku Ban and his nephew Kenichi travel to Metropolis to arrest Dr. Laughton, a mad scientist wanted for organ trafficking. Unbeknownst to Shunsaku, Duke Red has hired Laughton to build an advanced robot modeled and named after Reds deceased daughter Tima. Red intends for Tima to function as a central control unit for a powerful secret weapon hidden in the Ziggurat. However, Rock learns of Timas existence and, not wanting a robot to overshadow Red, shoots Laughton and sets fire to his laboratory.

Shunsaku comes across the burning lab and discovers the dying Laughton, who gives him his notebook. Meanwile Kenichi finds the activated Tima. The two fall into the sewers and are separated from Shunsaku. While Shunsaku searches for his nephew, Kenichi and Tima search for a way back the street level. They grow close as Kenichi teaches Tima how to speak. Neither are aware she is a robot. The two are hunted relentlessly by Rock and his subordinates, and encounter a group of unemployed human laborers who stage a revolution against Red.

The president and the mayor of Metropolis try to use the revolution to overthrow Red and gain control of Metropolis, but they are assassinated by the presidents top military commander General Kusai Skunk, who has sided with Red. The duke imposes martial law to suppress the revolution. In the aftermath of the failed revolt, Kenichi reunites with  Shunsaku, only to be wounded by Rock, who reveals Tima to be a robot. Rock however is disowned by Red and stripped of his command of the Marduks for attempting to kill Tima. Duke Red takes Tima away to the Ziggurat.

Still determined to dispose of Tima and regain his fathers affections, Rock kidnaps and deactivates Tima, who is now confused about her identity. Shunsaku rescues her, and after following instructions from Laughtons notebook, reactivates Tima. The two discover Kenichi is being held in the Ziggurat and are captured by Duke Red and the Marduks on their way to save him. Brought to the pinnacle of the Ziggurat, Tima confronts Duke Red about whether she is a human or robot. Duke Red tells her she is a "superhuman" and destined to rule the world from her throne. Disguised as a maid, Rock shoots Tima, exposing her circuitry.

The sudden shock of realizing she is a robot causes Tima to go insane. Sitting on the throne, she orders a biological and nuclear attack on humanity in revenge. While the others flee, Kenichi tries to reason with Tima. Robots drawn by Timas command attack Duke Red. Not wanting his father to die at the hands of filthy robots, Rock kills himself and Duke Red in a massive explosion. As the fortress starts to collapse around them Kenichi reaches Tima and separates her from the throne. Seemingly lost, Tima tries to kill Kenichi. In the struggle Tima falls off the tower. Kenichi tries to pull her up using one of the cables still grafted to her. As the cable begins to fray Tima remembers the time Kenichi taught her language and asks Kenichi, "Who am I?" before she slips and falls to her death. The Ziggurat collapses, destroying a large part of Metropolis.

The next morning Kenichi searches the ruins and discovers a group of robots have salvaged some of Timas parts in an effort to rebuild her. While Shunsaku and many other human survivors are evacuated, Kenichi chooses to remain behind and rebuild the city. Kenichi wants to create a place where humans and robots can coexist peacefully.

===Differences between manga and anime===
In Osamu Tezuka|Tezukas original manga, the story revolves around a humanoid named Mitchi, who has the ability to fly and change gender. Mitchi is pursued by Duke Red and his "Red Party" who intend to use Mitchi for destructive purposes. However, Shunsaku Ban and his nephew Kenichi find Mitchi after her creator, Dr. Charles Laughton, is killed and protect her as they search for her parents. Unlike Timas desire to be human, the cause for Mitchis destructive rampage in the mangas climax is the revelation that, as a robot, she does not have parents.
 Metropolis manga, Tezuka said that the only real inspiration he got from Fritz Langs Metropolis (1927 film)|Metropolis was a still image from the movie where a female robot was being born.   In addition to adopting set designs of the original film, this version has more emphasis on a strong and pervasive theme of class struggle in a dystopian, plutocratic society and expands it to examine the relationship of robots with their human masters.  (This relationship was explored by Tezuka in great detail with his popular series Astro Boy.)  The anime adaptation also removes many of the more fanciful elements out of Tezukas manga, such as a flying, gender swapping humanoid.  Here, Mitchi is replaced by "Tima", who is permanently female and cannot fly.

===Deleted scene===
The Japanese release of the film shows a picture after the credits depicting a shop named "Kenichi & Tima Robot Company," with Tima visible in the window. This implies that Kenichi succeeded in rebuilding Tima and they set up the shop together. This picture was included in the English theatrical release and Blu-ray printing, though not on the DVD release.

==Voice Cast==

===Japanese===

*Kei Kobayashi - Kenichi
*Yuka Imoto - Tima
*Kōsei Tomita - Detective Shinsaku Ban
*Koki Okada - Rock
*Tarô Ishida - Duke Red
*Norio Wakamoto - Pero
*Norihiro Inoue - Atlas
*Masaru Ikeda -President Boone
*Toshio Furukawa - General Kusai Skunk
*Takeshi Aono -Dr. Ponkotsu
*Junpei Takiguchi - Dr. Laughton
*Shun Yashiro - Supt. Notarlin
*Shigeru Chiba - Acetylene Lamp
*Rikako Aikawa - Fifi
*Masashi Ebara - Ham Egg

===English===

*Brianne Siddall - Kenichi   
*Rebecca Forstadt - Tima 
*Tony Pope - Detective Shinsaku Ban
*Jamieson Price - Duke Red 
*Michael Reisz - Rock 
*Dave Mallow - Pero
*Scott Weinger - Atlas
*Simon Prescott - Dr. Laughton 
*Richard Plantagenet - President Boone 
*Dan Woren - General Kusai Skunk
*Peter Spellos - Mayor Lyon
*Steve Blum - Acetylene Lamp Doug Stone - Dr. Ponkotsu
*William Frederick Knight - Supt. Notarlin
*Barbara Goodson - Emmy
*Mary Elizabeth McGlynn - Announcement Voice

==Soundtrack==
{{Infobox album  
| Name       = Metropolis – Original Soundtrack
| Type       = soundtrack
| Artist     = Toshiyuki Honda
| Cover      = Metropolis_Soundtrack_Cover.png
| Alt        = 
| Released   = 2002 
| Recorded   = 
| Genre      = Jazz, Dixieland
| Length     =  
| Label      = 
| Producer   = 
| Last album = 
| This album = 
| Next album = 
}}
The Metropolis soundtrack consists mainly of New Orleans-style jazz music composed by Toshiyuki Honda and features Atsuki Kimuras cover of "St. James Infirmary Blues" and the ending theme "Therell Never Be Good-Bye" by Minako "Mooki" Obata. The soundtrack album is available on King Records.

During the films climactic scene, the song "I Cant Stop Loving You" performed by Ray Charles was used as most of the audio when the Ziggurat was destroyed, with sound effects only audible later on in the scene. The song is not included on the soundtrack album.

{{Track listing
| collapsed       = yes
| total_length    = 63 min
| writing_credits = yes

| title1          = Metropolis
| writer1         = Toshiyuki Honda
| length1         = 4:08
| title2          = Foreboding
| writer2         = Toshiyuki Honda
| length2         = 2:42
| title3          = Ziggurat
| writer3         = Toshiyuki Honda
| length3         = 2:56
| title4          = Going to "Zone"
| writer4         = Toshiyuki Honda
| length4         = 2:03
| title5          = Sniper
| writer5         = Toshiyuki Honda
| length5         = 3:04
| title6          = El Bombero
| writer6         = Toshiyuki Honda
| length6         = 2:20
| title7          = Three-Faced of "Zone"
| writer7         = Toshiyuki Honda
| length7         = 5:12
| title8          = "Zone Rhapsody"
| writer8         = Toshiyuki Honda
| length8         = 2:18
| title9          = Hide Out
| writer9         = Toshiyuki Honda
| length9         = 1:30
| title10         = Run
| writer10        = Toshiyuki Honda
| length10        = 3:18
| title11         = St. James Infirmary
| writer11        = Toshiyuki Honda
| note11          = performed by Atsuki Kimura
| length11        = 3:02
| title12         = Sympathy
| writer12        = Toshiyuki Honda
| length12        = 1:37
| title13         = Snow
| writer13        = Toshiyuki Honda
| length13        = 6:53
| title14         = Propaganda
| writer14        = Toshiyuki Honda
| length14        = 1:07
| title15         = Chase
| writer15        = Toshiyuki Honda
| length15        = 2:18
| title16         = Judgement
| writer16        = Toshiyuki Honda
| length16        = 1:36
| title17         = Awakening
| writer17        = Toshiyuki Honda
| length17        = 1:39
| title18         = Fury
| writer18        = Toshiyuki Honda
| length18        = 3:07
| title19         = After All
| writer19        = Toshiyuki Honda
| length19        = 3:42
| title20         = Therell Never Be Good-Bye
| writer20        = Toshiyuki Honda
| note20          = performed by Minako "Mooki" Obata
| length20        = 4:38
}}
 

==Release==
The film was first released in Japan on April 26, 2001. When it was released in the USA and other foreign countries it made a total of $4,035,192. In the United States, the film was given a PG-13 rating by the MPAA for "violence and images of destruction" and TV Parental Guidelines|TV-14-LV rating when it aired on Adult Swim It was also one of the first anime films to be submitted for consideration for Best Animated Film at the Academy Awards.

===Home video===
Metropolis was first released on VHS, and is now available in North America as a two disc DVD, with the second disc being a MiniDVD (called a "Pocket DVD").

==Reception==
Metropolis received highly positive reviews, based on 60 reviews from Rotten Tomatoes, Metropolis received an overall 86% Certified Fresh approval rating. The sites critical consensus states that "Even though the storyline is nothing new, Metropolis is an eye-popping visual treat."   
A quote from James Cameron was used on the DVD cover.

==See also==
* List of Osamu Tezuka anime
* Metropolis (manga)|Metropolis - Manga
* Osamu Tezuka Osamu Tezukas Star System

==References==
 

==External links==
*   (US)
*  
*  
*  
*  
*  
*  
*     
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 