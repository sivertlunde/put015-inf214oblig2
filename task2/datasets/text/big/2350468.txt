Prison on Fire II
 
 
{{Infobox film
| name           = Prison on Fire II
| image          = Cityonfireposter.jpg
| image_size     = 
| caption        = 
| director       = Ringo Lam
| producer       = Karl Maka
| writer         = Nam Yin
| starring       = Chow Yun-fat Chen Sung-young Roy Cheung
| music          = Lowell Lo
| cinematography = Andy Fan
| editing        = Tony Chow
| distributor    = Cinema City Film Co. Ltd. Golden Princess Film Production Ltd.
| released       =  
| runtime        = 109 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
}} Hong Kong action film directed by Ringo Lam, and starring Chow Yun-fat and Roy Cheung. It is the sequel to the 1987s Prison on Fire.

==Plot==
Like the first film, this follows the fraternal bond of two inmates, once again Ching, but this time with "Boss Dragon", a boss of the "Black Rats" gang. Ching escapes the prison in order to see his son, Leung was placed in an orphanage after his mother died, and gets into trouble with Assassins replacement, Officer Zau. Ching escapes a second time to follow Boss Dragon after he lied about jumping off a cliff to escape. Ching looked for his son and intended to bring him along, but was refused by the social worker.

He was later apprehended by the police and was brought back to prison. On the way, Officer Zau tortures Ching. Ching gets thrown into the prison cell of the Mainland group and is beaten up by them as they thought he was the one who betrayed Brother Dragon. Skull takes a toothbrush, with a sharpened back and stabs Ching. Ching then convinces Fireball Brother and the other cellmates to set a fire in the prison cell, so as to escape from prison.

After the fire breaksout, the prisoners assembles outside, but Ching yells for help and runs towards to the Hongkie group for help. A fight then occurs between the Hongkie group and the Mainlanders, resulting in the arrival of more police personnel, who use water jet sprayers to control the situation, while Skull goes into hiding inside the canteen. Ching follows Skull and a violent fight starts. Officer Zau and his officers witness the fight and instead of breaking it up, Zau gives the order to lock them up, watching them fighting behind the gate. Skull begs Officer Zau to open the gate but Zau refuses.

Skull self reproaches on his mistakes to Ching, and when Ching turns his back, Skull tries to stab Ching with the toothbrush again. Ching eventually knocks Skull down. Officer Zau then opens up the gate,and tries to use his baton to hit Ching, but was stopped by a good officer. Unfortunately, the good officer gets knocked down by Zaus barbaric act. Ching picks up the toothbrush and hides it, while Officer Zau approaches him. A violent fight starts and Ching uses the toothbrush, and stabs it into Officer Zaus left eye. Officer Zau screams in pain and engages in delirium. Officer Zau collapses on the floor and Ching faints. Ching awakes in a hospital and takes up his sons report card and reads it. 

His cellmate commented that his sons "handwriting was not bad", also a message for his dad: Dad, dont be naughty in prison, Dont let me worry about you, remember... this sentence is meaningful, Tolerate, Tolerate, Tolerate.... Ching was discharged from the hospital and was being brought back to his cell, an officer told him that he was lucky that he had an officer to stand witness for him about the fight.

A superintendent visits the cell and it is a coincidence that they met together when he escaped the prison for the first time. Ching requests the superintendent, when he visits the orphanage, to look out for his son. As the ending credits roll, an officer says "Long time no see" to Ching, and it is believed to be Officer Scarface from the previous movie, Ching then self mutters ... about how unlucky he is.

==Cast==
* Chow Yun-fat - Chung Tin-ching
* Chen Chung-young - Dragon
* Roy Cheung - Officer Scarface Hung
* Victor Hon - Chiu Chow Man
* Ng Chi-hung - Blind Snake
* Elvis Tsui - Officer Zau
* Wan Yeung-ming - Fai Chi
* Tommy Wong - Bill
* Yu Li - Miss Wong

==See also==
*Chow Yun-fat filmography
*List of Hong Kong films

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 