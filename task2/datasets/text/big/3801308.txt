Heads or Tails (2005 film)

{{Infobox Film
| name           = 
| image          = 
| image_size     = 
| caption        = 
| director       = Clark Kline Joey McAdams 
| producer       = Ryan Keller Jared Freedom Leonard
| writer         = Clark Kline Joey McAdams
| narrator       = 
| starring       = Jeff Keilholtz Jon Liddiard Douglas Cathro Amber Davila Mike Hotovy Madalyn Loughlin
| music          = Will Saxton
| cinematography = Brian Mackey
| editing        = Jake Ruthven
| distributor    = Fool Martyr Productions 2005
| runtime        = 
| country        = United States English
| budget         = United States|$12,000
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Heads or Tails is a  , By Dickson Mercer (February 10, 2005), "Heads or Tails -- local moviegoers win", accessed 01-03-2009     , by Nathan Oravec (February 16, 2005), "Moviemakers fulfill a wish with heads or tails", accessed 01-03-2009  

The screenplay was adapted  from a short story Kline wrote in 1994.  Shooting began in September 2003 with a script that was shrouded in secrecy even from cast and crew, as each were given the days lines on the day a scene was to be shot.     Its premiere gala acreening was at The Weinberg Center for the Arts in February 2005.    A review by Courtney Oates of The Flow magazine describes Heads or Tails as an "incredibly unique story of murder, myths and mania."  It was released to DVD in the spring  of 2008.

==Synopsis== myth and cycles of the moon, two people stand on a cliff and throw a coin off to compete for a wish. Only those that partake in the ritual see that the wish is only the beginning. 

==Cast==
* Jeff Keilholtz as Blaze
* Jon Liddiard as Typhoon
* Douglas Cathro as Miller
* Amber Davila as Kari
* Mike Hotovy as John
* Madalyn Loughlin as Anise
;additional cast
* Mary Hotovy as Julianna
* Wendy Loughlin as Millers Wife
* Christine Maisano as Samantha
* Marsha OConnor as News Reporter
* Stewart Walker as  Greg
* Clark Kline as Blazes Father

==Additional sources==
*Maryland Gazette, "...Joey McAdams, a director and producer with Fool Martyr productions..." 
*Film Fodder, "Fool Martyr Q&A" 
*Tunecore, Heads Or Tails Motion Picture Soundtrack by Will Sxton on fool martyr productions 
*Hunnting Dragonflies, "...Douglas DC Cathro recently made his feature screen debut in the Fool Martyr production Heads or Tails." 
*Cinemarts, "Formed in 2003, Fool Martyr Productions, an ultra-low budget production company, screened their first feature film Heads or Tails to a capacity audience at the Weinberg in early 2005 and stirred a lively debate in the local paper for weeks after the screening." 
*Frederick News Post, filmmaker "Joey McAdams meets up with Wiley Mackintosh and Elina Mavashev to shoot "below" at various locations around Frederick" 
*Mobie Bytes, "...Joey McAdams formed Fool Martyr Productions, LLC in 2003 with writing partner Clark Kline..." 

==References==
 

==External links==
*  at Internet Movie Database
* 
* 
* 

 
 
 