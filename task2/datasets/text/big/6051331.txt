Back Door to Hell
{{Infobox film
| name            = Back Door to Hell
| image           = Back_door_moviep.jpg
| image_size      =
| caption         =
| director        = Monte Hellman
| producer        = Fred Roos John Hackett Richard A. Guttman Jimmie Rodgers John Hackett Jack Nicholson
| music           = Mike Velarde
| cinematography  = Mars Rasca
| editing         = Fely Crisostomo
| studio          = Lippert Pictures Medallion Films
| distributor     = Twentieth Century-Fox
| released        =  
| runtime         = 75 min.
| country         = United States
| language        = English
| budget          =
}}

Back Door to Hell is a 1964 film concerning a three-man team of United States soldiers preparing the way for Douglas MacArthur|Gen. MacArthurs World War II return to the Philippines by destroying a Japanese communications center. It was produced on a relatively small budget and received lukewarm reviews, and is most notable as one of Jack Nicholsons earlier roles. Hellman, Nicholson and Hackett also made the film back to back with Flight to Fury (1964).

== Plot==
 

== Cast == Jimmie Rodgers as Lt. Craig
* Jack Nicholson as Burnett
* John Hackett as Jersey
* Annabelle Huggins as Maria
* Conrad Maga as Paco
* Johnny Monteiro as Ramundo
* Joe Sison as Japanese Capt.
* Henry Duval as Garde
* Ben Perez
* Vic Uematsu

==Production== Thunder Island Jimmie Rodgers had a substantial part in the film, and co-financed it.

The film, directed by Monte Hellman, was shot on location in the Philippines, giving it a particularly authentic look. The same plot was reused in Ib Melchiors Ambush Bay (1966) with a larger Marine patrol destroying a minefield prior to the American and Filipino invasion of the Philippines.

==Notes==
 

== External links ==
*  
*  
*  
*  
* original film trailer http://www.youtube.com/watch?v=MhwQG--b_Uk&feature=related

 

 
 
 
 
 
 
 
 


 