Cheran Pandiyan
 
{{Infobox film
| name = Cheran Pandiyan
| image =
| caption =
| director = K. S. Ravikumar
| writer = K. S. Ravikumar Erode Soundar (dialogues) Vijayakumar Nagesh Chitra Goundamani Senthil K.S. Ravikumar
| producer = R. B. Choudary
| music = Soundaryan
| editing = K. Thanikachalam Super Good Films Super Good Films
| released =  
| cinematography = Ashok Rajan
| language = Tamil
| country = India
| budget =
}}
Cheran Pandiyan ( ; is an Indian film written and directed by K. S. Ravikumar. The film proved to be successful at the box office completed 200 days.

==Plot==
Vijayakumar is the head of the village (Oor Gounder) with his wife Manjula and his daughter Srija. He is a person of who strictly adheres to the caste. Sarathkumar is also in the same village. The father of both Vijayakumar and Sarathkumar are one but Sarathkumars mother is from a lower caste. So Vijayakumar always ignores Sarathkumar. Anandbabu who is relative of Sarathkumar comes to meet him in the village and falls in love with Srija. Vijayakumar comes to know about this and he makes arrangements for Srijas marriage with K.S.Ravikumar, son of Nagesh. At the end Vijayakumar becomes a good person with the advice of Manjula and a happy end of Anandbabu marrying Srija.

==Remake== Shobhan Babu, Rajasekhar (actor)|Dr. Rajashekhar, Jagapathi Babu, Ramya Krishna in the lead roles.

==Cast==
* Sarathkumar as Chinna Gounder (Rajapaandi Gounder)
* Sreeja as Vennila Vijayakumar as Periya Gounder
* Nagesh as Maniyam
* Anand Babu as Chandran
* Manjula Vijayakumar as Paarvathi
* Goundamani as Mechanic Maanikkam (motor mechanic)
* Anuja as Manikkams wife
* Senthil Chitra as Parimalam
* K.S. Ravikumar
* Kumarimuthu Cheran as Bus conductor

==Location==
The movie was shot around the Siruvani area such as Alandhurai, Semmedu, Iruttuppallam, Thombilipalayam, Mullangadu, Karamarathur, Narasipuram, Poondi, Sadivayal.

==Soundtrack==
{{Infobox album 
| Name = Cheran Pandiyan
| Type = soundtrack
| Cover =
| Released = 1991
| Genre =
| Length = 40: 22
| Composer = Soundaryan
| Label = Lahari Music
}}

===Songs===
The music composed by Soundaryan while lyrics also written by himself.
{{Track listing
| extra_column = Singer(s) Chitra | length1 = 5:19
| title2 = Kadhal Kaditham | extra2 = S.A. Rajkumar, Swarnalatha | length2 = 5:00
| title3 = Samba Naathu | extra3 = Swarnalatha | length3 = 4:47
| title4 = Va Va Endhan | extra4 = S. P. Balasubrahmaniam | length4 = 4:43
| title5 = Kodiyum | extra5 = Malaysia Vasudevan, Sunandha | length5 = 5:05
| title6 = Chinna Thangam | extra6 = K. J. Yesudas | length6 = 4:33 Chitra | length7 = 5:22
| title8 = Ooru Vittu Ooru | extra8 = S. P. Sailaja | length8 = 5:24
}}

 

 
 
 
 
 
 
 


 