Ashamed (film)
{{Infobox film
| name           = Ashamed 
| image          = Ashamed.jpg
| film name      = {{Film name
| hangul         =  
| hanja          =  해
| rr             = Changpihae  
| mr             = Ch‘angp‘ihae}}
| director       = Kim Soo-hyeon  
| producer       = Lee Gyeong-hui
| writer         = Kim Soo-hyeon 
| starring       = Kim Hyo-jin   Kim Kkot-bi 
| music          = Lee Eun-jeong
| cinematography = Kim Jin-woo
| editing        = Lee Yeon-jin   Seo Seung-hyeon
| distributor    = Mountain Pictures 
| released       =  
| runtime        = 129 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =     
}}
Ashamed ( ), also known under the international title Life Is Peachy, is a South Korean queer film written and directed by Kim Soo-hyeon. This is Kims second feature film after 2004s So Cute. Ashamed was released in theaters on December 8, 2011, but had already been making the rounds on the festival circuit. The film had its world premiere in the New Currents section at the 15th Busan International Film Festival   and screened in the Panorama section at the 61st Berlin International Film Festival.  

==Plot==
Arts professor Jung Ji-woo (Kim Sang-hyun) is searching for a nude model for a video clip that will be played at her exhibition. When Hee-jin (Seo Hyun-jin), one of her students, recommends Yoon Ji-woo (Kim Hyo-jin) for the job, the three women head to the beach to shoot the video. As they spend time together, Yoon Ji-woo begins to share pieces of her past relationship with Kang Ji-woo (Kim Kkot-bi). The film weaves the pasts and presents of the three Ji-woos and focuses on their intersecting relationship.  

==Cast==
*Kim Hyo-jin as Yoon Ji-woo
*Kim Kkot-bi as Kang Ji-woo
*Seo Hyun-jin as Hee-jin
*Kim Sang-hyun as Jung Ji-woo
*Choi Min-yong as Detective Min-yong
*Woo Seung-min
*Kim Sun-hyuk

==References==
 

==External links== Daum  
*    
*  
*  
*  

 
 
 
 
 
 
 
 


 
 