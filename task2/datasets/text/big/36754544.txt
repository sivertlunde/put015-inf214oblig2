The First Kiss (film)
{{Infobox film
| name           = The First Kiss
| image          = The_First_Kiss_1928_Poster.jpg
| border         = yes
| caption        = Lobby card
| director       = Rowland V. Lee
| producer       = {{Plainlist|
* Adolph Zukor
* Jesse Lasky
}}
| writer         = John Farrow
| based on       =  
| starring       = {{Plainlist|
* Fay Wray
* Gary Cooper
}}
| music          = 
| cinematography = Alfred Gilks
| editing        = Lee Helen 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes (6 reel#Motion picture terminology|reels)
| country        = United States
| language       = English intertitles
| budget         = 
| gross          = 
}}
The First Kiss is a 1928 American silent romantic-drama film directed by Rowland V. Lee and starring Fay Wray and Gary Cooper.    Based on the short story Four Brothers by Tristram Tupper, the film is about a Chesapeake Bay fisherman who turns to pirating in order to be rich enough to marry a society girl.
 lost film.    Some filming took place on the Chesapeake Bay in Maryland.

==Cast==
* Fay Wray as Anna Lee
* Gary Cooper as Mulligan Talbot
* Lane Chandler as William Talbot
* Leslie Fenton as Carol Talbot
* Paul Fix as Ezra Talbot
* Malcolm Williams as Pap
* Monroe Owsley as Other Suitor

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 