Mabel's Blunder
 
{{Infobox film
| name = Mabels Blunder
| image =
| caption =
| director = Mabel Normand
| producer =
| writer = Mabel Normand
| narrator = Charles Bennett Harry McCoy
| music =
| cinematography =
| editing =
| studio = Keystone Film Company
| distributor =
| released =  
| runtime = 13 minutes
| country = United States
| language = English
| budget =
|
}} silent comedy silent screen comediennes.

==Plot==
Mabels Blunder tells the tale of a young woman who is secretly engaged to the bosss son. 
The young mans sister comes to visit at their office, and a jealous Mabel, not knowing who the visiting woman is, dresses up as a (male) chauffeur to spy on them.

==Production background==
Produced at Mack Sennetts Keystone Studios, known at the time as "The Fun Factory", Mabels Blunder showcases Normands spontaneous and intuitive playfulness and her ability to be both romantically appealing and boisterously funny.

==National Film Registry==
This film, with its unusual gender-bending aspect, was added to the National Film Registry by the Library of Congress in December 2009 for being “culturally, historically or aesthetically” significant. 

==See also==
*A Florida Enchantment (1914) cross-dressing comedy directed by and written by Sidney Drew

==References==
 

==External links==
* 
* 
* §

 
 
 
 
 
 
 
 
 
 
 


 