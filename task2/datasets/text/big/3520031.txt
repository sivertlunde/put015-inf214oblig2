The Milagro Beanfield War
{{Infobox film
| name           = The Milagro Beanfield War
| image          = Milagro_Beanfield_War_Poster.jpg
| caption        = Theatrical release poster
| alt            =
| director       = Robert Redford
| producer       = Moctesuma Esparza Gary Hendler Charles Mulvehill Robert Redford John Nichols David S. Ward Richard Bradford Sônia Braga
| music          = Dave Grusin
| cinematography = Robbie Greenberg Jim Miller
| distributor    = Universal Pictures
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = $22 million 
| gross          = $13,825,794
}} John Nichols John Heard, Daniel Stern, Chick Vennera, James Gammon and Christopher Walken. 

Filmed on location in Truchas, New Mexico, the film is set in the fictional rural town of Milagro, with a population of 426, a predominantly Hispanic and Catholic town, with a largely interrelated population.

The film tells of one mans struggle as he defends his small beanfield and his community against much larger business and state political interests.

==Plot==
Nearly 500 residents of the agricultural community of Milagro in the mountains of northern New Mexico face a crisis when politicians and business interests make a backroom deal to usurp the towns water in order to pave the way for a land buy-out. Due to the new laws, Joe Mondragon is unable to make a living farming because he is not allowed to divert water from an irrigation ditch that runs past his property.

Frustrated, and unable to find work, Joe visits his fathers field. He happens upon a tag that reads "prohibited" covering a valve on the irrigation ditch. He kicks the valve, unintentionally breaking it, allowing water to flood his fields. He decides against repairing the valve and instead decides to plant beans in the field. This leads to a confrontation with powerful state interests, including a hired gun brought in from out of town.

An escalation of events follows, leading to a final showdown between law enforcement and the citizens of Milagro.

==Cast==
* Rubén Blades as Sheriff Bernie Montoya Richard Bradford as Ladd Devine
* Sônia Braga as Ruby Archuleta
* Julie Carmen as Nancy Mondragon
* James Gammon as Horsethief Shorty Wilson
* Melanie Griffith as Flossie Devine John Heard as Charlie Bloom
* Carlos Riquelme as Marshal Amarante Cordova Daniel Stern as Herbie Platt
* Chick Vennera as Joe Mondragon
* Christopher Walken as Kyril Montana
* Freddy Fender as Mayor Sammy Cantú
* Tony Genaro as Nick Rael
* Jerry Hardin as Emerson Capps
* Ronald G. Joseph as Jerry Gomez
* Mario Arrambide as Carl
* Robert Carricart as Coyote Angel

==Production==
According to an article by Patricia Rodriguez in the Fort Worth Star Telegram, Robert Redford was interested in filming part of the movie in the Plaza del Cerro of Chimayo, New Mexico, which is argued to be the last surviving fortified Spanish plaza in North America. Some locals responded favorably but many objected to the idea of big business changing the small community which forced Redford to film the movie in Truchas, New Mexico.
 John Nichols in his essay, "Night of the Living Beanfield: How an Unsuccessful Cult Novel Became an Unsuccessful Cult Film in Only Fourteen Years, Eleven Nervous Breakdowns, and $20 Million," gives an account of the film project as he saw it.  
Nichols also describes the origin of the novel and the making of the film in the biographical documentary, "The Milagro Man: The Irrepressible Multicultural life and Literary Times of John Nichols," which premiered at the 2012 Albuquerque Film Festival. 

==Reception==
 

===Box office===
The films premiere in three cites was called "risky" and "disappointing" by industry analysts. 

===Critical response===
 , Robert Redford and Sônia Braga promoting the film at the 1988 Cannes Film Festival.]]
The film received mixed reviews from critics. Vincent Canby, film critic for The New York Times, believes the film missed its mark, and wrote, "The screenplay, by David Ward and John Nichols, based on Mr. Nicholss novel, is jammed with underdeveloped, would-be colorful characters, including a philosophical Chicano angel, who face a succession of fearful confrontations with the law that come to nothing. The narrative is a veritable fiesta of anticlimaxes, from the time the sun sets at the beginning of the film until it sets, yet again, behind the closing credits." 

Roger Ebert also gave the film a mixed review and had problems with the films context, writing, "The result is a wonderful fable, but the problem is, some of the people in the story know its a fable and others do not. This causes an uncertainty that runs all through the film, making it hard to weigh some scenes against others. There are characters who seem to belong in an angry documentary - like Devine, who wants to turn Milagro into a plush New Mexico resort town. And then there are characters who seem to come from a more fanciful time, like Mondragon, whose original rebellion is more impulsive than studied." 

Critic Richard Scheib liked the films direction and the characters portrayed. He wrote, "Redford arrays a colorfully earthy ensemble of characters. The plot falls into place with lazy, deceptive ease. Redford places it up against a gently barbed level of social commentary, although this is something that comes surprisingly light-heartedly. There’s an enchantment to the film – at times it is a more successful version of the folklore fable that Francis Ford Coppolas Finians Rainbow (film)|Finians Rainbow (1968) tried to be but failed."

Scheib believes the film is "one of the first American films to fall into the Latin American tradition of magical realism. This is a genre that usually involves an earthily naturalistic, often highly romanticized, blend of the supernatural and whimsical." 

The review aggregator Rotten Tomatoes reported that 59% of critics gave the film a positive rating, based on twenty-seven reviews. 

The film was screened out of competition at the 1988 Cannes Film Festival.   

===Accolades===
;Wins
* Academy Awards: Oscar; Best Music, Original Score, Dave Grusin; 1989.
* Political Film Society: PFS Award; Democracy; 1989.

;Nominations
* Golden Globes: Golden Globe; Best Original Score - Motion Picture, Dave Grusin; 1989.
* Political Film Society: PFS Award; Exposé; 1989.

==Soundtrack==
Veteran jazz pianist and composer Dave Grusin contributed the films original music. A formal soundtrack album has never been released, although tracks from the score were included as a bonus suite on Grusins 1989 album, "Migration."

==References==
 

==External links==
 
*  
*  
*  
*   film trailer at YouTube

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 