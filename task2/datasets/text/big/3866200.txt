The Guyver
{{Infobox Film
  | image = The Guyver poster.jpg
  | caption = Film poster
  | name = The Guyver 
  | director = {{plainlist|
* Screaming Mad George
* Steve Wang
}}
  | producer = Brian Yuzna
  | screenplay = Joe Woo Jr.
  | based on =   
  | starring =  {{plainlist|
* Mark Hamill
* Vivian Wu
* Jack Armstrong
* Jimmie Walker David Gale
}}
  | music = Matthew Morse
  | cinematography =Levie Isaacks
  | editing = {{plainlist|
* Andy Horvitch
* Joe Woo Jr.
}}
  | released =   
  | distributor = New Line Cinema 
  | runtime = 88 minutes 
  | country = United States
  | language = English
  | budget = $3 million   
}}
The Guyver (released in Europe as Mutronics) is a 1991 American  .

==Plot== CIA Agent Max Reed witnesses the murder of Dr. Tetsu Segawa, a researcher for the mysterious Chronos Corporation. Dr. Segawa had stolen an alien device known as “the Guyver” from Chronos, but he hid it among a pile of garbage by the Los Angeles River before his death. Lisker, leader of the thugs that murdered Dr. Segawa, returns the metal briefcase to Chronos president Fulton Balcus, only to discover that it contains an old toaster. At a dojo, Reed notifies Dr. Segawas daughter Mizki of the incident while her boyfriend Sean Barker struggles to pay attention in class. Sean follows Reed and Mizki to the crime scene; there, he stumbles upon the Guyver unit stored inside a lunch box and stuffs it in his backpack. On his way home, his scooter breaks down in the middle of a back alley before a gang corners him. While Sean is being attacked by the gang, the Guyver suddenly activates and fuses with him. Sean, in his newly armored form, dispatches the gang members, but is shocked by his physical appearance before the armor quickly disappears into two scars on the back of his neck.

The next night, Sean goes to Mizkis apartment and discovers his sensei murdered and Mizky abducted by Liskers thugs. With the help of Reed, Sean rescues Mizki before the trio are chased by Liskers gang of Zoanoids. They are trapped in an abandoned warehouse, where Liskers thugs hold Mizki captive and Sean once again transforms into the Guyver to battle them. Sean defeats the Zoanoids before squaring off against Lisker. During the fight, Sean executes a headbutt, which temporarily malfunctions the armors Control Metal. He kills Liskers girlfriend Weber, but mistakenly knocks Mizki unconscious before the Zoanoids gang up on him and Lisker rips the Control Metal off his forehead, disintegrating the armor and seemingly killing Sean.

Mizki wakes up at Chronos headquarters, where Balcus shows her a gallery of Zoanoids before questioning her on how Sean was able to activate the Guyver. Dr. East, the head of genetics research, discovers that the Control Metal is regenerating itself into a new Guyver unit. After seeing Reed being experimented on, Mizki assaults Balcus and takes the Control Metal, threatening to throw it into the disposal chamber. In the middle of the ruckus, the Control Metal is flung off her hand and accidentally swallowed by Dr. East before it bursts through the Zoanoids body and once again becomes the Guyver. Sean and Mizki free Reed from the experimental chamber before Sean once again battles Lisker and kills him. Before the trio proceed to escape, Reed suddenly mutates into a Zoanoid and dies due to his system rejecting the new form. Balcus reveals his true form as the Zoalord and corners Sean, but the Guyvers defensive system activates the Mega Smasher cannons on his chest and obliterates Balcus and the laboratory. Sean deactivates the Guyver armor before he and Mizki leave Chronos headquarters as Reeds former partner Col. Castle and the Zoanoid thug Striker look on.

==Cast==
* Jack Armstrong as Sean Barker/The Guyver
* Mark Hamill as Max Reed
* Vivian Wu as Mizki Segawa David Gale as Fulton Balcus
* Michael Berryman as Lisker
* Jimmie Walker as Striker
* Peter Spellos as Ramsey
* Spice Williams-Crosby as Weber
* Willard E. Pugh as Col. Castle
* Jeffrey Combs as Dr. East
* David Wells as Dr. Gordon
* Linnea Quigley as Scream Queen
* Greg Paik as Dr. Tetsu Segawa

==Release==
The American version of the film, distributed by New Line, cut several scenes to focus more on action than humor.  Producer Yuzna expressed confusion at some of the choices. 

==Reception==
Glenn Kenny of Entertainment Weekly said the film features “surprisingly convincing costumes and effects, inspired casting, and energetic direction,   what sinks it is its unfortunate adherence to the time-honored direct-to-video clichés: an unearned paycheck for a onetime A-picture star, and a tendency to fall back on lame humor whenever the going gets slow.” 

David Johnson of DVD Verdict criticized the films "ham-fisted over-acting", "ludicrous plot contrivances", and "nauseatingly hokey soundtrack."  , David Johnson, DVD Verdict, August 25th, 2004  Johnson called the film "a big, dumb joke" and said: "Despite some good creature effects, the movie crashed and burned and crashed again, weighted down by preposterous acting   corny music." 

Nathan Shumate of Cold Fusion Video Reviews criticized the film, in particular “the annoying demeanor and lack of personality” of lead actor Jack Armstrong, adding: "If there ever was a movie made for fan appreciation only, this is it,   but not everything can be blamed on audience unfamiliarity; there are plenty of elements in this movie that don’t work even by fanboy standards.” 

The film generated enough interest for a sequel,  .  Armstrong was replaced by David Hayter in the role of Sean. The film was more well-received critically than its predecessor. The original film poster is commonly used as an example of deceptive advertizing;  the shot is composited to imply Hamill is the Guyver, rather than having only a minor part.

==References==
 

==External links==
*  
*  
*  
*  at Superheroes Lives

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 