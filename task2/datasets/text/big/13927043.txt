Breaking the Habit (DVD)
{{multiple issues|
 
 
}}

{{Infobox album  
| Name        = Breaking The Habit
| Type        = video
| Artist      = Linkin Park
| Cover       = Breaking the Habit (DVD).jpg
| Released    = July 27, 2004
| Recorded    = At Various location
| Genre       = Nu metal, alternative metal
| Length      = 20:34 
| Label       = Locomotive Music
| Director    = Mark Fiore
| Producer    = Joe Hahn, Eric Calderon, Matt Caltabiano
| Reviews     = 
| Chronology  =  Live in Texas (2003)
| This album  = Breaking The Habit (2004) Collision Course (2004)
}}
Breaking the Habit (DVD) is the fourth DVD by Linkin Park, released on July 27, 2004 through Locomotive Music.  

The release documented the band while making the video of the single " .

== Chapter listing ==
{{tracklist title1   = Intro note1    = By Joe Hahn length1  = 1:00 title2   = Recording the Band length2  = 6:10 title3   = Animation at Japan length3  = 6:20 title4   = Joe roaming in Japan length4  = 1:02 title5   = Communication Process length5  = 1:18 title6   = The influence of the song on the fans. note6    = With a few live footages of the band and fans singing the song. length6  = 0:49 title7   = End Credits length7  = 1:02 title8   = Alternate Video "Breaking the Habit (5.28.04 3:37 PM)" length8  = 3:20  
}}

== Personnel ==

=== Linkin Park ===
* Chester Bennington – lead vocals
* Rob Bourdon – drums, backing vocals
* Brad Delson – guitars, backing vocals
* Joe Hahn – DJ, sampling, backing vocals Dave "Phoenix" Farrell – bass, backing vocals
* Mike Shinoda – MC, vocals, beats, sampling, guitar

=== Production ===
* Produced by: Eric Calderon, Mr. Hahn and Matt Caltabiano 
* Executive producer: Mr. Hahn
* Edited by: Mario Mares
* Assistant Editor: Carlos Diaz
* Associate Producer: Misty Martin
* DVD-video producers: Mr. Hahn
* Technical director: Eric Calderon
* Online Artist: Michael Forrest
* Mixer: Jim Corbett
* Interviewed and Filmed by: Mark Fiore
* Photography for package: Mark Fiore
* Art direction and design: Mike Shinoda
* Additional Footage: Jeremy Kline

== References ==
 

 

 
 
 
 
 
 
 


 