Ek Shrimaan Ek Shrimati
{{Infobox film
| name           = Ek Shrimaan Ek Shrimati
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Bhappi Sonie
| producer       = Surinder Kapoor	
| writer         = Sachin Bhowmick
| screenplay     = Sachin Bhowmick
| story          = 
| based on       =  
| narrator       = 
| starring       = Shashi Kapoor Babita Kapoor
| music          = Kalyanji Anandji
| cinematography = Taru Dutt
| editing        = M. S. Shinde	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 140 min.
| country        = India Hindi
| budget         = 
| gross          = 
}}
 Indian Bollywood film directed by Bhappi Sonie and produced by Surinder Kapoor. It stars Shashi Kapoor and Babita Kapoor in pivotal roles.

==Plot==
Ek Shriman Ek Shrimati is a story about Pritam (Shashi Kapoor) and Deepali Babita Kapoor|(Babita Kapoor). Pritam falls for Deepali who has already chosen Ajit (Prem Chopra) as her life partner. After having a few comical encounters with Deepalis uncle (Om Prakash), Pritam wins the favour of her uncle. Soon with the help of her uncle, Pritam wins the love of Deepali.  
==Cast==
* Shashi Kapoor...Pritam
* Babita Kapoor...Deepali Lakhanpal
* Rajendra Nath...Ram Bharose Agnihotri
* Prem Chopra...Ajit Choudhary
* Kamini Kaushal...Rama
* Helen (actress)|Helen...Sherry

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaye Baithe Khaye Piye Khiske"
| Mohammed Rafi
|-
| 2
| "Chandni Raat Mein"
| Mohammad Rafi
|-
| 3
| "Hello Hello Everybody"
| Asha Bhosle
|-
| 4
| "Jeevan Path Par Pyar Ne Chhedi"
| Mohammed Rafi, Lata Mangeshkar
|-
| 5
| "Kuchh Kahen To Khafa"
| Mohammed Rafi
|-
| 6
| "Pyar To Ek Din Hona Tha"
| Mohammed Rafi, Asha Bhosle
|-
| 7
| "Raja Tohe Nainon Se Madira"
| Asha Bhosle
|}

==External links==
* 

 
 
 
 

 