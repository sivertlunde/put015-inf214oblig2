Aval Kanda Lokam
{{Infobox film
| name           = Aval Kanda Lokam
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = KP Kottarakkara
| writer         = KP Kottarakkara
| screenplay     = Ravikumar Seema Seema
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Jayadevi Movies
| distributor    = Jayadevi Movies
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Ravikumar and Seema in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jayan
*Jose Prakash Ravikumar
*Seema Seema

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Edavappaathi Kaattadichaal || P Jayachandran, Jency || Sreekumaran Thampi ||
|-
| 2 || Kalakalam Padumee || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Manmadhaninnen || Vani Jairam || Sreekumaran Thampi ||
|-
| 4 || Orikkalorikkal || Vani Jairam || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*  

 
 
 


 