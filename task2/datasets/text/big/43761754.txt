Swearnet: The Movie
{{Infobox film
| name           = Swearnet: The Movie 250px
| caption        = Theatrical poster
| director       = Warren P. Sonoda Mike Smith Gary Howsam Bill Marks
| writer         = John Paul Tremblay Robb Wells Mike Smith
| based on       =  Pat Roach Mishael Morgan Sarah Jurgens Shannon Leroux Dana Woods Howard Jerome with Tom Green and Carrot Top 
| music          = Blain Morris
| cinematography = Bobby Shore
| editing        = Christopher Cooper
| studio         = Swearnet Pictures Rollercoaster Entertainment Vortex Words
| distributor    = Entertainment One
| released       =  
| runtime        = 112 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} comedy film Mike Smith, John Paul Tremblay and Robb Wells, stars of the Canadian television series Trailer Park Boys. In the film, Smith, Tremblay and Wells appear as fictionalized versions of themselves, who embark on creating a fully uncensored internet network.

The Guinness World Records website lists the film as holding the record for the most expletives in a film, with a total of 935 of them.

==Plot==
Fed up with being censored in their post-Trailer Park Boys lives, the out of work stars/world-renowned swearists, Mike Smith, Robb Wells and John Paul Tremblay decide to start their own uncensored network on the internet.

==Cast== Mike Smith as Mike Smith
*John Paul Tremblay as John Paul Tremblay
*Robb Wells as Robb Wells Pat Roach as Pat Roach/Swearman
*Mishael Morgan as Jamie
*Sarah Jurgens as Julie
*Shannon Leroux as Rachel
*Dana Woods as Jogi
*Howard Jerome as Trigger
*Tom Green as Tom Green
*Carrot Top as Carrot Top
*Leigh MacInnis as Leigh
*Sebastian Bach as Sebastian Bach
*John Dunsworth as John Dunsworth

Wells, Tremblay and Smith briefly reprise their roles as Ricky, Julian and Bubbles respectively in the mid-credits scene.

==Production==
The film was shot between August 20, 2012 and September 12, 2012 in Sault Ste. Marie, Ontario and Halifax, Nova Scotia, Canada using the Arri Alexa digital camera.

== Reception ==

The film has been met with a mostly-negative critical response. The film currently holds just a 20% "Rotten" approval rating on Rotten Tomatoes, with an average critical rating of 4.6/10, though a critical consensus has not yet been reached. On nearly 200 audience reviews, however, 85% of audience members reported liking the film, with an average audience score of 4.3/5. 

Writing for the  , Justin Robar gave the film a 75% score, citing his appreciation for the style of Wells, Smith & Tremblay: "Most people will look at "SwearNet" and consider it brainless. I do not. I accepted long ago the style of this trio, and at the same time, I came to understand the heart and substance behind the exterior. And if you watch the movie with that eye, youll see that same heart and same substance that makes the people who like "Trailer Park Boys", love "Trailer Park Boys"." Robar further praised the film as "...uproariously funny." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 