Coolie Killer
{{Infobox film
| name           = Coolie Killer
| image          = CoolieKiller.jpg
| alt            = 
| caption        = Film poster
| film name      = {{Film name
| traditional    = 殺出西營盤
| simplified     = 杀出西营盘
| pinyin         = Shā Chū Xī Yíng Pán
| jyutping       = Saat3 Ceot1 Sai1 Jing4 Pun4 }}
| director       = Terry Tong
| producer       = Dennis Yu Jeffrey Lau
| writer         = 
| screenplay     = Eddie Fong Chun Tin-nam Chiu Kang-chien
| story          = 
| based on       = 
| starring       = Charlie Chin Elliot Ngok Lisa Chiao Chiao Cecilia Yip
| narrator       =  David Wu
| cinematography = David Chung Brian Lai
| editing        = David Wu
| studio         = Century Motion Picture & Distribution
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$5,532,180
}}
 Hong Kong action film directed by Terry Tong and starring Charlie Chin, Elliot Ngok, Lisa Chiao Chiao and Cecilia Yip.

==Plot==
Ko Tat-fu (Charlie Chin), leader of a coolie killer group in Sai Ying Pun who specializes in assassinations in countries, is now the boss of a shipping company in Central, Hong Kong|Central. However, one time, after an ambushed by a group of killers which left four of his underlings dead, he re-assumed his old profession. Ko believes the ambush was related to his refusal of a recent contract to kill someone in Hong Kong. He asks his lover, Hung-kam (Lisa Chiao Chiao) to investigate in to this, but she commits suicide. Ko was determined to battle for the truth. During a rainy night, when Ko takes out his foes, he ends his legendary and romantic life.

==Cast==
*Charlie Chin as Ko Tat-fu
*Elliot Ngok as Inspector Chung
*Lisa Chiao Chiao as Cheung Hung-kam
*Cecilia Yip as Tong Ho-yee
*Michael Tong as Siu
*Poon Chun-wai as Mak
*Newton Lai as Wor Danny Lee as Chungs boss (cameo)
*Kwan Hoi-san as Choi (cameo)
*Lau Siu-ming as Ding
*Chan Shen
*Lau Hok-nin as Man
*Hui Ying-sau as Old doctor
*Alex Ng as gets stuffed in garbage can
*Chan Chik-wai
*Cheung Hei
*Steve Mak
*To wai-wo
*Wong Ha-fei
*Wong Kung-miu
*Chow Kin-ping
*Fung Ming
*Chan Ling-wai
*Tony Tam
*Ng Kit-keung
*Ronnie

==Reception==
===Critical===
Kenneth Brorsson of   gave the film a positive review and writes "Great, more theme- and character based flicks would follow but Coolie Killer offers up a glimpse of why a story of nasty men fighting for own selfish revenge and not silly illusions of love is so pleasurable." 

===Box office===
The film grossed HK$5,532,180 at the Hong Kong box office during its theatrical run from 3 June to 14 July 1982 in Hong Kong.

==Award nominations==
*2nd Hong Kong Film Awards Best Film Best Cinematography (David Chung, Bryan Lai) David Wu)

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 
 
 
 