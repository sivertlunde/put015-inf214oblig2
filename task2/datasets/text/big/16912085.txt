Soft Cushions
 
{{Infobox film
| name           = Soft Cushions
| image          = 
| caption        = 
| director       = Edward F. Cline
| producer       = 
| writer         = Wade Boteler Frederick Chapin George Randolph Chester
| starring       = Douglas MacLean Sue Carol
| music          = 
| cinematography = Jack MacKenzie
| editing        = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = 
}}
 lost by Arne Andersens Lost Film Files website. 

==Cast==
* Douglas MacLean - The Young Thief
* Sue Carol - The Girl
* Richard Carle - The Slave Dealer
* Russ Powell - The Fat Thief (as Russell Powell)
* Frank Leigh - The Lean Thief
* Wade Boteler - The Police Judge
* Nigel De Brulier - The Notary
* Albert Prisco - The Wazir
* Boris Karloff - The Chief Conspirator
* Albert Gran - The Sultan
* Fred Kelsey - Policeman
* Harry L. Fraser - The Citizen (as Harry Jones)
* Noble Johnson - The Captain of the Guard

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 