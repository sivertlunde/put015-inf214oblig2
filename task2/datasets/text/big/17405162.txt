Thirst (2009 film)
 
{{Infobox film
| name           = Thirst
| image          = Thirstposter.jpg
| alt            = 
| caption        = Korean theatrical poster
| film name      = {{Film name
 | hangul         =  
 | rr             = Bakjwi
 | mr             = Pakchwi}}
| director       = Park Chan-wook
| producer       = Park Chan-wook Ahn Soo-hyun
| writer         = Park Chan-wook Jeong Seo-Kyeong
| based on       =  
| starring       = Song Kang-ho Kim Ok-bin Shin Ha-kyun
| music          = Jo Yeong-wook
| cinematography = Chung-hoon Chung
| editing        = Kim Sang-beom Kim Jae-beom Focus Features Internationals
| distributor    = CJ Entertainment   Focus Features  
| released       =  
| runtime        = 134 minutes   145 minutes  
| country        = South Korea
| language       = Korean English French
| budget         = 
| gross          = $13,008,937 
}} Jury Prize at the 2009 Cannes Film Festival.    It is the first mainstream Korean film to feature full-frontal male nudity. 
 
==Plot==
Sang-hyun is a Catholic priest who volunteers at the hospital, providing ministry to the patients. He is well respected for his unwavering faith and dedicated service, but he secretly suffers from overwhelming feelings of doubt and sadness. Sang-hyun volunteers to participate in an experiment to find a vaccine for the deadly Emmanuel Virus (EV). Although the experiment fails disastrously and Sang-hyun is infected with the seemingly fatal disease, he makes a complete and rapid recovery after receiving a blood transfusion.

News of his marvelous recovery quickly spreads among the devout parishioners of Sang-hyun’s congregation, and they begin to believe that he has a miraculous gift for healing. Soon, thousands more people flock to Sang-Hyun’s services. Among the new churchgoers are Kang-woo, Sang-hyun’s childhood friend, and his family. Kang-woo invites his old friend to join the weekly mahjong night at his house, and there, Sang-hyun finds himself attracted to Kang-woo’s wife, Tae-ju. Suddenly, Sang-hyun relapses into his illness; he coughs up blood and passes out. The next day, he wakes in dire need of shelter from the sunlight, having become a vampire.

At first, Sang-hyun feels a new-found vigor and is energized by his insistent bodily desires, but soon he is aghast to find himself drinking blood from a comatose patient in the hospital. After attempting to kill himself, Sang-hyun finds himself irresistibly drawn to the taste of human blood. To make matters worse, the symptoms of EV return and only seem to go away when he has drunk blood. Desperately trying to avoid committing a murder, Sang-hyun resorts to stealing blood transfusion packs from the hospital. Tae-ju, who lives with her ill husband and overprotective mother-in-law, Lady Ra, leads a dreary life. She is drawn to Sang-hyun and his odd new physicality, including his inability to resist his desires. The two begin an affair, but when Tae-ju discovers the truth about Sang-hyun’s new lifestyle, she retreats in fear. When Sang-hyun pleads with her to run away with him, she turns him down, suggesting that they kill her husband instead. 

When Sang-hyuns superior at the monastery requests some vampire blood so that his eyes may heal and he may see the world before dying, Sang-hyun flees his position at the monastery. He moves into Lady Ras house so that he may secretly be with Tae-ju. Sang-hyun notices bruises on Tae-ju and assumes her husband is the cause, a suspicion she sheepishly confirms. Sang-hyun decides to kill Kang-woo during a fishing trip with the couple. He pulls Kang-woo into the water and claims that he placed the body inside a cabinet in a house at the bottom of the lake, putting a rock on the body to keep it from floating to the surface.
 paralyzed state. In the meantime, Sang-hyun and Tae-ju are haunted by terrifying visions of Kang-woos drowned and bloated corpse suddenly appearing in their bed, in a cabinet in the basement, and between the two of them during sex. In one scene, Tae-ju is tormented by Kang-woo appearing directly above her and making a repeated stabbing motion with sewing scissors, each time stopping the sharp tips just short of her lips, until she cannot bear the vision anymore and hides under a bed sheet- only to have Kang-woo reappear under the sheets. When Tae-ju lets it slip that Kang-woo never abused her, Sang-hyun is enraged because he only killed Kang-woo to protect Tae-ju. Teary-eyed, Tae-ju asks Sang-hyun to kill her and let her return to her husband. He obliges by snapping her neck, but after feeding on her blood, he decides he does not want to be alone forever and feeds her corpse some of his own blood. She awakens as a vampire. Lady Ra, knocked to the floor by a seizure, witnesses everything.

Tae-ju quickly shows herself to be a remorseless monster, killing indiscriminately to feed, while Sang-hyun acts more conservatively, not killing unless he has to. Their conflicting ethics result in a chase across the rooftops and a short battle. Some time later, Lady Ra manages to communicate to Kang-woos friends, through blinking and scratching characters into the table with the one finger she is capable of moving, that Sang-hyun and Tae-ju killed her son. Tae-ju quickly disposes of two of the friends, and Sang-hyun appears to eliminate the third.

Realizing the gravity of the situation, Sang-hyun tells Tae-ju that they must flee or be caught and found guilty of multiple murders. Before leaving with her, he makes a visit to the camp of worshipers who consider him the miracle EV survivor. He makes it seem like he tried to rape a girl, leading the campers to chase him away with rocks and sticks, no longer idolizing him.

Sang-hyun then places Lady Ra in his car, and with Tae-ju, drives into the night. Back at the house, the third friend  (apparently not killed by Sang-hyun) gets up and escapes. Upon waking up from a nap in the car, Tae-ju realizes that Sang-hyun has driven to a desolate field with no cover from the imminent dawn. Sang-hyun snaps the key off in the ignition and throws it into the ocean. Realizing his plan to have them both burn when dawn breaks, Tae-ju tries to hide in the trunk, an attempt that is foiled when Sang-hyun rips the trunk lid off and throws it, also, into the ocean. She then attempts to hide under the car, but Sang-hyun pushes it off her. Resigning herself to her fate, she joins him on the hood of the car, and both are burnt to ash by the sun, all as Lady Ra watches from the back seat of the car.

==Cast==
* Song Kang-ho as Sang-hyun
* Kim Ok-bin as Tae-ju
* Shin Ha-kyun as Kang-woo
* Kim Hae-sook as Lady Ra, Tae-jus mother-in-law
* Eriq Ebouaney as Immanuel
* Hwang Woo-seul-hye as Girl with a whistle
* Mercedes Cabral as Evelyn
* Song Young-chang as Seung-dae 
* Oh Dal-su as Young-du 
* Ra Mi-ran as Nurse Yu

==Production== No Regret). 

==Reception==
  normalized rating average score of 73, based on 21 reviews. 

Prominent film critic Roger Ebert awarded Thirst three out of a possible four stars, citing that the director was "todays most successful director of horror films."  The website IGN awarded the film three and a half out of five stars and said "Thirst may not be the greatest vampire movie ever made, but Parks willingness to try something different makes it a decidedly fresh take on the genre." 

===Box office===
On 3 May, Thirst debuted at #1 at the South Korean Box office and grossed   the first day and   for that three-day weekend.   More than 2,223,429 tickets were sold nationwide becoming the 9th most attended film of 2009. 

===Awards and nominations===
;2009 Cannes Film Festival  Jury Prize
* Nomination - Palme dOr

;2009 Chunsa Film Art Awards
* Best Director - Park Chan-wook
* Best Actor - Song Kang-ho
* Best Supporting Actress - Kim Hae-sook
* Best Lighting - Park Hyun-won

;2009 Grand Bell Awards
* Nomination - Best Supporting Actress - Kim Hae-sook
 2009 Blue Dragon Film Awards
* Best Supporting Actress - Kim Hae-sook
* Best Music - Jo Yeong-wook
* Nomination - Best Film
* Nomination - Best Director - Park Chan-wook
* Nomination - Best Actor - Song Kang-ho
* Nomination - Best Actress - Kim Ok-bin
* Nomination - Best Supporting Actor - Shin Ha-kyun
* Nomination - Best Cinematography - Chung Chung-hoon
* Nomination - Best Art Direction - Ryu Seong-hee
* Nomination - Best Lighting - Park Hyun-won

;2009 Directors Cut Awards
* Best Director - Park Chan-wook
* Best Actor - Song Kang-ho
 2010 Asian Film Awards
* Best Visual Effects - Lee Seon-hyeong Best Actor - Song Kang-ho
* Nomination - Best Cinematography - Chung Chung-hoon
 2010 Baeksang Arts Awards
* Nomination - Best Film
* Nomination - Best Actress (Film) - Kim Ok-bin

==Home media== Universal Studios Home Entertainment released a region 1 DVD of Thirst on 17 November 2009.  No extras are included, but the film was produced in amamorphic widescreen with Korean DD5.1 Surround audio and subtitles in English, English SDH, French and Spanish. The directors cut, running 145 minutes, has been so far released in Korea only, on DVD and Blu-ray Disc. 

==See also==
* List of South Korean films of 2009 Nudity in film (East Asian cinema since 1929)
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*   at The Korea Society

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 