P.U.N.K.S.
{{multiple issues|
 
 
}}

{{Infobox film
| name           = P.U.N.K.S.
| image          = PUNKS.jpg
| image_size     =
| caption        = P.U.N.K.S. DVD Cover Sean McNamara
| producer       =  Bill Barnett Harel Goldstein Patrick Peach {{cite web|url=http://www.locatetv.com/movie/punks/814724|title=Punks-Movie (1999)|accessdate=18 December 2008|work=|publisher=LocateTV
|date=}}  Sean McNamara
| narrator       = Roger Clinton Henry Winkler Megan Blake
| music          =
| cinematography =
| editing        =
| distributor    = Ardustry Home Entertainment
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} 1999 movie about a group of bullied teens who find a suit created by a scientist. The suit provides whoever wears it superhuman strength, as well as leaving the wearer open to having their body controlled by someone else via wireless computer signals.  After learning that Drews father, who has a serious heart condition, is required to present the prototype to investors, and after soon discovering that the suit would cause Drews father to die of massive heart failure, the group goes on a mission to save his father and shut down the company responsible for building the incomplete and dangerous device.

==Plot==
A research and production company is performing a live human trial for a machine called the “Augmentor 1,000.”  The device is designed for medical use, with an ancillary benefit of enhanced strength.  The machine’s creator, workaholic Pat Utley, objects due to potentially lethal side effects.  Edward Crow, the company owner, overrules him.  The Augmentor is successfully activated but the subject suddenly goes berserk until Pat literally pulls the plug on the device.

Drew, Pat’s teenage son, meets up with friends Miles and Lanny after school.  All three are bullied by jocks; they later commiserate at Drew’s house and decide to form a club to defend themselves.  The next day, Drew and Miles hang out at Pat’s work, and hack into the computer systems for fun.  While Miles browses classified files, Drew overhears Crow demanding that Pat prepare for a motion control Augmentor demonstration.  To ensure his compliance, Pat is ordered to wear the machine for the test or be taken off the project.  Pat reluctantly agrees, despite a problematic heart condition.  Drew concludes they’re trying to get rid of his dad, so Miles tries to find out more about the machine, but they need Crow’s superuser password.

To get it, the three of them recruit Jonny, the school hustler, for their club.  Together, they trick a system supervisor out of the superuser password and download the Augmentor schematics.  Miles determines that an Augmentor user will fry their nervous system in 20 minutes, but Drew’s dad would be dead in only five.  They determine the best way to save Pat is to steal the machine.  To help break into the building, Jonny’s cousin Samantha, a skilled car boost and lockpick, is enlisted.  The team calls themselves the “P.U.N.K.S.,” an acronym formed by the first letters of their surnames.  During a night raid, they successfully infiltrate the building.  Miles copies and deletes the Augmentor files while Drew and Sam plant an audio bug and tap into a camera feed for surveillance from their “master control.”  A silent alarm attracts guards, and Miles crashes the system with a virus before the teens narrowly escape with the Augmentor.

The day after, they learn from their audio/video feed that Crow has file backups and an Augmentor prototype.  They attempt two public meets to obtain the prototype, but both fail.  Crow organizes an impromptu demonstration for an important investor with Middle Eastern buyer connections which Crow needs, as he has secretly put all of his company’s financial hopes on black market trade.  Frustrated with Pat, Crow decides to test out the prototype Augmentor with the remote control unit personally.  Jonny uses a second motion control suit to embarrass Crow by making him do ridiculous antics.  Furious, Crow threatens to fire Pat unless the real demo goes perfectly.

The victory celebration is cut short when Crow is overheard talking about eliminating them, and mentioning a troublesome FBI agent named “Houlihan.”  Everyone panics and bails except Drew.  He meets Houlihan, falling into a trap set by Crow.  The other P.U.N.K.S. save him at the last minute by radio, and he escapes.  Pat tries to resign, but is drugged and forcibly suited up for the buyer presentation.  Drew returns to “master control,” only to find Crow kidnapped his friends.  He rides in during the demo and rescues his friends and saves his dad.  Crow puts on the Augmentor and fights Drew.  The boy has the upper hand, but runs past his twenty minute limit and starts losing consciousness.  As Crow prepares to pummel him, Sam intervenes in the motion control suit and has Crow beat himself up.  The police arrive and arrest the criminals.

Back at school, the jocks pick on Miles, but the P.U.N.K.S. unite, and Lanny literally tosses them across the courtyard by using the Augmentor.  On their way home together, Miles reads an article detailing the exposure of Crow’s operation, the arrests, and crediting the “covert team of operatives,” the P.U.N.K.S., for bringing them to justice.

== Cast ==
* Tim Redwine as Drew Utley
* Jessica Alba as Samantha Swoboda
* Brandon Baker as Jonny Pasiotopolis
* Kenneth A. Brown as Miles Kitchen
* Patrick Renna as Lanny Nygren
* Randy Quaid as Pat Utley
* Cathy Moriarty as Mrs. Utley
* Louan Gideon as Mrs. Grimes Roger Clinton as Carlson
* Henry Winkler as Edward Crow
* Megan Blake as FBI Agent Houlihan
* Kim Morgan Greene as Woman in bar
* Gregory Mortensen as Bartender

==References==
 

==External links==
*  

 

 
 
 
 
 