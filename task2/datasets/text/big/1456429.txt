Main Hoon Na
 
 
{{Infobox film
| name           = Main Hoon Na
| image          = poster_mainhoonna.jpg
| caption        = Theatrical release poster
| director       = Farah Khan
| producer       = Shahrukh Khan   Gauri Khan   Ratan Jain
| story          = Farah Khan
| screenplay     = Farah Khan Abbas Tyrewala Rajesh Saathi
| writer         = Abbas Tyrewala (dialogue)
| starring       = Shah Rukh Khan Sunil Shetty Sushmita Sen Zayed Khan Amrita Rao
 Boman Irani Satish Shah
 Bindu Kirron Kher Rakhi Sawant

music          = Anu Malik Ranjit Barot (Background score)
| cinematography = V. Manikandan
| editing        = Shirish Kunder
| distributor    = Red Chillies Entertainment   Eros International
| released       = 30 April 2004
| runtime        = 173 mins
| country        = India
| language       = Hindi
| budget         =   
| gross          =     
}}

Main Hoon Na  is a 2004 Indian action comedy thriller film co-written and directed by Farah Khan. The screenplay was written by Farah Khan and Abbas Tyrewala based on the story Anvita Dutt Guptan. It stars Shah Rukh Khan, Sunil Shetty, Sushmita Sen, Zayed Khan and Amrita Rao in the main cast. It was remade into Tamil as Aegan starring Ajith Kumar, Nayantara, Pia Bajpai. 
 Major Ram Prasad Sharma (Shah Rukh Khan) who becomes embroiled in the events to ensure that "Project Milap" - the releasing of civilian captives on either side of the borders of India and Pakistan - can take place as a sign of trust and peace between the two nations.  Main Hoon Na is one of the most successful Indian films discussing the Indo-Pakistani conflict from a neutral point of view. The film was shot at St. Pauls school, Darjeeling.

It is the first film of Shah Rukh Khans production company Red Chillies Entertainment and choreographer Farah Khans directorial debut film. It was released on 30 April 2004 and was declared a hit in India by Box Office India.

==Plot==
The movie is about the relationship between India and Pakistan. The governments of India and Pakistan have launched, "Project Milaap", where both the governments will release the POWs from the previous wars. Raghavan, a deranged ex-army man-turned-militant does not want the peaceful operation to turn to reality and makes an attempt on Gen. Amarjeet Bakshis life. While trying to rescue Gen. Bakshi, Brigadier Shekhar Sharma falls prey to a stray bullet. While on deathbed he confesses to his son Major Ram Prasad Sharma that his wife and son had left him many years ago due to his infidelity. He tells Ram his last wish: his family be reunited under one roof, and dies. Meanwhile, Gen. Bakshi tells Ram that his teenage daughter Sanjanas life is in danger, and asks him to go undercover to Darjeeling and pose as a student in Sanjanas class in front of her own eyes. As the fate would have it, Ram learns that his younger brother Laxman "Lucky" is studying at the same college. So Maj. Ram starts off on his dual mission: personal and national. While at the college he falls in love the new chemistry teacher but, keeps making a fool of himself when singing. He meets Sanju, who has a very hard  time impressing Lucky since she has a crush on him. Since one of the aims of the mission is to provide protection to Sanju, he tries getting close to her but she has only one thing on her mind, impressing Lucky. Sanju is constantly being irritated by Ram because of his attempts at friendship and she eventually complains to Lucky about him. Since he is the coolest guy on campus and Ram is the uncle type they dont get along at all. Lucky, being the mischievous guy he is, ends up in a competition of getting to the roof of the school and in a sudden turn of events ends up hanging from the roof. Ram saves him. This causes all barriers to collapse between them and they become best of friends. Automatically, Sanju also gets close to Ram. Soon thereafter, Ms. Sharma, Luckys mother, advertises for a paying guest in their house in order to make ends meet and thus Maj. Ram ends up in their house. Sanju soon starts wearing new kinds of clothes after visiting Ms. Chandini the chemistry teacher, along with Ram.Soon, when Ram expresses his feelings to Ms. Chandini, she tells him she feels the same way but they cant be together because she was older than him. In addition, Lucky falls in love with Sanju because of her new look. Lucky and, Ms.Sharma find out that Ram is actually Shekars son and kicks him out of their house when he is about explain. Ram discovers Raghavan has kidnapped Sanjana.Raghavan and Ram have a deadly fight. But, surprisingly Ram wins the fight even though he is wounded by a bullet shot inflicted by Raghavan. Later it shows everybody at college having a party. There it is revealed that it was Rams graduation party. It shows Lucky and Sanju expressing their feelings to each other. At the end it shows Ram and Lucky dancing while everybody is watching.

==Cast==
*Shah Rukh Khan as Major Ram Prasad Sharma
*Sunil Shetty as ex-Major Raghavan Dutta 
*Sushmita Sen as Chandni (chemistry teacher)
*Zayed Khan as Lakshman Prasad Sharma/Lucky
*Amrita Rao as Sanjana Bakshi/Sanju
*Kabir Bedi as General Amarjeeth Bakshi
*Naseeruddin Shah as Brigadier Shekhar Sharma
*Kirron Kher as Mrs. Madhu Sharma
*Murli Sharma as Khan Bindu as Mrs. Kakkad
*Boman Irani as the principal
*Satish Shah as professor Rasai
*Rakhi Sawant as Mini
*Praveen Sirohi as Vivek
*Rajeev Punjabi as Percy Tabu as The girl who was watching Shahrukh dancing (Special Appearance)

== Release ==
Jamie Russell wrote in his review in the BBC, "A bonkers masala movie, Main Hoon Na could be the mutant offspring of Grease (film)|Grease and The Matrix. Part thriller, part high school comedy and all Bollywood musical, its the directorial debut of Farah Khan." 

===Box office===
Main Hoon Na was the second highest grossing Indian movie of 2004 behind Veer-Zaara (also a Shahrukh Khan starrer).  It made   in India and an additional   in the overseas market. Box Office India declared it a hit in India and a blockbuster overseas.  

==Character map of remakes==
{| class="wikitable"
|- Tamil !! Main Hoon Naa (2004 film)Hindi 
|-
| Ajith Kumar || Shah Rukh Khan
|-
| Nayantara || Sushmita Sen
|- Suman || Sunil Shetty
|-
|}

== Satellite Rights ==
Currently Star India owns the rights to Main Hoon Na which is televised on Star Gold many times throughout the year

==Music==
{{Infobox album |  
 Name = Main Hoon Na
| Type = Soundtrack
| Artist = Anu Malik
| Cover  = Main Hoon Na (Anu Malik album - cover art).jpg|  Feature film soundtrack
| Length = 40:25
| Recorded    = YRF Studios  (Mumbai) 
| Label = T-Series
| Producer = Anu Malik Hindi   English Murder     (2004)
| This album = Main Hoon Na   (2004)
| Next album = Fida    (2004)
}}
The music was composed by Anu Malik and for this, he won his third Filmfare award. The lyrics were provided by Javed Akhtar.

{| border="3" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration !! Notes
|-
|"Main Hoon Na" Sonu Nigam & Shreya Ghoshal
|06:03 Pictured on Shahrukh Khan, Zayed Khan & Amrita Rao; Sonu Nigam received several award nominations and an MTV Immies Best Male Singer Award
|-
|"Tumse Milke" Sonu Nigam, Aftab Sabri & Hasim Sabri
|06:02 Pictured on Shahrukh Khan, Sushmita Sen, Zayed Khan & Amrita Rao; Sonu Nigam received several award nominations
|-
|"Tumhe Jo Maine Dekha" Abhijeet Bhattacharya & Shreya Ghoshal
|05:44 Pictured on Shahrukh Khan & Sushmita Sen
|-
|"Gori Gori" Krishnakumar Kunnath|KK, Anu Malik, Sunidhi Chauhan & Shreya Ghoshal
|04:31 Pictured on Bindu
|-
|"Chale Jaise Hawayein" KK & Vasundhara Das
|05:26 Pictured on Zayed Khan & Amrita Rao
|-
|"Main Hoon Na (Sad Version)" Abhijeet Bhattacharya
|04:18
| Pictured on whole cast without Zayed Khan & Kirron Kher
|-
|"Yeh Fizayein" KK & Alka Yagnik
|05:19
| Pictured on whole cast and crew during end credits
|-
|"Main Hoon Na" (Remix) Ranjit Barot
|02:31
| Not in film
|}

==Awards==
Main Hoon Na received many awards and nominations. The awards it won are highlighted in bold: 

===Filmfare Awards===
* Filmfare Award for Best Music Director – Anu Malik.
* Filmfare Award for Best Actor – Shahrukh Khan.
* Filmfare Award for Best Supporting Actor – Zayed Khan.
* Filmfare Award for Best Supporting Actress – Amrita Rao.
* Filmfare Award for Best Performance in a Negative Role – Suniel Shetty.
* Filmfare Award for Best Performance in a Comic Role – Boman Irani.
* Filmfare Award for Best Male Playback Singer – Sonu Nigam.
* Filmfare Award for Best Lyricist – Javed Akhtar.

===Zee Cine Award=== Zee Cine Award for Best Debuting Director - Farah Khan
* Zee Cine Award for Best Music Director – Anu Malik
* Zee Cine Award for Best Director – Farah Khan
* Zee Cine Award for Best Actor – Male – Shahrukh Khan
* Zee Cine Award for Best Film
* Zee Cine Award for Best Song of the Year - "Tumse Milke" Red Chillies
* Zee Cine Award for Best Playback Singer Male - Sonu Nigam "Main Hoon Na"
* Zee Cine Award for Best Art Direction - Sabu Cyril
* Zee Cine Award for Best Costume Design - Karan Johar, Manish Malhotra
* Zee Cine Award for Best Background Score - Ranjit Barot

===Star Screen Award ===
* Star Screen Award for Best Music  – Anu Malik.
* Star Screen Award for Best Male Playback Singer – Sonu Nigam.
* Star Screen Award for Best Actor in a Supporting Role – Zayed Khan.
* Star Screen Award for Best Action  – Allan Amin.
* Star Screen Award for Best Background Music  – Ranjit Barot.
* Star Screen Award for Best Choreography  – Farah Khan.
* Star Screen Award for Best Lyrics – Javed Akhtar.
* Star Screen Award for Publicity Design – Rahul Nanda & Himanshu Nanda.
* Star Screen Award for Best Sound Recording  – Shirish Kunder & Rakesh Ranjan.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 