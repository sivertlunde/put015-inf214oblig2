Manassu
{{Infobox film 
| name           = Manassu
| image          =
| caption        =
| director       = Hameed Kakkassery
| producer       = HH Abdulla Settu
| writer         = Hameed Kakkassery Jagathy NK Achari (dialogues)
| screenplay     = Jagathy NK Achari
| starring       = Prem Nazir Jayabharathi Adoor Bhasi Sankaradi  Rajasree
| music          = MS Baburaj
| cinematography = K Ramachandrababu
| editing        = TR Sreenivasalu
| studio         = Kalalaya Films
| distributor    = Kalalaya Films
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by Hameed Kakkassery and produced by HH Abdulla Settu. The film stars Prem Nazir, Jayabharathi, Adoor Bhasi and Sankaradi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
 
*Prem Nazir
*Jayabharathi
*Adoor Bhasi
*Sankaradi
*T. R. Omana
*Bahadoor
*Bhagyalakshmi
*Rajasree
*K. P. Ummer Sudheer
*Sujatha Sujatha
*Vincent Vincent
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Adutha Lottery || Raveendran, KR Venu || P. Bhaskaran || 
|-
| 2 || Ammuvininnoru Sammaanam || B Vasantha, Chorus || P. Bhaskaran || 
|-
| 3 || Ellaamarinjavan Nee Maathram || S Janaki || P. Bhaskaran || 
|-
| 4 || Kalpanaaraamathil || Cochin Ibrahim, LR Anjali || P. Bhaskaran || 
|-
| 5 || Krishna Dayaamaya || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 