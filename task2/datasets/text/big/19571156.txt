Iska's Journey
 
 
 
}}
{{Infobox Film
| name           = Iszka utazása
| director       = Csaba Bollók
| producer       = Ágnes Csere
| writer         = Csaba Bollók
| starring       = Mária Varga Rózsika Varga Marian Rusache Marius Bodochi Dan Tudor Ágnes Csere Gergely Levente
| music          = 
| cinematography = 
| editing        = 
| distributor    = Merkel Film
| released       =  
| runtime        = 92 min.
| country        = Hungary Romanian
| budget         = HUF 154,000,000 
| gross          = 
}}

Iszka utazása ( )  is a 2007 Hungarian film directed by Csaba Bollók and stars newcomer Mária Varga.

== Synopsis ==
The film is the result of director Bollóks accidental meeting with a 14-year-old street child, Maria Varga, living in a coal mining town of the Southern Carpathians.  The story follows her journey from her small town to the Black Sea, where she becomes a victim of human trafficking.

== Distribution and response ==

The film was first screened in February 2007 at the Hungarian Film Festival, where it ended up as the surprise winner of the Grand Prize. After its international premier at the Berlinale in 2007, Iskas Journey was received on the international festival circuit as a huge success. More than 50 European, Asian, and American film festivals have presented Bollók’s feature to date. Following its North American premier in Seattle last year, Iskas Journey was presented in Chicago, Cleveland, Toronto, Palm Springs and Vancouver.
 official submission Best Foreign Language Film. 

== References ==
 

== External links ==
*  
*  

 
 


 