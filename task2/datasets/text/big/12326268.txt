All the Fine Young Cannibals
{{Infobox film
| name           = All the Fine Young Cannibals
| image          = all_the_fine_young_cannibals_poster.jpg
| image size     = 
| alt            = 
| caption        = Original movie poster by Reynold Brown Michael Anderson
| producer       = Pando S. Berman Robert Thom  based on = novel "The Bixby Girls" by Rosamond Marshall George Hamilton
| music          = Jeff Alexander
| cinematography = William H. Daniels
| editing        = 
| studio         = MGM
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 112 min.
| country        = USA
| language       = English
| budget         = $1,638,000  . 
| gross          = $1,810,000 
}}
 Michael Anderson, George Hamilton, and Pearl Bailey.  Hamilton said it "combined Southern Gothic with a biopic of jazzman Chet Baker". George Hamilton & William Stadiem, Dont Mind If I Do, Simon & Schuster 2008 p 139 

==Overview==
The story concerns the lives of Chad Bixby (Wagner) and Sarah "Salome" Davis (Wood)&nbsp;– how they are forced apart by bad circumstances and brought together by chance. The film examines the experience of starting off "dirt poor" and ending up "idle rich", in a melodramatic style; but concludes that after the changes in lifestyle, the personalities remain the same. The story is loosely based on the life of Chet Baker. The film was the first Wagner and Wood made as husband and wife.

The film was the inspiration for the name of the 1980s musical group Fine Young Cannibals. 

==Cast==
*Robert Wagner as Chad Bixby
*Natalie Wood as Sarah "Salome" Davis
*Susan Kohner as Catherine McDowall George Hamilton as Tony McDowall
*Pearl Bailey as Ruby
*Jack Mullaney as Putney Tinker
*Onslow Stevens as Joshua Davis Anne Seymour as Mrs. Bixby
*Virginia Gregg as Ada Davis
*Mabel Albertson as Mrs. McDowall
*Louise Beavers as Rose
==Production==
This was the first film Wagner and Wood made together. Smooth Sailing for the Wagners: Sure Sign of Success in Hollywood: Bob and Nat Are Buying Fifth Boat
Hopper, Hedda. Chicago Daily Tribune (1923-1963)   24 Jan 1960: f22.  

George Hamilton says Vincente Minelli shot an ending for the film that was not used. 
==Box office==
According to MGM records, the film earned $950,000 in the US and Canada, and $860,000 elsewhere, resulting in a loss of $1,108,000. 

==References==
 

==External links==
* 
* 
*  at TCMDB
 

 
 
 
 
 
 
 
 
 
 
 
 


 