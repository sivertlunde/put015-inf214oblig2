The Hideous Sun Demon
{{Infobox Film
| name           = The Hideous Sun Demon
| image          = The Hideous Sun Demon.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = {{plainlist|
* Tom Boutross
* Robert Clarke
}}
| producer       = Robert Clarke
| writer         = {{plainlist|
* Robert Clarke
* Phil Hiner
* Doane R. Hoag
* E.S. Seeley Jr.
}}
| starring       = {{plainlist|
* Robert Clarke
* Patricia Manning
* Nan Peterson
* Patrick Whyte
* Fred La Porta
* Peter Similuk
* William White
}}
| music          = John Seely
| cinematography = {{plainlist|
* Stan Follis
* Vilis Lapenieks
* John Arthur Morrill
}}
| editing        = Tom Boutross
| distributor    = Pacific International Enterprises
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = $50,000
| gross          = 
}}
The Hideous Sun Demon (UK title: Blood on His Lips) is a 1959 science fiction horror film written, directed, and produced by Robert Clarke, who also starred in the film. The film focuses on a scientist (portrayed by Clarke) who is exposed to a radioactive isotope and soon finds out that it comes with horrifying consequences.

==Plot==
 
When research scientist Dr. Gilbert "Gil" McKenna falls unconscious after accidentally being exposed to radiation during an experiment with a new radioactive isotope, he is rushed to a nearby hospital. Attending physician Dr. Stern is surprised to find that Gil shows no signs of burns typical to a five-minute exposure to radiation and informs Gils co-workers, lab assistant Ann Lansing and scientist Dr. Buckell, that he will keep the patient for several days of observation.

Later, Gil is taken to the solarium to receive the suns healing rays, but while he naps, the suns rays metamorphose Gil into a scaled creature, horrifying the other patients. Seeing his own skin, Gil flees to the bathroom to confront his new appearance. Later, Stern explains Gils affliction to Lansing and Buckell: Humans have evolved from a chain of living beings beginning with one-celled organisms that progressed into fish, amphibians, reptiles, mammals and finally humans.  Stern assumes that the radiation poisoning has caused a reversal of evolution changing Gil into a prehistoric amphibian and that the catalyst for the regression is sunlight. Stern suggests that Gil can control his symptoms by staying in the dark and remaining in the hospital, but admits that the patient cannot be held against his will.

Although Gil has resumed his normal appearance, he has become mentally unstable. Notifying Lansing of his resignation, Gil drives his convertible to his large manor in an isolated coastal region where he drinks himself into a stupor. After hours of aimlessly walking the grounds, Gil drives to a bar where he finds sultry piano player Trudy Osborne singing. Removing his sunglasses in the darkened bar, Gil meets Trudys eyes in a romantic glance, but he soon leaves and recklessly drives back to his house.

Back at the research facility, Buckell receives word that noted radiation-poisoning specialist Dr. Hoffman has agreed to help Gil and plans on arriving in the area within a few days. Because Gil has disconnected his phone, Lansing offers to drive to the manor to deliver Hoffmans letter. After studies about radiation poisoning offer no leads on solving Gils own particular symptoms, he walks to the ocean bluffs to commit suicide, but the laughter from children playing nearby softens his resolve. Instead, Gil returns to the bar where Trudy joins him for a drink and comments that the evening is not over because it is "never late until the sun comes up." Although Gil is disturbed by the comment, his loneliness draws him closer to Trudy.

When bar patron George insinuates that he has purchased Trudys company for the evening and she rebuffs him, Gil defends her decision, causing a fistfight between the men. After knocking George to the ground, Gil flees with Trudy into the night in his convertible. Later that evening, after they kiss while walking the shoreline, they make love, falling asleep in the sand until the morning light awakens Gil. Realizing the suns rays will cause him to become the amphibious monster, Gil speeds away in his car leaving Trudy stranded on the beach. Arriving at the house, Gil runs in, but not before the transformation occurs.

Meanwhile, Lansing arrives and seeing the cellar door ajar, bravely opens it to find Gil cowering in a corner, physically recovered from the transformation but in a state of shock. Gil is at first uninterested in seeing Hoffman because he believes he is "beyond help," but Lansings sobbing pleas convince him to see the doctor. During his examination, Hoffman orders Gil to remain in the house at all times for precaution until he can return with help. Alone in the house, Gils restless sleep leads him to return to the bar, where George and his thugs, prompted by Trudys story about Gils abandoning her, beat Gil almost unconscious.

Fearing Gil will die if left unattended, Trudy takes him to her apartment where he sleeps until morning. After Gil asks to remain there until the evening, explaining he has a reaction to the sun, George arrives and, seeing Gil, forces him at gunpoint out into the daylight, causing the transformation to occur. Infuriated by Georges threats, the creature strangles him to death, then runs into the hills, frightening children and brutally killing a dog in his path. Returning to the house, the creature finds Hoffman, Lansing and Buckell waiting there and returns to his normal human state. When Gil admits to the murder, others assure him that he acted in self-defense, but when the police arrive with an arrest warrant, a hysterical Gil races from the grounds in his car and accidentally hits a police officer.

Later, Gil hides inside an oil field shack in a residential district, while police comb the area and set up roadblocks. Despite radio and newspapers reports that a killer is on the loose, young Suzy evades her mothers orders to remain inside the house and runs to her hideout, the oil field shack. Finding Gil there, Suzy offers to fetch him cookies and promises not to tell her mother about the strange man. However, when Suzys mother sees her hoarding cookies, she questions Suzy until she admits that she has a new friend, a sick, hungry man. While the terrified mother calls the police, Suzy slips out the door to return to Gil. Her mother chases after her into the oil field where police cars are just arriving. Realizing Suzy is endangered by being with him, Gil carries the girl out of the shack into the sunlight where he lets her go and soon changes into the creature. In the ensuing police chase, the creature attacks another officer and then climbs the stairs to the top of a tall natural gas tank where another officer tries to apprehend him. As the creature begins to strangle him, the officer shoots him in the neck and Gil falls several stories to his death while Buckell, Hoffman and a sobbing Lansing watch in dismay.

==Cast==
* Robert Clarke as Dr. Gilbert McKenna
* Patricia Manning as Ann Russell
* Nan Peterson as Trudy Osborne 
* Patrick Whyte as Dr. Frederick Buckell
* Fred La Porta as Dr. Jacob Hoffman
* Peter Similuk as George Messorio
* William White as Police Lt. Peterson

==Production==
=== Development === multiple personalities. Clarke and co-writer/director Tom Boutross wrote the first draft of the screenplay, then titled Saurus. The films crew consisted of students from the University of Southern California. Clarke pitched the story idea to Robin Kirkman, who liked the idea. The two men formed the production company Clarke-King Enterprises, and Kirkman worked as the films associate producer. E.S. Seely wrote the final draft of the films screenplay, which was then rewritten by Doane Hoag who "polished the dialogue," according to Clarke. The films was initially budgeted at $10,000, but eventually cost $50,000 in total.   

=== Casting ===
Clarke, the films director, writer and producer, starred in the lead role of Dr. Gilbert McKenna. The rest of the cast consisted mostly of aspiring actors and actresses from around USC. Actress Nan Peterson was cast because of her voluptuous figure, according to Clarke. Xandra Conkling, who plays the little girl that befriends McKenna in the film, was actually the daughter of Clarkes wifes sister; Pearl Driggs, the woman who portrayed the old woman on the hospital roof, was Clarkes mother-in-law. 

=== Filming === Signal Hill. Union Station train depot.  The film would be director Clarkes first and only effort as writer or director. 

==Reception==
 
Critical reception for the film has been mixed to negative.

Bob Stephens of the San Francisco Chronicle wrote, "But I must confess that I enjoy Demon. Its naivet is a more reliable pathway to wonder than the cynicism and condescension of contemporary fantasy films could ever be."  
TV Guide gave the film a negative review awarding it 1.5 / 4 stars calling it "laughable", but also commented that the monster costume was good. 
Leonard Maltin gave the film a negative review panning the films production values.    Wolfman theme" and an "effective and gritty film   boasts an excellent monster costume". 

The film has developed a cult following over the years since its release and is now considered a cult classic.   

==Home media release==
The film was released on DVD by Image Entertainment on March 21, 2000.  In 1983, a comedic re-dub titled Whats Up, Hideous Sun Demon was released with the permission of director Clarke.  This version of the film was later released on DVD by Image Entertainment on July 15, 2003. 

==Legacy==
 
The band Hideous Sun Demons were named after the film.  Clips from The Hideous Sun Demon appeared in the film It Came from Hollywood.  Part of the films soundtrack was used during the cemetery chase scene in Night of the Living Dead.  The film appeared on Elviras Movie Macabre, a television show in which the title character comments on the shown films.  The screenplay for the film was published as Scripts from the Crypt: The Hideous Sun Demon by BearManor on May 1, 2011.  

==References==
 

==External links==
*  
*  
*  
*  
 
 
 
 
 
 
 
 