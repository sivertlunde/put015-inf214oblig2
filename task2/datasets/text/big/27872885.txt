Cytherea (film)
{{Infobox film
| name           = Cytherea
| image          =
| caption        =
| director       = George Fitzmaurice
| producer       = Samuel Goldwyn
| writer         = Frances Marion (adaptation)
| based on       =  
| starring       = Alma Rubens Constance Bennett Norman Kerry Lewis Stone Irene Rich
| music          =
| cinematography = Arthur C. Miller J. A. Ball (Technicolor consultant)
| editing        = Stuart Heisler
| studio         = Samuel Goldwyn Productions Madison Productions Associated First National
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
| gross          =
}}

Cytherea is a lost  1924 American silent romantic drama film directed by George Fitzmaurice and starring Alma Rubens, Constance Bennett, and Norman Kerry. Based on the novel Cytherea, Goddess of Love, by Joseph Hergesheimer and was adapted for the screen by Frances Marion. Cytherea features
two dream sequences filmed in an early version of the Technicolor color film process.   

==Production background== Associated First natural light. 

==Cast==
*Irene Rich - Fanny Randon
*Lewis Stone - Lee Randon
*Norman Kerry - Peyton Morris
*Betty Bouton - Claire Morris
*Alma Rubens - Savina Grove
*Charles Wellesley - William Grove
*Constance Bennett - Annette Sherman
*Peaches Jackson - Randon Child Mickey Moore - Randon Child 
*Hugh Saxon - Randon Butler Lee Hill - Grove Butler
*Lydia Yeamans Titus - Laundress
*Brandon Hurst - Daniel Randon

==See also==
*List of early color feature films
*List of lost films

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 