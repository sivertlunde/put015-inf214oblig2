Xuxa Popstar
 
{{Infobox film
| name           = Xuxa Requebra 
| image          = 
| caption        = 
| director       = Paulo Sérgio de Almeida Tizuka Yamasaki
| producer       = Diler Trindade
| writer         = Elizeu Ewald Vivian Perl Wagner de Assis
| starring       = Xuxa Meneghel Luigi Baricelli Marcos Frota Deborah Blando Sílvia Pfeifer
| music          = Deborah Blando Mu Carvalho Maurício Manieri	
| cinematography = Cezar Moraes	
| editing        =
| studio         = 
| distributor    = Globo Filmes Xuxa Produções
| released       =  
| runtime        = 86 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}
Xuxa Popstar is a 2000 Brazilian film, directed by Tizuka Yamasaki and Paulo Sérgio de Almeida. The film is starring Xuxa Meneghel and Luigi Baricelli. "Xuxa Popstar" it became the most seen film in Brazil in 2001, with more than 2.4 million viewers. 

== Plot ==
After a brilliant career abroad, a top international model called Nicky back to Brazil to find her prince charming, she met through a chat on the Internet. In the country, it becomes a successful fashion entrepreneur, but your competitor does everything to hinder the girl.

== Cast ==
*Xuxa Meneghel ... Nicky
*Luigi Baricelli ... Raio de Luz
*Marcos Frota ... JP
*Sílvia Pfeifer ... Vany
*Cláudio Corrêa e Castro ... Olímpio
*Cláudia Rodrigues ... Mari
*Luís Salém ... Cicinho
*Deborah Blando ... Mel
*Isabelle Drummond ... Ju
*Brunno Abrahão ... Lipe
*Leonardo Netto ... Vitinho Leonardo ... Dado
*Gianne Albertoni ... Herself
*Harmonia do Samba ...
*Scheila Carvalho .... Herself

== See also ==
* List of Brazilian films of 2000

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 