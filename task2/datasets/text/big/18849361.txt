Baghdad Thirudan
{{Infobox film
| name           = Baghdad Thirudan பாக்தாத் திருடன்
| image          = Baghdad_Thirudan.jpg
| director       = T. P. Sundaram
| producer       = T. P. Sundaram Harilal Patoviya
| writer         = A. S. Muthu
| story          = A. S. Muthu
| starring       = M. G. Ramachandran Vyjayanthimala M. N. Nambiar T. S. Balaiah T. R. Ramachandran M. N. Rajam
| music          = G. Govindarajulu Naidu
| cinematography = M. Krishnaswamy
| editing        = G. D. Joshi
| studio         = Southern Movies
| distributor    = Southern Movies
| released       =   
| runtime        = 170 mins
| country        = India Tamil
| budget         = 
}}
 Tamil film The Thief of Bagdad and was inspired by 1951 Tony Curtis-starrer, The Prince Who Was A Thief. 

==Plot==
Following the betrayal of the Prime Minister, the Maharaja and the Maharani of the kingdom are murdered and their son, MGR, is hidden in a herd of cows. MGR is found by robbers hidden in the forest and eventually grows up to become their leader. The two impostors who sit on the throne have a daughter and a child servant who poses as the son who survived the death of the former royal couple. MGR, a sort of Robin Hood, robs the rich to give to the poor and strives to reconquer his kingdom.

==Cast==
* M. G. Ramachandran as Ali
* Vyjayanthimala as Zarina
* M. N. Nambiar
* T. S. Balaiah
* T. R. Ramachandran
* S. A. Ashokan
* M. N. Rajam
* A. Sandhya
* S. N. Lakshmi 
* M. S. S. Pakiyam
* K. S. Angamuthu

==Crew==
*Producer: T. P. Sundaram & Harilal Patoviya
*Production Company: Southern Movies
*Director: T. P. Sundaram
*Music: G. Govindarajulu Naidu
*Lyrics: A. Maruthakasi
*Story: A. S. Muthu
*Dialogues: A. S. Muthu
*Art Direction: Raju
*Editing: G. D. Joshi
*Choreography: R. Krishnaraj, B. Sohanlal, Jayashankar & V. S. Muthusami Pillai
*Cinematography: M. Krishnaswamy
*Stunt: R. N. Nambiar
*Audiography: C. P. Kanniyappan
*Songs Recording: T. S. Rangasamy & C. P. Kanniyappan
*Dialogues Recording: M. S. Nageswaran Gopi Krishna, Lalitha & Ragini

==Production==
Baghdad Thirudan was financed by Naidu, the former owner of Golden Studios. A single set in the film cost  , and MGR always wanted new sets. By this type of over-expenditure, it was believed that the film would ultimately cost  . MGR demanded that an additional   be spent to complete the film, much to Naidus horror. 

Baghdad Thirudan was also the first film that featured MGR opposite Vyjayanthimala. MGR briefly served as the films editor. During the filming of Baghdad Thirudan, MGR said, "I was at the editing table, and it was so easy to join the cuts, because the movements just flowed into each other".  Actress S. N. Lakshmi, who played an important role in the film, had to fight a leopard in one scene without the use of a "body double|dupe",  though she later remarked that she feared cats. 

==Soundtrack== Playback singers are T. M. Soundararajan, Pendyala Nageswara Rao, Jikki, P. Suseela, & K. Jamuna Rani.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Azhagu Laila || Jikki || rowspan=11|A. Maruthakasi || 02:28
|-
| 2 || Enthan Kathai Idhana || P. Suseela || 02:51
|-
| 3 || Indha Jagame En Kaiyile || K. Jamuna Rani || 05:24
|-
| 4 || Kanneeril Vellam || Jikki || 03:16
|-
| 5 || Oru Kul Kul Paarvaiyile || Jikki || 02:12
|-
| 6 || Pothukulunguthey || P. Suseela || 03:30
|-
| 7 || Siricha Pothum || P. Suseela || 02:44
|-
| 8 || Sokkudhe Manam || P. Susheela || 03:33
|-
| 9 || Unmai Anbil || T. M. Soundararajan & P. Suseela || 02:18
|-
| 10 || Vinnil Yedho Vedhanai || Pendyala Nageswara Rao || 03:27
|-
| 11 || Yaarukku Dimikki || T. M. Soundararajan || 02:34
|}

==Release==

===Reception===
Kalyanamalai Magazine labelled Baghdad Thirudan as one of Vyjayanthimalas "memorable films." 

===Box office===
The film ran for over 125 days in theatres, officially declared a blockbuster hit and highest grossing film of the year.  

==References==
 

==External links==
*  
*   at Upperstall.com

 
 
 
 
 