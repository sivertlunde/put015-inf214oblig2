Gun (2010 film)
{{Infobox film
| name           = Gun
| image          = Gun Film Poster.jpg
| border         = yes
| caption        = Poster
| director       = Jessy Terrero
| producer       = Randall Emmett   Curtis Jackson   George Furla Curtis Jackson Curtis Jackson Val Kilmer AnnaLynne McCord James Remar Danny Trejo La La (entertainer) Christa Campbell John Larroquette
| music          = Ben Zarai   David Allen Kitchens
| cinematography = Zeus Morand
| editing        = Kirk Morri Cheetah Vision Films   Hannibal Pictures   Richard Jackson Films   120 Tax Credit Finance   Emmett/Furla/Oasis Films|Emmett/Furla Films
| distributor    = Image Entertainment
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Curtis Jackson Grand Rapids, Michigan.

==Synopsis==
Angel (Kilmer) gets out of prison only to get involved in the Arms trafficking|gun-running ring of his old friend, Rich (Jackson). Rich and friends raid a club, killing Ali Tyrell, another arms dealer.
 ATF agents come disrupting but continues about mathematics teacher buying a Smith & Wesson firearm but his house gets robbed by a gangsta and later he kill someone with the same gun. He later decides to sell the gun back to the same shop without knowing the shop owner gives the firearms to Rich and his friends.

Angel meets with Rich who helped him before to move guns. Afterwards Rich and his friends torture an arms dealer who lied to them. Rich meets a news reporter to talk about the gun business and she takes him to his house.

The next day Angel meets the officer, and its shown that he was let out of prison to be a narc for them, with the seen being shown where he is in prison and it told that his wife was killed. The reporter goes to her wealthy boss, to sell advanced guns to Rich, but her boss gives a lecture saying he might be a thug selling guns and says this a family business which started off with Mexican drugs now to advanced guns but she says they could trust him.

The next day they meet without knowing that Angel is against them and the police are seeing their every move. The boss introduces himself to Rich and gives his van full of guns to him. He calls one of his minions to give Rich one of the guns to examine. Rich immediately agrees to buy them and is just about to pay when the police attack. During the fight Rich gets shot. Angel rescues him, but when they are alone turns and points his gun at Rich, who realizes that Angel has been the rat all along. Angel tells him that the cocktail waitress that was killed was his wife. Rich laughs and tells Angel that he is just like him, a killer, and that everybody that Angel has killed was someones son or husband. As Angel breaks down, Rich pulls a gun, but Angel gets the shot off first. Rich lies on the ground, telling Angel to kill him, that nobody will miss him and that hell see him in Hell. As Angel is about to kill him, the agent shoots Angel.

As time elapsed, we see Angel walking down a corridor with a sling on his arm, and he meets with his daughter. The agent is seen walking into his office and is questioned by reporters on how he feels about Richs plea bargain, and on the rumors that he has stepped down. The movie ends with Rich walking to his cell in handcuffs, and he looks up through the bars as the cell is opening.

==Cast==
* 50 Cent as Rich
* Val Kilmer as Angel
* AnnaLynne McCord as Gabriella

* James Remar as Detective Rogers
* Malik Barnhardt
* Paul Calderón as Detective Jenkins
* Christa Campbell as News reporter
* Josh Carrizales as Valentine
* Alton Clinton as Alis Crew #1
* Jill Dugan as Angels Wife
* Mark Famiglietti as ATF Agent Peterson
* Gary Darnell Jackson Jr. as Young Rich
* Hassan Johnson as Clinton
* Kristin Kandrac as News Reporter #2
* Anthony Kennedy as Richs Dad
* John Larroquette as Sam Mike Malin as ATF Agent Monroe
* C. Malik Whitfield as Dante

==External links==
*  

 
 
 
 
 
 
 
 