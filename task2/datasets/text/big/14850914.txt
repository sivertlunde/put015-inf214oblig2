The Black Tavern
 
 
{{Infobox film name =  image = TheBlackTavern.jpg alt =  caption = DVD cover art traditional = 黑店 simplified = 黑店 pinyin = Hēi Diàn}} director = Teddy Yip producer = Run Run Shaw writer = Yeh I-fang starring = Shih Szu music = Frankie Chan Wu Dajiang cinematography = Yau Kei editing = Chiang Hsing-lung Lee Yim-hoi studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 83 minutes country = Hong Kong language = Mandarin budget =  gross =
}} Teddy Yip and produced by the Shaw Brothers Studio, starring Shih Szu.

==Cast==
*Shih Szu as Zhang Caibing
*Tung Li as Zha Xiaoyu
*Ku Feng as Zheng Shoushan
*Kong Ling as Jinglu
*Kwok Chuk-hing as Jinghong
*Barry Chan as Jinghu
*Yeung Chi-hing as Hai Gangfeng
*Dean Shek as wandering monk
*Wang Hsieh as Gao Sanfeng
*Yue Fung as Sanniang
*Situ Lin as Doggie
*Law Hon as tavern cook
*Lee Ho as Iron Arm Liu Tong
*Wu Ma as leader of Xiangxi Five Ghosts
*Yau Ming as Three-headed Cobra
*Chiang Nan as skilled robber
*Liu Wai as Hu
*Chan Chan-kong as Hus partner
*Yeung Chak-lam as robber
*Chu Gam as Taian
*Unicorn Chan as Three-headed Cobra
*Yuen Wah as Xiangxi Ghost
*Sa Au as Xiangxi Ghost / constable
*Ho Kei-cheong as constable Mars as Official Hais servant
*Ling Hon as restaurant book keeper
*Yi Fung as waiter
*Cheung Hei as restaurant guest
*Wong Yuet-ting as restaurant guest
*Gam Tin-chue as restaurant guest

==External links==
* 
*  on Hong Kong Cinemagic


 
 
 
 
 
 
 


 
 