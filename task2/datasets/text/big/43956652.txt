REMTV
 
{{Infobox album
| Name = REMTV
| Type = video
| Artist = R.E.M.
| Cover = 
| Alt = 
| Released =  
| Recorded = various dates and locations; Unplugged: April 10, 1991 in Chelsea Studios and May 21, 2001 in TRL Studio at MTV Studios, both in New York City, New York, United States
| Genre = Alternative rock
| Length =  
| Language = English Rhino
| Director = 
| Last album =   (2014)
| This album = REMTV (2014)
| Next album = 7IN—83–88 (2014)
{{Extra chronology
 | Artist = R.E.M.
 | Type = video
 | Last album = Live from Austin, TX (R.E.M. video album)|R.E.M. Live from Austin, TX (2010)
 | This album = REMTV (2014)
 | Next album =
}}
}}

REMTV is a six-disc DVD boxset collecting appearances by American  . 

The boxset includes a new documentary on the band, named "R.E.M. by MTV", by Alex Young (producer, director) and Dave Leopold (editor).  The documentary is a collage of various R.E.M. appearances not only on MTV, but on several programs.

==Track listing==
All songs written by Bill Berry, Peter Buck, Mike Mills and Michael Stipe except as indicated.

Disc 1

:MTV Unplugged 1991 (April 10, 1991)
:#"Half a World Away"
:#"Disturbance at the Heron House"
:#"Radio Song"
:#"Low"
:#"Perfect Circle"
:#"Fall On Me"
:#"Belong" Love Is All Around" (Reg Presley)
:#"Its the End of the World as We Know It (And I Feel Fine)"
:#"Losing My Religion"
:#"Pop Song 89"
:#"Endgame"

::Outtakes
:#"Fretless"
:#"Swan Swan H"
:#"Rotary Eleven" Get Up"
:#"World Leader Pretend"

:MTV Unplugged 2001 (May 21, 2001)
:#"All the Way to Reno (Youre Gonna Be a Star)" (Buck, Mills, Stipe)
:#"Electrolite"
:#"At My Most Beautiful" (Buck, Mills, Stipe)
:#"Daysleeper" (Buck, Mills, Stipe)
:#"So. Central Rain (Im Sorry)"
:#"Losing My Religion"
:#"Country Feedback"
:#"Cuyahoga" Imitation of Life" (Buck, Mills, Stipe)
:#"Find the River"

::Outtakes The One I Love"
:#"Disappear" (Buck, Mills, Stipe)
:#"Beat a Drum" (Buck, Mills, Stipe)
:#"Ive Been High" (Buck, Mills, Stipe)
:#"Ill Take the Rain" (Buck, Mills, Stipe)
:#"Sad Professor" (Buck, Mills, Stipe)
:#"The Great Beyond" (Buck, Mills, Stipe)


Disc 2

:VH1 Storytellers (October 23, 1998)
:#"Electrolite"
:#"Daysleeper" (Buck, Mills, Stipe)
:#"Losing My Religion"
:#"Perfect Circle"
:#"Sad Professor" (Buck, Mills, Stipe)
:#"Fall On Me"
:#"Im Not Over You" (Buck, Mills, Stipe)
:#"The Apologist" (Buck, Mills, Stipe) Man on the Moon"

::Outtakes
:#"New Test Leper"
:#"Parakeet" (Buck, Mills, Stipe)
:#"(Dont Go Back To) Rockville"
:#"Suspicion (R.E.M. song)|Suspicion" (Buck, Mills, Stipe)
:#"Walk Unafraid" (Buck, Mills, Stipe)
:#"At My Most Beautiful" (Buck, Mills, Stipe)
 The Cutting Edge (June 14, 1984)
:#"(Dont Go Back To) Rockville"
:#"Driver 8"
:#"Wendell Gee"
:#"Smokin in the Boys Room" (Cub Koda, Michael Lutz)
:#"Time After Time (Annelise)"
:#"Driver 8"

:Livewire (talk show)|Livewire (October 30, 1983)
:#"So. Central Rain (Im Sorry)"
:#"Carnival of Sorts (Box Cars)"

:MTV 10th Anniversary Special (November 10, 1991, featuring members of the Atlanta Symphony Orchestra)
:#"Losing My Religion"

:MTV Video Music Awards (September 2, 1993, including Brian Harris and Duane Saetveit)
:#"Everybody Hurts"
:#"Drive (R.E.M. song)|Drive"

:MTV Video Music Awards (September 7, 1995)
:#"The Wake-Up Bomb"
 European Music Awards (November 12, 1998)
:#"Daysleeper" (Buck, Mills, Stipe)

:European Music Awards (November 8, 2001)
:#"Imitation of Life" (Buck, Mills, Stipe)

:Rock and Roll Hall of Fame Induction (March 12, 2007)
:#"Begin the Begin"
:#"Gardening at Night"
:#"Man on the Moon"

:The Colbert Report (April 2, 2008)
:#"Supernatural Superserious" (Buck, Mills, Stipe)


Disc 3

:R.E.M. in Dallas (September 19, 1995)
:#"I Took Your Name"
:#"Whats the Frequency, Kenneth?"
:#"Crush with Eyeliner"

:R.E.M. Uplink at the Bowery Ballroom (October 28, 1998)
:#"Losing My Religion"
:#"Lotus (song)|Lotus" (Buck, Mills, Stipe)
:#"Daysleeper" (Buck, Mills, Stipe)
:#"E-Bow the Letter"
:#"The Apologist" (Buck, Mills, Stipe)
:#"So. Central Rain (Im "Sorry)"
:#"Walk Unafraid" (Buck, Mills, Stipe)
:#"Man on the Moon" Radio Free Europe"

:Live in Cologne (May 12, 2001)
:#"All the Way to Reno (Youre Gonna Be a Star)" (Buck, Mills, Stipe)
:#"The Lifting" (Buck, Mills, Stipe)
:#"Imitation of Life" (Buck, Mills, Stipe)
:#"The One I Love"
:#"She Just Wants to Be" (Buck, Mills, Stipe)
:#"Walk Unafraid" (Buck, Mills, Stipe)
:#"Losing My Religion"
:#"Man on the Moon"
:#"Its The End Of The World As We Know It (And I Feel Fine)"

::Outtakes
:#"Whats the Frequency, Kenneth?"
:#"Cuyahoga"
:#"Electrolite"
:#"Ive Been High" (Buck, Mills, Stipe)
:#"Find the River"
:#"Ill Take the Rain" (Buck, Mills, Stipe)
:#"At My Most Beautiful" (Buck, Mills, Stipe)
:#"So. Central Rain (Im Sorry)"


Disc 4

:R.E.M. at The Tabernacle, London (March 2, 1999)
:#"Losing My Religion"
:#"Daysleeper" (Buck, Mills, Stipe)
:#"Walk Unafraid" (Buck, Mills, Stipe)
:#"Man on the Moon"
:#"Whats the Frequency, Kenneth?"
:#"Its the End of the World as We Know It (And I Feel Fine)"

:MTV Supersonic Milan (May 2, 2001)

:#"Losing My Religion"
:#"The Great Beyond" (Buck, Mills, Stipe)
:#"Whats the Frequency, Kenneth?"
:#"Daysleeper" (Buck, Mills, Stipe)
:#"All the Way to Reno (Youre Gonna Be a Star)"
:#"The Lifting" (Buck, Mills, Stipe)
:#"Ill Take the Rain" (Buck, Mills, Stipe)
:#"Ive Been High" (Buck, Mills, Stipe)
:#"Man on the Moon"
:#"She Just Wants to Be" (Buck, Mills, Stipe)
:#"Imitation of Life" (Buck, Mills, Stipe)
 Rock am Ring (June 3, 2005)
:#"Whats the Frequency, Kenneth?"
:#"Leaving New York" (Buck, Mills, Stipe)
:#"Imitation of Life" (Buck, Mills, Stipe)
:#"Electron Blue" (Buck, Mills, Stipe)
:#"Man on the Moon"

::Outtakes
:#"I Took Your Name" Bad Day"
:#"Drive"
:#"The Outsiders" (Buck, Mills, Stipe)
:#"Leave"
:#"Me in Honey"
:#"Wanderlust (R.E.M. song)|Wanderlust" (Buck, Mills, Stipe)
:#"Everybody Hurts"
:#"Electrolite" Orange Crush"
:#"The One I Love"
:#"Walk Unafraid" (Buck, Mills, Stipe)
:#"Losing My Religion"
:#"Imitation of Life" (Buck, Mills, Stipe)
:#"The Great Beyond" (Buck, Mills, Stipe)
:#"Animal (R.E.M. song)|Animal" (Buck, Mills, Stipe)
:#"Im Gonna DJ" (Buck, Mills, Stipe)


Disc 5

:Live at Rolling Stone, Milan (March 18, 2008)
:#"Living Well Is the Best Revenge" (Buck, Mills, Stipe)
:#"Drive"
:#"Accelerate" (Buck, Mills, Stipe) Hollow Man" (Buck, Mills, Stipe)
:#"Electrolite"
:#"Houston" (Buck, Mills, Stipe)
:#"Supernatural Superserious" (Buck, Mills, Stipe)
:#"Bad Day"
:#"Losing My Religion"
:#"Im Gonna DJ" (Buck, Mills, Stipe)
:#"Horse to Water" (Buck, Mills, Stipe)
:#"Imitation of Life" (Buck, Mills, Stipe)
:#"Until the Day Is Done" (Buck, Mills, Stipe)
:#"Man on the Moon"
 Oxegen Festival (July 12, 2008)
:#"Whats the Frequency, Kenneth?"
:#"Drive"
:#"Its the End of the World as We Know It (And I Feel Fine)"
:#"Man-Sized Wreath" (Buck, Mills, Stipe)
:#"Im Gonna DJ" (Buck, Mills, Stipe)
:#"Supernatural Superserious" (Buck, Mills, Stipe)
:#"Man on the Moon"

:R.E.M. in Athens, Greece (October 5, 2008)
:#"Living Well Is the Best Revenge"
:#"Whats the Frequency, Kenneth?"
:#"Drive"
:#"Man-Sized Wreath" (Buck, Mills, Stipe)
:#"Bad Day" (Buck, Mills, Stipe)
:#"Electrolite"
:#"(Dont Go Back To) Rockville"
:#"The Great Beyond" (Buck, Mills, Stipe)
:#"The One I Love"
:#"Losing My Religion"
:#"Let Me In"
:#"Orange Crush"
:#"Imitation of Life" (Buck, Mills, Stipe)
:#"Supernatural Superserious" (Buck, Mills, Stipe)
:#"Man on the Moon"


Disc 6

:R.E.M. by MTV (original documentary)
:Deleted scenes:
:#"Peter"
:#"Politics"
:#"Golf"
:#"The Hornblower Affair"
:#"The Year 2000"

 download of the MTV Supersonic show in Milan. 

==References==
{{Reflist|refs=

   

   

   

}}

==External links==
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 