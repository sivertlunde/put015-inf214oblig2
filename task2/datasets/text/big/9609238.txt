The Lone Defender
{{Infobox film
| name           = The Lone Defender
| image          = File:The Lone Defender poster.jpg
| caption        = Poster for chapter two of the serial
| director       = Richard Thorpe
| producer       = Nat Levine Harry Fraser
| narrator       = Walter Miller June Marlowe Josef Swickard Buzz Barton Lee Shumway
| music          = Lee Zahler
| cinematography = Benjamin Kline
| editing        = Wyndham Gittens
| distributor    = Mascot Pictures
| released       = 1930
| runtime        = 12 chapters (217 min)
| country        = United States English
| budget         = $40,000 (estimated)
}} Mascot movie serial starring Rin Tin Tin.  This was Mascots first all sound serial (the second to have any sound at all, after the partial sound in The King of the Kongo).  This was Rin Tin Tins first serial at Mascot, after being dropped by Warner Bros. when they decided animal pictures would not work with "Talkies".  He also starred in the later serial The Lightning Warrior, which would be his last appearance.  Mascot made a third serial, The Adventures of Rex and Rinty, but that Rin Tin Tin was not the same dog.

The plot revolves around Rin Tin Tin as "Rinty" and a secret gold mine fought over by the criminal "The Cactus Kid" and the legitimate owners. Material from this serial was edited into a feature film version and released under the same name in 1934 in film|1934.

==Plot==
Prospector Juan Valdez is murdered by The Cactus Kid and his gang in an attempt to discover the location of his gold mine.  Valdezs dog Rinty witnesses the murder and can also lead the gang to the mine, making him the villains target throughout the serial.  In addition Rinty must help Valdezs daughter Dolores legitimately find and claim the mine while being blamed for the wolf attacking local livestock.

The mysterious figure of Ramon is constantly on hand, overhearing pieces of the villains conversations.  He appears to be another bandit but his actions seem to contradict that.
 Justice Department.

==Cast==
*Rin Tin Tin as "Rinty", Juan Valdezs dog Walter Miller Justice Department Agent posing as the "Mysterious Figure", Ramon
*June Marlowe as Dolores Valdez, Juan Valdezs daughter
*Josef Swickard as Juan Valdez, Prospector who owns a secret gold mine
*Buzz Barton as Buzz, Ramons sidekick
*Lee Shumway as Amos Harkey, the villainous cantina owner
*Julia Bejarano as Maria, the Dueña
*Lafe McKee as Sheriff Billings
*Arthur Morrison as Limpy
*Frank Lanning as Burke, Prospector and Juan Valdezs partner
*Bob Kortman as Jenkins, one of the Cactus Kids Henchmen
*Victor Metzetti as Red, one of the Cactus Kids Henchmen
*Otto Metzetti as Reds Partner

==Production==
===Stunts===
*Joe Bonomo
*Kermit Maynard
*Arthur Metzetti
*Victor Metzetti

==Chapter titles==
# Mystery of the Desert
# The Fugitive
# Jaws of Peril
# Trapped
# Circle of Death
# Surrounded by the Law
# The Ghost Speaks
# The Brink of Destruction
# The Avalanche
# Fury of the Desert
# Cornered
# Vindicated
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | origyear = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 202
 | chapter = Filmography
 }} 

==See also==
*List of film serials by year
*List of film serials by studio

==References==
 

==External links==
* 
* 
* 

 
{{Succession box Mascot Serial Serial
| before=The King of the Kongo (1929 in film|1929)
| years=The Lone Defender (1930 in film|1930)
| after=The Phantom of the West (1931 in film|1931)}}
 

 
 

 
 
 
 
 
 
 
 
 