Two for the Road (film)
{{Infobox film
| name           = Two for the Road
| image          = Two_road_moviep.jpg
| caption        = Theatrical release poster
| border         = yes
| director       = Stanley Donen
| producer       = Stanley Donen
| writer         = Frederic Raphael
| starring       = Albert Finney Audrey Hepburn William Daniels Eleanor Bron
| music          = Henry Mancini
| cinematography = Christopher Challis
| editing        = Madeleine Gug Richard Marden
| distributor    = 20th Century Fox
| released       =  
| runtime        = 111 minutes
| country        = United Kingdom
| language       = English
| budget         = $4 million    or $5.08 million 
| gross          = $12,000,000  $3,500,000  
}}
Two for the Road is a 1967 British comedy drama film directed by Stanley Donen and starring Albert Finney and Audrey Hepburn. Written by Frederic Raphael, the film is about a husband and wife who examine their twelve-year relationship while on a road trip to Southern France. The film was considered somewhat experimental for its time because the story is told in a non-linear fashion, with scenes from the latter stages of the relationship juxtaposed with those from its beginning, often leaving the viewer to interpolate what has intervened, which is sometimes revealed in later scenes. Several locations are used in different segments to show continuity throughout the twelve-year period.
 Breakfast at PVC trouser 100 Years... 100 Passions list.

==Plot== 230SL roadster to Northern France in order to drive to Saint-Tropez to celebrate the completion of a building project for a client, Maurice. Tensions between the couple are evident, and as they journey south they both remember and discuss several past journeys along the same road.

The earliest memory is their first meeting on a ferry crossing in 1954, when Mark was travelling alone and Joanna was part of a girls choir. They meet again when Joannas choir bus goes off the road and Mark helps get them back on the road. When the other girls get chickenpox, Joanna and Mark unexpectedly wind up hitchhiking south together.

The next story involves the two newlyweds travelling with Marks ex-girlfriend Cathy Manchester (Eleanor Bron), her husband (William Daniels) and daughter Ruth Ruthie (Gabrielle Middleton) from the USA. Ruthie is not given any limits, and her behaviour frustrates Mark and Jo. Eventually Ruthie reveals the unkind descriptions of Joanna her parents have made in private. At this point Mark and Joanna decide to travel alone.

One scene depicts Mark and Joanna dining in a restaurant during a particularly strained period in their marriage. They are not speaking at all. Joanna looks around the restaurant and asks Mark, "What kind of people can sit there without a word to say to each other?" Mark replies, quite sullenly, "Married people!" 
 Claude Dauphin) and his wife Françoise (Nadia Gray). Maurice becomes a generous but demanding client for Mark.

The next story shows them travelling with their young daughter Caroline (Kathy Chelimsky).

In another episode, Mark is travelling alone and has a fling with another motorist. The fling is shown to be fleeting and unserious in nature. Later, Joanna has an affair with Françoises brother David (Georges Descrières), which is portrayed as much more serious than Marks and threatens to end the marriage. However, while Joanna dines with David, they witness a couple eating together without saying a word. David asks offhandedly, "What kind of people can sit there without a word to say to each other?" Joanna replies excitedly, "Married people!" and, realizing she misses Mark despite their faded passion, runs back to him. 

At the end of the film, the Wallaces manage to end their long-term relationship to Maurice and find a new client in Rome. They honestly analyse the fears and insecurities which have plagued them throughout the film. Finally, they cross the border from France into Italy. This is new ground for them as well as for the audience, signalling a move beyond the old issues into a more mature future.

==Cast==
* Audrey Hepburn as Joanna Jo Wallace
* Albert Finney as Mark Wallace
* Eleanor Bron as Cathy Maxwell-Manchester born Seligman
* William Daniels as Howard Howie Maxwell-Manchester
* Gabrielle Middleton as Ruth Ruthie Maxwell-Manchester Claude Dauphin as Maurice Dalbret
* Nadia Gray as Françoise Dalbret
* Georges Descrières as David
* Jacqueline Bisset as Jackie
* Judy Cornwell as Pat
* Irène Hilda as Yvonne de Florac
* Leo Penn as Morrie Goetz
* Dominique Joos as Sylvia Obino
* Olga Georges-Picot as Joannas Touring Friend   

==Production==
;Filming locations
* Beauvallon, Drôme, France 
* Cap Valéry, France 
* Château de Chantilly, Chantilly, Oise, France 
* French Riviera, Alpes-Maritimes, France 
* Grimaud, Var, France 
* La Colle-sur-Loup, Alpes-Maritimes, France 
* Nice, Alpes-Maritimes, France 
* Paris, France 
* Port de Nice, Nice, Alpes-Maritimes, France (ferry disembarkment sequence)
* Ramatuelle, Var, France (Dalbret villa scenes)
* Restaurant Leï Mouscardins, Rue Portalet, Saint-Tropez, Var, France 
* Saint-Tropez, Var, France 
* Studios de la Victorine, 16 avenue Edoard Grinda, Nice, Alpes-Maritimes, France (studio)
* Étangs de Commelles, Coye-la-Forêt, Oise, France   

==Reception== review aggregate website Rotten Tomatoes, with an average of 7.2 out of 10. 

==Awards and nominations== Academy Award Nomination for Best Writing (Frederic Raphael) BAFTA Film Award Nomination for Best British Screenplay (Frederic Raphael)
* 1968 Cinema Writers Circle Award for Best Foreign Film (Mejor Película Extranjera) Won
* 1968 Directors Guild of America Award Nomination for Outstanding Directorial Achievement (Stanley Donen) Golden Globe Award Nomination for Best Motion Picture Actress (Audrey Hepburn)
* 1968 Golden Globe Award Nomination for Best Original Score (Henry Mancini) 
* 1967 San Sebastián International Film Festival Golden Seashell (Stanley Donen) Won
* 1967 Writers Guild of Great Britain Merit Scroll for Best British Comedy Screenplay (Frederic Raphael) Won
* 1967 Writers Guild of Great Britain Merit Scroll for Best British Original Screenplay (Frederic Raphael) Won   

==Trivia== Dangerous Curves", Marge and Homer as Ned and Maude Flanders portraying the characters played by Daniels and Bron. 

==References==
 

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 