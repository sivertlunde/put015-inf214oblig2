Kiss Me Again (1925 film)
{{Infobox film
| name           = Kiss Me Again
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Ernst Lubitsch
| producer       = 
| writer         = 
| screenplay     = Hanns Kräly
| story          = 
| based on       =  
| narrator       = 
| starring       = Marie Prevost Monte Blue Clara Bow
| music          = 
| cinematography = Charles Van Enger
| editing        = 
| studio         =  UFA (Germany)
| released       =  
| runtime        = 
| country        = United States
| language       = Silent English intertitles
| budget         = 
| gross          =
}}
 silent film comedy-romance directed by Ernst Lubitsch. It stars Marie Prevost, Monte Blue and Clara Bow. The film was based on the French play Divorçons! (1880), by Victorien Sardou and Émile de Najac, and the adapted version of the play Cyprienne. 

The film is now considered lost film|lost.  

==Cast==
*Marie Prevost as LouLou Fleury
*Monte Blue as Gaston Fleury John Roche as Maurice
*Clara Bow as Grizette
*Willard Louis as Dr. DuBois

==See also==
*Lets Get a Divorce (1918) That Uncertain Feeling (1941)
*List of lost films

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 