Sergeant Rutledge
{{Infobox film
| name           = Sergeant Rutledge
| image          = Sergeant_Rutledge_image.jpg
| image_size     =
| caption        =
| director       = John Ford
| producer       = Willis Goldbeck Patrick Ford
| writer         = James Warner Bellah Willis Goldbeck
| narrator       =
| starring       = Woody Strode Jeffrey Hunter Howard Jackson Mack David (Song "Captain Buffalo") Jeffrey Livingston (Song "Captain Buffalo")
| cinematography = Bert Glennon Jack Murray
| distributor    = Warner Bros.
| released       =  
| runtime        = 111 min.
| country        = United States English
| budget         =
}} Western and military courtroom drama starring Woody Strode and Jeffrey Hunter.  It was directed by John Ford and shot on location in Monument Valley, Utah.
 

The film starred Strode as a black first sergeant in the United States Cavalry accused of the rape and murder of a white girl at a U.S. Army fort in the late 1880s. 

==Plot== 9th U.S. Cavalry. His defense is handled by Lt. Tom Cantrell (Jeffrey Hunter|Hunter), Rutledges troop officer. The story is told through a series of Flashback (narrative)|flashbacks, expanding the testimony of witnesses as they describe the events following the murder of Rutledges Commanding Officer, Major Dabney, and the rape and murder of Dabneys daughter, for which Rutledge is the accused.

Circumstantial evidence suggests that the first sergeant  raped and murdered the girl and then killed his commanding officer. Worse still, Rutledge deserts after the killings. Ultimately, he is tracked down and arrested by Lt. Cantrell. At one point, Rutledge escapes from captivity during an Indian raid, but later, he voluntarily returns to warn his fellow cavalrymen that they are about to face an ambush, thus saving the troop. He is then brought back in to face the charges and the prejudices of an all-white military court.
 
Eventually he is found not guilty of the rape and murder of the girl when a local white man breaks down under questioning and admits that he raped the girl.

==Cast==
*Jeffrey Hunter as 1st Lt. Tom Cantrell, 9th Cavalry (counsel for the defense)
*Constance Towers as Mary Beecher
*Billie Burke as Mrs. Cordelia Fosgate
*Woody Strode as First Sergeant Braxton Rutledge, 9th Cavalry
*Juano Hernández as Sgt. Matthew Luke Skidmore, 9th Cavalry
*Willis Bouchey as Lt. Col. Otis Fosgate, 9th Cavalry (president of the court-martial)
*Carleton Young as Capt. Shattuck, 14th Infantry (prosecutor)
*Judson Pratt as 2nd Lt. Mulqueen, 9th Cavalry (court-martial board member)

==See also==
*Military history of African Americans
*Racism

==References==
 

==External links==
 
* 
*  
*  
*   Review of the film at  

 

 
 
 
 
 
 
 
 
 

 