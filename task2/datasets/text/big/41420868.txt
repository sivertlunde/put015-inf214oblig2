The Clown (1916 film)
{{infobox film
| title          = The Clown
| image          = The Clown (1916) lobby card.jpg
| imagesize      =
| caption        =
| director       = William C. deMille
| producer       = Jesse Lasky
| writer         = Marion Fairfax
| starring       = Victor Moore
| cinematography = Charles Rosher
| editing        =
| distributor    = Paramount Pictures
| released       = June 19, 1916
| runtime        = 5 reels
| country        = USA
| language       = Silent film(English intertitles)
}}
The Clown is a lost  1916 silent film starring stage star Victor Moore and directed by William C. deMille. It was produced by Jesse Lasky and distributed by Paramount Pictures. 

==Cast==
*Victor Moore - Piffle
*Thomas Meighan - Dick Ordway
*Ernest Joy - Judge Jonathan Le Roy
*Florence Dagmar - Millicent, His Daughter
*Gerald Ward - Jackie, His Son Tom Forman - Bob Hunter
*Horace B. Carpenter - Circus Manager
*Wallace Pyke - Rollo
*Billy Jacobs - Jonathan Le Roy Fox

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 


 