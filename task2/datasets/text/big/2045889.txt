How to Be Loved
{{Infobox film
| name =How to be Loved
| image =How to be loved poster.jpg
| caption =
| director = Wojciech Has
| producer =
| writer = Kazimierz Brandys
| starring =Zbigniew Cybulski Barbara Krafftówna
| cinematography = Stefan Matyjaszkiewicz
| music = Lucjan Kaszycki
| editing =
| distributor = Zespół Filmowy Kamera
| released =  
| runtime = 97 min Polish
| budget =
}}
 Polish film released in 1963, directed by Wojciech Has.

The film, based on a novel of the same name by Kazimierz Brandys, examines the emotional casualties of war, which is perhaps the central theme of the Polish Film School. On a deeper level, the film manages to construct a personal tragedy that results from a struggle of egoism and cowardice versus devotion and courage. 

On a plane bound for Paris, Felicja (Barbara Krafftówna), a successful radio actress, recalls the night in 1939 when she was to debut as Ophelia (character)|Ophelia, with the man she loved, Wiktor (Zbigniew Cybulski), playing Hamlet. World War II intervenes, and Felicja takes a job as a waitress to avoid acting on a German stage, giving her lover sanctuary when hes accused of killing a collaborator. After the war, Wiktor cant get away fast enough, hot on the trail of fame and applause, and the woman who saved him is herself wrongly accused of collaboration. Years later, Wiktor and Felicja meet again, and the tables have turned.

The film was entered into the 1963 Cannes Film Festival.    

==Cast==
* Barbara Krafftówna - Felicja
* Zbigniew Cybulski - Wiktor Rawicz
* Artur Młodnicki - Tomasz
* Wieńczysław Gliński - Bacteriologist
* Wiesław Gołas - German Soldier
* Wiesława Kwaśniewska - Photographer
* Zdzisław Maklakiewicz - Journalist Zenon
* Tadeusz Kalinowski - Peters
* Mirosława Krajewska - Flight attendant

== See also ==
*Cinema of Poland
*List of Polish language films

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 
 