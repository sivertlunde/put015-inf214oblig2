Goubbiah, mon amour
{{Infobox film
| name           = Goubbiah, mon amour
| image          = 
| caption        = 
| director       = Robert Darène
| producer       = Consortium de Productions de Films
| writer         = René Barjavel Jean Martet
| starring       = Jean Marais Kerima
| music          = Joseph Kosma
| cinematography = 
| editing        = 
| distributor    = 
| released       = 14 March 1956 (France); 27 April 1956 (West Germany); 1959 (UK)
| runtime        = 96 minutes
| country        = Italy, France
| awards         = 
| language       = French
| budget         = 
}}
 French Romance romance Drama drama film from 1956, directed by Robert Darène, written by René Barjavel, starring Jean Marais. The scenario was based on a novel of Jean Martet.  The film was known under the title Cita en el Sol (Spain), Kiss of Fire (UK), Liebe unter heißem Himmel (West Germany). 

== Cast ==
* Jean Marais : Goubbiah
* Kerima : Carola
* Delia Scala : Trinida
* Charles Moulin : Jao – Trinidas Father
* Henri Nassiet : Goubbiahs Father
* Gil Delamare : Peppo – Trinidas Fiancee
* Marie-José Darène : Minnie
* Henri Cogan : The Scarface
* Louis Bugette : Erika

== References ==
 

== External links ==
*  
*   at the Films de France

 
 
 
 
 
 
 
 
 


 