Humorous Phases of Funny Faces
{{Infobox film
| name           = Humorous Phases of Funny Faces
| image          = Humorous Phases of Funny Faces.ogg
| director       = J. Stuart Blackton
| producer       = J. Stuart Blackton
| writer         =
| starring       = J. Stuart Blackton
| cinematography =
| editing        =
| distributor    = Vitagraph Company of America
| released       =  
| runtime        = 3 minutes
| country        = United States
| language       = silent
}} James Stuart Blackton and generally regarded by film historians as the first animated film recorded on standard picture film.   

==Content==
In the cartoon, animated hand-drawn scenes appear on a chalkboard, such as a clown playing with a hat and a dog jumping through a hoop. In the beginning the cartoonists hands are included, too, which are then left out, using stop-motion.

==Techniques==
  technique]]
Stop-motion as well as cutout animation are used, just as Edwin Porter moved his letters in How Jones Lost His Roll, and The Whole Dam Family and the Dam Dog. However, there is a very short section of the film where things are made to appear to move by altering the drawings themselves from frame to frame.

The film moves at 20 frames per second.
 

==See also==
* Fantasmagorie (1908 film)|Fantasmagorie
*History of animation

==References==
 

==External links==
* 
* 
*  at the Library of Congress
*  
 

 

 
 
 
 
 
 
 
 
 
 
 


 
 