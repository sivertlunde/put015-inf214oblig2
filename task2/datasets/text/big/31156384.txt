Masappady Mathupillai
{{Infobox film 
| name           = Masappady Mathupillai
| image          =
| caption        =
| director       = AN Thampi
| producer       = CK Sherif
| writer         = Veloor Krishnankutty
| screenplay     =
| starring       = Jayabharathi KPAC Lalitha Adoor Bhasi Jose Prakash
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = T&S Combines
| distributor    = T&S Combines
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by AN Thampi and produced by CK Sherif. The film stars Jayabharathi, KPAC Lalitha, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by G. Devarajan.   

==Cast==
 
*Jayabharathi
*KPAC Lalitha
*Adoor Bhasi
*Jose Prakash
*Manavalan Joseph
*Muthukulam Raghavan Pillai
*Alummoodan
*Bahadoor
*Kaduvakulam Antony
*Kaval Surendran Khadeeja
*Kunchan Kunchan
*Kuttyedathi Vilasini Meena
*Narayana Das
*Nellikode Bhaskaran
*Paravoor Bharathan
*S. P. Pillai Sudheer
*Vincent Vincent
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma, Kilimanoor Ramakanthan and Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ayalathe Chinnamma || CO Anto || Vayalar Ramavarma || 
|-
| 2 || Purushagandham Sthree || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Swarnamurukkiyozhichapole || P. Leela, P. Madhuri || Kilimanoor Ramakanthan || 
|-
| 4 || Zindabaad Zindabaad || PB Sreenivas || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 