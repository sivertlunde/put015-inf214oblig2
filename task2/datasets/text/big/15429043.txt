The Deerslayer and Chingachgook
 
{{Infobox film
| name           = The Deerslayer and Chingachgook
| image          =
| caption        =
| director       = Arthur Wellin
| producer       = Arthur Wellin
| writer         = James Fenimore Cooper (novel) Robert Heymann
| starring       = Béla Lugosi
| cinematography = Ernest Plhak
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = Weimar Republic Silent
| budget         =
| gross          =
}}
 The Last of the Mohicans (Der Letzte der Mohikaner).

==Preservation status==
Believed to be a lost film for many years, a print of Lederstrumpf, in its heavily edited US release version titled The Deerslayer, was eventually discovered, and became available on DVD in the 1990s.

==Cast==
* Emil Mamelok as Deerslayer
* Herta Heden as Judith Hutter
* Béla Lugosi as Chingachgook
* Gottfried Kraus as Tom Hutter
* Edward Eyseneck as Worley
* Margot Sokolowska as Wah-ta-Wah
* Frau Rehberger as Judith Hutter
* Willy Schroeder as Hartherz
* Herr Söhnlein as Col. Munro
* Heddy Sven as Cora Munro
* Frau Wenkhaus as Alice Munro

==See also==
* Béla Lugosi filmography
* List of rediscovered films

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 