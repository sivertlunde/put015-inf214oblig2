Hell on Frisco Bay
{{Infobox film
| name           = Hell on Frisco Bay
| image          = Hell on Frisco Bay - 1955 Poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Frank Tuttle
| producer       = 
| writer         = Martin Rackin
| based on       = novel by William McGivern
| narrator       = 
| starring       = Alan Ladd Edward G. Robinson
| music          = Max Steiner
| cinematography = John F. Seitz
| editing        = Folmar Blangsted Jaguar Productions
| distributor    = Warner Bros.
| released       = 1955
| runtime        = 98 minutes
| gross          = $2 million (US)  
| country        = United States
| language       = English
}}
Hell on Frisco Bay is a 1955 American crime film directed by Frank Tuttle, starring Alan Ladd, Edward G. Robinson and Joanne Dru. It was made for Ladds own production company, Jaguar.
 Long John Silver (1954). 

==Plot==
The film involves a former police officer (Ladd) seeking revenge after being falsely imprisoned for a murder he did not commit
in San Quentin for five years.  He returns and hunts the San Francisco waterfront for the Mob racketeers who are responsible

==Cast==
* Alan Ladd as Steve Rollins
* Edward G. Robinson	as Victor Amato
* Joanne Dru	as Marcia Rollins
* William Demarest as Dan Bianco Paul Stewart as Joe Lye
* Perry Lopez as Mario Amato
* Fay Wray as Kay Stanley
* Renata Vanni as Anna Amato
* Nestor Paiva as Louis Fiaschetti Stanley Adams as Hammy
* Willis Bouchey as Police Lt. Paul Neville Peter Hansen as Detective Connors Anthony Caruso as Sebastian Pasmonick
* George J. Lewis as Father Larocca
* Tina Carver as Bessie Coster

==Production notes==
The working titles of the film were The Darkest Hour and Hell on the Dock. William P. McGiverns novel, The Darkest Hour, was serialized in Colliers (15 April-13 May 1955). EVA LE GALLIENNE WILL ACT IN FILM: Stage Leader, Consultant on Prince of Players, Set for Debut on Screen
By THOMAS M. PRYORSpecial to The New York Times. New York Times (1923-Current file)   21 Aug 1954: 10.  

Production Dates: 4 Apr—mid-May 1955.  Much of the film was shot on location throughout San Francisco, CA. Extensive shooting was done in and around the Fishermans Wharf and San Francisco Bay.

==References==
 

==External links==
*   in the Internet Movie Database

 
 
 
 
 
 

 