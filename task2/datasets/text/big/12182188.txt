Back Street (1932 film)
{{Infobox film
| name           = Back Street
| image          = Back Street 1932.jpg
| caption        = Theatrical release poster
| director       = John M. Stahl
| producer       = Carl Laemmle Jr. E.M. Asher  (associate producer)
| writer         = Gladys Lehman Fannie Hurst (novel) Gene Fowler  Ben Hecht  Lynn Starling John Boles
| music          = James Dietrich
| cinematography = Karl Freund 
| editing        = Milton Carruth
| distributor    = Universal Pictures
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} novel by John Boles.

==Plot== 1941 and Back Street (1961 film)|1961.

==Cast==
* Irene Dunne as  Ray Smith  John Boles as  Walter Saxel 
* June Clyde as  Freda Schmidt 
* George Meeker as  Kurt Shendler 
* ZaSu Pitts as  Mrs. Dole 
* Shirley Grey as  Francine 
* Doris Lloyd as  Saxels wife 
* William Bakewell as  Richard Saxel 
* Arletta Duncan as  Beth Saxel 
* Maude Turner Gordon as  Mrs. Saxel Sr 
* Walter Catlett as  Bakeless 
* James Donlan as  Profhero 
* Paul Weigel as  Mr. Schmidt 
* Jane Darwell as  Mrs. Schmidt 
* Robert McWade as  Uncle Felix

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 


 