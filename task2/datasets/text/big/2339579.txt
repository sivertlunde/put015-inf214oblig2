Fat City (film)
{{Infobox film
| name           = Fat City
| image          = Fat_City_DVD_cover.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = John Huston
| producer       = Ray Stark John Huston
| screenplay     = Leonard Gardner
| based on       =  
| starring       = Stacy Keach Jeff Bridges Susan Tyrrell Candy Clark
| music          = Kenneth Hall
| cinematography = Conrad L. Hall Walter Thompson
| studio         = Rastar
| distributor    = Columbia Pictures
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Fat City is a 1972 American neo-noir boxing drama film directed by John Huston. The picture stars Stacy Keach, Jeff Bridges, and Susan Tyrrell. 
 Fat City (1969) by Leonard Gardner, who also wrote the screenplay.
 Oscar nomination as the alcoholic, world weary Oma.

==Plot==
 
Billy Tully (Keach), a boxer past his prime, goes to a Stockton, California gym to get back into shape and spars with Ernie Munger (Bridges), an eighteen-year-old he meets there. Seeing potential in the youngster, Tully suggests that Munger look up his former manager and trainer, Ruben (Nicholas Colasanto). Tully later tells combative barfly Oma (Tyrrell) and her easygoing boyfriend Earl (Curtis Cokes) how impressed he is with the kid. Newly inspired, Tully decides to get back into boxing himself.

Tullys life has been a mess ever since his wife left him. He drinks too much, cannot hold down a job, and picks fruit and vegetables with migrant workers to make ends meet. He still blames Ruben for mishandling his last fight.

Tully tries moving in with Oma after Earl is sent to prison for a few months, but their relationship is rocky. Earl, still paying the rent, returns to assure him that the alcoholic Oma wants nothing more to do with Tully.

Munger loses his first fight, his nose broken, and is knocked out in his next bout as well. He gets pressured into marriage by Faye (Candy Clark) because a babys on the way, so he picks fruit in the fields for a few dollars.

For his first bout back, Tully is matched against a tough Mexican boxer named Lucero (Sixto Rodriguez), who is also of an advanced age and in considerable pain. They knock each other down before Tully is declared the winner. His celebration is brief when Tully discovers that he will be paid only $100. He ends his business relationship with Ruben and goes back to his old ways.

Munger is returning home from a fight one night when he sees Tully in the street, drunk. Munger tries to ignore him, but when Tully asks to have a drink, he reluctantly agrees to coffee. The two men sit and drink and Tully looks around at all the people immediately around him, all of whom now seem at an impassable distance. Munger says he needs to leave, but Tully asks him to stay and talk a while. Munger agrees, and the two men sit drinking their coffee together in silence.

==Cast==
* Stacy Keach as Billy Tully
* Jeff Bridges as Ernie Munger
* Susan Tyrrell as Oma
* Candy Clark as Faye
* Nicholas Colasanto as Ruben
* Art Aragon as Babe
* Curtis Cokes as Earl. Ironically, though Cokes was a real-life world welterweight champion, he did not play a boxer.
* Sixto Rodriguez as Lucero
* Alfred Avila as Luceros coach
* Billy Walker as Wes
* Wayne Mahan as Buford
* Ruben Navarro as Fuentes

==Production==
Like the novel, the film was set in Stockton, California and shot mostly on location there. All of the original skid row area depicted in the novel was demolished (West End Redevelopment) from 1965-69. Most of the skid row scenes were filmed in the outer fringe of the original skid row area which was torn down a year after Fat City was filmed, in order to make way for the construction of the Crosstown Freeway, aka "Ort Lofthus Freeway".

The drama is featured in the   (1992) for Conrad L. Halls use of lighting. 

The melancholic "Help Me Make It Through the Night" is sung by Kris Kristofferson at the beginning and end of the movie.

==Meaning of Title==
In a 1969 interview with LIFE Magazine, Leonard Gardner explained the meaning of the title of his novel.
 "Lots of people have asked me about the title of my book. Its part of Negro slang. When you say you want to go to Fat City, it means you want the good life. I got the idea for the title after seeing a photograph of a tenement in an exhibit in San Francisco. Fat City was scrawled in chalk on a wall. The title is ironic: Fat City is a crazy goal no one is ever going to reach."     Fat City is also an old nickname for Stockton, California, where the novel and film are set.  The nickname preceded Gardners novel.

==Distribution==
The film premiered in the United States on July 26, 1972.

The film was screened at various film festivals, including: the Cannes Film Festival, France, the Palm Springs International Film Festival, USA; and others.

==Reception==

===Critical response===
After a string of box office flops, John Huston rebounded with this film, which opened to tremendous praise and good business: he was soon in demand for more work.
Vincent Canby, film critic for  The New York Times, liked the film and Hustons direction. He wrote, "This is grim material but Fat City is too full of life to be as truly dire as it sounds. Ernie and Tully, along with Oma (Susan Tyrrell), the sherry-drinking barfly Tully shacks up with for a while, the small-time fight managers, the other boxers and assorted countermen, upholsterers, and lettuce pickers whom the film encounters en route, are presented with such stunning and sometimes comic accuracy that Fat City transcends its own apparent gloom." 

Roger Ebert made the case for it as one of Hustons best films.  He also appreciated the performances.  Ebert wrote, "  treats   with a level, unsentimental honesty and makes it into one of his best films...  the movies edges are filled with small, perfect character performances." 

J. Hoberman of the Village Voice wrote, "The movie is crafty work and very much a show. In one way or another, right down to the percussively abrupt open ending, its all about being hammered." 

Dave Kehr of the Chicago Reader wrote, "John Hustons 1972 restatement of his theme of perpetual loss is intelligently understated." 

Film critic Dennis Schwartz wrote, "The downbeat sports drama is a marvelous understated character study of the marginalized leading desperate lives, where they have left themselves no palpable way out. The stunning photography by Conrad Hall keeps things looking realistic." 

In 2009, Fat City enjoyed a week-long revival screening at New York Citys Film Forum. 

It has a 100% fresh rating on Rotten Tomatoes, based on seventeen reviews. 

===Awards===
Wins
*   for The Godfather); 1972.
*  ; 1974.

Nominations
*  , Susan Tyrrell; 1973.

====New York Film Critics Circle====
Under the then-extant rules, Stacy Keach should have been awarded Best Actor honors from the New York Film Critics Circle for his portrayal of Tully, as it required only a plurality of the vote. Keach was the top vote-getter for Best Actor. At the time, the NYCC was second in prestige only to the Academy Awards and was a major influence on subsequent Oscar nominations. A vocal faction of the NYFCC, dismayed by the rather low percentage of votes that would have given Keach the award, successfully demanded a rule change so that the winner would have to obtain a majority. In subsequent balloting, Keach failed to win a majority of the vote, and he lost ground to his main rival, Marlon Brando in The Godfather. However, Brando could not gain a majority either. A compromise candidate, Laurence Olivier in Sleuth (1972 film)|Sleuth eventually was awarded Best Actor honors. Coincidentally, director John Huston had initially wanted Brando to play the role of Tully. When Brando informed Huston repeatedly that he needed some more time to think about it, Huston finally came to the conclusion that the star wasnt really interested and looked out for another actor until he finally cast the then relatively unknown Keach. 

==References==
 

==External links==
*  
*  
*  
*  
*   at Noir of the Week by noir historian William Hare

 

 
 
 
 
 
 
 
 
 
 
 