Pirates of the Caribbean: The Curse of the Black Pearl
 
 
 
{{Infobox film
| name           = Pirates of the Caribbean:  The Curse of the Black Pearl
| image          = Pirates of the Caribbean movie.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gore Verbinski
| producer       = Jerry Bruckheimer Pirates of the Caribbean
| screenplay     = {{Plainlist| Ted Elliott
* Terry Rossio}}
| story          = {{Plainlist|
* Ted Elliott
* Terry Rossio
* Stuart Beattie
* Jay Wolpert}}
| starring       = {{Plainlist| 
* Johnny Depp
* Geoffrey Rush
* Orlando Bloom
* Keira Knightley}}
| music          = Klaus Badelt
| cinematography = Dariusz Wolski
| editing        = {{Plainlist|
* Stephen E. Rivkin Arthur Schmidt Craig Wood}}
| studio         = {{Plainlist|
* Walt Disney Pictures
* Jerry Bruckheimer Films}} Buena Vista Pictures
| released       =  
| runtime        = 143 minutes  
| country        = United States
| language       = English
| budget         = $140 million   
| gross          = $654.3 million 
}} fantasy swashbuckler Pirates of Disney theme pirate Jack Captain Jack Sparrow (Johnny Depp) and blacksmith Will Turner (Orlando Bloom) as they rescue the kidnapped Elizabeth Swann (Keira Knightley) from the cursed crew of the Black Pearl, captained by Hector Barbossa (Geoffrey Rush), who become undead skeletons at night.
 Ted Elliott and Terry Rossio work on the script, adding the supernatural curse to the storyline.  Filming took place from October 2002 to March 2003 in Saint Vincent and the Grenadines and on sets constructed around Los Angeles, California.

The world premiere was held at   and  , released in 2006 and 2007. A fourth film,  , was released in 2011 and a fifth film,  , is scheduled for release in 2017.

The film received positive reviews from the critics and was an unexpected success, grossing over $654 million worldwide. Johnny Depps portrayal of Jack Sparrow was universally praised, winning him the Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Leading Role and earned him nominations for the Academy Award for Best Actor, BAFTA Award for Best Actor in a Leading Role, and Golden Globe Award for Best Actor – Motion Picture Musical or Comedy. The Curse of the Black Pearl was also nominated for four other Academy Awards and BAFTAs.

==Plot== Lieutenant James Norrington encounter a burning shipwreck and an unconscious young boy, Will Turner. Elizabeth conceals a gold medallion worn by Will, fearing it will identify him as a pirate. Elizabeth then glimpses a ghostly pirate ship, the notorious Black Pearl, sailing away.
 Commodore Norrington proposes marriage to Elizabeth. However, her over-tightened corset causes her to faint before she can answer, and she falls from the fort into the bay. The gold medallion she wears as a necklace emits a pulse in the water which signals the Black Pearl. She is saved by pirate Jack Sparrow, who is in Port Royal to steal a ship. Norrington recognizes Jack as a pirate and orders his arrest. Jack attempts to escape, but runs into an adult Will. After an evenly matched sword fight with Will, soldiers arrive and capture Jack. He is jailed to await his execution.
 Captain Barbossa stops the attack on Port Royal, but keeps Elizabeth prisoner.

Will, who is in love with Elizabeth, breaks Jack out of prison and persuades him to help rescue her. Jack agrees after learning that Wills surname is Turner. Will and Jack commandeer HMS Interceptor and recruit a crew in Geography of Pirates of the Caribbean#Tortuga|Tortuga. With help from Jacks old friend, Joshamee Gibbs, they set sail for the remote Isla de Muerta, where the Pearl ports.
 William "Bootstrap Bill" Turner, Jacks only supporter during the mutiny, sent a coin to his son, Will, believing the crew should remain cursed. Barbossa had Bootstrap tied to a cannon and thrown overboard, before realizing that his blood was needed to break the curse.

At Isla de Muerta, Barbossa, believing Elizabeth is Bootstraps child, anoints the last coin with her blood, but the curse remains. After reaching the island, Will suspects Jack may betray him and knocks him out. Will rescues Elizabeth and they escape to the Interceptor, leaving Jack behind. Jack barters with Barbossa, offering Will in exchange for the Pearl, but Barbossa rejects his offer and pursues Interceptor, sinking the ship and imprisoning its crew. Will reveals that he is Bootstraps son and demands that Elizabeth and the crew be freed, or he will shoot himself and fall overboard. Barbossa agrees, but maroons Elizabeth and Jack on the same island Jack had been left on ten years earlier. Elizabeth discovers how Jack escaped before: the island was used as a cache by rum runners and Jack bartered passage. However, the rum runners have long since abandoned the island, making another rescue impossible.

Elizabeth burns the cache of rum to create a signal that Norringtons ship spots. She convinces Norrington to rescue Will by accepting his marriage proposal. Norrington locks Elizabeth in his cabin in order to keep her safe. Returning to Isla de Muerta, Norrington sets an ambush while Jack persuades Barbossa to form an alliance, telling him to delay breaking the curse until they have taken Norringtons ship, HMS Dauntless. Jacks plan goes awry when Barbossa orders his undead crew to infiltrate the Dauntless from underwater. Meanwhile, Elizabeth escapes and sneaks aboard the Pearl to free Jacks crew. They leave with the Pearl while Elizabeth heads to the island alone to save Will. Jack duels with Barbossa, and is seemingly killed. Barbossa is then shocked to discover that Jack is also immortal after having taken one of the medallions while attempting to negotiate with him.

Meanwhile, Norrington and his men fight the cursed pirates. When Barbossa attempts to kill Elizabeth, Jack shoots Barbossa as Will drops the last two medallions, stained with his and Jacks blood, into the chest. Now mortal, Barbossa collapses and dies. The remaining cursed pirates&nbsp;— heavily outnumbered and also now mortal&nbsp;— aboard Dauntless surrender. Despite the victory, Jack is arrested and condemned to death.
 A Pirates Life for Me".

In a post-credits scene, Jack, Barbossas pet monkey, steals a gold coin from the chest, thus cursing itself once again.

==Cast==
See List of Pirates of the Caribbean characters.
*  , just the cocky pirate." At the first  .  Although Verbinski and Bruckheimer had confidence in Depp, partly because it would be Bloom who was playing the traditional Errol Flynn-type,  Disney executives were confused, asking Depp whether the character was drunk or gay, and Michael Eisner even proclaimed while watching Dailies|rushes, "Hes ruining the film!"  Depp answered back, "Look, these are the choices I made. You know my work. So either trust me or give me the boot."  Captain Hector Barbossa; Verbinski approached Rush for the role of Barbossa, as he knew he could hint at the subtle complexities of the character while still portraying a simple villainy that would suit the storys tone.    Ned Kelly, suggested it to him. 
* Keira Knightley as Elizabeth Swann; Knightley came as a surprise to Verbinski; he had not seen her performance in Bend It Like Beckham and was impressed by her audition.  Commodore James Norrington: A commanding Royal Navy officer at Port Royal, and Elizabeths fiancee. Governor Weatherby Swann; Tom Wilkinson was negotiated with to play the part,  but the role went to Pryce, whom Depp idolized. 
* Kevin McNally as Joshamee Gibbs: Jack Sparrows friend and first mate, he was once a sailor for the Royal Navy.
* Zoe Saldana as Anamaria: A female pirate who signs up to join Will Turner and Mr. Gibbs for a chance to confront Jack Sparrow for stealing her ship.
* Lee Arenberg as Pintel and Ragetti|Pintel: A pirate aboard the Black Pearl who, with Ragetti (see below), serves as comic relief for most of the film. He and Ragetti dress up as women to provide the distraction that allows the cursed pirates to board the Dauntless near the end of the movie.
* Mackenzie Crook as Pintel and Ragetti|Ragetti: A pirate aboard the Black Pearl, Pintels buddy, with a wooden eye that never seems to stay in place. Lieutenant Gillette: The second-in-command to Commodore Norrington.
* Treva Etienne as List of Pirates of the Caribbean characters#Koehler|Koehler: One of Barbossas crew.
* Michael Berry Jr. as List of Pirates of the Caribbean characters#Twigg|Twigg:
* David Bailie as List of Pirates of the Caribbean characters#Cotton|Cotton: A sailor who had his tongue cut out, is now mute and has a macaw to talk for him.
* Christopher S. Capp as Mr. Cottons Parrot.
* Martin Klebba as List of Pirates of the Caribbean characters#Marty|Marty: A dwarf pirate who also lived in Tortuga until hired by Jack and Will to rescue Elizabeth.
*   during the battle of Isla de Muerta.
* Giles New as  . He serves under the command of Commodore Norrington.
* Angus Barnett as List of Pirates of the Caribbean characters#Murtogg and Mullroy|Mullroy: A dutiful but daft Royal Marine. Greg Ellis as Lieutenant Theodore Groves: a lieutenant who admires Sparrow, to the ire of Norrington.

==Production==
===Development=== Ted Elliott and Terry Rossio began to think of a supernatural spin on the pirate genre.    Disney had Jay Wolpert write a script based on the ride in 2001, which was based on a story created by the executives Brigham Taylor, Michael Haynes, and Josh Harmon. This story featured Will Turner as a prison guard who releases Sparrow to rescue Elizabeth, who is being held for ransom money by Captain Blackheart. The studio was unsure whether to release the film in theaters or direct-to-video. The studio was interested in Matthew McConaughey as Sparrow because of his resemblance to Burt Lancaster, who had inspired that scripts interpretation of the character. If they chose to release it direct-to-video, Christopher Walken or Cary Elwes would have been their first choices.    Stuart Beattie was brought in to rewrite the script in March 2002, because of his knowledge of piracy.   

When Dick Cook managed to convince producer Jerry Bruckheimer to join the project,  he rejected the script because it was "a straight pirate movie."    Later in March 2002, he brought Elliott and Rossio,  who suggested making a supernatural curse – as described in the opening narration of the ride – the films plot.    In May 2002, Gore Verbinski signed on to direct Pirates of the Caribbean.  He was attracted to the idea of using modern technology to resurrect a genre that had disappeared after the Golden Age of Hollywood and recalled his childhood memories of the ride, feeling the film was an opportunity to pay tribute to the "scary and funny" tone of it. 
 The Lord The Matrix. Eisner concurred, but with the stigma attached to theme-park adaptations, Eisner requested Verbinski and Bruckheimer remove some of the more overt references to the ride in the script, such as a scene where Sparrow and Turner enter the cave via a waterfall.   

====Influence of the Monkey Island series of games====
Ted Elliott was allegedly writing a Steven Spielberg-produced animated film adaptation of The Curse of Monkey Island, which was cancelled before its official announcement, three years prior to the release of The Curse of the Black Pearl.  This film was allegedly in production at Industrial Light and Magic before being cancelled.

 , for its similarities to his game.  Gilbert has also stated that  , was the principal source of inspiration for his video games. 

===Filming and design=== Manhattan Beach.  A fire broke out in September 2002, causing $525,000 worth of damage, though no one was injured. 

 
The filmmakers chose  , the HMS Dauntless (Pirates of the Caribbean)|Dauntless, and the Interceptor. For budget reasons, the ships were built on docks, with only six days spent in the open sea for the battle between the Black Pearl and the Interceptor.  The Dauntless and the Black Pearl were built on barges, with computer-generated imagery finishing the structures. The Black Pearl was also built on the Spruce Goose stage, in order to control fog and lighting.  The Interceptor was a re-dressed Lady Washington, a full-scale replica sailing ship from Aberdeen, Washington, fully repainted before going on a 40-day voyage beginning December 2, 2002, arriving on location on January 12, 2003.  A miniature was also built for the storm sequence. 

 , and in January they were at the cavern set at Los Angeles.  The script often changed with Elliott and Rossio on set, with additions such as Gibbs (Kevin McNally) telling Will how Sparrow allegedly escaped from an island – strapping two turtles together with rope made of his back hair – and Pryce was written into the climactic battle to keep some empathy for the audience. 

Because of the quick schedule of the shoot,   stage.  With the shoot only wrapping up four months before release, Verbinski spent 18-hour days on the edit,  while at the same time spending time on 600 effects shots, 250 of which were merely removing modern sailboats from shots. 

===Music===
 
Verbinski managed the score with Klaus Badelt and Hans Zimmer, who headed 15 composers to finish it quickly.  Alan Silvestri, who had collaborated with Verbinski on Mouse Hunt and The Mexican, was set to compose the score, but Bruckheimer decided to go with Zimmers team instead, who were frequent collaborators of his productions. Silvestri left the production before recording any material. 

===Rating===
This was the first Walt Disney Pictures to be rated PG-13 by the MPAA; one executive noted that she found the film too intense for her five-year-old child.  Nonetheless, the studio was confident enough to add The Curse of the Black Pearl subtitle to the film in case sequels were made,  and to attract older children. Verbinski disliked the new title because it is the Aztec gold rather than the ship that is cursed, so he requested the title to be unreadable on the poster. 

==Reception==
===Box office===
The film was a big success. However, before its release, many journalists expected Pirates of the Caribbean to be a flop. The pirate genre had not been successful for years, with Cutthroat Island (1995) a notable flop. The film was also based on a theme park ride, and Johnny Depp, known mostly for starring in cult films, had little track record as a box office leading man. 

Pirates of the Caribbean: The Curse of the Black Pearl opened at #1, grossing $46,630,690 in its opening weekend and $70,625,971 since its Wednesday launch. It eventually made its way to $654,264,015 worldwide ($305,413,918 domestically and $348,850,097 overseas), becoming the fourth-highest-grossing film of 2003. 
 The Smurfs (with eight consecutive #1 weekends).  It is currently the 71st-highest-grossing film of all time. 

===Critical reception=== Kenneth Turans negative review, feeling it "spends far too much time on its huge supporting cast of pirates (nowhere near as entertaining as everyone assumes) and on bloated adventure set pieces," despite having also enjoyed Depps performance. 

===Accolades===
  Best Actor Golden Reel Award for Sound Editing, two VES Awards for Visual Effects, and the Peoples Choice Awards|Peoples Choice Award for Favorite Motion Picture. 

; American Film Institute Lists
* AFIs 100 Years...100 Movies (10th Anniversary Edition)—Nominated 
* AFIs 10 Top 10#Fantasy|AFIs 10 Top 10 – Fantasy—Nominated 

==Home media==
The DVD and VHS editions of the film were released five months after the theatrical release, December 2, 2003,     with 11 million copies sold in the first week, a record for live action video.  It earned $235,300,000 from DVDs as of January 2004.  The DVD featured two discs, featuring three commentary tracks (Johnny Depp and Gore Verbinski; Jerry Bruckheimer, Keira Knightley and Jack Davenport; and the screenwriter team), various deleted scenes and documentaries, and a 1968 Disney anthology television series|Disneyland episode about the theme park ride.  A special three-disc edition  was released in November 2004. 

A PSP release of the film followed on April 19, 2005.  The high-definition Blu-ray Disc version of the film was released on May 22, 2007.  This movie was also among the first to be sold at the iTunes music store.  The Curse of the Black Pearl had its UK television premiere on Christmas Eve 2007 on BBC One at 20:30.  It was watched by an estimated 7 million viewers. 

==Sequels==
The film spun off three sequels, with a fourth sequel set to be released in 2017. The first two were   and  , respectively. The third sequel,  , was released in 2011. The fourth sequel was revealed to be called,  .          Production is slated to begin in October 2014 and was scheduled for a summer 2016 release,  but was eventually delayed to 2017.  It is to be directed by Joachim Rønning and Espen Sandberg.   

==References==
 

==External links==
 
 
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 