Rollercoaster (1977 film)
{{Infobox film
| name           = Rollercoaster
| image          = RollercoasterFilmPoster.jpg
| caption        = Promotional poster of Rollercoaster
| writer         = Richard Levinson William Link Tommy Cook (story)
| starring       = George Segal Richard Widmark Timothy Bottoms Harry Guardino Susan Strasberg Richard Widmark
| music          = Lalo Schifrin
| cinematography = David M. Walsh
| director       = James Goldstone
| producer       = Jennings Lang Universal Pictures
| released       =  
| country        = United States
| runtime        = 119 min. (theatrical)
| language       = English
| budget         = $9 million 
}}
Rollercoaster is a 1977 disaster film|disaster-suspense film starring George Segal, Richard Widmark, Henry Fonda and Timothy Bottoms, and directed by James Goldstone. It was one of the few films to be shown in Sensurround, which caused audience seats to vibrate during certain periods during the "thrill scenes" on the rides.

==Plot==
A psychopathic bomber (Timothy Bottoms, credited only as "Young Man" and never referred to by any name in the film) sneaks into Ocean View Amusement Park and places a small radio-controlled bomb on the tracks of the parks wooden roller coaster, The Rocket. The bomb detonates, causing the ride to derail, which kills several riders. Harry Calder (George Segal), a ride inspector who is trying to kick his smoking habit, is called to the park to investigate what initially appears to have been an accident. Calder determines that there was an unauthorized man on the track, thus eliminating structural problems as the cause of the crash.

Meanwhile, the bomber flies to Pittsburgh where he sets a dark ride at another park on fire, although everyone escapes without injuries. Unable to discover any more information from the parks, Calder heads to Chicago where the heads of the parks are having a meeting. The room where the meeting is held has been bugged by the bomber, who has disguised himself as a bell boy. Calder demands to be let into the meeting, then one of the Parks heads plays a tape the Young Man has sent, wherein he demands $1 million to stop his bombing campaign.
 Rebel Yell, the parks racing wooden coaster.

When he gets on the Gondola lift|Skyway, the Young Man tells Calder that the bomb is in his radio but he must not throw it away because it will explode on impact, and the paths below the Skyway are crowded with patrons. Calder is then ordered to falsely signal that he has made the delivery once leaving the Skyway, in order to distract the FBI who are watching him. The bomber tells Calder to leave the money on a bench for the Young Man in Hanna-Barbera Land. When the Young Man fails to respond on the radio, Calder turns around to see the bag gone. After a heated argument with Hoyt and the discovery that Hoyt marked the money (violating the Young Mans condition that he be paid with unmarked bills), Calder demands to be sent home leaving the bomb squad to take care of the radio-bomb.
 The Great Magic Mountain as his next target, because it is one of the coasters that Calder personally inspected and cleared. Calder assumes that the Young Man will take revenge on both the FBI and Calder. Though Hoyt and the other FBI agents dont accept Calders theory, they decide to go there anyway to investigate because the ride is scheduled to debut on July 4, when park attendance will be at its highest for the season. Agents disguised as park maintenance men find a bomb attached to the tracks. They are able to cut the bombs antenna just moments before the Young Man realizes that his bomb has been discovered and tries to detonate the explosives.

The bomber returns to his car and gets a new bomb just as the Revolution is about to open. In order to get on board, he pays $100 for a "Gold Ticket" which entitles him to be one of the first trainload of riders. He places the new bomb under his seat in the back of Revolutions train. As the riders get off, Calder recognizes the Young Mans voice during his ride exit interview with a reporter. He immediately chases the bomber, and alerts the agents that the bomber might have placed something in train itself. However, it is too late to stop the train since it has already passed the chain lift on its second ride through.

Intrigued by Calders intelligence, the cornered bomber threatens to blow up the ride, holding the detonator in his hand while the agents try to jam the signal. The Young Man demands a firearm, and Calder takes one from an agent and begins to hand it to him. The FBI succeed in jamming the detonators signal, and alert Calder. Calder retains the gun and shoots the Young Man, who then runs away. He hops a fence into the field below the Revolution, running blindly, not realizing he is going in a circle that leads him right back to Calder. The bomber climbs onto the track of the Revolution and prepares to escape, but sees Calder and freezes. Distracted, the Young Man is hit and killed by the coaster. The film ends with Calder requesting a cigarette and a light from a stranger, but then drops them on the ground, deciding not to smoke, as the ride re-opens following the accident.

==Cast==
*George Segal as Harry Calder
*Richard Widmark as Agent Hoyt
*Timothy Bottoms as Young Man
*Henry Fonda as Simon Davenport
*Harry Guardino as Keefer
*Susan Strasberg as Fran
*Helen Hunt as Tracy Calder

==Production== Battlestar Galactica (1978).
 Sparks for Big Beat. Kiss who turned it down. Sparks later cited their appearance in the film when asked about the biggest regret of their career.   

===Casting===
Helen Hunt, in her first feature film role, has the supporting role as Tracy Calder, Harrys teenage daughter and a potential victim of the young man. Steve Guttenberg, also in his first brief film role, plays a messenger at Six Flags Magic Mountain who brings the plans for the Revolution to Calder and Hoyt. Craig Wasson, in his second film, appears as a hippie who sits in the car with the bombers second bomb at Magic Mountain.

Radio announcer Charlie Tuna also appears in the film as the MC for the concert and the Revolution coaster launch.

===Locations===
Several real amusement parks were used in the film for the various park and roller coaster scenes. Ocean View Park in Norfolk, Virginia was used for the first park in the film. Although the park was located on the Chesapeake Bay, it was described as a west coast park in the film. Goldstone chose the park because of its old fashioned feeling. The parks major wooden roller coaster, The Southern Belle, was renamed "The Rocket" for the film. Kings Dominion in Doswell, Virginia, also appeared in the film. Some of the rides featured in the film, such as the Shenandoah Lumber Company and Rebel Yell wooden roller coaster, still exist today. The final park is Six Flags Magic Mountain in Valencia, California, then just known as "Magic Mountain" (it hadnt yet been purchased by the Six Flags organization). Its Revolution roller coaster was featured prominently in the films climax.
 Kennywood Park was one of the locations to be used in the film.  After the parks owners reviewed the script, they objected.  In the end, the parks name was changed to "Wonderworld" and did not appear in the final version of the film.

According to Goldstone, the three parks that appeared in the final film were chosen from over 20 candidates.

Additionally, parts of the movie were filmed at the Hyatt Regency Chicago.

==Soundtrack==
 
The films musical score was written by veteran composer Lalo Schifrin. 

==Reception==
Despite having been released in the summer of 1977, and being overlooked in favor of the smash hit Star Wars, it went on to be a moderate success at the box office. Reviews were mixed, however.  Time Out London felt that the results should have been sensational, but that, ultimately the film-makers botched the job. Many of the best runs are interrupted by close-ups, and the filler plot is dumb in the extreme.  Variety (magazine)|Variety said that, The rollercoaster rides are the picture’s highlights and they are fabulous. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 