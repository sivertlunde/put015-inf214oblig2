Ultra Warrior
{{Infobox Film name           = Ultra Warrior image          = Ultra Warrior.jpg image_size     = 150px caption        = Theatrical release poster director       = Augusto Tamayo San Román   Kevin Tent producer       = Mike Elliott   Luis Llosa   Roger Corman writer         = Len Jenkin   Dan Kleinman starring       = Dack Rambo Clare Beresford Meshach Taylor music          = Kevin Klinger cinematography = Cusi Barrio editing        = Augusto Tamayo San Román   Dan Schalk distributor    = Concorde Pictures released       =   March 16, 1990 runtime        = 80 minutes country        =       Peru language  English
|budget         =  gross          = $410,880   followed_by    = 
}}

Ultra Warrior, also known as Welcome to Oblivion, is a Apocalyptic and post-apocalyptic fiction|post-apocalyptic 1990 film directed by Augusto Tamayo San Román and Kevin Tent. Set in a futuristic dystopia, a nuclear holocaust creates mutants who inhabit the radioactive areas. One man emerges as the leader of a group of survivors called "Muties". The main character, portrayed by Dack Rambo, is looking for zirconium which is used to make bombs to prevent aliens from destroying Earth.   
 B list film is notable for its excessive use of stock footage and its poor quality. Some footage comes from other New Horizons films such as Lords of the Deep, Crimezone, Battletruck, Battle Beyond the Stars, Wheels of Fire and Dune Warriors.     In fact, even a sex scene comes from stock footage, as the character in the scene has long, blond hair, while Dack Rambo’s hair is dark and short. 

The Concorde theatrical release was filmed in Peru and was later cut up for the VHS release.   

==Plot==
A failed space based defense system kills millions in a nuclear disaster. In the subsequent chaos, the wealthy developed cities run by brutal military fascists, while the poor roam the radioactive deserts created by the nuclear fallout. Many turn to extreme violence and slavery to survive. The weaker mutated victims of these violent bandits are called Muties.  

In 2058 Kansas City, Rudolph Kenner is staying at a casino/strip bar when he receives a message notifying him that a zirconium mine on Mars was lost to rebel forces. He is ordered to complete his mission and to take care of his trainee, Phil, the son of a zirconium business leader. The next morning, Kenner, Phil, and Phil’s girlfriend Sheila, head east to Oblivion, a desolate area once known as the Atlantic seaboard. 

Kenner finds a young Mutie stuck in a trap and returns her to her colony. While he is there bandits attack the colony, killing Phil and capturing Kenner and Sheila. Kenner is taken to a bandit base, the Colosseum. He is brought before Bishop, the leader of the bandits, who knows Kenner’s mission and plans to kill him during a gladiator battle. Fortunately, Kenner escapes from the base with a group of Muties in a bandits’ car.

Later, Kenner explains to Grace, one of the Muties, about his mission. He is to take radioactivity readings to see if they can zirconium around Oblivion. The precious mineral is important in fighting aliens from a parallel universe who transforms planets into stars. To thwart them, scientists developed a zirconium bomb which was used to destroy an alien starship. Kenners mission is to find more zirconium. When Kenner asks about Grace’s family, she explains her parents were eaten by subterranean, cannibalistic mutants called “Whities”.  

Eventually, Kenner and the Muties make their way back to camp, which is soon attacked by bandits. However, Kenner sits in the open and draws the bandits into the Mutie shelter. Meanwhile, the Muties hijack the bandits’ vehicles and Kenner escapes. The Muties demolish their shelter, crushing the bandits inside. They capture Elijah, a stranger who was helping the bandits and who Kenner encountered earlier. 

The Mutie spiritual leader, Uncle Lazarus, puts Kenner in charge and reveals that he is part of an unfolding prophesy. Later, Uncle Lazarus kills himself. Initially, Kenner is reluctant about his new role but soon adapts to it.

The Bishop calls the bandit patrol over on one of the vehicles radio and Kenner answers back, pretending to be a bandit. He then decides to take the hijacked convoy to the Colosseum disguised as a patrol. Later, Kenner and Grace talk about the world Kenner comes from and the two make love.

The band of Muties arrive at the Colosseum, but Elijah runs screaming out of the convoy and is shot. A firefight errupts between the Muties and the bandits. Kenner finds Sheila along with other prisoners, and then searches for the Bishop. Kenner and the Muties are ambushed by the bandits. They finally corner Bishop and discover he is actually a Mutie himself. During this confrontation, the Bishop escapes.

In the end, Kenner writes back to his employers stating the conditions in Oblivion are too radioactive to mine, so they would have to buy the zirconium from the resident Muties. Grace tells Kenner she does not feel at home in the city, and Kenner agrees. The film ends with them driving back into Oblivion.

==Cast==
*Dack Rambo&nbsp;– Kenner, the main character who initially searches for zirconium but later becomes the leader of the Muties.
* Clare Beresford&nbsp;– Grace, a Mutie who helps Kenner and is also his love interest.
*Meshach Taylor&nbsp;– Elijah, an eccentric character who first warns Dack about Oblivion. He was captured by the bandits and helped them before being captured by the Muties.
* Mark Bringelson&nbsp;– Big, one of the Muties helping Kenner.
* Charles Dougherty&nbsp;– Zig, another one of the Muties helping Kenner.
* Ramsay Ross&nbsp;– Lazarus, the Mutie spiritual leader who prophesies Kenners coming.
* Diana Quijano&nbsp;– Radio, one of the Muties who can tell what other people are thinking.
* Orlando Sacha&nbsp;– Bishop, the main antagonist who is the leader of the bandits.
* Emily Kreimer&nbsp;– Shiela, Phils most recent girlfriend who wanted to see Oblivion.
* Ramon Garcia&nbsp;– Bad Guy, one of the bandits.
* Tony Vasques&nbsp;–  Yorrick, one of the bandits.
* David Killerby&nbsp;– Buddy
* Ian Igberg&nbsp;– Horst
* Brayton Lewis&nbsp;– Old Handler
* Alasdair Ross&nbsp;– Young Handler
*Milagros Relayce&nbsp;– Child
*Diego Bertie&nbsp;– Phil, Kenners assistant whose father is the "bossman" of a zirconium mining company.
*Jeanne Cervantes&nbsp;– Old Woman
*Mercy Bustos&nbsp;– Bald Woman
*Carlos Cano de la Fuente&nbsp;– Soldier (as Carlos Cano)

==VHS release==
The VHS was released on December 17, 1992 by New Horizons Home Video.

==References==
 

==External links==
*  
*  
*  
* 

 
 
 
 
 
 