Zombi 2
 
 
{{Infobox film
| name           = Zombi 2
| image          = Zombie Flesh eaters.jpg
| caption        = Italian theatrical poster
| director       = Lucio Fulci
| producer       = {{plainlist|
* Fabrizio De Angelis
* Ugo Tucci
}}
| writer         = {{plainlist|
* Elisa Briganti
* Dardano Sacchetti  (uncredited) 
}}
| starring       = {{plainlist|
* Tisa Farrow Ian McCulloch Richard Johnson
* Al Cliver
}}
| cinematography = Sergio Salvati
| music          = {{plainlist|
* Fabio Frizzi
* Giorgio Cascio  (as Giorgio Tucci) 
* Adriano Giordanella (uncredited)
* Maurizio Guarini (uncredited)
}}
| editing        = Vincenzo Tomassi
| studio         = Variety Film Production
| distributor    = The Jerry Gross Organization  (U.S.) 
| released       =  
| runtime        = 91 minutes
| country        = Italy
| language       = Italian English ITL 410,000,000  (estimated) 
| gross          = ITL 614,000,000  (Italy) 
}}
 zombie horror splatter genre. Conservative government. 

==Plot==
 
An abandoned yacht drifts into New York Harbor, and as two Harbor Patrol officers (Marty) and (Bill) investigate, a huge, decomposing, flesh-hungry ghoul attacks them, biting Marty in the neck. The remaining officer Bill shoots the hulking zombie and it topples overboard. The body of the deceased officer Marty is deposited in the morgue.
 Ian McCulloch) is assigned by his news editor (director Lucio Fulci in a cameo) to report on the mysterious boat and there he meets Anne. While on the boat, Anne and Peter discover a note from her father explaining he is on the island of Matool (Saint Thomas, U.S. Virgin Islands) suffering from a strange disease.  They decide to continue to investigate together. Upon their arrival in the tropics, they enlist the aid of a seafaring couple, Brian Hull (Al Cliver aka Pier Luigi Conti) and his wife Susan Barrett (Auretta Gay), to assist them in finding the island.
 Richard Johnson), a resident on the island and physician at the local Missionary|mission, is investigating its secrets. His contemptuous, high-strung wife Paola (Olga Karlatos) wants to leave the island in fear of the increasing zombie attacks, but Dr. Menard insists on continuing his research.

Near the island Susan decides to go for a dive. While in the water, Susan encounters a shark, which tries to attack her, but she manages to hide among the coral reefs. She immediately surfaces and begs for help. Brian shoots the shark, but it hits the boat, causing them to lose control. Susan dives under again and tries to escape. As she is hiding in a coral reef, a zombie (Ramón Bravo) attacks her, but she avoids the creature by slashing its water-bloated face with a piece of coral. The shark then turns and attacks the zombie; the two creatures battle until the shark severs the zombies arm. The shark swims away while the zombie seemingly gives up and heads in the opposite direction.

Susan manages to get on the boat as Anne, Peter and Brian help her. Meanwhile, Dr. Menard and Missy, his nurse (Stefania DAmario) continue to study the zombies. Some of them are newly bitten, some are in the zombiefication stage and some are already decomposing. Menards local assistant Lucas (Dakar) appears at the door, says that the zombies are attacking everyone on the island, and asks how to kill the zombies.

Night falls and Menards wife, Paola, takes a shower. The camera shows a zombie (Giannetto De Rossi) spying on her from outside the house. The zombie comes in; she manages to close the door and hold it shut with her body. The zombies hands break through the door and grab her hair. In possibly the most famous scene from the film, Paolas eye is pierced by a large splintered piece of wood and she is then killed off-camera.

The boat finally arrives at the dock. Back at the hospital Missy wakes Dr. Menard from a deep sleep. She tells him that Matthias (Franco Fantasia) has died because of the infection. Dr. Menard waits for his friends body to reanimate, then shoots him in the head. As Lucas is digging the graves of Matthias and the others that have died from the contagion, he sees a flare gun fire. He follows it and discovers the crew from the boat. Dr. Menard tells Anne about her father Ugo Bologna and when the contagion started. As they arrive, Lucas says that something happened to His friend, Fritz Briggs (Leo Gavero). He tells the group to go to his mansion, where Paola is located. He approaches Fritz and he says that he has been bitten.

The group arrive at the mansion where they discover the horribly mutilated corpse of Paola being hungrily devoured by zombies. A swarm of zombies attack them but they fight them off and escape. They get in the jeep but lose control and drive off the road. Peters ankle is badly hurt. The group traverse through the jungle. Peter takes a rest and Anne examines his wound. Susan and Bryan explore their surroundings, and Brian finds an old helmet. It appears that the group have stumbled onto a Spanish Conquistador cemetery. Anne and Peter are lying on the ground and proceed to have an ill-timed love session when a zombie grabs her hair and another zombie grabs Peters foot. Bryan hears Annes scream; he leaves Susan to follow it. In another famous scene, a petrified Susan watches in horror as an ominous, worm-infested zombie Conquistador (Ottaviano Dellacqua) rises through the earth, lunges at her and bites her, tearing out a chunk of her throat. Brian saves Anne and Peter but fails to save Susan. He shoots the rotting zombie in the back twice but it still stands, until Peter grabs a nearby wooden cross from his grave and smashes the zombies head, destroying it. The group then head back to the hospital.

More and more zombies rise from their graves. The group finally arrive at the hospital and barricade themselves inside. Dr. Menard asks what happened to Paola and is told that she is dead. Dr. Menard explains to them that a voodoo curse has made the dead rise. He says he is still searching for a way to stop the curse. The zombies then begin their assault on the hospital. Dr. Menard goes to look for bullets, but is attacked and killed by a reanimated Fritz. Brian sees the attack and shoots the rabid Fritz in the head as he ravenously gnaws on the doctors cheek. The people who have been infected in the hospital begin to reanimate. A frothing zombie bites a huge chunk of flesh and muscle out of Lucas forearm. Lucas lets out a blood curdling scream, then silently dies of his horrific injury while the zombies start attacking Missy. Brian hears her scream and tries to help her but a zombie breaks out the window. Brian shoots the zombie. He helps Missy and continues his defense against the undead. Missy goes to get some supplies but a reanimated Lucas grabs and bites her.

The zombies finally destroy the main door and break in. Peter and Brian shoot at the zombies while Anne throws Molotov cocktails at them. They manage to stop some of the zombies and escape the hospital, which is now burning down. The last of the group are on the road heading to the boat and destroying more zombies, but a reanimated, blood-caked Susan appears in front of Brian and bites his arm. Peter shoots the reanimated Susan in the head. They reach the boat and sail away. Now out at sea, Brian is showing bad signs of contagion. He dies and they lock him in one of the rooms on the boat, taking him with them as evidence. When they reach the open ocean, however, they receive a radio report that a plague of zombies has attacked New York City. As the credits roll, the zombies are walking on the Brooklyn Bridge, leaving Peter and Anne to an unknown fate.

==Cast==
* Tisa Farrow as Anne Bowles Ian McCulloch as Peter West Richard Johnson as Dr. David Menard
* Al Cliver as Brian Hull
* Auretta Gay as Susan Barrett (Hull)/Zombie
* Stefania DAmario as Missy, Menards nurse
* Olga Karlatos as Paola Menard
(Uncredited cast)
* Nick Alexander as Brian Hull (Voice Dub)
* Susan Spafford as Paola Menard (Voice Dub)
* Dakar as Lucas, Menards Assistant/Zombie
* Leslie Thomas as Coroner 
* James Sampson as James, 2nd Coroner 
* Ramón Bravo as Underwater Zombie
* Franco Fantasia as Matthias
* Leo Gavero as Fritz Briggs/Zombie
* Edward Mannix as Cop guarding boat (Voice Dub)
* Ugo Bologna as  Dr. Bowles, Annes father/Zombie
* Lucio Fulci as News editor and reporter
* Arthur Haggerty as Zombie Sailor on boat
* Brendan Elliott as Harbor Patrol Cop 
* Omero Capanna as Tall Dark Crew Cut Haired Zombie 
* Giannetto De Rossi as Zombie Hand on Paola 
* Ottaviano DellAcqua as Worm-Eyed Zombie 
* Alberto DellAcqua as Zombie
* Roberto DellAcqua as Zombie
* Arnaldo DellAcqua as Zombie
* Romanokids as Various Zombies

==Production==

===Development===
Writing for Zombi 2 started before Dawn of the Dead was released in Italy. The opening and ending sequences were later written into the script when the producers wanted to cash in on George Romeros film. Screenwriter Dardano Sacchetti chose to remove his name from the films credits because his father had died during the films pre-production. During the films early development, director Enzo G. Castellari was asked to direct the film, but he tuned it down as he mostly directed action films and was not a fan of horror films. Fulci was then signed as the films director when Castellari, who was a personal friend of Fulci, recommended him as a replacement. 

===Filming===
Zombi 2 was filmed in Rome, Italy at Elios Studio and Studio Mafera as well as outside the studios in Rome. The films underwater sequence was filmed in Isla Mujeres, Mexico. Additional scenes were filmed in New York, and in the Dominican Republic.  

While shooting on location in New York City, Arthur Haggerty, who plays the large zombie who attacks the harbor patrol at the beginning of the film, walked into CBGBs, a Bowery bar which was a flourishing punk rock venue at the time, in full zombie makeup complete with splattered fake blood and mud caked all over his face and body. Due to the outrageous punk styles in those days of the other bar patrons, he was barely noticed. Even the bartender never looked twice at him.  

Due to budget constraints, filming the final sequence that shows hordes of zombies walking on the Brooklyn Bridge, they were unable to stop traffic on the bridge. This is why in the films ending, cars can be seen driving below.   

The films make-up effects were done by renowned Italian Giannetto De Rossi. The make-up effects were placed on the actors faces in layers to the effect that Lucio Fulci referred to the zombies as "walking flower pots."  

The shark versus zombie scene was not desired or produced by Lucio Fulci.  The idea, as the final scene, was by Ugo Tucci, when he met René Cardona Jr., who had directed the shark film  Tintorera. It was filmed in Isla Mujeres and the zombie was interpreted by a local trainer marine of sharks. 

== Reception ==

=== Europe ===
 
Zombi 2  s success in Europe reignited Fulcis sagging career and reinvented the director as a horror icon. Zombi 2 introduced several of his trademarks: hordes of shambling putrefied zombies, hyper-realistic gore and blood, and the infamous "eyeball gag" (a character is impaled or otherwise stabbed through the eyeball). Fulci would go on to direct several more horror films. 

There is some controversy about when the Zombi 2 screenplay was written, and whether it lifted dialogue from Dawn of the Dead. 

Despite the popularity of the film, Zombi 2 was banned in several countries, including Great Britain, due to its gore content. It was released by Vipco but with a lot of violence edited out. It was finally released uncut in 2005. Lead actor Ian McCulloch, who is British, never actually had the opportunity to watch the full film until he recorded a commentary for the Roan Groups LaserDisc release of Zombi 2 in 1998, and was shocked at the gore level.

Zombi 2  s European box office take led to four sequels. All have self-contained plots. While the Zombi series proved to be lucrative, Zombi 2 is by far the most recognizable of the European zombie films.
 action adventure adventure thriller thriller with no link to George A. Romeros films. The opening and closing scenes (which take place in New York) were added to the script later when the producers wanted to cash in on the success of Dawn of the Dead.

=== United States ===
  zombie canon. The theatrical trailers for Zombie provided the memorable tagline of "We Are Going to Eat You!" and showcased some of the make-up effects, but did nothing to indicate the plot of the picture (although the audience was indeed warned about the graphic content of the film: a humorous crawl at the end of the preview promises "barf bags" to whoever requested them upon viewing the film).

Released theatrically to U. S. theaters and drive-in theaters in the summer of 1980 from distributor The Jerry Gross Organization (no longer in existence today), its tagline was: "When the earth spits out the dead... They will return to tear the flesh of the living..."

It currently has a 42% "Rotten" by critics and 70% from audience on Rotten Tomatoes. 

=== Accolades ===
  Best Special Best Make-up

== Home video release history == Anchor Bay and The Roan Group respectively. Both versions present the film in widescreen, but the transfer was still dark and muddy as the earlier VHS releases. This version also omitted several shots of nudity from the film and other miscellaneous bits because of the print used.

Five years later, both   while the Media Blasters release is Interlaced, resulting in combing during fast motion and stair casing jaggies.

On 25 October 2011, the film was released by Blue Underground on Blu-ray Disc|Blu-ray along with a new DVD edition containing a new 2K transfer. 

The other films in the Zombi series made it to America as video releases. None were released theatrically in the States, or had any real connection with this entry other than zombies.

=== Video nasty ===
  video nasties". Three scenes in particular were criticized by the British Parliament for their bloody and graphic content: the eye gouge viewed from the perspective of the both the victim and the specific jagged splinter of wood concerned, the zombie feast scene, and the scene in which a petrified Susan has her throat excavated by a zombie conquistador.

A version of the film was released on video by Vipco in 1992. This was the original cut cinema version. The film was re-submitted in 1999, and an "Extreme Version" was passed with 23 seconds of cuts, trimming back the eye gouge scene and the zombie feast. The  .

== Legacy ==
  The Real another Lucio Fulci film.
*  , on the fifth track "Electric Head, Pt. 2 (The Ecstasy)", Rob Zombies lyric being "a fistful of hair and a splinter in the mind."
* The Canadian band Fake Shark – Real Zombie! took their name as a reference from a scene in this film.
* The band Send More Paramedics have a song called "Zombie vs. Shark" in homage to this film. Necro sampled the theme in the song "Carnivores" on the 2005 self-titled album from his group Circle of Tyrants.
* This film was No. 98 on Bravo (US TV channel)|Bravos 100 Scariest Movie Moments for the scene when a zombie pulls a victim towards a splintered wood shard.  
* British comic Jamie Hewletts animation company is called Zombie Flesh Eaters.
* The New York band Grasshopper performed a track live on WFMU titled "Zombie Shark Mangler", referencing the zombie vs. shark fight scene. 
* The shark scene featured in a Windows 7 commercial in 2010, on a fictitious website called "Zombie Companion", where the footage is visible dubbed over with nature documentary-style narration. 
* In Sealab 2021 Green Fever, a zombie diver is seen outside of the lab and gets attack by a shark, this is a reference to the film of the shark and zombie fight. 
* The video game Dead Island has a very similar premise to Zombi 2 in the case of survivors on an island fighting off hordes of the undead. However, unlike Zombi 2, Dead Island has more than one type of zombie; regardless, the game is clearly inspired by Zombi 2.
*A scene from the film The Monster Squad shows a Zombi 2 poster.
*In the film The Stink of Flesh there is a character named Matool, remembering the Matul island.
* There is a scene in the Planet Terror segment of Grindhouse (film)|Grindhouse in which Quentin Tarantinos character is stabbed in the eye with a piece of wood that was inspired by a scene from Zombi 2. Tarantino stated at the 2006 San Diego Comic-Con International that he and Robert Rodriguez would show double features of Zombi 2 and Torso (1973 film)|Torso with film trailers to give the actors the feel of a grindhouse experience. Warm Bodies.

==Sequels==
 

==See also==
*1979 in film

== References ==
 

*  

== External links ==
*  
*  
*  
*   at The Deuces Grindhouse Cinema Database
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 