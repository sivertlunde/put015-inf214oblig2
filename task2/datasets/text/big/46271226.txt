Blood Legacy
{{Infobox film
| name           = Blood Legacy
| director       = Carl Monson
| producer       = Carl Monson (producer) Ben Rombouts (executive producer)
| screenplay     = Eric Norden (screenplay) Carl Monson (story)
| starring       = Rodolfo Acosta Merry Anders Norman Bartold John Carradine Faith Domergue
| music          = Jaime Mendoza-Nava
| cinematography = Jack Beckett
| editing        = John Post
| studio         = Studio West Film Distributors
| distributor    = Ellman Film Enterprises
| released       =  
| runtime        = 82 minutes 90 minutes (full)
| country        = United States
| language       = English
| budget         = $56,000 (estimated)
}}
Blood Legacy (also called Legacy of Blood and Will to Die) is a 1971 horror movie directed by Carl Monson.

==Plot==
After a patriarch dies, family members gather in the estate to inherit the fortune.  But then someone starts killing them.

==Cast==
*Rodolfo Acosta: Sheriff Dan Garcia
*Merry Anders: Laura Dean
*Norman Bartold: Tom Drake
*Ivy Bethune: Elga
*John Carradine: Christopher Dean
*Richard Davalos: Johnny Dean
*Faith Domergue: Veronica Dean
*Buck Kartalian: Igor Garin
*Brooke Mills: Leslie Dean
*Jeff Morrow: Gregory Dean
*John Russell: Frank Mantee
*John Smith: Dr. Carl Isenburg
*Mr. Chin: Chin

==Presentations==
Horror hostess Elvira, Mistress of the Dark (Cassandra Peterson) presented Blood Legacy as part of her Movie Macabre series.

==External links==
*  

 
 