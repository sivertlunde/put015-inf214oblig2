We Are Not Angels 3: Rock & Roll Strike Back
{{Infobox film | name =Ми нисмо анђели: Рокенрол узвраћа ударац Mi nismo anđeli 3: Rokenrol uzvraća udarac We Are Not Angels 3: Rock & Roll Strike Back 
  | image = 
  | caption = 
  | director = Petar Pašić		
  | producer = Srđan Dragojević Snežana Penev Biljana Prvanović
  | writer = Srđan Dragojević Petar Pašić Dimitrije Vojnov
  | starring = Nikola Pejaković Srđan Todorović Uroš Đurić Zlatko Rakonjac Nada Macanković Zoran Cvijanović Vesna Trivalić Seka Aleksić
  | music = Igor Perović
  | cinematography = Vladan Janković Dušan Joksimović
  | editing = Marko Glušac	
  | distributor = 2006
  | runtime = 98 min Serbian
  | budget =
  }}
A3: Rokenrol uzvraća udarac is a 2006 Serbian film.

==Plot== Youth Work SFR Yugoslavia, kolo to folkish harmonica sounds rather than listening to him play his acoustic guitar. Heartbroken and depressed he confides to his best friend about once reading that young people in America make the devil appear by playing their music backwards and that the devil then makes them rich and famous. He decides to test the theory and suddenly the devil and angel from the previous Mi nismo andjeli movies materialize and grant him his wishes of women, fame, and fortune. Young Borko thus becomes the mega popular Yugoslavia-wide rocknroll superstar Dorijan.

Cut to 30+ years later Dorijan is still a debauched, coke-snorting, and alcoholic superstar, except that hes now switched to playing turbo folk instead of rocknroll. He lives with a silicone trophy girlfriend Smokvica and his childhood best friend is his business manager. Despite still having his women, fame, and fortune, Dorijan is unhappy about having to resort to playing a musical style he hates in order to maintain all that.

==External links==
* 

 
 
 
 
 
 
 

 
 