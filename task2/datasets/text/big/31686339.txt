Thomas Muentzer (film)
{{Infobox film
| name           =Thomas Muentzer
| image          = 
| image size     =
| caption        =
| director       =Martin Hellberg
| producer       =Paul Ramacher Friedrich Wolf
| narrator       =
| starring       =Wolfgang Stumpf
| music          = Ernst Roters
| cinematography = Götz Neumann
| editing        =Lieselotte Johl
| distributor    = PROGRESS-Film Verleih
| released       = 17 May 1956
| runtime        =119 minutes
| country        = East Germany German
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 East German Protestant theologian and peasant leader Thomas Muentzer, directed by Martin Hellberg. 

==Plot==
At 1519, the teachings of Martin Luther sweep through the German principalities. They are welcomed by the peasants, who hope that the new doctrines will help to liberate them from the oppressive yoke of the nobility and the magistrates. The young pastor Thomas Muentzer embraces Lutheranism, but he is more radical in his support for the peasants. 

At 1523, Muentzer arrives in Allstedt to assume the office of the local pastor. When a local villager is arrested after assaulting an overseer who tried to rape his sister, the priest helps him escape. He also carries a first Mass in German rather than Latin, and preaches his flock to destroy all the saints icons in the local chapel, which he deems to be heretical. The people burn it down. The local baron retaliates by destroying the village. The priest now realizes that he is no longer a follower of Luther, who called to refrain from violence. He flees to southern Germany, where he and his friend Heinrich Pfeiffer take over the city of Muehlhausen and form a peasant rebel army, intending to liberate the people. But betrayal and the schemes of the nobility bring about their defeat in the Battle of Frankenhausen. Muentzer is captured; as he is tortured, he refuses to deny his religious doctrines and is then executed.

==Selected cast==
*Wolfgang Stumpf as Thomas Muentzer. Ottilie von Gerson. Heinrich Pfeiffer.
*Wolf Kaiser as Hannes the Swab.
*Ulrich Thein as student. 
*Gerd Michael Henneberg as Evangelist priest. 
*Horst Giese as miner. 
*Fritz Diez as Field Captain Hoffmann.
*Franz Loskarn as Captain Krumpe.  Ernst II, Count von Mansfeld.
*Edgar Bennert as Frederick III, Elector of Saxony. 
*Friedrich Richter as John, Elector of Saxony. 
*Fred Diesko as John Frederick I, Elector of Saxony. 
*Guido Goroll as Louis V, Elector Palatine.
*Jan Franz Krueger as Henry V, Duke of Brunswick-Lueneburg. 
*Peter Herden as Philip I, Landgrave of Hesse. 
*Paul Paulsen as George, Duke of Saxony.
*Ruediger Renn as Charles V, Holy Roman Emperor.

==Production==
The communist leadership of East Germany, in its attempts to create a unifying narrative for the citizens of their country, attempted to portray the history of the land as a chain of events which developed according to the rules of Marxism-Leninism, leading unavoidably to the consolidation of socialist power in the state. The figure of the preacher Thomas Muentzer held an especially important status in the eyes of the establishment, both because of his radical theology that was regarded as a precursor to communism and his recognition by Friedrich Engels, who viewed him as a revolutionary leader - as stated in Engels book, The Peasant War in Germany.  
 Friedrich Wolf Barracked Peoples Police and apprentices, were involved in the filming, which took place in the town of Quedlinburg. The picture was shot using Agfacolor reels.    

==Reception==
The film had its premiere on 17 May 1956, on the tenth anniversary to DEFAs founding.   on DEFA Foundations website.   It was commercially released on the following day. The Der Spiegel film critic wrote that the picture was "intended to depict the 16th-Century Iconoclast as Walter Ulbrichts ideological predecessor" and that its plot culminated in "a complete confusion."  West Germanys Catholic Film Service described it as "an immense production... with a superficial and biased interpretation of history... in spite of the expensive crowd scenes, it is anemic."  In 1970, a review of the German Film Studies Institute noted that for uncertain reasons, Martin Luther was not seen in the film at all.  

In 2005, the picture was released on DVD format by the company Icestorm Entertainment. In a special 13-minutes-long supplement to the new edition, historian Susanne Galley noted several inaccuracies in the plot: Muentzer is shown to have held to his beliefs under torture, while in reality he agreed to deny them before his execution; the peasants defeat in the Battle of Frankenhausen is attributed to betrayal and sabotage, rather than to the weakness of their army. Galley believed that the plot was affected by the governments ideological tendencies. 

==References==
 

==External links==
  on the IMDb.

 
 
 
 
 
 
 