Magic Cop
 
 
{{Infobox film name = Magic Cop image = Magic-cop-poster.jpg alt =  caption =  traditional = 驅魔警察 simplified = 驱魔警察 pinyin = Qū Mó Jǐngchá jyutping = Keoi1 Mo1 Ging2 Caat3}} director = Stephen Tung producer = Lam Ching-ying writer = Sam Chi-leung Tsang Kan-cheung starring = Lam Ching-ying Michael Miu Wilson Lam music = BMG Melody Bank cinematography = Cho On-Sun Kwan Chi-kan Lam Fai-taai editing = Kwok Ting-hung Woo Kei-Chaan studio = Movie Impact Ltd. Millifame Productions Ltd. distributor = Media Asia released =   runtime = 87 minutes country = Hong Kong language = Cantonese budget =  gross = HK$3,645,000
}}
Magic Cop, also informally known as Mr. Vampire 5, is a 1990 Hong Kong horror comedy film produced by, and starring Lam Ching-ying.

==Plot== living corpse", and is being controlled by a Japanese magician for smuggling. With Fengs supernatural skills and detective techniques, they finally find the location of the secret altar of the Japanese magician. 

==Cast==
* Lam Ching-ying as Uncle Feng
* Michael Miu as Sergeant No. 2237
* Wilson Lam as Detective Lam
* Wong Mei-way as Lin
* Michiko Nishiwaki as Japanese magician
* Wu Ma as Ma
* Billy Chow as Japanese magicians henchman
* Frankie Chin as Eddie

 List of cast   

==Home media==

===VCD releases===

{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Language
! Subtitles 
! Notes 
! REF 
|- style="text-align:center;"
|| Unknown 
|| Hong Kong
|| II
|| Megastar (HK) 
|| NTSC
|| Cantonese
|| Cantonese/English
|| 2VCDs
|| 
|}

===DVD releases===
{| class="wikitable"
|-
! Release date 
! Country 
! Classifaction 
! Publisher  
! Format  
! Region  
! Language 
! Sound  
! Subtitles  
! Notes 
! REF 
|- style="text-align:center;"
|| 20 April 2004
|| Hong Kong
|| N/A
|| Mega Star (HK)
|| NTSC
|| ALL
|| Cantonese, Mandarin
|| Dolby Digital 5.1
|| English, Traditional Chinese, Simplified Chinese
||
|| 
|}

==References==
 

==External links==
*  
*  

 
 
 


 