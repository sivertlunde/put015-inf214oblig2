Faust: Love of the Damned
{{Infobox film
| name           = Faust: Love of the Damned
| image          = Faust-Love-of-the-Damned.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Brian Yuzna
| producer       = Ted Chalmers Carlos Fernández Julio Fernández Antonio González Bea Morillas Miguel Torrente Brian Yuzna David Quinn Miguel Tejada-Flores
| based on       =  
| starring       = Mark Frost Isabel Brook Jennifer Rope Jeffrey Combs Andrew Divoff
| music          = Xavier Capellas
| cinematography = Jacques Haitkin
| editing        = Luis de la Madrid
| studio         = Castelao Producciones Fantastic Factory (Filmax) TVC Vía Digital
| released       =   |df=y}}
| runtime        = 101 minutes
| country        = Spain
| language       = English
| budget         =
| gross          =
}}
 horror film, comic book Sitges International Fantastic Film Festival on 12 October 2000.

The film, which was the first of nine to be produced by Filmaxs Fantastic Factory label, won the award for Best Special Effects at the 2000 Catalonian International Film Festival in Sitges, Spain. 

== Plot ==
An artist, John Jaspers (Mark Frost) sells his soul to the mysterious M (as for Mephistopheles) (Andrew Divoff) in order to avenge the death of his girlfriend. However, the deal has an unexpected price, and he is periodically transformed into a horned demon with a passion for killing. He discovers that M plans to release a giant monster called the Homunculus, thereby opening the gates of Hell, and sets out to stop him.

== Cast ==
* Mark Frost as John Jaspers / Faust
* Isabel Brook as Jade de Camp
* Jennifer Rope as Blue 
* Jeffrey Combs as Lt. Dan Margolies
* Andrew Divoff as M (Mephistopheles)

== Soundtrack == Machine Heads "Take My Scars" was used as the films theme song.

{{tracklist
| headline = 
| extra_column = Artist
| title1       = Take My Scars Machine Head
| length1      = 4:19
| title2       = Lady Bird
| extra2       = Baby Fox
| length2      = 4:06
| title3       = Def Beat extra3 = Junkie XL
| length3      = 4:56
| title4       = Remanufacture
| extra4       = Fear Factory
| note4        = remixed by Rhys Fulber
| length4      = 6:43
| title5       = For Fucks Sake
| extra5       = Nailbomb
| length5      = 5:44 Bleed
| note6        = feat. Fred Durst and DJ Lethal
| extra6       = Soulfly
| length6      = 4:07
| title7       = The Blood, the Sweat, the Tears
| extra7       = Machine Head
| length7      = 4:11
| title8       = Breed Apart
| extra8       = Sepultura
| length8      = 4:01 Loco
| extra9       = Coal Chamber
| length9      = 4:15
| title10      = Replica
| extra10      = Fear Factory
| length10     = 3:56
| title11      = Timelessness
| extra11      = Fear Factory
| length11     = 4:08
}}

== Release ==
Faust: Love of the Damned premiered at Sitges Film Festival on 12 October 2000.   It was released theatrically in late October 2000. 

Trimark released it on DVD in 2001,    and Mosaic released a DVD in the UK in January 2002.   Arrow Video re-released the DVD on 18 April 2011, containing several special features. 

== Reception ==

AllRovi|AllMovies review of the film was mixed, writing, "Check your brain at the door and eat up this grisly eye candy."   Jonathan Holland of Variety (magazine)|Variety described it as "entertaining in a voyeuristic way but also as corny, crude and excessive as they come."   Gareth Jones of Dread Central rated it 2/5 stars and called it "utter, utter trash" that is a guilty pleasure.   Bloody Disgusting rated it 4/5 stars and wrote that it was much better than expected, though cheesy and corny in spots.   Patrick Naugle of DVD Verdict called it "low budget horror slop with lots of T&A" of interest mostly to Yuzna fans. 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 