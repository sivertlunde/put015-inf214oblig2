We're No Angels (1955 film)
 
{{Infobox film name = Were No Angels image = Were No Angels - 1955 - poster.png caption = Original movie poster director = Michael Curtiz producer = Pat Duggan based on =   writer = My Three Angels 1953 play Samuel and Bella Spewack screenplay = Ranald MacDougall starring = Humphrey Bogart Aldo Ray Peter Ustinov music = Frederick Hollander cinematography = Loyal Griggs editing = Arthur P. Schmidt distributor = Paramount Pictures released =   runtime = 106 min. country = United States language = English French French
|gross = $3 million (US)  
}}
Were No Angels is a 1955 Christmas comedy film starring an ensemble cast of Humphrey Bogart, Peter Ustinov, Aldo Ray, Joan Bennett, Basil Rathbone, and Leo G. Carroll. Shot in both  VistaVision and Technicolor, the Paramount Studios production was directed by Michael Curtiz, who had directed Bogart in Casablanca (film)|Casablanca when both were under contract to Warner Brothers. It is one of Bogarts rare comedies. 

The screenplay was written by Ranald MacDougall, based on the play My Three Angels by Samuel and Bella Spewack, itself was based upon the French play La Cuisine Des Anges by Albert Husson. Mary Grant designed the films costumes.

==Plot==
Three convicts - Joseph, Albert and Jules - escape from prison on Devils Island just before Christmas and arrive at a nearby French colonial town. They go to a store managed by the Ducotel family, the only one to give supplies on credit. While there, they notice its roof is leaking, and offer to fix it. They do not actually intend to, but decide to remain there until nightfall, when they will steal clothes and supplies and escape on a ship waiting in the harbor. As they wait, they find that the small family of Felix, Amelie, and daughter Isabelle, is in financial distress and offer their services to hide the trios all-too-sinister ruse. Joseph even gets to work conning people and falsifying records to make the store prosperous.  However, the three felons begin to have a change of heart after they fix a delicious Christmas dinner for the Ducotels made mostly of stolen items.
 halos appear over their heads...followed by one above the cage of Adolphe.

==Cast==
*Humphrey Bogart as Joseph
*Aldo Ray as Albert
*Peter Ustinov as Jules
*Joan Bennett as Amelie Ducotel
*Basil Rathbone as Andre Trochard
*Leo G. Carroll as Felix Ducotel
*Gloria Talbott as Isabelle Ducotel John Smith as Doctor Arnaud
*Torben Meyer as Butterfly Man
*Paul Newlan as Port Captain
*John Baer as Paul Trochard
*Lea Penman as Mme. Parole
*Jack Del Rio as Gendarme
*Joe Ploski as Customs Inspector
*Louis Mercier as Celeste
*George Dee as Coachman
*Victor Romito as Gendarme

==References==
 

==External links==
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 