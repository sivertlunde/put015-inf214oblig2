Whispering Smith
 
{{Infobox film
| name           = Whispering Smith
| image          = Whispering Smith poster.jpg
| image size     =
| caption        =
| director       = Leslie Fenton
| producer       = Frank Butler Karl Kamb Frank H. Spearman (novel)
| narrator       = Robert Preston Brenda Marshall
| music          =
| cinematography =
| editing        =
| distributor    = Paramount Pictures
| released       = December 9, 1948
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Western film Robert Preston and Brenda Marshall. 
 1926 film John Bowers, and Eugene Pallette as supporting cast.
 series of the same name, with Audie Murphy, film star and World War II hero, in the title role.

==Plot==
The bad Barton boys—Blake, Leroy and Gabby—rob a train and shoot a guard. Luke Smith, known as "Whispering" to some for his quiet, sly ways, is a detective for the railroad, sent to investigate.

Murray Sinclair, an old friend of Smiths, is in charge of the railroads wrecking crew. Hes glad to see Smith, who shoots Leroy and Gabby and is saved when a bullet is deflected by a harmonica in his pocket, given him long ago by his sweetheart Marian, who is now Sinclairs wife.

It saddens Smith to find out that Sinclair might be in cahoots with Barney Rebstock, a rancher with a bad reputation. Rebstock has been hiding the remaining Barton brother, Blake, who is tracked down by Smith.

Whitey DuSang is a hired gun for Rebstock, who wants to see Smith dead. When the railroads boss gives Sinclair an order, Sinclair rebels and is fired. Rebstock hires him to pull off a string of daring train holdups.

Smith forms a posse. Whitey kills a guard and betrays Rebstock, shooting him. Sinclair is wounded. Smith does away with Whitey but gives his old friend Sinclair a last chance. When he rides home, Sinclair finds his wife packing and strikes her, accusing his Marian of leaving him for Smith. 

Smith shows up and Sinclair apologies for his actions. He seems sincere, but when Smiths back is turned, Sinclair pulls a hidden gun. Before he can fire, Sinclair falls over and dies. Smith leaves town, his work there done.

==Cast==
*Alan Ladd as Luke "Whispering" Smith Robert Preston as Murray Sinclair
*Brenda Marshall as Marian Sinclair
*Donald Crisp as Barney Rebstock
*Fay Holden as Emmy Dansing
*Murvyn Vye as Blake Barton
*Frank Faylen as Whitey Du Sang
*John Eldredge as George McCloud
*Ward Wood as Leroy Barton
*J. Farrell MacDonald as Bill Baggs Will Wright as Sheriff McSwiggin

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 


 