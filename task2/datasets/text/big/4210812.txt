The Wonderful Wizard of Oz (1910 film)
 
{{Infobox film name            = The Wonderful Wizard of Oz  image           = The Wonderful Wizard of Oz (1910).ogv caption         = 13-minute film clip director        = Otis Turner (unconfirmed) producer        = William Selig writer          = Otis Turner (unconfirmed)
| based on       =   starring        = Bebe Daniels music           = cinematography  = distributor     = Selig Polyscope Company
| country        = United States released        =   runtime         = 13 mins. language        = Silent film English intertitles budget          =
}}
 silent fantasy The Wonderful Wizard of Oz, though much of the film deals with the Wicked Witch of the West, who does not appear in the musical.

==Plot==
In Kansas, Dorothy and Betsy are chased into the cornfield by the mule, Hank, and the farmhands draw their muskets at the beast. Dorothy discovers in the field that the family scarecrow is alive. The Scarecrow builds a haystack and warns Dorothy and the farm animals to take cover. A cyclone appears overhead and carries the haystack away, thus letting it fall into the Land of Oz.

In Oz, the travelers meet the Tin Woodman and the Cowardly Lion|Lion. When they enter a forest, Momba the Witch flies out the window as her soldiers come out of the cottage, they are all captured and led into the witchs jail-house. After defeating the wicked witch, the travelers arrive at the Emerald City for the retirement party of the Wizard, who names the Scarecrow king and leaves in a balloon with Dorothy.

==Cast==
The credits to this film are lost, and the identity of the director and actors are disputed.

There is no definitive proof who is in the cast, or who directed the film.  Otis Turner may have directed the film, but Mark Evan Swartz points out that it is highly unlikely that both Otis Turner and Bebe Daniels worked on the film, as they were in different parts of the country at the time (Turner in Chicago, Daniels in California), and neither had a strong impetus for travel.  Dorothy does look like contemporary photos of Daniels, which would make Turners direction improbable. Michael Patrick Hearn disputes this, and has found ample evidence that both were in California at the time.  At any rate, that Baum knew of Turner is confirmed by his spoofery of an "Otis Werner" in his Aunt Janes Nieces Out West, a fictional account inspired by his optimism as an independent filmmaker.

Other reported cast members include Hobart Bosworth, Robert Z. Leonard, Eugenie Besserer, Winifred Greenwood, Lillian Leighton, Olive Cox, Marcia Moore, and Alvin Wycoff. Swartz suggests Bosworth was the Scarecrow and Leonard the Tin Woodman, but photographs of the actors make this appear unlikely and suggest that Bosworth was the Wizard and Leonard the Scarecrow. Based on photographs, and assuming the cast list is correct, it appears that Cox is Glinda and Leighton is the servant who pulls out a list of Union rules. Besserer is most likely Momba, and Greenwood likely to be Aunt Em. There is quite a large cast before the camera, and it is unlikely that they will all ever be identified. Michael Patrick Hearn emphasizes that this cast list is not contemporary with the film and may have no basis in fact.

==Production history== The Wizard MGM film which has become so famous.)  As is clear from the plot descriptions below, the presence of Eureka the kitten is drawn from the commingling of material from The Marvelous Land of Oz and Dorothy and the Wizard in Oz—Eureka appears in the latter novel.
 stage and film show created and presented by Baum in 1908), this was proven not to be the case when the film was recovered.  Although the only known Fairylogue film footage has decomposed (and the interactive nature of the presentation makes the discovery of another print unlikely), the slides, script, and production stills are available (and many have been reprinted in books and magazines) and clearly from another production, which emphasized material from Ozma of Oz that the descriptions of the Selig films imply was ignored.  This film, and its sequels, were created in the wake of Baums loss of the rights to The Wonderful Wizard of Oz and temporary licensing rights on The Marvelous Land of Oz and John Dough and the Cherub.

==Other adaptations== The Patchwork Girl of Oz, The Magic Cloak of Oz, and His Majesty, the Scarecrow of Oz (all released in 1914).

==Sequels==

The Selig Polyscope sequels are known only from their catalog descriptions, derived from press releases printed in Motion Picture World:

===Dorothy and the Scarecrow in Oz===
 Dorothy and the Scarecrow are now in the Emerald City. They have become friendly with the Wizard, and together with the woodman, the cowardly lion, and several new creations equally delightful, they journey through Oz -- the earthquake -- and into the glass city. The Scarecrow is elated to think he is going to get his brains at last and be like other men are; the Tin-Woodman is bent upon getting a heart, and the cowardly lion pleads with the great Oz for courage. All these are granted by his Highness. Dorothy picks the princess. -- The Dangerous Mangaboos. -- Into the black pit, and out again. We then see Jim, the cab horse, and myriads of pleasant surprises that hold and fascinate. 

===The Land of Oz=== General Jinger   showing myriads of Leith soldiers in glittering apparel forming one surprise after the other, until the whole resolves itself into a spectacle worthy of the best artists in picturedom. Those who have followed the two preceding pictures of this great subject cannot but appreciate "The Land of Oz," the crowning effort of the Oz series. 

===John Dough and the Cherub===
No description of this film was given, but it does mention the name. It was unlikely to be considered a direct sequel, but is probably based on another L. Frank Baum novel, John Dough and the Cherub.

==Home media== 1902 stage the 1939 film version. On this edition, John Thomas performs a compilation of Oz-related music by Louis F. Gottschalk.

==See also==
*The Wizard of Oz (adaptations) — other adaptations of The Wonderful Wizard of Oz
*Treasures from American Film Archives

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 