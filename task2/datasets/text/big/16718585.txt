Armored (film)
{{Infobox film
| name           = Armored
| image          = Armored ver2.jpg
| caption        = Theatrical release poster
| director       = Nimród Antal
| producer       = Sam Raimi Josh Donen Dan Farah
| writer         = James V. Simpson
| starring       = Matt Dillon Jean Reno Laurence Fishburne Amaury Nolasco Milo Ventimiglia with Skeet Ulrich and Columbus Short John Murphy
| cinematography = Andrzej Sekuła
| editing        = Armen Minasian
| distributor    = Screen Gems
| released       =  
| runtime        = 88 minutes 
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $22,942,221
}}

Armored is a 2009 American crime thriller film directed by Nimród Antal, written by first-time screenwriter James V. Simpson, and starring Matt Dillon, Jean Reno, Laurence Fishburne, Amaury Nolasco, Milo Ventimiglia, Skeet Ulrich, and Columbus Short. It was released on December 4, 2009. 

==Plot== armored car transport teams. Now the legal guardian of younger brother Jimmy (Andre Kinney), the Silver Star recipient struggles to make ends meet. In a bar after an armored car prank heist played on Ty, his co-workers recount several historical attacks on armored cars. It is explained that Ashcroft (Fred Ward), their immediate supervisor, was once involved in an ambush, becoming the only survivor as he single-handedly killed five assailants. This story was topped by "a better" one of an armored car jacking in 88, wherein the guards surrendered the funds they were escorting in order to prevent the killing of a fellow guard; it concluded in the safety of all the guards and escape of the hijackers. The thieves were never caught, and the crew speculates that the escort team staged the attack.

After receiving constant letters about impending foreclosure on his home, Ty finds himself in need of additional money to help make ends meet. While contemplating this, Ty is approached by Mike Cochrane (Matt Dillon), his godfather and co-worker, who has devised a plan to steal money being transferred from the Federal Reserve to the local banks, but Ty turns down the offer. Upon arriving home that evening, he finds a social worker who explains that Tys younger brother has missed most of the school year, and that the lack of proper care has forced the state to consider placing Jimmy in a foster home.

The following morning, after receiving assurances from Mike that no one will be hurt, Ty reluctantly agrees to participate in the heist. The six-person crew are dispatched as normal in two trucks to collect $42 million, or $21 million each, from the Federal Reserve, and after a radio check-in with their superiors the plan is set into motion by driving to an abandoned steel mill located in a radio dead zone after pickup. Here the team begins to unload the money for storage, intending to collect the cash after the heat from the heist dies down. The group offloads the first truck, but their plan is compromised when a homeless man living in the mill is spotted observing them. Because the man has seen too much, a chase ensues and an armed guard, Baines (Laurence Fishburne), shoots him. Ty tries to take the injured man to the unloaded truck, but as he attempts to do so, Mike kills the man, stating that he had no choice. Upset over this, Ty barricades himself inside the truck with the remaining $21 million inside. After an attempt to flee in the truck fails, Ty sets off the trucks alarm, which catches the attention of a local sheriffs deputy, Jake Eckehart (Milo Ventimiglia).

To turn off the siren, Cochrane pulls out the fuse to the engine, disabling the vehicle, but not before Jake draws closer due to the noise of the siren.  Meanwhile, out of anger, the remaining thieves devise a plan to break into the truck by knocking the pins out of the door hinges, which will allow them to remove the doors. Jake arrives, and Mike stops him before he enters the mill, claiming to be a security guard. Just as Mike convinces Jake to leave, Ty successfully restores power to the trucks alarm, which blares immediately. In reaction to the plans being compromised, Baines shoots Jake, seriously injuring him. While the thieves are distracted, arguing over what to do, Ty uses kerosene and a flare in the hideout hole containing the first $21 million. It flares up; while the thieves run over to the stash, with the first $21 million engulfed in flames, Ty seizes the opportunity to grab Jake and put him in the truck that he had hidden in earlier. Ty reassures Jake, calms him and applies first aid.  The remaining guards continue to work on the door of the truck to get in and kill Ty and Jake. Dobbs (Skeet Ulrich) begins to have second thoughts about the operation, and agrees with Ty to make an attempt to get the fuse Mike removed from the engine. He retrieves the fuse, but is caught trying to put it back into the engine. As Dobbs begs for forgiveness for not being able to continue with the plan, he asks Palmer (Amaury Nolasco) to reassure him that the other men will not harm him. The rest of the thieves signal Palmer to murder Dobbs. Palmer keeps his promise not to let the other men hurt Dobbs and stabs Dobbs to death himself.

As the thieves continue their attempts to remove the door hinges, Ty discovers that the floor of the truck has a removable panel. Covering the interior windows with the remainder of the $21 million, Ty removes the panel, and takes Jakes radio to the roof to get better reception. Ty contacts the authorities, but Palmer catches him before he can return to the vehicle. They exchange words as Ty attempts to convince Palmer that what they are doing is not right. Palmer finally asks Ty if he thinks God will forgive him, and jumps off the building, killing himself. Before deputies arrive, the remaining thieves reveal a kidnapped Jimmy and demand that Ty open the truck. Ty complies, and Quinn (Jean Reno) and Baines head for the money; however Ty has rigged the money case with a booby trap, which detonates upon opening. The explosion destroys the money and kills both Quinn and Baines. Ty has Jimmy look after Jake while he runs to get help.

Mike, who was injured during the blast, chases after Ty in the working armored truck, while Ty tries to direct authorities to the scene in the deputys car. After the car becomes disabled from ramming, Ty flees on foot with the surviving truck in pursuit. The truck crashes into a pit in the plant, which results in Mikes death. Later, Ty is seen in the hospital waiting room while a recovering Jake is debriefed by authorities and Ashcroft. Ashcroft approaches Ty, stating that theres talk of a reward, implying that Jake spoke about his efforts in the foiling of the thieves. With Jimmys also being released from the hospital, Ty and Jimmy now go home.

==Cast==
* Matt Dillon as Mike Cochrane   
* Jean Reno as Quinn 
* Laurence Fishburne as Baines 
* Skeet Ulrich as Dobbs 
* Columbus Short as Tyler "Ty" Hackett 
* Amaury Nolasco as Palmer 
* Milo Ventimiglia as Officer Jake Eckehart 
* Fred Ward as Duncan Ashcroft 
* Andre Kinney as Jimmy Hackett 

==Production==
Filming took place in Los Angeles. 

==Reception==
The film was not screened in advance to critics. Scott, A. O.,  , review, The New York Times, p C9, December 5, 2009, retrieved same day  The film received mixed reviews, getting a 40% rating on Rotten Tomatoes.
 Pulp Fiction) helps capture that mood, according to Scott. 

The film was "accidentally" released by Sony on PlayStation Network free of charge, though it was pulled after an unspecified amount of time. The movie was issued while it was still showing in theaters, and although the mistake was eventually spotted, it is thought to have been downloaded thousands of times before the error was fixed. 

== Home media == Region 1 Anamorphic widescreen and Blu-ray Disc Region A formats on March 16 of 2010.

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 