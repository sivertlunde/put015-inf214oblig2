Dawn of the Planet of the Apes
 
{{Infobox film
| name           = Dawn of the Planet of the Apes
| image          = Dawn of the Planet of the Apes.jpg
| alt            = A chimpanzee brandishes an automatic rifle while astride a rearing horse.
| caption        = Theatrical release poster
| director       = Matt Reeves
| producer       = {{Plain list|
* Peter Chernin
* Dylan Clark Rick Jaffa Amanda Silver
}}
| writer         = {{Plain list|
* Mark Bomback
* Rick Jaffa
* Amanda Silver
}}
| based on =    
| starring       = {{Plain list|
* Andy Serkis Jason Clarke
* Gary Oldman
* Keri Russell
* Toby Kebbell
* Kodi Smit-McPhee
* Kirk Acevedo
}}
| music          = Michael Giacchino
| cinematography = Michael Seresin
| editing        = {{Plainlist|
* William Hoy
* Stan Salfas
}}
| studio         = {{Plain list |
* Chernin Entertainment
*TSG Entertainment
}}
| distributor    = 20th Century Fox
| released       =  
| runtime        = 130 minutes  
| country        = United States  
| language       = English
| budget         = $170 million 
| gross          = $708.8 million   
}} Jason Clarke, Gary Oldman, Keri Russell, Toby Kebbell, Kodi Smit-McPhee, and Kirk Acevedo. It is the sequel to the 2011 film Rise of the Planet of the Apes, which began 20th Century Foxs reboot of the original Planet of the Apes series, which making it the eighth theatrical film in the franchise. Dawn is set ten years after the events from Rise, and it follows a group of scientists in San Francisco struggle to stay alive in the aftermath of a plague that is wiping out humanity, while Caesar tries to maintain dominance over his community of intelligent apes. 
 Best Visual Effects. A third installment is set to be released in July 2017.

==Plot== civil unrest and the economic collapse of every country in the world. Over 90% of the human population has died in the pandemic, while apes with genetically enhanced intelligence have started to build a civilization of their own.
 Caesar leads and governs an ape colony located in the Muir Woods. While walking through the forest, Caesars son Blue Eyes and his friend Ash encounter a human named Carver, who panics and shoots Ash, wounding him. Carver calls for the rest of his small party of armed survivors, led by a man named Malcolm, while Blue Eyes calls for the other apes. Caesar orders the humans to leave. The remaining humans in San Francisco, who are genetically immune to the virus, are living in a guarded and unfinished high-rise tower within the ruined city. Prompted by Koba, a scarred bonobo who holds a grudge against humans for his mistreatment, Caesar brings an army of apes to the city tower where he conveys the message that while the apes do not want war, they will fight to defend their home. He demands that the humans stay in their territory and states the apes will stay in theirs too.

Malcolm convinces his fellow leader Dreyfus to give him three days to reconcile with the apes to gain access to a hydroelectric dam in their territory, which could provide long-term power to the city. Dreyfus, distrustful of the apes, arms survivors using an abandoned Armory (military)|armory. Malcolm then travels into the ape village, but is captured by gorilla guards, who bring him to Caesar. After a tense discussion, Caesar allows Malcolm to work on the dams generator, if they surrender their guns. As Malcolm, his wife Ellie and son Alexander work, they bond with the apes. Mutual distrust of both sides gradually subsides; the truce is endangered when Caesars infant son discovers a shotgun smuggled in by Carver, but the two sides reconcile when Ellie offers to help treat Caesars ill wife Cornelia with antibiotics. Meanwhile, Koba discovers the armory and confronts Caesar, questioning his allegiance and taunting him over his "love" for humans. In response, Caesar severely beats Koba, but at the last moment refrains from killing him; adhering to his philosophy that "ape not kill ape," Caesar hesitantly forgives Koba. The furious Koba then returns to the armory, where he steals an assault rifle and murders two human guards. Returning home, he secretly kills Carver, stealing his lighter and cap.
 alpha and the fire, Koba takes charge, and having planted Carvers cap at the scene of the shooting, urging the apes to fight against the humans. Malcolms group hides as Koba leads the apes into San Francisco. The apes plunder the armory and charge the towers gates. Despite heavy casualties, the apes breach the gates using a hijacked armored car, overrun the tower and imprison all the humans as Dreyfus flees underground. When Ash refuses Kobas orders to kill unarmed humans, citing Caesars teachings, Koba kills Ash and imprisons all those known to be loyal to Caesar.

Malcolms group finds Caesar barely alive and transport him to his former home in San Francisco. Caesar reveals to Malcolm that Koba shot him, realizing his notion that all apes were better than humans was naïve and that apes can be as violent as humans. Malcolm leaves the group and heads to the city to find medical supplies for Caesar. While looking for medical supplies, Malcolm encounters Blue Eyes; disenchanted with Kobas leadership, the young ape spares Malcolms life and returns to the house with him, where he reconciles with his father. Caesar grows nostalgic watching a video clip from his childhood of his former owner and father figure Will Rodman on his old   charges he has planted beneath the tower. The resulting explosion kills him and collapses part of the tower. Caesar overpowers Koba, with Koba hanging over the edge of the tower. Pleading for his life, Koba reminds Caesar that apes do not kill apes, but Caesar states that Koba is not an ape and lets him fall to his death.

Malcolm informs Caesar of the impending arrival of human military reinforcements and both lament the lost opportunity for peace. Caesar tells Malcolm that the humans will never forgive the apes for the war they started and advises him to leave with his family for safety as the two of them acknowledge their friendship. As Malcolm disappears into the shadows, Caesar stands before a kneeling mass of apes, awaiting the war to come. 

==Cast==

===Apes===
*Andy Serkis as Caesar, a common chimpanzee and leader of the apes.
*Toby Kebbell as Koba, a bonobo and Caesars treacherous advisor. He was previously played by Christopher Gordon in Rise.
*Nick Thurston as Blue Eyes, a common chimpanzee, Caesar and Cornelias first son.
*Karin Konoval as Maurice, a Bornean orangutan and Caesars friend and advisor.
*Terry Notary as Rocket, a common chimpanzee and Caesars friend and Ashs father.
*Doc Shaw as Ash, a common chimpanzee, son of Rocket, also Blue Eyes best friend.
*Judy Greer as Cornelia, a common chimpanzee and Caesars wife, mother of Blue Eyes and a newborn son named Milo. She was previously played by Devyn Dalton in Rise. Lee Ross as Grey, a common chimpanzee and a follower of Koba.

===Humans=== Jason Clarke as Malcolm, the leader of the small group that formed a strong bond with Caesar and the other apes.
*Gary Oldman as Dreyfus, the leader of the remaining human survivors.
*Keri Russell as Ellie, a former nurse at the Centers for Disease Control|CDC, and Malcolms wife.
*Kodi Smit-McPhee as Alexander, Malcolms son from a previous marriage.
*Kirk Acevedo as Carver, a former San Francisco water worker and a member of Malcolms group.
*Jon Eyez as Foster, a member of Malcolms group.
*Enrique Murciano as Kemp, a member of Malcolms group.
*Jocko Sims as Werner, the colonys radio operator.
*Keir ODonnell as Finney, an ally of Dreyfus. Kevin Rankin as McVeigh, a guard at the colonys armory.
*Lombardo Boyar as Terry, a guard at the colonys armory.
*James Franco as Will Rodman, from Rise, in a cameo via a video from Caesars childhood.

==Production==
 

===Development===
After the release of Rise of the Planet of the Apes, director Rupert Wyatt commented on possible sequels: "I think were ending with certain questions, which is quite exciting. To me, I can think of all sorts of sequels to this film, but this is just the beginning."  Screenwriter and producer Rick Jaffa also stated that Rise featured several clues as to future sequels: "I hope that were building a platform for future films. Were trying to plant a lot of the seeds for a lot of the things you are talking about in terms of the different apes and so forth." 

In an interview recorded after the release of Rise, Wyatt stated, "We want to grow and evolve, in the films that will   come after this, to the 68 original."  Wyatt also stated that he wants it to take place eight years after Rise, as a whole new ape generation can be born, and explore the dynamics of Caesar and Kobas relationship.  According to screenwriter Rick Jaffa, a version of the spaceship from the 1968 Planet of the Apes under the name Icarus (Planet of the Apes)|Icarus was in Rise as a deliberate hint to a possible sequel. 

In November 2011, Andy Serkis was the first to be announced as having closed a deal for a sequel to Rise. It was reported to be a "healthy seven-figure deal" for him to reprise his role as Caesar, the ape leader.  On May 15, 2012, it was announced Scott Z. Burns had been hired to do rewrites on the original screenplay by Rise writers Rick Jaffa and Amanda Silver.  On May 31, 2012, 20th Century Fox announced that the sequel, now titled Dawn of the Planet of the Apes, was scheduled for release on May 23, 2014. 

  Twilight Zone film.  On October 18, Mark Bomback, writer of Live Free or Die Hard, was reported to be doing a re-write for Reeves.  It was announced on June 20, 2013 that the release date for Dawn was being pushed back two months to July 18, 2014.  On December 10, 2013, the film was pushed up one week to July 11, 2014. 

===Casting===
In December 2012, after the departure of director Wyatt, James Franco speculated that he would not be returning for the sequel, saying, "Now Ruperts not a part of it so I dont know. My guess is I wont be in it. Nobodys talked to me since Rupert left."  Later, Matt Reeves revealed that Franco would be seen in a cameo in the film.   Freida Pinto, who played primatologist Caroline Aranha in Rise, confirmed that she would not be returning for Dawn.  In April 2014, when asked by IGN about the fate of Franco and Pintos characters, producer Dylan Clark said, "I mean, they’re the ones that died...They were ground zero of the virus." 
 Jason Clarke, and Kodi Smit-McPhee were cast in lead roles for the sequel, set ten years after the events from the first film.   In March 2013, actress Keri Russell was cast in a role.  That same month, Judy Greer was cast as Cornelia, a female chimp and love interest for Caesar.  Toby Kebbell, Enrique Murciano and Kirk Acevedo joined the cast during filming.  On May 15, 2013, Jocko Sims was cast in a supporting role of military operative Werner. 

===Filming=== Campbell River, British Columbia.  The location of Vancouver Island was chosen for its similarity to the locations depicted in the film, the forests, and the variety of landscapes.  Filming in New Orleans started in May 2013 and continued in July 2013 at various locations such as the former Six Flags park Six Flags New Orleans.   
 

==Soundtrack==
{{Infobox album
| Name       = Dawn of the Planet of the Apes
| Type       = Soundtrack
| Artist     = Michael Giacchino
| Cover      =
| Released   = August 12, 2014
| Recorded   = 2014
| Genre      = Film score
| Length     = 
| Label      = Sony Masterworks
| Producer   =
| Chronology = Michael Giacchino film scores
| Last album = Star Trek Into Darkness (2013)
| This album = Dawn of the Planet of the Apes (2014)
| Next album = This Is Where I Leave You (2014)
}}

The films score was composed by Michael Giacchino. The soundtrack was released by Sony Masterworks on July 8, 2014. 

===Track listing===
{{Track listing
| all_music       = Michael Giacchino
| total_length    = 
| title1          = Level Plaguing Field
| length1         = 2:21
| title2          = Look Who’s Stalking
| length2         = 2:35
| title3          = The Great Ape Processional
| length3         = 4:34
| title4          = Past Their Primates
| length4         = 1:57
| title5          = Close Encounters of the Furred Kind
| length5         = 4:38
| title6          = Monkey to the City
| length6         = 1:16
| title7          = The Lost City of Chimpanzee
| length7         = 3:46
| title8          = Along Simian Lines
| length8         = 5:04
| title9          = Caesar No Evil, Hear No Evil
| length9         = 2:27
| title10         = Monkey See, Monkey Coup
| length10        = 5:12
| title11         = Gorilla Warfare 
| length11        = 7:37
| title12         = The Apes of Wrath
| length12        = 4:28
| title13         = Gibbon Take 
| length13        = 2:55
| title14         = Aped Crusaders
| length14        = 3:26
| title15         = How Bonobo Can You Go 
| length15        = 5:42
| title16         = Enough Monkeying Around
| length16        = 3:35
| title17         = Primates for Life
| length17        = 5:42
| title18         = Planet of the End Credits 
| length18        = 8:56
| title19         = Ain’t That a Stinger
| length19        = 1:10
}}

==Release== San Francisco, California on June 26, 2014.  The film closed the 36th Moscow International Film Festival on June 28.   

In Hungary, the largest cinema chain called Cinema City could not agree with the films distributor, InterCom, and as a result it opened on July 17, 2014 on 45 screens, significantly fewer than similar big-budget productions.  However, the film still managed to top the weekend box office chart of the country beating  , which had been leading the chart for three weeks (on 105 screens distributed by UIP Duna). 
===Marketing===
A  .  
A partnership with 20th Century Fox and Ndemic Creations saw mobile/PC game Plague Inc. get a Dawn of the Planet of the Apes-themed update on July 10, 2014. It allows players to create and customize a simian flu virus to infect the world and eradicate humanity whilst helping apes survive. 

==Reception==

===Box office=== ninth highest-grossing film of 2014. 

In the U.S. and Canada, the film is the highest grossing film in the Planet of the Apes franchise, unadjusted for inflation    and eight highest-grossing film of 2014.  It opened on July 11, 2014 across 3,967 theaters and topped the box office on its opening day earning $27.7 million (including previews).  During its traditional three-opening, the film debuted at number one earning $72.6 million, which was 33% higher than its predecessor.   in its second week.  

Dawn of the Planet of the Apes earned $31.3 million during its opening weekend internationally from 4,913 screens in 26 markets where it opened at No. 1 in 14 of those. International opening weekend tallies of more than $5 million were witnessed in the UK ($14.88 million), Mexico ($12.94 million), Korea ($11.5 million), Russia ($9.99 million), Brazil with ($9.2 million) and Australia ($6.6 million).       The film topped the box office outside North America for two non-consecutive weekends.  

===Critical response===
Dawn of the Planet of the Apes was met with widespread critical acclaim.    On  , which assigns a weighted average, the film has a "generally favorable" rating score of 79 out of 100 based on 48 reviews. 
 Star Wars—its that much better."  Tim Robey of The Daily Telegraph said, "Theres evident patience and intelligence to the filmmaking all over, as well as an engagement with genuine ideas about diplomacy, deterrence, law and leadership. However often it risks monkey-mad silliness, its impressively un-stupid."  Drew McWeeny of HitFix awarded the film "A+" and said "Dawn is not just a good genre movie or a good summer movie. Its a great science-fiction film, full-stop, and one of the years very best movies so far." 
 Shakespearean grandeur."    A.O. Scott of The New York Times praised the film for being able to balance out the action sequences and special effects with strong storytelling, writing that "Dawn is more than a bunch of occasionally thrilling action sequences, emotional gut punches and throwaway jokes arranged in predictable sequence. It is technically impressive and viscerally exciting, for sure, but it also gives you a lot to think, and even to care, about."   

Less favourable reviews included Andrew OHehir at Salon (website)|Salon.com who wrote, "Here’s a rule that has gradually become clear to me: Any film that begins with one of those fake-news montages, where snippets of genuine CNN footage are stitched together to concoct a feeling of semi-urgency around its hackneyed apocalypse, already sucks even before it gets started. This one makes a dutiful attempt to struggle back from that suckage, but it all ends in yelling." 

===Home media===
Dawn of the Planet of the Apes was released by 20th Century Fox Home Entertainment on Blu-ray 3D, Blu-ray, and DVD on December 2, 2014.   According to Nielsen VideoScan, it subsequently became the best-selling home video release for the week. 

==Sequel==
On January 6, 2014, 20th Century Fox announced a third installment with Reeves returning to direct and co-write along with Bomback for a July 2016 release.   An early licensing promo gave a place-holder title of Planet of the Apes,  but no official title has yet been announced. In January 2015, Fox delayed the release of the film to July 14, 2017.  
 

==References==
{{reflist|30em|refs=
   

   
}}

==External links==
 
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 