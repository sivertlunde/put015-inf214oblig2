Hello Out There!
{{Infobox play
| name       = Hello Out There!
| image      = 
| image_size = 
| caption    = 
| writer     = William Saroyan
| characters = 
| setting    = A small jail in Texas
| premiere   =  
| place      = Lobero Theatre in Santa Barbara, California English
| subject    = 
| genre      = one-act play
| web        = 
}}Hello Out There! is a one-act play by the Armenian-American playwright William Saroyan written early in August 1941.

==Plot== set in a small Texas jail. There are two major Character (arts)|characters, Photo-Finish and Emily, whom Saroyan refers to simply as "A Young Man" and "A Girl". Photo-Finish is a down on his luck gambler and ends up in jail in a hole-in-the-wall town as a result of a married harlot crying rape when he refused to pay her after coming over to her house.

There he meets Emily, an unhappy cook. When they meet, it is love at first sight.  Emily and Photo-Finish fall in love and make plans to go to San Francisco, but their plans are crushed when the men looking for Photo-Finish find him, and kill him.

==Production history==
The play was first performed in 1941 at the Lobero Theatre in Santa Barbara, California as the curtain raiser to a revival of George Bernard Shaws The Devils Disciple, and was first performed on Broadway in 1942. The Broadway production starred Eddie Dowling and Julie Haydon. Al Pacino played the Young Man in a performance in 1963 that marked his first appearance on stage in New York City.

==Film adaptation== Lee Patrick, and Ray Teal. The film, intended to be one episode in an anthology film in the style of similar films such as W. Somerset Maughams Quartet (1948 film)|Quartet (1948), was never released. Hello Out There was Whales last film.

==Opera adaptation==
In 1953, composer Jack Beeson composed a one-act chamber opera based on the play.

==External links==
* 
 

 
 
 
 
 
 


 