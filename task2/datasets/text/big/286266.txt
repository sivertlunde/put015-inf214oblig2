Stalker (1979 film)
{{Infobox film
| name = Stalker
| image = Stalker_poster.jpg
| image_size =
| caption =
| director = Andrei Tarkovsky
| producer = Aleksandra Demidova  Arkadi Strugatsky Boris Strugatsky
| based on =  
| narrator =
| starring = Alexander Kaidanovsky Anatoli Solonitsyn Nikolai Grinko
| music = Eduard Artemyev
| cinematography = Alexander Knyazhinsky
| editing = Lyudmila Feiginova
| studio = Mosfilm
| distributor =
| released =   Dom Kino, Moscow   
| runtime = 163 minutes
| country = Soviet Union
| language = Russian SUR 
| gross =
}}
 1979 art philosophical and psychological themes. 
 sentient while their path through it can be sensed but not seen. In the film, a stalker is a professional guide to the Zone, someone having the ability and desire to cross the border into the dangerous and forbidden place with a specific goal.    
 Strugatsky brothers in their novel The Roadside Picnic (1972), making an allusion to Rudyard Kiplings character Stalky from the Stalky & Co. stories. In Roadside Picnic, "Stalker" was a common nickname for men engaged in the illegal trade of prospecting for and smuggling alien artifacts from the mysterious and dangerous "Zone".  All of this terminology is unrelated to the modern Anglosphere use of the term stalking in reference to trailing and spying on victims.

The film has received many positive reviews, being labeled as one of the best drama films of the latter half of the 20th century. In September 2012, the British Film Institutes 50 Greatest Films of All Time poll conducted for Sight & Sound listed Stalker at #29, just above the 1985 documentary Shoah (film)|Shoah. http://www.bfi.org.uk/news/50-greatest-films-all-time 

==Plot==
The Stalker (Alexander Kaidanovsky) works in some unclear area in the indefinite future as a guide who leads people through the Zone, a vicinity in which the normal laws of reality no longer fully apply. The Zone contains a place called the Room, said to grant the wishes of anyone who steps inside. The area containing the Zone is sealed off by the government and great hazards exist within it. At home with his wife and daughter, the Stalkers wife (Alisa Freindlich) begs him not to go into the Zone but he ignores her pleas. In a rundown bar, the Stalker meets his next clients for a trip into the Zone. The Writer (Anatoly Solonitsyn) and the Professor (Nikolai Grinko) agree to put their fate into the hands of the Stalker. Their specific names do not come up as they all agree to refer to each other pseudo-anonymously by just their professions.
 military blockade railway work traps by actions such as throwing metal nuts tied to strips of cloth ahead of them. The three men must deal with the fact that the complicated path that they must take cannot be specifically seen or heard but can only be sensed.
 altruistic aim of aiding the desperate. At times, he refers to a previous Stalker named Porcupine. That man had led his brother to his death in the Zone, visited the Room, gained a large sum of money, and then hanged himself, failing to achieve the happy ending implied in rumors about the Zone. 
 unconscious desires of those that come in. As well, it appears clear that the Zone itself has a kind of sentience. When the Writer later confronts the Stalker about his knowledge of the Zone and the Room, the Stalker replies that his information came from the now deceased Porcupine. After traveling through tunnels the three reach their destination. They determine that their goal lies inside a decayed and decrepit industrial building. In a small antechamber, a phone begins to ring. The Writer answers and cryptically speaks into the phone, stating "this is not the clinic", before hanging up. The surprised Professor decides to use the phone to ring up a colleague. In the ensuing conversation, he reveals his true intentions behind his whole journey.
 nuclear device with him, and he intends to destroy the Room for fear it might be used by evil men. The three visitors to the Zone then fight verbally and physically in a larger antechamber, just outside the Room. The fight ends in a tie, all three men feeling exhausted. As they catch their breath, the Writer experiences an epiphany about the Rooms true nature. He argues that when Porcupine met his goal that, despite the mans conscious motives, the room fulfilled Porcupines true, secret desire for wealth, instead of bringing back his brother from death. Thus, Porcupines suicide came about from the resulting guilt. The Writer further reasons the Room is genuinely useless to the ambitious since its ability to look inside those who enter it renders the Room only dangerous to those who seek it for negative reasons. With his earlier fears assuaged, the Professor gives up on his plan of destroying the Room. Instead, he disassembles his bomb and scatters its pieces. 

The men rest before the doorway and yet, despite having come so far and faced so much, they never enter. Rain begins to fall into the Room through its ruined ceiling, then gradually fades away. The Stalker, the Writer, and the Professor are shown back in the bar, and are met by the Stalkers wife and daughter. A black dog that had followed the three men through the Zone is in the bar with them. When his wife asks where he got the dog, Stalker declares that it just came to him, and he remarks that he felt unable to leave it behind.
 love poem by Fyodor Tyutchev.

The Stalkers daughter holds the large book and lays her head on the table before her. She then appears to use psychokinesis to push three drinking glasses across it, one after the other moving. The final glass falls to the floor. However, it does not break. A train passes by where the Stalkers family lives, and the entire apartment shakes. As the stark, crashing noises of the train grow and the walls shake more and more, the film suddenly ends. 

==Cast==
* Alisa Freindlich as the Stalkers Wife
* Alexander Kaidanovsky as the Stalker
* Anatoli Solonitsyn as the Writer
* Nikolai Grinko as the Professor

Supporting actors:
* Natasha Abramova as Monkey, the Stalkers daughter
* Faime Jurno, Y. Kostin and R. Rendi

==Production==

===Writing=== classical Aristotelian unity, that is the unity of action, the unity of location, and the unity of time. 
 idealistic views of the Stalker as he finds himself unable to really make others in his life happy. The films contents depart considerably from the novel. According to an interview with Tarkovsky in 1979, the film has basically nothing in common with the novel except for the two words "Stalker" and "Zone". 

However, watching the film and reading the novel demonstrates that there are, in fact, several similarities between the novel and the film. In both works, the Zone is guarded by a police or military guard, apparently authorized with deadly force. The Stalker in both works tests the safety of his path by tossing nuts and bolts (tied with scraps of cloth), ensuring that gravity is normal (i.e., the object flies in an expected path.) A character named Hedgehog is a mentor to Stalker. In the novel, frequent visitations to the zone increase the likelihood of abnormalities in the visitors offspring. In the book, the Stalker has a daughter with light hair all over her body, nicknamed "Monkey"—the same nickname used for the Stalkers daughter in the film, though in the film she is crippled. Neither in the novel nor in the film do the women enter the Zone. Finally, the target of the expedition (the final expedition in the case of the novel) in both works is a wish-granting device.

In Roadside Picnic the site was specifically described as the site of alien visitation; the name of the novel derives from a metaphor proposed by a character who compares the visit to a roadside picnic.
 looks directly into the camera and explains, with increasing authority, how she met the Stalker and decided to stick with him. It is the only such scene in the entire 160 minutes of the film; the content though is a kind of answer to what the same woman had said in the opening scene, when she blamed her husband for their miseries. It carries clear allusions to Christ  (who also called strangers to "follow me") and as some reviewers pointed out, echoes the style of 19th-century Russian novels with their bold and passionate heroines. 

An early draft of the screenplay was published as a novel Stalker (novel)|Stalker that differs much from the finished film.
 MK2 DVD, production designer Rashit Safiullin describes the Zone as a space in which humans can live without the trappings of society and can speak about the most important things freely.

===Production===
 

In an interview on the MK2 DVD, the production designer, Rashit Safiullin, recalls that Tarkovsky spent a year shooting a version of the outdoor scenes of Stalker. However, when the crew got back to Moscow, they found that all of the film had been improperly developed and their footage was unusable. The film had been shot on new Kodak 5247 stock with which Soviet laboratories were not quite familiar. 

Even before the film stock problem was discovered, relations between Tarkovsky and Stalkers first cinematographer, Georgy Rerberg, had deteriorated. After seeing the poorly developed material, Rerberg was fired from the production by Tarkovsky. By the time the film stock defect was discovered, Tarkovsky had shot all the outdoor scenes and had to abandon them. Safiullin contends that Tarkovsky was so despondent that he wanted to abandon further production of the film. 

After the loss of the film stock, the Soviet film boards wanted to shut the film down, officially writing it off. But Tarkovsky came up with a solution: he asked to make a two-part film, which meant additional deadlines and more funds. Tarkovsky ended up reshooting almost all of the film with a new cinematographer, Aleksandr Knyazhinsky. According to Safiullin, the finished version of Stalker is completely different from the one Tarkovsky originally shot. 
 sepia and color footage; within the Zone, in the countryside, all is colorful, while the outside, urban world is tinted sepia.

  hydro power plants on the Jägala river near Tallinn, Estonian SSR|Estonia.    The shot before they enter the Zone is an old Flora chemical factory in the center of Tallinn, next to the old Rotermann salt storage and the electric plant — now a culture factory where a memorial plate of the film was set up in 2008. Some shots from the Zone were filmed in Maardu, next to the Iru powerplant, while the shot with the gates to the Zone was filmed in Lasnamäe, next to Punane Street behind the Idakeskus. Some shots were filmed near the Tallinn-Narva highway bridge on the Pirita River. 

The documentary film Rerberg and Tarkovsky: The Reverse Side of "Stalker" by Igor Mayboroda sheds new light on the production of Stalker and the relationship between Rerberg and Tarkovsky. Rerberg felt that Tarkovsky was not ready for this script. He told Tarkovsky to rewrite the script in order to achieve a good result. Tarkovsky ignored him and continued shooting. After several arguments, Tarkovsky sent Rerberg home. Ultimately, Tarkovsky shot Stalker three times, consuming over 5,000 meters of film. People who have seen both the first version shot by Rerberg (as Director of Photography) and the final theatrical release say that they are almost identical. Tarkovsky sent home other crew members in addition to Rerberg and excluded them from the credits as well.

Several people involved in the film production — including Tarkovsky — had met deaths, which some crew members attribute to the films long, arduous shooting schedule in toxic locations. Sound designer Vladimir Sharun recalls:

  }}

===Style===
Like Tarkovskys other films, Stalker relies on long takes with slow, subtle camera movement, rejecting the use of rapid Montage (filmmaking)|montage. The film contains 142 shots in 163 minutes, with an average shot length of more than one minute and many shots lasting for more than four minutes. 

However, the final scene of the movie is an exception, which shots happening quickly and the film itself coming to a sudden, surprising end. 

Almost all of the scenes not set in the Zone are in a high-contrast brown monochrome.

==Soundtrack==
 
 The Mirror. For Stalker Artemyev composed and recorded two different versions of the score. The first score was done with an orchestra alone but was rejected by Tarkovsky. The second score that was used in the final film was created on a synthesizer along with traditional instruments that were manipulated using sound effects.  In the final film score the boundaries between music and sound were blurred, as natural sounds and music interact to the point were they are indistinguishable. In fact, many of the natural sounds were not production sounds but were created by Artemyev on his synthesizer.  For Tarkovsky music was more than just a parallel illustration of the visual image. He believed that music distorts and changes the emotional tone of a visual image while not changing the meaning. He also believed that in a film with complete theoretical consistency music will have no place and that instead music is replaced by sounds. According to Tarkovsky, he aimed at this consistency and moved into this direction in Stalker and Nostalghia. 
 Ninth Symphony was removed and in the opening scene in Stalkers house ambient sounds were added, changing the original soundtrack, in which this scene was completely silent except for the sound of a train. 

===Film score=== Armenia and Azerbaijan and tar based on mugham, accompanied by orchestral background music written by Artemyev.  Tarkovsky, who, unusually for him, attended the full recording session, rejected the final result as not what he was looking for. 

 , a traditional Iranian instrument is used in the Stalker theme.]] flute as SYNTHI 100 synthesizer. These effects included modulating the sound of the flute and lowering the speed of the tar, so that what Artemyev called "the life of one string" could be heard. Tarkovsky was amazed by the result, especially liking the sound of the tar, and used the theme without any alterations in the film. 

===Sound design===
The title sequence is accompanied by Artemyevs main theme. The opening sequence of the film showing Stalkers room is mostly silent. Periodically one hears what could be a train. The sound becomes louder and clearer over time until the sound and the vibrations of objects in the room give a sense of a trains passing by without the trains being visible. This aural impression is quickly subverted by the muffled sound of Beethovens Ninth Symphony. The source of this music is unclear, thus setting the tone for the blurring of reality in the film.    For this part of the film Tarkovsky was also considering music by Richard Wagner or the La Marseillaise|Marseillaise.

In an interview with Tonino Guerra in 1979, Tarkovsky said that he wanted "music that is more or less popular, that expresses the movement of the masses, the theme of humanitys social destiny." He added, "But this music must be barely heard beneath the noise, in a way that the spectator is not aware of it."  In one scene, the sound of a train becomes more and more distant as the sounds of a house, such as the creaking floor, water running through pipes, and the humming of a heater become more prominent in a way that psychologically shifts the audience. While the Stalker leaves his house and wanders around an industrial landscape, the audience hears industrial sounds such as train whistles, ship foghorns, and train wheels. When the Stalker, the Writer, and the Professor set off from the bar in an off-road vehicle, the engine noise merges into an electronic tone. The natural sound of the engine falls off as the vehicle reaches the horizon. Initially almost inaudible, the electronic tone emerges and replaces the engine sound as if time has frozen. 

 

The journey to the Zone on a motorized draisine features a disconnection between the visual image and the sound. The presence of the draisine is registered only through the clanking sound of the wheels on the tracks. Neither the draisine nor the scenery passing by is shown, since the camera is focused on the faces of the characters. This disconnection draws the audience into the inner world of the characters and transforms the physical journey into an inner journey. This effect on the audience is reinforced by Artemyevs synthesizer effects, which make the clanking wheels sound less and less natural as the journey progresses. When the three arrive in the Zone initially, it appears to be silent. Only after some time, and only slightly audibly can one hear the sound of a distant river, the sound of the blowing wind, or the occasional cry of an animal. These sounds grow richer and more audible while the Stalker makes his first venture into the Zone, initially leaving the professor and the writer behind, and as if the sound draws him towards the zone. The sparseness of sounds in the zone draws attention to specific sounds, which, as in other scenes, are largely disconnected from the visual image. Animals can be heard in the distance but are never shown. A breeze can be heard, but no visual reference is shown. This effect is reinforced by occasional synthesizer effects which meld with the natural sounds and blur the boundaries between artificial and alien sounds and the sounds of nature. 

After the three travelers appear from the tunnel, the sound of dripping water can be heard. While the camera slowly pans to the right, a waterfall appears. While the visual transition of the panning shot is slow, the aural transition is sudden. As soon as the waterfall appears, the sound of the dripping water falls off while the thundering sound of the waterfall emerges, almost as if time has jumped. In the next scene Tarkovsky again uses the technique of disconnecting sound and visual image. While the camera pans over the burning ashes of a fire and over some water, the audience hears the conversation of the Stalker and the Writer who are back in the tunnel looking for the professor. Finding the Professor outside, the three are surprised to realize that they have ended up at an earlier point in time. This and the previous disconnection of sound and the visual image illustrate the Zone’s power to alter time and space. This technique is even more evident in the next scene where the three travelers are resting. The sounds of a river, the wind, dripping water, and fire can be heard in a discontinuous way that is now partially disconnected from the visual image. When the Professor, for example, extinguishes the fire by throwing his coffee on it, all sounds but that of the dripping water fall off. Similarly, we can hear and see the Stalker and the river. Then the camera cuts back to the Professor while the audience can still hear the river for a few more seconds. This impressionist use of sound prepares the audience for the dream sequences accompanied by a variation of the Stalker theme that has been already heard during the title sequence. 

During the journey in the Zone, the sound of water becomes more and more prominent, which, combined with the visual image, presents the zone as a drenched world. In an interview Tarkovsky dismissed the idea that water has a symbolic meaning in his films, saying that there was so much rain in his films because it is always raining in Russia.  In another interview, on the film Nostalghia, however, he said "Water is a mysterious element, a single molecule of which is very photogenic. It can convey movement and a sense of change and flux."  Emerging from the tunnel called the meat grinder by the Stalker they arrive at the entrance of their destination, the room. Here, as in the rest of the film, sound is constantly changing and not necessarily connected to the visual image. The journey in the Zone ends with the three sitting in the room, silent, with no audible sound. When the sound resumes, it is again the sound of water but with a different timbre, softer and gentler, as if to give a sense of catharsis and hope. The transition back to the world outside the zone is supported by sound. While the camera still shows a pool of water inside the Zone, the audience begins to hear the sound of a train and Maurice Ravel|Ravels Boléro, reminiscent of the opening scene. The soundscape of the world outside the zone is the same as before, characterized by train wheels, foghorns of a ship and train whistles. The film ends as it began, with the sound of a train passing by, accompanied by the muffled sound of Beethovens Ninth symphony, this time the Ode to Joy from the final moments of the symphony. As in the rest of the film the disconnect between the visual image and the sound leaves the audience in the unclear whether the sound is real or an illusion. 

==Distribution==
Stalker sold 4.3 million tickets in the Soviet Union. 

==DVD== GDR DEFA did a complete German synchronization of the movie which was shown in cinema 1982. This was used by Icestorm Entertainment on a DVD release, but was heavily criticized for its lack of the original language version, subtitles and had an overall bad image quality.
* RUSCICO produced a version for the international market containing the film on two DVDs with remastered audio and video. It contains the original Russian audio in an enhanced Dolby Digital 5.1 remix as well as the original mono version. The DVD also contains subtitles in 13 languages and interviews with Alexander Knyazhinsky, Rashit Safiullin and Edward Artemiev.   

==Reception== 1985 film Shoah (film)|Shoah. 

However, the film received some negative commentary upon its release. Officials at Goskino, a government group otherwise known as the State Committee for Cinematography, were critical of the film.  On being told that Stalker should be faster and more dynamic, Tarkovsky replied:
 

The Goskino representative then stated that he was trying to give the point of view of the audience. Tarkovsky supposedly retorted:
 
 heart of darkness" Stalker looks "a good deal more persuasive than Coppolas."  As well, Slant Magazine reviewer Nick Schager has praised the film as an "endlessly pliable allegory about human consciousness". Hes written that Stalker shows "something akin to the essence of what man is made of: a tangled knot of memories, fears, fantasies, nightmares, paradoxical impulses, and a yearning for something thats simultaneously beyond our reach and yet intrinsic to every one of us." http://www.slantmagazine.com/film/review/stalker 

The film is rated at 100% by Rotten Tomatoes. 

===Influence===
Seven years after the making of the film, the Chernobyl disaster led to the depopulation of a surrounding area (officially called the "Zone of alienation") rather like that in the film. Some of those employed to take care of the abandoned nuclear power plant refer to themselves as "stalkers". 

In 2007, Ukrainian developer GSC Game World published  , an open world first-person shooter video game. The game is loosely based on both the film and the original story, Roadside Picnic, with elements of both works present throughout the game.  The plot is markedly different, however, on several key points. The creation of the Zone was due to project worked upon in secret laboratories founded in depopulated areas after explosions at the  , and one sequel,  .  In the game, S.T.A.L.K.E.R. stands for "Scavengers, Trespassers, Adventurers, Loners, Killers, Explorers, Robbers".

In 2012 English writer Geoff Dyer published Zona: A Book About a Film About a Journey to a Room drawing together his personal observations as well as critical insights about the film and the experience of watching it.

The 2012 film Chernobyl Diaries also involves a tour guide, similar to a stalker, giving groups "extreme tours" of the Chernobyl area.

==Notes==
 

==References==
 

==External links==
*  
*  
*  
*   at Nostalghia.com, a website dedicated to Tarkovsky, featuring interviews with members of the production team
*     – filming locations of Stalker
*   at official Mosfilm site with English subtitles

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 