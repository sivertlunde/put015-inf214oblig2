Shadowless Sword
{{Infobox film
  | name         = Shadowless Sword
  | image        = Shadowless Sword movie poster.jpg
  | caption      = Shadowless Sword movie poster
  | writer       = Kim Tae-kwan   Shin Joon-hee Shin Hyun-joon
  | director     = Kim Young-jun
  | producer     = Park Soo-yun   Jung Tae-won
  | editing      = Nam Na-yeong
  | cinematography = Seo Geun-hee
  | distributor  = CJ Entertainment
  | released     =  
  | runtime      = 104 minutes
  | country      = South Korea
  | language     = Korean
  | music        = Kim Jun-seong
  | budget       =
  | gross        = $3,763,375 
  | film name      = {{Film name
   | hangul       =  
   | hanja        =  
   | rr           = Muyeonggeom
   | mr           = Muyŏnggŏm}}
}}
 2005 South Shin Hyun-joon. Balhae Kingdom, who hides his identity in a small village until he is called to battle invaders from Khitan people|Khitan.  It was released in North America, the United Kingdom and Ireland by New Line Cinema on DVD as The Legend of the Shadowless Sword.

== Plot ==
The film is set after the fall of Sanggyeong, the capital of Balhae in 926. Dongdan Kingdom dispatches the Chucksaldan (a.k.a. Killer Blade Army) to find and kill the last remaining member of the Balhae royal family, exiled prince Jeong-hyun, to prevent the possible reconstruction of Balhae. Prime Minister Lim Sun-ji, on the other hand, sends a young, talented swordswoman, Yeon So-ha, to find Prince Jeong-hyun first and bring him back safely. Jeong-hyun, however, is reluctant to come and fight for the throne, still bitter about his unfair exile. The rest of the movie follows So-ha and Jeong-hyuns journey as they fight their way back to the capital, fall in love and whether or not Jeong-hyun manages to reconstruct the fallen kingdom of Balhae.

== Cast and characters ==
* Lee Seo-jin as Dae Jeong-hyun, the last prince of Balhae
: When Dae Soo-hyun, the last member of the royal family is killed by the Chucksaldan, the prime minister of Balhae Lim Sun-ji is reminded of one person in a full of mournful atmosphere. Dae Jeong-hyun, the last prince of Balhae who has been forgotten from peoples memory since he was involved in the political strife of the royal family and condemned to exile 14 years ago. Now he is the only hope.

* Yoon So-yi as Yeon So-ha, the best warrior of Balhae
: Yeon So-ha with an image of graceful and straight figure is the best woman warrior of Palhae, who always carries a shadowless sword (Mooyounggeom) and shows excellent skill in swordsmanship. She takes up the task of bringing Daejeonghyun to the camp safely in order to make him a king of Balhae.
 Shin Hyun-joon as Gun Hwa-pyung
: Gun Hwa-pyung was the son of a Balhae general/official who was executed by Ae of Balhae, who was the last king of Balhae. Gun Hwa-pyung somehow survived the execution and defected to the rising Khitan. He and several other Balhae defectors gathered together to form the Chuk Ssal Dan. He and this organization were under the control of a governor of the Dongdan. Hwa-pyung vowed revenge on the Balhae Royal Family for destroying he and his fathers family name and status. He secretly plotted to kill off all of the royal family members, betray his master, and put himself on the former Balhae throne as king of a new kingdom. This plan does not seem to work out for him, as he finds that the last prince of Balhae has some secrets of his own to share.

=== Extended cast ===
 
* Lee Ki-yong as Mae Yung-ok
* Jo Won-hee as Jo Chun-soo
* Park Sung-woong
* Lee Han-sol as Dan Yang-soo
* Jeong Ho-bin
* Jin Bong-jin
* Kim Seo-hyeon
* Jo Yeon-ho
* Lee Sang-hong
* Kim Gyeong-ryong
* Park Su-yong
* Han Gang-ho Nam Ji-hyun as young Yeon So-ha
* Baek Shin
* Lee Jang-hyeon
* Choi Yeong-gyun
* Lee Su-yong
* Bae Sang-cheol
* Im Se-jin
* Kang Yeong-gu
* Baek Ji-yeol
* Choi Ji-woo (cameo)
* Hwang In-seong
* Kim Su-ro (cameo)
* Jeong Jun-ha (cameo)
* Park Chan-dea
* Lee Han-gal
 

== References ==
 

== External links ==
* http://www.balhae2005.co.kr/
*  
*  
*  

 
 
 
 
 
 
 
 