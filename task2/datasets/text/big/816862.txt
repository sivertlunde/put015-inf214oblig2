Ever After
 
 
{{Infobox film
| name           = Ever After: A Cinderella Story
| image          = Everafterposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Andy Tennant
| producer       = {{Plainlist|
* Mireille Soria
* Tracey Trench}}
| screenplay     = {{Plainlist|
* Susannah Grant
* Andy Tennant
* Rick Parks}}
| based on       =  
| starring       = {{Plainlist|
* Drew Barrymore
* Anjelica Huston
* Dougray Scott}}
| music          = George Fenton Andrew Dunn
| editing        = Roger Bondelli
| studio         = Flower Films
| distributor    = 20th Century Fox
| released       =  
| runtime        = 121 minutes  
| country        = United States
| language       = English
| budget         = $26 million   
| gross          = $98 million 
}} romantic comedy-drama original music Put Your Arms Around Me" is performed by the rock band Texas (band)|Texas.

The usual pantomime and comic/supernatural elements are removed and the story is instead treated as historical fiction, set in Renaissance-era France. It is often seen as a modern, Third-wave feminism|post-feminism interpretation of the Cinderella myth.   

==Plot== The Brothers Prince Henry, she abases herself. He gives her a bag of gold in exchange for her silence. She decides to use the money to rescue their servant, Maurice, whom the Baroness has sold to pay her debts. 
 King Francis and Queen Marie of France tell Henry that he must choose a bride before the upcoming masquerade ball, or he will have to wed the Spanish princess Gabriella. All the noble families receive an invitation and at first Danielle believes she is included. When collecting truffles, she meets Henry by the river in the company of da Vinci. Henry and Danielle engage in a lively debate before Danielle runs off. Henry searches for her, inviting her to visit the library of a nearby monastery. On the way home, they are accosted by the gypsies, who are amused by Danielles outrage and agree to release her with whatever she can carry. She picks up Henry and begins to walk away; they laughingly offer them a horse. Henry and Danielle spend the night in the gypsy camp, sharing their first kiss and arranging to meet again. 

The next morning, Danielle catches the Baroness and Marguerite stealing Danielles mothers dress and slippers for Marguerite to wear to the ball. After Marguerite insults her about her mothers death, Danielle punches Marguerite in the face and chases her around the manor until Marguerite threatens to throw Utopia into the fireplace. Danielle returns her mothers slippers to the Baroness in exchange for the book but Marguerite burns it in the fire anyway out of spite. Danielle is punished by whipping. Jacqueline tends to Danielle and says that Marguerite was wrong to say the comment about her mother. When Danielle meets Henry the next day, she wishes to tell him the truth, but is afraid he will reject her after he confesses his love. During a lunch with the Queen, the Baroness discovers that Danielle is the comtesse Henry has been spending time with and tells the Queen that Danielle is to be married to another man, in an effort to keep Henry and Danielle apart.  The Baroness confronts Danielle and accuses her of stealing the dress and slippers as they have disappeared. When Danielle refuses to produce them, the Baroness locks her in the pantry. Her childhood friend Gustave goes to da Vinci who comes to help free her, then makes her a pair of wings for the ball with her mothers dress and slippers. Moments after Danielle arrives at the ball, before she can tell Henry the truth, the Baroness exposes her identity in front of Henry. Shocked and enraged over her deception, Henry refuses any explanation from her. Heartbroken, Danielle flees the castle, losing one of her slippers. Leonardo finds it, and reprimands Henry for his attitude to no avail, leaving him with the slipper. The wedding of Henry and Gabriella begins, but seeing how unhappy she is, Henry realises its a mistake and calls the wedding off, letting her return to the man she really loves. He runs out to look for Danielle, only to find that the Baroness has sold her to vile landowner Pierre le Pieu. 

Danielle outsmarts Pierre and secures her freedom. Henry arrives as she is leaving Pierres mansion, apologizes for his ignorance and proposes to her by putting the glass slipper on Danielles foot. She cries into his arms and they kiss. The Baroness and her daughters are summoned to the court, assuming that Henry plans to propose to Marguerite. The Baroness is publicly accused of lying to the Queen, stripped of her title and she and Marguerite are threatened to be exiled to the Americas if no one will speak on their behalf.  At the last minute, Danielle speaks for them. Henry introduces Danielle as his wife and she asks the King and Queen to show them the same courtesy the Baroness has shown her. The Baroness and Marguerite are sent to work as laundry maids in the palace, while Jacqueline is spared punishment due to her kindness towards Danielle and becomes her lady-in-waiting. Later, Leonardo gives Danielle a portrait he painted of her as a wedding present. 

The Grande Dame tells The Brothers Grimm that Danielle was her ancestor, and that Danielle and Henry did live happily ever after, but the point is that they lived.

==Cast==
 
* Drew Barrymore as Danielle de Barbarac / "Comtesse Nicole de Lancret"
** Anna Maguire as 8-year-old Danielle
* Anjelica Huston as Baroness Rodmilla de Ghent
* Dougray Scott as Prince Henry
* Megan Dodds as Marguerite de Ghent
** Elizabeth Earl as young Marguerite
* Melanie Lynskey as Jacqueline de Ghent
** Alex Pooley as young Jacqueline
* Patrick Godfrey as Leonardo da Vinci
* Timothy West as King Francis
* Judy Parfitt as Queen Marie 
* Richard OBrien as Pierre le Pieu
* Jeroen Krabbé as Auguste de Barbarac
* Lee Ingleby as Gustave
** Ricki Cuttell as young Gustave
* Matyelok Gibbs as Louise
* Kate Lansbury as Paulette
* Walter Sparrow as Maurice
* Toby Jones as Royal Page Peter Gunn as Captain Laurent
* Jeanne Moreau as Grande Dame
* Joerg Stadler as Wilhelm Grimm
* Andy Henderson as Jacob Grimm
 

==Production==
Ever After was filmed in Super 35 mm film format; however, both the widescreen and pan-and-scan versions are included on the DVD. This is the only Super 35mm film directed by Tennant; his previous films were filmed with spherical lenses, while his subsequent films used anamorphic.
 de Fénelon, de Losse, de Lanquais, de Beynac as well as the city of Sarlat-la-Canéda.
  Head of a Woman (La Scapigliata).

==Critical reception== normalized rating out of 100 top reviews from mainstream critics, calculated a favorable score of 66 based on 22 reviews.   

Lisa Schwarzbaum from  ."    She also praised Anjelica Hustons performance as a cruel stepmother: "Huston does a lot of eye narrowing and eyebrow raising while toddling around in an extraordinary selection of extreme headgear, accompanied by her two less-than-self-actualized daughters -- the snooty, social-climbing, nasty Marguerite, and the dim, lumpy, secretly nice Jacqueline. "Nothing is final until youre dead", Mama instructs her girls at the dinner table, "and even then Im sure God negotiates". 

Chicago Sun-Times film critic Roger Ebert, while praising the film with three out of four stars, wrote that "The movie   is one of surprises, not least that the old tale still has life and passion in it. I went to the screening expecting some sort of soppy childrens picture and found myself in a costume romance with some of the same energy and zest as The Mask of Zorro. And I was reminded again that Drew Barrymore can hold the screen and involve us in her characters.   Here, as the little cinder girl, she is able to at last put aside her bedraggled losers and flower as a fresh young beauty, and she brings poignancy and fire to the role." 

==Home media==
The film was released on DVD on March 3, 1999.  On January 4, 2011, the film was released on Blu-ray Disc|Blu-ray. 

==Musical adaptation== musical is currently in the works, with the book and lyrics by Marcy Heisler and music by Zina Goldrich.  The musical was originally scheduled to have its world premiere in April 2009 at the Curran Theatre in San Francisco, but the pre-Broadway run was postponed.  In May 2012, it was announced that the project is back on track with Kathleen Marshall signing on to direct a Broadway run.    
 Jeremy Jordan Ashley Spencer James Snyder as Henry, Charles Shaughnessy as King Francis, and Tony Sheldon as Da Vinci. 

==See also==
* Ever After, the novelization by Wendy Loggia, based on the screenplay by Susannah Grant, Andy Tennant and Rick Parks

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 