Under the Lighthouse Dancing
{{Infobox film
| name           = Under the Lighthouse Dancing
| image          = UnderTheLighthouseDancing.jpg
| image_size     =
| caption        = Movie Poster
| director       = Graeme Rattigan
| producer       = David Giles
| writer         = David Giles Graeme Rattigan
| narrator       = Jack Thompson Jacqueline McKenzie Naomi Watts
| music          = Nerida Tyson-Chew Paul Murphy
| editing        = David Stiven
| distributor    = Village Roadshow Pictures
| released       =   (Australia)
| runtime        = 94 minutes
| country        = Australia
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} 

Under the Lighthouse Dancing is a 1997 Australian romantic drama film directed by Graeme Rattigan, based on a true story.

==Plot==
Three couples travel to Rottnest Island near Perth, Western Australia for the weekend. One of the couples announces that they intend to get married and when the bride-to-be tells the others that she is terminally ill, they make sure the wedding takes place that weekend.

==Cast== Jack Thompson as Henry
* Jacqueline McKenzie as Emma
* Naomi Watts as Louise
* Philip Holder as Garth
* Zoe Bertram as Juliet
* Aden Gillett as David
* Michael Loney as Father Flynn

==Reception==
Writing for Variety (magazine)|Variety, David Stratton praised the acting and production design of the film, but said that it had thin material, and that the ending was "hokey". {{Cite news
  | last = Stratton
  | first = David
  | author-link = David Stratton
  | title = Under the Lighthouse Dancing Review Variety
  | pages =
  | year =
  | date = 5 October 1997
  | url =http://www.variety.com/review/VE1117341153.html?categoryid=31&cs=1
  | accessdate =20 August 2009
  | postscript =   }}
 
 SBS gave the film a negative review concluding that the production "obviously wants to have an emotional impact, it just hasn`t done it very successfully." 

==Box Office==
Under the Lighthouse Dancing grossed $30,321 at the box office in Australia. 

==See also==
*Cinema of Australia

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 


 
 