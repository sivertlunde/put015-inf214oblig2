High Lane (film)
 
{{Infobox film
| name           = High Lane
| film name      = Vertige
| image          = High Lane Poster.jpg
| alt            = 
| caption        = 
| director       = Abel Ferry
| producer       = 
| writer         =  Johanne Bernard (scenario), Louis-Paul Desanges (scenario)
| narrator       = 
| starring       = {{Plainlist|
*Fanny Valette
*Johan Libéreau
*Nicolas Giraud}}
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = France
| language       = French, English
| budget         = 
| gross          = 
}}
 French horror film directed by Abel Ferry. A group of friends on vacation decide to venture onto a trail high up in the mountains that has been closed for repairs. The climb proves more perilous than planned, especially as they soon realize that they are not alone. The adventure turns into a nightmare.  

==Plot==
Teenagers Fred (Nicolas Giraud), his girlfriend Karine and their friends Chloé (Fanny Valette), William and Luke (Johan Libéreau) travel to Croatia to climb and hike. They find the starting point closed off with rocks but experienced climber Fred convinces the others, including Chloés boyfriend Luke, who wants to be close to her, to cross the track. As their journey progresses, they realize that the trail is more dangerous than they had first thought: the rope bridge collapses after their crossing. Fred climbs with Karine to help their friends but is wounded in a bear trap. Karine uses a rope to bring help their friends to the top, but they cant find Fred. When Chloé falls into a hole with another trap, they soon realize that their entertainment has turned into a fight to survive.

==Cast==
*Fanny Valette as Chloé
*Johan Libéreau as Loïc
*Nicolas Giraud as Fred
*Raphaël Lenglet as Guillaume
*Maud Wyler as Karine
*Justin Blanckaert as Anton
*Guilhem Simon as an adolescent

==References==
 

 
 
 


 
 