Waltz for Monica
 

{{Infobox film
| name           = Waltz for Monica
| image          =
| caption        =
| image_size     =
| director       = Per Fly
| producer       = Lena Rehnberg
| writer         = Peter Birro
| based on       =
| starring       = Edda Magnason
| music          = Peter Nordahl
| cinematography = Eric Kress
| editing        = Åsa Mossberg
| studio          = Film i Väst StellaNova Film SF
| released       =  
| runtime        = 111 minutes
| country        = Sweden
| language       = Swedish English
| budget         =
| gross          =
}} Swedish biographical film directed by Per Fly.   It is based on the true life and career of singer and actress Monica Zetterlund (Edda Magnason).

==Plot==
Monica makes it from a telephone operator to a national celebrity. But fame and party life take their toll.

==Cast==
*Edda Magnason as Monica Zetterlund
*Sverrir Gudnason as Sture Åkerberg
*Kjell Bergqvist as Monicas father
*Cecilia Ljung as Monicas mother
*Vera Vitali as Marika
*Johannes Wanselow as Beppe Wolgers
*Oskar Thunberg as Vilgot Sjöman
*Randal D. Ingram as Bill Evans
*Rob Morgan as Miles Davis
*Amelia Fowler as Ella Fitzgerald
*Clinton Ingram as Tommy Flanagan
*Harry Friedländer as Hasse Alfredson
*Andréa Ager-Hanssen as	 Lena Nyman

==Reception== Metro .  However, the film was criticized by documentary filmmaker Tom Alandh, who wrote an article in in the newspaper Dagens Nyheter where he objected to the films negative portrayal of Zetterlunds father. 

Waltz for Monica has received 11 Guldbagge Award nominations.

==Soundtrack==
A soundtrack album was released on the film by Universal Music charting in various Scandinavian charts, notably Sweden, Finland and Denmark.

===Tracklist===
#"Sakta vi gå genom stan"
#"Hit The Road Jack"
#"Monicas vals"
#"O vad en liten gumma kan gno"
#"En gång i Stockholm"
#"It Could Happen to You"
#"Gröna små äpplen"
#"Trubbel"
#"Du"
#"Bedårande sommarvals"
#"I Cant Give You Anything But Love"
#"I New York"
#"Monica Z – Svit ur filmen"

===Charts===
{|class="wikitable sortable"
!Chart (2006) Peak position
|- Danish Albums Chart {{cite web
| url=http://danishcharts.com/showitem.asp?interpret=Soundtrack+%2F+Edda+Magnason&titel=Monica+Z&cat=a
| title=Soundtrack / Edda magnason - Monica Z in Danish charts
| publisher=danishcharts.com 
| accessdate=2014-03-28}}  29
|- The Official Finnish Albums Chart {{cite web
| url=http://finnishcharts.com/showitem.asp?interpret=Soundtrack+%2F+Edda+Magnason&titel=Monica+Z&cat=a
| title=Soundtrack / Edda magnason - Monica Z in Finnish charts
| publisher=danishcharts.com 
| accessdate=2014-03-28}}  50
|- Swedish Albums Chart {{cite web
| url=http://swedishcharts.com/showitem.asp?interpret=Soundtrack+%2F+Edda+Magnason&titel=Monica+Z&cat=a
| title=Soundtrack / Edda magnason - Monica Z in Swedish charts
| publisher=swedishcharts.com 
| accessdate=2014-03-28}}  3
|-
|}

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 


 
 