Footloose Widows
{{infobox film
| title          = Footloose Widows
| image          =
| imagesize      =
| caption        =
| director       = Roy Del Ruth William McGann(asst director)
| producer       = Warner Brothers
| writer         = Darryl F. Zanuck Roy Del Ruth(uncredited)
| based on       =  
| starring       = Louise Fazenda
| music          =
| cinematography = David Abel Charles Van Enger
| editing        =
| distributor    = Warner Brothers
| released       =  
| runtime        = 70 minutes
| country        = USA
| language       = Silent film English intertitles
}}
Footloose Widows is a 1926 silent film feature comedy produced and distributed by Warner Brothers, directed by Roy Del Ruth and starring Louise Fazenda and Jacqueline Logan.  

A print is preserved in the Library of Congress collection.  

==Cast==
*Louise Fazenda - Flo
*Jacqueline Logan - Marian
*Jason Robards, Sr. - Jerry (*billed Jason Robards)
*Douglas Gerrard - Grover
*Neely Edwards - The Ex-Mayor
*Arthur Hoyt - Henry
*Jane Winton - Mrs. Drew
*Mack Swain - Ludwig, Marians husband in retrospect
*John Miljan - Mr. Smith Eddie Phillips - Tuxedo Eddie
*Henry A. Barrows - Hotel Manager (*as Henry Barrows)

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 