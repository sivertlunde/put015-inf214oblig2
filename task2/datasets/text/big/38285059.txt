Closed Curtain
{{Infobox film
| name           = Closed Curtain
| image          = File:Closed Curtain poster.jpg
| caption        = Theatrical release poster
| director       = Jafar Panahi Kambuzia Partovi
| producer       = Jafar Panahi Hadi Saeedi
| writer         = 
| screenplay     = Jafar Panahi
| story          = 
| starring       = Kambuzia Partovi Maryam Moqadam Jafar Panahi
| music          = 
| cinematography = Mohamad Reza Jahanpanah
| editing        = Jafar Panahi
| studio         = Jafar Panahi Film Productions 
| distributor    = 
| released       =  
| runtime        = 106 minutes
| country        = Iran
| language       = Persian
| budget         = 
| gross          = $27,091   
}}
Closed Curtain ( ) is a 2013 Iranian drama film by Jafar Panahi and Kambuzia Partovi. It premiered at the 63rd Berlin International Film Festival      on February 12, 2013  where Panahi won the Silver Bear for Best Script.    It was shot secretly at Panahis own beachfront villa on the Caspian Sea. Panahi stated that he began shooting the film in a state of melancholy but managed to recover by the films completion. Closed Curtain is Panahis second film since his Jafar Panahi#Imprisonment|20-year ban on filmmaking after 2011s This Is Not a Film.

The film was selected as the closing film of the 2013 Hong Kong International Film Festival. 

== Plot ==
An unnamed screenwriter (Kambozia Partovi) arrives at a secluded three-story villa on the Caspian Sea. He secretly brings along his pet dog named "Boy". Dogs are considered unclean under Islamic rule and the writer intends to hide "Boy" from authorities as he tries to get some writing done. The writer shaves his head to disguise his identity and covers all the windows in the villa with black, opaque material.

One night during a thunderstorm Melika (Maryam Moqadam) and her brother Reza (Hadi Saeedi) break into the villa. They tell the writer that they have fled from an illegal beach party in which alcohol was consumed and are hiding from the police. The writer demands that they leave, but Reza states that his sister is suicidal and then leaves Melika there while he looks for a car.

The presence of Melika begins to unsettle the writer. Melika speaks to the writer cryptically and theatrically, often lying and demanding to know about personal details from the writers life. The writer becomes increasingly paranoid and begins to suspect Melika of being a police spy. Melika wants to open the curtains against the writers protests. Melika suddenly vanishes and the writer is left alone. When he hears the back patio door smashed open, the writer and "Boy" hide and listen to the sounds of the house being ransacked by thieves.

The film abruptly changes and becomes more surrealistic when film director Jafar Panahi and other film crew members appear in the villa, with the entire film up that that point having been fictitious. Panahi is seen in everyday situations, such as eating, talking to workers who repair the patio window and interacting with friends. Characters from earlier in the film begin to haunt Panahi, especially Melika. Melika leaves the villa and goes into the water. Panahi follows her there, but the film is suddenly rewound and he finds himself in the villa again. On a cell phone he looks at pictures of filming in the house, showing the writer as he first meets Melika and Reza. At the end of the film Panahi leaves the villa as Melika looks on.

== Cast ==
* Kambozia Partovi as the writer
* Maryam Moqadam as Melika
* Jafar Panahi as himself
* Hadi Saeedi as Reza, Melikas brother
* Azadh Torabi as Melikas sister
* Zeynab Khanum as himself 
* Abolghasem Sobhani as Agha Olia
* Mahyar Jafaripour as the young brother
* Ramin Akhariani as a worker
* Sina Mashyekhi as a worker

== Production == The Fish, The Circle Border Café. The film was shot in secret with a crew of four to five people at Panahis home.  Partovi said that scenes with curtains on the windows were shot last so as to avoid raising suspicion or getting Panahi into trouble.  The subject of suicide is discussed throughout the film and Panahi has been said to have suffered from depression since his legal troubles began. When asked about the subject at the Berlin Film festival, Partovi stated that Panahi "was not constantly thinking about suicide, no, because then he wouldnt have been able to make the film. But if I imagine myself unable to work and just sitting at home, then I am sure I would start to think about suicide." Panahi stated that he was in a state of deep melancholy when shooting began, but that he recovered by the films completion. 

== Release ==
Closed Curtain was announced as being selected to screen in competition of the 63rd Berlin International Film Festival on January 11, 2013.  Festival director Dieter Kosslick is a long time supporter of Panahi and said that he "asked the Iranian government, the president and the culture minister, to allow Jafar Panahi to attend the world premiere of his film at the Berlinale."  The film premiered at the festival on February 12, 2013 and later won the Silver Bear for Best Script. 

=== Iranian response ===
In response to the Best Script Award Javad Shamaqdari, the head of the Iranian Students News Agency, stated "We have protested to the Berlin Film Festival. Its officials should amend their behavior because in cultural and cinematic exchange, this is not correct" and added "Everyone knows that a license is needed to make films in our country and send them abroad but there are a small number who make films and send them out without a license. This is an offense ... but so far the Islamic Republic has been patient with such behavior."  In response to the ISNAs complaint the Berlin Film Festival released the statement "We would very much regret if the screening will have any legal consequences for the filmmakers."  On February 28, 2013 Iranian authorities confiscated the passports of Partovi and Moqadam. This prevents them from traveling outside of Iran to promote the film abroad or at film festivals,  such as the 37th Hong Kong International Film Festival where it is scheduled to screen on April 2, 2013. 

== Reception ==
Hanns-Georg Rodek of Die Welt said that the film was "simultaneously a film about courage and cowardice in the repression...  simultaneously allegorical and concrete" and that Melika was the "embodiment of free thought".  Jay Weissberg of Variety magazine|Variety compared the film to Luigi Pirandellos Six Characters in Search of an Author and to the theater of the absurd, but said that "the multitude of thematic levels has the overall effect of weakening the films impact."  Deborah Young of The Hollywood Reporter praised the films cinematography and lighting despite its low budget and said that the film was "not an easy film to relate to – in fact it deliberately short-circuits any emotional response from the audience-- and will have to rely largely on Panahi’s reputation to get off the ground in foreign art venues."  Eric Kohn of IndieWire gave the film an A- rating, but called it "less a finished movie than a cry for help, its enigmatic trajectory allows viewers to empathize with Panahi exclusively through his ideas."  Patrick Gamble of cine-vue.com called the film "an existential example of meta filmmaking disguised as a humanist chamber piece about political repression" but criticized the films third act, called Panahis role an "ill-advised cameo, the films self-reverential aesthetic tips the scales, turning a heavy-handed, yet enjoyable metaphor for his creative repression into an overblown and jarringly personal odyssey."  Rui Martins of Pravda called it "a film allegory about his inability to connect with the outside world." 

== References ==
 

== External links ==
* 
* 
* 

 

 
 
 
 