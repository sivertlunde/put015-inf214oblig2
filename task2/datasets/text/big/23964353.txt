Thingyan Moe
{{Infobox film
| name           = Thingyan Moe
| image          = Thingyan Moe.jpg
| caption        = DVD cover for Thingyan Moe.
| director       = Maung Tin Oo
| producer       = Thuriya Pyinnya Films
| writer         =
| starring       = Nay Aung, Zin Wine, Khin Than Nu, May Than Nu
| music          =
| cinematography = Chit Min Lu
| editing        = Myint Khine
| distributor    =
| released       =  
| runtime        =
| country        = Myanmar
| language       = Myanmar
| budget         =
| gross          =
}}
 Burmese film directed by Maung Tin Oo and starring Nay Aung, Zin Wine, Khin Than Nu and May Than Nu in pivotal roles. The movie follows the life of a musician from 1959 to 1982, with many of the scenes set against traditional Thingyan celebrations. 

==Plot==
Thingyan Moe is the story of Thingyan. Story begans with a poor pianist Nyein Maung and Khin Khin Htar a wealthy teen girl of Mandalay. Despite the difference between them, they fell in love deeply as so Nyein Maung was asked to elope Htar but failed as his pupils mother was sick and he headed to hospital with them instead. As per Nyein Maungs absence, Htar was brought back by his mother and forced to accept arrange marriage.

==Cast==
*Nay Aung as .... Nyein Maung
*Zin Wine as .... Thet Htway
*Khin Than Nu as ... Khin Khin Htar
*May Than Nu as ...  

==Awards==
; 1985: Myanmar Motion Picture Academy Awards Academy Award for Best Picture : Thuriya Pyinnya Films Academy Award for Best Director : Maung Tin Oo Academy Award for Best Cinematography : 	Chit Min Lu

==References==
 

 
 

 