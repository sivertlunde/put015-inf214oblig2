A Martian Christmas
 
{{Infobox film
| name           = A Martian Christmas
| image          = 
| alt            = 
| caption        = U.S. DVD cover
| director       = José Alejandro García Muñoz
| producer       = Jose C. Garcia de Letona
| screenplay     = Linda Miller Alex Mann John Behnke Rob Humphrey
| story          = Linda Miller Alex Mann David Lodge Robert Mark Klein Katie Leigh Dave Mallow
| music          = Guy Michelmore
| cinematography = 
| editing        = Sean Stack
| studio         = Porchlight Entertainment Ánima Estudios Telegael
| distributor    = Porchlight Entertainment
| released       =  
| runtime        = 45 minutes
| country        = United States  Mexico  English
| budget         = 
| gross          = 
}} American  Christmas animation|animated film, released direct-to-video on November 11, 2009. The film was produced by Ánima Estudios and Porchlight Entertainment.    This film was directed by José Alejandro García Muñoz.
 television movie",     there is currently no further information about the films air date or network.

==Synopsis==
A Martian boy, Kip, was caught up in an intergalactic race to save both Christmas and Earth.

==Cast==
*Cindy Robinson as Kip, Mary Kate
*Dino Andrade|K.C.D. Shannon as Zork, Dwight David Lodge as Gleeb, Santa Claus, Martian Leader
*Mac Grave as Ned, Drang, Martian Scientist Robert Mark Klein as Office Manager, Martian Officer
*Katie Leigh as Roxy
*Dave Mallow as VOX, Shopper

==Production==
On 6 October 2008, The Hollywood Reporter reported that A Martian Christmas is in development from Porchlight Entertainment and Ánima Estudios.  Post-production services was handled by Telegael.

==Reception==
The film received mixed reviews. IMDB reported the film received a 7.1 audience rating out of 10.     Sierra Filucci of Common Sense Media gave this film 2 out of 5 stars and said, "Unfortunately the build-up to the main part of the story -- the journey to Earth -- takes so long and is packed with so much backstory that kids and even adults might get lost, or just bored. Once the trip begins, things pick up and its sort of interesting to see humans and Christmas from outsiders eyes." 

==Release== PorchLight Home Entertainment. Due to its obscurity, it remains one of the rarest films to this day, most likely due to lack of promotion, and the fact that this film was primarily produced in Mexico, as it is difficult for a Mexican animated production to get into the United States market, according to producer Fernando de Fuentes.    The DVD of the film is only available on online shopping sites, such as eBay and Amazon.com.

==References==
 

==External links==
*  on Ánima Estudios
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 