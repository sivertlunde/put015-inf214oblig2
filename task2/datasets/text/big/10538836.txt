Forest of the Damned
 
 
{{Infobox Film
| name           = Forest of the Damned
| image          = Forest of the Damned.jpg
| image_size     =
| caption        =
| director       = Johannes Roberts
| producer       = Miguel Ruz
| writer         = Johannes Roberts Joseph London
| narrator       =
| starring       = Nicole Petty Daniel MacLagan 
Tom Savini Shaun Hutson  Richard Cambridge 
Marysia Kay James Fusco
| music          = Ollie Knight
| cinematography = John Raggett
| editing        = Richard Mitchell
| studio         = Gatlin Pictures
| distributor    = Universal Music & Video
| released       =   (Cannes)
| runtime        = 95&nbsp;minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
Forest of the Damned (known in the United States as  horror 
feature film directed by Johannes Roberts.
==Plot==
 
Five friends have a car accident and fall into the clutches of fallen angels who first seduce their victims and then kill them. A deadly battle of seduction, fear, and blood consumes the group as these angels have an insatiable appetite for human flesh.

==Cast==
* Nicole Petty as Molly
* Daniel MacLagan as Judd
* Tom Savini as Stephen
* Shaun Hutson as himself
* Marysia Kay as Angel 1 Richard Cambridge as Emilio
* Sophie Holland as Ally
* David Hood as Andrew
* Eleanor James as Angel 2

==Release==
The film premiered on 13 May 2005 at the Cannes Film Festival and was released in the UK on 26 November 2005.

==Reception==
 

==Sequel==
 
Forest of the Damned 2, directed by Ernest Riera, was released in 2011. Eleanor James and Marysia Kay reprise their roles as bloodthirsty fallen angels.

==External links==
*  

 
 
 
 
 
 


 