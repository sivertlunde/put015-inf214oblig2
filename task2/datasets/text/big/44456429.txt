Richard's Wedding
{{Infobox film
| name           = Richards Wedding
| image          = 
| caption        = 
| director       = Onur Tukel
| producer       = Jason Klorfein Onur Tukel Devoe Yates Andrew Krucoff (associate producer) Rachel Wolther (associate producer)
| writer         = Onur Tukel See Cast
| music          = 
| cinematography = Jason Banker Jorge Torres-Torres
| editing        = Onur Tukel
| distributor    = Devolver Digital
| released       =  
| runtime        = 86 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} ensemble cast are actors Josephine Decker, Randy Gambill, Lawrence Michael Levine, Jennifer Prediger, and Tukel himself.

==Cast==
*Thomas J. Buchmueller as Simon
*Josephine Decker as Phoebe
*Dustin Guy Defa as Taco
*Jamie Dobie as Deedee
*Randy Gambill as Louis
*Heddy Lahmann as Amy
*Lawrence Michael Levine as Richard
*Theresa Lu as Lyndsey
*Oona Mekas as Kristin
*Jennifer Prediger as Alex
*Darrill Rosen as Darrill Rosen
*Adam Schartoff as Andrew
*Onur Tukel as Tuna

==Release==
===Media=== VOD through Devolver Digital.

==Reception==
===Critical response===
The film received a generally mixed response from critics.  David DeWitt of The New York Times called the film "a slice of the John Cassavetes, John Sayles and Richard Linklater life" and added "Richard’s Wedding is modest in umpteen ways, and if your cinematic diet craves the substantial, be forewarned. But the film’s DIY ethos has a heart, which makes the script’s small talk about humanity not so small after all."   Ronnie Scheib of Variety (magazine)|Variety added, "Turkel constantly undermines the feel-good with the ridiculous and vice versa, vacillating between infantile insults and professions of affection, a duality that ultimately wears thin. Still, if the message sometimes veers uncomfortably toward sententiousness, it reaches its comic apogee in Alex’s born-again ex-junkie cousin Louis (Randy Gambill), now an online-ordained minister. Proffering thought-provoking questions about the nature of friendship and demanding crack with equal sweetness, his helpfulness and obliviousness run neck and neck." 

Conversely, Michael Atkinson of the The Village Voice viewed the film very unfavorably, calling it "A handheld New York indie that dallies in the queasy DMZ between hipster-narcissism and a self-satisfied critique of hipster-narcissism".   Kenji Fujishima of Slant Magazine gave the film a more mixed 2 out of 4 stars, saying "Richards Wedding may be admirably nervy in some ways, but in the directors preference for above-it-all contempt over tough-minded empathy, the film ends up seeming little more than an 89-minute hatefest, with no special insights into human nature to make the endeavor worthwhile." 

==References==
 

==External links==
* 

 
 
 
 
 
 