So Big (1924 film)
{{Infobox film
| name           = So Big
| image          = Colleen so big field.jpg
| caption        = Colleen Moore as Selina Peake in So Big
| director       = Charles Brabin
| producer       = Earl Hudson
| writer         = Edna Ferber (novel) Adelaide Heilbron (scenario) Earl Hudson (adaptation)
| starring       = Colleen Moore
| cinematography = Ted D. McCord
| editing        = Arthur Tavares Marion Fairfax (edit. director) Associated First National
| released       = December 28, 1924 (USA)
| runtime        = 9 reels
| country        = United States
| language       = Silent films (English intertitles)
}}
 novel of Associated First National.  Unseen for decades, it is considered a lost film. Only a trailer survives at the Library of Congress.  

==Cast==
*  (March 1925), p. 45 
*Joseph De Grasse - Simeon Peake John Bowers - Pervus DeJong
*Ben Lyon - Dirk DeJong
*Wallace Beery - Klaus Poole
*Gladys Brockwell - Maartje Poole
*Jean Hersholt - Aug Hempel
* Charlotte Merriam - Julie Hempel
*Dot Farley - Widow Paarleburg
*Ford Sterling - Jacob Hoogenduck
*Frankie Darro - Dirk DeJong (as a child)
*Henry Hebert - William Storm (as Henry Herbert)
*Dorothy Brock - Dirk DeJong (as an infant)
*Rosemary Theby - Paula Storm
*Phyllis Haver - Dallas OMeara
*Valentine Black - Child (unbilled)
*Joe Coppa - A boy (unbilled)

==Reception==
Colleen Moores role in So Big quite different from her usual jazz baby/flapper parts. Because Moores part in the film was so different from the roles she had become known for following Flaming Youth, there was an outcry against her.  Forgotten was that prior to her success in Flaming Youth, the vast majority of her roles had been dramatic in character, a few even tragic. She had not only played mothers, but mothers who had lost children (Broken Chains) and mothers who died (Dinty). Still, her performance was generally well received. The film, however, was not so big a success as her previous few comedies, and so Sally was lined up to follow So Big. Moore wrote in Silent Star that upon meeting Edna Ferber some years after the film, the author gave her approval of Moores performance. 

==See also==
*List of lost films

==References==
 

==External links==
* 
*Posters for the 1924 version of So Big  ,  

 

 
 
 
 
 