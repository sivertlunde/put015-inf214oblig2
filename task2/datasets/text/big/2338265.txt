A Samba for Sherlock
{{Infobox film
| image          = ASambaForSherlock2001Poster.jpg
| caption        = Theatrical release poster
| director       = Miguel Faria, Jr.
| producer       = Caíque Martins Ferreira Marcelo Laffitte
| writer         = Marcos Bernstein
| based on       =   Anthony ODonnell Maria de Medeiros Marco Nanini
| music          = Edu Lobo
| cinematography = Lauro Escorel
| editing        = Diana Vasconcellos
| studio         = Sky Light Cinema 
| distributor    = Columbia Pictures
| released       =  
| runtime        = 124 minutes
| country        = Brazil Portugal
| language       = Portuguese English French Italian Spanish
| budget         = R$4–10 million     
| gross          = R$2,275,052  
}}
O Xangô de Baker Street (English title: A Samba for Sherlock) is 2001 Brazilian-Portuguese film directed by Miguel Faria, Jr., based on the novel of the same by Jô Soares. The film was first announced in 1996, but the filming only started on September 1998 in Porto, Portugal,    lasting until 1999. 

==Plot==
  violin presented by him to the charming widow Baroness Maria Luiza. The actress suggests to the monarch to hire her friend, the legendary   had been killed and mutilated, her ears cut off and a violin cord strategically placed on her body by the perpetrator. Later, under the heat of the tropical sun, the lives of Holmes and Doctor Watson are changed forever, as they find themselves neck-deep in a cultural milieu that portrays all standard Brazilian stereotypes.

==Cast==
*Joaquim de Almeida	... 	Sherlock Holmes
*Marco Nanini	... 	Mello Pimenta Anthony ODonnell	... 	Doctor Watson
*Maria de Medeiros	... 	Sarah Bernhardt
*Cláudia Abreu	... 	Baroness Maria Luiza
*Caco Ciocler	... 	Miguel Solera de Lara
*Marcello Antony	... 	Marquês de Salles Emperor Pedro II Empress Theresa Christina
*Thalma de Freitas	... 	Ana Candelária
*Letícia Sabatella	... 	Esperidiana
*Malu Galli 	... 	Chiquinha Gonzaga
*Jô Soares  	... 	Desembargador Coelho Bastos
*Walney Costa	... 	José White Lafitte|José White Comte dEu Princess Isabel

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 