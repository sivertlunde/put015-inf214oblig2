Lovers and Luggers
 
{{Infobox film
| name           = Lovers and Luggers
| image          = Lovers_and_Luggers.jpg
| image_size     = 
| caption        = Flyer for theatrical release
| director       = Ken G. Hall
| producer       = Ken G. Hall Frank Harvey   Edmund Barclay Gurney Slade
| narrator       = 
| starring       = Lloyd Hughes   Shirley Ann Richards
| music          = Hamilton Webber George Heath
| editing        = William Shepherd
| studio         = Cinesound Productions
| distributor = British Empire Films (Aust) Paramount Pictures (UK) Astor Pictures (USA)
| released       = 31 December 1937 (Australia) 1940 (USA)
| runtime        = 99 mins (Australia) 65 mins (USA)
| country        = Australia
| language       = English
| budget         = ₤24,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 180. 
}}
Lovers and Luggers is a 1937 Australian film directed by Ken G. Hall. It is an adventure melodrama about a pianist (Lloyd Hughes) who goes to Thursday Island to retrieve a valuable pearl. 

It was retitled Vengeance of the Deep in the USA and United Kingdom.

==Synopsis==
In London, concert pianist Daubenny Carshott is feeling dissatisfied with his life and wanting a masculine adventure; he also desires the beautiful Stella Raff. Stella agrees to marry him if he brings back a large pearl with his own hands from Thursday Island. Daubenny notes a painting in Stellas apartment from "Craig Henderson" but when asked Stella is evasive about the artist.

Daubenny travels to Thursday Island where he buys a lugger and a house from the villainous Mendoza. He makes friends on the island, including another diver, Craig Henderson, the drunken duo of McTavish and Dorner, and the boisterous Captain Quidley. He also meets Quidleys daughter, the beautiful Lorna, who likes to dress in mens clothing so she can walk around on her own at night. Lorna and Daubenny become friends and she secretly falls in love with him but Daubenny assumes she is in love with Craig. 

Captain Quidley, teaches Daubenny to dive. Quidley, Lorna, Daubenny and Mendoza all go out diving for pearls. Daubenny finds a pearl, to the fury of Mendoza, who believes since Daubenny used his lugger that Mendoza should have a share. Daubenny disagrees and the two men fight on board the lugger, causing the pearl to drop over the side. 

Both men get in their diving suits and go down to retrieve the pearl. Mendoza dies and Daubenny is trapped. Bill Craig risks his life to rescue Daubenny.

Back on Thursday Island, Stella has arrived, accompanied by an aristocratic friend, Archie. Daubenny discovers that Bill Craig is Craig Henderson, and was also in love with Stella, and sent on a similar mission to find a pearl. Daubenny and Craig both reject Stella.

Daubenny decides to leave Thursday Island on his boat. Lorna reveals she is in love with him, not Craig and the two kiss and decide to get married. They sail off into the sunset with Captain Quidley. 
==Cast==
 
*Lloyd Hughes as Daubenny Carshott
*Shirley Ann Richards as Lorna Quidley
*Sidney Wheeler as Captain Quidley
*James Raglan as Bill Craig/Craig Henderson
*Elaine Hamill as Stella Raff  Frank Harvey as Carshotts manager
*Ronald Whelan as Mendoza
*Alec Kellaway as McTavish
*Leslie Victor as Dormer
*Campbell Copelin as Archie
*Charlie Chan as Kishimuni
*Marcelle Marnay as Lotus
*Horace Cleary as China Tom
*Claude Turton as Charlie Quong
*Bobbie Hunt as Lady Winter
*Paul Furness as Professor of psychology
*Charles Zoli as Carshotts valet
 

==Original Novel==
{{infobox book |  
| name          = Lovers and Luggers
| title_orig    = 
| translator    = 
| image         = 
| caption =  Gurney Slade,
| illustrator   = 
| cover_artist  = 
| country       = UK
| language      = English
| series        = 
| genre         = adventure
| publisher     = 
| release_date  = 1928
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}} Gurney Slade, from whom Cinesound obtained the film rights in late 1936.  In the novel, Daubenny travels to "Lorne" (Broome, thinly-disguised) rather than Thursday Island. Lorna is not related to Captain Quid, but actually is Stellas half-sister. There are two other British expatriates diving for pearls in addition to Craig, Chillon and Major Rawlings. Daubney does not romance Lorna and is reunited with a reformed Stella at the end. Lorna winds up with Craig. 
 Broome Ken Frank Harvey relocate the story to Thursday Island because it was easier to access. 
  
==Production==
===Casting=== The Broken Melody for Hall.

This was the first of what would be several character roles Alec Kellaway played for Ken G. Hall.

===Shooting===
Hall was enthusiastic about the project because of his love for the tropics, although budget considerations meant most of the film had to be shot in the studio, with only the second unit going to Thursday Island under Frank Hurley. Hurley also shot some footage at Port Stephens and Broken Bay.  Cinesound built one of its largest ever sets to recreate Thursday Island. 

A tank was built to shoot the underwater scenes. However the water was not clear, so the scenes were shot at North Sydney Olympic Pool.  

Hall would direct scenes on boats by radio. 

Stuart F. Doyle had resigned from Cinesound during production but was kept on to supervise the finishing of the movie. 

Reports of the budget ranged from ₤18,000     to ₤24,000.

==Reception==
A charity ball was held to promote the release of the film. 
The film was released in both the US and England.  It was the last Australian film sold to Britain as a British quota picture before the British quota laws were amended. 
===Critical===
Reviews were positive, the critic from the Sydney Morning Herald calling it "Australias finest picture to date."  
===Box Office===
The movie was a slight disappointment at the box office, and Ken G. Hall thought this helped make Greater Unions then-managing director Norman Rydge disillusioned with feature production.  However Hall said in 1972 that "I think I like it best of all the pictures that Ive made. Because of the backgrounds. Id go tomorrow to make a film about the Tropics." 

==References==
 

==External links==
*  in the Internet Movie Database
*  at Australian Screen Online
*  at Oz Movies

 

 
 
 
 
 