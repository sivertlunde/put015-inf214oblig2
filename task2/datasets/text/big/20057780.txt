The Hunchback and the Dancer
 
{{Infobox film
| name           = The Hunchback and the Dancer
| image          = 
| caption        = 
| director       = F. W. Murnau
| producer       = 
| writer         = Carl Mayer
| music          = 
| cinematography = Karl Freund
| editing        = 
| distributor    = Helios Film
| released       =  
| runtime        = 50 minutes
| country        = Weimar Republic Silent German intertitles
| budget         = 
}}
 silent German horror film directed by F. W. Murnau. This is now considered to be a lost film.   

==Cast==
* Sascha Gura as Gina
* John Gottowt as James Wilton
* Paul Biensfeldt as Smith
* Henri Peters-Arnolds as Percy
* Bella Polini as Tänzerin
* Werner Krauss (unconfirmed)
* Lyda Salmonova (unconfirmed)
* Anna von Palen as  Smiths mother

==See also==
*List of lost films

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 
 