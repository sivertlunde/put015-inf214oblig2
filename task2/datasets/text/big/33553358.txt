The Indian Tomb (1938 film)
{{Infobox film
| name           = The Indian Tomb
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Eichberg
| producer       = Richard Eichberg (producer) Herbert Engelsing (line producer)
| writer         = Thea von Harbou (novel) Richard Eichberg (writer) Hans Klaehr (writer) Arthur Pohl (writer)
| narrator       = 
| starring       = See below
| music          = Harald Böhmelt
| cinematography = Ewald Daub W. Meyer-Bergelt Hans Schneeberger Hugo O. Schulze
| editing        = Willy Zeyn
| studio         = 
| distributor    = 
| released       = 1938
| runtime        = 94 minutes 100 minutes (Germany)
| country        = Nazi Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

The Indian Tomb (originally Das indische Grabmal) is a 1938 German film directed by Richard Eichberg.

== Plot summary ==
 

=== Differences from novel ===
 

== Cast ==
*Philip Dorn as Maharadscha von Eschnapur
*Kitty Jantzen as Irene Traven La Jana as Indira, eine indische Tänzerin
*Theo Lingen as Emil Sperling
*Hans Stüwe as Peter Fürbringer, Architekt
*Alexander Golling as Prinz Ramigani, Vetter des Maharadscha
*Gustav Diessl as Sascha Demidoff, Ingenieur
*Gisela Schlüter as Lotte Sperling
*Karl Haubenreißer as Gopal, Würdenträger in Eschnapur
*Olaf Bach as Sadhu, Radscha eines Bergvolkes
*Rosa Jung as Myrrha, Vertraute der Maharani
*Albert Hörrmann as Ragupati, im Dienste Ramiganis
*Gerhard Bienert as Ratani, Werkmeister
*Valy Arnheim as Wachmann Ramura
*Carl Auen as Indischer Nobiler
*Rudolf Essek as Hotelgast in Bombay
*Jutta Jol as Indische dienerin bei irene traven
*Fred Goebel as Indischer Ingenieur Klaus Pohl as Inder, der beim Fest nach den Gewehren fragt
*Paul Rehkopf as Indischer Nobiler
*Gerhard Dammann
*Josef Peterhans as Indischer Nobiler

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 

 