The Evil Below
{{Infobox film
| name           = The Evil Below
| image          = Evil below.jpg
| image_size     = 
| caption        = Region 2 DVD Release Cover
| director       = Wayne Crawford Jean-Claude Dubois
| producer       = Barrie Saint Clair Arther Payne
| narrator       =  Paul Siebert
| music          = Julian Laxton
| cinematography = Keith Dunkley
| studio         = Gibraltar Entertainment Legend Film Productions Image Organization
| distributor    = Raedon Home Video
| released       = United States:  July 1, 1989 (Video)
| runtime        = 98 minutes
| country        = United States
| language       = English
}} horror film Paul Siebert and June Chadwick.
 The Rift (Endless Descent).

==Plot==

Max Cash is a down-on-his luck fisherman and charter boat captain living in the Bahamas when his life takes a turn when he meets Sarah Livingstone, a tourist who is seeking to find the treasure of accursed shipwreck, The El Diablo which has been rumored to have sunk on an offshore reef near one of the many islands. Max and Sarah then team up to locate the wreck while dodging a local crime boss as well as a mysterious businessman who claims that the wreck is guarded by supernatural forces in form of a sea monster that no one claims to have ever seen and survived. 

==Cast==
*Wayne Crawford as Max Cash
*June Chadwick as Sarah Livingstone
*Sheri Able as Tracy
*Ted Le Plat as Adrian Barlow Graham Clarke as Ray Calhoun
*Liam Cundill as Ray Calhoun Junior
*Gordon Mulholland as Max Cash Senior Brian OShaughnessy as Father Shannon
*Peter Terry as Constable Chambers Paul Siebert as Guido

==External links==
*  

 
 
 

 