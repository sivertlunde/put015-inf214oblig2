A Cosmic Christmas
 
 
{{Infobox film
| name         = A Cosmic Christmas
| image        = CosmicChristmas-Nelvana-1977.png
| caption      = Scene from the film
| director     = Clive A. Smith Michael Hirsh Patrick Loubert
| screenplay   = Ida Nelson Laura Paull
| story        = Patrick Loubert
| music        = Sylvia Tyson
| starring     = Joey Davidson Martin Lavut Patrick Moffatt Nick Nichols
| studio       = Nelvana Limited CBC (TV, 1977) Warner Home Video (VHS, 1980s)
| released     =  
| country      = Canada
| runtime      = 26 minutes
}}

 
A Cosmic Christmas is the first television special produced by the Canadian animation company, Nelvana. It premiered on 4 December 1977 in Canada on CBC Television.

==Plot==
Three aliens from an unknown planet, who bear a strong resemblance to the Biblical Magi, visit earth to know the true meaning of Christmas. Peter, a young boy, and Lucy, his goose, are the first to encounter them. Unable to find the true meaning of Christmas in town, Peter takes them to his familys house in the woods. While Peters grandmother tells the aliens about her memories of Christmas, Marvin, one of the towns bullies, steals Lucy. In the chase to rescue Lucy, Marvin falls through the ice in a lake. Peter attempts to rescue him but falls into the lake as well. The townsfolk, who were out searching for the aliens, attempt to save the boys but their human chain isnt long enough to reach them. The three aliens, who had sworn not to interfere with events on earth, decide to help in order to learn the meaning of Christmas. The rescue effort is successful. The townsfolk are quick to condemn Marvin for stealing Lucy, but have a change of heart when they realize that Marvin stole Lucy because he had nothing to eat. Peter offers Marvin and his friends the chance to join them for Christmas dinner and the aliens realize that family and the spirit of forgiveness are the true meaning of Christmas.

==Cultural reference==
The soundtrack was sampled on Dan the Automators remix album Wanna Buy A Monkey?

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 
 