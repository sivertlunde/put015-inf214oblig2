Pudd'nhead Wilson (film)
{{Infobox film
| name           = Puddnhead Wilson
| image          = 
| alt            = 
| caption        = 
| director       = Frank Reicher
| producer       = Jesse L. Lasky Margaret Turnbull
| based on       =  
| starring       = Theodore Roberts Alan Hale, Sr. Thomas Meighan Florence Dagmar Jane Wolfe Ernest Joy
| music          = 
| cinematography = Walter Stradling
| editing        = 
| studio         = Jesse L. Lasky Feature Play Company
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 comedy silent Margaret Turnbull. The film stars Theodore Roberts, Alan Hale, Sr., Thomas Meighan, Florence Dagmar, Jane Wolfe and Ernest Joy. The film was released on January 31, 1916, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Theodore Roberts as Puddnhead Wilson
*Alan Hale, Sr. as Tom Driscoll
*Thomas Meighan as Chambers
*Florence Dagmar as Rowena Cooper
*Jane Wolfe as Roxy 
*Ernest Joy as Judge Driscoll
*Gertrude Kellar as Mrs. Driscoll

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 