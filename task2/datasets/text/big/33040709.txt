Matti: Hell Is for Heroes
{{Infobox film
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Aleksi Mäkelä
| producer       = Markus Selin
| writer         = Marko Leino
| starring       = Jasper Pääkkönen Peter Franzén Elina Hietala Jope Ruonansuu Juha Veijonen Elina Knihtilä
| music          = Tuomas Kantelinen
| cinematography = Pini Hellstedt
| editing        = Kimmo Taavila
| studio         = Solar Films Buena Vista
| released       =  
| runtime        = 135 minutes
| country        = Finland
| language       = Finnish
| budget         = €1.6 million 
| gross          = €3,620,391   
}} Finnish biographical skijumper Matti Nykänen. The film was directed by Aleksi Mäkelä and written by Marko Leino.    With 461,665 views it was the most watched film in Finland in 2006. 

== Cast ==
* Jasper Pääkkönen as Matti Nykänen, a four-time Olympic gold medalist 
* Peter Franzén as Nick Nevada, a character based on Mike Sierra, Nykänens manager 
* Elina Hietala as Taina
* Juha Veijonen as Maisteri, based on Matti Pulli, a ski jumping coach   
* Elina Knihtilä as Mirva, based on Mervi Tapola 
* Jope Ruonansuu as Oksanen
* Kari Hietalahti as Hammer
* Jani Volanen as Nipa
* Jussi Lampi as Jorma Tapio
 

== Soundtrack ==
The soundtrack album for the film, Musiikkia elokuvasta Matti, was released on January 11, 2006.  It contains songs performed by Nykänen himself.    Two tracks used in the film, "Pidä varas"  and "Kingi"  performed by Nykänen, were not included in the soundtrack album.

{{Track listing
| extra_column    = Performer
| title1          = Lennä Nykäsen Matti
| extra1          = Timo Kotipelto
| length1         = 4:22
| title2          = V-tyyli
| extra2          = Matti Nykänen
| length2         = 3:35
| title3          = Sukset
| extra3          = Popeda
| length3         = 3:22
| title4          = Backseat
| extra4          = Peer Günt
| length4         = 4:17
| title5          = I Surrender Rainbow
| length5         = 4:01
| title6          = Poliisi pamputtaa
| extra6          = Eppu Normaali
| length6         = 1:52
| title7          = Yllätysten yö
| extra7          = Matti Nykänen
| length7         = 2:50
| title8          = Bodya, sporttia, tsemppistä
| extra8          = Sleepy Sleepers
| length8         = 2:36
| title9          = Long gone boy
| extra9          = Havana Black
| length9         = 4:58
| title10          = Escape Stone
| length10         = 5:21
| title11          = Kuuma kesä
| extra11          = Popeda
| length11         = 2:59
| title12          = Samaa nauhaa
| extra12          = Matti Nykänen
| length12         = 2:40
| title13          = On hetki
| extra13          = Kari Hietalahti
| length13         = 1:44 The Final Countdown Europe
| length14         = 5:09
}}
  

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 
 