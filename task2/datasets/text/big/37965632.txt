King of the Railway
 
{{Infobox film
| name           = Thomas & Friends: King of the Railway
| image          =
| caption        =
| director       = Rob Silvestri Andrew Brenner
| producer       = Brian Lynch (Arc Productions) Ian McCue Halim Jabbour (HiT Entertainment)
| narrator       = Mark Moraghan (UK/US)
| music          =
| cinematography =
| editing        =
| studio         = Hit Entertainment  Arc Productions
| distributor    = Vue Cinemas (theatrical) Lionsgate Entertainment (US) Hit Entertainment (UK/US) Martin Sherman William Hope Jonathan Forbes Mike Grady Michael Legge David Bedella
| released       = September 2, 2013 (UK) September 17, 2013 (US) October 2, 2013 (AUS)
| runtime        = 62 minutes
| language       = English
| budget         =
| gross          =
}} 

King of the Railway is a 2013 CGI feature-length special and film of the TV series, Thomas & Friends. The films CGI animation is by Arc Productions.  

==Plot==
Thomas, Percy, James and all their friends become very excited when Sir Robert Norramby, the Earl of Sodor, returns to the island with a big surprise. As the Steam Team discover mysteries of the past and legends of long-ago heroes, they also meet Millie, the Earls private engine, and Stephen, one of the earliest engines ever built. Meanwhile, Gordon and Spencer are stunned when they encounter Connor and Caitlin, two streamlined engines from the Mainland who are even faster than they are. But disaster strikes when Stephen goes missing, and Thomas and the other engines have to put their bravery to the test in order to find him in time for the re-opening of Ulfstead Castle the very next day.

==Cast== Jonathon Forbes Rebecca OMara Bob Golding David Menkin (Porter and Jack) joined the cast.   This special marked the return of Jack the Front Loader (to the fold). 

{| class="wikitable"
|-
! Actor
! Region
! Role(s)
|-
| Mark Moraghan
| UK/US
| Narrator
|- Martin Sherman
| US Diesel
|-
| rowspan="2"| Ben Small
| UK Toby
|-
| UK/US Rheneas and Troublesome Trucks
|-
| David Bedella
| UK/US
| Victor
|- Jonathan Forbes
| UK/US
| Connor
|-
| rowspan="2"| Teresa Gallagher
| UK Emily
|-
| UK/US
| Belle, Annie and Clarabel
|-
| Bob Golding
| UK/US Stephen
|- Mike Grady
| UK/US Sir Robert Norramby
|- William Hope
| US Edward and Toby
|-
| Togo Igawa
| UK/US
| Hiro
|-
| Jules de Jongh
| US
| Emily
|-
| rowspan="2"| Steve Kynman
| UK Jack
|-
| UK/US
| Paxton
|- Michael Legge
| UK/US Luke
|-
| Rebecca OMara
| UK/US
| Caitlin
|-
| Miranda Raison
| UK/US Millie
|-
| rowspan="2"| Kerry Shale
| US Sir Topham Hatt
|-
| UK
| Diesel
|-
| rowspan="2"| Keith Wickham
| UK
| Edward, Henry, Gordon, James, Percy, and The Fat Controller
|-
| UK/US Skarloey
|-
| Matt Wilkinson
| UK
| Railway engines (Thomas and Friends)#Spencer|Spencer, Cranky, and Kevin
|-
| Glenn Wrage
| US
| Spencer and Cranky
|-
| David Menkin
| US
| Jack 
|-
|}

==References==
 

==External links==
 
 

 
 
 
 
 
 