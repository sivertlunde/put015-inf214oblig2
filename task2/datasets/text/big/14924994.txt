Something to Live For (film)
 
{{Infobox film
| name           = Something to Live For
| image          = SomethingToLiveFor.JPG
| image size     = 
| alt            = 
| caption        = Original poster for the French release
| director       = George Stevens
| producer       = George Stevens
| writer         =  Dwight Taylor
| story          = 
| narrator       = 
| starring       = Joan Fontaine Ray Milland Teresa Wright
| music          = Victor Young George Barnes
| editing        = William Hornbeck
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 89 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Dwight Taylor was the first to focus on the Alcoholics Anonymous program as a means of overcoming an addiction to liquor.

==Overview==
The plot centers on Jenny Carey, a budding actress whose developing career is threatened by an increasing dependence on alcohol spurred by her self-destructive romance with theatre director Tony Collins. Reformed drunk Alan Miller attempts to help her by introducing her to AA, but his growing interest in her strains his marriage to Edna, who suspects his motive for assisting Jenny is more than humanitarian.

==Principal cast==
*Joan Fontaine ..... Jenny Carey
*Ray Milland ..... Alan Miller
*Teresa Wright ..... Edna Miller
*Richard Derr ..... Tony Collins
*Douglas Dick ..... Baker

==Production notes==
Screenwriter Dwight Taylor based the character of Jenny on his mother, noted stage actress Laurette Taylor, whose struggle with alcoholism kept her from acting for years at a time. She was a longtime friend of director/producer George Stevens uncle, theatre critic Ashton Stevens. 

Joan Fontaine, in San Francisco for the films premiere, told reporters Jenny Carey was one of her more difficult roles "partly because Ive never been drunk." In order to achieve a convincing performance, she said, "I talked to members of Alcoholics Anonymous and watched my friends at cocktail parties."  

==Critical reception==
In his review in the New York Times, Bosley Crowther said, "Mr. Stevens production and the direction he has given this film . . . are as sleek and professionally efficient as any you are going to see around. But, oh, that script by Dwight Taylor! It is a fearsomely rigged and foolish thing, planted with fatuous situations that even Mr. Stevens cant disguise. And how that long arm of coincidence keeps batting you in the face! At first it is simply embarrassing. Then it is vexingly absurd."  

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 