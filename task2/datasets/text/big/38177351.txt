Attack on Baku
{{Infobox film
| name           = Attack on Baku
| image          =
| image_size     =
| caption        =
| director       = Fritz Kirchhoff 
| producer       = Hans Weidemann
| writer         =  Hans Wolfgang Hillers   Hans Weidemann
| narrator       =
| starring       = Willy Fritsch  René Deltgen   Fritz Kampers   Hans Zesch-Ballot
| music          = Alois Melichar
| cinematography = Robert Baberske
| editing        = Erich Kobler UFA
| distributor    =  UFA
| released       = 25 August 1942
| runtime        = 91 minutes
| country        = Germany German
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} on location in German-allied Romania, and at Babelsberg Studio in Berlin.

==Synopsis==
Azerbaijan, 1919. the British hope to secure control of the vast oil fields around Baku by launching a series of terrorist attacks on them. Hans Romberg, a German who is working as a security officer, battles with the British chief agent Captain Forbes and his associates.

==Cast==
* Willy Fritsch as  Hans Romberg
* René Deltgen as Captain Percy Forbes, British Chief Agent 
* Fritz Kampers as  Sergeant Mathias Ertl
* Hans Zesch-Ballot as  Police Minister Barakoff 
* Paul Bildt as Camps, U.S. oil magnate in Baku 
* Lotte Koch as Sylvia, his daughter 
* Erich Ponto as Jensen, Danish oil magnate 
* Aribert Wascher as Mamulian, Armenian oil magnate 
* Walter Janssen as Hanson, Swedish oil magnate 
* Joachim Brennecke as  Ali Baba
* Josef Kamper as Zolak Wilhelm König as Thatul
* Heinrich Marlow as Lord Seymour, British officer 
* Hellmut Helsig as  Richard Twinning, British Agent 
* Alexander Enge as  Steffens, British Agent 
* Walter Holetzko as  Richards, British Agent 
* Peter Elsholtz as British Lieutenant 
* Nikolai Kolin as Russian waiter 
* Aruth Vartan as GPU agent 
* Willy Maertens as Jensens lawyer
* Boris Alekin as  Turkish officer 
* Angelo Ferrari as Turkish officer 
* Erik Radolf as Forbes servant 
* Herbert Gernot as Colonel Ahmed Bey 
* Fred Goebel as British agent 
* Reginald Pasch as British agent
* Arthur Reinhardt as British agent 
* Nico Turoff as British agent 
* Kurt Iller as British agent 
* Karl Jüstel as British agent 
* Günther Ballier as Jensens secretary
* Herbert Scholz as Assassin 
* Werner Völger as Assassin 
* Peter Busse as Oil Tycoon
* Julius E. Herrmann as Oil Tycoon 
* Erich Walter as Gregor 
* Lotte Hermann as Dancer 
* Lula Sachnowsky as Dancer

==References==
 

==Bibliography==
* Hake, Sabrine. Popular Cinema of the Third Reich. University of Texas Press, 2001.
* Eltin, Richard A. Art, Culture, and Media Under the Third Reich. University of Chicago Press, 2002

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 
 