Ko Ko
 
 
{{Infobox film
| name = Ko Ko
| image = 
| caption = 
| director = R. Chandru
| producer = Bhasker Adhi
| writer = 
| dialogue writer =  Srihari
| music = Ramana Gogula
| cinematography = K. S. Chandrashekar
| editing = 
| background score = 
| distributor =  
| released =   
| runtime = 
| country = India
| language = Kannada
| budget = 8 Crores
| gross = 
}} Kannada romantic film genre starring Srinagar Kitty and Priyamani in the lead roles. The film is directed by R. Chandru. Ramana Gogula is the music director of the film. Bhaskar and Adhi have jointly produced the venture under Bharani films. 

== Cast ==
* Srinagar Kitty as Kitty
* Priyamani as Cauvery Srihari as Srihariprasad
* Anu Prabhakar
* Rangayana Raghu
* Harshika Poonachha
* Ravi Kale
* Varada Reddy
* Praveen

== Soundtrack ==
The audio soundtrack was released on 7 December 2011 at the Bell Hotel in Bangalore. Ramana Gogula has composed 6 songs and Kaviraj has penned lyrics for 5 of them.

{{Infobox album  
| Name        = Ko Ko
| Type        = Soundtrack
| Artist      = Ramana Gogula
| Cover       = 
| Alt         = 
| Released    =  
| Recorded    = Feature film soundtrack
| Length      = 
| Label       = Ananda Audio Video
}}

{{Track listing
| total_length   = 
| lyrics_credits = yes
| extra_column	 = Singer(s)
| title1	= Aakasmika Geleyanu
| lyrics1 	= Kaviraj
| extra1        = Kunal Ganjawala, Shreya Ghoshal
| length1       = 
| title2        = Government College
| lyrics2 	= Sanju
| extra2        = Ramana Gogula
| length2       = 
| title3        = Mellane Sunitha
| lyrics3 	= Kaviraj
| length3       = 
| title4        = Labaa Labaa Labaa
| extra4        = Kailash Kher, Chaitra H. G.
| lyrics4 	= Kaviraj
| length4       = 
| title5        = Naa Kolluve Sunitha
| lyrics5 	= Kaviraj
| length5       = 
| title6        = Kitti Bhaava
| extra6        = Gurukiran, Chaitra H. G.
| lyrics6 	= Kaviraj
| length6       = 
}}

==Release==
KO KO had a good opening in allover Karnataka. It successfully ran in theaters and finished 30 days. When Sidlingu and Ko Ko was released together, Sidlingu did not get good response as it was in off illegal activities used in the film.

==References==
 

 

 
 
 


 