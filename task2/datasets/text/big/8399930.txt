Abuso de confianza
 
{{Infobox film
| name           =  Abuso de confianza
| image          = Abusodeconfianza.jpg
| image_size     =
| caption        = Film promotional shot
| director       = Mario C. Lugones
| producer       = Mario C. Lugones
| writer         = Pierre Wolff   Julio Porter
| starring       = Manuel Alcón   Carlos Bellucci   Iris Alonso   María Armand
| music          = George Andreani
| cinematography = Alfredo Traverso
| editing        = Oscar Orzábal Quintana
| studio   = Lumiton
| released       = 21 September 1950
| runtime        = 77 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
| followed_by    = 
| amg_id         = 
| imdb_id        =
}} Argentine  drama film directed by Mario C. Lugones and written by Julio Porter. It was based on the novel by Pierre Wolff. Starring Manuel Alcón and Iris Alonso.

==Cast==
*Manuel Alcón
*Iris Alonso
*Alejandro Anderson
*María Armand
*Carlos Bellucci
*Mario Roque Benigno
*Arnoldo Chamot
*Manuel Collado
*Margarita Corona
*Renée Dumas
*Celia Geraldy
*Juan Latrónico
*Adolfo Linvel
*Sergio Malbrán
*José Nájera
*Juan Pecci
*Nélida Romero
*Maria Elena Sagrera
*Carlos Thompson
*Jorge Villoldo
*Olga Zubarry
*Dora Zular

==Release==
The film was released on 21 September 1950.

==External links==
*  

 
 
 
 
 
 


 
 