Black Roses (1921 film)
{{Infobox film
| name =Black Roses
| image =File:Black Roses (1921) - Ad 1.jpg
| alt =An advertisement for the film
| caption =An advertisement for the film
| film name =
| director =Colin Campbell
| producer =
| writer =Richard Schayer
| screenplay =
| story =
| based on =  
| narrator =
| starring ={{Plainlist|
*Sessue Hayakawa
*Myrtle Stedman
*Tsuru Aoki
*Andrew Robson
*Toyo Fujita
}}
| music =
| cinematography =
| editing =
| studio = Hayakawa Feature Play Company
| distributor =
| released =  
| runtime =
| country =
| language =
| budget =
| gross =
}}
Black Roses is a 1921 American crime drama film directed by Colin Campbell. Sessue Hayakawa, Myrtle Stedman, Tsuru Aoki,    Andrew Robson and Toyo Fujita appeared in the film.

== References ==
 

== External links ==
*  

 