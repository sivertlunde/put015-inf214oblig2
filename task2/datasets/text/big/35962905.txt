Open (film)
{{Infobox film
| name          = Open 
| image         = Open Poster NA.jpg
| image_size    = 
| border        = 
| alt           = Open Poster NA
| caption       = Poster from North American release 
| director      = Jake Yuzna
| producer      = Kelly Gilpatrick Jake Yuzna
| writer        = Jake Yuzna
| starring      = Gaea Gaddy Tempest Crane Morty Diamond Daniel Luedtke
| music         = Adult (band)|ADULT.
| editing       = Jake Yuzna
| studio        = Narrative Films
| distributor   = Ariztical Entertainment
| released      =  
| runtime       = 88 minutes
| country       = United States
}}
Open is a 2011 American drama film, written and directed by Jake Yuzna. The film was produced by Narrative Films and released by Ariztical Entertainment in North America on September 27, 2011.
 pandrogynous individuals   as they explore new forms of love, sex, and gender emerging at the dawn of the new millennium. 

Premiering at the Berlin Film Festival in 2010,  Open became the first American film to win the Teddy Jury Prize at the Teddy Award.    The film also won Best Narrative Film at the TLV Festival in Tel Aviv Israel, Best Performance at Newfest, as well as having the Jake Yuzna named a Four in Focus filmmaker at Outfest.   Much of the film was inspired by the artist and musician Genesis P-Orridge,  who served as creative consultant on the film and is included in a post screening dialog with Jake Yuzna at New Museum   available as a DVD bonus feature on the North American release.

The Detroit based music duo Adult (band) created an original score for the film.

==Plot==
When the young hermaphrodite Cynthia (Gaea Gaddy) meets Gen (Tempest Crane) and Jay (Jendeen Forberg), a couple recovering from plastic surgery, she learns of Pandrogyny, in which two people merge their facial features in order to reflect their evolution from separate identities into one unified entity.

Inspired by this, Cynthia abandons her husband and suburban life to embark on a road trip with Gen through the remnants of 20th century America.

Simultaneously, a young transman, Syd (Morty Diamond), meets a young punk man, Nick (Daniel Luedtke). After having sex with one another, and someone born of the opposite sex for the first time, Syd and Nick find themselves falling into love, a love that forces them to confront how hormone treatments have forever changed sex and relationships.

Not science fiction, but American reality, OPEN brings together a cast of real hermaphroditic, pandrogynous, and transpeople to create a revealing look at the pioneers of the new human experience, and the emerging possibilities for humanity at the dawn of a new millennium.

==Cast==
* Gaea Gaddy as Cynthia, a young intersexed person.
* Morty Diamond as Sid, a young Transman.
* Daniel Luedtke as Nick, a young queer man. pandrogynous person Genesis and Lady Jaye Breyer P Orridge.

==Awards==
* 2010 Teddy Jury Prize at the Teddy Award
* Best Narrative Film at the TLV Festival in Tel Aviv Israel
* Best Performance (awarded to actor Morty Diamond) at Newfest
* Four in Focus filmmaker (awarded to director Jake Yuzna) at Outfest.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 