Somebody Killed Her Husband
 
{{Infobox television film name = Somebody Killed Her Husband image =  image_size =  caption =  genre = mystery
 distributor =  creator =  director = Lamont Johnson producer =  writer = Reginald Rose screenplay = Reginald Rose story =  based on =  narrator =  starring = Farrah Fawcett Jeff Bridges music = Alex North cinematography =  editing =  studio =  budget = $5.1 million SPOTLIGHT: MOVIE MOGUL MELVIN SIMON: HIS LOVE AT FIRST BITE IS A HIT
By HOLLIS ALPERT. New York Times (1923-Current file)   06 May 1979: 133  country = United States language = English
 network =  released = 1978 first_aired =  last_aired =  runtime =  num_episodes =  preceded_by =  followed_by =  website = 
}} John Wood, John Glover.

Tagline: Is this any way to begin a love affair?

==Plot==
The film is set in Manhattan, New York City. The plot concerns the efforts of a woman (Fawcett) and her lover (Bridges) to find the murderer of her husband before they are accused of it themselves. The storys climax occurs at the Macys Thanksgiving Day Parade. Reginald Roses screenplay was nominated for an Edgar Allan Poe Award.

==Cast==
*Farrah Fawcett - Jenny Moore (as Farrah Fawcett-Majors)
*Jeff Bridges - Jerry Green John Wood - Ernest Van Santen
*Tammy Grimes - Audrey Van Santen John Glover - Herbert Little
*Patricia Elliott - Helene
*Mary McCarty - Flora
*Laurence Guittard - Preston Moore
*Vincent Robert Santa Lucia - Benjamin
*Beeson Carroll - Frank Danziger
*Eddie Lawrence - Other Neighbor
*Arthur Rhyris - Customer
*Jean-Pierre Stewart - Man in Beret
*Terri DuHaime - Lulus Mother
*Sands Hall - Girl Typist

==Release==
The film was not a big hit but the filmmakers made a $1 million profit by pre-selling the film. 

==Additional information==
This film was also released under the following titles:
*Aki halt, aki nem - Hungary (imdb display title)
*Alguém Matou o Marido Dela - Portugal (imdb display title)
*Alguien mató a su marido - Spain
*Charade 79 - Japan (English title)
*Ihanet çemberi - Turkey (Turkish title)
*Joku murhasi hänen miehensä - Finland
*Kapoios skotose ton andra tis - Greece (transliterated ISO-LATIN-1 title)
*Knivhugg går igen - Sweden
*Ktos zabil jej meza - Poland
*Qualcuno ha ucciso mio marito - Italy
*Qui a tué mon cher mari? - Belgium (French title)
*Rendezvous mit einer Leiche - West Germany

==Soundtrack listing==
# Title Theme (03:19)
# Jerry and Jenny (01:33)
# Pretty Jenny (01:22)
# In the Restaurant (02:41)
# Two Lovers (01:26)
# Loving Each Other (01:13)
# First Date (01:19)
# Happiness (01:29)
# Unexpected Murder (01:32)
# Mysterious Days (02:34)
# Doubtful Ernest (01:42)
# Pirate Listener (01:37)
# Second Murder (01:54)
# Secret of Jewels (01:43)
# Jerrys Trick (01:37)
# The Criminal (01:42)
# Fearful Escape (02:03)
# In the True Love (02:25)

==See also==
*List of American films of 1978

==References==
 

==External links==
*   at IMDB.com
 

 
 
 
 
 
 
 
 

 