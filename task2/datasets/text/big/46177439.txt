Zolykha's Secret
{{Infobox film
| name           = Zolykhas Secret
| image          = Zolykhas_Secret_poster.jpg
| alt            = 
| caption        = 
| film name      =  
| director       = Horace Shansab
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = 
| cinematography = S.A. Wagef Hussaini 	
| editing        = Horace Shansab
| studio         =  
| distributor    =  
| released       =  2006  
| runtime        = 135 minutes   
| country        = Afghanistan
| language       = English Persian
| budget         = 
| gross          =  
}} Afghan film directed by Horace Shansab. It is one of the first feature films produced in the post-Taliban Afghanistan.      

== Plot ==
The film is set in the last year of the oppressive Taliban-era in Afghanistan.  The story of Zolykhas Secret is woven around an Afghan family who made their living in a village at the foot of a hill. The family, having suffered the oppressive Taliban war, particularly the last year of the struggle against them, are making efforts to address new tensions created by another war. Under the most trying conditions, the family struggles to build their hilly abode with mutual support of all members of the family. In this family, Zolykha is the youngest girl, vivacious and curious, exhibits special psychic powers of extrasensory perception of past events and people whose ghosts haunt behind their home. Amena and Zalmai are her older co-borns, who, in their efforts to decipher the meaning of past tragedies and tribulations (both natural and man made),  convince themselves that a positive future is feasible.  However, the eldest daughter of the family is harassed by a Taliban soldier to have relations with him. Her father protests and fights for his daughters honour but events take a tragic turn. This forces her younger siblings to flee the village to a city with the help of an "apostate" (a religious convert).  

==Production== Pashtu and English sub-Titles. Horace Ahmad Shansab is the director, screenwriter, and producer of the film. S.A. Waquef Hussaini is the cinematographer. Local Afghan Music is rendered by Uslad Gholam Hussein and Shereen Agha. It has a run time of 135 minutes. The film was released in 2007.   

== Cast ==
* Dadullah Ahmadzai
* Gholam Farouq Baraki
* Zbiullah Furutan
* Marina Golbahari
* Nasrine Habibi
* Abdul Wahed Hasanzada
* Mahmur Pashtun
* Hamida Refah
* Zubaida Sahar

== References ==
 

== External links ==
*  
*  

 
 