Final Cut (1998 film)
 
Final Cut is a film released in 1998, jointly written and directed by Dominic Anciano and Ray Burdis (who also appear in the film). This film features several of the actors / actresses from the Primrose Hill set. It was nominated for the Golden Hitchcock at the 1999 Dinard Festival of British Cinema. All the characters (except Tony, played by Perry Benson) in this film share their forename with the actors / actresses who play them, a gimmick used in the directors later film Love, Honour and Obey (2000).

==Plot outline==
The film opens with the cast gathering after the funeral of Jude to see a film he had been working on for two years. It turns out that the film is secret videos of all those gathered together in their most despicable moments including thievery, spousal abuse, adultery, etc. The revelations remove the masks from the so-called close friends.

==Cast==
* Perry Benson as Tony
* Ray Winstone as Ray
* Sadie Frost as Sadie John Beckett as John
* William Scully as Bill
* Mark Burdis as Mark
* Jude Law as Jude
* Lisa Marsh as Lisa
* Ray Burdis as Burdis
* Dominic Anciano as Dominic
* Holly Davidson as Holly

As with most films involving the Primrose Hill set, there are numerous links between the cast members. Sadie Frost appears alongside her husband Jude Law and her sister Holly Davidson; brothers Ray and Mark Burdis both appear in the film; Ray Burdis and Dominic Anciano have worked together throughout their careers; and Winstone, Burdis and Benson all appeared in Scum (film)|Scum (1979).

==See also==
* The Final Cut (2004 film)|The Final Cut, a sci-fi thriller film released in 2004

==External links==
*  
*  

 
 
 
 
 
 


 