Main Bhi Ladki Hoon
{{Infobox Film
| name           = Main Bhi Ladki Hoon
| image          = 
| image_size     = 
| caption        = 
| director       = A. C. Tirulokchandar
| producer       =
| writer         = 
| narrator       = 
| starring       = Dharmendra Meena Kumari music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1964
| runtime        = 02.31.40
| country        = India
| language       = Hindi
| budget         = 
}}

Main Bhi Ladki Hoon is a 1964 Hindi movie. Directed by A. C. Tirulokchandar the film stars Dharmendra and Meena Kumari. The film was a remake of Tamil film Naanum Oru Penn and Telugu "Naadi Aada Janme.

==Casting==
* Dharmendra
* Meena Kumari 
* S. V. Rangarao
* Balraj Sahani 
* A. V. M. Rajan
* Puspalatha
* Om Prakash 
* Bhagwan Manorama
* Mohan Choti
* Kammo
* Umesh Sharma

==Soundtrack==

The music director of this movie is Chitragupt. "Chanda Se Hoga" and "Krishna O Kale" songs have the same tune as the Tamil and the Telugu version.

*Chanda Se Hoga = P. B. Srinivas, Lata Mangeshkar
*Krishna O Kale = Lata Mangeshkar
*Yehi To Dhin Hai = Mohammad Rafi, Asha Bhosle
*Aye The Huzur Bade (female) = Asha Bhosle
*Aye The Huzur Bade (male) = Mohammad Rafi

== External links ==
*  

 
 
 
 
 
 
 