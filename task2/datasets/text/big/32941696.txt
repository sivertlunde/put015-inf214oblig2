Papa 2 (film)
 
{{Infobox film name        = Papa 2 image       =  caption     = Promotional poster director    = Gopal Menon producer    = Spring Thunder Films script      =  cinematography = Gopal Menon editing     =   music       =  research     =  released    = 2000 runtime     = 25 minutes country     = India language    = Kashmiri Urdu (English subtitles) budget      =  gross       = 
}}
Papa 2 is a film about the notorious interrogation centre, Papa 2.  This interrogation center was run by the Indian Armed Forces in Kashmir till 1996. Officially, over 2000 - unofficially, over 8000 - people have disappeared from the Kashmir Valley over the past 15 years. Most of these are enforced disappearances. The film, PAPA 2, documents the struggle of the mothers and wives of disappeared persons to trace their loved ones. It features interviews with the families of the affected people and also members of the Association of Parents of Disappeared People.

 
 
 
 
 
 
 

 