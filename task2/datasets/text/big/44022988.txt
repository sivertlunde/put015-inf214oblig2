Chumaduthangi
{{Infobox film
| name           = Chumaduthangi
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       =
| writer         = Sreekumaran Thampi
| screenplay     =
| starring       = Prem Nazir Sukumari Jayabharathi Kaviyoor Ponnamma
| music          = G. Devarajan V. Dakshinamoorthy
| cinematography = SJ Thomas
| editing        = Chakrapani
| studio         = Sreekanth Films
| distributor    = Sreekanth Films
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed P. Bhaskaran. The film stars Prem Nazir, Sukumari, Jayabharathi and Kaviyoor Ponnamma in lead roles. The film had musical score by G. Devarajan and V. Dakshinamoorthy.   

==Cast==
 
*Prem Nazir
*Sukumari
*Jayabharathi
*Kaviyoor Ponnamma
*Adoor Bhasi
*Jose Prakash
*Prathapachandran
*Sukumaran
*Bhargavan
*CA Balan
*CR Lakshmi Kunchan
*Pallikkara Aravindakshan
*Raghava Menon
*Simhalan Sujatha
*Suresh
*Usharani
*V Govindankutty
*Vanchiyoor Venu
 

==Soundtrack==
The music was composed by G. Devarajan and V. Dakshinamoorthy and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ethusheethalachaaya thalangalil || K. J. Yesudas, S Janaki || P. Bhaskaran ||
|-
| 2 || Maanathoru Kaavadiyaattam || S Janaki || P. Bhaskaran ||
|-
| 3 || Maayalle || Ambili || P. Bhaskaran ||
|-
| 4 || Swapnangal Alankarikkum || Jayashree || P. Bhaskaran ||
|-
| 5 || Swapnangal Alankarikkum (Pathos) || Jayashree || P. Bhaskaran ||
|-
| 6 || Swapnangal Thakarnnittum || V. Dakshinamoorthy || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 


 