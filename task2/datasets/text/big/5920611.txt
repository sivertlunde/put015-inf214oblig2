Magic Mike
 
 
{{Infobox film name           = Magic Mike image          = Magic_Mike.jpg alt            =   caption        = Theatrical release poster cinematography = Steven Soderbergh director       = Steven Soderbergh producer  Nick Wechsler Gregory Jacobs Channing Tatum Reid Carolin writer         = Reid Carolin starring       = Channing Tatum Alex Pettyfer Cody Horn Matt Bomer Olivia Munn Joe Manganiello Matthew McConaughey editing        = Steven Soderbergh studio  Nick Wechsler Productions Iron Horse Entertainment Extension 765 distributor  Warner Bros. Pictures   FilmNation Entertainment    released       =   runtime        = 110 minutes   country        = United States language       = English budget         = $7 million   gross          = $167,221,571   
}} male stripping, guided by Mike Lane, who has been in the business for six years.
 widely released by Warner Bros. on June 29, 2012, and was a critical and commercial success.

A sequel, Magic Mike XXL, is scheduled to be released on July 1, 2015.

==Plot==
Mike Lane has big plans for a business of his own but pays his bills through a series of odd jobs, most notably performing as the star stripper at Xquisite Strip Club in Tampa, a club owned by Dallas, who has dreams of creating an "empire" of strip clubs.

Mike soon meets 19-year-old Adam, who has recently left school, and is looking for a construction job. Mike takes Adam under his wing and encourages him to begin stripping at Xquisite. Mike is introduced to Adams sister Brooke. Despite his on-and-off relationship with a woman named Joanna, Mike finds himself attracted to Brooke and promises to look after Adam.

Adam falls further into the excessive lifestyle of the Xquisite dancers, using drugs and having sexual encounters with many clients. When Dallas announces he has a plan to move their act to Miami, Mike confides in Brooke that he is tiring of the lifestyle and wants to get a small business loan to pursue his dream of opening a custom furniture business. The bank declines his loan application and Mike realizes that he has to stay in the business to continue to pay his bills. Mike later attends a hurricane party at Dallas house, where Adam becomes part of a scheme created by Tobias, Xquisites DJ, to sell drugs to Xquisites clients, and is eventually given a package of ecstasy. Adam begins using drugs regularly, and Mike notices more of Adams reckless behavior to the chagrin of Brooke who is relying on Mike to protect him.

A few days later, Mike and Adam perform for a private party at a sorority house where Adam brings the package of drugs with him. At the party, Adam gives a young woman an ecstasy pill causing a brawl between Adam and the womans boyfriend. Mike and Adam are forced to flee the scene. Later, at Xquisite, Dallas is infuriated to learn Mike didnt collect payment for the private show before the fight occurred. Also, Tobias becomes dismayed when he discovers that Adam had left the package of drugs, valued at $10,000, at the sorority.
 
After the next nights show at the club is finished, Mike and Adam take drugs and go to a club. Adam winds up vomiting and passing out, where Brooke finds him on the floor the next morning. Brooke angrily confronts Mike about his lifestyle, and ends her friendship with him. Later on, Tobias suppliers break into Mikes house looking for Adam. After realizing Adam lied to him about value of the drugs, Mike—unbeknownst to Brooke—gives them $10,000 (most of his life savings) to pay Adams debt.

Later, before the dancers final performance at Xquisite, Mike decides he has had enough. Knowing that Dallas has no loyalty to any of them and is driven by greed, Mike leaves through the clubs exit. He drives away to end up at Brookes apartment where he tells her he is through with stripping. After realizing Mike is not coming back, Dallas invites Adam to replace Mike as the front man of the dancers. Brooke and Mike realize they have an attraction for each other, and make plans for breakfast.

==Cast==
*Channing Tatum as Michael "Magic Mike" Lane,      a male stripper who performs at Xquisite.
*Alex Pettyfer as Adam "The Kid",  Mikes protege who befriends him.
*Cody Horn as Brooke,  Adams sister and Mikes love interest
*Matt Bomer as Ken    
*Olivia Munn as Joanna, Mikes on-again, off-again lover.
*Joe Manganiello as Big Dick Richie   
*Matthew McConaughey as Dallas,  a former stripper who owns Xquisite and is Mikes boss.
*Adam Rodríguez as Tito
*Kevin Nash as Tarzan
*Gabriel Iglesias as Tobias 
*Mircea Monroe as Kens wife
*Riley Keough as Nora   
*Wendi McLendon-Covey as Tara 
*Betsy Brandt as Banker
*Michael Roark as Ryan

==Production==

===Development=== Nicolas Refn, who did the film Bronson (film)|Bronson, to do it because hes insane for it."  Refn initially agreed on doing the film because Tatum and he were going to do another movie together. However, since that project did not get off the ground and due to his busy schedule, Tatum, who was then working with Soderbergh on Haywire (film)|Haywire, brought him the idea.    After Soderbergh cast Tatum and Pettyfer in the lead roles, Carolin spent time revising the screenplay.    He wrote the first draft in a month.    Soderbergh suggested showing two points of view in the film: seeing through the eyes of young Adam and of his mentor Mike.   
 Excision and Datsik (musician)|Datsik.  McConaughey who had no stripping scene in the original script requested to have one which became the last dance number during which he first sings "Ladies of Tampa" and then strips. The song was written in three hours by music supervisor Frankie Pine, Matthew McConaughey and Martin Blasick, McConaugheys guitar coach.  The thongs were made by company Pistol Pete. 

===Casting===
On August 16, 2011, it was reported that   co-star Chris Rock who convinced him to do it.  Cody Horn gave her insight on how she got the part of Brooke:
 

The cast visited a strip club to see what this world was like and the dance routines and to get information on the backstage life.  To prepare for the role, McConaughey went in a Los Angeles strip mall to get used to regular waxing.  Matt Bomer had to put on about 15 pounds for his part.  Adam Rodriguez went through a cardio and weights training. 

===Filming=== Dunedin coast, Tarpon Springs, Tierra Verde.   Soderbergh chose to shoot the entirety of the film with a double straw camera filter, except the interior of the club. 

===Music===
{{Infobox album   Name       = Magic Mike: Original Motion Picture Soundtrack Type       = Soundtrack Artist     = Various artists Cover      = Magic Mike OST.jpg Released   = June 26, 2012 Recorded   =  Genre      =  Length     = 36:18 Label      = WaterTower Music Chronology =  Last album =  This album = Magic Mike (2012) Next album = 
}}
The soundtrack was released on June 28, 2012, through WaterTower Music.   Other songs featured in the film include Cobra Starships "#1Nite", Sean Pauls "Got 2 Luv U", and Ginuwines "Pony". 

;Track listing
{{Track listing extra_column = Performer(s) title1       = Breakdown length1      = 3:45 extra1  Alice Russell  title2       = Its Raining Men length2      = 1:57 extra2       = Countré Black title3       = Bang Bang Boom length3      = 1:58 extra3       = The Unknown title4       = Gimme What You Got length4      = 3:13 extra4       = Black Daniel title5       = Just For Now length5      = 3:57 extra5       = Cloud Control title6       = Money length6      = 2:54 extra6       = Ringside title7       = Sassy Sexy Wiggle length7      = 4:02 extra7       = Joe Tex title8       = Mo Cash! length8      = 2:08 extra8       = Vegas Audio Ninjas title9       = Wash U Clean length9      = 3:13 extra9       = Beth Thornley  title10      = Victim length10     = 3:37 extra10      = Win Win & Blaqstarr title11      = Ladies of Tampa length11     = 1:14 extra11      = Matthew McConaughey title12      = Feels Like the First Time length12     = 4:20 extra12  Foreigner
}}

==Release==
Warner Bros. Pictures acquired US distribution rights to the independently produced film on October 27, 2011. Magic Mike was first screened at the Los Angeles Film Festival as the closing film on June 24, 2012, and was widely released on June 29, 2012, in the United States.   

===Marketing=== trailer for Magic Mike was released on iTunes on April 18, 2012 with a poster indicating the films name.  The first poster featuring the cast was released on June 1, 2012, along with a new trailer.  A second poster followed on June 4, 2012, and a fourth trailer was released on June 13, 2012.   To promote the film, Channing Tatum, Matthew McConaughey, Joe Manganiello and Matt Bomer participated in a photo shoot for Entertainment Weekly that came out in the May 25, 2012, issue.  Tatum, McConaughey, and Manganiello attended the MTV Movie Awards where they presented the award for the best on-screen transformation. During the presentation, Manganiello arrived dressed as a fireman stripper. 
 romantic comedy" through Tatum and Horns relationship, but that it changed when the camapaign also focused on "the male form part of it  " through a red-band trailer (the fourth one).  In June 2012, a Magic Mike float was included in the West Hollywood gay pride parade and in other gay pride events such as in New York and in San Francisco. 

===Box office===
Magic Mike proved to be a box office hit, grossing $113,721,571 and $53,500,000 outside North America with a total gross of $167,221,571 worldwide from a modest $7 million budget; it is the highest top-grossing movie under the Dance genre.  On its opening day (June 29, 2012), the film performed better than expected, earning about $19 million including a $2 million midnight run.     It finished the weekend at the second spot behind Ted (film)|Ted with a total of $39,127,170.  It was the first time two Motion Picture Association of America film rating system|R-rated films grossed more than $21 million each during a weekend.    CinemaScore reported that audiences gave a "B" average grade. 
 Sex and the City, which had also appealed to large groups of women. 

===Reception=== normalized rating in the 0–100 range based reviews from top mainstream critics, calculated an average score of 72, indicating "generally positive reviews", based on 38 reviews.  The movie also launched Academy Award buzz for Matthew McConaughey as Dallas.    He, however, was not selected for inclusion among the nominees, which was described as a "snub" by many.      

Magic Mike appeared on several critics lists of the best films of 2012:
*Kyle Smith, The New York Post: No. 2    
*Tom Shone, Intelligent Life: No. 9 
*Richard Brody, The New Yorker: Unranked 
*TV Guide: No. 15 

===Accolades===
{|class="wikitable"
|+ List of awards and nominations
!Award !! Category !! Recipient(s) !! Result
|- Broadcast Film Critics Association   Broadcast Film Best Supporting Actor Matthew McConaughey
|rowspan="3"  
|- Detroit Film Critics Society     Best Supporting Actor Matthew McConaughey
|- Houston Film Critics Society     Best Supporting Actor Matthew McConaughey
|- Independent Spirit Independent Spirit Awards    Independent Spirit Best Supporting Male Matthew McConaughey
|rowspan="3"  
|- National Society of Film Critics Awards   National Society Best Supporting Actor Matthew McConaughey (Also for Bernie (2011 film)|Bernie)
|- New York Film Critics Circle Awards  New York Best Supporting Actor Matthew McConaughey (Also for Bernie)
|- 39th Peoples Choice Awards|Peoples Choice Awards    Best Dramatic Film
|Magic Mike
|rowspan="6"  
|- Favorite Movie Actor Channing Tatum 21 Jump The Vow)
|- Favorite Dramatic Actor Channing Tatum (also for The Vow)
|- 2013 MTV MTV Movie Awards   Best Male Performance Channing Tatum
|- Best Shirtless Performance Channing Tatum
|- Best Musical Moment Channing Tatum, Matt Bomer, Joe Manganiello, Kevin Nash and Adam Rodríguez
|}

===Home media===
Magic Mike was released on DVD and Blu-ray disc|Blu-ray Disc on October 23, 2012. 698,497 DVD units were sold within its first week, generating revenue of $10,449,515. As of December 23, 2012, DVD sales totaled over 1.9 million units with $26,362,047 in revenue. 

==Sequel==
 
In a Twitter Q&A in July 2012, Tatum confirmed work on a sequel. The actor responded: "Yes, yes and yes! Were working on the concept now. We want to flip the script and make it bigger."  By March 2014, Magic Mike first assistant director Greg Jacobs had been chosen to direct the sequel, titled Magic Mike XXL and scheduled to begin shooting in Northern Hemisphere autumn 2014.     The film is scheduled for a July 1, 2015 release. 

==Stage musical==
Reid Carolin, who wrote the film, is adapting the film as a stage musical for Broadway theatre|Broadway. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 