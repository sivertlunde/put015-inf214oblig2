A Children's Story
 
{{Infobox film
| name           = A Children’s Story
| image          = A Childrens Story.jpg
| alt            =  
| caption        = 
| director       = Andrea and Antonio Frazzi
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = Gianluca Di Gennaro  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
A Children’s Story ( ), also known as Certain Children in Australia, was directed by Andrea and Antonio Frazzi in 2004 and is an Italian film which focuses on the life of an eleven-year-old boy growing up in an environment of poverty and criminality in Naples. This was the second film to be made by the Frazzi twins, Il cielo cade (starring Isabella Rossellini and Jeroen Krabbé) having been released in the year 2000. It was to be Andrea Frazzi’s last film; he died in May 2006.

Certi bambini was awarded the Crystal Globe at the Karlovy Vary International Film Festival in 2004.

==External links==
* 
 

 
 
 
 
 
 
 
 
 


 
 