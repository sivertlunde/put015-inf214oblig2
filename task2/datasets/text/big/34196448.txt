War Italian Style
{{Infobox film 
 | name =   War Italian Style 
 | image =  War-italian-style-movie.jpg
 | caption = Original American film poster
 | director = Luigi Scattini  
 | writer =   Castellano & Pipolo Fulvio Lucisano
 | starring = Buster Keaton Franco Franchi Ciccio Ingrassia
 | music = Piero Umiliani
 | cinematography =  	  
 | editing =  
 | producer = Fulvio Lucisano 
 | distributor = American International Pictures (US) 
 | released = 20 April 1966	
 | runtime =  85 min
 | awards = 
 | country = Italy
 | language = Italian
 | budget = 
}} 1966 Cinema Italian comedy film directed by Luigi Scattini. The film puts together the famous Italian comedian duo of Franco Franchi and Ciccio Ingrassia and the silent cinema icon Buster Keaton. 

The Keaton role is a mute role, except for the words "Thank you" that he pronounces in the final scene of the film.  

==Plot==
Two US Marines who are so incompetent that they are sent to the European Theatre of Operations end up in the Battle of Anzio where they face the Germans giant artillery piece "Anzio Annie".

== Cast ==
* Franco Franchi: Frank
* Ciccio Ingrassia: Joe
* Buster Keaton: Gen. von Kassler
* Martha Hyer: Lt. Inge Schultze
* Fred Clark: Gen. Zacharias
* Barbara Lory: Inger
* Franco Ressel: Col. Jaeger 
* Lino Banfi: German Soldier

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 