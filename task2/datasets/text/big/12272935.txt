La Llorona (film)
{{Infobox film
| name           = La Llorona
| image          = 
| alt            = 
| caption        = 
| director       = René Cardona
| producer       = José Luis Bueno
| writer         = Adolfo Torres Portillo (screenplay) Carmen Toscano (Stage play)
| starring       = María Elena Marqués Mauricio Garcés Eduardo Fajardo Luz María Aguilar
| music          = Luis Mendoza López
| cinematography = Jack Draper
| editing        = Jorge Bustos
| studio         = 
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Mexico
| language       = Spanish
| budget         = 
| gross          = 
}}
La Llorona ( ) is a 1960 Mexican film directed by René Cardona. Based on the tale of La Llorona, it tells of a family that is cursed by the evil spirit of Luisa, this storys "weeping woman". The film was filmed on location in Guanajuato, Mexico.
==Plot==
In 20th century Mexico, newly wed couple Felipe (Mauricio Garcés) and Margarita (Luz María Aguilar) are visited by Margaritas father, Don Gerardo Montes (Carlos López Moctezuma) tells them the story of La Llorona.
 Indian and Spaniard woman named Luisa is visited by an upper class Spaniard conquistador named Don Nuño de Montesclaros (Eduardo Farjado). She falls in love with the man so much so that she leaves her life to start anew with Don Nuño. Don Nuño and Luisia have a boy and girl. However, Don Nuño leaves, as he must go on missions. Days later she is visited by one of Dons fellow conquistadors. He tells her that Don Nuño will not return home for a long time due to his duties. Suspicious, she follows the conquistador and comes to a palace where she finds Don. Don explains that he is getting married to a new woman since Luisa is not fully Spaniard. Enraged and heartbroken, she curses Don Nuño and his descendants that all his bloodlines firstborn will be murdered violently. When she returns home, she stabs her own children to death. The entire town finds out and Luisa is sentenced to death.

Gerardo finishes the story and says that Margaritas brother was murdered violently when he was 4 because he was a firstborn. Felipe passes the story off as just a folktale but the house is then visited by a woman in a black cloak who is taking up the job of a nanny for Felipe and Margaritas newborn baby. Luisa (under the alias  "Carmen Asiul") is accepted but secretly plans on murdering the baby. She tries killing the baby many times but is unsuccessful with each attempt. Gerardo is suspicious of the new nanny since he feels he has seen her before but cannot make out where.

One night Felipe and Margarita decide to go out, leaving only Luisa and Gerardo home. Luisa is prepared to kill the baby using the very dagger with which she killed her own children. Gerardo realizes that it is Luisa and rushes to the babys bedroom. Luisa is somehow pulled back and as Gerardo opens the door, he finds that Luisa has vanished and the dagger is impaled on the ground. With the baby safe, the parents home, and Luisas ghost gone, Gerardo burns the dagger and the drawing of Luisa in a fire. He realizes that the curse under which his family had suffered for years is finally gone.

==Production==
This is a literal retelling of the legend of   leads a cast of distinguished Spanish performers.

==See also==
*List of ghost films

==External links==
* 

 

 
 
 