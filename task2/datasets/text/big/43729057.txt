Ricky (2015 film)
{{Infobox film
| name           = Ricky
| image          = Ricky_(2015_film)_Kannada_movie_poster.jpg
| caption        = Promotional poster 
| director       = Rishab Shetty
| producer       = S. V. Babu
| writer         = 
| starring       = Rakshit Shetty Haripriya
| music          = B. Ajaneesh Loknath
| cinematography = Venkatesh Anguraj
| editing        = N. M. Vishwa
| studio         = S. V. Productions
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Kannada
| budget         = 
| gross          = 
}}
 Kishore and Achyuth Kumar. The film deals with the prevalence of naxalism in India, with a love story in the backdrop.   

==Cast==
* Rakshit Shetty
* Haripriya Kishore
* Achyuth Kumar

==Production== Kishore and Achyuth Kumar signed for the film.  Haripriya was signed as the female lead opposite Rakshit Shetty.

===Filming===
It was revealed by the official twitter handle of the film that shooting commenced on September 1, 2014, also revealing the first look poster of the film.  Filming began Karkala, Udupi district. Majority of the filming took place in Udupi and Dakshina Kannada. Rakshit Shetty confirmed that 90 percent of the filming was completed by October 12, in a span of 42 days, with a song sequence to be shot in Gujarat.  The director revealed that songs and fight sequences were to be shot in Bengaluru. 

==References==
 

 
 
 
 


 