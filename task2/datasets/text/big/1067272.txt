Bandits (2001 film)
 
{{Infobox film
| name = Bandits
| image = Bandits_2001 film.jpg
| caption = Theatrical release poster
| director = Barry Levinson
| producer = Barry Levinson Michael Birnbaum
| writer = Harley Peyton
| starring = Bruce Willis Billy Bob Thornton Cate Blanchett
| music = Christopher Young
| cinematography = Dante Spinotti
| editing = Stu Linder
| studio = Hyde Park Entertainment Empire Pictures Cheyenne Enterprises
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 123 minutes
| country = United States
| language = English
| budget = $75 million 
| gross = $67,631,903 
}} National Board of Review Best Actor Award for 2001. Thornton and Blanchetts performances earned praise, as each was nominated for Best Actor and Best Actress Golden Globe Awards for their performances in this film, while Blanchett was nominated for Best Supporting Actress at the Screen Actors Guild Awards.

==Plot== getaway driver Ten Most Wanted list.

When Kate, a housewife with a failing marriage (Cate Blanchett), decides to run away, she ends up in the hands of the criminals. Initially attracted to Joe, she also ends up in bed with Terry and a confused love triangle begins.

The three of them go on the lam and manage to pull off a few more robberies, but after a while the two begin to fight over Kate, and she decides to leave them. The two criminals then decide to pull off one last job.

The story is told in Flashback (narrative)|flashbacks, framed by the story of the pairs last robbery of the Alamo Bank, as told by Criminals at Large, a fictional reality television show. The show tells the story of the last job to be a failure when Kate tips off the police and the two are caught in the act. The two then begin to argue when Joe tells the police "You wont take us alive!" and the argument gets to the point where the two of them shoot each other dead.

At the end of the film the real story behind the last job is revealed: Harvey used some of his special effects to make it seem as though Terry and Joe were shooting each other. Harvey and his girlfriend then ran in dressed as paramedics and placed the stolen money, Terry, and Joe in body bags. In the ambulance, Harvey uses electronics to blow out his tires which sends the ambulance into a junkyard. Under his jumpsuit, Harvey was wearing a fire suit. He lights himself on fire and rigs a bomb to go off. Kate, Harvey, Harveys girlfriend, Terry, and Joe flee the scene, leading officials to believe the bodies were burned.

Reunited, Joe, Terry, Harvey and Kate make it to Mexico to live out their dream. The last scene shows Harvey getting married in Mexico and Kate kissing Joe and Terry passionately.

==Cast==
* Bruce Willis as Joe Blake
* Billy Bob Thornton as Terry Collins
* Cate Blanchett as Kate Wheeler
* Troy Garity as Harvey Pollard
* Brian F. OByrne as Darill Miller
* Stacey Travis as Cloe Miller
* Bobby Slayton as Darren Head
* January Jones as Claire
* Peggy Miley as Mildred Kronenberg
* William Converse-Roberts as Charles Wheeler
* Richard Riehle as Lawrence Fife

==Soundtrack==
# "Gallows Pole" – Jimmy Page & Robert Plant
# "Tweedle Dee & Tweedle Dum" – Bob Dylan
# "Holding Out for a Hero" – Bonnie Tyler
# "Twist in My Sobriety" – Tanita Tikaram
# "Rudiger" – Mark Knopfler
# "Just Another" – Pete Yorn Walk On By" – Aretha Franklin
# "Superman (Its Not Easy)" – Five for Fighting
# "Crazy Lil Mouse" – In Bloom Just the Two of Us" – Bill Withers and Grover Washington, Jr.
# "Wildfire" – Michael Martin Murphey
# "Total Eclipse of the Heart" – Bonnie Tyler
# "Bandits Suite" – Christopher Young
# "Beautiful Day" - U2

==Reception==
Reviews to Bandits were positive, as the film holds a 65% "fresh" rating on Rotten Tomatoes based on 139 reviews. It did not help matters that many critics noticed the clear similarities between this film and the film Butch Cassidy and the Sundance Kid with one going as far to write "whilst Bandits is entertaining, its blatant pastiching of the narrative from Butch Cassidy and The Sundance Kid will leave a bitter taste in the mouth of a more mature viewer and add further credence to the argument that  Hollywood either has no new ideas or is increasingly unwilling to take chances".

===Box office===
In its opening weekend, the film opened at #2 raking in $13,050,700, behind Training Day, which was on its second week at the top.  The film grossed $67.6 million worldwide, and when comparing it to its budget of $75 million, Bandits was a box office disappointment.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 