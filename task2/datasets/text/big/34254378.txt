Intlo Illalu Vantintlo Priyuralu
{{Infobox film
| name           = Intlo Illalu Vantintlo Priyuralu
| image          = Intloillaluvantinlopriyuralu+2.jpg
| image_size     =
| caption        =
| director       = E. V. V. Satyanarayana
| producer       = K.L.Narayana
| writer         = Esukapalli Mohana Rao  (dialogues) 
| story          = K. Bhagyaraj
| narrator       = Venkatesh Soundarya Vineetha Koti
| cinematography = S. Gopal Reddy
| editing        = K. Ravindra Babu
| studio         = Sri Durga Arts
| distributor    =
| released       = 22 May 1996
| runtime        = 2:27:27
| country        = India
| language       = Telugu
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Tollywood  film directed by E. V. V. Satyanarayana and produced by K.L.Narayana on Sri Durga Arts. Starring Daggubati Venkatesh|Venkatesh, Soundarya and Vineetha played the lead roles and music composed by Saluri Koteswara Rao|Koti.   The film was super hit at the Box office.  The film was a remake of Tamil film Thaikulame Thaikulame.

==Plot== Nepali girl Manisha (Vineetha) under some unavoidable circumstances. When he comes to know that she is carrying his child, Sriram arranges for her stay at his friend Giris (Brahmanandam) house. Manisha delivers a boy and Sriram adopts him with his wifes consent who is not aware of the truth. After some years Srirams father comes to know truth through Giri, he wants Sriram to get back Manisha and make a clean of things to Seeta. But Sriram who is apprehensive of his Seetas reaction begs his father to keep the whole affair under wraps and he brings Manisha as a cook to his house. The story takes a turn to climax when Seeta is not happy with the goings on in the kitchen, she tries to get Manisha married off to someone else, then Sriram reveals the secret, Seeta also understands situations and circumstances and she happily welcomes Manisha into their lives. Finally, the movie ends on a happy note of two wives live happily with one husband.

==Cast== Venkatesh as Shri Ram
* Soundarya as Seeta
* Vineetha as Manisha
* Brahmanandam as Giri
* Kota Srinivasa Rao as Srirams father Mallikarjuna Rao as Yerri Nayudu
* Babu Mohan as Shri Rams manager AVS as Dr. Amaram
* Ananth as compounder

==Soundtrack==
{{Infobox album
| Name        = Intlo Illalu Vantintlo Priyuralu
| Tagline     =
| Type        = Soundtrack Koti
| Cover       =
| Released    = 1996
| Recorded    =
| Genre       = Soundtrack
| Length      = 27:12
| Label       = Supreme Audio Koti 
| Reviews     =
| Last album  = Jagadeka Veerudu   (1996) 
| This album  = Intlo Illalu Vantintlo Priyuralu   (1996)
| Next album  = Naidugari Kutumbam   (1996)
}}
Music composed by Saluri Koteswara Rao|Koti. Lyrics written by Samavedham Shanmuka Sharma. Music released on Supreme Music. 
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 27:12
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = 
| music_credits =

| title1  = Paaparo Pop 
| extra1  = Mano (singer)|Mano, Sangeetha
| length1 = 4:39

| title2  = Bol Bol Bolu Raja  SP Balu, Sujatha 
| length2 = 4:46

| title3  = Priyurale Premaga  Chitra
| length3 = 5:28

| title4  = O Lammi Timmirekkinde 
| extra4  = Mano
| length4 = 3:42

| title5  = Chilakatho Majaa 
| extra5  = SP Balu,Chitra
| length5 = 4:34

| title6  = Ammane Ayyanura 
| extra6  = Chitra
| length6 = 5:23
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 

 