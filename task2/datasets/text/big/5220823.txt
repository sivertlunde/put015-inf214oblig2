3 Chains o' Gold
{{Infobox Film
| name = 3 Chains o Gold
| image = 3chains.jpg Prince and The New Power Generation
| music = Prince
| director = Parris Patton, Randee St. Nicholas, Prince (as Paisley Park) Paisley Park Warner Music Vision
| released =  
| country = United States
| runtime = 73 minutes
| language = English
}} Prince and starring Prince and The New Power Generation.  It is a video collection, tied together with a loose plotline. The film was the 69th best-selling video of 1994. 

==Plot==
The story begins with the assassination of Egyptian Princess Mayte Garcia|Maytes father by seven unknown assailants. Mayte believes that the assassins were after the sacred "3 Chains of Gold". She sets out to meet with Prince, as she believes he is the only one that can protect the chains from the seven assassins. What follows is a romance between Prince and Mayte, and Prince organizing the assassination of the assailants (accompanied by the song "7 (song)|7").

==Epilogue==
At the end of the film, prior to the credits, is an explanation of the name change, attributing it to Princes desire to be "reborn" and start a perfect life:

 
Upon the seventh day of the sixth month 
Nineteen hundred and ninety-three 
Marking the beginning and ending of cycles of creation 
Prince, reaching the balance of thirty-five years, 
Put into practice the precepts of perfection: 
Voicing bliss through the freedom of being ones self 
Incarnating the New Power Generation into 
The close of the six periods of involution giving  Love Symbol) 
For in the dawn, all will require no speakable name 
To differentiate the ineffable one that shall remain.
 

==Film and album==
The film was accompanied by the Love Symbol album, however not all of the albums songs make an appearance in the film, and some of the songs are edited versions. The films title song is only an instrumental at the end. However, some of the dialogue from the album does appear in the film (a phone call from a reporter, played by Kirstie Alley, to Prince). The final speech of the film (made by Mayte) does not appear on the album, instead another phone call is in its place. The actual songs that appear in the film are:
* "My Name Is Prince"
* "Sexy MF"
* "Love 2 the 9s"
* "The Morning Papers"
* "The Max"
* "Blue Light"
* "I Wanna Melt with U"
* "Sweet Baby"
* "The Continental"
* "Damn U"
* "7 (song)|7"
* "The Call"

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 