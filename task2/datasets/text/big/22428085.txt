Spirit of the Marathon
 
{{Infobox film
| name           = Spirit of the Marathon
| image          = Spiritoffthemarathon cover.jpg
| caption        = DVD Cover
| director       = Jon Dunham
| producer       = Mark Jonathan Harris Gwendolen Twist Jon Dunham
| writer         = 
| starring       = Deena Kastor Daniel Njenga Jerry Meyers Leah Caille Lori OConnor Ryan Bradley
| music          = Jeff Beal  
| cinematography = Sarah Levy
| editing        = Christo Brock
| distributor    = Image Entertainment (USA)
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
}}
Spirit of the Marathon is a 2007 documentary film directed by Jon Dunham.  The film chronicles the journey six marathon runners experience while training and competing in the 2005 Chicago Marathon.  It was screened at the Chicago International Film Festival on October 5, 2007 and received a limited release in the United States on January 24, 2008.

==Synopsis== Joan Benoit-Samuelson, and Kathrine Switzer provide commentary about the sport.
 2004 Olympic Marathon, but has yet to win a marathon.  Njenga had finished second multiple times in previous Chicago Marathons.  Kastor is shown training and recovering from an injury in Mammoth Lakes, California, while Njengas life as a sponsored runner in Tokyo is profiled.

The rest of the runners featured live in Chicago. Ryan Bradley and Lori OConnor are both married young professionals; Bradley is a veteran marathoner who hopes to earn a qualifying time for the Boston Marathon, and OConnor is running her first marathon.  She finds it humorous when colleagues ask if she expects to win.  Jerry Meyers is a jovial 70-year-old who claims to run marathons for the t-shirt.  Leah Caille is a new runner that took up the sport to help recover from an emotional divorce.

While preparing for the race, Bradley suffers a knee injury and is unable to compete. He is clearly upset by this, and takes out his frustration by going for a long bike ride. OConnor and Caille go through the new experience of the long training sessions necessary for a marathoner.  Meyers lends his veteran knowledge while leading slower training runs with his daughter, who is running her first marathon.

All of the long and dedicated training culminates on the day of the marathon.  There are panoramic shots of the city that display the mass numbers of race participants and spectators.  Njenga runs with the lead pack, but once again falls short in third place.  Kastor has a lead for most of the race, but is challenged by Romanian Constantina Diţă-Tomescu at the very end of the race.  In a dramatic finish, Kastor is able to fend off Diţă-Tomescu for the victory.
 IT Band Syndrome, but eventually has a successful finish, crossing the line in 5:01:15.  Meyers wears his favorite "Kansas" shirt and enjoys his race, finishing in 5:59:39.

==Production==
Jon Dunham, the director of the film, was inspired to make a documentary about the marathon because of the impact running marathons had on his life.     He said that running a marathon was the most difficult thing he had done in his life, until he started to make a documentary about it.  Following storylines of runners training all over the world, and filming the subjects in the crowded Chicago Marathon are some of the challenges Dunham encountered when making the film. 

Initially, Dunham filmed Kastor while financing the production himself.  Eventually, the Chicago Marathon provided Dunham some financial resources to help him film the event.  In post-production, Dunham and co-producer Gwendolen Twist raised money from investors to fund the completion of the film.   

To film the subjects running the marathon, the camera crews rode in pedicabs.  They directed the amateur runners to go on a side of the road at specific areas in the course to help with filming.  In addition, the amateur runners had other runners alongside them holding up signs to help the crew identify them in the crowd. 

Dunham originally thought that the film should focus on the amateurs more than Kastor, but said he found her enthusiasm, talent, and determination "irresistible".  In an explanation to why Dunham choose to follow the six runners he selected, he says he finds the marathon to be an "incredible mixing of elite runners with the amateur runners from all across the globe and all ranges of human experience".      He said that he wants the film "to offer inspiration for anyone who sets out to achieve a goal that takes the determination, effort and attitude of running a marathon".   

On being filmed, Kastor said that she "went about her normal routine and training", and commented it that was not distracting, but "motivating". Kastor also revealed that the six runners did not meet until a brunch after the marathon, and felt an "immediate bond of friendship".   

==Film score==
Jeff Beal, an Emmy award-winner film composer, wrote the score for the film.  The music was performed by the Prague Philharmonic Orchestra. Active.coms review compliments the score, noting that it "beautifully blends the emotion, strain and exhilaration of the marathon". 

==Release==
The film premiered to sellout crowds at the 2007 Chicago International Film Festival, right before the running of the 2007 Chicago Marathon.  Daniel Njenga, one of the subjects of the film, was in attendance.    National CineMedia distributed the film by satellite to over 500 theatres as a special one-night only viewing, with an encore a month later. The release grossed over $1 million for the film. 

== Critical reception ==
The Los Angeles Times gave a favorable review of the film, calling it an "efficient yet comprehensive study".  The footage of Chicago is praised, as is the capture of the "inspiring journey of physical endurance and personal achievement".    The New York Times compliments the films enthusiasm toward the marathon, and the commentary provided by former marathoners.    Australias The Age offers a mixed review, writing that "In its more tranquil stretches, Spirit of the Marathon might be mistaken for a feature-length sneaker advertisement, but if youre looking for inspiration, you could do worse."   

== Awards and nominations ==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 2px #aaa solid; border-collapse: collapse; font-size: 90%;" 
|- bgcolor="#CCCCCC" align="center"
! colspan="4" style="background: LightSteelBlue;" | Awards
|- bgcolor="#CCCCCC" align="center" 
! Award
! Category
! Results
|-
|Chicago Independent Film Festival    Audience Choice Won
|-style="border-top:2px solid gray;"

 |-
|Mammoth Film Festival    Best Picture Won
|-style="border-top:2px solid gray;"
|}

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 