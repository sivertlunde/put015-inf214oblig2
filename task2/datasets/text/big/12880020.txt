TC 2000 (film)
TC 1993 action/science-fiction film starring Bolo Yeung, Jalal Merhi, Billy Blanks, Bobbie Phillips, Matthias Hues, Harry Mok, and Kelly Gallant. The film was written by T.J. Scott from a story by J. Stephen Maunder and Richard M. Samuels, produced by Jalal Merhi and directed by T.J. Scott. The soundtrack was composed by VaRouje.

==Video box description==
“International action star Billy Blanks teams up with beautiful and lethal Bobbie Phillips in this high-tech, futuristic thriller.

“Jason Storm (Blanks) and Zoey Kinsella (Phillips) are two cops under the leadership of The Controller, whose task it is to contain the vicious thugs and roving gangs that terrorize the otherwise unprotected community.

“When Zoey is murdered by the notorious Niki Picasso (Jalal Merhi), The Controller converts her body into a cybernetic killing machine that is unleashed upon the gangs without mercy. But Jason knows too much, and eventually must battle impossible odds as he takes on the gangs, The Controller, and the seemingly unbeatable Zoey in a race to save the Earth’s atmosphere from nuclear destruction. This fast-paced chiller boasts furious fight sequences featuring Matthias Hues and legendary martial arts star Bolo Yeung.”

==Availability==
The movie was released directly to video on August 18, 1993 by MCA/Universal Home Video in the United States, and in Canada that same year by Cineplex Odeon. A DVD is available in the UK. As for a region 1 release, there is a new special edition DVD in the works, but as of December 25, 2009, nothing else has announced.

==External links==
*  
*  

 
 
 
 
 
 
 
 


 