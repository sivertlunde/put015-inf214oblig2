Revathikkoru Pavakkutty

{{Infobox film
| name           = Revathikkoru Pavakkutty
| image          = Revathikkoru Pavakkutty.jpg
| image_size     =
| alt            = 
| caption        = 
| director       = Sathyan Anthikad
| producer       = Shahid Koker   Siyad Koker John Paul   Ravi Vallathol John Paul 
| narrator       =  Radha Menaka Menaka Lizy Lizy
| Shyam
| cinematography = Anandakuttan
| editing        = K. Rajagopal     
| studio         = Kokers Films
| distributor    = Kokers Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Revathikkoru Pavakkutty (Translation: A doll for Revathi) is a 1986 Malayalam film by Sathyan Anthikkad starring Bharath Gopi, Mohanlal and Radha (actress)|Radha.     The film is an adaptation of the stage play of the same name by Ravi Vallathol.

==Synopsis==
 

Revathis (Lizy (actress)|Lizy) parents separated when she was young; she was raised by her mother who taught her that her father (Bharath Gopi) was a bad man. Years later, while on his deathbed, her father makes a last wish to see his daughter. Revathi refuses to go, so her orphan friend Susanna (Radha (actress)|Radha) goes in her place to fulfill his wish. Revathis father is very happy to be with Susanna, thinking she is Revathy but Susanna dies. Finally Revathy returns home after the death of Susanna, but her visit fails to move a grief-stricken father.

==Cast==
*Bharath Gopi as Balan Menon
*Mohanlal as Dr. P. Madhavankutty Radha as Susanna Menaka as Indu
*Sukumari as Devootty Lizy as Revathi Menon Innocent as Bhasi Pillai
*Bahadoor as Ayyappan Pillai
*Jagathy Sreekumar as Kalyan Kumar
*Mala Aravindan as Vaidyar Rarichan Nair

==Soundtrack== Shyam and lyrics was written by Bichu Thirumala.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chinnakkutti Chellakkutti Thankakkatti (M) || K. J. Yesudas, Chorus || Bichu Thirumala ||
|-
| 2 || Chinnukkutti Chellakkutti Thankakkatti (F) || KS Chithra, Chorus || Bichu Thirumala ||
|-
| 3 || Vellaaram Kunnummele || K. J. Yesudas || Bichu Thirumala ||
|}

==References==
 

==External links==
*  
*   at Oneindia.in

 
 
 
 

 