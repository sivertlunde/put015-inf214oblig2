Birds of a Feather (1931 film)
{{Infobox Hollywood cartoon
| cartoon name      =Birds of a Feather
| series            = Silly Symphonies
| image             =
| image size        = 
| alt               = 
| caption           = 
| director          = Burt Gillett 
| producer          = Walt Disney
| story artist      = 
| narrator          = 
| voice actor       = 
| musician          = 
| animator          =    
| layout artist     = 
| background artist =  Walt Disney Productions
| distributor       = Columbia Pictures
| release date      = February 10, 1931
| color process     = Black and White
| runtime           = 8 min
| country           = United States
| language          = English
| preceded by       = Playful Pan
| followed by       = Mother Goose Melodies
}}
 animated Disney short film. It was released in 1931.

==Summary==  
Swans swim by, a peacock displays its plumage in glorious black-and-white while a passing duck jeers, assorted songbirds chirp, a woodpecker chases a caterpillar, and a chorus of owls croons. A chicken goes after worms while ignoring her brood until a hawk circles. When the hawk captures one chick, the crows form an attack squadron.

==External links==
*  
*  

 

 
 
 
 
 
 

 