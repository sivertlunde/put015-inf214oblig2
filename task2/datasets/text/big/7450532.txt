Chronic Bachelor
 
 
 
{{Infobox film name = Chronic Bachelor image = Chronic Bachelor.jpg director = Siddique
|producer Fazil
|writer Siddique
|starring Mukesh Rambha Rambha Bhavana Bhavana Indraja Indraja Innocent Innocent
|music = Deepak Dev cinematography = editing = production_company = distributor = PJ Entertainments released = 16 April 2003 runtime = 165 minutes rating = country = India language = Malayalam budget = gross =
}}
 romantic comedy Innocent and others. The movie was produced by Malayalam director Fazil (director)|Fazil.

It was later remade in Tamil as Engal Anna starring Vijaykanth, Prabhu Deva, Namitha and Vadivelu and in Telugu as Kushi Kushiga with Jagapati Babu.

==Plot==
The film opens with a flashback showing the family feud between families of Sathyaprathapan (Mammootty) and Bhavani (Indraja). Sathyaprathapans sister and Bhavanis cousin are in love. The murder of Bhavanis cousin Vishnu, is wrongly accused on Balagangadharan (Lalu Alex), father of Sathyaprathapan. Sathyaprathapans sisters commits suicide and Sathyaprathapan turns against his father. He starts to help Bhavanis family and mortgages his own house to finance her factory. A love develops between Sathyaprathapan and Bhavani. But Bhavanis greedy father wants to destroy Sathyaprathapans family and tries to take their home. That is when Parameshwara Maman shows up, Sathyaprathapans mom requests Maman to prove Balagangadharans innocence. Parameshwara Mama and Sathyaprathapan finds Kuruvilla who witnessed the murder being committed by Bhavanis father goons. Meanwhile we see that Sathyaprathapans mom faints and dies in the hospital. While in the hospital, his mom tells how Balagangadharan had a different wife and he should ask them forgiveness. He storms into Bhavanis house and accuses her dad, also trying to make her understand the truth but she doesnt believe him and stands by her father. Then Sathyaprathapan says he doesnt love her any more and vows that he will never trust a woman by remaining a chronic bachelor forever.

Then film comes to the present time showing a legal battle between Sathyaprathapan (now called SP) and Bhavani. Bhavani becomes furious when SP wins the legal battle to get back his house. She threatens to destroy SP.

From here, the story moves to the day-to-day life of SP and falls into a comic track throughout the first half. SP is now a successful businessman. SP has a stepsister, Sandhya (Bhavana Menon|Bhavana), who is his fathers daughter from the other wife. He now lives for her. But she doesnt know that SP is her brother, although she stays next door to him. Kuruvilla (Innocent (actor)|Innocent) is SPs aide and he too is a bachelor. Bhama (Rambha (actress)|Rambha) comes to stay in the hostel where Sandhya stays and tries to win the heart of SP. But SP considers her a nuisance.

SP agrees to take care of Srikumar (Mukesh (actor)|Mukesh). He is the son of Parameshwaran Mama (Janardhanan (actor)|Janardhanan), who had helped SP to become a successful businessman. Srikumar is a flirt and womanizer. He is now after Sandhya and agrees to stay with SP, when he comes to know Sandhya stays next door to SP. Comical scenes recur throughout the movie, where Kuruvila, who doesnt like Srikumar and his friend Ugran (Harisree Ashokan) staying with them, tries to get them out of the house.

Bhama comes to know SP is sponsoring Sandhyas studies and confronts him. SP tells her that Sandhya is his sister. Bhama then asks Sandhya to call SP for her birthday party. But SP gets upset and confronts Bhama. Bhama then reveals that her sponsor was SP all the while. During the birthday party, Bhavani comes and takes Bhama away. Then it becomes clear that Bhama is Bhavanis sister. Rivalry arises between Bhama and Bhavani and Bhama runs away from home and promises to help SP, but he tells her he has no hatred at all. Then Bhamas family members come to take her forcefully, but is stopped by Srikumar and SP.

Thats when SP asks Sandhya to move to his house. Srikumar and Ugran is moved to the guest house. Sandhya finally shows affection to Srikumar. One day SP and Kuruvilla catch Srikumar trying to reach Sandhya through the balcony. That is where SP knows that both of them love each other. And he tells that he knew it all the while and he arranged for everything. He fixes the marriage of Sandhya and Srikumar. He transfers everything he has to Sandhyas name because Srikumars family thought Sandhya was an orphan. SP tells everything to Parameshwaran Mamas family which is overheard by Sandhya who runs home crying. SP upset runs to comfort her, where he explains how he will live his life as an apology to her mothers curses. But Sandhya tells that her mother loved him and told her to ask forgiveness if she sees him. Now Sandhya and SP re-unite as siblings. During the marriage festivities, Bhavani and her brother Hareendhran (Biju Menon) come to prevent the celebrations. Along with them comes the elder brother of Sandhya, Shekarankutty (Lalu Alex). He challenges SP, saying that he has more right over Sandhya as he is her brother, while SP is just a stepbrother. Shekarankutty then claims all of SPs property, which SP is willing to give, provided Sandhya lives happily and marries Srikumar. Bhavanis household members use Shekarankutty to take advantage of SP; first by trying to snatch his company then trying to snatch his home. In the brawl, SP vows that Sandhya will marry Srikumar and he will wipe off everyone who stands in the way. Bhavani then tells SP that Sandhya will be married off to Hareendhran. Shekarankutty supports in the name of revenge.

Sandhya, torn between two brothers, comes running when the brothers fight among themselves on the account of who Sandhya will marry. She says she would marry anyone that her brothers tell ber too because she cares about both of them. SP then says he know what needs to be done to end the family feud, which should have been long ago. He then marches to Bhavanis house and starts hitting Bhavanis dad thinking of killing him. In the end, SP points at Kuruvilla and says he is the witness of Vishnus murder. Bhavanis father finally admits that he killed Vishnu and framed it on SPs father. Then SP pours kerosene on Bhavanis dad and as he approach Bhavani, he is stopped by Hareendharan who begs for his sisters life. Bhavani realizes her mistakes and goes to SPs house to apologize. But she tells SP that only thing she can now offer is the marriage proposal of her sister Bhama to SP. SP initially refuses when Srikumar tells that hell also remain bachelor if SP doesnt marry. SP finally agrees and tells him to go inside with Bhama.

==Cast==
*Mammootty as Sathyaprathapan (SP) Mukesh as Srikumar Rambha as Bhama Bhavana as Sandhya Indraja as Bhavani (Elder sister of Bhama) Innocent as Kuruvila
*Lalu Alex as Balagangadharan and Shekarankutty
*Biju Menon as Hareendran (Brother of Bhama and Bhavani) Janardhanan as Parameshwaran Mama (Srikumars father)
*Harisree Ashokan as Ugran (friend of Srikumar)
*Edavela Babu as Kuruvila (young)
*Sabitha Anand as Saraswathi (SPs Mother)
*Seema G. Nair as Shekarankuttys mother
*Zeenath as Bhavani, Hareendran, and Bhamas Mother
*K. P. A. C. Lalitha as Vimala (Srikumars mother)
*Master Joemon as Joemon (SPs Neighbour)
*Deepika Mohan

==Reception== Siddique kept his history of never delivering a flop.

==Controversy==
Siddique later claimed that the movies collections were affected by pirated CDs and decided against directing another movie in Malayalam until piracy is controlled.He has since done many movies regardless of piracy.

==Soundtrack==
Chronic Bachelors songs are composed by Deepak Dev as Debutant. The music album has 7 songs:

{| class="wikitable"
|-
! Track !! Song Title !! Singer(s)
|-
| 1 || "Pakalpoove" || Dr. K. J. Yesudas, Renuka
|-
| 2 || "Shilayil Ninnum" || Sujatha Mohan, Dr. Fahad
|-
| 3 || "Chundathu" || M. G. Sreekumar, Chitra Iyer
|-
| 4 || "Chirichiriyo" || Dr. K. J. Yesudas, Ganga
|-
| 5 || "Swayamvara Chandrike"  || P. Jayachandran, Sujatha Mohan
|-
| 6 || "Kannil Nilavu" || K. S. Chitra
|-
| 7 || "Theme Song" || Deepak Dev - (Instrumental)
|}

==External links==

 
 

 
 
 
 
 
 
 
 
 
 
 

 