El Yazısı
{{Infobox film
| name = El Yazısı
| foreign name = One Day or Another
| image =
| alt = 
| caption = Theatrical release poster
| director = Ali Vatansever
| producer =     Selin Vatansever  Oya Özden
| writer = Ali Vatansever
| starring = Cansu Dere, Sarp Akkaya, Baran Akbulut, Wilma Elles, Sercan Badur, Bahtiyar Engin, Kenan Bal, Salih Kalyon
| music = İnanç Şanver    
| cinematography = Ali Olcay Gözkaya
| editing = Baptiste Gacoin
| studio  = Terminal Film
| distributor = | released =  
| runtime = 95 minutes
| country = Turkey
| language = Turkish, French, English
| official website = http://www.elyazisifilm.com/
}}

El Yazısı (international title: One Day or Another) is a Turkish drama, comedy film written and directed by Ali Vatansever (Ali Vatansever). Produced by Terminal Film, it follows three stories of hope on a single day in an oppressed small Anatolian town, when they are welcoming their very first foreign English teacher.

Bringing together a powerful Turkish cast of Cansu Dere (Cansu Dere), Sarp Akkaya (Sarp Akkaya), Sercan Badur (Sercan Badur), Salih Kalyon (Salih Kalyon), Baran Akbulut (Baran Akbulut), Bahtiyar Engin (Bahtiyar Engin), Ayşe Selen (Ayşe Selen), Kenan Bal (Kenan Bal) and a foreign actress Wilma Elles (Wilma Elles), the film will be released on March 23, 2012 in Turkey. 

El Yazısı is also one of the first partially crowdfunded film in Turkey.  , El Yazısı Crowdsourcing Page. 

==Awards==
Prior the production, El Yazısı won a script development and co-production support award at 45th Golden Orange International Film Festival, Antalya.  , 45th Golden Orange Film Festival Awards List. 

==Plot==
Set in a humble Anatolian town, El Yazısı follows three stories on a day when the town is welcoming its very first foreign English teacher. As an accidental tourist is mistaken for her, the wind of change starts to blow.

The first story is young Ahmets. He loves a girl from the village; both the townspeople and the villagers are against this relationship. Today, as he is planning to elope with her, he is assigned to guide the foreign teacher around the town. 

Second story is about Zeynep, the towns pharmacist from the big city. She is about to marry the towns respected teacher Celal. Today, Volkan, a drug representative and an old friend, arrives for stock-taking. Wedding news is a surprise for him. 

And finally Ragıp, an eight-year-old boy who is on a quest for his missing love letter to Zeynep. As the letter gets stolen during the welcoming ceremony, he traces the leads with Sevgi, a little girl from the village with a crush on Ahmet.

El Yazısı is a tale about the ones who try to break the enduring morality of the small-town life.

==References==
 

== External links ==
*  
*  

 
 
 