Die große Liebe (1931 film)
 
{{Infobox film
| name           = Die große Liebe
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Otto Preminger
| producer       = Philipp Hamber (Allianz Film GmbH)
| writer         = Artur Berger Siegfried Bernfeld
| narrator       = 
| starring       = Attila Hörbiger Hansi Niese
| music          = Walter Landauer
| cinematography = Hans Theyer
| editing        = Paul Falkenberg   
| studio         = 
| distributor    = Süd-Film
| released       = December 21, 1931
| runtime        = 76 minutes
| country        = Austria
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1931 Cinema Austrian drama film directed by Otto Preminger, the first of his career. The screenplay by Artur Berger and Siegfried Bernfeld is based on a true story.

==Plot==
Ten years after the end of World War I, Austrian soldier Franz leaves Russia and returns to his village, where he is reunited with Frieda, a woman who believes he is her long-lost son.

==Production== Viennese musical comedy star Hansi Niese in the lead roles and managed to elicit from them an acting style better suited to the screen than stage. The film was produced by Philipp Hamber, at the time owner of Allianz Film GmbH.    Werner Michael Schwarz, Die Brüder Hamber und die Kiba. Zur Politisierung der Vergnügens im
Wien der Zwischenkriegszeit. In: Christian Dewald (Hrsg.), Arbeiterkino. Linke Filmkultur in der
ersten Republik, Wien 2007, S.118  Although the film, which opened at the Emperor Theater in Vienna on December 21, 1931, was a critical and commercial success, in later years Preminger described it as a juvenile folly he preferred to forget.  

==References==
 

==External links==
 

 

 
 
 
 
 
 
 


 
 