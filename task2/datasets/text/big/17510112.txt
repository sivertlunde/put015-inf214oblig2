Bowl of Oatmeal
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Bowl of Oatmeal
| image          = Bowl of Oatmeal cover art.jpg
| image size     =
| caption        =
| director       = Dietmar Post, Lawrence Gise, Matthew Bezanis, Leslie Hucko, David White, Hsia-Huey Wu
| producer       = Dietmar Post, Lawrence Gise, Matthew Bezanis, Leslie Hucko, David White, Hsia-Huey Wu
| writer         = Lawrence Gise
| narrator       =
| starring       = Pietro Gonzales, Will Bartlett
| music          =
| cinematography = Dietmar Post, Matthew Bezanis
| editing        = Dietmar Post, Karl-W. Huelsenbeck
| distributor    = Play Loud! Productions {{cite web|url=http://www.imdb.com/title/tt0806031/ | title=Bowl of Oatmeal (1996)
| accessdate=2008-05-19}} 
| released       = September 1996 at UFVA Student Film and Video Festival in Philadelphia (USA)
| runtime        = 10 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Bowl of Oatmeal is a 1996 film directed & produced by a group of six students at New York University - School of Continuing Education. Among this group was the award winning German-American film director Dietmar Post.  The American-Chilean theatre actor Pietro Gonzales played the key character. Gonzales appeared later in many plays, TV series and also in Sidney Pollacks The Interpreter.

==Cast==
* Pietro Gonzales - The Man
* Will Bartlett - The Oatmeal

==Synopsis==
A lonely man on the brink of emotional  desolation talks to his Oatmeal.
His need for friendship compels the man to a bizarre act.

==Critical reception==
The film premiered at UFVA Student Film and Video Festival in Philadelphia (USA), September 1996.
 New York were very supportive of this film. 

Michael Simmons of L.A. Weekly writes: "Bowl of Oatmeal, a rare group effort led by Dietmar Post, is the tale of a hermit in the throes of a nervous breakdown who receives advice from a sly, articulate bowl of oatmeal with a Bostonian accent."

British critic Rob Daniel writes: “Bowl of Oatmeal serves as a dress rehearsal for the final film, but in its own right stands as unsettling cinema. An agoraphobic loner inexorably loses his mind within the confines of a bed-sit. Not even the revelation that a bowl of unappetizing oatmeal is taunting him alleviates the
gloom. As the high fiber breakfast continues its needling, the man develops a keen interest in
dead meat, but the film takes this into an area unexpected and haunting.”

==Awards==

* 1996 Winner of the Director’s Choice Award at the UFVA Student F&V Festival in Philadelphia (USA)
* 1998 3rd Winner of the Audience Award “eject” at 14.Intern. Short Film Festival Berlin (Germany)

==References==
 

==External links==
*   Official movie site
* 

 
 
 
 
 


 