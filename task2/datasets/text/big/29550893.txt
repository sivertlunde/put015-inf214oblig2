Meri Adalat (2001 film)
 
{{Infobox film
 | name = Meri Adalat
 | image = MeriMithun.jpg
 | caption = DVD Cover
 | director = Hamid Ali
 | producer = 
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Suvarna Mathew Vishal Bakshi Meghna Raza Murad Shakti Kapoor Prem Chopra
 | music = Babul Bose
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = December 14, 2001
 | runtime = 135 min.
 | language = Hindi Rs 1.8 Crores
 | preceded_by = 
 | followed_by = 
 }}
 2001 Hindi Indian feature directed by Hamid Ali, starring Mithun Chakraborty, Suvarna Mathew, Meghna, Vishal Bakshi, Prem Chopra, Shakti Kapoor and Raza Murad

==Plot==
Vijay Verma (Mithun) is a journalist and his family consists of his father Satya Prakash (Prithvi Zutshi), mother (Lekha Govil) and his younger brother Vishal. Vijay Verma is in love with, Inspector Kiran Chowdhary, who is a dare devil police officer. Mariya falls in love with Vishal, at a musical show, but Vishals father is against this affair, but circumstances forces Vishal to marry Mariya, as Vijay convinces his father for the marriage. At home, Mariya is shocked to see Vijay, as she has seen Vijay committing a murder and Vijay too recognizes her, moreover Inspector Kiran has been assigned to investigate the same murder case. Climax provides answers for all questions.

==Cast==
*Mithun Chakraborty
*Suvarna Mathew
*Meghna Pashto
*Vishal Bakshi
*Deepak Shirke
*Prem Chopra
*Shakti Kapoor
*Raza Murad
*Prithvi Zutshi
*Lekha Govil

==References==
* http://www.indiaweekly.com/dvdImages/b4101.jpg
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Meri+Adalat+%282001%29

==External links==

 
 
 
 
 
 
 