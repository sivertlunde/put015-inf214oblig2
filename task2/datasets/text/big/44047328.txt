Krishnapparunthu
{{Infobox film
| name = Krishnapparunthu
| image =
| caption =
| director = O Ramdas
| producer = Babu George
| writer = Sreerangam Vikraman Nair
| screenplay = Sreerangam Vikraman Nair Madhu Srividya Sreelatha Namboothiri Shyam
| cinematography = Jayanan Vincent
| editing =
| studio = Navabhavana Films
| distributor = Navabhavana Films
| released =  
| country = India Malayalam
}}
 1979 Cinema Indian Malayalam Malayalam film, directed by O Ramdas and produced by Babu George. The film stars P Jayachandran, Madhu (actor)|Madhu, Srividya and Sreelatha Namboothiri in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
  
*P Jayachandran  Madhu 
*Srividya 
*Sreelatha Namboothiri 
*Prathapachandran  Ambika 
*Bahadoor 
*Dr Namboothiri
*K. P. Ummer 
 

==Soundtrack== Shyam and lyrics was written by Onakkoor Radhakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anjanashilayile || P Jayachandran, Jaseentha || Onakkoor Radhakrishnan || 
|- 
| 2 || Janananmaykkaay || K. J. Yesudas, P Jayachandran || Onakkoor Radhakrishnan || 
|- 
| 3 || Makara Maasathile || K. J. Yesudas, P Susheela || Onakkoor Radhakrishnan || 
|- 
| 4 || Thrissivaperoore || P Jayachandran, Jaseentha || Onakkoor Radhakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 