Pope Dreams
{{Infobox film
| name           = Pope Dreams
| image          = Pope Dreams.jpg
| alt            =
| caption        = Movie Poster
| director       = Patrick Hogan 
| producer       = Steve Loh
| writer         = Parick Hogan
| starring       = Phillip Vaden Julie Hagerty Stephen Tobolowsky
| music          = Joel J. Richard
| cinematography = John Ealer
| editing        = Mae Edwards
| distributor    = PorchLight Entertainment
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} 2006 film written and directed by Patrick Hogan.  The film won five awards at various film festivals. 

==Tagline==
"Hes dealing with love... fighting with his dad... caring for mom... and making music. Whats that got to do with the Pope?"

"Where would your dreams take you?"

==Plot==
 
An aspiring drummer wants his dying mother to get an audience with the pope while he also tries to impress a girl out of his league.

==Principal cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Phillip Vaden|| Andy Venable
|-
| Marnette Patterson || Brady Rossman
|-
| Julie Hagerty|| Kristina Venable
|-
| Stephen Tobolowsky || Carl Venable
|- Noel Fisher || Pete
|-
| Samantha Anderson || Heather
|-
| Casey Barclay || Eric
|- Lucas
|}

==References==
 

== External links ==
*  
*  
 

 
 
 
 
 


 