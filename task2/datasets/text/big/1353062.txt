The Saint Strikes Back
{{Infobox film
| name            = The Saint Strikes Back
| caption         =
| image           = The Saint Strikes Back FilmPoster.jpeg
| director        = John Farrow
| producer        = Robert Sisk
| writer          = John Twist
| starring        = George Sanders Wendy Barrie Jonathan Hale
| music           = Roy Webb
| cinematography  = Frank Redman
| editing         = Jack Hively
| distributor     = RKO Radio Pictures
| released        =  
| runtime         = 64 min.
| language        = English
| budget          = $128,000 Richard B. Jewells RKO film grosses, 1929–51: the C. J. Trevlin Ledger: a comment, Historical Journal of Film, Radio and Television, Volume 14, Issue 1, 1994 
| gross           = $460,000 
}}
 American crime The Saint film series about The Saint.

In the film The Saint foils an assassination attempt by a member of Val Travers gang, but is arrested by the police for the murder of the gang member.  He persuades them to let him assist in apprehending a shadowy criminal mastermind.
The script was based on the Leslie Charteris novel She Was a Lady (Hodder and Stoughton, 1931) which was also published as Angels of Doom and The Saint Meets His Match. The screenplay was by John Twist, who set the story in San Francisco (the book is set in England). Robert Sisk produced and John Farrow directed.

==Plot==
While dancing at a New Years party, the Saint spots an agent of Valerie "Val" Travers preparing to shoot someone, so Templar guns him down first at the stroke of midnight. Templar is placed by witnesses at the scene, so the San Francisco police request the assistance of Inspector Henry Fernack (Jonathan Hale) of the New York police department. Before Fernack can leave, the Saint arrives in New York and accompanies him to the west coast.

Val Travers father had been a police inspector whose efficiency caused trouble for a mysterious criminal mastermind named Waldeman. When a large sum of money was found in his safe deposit box, however, he was fired on suspicion of working for Waldeman and committed suicide. Travers is determined to clear his name by any means necessary. The Saint takes up her cause, despite her hostility for his interference in her plans and her suspicions about his motives. Templar gets the cooperation of the police commissioner, over the objections of Chief Inspector Webster and criminologist Cullis, who wonder if the Saint is Waldeman himself.  

Templar and Travers cross paths again when the trail leads to Martin Eastman, a noted philanthropist and seemingly-irreproachable citizen, whom they both suspect is linked to Waldeman in some way. Templar forces Travers and her gang to drive away, all except her burglar, Zipper Dyson. Templar gets Dyson to open Eastmans safe and takes the money inside. The serial numbers confirm that it was stolen in a robbery perpetrated by Waldeman. When Eastman contacts Cullis instead of reporting the theft, Templar knows that Cullis is also working for Waldeman. With that information, not only does the Saint exonerate Travers father, he also identifies Waldeman.

==Cast==
* George Sanders as The Saint / Simon Templar
* Wendy Barrie as Valerie "Val" Travers
* Jonathan Hale as Inspector Henry Fernack
* Jerome Cowan as Cullis
* Barry Fitzgerald as Zipper Dyson, a burglar working for Travers Neil Hamilton as Allan Breck, Travers friend and admirer Robert Elliott as Chief Inspector Webster
* Russell Hopton as Harry Donnell, another of Travers gangsters
* Edward Gargan as Pinky Budd, one of Travers henchmen
*Robert Strange as Police Commissioner
* Gilbert Emery as Martin Eastman James Burke as Headquarters Police Officer
* Nella Walker as Mrs. Betty Fernack

==References==
 	

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 