Special ID
 
{{Infobox film
| name           = Special ID
| image          = SpecialID.jpg
| caption        = China poster
| film name      = {{Film name
 | traditional    = 特殊身份
 | simplified     = 特殊身份 
 | pinyin         = Tè Shū Shēn Fèn
 | jyutping       = Dak6 Syu4 San1 Fan2 }}
| director       = Clarence Fok
| producer       = Yong Er Donnie Yen
| writer         = Clarence Fok
| starring       = Donnie Yen  Jing Tian   Andy On   Zhang Hanyu Ronald Cheng Collin Chou
| music          = Dou Peng
| cinematography = Peter Pau Cheung Ka-fai
| studio         = Eastern New Vision Film & TV Culture   
| distributor    = 
| released       =  
| runtime        = 99 minutes
| country        = China 
| language       = {{plainlist|
*Cantonese
*Mandarin
*English}}   
| budget         = 
| gross          = US$29,139,936 
}}
Special ID is a 2013 Chinese action film directed by Clarence Fok and starring Donnie Yen. 

==Plot==
Yen takes on the role of Dragon Chan, a Hong Kong undercover police officer deep within the ranks of one of China’s most ruthless underworld gangs. The leader of the gang, Xiong (Collin Chou), has made it his priority to weed out the government infiltrators in his midst. Struggling to keep his family together and his identity concealed, Chan is torn between two worlds.

Upping the stakes, as Chan’s undercover comrades are being dealt with, one by one, Chan fears his days are numbered. Now, he must risk everything to take down the organization and reclaim the life he lost when he took on this perilous assignment. As the action mounts, Chan must do everything he can to protect the SPECIAL IDENTITY he wishes he never had before it’s too late.

==Cast==
*Donnie Yen as Dragon Chan
*Jing Tian as Fang Jing
*Andy On as Sunny
*Zhang Hanyu as Blade
*Ronald Cheng as Captain Cheung
*Collin Chou as Xiong
*Paw Hee-ching as Sister Amy, Chans mother
*Yang Zhigang as Captain Lei Peng
*Rain Lau as Mary
*Ken Lo as Brother Bao
*Frankie Ng as Mahjong player
*Qi Daji
*Terence Yin as Sunnys assistant
*Evergreen Mak Cheung-ching as Brother Kun

==Critical response== average score of 46, based on 6 reviews indicating "mixed or average reviews". 

Andrew Chan of the Film Critics Circle of Australia writes, "The good news is that “Special ID” remains largely entertaining for most of the duration, but in many ways it remains a huge pile of mess." 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 