Hamlet (1954 film)
{{Infobox film
| name           = Hamlet 
| image          = Hamlet_(1954).jpg
| image_size     = 
| caption        = Screen shot of Kishore Sahu as Hamlet
| director       = Kishore Sahu
| producer       = Kishore Sahu
| writer         = Shakespeare Kishore Sahu  (screenplay) 
| narrator       = 
| starring       = Kishore Sahu Mala Sinha Venus Bannerjee
| music          = Ramesh Naidu
| cinematography = K. H. Kapadia  
| editing        = Kantilal B. Shukla
| distributor    =
| studio         = Hindustan Chitra 
| released       = 1954
| runtime        = 79 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1954 Hindi tragedy drama film, produced and directed by Kishore Sahu.    The film was a free adaptation of Shakespeares tragedy, with Sahu playing Hamlet as well as writing the screenplay, while the dialogues were by Amanat Hilal and B. D. Verma.    It was produced under the "Hindustan Chitra" banner, a production company started by Sahu in 1944.    It was Ramesh Naidus debut film as a music composer. The film starred Mala Sinha, Kishore Sahu, Venus Banerji, Kamaljeet and Jankidas.   

Sahu was influenced by "classic European sources".     Though termed a "free adaptation" in the credit roll of the film, Sahus Hamlet stayed true to the title, its setting, and the original names in the play, remaining as close as possible to Laurence Oliviers Hamlet (1948 film)|Hamlet (1948).   

==Plot==
After seeing his fathers ghost the film follows the play focusing on Hamlets revenge on his Uncle Claudius, who has married his mother Gertrude after murdering Hamlets father. He pretends to be insane and is in the process of staging a play where he plans to denounce his mother and Uncle. 

==Cast==
* Kishore Sahu as Hamlet
* Mala Sinha as Ophelia
* Venus Banejee
* S. Nazir as Polonius
* Kamaljeet as Laertes
* Jankidas as Osric
* Shreenath as Horatio
* Rajan Kapoor
* Hiralal as Claudius
* Paul Sharma
* Haroon

==Production==
There were several plot changes, with Ophelia telling her part in flashback and singing songs with friends, while the gravediggers were "used for comic effect", thus giving in to Indian film-goers sensibilities.   The film had its inspiration from the Parsi theatre days, with Sahus monologue inculcating couplets from famous Indian poets and using parts of dialogues from Ahsans Khoon-Nahak (1928). Ophelia sang Bahadur Shah Zafars "Na Kisi Ki Ankh Ka Noor Hoon", and a dying Hamlet quoted Zauqs "Layee Hayaat Aaye, Qaza Le Chali Chale".    

The "Parsi theatre tradition", which gave rise to several freely adapted Hindi films from Shakespeare, like Modis Khoon Ka Khoon (1935), Akhtar Hussains Romeo and Juliet (1947) and Cleopatra (1950), came to an end with Hamlet.   

==Reception==
The film did "reasonably well" at the box office. Acclaimed by the Filmfare critic, it was panned harshly by FilmIndia, which called it a "slander" to Shakespeare. As stated by Manju Jain, Sybil Thorndyke who was present at the premiere of the film in Bombay thought that Gertrude was "magnificent".   

==Adaptations==
* Khoon-E-Nahak (Murder Most Foul) (1928) was the first Hindi film adaptation, directed by Dada Athawale and written by Mehdi Hassan Ahsan.   

* Khoon Ka Khoon (Hamlet) (1935), the next adaptation directed by Sohrab Modi, had Modi playing Hamlet with Naseem Banu as Ophelia, and Naseems mother Shamshadbai playing Gertrude. 

* Hamlet (1954) by Kishore Sahu was the closest to the original play,    and is cited as the "most noted adaptation".    

* Haider (film)|Haider (2014) is an adaptation set against the Kashmir conflict and directed by Vishal Bhardawaj. 

==Soundtrack== Telugu films in the 1970s, the most popular being Meghasandesam (1983), for which he won the National Film Award for Best Music Direction. The lyrics were written by Hasrat Jaipuri, while the playback singing was provided by Asha Bhosle, Mohammed Rafi and Jagmohan Bakshi.   

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Ankhon Mein Pyar Mere Diwana Haal Mera 
| Asha Bhosle
|-
| 2
| Aa Jao Mere Pyare Arman Tujhko Pukare 
| Asha Bhosle
|-
| 3 Chahe Sataye Wo Chahe Rulaye 
| Asha Bhosle
|-
| 4
| Sitamgar Kya Maza Paya Bata To Dil 
| Asha Bhosle
|-
| 5
| Ghir Ghir Aaye Badarwa O Bhaiya 
| Mohammed Rafi, Jagmohan Bakshi 
|}

==References==
 

==External links==
* 

 

 

 
 
 
 
 