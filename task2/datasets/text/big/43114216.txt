Julius Caesar (1914 film)
{{Infobox film
| name           = Julius Caesar 
| image          =
| caption        =
| director       = Enrico Guazzoni 
| producer       = Enrico Guazzoni
| writer         = William Shakespeare (play)   Raffaele Giovagnoli 
| starring       = Amleto Novelli   Bruto Castellani   Pina Menichelli
| music          = 
| cinematography = 
| editing        = 
| studio         = Società Italiana Cines 
| distributor    = Società Italiana Cines 
| released       = November 1914  
| runtime        = 112 minutes
| country        = Italy
| awards         =
| language       = Silent   Italian intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent historical play of Quo Vadis it was produced on an epic scale, including vast sets recreating Ancient Rome and more than 20,000 extras. 

==Cast==
* Amleto Novelli as Julius Caesar  
* Bruto Castellani  
* Irene Mattalia as Servilia  
* Ignazio Lupi 
* Augusto Mastripietri 
* Antonio Nazzari as Brutus  
* Gianna Terribili-Gonzales 
* Lia Orlandini
* Ruffo Geri 
* Pina Menichelli 
* Orlando Ricci

== References ==
 

== Bibliography ==
* Moliterno, Gino. Historical Dictionary of Italian Cinema. Scarecrow Press, 2008.

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 