Jailor (1938 film)
{{Infobox film
| name           = Jailor 
| image          = 
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Minerva Movietone
| writer         = Kamal Amrohi Ameer Haider
| narrator       = 
| starring       = Sohrab Modi Leela Chitnis Sadiq Ali Eruch Tarapore
| music          = Mir Saheb
| cinematography = Y. D. Sarpotdar
| editing        = 
| distributor    =
| studio         = Minerva Movietone
| released       = 1938
| runtime        = 150 minutes
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1938 Hindi psychosocial melodrama film produced and directed by Sohrab Modi. Produced by Minerva Movietone, the story and lyrics were written by Kamal Amrohi and Ameer Haider with screenplay by J. K. Nanda. The film had music direction by Mir Sahib, while the cinematographer was Y. D. Sarpotdar.    The film starred Sohrab Modi, Leela Chitnis, Sadiq Ali, Eruch Tarapore, Abu Bakar, Baby Kamala and Kusum Deshpande. 

The film shows the transformation of a tolerant, kind-hearted jailor into a ruthless, intolerant tyrant when his wife leaves him for another man. Modi in his psychodramas tended to use a "misogynist viewpoint" regarding problems in marriage.    The role, as cited by Rishi, was "chillingly portrayed" by Sohrab Modi.      The film was remade with the same title Jailor (1958 film)| in 1958 with Modi playing the same role, that of the Jailor with a different supporting cast.

==Plot==
Sohrab Modi is the benevolent prison warden whom everyone likes except his wife Kanwal (Leela Chitnis). The wife elopes with a doctor, Dr. Ramesh, leaving her young daughter behind. This turns the normally kind-hearted Jailor into a tyrannical man of whom everyone is scared. Circumstances make him bring his wife home when she and her lover meet with an accident and the lover turns blind. The Jailor keeps her imprisoned in a room, where she eventually kills herself due to his ill-treatment of her. The Jailor meets a blind girl and starts changing his despotic ways. On realising that the blind girl too loves Dr. Ramesh he helps unite them.
 
==Cast==
* Sohrab Modi as Jailor 
* Leela Chitnis as Kanwal 
* Sadiq Ali 
* Eruch Tarapore,  
* Abu Baker 
* Kumari Kamala 
* Kusum Deshpande 
* Sheila 
* Sharifa 

==Soundtrack==
The music was composed by Mir Saheb with Lyrics by Kamal Amrohi. The singers were Sheela, Leela Chitnis and Sadiq Ali. 
===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Allah Wale Teri Nagri Mein Bolta Hai Kaun
| Sheela
|-
| 2
| Bhala Kare Bhagwan
| Sheela
|-
| 3
| Yeh Maya Aani Jaani Hai
| Chorus
|-
| 4
| Yeh Maya Aani Jaani Hai Yeh May Behta Pani Hai
| Sheela
|-
| 5
| Kahe Ko Ab Rove Moorakh Rove
| Leela Chitnis
|-
| 6
| Prem Ka Ik Sansar Basa Le 
|
|-
| 7
| Kaaya Ret Gharonda Hai Iska Sab Kuchh Ret Ki Kaaya
| Khan Mastana
|-
| 8
| Jahan Mein Kash Paida Hi Na Hote
| Leela Chitnis, Sadiq Ali
|-
| 9
| Kaahe Ko Bihai Bides Re Sun Babul Mora
| E Tarapore
|-
| 10
| Kaise Kategi Mori Rain, Rain Birha Ki
|
|-
| 11
| Prem Ka Dukh Hai Apaar Sakhi Ri
| Sheela
|}

==References==
 

==External links==
* 

 

 
 
 
 