The Gypsy Moths
{{Infobox film
| name           = The Gypsy Moths
| image          = Gypsymoths.jpg
| image_size     =
| caption        = The Gypsy Moths Movie poster
| director       = John Frankenheimer Bobby Roberts Edward Lewis  
| screenplay     = William Hanley
| based on       = story by James Drought
| narrator       = Scott Wilson
| music          = Elmer Bernstein
| cinematography = Philip H. Lathrop
| editing        = Henry Berman
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 107 mins
| country        = United States
| language       = English language
| budget         =
}}
The Gypsy Moths is a 1969 American drama film directed by John Frankenheimer and starring Burt Lancaster and Deborah Kerr, based on the novel of the same name by James Drought.

It is the story of three barnstorming skydivers and their effect on a midwestern American town. At the time, the sport of skydiving was in its infancy, yet the movie featured an extreme variation of the sport known as wingsuit flying. Influenced by this movie, wingsuits gained a prominent resurgence in the 21st century. Todd Higley, a prominent skydiver in the Seattle area today, is said to have been the main technical advisor and stunt double for Mr. Lancaster, and today is well known for having invented wingsuit BASE jumping.
 Bonnie and Separate Tables. The movie focuses on the differences in values between the town folk and the hard living skydivers and features Deborah Kerrs only nude love scene in her movie career. Deborah Kerr, Braun, Eric, St. Martins Press, ISBN 0-312-18895-1, 1978 

The director, John Frankenheimer, expressed his anguish and disappointment at the critical reception of this piece and subsequent narrow release in the United States. The film was widely seen in Australia and the local skydiving fraternity there was quick to seize the opportunity to promote their sport.

Elmer Bernstein composed the score.

==Plot== Scott Wilson).
 William Windom, Deborah Kerr). The distractions begin almost immediately. Mike becomes romantically involved with Elizabeth, whose husband overhears her making love with Mike in their home. Malcolm falls for local student Annie Burke (Bonnie Bedelia), a boarder in the Brandon house. Joe takes an interest in a topless dancer.

Mike eventually asks Elizabeth to leave town with him, but she declines. During the next skydiving exhibition, Mike intends to do a spectacular "cape jump" stunt but fails to pull the ripcord, and hits the ground at over 200 miles per hour.  Although nobody wants to discuss it, theres an underlying suspicion that his death was not an accident, and he simply decided not to open his chute.  That night, Annie consoles Malcolm, and they make love.  Before the team leaves for good, they have to bury Mike, and Malcolm does the same stunt that killed Mike to pay for the funeral.  He leaves by train that night, without attending the funeral, and were left to wonder if hes heading down the same aimless path as Mike.

==Cast==
*Burt Lancaster as Mike Rettig
*Deborah Kerr as Elizabeth Brandon
*Gene Hackman as Joe Browdy Scott Wilson as Malcolm Webson William Windom as John Brandon
*Bonnie Bedelia as Annie Burke
*Sheree North as the Waitress

==Production== decoding the "DGA" designation, he opines that "Youre much better off jumping out of it, than taking a chance on landing it." This line was probably unscripted, but attributable to the Howards pilot, David Llorente.

Carl Boenish did the aerial photography.
 Scott Wilson replaced John Phillip Law after Law broke his wrist. 

==Reception==
The film ran in limited release in the U.S. and saw few theaters taking it in for extended showings. Soon as it appeared, it disappeared, not getting an audience and it did not run until it came to TV years later. Director John Frankenheimer was depressed. He felt the film did not get enough attention as his thrillers, like Seconds (film)|Seconds and The Manchurian Candidate. Despite this, he would call this his favorite film.

==References==
 

==External links==
* 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 