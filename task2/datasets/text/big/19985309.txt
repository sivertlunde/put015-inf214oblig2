Drona (2009 film)
{{Infobox film
| name        = Drona
| image       =
| writer      = Nitin Priyamani Rakhi Sawant
| director    = J.Karun Kumar
| producer    = D.S. Rao
| distributor = Sai Krishna Productions
| cinematography =
| released    = 20 February 2009
| runtime     = Telugu
| music       = Anoop Rubens
| country     = India
| budget      =
|}} Telugu film, starring Nitin Kumar Reddy|Nitin, Priyamani, Kelly Dorjee, Mukhesh Rishi, Sita, Sunil Verma|Sunil, Raghubabu, directed by Karunakumar. Produced by DS Rao, and music composed by Anoop Rubens. The film released in 2009.  In spite of publicity stunts like Priyamanis bikini act, the film turned out to be an average flick at the box-office.

==Story==
Drona (Nitin Kumar Reddy|Nitin) is the only son of Mukhesh Rushi and Sita. Drona’s father Mukesh Rushi is an honest and dedicated Police officer. Drona likes his mother Meenakshi (Sita) so much and love her as his own life. When he was a child Drona steals his father’s revolver to scare his friends. On knowing this his father scolds him, consequently Drona leaves his home and gets lost. After 10 years, he comes back to his parents with the name Chandu. A Neighboring girl Indu (Priyamani) likes Drona/Chandu a lot. On a fateful day, she finds out that Chandu is none other than the long lost boy, Drona. Its revealed that Drona and his friend were kidnapped along with other boys. They were forced to become thieves. Drona had no other choice to kill a group of police officers in self-defense, while trying to escape with the jewelry because the bad guys have to take poison out of Dronas body and the other guys might get killed by the bad guys. The officers had duty to shoot Drona. Drona made a bad choice to kill 2 innocent people flying a police helicopter. Drona shouldnt have killed the people, who were flying the helicopter. Drona becomes an approver to send the robbers and their kingpin to jail.

==Credits==
* Banner: Sai Krishna Productions
* Starring: Nitin (actor)|Nitin, Priyamani, Kelly Dorji, Mukhesh Rushi, Sita, Sunil (actor)|Sunil, Raghu Babu, Sivaprasad and Rakhi Sawant
* Producer: DS Rao
* Director: Karunakumar
* Music Director: Anoop Rubens
* Cinematographer: Bhupati
* Release date:20 February 2009

==Music== Ranjith
*Voddantana- Rehan, Pranavi
*Drona Theme - Raghu Ram, Murali, Siddardha
*Vaade Vaade - Shreya Ghoshal
*Vennela Vaana - Udit Narayan, Soumya
*Sayyare Sayya - Kalpana Khan
*Sentiment Bit - Revathi
*Yem Maaya Chesavo - Harshika, Ranjith

==Box office==
Drona was just an average grosser.

==References==
 
*  News Source:indiaglitz.com  . Fair use under educational and information purpose.  

 
 
 
 


 