£500 Reward
 
 
{{Infobox film
| name           = ₤500 Reward
| image          = 
| image_size     =
| caption        = 
| director       = Claude Flemming
| producer       = Claude Flemming Barry Lupino
| writer         = Claude Flemming
| narrator       =
| starring       = Claude Flemming Renée Adorée
| music          =
| cinematography = Lacey Percival
| editing        = 
| distributor    = 
| released       = 18 November 1918
| runtime        = five reels 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

£500 Reward is a 1918 Australian silent film starring, written, produced, financed and directed by Claude Flemming who later described it as "a very lurid melodrama".   

==Synopsis==
A "five-act" drama about a couple who travel from the Rocky Mountains to Seattle then wind up shipwrecked en route to Queensland.  The heroine Irene is kidnapped by a ship captain and winds up wrecked on the Queensland coast. She is rescued by the hero.

==Cast==
*Claude Flemming as Jack John Faulkner as Captain Wolff Forrest
*Renée Adorée as Irene
*Barry Lupino
*Lorna Ambler
*David Edelsten

==Production==
The film was written, produced, directed and financed by Claude Flemming. It was reported to be the first feature film to include footage of Mount Kosciuszko, which stood in for Canada.  Scenes were also shot at Bermagui, New South Wales|Bermagui, on board an American sailing ship visiting Sydney, and at Rushcutters Bay studio.

The cast included Renée Adorée, who was then a dancer touring Australia on the Tivoli circuit with an act called "The Magnys", and subsequently went on to star in The Big Parade (1925). 

John Faulkner and Claude Flemming reputedly clashed during the film over interpretation. 

==Reception==
The film ran for two weeks in a cinema in Sydney. Flemming then had to go overseas to London and by the time he came back he was unable to locate a copy of the negative and the movie was thought to be lost until a copy was found in a basement in 1938. 

During the 1920s Flemming tried to re-register the film under a different, more saleable title, but was refused six times by the New South Wales censorship board. The other titles he attempted to use were, The Lure of a Woman, Primal Passion, When Men Desire, The Auction of Virtue, The Reckless Lover, and A Romance of Two Worlds. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 