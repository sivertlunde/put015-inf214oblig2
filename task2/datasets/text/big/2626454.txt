In the Name of the King
{{Infobox film
| name           = In the Name of the King
| image          = In the Name of the King - theatrical poster.jpg
| director       = Uwe Boll
| producer       = Uwe Boll Dan Clarke Shawn Williamson Wolfgang Herold
| based on       =  
| writer         = Doug Taylor
| starring       = Jason Statham Leelee Sobieski John Rhys-Davies Ron Perlman Claire Forlani Kristanna Loken Matthew Lillard Ray Liotta Burt Reynolds
| music          = Jessica de Rooij Henning Lohner
| cinematography = Mathias Neumann
| editing        = Paul Klassen David M. Richardson
| studio         = Boll KG Productions / Herold Productions / Brightlight Pictures
| distributor    = 20th Century Fox  (Germany)  Freestyle Releasing  (US) 
| released       =  
| runtime        = 127 minutes
| country        = Germany Canada United States
| language       = English
| budget         = $60 million 
| gross          = $13,097,915   
}}
In the Name of the King is a 2007 fantasy adventure film directed by Uwe Boll, inspired by the Dungeon Siege video game series. The English spoken film was an international (mostly German and Canadian) co-production, filmed in Canada. It premiered in the Brussels Festival of Fantastic Films in April 2007 and in the theatres in November 2007.

==Plot==

In the previous war involving the Kingdom of Ehb, a three-year-old boy was found wandering the field of the Battle of Oxley Pass by the rancher Norick (Ron Perlman) and adopted by the town of Stonebridge.  While Norick could be considered his stepfather, the child was cared for by the entire town, including the family of Basstian (Will Sanderson) and Solana (Claire Forlani).  His identity unknown, the boy grew up to be known as Farmer (Jason Statham), married Solana, and was raising his first son Zeph (Colin Ford) when war suddenly struck again with a surprise attack by the Krug.

The adversary was a Magus-in-exile, Gallian (Ray Liotta), sadistic, megalomanical, and very powerful, influencing the normally primitive, almost animal-like Krug to take up arms, don armor, and fight against Ehb with a courage, intelligence, and ferocity that surprises all of the Kingdoms inhabitants.  While King Konreid (Burt Reynolds), Commander Tarish (Brian J. White), and a significant proportion of Ehbs standing army surveys the damage at and seeks recruits from Stonebridge, the Kings nephew Duke Fallow (Matthew Lillard) and Muriella (Leelee Sobieski) allow Gallian to infiltrate the castle.  Muriellas father Merick (John Rhys-Davies), the Kings Magus is with the King at Stonebridge, and takes the liberty to investigate the matter of Farmers true identity.

Farmers adopted name belies his leadership and combat abilities and, in defiance of the King, he convinces Stonebridges civilian combatants to mount a rescue mission.  Gallian, via an avatar, had killed Zeph and taken Solana and other inhabitants of Stonebridge prisoner.  Farmers rescue mission goes very badly, Gallian nearly kills him because of the threat he poses (a mechanic of Kings, Magi, and magical power in the movies world.)  Farmer kills several of Gallians avatars and escapes execution with the help of Merick, who brings him before the King to reveal his true identity as Camden Konreid, the Kings son, solving a major inheritance problem: Duke Fallow is selfish and immature, poor material for royalty even if he werent in league with Gallian.

Muriella had betrayed Ehb and her father largely by accident: she fell in love with Gallian, who proceeded to deceive and train her, stealing her power.  After she realized his dark nature, she breaks off their romance, and confesses to her father Merick, who finally has an answer to another problem of his: a growing imbalance of the magical power in Gallians favour.  To offset this, the normally reclusive nymphs of Sedgwick Forest, led by Elora (Kristanna Loken) side with Ehb against Gallian.

The King decides on a surprise attack against Gallians advancing forces, and Duke Fallow, caught in his treachery, has only his personal guard remaining.  Gallian seeks the blood of Farmer, who prevails, while Duke Fallow succeeds in mortally wounding the King, who dies after the forces of Ehb force Gallian to retreat.  Farmers brief battlefield coronation surprises everyone except Gallian, and he decides to press to the attack all the way to Gallians keep the following day.

Farmer leads a small force consisting of Merick, Muriella, and Elora through mountains to Gallians back door.  The main force led by Tarish and escape efforts led by Norick and Basstian keep Gallian busy, even as he interrogates Solana.  Gallians magical sense for royal blood reveals to him that Solana is pregnant with Farmers second child, and because of his preoccupation with this, she is able to join in the final battle between Gallian and Farmers infiltration team.  Elora is not able to enter, while Gallian kills Merick and defeats Muriellas magic; Solana and Farmer kill Gallian together.  With his magical influence gone, Gallions avatars vanish and the Krug immediately revert to their natural disposition, retreating from Tarishs hard-pressed forces.  The movie ends with the royal couple, still in their peasants clothes, happily reunited.

==Cast==
* Jason Statham as Camden Konreid/Farmer
* Leelee Sobieski as Muriella
* John Rhys-Davies as Merick
* Ron Perlman as Norick
* Claire Forlani as Solana
* Kristanna Loken as Elora
* Matthew Lillard as Duke Fallow
* Ray Liotta as Gallian
* Burt Reynolds as King Konreid Brian White as Commander Tarish
* Mike Dopud as General Backler
* Will Sanderson as Basstian
* Tania Saulnier as Talwyn Gabrielle Rose as Delinda
* Terence Kelly as Trumaine
* Colin Ford as Zeph
 House of the Dead.

==Production==
The production budget was $60 million.  Uwe bolls most expensive film production to date.

 ]]
Boll has said that two versions will be produced due to length. The first will run for 127 minutes as a single movie trimmed down for cinematic release. The second, a directors cut, will be for DVD and run for approximately 156 minutes. 

The film was shot near the Municipality of Sooke, the westernmost area of the Greater Victoria, Capital Regional District (CRD), British Columbia. Locals and First Nations people were recruited as extras and for other duties.
 The Orphanage, PICTORION das werk, Rocket Science VFX, Technicolor Creative Services, TVT postproduction, and upstart! Animation.

==Soundtrack== Threshold contributed Dead Reckoning. soundtrack producer.

==Reception==

===Box office===
In the Name of the King was a  . Accessed February 13, 2008.  It had grossed $10.3 million worldwide, including $2.47 million in Germany, $1.39 million in Russia and $1.22 million in Spain.  Afterwards, Uwe Boll announced that this would be his first and last movie with a large budget. 

===Critical reception===
The film was critically panned. The film holds a 4% "Rotten" rating on the review aggregator website Rotten Tomatoes, based on 49 reviews, with the consensus "Featuring mostly wooden performances, laughable dialogue, and shoddy production values, In the Name of the King fulfills all expectations of an Uwe Boll film." The film is also ranked in that sites 100 worst reviewed films of the 2000s  and in 2008, Time (magazine)|Time listed the film on their list of top ten worst video games movies.  Metacritic reported the film had an average score of 15 out of 100, based on 11 reviews &mdash; indicating "overwhelming dislike."  
Many critics have attacked the films close resemblances to other fantasy films, especially the popular The Lord of the Rings film trilogy|Lord of the Rings films.     

The film was nominated for five Razzie Awards, including Worst Picture, Worst Screenplay, Worst Supporting Actor (Burt Reynolds) and Worst Supporting Actress (Leelee Sobieski), with Uwe Boll winning Worst Director.

==Sequels==
 
Despite being considered a bomb, Boll filmed a  .  Filming began on December 1, 2010 and it was released in 2011. The film stars Dolph Lundgren and Natassia Malthe.

A third film,  , was filmed in 2013 but not released until 2014. The film starred Dominic Purcell, with Boll returning to direct. 

==Home media   ==
The DVD, released on April 15, 2008, does not include the 156-minute version. The Blu-ray Disc|Blu-ray release in December 2008 contains this edition. 813,147 units were sold, gathering a revenue of $14,865,984, more than its box office grossing. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   at Movieset.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 