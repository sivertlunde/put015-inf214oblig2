Year of the Carnivore
{{Infobox Film
| name = Year of the Carnivore
| image = ChesterBrownYearOfTheCarnivorePoster.jpg
| caption = 
| director = Sook-Yin Lee
| producer = Trish Dolman
| writer = Sook-Yin Lee
| starring = Cristin Milioti Mark Rendall Will Sasso Ali Liebert Luke Camilleri
| music = Sook-Yin Lee Buck 65 Adam Litovitz
| cinematography = Bruce Chun
| editing = James Blokland
| distributor = E1 Entertainment
| released =  
| runtime = 88 minutes
| country = Canada
| language = English
| budget = 
| gross = 
}}
Year of the Carnivore is a 2009 Canadian romantic comedy film about a grocery store detective with a crush on a man who rejects her because she has too little sexual experience.

Year of the Carnivore is Sook-Yin Lees feature directorial debut and it premiered at the Toronto International Film Festival as a Canada First selection. 

==Cast==
* Cristin Milioti as Sammy Smalls
* Mark Rendall as Eugene
* Will Sasso as Dirk
* Ali Liebert as Sylvia
* Sheila McCarthy as Mrs. Smalls
* Kevin McDonald as Mr. Smalls Patrick Gilmore as Todd
* Luke Camilleri as Jie

== Plot ==
Sammy Smalls, a 21-year old tomboy, works as a grocery store detective at Big Apple Food Town. She tracks down shoplifters and transfers them to her boss Dirk, who beats up the shoplifters as punishment. Sammy dislikes her job, but she cant quit, as she would have to move back in with her overbearing parents.  Sammy meets and becomes infatuated with Eugene Zaslavsky, an equally quirky musician who performs outside her grocery store.  The two develop a friendship that culminates into a disastrous one-night stand.  Eugene, unimpressed by Sammy’s immaturity and sexual inexperience, suggests they maintain an open relationship.  Sammy concocts a plan to gain sexual experience to impress Eugene which leads her into many sexual misadventures. 

==Reception==
Upon its release in Canada, Year of the Carnivore has received polarized reviews.

In his review for  . ” Jam! Movies Jim Slotek rated Year of the Carnivore three out of five stars and wrote that “there’s a sweetness to The Year of the Carnivore that survives its flaws. It is not successful as a comedy in the sense of there being a lot of laughs. But it is an interesting and different character portrait.” 

==References==

 

== External links ==
*  
*  
*  
*  
*   at Twitch Film
*   at The Globe and Mail
*   at Exclaim!

 
 
 
 
 
 
 