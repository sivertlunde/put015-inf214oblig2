A Single Man (1929 film)
{{infobox film
| title          = A Single Man
| image          =File:A Single Man poster.jpg
| imagesize      =
| caption        =Film poster
| director       = Harry Beaumont
| producer       = Metro-Goldwyn-Mayer George OHara (script) Joseph Farnham (intertitles) Lucille Newmark (intertitles)
| starring       = Lew Cody Aileen Pringle
| music          =
| cinematography = André Barlatier ( )
| editing        = Ben Lewis
| distributor    = MGM
| released       = January 12, 1929
| runtime        = 7 reels
| country        = United States Silent (English intertitles)
}} lost   1929 MGM silent comedy film directed by Harry Beaumont and starring Lew Cody. It is based on a 1911 Broadway stage play by Hubert Henry Davies, A Single Man.   

==Cast==
*Lew Cody - Robin Worthington
*Aileen Pringle - Mary Hazeltine
*Marceline Day - Maggie
*Edward Nugent - Dickie
*Kathlyn Williams - Mrs. Cottrell
*Aileen Manning - Mrs. Farley

===Unbilled===
*Robert Bolder - unknown part
*Ruth Holly - unknown part
*Patsy Kelly - unknown part

==References==
 

==External links==
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 