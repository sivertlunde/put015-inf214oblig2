Alone in the Dark II (film)
{{Infobox film
| name           = Alone in the Dark II
| image          =
| alt            =
| caption        =
| director       = Michael Roesch Peter Scheerer Ari Taub
| based on       =  
| writer         = Michael Roesch Peter Scheerer 
| starring       = Rick Yune Rachel Specter Lance Henriksen
| music          = Jessica de Rooij
| cinematography = Zoran Popovic
| editing        = Joe Pascual
| studio         = HJB Filmproduktion Boll Kino Beteiligungs GmbH & Co. KG Brooklyn Independent Studios Hans J. Baer Filmproduktion
| distributor    = Splendid Film & WVG Medien GmbH  (Germany)  High Fliers Video Distribution  (UK)  Universal Studios Home Entertainment  (US) 
| released       =  
| runtime        = 87 minutes
| country        = United States Germany
| language       = English
| budget         = 
| gross          =
}} Alone in the Dark, although it features an entirely new cast and a story that is unrelated to the original film.

Alone in the Dark II was filmed in New York City and Los Angeles. It is loosely based on the Infogrames Alone in the Dark video game series.

==Plot==
Former witch-hunter Abner Lundberg (Lance Henriksen) is forced to come back to fight his old nemesis, a century-old dangerous witch out on the prowl again. This time, Lundberg joins forces with Edward Carnby (Rick Yune), who attempt to track down the dangerous witch Elisabeth Dexter (Allison Lange).

==Cast==
*Rick Yune as Edward Carnby
*Rachel Specter as Natalie
*Lance Henriksen as Abner Lundberg
*Bill Moseley as Dexter
*Ralf Möller as Boyle
*Danny Trejo as Perry
*Zack Ward as Xavier
*Natassia Malthe as Turner
*Jason Connery as Parker
*Michael Paré as Willson
*P. J. Soles as Martha
*Brooklyn Sudano as Sinclair
*Allison Lange as Witch/Elizabeth Dexter
*Peter Looney as Ward Dexter
*Lisette Bross as Old Witch
*Louise Griffiths as Witch Voice (voice)

==Release and reception==
The film was released in Germany on September 25, 2008, in the United Kingdom on July 27, 2009  and in the United States on January 26, 2010.   An American Blu-ray release is sold exclusively by Best Buy. 
 Alone in the Dark did not require a sequel. Critics hated the film. Fans hated the film. ... Thankfully though, Alone in the Dark II is actually a much better film than the first. Its a touch light on action, and the setting is far more limited in terms of production design, but the tone is a little more in keeping with the ideas of the game franchise. ... Look at Alone in the Dark II more as a reboot than a sequel and it plays OK." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 

 