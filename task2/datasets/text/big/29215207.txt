Payanam (film)
 
 
{{Infobox film
| name           = Gaganam / payanam
| image          = Payanam.jpeg
| caption        = Poster of Payanam and Gaganam
| director       = Radha Mohan
| producer       = Prakash Raj
| writer         = Radha Mohan
| starring       =  
| music          = Pravin Mani
| cinematography = K. V. Guhan
| editing        = Kishore Te.
| studio         = Duet Movies & Matinee Entertainment
| distributor    = AGS Entertainment
| released       =  
| runtime        = 1:55:07
| country        =   India 
| language       =  
}}
 flight hijacking Nagarjuna in Hindi as Mere Hindustan Ki Kasam which was released on 10 August 2013.  

==Plot==
On a flight from Chennai to Delhi five passengers retrieve weapons hidden in the toilet and use them to hijack the plane. An engine is damaged during the struggle with the pilots so they make an emergency landing at Tirupati Airport. Government authorities arrive. The hijackers demand  100 Crore (c. US $22,000,000), release of their imprisoned leader Yusuf Khan (Sricharan), and a fresh aircraft for their escape.
 commando operation, but officials fear risking passengers life, not trusting Raveendras team. The major is frustrated when the government vacillates and it seems that Yusuf Khan might be released. Inside the flight the passengers fall into groups discussing their past lives and religion. One of the terrorists becomes sentimentally attached to Afshana, a little girl returning to Karachi after her heart operation. The terrorist, however, is warned by his comrade to not become too attached as his main duty is to carry out the current tasks which include killing the passengers at the end no matter what. Another passenger frequently irritates the terrorists with bitter questions about their motives, in which he is supported by Jagdeesh, a retired army colonel. Time passes and tensions rise, until one of the passengers Praveen (a drug addict) attempts an escape but is shot down by a terrorist who warns the authorities to act soon.

Raveendras colleague Nawaz Khan (Bharath Reddy) discovers that the missing cleaner who prepared the toilet of the flight before its departure has been identified by Tamil Nadu Police. Ravi and Nawaz go to find the cleaner in the hope of getting information about the nature of weapons that were left for the hijackers. After a chase in Red Hills they capture the man, who confesses his involvement in the plot and also gives vague information about the size of the weapon that he placed in the toilet. Ravi and Nawaz deduce that it must be a plastic explosive.

On his return Ravi is told the government has decided to release Yusuf Khan. But Yusuf dies in an accident while being transported to the airport. Ravi asks the officials to be very confidential about this information so that they can plan an assault without the terrorists knowledge. However, a reporter disguises himself as an assistant to a senior police official, and so gets close to the aircraft. He records footage of the flight through his pen camera and overhears two officials talking about the accident of Yusuf Khan in the restroom. The terrorists get alarmed. Despite attempts to contain the news, the media leak it and one of the terrorists kills the passenger Subash (Kumaravel), seeking proof that Yusuf is alive, failing which one passenger will be killed every half-an-hour. Ravi later arranges for a scared civilian Ranganathan (Sricharan) to pose as Yusuf to make the terrorists believe that Yusuf is alive and getting treated at the hospital.

Meanwhile Ravi plans an operation to rescue the victims and christens it as Operation Garuda. Through a cleaning lady he secretly sends a mobile phone onto the plane in a food packet, so he can exchange information with Colonel Jagadeesh. Ravi plans to shoot the terrorists during their transit to a new flight by asking the passengers to bend down when they get a signal. The firing succeeds and four of the five terrorists are killed. The last terrorist is killed with help of the passengers Chandrakanth (Babloo Prithviraj) and Vinod (Rishi (actor)|Rishi). The plastic explosive is found in the girls bag. Ravi throws it away which explodes. The operation ends with the Prime Minister appreciating Ravi and the passengers heading to their destination.

The passengers happily take leave and farewell each other and agree to meet again. The terrorists names are revealed to be Yasin (the leader), Munna, Omar, Anwar, and Abdul.

==Cast==
* Nagarjuna Akkineni as Major Raveendra Bharath Reddy as Captain Nawaz Khan
* Prakash Raj as K. Viswanath I.A.S., Home Secretary
* Shankar Melkote as S. K. Sharma, Spl. Secretary - Internal Security
* Iqbal Yaqub as Yasin, leader of the terrorist gang Narayan as Praveen, the drug addict
* Poonam Kaur as Air Hostess Vimala Gupta
* Sana Khan as Sandhya Rishi as Vinod
* Sricharan as Yusuf Khan and Ranganathan
* Ravi Prakash as Flight Captain Girish
* Brahmanandam as Director Rajesh Kapur
* M. S. Bhaskar as Rev. Fr. Alphonse
* Manobala as Narayana Shastry
* Babloo Prithviraj as Shining Star Chandrakanth
* Thalaivasal Vijay as Col. Jagadeesh
* Elango Kumaravel as Subash
* Badava Gopi as Gopinath
* Mohan Ram as Venkatraman
* Jayashree as Dhivya
* Chaams as Balaji

==Production== flight hijacking. NSG commando.    Speaking about his role, Nagarjuna revealed that the film turned out to be "one of the easiest" he had worked in as he didnt have to prepare much for the role, since Radha Mohan and Prakash Raj had done "so much research", spoken to army officers and had "everything about the role on paper&nbsp;– how commandos behave and dress."  Nagarjuna, starring in his first direct Tamil film after 13 years, was part of an ensemble cast that featured around 45 known artists from both Tamil and Telugu industries, with the cast being retained for both versions.  Prakash Raj himself, who had also previously starred in all Radha Mohan films, was cast in a pivotal role. 
 1999 Kandahar RED camera, handled by cinematographer K. V. Guhan.  Pravin Mani was assigned as the film composer, who worked on the films background score. The film would feature only one solo song, which too was composed by Pravin and was written by Vairamuthus son Madhan Karky.  The trailer of the film was launched on 14 January 2011. 

==Soundtrack==
The film did not have an original soundtrack, a notable rarity in Tamil cinema. However, Madhan Karky had penned the lyrics for a promotional song, which was also the films only song. Pravin Mani scored the films background score.  According to director Radha Mohan, "We did not have songs in the film because there are no situations in the film which warranted songs. Hence we decided to do away with songs in Payanam." 

==Release==
The satellite rights of the film were sold to STAR Vijay.

===Reception===
The film opened to mainly positive reviews. The story and screenplay by Radhamohan were well appreciated. Rediff.com gave the film 3.5 out of 5 and stated that "The sequences are logical, even while allowing for dashes of humour, soul-searching and feel-good factors; the whole setting has a realistic feel that draws you in."  Nowrunning.com also gave 3.5 out of 5 and wrote "Payanam has come out as something new for Tamil cinema. Its worth watching."  Southdreamz expressed that "Payanam is a movie that can be watched for a different experience."  Indiaglitz also praised the film stating "Kudos to Payanam for a great successful journey."  Another review website gave 3.5 out of 5 stated "The movie has the necessary elements which can be called the directors touch."  National Award winning critic Baradwaj Rangan wrote "You could call Payanam a cross between a disaster movie (say, Airport) and a Mouli stage play. There’s such an air of bonhomous familiarity to those who grew up in the seventies and the eighties, reading Vikatan jokes about actresses named Kalasri."  Sify.com stated that "The films supporting cast is what makes it work. On the whole, Radha Mohan’s Payanam is an enjoyable ride."  Chennai Online praised the attempt for inserting comedy in the action sequences saying "A big plus is the comedy in the script with the various characters from all backgrounds from a star to an astrologer, all thrown together in the aircraft."  The Hindu noted that "The film grabs audience attention from the word go&nbsp;—youre worried about the plight of the passengers one minute, chuckling the very next at the wry humour, and before long, gripped by the suspense." admiring the art director stating "The aircraft and airport are apparently a set. Really? Art director Kathir, take a bow!" and further cited that "As the end credits roll, you only wish there had been a Major Raveendran who had his way in December 1999 at Amritsar!" 

In contrast, the film also received mixed and negative reviews. A critic from the Times of India gave 2.5 out of 5 and said that "Radhamohans Payanam would have been a more engaging watch only if the journey was a littler shorter."  Behindwoods gave it 2 out of 5 and said that "Although humour shares equal space with action and suspense, a slight swaying on the lighter side dilutes the serious effect a wee bit which could have been taken care of."  One India commented "The film appears a little nagging and boring because the audiences continue to expect a big action scenes but the film continues to roll without any such incident." 

===Box office===
The film collected   in Chennai in the first week and had an average opening. 

==See also== flight hijacking incident, but a different story.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 