Up in the Air (1940 film)
{{Infobox film
| name           = Up in the Air
| image_size     =
| image	=	Up in the Air FilmPoster.jpeg
| caption        =
| director       = Howard Bretherton
| producer       = Lindsley Parsons (producer)
| writer         = Edmond Kelso (original story and screenplay)
| narrator       =
| starring       =
| music          =
| cinematography = Fred Jackman Jr.
| editing        = Jack Ogilvie
| distributor    =
| released       = 1940
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}

Up in the Air is a 1940 American film directed by Howard Bretherton. A none-too-popular (nor good) radio singer, Rita Wilson is murdered while singing on the air in a radio studio. Radio page boy, Frankie Ryan, and his janitor pal, Jeff, solve the mystery for the none-too-sharp police.

== Plot summary ==

Frankie Ryan works as a page boy at a radio station located in Hollywood. His friend Jeff works in the same place, but as a porter. Their real dream is to perform as radio comedians on the air, with their own show. Unfortunately they havent convinced anyone about their great sense of humor yet. When they try to help the station receptionist, Anne Mason, by setting up a false audition for the position as singer, they are almost fired for their antics.

The station has financial problems related to their current moody singer Rita Wilson, and try to find a way to get rid of her. Their prayers are heard when Rita is shot and killed during a blackout from powerloss when she is rehearsing for a broadcast.

Police detectives Marty Phillips and Delaney arrive at the scene, and even though they havent found the murder weapon, they start suspecting a wannabe cowboy singer, Tex Barton, who tries to slip out the back door after the shooting. He was in the audience when Rita was rehearsing beforw the blackout.

Station producer Farrell is afraid of being suspected as well, since he had an argument with Rita not long before the shooting. He asks Frankie, who overheard the discussion, to not tell the police about it. As a sign of gratitude, Farrell promises to give Anne a real audition for the position as singer, which is empty since Rita is gone.

Frankie soon finds the weapon used to shoot Rita, hidden in a ventilator duct. It turns out the gun belongs to Tex, and has been used in a prior shooting by a woman named Gladys Wharton. When Frankie and Jeff audition for a comedy spot on air (with Frankie in blackface as a disguise), the police come looking for Tex. Later, Tex is found murdered in the office of the station owner.

Frankie and Jeff decide to do a little investigation of their own, and search Texs room to see if they can find anything. The only thing of interest is a picture of Anne, suggesting that her real name is Gladys. Anne is therefore suspected of the murder and arrested by the police. However, a while later she makes bail and is released.

Frankie discovers from a radio station in Cheyenne that the shooter Gladys Wharton was a blonde woman who fell for one of her superiors and left her husband Tex. Since Anne is a true brunette, Frankie concludes that Rita could be Gladys instead of Anne.

When all the station executives are gathered in one room by the police, one of them, Van Martin, pulls out a gun and confesses to both crimes. When Jeff enters the room unannounced, he accidentally knocks the gun out of Vans hand and the police can arrest him. 

== Cast ==
*Frankie Darro as Frankie Ryan
*Marjorie Reynolds as Anne Mason
*Mantan Moreland as Jeff Jefferson Gordon Jones as Tex Barton
*Lorna Gray as Singer Rita Wilson aka Gladys Wharton Tristram Coffin as Bob Farrell
*Clyde Dilson as Detective Lt. Marty Phillips
*Dick Elliott as B. J. Hastings, Station Owner John Holland as Sam Quigley
*Carleton Young as Band Leader Dick Stevens
*Alex Callam as Van Martin - Announcer
*Maxine Leslie as Stella - Switchboard Operator Ralph Peters as Detective Delaney
*Jack Mather as Tim Wallace - Comic Dennis Moore as Gag-Writer Pringle
*Phil Kramer as Gag-Writer

== Soundtrack ==
*Lorna Gray - "Doin the Conga" (Written by Johnny Lange, Lew Porter and Edward J. Kay)
*Marjorie  - "Doin the Conga" (Written by Johnny Lange, Lew Porter and Edward J. Kay)
*Marjorie Reynolds - "By the Looks of Things" (Written by Harry Tobias and Edward J. Kay)
*Marjorie Reynolds - "Somehow or Other" (Written by Harry Tobias and Edward J. Kay)
*Gordon Jones - "Bury Me Not on the Lone Prairie|Oh, Bury Me Not on the Lone Prairie"

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 