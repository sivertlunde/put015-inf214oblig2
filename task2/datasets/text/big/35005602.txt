VHS Kahloucha
 
{{Infobox film
| name           = VHS Kahloucha
| image          = 
| caption        = 
| director       = Nejib Belkhadi
| producer       = Propaganda Productions
| writer         = 
| starring       = 
| distributor    = 
| released       = 2006
| runtime        = 80 minutes
| country        = Tunisia
| language       = 
| budget         = 
| gross          = 
| screenplay     = Nejib Belkhadi
| cinematography = Chakib Essafi
| editing        = Badi Chouka
| music          = Neshez
}}

VHS Kahloucha is a Tunisian 2006 documentary film.

== Synopsis ==
Moncef Kahloucha, a great fan of 70s movies, and also a house painter, shoots hilarious feature films in VHS with the help of the inhabitants of Kazmet, a poor district in Sousse (Tunisia). He produces, directs and stars in his films which are an opportunity for the locals to get away from their dull lives and to experience unique moments, from preparation to the screening of the film in the local café. The camera follows Kahloucha shooting his latest production: Tarzan of the Arabs.

== Awards ==
* Damascus 2007
* FID Marseille 2007
* Vues d’Afrique 2007
* AFF Rotterdam 2007
* Dubai 2006

== References ==
 

 
 
 
 
 
 
 

 
 