Chesty Anderson, USN
{{Infobox film
| name           = Chesty Anderson, U.S. Navy
| image          =
| caption        =
| director       = Ed Forsyth
| producer       = Philip J. Hacker Paul Pompian
| writer         = H.F. Green Paul Pompian
| starring       = Shari Eubank Dorrie Thomson Rosanne Katon
| music          =
| cinematography = Henning Schellerup
| editing        =
| distributor    = Atlas Films
| released       =  
| runtime        = 88 min.
| country        = United States English
| budget         =
}} WAVE (Woman Accepted for Volunteer Emergency Service) in the United States Navy|U.S. Navy.

When one of her friends goes missing, Chesty and several fellow WAVEs go looking for her and end up in a world of senatorial corruption and Mafia intimidation.

==Cast==
* Shari Eubank as Chesty
* Dorrie Thomson as Tina
* Rosanne Katon as Cocoa
* Marcie Barkin as Pucker
* Constance Marie as Baby
* Fred Willard as Peter Linden
* George Cooper as Senator Dexter
* Frank Campanella as The Baron
* Timothy Agoglia Carey as Vincent The Terrible
* Phil Hoover as Michael
* Tim Wade as Ferret
* Scatman Crothers as Ben Benson
* Mel Carter as Sam Benson John Chandler as Dr. Cheech
* Betty McGuire as CPO
* Lynne Guthrie as Lt. Ambrose
* Brenda Fogarty as Brenda
* Joyce Mandel as Suzi
* Roy Applegate as Phil
* Stanley Brock as Dr. Finkle
* Dyanne Thorne as Nurse
* Uschi Digard as Barons Girlfriend #1
* Pat Parker as Barons Girlfriend #2
* Betty Thomas as Party Guest #1
* Deborah Harmon as Party Guest #2
* Adair Jameson as Party Guest #3
* Pam Rice as Party Guest 4

==External links==
*  
*  

 
 
 
 
 
 


 