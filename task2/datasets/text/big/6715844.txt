Kedi (2006 film)
 
{{Infobox film
| name = Kedi
| image = Kedi.jpg
| director = Jyothi Krishna
| writer = Jyothi Krishna Ileana Tamannaah Tamanna Ramesh Adithya Menon M. S. Baskar
| producer = A. M. Rathnam
| cinematography = A. T. Karun
| music = Yuvan Shankar Raja
| editing = Kola Bhaskar
| distributor =
| studio = Sri Surya Movies
| released =  
| runtime = 
| language = Tamil
| country = India
| budget =
}} Ileana and Telugu and released as Jadoo.

==Plot==

Raghu (Ravi Krishna) is a happy-go-lucky youngster, who is not serious about his studies or life. He has a great time in college playing pranks with fellow students. His dad (M.S.Baskar) dotes on him and so does his close friends (Suman Shetty & gang). A new girl Priyanka (Tamannaah) joins his class and she is the sister of Pukazhenthi (Atul Kulkarni) a powerful gangster turned Minister. Priyanka is born with the silver spoon and the college gets a face lift as her class room is fitted with air conditioner and the canteen gets a hip look.

Priyanka (Tammanah) even manages to change the Principal using her brother’s influence and she hates the sight of Raghu whom she considers a creep. Aarthi (Ileana) a maid’s daughter studies in the same class and she has a secret admiration towards Raghu. Seeing the way in which Priyanka treats him, Aarthi gives moral support and motivates Raghu to come above Priyanka who is an all-rounder and best student in the university. This infuriates Priyanka who slowly “wants” Raghu at any cost.Aarthi is blackmailed by a boy who takes her vulgar photos in a toilet and he saves her from it.Priyanka decides to separate them and creates photos with him .

Priyanka comes to know about Raghu love towards Aarthi and she tries everything possible to separate them. Pukazhenthi will go to any extreme costs to make Raghu marry his sister. he makes Arthi mad by first kidnapping her and he start electrifing  her, but she turns normal on seeing raghu but goes back as she sees raghu get shot.In the end Aarthi goes mental which enrages Raghu who vows revenge on Priyanka, he pretends to accept the marriage and marries Aarthi in the last minute. Raghu comes to know that Priyanka even murdered her brother in order to live with Raghu. The film ends with Priyanka killing herself and Raghu burying Aarthis chain at Priyankas grave.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Ravi Krishna || Raghu
|-
| Tamannaah Bhatia|Tamannaah || Priyanka
|-
| Ileana DCruz|Ileana || Aarthi
|-
| Atul Kulkarni || Pughazhenthy
|}

==Soundtrack==
{{Infobox album
| Name = Kedi
| Type = soundtrack 
| Artist = Yuvan Shankar Raja
| Cover = 
| Released =  
| Recorded = 2006 Feature film soundtrack 
| Length = 
| Label = An Ak Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Vallavan (2006)
| This album  = Kedi (2006) 
| Next album  = Thimiru (2006)
}}

After working with A. R. Rahman in his previous project Enakku 20 Unakku 18, Jyothi Krishna decided to work together with Yuvan Shankar Raja in this film. The soundtrack, released on 9 June 2006, consists of 8 tracks with lyrics written by Pa. Vijay, whilst director Perarasu and Kabilan wrote lyrics for each one song. Music Director and composer Deva (music director)|Devas brother Sabesh had sung one of the songs.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Lyricist!! Notes 
|-  1 || "Aadhivasi Naane" ||   ||
|- 2 || Ranjith & Chinmayi || 5:00 || Pa. Vijay ||
|- 3 || "KD Paiya" || Udit Narayan & Shreya Ghoshal || 4:43 || Pa. Vijay ||
|- 4 || "Kungumam Kalainthathe" || Unni Krishnan || 1:20 || Pa. Vijay ||
|- 5 || "College Life Da" || Sabesh || 3:42 || Pa. Vijay || 
|- 6 || "Chumma Chumma" || Sunitha Sarathy || 4:10 || Pa. Vijay ||
|- 7 || Karthik & Chinmayi || 4:14 || Kabilan ||
|- 8 || "Unna Petha Aatha" ||   ||
|}

==Cast==

 
 
 
 