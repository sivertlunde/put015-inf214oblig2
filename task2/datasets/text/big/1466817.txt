Men with Brooms
{{Infobox film
| name = Men with Brooms
| image = MenwithBroomsMP.jpg
| border = yes
| caption = Promotional poster
| director = Paul Gross
| producer = 
| writer = Paul Gross John Krizanc Paul Quarrington
| starring = Paul Gross Connor Price Leslie Nielsen Peter Outerbridge Kari Matchett Molly Parker Polly Shannon
| music =
| cinematography = Thom Best
| editing =
| distributor = Alliance Atlantis
| released = March 8, 2002
| runtime = 102 min.
| country = Canada English
| budget = $7.5 million  
| gross = $4.2 million http://www.boxofficemojo.com/movies/?id=menwithbrooms.htm 
}}
  2002 Cinema Canadian romantic comedy film, starring and directed by Paul Gross.  Centred on the sport of curling, the offbeat comedy tells the story of a reunited curling team from a small Canadian town as they work through their respective life issues and struggle to win the championship for the sake of their late coach.
 Brier champion trademark "spin-o-rama" shot.
 Men with Brooms debuted October 4, 2010 on CBC Television for the 2010-11 television season.

==Plot== codicil to rink he formerly coached be re-assembled, and enter a bonspiel to win the Golden Broom by placing a stone containing his ashes on the button.  The teams skip, Chris Cutter, had skipped town ten years ago over the shame of failing to call a burnt stone, abandoning his fiancée Julie Foley (Donalds daughter) at the altar, and throwing the teams stones into the lake.  Chris returns to Long Bay, where he convinces the former members of his team, Neil Bucyk, James Lennox, and Eddie Strombeck, to enter the competition for the Golden Broom.  While the rink practices for the Golden Broom tournament, Chris tries to make amends with Julie, which is complicated by his feelings for her younger sister Amy.  Neil deals with his resentment towards his wife, and unhappiness at running a funeral home inherited from his father in law.  Eddie deals with his low sperm count and dissatisfaction about being unable to father children.  James is working as a minor drug dealer, and tries to raise money to pay off a supplier to whom he is indebted.   

After losing a match to an extremely elderly rink, the team realises they need a coach to be prepared for the bonspiel.  Chris reconciles with his estranged father Gordon Cutter, so he will coach the team.  Gordon trains the team for the upcoming bonspiel.  In the first match of the bonspiel, the rink plays another rink, skipped by former Olympian Alexander Yount.  Chris again fails to call a burnt stone, demoralising himself, the rest of his rink, and his father.  Chris goes drinking at a bar, where Amy meets him and informs him she Julie have come to an understanding; Julie accepts that Chris and Amy love one another, and once Chris accepts it they can be together.  Julie, meanwhile, will be blasted off into space.  Chris goes to his mothers grave where he encounters his father; the two reconcile, and Gordon tells Chris to go be with Amy. 

Neil quits the rink, and is replaced by Gordon.  In the final match of the bonspiel, the rink once again meets Younts rink.  On the critical final shot, one of the sweepers burns the stone, noticed only by Chris.  In this instance, Chris calls the burn.  Yount allows Chris to retake the shot, and he successfully clears the house, leaving the stone containing Coach Foleys ashes on the button, as requested in his will. 

==Cast==
*Paul Gross as Chris Cutter
*Peter Outerbridge as James Lennox
*Jed Rees as Eddie Strombeck
*James Allodi as Neil Bucyk
*James B. Douglas as Donald Foley
*Leslie Nielsen as Gordon Cutter Barbara Gordon as Eva Foley
*Molly Parker as Amy Foley
*Connor Price as Brandon Foley
*Michelle Nolden as Julie Foley
*Polly Shannon as Joanne
*Jane Spidell as Lilly Strombeck
*Kari Matchett as Linda Bucyk
*Bob Bainborough as Greg Guinness
*Paul Savage (curler)|A. Paul Savage as Paul Savage
*Greg Bryk as Alexander "The Juggernaut" Yount
*Stan Coles as Minister
*Darryl Casselman as Ronnie
*Mike "Nug" Nahrgang as Nug McTeague
*Timm Zemanek as Marvin Fleiger
*George Buza as Stuckmore

==Production==

===Origins===
The film began from a discussion that Gross had with producer   about it, but it was too complicated with that number of characters. Besides, hockey is political.  Its hard to talk hockey in this country since we think of it as our game, but its largely owned by others."    Gross solved those problems by replacing hockey with curling.

In October 2000, Gross was still working on the script with Krizanc and another writer, Paul Quarrington. 

===Pre-production and filming=== Hamilton and Brampton curling rinks.

Alliance Atlantis invested $1.5 million or more in the films prints and advertising campaign, which included an eight-city, private jet tour for cast, the director, and producer Robert Lantos. http://www.playbackonline.ca/articles/magazine/20020318/bo.html?print=yes 

==Reception==
The film attracted $1.04 million in North American box office, opening on 215 screens in 207 theatres for a $5,024 per-screen average, the third highest among all North American releases for the three-day opening period starting March 8.   It ended up grossing over $4.2 million in Canada  http://www.playbackonline.ca/articles/magazine/20020902/lantos.html?print=yes &nbsp;– making it the top-grossing Canadian English film subsidized by Telefilm Canada between 1997 and 2002.   Released on 27 screens in the United States, it grossed $14 765. 
 grade of C-, calling the films cast "charming" but criticized the script for being "alternately overdetermined and touching, crass and sharply comic."   Reviewers for Jam! were split.   One called it a "perplexing example of promise unfulfilled, despite many charming moments...  romantic elements are light, like watery beer with the alcoholic kick removed.  And the comedy elements are often too crude and clumsy to do justice to the movies situations."  Another called it a "winning ensemble comedy that shows Canadians can put gentle laughs and equally gentle sentiments on the button, just as easily as their counterparts anywhere else in the world."  Hollywood trade paper Variety (magazine)|Variety called it a "wan romantic comedy" with "ineffective physical comedy in slightly crude Brit geezer vein...  a load of unneeded expletives." 

The film now has a cult following on DVD.  Many relish the gentle Canadian comedy with its wry look at its country. Many drinking games have evolved as a result. 

In September 2002, Lantos told Playback (magazine)|Playback that he and Gross had a sequel in development. 
 Canadian Comedy Pretty Funny Direction", and received two nominations at the 23rd Genie Awards, one for Molly Parkers performance and another for the screenplay. 

==Soundtrack and book== soundtrack album for the film was released, with songs by The Tragically Hip, Kathleen Edwards, The New Pornographers, and Our Lady Peace among others. A best-selling novelization  by Diane Baker-Mason (ISBN 1-55278-263-8) was also published in 2002.

Not included on the soundtrack is the opening theme, an arrangement by Jack Lenz of the Canadian folksong Land of the Silver Birch performed by Paul Gross, Jack Lenz and D Cameron. The songs lyrics and relevance remain the focus of debate for many fans.

== References ==
 

== External links ==
*  
*  
*  , an April 2002 Playback (magazine)|Playback interview with Thom Best, with details on the films cinematography

 
 
 
 
 
 
 
 
 
 
 
 
 