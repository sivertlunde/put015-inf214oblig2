Olympus Has Fallen
{{Infobox film
| name           = Olympus Has Fallen
| image          = Olympus Has Fallen poster.jpg
| image_size     = 220px
| caption        = Theatrical release poster
| director       = Antoine Fuqua
| producer       = {{Plainlist|
*  Gerard Butler
* Alan Siegel
* Mark Gill }}
| writer         = {{Plainlist|
* Creighton Rothenberger
* Katrin Benedikt }}
| starring       = {{Plainlist|
* Gerard Butler
* Aaron Eckhart
* Morgan Freeman
* Angela Bassett
* Robert Forster
* Cole Hauser
* Finley Jacobsen
* Ashley Judd
* Melissa Leo
* Dylan McDermott
* Radha Mitchell
* Rick Yune }} Trevor Morris
| cinematography = Conrad W. Hall
| editing        = John Refoua
| studio         = Millennium Films
| distributor    = FilmDistrict
| released       =  
| runtime        = 120 minutes   
| country        = United States
| language       = English Korean
| budget         = $70 million  
| gross          = $161 million 
}} action thriller guerrilla assault Secret Service agent Mike Bannings efforts to stop them.

Olympus Has Fallen was released on March 22, 2013, by FilmDistrict and received mixed critical reception but earned over $160 million against a $70 million production budget. Olympus Has Fallen is one of two films released in 2013 that deals with a terrorist attack on the White House, the other being White House Down. A sequel titled London Has Fallen is in production.

==Plot==
  Special Forces soldier, is the lead United States Secret Service|U.S. Secret Service agent assigned to head the U.S. Presidential Detail. He maintains a personal, friendly relationship with President Benjamin Asher (Eckhart), First Lady Margaret (Judd) and, especially, their son Connor (Jacobsen). During a Christmas evening drive from Camp David to a campaign fundraiser on a snowy Christmas nght, the car transporting the First Family crashes; Banning saves President Asher, but Margaret dies in the crash.
 Treasury Department Prime Minister White House Director of the Secret Service Lynne Jacobs (Bassett) that "Olympus has fallen", shortly before his death.
 reunify Korea. Seventh Fleet and United States Forces Korea|U.S. forces from the Korean Peninsula, removing American opposition from a North Korean invasion of South Korea. He also seeks to destroy the American nuclear weapons stockpile by detonating them in their respective silos across the country, turning the U.S. into an irradiated wasteland as revenge for the deaths of his family (his mother having been killed by an American landmine, and his father having been executed by the North Koreans). To accomplish this, he requires the access codes to a system in the bunker called Cerberus, which are held by three top government officials within the bunker, including the President. Asher orders the other two officials to reveal their codes to save their lives, certain that he will not give up his code.
 Speaker of Acting President. Army Chief General Edward SEAL assault on the White House. Kang deploys an advanced anti-aircraft gun system in his possession, called Hydra 6. Discovering this, Banning advises Trumbull and Clegg to abort the mission, but it proceeds and the new defense annihilates the assault force before Banning can stop it. Kang retaliates for the attempted infiltration by killing Vice President Charlie Rodriguez (Phil Austin) on the video feed to the Pentagon.

After Banning disables Kang’s communications, Kang tries to execute Secretary of Defense Ruth McMillan (Leo) outside the White House in front of the media, but Banning rescues her and takes out several more of Kangs men in the process. With Kangs forces dwindling, he fakes both his own death and Ashers by sacrificing several of his men and the remaining hostages. However, Banning believes that Kang has faked his death and will attempt to sneak away. Kang eventually cracks Asher’s code and activates Cerberus. As Kang attempts to escape with Asher, Banning kills the remaining terrorists, but President Asher is shot by Kang in the abdomen when Asher tries to fight him. Banning kills Kang by stabbing him in the head with a knife during a fight. Banning then disables Cerberus with the assistance of Trumbull, with only seconds to spare. During daybreak that day, Banning walks out with Asher and is received by the soldiers posted to await their arrival. After the events, the U.S. begins to heal from the attack, while Banning once again becomes head of the Presidential Detail. Then Banning, Jacobs, Clegg, and Connor observe President Asher as he addresses the public.

==Cast==
  Secret Service agent.  President Benjamin Asher    Speaker Allan Trumbull  Director of Secret Service.   
* Rick Yune as Kang Yeonsak, a North Korean terrorist mastermind disguised as a South Korean ministerial aide.  Secret Service agent now working for South Korean Prime Ministers private security detail.   
* Finley Jacobsen as Connor Asher, the son of Benjamin and Margaret Asher. Secretary of Defense Ruth McMillan   
* Radha Mitchell as Leah Banning, a nurse and Mikes wife.    US Army General Edward Army Chief of Staff.  Secret Service Special Agent-in-Charge.   
* Ashley Judd as Margaret Asher, the First Lady of the United States.    Vice President Charlie Rodriguez   Admiral Joe Chairman of the Joint Chiefs. Secret Service.
* Lance Broadway as Agent ONeil, a member of the United States Secret Service|Presidents security detail.   
* Tory Kittles as Agent Jones, a member of the United States Secret Service|Presidents security detail.   
* Sean OBryan as Ray Monroe, Deputy Director of the National Security Agency. South Korean Prime Minister Lee Tae-Woo
* Kevin Moon as Cho, Kangs henchman.
* Malana Lea as Lim, Kangs henchwoman who serves as the technical expert of the group. 
* Sam Medina as Yu, Kangs henchman.
 

==Production==
Olympus Has Fallen was directed by Antoine Fuqua based on a script by Creighton Rothenberger and Katrin Benedikt in their first screenwriting effort. The production company Millennium Films acquired the spec script in March 2012, and Gerard Butler was cast later in the month as the star.    The rest of the characters were cast throughout June and July.  In 2012, Millennium Films competed against Sony Pictures, which was producing White House Down (also about a takeover of the White House) to complete casting and to begin filming. 

Filming began in  , 25 March 2013.   For example, computers created nearly all of the opening sequence in which the First Lady is killed in a car accident, with chroma key greenscreen technology used to composite the actors into the computer-generated snowy scenery. 

For scenes where actors walked in or out of the White House, a first-floor façade and entrance were built; computers added the second floor, roof, and downtown D.C. cityscape.  Action scenes with the White House in the background were filmed in open fields and the White House and D.C. were added in post-production. 

==Score== Trevor Morris The Borgias.    The record was released on March 15, 2013 via Relativity Music Group label. The runtime is one hour, eight minutes, and 56 seconds.

==Release== The Heat, which was to open at the same time (its release was later pushed back to June 28). FilmDistrict distributed the film.  The film was released on DVD and Blu-ray Disc on August 13, 2013, in the U.S. 

===Box office===
At the end of the first weekend, the film earned $30.5 million, and exceeded Hollywood experts predictions by $7 million.       The film received an A− CinemaScore grade.    
 , it has made a total of over $161 million at the box-office worldwide, making it FilmDistricts highest grossing film to date.   

=== Critical response=== average rating of 5.4/10. The sites critical consensus reads, "Its far from original, but Olympus Has Fallen benefits from Antoine Fuquas tense direction and a strong performance from Gerard Butler—which might be just enough for action junkies". 
Metacritic assigns the film a weighted average score of 41 out of 100, based 31 critics, indicating "mixed or average reviews".  Critics widely compared Olympus Has Fallen with the Die Hard (film series)|Die Hard series for sharing the same style and momentum, with Richard Roeper calling it as "just too much of a pale Die Hard ripoff." He gave the film a C. 

David Edelstein was much more negative about the film. While praising Butlers role as a "solid" character, Edelstein criticized the script and the violence of the film, writing "Olympus Has Fallen is a disgusting piece of work, but it certainly hits its marks – it makes you sick with suspense." 

==Sequel==
  The Equalizer.  On May 1, 2014, it was announced Focus Features had acquired distribution rights to the sequel and will release it on October 2, 2015.  On August 18, 2014, it was announced that Charlie Countryman director Fredrik Bond would be taking over direction from Fuqua,  but Bond left the film on September 18, just six weeks before the shooting was set to begin.  However, on September 28, 2014, it was announced that Babak Najafi will take over direction of the sequel.  On October 10, 2014, it was announced that Jackie Earle Haley would be joining London Has Fallen as a Deputy Chief named Mason. 

Filming for the sequel began on October 24, 2014. 

==See also==
* White House Down (2013)
* Transfer of Power (1999)

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
   
   
 
   
 
 
 
 
 
 
 
 
 
 