Somewhere in Africa
{{Infobox film
| name           = Somewhere in Africa 
| image          = Somewhere_in_Africa_film.jpg
| caption        = Theatrical poster
| alt            = 
| genre          = Drama, Thriller
| director       = Frank Rajah Arase
| producer       = Kwame Boadu
| writer         = Frank Rajah Arase
| starring       =  
| cinematography = 
| editing        = 
| screenplay     = Pascal Amanfo
| studio         = Heroes Films Production   Raj Films
| distributor    = 
| released       =  
| budget         = 
| country        = Ghana   Nigeria
}}

Somewhere in Africa: The Cries of humanity is a 2011 Ghanaian Nigerian drama film directed by Frank Rajah Arase, starring Majid Michel, Martha Ankomah and Kofi Adjorlolo. It received 7 nominations at the 9th Africa Movie Academy Awards including categories: Best Actor in a Leading Role, Achievement in Soundtrack, Achievement in Visual effects and Achievement in Make-Up. Majid Michel was the recipient of the films only award.     

==Cast==
*Majid Michel  as General Yusuf Mombasa
*Majid Michel as Frank Leuma, Reverend Francis Jackson
*Martha Ankomah as Nivera
*Eddie Nartey   as Pascal
*Kofi Adjorlolo as General Olemba
*Roselyn Ngissah as Captain Rajile
*David Dontoh as President Gabiza

==Reception==
Nollywood Reinvented gave it a 43% rating, praised its soundtrack, cinematography and screenplay. 

==Awards==
It received 7 nominations at the 9th Africa Movie Academy Awards for categories: Achievement in Production Design, Achievement in Costume Design, Achievement in Make-Up, Achievement in Soundtrack, Achievement in Visual effects, Best Young/Promising Actor, and Best Actor in a Leading Role. Majid Michel was the recipient of the films only award.

==References==
 

 
 
 
 
 