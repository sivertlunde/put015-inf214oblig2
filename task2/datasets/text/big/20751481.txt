Kutty (2010 film)
 
 

{{Infobox film
| name           = Kutty
| image          = Kutty.jpg
| caption        = Kutty
| director       = Mithran Jawahar
| producer       = Raju Easwaran T. Muthuraj
| writer         =  
| based on       =  
| starring       =  
| music          = Devi Sri Prasad
| cinematography = Balasubramaniem
| editing        = Kola Bhaskar
| studio         = Gemini Film Circuit
| distributor    =
| released       =  
| runtime        = 152 minutes
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Telugu film Arya (2004 film)|Arya, has Gemini Film Circuit as the producers, whilst Devi Sri Prasad handled the music composition.    The film, which was under production for a year, was released on 14 January 2010 coinciding with the Tamil festival of Thai Pongal.

==Plot==
At the Kanyakumari beach, Geetha (Shriya Saran) looks at a lonely lying personal diary where she enjoys a poem written in it. She admires the poem by replying on the same page. While she and her friend admire the beauty of Sunrise, she accidentally loses one of her anklets (it falls in the sea). She sees a person jumping in after it and she perceives him to have died in the waters. The incident haunts her in her dream again and again and feels very guilty for it. In her college, she meets a rich, handsome boy Arjun (Sameer Dattani) who impresses her to win her love. He proposes her and threatens her to love him or else he would die by jumping from college roof top. Already feeling guilty for being cause of a persons death, not willing to let it happen again Geetha accepts his love and screams I love You in front of the entire college to Arjun which was witnessed by Kutty (Dhanush).

Kutty proposes his love to Geetha in front of Arjun, which shocks Geetha and annoys Arjun. Geetha turns down his request again and again and reminds him of her relationship with Arjun. But Kutty instead tells her that it is not an issue to him and tells her to continue loving Arjun and he would never stop loving her. Arjun is irritated by the acting of Kutty. He fights with him in order to bully him into stopping but instead was challenged by Kutty, that if Arjun is confident in his love no one would separate Geetha from him & may the best man win. Arjun finds no option but to accept in order to prove his confidence in his love (else accept that his love is weak). While Kutty pulls all kind of stunts to impress Geetha (including winning a rigged race in order to protect Geethas chastity from Arjun) but fails to make her "feel his love".

Arjun introduces Geetha to his father (Radha Ravi) expecting him to approve & bless their marriage but his father, seeing to further his political ambitions, engages a Ministers daughter (who is shown to be a spoilt girl of questionable character), for Arjun. So Arjun escapes with Geetha who is helped by Kutty. Kutty, Arjun, Geetha are chased by Arjuns fathers men but are unable to catch them. Meanwhile old wounds renew between Kutty and Arjun. After an intense (kung-fu) fight with the henchmen (where Arjun runs & Kutty fights) Arjun leaves Geetha with Kutty without saying anything to either of them. Kutty takes care of Geetha, which touches her. She soon realises the love he has for her and regrets not having met him before her commitment to Arjun. She tries telling the same to him, but Kutty thinking that Geetha is scolding him closes his ears and does not hear what she actually said to him (he does that a couple of times in the movie). Arjun arrives at the scene his father in tow, now all happy for their marriage. Geetha leaves with Arjun to marry him. On the wedding day of Arjun & Geetha, Kutty wanders here and there to look after wedding related works. His friends understand his internal pain and ask him to cry. While Geetha is on her way to the ceremony, Kutty stops her and openly expresses his pain of his love and how he is going to miss her and asks if his love did touch her at least once, without knowing that his love touched her already. (He then laughs & passes it off as a prank). Geetha does not answer him. Kuttys kids friends present Geetha a gift (from Kutty) in which she finds her lost anklet and the poem page where she wrote a reply while she was in Kanyakumari. Kutty was the person who jumped into the sea to take her anklet and was assumed dead, by Geetha. Geetha finally realises her love for Kutty, rejects her marriage with Arjun and goes to Kutty and accepts his love finally.

==Cast==
* Dhanush as Kutty, a believer in, and propagator of, one-sided love. He is an orphan, and is shown to be generous and caring, especially where his lover is concerned.
* Shriya Saran as Geethanjali, a college student who becomes the target of two mens affections- Arjuns and Kuttys.
* Sameer Dattani as Arjun Devanayagam, the college troublemaker, who is popular as his father is a minister. He goes to great lengths to get what he desires.
* Radha Ravi as Devanayagam, Arjuns father, a minister who thinks only of his political career. Srinath as Arjuns right-hand man.
* Vincent Asokan as Muthu
* Rajalakshmi as Geethas mother, who arrives to console her daughter. Aarthi as Dhanushs best friend, a tomboy by nature who dresses and acts, to a great extent, like a boy, such that she is treated as one.
* Mayilsamy as Paramasivam, Railway ticket checker, who provides a brief moment of humor during the train ride.
* Neelima Rani as Shruthi, Geethas best friend.
* Meghna Naidu in an item number. She is accompanied by dancers as she dances on the train with Kutty.

==Production== Telugu film Pongal festival on 14 January 2010.

==Soundtrack==
The soundtrack consisted primarily of recycled tunes from the original Telugu film by music director Devi Sri Prasad. The films soundtrack was released 23 December 2009. People who were present at the small function were A. Jawahar, Devi Sri Prasad and the producers . 
{{Infobox album
| Name       = Kutty
| Type       = Soundtrack
| Artist     = Devi Sri Prasad
| Cover      = 
| Released   =  
| Recorded   = 
| Genre      = Film soundtrack
| Label      = Think Music Gemini Audio
| Producer   =  
| Last album = Namo Venkatesa (2009)
| This album = Kutty (2009)
| Next album = Adhurs (2010)
}}
{{tracklist
| headline     = Tracklist
| extra_column = Singer(s)
| total_length = 
| title1       = Life, A Jaali Than
| extra1       = Devi Sri Prasad
| length1      = 
| title2       = Feel My Love (Solo)
| extra2       = Kay Kay
| length2      = 
| title3       = Nee Kadhalikkum Ponnu
| extra3       = Mukesh
| length3      = 
| title4       = Yaro En Nenjai
| extra4       = Sagar, Sumangali
| length4      = 
| title5       = Feel My Love
| extra5       = Kay Kay
| length5      = 
| title6       = Kannu Rendum
| extra6       = Mukesh, Priya Himesh
| length6      = 
}}

==Release==
===Critical reception===
Upon release, the film generally received mixed reviews. A reviewer from Rediff gave the film 2.5 out of 5, claiming that the film is a "fun ride" and despite seeming to be the "same old story" with a "predictable end", he adds, "youre glued to the screen because of the intriguing plot twists".    Sify describes the film as a "clean entertainer", citing that the film works "as it is a clean, honest, romantic musical thats worth a matinee for the family audiences", though it isnt "without hiccups" as it "lacks comedy" and could have needed some editing works in the second half.  Similarly a reviewer from Times of India also described the film as a "clean entertainer" and praised director Jawahar, citing that he "deserves a round of applause", while criticizing the narration that "appears like a lecture students term as ‘blade’" and the climax that "has happened in only a 1000 other movies".    Behindwoods gave the film 2 out of 5 and cites that the film is "definitely different from the regular commercial" and that "the director has tried hard to make a feel good romantic flick and has partially succeeded".   

===Box office===
The film opened in Malaysia at eleventh place in the box office. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 