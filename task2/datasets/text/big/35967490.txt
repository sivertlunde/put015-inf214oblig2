The Silence of Joan
{{Infobox film
| name           = The Silence of Joan
| image          = 
| caption        = 
| director       = Philippe Ramos
| producer       = Sophie Dulac   Michel Zana
| writer         = Philippe Ramos 
| narrator       = 
| starring       = Clémence Poésy Thierry Frémont Liam Cunningham   Mathieu Amalric
| music          = 
| editing        = Philippe Ramos
| cinematography = Philippe Ramos
| studio         = 
| distributor    = Sophie Dulac Distribution
| released       =  
| runtime        = 92 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
The Silence of Joan (  ) is a 2011 French historical film directed by Philippe Ramos and starring Clémence Poésy, Thierry Frémont and Liam Cunningham. The film appeared in the Directors Fortnight at the 2011 Cannes Film Festival.

==Plot==
The film depicts the period between Joan of Arcs capture and her execution in 1431.

==Cast==
* Clémence Poésy as Jeanne dArc
* Thierry Frémont as the healer
* Liam Cunningham as English Captain
* Mathieu Amalric as the preacher
* Louis-Do de Lencquesaing as Jean de Luxembourg
* Jean-François Stévenin as the monk
* Johan Leysen as the commander of the guard	
* Bernard Blancan as the carpenter
* Pierre Pellet as the jailer	
* Pauline Acquart as the carpenters daughter Christopher Craig as the seargent in charge of the convoy
* Kester Lovelace as the seargent in charge of the prison

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 

 
 