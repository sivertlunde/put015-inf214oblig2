Under the Skin of the City
 
{{Infobox film
| name           = Under the Skin of the City
| image          = 
| caption        = 
| director       = Rakhshan Bani-Etemad
| producer       = Rakhshan Bani-Etemad
| writer         = Rakhshan Bani-Etemad
| starring       = Golab Adineh
| music          = 
| cinematography = Hossein Jafarian
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Iran
| language       = Persian
| budget         = 
}}

Under the Skin of the City ( , also released as Under the Citys Skin) is a 2001 Iranian drama film directed by Rakhshan Bani-Etemad. It was entered into the 23rd Moscow International Film Festival where it won the Special Golden St.George.   

==Cast==
* Golab Adineh as Tuba
* Mohammad Reza Forutan as Abbas
* Baran Kosari as Mahboubeh
* Ebrahin Sheibani as Ali
* Mohsen Ghazi Moradi as Mahmoud the Father
* Mehraveh Sharifinia as Masoumeh
* Homeira Riazi as Hamideh
* Alireza Oosivand as Nasser Khan
* Mehrdad Falahatger as Marandi

==References==
 

==External links==
*  

 
 
 
 
 
 
 