The Blizzard (1923 film)
{{Infobox film
| name = The Blizzard
| image = Gunnar Hedes saga.jpg
| caption = Theatrical release poster
| director = Mauritz Stiller
| producer = Charles Magnusson
| screenplay = Mauritz Stiller Alma Söderhjelm
| based on =  
| starring = Einar Hanson Mary Johnson Pauline Brunius Stina Berg Hugo Björne
| cinematography = Julius Jaenzon
| editing = AB Svensk Filminspelning
| distributor = AB Svenska Biografteaterns Filmbyrå
| released =  
| runtime = 101 minutes
| country = Sweden Silent Swedish Swedish intertitles
| budget =
}}
The Blizzard  is a 1923 Swedish drama film directed by Mauritz Stiller, starring Einar Hanson, Mary Johnson, Pauline Brunius and Hugo Björne. The films original Swedish title is Gunnar Hedes saga, which means "The story of Gunnar Hede". The narrative revolves a student who tries to save his familys mansion which is facing bankruptcy. The film is loosely based on the Selma Lagerlöf novel The Tale of a Manor.

==Cast==
* Einar Hanson as Gunnar Hede
* Pauline Brunius as Mrs. Hede, Gunnars mother
* Hugo Björne as Mr. Hede, Gunnars father
* Mary Johnson as Ingrid
* Adolf Olschansky as Mr. Blomgren
* Stina Berg as Mrs. Blomgren
* Thecla Åhlander as Stava

==Production==
 , Mauritz Stiller, Mary Johnson, and Einar Hanson during the production of the film]]
AB Svensk Filmindustri (SF) had initially tried to make a film adaptation of Selma Lagerlöfs The Tale of a Manor in 1915, and Gustaf Molander had developed a screenplay. The film was however cancelled. In the early 1920s the plans were revived and the project was given to Mauritz Stiller, who is credited as writer together with the Finnish-Swedish author Alma Söderhjelm. The screenplay differs from the original story in several regards, and the opening titles call it a "free adaptation" of the novel. Just like Stiller had done when he made his previous Lagerlöf adaptation, Sir Arnes Treasure, and Victor Sjöström had done with his, Stiller travelled to Lagerlöf and presented the screenplay to have it approved. Lagerlöf was however deeply dissatisfied with the liberties Stiller had taken, and the production company had to convince her to not denounce the film publicly.    The film was produced through AB Svensk Filminspelning, a subsidiary of SF which only existed from 1922 to 1923. 

Lars Hanson was originally cast in the role of Gunnar Hede, but was replaced by Einar Hanson soon before production started. This was Einar Hansons first leading role in a film.  Principal photography took place between February and June 1922 in the Filmstaden studios. Exteriors were shot in the surrounding area, Nacka, and Kallsjön in Jämtland. 

==Release==
The film premiered on 1 January 1923.  The film was sold to 17 markets abroad, which was significantly fewer than Stillers five previous films. The Blizzard is partially lost; only about two thirds of the original film are still known to exist. 

==See also==
* 1923 in film
* Cinema of Sweden

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 