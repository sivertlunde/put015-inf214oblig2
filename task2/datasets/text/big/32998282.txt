Lovers & Leavers
{{Infobox film
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Aku Louhimies
| producer       = Jarkko Hentula Ilkka Y. L. Matila Marko Röhr
| writer         = Katja Kallio Aku Louhimies
| based on       =  
| starring       = Minna Haapkylä Anna-Leena Härkönen Laura Malmivaara Peter Franzén Matti Ristinen Pirkko Saisio
| music          = Leri Leskinen
| cinematography = Heikki Färm
| editing        = Samu Heikkilä
| studio         = Matila Röhr Productions
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = Finland
| language       = Finnish
| budget         = 
| gross          = 
}} Finnish romantic drama film directed by Aku Louhimies.     The film is written by Katja Kallio and Louhimies, based on Kallios novel Kuutamolla: Levoton tarina rakastamisesta.    Lovers & Leavers is about Iiris, a 30-year-old bookstore assistant, who meets the man of her dreams.  

== Cast ==
* Minna Haapkylä as Iiris Vaara
* Anna-Leena Härkönen as Anna
* Laura Malmivaara as Laura
* Peter Franzén as Marko
* Matti Ristinen as Sami
* Mikko Kouki as Jukka
* Pirkko Saisio as Leila Vaara
* Santeri Nuutinen as Santtu
* Rasmus Nuutinen as Tintti
* Veeti Kallio as Mikko
* Linda Zilliacus as Ilona
 

== Awards ==
;Cinequest San Jose Film Festival
 
|-
| 2003|| Lovers & Leavers || Best Feature ||  
|}

;Durango Film Festival
 
|-
| 2003|| Lovers & Leavers || Jury Award in category Best Narrative Feature - Drama ||  
|}

;Jussi Awards
 
|-
| 2003|| Lovers & Leavers || Best Actress (Paras naispääosa) Best Editing (Paras leikkaus) Best Film (Vuoden elokuva) Best Set Design (Paras lavastus) Best Supporting Actress (Paras naissivuosa) ||  
|}
 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 


 
 