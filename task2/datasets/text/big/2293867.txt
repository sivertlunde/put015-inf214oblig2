Queen Kelly
{{Infobox film
| name           = Queen Kelly
| image          = Queen Kelly video cover.jpg
| writer         = Erich von Stroheim Marion Ainslee (titles) Walter Byron Seena Owen Tully Marshall
| director       = Erich von Stroheim Edmund Goulding Richard Boleslawski Sam Wood Gloria Swanson Irving Thalberg (all uncredited except Stroheim)
| producer       = Erich von Stroheim Gloria Swanson
| music          = Adolf Tandler
| cinematography = Paul Ivano William H. Daniels Gordon Pollock Hal Mohr (uncredited) Gregg Toland (alternate ending)
| editing        = Viola Lawrence
| released       =  
| distributor    = United Artists| Kino International (Current)
| runtime        = 101 minutes
| country        = United States English intertitles
}} Walter Byron and Seena Owen. The film was produced by Joseph P. Kennedy, who was Swansons lover at the time.   

In 1932, Swanson was able to release in Europe and South America only,  because of her contract with Stroheim,  a part-talkie version. This version had an alternate ending directed by Swanson and filmed by cinematographer Gregg Toland. 

==Plot==
Prince Wolfram (Byron) is the betrothed of mad Queen Regina V of Kronberg (Owen). As punishment for partying with other women, he is sent on manoeuvres. He sees Kitty Kelly (Swanson) walking with other convent students and flirts with her. She is embarrassed when he makes a comment after seeing that her underwear is visible, so she takes it off and throws it at him, to the horror of the nuns, who punish her for her "indecency".

Enthralled by her beauty, he kidnaps her that night from the convent, takes her to his room and professes his love for her. When the Queen finds them together the next morning, she whips Kelly and throws her out of the castle. Regina then puts Wolfram in prison for not wanting to marry her.

===Endings=== madam of her aunts brothel. Her extravagances and style earn her the name "Queen Kelly".

Alternate ending: Kelly dies in despair after her humiliation at the hands of the Queen. Wolfram, contrite, visits her body.

==Cast==
*Gloria Swanson - Kitty Kelly, aka Queen Kelly  Walter Byron - Prince Wolfram 
*Seena Owen - Queen Regina V 
*Sylvia Ashton - Kellys Aunt 
*Wilson Benge - Prince Wolframs Valet 
*Sidney Bracey - Prince Wolframs Lackey 
*Rae Daggett - Coughdrops 
*Florence Gibson - Kellys Aunt 
*Madge Hunt - Mother Superior 
*Tully Marshall - Jan Vryheid  Ann Morgan - Maid Escorting Kelly to Altar  
*Madame Sul-Te-Wan - Kali Sana, Aunts Cook  
*Lucille Van Lent - Prince Wolframs Maid  
*Wilhelm von Brincken - Prince Wolframs adjutant 
*Gordon Westcott - Lackey

==Production==
  and Gloria Swanson]]
The production of the costly film was shut down after complaints by Swanson about the direction the film was taking. Though the European scenes were full of innuendo, and featured a philandering prince and a sex-crazed queen, the scenes set in Africa were grim and, Swanson felt, distasteful. In later interviews, Swanson had claimed that she had been misled by the script which referred to her character arriving in, and taking over, a dance hall; looking at the rushes, it was obvious the dance hall was actually a brothel.

Stroheim was fired from the film and the African storyline scrapped. Swanson and Kennedy still wanted to salvage the European material, as it had been so costly and time-consuming, and had potential market value. An alternate ending was, however, shot on November 24, 1931. In this ending, Kelly dies after her experiences with the prince (it is implied to be suicide). Prince Wolfram is shown visiting the palace. A nun leads him to the chapel, where Kellys body lies in state. These scenes were directed by Swanson herself, photographed by Gregg Toland and edited by Viola Lawrence.  This has been called the Swanson Ending.

==Distribution==
The film was not theatrically released in the United States, but it was shown in Europe and South America with the Swanson ending tacked on. This was due to a clause in Stroheims contract. 
 Poto Poto to MGM. Though it was never produced, the script contained several elements recycled from the African story of Queen Kelly.
 Sunset Boulevard (1950), representing an old silent picture Swansons character Norma Desmond - herself a silent movie star - had made. Von Stroheim is also a primary character in Sunset Boulevard, as her ex-director, ex-husband, and current butler.  By some accounts, Von Stroheim suggested the clip be used for its heavy irony.  This was the first time viewers in the US got to see any footage of the infamous collaboration.

In the 1960s, it was shown on television with the Swanson ending, along with a taped introduction and conclusion in which Swanson talked about the history of the project.
 Kino International had acquired the rights to the movie and restored two versions: one that uses still photos and subtitles in an attempt to wrap up the storyline, and the other the European "suicide ending" version. Kino remains the rights holder and is responsible for all distribution, including television and home video.

==See also==
*Sadie Thompson (1928)
*The Love of Sunya (1927)

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
   
 
   
 
 
 