Chal Mere Bhai
 
 
 
{{Infobox film
| name           = Chal Mere Bhai
| native title   = 
| image          = Chal Mere Bhai.jpg
| caption        = Luv is tough... so laugh a little
| director       = David Dhawan
| producer       = Nitin Manmohan
| writer         = Rumi Jaffrey (Dialogues)
| story          = Ikram Akhtar
| screenplay     = Ikram Akhtar Yunus Sejawal
| narrator       = 
| starring       = Sanjay Dutt Salman Khan Karisma Kapoor Sameer (Lyrics) Harmeet Singh
| editing        = A. Muthu
| studio         = 
| distributor    = Gaurav Digital Neha Arts
| released       =  
| runtime        = 136 min.
| budget         =
| country        = India
| language       = Hindi
| gross          =  
}}

Chal Mere Bhai ( ,  }}, translated: Come on, my brother) is a Bollywood comedy film released in 2000. The film is produced by Nitin Manmohan and directed by David Dhawan. It stars Sanjay Dutt, Salman Khan and Karisma Kapoor in the lead roles. This was Karismas fourth time in a row playing a character with the name Sapna. The film is inspired with popular Pakistani TV drama Ankahi (1982).

== Plot ==
This is the story of two brothers, Vicky (Sanjay Dutt) and Prem Oberoi (Salman Khan) and how their lives are turned upside down by a girl named Sapna (Karishma Kapoor).

Vicky is a business tycoon who runs his familys business bidding on multinational contracts.  When he needs a secretary he meets Sapna, a girl who has lost her family and has moved in with her uncle and auntie. Sapna doesnt have the required experience to be Vickys secretary, but Vickys father, Balraj Oberoi is impressed by Sapnas passion and hires her.

Prem is an aspiring actor, much to the chagrin of Balraj, who would like Prem to also work for the family business.  However, Prems grandmother and Vicky support Prems decision to be an actor.

Sapnas career as a secretary has many blunders:- At first—she is so nervous around Vicky she makes numerous mistakes.  But when Vicky is attacked after work, it is Sapnas fast thinking that saves his life.  Vickys family becomes fond of Sapna very quickly—Prem especially.

Prem and Sapna fall in love with each other, but before Prem can tell his family, Sapnas auntie and Prems grandmother arrange for Vicky and Sapna to be married. However, on the day before the marriage, Vicky finds out that Sapna loves Prem not him. Finally Prem and Sapna are united. 

== Cast ==
* Sanjay Dutt as Vicky Oberoi
* Salman Khan as Prem Oberoi
* Karishma Kapoor as Sapna Mehra
* Dalip Tahil as Balraj Oberoi
* Sushma Seth as Dadima, Vicky & Prems Grandmother
* Shakti Kapoor as Mamaji, Sapnas Uncle
* Ravi Baswani as Waiter
* Anil Dhawan as Doctor in hospital
* Birbal as Astrologer
* Himani Shivpuri as Mamiji, Sapnas Aunt Shahbaz Khan  as Rakesh
* Ghanshyam Rohera as Nandu, the cook Javed Khan as Murari
* Shashi Kiran as Suri
* Asrani as Family doctor
* Nagma as Priya in Guest Appearance 
* Sonali Bendre as Piya, Vickys beloved in Special Appearance
* Twinkle Khanna as Pooja, Special Appearance 
* Shankar Mahadevan as Himself in Title sequence Lesle Lewis as Himself in Title sequence
* Satish Kaushik
* Divya Palat
* Zeeshan
* Raju Shrestha
* Mangal Dhillon
* Dinesh Anand
* Shiva
* Suresh Bhagwat  

== Soundtrack ==
{{Infobox album |  
 Name = Chal Mere Bhai |
 Type = Soundtrack |
 Artist = Anand-Milind |
 Cover = |
 Released = 2000 |
 Recorded = | Feature film soundtrack |
 Length = |
 Label =  T-Series|
 Producer = Anand-Milind | 
 Reviews = |

 Last album = Shikaar   (2000) |
 This album = Chal Mere Bhai (2000) |
 Next album = Chhupa Rustam  (2000) |
}}

{{Track listing
| headline     = Songs
| extra_column = Playback Sameer
| all_music    = Anand-Milind
The music featured in the top 15 best selling albums of 2000. 

| title1   = Aaj Kal Ki Ladkiyan
| extra1   = Sonu Nigam, Vinod Rathod, Sushma Shrestha|Poornima, Vaijanti
| length1  = 6:35

| title2   = Chal Mere Bhai
| extra2   = Sanjay Dutt, Salman Khan
| length2  = 5:00

| title3   = Chal Mere Bhai
| note3    = Remix Lesle Lewis, Dominique
| length3  = 4:20

| title4   = Chori Chori Sapno Mein
| extra4   = Abhijeet, Alka Yagnik
| length4  = 5:03

| title5   = Chori Chori Sapno Mein
| note5    = Sad
| extra5   = Abhijeet, Alka Yagnik
| length5  = 1:08

| title6   = Mehndi Rang Layee
| extra6   = Udit Narayan, Sonu Nigam, Alka Yagnik, Jaspinder Narula
| length6  = 6:49

| title7   = Mere Baap Ki Beti
| extra7   = Abhijeet Bhattacharya|Abhijeet, Vinod Rathod
| length7  = 4:40

| title8   = Meri Neend Jaane Lagi Hai
| extra8   = Alka Yagnik, Sonu Nigam
| length8  = 5:30

| title9   = Thodi Si Beqarari
| extra9   =  Kumar Sanu & Alka Yagnik
| length9  = 4:39

| title10  = Theme Music
| note10   = Instrumental
| extra10  =  
| length10 = 3:23
}}

== References ==
 

== External links ==
* 

 

 
 
 
 
 
 
 