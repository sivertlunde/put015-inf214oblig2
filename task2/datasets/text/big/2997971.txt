Drumline (film)
{{Infobox film
| name           = Drumline
| image          = Drumlineposter2002.jpg
| caption        = Theatrical release poster
| director       = Charles Stone III
| producer       = Dallas Austin Timothy M. Bourne Wendy Finerman Jody Gerson Greg Mooradian
| writer         = Tina Gordon Chism Shawn Schepps
| starring       = Nick Cannon Zoe Saldana|Zoë Saldaña Orlando Jones Leonard Roberts John Powell
| cinematography = Shane Hurlbut
| editing        = Patricia Bowers Bill Pankow
| distributor    = 20th Century Fox
| released       =  
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $20 million
| gross          = $57.6 million 
}} historically black New York, played by Nick Cannon, who enters the fictional Atlanta A&T University and bumps heads with the leader of his new schools drum section. Leonard Roberts, Zoe Saldana, and Orlando Jones co-star.

The film received generally positive reviews, with most critics praising the energy of the film, as well as the musical talents of the bands.  It was a box office success, earning over $56 million in the U.S., and almost $1.2 million in foreign markets.  A television sequel,   premiered on VH1 on October 27, 2014. 

==Plot==
The film centers on Devon Miles (Nick Cannon|Cannon), a teen who has just graduated from a high school in New York City. Upon graduating, Devon heads to Atlanta, Georgia to attend the fictitious Atlanta A&T University, a historically black college that takes enormous pride in its marching band. Devon was personally invited to attend on full scholarship by Dr. James Lee (Orlando Jones|Jones), head of the band, for his prodigious talents. The A&T band separates itself from its competitors by requiring all members to read music, by focusing on various styles of music rather than what music is currently popular on the radio, and dedication to the teamwork emphasized "one band, one sound" concept. The band has a preseason that is similar to an athletic teams induction in that it is very physically and mentally difficult. It challenges all recruits to push themselves past what they previously thought were their limits. At the end of preseason, the musicians audition for spots on the field, and Devon is the only freshman to make P1, the highest level player. While going through his rigorous process, Devon also finds time to romance an upperclassman dancer, Laila (Zoe Saldana|Saldana).  The plot is loosely based on the real life events of the late southern drum phenom Andy Johnston.

College life starts well for Devon, as he has the girl and a spot on the field. Things begin to turn sour when Sean (Leonard Roberts|Roberts), Devons percussion leader, begins to grow weary of Devons cocky attitude. Sean later challenges Devon to take a solo in his first game, believing the freshman will panic and be embarrassed in front of everyone. He is shocked when Devon takes the solo and is subsequently humiliated. This sets up some tension in the drumline which is exacerbated when Dr. Lee is told by President Wagner (Afemo Omilami), the schools president, to change his focus from music to entertainment or lose his funding. Lee does not want to give Devon more playing time because he feels Devons attitude and respect are lacking. The situation further deteriorates when it is revealed that Devon cannot read music. Devon is demoted to P4 by Dr. Lee until he learns, then later put back on P1 when Wagner pressures Dr. Lee to do so. However, after inflaming a melee with a visiting band at A&Ts homecoming game after Devon plays on an opposing band members drum, Devon is finally kicked out of the band by Dr. Lee. The fight also harms his relationship with Laila as she is embarrassed to introduce him to her parents, who attended the game and thought of Devon as a Criminal|hoodlum.
 BET Big Southern Classic (a large competition of college bands), Devon realizes that his heart and honor are still with the A&T band. He rejects the scholarship offer from the rival band and returns to A&T. Though Devon is still not playing for the band, he cannot give up his drumming. He is sent cassette tapes from his estranged father and gets some ideas for new drum arrangements. He and Sean have a final confrontation that clears the air and they begin to work together. The two present their idea for an entrance cadence to Dr. Lee who decides they will be used during the Classic. Devon helps the drumline prepare and patches up his relationship with Laila. Lee also tells Devon that he can guarantee him a full return to the band next year.

At the Classic, the bands are shown performing a mixture of popular songs. Morris Browns band even gets rapper Petey Pablo to perform during their routine. A&T is not fazed by this and performs their mix of retro and current sounds. A tie results and the Morris Brown and A&T drumlines face off. Dr. Lee tells Devon he can play for this face-off, showing his faith in Devons improved character and in thanks for all the hard work he has done in getting the band ready for the Classic. Morris Brown goes first and A&T responds. Morris Browns second cadence includes their snares moving forward and playing on the A&T drums (the same move that incited the fight at A&Ts homecoming game, which resulted in Devons expulsion from the band), then throwing down their sticks. The A&T line manages to hold their composure in the face of the insult. They play their cadence and in the middle throw down their drumsticks, mimicking the Morris Brown actions, but then the entire line pulls out another set of sticks and continues playing. They end their routine in the faces of the Morris Brown drumline, but instead of playing on their drums, the line all drop their sticks onto the other drumlines drums. The judges award the win to A&T.

==Cast==
 
*Nick Cannon as Devon Miles
*Zoe Saldana as Laila
*Orlando Jones as Dr. James Lee
*Leonard Roberts as Sean Taylor GQ as Jayson Flore
*J. Anthony Brown as Mr. Wade
*Candace Carey as Diedre
*Von Coulter as Ray Miles
*Angela E. Gibbs as Dorothy Miles
*Brandon Hirsch as Buck Wild
*Afemo Omilami as President Wagner
*Earl C. Poitier as Charles
*Shay Roundtree as Big Rob
*Jason Weaver as Ernest
*Omar Dorsey as James
*Stuart Scott as himself
*Petey Pablo as himself
 

==Release==

===Home video=== Joe and "Blowin Me Up (With Her Love)" by JC Chasez from the films soundtrack.  A "special edition" DVD version of the film was later released on January 29, 2008.  The film was released in the Blu-ray Disc|Blu-ray format on January 27, 2009. 

==Critical reception==
The film was given 3½ stars at Allmovie, where reviewer Josh Ralske gave positive note to the performances of the main cast and Charles Stone III|Stones direction but still called the plot "formulaic."  At Metacritic, the film has averaged a 63 out of 100 rating from critics, based on 28 reviews.  The film aggregater website Rotten Tomatoes has an 82% "fresh" rating.  It is currently tied for the #98 spot on the sites list of 100 Best Films of 2002.  At Yahoo! Movies, the film has been given a B average based on 14 reviews from critics, and a B- by over 30,000 users. 
 Mike Clark at USA Today awarded the film two and a half of four stars, feeling the film to be conventional but competent, and giving particular positive note to J. Anthony Brown, Orlando Jones, and Leonard Roberts performances.  One of the films negative reviews came courtesy of David Levine at Christopher Null|FilmCritic.com. Giving the film 2.5 out of 5 stars, Levine called the screenplay "standard formula" and "predictable," and went on to say that it was unfunny as well as uninspiring. He did however say he was impressed by the precision and artistry of the marching band. 

===Awards and nominations===
{| class="wikitable"
|-
! Year
! Award 
! Category — Recipient(s)
|- 2003
|rowspan="2"|Black Black Reel Awards Best Breakthrough Performance, Viewers Choice — Nick Cannon (nominated)
|- Best Director (Theatrical) — Charles Stone III (nominated)
|- Motion Picture Sound Editors, USA Best Sound Lee Scott (nominated)
|-
|rowspan="2"|MTV Movie Awards Best Kiss — Nick Cannon, Zoe Saldana (nominated)
|- Breakthrough Male Performance — Nick Cannon (nominated)
|- NAACP Image Awards Outstanding Motion Picture (nominated)
|-
|Phoenix Film Critics Society Awards Overlooked Film of the Year (nominated)
|-
|rowspan="3"|Teen Choice Awards Teen Choice Choice Movie, Drama/Action Adventure (nominated)
|- Choice Movie Actor, Drama/Action Adventure — Nick Cannon (nominated)
|- Choice Movie Breakout Star, Male — Nick Cannon (nominated)
|}

==Soundtrack==
{{Infobox album   Name        = Drumline (soundtrack) Type        = soundtrack Artist      = Various Artists Cover       = Drumline Soundtrack Cover.jpg Released    = December 10, 2002 Genre  Hip hop, contemporary R&B|R&B Length      = 61:31 Label  Jive
|Producer Cliff Jones, John Powell
}}

The films soundtrack was also executive produced by Dallas Austin. Released December 10, 2002,  it reached the number 61 spot on Billboards Top R&B/Hip-Hop Albums chart, number 10 on the Top Soundtracks list, and peaked at 133 on The Billboard 200 in 2003.  The tracks "I Want a Girl Like You," "Blowin Me Up (With Her Love)," and "Club Banger" were all released as singles, with JC Chasez "Blowin Me Up..." obtaining the highest level of success, reaching the number 24 spot on the Canadian Singles Chart, number 17 on the Top 40 Tracks chart, and number 14 on the Top 40 Mainstream chart.  

{{tracklist
| headline        = Track listing
| extra_column    = Performer(s)
| title1          = Time
| note1           = 
| extra1          = A&T Drumline "The Senate"
| length1         = 0:28

| title2          = Been Away
| note2           = feat. Jermaine Dupri
| extra2          = Q The Kid
| length2         = 3:50

| title3          = I Want a Girl Like You *
| note3           = feat. Jadakiss Joe
| length3         = 3:59

| title4          = Blowin Me Up (With Her Love) *
| note4           = 
| extra4          = JC Chasez
| length4         = 4:50

| title5          = Club Banger *
| note5           = 
| extra5          = Petey Pablo
| length5         = 3:49

| title6          = Faithful to You
| note6           = 
| extra6          = Syleena Johnson
| length6         = 3:30

| title7          = Butterflyz (Krucialkeys Remix)
| note7           = 
| extra7          = Alicia Keys
| length7         = 4:12

| title8          = Uh Oh
| note8           =  Monica
| length8         = 3:39

| title9          = My Own Thing
| note9           = 
| extra9          = Raheem DeVaughn
| length9         = 3:58

| title10          = What You Waitin For
| note10           =  Nivea
| length10         = 3:35

| title11          = Peanuts
| note11           = 
| extra11          = Nappy Roots
| length11         = 4:35

| title12          = Im Scared of You
| note12           = 
| extra12          = Nick Cannon
| length12         = 4:00

| title13          = Shout It Out
| note13           = 
| extra13          = Too $hort & Bun B
| length13         = 4:47

| title14          = Lets Go
| note14           = feat. Deuce Poppi, Tre + 6 & Unda Presha
| extra14          = Trick Daddy
| length14         = 4:11

| title15          = Marching Band Medley
| note15           =  Bethune Cookman College Marching Band Grambling State University Tiger Marching Band, Clark Atlanta Marching Band, Morris Brown College & A&T
| length15         = 4:04

| title16          = Classic Drum Battle
| note16           =  Morris Brown College Drumline & A&T Drumline "The Senate"
| length16         = 4:04
}}
(*): Indicates songs were released as singles

==See also==
* Drumline Live, a theatrical production from the creators of the movie.
* Honda Battle of the Bands, a similar competition to the "Big Southern Classic" from the movie.
*  , a possible inspiration for Drumline featuring Pernell Briggs, Gerald Tharrington, Tim Bryant and Daryl Spellman.
* Historically black colleges and universities

==References==
 
  tags!-->

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 