Cookie's Fortune
 
 
{{Infobox film
| name           = Cookies Fortune
| image          = Cookies fortune.jpg
| caption        = Promotional film poster for the film
| alt            =
| director       = Robert Altman
| producer       = Willi Baer
| writer         = Anne Rapp
| starring       = Glenn Close Julianne Moore Liv Tyler Chris ODonnell Charles S. Dutton Patricia Neal
| music          = David A. Stewart
| cinematography = Toyomichi Kurita
| editing        = Abraham Lim
| studio         = New Films International
| distributor    = October Films
| released       =   
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $10 million
| gross          = $10.9 million
}}
 criminal comedy film directed by Robert Altman and starring an ensemble cast, including Glenn Close, Julianne Moore, Liv Tyler, Patricia Neal, Charles S. Dutton and Chris ODonnell.
 Holly Springs, Mississippi, where the film was mostly shot. It was entered into the 49th Berlin International Film Festival, held in February 1999.   

==Plot==
When a small Mississippi towns wealthy dowager Jewel-Mae "Cookie" Orcutt (Neal) tires of her widowed life, she decides to take one of her late-husband Bucks pistols from the gun cabinet and kill herself.

She is discovered by her pretentious playwright niece, Camille (Close), and Camilles eccentrically odd and adorably shy younger sister, Cora (Moore), who plot to set the suicide up as a murder to preserve the familys reputation and ensconce themselves in the family mansion.  The family of eccentrics is rounded out by Coras wayward outlaw of a daughter, Emma (Liv Tyler).  Emma has a love interest and inept jailer, Jason (ODonnell).

The key suspect is Willis (Dutton), Cookies handyman, who happens to have cleaned the guns the night before Cookies death.

What follows is a tale of how a shocking incident casts a ripple among a group of small-town oddballs.

==Production==
 
The screenplay was written by Anne Rapp and the film was produced by Willi Baer.

The film was shot on location in Holly Springs.

==Cast==
 
* Glenn Close as Camille Dixon
* Julianne Moore as Cora Duvall
* Liv Tyler as Emma Duvall
* Chris ODonnell as Jason Brown
* Ned Beatty as Lester Boyle
* Courtney B. Vance as Otis Tucker
* Charles S. Dutton as Willis Richland
* Patricia Neal as Jewel Mae "Cookie" Orcutt
* Donald Moffat as Jack Palmer
* Lyle Lovett as Manny Hood
* Danny Darst as Billy Cox
* Matt Malloy as Eddie "The Expert" Pitts
* Randle Mell as Patrick Freeman
* Niecy Nash as Wanda Carter
* Rufus Thomas as Theo Johnson
* Ruby Wilson as Josie Martin
* Preston Strobel as Ronnie Freeman
* Ann Whitfield as Mrs. Henderson / Herodias
 

==Reception==
 
The film received mostly positive reviews, with a fresh 86% on Rotten Tomatoes. 

==Soundtrack==
Music for the film is composed by David A. Stewart. The soundtrack album was released on April 2, 1999.    It features appearances by saxophonist Candy Dulfer.

# "Cookie" 
# "Wild Women Dont Get The Blues"
# "Helios"
# "Camillas Prayer"
# "The Cookie Jar"
# "Hey Josie"
# "All Im Sayin Is This"
# "A Good Man"
# "I Did Good Didnt I?"
# "A Golden Boat"
# "Im Comin Home"
# "Willis Is Innocent"
# "Patrol Car Blues"
# "Emma"
# "Humming Home"

All songs composed by Stewart, except "Cookie", "Camillas Prayer" and "Patrol Car Blues" composed by Dulfer and Stewart.   

==See also==
 
* 1999 in film
* List of comedy films of the 1990s
* List of crime films of the 1990s
 

==References==
 

==External links==
*  
*   at Rotten Tomatoes
*  
*  
*   Holly Springs, Mississippi

 

 
 
 
 
 
 
 
 
 