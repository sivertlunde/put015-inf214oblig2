Méditerranée (1963 film)
 Méditerranée is a 1963 French experimental film directed by Jean-Daniel Pollet and Volker Schlöndorff. It was written by Philippe Sollers and produced by Barbet Schroeder, with music by Antione Duhamel. The 45 minute film is cited as one of Pollets most influential films, which according to Jonathan Rosenbaum directly influenced Jean-Luc Goddards Contempt (film)|Contempt, released later the same year.  Footage for the film was shot around the Mediterranean, including at a Greek temple, a Sicilian garden, the sea, and also features a fisherman, a bullfighter, and a girl on an operating table.   

==References==
 

 
 
 
 


 