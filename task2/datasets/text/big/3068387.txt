Digna... hasta el último aliento
{{Infobox film
| name        = Digna... hasta el último aliento
| image       =
| caption     = Digna... hasta el último aliento movie poster
| writer      = Felipe Cazals
| starring    = Vanessa Bauche
| director    = Felipe Cazals
| producer    = Academia Mexicana de Derechos Humanos Grupo de Comunicación Publicorp Conaculta-IMCINE Luis Kelly Vicente Silva
| distributor =
| released    =  
| runtime     = 118 min.
| country     = Mexico
| language    = Spanish
| music       = Alejandro Rosso
| awards      = Ariel Award
| budget      =
}}
 Mexican film released in 2003.
 Guadalajara and Berlin Film Festivals. It won the Ariel Award in 2005 in the category of Best Feature Length Documentary ("Mejor Largometraje Documental") for Felipe Cazals. It was also nominated for the Ariel Award for the Best Actress (Vanessa Bauche).

==See also==
* List of Mexican films
* Human Rights in Mexico

==External links==
*   
*    with Felipe Cazals about this film
*  at the Spanish Film Festival in Manchester
*  at the San Diego Latino Film Festival
*  at the Chicago International Documentary Festival
*  at the Montreal Film Festival
* 

 
 
 
 
 
 
 
 
 
 


 
 