Ball Pen (film)
{{Infobox film
| name           = Ball Pen
| image          = Ball Pen kannada film poster.jpg
| caption        = Theatrical release poster
| director       = Shashikanth
| producer       = Bhavana Belagere Srinagar Kitty
| writer         = K.C. Manjunath
| screenplay     = Shashikanth
| starring       = Suchendra Prasad Master Skanda Master Shalom Raj Master Samarth
| music          = Manikanth Kadri
| cinematography = C.J. Rajkumar
| editing        = Shree (Crazy Minds)
| studio         = 
| released       =  
| country        = India
| language       = Kannada
| budget         = 
}} Kannada film directed by Shashikanth. The film produced by Srinagar Kitty and Bhavana Belagere is a childrens film which portrays the adventurous spirit, emotions in children and also focuses on child abuse.

==Cast==
* Suchendra Prasad
* Master Skanda
* Master Shalom Raj
* Master Samarth
* Srinagar Kitty ...Cameo

==Critical response== IBN Live gave the film a rating of 3.5/5 praising the performances of the children in the film. The role of the technical departments was appreciated too.  Supergoodmovies, which gave the film a rating of 3.5/6 commented "Ball Pen of course having an interesting tale that touches upon various facets. The interesting part is the innocent children minds working in right perspective." The role of the technical departments were also praised.  Rajendra Chintamani of Oneindia.in gave the film 3/5 rating and wrote, "The movie has a heart-touching tale, melodious tracks and eye-opening subject for the grown-ups" and concluded writing, "Though the movie, at places, has boring moments, the overall product makes you to ignore the flaws." 

==Soundtrack==
{{Infobox album 
| Name = Ball Pen
| Longtype = to Ball Pen
| Type = Soundtrack
| Artist = Manikanth Kadri
| Cover = 
| Border = 
| Alt = 
| Caption =
| Released =  
| Recorded =  Feature Film soundtrack
| Length =  Kannada
| Label = Bhavana Audio
| Producer = 
| Last album =Crazy Loka (2012)
| This album =Ball Pen (2012)
| Next album =Goondaism (2012)
}}
The music of the film was composed by Manikanth Kadri. The first song of the film, Saavira Kiranava Chelli sung by Aditya Rao, was released on YouTube received a good response. 

Tracklist
{| class="wikitable"
|-
! # !! Title !! Singer(s) !! Length
|-
| 1 || "Saavira Kiranava Chelli || Aditya Rao || 2:26
|-
| 2 || "Idu Yaara Bhuvi" || Bhoomika || 3:35
|-
| 3 || "Baleya Thotada Pakkada Kadali" || Samarth || 4:15
|-
| 4 || "Tiliyo Manava" || Aditya Rao || 1:52
|}

==References==
 

 
 
 


 