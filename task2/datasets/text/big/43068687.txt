Preethigaagi
{{Infobox film
| name           = Preethigaagi
| image          = 
| caption        = 
| director       = S. Mahendar
| producer       = Ramu Fazil
| screenplay     = S. Mahendar
| based on       =  
| starring       = Srimurali Sridevi Vijaykumar
| narrator       = 
| music          = S. A. Rajkumar
| cinematography = Shekhar Chandra
| editing        = P. R. Soundar Raj
| studio         = Ramu Films
| distributor    = 
| released       =  
| runtime        = 158 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Malayalam film, Hindu boy Christian girl, who relationship is not approved by their families, who eventually elope.     

==Cast==
 
* Srimurali
* Sridevi Vijaykumar Jayanthi
* Doddanna
* Mukhyamantri Chandru
* Avinash
* Shobhraj
* Srujan Lokesh
* Chithra Shenoy
* Preethi
* Amrutha
* Anand
* Nagashekar
* Ramakrishna
* Bhavya
* Bank Janardhan
* Honnavalli Krishna
* Prakash Shenoy
* Kempanna Mysore
* Ashwath Narayan 
* Sringeri Ramanna
* Sampath Nayak
* Ramkumar
* Baby Suma
* Baby Sushma
* Baby Kavyashree
* Master Shashank
 

==Soundtrack==
{{Infobox album
| Name        = Preethigaagi
| Type        = Soundtrack
| Artist      = S. A. Rajkumar
| Cover       = 2007 Kannada film Preethigaagi album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 15 September 2006
| Recorded    = 2006 Feature film soundtrack
| Length      = 34:53
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

S. A. Rajkumar composed the films background score and music for its soundtrack. The track "Ennai Thalatta" from the Tamil version was reused as "Kanninalli". The track "Ee Preethi" was reused from composers own Tamil track "Pirivondrai" from Piriyadha Varam Vendum (2001). The album consists of eight tracks. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 34:53
| lyrics_credits = yes
| title1 = Ananda Nammi Maneyalli (Patho)
| lyrics1 = K. Kalyan Rajkumar
| length1 = 1:18
| title2 = Ananda Nammi Maneyalli
| lyrics2 = K. Kalyan
| extra2 = Chithra, Prasanna, S. A. Rajkumar
| length2 = 4:14
| title3 = Ee Preeti Manasugalu
| lyrics3 = K. Kalyan
| extra3 = Chithra, Srinivas
| length3 = 5:04
| title4 = Kanninalli Kanaside
| lyrics4 = K. Kalyan Hariharan
| length4 = 5:03
| title5 = Midiyuthide
| lyrics5 = K. Kalyan
| extra5 = Prasanna, Priya, Aanchal
| length5 = 4:37
| title6 = Muddadthivi Guddadthivi
| lyrics6 = K. Kalyan
| extra6 = Shankar Mahadevan
| length6 = 4:47
| title7 = Preetiyalli Aralide
| lyrics7 = K. Kalyan
| extra7 = Harani
| length7 = 5:01
| title8 = Ellinda Bande Kaviraj
| extra8 = Hariharan, Priya
| length8 = 4:49
}}

==References==
 

 
 
 
 
 


 