The Chinese Bungalow (1926 film)
{{Infobox film
| name           = The Chinese Bungalow
| image          = 
| image_size     = 
| caption        = 
| director       = Sinclair Hill
| producer       = 
| writer         = James Corbett (play)   Marian Osmond (play)
| narrator       = 
| starring       = Matheson Lang   Genevieve Townsend   Juliette Compton   Shayle Gardner
| music          = 
| cinematography = Jack E. Cox
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures 
| released       = May 1926
| runtime        = 
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} British silent silent drama The Chinese Bungalow which was adapted for further films in 1930 and 1940.

It was made by Stoll Pictures, whose principal star throughout the 1920s was Lang.

==Cast==
* Matheson Lang ...  Yuan Sing
* Genevieve Townsend ...  Charlotte
* Juliette Compton ...  Sadie
* Shayle Gardner ...  Richard Marquess
* George Thirlwell ...  Harold Marquess
* Malcolm Tod ...  Vivian Dale
* Clifford McLaglen ...  Abdal
* George Butler ...  Chinese Servant
* Guy Mills ...  Chinese Servant

==References==
 

==Bibliography==
* Bamford, Kenton. Distorted Images: British National Identity and Film in the 1920s. I.B. Tauris, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 


 