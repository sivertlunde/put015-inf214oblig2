Up the Junction (film)
 
{{Infobox film
| name           = Up the Junction Peter Collinson
| image	         = Up_the_Junction_UK_quad.jpg quad film poster
| producer       = John Brabourne Anthony Havelock-Allan Roger Smith
| based on       =   
| starring       = Dennis Waterman Suzy Kendall Adrienne Posta Maureen Lipman Liz Fraser
| music          = Mike Hugg Manfred Mann
| studio         = BHE Films Crasto Paramount Film Service  
| released       =  
| country        = United Kingdom
| language       = English
}}
 1968 United British film Peter Collinson book of BBC TV adaptation in 1965, but returned to the original book. It did not cause a similar controversy or have as much impact. 

==Plot summary== Chelsea and moves to a working-class community in Battersea where she takes a job in a confectionery factory in an attempt to distance herself from her moneyed upbringing and make her own living. She becomes friends with two working-class sisters and forms a relationship with a working-class boy who envies her access to an easy life and is frustrated by her denial of it.  When Pollys friend becomes pregnant she decides to have an illegal abortion.  Tragedy hits when a motorcycle accident kills one of the friends.

==Main cast==
  
* Suzy Kendall as Polly 
* Dennis Waterman as Pete 
* Maureen Lipman as Sylvie 
* Adrienne Posta as Rube 
* Liz Fraser as Mrs. McCarthy 
* Linda Cole as Pauline 
* Doreen Herrington as Rita 
* Jessie Robins as Lil 
* Barbara Archer as May 
* Ruby Head as Edith  Susan George as Joyce  Sandra Williams as Sheilah 
* Michael Robbins as Figgins 
* Michael Gothard as Terry  Billy Murray as Ray  Michael Standing as John 
* Alfie Bass as Charlie 
* Aubrey Morris as Creely 
* Hylda Baker as Winnie 
* Shaun Curry as Ted 
* Olwen Griffiths as Fat Lil
* Queenie Watts as Mrs. Hardy 
* Lockwood West as Magistrate 
* Michael Barrington as Barrister 
* Yvonne Manners as Hotel Receptionist  Harry Hutchinson as Hotel Porter 
* Larry Martyn as Barrow Boy  Derek Ware as Teds Friend Mike Reid as Policeman outside courtroom (uncredited)
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 
 
 

 