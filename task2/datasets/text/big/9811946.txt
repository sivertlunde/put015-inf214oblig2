Dead Man's Eyes
{{Infobox film
| name = Dead Mans Eyes
| image = Dead Mans Eyes.jpg
| caption = 
| director = Reginald Le Borg
| producer = Will Cowan
| writer =Dwight V. Babcock
| starring =Lon Chaney, Jr. Acquanetta Jean Parker 
| music = Paul Sawtell
| cinematography = Paul Ivano
| editing = Milton Carruth
| distributor = Universal Pictures 
| released =  
| runtime = 64 minutes
| country = United States
| language = English
| budget = 
}} Inner Sanctum popular radio series and all of the films star Lon Chaney, Jr.

==Plot==
Artist Dave Stuart is blinded by a jealous assistant. The father of his fiance offers an operation to restore his sight, but Stuart will have to wait until the man dies. The benefactor dies a premature death and Stuart becomes a suspect.

==Cast==
* Lon Chaney, Jr. as David Dave Stuart (as Lon Chaney)
* Acquanetta as Tanya Czoraki
* Jean Parker as Heather Brat Hayden Paul Kelly as Dr. Alan Bittaker
* Thomas Gomez as Capt. Drury
* Jonathan Hale as Dr. Sam Welles
* Edward Fielding as Dr. Stanley Dad Hayden
* George Meeker as Nick Phillips
* Pierre Watkin as The Lawyer

== External links ==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 


 