Escape to Athena
{{Infobox film
| name           = Escape to Athena
| image          = Escape To Athena.jpg
| caption        = Escape to Athena release poster George P. Cosmatos
| producer       = Lew Grade David Niven Jr. Jack Wiener
| writer         = George P. Cosmatos (story) Edward Anhalt (screenplay) Richard Lochte (screenplay & story)
| starring       = Roger Moore Telly Savalas David Niven Stefanie Powers Claudia Cardinale Richard Roundtree Sonny Bono and Elliott Gould
| music          = Barry Blue & Rod Temperton (song, "Keep Tomorrow For Me") Lalo Schifrin Gil Taylor
| editing        = 
| distributor    = Associated Film Distribution
| released       = 6 June 1979
| runtime        = 125 min. (Sweden: 119 min.) (Argentina: 120 min.) (United States: 101 minutes)
| country        = UK
| language       = English
| budget         = 
| 
}}
Escape to Athena is a British adventure war film (with several elements of comedy) released in 1979, directed and co-authored by George Pan Cosmatos and produced by Lew Grades ITC Entertainment. The international cast included many well-known actors of the 1970s, including Roger Moore, Telly Savalas and Elliott Gould.
 Second World Greek island. According to the film credits, it was filmed on the island of Rhodes.

==Plot == POW camp are forced to work for the Germans by digging up ancient Greek archaeological artifacts. The camp Commandant, Major Otto Hecht (Roger Moore), was an Austrian antiques dealer before the war. Unknown to Hechts superiors is the fact that hes withholding the more valuable finds for his own personal gain, and sending them to his sister living in Switzerland. Hecht is far from the dutiful army officer, the total opposite of the nearest towns Commandant, SS Major Volkmann (Anthony Valentine). The prisoners conspire as well.  Knowing that they would be sent to another prison camp and endure worse hardships should they run out of artifacts, they repeatedly "discover" the same ones.  Volkmann and his Lieutenants (Siegfried Rauch and Richard Wren) rule the towns population with a sadistic grip, executing the innocent residents just as easily as enemy soldiers. 

The only opposition to the Germans is Zeno (Telly Savalas), a former Monk, and his few resistance fighters who use the local brothel as an undercover headquarters. Zeno, who is in contact with Allied Headquarters, is ordered to break the prisoners out of their camp to increase his numbers and therefore liberate the town from the Germans and also secure a U-Boat refuelling depot. Using two captured USO artists Charlie (Elliott Gould) and Dottie (Stefanie Powers) to perform a concert as cover, the prisoners and Zenos resistance take over the camp. With the choice of helping the resistance or being killed by Zeno, Hecht joins forces with the allies and helps them eradicate Volkmanns troops as well as capturing the fuel depot.

With Volkmann dead and the Germans in town no longer a threat, Charlie asks Zeno to lead him and two other prisoners, Judson and Rotelli (Roundtree and Bono) up to the monastery on Mount Athena to steal Byzantine treasures being kept by the monks. As with Charlie, the others care less about the war than the opportunities for looting.  Zeno warns Charlie that the Monks treasures are really the property of the Greek people.  

However, Zeno soon receives word from the Allies that the invasion of the Greek islands has been moved up, requiring that he neutralize a German garrison housed in the monastery atop Mount Athena.  Without telling them the whole truth, Zeno now tells Charlie, Rotelli and Judson that they can now climb Mount Athena to liberate the monks there from a small band of Germans.  Zenos turnabout - and the weapons they will be carrying - arouse some suspicion among the Americans, but ultimately their greed gets the better of them and they join Zeno.   

Once they reach the Monastery, the Americans find a heavily manned garrison armed with V-2 rockets.  Zeno uses gas to neutralize much of the garrison, but not before the garrisons commander has been alerted of their presence, and orders the launching of his rockets.  Observing the V-2 being readied for launch, the Americans realize that Zeno had lied to them about the mission.  Instead of hunting for treasure, they were sent to the monastery to destroy missiles the Germans were preparing to launch against the invading allies.  Judson knocks out the missile control room using grenades, but one of the Germans survives long enough to set the bases self-destruct mechanism.  Not realizing the danger immediately, Charlie and Rotelli scour the monastery for the monks treasure while Judson frees the monks.  Zeno finds the self-destruct clock, but cannot deactivate it.

With Zeno and the monks, the Americans escape the monastery before it explodes.  Searching for treasure up until the last minute, Charlie escapes the explosion with the only treasure the Germans left behind - tin plates adorned with Hitlers face.

During the victory celebration in the village, Hecht, Charlie, and Dottie make plans to capitalize on treasures Hecht has already looted - making copies to sell to the Americans.  Professor Blake (David Niven) learns from one of the freed monks that their treasure - Byzantine plates made of gold - are safe, having been hidden in the brothel the entire time.
 Heatwave song Keep Tomorrow for Me.

==Cast==
*Roger Moore as Major Otto Hecht &ndash; an Austrian and the Wehrmacht commandant of the POW camp who opposes his governments ideology
*Telly Savalas as Zeno &ndash; the head of the Greek Islands resistance movement
*David Niven as Professor Blake &ndash; Senior British Officer amongst the prisoners and a well known archaeologist
*Stefanie Powers as Dottie Del Mar &ndash; an American USO artist (in fact, stripper), who was shot down with Charlie and detained in the POW camp.
*Elliott Gould as Charlie &ndash; an American comedian, USO performer and professional partner of Dottie.
*Claudia Cardinale as Eleana &ndash; a local madame, girlfriend of Zeno
*Richard Roundtree as Sgt. Nat Judson &ndash; African-American POW and amateur magician
*Sonny Bono as Bruno Rotelli &ndash; an Italian POW, professional chef
*Anthony Valentine as Maj. Volkmann &ndash; the ruthless SS officer, town commandant and Hechts rival
* Siegfried Rauch as Lt. Braun &ndash; SS officer under Volkmanns command Richard Wren as Capt. Reistoffer &ndash; Volkmanns adjutant NCO
* Philip Locke as Maj. Vogel
* Steve Ubels as Capt. Lantz
* Paul Picerni as Zenos Man
* Paul Stassino as Zenos Man

===Cameo===
William Holden, who was in a relationship with Stefanie Powers at the time of filming, makes a cameo in the film as a POW. Elliott Goulds character passes by Holden leaning against the POW barracks, looks at him and asks, "Are you still here?" This is in reference to Holdens Oscar-winning performance in Billy Wilders World War II/POW film Stalag 17 as Sgt. J.J. Sefton, who actually escapes at the end of that film.

==Reception==
The film was partly financed by Lew Grade who wanted an action movie. However he felt it did not live up to the script, in part because the first eighty minutes mixed comedy and action and "the combination just didnt work... but the last forty, action-packed minutes were wonderful. If only the emphasis had been on action throughout the film would have been a hit. Unfortunately it wasnt. Still, with the pre-sales Id made we didnt lose nearly as much as we might have." Lew Grade, Still Dancing: My Story, William Collins & Sons 1987 p 250-251 
==See also==
*Kellys Heroes (1970) Inside Out (1975)

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 