1971 (2007 film)
{{Infobox film
| name           = 1971
| image          = 1971 film poster.jpg
| image_size     =
| caption        =
| director       = Amrit Sagar
| producer       = Amrit Sagar Moti Sagar
| writer         = Piyush Mishra  A
| starring       = Manoj Bajpai Ravi Kishan Chitaranjan Giri  Kumud Mishra Manav Kaul Deepak Dobriyal Piyush Mishra Vivek Mishra
| music          = Akash Sagar
| lyrics         =
| cinematography =  Chirantan Das
| editing        = Shyam K. Salgonkar
| distributor    =
| released       =  
| runtime        = 160 minutes
| country        = India
| language       = Hindi
}} Hindi war war drama National Film Awards, it won the National Film Award for Best Feature Film in Hindi 
The film is an account of the escape of six soldiers of the Indian Army taken as prisoners of war by Pakistan Army, during the Indo-Pakistani War of 1971.

== Special Mention ==

1971 was included in Avijit Ghoshs book, 40 Retakes: Bollywood Classics You May Have Missed


==Plot summary==
 

===The Camp===
The story takes place in Pakistan in 1977, six years after the 1971 Indo-Pakistan War. The scene opens to show a POW camp at an undisclosed location in Pakistan. Indian soldiers are holding a morning parade. As the Pakistanis Major Karamat and Colonel Sheriar Khan walk by, the Indians turn their backs on them in a gesture of disrespect. The cause for this disrespect is soon known as we see the Indian Major Suraj Singh (Manoj Bajpai) being called out of barrack Number 6 for interrogation by the Pakistanis. He is from the 18 Rajputana Rifles and was caught in the Uri sector in December 1971 as he ran out of ammunition. Before being brought to this camp, he was held in Kot Lakhpat, Lahore, where he was punished twice for attempting to escape. Now, he was put in barrack Number 6 as punishment for a third attempt. Colonel Sheriar Khan taunts him as to the condition of his fellow POWs in the barracks and asks him who else aided him in his escape attempt. Major Suraj Singh refuses to reveal his accomplices, and is sent back to barrack Number 6 for a few more days. He has already spent three days there.

We see some of the other POWs; Captain Kabir (Kumud Mishra), Captain Jacob (Ravi Kishan) and Pali discussing the camp in general. They wonder why the Indians, who were so far held in various jails all over Pakistan, have been brought to this camp. They take note of the fact that the camp is well-facilitated and that they are receiving good treatment as POWs. We also see Karamat and Khan discussing the inmates of barrack Number 6. The inmates are Indian POWs from the 1965 and 1971 wars who have lost their sanity. This is Major Suraj Singhs punishment; to be imprisoned in this part of the POW camp. Out of magnanimityColonel Sheriar Khan orders that Suraj be released the next morning.

===Arrival Of More POWs===
The next morning, an army truck is driving towards the camp.  This truck has a few more Indian POWs.  This group includes Flight Lieutenant Ram (Manav Kaul), Flight Lieutenant Gurtu (Deepak Dobriyal) and Colonel Puri.  The Flight Lieutenants, being youngsters, are of a bright disposition, and look for something or the other to be cheerful about despite their being captive.  When the truck stops at a side road and the POWs are allowed to relieve themselves, Ram and Gurtu contemplate the idea of jumping into the valley and running away.  Then, they give up the idea, as they are too weak to do so.  For consolation, they steal a guards wallet and get a Pakistani army ID card.

They reach the camp and are introduced to the other inmates already present. No one has any idea as to the reason they were taken there.

===Hope Against Hope===
Major Suraj Singh and his men, especially Captain Kabir, Captain Jacob and Subedar Ahmed, keep their own spirits alive by caring for the broken souls in barrack Number 6, and by remembering their own kin back home.  No matter how good the facilities in the camp, they are still prisoners and they hope to return home someday.

By asking the guards a few innocuous questions and putting their answers together, the POWs realize that they are in a place less than 200&nbsp;km from the Indo-Pak border.  (The place, it is revealed later, is Chaklala.) When Colonel Puri is told of this and the idea of an escape is put forward, he overrules it.  His reasons are that perhaps they will finally be repatriated and that a failed attempt could result in all of them being killed.

===The Reason===
A jail in Multan, Pakistan, is being examined by a joint delegation of the International Red Cross Society and the Pakistan Human Rights Commission.  They are accompanied by some Indian ladies who claim that their kin are held prisoner.  Their proof is that letters written by their missing kin bear the addresses of Pakistani jails.  The women are being shown around by Colonel Shakoor, and they are distraught to see no trace of their loved ones here.  The head of the Human Rights Commission, Sabeena Jahangir, signs a document stating that they are satisfied with the results of the search.

Here we understand the reason why the Indian POWs were brought to Chaklala: The Pakistani military/government bowed to international pressure and allowed the delegation to examine the jails.  The POWs has to be hidden away in a secret camp for the duration of the delegations visit in Pakistan.

===The Repatriation And The Realization===
Colonel Shakoor arrives at the Chaklala camp; he informs Colonel Puri and Major Singh that all the POWs will be repatriated.  He also says that as there are some formalities to be fulfilled, they will have to remain there for nearly two months.  The POWs are further informed that the same night they will be treated to an open-air screening of a recent popular movie followed by a banquet. Major Singh, Captain Kabir and Subedar Ahmed are distrustful of Colonel Shakoors words, although the other prisoners celebrate their imminent return home.

That night, while the movie is being screened, Ahmed steals a newspaper from an army jeep and calls Major Singh and Captains Kabir and Jacob into the barracks.  From reading the newspaper, they learn that General Zia Ul-Haq has overthrown Zulfikar Ali Bhutto in a military coup and formed a new government.  Further, General Zia has stated that all Indian soldiers who were taken prisoner so far have been returned and, as proof, he has allowed the International Red Cross Society to inspect the Pakistani jails.  Since no POW was found, General Zia has reasserted the Pakistani governments innocence in this matter.  The four soldiers now understand the reason why they were taken to this camp.  Also, that they were being provided with good facilities so that they would not think of escaping.  Then, they realize that they are being overheard and discover that the eavesdroppers are Ram and Gurtu.  They overpower them and are about to thrash the two, when the Flight Lieutenants inform them that they too wish to escape to India. They prove their willingness by producing the ID card they had stolen earlier.

===The First Steps===
The six soldiers decide to escape, and they must waste no time.  They take two steps the same night. First, they request the Pakistanis for a group photo.  This request is granted. Second, at the dinner, Ahmed attempts to strike Jacob, for which he is punished by Colonel Puri the next morning.  When Ahmed refuses to bear out his punishment, Colonel Puri wishes to court-martial him. The POWs ask for the use of a barrack to serve as a courtroom, a request that is also granted.

The Pakistanis are preparing to celebrate the Pakistani Independence Day (14 August) by having a song performance by Ms. Sultana Khanum, a ghazal singer.  The Indians, who wish to celebrate the Indian Independence Day (15 August), ask for fresh uniforms, paints to make an Indian flag and jaggery for making a sweet drink.  The Pakistanis grant them all their requests in order to keep them pacified.

While the court martial is going on, Ram and Gurtu combine the paints to dye the uniforms in the colours of the Pakistani Army.  Maj. Karamat is presiding over the court martial.  Here, with Kabir as the prosecutor and Suraj as the defense, the would-be escapees impress upon Karamat that Puri hates Ahmed simply because he is a Muslim.  This prompts Karamat into starting the proceedings to implant Ahmed as a spy in the Indian Army.

===Initial Success===
While Karamat is interviewing Ahmed to offer sending him back to India so that he can spy for the Pakistanis, Kabir and Jacob stealthily make their way to the camp periphery.  They trigger the alarm and run back into the camp before they can be seen. The guards conclude that it must have been triggered by a wild animal.  At this, Karamat switches off the alarm from the electricity room adjacent to his own office.  Ahmed follows him like a dimwitted man and sees the location of the switch.

As Ahmeds repatriation formalities are being fulfilled, he manages to steal from Karamats office the accessories and insignia that are present on Pakistani uniforms.  Ahmed also learns that the electricity room houses the communications line and the power generator.  He passes on this information to Suraj Singh and the others (Kabir, Jacob, Ram and Gurtu).

The plan is to create a stampede on the night of 14 August and escape under the pretext of escorting the ghazal singer out of the camp.  But to cause a stampede, they need to detonate a bomb. For this, Ram and Gurtu go on a pickpocketing spree and bring back to Jacob (an explosives and topography expert) a lot of matchboxes.  They remove the phosphorus heads of the matchsticks and make a crude bomb out of the combined match-heads.  They intend to throw this bomb into the ammunitions room to create a blast.  Once the bomb is ready, they tie it to the lower side of the floorboards to hide it.

The group photograph has been given to Colonel Puri.  To steal it from him, Ram and Gurtu make an alcoholic drink out of the jaggery and soon many of the POWs are drunk.  Colonel Puri is too drunk to notice the real reason behind the drinking session.  The photo is stolen.  Out of this, the face of Suraj Singh is cut out and stuck onto the stolen ID card.

The fake Pakistani uniforms are ready. So are the bomb and the ID card. The six men just have to wait for the song performance before starting their action. At this point, Suraj tells the others that the real intent of the mission is to alert the Indian authorities of the presence of Indian POWs in Pakistan. He also tells them that one or more of them may die while escaping and that their only consolation will be that they will have died as escapees instead of as prisoners.

===The Breakout===
While the floor is being cleaned with water that morning, the bomb gets soaked, but the escape team does not know this. As 14 August dawns, the six men wear two layers of uniforms; the Indian uniforms on top of the Pakistani ones. By evening, the ghazal singer is ready to give her song performance.

The senior Pakistani officers, including Shakoor and Karamat, are among the audience. The Indian POWs are allowed to sit as a separate audience to enjoy the songs. As the performance begins, Jacob and Kabir sneak away from the main POW group at separate times. Ahmed goes into Karamats office, telling the guard that the Major has asked for his jacket. Once inside, he knocks the guard unconscious and through a side door lets in Kabir, who has changed into his Pakistani uniform. He goes into the electricity room to disconnect the communication lines and power lines. He cuts the communication lines first, then, as per the plan, he must wait for the blast before he can cut the power line.

Jacob reaches the crude bomb and realizes that it is now soaked. He tells Ahmed, who is now by his side, that the mission must be called off. Then he rushes off to stop Suraj. However, before Ahmed can stop Kabir, he impatiently cuts the power line, plunging the camp in darkness. Ahmed does the last thing left to him to save his friends. He runs to the ammunitions room, forces his way in, locks himself in and primes one of the grenades. In the few seconds before the blast, he closes his eyes and thinks of his family; his aged parents, his wife and his daughter whom he has never seen and who would now be six. The room blows up into a ball of fire.

The remaining five men carry out the plan as conceived. There is a stampede and a general confusion as the soldiers attempt to put out the fire. Suraj and his men escort the ghazal singer out of the camp in an army truck. A few Pakistani soldiers already inside the truck become their unwitting captives. The Indians are no longer POWs, they are now soldiers on a mission.

===The Aftermath===
At the camp, the Pakistani guards take a head count of the prisoners and, seeing six of them missing, including Suraj Singh, discover that an escape has taken place. They try to locate the truck escorting Sultana Khanum. They also alert Major Bilal Mallik (Piyush Mishra), who is in charge of a helicopter, to help in their search.

Meanwhile, the escape truck has gone on a highway to Abbottabad. At a lonely spot, the escapees decide to get rid of their captives. One of them tries to rush his captors and in a scuffle shoots Jacob in the stomach. Suraj and Kabir shoot the Pakistani dead. Jacob lies to his comrades, saying that the bullet has just grazed him. The escapees relieve the Pakistani soldiers of their weapons and wallets and knock them unconscious. They also hide the body of the dead soldier. They are about to render Sultana unconscious, when she says that she was once the head of the Pakistan Human Rights Commission. She says that she was aware of the Red Cross raids across Pakistan in search of the Indian POWs. She is sympathetic to the plight of the POWs and promises to misguide the search party if she is left unharmed. In a touching line, she says to Suraj, "Hamaare mulk se thhoda yakeen hee lekar jaao." ("If nothing else, at least take home some trust from our country.")

===The Night===
The escapees are in a different truck now. They remember the blast at the camp and understand that it was done by Ahmed. They have gone off the highway to Abbottabad and are on another one, which leads to Muzaffarabad. The search party, including Major Mallik, has reached the escape truck, where they find Sultana Khanum, who has been left unharmed. She says that the escapees were in plain clothes and were talking of going towards Islamabad. The searchers believe her and arrange for her to be sent to her home. However, Major Mallik notices that the military map is missing from the trucks dashboard. When he asks Colonel Shakoor if the escapees were in any way connected to the army, Shakoor answers that it does not matter and that the escaped prisoners have to be brought back. In the meanwhile, the Chaklala camp is hurriedly closed and the Indian POWs are moved out of there.

Major Mallik separates from the search party and heads out in his chopper to cover the highway to Muzaffarabad. At a checkpoint, the escapees, led by Suraj Singh, gain access with the help of the fake ID card. Suraj asks the guard the reason for the checkpoint and is told that there is a search on for six fugitives. From this, he understands that the Pakistanis do not want it to be known that the fugitives are actually POWs.

Sultana Khanum phones Sabeena Jahangir from a wayside hotel and informs her of the presence of Indian POWs in a camp in Chaklala. She says that she is ready to provide testimony to that effect.

By early dawn, the searchers converge at the military hospital in Abbottabad to glean information from the injured soldiers. Here, Colonel Sheriar Khan and Major Azzam Baig berate Major Karamat and Colonel Shakoor for letting the escape take place. When Major Mallik asks if the escapees are Indian POWs, they grudgingly tell him the truth, and he agrees to maintain the secrecy of the search. They then talk to an injured soldier, who tells them about the preparedness of the fugitives. When the soldier mentions that they even have an ID card, Major Mallik realizes that he had actually seen them passing through the checkpoint on the highway to Muzaffarabad. The searchers prepare to go there.

===The Wild Chase At Muzaffarabad===
On the morning of 15 August, the escapees arrive at Muzaffarabad. The truck-driver goes on his way, and the soldiers check into a hotel. Once inside the hotel room, Suraj instructs Ram and Gurtu to purchase some necessary medical items from the local stores. He then asks Jacob to plot out an escape route on the map they took off the first escape truck. He charts out a route going up to Baramulla, India. This route avoids the main highway and goes through hills and villages. The last twenty kilometres have to be covered on foot, because the heavy snowfall makes it impossible for any vehicle to pass. Jacob then collapses and the others realize that he was not merely grazed by the bullet, but that his gunshot wound was life threatening. Kabir holds him tightly while Suraj attempts to remove the bullet from his stomach.

Meanwhile, in the town, while Ram and Gurtu are purchasing medicines and painkillers for Jacob, Ram notices a military convoy approaching. The search party is here. He and Gurtu run to the hotel to warn the others. Although they were not seen by the searchers, the place is soon swarming with soldiers. In the hotel room, Jacob finds the pain unbearable and reaches for a pistol. Ram and Gurtu reach the room just in time to see him shoot himself in the head. Although the four men are stunned, they have no time to waste. They cover Jacob in a blanket, take the map and the bags, and flee through the window.

As they run through the side streets of the town, the Pakistanis are on their heels. When the fugitives reach the main street, they have to hide behind a truck as it is crowded with soldiers led by Colonel Shakoor. They cannot remain behind the truck for long, because the column of soldiers chasing them is getting closer. Then Ram spots a motorbike, he tells Suraj and the others to be ready to take it. Before they can stop him, he runs into Colonel Shakoors view, shoots his guard and runs into a side-lane. As the soldiers run after him, the fugitives hide themselves under the tarpaulin of another truck, thereby escaping notice by the column that was chasing them. This column joins the other soldiers in the chase. As a straggler is attempting to start the motorbike, the escapees (Suraj, Kabir and Gurtu) run out of their hiding place, beat him unconscious and ride away on it.

Ram has taken control of an army jeep after killing a few soldiers, including Major Azzam Baig. He too has sustained a few bullet injuries, and is now leading the Pakistanis away from his friends. He now has only one pistol and one grenade for weapons. He leads the soldiers out of the town as far away as he can. The chasing convoy is led by Colonel Shakoor, who is berating his men to drive faster. Major Mallik is in the second jeep. Suddenly, Major Suraj Singh turns in from a dirt road and is riding next to the leading jeep. Colonel Shakoor stares into the faces of Suraj, Gurtu and Kabir. The jeep drivers face registers terror as he sees Kabir priming a grenade. Kabir lobs the grenade into the jeep as Suraj picks up speed and races away. In the next few seconds, the convoy comes to a standstill and Colonel Shakoor leaps out of his jeep. He runs a few steps and throws himself flat on the ground. The jeep explodes as the Indians race away. A stunned Major Mallik is unable to believe his eyes. The expression on his face sums up the scene: he did not expect the Indians to pull off a move like that.

===The Sacrifice===
The men on the bike catch up with Ram, who is relieved to see them safe. Gurtu asks innocently, "Listen, sir, is this the road to Delhi?" then tells him to watch where he is driving. But their victory is short-lived as they are attacked by a truck coming from the opposite direction. Although Kabir and Gurtu manage to shoot the driver of the truck and the shooters in its front, Suraj is forced to divert the bike into a ditch. Ram stops the jeep and watches his friends careening wildly downslope. They hit a rock and the bike cartwheels, then lands on Kabirs right leg. As the bike slides down the slope, it drags him with it. Suraj and Gurtu are lucky to survive with only a few scratches.

Ram is shot by the soldiers who are climbing out of the back of the truck. Gurtu is climbing upslope to rush to his aid with a furious Suraj attempting to stop him. Ram, still alive, sees the scene: Shakoor and Mallik have caught up and are driving towards him, the soldiers from the truck ahead are running towards him, and Gurtu and Suraj are dangerously close to revealing their position in the ditch. He quickly turns his jeep around and, with the soldiers still running after him, drives straight at Shakoors vehicle. Mallik, seated next to Shakoor, orders the driver to stop and reverse. He is sure that Ram is playing a fresh new trick, while Shakoor wants to get closer to Ram so that he can shoot him dead. Ram slams into them at high speed, causing serious injury to Shakoor, the soldiers in Shakoors truck and to himself. Shakoor, who is now unconscious, is laid on the ground as Mallik orders for an ambulance. Mallik then walks to Rams jeep to see what he was trying to do.

Ram is bleeding profusely, he has a few breaths worth of life left in him and he has primed a grenade. When Mallik sees the grenade, he shouts a warning for everyone to run away. As they all run away from the jeep, Mallik leading them, it explodes. In the ditch, Suraj and Gurtu see the explosion. Suraj covers Gurtus mouth so that his cries cannot be heard by the men on the road. They watch the jeep and the truck go up in flames, then Suraj forcibly turns Gurtu around and they come back to the motorbike, which has Kabir still partially pinned underneath it. The blast has claimed many lives, including Colonel Shakoors. As Mallik directs the rescue, Colonel Sheriar Khan catches up. Simultaneously, the fugitives get the bike upright and drive away.

===The Intervention===
While Mallik and Khan are directing the rescue, Sabeena Jahangir arrives there with the Red Cross delegates and questions Khan as to his lies regarding the Indian POWs. Mallik explains that this blast was the result of an accident involving two vehicles and that they are in no way connected to the POW issue. They go to the Muzaffarabad base, where in a closed room, Colonel Sheriar Khan is informed on the phone that a delegate has been sent to recall Sabeena Jahangir. When he expresses disbelief and frustration that the POW matter is now known, Mallik tells him that catching the POWs is now difficult. He says that in addition to being highly motivated, they are also military experts. He mentions Rams suicide bombing of the trucks as proof of how far they are willing to go. But until the arrival of the delegate, he says, Colonel Sheriar Khan must keep Jahangir pacified. When Khan attempts to tell Jahangir that there is a misunderstanding regarding the accident and the blast, she says that the occurrence of one blast at Chaklala and within one day another one at Muzaffarabad is highly suspicious. She alleges that the Pakistani military have deceived the Red Cross and the Pakistani public by lying that there are no Indian POWs in the country. Just then, the High Commission delegate arrives and shows Jahangir an order to vacate the area immediately. She glares at Mallik and Khan, then leaves with her colleagues. As soon as she is gone, the soldiers spring into action resuming their search.

===Saluting Fallen Comrades===
However, this intervention has bought off valuable time for the escapees. They have gone on the route marked by Jacob and reached the point from where they have to go by foot. The bike is anyway out of petrol. While Suraj Singh throws the bike down a mountainside, Gurtu has leaned Kabir against a tree and is making three mounds in the snow; one for each man who died on this mission. Seeing this, Suraj points out that they sacrificed their lives for their battle, so they must be respected as martyrs. He, Kabir and Gurtu stand up and salute this memorial. Then, they walk on into the snow, Kabir being held up by the others.

===The Cold Night===
The Indian soldiers have reached a point where they must stop and wait until dark before resuming their journey. They risk being caught if they go any further while there is still light. Suraj and Gurtu tie a splint improvised out of twigs onto Kabirs fractured leg.

By nightfall, the Pakistanis have reached a stream where they retrieve the bike that was thrown down the mountain. Khan and Mallik disagree in their conclusions. While Khan is certain that the fugitives have died and that their bodies should be lying close by, Mallik thinks that they could have thrown the bike down the mountainside and continued on foot. Khan berates Mallik for overestimating the fugitives. Mallik steps aside, and while Khan continues the search in the vicinity, gives voice to his own thoughts: "When the bloody war was over, there was never any need to detain these soldiers. Not only have we incurred the curses of their kin, we have also created a nuisance for ourselves. If I say this aloud, I will be declared a traitor. If I dont, then this Pakistani conscience of mine will torment me for life."

Suraj awakens Gurtu and Kabir to continue their journey. Kabir is not able to move his leg. Suraj removes the shoe on his injured leg only to see that it has turned black. Kabir says that it is frostbite and that it will climb up his body. However, when he requests to be left behind, Suraj will not hear of it. He carries Kabir bodily on his back, and their trek continues. They can do nothing but walk on. They keep themselves sane by exchanging little jokes and talking about their hometowns. When Suraj stumbles under Kabirs weight, Kabir points out that this way a two-hour journey will stretch to six hours. Suraj will again hear none of it. Sometime later, Kabir tells Suraj that he respects him more than he would respect his own father. Suraj replies that in the army a senior officer is like a father anyway.

===Unparalleled Heroism===
It is dawn. The three soldiers have stopped to rest by the side of a path winding through the mountains. Kabir and Gurtu are asleep; Suraj is sitting huddled next to them. He sees something far below him in the valley. He rises, then wakes up Gurtu and asks him to take a look. They strain their eyes. They cannot believe what they are seeing. It is an Indian army outpost with the Indian Flag raised above it. They cheer, scream, and laugh in jubilation. But when Gurtu tries to rouse Kabir, he falls limply to one side. He is dead. With nothing else left to do, Suraj and Gurtu bury Kabir in the snow and walk on towards the Indian side of the border. It is about a few hundred yards away.

Suddenly they hear the sound of a helicopter. It carries Colonel Sheriar Khan and Major Bilal Mallik. As Khan fires at them with a machine gun, Mallik cautions him that they are too close to the Line Of Control (LOC). As Suraj and Gurtu attempt to run, they end up taking a few bullet wounds. Gurtu is shot in his leg, which disables him from standing up. The Indian soldiers at the outpost, who see the helicopter, think that the Pakistanis are starting a skirmish. They get ready to fire a rocket.

Gurtu crawls into a rock alcove and asks Suraj to proceed alone without him. Suraj replies that he has no family back home in India and that if he reaches home he will do so with his companions. Suraj then says that they must survive for the sake of their comrades who died in the escape and for the sake of those who are still prisoners. Meanwhile, Mallik mutinies against Khan and orders the pilot to turn the chopper around. The Indians fire a rocket at the chopper. As the helicopter turns around and flies away behind the mountains, the rocket explodes harmlessly on a mountainside. On the ground, Gurtu is unable to walk, so Suraj seats him against a rock. He promises to bring help from the Indian side.

Major Suraj Singh calls out to the Indians, but he has been a prisoner for six years. As he calls for help, his voice stammers and fails many times. The Indians simply do not or cannot hear him. Then suddenly gunshots are heard and Suraj collapses. A column of Pakistani soldiers is running towards the two fugitives. Suraj is shot badly. The Indian soldiers rush to their defensive positions. As Gurtu is still recovering from the shock, Suraj struggles to his feet. He tells Gurtu that he will be back with help even before the Pakistanis arrive. He is still the father figure his men loved him for being. The Pakistanis are rushing on, screaming obscenities. Suraj runs towards the Indian outpost. As the Pakistanis fire at him, the Indians, thinking that a skirmish is on, fire back at the Pakistanis. This pins them down considerably so their firing on Suraj does not have much effect. Then the Indian officer sees Suraj through his binoculars and orders his men to stop firing as he realizes that the Pakistanis were chasing a fugitive. Then the Pakistanis are able to come out into the open and fire at Suraj. He nevertheless runs as hard as he can. There is an explosion close to his feet, probably a land mine, and he is thrown forward to the ground by the force of the blast.

Gurtu is looking at Suraj over the distance. His face has a look of peace. Suraj has reached the Indian side. He slowly stands up and looks ahead. He is home. He looks at the soldiers and at the flag above them. He is too overcome by emotion to say even a word. The Indians see a man in Pakistani army uniform before them. Suraj raises his right hand as if reaching for the flag. Then the Pakistanis fire one shot, which goes through Surajs heart. He falls to the ground and dies.

The Pakistani soldiers have reached the border. Khan and Mallik make their way to their head. The Indian officer shouts a question across the no-mans-land, asking what the matter is. Khan replies that the dead fugitive was a deserter from the Pakistani army who was court-martialled and had killed two civilians while escaping. He asks the Indians to search the dead man for an ID card, which should confirm his identity. The Indians find on Surajs person the fake ID card, which had got him through the Pakistani check post. The officer grants permission for the Pakistanis to take away the corpse and warns them to be careful in future. As Surajs body is dragged back to the Pakistani side, Mallik removes his beret in a gesture of respect to the man who, in his own way, did reach his country.

===Epilogue===
It is now 2007. The place is Multan Jail in Pakistan. We see an old man walking in the prison compound. He has made five little mounds of earth and he is putting a few flowers on them. He sits on the ground next to them and leafs through what was once Major Suraj Singhs prison diary. This old man is Gurtu. He now has only two motives in his life: to hope to return home and to keep alive the memory of his five friends.

The screen now blurs, and a Voice over informs us that there are still 54 Indian POWs of the 1971 and the 1965 Indo Pak wars who are languishing in Pakistani Jails. They were last seen alive in 1988. Copies of original letters posted by them come up as the credits Roll

==Cast==
* Manoj Bajpai as Major Suraj Singh
* Ravi Kishan as Capt. Jacob
* Piyush Mishra as Maj. Bilal Malik
* Deepak Dobriyal as Flight Lt. Gurtu
* Chittaranjan Giri as Ahmed
* Manav Kaul as Flight Lt. Ram
* Kumud Mishra as Capt. Kabir
* Gyan Prakash as Col. Puri
* Bikramjeet Kanwarpal as Col. Shakoor
* Sanjeev Wilson as Maj. Azzam baig
* Satyajit Sharma as Commander of the Indian checkpost

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 