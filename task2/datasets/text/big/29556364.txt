Queen of Babylon
{{Infobox film
| name           = Queen of Babylon
| image          = Queen_of_babylon_poster_01.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Carlo Ludovico Bragaglia
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Rhonda Fleming Ricardo Montalbán  Renzo Rossellini
| cinematography =  Gábor Pogány
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}} Italian film set in the Neo-Babylonian Empire in the year 600 BC.

This film was directed by Carlo Ludovico Bragaglia.

==Cast==
*Rhonda Fleming as "Semiramis"
*Ricardo Montalbán as "Amal"
*Roldano Lupi as "Assur"
*Carlo Ninchi as "Sibari"
*Tamara Lees as "Lysia"
* Furio Meniconi as "Bolgias" 
*Gildo Bocci 

==See also==
* List of historical drama films

==External links==
*  
*  

 

 
 
 
 
 
 

 
 