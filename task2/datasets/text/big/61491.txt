Les Misérables (1935 film)
{{Infobox film
| name           = Les Misérables
| image          = Les Misérables (1935 film) poster.jpg
| image_size     =
| caption        = Original film poster
| director       = Richard Boleslawski
| producer       = Darryl F. Zanuck
| screenplay     = W. P. Lipscomb
| based on       = Les Misérables by Victor Hugo
| starring       = Fredric March Charles Laughton Cedric Hardwicke Rochelle Hudson Alfred Newman
| cinematography = Gregg Toland
| editing        = Barbara McLean 20th Century Pictures, Inc.
| distributor    = United Artists   20th Century Fox  
| released       =  
| runtime        = 108 min.
| country        = United States
| language       = English
}}
 novel of the same name. The movie was adapted by W. P. Lipscomb and directed by Richard Boleslawski. This was the last film for 20th Century Pictures before it merged with Fox Film Corporation to form 20th Century Fox. The plot of the movie basically follows Hugos novel Les Misérables, but there are a large number of differences.
 National Board of Review named the film the sixth best of 1935.

==Cast==
* Fredric March as Jean Valjean/Champmathieu Inspector Émile Javert
* Cedric Hardwicke as Bishop Myriel
* Rochelle Hudson as Cosette
* Marilyn Knowlden as Young Cosette
* Florence Eldridge as Fantine John Beal Marius
* Frances Drake as Éponine
* John Carradine as Enjolras
* Ferdinand Gottschalk and Jane Kerr as the Thénardiers
* Vernon Downing as Brissac

==Differences from the novel==
This adaptation made quite a lot of changes, many of which can also be found in later adaptations:
* Valjeans trial, life as a convict and release are presented chronologically, whereas in the novel his previous life is presented in flashback. In addition, the novel begins by introducing the bishop, while in the film he does not appear until Valjean arrives at his door.
* The film begins with Valjean being sentenced in 1800 for ten years, rather than in 1796 for five years. sending someone to the galleys was abolished in mid-18th century. The galleys portrayed in this film are a huge anachronism.
* In the film, Javert is shown being assigned to the galleys, and seeing Valjeans display of strength at the beginning. In the novel he is not introduced until after Valjean has become mayor.
* Javerts first name is given as Émile, while in the novel it is never given.
* In the film, Valjeans prison number is 2906, while in the novel it is 24601.
* In the novel, Javert is described as a tall man, with a small head, sunken eyes, large sideburns and long hair hanging over his eyes, which differs greatly from Charles Laughtons appearance, and his version of Javert in the film wears different clothes than in the novel.
* Valjean is released after the 10-year sentence, despite mention of an escape attempt. In the novel he spends 19 years in prison, having been given extra time for trying to escape. He still receives a yellow passport, branding him a violent man for his attempts, however.
* In the film, there is no mention of Fantine selling her hair and teeth, or becoming a prostitute, to afford her payments to the Thénardiers.
* Valjean brings Cosette to Fantine before Fantine dies, while in the novel he does not fulfill this pledge to Fantine until on his own deathbed.
* The Thénardiers inn is called "The Sergeant at Waterloo" in the novel, but is called "The Brave Sergeant" in the film.
* In the novel, Valjean pays Thénardier 1,500 francs to settle Fantines debts and takes Cosette, the Thénardiers appear in Paris several years later. No discussion regarding Valjeans intentions takes place in the film; after speaking with Cosette alone, Valjean is seen riding away with her and the Thénardiers are not seen again. Champmathieu as being Valjean, in court. The film adds a fourth convict, Genflou, to the witnesses.
* In the film Valjean and Cosette go to the convent with a letter of introduction from M. Madeleine, whereas in the novel they came upon the convent purely coincidentally while fleeing from Javert.
* We see Valjean rescue a man whose cart had fallen on him, which arouses Javerts suspicion, but the film does not mention that this man (Les Misérables#Minor|Fauchelevent) and the gardener at the convent are the same person.
* Marius meets Valjean and Cosette while they ride into the park where he is giving a speech, while in the novel he is simply walking in the Luxembourg Gardens when he sees them.
* Éponines role is changed from the novel. In the film, she is the secretary of the revolutionary society Marius belongs to. In the novel, she is the Thénardiers daughter, and is not connected to the revolutionary society. The film makes no mention of her being the Thénardiers daughter.
* Gavroche is cut entirely.
* In the novel, Enjolras is the leader of the revolutionaries and Marius is not even a very faithful follower (him being a Bonapartist with different ideas than his friends). In the film, Marius is the leader. In addition, the students goal is not a democracy but to better the conditions in the French galleys. Marius says himself: "We are not revolutionaries."
* In the film, Éponine delivers the message from Marius to Cosette, which Valjean intercepts, causing Valjean to come to the barricade to rescue Marius. In the novel, Gavroche delivers it.
* In the film, Javert pursues Valjean and Marius into the sewers, which he does not in the novel, although he does meet Valjean when he exits the sewers, having pursued Thénardier there.
* Valjean brings Marius to Valjeans house and Cosette, while in the novel Valjean brings Marius to the house of Marius grandfather Les Misérables#Minor|M. Gillenormand, who does not appear in the film. Also, while Valjean thinks Javert is waiting for him and he is going away, he gives Marius and Cosette instructions, including to love each other always and leaving the candlesticks to Cosette, which in the novel appear in his deathbed scene.
* In the film, Valjean hides the fact that he expects Javert to arrest him by telling Cosette and Marius that he is still moving to the United Kingdom.
* The film ends with Javerts suicide, while at the end of the novel Valjean dies of grief after having been separated from Cosette, because Marius severed all ties with him after learning of Valjeans convict past, but both arrive to see him before he dies.

==See also== Adaptations of Les Misérables

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 