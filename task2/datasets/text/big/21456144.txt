The Final Test
 The Celestial Toymaker}}
{{Infobox film
| name           = The Final Test
| director       = Anthony Asquith
| writer         = Terence Rattigan Jack Warner Robert Morley Ray Jackson
| music          = Benjamin Frankel
| cinematography =
| editing        =
| distributor    = Rank Organisation
| released       = 1953
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
}} British sports Jack Warner, Act Films Ltd. 

==Plot==
The film is a comedy drama, set around leading cricketer Sam Palmers last appearance for England national cricket team|England. He desperately wants his son to be there at The Oval to witness this, but the son is more concerned with meeting a leading poet Alexander Whitehead. Whitehead, it turns out, is more interested in cricket.   Whitehead takes Reggie along to the match, in time to see Sam get out for a Duck (cricket)|duck.

==Cast== Jack Warner as Sam Palmer 
* Robert Morley as Alexander Whitehead 
* George Relph as Syd Thompson 
* Adrianne Allen as Aunt Ethel 
* Ray Jackson as Reggie Palmer 
* Brenda Bruce as Cora 
* Stanley Maxted as Senator 
* Joan Swinstead as Miss Fanshawe 
* John Glyn-Jones as Mr. Willis

The England cricketers Len Hutton, Denis Compton, Alec Bedser, Godfrey Evans, Jim Laker and Cyril Washbrook appear as themselves with John Arlott providing the match commentary.

==Genesis== Harrow Eleven, Victor Rothschild, duck in Jack Warner.

As with almost all of Rattigans plays, the theme of the relationship between father and son, and the tension between parental expectations and the sons driving force leading him in different directions, is explored.

It was filmed in 1953, directed by Anthony Asquith.

==Media releases==
The film was released on Region 2 DVD on 6 August 2007. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 