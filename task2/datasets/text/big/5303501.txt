A Tale of Winter
{{Infobox film
| name           = Conte dhiver
| image          = ConteDHiver1992Poster.jpg
| caption        = French poster
| director       = Éric Rohmer
| producer       = Margaret Ménégoz
| writer         = Éric Rohmer
| screenplay     = 
| story          = 
| based on       =  
| starring       = Charlotte Véry Frédéric van den Driessche Michael Voletti Hervé Furic Ava Loraschi
| music          = Sébastien Erms
| cinematography = Luc Pagès
| editing        = Mary Stephen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 114 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

A Tale of Winter ( ) is a 1992 French drama film directed by Éric Rohmer, and starring Charlotte Véry, Frédéric van den Driessche and Michael Voletti. It is the second of Rohmers "Tales of the Four Seasons" (Contes des quatre saisons), which also include A Tale of Springtime (1990), A Summers Tale (1996) and Autumn Tale (1998). The film was entered into the 42nd Berlin International Film Festival.    

==Plot==
During her holidays Félicie falls in love with a cook named Charles. Giving him her address for further meetings she makes a mistake and consequently he fails to find her. Five years later shes a single mother raising his daughter. She seems to be torn between the hair dresser Maxence and the librarian Loic but eventually it shows she cant be happy with either of them. Fortunately she runs again into Charles and they cling immediately as they did before. After their marriage they open a restaurant.

== Cast ==
* Charlotte Véry as Félicie
* Frédéric van den Driessche as Charles
* Michel Voletti as Maxence
* Hervé Furic as Loïc
* Ava Loraschi as Elise
* Christiane Desbois as Félicies mother Rosette as the sister
* Jean-Luc Revol as the brother-in-law
* Haydée Caillot as Edwige
* Jean-Claude Biette as Quentin
* Marie Rivière as Dora
* Claudine Paringaux as the customer
* Roger Dumas as Leontes
* Danièle Lebrun as Paulina
* Diane Lepvrier as Hermione

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 


 