Slipstream (1989 film)
{{Infobox film
| name           = Slipstream
| image          = Slipstream lobby small.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = UK Theatrical release cinema poster
| director       = Steven Lisberger
| producer       = Gary Kurtz
| writer         = 
| screenplay     = Tony Kayden
| story          = {{plainlist|
*Bill Bauer
*Charles Edward Pogue
}}
| based on       =  
| narrator       = 
| starring       = {{plainlist|
*Bob Peck
*Mark Hamill
*Kitty Aldridge
*Bill Paxton
*Robbie Coltrane
*Ben Kingsley
*F. Murray Abraham}}
| music          = Elmer Bernstein
| cinematography = Frank Tidy
| editing        = Terry Rawlings
| studio         = Entertainment Film
| distributor    = 
| released       =   
| runtime        = 102 minutes
| country        = United Kingdom   
| language       = English
| budget         = 
| gross          = 
}} humanity amongst artificial intelligence.
 cult classic 1982 science the original Star Wars, The Empire Strikes Back, The Dark Crystal, Return to Oz and American Graffiti.

Slipstream reunited Gary Kurtz with Star Wars star Mark Hamill, who portrays the central antagonist in Slipstream and had previously portrayed Luke Skywalker in Star Wars. Other stars of Slipstream include Bill Paxton, Bob Peck and Kitty Aldridge, and there are also cameo appearances from Robbie Coltrane, Ben Kingsley and F. Murray Abraham.

==Setting==
Slipstream takes place in an undetermined future in which human abuse of the natural world is taking a drastic toll. A voice over at the beginning of the film explains that a doomsday event has occurred, referred to as the "Convergence". Exactly what causes this natural disaster is unclear, but it is stated that many parts of the world were flooded; whereas, others were buried under cataclysmic earthquakes and rapidly shifting continents, leaving the land a bleak wasteland of high windswept plateaus, deep valleys, towering mountains and canyons of razor-sharp rock (panoramic aerial views of Cappadoccia, Turkey, are used for such shots). However, the worst part of this apocalypse was not geological but meteorological; the atmosphere churned with such ferocity that the jet stream expanded to the planets surface and swept it clean of human civilization with winds far surpassing anything ever seen before -- even the strongest hurricanes. Cities and entire nations were blown away like leaves in a strong gale.
 river currents; this is known as "riding the slipstream". As such, the remnants of society have become airborne and reside on high precipices or beneath the howling slipstream. Flying in airplanes, biplanes, Glider (sailplane)|gliders, gyrocopters, Zeppelins or hot air balloons is now the main, if not only, form of long distance transportation. It is stated that nobody has ever been to the ends of the slipstream, as the extreme turbulent atmospheric conditions at the ends of the slipstream make human survival impossible.

==Plot==
The film opens with a mysterious man, later referred to as "Byron" (portrayed by Bob Peck), running down a canyon, being harassed by an aeroplane. Byron takes refuge from his pursuers on a precipice overlooking the canyon. The plane lands and its occupants, two obsessive law enforcement officers named Will Tasker and Belitski (Mark Hamill and Kitty Aldridge), chase the man on foot. They shoot the fugitive through the arm with a grappling hook. The fugitive looks at his arm, but does not seem injured, only intrigued. Tasker pulls on the rope now connected to the fugitive. He tumbles down the side of the canyon, but once again is not harmed. Immediately after his fall, the fugitive recites the words of famous World War II aviator and poet John Gillespie Magee, Jr., "I have slipped the surly bonds of Earth - put out my hand and touched the Face of God.", from the poem High Flight.

They take the man prisoner and fly to a busy civilian airstrip where he stands beside them, handcuffed, as they eat in the airstrips diner. A roguish young wanderer and hustler, Matt Owens (Bill Paxton), makes a pass at Belitski and she nearly breaks his arm. Owens tries to sell contraband grenades to Tasker. It is then revealed that Tasker and Belitski are part of the remnants of a law enforcement agency, trying to keep the peace in what is left of society. Byron is wanted for murder, and they are bringing him to justice. Tasker seizes Owens weapons. Later, as the cops are preparing to leave, Owens forces them to give up Byron and flees in his own plane, but not before Belitski shoots him with a dart, both poisoning him and planting a tracking device in his body. Owens then flies off "downstream", planning to turn Byron in to the authorities for a reward. Belitski and Tasker are in hot pursuit.

Owens and Byron land at the home of a cult of people that worship the Slipstream and have recently been under attack by bandits. After Byron heals a boy who was born blind, Owens knows that he is more than what he appears to be. It is then revealed by Byron that he is an android (robot)|android. When the wind cult finds out about Byrons abilities, they tie him to a massive kite, and let the wind decide what to do with him. The bounty hunters arrive in the middle of a windstorm, and Belitski and Owens are forced to work together to get Byron down. Belitski then allows Byron and Owens to get away (or as she says, she is just giving Owens a head start), along with another visitor to the valley, a woman Ariel who has become very attached to Byron.

Ariel takes them to her home. She grew up with a group of hedonism|hedonists, who inhabit an underground museum. Byron and Ariel develop feelings for each other and spend the night holding each other among an African Savannah exhibit. Owens gets drunk and has a one-night stand with a sexy, but depressed local girl. Later that night, Owens and Byron have a heartfelt talk, in which Byron explains more of his past, that the man he killed was his master, although an educated man, Byron was little more than a slave to him and in killing him, he freed both of them. Byron also excitedly tells Matt that he actually slept for the first time had a dream. In fact, he was more excited about that than having sex, showing his innocence. And he dreamt the special place he was heading, to which Owens is won over and tells Byron he is cutting him loose and setting him free, and that he is now able to make his own decisions and find his own way in life. Byron is overwhelmed and unsure what to do with his newfound freedom and asks Matt is he free to stay as well, to which Matt says yes and Byron whoops for joy. Meanwhile the committee elders debate on what to do with the outsiders, as they do not wish for more outsiders to come in their wake.

Indeed they do. Tasker and Belitski track the trio to the museum, which they storm. They kill the gate guards and some of its inhabitants. Tasker, after bashing the curator, forces the rest to seek out Byron. Byron and Owens (who has just managed to persuade him to join him as an equal) are taken by surprise; Byron is seized and Owens floored. They leave and Belitski faces him holding a gun. Shooting him in the chest Owens punches her, almost knocking her out, but when she explains it was the antidote to the poison he realises his mistake. Instead of killing her, he handcuffs her to a bed while he sets off after Tasker with Belitskis shotgun. The two do however flirt a little, although Belitski still resists his advances.

In the lobby. Owens gets the drop on Tasker, but his aim is poor and his first shot goes wide. Byron will not let Owens kill Tasker and stands in his way as Owens shoots. Tasker then shoots Owens in the shoulder. Ariel does not want a shootout, but picks up a gun near her to protect Byron and fires at Tasker. Tasker shoots and kills her. Tasker, for the first time, when he sees Byrons reaction regrets his action, but it is too late as Byron cradles Ariel as she dies in his arms. Byron finally lets go of his passive nature and sets out to kill Tasker. Tasker now knows he is in trouble. Matt warns Byron that he will lose if he goes after Tasker and Byron says "I already have." Tasker now has a grieving, vengeful and almost invincible android after him, no longer passive and subservient, and having made it back to his plane retrieves an assault rifle and shoots at Byron, to no effect. Tasker tries to flee in his plane, which Byron manages to jump and hold onto, and then smash his way inside. Eventually, Byron relents to actually committing murder, using his free will for good and forgiveness, and attempts to control the plane with wires as the steering column was damaged in the altercation. However, Byron is unable to fully control the plane, and as it banks it just clips a mountain top which causes it to crash in a fiery ball of flame. Taskers second last words are that of John Gillespie Magee, Jr., "I have put out my hand and touched the Face of God," the same poem which Byron first recited to him, and is relieved that he believes at first that they will make it. Tasker is killed in the crash, but Byron, being an android, is blackened and burned but otherwise unharmed. He relents his anger and returns to the museum to find that Belitski and Owens have developed a budding romance. The film ends on a sanguine note. Byron hikes off by himself to look for some of his own kind he knows is far West, at the ends of the ocean, high up above the canyons, at the far ends of the slipstream, while Owens and Belitski leave to open their own airstrip with Matts dream of the hot air balloons, filling the sky.

==Cast and characters== pilot and Optica OA7. law and resists arrest, typecast as the naïve young hero in science fiction films. Although he was "Luke" in the "Star Wars" universe, here his character is closer to that of Han Solo.
*Kitty Aldridge as Belitski, Her first name is not given. Taskers younger and pretty female companion, is perhaps a rookie and his protégé. Belitski shows more sympathy towards their hunted fugitives than Tasker. While initially resistant to Matt Owens charms, she is eventually taken in by his plans for the future enough to give up her life as a bounty hunter. bounty on Byrons head, Owens is overtaken by greed and kidnaps Byron from Will Taskers custody so that he can turn Byron in himself and claim the large reward personally. blind boy regenerative qualities android who killed his elderly master by snapping his neck upon his masters own request as an act of euthanasia or mercy killing, and this is the "murder" that Byron is being hunted for. Byron feels sorrow and guilt for the act. His artificial intelligence is capable of growing and Byron is capable of overriding his own programming and utilizing free will and feeling emotion, although he is scared to do so. This free will eventually manifests itself in the desire to commit an actual murder, the same crime he has been hunted for, in retaliation for Will Taskers murder of Ariel, a woman whom Byron had begun to develop feelings for. Towards the end of the film Byron falls asleep for the first time and has a dream of finding more of his own kind in the perilous mountains at the end of the Slipstream, where he believes more androids to exist, and which at the end he goes off to find.
*Eleanor David as Ariel, a woman who joins Owens and Byron in the valley and leads them to the hedonistic underground museum. Byron, despite being an android, quickly develops emotions for Ariel. Ariel sacrifices her life to save Byron, when she stands in front of him as Will Tasker prepares to shoot him. In retaliation, Byron decides to go off and kill Will Tasker as vengeance, but relents in the end.
*Robbie Coltrane as Montclaire, a portly associate of Matt Owens in the mountain caves and apparent rogue whose favourite place appears to be a jacuzzi. Montclaire along with his entire party are put under arrest by Will Tasker in the woods for carrying contraband, and when they resist, they are all shot dead by Tasker and Belitski.
*Ben Kingsley as Avatar, the elderly leader of a highly superstitious wind-worshipping cult who see the slipstream as the manifestation of God. Avatar has a particularly nasty test for Byron, which involves him being put into the "Wrath of God", or tied to a kite which is then flown in the Slipstream overnight. Avatar is killed in conflict with unseen Banditry|marauders.
*F. Murray Abraham as Cornelius, the curator of a vast, preserved, underground museum, who wishes to keep it secret from the outside world and is wary of outsiders despite the fact many of his own are hedonistic.

==Releases, box office and reception==
Slipstream  was filmed in Cappadocia, Turkey, Ireland, and Pinewood Studios, Iver Heath, Buckinghamshire, England, UK.
Slipstream had a short cinema run in the United Kingdom, where it was considered a flop, and Australia, where the film grossed just $66,836 during its entire theatrical run. The film was never released in theaters in North America and enjoyed only moderate VHS sales. The film was released on videocassette by M.C.E.G. Virgin Home Entertainment in 1990. Several budget DVD copies of Slipstream have been released due to the fact the film has fallen into the public domain, and all are without any special features. Fans awaiting a directors cut have been disappointed after producer Gary Kurtz said in an interview that the script was originally much more violent, but that these violent scenes, which would have made the plot more coherent, were never even filmed.

It has also been released as a DVD double feature by "Destra Entertainment - Payless", the other feature being 1967s In the Year 2889.

The film currently has a "rotten" score on Rotten Tomatoes of 20%.

==Notes==
 

==External links==
*  
*  
*  
*   detailed synopsis and review of the movie at post-apocalypse.co.uk
*   at Moria film review

 

 
 
 
 
 
 
 
 
 
 
 
 
 