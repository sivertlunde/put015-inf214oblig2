Malady of Love
{{Infobox film
| name           = Malady of Love
| image          = 
| caption        =
| director       = Jacques Deray
| producer       = Marie-Laure Reyre
| writer         = Danièle Thompson Andrzej Zulawski
| narrator       = 
| starring       = Nastassja Kinski Jean-Hugues Anglade Michel Piccoli Jean-Claude Brialy
| music          = Romano Musumarra	
| cinematography = Jean-François Robin	
| editing        = Henri Lanoë
| distributor    = AMLF
| released       =  
| runtime        = 122 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $5,974,222  
}}
Malady of Love ( ) is a 1987 French drama romance film, directed by Jacques Deray.

==Cast==
* Nastassja Kinski as Juliette
* Jean-Hugues Anglade as Clément Potrel
* Michel Piccoli as Raoul Bergeron
* Jean-Claude Brialy as Frédéric
* Souad Amidou as Farida
* Jean-Paul Roussillon as Jacques
* Jean-Luc Porraz as Jean-Luc
* Sophie dAulan as Diane Catherine Jacob

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 