Bicycle Dreams
{{Infobox film
| name           = Bicycle Dreams
| image          = 
| alt            =  
| caption        = Theatrical release poster
| director       = Stephen Auerbach
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| starring       = Jure Robic Bob Breedlove Christopher MacDonald
| music          = 
| cinematography = 
| editing        = 
| studio         = Auerfilms
| distributor    = 
| released       =   
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Bicycle Dreams is a 2009 documentary film by director Stephen Auerbach about the Race Across America, a 3000-mile cross-country bicycle race. The film has won numerous film festival awards and had a successful screening tour.

==Plot==
The film covers the 2005 edition of the Race Across America. It focuses on several riders, including Jure Robic, Bob Breedlove, Christopher MacDonald, and Patrick Autissier. From the starting line in San Diego, the film follows the riders across the country as they endure difficult weather, challenging terrain, and sleep deprivation.  Several days into the race, Breedlove, a race veteran and endurance cycling legend, is killed in a head-on collision with a pickup truck. The other riders must deal with the aftermath of the accident and decide whether to continue the race.

==Production==
Bicycle Dreams was filmed during the 2005 edition of the Race Across America. The crew used 18 cameras, which they embedded within the riders’ crews to get more intimate footage.  In an interview about the production, director Stephen Auerbach said he endured stretches of 36 hours without sleep to document the race and to experience the same sleep deprivation as the riders.  The crew eventually shot 450 hours of raw footage. Auerbach chose to use little narration in the film, in order to preserve the authenticity of the race and keep the storytelling as “unvarnished” as possible. 

==Reception==
Bicycle Dreams has received exceptionally positive reviews. USA Todays Sal Ruibal noted that the film has "beautiful scenery and inspirational words from riders and coaches" while it "also probes deeper into the sports heart of darkness." 

The film has received a number of film festival awards, including Best Feature Documentary honors at the Yosemite Film Festival, Las Vegas Film Festival, Lake Arrowhead Film Festival, Grand Rapids Film Festival, Solstice Film Festival, Fallbrook Film Festival, Tiburon Film Festival, Red Rock Film Festival, Los Angeles Sports Film Festival, Moscow Sports Film Festival, Canada International Film Festival and the Breckenridge Film Festival. 

Bicycle Dreams has been screening continuously across the United States since its release in 2009. It has also screened internationally in India, Australia, Turkey, Russia, and the United Kingdom. The film’s current tour, which began in January 2012, has taken it to dozens of cities across the country, often playing to full houses. 

Bicycle Dreams was released on DVD in 2009 and is available through its website and Amazon.com, where it consistently ranks among the most popular cycling DVDs. 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 