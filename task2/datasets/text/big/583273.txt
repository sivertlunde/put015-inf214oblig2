Limelight (1952 film)
 
{{Infobox film
| name           = Limelight
| image          = Limelight4232.jpg
| caption        = Theatrical release poster
| director       = Charlie Chaplin
| producer       = Charlie Chaplin
| writer         = Charlie Chaplin
| starring       = {{plain list|
*Charlie Chaplin
*Claire Bloom
*Nigel Bruce
*Buster Keaton Sydney Earl Chaplin
*Wheeler Dryden Norman Lloyd
}}
| music          = Charlie Chaplin
| cinematography = Karl Struss
| editing        = Joe Inge
| distributor    = United Artists Image Entertainment
| released       =  
| runtime        = 137 minutes
| country        = United States
| language       = English
| budget         = $900,000 Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 56-58 
| gross          = $1,000,000  (domestic)  $7,000,000  (outside USA)  
}} Melissa Hayden. Academy Awards.

==Plot== Sydney Earl Chaplin), a young composer Calvero believes would be better suited to her. In order to give them a chance Calvero leaves home and becomes a street entertainer. Terry, now starring in her own show, eventually finds Calvero and persuades him to return to the stage for a benefit concert. Reunited with an old partner (Keaton), Calvero gives a triumphant comeback performance but immediately suffers a heart attack and dies in the wings while just a few feet away from him Terry, the second act on the bill, dances on stage. This represents the end of a generation and the coming of another, the fading of age and the emergence of youth under the Limelight, as said during the initial credits.

==Cast==
* Charlie Chaplin as Calvero
* Claire Bloom as Terry
* Nigel Bruce as Postant
* Buster Keaton as Calveros partner Sydney Earl Chaplin as Neville Norman Lloyd as Bodalink
* André Eglevsky as Male Ballet Dancer Melissa Hayden as Terrys dance double
* Geraldine Chaplin as Little Girl in Opening Scene
* Josephine Chaplin as Child in Opening Scene
* Charles Chaplin, Jr. as Clown
* Snub Pollard as Street Musician Michael Chaplin as Child in Opening Scene
* Oona ONeill as Extra

==Production==
  and Claire Bloom in Limelight]]
 his family in the film, including five of his children and his half-brother Wheeler Dryden. Chaplin chose stage actress Claire Bloom for the role of Terry, her first major role in films. Chaplin told his older sons he expected Limelight to be his last film. By all accounts he was very happy and energized during production, a fact often attributed to the joy of recreating his early career in the Music Hall. Most people who have studied the life of Chaplin would assume that his character in the film was based on his father Charles Chaplin, Sr. who had also lost his audience and had turned to alcohol which led to his death in 1901. In both his 1964 autobiography, and his 1974 book My Life in Pictures, however, Chaplin insists that Calvero is based on the life of stage actor Frank Tierney. Then, in contrast, Limelight was made during a time where Chaplin himself was starting to lose his audience. In many ways, the movie remains highly autobiographical.

The pairing of Chaplin and Buster Keaton in the final musical number is historic for being the only time the two performed together on film. Chaplin at first had not written the part for Keaton because he believed that the role was too small. It was not until he learned that Keaton was going through hard times (before Limelight, Keaton had gone through a disastrous marriage, lost most of his fortune in the divorce process, and had appeared infrequently in films in the recent years) that Chaplin insisted Keaton be cast in the film. A rumor has persisted, fueled by the intense rivalry among fans of the two comics, that Keaton gave such a superior performance that Chaplin jealously cut his scenes so he would not be upstaged by his rival. A close associate of Chaplin claimed that Chaplin not only did not feel threatened by Keatons performance, but also heavily edited his own footage of the duet while enhancing Keatons. According to Keatons biographer Rudi Blesh, Chaplin eased his notoriously rigid directorial style to give Keaton free rein to invent his own comic business during this sequence. Keatons widow Eleanor claimed that Buster was thrilled with his appearance in the film, and believed his business partner Raymond Rohauer started and fed the rumors.  Chaplins son Sydney Chaplin (actor)|Sydney, who also appeared in the film, said that even if some of Keatons best scenes were cut (which he did not believe), the storyline would not logically allow a supporting actor to suddenly appear and upstage the climactic comeback of Chaplins character. 

==Reception== Britain to communist sympathies, and many American theaters refused to play Limelight. Outside of cinemas in several East Coast cities, the film was not seen by the American moviegoing public. It was not until 1972 that the film was finally seen in wide American release. Limelight currently holds an excellent 96% on Rotten Tomatoes. The film was massively popular in Japan.  It was enormously successful in Europe and around the world. However in the US it was a relative disappointment, only taking in $1 million. 

Limelight enjoyed a cumulative worldwide gross of $8 million.  Chaplin biographer Jeffrey Vance notes that the film’s reputation has slowly grown over the decades. Vance maintains “Limelight is Chaplins last great film, and it plays like a self-conscious summing up of his life and career. As a journey back to his beginnings and an often rapier-sharp self-critique, Limelight is Chaplin’s most deeply personal and introspective film.” 

==The music==
 , and published by Bourne Co. Music Publishers|Bourne, Inc.]] Geoff Parsons John Turner. Oscar for Best Original Russell Garcia was the actual composer that should have been awarded the 1972 Oscar.  Larry Russells family denies the report. Regardless, it was the only competitive Academy Award Chaplin ever received. (He had previously received two Honorary Oscars.)

==Theatrical adaptation==
In 2012, it had its first worldwide stage adaptation by director, writer and actor Ariel Varela, in Peru.

==Home Video==
The film was released on a special edition 2 disc DVD from Warner Home Video in 2003, which later went out of print.  The Criterion Collection will release the film on both Blu-ray and DVD on May 19, 2015. 

==Legacy==
The sixtieth anniversary of Limelight was celebrated by the Academy of Motion Picture Arts and Sciences with a reception, panel, and film screening at their Samuel Goldwyn Theater in Beverly Hills, California, on October 3, 2012. Cast members Claire Bloom and Norman Lloyd shared their recollections in a conversation moderated by Chaplin biographer/archivist Jeffrey Vance.  

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 