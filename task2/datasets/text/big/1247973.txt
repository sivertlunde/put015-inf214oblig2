Open Range
 
{{Infobox film
| name           = Open Range
| image          = Open range poster.jpg
| director       = Kevin Costner
| producer       = Kevin Costner Jake Eberts David Valdes
| screenplay     = Craig Storper
| story          = Lauran Paine
| starring       = Robert Duvall Kevin Costner Annette Bening 
| music          = Michael Kamen
| cinematography = J. Michael Muro
| editing        = Miklos Wright Beacon Communications TIG Productions Buena Vista Pictures
| released       =  
| runtime        = 139 minutes
| country        = United States
| language       = English
| budget         = $22 million
| gross          = $68.2 million
}} western film directed and co-produced by Kevin Costner, starring Robert Duvall and Costner, with Annette Bening and Michael Gambon appearing in supporting roles. The film was the final on-screen appearance of Michael Jeter, who died before it was released, and the film was dedicated to Jeters memory, and to that of Costners parents, Bill and Sharon.

The film was a box office success, and was critically favored.

==Plot== Civil War and feels guilty over his past as a killer.
 Irish immigrant land baron, Denton Baxter (Gambon), who hates open-rangers. Mose is badly beaten and jailed by the marshal, Poole (Russo). The only friendly inhabitant is Percy (Jeter), a livery stable owner.

Boss and Charley become concerned when Mose does not return. They retrieve him from the jail but not before getting a warning from Baxter about free-grazing on his land. Moses injuries are so severe that Boss and Charley take him to Doc Barlow (McDermott). There they meet Sue Barlow (Bening). Charley is attracted immediately, but assumes that Sue is the doctors wife.

After catching masked riders scouting their cattle, Boss and Charley sneak up on their campfire in the night, and disarm them. At the same time, another attack results in Moses death. Button is badly injured and left for dead. Charley and Boss vow to avenge this injustice. They leave Button at the doctors house and go into town, where they lock Poole in his own jail. Boss knocks him out with chloroform he has stolen from the doctors office. The deputies are locked up as well.

Charley learns that Sue is the doctors sister, not his wife. He declares his feelings for her, and she gives him a locket for luck. Charley leaves a note with Percy, in which he states that if he should die, money from the sale of his saddle and gear are to be used to buy Sue a new tea set, having accidentally destroyed her previous one during a "flashback" episode.

Boss and Charley are pitted against Baxter and his men. Charley shoots Butler (Coates), the gunman who shot Button and killed Mose. An intense gun battle erupts in the street, with Boss, Charley and Percy outnumbered before the townspeople begin to openly fight against Baxter. After an intense firefight, Baxters men are dead and Baxter ends up wounded and alone, trapped in the jailhouse. Boss rushes to the jail, mortally wounding Baxter.

Sues brother tends to the wounded townspeople and open-rangers.  Charley speaks to Sue in private, telling her he must leave. She counters that she has a "big idea" about their future together and that she will wait for him to return. He does return, and proposes to Sue. Charley and Boss decide to give up the cattle business and settle down in Harmonville, taking over the saloon.

==Cast==
* Robert Duvall as Bluebonnet "Boss" Spearman
* Kevin Costner as Charley Waite
* Annette Bening as Sue Barlow
* Michael Gambon as Denton Baxter
* Michael Jeter as Percy
* Diego Luna as Button
* James Russo as Marshal Poole 
* Abraham Benrubi as Mose Harrison
* Dean McDermott as Dr. Walter "Doc" Barlow
* Kim Coates as Butler  Herb Kohler as Cafe Man
* Peter MacNeill as Mack
* Cliff Saunders as Ralph
* Patricia Stutz as Ralphs Wife
* Julian Richings as Wylie
* Ian Tracey as Tom
* Rod Wilson as Gus
* Alex Zahara as Chet

==Production==
===Inspiration===
Kevin Costner grew up reading the western romance novels of Lauran Paine and Open Range is based on Paines 1990 novel The Open Range Men. Screenwriter Craig Storper wanted to make a movie about "the evolution of violence in the West." Storper continues: "These characters dont seek violence... But the notion that its sometimes necessary... is the Westerns most fundamental ideal."   

===Casting===
Robert Duvall is the only actor that Costner had in mind for the role of Boss Spearman. Costner said that if Duvall had turned down the part, he might not have made the movie at all. Duvall accepted the role immediately and Costner gave him top billing. Duvall got bucked off a horse and broke six ribs while practicing his riding for this role. 

===Filming===
Cinematographer J. Michael Muro, was hand-picked by director Kevin Costner for his work on Dances With Wolves. 
 Stoney Indian Stoney Sioux First Nation worked as a film liaison.

Filming took place from June 17, 2002 to September 8, 2002.  Production spent over one million dollars to build a town from scratch since Costner didnt like any of the existing ones. This location was so far from civilization that they had to spend $40,000 to build a road to get there. Professional wranglers handled 225 head of cattle on the set. 

==Releases==
The films US theatrical release was on August 11, 2003, and went on theatrical release in the UK on March 19, 2004. 

==Reception==

===Box office===
Open Range was a success at the box office, making about $14 million in its opening weekend in the U.S across 2,075 screens. On a budget of around $22 million the film made back a box office return of around $70 million worldwide. 

===Critical reception===
The film received mostly positive reviews, receiving a 79% Fresh rating on Rotten Tomatoes. Roger Ebert gave it 3.5 stars out of 4, calling it "an imperfect but deeply involving and beautifully made Western".  Peter Bradshaw of The Guardian gave the film 4 stars out of 5, writing, "Duvall gives his best performance in ages" in a "tough, muscular, satisfying movie". 
 real time, grabbing the audience and showing them that when this kind of stuff happens in real life, it happens faster than you think it would."  A review on Moviola stated that the film has "one of the most exciting final gunfights ever filmed".  IGN, USA Today, Total Film and Guns & Ammo all also say the shootout scene is one of the best of all time. 

===Awards=== Taurus Award for stunt artist Chad Camilleri. It was #48 in TimeOut Londons "The 50 greatest westerns" list. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 