How I Spent My Summer Vacation (film)
 
{{Infobox film
| name           = How I Spent My Summer Vacation
| image          = How_I_Spent_My_Summer_Vacation_DVD.jpg
| director       = John Fisher
| producer       = Alan James Gay Christopher Mills
| writer         = John Fisher
| starring       = Deanna Davis RonReaco Lee E. Roger Mitchell Jade Jenise Dixon Sahr Ngaujah
| music          = Johnny Barrow
| cinematography = Charles Mills
| editing        = Alan James Gay Norman Todd
| distributor    = The Cinema Guild Xenon Pictures
| released       =  
| runtime        = 73 min.
| country        = United States English
}} directed by John Fisher. The film stars Deanna Davis and RonReaco Lee, and is Fishers directorial debut film.

==Plot==
Perry (RonReaco Lee|Lee) and Stephanies (Davis) junior year in college has just ended. Though the two have been dating since high school, they both feel that the spark in the relationship is gone, and that their constant bickering, breaking up, and making up has become more like a game. They both decide the best thing to do is to call off the relationship before their senior year, so they can test the waters with other people. Perry begins to date Tammy, a young woman who actually has a boyfriend overseas. While hes very attracted to Tammy (Dixon), he cant really shake his yearning for Stephanie. Eventually Perry tries to reconcile their relationship, and finds that Stephanie might be testing waters of her own.

==Cast==
* Deanna Dawn Davis &mdash; Stephanie
* RonReaco Lee &mdash; Perry
* Darren Law &mdash; Nolan
* E. Roger Mitchell &mdash; Joseph
* Felice Heather Monteith &mdash; Helen
* Jade Jenise Dixon &mdash; Tammy
* Maisha Dyson &mdash; Rhonda
* Maude Bond &mdash; Monica
* Sahr Ngaujah &mdash; DAngelo
* TErika Jenks &mdash; Kim

==External links==
*  
*  

 
 
 
 
 
 


 