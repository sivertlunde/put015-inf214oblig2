Beyond the Clouds (1995 film)
{{Infobox film
| name           = Beyond the Clouds
| image          = Beyond the Clouds poster.jpg
| caption        = Italian theatrical release poster
| border         = yes
| director       = {{Plainlist|
* Michelangelo Antonioni
* Wim Wenders
}}
| producer       = {{Plainlist|
* Philippe Carcassonne
* Stéphane Tchalgadjieff
}}
| writer         = {{Plainlist|
* Michelangelo Antonioni
* Tonino Guerra
* Wim Wenders
}}
| starring       = {{Plainlist|
* John Malkovich
* Sophie Marceau
* Vincent Perez
* Irène Jacob
* Jean Reno
}}
| music          = {{Plainlist|
* Bono
* Adam Clayton
* Van Morrison
* Laurent Petitgand
}}
| cinematography = {{Plainlist|
* Alfio Contini
* Robby Müller
}}
| editing        = {{Plainlist|
* Michelangelo Antonioni
* Peter Przygodda
* Lucian Segura
}}
| distributor    = {{Plainlist|
* Kidmark  (Italy) 
* Mercure Distribution  (France) 
* Artificial Eye  (UK) 
* Sceneries Distribution  (USA) 
}}
| released       =  
| runtime        = 112 minutes
| country        = Italy, Germany, France
| language       = Italian, French, English
| budget         = 
| gross          = {{Plainlist|
* $3,252,000  (Italy) 
* $31,738  (USA) 
}}
}} romantically infatuated with a woman who is soon to enter a convent.   

==Plot==
The director (John Malkovich) is flying to Italy following the conclusion of his latest film. On the airplane, as he looks out beyond the clouds, he begins to think about his next film and the art of filmmaking. Upon landing, he drives through the night through thick fog, with people appearing and disappearing like apparitions.

;Story of a Love Affair that Never Existed
In Ferrara, Italy, Silvano (Kim Rossi Stuart) meets Carmen (Inés Sastre) and asks her where he can find a room for the night. She directs him to a hotel where she is staying. After checking in, Silvano sees her in the hotel restaurant and joins her. Silvano learns that Carmen is a teacher. They are attracted to each other, but they retire to their separate rooms. She undresses and waits for him, but he never comes. The next morning, he finds that she has checked out of the hotel.

Two years later in a movie theater in Ferrara, Silvano sees Carmen again and later approaches her outside. As they walk past the Castello Estense, she speaks of words that need to be spoken, but he says the only words worth speaking are hidden inside. Silvano walks Carmen to her home, where she reveals that she lived with a man for a year, and only recently he left her. "Words do us good," she tells Silvano, "even in writing. A woman expects them. She always does." Although seemingly attracted to Silvano, Carmen rebufs his attempted kiss, and he leaves. Later he returns to her room, they undress, and he passes his hand over her skin as if unable to touch her. They move to passionately kiss, but they do not. Finally, he leaves without making passionate love to her. Out in the street, he looks back at her watching from a window.

;The Girl, The Crime ...
The director visits a deserted beach on a dreary day. The wind sweeps the sand across the beach. He finds an old photograph of a seaside town cradled on a hillside. In Portofino, Italy, while exploring the quaint passageways above town, the director encounters a beautiful woman (Sophie Marceau) and follows her to a seaside clothing shop where the woman works. In the shop, she gives him a look of recognition. They do not speak, but seem drawn to each other. As the director leaves, she gestures to him but he does not respond.

Some time later, the woman meets her friend at the Caffè Excelsior, but notices the director sitting nearby. She approaches him and confesses, "I killed my father. I stabbed him twelve times," and then walks away. The director follows and they talk about the killing that took place a year prior, for which she spent three months in jail before being acquitted. She takes him to the "scene of the crime" at the waterfront. Conflicted by her feelings for the director, she says, "You remind me of—somebody." They walk to her apartment and make love. Later, the director sits at a pool above the town contemplating the womans story and its impact on the film he is writing.

;Dont Try to Find Me
In a Paris café, a young woman (Chiara Caselli) approaches a man (Peter Weller) sitting by himself. She wants to talk with someone about a magazine article she just read. The man is enchanted by the young womans story. Three years later, the man returns home after seeing the young woman, who is by now his mistress. His wife, Patricia (Fanny Ardant), confronts her husband with an ultimatum: he must choose between them. When he goes to break up with his mistress, they end up making love. He returns home to find his wife drunk. "Everything seems ridiculous," she tells him. He assures her that he will leave his mistress, and they make love for the first time in three years. The husband returns to his mistress who becomes jealous when she learns that he slept with his wife. They fight, but again they end up making love.

Meanwhile, Carlo (Jean Reno) returns to his high-rise apartment to find it empty. He gets a phone call from his wife who has just emptied the apartment of most of their belongings and left him. After a brief angry exchange she hangs up on him. Looking around the apartment he sees a picture of his wife naked that she had ripped up before leaving. Patricia arrives at the apartment which she has just rented from Carlos wife. Patricia has just left her husband and is expecting their furniture to arrive shortly—she too has emptied her husbands home. Carlo reveals that his wife left him for her lover because he was away too often on business. Patricia then gets a phone call from her husband, and she tells him, "Dont try to find me." Carlo and Patricia approach each other and he says, "Theres a cure for everything." Patricia responds, "Thats what disturbs me." He kisses her hand gently.

In the French countryside, a man pulls a woman from the railroad tracks as a train passes. On the train the director considers the "limits of our brains, of our experience, of our culture, of our inspiration, of our imagination, of our sensitivity." A woman enters his compartment, gets a phone call, and says, "Dont call me again." Meanwhile, on a nearby hill, an artist (Marcello Mastroianni) is painting the very landscape the train is passing through. He explains to a woman the value of copying from the masters.

;This Body of Filth
In Aix-en-Provence, the director is contemplating the paintings in his hotel lobby when he notices a man entering the building across the street to deliver architectural drawings. When he reemerges, the man, Niccolo (Vincent Perez), passes a young woman (Irène Jacob) in the doorway and decides to follow her. He asks if he can accompany her, and she tells him shes going to church. Niccolo is surprised that this quiet woman has little to say and seems uninterested in the world around her. She tells him that to be happy we need to eliminated pointless thoughts—that she prefers the quiet.

As they walk along the cobblestone streets, the young woman tells Niccolo that she longs to escape her body, that it needs too much and is never satisfied. Niccolo seems more interested in her body. When she does not respond to his romantic approaches, he observes that she looks like someone who is in love, like someone who is satisfied. She acknowledges that she is. When they arrive at the Church of Saint-Jean-de-Malte, the young woman enters the church, and he follows her inside. During the service, the young woman appears deeply spiritual as the choir sings. Detached and bored, Niccolo walks around like a tourist gazing up at the architecture before sitting down away from the congregation.

Later Niccolo wakes up in the now empty church following the service. He runs out into the dark streets looking for the young woman, finding her at a well. They talk about the impermanence of things. He admits that he is scared of death, and she tells him she is scared of life—the life people lead. Niccolo accompanies the young woman home, stopping in an entranceway to escape the rain. Asked what would happen if he fell in love with her, she responds, "Youd be lighting a candle in a room full of light." When they reach her apartment, he asks to see her the next day. She replies simply, "Tomorrow I enter a convent." Niccolo walks out into the night and the rain.

==Cast==
* Fanny Ardant as Patricia
* Chiara Caselli as Mistress
* Irène Jacob as The Girl
* John Malkovich as The Director
* Sophie Marceau as The Girl
* Vincent Perez as Niccolo
* Jean Reno as Carlo
* Kim Rossi Stuart as Silvano
* Inés Sastre as Carmen
* Peter Weller as Husband
* Marcello Mastroianni as The Man of All Vices
* Jeanne Moreau as Friend
* Enrica Antonioni as Boutique Manager 

==Production==
===Screenplay===
The screenplay was adapted from four sketches titled "Story of a Love Affair that Never Existed", "The Girl, The Crime ...", "Dont Try to Find Me", and "This Body of Filth", from Antonionis book, That Bowling Alley on the Tiber. Antonioni, who was 83 at the time of the films production, had a stroke that left him severely incapacitated. The film was completed with help from Wim Wenders, who wrote its prologue and epilogue and worked on the screenplay. 

===Filming locations===
Beyond the Clouds was filmed in the following locations:
* Ferrara, Italy
* Aix-en-Provence, Bouches-du-Rhône, France
* Portofino, Genoa, Liguria, Italy
* Paris, France 

==Reception==
===Critical response===
Beyond the Clouds received mixed reviews with the general consensus being that fans of Antonionis work would welcome and appreciate this one of the directors last films; others would be less embracing of his distinctive cinematic style.

In his review in The New York Times, Stephen Holden wrote, "There are moments of such astounding visual power in Michelangelo Antonionis film Beyond the Clouds that you are all but transported through the screen to a place where the physical and emotional weather fuse into a palpable sadness. ... More than just a great directors autumnal musings, Beyond the Clouds is a long goodbye to an idea of cinema as a high art, one that can flourish only as long as there are directors of Mr. Antonionis vision to dream it into being." 

In his review in the San Francisco Chronicle, Edward Guthmann gave the film a qualified positive review, writing, "For Antonioni lovers, who dont mind the moody silences and labyrinths of an Antonioni film and will recognize the echoes of earlier films, Beyond the Clouds wont be disappointing. A less partial audience, Id guess, will find it slow, labored and self-conscious." Guthman sees as the dominant theme of the film "the ways in which desire distorts perception and leaves us betrayed when our wishes and reality prove incompatible. ... Its that compelling sense of mystery, of the endless search and its undercurrent of loneliness, that sets this great filmmaker apart."   

In his review in the Chicago Reader, Jonathan Rosenbaum wrote, "There are a lot of beautiful things in Beyond the Clouds: the style, the settings, the bodies of young men and women-many of them beautiful in the vaguely blank way that models are."   

On the aggregate reviewer web site Rotten Tomatoes, the film received a 65% positive rating from top film critics based on 20 reviews, and a 68% positive audience rating based on 3,618 reviews.   

===Box office===
The film earned $3,252,000 in gross revenue in Italy,  and $31,738 in the United States.    As of February 1996, the film had 230,924 admissions in France, and as of August 1996, the film had 24,951 admissions in Portugal.   

===Awards and nominations===
* 1995 Venice Film Festival FIPRESCI Prize (Michelangelo Antonioni, Wim Wenders) Won
* 1995 Valladolid International Film Festival Golden Spike Nomination (Michelangelo Antonioni, Wim Wenders)
* 1996 Italian National Syndicate of Film Journalists Silver Ribbon for Best Score (Lucio Dalla) Won
* 1996 Italian National Syndicate of Film Journalists Silver Ribbon Nomination for Best Director (Michelangelo Antonioni)
* 1996 David di Donatello Award for Best Cinematography (Alfio Contini) Won   

==References==
;Notes
 

;Citations
 

;Bibliography
 
*  
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 