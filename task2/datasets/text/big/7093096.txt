Taste of Fear
 
 
{{Infobox film
| name           = Taste of Fear Scream of Fear
| image          = Taste of Fear poster.jpg
| image_size     = 250px
| caption        = Original British theatrical poster 
| director       = Seth Holt
| producer       = Jimmy Sangster Michael Carreras
| writer         = Jimmy Sangster Ronald Lewis Ann Todd Christopher Lee
| music          = Clifton Parker
| cinematography = Douglas Slocombe
| editing        = Eric Boyd-Perkins
| studio         = Hammer Film Productions
| distributor    = Columbia Pictures Corporation
| released       = London: Jan 1961 New York: 4 April 1961
| runtime        = 78 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =  $800,000 (Europe) Marcus Hearn, The Hammer Vault, Titan Books, 2011 p61 
}}
 1961 United British Thriller thriller film Hammer Films. Ronald Lewis, Ann Todd, and Christopher Lee, the latter, one of Hammers most bankable stars, in a supporting role.

==Plot==
A young paralysed woman (Susan Strasberg) returns to her family home after the mysterious disappearance of her father. She has a cool relationship with her stepmother, while the chauffeur helps her to investigate the fathers disappearance. During the investigations, she finds the fathers corpse in various locations around the house, but it always quickly vanishes again before anyone else sees it.

==Cast==
* Susan Strasberg as Penny Appleby Ronald Lewis as Bob
* Ann Todd as Jane Appleby
* Christopher Lee as Doctor Gerrard
* John Serret as Inspector Legrand
* Leonard Sachs as Spratt
* Anne Blake as Marie Fred Johnson as Father

==Reception==
The film was not a large success in the UK and US but was very popular in Europe and led to a cycle of similar films. 

==Production notes==
* Christopher Lee has been quoted as saying: "Taste of Fear was the best film that I was in that Hammer ever made...   It had the best director, the best cast and the best story." 

* Lee recalled that, while shooting the dining-room scene, he suddenly realized he was being observed by Gary Cooper. Director Seth Holt remarked that Lee looked as though hed "seen a ghost". Cooper was shooting what was to be his last film, The Naked Edge, at the studio.  Lee recalls seeing Cooper having lunch every day by himself; the actor was dying of cancer.  
 The Orphanage. 

==See also==
*List of Hammer films

== References ==
 
; Sources
*  

== External links ==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 