Damsels in Distress
{{Infobox film
| name = Damsels in Distress
| image = Damsels in distress poster.jpg
| caption = 
| director = Whit Stillman
| producer = Martin Shafer Liz Glotzer
| writer = Whit Stillman Hugo Becker Ryan Metcalf
| music = Mark Suozzo
| cinematography =Doug Emmett 
| editing = Andrew Hafitz
| distributor = Sony Pictures Classics
| released =  
| runtime = 99 minutes
| country = United States
| language = English
| budget = $3,000,000 
| gross = $1,220,699 
}}

Damsels in Distress (original title: Violet Wisters Damsels in Distress and Whit Stillmans Damsels in Distress) is an American comedy film written and directed by Whit Stillman and starring Greta Gerwig, Adam Brody, and Analeigh Tipton. It is set at a United States East Coast university. First screened at the 68th Venice International Film Festival  and the Toronto International Film Festival,    the film opened in New York and Los Angeles on April 6, 2012.

==Plot==
Newly transferred college student Lily becomes friends with Violet, Heather and Rose, a clique who run the campus suicide prevention centre. Over the course of the film, the four girls date less attractive men in order to help the mens confidence; they try to clean up the "unhygenic" Doar Dorm; they clash with the editor of the campus newspaper The Daily Complainer who wants to close down the "elitist" Roman letters fraternities; and finally they try and start a new dance craze, called The Sambola!

==Cast==
* Greta Gerwig as Violet Wister
* Adam Brody as Charlie
* Analeigh Tipton as Lily
* Carrie MacLemore as Heather
* Megalyn Echikunwoke as Rose Hugo Becker as Xavier 
* Ryan Metcalf as Frank
* Billy Magnussen as Thor
* Caitlin Fitzgerald as Priss
* Jermaine Crawford as Jimbo
* Alia Shawkat as Mad Madge 
* Aubrey Plaza as Debbie
* Zach Woods as Rick DeWolfe
* Taylor Nichols as Professor Black
* Carolyn Farina as Carolina Antonucci
* Meredith Hagner as Alice

==Development==
The film was Stillmans first produced feature since The Last Days of Disco. In August 1998 he had moved from New York to Paris with his wife and two daughters. In that time he wrote a novelisation of The Last Days of Disco plus several original film scripts which were not made, including one set in Jamaica in the 1960s. He resolved to make a lower budgeted film in the style of his debut, Metropolitan. In 2006 he met with Liz Glotzer and Mart Shafter at Castle Rock Entertainment, who had financed his second and third films. Shafer:
 Whit said, ‘I want to write a movie about four girls in a dorm who are trying to keep things civil in an uncivil world.’ It took him a year to write 23 pages. Six months later, a few more dribbled in. He just doesn’t work very fast. Finally we had a draft. When we started production he said, ‘I think 12 years is the right amount of time between movies.’   accessed 30 March 2015  
Castle Rock ended up providing most of the $3 million budget. 
==Production==
The movie was filmed on location in New York City on Staten Island at the Sailors Snug Harbor Cultural Center.

Filming finished on 5 November 2010. 

Stillman has said that the film was cut between its festival and theatrical runs:
 I felt the   vagueness and delayed the laugh.  

==Reception==
The film currently holds a 75% approval rating on review aggregator Rotten Tomatoes. The consensus reads, "Damsels in Distress can sometimes feel mannered and outlandish, but its redeemed by director Whit Stillmans oddball cleverness and Greta Gerwigs dryly funny performance."

In festival screenings, the film has been enthusiastically received by critics. In Variety (magazine)|Variety, Leslie Felperin wrote, "a film that raises laughs even with its end credits, Whit Stillmans whimsical campus comedy Damsels in Distress is an utter delight."  In Time (magazine)|Time, critic Richard Corliss wrote, "Innocence deserted teen movies ages ago, but it makes a comeback, revived and romanticized, in this joyous anachronism."  Andrew OHehir of Salon (magazine)|Salon praised Gerwigs "powerful and complicated performance" and said that "its both a relief and a delight to discover that Stillman remains one of the funniest writers in captivity." He concluded, "I laughed until I cried, and you may too (if you don’t find it pointless and teeth-grindingly irritating). Either way, Whit Stillman is back at last, bringing his peculiar brand of counterprogramming refreshment to our jaded age."  Jordan Hoffman of World Film gave the film four stars out of five, calling Gerwig "a massive, multi-faceted talent" and the film a "love it or hate it movie. Personally, I think the ones who arent charmed to pieces by its endless banter and preposterous characters very much need our help to expand their tastes and accept a more enlightened purview of what, indeed, is refined and acceptable motion picture entertainment." 

==References==
 

==External links==
*  
*  
*  
*   New York Times, March 18, 2012

 

 
 
 
 
 
 
 
 
 
 