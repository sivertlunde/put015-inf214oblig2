The Merry Widow (1962 film)
{{Infobox film
| name           = Die lustige Witwe
| image          = 
| image_size     = 
| caption        = 
| director       = Werner Jacobs
| producer       = Herbert Gruber (producer)
| writer         = Janne Furch (adaptation) Janne Furch (screenplay) Viktor Léon (libretto) Leo Stein (libretto)
| narrator       = 
| starring       = See below
| music          = Franz Lehár (from operetta Die lustige Witwe)
| cinematography = Friedl Behn-Grund Rudolf Sandtner
| editing        = Arnfried Heyne
| studio         = 
| distributor    = 
| released       = 1962
| runtime        = 110 minutes
| country        = Austria France
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

  Austrian / Peter Alexander, Karin Hübner and Gunther Philipp.  It is based on the operetta The Merry Widow by Franz Lehár.

== Plot summary ==
 

=== Differences from source ===
 

== Cast == Peter Alexander Danilo
*Karin Hübner as Hanna
*Gunther Philipp as Hugo
*Maurice Teynac as André Napoleon Renard
*Geneviève Cluny as Valencienne, Revuestar
*Germaine Montero as Anna, Wirtin von "Chez Anna"
*Ernst Waldbrunn as Testamentsvollstrecker
*Harald Maresch as Baron Zeta
*Herbert Kersten as Dr. Martin
*Helmut Lex as Jack Bromfield
*Darío Moreno as Camillo, Valenciennes Mann

== Soundtrack ==
 

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 

 
 