Half Past Dead 2
{{Infobox Film
| name           = Half Past Dead 2
| image          =  Half Past Dead 2.jpg
| image_size     =
| caption        = DVD cover
| director       = Art Camacho
| producer       = Andrew Stevens William B. Steakley Jason DeBose
| writer         = Andrew Stevens D. Kyle Johnson Don Michael Paul (characters) Jack Conley Robert Madrid
| cinematography = Ken Blakey
| editing        = Michael Kuge
| distributor    = Sony Pictures Home Entertainment
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
Half Past Dead 2 is a 2007 American action film directed by Art Camacho, and starring Bill Goldberg and Kurupt. It is a sequel to the 2002s Half Past Dead starring Steven Seagal and Morris Chestnut. Among the main cast of the original movie, and only Kurupt returns to reprise his role of Twitch. The film was released in the United States on May 15, 2007.

==Plot== New Alcatraz massacre, long time inmate Twitch (Kurupt) gets himself transferred to another. He claims its to be closer to his lady but his real motives are a bit more grandiose than that.

There he crosses paths with Burke (Bill Goldberg) a bulky prisoner who is unfrendly and doesnt want to talk about anyone. Twitch, despite being less muscular, is just as mouthy and is pretty much the same. But there is a gang war brewing between the Black and Hispanic inmates that explodes into a hostile takeover of the prison when the Blacks gang leader is shot dead and the finger points at Burke. But the situations worsen when the real killer and leader of the Hispanics, Cortez (Robert Madrid) takes Twitchs girlfriend (Angell Conwell) and Burkes daughter (Alona Tal) hostage as well, betraying his comrades to escape. Eventually things get more complicated as Twitchs real reason for his transfer is to find the gold from the heist, organized from the fellow New Alcatraz inmate Lester McKena.

Cortez demands a helicopter out of state or otherwise the hostages are dead. Burke and Twitch eventually catch up to Cortez and after a long fight with Burke ending up wounded, Cortez is knocked out and transferred to another prison.

Twitch is given parole after his actions that could have seen him wait even longer before he actually gets out, with Burke having to serve only a few more weeks rather than years. Twitch and his girlfriend find the gold and, as a favor for Burke, set up his account with 80 million dollars along with a plan to help Burkes daughter for college, surprising Burke himself.

==Cast==
* Kurupt as Twitch
* Bill Goldberg as Burke
* Angell Conwell as	Cherise
* Robert Madrid as 	Cortez
* Joe Perez as Lewis
* Alona Tal as Ellie
* Morocco Omari as J.T. Jack Conley as Wallace
* Robert LaSardo as Rivera
* Bruce Weitz as Kentut or Fart

==Music==
The main title for Half Past Dead 2, "Day By Day", was written by Jon Lee and Redd Stylez. The vocals were performed by R&B artist Redd Stylez.

==External links==
*  

 
 
 
 
 
 
 
 