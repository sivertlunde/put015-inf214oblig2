Matthew Barney: No Restraint
  2006 Documentary documentary directed by Alison Chernick. It follows artist Matthew Barney (best known for The Cremaster Cycle) and his collaborator, singer-songwriter Björk, as they embark on a filmmaking journey in Japan. It reveals Barneys process in creating Drawing Restraint 9, a cinematic "piece" that combines a whaling vessel; 45,000 pounds of petroleum jelly; and traditional Japanese rituals into a fantasy love story. 

It premiered at the Berlin Film Festival and had its theatrical release in December 2006 after being acquired by Independent Film Channel|IFC/The Weinstein Company.

== External links ==
*  
*  

 
 
 
 

 