Deadly Illusion
{{Infobox film
| name           = Deadly Illusion
| image          = Deadly Illusion 1987.jpg
| image_size     = 
| caption        =  Michael Shapiro Rodney Sheldon Michael Tadross
| director       = Larry Cohen William Tannen
| writer         = Larry Cohen
| narrator       = 
| starring       = Billy Dee Williams
| music          = Patrick Gleeson
| cinematography = Daniel Pearl
| editing        = Steve Mirkovich Ron Spang
| studio         = Pound Ridge Films
| distributor    = Cantel Films RCA/Columbia Pictures Home Video
| released       = October 16, 1987
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}} Vanity and Morgan Fairchild. 

==Cast==
*Billy Dee Williams as Hamberger Vanity as Rina
*Morgan Fairchild as Jane Mallory / Sharon Burton John Beck as Alex Burton
*Detective Paul Lefferts as Joseph Cortese

==Production==
Filming took place at Rockefeller Center and Times Square in Manhattan and at Shea Stadium in Queens. 

==References==
 

==External links==
* 
* 

 

 
 