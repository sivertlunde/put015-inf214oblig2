Stealing Rembrandt
{{Infobox Film
 | name = Stealing Rembrandt
 | image_size = 
 | caption = 
 | director = Jannik Johansen
 | producer = Thomas Gammeltoft
 | writer = Jannik Johansen, Anders Thomas Jensen
 | narrator = 
 | starring = Jakob Cedergren, Lars Brygmann, Nicolas Bro
 | music = Anthony Genn
 | cinematography = Eric Kress
 | editing = Per K. Kirkegaard
 | studio         = Fine & Mellow Productions
| distributor = Nordisk Film   Biografdistributionen
 | released = 5 September 2003
 | runtime = 104 minutes
 | country = Denmark Danish
 | budget = 
 | preceded_by = 
 | followed_by = 
 | website = 
 | image = Stealing Rembrandt VideoCover.jpeg
}}
 2003 Danish language|Danish-language film. An action-comedy, the film concerns a father and son who accidentally steal a painting by Rembrandt. A Danish/UK co-production, the film was directed by Jannik Johansen and written by Anders Thomas Jensen and Jannik Johansen.

The film was premiered at the 2003 Cannes Film Festival.

==Relation to real events==
On 29 January 1999, Rembrandts "portrait of a lady" (as well as another painting by Bellini) was stolen from the poorly protected Nivaagaard Samlingen in Nivå in Denmark. The depiction in the film of the level of security at that time is fairly accurate, as well as the involvement of bounty hunters as civilian agents in the case. Also, the persons involved were related as depicted, although the role of the father (Mick in the movie) was actually the uncle (of Tom in the movie).  

==Cast==
*Lars Brygmann - Mick
*Jakob Cedergren - Tom
*Nikolaj Coster-Waldau - Kenneth (as Nikolaj Coster Waldau)
*Nicolas Bro - Jimmy
*Sonja Richter - Trine
*Søren Pilmark - Bæk Gordon Kennedy - Christian
*Paprika Steen - Charlotte
*Ulf Pilgaard - Flemming
*Thomas W. Gabrielsson - Erik
*Ole Ernst - Frank
*Nikolaj Lie Kaas - Kiosk Karsten
*Patrick OKane - Nigel
*Martin Wenner - Toby
*Søren Poppel - Allan Rocker

==See also==
*Cinema of Denmark

==Notes==
 

==External links==
* 
* 

 
 
 
 
 


 
 