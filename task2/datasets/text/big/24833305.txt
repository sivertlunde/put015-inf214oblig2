Rampage (1963 film)
{{Infobox film
| name           = Rampage
| image          = Ramppos.jpg
| image_size     =265px
| caption        = Original film poster
| director       = Phil Karlson
| producer       = William Fadiman
| writer         = Alan Caillou (novel) Robert I. Holt Marguerite Roberts Jerome Bixby (uncredited)
| narrator       =
| starring       = Robert Mitchum Jack Hawkins Elsa Martinelli
| music          = Elmer Bernstein
| cinematography = Harold Lipstein
| editing        = Gene Milford
| distributor    = Seven Arts Productions
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         =
}}
Rampage is a 1963 adventure film about big game hunters set in Malaysia and starring Robert Mitchum, Jack Hawkins, and Elsa Martinelli.  The movie was directed by Phil Karlson from the novel by Alan Caillou and features a musical score by Elmer Bernstein.

==Cast==
Robert Mitchum	 ... 	Harry Stanton 
Jack Hawkins	... 	Otto Abbot 
Elsa Martinelli	... 	Anna  Sabu	... 	Talib 
Cely Carillo	... 	Chep 
Émile Genest	... 	Schelling 
Stefan Schnabel	... 	Sakai Chief 
David Cadiente	... 	Baka

==External links==
*  

 

 
 
 
 
 
 