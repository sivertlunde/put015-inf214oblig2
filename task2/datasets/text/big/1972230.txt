Hold the Lion, Please
{{Infobox Hollywood cartoon|
| cartoon_name = Hold the Lion, Please
| series = Merrie Melodies/Bugs Bunny
| image =  Hold the Lion Please title card.png
| caption = Title card of Hold the Lion, Please. Charles M. Jones
| story_artist = Tedd Pierce
| animator = Ken Harris
| voice_actor = Mel Blanc (Bugs Bunny) Tedd Pierce (Lion)  Tex Avery (all uncredited) Carl W. Stalling
| producer = Leon Schlesinger Leon Schlesinger Productions
| distributor = Warner Bros. Pictures
| release_date = June 13, 1942
| color_process = Technicolor
| runtime = 8:00
| movie_language = English
}}

Hold the Lion, Please is a 1942 Merrie Melodies cartoon, first released on June 13, 1942, distributed by the Vitaphone Corporation and Warner Bros.. This is the first Bugs Bunny cartoon where the title does not refer "hare", "bunny", or "rabbit". This short is Bugs Bunnys tenth appearance and Chuck Jones 39th Warner Bros. cartoon.

The short was directed by Chuck Jones, animated by Ken Harris and scripted by Tedd Pierce. Musical Direction was assigned to Carl Stalling.

The title is a play on the expression used by switchboard operators of the day, asking the caller to "hold the line." The Three Stooges made a short with a similar title, Hold That Lion, which also featured a renegade lion.

==Synopsis==
The short focuses on a lion named Leo whos trying to prove hes still "King of the Jungle" by hunting a small, defenseless animal.  He chooses Bugs Bunny as his intended victim, but Leo soon finds out that, in a battle of wits, hes the defenseless one. However, Leo eventually gets Bugs under one paw while raising the other one, claws extended, and looking extremely angry; a truly frightened-looking Bugs could be facing his end.
 shows the audience that she "wears the pants the family."

==Cast==
* Mel Blanc voiced Bugs Bunny, Monkey, Giraffe, Mrs. Bugs Bunny.
* Tedd Pierce voiced Leo the Lion.
* Tex Avery voiced the Hippopotamus|Hippo, having recorded his lines before he left the studio.

==External links==
* 
* 
*  

         
{{succession box |
before= The Wacky Wabbit | Bugs Bunny Cartoons |
years=1942 |
after= Bugs Bunny Gets the Boid|}}
 

 
 
 
 
 