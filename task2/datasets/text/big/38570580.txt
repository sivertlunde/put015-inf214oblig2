Must Be... Love
{{Infobox film 
| name           = Must Be... Love
| image          = Must Be Love (film).jpg
| caption        = Theatrical movie poster
| director       = Dado Lumibao
| producer       =  
| writer         =  
| screenplay     =  
| starring       = Kathryn Bernardo   Daniel Padilla
| music          = Jessie Lasaten
| cinematography =  
| editing        = Marya Ignacio
| distributor    = Star Cinema
| released       =  
| runtime        = 122 minutes
| country        = Philippines
| language       = Tagalog English
| budget         = 
| gross          = PHP 73 million    
 
}}
 Laguna in the Philippines. The film was produced by Star Cinema and was released nationwide on March 13, 2013. 

This marks the first solo film with love tandem Kathryn Bernardo and Daniel Padilla in a leading role following their roles in the ensemble films 24/7 in Love and Sisterakas in 2012. 

== Synopsis ==
"Must Be... Love" follows the love story of Patricia, better known as "Patchot or Patch" played by (Kathryn Bernardo) and her childhood best friend Ivan (Daniel Padilla). But what will Patchot do if she starts falling in love with her best friend?. And what if Ivan only sees her as a best friend? This problem will develop when another girl enters Ivans life. Unfortunately for Patchot, its her cousin Angel (Liza Soberano). Will Ivan fall in love with "Patchot" or will they remain as best friends forever?

==Casts==

===Main Cast===
*Kathryn Bernardo as Princess Patricia "Patchot" Espinosa
*Daniel Padilla as Ivan Lacson

===Supporting Cast===
* John Estrada as King Espinosa
* Liza Soberano as Angel Gomez
* Arlene Muhlach as Gwen Martinez
* John Lapus as Tita Baby
* Cacai Bautista as Dolly
* Janus Del Prado as Gordo
* Paul Salas as Jake
* Sharlene San Pedro as Lavinia
* Ramon Christopher Gutierrez as Tito Conde
* Kit Thompson as Nicco
* Alan Paule as Mang Badong
* Miguel Morales as Bok
* Cris Gabriel Queg as Bibap

=== Cameo Appearance ===
* G. Toengi as Patchots Mother 
* Enrique Gil as Dave (uncredited)
* Sofia Milla as young Patchot Espinosa 
* Kyle Banzon as young Ivan Martinez

==Production==

===Music=== Nasa Iyo Himig Handog P-Pop Love Songs originally recorded by Daniel Padilla, was used as the films theme song. A mid-tempo version of the song was recorded by Sam Milby, which it was used in the trailer of the film and was also played in the film. 

==Reception==

===Rating===
The film was graded "B" by the Cinema Evaluation Board, and it received G rating by the MTRCB.

===Box office=== A Moment In Time which earned ₱64.54 million.

==References==
 

== External links ==
 

 
 
 
 
 
 
 
 