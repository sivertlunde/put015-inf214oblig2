Bhaaryaavijayam
{{Infobox film
| name           = Bhaaryaavijayam
| image          =
| caption        =
| director       = AB Raj
| producer       = Sadanandan
| writer         = Gopu Sreekumaran Thampi (dialogues)
| screenplay     = Sreekumaran Thampi Vincent
| music          = M. K. Arjunan
| cinematography =
| editing        =
| studio         = Shanmukham Arts
| distributor    = Shanmukham Arts
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Vincent in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Jayabharathi Vincent

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || April Maasathil || Ambili || Sreekumaran Thampi || 
|-
| 2 || Kaamadevanenikku Thanna || P Jayachandran || Sreekumaran Thampi || 
|-
| 3 || Kadalum Karayum || P Susheela, P Jayachandran || Sreekumaran Thampi || 
|-
| 4 || Madhuvidhuvin Maadhavamen || Vani Jairam || Sreekumaran Thampi || 
|-
| 5 || Vaarmudi Pinnitharaam || KP Brahmanandan || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 