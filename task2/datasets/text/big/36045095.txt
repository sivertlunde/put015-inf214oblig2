Sarocharu
 
 

{{Infobox film
| name           = Sarocharu
| image          = Sarocharu poster.jpg
| alt            =  
| caption        = Film poster
| director       = Parasuram
| producer       = Priyanka Dutt
| writer         = Parasuram    Chandra Mohan
| music          = Devi Sri Prasad
| cinematography = Vijay K Chakravarthy
| editing        = Kotagiri Venkateswara Rao Vyjayanthi  (Presents)  Three Angels Studio  Pvt. Ltd. 
| distributor    = Blue Sky  (Overseas)  
| released       =   }}
| runtime        = 138 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood romance romantic film Vyjayanthi banner. Devi Sri Prasad scored the soundtrack for the film. The film was released on 21 December 2012.  The movie received Positive - mixed reviews and was a Hit at the Box Office.    The film was dubbed into Tamil as Sir Vanthara which was released in 27 December 2013. 

== Plot ==
Sandhyaa Arun(Kajal Aggarwal) is a student who is madly in love with Karthik (Ravi Teja), a techie based in Italy. She decides to travel with him to India, confident that the travel time would be enough for her to get Karthik to fall in love. Karthik reciprocates her gestures in a friendly way, but finally reveals that he is married to Vasudha (Richa Gangopadhyay) but is waiting to seek Divorce.

Sandhya comes to know about Karthik’s stint in Subramaniapuram as a football coach and about his affair with Vasudha and that he wants to divorce her because of Incompatibility. She also realises that despite all that baggage, she still has feelings for Karthik. At this point, Karthik springs a surprise and reveals a shocking bit of information that he is not married and the story he narrated was his friend’s and told it so that it should not repeat in their life as he loved her too. This infuriates her to the core and leaves him breaking in tears.

Into this comes in Gautham (Nara Rohith), who is Sandhya’s brother in law and madly in love with her. Based on her Mother’s (Jayasudha) suggestion, she agrees to marry Gautham. Gautham observes that she still loves Karthik and goes to talk with him. After a conversation, he comes to know the Sincerity in Karthik’s love, he lets Sandhya realise it and lets her go to him on their Engagement Day. But all goes vain as Sandhya knows that Karthik left to Ooty leaving her. Finally she too goes to Ooty after 3 months and reunites with him.

== Cast ==
  was selected as lead heroine marking her second collaboration with Ravi Teja.]]
* Ravi Teja as Karthik
* Kajal Agarwal as Sandhya
* Richa Gangopadhyay as Vasudha
* Nara Rohith as Gautham
* Jayasudha as Sandhyas Mother Chandra Mohan as Sandhyas Father
* Raviprakash as Ravi
* M. S. Narayana as Platinum Prasad
* Srinivasa Reddy as Karthiks Friend
* Kalpika as Kalpika Devan

== Crew ==
* Fights: Ram – Lakshman
* Art: Sahi Suresh
* Dance choreography: Raju Sundaram, Dinesh Kumar (choreographer)|Dinesh, Lalitha
* Co-directors: Kiran, Bujji
* Production controller: V. Mohan Rao

== Production ==

=== Development === Anjaneyulu directed Trisha  Chief Minister Chandra Mohan were roped in to play important roles in the film.  In November, a set of stills and 2 First Look Posters were Launched  which got stunning response.  It was also reported later that the pair of Ravi Teja and Kajal will be seen in a very beautiful romantic track that is expected to be a highlight of the movie. 

=== Filming ===
Sarocharu, to be produced by Vyjayanthi Movies, will go on the floors from 16 June in Ooty  but postponed to 1 July  and again postponed to 8 July.  The new schedule of the film started on 29 July 2012.  In this flick, Ravi Teja plays the role of a Football Coach  and Richa tweeted her character name to be Vasudha. After wrapping up a schedule in Ooty, the unit members have flown to Italy and the film was shot for 12 days and later they moved to Germany for completing the schedule and there scenes Ft. Ravi Teja and Kajal Agarwal were shot.  During the shooting of the Schedule, It was reported that the film will be shot in exotic places of Italy and Switzerland.  In the end of September 2012, The song "Racha Rambola" was Shot in RFC, Hyderabad and it was reported that Richa is shaking leg along with Ravi Teja in the song and Richa loved the Song.  In November, it was declared that the film is in the last leg of shooting and except for a song, most part of the film has already been wrapped up.  In the end if November, the shooting of the film was wrapped up.  Leading distributors Blue Sky, who were associated with films like 100% Love, Gabbar Singh and Sri Rama Rajyam, purchased the overseas rights for an undisclosed price which is said to be the highest ever for Ravi Teja in overseas area.   

== Release ==
The film was awarded with a Clean U Certificate by Central Board of Film Certification on 19 December 2012.    The film was released on 21 December 2012.  The film was released in 450 screens worldwide.

=== Critical reception ===
The film received Positive - Mixed Reviews from Critics.
123telugu stated that " Overall, Sarocharu ends up being an OK watch. " and has given a rating 3/5 for the film.    SuperGoodMovies stated that " Go and Watch Sarocharu, It is just one time watchable movie because of Raviiteja. " and has given a rating 2.25/5 for the film.    Teluguone stated that " Sarocharu is a refreshing family entertainer which goes on a slow pace. Watch out for the lead actors performances. " and has given a rating 2.75/5 for the film.    Oneindia entertainment has given a rating 3/5 for the film.    Apherald stated that " Ravi teja sinked his ship yet again with Kajal over shadowing him in this fim justification for "Manchi Prema Katha Tho" got incomplete with multiple errors. " and has given a rating 2/5 for the film.   

=== Box office ===
The film has collected a share of   in Andhra Pradesh in its first weekend.    The film has collected $7,037 from 15 screens in the USA in its Second weekend and the per screen average is $469. The film has collected   in ten days in USA.    The film grossed   worldwide at the box office.
The film declared below average grosser in first week later it Hit at the box office.

=== Home media ===
The VCDs, DVDs and Blu-rays of the film were released on 3 May 2013 with prices of $9.99 per DVD and $19.99 per Blu-ray through the manufacturer Bhavani DVD.  

== Soundtrack ==
{{Infobox album 
| Name     = Sarocharu
| Type     = Soundtrack
| Artist   = Devi Sri Prasad
| Released =  
| Recorded = 2012
| Genre    = Film soundtrack
| Length   =   Telugu
| Producer = Devi Sri Prasad
| Label    = Aditya Music
| Last album = Dhamarukam (2012)
| This album = Sarocharu (2012)
| Next album = Bad Boy (2012)
}}

Producer Priyanka Dutt advised that Aditya Music purchased the audio rights and audio will be released through it for the film.    The audio of the film was released directly into market without audio launch on 5 December 2012.  Music of the film was composed by Devi Sri Prasad. The album consists of five songs. Lyrics for the two songs were penned by Ramajogayya Sastry and remaining songs were written by Srimani, Anantha Sreeram, Chandrabose (lyricist)|Chandrabose. Devi sri prasad himself sung a song in the album.

=== Reception ===
The album has received a good response. 123telugu stated that "Decent album from DSP".    Apherald.com gave a rating 2.5/5 for the album and stated that "Devi Sri Prasad’s magical tunes are missing to core in the audio of this film".   

{{Track listing
| lyrics_credits = yes
| extra_column   = Singer(s)
| total_length   = 21:07
| title1  = Made For Each Other
| extra1  = Devi Sri Prasad
| lyrics1 = Ramajogayya Sastry
| length1 = 4:26
| title2  = Jaga Jaga Jagadeka Veera
| extra2  = Venu, Ranina Reddy
| lyrics2 = Ramajogayya Sastry
| length2 = 4:47
| title3  = Raccha Rambola
| extra3  = Javed Ali, Rita
| lyrics3 = Srimani
| length3 = 4:19
| title4  = Gusa Gusa Sunitha
| lyrics4 = Anantha Sreeram
| length4 = 3:37
| title5  = Kaatuka Kallu
| extra5  = Khushi Murali, Swetha Mohan, Chinnaponnu
| lyrics5 = Chandrabose
| length5 = 3:58
}}

== References ==
 

== External links ==
*  

 

 
 
 
 