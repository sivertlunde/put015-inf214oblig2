Three Steps to the Gallows
{{Infobox film
| name           = Three Steps to the Gallows
| image          = Three Steps to the Gallows poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = John Gilling
| writer         = 
| narrator       = 
| starring       = Scott Brady Mary Castle
| music          = 
| cinematography = 
| editing        = 
| studio         = Tempean Films
| distributor    = Lippert Pictures (USA) Eros Films (UK)
| released       = 1 January 1954 (USA)
| runtime        = 81 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
 British crime film directed by John Gilling and starring Scott Brady, Mary Castle and Gabrielle Brune.  An American comes to London to attempt to save his brother from being hanged for a murder he didnt commit.

==Partial cast==
* Scott Brady - Gregor Stevens 
* Mary Castle - Yvonne Durante 
* Gabrielle Brune - Lorna Dreyhurst 
* Ferdy Mayne - Mario Satargo 
* Colin Tapley - Arnold Winslow 
* John Blythe - Dave Leary  Michael Balfour - Carter 
* Lloyd Lamble - James Smith
* Julian Somers - John Durante 
* Ballard Berkeley - Inspector Haley 
* Ronan OCasey - Crawson 
* Johnnie Schofield Charlie
* Paul Erickson - Larry Stevens 
* Hal Osmond - Desk clerk
* Ronald Leigh-Hunt - Captain Adams
* Dennis Chinnery - Bill

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 