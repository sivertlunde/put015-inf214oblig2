Under the Gaslight
{{infobox film
| name           = Under the Gaslight
| image          =
| imagesize      =
| caption        =
| director       = Lawrence Marston
| producer       = Klaw & Erlanger Biograph Company
| based on       =  
| writer         =  William Russell
| music          =
| cinematography =
| editing        =
| distributor    = The General Film Company
| released       = December 1914 reels
| country        = United States
| language       = Silent film (English intertitles)
}} lost 1914 silent film melodrama produced by the Biograph Company, for theatrical impresarios Klaw & Erlanger, and distributed by The General Film Company. It is based on the old Victorian stage melodrama by Augustin Daly popular in the 1860s and 1870s and revived periodically for years afterwards. This film was directed by Lawrence Marston and stars Lionel Barrymore .  

==Cast==
*Lionel Barrymore - William Byke William Russell - Ray Trafford
*Irene Howley - Pearl Courtland
*Millicent Evans - Laura Courtland
*Isabel Rea - Mrs. Courtland
*Thomas Jefferson - Mr. Courtland
*Hector Sarno - Snorky (*Hector V. Sarno)
*Zoe Gregory - Blossom
*Maurice Steuart - Laura as a child
*Rosanna Logan - Pearl as a child

unbilled
*Mrs. A.C. Marston - Judas

==Cast note==
In 1874 Maurice Barrymore, father of the star of the movie, arrived in the United States and joined Augustin Dalys stage company playing a role, Ray Trafford, in this play. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 


 