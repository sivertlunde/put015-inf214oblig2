Julius Caesar (1953 film)
{{Infobox film
| name           = Julius Caesar
| image          = Julius caesar.jpeg
| image_size     =
| caption        = Theatrical release poster
| director       = Joseph L. Mankiewicz
| producer       = John Houseman
| writer         = Joseph L. Mankiewicz William Shakespeare  
| starring       = Marlon Brando James Mason John Gielgud Louis Calhern Edmond OBrien Greer Garson Deborah Kerr
| music          = Miklós Rózsa
| cinematography = Joseph Ruttenberg John Dunning
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         =$2,070,000  . 
| gross          = $3,920,000 
}} play by William Shakespeare|Shakespeare, directed by Joseph L. Mankiewicz, who also wrote the uncredited screenplay, and produced by John Houseman. The original music score is by Miklós Rózsa.  The film stars Marlon Brando as Mark Antony, James Mason as Marcus Junius Brutus|Brutus, John Gielgud as Gaius Cassius Longinus|Cassius, Louis Calhern as Julius Caesar, Edmond OBrien as Servilius Casca|Casca, Greer Garson as Calpurnia Pisonis|Calpurnia, and Deborah Kerr as Portia (Julius Caesar)|Portia.

==Casting== Shakespeare Memorial 1970 film Richard Johnson Broadway version of the play starring Orson Welles and the Mercury Theatre, also produced the MGM film. By this time, however, Welles and Houseman had had a falling out, and Welles had nothing to do with the 1953 film. P. M. Pasinetti, Italian-American writer, scholar, and teacher at UCLA served as a technical advisor.

Brandos casting was met with some skepticism when it was announced, as he had acquired the nickname of "The Mumbler" following his performance in  , a proposition that Brando turned down.   During filming, James Mason became concerned that Brando was stealing the audiences sympathy away from him and his character, Brutus, so Mason appealed to Mankiewicz, with whom he had bonded earlier while making the film 5 Fingers, requesting that the director stop Brando from dominating the film and "put the focus back where it belongs. Namely on me!"     The subsequent shift in directorial attention didnt escape Brando, who threatened to walk off the film if Mankiewicz "threw one more scene to Mason", alleging a ménage à trois  between Mankiewicz, Mason and Masons wife Pamela Mason|Pamela.   Despite the feuding, production continued with only minimal disruption, thanks to what Gielgud called, "Mankiewiczs consummate tact that kept us together as a working unit."   
 Cinna the Poet in the film and not receiving screen credit, but his one scene was deleted before release, and it is not included in any DVD or video releases of the film. (However, Cinna the Conspirator does appear; he is played by actor William Cottrell.)

==Cast==
:: 
  
* Marlon Brando as Mark Antony Mark Antony|  Brutus Marcus Junius Brutus the Younger|  Cassius Gaius Cassius Longinus| 
* Louis Calhern as Julius Caesar Julius Caesar|  Casca Servilius Casca|  Calpurnia Calpurnia Pisonis|  Portia Porcia Catonis|  Marullus Gaius Epidius Marullus|  Flavius Lucius Caesetius Flavus| 
* Richard Hale as a Soothsayer
* Alan Napier as Cicero Cicero|  Decius Brutus Decimus Junius Brutus Albinus|  Metellus Cimber Tillius Cimber|  Cinna Lucius Cornelius Cinna|  Trebonius Gaius Trebonius|  Ligarius Quintus Ligarius| 
* Morgan Farley as Artemidorus Bill Phipps as Servant to Antony Octavius Caesar Augustus|  Lepidus Marcus Aemilius Lepidus|  Rhys Williams as Lucilius
* Michael Ansara as Pindarus Messala Marcus Valerius Messalla| 
* Edmund Purdom as Strato
* John Doucette as a Carpenter
* John Hardy as Lucius
* Chester Stratton as Servant to Caesar
* Lumsden Hare as Publius
* Preston Hanson as Claudius
* Victor Perry as Popilius Lena
* Michael Tolan as Officer to Octavius
* John Lupton as Varro
* Joe Waring as Clitus
* John Parrish as Titinius
* Stephen Roberts as Dardanius Paul Guilfoyle as a Citizen of Rome
* Lawrence Dobkin as a Citizen of Rome
* Jo Gilbert as a Citizen of Rome
* David Bond as a Citizen of Rome
* Ann Tyrrell as a Citizen of Rome
* John OMalley as a Citizen of Rome
* Oliver Blake as a Citizen of Rome
* Alvin Hurwitz as a Citizen of Rome
* Donald Elson as a Citizen of Rome
 
===Dramatis personae===
in the order of their appearance
*John Doucette … a Carpenter
*George Macready .. Marullus
*Michael Pate ….. Flavius
*Louis Calhern . Julius Caesar
*Edmond OBrien …… Casca
*Greer Garson …. Calpurnia
*Deborah Kerr …… Portia
*Marlon Brando . Mark Antony
*James Mason …… Brutus
*John Gielgud ……. Cassius
*Richard Hale … a Soothsayer
*Alan Napier ……. Cicero
*William Cottrell …. Cinna
*John Hardy …….. Lucius
*John Hoyt … Decius Brutus
*Tom Powers . Metellus Cimber
*Jack Raine …… Trebonius
*Ian Wolfe …….. Ligarius
*Chester Stratton . Servant to Caesar
*Lumsden Hare …… Publius
*Morgan Farley … Artemidorus
*Victor Perry … Popilius Lena Bill Phipps .. Servant to Antony
*Michael Tolan . Officer to Octavius
*Douglas Watson . Octavius Caesar
*Douglass Dumbrille … Lepidus Rhys Williams ….. Lucilius
*Michael Ansara …. Pindarus
*Dayton Lummis ….. Messala
*John Lupton ……. Varro
*Preston Hanson … Claudius
*John Parrish ….. Titinius
*Joe Waring …….. Clitus
*Stephen Roberts .. Dardanius
*Thomas Browne Henry . Volumnius
*Edmund Purdom …. Strato
and as citizens of Rome
*Paul Guilfoyle&nbsp; &nbsp;Lawrence Dobkin
*David Bond&nbsp; &nbsp;Jo Gilbert&nbsp; &nbsp;Ann Tyrrell
*John OMalley&nbsp; &nbsp;Oliver Blake
*Alvin Hurwitz&nbsp; &nbsp;Donald Elson
 

==Reception== David Shipman pointed to Gielgud "negotiating the verse as in no other Shakespeare film to date except Oliviers".  The film currently has a 100% approval rating on Rotten Tomatoes  indicating universal acclaim.
===Box office===
According to MGM records the film earned $2,021,000 in the US and Canada and $1,899,000 elsewhere, resulting in a profit of $116,000. 

==Music==
Intrada Records released an album featuring a 1995 re-recording of the films score. The re-recording was performed by the Sinfonia of London and conducted by Bruce Broughton. 
{{Tracklist
| collapsed = yes
| headline  = Intrada Album
| title1    = Julius Caesar Overture 
| length1   = 3:15
| title2    = Praeludium 
| length2   = 3:38
| title3    = Caesars Procession 
| length3   = 2:45
| title4    = Flavius Arrested 
| length4   = 0:18
| title5    = Feast Of Lupercal 
| length5   = 0:44
| title6    = Caesar And His Train 
| length6   = 0:51
| title7    = The Scolding Winds 
| length7   = 2:42
| title8    = Brutus Soliloquy 
| length8   = 6:34
| title9    = Brutus Secret 
| length9   = 2:11
| title10   = They Murder Caesar 
| length10  = 1:08
| title11   = The Ides Of March 
| length11  = 4:36
| title12   = Black Sentence 
| length12  = 3:55
| title13   = Brutus Camp 
| length13  = 1:31
| title14   = Heavy Eyes 
| length14  = 1:47
| title15   = Gentle Knave 
| length15  = 2:07
| title16   = Ghost Of Caesar 
| length16  = 1:42
| title17   = Most Noble Brutus 
| length17  = 1:10
| title18   = Battle At Philippi 
| length18  = 1:28
| title19   = Titinius Enclosed 
| length19  = 0:40
| title20   = Caesar Now Be Still! 
| length20  = 8:54
| title21   = Finale 
| length21  = 1:10
}}

==Awards and nominations==
 
  Best Actor Best Cinematography, Best Music, Best Picture. Best Actor A Streetcar Named Desire and in 1952 for Viva Zapata!. He would win the following year for On the Waterfront.
 BAFTA awards Best British Actor (John Gielgud) and Best Foreign Actor Best Film BAFTA Best Actor award in three consecutive years for Viva Zapata! (1952), Julius Caesar (1953),  and On the Waterfront (1954).

It won the Best Film and Best Actor Award for James Mason from The National Board of Review. It also won the Golden Leopard at the Locarno International Film Festival.   

==Julius Caesar on Turner Classic Movies==
Julius Caesar has had regular showings on Turner Classic Movies. On August 11, 2014, it was one of the features spotlighting Marlon Brando in the annual "Summer Under the Stars" series and, on February 15, 2015, was screened as part of another annual tradition, "31 Days of Oscar".
===Introductory comments (August 11, 2014)=== The Men A Streetcar Named Desire and Viva Zapata! So imagine the jokes that went around when it was announced that film number four for Marlon Brando was gonna be a movie version of Shakespeares Julius Caesar. Brando, the mumbler, speaking Shakespeare? I mean — ya gotta be kidding. But, indeed, soon up there on the big screen, was Brando playing Mark Antony, delivering the famous funeral oratory, for his great assassinated friend, Julius Caesar, speaking his lines clearly and brilliantly, and surprising everybody yet again.

Well, our movie has many assets beyond Marlon Brando. Julius Caesar was directed by Joseph Mankiewicz and boasts an all-star cast that includes James Mason, John Gielgud, Edmond OBrien and Louis Calhern. And, for the first and only time in the same film, Greer Garson who, after ten years as the queen of the MGM studios, was on her way out, and Deborah Kerr, Garsons successor as MGMs first lady was even more solidly in. This was quite a defining film for Marlon Brando, particularly though. It showed everybody that Brando was indeed an actor capable of classical work as well as so-called kitchen-sink dramas which were so popular at that time. Well, heres the movie which MGM premiered in New York not in a regular cinema house, but a legit theater, just like a stage play. Heres Julius Caesar."

===Robert Osbornes closing comments (August 11, 2014)=== Quo Vadis, The King Separate Tables The Formula (1980)] 

==See also==
*List of historical drama films
*List of films set in ancient Rome
*List of films based on military books (pre-1775)

==References==
 

==External links==
 
* 
* 
* 
* 
*  at Box Office Mojo 
* 
*  at TV Guide (1987 write-up was originally published in The Motion Picture Guide)

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 