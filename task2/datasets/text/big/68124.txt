American Graffiti
 
{{Infobox film
| name                 = American Graffiti
| image                = American graffiti ver1.jpg
| caption              = Theatrical release poster by Mort Drucker
| director             = George Lucas
| producer             = Francis Ford Coppola
| writer               = {{Plain list|
* George Lucas
* Gloria Katz
* Willard Huyck
}}
| starring             = {{Plain list|
* Richard Dreyfuss
* Ronny Howard
* Paul Le Mat Charlie Martin Smith
* Candy Clark
* Mackenzie Phillips
* Cindy Williams
* Wolfman Jack
* Suzanne Somers
 
}}
| cinematography       = {{Plain list|
* Ron Eveslage Jan DAlquen
}}
| editing              = {{Plain list|
* Verna Fields
* Marcia Lucas
}}
| production companies = {{Plain list| Lucasfilm Ltd
* Coppola Company
}}
| distributor          = Universal Pictures
| released             =  
| runtime              = 112 minutes
| country              = United States
| language             = English
| budget               = $777,000 
| gross                = $140 million   
}}
 cruising and rock and roll cultures popular among the post–World War II baby boom generation. The film is told in a series of vignette (literature)|vignettes, telling the story of a group of teenagers and their adventures in one night.
 pitching the Universal Pictures after United Artists, 20th Century Fox, Columbia Pictures, Metro-Goldwyn-Mayer, Warner Bros., and Paramount Pictures turned him down. Filming was initially set to take place in San Rafael, California, but the production crew was denied permission to shoot beyond a second day. As a result, most filming was done in Petaluma, California|Petaluma.

American Graffiti premiered on August 2, 1973 at the Locarno International Film Festival in Switzerland and was released on August 11, 1973 in the United States. It received widespread critical acclaim and was nominated for the Academy Award for Best Picture. Produced on a $777,000 budget,  it has become one of the most profitable films of all time. Since its initial release, American Graffiti has garnered an estimated return of well over $200 million in box office gross and home video sales, not including merchandising. In 1995, the United States Library of Congress deemed the film "culturally, historically, or aesthetically significant" and selected it for preservation in the National Film Registry.

==Plot== Chevy Impala for the evening, and while he will be away at college. Steves girlfriend, Laurie, who also is Curts younger sister, is unsure of Steves leaving, to which he suggests they see other people while he is away to "strengthen" their relationship.
 back to greasers ("The Pharaohs") through an initiation rite that involves hooking a chain to a police car and successfully ripping out its back axle. Curt is told rumors that "The Blonde" is either a trophy wife or prostitute, which he immediately refuses to accept.

Steve and Laurie break up following a series of arguments, and John inadvertently picks up Carol, an annoying teenybopper who seems fond of him.  Toad, who is normally socially inept with girls, meets a flirting|flirtatious, and somewhat rebellious, girl named Debbie.  Meanwhile, Curt learns that DJ Wolfman Jack broadcasts from just outside of Modesto. Inside the dark, eerie radio station, Curt encounters a bearded man he assumes to be the manager. Curt hands the man a message for "The Blonde" to call or meet him.  As he walks away, Curt hears the voice of The Wolfman, and, having just seen The Wolfman broadcasting, he realizes he had been speaking with The Wolfman.

The other story lines intertwine until Toad and Steve end up on "Paradise Road" to watch John race against the handsome (but arrogant) Bob Falfa, with Laurie as Bobs passenger. Within seconds Bob loses control of his car after blowing a front tire, plunges into a ditch and rolls his car. Steve and John run to the wreck, and a dazed Bob and Laurie stagger out of the car before it explodes. Distraught, Laurie grips Steve tightly and tells him not to leave her. He assures her that he has decided not to leave Modesto after all. The next morning Curt is awakened by the sound of a phone ringing in a telephone booth, which turns out to be "The Blonde". She tells him she might see him cruising tonight, but Curt replies that is not possible, because he will be leaving. At the airfield he says goodbye to his parents, his sister, and friends. As the plane takes off, Curt, gazing out of the window, sees the white Ford Thunderbird belonging to the mysterious blonde.

Prior to the end credits an on-screen epilogue reveals that John was killed by a drunk driver in December 1964, Toad was reported missing in action near An Lộc in December 1965, Steve is an insurance agent in Modesto, California, and Curt is a writer living in Canada.

==Cast==
*Richard Dreyfuss as Curt Henderson
*Ron Howard as Steve Bolander
*Paul Le Mat as John Milner
*Charles Martin Smith as Terry "The Toad" Fields
*Cindy Williams as Laurie Henderson
*Candy Clark as Debbie Dunham
*Mackenzie Phillips as Carol Morrison XERB Disc Jockey
*Bo Hopkins as Joe Young
*Manuel Padilla, Jr. as Carlos
*Beau Gentry as Ants
*Harrison Ford as Bob Falfa
*Jim Bohan as Officer Holstein
*Jana Bellan as Budda
*Deby Celiz as Wendy
*Lynne Marie Stewart as Bobbie Tucker Terry McGovern as Mr. Wolfe
*Kathleen Quinlan as Peg
*Scott Beach as Mr. Gordon John Brent as Car Salesman
*Del Close as Man at Bar (Guy)
*Johnny Weissmuller, Jr. as Badass #1
*Susan Richardson as Judy
*Kay Lenz as Jane
*Joe Spano as Vic
*Debralee Scott as Falfas Girl
*Suzanne Somers as "The Blonde" in T-Bird
*Flash Cadillac & the Continental Kids as Herbie and the Heartbeats

==Development==

===Inspiration=== cruising in my generation used as a way of meeting girls," Lucas explained.  As he developed the story in his mind, Lucas included his fascination with Wolfman Jack. Lucas had considered doing a documentary about the Wolfman when he attended the USC School of Cinematic Arts, but dropped the idea. Baxter, pp. 70, 104, 148, 254 

Adding in semi-autobiographical connotations, Lucas set the story in his hometown of 1962 Modesto.  The characters Curt Henderson, John Milner and Terry "The Toad" Fields also represent different stages from his younger life. Curt is modeled after Lucass personality during USC, while Milner is based on Lucass teenage street racing and junior college years, and hot rod enthusiasts he had known from the Kustom Kulture in Modesto. Toad represents Lucass nerd years as a freshman in high school, specifically his "bad luck" with dating.  The filmmaker was also inspired by Federico Fellinis I Vitelloni (1953). Baxter, pp. 106–118 

After the financial failure of THX 1138, Lucas wanted the film to act as a release for a world-weary audience: Sturhahn, Larry (March 1974). "The Filming of American Graffiti". Filmmakers Newsletter. 

 

===United Artists=== pitching the American Graffiti treatment to various Hollywood studios and production companies in an attempt to secure the financing needed to expand it into a screenplay,  but they were unsuccessful. The potential financiers were concerned that music licensing costs would cause the film to go way over budget. Along with  Easy Rider (1969), American Graffiti was one of the first films to eschew a traditional film score and successfully rely instead on synchronizing a series of popular hit songs with individual scenes.   

THX 1138 was released in March 1971 Hearn, pp. 10–11, 42–47  and Lucas was offered opportunities to direct Lady Ice (1973), Tommy (1975 film)|Tommy (1975) or Hair (film)|Hair (1979). He turned down those offers, determined to pursue his own projects despite his urgent desire to find another film to direct.   During this time, Lucas conceived the idea for a space opera (as yet untitled) which later became the basis for his Star Wars franchise. At the May 1971 Cannes Film Festival, THX was chosen for the Directors Fortnight competition. There, Lucas met David Picker, then president of United Artists, who was intrigued by American Graffiti and Lucass space opera. Picker decided to give Lucas $10,000 to develop Graffiti as a screenplay. 
 Richard Walter, East Coast West Coast teenagers in the early 1960s. Walter was paid the $10,000, and he began to expand the Lucas/Huyck/Katz treatment into a screenplay. 
 playing chicken and things that kids didnt really do," Lucas explained. "I wanted something that was more like the way I grew up."  Walters script also had Steve and Laurie going to Nevada to get married without their parents permission.  Walter rewrote the screenplay, but Lucas nevertheless fired him due to their creative differences. 

After paying Walter, Lucas had exhausted his development fund from United Artists. He began writing a script, completing his first draft in just three weeks. Drawing upon his large collection of vintage records, Lucas wrote each  scene with a particular song in mind as its musical backdrop. Hearn, pp. 52–53  The cost of licensing the 75 songs Lucas wanted was one factor in United Artists ultimate decision to reject the script; the studio also felt it was too experimental—"a musical montage with no characters". United Artists also passed on  , which Lucas shelved for the time being. 

===Universal Pictures=== Mill Valley Los Angeles. Lucas also intended to end American Graffiti showing a title card detailing the fate of the characters, including the death of Milner and the disappearance of Toad in Vietnam. Huyck and Katz found the ending depressing and were incredulous that Lucas planned to include only the male characters. Lucas argued that mentioning the girls meant adding another title card, which he felt would prolong the ending. Because of this, Pauline Kael later accused Lucas of chauvinism. 
 Universal Pictures, who allowed Lucas total artistic control and the right of final cut privilege on the condition that he make American Graffiti on a strict, low budget.  This forced Lucas to drop the opening scene, in which the Blonde Angel, Curts image of the perfect woman, drives through an empty drive-in cinema in her Ford Thunderbird, her transparency revealing she does not exist. 

Universal initially projected a $600,000 budget, but added an additional $175,000 once producer Francis Ford Coppola signed on. This would allow the studio to advertise American Graffiti as "from the Man who Gave you The Godfather (1972)". However, Lucas was forced to concede final cut privilege. The proposition also gave Universal first look deals on Lucass next two planned projects, Star Wars (1977) and Radioland Murders (1994).  As he continued to work on the script, Lucas encountered difficulties on the Steve and Laurie storyline. Lucas, Katz and Huyck worked on the third draft together, specifically on the scenes featuring Steve and Laurie. 

Production proceeded with virtually no input or interference from Universal. American Graffiti was a low-budget film, and executive Ned Tanen had only modest expectations of its commercial success. However, Universal did object to the films title, not knowing what "American Graffiti" meant;  Lucas was dismayed when some executives assumed he was making an Italian movie about feet.  The studio therefore submitted a long list of over 60 alternative titles, with their favorite being Another Slow Night in Modesto  and Coppolas Rock Around the Block.  They pushed hard to get Lucas to adopt any of the titles, but he was displeased with all the alternatives and persuaded Tanen to keep American Graffiti. Hearn, pp. 58–60 

==Production==

===Casting===
The films lengthy casting process was overseen by Fred Roos, who worked with producer Francis Ford Coppola on The Godfather.  Because American Graffitis main cast was associated with younger actors, the casting call and notices went through numerous high school drama groups and community theaters in the San Francisco Bay Area.  Among the actors was Mark Hamill, the future Luke Skywalker in Lucas Star Wars trilogy. 

Over 100 unknown actors auditioned for Curt Henderson before Richard Dreyfuss was cast. George Lucas was impressed with Dreyfuss thoughtful analysis of the role,  and, as a result, offered the actor his choice of Curt or Terry "The Toad" Fields.  Roos, a former casting director on The Andy Griffith Show, suggested Ron Howard for Steve Bolander. Howard accepted the role to break out of the mold of his career as a child actor. Hearn, pp. 56–57  Howard later appeared in the very similar role of Richie Cunningham on the Happy Days sitcom. {{cite web|url=https://books.google.com/books?id=cj5LfIhSaoIC&pg=PA184|title=Last Season of Innocence: The Teen Experience in the 1960s
 By Victor Brooks|quote=Happy Days began airing only a few months after Graffiti came out, and much of the plotline revolved around Ron Howards character, Richie Cunningham, who was almost an exact clone of Steven in the film.}}  Bob Balaban turned down The Toad out of fear of typecasting, a decision which he later regretted. Charles Martin Smith, who, in his first year as a professional actor, had already appeared in two feature films including 20th Century Foxs The Culpepper Cattle Co., and 4 TV episodes, was eventually cast in the role. 

Although Cindy Williams was cast as Laurie Henderson, the actress hoped she would get the part of Debbie Dunham, which ended up going to Candy Clark.  Mackenzie Phillips, who portrays Carol, was only 12 years old, and under California law, producer Gary Kurtz had to become her legal guardian for the duration of filming. Baxter, pp. 124–128  As Bob Falfa, Roos cast Harrison Ford, who was then concentrating on a carpentry career. Ford agreed to take the role on the condition that he would not have to cut his hair. The character has a flattop haircut in the script, but a compromise was eventually reached whereby Ford wore a stetson to cover his hair. Producer Francis Ford Coppola encouraged Lucas to cast Wolfman Jack as himself in a cameo appearance. "George Lucas and I went through thousands of Wolfman Jack phone calls that were taped with the public," Jack reflected. "The telephone calls   in the motion picture and on the soundtrack were actual calls with real people." 

Charles Martin Smith (18) and Ron Howard (18) were the only two real teenage principal actors of the film.  Most of the remaining principal cast members were in their 20s with the exceptions of the 12-year-old Mackenzie Phillips, and Harrison Ford, who turned 30 during filming.

===Filming=== San Rafael as the primary shooting location.  Filming began on June 26, 1972. However, Lucas soon became frustrated at the time it was taking to fix camera mounts to the cars.  A key member of the production had also been arrested for growing marijuana, Pollock, pp. 105–111  and, in addition to already running behind the shooting schedule, the San Rafael City Council immediately became concerned about the disruption that filming caused for local businesses and therefore withdrew permission to shoot beyond a second day. 

Petaluma, California|Petaluma, a similarly small town approximately 20 miles north of San Rafael, became more cooperative and American Graffiti moved there without the loss of a single day of shooting. Lucas convinced the San Rafael City Council to allow two further nights of filming for general cruising shots, which he used to evoke as much of the intended location as possible in the finished film. Shooting in Petaluma began on June 28 and proceeded at a quick pace.  Lucas mimicked the filmmaking style of B movie producer Sam Katzman in attempting to save money and authenticated low-budget filming methods. 
 Van Ness Mill Valley. 

More problems ensued during filming: Paul Le Mat was sent to the hospital after an allergic reaction to walnuts. Le Mat, Harrison Ford, and Bo Hopkins were often drunk between takes, and had conducted climbing competitions to the top of the local Holiday Inn sign. One actor set fire to Lucas motel room. Another night, Le Mat threw Richard Dreyfuss into a swimming pool, gashing Dreyfuss forehead on the day before he was due to have his close-ups filmed. Dreyfuss also complained over the wardrobe that Lucas had chosen for the character. Ford was arrested one night while in a bar fight and kicked out of his motel room. In addition, two camera operators were nearly killed when filming the climactic race scene on Frates Road outside Petaluma. Baxter, pp. 129–135  Principal photography ended on August 4, 1972. 
 Douglas DC-7C airliner of Magic Carpet Airlines which had previously been leased from owner Club America Incorporated by the rock band Grand Funk Railroad from March 1971 to June 1971.     

===Cinematography=== 16 mm. improvise scenes. He also used goofs for the final cut, notably Charles Martin Smiths (Toad) arriving on his scooter to meet Steve outside Mels Drive-In.  Jan DAlquen and Ron Eveslage were hired as the cinematographers, but filming with Techniscope cameras brought lighting problems. As a result, Lucas commissioned help from friend Haskell Wexler, who was credited as the "visual consultant". Hearn, pp. 61–63 

===Editing=== audio mixing and sound design purposes. Hearn, pp. 64–66  Murch suggested making Wolfman Jacks radio show the "backbone" of the film. "The Wolfman was an ethereal presence in the lives of young people," said producer Gary Kurtz, "and it was that quality we wanted and obtained in the picture." 

==Soundtrack==
 
Lucass choice of background music was crucial to the mood of each scene, but he was realistic about the complexities of copyright clearances and suggested a number of alternative tracks. Universal wanted Lucas and producer Gary Kurtz to hire an orchestra for sound-alikes. The studio eventually proposed a flat deal that offered every music publisher the same amount of money. This was acceptable to most of the companies representing Lucass first choices, but not to RCA - with the consequence that Elvis Presley is conspicuous by his absence from the soundtrack.  Clearing the music licensing rights had cost approximately $90,000,  and as a result there was no money left for a traditional film score. "I used the absence of music, and sound effects, to create the drama," Lucas later explained. 

A soundtrack album for the film, 41 Original Hits from the Soundtrack of American Graffiti, was issued by MCA Records. The album contains all the songs used in the film (with the exception of "Gee" by the Crows, which was subsequently included on a second soundtrack album), presented in the order in which they appeared in the film.

==Reception==

===Release===
Despite unanimous praise at a January 1973 test screening attended by Universal executive Ned Tanen, the studio told Lucas they wanted to re-edit his original cut of American Graffiti. Hearn, pp. 67–69  Producer Coppola sided with
Lucas against Tanen and Universal, offering to "buy the film" from the studio and reimburse it for the $775,000 (equivalent to $ |r=0}}}} as of  )  it had cost to make it.  20th Century Fox and Paramount Pictures made similar offers to the studio.  Universal refused these offers and told Lucas they planned to have William Hornbeck re-edit the film. 
 Some Enchanted Evening"—but decided that the film was fit for release only as a television movie. 

However, various studio employees who had seen the film began talking it up, and its reputation grew through word of mouth.  The studio dropped the TV movie idea and began arranging for a limited release in selected theaters in Los Angeles and New York.  Universal presidents Sidney Sheinberg and Lew Wasserman heard about the praise the film had been garnering in LA and New York, and the marketing department amped up their promotion strategy for it,  investing an additional $500,000 (equivalent to $ |r=0}}}} as of  )  in marketing and promotion.  The film was released in the United States on August 11, 1973  to sleeper hit reception.  The film had cost only $1.27 million (equivalent to $ |r=0}}}} as of  )  to produce and market, but yielded worldwide box office gross revenues of more than $55 million (equivalent to $ |r=0}}}} as of  ) .  It had only modest success outside the United States, but became a  cult film in France. 

Universal reissued Graffiti in 1978 and earned an additional $63 million (equivalent to $ |r=0}}}} as of  ) , which brought the total revenue for the two releases to $118 million (equivalent to $ |r=0}}}} as of  ) .  The reissue included stereophonic sound,  and the additional four minutes that the studio had removed from Lucass original cut. All home video releases also included these scenes.  Also, the date of John Milners death was changed from June 1964 to December 1964 to fit the narrative structure of the upcoming sequel More American Graffiti. At the end of its theatrical run, American Graffiti had one of the lowest cost-to-profit ratios of a motion picture ever.  Producer Francis Ford Coppola regretted having not financed the film himself. Lucas recalled, "He would have made $30 million (equivalent to $ |r=0}}}} as of  )  on the deal. He never got over it and he still kicks himself." Pollock, pp. 120–128  It was the thirteenth-highest grossing film of all time in 1977,    and, adjusted for inflation, is currently the forty-third highest.  By the 1990s, American Graffiti had earned more than $200 million (equivalent to $ |r=0}}}} as of  )  in box office gross and home video sales.  In December 1997 Variety (magazine)|Variety reported that the film had earned an additional $55.13 million in rental revenue (equivalent to $ |r=0}}}} as of  ) . 

Universal Studios Home Entertainment first released the film on DVD in September 1998,  and once more as a double feature with More American Graffiti (1979) in January 2004. 

Aside from the four minutes originally deleted from Lucas original cut retained, the only major change in the DVD version is the main title sequence, particularly the sky background to Mels Drive-In, which was redone by Industrial Light and Magic|ILM.

Universal released the film on Blu-ray on May 31, 2011.  

===Critical analysis===
American Graffiti went on to receive widespread critical acclaim. Based on 33 reviews collected by  s, American Graffiti is a funny, nostalgia|nostalgic, and bittersweet look at a group of recent high school grads last days of innocence."  Roger Ebert praised the film for being "not only a great movie but a brilliant work of historical fiction; no sociological treatise could duplicate the movies success in remembering exactly how it was to be alive at that cultural instant". 

Jay Cocks of Time (magazine)|Time magazine wrote that American Graffiti "reveals a new and welcome depth of feeling. Few films have shown quite so well the eagerness, the sadness, the ambitions and small defeats of a generation of young Americans."  A.D. Murphy from Variety (magazine)|Variety felt American Graffiti was a vivid "recall of teenage attitudes and morals, told with outstanding empathy and compassion through an exceptionally talented cast of unknown actors".  Dave Kehr, writing in the Chicago Reader, called the film a brilliant work of popular art that redefined nostalgia as a marketable commodity, while establishing a new narrative style. 

===Themes=== American Pie" counterculture movement. American Graffiti evokes mankinds relationship with machines, notably the elaborate number of hot rods - having been called a "classic car flick", representative of the motor cars importance to American culture at the time it was made.   Another theme is teenagers obsession with radio, especially with the inclusion of Wolfman Jack and his mysterious and mythological faceless (to most) voice.

===Accolades=== Best Director Best Original Best Supporting Best Film Best Motion Most Promising Best Director Best Actor Best Actress Directors Guild Best Original Comedy. 

==Legacy==
Internet reviewer MaryAnn Johanson acknowledged that American Graffiti rekindled public and entertainment interest in the 1950s and 1960s, and influenced other films such as   (1977). Hearn, pp. 70–75 

The financial success of Graffiti also gave Lucas opportunities to establish more elaborate development for Lucasfilm, Skywalker Sound, and Industrial Light & Magic.  Based on the success of the 1977 reissue, Universal began production for the sequel More American Graffiti (1979).  Lucas and writers, Willard Huyck and Gloria Katz, later collaborated on Radioland Murders (1994), also released by Universal Pictures, for which Lucas acted as executive producer. The film features characters intended to be Curt and Laurie Hendersons parents, Roger and Penny Henderson. Hearn, pp. 79–86, 122  In 1995 American Graffiti was deemed culturally, historically, or aesthetically significant by the United States Library of Congress and selected for preservation in the National Film Registry.  In 1997 the city of Modesto, California, honored Lucas with a statue dedication of American Graffiti at George Lucas Plaza. 
 deuce coupe,  while Dexs Diner is reminiscent of Mels Drive-In.  Adam Savage and Jamie Hyneman of MythBusters conducted the "rear axle" experiment on the January 11, 2004, episode. 
 customizers and hot rodders in the years since its release, their fate immediately after the film is ironic. All were offered for sale in San Francisco newspaper ads; only the Chevrolet Impala#First generation (1958)|58 Impala (driven by Ron Howard) attracted a buyer, selling for only a few hundred dollars. The yellow Deuce and the white Ford Thunderbird|T-bird went unsold, despite being priced as low as US$3,000.  The registration plate on Milners yellow, deuce coupe is THX&nbsp;138 on a yellow, California license plate, slightly altered, reflecting Lucass earlier science fiction film.

==References==

===Footnotes===
 


===Bibliography===
* 
* 
* 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  

{{Navboxes|list1=
 
 
 
 
 
}}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 