Evergreen (film)
{{Infobox film
| name           = Evergreen
| image          =  EvergreenFilm.jpg
| image_size     =
| director       = Victor Saville
| writer         = Marjorie Gaffney Emlyn Williams Benn W. Levy  
| starring       = Jessie Matthews Sonnie Hale Barry MacKay Betty Balfour
| producer       = Michael Balcon
| cinematography = Glen MacWilliams
| music          = Richard Rodgers Lorenz Hart Harry M. Woods
| distributor    = Gaumont British
| released       =  
| runtime        = 90 minutes
| language       = English
| country        = United Kingdom
| budget         = 
}} musical Ever Green, also starring Matthews.  Matthews had a dual role as both mother and daughter.  

The film was produced by Michael Balcon and directed by Victor Saville. The music was by Rodgers and Hart. 

==Plot==
Set in the Edwardian era music halls of London, the popular singing star Harriet Green (Jessie Matthews) delights audiences with her coy rendition of Daddy Wouldnt Buy Me a Bow Wow; however she has an illegitimate baby daughter that she keeps secret from the public. Blackmailed into leaving the stage she moves to South Africa to raise her daughter quietly. Years later her daughter, Harriet Hawkes (played by Matthews again), looking remarkably like her mother, returns to London as a young show-biz hopeful. A handsome young publicity man Tommy Thompson (Barry MacKay), convinces a theater producer (Sonnie Hale) to star her in a new revue as the "remarkably preserved" original Harriet Green. The ruse works; however Harriet and Tommy have fallen in love, but the public believes Harriet is a well-preserved 60-year-old and Tommy is her son. The masquerade is revealed when Harriet does a strip-dance to the Harry M. Woods song Over My Shoulder.

==Production== West End production of Rodgers and Harts Ever Green musical, producer Michael Balcon engaged her for Emlyn Williams film adaptation of Benn W. Levys stage play. Harry M. Woods added four songs, dropping a number of the original Rodgers and Harts numbers. 
 Palace Theatre, and wanted to appear with Matthews. Contemporary reviews commented that such a partnership would be popular with critics and public. 

Even though Matthews was at the peak of her popularity at the time, she was near to a mental breakdown during the making of the film.  She credits director Victor Saville in her autobiography Over My Shoulder with giving her the support needed to complete the filming.

==Cast==
 

* Jessie Matthews as Harriet Green
* Sonnie Hale as Leslie Benn
* Betty Balfour as Maudie Barry MacKay as Tommy Thompson
* Ivor McLaren as Marquis of Staines
* Hartley Power as Treadwell
* Patrick Ludlow as Lord Shropshire
* Betty Shale as Mrs Hawkes
* Marjorie Brooks as Marjorie Moore
* Charles Mortimer as Butler

Jessie Matthews the popular English actress, dancer, and singer of the 1930s had a dual role as music hall star Harriet Green and her daughter Harriet Hawkes. Matthews real life husband, Sonnie Hale, played the worried theatre producer Leslie Benn. The love interest, publicity man Tommy Thompson, was acted by Barry MacKay. The other main parts are - Maudie, played by Betty Balfour, Marquis of Staines by Ivor MacLaren and Treadwell by Hartley Power.

==Reception==
Popular in its day, the film has retained critical and contextual attention due to a strong plot, energetic performances and catchy Rodgers and Hart numbers comparable to "the best Astaire/Rodgers or Busby Berkeley films"; it is regarded as director Victor Savilles most worthy work. 

Success in America resulted in MGM making an offer of a big Hollywood role which Gaumont British would not release her to as she was too valuable to the British studio. 

==References==
===Notes===
 
===Bibliography===
*The Great British Films, Jerry Vermilye, 1978, Citadel Press, ISBN 0-8065-0661-X
*Over My Shoulder - An Autobiography, Jessie Matthews, 1974, W.H. Allen, ISBN 0-491-01572-0

==External links==
* 
* 

 
 
 
 
 
 
 