Invasion of the Star Creatures
{{Infobox film
| name           = Invasion of the Star Creatures
| image          = 
| image_size     =
| caption        = 
| director       = Bruno VeSota
| producer       = Samuel Z. Arkoff Berj Hagopian
| writer         = Jonathan Haze
| narrator       =
| starring       = Bob Ball Frankie Ray Gloria Victor Dolores Reed
| music          =
| release        =
| cinematography = Basil Bradbury
| editing        = Lew Guinn
| released       = May 3, 1962 (USA)
| runtime        = 70 minutes(theatrical); 80 minutes (TV version)
| country        = United States of America
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| distributor    = American International Pictures
}}

Invasion of the Star Creatures is a 1962, science fiction/comedy film directed by Bruno VeSota and starring Bob Ball and Frankie Ray. It was released theatrically by American International Pictures. It was released as a double feature with The Brain That Wouldnt Die. The theatrical version (on DVD) runs 70 minutes. The television version added a 10 minute sequence, bringing the running time to 80 minutes. 

==Plot== American Indian band, the two soldiers race back to their base to rally a defense of the Earth against the Kalar aliens. They succeed in saving the Earth by prematurely launching the alien spaceship, but are captured by the aliens. However, Puna convinces Tanga not to kill them as they have no way of returning home now and they need to rely on the two soldiers. Tanga accepts this, kisses Penn and the sparks fly. At the end of the film, the soldiers receive medals for their actions and they drive away with the aliens, now their wives.   

==Production==
Invasion of the Star Creatures was written by Jonathan Haze, an actor best known for his starring role in the 1960 Roger Corman feature The Little Shop of Horrors.  Haze’s screenplay originally had the title "Monsters from Nicholson Mesa," which was intended as a jokey reference to James H. Nicholson, the co-founder of American International Pictures. Haze was originally planned to be a star of the film, but he did not appear in the production.  Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p161 

Director Bruno VeSota was best known for B-level features including Female Jungle (1955) and The Brain Eaters (1958). 

The cavern and desert scenes for Invasion of the Star Creatures were shot in Bronson Canyon, a section of Griffith Park in Los Angeles, California, that has been widely used for film and television productions. 

==Release==
Invasion of the Star Creatures has never been well reviewed by critics. When it first opened, the film trade magazine  , in his 1997 book Keep Watching the Skies!, called the film "astonishingly bad...so helplessly bad that its almost unwatchable." 

In 1979, the film was cited by Harry and  !” 

==References==
 

==External links==
* 

 
 
 
 
 
 