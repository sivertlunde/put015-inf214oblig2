Believe in Me (1971 film)
{{Infobox film
| name           = Believe in Me
| image          = Believe in Me 1971 poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Stuart Hagmann
| producer       = Robert Chartoff Irwin Winkler
| screenplay     = Israel Horovitz
| starring       = Michael Sarrazin Jacqueline Bisset
| music          = Fred Karlin
| cinematography = Richard C. Brooks Richard C. Kratina
| editing        = Andrew Horvitch John C. Howard
| studio         =  MGM
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Believe in Me is a 1971 American romantic drama film directed by Stuart Hagmann and written by Israel Horovitz. The film was produced by Robert Chartoff and Irwin Winkler.

==Cast==
*Michael Sarrazin as Remy
*Jacqueline Bisset as Pamela
*Jon Cypher as Alan
*Allen Garfield as Stutter
*Kurt Dodenhoff as Matthew

==Reception== New York s Judith Crist disliked Hagmanns direction and Horovitzs screenwriting and wrote, "  is a sloppy story about an intern driven to drugs because he sees kids and old people get sick, and who apparently makes his girl an addict too—or simply makes her stop wearing eyeliner. You cant tell which—and couldnt care less. 

Roger Greenspun of The New York Times wrote that Believe in Me avoided melodrama seen in other drug films but found that it had predictable surprises and failed to explain "crucial" questions. He reviewed, "  is full of plot hints dropped and never retrieved, and it seems to have been cut—not so much edited as maimed. When allowed some emotional range ... Stuart Hagmann directs a rather decent movie. But such moments are too few, and in suppressing even pathos, the film also suppresses the other feelings that could have made it live." 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 