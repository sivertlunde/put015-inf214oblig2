Doctors' Wives (1931 film)
{{Infobox film
| name           = Doctors Wives
| image          = Joanbennett Doctors Wife.jpg
| image size     = 
| alt            = Minna Gombell, Warner Baxter, Cecilia Loftus, Joan Bennett in Doctors Wives.
| caption        = Publicity photo for the film
| director       = Frank Borzage
| producer       = John W. Considine Jr.
| writer         = Maurine Dallas Watkins Novel: Henry Lieferant  Sylvia Lieferant
| narrator       = 
| starring       = Warner Baxter Joan Bennett   Cecilia Loftus   Minna Gombell
| music          = 
| cinematography = Arthur Edeson
| editing        = Jack Dennis
| studio         = Fox Film Corporation
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}   
Doctors Wives is a 1931 romantic drama film made by Fox Film Corporation, directed by Frank Borzage. The film stars Warner Baxter and Joan Bennett.  The screenplay was written by Maurine Dallas Watkins, based on a novel by Henry Lieferant and Sylvia Lieferant.

==Cast==
* Warner Baxter as  Dr. Judson Penning 
* Joan Bennett as  Nina Wyndram 
* Victor Varconi as  Dr. Kane Ruyter 
* Cecilia Loftus as  Aunt Amelia 
* Paul Porcasi as  Dr. Calucci 
* Minna Gombell as  Julia Wyndram 
* Helene Millard as  Vivian Crosby 
* John St. Polis as  Dr. Mark Wyndram 
* George Chandler as  Dr. Roberts 
* Violet Dunn as  Lou Roberts 
* Ruth Warren as  Charlotte 
* Louise Mackintosh as  Mrs. Kent 
* William Maddox as  Rudie 
* Marion Lessing   
* Nancy Gardner as  Julia Wyndram

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 


 