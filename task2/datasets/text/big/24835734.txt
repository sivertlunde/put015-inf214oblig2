Darling (2010 film)
 
 

{{Infobox film
| name           = Darling
| image          = Darling (2010) poster.jpg
| alt            =  
| caption        =
| director       = A. Karunakaran
| producer       = B. V. S. N. Prasad
| story         = A. Karunakaran 
                      Dialogues - Darling Swamy
| dialogues  =  Darling Swamy Prabhas Kajal Prabhu M S Narayana Dharmavarapu Subramanyam
| music          = G. V. Prakash Kumar
| cinematography = Andrew
| editing        = Kotagiri Venkateswara Rao
| studio         = Sri Venkateswara Cine Chitra Sri Venkateshwara Reliance Movies
| released       =  
| runtime        = 153mins
| country        = India
| language       = Telugu
|budget=     |gross=  
}} Prabhu plays a key role. The music is composed by G. V. Prakash Kumar. The film was released on 23 April 2010 and has received positive reviews from both critics and the audience. The film was a commercial success at the box office.  It was later dubbed in Hindi under the title Sabse Badhkar Hum  It was remade into Kannada as Bulbul (film)|Bulbul. 

==Story== Prabhu Ganesan), Chandra Mohan), electric shock, leading to a scuffle between Prabha and Rishi. The fight results in Hanumantha Rao ejecting Prabha out of the house. Later, Prabha finds out that Rishi is not Appala Rajus son, but the nephew of Vishwanath. Vishwanath betrayed his father (Kota Srinivasa Rao) by marrying his girlfriend rather than the girl chosen by his father. As a result, he is ousted by his father. Hanumantha Rao, on behalf of Viswanath negotiates with his father for a reconciliation. After many attempts, he agrees to forgive Viswanath but on the condition that his daughters son should marry Nandini. Prabha decides not to reveal his love for Nandini and makes up his mind to leave. As Prabha tries to leave, Nandini reveals that she too loves him. But to save his fathers reputation, he doesnt reveal his love to her and leaves the place leaving Nandini heartbroken. Nishas father too realizes the truth that the story that Prabha had said was a lie, and tries to kill Prabha. Meanwhile, Nandini gets to know the truth behind Prabhas silence and convinces her father that she loves Prabha and he should get her married to Prabha. Prabha is rescued from Nishas father in time by Nandini, who urges Nishas father to spare Prabha as both of them love each other. The film ends with Prabha and Nandini getting married. 

==Cast== Prabhas as Prabhas/Prabha
* Kajal Agarwal as Nandini
* Shraddha Das as Nisha Prabhu as Hanumanta Rao Chandra Mohan as Bhadram Tulasi as Rajeshwari
* Mukesh Rishi as Nishas father
* Ahuti Prasad as Vishwanath
* Kota Srinivasa Rao as Vishwanaths father
* Dharmavarapu Subrahmanyam as Buchchaiah
* M. S. Narayana as Appala Raju
* Vamsi Krishna as Rishi
* Srinivasa Reddy as Prabhas friend
* Prabhas Srinu as Prabhas friend Sivannarayana Peddi as Hanumanta Raos friend
* Master Gaurav as Nandinis younger brother
* Russell Geoffrey Banks as Kidnapper
* David Firestar
* Alex Martin

==Crew==
* Director: A Karunakaran
* Screenplay: A Karunakaran
* Story: A Karunakaran
* Producer: B V S N Prasad
* Music: G V Prakash Kumar
* Stunts: Peter Heins
* Cinematography: Andrew
* Editing: Kotagiri Venkateswara Rao

==Reception==
The film opened to positive reviews. The Times of India gave a two and half star rating explaining "Director Karunakaran extracts good performances from his actors, but cant come up with a refreshing plot. It looks like the director hasnt really come out of his Tholi Prema hangover as he dishes out yet another one-sided love saga but fails to come up with a valid and logical reason to make a Gen Z lad to hold back his feelings for his lover until the last moment. However, he compensates it all, by adding loads of fun moments in the film and the screenplay does have its share of some touching moments".  Sify noted "Prabhas looked very handsome and his performance can be termed as the major highlight of the film".  Rediff felt that the film "is entertaining" and lauded the director for "showcasing clean wholesome family entertainment".  Local review sites GreatAndhra and Idlebrain both gave a three star rating and praised the actors performance and technical works of the film.  

==Soundtrack==
{{Infobox album
| Name = Darling
| Longtype = to Darling
| Type = Soundtrack
| Artist = G. V. Prakash Kumar
| Cover =
| Border = yes
| Alt =
| Caption =
| Released =  
| Recorded = 2010 Feature film soundtrack
| Length = 25:56 Telugu
| Label = Supreme Music
| Producer = G. V. Prakash Kumar
| Reviews =
| Last album = Madarasapattinam (2010)
| This album = Darling (2010)
| Next album = Va (film)|Va (2010)
}}

The film had its audio release function on 8 April at Cyber Gardens, Hyderabad. The audio function was attended by Prabhas Raju Uppalapati|Prabhas, Allu Arjun, Ravi Teja, Kajal Aggarwal, Puri Jagannadh, V. V. Vinayak, S. S. Rajamouli, Boyapati Srinu, Dil Raju, Meher Ramesh, A. Karunakaran, B. V. S. N. Prasad, Krishnam Raju, Sravanti Ravi Kishore, Shyam Prasad Reddy, Gemini Kiran, Aditya Babu, Andrews, G. V. Prakash Kumar, Supreme Raju, Parachuri Prasad, Nallamalapu Bujji, Ananth Sreeram, etc.

The programme started off with a cake cutting on the occasion of Allu Arjun and Ananth Sreerams birthday. Later the audio was released by all the directors and the first copy of it was received by all heroes who attended the function.  The audio was well Received by the Public.

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 25:56
| lyrics_credits = yes
| title1 = Hoshare
| lyrics1 = Ananth Sreeram Lesle Lewis
| length1 = 3:53
| title2 = Inka Aedo
| lyrics2 = Ananth Sreeram
| extra2 = Sooraj Santhosh, Prashanthini
| length2 = 5:17
| title3 = Neeve
| lyrics3 = Ananth Sreeram
| extra3 = G. V. Prakash Kumar
| length3 = 4:46
| title4 = Bulle
| lyrics4 = Ramajogayya Sastry
| extra4 = Mallikarjun, Priya Himesh
| length4 = 4:35
| title5 = Pranama
| lyrics5 = Ananth Sreeram
| extra5 = Rahul Nambiar
| length5 = 3:31
| title6 = Yeyo
| lyrics6 = Ananth Sreeram
| extra6 = Benny Dayal
| length6 = 3:51
}}

==Awards==
; CineMAA Awards
*  CineMAA Award for Best Actor (Jury) - Prabhas

; Nandi Awards
*  Nandi award for best Editor- Kotagiri Venkateswara Rao
*  Nandi Award for Best Male Dubbing Artist-R C M Raju

==Release==
* The film was released in 800 theaters world wide

==Remake==
Darling movie was remade in Kannada as Bulbul (film)|Bulbul starring Darshan (actor)|Darshan, Ambareesh, Ramya Barna, Rachita Ram and directed by M.D Shridhar and Produced by Meena Thoogudeepa. 

{| class="wikitable" style="width:50%;"
|- style="background:#ccc; text-align:center;"
| Darling (2010) (Telugu language|Telugu)  || Bulbul (film)|Bulbul (2013)  (Kannada language|Kannada)
|- Darshan
|-
| Kajal Aggarwal || Rachita Ram
|-
| Shraddha Das || Ramya Barna
|-
| Prabhu || Ambareesh
|}

==References==
 

==External links==
*  
 

 
 
 
 
 
 