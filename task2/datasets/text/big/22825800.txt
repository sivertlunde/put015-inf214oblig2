Sekstet
{{Infobox film
| name           = Sekstet
| image          = 
| caption        = 
| director       = Annelise Hovmand
| producer       = Johan Jacobsen
| writer         = Poul Bach Annelise Hovmand
| starring       = John Kelland
| music          = 
| cinematography = Karl Andersson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Denmark
| language       = Danish
| budget         = 
}}
 Best Foreign Language Film at the 37th Academy Awards, but was not accepted as a nominee. 

==Cast==
* John Kelland as Peter
* Ghita Nørby as Lena
* Axel Strøbye as Robert, Elaines man
* Ingrid Thulin as Elaine
* Hanne Ulrich as Rachel
* Ole Wegener as John

==See also==
* List of submissions to the 37th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 