Andala Ramudu (2006 film)
 
{{Infobox Film
|  name            = Andala Ramudu
|  image           = 
|  imdb_id         = 
|  writer          =  Sunil Aarthi Aggarwal
|  director        = P Lakshmi Narayana
|  producer        = NV Prasad, Paras Jain
|  editing         = Nandamuri Hari
|  distributor     = 
|  music           = S. A. Rajkumar
|  released        = August 11, 2006
|  language        = Telugu
|  budget          = 
}} Telugu film Sunil and Sundara Purushan Livingston and Rambha (actress)|Rambha.

==Plot summary==
 Venu Madhav) born to his step mom.

Ramudu’s love for Radha remains unchanged. In fact, he returns home after so long time only to win Radha’s love and marry her.  On the contrary, Radha loves another person, Raghu (Jai Akash|Akash) who is an orphan and also jobless. Radha’s father (Kota Srinivasa Rao) dislikes this; he lays a condition that he would agree for their marriage only if Raghu finds a job.

Ignorant of the fact, but with good intention, Ramudu offers a job to Raghu. Later, he learns about the fact and gets despaired. In such a circumstance, Ramudu’s brother resolves to unite his brother with Radha and eliminate Ramudu’s misery.
Ramudu’s brother tactfully implicates Raghu in a murder and sends him to prison. Thus he plays a spoil game in averting marriage between Radha and Raghu. In such a distressed condition, Radha’s father with no other option remaining pleads with Ramudu to marry Radha. Ramudu readily agrees and marries Radha.

What happens when Radha learns about the fact later? Will she continue her married life with Ramudu or does she goes to Raghu or anything strange happens... The remaining part of the movie is based on these circumstances.

==Cast== Sunil .... Ramudu
* Aarthi Aggarwal .... Radha
* Akash .... Raghu
* Kota Srinivasa Rao .... Bhushanam (Radhas father)
* Brahmanandam
* Dharmavarapu Venu Madhav .... Ramudus stepbrother
* Kondavalasa
* Lakshmipti
* Ramachandra Rao
* Lakkimsetty
* Duvvasi Mohan
* Chalapathi Rao

==Crew==
* Director: P Lakshmi Narayana (Deepti)
* Screenplay: P Lakshmi Narayana (Deepti)
* Story: Livingston
* Dialogues: Ramesh - Gopi
* Music: SA Raj Kumar
* Lyrics: Bhuvana Chandra, ES Murthy & Acharya
* Executive producer: Vakada Appa Rao
* Producers: NV Prasad & Paras Jain
* Cinematographer: Sameer Reddy
* Editing: Nandamuri Hari
* Art: Kumar
* Presented by: RB Chowdary

==External links==
* 

==See also==
* Andala Ramudu (1973 film), an earlier period Telugu film directed by Bapu (artist)|Bapu.

 
 
 
 
 

 