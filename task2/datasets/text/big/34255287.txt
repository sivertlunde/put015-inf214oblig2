Trimurtulu
 
{{Infobox film
| name           = Trimurtulu
| image          =
| caption        =
| director       = K. Murali Mohan Rao
| producer       = T. Subbarami Reddy
| writer         =
| story          = Venkatesh Arjun Arjun Gadde Rajendra Prasad Kushboo Ashwini Aswini
| music          = Bappi Lahiri
| cinematography =
| editing        =
| studio         = Maheswari Parameswari Productions
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood film Rajendra Prasad, Kushboo and Aswini played the lead roles and music composed by Bappi Lahiri.     It is remake of Hindi film Naseeb (1981 film)|Naseeb. The film recorded as Average at the box-office. 

==Plot==
The story begins with four friends Ram Murthy (Kaikala Satyanarayana|Satyanarayana) hotel Server, Damodaram (Rao Gopal Rao) Photographer, Kotaiah (Nutan Prasad) Chariot Rider, Bhadraiah (Bheemiswara Rao) bandmaster. Ram Murthy happy with his wife and two sons Raja and Sandeep, Damodaram has a son Siva, even Raja & Siva are also close friends, BBhadraiah has two daughters Lata and Rani. Once a drunker (Allu Ramalingaiah) who cannot pay his bill decides to sell his ticket to the server Ram Murthy and he purchases the ticket with his three friends. When the ticket turns out to be a winner, Damodaram and Kotaiah turn on the other two, murdering Bhadraiah and framing Ram Murthy goes on the run but Kotaiah and Damodaram intervene and throw him over a bridge into a river and soon after Ram Murthy wife also dies in an accident and his two children are taken care by Mary (Sumithra (actress)|Sumitra) whom Ram Murthy looks as her own sister, along With her daughter Julie.
 Rajendra Prasad) and Rani (Ashwini (actress)|Aswini) are college meets and they are fallen in love but Ranis mother doesnt agree because She knows that Sandeep father is Ram Murthy, Sandeep Chanllenges to her that he will prove that his father is innocent.

Meanwhile, Ram Murthy is alive in Hong Kong he is rescued by a Don (Anupam Kher), and no one is aware he is alive. Damodaram and Kotaiah are business partners to Don but they never seen him, once they play mischief with him, Don sends Ram Murthy in his place to take revenge. Kotaiah finds out he is Ram Murthy not Don, he want alienate both Ram Murthy and Damodaram because he is the only proof for the Bhadraiahs murder. Simultaneously Siva knows regarding the sacrifice of Raja and Julies, he gives back his love to Raja and decides to marry Julie.

At the same time Kotaiah plans attack by his son Ashok (Betha Sudhakar|Sudhakar) on Damodaram but he escapes, Simultaneously Ram Murthy kidnaps Siva and Ashok, Raja follows him and finds out Ram Murthy is his father through Mary and even Siva also, and he joins hands with them. Meanwhile, Damodaram decides to take revenge against Kotaiah, he has proof of photograph while Kotaiah killing Bhadraiah, Kotaiah kills Damodaram, Sandeep reaches there Damodaram handovers the proof to him before his death. Kotaiah keeps Sandeep in his grip and asks his son Ashok in exchange. Suddenly, Don arrives from Hong Kong, takes everyone into his control, keeps the murder proof with him and blackmails Ram Murthy to go back to Hong Kong and surrender to Police in his place. Finally Raja, Siva, Sandeep, joins together saves Ram Murthy from baddies, takes their revenge against them and movie ends with marriage of 3 heroes with their fiances and again the same drunker comes to their new hotel to sell a lottery ticket.

==Cast==
{{columns-list|3| Venkatesh as Raja Arjun as Siva  Rajendra Prasad as Sandeep
* Shobana as Lata Kushboo as Julie Aswini as Rani
* Rao Gopal Rao as Damodaram Satyanarayana as Ram Murthy 
* Allu Ramalingaiah as A Drunker
* Nutan Prasad as Kotaiah  
* Anupam Kher as Don  Sudhakar as Ashok 
* Balaji as Kotaiahs Younger Son
* Ramji as Dons Son
* Bob Christo as Fighter
* Suthi Veerabhadra Rao as Principal
* Suthivelu as Achary 
* Thyagaraju as Agent KK
* Bhimeswara Rao as Bhadrayah
* Peketi Sivaram as Ad Film Director Chitti Babu 
* KK Sarma as Hotel Owner
* Mada 
* Dham as Hotel Server Sumitra as Mary 
* Anitha as Malathi
* Mamatha as Lecturer
* K.Vijaya as Shanthi
* Master Satish as Childhood Raja
* Master Suresh as Childhood Siva
* Baby Seeta as Childhood Julie
}}

==Special Appearance==
  Krishna
* Sobhan Babu
* Krishnam Raju
* Chiranjeevi
* Nandamuri Balakrishna
* Akkineni Nagarjuna
* Murali Mohan Chandra Mohan
* Paruchuri Brothers
* Gollapudi Maruti Rao Padmanabham
* A. Kodandarami Reddy
* Kodi Rama Krishna
* Vijaya Nirmala Sharada
* Raadhika
* Vijayashanti Radha
* Jayamalini Anuradha
* Y.Vijaya 
 

==Soundtrack==
{{Infobox album
| Name        = Trimurthulu
| Tagline     = 
| Type        = film
| Artist      = Bappi Lahiri
| Cover       = 
| Released    = 1987
| Recorded    = 
| Genre       = Soundtrack
| Length      = 30:19
| Label       = Lahari Music
| Producer    = Bappi Lahiri
| Reviews     =
| Last album  = Tene Manasulu  (1987)  
| This album  = Trimurthulu  (1987)
| Next album  = State Rowdy  (1989)
}}
Music composed by Bappi Lahiri. Music released Lahari Music Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!Lyrics!!length
|- 1
|Oke Maata Oke Baata  SP Balu Veturi Sundararama Murthy
|5:28
|- 2
|Ayyayyo Ayyayyayyo  SP Balu, P. Susheela Veturi Sundararama Murthy
|4:48
|- 3
|Mangchaav Mangchaav  SP Balu, S. Janaki Veturi Sundararama Murthy
|4:07
|- 4
|SeethaaKaalam SP Balu, P. Susheela Veturi Sundararama Murthy
|3:45
|- 5
|Ee Jeevitham SP Balu, Mano (singer)|Mano, P. Susheela Aatreya (playwright)|Acharya Atreya
|5:11
|- 6
|Bye Bye Bye SP Balu,Mano, S. Janaki, P. Susheela, S. P. Sailaja Veturi Sundararama Murthy
|7:00
|}

==References==
 