Fail Safe (1964 film)
{{Infobox film
| name           = Fail-Safe
| image          = Fail_safe_moviep.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = Sidney Lumet
| producer       = Sidney Lumet Charles H. Maguire Max E. Youngstein
| based on       =   Peter George
| starring       = Henry Fonda Dan OHerlihy Walter Matthau Frank Overton Larry Hagman
| cinematography = Gerald Hirschfeld
| editing        = Ralph Rosenblum
| distributor    = Columbia Pictures
| released       =  
| runtime        = 112 minutes
| language       = English 
| budget         = 
| gross          = $1,800,000  (rentals)  
}}
 thriller film 1962 novel of the same name by Eugene Burdick and Harvey Wheeler. It portrays a fictional account of a Cold War nuclear crisis. The film features performances by veteran actors Henry Fonda, Dan OHerlihy, Walter Matthau and Frank Overton. Fritz Weaver, Dom DeLuise and Larry Hagman made their early roles.
 televised play, live in black-and-white on CBS.

==Plot==
During the early 1960s, Cold War tensions existing between the Soviet Union and the United States are heightened. An accidental thermonuclear first-strike attack by a group of United States Vindicator bombers (Convair B-58 Hustler aircraft) is launched in a mission against Moscow, the capital of what was then the Soviet Union.
 VIPs at the headquarters of the Strategic Air Command (SAC) at Offutt AFB in Omaha, Nebraska, an alert is initiated when SAC radar indicates an intrusion into American airspace of an unidentified flying object. The standard procedure of SAC is to keep several groups of bombers constantly flying around the clock as an immediate response to any potential nuclear attack on the country. Upon an initial alert from headquarters, these airborne groups proceed to pre-identified aerial points around the globe called "fail-safe points" to await final instruction before proceeding towards Soviet targets.

Shortly after reaching those points, the flying object is identified as an off-course airliner and the alert is canceled. However, a technical error sends an errant "go code" to one group of bombers, ordering them to proceed and attack their target. Coincidentally and simultaneously, a new Russian jamming device begins radio jamming communications between SAC headquarters and the bomber group with the result that the group commander, Colonel Jack Grady (Edward Binns), begins to lead the attack on Moscow.
 Soviet Chairman, whereupon mistakes on both sides (the American accidental launch of the mission and the coincidental Soviet jamming) are acknowledged. The jamming is reversed; however, SAC training and protocols cause the crew to reject counter-orders to abort the mission.

Before completion of the accidental attack on Moscow, the President realizes the severity of the situation and seeks a resolution to the matter that will avoid reprisal from the Russians and, ultimately, an all-out nuclear holocaust. With this threat in mind, the President orders an American bomber toward New York City, which otherwise would be destroyed by the Soviets, along with many other American cities, in any counter-attack. Upon failure to stop the destruction of Moscow, the President orders General Black (Dan OHerlihy), who is flying the bomber, to drop the same nuclear payload which struck Moscow in the hope that it will appease the Soviets. After releasing the bombs, Black commits suicide.

==Cast==
  
* Dan OHerlihy as Brigadier General Warren A. "Blackie" Black, USAF
* Walter Matthau as Professor Groeteschele
* Frank Overton as General Bogan, USAF
* Ed Binns as Colonel Jack Grady, USAF
* Fritz Weaver as Colonel Cascio, USAF
* Henry Fonda as the President
* Larry Hagman as Buck, the Presidents interpreter William Hansen as Defense Secretary Swenson
* Russell Hardie as General Stark
 
* Russell Collins as Gordon Knapp
* Sorrell Booke as Congressman Raskob
* Nancy Berg as Ilsa Woolfe
* Hildy Parks as Betty Black
* Janet Ward as Helen Grady
* Dom DeLuise as Technical Sergeant Collins, USAF
* Dana Elcar as Mr. Foster
* Louise Larabee as Mrs. Cascio
* Frieda Altman as Mrs. Jennie Johnson
 

==Production== Pentagon war freezing at the moment of impact. No mushroom clouds appear in the film.

The Soviets are never seen in the film. The progress of the attack is followed almost exclusively on giant, electronic maps overlooking the War Room in the Pentagon and SAC Headquarters. Conversations with the Soviet Premier (Russian language occasionally heard in the background on the Moscow–Washington hotline|"Hot-Line") are translated by an American interpreter (Larry Hagman). Suspense builds through dialog between the President and other officials, significantly including the character representing the advisor to the Department of Defense, Prof. Groeteschele (Walter Matthau), an old college friend, General Black (Dan OHerlihy), and SAC commander General Bogan (Frank Overton).

The "Vindicator" bombers (an invention of the novelists) are represented in the film by sometimes stock footage of a real U.S. aircraft, the Convair B-58 Hustler, shown in negative. Fighters sent to attack the bombers are illustrated by film clips of the Lockheed F-104 Starfighter, Convair F-102 Delta Dagger and McDonnell F-101 Voodoo. Stock footage was used inasmuch as the United States Air Force declined to cooperate with the films producers fearful of possible negative publicity from a fictional plot predicated on an inability to positively control its nuclear strike forces.   strategypage.com. Retrieved: September 5, 2012.  The scene depicting Gradys Group Six bombers taking off under afterburner power was stock footage of a single B-58 takeoff edited to look like several bombers taking off in succession.

The film was the second movie role for actor Dom DeLuise. He plays the unfortunate Sgt Collins, who triggers the false go signal by replacing a failed electronic module in the master "fault indicator", a control computer for the entire SAC complex. Later in the film, a reluctant and frightened Collins is forced to give the Soviets information on how to destroy the nuclear-tipped air-to-air missiles on the American aircraft by Bogan after Cascio and his immediate subordinate refuse to do so.

==Reception==
When Fail-Safe opened, it garnered excellent reviews, but its box-office performance was poor. Its failure rested with the similarity between it and the mutually assured destruction satire Dr. Strangelove, which appeared in theaters first. Despite this, the film later was applauded as a Cold War thriller. The novel sold through to the 1980s and 1990s, and the film was given high marks for retaining the essence of the novel.  Over the years, both the novel and the movie were well-received for their depiction of a nuclear crisis, although garnering a legion of critical reviews that centered on the one fallacy, in that the "fail safe" command sequence was misinterpreted.  

===Lawsuit=== Peter Georges Red Alert, insisted  the studio release his movie first (in January 1964). Jacobson, Colin.   dvdmg.com, 2000. Retrieved: November 21, 2010.  Fail-Safe so closely resembled Red Alert that George filed a plagiarism lawsuit. The case was settled out of court. 

==References==
Notes
 

Bibliography
* Dolan Edward F. Jr. Hollywood Goes to War. London: Bison Books, 1985. ISBN 0-86124-229-7.
* Evans, Alun. Brasseys Guide to War Films. Dulles, Virginia: Potomac Books, 2000. ISBN 1-57488-263-5.
* Harwick, Jack and Ed Schnepf. "A Viewers Guide to Aviation Movies". The Making of the Great Aviation Films, General Aviation Series, Volume 2, 1989.
* Lobrutto, Vincent. Stanley Kubrick: A Biography. New York: Da Capo Press, 1999. ISBN 978-0-306-80906-4.

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 