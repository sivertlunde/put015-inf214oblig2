Oliver Twist (1982 TV film)
{{Infobox film
| name           = Oliver Twist
| image size     = 
| image	=	Oliver Twist VideoCover.jpeg
| alt            = 
| caption        = 
| director       = Clive Donner
| producer       = Ted Childs Norton Romsey
| writer         = James Goldman
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = George C. Scott Tim Curry Michael Hordern Timothy West Eileen Atkins Cherie Lunghi Oliver Cotton Richard Charles Lysette Anthony John Barrard
| music          = Nick Bicât
| cinematography = Norman G. Langley
| editing        = Peter Tanner
| studio         =
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = United States United Kingdom
| language       = English
| budget         = 
| gross          = 
}} 1982 made-for-TV classic of the same name, premiering on the CBS television network as part of the Hallmark Hall of Fame.  Stars include George C. Scott, Tim Curry, Cherie Lunghi, and introducing Richard Charles as Oliver, in his first major film role.

==Plot==
The film opens with a young, blonde-haired woman outdoors during a rainstorm.  She is seen struggling on her knees, and she manages to stumble into a home.  The occupants, learning that the pregnant woman is about to go into labour, make preparations for her birth.
 Oliver Twist.  

Like the other boys in the workhouse, Oliver lives a hard life of endless labour and schooling, with only a bowl of gruel for supper, while Bumble sits above them feasting on food such as leg of lamb.  After seeing his half-starved friend Dick devour his bowl and still wanting more, Oliver, in a gesture of compassion, offers the lad his own, then goes up to Bumble and asks for more, unaware of the consequences.  His request angers Bumble, who hires him out to work for Mr Sowerberry, a local undertaker.
 Philip Davis) hatred.  

Claypole, a teenager, has been assigned to supervise Oliver. He gives Oliver harsh tasks and becomes further resentful when Sowerberry decides to use Oliver instead of him for silent mourner duties. Claypole taunts Oliver one day, making fun of his dead mother. The remark angers Oliver, who delivers a surprisingly powerful blow to Claypoles face, breaking his nose.

Sowerberry rushes in and after learning what caused the fracas, takes Claypoles side and tells Oliver he will be returned to the workhouse the following day. Oliver waits until later that evening and then sneaks out. He roams the streets that evening until he arrives in the market town of Barnet (near London), where he is met by the Artful Dodger (Martin Tempest). Aware of Olivers plight as he too is an orphan, the Artful Dodger offers Oliver lodgings from his benefactor. Oliver agrees, unaware of what he has got himself into.
 Jewish man of dark features. Among Fagins group are Bill Sikes (Tim Curry), a drunk who oversees the orphan thieves, and Nancy (Oliver Twist)|Nancy, an attractive young woman often used for sexual favors, and frequently abused by Bill. She takes a liking to Oliver and tries to help him, but for this, she is eventually viciously murdered by Bill.
 Mr Brownlow, along with his niece Rose Maylie and housekeeper, Mrs. Bedwin, take pity upon him and nurse the boy back to health.  He finds both a newly found happiness and joy with them.  Mr. Brownlow and Mrs. Bedwin notice a close resemblance between Oliver and a ladys portrait on the wall, leading them to eventually discovering the boys true identity.
 Monks (Oliver Cotton), who has a distinguishing red birthmark over his right eye, has learned that although he and Oliver are born of different mothers, they are of the same father.  Monks learns that their father has disinherited him in favor of Oliver, though he inherited what should have been Olivers inheritance after his mothers death.  Though Monks is legitimate, he also has aspirations of wealth and stature that his inheritance would provide.  Thus, he also sees the relationship between them as socially scandalous.  His efforts prove unsuccessful in the end, however.

When Nancys body is discovered, an angry mob descends upon the gang and captures Fagin. "Filthy Jew!" shouts one man who strikes him in the face. Fagin rails against the crowd as he is led away. "If you need money, I am the clever Jew! If you need my help I am the kind Jew! You all sicken me!" 

Bill, fleeing after Nancys murder, tries to kill his pet dog, but the dog escapes and leads the mob straight to Bill, who accidentally hangs himself after trying to flee over a rooftop and hallucinating that he sees Nancys bloody ghost.

Brownlow is revealed to be a friend of Olivers father, Philip ("Edwin" in the novel).  Monks real name is revealed to be Edward Leeford, and Philips marriage to Edward mother had been an unhappy one.  Having separated from her, Philip moved to the country where he met and fell in love with Olivers mother, revealed to have been named Agnes Fleming.  Leeford never told Agnes of his marriage, nor did she tell him when she became pregnant with his child (Oliver).  He left for London to ask Edwards mother for a divorce so he could marry Agnes, but died before he could do so.  Already having had a premonition that he was going to die, Leeford wrote a will in which he left a small inheritance to Edward and his mother, but the rest to Agnes.  Feeling abandoned and ashamed, Agnes ran away and disappeared, leading up to the events at the beginning of the film.  Before he went to London, Leeford left both the will and a portrait he had painted of Agnes with Brownlow.

Brownlow does some investigative work on his own to bring justice to his friends young son. He learns of the cruelty and inhumane conditions at the workhouse, and also learns of Bumbles theft of workhouse funds for his own benefit, money which has been intended to properly feed and clothe the orphans. Bumble immediately blames his wife, Mrs. Mann (to whom he proposed marriage at the start of the film) for the misappropriations and claims to love Oliver as he does the other orphans.  After receiving a locket Mrs. Bumble had stolen from Agness corpse and revealing to everyone the boys true identity, Brownlow tells Monks that he will be going to prison. Then, not fooled by Bumbles charade, Brownlow informs him that under British law, a husband is accountable for his wifes misdeeds, prompting Bumbles famous reply "If thats the eye of the law, then the law is a ass."  Brownlow then tells Bumble that he will use his influence to see to it that he and his wife lose their workhouse jobs and may even face criminal charges.

The film ends with Monks going to prison and Brownlow and Rose assuring Oliver that he is no longer a foundling, but now has a true identity of his own.  Everyone then climbs into Brownlows coach and they make the journey back to Brownlows estate.

==External links==
* 

 
 

 
 
 
 
 
 