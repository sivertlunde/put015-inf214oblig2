The Fatal Ring
 
{{Infobox film
| name           = The Fatal Ring
| image          = The Fatal Ring.jpg
| caption        = Advertisement (1917)
| director       = George B. Seitz
| producer       = Louis J. Gasnier
| writer         = Frederick J. Jackson Bertram Millhauser George B. Seitz
| starring       = Pearl White Earle Foxe
| cinematography = 
| editing        =  Astra Films
| released       =  
| runtime        = 20 episodes 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 action film serial directed by George B. Seitz. Silentera.com reports that the UCLA Film and Television Archive may have a complete print.   

==Cast==
* Pearl White - Violet Standish
* Earle Foxe - Nicholas Knox
* Warner Oland - Richard Carslake
* Ruby Hoffman - The Priestess
* Henry G. Sell - Tom Carlton
* Floyd Buckley
* Cesare Gravina
* Mattie Ferguson
* Richard LaMarr
* Bert Starkey
* L.J. OConnor (as Louis J. OConnor)
* Harriet Reller
* George B. Seitz - Undetermined Role (uncredited)

==Chapter titles==
 
# The Violet Diamond
# The Crushing Walls
# Borrowed Identity
# The Warning On The Ring
# Danger Underground
# Rays of Death
# The Signal Lantern
# The Switch In The Safe
# The Dice of Death
# The Perilous Plunge
# The Short Circuit
# A Desperate Chance
# A Dash of Arabia
# The Painted Safe
# The Dagger Duel
# The Double Disguise
# The Death Weight
# The Subterfuge
# The End of The Trail

==References==
 

==External links==
 
* 

 
 

 
 
 
 
 
 
 
 


 
 