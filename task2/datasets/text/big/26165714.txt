Jacqueline (film)
{{Infobox film
| name           = Jacqueline
| image_size     = 
| image	=	Jacqueline FilmPoster.jpeg
| caption        = 
| director       = Roy Ward Baker
| writer         = 
| narrator       = 
| starring       = John Gregson
| music          = Cedric Thorpe Davie
| cinematography = Geoffrey Unsworth
| editing        = John D. Guthridge
| studio         = George H. Brown Productions Rank 
| released       = 5 June 1956
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Jacqueline is a 1956 British drama film shot in Belfast and directed by Roy Ward Baker. It is based on the novel The Grand Man (1954) by Catherine Cookson. 

==Plot==
Steel worker Mike McNeils drinking spirals out of control when he loses his job due to vertigo at the Belfast shipyard. But his devoted young daughter Jacqueline vows to help him. She attempts to persuade a tough land-owner to give her troubled dad another chance. 

==Cast==
* John Gregson as Mike McNeil  
* Kathleen Ryan as Elizabeth McNeil  
* Jacqueline Ryan as Jacqueline McNeil   Noel Purcell as Mr. Owen  
* Cyril Cusack as Mr. Flannagan   Tony Wright as Jack McBride  
* Maureen Swanson as Maggie  
* Liam Redmond as Mr. Lord  
* Maureen Delany as Mrs. McBride  
* Richard OSullivan as Michael
* Marie Kean as Mrs. Flannagan
* J.G. Devlin as Mr. Lords servant
* Harold Goldblatt as Schoolmaster

==Critical reception==
Britmovie called the film "gushingly sentimental";  while Sky Movies called it "a likeable little drama with earnest performances and atmospheric background detail." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 