Adavi Ramudu (1977 film)
 
{{Infobox film
| name          = Adavi Ramudu
| image         = Adavi Ramudu 1977.jpg Jandhyala   (Dialogues )  Veturi Sundararama Murthy   (Lyrics)   NTR  Jayaprada  Jayasudha Sreedhar Surapaneni
| director      = K. Raghavendra Rao
| producer      = Satyanarayana   Suryanarayana  (Satya Chitra) 
| distributor   =
| released      =  
| runtime       =
| language      = Telugu
| country       = India
| music         = K. V. Mahadevan
| cinematography = A. Vincent
| editing       = Kotagiri Venkateswara Rao Kotagiri Gopala Rao
| budget        =
}}
Adavi Ramudu  is a Telugu Drama film which released on 28 April 1977 and was directed by K. Raghavendra Rao. Nandamuri Taraka Rama Rao|NTR, Jayaprada, and Jayasudha played important roles and this film won Filmfare Best Film Award (Telugu).

Some scenes in the film were taken from superhit Kannada movie Gandhada Gudi (1973); starring Rajkumar (actor)|Rajkumar. 

==Plot==
Nagabhushanam and his son Nagaraju (Satyanarayana) runs smuggling and Illegal transport of forest produce in the Forest. Ramu (NTR) opposes and raises the villagers to fight against him. Padma (Jayaprada) Daughter of Forest officer (Jaggayya) loves him. One tribal lady Chilakamma (Jayasudha) also loves him as a brother. Nagabhushanam takes the help of Jaggu (Sridhar) to sent Ramu away from the forest. Later half reveals that Ramu was in fact a Forest officer in secret mission to investigate the case of Forest affairs. The story finally leads to the arrest of criminals.

==Cast==
{| class="wikitable"
|-
! Actor / Actress !! Character
|-
| N. T. Rama Rao || Ramu
|-
| Jayaprada || Padma, daughter of Forest officer
|-
| Jayasudha || Chilakamma
|-
| Nagabhushanam ||
|-
| Kaikala Satyanarayana || Nagaraju
|- Raja Babu || Bheemanna, the cook
|-
| Kongara Jaggayya || Forest Officer
|-
| Pandari Bai ||
|-
| Gummadi Venkateswara Rao ||
|-
| Rama Prabha ||
|-
| Sridhar || Jaggu
|-
| Arja Janardhana Rao ||
|-
| Mada Venkateswara Rao ||
|-
| Rohini || Dumb Girl
|}

==Production==
Producer Suryanarayana speaks:"After a disastrous Prema Bandham, we planned Adavi Ramudu with Ramarao gaaru, and a two-films old Raghavendra Rao. His much-celebrated Jyothi didnt start by that time. But Ramarao gaaru knew KRR well enough since the time Raghavendra Rao assisted  pouraaNika brahma Kamalakara Kameswara Rao in Pandava Vanavasamu and agreed readily to do the film. After completing the shooting, he just complemented us saying that shooting of this film shall remain as one of the best memories in his life. The film is such a big success that people of that generation speak volumes of it and feel nostalgic even today, wherever I go. I am really happy that I made such a film with my cousin Satyanarayana". http://www.telugucinema.com/c/publish/movieretrospect/retro_adaviramudu.php 

Satya Chitra banner (of producers Nekkanti Veera Venkata Satyanarayana and Arumilli Suryanarayana) started their first film with Sobhan Babus Tahsildar Gaari Ammaayi (November 12, 1971), which was based on a serial by Kavilipati Vijayalakshmi in Andhra Prabha titled Vidhi Vinyaasaalu. The film was directed by K.S. Prakasha Rao, father of K. Raghavendra Rao; Raghavendra Rao was an assistant director for that film. The film was one of the big hits for Sobhan Babu who played dual role in the film. After that film, they made Prema Bandham (March 12, 1976 - the same day as NTRs Aaradhana) with Sobhan Babu and Vanisree under the direction of K. Viswanath. It garnered good openings, thanks to Sobhan-Vanisree-Viswanath combo movies ( Chelleli Kaapuram, Chinnanaati Snehitulu, etc.), but it was soon declared a flop as it could not sustain the craze. (The film was released on the same day or NTRs Aaradhana.) 

When they were planning their next film, they approached Raghavendra Rao to work for them, whom they already knew as an assistant director as he just started taking up direction independently. NTR had already given them dates in September 1976. At that time, Kannada Rajkumars movie Gandada Gudi, made with a forest backdrop, went on to be a super hit in Kannada. This prompted the team to suggest that Jandhyala should work on the script and dialogues based on a forest backdrop. 

Jayasudha, who already worked with K. Raghavendra Rao for his film Raja, was chosen for an important role. Jayaprada was just a budding artist at that time with her Sirisirimuvva being a super hit. Thus, she was taken for the heroine role. Sridhar was approached to do an important role opposite to Jayasudha. Jayasudha was not aware that she was doing the second role until the last minute, but since she already agreed to do the film, she just went ahead keeping faith on Raghavendra Rao. She got a lot of letters after the film was released to not do second lead role again, though she had a good role in the movie(People were not aware that she signed this film before she did Jyothi, as Jyothi hit the screens  earlier than Adavi Ramudu and brought Jayasudha a good name.). 

Though he had given the dates for Adavi Ramudu, NTR was doing Daana Veera Soora Karna at the same time, which he planned to complete before January in order to ready it for Sankranthi. So, he called the producers Satyanarayana and Suryanarayana and requested them to and inform that hed give bulk dates once he completed the shooting for Daana Veera Soora Karna. 

Except the opening shot, all the of the movie shooting was done in Madumalai forest. The opening shot was done in Madras in a Studio on January 9, 1977. Adavi Ramudu was the first film for which NTR had given dates for more than a month (35 days) out of Madras! Adavi Ramudu is NTRs first color film shot in Cinemascope. This was also the first film that used a Cinemascope lens imported from Japan by Prasad Labs (Madras). Earlier Cinemascope films Alluri Seetharamaraju and Kurukshetram were done with a lens brought from Mumbai.  Cameraman Vincent first worked with NTR much earlier, for the film Sontha Ooru produced by Sree Ghantasala, and this is the second film for him with NTR after a long time. 

There were only three Government Guesthouses in Madumalai forest, and they could not accommodate the 300 plus cast and crew in that dense forest. Thus, the producers took carpenters, molders, painters, etc. to the forest, worked for fifty days to build new guesthouses and took special permission for electricity in the middle of the forest. The nearest town/ airport was 250&nbsp;km away from the forest, and for every small thing, they needed to go all the way to Mysore. Thus, they used to get all of a days needs each morning from Mysore.

Jayasudha recollects two accidents on this shooting: one was while doing a scene where she and Jayaprada were sitting on elephants.  Junior artistes were practicing a Dappu scene right then, and the elephants got panicked by the noise and threw down both the lead ladies with their trunks! Luckily, they escaped the major injuries. Another one was a chase scene where she and Jayaprada were chased by villains while they were trying to escape on a horse cart. The carts axle was broken and both of them were injured and were forced to rest for a couple of days!

==Soundtrack==
Adavi Ramudu has many hit songs penned by Veturi Sundararama Murthy. The music score is provided by K. V. Mahadevan.
# "Aarasukoboyi Paaresukunnanu" (Singers: S. P. Balasubrahmanyam, S. Janaki and P. Susheela)
# "Ammathodu Abbathodu Neethodu Naathodu" (Singers: S. P. Balasubrahmanyam, S. Janaki and P. Susheela)
# "Choodara Choodara Oka Choopu" (Singers: S. P. Balasubrahmanyam and S. Janaki
# "Ennallakennaallaku Ennellu Thirigoche Maa Kallaku" (Singers: S. P. Balasubrahmanyam, S. Janaki and P. Susheela)
# "Kuku Kuku Kokilamma Pelliki Konantha Pandiri" (Singers: S. P. Balasubrahmanyam and P. Susheela)
# "Manishai Puttinavadu Kaaradu Matti Bomma..Krushi Vunte Manushulu Rushulautaru" (Singer: S. P. Balasubrahmanyam)

==Box-office==
* The film was a huge blockbuster.It was the first Telugu film to collect Rs. 4 crore (Rs. 40 million). 
* It had a 100 day run in 32 centres,  200-day run in 8 centres   and a 365-day run in 4 centres (Hyderabad, Kurnool, Vijayawada and Visakhapatnam). 
* The film had run for 302 days in Alankar theatre, Vishakapatnam. 

==References==
 

== External links ==
*  
 

 
 
 
 
 
 