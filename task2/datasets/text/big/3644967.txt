The Scarlet Letter (2004 film)
{{Infobox film
| name         = The Scarlet Letter
| image        = The Scarlet Letter 2004 poster.jpg
| caption      = 
| writer       = Byun Hyuk   Kang Hyeon-joo
| starring     = Han Suk-kyu Lee Eun-ju Sung Hyun-ah Uhm Ji-won
| director     = Byun Hyuk
| producer     = Lee Seung-jae
| cinematography = Choe Hyeon-gi
| editing      = Ham Sung-won
| music        = Lee Jae-jin
| distributor  = Showbox
| released     =  
| country      = South Korea
| runtime      = 118 minutes
| language     = Korean
| budget       = 
| film name      = {{Film name
 | hangul       =  
 | hanja        =  글씨
 | rr           = Juhong geulshi
 | mr           = Juhong gǔlshi}}
}} 2004 South Korean film about a police detective who investigates a murder case while struggling to hang onto his relationships with his wife and mistress. It is the second film by La Femis-graduate and academic Byun Hyuk (Daniel H. Byun), and starred Han Suk-kyu, Lee Eun-ju, Sung Hyun-ah and Uhm Ji-won.  The film debuted as the closing film of the Pusan International Film Festival in 2004. 
 nudity in trunk scene" depression ended interpretation of the film, this particular one being her last.

Director Kim Ki-duk, no stranger to controversy over his own films, is quoted by Chinese film magazine "Movie Watch" (看電影) in singling out The Scarlet Letter as among the key Korean dramas from recent years. He subsequently cast Sung Hyun-ah, who rose to prominence with her role in The Scarlet Letter, as the heroine in his Time (2006 film)|Time.

At the films premiere in Japan, veteran actress Kumiko Akiyoshi praised the lead performances and likened the film to a landmark in erotic thrillers after Basic Instinct and Fatal Attraction. 

==Plot==
Lee Ki-hoon is an alpha male homicide detective; intelligent and with animal instincts. His wife, classical cellist Han Soo-hyun, is submissive and seemingly perfect. Meanwhile, he is carrying on a passionate affair with his mistress Choi Ga-hee, a sultry jazz singer at a nightclub. Ki-hoon lives a double life by moving back and forth between these two women, who also happen to be schoolmates from high school. One day Ki-hoon goes to a murder scene and there he meets Ji Kyung-hee, a woman accused of murdering her husband.  

==Cast==
*Han Suk-kyu - Lee Ki-hoon
*Lee Eun-ju - Choi Ga-hee
*Uhm Ji-won - Han Soo-hyun 
*Sung Hyun-ah - Ji Kyung-hee 
*Kim Jin-geun - Jung Myung-sik
*Kim Min-sung - Detective Jo
*Jung In-gi - Detective Ahn
*Choi Kyu-hwan - Detective Choi
*Kim Hye-jin - Oh Yeon-sim
*Do Yong-gu - President Han
*Seol Ji-yoon - Madam

==Awards and nominations==
;2004 Korean Association of Film Critics Awards  
*Top Ten Films of the Year

;2004 Blue Dragon Film Awards  
* Nomination – Best Actor – Han Suk-kyu
* Nomination – Best Actress – Lee Eun-ju
* Nomination – Best Supporting Actress – Uhm Ji-won
* Nomination – Best Music – Lee Jae-jin

;2005 Grand Bell Awards
* Nomination – Best Actress – Lee Eun-ju
* Nomination – Best Art Direction – Kim Ji-su
* Nomination – Best Costume Design – Jo Yun-mi

==References==
 

==External links==
* 
* 
* 
*Production company LJ Films  

 
 
 
 
 
 
 
 
 