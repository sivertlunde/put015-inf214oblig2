The Big Man
 

{{Infobox film
| name = The Big Man
| image = The Big Man poster.jpg
| imagesize = 200 x 229
| caption = poster for the film
| director = David Leland
| producer = Don MacPherson
| starring = {{Plainlist|
* Liam Neeson
* Kenny Ireland
}}
| distributor = Miramax Films
| released =  
| runtime = 93 mins
}}
 novel of the same name by William McIlvanney. It is featured in FlixMixs Ultimate Fights DVD.

==Plot== Scottish miner (Liam Neeson) becomes unemployed during a union strike. He is unable to support his family and cannot resolve his bitterness about his situation. Desperate for money, he accepts an offer made by a Glasgow gangster to fight in an illegal bare-knuckle boxing match. A long and brutal fight follows.

==Production==
Filmed at locations in Coalburn, Glasgow and Spain.

==Cast==
*Liam Neeson as Danny Scoular
*Joanne Whalley as Beth Scoular
*Billy Connolly as Frankie
*Ian Bannen as Matt Mason
*Hugh Grant as Gordon
*Kenny Ireland as Tony
*Juliet Cadzow as Margaret Mason

==External links==
*  

 

 

 
 
 
 
 
 
 

 