Appropriate Behaviour
{{Infobox film
| name           = Appropriate Behavior
| image          = Appropriate Behaviour UK POSTER.jpg
| caption        = Film poster
| director       = Desiree Akhavan
| producer       = Cecilia Frugiuele
| writer         = Desiree Akhavan
| starring       =  
| music          = Josephine Wiggs
| cinematography = Chris Teague
| editing        = Sara Shaw
| studio         = Parkville Pictures
| distributor    = Gravitas Ventures
| released       =   
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}} British comedy Brooklyn struggling to rebuild her life after breaking up with her girlfriend Maxine (Rebecca Henderson). 

The films cast also includes Scott Adsit, Halley Feiffer, Anh Duong, Hooman Majd, Arian Moayed and Aimee Mullins, and is the second feature film by UK-based company Parkville Pictures. The film had a theatrical release on January 16, 2015 in United States  and released on March 6, 2015 in UK.  

==Plot==
Shirin is struggling to be an ideal Persian daughter, a politically correct bisexual, and a hip young Brooklynite but fails miserably in her attempt at all identities.

==Cast==
*Desiree Akhavan as Shirin
*Scott Adsit as Ken
*Rebecca Henderson as  Maxine 
*Halley Feiffer as Crystal
*Anh Duong as Nasrin
*Hooman Majd as Mehrdad
*Arian Moayed as Ali
*Aimee Mullins as Sasha

==Reception==
Appropriate Behavior received mostly positive reviews from critics. Review aggregator Rotten Tomatoes reports a 98% of positive responses from the critics.  .  On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film holds an average score of 75, based on 16 reviews, indicating a generally favorable response. 

Andrew Barker of Variety (magazine)|Variety, said in his review that "It may be a lesbian Persian-American Girls (TV series)|Girls knockoff, but writer-director-star Desiree Akhavans debut still packs plenty of punch."  David Rooney in his review for The Hollywood Reporter praised the film by saying that "The promise of fresh cultural perspectives gives way to a more amorphous slice of contemporary romantic angst comedy."  Katie Walsh of Indiewire grade the film B+ by saying that "Funny, unique, and entirely inappropriate, Appropriate Behavior is a supremely satisfying and irreverent take on the New York rom-com." 

==Accolades==
 
{| class="wikitable sortable" style="width: 99%;"
|-
! Year
! Group/Award
! Category
! Recipient
! Result
! scope="col" class="unsortable"| Ref.
|- 2014
| 30th Independent Spirit Awards Best First Screenplay Desiree Akhavan 
|  
| align="center"| 
|-
| San Diego Asian Film Festival
| Grand Jury Prize
|  
| align="center"| 
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 