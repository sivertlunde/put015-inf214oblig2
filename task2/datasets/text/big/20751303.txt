Murattu Kaalai (2012 film)
 
{{Infobox film
| name = Murattu K
| image = Murattukkaalai-Poster-2.jpg
| image_size =
| caption = Movie Poster
| director = K. Selva Bharathy
| producer = Amirtham Gunanidhi
| writer = S. P. Muthuraman K. Selva Bharathy Sneha Sindhu Suman Vivek Vivek
| music = Srikanth Deva
| cinematography = Santonio Terzio Srikanth N. B.
| studio = Surya Productions
| distributor = Ayngaran International
| released =   
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}} Tamil action film directed by K. Selva Bharathy. A remake of the [[Murattu Kaalai (1980 film)|same-titled 1980s Rajinikanth-Jaishankar Sneha in the lead roles. The film, which had been under production since 2008, released on 15 June 2012.  Murattu Kaalai. It has been dubbed in Hindi as Murattu Kaalai|Return Of Joshilay.

==Plot==
Kaalaiyan (Sundar C) is a simple person and the best Jallikattu (Bullfight) player in his village. His world revolves around his four younger brothers. The antagonist is Varadharajan (Suman (actor)|Suman), a Zamindar from the neighbouring village who lives an extravagant lifestyle. His close aide is a Transgender Saroja (Vivek (actor)|Vivek). Varadharajans sister (Sindhu Tolani) falls for the macho Kaalaiyan when he wins a Jallikattu.

In the meantime, Bhuvana (Sneha (actress)|Sneha), on the run, after Varadharajan falls for her charm and tries to molest her, takes refuge in Kaalaiyans house. However, Varadharajan decides to marry his sister to Kaalaiyan with an eye on his land. On the day of marriage, when Kaalaiyan comes to know that the bride wants to split the bond between the brothers, he calls off the marriage. Kaalaiyan takes a liking for Bhuvana and the inevitable happens – romance blossoms. What follows is predictable but there comes a surprise in the form of Saroja in the climax.

==Cast==
* Sundar C. as Kaalaiyan Sneha as Bhuvana
* Sindhu Tolani as Priya Suman as Varadharajan Vivek as Saroja Devan
* Meenal as Sarojas maid
* Periya Karuppu Thevar as Sarojas Grandfather

==Production== Murattu Kaalai. Suman would Sneha was Vikram and the film was expected to be released by 12 November 2010,   but eventually did not.

The film subsequently remained completed by unreleased due to Ayngaran Internationals financial problems which had also stopped the release of other films such as Arjunan Kadhali and Kalavadiya Pozhudugal. The film finally was promoted and released by the production house on 15 June 2012.

==Soundtrack==
{{Track listing
| extra_column = Artist(s)
| title1 = Phodhuvaaga En Manasu Thangam | extra1 = Naveen
| title2 = Punnagai Enna Vilai | extra2 = Hariharan (singer)|Hariharan, Shraviya
| title3 =Sundara Purusha | extra3 = Silambarasan|Simbhu, Anuradha Sriram
| title4 = Dhavani | extra4 = Surmuki
| title5 = Bhonda Koli | extra5 = Malathi
}}

==Critical reception==
The film received mixed to negative reviews. Times of India rated the movie 3/5 stating that, "Still, the good news here is that, surprisingly, Murattu Kaalai isnt a disaster and works to an extent. But the bad news is that this is an entirely pointless remake with hardly anything new to offer".  Anupama Subramanian of "Deccan Chronicle" rated the movie 2.5/5 praising Viveks and Sundar.Cs performances and stated that the movie is "Watchable Once". 

Rohit Ramachandran of Nowrunning.com rated it 1/5 calling the film "sickening."  Sify rated the movie as "Below Average" praising Santonio Terzios work.  Vivek Ramz of in.com rated Murattu Kaalai 2/5 calling it "a poor remake" and added that its an example of how not to remake an old blockbuster.  Camera work of Santonio Terzio was appreciated overall.

==References==
 

 
 
 
 
 
 