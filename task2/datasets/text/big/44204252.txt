The Year 01
{{Infobox film
| name           = The Year 01
| image          = 
| caption        = 
| director       = Jacques Doillon Alain Resnais Jean Rouch
| producer       =
| writer         = Gébé
| starring       = 
| music          = François Béranger Jean-Marie Dusuzeau
| cinematography = Gérard de Battista William Lubtchansky
| editing        = Noëlle Boisson
| distributor    = Cinémas Associés MK2 Éditions
| released       =  
| runtime        = 90 min.
| country        = France French
| budget         =
| gross          = $1,935,825 
}} French comedy film, directed by Jacques Doillon, Alain Resnais and Jean Rouch released in 1973.

== Synopsis ==
The film narrates a utopian abandonment, consensual and festive of the market economy and high productivity. The population decides on a number of resolutions beginning with "We stop everything" and the second "After a total downtime will be revived - reluctantly - that the services and products including lack will prove intolerable. Probably: water to drink, electricity for reading at night, the TSF to say "This is not the end of the world, this is an 01, and now a page of Celestial Mechanics". The implementation of these resolutions is the first day of a new era, Year 01. The Year 01 is emblematic of the challenge of the 1970s and covers such diverse topics as ecology, negation of authority, free love, communal living, rejection of private property and labor.

== Cast ==
 
* Josiane Balasko
* François Béranger
* Cabu
* François Cavanna
* Professeur Choron
* Christian Clavier
* Coluche
* Gérard Depardieu
* Lee Falk
* Marcel Gotlib
* Jacques Higelin
* Gérard Jugnot
* Nelly Kaplan
* Martin Lamotte
* Patrice Leconte
* Stan Lee
* Thierry Lhermitte
* Miou-Miou
* Susan Shapiro
* Frederic Tuten
 

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 

 