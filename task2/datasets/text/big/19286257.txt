Raani Samyuktha
{{Infobox film
| name = RAANI SAMYUKTHA              
| image = 
| director = Dasari Yoganand|D. Yoganand
| producer = A. C. Pillai           
| writer = Kannadasan
| screenplay = Kannadasan Padmini Ragini Ragini M. G. Chakrapani K. A. Thangavelu M. N. Rajam
| music = K. V. Mahadevan
| cinematography = P. L. Roy
| editing = M. S. Mani C. P. Jambulingam D. Durairajan Vasu
| studio = Saraswathy Pictures
| distributor = Saraswathy Pictures
| released =   
| runtime = 167 mins
| country =   India Tamil
| budget = 
}}
 Padmini and M. G. Chakrapani in lead roles. The film was based on B. Sampathkumars Prithvirajan released in 1942 and starred by P. U. Chinnappa and A. Sakunthala. 
The film, produced by A. C. Pillai, had musical score by K. V. Mahadevan and was released on 14 January 1962.  However, the film didnt do well at box-office and was flop. 

==Plot==
Prithvirajan (MGR) falls in love when he sees a portrait of princess Samyuktha (Padmini). To insult him, her father Jayachandran (MG Chakrapani) instals his statue at the entrance to the palace hall where the ‘swayamvara (a ceremony to choose the bridegroom) by the princess is to take place. Getting wind of it, Prithvirajan rushes to the spot on a horse and carries away his sweetheart, while others present watch shocked and stunned.

==Cast==
* M. G. Ramachandran Padmini
* M. N. Nambiar   Ragini
* K. A. Thangavelu
* M. G. Chakrapani
* M. N. Rajam
* M. Saroja
* S. V. Sahasranamam

==Crew==
*Producer: A. C. Pillai
*Production Company: Saraswathy Pictures
*Director: Dasari Yoganand|D. Yoganand
*Music: K. V. Mahadevan
*Lyrics: Kannadasan & Avinashi Mani
*Story: B. Sampath Kumar
*Screenplay: Kannadasan
*Dialogues: Kannadasan
*Art Direction: Kotkangaar
*Editing: M. S. Mani, C. P. Jambulingam, D. Durairajan & Vasu
*Choreography: B. Hirala, Chinni & Sampath
*Cinematography: P. L. Roy
*Stunt: Shyam Sundar
*Songa Recording: T. S. Rangasamy
*Audiography: A. Krishnan
*Dance: None

==Soundtrack== Playback singers are T. M. Soundararajan, P. Suseela, K. Jamuna Rani, T. S. Baghavathi & A. P. Komala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers || Length (m:ss) 
|- 
| 1 || Nilavenna Pesum Kuyilenna Paadum || T. M. Soundararajan, P. Suseela || 02:46
|- 
| 2 || Chitthiratthil Pennezhudhi Seerpadutthum || K. Jamuna Rani || 03:13
|- 
| 3 || Idazh Irandum Paadattum || T. M. Soundararajan, T. S. Baghavathi || 03:14
|- 
| 4 || Nenjirukkum Varaikkum Ninaivirukkum || P. Susheela || 02:55
|- 
| 5 || Oho Vennilaa O Vennilaa || T. M. Soundararajan, P. Suseela || 03:08
|- 
| 6 || Mannavar Kulam Paramma  || P. Suseela || 04:01
|- 
| 7 || Mullaimalar Kaadu Engal Mananvan Ponnaadu || A. P. Komala || 03:42
|- 
| 8 || Paavai Unakku Seidhi Teriyumaa  || P. Suseela || 03:09
|-
|}

==References==
 

==External links==
 

 
 
 
 
 


 