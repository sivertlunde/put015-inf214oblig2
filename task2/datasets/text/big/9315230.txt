Kamchatka (film)
{{Infobox film
| name           = Kamchatka
| image          = Kamchatkaposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Marcelo Piñeyro
| producer       = Pablo Bossi Pedro DAngelo Oscar Kramer Francisco Ramos
| writer         = Marcelo Piñeyro Marcelo Figueras
| narrator       =
| starring       = Ricardo Darín Cecilia Roth Tomás Fonzi Héctor Alterio
| music          = Bingen Mendizábal
| cinematography = Alfredo F. Mayo
| editing        = Juan Carlos Macías
| distributor    = Buena Vista Hispano Foxfilms
| released       =  
| runtime        = 105 minutes
| country        = Argentina Spain Spanish
| budget         = $8,000,000
| gross          = $45,379,935
}} Argentine and Spanish drama film directed by Marcelo Piñeyro and written by Piñeyro and Marcelo Figueras. The movie features Ricardo Darín, Cecilia Roth, Tomás Fonzi,   Héctor Alterio, among others. 

The motion picture is set in Argentina during the Dirty War of the 1970s and tells the story of a family hiding from the government in rural Argentina.
 Oscar Awards in the foreign language film category.

==Plot==
The film is seen through the eyes of a ten-year-old boy, Harry (Matías del Pozo), who does not know that Argentinas 1976 coup détat is impacting his life.

After witnessing the "disappearance" of dissident friends, a human rights lawyer ( , and El Enano, his little brother. (Translated as "Little Guy" in the English subtitles, played by Milton de la Canal.  The actual translation is "dwarf".)  The family adopts new identities and attempts to lead a normal life.  Later, they are joined by a student who is using the alias Lucas (Tomás Fonzi).

Their new life is difficult, but a visit with their estranged grandparents (Fernanda Mistral and Héctor Alterio) reveals that they are still a close-knit family.  Subtly hinted, however, and used as a metaphor, is the mothers constant smoking and El Enanos renewed bed-wetting. Both serve to show how stressful and precarious their situation is.

==Cast==
* Ricardo Darín as Dad, David Vincent (alias)
* Cecilia Roth as Mom
* Matías Del Pozo as Harry (alias)
* Milton De La Canal as Simón, El Enano (alias)
* Héctor Alterio as Grandfather
* Fernanda Mistral as Grandmother
* Tomás Fonzi as Lucas
* Leticia Bredice as Teacher
* Mónica Scapparone as Bertuccios Mother
* Evelyn Dominguez as Niña Morena

==Background==

===Basis of film===
 
The film is based on the real-life political events that took place in Argentina after  , between 9,000 and 30,000 people deemed left-wing "subversives" disappeared from society. 
 

===Screenplay=== Argentine Film Critics Association credited the authorship of the final script to both of them.

===Title of film=== TEG as the ultimate stand-off, and who uses it as his last resource to win. The title thus alludes to the family situation of hiding away from imminent peril as a final act of defiance before their ultimate downfall.

===Politics in Argentine films=== Veronico Cruz (1988).
 Night of Funny Dirty Little War (1983) dealt frankly with the repression, the tortures, and the disappearances during the Dirty War.

This second group of films uses metaphor and suggestive images, and hints at wider socio-political issues.  

==Critical reception==
In a review, critic Anji Milanovic called the film an "heartrending drama" and liked the look of the film. He wrote, "The cinematography is gorgeous, and the Argentine countryside looks like a fairytale, all the more distressing given the killing and torture that occurred. The terror they feel is shown in little vignettes of family life." 
 Spanish daily El País, wrote "Kamchatka has many features to be considered a masterpiece, its cinema at its best, gifted with a great strength of emotional impact. It is a tender, grievous and touching elegy. Underneath the intense silent walls of captivity, it hides the thud and rage of the immeasurable collective tragedy." 

Mercedes Santos Moray, reporting from the Havana Film Festival, liked  that director Piñeyro delivered in giving the audience a suggestive image of the tragic, historical events, and wrote, "The painful memory is represented in an intimate way. Piñeyro works both with the feelings and the reason. His film is a metaphor about the dimensions of freedom. The people still suffer but the danger has disappeared. There are only the phantoms of the past... A family in the film escape the repressions for a moment, but at last their lives are affected by the violent events." 

==Awards==
Wins
* Argentine Film Critics Association Awards: Silver Condor; Best Sound, Carlos Abbate and José Luis Díaz; 2003.
* Cartagena Film Festival: Golden India Catalina; Best Screenplay, Marcelo Figueras and Marcelo Piñeyro; 2003.
* Havana Film Festival: Best Screenplay, Marcelo Piñeyro; Grand Coral - Third Prize, Marcelo Piñeyro; 2003.
* Vancouver International Film Festival: Most Popular Film Marcelo Piñeyro; 2003.
* Young Artist Awards: Young Artist Award; Best Young Ensemble in an International Film, Tomás Fonzi, Matías Del Pozo and Milton De La Canal; 2003.

Nominations
* Argentine Film Critics Association Awards: Silver Condor; Best Actor, Ricardo Darín; Best Art Direction, Jorge Ferrari; Best Cinematography, Alfredo F. Mayo; Best Original Screenplay, Marcelo Figueras and Marcelo Piñeyro; 2003.
* Cartagena Film Festival: Golden India Catalina; Best Film, Marcelo Piñeyro; 2003.
* Flanders International Film Festival: Grand Prix, Marcelo Piñeyro; 2003.

==References==
 

==External links==
*  
*   at cinenacional.com.  
*  

 
 
 
 
 
 
 
 
 
 
 
 