Pretty Village, Pretty Flame
{{Infobox film
| name           = Pretty Village, Pretty Flame
| image          = Lepaselalepogore.jpg
| image size     = 
| alt            = 
| caption        = DVD cover
| director       = Srđan Dragojević
| producer       = Dragan Bjelogrlić Goran Bjelogrlić Milko Josifov Nikola Kojo
| writer         = Vanja Bulić Srđan Dragojević Biljana Maksić Nikola Pejaković
| narrator       = 
| starring       = Dragan Bjelogrlić Nikola Kojo Milorad Mandić Dragan Maksimović Bata Živojinovć Aleksandar Habić Laza Ristovski
| cinematography = Dušan Joksimović
| editing        = Petar Marković RTS Government Ministry of Culture Cobra Films
| distributor    = 
| released       = May 1996
| runtime        = 115 minutes FR Yugoslavia
| language       = Serbian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Bosnian War.
 Serbian cinema.  Almost 800,000 people went to see the movie in cinemas across Serbia,  which equated to approximately 8% of the countrys total population.

==Summary==
The plot, inspired by real life events that took place in the opening stages of the Bosnian War, tells a story about small group of Serb soldiers trapped in a tunnel by a Bosniak force. The films screenplay is based on an article written by Vanja Bulić for Duga magazine about the actual event. Through flashbacks that describe the pre-war lives of each trapped soldier, the film describes life in SFRY|pre-war Yugoslavia and tries to give a view as to why former neighbours and friends turned on each other.

Following the success of the movie, Bulić wrote a novel named Tunel thats essentially an expanded version of his magazine article.

==Plot==

The movies main protagonist is Milan (Dragan Bjelogrlić), a Bosnian Serb. At the beginning of the war in Bosnia, his life in his little village with his best friend Halil (Nikola Pejaković), a Bosniak, is generally quiet and reminiscent of that of a normal lifestyle in the countryside. He gradually notices that Bosniaks whom he knew in his village are slowly but surely moving out.
 Bosnian Serb Army and is attached to a squad that includes:

* Velja (  who did most of his "work" in Germany in the late 1980s and early 1990s. On a visit home, he pretended to be his younger brother, knowing full well hed be taken to the frontlines, when authorities came to pick up his draft-dodging sibling. He provides the films title. After the squad set a village alight, they watch it burn and he says: "Pretty villages are pretty when they burn. Ugly villages stay ugly, even when they burn." (in Serbian, Lepa sela lepo gore, a ružna sela ostaju ružna, čak i kad gore.)
 nostalgic feelings towards SFRJ|Yugoslavia. While some of the others loot houses, he is more interested in literature. He intermittently reads from a burnt diary he found in one of the villages the squad passed through.
 off drugs. He is the only one in the unit who speaks English.
 German or Turk set foot here", completely unaware that the person behind the wheel is a Turkish trucker driving through Serbia.

* Viljuška (Fork) (  sympathizer from a rural area in central Serbia whose only motive for signing up for war was looking out for his best man Laza. He is nicknamed Fork because he carries a fork around his neck symbolizing Serbian sophistication in the 14th century and compares Serbian kings to English and German kings at the time, who he says ate using their hands. He is very cheerful and jovial until Laza dies.
 Confederate flag when leaving the Bosniak village sitting on top of an OT M-60, and he always wear a headband with the Chinese character for "dragon" (龍) on it.

* Gvozden (  and its ideals. He walked 350&nbsp;km to attend Titos funeral in 1980. He has past history with the Bosniak squads leader as they both served together in the JNA.

* Liza Linel ( , looking for stories at the front lines. She sneaked into Speedys medical truck before he drove it into the tunnel.

Milan is generally dissatisfied with the way the war is being conducted and is disturbed by the fact that profiteers are looting and burning his best friend Halils property. Milan shoots three of the War profiteering|profiteers, wounding them and is shocked that the nearby kafana owner Slobo (Petar Božović) (at whose kafana he used to drink with Halil) is with them. Milan is later told by the kafana owner that his mother has been killed by Bosniaks from Halils squad.

As the movie progresses, Milan and his squad are ambushed and encircled by a group of Bosniak fighters. Seeing that there is no way around the issue, Milan decides to tell his surviving squad mates about a tunnel he was scared of entering as a child. The squad enters the tunnel and engages in a firefights with the Bosniak fighters. They are joined by Brzi and Liza.
 flashbacks as to what brought each of them into this situation. Liza promises to portrays the Serbians in a fair manner but it is killed by Bosniak fire when she tries to retrieve her camera. After their force has been whittled down substantially, Gvozden drives the truck out of the tunnel; the truck explodes killing both Gvozden and the Bosniak fighters, and opening the way out.

The movies climax is when Milan and Halil meet outside at the tunnels exit and openly ask each other a few important questions. Halil asks who burned down his shop while Milan asks who killed his mother; both men deny each of the actions. Halil is then killed by a Serbian artillery strike.

Milan, Petar and Brzi escape and all three are sent to the Belgrade Military Hospital, where Brzi dies.

=== Structure ===
Although the film has highly non-linear structure, most consider it easy to follow. The story is told in several time periods, which cut back and forth:
* 1971 – The prologue is done as a faux newsreel, showing the opening of the Tunnel of Brotherhood and Unity by visiting President Josip Broz Tito and local dignitary Džemal Bijedić on 27 June 1971. During the ribbon-cutting ceremony, President Tito accidentally cuts his thumb with the scissors.
* 1980 – Milan and Halil are young boys, and this thread shows their childhood friendship set somewhere in the rural Eastern Bosnia. Though the specific village is not mentioned by name it is assumed to be somewhere in the Drina valley (probably near Goražde due to GŽ letters on license plates).
* Late 1980s/early 1990s – Milan and Halil open an auto-repair shop in the village.
* 1992 – Referenced in the film as "the first day of the war", this probably takes place on March 2, as the kafana owner reads about the shooting of Nikola Gardović in Sarajevo (which occurred on March 1 and is considered by Serbs to mark the start of the war) in the Večernje novosti newspaper.
* 1994 – The main thrust of the film, in which the squad is pinned down in the tunnel.
* 1994 (the "present") – Milan and the Professor, as well as an unconscious Speedy, are in a military hospital in Belgrade.

In addition, there is a prologue and epilogue:
* Epilogue – 21 July 1999. This is also supposedly a newsreel, showing the re-opening of the reconstructed tunnel under the new name – Tunnel of Peace.

In the prologue, how President Josip Broz Tito accidentally cuts his thumb in the ribbon-cutting ceremony for the tunnel symbolizes the blood that would be shed in this tunnel. And finally, there is a scene outside the main narrative of the film in which the young Milan and Halil enter the tunnel to find out it is full of dead bodies. Two of those bodies are themselves (from 1980s/1990s), symbolizing their fates sealed in this tunnel.

==Production==
The funds for the movie were raised through a legal entity Cobra Film Department that was registered as a limited liability company by Nikola Kojo, Dragan Bjelogrlić, Goran Bjelogrlić, and Milko Josifov.  Most of the money came from the Serbian governments (under prime minister Mirko Marjanović) Ministry of Culture (headed by cabinet minister Nada Perišić-Popović) as well as from the Serbian state television Radio Television of Serbia|RTS. Reportedly, the budget raised was US$2 million. 

The shooting of the film with working title Tunel began on 19 April 1995. Majority of the scenes were shot on location in and around Višegrad, Republika Srpska (Serb inhabited entity of Bosnia and Herzegovina, at the time still governed by Radovan Karadžić), often in places that were former battlefields. In July, after 85 shooting days, the production was put on hold due to lack of funds, some 7 planned shooting days short of completion. Following a new round of fund-raising, it resumed in mid-November 1995 and finished by early 1996. 
 Voyage au bout de la nuit, literary work that had a strong effect on Dragojević when he read it in his early youth. 

== Reception and reaction ==
The film won accolades for direction, acting and brutally realistic portrayal of the war in former Yugoslavia. It was also the first Serbian film to show the Serbian side of the conflict involved in atrocities and ethnic cleansing – the title of the film is an ironic comment on the protagonists activities in a Bosnian village.

===Europe===
However, the film also caused controversy, mostly among Bosniaks (Bosnian Muslims) and to a lesser extent among some Croats , who complained about its alleged pro-Serb bias and unequal treatment of warring sides, citing differing depictions of atrocities. According to this view, Bosnian Serb atrocities are shown with ironic flair, while Bosniak atrocities are shown with utter solemnity. Conversely, the film is considered controversial by some Bosnian Serbs for its supposedly anti-Serb bias.

The film was also controversial on the European film festival circuit. The Venice Film Festival refused the film with its director Gillo Pontecorvo calling it "fascist cinema". 

Writing in Sight & Sound in November 1996, British author Misha Glenny delivered a stinging attack on critics who view Pretty Village, Pretty Flame or Emir Kusturicas Underground (1995 film)|Underground through a simplistic, reductionist pro- or anti-Serb critical lens. 

British magazine Total Film praised the film, calling it "one of the most electrifying anti-war movies ever made" and further continuing: "What this small, worthy film excels at is showing how even long friendships became perverted in the Bosnian conflict.... Small it may be, but its powerfully and perfectly formed". 

In addition to FR Yugoslavia (Serbia and Montenegro), the only other former Yugoslav countries where the movie had an official theatrical distribution were Slovenia and Macedonia. Released in Slovenia as Lepe vasi lepo gorijo, it became a cinema hit in the country with 72,000 admission tickets sold.  In Macedonia it also posted a good box office result with more than 50,000 admission tickets sold. 

Swedish public service TV broadcast it under the name "Vackra byar" ("beautiful villages")

===North America===
The reaction to the film in North America was very positive. It got plenty of press coverage following its debut showings at festivals in Montreal and Toronto.

Variety (magazine)|Varietys Emanuel Levy penned a glowing review calling the film "wilder in its black humor than MASH (film)|MASH, bolder in its vision of politics and the military than any movie Stanley Kubrick has made, and one of the most audacious antiwar statements ever committed to the bigscreen". 

Ken Fox of the TV Guide praised Dragojević for "ultimately refusing to deal in heroes and villains and never shying away from self-condemnation" while concluding that Pretty Village, Pretty Flame is "a bloody, uncompromising and surprisingly enthralling piece of antiwar filmmaking that pulls no punches and demands to be seen". 
 Toledo Blade that covered it from the Bosnian War angle.  

In his very favourable 1997 review, American online film critic James Berardinelli labelled the film "a powerful condemnation of war that shares several qualities with the German films Das Boot and Stalingrad (film)|Stalingrad". 

New York Times Lawrence Van Gelder gave kudos to Dragojević for "unleashing a powerful assault on the insanity of the war that pitted Serb against Muslim in Bosnia" and praised the film as "a clear, well-meaning, universal appeal to reason". 
 Vonnegut at his most hallucinatory", concluding overall that "the film is somewhat cliched and a little more pro-Serb than necessary, but packs a genuine punch". 

Gerald Peary wrote in April 1998 that Dragojević "creates a crass, unsentimental, muscular guys world on the way to his vivid condemnation of the Bosnian War". 

===Recent reaction===
The reaction to the film and its various aspects continues long after its theatrical and festival run ended. It is frequently used by various public figures as springboard for political, ethnic, and historical discussions and open issues that continue to plague the Balkans region.
 No Mans Land, Bosniak director and former soldier in the Army of the Republic of Bosnia and Herzegovina during the war Danis Tanović called Pretty Village, Pretty Flame "well made, but ethically problematic due to its shameful portrayal of the war in Bosnia". 
 Hague tribunal to life in prison for war crimes. The basis Hadžihafizbegović provided for such a strong accusation was the fact that in the films closing credits, the filmmakers listed the Army of Republika Srpska among the institutions that helped the production: "As far as Im concerned, by thanking the Republika Srpska Army in closing credits theyre thanking Milan Lukić and his brethren". 

==Awards==
"Lepa sela lepo gore" garnered six wins and one nomination: Festival dAngers (1997) Festival dAngers (1997)
*Won Distinguished Award of Merit at the Lauderdale International Film Festival (1996)
*Won Bronze Horse at the Stockholm International Film Festival (1996)
*Won International Jury Award at the São Paulo International Film Festival (1996)
*Won Audience Award at the Thessaloniki Film Festival (1996)
*Nominated for Golden Alexander at the Thessaloniki Film Festival (1996)

==See also==
* 1996 in film
* Cinema of Yugoslavia
* List of Yugoslavian films, 1990–2003

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 