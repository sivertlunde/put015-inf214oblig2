Live Wire (film)
 
 
{{Infobox film
| name           = Live Wire
| image          = Live Wire (1992 film) poster.jpg
| caption        = Theatrical release poster Christian Duguay David Willis Bart Baker
| writer         = Bart Baker
| starring       = Pierce Brosnan Ron Silver Ben Cross Lisa Eilbacher
| music          = Craig Safan
| cinematography = Jeffrey Jur
| editing        = Christopher Greenbury distributor     = New Line Cinema
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = $11 million
}} 1992 action Christian Duguay and starring Pierce Brosnan, Ron Silver, Ben Cross and Lisa Eilbacher.

The plot revolves around a rash of seemingly inexplicable, explosive spontaneous human combustions and Danny ONeill (Brosnan), a bomb disposal expert that gets involved and will eventually have to solve the case.

==Plot summary==
When a Senator is killed in an explosion, the FBI investigates. The agent in charge is Danny ONeill (Pierce Brosnan), who is separated from his wife Terry (Lisa Eilbacher) (due to the accidental drowning of their only child in their pool) and behaving very erratically. Initially the investigation does not reveal the kind of explosive used or even what was used to detonate it. Eventually it is learned that terrorists have developed an "invisible" liquid explosive which is activated within the human body (by stomach acid). It also does not help that they have to report to Senator Traveres (Ron Silver), the man whom ONeills wife is having an affair with and whom Danny also assaulted.

Later, another senator is killed while riding in a limousine; the limo being driven by one of the main villains henchmen.  This henchman is subsequently struck by a moving car, taken into custody and brought into court.  Since this henchman is now considered a risk by the main villain, the judge in the case is slipped the liquid and she spontaneously explodes, killing the arraigned henchman in the process.  It is at this time that ONeill discovers the cause of the explosions - the chemically enhanced water in the judges pitcher.
 
It becomes obvious that the next target is Senator Traveres, so ONeill, concerned that his wife may become collateral damage, trails his every move.  He eventually infiltrates the senators heavily guarded mansion, at a very convenient time as it is being overrun by the bad guys.  ONeill concocts a cornucopia of interesting home-made weapons, even building bombs using fertilizer found in the kitchen cabinet. All the bad guys die, except for the main one (Ben Cross), who holds ONeills wife hostage in front of him and Traveres.

The bad guy swallows some of the liquid, conceding his fate, but intending to bring them all down with him. ONeill manages to free his wife and send her to safe ground. He and Traveres however have nowhere to go and are thus subsequently forced to jump from the third floor due to the explosion caused by the villain exploding. Traveres lands on a wrought-iron fence which impales and kills him. ONeill is seen dangling by his arm from Traveres. The ending features the hero (ONeill) bagging the ultimate prize (his wife), having a second child and (presumably) living happily ever after.

==External links==
*  

 

 
 
 
 
 
 
 

 