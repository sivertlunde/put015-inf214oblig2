The Dog Who Stopped the War
 
{{Infobox film
| name = The Dog Who Stopped the War
| image = 
| caption = 
| director = André Mélançon Nicole Robert
| writer = Roger Cantin Danyèle Patenaude
| narrator = 
| starring = Cédric Jourde Marie-Pierre A. DAmour Julien Elie Minh Vu Duc Maryse Cartwright
| music = Germain Gauthier
| cinematography = François Protat
| editing = André Corriveau (filmmaker)|André Corriveau
| distributor = 
| released = 25 October 1985 (USA) 3 October 1984 (QC)
| runtime = 92 mins
| country = Canada
| language = Quebec French 
| budget = 
| preceded_by = 
| followed_by = 
}} Tales for All (Contes pour tous) series of childrens movies created by Les Productions la Fête.         

==Plot==

The film involves a huge snowball fight between the children of a town who split into two rival gangs, one defending a snow castle, the other attacking it. The attackers are led by a boy who styles himself as "General Luc" and has a reputation for being bossy. The defenders are outnumbered and led by Marc, who owns a dog named Cleo. They also have the genius boy Francois on their side.

Francois designs a massive, elaborate snow fortress, and Marcs group constructs it. Luc arrives with his army, wearing makeshift armour and wielding wooden swords. They attempt to scale the walls with a ladder, but Luc is injured in the battle and orders a retreat. They regroup and stage a second, more covert attack, but they are spotted and beaten back again with snowballs soaked in ink.

Luc counters by attacking a third time, this time with his army dressed in garbage bags as protection from the ink. They overwhelm the forts defences, and Marc and Francois escape via toboggan through a secret tunnel. The two groups meet and agree to have one final battle to determine the winner.

Luc shows up for the final siege with an even larger army, having recruited additional (younger) children with chocolate. They also possess new weapons such as slingshots and a snowball cannon. Luc orders them to charge, and despite being slowed by barricades, they eventually breach the fortress walls and engage in melee combat with the defenders. Marcs dog Cleo comes after her owner, and one of the fortress walls collapses, killing her. The war ends, as both sides help bury her.

The song at the end of the movie is performed by Nathalie Simard.

An iconic line, "La guerre, la guerre, cest pas une raison pour se faire mal!" (Directly translated: War, war, its not a reason to hurt each other!) became a popular slogan against the Iraq war in 2003.   

==See also==
*Culture of Quebec
*Cinema of Quebec
*List of Quebec movies
*List of Canadian films

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 


 
 