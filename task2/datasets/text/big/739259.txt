Blind Date (1987 film)
{{Infobox film
| name = Blind Date
| image = Blinddateposter.jpg
| caption = Theatrical release poster
| director = Blake Edwards Tony Adams
| writer = Dale Launer
| starring = Bruce Willis Kim Basinger John Larroquette William Daniels Phil Hartman
| music = Henry Mancini
| cinematography = Harry Stradling Jr.
| editing = Robert Pergament
| distributor = TriStar Pictures
| released =  
| runtime = 95 minutes
| country = United States English
| budget = $18 million (estimated) 
| gross = $39,321,715 
}}

Blind Date is a 1987 romantic comedy film, directed by Blake Edwards and starring Bruce Willis in his first leading film role and Kim Basinger.
 Madonna and Sean Penn, but both backed out after the project failed to attract a director. The screenplay was re-written and this draft was given to Edwards. He agreed contingent he be allowed re-write that draft. The studio agreed. At that point Penn dropped out and Madonna met with Edwards and she dropped out as well. The movie was re-cast with Willis and Basinger. Blind Date earned mostly negative reviews from critics, but was a financial success and opened at number one at the box office.

==Plot==
Walter Davis (Willis) allows his brother, Ted (  with his wifes cousin, Nadia (Basinger).

Nadia is shy and the two experience some awkwardness. However, as the evening goes on, Nadia begins to drink and behave in a wild manner. (A warning about her behavior under the influence of alcohol had been given by Ted but disregarded by Walter, thinking it was a joke.) 

To make matters worse, Nadias jealous ex-boyfriend, David (John Larroquette), shows up and exacerbates the situation by stalking the couple all night, assaulting and attempting to assault Walter several times, even ramming Walters car with his own. 
 moonwalk before firing at the frightened mans feet.

Nadia posts $10,000 in bail and agrees to marry David if he will help Walter avoid prison time. Before the wedding, Walter gives Nadia chocolates filled with brandy. Walter attempts to stop the wedding. Chaos ensues.

==Cast==
* Bruce Willis as Walter Davis
* Kim Basinger as Nadia Gates
* John Larroquette as David Bedford
* William Daniels as Judge Harold Bedford
* George Coe as Harry Gruen
* Mark Blum as Denny Gordon
* Phil Hartman as Ted Davis
* Stephanie Faracy as Susie Davis
* Alice Hirson as Muriel Bedford
* Stanley Jordan as himself
* Graham Stark as Jordan the Butler
* Joyce Van Patten as Nadias Mother 
* Barry Sobel as Gas Station Attendant
* Armin Shimerman as French Waiter
* Brian George as Maitre d
* Dick Durock as Bouncer
* Sab Shimono as Mr. Yakamoto
* Momo Yashima as Mrs. Yakamoto
* Herb Tanney as Minister

==Production==
Billy Vera & The Beaters appear in the bar scene, playing several songs.

==Reception==
Blind Date received negative reviews from critics, as it holds a 21% rating on Rotten Tomatoes based on 24 reviews.

==Soundtrack==
The soundtrack to the motion picture was released by Rhino Records in 1987.

===Track listing===
# "Simply Meant To Be" - Gary Morris & Jennifer Warnes
# "Let You Get Away" - Billy Vera & The Beaters
# "Oh, What A Nite" - Billy Vera & The Beaters
# "Anybody Seen Her?" - Billy Vera & The Beaters
# "Talked About Lover" - Keith LNeire
# "Crash, Bang, Boom" - Hubert Tubbs
# "Something For Nash" - Henry Mancini
# "Treasures" - Stanley Jordan
# "Simply Meant To Be (Instrumental)" - Henry Mancini

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 