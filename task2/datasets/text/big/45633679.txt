Hosa Neeru
{{Infobox film|
| name = Hosa Neeru
| image = 
| caption =
| director = K. V. Jayaram
| producer = K. S. Sacchidananda
| writer = K. V. Raju
| starring = Ananth Nag  Suhasini  Lokanath
| music = G. K. Venkatesh
| cinematography = B. S. Basavaraj
| editing = S. Manohar
| studio = Panchama Movie Makers
| released = 1986
| runtime = 144 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada film directed by K. V. Jayaram and is written by K. V. Raju. The film starred Ananth Nag and Suhasini in the lead roles. 

The films score and  songs were composed by G. K. Venkatesh. The film, upon release, was critically acclaimed and won multiple awards at the Karnataka State Film Awards for the year 1985-86.


== Cast ==
* Ananth Nag
* Suhasini
* Loknath
* Lohithaswa
* Pandari Bai
* Shashikala
* Jaggesh
* Dingri Nagaraj
* Chethan Ramarao
* Thimmaiah


== Soundtrack ==
The music was composed by G. K. Venkatesh with lyrics by R. N. Jayagopal. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Neene Nanna Preethi
| extra1 = S. P. Balasubrahmanyam
| music1 = 
| length1 =
| title2 = Neene Indu Prema Veene
| extra2 = S. P. Balasubramanyam, S. Janaki
| music2 = 
| length2 = 
| title3 = Koreva Chaliyali
| extra3 = S. P. Balasubramanyam, S. Janaki
| music3 =
| length3 = 
| title4 = Olavu Thanda Besuge
| extra4 = S. P. Balasubramanyam, S. Janaki
| music4 =
| length4 =
| title4 = Naavella Ondagi
| extra4 = S. P. Balasubramanyam, S. Janaki
| music4 =
| length4 = 
}}


==Awards==
* 1985 -86 :Karnataka State Film Awards
 Best Film Best Actor - Ananth Nag Best Supporting Actress - Shashikala Best Music director - G. K. Venkatesh


== References ==
 

== External links ==
*  
*  


 
 
 
 
 
 
 



 