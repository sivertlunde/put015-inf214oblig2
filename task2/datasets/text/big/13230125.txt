Enemies of Happiness
 
{{Infobox film
| name           = Enemies of Happiness
| image          = 
| image_size     = 
| caption        = 
| director       = Eva Mulvad
| producer       = Helle Faber
| writer         = 
| starring       = Mardina Parmach (Interpreter)
| music          = Thomas Knak, Jesper Skaaning, Anders Remmer
| cinematography = Zillah Bowes
| editing        = Adam Nielsen
| studio         = Bastard Film
| distributor    = Women Make Movies, The Danish Film Institute
| released       = November 10, 2006
| runtime        = 58 minutes
| country        = Denmark
| language       = English 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
|}}
 Afghan politician Afghan Parliament Malalai Joya filmed by Danish director Eva Mulvad.
 2005 Afghan parliamentary election, which was the first democratic election in 30 years in Afghanistan. The film gives deep insight into the living conditions of the Afghan population.

== Awards ==
* 2006, Silver Wolf Award, IDFA, Amsterdam
* 2007, World Cinema Jury Prize: Documentary, Sundance Film Festival
* 2007, Special Jury Mention, Silverdocs 2007
* 2007, Best long documentary, Festival Films De Femmes, High School Jury, Creteil
* 2007, International Premier Award, One World Media Awards, London
* 2007, Nestor Almendros Prize, Human Rights Watch Film Festival, New York
* 2007, Special Mention, One World Film Festival
* 2007, Audience Mention Best Documentary, 15 Mostra Internacional de film de dones, Barcelona

== References  ==
 

==External links==
*  
*  
*  

 
 
{{Succession box
| title= 
| years=2007
| before=In the Pit
| after=Man on Wire}}
 

 
 
 
 
 
 
 
 

 
 