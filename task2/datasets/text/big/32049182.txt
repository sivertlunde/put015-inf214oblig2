I Was an Adventuress
{{Infobox film
| name           = I Was an Adventuress
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Gregory Ratoff
| producer       = Nunnally Johnson
| based on       =  
| screenplay     = Karl Tunberg Don Ettlinger John OHara
| narrator       = 
| starring       = Vera Zorina Richard Greene Erich von Stroheim Peter Lorre
| music          = David Buttolph
| cinematography = Leon Shamroy Edward Cronjager
| editing        = Francis D. Lyon
| studio         = 
| distributor    = Twentieth Century Fox Film Corporation
| released       =  
| runtime        = 81 min
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

I Was an Adventuress is a 1940 American drama film directed by Gregory Ratoff, starring Vera Zorina, Richard Greene, Erich von Stroheim and Peter Lorre. Actress/ballerina Countess Tanya Vronsky (Vera Zorina) is a phony countess, working in concert with two international con artists Andre Desormeaux (Erich von Stroheim) and Polo (Peter Lorre). 

==Plot summary==
Countess Tanya Vronsky acts as bait for notorious jewel thief Andre Desormeaux and his assistant, Polo, on their tour through Europe. Everything goes according to plan until Tanya falls in love with their next target, Paul Vernay. Still, the gang manages to take Paul to the cleaners. After the heist, Tanya announces that she is retiring and goes on to marry Paul. Desormeaux tries to convince her to change her mind, but in vain.

They meet again months later in Paris, and Desormeaux makes another attempt at getting the countess to work with them again. In order to get rid of them for good, she pretends to go along with their plans, but instead sets them up to be caught.

However, her plan is thwarted, and her two accomplices come to her home during a party and pretend to be guests. They manage to steal the guests jewels and escape from the house without getting caught. Before they leave, Desormeaux blackmails Tanya, threatening to tell Paul about her past if he doesnt get 200,000 Francs.

The theft is soon discovered and Paul and Tanya come after the thieves in their car. On the way Paul gets to hear about Tanyas background and forgives her. While they are away, Polo returns to their house in a sudden change of heart, and gives back the jewels. Meanwhile, Desormeaux is boarding a ship that will take them both to America.

Paul and Tanya come back and discover that the jewels have been returned. They are impressed by Polos act and forgive him. Tanya entertains her guests at the Vernay mansion, by performing a dance from the ballet "Swan Lake".

Polo then goes to the ship and boards it, pretending to still have the jewels in a briefcase. After boarding the ship he pretends to accidentally drop the suitcase in the water. 

==Cast==
 
* Vera Zorina as Countess Tanya Vronsky (as Zorina)  
* Richard Greene as Paul Vernay  
* Erich von Stroheim as Andre Desormeaux (as Erich Von Stroheim)  
* Peter Lorre as Polo  
* Sig Ruman as Herr Protz (as Sig Rumann)  
* Fritz Feld as Henri Gautier  
* Cora Witherspoon as Aunt Cecile  
* Anthony Kemble-Cooper as Cousin Emil (as Anthony Kemple Cooper)  
* Paul Porcasi as Fisherman  
* Inez Palange as Fishermans Wife  
* Egon Brecher as Jacques Dubois  
* Roger Imhof as Henrich Von Kongen  
* Rolfe Sedan as Waiter  
* Eddie Conrad as Waiter  
* Fortunio Bonanova as Orchestra Leader
* George Balanchine as Ballet Dancer 
* Lew Christensen as Ballet Dancer
* Phyllis Barry as Englishwoman at Exhibit
* Charles Laskey as Ballet Dancer
* Ellinor Vanderveer as Englishwoman at Exhibit
 

==Notes==
* Vera Zorina, whose real name was Brigita Hartwig, was married to dance director George Balanchine.   
* The twelve minute ballet sequence of Swan Lake was the longest ballet scene to appear in any film to date. For the scene, a $15,000 ( ) all-glass set, the first of its kind, was built using   of ¼-inch plate glass. 

==References==
 	

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 

 