Dangerous Paradise
{{Infobox film
| name = Dangerous Paradise
| image =
| image_size =
| caption =
| director = William A. Wellman
| producer = B.P. Schulberg
| writer = Joseph Conrad (novel)   Grover Jones   William Slavens McNutt
| narrator =
| starring = Nancy Carroll   Richard Arlen   Warner Oland   Gustav von Seyffertitz
| music = 
| cinematography = Archie Stout    
| editing = 
| studio = Paramount Pictures
| distributor = Paramount Pictures
| released = February 13, 1930
| runtime = 
| country = United States English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Paramount at the Joinville Studios in Paris.  

==Cast==
* Nancy Carroll as Alma 
* Richard Arlen as Heyst 
* Warner Oland as Schomberg 
* Gustav von Seyffertitz as Mr. Jones 
* Francis McDonald as Ricardo 
* George Kotsonaros as Pedro 
* Dorothea Wolbert as Mrs. Schomberg  Clarence Wilson as Zangiacomo
* Evelyn Selbie as Mrs. Zangiacomo 
* Willie Fung as Wang 
* Mrs. Wong Wing as Mrs. Wang

==References==
 

==Bibliography==
* Moore, Gene M. Conrad on Film. Cambridge University Press, 2006.

==External links==
* 
*   for the German language version

 

 
 
 
 
 
 
 
 
 
 

 