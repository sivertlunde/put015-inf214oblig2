Naalo Unna Prema
{{Infobox film
| name           = Naalo Unna Prema
| image          =
| caption        =
| writer         = Diwakar Babu  
| story          = Seetaram Karanth
| screenplay     = V.R Pratap
| producer       = K.L.N. Raju
| director       = V.R Pratap Laya   Gajala Koti
| cinematography = Ajay Vincent
| editing        = Nandamuri Hari
| studio         = Sri Sairam Productions
| released       =  
| runtime        = 2:19:08
| country        = India
| language       = Telugu
| budget         =
| preceded_by    =
| followed_by    =
}}
 romantic drama film produced by K.L.N. Raju on Sri Sairam Productions banner and directed by V. R. Pratap. The film stars Jagapati Babu, Laya (actress)|Laya, Gajala in the lead roles and music was composed by Saluri Koteswara Rao|Koti.           This film is first debut of Gajala as heroine in Telugu and she is named as Raji in titles by her character name in the film. Later her screen name was changed into Gajala from the film Student No.1 by K. Raghavendra Rao. The film was a remake of Kannada film Chandramukhi Pranasakhi.   

==Plot==
Hema (Laya (actress)|Laya) is a professional singer from Vizag. Sai Krishna (Jagapati Babu) is her secret admirer from Hyderabad. Raaji (Gajala) is the naughty younger sister of Hema. Sai Krishna, Hema and Raaji meet at a marriage function. Raaji tries to do mischief with Sai Krishna for fun. Sai Krishna also teases her, which Raaji takes very personal.

Sai Krishna takes the address of Hema and starts writing fan letters to her. Raaji plays mischief by opening them and writing replies to Sai Krishna in the guise of Hema. Over a period of love, then fall in love through letters and phone calls. Sai Krishna does not know that its Raaji, who is writing letters to him. Raaji too sincerely falls in love with Sai Krishna and fears that he might not talk to her if she tells him the truth.

The rest of the movie is all about how the truth is revealed to Sai Krishna and whom he does marry at the end.

==Cast==
* Jagapati Babu as Sai Krishna Laya as Hema
* Gajala as Raji Ranganath as Sais father
* Giri Babu as Hemas father
* M. S. Narayana as Tabelu Vamana Rao
* L. B. Sriram as Bose Annapurna as Sais mother
* Kovai Sarala as Sarala
* Indu Anand
* Ram Jagan
* Ananth
* Visweswara Rao

==Soundtrack==
{{Infobox album
| Name        = Naalo Unna Prema
| Tagline     =
| Type        = film Koti
| Cover       =
| Released    = 2001
| Recorded    =
| Genre       = Soundtrack
| Length      = 32:07
| Label       = Aditya Music Koti
| Reviews     =
| Last album  = Veedekkadi Mogudandi   (2001)
| This album  = Naalo Unna Prema   (2001)
| Next album  = Prema Sandadi   (2001)
}}

Music composed by Saluri Koteswara Rao|Koti.  Music released on ADITYA Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 32:07
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Veeche Chirugaali Sirivennela Sitarama Sastry SP Charan
| length1 = 4:48

| title2  = O Naa Priyathama
| lyrics2 = Sirivennela Sitarama Sastry Chitra
| length2 = 4:35

| title3  = Enno Enno
| lyrics3 = Sirivennela Sitarama Sastry Sujatha
| length3 = 4:16

| title4  = Edalo Okate Korika
| lyrics4 = Potula Ravikiran
| extra4  = Tippu, Sujatha
| length4 = 4:19

| title5  = Gopala Krishnudamma
| lyrics5 = Sirivennela Sitarama Sastry SP Balu, Chitra
| length5 = 4:46

| title6  = Manasa O Manasa 
| lyrics6 = Sriharsha
| extra6  = Chitra
| length6 = 5:05

| title7  = Veeche Chirugaali (F)
| lyrics7 = Sirivennela Sitarama Sastry
| extra7  = Chitra
| length7 = 4:48
}}
   

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 

 