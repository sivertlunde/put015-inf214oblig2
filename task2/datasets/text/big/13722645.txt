Manila by Night
{{Infobox film
| name           = Manila By Night
| image          = File:Manilabynight.jpg
| image size     =
| caption        =
| director       = Ishmael Bernal
| producer       = Lily Y. Monteverde
| writer         = Ishmael Bernal, Ricardo Lee
| narrator       =
| starring       = Charito Solis
| music          = The Vanishing Tribe
| cinematography = Ely Cruz, Sergio Lobo
| editing        = Augusto Salvador
| distributor    =Regal Films
| released       = 1980
| runtime        =
| country        = Philippines English
| budget         = image_size = }} 1980 Philippines|Filipino Gawad Urian Award winning drama film directed by critically acclaimed director Ishmael Bernal and starred Gina Alajar and Charito Solis. Released at the height of the Marcos regime, the film uncovers the other face of Manila by depicting the ugly aspects of life in the city - unemployment, prostitution, drug addiction, and lack of decent housing. Considered as one of Bernals masterpieces, it is an epic multi-narrative of people who have shady pasts and are trying to exist in an unforgiving world. 

==Synopsis==
The films events take place in the course of several nights, involving various protagonists and the city itself. William Martinez plays a folk singer from a rich family who becomes addicted to heroin through the influence of lesbian pusher and pimp, Cherie Gil. Martinezs mother in the movie, played by Charito Solis, is herself a reformed prostitute who, like Lady Macbeth, is obsessed with cleaning her hands to remove the dirt of her past. She does her best to be respectable after marrying an ex-cop played by Johnny Wilson. Meanwhile, Cherie Gils character is in love with a blind masseuse, played by Rio Locsin, with two illegitimate children. Locsin lives with Jojo Santiago, whose character fantasizes of earning American dollars while working in Saudi Arabia. Another character, portrayed by Alma Moreno, is a nurse who, in reality, is a call girl. Her live-in taxi-driver lover, played by Orestes Ojeda, is fooling around with a waitress played by Lorna Tolentino, who is the presumed girlfriend of a gay couterier played by Bernardo Bernardo. As dawn breaks over the city, the bizarre lives of the characters of Manilas nightlife seem like an alter-ego of the respectable, busy daytime world.

==Cast==
*Gina Alajar
*Charmie Benavidez
*Bernardo Bernardo
*Cherie Gil as Kano
*Anton Juan
*Rio Locsin William Martinez
*Alma Moreno
*Orestes Ojeda
*Charito Solis
*Lorna Tolentino
*Cris Vertido
*Johnny Wilson
*Ernie Zarate

==External links==
*  

 
 
 
 
 
 
 

 
 