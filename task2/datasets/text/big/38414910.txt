Hospitals Don't Burn Down
{{Infobox film
| name           = Hospitals Dont Burn Down
| image          = 
| image size     =
| caption        = 
| director       = Brian Trenchard-Smith
| producer       = 
| writer         = Anne Brooksbank Chris McGill
| based on = 
| narrator       =
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio = Film Australia Veteran Affairs Department
| distributor    = 
| released       = 1978
| runtime        = 24 mins
| country        = Australia
| language       = English
| budget         = $90,000  
 
| gross = 
| preceded by    =
| followed by    =
}}
Hospitals Dont Burn Down is a 1978 short dramatised documentary film directed by Brian Trenchard-Smith about a fire at a hospital.

==Plot==
A tossed cigarette from a patient causes fire to break out after midnight in a multi-storey hospital, cutting the top floors off from escape. It spreads quickly and despite the prompt action of the Fire Department, lack of fire safety training results in several fatalities.

==Production==
The movie was widely screened around the world and won a number of prizes. Trenchard Smith says it is one of the movies of which he is most proud:
   won all sorts of industrial safety awards all over the world, and was Australias highest-selling industrial film for 25 years, used all over the world. We staged a fire, cutting a multi-story hospital in half, bursting from the laundry chute out onto one floor. And the film was designed to have a whole series of lessons to be learned in a lecture afterwards. There were alarming incidents of smoking-related fires in their hospitals. It actually became a fire-safety film worldwide. Its actually a film I’m proud to have made, and I made it for very little money, but I’m very pleased that I spent the four months that I did making that. I was told that one hospital changed their arrangements after seeing the film, and moved the non-ambulatory patients from the fourth floor to the ground floor, and several months later, the fourth floor caught fire.  

==References==
 

==External links==
* 
* 
*  at Film Australia YouTube Channel
*  at Documentary Australian Foundation
*  at Oz Movies
 

 
 
 
 
 
 
 


 
 