Premi No.1
{{Infobox film
| name = Premi No. 1
| image =
| image_size=
| caption = Poster for Premi No. 1
| director =Debu Pattnaik
| writer =
| starring = Anuvab Mohanty Koel Mallick Rahul Dev
| producer =Surinder Films
| distributor =Nispal Singh
| cinematography =
| editing =
| released =  
| country        =India
| budget         =
| gross          =
| runtime = 120 minutes
| language =Oriya
| music =Swarup Nayak
}} Oriya film (dubbed in Cinema of West Bengal|Bengali) directed by Debu Pattnaik. The film stars Anuvab Mohanty, Koel Mallick, Rahul Dev. {{Cite news|url=http://www.telegraphindia.com/1090302/jsp/entertainment/story_10610499.jsp
|title=Comedy of errors|publisher=www.telegraphindia.com|accessdate=2009-03-02|last=Das|first=Mohua|location=Calcutta, India|date=2 March 2009}}
  {{Cite news|url=http://articles.timesofindia.indiatimes.com/2009-03-01/bengali/28053878_1_anubhav-preeti-rohit|title=Premi No.1 -Bengali-Movie Reviews
|publisher=indiatimes.com|accessdate=2009-03-02|last=|first=|first1=Roshni|last1=Mukherjee|date=1 March 2009}}  It is a remake of the Telugu superhit Dil (2003 film)|Dil starring Nitin Kumar Reddy|Nitin.

==Plot==
The story of Premi No. 1 revolves round Rohit (Anuvab Mohanty). He was sent to town by his father for higher studies; in college he falls in love with Preiti (Koel Mallick), the sister of the towns famous goon, Ranjit (Rahul Dev). A rivalry grows between Rohit and Ranjit — after several incidents ultimately Ranjit allows his sister to marry Rohit.

==Cast==
* Anuvab Mohanty...Rohit
* Koel Mallick...Preeti
* Rahul Dev...Ranjit

==Music==
* "E Sara Duniare Khiojile Paiba Nahin Mo Pari Premi" — Kumar Bapi & Tapu
* "Emiti Jhia Ku Mun Karichi Mo Priya" — Kumar Bapi
* "To Naan Re Mun Gote Geeta Lekhichi" — T. Souri & Tapu Mishra
* "Tu Nahin Kichi Mate Bhala Lagu Nahin O My Love" — Udit Narayan & Ira Mohanty

==References==
 

==External links==
*  

 

 
 
 
 
 

 