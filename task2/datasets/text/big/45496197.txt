King of the Jungle (1933 film)
{{Infobox film
| name           = King of the Jungle
| image          = 
| alt            = 
| caption        =
| director       = H. Bruce Humberstone Max Marcin
| producer       = 
| screenplay     = Charles Thurley Stoneham Max Marcin Philip Wylie Fred Niblo, Jr.
| starring       = Buster Crabbe Frances Dee Sidney Toler Nydia Westman Robert Barrat Irving Pichel Douglass Dumbrille
| music          = Herman Hand Rudolph G. Kopp John Leipold
| cinematography = Ernest Haller
| editing        = 
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

King of the Jungle is a 1933 American adventure film directed by H. Bruce Humberstone and Max Marcin and written by Charles Thurley Stoneham, Max Marcin, Fred Niblo, Jr. and Philip Wylie. The film stars Buster Crabbe, Frances Dee, Sidney Toler, Nydia Westman, Robert Barrat, Irving Pichel and Douglass Dumbrille. {{cite web|url=http://www.nytimes.com/movie/review?res=9D03E7DB1531EF3ABC4D51DFB4668388629EDE|title=Movie Review -
  King of the Jungle - Buster Crabbe as a Wild Jungle Man Who Under- stands Lions Better Than He Does Human Beings. - NYTimes.com|work=nytimes.com|accessdate=24 February 2015}}   The film was released on March 10, 1933, by Paramount Pictures.
 
==Plot==
 

== Cast ==
*Buster Crabbe as Kaspa the Lion Man
*Frances Dee as Ann Rogers
*Sidney Toler as Neil Forbes
*Nydia Westman as Sue
*Robert Barrat as Joe Nolan
*Irving Pichel as Corey
*Douglass Dumbrille as Ed Peters
*Sam Baker as Gwana
*Patricia Farley as Kitty
*Ronnie Cosby as Kaspa at age three

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 