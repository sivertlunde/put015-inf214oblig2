Dancing Arabs
 
{{Infobox film
| name           = Dancing Arabs
| image          = 
| caption        = 
| director       = Eran Riklis
| producer       = 
| writer         = Sayed Kashua
| starring       = Tawfeek Barhom
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Israel
| language       = Arabic, Hebrew
| budget         = 
}}

Dancing Arabs ( ,  ) is a 2014 Israeli drama film directed by Eran Riklis. It is based on Sayed Kashua book Dancing Arabs (2002).

It tells the story of Eyad, a Palestinian teenager from Tira who moves to Jerusalem to study at a Jewish high-school where he meets Naomi, a Jewish student and falls in love with her. As part of his community service, he meets Jonathan, who suffers from muscular dystrophy, and his mother, Edna. 

==Cast==
* Tawfeek Barhom as Eyad
* Daniel Kitsis as Naomi
* Ali Suliman as Salah
* Yael Abecassis as Edna
* Norman Issa as Jamel
* Michael Moshonov as Jonathan

==See also==
*  

==References==
 

==External links==
*  

 
 
 
 
 

 