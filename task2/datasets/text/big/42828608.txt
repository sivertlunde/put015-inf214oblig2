High Hopes in South Africa
{{Infobox film
| name           = High Hopes In South Africa 
| image          = 
| caption        =
| director       = 
| producer       =
| writer         =
| starring       = Bruce Springsteen & the E Street Band, Tom Morello
| music          =
| cinematography =
| editing        =
| distributor    = Sony Music
| released       = May 10, 2014
| runtime        = 45&nbsp;minutes
| country        = Netherlands
| location       = 
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}

High Hopes In South Africa is a 2014 documentary film documenting the first-ever South Africa concerts by Bruce Springsteen and the E Street Band with special guest Tom Morello during their High Hopes Tour in January 2014. The film was released in the Netherlands in May 2014 through Sony Music Netherlands and was also posted to YouTube. 

Footage from the bands pre-tour sound checks and press gatherings along with extensive interviews by Dutch journalists and filmmakers Leon Verdonschot, Art Rooijakkershad and Erik van Bruggen with Steven Van Zandt and Tom Morello are included. The film also followed South African fans at their homes, work and at the shows in Cape Town and Johannesburg. 

==References==
 

 

 