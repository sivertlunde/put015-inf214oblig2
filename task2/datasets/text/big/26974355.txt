Somewhere in France
{{Infobox film
|name= Somewhere in France
|image=
|caption=
|director= Charles Giblyn
|producer= Thomas H. Ince
|writer= J. G. Hawks
|starring= Louise Glaum Howard C. Hickman Dal Clawson
|editing= studio =
|distributor= Triangle Film Corporation
|released=  
|runtime= 50 minutes (5-reel#Motion picture terminology|reels) Silent with English intertitles
|country= United States
|budget=
}} silent era war espionage drama film|motion picture starring Louise Glaum and Howard C. Hickman.
  Directed by produced by adapted by same title by Richard Harding Davis, which was also serialized in The Saturday Evening Post.
  production companies distributed through the Triangle Film Corporation.
 
Glaum brings her  " to this feature length war drama.
 
==Plot==
An evil French woman, Marie Chaumontel (played by Glaum), is a spy for the Germans during   and seduces officers of the French high command, accumulating state secrets and then discarding her lovers.
 
Chaumontel is the mistress of Captain Henry Ravignac (played by Storm). She steals some papers from him and gives them to the Germans, then escapes to Berlin. He is tried and found guilty of neglect. He then commits suicide. His brother, Lieutenant Charles Ravignac (played by Hickman), vows revenge on Chaumontel. Pretending to be a spy, he goes to work for the Germans and becomes her assistant. He poses as a chauffeur of her phony countess.
 
When he gathers enough evidence against her, he turns the information over to the Allies of World War I|Allies. Chaumontel is arrested by French authorities for her foul deeds and sent to prison. He is then hailed as a hero for damaging German espionage operations.
 
==Cast in credits order==
*Louise Glaum as Marie Chaumontel
*Howard C. Hickman as Lt. Charles Ravignac
*Joseph J. Dowling as Gen. Andres
*Fanny Midgley as Madame Benet
*Jerome Storm as Capt. Henry Ravignac George Fisher as Herr Vogel
*William Fairbanks as Capt. Pierre Thierry
 
==External links==
*  AFI Catalog of Feature Films
 
 
 
 
 
 
 
 
 
 