Guns, Girls and Gambling
{{Infobox film
| name = Guns, Girls and Gambling
| image = File:Girls-guns-gambling.jpeg
| alt = 
| caption = DVD cover
| director = Michael Winnick
| producer = Michael Winnick Henry Boger Bob Yari
| writer = Michael Winnick
| starring = Gary Oldman Christian Slater Megan Park
| music = Jeff Cardoni
| cinematography = Jonathan Hale
| editing = Robert A. Ferretti Yari Film Group Freefall Films
| distributor = Universal Studios
| released =  
| runtime = 90 minutes
| country = United States
| language = English
}} Tony Cox, Chris Kattan, Dane Cook, and Jeff Fahey.

==Plot==
John Smith (Christian Slater) is down on his luck.  His girlfriend left him for a doctor.  He headed to an Indian Reservation casino only to have his wallet stolen by a hooker.  His money was in a security wallet though, so he enters an Elvis Impersonation contest and loses but then proceeds to plays cards with 4 other Elvis impersonators:  The Winner Elvis (Gary Oldman), Gay Elvis (Chris Kattan), Little Person Elvis (Tony Cox), and Asian Elvis (Anthony Brandon Wong).  They beat him at cards enough times to bankrupt him and then he dozes off.  He gets woken up by casino security guards who think he stole a priceless ancient Indian mask.

It turns out that the owner of the casino "The Chief" had the ancient Indian Mask in a saferoom and now its gone and witnesses saw an Elvis impersonator steal it, so John becomes the prime suspect.  The two Indian security guards figure John didnt take it but they still decide to kill him while they hunt for the real culprit, presumably Winner Elvis.  The Chief says he will pay 1 million dollars for its return.  The last thing John sees is them opening the trunk of his car, then writing down Elviss address, then they knock him out.

Meanwhile, we get introduced to a hitwoman known as "The Blonde" who quotes Edgar Allen Poe before killing her victims.  She confronts Gay Elvis and asks where the mask is.  He tells her he doesnt have it.  She kills him and leaves one of John Smiths credit cards on his corpse (apparently she is in cahoots with Johns wallet thief).

John regains consciousness and is still in the trunk of the car.  He busts out and finds the Indian security guards shot dead.  He gets Winner Elviss address and goes to his house.  When he gets home, he meets Elviss neighbor Cindy (Megan Park).  They realize the mask isnt there, but they find Asian Elviss address and go to his house.  On the way there, they are almost shot to death by another hitman "The Cowboy" (Jeff Fahey) and his sidekick Mo.  They also encounter "The Rancher" (Powers Boothe).  He explains that he hired the Elvises to steal the mask because he used to have it.  It is revealed through a flashback that back in the 1960s, a family consisting of a father, mother, and son were working for the rancher and transporting the mask.  They reached a train station only to get ambushed by Indians who killed the family and took back the mask.  Now he wants it back and will pay 1 million dollars to whoever gets it.

They leave and go to Asian Elviss house.  He tries to kill them before getting tomahawked to death by another hitman "The Indian".  They escape and head back to Elviss house only to have Little Man Elvis show up with a gun asking them where the mask is.  Before he can shoot them, The Blonde shows up and kills Little Man Elvis.  John suggests calling the sheriff only to have Cindy explain that there are two sheriffs and both are corrupt.  One is on The Chiefs payroll and the other is on The Ranchers payroll.  As they are leaving, they run into The Sheriffs who arrest John for Little Man Elviss murder.  However, Cindy posts bail and they are released.  But the Sheriffs know that John knows more than hes letting on so they follow him.

Meanwhile, Winner Elvis is on the way to delivering the mask only to have his car break down.  He hikes a long way and gets to a bus stop.  As he boards the bus, The Blonde enters and shoots him.  As he is dying he whispers something to The Blonde and she smiles real big.  The bus driver is on the phone with someone and The Blonde deduces that he is in cahoots with The Rancher and The Chief playing both sides.  She kills him as well.

Later, John, Cindy, and The Sheriffs make it to the bus.  They see Elvis and the bus drivers bodies inside the bus and written on them is "Bring the mask and John Smith to Station 12".  Station 12 is the same station where the family was killed in the 60s.  As theyre leaving, The Cowboy and Mo show up and reveal that The Cowboy is a fast sharpshooter who killed the two Indian security guards earlier in the film.  As The Sheriffs are drawing their guns, The Cowboy kills them both.  Then The Indian shows up and tomahawks The Cowboy and Mo before they can get a shot off.  The Indian accompanies them to Station 12.

As they show up, The Rancher and The Chief also show up each holding a briefcase with 1 million dollars.  At this point, Cindy reveals that she is The Ranchers daughter and was following John to make sure he would find the mask for her father.  The Blonde comes out and instructs them to hand their briefcases to John and he enters the station alone.  The Indian has a better idea and runs in with his tomahawk to kill The Blonde.  The Indian then tries to attack The Blonde, but she blocks all of his attacks.  She then kicks him several times, knocking him out.  Then The Blonde shoots The Indian and kills him.  She re-emerges and repeats her instructions promising more deaths if they dont comply.

John takes the briefcases and enters the station.  As he enters, he and The Blonde embrace.  They are scamming everybody.  

It turns out that the family that was killed in the 60s was John Smiths parents.  They had an Indian servant who hid John in the floorboard of the station and told them Indians that he was dead too.  The servant was part of a rival Indian tribe "The Hobi".  The mask really belonged to her tribe and The Chief stole it from her.

The Blonde is the girlfriend who left John Smith at the beginning of the movie because she realized she liked women. However, she knew what happened to Johns family so she stuck around long enough to assist him.  Also, when John told her she would get one of the briefcases, that enticed her further.

John and The Blonde emerge from the station and tell The Rancher and The Chief why they are doing this.  Also, she tells them that Elvis destroyed the mask when he realized he wouldnt be able to sell it.  That was what he said to her before dying.

John tells The Rancher and The Chief that hes keeping one briefcase because of what they did to his family.  The Blonde tells them she will return and kill them if they try to retaliate.  Then she leaves with the other briefcase on a motorcycle driven by the woman who stole Johns wallet.  She is actually the doctor that The Blonde left him for posing as a hooker.

At this point, he finds Elviss car and repairs it and drives it.  It only broke down before because he sabotaged it in a way that could be repaired.  Also, he actually tried to steal the mask earlier in the film, but Elvis caught him doing it and knocked him out and took it for himself.

It turns out the Indian servant was in on the scam too.  The mask wasnt really destroyed.  John returns it to her since her tribe rightfully owns it.  Elvis actually told The Blonde "Elvis has left the building" before dying.  She just lied about his last words.

And finally, it is revealed that his name isnt even John Smith.  Someone he bumped into at the beginning of the movie is really John Smith and he pickpocketed him.

==Cast==
* Gary Oldman as Elvis
* Christian Slater as John Smith
* Megan Park as Cindy, The Girl Next Door
* Helena Mattsson as The Blonde Tony Cox as Little Person Elvis
* Chris Kattan as Gay Elvis
* Dane Cook as Sheriff Hutchins
* Jeff Fahey as The Cowboy
* Anthony Brandon Wong as Asian Elvis
* Gordon Tootoosis as The Chief
* Matt Willig as The Indian
* Powers Boothe as The Rancher
* Danny James as Mo
* Sam Trammell as Sheriff Cowley
* Paulina Gretzky as The Deputy
* Heather Roop as Vivan, Woman at the bar

==Production==
The film was first announced on June 17, 2010.  Some of the filming occurred in Utah.  On July 8, 2010, Megan Park was confirmed to have joined the cast. 

On July 13, 2010, it was confirmed that Chris Kattan and Helena Mattsson had joined that cast.  On July 14, 2010, Jeff Fahey was announced to have joined the cast. 

Principal photography began on July 6, 2010. 

==Release==

The film was released on DVD  in North America on January 8, 2013. 

==References==
 
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 
 