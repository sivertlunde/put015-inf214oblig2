Maharathi (2008 film)
{{Infobox film
| name           = Maharathi
| image          = Maharathi_poster.jpg
| caption        = 
| director       = Shivam Nair
| producer       = Dhillin Mehta
| writer         = Uttam Gada
| starring       = Paresh Rawal Naseeruddin Shah Neha Dhupia Om Puri Boman Irani Tara Sharma
| music          = Shibani Kashyap
| based on =  
| cinematography = Venu Isc
| editing        = Aarti Bajaj
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
Maharathi is a Hindi film produced by Dhilin Mehta. The film was directed by Shivam Nair and stars Paresh Rawal, Neha Dhupia, Naseeruddin Shah, Boman Irani, Om Puri and Tara Sharma. The films music is by Shibani Kashyap

==Plot==
The film opens as Jai Singh Adenwaala (Naseeruddin Shah) gets into an accident due to drunk driving but is saved from death  by Subhash Sharma (Paresh Rawal). As Jai Singh is seriously drunk, Subhash hails a taxi and decides to drop him home. Once at the Adenwaala Bunglow, Jai Singh invites Subhash inside to thank him. He introduces him to his wife, Malika (Neha Dhupia) a glamorous but shady young lady and his lawyer and friend, AD Merchant (Boman Irani). He then ends up hiring Subhash as his chauffeur, much to the chagrin of Malika. Merchant also develops a dislike for him . Malika tries her best to get Subhash out of the house but Subhashs cleverness and quick wit saves him every time.

As Subhash spends time with the Adenwaala couple, he comes to know about the true intentions of Malika, to kill Jai Singh in order to get the 24 crore of his insurance claim. This is further proved when Malika throws Jai Singhs asthma inhaler out of the window when he needed it. This time Jai Singh is saved by Subhash once again as he gets him the inhaler just in time.

It is revealed that Adenwaala is neck deep in loans and very depressed due to his constant drinking and Malikas desperate attempts to kill him. He calls Subhash and Malika and hands Subhash a letter. He then tells Malika about a little change he had made in his insurance policy. According to the conditions in the policy, the insurance money could only be claimed if Jai Singh is murdered and not if he commits suicide. Jai Singh says that he knows Malika will try to make his suicide appear a murder. Saying this, he shoots himself. Malika rushes to call the police but Subhash convinces her that together they can prove his suicide a murder. Subhash offers to partner up with her on the condition that they will split the insurance money in half. The rest of the film revolves around Subhash trying to prove Adenwaalas suicide a murder.

==Cast==
* Paresh Rawal as Subhash Sharma
* Neha Dhupia as Mallika Adenwaala
* Tara Sharma as Swati
* Boman Irani as AD Merchant
* Om Puri as ACP Gokhale
* Ashwin Nayak as the ATM guy
* Naseeruddin Shah as Jaisingh Adenwaala
 

==External links==
*  

 
 
 

 