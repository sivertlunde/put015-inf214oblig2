Thuruppugulan (2006 film)
{{Infobox film
| name        = Thuruppu Gulan
| image       = Thuruppu Gulan.jpg
| image_size  =
| caption     =
| director    = Johny Antony
| producer    = Udayakrishna Siby K. Thomas Sneha Innocent Innocent Cochin Devan
| music       = Alex Paul
| cinematography =
| editing     =
| distributor = Playhouse Through PJ Entertainments UK
| released    =  
| runtime     =
| country     = India
| language    = Malayalam
| budget      = 
| gross       = 

}} Sneha in the lead roles. Others in the cast include Innocent (actor)|Innocent, Jagathy Sreekumar, Suraj Venjaramoodu, Salim Kumar and Cochin Haneefa.

==Plot==
Mr. Menon (Devan), a multi-millionaire, buys considerable real estate and entrusts it to one of his trusted lieutenants, Sreedharan Unnithan (Kalashala Babu), to construct a 5-star hotel. He also buys a small plot and gives it to Kochuthoma (Innocent (actor)|Innocent), Kunjumons (Mammootty) father. Menon leaves for Singapore and Kunjumon is packed off to Calicut by his father, as he fears that his sons life might be in danger as he badly wounded one of the local goons who picked up a fight with Menon.

After a few years, Menon decides to return to his native land and get his daughter married off in a good way. But Sreedharan Unnithan and his sons, in an effort to foil his plans, have him framed in a cooked up case and imprisoned. His daughter, Lakshmi (Sneha (actress)|Sneha) reaches Kerala to find a way to help her father. She meets up with the hero Gulaan Kunjumon, the hero of the Calicut Market. With many twists and turns and a lot of misunderstandings, it becomes clear that Kunjumon aka Gulan is the son of Kochuthoma, and Kunjumon finally realizes that Lakshmi is the daughter of Menon, not Unnithan. The helping hand raised and his influence over the people around him, he solves the issue taking a really short duration.

==Cast==
* Mammootty as  Kunjumon a.k.a. Gulan Innocent  as  Kochuthoma (Father of Gulan) Sneha  as  Lakshmi Menon
* Jagathy Sreekumar  as  Swamy
* Salim Kumar as Khadar and Khadar
* Harishree Ashokan as Shathru
* Kalashala Babu as Sreedharan Unnithan
* Cochin Hanifa as Raman Kartha Janardanan  as  Peethambaran (Police Constable) Devan  as  Menon (Father of Lakshmi) Vijayaraghavan as D.G.P Suresh Krishna as Unnithans Son Baburaj as Unnithans Son
* Bheeman Raghu as Tipper Vasu
* Suraj Venjaramood
* Kalabhavan Shajon
* Bindu Ramakrishnan 
* Urmila Unni as DGPs wife
* Ponnamma Babu as dance teacher
* Ambika Mohan as Sreedharan Unnithans wife

==Soundtrack==
The movie has two songs composed by Alex Paul.
* "Alakadalil" - Mahadevan and Chorus
* "Madayaana" - Vineeth Sreenivasan

==External links==
*  
* http://www.nowrunning.com/movie/2821/malayalam/thuruppu-gulan/review.htm
* http://www.indiaglitz.com/channels/malayalam/preview/8156.html

 

 
 