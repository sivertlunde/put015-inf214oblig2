Yemaindi Ee Vela
{{Infobox film
| name        = Yemaindi Ee Vela
| image       = Yemaindi Ee Vela Poster HD.jpg
| caption     = Movie poster
| director    = Sampath Nandi
| producer    = K. Radha Mohan
| writer      = Sampath Nandi Shashank M. S. Narayana Vennela Kishore
| distributor = Sri Satya Sai Arts
| cinematography = Bujji
| released    =  
| country     = India
| language    = Telugu Chakri
| budget     =    gross          =   
|}} Tollywood romantic drama film, starring Varun Sandesh, Nisha Aggarwal, Nisha Shah and Shashank. The film was directed and written by Sampath Nandi. The Movie was a commercial success and it gave a much needed break to Varun Sandesh whose career was in slump,  completing a 50 Day Run in 32 Centers.  It was later remade in Tamil as Ishtam (2012 film)|Ishtam, with Nisha Aggarwal reprising her role from the original and Vimal playing the role portrayed by Varun Sandesh.

==Plot==
Seenu (Varun Sandesh) is going for his second marriage and he meets Nimisha (Nisha Shah). Avantika (Nisha Aggarwal) is also going for her second marriage and meets Yuva (Shashank (actor)|Shashank). Both of them decide to tell their partners about their past. The flashback reveals how Seenu and Avantika meet each other, fall in love, get married against their parents wishes and in no time, get separated as well. Now, the time comes when they are almost about to get married to their new partners. All the four want to break their marriages and break them on the day of marriage. Seenu and Avantika Break their Respective Marriages with the consent of their new partners and reconcile to lead a happy life together.

==Cast==
* Varun Sandesh as Seenu
* Nisha Aggarwal as Avantika Shashank as Yuva
* Nisha Shah as Nimisha
* M S Narayana
* Vennela Kishore

==Box office==
The film was declared Super hit worldwide by 4/5 ratings by critics.The film collected   in 50 Days run at box office.Yemaindi Ee Yela was made with a budget of  . 

==Soundtrack==
{{Infobox album 
| Name = Yemaindi Ee Vela
| Caption = Album cover Chakri
| Type = Soundtrack
| Cover =
| Released =  
| Recorded = 2010
| Genre = Film soundtrack
| Length =
| Label = Madhura Entertainment Chakri
| Last album =
| This album =
| Next album =
}}

The audio Launch was held on October 11, 2010 at Hotel Park in Hyderabad. M.L. Kumar Chowdary, Posani Krishna Murali, Allari Naresh, Nani, Tanish, Kajal Aggarwal, Nisha Aggarwal, Chakri, Vennela Kishore, Shashank, Kodi Ramakrishna, Sampath Nandi and others graced the Function. 

== References ==
 

==External links==
*  
 
 
 