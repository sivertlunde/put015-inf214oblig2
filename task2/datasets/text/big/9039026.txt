Un étrange voyage
{{Infobox film
| name           = Un étrange voyage
| image          = 
| caption        = 
| director       = Alain Cavalier
| producer       = Danièle Delorme
| writer         = Camille de Casabianca 
| starring       = Jean Rochefort Camille de Casabianca
| music          = Maurice Leroux
| cinematography = Jean-François Robin	
| editing        = Joëlle Hache
| distributor    = Planfilm
| released       =  
| runtime        = 100 minutes
| country        = France 
| language       = French
| budget         = 
}}
 1981 French drama film directed by Alain Cavalier, starring Jean Rochefort and writer/filmmaker Camille de Casabianca. The film won the Prix Louis-Delluc in 1980.

==Plot summary==
A man walks a length of railroad track, looking for his elderly mother whom he believes fell from the train at some point. He brings his daughter along.

== Cast ==
*Jean Rochefort as Pierre
*Camille de Casabianca as Amélie
*Arlette Bonnard as Claire
*Dominique Besnehard as Marc
*Patrick Depeyrrat as Le guichetier méridional
*Roland Amstutz as Le surveillant des voies
*Gérard Chaillou as Le cadre SNCF
*Alain Lachassagne as Le serrurier
*Patrick Bonnel as Lagent SNCF
*Hubert Saint-Macary as Lexaminateur

==External links==
* 

 
 
 
 
 
 
 
 

 