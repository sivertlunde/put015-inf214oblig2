Gypsy Melody
{{Infobox film
| name =  Gypsy Melody
| image =
| image_size =
| caption =
| director = Edmond T. Gréville
| producer = Emil E. Reinert   Alfred Rode  
| writer =  Irving LeRoy   Dan Weldon   Alfred Rode 
| narrator =
| starring = Lupe Velez   Alfred Rode   Jerry Verno   Raymond Lovell
| music = Alfred Rode 
| cinematography = Claude Friese-Greene
| editing = Georges Grace
| studio = British Artistic 
| distributor = Wardour Films
| released = 27 July 1936  
| runtime = 77 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website = musical comedy John Mead.

The film was a remake of the 1935 French film Juanita (film)|Juanita.

==Cast==
* Lupe Velez as Mila 
* Alfred Rode as Captain Eric Danilo 
* Jerry Verno as Madame Beatrice 
* Raymond Lovell as Court Chamberlain 
* Margaret Yarde as Grand Duchess 
* Fred Duprez as Herbert P. Melon 
* Hector Abbas as Biergarten Manager 
* Louis Darnley as Hotel Manager 
* G. De Joncourt as Doctor Ipstein 
* Monti DeLyle as Marco 
* Wyn Weaver as Grand Duke

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 
 
 
 
 
 
 
 


 
 