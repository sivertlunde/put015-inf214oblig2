Under Milk Wood (film)
 
{{Infobox film
| name           = Under Milk Wood (film)
| image          = Under_Milk_Wood_1972_Poster.jpg
| image_size     = 215px
| border         = 
| alt            = 
| caption        = Theatrical Release Poster
| film name      = 
| director       = Andrew Sinclair
| producer       = John Comfort et al.
| writer         = Dylan Thomas
| screenplay     = Andrew Sinclair
| story          = 
| based on       =  
| narrator       = 
| starring       = Richard Burton, Elizabeth Taylor, Peter OToole
| music          = Brian Gascoigne 
| cinematography = Robert Huke 
| editing        = Willy Kemplen, Greg Miller 
| studio         = J. Arthur Rank
| distributor    = J. Arthur Rank Film Distributors (1972,UK), Altura Films International (1973,USA)
| released       =  
| runtime        = 87 min
| country        = UK
| language       = English
| budget         = 
| gross          = 
}}
Under Milk Wood is a 1972 British film directed by Andrew Sinclair and based on the 1954 radio play Under Milk Wood  by the Welsh writer Dylan Thomas. It featured performances from many well-known actors as the residents of the fictional Welsh fishing village of Llareggub including Richard Burton, Elizabeth Taylor, Siân Phillips, David Jason, Glynis Johns, Victor Spinetti, Ruth Madoc, Angharad Rees, Ann Beach, Vivien Merchant and Peter OToole.  As of January 2014, this was the only live-action theater-released film of the play. 

It was the only film in which Burton, Taylor and O’Toole appeared together. It was shot primarily on location in Wales and has since acquired a reputation among aficionados as a cult movie.   Interview with director Andrew Sinclair on englishpen.org  "The film, beautifully photographed and spoken, casts the brooding spell of Thomas’ verse in its reconstruction of the seaside village and the daily round of its inhabitants", wrote Andrew Sinclair in the  International Herald Tribune. 

In December 2012 the director of the film, Andrew Sinclair, gave its rights to the people of Wales. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 