The Wackiest Wagon Train in the West
{{Infobox film
| name           = The Wackiest Wagon Train in the West
| image          = The Wackiest Wagon Train in the West.jpg
| image size     = 
| border         = 
| alt            = 
| caption        =  Jack Arnold Earl Bellamy Bruce Bilson Oscar Rudolph
| producer       = Elroy Schwartz
| writer         = Ron Friedman Howard Ostroff Brad Radnitz Sherwood Schwartz Elroy Schwartz
| based on       = 
| starring       = Bob Denver Forrest Tucker
| music          = 
| cinematography = Alan Stensvold
| editing        = Jack Woelz
| studio         = 
| distributor    = Danton Films
| released       = August 1, 1976 
| runtime        = 86 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Western comedy Jack Arnold. The film stars Bob Denver as Dusty, the bumbling assistant to Wagonmaster Callahan featured in the syndicated series Dustys Trail.

The film itself consists of four episodes of Dustys Trail edited together.

==Summary==
A stagecoach and covered wagon heading west across the plains become separated from their wagon train thanks to Dusty (Denver), a bumbling assistant to Wagonmaster Callahan (Forrest Tucker). Lost in the wilderness, seven hapless souls must now make their way to California on their own using what brains they have or havent got.

First, the characters meet "Injuns" who are not unlike the Hekawis in F Troop. Then there is a "necktie party" looking to hang Dusty as a horse thief. Third, a couple of cattle rustlers want to "get friendly" with some willing females. And finally, a "shootout" takes place, with Dusty dressed up as Bat Masterson in a ghost town setting.

==Home media==
The Wackiest Wagon Train in the West was released on VHS by Jtc, Inc., Front Row Video, Inc. and Direct Source Special. The film has also been released on Region 1 DVD by numerous companies including Tango Entertainment, Digiview and St. Clair Vision.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 