Aadhipathyam
{{Infobox film 
| name           = Aadhipathyam
| image          =
| caption        =
| director       = Sreekumaran Thampi
| producer       =
| writer         = Sreekumaran Thampi
| screenplay     = Sreekumaran Thampi Madhu Nedumudi Lakshmi
| Shyam
| cinematography = C Ramachandra Menon
| editing        = L Bhoominathan
| studio         = Souparnika Arts
| distributor    = Souparnika Arts
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Lakshmi in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast==
 
*Prem Nazir as Raveendran Madhu as Sulaiman
*Nedumudi Venu as Antony Lakshmi as Vilasini
*K. R. Vijaya as Amina
*Mohanlal as Mohan
*Jagathy Sreekumar as Kuttappan Kalpana as Gracy Anuradha as Dancer
*Baby Ponnambili as Suhara
*Balan K Nair as Prabhakara Menon
*Kuthiravattam Pappu as Kunjiraman
*Master Vimal as Ajayan
*Shanavas as Prakash
*TG Ravi as Rajendran
*Vanitha Krishnachandran as Kanam
*Adoor Bhasi as Sankara Pilla
*PR Menon
 

==Soundtrack== Shyam and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deepangal engumengum || K. J. Yesudas, Chorus || Sreekumaran Thampi || 
|-
| 2 || Kadhaparayaam  || P Jayachandran, Krishnachandran || Sreekumaran Thampi || 
|-
| 3 || Paradeshakkaaranaanu || S Janaki, Unni Menon, Jolly Abraham || Sreekumaran Thampi || 
|-
| 4 || Urangaatha raavukal || P Jayachandran, Vani Jairam || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 