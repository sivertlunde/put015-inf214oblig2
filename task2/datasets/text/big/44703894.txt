Khoon Ka Khoon
{{Infobox film
| name           = Khoon Ka Khoon
| image          = Khoon_Ka_Khoon.jpg
| image_size     = 
| caption        = 
| director       = Sohrab Modi
| producer       = Stage Film Company
| writer         = Mehdi Hassan Ahsan 
| narrator       = 
| starring       = Sohrab Modi Naseem Banu Shamshadbai Ghulam Hussain
| music          = Kanhaiya Pawar
| cinematography = 
| editing        = 
| distributor    =
| studio         = Minerva Movietone, Stage Film Company, Bombay
| released       = 1935
| runtime        = 122 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}} 1935 sound film  adaptation of a Shakespeare play.  Directed by Sohrab Modi under his Stage Film Company banner, it is cited as one of the earliest talkie version of this play.    Credited as "the man who brought Shakespeare to the Indian screen", it was Modis debut feature film as a director.       The story and script were by Mehdi Hassan Ahsan from his Urdu adaptation of Shakespeares "Hamlet". Starring Sohrab Modi, Khoon Ka Khoon was also the debut in films of Naseem Banu who played Ophelia.    The other star cast included Shamshadbai, Ghulam Hussain, Obali Mai, Fazal Karim and Eruch Tarapore. 

Khoon Ka Khoon was a "filmed version of a stage performance of the play" with Sohrab Modi as Hamlet and Naseem Banu as Ophelia.  The film has been cited by National Film Archive of India founder P K. Nair, as one of "21 most wanted missing Indian cinema treasures". 

==Plot==
The film stays close to the play, with Hamlets dead father wanting Hamlet to punish the people responsible for his death. Hamlet, unwilling to believe his mothers involvement is in a quandary and pretends to be insane. He has a play staged denouncing his mother and uncle. Gertrude drinks the poison intended for Hamlet, Hamlet kills his step-father and succumbs to the wounds received.

==Cast==
* Sohrab Modi as Hamlet
* Naseem Banu as Ophelia
* Shamshadbai as Gertrude
* Ghulam Hussain
* Obali Mai
* Fazal Karim
* Eruch Tarapore
* Ghulam Mohiyuddin
* Shamshad
* Rampiary
* Gauhar
* B. Pawar

==Production==
As reported by Peter Morris, Khoon Ka Khoon was a "recording of a stage production",    Modi filmed it using two cameras as the play was being staged.  The play was a mix of Elizabethan costumes for the main leads, with a mixture of Western stage and Indian costumes for other characters in the play. Modi decided to film the play shot by shot as in the staging of the play.    Mehdi Ahsan, a known writer of the Parsi theatre had earlier adapted the play for the silent film Khoon-e-Nahak (1928), before staging it for Modi. He apparently stated that he had to alter the Shakesperean plays to suit the Indian audiences "way of thinking".      

Director Modi asked Shamshadbai, Naseem Banu’s mother, to play to play the role of Gertrude (Hamlet’s mother), in order to take the pressure off the young Naseem during shooting. 

==Box-office And Review==
Khoon Ka Khoon, which was the first sound version of a Shakespeare play was not a success at the box-office.  However, Modis film was appreciated mainly for the Urdu dialogues, the "quality of the play" and his acting. Modis acting was noted as a performance dominating the film.   

==Remakes==
The different versions of Hamlet made in Hindi:   
* Khoon-E-Nahak  (Murder Most Foul) (1928) directed by Dada Athawale and written by Mehdi Hassan Ahsan
* Khoon Ka Khoon (1935)
* Hamlet (1954 film)|Hamlet (1954)
* Haider (film)|Haider (2014)

==Soundtrack==
The music was provided by Kanhaiya Parwar with lyrics by Mehndi Hassan Ahmed. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title 
|-
| 1
| Aaja Tora Saiyan Mandarwa Mein Aayo
|-
| 2
| Baise Betaabiye Dil Hai
|-
| 3
| Tore Mann Ki Kachhu Na Puri Ho Aas 
|-
| 4
| Mohana Pyari Sanam Se Milao 
|-
| 5
| Sakhi Kaise Yeh Dukh Sah Jaaye 
|-
| 6
| Mohe Naare Jeeshan Tu Bana Bana Ae
|-
| 7
| Hain Yeh Sab Saamne Raahat Neend Aane Ke Liye
|-
| 8
| Kaahe Chhod Pyar Dilbar AAhe More Yaar
|-
| 9
| Maano Maano Ri Pyaari Batiyan
|-
| 10
| Ja Ja Mujhi Dekhi Teri Dilari Ki Shaan
|-
| 11
| Dar Gulistan Gulrakhan Naazani Neera
|-
| 12
| Gul Aur Sambul Soman Nargis Gulshan Mein 
|-
| 13
| Dar Dar Dilbar Dekhti Dukh Dariya Doobi Naari
|-
| 14
| Darwaja Pe Maara Nisaan 
|-
| 15
| Darkhansha Nazmein Muqaddar Aasman Mein
|-
| 16
| Daawar Daadar Kaadir Sataar Daatar Lakh Baar 
|}

==References==
 

==External links==
* 
*  for Khoon Ka Khoon

 

 

 
 
 
 
 