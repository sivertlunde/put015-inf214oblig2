So Proudly We Hail!
{{Infobox film
| name           = So Proudly We Hail!
| image          = So Proudly We Heil!.jpg
| caption        = Theatrical release poster
| director       = Mark Sandrich
| producer       = Mark Sandrich Allan Scott
| starring       = Claudette Colbert Paulette Goddard Veronica Lake
| music          = Edward Heyman Miklós Rózsa
| cinematography = Charles Lang
| editing        = Ellsworth Hoagland
| distributor    = Paramount Pictures
| released       =  
| runtime        = 126 minutes
| country        = United States
| language       = English
}}

So Proudly We Hail! is a 1943 American film directed and produced by Mark Sandrich, and starring Claudette Colbert, Paulette Goddard &ndash; who was nominated for an Academy Award for Best Supporting Actress for her performance &ndash; and Veronica Lake.  Also featuring George Reeves, it was produced and released by Paramount Pictures.

The film follows a group of military nurses sent to the Philippines during the early days of World War II.  The movie was based on a book written by nurse Juanita Hipps  a World War II nurse &ndash; one of the "Angels of Bataan" &ndash; who served in Bataan and Corregidor during the time when McArthur withdrew to Australia which ultimately led to the surrender of US and Philippine troops to Japan.  Those prisoners of war were subjected to the infamous Bataan Death March.  The film was based, in part, on Lieutenant Colonel Hipps memoir I Served On Bataan.

==Plot==
The story covers many day-to-day events  and contrasts the brutality of war against the sometimes futile efforts of the nurses to provide medical aid and comfort.  Each of the nurses has a past or present love story with a soldier, with the longest-term one between the characters played by Colbert and George Reeves. Flashback narration and a sequence where the nurses and injured soldiers are stranded in Malinta Tunnel pinned down by aircraft fire are two notable aspects of the film.

Moviegoers of the time found great timeliness in the movie, since MacArthur and the battles for Bataan, and Corregidor were familiar to every American. Although the love-story plot line is the primary thrust of the film, the difficulties and emotional toll of war are also shown.

==Cast==
  
{|
|- Claudette Colbert as Lt. Janet "Davy" Davidson   
|- Paulette Goddard as Lt. Joan ODoul 
|- Veronica Lake as Lt. Olivia DArcy 
|-
|}
 
*George Reeves as Lt. John Summers
*Cora Witherspoon as Mrs Burns-Norvell
*Barbara Britton as Lt. Rosemary Larson
*Walter Abel as Chaplain
*Sonny Tufts as Kansas
*Mary Servoss as Capt. "Ma" McGregor
*Ted Hecht as Dr. Jose Bardia
*John Litel as Dr. Harrison
*Dr. Hugh Ho Chang as Ling Chee
*Mary Treen as Lt. Sadie Schwartz
*Kitty Kelly as Lt. Ethel Armstrong
*Helen Lynd as Lt. Elsie Bollenbacher
*Lorna Gray as Lt. Tony Dacolli
 

== Adaptations == Goddard and Lake reprising their original roles.

==Awards==
The film was nominated for four Academy Awards:   
 Best Supporting Actress (Paulette Goddard) Best Cinematography Best Visual Effects (Farciot Edouart, Gordon Jennings, George Dutton) Best Original Screenplay

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 