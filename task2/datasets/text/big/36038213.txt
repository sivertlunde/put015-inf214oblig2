A German Robinson Crusoe
{{Infobox film
| name           = A German Robinson Crusoe
| image          = Ein Robinson 1940 Poster.jpg
| border         = 
| alt            = 
| caption        = German film poster
| director       = Arnold Fanck
| producer       = {{Plainlist|
* Oskar Marion
* Wilhelm Sperber
}}
| writer         = {{Plainlist|
* Arnold Fanck
* Rolf Meyer
}}
| starring       = {{Plainlist|
* Herbert A.E. Böhme
* Marieluise Claudius Claus Clausen
}}
| music          = Werner Bochmann 
| cinematography = {{Plainlist|
* Sepp Allgeier
* Albert Benitz
* Hans Ert
}}
| editing        = {{Plainlist|
* Arnold Fanck
* Johannes Lüdke
}}
| studio         = Bavaria-Filmkunst
| distributor    = Bavaria-Filmverleih
| released       =  
| runtime        = 81 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
}}
 Claus Clausen. Written by Arnold Fanck and Rolf Meyer, the film is a modern-day Robinson Crusoe story about a man so angry about the post-World War I conditions in Weimar Germany that he voluntarily goes to live on a desert island. The film was shot partly on location in South America. 

==Plot== SMS Dresden is attacked by British ships off the coast of Chile. The crew manages to abandon ship before it sinks. They make their way to an isolated island where they are taken prisoner. After spending three years in custody, the sailors manage to escape and make their way back to Germany, intending to continue fighting for their Fatherland. When they arrive, however, they encounter a different Germany than the one they left behind—one where they are ridiculed and attacked by mutineers.

One of the returning crew, Carl Ohlsen (Herbert A.E. Böhme), leaves Weimar Germany and returns to the island where he had been held prisoner for three years, determined to live out the rest of his life as a Robinson Crusoe. Sometime later, he hears a radio report that describes how things have improved in Germany during the 1930s. Later, when the new SMS Dresden passes the island, he makes his way to the ship and is taken aboard by his new respectful comrades.

==Cast==
*  Herbert A.E. Böhme as Carl Ohlsen 
* Marieluise Claudius as Antje  Claus Clausen as Fritz Grothe 
* Oskar Marion as Kapitän 
* Malte Jäger as Offizier 
* Wilhelm P. Krüger as Pagels 
* Otto Kronburger as Kommandant 
* Wolf Dietrich as Offizier 
* Ludwig Schmid-Wildy as Matrose 
* Leopold Kerscher as Matrose 
* Martin Rickelt as Matrose Pieter 
* Georg Völkel as 1. Offizier 
* Hans Kühlewein as  Obermaat 
* Charly Berger as Stabsarzt 
* Günther Polensen as  Matrose 
* Hans-Joachim Fanck as Klein Peter

==References==
;Notes
 
;Bibliography
 
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Rentschler, Eric. The Ministry of Illusion: Nazi Cinema and Its Afterlife. Harvard University Press, 1996.
* Richards, Jeffrey. Visions of Yesterday. Routledge & Kegan Paul, 1973.
* Welch, David. Propaganda and the German Cinema, 1933-1945. I.B.Tauris, 2001.
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 