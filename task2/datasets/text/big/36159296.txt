The Hot Box
{{Infobox film
| name           = The Hot Box
| image          =
| caption        =
| director       =  
| producer       = Jonathan Demme
| writer         =   Jonathan Demme
| based on       =
| starring       = Carmen Argenziano
| music          =
| cinematography =
| editing        =
| distributor    = New World Pictures
| released       =  
| runtime        =
| country        = United States English
| budget         = $181,000 Chris Nashawaty, Crab Monsters, Teenage Cavemen and Candy Stripe Nurses - Roger Corman: King of the B Movie, Abrams, 2013 p 106 
| gross          =
}}
The Hot Box is a 1972 women in prison film from   and Jonathan Demme, who had previously made Angels Hard as They Come (1971) for New World Pictures. It was shot in the Philippines and was originally known as The Prescription Revolution. Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 38 

==Plot==
Four American nurses working in the Republic of San Rosario are kidnapped by a band of guerillas.

==Cast==
*Carmen Argenziano as Flavio
*Andrea Cagan as Bunny Kincaid
*Margaret Markov as Lynn Forrest
*Rickey Richardson as Ellie St. George
*Laurie Rose as Sue Pennwright
*Zaldy Zschornack as Ronaldo Montoya
*Jose Romulo as Crao
*Rocco Montalban as Carragiero
*Charles Dierkop as The journalist Garcia, also known as Major Dubay
*Gina Laforteza as Florida
*Ruben Ramos as Mimmo
*Ruben Rustia

==Production==
The film came about because Roger Corman had a production deal in the Philippines with a young producer there, Cirio Santiago. Corman wanted to give Santiago a story outline and Viola did up a treatment in an afternoon, which became the film. Jonathan Demme shot some second unit footage, which impressed Roger Corman enough to support Demmes debut as director, Caged Heat (1974). 

==References==
 

==External links==
*  at IMDB

 
 
 
 
 
 
 
 
 