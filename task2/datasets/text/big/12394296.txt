No Looking Back
{{Infobox film
| name           = No Looking Back
| image_size     = 
| image	=	No Looking Back FilmPoster.jpeg
| caption        = 
| director       = Edward Burns
| producer       = Alysse Bezahler Ted Hope Michael Nozik Robert Redford John Sloss
| writer         = Edward Burns Lauren Holly Jon Bon Jovi Connie Britton
| narrator       = 
| starring       = Lauren Holly Edward Burns Jon Bon Jovi
| music          = Joe Delia Patti Scialfa
| cinematography = Frank Prinzi
| editing        = Susan Graef
| studio         = PolyGram Filmed Entertainment
| distributor    = Gramercy Pictures
| released       =  
| runtime        = 96 mins
| country        = United States English
| budget         = $5,000,000 
| gross          = $222,099 
| preceded_by    = 
| followed_by    = 
}} 1998 American drama-genre film directed, written, produced by, and starring Edward Burns. The film centers on the relationship of Charlie (Burns) and Claudia (Lauren Holly).  The film had a limited theatrical release and grossed less than $250,000 domestically from its $5 million budget.

==Plot==
Claudia is a waitress in a seaside blue-collar village. Stuck there most of her life as her 30th birthday approaches, she dreams of bigger things.

Her boyfriend of several years, Michael, is impatient to get married and start a family. Claudia is uncertain she wants to be tied down in this small town like her sister Kelly, a single mother, keeping an eye on their own mother, who has been morose since their father abruptly left.

Complicating the situation for Claudia is the return of Charlie Ryan, an old boyfriend, who had escaped this small-town life, but now takes a job in a garage and acts as if hes planning to stay.

==Cast==
*Lauren Holly as Claudia
*Edward Burns as Charlie
*Jon Bon Jovi as Michael
*Connie Britton as Kelly
*Blythe Danner as Claudias Mother

==Production==
Burns said he originally wanted a budget of $12 million to get more shooting days but he was only able to secure $5 million. The movie was shot over 35 days. At the time he described it as "the most personal film Ive ever made. The one thats closest to my heart." 

==Reception==
Burns later said his friends nicknamed the film Nobody Saw It. After its poor commercial reception he did not write anything for two years. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 