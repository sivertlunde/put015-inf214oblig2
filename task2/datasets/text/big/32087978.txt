The School for Scandal (1923 film)
{{Infobox film
| name           = The School for Scandal
| image          =
| caption        =
| director       = Bertram Phillips
| producer       = Bertram Phillips Richard Sheridan (play)   Frank Miller Frank Stanmore Basil Rathbone
| music          = 
| cinematography = 
| editing        =
| studio         = BP Productions
| distributor    = Butchers Film Servicer
| released       = 1923 
| runtime        =
| country        = United Kingdom 
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British silent silent comedy Frank Stanmore and Basil Rathbone.  It is an adaptation of the play The School for Scandal by Richard Brinsley Sheridan.

==Cast==
* Queenie Thomas - Lady Teazle Frank Stanmore - Sir Peter Teazle
* Basil Rathbone - Joseph Surface John Stuart - Charles Surface
* Sidney Paxton - Sir Oliver Surface
* A.G. Poulton - Moses
* Elsie French - Lady Sneerwell
* Mary Brough - Mrs. Candour Jack Miller - Trip
* Billie Shotter - Maria
* Lottie Blackford - Aunt Agatha
* James Reardon - Crabtree
* Wallace Bosco - Snake
* Kimber Philips - Sir Harry Bumper Richard Turner - Sir Benjamin

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 