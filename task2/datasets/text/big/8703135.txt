Bhakta Prahlada (1967 film)
{{Infobox film
| name           = Bhakta Prahlada
| image          =
| image size     =
| caption        =
| director       = Chitrapu Narayana Rao
| producer       = A. V. Meiyappan
| writer         = Narasa Raju D.V. Samudrala Raghavacharya
| narrator       = Rojaramani S. Bala Murali Krishna
| music          = Saluri Rajeshwara Rao
| cinematography =
| editing        =
| distributor    = AVM Productions
| released       =  
| runtime        =
| country        = India
| language       = Telugu
| budget         =
| preceded by    =
| followed by    =
}}
 Telugu Hindu devotional film The Story Rojaramani as Hiranya Kasapa and Anjali Devi as Leelavati. The film was eventually dubbed into Tamil and remade in Hindi with the same name.
The Tamil version was not fully dubbed. The part that Balaiyyah acted was remade.

==Awards== National Film Awards
*National Film Award for Best Child Artist - Roja Ramani - 1967

;Nandi Awards Nandi Award for Second Best Feature Film - 1967

==Crew==
* Producer - A. V. Meiyappan using the banner AVM productions along with Veerappan & Co.
* Director - Chitrapu Narayana Murthy, well known as Ch Narayana Murthy.
* Background and soundtrack Music - S. Rajeswara Rao.
* Story and Dailogues - D. V. Narasa Raju
* Song Lyrics - Samudrala Sr., Samudrala Jr., Dasarathi, Arudra, Kosaraju, Padmaraju
* Poems - from old poet Potana.
* Singers - Madhavapeddi Satyam, Pitapuram Nageswararao, P. Susheela, S. Janaki, Sulamangalam Rajalakshmi, L. R. Eswari.
* Dance composition - Vempati Chinasatyam, P.S. Gopalakrishnan, K.S. Reddy.
* Camera - Vincent
* Studios - AVM Studios, Gemini Studios.

==Cast== Hiranya Kasapa - S. V. Ranga Rao.
* Prahlada - Roja Ramani|Rojaramani. Leelavati - Anjali
* Bala Murali Krishna Relangi
* Haranath
* Snake Charmer - Ramana Reddy
* Other small characters - Dhulipala, Nagaiah, Jayanti, T. Kanakam, Vanisri, Santa, Meena Devi, Sunita, Sushila
* Dancers: L. Vijayalakshmi, Vijayalalita, Gitanjali, Venniradai Nirmala

==Soundtrack==
There are some 23 melodious songs and poems in the film directed by Saluri Rajeswara Rao. 
{| class="wikitable"
|-
! Song !! Lyricist !! Singer/s
|-
| "Aadi Anaadiyu Neeve Deva" || Dasarathi || Mangalampalli Balamuralikrishna
|-
| "Aadukovayya O Ramesha" || Samudrala || P. Susheela group
|-
| "Andani Suraseema" || Samudrala || P. Susheela, S. Janaki, S. Rajyalaxmi
|-
| "Balayutulaku Durbalulaku Balamevvadu" || Potana || P. Susheela
|-
| "Bhujashakti Naa toda Porada Shankinchi" || Potana || Madhavapeddi Satyam
|-
| "Chadivinchiri Nanu Guruvulu" || Potana || P. Susheela
|-
| "Janani Janani Varadayini" || Palagummi Padmaraju || S. Janaki
|-
| "Jeevamu Neeve Kada" || Samudrala || P. Susheela
|-
| "Kaladambodhi Galandu Gaali" || Potana || P. Susheela
|-
| "Kanjakshuku Gaani Kayambu Kayame" || Potana || P. Susheela
|-
| "Kanulaku Veluguvu Neeve Kaava" || Samudrala || P. Susheela, S. Janaki
|-
| "Karunaleni Manasu Kathina Pashanambu" || Dasarathi || P. Susheela
|-
| "Mandara Makaranda Maadhuryamunadelu" || Potana || P. Susheela
|-
| "Munchiti Vardhulan Gadala Mottiti" || Potana || Madhavapeddi Satyam
|-
| "Narayana Mantram Srimannarayana Bhajanam" || Samudrala || P. Susheela
|-
| "Panchabdambulavadu Tandrinagu" || Potana || Madhavapeddi Satyam
|-
| "Pamulollomayya Maa Pegge Choodarayya" || Kosaraju || Pithapuram Nageswara Rao, L. R. Eswari
|-
| "Raa Raa Priya Sundara" || Dasarathi || P. Susheela
|-
| "Siri Siri Laali Chinnari Laali" || Arudra || S. Janaki and Balamuralikrishna
|-
| "Srimanini Mandira Bhakta Mandara" || Samudrala || P. Susheela and Balamuralikrishna
|-
| "Varamosage Vanamaali" || Samudrala || Mangalampalli Balamuralikrishna
|-
| "Vatuthara Neeti Sastrachaya Paraga" || Pothana || Madhavapeddi Satyam
|-
| "Yella Sareera Dharulaku Nillanu" || Pothana || P. Susheela
|}

==See also== The first Telugu talkie and old Bhakta Prahlada movie

==References==
 

==External links==
*  

 

 
 
 
 
 
 