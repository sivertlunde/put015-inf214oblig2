On Dress Parade
{{Infobox film
| name           = On Dress Parade
| image_size     =
| image	         = On Dress Parade FilmPoster.jpeg
| caption        = William Clemens Noel Smith (uncredited)
| producer       = Bryan Foy Tom Reed Charles Beldon (uncredited)
| starring       = Billy Halop Bobby Jordan Huntz Hall Gabriel Dell Leo Gorcey Bernard Punsly John Litel Frankie Thomas Cissie Loftus Howard Jackson
| cinematography = Arthur L. Todd
| editing        = Douglas Gould
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English
| budget         =
}} The Dead End Kids headlined a film without any other well-known actors.

==Plot==
A hero of World War I, Colonel William Duncan (Don Douglas), is on his deathbed.  He summons his old friend, Colonel Mitchell Reiker (John Litel) to ask him if he will care for his son Slip (Leo Gorcey) when he dies.  Reiker agrees, and when Duncan passes, Slip, who does not want to leave the neighborhood he grew up in, is tricked into attending the military school that Reiker is in charge of.

Cadet Major Rollins (Billy Halop) tries to help Slip reform and adapt to military life, but is thrown out a window for his troubles.  He continues to have altercations with all of the other cadets, but in the end he winds up saving the life of Cadet Warren (Gabriel Dell) during a fire in the camp munitions storeroom.  Although he is seriously injured during the rescue, the other cadets respect his efforts and welcome him as one of their own.  For his heroics he is given his fathers distinguished service cross and given the title of cadet major.

==Character portrayals==
This is the only Dead End Kids film in which Gorcey appears in the type of role he would assume throughout the East Side Kids and The Bowery Boys films, as the tough guy malcontent/gang leader.  His characters name, Slip, also became his official character name in the Bowery Boys films.  In addition, this is Bernard Punslys briefest appearance in any of their films.

==Production==
Alternate titles for this film are Dead End Kids on Dress Parade and Dead End Kids at Military School.   

==Cast==
===The Dead End Kids===
*Billy Halop as Cadet Major Rollins
*Bobby Jordan as Cadet Ronny Morgan
*Leo Gorcey as Slip Duncan
*Gabriel Dell as Cadet Georgie Warren
*Huntz Hall as Cadet Johnny Cabot
*Bernard Punsly as Dutch

===Additional cast===
*Frankie Thomas as Cadet Lieutenant Murphy
*John Litel as Col. Michael Riker Cissie Loftus as Mrs. Neely
*Selmer Jackson as Capt. Evans Dover
*Aldrich Bowker as Father Ryan
*Douglas Meins as Hathaway William Gould as Dr. Lewis
*Don Douglas as Col. Wm. Duncan

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
   
 