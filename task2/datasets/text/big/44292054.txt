The Seer (film)
{{Infobox film
| name           = The Seer 
| image          = 
| caption        =
| director       = Luigi Desole
| producer       = 
| writer         =  Luca Pesaro Noelle Siri
| starring       = {{Plainlist|
*Michele Morrow
*Bella Thorne
*Alexander Fiske-Harrison
*Katia Winter
*Domiziano Arcangeli 
*Paul Marc Davis 
*Lisa Franks
*Michael Graves}}
| music          = 
| cinematography = 
| editor         = 
| distributor    = Nuragic Films
| released       =  
| runtime        = 120 minutes
| country        = Italy
| language       = English
| budget         = $180.000
| gross          = 
}} thriller and horror film directed by Luigi Desole and written by Luca Pesaro and Noelle Siri. It stars Michele Morrow, Bella Thorne, Alexander Fiske-Harrison, Katia Winter, Domiziano Arcangeli, Paul Marc Davis, Lisa Franks and Michael Graves. The film was released on September 6, 2007.  It was filmed in Sardinia, Italy. 

== Synopsis ==
Claire Seu (Michele Morrow) travels to an island in the Mediterranean. But the island is home by a bloodthirsty and  a dangerous sect who prepare to wake an ancient power that threatens to destroy mankind. They begin a hunt to find the missing key that will awaken their God, the heiress and descendant of the immortal, Claire.

== Cast ==
*Michele Morrow as Claire Seu
**Bella Thorne as Young Claire Seu
*Alexander Fiske-Harrison as Paolo Lazzari
*Katia Winter as Ada
*Domiziano Arcangeli as Lupo
*Paul Marc Davis as Adam
*Lisa Franks as Francesca Seu
*Michael Graves as Professor Giusti
*Ashlie Victoria Clark as Sheila
*Irena A. Hoffman as Agata
*Paul di Rollo as Luigi Bronzetti
*Amanda Fullerton as Magistrate Bianchi
*Richard Kinsey as Sergio Seu
*Emilio Roso as Michele Seu
*Marco Spiga as Bassano / Curator Soru
*Zondra Wilson as Dr. Blake

==References==
 

== External links ==
* 

 
 
 
 


 