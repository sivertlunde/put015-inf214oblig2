Aap Ki Khatir (2006 film)
{{Infobox film
| name           = Aap Ki Khatir
| image          = Aap Ki Khatir (movie poster).jpg
| caption        = Theatrical release poster
| director       = Dharmesh Darshan
| producer       = Ganesh Jain Ratan Jain
| screenplay     = Sunil Munshi
| starring       = Akshaye Khanna Priyanka Chopra Ameesha Patel Dino Morea Suniel Shetty
| music          = Himesh Reshammiya
| cinematography = W B Rao
| editing        = Raj Sinha
| released       =  
| distributor    = Venus Movies Pvt. Ltd.
| runtime        = 127 mins
| language       = Hindi
| country        = India
| gross          =
}}
 2005 Hollywood movie The Wedding Date.  It was released throughout India and internationally in late August 2006.

==Plot== NRI who is living in India after her boyfriend Danny (Dino Morea) ditched her at the altar. Now, she has come back to London to be a part of her sister Shirani’s (Ameesha Patel) marriage, to the New York-based, Gujarati businessman Kunal Shah (Suniel Shetty), who is also Dannys best friend.

In a plan to get back to Danny and to make him jealous, Anu convinces her colleague Aman (Akshaye Khanna), to accompany her to the wedding as her new beau. Incidentally, Shirani was also involved with Danny earlier, which resulted in heartbreak for her too, as everyone learns of Danny’s womanizing ways. Arjun Khanna (Anupam Kher) is the father of Shirani. Betty (Lilette Dubey), Anus mother, married Arjun after her first husband died when Anu was little.

As time passes by, Aman finds Anu fascinating and later it develops into a fondness between the two. How Anu and Aman’s relationship undergoes a change during the course of these incidents forms the crux of the story.

It turns out that Danny had used Kunal to get to Shirani and he believes that they can still be together. Shirani keeps trying to tell Danny that she loves Kunal. Anu reacts with Aman in an unfriendly way but later they form a friendship that in time turns into love. When Anu finds out about Dannys relationship with Shirani, she breaks down knowing that everyone knew (even Aman) but didnt tell her. She tells Aman to go away but Kunal (who thinks of Aman as a friend) convinces him to stay for his sake (Aap ki khatir). The truth is revealed to everyone and then they believe that Anu loves Aman.  Anus parents convince her to go after Aman but she is too late as he has already left. When Shirani tells Kunal about Danny, he is heartbroken and chases Danny away. While chasing Danny, he finds Aman going away and he convinces Aman to ask Anu again for her love. He agrees and makes Kunal realize that he and Shirani are also meant to be together and to marry Shirani. Anu ends up marrying Aman and Kunal accepts Shirani happily after knowing the truth.

==Cast==
* Akshaye Khanna as Aman Mehra
* Priyanka Chopra as Anu Khanna
* Ameesha Patel as Shirani Khanna
* Dino Morea as Danny Grover
* Suniel Shetty as Kunal Shah
* Bhumicka Singh as Nikki
* Anupam Kher as Arjun Khanna (Anus Dad)
* Lilette Dubey as Betty Khanna (Anus Mum)
* Tiku Talsania as Praful Shah (Kunals Dad)
* Kamini Khanna as Kunals Mum
* Mona Punjabi
* Ananya sharma as sushi bua

==Soundtrack==
The music is composed by Himesh Reshammiya, and the lyrics are penned by Sameer. The album has sixteen tracks, including seven remixes and two reprise tracks.

===Track listing===
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Aap Ki Khatir
| extra1 = Himesh Reshammiya
| length1 = 4:56
| title2 = Afsana
| extra2 = Himesh Reshammiya
| length2 = 3:49
| title3 = I Love You For What You Are RS
| length3 = 6:16
| title4 =  Keh Do Naa
| extra4 = Shaan (singer)|Shaan, Sunidhi Chauhan
| length4 = 5:53
| title5 = Meethi Meethi Batan
| extra5 = Sunidhi Chauhan, Kailash Kher, Jaspinder Narula
| length5 = 6:00
| title6 = Tu Hai Kamaal
| extra6 = Kunal Ganjawala
| length6 = 6:04
| title7 = Tu Hi Mera
| extra7 = Himesh Reshammiya
| length7 = 4:32
| title8 = Tu Hi Mera - II
| extra8 = Himesh Reshammiya
| length8 = 4:36
| title9 = Aap Ki Khatir (Remix)
| extra9 = Himesh Reshammiya
| length9 = 5:04
| title10 = Aap Ki Khatir (Unplugged)
| extra10 = Himesh Reshammiya, Akshaye Khanna
| length10 = 5:59
| title11 = Afsana (Remix)
| extra11 = Himesh Reshammiya
| length11 = 3:28
| title12 = I Love You For What You Are (Remix) KK
| length12 = 3:50
| title13 =  Keh Do Naa (Remix)
| extra13 =  Shaan (singer)|Shaan, Sunidhi Chauhan
| length13 = 5:12
| title14 = Meethi Meethi Batan (Remix)
| extra14 = Sunidhi Chauhan, Kailash Kher, Jaspinder Narula
| length14 = 4:17
| title15 = Tu Hai Kamaal (Remix)
| extra15 = Kunal Ganjawala
| length15 = 5:17
| title16 = Tu Hi Mera (Remix)
| extra16 = Himani Kapoor
| length16 = 4:39
}}

==External links==
*  

 
 
 
 
 
 