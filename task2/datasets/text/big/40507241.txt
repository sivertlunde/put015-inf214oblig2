Find the Woman
{{infobox film
| title          = Find the Woman
| image          = Findthewoman1922-newspaperad.jpg
| image_size     = 150px
| caption        = Newspaper advertisement
| director       = Tom Terriss
| producer       = Cosmopolitan Productions
| writer         = Doty Hobart (scenario)
| starring       = Alma Rubens
| music          =
| cinematography = Ira H. Morgan
| editing        =
| distributor    = Paramount Pictures
| released       = April 2, 1922
| runtime        = 60 minutes; 6 reels (5,144 feet)
| country        = United States
| language       = Silent (English intertitles)

}}
Find the Woman is a 1922 American silent mystery drama film directed by Tom Terriss and starring Alma Rubens. It was produced by Cosmopolitan Productions, owned by William Randolph Hearst, and distributed by Paramount Pictures. 

An incomplete print survives in the Library of Congress.  

==Cast==
*Alma Rubens - Sophie Carey
*Eileen Huban - Clancy Deane Harrison Ford - Philip Vandevent
*George MacQuarrie - Judge Walbrough
*Norman Kerry - Marc Weber
*Ethel Duray - Fab Weber Arthur Donaldson - Morris Beiner
*Henry Sedley - Don Carey
*Sydney Deane - Sofford
*Emily Fitzroy - Mrs. Napoli

==References==
 

==External links==
* 

 
 
 
 
 


 
 