Each Time I Kill
{{Infobox film
| name           = Each Time I Kill
| image          =
| image_size     =
| caption        =
| director       = Doris Wishman Michael Bowen Dawn Whitman
| narrator       =
| starring       = Tiffany Paralta Rob Vidal Linnea Quigley Lisa Ferber Jackie Gold
| music          = James Sizemore
| cinematography = C. Davis Smith Luigi Manicottale
| distributor    =
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} directed by thriller was completed in October 2006.  Tiffany Paralta stars as Ellie Saunders, a shy high school senior who finds a magical locket which will allow her to exchange one physical feature with anyone she murders. 

The director of photography was longtime Wishman collaborator C. Davis Smith, and cameo appearances were made by The B-52s frontman Fred Schneider and scream queen Linnea Quigley.

Each Time I Kill received its world premiere on March 30, 2007 at the New York Underground Film Festival and was also selected by the Philadelphia International Gay & Lesbian Film Festival, apparently under the mistaken impression that the storyline included lesbian content. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 


 