Musée haut, musée bas
{{Infobox film
| name           = Musée haut, musée bas
| image          = 
| caption        = 
| director       = Jean-Michel Ribes
| producer       = Frédéric Brillion Gilles Legrand Dominique Besnehard Michel Feller Camille Nahassia
| writer         = Jean-Michel Ribes
| based on       = 
| starring       = Josiane Balasko Yolande Moreau
| music          = 
| cinematography = Pascal Ridao
| editing        = Yann Malcor
| distributor    = Warner Bros.
| studio         = Epithète Films Mon Voisin Productions
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         = $7,6 millions
| gross          = $6,356,847 
}}
Musée haut, musée bas is a 2008 French ensemble comedy directed by Jean-Michel Ribes. 

==Plot==
The museum seen as a microcosm. This is both a theater, with its stage and behind the scenes, and an anthill with his queen (Conservative), his soldiers (guards) her workers (handlers) and aphids (visitors).

==Cast==
 
* Michel Blanc as Mosk
* Victoria Abril as Clara
* Pierre Arditi as Henri Province
* Josiane Balasko as Mother Chanel
* Urbain Cancelier as Bernard
* Isabelle Carré as Carole Province
* Judith Chemla as Rosine Bagnole
* Loïc Corbery as Luc
* Éva Darlan as The Family Art guide
* François-Xavier Demaison as The head home
* André Dussollier as The minister
* Julie Ferrier as The prospect guide
* Guillaume Gallienne as Max Perdelli
* Fabrice Luchini as A guardian
* Samir Guesmi as A guardian
* Gérard Jugnot as Roland Province
* Philippe Khorsand as Frilon
* Valérie Lemercier as Valérie
* Valérie Mairesse as Thérèse
* Marilú Marini as Giovanna Perdelli
* Yolande Moreau as Madame Stenthels
* François Morel (actor)|François Morel as Hervé Parking
* Chantal Neuwirth as Anne
* Dominique Pinon as Simon
* Micheline Presle as Liliane
* Daniel Prévost as Maurice Bagnole
* Muriel Robin as Dame Kandinsky
* Simon Abkarian as Gilles Paulin
* Louis-Do de Lencquesaing as Jocelyn Paulin
* Grégory Gadebois as Georges Paulin
* Évelyne Bouix as Nina Paulin
* Samuel Theis as Paulin
* Pierre Lescure as Christian
* Tonie Marshall as Laurence
* Dominique Besnehard as A visitor
* Géraldine Martineau as A visitor
* Michèle Garcia
 

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 