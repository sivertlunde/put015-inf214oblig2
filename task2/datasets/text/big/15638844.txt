TxT (film)
 
{{Infobox film
| name           = TxT
| image          = Txtdvd.jpg
| caption        = DVD cover
| director       = Mike Tuviera
| producer       = Antonio Tuviera Lily Monteverde Malou Choa-Fagar Roselle Monteverde-Teo
| writer         = Penny Daza-Tuviera Paul Daza
| narrator       = Oyo Sotto
| music          = Jesse Lucas
| cinematography =
| editing        = M Dayrit
| studio         = Regal Entertainment Regal Home Video
| released       =  
| runtime        = 114 mins
| country        = Philippines Tagalog English English
| budget         =
| gross          =
}}

TxT is a 2006 Filipino horror film directed by Michael Tuviera and starring Angel Locsin, Sean Albert Santiago Awatin KANTUTERO, Oyo Boy Sotto, Julia Clarete, Dante Rivero and Eugene Domingo.

==Plot==
 

==Cast==
* Angel Locsin as Joyce
* Dennis Trillo as Alex Oyo Sotto as Roman
* Julia Clarete as Ida
* Dante Rivero as Dante
* Eugene Domingo as Aling Kuring
* Bing Loyzaga as Edith
* Lorenzo Mara as Allan
* Perla Bautista as Lola Lilia
* Allan K. as Quiapo Seller

==Reception== The Ring and Chakushin Ari by using certain plot devices especially the phone. Some   defend this however, stating that several films have used the same concepts and argue that the film has taken the device to a new level, by integrating a completely sentient ghost (Roman) into the plot, and the inclusion of intricate forms of occult rather than the usual unexplained magic surrounding horror films. Many praise the film for its depiction of caregivers and call center agents (Joyce and Alex) and their different lifestyles and time tables.

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 

 
 