Captain Nagarjun
 
{{Infobox film
| name           = Captain Nagarjun
| image          =
| caption        =
| director       = V. B. Rajendra Prasad
| producer       = V. B. Rajendra Prasad Acharya Athreya  
| narrator       = V. B. Rajendra Prasad Rajendra Prasad Chakravarthy
| cinematography = S. Navakanth
| editing        = A. Sreekar Prasad A. Sanjeevi
| studio         = Jagapathi Art Pictures
| distributor    =
| released       =  
| runtime        = 173 mins
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Rajendra Prasad played the lead roles and music is composed by K. Chakravarthy|Chakravarthy. The film declared as flop at box-office.   

==Plot== Rajendra Prasad), who made the portrait. He invites Ravi to felicitate him for his award winning portrait. Radha is stunned to see Ravi at her home. Nags behavior troubles her. On the stage Radha is asked to unveil the portrait. When they reach home, Radha accuses Nag of suspecting her. When he wants to know the truth, she explains about Ravi. Ravi and Radha are collage mates. Ravi is in love with Radha, but, she does not agree. She discontinues collage unable to bear ragging by Ravis friends. Ravi is Ananda Raos son. Radha finally agrees to marry Ravi at the insistence of her mother and Ananda Rao. The wedding is cancelled when Ravi brings a first wife. Radha decides to never marry. Nag still wants an explanation about the mole. Radha denies any knowledge and leaves him for suspecting her. When Nag starts drinking and decides to die, Radha pledges to kill Ravi, to prove her innocence. Overhearing, Ravi writes letters to Nag and Ananda Rao. The truth is Ravi cancelled his marriage with Radha because he has a fatal illness. He noticed the mole when his friends tore Radhas blouse while ragging her at collage. Ravi dies confirming Radhas innocence. Nag and Radha reconcile and live happily.

==Cast==
 
*Akkineni Nagarjuna as Nagarjun
*Kushboo as Radha Rajendra Prasad as Ravi
*Nutan Prasad as Anand Rao
*Gollapudi Maruti Rao as Obbayah
*Subhalekha Sudhakar as Murthy
*Vinod as Prabhu Srilakshmi as Manju
*Lakshmi Priya 
*Dubbing Janaki 
*Kalpana Rai
 

==Soundtrack==
{{Infobox album
| Name        = Captain Nagarjun
| Tagline     = 
| Type        = film Chakravarthy
| Cover       = 
| Released    = 1986
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:33
| Label       = Echo Music Chakravarthy 
| Reviews     =
| Last album  = Vikram (1986 Telugu film)|Vikram   (1986)
| This album  = Captain Nagarjun   (1986)
| Next album  = Pratidwani   (1986)
}} Acharya Athreya. Music released on ECHO Audio Company. 
{|class="wikitable"
|-
!S.No!!Song Title !!Singers !!length
|- 1
|Okati SP Balu
|3:58
|- 2
|Muvvalanni SP Balu, P. Susheela
|4:30
|- 3
|Edi Andam SP Balu
|3:32
|- 4
|Nuvvu Nenu SP Balu, P. Susheela
|4:28
|- 5
|Manasu Padithe SP Balu
|4:15
|- 6
|Taitaka SP Balu, P. Susheela
|4:28
|- 7
|Eee Moodu
|P. Susheela
|4:22
|}

==References==
 

==External links==
* 

 
 
 
 

 