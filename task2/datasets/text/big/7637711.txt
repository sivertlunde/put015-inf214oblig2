Barney Oldfield's Race for a Life
{{Infobox film
| name = Barney Oldfields Race for a Life
| image = Barney Oldfields Race for a Life.jpg
| caption =
| director = Mack Sennett
| writer =
| starring = Mack Sennett Mabel Normand Ford Sterling Barney Oldfield
| producer = Mack Sennett
| cinematography = Lee Bartholomew Walter Wright
| distributor = Keystone Film Company
| budget =
| released =  
| country  = United States
| runtime = 13 minutes English intertitles
}}
Barney Oldfields Race for a Life (1913 in film|1913) is a silent comedy short, directed and produced by Mack Sennett and starring Sennett, Mabel Normand, and Barney Oldfield as himself. It is considered one of the earliest to include the plot of a villain tying a young damsel to the tracks of an oncoming locomotive; a holdover from the Gaslight era of Victorian stage melodrama.

==Plot==
Barney Oldfield races a speeding locomotive to rescue a damsel in distress tied up on the tracks by evil villain Ford Sterling.

==Cast==
* Mabel Normand - The Girl
* Mack Sennett - The Boy
* Ford Sterling - The Jealous Villain
* Hank Mann	- Villains accomplice
* Barney Oldfield - Himself, a racing driver
* Al St. John - another accomplice of the Villain	
* Helen Holmes - Beauty, talking to Oldfield at Picket Fence		
* William Hauber - ?

==External links==
* 
*  

 
 
 
 
 
 
 


 