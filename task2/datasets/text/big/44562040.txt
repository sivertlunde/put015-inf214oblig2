They Always Return at Dawn
 
{{Infobox film
| name =   They Always Return at Dawn 
| image =
| caption =
| director = Jerónimo Mihura
| producer = Julio Peña 
| writer = Miguel Mihura  
| narrator =
| starring = 
| music = Manuel Parada  
| cinematography = Jules Kruger  
| editing = Petra de Nieva
| studio = Peña Films 
| distributor = Goya Films
| released =  
| runtime = 87 minutes
| country = Spain
| language = Spanish
| budget =
| gross =
}}
They Always Return at Dawn (Spanish:Siempre vuelven de madrugada) is a 1949 Spanish drama film directed by Jerónimo Mihura. 

==Cast==
*   Mercedes Albert as Elvira  
* Margarita Andrey as Susana  
* Manuel Arbó as Comisario Suárez  
* Rafael Bardem as Don Carlos 
* Francisco Bernal as Barman  
* Rafael Cortés 
* Félix Fernandez (actor)|Félix Fernandez as Don José  
* José Franco (actor)|José Franco as El hombre  
* Casimiro Hurtado
* Rufino Inglés as Paco  
* José Isbert as Don Jacobo  
* María Martín as Mary  
* Arturo Marín as Don Ramón 
* Matilde Muñoz Sampedro as Maruja  
* Miguel Pastor as Manolo  
* Julio Peña as Luis 
* Antonia Planas as Doña Pilar  
* Conrado San Martín as Andrés  
* Asunción Sancho as Cecilia  
* Pilar Sirvent as Chica del tiro al blanco 
* María Luisa Solé

== References ==
 
 
==Bibliography==
* Bentley, Bernard. A Companion to Spanish Cinema. Boydell & Brewer 2008.

== External links ==
* 

 

 
 
 
 
 
 

 