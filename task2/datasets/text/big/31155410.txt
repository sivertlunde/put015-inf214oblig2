Ammu (film)
{{Infobox film
| name           = Ammu
| image          =
| caption        =
| director       = NN Pisharady
| producer       = M Kesavan
| writer         = PA Warrior
| screenplay     = PA Warrior Sathyan Madhu Madhu Sukumari Adoor Bhasi
| music          = MS Baburaj
| cinematography = Kutty Pollekkad
| editing        = 
| studio         = Vasanthichithra
| distributor    = Vasanthichithra
| released       =  
| country        = India Malayalam
}}
 1965 Cinema Indian Malayalam Malayalam film,  directed by NN Pisharady and produced by M Kesavan. The film stars Sathyan (actor)|Sathyan, Madhu (actor)|Madhu, Sukumari and Adoor Bhasi in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  Sathyan as Shekharan Madhu as Bhasi
*Sukumari as Saro
*Adoor Bhasi as Achummaavan
*Omanakuttan
*PK Saraswathi as Kalyaniyamma
*Prem Nawas as Appu Ambika as Ammu
*Baby Saru as Thankam
*Indira Thampi as Ammini
*Kaduvakulam Antony as Anthonimaappila
*Kedamangalam Ali
*Master Ajith P Bhaskaran
*Premji as Shankunni Nair Sujatha as Sharadha
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by Yusufali Kechery.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aattinakkare Aalinkombile || Thankam Thampi || Yusufali Kechery || 
|-
| 2 || Ambili Mama Vaa Vaa || P Susheela || Yusufali Kechery || 
|-
| 3 || Konchikkonchi || S Janaki, KP Udayabhanu || Yusufali Kechery || 
|-
| 4 || Kunjippenninu || S Janaki, LR Eeswari, MS Baburaj, Machad Vasanthi, Chandrasekharan Thambi || Yusufali Kechery || 
|-
| 5 || Maayakkaara Manivarnna || P. Leela || Yusufali Kechery || 
|-
| 6 || Pulliyuduppittu Konchikkuzhayunna || Thankam Thampi || Yusufali Kechery || 
|-
| 7 || Thedunnathaare || S Janaki || Yusufali Kechery || 
|-
| 8 || Thudikottippaadaam || KP Udayabhanu, Thankam Thampi || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 


 