Empire of the Air: The Men Who Made Radio
{{Infobox film
| name = Empire of the Air: The Men Who Made Radio
| image = Empire of the Air.png
| border = yes
| caption = DVD release cover
| director = Ken Burns
| producer = Ken Burns, Morgan Wesson, Tom Lewis
| based on = Empire of the Air: The Men Who Made Radio  by Tom Lewis
| screenplay = Geoffrey Ward
| narrator = Jason Robards
| music = 
| cinematography = 
| editing = Paul Barnes WETA
| PBS
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget = 
| gross  = 
}}
Empire of the Air: The Men Who Made Radio is a non-fiction book by Tom Lewis, a history of radio in the United States, published by HarperCollins in 1991. The book was adapted into both a 1992 documentary film by Ken Burns and a 1992 radio drama written and directed by David Ossman. The source of the title is from a quote by Lee DeForest.

==Documentary== PBS on January 29, 1992, narrated by actor Jason Robards.  The film focused primarily on the three pioneers of radio in America&mdash;David Sarnoff, Lee DeForest and Edwin Armstrong&mdash;it did not ignore Guglielmo Marconi and Reginald Fessenden but makes no mention of Nikola Tesla. The program interspersed audio and musical highlights of old-time radio|"old time" radio with the stories, achievements, failures, scams and bitter feuds between each of the main protagonists. Among those featured is radio and television historian Erik Barnouw.

==Drama== Peter Bergman, Philip Proctor and Rene Auberjonois.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 

 