The Virgin Queen (1955 film)
{{Infobox film
| name           = The Virgin Queen
| image          = The Virgin Queen, film poster.jpg
| caption        = Original poster
| director       = Henry Koster
| producer       = Charles Brackett Harry Brown
| based on       = Sir Walter Raleigh
| screenplay     = Mildret Lord
| starring       = Bette Davis Richard Todd Joan Collins
| music          = Franz Waxman
| cinematography = Charles G. Clarke Robert L. Simpson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 92 minutes English
| budget         = $1.6 million 
}}

The Virgin Queen is a 1955 DeLuxe Color historical drama film in CinemaScope starring Bette Davis, Richard Todd and Joan Collins. It focuses on the relationship between Elizabeth I of England and Sir Walter Raleigh. 

The film marks the second time Davis played the English monarch; the first was The Private Lives of Elizabeth and Essex (1939). It was also the first Hollywood film for Australian actor Rod Taylor.  
 Love Is a Many-Splendored Thing (1955).

==Plot==
In 1581, Walter Raleigh (Richard Todd), recently returned from the fighting in Ireland, pressures unwilling tavern patrons into freeing from the mud the stuck carriage of Robert Dudley, Earl of Leicester (Herbert Marshall). When Leicester asks how he can repay the kindness, Raleigh asks for an introduction to Queen Elizabeth I (Bette Davis), to whom Leicester is a trusted adviser. Leicester grants the request.
 Robert Douglas). As the court ventures outside, Raleigh graciously drapes his cloak (an expensive item borrowed from a reluctant tailor) over some mud so that the Queen need not soil her shoes. At dinner, Raleigh reveals his dream of sailing to the New World to reap the riches there. Elizabeth decides to make him the captain of her personal guard. He enlists his Irish friend, Lord Derry (Dan OHerlihy).

Meanwhile, Beth Throckmorton (Joan Collins), one of the Queens ladies in waiting, very forwardly makes Raleighs acquaintance. Raleighs relationship with both ladies is stormy. Beth is jealous of his attentions to Elizabeth, while the Queen is often irritated by his independence and constant talk of the New World. Hatton does his best to inflame her annoyance, but she is too clever to be taken in.

When Hatton informs Elizabeth that an Irishman is a member of her guard, Raleigh is stripped of his captaincy when he protests that his friend is loyal and refuses to dismiss him. Banished from court, Raleigh takes the opportunity to secretly marry Beth. Soon after, however, he is restored to Elizabeths favor.

Finally, Elizabeth grants Raleigh not the three ships he desires, but one. He enthusiastically sets about making modifications. In private, however, Elizabeth reveals within Beths hearing that her intentions do not include him actually leaving England. When so informed, Raleigh makes plans to sail to North America without royal permission.

Hatton tells the Queen not only of Raleighs plot, but also that he is married to Beth. Elizabeth orders the couples arrest. Raleigh delays those sent to take him into custody so that Derry can try to take Beth into hiding in Ireland, but they are overtaken on the road, and Derry killed. Raleigh and Beth are sentenced to death, but in the end, Elizabeth releases them. They set sail for the New World.

== Cast ==
* Bette Davis as Queen Elizabeth I
* Richard Todd as Sir Walter Raleigh
* Joan Collins as Elizabeth Throckmorton
* Jay Robinson as Chadwick
* Herbert Marshall as Robert Dudley, 1st Earl of Leicester
* Dan OHerlihy as Lord Derry Robert Douglas as Sir Christopher Hatton
* Romney Brent as French Ambassador
* Leslie Parrish as Anne
* Lisa Daniels as Mary
* Rod Taylor as Cpl. Gwilym (Uncredited)
* Nelson Leigh as Physician (Uncredited)

== References ==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 