Please Turn Over
{{Infobox film
| name           = Please Turn Over
| image_size     =
| image	         = Please Turn Over FilmPoster.jpeg
| caption        =
| director       = Gerald Thomas
| producer       = Peter Rogers
| writer         = Norman Hudis Basil Thomas Ted Ray Leslie Phillips Julia Lockwood Bruce Montgomery
| cinematography = Edward Scaife
| editing        = John Shirley
| distributor    = Anglo-Amalgamated Film Distributors
| released       = 1959
| runtime        = 87 minutes
| country        = United Kingdom
| language       = English
| budget         =
}} 1959 British Ted Ray, Charles Hawtrey, Lionel Jeffries and Victor Maddern. An English village is thrown into chaos when the daughter of one of the residents publishes a book detailing the supposed secrets of the inhabitants.  It was based on the play Book of the Month by Basil Thomas.

==Plot== English town, seventeen-year-old Jo Halliday lives a fairly boring life working as a hairdresser and living at home, with her nagging mother, pompous father, and fitness obsessed Aunt. Her father, an accountant, continually wishes that his dreamy, untidy daughter could be more like his secretary Miss Jones.

One morning the local newspaper reveals that she has authored a book - The Naked Revolt - which is an instant bestseller. It tells the story of a young girl who discovers the truth about her family and neighbours, and flees to London to become a prostitute.
 Doctor is painted as a philanderer who is sexually involved with a number of his patients while ignoring the desperate advances of his drunken assistant, Jos Aunt.

In fact none of these things are true, her father is scrupulously honest and in love with her mother. The local Doctor is a shy man, and the former army officer is simply a driving instructor. Jo has left town for London with a young playwright who is interested in turning her book into a play. After discovering they are kindred spirits, the two become engaged.

When they return home Jo is confronted by her angry family and neighbours. The Doctor is threatening to sue, and her father and mother have even begun questioning each others fidelity.

==Cast== Ted Ray - Edward Halliday
* Jean Kent - Janet Halliday
* Leslie Phillips - Dr. Henry Manners
* Joan Sims - Beryl, the maid
* Julia Lockwood - Jo Halliday
* Tim Seely - Robert Hughes, the playwright Charles Hawtrey - Jewellery Salesman
* Dilys Laye - Miss Jones, the secretary
* Lionel Jeffries - Ian Howard, the driving instructor
* June Jago - Gladys Worth, the Aunt
* Colin Gordon - Maurice
* Joan Hickson - Saleswoman
* Victor Maddern - Man at station Ronald Adam - Mr. Appleton
* Cyril Chamberlain - Mr. Jones
* Marianne Stone - Mrs. Waring 
* Anthony Sagar - Barman

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 