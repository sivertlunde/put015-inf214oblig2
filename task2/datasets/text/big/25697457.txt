Klimt (film)
{{Infobox film
| name           = Klimt
| image          = Klimt film poster.png
| caption        = 
| director       = Raúl Ruiz (director)|Raúl Ruiz
| producer       = Matthew Justice Arno Ortmair Dieter Pochlatko Andreas Schmid
| writer         = Gilbert Adair Raúl Ruiz (director)|Raúl Ruiz Herbert Vesely
| starring       = John Malkovich Veronica Ferres Stephen Dillane Saffron Burrows Sandra Ceccarelli Nikolai Kinski
| music          = Jorge Arriagada
| cinematography = Ricardo Aronovich
| editing        = Valeria Sarmiento
| distributor    = 
| released       =  
| runtime        = 131 minutes
| country        = Austria France Germany United Kingdom
| language       = English/German/French
| budget         = 
}}
 Raoul Ruiz, with an English screenplay adaptation by Gilbert Adair. The director of photography was  Ricardo Aronovich, and the music was composed by Jorge Arriagada.    The title role was played by John Malkovich and the cast included Stephen Dillane. Both  a 130 minute long directors cut  and a shortened producers cut of 96 minutes  were shown at the 2006 Berlin Film Festival. 

==Plot==
Gustav Klimts life story unfolds in a series of disjointed sequences in the artists mind as he lies dying of pneumonia in a Viennese hospital where he is visited by his friend, Egon Schiele (Nikolai Kinski). Themes within the film include Klimts platonic friendship with Emilie Floege (Veronica Ferres).  Much of the film is centred on Klimts relationship  with  Lea de Castro (Saffron Burrows), a dancer to whom he is introduced by the film pioneer Georges Méliès. 

==Cast==
* John Malkovich as Klimt
* Veronica Ferres as Midi
* Stephen Dillane as Secretary
* Saffron Burrows as Lea de Castro
* Sandra Ceccarelli as Serena Lederer
* Nikolai Kinski as Egon Schiele
* Aglaia Szyszkowitz as Mizzi
* Joachim Bißmeier as Hugo Moritz
* Ernst Stötzner as Minister Hartl Paul Hilton as Duke Octave
* Annemarie Düringer as Klimts Mother
* Irina Wanka as Berta Zuckerkandl

==Reception==
The film was shown at the 28th Moscow International Film Festival    where it was nominated for two awards, winning the Russian Film Clubs Federation Award. 

Philip French, in The Observer described the film as calculatedly enigmatic. Cosmo Landesman, in The Sunday Times, described the film as "frigid and silly" being unnecessarily difficult to follow in the style of Stanley Kubricks Eyes Wide Shut. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 