Howl's Moving Castle (film)
{{Infobox film
| name            = Howls Moving Castle
| image           = Howls-moving-castleposter.jpg
| alt             =
| caption         = Japanese release poster
| film name      = {{Infobox name module
| kanji          = ハウルの動く城
| romaji         = Hauru no Ugoku Shiro
}}
| director        = Hayao Miyazaki Toshio Suzuki
| screenplay      = Hayao Miyazaki
| based on        =  
| starring        = Chieko Baisho Takuya Kimura Akihiro Miwa
| music           = Joe Hisaishi
| cinematography = Atsushi Okui
| editing         = Takeshi Seyama
| studio          = Studio Ghibli
| distributor     = Toho
| released        =    
| runtime         = 119 minutes
| country         = Japan
| language        = Japanese  
| budget          = Japanese yen|¥2.4 billion United States Dollar|USD$24 million
| gross           = Japanese yen|¥23.2 billion United States Dollar|USD$235 million (worldwide)
}}
  novel of Toshio Suzuki, animated by Studio Ghibli and distributed by Toho. Mamoru Hosoda, director of one episode and two movies from the Digimon series, was originally selected to direct but abruptly left the project, leaving the then-retired Miyazaki to take up the directors role.

The film had its  .

Wynne Joness novel allows Miyazaki to combine a plucky young woman and a mother figure into a single character in the heroine, Sophie. She starts out as an 18-year-old hat maker, but then a witchs curse transforms her into a 90-year-old grey-haired woman. Sophie is horrified by the change at first. Nevertheless, she learns to embrace it as a liberation from anxiety, fear and self-consciousness. The change might be a blessed chance for adventure.   

==Plot== wizard named Howl while on her way to visit her younger sister Lettie. The Witch of the Waste, who romantically pursues Howl, visits the hat shop only to be refused service by Sophie.  The Witch exacts revenge by cursing Sophie, transforming her into a ninety-year-old woman. Seeking a cure for the transformation spell, Sophie travels into the Wastes. It is there she encounters a living scarecrow which she names "Turnip Head," and her new friend leads her to Howls castle. On arrival at the castle, Sophie meets Howls young apprentice, Markl, and the fire-demon Calcifer, who is the source of all of the castles energy and magical power. Calcifer offers to break the witchs curse in exchange for Sophies help in breaking the spell hes under, which keeps him bound to the house. When Howl appears, Sophie announces that she hired herself as a cleaning lady after seeing the terribly dirty condition of the house.

At the same time, Sophies country is caught up in the beginning of a war with its neighboring country, following the mysterious disappearance of the other countrys Crown Prince. The King summons Howl, under two different assumed identities, to fight in the war. However, Howl decides to send Sophie to the King under the pretense of being his mother, to profess the cowardice of one of Howls two aliases. Before leaving the castle, Howl gives to Sophie a magic charm (ring), which connects her to Calcifer and will guarantee her safe return. He also assures her that he will follow her to the palace in disguise. At the palace, Sophie runs into an asthmatic dog, Heen, who she thinks is Howl undercover. She also meets the Witch of the Waste, whom Suliman, the kings magic advisor, punishes by draining all of her power, causing her to regress into a harmless old woman. Suliman tells Sophie that Howl will meet the same fate if he does not contribute to the war. As Sophie vehemently protests these measures, the Witchs spell temporarily weakens revealing Sophies true form due to the passion in her words. Suliman realizes Sophies true relation to Howl and her intense romantic feelings towards him. Howl then arrives to rescue Sophie, disguised as the King. Suliman tries to trap Howl, but with Sophies help, they manage to escape, taking Heen and the former Witch of the Waste with them.

Sophie learns that Howl transforms into a bird-like creature in order to interfere in the war, but each transformation makes it more difficult for him to return to human form. Sophie fears that Howl is preparing to leave them, as his remaining time as a human is limited; Howl returns to interfering in the war. Sophies mother — under Sulimans control — arrives and leaves behind a bag containing a "peeping bug." The former Witch of the Waste discovers it and promptly destroys the bug by tossing it into Calcifer, who then becomes sick and weak, rendering him unable to protect the castle from being discovered. Sophies mother says Sophie can live with her again, but Sophie says she will stay with her new family.
 carpet bombed by enemy aircraft. Sulimans henchmen invade the flower shop Howl made for Sophie. After protecting the flower shop from the bombing, Howl draws the guards away just after healing Calcifer and tells Sophie he is not going to run away anymore because he has something he wants to protect, Sophie. Afterwards, he takes his leave. Deducing that Howl is trying to protect the castle and everyone inside it, Sophie moves everyone out, removes Calcifer from the fireplace and destroys the castle. She offers Calcifer her braid, allowing him to power a portion of the remaining castle. They head toward Howl, in order to let him know that the flower shop is not attached to the castle any more when the former Witch of the Waste realizes that Howl has given his heart to Calcifer, just like Sophie did with her hair. The Witch takes Calcifer and refuses to let go of him although it is burning her. A panicked Sophie pours water onto the Witch of the Waste, which douses Calcifer, making him lose his magical strength and power. The castle is split in two; Sophie and Heen fall down a chasm, while Markl, the Witch of the Waste and Calcifer continue traveling on the disintegrating castle.

Making her way toward Howls heart with the ring that Howl gave her, Sophie enters through the broken door of the castle into the "black region" and discovers a recollection of how Howl and Calcifer met: as a boy, Howl took pity on a falling (dying) star—Calcifer—and gave it his heart. The act bound Calcifer to Howl indefinitely; however, by losing his heart, Howl was emotionally trapped in adolescence.
 human consciousness, in bird form. They head back to Calcifer, accompanied by the Witch of the Waste and Markl, both of whom are on a plain wooden platform, the only remaining part of the former castle, and moving along the edge of a cliff. Sophie begs the Witch for Howls heart. She gives it to her and Sophie places the heart back inside Howl, returning him to life and freeing Calcifer. With Calcifer gone, the remaining platform collapses and starts falling down the mountain. Turnip Head saves the platform by using his pole as a brake. Sophie warmly kisses Turnip-head on the cheek as thanks, which breaks his curse, revealing that he is actually the missing prince from the neighboring country and was trapped in the form of a scarecrow until he received a true loves kiss. Howl wakes up and Sophie warmly embraces him.  Although Calcifer is now free, he returns to his former company. Heen shows the scene of the happy end and the discovery of the missing prince to Suliman, and Suliman decides to put an end to the war. Howl, Sophie, and the others are seen high above the bomber planes upon a new flying castle, while the bombers return from the war.

==Voice cast==
{| class="wikitable"
|-
! Character
! Japanese
! English
|-
| Sophie (young) || rowspan="2" | Chieko Baisho || Emily Mortimer
|-
| Sophie (old) || Jean Simmons
|-
| Howl || Takuya Kimura || Christian Bale
|-
| Witch of the Waste || Akihiro Miwa || Lauren Bacall
|-
| Calcifer || Tatsuya Gashūin || Billy Crystal
|-
| Markl || Ryūnosuke Kamiki || Josh Hutcherson
|-
| Madame Suliman || Haruko Katō || Blythe Danner
|-
| Lettie || Yayoi Kazuki || Jena Malone
|-
| Honey || Mayuno Yasokawa || Mari Devon
|-
| Prince Justin/Turnip Head || Yō Ōizumi || Crispin Freeman
|-
| Madge || Rio Kanno || Liliana Mumy
|-
| King of Ingary || Akio Ōtsuka || Mark Silverman
|-
| Heen || colspan="2" | Daijirō Harada
|-
|}

==Production==
In September 2001, Studio Ghibli announced the production of two films. The first would become The Cat Returns and the second was an adaptation of Diana Wynne Jones novel, Howls Moving Castle.    A rumor persists that Miyazaki had an idea to do Howls Moving Castle during a visit to Strasbourg Christmas market.  Mamoru Hosoda of Toei Animation was originally selected to direct the film, but quit after Studio Ghiblis bosses refused to accept any of his concept ideas.    The film was shelved until Miyazaki took over.  The project resumed with production in February 2003. 

Miyazaki went to Colmar and Riquewihr in Alsace, France, to study the architecture and the surroundings for the setting of the film.   Additional inspiration came from the concepts of future technology in Albert Robidas work. 
Miyazaki, a pacifist, said that the production of the film was profoundly impacted by the Iraq War.  

The film was produced digitally, but the original backgrounds were drawn by hand and painted prior to be digitized, and the characters were also drawn by hand prior to scanning them into the computer.  The 1400 storyboard cuts for the film were completed on January 16, 2004.  On June 25 the in-between animation was completed and checking was completed on June 26. 

The complex moving castle changes and rearranges itself several times throughout the movie in response to Howls eccentricity and the various situations.  The basic structure of the castle consists of more than 80 elements including turrets, a wagging tongue, cogwheels and bird feet, that were rendered as digital objects. 

==Differences between film and novel==
Diana Wynne Jones did meet with representatives from Studio Ghibli, but did not have any input or involvement in the production of the film. Miyazaki traveled to England in the summer of 2004 to give Jones a private viewing of the finished film. She has been quoted as saying: Its fantastic. No, I have no input—I write books, not films. Yes, it will be different from the book—in fact its likely to be very different, but thats as it should be. It will still be a fantastic film.  
 pacifist reasons. Also, the relationship of Howl and Sophie differs between the novel and the movie in that the two gradually develop their relationship through lots of bickering and quarreling in the novel while the movie does not portray this aspect as much.

Sophie herself is different in the film and than in the novel. For instance, Sophies personality is a little more irate in the novel; also she possess magic unlike her counterpart in the film. 

In contrast, the novel is concerned with Howls womanizing and his attempts to lift the curse upon himself (discovering later how his lethal predicament is entangled with the fates of a lost wizard and prince) as well as running from the incredibly powerful and beautiful Witch of the Waste, who is the storys main villain and not at all the old yet harmless character she plays on screen. Also in the book, the Witch of the Waste was later killed by Howl while the anime film keeps her alive in an old age. Another noteworthy difference is that Sophie, in the book, is herself an unwitting sorceress totally unaware of her power, with the ability to "talk life into things" like the hats she makes and her own walking stick; objects take on a life of their own the more attention Sophie gives to them.

The book detours for one chapter into 20th-century Wales, where Howl is known as Howell Jenkins and has a sister with children. This glimpse into Howls complicated past is not shown in the film, but one of Howls aliases is "The Great Wizard Jenkins".

In addition, Sophie has two sisters in the book, Lettie and Martha, not just one. Markl is called Michael in the book, is 15, and is in love with Sophies youngest sister, Martha (in the novel, Howl also courts Lettie for a while). Suliman is actually a man from Wales whose real name is Ben Sullivan, not a woman as portrayed in the movie. The film conflates this Suliman as a powerful wizard in his own right who has gone missing after a confrontation with the Witch of the Waste, with Mrs. Penstemmon, the Professor who taught Howl sorcery and gives Sophie clues as to how to free Calcifer and Howl from their contract.  Neither is an enemy of the heroine in the book. Besides Martha, several other characters were left out. 

==Music==
The   also composed Howls Moving Castle CD Maxi-Single, a CD single published on October 27, 2004 which includes the films theme song, sung by Chieko Baisho (the Japanese voice actor for Sophie), its karaoke version, and a piano version of the films main theme, "The Merry-Go-Round of Life". 

==Reception==
Howls Moving Castle received positive reviews. As of August 2011, review aggregate Rotten Tomatoes reports that 87% of critics gave positive reviews, based on 157 reviews, certifying it "Fresh".    USA Today critic Claudia Puig praised it for its ability to blend "a childlike sense of wonder with sophisticated emotions and motives"  while Richard Roeper called it an "insanely creative work". Other critics described it as "a visual wonder", "A gorgeous life-affirming piece", and "an animated tour de force." Roger Ebert, of the Chicago Sun-Times, gave it two and a half out of four stars, and felt that it was one of Miyazakis "weakest" films.  Helen McCarthy in 500 Essential Anime Movies says that natural world is "beautifully represented here", with "some absolutely breathtaking mountains and lakeside landscapes". She also praises the design of the Castle and adds that Miyazaki added his own themes to the film: "mans relationship to nature, the futility of war, and the joy of flight". 

===Top ten lists===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:25em; max-width: 25%;" cellspacing="5"
| style="text-align: left;"|"Theres a word for the kind of comic, dramatic, romantic, transporting visions Miyazaki achieves in Howls: bliss."
|-
| style="text-align: left;"|—Peter Travers, Rolling Stone   
|}

The film appeared on many critics top ten lists of the best films of 2005.   

* 2nd&nbsp;– Ella Taylor LA Weekly (tie)
* 4th&nbsp;– Kenneth Turan Los Angeles Times
* 5th&nbsp;– Tasha Robinson The Onion
* 6th&nbsp;– Lawrence Toppman, The Charlotte Observer
* 6th&nbsp;– Jonathan Rosenbaum, The Chicago Reader (tie)
* 8th&nbsp;– Michael Sragow, The Baltimore Sun
* 8th&nbsp;– Michael Wilmington The Chicago Tribune
* NA&nbsp;– Peter Rainer The Christian Science Monitor (Listed alphabetically)

===Accolades  ===
{| class="wikitable"
|-
! Year
! Award
! Category
! Result
! Recipient
|- 2004
| 61st Venice Film Festival
| Osella Awards for Technical Achievement
|  
| Howls Moving Castle
|-
| Mainichi Film Awards
| Best Japanese Movie Overall
|  
| Howls Moving Castle
|-
| Japan Media Arts Festival
| Excellence Prize, Animation
|  
| Howls Moving Castle
|- 2005
| Tokyo International Anime Fair
| Animation of the Year
|  
| Howls Moving Castle
|- Tokyo Anime Awards
| Best Director
|  
| Hayao Miyazaki
|-
| Best Voice Actor/Actress
|  
| Chieko Baisho
|-
| Best Music
|  
| Joe Hisaishi
|-
| Maui Film Festival
| Audience Award
|  
| Howls Moving Castle
|-
| Seattle International Film Festival
| Golden Space Needle Award
|  
| Howls Moving Castle
|-
| 2006
| 78th Academy Awards Best Animated Feature
|  
| Howls Moving Castle 
|}

===Influences===
Gore Verbinski cited the film as an influence for Rango (2011 film)|Rango. 

==References==
 

==External links==
 
 
 
*   (Japanese)
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 