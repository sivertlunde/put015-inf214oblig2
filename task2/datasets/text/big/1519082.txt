The Pacifier
{{Infobox film
| name           = The Pacifier
| image          = Pacifier poster.jpg
| caption        = Theatrical release poster
| alt            = 
| director       = Adam Shankman
| producer       = Roger Birnbaum Gary Barber Jonathan Glickman Thomas Lennon Robert Ben Garant
| starring       =  Vin Diesel Lauren Graham Faith Ford Brittany Snow  Max Thieriot Carol Kane Brad Garrett
| music          = John Debney Peter James
| editing        = Christopher Greenbury
| studio         = Walt Disney Pictures Spyglass Entertainment Offspring Entertainment Buena Vista Pictures
| released       =  
| runtime        = 95 minutes
| language       = English
| country        = United States
| budget         = $56 million 
| gross          = $198,636,868 
}} Thomas Lennon Robert Ben Garant. It stars Vin Diesel. The film was released in March 2005 by Walt Disney Pictures, and in its first weekend earned United States dollar|US$17 million.

==Plot==
U.S. Navy SEAL Lieutenant Shane Wolfe is assigned to rescue Howard Plummer, a man working on a top-secret government project from a group of Serbian rebels. Wolfe and his team manage to get Plummer off an enemy boat; moments later, Wolfe and Plummer are shot while boarding the escape helicopter. Plummer is killed in the attack and Wolfe spends two months in the hospital. Wolfes commanding officer, Captain Bill Fawcett, is assigned to escort Plummers widow, Julie, to Zürich, where a safety deposit box belonging to the Plummers has been discovered. Wolfe is assigned to stay at the Plummer residence, in Bethesda, Maryland to search for the secret project called GHOST, hidden somewhere in the house, and to look after the familys five children: Zoe, Seth, Lulu, Peter, and Baby Tyler.

The kids prove to be difficult to handle, even with the help of nanny Helga: Zoe and Seth rebel against Wolfes authority; Lulu is obsessed with the new houseguest; the pet duck, Gary, resents Wolfe; and Helga finally quits when one of Zoe and Seths pranks go wrong that was intended for Wolfe. During which when Shane tries to prevent Helga from leaving, but fails, he gets a phone call from Julie that herself and Bill have to stay at Zürich for a few more days due to them not being able to unlock the box. Later, Wolfe finds Lulu and her troupe of Fireflies (Jordan Todosey, Nikki Shah, Maria Georgas and Emi Yaguchi-Chow) at the kitchen table. Wolfe takes them to Costco and while he does the shopping the troupe tries to sell cookies outside the store. However, things go horribly wrong when a group of obnoxious rival boy scouts come over and wreck their stall. When they finally reach home, they encounter an unexpected house party hosted by Zoe and her boyfriend, Scott. Wolfe forces all the guests to clean up the house before going home. Feeling that Wolfe does not care about the fact that the kids are still grieving over the death of their father, Zoe yells at him, right before they are then attacked by a pair of masked ninjas, whom Wolfe eventually defeats. He explains the entire situation to the kids, who agree to cooperate with him.

A few days later, the schools vice principal, Duane Murney, brings the facts that Seth has cut and bleached his hair for no apparent reason, has a Nazi armband in his locker, and has skipped every wrestling practice for the past month, to Wolfes attention. At home, Seth furiously yells that he only joined the wrestling team in obedience to his fathers wish. He sneaks out of the house, tricking the alarm system with a refrigerator magnet. Wolfe, leaving Zoe in charge, follows him to the town theater, where he learns that Seth has secretly joined an amateur production of The Sound of Music. The director quits when he believes the show will be a failure, whereupon Wolfe volunteers to take his place, and juggles this task with taking care of the house, giving Zoe driving lessons, and teaching Lulu and the Firefly Scouts martial arts to defend themselves against the rival boy scout troop.

Later, Seth quits the wrestling team at Wolfes prompting, confessing that he is in The Sound of Music after Murney catches him under the bleachers, practicing his dancing. When Murney threatens the boy, Wolfe challenges him to a wrestling match in front of the entire school. Despite Murneys show of bluster, Wolfe easily wins. The training Wolfe gives the Firefly Scouts becomes useful when they once again have a conflict with the thuggish scouts. The girls beat and tie up the boys, refusing to let them go until the thugs agree to stop bothering them. Zoe and Wolfe share stories of their fathers, both of whom have died in similar circumstances. They are interrupted by a phone call from Julie, who has guessed the password ("My Angel"), retrieved the item in a box (a special key), and is on her way home. The kids immediately begin to plan a Welcome Home party. Less than an hour later, Wolfe discovers a secret vault underneath the garage, which requires a key to open. When Fawcett and Julie arrive home, Fawcett and Wolfe go to the garage, where Wolfe says he is rethinking his career. The two ninjas seen earlier arrive armed, and pull off their masks, revealing themselves as the Plummers North Korean neighbors, the Chuns. Fawcett suddenly knocks out Wolfe, proving that he is in fact a double agent. They tie up and gag Zoe, Seth, and Lulu, place Peter and Tyler in the playpen, and Fawcett and Mrs. Chun take Julie to the vault while Mr. Chun looks after the children. They manage to open the door, but the dangerous security system prevents them from going any farther.

The children, all tied up tightly and gagged with cloths, managed to escape after untying themselves by taking down Mr. Chun though their teamwork. They awaken Wolfe, who goes to the vault to help Julie, sending the kids to summon the police. Mr. Chun follows them in his car; with Zoe at the wheel, the kids force him to crash. Wolfe figures out how to get past the security system, using a dance ("The Peter Panda Dance") used to make Peter go to sleep each night to avoid its traps. He, Julie, Fawcett, and Mrs. Chun engage in combat, and end the fight when Julie knocks out Mrs. Chun. Wolfes voice activates the final vault, knocking out Fawcett with the door. By then, the children have lured a large crowd of police to the house. Mr. Chun, however, holds all of them at gunpoint. Wolfe notices school principal and love interest Claire Fletcher (Lauren Graham) right behind him, having followed the chase when she saw it pass by the school. Wolfe, aided by Gary the duck, distracts Mr. Chun, whereupon Claire knocks him unconscious.

With their missions accomplished and Fawcett and the Chuns arrested, Wolfe and the Plummers say their goodbyes, and Wolfe and Claire share a kiss. The family attends Seths performance, where we learn that Wolfe has retired from the Navy and joined the school staff as the new wrestling coach. Murney is shown briefly on stage, where he is singing "Climb Evry Mountain" off-key while dressed in a nuns habit, as the film concludes.

==Cast==
* Vin Diesel as Lieutenant Shane Wolfe, United States Navy SEAL
* Faith Ford as Julie Plummer
* Lauren Graham as Claire Fletcher, the principal of the Plummer children’s school, formerly in the Navy
* Brittany Snow as Zoe Plummer, the eldest of the children; she is sixteen years old and a typical teenage cheerleader girl, she seems to have a crush on Scott
* Max Thieriot as Seth Plummer, the second oldest child. He is thirteen years old and a sullen teenage boy
* Morgan York as Lulu Plummer, the middle child and is ten years old.  
* Kegan and Logan Hoover as Peter Plummer, the second youngest; he is three years old and can only go to sleep when someone sings the "Peter Panda" song
* Bo and Luke Vink as Baby Tyler Plummer, the youngest; a seven-month-old infant Chris Potter as Captain Bill Fawcett, Wolfes commanding officer
* Carol Kane as Helga, the children’s Romanian nanny
* Brad Garrett as Vice Principal Dwayne Murney, the schools deep voiced vice-principal and wrestling coach
* Tate Donovan as Howard Plummer, a professor who develops "GHOST”
* Dennis Akayama and Mung-Ling Tsui as Mr. and Mrs. Chun, villainous North Korean spies looking for "GHOST" Scott Thompson as the director of a local production of The Sound of Music

==Soundtrack==
{{Track listing
| collapsed       = 
| headline        = 
| extra_column    = Original artist(s)
| total_length    =

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = yes
| lyrics_credits  = 
| music_credits   =

| title1          = Everyday Superhero
| note1           = 
| writer1         = Greg Camp, Jeff Barry
| lyrics1         = 
| music1          = 
| extra1          = Smash Mouth
| length1         = 3:28

| title2          = Saturday Night
| note2           = 
| writer2         = Ozomatli, J. Smith-Freeman
| lyrics2         = 
| music2          = 
| extra2          = Ozomatli
| length2         = 3:59

| title3          = We Will Rock You
| note3           = 
| writer3         = Brian May
| lyrics3         = 
| music3          =  Queen
| length3         = 2:01
 The Anthem
| note4           = 
| writer4         = Benji Madden, Joel Madden, John Feldman
| lyrics4         = 
| music4          = 
| extra4          = Good Charlotte 
| length4         = 2:55

| title5          = Skip to My Lou
| note5           =  Traditional
| lyrics5         = 
| music5          = 
| extra5          = Larry Groce and Disneyland Childrens Sing-Along Chorus
| length5         =
 The Power
| note6           = 
| writer6         = Benito Benites, John "Virgo" Garrett III, Toni C.
| lyrics6         = 
| music6          = 
| extra6          = Snap!
| length6         = 3:47

| title7          = Sixteen Going on Seventeen 
| note7           = The Sound of Music
| writer7         = Rodgers and Hammerstein
| lyrics7         = 
| music7          = 
| extra7          = Daniel Truhitte, Charmian Carr
| length7         =

| title8          = Climb Evry Mountain
| note8           = The Sound of Music
| writer8         = Rodgers and Hammerstein
| lyrics8         = 
| music8          = 
| extra8          = Shirley Bassey
| length8         =
 The Good, the Bad and the Ugly the movie with the same name
| writer9         = Ennio Morricone
| lyrics9         = 
| music9          = 
| extra9          = Bruno Nicolai & Unione Musicisti di Roma
| length9         = 2:45
}}

==Reception==

===Critical response===
The film received generally negative reviews. Review aggregation website Rotten Tomatoes gives the film a score of 20% based on reviews from 128 critics. The sites consensus is "Vin Diesel parodies his tough guy image for the family audience, but the result is only moderately amusing." 
Metacritic gives a rating of 30% based on reviews from 27 critics. 

Roger Ebert gives the film 2 stars out of 4. 

===Box Office===
The film opened at #1 in the box office upon its opening weekend with $30,552,694.  It would finish to earn 198,636,868 worldwide, making an adequate box office success.   

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 