Off Season (1992 film)
{{Infobox film
| name           = Off Season
| image          = Horssaisonposter.jpg
| caption        = Promotional poster
| director       = Daniel Schmid
| producer       = Marcel Hoehn Christoph Holch Luciano Gloor Martine Marignac
| writer         = Daniel Schmid Martin Suter
| starring       = Sami Frey Maria Maddalena Fellini Geraldine Chaplin Marisa Paredes Ingrid Caven
| music          = Peer Raben
| cinematography = Renato Berta	 	
| editing        = Daniela Roderer			
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = France Switzerland Germany
| language       = French
| budget         =
| gross          =
}} Swiss submission for the Academy Award for Best Foreign Language Film.

==Plot==
Adult narrator, (Sami Frey) recalls his myserious childhood at a mountainside hotel which he shared with his mother, grandmother and the hotels guests. The hotel is host to a series of interesting guests, from the actress, Sarah Bernhard (Paredes), an anarchist assassin (Chaplin), torch singers and seductive women.   Variety. 22 August 2011 

==Cast==
*Sami Frey as Narrator
*Maria Maddalena Fellini as Grandma
*Marisa Paredes as Sarah Bernhardt
*Geraldine Chaplin as Anarchist
*Ingrid Caven as Lilo
*Andréa Ferréol as Mlle Gabriel
*Arielle Dombasle as Mme. Studer
*Maurice Garrel as Grandpa
*Dieter Meier as Max
*Ulli Lommel as Prof. Malini
*Carlos Devesa as Valentin
*Irene Olgiati as Couple

==Reception== David Robinson wrote in The Times that the film offers "rich nostalgia" and that it "delights in the colourful ghosts of the place  ". 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 