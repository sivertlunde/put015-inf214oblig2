The Great Morgan
{{Infobox Film
| name           = The Great Morgan
| image          = 
| image_size     = 
| caption        = 
| director       = Nat Perrin
| producer       = 
| writer         = 
| narrator       = 
| starring       = Frank Morgan 
| music          = 
| cinematography = 
| editing        = 
| distributor    = Metro-Goldwyn-Mayer
| released       = 1945
| runtime        = 57 minutes
| country        = United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 American musical film|musical-comedy film released by Metro-Goldwyn-Mayer. The film is considered one of the more unusual in the MGM canon in that it is a compilation film built around a slight plot line, with a running time of less than 60 minutes.
 The Wizard of Oz) appearing as himself. The premise of the film is that Morgan is given a chance to put together his own movie, and he does so by combining several unrelated comedic and musical short subjects with his own short film.

Segments include a musical number featuring dancer Eleanor Powell cut from one of her early 1940s musicals (some sources erroneously state the scene comes from Broadway Melody of 1936, but in fact it was a scene cut from her 1939 film Honolulu (1939 film)|Honolulu), several songs by Carlos Ramirez, and a musical number by Virginia OBrien backed by Tommy Dorsey and orchestra. Among the non-musical segments of the film is a look at the history of the automobile in suburban America and a profile of a champion badminton player. There are also cameos by art director Cedric Gibbons, supervisor of sound Douglas Shearer and costume designer Irene.

The film was believed to be a lost film for many years. In 1980, a print was found, and the film was subsequently released to the American home video market.

==External links==
*  

 
 
 
 
 

 