The Grand Highway
 
 

Le grand chemin is a 1987 French film directed by Jean-Loup Hubert. It was released in the U.S. as Grand Highway, and was remade as Paradise (1991 film)|Paradise.
 Best Actress.

== Plot ==
Louis, a sickly nine-year-old boy from Paris, spends his summer vacation in a small town in Brittany. His mother Claire has lodged him with her girlfriend Marcelle and her husband Pelo while she is having her second baby. There Louis makes friends with Martine, the ten-year-old girl next door, and learns from her about life. His subsequent adventures run the gamut from delightful to terrifying, with a little "coming of age" (via a few glimpses of nudity) thrown in. Also animal brutality.

== Cast ==
*Antoine Hubert—Louis
*Richard Bohringer—Pelo
*Anémone—Marcelle
*Christine Pascal—Claire
*Vanessa Guedj—Martine

== Discography ==
The original soundtrack music composed by Georges Granier for The Grand Highway was reissued in 2013 by Canadian label Disques Cinemusique.  

== External links ==
* 

 
 
 
 
 
 

 