Twisted Rails
{{Infobox film
| name           = Twisted Rails
| image          =
| image_size     =
| caption        =
| director       = Albert Herman
| producer       = Peter J. White (producer)
| writer         = James R. Gilbert (screenplay) L.V. Jefferson (screenplay) L.V. Jefferson (story)
| narrator       =
| starring       = See below
| music          = Ernest Miller Monte Stebbins
| editing        =
| distributor    = Imperial Distributing Corporation
| released       =
| runtime        = 51 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Twisted Rails is a 1934 American film directed by Albert Herman. It was distributed by the independent Imperial Distributing Corporation for the states-rights market.  The plot concerns $50,000 in gold that is hidden on a train and a gangs efforts to get it.

== Cast == Jack Donovan as Jim Conway
*Alice Dahl as Mary McGuire
*Philo McCullough as Black Jack Bolivar Donald Keith as Louie Weinstock
*Victor Potel as Tom Watson
*Robert Buddy Shaw as Tommy McGuire
*Donald Mack as Dude Malloy
*Henry Roquemore as Gilbert Henderson
*Pat Harmon as Barney McGuire
*Tom London as Sheriff James
*Adabelle Driver as Mrs. McGuire
*Lawrence Underwood as Master mechanic

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 