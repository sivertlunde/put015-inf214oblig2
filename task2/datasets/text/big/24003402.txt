The Unborn 2
 
{{Infobox Film
| name           = The Unborn 2 or Baby Blood II
| director       = Rick Jacobson
| producer       = Roger Corman Mike Elliot
| writer         = Rob Kerchner and Daniella Purcell Scott Valentine Darryl Henriques Carole Ita White Brittney Powell Leonard O. Turner Anneliza Scott
| follows        = The Unborn (1991)
| released       = August 26, 1994
| runtime        = 83 minutes
| country        = United States
| language       = English
}}
 The Unborn. It was directed by Rick Jacobson and written by Rob Kerchner and Daniella Purcell.

==Plot==
A fertility experiment gone awry has created at least several disfigured children with extremely high IQs. A woman who had the treatment is making it her mission to kill the mutants one by one before they destroy humankind. One mutant childs mother is trying to save her own deformed child from the pursuer, but the baby leaves a path of destruction in its wake. 

==Cast==
*Michele Greene -  Catherine Moore 
*Robin Curtis -  Linda Holt  Scott Valentine -  John Edson 
*Darryl Henriques -  Artie Philips 
*Carole Ita White -  Marge Philips 
*Brittney Powell -  Sally Anne Philips 
*Leonard O. Turner -  Lieutenant Briggs 
*Anneliza Scott -  Officer Craig

==External links==
* 

==References==
 

 
 
 
 

 