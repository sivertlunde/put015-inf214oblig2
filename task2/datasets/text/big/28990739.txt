Davy (film)
{{Infobox film
| name           = Davy
| image_size     =
| image	         = "Davy"_(1958).jpg
| caption        = British theatrical poster
| director       = Michael Relph
| producer       = Basil Dearden William Rose
| based on = 
| starring       = Harry Secombe Alexander Knox Ron Randell
| music          =
| cinematography = Douglas Slocombe
| editing        =  Peter Tanner
| studio         = Ealing Studios
| distributor    = Metro-Goldwyn-Mayer
| released       = 1958
| runtime        = 83 minutes
| country        = United Kingdom English
| budget         = $458,000  . 
| gross          = $305,000 
| preceded_by    =
| followed_by    = last comedy to be made by Ealing Studios and had the distinction of being the first British film in Technirama.  Already a popular British radio personality on The Goon Show, Davy was intended to launch Harry Secombes solo career, but it was only moderately successful.  

  (1972). In 1995, Peter Frampton won the Oscar® for Best Makeup for Braveheart. He remembered his filming on Davy fondly, as "it meant time off school and (getting the) star treatment."  

==Plot==
A young entertainer is conflicted over the chance of a big break. He has to decide whether to remain with his familys music hall act or to go solo. An audition scene at Covent Garden includes an especially fine rendition of Puccinis Nessun Dorma by Secombe, who, while known mainly as a comedian, had a fine tenor voice, and Mozarts Voi Che Sapete performend by Adele Leigh. 

==Cast==
* Harry Secombe ...  Davy Morgan 
* Alexander Knox ...  Sir Giles Manning 
* Ron Randell ...  George 
* George Relph ...  Uncle Pat Morgan 
* Susan Shaw ...  Gwen  Bill Owen ...  Eric 
* Isabel Dean ...  Miss Helen Carstairs 
* Adele Leigh ...  Joanna Reeves  Peter Frampton ...  Tim 
* Joan Sims ...  Tea Lady 
* Gladys Henson ...  Beatrice, Tea Lady 
* George Moon ...  Jerry 
* Clarkson Rose ...  Mrs. Magillicuddy 
* Kenneth Connor ...  Herbie 
* Liz Fraser ...  Tea Lady  Charles Lamb ...  Henry 
* Arnold Marlé ...  Mr. Winkler 
* Campbell Singer ...  Stage Doorkeeper
==Reception==
According to MGM records the film earned only $40,000 in the US and Canada and $265,000 elsewhere resulting in a loss of $279,000. 

TV Guide called the film a "pleasant if unimpressive drama" ;   Britmovie wrote, " stylistically the film is an awkward combination of broad farce, Secombe having made his name as one of the denizens of the celebrated Goon Show, and awkward, turgid scenes of moral conflict" ;   while  Allmovie noted, "a stellar supporting cast enables Davy to overcome its occasional banalities and cliches."  

==References==
 

==External links==
* 
 

 
 
 
 
 
 

 
 