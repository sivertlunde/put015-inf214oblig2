Up the Academy
{{Infobox film
| name           = Up the Academy
| image          = Up the Academy.jpg
| caption        = Theatrical release poster
| director       = Robert Downey, Sr.
| producer       = Danton Rissner Marvin Worth
| writer         = Tom Patchett Jay Tarses
| starring       = Wendell Brown Tommy Citera Ron Leibman Harry Teinowitz Hutch Parker Ralph Macchio Tom Poston King Coleman Barbara Bach
| music          =
| cinematography = Harry Stradling Jr.
| editing        = Bud Molin Ron Spang Warner Bros. Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
}}
 teen comedy film released in 1980, with a plot about the outrageous antics of a group of misfits at a military school.

==Plot==
At the Sheldon R. Wienberg Academy, four young teens are sent to school and learn the discipline that the school teaches. Almost immediately, they dont like what is going on. Along the way, they plan their own actions from looking for girls to holding a party without the facultys knowledge. The films title comes from the fictitious academy itself.

==Production==
The movie was an attempt to cash in on the phenomenal and unexpected success of National Lampoons Animal House, which was also a movie made by a comedy magazine about a group of misfits at college.  In 1983, Mad (magazine)|Mad publisher Bill Gaines explained the genesis of his magazines involvement in the film to the Comics Journal:
 

It was directed by Robert Downey Sr., and starred Wendell Brown, Tommy Citera, Harry Teinowitz, Hutch Parker (younger brother of Parker Stevenson and now known as movie executive J. Hutchison), Tom Poston, Barbara Bach, Stacey Nelkin, Ralph Macchio (his screen debut) and King Coleman.  The movie was filmed entirely in Salina, Kansas, mostly on the campus of St. Johns Military School. 

== Response ==
The film was neither a commercial nor critical success when it was originally released, and was disowned by both the staff of MAD magazine and actor Ron Leibman (who, despite his sizeable role, had his name completely removed from the credits and promotional material). Besides paying Warner Bros. $30,000 to remove all references to MAD from the film when it was released on home video, MADs publisher William Gaines issued personal handwritten apologies to every person that wrote the magazine to complain. However, the film developed a small cult following.  Following Time Warners purchase of MAD (and after Gaines death in 1992), all references to the magazine were reinstated on cable television. In 2006, the original version of the film was issued on DVD.

==Production notes== Rick Baker designed the Alfred E. Neuman masks for the film.

* A young Robert Downey, Jr appears as an extra in some soccer scenes. 

==References in Mad magazine==
* In the tradition of MAD making fun of movies, the magazine spoofed their own film with "MAD Magazine Resents Throw Up the Academy". The parody mainly concerned Ron Leibmans name being removed, and the teenage troublemakers being punished by having to star in the film. Unlike most MAD movie parodies which are often several pages in length, this one was only two (appearing in place of the magazines usual letters column), as the piece devolved into a series of supposed interoffice memos by the writer, artist and editors, all decrying their role in the parody.  Finally, a fake note said that the entire staff of MAD quit over their shame. and the article was hereby discontinued.
* The statue featuring Alfred E. Neuman with a pigeon on his head is currently located in MADs editorial offices.  

==Notes==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 