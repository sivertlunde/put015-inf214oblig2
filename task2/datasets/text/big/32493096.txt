Ulkadal
{{Infobox film
| name           = Ulkkadal
| image          = Oolkatal.jpg
| caption        =
| director       = K. G. George
| producer       = K. J. Thomas Dr. George John
| writer         = George Onakkoor
| based on       =  
| starring       = Venu Nagavally Sobha Ratheesh Jalaja
| music          =  
| cinematography = Balu Mahendra
| editing        = M. N. Appu
| studio         = Naveenachitra Movie Makers
| distributor    =  Vijaya Movies
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Oolkatal ( ,  ) is a 1979 Malayalam musical film|musical-romantic drama film directed by K. G. George and starring Venu Nagavally and Sobha in the lead roles. The film, considered to be the first campus film in Malayalam, was adapted from the novel of the same name by George Onakkoor. It was shot mainly from Mar Ivanios College, Trivandrum.

Ootkatal marked the debut for the great actor Thilakan who was then a strong presence in the Malayalam theatre industry.

==Plot==
The films protagonist is Rahulan who loves three different women in different stages of his life. After his childhood love Thulasi commits suicide, Rahulan falls in love with his friends sister Reena. She hails from an orthodox Christian family and Rahulan never has the courage to approach her father for a marriage proposal. After completing college, Rahulan gets a teacher job. He eventually falls in love with Meera, a rich student of his. Meeras family fixes her marriage with Rahulan. One night, Reena visits Rahulan in his house and at the same time Meera and her father also come and is shocked to see another woman with Rahulan. In the climax, Rahulan accepts Reena into his life. On the parallel, another romance story is also told, between Davis (Rahulans friend and Reenas brother), and a nun.

==Cast==
 
* Venu Nagavally as Rahulan
* Sobha as Reena
* Ratheesh as Davis
* Jalaja as Susanna
* Jagathy Sreekumar as Shanku
* Sankaradi as Father Chenadan
* P. K. Venukuttan Nair as Reenas father
* Suchitra as Meera Anuradha as Thulasi
* Kumudam
* Azeez as Meeras father
* Dr. Namputhiri
* Konniyoor Vijayakumar
* William Dicruz
* Thilakan as Rahulans father
* Sreemathi George Varghese
* Thrissur Elsy as Meeras mother
* Omanammal
* Jessy
* Shanthi
 

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup.  

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ente kadinjool pranaya || K. J. Yesudas, Selma George || ONV Kurup ||
|-
| 2 || Krishnathulasikkathirukal || K. J. Yesudas || ONV Kurup ||
|-
| 3 || Nashtavasanthathin || K. J. Yesudas || ONV Kurup ||
|-
| 4 || Puzhayil Mungi Thazhum || K. J. Yesudas || ONV Kurup ||
|-
| 5 || Sharadindu Malardeepa || P Jayachandran, Selma George || ONV Kurup ||
|}

==References==
 

==External links==
*   at the British Film Institute Movie Database
*  
*   at the Malayalam Movie Database

 
 
 
 
 
 
 