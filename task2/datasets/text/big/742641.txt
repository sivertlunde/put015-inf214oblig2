Over the Edge (film)
{{Infobox film
|name=Over the Edge
|image=Over-the-Edge-1979-2.jpg 
|caption=DVD cover
|director=Jonathan Kaplan Tim Hunter
|producer=Robert S. Bremson (associate producer) Joe Kapp (associate producer) George Litto (producer)
|starring=Matt Dillon Michael Eric Kramer Pamela Ludwig Harry Northup
|music=Sol Kaplan Andrew Davis
|editing=Robert Barrere
|distributor=Orion Pictures
|runtime=95 min.
|released= 
|country=United States
|language=English
}}  The Warriors and Boulevard Nights, Over the Edge had a limited theatrical release in 1979. It stars 14-year-old Matt Dillon in his feature film debut.   
 Nirvana  Evil Eye" Fu Manchu. 

Over the Edge was inspired by actual events that took place in Foster City, California in the early 1970s. Those events were chronicled in a November 11, 1973, article the San Francisco Examiner entitled "Mousepacks: Kids on a Crime Spree". 

==Plot== Michael Kramer) and his friends, Richie White (Matt Dillon), Claude Zachary (Tom Fergus), and Johnny (Tiger Thompson) hanging out at the rec, which is the only place in the community where young people can spend time and be supervised by rec counselor Julia (Julia Pomeroy).

From an overpass, Mark Perry (Vincent Spano) and his friend shoot a BB hole in the windshield of a passing patrol car and flee on their bikes. They pass Carl and Richie and tell them to hide. Sgt. Ed Doberman (Harry Northup) arrives shortly, pulls them from their hiding place and finds a pocket knife on Richie. He takes them in and calls Carls father Fred (Andy Romano) to pick up his son.

Carls father is a local businessman. Before getting Dobermans call, he is talking to Homeowners Association president Jerry Cole (Richard Jamison) about getting wealthy landowner Mr. Sloan (Lane Smith) to buy land across the street from the rec and build an industrial park instead of the planned twin cinema and roller rink, to the loss of the local kids. Doberman questions the defiant Richie and Carl about the BB gun. Richie is then taken out of the room while Doberman lectures Carl and warns him that he could end up at the hill, a juvenile detention facility.

The next day at school, Claude announces that he took drugs to prepare for an upcoming test and has a bad reaction. Afterward, the students are assembled in the schools Cafeteria for a presentation about the recent shooting of the police car, where Carl exchanges smiles with Cory (Pamela Ludwig). That evening, Carl asks his father about the land across from the rec, and his father tells him about the proposed industrial park. An apoplectic Carl storms out to join friends at the rec.

The kids have brought drugs to the recs playground, where Claude buys a gram of hash from Tip (Eric Lalich). An announcement about a party at a nearby house causes the group to migrate there. Carl sees Mark making out with Cory on a couch. Mark threatens Carl not to mention his name to the cops. Carl then tells Cory that she could do a lot better than Mark and leaves, unknowingly followed by Mark and his friend who, after Doberman arrives in his patrol car to bust the party, beat up Carl as he is walking home alone. Carl stumbles home but is unable to sneak past his parents, who interrupt their meeting with Jerry Cole to grill the boy. After Carl goes upstairs, Jerry suggests to Fred that the rec should be closed the next day so that the kids wont be making trouble when Mr. Sloan and his people come to visit.

The next day at the rec, Doberman arrives and Julia talks to him about keeping the rec open, but over Julias objections, he comes inside, finds drugs on Claude and takes him into custody. They emerge from the rec amongst rowdy teens to find Richie standing defiantly on the roof of the patrol car. After a brief foot chase, Richie gets away.

Richie and Carl come upon Cory and her friend, who have just emerged from a house with a stolen pistol. They all go to a half-finished town home that Carl and Richie call their condo. They plan a picnic with a gun for the next day. On the way home, Carl sees Mr. Sloans car at his house and plants firecrackers under the hood. The firecrackers go off as the men are leaving, and Mr. Sloan is scared away from buying land in New Granada.

At the picnic the next day, the kids take turns shooting the pistol until the ammo is gone. Later, Claude tells Carl that it was Tip who sold him the hash, and Cory announces that Tip had recently gotten busted. They decide to pay Tip a visit and interrogate him. Tip admits he told the police whom he sold the hash to. The group throws Tip into the adjacent pond while Tips mother watches. Cory gives Carl a kiss, showing him that she likes him. When Carl gets home, his mother Sandra (Ellen Geer) tells him that he is forbidden to see his friends and that the rec will be closed. Carl gets in a fight with his father.

In school the next day, Carl overhears Tips mother revealing the names of the boys who assaulted her son. Carl grabs Richie and they run to Richies house, where Richie grabs the pistol and the keys to his moms Bronco. As they leave town, Doberman chases them. They flip the Bronco and flee the scene in opposite directions. Doberman chases Richie and fires a warning shot. Richie points his unloaded pistol at him and Doberman fires and kills Richie. Carl runs away to his condo. Cory meets him there and they spend the night together. After Cory leaves in the morning, Carl goes home to grab money, and along the way he sees Mark riding his dirt bike. He takes Marks BB gun and shoots him in the shoulder, which causes him to crash his bike. Mark sits down with Carl and they decide their fight was stupid and they should team up instead. Carl goes home, sneaks in and sees his mother on the phone discussing a community meeting about the kids at the school for that night. He flees to the rec where all of his friends are.

The kids decide to go to the school to confront the parents. While the meeting is in progress, the kids chain the doors closed, begin lighting fireworks, trashing the school and destroying cars in the parking lot. The kids break open a patrol car and pull out guns, eventually blowing up several cars and starting fires. Julia, locked inside the school, sees Johnny and convinces him to give her a phone. Police arrive and the kids run. Doberman speeds off to find Carl. Hes able to arrest him and they drive off. Mark is waiting down the road and shoots the police car, causing it to crash and catch fire. Carl escapes leaving the unconscious Doberman inside. The car explodes in a massive fireball.

The next morning, Carl boards a bus with other teens involved in the vandalism for their ride to the hill. As the bus goes beneath an overpass, Carl smiles as he sees Claude, Johnny, and Cory waving down to them.

==Cast==
*Michael Eric Kramer as Carl Willat
*Matt Dillon as Richie White
*Pamela Ludwig as Cory
*Harry Northup as Sgt. Doberman
*Vincent Spano as Mark Perry Tom Fergus as Claude Zachary 
*Andy Romano as Fred Willat
*Ellen Geer as Sandra Willat 
*Richard Jamison as Cole
*Julia Pomeroy as Julia 

==Soundtrack album==
Side one Cheap Trick My Best Friends Girl" – The Cars
#"You Really Got Me" – Van Halen
#"Speak Now or Forever Hold Your Peace" – Cheap Trick
#"Come On (Part 1)" – Jimi Hendrix
Side two
#"Just What I Needed" – The Cars
#"Hello There" – Cheap Trick
#"Teenage Lobotomy" – Ramones
#"Downed" – Cheap Trick
#"All That You Dream" – Little Feat
#"Ooh Child" – Valerie Carter

==References==
 

==External links==
* 
* 
* 
*An   with co-writer Tim Hunter

 

 
 
 
 
 
 
 
 
 
 
 