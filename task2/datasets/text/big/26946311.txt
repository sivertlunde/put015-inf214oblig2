Children of the Corn (1984 film)
 
{{Infobox film| name = Children of the Corn
| image = ChildrenoftheCornPoster.jpg
| caption = Original 1984 theatrical poster
| director = Fritz Kiersch
| producer = Donald P. Borchers Terence Kirby
| writer = George Goldsmith
| based on  =  
| starring = {{Plainlist|
* Peter Horton
* Linda Hamilton
}}
| music = Jonathan Elias Joao Fernandes (credited as Raoul Lomas)
| editing = Harry Keramidas
| distributor = New World Pictures
| released =  
| runtime = 92 minutes
| country = United States
| language = English
| budget = $800,000 (estimated)  
| gross = $14,568,589 USA  
}} of the ritually murder 2009 adaptation. Eight sequels have been produced.

==Plot==  John Franklin), takes all the children of Gatlin into a cornfield to speak to them about the prophecies of a strange, bloodthirsty incarnation of the Abrahamic God called "He Who Walks Behind The Rows". Isaac, through his lieutenant Malachai (Courtney Gains), leads the children in a revolution, brutally killing all of the adults in the town. Over the ensuing years, the children take any adults passing through as sacrifices.
 death cult. The couple place his body in the trunk. They encounter an old mechanic, who is no help, as the children of Gatlin have employed him to lead all adults passing through to the town, but they betray him and kill him anyway.

Burt and Vicky finally end up in Gatlin, after searching for several hours for a phone. A struggle ensues between the couple and the children as the couple are chased through the city. Burt and Vicky rescue Job and his little sister Sarah, who do not wish to be part of the cult. Vicky is captured by Malachai, and is prepared as a sacrifice before they track down and capture Burt and the children.

Meanwhile, Malachai and the others have grown weary of Isaacs arrogance. Assuming command over the children, Malachai orders Isaac to be sacrificed in Vickys place, though Isaac warns them that they will all be punished for this affront, as by sacrificing him they will have broken their covenant with He Who Walks Behind the Rows. Night soon falls and Burt enters the cornfield to rescue Vicky. The sacrifice begins and He Who Walks Behind The Rows (in the form of a writhing, amorphous, pale-glowing light) seemingly devours Isaac. Burt arrives and battles Malachai, telling the children that their minds have been poisoned and their humanity sacrificed in the name of a false god. As Malachai tries to regain control of the children, Isaacs re-animated corpse (possessed by He Who Walks Behind The Rows) appears and kills Malachai, breaking his neck.

Soon, a terrible storm gathers over the cornfield and Burt and Vicky gather the children inside a barn to shield them from He Who Walks Behind The Rows wrath. As the storm intensifies all around them, Job shows a Bible verse to Burt and Vicky that indicates that they must destroy the cornfield for the evil to cease (it is heavily implied that He Who Walks Behind The Rows is not the God of the Bible but an aspect of the Devil). While filling the irrigation pumps with gasohol fuel, He Who Walks Behind The Rows (this time in the form of both a burrowing underground shape and a demonic red cloud) lashes out at Burt, and prepares to destroy the barn. However, Burt is able to spray the fields with the flammable liquid and lights a Molotov cocktail, tossing it into the field, burning it and seemingly destroying the demon.

Burt, Vicky, Job and Sarah survive and are able to leave Gatlin as the cornfields burn. As Burt grabs the map they used to get there, a teenage girl who is a member of the cult jumps out at him from the back seat and attempts to stab him. Vicky knocks her out with the passenger door, and the four walk off into the distance to parts unknown.

==Cast==
*Peter Horton as Burt Stanton
*Linda Hamilton as Vicky Baxter
*R. G. Armstrong as Diehl ("The Old Man") John Franklin as Isaac Chroner
*Courtney Gains as Malachai Boardman
*John Philbin as Richard Amos Deigan
*Robby Kiger as Job
*Anne Marie McEvoy as Sarah

==Reception== At the Movies.   At Rotten Tomatoes, it has a "Rotten" rating of 38%, based on 24 reviews, with the average review being 4.1/10.  The film took in over $14 million at the US box office. 

==Television remake== TV remake of the first film, which would premiere on the Syfy channel. Production began in August, filming in Davenport, Iowa,    however, it was moved to Lost Nation, Iowa.(TJC)
 Daniel Newman short story, and not that of the original film.

==See also==
*Who Can Kill a Child?

==References==
 
==External links==
* 
* 
* 
*   at Google Video
*  at YouTube
* 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 