The Tenants (2009 film)
 
{{Infobox film
| name           = The Tenants
| image          = 
| caption        = 
| director       = Sėrgio Bianchi
| producer       = Sėrgio Bianchi
| writer         = Sėrgio Bianchi Beatriz Bracher
| starring       = Marat Descartes Ana Carbatti Umberto Magnani
| music          = Sėrgio Bianchi
| cinematography = Marcelo Corpanni Gilverto Otero
| editing        = Andrė Finotti
| distributor    = 
| released       =  
| runtime        = 103 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
}}
The Tenants ( ) is a 2009 Brazilian drama film directed by Sėrgio Bianchi.

==Cast==
*Marat Descartes - Valter
*Ana Carbatti - Iara
*Umberto Magnani - Dimas

==Global Lens 2011 series==
The film is featured in the Global Lens 2011 film series, sponsored by The Global Film Initiative

==Awards==
*Festival Do Rio - Best Screenplay, Best Supporting Actress

==References==
 

 
 
 
 

 