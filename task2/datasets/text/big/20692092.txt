Seven Women from Hell
{{Infobox film
| name = Seven Women from Hell
| image =Poster of the movie Seven Women from Hell.jpg
| caption =
| director = Robert D. Webb
| producer = Harry Spalding
| writer = Jesse Lasky Jr.
| narrator = Patricia Owens John Kerr Margia Dean
| music = Paul Dunlap
| cinematography = Floyd Crosby
| editing = Jodie Copelan
| studio = Associated Producers (API)
| distributor = 20th Century Fox
| released =  
| runtime = 88 min.
| country = United States
| language = English
| budget = $300,000 
}} Patricia Owens, Denise Darcel and Cesar Romero about women prisoners in a Japanese World War II prison camp, interned with other prisoners. 

==Plot==
When the Japanese invade New Guinea in 1942, Grace Ingram ( ), a pregnant American teenager; Ann Van Laer (Sylvia Daneel), a tightlipped but sympathetic German widow; Claire Oudry (Denise Darcel), a French waitress; Mai-Lu Ferguson (Pilar Seurat), a Eurasian nurse; and two other Americans, Mara Shepherd (Margia Dean) and Regan (Evadne Baker).

During a bombing raid, Janets baby is born dead and the humane Captain Oda (Bob Okazaki) is killed. Sergeant Takahashi (Richard Loo), his sadistic assistant, assumes command of the camp, and a friendly Japanese, Doctor Matsumo (Yuki Shimoda), helps the women escape.
 John Kerr), who helps them make their way to the beach but dies before they can reach safety. A wealthy planter, Luis Hullman (Cesar Romero), finds the girls, feigns friendship, and then attempts to hand them over to the Japanese. But the women learn of his plan, kill him, and escape by boat to the Allied lines. 

==Cast== Patricia Owens as Grace Ingram
*Denise Darcel as Claire Oudry
*Cesar Romero as Luis Hullman John Kerr as Lt. Bill Jackson
*Margia Dean as Mara Shepherd
*Yvonne Craig as Janet Cook
*Pilar Seurat as Mai-Lu Ferguson
*Sylvia Daneel as Anna Van Laer
*Richard Loo as Sgt. Takahashi
*Evadne Baker as Regan
*Bob Okazaki as Capt. Oda
*Yuki Shimoda as Dr. Matsumo
*Lloyd Kino as Rapist Guard
*Kam Fong as House Guard

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 
 


 