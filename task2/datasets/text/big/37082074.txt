The Snitch Cartel
 
{{Infobox film
| name           = The Snitch Cartel
| image          = The Snitch Cartel.jpg
| caption        = Film poster Carlos Moreno
| producer       = Manolo Cardona
| writer         = Luiso Berdejo Juan Camilo Ferrand
| starring       = Manolo Cardona
| music          = 
| cinematography = Mateo Londono
| editing        = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = Colombia
| language       = Spanish
| budget         = 
}}
 Carlos Moreno. Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.   

==Cast==
* Manolo Cardona as Martin
* Tom Sizemore as DEA Agent Sam Mathews
* Juana Acosta as Sofia
* Kuno Becker as Damian
* Diego Cadavid as Pepe Cadena Robinson Díaz as El Cabo
* Julian Arango as Guadana
* Andrés Parra as Anestesia
* Fernando Solórzano as Oscar Cadena
* Juan Pablo Raba as Pirulo
* Ilja Rosendahl as Prison Guard
* Jhonny Rivera as Antonio Villegas

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Colombian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 