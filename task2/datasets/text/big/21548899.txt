Andaru Dongale
{{Infobox film
| name           = Andaru Dongale
| image          = Andaru Dongale.jpg
| image_size     =
| caption        =
| director       = V. B. Rajendra Prasad
| producer       =
| writer         =
| narrator       = Lakshmi Nagabhushanam Halam
| music          = K. V. Mahadevan
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1974
| runtime        =
| country        = India Telugu
| budget         =
}}

Andaru Dongale (  directed by V. B. Rajendra Prasad. It is a remake of 1972 Bollywood film Victoria No. 203.  It is a commercial hit film ran for more than 100 days in 3 centers.

==Credits==

===Cast===
* Shobhan Babu   as   Kumar, Son of Durga Rao Lakshmi    as   Lakshmi
* S. V. Ranga Rao    as   Chanti Babu
* C. Nagabhushanam    as   Bujji Babu
* Kaikala Satyanarayana    as   Durga Rao
* M. Prabhakar Reddy   as   Subordinate of Durga Rao
* Rao Gopal Rao   as   Thief
* Mukkamala    as   Jailer Raja Babu    as   Dhairyam (compounder)
* Ramana Reddy    as   Police Constable
* K. V. Chalam

===Crew===
* Director: V. B. Rajendra Prasad
* Producer: Akkineni Ananda Rao
* Production Company: Pallavee Productions Bhamidipati
* Director of Photography: S. Venkat Ratnam
* Art Director: S. Krishna Rao
* Film Editing: A. Sanjeevi
* Original Music: K. V. Mahadevan
* Assistant composer: Puhalendi
* Lyrics: Acharya Atreya
* Playback Singers: P. Susheela, V. Ramakrishna, S. P. Balasubramanyam

==Soundtrack==
* Chantibabu O Bujjibabu
* Chusanura Ee Vela
* Gudugudu Guncham Gunderagam
* Gurudeva Mahadeva
* Naidollinti Kaada Nallatumma Chettu Kinda

==Box-office==
The film ran for more than 100 days in Hyderabad, Vijayawada and Visakhapatnam. 

==References==
 

==External links==
*  

 
 
 
 
 


 