Famine-33
{{Infobox film 
|  name           = Famine-33 Голод-33
|  image          = Famine-33.jpg
|  caption        = Film poster
|  director       = Oles Yanchuk 
|  producer       = Oleksiy Chernishov  Serhiy Dyachenko Les Taniuk 
|  starring       = 
|  music          = Viktor Patsukevych Mykola Kolondionok 
|  cinematography = Vasyl Borodin Mykhailo Kretov
|  editing        = 
|  distributor    = Dovzhenko Film Studios  1991 (Ukraine, Soviet Union) 
|  runtime        = 90 min. Ukrainian
|  budget         =  
}} 1991 film by Oles Yanchuk about the Holodomor famine in Ukraine, and based on the novel The Yellow Prince by Vasyl Barka.  The film is told through the lives of the Katrannyk family of six.  It relies more on images than on words shot in black-and-white.

"In an early scene, the members of an impoverished farming family solemnly take turns dipping their ladles into the single bowl of watery soup that is their only meal of the day. Later in the film, scores of villagers numb with despair and hunger huddle silently in the pouring rain outside a Government office until a truckload of armed soldiers arrives to disperse them. In the most poignant scene, a little boy who has lost his parents calls for his mother as he wanders, panic-stricken, through a snowy woodland where the trees are outnumbered by crosses marking the dead."  

== References ==
 

==External links==

 
 
 
 
 
 
 