Most Welcome 2
 
{{Infobox film
| name           = Most Welcome 2
| image          = 
| caption        = 
| director       = Ananta Jalil
| producer       = Monsoon Films Ananta Jalil
| writer         = 
| starring       = Ananta Jalil Afiea Nusrat Barsha
| music          = 
| cinematography = 
| editing        = 
| distributor    = Monsoon Films
| released       =  
| country        = Bangladesh Bengali
| budget          = 80&nbsp;million BDT 
| gross           = 18.5&nbsp;million BDT  . boxofficebangladesh.wordpress.com. 28 July 2014 
}} action film written by and directed by Ananta Jalil. It is considered to be the most expensive film in the history of Bengali Cinema.
Development began in February 2012. The film is a sequel of 2012 blockbuster Most Welcome, The film released in 2014. The film stars Ananta Jalil, Afiea Nusrat Barshaa in lead role

==Cast==
*Ananta Jalil as Police Officer aryan
*Afiea Nusrat Barsha as lead actress
*Champa as Ananta Jalils Mother
*Sohel Rana (actor) as Bangladeshi Scientist
*Misha Sawdagor as main villain

==Production==
Following the success of the previous installments, producers planned to develop a second sequel to the franchise, with Ananta Jalil playing the protagonist again.   The shooting of the film started on 9 February 2013.

==Release==
Most Welcome 2 released only in 19 theaters in Dhaka, but after second week it was released in 62 screens nationwide. It has also a limited release in Japan and UK. 

==Soundtrack==
{{track listing
| headline  = Track listing
| extra_column  = Singer(s)
| music_credits = yes
| collapsed = no
| title1  = Chicken Tandoori
| extra1  = Akassh
| music1  = Akassh
| length1 = 
| title2  = Tor Jaadu
| extra2  = Akassh & Amrita
| music2  = Akassh
| length2 = 
| title3  = Manush
| extra3  = Kailash Kher
| music3  = Akassh
| length3 = 
| title4  = Haowa Hoye Chuye Chuye
| extra4  = Bappa Mazumder & Naumi
| music4  = Emon Saha
| length4 = 
| title5  = Obak Prem
| extra5  = Shahid & Shuvomita
| music5  = Rafi
| length5 = 
| title6  = Bolna Tor Kotha
| exrta6  = Tanvir Tareq & Somchonda
| music6  = Tanvir Tareq
| length6 = 
}}

==See also==
* Most Welcome
* Nishwartha Bhalobasa
* List of Bangladeshi films of 2014

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 
 
 


 
 
 