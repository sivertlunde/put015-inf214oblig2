Recoil (2011 film)
{{Infobox film
| name = Recoil
| image = 
| caption = 
| director = Terry Miles
| producer = Jack Nasser 
| writer = John Sullivan Steve Austin Serinda Swan Danny Trejo Keith Jardine Lochlyn Munro Noel Gugliemi
| music = Kevin Belen
| cinematography = Bruce Chun
| editing = Trevor Mirosh Gordon Rempel
| released =  
| runtime = 94 minutes
| country = Canada
| language = English
}} Steve Austin and Danny Trejo. The film was released on direct-to-video|direct-to-DVD and Blu-ray in Canada on March 1, 2011. This movie is about a cop turns vigilante after his family has been murdered, exacting vengeance on the killers and then on all criminals who have slipped through the system.

==Plot==
Ryan Varrett (Steve Austin), is an ex-cop determined to avenge the murder of his family. In 2009, when Ryan was a cop in Dallas, Texas, his wife Constance Marie Varrett (Rebecca Robbins) and 9-year-old son Matt Ryan Varrett (Connor Stanhope) were viciously killed by a gang. Ryan survived the assault and killed three of the gang.

However, Ryan quits the Dallas police and hits the road to find everyone else responsible for the deaths of his loved ones. Special Agent Frank Sutton (Lochlyn Munro) of the FBIs Seattle office is investigating the deaths of criminals.

After killing a rapist named Dale James Burrows (Roman Podhora), Ryan ends up in the small town of Hope, Washington, where a gas station owner named Kirby (Patrick Gilmore) directs Ryan to a tiny hotel run by a woman named Darcy (Serinda Swan). Ryan finds Rex Ray Salgado (Noel Gugliemi), a member of the gang that attacked Ryans family.

It turns out that Hope is under the control of the Circle, a drug and arms dealing biker gang led by Rexs brother, Drayke Salgado (Danny Trejo), who is on the ATFs most wanted list. Sheriff Cole (Tom McBeath) is in Draykes pocket, so Ryan has only himself and Darcy to depend on.

Ryan kills Rex, Drayke declares war on Ryan, and the town of Hope becomes a battleground. It turns out that Drayke masterminded the shooting of Ryans family members, and the families of other cops who years ago sent him to prison, where he was brutalized. Drayke got even with the prisoners who brutalized him, then got even with the cops who put him away.

After Kirby is killed by Crab (Keith Jardine), Draykes right-hand man, Sutton arrives in town, and Cole tries to convince him that Rexs death was an accident. Cole thinks that by appeasing the bikers, he is protecting the town from more bloodshed. Ryan finds Crab and burns him to death.

Since Cole is not much help, Sutton goes to Coles son, Deputy Hedge (Adam Greydon Reid), who is not in Draykes pocket. Hedge explains that a long-time ago, the Circle used to protect the town. That was before Drayke took over and started running guns. Rex provided Drayke with a recipe for methamphetamines|meth, and Drayke and his gang started selling drugs too.

However, Drayke and his gang are arming themselves for all-out war against Ryan, and they dont care whom they have to kill. At night, Darcy hears bikers approaching her hotel. Ryan kills the three bikers. Outside, more bikers grab Darcy and beat Ryan up.

On the next day, Ryan has been taken to Drayke, and some of his men bring Darcy into the room. Theyre on an abandoned ferry boat that Drayke uses as his headquarters. Drayke leaves his man Prospect (Tygh Runyan) to beat Ryan up. Ryan breaks free from his restraints and takes care of Prospect. Ryan overcomes Draykes men, finds Darcy and frees her. Deputy Hedge goes to his fathers house and finds Cole has shot himself. With Suttons help, but Hedge plans to go after Drayke.

Ryan remembers Drayke being present during the attack on his family. Until Ryan came to Hope, Drayke thought Ryan was dead. Ryan and Drayke start fighting. A shot kills Drayke. It was Sutton who fired the shot. Darcy decides that shes going to stay in Hope, and Sutton decides to not arrest Ryan. Hedge becomes the new sheriff. Ryan decides to leave town, but promises Darcy that he will send her a postcard to let her know where he is.

==Cast== Steve Austin as Ryan Varrett
* Serinda Swan as Darcy
* Danny Trejo as Drake Salgado
* Keith Jardine as Crab
* Lochlyn Munro as Agent Frank Sutton
* Noel Gugliemi as Rex Salgado
* Adam Greydon Reid as Deputy Hedge
* Tom McBeath as Sheriff Cole
* Patrick Gilmore as Kirby
* Tygh Runyan as The Prospect
* Rebecca Robbins as Connie Varrett
* Connor Stanhope as Matt Ryan Varrett
* Roman Podhora as Dale Burrows
* Daniel Boileau as Frank
* Jase Anthony Griffith as Lou

==Production==
It was set and filmed at Agassiz, British Columbia, and Tacoma Washington in 56 days between January 2 and February 27, 2010.

==Illegal downloading==
In 2012, Canadian judge ruled in favour of Recoils production company, NGN Prima Production, and ordered internet service providers to reveal the names and addresses of people who had allegedly downloaded the film illegally. This is the only recently made possible due to changes in Canadian law, making it the first lawsuit of its kind in the country.  The further developments include one of the involved internet service providers filing the motion opposing disclosure of the customers information. 

==Home media== Region 1 in the United States on March 6, 2012, it was distributed by Nasser Entertainment. DVD and Blu-ray was released in Region 2 in the United Kingdom on 16 April 2012, it was distributed by Entertainment One.

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 