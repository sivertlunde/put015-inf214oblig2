No Trace of Sin
 
{{Infobox film
| name           = No Trace of Sin
| image          = 
| caption        = 
| director       = José Fonseca e Costa
| producer       = 
| writer         = Mário de Carvalho José Fonseca e Costa
| starring       = Victoria Abril
| music          = 
| cinematography = Eduardo Serra
| editing        = 
| distributor    = 
| released       =  
| runtime        = 104 minutes
| country        = Portugal
| language       = Portuguese
| budget         = 
}}
 Best Foreign Language Film at the 56th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Victoria Abril as Maria da Luz / Lucilia
* Saul Santos as Cadete
* Armando Cortez as Tenente Sanches (as Armando Cortês)
* João Perry as Uncle Miguel
* Mário Viegas as Aspirante Henrique Sousa Andrade
* Inês de Medeiros as Rita (as Inês DAlmeida)

==See also==
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Portuguese submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 