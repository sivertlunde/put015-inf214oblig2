The Fearless Vampire Killers
 
{{Infobox film
| name           = The Fearless Vampire Killers
| image          = Fearlessvampirekillersposter.jpg
| caption        = Film poster by Frank Frazetta
| director       = Roman Polanski
| producer       = Gene Gutowski
| writer         = Roman Polanski Gérard Brach
| starring       = Jack MacGowran, Roman Polanski, Sharon Tate, Ferdy Mayne
| narrator       = Vladek Sheybal
| music          = Krzysztof Komeda
| cinematography = Douglas Slocombe
| editing        = Alastair McIntyre
| released       = February, 1967 (UK) November 13, 1967 (U.S.)
| studio         = Cadre Films Filmways
| distributor    = Metro-Goldwyn-Mayer (1967, original) Warner Bros. (2005, DVD and 2013, Blu-Ray DVD  )
| runtime        = 108 min (directors cut)
| country        = United Kingdom United States
| language       = English
| budget         = 
}} comedy horror film directed by Roman Polanski, written by Gérard Brach and Polanski, produced by Gene Gutowski and co-starring Polanski with future wife Sharon Tate. It has been produced as a musical named Dance of the Vampires.

==Plot==
The film is set in the heart of Transylvania and the story appears to take place sometime during the mid-19th Century. Professor Abronsius, formerly of the University of Königsberg and his apprentice Alfred are on the hunt for vampires. Abronsius is old and withering and barely able to survive the cold ride through the wintry forests, while Alfred is bumbling and introverted. The two hunters come to a small Eastern European town seemingly at the end of a long search for signs of vampires. The two stay at a local inn full of angst-ridden townspeople who perform strange rituals to fend off an unseen evil.

Whilst staying at the inn, Alfred develops a fondness for Sarah, the daughter of the tavern keeper Yoine Shagal. Alfred witnesses Sarah being kidnapped by the local vampire lord Count von Krolock. Upon being told of their daughters kidnapping, Shagal attempts to rescue her but doesnt get very far before hes captured, drained of his blood and vampirised. Abronsius and Alfred begin pursuing the kidnapper soon after following the snow trail, which leads them to Krolocks ominous castle in the snow-blanketed hills nearby. They break into the castle but are trapped by the Counts hunchback servant, Koukol. They are taken to see the count, who affects an air of aristocratic dignity whilst questioning Abronsius about why he has come to the castle. They also encounter the Counts son, the foppish (and homosexual) Herbert. Meanwhile, Shagal no longer caring about his daughters fate, sets on his plan to turn Magda, the taverns beautiful maidservant and the object of his lust while he was still human, into his vampire bride.

Despite misgivings, Abronsius and Alfred accept the Counts invitation to stay in his ramshackle Gothic castle, where Alfred spends the night fitfully. The next morning, Abronsius plans to find the castle crypt and destroy the Count by staking him in the heart, seemingly forgetting about the fate of Sarah. The crypt is guarded by the hunchback, so after some wandering they attempt to climb in through a roof window. However, Abronsius gets stuck in the aperture, and it falls to Alfred to complete the task of killing the Count in his slumber. But at the last moment his nerve fails him and he cannot accomplish the deed. Alfred then has to go back outside to free Abronsius but on the way he comes upon Sarah having a bath in her room. She seems oblivious to her danger when he pleads for her to come away with him and reveals that a ball is to take place this very night. After briefly taking his eyes off her, Alfred turns to find Sarah has vanished into thin air.

After freeing Abronsius, who is half frozen, they re-enter the castle. Alfred again seeks Sarah but meets Herbert instead, who first attempts to seduce him and then after Alfred realises that Herberts reflection does not show in the mirror, reveals his vampire nature and attempts to bite him. Abronsius and Alfred flee from Herbert through a dark stairway to safety, only to be trapped behind a locked door in a turret. As night is falling, they become horrified witnesses as the gravestones below open up to reveal a huge number of vampires at the castle, who hibernate and meet once a year only to feast upon any captives the Count has provided for them. The Count appears, mocking them and tells them their fate is sealed. He leaves them to attend a dance, where Sarah will be presented as the next vampire victim.

However, the hunters escape by firing a cannon at the door by substituting steam pressure for gunpowder, and come to the dance in disguise, where they grab Sarah and flee. Escaping by horse carriage, they are now unaware that it is too late for Sarah, who awakens in mid-flight as a vampire and bites Alfred, thus allowing vampires to be released into the world.

==Cast==
 
*Jack MacGowran as Professor Abronsius
*Roman Polanski as Alfred, Abronsiuss assistant
*Sharon Tate as Sarah Shagal
*Ferdy Mayne as Count von Krolock
*Iain Quarrier as Herbert von Krolock
*Alfie Bass as Yoine Shagal, the innkeeper
*Terry Downes as Koukol, Krolocks servant
*Jessie Robins as Rebecca Shagal
*Fiona Lewis as Magda, Shagals maid

==Production== Daliesque quality, notable in its lighting and visual imagery. The outdoor sequences in the snow and the visuals inside the inn, the castle and the minuet scene are filmed to create a dreamlike image. The production design by Wilfrid Shingleton contributes to the captivating visual impression.
 Hallenbeck 2009: 83 

Cinematographer Douglas Slocombe said: "I think he (Roman) put more of himself into Dance of the Vampires than into any other film. It brought to light the fairy-tale interest that he has. One was conscious all along when making the picture of a Central European background to the story. Very few of the crew could see anything in it - they thought it old-fashioned nonsense. But I could see this background....I have a French background myself and could sense the Central European atmosphere that surrounds it. The figure of Alfred is very much like Roman himself - a slight figure, young and a little defenseless - a touch of Franz Kafka|Kafka. It is very much a personal statement of his own humour as he used to chuckle all the way through the scenes." 
 MGM wanted Hallenbeck 2009: 84. 
 Tanz der Vampire. It is peppered with numerous references to King Richard III of England, who even appears in the ball scene.

==Style and themes==
The Fearless Vampire Killers was Polanskis first feature to be photographed in color using a widescreen 2.35:1 aspect ratio. The film is also notable in that it features Polanskis love of winter sports, particularly skiing.

==Animated opening sequence==
The sequence begins on a sunny day where a bird minding his own business when the sun behind him sinks and the crescent moon rises in its place, starting night. The bird becomes alarmed and speeds way. Zooming out reveals the setting of a graveyard. Professor Abronsius and Alfred bump into each other, they shake hands and a green vampire pops out from a grave behind them and with each successful scare, the vampire laughs as the camera zooms in on him. A bag falls from the sky; Abronsius reaches in and takes out a box containing garlic. He takes out the garlic and he and Alfred eat it. They sneak up to the vampire (who had no idea where they are), tap his shoulder and breathe garlic breath on him, causing him to shrink and run off. The bag drops to them again and Alfred takes out a gold crucifix and gives it to Abronsius. They sneak up on the now plugged-nose vampire, show him the crucifix, and again he shrinks and run off. He hides behind the tree and tries to scare them, but the sun replaces the moon, signaling the vampire to run, and Abronsius and Alfred chase him to a coffin. The bag appears one last time and Abronsius takes out a mallet and a wooden stake. With Alfreds help, he kills the vampire, then places the lid on the coffin. In the pitch-black backdrop Professor Abronsius and Alfred congratulate each other until the MGM logo appears, startling them. The lion in the logo roars as its fangs grow longer. Frightened Abronsius and Alfred run away.

==Soundtrack==
The score was provided by Krzysztof Komeda, who also scored Rosemarys Baby (film)|Rosemarys Baby.

==In popular culture==
 
*The Bad Brains song "F.V.K." on their 1983 album Rock for Light is named in reference to this movie.
*A parody of the Dance of the Vampires ballroom scene is featured in the German comedy film Die Einsteiger starring Thomas Gottschalk and Mike Krüger. 
*The Skinny Puppy song "Rivers" features numerous dialogue clips from the movie. Frostbite was inspired by "Fearless Vampire Killers" in its comical approach to the vampire mythos as well as its dark and unexpected ending. Fearless Vampire Killers named themselves after the film.
*The Doom metal band Pagan Altar recorded a song titled Dance of the Vampires in 2011, using footage from the 1967 movie in an accompanying promotional video clip.

==DVD==
The Fearless Vampire Killers was released to DVD by Warner Home Video on September 13th, 2005 as a Region 1 widescreen DVD; a French Region 2 Blu-Ray-DVD was released on December 20th, 2013.

==See also==
*Vampire film

== Notes ==
 

== References ==
*  

==External links==
* 
* 
* 
* 
* 

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 