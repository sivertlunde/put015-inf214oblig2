The Vote That Counted
 
{{Infobox film
| name           = The Vote That Counted
| image          = 
| caption        = A surviving film still
| director       =
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       = 
| music          =
| cinematography =
| editing        =
| distributor    = Motion Picture Distributing and Sales Company
| released       =  
| runtime        =
| country        = United States English inter-titles
}}
 silent short short drama produced by the Thanhouser Company.

The film was released on January 13, 1911, it was the second of four films in the "Violet Gray, Detective" series. The film is presumed lost film|lost. 

== Plot ==
The Moving Picture World synopsis states, "State Senator Jack Dare, one of the reform members of the legislature, starts to the state capitol to attend an important session of that body. That he took the midnight train from his home city is clearly proven, for his aged mother was a passenger on it, and besides the conductor and porter are certain that he retired for the night Lower 9. In the morning, however, his berth is empty, although some of his garments are found there. The case puzzles the railroad officials and the police, and Violet Gray is given a chance to distinguish herself. She learns from the conductor and porter, who had happened to spend the night awake at opposite ends of the car, that the senator did not go by them. Consequently this leaves only the window as his means of egress, and she knows that he must have gone that way. Violet discovers that Dare is a hearty supporter of a bill that a powerful lobby is trying to defeat. The fight is so close that his is the deciding vote. Dare cannot be bribed, so his opponents spirited him away in a novel fashion. But the girl finds where he is hidden and brings him back, although he is much injured. He reaches his seat in time to cast the needed vote, and to astound and defeat the lobby." 

== Cast and production ==
The only known credit in the cast is  Julia M. Taylor as Violet Gray.  Film historian Q. David Bowers does not cite any scenario or directorial credits.  At this time the Thanhouser company operated out of their studio in New Rochelle, New York. In October 1910, an article in The Moving Picture World described the improvements to the studio as having permanently installed a lighting arrangement that was previously experimental in nature. The studio had amassed a collection of props for the productions and dressing rooms had been constructed for the actors. The studio had installed new equipment in the laboratories to improve the quality of the films.  By 1911, the Thanhouser company was recognized as one of leading Independent film makers, but Carl Laemmles Independent Moving Picture Company (IMP) captured most of the publicity. Two production companies were maintained by the company, the first under Barry ONeil and the second under Lucius J. Henderson and John Noble, an assistant director to Henderson. Though the company had at least two Bianchi cameras from Columbia Phonograph Company, it is believed that imported cameras were also used. The Bianchi cameras were unreliable and inferior to competitors, but it was believed to be a non-infringing camera, though with "rare fortune" it could shoot up to 200 feet of film before requiring repairs. 

==Release and reception ==
The single reel drama, approximately 1,000 feet long, was released on January 13, 1911.   

The Lubin Manufacturing Company would make a film with a reference to the series with Violet Dare, Detective in June 1913.   

== References ==
 

 
 
 
 
 
 
 
 
 