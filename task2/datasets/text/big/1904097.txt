The Exterminator
{{Infobox film
| name            = The Exterminator
| image           = Exterminator ver1.jpg
| caption         = Theatrical release poster
| director        = James Glickenhaus
| writer          = James Glickenhaus
| starring        = Robert Ginty Samantha Eggar Christopher George
| music           = Joe Renzetti
| cinematography  = Robert M. Baldwin
| editing         = Corky OHara
| producer        = Mark Buntzman
| distributor     = Avco Embassy Pictures Media Blasters Synapse Films
| released        =  
| runtime         = 104 minutes
| country         = United States
| language        = English
| budget          = $2,000,000
}}
The Exterminator is a 1980 vigilante film that was written and directed by James Glickenhaus and starring Robert Ginty as Vietnam veteran vigilante John Eastland, also known as "The Exterminator", who takes out the street punks and those involved in organized crime when the law fails to do justice.

== Plot == Steve James). Both men, along with several other U.S. soldiers are quickly captured by the Viet Cong. The film cuts to an open-air VC camp where we next see the soldiers tied to wooden stakes in the ground. The VC commander demands information on when a U.S. strike is going to occur. Eastland refuses to answer and the commander slowly decapitates one of his fellow soldiers. Jefferson manages to get free and quickly slaughters the Vietnamese soldiers. 

The film then shifts to New York, where Eastland and Jefferson work in a food warehouse. One day, a group of thugs called the Ghetto Ghouls attack Eastland while he is working after Eastland catches them trying to steal beer. Jefferson comes to his aid. They defeat the thugs, but the gang return to cripple Jefferson by gouging his spine with a meathook, leaving him paralyzed. Eastland, taking the law into his own hands, interrogates one of the members with a flame thrower to get information of the gangs hideout. Eastland tells the thug that if theyre lying, then he will be back. Eastland then attacks the gangs base of operations, shooting one gang member and leaving two others tied up in the basement, which is full of hungry rats.   

Eastlands vigilante justice does not end there. The warehouse where he works has been cowed into paying protection money to organized crime. Unrelenting, even though Congress has stated it will hold hearings to investigate the price of meat in New York, the mob has squeezed money out of the paychecks of workers such as Jefferson. Eastland manages to kidnap one of the mobsters, placing him, chained up above an industrial meat grinder. Eastland needs the safe number from the mobster to steal his money. After the mobster is lowered dangerously close to the jaws of the meat grinder, the mobster gives up the safe number, as well as giving Eastland his set of keys. Eastland asks if theres anything else he should know; the mobster tells him no. Eastland repeats his earlier line that if they are lying, then he will be back. Eastland barely survives an attack by the mobsters Doberman, (who he dispatches with an electric carving knife). Upon returning, Eastland lowers the mobster into the grinder for lying. Eastland gives the money to the Jeffersons. 
 muggers who had just attacked and robbed an elderly woman.

Meanwhile, the CIA has heard of the Exterminator and reaches odd conclusions. Based on the current administrations promise to cut down crime rates, they believe that the Exterminator is either an opposition partys stunt or a foreign powers ruse to humiliate the current administration by exposing their inability to handle the crime problem. They monitor Daltons investigation of the Exterminator. Dalton, working from a bootprint found at the mobsters home, discovers the Exterminator wears a hunting boot manufactured by a mail order firm in Maine. Asking them for a list of clients in New York, and following the hunch that the Exterminator may be a veteran (since when he killed the Ghetto Ghouls he was seen with an M16 rifle|M-16), Dalton has narrowed the suspects accordingly. 

In the dénouement, Eastland visits Jefferson in the hospital. Jefferson has asked to see him. Never being able to walk again, Jefferson wishes that Eastland would kill him. Eastland does, but coincidentally, Dalton is visiting the hospital at the time. When he learns about Jeffersons death, Dalton concludes (apparently having investigated Jefferson earlier as a possible suspect for the Exterminator and having learned about the attack on him) that one of Jeffersons friends was the Exterminator, and learns that Eastland was one of them. 
 bugging his phone, hear his call with Dalton, and ambush them at the rendezvous. Eastland escapes alive.

== Cast ==
*Christopher George as Detective James Dalton
*Samantha Eggar as Dr. Megan Stewart
*Robert Ginty as John Eastland/The Exterminator Steve James as Michael Jefferson
*Tony DiBenedetto as Chicken Pimp
*Dick Boccelli as Gino Pontivini
*Patrick Farrelly as CIA Agent Shaw
*Michele Harrell as Maria Jefferson David Lipman as The State Senator from New Jersey
*Tom Everett as Hotel Clerk
*Ned Eisenberg as Marty
*Irwin Keyes as Bobby
*Cindy Wilks as Candy
*Dennis Boutsikaris as Frankie
*Mark Buntzman as Burping Ghoul
*Tony Munafo as Pontivinis Bodyguard
*Judy Licht as herself

==Production==
James Glickenhaus was a maker of industrial, educational and promotional films in his native New York City.  He had previously made one other feature film The Astrologer, in 1977.  His experiences taking the film for showings in the Southern part of the United States led him to interview cinema owners and observe their audiences which led him to make a more commercial film.  Glickenhaus also took The Astrologer to Cannes where he felt that the lengthy dialogue of the film worked against it.  He vowed to make his next film with as little dialogue as possible as his observance of the audiences made him feel that they didnt enjoy reading subtitles or hearing dubbing of dialogue where the original nuances would not translate.
 The Paper Coming Home. Steve James had originally tested for the role of a bartender but Glickenhaus was so impressed with his reading he enlarged the role of the lead characters friend. 

The Vietnam sequence was filmed after most of the film was completed.  It was filmed in Indian Dunes, California where   was made; using the special effects by Stan Winston. The sequence was done in five days using 15% of the budget. 

The MPAA demanded and received 47 seconds of cuts to the violence which remained in the version shown in Japan.

Glickenhaus wished to make other types of films so was not involved with the sequel. In his opinion, the film First Blood would have been his ideal sequel.

Jazz saxist Stan Getz briefly appears performing at an outdoor concert.

==Reception==

The Exterminator received negative reviews, earning a 22% approval rating on Rotten Tomatoes.

==Home video releases==
On 13 September 2011, Synapse Films and Media Blasters released The Exterminator on a DVD/Blu-ray Disc combo pack.  Extra features include the films theatrical trailer, some TV spots, and a commentary with writer/director James Glickenhaus. 

==Sequel==
A sequel, Exterminator 2, was released in 1984.

==References==
 

==External links==
*  
*  
 

 
 
 
 
 
 
 
 
 