Doni Sagali
{{Infobox film
| name           = Doni Sagali
| image          = 
| director       = Rajendra Singh Babu
| producer       = H. N. Maruthi   N. M. Madhusudhan Gowda
| screenplay     = Rajendra Singh Babu
| story          = C. N. Muktha
| based on       =  
| starring       = Soundarya   Shashikumar   Girish Shetty
| music          =  V. Manohar
| cinematography = D. V. Rajaram
| editing        = Basavaraj Urs
| studio         =  Anjan Arts
| released       = 1998
| country        = India
| language       = Kannada
}}
 Kannada drama film directed by Rajendra Singh Babu. The storyline is based on the novel Vimukthe by Dr. C. N. Muktha.   The film stars Soundarya and Shashikumar in the lead roles.

The film was released in 1998 and met with critical appraisals. The film went to win   and Best cinematographer for D. V. Rajaram. The films soundtrack and score was composed by V. Manohar who also wrote the songs.

==Cast==
* Shashikumar
* Soundarya
* Girish Shetty
* Siddharth
* Suman Nagarkar
* Mynavathi
* Shankar Ashwath
* Harish Raj
* Shanthamma
* Tennis Krishna

==Soundtrack==
{|class="wikitable"
! Sl No. !! Song Title !! Singer(s) !! Lyrics
|-
| 1 || "Oho Chiguritu" || S. P. Balasubrahmanyam, K. S. Chithra || V. Manohar
|-
| 2 || "Nakshatrave" || K. S. Chithra || V. Manohar 
|-
| 3 || "Ah Nidirege" || Rajesh Krishnan, K. S. Chithra || V. Manohar
|-
| 4 || "Swarada Sangama" || Ramesh Chandra || V. Manohar
|-
| 5 || "Gulmohar" || K. S. Chithra || V. Manohar
|-
|}

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 

 
 