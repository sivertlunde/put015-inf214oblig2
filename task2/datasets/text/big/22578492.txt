La fille de Monaco
{{Infobox film
| name           = The Girl from Monaco
| image          =   
| image size     =
| caption        =    Anne Fontaine
| producer       = Philippe Carcassonne Bruno Pésery
| writer         = Anne Fontaine Benoît Graffin
| narrator       =
| starring       = Fabrice Luchini Roschdy Zem Louise Bourgoin Stéphane Audran
| music          = Philippe Rombi
| cinematography = Patrick Blossier
| editing        = Maryline Monthieux	
| studio         =   
| distributor    =   
| released       = 20 August 2008 (France)
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         =
| gross          = $599,595 (USA) (25 October 2009) 
| preceded by    =
| followed by    =
}}
 French film Anne Fontaine. The film stars Fabrice Luchini, Roschdy Zem, Louise Bourgoin, and Stéphane Audran.

== Plot ==

Middle-aged and highly successful lawyer Bertrand Beauvois (Fabrice Luchini) is hired by Monaco businessman Louis Lassalle (Gilles Cohen) to defend his mother Édith Lassalle (Stéphane Audran), who has killed her former lover. Lassalle assigns a bodyguard to Beauvois, Christophe Abadi (Roschdy Zem).

Audrey Varella (Louise Bourgoin), a beautiful local TV weather girl, who is highly promiscuous, and whose previous lovers include Christophe, enamors Beauvois, hoping to make a better life with him. This despite the warnings of Christophe to Beauvois, who have formed a bond of friendship, to stay away from her.

Audrey spends all her time with Beauvois, including nights of exhausting wild sex, and Beauvois entreats Christophe to do something that she should disappear from his life. He then continues to the court, for his final plead in the Lassalle case.

After having sex with her, Christophe pushes Audrey and her scooter from the road, and kills her. Beauvois willingly takes the blame. In one of the last scenes we see Mrs. Lassalle being freed prison after only one year of imprisonment, while Beauvois remains among the inmates.

== Awards and nominations ==
*César Awards (France) Best Actor &ndash; Supporting Role (Roschdy Zem) Most Promising Actress (Louise Bourgoin)

== Notes ==
* 

 

 
 
 
 
 
 

 