Such a Little Queen (1914 film)
{{infobox film
| name           = Such a Little Queen
| image          = Mary pickford such a little queen ad.png
| imagesize      = 200px
| caption        = Ad for the 1914 film Hugh Ford
| producer       = Adolph Zukor Daniel Frohman Channing Pollock (play) Hugh Ford
| starring       = Mary Pickford
| music          =
| cinematography = Ernest Haller
| editing        =
| distributor    = Paramount Pictures
| released       = September 21, 1914
| runtime        = 5 reels
| country        = United States Silent (English intertitles)
}} 1914 American silent film Channing Pollock 1921 with Constance Binney in the lead. Cinematographer Ernest Haller was in charge of photography on both films. 
 romantic comedy in five parts. 

==Cast==
*Mary Pickford - Queen Anna Victoria
*Carlyle Blackwell - King Stephen
*Harold Lockwood - Robert Trainor
*Russell Bassett - The Prime Minister
*Arthur Hoops - Prince Eugene

==See also==
*List of lost films

==References==
 

==External links==
* 
* 
* 


 
 
 
 
 
 
 