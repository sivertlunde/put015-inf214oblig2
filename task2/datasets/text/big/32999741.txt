The St. Louis Kid
{{Infobox film
| name           = The St. Louis Kid
| image          =
| caption        =
| director       = Ray Enright
| producer       = Samuel Bischoff
| writer         = Frederick Hazlitt Brennan Warren Duff Seton I. Miller
| narrator       =
| starring       = James Cagney Patricia Ellis Allen Jenkins
| music          =
| cinematography = Sidney Hickox
| editing        = Clarence Kolster
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The St. Louis Kid is a 1934 drama film starring James Cagney as a truck driver who gets mixed up in a union dispute after a union leader is killed and his girlfriend is kidnapped after witnessing the crime.

==Cast==
* James Cagney as Eddie Kennedy
* Patricia Ellis as Ann Reid
* Allen Jenkins as Buck
* Robert Barrat as Farmer Benson
* Hobart Cavanaugh as Richardson
* Spencer Charters as Messeldopp
* Addison Richards as Mr. Brown
* Dorothy Dare as Gracie Smith
* Arthur Aylesworth as Judge Jeremiah Jones Charles C. Wilson as Mr. Harris - the Trucking Company Boss 
* William B. Davidson as Joe Hunter 

==External links==
*  

 

 
 
 
 
 
 
 
 
 

 