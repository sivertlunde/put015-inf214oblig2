Sreeram
 
{{Infobox film
| name           = Sreeram
| image          = Dharani Paruchuri Brothers Anita Hassanandani Ashish Vidyarthi
| director       = V. N. Aditya
| producer       = Burugapally Siva Rama Krishna Shiva Kumar
| editing        = Marthand K. Venkatesh
| music          = R. P. Patnaik
| released       =  
| language       = Telugu
| country        = India
| budget         = 12 crores
| box office     = 33 crores
}}
 2002 Cinema Tollywood film Anita Hassanandani Vivek in critical roles. The film received positive reviews and was declared a Hit.

==Plot== Anita Hassanandani). One evening, Sreeram beats a guy black and blue as that guy misbehaves with Madhu. Later on, Sreeram realizes that the person whom he has beaten is a deadly CI (Circle Inspector) of Police whose name is Encounter Shankar (Ashish Vidyarthi). Sreerams sister marriage is settled with the brother of Madhu. When the marriage of Sreerams sister is about to happen, Encounter Shankar traces Sreeram and beats him up badly in police station and leaves him on a railway track and expects the train to run over Sreeram. But luckily, Sreeram escapes from death. The parents of Sreeram find out the beaten up Sreeram with the help of Narayana and they join him in a hospital. The doctor inspects the injuries, dresses him up and tells them that Sreeram wont be able to move his body for 3 months and he needs complete rest. But his interview for SI post is scheduled in the next month, which requires him to be physically fit. He motivates himself and attends interview as with 100% fitness. In the meantime, Encounter Shankar is under impression that Sreeram is dead. But Sreeram is selected for the post of SI and Encounter Shankar is asked to enquire and give the conduct certificate of Sreeram. After confirming that Sreeram is alive, Encounter Shankar gives a bad conduct certificate. And he also teases Sreeram into a street fight and takes clandestine photographs and produces them to the higher authorities as evidence. The hide and seek game between Sreeram and Encounter Shankar comes to an end when Encounter Shankar comes to Sreerams place to humiliate him. How Sreeram wins over Encounter Shankar is the point that is covered in the climax.

 
 

 
 
 
 

 