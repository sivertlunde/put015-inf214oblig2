Magandang Hatinggabi
 

{{Infobox film
| name           = Magandang Hatinggabi
| image          = 
| caption        = 
| director       = Laurenti Dyogi
| writer         = Ricardo Lee
| producer       = Charo Santos-Concio Malou Santos Tess Fuentes
| screenplay     = 
| based on       = 
| narration by   = Noni Buencamino
| starring       = Angelika dela Cruz Jericho Rosales Angelica Panganiban Bojo Molina Mylene Dizon Marvin Agustin Diether Ocampo Jacklyn Jose Eula Valdez Allan Paule Noni Buencamino
| cinematography = Joe Tutanes
| editing        = Kelly Cruz
| music          = Greg Caro
| studio         = 
| distributor    = Star Cinema
| released       =  
| runtime        = 100 minutes
| country        = Philippines English Tagalog Tagalog
| budget         = 
| gross          = 
}}
 Filipino comedy film|comedy-horror film directed by Laurenti Dyogi and release by Star Cinema. The film is divided into three episodes, each consisting of different set of stories, characters, and settings. The episodes are presented as a stories of urban legends as told by the character of Noni Buencamino.

==Episodes==

===Killer Van===

====Cast and characters====
*Noni Buencamino - Killer
*Eula Valdez - Carla
*Allan Paule - Rene
*Angelica Panganiban - Myla
*Alwyn Uytingco - Carlo / Paolo
*Lorena Garcia - Ghost
*Fatima Vargas - Lilys Mom
*Andy Bais - car dealer

===Kuba===

====Cast and characters====
*Noni Buencamino - Master Gago
*Jacklyn Jose - Gloria
*Angelika dela Cruz - Marianne
*Jericho Rosales - Darwin
*Boom Labrusca - Darwins Friend
*Lui Villaruz - Darwins Friend
*CJ Tolentino - Darwins Friend
*Monina Bagatsing - Dahlia
*Corrine Mendez - Tracy
*Mary Kaye De Leon - Zeny
*Wilson Santiago - Glorias Husband
*Paz Bautista - Glorias Double
*Gilberto Lariba - Master Aswang
*Rigor Ferrer - Master Aswang
*Eva Atienza - Mariannes Double
*Pelocronio Campos - Master Kapre

===Fatman===

====Cast and characters====
*Noni Buencamino - Fatman
*Marvin Agustin - Frankie
*Diether Ocampo - Louie
*Mylene Dizon - Abby
*Bojo Molina - Richie
*Laura James - Kaye
*Jay Delos Reyes - Slimy Creature
*Kay Flores - Slimy Creature
*Andre Velasco - Greenman

==External links==
 

 
 
 
 
 
 
 