881 (film)
{{Infobox film name = 881 image = 881_Promotional_Poster.jpg caption = Theatrical poster director = Royston Tan producer = Daniel Yun Eric Khoo James Toh John Ho Mike Wiluan writer = Royston Tan starring = Qi Yuwu Yeo Yann-Yann Mindee Ong Liu Lingling music = Funkie Monkies Productions Poh Tiong-Cai Robert Mackenzie cinematography = Daniel Low editing = Low Hwee-Ling distributor = Zhao Wei Films released = 9 August 2007 (Singapore) runtime = 105 minutes country = Singapore
|language Mandarin Hokkien budget = Singapore dollar|S$1,000,000   Channelnewsasia. Retrieved 8 August 2007.  gross = followed_by =
}} Singaporean musical film|musical-comedy-drama film written and directed by Royston Tan, based on the Singapore Getai scene. It is only the second Singaporean film that has been released in Japan. 
 Academy Award Golden Horse Awards by Mediacorp, and received a nomination for best makeup and costume design. 

==Plot==
A talented student from a broken home and an orphan who lost both of her parents at a young age, Big Papaya and Little Papaya grow up idolizing Chen Jin Lang, the King of Hokkien Getai, and dream of becoming Getai singers themselves.

After a chance meeting at a Chen Jin Lang concert and coached by their seamstress, Little Papayas Aunt, Ling Yi (Aunt Ling), the Papaya sisters struggle at first because they have no “feel” in their voices. As a last resort, they appeal to Aunt Ling’s estranged twin sister, the Goddess of Getai, for help. She grants them their wish, but warns them the price will be high, especially if they do not obey the rules of Getai. One of the rules states that they shall not love or be loved by any man.

The Papaya sisters hit the Getai circuit and sing their hearts out with their newfound “feel”. Big Papaya is pursuing her dream in spite of strong parental objections, to the point of being kicked out of her house and is now living with Aunt Ling and Little Papaya. Little Papaya, being an orphan, is in a personal race against time to realize her potential. Like her deceased parents, Little Papaya suffers from cancer and has little time left. Through their perseverance, hard work and Aunt Ling’s amazing costumes, the Papayas become the most popular sister act in town. They use their fame to help raise funds to help Chen Jin Lang, who is also suffering from cancer. But they are soon devastated by the death of their idol. While Little Papaya grieves, Big Papaya finds solace in the arms of Guan Yin, Aunt Lings mute son and their driver.

Unknown to them, rival sister group Durian Sisters have become intensely jealous of the Papayas success, and are determined to trip them up by messing up their schedule. With the help of their gangster Godfather, the Durians succeed in shutting the Papayas out of many Getai. They are ruthless in their underhand attacks on the Papayas, using magical darts to hurt their rivals. Guan Yin cannot defend them, and even the Goddess is appalled by the tactics employed by the Durians. Although she grants the Papayas whatever celestial powers she can bestow, she warns them that there is a limit to what she can do.

At a confrontation, the Durians challenge the Papayas to a showdown. The stakes are high: whoever loses will leave the getai scene for good. The Papayas accept, and start preparing for the big day – new costumes, new songs, new dances. The movie climaxes with a dramatic musical battle. Both sides pull out all the stops to win over the audience. Eventually Little Papaya collapses from the strain.

Distraught, Big Papaya begs the Goddess of Getai for assistance but receives none, as she has broken the rule by being with Guan Yin. Little Papaya wastes away and eventually dies.

Years later, Big Papaya is still performing Getai. Guan Yin mentions that he looks forward to the Seventh Month more, as that is the only time they can all be together

 

==Cast==
* Mindee Ong as Little Papaya
* Yeo Yann-Yann as Big Papaya
* Liu Lingling as Goddess of Getai (Xiangu) / Aunt Ling
* Qi Yuwu as Guan Yin
* Kelvin Tan as Chen Weilian
* Wang Lei as Wang Lei
* Teh May-Wan and Teh Choy-Wan as Durian Sisters

==Production==
The films story was originally conceived as a joke after Royston and the two lead actresses agreed that Getai was one of Singapores uniquely Singaporean cultural attributes. 

It took Royston Tan 22 days to produce the film and only two weeks to write it, which, according to him, was the easiest and fastest script he has written.  The large variety of costumes used in the production cost upwards of S$100,000.

==Critical reception==
Even before its official release, 881 garnered good reviews from critics 

==Cultural significance==
The films name, pronounced in Mandarin, sounds like Papaya, which is the name of the main characters getai group. The largely non-English/Malay speaking audience portrayed in the movie is seen chanting and holding up signboards painted with the numbers instead of the romanised term.

==Soundtrack==
The official soundtrack was released the evening before the show opened, on the August 8. Produced by Eric Ng of Funkie Monkies Productions, the soundtrack consists of 15 songs (1 hidden) and 4 music videos. The theme song, One Half, is sung by Wu Jiahui. A second volume of the original soundtrack has since been released with 18 songs.

==Egg pelting incident== eggs by an unidentified assailant riding pillion. Lead actress Mindee Ong later suffered from an eye infection from the attack. No charges were pressed. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 