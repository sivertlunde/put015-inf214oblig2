Drunken Master III
 
 
{{Infobox film
| name           = Drunken Master III
| image          = DrunkenMasterIII.jpg
| alt            = 
| caption        = 
| film name = {{Film name| traditional    = 醉拳III
| simplified     = 醉拳III
| pinyin         = Zuì Quán Sān
| jyutping       = Zeoi3 Kyun4 Saam1}}
| director       = Lau Kar-leung
| producer       = Chris Lee
| writer         = Stanley Siu
| starring       = Andy Lau Michelle Reis Willie Chi Simon Yam Lau Kar-leung Adam Cheng Gordon Liu William Ho
| music          = Brother Hung
| cinematography = Peter Ngor
| editing        = Siu Nam
| studio         = Super Film Production
| distributor    = Modern Films and Entertainment
| released       =  
| runtime        = 91&nbsp;minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = HK$7,076,791
}}
Drunken Master III is a 1994 Hong Kong martial arts film directed by Lau Kar-leung and starring Andy Lau, Michelle Reis, Willie Chi, Simon Yam, Lau Kar-leung and Adam Cheng. This film was quickly produced after Director Lau and Jackie Chan fell out on the set of Drunken Master II with the style of action and Lau decided to produce a more authentic entry in the Drunken Master series.  Despite the title, Drunken Master III is not a sequel to the Drunken Master series and is widely considered an imitator.   

==Plot==
At the turn of the century of China, the White Lotus Society plots to put the sinister Manchu Emperor Yuan Shikai (William Ho) to become the Emperor of China. However, he needs to given a Jade Ring possessed by his fiance Princess Sum Yuk (Michelle Reis). The White Lotus Society gets Yeung Kwan (Andy Lau) to get the princess. However, Yeung is actually a rebel leader working for Sun Yat-sen and he abducts Sum Yuk and find refuge at the Po Chi Lam clinic owned by Wong Kei-ying (Adam Cheng) with his mischievous son Wong Fei-hung (Willie Chi). Later, Fei Hung gets involved with Yeung and Sum Yuk and end up on the run together. Along the way, Fei-hung later learns the secrets of Drunken Boxing from an old master Uncle Yan (Lau Kar-leung).

==Cast==
*Andy Lau as Yeung Kwan
*Michelle Reis as Princess Sum Yuk
*Willie Chi as Wong Fei-hung
*Simon Yam as Gay bus passenger
*Lau Kar-leung as Uncle Yan
*Adam Cheng as Wong Kei-ying
*Gordon Liu as Governor Lee
*William Ho as Yuan Shikai
*Jimmy Lau
*Woo Kin Keung
*Lee Kwok Man
*Daniel Chan
*Zhang Lei
*Brad Allan as Bus passenger
*Lau Heung Yeung
*Ma Lee Hung
*Luk Lan Fung
*Chow Man Wai
*To Man Hung
*Sze Sui Fan
*Fong Wai Chung
*Chou Jing
*Hon Man Kit
*Hau Chan Yu
*Luk Siu Pang
*Giorgio Pasotti
*Chow Man
*Chin Hiu Kwan
*Kwok Nai Ming
*Zhang Teng
*Suen Ka Wing
*Tsui Fai
*Cheung Yiu Man

==Box office==
The film grossed HK$7,076,791 in its theatrical from 2 July to 20 July 1994 in Hong Kong.

==See also==
*Andy Lau filmography

==References==
 

==External links==
* 
*  at Hong Kong Cinemagic
* 
*  at LoveHKFilm.com

 

 
 
 
 
 
 
 
 
 
 
 
 
 