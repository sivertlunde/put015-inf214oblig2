No. 2 (film)
 
 
 
{{Infobox film |
 name = No. 2 |
 image = |
 writer = Toa Fraser |
 starring = Ruby Dee Mia Blake Tuva Novotny|
 director = Toa Fraser |
 producer = Tim Bevan  Philippa Campbell |
 distributor = Miramax Films |
 released =  
 runtime = 94 mins |
 language = English |
 budget =
}} New Zealand Fijian playwright Frasers 2000 Play (theatre)|play.

==Plot== Fijian extended family living in a suburb of Auckland, New Zealand, feels that the heart and passion has gone out of her clan. One morning, she demands that her grown grandchildren put on a big family feast at which she will name her successor. The grandchildren&mdash;Soul, Charlene, Hibiscus, Erasmus, and her favorite, Tyson&mdash;reluctantly turn up, Tyson with his new Danish girlfriend, Maria. Family conflicts play out as the difficult day progresses, but in the end the grandchildren—and eventually Nannas children too—join with cousins and others in a traditional celebration.

==Production & Trivia==
The film was shot in the Auckland, New Zealand suburb of Mount Roskill during the latter part of summer 2005, with interior scenes of Nanna Marias house taking place on a soundstage at Henderson Valley Studios. Post Production was completed in Wellington, at Park Road Post.

Soon after the arrival of Ruby Dee in New Zealand, the film was put on hold after the news that her husband Ossie Davis had died in the U.S. Dee flew back to attend the funeral. However, before she left New Zealand, she vowed to return and complete the film. The crew were stood down for a 3 week period, and principal photography commenced at the beginning of March 2005.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Ruby Dee || Nanna Maria
|-
| Taungaroa Emile || Soul
|-
| Rene Naufahu || Erasmus
|-
| Tuva Novotny || Danish Maria 
|-
| Mia Blake || Charlene
|-
| Xavier Horan || Tyson
|-
| Miriama McDowell || Hibiscus
|-
| Nathaniel Lees || Uncle John
|-
| Tanea Heke || Aunty Cat
|-
| Pio Terei || Uncle Percy
|-
| Te Paki Cherrington || Father Francis
|-
| Antony Starr || Shelly
|}

==Awards==
* 2006      Grand Jury Prize 

==Soundtrack==
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 