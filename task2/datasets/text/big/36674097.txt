Pirates of Monterey
{{Infobox film name            = Pirates of Monterey image           = director        = Alfred L. Werker producer        = Paul Malvern writer          = Sam Hellman Margaret Buell Wilder based on Edward Lowe Bradford Ropes starring  Rod Cameron music           = Milton Rosen cinematography  = W. Howard Greene Harry Hallenberger Hal Mohr editing         = Russell F. Schoengarth studio          = Universal International distributor     = Universal International released        =   runtime         = 79 minutes country         = United States language        = English budget          =
}}
Pirates of Monterey is a 1947 film starring Maria Montez. It was the last movie she made for Universal. 

==Plot==
Wealthy aristocrat Marguerita Novarro and her maid are rescued by Phillip Kent when their carriage breaks loose. It is 1840 and Kent is transporting rifles from Mexico City to California to be used by soldiers there.
 Santa Barbara. Although she is wealthy and can pay, Kent says he will forego any remuneration from Marguerita in exchange for the first dance at a festival. As love blossoms, they continue north to Monterey with the caravan, where Kent is reunited with an old friend, Lt. Carlos Ortega, only to learn that Ortega is engaged to be married to Marguerita.

An attack by Spanish royalists leaves Ortega seriously injured. Manuel De Roja is taken prisoner by Kent and turned over to one of Ortegas men, but the officer in charge turns out to be Manuels own brother, Major De Roja.

Now in love, Marguerita and Kent try to leave Monterey together but are captured by De Rojas men. A jealous Ortega searches and is also taken captive, but after an escape, Kent kills De Roja in a battle with swords. Mexicos soldiers rout the royalists, and a grateful Ortega gives his blessings to Marguerita and Kent.

==Cast==
* Maria Montez as Marguerita Novarro Rod Cameron as Phillip Kent
* Gilbert Roland as Major De Roja
* Gale Sondergaard as Senorita De Sola
* Philip Reed as Lt. Carlos Ortega
* Michael Raffetto as Sgt. Gomara

==Production== Rod Cameron had impressed Universal with his performance opposite Yvonne de Carlo in Salome Where She Danced and the studio wanted to put him in a similar technicolor film with Maria Montez. Special Story to Star Finds Lorring and Dall
Schallert, Edwin. Los Angeles Times (1923-Current File)   02 Apr 1945: A2.   This was meant to be Frontier Gal but Montez refused to make the movie and went on suspension. De Carlo acted opposite Cameron instead. NEWS OF THE SCREEN: Fred MacMurray Set for Guerrilla in Philippines-- Loews Houses to Show Special Short V-E Day
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 Apr 1945: 32.   Yvonne De Carlo Stands Out Again as Threat Girl: Carroll Cutie Has Spotlight in Sarong Set Yvonne De Carlo Causes Concern in Sarong Circles
Schallert, Edwin. Los Angeles Times (1923-Current File)   03 June 1945: B1.  

Eventually Montez agreed to make this movie with Cameron, which was announced in April 1946. Earl Kenyon was originally meant to direct. MONTEZ, CAMERON TO CO-STAR IN FILM: Named for Leads in Pirates of Monterey at Universal, to Be Done in Technicolor
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   02 Apr 1946: 23.  

Mikhail Rasumny was borrowed from Paramount to appear in the film. Gail Russell Borrowed for Angel Portrayal
Schallert, Edwin. Los Angeles Times (1923-Current File)   11 Apr 1946: A2.  
==Reception==
The Chicago Tribune wrote that "With the exception of sprightly little Mikhail Rasumny, who contributes a bit of comedy occasionally, this film is dull business, peopled by elaborately costumed but expressionless characters. " Movie Gets All Dressed Up but Goes No Place: "PIRATES OF MONTEREY" THE CAST
Tinee, Mae. Chicago Daily Tribune (1923-1963)   19 Nov 1947: 33. 

The Los Angeles Times said the film "doesnt, by any stretch of the imagination, class as an "epic", but it is beautifully photographed in Technicolor and contains enough fightin, feudin, and fussin to satisfy fans of shoot-em-up cinema fare." Pirates of Monterey Old California Thriller
Scott, John L. Los Angeles Times (1923-Current File)   17 Dec 1947: A9.  

The New York Times claimed "the script for this rawhide romance was obviously ground out mechanically by an old typewriter on the Universal lot, set for 200 pages, while the writers played gin or slept. And the gent who is credited as director—Alfred Werker, who also has been around—plainly fulfilled his assignment in the same tired, mechanical way." 

The Christian Science Monitor said "only the very young will be able to excite themselves about" the film. Pirates of Monterey
M.M.. The Christian Science Monitor (1908-Current file)   16 Jan 1948: 4.  

Universal were reportedly so pleased with Philip Reeds performance they offered him a seven year contract at one film a year. DRAMA AND FILM: Zanuck Accumulating New Italian Sparklers
Schallert, Edwin. Los Angeles Times (1923-Current File)   25 Dec 1947: A11.  
== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 

 