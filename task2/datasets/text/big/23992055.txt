The Summer Guest
 
{{Infobox film
| name           = The Summer Guest
| image          = 
| caption        = 
| director       = Can Togay
| producer       = 
| writer         = Can Togay Edit Kőszegi
| starring       = Géza Balkay
| music          = 
| cinematography = Tamás Sas
| editing        = Ágnes Ostoros
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Hungary
| language       = Hungarian
| budget         = 
}}

The Summer Guest ( ) is a 1992 Hungarian drama film directed by Can Togay. It was screened in the Un Certain Regard section at the 1992 Cannes Film Festival.   

==Cast==
* Géza Balkay - A nyaraló
* Mari Törőcsik - A nyaraló anyja
* Marta Klubowicz - Fiatal nő
* Adél Kováts - Fiatal nő (voice)
* Juli Básti - Kocsmárosnő
* József Madaras - A kocsmárosnő férje
* Miklós Székely B. - Rendőr
* Gábor Reviczky - Sofőr
* Gábor Ferenczi
* Endre Kátay
* László Csurka
* Károly György
* János Ács
* Csaba Magyar
* Árpád Babay
* Alexandra Nagy

==References==
 

==External links==
* 

 
 
 
 
 
 
 