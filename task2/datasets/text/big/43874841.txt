Rookies in Burma
{{Infobox film
| name           = Rookies in Burma
| image          = 
| alt            =
| caption        = 
| film name      =  James Casey (assistant)
| producer       = Bert Gilroy Edward James
| screenplay     = 
| story          = 
| based on       =  
| starring       = Wally Brown Alan Carney Erford Gage Joan Barclay
| narrator       = 
| music          = Constantin Bakaleinikoff|C. Bakaleinikoff
| cinematography = Harry J. Wild
| editing        = Harry Marker RKO Radio Pictures
| distributor    = 
| released       =   }}
| runtime        = 62 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Rookies in Burma is a 1943 American comedy film directed by Leslie Goodwins from an original screenplay by Edward James. Produced and distributed by RKO Radio Pictures, it was released on December 7, 1943, being a sequel to the earlier 1943 film, Adventures of a Rookie. Bert Gilroy, who had been a producer at RKO since 1938, would leave the studio after completing this film.  He would produce only one other film in his career, Hollywood Bound, in 1946 for Astor Pictures. This film would also mark the last film in which the actor Erford Gage would perform. After the film wrapped, Gage reported for duty in the US Army.  He would die in March 1945 in the Philippines, as a result of wounds suffered in action.   As in the earlier film, this picture stars the comedy duo of Wally Brown and Alan Carney.

==References==
 

 
 
 
 
 
 
 
 
 


 