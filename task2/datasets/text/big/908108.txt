Kleine Freiheit
{{Infobox film
| name = Kleine Freiheit
| image = Kleine Freiheit poster.jpg
| caption = Region 2 DVD cover
| director = Yüksel Yavuz
| producer = Tobias Büchner, Ralph E. Cotta, Peter Stockhaus, Claudia Tronnier
| writer = Henner Winckler Yüksel Yavuz
| starring = Cagdas Bozkurt, Necmettin Çobanoglu, Leroy Delmar
| music = Ali Ecber
| cinematography = Patrick Orth
| editing = Lars Späth
| distributor =
| released = 2003
| runtime = 100 min.
| country = Germany German
| budget =
| preceded_by =
| followed_by =
}}
 Kurdish director Yüksel Yavuz about the friendship (and later relationship) between two teenage boys who are illegal immigrants in Germany.

== Plot ==
 Turkish militia. Now that Baran is 16, he is no longer allowed to stay in Germany and faces the bleak prospect of getting deported back.

Baran meets Chernor (Leroy Delmar), an African boy who has the same problem and trafficks drugs to make some money. Chernor is openly gay and their friendship has sexual overtones from the beginning. Baran also has very little interest in girls, even though a marriage might solve his immigration-related legal trouble.

Things get even more complicated when Baran spots the traitor of his family and wants to kill him. However, the man pleads for his life and Baran spares him. After this act of forgiveness, Cherno and Baran have sex together for the first time, making it clear to Baran that his interest in Chernor goes beyond friendship.  Finally, both Cherno and Baran, who had made a desperate attempt to free Cherno, are arrested by the police.

== Critical reaction ==
The movie was critically well-received, particularly because of its accurate depiction of the Turkish&ndash;Kurdish conflict, its handling of the characters sexuality (three of which are gay), and the acting prowess of the nonprofessional actors. Cagdas Bozkurt won an acting prize at the Ankara film festival, while the movie won a viewers choice award in Istanbul.

Kleine Freiheit, the German title of the movie, ("Little Freedom", translated literally) is a wordplay on Große Freiheit (literally "Great Freedom"), the rather famous name of a street in the red light St. Pauli district where the plot is set.

==External links==
* 

 
 
 
 
 
 


 
 