Red Obsession
{{Infobox film
| name           = Red Obsession
| image          = Red-Obsession-poster.jpg
| border         = Yes
| caption        = Film poster
| director       = David Roach Warwick Ross
| producer       = Warwick Ross
| writer         = David Roach Warwick Ross
| narrator       = Russel Crowe
| music          = Burkhard Dallwitz Amanda Brown
| cinematography = Steve Arnold Lee Pulbrook
| editing        = Paul Murphy
| studio         = Lion Rock Films
| released       = 
| runtime        = 75 minutes
| country        = 
| language       = English
| budget         = 
| gross          = 
}}
Red Obsession is a 2013 Australian documentary film which collects interviews with winemakers and wine lovers across the world. The film is narrated by Russell Crowe. Red Obsession was co-directed and co-written by David Roach and Warwick Ross.   

==Plot== wine industry are also examined, with occasional narration provided by Russell Crowe. 

==Production==
Principal photography began in April 2011.  The film was co-directed and co-written by David Roach and Warwick Ross. Ross was in charge of production for Lion Rock Films, while the soundtrack was composed by Burkhard Dallwitz and Amanda Brown. Paul Murphy signed on as editor. 

==Release==

===Critical response=== Daily News The Sunday Time Out New York offered that " he reliance on talking-head testimonials leaves a weak aftertaste, but its a palate-pleasing provocation nonetheless".  Doris Toumarkine of Film Journal International lauded the films "superb" cinematography.  Kenji Fujishima of Slant Magazine awarded the film three out of four stars, commenting that it "may well inspire one to a higher appreciation of wine while also bringing about a greater awareness of the market forces that turn even the highest art into commodities".  Ronnie Scheib of Variety (magazine)|Variety opined that " he films rather simplistic cultural juxtapositions, pitting artistic appreciators against status-seeking philistines, work best when narrowly focused on the subject of wine." 

===Awards & nominations===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards|AACTA Award  (3rd AACTA Awards|3rd)  AACTA Award Best Feature Length Documentary Warwick Ross
| 
|- AACTA Award Best Direction in a Documentary
| 
|- David Roach
| 
|- Best Cinematography in a Documentary Steve Arnold
| 
|- Lee Pulbrook
| 
|- Best Sound in a Documentary Burkhard Dallwitz
| 
|- Amanda Brown
| 
|- Liam Egan
| 
|- Andrew Neil
| 
|-
|}

==See also==
* List of 2013 box office number-one films in Australia

==References==
 

==External links==
*  
*  
*  

 
 
 
 