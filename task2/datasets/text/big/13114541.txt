Tom et Lola
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Tom et Lola
| image          = Tom-et-Lola.jpg
| alt            = Tom et Lola Theatre Poster
| caption        = Theatrical release poster
| director       = Bertrand Arthuys
| producer       = Alain Belmondo Gerard Crosnier
| writer         = Bertrand Arthuys Christian Chalong Muriel Téodori
| based on       =
| starring       = Neil Stubbs  Mélodie Collin Cecile Magnet Marc Berman Catherine Frot
| music          = Christophe Arthuys Jean-Pierre Fouquey 
| cinematography = François Catonné 
| editing        = Jeanne Kef
| distributor    = Award Films International
| released       =  
| runtime        = 97 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}

Tom et Lola (English: Tom and Lola) is a French film about two ten-year-olds who have always lived isolated in sterile, hermetically sealed plastic bubbles due to an (unspecified) immune-deficiency disease. However, neither their environment or the hospital technicians can keep down the indomitable spirit of these children. Nor can anything stop their quest to one day run truly free, unbound by the medical shackles that bind them.

== Plot ==
Tom and Lola are a young boy and girl who are victims of a rare disease which means that they are prevented from any contact with the outside world. They are kept inside plastic bubbles in a Paris hospital. In this aseptic and strictly controlled microcosm run by doctors especially chosen for the task, the children are left with almost no external stimuli, other than an old black and white TV set tuned to educational films about subjects such as whale song, trains, and a show called The Adventures of Tikira in Alaska. Toms mother is in constant attendance, but her increasingly desperate attempts to beguile him have alienated her almost totally from them, and apart from Hélene, a member of the medical team who has been their nearest thing to a parent, Tom and Lolas only close attachment is to each other.

Eventually, the two children discover that by using the gauntlets that form part of their plastic pods, they are able to reach out and grasp each others hands. One night, Tom discovers that if he pulls Lolas arms hard enough and she pulls his, they can draw their bubbles close enough for him to reach the external emergency deflate controls on her bubble. After he has activated these, Lola escapes her bubble and releases Tom from his.
 medication drip line pulls out. The pair save him, and get him back to his room.

During another visit, Robert tells Tom and Lola that he is originally from Izouard, a small skiing village in the French Alps and shows them plans for escaping the hospital in order to return home. Tom and Lola agree this is a good idea and all three plan to escape the next night. However, when they arrive at his room the following night, Robert has already left and (they later discover) has come to grief while climbing over the hospital wall. Undeterred, Tom and Lola decide to escape anyway, having secured the address of Hélene.

Escaping the hospital, the two find their way across the Seine to a railway shunting yard in search of the train to Izouard, and climb aboard a passenger carriage where they fall asleep. They are discovered and taken to Hélenes home by a railway worker, having found on them a card with the address.

When Tom and Lola arrive at her house, her children are at home, alone as usual, as their mother spends more time with Tom and Lola at her work than with her own kids. A mutual regard and understanding develops between them, and together they contrive to set up a home situation more like the environment at the hospital, in the hope of regaining Hélenes attention. These hopes are dashed when she arrives home: her priority clearly remains the wellbeing of the escapees, and when her oldest son overhears her promise to the runaways that she will shelter them, he is disillusioned and calls the authorities.

The inevitable consequence in these circumstances is that the children will die, and the movie does not pull back from that, but treats the prospect in an allegorical, "magical realist" way. As the medical staff had planned in any case to separate the children without regard for their attachment - something they discovered only in the process of escaping - their pronostic had always been bleak.

== Synopsis == libertarian perspective for children where false morality has no place. Arthuys has successfully avoided the mire of existentialist melodrama common with childrens characters in situations of illness, and in doing so he manages a difficult task: to captivate the audience so that the movie is more like a carefree adventure.

Arthuys does not spare his characters natural exposure on camera … the nudity of the two children for roughly half the duration of the film could have led to oblivion for this movie. But it is all intrinsically subservient to his strong desire to get his message across. Neither do their tender physical expressions of mutual affection ever transgress, in degree or nature, what would reasonably be considered age-appropriate .

Lola and Tom do engage in gritty and sharply witty dialogue with the scientific team each morning, but this is largely to relieve the daily tedium.

Another striking motif is the repeated reference to an idealised heaven conceived by the two with the code phrase Iceberg-Alaska-Tikira, a whispered mantra as a sign of celebration or connection, accompanied with expressive signing gestures. This is later supplemented or supplanted by "Izouard!" as an aspirational symbol.

Their interaction with the adult world, populated by imbeciles who occasionally play ridiculous roles to entertain the pair from outside the bubbles, is clearly disproportionate, as if the children were a few light years ahead of the common sense practised within the French hospital.

With the moralistic and sometimes hysterical persecution of artistic licence observed since the late 1980s, this film gained only a narrow VHS distribution, with regard to the fiercely competitive category of collectors items. A great example of avant-garde cinema, victimised by the approach.

The aesthetics of scenarios in the laboratory and the excessive use of white, is similar to THX 1138 by George Lucas, a kind of paradigm in the science fiction of the 1970s. This neatly evokes and reinforces the chosen environments on which the two children fixate, whose snowy and frozen purity presumably promises a freedom from the bacterial threats which enforce their captivity in civilisation.

==References==
 

==External links==
*  

 
 