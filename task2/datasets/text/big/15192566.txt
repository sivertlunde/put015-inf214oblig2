The Bearcat
 
{{Infobox film
| name           = The Bearcat
| image          = Poster of the movie The Bearcat.jpg
| caption        = Film poster
| director       = Edward Sedgwick
| producer       = 
| writer         = Frank R. Buckley George Hively
| starring       = Hoot Gibson
| cinematography = Charles E. Kaufman
| editing        = 
| distributor    = 
| released       =  
| runtime        = 50 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 Western film directed by Edward Sedgwick and featuring Hoot Gibson.   

==Cast==
* Hoot Gibson as The Singin Kid
* Lillian Rich as Alys May
* Charles K. French as Sheriff Bill Garfield (credited as Charles French)
* Joe Harris as Doc Henderson
* Alfred Hollingsworth as John P. May Harold Goodwin as Peter May
* William Buckley as Archer Aitken
* Fontaine La Rue as Mary Lang
* James Alamo as Henry
* J.J. Allen as Jake Hensen
* Stanley Fitz as Cut Face
* Joe De La Cruz as One Eye
* Sam Polo as Pining Willis

==See also==
* List of American films of 1922
* Hoot Gibson filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 