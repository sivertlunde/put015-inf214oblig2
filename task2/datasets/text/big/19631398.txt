Rock Odyssey
{{Infobox film
| name = Rock Odyssey Robert Taylor (uncredited)
| producer = Joseph Barbera William Hanna
| writer = Robert Taylor Joseph Barbera Neal Barbera
| voices = Scatman Crothers
| music = Hoyt Curtin
| cinematography =
| editing = Warner E. Leighton Margaret Webb
| studio = Hanna-Barbera Productions
| distributor = Warner Bros. (TV)
| released = 1987
| runtime = 120 minutes
| country = United States
| language = English
| budget =
| gross =
}}

Rock Odyssey is an animated feature film produced by Hanna-Barbera for a theatrical release in 1987. Robert Taylor (but credited to William Hanna and Joseph Barbera on the final print),  with storyboards by Pete Alvarado. 

==Plot==
The story of a mysterious woman named Laura, who embarks on a journey to find her true love. The movies soundtrack is set to four decades of classic rock. Scatman Crothers provides the voice of a living jukebox who narrates the story.

==Characters==
*The Jukebox:
*Laura (a.k.a. Peggy Sue):
*50s Love Interest:
*Billy Bob (60s Love Interest):
*70s Love Interest:
*True Love:

==Production notes== Taft Entertainment saw the films graphic imagery, particularly relating to the 1960s and the Vietnam War. (Subsequently, Robert Taylor left the studio, and H-B closed their feature animation unit, after the box-office failure of Heidis Song.) 

The program was shelved by Hanna-Barbera, who intended to retool the program; at a March 1983 Congressional hearing on childrens television, ABC childrens programming VP Squire Rushnell mentioned that it was slated to air that year.  However, the film remained on the shelf until the mid-80s, at which time a new sequence was added, featuring classic Hanna-Barbera cartoon clips set to "Wake Me Up Before You Go-Go". This sequence was intended to bring the film "up to date", since the rest of the soundtrack only covered songs up to 1980. Hanna-Barbera and Worldvision Enterprises made Rock Odyssey available for international television distribution in 1987, and the film is mentioned in trade advertisements from that year. (Ironically, this final completed and released version seems to retain all of the "offensive" scenes which had led H-B to shelve the project back in the early 80s.)

Rock Odyssey has not yet been aired on TV, or released on VHS, DVD or Blu-ray in the United States by Warner Home Video, however, it was screened at the Second Los Angeles International Animation Celebration in July 1987, and is available for viewing at the Library of Congress in Washington, D.C.. It has also aired on TV in Spain and several Latin American countries, with the narration dubbed into Spanish.
 Boomerang Southeast Asia.

==References==
 

==External links==
*  
*   at the Big Cartoon Database

 

 
 
 
 
 
 
 
 