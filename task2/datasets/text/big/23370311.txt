The Gun That Won the West
{{Infobox film
| name = The Gun That Won the West
| image = Tgtwtwpos.jpg
| caption = Original film poster
| director = William Castle
| producer = Sam Katzman
| writer = Robert E. Kent
| starring = Dennis Morgan Paula Raymond Richard Denning Robert Bice Don C. Harvey
| music = Mischa Bakaleinikoff George Duning
| cinematography = Henry Freulich Al Clark Richard Halsey
| distributor = Columbia Pictures
| released =  
| runtime = 71 min.
| country = United States
| language = English
| budget = Unknown
}} Western film starring Dennis Morgan, Paula Raymond, Richard Denning, Chris OBrien , Michael Morgan , Roy Gordon, Robert Bice, directed by William Castle and produced by Sam Katzman. The screenplay was written by Robert E. Kent. The original music score was composed by Irving Gertz.

==Plot==
Colonel Carrington (Gordon) and his command are assigned the job of constructing a chain of forts in the Sioux Indian territory of Wyoming during the 1880s. The Colonel recruits former cavalry soldiers turned frontier scouts Jim Bridger (Morgan) and "Dakota Jack" Gaines (Denning), now running a Wild West Show, to head the fort building.

Bridger and Gaines are friendly with Sioux chief Red Cloud (Bice) but have reservations about the chiefs 2nd in command, Afraid of Horses (Morgan). Both Bridger and Gaines are confident a peace treaty with the Sioux can be made. However, if war breaks out, the cavalry is depending on getting a new type of breech loading Springfield Model 1865 rifle. Gaines, Mrs. Gaines (Raymond), and Bridger arrive at the fort for the conference. Gaines gets drunk and attempts to intimidate the Indians into signing a treaty. Chief Red Fox threatens war if his territory is invaded by any troops building forts.

==Production== Buffalo Bill. 

==See also== Colt .45
*Winchester 73 Springfield Rifle

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 


 