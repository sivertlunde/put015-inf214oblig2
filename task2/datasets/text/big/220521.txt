Diamonds (1975 film)
 
{{Infobox film
| name           = Diamonds
| caption        = The theatrical poster.
| image	=	Diamonds FilmPoster.jpeg
| director       = Menahem Golan Arik Dichner
| producer       = Yoram Globus Menahem Golan
| writer         = Menahem Golan David Paulsen
| narrator       =  Robert Shaw Richard Roundtree Barbara Hershey Shelley Winters
| music          = Roy Budd Adam Greenberg
| editing        = Dov Hoenig AVCO Embassy Pictures Anchor Bay Entertainment
| released       = October 22, 1975 (U.S.)
| runtime        = 120 minutes
| country        = Israel United States
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Robert Shaw twin brothers. Richard Roundtree, Barbara Hershey and Shelley Winters are co-stars. The film was also released as Diamond Shaft, although it has no relation to the Shaft (1971 film)|Shaft films other than having Roundtree in the cast.

==Plot==
Charles Hodgson is a British aristocrat who decides to become a thief as a way of getting at his twin brother, Earl, a security expert who has built a supposedly impregnable vault in Tel Aviv, which holds a cache of diamonds. For the caper, Charles enlists Archie, a heist expert, and Sally. He also becomes acquainted with an American woman, Zelda Shapiro, who is in Israel looking for a new husband.

==Cast== Robert Shaw as Charles Hodgson/Earl Hodgson
* Richard Roundtree as Archie
* Barbara Hershey as Sally (credited as Barbara Seagull)
* Shelley Winters as Zelda Shapiro

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 