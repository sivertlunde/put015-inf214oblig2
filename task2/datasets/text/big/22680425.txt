On the Count of Zero (2007 film)
{{Infobox film
| name     = On the Count of Zero
| image          = SifirDedigimde.jpg 
| caption        = Teaser poster for On the Count of Zero 
| director       = Gokhan Yorgancigil 
| writer         = Gokhan Yorgancigil 
| starring       = Oktay Kaynarca  Hazim Körmükçü Damla Tokel Görkem Yeltan 
| producer       = Tarkan Atesmen  Melih Bulu  Mehmet Akif Malatyali  Sakir Sari  Gokhan Yorgancigil
| released   =  2 November 2007
| runtime        = 92 minutes Turkish 
| music = Volkan Topsakal
| cinematography = Dogan Sarigüzel
| editing        = Savas Dogan Gokhan Yorgancigil
}}
 Turkish film, written and directed by Gökhan Yorgancıgil. 

The film stars Oktay Kaynarca, Hazim Körmükçü and Damla Tokel as the main characters, but also includes the talents of Görkem Yeltan, Özge Özder, Semih Sergen and Riza Pekkutsal. The film tells the story of the young art student Aslı, through her desperate journey in the world of hypnosis in order to find the precious art book she has lost and her unexpected encounter with a mysterious stranger in the world of hypnosis. The plot thickens with the parallel story of a mother reading fairy-tales to her son which are depicted as 3D animation renderings of traditional Turkish shadow puppeteering art. The parallel stories entwine to lead the characters towards the mysterious Burgaz Island through a journey into the ancient capital of the East, İstanbul.

On the Count of Zero is the first Turkish film in the mystery / thriller genre. The film project was developed through the blog page Mahkum.net, which makes it the first film project to be developed online with the participation of Mahkum.nets followers on topics ranging from story ideas to casting decisions.

The film was screened in 4 festivals in the USA and won 2 Special Jury Remi Awards in the 42nd Annual WorldFest-Houston International Film Festival in the "Best First Feature" and "Theatrical Feature" contending for Grand REMI Award categories.

==Plot==
Young art student Asli has lost a precious miniature-art book, but has
no recollection of how it got misplaced. She and her medical student
friend Nevin, having lost all hope due to her unexplained temporary
amnesia, go through an intense hypnosis session under the supervision
of an expert psychiatrist. As the psychiatrist helps Asli relive the
day she lost the book, she unexpectedly comes across a mysterious
stranger in the hypnosis world.

A mystical eastern fairytale unfolds in both the world of hypnosis and
the real world, as the characters, led by an unseen hand, are
uncontrollably steered towards the mysterious Burgaz Island. Asli
enters the realms of traditional Turkish fairy-tales and the uncharted
world of hypnosis, through a journey into the ancient capital of the
East, Istanbul.

==Cast==
* Oktay Kaynarca - Oguz

* Hazim Körmükçü - Melih

* Damla Tokel - Asli

* Görkem Yeltan - Nevin

* Özge Özder - Young Müberra

* Semih Sergen - Müfit

* Özhan Carda - Talat

* Riza Pekkutsal - Istavridis

* Aykut Bilgin - Ibo

== Production Photos ==

 

==Film Festival Participation==
* 42nd Annual WorldFest-Houston International Film Festival - Houston, Texas, USA (April 17–26, 2009)
* 39th Annual USA Film Festival - Dallas, Texas, USA (April 29 - May 3, 2009)
* 15th Annual Austin Film Festival - Austin, Texas, USA (October 16–23, 2008)
* Florida State University Middle Eastern Film Festival 2008 - Tallahassee, Florida, USA (September 30 - October 9, 2008)

==Awards==
42nd Annual WorldFest-Houston International Film Festival - Houston, Texas, USA (April 17–26, 2009)
* "Best First Film" Special Juri Remi Award
* "Theatrical Feature" Special Juri Remi Award contending for Grand REMI Award
{|
|-
| Special Juri Remi Award]] -->
| Special Juri Remi Award]] -->
|}

== External links ==
*  
*  
* Composer Volkan Topsakals  

 
 
 
 
 