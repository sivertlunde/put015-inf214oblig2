Big Jake (film)
 
{{Infobox film
| name           = Big Jake
| image          = Big jake ver2.jpg
| caption        = The second version of the theatrical release poster.
| director       = George Sherman
| producer       = Michael Wayne
| screenplay     = Harry Julian Fink Rita M. Fink
| narrator       = George Fenneman
| starring       = John Wayne Richard Boone Maureen OHara Patrick Wayne Christopher Mitchum Héctor Veira
| music          = Elmer Bernstein
| cinematography = William H. Clothier
| editing        = Harry Gerstad
| studio         = Batjac Productions
| distributor    = Cinema Center Films through National General Pictures (1971, original) Paramount Pictures (2003, DVD, and 2011, Blu-Ray DVD)
| released       =  
| runtime        = 110 min.
| country        = United States
| language       = English
| budget         = $4.8 million
| gross          = $7,500,000 
}}
 Western film directed by George Sherman, written by Harry Julian Fink and Rita M. Fink, produced by Michael Wayne, edited by Harry Gerstad, starring John Wayne, Richard Boone and Maureen OHara, narrated by George Fenneman, and shot on location in Durango, Mexico.  Jim Davis, John Agar, Harry Carey, Jr., Ethan Wayne and Hank Worden.

Big Jake was released to generally favorable critical reviews but to a lukewarm box office performance.

== Plot ==
 Texas Rangers in hunting the gang. She replies that this will be "a harsh and unpleasant kind of business and will require an extremely harsh and unpleasant kind of person to see it through." In consequence, she sends for her estranged husband, the aging Jacob "Big Jake" McCandles, a near-legendary gunfighter who wanders the west with his Rough Collie, simply named Dog. 
 REO touring cars. They, however, are ambushed and their cars put out of action. Jake, preferring the old ways, has followed on horseback, accompanied by an old Apache associate, Sam Sharpnose. He is now joined by his sons, Michael and James, with whom his relations are tense because of his desertion of the family ten years before.

That night, Fain rides into their camp to make arrangements for the handover, telling Jake that they will “send the boys body back in a basket" if anything should go wrong. Both men deny any personal stake in the business, each claiming to be "just a messenger boy". The family party crosses into Mexico the next day and checks into a hotel. Knowing that they have been followed by another gang intent on stealing the strongbox, Jake sets a trap for them and they are all killed. During the attack, the chest is blasted open, revealing clipped bundles of newspaper instead of money. Michael and James become suspicious of Jake and the three slug it out, but Jake assures them that it was both his and Marthas idea. James fears for Little Jakes life, but Jake tells them theyll have to go in anyway. 

A thunderstorm breaks and Pop Dawson, one of the outlaws, arrives to give them the details of the exchange. He warns them that a sniper will have a gun trained on the boy in case of a double-cross. Jake arranges for Michael to follow after them to take care of the sharp shooter and convinces Dawson that he had been killed in the fight. At the hideout, Jake is led in alone to where Fain and four other gang members are waiting, one of them holding a shotgun on the boy. Jake tosses the key of the chest to Fain, who opens it up to discover that he has been tricked. Ten minutes before the end, the final gunfight will last eight supenseful minutes.

Fain orders his brother Will to kill the boy but he is shot by Jake. Dog is wounded by the sniper and Jake is wounded in the leg before Michael kills him. Jake tells the boy to escape but Little Jake is hunted by the machete wielding John Goodfellow, who has already hacked Sam to death. Dog comes to the rescue and is himself killed before Big Jake arrives and impales Goodfellow on a pitchfork. Fain rides up and is preparing to finish off the two of them when Michael arrives from where he had been waiting in ambush and blasts him off his horse. Before he dies, Fain asks, "Who are you?" When Jake answers, "Jacob McCandles," Fain says, "I thought you was dead," as have other characters during the course of the film. “Not hardly," Jake replies.

With Little Jake rescued, and the broken family bonded, they prepare to head home.

==Main cast==
 
* John Wayne as Jacob McCandles
* Richard Boone as John Fain (Leader of Fains Gang)
* Maureen OHara as Martha McCandles
* Patrick Wayne as James McCandles 
* Christopher Mitchum as Michael McCandles 
* Bruce Cabot as Sam Sharpnose 
* Bobby Vinton as Jeff McCandles 
* Glenn Corbett as OBrien, aka Breed (John Fains Gang) 
* John Doucette as Texas Ranger Capt. Buck Duggan  Jim Davis as Head of lynching party 
* John Agar as Bert Ryan 
* Harry Carey, Jr. as Pop Dawson (as Harry Cary, Jr.) (John Fains Gang)
* Gregg Palmer as John Goodfellow (John Fains Gang)
* Jim Burk as Trooper (John Fains Gang)
* Dean Smith as James William "Kid" Duffy (John Fains Gang)
* Robert Warner as Will Fain (John Fains Brother, John Fains Gang)
* Jeff Wingfield as Billy Devries (John Fains Gang)
* Everett Creach as Walt Devries (John Fains Gang)
* Roy Jenson as Gunman at bathhouse in Escondero 
* Virginia Capers as Delilah 
* Hank Worden as Hank
* Ethan Wayne as Little Jake McCandles William Walker as Moses Brown
* George Fenneman as Narrator
* Tom Hennesy as Mr. Sweet
* Chuck Roberson as Texas Ranger
 

==Production==
  and Richard Boone at the films premiere at John Wayne Theatre at Knotts Berry Farm in 1971]]
Written as The Million Dollar Kidnapping, which was used as the shooting title, it was filmed from early October to early December, 1970, in the Mexican states of Durango and Zacatecas,  including scenes shot at the El Saltito waterfall and Sierra De Organos (in the municipality of Sombrerete, Zacatecas). 

John Waynes real-life son, Patrick Wayne, portrays James McCandles in the film, while Robert Mitchums son, Christopher Mitchum plays Michael McCandles.  Waynes youngest son Ethan Wayne is seen as his grandson, Little Jake, in the movie.

Two collies, trained by Bob and  ) for the stunts. Eventually Laddie won a PATSY award for his performance in Big Jake in 1971. The wooden base with a tile and a small brass placard read "Award of Excellence - 1971 / Laddie (Dog) / Bobby Weatherwax, Owner-Trainer / Big Jake". "Streaks and tip" from an aerosol can were used to make their coats darker. 

The couple who wrote the screenplay for the film, Harry Julian Fink and Rita M. Fink, also wrote the original script for Dirty Harry, which was also released in 1971. They also wrote Waynes later film, Cahill US Marshal.

This was the last of five films in which John Wayne and Maureen OHara appeared together. The previous four were  Rio Grande (1950)
*The Quiet Man (1952)
*The Wings of Eagles (1957)
*McLintock! (1963)

Theme music was composed by famed composer Elmer Bernstein.

Director George Sherman and John Wayne were friends from their days in the 1930s when Sherman directed him in several westerns at Republic Pictures. Sherman was 63 years old at the time this film was shot and not in the best of health. Parts of the picture were shot in remote locations in Mexico, and when Shermans health prevented him from going on location, Wayne directed the shooting himself. However, although he did direct enough of the picture to be listed as co-director, Wayne insisted that only Shermans name be listed in the credits as director.

==Reception== 26th highest grossing film of 1971.

==DVD==
Big Jake was released to DVD by Paramount Home Entertainment on April 29th, 2003 as a Region 1 widescreen DVD and on May 31st, 2011 as a Region 1 widescreen Blu-Ray DVD.

==See also==
*John Wayne filmography

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 