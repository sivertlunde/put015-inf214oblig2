Pour la suite du monde
 
{{Infobox film
| name           = Pour la suite du monde
| image          = suite0.jpg
| caption        = Title card
| director       = Michel Brault Marcel Carrière Pierre Perrault
| producer       = Fernand Dansereau
| writer         = Michel Brault Pierre Perrault Stanley Jackson
| starring       = 
| music          = 
| cinematography = Michel Brault Bernard Gosselin
| editing        = Werner Nold
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 105 minutes
| country        = Canada
| language       = French
| budget         = 
}}
Pour la suite du monde is a 1963 Canadian documentary film directed by Michel Brault, Marcel Carrière and Pierre Perrault. It was entered into the 1963 Cannes Film Festival.   

A work of ethnofiction, the film follows residents of Île aux Coudres, an island in the Saint Lawrence River in Quebec, as they agree to re-enact their traditional Beluga whale hunt for the filmmakers, one last time.

The film features local residents Léopold Tremblay, Alexis Tremblay, Abel Harvey, Louis Harvey and Joachim Harvey.

The film is considered a milestone work in the history of Direct Cinema.  It received a special award and was named film of the year at the 1964 Canadian Film Awards.    In 1984 the Toronto International Film Festival ranked the film eighth in the Top 10 Canadian Films of All Time. 

==Alternate English versions and titles==
The film has been screened in various versions and with no less than four English-language titles. At its 1963 Cannes premiere, it was billed as For Those Who Will Follow.  The NFB has also promoted the film in English as Of Whales, the Moon and Men   or The Moontrap,    depending upon whether it was the 105 minute or 84 minute version, respectively. The release of a 2007 "Île-aux-Coudres Trilogy" DVD trilogy also translates the film title as For the Ones to Come.   

The film is commonly referred to as simply Pour la suite du monde in both French and English.      

==See also==
* Docufiction
* List of docufiction films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 