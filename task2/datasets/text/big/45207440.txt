Emaan
{{Infobox film
| name           = Emaan
| director       = Haris Yosufi
| producer       = Haris Yosufi Popal Yosufi Qudratullah Yosufi
| writer         = Afghan Jan
| starring       = Rasul Iman Tamana Amini Ghafar Qutbyar
| music          = Mohib Hamidi Ahmad Fanoos
| cinematography = Haroon Azizi
| editing        = Mohib Hamidi
| released       =  
| runtime        = 81 minutes
| country        = Afghanistan
| language       = Dari language
| budget         = $100,000 (estimated)}}
 action Romance romance Drama film directed and produced by Haris Yosufi and starring Rasul Iman, Tamana Amini and Ghafar Qutbyar. After much await and public demand, Emaan film was finally screened at Reading Cinemas in Australia. This is the first time an Afghan Film to be screened at a Cinema. Emaan - the winner of 2011 South Asian Film Festival (Canberra Australia) for Best Story and Best Film, where films like 3 Idiots of India was also nominated and shown. The films director and Producer Haris Yosufi resident of Australia he travelled to Afghanistan and spent six months shooting his film. He spent a further Five months in Australia for the post-production work. the film was released on 27 February 2011 in Afghanistan, You can visit Emaan official website http://www.emaanthefilm.com

==Plot==
The film is based around the life of Emaan, a young, honest policeman who doesnt mind bending the rules to uphold justice. The story begins with the police investigating the rape and murder of a girl and soon after arresting the culprit. Emaans character, played by Rasool Emaan, is portrayed by the first sequence as a lonely, hardened man.

The lonely Emaan meets and instantly takes a liking to a young woman who has been married against her wishes to an older man, who then deserted her along with their small daughter...

==Cast==
* Rasul Iman as Emaan
* Tamana Amini as Freshta
* Ghafar Qutbayar as Parwez Khan
* Roientan Enkesar as Afzali
* Abida Frotan as Mom
* Basir Tahiri as Haji Samey

==Music==
The film music was composed by Mohib Hamidi and Ahmad Fanoos.

==See also==
*Cinema of Afghanistan

==External links==
*  

 
 
 
 
 
 
 
 


 