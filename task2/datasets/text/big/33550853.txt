The Ice House (film)
{{Infobox film
| name           = The Ice House
| image          = The-ice-house-movie-poster-1969-1020695540.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Stuart E. McGowan
| producer       = Dorrell McGowan
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Jim Davis Sabrina
| music          = Gene Kauer Douglas Lackey
| cinematography = William G. Troiano 
| editing        = Irwin Cadden
| studio         = C-B Productions
| distributor    = Orbit Media Group, USA, 1969 Marden Films, Canada, 1972 Something Weird Video, VHS, 1996 Grindhouse Releasing Directors cut, 2008 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American horror thriller film Jim Davis, Sabrina in one of her last film roles.  

==Plot==
Ric Martin (Robert Story), a disgraced and long-fired cop, hits on Ice House dancer Venus De Marco (Sabrina (actress)|Sabrina) and is struck with a beer bottle for his efforts. Angered, he stalks the dancer, and when she again raises a bottle in a defensive manner, he strangles her. He is thwarted in efforts to hide the body at a local lovers lane, and ends up hiding it at The Ice House, where he works in the menial position of attendant. Other women become his victims and their bodies are stored there as well. His identical twin brother Fred Martin (David Story), himself a cop and investigating the disappearances, cannot understand why his brother is acting oddly. In trying to slow down the hunt for the serial killer, Ric kills Fred and takes his place in the investigation.

==Cast==
* David Story as Fred Martin
* Robert Story as Ric Martin Jim Davis as Jake
* Scott Brady as Lt. Scott
* Nancy Dow as Jan Wilson Sabrina as Venus De Marco John Holmes as Dancer
* Karen Lee as Unknown
* Ken Osborn as Killer
* Kelly Ross as Kandy Kane

==Production== bombshell Jayne Sabrina who was thrilled at the chance to play the role; therefore, she accepted. A fully restored directors cut of the film was released on video in 2008 by Grindhouse Releasing|Grindhouse.

==Release== Marden Films gave the film theatrical release in Canada in 1972. The film was released on VHS in the USA by Something Weird Video in 1996 as part of Frank Henenlotters Sexy Shockers from the Vaults (Vol. 60), and a fully restored directors cut had worldwide release in 2008 by Grindhouse Releasing. The film is also known as Cold Blood, Crimen on the Rocks (Spain), Love in Cold Blood, and The Passion Pit.

==Critical response== Video Watchdog Magazine, wrote "Character actors Scott Brady, Jim Davis and Tris Coffin, and a pair of musclebound, thespically challenged leading men are the main points of interest in this thriller/softcore hybrid, which delivers little more than copious nudity." He panned the film for the poor directing of Stuart E. McGowan, and notes that while the film set up the viewer for mystery and horror, it failed to deliver and meandered to a predictable twist ending. He also panned the performances of real-life twins David and Robert Story as "incredibly stiff", and made note that "some amusingly unhip slang" and an undramatic "ridiculous" and "undercranked" motorcycle chase provided only "intermittent entertainment".  While noting Grindhouse Releasings intent to remarket the film, they spoke toward Something Weird Videos 1996 video release, and noted that although SWVs 35mm source material was "damaged in every way imaginable", its color and resolution were still decent.   

==References==
 

==External links==
* 

 
 
 
 