Net Worth (2000 film)
{{Infobox Film
| name           = Net Worth
| image          = 
| image_size     = 
| caption        = 
| director       = Kenny Griswold Michael Baker   Cerise Fukuji   Bill Kerig   Kris Wiseman McIntyre
| writer         = Kenny Griswold   Bill Kerig
| narrator       = 
| starring       = Todd Field   Craig Sheffer   Daniel Baldwin Ernie Garrett David Lawrence
| cinematography = Yoram Astrakhan
| editing        = Sean Hubbert
| distributor    = A-Mark Entertainment
| released       = 
| runtime        = 94 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} 2000 drama film that starred Todd Field, Craig Sheffer, Daniel Baldwin,  Michael T. Weiss, Tara Wood, Ernie Garrett, and Alix Koromzay. It was directed by Kenny Griswold from a script written by Kenny Griswold and Bill Kerig.

==Plot summary==

About 4 competitive friends agree to a bet: they will all go to a city where none of them know anybody, with only $100 in their pockets. As the winner will be the person who has the greatest net worth at the end of 30 days. Despite all having a different philosophy about work and wealth they all believe they will win the bet.

== External links ==
* 

 
 


 