Art Museum by the Zoo
{{Infobox film
| name           = Art Museum by the Zoo
| image          = Art Museum by the Zoo film poster.jpg
| caption        = Theatrical poster
| director       = Lee Jeong-hyang
| producer       = Lee Choon-yeon
| writer         = Lee Jeong-hyang
| starring       = Shim Eun-ha Lee Sung-jae
| music          = Kim Yang-hui
| cinematography = Jo Yong-gyu
| editing        = Kim Sang-beom
| distributor    = Cinema Service
| released       =  
| runtime        = 108 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}} 1998 South Korean film.

== Plot ==
On leave from the military, Cheol-soo arrives at his girlfriends apartment only to find it occupied by another woman, Choon-hee. After a few days he finds out that his girlfriend is now engaged to someone else, and having nowhere else to go he ends up staying with Choon-hee. At first the two struggle to get along, but before long Chul-soo discovers that she is writing a screenplay to enter into a competition, and they end up working on a story together based on their own experiences of love, titling it "Art Museum by the Zoo."

== Cast ==
* Shim Eun-ha ... Choon-hee
* Lee Sung-jae ... Cheol-soo
* Ahn Sung-ki ... In-gong
* Song Seon-mi ... Da-hye

== Reception ==
Art Museum by the Zoo was released in South Korea on December 19, 1998, drawing a then-impressive 412,472 viewers in Seoul alone, making it the fifth best-selling Korean film of 1998.  

In 1999, Shim Eun-ha won Best Actress at the Grand Bell Awards, and Lee Sung-jae swept Best New Actor awards at the Baeksang Arts Awards, the Chunsa Film Art Awards, the Grand Bell Awards and the Blue Dragon Film Awards.

Director Lee Jeong-hyang was praised for her sophisticated and detailed direction, and the film is now considered a classic of 1990s Korean cinema. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 
 