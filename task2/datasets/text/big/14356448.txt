The Captain from Köpenick (1956 film)
{{Infobox film
| name           = The Captain from Köpenick Der Hauptmann von Köpenick
| image          = The Captain From Koepenick - half sheet.jpg
| caption        = Theatrical release poster by Tom Jung
| director       = Helmut Käutner
| producer       = Gyula Trebitsch (producer)
| writer         = Helmut Käutner Carl Zuckmayer (play) Carl Zuckmayer (screenplay)
| starring       = Heinz Rühmann Hannelore Schroth Martin Held Erich Schellow
| music          = Bernhard Eichhorn
| cinematography = Albert Benitz
| editing        = Klaus Dudenhöfer
| studio         =
| distributor    = Real-Film GmbH Distributors Corporation of America (US)
| released       =   Ufa-Palast Cologne
| runtime        = 93 minutes
| country        = West Germany
| language       = German
| budget         =
| gross          =
}} The Captain of Köpenick by Carl Zuckmayer. The play was based on the true story of Wilhelm Voigt, a German impostor who masqueraded as a Prussian military officer in 1906 and became famous as the Captain of Köpenick.

== Cast ==
*Heinz Rühmann as Wilhelm Voigt
*Martin Held as Dr. Obermüller
*Hannelore Schroth as Mathilde Obermüller
*Willy A. Kleinau as Friedrich Hoprecht
*Leonard Steckel as Adolph Wormser
*Friedrich Domin as Jail Director
*Erich Schellow as Capt. von Schlettow
*Walter Giller as Willy Wormser
*Wolfgang Neuss as Kallenberg
*Bum Krüger as Schutzmann Kilian
*Joseph Offenbach as Wabschke
*Ilse Fürstenberg as Marie Hoöprechtr, Voigts Schwester
*Maria Sebaldt as Auguste Viktoria Wormser, seine Tochter
*Edith Hancke as Sick girl
*Ethel Reschke as Pleureusenmieze
*Siegfried Lowitz as Stadtkämmerer Rosenkranz
*Willi Rose as Police sergeant
*Willy Maertens as Prokurist Knell
*Kurt Fuß
*Karl Hellmer as Nowak
*Robert Meyn as Polizeipräsident von Jagow
*Otto Wernicke as Schuhmachermeister
*Ludwig Linkmann as Betrunkener Zivilist Wolfgang Müller
*Rudolf Fenner as Polizeioberwachtmeister in Potsdam
*Reinhard Kolldehoff as Drunken soldier
*Kurt Klopsch as Polizei-Inspektor von Köpenick
*Helmut Gmelin as Kürassier-Oberst
*Reinhold Nietschmann
*Jutta Zech
*Jochen Blume as Paß-Kommissar
*Peter Ahrweiler as Anstaltsgeistlicher
*Jochen Meyn as Von Schleinitz
*Werner Schumacher as 2.Gefreiter
*Joachim Hess as 1.Gefreiter
*Balduin Baas as Ostpreußischer Grenadier
*Peter Franz
*Joachim Wolff as 2.Bahnbeamter
*Erich Weiher as Dorfschulze
*Holger Hagen as Dr. Jellinek
*Eddi Thomalla as 1. Bahnbeamter

==Awards== Best Foreign Language Film   
* Bundesfilmpreis (Filmband in Gold) in the categories Best Actor, Best Director, Best Screenplay and Best Film Architecture
* Bundesfilmpreis (Filmband in Silber) in the category Best Film Promoting Democracy
* Bundesfilmpreis (Goldene Schale) in the category Best Feature Film Bambi in the categories Best Film and Most Commercially Successful Film
* Preis der deutschen Filmkritik
* Price of the Berlin Film Critics Association for Heinz Rühmann
* Special Merit recognition by the Filmbewertungsstelle Wiesbaden
* Screened at the Venice Film Festival, the Edinburgh International Film Festival and the San Francisco International Film Festival

==See also==
* Wilhelm Voigt
* The Captain from Köpenick (1931 film)
* The Captain from Köpenick (1941 film)
* List of submissions to the 29th Academy Awards for Best Foreign Language Film
* List of German submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
* 
* 
*  at    

 

 
 
 
 
 
 
 
 
 
 