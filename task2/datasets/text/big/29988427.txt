!Women Art Revolution
{{Infobox film
| name           = !Women Art Revolution
| image          = !Women Art Revolution (documentary film) poster art.jpg
| alt            = 
| caption        = 
| director       = Lynn Hershman Leeson
| producer       = Lynn Hershman Leeson Sarah Peter Kyle Stephan Alexandra Chowaniec Laura Blereau
| writer         = Lynn Hershman Leeson
| music          = Carrie Brownstein
| cinematography = 
| editing        = Lynn Hershman Leeson
| studio         = 
| distributor    = Zeitgeist Films
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
}}
!Women Art Revolution is a 2010 documentary film directed by Lynn Hershman Leeson and distributed by Zeitgeist Films. It was released theatrically in the United States on June 1, 2011.

==Synopsis==
!Women Art Revolution explores the "secret history" of feminist art, through conversations, observations, archival footage, and works of visionary artists, historians, curators and critics. Starting from its roots in 1960s antiwar and civil rights protests, the film details major developments in feminist art through the 1970s and explores how the tenacity and courage of these pioneering artists resulted in what is now widely regarded as the most significant art movement of the late 20th century.

For more than forty years, filmmaker Lynn Hershman Leeson (Teknolust, Strange Culture) has collected a plethora of interviews with her contemporaries—and shaped them into an intimate portrayal of their fight to break down barriers facing women both in the art world and society at large. With a score by Sleater-Kinney’s Carrie Brownstein, !W.A.R. features Miranda July, The Guerilla Girls, Yvonne Rainer, Judy Chicago, Marina Abramović, Yoko Ono, Cindy Sherman, Barbara Kruger, B. Ruby Rich, Ingrid Sischy, Carolee Schneemann, Miriam Schapiro and Marcia Tucker.

==Awards==
*2010: Official Selection at Toronto International Film Festival
*2011: Official Selection at Sundance Film Festival, New Frontier
*2011: Official Selection at Berlin International Film Festival

==Release==
!Women Art Revolution played at New Yorks IFC Center beginning June 1, 2011, before opening around the country.  

==Digital Archive==
A digital archive representing two decades of Hershmann Leesons interviews with artists and critics is available through the Stanford University Libraries collection, !W.A.R. Voices of a Movement. According to the collection website, Hershmann Leeson desired this repository to "be shared with as wide an audience as possible." 

== References ==
 

==External links==
*  
*  
*   on indieWire
*  

 
 

 
 
 
 
 
 
 
 
 


 