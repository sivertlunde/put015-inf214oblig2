Der rote Rausch
 
{{Infobox film
| name           = Der rote Rausch
| image          = 
| caption        = 
| director       = Wolfgang Schleif
| producer       = Ernst Müller
| writer         = Hellmut Andics Hans Ulrich Horster
| starring       = Klaus Kinski
| music          = 
| cinematography = Walter Partsch
| editing        = Paula Dvorak
| distributor    = 
| released       =  
| runtime        = 88 minutes
| country        = Germany 
| language       = German
| budget         = 
}}

Der rote Rausch is a 1962 German thriller film directed by Wolfgang Schleif and starring Klaus Kinski.

==Cast==
* Klaus Kinski as Martin
* Brigitte Grothum as Katrin
* Marina Petrova as Anna
* Sieghardt Rupp as Karl
* Dieter Borsche as Professor Lindner
* Jochen Brockmann as Vollbricht
* Hans Obonya as Klobner
* Elisabeth Terval as Theres
* Annemarie Berthe as Professor Lindners Assistantin
* Edd Stavjanik as Kriminalrat Berger
* Peter Machac as Franz
* Christine Ratej as Hanni
* Helmuth Silbergasser as Stephan Renate Schmidt as Verkäuferin
* Josef Krastel as Ladenbesitzer
* Herbert Fux as Lastwagenfahrer
* Walter Regelsberger as Verkehrspolizist

==External links==
* 

 
 
 
 
 
 
 


 
 