The Cassava Metaphor
 

{{Infobox film
| name           = The Cassava Metaphor
| image          = 
| alt            =  
| caption        = 
| director       = Lionel Meta 
| producer       = Ether Prod 
| writer         = 
| screenplay     = Lionel Meta 
| story          = 
| starring       = Ricky Tribord Mata Gabin Daniel Ndo
| music          = Olivier Bessaignet 
| cinematography = Christophe Larue 
| editing        = Fabien Wouters 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 15
| country        = Cameroon
| language       = 
| budget         = 
| gross          = 
}}
 2010 film.

== Synopsis ==
Dawn in Yaoundé. Coco, a twenty something Cameroonian is driving a pretty young woman in his taxi. On the way to the airport, he tries to chat her up, but her thoughts seem to be elsewhere. Wistful, she looks out at the streets of the town she is leaving.

== Awards ==
*2012 Luxor African Film Festival (Egypt)
-Special prize of Best first film
*2012 Festival CinéSud, Saint-Geordes-de-Didonne (France)
-Mention of the Jury to Mata Gabin
*2011 Cinamazonia, Guyana (France)
-Canal plus Prize
*2011 Cinemigrante, Buenos Aires (Argentina)
-Special Jury Prize
*2011 Festival Focus du Mée sur Seine (France)
-International Prize
*2011 Festival du court métrage d’Atakpamé (Togo)
-Grand Prix
-Professional Jury Prize
*2011 In the Palace international short film festival, Balchik (Bulgaria)
-Special mention to Mata Gabin
-Nominee for best film
*2011 Festival du film court de Voiron (France)
-Grand Prix
*2011 Fespaco (Burkina Faso)
-Special Jury Prize
-Hope prize
-Best emerging talent
*2011 Festival Etang d’Arts de Marseille (France)
-Best film
*2011 Festival du court métrage de Lussac (France)
-Best fiction
-Special Jury Prize to Ricky Tribord
*2011 Festival Armoricourt de Plestin-les-Grèves (France)
-Special Jury Prize
*2011 Douala audiovisual meetings (Cameroon)
-Totem d’Or
-Best short film
*2010 Festival international du film d’Amiens (France)
-Public prize
-Special Cinécourt prize
*2010 Festival du 1er court métrage de Pontault Combault (France)
-Special Apollo + Prize
*2010 Festival des cinémas d’Afrique du pays d’Apt (France)
-Special Jury Prize
*2010 Festi’Val d’Oise du Court (France)
-Best short film
*2010 Ecrans noirs de Yaoundé (Cameroon)
-Best short film
*2010 Festival international du court métrage d’Abidjan (Ivory Coast)
-Grand Prix Fica d’Or 
-Best actress 
-Best actor

== Festivals ==
*2010 International short film festival, Clermont-Ferrand (France)
*2010 Tampere film festival (Finland)
*2010 Rencontres Cinémaginaires d’Argelès sur mer (France)
*2010 Addis Ababa international short film festival (Ethiopia)
*2010 Norwegian short film festival (Norway)
*2010 Festival du cinéma euro-africain de N’djamena (Chad)
*2010 Durban International film festival (South Africa)
*2010 Palm Springs international shortfest (USA)
*2010 Zanzibar international film festival (Tanzania)
*2010 Warsaw film festival (Poland)
*2010 São Paulo international short film festival (Brazil)
*2010 Festival international du film d’Afrique et des Iles (La Réunion, France)
*2010 Festival Courts Courts de Tourtour (France)
*2010 Festival international du cinéma francophone en Acadie de Moncton (Canada)
*2010 Starz Denver film festival (USA) 
*2010 Africa in Motion, Edinburgh African Film festival (United Kingdom)
*2010 Chicago international film festival (USA)
*2010 Festival du court métrage de Limoges (France)
*2010 European short film festival of Cologne (Germany)
*2010 Festival Ecran Libre d’Aigues Mortes (France)
*2010 Festival Court c’est court, Cabrières d’Avignon (France)
*2011 Festival CinemAfrica, Stockholm (Sweden)
*2011 Festival Quintessence, Ouidah (Benin)
*2011 Festival du film court francophone, Vaux-en-Velin (France)
*2011 Festival international des programmes audiovisuels, Biarritz (France)                 
*2011 Festival Cinéma et Migrations, Agadir (Maroc)
*2011 Cinequest Film Festival, San Jose (USA)
*2011 Larissa Mediterranean Festival of New Directors (Greece) 
*2011 Festival du film francophone de Kalamazoo (USA)
*2011 Festival de Cine Africano de Tarifa (Spain)
*2011 Festival Vues d’Afrique de Montréal (Canada)
*2011 Gulf Film Festival (United Arab Emirates)
*2011 Festival international du cinéma et de laudiovisuel (Burundi)
*2011 Mulhouse Tous Courts (France)
*2011 Rencontres interrégionales du documentaire et du court métrage (Guyane)
*2011 International pan African film festival, Cannes (France)
*2011 Rencontres cinématographiques de Bejaïa (Algeria)
*2011 Seattle international film festival (USA)
*2011 Festival Autour du court, Nogent (France)
*2011 Maremetraggio film festival, Trieste (Italy)
*2011 Festival international du film vidéo de Vébron (France)
*2011 Guanajuato international film festival (Mexique)
*2011 Festival Cinémas d’Afrique, Lausanne (Swiss)
*2011 Favourites film festival, Berlin (Germany)
*2011 Festival international de Contis (France)
*2011 Film Africa, London (United Kingdom)
*2011 Rencontres du cinéma d’Afrique et des Iles, Mayotte (France)
*2011 Southern Appalachian international film festival (USA)
*2011 Festival Tambour Battant, Genève (Suisse)
*2012 Festival Image et Vie, Dakar (Sénégal)
*2012 Festival Femi de Guadeloupe (France)
*2012 Festival Polyglotte, Nord-sur-Erdre (France)
*2012 Festival L’Ombre d’un Court, Jouy-en-Josas (France)
*2012 Festival Regards d’Afrique en Allier (France)
*2012 Festival CineMigrante de Bogota (Colombia)

== References ==
 

 
 
 
 