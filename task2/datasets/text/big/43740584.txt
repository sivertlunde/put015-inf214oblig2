An American Girl: McKenna Shoots for the Stars
{{Infobox film
| name           = An American Girl:  McKenna Shoots for the Stars
| image          = AG McKenna DVD.jpg
| alt            =  
| caption        = 
| director       = Vince Marcello
| producer       = 
| screenplay     = Jessica OToole Amy Rardin
| based on       =  
| starring       = Jade Pettyjohn Ysa Penarejo Nia Vardalos Ian Ziering Cathy Rigby Kerris Dorsey
| music          = Timothy Michael Wynn
| cinematography = 
| editing        = 
| studio         = Universal Studios
| distributor    = 
| released       =   
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 American Girl series overall.

The film revolves around the life of McKenna Brooks, as she struggles to balance her time at school and in her career as a gymnast.  The screenplay was written by Jessica OToole and Amy Rardin. The movie was directed by Vince Marcello.

A free screening of the film was held at the Tysons Corner Marriott on July 21, 2012,    as part of a promotional package by American Girl and Marriott.

==Plot==
In the Shooting Star gym. McKenna Brooks is practicing her back handspring on the beam. Coach Isabelle Manning tells McKenna that the move shes doing is too advanced. McKenna tries to protest, but loses. Then the coach tells her to just stretch. As McKennas feeling depressed, her best friend Toulane Thomas comes over and tells McKenna she was great. She then gets on about how the 2 of them will get into the 2016 Summer Olympics. Toulane explains that shell be on the podium with her gold, and McKenna next to her with her silver. McKenna tells her that shell get a gold instead. The 2 get back to gymnastics until McKennas grandma comes into the gym with McKennas younger twin sisters, Maisey and Mara. The 4 of them leave the gym.

At home, McKenna, Maisey, Mara, Mr. and Mrs. Brooks are at the table and talking about Mrs. Brooks business, and how her guitarist didnt show up on his 1st day. Mr. Brooks tries to convince Mrs. Brooks to let his old rock band play at her coffee shop. Mr. Brooks starts singing one of his songs. Then, Mrs. Brooks tells Maisey and Mara to go to their room, while she and Mr. Brooks talk to McKenna. When the twins leave, McKenna thinks its about her accidentally feeding the class pet. Mr. and Mrs. Brooks explain that Mr. Wu e-mailed them and said that McKennas grades were slipping, and he recommended that she gets a tutor from the middle school. McKenna refuses and promises to bring her grades up as theres going to be a science quiz the next day. She starts to read the science book, but doesnt understand it and quits.

At school, McKenna is desperate to improve her grades and ends up cheating off of Sierra Kuchinkos paper. McKenna gets caught by Mr. Wu. As soon as her parents find out, McKennas grounded and they tell her that unless she gets a tutor, shell have to quit gymnastics. McKenna reluctantly meets Josie Myers at the schools library. When McKenna sees Sierra, she tries to hide. Josie doesnt understand this. After Sierra leaves, Josie tries to tutor McKenna. McKenna gets angry when she finds out Josie wants her to read "baby books". She storms out of the library and asks Mr. Wu to find her a new tutor. She tries 3 different tutors who wont work for her. McKenna is soon forced to go back with Josie.

At the presentation run, the Shooting Stars gymnastics team perform for the parents. When McKenna goes up, she does everything perfectly. She comes to the very end of her practice and decides to disobey the coach by doing the back handspring. McKenna falls and breaks her ankle. When McKenna comes home, she is very angry, as her parents say that the accident will allow her to focus on school now instead of gymnastics. McKenna yells at them saying they must be happy she broke her ankle as it she would have nothing to do, but study, and runs into her room. She tears down most of her posters about the Olympics, and cries. The next day, McKenna comes to school, and Mr. Wu tells McKenna that hes sorry about her accident. McKenna keeps meeting with Josie, and is soon found out by Toulane. Toulane is very upset that McKenna lied to her. McKenna still goes on a camping trip with Josie. Afterwards, Josie invites McKenna to go to a horse riding place for kids with disabilities, as Josie is going to ride a horse. McKenna first agrees, but then remembers she gets her cast off the same day. Josie is very upset that McKenna cant go. Soon, McKenna realizes that she should go and does. There, she is surprised to see Toulane. Toulane says that McKenna showed Josie their secret handshake on their camping trip. Toulane and McKenna have a talk and make up with McKennas apology. In the end, McKennas grades improve. Toulane has a talk with her mother, and is allowed to do rhythmic gymnastics. Sierra takes Toulanes place on the team with McKenna. McKenna makes the regional competitive team, and did well during her presentation at school.

== Cast ==
*Jade Pettyjohn: McKenna Brooks (Kathryn White was Jades gymnast double)
*Ysa Penarejo: Toulane Thomas
*Kally Berard: Sierra Kuchinko
*Kerris Dorsey: Josie Myers
*Ian Ziering: Mr. Brooks
*Nia Vardalos: Mrs. Brooks
*Talia Pura: Grandma Brooks
*Will Woytowich: Bob
*Paula Rivera: Mrs. Thomas
*Aisha Alfa: Gymnastics Competition Announcer
*Abbey Thickson: Impatient Female Tutor
*Emma Leipsic: Megan Murphy
*Christina Ricci: Riding Volunteer
*Rosie: Snowflake the horse
*Cathy Rigby: Coach Isabelle Manning
*Kadence Kendall Roach: Maisey Brooks
*Paiten Raine Roach: Mara Brooks
*George Chiang: Mr. Wu

==Television release==
The film aired on NBC on July 14, 2012.   

==Home video release==
The movie was released on DVD and Blu-Ray on July 3, 2012.    The film was initially only available at American Girl and Wal-Mart, but was eventually released in PAL territories as American Girl: Shooting for the Stars.    

==Production==
An American Girl: McKenna Shoots for the Stars was filmed in Winnipeg, Manitoba, Canada. 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 