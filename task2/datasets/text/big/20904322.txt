Do Musafir
{{Infobox film
| name           = Do Musafir
| image          = Do Musafir.jpg
| image_size     = 
| caption        = 
| director       = Devendra Goel
| producer       = RabindraNath Kumar
| writer         =
| narrator       = 
| starring       =Shashi Kapoor and Rekha 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1978
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1978 Bollywood film directed by Devendra Goel. The film stars Shashi Kapoor and Rekha .

==Cast==
*Shashi Kapoor ...  Raju / Vicky 
*Ashok Kumar ...  Kailash Nath 
*Prem Chopra ...  Avinash Kumar  Pran ...  Shambhu Choudhury 
*Jagdeep ...  Murali 
*Meena T. ...  Bela 
*Chandrashekhar    Manorama ...  Kaushalya (Belas Aunt)  Dulari   
*Shetty ...  Girdhari 
*Aarti   
*Kaiser Usmani   
*Shivraj ...  Bansi 
*Chaman Puri ...  Bhavani Singh

Playback Singer :

Mohd Rafi 
Kishore Kumar
Lata Mangeshkar
Asha Bhosle and Anuradha Paudwal.

The song Hum Hain Pyar Ki by Mohd Rafi & Lata Mangeshkar was a romantic and popular song of the movie. It was picturised on Shahsi and Rekha.

==External links==
*  

 
 
 
 
 