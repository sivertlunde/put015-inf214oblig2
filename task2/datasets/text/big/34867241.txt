Iru Thuruvam
{{Infobox film
| name           = Iru Thuruvam
| image          = 
| image_size     = 
| caption        = 
| director       = S. Ramanathan
| producer       = P. S. Veerappa
| writer         = M. K. Ramu
| story          = Dilip Kumar
| narrator       =  Padmini R. Muthuraman Rajasree
| music          = M. S. Viswanathan
| cinematography = A. Vincent
| editing        = A. Paul Duraisingam
| distributor    = Jose Films
| studio         = P. S. V. Pictures
| released       =  
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} dacoit drama Padmini in the lead with R. Muthuraman, Rajasree, Pandhari Bai, P. S. Veerappa and Chittor V. Nagaiah in other significant role.  The film was a remake of 1961 Bollywood film Gunga Jumna starring Dilip Kumar and Vyjayanthimala in the lead.

==Cast==
* Sivaji Ganesan as Rangan Padmini as Kumari
* R. Muthuraman as Durai
* Rajasree as Kamala
* Pandhari Bai as Rangans and Durais mom
* P. S. Veerappa as landlord
* Chittor V. Nagaiah as Kumaris father

 

==Soundtrack==
The music composed by M. S. Viswanathan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Kannadasan || 03:26
|-
| 2 || Amman Nagan || L. R. Eswari || 04:17
|-
| 3 || Rathiri Nadanthathai || P. Susheela || 04:27
|-
| 4 || Theru Paarkka Vanthirukkum || T. M. Soundararajan, P. Susheela || 05:33
|-
| 5 || Thullivarum Sooraikkatru || L. R. Eswari || 03:07
|}

==References==
 

==External links==
*  

 
 
 
 


 