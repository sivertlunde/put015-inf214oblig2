Osaka Story
 
{{Infobox film
| name           = Osaka Story
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Jun Ichikawa
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Chizuru Ikewaki Kōsuke Minamino Kenji Sawada Yūko Tanaka
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 120 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 1999 Japanese drama film directed by Jun Ichikawa and starring Chizuru Ikewaki, Kōsuke Minamino, Kenji Sawada and Yūko Tanaka. It was released on 27 March 1999. 

==Cast==
*Chizuru Ikewaki
*Kōsuke Minamino
*Kenji Sawada
*Yūko Tanaka

==Reception==
It was chosen as the 6th best film at the 21st Yokohama Film Festival. 
{| class="wikitable sortable" width="90%"
|- style="background:#ccc; text-align:center;"
! Award
! Date
! Category
! Recipients and  nominees
! Result
|-
| Yokohama Film Festival 6 February 2000 
| Best Newcomer
| Chizuru Ikewaki
|  
|}

==References==
 

==External links==
* 

 

 
 
 
 

 
 