List of Bollywood films of 1999
 
 
 
 
A list of films produced by the Bollywood film industry based in Mumbai in 1999:

==Film list==
{| class="wikitable"
|-
! Title !! Director !! Cast !! Genre
|-
|   | 1999
|- Rishi Kapoor || Rajesh Khanna, Akshaye Khanna, Aishwarya Rai, Kader Khan, Jaspal Bhatti, Satish Kaushik, Mousami Chatterji, Himani Shivpuri ||Romance, Drama,
|- Lawrence DSouza Akshay Kumar, Drama
|-
|Anari No.1 || Sandesh Kohli || Govinda (actor)|Govinda, Raveena Tandon, Simran Bagga, Kader Khan, Satish Shah, Aruna Irani || Comedy
|-
|Arjun Pandit || Rahul Rawail || Sunny Deol, Juhi Chawla, Saurabh Shukla || Action
|- Abbas Burmawalla|Abbas-Mustan Mustan ||Shahrukh Khan, Twinkle Khanna ||Action, Comedy
|-
|Bade Dilwala || Shakeel Noorani || Sunil Shetty, Priya Gill, Paresh Rawal || Action
|-
|Bhopal Express || Mahesh Mathai || Naseeruddin Shah, Kay Kay Menon, Nethra Raghuraman, Zeenat Aman || Drama
|- David Dhawan Salman Khan, Comedy
|- Horror
|-
| Chehraa || Gautam Adhikari || Seema Kapoor, Ayub Khan, Madhoo||
|-
|  || Raj Kanwar || Sanjay Dutt, Chandrachur Singh, Mahima Chaudhry || Action, Romance
|- Ahmed Khan || Drama
|-
|Dil Kya Kare || Prakash Jha || Ajay Devgan, Kajol, Mahima Chaudhry, Chandrachur Singh || Drama
|- Sunny Deol Sunny Deol, Drama
|- Navneet Nishan, Baba Sehgal || Comedy
|- Sadashiv Amrapurkar, Ashna, Mohan Joshi || Horror
|-
|Dulhan Banoo Main Teri || Anoop Kumar || Faraaz Khan, Deepti Bhatnagar || Romance, Drama
|-
|Gair || Ashok Gaekwad || Ajay Devgan, Raveena Tandon, Reena Roy, Amrish Puri || Action
|- Vinay Shukla || Shabana Azmi, Milind Gunaji, Nirmal Pandey|| Crime, Drama
|- David Dhawan Comedy
|- Kawal Sharma  || Mithun Chakraborty, Johnny Lever, Payal Malhotra||Drama, Action, Comedy, Crime
|- Hello Brother|| Salman Khan, Arbaaz Khan Comedy
|- Hindustan Ki Kasam || Veeru Devgan || Amitabh Bachchan, Ajay Devgan, Manisha Koirala, Sushmita Sen, Prem Chopra || Patriotic, Action
|-
| Hogi Pyaar Ki Jeet || P. Vasu|| Ajay Devgn, Neha, Arshad Warsi||
|-
|Hote Hote Pyar Ho Gaya || Firoz Irani || Jackie Shroff, Kajol, Atul Agnihotri, Ayesha Jhulka || Drama
|- Gulzar (lyricist)|Gulzar Nana Patekar, Drama
|- Romantic drama
|- Sanjay Leela Salman Khan, Drama
|- Family Drama
|-
|Hum Tum Pe Marte Hain|| Nabh Kumar Raju || Govinda (actor)|Govinda, Urmila Matondkar || Romance, Drama
|- Umesh Mehra Akshay Kumar, Twinkle Khanna, Rajat Bedi || Action, Crime, Drama, Romance
|- Salman Khan, Urmila Matondkar, Monica Bedi || Romance, Drama
|- Suneel Darshan|| Akshay Kumar, Karishma Kapoor, Shilpa Shetty, Shakti Kapoor, Ashutosh Rana || Drama
|- Desh Deepak  || Rana Jung Bahadur, Shail Chaturvedi, Sonali Kulkarni|| Romance
|-
| Jai Hind || Manoj Kumar || Rishi Kapoor, Kunal Goswami, Shilpa Shirodkar, Pran (actor)|Pran|| Action, Romance
|- Deepak Bahry||Sunil Shetty, Monica Bedi, Amrish Puri|| Action
|-
|Kachche Dhaage || Milan Luthria || Ajay Devgan, Manisha Koirala, Saif Ali Khan, Namrata Shirodkar || Romance, Drama
|- Action
|-
|Kartoos || Mahesh Bhatt || Sanjay Dutt, Manisha Koirala, Jackie Shroff || Action
|-
|Kaun (film)|Kaun || Ram Gopal Varma || Urmila Matondkar, Manoj Bajpai, Sushanth Kumar || Thriller
|- Sanjay Dutt, Urmila Matondkar, Om Puri, Farida Jalal || Romance, Drama
|- Kishore Anand Horror
|- Ramesh U. Rajesh Bakshi, Horror
|-
|Kohram || Mehul Kumar || Amitabh Bachchan, Tabu (actress)|Tabu, Nana Patekar, Jaya Pradha || Action
|-
|Laawaris (1999 film)|Laawaris || Shrikant Sharma || Akshaye Khanna, Manisha Koirala, Jackie Shroff, Dimple Kapadia || Drama
|- Action
|- Action
|-
| Love You Hamesha || Kailash Surendranath|| Rishma Malik, Rohit Bal, Sonali Bendre||
|- Ashok Gaekwad  || Kasam Ali, Vikas Anand, Arjun|| Action
|- Tanikella Bharani, Brahmanandam, J.D. Chakravarthi || Comedy
|- Vivek Mushran, Romance
|- Aamir Khan, Manisha Koirala, Anil Kapoor, Sharmila Tagore, Deepti Bhatnagar || Romance, Drama
|- Ram Gopal Aftab Shivdasani, Urmila Matondkar || Romance, Drama
|- Saawan Kumar Saawan Kumar ||Jeetendra, Rekha, Randhir Kapoor ||Comedy, Drama, Family
|- Kanti Shah  ||Dharmendra, Mohan Joshi, Johnny Lever ||
|- Samir Ahmed, Vikrishnah Batt, Anu Joseph ||
|- Action
|- Rahul Roy, Saraf Deepak, Farah ||Drama, Family
|-
| Phool Aur Aag || T.L.V. Prasad || Mithun Chakraborty, Jackie Shroff, Archana|| Drama, Action, Comedy, Crime
|- Shaji N. Karun  ||Suhasini, Mohanlal, Mattanoor Shankara Marar || Drama
|- Rajesh Bakshi, Raj Kiran, Raza Murad ||
|-
|Pyaar Koi Khel Nahin || Subhash Sehgal || Sunny Deol, Mahima Chaudhry, Apoorva Agnihotri || Drama
|- Raj Kaushal || Sanjay Suri, Dino Morea, Rinke Khanna || Romance
|-
| Rajaji (film)|Rajaji || Vimal Kumar || Govinda, Raveena Tandon, Satish Kaushik|| Comedy, Romance
|-
|Safari || Jyotin Goel || Sanjay Dutt, Juhi Chawla, Tanuja || Action
|-
|Samar (1999 film)|Samar || Shyam Benegal || Seema Biswas, Ravi Jhankal, Rajeshwari Sachdev || Drama
|- Akshay Kumar, Preity Zinta, Alia Bhatt, Ashutosh Rana || Thriller, Drama
|- Imran Khalid || Mithun Chakraborty, Siddharth Dhawan, Mohan Joshi|| Action
|-
|Sarfarosh (1999 film)|Sarfarosh|| John Matthew Matthan || Aamir Khan, Sonali Bendre, Naseeruddin Shah || Action, Thriller
|- Ishrat Ali, Action
|- Rama Rao Action
|- Action
|-
|Shool || E. Nivas || Manoj Bajpai, Raveena Tandon, Sayaji Shinde || Action, Crime
|-
|Silsila Hai Pyar Ka || Shrabrani Deodar || Chandrachur Singh, Karisma Kapoor, Danny Denzongpa, Aruna Irani || Romance
|- Sirf Tum|| Agathiyan || Sanjay Kapoor, Jackie Shroff, Sushmita Sen, Priya Gill, Salman Khan || Romance
|- Drama
|- Subhash Ghai || Akshaye Khanna, Anil Kapoor, Aishwarya Rai ||Musical, Romance
|- Gopi Sapru Action
|- Govind Nihlani Drama
|- Madhur Bhandarkar Sadashiv Amrapurkar, Milind Gunaji, Jatin Kanakia || 
|- Mahesh Manjrekar || Sanjay Dutt, Namrata Shirodkar ||Crime, Drama
|- Saif Ali Khan, Twinkle Khanna ||
|-
| Zulmi  || Sandesh Kohli || Akshay Kumar, Twinkle Khanna|| Action, Romance
|-
|}

==References==
 

==External links==
*   at the Internet Movie Database
 

 
 
 

 
 
 
 