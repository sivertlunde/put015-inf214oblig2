Allemaal tuig!
 
{{Infobox film
| name           = Allemaal tuig!
| image          = 
| image_size     = 
| caption        = 
| director       = Ben Sombogaart
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1982
| runtime        = 94 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1982 Netherlands|Dutch film directed by Ben Sombogaart.

==Plot==
A gang of youths is arrested by the police on charges of theft and vandalism. They must all wait at home (as a form of house arrest) to be summoned in court. Through a series of flashbacks the viewer gains insight as to how the gang lost its way.

==Cast==
* Tonny Tedering - Bart
* Floris Andringa - Monne
* Annemarie van Ginkel - Anja
* Pieter Hessel - Wijnand
* Femy Keuben - Shirley
* Ronald Straub - Leen
* Germaine Groenier - television reporter

== External links ==
*  

 
 
 

 