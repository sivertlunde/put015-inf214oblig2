Rustlers of Red Dog
{{Infobox film
| name           = Rustlers of Red Dog
| image          = DVD cover of the movie Rustlers of Red Dog.jpg
| image_size     =
| caption        = DVD cover
| director       = Lew Landers
| producer       = Milton Gatzert Henry MacRae (associate) Ella ONeill Nate Gatzert Vin Moore Nathaniel Eddy
| narrator       = Walter Miller Harry Woods
| music          = Richard Fryer William A. Sickner
| editing        = Irving Applebaum Saul A. Goodkind Alvin Todd Edward Todd
| distributor    = Universal Pictures
| released       =  
| runtime        = 12 chapters (231 min)
| country        = United States English
| budget         =
}} Universal Serial movie serial William "Buffalo 1930 serial The Indians are Coming

==Cast==
* Johnny Mack Brown as Jack Wood
* Joyce Compton as Mary Lee
* Raymond Hatton as Laramie Walter Miller as "Deacon" Harry Woods as "Rocky"
* Fred MacKaye as Snakey William Desmond as Ira Dale, the Wagonmaster
* Charles K. French as Tom Lee 
* J.P. McGowan as Capt. Trent
* Lafe McKee as Bob Lee
* Edmund Cobb as Henchman Buck 
* Chief Thundercloud as Chief Grey Wolf 
* Chief Many Treaties as Indian
* Jim Thorpe as Chief Scarface

==Production==

===Stunts=== Cliff Lyons
*George Magrill
*Frank McCarroll  Wally West

==Chapter titles==
# Hostile Redskins
# Flaming Arrows
# Thundering Hoofs sic| 
# Attack at Dawn
# Buried Alive
# Flames of Vengeance
# Into the Depths
# Paths of Peril
# The Snake Strikes
# Riding Wild
# The Rustlers Clash
# Law and Order
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 212
 | chapter = Filmography
 }} 

==See also==
* List of film serials
* List of film serials by studio

==References==
 

==External links==
* 
* 

 
{{succession box  Universal Serial Serial  Tailspin Tommy (1934)
| years=Rustlers of Red Dog (1935)
| after=The Call of the Savage (1935)}}
 

 
 

 
 
 
 
 
 
 
 
 


 