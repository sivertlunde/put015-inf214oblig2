Mazhayethum Munpe
{{Infobox film
| name           = Mazhayethum Munpe
| image          = Mazhayethum Munpe.jpg Kamal
| producer       = Madhavan Nair Sreenivasan
| Annie Sreenivasan
| music          = Original Songs:  
| cinematography = S. Kumar
| editing        = K. Rajagopal
| studio         = Murali Films
| distribution   = Central Prism
| country        = India
| language       = Malayalam
}}

Mazhayethum Munpe ( ) is a 1995   starring Ajay Devgan, Ameesha Patel and Mahima Chaudhry. 

==Plot== flashback mode revealing the reasons for Nandakumars estranged state.

Nandakumar was a college professor. He had moved to the city from his village of kerala for the sake of his job. Rahman was his colleague with whom he stayed. In the college he had to confront a mischievous gang of girls headed by Shruthi (Annie (actress)|Annie). Nandakumar had a serious, no-nonsense attitude and the gang played a lot of pranks upon him. Nandakumar had a second life in his native village, where he had to take care of the treatment of his paralyzed fiancée Uma Maheshwari (Shobhana). It was to meet her medical expenses that he had taken up this job. 

The tussle between Nandakumar and the gang proceeded in parallel. Gradually, Shruthi falls for Nandakumar and revealed her feelings for him. He laughed it off as a teenage infatuation. But she persisted. Meanwhile, Umas condition improved remarkably with her regaining the ability to walk. Nandakumar, who had developed a cordial relationship with the gang by then, took them for a trip to his picturesque village. His main intention was to make Shruthi meet Uma so that she would change her mind. Shruthi got shattered on witnessing the warmth in the relationship between Uma and Nandkumar. After returning, she paid a discreet visit to Uma. There she revealed her feelings for Nandakumar to Uma. She accused Uma of being selfish and possessive by forcing Nandakumar to sacrifice his life and pleasures for her sake. Uma got a mental shock from the vitriolic behaviour of Shruthi and that triggered a second stroke. She became paralyzed again and doctors gave up all hope. She forced him to marry Shruthi and he complied reluctantly.

Even after the marriage Nandakumar was not able to find any peace of mind. Their relationship was very cold. Later, Shruthi told Nandakumar about her meeting with Uma. Enraged by this revelation he left his home. He wandered across places like a mad man. 

On coming back, Nandakumar learns that Shruthi had committed suicide after giving birth to his child. Rahman takes him to Umas home. There he finds Uma, whose condition improved, taking care of his child.

==Cast==
* Mammootty as Nandakumar Varma
* Shobhana as Uma Maheswari Annie as Shruthi Sreenivasan as Rahman
* Suma Jayaram as Rehna, Rahmans wife
* Praseetha Menon as Kunjumol, Shruthis friend
* Manju Pillai as Anjana, Shruthis friend
* Keerthi Gopinath as Shwetha, Shruthis friend
* Sankaradi as Umas father
* N. F. Varghese as Valappil Kaimal
* Sukumari as Mariamma, caretaker of Shruthis house
* T. P. Madhavan as Narayanan Nair
* Valsala Menon as College Principal
* Madhu Mohan as Dr. Issac
* Usharani

== Soundtrack ==
{{Infobox Album |  
| Name           = Mazhayethum Munpe  
| Cover          = 
| Caption        = 
|Type=soundtrack
| Artist         = Raveendran & R. Anandh 
| Released       = 1995  (India) 
| Recorded       =  Feature film soundtrack 
| Length         = 
| Label          = 
| Producer       = 
| Certification  = 
| Chronology     =  Raveendran
| Last album     = "Kakkakkum Poochakkum Kalyanam	" (1995)
| This album     = "Mazhayethum Munpe" (1995)
| Next album     = "Oru Abhibhashakante Case Diary" (2004)
| Misc           = {{Extra chronology
| Artist         = R. Anandh
| Type           = albums
| Last album     = 
| This album     = "Mazhayethum Munpe" (1995)
| Next album     = "Nirnayam (1995 film)|Nirnayam"  (1995)
}}
}}
The songs of the film were well acclaimed. Raveendran composed 3 out of the six songs, while famed advertisement jingle composer R. Anandh composed the rest. Raveendrans Aatmavin Pusthakathaalil and Enthinu Veroru Sooryodayam went on to become hits, while Anandhs Ladies Collegil too became a crowd favourite. 
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! #!! Song !! Artist(s) !! Composer !!
|-
| 1
| Aathmavin Pusthaka 
| K. J. Yesudas
| Raveendran
|-
| 2
|Chicha Chicha 
| S. Janaki
| Raveendran
|-
| 3
| Enthinu Veroru Sooryodayam
| K. J. Yesudas, K. S. Chithra
| Raveendran
|-
| 4
| Ladies Collegil Campus
| Usha Uthup, Annupamaa, M. G. Sreekumar, Mammootty
| R. Anandh
|-
| 5
| Manassu Pole Mano
| R. Anandh
|-
| 6
| Swarnapakshi Swarnapakshi Ku Koo
| Sujatha Mohan, Manoj
| R. Anandh
|}

==Awards== Best Screenplay Kamal won the best director Ramu Kariat award of 1995.

==External links==
* 

 

 
 
 
 
 