The Last Days of Pompeii (1959 film)
 
{{Infobox film
| name           = The Last Days of Pompeii
| image          = Gli ultimi giorni di Pompei- Los ultimos dias de Pompeya .JPG
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Mario Bonnard Sergio Leone
| producer       = Paolo Moffa Lucio Fulci
| writer         = Sergio Corbucci Ennio de Concini Sergio Leone
| based on       =   
| starring       = Steve Reeves Christine Kaufmann Fernando Rey
| music          = Angelo Francesco Lavagnino
| cinematography = Antonio L. Ballesteros
| editing        = Eraldo Da Roma   Julio Peña
| studio         = Cineproduzioni Associate  (Italy)  Procura (Spain) Transocean (Germany)
| distributor    = United Artists (US)
| released       =  
| runtime        = 100 minutes
| country        = Italy   Spain  Germany
| language       = English
| budget         = 
| gross          = 
}}
The Last Days of Pompeii (1959) ( ) is an Italian sword and sandal action film starring Steve Reeves, Christine Kaufmann, and Fernando Rey and directed by Sergio Leone. Mario Bonnard, the original director, fell ill on the first day of shooting, so Leone and the scriptwriters finished the film.

The film is characterized by its CinemaScope framing and lavish look and is one of many films produced during the 1960s as part of the "peplum" sword and sandal craze, originally launched by Pietro Franciscis 1958 film Le fatiche di Ercole, released as Hercules in the United States by Joseph E. Levine.
 series of spaghetti westerns starring Clint Eastwood. In his use of widescreen, as well as his penchant for colorful violence and outlandish plot twists, Leone displays a visual flair that would soon propel him to directorial fame.

== Plot ==
Glaucus, a centurion returning to his home in Pompeii after a spell in Palestine, arrives on the edge of town just in time to see Ione, the beautiful daughter of the citys Consul, lose control of her chariot. Glaucus saves Iones life and then heads into town to see his father. On the way, Glaucus intervenes in defense of a thief named Antonius, who is being severely punished on orders given by Gallinus, a Praetorian Guard. On arriving at his fathers house, Glaucus discovers that it has been looted and his father murdered by a band of ruthless hooded thieves who have been terrorizing the city, always leaving a cross painted on a wall as a calling card. Glaucus vows revenge against the killers.

In order to convince the Emperor that the mass murders are not a sign of trouble, Ascanius, the Consul of Pompeii, orders a city-wide festival. In the streets, Antonius rolls a drunken soldier and steals his pouch. The pouch contains a ring that belonged to Glaucuss father and a black hood identical to that worn by the band of killers. Antonius brings the ring to Glaucuss friend Marcus, who follows the suspicious soldier to Pompeiis Temple of Isis. But before Marcus can tell anybody what he has discovered, he is killed by Arbaces, the High Priest of Isis, and his body is left to be found with a Christian cross carved into it.

During the festival Glaucus takes out his anger by getting drunk and crashes a party at Ascaniuss house. There, the disreputable Praetorian Guard Gallinus tries to rape Nydia, Ione’s blind slave, much to the amusement of the crowd. Glaucus defends Nydia, easily defeating Gallinus in a fight.
The next day Marcuss funeral is held, with Glaucus and Antonius in attendance. After the ceremony, Antonius reaffirms his anti-Christian prejudices. But Nydia is a Christian, and thinking she is addressing Antonius, says that he should attend a secret Christian gathering to find out how good the Christians really are, and tells him how to find it. However, it is Gallinus who hears her plea. Gallinus is in charge of persecuting Christians and, with this piece of information, that night rounds up and imprisons all the Christians. The leader of the Christians is tortured with his followers. They are condemned to death, accused of the crime wave that has affected Pompeii.

Glaucus and Ione have fallen in love, and convinced that the Christians have been falsely accused, he heads to Herculanum to intervene in their favor with Iones father, who has left Pompeii. On his way, Glaucus is ambushed by the hooded men. He survives the attack, arriving injured at Ascaniuss retreat. Meanwhile, Antonius follows Marcuss killer to the temple of Isis, discovering that the men in the hooded masks are working under Arbacess orders. Antonius arrives at Herculenum and informs Glaucus and Ascanius of his discovery. As proof, he tells them that the treasures stolen from the citizens of Pompeii are hidden in the temple of Isis. Once back in Pompeii, Antonius recruits the help of Helios and Caios, Glaucuss army friends. At the temple of Isis, Glaucus fights off both Arbaces and Gallinus, but he is thrown into a secret ditch and finds himself in a waterlogged underground chamber wrestling with a crocodile. He wins the fight and escapes the trap. Julia, the Consuls Egyptian mistress, is in fact the mastermind behind the crimes of the black-hooded men and the dirty dealing of Gallinus and Ascanius. They are raising funds to finance an uprising against the Roman Empire. She confesses this to Ascanius and stabs him, blaming Glaucus for the killing. Accused of murder, Glaucus is imprisoned near the Christians. Ione tries to come to his defense, but since she has converted to Christianity, she is also sent to prison. The Christians are tossed into the arena to be devoured by lions, but Glaucus manages to pull his chains from out of a lump of rock, stabs the lion, and spears two gladiators sent to kill him. A band of bow-men, including friends of Antonius and Glaucus, arrive wearing masks and attack those who had condemned the Christians. Opening fire on the royal box, they kill Gallinus with their arrows. As the city troops arrive to stop them, Mount Vesuvius erupts. In the chaos, everyone tries to escape. Julia and Arbaces are crushed at the temple of Isis by the falling masonry while trying to retrieve their treasure. Nydia dies in Antoniuss arms, killed by falling debris. Glaucus swims through a burning harbor. With Ione, he survives Pompeiis destruction, sailing toward open sea.

==Cast==
 
 
* Steve Reeves as Glaucus
* Christine Kaufmann as Ione
* Fernando Rey as Arbaces, high priest
* Barbara Carroll as Nydia
* Anne-Marie Baumann as Julia
* Angel Aranda as Antonius
* Mimmo Palmara as Gallinus
 
* Guillermo Marín as Ascanius, Consul of Pompeii
* Carlo Tamberlani as Leader of the Christians
* Mino Doro as Second Consul
* Mario Berriatúa as Marcus
* Mario Morales as Caius
* Ángel Ortiz as Helios
* Lola Torres as hotel keeper
 

== Production ==

===Background===
The 1959 film The Last Days of Pompeii was the eighth cinematic version of Edward Bulwer-Lyttons novel of the same name. Frayling, Something to Do With Death, p. 92  First published in 1834, the novel became a bestseller, helped on its release by the eruption of Vesuvius just before publication. The novel was a fictional account of the events surrounding the eruption of Vesuvius that buried the Roman city of Pompeii in AD 79.   It was an example of a widespread English fascination in the 1830s with great natural catastrophes and the moral lessons to be learned from them.
 Robert Harris novel set in the city—a project that has not advanced any further. The 1984 television film is the only version so far to have closely followed the original Bulwer-Lytton novel.
 The Last Days of Pompeii ten years earlier. 

===Writing=== The Ten Commandmentss success.

===Casting===
Two weeks before shooting began,  . The injury presented a balance problem for Reeves, and stunt doubles were used for his scenes on horseback. 

The female lead is played by Christine Kaufmann, who was only 14 years old at that time. The respected Spanish character actor Fernando Rey appears as the villainous high priest.  Barbara Caroll, of 1961s Goliath contro i giganti plays the blind slave Nydia, and fans of Goliath contro i giganti should also recognize Spanish actor Ángel Aranda as the young Antonius. Mimmo Palmara was already a genre regular, having appeared in both Hercules films with Reeves. He has the role of the Praetorian guard Gallinus. Anne-Marie Baumann, who only had a brief career in films, plays Julia, the Consul’s Egyptian concubine. Each of the contributing production companies insisted on a name above the title to represent and protect its investment. 

===Filming===
Credited director Mario Bonnard worked in the preproduction of the film and the concept for it was his. However, during the early days of filming, once he arrived with cast and crew to shoot exteriors to be filmed in Spain, Bonnard felt seriously ill with a liver problem and he was replaced by assistant director Sergio Leone. Leone, by coincidence, had also stepped in once before to help finish another film directed by Mario Bonnard, the comedy Hanno rubato un tram.

An Italian, Spanish, and West German co-production, the film was largely founded by Procura of Madrid, and United Artists distributed it worldwide. The film was shot in the Andalusian countryside, with most of the interiors filmed in Madrid.

==Notes==
 

==References==
* Frayling, Christopher. Something to Do With Death. Faber & Faber, 2000. ISBN 0571164382

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 