Un tour de manège
{{Infobox Film
| name           = Un Tour de Manège
| image          = Tourdemanege.jpg
| caption        = Film Poster ©1989 Gaumont
| director       = Pierre Pradinas
| producer       = Robert Guédiguian, Albert Prévost
| writer         = Pierre Pradinas, Alain Gautré
| narrator       = 
| starring       = Juliette Binoche, François Cluzet, Denis Lavant, Michel Aumont
| music          = Albert Marcoeur
| cinematography = Jean-Pierre Sauvaire
| editing        = Chantal Delatre Gaumont
| released       = 29 March 1989
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Un Tour de Manège ( ; a.k.a. Roundabout (UK), Once Around the Park (US)) is a 1989 French film starring Juliette Binoche, François Cluzet, Denis Lavant and Michel Aumont. It is the first and to date only film directed by the acclaimed French theatre director Pierre Pradinas. The film was shot on location in Paris in the summer of 1988.

==Plot summary==
Al (Francois Cluzet) and Elsa (Juliette Binoche) have been a couple for some time, but the chances that their relationship will be long-lived are few. For one thing, Al is appallingly dependent on Elsa for his every emotional need. For another, Elsa is an incredibly elusive person, extremely difficult to pin down about anything—especially whatever is bothering her. How they have managed to survive this long is a cause for wonder. When Al gets an opportunity to be cast in a movie role, complete with no-cost occupancy in the casting agents ugly but fashionable apartment, he jumps at the chance to provide a little material satisfaction for his beloved Elsa. But what exactly does she want?

== External links ==
*  

 
 
 
 

 