Aagaman
{{Infobox film
| name = Aagaman
| image =
| alt =
| caption =
| film name =
| director = Muzaffar Ali
| producer =Uttar Pradesh Sugarcane Seed and Development Corporation
| writer =
| screenplay =
| story = R. Trivedi
| based on =  
| narrator =
| starring ={{Plainlist|
* Anupam Kher
* Rani Deepa
* Suresh Oberoi
* Saeed Jaffrey 
* Dilip Dhawan
}}
| music = {{Plainlist|
* Ustad Ghulam Mustafa Khan   
* Faiz Ahmed Faiz
* Hasrat Jaipuri
}}
| cinematography =
| editing =
| studio =Integrated Films
| distributor =
| released = 1982
| runtime = 150 mins.
| country = India
| language = Hindi
| budget =
| gross =
}}
Aagaman is a 1982 Hindi language drama film directed by Muzaffar Ali and produced by Uttar Pradesh Sugarcane Seed and Development Corporation under the banner of Integrated Films. The cast included Saeed Jaffrey, Suresh Oberoi, Dilip Dhawan, Anupam Kher, Bharat Bhushan and Raj Bisaria. The film marked the debut of Anupam Kher.   In 2012 The Hollywood Reporter included him in its list of five best actors in Asia.  The plot involved the politics and working techniques of Uttar Pradesh sugarcane co-operative societies.     Upon release the film received mixed reviews. In November 2012 the film was shown at the 43rd International Film Festival of India. 

== Plot ==

In a village of Awadh, sugarcane planters are exploited by mill owner (Bharat Bhushan). A young man (Suresh Oberoi) returns to this village after receiving a degree in law from a reputed university of Lucknow. Seeing the poor condition of planters he urges them to stop selling their sugarcane to the mill owner and instead start their own co-operative mill. The mill owner, his landlord-agent (Saeed Jaffrey) and son discourage the villagers from doing so. The mill owners son urges the people to form a planters union and continue selling their crop to the mill. Initially the villagers trust him but after continuous persuasions from the young man and realising that only self-sufficiency can improve their condition, they unite and set up their own mill. In the end the young man marries a village girl (Deepa) who was very active in the movement. 

== Cast ==

 

* Bharat Bhushan — Mill owner
* Saeed Jaffrey — Mill owners agent
* Suresh Oberoi — The young man
* Deepa — Village girl

== Production ==

The then chief minister of Uttar Pradesh, Vishwanath Pratap Singh asked Muzaffar Ali to make a film for promoting the sugarcane co-operative societies in the state. Uttar Pradesh Sugarcane Seed and Development Corporation provided the necessary budget.  R. N. Trivedi and Asghar Wajahat wrote the films story,   Ustad Ghulam Mustafa Khan composed the films music and the lyrics were written by Faiz Ahmad Faiz and Hasrat Jaipuri.  

== Reviews ==
 Umrao Jaan and Aagaman focused on Awadh. In an article published in 2012, Business Standard noted that "To   Ali goes the credit of portraying Awadh truthfully on cinema;  the only other claimant being Satyajit Ray in Shatranj Ke Khiladi".  Sumit Mitra of India Today called it a disappointing film and critised Alis film-making technique and cinematography. He added that "Alis approach to direction is more that of film club enthusiast than film maker" and the lyrics used were "totally out of context". Mitra criticised the promotional nature of the film by saying that the state owned co-operative societies have dominated the market but praised Saeed Jaffreys acting.   

==References==
 

== External links ==
* 
 
 

 
 
 
 