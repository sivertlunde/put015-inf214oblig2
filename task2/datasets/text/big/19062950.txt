Keïta! l'Héritage du griot
{{Infobox film
| name           = Keïta! lHéritage du griot
| image          = Keita!.jpg
| image size     = 
| caption        = 
| director       = Dani Kouyaté 
| producer       = Sahelis Productions
| writer         = Dani Kouyaté
| narrator       = 
| starring       = Sotigui Kouyaté
| music          = 
| cinematography = 
| editing        = 	
| distributor    = 
| released       = 1995
| runtime        = 94 minutes
| country        = Burkina Faso
| language       = French
| budget         = 
}}
 1995 Burkina Faso|Burkinabé drama film directed by Dani Kouyaté and starring Sotigui Kouyaté.

==Cast==
*  
*Hamed Dicko: Mabo Keïta 
*Abdoulaye Komboudri: Drissa Fafana 
*Sotigui Kouyaté: Djeliba Kouyate 
*Claire Sanon: Sitan 
*Blandine Yaméogo: Sogolon

==Plot==
Keïta is a retelling of the first third of Sundjata Keitas 13th-century epic, Sundjata.    It tells of Mabo Keïta (Dicko), a thirteen-year-old boy who lives in a middle-class family in Ouagadougou and attends a good school. One day he encounters Djeliba Kouyate (Kouyaté), an elderly griot, who wants to tell the young Keïta the origin of his name, being related to Sundata (Boro). Kouyate begins his story with the Mandeng creation myth: As all living beings come together in the newly formed Earth, one man proclaims to the masses that he wants to be their king. They respond, "We do not hate you." The old griot goes on to tell how Keitas family are descended from buffalo, the blackbirds are always watching him, and how people have roots that are deep in the earth. The film shows realistic-looking flashbacks to ancient times and ends with Sundjata Keita being exiled from the Kingdom of Mande, to which he lays claim.

==Production==
Dani Kouyaté directed a number of short films before the release of Keïta, his first full-length feature.  The films working title was Keita: From Mouth to Ear.    It was shot in the towns of Ouagadougou, Sindou, and Ouahabou. 
The assistant director was Alidou Badini. {{cite web
 |url=http://www.africultures.com/php/index.php?nav=personne&no=9140
 |title=Alidou BADINI
 |work=Africutlures
 |accessdate=2012-03-12}} 

==Reception==
Keïta! received the Best First Film Prize from the Panafrican Film and Television Festival of Ouagadougou (Fespaco) and was awarded the Junior Prize at the Cannes Film Festival.  The New York Times praised the film, claiming it "succeeds admirably in keeping... history alive."  In a 1995 interview, Kouyate reflected on the experience and commenting on traditional society, saying:

 

==Notes==
 

==References==
* 
* 

== External links ==
*  

 
 
 
 
 