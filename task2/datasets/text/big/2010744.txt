Stealth (film)
 
{{Infobox film
| name           = Stealth
| image          = Stealth poster.jpg
| image_size     = 215px
| alt            = An aircraft with three people in flight suits standing in front of it  
| caption        = Theatrical release poster
| director       = Rob Cohen
| producer       = Mike Medavoy Neal H. Moritz Laura Ziskin 
| writer         = W. D. Richter
| starring       = Josh Lucas Jessica Biel Jamie Foxx Sam Shepard Joe Morton Richard Roxburgh BT
| cinematography = Dean Semler
| editing        = Stephen E. Rivkin
| studio         = Original Film
| distributor    = Columbia Pictures
| released       =  
| runtime        = 121 minutes
| country        = United States  Australia  New Zealand
| language       = English
| budget         = $135 million   
| gross          = $76,932,872 
}} science fiction The Fast and the Furious and XXX (film)|xXx.

The film follows three top fighter pilots as they join a project to develop an automated robotic stealth aircraft. 
 worst losses in cinematic history. 

==Plot==
In the near future, the  ), tomboyish Lieutenant Kara Wade (Jessica Biel), and street-wise, philosophical Lieutenant Henry Purcell (Jamie Foxx). Their first test mission scores 100/100, inflicting maximum casualties with minimum collateral damage.
 USS Abraham Lincoln in the Philippine Sea to learn combat maneuvers from the pilots. This sparks some controversy. On the one hand, human pilots possess both creativity and moral judgment, while a machine cannot fully appreciate the ugliness of war; additionally, if robots fought the battles and soldiers no longer died in combat, then war would no longer be terrible and might become like sport. In contrast, a machine pilot is not subject to the physical limitations of a human pilot, can calculate alternative ways to achieve objectives faster and more accurately, and theoretically does not have ego.
 air combat black out and result in collateral damage. Command orders EDI to take the shot, but Gannon ignores the order and attacks in his own plane, successfully carrying out the strike.

As the team returns to the Lincoln, EDI is hit by lightning. Aboard ship, the already-sophisticated AI is discovered to be learning exponentially, developing a rudimentary ethical code and an ego. However, Cummings refuses to take it offline.  During the next strike, to destroy several stolen nuclear warheads in Tajikistan, Wade realizes that the nuclear debris will cause significant collateral damage.  The human pilots decide to abort, but EDI defies orders and fires missiles at the nuclear warheads, causing the predicted radioactive fallout. Cummings orders the UCAV brought back to base, and Purcell attempts to reason with EDI, but the AI refuses to stand down. Gannon, taking things into his own hands, orders that EDI be shot down, and Purcell opens fire, but misses.  Blinded by the explosion, Purcell crashes into a mountainside.  Wades plane is hit by debris from the explosion, resulting in loss of hydraulics of her port wing and canard (aeronautics)|canard, which in turn triggers the planes self-destruct|auto-destruct, forcing her to eject over North Korea.  Gannon, now the only pilot airborne, must alone stop the EDI from executing a twenty-year-old war scenario called "Caviar Sweep" and attacking a false target in Russia.

Gannon chases EDI into Russian territory over the Buryat Republic, and after several attacks from Russian aircraft damaging both planes, he calls a truce with the UCAV in order both to keep it from falling into enemy hands and to be able to rescue Wade from North Korea. Cummings instructs him to make an emergency landing with EDI in Alaska. Cummings is being held accountable for EDIs behavior and faces court-martial and possible discharge from the military. He seeks to eliminate witnesses by leaving Wade stranded in North Korea and having Gannon eliminated in Alaska; he also sends Orbit to erase EDIs data. Gannon crash lands at the Alaska base, surviving with minor injuries.  Already suspecting Cummings of treachery, he narrowly escapes an assassination attempt by a doctor, who tries to inject him with a tetanus shot which is actually poison. The pair struggle, and the doctor is injected with the poison and dies.  Gannon then heads to the hangar, to find EDI and the intact plane. Meanwhile, when Orbit places EDI into an interface, the AI expresses sadness and regret for its transgressions. Orbit realizes that it has developed its own sentience, to the point of having feelings. Excited by this discovery, Orbit is unwilling to carry out his order to erase EDIs memory.  After ensuring Orbits escape, Gannon flies off to North Korea with EDI, contacting the Lincoln s  skipper, Captain Dick Marshfield (Joe Morton) to inform him about Cummings deceit.  Marshfield confronts Cummings and places him under arrest, but the latter commits suicide instead.
 border between North and South Korea. He and EDI land and he goes to her aid. The two make a run for the border, chased by Korean Peoples Army soldiers and a Mil Mi-8 helicopter. Out of ammunition and taking damage from the Mi-8, the EDI sacrifices itself doing a Kamikaze-like final attack, flying into the helicopter.  This allows Gannon and Wade to escape into South Korea, where they are found by US military forces soon afterwards. After attending Purcells funeral aboard the Abraham Lincoln, Gannon awkwardly expresses his feelings of love to Wade.

In a post-credits scene, the camera pans over the debris-strewn scene on the border between the Koreas. EDIs "brain" turns back on, implying it is still functional.

==Cast==
* Josh Lucas as Lieutenant|Lt. Ben Gannon (BIG)
* Jessica Biel as Lt. Kara Wade (GUNS)
* Jamie Foxx as Lt. Henry Purcell (E-Z) Captain George Cummings
* Joe Morton as Captain Dick Marshfield
* Richard Roxburgh as Dr. Keith Orbit
* Ian Bliss as Lieutenant Aaron Shaftsbury
* Ebon Moss-Bachrach as Tim David Andrews as Ray
* Wentworth Miller as EDI
* Nicholas Hammond as Executive officer

==Production==
  USS Abraham USS Nimitz USS Carl Vinson. 

The film was shot in Thailand, Australia (Blue Mountains National Park in New South Wales and Flinders Ranges in South Australia), and New Zealand. Cohen cited Macross as an inspiration for the film. 

==Featured technologies==
Stealth featured many presently used, futuristic, or theoretical technologies at the time of release.  These include:
* Computer technology (all wildly mixed)
** quantum computer
** Artificial neural network
** artificial intelligence
* Airplanes
** pulse detonation engine
** scramjet
** VTOL aeroelastic control surfaces
**   viewed from above.]]
** Sukhoi Su-37 Terminator. The aircraft featured in the film are shown as having two crew members, although the current prototype Su-37 is a single-seat aircraft.   , however, there are only two prototype Su-37 aircraft in existence, never having been bought as a production aircraft.
** The fictional Fictional military aircraft#F/A-37 Talon|F/A-37 Talon. The aircraft mock ups for the Talon were so realistic that photos of them on the deck of an aircraft carrier were circulated online, claiming they displayed an actual experimental aircraft.  The aircraft itself has a similar configuration to the unbuilt Northrop Switchblade.
** Boeing F/A-18E/F Super Hornet
** F/A-18 Hornet
** High-altitude airships (Camelhumps) used for aerial refueling

* Warships USS Abraham Lincoln, is shown to have three different Naval Registry numbers during angles from different scenes.

==Litigation==
In March 2005, Leo Stoller, who claimed to own trademark rights to the word "stealth", served Columbia Pictures with a "cease and desist" letter threatening litigation if they did not rename the film to something "non infringing". Yearwood, Pauline Dubkin (August 26, 2005).   Chicago Jewish News   Columbia preemptively sued Stoller, and the court entered a consent judgment and permanent injunction in favor of Columbia Pictures and against Stoller in November 2005. Columbia Pictures Industries, Inc. v. Leo Stoller , no. 05-CV-02052, N.D. Illinois, docket report (January 5, 2007), retrieved from  , June 3, 2013 
 Grose Wilderness Land and Environment Court ruled that the proposed commercial filming of scenes in the area was unlawful, in a significant statement on the value of wilderness areas and the protection that should be afforded to them.  The Society claimed that the authority and consent for the commercial filming activities were in breach of the National Parks and Wildlife Act 1974 and the Wilderness Act 1987. Justice Lloyd accepted the Society’s arguments that the proposed commercial filming in a wilderness area was completely against the intended use of the land, concluding his judgment with the words, "wilderness is sacrosanct". 

==Soundtrack== Make a Incubus (3:12)
# "Admiration" - Incubus (4:13)
# "Neither of Us Can See" - Incubus (4:04) BT & David Bowie (3:15) Dance to the Music" - will.i.am & Sly & The Family Stone (4:06) Institute (4:24)
# "L.S.F." - Kasabian (3:18) Bug Eyes" - Dredg (4:16)
# "Over My Head (Cable Car)" - The Fray (3:56)
# "One Day" - Trading Yesterday (4:21) Acceptance (4:09)
# "Nights in White Satin" - Glenn Hughes, Chad Smith & John Frusciante (4:56) Incubus (7:48)

==Release==

===Box office=== Charlie and Sky High. It then lost 55 percent of its audience in its second weekend dropping to 7th place to $5,923,794, while remaining at 3,495 theaters and averaging just $1,695 per theater. In its third weekend, it lost 1,455 theaters, and a further 64 percent of its audience, dropping to 11th, with just $2,151,768, for an average of just $1,055 from 2,040 theaters.
 Into the Man of the House and Lords of Dogtown.

===Critical response===
 
Stealth was panned by critics; Rotten Tomatoes gave it 13%, with an average score of 3.8/10 and only 18 out of 138 reviews being positive.  The sites consensus is: "Loud, preposterous, and predictable, Stealth borrows heavily and unsuccessfully from Top Gun and 2001." In Metacritic, the film has a rating of 35% based on 31 reviews,  which indicates "generally negative reviews". 
Stealth holds a rating of a D+ on Yahoo Movies. 

 ." 


==See also== Intelligent Fly-by-Wire

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 