Ureme 8
 
{{Infobox film
| name           = Uremae 8
| image          =  Ureme 8 Front.jpg
| film name      = {{Film name
 | hangul         =   8
 | hanja          = 에스퍼맨과 우뢰매 8
 | rr = Eseupeomaengwa Ulemae 8 
 | mr = Esŭp‘ŏmaengwa Uroemae 8 }}
| director       = Kim Cheong-gi
| producer       =
| writer         = Kim Cheong-gi |
  music          =
| cinematography =  |
  editor         =
| starring       = Shim Hyung-rae   Cheon Eun-gyeong   Kang Ri-na   Kim Hak-rae   Eom Yong-su   Woo Yoon-ah |
  distributor    =
| released       =  
| runtime        = 70 minutes
| country        = South Korea
| language       = Korean
| budget         =
}}
Ureme 8: Esperman and Ureme 8 (1993) is the eighth in the Ureme series of Korean childrens science-fiction films. After separating in the previous two entries, director Kim Cheong-gi and star Shim Hyung-rae were reunited in this film. This is the eighth installment in the Ureme saga.

 

 
 
 


 