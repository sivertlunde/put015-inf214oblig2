Ghostride the Whip
 
{{Infobox film
| name = Ghostride the Whip
| image = Ghostridethewhip.jpg
| caption = Official GhostRide the Whip Poster
| producer = DJ Vlad  Peter Spirer
| director = DJ Vlad
| writer = DJ Vlad David Wilson Lazlo
| editing = David Wilson Lazlo Joel Rutkowski
| cinematography = DJ Vlad   Jay Gira   Keith Gruchalla   Jeff Bollman Sway
| starring = Mac Dre E-40 Keak Da Sneak   MC Hammer   Too Short   Mistah F.A.B.
| graphics = Buildestroys Corey Shaw and Andrew Cleary
| colorist = Matchframes Kelly Reese
| technical consultant = Bryson Jones
| assistant editor = Joel Rutkowski
| music = Various Artists
| distributor = Image Entertainment
| released =  
| runtime = 86 minutes
| country = United States
| language = English
}} San Franciscos bay area several years ago. At times, through a historical perspective, the film brings to light the elements and trends that may have been responsible for the origins of ghostriding. The documentary also explores the demographic that most appeals to this art form and the lifestyles of those that regularly participate in it.
 ghostriding the whip involves a car in motion with no one operating it. The closest thing that can come to describing ghostriding the whip is perhaps the Chinese fire drill that was popular back in the 1950s. However, unlike the Chinese fire drill the car remains in motion with no urgency to get back into it. In fact, the driver and passengers will walk next to the car as it casually rolls down the street, with thumping music playing at extreme volumes as the riders participate in free-form dance within close proximity to the car. Often the occupants of the vehicle will climb on the hood and roof, expressing themselves in dance high atop the automobile.
 Ghostriding the Whip are all attributed to the rap star that was murdered in 2004 after performing a show in Kansas City, Missouri.

==Cast==
*Mac Dre
*MC Hammer
*Keak Da Sneak Too $hort 
*Mistah F.A.B.
*Akon
*Gary Archer
*Adisa Banjoko
*Bavgate
*Baygreen
*Cellski
*Co-Co
*John Costen
*Big Daut
*Kuzzo Fly
*Roland Homgren
*Zab Judah
*Sean Kennedy
*B. Legit
*Messy Marv
*Money-B
*T.L. Muhammad
*Nump
*Keak Da Sneak
*Haji Springer
*Stretch
*Sumthin Terrible
*Traxamillion
*Coolio Da Undadogg
*Dwayne P. Wiggins

==Soundtrack==
The movies official soundtrack is performed by J-Diggs.

==Shooting locations==
East Bay, California.

==References==
 
#   
 

==External links==
*  
*  

 
 
 
 
 