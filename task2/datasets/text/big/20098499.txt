The Rainbow (film)
 
 
{{Infobox film
| name           = The Rainbow
| image          = The-rainbow-ken-russell.jpg
| caption        = Theatrical release poster
| director       = Ken Russell
| producer       = Dan Ireland William J. Quigley
| writer         = D. H. Lawrence Ken Russell Vivian Russell
| starring       = Sammi Davis
| music          = Carl Davis
| cinematography = Billy Williams
| editing        = Peter Davies
| distributor    = 
| released       =  
| runtime        = 113 minutes
| country        = United Kingdom 
| language       = English
| budget         = 
}}
 of the an homonymous film by Russell in 1969.

Sammi Davis stars as Ursula, a sheltered young pupil, then schoolteacher, whos taken under the wing (sexually and otherwise) by the more sophisticated Winifred (Amanda Donohoe). Glenda Jackson appears as the mother of the character she played in Women in Love.

Leonard Maltin commented that "Many beautiful and striking moments dont quite gel, but still worth watching". The film was entered into the 16th Moscow International Film Festival.   

==Cast==
* Sammi Davis as Ursula Brangwen
* Paul McGann as Anton Skrebensky
* Amanda Donohoe as Winifred Inger
* Christopher Gable as Will Brangwen
* David Hemmings as Uncle Henry
* Glenda Jackson as Anna Brangwen
* Dudley Sutton as MacAllister Jim Carter as Mr. Harby
* Judith Paris as Miss Harby
* Kenneth Colley as Mr. Brunt
* Glenda McKay as Gudrun Brangwen Mark Owen as Jim Richards
* Ralph Nossek as Vicar
* Nicola Stephenson as Ethel
* Molly Russell as Molly Brangwen
* Alan Edmondson as Billy Brangwen
* Rupert Russell as Rupert Brangwen
* Richard Platt as Chauffeur
* Bernard Latham as Uncle Alfred
* John Tams as Uncle Frank
* Zoe Brown as Ursula (aged 3)
* Amy Evans as Baby Gudrun
* Sam McMullen as Winifreds Baby

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 