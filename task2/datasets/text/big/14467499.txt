Ek Musafir Ek Hasina
{{Infobox film
| name           = Ek Musafir Ek Hasinaa 
| image          = Ek Musafir Ek Hasina.jpg
| caption        = DVD Cover
| director       = Raj Khosla
| producer       = Sashadhar Mukherjee
| writer         = Raj Khosla  Sadhana  Rajendra Nath
| music          = O.P. Nayyar
| cinematography = Fali Mistry
| editing        = S. E. Chandiwale
| released       =  
| country        = India Hindi
| gross          =  2,50,00,000   
}}
 1962 Bollywood Sadhana and Rajendra Nath. The films music is by O.P. Nayyar.  

The film became a "superhit," taking a second top spot at the box office in 1962.   

The director   (1964), Mera Saaya (1966) and Anita (1967 film)|Anita (1967).

==Plot==
Ajay Mehra (Joy Mukherjee) is on a secret mission to Kashmir to counter Kashmir rebels right after Independence of India|Independence. While on mission he is injured due to a bomb blast. A young girl Asha (Sadhana Shivdasani|Sadhana) from a poor family is forced to flee for her own safety following an attack on her house by Kashmir rebels. She comes across Ajay who is injured. She nurses him back to health over a period of time and they start falling in love with each other. But after the bomb blast, Ajay has lost his memory. Hence they both decide to go to Srinagar to get treatment for him at a hospital. But Ajay gets some clue from his things and decides to go to Bombay to find the truth about himself. 

In Bombay while searching some more clues Ajay sees bank robbers fleeing after robbing a bank. He tries to stop them and is hit by their car. In the process he recognises the robbers, to be from Continental Hotel, where he was looking for clues. He is then taken to a hospital where his brother comes to meet him. Due to this accident he starts regaining some parts of his memory. But he has completely forgotten about the last six months. 

The robbers decide to kill Ajay as he has recognised them. They send a group members wife to Ajays house and there she claims to be his wife. Looking for Ajay, Asha also reaches his home. But Ajay fails to recognise her. Finding it suspicious, Police keep an eye on both women. The robbers try to kill Ajay several times but are unsuccessful as he comes to know about their plans. The police and Ajay draw up a plan to fool the robbers. They fake Ajays death and, relieved by that, the robbers stop hiding and are then caught by police. In the last fight Ajay again loses consciousness but regains his full memory later on. Ajay and Asha are then united indicating a happy ending.

==Cast==
* Joy Mukherjee as Ajay Mehra
* Sadhana Shivdasani as Asha
* Rajendra Nath Dhumal
* Malka
* Jagdish Raj
* Sujit Kumar
* Kamal Kapoor

==Music== Kedar raga. 
{{Track listing
| extra_column = Singer(s)
| lyrics_credits  = yes
| title1 = Bahut Shukriya Badi Meherbani | extra1 = Mohammed Rafi, Asha Bhosle | lyrics1 = S. H. Bihari | length1 = 05:17
| title2 = Aap Yun Hi Agar Humse Milte Rahe | extra2 = Mohammed Rafi, Asha Bhosle| lyrics2 = Raja Mehdi Ali Khan| length2 = 04:13
| title3 = Main Pyar Ka Rahi Hoon | extra3 = Mohammed Rafi, Asha Bhosle | lyrics3 = Raja Mehdi Ali Khan| length3 = 03:10
| title4 = Mujhe Dekhkar Aapka Muskurana | extra4 = Mohammed Rafi| lyrics4 = S. H. Bihari | length4 = 03:07
| title5 = Humko Tumhare Ishq Ne | extra5 = Mohammed Rafi| lyrics5 = Shewan Rizvi | length5 = 04:22
| title6 = Phir Tere Shehar Mein | extra6 = Mohammed Rafi | lyrics6 = Shewan Rizvi | length6 = 04:28
| title7 = Zaban-e-yaar Man Turki | extra7 = Mohammed Rafi, Asha Bhosle | lyrics7 = Shewan Rizvi | length7 = 07:20
| title8 = Udhar Woh Chal Chalte Hain, | extra8 = Asha Bhosle | lyrics8= Shewan Rizvi | length8= 04:07
| title9 = Tumhein Mohabbat hai humse mana, | extra9 = Asha Bhosle,Mohammed Rafi | lyrics9= Shewan Rizvi | length9= 05:17
| title10 = Meri najare haseen, | extra10 = Asha Bhosle,| lyrics10= Shewan Rizvi | length10= 05:28
}}

==References==
 

== External links ==
*  

 
 
 
 