The Slammin' Salmon
{{Infobox film
| name           = The Slammin Salmon
| image          = Slammin salmon poster.jpg
| caption        = Theatrical release poster Kevin Heffernan   
| producer       = Peter Lengyel Richard Perello
| writer         = Broken Lizard
| starring       = Michael Clarke Duncan Jay Chandrasekhar Kevin Heffernan Steve Lemme Paul Soter Erik Stolhanske Cobie Smulders  April Bowlby  Olivia Munn
| music          = Nathan Barr
| cinematography = Robert Barocci
| editing        = Brad Katz
| distributor    = Anchor Bay Films
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}} Kevin Heffernan directed the film; it was his first time directing a Broken Lizard film.   Salmon was filmed in 25 days at the beginning of 2008.

==Plot==
"Slammin" Cleon Salmon (Michael Clarke Duncan) is a former world heavyweight boxing champion who retired to open a sports-themed restaurant in Miami. His antics lead him to believe that he owes $20,000 to the head of the Japanese Yakuza. Needing to come up with the money in one night, he challenges the wait staff to sell more food than they have ever sold, with the top waiter receiving $10,000 in cash and the lowest waiter getting a "broken ribs sandwich" courtesy of the champ. Zany hijinks ensue as the staff try to one-up each other and win the prize while avoiding a beating.  In the end, Cleon realizes that he only owed 20,000 Yen to the Japanese Yakuza (which works out to be $170) and shares the takings with the wait staff. But before he goes to give the Japanese Yakuza the money, he beats up Guy since he was the lowest selling waiter.

==Cast==
Staff
*Michael Clarke Duncan - Cleon "Slammin" Salmon, the owner of the restaurant
*Jay Chandrasekhar - Nuts/Zongo Kevin Heffernan- Rich
*Steve Lemme - Connor
*Paul Soter - Donnie Kinogie/Dave Kinogie
*Erik Stolhanske - Guy "Meat-drapes" Metdrapedes
*Cobie Smulders - Tara
*April Bowlby - Mia
*Nat Faxon - Carl the Manager

Customers
*Will Forte - Horace ("The Lone Diner")
*Lance Henriksen - Dick Lobo
*Olivia Munn - Samara Dubois
*Vivica A. Fox - Nutella
*Morgan Fairchild - Morgan Fairchild
*Sendhil Ramamurthy - Marlon Specter
*Angel Oquendo - Hispanic Customer
*Jeff Chase - Anthony
*Carla Gallo - Stacy
*Rosalie Ward - Merlot Customer
*Jim Gaffigan - Stanley Bellin
*Bobbi Sue Luther - Cod Customer #1
*J.D. Walsh (actor)|J. D. Walsh - Cod Customer #3
*Smith Cho - Translator
*Jim Rash - Disgruntled Businessman
*Gillian Vigman - The Escort Michael Weaver - The John
*Philippe Brenninkmeyer - King Crab Customer
*Peter "Navy" Tuiasosopo - Miami Dolphin #1
*Marc Evan Jackson - Dry Sac Customer

== Release ==
The film premiered at the Slamdance Film Festival on January 17, 2009.  It was released to limited theaters in the US on December 11, 2009.  On April 13, 2010, the film was released on DVD and Blu-ray.

==Reception==
The film has received mostly negative reviews. Rotten Tomatoes reports that 36% of critics gave the film a positive review, based upon a sample of 25, with an average score of 4.9 out of 10.  Neil Genzlinger of the New York Times praised Duncans performance, but noted that many of the jokes in the movie are tired and old.  Michael OSullivan, writing for the Washington Post, noted that the film engages in "lowbrow insults and slapsticky shenanigans" and that its humor "hovers around crotch level." 

==References==
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 