The Village Squire
{{Infobox film
| name           = The Village Squire
| image          = "The_Village_Squire"_(1935).jpg
| image_size     =
| caption        =
| director       = Reginald Denham 
| producer       = Anthony Havelock-Allan 
| writer         = Arthur Jarvis Black (play)   Sherard Powell
| narrator       = David Horne   Leslie Perrins   Moira Lynd   Vivien Leigh.
| music          = 
| cinematography = Francis Carver
| editing        = Cecil H. Williamson
| studio         = British & Dominions Film Corporation
| distributor    = Paramount British Pictures
| released       = April 1935
| runtime        = 66 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} British comedy David Horne, Leslie Perrins, Moira Lynd and Vivien Leigh.  It was based on a play by Arthur Jarvis Black. A villages amateur production of MacBeth is aided by the arrival of a Hollywood star. This provokes the fierce resistance of the village squire who hates films. 
 Paramount to help them meet their yearly quota set down by the British government. Today the film is best known for marking the debut of Vivien Leigh.

==Cast== David Horne - Squire Hollis
* Leslie Perrins - Richard Venables
* Moira Lynd - Mary Hollis
* Vivien Leigh - Rose Venables
* Margaret Watson - Aunt Caroline
* Haddon Mason - Doctor Blake
* Ivor Barnard - Mr Worsford

==References==
 

==Bibliography==
* Chibnall, Steve. Quota Quickies: The Birth of the British B film. British Film Institute, 2007.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 