The Wraith
 
{{ Infobox film |
| name = The Wraith
| image = The-wraith-poster.jpg Theatrical release poster
| director = Mike Marvin
| writer = Mike Marvin
| producer = John Kemeny Charles "Charlie" Sheen Sherilyn Fenn Nick Cassavetes Randy Quaid Reed Smoot
| editing = Scott Conrad Gary Rocklen
| music = Michael Hoenig J. Peter Robinson
| awards =
| budget = $2.7 million
| gross = $3.5 million ($1,402,535 US)
| released =  
| studio = New Century Entertainment Corporation Alliance Entertainment Turbo Productions
| distributor = New Century Vista Film Company
| runtime = 93 min. English
| country = United States
}} written and directed by Charles "Charlie" Sheen, Sherilyn Fenn, Nick Cassavetes, and Randy Quaid.   Dread Central, March 17, 2010.  The film was released theatrically on 288 screens in the U.S. by New Century Vista Film Company (later New Century Entertainment Corporation).
 wraith intent on taking revenge on a gang of car thieves and their psychotic leader, who murdered the teen so the leader could then exert emotional control over the dead teens girlfriend using intimidation. 

==Plot== Dodge M4S Turbo Interceptor, driven by a black-clad and helmeted figure.
 pink slips, cheating them in order to win; he terrorizes and controls everyone through fear and intimidation, including his object of obsession Keri Johnson (Sherilyn Fenn), whom he views as his property. Keris boyfriend James "Jamie" Hankins (Christopher Bradley) was mysteriously murdered, leaving no trace; Keri, who was with him, was hospitalized with no memory of the traumatic event. 
 Honda XL350R Enduro dirt bike. He stops and asks Keri Johnson for directions; for reasons Keri doesnt yet understand she is attracted to the stranger. Jake soon befriends both Keri and Jamies brother William "Billy" Hankins (Matthew Barry), who works at Big Kays the local burger drive-in; they later meet up at a sun-and-swim gathering on a local river (while there, Jake is seen to have multiple scars on his neck and back).

Packards control of the illegal races is suddenly over when the sleek, all black Turbo Interceptor and its phantom driver (referred to as "The Wraith") suddenly appear out of nowhere. The mysterious driver of this supercar is covered head-to-toe in futuristic black body armor and a black race helmet. The armor is adorned with bright metal braces resembling those worn by victims recovering from severe physical trauma. The car and driver challenges Packards gang to race, explosively killing them one at a time in high-speed, fiery crashes, which leaves their bodies untouched except for burned-out eye sockets: First to race is Oggie Fisher (Griffin ONeal) and later Minty (Chris Nash). Skank (David Sherrill) and Gutterboy (Jamie Bozian) are obliterated when the Wraith drives his supercar at high speed through the gangs isolated, warehouse garage, causing a huge explosion. Sheriff Loomis (Randy Quaid) and his lawmen are always close behind and in hot pursuit during each deadly encounter, but the sleek Turbo Interceptor and its phantom driver vanish in an electric cloud of glowing light streaks right after the violent death of each gang member.

With Packards gang having been destroyed by the Wraith, Rughead (Clint Howard), the gangs tech-geek, who alone among them did not participate in Jamies murder, realizes that his gang was targeted because of that murder. Rughead then talks over all the recent strange events with Sheriff Loomis, the Sheriff learning from him that Packard murdered Jamie Hankins, Keris boyfriend; Packard coveted Keri strictly for himself. Loomis realizes he cannot arrest Packard due to the disappearance of Jamies body. 

Keri becomes suspicious of who Jake actually is, and after she confronts him, he tells her to ask Packard, explaining that he knows why Jake is here; Jake also tells Keri he has come a long way for her and that "my time is just about over".

Packard, now the sole survivor of his gang, forces Keri at Big Kays to leave town with him. When Billy attempts to intervene, Packard savagely beats him and then kidnaps Keri, telling her they are going to California. When she confronts him about murdering Jamie, he admits to the killing and stops his Corvette; he pulls out a switchblade just as the Turbo Interceptor pulls up. Packard then challenges its phantom driver to a final road race. Sheriff Loomis and his lawmen are again in hot pursuit, but the racers are soon far ahead; the supercar suddenly reverses direction, and Packard dies horribly in the fiery head-on collision and explosion that follows; his dead body is left intact, just like his dead gang members. With this destructive outcome, a frustrated Sheriff Loomis is forced to abandon his investigation. He observes "You cant stop something that cant be stopped", which leaves him with no rational answers for these violent events.

As Keri arrives home that night, the Turbo Interceptor pulls up, and the all black armored driver emerges, enveloped within flashing bolts of bright light; the figure transforms into Jake. Keri realizes that Jake is actually a returned version of her dead boyfriend Jamie, who then says "this is as close I could come to who I once was". He then asks her to wait for him because he has one last thing to do.

Jake drives the Turbo Interceptor to Big Kays, startling his brother Billy. Jake hands over the keys to the supercar, noting "Its the only one in existence; it does very special things". He then tells Billy that his work here is finished. Billy asks, "Who are you, bro"? To this, Jake wryly replies, "You said it, Billy ... the instructions are in the glove compartment". As Jake rides off on his dirt bike, Billy comes to the grief-stricken realization that Jake is somehow his returned older brother, Jamie.

Jake picks up Keri, who is now being watched from a distance by Sheriff Loomis, and together they ride off into the night, leaving the desert city and the past behind them forever.

==Cast==
 
* Charlie Sheen as Jake Kesey / The Wraith
* Matthew Barry as William "Billy" Hankins
* Sherilyn Fenn as Keri Johnson
* Randy Quaid as Sheriff Loomis
* Clint Howard as Rughead
* Nick Cassavetes as Packard Walsh
* David Sherrill as Skank
* Jamie Bozian as Gutterboy
* Griffin ONeal as Oggie Fisher
* Chris Nash as Minty
* Christopher Bradley as James "Jamie" Hankins
 

==Production==
The Wraith is dedicated to the memory of Bruce Ingram, a camera operator who died during the filming of one of the car chases; another crew member was seriously injured. According to supplementary material on the DVD; the camera car was overloaded and overturned while traveling at high speed. 

===Shooting locations===
The Wraith was shot entirely in and around Tucson, Arizona; shots of the hilly road leading into the fictional "Brooks, AZ" were filmed on Freeman Road on the citys south side. Keris (Sherilyn Fenn) home is located at 2128 East 5th Street; "Big Kays Burgers" was a set built especially for the film at 2755 East Benson Highway and no longer exists.  

Sheriff Loomis (Randy Quaid) goes to talk to Skank (David Sherrill) and Gutterboy (Jamie Bozian) at the Davis-Monthan Air Force Base. The films swimming hole is located in Sabino Canyon, off North Upper Sabino Canyon Road. The curvy mountain road where Packard (Nick Cassavetes) and his gang challenge other cars to deadly races is the General Hitchcock/Catalina/Mount Lemmon Highway that winds through natural stone monoliths north of the city. Skank and Gutterboy chase after Jamie and Keri down North 4th Avenue at East 7th Street. The portion of the chase that leads into a tunnel is the since-redone tunnel on North 4th Avenue, where it crosses under railroad tracks; Jake and Keri are seen riding down the road through Sabino Canyon Recreation Area (near Sabino Lake Dam) northeast of Tucson.

===Turbo Interceptor===
The Dodge M4S Turbo Interceptor seen in the film was a pace car built by   in Auburn Hills, MI. 

===Other cars used=== GMC pickup truck. Billy Hankins drives a Triumph TR6. The unnamed couple, who are cheated out of their car at the beginning of the film, drive a 1987 Dodge Daytona Shelby Z (distinguished by its pop-up headlights, unlike the exposed headlights of Oggies 1986 model). The police drive a variety of mid-1980s Plymouth Caravelles and Plymouth Gran Furys, as well as early-1980s Chevrolet Malibus. Sheriff Loomis drives a "civilian" Plymouth Caravelle. 

==Soundtrack== Rick Hart NED Synclavier#Synclavier Synclavier II

Many famous 1980s rock music hits are included on the films soundtrack:
* Tim Feehan – "Wheres the Fire"
* Ozzy Osbourne – "Secret Loser"
* Stan Bush – "Hearts vs. Heads" Ian Hunter – "Wake Up Call"
* Mötley Crüe – "Smokin in the Boys Room" Robert Palmer Addicted to Love"
* Nick Gilder – "Scream of Angels" Lion – "Power Love"
* Honeymoon Suite – "Those Were the Days" Lion – "Never Surrender"
* Bonnie Tyler – "Matter of the Heart"
* LaMarca – "Hold on Blue Eyes" Rebel Yell"
* Jill Michaels – "Young Love, Hot Love" James House – "Bad Mistake"

==Reception==
Film historian and critic Leonard Maltin dismissed the film, as, "... for those who favor fast cars and lots of noise." In the Time Out review, editor John Pym saw The Wraith having "comic-strip killer car thieves" with "... the best joke having one of the thugs knowing the word "wraith"." 

At the film aggregate website Rotten Tomatoes, the film has received a 27% positive rating based on only 11 reviews submitted.

Following its theatrical run, the film was featured on television in an episode of Cinema Insomnia.   Cinema Insomnia.  
==Video releases==
In 1987 the film was released to VHS video by Lightning Video and then on LaserDisc by Image Entertainment; it was then released in 2003 on DVD by Platinum Disc Corporation (now Echo Bridge Home Entertainment). In spite of having no special features and only being available in the pan-and-scan format, there is missing footage on the original VHS and LaserDisc releases. LionsGate released a widescreen Special Edition DVD on March 2, 2010, which includes this footage. 

==See also==
* List of American films of 1986
* List of film accidents

==References==
Notes
 
Bibliography
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* Pym, John, ed. "The Wraith." Time Out Film Guide. London: Time Out Guides Limited, 2004. ISBN 978-0-14101-354-1.
 

==External links==
*  
*  
*  
*  
*  
*  
*   – Official Restoration website (loads slowly)
*  
*   – The Wraith Fan Site (Details About DVD/VHS Releases, Photos, Soundtrack and more!)
*   – The Wraith Music Fan Site



 
 
 
 
 
 
 
 
 
 
 
 