Kanne Pappa
{{Infobox film
| name           = Kanne Pappa
| image          =
| image_size     =
| caption        =
| director       = P. Madhavan
| producer       = 
| writer         = Balamurugan (dialogues)
| story          = Balamurugan
| screenplay     =
| starring       = R. Muthuraman K. R. Vijaya Baby Rani J. P. Chandrababu
| music          = M. S. Viswanathan
| cinematography = P. N. Sundaram
| editing        = R. Devarajan
| studio         = Muthuvel Movies
| distributor    = Muthuvel Movies
| released       =  
| country        = India Tamil
}}
 1969 Cinema Indian Tamil Tamil film,  directed by P. Madhavan. The film stars R. Muthuraman, K. R. Vijaya, Baby Rani and J. P. Chandrababu in lead roles. The film had musical score by M. S. Viswanathan.  

==Cast==
 
*R. Muthuraman as Bhaskar
*K. R. Vijaya as Kalyani
*Baby Rani as Lakshmi
*J. P. Chandrababu
*C. R. Vijayakumari
*Thengai Srinivasan
*M. N. Nambiar Manorama
*V. K. Ramasamy (actor)|V. K. Ramasamy
*Major Sundarrajan
*C. K. Saraswathi
*Kannan
*Pakkirisami
*Ramamoorthy
*Suruli Rajan
*Kathar
*K. P. Ramakrishnan
*Ponnandi
*N. G. P. Sarathi
*Balaiah
*Shanmugam
*Sukirtharaj
*Dhashinamoorthy
*Raju
*M. K. Kamatchi Nathan
*S. Kolappan
 

==Soundtrack==
The music was composed by M. S. Viswanathan.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Susheela || Kannadasan || 03.17 
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 