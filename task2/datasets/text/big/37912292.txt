La morte non conta i dollari
{{Infobox film
 | name = La morte non conta i dollari
 | image = La morte non conta i dollari.jpg
 | caption =
 | director = Riccardo Freda
 | writer = Luigi Masini, Riccardo Freda 
 | music = Nora Orlandi, Robby Poitevin   
 | cinematography =   Gábor Pogány
 | editing =  
 | producer = Cinecidi (Italy)
 | distributor =
 | released = 21 July 1967 (Italy) 
 | runtime = 93 minutes
 | awards =
 | country = Italy
 | language =
 | budget =
 }} 1967 Cinema Italian Spaghetti spaghetti westerns film directed by Riccardo Freda. 

It represents the only western film directed by Freda, here credited as "George Lincoln" in polemic with the production,which had cut some violent sequences from the film.   

== Cast ==
*Mark Damon: Lawrence White
*Luciana Gilli: Jane
*Stephen Forsyth: Harry Boyd
*Pamela Tudor: Lisbeth
*Luciano Pigozzi: giudice Warren
*Nello Pazzafini: Doc Lester
*Ignazio Spalla: Pedro Rodriguez  Lydia Biondi 
*Renato Chiantoni 
*Dino Strano
==Plot==
Lawrence White returns to his hometown and reopens the investigation about the murder of his father. At the same time arrives Boyd, who frequently gets into fights. Doc Lester, who is the one behind the murder, hires Boyd and makes him the sheriff to stop White. However, it turns out that "Boyd" is the real Lawrence White, while ”White” is a friend of his.

Rustlers kill Major White, and a witness who reports to the sheriff is later shot and has his tongue cut out.
Many years later the son of major White returns in a stagecoach. It is stopped by some military, but another passenger, Boyd, discloses them as false, and they ride off.
When White arrives people say that this will mean trouble for major Lester. White now tries to reopen the murder case of his father and gets into a fight with Lesters men. White’s sister, on the other hand, advocates vengeance. 

Boyd gets into several fights and is recruited by Lester, who has him appointed the new sheriff when the old one is killed. Elisabeth, a girl he has acquainted, now rejects him and asks him to find the murderer of her father, who has been killed by men of Lester, because he intended to help White.

When he visits the widow of the former land officer, White is caught in a trap by Lesters men. She explains that they would have tortured her otherwise. Lester sends Boyd to make White confess to murder, but instead he unties White. Some Lester men now appear saying that Lester did right to check him. They start beating Boyd when the widow appears with a rifle. She is shot but Boyd gets a gun and kills them. We now learn that Boyd is the real son of major White, while "White" is his friend, a federal lawyer from Washington.

White follows a lead and finds judge Warren’s daughter as a prostitute in Mexico. He now can convince the judge top reopen the case.

Lesters gang ambush White, but the stage is manned with dolls and explodes, and the rest of the gang is killed in the ensuing gunfight. Lester is arrested.

White stays on as sheriff united with Elisabeth, while his friend leaves with White’s sister.

==Reception==
In his investigation of narrative structures in Spaghetti Western films, Fridlund ranges White with his false identity and double play in La morte non conta i dollari among the stories of infiltrators with hidden agendas that took their inspiration from A Fistful of Dollars. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 