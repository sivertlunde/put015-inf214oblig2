Loyalties (1987 film)
{{Infobox film
| name           = Loyalties
| image          = Loyaltiesfilm.jpg
| image_size     =
| caption        = Movie poster
| director       = Anne Wheeler William Johnston Ronald Lillie
| writer         = Sharon Riis Anne Wheeler
| narrator       =
| starring       = Kenneth Welsh Susan Wooldridge
| music          = Michael Conway Baker
| cinematography = Vic Sarin
| editing        = Judy Krupanszky
| distributor    = Cinema Group (USA)
| released       = March 21, 1987 (USA)
| runtime        = 98 minutes
| country        = Canada United Kingdom
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} Lac la Biche, Alberta.

==Plot==
Loyaties is a story about the relationships between an upper-class Englishwoman, her husband and their housekeeper, the English couple have relocated from the UK to a remote Northern Alberta town. The climax of the movie reveals the dark secret that had forced their sudden departure from Britain.

==Cast==
*Kenneth Welsh as David Sutton
*Tantoo Cardinal as Rosanne Ladouceur
*Susan Wooldridge as Lily Sutton
*Vera Marin as Beatrice
*Diane Debassige as Leona Tom Jackson as Eddy
*Christopher Barrington-Leigh as Robert Sutton Jeffrey Smith as Nicholas Sutton
*Meredith Rimmer as Naomi Sutton
*Yolanda Cardinal as Lisa
*Dale Willier as Jesse
*Wesley Semenovich as Wayne
*Janet Wright as Audrey Sawchuk
*Don MacKay as Mike Sawchuk
*Paul Whitney as Joe Pilsudski

==Awards== Genie Award for Best Achievement in Costume Design. It was nominated for several other Genie Awards. {{cite web
  | title =Awards for Loyalties
  | publisher =Internet Movie Database
  | url =http://www.imdb.com/title/tt0091443/awards
  | accessdate = December 25, 2007}}
 

==References==
 

==External links==
* 
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 


 