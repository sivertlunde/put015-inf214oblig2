Children of the Revolution (1996 film)
 
 
{{Infobox film
| name = Children of the Revolution
| image =
| caption = Peter Duncan
| producer = Tristram Miall
| writer = Peter Duncan
| starring = Judy Davis Sam Neill F. Murray Abraham Richard Roxburgh
| music = Nigel Westlake Martin McGrath
| editing = Simon Martin
| distributor = Miramax Films
| released =  
| runtime = 99 minutes
| country = Australia
| language = English
| budget =
| gross =   
}} historic black comedy film, deterministic path The Revolution in modern day Australia.  It stars Judy Davis, Geoffrey Rush, Sam Neill, and F. Murray Abraham as Joseph Stalin.

==Plot==
Joan (Judy Davis) is a young Australian communist who goes to the Soviet Union as part of a work study program in the 1950s.  There she catches the eye of Soviet dictator  Joseph Stalin (F. Murray Abraham) and the two sleep together just before Stalin dies.  Returning to Australia, Joan discovers she is pregnant and gives birth to Stalins love child, whom she names Joe (Richard Roxburgh).  Her son has a troubled upbringing, rebelling against both his mothers left wing politics and Australian society in general.  He spends time in jail where he learns about Stalins crimes from a fellow inmate.  Upon release marries Anna (Rachel Griffiths) a police officer who had arrested him.  She is the child of Ukrainian refugees who fled to Australia to escape Stalins purges.  Pledging to go on the straight and narrow, Joe rises to become the head of Australias police union and seizes more and more political power.  Anna learns of Joes true parentage, but keeps this secret from Joe out of love and a conviction that she cannot truly know for certain.  The secret eats at their relationship and Joe resents the secrecy when it is revealed.

==Cast==
* Judy Davis as Joan
* Sam Neill as Nine
* F. Murray Abraham as Joseph Stalin
* Richard Roxburgh as Joe
* Rachel Griffiths as Anna
* Geoffrey Rush as Welch
* Ronald Reagan (archive footage) as Himself
* Joseph Stalin (archive footage) as Himself

==Production==
The film was inspired in part by Peter Duncans grandfather, who was a long-standing member of the Communist Party. He wrote the script to help him get into the Australian Film Television and Radio School and showed it to Tristram Miall after he graduated; the producer loved it and decided to turn it into a film. 


==Critical Reception==

The film currently holds a rating of 80% on Rotten Tomatoes, based on 20 reviews, with an average rating of 6.6/10.  The website Metacritic gave the film a score of 76/100,    and at IMDb the film has a 6.3/10 rating. 

==See also==
* Cinema of Australia

==References==
 

==External links==
*  
*  
*  
*  
*  at  

 
 
 
 
 
 
 
 
 
 
 