Pattanathil Bhootham
 

{{Infobox film
| name            = Pattanathil Bhootham
| image           = Pattanathil Bhootham poster.jpg
| caption         = Film poster
| director        = M. V. Raman
| producer        = T. Govindarajan
| writer          = Javar Seetharaman
| starring        = Jaishankar   K. R. Vijaya   Vijaya Lalitha   Nagesh   V. K. Ramasamy (actor)|V. K. Ramasamy   Javar Seetharaman
| music           = R. Govardhanam
| cinematography  = H. G. Raju
| editor          = M. V. Raman   R. Bhaskaran
| studio          = Sharada Productions
| distributor     = Venus Pictures
| released        = April 14, 1967
| runtime         = 2 hr 46 min
| country         = India Tamil
}}
 Indian Tamil Tamil fantasy Eastman Color. The Brass Bottle with a romantic comedy twist, written by Javart Javar Seetharaman|Seetharaman. The film stars Jaishankar, K. R. Vijaya and Nagesh in the lead roles while Seetharaman himself played the genie.

== Plot ==
Thangavel (V. K. Ramaswamy (actor)|V. K. Ramasamy) runs a successful business enterprise with his partner Sabapathy (V.S. Raghavan). He has a large urn which he considers bad luck. To get rid of it he donates it as a prize for a competition at his daughter Lathas (K.R. Vijaya) college.

Bhaskar (Jaishankar) lives with his best friend Seenu (Nagesh). He is in love with Latha and they go to the same college. At the competition, Bhaskar wins the urn and back home while cleaning the urn, Seenu and  Bhaskar unwittingly release a Genie imprisoned in the vase for three thousand years.

The name of the genie is Jee-Boom-Baa (The Tamil version of Abracadabra) (Javar Sitaraman), who vows to help the two guys who set it free. It offers them money, cars, mansions to live and takes care of every need of them. Bhaskar who is in love with Latha, seeks the magic powers of Jee-Boom-Ba to impress Lathas dad but things dont go quite as planned.

Sabapathy and his son (K. Balaji) who are running a smuggling racket, realize that something is amiss with Bhaskars sudden ascent. The enemies of Bhaskar seize this opportunity to abduct Latha and a series of dramatic incidents ensues. Will Jee-Boom-Baa come back to help the friends?

== Cast ==
* Jaishankar as Bhaskar
* K. R. Vijaya as Latha
* Nagesh as Seenu
* Vijaya Lalitha
* K. Balaji
* R. S. Manohar
* V. K. Ramasamy (actor)|V. K. Ramasamy as Thangavel
* V. S. Raghavan as Sabapathy
* Javar Seetharaman as Jee Boom Baa

== Soundtrack ==
{{Infobox album
| Name      = Pattanathil Bhootham
| Longtype  = to Pattanathil Bhootham
| Type      = Soundtrack
| Artist    = R. Govardhanam
| Producer  = R. Govardhanam
| Genre     = Film soundtrack
| Length    = 25:31 Tamil
}}

The soundtrack album was composed by R. Govardhanam. The lyrics were penned by Kannadasan.

; Tracklist 
{{track listing
| extra_column  = Singer(s)
| all_lyrics    = Kannadasan
| total_length  = 25:31

| title1     = Kannil Kandathellam
| extra1     = T. M. Soundararajan, P. Susheela
| length1    = 04:10

| title2     = Ithazhlai Virithathu
| extra2     = T. M. Soundararajan, L. R. Eswari
| length2    = 05:58

| title3     = Naan Yaar 
| extra3     = P. Susheela
| length3    = 03:14

| title4     = Andha Sivagami
| extra4     = T. M. Soundararajan, P. Susheela
| length4    = 06:07

| title5     = Ulagathil Sirandhadhu
| extra5     = T. M. Soundararajan, A. L. Raghavan, P. Susheela
| length5    = 06:02
}}

== Reception ==
Film critic and historian Randor Guy wrote that the film will be "remembered for the interesting onscreen narration, good performances by Jaishankar and Nagesh, Vijaya in a swimsuit, and popular songs".  

== Remake == Srinivasan is to play the genie.   

== References ==
 

== External links ==
*  
*  

 
 
 
 