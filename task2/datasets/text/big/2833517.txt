Two Days
{{Infobox film
| name           = Two Days
| image          = Two Days Poster.jpg
| caption        = Theatrical release poster
| director       = Sean McGinly
| producer       = Mohit Ramchandani
| writer         = Sean McGinly   Karl Wiedergott Adam Scott Karl Wiedergott Joshua Leonard Caroline Aaron Graham Beckel
| music          = Alan Ari Lazar
| cinematography = Jens Sturup
| editing        = Tripp Reed
| distributor    = American World Pictures
| released       = 2003
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Two Days is a 2003 drama film written and directed by Sean McGinly about a man (Paul Rudd) who has a film crew documenting the last two days of his life before his planned suicide. During this time, friends, former girlfriends, and his parents try to convince him to stay alive; most of them think he is merely joking or making a plea for attention.

The film also stars Karl Wiedergott, Mackenzie Astin, Marguerite Moreau, Lourdes Benedicto and Donal Logue.

==External links==
 
*  
*  

 
 
 
 
 
 


 