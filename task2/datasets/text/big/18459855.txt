Barood (1998 film)
 
 

{{Infobox film
| name            = Barood
| image           = Barood_(1998_film)_poster.jpg
| image_size      =
| caption         =
| director        = Pramod Chakravorty
| producer        = Pramod Chakravorty Lakshmi Chakravorty(associate producer)
| writer          = Rajeev Kaul Praful Parekh Javed Siddiqui(dialogue)
| narrator        = 
| starring        = Akshay Kumar Raveena Tandon Amrish Puri Rakhi Gulzar Mohnish Behl
| music           = Anand-Milind
| cinematography   =
| editing         = Subhiraaj Pal
| distributor     =
| released        = 7 August 1998
| runtime         = 165 minutes
| country         = India
| language        = Hindi
| budget          =  
| gross           =  
}}
 action Romance romance film released in the year 1998. It stars Akshay Kumar and Raveena Tandon as the lead protagonists. Amrish Puri, Rakhi Gulzar and Mohnish Behl played supporting roles in the film.

== Plot ==

Honest and diligent Police Inspector Jai Sharma suspects Mr. Singhal of being a hardcore criminal don, but is unable to apprehend him due to pressure from his superior officer, Police Commissioner Kalinath Gaur. Singhals only daughter, Neha, is in love with Jai, and this news enrages Singhal as he wants her to marry Sanjay Gaur, the only son of the Commissioner, and he will not rest until he gets Jai killed, and will utilize every possible and available resource and influence to do this, which also includes exposing Gayetris (Jais mom) questionable past.

== Cast ==
* Akshay Kumar ... Jai
* Raveena Tandon ... Neha Singhal
* Amrish Puri ... Singhal
* Rakhi Gulzar ... Jais mom
* Mohnish Behl ... Sanjay
* Ayesha Jhulka ... Guest Appearance Arjun ... Singhals Man

== Music ==
{| class="wikitable"
|-
! #
! Song
! Singer(s)
|-
| 1
| "Ek Ladki Ek Ladka"
| Kumar Sanu, Alka Yagnik
|-
| 2
| "Meri Saanson Mein Garmi" Abhijeet
|-
| 3
| "Mach Gaya Shor" Poornima
|-
| 4
| "Hum To Tujhse Mohabbat"
| Kumar Sanu, Alka Yagnik
|-
| 5
| "Razi Razi Mein Hoon Razi"
| Udit Narayan, Alka Yagnik
|-
| 6
| "Sana San Sannana"
| Abhijeet, Poornima
|}

== External links ==
*  

 
 
 
 


 