Yeh Hai Chakkad Bakkad Bumbe Bo
{{Infobox film
| italic title   = 
| name           = Yeh Hai Chakkad Bakkad Bumbe Bo
| image          = CBBB.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sridhar Rangayan
| producer       = Childrens Film Society, India
| writer         = Vijay Tendulkar Sushma Bakshi
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Kamlesh
| cinematography = Sudhir Palsane  	
| editing        = Jabeen Merchant	 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = India
| language       = Hindi
| budget         = Rs 4 million
| gross          = 
}}
Yeh Hai Chakkad Bakkad Bumbe Bo (also known as The Sensational Six) is a 2003 Indian childrens film directed by  Sridhar Rangayan, and written by Vijay Tendulkar and Sushma Bakshi. The film is based on a short story by Shakuntala Paranjpye and was shot on location over 20 days in Mandangad, Bankot and Mumbai. 

==Plot==
The peace of their small Konkan village is shattered when four adventurous children come upon the dangerous smuggler Don Douglas. Two of the boys are caught spying and taken away by his goons. With the help of their dog Sikander and their monkey Birbal, the rest of the team must rescue them.

==Cast==
*Mona Ambegaonkar
*Aardra Athalye 
*Rahul Joshi	
*Ravindra Mankani as Headmaster
*Anvay Ponkshe
*Tom Alter
*Seema Ponkshe as Fisherwoman
*Brij Bhushan Sahni as Fisherman
*Bakul Thakker		
* 

==Awards==
* 2004 Bronze Remi Award for Best Film at WorldFest-Houston International Film Festival 

==References==
 

 
 
 
 
 
 
 


 
 