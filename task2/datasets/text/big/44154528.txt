Vaashi
{{Infobox film
| name           = Vaashi
| image          =
| caption        =
| director       = MR Joseph
| producer       = MR Joseph
| writer         = MR Joseph Velliman Vijayan (dialogues)
| screenplay     = MR Joseph
| starring       = Nedumudi Venu Mancheri Chandran Sukumaran Unnimary
| music          = Raveendran
| cinematography = C Ramachandra Menon
| editing        = K Sankunni
| studio         = Three Star Creations
| distributor    = Three Star Creations
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, directed by MR Joseph and produced by MR Joseph. The film stars Nedumudi Venu, Mancheri Chandran, Sukumaran and Unnimary in lead roles. The film had musical score by Raveendran.   

==Cast==
*Nedumudi Venu
*Mancheri Chandran
*Sukumaran
*Unnimary Anuradha
*C. I. Paul
*Jalaja
*Sathyakala

==Soundtrack==
The music was composed by Raveendran and lyrics was written by Mankombu Gopalakrishnan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aaraaro poomuthe || Shailaja M Ashok || Mankombu Gopalakrishnan || 
|-
| 2 || Deepam Thilangi || P Jayachandran, Chorus || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 

 