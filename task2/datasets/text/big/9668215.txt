April Maadhathil
{{Infobox film|
| name = April Maadhathil
| image =April_Maadhathil.jpg
| director = S. S. Stanley
| writer = S. S. Stanley Srikanth Sneha Sneha Gayatri Jayaraman Venkat Prabhu Karunas
| producer = V. Gnanavelu V. Jayaprakash
| music = Yuvan Shankar Raja
| cinematography = M. V. Panneerselvam
| editing = G. R. Anil Malnad
| studio = G. J. Cinema
| distributor =
| country = India
| released = 29 November 2002
| runtime = 153 min Tamil
}}
 2002 Cinema Indian Tamil Tamil Romance romantic drama Srikanth and Sneha in the lead roles with Gayatri Jayaraman, Venkat Prabhu, Devan and Karunas among others in the supporting cast. The film, which had music scored by Yuvan Shankar Raja and cinematography handled by M. V. Panneerselvam, released on 29 November 2002.  The film was later dubbed into Hindi as Mr. Rangeela and in Telugu as Vaallidharu and released in 2004.

==Plot==
A story of friendship 8 girls and boys which grows vastley. Smart man named Kathir  who comes from a poor village to a College. His younger brother of Kathir stopped his education for Kathir and works. So Kathir meets a girl named Shwetha, starts a friends. Many guys are interested in her but she finds something different about Kathir. a incident happens with Shwethas family friend named Suresh, but she considered him as a friend. Kathir and Shwetha are in a situation where they are unable to express their love for each other. The friends and then decides to go visit each friends home on their vacation. At Shwetha home Shwethas father thought of Her marriage to occur the next year on their vacation. At graduation swap names on a tree since its their final year. Shwethas wedding occurs but their tree is about to be chopped of because the school decides to cut them off for businessmen. In the Shwetha and Kathir a crying behind the tree they wrote their name. What happens after?

==Cast and crew==

===Cast===
* Srikanth (actor)|Srikanth as Kathir
* Sneha (actress)|Sneha as Shwetha
* Gayatri Jayaraman as Nimmi
* Venkat Prabhu
* Devan (actor)|Devan as Shwethas father
* Karunas as Cycle Jackson
* Kamala Krishnasamy
* Balaji
* Mayilsamy
* Lakshmanan
* Anju Mahendran
* S. P. Balasubramaniam
* Ramakrishnan

===Crew===
* Story, Screenplay, Dialogues, Direction: S. S. Stanley
* Production: V. Gnanaavelu & Jayaprakash|V. Jayaprakaash
* Music: Yuvan Shankar Raja
* Cinematography: M. V. Panneerselvam
* Editing: G. R. Anil Malnad
* Art direction: Naghu R. K.
* Stunts: Peter Hein
* Choreography: Kalyan & Dinesh
* Lyrics: Pazhani Bharathi, Pa. Vijay, Thamarai, Na. Muthukumar & Snehan
* Stills: V. Vijay
* Executive producer: T. V. Sivasankaran

==Production==
S. S. Stanley who had apprenticed with directors Mahendran and Sasi made his directorial debut with this film. It was Srikanths second film after Rojakoottam.

The film was mostly shot at the YMCA College of Physical Education in Chennai, since a large part of the film plays in a campus, filming was held at locations in Chennai, Bangalore, Mysore, Ooty and Vishakapatnam.  

==Release==
After the success, S. S. Stanley and Srikanth again collaborated with films like Kizhakku Kadalkarai Salai and Mercury Pookal which failed to replicate the success of their first collaboration.

===Critical reception===
Hindu wrote:"Stanley who heads   direction, deserves special mention for a very decent handling of romance. 

==Soundtrack==
{{Infobox album|  
| Name = April Maadhathil
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover =
| Released =  6 October 2002 (India)
| Recorded = 2002 Feature film soundtrack
| Length = 27:40
| Label = The Best Audio
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album = Kadhal Samrajyam (2002)
| This album = April Madhathil (2002)
| Next album = Bala (2002 film)|Bala (2002)
}}

The soundtrack, which released on 6 October 2002, was composed by Yuvan Shankar Raja, who himself had sung one of the songs. Opera singer Shekhina Shawn Jazeel sang one song under the name Prasanna.  The soundtrack features 6 songs with lyrics written by five different lyricists, Pazhani Bharathi, Pa. Vijay, Thamarai, Na. Muthukumar and Snehan. The song "Bailomo Bailomo" originally composed for this film was used in Shaam starrer Bala (2002 film)|Bala. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration !! Lyricist!! Notes
|- 1 || Yeh Nenje ||   ||
|- 2 || Azhagaana Aangal || Prasanna || 4:27 || Pazhani Bharathi ||
|- 3 || Kanavugal Pookkum ||   ||
|- 4 || Manasae Manasae ||   ||
|- 5 || Poi Solla Manasukku ||   ||
|- 6 || Sight Adippom ||   ||
|}

==References==
 

==External links==
*  

 
 
 
 
 