The Nutty Professor
 
 
 
{{Infobox film
| name           = The Nutty Professor
| image          = Nutty professor.jpg
| alt            =  poster
| director       = Jerry Lewis
| producer       = Ernest D. Glucksman Arthur P. Schmidt Jerry Lewis
| based on       =   Bill Richmond
| starring       = Jerry Lewis Stella Stevens Del Moore Kathleen Freeman Les Brown and His Band of Renown
| cinematography = W. Wallace Kelley
| editing        = John Woodcock
| distributor    = Paramount Pictures
| released       =  
| runtime        = 107 minutes  
| country        = United States
| language       = English
| gross          = est. $3.5 million (US/ Canada)  1,956,744 admissions (France) 
}} Bill Richmond) and starring Jerry Lewis. The score was composed by Walter Scharf. The film is a parody of Robert Louis Stevensons The Strange Case of Dr. Jekyll and Mr. Hyde|Dr. Jekyll and Mr. Hyde.

In 2004, The Nutty Professor was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".
 remake starring Eddie Murphy and Jada Pinkett-Smith was released, directed by Tom Shadyac.

==Plot==
Professor Julius Kelp, is a nerdy, unkempt, buck-toothed, introverted, accident-prone, socially inept university professor whose experiments in the classroom laboratory are unsuccessful and humorously destructive. When a football-playing bully humiliates and assaults him, Kelp decides to "beef up" by joining a local gym. Kelps failure to succeed in the gym prompts him to invent a serum that turns him into the handsome, extremely smooth, cool, and obnoxious girl-chasing hipster, Buddy Love. 

This new found persona gives him the confidence to pursue one of his students, Stella Purdy. Although she despises Love, she finds herself strangely attracted to him. Buddy wows the crowd with his jazzy, breezy musical delivery and cool demeanor at the Purple Pit, a nightclub where the students hang out. The formula wears off at inopportune times, often to Kelps embarrassment.

Although Kelp knows that his alternate persona is an arrogant person, he cannot prevent himself from continually taking the formula as he enjoys the attention that Love receives. As Buddy performs at the annual student dance the formula starts to wear off. His real identity now revealed, Kelp gives an impassioned speech, admitting his mistakes and seeking forgiveness. Kelp says that the one thing he learned from being someone else is that if you dont like yourself, you cant expect others to like you. Purdy meets Kelp backstage, and confesses that she prefers Kelp over Buddy Love.

Eventually, Kelps formerly henpecked father chooses to market the formula (a copy of which Kelp had sent to his parents home for safekeeping), endorsed by the strait-laced president of the university who proclaims, "Its a gasser!" Kelps father makes a pitch to the chemistry class, and the students all rush forward to buy the new tonic. In the confusion Kelp and Purdy slip out of the class. Armed with a marriage license and two bottles of the formula, they elope.

During the short closing credits, each of the characters come out and bow down to the camera, and when Jerry Lewis, still portraying Kelp, comes out and bows, he trips and goes into the camera, breaking it and causing the picture go black.

==Cast==
* Jerry Lewis as Professor Julius F. Kelp/Buddy Love/Baby Kelp
* Stella Stevens as Ms. Stella Purdy
* Del Moore as Dr. Mortimer S. Warfield
* Kathleen Freeman as Ms. Millie Lemmon
* Howard Morris as Mr. Elmer Kelp
* Elvia Allman as Mrs. Edwina Kelp
* Julie Parrish as College Student
* Milton Frome as Dr. M. Sheppard Leevee

==Production==
The entire production was filmed from October 9-December 17, 1962, mostly on the campus of Arizona State University in Tempe, Arizona.  

The casts costumes were designed by Edith Head.
 Les Brown and his Band of Renown play themselves in the extended senior prom scenes. 
 The Uninvited (1944).

Love instructs the bartender to "mix it nice" and pour it into a tall glass. The bartender asks if he can take a sip; after doing so, he freezes like a statue.  While the drink started as fictional, it is now listed on some cocktail websites.   

===Professor Kelp/Buddy Love character=== The Family Jewels, and 1967s The Big Mouth.

Buddy Love is often interpreted as a lampoon of Lewis former show business partner Dean Martin. Lewis, however, has consistently denied this, saying that the character of Love was based on every obnoxious self-important hateful hipster he ever knew, including in his 1982 autobiography, and in a special documentary produced for the DVD release of the film, entitled The Nutty Professor, Making The Formula. On the DVD commentary Lewis speculates that he perhaps should have made Love more evil — since to his surprise more fan mail came for Love than the professor. Film critic Danny Peary has made the claim in his 1981 book Cult Movies that the character of Love is actually the real counterpart of Jerry Lewis. Lewis has stated that the two represented good and evil. 

The character of Professor Frink from the animated television series The Simpsons loosely borrows much of his mannerisms and technique from Lewiss delivery of the Julius Kelp character, as well as the transition to a "Buddy Love" version of Frink in several episodes. In one episode, the character of Frinks father appears, and was voiced by guest star Lewis.

===Home video===
The DVD of the film contains a long deleted scene in which Kelps love interest is portrayed as a sultry siren whose choreographed, jaw-dropping entrance to the Purple Pit, accompanied by jazz music, is quite a contrast to the final edit in which she is portrayed as a smart but fairly unassuming

==Awards and honors==
American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #99

==Home media==
The Nutty Professor was released on DVD in October 2000. In October 2004, a "Special Edition" was released including an audio commentary by Lewis and Steve Lawrence, a documentary and a short feature. In the commentary, Lewis discusses aspects of production, including his creating a real-time, on-camera monitor, which subsequently became standard in the film industry. He mentions that he recut the film for his own home viewing. He notes places where he would like to redo the scene, for example making the professors watch sound tinny.

==Animated sequel==
An  .

==Musical adaptation==
A musical theatre version premiered at the Tennessee Performing Arts Center in Nashville from July 31-August 19, 2012. The book is by Rupert Holmes, and the score is by Marvin Hamlisch. The production was directed by Jerry Lewis, with choreography by Joann M. Hunter. The cast featured Michael Andrew, Klea Blackhurst, Mark Jacoby and Marissa McGowan. The scenery was by David Gallo, with costume design by Ann Hould-Ward.  

==References==
 
* Jerry Lewis: In Person with Herb Gluck. New York: Atheneum, 1982, ISBN 0-689-11290-4

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 