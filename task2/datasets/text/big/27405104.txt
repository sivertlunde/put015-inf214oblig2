Titanic II (film)
 

 
{{Infobox film
| name           = Titanic II
| image          = Titanic2dvdcover.jpg
| caption        =
| director       = Shane Van Dyke
| producer       = David Michael Latt David Rimawi Paul Bales Tim Ubels
| writer         = Shane Van Dyke
| starring       = Bruce Davison Brooke Burns Shane Van Dyke Marie Westbrook
| music          = 
| cinematography = Alexander Yellen Mark Atkins
| distributor    = The Asylum
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $500,000
| preceded_by    =
| followed_by    =
}}
 Sky in the UK and Ireland on August 9. It was released on August 24 in the United States. The film is set on a fictional reproduction of Titanic that sets off on the same day and route of the originals voyage, but global warming and the forces of nature cause history to repeat itself on the same night, only on a more disastrous and deadly scale.

==Plot==
In the Arctic waters near the Helheim Glacier in Greenland, a person is surfing on waves created by falling chunks of ice that fall off the glacier and into the ocean caused by the effects of global warming. However, a very large chunk of ice falls into the water, creating an especially large wave. The surfer tries to escape from the wave, but it is far too fast for him, and in a matter of seconds, it catches up to him then drowns and kills him instantly.
 RMS Titanic, cruise liner Atlantic crossing, back up near the Glacier, an even larger chunk of ice collapses into the water, creating a tsunami that sends an iceberg crashing into the ship, while the passengers are celebrating in the dining saloon. The entire starboard side of the ship and the starboard lifeboat ramps are crushed; immense pressure is placed on the ships turbines. As people struggle to escape the rising waters and run for the submarine-shaped lifeboats on the port side, the turbines eventually explode, killing many people, including ship captain Will Howard; the explosion also causes an immense fire on the Titanic II, which is sinking by its bow while also listing at a shallow angle to its starboard side.
 lifeboats are destroyed by various pieces of iceberg, killing everyone inside them. With most of the ship flooded, the Titanic II finally sinks.

Having chosen to stay aboard the ship instead of going to the lifeboats, ship owner Hayden Walsh and nurses Amy Maine and Kelly Wade survive to this point. (This was against the orders of Amys father, US Coast Guard Captain James Maine.) Kelly is later killed when a very heavy door crushes her. The ships diving facility only has one oxygen tank, which Hayden gives to Amy. Before sacrificing his life for her, Hayden kisses Amy and with his last words tells her to resuscitate him should he drown before they are rescued. Captain Maine arrives to rescue Amy and Hayden, but Hayden has drowned by then. Amy attempts to save Haydens life in the rescue raft, but she fails. She and an unknown number of injured passengers whom Hayden ordered his helicopter to take (earlier in the film) are the only known survivors of the disaster.

==Cast==
*Shane Van Dyke as Hayden Walsh
*Marie Westbrook as Amy Maine
*Bruce Davison as Captain James Maine
*Brooke Burns as Dr. Kim Patterson
*Michelle Glavan as Kelly Wade
*Josh Roman as Elliot Snipes
*Carey Van Dyke as First Officer Elmer Coolidge
*D. C. Douglas as Captain Will Howard
*Dylan Vox as Second Officer Dwayne Stevens
*Wittly Jourdan as Elijia Stacks

==Production== RMS Queen SS Poseidon The Poseidon Adventure and the original RMS Titanic in the 1979 television miniseries S.O.S. Titanic.

==References==
 

==External links==
*  at The Asylum
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 