The First Grader
 
{{Infobox film
| name           = The First chater mater
| image          = TheFirstGrader2010Poster.jpg
| image_size     = 215px
| alt            =  
| caption        = British release poster
| director       = Justin Chadwick
| producer       = Sam Feuer Richard Harding Nicola Blacker
| writer         = Ann Peacock
| starring       = Oliver Litondo Naomie Harris Tony Kgoroge
| music          = Alex Heffes
| cinematography = Rob Hardy
| editing        = Paul Knight
| studio         = BBC Films UK Film Council
| distributor    = National Geographic Entertainment
| released       =  
| runtime        = 103 minutes
| country        = United Kingdom United States Kenya
| language       = English
| budget         = 
| gross          = $714,722
}} biographical drama film directed by Justin Chadwick, starring Naomie Harris, Oliver Litondo, and Tony Kgoroge, and based on the true story of Kimani Maruge, a Kenyan man who enrolled in elementary education at the age of 84 after the Kenyan government announced universal and free elementary education in 2003. 

==Plot==
Kenya, 2003: A radio DJ announces that the Kenyan government is offering free primary school education to all. Maruge (Oliver Litondo), an 84 year-old villager, hears this and decides he wants to educate himself. Arriving at his local school, with a newspaper clipping about this change in policy, he meets Jane (Naomie Harris), the school’s principal, and expresses his desire to learn. Her colleague Alfred (Alfred Munyua), in an effort to get rid of him, tells him all pupils need two exercise books and a pencil.

The next day, Maruge returns, telling Jane he wants to learn to read. He has a letter from the “Office of the President” that he wants to understand. Exasperated, she tells him the school already has too many pupils and that he needs a uniform. Later that night, she tells her husband Charles (Tony Kgoroge) about Maruge. Cautious of his own position, working alongside the government in Nairobi, he advises her to fight the battles she can win.

After cutting his trousers and turning them into shorts for the school uniform, Maruge returns to the school again. While Jane tells the school inspector Mr. Kipruto (Vusi Kunene) on the telephone that she currently has five children to a desk, when Maruge re-appears, she relents. Alfred is reluctant, yet Jane is defiant, claiming Kipruto is not the head of the school. Allowing Maruge into her class, she seats him near the front – after he admits his eyesight is not so good – and begins to teach him, and her other charges, how to write the alphabet.

Plagued by memories of his time in Kenya in 1953, when he fought with the Mau Mau against the British, it even impacts upon Maruge in class, when Alfred scolds him for not keeping his pencil sharp. Made to sharpen it, he breaks down as he recalls a time when the British tortured him – using a sharp pencil brutally thrust into his ear. Apologising to Jane, saying it won’t happen again, Maruge later educates his fellow pupils, patiently explains about the fight for land that he and other Mau Mau undertook and teaching them the word for ‘freedom’.

Resentment brews over Maruge’s education. At home, people shout that he should stay away from the school, while in the playground, covert photographs are taken of him. Soon enough, the story that an old man is going to school hits the radio airwaves. Kipruto arrives, furious that he has learnt in the press that Maruge is attending his school. Jane tells him that Maruge fought against the British. She later learns from Maruge that the same soldiers killed his family.

Desperate to keep Maruge in school, Jane calls Charles, but he advises her not to go over Kipruto’s head. She wilfully ignores him, visiting the head of the education board to plead Maruge’s case. Her protests fall on deaf ears and Maruge is made to attend an adult education centre, where he soon finds himself surrounded by people with no ambitions to learn. He goes to see Jane, telling her he must learn to read because he wants to be able to understand the letter he’s been sent. Refusing to go back to the adult education centre, Maruge nevertheless must say his goodbyes to the children. Yet Jane offers him a reprieve – as her teaching assistant.

As the story breaks, the press descends on the school, surrounding Jane and wanting to question Maruge. He tells the reporters that the power is in the pen. Nevertheless, his presence in the school is beginning to cause anger amongst the parents of the young pupils. One mother confronts Jane, accusing her of seeking fame and fortune from all the attention, while another father proclaims to Alfred that the school is spending too much time on Maruge. Again, Kipruto arrives with the school in chaos, telling Jane that her special pupil cannot stay and that plans are afoot for the government to compensate the Mau Mau.

Resolute, Jane decides to teach Maruge to read after school has finished – despite receiving threatening phone calls. A delegation of politicians arrive at the school, keen to cash in on the free publicity surrounding Maruge, while secretly demanding that Jane cut them in on any money she has received. Events begin to spiral - people attack the school with sticks while Charles receives an anonymous telephone call, noting his wife is now out of control. Jane soon receives a letter that she is to be transferred to a school 300 miles away. Charles tells her that events surrounding Maruge are tearing them apart, explaining that he’s received calls claiming she has been unfaithful.

Jane explains to Maruge that she is being transferred, and then undertakes an emotional goodbye to the children, who all bring her gifts. Meanwhile, Kipruto introduces the class’ new teacher. Enraged, the children padlock the school gate and throw missiles at her and Kipruto. Meanwhile, Maruge travels to Nairobi, heading to the Ministry of Education, where he confronts the board on behalf of Jane, showing them the scars he sustained as a young man tortured by the British.

Jane returns to the school, where Maruge is there to welcome her back. He wants her to read to him his letter, which explains he will be compensated for his time in the prison camps. As the film draws to a close, the radio DJ announces that Maruge – the Guinness Book of Records holder for the oldest person to go to primary school – will speak at the United Nations.

Producers Sam Feuer and Richard Harding had previously released the short documentary film The First Grader: The True Story of Kimani Nganga Maruge (2006).

==Cast==
*Naomie Harris as Jane Obinchu
*Oliver Litondo as Kimani Maruge
*Tony Kgoroge as Charles Obinchu

==Production== Rift Valley in Kenya, despite earlier reports that it would be filmed in South Africa. Director Justin Chadwick said: "We could have shot it in South Africa, but Kenya has this unbelievable, inexplicable energy - inherent in the children, and the people we were making the film about". 

==Reception==
The First Grader received mixed reviews from critics, currently holding a 59% rating on Rotten Tomatoes. 

==References==
 


==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 