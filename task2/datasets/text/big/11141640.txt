Sathya (film)
 
{{Infobox film
| name           = Sathya
| image          = sathyafilm.jpg
| image_size     =
| caption        = 
| director       = Suresh Krissna
| producer       = Kamal Haasan Chandra Haasan
| writer         = Ananthu (dialogues)
| story          = Javed Akhtar
| narrator       = Amala Rajesh Rajesh Nassar Vaali Raja Kitty
| music          = Ilaiyaraaja
| cinematography = S. M. Anwar
| editing        = N. R. Kittu
| distributor    = Raaj Kamal Films International
| studio         = Raaj Kamal Films International
| released       = 14 January 1988
| runtime        =
| country        = India Tamil
| budget         =
| gross          =  
| preceded_by    =
| followed_by    =
| website        =
}}
 1988 Kollywood|Tamil-language Indian feature directed by Suresh Krissna and produced by Kamal Haasan starring Kamal Haasan himself along with Amala (actress)|Amala. The film was remake from the 1985 Hindi film Arjun (1985 film)|Arjun. The film is the debut directional venture for Suresh Krishna. The film completed a 150-day run at the box office. The cinematography and editing were handled by  S. M. Anwar and N. R. Kittu respectively.

==Plot==
Sathya (Kamal Hassan) is a young man who wants to make a difference and ends up becoming a henchman to a political leader who has his own agenda.

==Cast==
* Kamal Haasan Amala
* Rajesh
* Kitty
* Bahadoor
* Nassar
* Janakaraj
* Vadivukkarasi Vaali
* Anand
* Kaviyoor Ponnamma

==Production== Kitty who played the negative role in the film.

==Soundtrack==
The films music was composed by Ilaiyaraaja and lyrics were penned by Vaali (poet)|Vaali.   Ilaiyaraaja had originally composed the number Valayosai for one of his studio albums, and on Kamal Haasans insistence, included it in Sathya. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers
|-
| 1 || Eley Thamizha || T. Sunderrajan, Saibaba
|-
| 2 || Ingeyum || Lata Mangeshkar
|-
| 3 || Nagaru Nagaru || Lalith Sahari, T. Sunderrajan, Saibaba
|-
| 4 || Potta Padiyudhu || Kamal Haasan, T. Sunderrajan, Saibaba
|-
| 5 || Valaiosai || S. P. Balasubrahmanyam, Lata Mangeshkar
|}

==Themes and influences==
Sathya is basically the story of one man who wants to make a difference and ends up becoming a henchman to a political leader who has his own agenda.  The movie is an effective commentary on the sorry state of such unemployed youngsters and the resulting unwilling choices they have to make to earn money. It clearly shows us how the youth, in their time of need, become willing toys in the hands of the people with the money to hire them.  Kamals characterisation was similar to his previous film Varumayin Niram Sivappu (1980). Kamals character is of a person who hails from a middle-clas family, he is unemployed, has rough attitude and cannot tolerate injustice. 

==Legacy==
The character name of Ajith from Yennai Arindhaal is called as Sathyadev. The director Gautham Menon initially wanted to keep this title for the film as he has affinity towards Kamal and the film but later opted against to do so.   Arun Vijay starrer Jananam (2004) was compared with "Sathya".  Prior to the release of Velaiyilla Pattathari (2014), there were concerns raised that the film was similar to the Kamal Haasan starrer, Sathya. Dhanush clarified that there were no similarities between the films, nor was it similar to another Haasan film, Varumayin Niram Sivappu, except for the fact that the protagonist was unemployed.  Entertainment portal Behindwoods placed this film along with Nayakan (1987 film)|Nayakan (1987) in Top 10 Gangsters of Tamil cinema. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 