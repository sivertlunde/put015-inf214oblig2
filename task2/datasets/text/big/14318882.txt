Are Crooks Dishonest?
 
{{Infobox film
| name           = Are Crooks Dishonest?
| image          =
| caption        =
| director       = Gilbert Pratt
| producer       = Hal Roach
| writer         =
| starring       = Harold Lloyd
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 13 minutes
| country        = United States Silent English intertitles
| budget         =
}}
 short comedy film featuring Harold Lloyd. Prints of the film survive in the film archives of The Museum of Modern Art and Filmoteca Espanola.   

==Plot==
Con artists Harold (Harold Lloyd) and Snub (Snub Pollard) try to con the "not easily outwitted" Miss Goulash (Bebe Daniels) and her father.

==Cast==
* Harold Lloyd as Harold
* Bebe Daniels as Miss Goulash
* Snub Pollard as Snub (as Harry Pollard)
* William Blaisdell
* Sammy Brooks
* Lige Conley
* William Gillespie
* Helen Gilmore as Old lady in park
* Lew Harvey
* Gus Leonard as Old man in park Charles Stevenson (as C.E. Stevenson)

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 
*  
*   on YouTube

 
 
 
 
 
 
 
 
 
 