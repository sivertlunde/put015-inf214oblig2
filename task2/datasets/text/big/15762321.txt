Seven Pounds
{{Infobox film
| name           = Seven Pounds
| image          = Seven Pounds poster.jpg
| caption        = Theatrical release poster
| director       = Gabriele Muccino
| producer       = Todd Black Jason Blumenthal James Lassiter Will Smith Steve Tisch
| writer         = Grant Nieporte
| starring       = Will Smith Rosario Dawson Michael Ealy Barry Pepper Octavia Spencer Woody Harrelson
| music          = Angelo Milli
| cinematography = Philippe Le Sourd
| editing        = Hughes Winborne
| studio         = Relativity Media Overbrook Entertainment Escape Artists Columbia Pictures
| distributor    = Sony Pictures Releasing
| released       =  
| runtime        = 123 minutes
| country        = United States
| language       = English
| budget         = $54 million  . The Numbers. 
| gross          = $168,168,201 
}}
Seven Pounds is a 2008 American fantasy drama film, directed by Gabriele Muccino, in which Will Smith stars as a man who sets out to change the lives of seven people. Rosario Dawson, Woody Harrelson, and Barry Pepper also star. The film was released in theaters in the United States and Canada on December 19, 2008, by Columbia Pictures. Despite generally negative reviews from critics it was a box office success, grossing $168,168,201 worldwide. 

==Plot==
 
Tim Thomas ( ).

Two years later, in a conscious bid for atonement, unable to live with what he did, Tim sets out to save the lives of seven good people by donating his own vital organs, a process that will be completed after his planned suicide.  A year after the crash, having quit his job as an aeronautical engineer, Tim donates a lung lobe to his brother Ben (Michael Ealy), an IRS field agent. He steals his brothers federal IRS identification badge and credentials, puts his picture over Bens, identifies himself by his brothers name, and uses Bens privileges to check out the financial backgrounds of further potential candidates for his donations. In each case he "interviews" them first to determine if they are "good" people.

In one case, the director of a hospice nursing home facility seeks a six-month extension on his back taxes. Tim is unsure of the mans ethics, because he drives a BMW yet claims to be insolvent.  To resolve the issue, Tim passionately asks a resident patient, an elderly bedridden woman, to tell him whether he is a "good man," only to discover that the man is punishing the woman for not eating by not allowing the nurses to bathe her.

Six months later, Tim donates part of his liver to a Child Protective Services social worker named Holly (Judyann Elder). He then finds George (Bill Smitrovich), a junior hockey coach and donates a kidney to him.  He then donates bone marrow to a young boy named Nicholas (Quintin Kelley), opting to have no anesthesia during the procedure, an evident consequence of his desire for atonement.  In each case he does not tell the people why hes doing what hes doing, despite being repeatedly asked.

Two weeks before he dies, he contacts Holly and asks if she knows anyone "in the system" who needs and deserves help but is too proud to ask for it. Holly suggests Connie Tepos (Elpidia Carrillo), who lives with her two kids and an abusive boyfriend, but cant afford to leave. When "Ben" arrives to "interview" her under the guise of dealing with the IRS, Connie is embarrassed and humiliated that he knows whats been going on.  She defends her boyfriend, is offended by Tims suggestion that she should leave with her kids, and kicks him out of her house.  In the meantime, Tim moves out of his house and into a local motel, taking with him his pet box jellyfish—decidedly the most venomous creature on earth, with its sting causing death in three to five minutes. That night, after being beaten by her boyfriend again, Connie contacts Tim.  He meets her, tells her not to be weak, and gives her the keys and directions to his beach house. She takes her two children and they move into the house. Then she reads a letter from Tim which includes the deed to the house, again giving no explanation, and asking for her to (1) respect his wishes, (2) not try to contact him, (3) not tell anyone how she got the house, and (4) "live life abundantly."

Tims sixth candidate is Ezra Turner (Woody Harrelson), a blind telemarketer for a meat company, who plays the piano. Tim had called Ezra and harassed him at work weeks earlier, to see if he was quick to anger or was a "good" person. Ezra remains calm, humble, and teary-eyed, and later after observing him in a restaurant, Tim decides he is worthy.
 Heidelberg Windmill press. He visibly fights his affections for her, again seeking atonement for the death of his fiancée, but he slowly falls in love with her.

Ben finally tracks Tim down at Emilys house, demanding that he will return his IRS credentials. After a passionate sexual interlude with Emily, and with Ben waiting outside her house, Tim disappears out the back door, leaving her sleeping. He returns on foot to the motel, first stopping by the hospital to ask the doctor if there is any chance that Emily will improve.  Discovering that her condition has worsened and that she will likely die before a donated organ becomes available, he decides "its time."

Tim then fills the motel bathtub with ice water to preserve his vital organs, calls 9-1-1 emergency and reports his own suicide, climbs into the tub, and releases his box jellyfish into the water with him. The jellyfish wraps its tentacles around his arm, causing a quick but excruciatingly painful death.

At the hospital, his best childhood friend Dan (Barry Pepper), though distraught over promising to see Tims final wishes through, acts as executor of Tims living will to ensure that his organs are donated to Emily and Ezra. Ezra receives Tims corneas, which correct his blindness, and Emily receives his heart.

Afterward, Ben finds letters from Tim that he is to give to each person explaining why he did what he did. This leaves Emily heartbroken.  Emily finds Ezra (now a school teacher) at his kids choir concert at a park and stops him as he passes by. Having never met before, Emily is fixated on Ezras eyes, knowing they belonged to Tim.  Emily begins to break down, which clues Ezra in to who she is.  When he says, "You must be Emily," she breaks into tears and they share a heartfelt embrace out of mutual love and respect for Tim.

==Production==
Seven Pounds is based on a script written by Grant Nieporte under Columbia Pictures. In June 2007, Will Smith joined the studio to star in the planned film and to serve as one of its producers.  In September 2007, director Gabriele Muccino, who worked with Smith on The Pursuit of Happyness (2006), was attached to direct Seven Pounds, bringing along his creative team from the 2006 film.  Smith was joined by Rosario Dawson and Woody Harrelson the following December to star in Seven Pounds. Filming began in February 2008.   
 Sheraton and The Pasadena Ice Skating Rink all in Pasadena, as well as Malibu Beach in Malibu. 

==Cast==
* Will Smith as Tim Thomas 
* Michael Ealy as Ben Thomas, Tims brother and an IRS agent. 
* Barry Pepper as Dan Morris, Tims friend and executor of his will.
* Rosario Dawson as Emily Posa, a self-employed greeting card printer and Tims love interest.
* Octavia Spencer as Kate, Emilys caring nurse.
* Woody Harrelson as Ezra Turner, a blind meat salesman who plays the piano.
* Elpidia Carrillo as Connie Tepos, a woman in a battered relationship with her boyfriend.
* Judyann Elder as Holly, a child services employee.
* Bill Smitrovich as George, a junior hockey coach.
* Quintin Kelly as Nicholas, a child.
* Robinne Lee as Sarah Jenson, Bens fiancee
* Madison Pettis as Connies daughter
Smith described the reason he took on the role:
 

Smith felt that the character needed to be a quiet and rather introverted person who does not burn himself out at every possible instance. The character was a contrast to Smiths previous characters, and Smith felt that director Gabriele Muccinos trust in him helped him relax and avoid overextending himself. Smith acknowledged Seven Pounds as a drama film, but he saw it as more of a love story. 

Will Smith handpicked Ealy for the role of the main characters brother.    Connor Cruise, the adopted son of actor Tom Cruise and actress Nicole Kidman, was cast in his first role as a younger version of Tim Thomas. 

==Title==
Before the films release, the title Seven Pounds was considered a "mystery" which the studio refused to explain. Early trailers for Seven Pounds kept the films details a mystery. Director Gabriele Muccino explained the intent: "The   will not know exactly what this man is up to." Will Smith is reported to have confirmed that the title refers to Shakespeares The Merchant of Venice, in which a debtor must pay a pound of flesh.    In this case, it amounts to seven gifts to seven individuals deemed worthy by Smiths character, to atone for seven deaths he caused.

==Release==
Seven Pounds was promoted on a five-city tour across the United States in November 2008, screening in Cleveland, Miami, Dallas, St. Louis, and Denver to raise funds for food banks in each region.    The film was promoted at a charity screening in Minneapolis in support of Second Harvest Heartland.  Since screenings of new films usually took place in Los Angeles or New York City, the choice of cities was unconventional. Smith said, "This is more like the old-school music tours. Different clubs, different cities, meeting people. You get in touch with what people are feeling and thinking, and its much more personal when youre actually out shaking hands."  The actor sought to "get reacquainted" with an America that he felt had an "openness to change" with the United States presidential election, 2008|countrys election of Barack Obama as the first African-American president. 
 Yes Man. Northeast over the weekend. 

==Home media release==
The film was released on DVD on March 31, 2009, by   in standard or high definition format. 
 , in North American DVD sales, the film has grossed $28,812,423. 

==Critical reception==
<!--

PLEASE MAKE SURE THAT YOU UPDATE THE PERCENTAGE, THE SAMPLE SIZE, THE AVERAGE SCORE, AND THE ACCESSDATE.

--> average score normalized rating out of 100 to reviews from mainstream critics, the film has received a below average score of 36 based on 33 reviews.    

Variety (magazine)|Variety s film reviewer Todd McCarthy predicted that the movies climax "will be emotionally devastating for many viewers, perhaps particularly those with serious religious beliefs," and characterized the film as an "endlessly sentimental fable about sacrifice and redemption that aims only at the heart at the expense of the head."  A. O. Scott, writing for The New York Times, said that the movie "may be among the most transcendently, eye-poppingly, call-your-friend-ranting-in-the-middle-of-the-night-just-to-go-over-it-one-more-time crazily awful motion pictures ever made."   

Positive reviews singled out Dawsons performance. Richard Corliss wrote in Time (magazine)|Time that Dawson gives "a lovely performance,"  while Mick LaSalle of the San Francisco Chronicle noted that Dawsons performance "shows once again that she has it in her to be the powerhouse."  Roger Ebert of the Chicago Sun Times commented on the fact that the audience is kept completely out of the loop as to what Tim is doing, comparing the film to Jean-Pierre Melvilles Le Samouraï, pointing out how he "finds this more interesting than a movie about a man whose nature and objectives are made clear in the first five minutes, in a plot that simply points him straight ahead." 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 