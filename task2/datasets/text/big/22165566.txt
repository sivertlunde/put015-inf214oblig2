Undead or Alive
{{Infobox film
| name           = Undead or Alive: A Zombedy
| image          = Undead or Alive poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical poster
| director       = Glasgow Phillips
| producer       = Deborah Del Prete Gigi Pritzker David Greathouse
| writer         = Glasgow Phillips (screenplay and story) Scott Pourroy (story)
| narrator       = 
| starring       = Chris Kattan James Denton Navi Rawat
| music          = Ivan Koutikov
| cinematography = Thomas L. Callaway
| editing        = Larry Madaras
| studio         = Odd Lot Entertainment
| distributor    = Image Entertainment 
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Western genre. Directed by Glasgow Phillips, and written by Phillips and Scott Pourroy,  the film stars Navi Rawat, Chris Kattan and James Denton. The film was later acquired by Image Entertainment, a deal that included both theatrical and home video rights. 

==Synopsis==
Before his capture, Geronimo had cursed white men, causing people to become zombies. Later, at a house in a small town a man named Ben Goodman (Brian Posehn) acts suspiciously and attacks and eats his wife and daughters brains.

Luke (Chris Kattan), a cowboy living in the town, plans to marry a saloon girl. On arriving at the saloon to propose to her, he encounters Elmer Winslow (James Denton), who has just arrived in town, and discovers that his "princess" is a prostitute. He picks a fight with Elmer until Sheriff Claypool (Matt Besser), who owns the saloon, jails them and steals $500 which Elmer is carrying. In the adjacent cell is Ben, awaiting his hanging the next morning. Elmer and Luke escape and lock Deputy Cletus (Chris Coppola) in the cell, where he is bitten by Ben.

Luke and Elmer find the sheriff and the prostitute having sex. They steal all the sheriffs money and make a getaway. When the sheriff releases Cletus from the cell he is bitten by him. The next morning hang Ben and assemble a posse to pursue the two fugitives.

Luke and Elmer are ambushed in their sleep by Sue (Navi Rawat), Geronimos niece who was kidnapped and taken to New York to be educated and has now returned to take her revenge on the soldiers who killed her uncle. After a difficult start they become friends, despite Elmer revealing that he is an army deserter. Sheriff Claypool and Cletus, now zombies, attack the other members of the posse, who also become zombies. They are ambushed by Elmer, Luke and Sue, who realise they cannot be killed and flee.

Meanwhile, the townspeople cut Ben down from the gallows, not realising that hanging cannot "kill" him. He attacks the townspeople. The towns minister (Leslie Jordan) manages to escape by locking himself in an upstairs room at the saloon, now besieged by a horde of zombies. The following morning he climbs out of the window to get water, but is pursued by zombies and bitten by the prostitute.

Elmer, Luke and Sue are captured by soldiers, former colleagues of Elmers (his $500 is revealed to have been won from them while gambling), who take them to their fort on the edge of the Grand Canyon. There they discover that the posse has arrived first and turned all the garrison into zombies, who attack the party and also turn all the other soldiers into zombies. They discover they can kill the zombies by decapitation.

During the night, while hiding in the fort, Sue reveals that the only way to be cured of the zombie curse is to eat the living flesh of the medicine man who created the curse. They shoot several cookware objects out of a homemade blunderbuss at the sheriff and the zombies to no avail. Elmer tries to punch First Sergeant Kermit in the mouth, but gets bitten by him. After destroying most of the zombies by blowing up the magazine, he becomes a zombie and throws Cletus off the cliff, before biting Luke.

Sue kills Sheriff Claypool, but is then attacked by Elmer and Luke. The next scene shows Elmer and Luke looking normal again, after having eaten Sues flesh. The "eating the living flesh" cure apparently also works for relatives of the person who created the curse. Sue apparently tasted like gingerbread. The duo ride off into the sunset as Cletus stands up and chases them.

An epilogue shows Ben digging up his family, now zombies themselves, from the cemetery and the three of them (with their dog) skipping into town.

==Cast==
 
* Chris Kattan as Luke
* James Denton as Elmer
* Lew Alexander	as Geronimo
* Todd Anderson as Kermit
* Richard Barela as Drunk Zombie
* Matt Besser as Sheriff Claypool
* Brett Brock as Townsman 1
* Richard Bucher as Townsman
* Jeff Chase as Zombie
* Chris Coppola as Cletus
* Gino Crognale	as General Store Owner
* Jeffrey J. Dashnaw as Townsman
* Scott Flick as Zombie Soldier
* Michelle Greathouse as Schoolmarm
* Patricia Greer as Saloon Girl
* Leslie Jordan as Padre
* Geoffrey C. Martin as Trap door zombie
* Keola Melhorn	as Zombie
* Tracy Melhorn	as Zombie
* Michael Patrick Metzdorff as Gunfighter Zombie / Union Soldier Zombie
* Cristin Michele as Kate
* Christopher Allen Nelson as The Captain
* T. Jay OBrien as Farlow
* Brian Posehn as Ben
* Navi Rawat as Sue
* Chloe Russell	as Ruby
* Elizabeth Slagsvol as Saloon Girl
* Russell Solberg as Townsman
* Mia Stallard as Anna
* Ben Zeller as Ebeneezer
* James Blackburn as Zombie soldier
* Brent Lambert	as Zombie
 

==Production==
The film was shot in Santa Fe, New Mexico for Odd Lot Entertainment.  Gary Jones created the make-up and masks for the film. 

==Release==
The film premiered on 15 March 2007 as part of the South by Southwest Festival and was released on 11 December 2007 over Image Entertainment on DVD. 

==Soundtrack==
The score was composed by Bulgarian music producer Ivan Koutikov. 

===Tracklist===
# Hal Ozsan and Poets and Pornstars - Monkey
# Poets and Pornstars - Get your Kicks
# David Baker - Twists and Turns
# Three Bad Jacks - Crazy in the Head
# Slick Pelt - Cowboy Song
# Colin Blades - Best Friend
# Three Bad Jacks - Hellbound Train
# Terence Jay - Undead or Alive
# Slick Pelt - Danger
# Chris Gerolmo - Im your Danger

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 