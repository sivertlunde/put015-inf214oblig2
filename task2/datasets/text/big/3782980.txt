An American Haunting
 
{{Infobox film
| name = An American Haunting
| image = An American Haunting.jpg
| image_size = 220px
| border = yes
| alt = 
| caption = Theatrical release poster
| director = Courtney Solomon
| producer = Courtney Solomon   Allan Zeman
| screenplay = Courtney Solomon
| based on =  
| starring = Donald Sutherland Sissy Spacek James DArcy Rachel Hurd-Wood
| music = Caine Davidson
| cinematography = Adrian Biddle
| editing = Richard Comeau Remstar Productions MediaPro Studios Odeon Films   Freestyle Releasing  
| released =  
| runtime = 90 minutes  
| country = United Kingdom Canada Romania United States
| language = English
| budget = $14 million
| gross = $29,612,137 
}} United Kingdom, United States.

The film is based on the novel The Bell Witch: An American Haunting, by Brent Monahan. The events in the novel are based on the legend of the Bell Witch. The film switches from the 21st century to the 19th, and features a subplot about a recently divorced mother (Susan Almgren) whose daughter (Isabelle Almgren-Doré) is going through something like the same experience as Betsy Bell.

==Plot==

It is modern day and a young girl is running through the forest and runs into her house, trying to get away from something. It turns out it was just a dream that the young girl was having. Her mother comes in to remind her that she is going to go and stay with her father for the weekend. 

Later, the divorced mother finds an old binder of letters from the 19th century. The letters are from a previous occupant of the house. The film switches to the early 19th century, focusing on a village that used to stand around the house, and the story of the Bell Witch is told. 

John Bell is taken to church court, guilty of stealing a womans land. The church lets him go because the loss of his honor is good enough. The offended woman, Kate Batts, who is infamous in the village over claims of witchcraft, hints that she means ill to him and his daughter, Betsy. 

Soon, strange things start happening and John believes that Batts has cursed him. Betsy starts to look very sick and the haunting gets worse. She falls asleep in school, and her attitude towards people changes and she becomes very irritable. Her young teacher, Richard Powell notices the changes in her behavior. When the Bell family tells him of the events going on and that they are being caused by a spirit, he goes on to try to prove to them that that cannot be the case, as spirits are not real. Richard stays in the Bells home, observing Betsys behavior to try to prove to them that it is not supernatural. He is proved wrong, when he witnesses Betsy dangling off of the floor, as if someone is holding her up by her hair. Through the movie, it is implied that Richard is in love with Betsy.
 ghosts as well. He goes insane and asks Batts to kill him but she refuses, telling him he cursed himself. John attempts suicide unsuccessfully.
 sexually abused assault herself. Both had apparently repressed the incident. Betsy poisons her sick bed-ridden father with medicine while her mother watches. Betsy is then seen at her fathers grave, and the narrator says that Betsy was never haunted from that point forward.  

The story then returns to present day, where the mother has been reading the journal. Her daughter comes, saying that her father has come to take her for a weekend stay with him. She sends her daughter to her ex-husband, who is waiting outside. Betsys apparition suddenly appears and cries "Help her!" The mother suddenly realizes that Betsy is trying to warn her that something is amiss between her daughter and her ex-husband. 

She runs out of her house, only to catch a glimpse of her daughters worried face out of the back window of the car as she and her father drive away; it is implied that the father has already started sexually abusing her. The film ends with the mother running after the fathers car.

==Cast==
* Donald Sutherland as John Bell
* Sissy Spacek as Lucy Bell
* James DArcy as Richard Powell
* Rachel Hurd-Wood as Betsy Bell / Entity Voice Matthew Marsh as James Johnston
* Thom Fell as John Bell, Jr.
* Zoe Thorne as Theny Thorn
* Gaye Brown as Kate Batts
* Sam Alexander as Joshua Gardner
* Miquel Brown as Chloe

==Critical reception==
An American Haunting was panned by critics, holding a 38/100 rating on Metacritic, indicating "generally unfavorable reviews".  Rotten Tomatoes reports a 12% rating from 68 reviews; the consensus states: "Well, it looks good. But wasnt it supposed to be scary?" 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 