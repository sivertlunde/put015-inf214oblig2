The Wicked Dreams of Paula Schultz
 
{{Infobox film
| name           = The Wicked Dreams of Paula Schultz
| image          =
| caption        = George Marshall
| producer       = Edward Small
| writer         = Ken Englund Albert E. Lewin
| based on       = story by Ken Englund
| narrator       =
| starring       = Elke Sommer Bob Crane Werner Klemperer
| studio         = Edward Small Productions
| distributor    = United Artists
| released       =  
| runtime        = 113 minutes
| country        = United States
| language       = English
| budget         =
}} American comedy George Marshall East German the West by pole-vaulting over the Berlin Wall. 

==Plot==
Paula Schultz has been preparing to compete in the Olympic Games, but instead pole-vaults over the Berlin Wall to freedom in West Germany.

A black-market operator, Bill Mason, hides her in the home of an old Army buddy, Herb Sweeney, who now works for the CIA. Bill is willing to hand her over for a price, to either side, so a disappointed Paula returns to East Germany with propaganda minster Klaus instead. At this point, Bill comes to his senses, realizes he loves her, then disguises himself as a female athlete to get Paula back.

==Trivia==
Four of the main actors involved in the film (Bob Crane, Werner Klemperer, John Banner, and Leon Askin) also worked together on the set of the popular late 1960s sitcom Hogans Heroes.  Crane, Banner, and Klemperer appeared in every episode, and Askin had a frequent recurring role.

In the film Kill Bill Volume 2, written and directed by Quentin Tarantino, the character played by Uma Thurman is buried alive in a grave marked "Paula Schultz".

==Cast==
* Elke Sommer - Paula Schultz
* Bob Crane - Bill Mason
* Werner Klemperer - Klaus
* Joey Forman - Herbert Sweeney
* John Banner - Weber
* Leon Askin - Oscar
* Maureen Arthur - Barbara Sweeney
* Robert Carricart - Rocco

==Production==
The film was based on an original screenplay by Ken Englund which Edward Small bought in 1966. Elke Signed for Wicked Dreams
Martin, Betty. Los Angeles Times (1923-Current File)   11 Aug 1966: d13.  Henry Tugend was hired to rewrite it. MOVIE CALL SHEET: Liz Signed for Comedians
Martin, Betty. Los Angeles Times (1923-Current File)   04 Oct 1966: c15. 

Bob Crane was given the lead due to his success in Hogans Heroes. New Time Angers Hogans Heroes Star
Gowran, Clay. Chicago Tribune (1963-Current file)   12 June 1967: a10. 

==Reception==
Reviews were poor. Paula Schulz Wicked Dreams Are a Nightmare at Keiths
By William Rice Washington Post Staff Writer. The Washington Post, Times Herald (1959-1973)   15 Feb 1968: E24.  The Wicked Dreams of Paula ...: Lively athleticism
By Alan N. Bunce. The Christian Science Monitor (1908-Current file)   02 Feb 1968: 4.  The Screen: A Teutonic Striptease:  The Wicked Dreams of Paula Schultz Opens Elke Sommer a Victim of the Cold War
By RENATA ADLER. New York Times (1923-Current file)   04 Jan 1968: 28. 

==See also==
* List of films set in Berlin

==References==
 

 
 

 
 
 
 
 
 
 
 
 