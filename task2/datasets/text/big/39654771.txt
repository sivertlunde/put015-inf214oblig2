Kiccha
{{Infobox film
| name       = Kiccha
| image      = Kiccha Kannada Movie.jpg
| director   = Arun Prasad P.A.
| producer   = Ramu
| story      = Arun Prasad P.A.
| screenplay = Arun Prasad P.A.
| starring   =  
| music      = Hamsalekha
| cinematography = Sundarnath Suvarna
| editing    = S. Manohar
| studio     = 
| distributor = 
| released   =  
| country    = India
| language   = Kannada
| runtime    = 154 min
| budget     = 
| gross      = 
| website    =  
}}
 Kannada political-drama film directed by Arun Prasad P.A. featuring Sudeep and Shwetha in the lead roles. The film features background score and soundtrack composed by Hamsalekha and lyrics by Hamsalekha. The film released on 11 April 2003. 

==Cast==
* Sudeep ... Kiccha alias Krishna
* Shwetha ... Suma Sujatha ... Sharada (Kicchas mother)
* Ajay Rao ... Kicchas friend
* Sadhu Kokila ... CD Babu
* Kashi ... 
* Avinash ... Chief Minister
* Vinayak Joshi ... Kicchas friend
* Mico Manju ... Kumar
* Karibasavaiah ... 
* Vaijanath Biradar ... hostel watchman
* Mandya Ramesh ... Umapathi
* M.S. Karanth ... Home Minister
* Ravi Srivatsa ... 
* Bhavyashree Rai ...

==Soundtrack==
{{Infobox album
| Name = Kiccha
| Type = soundtrack
| Artist = Hamsalekha Kannada
| Label = 
| Producer = 
| Cover = 
| Released =  Feature film soundtrack
| Last album = 
| This album = 
| Next album = 
}}   

==Awards==
{| class="infobox" style="width: 22.7em; text-align: left; font-size: 85%; vertical-align: middle; background-color: #eef;"
|-
| colspan="3" |
{| class="collapsible collapsed" width="100%"
! colspan="3" style="background-color: #d9e8ff; text-align: center;" | Awards and nominations
|- style="background-color:#d9e8ff; text-align:center;"
!style="vertical-align: middle;" | Award
| style="background:#cceecc; font-size:8pt;" width="60px" | Wins
| style="background:#eecccc; font-size:8pt;" width="60px" | Nominations
|-
|align="center"|
;Filmfare Awards South
| 
| 
|}
|- style="background-color:#d9e8ff"
| colspan="3" style="text-align:center;" | Totals
|-
|  
| colspan="2" width=50  
|-
|  
| colspan="2" width=50  
|}
Filmfare Awards South :- Best Director - Kannada - Arun Prasad P.A.|P.A. Arun Prasad  Filmfare Awards South 2003:
*  
* {{cite web|url=http://www.chitraloka.com/awards/2917-film-fare-awards-2004-sudeep-on-a-hattrick.html|title=
Film Fare Awards 2004 – Sudeep On A Hattrick|publisher=chitraloka.com|date=5 June 2004   }}
*  
*  
*   

==References==
 

==External links==
*  

 
 
 
 


 