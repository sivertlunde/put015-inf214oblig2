Too Late for Love (film)
 
 
{{Infobox Film
| name           = Too Late for Love
| image          =
| image_size     = 280px
| caption        = 
| director       = Zhen Luo
| producer       = 
| writer         = Zhen Luo
| narrator       = 
| starring       = Ivy Ling Po Kwan Shan Ching Miao Ouyang Sha-fei
| music          = 
| cinematography = 
| editing        = 
| distributor    =  1967
| runtime        = 
| country        = Hong Kong Mandarin
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Too Late for Love (Mandarin: Feng huo wan li qing) is a 1967 contemporary Hong Kong movie written and directed by Zhen Luo.

==Synopsis==

Su Fen, a young and frail girl, looked forward to her wedding with her fiancé, Li Kuo-liang. The happy couples bliss was cut short when war broke out. Kuo-liang was summoned to fight at the front lines. In his absence, Su Fen discovered she had tuberculosis. This did not please her mother-in-law who was looking for a healthy daughter-in-law to bear sons to continue the family name. She forced Su Fen to divorce her son and leave. When Kuo-liang returned on leave, he was angry and in despair for he still loved Su Fen very much. He went to look for her and was prevented from seeing her by her furious father. To his father-in-law, he swore his undying love for Su Fen, vowing never to marry another and left to return to the front lines. Su Fen suffered a lapse after failing to see Kuo-liang in time. 

Months passed and Kuo-liang returned to Su Fens home to see her. However, her father had brought her out on a trip. Incidentally, Su Fen was returning that day and saw him as both their trains left and arrived respectively. At this second failure to meet Kuo-liang, Su Fens illness took a turn for the worse. As the war drew to an end, Kuo-liang took a serious injury to his leg which left him half-crippled. After the war, he visited Su Fen once more.

== Golden Horse Award (1967) ==
 
* Best Actress : Ivy Ling Po
* Best Supporting Actor : Ching Miao
* Best Supporting Actress : Ouyang Sha-fei
* Best Music Score : Wang Fook Ling
* Honorable Mention For Dramatic Feature

== Cast ==

* Ivy Ling Po as Su Fen
* Kwan Shan as Kuo-liang
* Ching Miao as General
* Ouyang Sha-fei as Mdm 

==External links==
* 

 
 
 
 


 