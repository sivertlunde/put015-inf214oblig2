Dry Martini (1928 film)
{{infobox film
| name = Dry Martini
| image =Mary Astor-Albert Conti in Dry Martini.jpg
| image_size =180px
| caption =Mary Astor and Albert Conti in the film
| director = Harry dAbbadie dArrast William Fox
| writer = Douglas Z. Doty
| starring = Mary Astor
| music = Erno Rapee
| cinematography = Conrad Wells
| editing = Frank E. Hull
| distributor = Fox Film Corporation
| released =  
| runtime = 80 minutes
| country = United States
| language = English
}}
 Matt Moore. Samuel L. Rothafel also contributed music for the film. It was adapted from the novel Dry Martini: a Gentleman Turns to Love by John Thomas. Ray Flynn was an assistant director.  

This film is lost film|lost.   

==Plot==

Wealthy divorced American Willoughby Quimby has been living in Paris, France for ten years when he learns his adult daughter Elizabeth is coming to visit. He has been living the high life full of wine and women but decides to forego both during her stay. Elizabeth gets bored with him so she begins seeing rakish artist Paul De Launay. Quimbys young pal Freddie Fletcher saves Elizabeth from the clutches of de Launay in the nick of time. After Elizabeths marriage to Freddie her father returns to his wanton ways.

==Cast==

* Mary Astor&nbsp;– Elizabeth Quimby Matt Moore&nbsp;– Freddie Fletcher
* Sally Eilers&nbsp;– Lucille Grosvenor
* Albert Gran&nbsp;– Willoughby Quimby Albert Conti&nbsp;– Paul De Launay
* Tom Ricketts&nbsp;– Joseph
* Hugh Trevor&nbsp;– Bobbie Duncan
* John Webb Dillon&nbsp;– Frank
* Marcelle Corday&nbsp;– Mrs. Koenig

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 


 