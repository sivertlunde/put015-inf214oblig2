A Strange Transgressor
{{Infobox film
|name= A Strange Transgressor
|image=
|caption=
|director= Reginald Barker
|producer= Thomas H. Ince
|writer= J. G. Hawks
|starring= Louise Glaum J. Barney Sherry   Colin Chase
|music=
|editing= studio = Triangle Distributing
|released=  
|runtime= 50 minutes (5-reel#Motion picture terminology|reels) Silent (English intertitles)
|country= United States
|budget=
}} silent drama film  starring Louise Glaum, J. Barney Sherry, and Colin Chase.
  Directed by produced by adapted by John Lynch.
  distributed by Triangle Distributing.
 
==Plot== kept woman. The man she lives with while facing the scorn of society, famous surgeon Dr. John Hampton (played by Sherry), supports her in lavish style. She wishes he will marry her. Having tired of his mistress, however, Hampton tells her that he plans to marry a "good woman," Paula Chester (played by Matthews), who was originally intended for his son, Irwin (played by Chase). He is sure she will exert proper influence over Irwin.
 
Lola begs Hampton, whom she loves, to marry her instead. She tells him of her son, David (played by Giraci), who she sent away to school. But Hampton insists that he must not spoil his sons future.
 
In revenge, Lola decides to marry Irwin. Getting the young man intoxicated, she gets him to propose and they go to the minister. Because Irwin is drunk, however, the clergyman refuses to perform the marriage ceremony. As he was so drunk, Irwin does not realize that there was no actual wedding. He brings Lola home and introduces her to his father as his wife. Hampton, naturally, denounces her.
 
Lola then receives a call that her son, David, has been badly injured by a fall at school. His new wife, Paula, goes to Hampton and convinces him to operate on David, which saves the boys life. In gratitude, Lola relents. Admitting that the marriage was a hoax, she lets go of Irwin. Seeing his error, Hampton agrees to take care of her and her son.
 
==Cast (in credits order)==
*Louise Glaum as Lola Montrose
*J. Barney Sherry as John Hampton
*Colin Chase as Irwin Hampton
*Dorcas Matthews as Paula Chester
*Mae Giraci as David
*J. Frank Burke as Brother Eulofian
*Will H. Bray as Hart Chester

==Reception== city and state film censorship boards. The Chicago Board of Censors cut two intertitles, "A slave of the senses" and "My mistress", and issued the film, due to its subject matter, an "adults only" permit. 

==References==
 
 
==External links==
*  AFI Catalog of Feature Films
 
 
 
 
 
 
 
 
 