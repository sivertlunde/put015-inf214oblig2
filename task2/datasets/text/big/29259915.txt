Easan
{{Infobox film
| name = Eesan
| image = Easan Poster.jpg
| caption = Production poster
| director = M. Sasikumar
| producer = M.Sasikumar
| writer =  M. Sasikumar
| starring =  
| music = James Vasanthan
| cinematography = S. R. Kathir
| editing = A. L. Ramesh Company Productions
| distributor = Company Productions
| released =  
| runtime = 164 minutes
| country = India
| language = Tamil
| budget =
| gross =  16 crore
}} Abhinaya in lead roles alongside several newcomers.  The film was known and referred to as Nagaram and Aaga Chiranthavan before the official title was confirmed.     It released on 17 December 2010.

==Plot==
The story begins with a pub dance where girls and boys enjoy themselves to the fullest in a much spoilt way which leads to Eve teasing and results in a girls death. Sangayya (Samudrakani), the assistant commissioner of police, is forced to revoke the case because of Chezhiyans (Vaibhav) (a rich, spoiled guy) influence. Chezhiyan with his fathers Deiva Nayagam (A.L. Azhagappan), a leading politician, would bail them out even if he and his friends commit a murder. Vaibhav falls in love with a girl named Reshma, (Aparnaa Bajpai|Aparna) daughter of a businessman (largely inspired from Vijay Mallya). There are some political dramas that happens before the families agree to their marriage. Sangayya has been used in this political drama. He gets furious after learning this, but his hands are tied by the commissioner of police. When Chezhiyan is mysteriously kidnapped and beaten up by an unknown character, Sangayya starts an investigation. Sangayya solves the case and comes to know that a XI grade student named as Easan is responsible.

Easan has a flash-back sequence where his sister Poorani (Abhinaya) is raped by Chezhiyan and his friend Vinoth in a birthday party. Easans family, not being able to cope with the incident, commits mass suicide by drinking poisoned coffee. Easan survives since he only drinks a bit. He is rushed to the emergency where Pooranis friend Shyamala visits him and reveals what happened to his sister. Due to the strong affection he has for his sister, he decides to take revenge over the convicts. This is revealed to Sangayya by Easan himself, while Deiva Nayagam and his assistant arrive at the scene of investigation and find the dead Chezhiyan. The rest of the climax is how Sangayya saves Easan and himself from Deiva Nayagam and his assistant.

==Cast==
* Samudrakani as Sangayya Vaibhav as Chezhian Deivanayagam
* A. L. Alagappan as Deivanayagam Abhinaya as Poorani
* Aparnaa Bajpai as Reshma Shivaraj
* Rao Ramesh as Neethirajan
* Blessy as Karupusamy
* Thulasi as Shyamala
* Dushy (Udith Dushyanth Jayaprakash) as Easan
* Niranjan Jayaprakash as Vinod
* Latha Rao

==Soundtrack== Bala and Ameer Sultan from the Tamil film industry along with Bollywood director Anurag Kashyap, Malayalam director Ranjith (director)|Ranjith, Telugu director Trivikram and Kannada director Yograj Bhat - participating.  
{{Track listing
| extra_column = Performer(s)
| lyrics_credits = yes
| title1 = Meyyana Inbam | extra1 = Sukhwinder Singh, Benny Dayal, Sunandan | lyrics1 = Na. Muthukumar
| title2 = Jilla Vittu | extra2 = Thanjai Selvi | lyrics2 = Mohan Rajan
| title3 = Get Ready | extra3 = Benny Dayal, Gerard Thompson | lyrics3 = Na. Muthukumar
| title4 = Kannil Anbai | extra4 = Padmanabhan | lyrics4 = Na. Muthukumar
| title5 = Sugavaasi | extra5 = K. S. Chitra, Malgudi Subha | lyrics5 = Yugabharathi
}}

==Production== Sasikumar would direct his first film, the action thriller 

==References==
 

==External links==
*  

 

 
 
 
 
 