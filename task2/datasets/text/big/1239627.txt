Two for the Seesaw
{{Infobox film
| name           = Two for the Seesaw
| image          = Two for the seesaw.jpg
| image_size     = 225px
| caption        = Original film poster by Mitchell Hooks
| director       = Robert Wise
| producer       = Walter Mirisch William Gibson Isobel Lennart
| narrator       =
| starring       = Robert Mitchum Shirley MacLaine
| music          = André Previn
| cinematography = Ted D. McCord
| editing        = Stuart Gilmore
| distributor    = United Artists
| released       =  
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1,750,000 (US/ Canada) 
}} William Gibson.

==Plot==
Jerry Ryan (Mitchum) is a lawyer from Nebraska who has recently separated from his wife. To get away from it all, he has moved to a shabby apartment in New York. He is struggling with the divorce, which has been filed but is not final, and takes long walks at night.

At a party he meets Gittel Mosca (MacLaine), a struggling dancer. They instantly get along, and begin to fall in love. But the relationship is hampered by their differences in background and temperament.

Jerry gets a job with a New York law firm and prepares to take the bar examination. He helps Gittel rent a loft for a dance studio, which she rents out to other dancers. But their relationship is stormy. Gittel has a fling with an old boyfriend, and Jerry has
difficulty separating himself emotionally from his wife.

They prepare to move in together nevertheless, but Gittel is upset when she learns that the divorce came through and Jerry did not tell her about it. Jerry explains that even though he is divorced from his former wife on paper, they continue bonded in many ways, and he decides to return to Nebraska.

==Production== Broadway production ran for 750 performances, which included in its cast Henry Fonda and Anne Bancroft. 
 The Hustler. 

The title tune, "Second Chance",  became a pop music and jazz standard, recorded by Ella Fitzgerald and other artists.

MacLaine revealed on The Oprah Winfrey Show|Oprah on April 11, 2011, that she and Mitchum began a relationship during the filming of this film that lasted until his death.

==Cast==
* Robert Mitchum as Jerry Ryan
* Shirley MacLaine as Gittel Mosca
* Edmon Ryan as Frank Taubman
* Elisabeth Fraser as Sophie
* Eddie Firestone as Oscar Billy Gray as Mister Jacoby

==References==
 

==External links==
* 
* 
* 
*  on the Internet Broadway Database

 

 
 
 
 
 
 
 
 