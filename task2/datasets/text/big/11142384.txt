Samasya
{{Infobox film
| name           = Samasya
| image          =
| caption        =
| director       = K Thankappan
| producer       =
| writer         = KS Namboothiri
| screenplay     = KS Namboothiri Madhu Srividya Kamalahasan Sankaradi Shyam KP Udayabhanu
| cinematography = RN Pillai
| editing        = KB Singh
| studio         = Kalaratnam Films
| distributor    = Kalaratnam Films
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, Shyam and KP Udayabhanu.   

==Cast==
  Madhu
*Srividya
*Kamalahasan
*Sankaradi
*Anandavally
*Balan K Nair
*Kuthiravattam Pappu
*MG Soman
*Premji
*Sangeetha
 

==Soundtrack== Shyam and KP Udayabhanu and lyrics was written by ONV Kurup, P. Bhaskaran and Bichu Thirumala.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Abhayam Neeye || Lekha K Nair || ONV Kurup ||
|-
| 2 || Adithottu Mudiyolam || S Janaki || P. Bhaskaran ||
|-
| 3 || Kili Chilachu || K. J. Yesudas || ONV Kurup ||
|-
| 4 || Mangalayaathira Rathri || Chorus, Lekha K Nair || ONV Kurup ||
|-
| 5 || Mrigamadasugandha Thilakam || K. J. Yesudas || Bichu Thirumala ||
|-
| 6 || Nirapara Chaarthiya || P Susheela || ONV Kurup ||
|-
| 7 || Poojayum Manthravum || Raveendran, Chorus || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 