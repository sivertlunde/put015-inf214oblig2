This Thing of Ours
{{Infobox Film 
  | name = This Thing of Ours
  | image = 
  | caption = cover
  | director = Danny Provenzano	
  | producer = Ted A. Bohus Michael DelGaizo 
  | writer = Ted. A Bohus Danny Provenzano Edward Lynch Vincent Pastore James Caan
  | music = 
  | cinematography = 	
  | editing =	
  | distributor = Maverick   July 18, 2003 (U.S.)
  | runtime = 100 min
  | language = English 
  | budget =
  }}
This Thing of Ours is a crime/drama film released in 2003 starring Frank Vincent, Danny Provenzano, Edward Lynch, Vincent Pastore and James Caan. The title is a reference to the Italian term Cosa Nostra—"This Thing Of Ours"—which refers to the Italian Mafia.

==Plot==
Using the internet and old school mafia traditions, a crew of young gangsters led by Nicholas "Nicky" Santini attempt to pull off the biggest heist in the history of the mafia. Nicky must first convince his uncle Danny Santini, a respected caporegime of the Genovesso crime family in New Jersey, to put up the necessary "seed money", $50,000,000.00. When Danny agrees, Nicky - along with his partner and friend Johnny "Irish" Kelly - use violence and murder to put the plan in motion. But as much as things seem to be changing in the familys way of doing business, old habits and traditions remain and Nicholas must decide between his friends who helped him pull off the scam or his uncle Danny.

==Cast==
*Danny Provenzano - Nicholas Santini 
*Frank Vincent - Danny Santini 
*Edward Lynch - Johnny "Irish" Kelly
*Vincent Pastore - Skippy   James Caan - Jimmy "The Con" 
*Chuck Zito - Chuck 
*Louis Vanaria - Austin Palermo                       
*Christian Maelen - Robert Biaggio 
*Michael DelGaizo - Patsy DeGrazio 
*Pat Cooper - John Bruno 
*Vinny Vella - Carmine
*Joseph Rigano - Joe 
*Tony Ray Rossi - Anthony Russo 
*Jonathan Doscher - Agent Clark

== External links ==
*  

 
 
 
 


 