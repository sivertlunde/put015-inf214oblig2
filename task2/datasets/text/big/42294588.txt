Virattu
{{Infobox film
| name           = Virattu / Dega
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = Film Poster
| film name      = 
| director       = Kumar Taurani
| producer       = Kumar Taurani
| writer         = Kumar Taurani
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Sujiv  Erica Fernandes Pragya Jaiswal
| music          = Dharan Kumar
| cinematography = K. Prasad
| editing        = S. Karthik
| studio         = White Candy Entertainment
| distributor    = 
| released       =   (Tamil)
| runtime        = 
| country        = India Tamil
| budget         = 
| gross          = 
}} thriller film produced and directed by Kumar Taurani.  The film stars Tauranis son Sujiv and Erica Fernandes in the lead roles, while Pragya Jaiswal, Sai Vishal and Manobala appear in other pivotal roles. The Tamil version of the film was released on 21 February 2014 to average reviews, while the Telugu version Dega will release in mid 2014. 

==Cast==
*Sujiv as Sujiv
*Erica Fernandes as Sri
*Pragya Jaiswal
*Manobala
*Suman Shetty

==Production==
Sequences were filmed on a train travelling from Thailand to Malaysia and the stunt scenes were composed by Dho Dho of Thailand and Williams of Malaysia.  Further scenes were canned in New Zealand. 

==Release==
The film opened on 21 March 2014 to average reviews, ." 

==Music==
{{Infobox album Name = Virattu Type = Soundtrack
|Artist= Dharan Kumar Cover =  Caption = Front cover Released = 25 June 2013 Genre = Feature film soundtrack Length =  Language = Tamil
|Label = Sony Music India Producer =Dharan Kumar Last album = Podaa Podi (2012) This album = Virattu (2013) Next album = Endrendrum (2013) Reviews =
}}
The films audio was released in June 2013 by R. B. Choudary, while S. J. Surya received the first copy at a launch event. The film has four songs with four different lyricists contributing - Madhan Karky, Na. Muthukumar, Vignesh Shivan and Lallu. The films soundtrack opened to average reviews, with a music critic noting it was as "mercurial as its composer".  The song "Podhum Podhum" was released with a promotional video featuring the lead singers Andrea Jeremiah and Naresh Iyer and was well received by critics.  

{{tracklist
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    =

| title1          = Mouname Mouname
| extra1          = Andrea Jeremiah, Haricharan
| length1         = 5:18
| title2          = En Life In Angel
| extra2          = Dharan Kumar
| length2         = 4:50
| title3          = Meeta Paanu
| extra3          = M. M. Manasi, Santhosh Hariharan
| length3         = 5:01
| title4          = Podhum Podhum
| extra4          = Andrea Jeremiah, Naresh Iyer
| length4         = 5:06
}}

==References==
 

 
 
 
 
 
 
 