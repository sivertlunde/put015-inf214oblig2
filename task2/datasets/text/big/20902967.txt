Jaise Ko Taisa
{{Infobox film
| name           = Jaise Ko Taisa
| image          = Jaise Ko Taisa.jpg
| image_size     = 
| caption        = 
| director       = Murugan Kumaran
| producer       = M. Saravanan (film producer)|M. Saravanan M. Balasubramaniam
| writer         =
| narrator       = 
| starring       = Jeetendra Reena Roy Sridevi
| music          = R. D. Burman
| cinematography = 
| editing        = 
| studio         = AVM Productions
| distributor    = AVM Productions
| released       = 24th May 1973 
| runtime        = 139 mins
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1973 Bollywood drama film directed by Murugan Kumaran about twins which live their lives in contrasting social status after they are separated at birth. It stars Jeetendra and Sridevi as a child artist. R D Burman scored the music. Two songs, "Ab ke sawan mein jee dare" and "Daiya re daiya re, paap ki naiyya re," are good to listen and picturised well. It is one more hit film having double roles. "Kaunsi hai woh cheez jo yahan nahin milti, sub kuch mil jata hai, lekin hahn, Maa nahin milti..(You can get everything in this world, the only thing you cannot get or replace is: Mother), is heart rending and emotional.

==Cast==
*Jeetendra ... Vijay and Vinod Kumar (Double Role)
*Reena Roy ... Roopa
*Srividya ... Radha (as Sri Vidya) Anwar Hussain ... Shyamlal (Mamaji)
*Aruna Irani ... Maidservant
*Kamini Kaushal ... Vijays mom
*Ramesh Deo ... Prakashchand
*Mohan Choti ... Gopal (Munim)
*Dinesh Hingoo ... Prem (Cook)
*Sridevi ... Munni

==Music==

* Ab Ke Sawan Mein Jee Dare ... Kishore Kumar, Lata Mangeshkar

==References==
 

==External links==
*  

 

 
 
 
 
 


 
 