I Am an ESP
{{Infobox film
 | name = I Am an ESP
 | image =I Am an ESP.jpg
 | caption =
 | director = Sergio Corbucci
 | writer =  Bernardino Zapponi  Giovanni Romoli   Sergio Corbucci  Alberto Sordi 
 | starring = Alberto Sordi
 | music = Piero Piccioni
 | cinematography =Sergio dOffizi
 | editing =   Italian 
| released =  
 }} 1985 Italian comedy film written and directed by Sergio Corbucci.         

== Plot ==
Roberto Razzi, the skeptical and atheist host of the TV-program "Future", is a well-known debunker of pseudoscientists and gurus. During a travel in India, he will receive paranormal powers, that will put at risk his professional and sentimental life.

== Cast ==

*Alberto Sordi: Roberto Razzi
*Eleonora Brigliadori: Olga
*Elsa Martinelli: Carla Razzi
*Claudio Gora: Prof. Palmondi
*Maurizio Micheli: Priest
*Gianni Bonagura: De Angelis
*Donald Hodson: Babasciò
*Rocco Barocco: Maraja
*Ines Pellegrini: Concubine of Maraja 
*Néstor Garay: TV owner
*Pippo Baudo: Himself

==References==
 

==External links==
* 
 

 
 
 
 