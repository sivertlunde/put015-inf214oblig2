Pals For Life
{{Infobox film
| image = Pals For Life.jpg
| alt = 
| caption = Film poster
| director = Dan Rowbottom
| producers =        
| writer =          
| screenplay = 
| story = 
| based on =       
| starring =   Samantha Edwards Kyle Martin Mark Brent Saime Higson 
| narrator =       
| music = 
| cinematography = 
| editing = 
| studio = Rowbot Street
| distributor =    
| released =  
| runtime = 
| country = United Kingdom English
| budget = Pound sterling|£48,000
| gross =          
}} British World National Lottery through the Heritage Lottery Fund. The films tell the story of the Pals battalions during World War I, including the Accrington Pals and the Nelson Pals. Rowbot Street, the creators and producers, was granted Pound sterling|£48,000 funding in total to complete the films.
Rowbot Street, the producers was also involved in an educational project across primary schools and high schools in Lancashire to educate children and young teens about World War I.

The films were Shot on location in Bury, Preston, Lancashire|Preston, Goosnargh, Accrington and Springvale Mill in Haslingden.

==Initial Release/Screening==
The films were screened at the Imperial War Museum North in September 2014 at MediaCityUK in Manchester, United Kingdom. 

==Plot==
The first in the trilogy is set in Salford during World War I,telling the stories of local war heroes. 
The Nelson Pals film is the second in the Pals for Life trilogy, it outlines the scouts that was sent to war. It follows fifteen-year-old scout Archie (played by Jack Meakin) who is the son of Army Sergeant Henry (played by Mark Brent). While his mother disagrees, as the son of an Army Sergeant he has specific duties to live up to. He meets field nurse Grace (played by Samantha Edwards) where he finds comfort and friendship; throughout the film both Archie and Grace help each other both physically and mentally.

==Cast==
;Main Cast
* Laura Danielle Sharp as Rose
* Saime Higson as Elizabeth
* Jack Meakin as Archie
* Samantha Edwards as Grace
* Mark Brent as Henry
* Kyle Martin as Harry
* Dave Taylor  as Mayor

== References ==
 
*  
*  
*  

== External links ==
*  

 


 