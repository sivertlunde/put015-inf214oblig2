Oru Kadankatha Pole
{{Infobox film 
| name = Oru Kadamkatha Pole
| image =
| caption =
| director = Joshy Mathew
| producer = Balan, Satheesh, Achahci for B.A.M Productions
| writer = Nedumudi Venu John Paul
| starring = Jayaram, Geetha, Nedumudi Venu, Ashokan, Idavela Babu, Oduvil, Innocent, Mathu, Kaviyoor Ponnamma, Usharani etc
| music = Mohan Sithara Venu
| editing = B. Lenin V. T. Vijayan
| studio = BAM Productions
| distributor = BAM Productions
| released =  
| country = India Malayalam
}}
 1993 Cinema Indian Malayalam Malayalam film, Ashokan in lead roles. The film had musical score by Mohan Sithara.   

==Cast==
  
*Jayaram  Geetha  Ashokan
*Nedumudi Venu 
*Adoor Bhavani 
*Kaviyoor Ponnamma  Innocent 
*Idavela Babu 
*Maathu 
*Oduvil Unnikrishnan 
 

==Soundtrack==
The music was composed by Mohan Sithara. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Ponnum Pooppada || K. J. Yesudas, KS Chithra || ONV Kurup || 
|- 
| 2 || Sopaanasangeethalahariyil || K. J. Yesudas || ONV Kurup || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 