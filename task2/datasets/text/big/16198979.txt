Kipps (1941 film)
 
 
{{Infobox film
| name           = Kipps
| image_size     =
| image	         = Kipps FilmPoster.jpeg
| caption        =
| director       = Carol Reed Edward Black
| writer         = H. G. Wells (novel) Sidney Gilliat Frank Launder (uncredited)
| starring       = Michael Redgrave Diana Wynyard Phyllis Calvert
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 111 minutes (UK)
| country        = United Kingdom English
| budget         =
| gross          =
}} novel of the same name, directed by Carol Reed. Michael Redgrave stars as a drapers assistant who inherits a large fortune.

==Plot==
The day before the fourteen-year-old Kipps leaves to begin a seven-year apprenticeship in a drapers shop, he asks his friends sister, Ann Pornick, to be his girl. She gladly agrees.

Kipps goes to work for Mr. Shalford (Lloyd Pearson). Years pass and Kipps grows up into an unremarkable young man. One day, he attends a free lecture on self-improvement presented by Chester Coote (Max Adrian) and decides to take a course. Coote, disdaining Kipps lower class origins, steers the young man away from the literature class he wants to take to a woodworking class taught by Helen Walshingham (Diana Wynyard), a member of the local gentry. Kipps is soon smitten with his lovely teacher, but she is mindful of his social inferiority and ignores him.

One night, actor and playwright Chitterlow (Arthur Riscoe), riding a bicycle, collides with Kipps and tears his trousers. He takes Kipps back to his lodgings to repair his clothes. They get drunk together, while Chitterlow tells Kipps about his latest play, a comedy involving a beetle. By coincidence, one of Chitterlows characters is also called Kipps, a name the writer got from a newspaper advertisement.

When Kipps shows up for work late, he is sacked for breaking one of Mr. Shalfords strict rules of conduct. Then, Chitterlow tells Kipps that the advertisement was about him. It turns out Kipps has inherited a large house and a fortune (£26,000) from a grandfather he had never met.
 Michael Wilding) to look after his fortune. When Kipps finds out the man is Helens brother, he becomes interested.

Soon, Coote and the Walshinghams have maneuvered the naive Kipps into an engagement with Helen (though no encouragement is required), but he cannot handle her attempts at his self-improvement. Then, Kipps meets Ann, now a parlour maid, on her day off. His feelings for her resurface and he kisses her. Later, when he and the Walshinghams attend a party, Kipps is mortified to find the front door opened by Ann. During the gathering, Ann overhears the news of his engagement to Helen and rushes away. Kipps finds her and tells her he loves her. They sneak away to get married.

The newlyweds clash over Kipps insistence on maintaining his lofty social position. Then, Kipps receives a request to go to Ronnie Walshinghams office. Dreading a breach-of-promise suit, Kipps is surprised to meet Helen, rather than Ronnie. She has terrible news for him. Ronnie has lost all Kipps money and fled. The good-natured man reassures Helen that he will not set the police on her brother.

Just when all seems blackest, Chitterlow shows up in the middle of the night and informs Kipps that his play is a great success, and Kipps has a half-share in the profits. It is enough for Kipps to set up a bookshop and live comfortably with Ann and their baby son.

==Cast==
*Philip Frost as Kipps as a boy
*Michael Redgrave as Kipps as a man
*Diana Wynyard as Helen Walshingham
*Diana Calderwood as Ann Pornick as a girl
*Phyllis Calvert as Ann Pornick as a woman
*Arthur Riscoe as Chitterlow
*Max Adrian as Chester Coote
*Helen Haye as Mrs. Walshingham
*Betty Jardine as Doris Michael Wilding as Ronnie Walshingham
*Lloyd Pearson as Shalford
*Edward Rigby as Buggins, an older Shalford employee

==External links==
 
* 
*  
*  

 
 

 
 
 
 
 
 
 
 