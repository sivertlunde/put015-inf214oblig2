The Foxes of Harrow
{{Infobox film
| name           = The Foxes of Harrow
| image          = 699a125 foxes of harrow.jpeg
| caption        = Theatrical film poster
| director       = John M. Stahl|
| producer       = William A. Bacher|
| writer         = Frank Yerby (novel)
| based on       = The Foxes of Harrow Dwight Taylor Edwin Justus Mayer Thomas Job
| starring       = Rex Harrison Maureen OHara Richard Haydn Victor McLaglen Vanessa Brown Patricia Medina Gene Lockhart|
| music          = David Buttolph
| cinematography = Joseph LaShelle James B. Clark
| distributor    = 20th Century Fox
| released       = 24 September 1947
| runtime        = 117 minutes
| country        = United States/United Kingdom
| awards         =
| language       = English
| budget         = $2,750,000 
}}

The Foxes of Harrow is a 1947 adventure film directed by John M. Stahl. The film stars Rex Harrison, Maureen OHara and Richard Haydn.
 Best Art Direction (Lyle Wheeler, Maurice Ransford, Thomas Little, Paul S. Fox).   

==Plot summary==
In the pre-Civil War New Orleans, roguish Irish gambler Stephen Fox (Rex Harrison) buys his way into society – something he couldnt do in his homeland because he is illegitimate. 

==Cast==
* Rex Harrison as Stephen Fox
* Maureen OHara as Odalie Lilli DArceneaux
* Richard Haydn as Andre LeBlanc
* Victor McLaglen as Captain Mike Farrell
* Vanessa Brown as Aurore DArceneaux
* Patricia Medina as Desiree
* Gene Lockhart as Viscount Henri DArceneaux
* Charles Irwin as Sean Fox
* Hugo Haas as Otto Ludenbach
* Dennis Hoey as Master of Harrow
* Roy Roberts as Tom Warren
* Randy Stuart as uncredited, her first acting role
* Ralph Faulkner as uncredited Fencing Instructor

==Notes==
The storyline is derived from the eponymous novel The Foxes of Harrow by Frank Yerby. Fox paid author Frank Yerby $150,000 for the motion picture rights to The Foxes of Harrow, which was his first novel. A December 1947 Ebony magazine|Ebony article called the figure "the biggest bonanza ever pocketed by a colored writer" and stated that the book was "the first Negro-authored novel ever bought by a Hollywood studio."  

==See also==
*List of films featuring slavery

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 