King Kelly of the U.S.A.
{{Infobox film
| name           =  King Kelly of the U.S.A.
| image          = Guy Robertson and Irene Ware, King Kelly of the U.jpg image_size      = 200px caption         =  Cartoon Guy Robertson sings to Irene Ware, in King Kelly of the U.S.A. (1934)|
| director       = Leonard Fields
| producer       = George C. Bertholon
| writer         = Story: George C. Bertholon    Leonard Fields
| starring       = Guy Robertson Edgar Kennedy Irene Ware Ferdinand Gottschalk
| music          = Bernie Grossman Joe Sanders
| cinematography = Robert H. Planck
| editing        = Jack Ogilvie
| distributor    =  Monogram Pictures
| released       =  
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

King Kelly of the U.S.A. is a 1934 American romantic musical film directed by Leonard Fields.

The film is also known as Irish and Proud of It.

There is a running gag with exploding cigars, and a curious animated sequence, during a song.

== Plot summary ==
James W. Kelly, (Guy Robertson), and his pal Happy Moran, (Edgar Kennedy), are taking their all-girl-dancing troupe across the ocean to tour Europe, when Kelly and the mysterious Catherine Bell, (Irene Ware), fall for each other, literally.
 dough to GDP is measured in mops.

As a crooner, Kelly tries to sell enough mops, with a radio show, to pull the kingdom out of bankruptcy, and win Princess Tania, (Irene Ware), the mysterious Catherine Bell, from their shipboard romance.

Time is running out, as Prince Alexis, (William Orlamond), invades, from the neighbouring country, to seize the castle and marry the Princess.

Their only defence is the women and their “Personality Mops”, when the army quits, over back wages; and, Kelly is being marched off in front of a Firing Squad.

“Make Love not War”, could be their slogan, as the women all march to the castle, singing and waving their mops.
  falls for Irene Ware (1934)]]

== Cast ==
*Guy Robertson as James W. Kelly
*Edgar Kennedy as Happy Moran
*Irene Ware as Princess Tania aka Catherine Bell
*Ferdinand Gottschalk as King Maxmilian of Belgardia
*Franklin Pangborn as J. Ashton Brockton
*Joyce Compton as Maxine Latour
*Wilhelm von Brincken as Stranger
*Otis Harlan as Prime Minister
*Lorin Raker as Rodney
*William Orlamond as Prince Alexis
*Bodil Rosing as Maid

== Soundtrack ==
*Guy Robertson - "Right Next Door to Love" (Music by Joe Sanders, lyrics by Bernie Grossman)
*Guy Robertson - "Believe Me" (Music by Joe Sanders, lyrics by Bernie Grossman)
*Guy Robertson and the marching mop ladies - "Theres a Love Song in the Air" (Music by Joe Sanders, lyrics by Bernie Grossman)

== External links ==
*  
*  
*  

 
 
 
 
 
 
 
 
 