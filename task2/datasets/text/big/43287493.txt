Riding for Germany
{{Infobox film
| name = Riding for Germany
| image =
| image_size =
| caption =
| director = Arthur Maria Rabenalt
| producer = Richard H. Riedel 
| writer =  Josef Maria Frank   Clemens Laar   Fritz Reck-Malleczewen   Karl Friedrich Freiherr von Langen   Richard H. Riedel 
| narrator =
| starring = Willy Birgel   Gertrud Eysoldt   Gerhild Weber   Rudolf Schündler
| music = Alois Melichar 
| cinematography = Werner Krien 
| editing =  Kurt Hampp     UFA 
| distributor = UFA
| released = 11 April 1941  
| runtime = 92 minutes
| country = Nazi Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} equestrian events.

==Cast==
* Willy Birgel as Rittmeister Ernst von Brenken 
* Gertrud Eysoldt as Tante Ulle  
* Gerhild Weber as Tomasia Kolrep  
* Herbert A.E. Böhme as Olav Kolrep  
* Willi Rose as Karl Marten 
* Rudolf Schündler as Generaldirektor Brenner  
* Paul Dahlke as Dolinski  
* Hans Zesch-Ballot as Brigadekommandeur  
* Walter Werner as Geheimrat  
* Gerhard Dammann as Fuhrwerksbesitzer  
* Hans Quest as Sohn des Fuhrwerksbesitzers  
* Peter Elsholtz as Deutscher Ulan  
* Herbert Hübner as Pferdehändler  
* Karl Kahlmann as Regierungspräsident  
* Walter Lieck as Prosiger, Pferdehändler 
* Ruth Lommel as Junge Frau   Klaus Pohl as Gerichtsvollzieher  
* Armin Schweizer as Kapellmeister  
* Karl Junge-Swinburne as Leiter des Reitturniers 
* Angelo Ferrari as Italienischer Offizier  
* Karl Jüstel as Ausländischer Offizier 
* Jakob Sinn as Henseleit, Gutsinspektor  
* Marianne Stanior as Brenners Sekretärin 
* Wolfgang Staudte as Rebenschütz, Wachtmeister  
* Ewald Wenck as Alter Kutscher  
* Franz Ernst Bochum as Brenkens Diener  
* Fanny Cotta as Krankenschwester 
* Herbert Gernot as Voigt, Ordonanzoffizier 
* Willi Witte as Ordonanzoffizier   Knut Hartwig as Polizei-Wachtmeister  
* Leopold von Ledebur as Toms Logennachbar in Genf  
* Benno Mueller as Stationsvorstand 
* Jaspar von Oertzen as Von Burt, Adjutant des Kommandeurs 
* Hellmuth Passarge as Pferdehändler 
* Ernst Rotmund as Galizischer Pferdehändler 
* Eduard Wenck as Pferdehändler  
* Anton Pointner as Reporter 
* Paul Rehkopf as Reinboth, Futtermeister 
* Ferdinand Robert as Angestellter der Rennleitung  
* Walter Steinweg as Deutscher Offizier

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 
 
 
 
 
 
 
 


 