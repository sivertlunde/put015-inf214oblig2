Chase Step by Step
{{Infobox film
| name           = Chase Step by Step
| image          = 
| image_size     = 
| caption        = 
| director       = Yu Min Sheong
| producer       = C.Y. Yang (producer) T.K. Yang (producer)
| writer         = Yang Hsiang
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Hsin Chuan Huang
| editing        = 
| studio         = Hai Hua Cinema Company   Lee Ming Film Company
| distributor    = Mill Creek Entertainment (2005) USA DVD  Ocean Shores Video (1982) worldwide VHS
| released       = 1974
| runtime        = 82 minutes (USA)
| country        = Taiwan
| language       = Mandarin
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Chase Step by Step (Bu bu zhui zong) is a 1974 Taiwanese film directed by Yu Min Sheong.

The film is also known as Bu bu zhui sha in Hong Kong (Mandarin title). 

== Plot summary ==
Two circus performers are tasked with escorting relief in the form of gold bullion to a region hit by drought. On the way they are beset by countless bandits.    The marauders, of course, are waiting for them around every corner and they are besieged at each step. Also present are elements of romantic tension and some vaguely historical political references.   

==Cast==
*Barry Chan
*Shu Lin Chang
*Chung-Lien Chou
*Ping Ge
*Feng Hsu
*Yun Lan
*Hua Li
*Kuang Yung Lin
*Ka-Niu Na
*Ming Tsan Pan
*San-di Shi
*Kuo-Liang Su
*Liang Tai
*Meng Tien
*Ching Kang Tsai
*Chao Tseng Fei Wang Kai Wang
*Kuan-Hung Wang
*Hsieh Li Wu
*Min-hsiung Wu
*Fung Yue

==Soundtrack==
 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 
 