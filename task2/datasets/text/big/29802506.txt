Salt N' Pepper
 
 
 
 
{{Infobox film
| name = Salt N Pepper
| image = Salt NPepper 01.jpg
| director = Aashiq Abu
 | caption = Theatrical poster
| producer = Sadanandan Rangorath
| writer = Shyam Pushkaran Dileesh Nair Lal Shweta Baburaj
| Avial
| cinematography = Shyju Kahild
| editing = V. Saajan
| studio = Lucsam Creations
| distributor = Remya Movies
| released =  
| runtime = 118 minutes
| country = India
| language = Malayalam
| budget =    
| gross  =   
}} Indian Malayalam Malayalam romantic Baburaj and Vijayaraghavan play supporting roles. Cinematography is by Shyju Kahild, whose debut was Traffic (2011 film)|Traffic (2011). The screenplay was written by Shyam Pushkaran and Dileesh Nair.

The film follows the love stories of two couples. The main characters are: Kalidasan (Lal), an archaeologist; Maya (Shwetha Menon), a dubbing artiste; Meenakshi (Mythili), an IELTS student; Manu (Asif Ali), a happy-go-lucky management graduate; and Babu (Baburaj), Kalidasans chef. Food plays an important role in the story and the tagline of the film is Oru Dosa Undakkiya Katha ("The story born out of a Dosa"). 

The film has an original score by Bijibal, with three songs composed by Bijibal and the song "Aanakkallan" written and sung by Malayalam rock band Avial (band)|Avial. The film was produced by Lucsam Cinema and released by Lal. Principal production for the film started on 3 January 2011 and it was released in theatres on 8 July 2011 to positive reviews and good initial viewing figures. 
 Telugu and Hindi remake Sneha did the role of Shweta Menons character.

==Plot==
Kalidasan (Lal (actor)|Lal) works in the state archaeological department in Thiruvananthapuram (Trivandrum) and is a food lover. His only companion is his cook, Babu (Baburaj (actor)|Baburaj). Manu Raghav (Asif Ali) is Kalidasans nephew who comes to stay with him, while looking for a job. Kalidasan has a normal life until he gets a mis-dialled phone call from Maya (Shweta Menon), a dubbing artiste living with her friend Meenakshi (Mythili). Maya rings to order a special Dosa (named "Thattil Kutti Dosa" in the film) from a restaurant, but gets Kalidasan instead. Their conversations do not go well at first, but a long-distance romance develops due to their common interest—cooking and food. Kalidasan is a born gourmet while Maya is indulging in culinary activities in memory of her dead mother. Kalidasan starts to let Maya into the secrets of baking with a multi-layered cake called "Joans Rainbow".

Kalidasan and Maya both get the jitters before their first face-to-face meeting, as each becomes conscious of their own physical appearances, and both decide to send younger and better looking substitutes instead, Manu and Meenakshi. When they meet, neither Manu nor Meenkashi realise that the other person is a substitute, since they introduce themselves as Kalidasan and Maya respectively. Manu thinks that Kalidasan is actually in love with Meenakshi, while Meenakshi thinks that Maya is in love with Manu. They attempt to sabotage their older counterparts relationship by telling Kalidasan and Maya respectively that the person they met would be unsuitable for them. Kalidasan and Maya try to forget each other, but fail, and they decide to call each other and meet anyway. Manu and Meenakshi, who by this time had started to develop feelings for each other, are dejected upon hearing this and decide to leave the city so that their older counterpart may have a good relationship. However, they discover the truth and each others real identities during a chance encounter with a common acquaintance Pooja (Archana Kavi) at the railway station. Thus Manu and Meenakshi calls Kalidasan and Maya respectively and narrate all the incidents unknown to them. Kalidasan and Maya meet each other and their relationship begins.

==Cast== Lal as Kalathil Parambil Kalidasan. He has some peculiar characteristics. He is an archaeologist by profession and a food lover. He is also a reasonable cook and a food critic. 
* Shweta Menon as Maya Krishnan. A single woman who is working as a dubbing artiste, she is also a "foodie" and very good in cooking.
* Asif Ali as Manu Raghav. He is a carefree youngster character who often puts himself into awkward situations trying to be over friendly.  Asif Ali says, "Its the one character among all that I have   thats closest to my real-life persona." 
* Mythili as Meenakshi. She is a bubbly and glamorous girl who is beauty conscious and loves to take care herself.  Baburaj as Cook Babu. A professional chef  Vijayaraghavan as Balakrishnan. Kalidasans friend and colleague.
* Ahmed Sidhique as K. T. Mirash. Manus friend and Meenakshis teacher. He is an irksome guy who annoys people with unnecessary advice.
* Kelu Mooppan as Mooppan. A tribal whom Kalidasan brings to the city. Kalpana as Mary. The owner of the house where Maya and Meenakshi live.
* Ambika Mohan as Itha. A regular customer at Marys beauty parlour
* Archana Kavi as Pooja Nair. A girl whom Manu meets in a train.
* Dileesh Pothan as Director. A movie director who tries to woo Maya
* Nandhu as Bhaskaran Nair. Poojas father.

==Themes==
The film centres around food and Aashiq Abu says: "For a society that is so fond of food, this genre of cinema has not been really explored much in Mollywood  , save for a few films. As a foodie, I was inspired to make a film centred on food when I came across this interesting script by Syam Pushkaran and Dileesh Nair." 

The film often mentions Kerala cuisine. The title song "Chembavu", which features visuals of famous eateries across Kerala such as the hotels Zain, Sagar, Paragon, Bombay, Buhari, Ananda Bhavan, Paradise, etc. as well as the unique three-metre-tea at a stall in Kumbalangi. It also portrays some Kerala specialities such as Malabar Erachi pathiri. The lyrics of the song written by Rafeeque Ahammed, contains food. 

The reason for the mis-dialled call, Thattil Kutty Dosa, is a local speciality. Shibu B. S. (16 July 2011).  . The New Indian Express. Retrieved 18 July 2011.  The relationship develops between Kalidasan and Maya around the secrets of baking a multi-layered cake known as Joans Rainbow. The legend of Joans Rainbow is described by Nita Sathyendran in The Hindu:   According to Abu, "Salt N Pepper is meant to be a light-hearted entertainer; its nothing serious – no big plots, no big twists – but plain old common sense, and dollops of good ol fun." 

==Production== Daddy Cool fame. Lucky for us Aashiq was looking for a light-hearted script, something to do before his next superstar film came through. He loved our one-liner enough to give us an advance on the spot and asked us to come back with a full-fledged script". The duo was inspired by films such as Ponmuttayidunna Tharavu, Meleparambil Aanveedu, Kodiyettam and Yavanika, and the literary works of Vaikom Muhammad Basheer. Nita Sathyendran. (13 July 2011).  . The Hindu. Retrieved 15 July 2011. 
 Daddy Cool. Cinematographer Shyju Kahlid had previously worked with Sameer Thahir in Daddy Cool.    The film was produced by Mumbai-based Lucsam Creations. Salt N Pepper is their first project in the Malayalam film industry.  Casting was finalised by December 2010. The original cast included Lal (actor)|Lal, Asif Ali and Mythili. Nedumudi Venu was also reported to be cast during the original announcement. 

Filming began eight months after the script was submitted,  and was launched on 5 Jan 2011 with a blessing held at BTH Sarovaram, Kochi. The ceremony als included film producer Naushad making a dosa on stage.   Principal production for the film started on 3 January 2011 and was shot entirely in Thiruvananthapuram. 

==Reception==
The film received generally positive reviews, although some critics felt that certain scenes were far from convincing. In the Deccan Chronicle, Keerthy Ramachandran gave the film a three-star rating writing "This is a movie which weaves together taste, flavor and love to make an exquisite recipe for good cinema. A must watch for all gourmets, Salt N Pepper is sweet humour interspersed in a light plot." She also described the film as "one of the most enjoyable films of recent times." and praised the cast performances stating, "Shwetha Menon has acted brilliantly in the movie, proving that she has more to her than meets the eye. Lal has done complete justice to the role and appears likeable throughout the movie. But, the major share of credit should go to Baburaj – the stereotypical villain in Malayalam cinema. The role of Kalidasan’s cook is a milestone in his career. Asif Ali and Mythili appear perfect in their roles. Actor Vijaya Raghavan in a cameo has made a commendable appearance." 

Navamy Sudhish of The New Indian Express said, "A delightful addition to GenY fun flicks, ‘Salt N Pepper’ is an out-and-out entertainer. It gives two hoots to time-tested tricks and indulges you with a stimulating storyline and unfeigned artistry." He labelled the script "smart", Shyju Khalids appearances "rich" and "peachy", and Bijibals background score "superb". Sudhish also praised Abu saying "  narrative technique is unpretentious and devoid of any jaded gimmickry."  Paresh C. Palicha of Rediff.com said, "Director Aashiq Abu has put together the right ingredients in his new film Salt N Pepper and come up with quite an interesting dish" and that the film will meet the tastes of all who watch it.  Veeyen of Nowrunning.com said, "Aashiq Abu and his team adhere to the golden rules of good cooking, and see to it that the griddle is all hot, before they gently spread out a light hearted Dosa story on it." The critic also praised the cast performances and Shyju Kahlids cinematography.  ( )  A reviewer from Sify.com said, "Salt N Pepper may have its own share of shortcomings, but the sincerity with which it has been made is there to be seen in the film. It’s a young film which oozes lots of freshness and it is enjoyable for people of all ages, especially if you love your food.". 

The film became a sleeper hit in the box office, completing a running of 100 days at many centres in Kerala.  The 100th day celebration of the film was held in Dubai on 27 October with a concert by Avial. Bijibal also performed at the event.  

==Soundtrack== rock band Avial (band)|Avial, "Aanakkallan".  It is the bands first release since their self-titled debut album released in 2008. Avial singer Tony John says that "this song is more or less like a teaser for our second album". A music video was filmed which shows in the film at the end. John said: "We had loads of fun shooting the video, which is very kind of like what you would see in a music video rather than a filmy number".  The video, directed by Aashiq Abu, is the second one for Avial after their first hit "Nada Nada". It is also the first Avial track with John as the lead singer and Benjamin Isaac as the bass player. 

{{tracklist
| headline     =
| extra_column = Artist(s)
| writing_credits = yes
| total_length =
| title1       = Kaanamullal Ranjith
| length1      = 3:41
| writer1      = Bijibal, Santhosh Varma
| title2       = Chembavu
| extra2       = Pushpavathy
| length2      = 3:38
| writer2      = Bijibal, Rafeeque Ahammed
| title3       = Premikkumbol
| extra3       = P. Jayachandran, Neha Nair
| length3      = 3:10
| writer3      = Bijibal, Rafeeque Ahammed
| title4       = Aanakkallan
| extra4       = Avial
| length4      = 3:58 Avial
| title5       = Kaanamullal
| extra5       = Shreya Ghoshal
| length5      = 3:41
| writer5      = Bijibal, Santhosh Varma
}}

==Awards==
;Kerala State Film Awards Best Popular Film with Aesthetic value –  Sadanandan Rangorath Best Actress – Shweta Menon

; Asiavision Awards
* Best Family Movie
* Best Music Director – Bijibal
* Best Lyricist – Rafeeque Ahamed
* Best Second Actress – Shweta Menon Baburaj
* Special Mention – Mythili

; Asianet Film Awards 2011
* Best Star Pair – Asif Ali & Mythili

; Vanita Film Awards 2011
* Best Music Director – Bijibal Baburaj
* Lal & Shweta Menon

; Mathrubhumi Film Awards 2011
* Best Path Breaking Movie of the Year

; Amrita Film Awards
* Best Film

==References==
 

==External links==
*  
* 
*  
*  