Rockin' in the Rockies
{{Infobox film
| name           = Rockin in the Rockies
| image          = Rockinin rockies.jpeg
| caption        =
| director       = Vernon Keays
| producer       = Colbert Clark
| writer         = Gail Davenport Louise Rousseau J. Benton Cheney John Grey
| starring       = Moe Howard Larry Fine Curly Howard Mary Beth Hughes Jay Kirby Gladys Blake Jack Clifford
| music          = Paul Sawtell
| cinematography = Glen Gano
| editing        = Paul Borofsky
| distributor    = Columbia Pictures
| released       =  
| runtime        = 67 25"
| country        = United States
| language       = English
| budget         =
}}
 musical Western western full-length movie starring the Three Stooges (not to be confused with their 1940 short subject Rockin Thru the Rockies).  The picture was one of the Stooges few feature films made during the run of their more well-known series of short subjects for Columbia Pictures, although the group had appeared in supporting roles in other features. It is the only Stooges feature with the acts most famous line-up (Moe Howard, Larry Fine, and Curly Howard) in starring roles.

==Plot== Tim Ryan).

==Cast==
* The Three Stooges as Themselves
* Moe Howard as Shorty Williams (Ranch Foreman) (as The Three Stooges)
* Larry Fine as Larry (a Vagrant) (as The Three Stooges)
* Curly Howard as Curly (a Vagrant) (as The Three Stooges)
* Mary Beth Hughes as June McGuire
* The Hoosier Hotshots as Ranch Hands / Musicians
* Ken Trietsch as Hotshot Ken (as The Hoosier Hotshots)
* Paul Trietsch as Hotshot Hezzie (as The Hoosier Hotshots)
* Charles Ward as Hotshot Gabe (as The Hoosier Hotshots)
* Gil Taylor as Hotshot Gil (as The Hoosier Hotshots)
* Jay Kirby as Rusty Williams
* Cappy Barra Boys as Harmonica Musicians
* Gladys Blake as Betty Vale Tim Ryan as Tom Trove
* Spade Cooley as Spade Cooley (as Spade Cooley King of Western Swing)

==Production and reception==
Rockin in the Rockies featured musical numbers by Western Swing orchestra Spade Cooley and the Hoosier Hot Shots.  The Hoosier Hotshots were comedic musicians but, unlike Spike Jones orchestra, their  country-swing music never hit mainstream playlists and they are relatively unknown today. 

Oddly, Moe plays straight man as a non-Stooge character, with Larry and Curly interacting as a comedy duo. Curly is relatively subdued, as his mannerisms and reactions were starting to slow down by this time. Filmed during the same period as Idiots Deluxe, Curly (who noticeably played trombone in both films) was a few short weeks away from suffering a minor stroke, one that would hamper his remaining time with the Stooges. In addition, his falsetto voice sounds hoarse and strained. 

As a result of Moe being cast separately from the team, Larry awkwardly assumes Moes role as leader of the duo. As author Jon Solomon put it, though the Stooges do give the film "all the energy they can muster . . . when the writing divides them into a duo and a solo, they lose their comic dynamic."   

Solomon continues:
 "Rockin in the Rockies ignored many of the ingredients that were making contemporary Stooge short-films so successful. Writers Johnny Grey and J. Benton Cheney, who had barely written for the Stooges before, separated the Stooges and left Moe to act solo, including very few slapstick exchanges, and omitted an effective foil whom the Stooges could abuse or frustrate. At one point, Moe has words with and almost strangles Betty (Gladys Blake) ...
* Moe: "Jasper,   and I are alike in a lot of things."
* Betty: "Only your ears are shorter."
* Moe: "I resemble that last remark!"

...which is exactly the sort of personnel combination in which the Stooges do not succeed. Normally the Stooges either rescue a damsel in distress or are beaten up by tough, ugly, or overweight women. Here, instead of a heroic rescue or a slapstick exchange, Moe has to pull back his hands. Betty has no verbal or physical comeback, but later she gives Moe a kiss. This film may headline the Stooges, but it is not a Stooge film. 

Either the writers/director (Vernon Keays) did not understand what the Stooges were all about or they consciously tried to create a new kind of vehicle for them. Characterizing Moe and Curly as wiseguy tricksters fails because the writers were unable to make them either tricky or clever liars. Often in their mid-career feature films the Stooges are called upon to do their old gags and cram as many of them as possible into a few minutes, but here they simply recycle old gags without the kind of improvements Time Out for Rhythm achieved, and the dialogue is so limited that although the stag, horse and mule all talk, they actually have very little to say. Larry and Curly speak in uncharacteristically courteous dialogue as they mount the horse, and at one point the creativity is so lacking Moe calls Curly merely, You silly so-and-so. 

Even the sound effects are anemic or inappropriate. For the physical gags Curlys ailing health is apparent, and Moe is rarely around to cover for or interact with him. This leaves Larry as the toughie — not his best persona. Larry even has to run the when-I-say-go-we-all-point-to-the-right routine. When Curly and Larry finally mount the horse, when Larry rides on top of Curly, and when Larry uses a sledgehammer on Curlys head, there is a real absence of either franticness or even the basic Stoogeness that makes them elsewhere so successful. 

Ultimately, the entertainment in Rockin in the Rockies derives from its wacky and upbeat musical acts."  

Rockin in the Rockies was not a success, and the Stooges continued their series of shorts, again with occasional supporting roles in others feature films.  The group eventually achieved some feature film success with a series of full-length pictures made during a television-fueled resurgence after Columbia had ended their series of shorts. Beginning with 1959 in film|1959s Have Rocket, Will Travel, these later films starred Moe, Larry, and Joe DeRita, who joined the group after the deaths of both Curly and Shemp Howard and the departure of comedian Joe Besser.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 