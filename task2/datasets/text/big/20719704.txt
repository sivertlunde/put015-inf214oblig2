Boy! What a Girl!
{{Infobox film
| name           = Boy! What a Girl!
| image          = BoyWhatAGirl.jpg
| image_size     =
| caption        = DVD box art
| director       = Arthur H. Leonard
| producer       = Jack Goldberg Arthur H. Leonard
| writer         = Vincent Valentini
| narrator       = Tim Moore The Brown Dots Slam Stewart Sid Catlett Gene Krupa.
| music          =
| release        =
| cinematography = George Webber
| editing        = Jack Kemp
| released       = April 7, 1947 (USA)
| runtime        = 70 minutes United States of America
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
| distributor    = Herald Pictures
}}
 Tim Moore, with guest appearances by the Brown Dots, Slam Stewart, Sid Catlett and Gene Krupa.

==Plot==
Would-be theatrical producer Jim Walton is planning a new show that will feature bandleader Slam Stewart and the comic female impersonator Bumpsie (Tim Moore).    Mr. Cummings, the wealthy father of Jim’s girlfriend Cristola, has agreed to finance half of the show if the famous Parisian impresario Madame Deborah will provide the second half of the funding. When word arrives that Madame Deborah’s arrival from France has been delayed, Bumpsie is brought in to keep Mr. Cummings occupied. Mr. Cummings, however, is unaware that Bumpsie is a man in drag and he falls in love with him. The real Madame Deborah unexpectedly arrives early and passes herself off as Mrs. Martin. Two other would-be suitors, impressed with Madame Deborah’s wealth, begin to pursue Bumpsie. A fundraising party for the show is held, where several musical acts arrive to perform. A pair of thugs attempt to kidnap Bumpsie, believing he is Madame Deborah, but he manages to escape. The real Madame Deborah identifies herself and agrees to finance Jim’s show, enabling him to achieve his professional goals and to marry Cristola.   

==Production==
Boy! What a Girl! was planned to be the first in a series of all-black race films produced by the independent company Herald Pictures. The film’s press kit acknowledged the segregated distribution patterns of the race film by proclaiming Boy! What a Girl! would be “an all-Negro motion picture can be produced to play in any theater in the country and not merely confined to the some 600 odd playhouses that cater strictly to an all-Negro audience.”    

The film was shot at the Fox Movietone Studios in New York City. Gene Krupa, the only white member of the cast, was not originally signed to appear in the film; director Arthur H. Leonard invited Krupa to be on camera when the famous drummer stopped by to visit cast member Sid Catlett on the set. 

Boy! What a Girl! was the only starring film role for Tim Moore, an African American vaudeville comedy star who later became famous as the Kingfish in the television series Amos n Andy.  A pre-production news item identified Marva Lewis, the ex-wife of boxing champion Joe Louis, as being a part of the cast, but she is not present in the finished film, as she was forced to withdraw due to illness. She was replaced by the Brown Dots.  

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  

 
 
 
 