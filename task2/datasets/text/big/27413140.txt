The Feathered Serpent (1948 film)
{{Infobox film
| name           = The Feathered Serpent
| image_size     =
| image	         = The Feathered Serpent FilmPoster.jpeg
| caption        =
| director       = William Beaudine
| producer       = James S. Burkett
| writer         = Oliver Drake (story and adaptation) Earl Derr Biggers (character) Hal Collins (uncredited)
| narrator       =
| starring       = Roland Winters
| music          = Edward J. Kay
| cinematography =
| editing        = Monogram Productions
| distributor    = Monogram Distributing Corp.
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
The Feathered Serpent (also titled Charlie Chan in the Feathered Serpent) is a 1948 mystery film, the fifth of six in which Roland Winters portrayed Charlie Chan.  It is the only Chan film which featured both Keye Luke and Victor Sen Yung together.  Luke had been popular in the Warner Oland Chan films while Yung appeared primarily in the Sidney Toler Chan movies.  This was Yungs last Chan movie.  Luke appeared in one more with Roland Winters, the last of the Chan films, Sky Dragon.

==Cast==
*Roland Winters as Charlie Chan
*Mantan Moreland as Birmingham Brown
*Keye Luke as Lee Chan
*Victor Sen Yung as Tommy Chan
*Carol Forman as Sonia Cabot Robert Livingston as John Stanley
*Nils Asther as Professor Paul Evans
*Beverly Jons as Joan Farnsworth
*Martin Garralaga as Pedro Lopez
*George J. Lewis as Captain Juan Gonzalez
*Leslie Denison as Professor Henry Farnsworth

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 


 