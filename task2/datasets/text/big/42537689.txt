Mayar Badhon
 

{{Infobox film
| name           = Mayar Badhon 
| image          = MayarBadhon.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Swapan Saha
| producer       = Shree Venkatesh Films
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Prosenjit Chatterjee Rituparna Sengupta Satabdi Roy Abhishek Chatterjee Subhendu Chatterjee Robi Ghosh Soham Chakraborty Srabanti Chatterjee
| music          = Debojyoti Mishra
| cinematography = 
| editing        = 
| studio         = Shree Venkatesh Films
| distributor    = Eskay Video 
| released       = 1997
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}}
 Bengali film directed by Swapan Saha and produced by  Shree Venkatesh Films under the banner of  Shree Venkatesh Films.The film is a story of father and daughter, played by Prosenjit Chatterjee and Srabanti Chatterjee.It is a first film of Srabanti Chatterjee as Child Artist.Rituparna Sengupta, Satabdi Roy and Abhishek Chatterjee play the other roles.Music of the film has been composed by Debojyoti Mishra.   

==Plot==
Sagar Mukherjee(Prosenjit) is a tea taster in profession. He lives beside the house of Shipra(Rituparna Sengupta), his neighbour. Prasad Halder is the boss of Sagar’s company and her also lives beside them. Sagar and Shipra fall in love and go for registrary marriage without informing Shipra’s parents. Prasad(Robi Ghosh) helps them. Shipra starts living with Sagar. Shipra’s father does not accept this relationship. Shipra becomes pregnant. She has to be admitted in a hospital. There she meets with Krishna(Satabdi Roy) who is also pregnant. Shipra and Krishna give birth to girls on the same day, but suddenly fire catches in the hospital,and the girls are exchanged. Only the doctor and the nurse knows this. Shipra and Sagar’s daughter Maya(Srabanti Chatterjee), Nirmal and Krishna’s daughter Trishna grows up. One day Shipra dies due to cardiac failure. So, taking Maya, Sagar shifts to a tea-garden. There he meets Nirmal and Krishna with their son Kushal(Soham Chakraborty) and daughter Trishna. One day it is revealed that Trishna has a heart problem. The nurse accidentally comes to know about the Krishna’s present status in the city and her girl’s problem. In a major operation Trishna dies. The nurse reveals the incident of exchange of the babies to Krishna. Krishna and Nirmal came to know about Maya’s origin, but Sagar does not want to handover Maya to them. By law, Nirmal and Krishna get the charge of Maya. But Maya doesnot want to live with them. Sagar(Prosenjit) breaks down. Kushal does not want to recognise Maya as his sister. Maya runs away with Kushal and unites with her father sagar. Krishna and Nirmal accept it.

== Cast ==
* Prosenjit Chatterjee as a Sagar Mukherjee
* Rituparna Sengupta as a Shipra
* Satabdi Roy as a Krishna
* Abhishek Chatterjee as a Nirmal
* Subhendu Chatterjee
* Robi Ghosh as a Prasad Halder
* Soham Chakraborty as a Kushal (Child Artist)
* Srabanti Chatterjee as a Maya(Child Artist)
* Anuradha Ray

== References ==
 
 

 
 
 