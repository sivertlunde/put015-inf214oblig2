The Highest Ideals
{{Infobox film
| name = The Highest Ideals
| image =   |
| alt = 
| caption = 
| director = Anthony Gorsline
| producer = Gerard Schaefer
| screenplay = Anthony Gorsline
| studio = University of Nebraska Department of Photographic Production 
| distributor = 
| released =   
| runtime = 27&nbsp;minutes
| country = United States
| language = English
}}

The Highest Ideals is a 1955 American student documentary film produced in Technicolor for the National Society of Pershing Rifles.   Gerard Schaefer of the University of Nebraska Department of Photographic Production served as the producer and the film was written and directed by Anthony Gorsline. Technical advisers for the film were Colonel Chester Diestel, chairman of the Department of Military Science and Tactics, Lieutenant Colonel Ernst Liebmann, associate professor of the Department of Military Science and Tactics, and Pershing Rifles Major General Virgil Holtgrewe, National Commander of Pershing Rifles.  The Highest Ideals was produced to outline the background and aims of Pershing Rifles, the oldest continuously operating US college organization dedicated to military drill founded at the University of Nebraska in 1894.  

==Filming and production==
Research and writing of the script for the film began in February 1955. Principal photography and production of the of The Highest Ideals took place in 1955 on the University of Nebraska campus.  Editing and processing of the film took place in the Summer of 1955.  Period costumes were employed to portray the early history of Pershing Rifles in the 1890s.  

==Release==
The film premiered on October 27-29, 1955 at the Pershing Rifles National Assembly held at the Skirvin Hotel in Oklahoma City, Oklahoma. Copies of the film were distributed to local theaters, TV stations and to the 130 Pershing Rifles units throughout the country where it was shown at colleges and universities as a means of promoting the organization to prospective members.  

==Recognition==
The Highest Ideals received honorable mention as one of 10 finalists in the 1957 Annual Intercollegiate Awards competition for campus-produced films sponsored by the Screen Producers Guild and Life Magazine.  

==References==
 

*  

 
 