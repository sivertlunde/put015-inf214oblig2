Conan the Destroyer
 
 
{{Infobox film
| name           = Conan the Destroyer
| image          = Conan the destroyer.jpg
| director       = Richard Fleischer
| producer       = Raffaella De Laurentiis
| screenplay     = Stanley Mann
| story          = Roy Thomas Gerry Conway
| based on       =  
| starring = {{Plainlist|
* Arnold Schwarzenegger
* Grace Jones
* Wilt Chamberlain Mako
* Tracey Walter
* Olivia dAbo
* Sarah Douglas
}}
| music          = Basil Poledouris
| cinematography = Jack Cardiff
| editing        = Frank J. Urioste Dino De Laurentiis Company Universal Pictures
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $18 million  (est)  
| gross          = $100 million
}} Conan the Barbarian. The film was moderately successful at the box office in the United States|U.S., and very successful internationally, although critical response was not as strong as for the original film.

==Plot==
Conan (Arnold Schwarzenegger) and his companion, the thief Malak (Tracey Walter), are confronted by Queen Taramis (Sarah Douglas) of Shadizar.  She tests their combat ability with several of her guards. Satisfied, she tells Conan that she has a quest for him. He refuses her, but when she promises to resurrect his lost love, Valeria, Conan agrees to the quest. He is to escort the Queens niece, Jehnna (Olivia dAbo), an innocent who is destined to restore the jeweled horn of the dreaming god Dagoth; a magic gem must first be retrieved that will locate the horn. Conan and Malak are joined by Bombaata (Wilt Chamberlain), the captain of Taramiss guard. Bombaata has secret orders to kill Conan once the gem is obtained. 

Because the gem is secured in the fortress of a powerful wizard, Conan seeks the help of his friend, Akiro (Mako Iwamatsu|Mako), the Wizard of the Mounds. Akiro has been captured by a tribe of cannibals, and must first be rescued. Afterward, the adventurers encounter Zula (Grace Jones), a powerful bandit warrior being tortured by vengeful villagers. Freeing Zula at Jehnnas request, Conan accepts the indebted warriors offer to join their quest. 

The adventurers travel to the castle of Toth-Amon (Pat Roach) where the gem is located. As they camp for the night, the wizard takes the form of a giant bird and kidnaps Jehnna. The others wake in time to see the bird enter the castle. Sneaking in through a water gate, they search the castle, but Conan is separated from the group and the others are forced to watch him battle a fierce man-beast. Conan mortally wounds the creature, which is revealed as another form of Toth-Amon. With the wizards death, the castle begins to disintegrate, forcing the groups hasty retreat. They are ambushed by Taramiss guards, but drive them off. Bombaata feigns ignorance about the attack. The gem reveals the location of the jeweled horn. Jehnna expresses romantic interest in Conan, but he rebuffs her and declares his devotion to Valeria. 

They reach an ancient temple where the horn is secured. Jehnna obtains it while Akiro deciphers engravings. He learns that Jehnna will be ritually sacrificed to awaken Dagoth. They are attacked by the priests who guard the horn. A secret exit is revealed, but Bombaata blocks the others escape and seizes Jehnna. Despite this treachery, Conan and his allies escape the priests and trek to Shadizar to rescue Jehnna. 

Malak shows them a secret route to the throne room. Conan confronts Bombaata and kills him in combat. Zula impales the Grand Vizier (Jeff Corey) before he can sacrifice Jehnna. Because Bombaata and the Vizier were "impure sacrifices", the rising Dagoth (André the Giant) becomes distorted from a beautiful human form into a monstrous entity. Dagoth kills Taramis, then attacks Conan. Zula and Malak join the fight, but are effortlessly swept aside by the entity. Grappling with the monster, Conan tears out Dagoths horn, weakening the creature enough to kill it. 

Afterward, the newly-crowned Queen Jehnna offers each of her companions a place in her new court: Zula will be the new captain of the guard, Akiro the queens advisor, and Malak the court jester. Jehnna offers Conan marriage and the opportunity to rule the kingdom with her, but he declines and departs to find further adventures and his own place in the world.

==Cast== Conan
*Grace Jones as Zula
*Wilt Chamberlain as Bombaata Mako as Akiro
*Tracey Walter as Malak
*Sarah Douglas as Queen Taramis
*Olivia dAbo as Princess Jehnna
*Pat Roach as Toth-Amon
*Jeff Corey as Grand Vizier
*Sven-Ole Thorsen as Togra
*André the Giant as Dagoth
*Ferdy Mayne as The Leader

==Production==
 
===Toning down the violence===
When John Milius, director of Conan the Barbarian, proved to be unavailable to direct the sequel, Dino De Laurentiis suggested Richard Fleischer to his daughter Raffaella De Laurentiis, who was producing Conan the Destroyer. Fleischer had already made Barabbas (1961 film)|Barabbas (1961) and Mandingo (film)|Mandingo (1975) for Dino De Laurentiis.
 Universal Pictures and producer Dino De Laurentiis thought it would have been even more successful if it had been less violent, they wanted to tone down the violence in the sequel. Conan the Destroyer originally received an R rating like its predecessor, but the film was recut in order to secure a PG rating. Fleischer delivered a movie that was less violent and somewhat more humorous than the first, although some scenes of violence have bloody results.

===Casting=== David L. Lander was originally cast to play the foolish thief Malak, but due to Landers deteriorating health from the onset of multiple sclerosis, Lander was forced to quit the project, and the part was recast with Tracey Walter. André the Giant played Dagoth but was not credited in the film, as he was in costume.

===Photography=== A Matter The Red Shoes (1948).

===Locations===
Conan the Destroyer was filmed in a number of locations in Mexico&nbsp;— including Pachuca, the extinct volcano Nevado de Toluca, and the Samalayuca Dunes (near to Ciudad Juárez and El Paso)&nbsp;— as well as in the Churubusco Studios (also in Mexico). Carlo Rambaldi created the Dagoth monster.

===Deleted scenes===
In the film, a camel is knocked to the ground and, after struggling to get back up, its hind legs are drawn forward with wires so that it is forced to sit down before falling to the ground. This sequence is cut from the U.K. version, as is a double horse-fall in the opening battle. There are a number of deleted scenes from the movie confirmed by actress Sarah Douglas including; Conan and Queen Taramis love scene, Taramis slapping Bombatta, extended battle scene between Conan and the Man-ape, and more graphic action scenes.

==Music==

The musical score of Conan The Destroyer was composed, conducted and produced by Basil Poledouris, and it was performed by The Orchestra "Unione Musicisti Di Roma".

==Soundtrack==

Track Listing for the First Release on LP

Side One
# Main Title / Riders Of Taramis 3:31
# Valeria Remembered 3:02
# The Horn Of Dagoth 2:17
# Elite Guard Attacks 2:23
# Crystal Palace 6:00

Side Two
# The Katta 1:05
# Dream Quest 1:30
# Night Bird 2:21
# Approach To Shadizaar 2:40
# The Scrolls Of Skelos 2:26
# Dueling Wizards 1:25
# Illusions Lake 1:27
# Conan & Bombaata Battle 1:16

Track Listing for the First Release on CD

# Main Title / Riders Of Taramis 3:31
# Valeria Remembered 3:02
# The Horn Of Dagoth 2:17
# Elite Guard Attacks 2:23
# Crystal Palace 6:00
# The Katta 1:05
# Dream Quest 1:30
# Night Bird 2:21
# Approach To Shadizaar 2:40
# The Scrolls Of Skelos 2:26
# Dueling Wizards 1:25
# Illusions Lake 1:27
# Conan & Bombaata Battle 1:16

==Reception== Red Sonja a year later; however, Red Sonja was a critical and commercial disappointment and ended Schwarzeneggers involvement in sword-and-sorcery films. The film was nominated for two Razzie Awards, including Worst Supporting Actress and won Worst New Star for DAbo. 
 Marvel Super Special #35 (Dec. 1984).  The adaptation was also available as a two-issue limited series. 

 , published in 1990, with art by Mike Docherty.  The names of the characters were changed to distance the graphic novel from the movie: Dagoth became Azoth, Jehnna became Natari, Zula became Shumballa, Bombaata became Strabo, Toth-Amon became Rammon, and the characters of Queen Taramis and The Leader were combined into sorcerer Karanthes, father of Natari.

Robert Jordan wrote a novelization of the film in 1984 for Tor Books|Tor.

==Conan the Conqueror== Red Sonja Raw Deal, and he was not keen to negotiate a new one. The third Conan film thus sank into development hell. The script was eventually turned into Kull the Conqueror. 

==In popular culture==
Kim Wayans spoof portrayals of Grace Jones on the show In Living Color are based on Graces performance of Zula in this film. In 1985 Australian heavy metal music group Prowler changed its name to Taramis after the character from this film. McFarlane,   entry. Archived from   on 3 August 2004. Retrieved 22 March 2013. 

==References==
 

==External links==
 
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 