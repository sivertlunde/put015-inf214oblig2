A Single Girl
{{Infobox film  name = A Single Girl image = A_Single_Girl.jpg director = Benoît Jacquot     writer = Benoît Jacquot Jérôme Beaujour  producer = Brigitte Faure Philippe Carcassonne starring = Virginie Ledoyen Benoît Magimel cinematography = Caroline Champetier     editing = Pascale Chavance   released = 29 May 1995  country = France language = French distributor = Pyramide Distribution Strand Releasing (USA) 
}}
Benoît Jacquots A Single Girl ( ) follows a day in the life of a young Parisian woman named Valérie (played by Virginie Ledoyen) who begins a new job at a four star hotel the same day she reveals to her boyfriend that she is pregnant.  The 90 minute film is shot in real time in the random style of the French New Wave.
 The Beach, and earned her a César Award nomination.

==Cast==
*Virginie Ledoyen as  Valérie Sergent 
*Benoît Magimel as  Rémi 
*Dominique Valadié as  Valéries mother 
*Véra Briole as  Sabine 
*Virginie Emane as  Fatiah 
*Michel Bompoil as  Jean-Marc 
*Aladin Reibel as  M. Sarre 
*Jean-Chrétien Sibertin-Blanc as  Patrice 
*Guillemette Grobon as  Mme Charles 
*Toni Cecchinato as  Italian man 
*Giulia Urso as  Italian woman 
*Matéo Blanc as  Fabien, the child 
*Jean-Claude Frissung   
*Hervé Gamelin as  Jean 
*Catherine Guittoneau as  Jeans lover 
*Thang-Long as  Mr. Tranh 

==External links==
* 
*  

 

 
 
 
 
 
 
 