Shopping (film)
{{Infobox film
| name           = Shopping
| image          = Shoppingdvd.jpg
| image_size     =
| caption        = DVD cover for the film
| director       = Paul W. S. Anderson
| producer       = Jeremy Bolt
| writer         = Paul W. S. Anderson
| narrator       =
| starring       = Sadie Frost Jude Law
| music          = Barrington Pheloung
| cinematography = Tony Imi
| editing        = David Stiven
| studio         = Channel Four Films Impact Pictures Kuzui Enterprises PolyGram Filmed Entertainment WMG Film
| distributor    = New Horizons
| released       = June 24, 1994   February 9, 1996  
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = $3,061     
}}
 1994 British action crime crime drama drama film British teenagers joyriding and ramraiding. It was notably the first major leading role for actor Jude Law, who first met his co-star and future wife Sadie Frost on the set of this film. 

The film was located at Trellick Tower, Golborne Road, London.

==Cast==
*Sadie Frost - Jo
*Jude Law - Billy
*Sean Pertwee - Tommy
*Fraser James - Be Bop
*Sean Bean - Venning
*Marianne Faithfull - Bev
*Jonathan Pryce - Conway Daniel Newman - Monkey (as Danny Newman)
*Lee Whitlock - Pony
*Ralph Ineson - Dix
*Eamonn Walker - Peters
*Jason Isaacs - Market Trader
*Chris Constantinou - Yuppie
*Tilly Vosburgh - Mrs. Taylor
*Melanie Hill - Sarah

==Soundtrack==
*The Sabres of Paradise - Theme
*Smith & Mighty  - Drowning Man
*Disposable Heroes Of Hiphoprisy - Water Pistol Man
*Senser - No Comply
*Stereo MCs - Wake Up
*Barrington Pheloung - Hunters and Hunted James Vs The Sabres of Paradise - Honest Joe (Spaghetti Steamhammer Mix)
*Credit to the Nation - Call It What You Want
*Kaliphz - Vibe Da Joint
*Utah Saints - I still think of you Wool - The Witch
*Perfecto - Rise
*One Dove - Why dont you take me
*Barrington Pheloung - Billys Theme
*Shakespears Sister - Waiting
*Barrington Pheloung - Climb Down To Crash Orbital - Crash and Carry (a.k.a. The Meet)
*Salt-n-Pepa - Heaven or Hell EMF - Dont Look Back
*Barrington Pheloung - Tread The Thin Line

==Reviews==
Channel 4 wrote a mixed review of Shopping, stating that "borrowing from Blade Runner and Gotham City to build his vision of a country divided. While a lack of subtlety clouds his intentions, the director delivers a slick, diverting story that will probably be best remembered as Jude Laws first movie." 

==See also== Joyriding
*Ramraiding

== References ==
 

== External links ==
* 
*  
* 
* 
*  at Soundtrackcollector.com
* , Interview Paul W.S. Anderson, Sep. 13, 2002

 

 
 
 
 
 
 
 
 
 
 


 
 