Silent Minority
 
 
 

{{Infobox film
| name           = Silent Minority
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Nigel Evans
| producer       = Nigel Evans
| writer         = Nigel Evans
| narrator       = Nigel Evans
| starring       = 
| music          = 
| cinematography = Alan Jones Ron Conley
| editing        = Peter Cannon
| studio         = Colour Productions
| distributor    = Associated Television
| released       =  
| runtime        = 60 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
}}
 ATV which aired in June 1981  on ITV Network|ITV. {{cite web 
| url=http://ftvdb.bfi.org.uk/sift/title/11894?view=transmission 
| title=Silent Minority 
| publisher=British Film Institute  Borocourt Hospital near Reading, Berkshire and the St. Lawrence Hospital in Caterham, Surrey.  {{cite web
|url=http://www.unlockingthepast.org.uk/index/articles/view_articles/22/ 
|title=The Silent Minority 
|accessdate=15 June 2011}}  {{cite book 
| title=Meeting the needs of children with disabilities: families and professionals facing the challenge together 
| publisher=Routledge 
| author=Helen Warner 
| year=2002 
| location=Chapter 2 
| isbn=0-415-28038-9 
| url=http://books.google.co.uk/books?hl=en&lr=&id=1-CvZFkPk9IC&oi=fnd&pg=PA14&dq=%22Silent+Minority%22+documentary+1981+british+television&ots=oswZEXVDLT&sig=LLBi-T6BDbX1Zu6TKhfctHysNUo
|accessdate=17 June 2011}}  {{cite journal 
| url=http://direct.bl.uk/bld/PlaceOrder.do?UIN=077869876&ETOC=RN&from=searchengine 
| title=Policy and Service Development Trends: Forensic Mental Health and Social Care Services 
| author=Murphy, G. 
| journal=Tizard Learning Disability Review 
| year=2000 
| volume= 5 
| issue=Part 2 
| pages=pages 32–35 
| publisher=Pavilion 
| ISSN=1359-5474
|accessdate=15 June 2011
| doi=10.1108/13595474200000016}} 

Part polemic, part narrative, it came just before the transitioning of the British mental health system from an history of psychiatric institutions|asylum-based system to one of care in the community.

==Critical response== Glasgow Herald George Young ATV announced making changes to the films commentary due to these protests, and Charles Denton of ATV stated that film director Nigel Evans admitted that hospital administrators were deceived during the projects filming, but stated that such deception was "in the public interest".  Evans had been granted access to the facilities and was allowed to shoot individual patients only if he obtained consent of their family members.  When some families did not consent, Evans destroyed six reels of footage, but health officials determined he deliberately concealed other footage. These officials granted that the film footage was accurate, but complained that the narration was not, thus resulting in ATV modifying the commentary. {{cite news
|title=Deception row over hospital TV film
|url=http://news.google.com/newspapers?id=67VAAAAAIBAJ&sjid=yKUMAAAAIBAJ&pg=6279,1882287 Glasgow Herald
|date=10 June 1981
|accessdate=17 June 2011}}    IBA for refusing to ban it. {{cite news 
| url=http://news.google.com/newspapers?id=7LVAAAAAIBAJ&sjid=yKUMAAAAIBAJ&pg=2676,2284201&dq=silent-minority+nigel-evans&hl=en The film subsequently received the 1981 Royal Television Society Award for the best UK documentary.
| title=Last Nights View by Alison Downie  Glasgow Herald 
| date=11 Jun 1981 
| accessdate=18 June 2011 
| author=Alison Downie 
| pages=18}}  

Controversy about the film made into the British Parliament with questions in the House addressed to Norman Fowler, Secretary of State for Social Services. {{cite web 
| url=http://hansard.millbanksystems.com/written_answers/1981/jun/09/silent-minority-documentary
| title="Silent Minority" (Documentary)
| publisher=Hansard 
| date=October 1981 
| accessdate=17 June 2011 
| pages=HC Deb 9 June 1981 vol 6 c72W 72W }}   Fowler, while appreciating that the film drew attention to the plight of handicapped patients within the British health system, felt that concentrating "its attention on certain categories of the most severely handicapped", gave an "unrepresentative picture both of the two hospitals and of the care given by National Health Service staff to mentally handicapped people in general." But he expanded that the concerns brought forward by the film were being addressed through the creation of smaller facilities better able to address patients needs. {{cite web 
| url=http://hansard.millbanksystems.com/written_answers/1981/oct/29/the-silent-minority 
| title=The Silent Minority 
| publisher=Hansard 
| date=October 1981 
| accessdate=17 June 2011 
| pages=HC Deb 29 October 1981 vol 10 cc452-4W}} 

== References ==
 

 
 
 
 
 
 
 
 
 
 
 
 