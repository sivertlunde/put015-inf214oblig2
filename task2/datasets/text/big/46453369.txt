Close-Up (1948 film)
 

{{Infobox film
| name           = Close Up
| image          = 
| alt            =
| caption        =  Jack Donohue
| producer       = Frank Satenstein John Bright
| story          = James Poe
| starring       = Alan Baxter Virginia Gilmore Richard Kollmar
| music          = Jerome Moross William Miller
| editing        = Robert Klager
| studio          = Marathon Pictures
| distributor    = Eagle-Lion Films
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Jack Donohue John Bright. Alan Baxter, Virginia Gilmore and Richard Kollmar.

==Plot==

Phil Spar (Alan Baxter), a newsreel photographer in New York City, is doing a fashion shot outside a bank. Meanwhile in the bank Joseph Gibbons (Phillip Huston) s pal, Mr. Fredericks(Michael Wyler) says he is closing out his account tomorrow and will be withdrawing $800,000.00. Phil unintentionally films Martin Beaumont (Richard Kollmar) as he is leaving the bank. An effort is made by Fredericks associate, Beck (Russell Collins) to secure this film. Beck approaches Phil to buy the film, spinning a tale about his wife and his girlfriend not needing to see him in the film. Phil meets magazine reporter Peggy Lane (virginia Gilmore) and they go to deliver the film to Beck. Then Phils boss, Harry Avery (Loring Smith) discovers that Mr. Beaumont is actually Kurt Bauer, a wanted Nazi war criminal. Phil arranges a rendezvous with Miss Lane at his apartment, but Phil is kidnapped by Gibbons posing as a New York City detective. Phil makes a daring escape on the Staten Island ferry.   When Phil returns to his office he finds that his boss killed the hitman who was sent to knock him off and steal the film negative.  Phil gives the film can to a cabbie to take to the police, but the bad guys knock out the cabbie and kidnap Phil and Peggy. We learn that Peggy is actually tied in with the bad guys. She has a change of heart and acts to try and keep Phil from getting killed. Mr. Beaumont has hired Gibbons to fly him out of the country. Gibbons tries to double-cross Beauomnt and get all the $800,000, but Beaumont outsmarts him. Beaumont heads to meet the seaplane that will fly him out of the country,  using Phil as a shield. Peggy calls the cops. Gibbons shows up and engages in a gunfight with Beaumont. Beaumont kills Gibbons and his henchman, but Phil chases him and Beaumont is shot by the cops. Phil thanks Peggy for saving his life  before she goes off to jail.

==Cast==
 Alan Baxter as Phil Spar
*Virginia Gilmore as Peggy Lane
*Richard Kollmar as Martin Beaumont
*Loring Smith as Harry Avery
*Phillip Huston as Joseph Gibbons
*Joey Faye as Roger
*Russell Collins as Beck
*Michael Wyler as Fredericks
*Sid Melton as the cabbie
*Wendell Phillips as Harold
*Erin Selwyn as the office receptionist
*Jimmy Sheridan as Jimmy
*Marcia Walter as Rita

==Links==
*  

 