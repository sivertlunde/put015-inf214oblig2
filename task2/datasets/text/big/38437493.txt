Status Quo? The Unfinished Business of Feminism in Canada
{{Infobox film
| name           = Status Quo? The Unfinished Business of Feminism in Canada
| image          = Status Quo? The Unfinished Business of Feminism in Canada.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Karen Cho
| producer       = Ravida Din
| writer         = Karen Cho
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Lorraine Segato
| cinematography = Katerine Giguère
| editing        = Barbara Brown
| studio         = National Film Board of Canada
| distributor    = National Film Board of Canada
| released       =  
| runtime        = 87 min 13 s
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Status Quo? The Unfinished Business of Feminism in Canada is a 2012 documentary film about the state of feminism in Canada, directed by Karen Cho and produced by Ravida Din for the National Film Board of Canada.

==Synopsis==
The documentary combines archival material with contemporary stories, juxtaposing scenes from the 1967  , violence against women and abortion access.    

==Production==
Cho has stated that when she was approached by producer Ravida Din, she didnt know much about feminism and thought that most of the battles had already been won. In making the film, she had been shocked that "so many of the issues are still around today": 

 

Cho stated in March 2013 that she began research on what was originally planned as an historical film on feminism five years earlier, and did not consider herself a feminist before she conducted her research.   

==Release==
The film was named was best documentary at the Whistler Film Festival.    The film was streamed for free at NFB.ca in  conjunction with International Womens Day, and as of March 2013, was being screened at more than 60 events across Canada, with support from the YWCA, the Canadian Federation of University Women, Cinema Politica and public libraries.   

==References==
 

==External links==
* 
*  at Cinema Politica
* 

 
 
 
 
 
 
 