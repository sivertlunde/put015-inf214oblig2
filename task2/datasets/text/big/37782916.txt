Flick (2000 film)
 
 

{{Infobox film
| name           = Flick
| image          = Flick DVD cover USA.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Fintan Connolly
| producer     = Fiona Bergin
| writer         = Fintan Connolly
| narrator       =  David Murray David Wilmot Alan Devlin
| music          = Niall Byrne
| cinematography = Owen McPolin
| editing        = Mark Nolan
| studio         = A Fubar Film
| distributor    = Eclipse Pictures
| released       =  
| runtime        = 82 minutes
| country        = Ireland
| language       = English
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 David Murray, David Wilmot and Gerard Mannix Flynn.

== Plot summary ==
A plane touches down. Jack Flinter, mid-20’s, collects his friend Des Fitzpatrick, at Arrivals. Back in Des’s place, they celebrate the smuggling in of ten kilos of good Moroccan hash until the arrival of Barry Devlin. Jack leaves and back in his darkened flat, Alice, his girlfriend, is unimpressed.

Next day he does his rounds, dealing hash to various people around town. In the Club he hooks up with Des, who tells him he’s contacted Gerry and Pop, major dealers, about getting into business together. Jack is annoyed but agrees to meet with them. Isabelle catches his attention. They meet at the bar and talk – she’s a German artist visiting the city. They go to a hotel and have sex.

Back in the flat Jack is confronted by a pissed off Alice. He apologises, she leaves. Des arrives. Jack is apprehensive and Des pulls out a gun, talking about taking precautions. Jack is freaked. When he hears that Des got the gun from Barry, a nephew of the guy they are meeting, Jack confiscates the gun and hides it up the chimney.

They meet with Gerry and Pop, two criminals. The meeting is brisk and to the point. Put up the money and they’ll do the rest. Pop warns them not to mess up. One of Jack’s customers, a barman, tells Jack that Des is taking heroin. Jack hides his shock. He is approached by a stranger looking for hash. Something is not right – police activity. Jack leaves the pub. In the Club, he confronts Des, who tells him not to worry. Jack tells him to sort it out. All this is observed by Inspector Mulligan from the Drug Squad.

At home, Alice tells him she’s going away for the weekend and that he should be gone when she comes back. He watches some home video of happier days with her and crashes out. He is shaken awake roughly. Mulligan and his squad surround him. Handcuffed, he is marched away. In the car, Mulligan warns Jack to get out of city before it’s too late. They dump him on the outskirts of the city.

In the Club, he meets Des who was also picked up by the police that afternoon. Jack tells him it’s getting hairy and to break off with Gerry and Pop. Easier said than done says Des. Isabelle arrives. Des splits.

They talk, she suggests they drop an E and go to a party. Keen to get away from his troubles he agrees. They swallow the E. They wander through the city to a carnival, go up on the big wheel, watch the fireworks and go to the party. In a haze, Jack looks around. He sees Isabelle mingling, but bumps straight into Kay, an ex-girlfriend, who winds him up, lambasting him. His head is spinning. He walks straight ahead, away from the partygoers, down to a river. Isabelle follows and by the river, he tells her about how fucked up everything has become. She doesn’t really care and cuts him off at the pass.

Jack returns to Alice’s flat and collects his things – passport, money, and gun. He listens to a message from Des’s sister, Vera, about Des being beaten up. He is in hospital. As Jack leaves the flat, he is chased by Gerry and his cronies, but eventually gets away. He visits Des in hospital. Des lies sedated, attached to a respirator. Vera tells him it was Gerry and Barry and questions him about what’s going on. He makes his excuses and as he leaves the hospital, he meets a doctor who calls him "Flick".  The doctor is an old schoolmate who invites him to a school reunion.

Barry Devlin pulls up outside his house. Jack emerges from the bushes; Barry insults him and tries to walk away. Jack stops him, and a fight ensues. He leaves Barry in a heap. Jack meets up with Isabelle. He tells her he has to get away. They are interrupted by the sound of screeching brakes. Jack is bundled into a van.

Gerry and his cronies rough Jack up in the back of the van. He accuses him of grassing them up. Jack is defiant. The van pulls into a lock-up garage. Jack, bloodied and dazed, is interrogated by Pop Devlin. Jack feels the pain. Pop clutches his chest. The light flickers off. The place is plunged into darkness. Mulligan and his squad burst in.

In the melee, Jack escapes and runs for his life through the streets of Dublin. He runs to Isabelle. He asks for her help, then passes out. Dawn. Isabelle pulls into a lay-by overlooking the city. Jack gets out, dazed and confused, he doesn’t know where he is, what’s happening. He looks at the city shimmering below him. He looks at Isabelle. He looks back at the city. He turns and walks towards Isabelle. He gets back into her car. They drive west, away from the city.

== Tagline ==
In every club in every bar there’s a dealer dying to make it big.

== Cast == David Murray David Wilmot
* Isabelle – Isabelle Menke
* Mulligan – Gerard Mannix Flynn
* Alice – Catherine Punch Alan Devlin Aaron Harris
* Kay – Maria Lennon
* Barry – Alan Devine
* Taxi Driver – Vincent McCabe

== Production ==
The film was shot in 18 days on a budget of £18,000. According to Connolly "We put together a compact crew – first-timers like ourselves – and we shot the film in eighteen days, mostly within a three mile radius of O’Connell Bridge. We tried to follow some basic guidelines we had culled together from reading and hearing about low-budget film-making – namely, you write a script with a couple of characters in one location; you do an inventory of availability; you make it fit your budget; you rehearse well, talk everything through; you spend the money on getting the best gear you can afford; you keep the shoot tight – three weeks maximum; you use locations that are free; everyone defers their payments, but you feed and water them well and keep to a twelve-hour day, six day week; you shoot a ratio of 6/1 and you keep away from people who say it can’t be done."  

== Reception == Variety said "Pic is beautifully lit and strikingly filmed. Awash in bold red and blue strokes highlighting Dublins menacing underworld, pic establishes a realm of glamorous filth that is as fascinating as its characters are distasteful." 

== Accolades ==
The film had its world premiere at the 44th Murphys Cork Film Festival in October 1999. It was screened at the Galway Film Fleadh in July 2000 where programmer Pat Collins wrote "Flick is Fintan Connolly’s debut feature but he shows no sign of first time nerves.  It’s effortlessly stylish and slick, closer in tone and mood to some recent Spanish films than to the average Irish offering. David Murray acts in his first major role and has a great screen presence – his swaggering saunters through the Dublin streets form the spine of the film. Owen McPolin’s camerawork is worth special mention, seldom has Dublin looked so appealing." 

It had its North American premiere at the AFI Fest in Los Angeles in November 2000. Programmer Shaz Bennett wrote "Intoxicating performances and the resonant echoing soundtrack make this love thriller by director/writer Fintan Connolly a haunting, intensely erotic film. FLICK punctures the surface of an individual adrift in the absence of compassion or support – very much like the suburbs, minus the drinking, smoking and screwing." 

== Music ==
The musical score for Flick was written by Niall Byrne.

== Release ==
The film received a domestic release opening at 4 cinemas in Dublin and Cork on 8 September 2000.

== Home media ==
Xtra-vision produced the Region 2 VHS that came out in January 2001. Vanguard Cinema released the Region 1 DVD in Fall 2003. 

== References ==
 

== External links ==
* Flick at the Internet Movie Database  
* Flick trailer  
* Watch Flick  
* Fintan Connolly interview with Paul Byrne "Tales From The Dark Side"  
*  
*  

 
 
 
 
 