Italians (film)
{{Infobox film
 | name = Italians
 | image = Italians (film)Locandina.jpg
 | caption =
 | director = Giovanni Veronesi
 | writer =  Giovanni Veronesi  Andrea Agnello Ugo Chiti
 | starring =  Carlo Verdone  Sergio Castellitto
 | music =Paolo Buonvino	
 | cinematography = Tani Canevari
 | editing =    
 | producer =    
 | released =   
 | country = Italy
 | language  = Italian  
 }} 2009 Cinema Italian comedy film written and directed by Giovanni Veronesi. For this film Paolo Buonvino	won the  Silver Ribbon for best score.  the film also received three nominations at David di Donatello for best score, best sound and best special effects. 

== Plot ==
First segment: Fortunato is a trucker specialized in the transport of luxury vehicles, especially Ferrari, in Saudi Arabia and in other Gulf countries. Tired of a life away from home and in the process of retiring, he accompanies his successor, the young Marcello, for his last trip in Dubai. 
 Roman dentist, is forced to travel to St. Petersburg for a medical conference that he had organized but which would not want to participate because of the depression in which he fell since his wifes premature death. There he comes into contact with Vito Calzone, an exuberant Sicilian who lives in the Russian city and organizes meetings with  local escorts on behalf of the Italian turists.

== Cast ==

*Carlo Verdone as Giulio Cesare Carminati
*Sergio Castellitto as  Fortunato Polidori 
*Riccardo Scamarcio as  Marcello Polidori / Walter Lo Russo Ksenia Rappoport as  Vera
*Dario Bandiera as  Vito Calzone
*Valeria Solarino as  Haifa
*Remo Girone as  Roviglione
* Elena Presti as  Rovigliones wife
* John Graham Harper as  Mr. Vandenheim 

==References==
 

==External links==
* 
  
 
 
 
 
 


 
 