Beneath Clouds
 
 
{{Infobox film
| name           = Beneath Clouds
| image          = beneathcloudsposter.jpg
| caption        = DVD cover
| director       = Ivan Sen
| producer       = Teresa-Jayne Hanlon
| writer         = Ivan Sen
| starring       = Dannielle Hall Damian Pitt
| music          = Ivan Sen Alister Spence
| cinematography = Allan Collins
| editing        = Karen Johnson
| released       = 2002
| runtime        = 90 minutes
| country        = Australia
| language       = English
| music          = 
| budget         =  
| gross          = A $548,416  (Australia)
|}}

Beneath Clouds is a 2002 film by Australian director Ivan Sen. It is the feature film debut by the two lead actors. Damian Pitt was approached by Sen on the streets of Moree, New South Wales, and had never acted before. Dannielle Hall was cast through a more traditional method, via an audition tape. Much of the support cast were local residents from Pitts hometown of Moree.

==Plot synopsis==
Lena has an absent Irish father she longs to see and an Aboriginal mother she finds disgusting. When she breaks away, she meets up with petty criminal Vaughn whos just escaped from low security prison to reluctantly visit his dying mother.
Blonde and light-skinned, Lena remains in denial about her Aboriginal heritage; Vaughn is an angry young man with a grudge against all whites. An uneasy relationship begins to form as they hit the road heading to Sydney, taking them on a journey thats as emotional as it is physical, as revealing as it is desperate.

Initially the two reluctant travelling companions are suspicious and wary of each other, but their journey, mostly by foot and the odd lift, builds an understanding between them.
The film follows its creators (Ivan Sens) own experiences growing up in Inverell, NSW with an Aboriginal mother and a European father who was not around.

==Production==
The film was financed by the New South Wales Film Commission and the Australian Film Finance Corporation.

==Reception==
The Rotten Tomatoes Want-To-See score is currently 79%, but no official approval rating has been announced to this day. Only two film critics have posted reviews on the site, both positive. Urban Cinefile Critics commented "Displaying about equal amounts of naiveté, passion and talent, Beneath Clouds establishes Sen as a filmmaker of considerable potential". Andrew Howe of eCritic.com called it "One of the few feature films to canvass the issues facing the aboriginal community from an adolescent perspective".

===Accolades===

{| class="wikitable"
|-
! Award
! Category
! Subject
! Result
|- AACTA Awards  (2003 AFI Awards)  AACTA Award Best Film
|Teresa-Jayne Hanlon
| 
|- AACTA Award Best Direction Ivan Sen
| 
|- AACTA Award Best Original Screenplay
| 
|- AACTA Award Best Actress Dannielle Hall
| 
|- AACTA Award Best Cinematography Allan Collins
| 
|- AACTA Award Best Original Music Score Alister Spence
| 
|- Ivan Sen
| 
|- Berlin International Film Festival First Movie Award
| 
|- Golden Berlin Bear
| 
|- New Talent Award Dannielle Hall
| 
|- Film Critics FCCA Awards Best Female Actor
| 
|- Best Director Ivan Sen
| 
|- Best Cinematography Allan Collins
| 
|- Best Music Score Alister Spence
| 
|- Ivan Sen
| 
|- Inside Film Awards Best Direction
| 
|- Best Actress Dannielle Hall
| 
|- Best Cinematography Allan Collins
| 
|- Best Editing Karen Johnson
| 
|-
|}

==Box office==
 Beneath Clouds  grossed $548,416 at the box office in Australia. 

==See also==
* List of Australian films
* Cinema of Australia

==References==
 

==External links==
 
*  

 
 
 
 