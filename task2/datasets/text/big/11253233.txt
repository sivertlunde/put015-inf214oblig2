The Snows of Kilimanjaro (1952 film)
{{Infobox film
| name           = The Snows of Kilimanjaro
| image          = The Snows of Kilimanjaro.jpg
| image_size     = 
| caption        = Lobby Card Henry King
| producer       = Darryl F. Zanuck
| screenplay     = Casey Robinson
| based on       =  
| narrator       = Gregory Peck
| starring       = Gregory Peck Ava Gardner Susan Hayward
| music          = Bernard Herrmann
| cinematography = Leon Shamroy
| editing        = Barbara McLean
| distributor    = Twentieth Century Fox
| released       =  
| runtime        = 117 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $6.5&ndash;$12.5 million  
}}
 short story Henry King, written by Casey Robinson, and starred Gregory Peck as Harry, Susan Hayward as Helen, and Ava Gardner as Cynthia Green (a character invented for the film). The films ending does not mirror the books ending. 

Considered by Hemingway to be one of his finest stories, "The Snows of Kilimanjaro" was first published in Esquire (magazine)|Esquire magazine in 1936 and then republished in The Fifth Column and the First Forty-Nine Stories (1938).
 Best Cinematography, Best Art Direction, Color (Lyle R. Wheeler, John DeCuir, Thomas Little, Paul S. Fox).

The film has entered the public domain. 

==Plot==
 
The film begins with the opening words of Hemingways story:  "Kilimanjaro is a snow-covered mountain 19,710 feet high, and is said to be the highest mountain in Africa. Its western summit is called the Masai Ngje Ngi, the House of God. Close to the western summit there is the dried and frozen carcass of a leopard. No one has explained what the leopard was seeking at that altitude." 

The story centers on the memories of disillusioned writer Harry Street (Gregory Peck) who is on safari in Africa. He has a severely infected wound from a thorn prick, and lies outside his tent awaiting a slow death. The loss of mobility brings self-reflection. He remembers past years and how little he has accomplished in his writing. He realizes that although he has seen and experienced wonderful and astonishing things during his life, he had never made a record of the events. His status as a writer is undermined by his reluctance to actually write. He also quarrels with the woman with him, blaming her for his living decadently and forgetting his failure to write of what really matters to him: his experiences among poor and "interesting" people, rather than the smart Europeans with whom he has been lately.

He lives to see morning come. He watches vultures gather in a tree as he lies in the evening. He recapitulates his life and talks to his current girl-friend Helen (Susan Hayward). He tells her about his past experiences; then arguing, then coming to realization about his attitude, and finally reaching a sort of peace, even love, with her.

==Cast==
 
* Gregory Peck as Harry Street
* Susan Hayward as Helen
* Ava Gardner as Cynthia Green
* Hildegard Knef as Countess Elizabeth
* Emmett Smith as Molo
* Leo G. Carroll as Uncle Bill
* Torin Thatcher as Mr. Johnson
* Marcel Dalio as Emile
* Leonard Carey as Dr. Simmons
* Paul Thompson as Witch Doctor
* Ava Norring as Beatrice
* Helene Stanley as Connie
* Vicente Gómez as Guitarist (as Vicente Gomez)
* Richard Allan as Spanish Dancer

==Pre-production==
  Twentieth Century-Fox bought the rights to the story in June 1948, paying $125,000.   Humphrey Bogart, Richard Conte and Marlon Brando were all reported to be under consideration for the male lead, as was Dale Robertson. 

==Production== Pacific Palisades home, and Hildegard Knef came down with influenza in the studios.       She was able though to sing two Cole Porter tunes in the film.    Jazz musician Benny Carter performs early on in the film.   

==Post-production== archive footage, Blood and Sand. 

==Reception==
  Best Cinematography Best Art Direction (Lyle R. Wheeler, John DeCuir, Thomas Little, Paul S. Fox).    The film was much acclaimed by critics, although some vary in their opinion of it, ranging from "simply plodding" to "much-maligned".     The cinematography was highly acclaimed in particular, and even the sophisticated interiors were praised.     Bosley Crowther of The New York Times described the cinematography as "magnificent and exciting" and said that the "overall production in wonderful color is full of brilliant detail and surprise and the mood of nostalgia and wistful sadness that is built up in the story has its spell."  He praised Pecks character for his "burning temper and melancholy moods", although he said that Ava Gardner was "pliant and impulsive" in a role "as soggy and ambiguous as any in the film".  Bowkers Directory described it as having "plenty of action & romance" and stated that it was "the popular celebrity film of its time".    Hemingway, who disliked the typical Hollywood happy ending, accepted the money for the film, but he could not bring himself to view the film.   

==Home media== Under My The Sun A Farewell to Arms, and Hemingways Adventures of a Young Man.   

==References==
{{reflist|colwidth=30em|refs=
   
}}

==External links==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 