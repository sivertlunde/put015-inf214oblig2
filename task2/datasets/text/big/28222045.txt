Io non spezzo... rompo
{{Infobox film
| name           = Io non spezzo... rompo
| image          = Io non spezzo rompo 1971.jpg
| caption        = Film poster
| director       = Bruno Corbucci
| producer       = Dino De Laurentiis
| writer         = 
| starring       = Alighiero Noschese
| music          = Gianni Ferrio
| cinematography = Luciano Tovoli
| editing        = Tatiana Casini Morigi
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Io non spezzo... rompo is a 1971 Italian comedy film directed by Bruno Corbucci. It was shown as part of a retrospective on Italian comedy at the 67th Venice International Film Festival.   

==Cast==
* Alighiero Noschese - Riccardo Viganò
* Enrico Montesano - Attilio Canepari
* Janet Agren - Carla Viganò
* Claudio Gora - Frank Mannata
* Lino Banfi - Zagaria - policeman from Apulia
* Gino Pernice - Policeman from Liguria
* Anna Campori - Elena - wife of Riccardo
* Giacomo Furia - Policeman from Naples
* Gordon Mitchell - Joe il Rosso
* Ignazio Leone - Mazzetti
* Mario Donatone - Tony Cupiello
* Sandro Dori - Rick

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 