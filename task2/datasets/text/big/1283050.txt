Clerks II
{{Infobox film name           = Clerks II image          = Dandrposter.jpg image_size     = 215px alt            =  caption        = Theatrical release poster director       = Kevin Smith producer       = Scott Mosier Laura Greenlee Carla Gardini Bob Weinstein Harvey Weinstein writer         = Kevin Smith starring  Jennifer Schwalbach music          = James L. Venable cinematography = Dave Klein editing        = Kevin Smith studio         = View Askew Productions distributor    = The Weinstein Company Metro-Goldwyn-Mayer (theatrical distributor only) released       =   runtime        = 97 minutes country        = United States language       = English budget         = $5 million  gross          = $26.9 million 
}}
Clerks II is a 2006 American  , Randal Graves and Jay and Silent Bob 10 years after the events of the first film. Unlike the first film, which was shot in black-and-white, this film was shot in color. The film was released on July 21, 2006. It screened out of competition at the 2006 Cannes Film Festival    and won the Audience Award at the 2006 Edinburgh International Film Festival. 

==Plot== first film, Dante (Brian Randal (Jeff Becky Scott Jennifer Schwalbach), whose father will provide them with a home and a business to run. This leaves Randal bitterly disappointed, who fears that with Dante moving to Florida would leave him without his best friend. Jay and Silent Bob (Jason Mewes and Kevin Smith) have since followed Dante and Randal, and now loiter outside of Moobys. Jay and Silent Bob no longer do drugs after they were arrested for the possession of drugs and were sent to Drug rehabilitation|rehab, and become devout Christians following their release. However, the duo continue selling drugs.

Dante eventually confesses to Becky that he is worried about dancing at his wedding, so she takes him up onto the roof of the restaurant to teach him some moves. Dante soon lets go of his inhibitions and learns how to dance. When the song ends, Dante, caught up in the moment, tells Becky he loves her, and she reveals to him that she is Pregnancy|pregnant; Dante and Becky had a one night stand on the prep table a few weeks before. Becky tells Dante not to tell anyone about the baby; however Dante tells Randal, and an angered Becky leaves when she learns that Dante told Randal.
 Zak Knutson), whom Randal thought to be the pimp, is "The Sexy Stud". When Becky returns, Dante confesses his love for her. As they kiss, Emma arrives. She throws her engagement ring at Becky and angrily walks off.
 Today is the first day of the rest of our lives."

==Cast== Jason Lee make cameo appearances as Moobys customers as does comedian/actress Wanda Sykes.

* Brian OHalloran as Dante Hicks
* Jeff Anderson as Randal Graves Becky Scott Elias Grover Jennifer Schwalbach Emma Bunting Jay
* Silent Bob Jason Lee Lance Dowds Zak Knutson as Sexy Stud
* Wanda Sykes as Angry customer Earthquake as Angry customers husband
* Ethan Suplee as Weed customer
* Ben Affleck as Gawking guy
* Kevin Weisman as Lord of the Rings geek

==Production== Catch and Release.
 links below). They chronicle the entire making of the film from the first rehearsals all the way through to the final release. Some of these web diaries are also available on the two-disc DVD of the film. Smith released a Web-only teaser trailer on the Clerks II website on January 9, 2006,    and a web-only trailer on April 2, 2006.  Smith also released several shorts featuring action figures from his previous films to promote the film.

Before the release of the film, Smith had mentioned releasing an MP3 file commentary to be downloaded and listened to in movie theaters via iPod. Ultimately, theater owners and exhibitors objected,  and the plan was scrapped. The abandoned commentary, featuring Smith, Scott Mosier and Jeff Anderson, is included on the DVD.
 desaturated almost to the point where the film had a similar texture to the first film.  The contrast in color saturation used can be seen in the "ABC (song)|ABC" sequence in which a more vibrant and saturated color temperature is used to give a warm and sunny look that adds to the playful nature of the piece.

===Locations===
The Moobys restaurant was a shut-down Burger King at 8572 Stanton Ave in Buena Park, California,  (near Knotts Berry Farm), which has since been demolished.  The final days of principal photography were filmed at the Quick Stop and R.S.T. Video store in Leonardo, New Jersey, with some exceptions, the most notable being the go-kart scene, which was shot at Speedzone in Industry, California.  The opening sequence where Randal and Dante are driving to work is a montage of Route 35, mostly in Middletown, NJ.

===Casting===
Brian OHalloran, Jeff Anderson, Jason Mewes, and Kevin Smith all reprised their roles from the first film.

According to the DVD commentary ,  Kevin Smith originally wanted to cast his wife Jennifer Schwalbach Smith as Becky. Executive producer Harvey Weinstein objected, however, as he wanted a known actress to play the role, for marketing reasons. Other actresses that Smith had met with were Sarah Silverman and Bryce Dallas Howard, who both declined. Smith recalls having lunch with Howard, who said she was interested in the film but ultimately passed in order to do Lady in the Water. Silverman said she didnt want to play the character Becky as she had been cast as girlfriends in numerous other productions and feared type-casting, but loved the script and would have been more than willing to play the part of Randal. Rachel Weisz was another name the studio considered, but Smith figured she would turn the role down and never offered her the part. Ellen Pompeo expressed an interest but could not commit due to scheduling difficulties with Greys Anatomy.  Finally, the role was offered to Rosario Dawson, who loved the script. She later said that reading the "donkey show" scene sealed the deal for her. Jennifer Schwalbach Smith was given the secondary female role of Emma. Smith also cast his daughter Harley as the little girl Dante waves to in the window and his mother Grace as the Milk Maid, reprising her role from the first film.
 Now You Jason Lee and Ben Affleck had parts in the film. Lee played Randals old enemy, Lance Dowds, and Affleck played a random Moobys customer. After finding no one else who could pull off being the Sexy Stud, Smith turned to crew member Zak Charles Knutson to fill the role.
 Weinstein Company logos. The names are not present in the credits on the Region 2 DVD. 

==Release==

===Rating=== MPAA film rating, in order to avoid receiving an NC-17. Smith claimed "If we put it in front of the ratings board theyd be like, Youre insane. We have to create a new rating for that."  However, he later submitted it, and it received an R rating without any edits.

===Box office===
The film opened in 2,150 theaters and grossed $10,061,132 domestically in its first weekend.  The films theatrical gross was $24,148,068 domestically, plus an additional $2,833,903 foreign,   from Box Office Mojo  turning a profit on its reported budget of $5 million.

===Critical response===
The film received generally positive reviews from critics.       At review aggregator website Rotten Tomatoes, reviews for Clerks II were mostly positive, with the film rated "fresh" overall (63% positive).  , the critical consensus was that the film "dishes up much of the graphic humor and some of the insight that made the 1994 original a cult hit".  Metacritic characterised the films reception as "generally favorable", with metascore of 65 out of 100 from 29 reviews. 

In a review for The New York Times, A. O. Scott notes the following: What makes "Clerks II" both winning and (somewhat unexpectedly) moving is its fidelity to the original "Clerks" ethic of hanging out, talking trash and refusing all worldly ambition. If anything, the sequel is more defiant in its disdain for the rat race, elevating the white-guy-doing-nothing prerogative from a lifestyle choice to a moral principle. 
}}

Justin Changs review at Variety (magazine)|Variety called it a "softer, flabbier and considerably higher-budgeted follow-up to Kevin Smiths 1994 indie sensation that nevertheless packs enough riotous exchanges and pungent sexual obscenities to make its 97 minutes pass by with ease." 
 The Village before having seen it.  Smith later confronted Siegel in a live interview on Opie and Anthony; Siegel apologized for cursing and causing a scene, and told Smith that he thinks he is a "fine filmmaker," while still defending his decision to walk out. 
 IMDb users  and the 9th best reviewed comedy by Rotten Tomatoes. 

The film received an eight-minute standing ovation at the 2006 Cannes Film Festival.  

==Soundtrack==
{{Infobox album  
| Name = Music from the Motion Picture  Clerks II 
| Type = Soundtrack
| Longtype = to the film Clerks II
| Cover = Clerks_II_OST_cover.jpg 
| Released = August 22, 2006
| Recorded = Various
| Genre = Various
| Length = 56:41
| Label = Bulletproof Records|Bulletproof, Fontana North
| Producer =
| Reviews =
| Chronology = Clerks soundtrack
| Last album = Clerks#Soundtrack|Clerks (1994)
| This album = Clerks II (2006)
| Next album =
}}
Music from the Motion Picture Clerks II, the soundtrack to the film, was released on August 22, 2006 by Bulletproof Records. It includes songs from the film, which are of various artists and genres, as well as many soundclips of dialog from the film. One notable exception is that The Smashing Pumpkins "1979 (song)|1979", which was featured in the film, is not included. It has been replaced by All Too Muchs "Think Fast", which was not featured in the film.

# Dialogue: "Anne Frank vs. Helen Keller" &ndash; 0:27
# "(Nothing But) Flowers" &ndash; Talking Heads &ndash; 5:33
# Dialogue: "Regret" &ndash; 0:28 Welcome Home" &ndash; King Diamond &ndash; 4:36
# Dialogue: "Of Parts Enlarged" &ndash; 0:17
# "Neckin on the Swing" &ndash; James L. Venable &ndash; 3:49
# Dialogue: "The First of the Fallen" &ndash; 0:55 The Invisible Guests" &ndash; King Diamond &ndash; 5:04
# Dialogue: "The Unholiest Act" &ndash; 0:52
# "Goodbye Horses" &ndash; Q Lazzarus & Garvey &ndash; 6:27
# Dialogue: "On Knowing Pickles" &ndash; 0:17
# "Raindrops Keep Fallin on My Head" &ndash; B. J. Thomas &ndash; 3:02
# Dialogue: "Twelve Step" &ndash; 0:20
# "ABC (song)|ABC" &ndash; The Jackson 5 &ndash; 2:58
# Dialogue: "The Perfect Gift" &ndash; 0:54
# "Think Fast" &ndash; All Too Much &ndash; 3:24
# Dialogue: "Party Planning" &ndash; 0:31
# "Goin Away Party" &ndash; James L. Venable &ndash; 1:44 - This contains segments of the "Clerks" animated series theme song, also by Venable
# Dialogue: "Im Gonna Miss You, Man" &ndash; 0:39
# "Naughty Girls (Need Love Too)" &ndash; Samantha Fox &ndash; 3:21
# Dialogue: "Abstinence" &ndash; 1:01
# "Everything (Alanis Morissette song)|Everything" &ndash; Alanis Morissette &ndash; 4:36
# Dialogue: "Semantics" &ndash; 0:31
# "Misery (Soul Asylum song)|Misery" &ndash; Soul Asylum &ndash; 4:24
# Dialogue: "Battle of the Mega-Geeks" &ndash; 0:31

==Home media==
 
The Clerks II DVD was released on November 28, 2006. 

The Hollywood Reporter reported that the film opened to #4 in terms of rental and DVD sales, and made approximately $6 million in rentals, or a quarter of the total box office gross of $24.2 million. 
 high definition on one disc and the same extras as the DVD, also presented in 1080p, on a second disc.  After the conclusion of the high definition optical disc format war in February 2008, Clerks II was released on Blu-ray Disc on February 3, 2009 with two additional special features. 

==Sequel==
 
On March 12, 2015 Smith revealed he would film Clerks III in May 2015. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
* View Askewniverse:
**  
**  
*   from ljworld.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 