Cinderella Blues
{{Infobox Hollywood cartoon|
| cartoon_name = Cinderella Blues
| series = Aesops Fables (film series)|Aesops Fables
| image = 
| caption = 
| director = John Foster Harry Bailey
| story_artist = 
| animator = 
| voice_actor = 
| musician = 
| producer = Amadee J. van Beuren
| studio = Van Beuren Studios
| distributor = RKO Radio Pictures
| release_date = April 12, 1931
| color_process = Black and white
| runtime = 8:06 English
| preceded_by = Old Hokum Bucket
| followed_by = Mad Melody
}}

Cinderella Blues is an animated short subject produced by the Van Beuren Studio and distributed by RKO Radio Pictures. It retells the Cinderella story by Charles Perrault. Unlike most adaptations, the cartoon features a fabled version of the story.

==Plot==
A weepy feline Cinderella sits around in her murky house. Cinderella, or Cindy which she is sometimes called, hardly does anything other than household work under the authority of her bossy old stepmother. She also longs for a more pleasant livelihood.

While standing in the living room one day, a pixie appears and asks Cindy if she would like to go the ball. Cindy expresses interest but laments she has nothing to wear. The pixie puts forth a wand and transforms her rag outfit into a fancy dress. A luxury car with classy chauffeurs also appears, and the delighted young girl cat goes aboard.

The ball appears to be inside a castle. Cindys date turns out to the prince who is dressed like a 16th Century Spaniard. They then walk to the main room where musicians and other dancing guests are in attendance. After dancing, they head to a backyard which is like a forest. When the prince kisses her twice, Cindy merrily starts distancing herself and goes to hide in the trees. While the prince struggles to find her, Cindy heads to a nearby a clock, therefore finding out she has to leave immediately. As she flees, one of her shoes slip off and is left behind. The shoe is momentarily picked up by the prince who arrives at the scene.

Cindy leaves in her car, attempting to reach home. But when midnight strikes, she is reverted to her old rag clothing, and her once luxurious vehicle is reduced to some 1890s Daimler model with her being the one driving it. The car momentarily breaks down, prompting Cindy to run on foot. While she goes on running, an anchor lowers and picks her up. It appears the one who lifted her is the prince who is on a helicopter. The prince then helds out the shoe he found, and places it on Cindys foot. As a miracle, Cindy is in her fancy dress again. They then kiss each other and fly back to the castle.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 