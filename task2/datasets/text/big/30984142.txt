She Went to the Races
{{Infobox film
| name           = She Went to the Races
| image          =  She Went to the Races - Film Poster.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster 
| director       =  Willis Goldbeck
| producer       =  Frederick Stephani 	
| writer         =  Alan Friedman (story)   Lawrence Hazard 	  Buster Keaton (uncredited)   DeVallon Scott (story) 
| narrator       = James Craig  Frances Gifford   Ava Gardner   Edmund Gwenn
| music          =  Nathaniel Shilkret 	
| cinematography =  Charles Salerno Jr. 
| editing        =  Adrienne Fazan 
| distributor    = Metro-Goldwyn-Mayer
| released       =  4 November 1945
| runtime        =   86 min
| country        = United States English
| budget         =  
| gross          =  
}}
 American comedy James Craig, Frances Gifford and Ava Gardner.  A team of scientists discover a seemingly foolproof way of discovering the winner of horse races.

==Plot Summary==

Dr. Ann Wotters, working at the Brockhurst Institute of Research in Los Angeles, is devastated when she learns that her uncle, Dr. Homer Pecke, will be let off because the institute cant afford to keep him on staff and his research project running. The institute would need another $20,000 to afford Peckes services.

Ann and her uncles colleagues Dr. Gurke, Dr. Pembroke and Dr. Collyer, put their sharp heads together to come up with a plan to raise the money needed. They get unexpected help from the institute janitor, who has had recent success at the racetrack. To ensure their wins at the racetrack, the uncles scientist colleagues try to come up with a fool proof formula to foresee which horses will win the races.

The four scientists then decide to try their "luck" at the Pasadena racetrack. They discover that there are no available hotel rooms left in the city, but Ann manages to persuade the staff of a hotel to let her into the room reserved for Steve Canfield, who is a race horse owner. When Steve checks in, Ann persuades him to let her keep one of the rooms in his suite.

Steves old girlfriend and horse owning colleague Hilda is also staying at the hotel. She is keen on reuniting with Steve, but Ann and Steve are starting to get romantically involved. Meanwhile, the three other scientists are trying to decide which horse to bet on and eventually decide on Steves horse, Mr. McGillicudy. When Steve hears about this, he tries to persuade Ann not to bet on his horse, afraid that he will be blamed for her losing money if his horse loses.

Homer goes missing from his home in Los Angeles, then turns up at the racetrack in Pasadena, too, and makes contact with Steves horse trainer Jeff Habbard. Homer tells Ann that he has bet on Steves horse because Steve himself has bet on it.  Ann has followed Steves advice, however, and bet on another horse, losing their money when Mr. McGillicuddy wins. Ann is furious and suspicious towards Steve after this. Steve feels bad, not knowing that Uncle Homer was misinformed about his betting on his own horse. The relationship between Ann and Steve comes to an abrupt halt, giving Hilda greater leverage for reconciliation with Steve.  Even though Hilda is still in love with "the Count" and Steve with Ann, Hilda proposes they marry and merge their racing interests.

Although Homer wins on Steves horse, the scientists havent won enough money to get Homer reinstated at the institute. Ann goes to Steve to borrow the rest of the money from him, but finds him and Hilda drinking and on the floor in Hildas room. Steve, still unaware that Ann thinks he bet on his own horse, catches Ann off guard by offering her the money before she can ask.  Elated at first, Ann storms off without the money when Steve tells her that he is now engaged to Hilda.

At the next race, Ann asks Steve to place another bet for her on Mr. McGillicuddy, then challenges Hilda to withdraw her horse from a joint entry with McGillicudy to prove she isnt marrying Steve for the money. The scientists find that they have miscalculated, i.e., 2 + 2 = 5, and have placed the wrong bet.  Hilda accepts the challenge and they decide to bet the outcome of the race -- the one who has bet on the winning horse gets to be with Steve. Steve returns, having saved the scientists by betting on the horse he thought would win.  Just before the race begins, Ann learns that the institute has found money to reinstate Homer.  Mr. McGillicudy wins the race, Ann and the scientists win their bets, and Steve and Ann reunite with a kiss in the winners circle. 

==Cast== James Craig - Steve Canfield
* Frances Gifford - Doctor Wotters
* Ava Gardner - Hilda Spotts
* Edmund Gwenn - Doctor Pecke
* Sig Ruman - Doctor Gurke
* Reginald Owen - Dr. Pembroke
* J.M. Kerrigan - Jeff Habbard
* Charles Halton - Doctor Collyer
* Chester Clute - Wallace Mason
* Frank Orth - Bartender Skelly Joe Hernandez - Himself

==References==
 

==External links==
* 

 
 
 
 
 
 