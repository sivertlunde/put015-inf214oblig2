The Versace Murder
{{Infobox film
| name           = The Versace Murder
| image          = The Versace Murder.jpg
| image_size     = 
| alt            = 
| caption        = DVD cover
| director       = Menahem Golan
| producer       = Stuart Goldstein Sam Lupowitz Charles Rosenberg Robert L. Street
| writer         = Menahem Golan Bill Mesce Jr.  
| narrator       = 
| starring       = Franco Nero Steven Bauer
| music          = Claudio Simonetti
| cinematography = William G. Randall
| editing        = Michael Yanovich
| studio         = Splash Films Pan Am Pictures
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = United States
| language       =
| budget         = $4,000,000
| gross          =
| preceded_by    = 
| followed_by    = 
}}
The Versace Murder is a 1998 film written and directed by Menahem Golan.

==Plot==
Spree killer Andrew Cunanan (Shane Perdue) leaves a trail of murder victims as he travels from San Francisco to Miami, finally killing world-famous fashion designer Gianni Versace (Franco Nero).  The film recounts Cunanans life before and after the murder, including details on his four other victims and his efforts to evade a nationwide manhunt that would end in his suicide.

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Franco Nero || Gianni Versace
|-
| Steven Bauer || FBI Agent John Jacoby 
|-
| Shane Perdue || Andrew Cunanan
|-
| Matt Servitto  || David Madson
|-
| Oscar Torre || Antonio DAmico
|-
| David Anthony Pizzuto || Chief William Roberts 
|-
| Dania Deville ||  Donatella Versace
|}

==DVD availability==
Although the film is not available on some sites such as Netflix, it can be bought at online retailers such as Amazon.com.  

==References==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 
 
 


 