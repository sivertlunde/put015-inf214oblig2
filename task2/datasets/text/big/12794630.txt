You Can't Run Away from It
{{Infobox film
| name           = You Cant Run Away from It
| image          = You Cant Run Away from It poster.jpg
| image size     = 215px
| caption        = theatrical poster
| director       = Dick Powell
| producer       = Dick Powell
| writer         = Samuel Hopkins-Adams (story) Robert Riskin (prev. screenplay) Claude Binyon (screenplay)
| starring       = June Allyson Jack Lemmon
| music          = Johnny Mercer, lyrics, Gene DePaul, music (songs) George Duning (scoring)
| cinematography = Charles Lawton Jr.
| editing        = Al Clark
| distributor    = Columbia Pictures
| released       = October 31, 1956
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $1.45 million (US) 
}}

You Cant Run Away from It is a 1956 Technicolor and CinemaScope musical comedy starring June Allyson and Jack Lemmon. Directed and produced by Dick Powell, the film is a remake of the 1934 Academy Award-winning film It Happened One Night. The supporting cast features Charles Bickford, Jim Backus, Stubby Kaye, Jack Albertson and Howard McNear. It Happened One Night had also been remade as a musical comedy in 1945 as Eve Knew Her Apples.

==Plot==
Because she married an international playboy, Ellie Andrews (June Allyson) is kidnapped by her own father, Texas cattleman A. A. Andrews (Charles Bickford). She escapes, managing to evade his nationwide search for her with the help of Peter Warne (Jack Lemmon), a jobless reporter, who sees himself getting the biggest story of the year - until he and Ellie fall in love. When Ellie suspects Peter has sold her out, she returns home. Realizing his daughter really loves the newspaperman, Andrews tries to persuade Ellie to run away again, this time from her own wedding ceremony. Who will Ellie choose, her husband or the man who has stolen her heart?

==Cast==
*June Allyson as Ellie Andrews
*Jack Lemmon as Peter Warne
*Charles Bickford as A.A. Andrews Paul Gilbert as George Shapely
*Jim Backus as Danker
*Stubby Kaye as Fred Toten
*Jack Albertson as Third proprietor
*Queenie Smith as the elderly lady
*Frank Sully as Red
*Howard McNear as Vernon
*Jacques Scott as Jacques Jack Ballarino
*The Four Aces as Themselves
*Barrie Chase as Western Union Girl (uncredited)

==Soundtrack==

Decca Records issued selections from the soundtrack on one side of an Lp Record, with music from other film scores on the reverse. 

Selections include:

*"You Cant Run Away from It"
Performed by The Four Aces

*"Howdy Friends and Neighbours"
Performed by Stubby Kaye, June Allyson, and Jack Lemmon

*"Thumbin a Ride"
Performed by June Allyson and Jack Lemmon

*"Temporarily"
Performed by June Allyson and Jack Lemmon

*"Scarecrow Ballet"
Performed by Morris Stoloff conducting the Columbia Studio Orchestra

These selections were reissued on CD by Decca Broadway, paired with the Broadway cast album of Texas Lil Darlin. 

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 
 