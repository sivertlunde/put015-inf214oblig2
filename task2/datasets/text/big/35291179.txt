Meri Zabaan
{{Infobox film
 | name = Meri Zabaan
 | image = MeriZabaanfilm.jpg
 | caption = Promotional Poster
 | director = Shibbu Mitra
 | producer = Veena Sharma Vikas
 | writer = 
 | dialogue = 
 | starring = Mithun Chakraborty Shashi Kapoor Farha Kimi Katkar Tanuja Vinod Mehra Amjad Khan Sharat Saxena Rakesh Bedi
 | music = Anu Malik
 | lyrics = 
 | cinematographer = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released       =  
 | country = India
 | runtime = 140 min.
 | language = Hindi Rs 4 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1989 Hindi Indian feature directed by Shibbu Mitra, starring Mithun Chakraborty, Shashi Kapoor, Farha, Kimi Katkar, Tanuja, Vinod Mehra, Amjad Khan and Sharat Saxena. The film has been produced by Veena Sharma. . 

==Plot==

Meri Zubaan is an action thriller, featuring Mithun Chakraborty and Shashi Kapoor in lead roles, well supported by Farha, Kimi Katkar, Tanuja, Vinod Mehra and Amjad Khan.

==Cast==

*Shashi Kapoor...... Raja Vijay Singh
*Mithun Chakraborty...... Krishna
*Farha...... Baby
*Kimi Katkar...... Kimi/Rita
*Tanuja...... Mrs. Vikram Singh
*Ranjeet...... Ranjeet Mehra
*Vinod Mehra...... Vikram Singh
*Amjad Khan...... Inspector Malpani/Arjun Vaswani
*Rakesh Bedi...... Gone
*Beena Banerjee|Beena...... Doctor in mental hospital
*Sharat Saxena
*Jagdeesh Raj
*Ghanshyam...... Havaldar
*Rajan Haksar
*Dev Kumar...... Wrestler

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yeh Bhi Mujhe Chahey Baba"
| Anu Malik, Alisha Chinai
|-
| 2
| "Zindagi Pyar Ka"
| Anu Malik, Alisha Chinai
|-
| 3
| "Dam Mara Dam"
| Anu Malik, Alisha Chinai
|-
| 4
| "Honthon Pe Naam Pyar Ka"
| Anu Malik, Alisha Chinai
|-
| 5
| "Jhoolelal-Jhoolelal"
| Anu Malik, Alisha Chinai
|}

==References==
 
* http://www.bollywoodhungama.com/movies/cast/5321/index.html

==External links==
* 

 
 
 
 

 