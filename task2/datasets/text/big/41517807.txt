The Children of Sanchez (film)
{{Infobox film
| name           = The Children of Sanchez
| image          = Film poster for The Children of Sanchez.jpg
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Hall Bartlett
| producer       = 
| writer         = Hall Bartlett  Cesare Zavattini	 	
| screenplay     = Cesare Zavattini, Hall Bartlett
| story          = 
| based on       = a book by Oscar Lewis
| narrator       = 
| starring       = Anthony Quinn Dolores del Río  Katy Jurado  Lupita Ferrer
| music          = Chuck Mangione
| cinematography = Gabriel Figueroa
| editing        = Marshall M. Borden	 	
| studio         = 
| distributor    = 
| released       =  
| runtime        = 126 minutes
| country        = United States Mexico
| language       = English
| budget         = 
| gross          = 
}} the book and with the same title by Oscar Lewis. The film was entered into the 11th Moscow International Film Festival.   

==Reception==
Among those in attendance at the films American premiere on November 16, 1978 were President Jimmy Carter and Rosalynn Carter, who were greeted at the premiere by Quinn, Ferrer and director Bartlett. Quinn himself escorted the President and the First Lady to their seats, and all proceeds went to the Mexican Legal Defense and Educational Fund. 
The musical score for the film was written by Chuck Mangione and won a Grammy award. The films title song was also written by Mangione and earned him a Grammy for Best Pop Instrumental Performance. 

==Cast==
* Anthony Quinn as Jesús Sánchez
* Dolores del Río as Grandma Paquita
* Katy Jurado as Chata
* Lupita Ferrer as Consuelo Sánchez
* Stathis Giallelis as Roberto
* Bette Davis uncredited

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 