A Bell for Adano
 
 
{{Infobox film
| name           = A Bell for Adano
| image          = A Bell for Adano.jpg
| image size     =
| caption        = Theatrical poster for A Bell for Adano (1945) Henry King
| producer     = Louis D. Lighton Lamar Trotti
| screenplay    = Lamar Trotti Norman Reilly Raine
| based on      =  
| narrator       =
| starring       = Gene Tierney John Hodiak William Bendix Alfred Newman
| cinematography = Joseph LaShelle
| editing        = Barbara McLean
| distributor    = 20th Century Fox
| released       =  
| runtime        = 103 minutes
| country        = United States English
| budget         =
| gross          =
}} Henry King novel of Pulitzer Prize for fiction in 1945. In his review of the film for The New York Times, Bosley Crowther wrote, "... this easily vulnerable picture, which came to the Music Hall yesterday, is almost a perfect picturization of Mr. Herseys book." 
 invasion of Fascists at the war to be melted down for ammunition. Through his actions, Joppolo also wins the trust and love of the people.

Some of the changes Joppolo brings into the town include:
*Democracy
*Free fishing privilege
*The freedom of mule carts
*A bell from the American Navy to replace the town bell

The short-tempered American commander, General Marvin, fires Major Joppolo from his position when Joppolo disobeys an order to prohibit mule cart traffic in Adano, which has been disrupting Allied supply trucks, because the mule carts are vital to the survival of the town.

The character of Joppolo was based on the real life experiences of Frank Toscani, who was military governor of the town of Licata, Sicily after the Allied invasion. 

In A Bell for Adano, Major Joppolo and his men are given the task to bring back peace to the war-torn Italian town of Adano. His task is to administer the equipment that gets into the town without any intervention from troops. He wants to restore the bell that is central to the life of the people in Adano    

==Cast==
* Gene Tierney as Tina Tomasino
* John Hodiak as Maj. Victor P. Joppolo
* William Bendix as Sgt. Borth
* Glenn Langan as Lt. Crofts Livingstone
* Richard Conte as Nico 
* Stanley Prager as Sgt. Trampani Henry Morgan as Capt. N. Purvis
* Monty Banks as Giuseppe
* Reed Hadley as Cmdr. Robertson
* Roy Roberts as Col. W. W. Middleton 
* Hugo Haas as Father Pensovecchio
* Marcel Dalio as Zito
* Fortunio Bonanova as Chief of Police Gargano
* Henry Armetta as Errante
* Roman Bohnen as Carl Erba
* Luis Alberni as Cacopardo
* Eduardo Ciannelli as Maj. Nasta

==Production notes==
* Production Dates: Early Nov 1944–mid-Jan 1945
* Location filming was done at Brents Crags, near Malibu, California.
* Herseys novel was also the basis for Paul Osborns 1945 Broadway play A Bell for Adano, starring Fredric March. Barry Sullivan and Anna Maria Alberghetti and directed by Paul Nickell.
*On November 15, 1967, Hallmark Hall of Fame broadcast a version starring John Forsythe and Murray Hamilton and directed by Mel Ferrer.

==References==
 

==External links==
* 
* 
*  at Internet Archive

 

 
 
 
 
 
 
 
 
 
 
 
 
 