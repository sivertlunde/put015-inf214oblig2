Baby on Board (film)
{{Infobox film
| name           = Baby on Board
| image          = Babyonboardfilmposter1.jpg
| image_size     = 
| caption        = Promotional poster
| director       = Brian Herzlinger
| producer       = Emilio Ferrari  Russell Scalise
| writer         = Michael Hamilton-Wright  Russell Scalise
| narrator       =  Heather Graham John Corbett  Lara Flynn Boyle
| music          = Teddy Castellucci
| cinematography = Denis Maloney
| editing        = Ross Albert
| studio         = Screen Media
| distributor    = Lucky Monkey Pictures  National Entertainment Media
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Heather Graham, John Corbett, Jerry OConnell, Anthony Starke and Lara Flynn Boyle.

== Plot ==
The film centers around Angela Marks, an ambitious, image-conscious businesswoman working for over-demanding boss Mary. When Angela becomes unexpectedly pregnant at the peak of her career, her life with her divorce attorney husband, Curtis, is turned upside-down. The film begins with an inconvenient pregnancy that leads to a nine-month roller coaster ride as Angela and Curtis try to cope &mdash; even as the interference of best friends Danny and Sylvia escalate the situation into a battle of the sexes. 

==Credits==
Although credited as Lara Flynn Boyle in the opening credits, her name is spelled Laura Flynn Boyle in the closing credits.

==References==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 