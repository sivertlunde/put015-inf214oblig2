Meri Adalat (1984 film)
 
{{Infobox film|
| name = meri adalat
| image =
| caption =
| director = A.T.Raghu
| writer =
| starring =  
| producer =
| music =
| editor =
| released = 13 January 1984
| runtime =
| language = Hindi
| budget =  34 lakh
}}

Meri Adalat is a Hindi film is directed by A.T.Raghu. The film, released in 1984, was a revenge drama featuring Rajinikanth in the lead supported by Zeenat Aman, Mohnish Behl, Sonia Sahni and Komal Mahuakar.The song Gudiya Jaisi Behena hai sung by K.J.Yesydas was a big Hit.

==Plot==
It is the story of a brother played by Rajinikanth who is a tough cop and his sister played by Sonia Sahini.The sister is in love with her College friend played by Mohnish Behl.It is the loves story between them and the future incidents which make the cop take the law in his hand and have his own Adalat called Meri Adalat. Zeenat Aman a journalist plays the love interest of Rajinikanth supported by Kader Khan, Shreeram Lagoo, Rajendra Nath and others.

==Cast==
* Rajinikanth  as  as the Cop
* Zeenat Aman  as  as the Journalist
* Mohnish Behl  as  as lover
* Sonia Sahini  as  as Sister
* Kader Khan 
* Rajendra Nath 
* Shreeram Lagoo 
* Komal Mahuvakar

==Music==
{| border="4" cellpadding="6" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| Rajaji Raniji
| Asha Bhosle, Shabbir Kumar
|-
| 2
| Ek Baar Bach Gayi
| Asha Bhosle, Shabbir Kumar
|-
| 3 Gudiya Jaisi Behena
| Usha Mangeshkar, K.J.Yesudas
|-
| 4
| Osa Osa
| Asha Bhonsle, Bappi Lahiri
|-
| 5
| You are Beautiful
| Asha Bhosle, Kishore Kumar
|-
|}

==References==
 

==External links==
* 

 
 
 

 