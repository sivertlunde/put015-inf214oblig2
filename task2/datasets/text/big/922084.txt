Legend (1985 film)
{{Infobox film
| name           = Legend
| image          = Legendposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Ridley Scott
| producer       = Arnon Milchan
| writer         = William Hjortsberg
| starring = {{Plainlist|
* Tom Cruise
* Mia Sara
* Tim Curry
* David Bennent
* Alice Playten
* Billy Barty
* Cork Hubbert
}}
| music          = Jerry Goldsmith    Tangerine Dream   Alex Thomson
| editing        = Terry Rawlings Embassy International Pictures N.V. Universal Pictures    20th Century Fox  
| released       =  
| runtime        = 125 minutes   113 minutes   95 minutes   89 minutes  
| country        = United Kingdom United States
| language       = English
| budget         = $24.5 million 
| gross          = $15,502,112
}} fantasy adventure film directed by Ridley Scott and starring Tom Cruise, Mia Sara, Tim Curry, David Bennent, Alice Playten, Billy Barty, Cork Hubbert, and Annabelle Lanyon. It is a darker fairy tale and has been described as a return to more original, sometimes disturbing, fables, from the oral tradition of ancient times before reading and writing were widespread.   
 Best Makeup; cult classic. Wright, Benjamin (31 May 2012). " ."  IndieWire.com.  Retrieved 5 June 2012. 

==Plot==
"Once, long ago", the ancient Lord of Darkness (Tim Curry) laments his isolation in the shadows before sensing the presence of two unicorns who safeguard the Power of Light; whereupon Darkness instructs Blix and his fellow goblins to kill the unicorns and bring him their horns to free himself. Meanwhile, Princess Lili (Mia Sara) goes alone to the forest to meet her friend Jack (Tom Cruise), a forest dweller who teaches her the languages of animals before showing her the unicorns. Against Jacks pleas, Lili approaches the stallion before the unicorn is hit by a poisoned dart from Blixs Blowgun|blowpipe. The unicorns bolt, and Lili makes light of Jacks fears and sets him a challenge by throwing her ring into a pond, declaring that she will marry whoever finds it. As the stallion dies from the poison with the goblins taking his horn, the pond freezes, with Lili running off in terror before Jack can break free. Taking refuge in a frozen cottage, Lili overhears the goblins ordered to hunt down the mare.
 dwarves Brown Tom and Screwball (Cork Hubbert and Billy Barty), Jack searches for Lili before coming across the lifeless stallion and his mate. Knowing that the horn must be recovered and returned to the stallion, Jack is led by Gump and the others to obtain weapons while Brown Tom guards the mare. Lili is captured alongside the mare after Brown Tom is knocked out. Learning what has transpired, Jack and his group enter the ancient temple in the marshes where Darkness resides. Though Jack deals with the swamp hag Meg Mucklebones (Robert Picardo), he and his group fall into a pit trap that takes them to dungeon where they encounter Blunder, revealed to be a dwarf, before he is dragged off by the chefs to be cooked into a pie. Oona saves Jack and the others as they search for Lili and the mare.

Having fallen in love with Lili, Darkness tempts her; and Lili agrees to wed him on the condition that she kills the mare in the upcoming ritual. Jack and Gump learn that the antagonist can be destroyed by light. While saving Blunder, the group take giant metal platters to reflect the sunlight to the chamber where the mare is to be sacrificed. As the ritual begins, Lili frees the unicorn, and then is knocked out by Darkness. Jack attacks Darkness while the others relay the light of the setting sun. Once hit by the light, Darkness is expelled. As Gump returns the stallions horn, returning him to life, Jack retrieves the ring from the pond.

==Cast== Jack   
* Mia Sara as Princess Lili Darkness 
* David Bennent as Honeythorn Gump
* Alice Playten as Blix
** Alice Playten is also the uncredited voice of Gump because an executive thought that Bennents voice sounded too German. 
* Billy Barty as Screwball
* Cork Hubbert as Brown Tom
* Peter OFarrell as Pox
* Kiran Shah as Blunder
* Annabelle Lanyon as Oona
* Chris Lorch as Piecepypes
* Robert Picardo as Meg Mucklebones
* Tina Martin as Nell

==Production==

===Development=== Tristan and Isolde, fell through temporarily.    However, he felt that it was going to be an art film with limited audience appeal and went on to make Alien (film)|Alien and did pre-production work on Dune (film)|Dune, another halted project, which was eventually finished by director David Lynch. Frustrated, he came back to the idea of filming a fairy tale or mythological story.

For inspiration, Scott read all the classic fairy tales, including ones by the Brothers Grimm.  From that, he came up with an idea for a story about a young hermit who is transformed into a hero when he battles the Lord of Darkness in order to rescue a beautiful princess and release the world from a wintery curse.   

===Screenplay=== Beauty and the Beast. In January 1981, just before beginning principal photography on Blade Runner, Scott and Hjortsberg spent five weeks working out a rough storyline for what was then called Legend of Darkness.

Originally, Scott "only had the vague notion of something in pursuit of the swiftest steed alive which, of course, was the unicorn".  Scott felt that they should have a quest and wanted unicorns as well as magic armor and a sword. Hjortsberg suggested plunging the world into wintery darkness.  Hjortsbergs first draft of Legend of Darkness also had Princess Lili slowly transform into a clawed and fur-covered beast who is whipped and sexually seduced by the antagonist (called Baron Couer De Noir in this draft).  Scott wanted to show the outside world as little as possible and they settled on the clockmakers cottage.

Initially, the quest was longer, but it was eventually substantially reduced. Scott wanted to avoid too many subplots that departed from the main story and go for a "more contemporary movement rather than get bogged down in too classical a format".  By the time Scott had finished Blade Runner, he and Hjortsberg had a script that was "lengthy, hugely expensive, and impractical in its size and scope".  They went through it and took out large sections that were secondary to the story. The two men went through 15 script revisions.  

===Pre-production=== Snow White Alan Lee as a visual consultant who drew some characters and sketched environments. However, Scott eventually replaced Lee with Assheton Gorton, a production designer whom he had wanted for both Alien and Blade Runner. Scott hired Gorton because he knew "all the pitfalls of shooting exteriors on a soundstage. We both knew that whatever we did would never look absolutely real, but would very quickly gain its own reality and dispense with any feeling of theatricality". 

Scott also consulted with effects expert Richard Edlund because the director did not want to limit major character roles to the number of smaller people who could act.  At one point, the director considered Mickey Rooney to play one of the major characters but he did not look small enough next to Tom Cruise. Edlund came up with the idea of shooting on 70 mm film stock, taking the negative and reducing the actors to any size they wanted but this was deemed too expensive and Scott had to find an ensemble of small actors.  Universal Pictures agreed to finance and distribute Legend on a budget of $24.5 million. 
 Sequoias of Yosemite National Park to see the grand scale of trees there. "The whole environment is so stunning ... It was so impressive, but I didnt know how you would control it".   However, it would cost too much to shoot on location and he decided to build a forest set on the 007 Stage, named after and used for many James Bond films, at Pinewood Studios. The crew spent 14 weeks constructing the forest set, and Scott was worried that it would not look real enough. It was only days before the start of principal photography that it looked good enough to film. The trees were 60 feet high with trunks 30 feet in diameter and were sculpted out of polystyrene built onto tubular scaffolding frames.  In addition, other sets were constructed on five huge soundstages. 

===Casting===
While Scott was considering Richard OBrien to play Meg Mucklebones, he watched The Rocky Horror Picture Show and saw Tim Curry. He thought the actor would be ideal to play the Lord of Darkness because the actor had film and theatrical experience. Tim Currys make-up as Darkness in Legend is one of the most iconic images in all of fantasy cinema.  Scott discovered Mia Sara in a casting session and was impressed by her "good theatrical instincts". Pirani December 1985, p. 66  Sara was 16 when filming began.

===Makeup effects=== The Howling, The Thing.    Scott told him about Legend, and towards the end of production on The Thing, Bottin read a script for the film and saw an excellent opportunity to create characters in starring roles.

After wrapping his work with Carpenter, Bottin met with Scott to reduce the number of creatures to a manageable quantity (the script suggested thousands). The process would involve complicated prosthetic makeup that would be worn for up to 60 days with some full body prosthetics.  According to Bottin, at the time, Legend had the largest makeup crew ever dedicated to one project. Bottin divided his facility into different shops in order to cover the immense workload. As actors were cast, Bottin and his crew began making life casts and designed characters on drafting paper laid over sketches of the actors faces.  He designed the prosthetics in his Los Angeles studio and spent some time in England occasionally helping with the application of makeup. Pirani December 1985, p. 65 

With the exception of Cruise and Sara, all the principal actors spent hours every morning having extensive makeup applied.  Between 8 and 12 prosthetic pieces were applied individually to each face, then made up, molded and grafted into the actors face so that the prosthetics moved with their muscles.  Each person needed three makeup artists working on them for an average time of three and a half hours spent applying prosthetics. Actor Tim Curry took five and a half hours because his entire body was encased in makeup.  Out of all the characters, the most challenging one in terms of makeup was Darkness. Biodrowski 1986, p. 26 

Curry had to wear a large, bull-like structure atop his head with three-foot fiberglass horns supported by a harness underneath the makeup. Biodrowski 1986, p. 57  The horns placed a strain on the back of the actors neck because they extended forward and not straight up. Bottin and his crew finally came up with horns that were lightweight enough.  At the end of the day, he spent an hour in a bath in order to liquefy the soluble spirit gum. At one point, Curry got too impatient and claustrophobic and pulled the makeup off too quickly, tearing off his own skin in the process. Scott had to shoot around the actor for a week as a result. 

===Principal photography===
Principal photography began on March 26, 1984 on the 007 Stage at Pinewood Studios.  On June 27, 1984, with ten days filming left on this stage, the entire set burned down during a lunch break. Reportedly, flames from the set fire leapt more than 100 feet into the air and the clouds of smoke could be seen five miles away.  Fortunately, it occurred during lunchtime, and no one was hurt.  Scott quickly made changes to the shooting schedule and only lost three days moving to another soundstage.  Meanwhile, the art department rebuilt the section of the forest set that was needed to complete filming.  Four large trees were dropped into an existing forest and Scott shot the snowbound scenes there. 

===Post-production=== Orange County. However, it was felt that the audience had to work too much to be entertained, and another 20 minutes was cut.  The 95-minute version was shown in Great Britain and then the film was cut down even further to 89 minutes for North America.
 Yes lead singer Jon Anderson, and Bryan Ferry.  Similar to later modern noir film adaptations of classic fiction, like Romeo + Juliet in 1996, the soundtrack was deliberately set to add different energy, more depth, increased intensity, and an edgy appeal beyond the traditional fairytale audience of children and parents.

Scott allowed Goldsmiths score to remain on European prints and the composer said, "that this dreamy, bucolic setting is suddenly to be scored by a techno-pop group seems sort of strange to me".    Normally, Goldsmith would spend 6–10 weeks on a film score, but for Legend, he spent six months writing songs and dance sequences ahead of time. 

===Rediscovery===
In 2000, Universal unearthed an answer print of the 113-minute preview cut with Jerry Goldsmiths score. This print had minor visual anomalies that were eventually digitally replaced with finished shots from the 89-minute U.S. version. This edition is Scotts preferred 2002 "Directors Cut", with the restored Jerry Goldsmith soundtracks. The Directors Cuts source is one of only two prints of this extended version known to exist, used for Universals 2002 DVD (and eventual Blu-ray) "Ultimate Edition."

==Reception==
Legend received mixed reviews. Steve Biodrowski of Cinefantastique praised the film, highlighting the makeup design by Rob Bottin and Tim Currys performance as Darkness, saying that " ecause of the visuals (and Currys performance, which is mostly limited to the last 20 minutes), the film is worth seeing." Widgett Walls of needcoffee.com also praised the film, once again highlighting on Bottins makeup, focusing on the character of Darkness, saying simply that "Tim Currys Darkness is absolutely incredible."

Other reviews for the film were lukewarm, suggesting that, although not a bad movie, most critics felt that the film was simply a rehashing of several other fantasy, science fiction, and sword and sorcery films. One such review came from Vincent Canby of The New York Times, who said that " ts a slap-dash amalgam of Old Testament, King Arthur, "The Lord of the Rings" and any number of comic books."

Roger Ebert of the Chicago Sun-Times wrote a mixed negative review where he praised Bottins makeup and Assheton Gortons set design and the performances of Curry and Tom Cruise, but noted dryly that the effects were so good that the roles could have been played by almost anyone.  Ebert also said that the movie was composed of all of the right ingredients to be successful, but that the film simply "doesnt work". He went on to say that " ll of the special effects in the world, and all of the great makeup, and all of the great Muppet creatures cant save a movie that has no clear idea of its own mission and no joy in its own accomplishment." James OEhley of Sci-Fi Movie Page lampooned the film, saying that Hollywood fantasy movies in the 1980s were "evil". 
 cult following Best Makeup; BAFTA Awards for Best Costume Design, Best Makeup Artist, Best Special Visual Effects; DVD Exclusive Awards; and Young Artist Awards,  and since its initial release, has developed an increasing fanbase.  In addition, the release of the Directors Cut in 2002, with the original soundtrack restored, brought renewed attention to the film. Legend currently holds a 50% rating on Rotten Tomatoes based on 22 reviews.

==Music==

===Soundtrack=== The first, The second Yes and Bryan Ferry of Roxy Music. Both soundtracks are available on CD, although the Tangerine Dream soundtrack has become harder to find.

===Songs in the film=== Carpenters songs) and music by Jerry Goldsmith:

* "My True Loves Eyes" (the main theme, sung mostly by Lili. Mia Sara provided some of the singing, while session singers provided vocals wherever Sara was unable to perform).
* "Living River" (the first reprise of "My True Loves Eyes", sung as Lili calls to the unicorn).
* "Bumps and Hollows" (sung by Lili after her forbidden act of touching a unicorn).
* "Sing The Wee" (the theme for the fairies. The first sung version was cut from all editions of the film as it accompanied a scene with Jack and the fairies that was itself cut; the final sung version by the National Philharmonic Chorus is heard over the end credits).
* "Reunited" (the final reprise of "My True Loves Eyes", sung by Lili as she says goodbye to Jack).

The following songs appeared in the 89-minute U.S. re-cut when it was re-scored by Tangerine Dream:

* "Loved by the Sun" (music by Tangerine Dream, lyrics written and sung by Jon Anderson).
* "Is Your Love Strong Enough" (written and performed by Bryan Ferry over the U.S. prints end credits).

A promotional music video (presumably for the U.S. market, where the Tangerine Dream soundtrack was used) was created for the Bryan Ferry song "Is Your Love Strong Enough."  The video, which incorporates Ferry and guitarist David Gilmour into footage from the film, is included as a bonus on disc 2 of the 2002 "Ultimate Edition" DVD release.

==Home media==
In 2002, Universal released the aforementioned 113-minute "directors cut" on Region 1 DVD restoring previously cut scenes, and the original Goldsmith score. In creating the directors cut edition, producer Charles de Lauzirika turned to Legend fan and unofficial historian Sean Murphy, who runs the Legend FAQ, and Terry Rawlings, the editor of Legend, for help in finding the footage for the Ultimate DVD  creation. 

Universal has released a Blu-ray Disc|Blu-ray version of the "Ultimate Edition" as of May 31, 2011. With the exception of the 2002 DVD-ROM features, this disc carries over all the content from the DVD, including the Jerry Goldsmith-scored "Directors Cut" and the Tangerine Dream-scored theatrical version.

20th Century Fox, the international rights holder, has released a Blu-ray issue for Region 2 of both the 94-minute European version and the 113-minute directors cut (both with Jerry Goldsmiths music).

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 