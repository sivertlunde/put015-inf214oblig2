Amrithavaahini
{{Infobox film
| name           = Amrithavaahini
| image          =
| caption        =
| director       = J. Sasikumar
| producer       =
| writer         =
| screenplay     = Sharada Thikkurisi Sukumaran Nair Adoor Bhasi
| music          = A. T. Ummer
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film,  directed J. Sasikumar. The film stars  and Prem Nazir, Sharada (actress)|Sharada, Thikkurisi Sukumaran Nair and Adoor Bhasiin lead roles. The film had musical score by A. T. Ummer.   

==Cast==
*Prem Nazir
*Thikkurisi Sukumaran Nair
*Adoor Bhasi Sharada
*Jayan

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Sreekumaran Thampi, Adoor Bhasi and Bharanikkavu Sivakumar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Abhayadeepame || S Janaki || Sreekumaran Thampi ||
|-
| 2 || Angaadi Marunnukal || Adoor Bhasi, Sreelatha Namboothiri || Adoor Bhasi ||
|-
| 3 || Chembarathikkaadu || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Iruttil Koluthivacha || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 5 || Kodunkatte Nee || S Janaki || Sreekumaran Thampi ||
|-
| 6 || Marubhoomiyil Vanna Maadhavame || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 7 || Vrindaavanam Swargamaakkiya || Ambili || Bharanikkavu Sivakumar ||
|}

==References==
 

==External links==
*  

 
 
 


 