Confessions (1925 film)
{{Infobox film
| name           = Not for Sale
| image          =
| caption        =
| director       = W.P. Kellino 
| producer       = 
| writer         = Baillie Reynolds (novel)   Lydia Hayward Ian Hunter   Joan Lockton  Eric Bransby Williams   Gladys Hamer
| music          = 
| cinematography = Jack E. Cox
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = June 1925
| runtime        = 6,200 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    = silent comedy Ian Hunter, Joan Lockton and Eric Bransby Williams. It was based on the novel Confession Corner by Baillie Reynolds.

==Cast== Ian Hunter as Charles Oddy
* Joan Lockton as Phoebe Vollings
* Eric Bransby Williams as Percy Denham
* Gladys Hamer as Ada Best
* Fred Raynham as E.H. Slack
* W.G. Saunders as James Barnes
* Lewis Shaw as Henry
* Moore Marriott as Hardy
* Lewis Gilbert as Mr. Vallings
* Dodo Watts as Child

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

 
 
 
 
 
 
 
 
 
 


 
 