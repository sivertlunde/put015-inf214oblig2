Ruby & Quentin
{{Infobox film
| name           = Ruby & Quentin
| image          = Tais-toi_poster.jpg
| image size     = 
| alt            = 
| caption        = Film Poster
| director       = Francis Veber
| producer       = Saïd Ben Saïd Gérard Gaultier (executive)
| writer         = Francis Veber (scenario, adaptation & dialogue)  Serge Frydman (idea)
| narrator       =  Richard Berry  et André Dussollier   Leonor Varela  Aurélien Recoing   Jean-Pierre Malo  Laurent Gamelon   Jean-Michel Noirey  Armelle Deutsche  Edgar Givry Avec la participation de Ticky Holgado et Michel Aumont
| music          = Marco Prince
| cinematography = Luciano Tovoli
| editing        = Georges Klotz 
| studio         = UGC Images DD Productions  EFVE Films  TF1 Films Production  FILMAURO
| distributor    = UFD (UGC Fox Distribution)
| released       =   
| runtime        = 87 minutes
| country        = France-Italy
| language       = French
| budget         = 27,44 million €
| gross          = 26,68 million €
| preceded by    = 
| followed by    = 
}}
 2003 Cinema French comedy crime caper film, directed by Francis Veber. This film became widespread in China after having been dubbed in Northeastern Mandarin.

==Cast==
* Gérard Depardieu as Quentin
* Jean Reno as Ruby Richard Berry as Vernet
* André Dussollier as Le Psychiatre prison
* Jean-Pierre Malo as Vogel
* Jean-Michel Noirey as Lambert
* Laurent Gamelon as Mauricet
* Aurélien Recoing as Rocco
* Vincent Moscato as Raffi
* Ticky Holgado as Martineau
* Michel Aumont as Nosberg
* Leonor Varela|Léonor Varela as Katia/Sandra
* Loïc Brabant as Jambier
* Arnaud Cassand as Bourgoin
* Edgar Givry as Vavinet
* Guillaume de Tonquédec as The hospitals intern
* Adrien Saint-Joré as Adolescent 1
* Johan Libéreau as Adolescent 2
* Guy Delamarch as Lefevre
* Rebecca Potok as Mme Lefevre

* Stéphane Boucher as Le gardien chef
* Ludovic Berthillot as Le gros Taulard
* Thierry Ashanti as Le Taulard noir

==Plot summary== Ice Age is showing. He is caught when instead of continuing to run, he sits down and enjoys the film.

Ruby, a henchman of the crime lord Vogel, who has been having an affair with his boss wife, is thrown into a jail after getting caught hiding the loot that he stole from his boss in revenge for the death of his lover. In jail Ruby refuses to eat or speak, which concerns the jails psychiatrist but fails to impress the detective in charge of the investigation, who sees through Rubys ruse.

At the suggestion of the detective, the two criminals meet after Quentin, an intolerable extrovert who has driven mad two other inmates, is put in with him. Quentin thinks that he has struck up a one-sided friendship with Ruby after conducting an uninteresting monologue, however they are parted again after Ruby fakes suicide to get out. Quentin does the same and ends up in the bed next to Ruby in the infirmary.

Ruby manages to bribe the psychiatrist (who also works for Vogel) into helping him escape the prison. Quentin, however, thwarts the escape and makes his own botched attempt at escape with help from a drunken long-time friend driving a crane. Over the period of the film Vogels bodyguards tried to catch Ruby 3 times.After that they stole 2 cars of Vogel and his bodyguards returned them back. Once outside Ruby is unable to get rid of Quentin, and they both go through many adventures, including robbing a diminutive horse trainers house, dressing up in Chanel, exchanging clothing with two impertinent youths, stealing a series police and civilian cars and fixing Rubys shoulder with a breaking chair, in pursuit of his former partners in crime. And when Quentin tried to steal a car, 2 policemen noticed  them and Ruby was serious wounded.

Sheltering in an abandoned shop, where Quentin plans to make a café business, they meet another homeless woman, who looks like Vogels former wife. Ruby gives her some money and drops her off at a hotel to stay.

After an exciting car chase where the antagonists are trapped in a car jam, they enter the estate of Vogel and in a shoot-out results in an incapacitated Vogel, and Quentin supposedly mortally wounded. Ruby, much to his own surprise as anyone elses, expresses his grief for Quentin, who tricks him into actually opening a café after he recovers.

==External links==
* 

 

 
 
 
 
 