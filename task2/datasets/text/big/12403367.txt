West of the Divide
{{Infobox film
| name           = West of the Divide
| image          = West of the Divide.jpg
| caption        = Film poster
| director       = Robert N. Bradbury
| producer       = Paul Malvern
| writer         = {{plain list|
*Robert N. Bradbury
*Oliver Drake
}}
| starring       = {{plain list|
*John Wayne
*Virginia Brown Faire
}}
| music          =
| cinematography = Archie Stout
| editing        = Carl Pierson
| distributor    = Monogram Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}}
 Western film starring John Wayne.

==Cast==
* John Wayne as Ted Hayden, posing as Gat Ganns
* Virginia Brown Faire as Fay Winters
* George "Gabby" Hayes as "Dusty" Rhodes
* Lloyd Whitlock as Mr. Gentry
* Yakima Canutt as Hank (Gentry Henchman)
* Lafe McKee as Mr. Winters Billy OBrien as Spuds (later Jim Hayden)
* Dick Dickinson as Henchman Joe
* Earl Dwire as Sheriff

==See also==
* John Wayne filmography

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 
 