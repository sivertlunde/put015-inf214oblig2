Prem Mhanje Prem Mhanje Prem Asta
{{Infobox film
| name           = Prem Mhanje Prem Mhanje Prem Asta
| image          =  
| image_size     =
| caption        = 
| director       = Mrinal Dev-Kulkarni
| producer       = Sachin Parekar
| story          = Mrinal Dev-Kulkarni
| narrator       = 
| starring       = Mrinal Dev-Kulkarni Sachin Khedekar Pallavi Joshi Sunil Barve Suhas Joshi Mohan Agashe
| music          = Milind Ingle and Surel Ingle
| cinematography = Amlendu Chaudhary
| screenplay     = Manisha Korde
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
}}

Prem Mhanje Prem Mhanje Prem Asta ( . The film stars Mrinal Dev-Kulkarni, Sachin Khedekar, Pallavi Joshi, Sunil Barve, Suhas Joshi, Mohan Agashe and Smita Talwalkar. The films music is by Milind Ingle and Surel Ingle. 

The films is based on a very important question which is faced by almost everyone, connection between love and marriage. 

==Plot==
The movie is a heart-warming story of two different individuals who at one point in their lives were married. A single mother along with her two daughters live with her mother-in-law. Her husband had abandoned them 4 years ago, but staying in the same city had never bothered to check on his family. The only thing he did in those 4 years was to send divorce papers, which his wife has not signed.

Other side of the story revolves around a doctor who is a father to two kids. His ex-wife had to choose between staying home with family or career in USA and she chose career. But she never let the divorce hamper the relation she shares with her ex-husband. But this incident had definitely made her ex-husband depressed and alone.

One eventful day at their kids school gets them together and a conversation begins, which blooms into something amazing. Until there is a twist in the tale.

==Cast==
* Mrinal Dev-Kulkarni
* Sachin Khedekar
* Pallavi Joshi
* Sunil Barve
* Suhas Joshi
* Mohan Agashe
* Smita Talwalkar

==Crew==
*Director - Mrinal Dev-Kulkarni
*Story - Mrinal Dev-Kulkarni
*Producer - Sachin Parekar
*Cinematographer - Amlendu Chaudhary
*Art Director - Vinod Gunaji and Nitin Borkar
*Music Director - Milind Ingle and Surel Ingle
*Lyricist - Kishore Kadam

==Soundtrack==
The music has been directed by Milind Ingle and Surel Ingle, while the lyrics have been provided by Kishore Kadam. 

===Track listing===
{{Track listing
| title1 = Prem Mhanaje Prem | length1 = 5:14
| title2 = Man Bavarate  | length2 = 4:32
| title3 = Ya Rastyavar  | length3 = 5:24
}}

== References ==
 

 

 
 
 