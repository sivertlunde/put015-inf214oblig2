Ubojite misli
{{Infobox film
| name           = Ubojite misli
| image          = 
| alt            = 
| caption        = 
| director       = Stanka Gjurić
| producer       =Stanka Gjurić
| writer         = Stanka Gjurić
| starring       = female dog Hooper
| music          = 
| cinematography = Stanka Gjurić
| editing        = Stanka Gjurić
| studio         = Poeta Film
| distributor    = 
| released       =  
| runtime        = 3 minutes
| country        = Croatia
| language       = Croatian
| budget         = 
| gross          = 
}} 2006 Croatian short film directed by Stanka Gjurić. The film was shot on location in Zagreb.

==Summary== War started 1991 in former SFR Yugoslavia|Yugoslavia, where unexploded Land mines in Croatia are still pose a daily threat to life, seen through reaction of a dog. Author show us through the terrorized look of her dog Hooper, how the sun of shell burst has indelibly marked the dog’s memory. That nothing may be forgotten.

==Reception==
Exceptional reception by the audience in Festival of the First in Zagreb (a competition for artists in which they should represent the works of art, that are not within the domain of their creativity) result that people came every day to watch the film in a gallery in which, among other works, it was exposed all day.    This event instigate the author to send film to film festivals.
That is how the film Ubojite misli started its journey: through participating in Short Film Corner in Cannes Film Festival, through Toronto (Canada), Film Festival in Mostar (Bosnia and Herzegovina), Italy,    Greece, Portugal, Switzerland, again France,    Spain...   

A member of the International jury at the 5. Mostar Film Festival (2007) when the film won 1st Award, Valentina Mindoljević commented Award: The decision was unanimous, and all the members of the jury agreed that the film of Stanka Gjurić significantly stands out with its originality. Everything else has already been seen.
At the AluCine Film Festival in Toronto, to elucidate the award, Andres La Rota (one of the judges) said that the film is poetic, deeply emotional and upsetting.

==References==
 

==External links==
*  

 
 
 
 
 
 