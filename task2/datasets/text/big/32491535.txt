Sick Cylinders
{{Infobox Hollywood cartoon|
| cartoon_name = Sick Cylinders
| series = Oswald the Lucky Rabbit
| image =
| caption =
| director = Hugh Harman Ben Clopton
| story_artist =
| animator =
| voice_actor =
| musician = Bert Fiske
| producer = George Winkler
| distributor = Universal Pictures
| release_date = February 18, 1929
| color_process = Black and white
| runtime = 5:43 English
| preceded_by = Hen Fruit
| followed_by = Hold Em Ozzie
}}

Sick Cylinders is an animated short film by Winkler Productions which stars Oswald the Lucky Rabbit. It is among the few surviving Oswald films from the Winkler era.

==Plot==
Oswald is driving his uncovered car through the countryside. One day, he stops by a two-story house to pick up his date, a girl cat. When both are on board, they attempt to hit the road. To their surprise, the car breaks down. Oswald steps out to fix the vehicle while the girl cat impatiently waits.

While Oswald is fixing it, a terrier comes by, wanting to play fetch. Every time the rabbit hurls a stick, the terrier comes back with a bigger one. This goes on until the terrier returns with a huge log, dropping it on the cars front as well as flattening Oswald underneath. Tired of playing games, Oswald kicks away the little mutt and resumes working on the car.

In no time, the car is repaired and ready to go. Oswald and the girl cat finally set off. However, their journey is far from a smooth one because they have to dodge large rocks on the way which they do successfully. But more trouble comes as the two riders get a boulder rolling toward them from behind. Finding no way to escape, they run off a cliff where they plunge into a pond, splashing out all the water.

In the dry pit which is what is left of the pond, the car is capsized. Oswald and his date are lying on the ground. The girl cat then stands up and expresses her disgust. Momentarily, a boy dog in a luxuriant car stops by. The boy dog invites the girl cat, who is most flattered, to come along. She then takes a seat and the luxuriant car then departs. Oswald is left behind, frustrated, and battering his own vehicle.

==References in later works==
There are sequences which were later remade very closely in such Harman and Ising Warner Bros. efforts as Sinkin in the Bathtub (1930)  and Boskos Holiday (1931). 

==See also==
*Oswald the Lucky Rabbit filmography

==References==
 

==External links==
*  at the Big Cartoon Database
* 

 

 
 
 
 
 
 
 
 


 