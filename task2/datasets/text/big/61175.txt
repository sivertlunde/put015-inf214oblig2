The Thin Man (film)
 
{{Infobox film 
| name            = The Thin Man 
| image           = The Thin Man 1934 Poster.jpg
| image_size      = 225px
| caption         = Theatrical release poster
| director        = W. S. Van Dyke 
| producer        = Hunt Stromberg 
| screenplay      = {{Plainlist|
* Albert Hackett
* Frances Goodrich
}}
| based on        =   
| starring        = {{Plainlist|
* William Powell
* Myrna Loy
}} 
| music           = William Axt
| cinematography  = James Wong Howe
| editing         = Robert Kern
| studio          = Metro-Goldwyn-Mayer
| distributor     = Loews Cineplex Entertainment|Loews Inc. 
| released        =  
| runtime         = 93 minutes
| country         = United States
| language        = English
| budget          = $226,408
| gross           = $1,423,000 (worldwide est.)
}}
 novel of Asta is played by canine actor Skippy (dog)|Skippy.

The films screenplay was written by Albert Hackett and Frances Goodrich, a married couple. In 1934, the film was nominated for the Academy Award for Best Picture.

The titular "Thin Man" is not Nick Charles, but the man Charles is initially hired to find - Clyde Wynant (part way through the film, Charles describes Wynant as a "thin man with white hair"). The "Thin Man" moniker was thought by many viewers to refer to Nick Charles and, after a time, it was used in the titles of sequels as if referring to Charles.

==Plot==
Nick Charles William Powell|(Powell), a retired detective, and his wife Nora Myrna Loy|(Loy) are attempting to settle down. They are based in Los Angeles but decide to spend the Christmas holidays in New York. There he is pressed back into service by a young woman whose father, an old friend of Nicks, has disappeared after a murder. The friend, Clyde Wynant Edward Ellis (actor)|(Ellis) (the eponymous "thin man"), has mysteriously vanished. When his former secretary and love interest, Julia Wolf, is found dead, evidence points to Wynant as the prime suspect, but his daughter Dorothy Maureen OSullivan|(OSullivan) refuses to believe that her father is guilty. She convinces Nick to take the case, much to the amusement of his socialite wife. The detective begins to uncover clues and eventually solves the mystery of the disappearance through a series of investigative steps.

The murderer is finally revealed in a classic dinner-party scene that features all of the suspects. A skeletonized body, found during the investigation, had been assumed to be that of a "fat man" because it is wearing oversize clothing. The clothes are revealed to be planted, and the identity of the body is accurately determined by an old war wound to the leg. It turns out that the body belongs to a "thin man" — the missing Wynant. The double murder has been disguised in such a way as to make it seem that Wynant is the killer and still alive. The real killer was the attorney, who stole a lot of money from Wynant and killed him on the night he was last seen.

==Cast==
  
* William Powell as Nick Charles
* Myrna Loy as Nora Charles Skippy as Asta, their dog
* Maureen OSullivan as Dorothy Wynant
* Nat Pendleton as Lt. John Guild
* Minna Gombell as Mimi Wynant Jorgenson
* Porter Hall as Herbert MacCaulay
 
* Henry Wadsworth as Tommy William Henry as Gilbert Wynant
* Harold Huber as Arthur Nunheim
* Cesar Romero as Chris Jorgenson
* Natalie Moorhead as Julia Wolf Edward Ellis as Clyde Wynant
* Edward Brophy as Joe Morelli 
 

Cast notes: Pat Flaherty, Edward Hearn, Walter Long, Fred Malatesta, Lee Phelps, Bert Roach, Rolfe Sedan, Gertrude Short, Lee Shumway, Ben Taggart, Harry Tenbrook and Leo White. 

==Production==
The entire film was shot in twelve (out of fourteen) days. The film was released on May 25, 1934, only four months after the release of the book, which had been released in January 1934.

At the time of production, MGM, had anywhere from 50 to 90 other projects going and didnt give any real attention to the movie. Van Dyke was finally able to convince MGM executives to let Powell portray Nick Charles. Their biggest fear was that audiences would see Powell as the "Super Slueth" that he portrayed earlier in Paramounts films.

==Reception==
The film was such a success that it spawned five sequels:
*After the Thin Man (1936)
*Another Thin Man (1939)
*Shadow of the Thin Man (1941)
*The Thin Man Goes Home (1945)
*Song of the Thin Man (1947)

In 2002, critic Roger Ebert added the film to his list of  Great Movies.    Ebert praises William Powells performance in particular, stating that Powell "is to dialogue as Fred Astaire is to dance. His delivery is so droll and insinuating, so knowing and innocent at the same time, that it hardly matters what hes saying." 
 designated the film as one of the great comedies in the previous hundred years of cinema.

==Trailer==
The trailer contained specially filmed footage in which Nick Charles (William Powell) is seen on the cover of the Dashiell Hammett novel The Thin Man.  Nick Charles then steps out of the cover to talk to fellow detective Philo Vance (also played by Powell) about his latest case.  
 The Kennel Murder Case, a film in which Powell played Vance.  The Kennel Murder Case was released in October 1933, just seven months prior to the release of The Thin Man.

Charles goes on to explain to Vance that his latest case revolves around a "tall, thin man" (referring to Clyde Wynant), just before clips of the film are shown.

==Adaptations==
The Thin Man was dramatized as a radio play on the June 8, 1936 broadcast of Lux Radio Theater, with William Powell and Myrna Loy reprising their film roles. 

==Influence== Nick Charles. Echoing the name "Nick Charles", the 2013 Australian crime comedy series Mr & Mrs Murder features married couple "Nicola" and "Charlie" Buchanan, who run an industrial cleaning business specialising in crime scenes and, using this experience, they become amateur sleuths.

== References ==
 

==External links==
 
 
*  
*  
*  
*  

===Streaming audio===
*   on Lux Radio Theater: June 8, 1936 

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 