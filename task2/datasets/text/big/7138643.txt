Star Kid
{{Infobox Film
| name           = Star Kid
| image          = Star Kid.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Manny Coto
| producer       = Jennie Lew Tugend
| writer         = Manny Coto
| starring       = Joseph Mazzello Richard Gilliland Corinne Bohrer
| music          = Nicholas Pike Ronn Schmidt
| editing        = Bob Ducsay
| distributor    = Trimark Pictures Inc.
| released       = January 16, 1998
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = $12 million 
| gross          = $7,029,025 
}} independent Science fiction film|sci-fi/family film directed and written by Manny Coto. The film stars Joseph Mazzello, Richard Gilliland, and Corinne Bohrer.

==Plot== bully Turbo (Joey Simmrin), rescuing Michelle and her friends from a damaged ferris wheel, and ordering food from a fast-food restaurant drive-thru, along with a few hilarious antics such as trashing his house while getting his head stuck in a refrigerator, figuring out how to eat a hamburger through the suit and wanting to get out of the suit to pee when Cy wouldnt let him. 
 alien race of insectoids waging a war against the creator of the Cybersuit, Tenris DeThar and his fellow Trelkins. The Brood Warriors mission is to capture the Cybersuit so that his race can analyze it. After his first encounter with the Brood Warrior, Spencer escapes, forces Cy to eject him out of the suit and then abandons Cy telling him that hes afraid that he might not live to see his next birthday if he "engages" the Brood Warrior. Back at home, after Spencer looks over his comic book titled "The Midnight Warrior" and thinking about what kind of person he wants to be, he goes back out to find Cy only to find out that Cy was captured by the Brood Warrior. Spencer begins searching for Cy accompanied by Turbo, now becoming his friend. As they head to the junkyard, where Cy is about to be taken off-world by the Brood Warrior, they create a plan to distract the Brood Warrior long enough for Spencer to rescue Cy. Spencer gets Cy back and begins battling with the Brood Warrior.

During the battle, the Brood Warrior gets the upper hand and defeats Cy and Spencer. After getting bashed multiple times by the Brood Warriors mace and severely damaging the suit, Cy is forced to eject Spencer out before going completely offline. Spencer covers the suit with scrap metal to hide it from the Brood Warrior, takes a piece of the suit and continues to fight the Brood Warrior, who was later trying to chase down Turbo. Spencer confronts the Brood Warrior before getting chased himself and is suddenly cornered in a junked RV. Just when the Brood Warrior is about to dispose of Spencer, Turbo finds a control panel and activates the car crusher the RV is sitting in, revealing the whole thing to be a trap. Spencer escapes while the Brood Warrior is compressed along with the RV into a solid metal cube, killing the Brood Warrior.

With the Brood Warrior now destroyed, they return to Cy but it appears they were too late to save him. Just when Spencer begins to lose hope, Cys creator Tenris DeThar and Trelkin soldiers appear from a giant UFO and quickly repair him, bringing him back to life. After Cy and Spencer say goodbye to one another, one of the aliens gives Spencer a badge for his bravery and courage before their departure back to their home-world. The next day at school, a now confident Spencer, with encouragement from his new friend Turbo, starts up a conversation with Michelle.

==Cast==
* Joseph Mazzello as Spencer Griffith
* Richard Gilliland as Roland Griffith
* Corinne Bohrer as Janet Holloway
* Alex Daniels as "Cy"
** Arthur Burghardt as "Cy" (voice)
* Joey Simmrin as Manfred "Turbo" Bruntley
* Brian Simpson as the Broodwarrior
* Ashlee Levitch as Stacey Griffith Jack McGee as Hank Bruntley
* Danny Masterson as Kevin
* Lauren Eckstrom as Michelle Eberhardt
* Bobby Porter - Nath the Treklin
* Larry Nicholas - Tenris De-Thar the Trelkin
* Rusty Hanson - Trelkin
* Terry Castillo - Trelkin

==Release==
The film grossed a domestic total of $7,029,025, making the film a box office bomb from its estimated $12 million budget.

==Home media==
In the USA, Star Kid was released on VHS in 1997 and DVD format in 1998. It was also released on VHS in the UK and is now available on DVD. 

==Soundtrack==
All tracks (with the exception of the first two tracks) were composed by Nicholas Pike. The soundtrack was released on compact disc by Sonic Images (January 27, 1998) and further released for download through BSX Records (January 29, 2013) with modified cover art.
{{Tracklist
| extra_column= Notes
| collapsed= no
| headline= Track Listing Magic Carpet Ride  Steppenwolf cover by Edgar Winter
| length1 = 4:22
| title2  = Shadow in the Shade
| extra2  = Performed by Theresa Musser
| length2 = 4:21
| title3  = Battle on Trelkas
| length3 = 5:07
| title4  = Another Fun Day at School
| length4 = 1:04
| title5  = Turbo Trouble
| length5 = 4:19
| title6  = The Cybersuit Arrives
| length6 = 6:20
| title7  = Turbo Takes a Spin
| length7 = 3:07
| title8  = In the Fairground
| length8 = 4:41
| title9  = Mom
| length9 = 2:03
| title10 = Rearranging the Kitchen
| length10= 1:58
| title11 = Broodwarrior Arrives
| length11= 1:43
| title12 = On the Bridge
| length12= 4:40
| title13 = Anyone for Tennis
| length13= 1:31
| title14 = Home Improvement
| length14= 6:31
| title15 = Joyride in the Junkyard
| length15= 3:35
| title16 = Cy Runs Out of Steam
| length16= 6:49
| title17 = Trelkins Arrive
| length17= 2:46
| title18 = Farewell to the Trelkins
| length18= 2:35
| title19 = Finale
| length19= 2:41
}}

==Comic== John Stokes it was published by Dark Horse Comics. 

==See also==
* List of films featuring powered exoskeletons

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 