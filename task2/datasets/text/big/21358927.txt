Cheerleader Queens
{{Infobox film
| name           = Cheerleader Queens
| image          = Cheerleader-queens.jpg
| caption        = 
| director       = Poj Arnon
| producer       = Chareon Iamphongporn
| writer         = Haokrati Film
| starring       = Montonn Pariwat Wongeap Kunarattanawat Sumet Saelee Sittichai Leonseri Prathanporn Puwadolpitak
| music          = Sunit Asawanitkul
| cinematography = Panya Nimchareonpong
| editing        = Sunit Asawanitkul
| studio         = Five Star Production
| distributor    = 
| released       = 21 February 2003
| runtime        = 119 min.
| country        = Thailand
| language       = Thai
| budget         = 
| gross          = 
}} Thai film directed by Poj Arnon.

== Plot == rugby team, tries in an important game.

Toey, another kathoey from the boys hometown, comes to Bangkok with the idea of forming a new cheerleading team, which they name "The Queen". From the start the boys have to face a number of problems, but with the help of their friends on the rugby team they manage to get to the finals of the national cheerleading competition.

== Reception ==
Ronnie Scheib of Variety (magazine)|Variety reviewed Cheerleader Queens at the 2004 New York Lesbian, Gay, Bisexual, & Transgender Film Festival. He described the film as, "A fun date flick for all persuasions, with sexual content limited to a few chaste kisses and endless eye-batting innuendo, kitschy curio should perform cheerily on the gay fest circuit." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 
 