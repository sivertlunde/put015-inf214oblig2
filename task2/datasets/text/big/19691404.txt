Mariyadhai
{{Infobox film
| name = Mariyadhai
| image =
| alt =  
| caption =
| director = Vikraman
| producer = T. Siva
| writer = Vikraman Ambika Ramesh Khanna
| music = Vijay Antony
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}} Indian Tamil Tamil film Ambika also plays a significant role.  Directed by Vikraman, who earlier directed hits like Poove Unakkaga (1996) and Vanathai Pola (2000), the film released in 2009. 

==Plot==
 
The story unfolds in a small village near Pollachi. Annamalai (senior Vijayakanth) leads a contended life with his wife Alamelu (Ambika), his daughter Sumathy (Ammu) and his son Pichai (junior Vijayakanth). A caring a loving father, he even allows his son to change his name from Pichai to Raja, when he is made fun of by his friends when he was very young.

A respectable man in the village for his philanthropic activities, Annamalai gives enough liberties and freedom to his son Raja, who emerges out as an agriculture graduate. All troubles begin when Raja loses money by cultivating dates in the barren land owned by his father. Meanwhile an industrialist eyes the land for setting up chemical factory which is resisted by both father and son. But Raja comes across Radha (Meena), a music teacher and he falls for her immediately. Raja’s wedding is arranged with Radha by elders of both the families.

Two days before their wedding, Raja gives up his property to Radha to bail her out of trouble little realizing that it was a practical joke hatched by Radha to take away his land. Their wedding gets stopped. Raja vows to get back the land. Enters Chandra (Meera Jasmine) in Raja’s life. She comes to know of his bad past and vows to change his heart and marry him. Eventually it’s how Raja comes up in his life and marry Chandra.

==Cast==
* Vijayakanth filmography|Vijaykanth as Annamalai and Pichandi (Raja)
* Meena Durairaj|Meena as Radha 
* Meera Jasmine as Chandra Pichandi Ambika as Alamelu Annamalai
* Sampath Raj as Sampath
* Ramesh Khanna

==References==
 

 
 

 
 
 
 


 