Gambling House
{{Infobox film
| name           = Gambling House
| image          = Gambling house poster small.jpg
| image size     = 200px
| alt            =
| caption        = Theatrical release poster
| director       = Ted Tetzlaff
| producer       = Warren B. Duff
| screenplay     = Marvin Borowsky Allen Rivkin
| story          = Erwin S. Gelsey
| narrator       = Terry Moore William Bendix Cleo Moore
| music          = Roy Webb
| cinematography = Harry J. Wild
| editing        = Roland Gross
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English}}
 Terry Moore, William Bendix and Cleo Moore. 

==Plot==
A crooked gambler decides to turn on his murderous boss.

==Cast==
* Victor Mature as Marc Fury Terry Moore as Lynn Warren
* William Bendix as Joe Farrow
* Cleo Moore as Sally
* Basil Ruysdael as Judge Ravinek
* Gloria Winters as B.J. Warren
* Donald Randolph as Lloyd Crane
* Ann Doran as Mrs. Della Lucas
* Eleanor Audley as Mrs. Livingston
* Don Haggerty as Sharky

==Reception==
===Critical response===
When first released, critic Bosley Crowther panned the film. He wrote, "Dont look for very rich pickings in R. K. O.s Gambling House, a run-of-the-mill melodrama that came to the Mayfair on Saturday. Your chances for solid satisfaction from this tale of a crook who goes straight after meeting a decent young lady are about as good as they would be from a fixed wheel ... Put it down as claptrap and the performance of Mr. Mature as another demonstration of an actor doing the best he can with a bad role. Miss Moore is entirely incidental and William Bendix is mulishly mean as the tough and deceitful rascal who crosses up Mr. Mature. To say any more about it might tend to incriminate somebody." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 