Drop Dead Fred
{{Infobox film
| name           = Drop Dead Fred
| image          = Drop dead fred ver1.jpg
| alt            = 
| caption        = Theatrical release poster Ate De Jong Paul Webster
| screenplay     = Carlos Davis Anthony Fingleton
| story          = Elizabeth Livingston
| starring       = {{Plain list | 
* Rik Mayall
* Phoebe Cates
* Marsha Mason
* Tim Matheson
* Carrie Fisher
* Daniel Gerroll
* Ron Eldard
}}
| music          = Randy Edelman
| cinematography = Peter Deming
| editing        = Marshall Harve
| studio         = PolyGram Filmed Entertainment Working Title Films
| distributor    = New Line Cinema   Universal Pictures  
| released       =  
| runtime        = 99 minutes  
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          = $13.8 million (domestic)   
}} dark Fantasy fantasy comedy comedy film Ate De visual and make-up effects, Gross out|gross-out humor and some profanity.
 arch nemesis of her overbearing mother, Polly (Marsha Mason). He causes chaos around the home and neighborhood, but nobody can see him except her. When she grows up and has an emotional crisis, he returns to "cheer her up" in his own unique way, causing more chaos than ever before. The supporting cast includes Carrie Fisher, Ron Eldard, Tim Matheson, and Bridget Fonda.

==Plot==
Lizzie Cronin is an unassertive and repressed woman, domineered by her controlling mother, Polly. While taking her lunch break from work, she calls her husband, Charles, whom she is separated from, hoping to sort out their problems. He reasserts his desire for a divorce and says that he is in love with another woman named Annabella. While she is at the public phone, first her purse is stolen, then her car. Forced to walk back to work, she arrives late and loses her job. Polly then appears, again takes control, and brings her back to the house she grew up in.

While rummaging through past belongings in her childhood bedroom closet, Lizzie finds a taped-shut jack-in-the-box. After removing the tape and turning the crank, she frees her former imaginary friend, Drop Dead Fred (who was apparently her "jack"). Through a series of flashbacks, it is revealed that while he caused havoc for her, he also gave her happiness and a release from her oppressive mother. He agrees to help her become happy again, which she believes will only happen when she wins back Charles. However, his childish antics do more harm than good.

Worried by Lizzies recent strange behavior, Polly brings her to a psychologist. In the waiting room, Fred is seen meeting up with other patients imaginary friends. The doctor prescribes medication to rid her of him, whom he and Polly believe is a figment of her imagination. She also changes her appearance and wardrobe. Charles now wants her back and she is overjoyed, until Fred discovers he is still cheating on her with Annabella. Heartbroken, she tells Fred that she cant leave Charles, because she is scared of being alone. They escape to a dream sequence in which she is finally able to reject him, stand up to Polly, and declare she is no longer afraid of her. Fred tells her that she doesnt need him anymore, so they kiss and he disappears into her eternal subconscious.

Upon waking from the dream, Lizzie leaves Charles and asserts herself to Polly, who blames her for her father leaving home. Before leaving, she reconciles with Polly, and encourages her to find a friend to escape her own loneliness. She goes to her friend Mickeys house, and on meeting, they both express interest in becoming more than just friends. After his daughter, Natalie, comes up to them and blames Fred for mischief that has just prompted her nanny to quit, Lizzie realizes that he is now with Natalie. She can no longer see him, but he is now leading another. She just smiles.

==Cast==
* Rik Mayall as "Drop Dead Fred"
* Phoebe Cates as Lizzie Cronin
** Ashley Peldon as Young Lizzie
* Marsha Mason as Polly Cronin
* Ron Eldard as Mickey Bunce
* Carrie Fisher as Janie
* Tim Matheson as Charles
* Daniel Gerroll as Nigel Cronin Keith Charles as Murray
* Bridget Fonda as Annabella
* Eleanor Mondale as Attractive customer
* Bob Reid as Judge Dubben

==Reception==

===Box office===
 
the film was released theatrically in North America on April 19, 1991, and fared well (for an independent film), grossing $3,625,648 on its opening weekend, and $13,878,334 over its entire theatrical run.  It was Working Titles first financial hit and was (for a time) the most successful independent film released in Australia.

===Critical response===
The film received negative reviews; Leonard Maltin stated that "Phoebe Cates appealing performance cant salvage this putrid mess...recommended only for people who think nose-picking is funny."  Rotten Tomatoes  gave it a score of 9% based on 34 reviews.  Gene Siskel named Drop Dead Fred as the worst film of 1991.
 Carl J. Schroeder wrote, "The imaginary friend is cavortingly rude for a reason; he served to push the girlchild to do mischief for attention and as a cry for help. Now grown up, the woman has forgotten and is about to lose her soul, so events call for some kind of literal return of her demon to force the exposure of her pain. This psychic crisis is poignantly realistic... The creature who is visible only to the woman is like a poltergeist energy of her repressed self, a problematic ego container into which her powers of assertion and creativity were poured and stored. The movies resolution is startlingly beautiful..." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 