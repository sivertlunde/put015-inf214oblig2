A Strange Course of Events
 
{{Infobox film
| name           = A Strange Course of Events
| image          = 
| caption        = 
| director       = Raphaël Nadjari
| producer       = 
| writer         = Geoffroy Grison Raphaël Nadjari
| starring       = Ori Pfeffer Moni Moshonov Michaela Eshet
| music          = Jean-Pierre Sluys  Jocelyn Soubiran  
| cinematography = Laurent Brunet
| editing        = Simon Birman
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = France Israel
| language       = Hebrew
| budget         = 
}}

A Strange Course of Events is a 2013 French-Israeli drama film directed by Raphaël Nadjari. It was screened in the Directors Fortnight section at the 2013 Cannes Film Festival.      

==Plot==
Saul is a matured man who is prone to avoid confrontations. One day he decides to visit his father who seems to suffer with a variety of issues and who usually blames the son for all his misfortune. Their first encounter after five years is rather alienating for Saul who discovers that his father is into yoga.

==Cast==
* Ori Pfeffer as Saul
* Moni Moshonov as Simon
* Michaela Eshet as Bathy
* Maya Kenig as Orly
* Bethany Gorenberg as Michal
* Maya Dagan as Ronit

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 
 