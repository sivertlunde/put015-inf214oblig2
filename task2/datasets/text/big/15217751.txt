Out of the Fog (film)
{{Infobox film
| name           = Out of the Fog
| image          = Out of the Fog FilmPoster.jpeg
| alt            =
| caption        = Theatrical release poster
| director       = Anatole Litvak
| producer       = Hal B. Wallis
| screenplay     = {{plainlist|
* Robert Macaulay
* Robert Rossen
* Jerry Wald
}}
| based on       =  
| starring       = {{plainlist|
* John Garfield
* Ida Lupino Thomas Mitchell
}}
| music          = Heinz Roemheld
| cinematography = James Wong Howe
| editing        = Warren Low
| distributor    = Warner Bros.
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Thomas Mitchell. The film was based on the play Gentle People by Irwin Shaw. 

==Plot== extorts "protection" money of $5 a week from them. Goodwins daughter (Lupino) falls in love with Goff who learns that Goodwin has tried to persuade her to holiday in Cuba. After he demands $190 from them, the sum Goodwin had promised his daughter, the fishermen plan to kill the gangster. Neither though can go through with the act. When the gangster realises what they intend to do, he attempts to strike one of them but falls into the sea and drowns. Goff turns out to have been a wanted man in five cities, and they recover the extorted money.

==Cast==
 
 
* John Garfield as Harold Goff
* Ida Lupino as Stella Goodwin Thomas Mitchell as Jonah Goodwin
* John Qualen as Olaf Johnson
* Eddie Albert as George Watkins
* George Tobias as Igor Propotkin
* Aline MacMahon as Florence Goodwin
 
* Jerome Cowan as Assistant district attorney
* Odette Myrtil as Caroline Pomponette
* Leo Gorcey as Eddie
* Robert Homans as Officer Magruder
* Bernard Gorcey as Sam Pepper Paul Harvey as Judge Moriarty
 

==Reception==
On release, the film was criticized due to changes from the play, and the box office gross was lower than expected.   A contemporary review from Bosley Crowther of The New York Times described it as "a heavy and dreary recital of largely synthetic woes, laced with moderate suspense and spotted here and there with humor".   Writing at Bright Lights Film Journal, Alan Kohn said, "As Goff, Garfield — the progressive, the true common man whose miserable fate it was to be destroyed through the Hollywood Blacklist travesty — reveals the depravity and fantasy of Depression-era capitalist society as he condemns all fascist forces at play in a world at war."  

==References==
 

==External links==
*  
*  
*  
*  
*   informational site and DVD review at DVD Beaver (includes images) 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 