Love in a Wood
{{Infobox film
| name           = Love in a Wood
| image          =
| caption        =
| director       = Maurice Elvey
| producer       = 
| writer         = William Shakespeare (play)   Kenelm Foss
| starring       = Gerald Ames   Elisabeth Risdon   Kenelm Foss
| music          = 
| cinematography = 
| editing        = 
| studio         = London Films
| distributor    = Jury Films
| released       = November 1915  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent comedy film directed by Maurice Elvey and starring Gerald Ames, Elisabeth Risdon and Kenelm Foss. The film is a contempary-set version of William Shakespeares play As You Like It. 

==Cast==
*  Gerald Ames as Orlando 
* Vera Cunningham as Celia  
* Kenelm Foss as Oliver 
* Cyril Percival
* Elisabeth Risdon as Rosiland   Frank Stanmore as Touchstone  
* Dolly Tree

==References==
 

==Bibliography==
* Murphy, Robert. Directors in British and Irish Cinema: A Reference Companion. British Film Institute, 2006.

==External links==
*  

 

 
 
 
 
 
 
 
 

 