Darling of the Gods
 
{{Infobox film
| name           = Darling of the Gods
| image          = 
| image_size     = 
| caption        = 
| director       = Hanns Schwarz
| producer       = Erich Pommer Hans Müller (play)   Robert Liebmann   Richard Rillo
| narrator       =  Hans Moser
| music          = Willy Schmidt-Gentner 
| editing        = Willy Zeyn
| cinematography = Konstantin Irmen-Tschet   Günther Rittau 
| studio         = Universum Film AG
| distributor    = Universum Film AG
| released       = 13 October 1930
| runtime        = 112 minutes
| country        = Germany
| language       = German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} musical drama Hans Müller. It was made by Erich Pommers production unit, part of the German Major film studio Universum Film AG|UFA. It premiered at the Gloria-Palast in Berlin on 13 October 1930. 

==Synopsis==
A selfish opera singer leaves his wife and home in Germany to travel the worlds great cities. Eventually he is drawn back to his Bavarian homeland.

==Cast==
* Emil Jannings as Albert Winkelmann 
* Renate Müller as Agathe 
* Olga Tschechowa as Olga von Dagomirska  Hans Moser as Kratochvil 
* Max Gülstorff as The Medizinalrat 
* Eduard von Winterstein as Dr. Marberg 
* Willy Prager as Maurus Colwyn 
* Siegfried Berisch as Romanones 
* Vladimir Sokoloff as Boris Jussupoff 
* Evaristo Signorini as Filipo Cardagno 
* Oskar Sima as Member of the Embellishment Company 
* Truus Van Aalten   
* Ethel Reese-Burns
* Betty Bird   
* Lilian Ellerbusch   
* Betty Gast   
* Lydia Pollman   
* Valentine Wischnewskaja   
* Fritz Alberti   
* Luigi Bernauer   
* Fritz Greiner   
* Fritz Spira   
* Marcel Wittrisch

==References==
 

==Bibliography==
* Hardt, Ursula. From Caligari to California: Erich Pommers Life in the International Film Wars. Berghahn Books, 1996.
* Rogowski, Christian. The Many Faces of Weimar Cinema: Rediscovering Germanys Filmic Legacy. Camden House, 2010.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 