Bowery Daze
{{Infobox Hollywood cartoon|
| cartoon_name = Bowery Daze
| series = Krazy Kat
| image = 
| caption = 
| director = Manny Gould Ben Harrison
| story_artist = Harry Love
| animator = Allen Rose Preston Blair
| voice_actor = 
| musician = Joe de Nat
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures 
| release_date = March 30, 1934
| color_process = Black and white
| runtime = 6:12 English
| preceded_by = Cinder Alley
| followed_by = Busy Bus
}}

Bowery Daze is a 1934 short animated film distributed by Columbia Pictures. The film is one of the many animated adaptations featuring Krazy Kat who started out as a comic strip character.

==Plot==
Krazy is a bartender of a tavern on the street of Bowery. He serves his patrons by filling their mugs with lager from barrels.

When the pianist of the place is done playing, Krazy conducts for an orchestra to play some music. As the music plays, a stage act featuring dancing ladies takes place. The dancing ladies are fronted by none other than Krazys spaniel girlfriend. Krazys act is followed by one featuring a bass singer walrus singing a melancholy song.

Moments later, while Krazy and the spaniel are at a table chatting, a fat Onion Johnny comes in. The Onion Johnny takes the spaniel, and dances with her regardless of what she thinks. Krazy, who is a bit bothered, intervenes. When Krazy and spaniel pummel at the Onion Johnny, the patrons hurl glassware around before going into a brawl. Eventually the spaniel restrains the Onion Johnny in a corset.

==External links==
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 

 