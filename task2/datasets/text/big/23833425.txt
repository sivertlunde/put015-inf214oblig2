Devět kruhů pekla
{{Infobox film
| name           = Devět kruhů pekla
| image          = 
| caption        = 
| director       = Milan Muchna Jan Syrový
| writer         = Milan Muchna Alexander Cukes
| starring       = Milan Kňažko
| music          = 
| cinematography = Petr Hojda
| editing        = Dalibor Lipský
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
}}

Devět kruhů pekla (Nine Circles of Hell) is a 1989 Czechoslovak drama film directed by Milan Muchna. It was screened in the Un Certain Regard section at the 1989 Cannes Film Festival.   

The film describes the Khmer Rouge period in Cambodia. Tomáš is a Czech doctor working in Phnom Penh and Khema is famous Khmer actress.

==Cast==
* Milan Kňažko - Tomáš
* Oum Savanny - Khema
* Nov Chandary - Chivan
* Petr Čepek - Tomáš (voice)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 