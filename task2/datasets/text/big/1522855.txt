It Conquered the World
{{Infobox film
| name           = It Conquered the World
| image          = It Conquered the World.jpg Theatrical release poster  by Albert Kallis
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Lou Rusoff Charles B. Griffith (uncredited) Peter Graves Lee Van Cleef Beverly Garland Sally Fraser
| music          = Ronald Stein
| cinematography = Fred E. West
| editing        = Charles Gross
| distributor    = American International Pictures
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} American black-and-white Peter Graves, Lee Van Cleef, Beverly Garland and Sally Fraser. The film was distributed by American International Pictures. Warren 1982  

It Conquered the World concerns an alien creature from the planet Venus who secretly wants to take control of the Earth. The creature makes radio contact with a disillusioned human scientist, who agrees to help because the scientist believes such an alien intervention will bring peace and save a doomed humanity from itself.

==Plot== Venusian creature, while using his radio transmitter. The aliens secret motivation is to take complete control of the Earth by enslaving humanity using mind control devices; the alien claims it only wants to bring peace to our troubled world by eliminating all emotions. Anderson agrees to help the creature and even intends to allow it to assimilate his wife (Beverly Garland) and friend Dr. Nelson (Peter Graves). 

Zontar then disrupts all electric power on Earth, including motor vehicles, leaving Dr. Nelson to resort to riding a bicycle.

After killing a flying bat-like creature which carries the mind control device, Dr. Nelson returns home to find his wife newly assimilated. She then attempts to force his own assimilation using another bat-creature in her possession, and he ends up being forced to kill her in self-defense. By then, the only people who are still free from Zontars influence are Nelson, Anderson, Andersons wife, and a group of army soldiers on station in the nearby woods. 

Nelson finally persuades the paranoid Anderson that he has made a horrible mistake in blindly trusting Zontars motives, allying himself with a creature bent on world domination. When they discover Toms wife has taken a rifle to the aliens cave in order to kill it, they hurriedly follow her; but the creature kills Claire Anderson before the two doctors can rescue her. Finally, seeing the loss of everything he holds dear, Dr. Anderson viciously attacks Zontar by holding a blowtorch to the creatures face; Anderson dies at the aliens hand as it expires. 

==Cast==
  Peter Graves as Dr. Paul Nelson
* Lee Van Cleef as Dr. Tom Anderson
* Beverly Garland as Claire Anderson
* Sally Fraser as Joan Nelson
* Russ Bender as General James Pattick
* Taggart Casey as Sheriff N.J. Shallert
* Karen Kadler as Dr. Ellen Peters
* Dick Miller as First Sergeant
* Jonathan Haze as Corporal Manuel Ortiz
* Paul Harbor as Dr. Floyd Mason
* Charles B. Griffith as Dr. Pete Shelton
* Thomas E. Jackson as George Haskell
 

==Production==
It Conquered the World was written by Lou Rusoff, but before being completed, Rusoffs brother died and he had to leave for Canada. Roger Corman then called in Charles Griffith to do a final rewrite, two days before filming began; Griffith, however, didnt want his name to be credited on screen.  Graham, Aaron W.   Senses of Cinema, April 15, 2005. Retrieved: January 13, 2015. 

The design of the creature was Cormans idea, and he thought that coming from a big planet, It would have evolved to deal with heavy gravity and would therefore be low to the ground. Corman later admitted this was a mistake, saying the creature would have been more frightening had It been larger or taller. When Beverly Garland first saw the creature, she commented "That conquered the world?" and kicked It over. McGee 1996,  p. 58.  Griffith:
 I called it Denny Dimwit and somebody else called it an ice-cream cone. I was around when Paul Blaisdell was building it, and he thought the camera would make it look bigger. I have some photographs of it in construction, probably the only ones in existence. I asked for my name not to be on that picture, so I was unbilled. Surprisingly, it got good reviews.  

==Release History== RCA Columbia Home Video) but these are no longer in distribution, nor is the film available on DVD or Blu-ray in the US  or in the United Kingdom|UK. 

Before its theatrical release in England, British censors expressed concern about the scene near the end where the creature is destroyed by a blowtorch, on the grounds that it depicted cruelty to animals. AIP successfully argued an enemy alien from outer space was not an animal. 

==Reception==
===Reviews=== Time Out magazine, however, gave the film a negative review, criticizing the film for its poor special effects. Critic Tony Rayns opined, "You have to see a movie like this to realise that film-makers who feel they have nothing to lose are rarer than youd think." Chicago Reader gave the film a generally positive review, saying "Amazingly, this 1953 picture isnt half bad  ." 
 All Movie Guide gave the film three out of five stars, calling it an "above-average quickie." 
 

===In popular culture===
*In 1966, It Conquered the World was remade in 16mm color by self-proclaimed "schlockmeister" Larry Buchanan after he secured rights from AIP; he retitled his rewritten remake Zontar, the Thing from Venus and then sold it to television syndication.
*Frank Zappas 1974 live album Roxy & Elsewhere referred to the film in the introduction for the song "Cheepnis". 
*The end of the film was shown at the beginning of Elvira, Mistress of the Dark (1988).
*In 1991, It was the subject of the comedy television series Mystery Science Theater 3000; joke topics included poor monster props, occasionally wooden acting and an overblown closing monologue.  self titled debut album, released in 2001. 

==References==
Notes
 
Bibliography
 
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
* McGee, Mark. Faster and Furiouser: The Revised and Fattened Fable of American International Pictures. Jefferson, North Carolina: McFarland & Company, 1996. ISBN 978-0-78640-137-6.
* Bill Warren (film historian and critic)|Warren, Bill. Keep Watching The Skies Vol. I: 1950–1957. Jefferson, North Carolina: McFarland & Company, 1982. ISBN 0-89950-032-3.
 

==External links==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 