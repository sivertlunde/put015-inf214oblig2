Wills & Burke
{{Infobox film
| name           = Wills & Burke
| image          =
| caption        =
| director       = Bob Weis
| producer       = Bob Weis Margot McDonald
| writer         = Philip Dalkin
| based on       =
| starring       = Garry McDonald Kim Gyngell Nicole Kidman
| music          = Red Symonds Paul Gabrowsky
| cinematography =
| editing        =
| studio         = Stony Desert Productions
| distributor    = Greater Union
| released       =  
| runtime        =
| country        = Australia English
| budget         = A$1.75 million 
| gross          = A$54,000 (Australia) 
}}
Wills & Burke is a 1985 Australian comedy film about the Burke and Wills expedition. It came out the same year as another local film on the same subject, Burke & Wills (1985). 

==Plot==
Burke and Wills travel across Australia but wind up dying.

==Cast==
*Garry McDonald as Robert OHara Burke
*Kym Gyngell as William John Wills
*Nicole Kidman as Julia Matthews
*Peter Collingwood as Sir William Stawell
*Jonathan Hardy as John Macadam

==Production==
Philip Dalkin first wrote the story as a play, which Bob Weis read in 1978. He enjoyed it and at one stage thought of making it as a mini series before deciding on turning it into a feature. It was shot over six weeks in and around Melbourne. 

==Release==
The film was not a success at the box office. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p34 

==References==
 

==External links==
*  at Burkeandwills.net.au
*  at IMDB
*  at Oz Movies
 
 
 
 


 