Supernatural Activity
{{Infobox film
| name           = Supernatural Activity
| image          =File:Supernatural Activity film poster.jpg
| alt            =
| caption        =
| director       = Derek Lee Nixon
| producer       = {{plainlist|
* Kurt Wehner
* Patrick Cassidy
* Philip Marlatt
* Karina Medellin
}}
| writer         = Andrew Pozza
| starring       = {{plainlist|
* Andrew Pozza
* Liddy Bisanz
* Donny Boaz
* Joey Oglesby
}} 
| music          = Nathaniel Rendon
| cinematography = Michael Cano
| editing        = Brett Houston
| studio         = Aristar Entertainment
| distributor    =
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Paranormal Activity, The Last Exorcism, and The Exorcism of Emily Rose.     It stars Joey Oglesby, Donny Boaz, Andrew Pozza, Devin Bonnée, and Lizabeth Waters as paranormal investigators.

== Plot ==
A group of paranormal investigators go to Texas to expose a cryptid called the Smallsquatch as a fake.  However, they find evidence that suggests it may be more real than they initially thought.  Along the way, the encounter a possessed girl by the name of Emily Sunflower.

== Cast ==
* Andrew Pozza as Damon Dealer
* Liddy Bisanz as Blair Woods
* Donny Boaz as Brock Haas
* Joey Oglesby as Pepper Shepard
* Devin Bonnée as Doug
* Tim Ogletree as Dewey Normal
* Lizabeth Waters as Mitzy Para
* Conley Michelle Wehner as Emily Sunflower

== Release ==
Signature Entertainment released Supernatural Activity in the UK on August 13, 2012,  and Well Go USA released it in the US on November 6, 2012. 

== Reception ==
Chris Ball of The Plain Dealer called it a "very silly, mildly suggestive, mostly dumb 2012 spoof".   Rod Lott of the Oklahoma Gazette wrote that the film is juvenile and "simply not funny".   Lauren Taylor of Bloody Disgusting rated it 3/5 stars and wrote, "Ultimately, if you like goofball movies in the realm of Scary Movie, you’ll like Supernatural Activity. It is a well made film with a plot that is so bad that its pretty good."   Mark L. Miller of Aint It Cool News wrote, "While the film does have it’s fair share of groaners, ... it makes up for it with some clever gags".   TGM of HorrorTalk rated it 2/5 stars and wrote, "Im convinced that if Mr. Pozza parodied a genre that wasn’t so creatively exhausted that  he has it in him to write a truly hilarious movie. Unfortunately this  one isnt it."   Rohit Rao of DVD Talk rated it 1.5/5 stars and wrote, "Theres more of a creative spark here than your average Friedberg/Seltzer collaboration but the payoff is still severely lacking."   David Johnson of DVD Verdict wrote, "Whatever Supernatural Activity is, you should steer clear. The laughs are entirely absent, the melodrama is weird and nonsensical, and the whole thing wound up being one of the slowest 90-minute experiences of my life." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 