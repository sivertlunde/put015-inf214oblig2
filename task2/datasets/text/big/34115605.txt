Disorderly Conduct (film)
{{Infobox film
| name           = Disorderly Conduct
| image_size     =
| image	         = Tracy disorderly conduct.jpg Publicity still Dickie Moore.
| director       = John W. Considine Jr. William Fox
| writer         = William Anthony McGuire
| narrator       =
| starring       = Spencer Tracy Sally Eilers
| music          = George Lipschultz
| cinematography = Ray June
| editing        =
| distributor    = Fox Film Corporation
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $300,000 James Curtis, Spencer Tracy: A Biography. London: Hutchinson, 2011. ISBN 0-09-178524-3. p173 
| gross = $427,659 (US rentals) 
}}
Disorderly Conduct is a 1932 film directed by John W. Considine Jr. and starring Spencer Tracy. It was the seventh picture Tracy made under his contract with Fox Film Corporation, and the first to make a profit since his debut Up the River. 

Mordaunt Hall, in his review for The New York Times, praised the films "racy dialogue and highly commendable performances", but bemoaned the "strained and implausible" story. 

==Plot==
The movie stars Spencer Tracy as a policeman who becomes involved with a young woman (Sally Eilers) after clashing with her politician father (Ralph Morgan).

==Cast==
*Spencer Tracy - Dick Fay
*Sally Eilers - Phyllis Crawford
*El Brendel - Olsen Dickie Moore - Jimmy
*Ralph Bellamy - Captain Tom Manning
*Ralph Morgan - James Crawford
*Alan Dinehart - Fletcher
*Frank Conroy - Tony Alsotto
*Cornelius Keefe - Stallings
*Geneva Mitchell - Phoebe Darnton
*Sally Blane - Helen Burke
*Claire Maynard - Lunch Room Girl
*Nora Lane - Gwen Fiske
*Charley Grapewin - Limpy
*James Todd - Pierce Manners

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 

 