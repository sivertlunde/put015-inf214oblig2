Chongqing Blues
{{Infobox film
| name           = Chongqing Blues
| image          = Chongqing Blues.jpg
| image size     = 
| caption        = 
| director       = Wang Xiaoshuai
| producer       = Hsu Hsiao-ming Wang Xiaoshuai
| writer         = Yang Yishu Wang Xiaoshuai
| narrator       = 
| starring       = Wang Xueqi 
| music          =  Wu Di
| editing        = Yang Hongyu Fang Lei
| studio         = Tempo Films
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = China
| language       = Mandarin
| budget         = 
| preceded by    = 
| followed by    = 
}}
Chongqing Blues ( , pinyin: Rizhao Chongqing; literally Sunshine over Chongqing) is a 2010 Chinese drama film directed by Wang Xiaoshuai. It was selected for the main competition at the 2010 Cannes Film Festival. 

==Plot==
Lin Quanhai, a sea captain and father, returns from a six-month journey and is informed that his 25-year-old son Lin Bo has been shot by the police. In finding out what happened, he comes to realize that he knew little of his son. He starts journeying back to Chongqing, a city where he once lived. He begins to understand the effect that his repeated absence had on his sons life.


==Cast==
* Wang Xueqi as Lin Quanhai, the father
* Qin Hao as Xiao Hao, the friend
* Zi Yi as Lin Bo, the son
* Li Feier as Xiao Wen, the girlfriend

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 

 