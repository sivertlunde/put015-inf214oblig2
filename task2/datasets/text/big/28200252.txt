Senza Tempo
 
{{Infobox film
| name           = Senza Tempo
| image          = 
| image_size     = 
| caption        =
| director       = Gabriele Muccino
| producer       = Lorella Stortini
| writer         =  
| starring       =  
| music          = 
| cinematography = Arnaldo Catinari
| editing        =
| distributor    = Peroni Nastro Azzurro, SABMiller plc
| released       =  
| runtime        = 4 minutes
| country        = United Kingdom
| language       = Italian
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Senza Tempo (Timeless in English) is a short film by Italian director Gabriele Muccino, director of Lultimo bacio (One Last Kiss), Remember Me, My Love (Ricordati di me) with Monica Bellucci and The Pursuit of Happyness with Will Smith. The film is the result of a collaboration between Peroni Nastro Azzurro and Muccino and was shown in cinemas throughout the UK. 

== Plot ==
Senza Tempo (Timeless) is a tale of enduring passion, lasting love and timeless Italian style. It tells the story of a young Italian director as he recreates through film, the love he lost but has never forgotten. The Blue Ribbon is a symbol of the enduring nature of his passion, and the timelessness of love.

== Production ==
The film was made as a joint production between Peronis ad agency The Bank and Muccinos company Indiana Production.  It is part of Accademia del Film Peroni Nastro Azzurro, which promotes the iconic world of Italian film in the UK by celebrating the principles that have made and continue to make the Italian craft of film-making so unique. Part of the films creation included giving a group of hand-picked, up and coming film talent the opportunity to learn these principles directly from Muccino himself during the creation of the film.  

The ‘Academy’ celebrates Italian film through a series of initiatives illustrating how Italian film can make the ordinary extraordinary, using the following founding principles:

* Heritage - Understanding the family of past masters to inform the present and craft the future
* Imagination - Storytelling without constraints or formulas
* Passion - Unbridled emotions on both sides of the camera
* Realism - Reflecting the beauty of the ordinary
* Visual Aesthetic - Making every frame a work of art
* Natural Style - Exuding effortless style and an independent attitude

== References ==
 
 

== External links ==
*  

 

 
 
 
 
 