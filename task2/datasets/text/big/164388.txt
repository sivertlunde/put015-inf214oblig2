The Great Santini
 
{{Infobox film
| name = The Great Santini
| image = Great santini.jpg
| caption = Theatrical release poster
| producer = Charles A. Pratt
| director = Lewis John Carlino
| screenplay = Lewis John Carlino Herman Raucher The Great Santini by Pat Conroy
| starring = Robert Duvall Blythe Danner Michael OKeefe
| music = Elmer Bernstein
| cinematography = Ralph Woolsey
| editing = Houseley Stevenson Jr.
| distributor = Warner Bros. & Orion Pictures
| released = October 26, 1979
| runtime = 115 min. English
| gross= $4,702,575 (USA)
}} 1976 novel Brian Andrews, Stan Shaw and David Keith.
 Marine officer heroism and self-sacrifice. The film is set in 1962 before widespread American involvement in the Vietnam War.

==Plot==
A warrior without a war, Lt. Col. "Bull" Meechum, a pilot also known as "The Great Santini" to his fellow Marines, moves his family to the military-base town of Beaufort, South Carolina in peacetime 1962. His wife Lillian is loyal and docile, tolerant of Meechums temper and drinking. Their teenaged kids, Ben and Mary Anne, are accustomed to his stern discipline and behave accordingly, while adapting to their new town and school.

Ben is a basketball star. On the court at school, he is a dominating player. In one-on-one games on his driveway at home, his father wont let him win, even if it means using unnecessarily physical tactics or humiliating the boy, bouncing the ball off him. Ben is publicly embarrassed one night at the school gym when his dad, drunk, orders him to get even with an opponent who committed a foul. Ben decks the boy and is ejected from the game.

Ben befriends a young black man called Toomer, who is being harassed by Red Petus, a bigoted bully. Toomer exacts revenge on Red with the help of a hive of bees. But tragic consequences ensue.

Meechum is unwilling or unable to appreciate the sensitive nature of his son. Their relationship is still a delicate one when the Great Santini flies one last mission, a military maneuver, from which he does not return.

==Cast==

* Robert Duvall as Lt. Col. Wilbur "Bull" Meechum
* Michael OKeefe as Ben
* Blythe Danner as Lillian
* Lisa Jane Persky as Mary Anne
* Stan Shaw as Toomer
* David Keith as Red
* Theresa Merritt as Annabelle

==Production notes==
The script was adapted by Lewis John Carlino from the novel, with assistance from an un-credited Herman Raucher. Carlino directed the film. The title character, Lt. Col. Wilbur "Bull" Meechum, aka "The Great Santini", was based on Conroys father.
 The Big Chill.

The story, for the most part, follows the book. The movies major divergence is the absence of Sammy, Ben Meechams Jewish best friend. The spelling of the familys name was also changed from Meecham to Meechum. Also changed is that in the book Meecham flies and commands a squadron of F-8 Crusaders while in the film the fighters shown are F-4 Phantom IIs.

The film was shot in a 1.85:1   or pan & scan. To date the film has not had a release in the Blu-ray Disc format.

==Release==
Warner Bros. executives were concerned that the films plot and lack of bankable actors would make it hard to market. It made its world premiere in Beaufort in August 1979 and was soon released in North Carolina and South Carolina to empty houses. Believing that the films title - giving the perception  that it was about circus stunts - was the problem, it was tested as Sons and Heroes in Fort Wayne, Indiana, as Reaching Out in Rockford, Illinois, and The Ace in Peoria, Illinois. As it tested better in Peoria, The Ace stuck, though even with its new title it was still performing poorly. Orion Pictures eventually pulled the film and sold cable rights to HBO along with the airline rights to recoup its losses. 

Producer Charles A. Pratt still had faith in the film and raised enough money, some coming from Orion, to release The Great Santini in New York under its original title. It ended up getting great reviews and business was steady, but two weeks later debuted on HBO, and audiences stopped coming. Orion executive  : Atria Books 

==Critical reception==
The film was well received. Roger Ebert wrote that "Like almost all my favorite films, The Great Santini is about people more than its about a story. Its a study of several characters, most unforgettably the Great Santini himself, played by Robert Duvall...There are moments so unpredictable and yet so natural they feel just like the spontaneity of life itself." 

==Awards== Best Actor in a Supporting Role (OKeefe).

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 