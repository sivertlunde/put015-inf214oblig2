Center Stage (2000 film)
 
{{Infobox film
| name = Center Stage
| image = Center Stage movie poster.jpg
| border = yes
| caption = Theatrical release poster
| director = Nicholas Hytner
| producer = Laurence Mark
| writer = Carol Heikkinen
| starring = Amanda Schull Zoe Saldana|Zoë Saldana Susan May Pratt Peter Gallagher Debra Monk Ethan Stiefel Sascha Radetsky
| music = George Fenton
| cinematography = Geoffrey Simpson Tariq Anwar
| distributor = Columbia Pictures
| released =  
| runtime = 115 minutes
| country = United States
| language = English
| budget = $18 million 
| gross = $26,385,941.  . Box Office Mojo. Retrieved 2011-04-10. 
}}
 teen drama film, directed by Nicholas Hytner, about a group of young dancers from various backgrounds who enroll at the fictitious American Ballet Academy in New York City. The film explores the issues and difficulties in the world of professional dance, and how each individual copes with the stresses.

==Plot==
After a series of country-wide auditions, 12 young dancers gain entry to the American Ballet Academy (which is loosely based on the School of American Ballet). They work hard, attending classes every day for weeks to make them the best dancers they can possibly be, and in preparations for a final dance workshop which will determine the three boys and three girls who will be asked to join the American Ballet Company (which appears to be based on either the American Ballet Theatre or the New York City Ballet). The workshop will also provide an opportunity for the students to showcase their talent to other ballet companies across the country. Gaining a leading part in the workshop is therefore essential.
 choreographer and best dancer, corps because of her attitude. Tensions also arise between Charlie (a naturally gifted fellow student) and Cooper. Charlie has a crush on Jody, who had a one-night stand with Cooper and remains infatuated with him.

Despite Jonathans objections, Cooper choreographs a rock music based ballet for the workshop. Three ballets are being presented; Jonathan and another choreographer create the other two respectively—the two more "traditional" ballets are not danced to actual ballet music, however. The first is to Felix Mendelssohn|Mendelssohns Italian Symphony, while Jonathans ballet is set to Rachmaninovs 2nd Piano Concerto. Coopers ballet mirrors the relationship between himself, Jonathan, and Kathleen (and also, though done unconsciously, Jody, Charlie and Cooper). Jody, Charlie, and Erik (Shakiem Evans) are set to dance the three lead roles when Erik sprains his ankle in a rehearsal. Cooper then steps in to fill the role, and the tensions between Jody, Charlie and Cooper play out on the stage.

After the final workshop, Cooper starts his own dance company - much to Jonathans chagrin, as Coopers financial backer is a woman that Jonathan was hoping would donate to his own company. Cooper asks Jody to be a principal dancer as her dancing style, though technically behind, is perfect for the kind of dance he wants in his company. He also asks to date her, but Jody turns him down in favor of Charlie. Maureen decides to give up ballet because she finally realizes that ballet is just something she does well, and not what she wants from life. She decides to attend regular university and also seek help for her eating disorder. Eva is picked by Jonathan to join the prestigious American Ballet Company after proving her worth in the workshop - secretly taking the place of Maureen, who had the lead, in Jonathans ballet. Jodys boyfriend Charlie, and their friends Anna (a girl who was always favored by Jonathan) and Erik are also asked to join the American Ballet Company, and Sergei (a Russian dancer who also befriended them) joins his girlfriend in the San Francisco Ballet Company.

There is a subplot in which Cooper attracts the financial support of a flirtatious wealthy female philanthropist (played by Elizabeth Hubbard). An August 15, 2004 New York Times article entitled "How Much Is That Dancer in the Program?" revealed that Stiefel has a very similar real-life sponsorship relationship with a philanthropist named Anka Palitz.

==Cast==
* Amanda Schull as Jodi Sawyer
* Zoe Saldana|Zoë Saldana as Eva Rodríguez
* Susan May Pratt as Maureen Cummings
* Peter Gallagher as Jonathan Reeves
* Debra Monk as Nancy Cummings
* Ethan Stiefel as Cooper Nielson
* Sascha Radetsky as Charlie Sims
* Donna Murphy as Juliette Simone Julie Kent as Kathleen Donahue
* Ilia Kulik as Sergei
* Eion Bailey as Jim Gordon
* Shakiem Evans as Erik O. Jones
* Elizabeth Hubbard as Joan Miller
* Cody Green as Nick Hoffman
* Mauricio Sanchez as Dancer at Salsa Club
* Julius Catalvas as Instructor
* Priscilla Lopez as Broadway Dance Teacher

==Production== Julie Kent), one is a professional figure skater (Ilia Kulik), one had ballet training (Zoe Saldana), and two were actors with no ballet training (Susan May Pratt and Shakiem Evans). Body doubles were used for many of the major dance sequences. 

==Reception==

===Critical response===
 
Center Stage received moderate to negative reviews.

The New York Times critic A. O. Scott wrote for the film:
 

Contactmusic.com gave the film just two stars, commenting that:
 

The film currently holds a 43% "Rotten" score, on Rotten Tomatoes. 

===Box office===
The film opened at #6 at the box office making $4,604,621 USD in its opening weekend.  The film has grossed a total of $26,385,941 worldwide. 

==Sequel==
 
Center Stage: Turn It Up was first broadcast in the United States on November 1, 2008, on the Oxygen Network. The DVD was released in January 2009, by Sony Pictures Home Entertainment. The film was theatrically released in Australian cinemas in October 2008.

It stars newcomer Rachele Brooke Smith as dancer Kate Parker, who has only ever wanted to perform with the American Ballet Academy. When she doesnt make it after auditioning, she learns that it takes more than a little talent to succeed in the dance world. With a turn in a cutting -edge hip-hop club and the help of a good-looking hockey player-turned ballet dancer (played by Kenny Wormald), she may just find the passion she needs to make her dreams come true. The film also features Sarah Jayne Jensen, Crystal Lowe, Peter Gallagher and Ethan Stiefel.

==Soundtrack== I Wanna Be with You" - Mandy Moore
# "First Kiss" - i5
# "Dont Get Lost in the Crowd" - Ashley Ballard
# "Were Dancing" - P.Y.T. (band)|P.Y.T. Thunderbugs
# "Get Used to This" - Cyrena
# "A Girl Can Dream" - P.Y.T.
# "Cosmic Girl" - Jamiroquai Higher Ground" - Red Hot Chili Peppers Come Baby Come" - Elvis Crespo & Gizelle DCole
# "The Way You Make Me Feel" - Michael Jackson If I Was the One" - Ruff Endz Canned Heat" - Jamiroquai
# "I Wanna Be with You" (Soul Soul Solution Remix) - Mandy Moore

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 