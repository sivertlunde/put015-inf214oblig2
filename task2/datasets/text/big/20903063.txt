Trouble Makers
 

{{Infobox film 
|  name = Trouble Makers
|  image = Trouble_Makers_Poster.jpg
|  caption = 
|  director = Cao Baoping 
|  writer = Cao Baoping Que Diwei (novella)
|  starring = Li Xiaobo Kong Qingsan Wu Gang
|  producer = Zhang Yaoli Sun Yang Wang Ping Cao Baoping Liu Qing
|  cinematography = Tao Shiwei
|  editing = Cao Baoping
|  distributor = Infotainment China
|  released =  : June 30, 2007
|  runtime = 100 min. Mandarin
|  budget =   China
}}
Trouble Makers ( ) is a 2006 Chinese black comedy film written and directed by Cao Baoping. It was Caos solo directorial debut, though Cao had previously co-directed the film Absolute Emotion in 2001. Based on Que Diweis novella, "Village Operation," which was published in a magazine in 2000, Trouble Makers had difficulty traversing the Chinese censors and often found itself lacking funds until 2005.  As a result, Cao eventually inserted a happy ending that differed from ending in the original novella.   

== Plot ==
Trouble Makers tells the story of four simple men living in Black Well Village (in rural Yunnan province) who decide to finally run the criminal Xiong Brothers out of their homes. The Xiongs have managed to build a criminal "empire" in the small town even managing to elect themselves the mayor and official accountant of Black Well Village. Two other brothers enforce the Xiongs will with violence and intimidation. The brothers, known by the political-infused moniker, the Gang of Four, smuggle salt and generally terrorize the villagers of Black Well.

When Ye Guangrong, the nerdy and nervous local party secretary has had enough, he rounds up several other oppressed villagers including Tugua, an account, and Dawang, whose wife was raped by one of the brothers, to clean up the village.

Guangrong and his vigilantes quickly find themselves over their heads. Hiring less than savory characters like "Dog Balls," a kung-fu obsessed buffoon, Guangrong and his men give themselves officious sounding military titles. As events spiral out of control, a confrontation between the dangerous Xiong Brothers and Guangrongs vigilantes seems inevitable.

== Cast ==
* Li Xiaobo as Xiong Brother No. 1, the eldest of the four brothers who terrorize Dragon Well Village.
* Kong Qingsan as Xiong Brother No. 2, the second eldest of the brothers, he has appointed himself as the village accountant.
* Wan Yanhui as Xiong Brother No. 3, the villages "mayor."
* Zhi Yu as Xiong Brother No. 4, the youngest of the brothers, and a spikey haired punk he serves as the brothers enforcer. 
* Wu Gang as Party Secretary Ye Guangrong, the a nervous low-level Communist official who leads the opposition to the gang
* Wang Shujun as Tugua, a cowardly accountant.
* Li Changyuan as Dawang, another villager who has been terrorized by the Xiongs
* He Yunqing as Shuigen
* Pu Xiaohu as Dog Balls, a local recruited by Guangrong who is obsessed with kung fu.

== Release ==
Trouble Makers was premiered first in the small village in Yunnan where it was filmed. From there, it was screened at numerous interntanional film festivals and received its United States Premiere at the New York Asian Film Festival on June 30, 2007.  The year before, the film also premiered at several other boutique festivals, including the International Thessaloniki Film Festival,  which focuses on first and second features. At the 2006 Shanghai International Film Festival, Trouble Makers won the jury prize at the Asia New Talent side-competition. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 