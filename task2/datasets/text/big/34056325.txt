Każdemu wolno kochać
{{Infobox film
| name           = Każdemu wolno kochać
| image          =
| caption        = 
| director       = Mieczysław Krawicz
| producer       = 
| writer         = 
| based on       = 
| screenplay     = 
| starring       = Ludwik Lawiński Janina Brochwiczówna Józef Kondrat
| music          =
| cinematography = 
| editing        = 
| studio         = 
| distributor    =
| released       =  
| runtime        = 80 minutes
| country        = Poland Polish
| budget         =
}}

Każdemu wolno kochać (Anybody Can Love   ) is a 1933 Polish romantic comedy film directed by Mieczysław Krawicz and produced by the Rex-Film studio.

==Cast==
* Ludwik Lawiński - dyrektor teatru
* Stanisława Kawińska - Weronika
* Janina Brochwiczówna - gwiazda rewiowa
* Józef Kondrat - widz w teatrze
* Henryk Małkowski - dozorca
* Ludwik Fritsche - professor Robaczek
* Mariusz Maszyński - Alojzy Kędziorek
* Liliana Zielińska - Renia
* Feliks Chmurkowski - gość u Renaty
* Czesław Skonieczny - Maciej Baleron Mira Zimińska - Lodzia
* Józef Orwid - właściciel kamienicy
* Adolf Dymsza - Hipek
* Witold Conti - Gwiazdor rewiowy
* Stanisław Łapiński - Burmistrz

==References==
 

==External links==
*  

 
 
 
 
 
 


 
 