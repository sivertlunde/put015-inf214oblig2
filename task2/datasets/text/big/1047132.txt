Star Tours
 
 
{{Infobox attraction
 
| name                = Star Tours
| logo                = 
| logo_width          = 
| image               = Star Tours poster.jpg
| imagedimensions     = 250px
| caption             = Attraction poster.
  Disneyland Park (Anaheim)
| section             = Tomorrowland
| coordinates         =  
| status              =  Closed
| cost                =
| soft_opened         = December 1986
| opened              = January 9, 1987
| closed              = July 27, 2010
| previousattraction  = Adventure Thru Inner Space
| replacement         = Star Tours—The Adventures Continue
| altname2            =
| location2           = Tokyo Disneyland
| section2            = Tomorrowland
| coordinates2        =  
| status2             = Closed
| cost2               =
| soft_opened2        =
| opened2             = July 12, 1989
| closed2             = April 2, 2012
| previousattraction2 =
| replacement2        = Star Tours—The Adventures Continue
| location3           = Disneys Hollywood Studios Echo Lake
| coordinates3        =  
| status3             = Closed
| cost3               =
| soft_opened3        =
| opened3             = December 15, 1989
| closed3             = September 7, 2010
| previousattraction3 =
| replacement3        = Star Tours—The Adventures Continue
| location4           = Disneyland Park (Paris) Discoveryland
| coordinates4        =  
| status4             = Operating
| cost4               =
| soft_opened4        =
| opened4             = April 12, 1992
| closed4             = Late 2015 or Early 2016
| previousattraction4 =
| replacement4        =Star Tours—The Adventures Continue
 
| type                = Flight simulator with Audio-Animatronics synced to film
| manufacturer        = Rediffusion Simulation
| designer            = Walt Disney Imagineering
| model               =
| theme               =  
| music               = Richard Bellis 
| height_ft           =
| drop_ft             =  
| drop_m              =  
| length_ft           =  
| length_m            =  
| speed_mph           =  
| speed_km/h          =  
| sitearea_sqft       =  
| sitearea_sqm        =  
| gforce              =
| capacity            =
 
| vehicle_type        =
| vehicles            = 4-6
| riders_per_vehicle  = 40
| rows                =
| riders_per_row      =  
| participants_per_group= 40
| audience_capacity   =  
| duration            = 4:30
| restriction_in      = 40
| virtual_queue_name  = Disneys Fastpass
| virtual_queue_image = Fastpass availability icon.svg
| virtual_queue_status= available
| single_rider        =
| pay_per_use         =  
| custom_label_1      = Audio-animatronics
| custom_value_1      = Yes
| custom_label_2      = Host
| custom_value_2      = Captain Rex (Paul Reubens)
| custom_label_3      =
| custom_value_3      =
| custom_label_4      =
| custom_value_4      =
| custom_label_5      =
| custom_value_5      =
| custom_label_6      =
| custom_value_6      =
| custom_label_7      =
| custom_value_7      =
| custom_label_8      =
| custom_value_8      =
| accessible          =
| transfer_accessible = yes
| assistive_listening =
| cc                  =
| small               =  
}}

Star Tours is a motion simulator attraction currently operating at Disneyland Paris. The ride is based on the successful Star Wars (film series)|Star Wars film series created by George Lucas. At its debut at Disneyland in 1987, it was the first Disney attraction based on a non-Disney produced film. However, in October 2012, the Walt Disney Company acquired Lucasfilm, and in turn, now owns the Star Wars franchise. 

The first incarnation of the ride appeared in Tomorrowland at Disneyland in 1987, replacing the previous attraction, Adventure Thru Inner Space. Star Tours at Disneyland closed on July 27, 2010 to allow for its sequel, Star Tours—The Adventures Continue. Disneys Hollywood Studios closed its attraction on September 7, 2010 in anticipation of the same conversion which was completed on May 20, 2011. Tokyo Disneylands Star Tours closed on April 2, 2012, to make way for Star Tours—The Adventures Continue which opened on May 7, 2013.    On November 7, 2014, it was announced that Disneyland Paris will receive the updated ride in 2017, which will mark the original rides final closure. 

==History==
 
The ride that became Star Tours first saw light as a proposal for an attraction based on the 1979 Disney live-action film The Black Hole. It would have been an interactive ride-simulator attraction where guests would have had the ability to choose the route. However, after preliminary planning the Black Hole attraction was shelved due to its enormous cost&mdash;approximately $50 million USD&mdash;as well as the unpopularity of the film itself.
 Imagineers purchased four military-grade flight simulators at a cost of $500,000 each and designed the ride structure.

Meanwhile, Lucas and his team of special effects technicians at Industrial Light & Magic produced the first-person perspective film that would be projected inside the simulators. When both simulator and film were completed, a programmer then sat inside and used a joystick to synchronize the movement of the simulator with the apparent movement on screen. On January 9, 1987, at a final cost of $32 million, almost twice the cost of building the entire park in 1955, the ride opened to throngs of patrons, many of whom dressed up as Star Wars characters for the occasion. In celebration, Disneyland remained open for a 60-hour marathon from January 9 at 10 a.m. to January 11 at 10 p.m.

===Closing=== Hyperspace Hoopla, D23 members.

==Attraction==
 
Advertised as "The Ultimate Star Wars Adventure!", Star Tours puts the guest in the role of a  , via the Star Tours travel agency. Much is made of this throughout the ride queue, which is designed to look like a spaceship boarding terminal: posters advertise voyages to different planets, and a giant screen informs riders of the benefits of going to Endor. This area is stocked with Audio-Animatronics|Audio-Animatronic characters that seem to speak to the ride patrons (including C-3PO and R2-D2), as well as a life-size mock-up of a StarSpeeder 3000, the fictional spacecraft which riders are about to board. According to the book Disneyland Detective by Kendra Trahan, the figures of C-3PO and R2-D2 in the Disneyland attraction are actual props from the original film, modified to operate via Audio-Animatronics.
 droid performs repairs on another droid while being distracted by the observing guests, and another droid inadvertently points out all the supposed flaws of the StarSpeeder 3000 and its RX pilots. The G2 droids are in fact the animatronic skeletons of two geese from the defunct Tomorrowland attraction America Sings. A ride attendant escorts guests to one of several loading stations where they wait for their turn to ride.
 pilot Droid droid of the ship, RX-24 or Rex (voiced by Paul Reubens), appears on the side screen and chats to the guests about the trip as R2-D2 is loaded onto the spacecraft.

Rex lowers the cockpit shield, and the hangar crew activates the flight platform. All goes well until a slight mistake on Captain Rexs part sends the Starspeeder crashing into the maintenance bay doors and plummeting into the maintenance yard. They just barely crash into the control room and nearly collide with a giant mechanical arm. Once in space, Rex asks R2-D2 to make the jump to speed of light|lightspeed. However, the ship accidentally passes the Endor moon and instead gets caught inside a comet cluster. The ship gets hit by several comets before getting trapped in one of the larger comets. The Starspeeder weaves its way through the comet and escapes by crashing through one of the walls. Upon escaping the comet, however, the ship encounters a Star Destroyer of the Imperial Remnant.
 Death Star III (later revealed by Leland Chee in 2013 to be a Habitation Sphere disguised as a Death Star in a plot by an Imperial Warlord to distract the New Republic). Rex uses the Starspeeders lasers to eliminate several TIE fighters while a Republic pilot destroys the Death Star in the same manner as Luke Skywalker by firing two proton torpedoes into the exhaust port. The X-Wings jump to lightspeed as the Death Star explodes, and a final lightspeed jump sends the Starspeeder back to the spaceport, nearly colliding with a fuel truck in the hangar and sending a Star Tours employee ducking under his desk. As the cockpit shield raises and cuts off Rex as he apologizes for the near-fatal flight, C-3PO instructs the passengers on the exit procedure and thanks them, oblivious to the perils they just faced. The exit doors opposite the entrance then open and the passengers proceed across another set of bridges into the exit hall towards the Star Trader gift shop.

===Cast===
====English====
*Anthony Daniels - C-3PO    
*Paul Reubens - Captain RX-24, a.k.a. Rex (voice) 
*Brian Cummings - Vid-Screen Announcer (planetary destinations) (voice)
*Stephanie Taylor - Safety Instructor  
*Steve Gawley - cameo as Red Leader (onboard video)   Wicket the Ewok 

====French====
* Anthony Daniels – C-3PO
*   – Captain RX-24, a.k.a. Rex (voice)

Muren, Gawley, and Keeler are all Industrial Light & Magic special effects staff. One year earlier, Reubens had voiced the shipboard computer in the Disney film Flight of the Navigator (credited as Paul Mall), in which his character was named Max. Reubens credits this role with his being cast for the ride.   

===Ride system=== motion base cabins featuring six degrees of freedom, including the ability to move 35 degrees in the Cartesian coordinate system#In three dimensions|X-Y-Z plane. The simulator was patented as Advanced Technology Leisure Application Simulator (ATLAS), originally designed by Rediffusion Simulation  in Sussex, England, now owned by Thomson-CSF. The Rediffusion Leisure simulator was originally developed for a much simpler show in Canada called "Tour of the Universe", where it featured a single entrance/exit door in the rear of the cabin and a video projector. The film is front-projected onto the screen from a 70 mm film projector located beneath the cockpit barrier. The Disneyland original has four simulators, while the shows in Tokyo Disneyland, Disneyland Paris, and WDW Disneys Hollywood Studio each have six motion bases.

 

== Successor ==
 
The successor attraction opened in Disneys Hollywood Studios on May 20, 2011 and at Disneyland on June 3, 2011, replacing the parks original Star Tours attractions. It features an updated ride system, consisting of a new high-definition video, a Dolby 3D 3-D film|high-definition screen, improved motion simulators and several new special effects and Audio-animatronics.

The attraction is set at an earlier point in the Star Wars timeline    (between Revenge of the Sith and A New Hope) and is piloted by C-3PO.   

==Gallery==
  An Audio-animatronic Mon Calamari can be seen in the control booth Queue line showing Star Speeder. Loading Area
File:Star Tours Stormtrooper.jpg|  dressed as a   for Childrens Hospital of Orange County
File:MGM - Star Tours.jpg|Disneys Hollywood Studios entrance
File:StarToursEntrance98 wb.jpg|Disneylands entrance in 1998 after the Tomorrowland makeover
File:StarToursEntrance96 wb.jpg|Disneylands  entrance in 1996 before Tomorrowland makeover Tokyo Disneylands entrance
 

==See also==
 
* List of former Disneyland attractions
* Disneys Hollywood Studios attraction and entertainment history
* List of amusement rides based on film franchises

==References==
 

==External links==
* 
* 
*  
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 