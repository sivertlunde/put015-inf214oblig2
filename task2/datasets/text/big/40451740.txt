W (2014 film)
 
{{Infobox film
| name           = W
| image          = File:WFilmPoster2014.jpg
| caption        = Theatrical poster
| director       = Tarun Madan Chopra
| producer       = Subhash Sehgal  Shivang Sehgal
| writer         = Tarun Madan Chopra
| starring       = Leeza Mangaldas Leslie Tripathy Sonal Giani Raaj Singh Arora Danish Pandor Abhey Jit Attri Meer Ali Gagan Guru Daboo Sardar Malik
| cinematography = Bhavesh Rawal
| editing        = Altaf Shaikh
| studio         = Legacy Film Productions
| distributor    = 
| released       = March 14, 2014
| runtime        = 
| country        = India
| language       = Hindi
| budget         = Rupee|Rs. 2.5 crore    
| gross          = 
| website        =  
}} Hindi film Daboo Sardar Malik.  The movie starred Leeza Mangaldas, Leslie Tripathy, Sonal Giani, Raaj Singh Arora, Abhey Jit Attri and Danish Pandor and released on March 14, 2014. 

==Synopsis==
Sandy (Leeza Mangaldas), Ruhi (Leslie Tripathy), and Manu (Sonal Giani) are three best friends that run W, an event management company that focuses on music events. While traveling to The Freedom Concert, an event that was supposed to be their big break, they come across a group of misogynistic men who dont like how modern and self-sufficient the three women are and end up raping them. Because the three men are of good social standing, the police are particularly unhelpful and the women are traumatized by how they are treated afterwards. This prompts the three women to form the Vengeance Squad in order to take their own revenge against their rapists.

==Cast==
* Leeza Mangaldas as Sandhya Singh a.k.a. Sandy
* Leslie Tripathy as Roohi Malik
* Sonal Giani as Manu
* Raaj Singh Arora as Abhay a.k.a. Bhaiji
* Danish Pandor as Dhruv
* Abhey Jit Attri as Chote
* Meer Ali
* Gagan Guru

==Production and release==
In January 2013 it was confirmed that Leslie Tripathy would be performing in the film in an unspecified role, as would Leeza Mangaldas, Sonal Gyaani, Raj Arora, Daanish Pandor and Abhey Attri.  Filming took place in Delhi. 

==Reception==
The Times of India panned the film, giving it 1 1/2 stars and stating "There is no emotional conflict in the story that provokes you to think or get inspired. Situations are written to convenience the protagonists quest for revenge. And they get it way too easy. The shallow treatment doesnt live up to the topic addressed."  Business Insider criticized the film as missing the "emotional pain and conflict" that similarly themed films such as Adalat o Ekti Meye possessed, commenting that they felt W was a "stereotype revenge drama that   out all too smoothly". 

==Music== Daboo Sardar Malik.  The musical duo of Amaal Mallik and Armaan Malik will be contributing to the soundtrack as singers, as will Neha Bhasin, and Apeksha Dandekar.

===Tracklist===
{{Infobox album  Name = W Type = Soundtrack Artist = Daboo Malik Cover =  Released =  7 March 2014 Recorded =  Genre = Feature film soundtrack Length =   Label = Sony Music Producer = Subhash Sehgal  Shivang Sehgal Last album =  This album =  Next album = 
}}
{{Track listing extra_column = Singer(s) lyrics_credits = yes length1 = 4:55 title1 = Aana Nahi  extra1 =  Amaal Mallik lyrics1 = Daboo Malik length2 = 4:15 title2 = Hum Hain Tum Ho extra2 = Daboo Malik, Apeksha Dandekar lyrics2 =  Daboo Malik length3 = 3:43 title3 = Wild Wild extra3 = Daboo Malik, Amal Mallik, Armaan Malik lyrics3 = Daboo Malik length4 = 5:36 title4 = Tu Hawa extra4 = Armaan Malik lyrics4 = Daboo Malik length5 = 4:25 title5 = Dil Ke Armaan (Womens Day Anthem) extra5 = Daboo Malik, Neha Bhasin lyrics5 = Daboo Malik
}}

==References==
 

== External links ==
*  
*  
*  

 
 
 
 