Lightning Strikes Twice (1951 film)
{{Infobox film
| name           = Lightning Strikes Twice
| image          = Lightning strikes twice poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = King Vidor
| producer       = Henry Blanke
| screenplay     = Lenore J. Coffee
| based on       =  
| starring       = Richard Todd Ruth Roman Mercedes McCambridge
| music          = Max Steiner
| cinematography = Sidney Hickox
| editing        = Thomas Reilly
| studio         = Warner Bros.
| distribution   = Warner Bros.
| released       =   
| runtime        = 91 min.
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Lightning Strikes Twice is a 1951 film drama starring Ruth Roman and Richard Todd. 

==Plot==
Once a rancher, Richard Trevelyan (Richard Todd) is now on a Texas prisons death row. But he wins a new trial, then a complete acquittal when a lone juror holds out.
 Frank Conroy) and Myra Nolan (Kathryn Givney) and ends up borrowing their car. Lost in a storm, she encounters Trevelyan by chance. It turns out he knows J.D. and Myra.

The dude ranch is closed when Shelley gets there. Liza McStringer (Mercedes McCambridge), who runs it with a younger brother nicknamed String (Darryl Hickman), explains that she was the juror who let Trevelyan go free. And now shes being shunned by neighbors and friends.

Shelley bonds with the troubled String, so she is invited to stay a while. She learns that Loraine, the late wife of Trevelyan and murder victim, was a rather wicked woman, loathed by many. There is reason to believe Loraine once had an affair with J.D.

Returning the car, Shelley spends a night with the Nolans and is introduced to Harvey Turner (Zachary Scott), a neighbor who is immediately attracted to her. Harvey, too, speaks ill of the late Loraine and describes himself as lucky to have escaped her clutches.

Shelley again meets Trevelyan and the two cannot resist each other. A jealous and spiteful Liza turns out to be the one who murdered Loraine, and now she nearly does likewise to Shelley before a last-minute rescue. Liza and String flee in their car, but dont get very far.

==Cast==
* Ruth Roman as Shelley Carnes
* Richard Todd as Richard Trevelyan
* Mercedes McCambridge as Liza McStringer
* Zachary Scott as Harvey Turner Frank Conroy as J. D. Nolan
* Kathryn Givney as Myra Nolan Rhys Williams as Father Paul
* Darryl Hickman as String
* Nacho Galindo as Pedro
* Franklin Parker as Guard

==Production==
Virginia Mayo was originally cast in the female lead. 

==Reception==

===Critical response===
Film critic Glenn Erickson discussed the directors film style in his review: The Fountainhead and Ruby Gentry break down into interesting patterns of dynamic visuals, even as their overheated dramatics are impossible to take seriously. 1951s Lightning Strikes Twice forms a link between King Vidor and Douglas Sirks delirious womens pictures. Faced with a gimmicky, far-fetched storyline and inconsistent characters, Vidor still manages to make the movie highly watchable, even enjoyable ... But get ready to smile at the overcooked, sometimes hysterical acting and the big fuss made over a fairly simple mystery ... the picture is a camp hoot from one end to the other.  

==References==
 

==External links==
*  
*  
*  
*  
*   information site and DVD review at DVD Beaver (includes images) 
*  

 

 
 
 
 
 
 
 
 
 
 