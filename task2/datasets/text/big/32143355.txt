Bandhanam
{{Infobox film
| name = Bandhanam
| image =
| caption =
| director = MT Vasudevan Nair
| producer = VBK Menon
| writer = MT Vasudevan Nair
| screenplay = MT Vasudevan Nair Shubha
| music = MB Sreenivasan
| cinematography = K Ramachandrababu
| editing = G Venkittaraman
| studio = Marunadan Movies
| distributor = Marunadan Movies
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, Shubha in lead roles. The film had musical score by MB Sreenivasan.    The film observes a boys return from city living to the village. The films mood is sorrowful: the boy cannot shake himself free of the experience of the city and fails to adapt to village life.

The title of the film literally translates to"bondage". The title is taken from a short story of the same name by MT although the film is not an adaptation the story.

==Cast==
  
*Shobha as Thankam 
*Sukumaran as Unnikrishnan 
*Sankaradi as Achummaan  Shubha as Sarojini 
*Nilambur Balan as Kaaranavar 
*Janardanan  Kunchan 
*Kunjandi as Sankara Menon 
*P. K. Abraham 
*Santhakumari as Office Colleague 
*Thrissur Elsy as Ammini 
*Veeran 
*Vijayalakshmi 
 
 
==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by ONV Kurup. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Kani Kaanenam || Leela Menon || ONV Kurup || 
|- 
| 2 || Raagam Sreeragam || P Jayachandran || ONV Kurup || 
|- 
| 3 || Raagam Sreeragam || Vani Jairam, Leela Menon || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 
 


 