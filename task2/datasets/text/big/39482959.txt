Boulevard (film)
{{Infobox film
| name           = Boulevard
| image          = 
| alt            =
| caption        = 
| director       = Dito Montiel
| producer       = Monica Aguirre Diez Barroso Ryan Belenzon Mia Chang Jeffrey Gelber
| writer         = Douglas Soesbe
| starring       = Robin Williams Bob Odenkirk Kathy Baker Giles Matthey Roberto Aguire J. Karen Thomas
| music          = Jimmy Haun David Wittman
| cinematography = Chung-hoon Chung
| editing        = Jake Pushinsky
| studio         = Camellia Entertainment Evil Media Empire Starz Digital
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =
}} drama film directed by Dito Montiel. The film premiered at the Tribeca Film Festival. 

==Synopsis==
Nolan Mack (Robin Williams) has worked at the same bank for almost 26 years in a life of monotony. He and his wife Joy (Kathy Baker) have embraced their marriage as a convenient distraction from facing reality. However one day, what starts as an aimless drive down an unfamiliar street turns into a life-altering decision for Nolan. When he meets a troubled young man named Leo (Roberto Aguire) on his drive home, Nolan finds himself breaking from the confines of his old life and coming to terms with who he really is.

==Cast==
*Robin Williams as Nolan Mack 
*Bob Odenkirk as Winston 
*Kathy Baker as Joy 
*Giles Matthey as Eddie 
*Roberto Aguire as Leo
*J. Karen Thomas as Cat

==Release== Starz Digital, and will be released in theaters on July 17, 2015.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 