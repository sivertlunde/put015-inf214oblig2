The Don Is Dead
{{Infobox film
| name           = The Don Is Dead
| image          = Poster of the movie The Don Is Dead.jpg
| image_size     =
| caption        =
| director       = Richard Fleischer
| producer       = Paul Nathan Hal B. Wallis
| writer         = Christopher Trumbo     Marvin H. Albert Michael Butler
| narrator       =
| starring       = Anthony Quinn Frederic Forrest Al Lettieri Robert Forster
| music          = Jerry Goldsmith
| cinematography = Richard H. Kline
| editing        = Edward A. Biery
| distributor    = Universal Pictures
| released       =  
| runtime        = 115 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
| amg_id         =
}}
The Don Is Dead is a 1973 crime drama film directed by Richard Fleischer. It stars Anthony Quinn.

==Plot==
Frank is the ambitious son of an organized-crime boss. He plans a heroin deal with the help of brothers Tony and Vince, but a snitch tips off the cops.

After the death of his father, a mob war breaks out between two rival families. One is run by Don Angelo, but he does not get the support of the brothers, Tony and Vince, and must seek power through other means. He begins a romance with Franks young and beautiful fiancee, Ruby, which sends Frank into a self-destructive rage.

==Cast==
* Anthony Quinn as Don Angelo
* Angel Tompkins as Ruby
* Al Lettieri as Vince
* Robert Forster as Frank
* Louis Zorich as Mitch
* Ina Balin as Nella
* Frederic Forrest as Tony
* Charles Cioffi as Orlando

==Reception==
A.H. Weiler of the New York Times was positive: "Expertise, if not imagination, is evident in the explosive, action-oriented direction of Richard Fleischer... The Don Is Dead has the attributes of some lively, pithily accented performances that are adult and effectively natural. Among these are Forrest, as the brainly hood who attempts to escape the racket, but winds up a don, Al Lettieri, as his roughhewn, dependent, ill-fated brother, and Forster, as the rising, vengeful muscleman who is eventually cut down.
As the embattled don who is finally felled by a stroke, not a gun, Quinn is moodily menacing and as polished and relaxed as a professional long familiar with this sort of role." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 