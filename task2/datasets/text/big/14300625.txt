The Tip
 
 
{{Infobox film
| name           = The Tip
| image          = 
| caption        =  Billy Gilbert Gilbert Pratt
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = Harold Lloyd
| music          = 
| cinematography = Walter Lundin
| editing        = Della Mullady
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels 
* W.L. Adams
* William Blaisdell
* Sammy Brooks
* Harry Frick
* Marie Gilbert
* William Gillespie
* Max Hamburger
* Oscar Larson
* Gus Leonard
* Chris Lynton
* M.J. McCarthy
* Belle Mitchell
* Marie Mosquini
* Fred C. Newmeyer
* Lottie Novello
* Evelyn Page
* Hazel Powell
* Dorothy Saulter
* Nina Speight Charles Stevenson
* William Strohbach - (as William Strawback)
* Robert Van Meter
* Dorothea Wolbert

==See also==
* Harold Lloyd filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 