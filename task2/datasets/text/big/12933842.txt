Brother John (film)
{{Infobox film
| name           = Brother John
| image_size     =
| image	         = Brother John FilmPoster.jpeg
| caption        =
| director       = James Goldstone
| producer       = Joel Glickman
| writer         = Ernest Kinoy
| starring       = Sidney Poitier Will Geer Bradford Dillman
| music          = Quincy Jones
| cinematography =
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}
Brother John is a 1971 drama film about an enigmatic African-American man who shows up every time a relative is about to die. In this story, he returns to his Alabama hometown as his sister is dying of cancer.

==Plot==
His arrival into town is clouded by unrest at a factory where workers are seeking to unionize. Local authorities wrongly suspect John to be an outside agitator for the union cause.  The suspicions of the local Sheriff and Doc Thomas son, the District Attorney, grow after they search Johns room and find a passport filled with visa stamps from countries all over the world, even some that few Americans are allowed to travel to. They also find newspaper clippings in a variety of different languages. Only Doc Thomas suspects that Johns real purpose is something else.

After the funeral of Johns sister, he admits to a young woman, Louisa - a teacher at the local elementary school - that his "work" is finished, and that he has a few days to "do nothing" before he must leave. She decides to initiate a relationship with him, in hopes that he will stay. This puts him at odds with a local man who has had his eyes on her since they were in high school. During his time with Louisa, John mentions that one of his friends, now a union organizer, will die soon. After this happens, word of his prediction finds its way to the Sheriff who uses it as an excuse to arrest him. During his questioning we find out more about his travels, but he will never say exactly what his "work" really is. Doc Thomas comes to visit him in jail, and they have a pivotal conversation. John then walks out of jail and leaves town while the Sheriff and his men are preoccupied with the local labor unrest.

Throughout the film there are allusions to Johns true nature in a confrontation with the sheriff, his hesitant relationship with Louisa, his unexplained ability to travel extensively, his apparent facility with multiple languages, his caring aloofness, etc.

==Cast==
*Sidney Poitier as John Kane
*Will Geer as Doc Thomas
*Bradford Dillman as Lloyd Thomas
*Beverly Todd as Louisa MacGill
*Ramon Bieri as Orly Ball
*Warren J. Kemmerling as George
*Lincoln Kilpatrick as Charley Gray
*P. Jay Sidney as Reverend MacGill Richard Ward as Frank
*Paul Winfield as Henry Birkart
*Zara Cully as Miss Nettie Michael Bell as Cleve
*Howard Rice as Jimmy
*Darlene Rice as Marsha
*Harry Davis as Turnkey Lynn Hamilton as Sarah

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 

 