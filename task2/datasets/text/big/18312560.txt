War Made Easy: How Presidents & Pundits Keep Spinning Us to Death
{{Infobox film
| name           = War Made Easy: How Presidents & Pundits Keep Spinning Us to Death
| image          = War-made-easy (poster).jpg
| image_size     = 
| caption        = 
| director       = Loretta Alper, Jeremy Earp
| producer       = Loretta Alper
| writer         = 
| narrator       = Sean Penn
| starring       = 
| music          = Leigh Phillips, John Van Eps
| cinematography = 
| editing        = Andrew Killoy
| distributor    = 
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         =
}}
War Made Easy: How Presidents & Pundits Keep Spinning Us to Death is a 2007 American documentary film. The film is narrated by Sean Penn and is adapted from the book of the same name,    authored by Norman Solomon.

==Plot==
The film attempts to expose how the American government over 50 years has tried to strum up war effort using the media as a tool. "War Made Easy gives special attention to parallels between the Vietnam war and the war in Iraq." 

==Reception== New York Times columnist Jeannette Catsoulis described War Made Easy as "cinematically inert if ultimately persuasive".  V.A. Musetto of the New York Post criticized the film as "conventional and one-sided".  Aaron Hillis wrote for The Village Voice that the film is "sobering, straightforward, and a bit drab, but... its also an entirely nonpartisan endeavor".  Variety (magazine)|Variety critic Dennis Harvey credited "Solomon’s astute onscreen analysis" for driving the film. 

Rotten Tomatoes rates War Made Easy 88% on the basis of 17 aggregated reviews,  and Metacritic rates the film 57 based on 5 reviews. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 