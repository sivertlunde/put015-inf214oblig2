The Perfect Holiday
{{Infobox Film
| name           = The Perfect Holiday
| image          = The Perfect_Holiday_Poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Lance Rivera Mike Elliott Petra Hoebel (line) Queen Latifah Shakim Compere Stevie "Black" Lockett (associate)
| writer         = Jeff Stein Lance Rivera Marc Calixte Nat Mauldin
| narrator       = Queen Latifah
| starring       = Morris Chestnut Gabrielle Union Faizon Love Terrence Howard Queen Latifah
| music          = Christopher Lennertz
| cinematography = Teodoro Maniaci
| editing        = Paul Trejo
| studio         = Destination Films Capital Arts Entertainment Truenorth Productions Flavor Unit Entertainment
| distributor    = Yari Film Group Freestyle Releasing 
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| gross          = $5,812,781 
}} Family and BET.

Tagline: This Christmas, the perfect man just happens to be Santa.

==Plot== Charles Q. Murphy). Nancy (Union) is a divorced mother, who is too busy taking care of her three children to take care of herself.  Her daughter Emily (Khail Bryant) overhears her mother say that she wished for a compliment from a man, and the daughter tells the local malls Santa Claus about her mothers wish.  

The Santa Claus turns out to be Benjamin, who notices Nancy.  Later, while sitting in a Starbucks after his shift as Santa, Benjamin and his friend Jamal (Faizon Love) see Nancy go into a dry cleaners. Benjamin borrows Jamals jacket, pretends to drop it off at the cleaners, tells Nancy that shes a very attractive woman (granting her wish), and leaves. Eventually, the two start to date and end up falling in love—without Ben realizing that Nancys ex-husband is J-Jizzy. 

Things take a turn for the worse, however, because Nancys oldest son, John-John (Malik Hammond) is jealous of Benjamin going out with his mother and plots to break up the relationship. What follows is a series of funny and touching scenes that show viewers what "family" is really about. 

Queen Latifah and Terrence Howard play omniscient roles in the movie. Howard is a mischievous and sly angel named "Bah Humbug", while Latifah is the kind, thoughtful angel, called "Mrs. Christmas".

==Cast==
*Gabrielle Union - Nancy Taylor
*Morris Chestnut - Benjamin Armstrong
*Charlie Murphy - J-Jizzy
*Faizon Love - Jamal
*Jeremy Gumbs - Mikey
*Jill Marie Jones - Robin
*Katt Williams - Delicious
*Khail Bryant - Emily
*Malik Hammond - John-John
*Queen Latifah - Mrs. Christmas
*Rachel True - Brenda
*Terrence Howard - Bah Humbug

==Reception==
The film was neither a critical nor commercial success. Rotten Tomatoes reported that 16% of critics gave positive reviews based on 54 reviews. It has a consensus stating The Perfect Holiday is the perfect example of Christmas movie clichés run amok.  Metacritic gave the film a 35/100 approval rating based on 22 reviews classifying the film has "generally negative reviews".  On its opening weekend, it opened poorly at #6 with $2.2 million. 
  The film grossed $5.8 million domestically.

==References==
 
  tags!-->

==External links==
*  
*  
*  
*  
*  
*  
*   at Mosaec.com

 
 
 
 
 
 
 
 