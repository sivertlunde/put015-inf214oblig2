His Hour
 
{{Infobox film
| name           = His Hour
| image          = 
| caption        =
| director       = King Vidor
| producer       = Irving Thalberg
| writer         = Maude Fulton (intertitles) Elinor Glyn King Vidor (intertitles)
| starring       = Aileen Pringle
| cinematography = John J. Mescall
| editing        =
| distributor    = Metro Goldwyn Mayer (as Metro Goldwyn)
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         = $197,000 H. Mark Glancy, MGM Film Grosses, 1924-28: The Eddie Mannix Ledger, Historical Journal of Film, Radio and Television, Vol 12 No. 2 1992 p127-144 at p129 
| gross = $595,000 
}} silent drama Three Weeks, written by Elinor Glyn, and starring Aileen Pringle, one of the biggest moneymakers at the time of the amalgamation. 

==Plot==
Gritzko (John Gilbert) is a Russian nobleman and Tamara (Aileen Pringle), is the object of his desire.

==Cast==
* Aileen Pringle - Tamara Loraine John Gilbert - Gritzko
* Emily Fitzroy - Princess Ardacheff
* Lawrence Grant - Stephen Strong Dale Fuller - Olga Gleboff
* Mario Carillo - Count Valonne
* Jacqueline Gadsden - Tatiane Shebanoff (as Jacquelin Gadsdon)
* George Waggner - Sasha Basmanoff (as George Waggoner)
* Carrie Clark Ward - Prinncess Murieska
* Bertram Grassby - Boris Varishkine
* Jill Reties - Sonia Zaieskine
* Wilfred Gough - Lord Courtney
* Frederick Vroom - English Minister
* Mathilde Comont - Fat Harem Lady
* E. Eliazaroff - Khedive
* David Mir - Serge Greskoff
* Bert Sprotte - Ivan
* George Beranger - (as Andre Beranger)
* Mike Mitchell
* Jack Parker - Child (uncredited)
* Thais Valdemar - (uncredited)

==Production==
A former officer of the Russian Imperial Army, by now living in Los Angeles, served as a technical advisor on the film. His actual name has not been confirmed, however the studio press releases referred to him as Mike Mitchell. This film marked the first of four times that John Gilbert and King Vidor would work together. Despite showcasing his riding ability and appearance, Gilbert hated the script and felt it gave him nothing substantial to do as an actor. 

==Reception==
The film made a profit of $159,000. 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 