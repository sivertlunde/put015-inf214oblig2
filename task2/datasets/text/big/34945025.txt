Asari-chan
{{Infobox animanga/Header
| name            = Asari-chan
| image           = Asari-chan.jpg
| caption         = Cover of the first volume of Asari-chan as published by Shogakukan
| ja_kanji        = あさりちゃん
| ja_romaji       = 
| genre           = Comedy<!-- Genres should be based on what reliable sources list them as and not on personal interpretations. 
                         Limit of the three most relevant genres in accordance with  . -->
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Mayumi Muroyama
| illustrator     = 
| publisher       = Shogakukan
| publisher_en    = 
| demographic     = Shōjo manga|Shōjo
| imprint         = 
| magazine        = 
| first           = 1978
| last            = 2014
| volumes         = 100
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = tv series
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = Toei Animation
| licensee        = 
| network         = TV Asahi
| network_en      = 
| first           = January 25, 1982
| last            = February 28, 1983
| episodes        = 54
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = film
| title           = Asari-chan Ai no Marchen Shōjo
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensee        = 
| released        =  
| first           = 
| last            = 
| runtime         = 25 minutes
| films           = 
| film_list       = 
}}
 
  is a Japanese Shōjo manga|shōjo slice of life manga series by Mayumi Muroyama. It was adapted into an anime television series and an anime film.  The TV series was produced by Toei Animation a subsidiary of Toei Company, and directed by Kazumi Fukushima. The anime follows Asari, a normal but stupid elementary school fourth-grade girl who does not get along with her family.

==Manga==
The manga was written by Mayumi Muroyama and serialized from the August 1978 issue to the March 2014 issue of Shogakukans Shogaku Ninensei magazine. During its serialization it was also published in several other Shogakukan magazines, including CoroCoro Comic, Pyonpyon, Ciao (magazine)|Ciao, Shogaku Ichinensei, Shogaku Sannensei, Shogaku Yonnensei, and Shogaku Gonensei.

==Anime==
The anime was produced by  , with 54 episodes.

==Reception==
In 1985, Asari-chan won the 30th Shogakukan Manga Award in the category Best Childrens Manga.  In 2014, it won the Judging Committee Special Award at the 59th Shogakukan Manga Award.  The manga won the grand award at the 2014 Japan Cartoonists Association Award.  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 