Ocean of Pearls
     | runtime = 97 minutes | country = U.S. | language = English, Punjabi dialogue | budget = $1.5 million  | gross = $7,523 in 1st weekend  | preceded_by = | followed_by = }}

Ocean of Pearls is a movie released in 2008 in film|2008.  It is the first film directed by Sarab Singh Neelam, a Sikh gastroenterologist from Troy, Michigan.       It was written by Neelam and Veerendra Prasad.

==Plot==
Dr. Amrit Singh, A young Sikh surgeon, moves from Toronto to Detroit to take a position at a new transplant facility, leaving behind his family and Indian girlfriend. The film follows Amrits struggles against the pressures to assimilate, including considering removing his turban and cutting his hair, racial discrimination, an unfair medical system in which uninsured patients cannot receive transplants, and romantic temptation in the shape of an attractive colleague.     The film is semi-autobiographical, and reflects the experience of Sikhs in America post-9/11. 

==Awards and screenings==
Ocean of Pearls won Best Feature Film at the 2009 Detroit Windsor International Film Festival, the Grand Jury Prize and Audience Choice Award at the 2008 Los Angeles Asian Pacific Film Festival, and the Audience Choice Award at the 2008 Toronto ReelWorld Film Festival.    

The film showed at the 10th International Film Festival, Mumbai, in March 2008.  and the Cannes Film Festival by iDream Independent Pictures.  

==Cast and crew==
Director Sarab Neelam lived in India until his family moved to Toronto when he was aged 10. He spent ten years learning filmmaking while working as a hospital doctor before making the film, taking advice from Kurt Luedtke who directed Out Of Africa.  

Omid Abtahi, previously in Over There stars as Amrit, Navi Rawat from The O.C. and Numb3rs plays Amrits girlfriend, Ajay Mehta of Americanizing Shelley plays his father, Ron Canada plays the Detroit doctor who tempts Amrit to his new post, Heather McComb plays his attractive colleague, Todd Babcock plays a Senators son vying for the same post as Amrit, and Brenda Strong of Desperate Housewives plays a transplant patient.     Local members of the Sikh community appeared as extras and offered their gurdwara for production and sets. 
 Renaissance Man, was an executive producer and assisted with the script. Burnstein first met Neelam in the late 1980s, when he pitched filming a Middle Eastern epic, but Burnstein was interested in Neelams own life story. He advised Neelam to change the main character from a Caucasian doctor to a Sikh.      Jeff Dowd was another executive producer. 

==Reception==
The San Francisco Chronicle said "it sounds heavy-handed, and it is. Although the film looks professional, it feels like a soap opera with a message." 

Asia Pacific Arts included the film in its top ten films of 2008, saying that "its domestic and romantic melodrama feels true and never sensational, while its passion to reveal social injustice is never didactic." 

The Detroit News and the Detroit Metro Times both gave the film a C+ rating, Tom Long concluding that "as a vehicle for demystifying the Sikh life while again highlighting the confusion of the modern world, "Ocean of Pearls" works well indeed."   

Variety magazine|Variety argued that the "mix of tepid hospital intrigue plus underdeveloped cultural/relationship conflicts feels like a routine TV episode stretched to feature length, with little dramatic urgency or cinematic style to render its good intentions compelling."   

SF Weekly said "Mishandled, this film would be very funny in a bad way, but Neelam skirts the very real perils of risibility." 

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 