American Friends
 
{{Infobox film
| name           = American Friends
| image          = American Friends (cover).jpg
| caption        =
| director       = Tristram Powell Steve Abbott   Patrick Cassavetti
| writer         = Michael Palin   Tristram Powell
| starring       = Michael Palin   Connie Booth   Trini Alvarado
| music          = Georges Delerue
| cinematography = Philip Bonham-Carter
| editing        = George Akers
| studio  = Prominent Features British Screen Productions BBC
| distributor    = Palace Pictures (UK) Castle Hill Productions (US)
| released       =   
| runtime        = 95 minutes
| country        = United Kingdom
| awards         = Writers Guild of Great Britain Award - Best film/screenplay
| language       = English
| budget         =
| gross          =  $23,034 
}}

American Friends is a 1991 British film starring Michael Palin. It was written by Palin and its director, Tristram Powell.

==Plot== Oxford professor ward Elinor (Trini Alvarado). Ashby is drawn to them both, particularly Elinor, but is rather surprised when they arrive in Oxford and rent a house. Women are not allowed in the College, nor are Fellows allowed to marry, which puts him in an embarrassing situation. Ashbys rival for the post of College President, Oliver Syme (Alfred Molina), takes full advantage of this to try to discredit Ashby.

==Inspiration==
The plot was based on a real-life incident involving Palins great-grandfather, Edward Palin.

==Cast==
 
* Michael Palin - Rev. Francis Ashby
* Trini Alvarado - Elinor Hartley
* Connie Booth - Caroline Hartley
* Alfred Molina - Oliver Syme
* Bryan Pringle - Haskell Fred Pearson - Hapgood
* Susan Denaker - Mrs. Cantrell
* Jonathan Firth - Cable
* Ian Dunn - Gowers
* Robert Eddison - William Granger Rushden David Calder - Pollitt Simon Jones - Anderson
* Charles McKeown - Maynard
* Roger Lloyd-Pack  -  Dr. Butler John Nettleton - Rev. Groves Alun Armstrong - Dr. Victor Weeks
* Sheila Reid - Mrs. Weeks
* Edward Rawle-Hicks - John Weeks
* Markus Gehrig - Swiss Guide
* Jo Stone-Fewings - Undergraduate
* Jimmy Jewel - Ashby Senior
* Wensley Pithey - Cave
* Arthur Howard - Voe
* Charles Simon - Canon Harper
* Adrian Gannon - Extra
 

==Awards==
The film won the Writers Guild of Great Britain|Writers Guild of Great Britain Award for Best film/screenplay.

==Discography==

The CD soundtrack composed by Georges Delerue is available on Music Box Records label ( ).

==Reference list==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 
 