Breaking the Ice (1938 film)
{{Infobox film
| name           = Breaking the Ice
| image          =
| caption        =
| director       = Edward F. Cline
| producer       = Sol Lesser Mary C. McCall Jr. (writer) Brewster Morse (story) Bernard Schubert (writer) Manuel Seff (writer)
| narrator       =
| starring       = Bobby Breen Charles Ruggles Dolores Costello Irene Dare
| music          =
| cinematography = Jack MacKenzie
| editing        = Arthur Hilton
| distributor    =
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Breaking the Ice is a 1938 American film directed by Edward F. Cline. A young Mennonite boy runs away from home to earn money for his widowed mother.               

==Plot==
Breaking The Ice begins while Tommy Martin ( Bobby Breen ) and his mother, Martha Martin (Dolores Costello) say goodbye to Henry and Reuben Johnson ( John Dusty King and Delmar Watson ). After having stopped by the Mennonite farm, where Tommy and Martha stay with the William and Annie Decker ( Robert Barrat and Dorothy Peterson ), the Johnsons are headed back to their hometown of Goshen. That night over dinner, Tommy, Martha and the Decker’s discuss the possibility of Tommy and Martha returning to their home in Kansas to work their farm, having fully recovered from the death of Mr. Marti. William Decker does not wish to allow them to return without the help of a man on the farm. Tommy suggests that Henry Johnson would be happy to assist. Mrs. Decker agrees that Henry had displayed affections for Martha while he was at the farm. It is decided that in order to get both Tommy and Martha home, $92 would be needed in order to pay the train fare home. To Martha’s dismay, William writes Henry for the $92 if he wishes to marry Martha and send her home. Henry is to respond with a letter detailing his answer. William gives the letter to Tommy, who is to mail it when he is through with his chores. Instead, Tommy tears the letter up and does not send it.    

Enter Samuel Terwilliger ( Charles Ruggles ), a street-smart, money-making man. Tommy tells Mr. Terwilliger that he has saved old newspapers for him to buy. As Tommy runs in the house to get the papers, a man comes by to drop off money for William Decker for a tobacco sale. Tommy takes the money and sets it on William’s desk. Next to the desk is the trunk where Tommy kept the newspapers. While Tommy was rummaging through the trunk, one of the twenty dollar bills on the desk landed in a newspaper, to be taken to Mr. Terwilliger. As Tommy and Mr. Terwilliger get to talking Tommy learns about Philadelphia, Mr. Terwilliger’s home town. That evening, Tommy decides to go to Philadelphia with Mr. Terwilliger, so he sneaks out of the house and hops in Mr. Terwilliger’s wagon.

Mr. Terwilliger and Tommy travel along the next day, and after a close-call with a train, make it to Philadelphia. Soon Tommy gets a job at the adjacent ice rink, scraping the ice after the performers practice. Back at home, William has discovered the missing twenty dollars, and blames Tommy for taking it. Every week, Tommy sends a letter home with a dollar, but William remains angry.  One day as Tommy is scraping the ice, he sings along to a song the band is playing. The manager of the rink offers to hire him on as a singer for the shows. Mr. Terwilliger acts as Tommy’s manager, and negotiates a wage. Later Tommy finds out that Mr.Terwilliger had been secretly keeping Tommy’s money. Tommy uses the stashed money to return home.

Upon his return home, he is greeted with joy from everyone except for William, who insists on the return of his twenty dollars. Tommy traces the money back in his mind, and realizes that it fell into one of the newspapers Mr. Terwilliger used to stuff an antique chair. The two of them then run around the country in order to seek the chair, and find the twenty dollars.  Finally, they locate the chair, and bring the twenty dollars back home. With all debts cleared, Tommy and Martha can finally go home to Goshen where they run the farm with Henry and Reuben Johnson.

==Cast==
*Bobby Breen as Tommy Martin
*Charles Ruggles as Samuel Terwilliger
*Dolores Costello as Martha Martin
*Irene Dare as Irene Dare
*Robert Barrat as William Decker
*Dorothy Peterson as Annie Decker
*John Dusty King as Henry Johnson
*Billy Gilbert as Mr. Small Margaret Hamilton as Mrs. Small Charles Murray as Janitor
*Jonathan Hale as Kane
*Delmar Watson as Reuben Johnson
*Spencer Charters as Farmer Smith
*Cy Kendall as Judd

==Soundtrack==
*"Happy as a Lark"
*"Put Your Heart in a Song"
*"The Sunny Side of Things" by Frank Churchill and Paul F. Webster 
*Tellin My Troubles to a Mule
*Goodbye, My Dreams, Goodbye (Arrangement of Shuberts Serenade) by Victor Young and Paul F. Webster

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 