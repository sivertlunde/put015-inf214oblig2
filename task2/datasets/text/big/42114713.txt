Batman: Assault on Arkham
 
{{Infobox film name        = Batman: Assault on Arkham image       = Batman Assault on Arkham cover.jpeg caption     = Home video release cover art director    = {{plainlist|
*Jay Oliva
*Ethan Spaulding
}} producer    = James Tucker writer      = Heath Corson based on    =   by Rocksteady Studios DC Entertainment starring    = {{plainlist|
*Kevin Conroy
*Neal McDonough
*Hynden Walch
*Matthew Gray Gubler
}} music       = Robert J. Kral editing     = Christopher D. Lozinski studio      = {{plainlist|
*Warner Bros. Animation
*DC Entertainment
}} distributor = Warner Home Video released    =   runtime     = 76 minutes country     = United States language    = English budget      = gross       = $4,571,025 
}}
Batman: Assault on Arkham is a 2014 direct-to-video animated superhero film that is part of the DC Universe Animated Original Movies.  Jay Oliva and Ethan Spaulding direct, with a script by Heath Corson, and James Tucker producing.  The film was screened at the 2014 San Diego Comic-Con International on July 25, 2014, and it was released digitally on July 29 and on physical media on August 12. 
 Harley Quinn/Dr. The Batman) Martin Jarvis reprise their roles of Joker (comics)|Joker, Amanda Waller, Penguin (comics)|Penguin/Oswald Cobblepot and Alfred Pennyworth from Arkham Origins (with North and Jarvis also appearing in Arkham City), while Jennifer Hale reprises her role of Killer Frost from the DCAU and other properties.

==Plot==
Batman rescues the Riddler from a black ops assassination ordered by Amanda Waller, returning him to Arkham Asylum. Invoking Priority Ultraviolet, Waller captures criminals Black Spider, Captain Boomerang, Deadshot, Harley Quinn, KGBeast, Killer Frost, and King Shark, forming the Suicide Squad. Their mission is to break into Arkham Asylum and recover a thumbdrive in the Riddlers cane. While in Wallers employ, Riddler copied information on the squad, and is planning to make it public. She forces them to comply by threatening to detonate nano-bombs implanted in their necks. KGBeast, who believes the bombs are a bluff, walks out on Waller and is killed as an example to the others.
 Penguin has Joker hid a dirty bomb. Batman has been ransacking the city in search of it, even resorting to interrogating Riddler for answers. However, Harley denies knowledge of the bombs location, and Batman has her sent back to Arkham.

At Arkham, Jokers taunting of Harley causes her to attempt shooting him. This distraction allows the Suicide Squad to successfully infiltrate Arkham in a variety of disguises, and proceed smoothly with their mission by playing the previous days footage. Although some problems and fights arise, the team gets by with relative ease and finds the cane in the storage room as well as Harleys mallet. Batman arrives and fights each member of the squad, which ends in a duel with Black Spider. Batman defeats Black Spider and switches costumes with him, joining the Suicide Squad without their knowledge. Meanwhile, Deadshot learns that the cane contains no data and that Killer Frost is missing. Killer Frost locates Riddler, but decides not to kill him when she learns that he knows how to defuse the bombs implanted in their necks. Though angry at Harley, Joker realizes her assault weakened his cell, and he escapes.

The Suicide Squad agree to spare Riddler in exchange for his help, and most are able to deactivate their bombs with the use of a device intended for electroshock therapy. Black Spider, who wasnt with the group due to Batman, and King Shark, whose skin was too thick, however, are gruesomely executed by Waller when she learns what the Squad has done. As "Black Spider" is still alive, Riddler deduces him as Batman, and tries to shoot him. The four remaining members of the Squad try to escape, before Harley reunites with an angered and abusive Joker. Joker then takes Harley and frees all of the asylum inmates. Joker reveals that his dirty bomb was hidden within Harleys mallet, which was stored at Arkham, and that he will detonate it in the city. Chaos spreads across Arkham Island as police sortie with super-criminals, and teamwork in the Suicide Squad collapses as Killer Frost and Captain Boomerang abandon Deadshot.
 Bane destroys the police car she was trying to escape in). Deadshot travels to Gotham City by stolen helicopter (which he fought with Captain Boomerang over), but discovers Joker and Harley were stowing away with him. As Deadshot battles Joker in the helicopter, Harley is unable to take a side, and instead, she accidentally steers the helicopter into a skyscraper. In the aftermath, Deadshot finds himself alone with the Joker, while Batman chases after Harley. Batman easily defeats Harley and defuses the bomb hidden in her mallet, saving the city. A heavily wounded Deadshot is likewise able to defeat the Joker and pins him in the helicopter, which tips over and out of the building. It is later mentioned that the police never find his body. 

An epilogue shows Batman confronting Amanda Waller over the Suicide Squad program, which caused collateral damage in Arkham and cost the lives of many, all because Waller wanted the Riddler dead. Waller assures him that she and her agents will capture him next time, but Batman claims there will be no next time and warns her to avoid such action in the future. The much too arrogant Waller, however, dismisses his advice. After Batman leaves, Deadshot—now a free man and reunited with his daughter—is shown about to assassinate Waller with a sniper rifle. The film ends before he pulls the trigger.

==Cast==
  Batman / Bruce Wayne  Deadshot / Floyd Lawton  Harley Quinn / Dr. Harleen Quinzel  Riddler / Edward Nygma  Joker 
*C. C. H. Pounder|C.C.H. Pounder - Amanda Waller  Greg Ellis Captain Boomerang / George Harkness  Black Spider / Eric Needham  King Shark / Nanaue  Killer Frost / Louise Lincoln  KGBeast / Oswald Cobblepot / Penguin 
*Eric Bauza - Security Guy Chris Cox Commissioner James Gordon Martin Jarvis - Alfred Pennyworth
*Peter Jessop - Watch Commander Scarecrow / Zsasz / Victor Zsasz
*Andrea Romano - Woman
*Travis Willingham - Morgue Guy
*Mick Wingert - Joker Security Guy
 
 Poison Ivy.

==Soundtrack==
{{Infobox album
| Name = Batman: Assault on Arkham (Music from the DC Universe Movie)
| Type = Film score
| Artist = Robert J. Kral
| Longtype=
| Cover = 
| Released = July 30, 2014
| Length =51:21
| Label = WaterTower Music
| Reviews =
}}

The soundtrack to Batman: Assault on Arkham was released on July 30, 2014, with music composed by Robert J. Kral. 
;Tracklist
{{Track listing
| collapsed       = yes
| headline        = Batman: Assault on Arkham (Music from the DC Universe Movie)
| extra_column    = 
| total_length    = 51:21

| all_writing     = 
| all_lyrics      = 
| all_music       =

| writing_credits = 
| lyrics_credits  = 
| music_credits   =

| title1          = Nigmas Confrontation / Its Batman 
| note1           = 
| writer1         = 
| lyrics1         = 
| music1          = 
| extra1          = 
| length1         = 3:10

| title2          = Criminal Montage 
| note2           = 
| writer2         = 
| lyrics2         = 
| music2          = 
| extra2          = 
| length2         = 3:06

| title3          = Task Force Indoctrination 
| note3           = 
| writer3         = 
| lyrics3         = 
| music3          = 
| extra3          = 
| length3         = 2:41

| title4          = Dropping Down 
| note4           = 
| writer4         = 
| lyrics4         = 
| music4          = 
| extra4          = 
| length4         = 2:00

| title5          = Gearing Up / Beer Room Challenge 
| note5           = 
| writer5         = 
| lyrics5         = 
| music5          = 
| extra5          = 
| length5         = 1:51

| title6          = Harley Arrested to Arkham 
| note6           = 
| writer6         = 
| lyrics6         = 
| music6          = 
| extra6          = 
| length6         = 3:01

| title7          = Infiltrating Arkham & Joker Assault 
| note7           = 
| writer7         = 
| lyrics7         = 
| music7          = 
| extra7          = 
| length7         = 5:33

| title8          = Killer Frosts Kiss & Black Spiders Microwave 
| note8           = 
| writer8         = 
| lyrics8         = 
| music8          = 
| extra8          = 
| length8         = 1:59

| title9          = Harley Bait & King Sharks Work 
| note9           = 
| writer9         = 
| lyrics9         = 
| music9          = 
| extra9          = 
| length9         = 1:27

| title10         = Suicide Squad in the Big House 
| note10          = 
| writer10        = 
| lyrics10        = 
| music10         = 
| extra10         = 
| length10        = 3:44

| title11         = Batmans Gotham & Property Room Access 
| note11          = 
| writer11        = 
| lyrics11        = 
| music11         = 
| extra11         = 
| length11        = 2:59

| title12         = Batman Fights Suicide Squad 
| note12          = 
| writer12        = 
| lyrics12        = 
| music12         = 
| extra12         = 
| length12        = 2:12

| title13         = Jokers Out / Suicide Squad vs. SCU 
| note13          = 
| writer13        = 
| lyrics13        = 
| music13         = 
| extra13         = 
| length13        = 3:34

| title14         = Joker Attacks Batman 
| note14          = 
| writer14        = 
| lyrics14        = 
| music14         = 
| extra14         = 
| length14        = 1:40

| title15         = Prisoners Released 
| note15          = 
| writer15        = 
| lyrics15        = 
| music15         = 
| extra15         = 
| length15        = 2:45

| title16         = Chopper Fight / Poison Ivy / The Batplane 
| note16          = 
| writer16        = 
| lyrics16        = 
| music16         = 
| extra16         = 
| length16        = 2:03

| title17         = Chopper Crash 
| note17          = 
| writer17        = 
| lyrics17        = 
| music17         = 
| extra17         = 
| length17        = 2:24

| title18         = Final Confrontations 
| note18          = 
| writer18        = 
| lyrics18        = 
| music18         = 
| extra18         = 
| length18        = 2:08

| title19         = Batman - Assault On Arkham End Credits 
| note19          = 
| writer19        = 
| lyrics19        = 
| music19         = 
| extra19         = 
| length19        = 3:04
}}

==Reception==
Scott Mendelson of Forbes roundly praised Assault on Arkham for its action, artstyle, humor, voices, and characters, calling it one of the best films of DCs direct-to-video lineup. He described it as a "gleefully immoral" heist film which, having "no real   arc to speak of", relies on its violent action and clever character dynamics within the Suicide Squad to carry it. Due to its villainous protagonists, dark comedy, and sexual content, Mendelson considers the production of Assault on Arkham an experimental decision by DC, and a successful experiment as it tells a style of comic book story that would never get approval as a live-action project. 

Seth Robison of  ". 

==References==
{{Reflist||refs=

   

   

   

   

   

   

   

   

   

  
 
}}

==External links==
* 

 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 