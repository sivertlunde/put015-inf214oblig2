Vampire Clan
{{Infobox film
| name        = Vampire Clan
| image       = Vampire_clan.jpg
| starring    = Drew Fuller Alexandra Breckenridge Timothy Lee DePriest Marina Black Kelly Kruger Richard Gilliland Larry Dirk Stacy Hogue Spencer Redford
| writer      = Aaron Pope
| director    = John Webb
| producer    = Elie Cohn Keith Walley John Langley (executive producer)
| music       = Guy Harrington
| editing     =
| distributor =
| released    =  
| runtime     = 87 min.
| country     = United States
| language    = English
| budget      =
}}
  Horror movie, released in 2002, directed by John Webb.  The film received its premiere screening at the 2002 Dances With Films Festival. 

==Plot==
Based on the horrific true story of the 1996 Vampire Killings in Florida,  the film follows the police investigation of five Goth teenagers who claimed to be real-life vampires. They drank each others blood and embraced the occult. But they were also ordinary, middle-class kids looking for an outlet for their angst and morbid curiosity. Somewhere along their road trip to New Orleans, their fantasy life became all too real. Now the police have two savagely beaten corpses on their hands... parents of the teenaged vampires. What really happened? And how did these normal kids become such monsters? 
 
==Cast== Roderick Rod Justin Farrell
* Alexandra Breckenridge ....  Charity Lynn Keesee
* Timothy Lee DePriest ....  Howard Scott Anderson
* Marina Black ....  Dana Lynn Cooper
* Kelly Kruger ....  Heather Ann Wendorf
* Richard Gilliland ....  Sgt. Ben Odom
* Larry Dirk ....  Sheriff Mike Dane
* Stacy Hogue ....  Jeni Wendorf
* Spencer Redford ....  Jeanine Leclair

==Reception==
The film received mixed reviews and holds a 66% fresh rating on Rotten Tomatoes. 

==References==
 

==External links==
*  
*  

 
 
 
 


 