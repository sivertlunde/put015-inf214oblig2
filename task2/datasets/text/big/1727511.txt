Tower of Terror (film)
 
{{Infobox film
| name = Tower of Terror
| image= Tower of Terror VideoCover.png
| image_size =
| caption = Tower of terror video cover
| director = D. J. MacHale
| producer = Iain Paterson
| writer = D. J. MacHale
| based on = Disneys The Twilight Zone Tower of Terror
| narrator = John Franklin Michael McShane
| music = Louis Febre
| cinematography = Stephen McNutt
| editing = Barry Zetlin
| studio = Zaloom/Mayfield Productions   Walt Disney Television ABC
| released =  
| runtime = 89 minutes
| country = United States English
| budget =
}} thriller film directed by D. J. MacHale. It is based on the theme park attraction, The Twilight Zone Tower of Terror, at Disneys Hollywood Studios at the Walt Disney World Resort in Lake Buena Vista, Florida and was originally a presentation of The Wonderful World of Disney. It is also Disneys first film based on an attraction at Walt Disney Parks and Resorts | one of its theme parks, though its also the only adaptation to be made for television rather than being made as a theatrical film. 

Unlike the theme park ride, the film has no connection to any incarnation of The Twilight Zone.

Much of the film was shot at the actual attraction at Disneys Hollywood Studios. The rest was filmed on a closed stage in Hollywood, California. 

==Plot==
The film begins on Halloween night 1939, revolving around the fate of five people - singer Carolyn Crosson, her boyfriend Gilbert London, child actress Sally Shine (who is modeled after child actress Shirley Temple), The way Sally dresses in the film and in the ride, as well as her hair style, suggests of an allusion to Shirley Temple. Also it is implied in one scene that Sally also had a line of dolls based on her likeness, a parallel to the real life Shirley having a doll line of her own.   her nanny Emeline Partridge, and bellhop Dewey Todd. They were invited to be at the Tip Top Party located on the hotels twelfth floor. The elevator suddenly got stuck at the eleventh floor, then at exactly 8:05 pm, lightning strikes the building which causes the elevator to collapse, and the five people mysteriously vanish.
 spirits in the earthly realm inside the hotel. Abigail says she can reverse the spell if the elevator is repaired and the team finds something that belonged to each of the hotel guests, then repeat the guests actions in the elevator on Halloween. This will free their spirits from the hotel. They then enlist the help of Chris "Q" Todd, a car mechanic and Deweys grandson, who, despite being initially reluctant, volunteers to help his deceased grandfather and the four other guests.

The team realize that Abigail was the one responsible for the disappearance of the hotel guests on the elevator, including her younger sister Sally, born Sally Gregory,  out of personal vendetta and jealousy against her sisters booming career. The final straw being the party, to which she wasnt invited, having been set on her birthday, which no one remembered . Buzzy then realizes that what they did actually gave Abigail the means to complete her spell. The team then rushes back to the hotel, but they are too late.

Meanwhile, the ghosts board the elevator. Anna rushes in as well, trying to keep them from boarding. Sally manages to run out of the elevator, joining the living, but Anna gets trapped as the passenger elevator moves up. They then confront Abigail, who then tearfully admits her wrongdoing. Abigail Gregory: "I.... I couldnt sing. And I couldnt dance."  Meanwhile, the elevator continues to move up, only to once again get stuck on the eleventh floor, with only minutes left before history repeats itself. Sally, wondering what the commotion was about, joins the group, and Abigail gets frightened. When asked by Buzzy what would she say to Abigail, Sally says that the whole party was meant to be a surprise birthday for her older sister, and apologizes for not being able to get to the party. Sally even kept the present she wanted to give to Abby, a bracelet with their names on it, but couldnt since she could not get to the party. Abby, Buzzy, Jill, Q and Sally then board the service elevator, catching up with the others on the eleventh floor. Anna manages to escape from an emergency escape hatch, rejoining Buzzy and the others in their elevator. At exactly 8:05pm, the lightning strikes the hotel again, and both elevators plummet downwards. Amidst the chaos, Sally forgives her sister, and as they hold hands, they both turn into a shower of gold dust, breaking the curse and stopping both elevators just as they were about to hit the ground floor.

The groups are saved, and they all go to the Tip-Top Club on the top floor, restored to its former glory. One by one, the ghosts then ascend to Heaven, along with the other partygoers. Abigail, young once more, appears, meeting up once more with her sister, and thanks her for the present. The Gregory sisters then join hands and vanish into the night, breaking the curse on the hotel. With the spell broken, the Tower is re-opened to the public, with Q taking charge.

==Cast==
*Steve Guttenberg &mdash; Buzzy Crocker
*Kirsten Dunst &mdash; Anna Petterson
*Nia Peeples &mdash; Jill Perry Michael McShane &mdash; Chris Q Todd
*Amzie Strickland &mdash; Abigail "Abby" Gregory
*Melora Hardin &mdash; Carolyn Crosson / Claire Poulet
*Alastair Duncan &mdash; Gilbert London
*Lindsay Ridgeway &mdash; Sally "Sally Shine" Gregory John Franklin &mdash; Dewey Todd
*Wendy Worthington &mdash; Emeline Partridge
*Lela Ivey &mdash; Patricia Petterson
*Richard Minchenberg &mdash; Dr. Daniels
*Marcus Smythe &mdash; Surgeon
*Don Perry &mdash; Great Grand Dad
*Michael Waltman &mdash; Reporter
*Ben Kronen &mdash; Mr. Galvao
*Bill Elliot &mdash; Bandleader
*Shira Roth &mdash; Young Abigail
*Lynne Donahoe &mdash; Chloe
*Dean Marsico &mdash; Photographer

==See also==
*List of ghost films

==Notes== 
*Dewey Todd appears in D.J. MacHales 2003 novel, The Never War, book 3 of his series The Pendragon Adventure. The Manhattan Tower Hotel is also a major setting, the sister to the Hollywood Tower Hotel. Additionally, in The Pilgrims of Rayne, Dewey Todd is reported missing as he was in an elevator in the Hollywood Tower Hotel the moment it was struck by lightning.
 

==External links==
 
*   
* 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 