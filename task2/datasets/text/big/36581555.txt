Peaceable Kingdom: The Journey Home
 
{{Infobox film
| name           = Peaceable Kingdom: The Journey Home
| image          = Poster for the movie Peaceable Kingdom The Journey Home.png
| alt            = Cover picture
| director       = Jenny Stein
| producer       = James LaVeck Kevin Bartlett  Joy Askew
| editing        = Jenny Stein
| cinematography = Jenny Stein 
| released       =  
| runtime        =  78 minutes
| country        = United States
| language       = English
}}
Peaceable Kingdom: The Journey Home is a documentary released in 2009 which relates the personal transformation of farmers as they reexamine their relationship to animals. The movie also tells the story of two animal rescues.  
   

The principal filmmakers were James LaVeck and Jenny Stein working with associate producers Eric Huang and Kevin Smith. The name of LaVeck and Steins charitable organization is Tribe of Heart.  

The stories of seven people compose the core of the documentary: Harold Brown, Howard Lyman and his wife Willow Jeane Lyman, Cheri Ezell-Vandersluis and her husband Jim Vandersluis, and Jason Tracy and his partner Cayce Mell.

In the case of Harold Brown, the Lyman couple, and the Ezell-Vandersluis couple, the movie tells the story of how they began as traditional farmers working as part of the system that uses animals for food or other human purposes, but then came to the view that such practices are cruel and unethical.

In the case of Jason Tracy and Cayce Mell the movie tells the story of two animal rescues they participated in:  one occurring after a tornado hit a chicken farm, and the other from the house of a woman who suffered from an animal hoarding compulsion.
 Kevin Bartlett and Joy Askew.
 abolitionism movement within animal rights) is listed in the credits for the movie. Consistent with Abolitionism (animal rights)|abolitionism—LaVeck and Stein created a web site called HumaneMyth.org which advocates that it is not possible for animals to be treated humanely in an agricultural process which uses the animal for food. 

Actor Alicia Silverstone also appears in the credits and has supported the work of Tribe of Heart.

==Awards==
The film has won various awards: 

*Berkshire International Film Festival: Best of Fest Audience Award 
*Moondance International Film Festival: "Spirit of Moondance" Best Feature Documentary 
*Canadian International Film Festival: Grand Jury Prize
*Peace on Earth Film Festival:  Best Feature Documentary 
*Environmental Film Festival at Yale: Audience Award: Best Feature
 Peaceable Kingdom (without the subtitle: The Journey Home) which was released in 2004, but which is not mentioned on the Tribe of Heart web site.

== See also ==
* Animal rights
* Veganism

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 