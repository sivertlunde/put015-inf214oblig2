Listen, Darling
{{Infobox film
| name = Listen, Darling
| image = Listen, Darling VideoCover.jpeg
| caption =
| director = Edwin L. Marin Jack Cummings
| writer = Katharine Brush  (story)  Anne Morrison Chapin Elaine Ryan Noel Langley  (uncredited) 
| starring = Judy Garland Freddie Bartholomew Mary Astor Walter Pidgeon
| music = William Axt
| cinematography = Charles Lawton Jr. Lester White
| editing = Blanche Sewell
| distributor = Metro-Goldwyn-Mayer
| released =  
| runtime = 75 minutes
| country = United States
| language = English
| budget = $566,000  . 
| gross = $583,000 
}}
 musical comedy film starring Judy Garland, Freddie Bartholomew, Mary Astor, and Walter Pidgeon. 

==Cast==
* Judy Garland as "Pinkie" Wingate
* Freddie Bartholomew as Herbert "Buzz" Mitchell
* Mary Astor as Dorothy "Dottie" Wingate
* Walter Pidgeon as Richard Thurlow Alan Hale as J.J. Slattery
* Scotty Beckett as Billie Wingate
* Barnett Parker as Abercrombie
* Gene Lockhart as Arthur Drubbs
* Charley Grapewin as Uncle Joe Higgins

==Plot synopsis==
A girl (Garland) and her friend (Bartholomew) go to great lengths to prevent her widowed mother from marrying the wrong person.

==Box Office==
According to MGM records, the film earned $381,000 in the US and Canada and $202,000 elsewhere, resulting in a loss of $206,000. 

==References==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 