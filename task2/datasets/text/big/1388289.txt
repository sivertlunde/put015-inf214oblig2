White Dog
 
{{Infobox film
| name           = White Dog
| image          = White Dog DVD cover.jpg
| image_size     =
| caption        = The Criterion Collection DVD cover
| director       = Samuel Fuller Jon Davison
| screenplay     = Samuel Fuller Curtis Hanson
| based on       =  
| narrator       =
| starring       = Kristy McNichol Paul Winfield Burl Ives Jameson Parker Parley Baer
| music          = Ennio Morricone
| cinematography = Bruce Surtees
| editing        = Bernard Gribble
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = United States Dollar|$7,000,000 (estimated)
| gross          = $46,509  (United States) 
}} same title. The film depicts the struggle of a dog trainer named Keys (Paul Winfield), who is black, trying to retrain a stray dog found by a young actress (Kristy McNichol), that is a "white dog"—a dog trained to viciously attack any black person. Fuller uses the film as a platform to deliver an anti-racist message as it examines the question of whether racism is a treatable problem or an incurable condition.

The films theatrical release was suppressed in the United States by Paramount Pictures out of concern of negative press after rumors began circulating that the film was racist. It was released internationally in France and the United Kingdom in 1982, and broadcast on various American cable television channels. Its first official American release came in December 2008 when The Criterion Collection released the original uncut film to DVD.

Critics praised the films hard line look at racism and Fullers use of melodrama and metaphors to present his argument, and its somewhat disheartening ending that leaves the impression that while racism is learned, it cannot be cured. Reviewers consistently questioned the films lack of wide release in the United States when it was completed and applauded its belated release by Criterion.

==Plot==
Young actress Julie Sawyer (Kristy McNichol) accidentally runs over a stray white German Shepherd dog one night. After the dog is treated by a vet, Julie takes him home while trying to find his owners. A rapist breaks into her house and tries to attack her, but the dog protects her so she decides to adopt him, against the wishes of her boyfriend (Jameson Parker). Unbeknown to her, the dog was trained by a white racist to attack any and all black people on sight. It sneaks out of the house one night and kills a black truck driver in an attack. Later, when Julie takes the dog to work with her, it attacks a black actress on the set.

Realizing something is not right with the dog, Julie takes him to a dog trainer, Carruthers (Burl Ives), who tells her to kill the dog. Another dog trainer named Keys (Paul Winfield), who is black himself, undertakes re-educating the dog as a personal challenge. He dons protective gear and keeps the dog in a large enclosure, taking him out on a chain and exposing himself to the dog each day and making sure he is the only one to feed or care for the dog.

The dog manages to escape, and kills an elderly black man in a church, after which Keys manages to recover him, and opts not to turn the dog in to the authorities, but to continue the training, over Julies protests.  He warns her that the training has reached a critical point, where the dog might be cured or go insane.  He believes that curing the dog will discourage white racists from training dogs like this, though there is no indication in the story that this is any kind of national problem (the film is set well after the civil rights era, the setting of the original novel).

After a lengthy time, it seems as if the dog is cured, in that he is now friendly towards Keys.  Julie confronts the dogs original owner, who has come to claim him, and who presumably trained him to attack black people. She angrily tells him in front of his grandchildren, who only know the dog as a loving family pet, that the dog has been cured by a black man.

Just as Julie and Keys celebrate their victory, the dog, without warning, turns its attention to Carruthers and brutally attacks him. The dog had not previously shown any aggression towards him — no explanation for this is given, but the implication is that the dogs programming has somehow been reversed, though that was never Keys intention. To save his employers life, Keys is forced to shoot the dog, and the film ends with the image of the dogs body lying in the center of the training enclosure.

==Themes== Captain Ahab, he declares that if he fails with this dog, he will find another and another until he succeeds.  Keys counterpart, Carruthers, a white trainer, believes the dog is irredeemable and should be killed, representing the view that racism cannot be cured.   

 

Scenes showing Kristy McNichol innocently burying her hands in the dogs fur and his normal loving behavior when alone with her provides a stark image of "how hatred can be familiar, reassuringly close".  J. Hoberman notes that the film "naturalizes racism in an unnatural way" in the contrasting depictions of white characters horrified by the dogs behavior, and black characters who grimly accept it as a fact of life.  The films ending have been argued to emphasize Fullers own view that racism is something that is learned, but that once learned is a "poison" that can never truly "be banished from those it infects". But on the other hand, the dog is actually cured of attacking blacks, but not cured of his own hatred since the last thing he does is to, unprovoked, attack a white man. The ending implies therefore that it is hatred (and not racism) that cannot be banished from those it infects. 

In the original Romain Gary novel, this was not the story that was told — the dog started to attack white people because a black man embittered by white racism deliberately retrained him to do so.

==Production==
White Dogs roots lie with a 1970 autobiographical novel written by Romain Gary of the same name.    The story was purchased for use by Paramount in 1975, with Curtis Hanson selected to write the screenplay and Roman Polanski hired to direct. Before shooting commenced, Polanski was charged with statutory rape and fled the country, leaving the production in limbo.  Over a span of six years, the project was given to various writers and producers, who all focused on the stray dog story from Garys original work.   Garys activist wife was replaced in the script with a young, unmarried actress because Paramount wanted to contrast the dogs random attacks with a loving relationship between the protagonist and the dog. Paramount executives noted that they wanted a "Jaws (film)|Jaws with paws" and indicated that they wanted any racial elements to be downplayed. In one memo, the company noted: "Given the organic elements of this story, it is imperative that we never overtly address through attitude or statement the issue of racism per se". 

By 1981, Garys wife and then Gary himself had committed suicide. At the same time, Hollywood was under threat of strikes by both the writer and director guilds.   Needing enough films to carry the studio through in case the strikes happened, White Dog was one of thirteen films considered to be far enough along to be completable in a short time frame. With a push from Michael Eisner, White Dog was one of seven that Paramount put on a fast track for production. Eisner pushed for the film to be one of the selected ones because of its social message that hate was learned. Producer Jon Davison was less certain and questioned the films marketing early on.  Hanson, back on board as the films screenwriter, suggested Samuel Fuller be named the films director as he felt Fuller was the only one available with the experience needed to complete the film on such a short schedule and with a low budget, while still doing so responsibly with regard to the sensitive material.     Davison agreed after visiting Fuller and seeing Fuller act out how he would shoot the film.   

Fuller readily agreed, having focused much of his career on racial issues.  Already familiar with the novel and with the concept of "white dogs", he was tasked with "reconceptualizing" the film to have the conflict depicted in the book occur within the dog rather than the people. In an earlier Variety (magazine)|Variety magazine interview, Fuller stated that viewers would "see a dog slowly go insane and then come back to sanity".  Before filming began, the National Association for the Advancement of Colored People (NAACP), the Black Anti-Defamation Coalition (BADC), and other civil-rights leaders began voicing concerns that the film would spur racial violence. In an editorial in the Los Angeles Times, Robert Price, executive director of the BADC, criticized the studio for producing the film based on a book by a white man and using a primarily white cast and crew, rather than producing the film with African Americans in key positions. He also considered Garys work to be a "second-rate novel" and questioned its use when "book shelves are laden with quality novels by black writers who explore the same social and psychological areas with far more subtlety?" 

Fuller was confident in his work and the idea that the film would be strongly antiracist, particular with the changes he had made to the original work. The novels hate-filled Muslim black trainer, who deliberately retrained the dog to attack white people, was converted into the character of Keys, who genuinely wished to cure the animal. Fuller also changed the novels original ending into a more pessimistic film ending.   The film was shot in only forty-five days at a cost of United States Dollar|$7 million. Five white German Shepherd Dogs played the unnamed central character. 
  a series of murders of black children was occurring in Atlanta.  The two men provided a write-up of their views for the studio executives, which were passed to Davison along with warnings that the studio feared a film boycott.  Fuller was not told of these discussions nor given the notes until two weeks before filming was slated to conclude.  Known for being a staunch integrationist and for his regularly giving black actors non-stereotypical roles, Fuller was furious, finding the studios actions insulting.  He reportedly had both representatives banned from the set afterwards, though he did integrate some of the suggested changes into the film.  

The film was completed in 1981, but Paramount was hesitant to release the film out of continuing concerns that the film would be misconstrued.    Though no one from the organization had viewed the completed film, the NAACP threatened boycotts.  In early 1982, the studio finally held preview screenings in Seattle, Washington and Denver, Colorado, with mixed responses. That fall, another test run was held in Detroit, Illinois which resulted in praise from critics but little public interest.  The film was finally left unreleased, with Paramount feeling it did not have enough earnings potential to go against the threatened boycotts and possible bad publicity.       Dumbfounded and hurt by the films shelving, Fuller moved to France, and never directed another American film.  

==Distribution==
Paramount felt the film was too controversial for release, giving it only a few limited preview runs before shelving it.   The films first theatrical release occurred in France on July 7, 1982. In the United Kingdom, it was part of the 37th Edinburgh International Film Festival and the 27th London Film Festival    in 1983, and was released late that year by United International Pictures. It received positive reviews in both countries.  Lisa Dombrowski of Film Comment notes, "In the end, Sam Fullers White Dog was muzzled by a collision of historically specific economic and political interests, as support for freedom of expression took a backseat to Paramounts bottom line and the NAACPs ongoing battles with Hollywood over representation and employment. A Sam Fuller thriller was simply not the kind of antiracist picture that a major studio knew how to market in 1981 or that African-American organizations wanted Hollywood to make at the time". 

In 1983, White Dog was edited for a direct-to-television broadcast and made available purchase by cable channels. The following year, NBC bought broadcast rights for $2.5 million and slated the film to air during the February sweeps, then canceled the broadcast two days later due to pressure from the continuing NAACP campaign and concerns of a negative reaction by both viewers and advertisers.  The film was eventually aired on other cable channels sporadically and without fanfare.    It was also infrequently screened at independent film houses and film festivals. 

Its first official American release came on December 2, 2008, when The Criterion Collection released the film to DVD.   The DVD has the uncut version of the film, video interviews from the original producer and writer, an interview with the trainer of the dog used in the film, and a booklet of critical essays.    The National Society of Film Critics bestowed the distributor with a special film heritage award for releasing the film.  

==Reception==
Due to its limited release, it only grossed $46,509.    While today the film is generally seen as a B-movie, it was initially praised by critics upon its release, particularly for its treatment of racism and Fullers directorial talents. Dave Kehr, of the Chicago Tribune, praised Fuller for "pulling no punches" in the film and for his use of metaphors to present racism "as a mental disease, for which there may or may not be a cure". Kehr considered the film less melodramatic or bizarre than Fullers earlier works, which was also positive since it left the film "clean and uncluttered with a single, concentrated line of development mounting toward a single, crushingly pessimistic moral insight".  Entertainment Weekly s Kim Moran called it a "uncompromising, poignant examination of racism" and felt it was one of Fullers most inspired films and a "gripping, meditative, and ultimately beautiful achievement".    Video Business reviewer Cyril Pearl called it "bombastic, odd and quite chilling" and felt the film was an antiracist work that "deserve  an audience".   
 
Charles Taylor, writing for  , referred to the film as "an impassioned attack on racial hatred".  Another New York Times reviewer, Janet Maslin, praised Fullers "command of stark, spooky imagery", "B-style bluntness", and the way the cinematography, scene setting, and soundtrack combine to give the film "the blunt, unnerving power of a horror story". She also commended Paul Winfields performance as Keys, feeling the actor turned what might have been a boring character into one audiences would find interesting.    Slant Magazines Fernando F. Croce felt the film was "part marauding-animal horror movie, part Afterschool Special,   part tragic-sardonic agitprop" B-movie that is "searing confrontation of the irrationality of prejudice".   

In The Magic Hour: Film at Fin de Siècle, J. Hoberman referred to the film as an "unusually blunt and suggestive metaphoric account of American racism". Though he felt the film was a "sad waste" of Fullers talent, he praised the directors treatment of the work, including the changes made to the source material, noting that "filmed in headlines, framed as allegory, White Dog combines hard-boiled sentimentality and hysterical violence." He praised the musical score used in the film for lending dignity to the "iconic visuals and cartoon dialogue."   

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 