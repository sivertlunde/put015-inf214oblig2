Murder Rock
{{Infobox film
| name           = Murder Rock
| image          = Murder Rock.jpg
| alt            = 
| caption        = Italian theatrical release poster
| director       = Lucio Fulci
| producer       = Augusto Caminito Gabriele Silvestri
| screenplay     = Gianfranco Clerici Lucio Fulci Roberto Gianviti Vincenzo Mannino
| story          = Gianfranco Clerici Lucio Fulci Vincenzo Mannino
| starring       = Olga Karlatos Ray Lovelock Claudio Cassinelli Cosimo Cinieri Giuseppe Mannajuolo
| music          = Keith Emerson
| cinematography = Giuseppe Pinori
| editing        = Vincenzo Tomassi
| studio         = Scena Film
| released       = 20 April 1984
| runtime        = 93 min.
| country        = Italy
| language       = Italian
}}

Murder Rock (Italian: Murderock - uccide a passo di danza; also known as Murder-Rock: Dancing Death, Slashdance and The Demon Is Loose) is a 1984 Italian giallo film starring Olga Karlatos and Ray Lovelock, and written and directed by Lucio Fulci.   Fulci recalled the producer forced him to turn the film into a musical with the music of Keith Emerson due to the success of Flashdance. 

==Plot==
At the Arts for the Living Center in New York, Candice Norman (Olga Karlatos) oversees the latest dance routine choreographed by Margie (Geretta Marie Fields). Candice tells Margie that the act needs even "more perfection" in preparation for a visit from three talent agents. The academy director Dick Gibson (Claudio Cassinelli) meets with TV producers Bob Steiner and John Morris, who watch a video of the dance, and Candice learns that the men will only select three dancers for an upcoming TV show. That evening after the dance class is over, one of the dancers, Susan, is murdered in the locker room by an unseen person who chloroforms then stabs her in the heart with a long hairpin needle. NYPD Lieutenant Borges (Cosimo Cinieri) arrives on the scene to investigate. Also with Lt. Borges is the police profiler and psychotherapy professor Dr. Davis (Giuseppe Mannajuolo). With Candice nowhere to be found, suspicion begins to focus not only on her but on the victims boyfriend Willy Stark (Christian Borromeo), as well as Dick Gibson.

Candice arrives back at her apartment, where she finds Dick waiting. He wants to talk about the potential relationships between the students, and tries to convince Candice that there is nothing going on between him and any of the other students. While he is there, the DJ from the studio, Bob, phones Candice and updates her on the murder at the academy.

The next day, the routine at the campus continues as normal, causing Dick a great deal of upset, since nobody seems to care about Susans death. At a nearby coffee shop, Lt. Borges talks to Dick about a possible rivalry between the dancers. That evening at a local nightclub, another student from the academy, Janice, dances alone for an audience and then walks home to her apartment. As with Candice the previous night, she finds Dick waiting and wanting to talk. In her bedroom, Janice finds a photograph of Dick and Susan, but when she calls out to him, he is gone. Janice finds her pet canary dead with a hairpin needle through its body. Panicked, she runs to the front door, where she is attacked and killed by the unseen assailant who thrusts a hairpin needle into her heart.

Candice begins having nightmares of being attacked by a handsome young man (Ray Lovelock) wielding a long, ornamental needle identical to the one used in the killings. Candice becomes more obsessed with the dream assailant when she sees an advertising billboard which features him prominently. Unable to shake the feeling that they are in some way predetermined to meet, Candice tracks down the man in the poster to the seedy Fulton Hotel, and bribes the desk clerk for the key to "Mr. Robinsons" room. She explores the room, but is shocked when the handsome model returns suddenly. The man introduces himself as George Webb who, to Candices repulsion, is a drunken and disheveled wreck. With what seems like disappointment mingled with terror, Candice flees from the room, leaving her purse and ID behind.

Meanwhile, Lt. Borges records a phone call to the police station from a person claiming be the killer. When a voice analyst identifies it as Bart, one of the dancers, Borges arrests him. Bart confesses to the phone calls as well as to killing Susan because she was crazy, and Janice because she was Hispanic. Borges, however, states his belief that Bart is not the killer, but a pathological liar.

George goes to the academy to return Candices purse where another dancer, Gloria, recognizes him from a modeling show they appeared in years earlier. Dick sees George and Candice on a security monitor and calls Borges saying that George must be the killer. Over lunch at a local Chinese restaurant, the relationship between Candice and George becomes closer when she confides in him about an incident years earlier, when a man on a motorcycle ran her over in a hit-and-run putting an end to her dancing career and forcing her to teach. Back at Candices apartment, she gets a phone call from Phil (Lucio Fulci), a local talent agent, who has made a background check for her on George and informs her that he once had an affair with a young girl who later died. At the studio, Candice is attacked by Margie, using the killers M.O., but she is subdued by Dick, who turns Margie over to the police.

A few days later, Jill, another dancer, baby-sits for the wheelchair-bound Molly, who takes pictures of her. When Jill answers a knock on the front door, the killer forces his way into the apartment and stabs her with the hairpin. Molly frantically snaps away, but does not get a clear view of the killers face. The police arrest Dick when hes found running away, claiming that he only arrived after the attack to find Jill dead. The following night, the killer attacks and kills Gloria in the locker room in the same manner.

At Georges hotel room, Candice lets herself in and finds a hairpin and bottle of chloroform in his drawer. Candice rushes out to her car and drives away. George arrives and seeing what Candice found, tries to call her at her apartment, but she is not home. At the police station, Candice tells Borges of her discovery and that George is the killer and names the hotel where he is staying. Candice then goes over to the academy which has been closed down for the night, and finds Gloria dead in the locker room. She calls Borges and tells him to meet her at the academy.

Candice runs to the managers room and finds George there. He turns on the music and a video showing all the dancers who have been killed. George walks in with the hairpin murder weapon and asks Candice why she planted this evidence in his hotel room, thus connecting him to the murders. Candice then reveals that she has known from the very start that it was he, George, who was the hit-and-run motorcycle rider who ran her over, ruining her career, her future and her life. She tells him that she killed the young dancers out of jealousy of their talent, their beauty and their rising careers, all the while planning to frame him for the killings as her final act of revenge against him. She grabs Georges pin-holding hand and intentionally impales herself on it. Just as she falls to the floor, breathing her last, Lt. Borges and Dr. Davis arrive on the scene but, before George can begin to explain, they reveal that they already know he is not the killer. When they saw the photo slide Molly took of the killers jacket, they noticed that the buttons on the jacket were on the left side, indicating that it belonged to and was being worn by a woman. In addition, Candice told them details about the lions head hairpin needle used in the killings, information that, since it had never been made public, only the killer could know. George leaves with Lt. Borges as the police seal off and investigate the scene.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 


 