Leïla (2001 film)
 
{{Infobox film
| name           = Leïla
| image          = 
| caption        = 
| director       = Gabriel Axel
| producer       = Ulrik Bolt Jørgensen
| writer         = Gabriel Axel Bettina Howitz
| starring       = Mélanie Doutey
| music          = Younès Mégri
| narrator       = Michel Bouquet
| cinematography = Morten Bruus
| editing        = Grete Møldrup
| distributor    = Angel Films
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = French
| budget         = 
}}
Leïla is a 2001 Danish film directed by Gabriel Axel. 

==Plot== Berber girl Leïla. It marks the beginning of a great, all-encompassing passion. Leïla must defy her family to be with Nils, and their love has big consequences. 

==Cast==
* Mélanie Doutey as Leïla
*   as Nils
* Malika El-Omari as Moona
* Azeddine Bouayad as Brahim
* Michel Bouquet - narrator
* Mitch Bateman as party goer
* Christian E. Christiansen as Nils friend no. 2
*   as Nils friend no. 1 (uncredited)

==Reception== film critic   in Variety (magazine)|Variety wrote "Pic feels tepid and devoid of emotional involvement. Due to the use of Voice-over|v.o., the two leads never get a real chance to convey their characters emotions." 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 