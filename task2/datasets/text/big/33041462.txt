How McDougall Topped the Score
 

 
{{Infobox film
| name           = How McDougall Topped the Score
| image          = 
| image_size     =
| caption        = 
| director       = V. Upton Brown
| producer       = 
| writer         = V. Upton Brown
| based on       = poem by Thomas Edward Spencer
| narrator       =
| starring       = Leslie Gordon
| music          =
| cinematography = 
| editing        = 
| studio = Pacific Screen Plays 
| distributor    = 
| released       = 10 November 1924
| runtime        = 5 reels
| country        = Australia English intertitles
| budget         = ₤800 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 123. 
| preceded_by    = 
| followed_by    = 
}}

How McDougall Topped the Score is a 1924 Australian silent film directed by V. Upton Brown. It is based on a famous poem by Thomas Edward Spencer about a cricket match won when a dog steals a ball, enabling the batting team to score plenty of runs. 

It is considered a lost film.

==Plot==
A cricket match takes place in the country between the towns of Pipers Flat and Molonglo. An old Scotsman, McDougall, comes out to bat with his team needing 50 runs to win. He hits the ball to the boundary, when his dog, Pincher, runs away with it. By the time the fielding team catch Pincher, McDougall has make enough runs to win. There is a romantic subplot involving McDougalls daughter.

==Cast==
*Leslie Gordon as McDougall
*Ida Gresham as Mrs McDougall
*Dorothy May as Mary McDougall
*Wesley Barry as McDougall Jr
*Frank Blandford as Johnstone
*William Ralston as Brady
*Joy Thompson
*Pincher

==Production==
The film was shot in the Melbourne suburb of Ashburton and on location in the Dandenong Ranges.  It was the first feature from Pacific Screen Plays, an Australian and New Zealand company. 

==Release==
The film was previewed in October and made its debut in Adelaide on 10 November 1924 to coincide with the first match between the South Australian cricket team and a visiting English side.  However it does not appear to have had a wide release. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
* 

 
 
 
 
 


 