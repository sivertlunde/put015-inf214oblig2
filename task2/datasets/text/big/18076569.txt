Kambakkht Ishq
 
 
{{Infobox film
| name           = Kambakkht Ishq
| image          = KambakkhtIshq.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Sabbir Khan
| producer       = Sajid Nadiadwala
| screenplay     = Kiran Kotrial Anvita Dutt Guptan Ishita Mohitra Sabbir Khan
| story          = Anvita Dutt Guptan Ishita Mohitra Sabbir Khan
| starring       = Akshay Kumar Kareena Kapoor Aftab Shivdasani Amrita Arora Sylvester Stallone Denise Richards RDB
| cinematography = Vikas Shivraman
| editing        = 
| studio         = 
| distributor    = Nadiadwala Grandson Entertainment Eros Entertainment
| released       =  
| runtime        = 135 minutes 
| country        = India
| language       = Hindi
| budget         =   
| gross          =   
}}
 Tamil film Pammal K. Sambandam,    and features Akshay Kumar and Kareena Kapoor in pivotal roles along with actors Aftab Shivdasani and Amrita Arora in supporting roles. Hollywood actors Sylvester Stallone, Denise Richards, Brandon Routh and Holly Valance appear in cameos, playing themselves.    Originally scheduled to release in December 2008,  the film was postponed due to extensive production work and was released on 3 July 2009, and was declared a semi-hit at the box office. 

==Plot==
When Hollywood stuntman Viraj Shergill (Akshay Kumar) and medical student Simrita Rai (Kareena Kapoor) come across each other at his brother Luckys (Aftab Shivdasani) and her best friend Kaminis (Amrita Arora) hastily planned wedding ceremony, they instantly develop a dislike for each other. They both have a very low opinion of the opposite sex, and staunchly believe marriage is not the way to go for the two newlyweds, whom they try to discourage from proceeding further.

Simrita convinces Kamini to test out her theory that men are after only one thing, by forcing Lucky to delay their marriage vows for three months. She is sure, Lucky will not be able to do so and this will prove to Kamini that Lucky is just another low-class male, like all others, and her claims of him being different than most men are unfounded. Viraj, upon hearing about this new development, tries to do the exact opposite. This results in a hilarious scene at a local disco bar, where Viraj lures Kamini with the hope of her finding Lucky with a "girlfriend" that he has planted in the lap of Lucky in order to make Kamini jealous. The plan backfires and Kamini and Lucky end up in divorce court where the judge puts them on a three-month probation and marriage counselling. Simrita keeps on telling Kamini that all men are the same and that all they want is sex. Lucky tries to make love with Kamini but she never lets him come near her.

Meanwhile, Simrita and Viraj keep bumping into each other when they both travel to Italy independently; the former to make some quick cash as a model to pay for her medical bills, and the latter, with Lucky, to chill out and take a vacation from all of the marital stress Lucky has been facing back home. Upon returning home, Simrita is given a watch pendant by her Dolly Aunty (Kirron Kher) to wear as a good luck charm. The pendant hangs from a bracelet on her wrist. Viraj gets seriously hurt in an accident at work and is brought for emergency surgery to the hospital where Simrita is an intern. Much to each others irritation, Simrita is given the charge to perform surgery on Viraj. After the surgery, while looking at the X-rays, she is horrified to discover that the watch on her wrist has accidentally ended up in Virajs stomach.

Simrita tries various ways of getting the watch out. During one such process, which fails, it becomes clear that Simrita is bitter about her perceived unfaithfulness of the two most prominent men from her childhood: her divorced father and her elder sisters ex-husband. By this time, Viraj is secretly beginning to fall in love with Simrita. Several days later, Simrita is successful in removing the watch from Virajs stomach. Following the surgery, Viraj overhears the truth and leaves. Several days later, he proposes to Denise Richards in an attempt to forget about Simrita. Upon realising her mistake, Simrita helps Lucky and Kamini reconcile and decides to confess her love for Viraj at the wedding ceremony. At first, Denise is upset but then tells Viraj to go to Simrita. The two embrace and go off together in the car sharing a few passionate kisses.

==Cast==
* Akshay Kumar as Viraj Shergill
* Kareena Kapoor as Simrita Rai  
* Aftab Shivdasani as Lucky
* Amrita Arora as Kamini
* Vindu Dara Singh as Tiger 
* Kirron Kher as Dolly
* Javed Jaffrey as Keswani
* Rajesh Khera as Dr. Ali
* Kehkashaan Patel as Nimrata
* Ashwin Mushran as Parmeet

Cameo appearances (in alphabetical order);
* Boman Irani as ENT Doctor
* Brandon Routh as Himself
* Denise Richards as Herself
* Holly Valance as Herself
* Sylvester Stallone as Himself

== Production ==
  on set of Kambakkht Ishq (2009) with Sylvester Stallone]]
In November 2007, producer Sajid Nadiadwala signed on actor Akshay Kumar to play the lead role in his film.  Later that month Kareena Kapoor was signed on to play the female lead.    It is the seventh time Kumar and Kapoor are paired on-screen. Kumar plays a Hollywood stuntman in the film    and Kapoor plays a high-society girl.  According to Nadiadwala, "It will be an international love story on a lavish scale." 

The project was originally expected to commence filming in January 2008 in Los Angeles with nine actors from Hollywood  but due to extensive pre-production work on the film, it was later shifted to May 2008.    Reports indicated that actor and politician Arnold Schwarzenegger was signed on to do a cameo and that the film would feature a performance from singer and actress Beyoncé Knowles.  Prashant Shah, line producer of Kambakkht Ishq in L.A. commented, "We are in talks with Beyoncé and we have also received a letter from Arnolds office...Things will be confirmed shortly and we will be making a formal announcement when that happens."  It was later reported that neither star would be appearing in the film.   
  Universal Studios." fire broke out on the backlot of Universal Studios on 1 June 2008, the shooting of the film was expected to be delayed. But the producer indicated, "so far the area where we are supposed to shoot remains unaffected. So for now we are proceeding on schedule."  Later that month, the cast finally began filming for the project in Los Angeles, California, where pivotal scenes were shot at the Kodak Theatre and Universal Studios.  In August 2008, reports had indicated that the film was 50–60% complete and the cast would continue filming for the project in Venice, Italy.  After completing their overseas schedule, the production team later headed to shoot in Mumbai, India. 

During December 2008, reports surfaced indicating that Kumar and Kapoor would appear alongside Carmen Electra in a promotional music video for the film.   In February 2009, Nadiadwala announced that Electra wouldnt be appearing in the music video and was replaced by the music group RDB (Rhythm Dhol Bass)|RDB. According to Nadiadwala, "It is not a music video but a song that we will use as an integral part of the film. It will occur close to the films interval. It will also be the first promotional song."  The first trailer of the film was shown alongside the release of Jehangir Surtis film Aa Dekhen Zara (2009). 

== Release ==

=== Box office ===
  Rs 10&nbsp;million from its 125 advanced paid previews shown on Thursday   night.    On 3 July 2009, Kambakkht Ishq opened to a bumper response of 90–100% all over India.  Making a first day total of Rs85.0&nbsp;million,  the film emerged as the second biggest opening day gross of all time for a Bollywood film.      For its opening weekend in India, Kambakkht Ishq accumulated a total of Rs245.0&nbsp;million  from 1,400 screens.    The following Monday, it grossed another Rs40&nbsp;million,  and the following Tuesday it grossed Rs30&nbsp;million.  At the end of its first week, Kambakkht Ishq collected a total of Rs359.5&nbsp;million.  During its second week at the box office, the film showed a 70% drop in its collections; the movie was a semi hit grosser overall at the box office. 

Besides being released domestically in India, Eros International released the film in over 600 screens overseas in a combination of prints and digital format.  For its opening weekend in the United Kingdom, the film debuted at number 8 and grossed Pound sterling|£ 299,533 on 56 screens, for an average of £5,349 per screen.    Kambakkht Ishq debuted at number 14 in the United States and number 11 in Australia, accumulating US$768,542 from 100 screens in the former and Australian dollar|$152,626 from 17 screens in the latter.   As of 29 July 2009, Kambakkht Ishq has grossed a total of £681,154 in the U.K. and $1,445,739 in the U.S. 

=== Critical response === AOL India concluded, "The film has nothing going for it. It has a worryingly bad script, horrible screenplay, traumatising dialogues and unpleasant music."    Parasara further explained, "Kambakkht Ishq is a shame when it comes to watching Indian films on the world stage."  Rajeev Masand from CNN-IBN, who gave the film 1 star out of 5, noted that Kambakkht Ishq "is a loud, vulgar and seriously offensive film". 

Kambakkht Ishq started off well in the first week of release. The films first half is tremendous, while the second half takes a dip, with the film picking up towards the climax.

Taran Adarsh of indiaFM described Kambakkht Ishq as a film that would "strike a chord with the youth and those who relish zany and madcap entertainers".    He praised the performances, direction, and writing, noting that the film would "see an earth-shattering opening weekend and a historic Week 1."  Rachel Saltz from The New York Times concluded that "  has only one frantic desire: to entertain. It spottily succeeds, despite its frequently crude humor, relentless pace and a few unpalatable racial bits." 

== Soundtrack ==
{{Infobox album  
| Name        = Kambakkht Ishq 
| Type        = Soundtrack 
| Artist      = Anu Malik 
| Cover       = KI CDCover.jpg 
| Released    =  12 June 2009 (India)
| Recorded    =  Feature film soundtrack
| Length      = 41:03 
| Label       =   Eros Music
| Producer    = Sajid Nadiadwala
| Reviews     = 
| Last album  = Chal Chala Chal  (2009)
| This album  = Kambakkht Ishq (2009)
| Next album  = TBA 
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)!! Duration !! Music By
|-
| Om Mangalam RDB & Nindy Kaur
| 4:22 RDB
|-
| Lakh Lakh Neeraj Shridhar
| 5:15
| Anu Malik
|-
| Bebo
| Alisha Chinai
| 4:19
| Anu Malik
|-
| Kambakkht Ishq KK & Sunidhi Chauhan
| 4:51
| Anu Malik
|-
| Kyun Shaan & Shreya Ghoshal
| 5:29
| Anu Malik
|-
| Om Mangalam – Reprise RDB & Nindy Kaur
| 4:35 RDB
|-
| Lakh Lakh (Electro Dhol House Remix)
| Eric Pillai
| 4:02
| Anu Malik
|-
| Bebo (Club Mix)
| Kilogram K & G 
| 3:38
| Anu Malik
|-
| Kambakkht Ishq (Remix)
| Kilogram K & G 
| 4:00
| Anu Malik
|-
| Kyun (Female) – Reprise
| Shreya Ghoshal
| 4:30
| Anu Malik
|-
| Welcome To Hollywood
| Karsh Kale & Anushka Manchanda
| 2:11
| Salim-Sulaiman
|}

== References ==
 

== External links ==
*   
*   
*  

 
 
 
 
 
 