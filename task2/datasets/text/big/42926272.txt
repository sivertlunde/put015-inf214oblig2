Midnight Lovers (1926 film)
{{infobox film
| title          = Midnight Lovers
| image          =
| imagesize      =
| caption        = John Francis Dillon John McCormick
| writer         = Carey Wilson (story) George Marion, Jr. (intertitles)
| starring       = Lewis Stone Anna Q. Nilsson
| music          =
| cinematography = James Van Trees
| editing        =
| distributor    = First National Pictures
| released       = October 25, 1926
| runtime        = 70 minutes
| country        = United States Silent (English intertitles)

}} John Francis Dillon and distributed by First National Pictures. It starred Lewis Stone and Anna Q. Nilsson. 

Prints of the film are preserved at the Library of Congress and the Wisconsin Center for Film and Theater Research, Madison.  

==Cast==
*Lewis Stone - Major William Ridgewell, RFC
*Anna Q. Nilsson - Diana Fothergill John Roche - Owen Ffolliott
*Chester Conklin - Moriarty Dale Fuller - Heatley
*Purnell Pratt - Wibley Harvey Clark - Archer

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 
 