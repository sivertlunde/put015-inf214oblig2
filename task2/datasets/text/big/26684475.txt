Ikaw Ang Pag-ibig
{{Infobox film
| name           = Ikaw Ang Pag-ibig
| image          = Ikawangpagibigposter.png
| caption        = 
| director       = Marilou Diaz-Abaya   accessed September 11, 2011 
| producer       = Marilou Diaz-Abaya Charo Santos-Concio Malou N. Santos Leonardo Legaspi
| writer         = Marilou Diaz-Abaya 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Jomari Yllana Marvin Agustin Ina Feleo
| music          = Nonong Buencamino
| cinematography = David Abaya
| editing        = Tara Illenberger Archdiocese of Caceres Marilou Diaz-Abaya Film Institute and Arts Center
| distributor    = Star Cinema
| released       =  
| runtime        = 120 minutes
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = 
}}

Ikaw ang Pag-ibig (You Are Love) is a  , the patroness of the Bicol Region in the Philippines.  The film was released on September 14, 2011. 

==Overview==

===Production===
Preparations for the film began in July 2010. The film was to be helmed by the critically acclaimed director of Muro Ami and Pusod ng Dagat, Marilou Diaz-Abaya. Initially, the films tentative titles were Peñafrancia and Ina, but these later changed to Ikaw Ang Pag-Ibig.  Filming started middle of 2010 in Naga City during the 300th anniversary of Our Lady of Peñafrancia. Unitel Productions was supposed to produce this film but when it backed out, the Archdiocese of Caceres in Naga City, Camarines Sur decided to fund the project. The said archdiocese is led by Archbishop Leonardo Legaspi, O.P.D.D., who has held this position for the past 26 years. 

===Reception===
The film has been Graded "A" by the Cinema Evaluation Board,    and Rated PG-13 by the Movie and Television Ratings and Classification Board. It has also been endorsed by the Catholic Bishops Conference of the Philippines, the Association of Catholic Universities and the Manila Archdiocese and Parochial Schools, Association, Inc. 

===Synopsis===
The movie revolves around a young, contemporary, rebellious woman Vangie Cruz (Ina Feleo), whose family life and career as a video editor are disrupted when her only brother, a newly ordained priest, Fr. Johnny (Marvin Agustin), is diagnosed of Acute Myeloid Leukemia. As a sibling, Vangie is called upon to be a donor for Fr. Johnnys bone marrow transplant. At first, Vangie is very reluctant. She has a clinical phobia for medical procedures, the reasons for which are rooted in an attempted, but botched, abortion which she suffered through many years earlier and has since been troubled about. Her life is saved by Dr. Joey Lucas (Jomari Yllana) with whom she has a love child, and whom she eventually marries. Vangies dysfunctional family gravitates around Fr. Johnny, and in their struggle to cope with his illness, find themselves drawn to Ina, begging for her intercession. Their prayers are answered, not so much by way of a miraculous cure for Fr. Johnny, but by the grace of conversion, of love, of forgiveness, reconciliation, and hope.

== Cast ==
* Ina Feleo  as Evangeline "Vangie" Cruz
* Marvin Agustin  as Fr. Johnny 
* Jomari Yllana   accessed September 2, 2011  as Dr. Joey Lucas 
* Shamaine Centenera
* Nonnie Buencamino
* Jaime Fabregas
* Eddie Garcia
* Yogo Singh

== References ==
 

== External links ==
* 
* 

 
 