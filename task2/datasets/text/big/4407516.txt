Leftenan Adnan
 
 
{{Infobox film name           = Leftenan Adnan image          = Leftenan Adnan.jpg caption        = director       = Aziz M. Osman producer       = writer         = Aziz M. Osman (screenplay) narrator       = starring       = Hairie Othman Umie Aida Farid Amirul Faizal Hussein Rusdi Ramli Shaharuddin Thamby Wahid (Senario) Rambo Chin cinematography = editing        = distributor    = Ministry of Defence Malaysia Royal Malaysian Army Grand Brilliance Sdn Bhd released       = 2000 runtime        = country        = Malaysia language  Malay English English Japanese Japanese Mandarin Mandarin
|budget         = RM 2.5 million gross    = RM1,075,697

}}

Leftenan Adnan is a 2000 Malaysian war film directed by Aziz M. Osman and produced by the Malaysian Army and Grand Brilliance Sdn Bhd.

==Plot==

The movie is about Adnan bin Saidi, a young Malay from Sungai Ramal in Kajang, Selangor who had joined the Malay Regiment of the British Colonial Forces just before the Second World War broke out in Asia. By the time the war broke out, he had been promoted to the rank of Second Lieutenant, and was in command of Company C, 1st Battalion, Malay Regiment after the death of the British company commander, Captain H R Rix. His exploits and bravery in combat while leading his men against the Japanese Imperial Army became legendary. The two known engagements he was involved in are:
* The Battle of Pasir Panjang, and
* The Battle of Bukit Chandu or Opium Hill.
 General Tomoyuki Yamashita commented on the leftenants bravery and valour before Adnans execution possibly as a lesson for the Japanese troops and said that if there were ten more soldiers like Adnan in the British Colonial Forces in Malaya at that time, he would have needed ten more divisions to conquer Malaya.

==Alexandra Hospital Massacre==
 
Although not referred to in the film, there is a current theory that the subsequent Alexandra Hospital massacre was directly related to the great frustration felt by the Japanese soldiers due to the heavy losses suffered in overcoming the stout resistance put up by Lt Adnans Company C on Opium Hill. With the loss of Company C, the remnants of the 1st Battalion withdrew leaving the way clear to the British military hospital at Alexandra and the subsequent massacre of the patients, doctors and nurses within. Therefore Adnan, according to this theory, could have been the motivating reason for the massacre.

==Versions of Lt Adnans death==
* Version 1 - Official version: The official version as recorded by Japanese Imperial Army indicated that he was executed and then hung upside down from a cherry tree. British accounts have confirmed that his corpse was found hung upside down after the surrender and this has been repeated in a number of authoritative texts on the Malayan campaign. The actual mode of execution was never officially recorded.
* Version 2 - Film version: His death was not shown, but it was indicated in the closing credits that he and the surviving wounded in his company were tied to trees and bayonetted to death. This is probably the more correct version and in keeping with similar Japanese practice elsewhere.
* Version 3 - A version which was shown on a local tabloid magazine, purportedly revealed by Lt Adnans former aide, just before his death. This version could not be verified.

==Criticism==
The film received criticism for using Malay actors to portray Japanese and English soldiers throughout the film. Further, the original English dialogue as spoken by the actors was voiced over by Malays speaking in halting and strongly accented English suggesting that there was an awkward attempt to alter the dialogue to give a different slant to the situations depicted and to portray the British in an unfavourable light.

==External links==
*  
*  

 
 
 
 