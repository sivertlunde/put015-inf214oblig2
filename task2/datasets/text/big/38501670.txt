The Wonderful Chance
{{infobox film
| name           = The Wonderful Chance
| image          =
| imagesize      =
| caption        =
| director       = George Archainbaud
| producer       = Selznick Pictures
| writer         = H. H. Van Loan (original story) Mary Murillo (scenario) Melville Hammett (scenario) Eugene OBrien Martha Mansfield Rudolph Valentino
| cinematography =
| editing        =
| distributor    = Select Pictures
| released       =   reels (5,137 feet)
| country        = United States
| language       = Silent (English intertitles)

}} Eugene OBrien and was directed by George Archainbaud. While this film survives today in several archives, it is best known for featuring Rudolph Valentino in a villain role rather
than the hero. In the 1960s scenes from the film were used in the documentary The Legend of Rudolph Valentino (1961) narrated by Graeme Ferguson.  

==Cast== Eugene OBrien - Lord Birmingham / Swagger Barlow
*Martha Mansfield - Peggy Winton
*Tom Blake - Red Dugan
*Rudolph Valentino - Joe Klinsby
*Joseph Flanagan - Haggerty (as Joe Flanagan)
*Warren Cook - Parker Winton

==References==
 

==External links==
* 
* 
*  available for free download at Internet Archive
 
 
 
 
 
 
 
 


 
 