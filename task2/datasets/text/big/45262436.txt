Missing Millions
{{Infobox film
| name           = Missing Millions
| image          = 
| alt            = 
| caption        =
| director       = Joseph Henabery
| producer       = Adolph Zukor
| screenplay     = Jack Boyle Albert S. Le Vino  David Powell Frank Losee Riley Hatch John B. Cooke William B. Mack George LeGuere
| music          = 
| cinematography = Gilbert Warrenton
| editing        = 
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 drama silent David Powell, Frank Losee, Riley Hatch, John B. Cooke, William B. Mack and George LeGuere. The film was released on September 17, 1922, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Alice Brady as Mary Dawson David Powell as Boston Blackie
*Frank Losee as Jim Franklin
*Riley Hatch as Detective John Webb
*John B. Cooke as Handsome Harry Hawks
*William B. Mack as Thomas Dawson
*George LeGuere as Daniel Regan
*Alice May as Mrs. Regan
*H. Cooper Cliffe as Sir Arthur Cumberland
*Sydney Deane as Donald Gordon
*Beverly Travers as Claire Dupont
*Sidney Herbert as Frank Garber 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 