Kamla (film)
{{Infobox film
| name           = Kamla
| image          = Kamla-poster.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jagmohan Mundhra
| producer       = Jagmohan Mundhra
| writer         = Vijay Tendulkar Vasant Dev  (dialogue) 
| screenplay     = Vijay Tendulkar
| story          = Vijay Tendulkar
| based on       =  
| narrator       = 
| starring       = Shabana Azmi Deepti Naval Marc Zuber A.K. Hangal Tun Tun Sulabha Deshpande
| music          = Bappi Lahiri
| cinematography = Pravin Bhatt
| editing        = Ashok Bandekar 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}}
 1985 Bollywood|Hindi film produced and directed by Jagmohan Mundhra, starring Deepti Naval, Shabana Azmi, and Marc Zuber in lead roles. The film is titled after the character of Naval. 

==Plot==
 Bhil tribe. Like any avid journalist he travels to the village followed by really buying a girl named Kamla (Deepti Naval) and takes her to his home in Delhi. As his intentions were good, some days later he holds a press conference where he reveals the actual wrongdoings going on in the village. 

==Cast==
* Shabana Azmi as Sarita Jadhav
* Deepti Naval as Kamla
* Marc Zuber as Jaisingh Jadhav
* A.K. Hangal as Kakasaab (Saritas uncle)
* Sulabha Deshpande
* Tun Tun

==Critical reception==

Upon its release, Kamla received rave reviews. The Times of India in a review stated, "Deepti Naval is called to widen her eyes, tremble like a scared rabbit and drop pearly smiles. To her credit, she performs these chores competently." The Indian Express wrote, "Mundhara is to be complimented on getting two credible – perhaps notable – performances from Deepti Naval and Marc Zuber – whose talents have so far been pretty well concealed." 

==True Incident Inspiration==
The movie is inspired by a real life expose by the journalist Ashwin Sarin, of The Indian Express. In the expose he actually bought a girl from the rural flesh market, in a village named Shivpuri, Madya Pradesh for an amount of Rs.2,300. He presented the woman at a press conference.


== References ==
 

==External links==
*  

 
 
 
 
 
 
 