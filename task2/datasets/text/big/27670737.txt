The Flower Girl
 
 
{{Infobox Korean name
| img = The Flower Girl opening.jpg
| imgwidth = 300px
| caption = Opening title screen of the 1972 film adaptation.
| context = north
| hangul =    
| hanja = 꽃파는  
| mr = Kkot Panŭn Chŏnyŏ
| rr = Kkot Pa-neun Cheo-nyeo
}}
The Flower Girl is a  : 5대  ), a group of classical, revolution-themed opera repertoires well received within North Korea.   - NK Chosun   2008年03月26日,   - 浙江在线新闻网  It was also made into a novel.   A film adaption of the opera starring Hong Yong-hee was made in 1972. 

==Plot==
The story is set during the 1930s, and is based on the    A poor, rural girl, around whom the plot is centred, picks flowers on the mountain every day to sell at the market, to care for her ill mother. Additionally, she has a blind sister, and her father is deceased. Her mother is in debt to the landlord, and is bankrupt and unable to purchase food. The landlords subordinates frequently harass the girl and call for her to work for them, to which her mother refuses. The girl then finds her blind sister attempting to earn money by singing on the street, to her anger.
 Revolutionary Army, returns home to visit family when he realises that the flower girl has been locked up, and so organises a group of villagers to overthrow the landlord.

==Creation==
According to  , entitled "Anti-Japanese Revolution", notes that:  金日成:与世纪同行，郑万兴译，北京：中国社会科学出版社，1994年. 

 
"There was a time during our countrys independence movement where we held on to our vision to build an "ideal village" concept... At the time, we adopted the Korean students in Jilin to teach village people to sing a large variety of revolutionary songs, such as the Red Flag Song and Revolution Song. In Wujiazi we formed a performance group based at Samsong school led by Kye Yong-chun. It was during this time that I was completing the script for The Flower Girl, which I had started whilst I was in Jilin City. Upon finishing the script, production of the opera began, and we staged the opera in the Samsong school hall on the 13th anniversary of the October Revolution. For many years after liberation, the opera hadnt been performed since, until it was improved and adapted for film, and re-written as a novel, under the guidance of the Organising Secretary (Kim Jong-il) and released in the early 1970s." English translation of memoir is abridged from the original text for conciseness, and is based on the 1994 Chinese-language publication. Original memoir text from the Chinese-language version published in 1994, unabridged:

"有一个时期，我国的独立运动者们抱着要建设一个“理想村”的构想，为实现这个构想从各方面作过努力……当时在吉林文光中学念书的朝鲜学生当中，有几个来自孤榆树和五家子的青年，他们常夸五家子是个好地方。因此，我就开始注意五家子，并决心把这个村改造成革命村。 1930年10月，我从东满来到五家子……当时，我们通过学生，给村里的人教唱了很多革命的歌曲。《赤旗歌》、《革命歌》等革命歌曲，只要到学校去教唱一次，当天就传遍全村。 五家子村有个由我们组织起来的演艺队。在桂永春的领导下，这个演艺队以三星学校为据点，积极开展了活动。 我也着手完成《卖花姑娘》的剧本。这个剧本，我在吉林市就已开始写，并已试演过几次。剧本一完成，桂永春就带领三星学校的戏剧小组成员开始排练。 在十月革命13周年纪念日那天，我们在三星学校礼堂演出了这出歌剧。这出歌剧解放后长期被埋没，到了70年代初，才在党中央组织书记的指导下，由我们的作家、艺术工作者改编成电影、歌剧和小说公诸于世。组织书记为此付出了很大的力量。"

 Alternate English translation provided by   (page 276 of 2161):

"In Wujiazi we had formed an art troupe. This troupe was based at Samsong School and worked successfully under the guidance of Kye Yong Chun. I worked hard to complete the libretto of The Flower Girl which I had begun to write in my days in Jirin and then staged rehearsals for it. Once the libretto was finished, Kye started the production of the opera with the members of the drama group that had been formed at the school. We staged this opera in the hall of the Samsong School on the 13th anniversary of the October Revolution. This opera was not seen on stage for many years after liberation, and then was improved and adapted for the screen, re-written as a novel by our writers and artistes under the guidance of Organizing Secretary Kim Jong Il and presented to the public in the early 1970s. At that time the Organizing Secretary did a lot of work." 
 

Although it is commonly stated that Kim Il-sung was the sole author of the production, many critics in China cast doubts over the reliability of the claim, and suggest that other North Korean writers may have also had some form of interaction in the operas production.

The first official premiere of the opera production was held on November 30, 1972 in Pyongyang, where it was hailed as a great success. 

According to official North Korean reports, in April 1968,    The opera was intended to promote the communist ideology, by incorporating themes such as the class struggle against the bourgeois; 2008.04.17,   - 韩国朝鲜日报中文网络版  such themes were similarly maintained in the film. 

In April 1972, the film adaptation was officially launched. The film was directed by Choe Ik-kyu and the script was written by Pak Hak; Paekdu-san Group was responsible for the production of the film. 

==Reception==
The opera and its film adaptation were both well received in the   was received by Hong Yong-hee during his visit to North Korea.

As of 2008, the opera has been performed over 1,400 times in North Korea and more than 40 other countries,  mostly Eastern Bloc states; other countries include France, Italy, Germany, Algeria and Japan.   The title of the opera and film was known as Blomsterflickan in Sweden, Das Blumenmädchen in the German Democratic Republic, Kvetinárka in Czechoslovakia, and Kwiaciarka in Poland.

In China, the film adaptation of the opera was  , who was earlier responsible in 1958 in the translation of the North Korean film adaptation of  , song lyrics remained in Korean language|Korean. As the film was played in Chinese cinemas during the period of the Cultural Revolution, the movie became immensely popular due to its proletarian revolution-based content, to the point where theaters even adopted a 24-hour screening cycle because of high ticket sales. 

In   ruled that The Flower Girl and six other North Korean films were "not favouring anti-ROK sentiments" in regards to national security laws. 
 North Korean one won banknote, in her role as the flower girl. 

==Footnotes==
 

==See also==
*Sea of Blood
*List of North Korean operas
*Culture of North Korea
*North Korean literature

==References==
 

==Further reading==
*이종석 (1997년). 《조선로동당연구》. 서울: 역사비평사, 54쪽. ISBN 978-89-7696-106-8

==External links==
* 
* 

 
 
 
 
 
 
 