The Frog
 
{{Infobox film
| name =  The Frog
| image = "The_Frog"_(1937).jpg
| image_size =
| caption = Noah Beery, Gordon Harker & Jack Hawkins
| director = Jack Raymond
| producer = Herbert Wilcox
| writer = Ian Hay (adaptation)   Gerald Elliott  (screenplay)
| based on = novel The Fellowship of the Frog by Edgar Wallace
| starring = Noah Beery  Jack Hawkins  Richard Ainley
| music = 
| cinematography = Freddie Young	(as F.A. Young)
| editing = Merrill G. White  (as Merrill White) Frederick Wilson	(as Fred Wilson)
| studio = Herbert Wilcox Productions
| distributor = General Film Distributors  (UK) 
| released = 23 March 1937	(London)  (UK)
| runtime = 75 minutes
| country = United Kingdom English 
| budget =
| gross =
}}
The Frog is a 1937 British crime film directed by Jack Raymond and starring Noah Beery, Jack Hawkins and Richard Ainley.  The film is about the police chasing a criminal mastermind who goes by the name of The Frog. It was based on a novel by Edgar Wallace. It was followed by a loose sequel The Return of the Frog, the following year.

==Cast==
* Noah Beery - Joshua Broad
* Jack Hawkins - Captain Gordon
* Richard Ainley - Ray Bennett
* Vivian Gaye - Stella Bennett
* Gordon Harker - Sgt. Elk
* Esme Percy - Philo Johnson
* Felix Aylmer - John Bennett
* Carol Goodner - Lola Bassano Cyril Smith - PC Balder
* Harold Franklyn - Hagen Gordon McLeod - Chief Commissioner
* Julien Mitchell - John Maitland

==Critical reception==
Britmovie called it a "routine thriller" ;  while British Pictures wrote, "(it) suffers through being an adaptation of a theatre adaptation (by Ian Hay) of the original novel. Some of the exposition is clunky and at times confusing; and the direction needed someone like Walter Forde to make the most of it. Hawkins and Harker, in the roles they played on stage, hold it together."  

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 


 
 