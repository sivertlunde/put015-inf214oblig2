Chaukat Raja
{{Infobox film
| name           = Chaukat Raja - Marathi Movie
| image          = Chaukat Raja.jpg
| image_size     =
| caption        =
| director       = Sanjay Surkar
| producer       = Smita Talwalkar
| writer         =
| narrator       =
| starring       = Dilip Prabhavalkar Smita Talwalkar Sulabha Deshpande
| music          = Anand Modak Sudhir Moghe (Lyrics)
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Marathi
| budget         =
| gross          =
}}
 Marathi film made in 1991. The film is directed by Sanjay Surkar and produced by Smita Talwalkar.  The movie is based on a family who take a mentally challenged person under their wing and encourage him to develop his skills while they deal with their own instabilities. 

==Synopsis==

A happy family consisting of Rajan Ketkar, his wife Meenal and their daughter Rani reside in a society opposite to the chawl where Durgamaushi and her mentally deranged son Nandu stay. One day, Meenal realises that Nandu is her childhood friend and she is filled with compassion for him which doesn’t sit well with Rajan. For his benefit, Meenal admits Nandu into a rehabilitation centre. 

The Ketkars visit the Centre for a function where Nandu upon seeing Meenal after a long time, gets restless and runs away. Durgamaushi is taken ill is hospitalized while Nandu roams the streets of Mumbai. After the police bring him home, Durgamaushi passes away. Nandu is now orphaned and he comes to Meenal for support. Rajan is not ready to accept him and objects strongly, but after noticing his innocence, agrees to adopt him. They encourage his interest in painting for which Nandu attains fame as an artist of international repute.

==Cast==
* Dilip Prabhavalkar as Nandu (Disabled boy)
* Smita Talwalkar as Minal
* Sulabha Deshpande as Usha Aaji (Mother of a disabled boy)
* Dilip Kulkarni as Rajan
* Ashok Saraf Gana
* Nayana Apte Gynecologist
* Nirmiti Savant Colony resident
* Dilip Kulkarni Minal Husband

==Awards==
* 1992 - Maharashtra State Award for Best Actor - Dilip Prabhavalkar - for his portrayal of a disabled boy 

==Soundtrack==
The music is by Anand Modak.  Songs were penned by Sudhir Moghe.  
{| class="wikitable"
! Song 
! Singer/s
! Duration
|-
| Ek Jhoka Chuke Kaaljacha Thoka || Asha Bhosale ||4:41
|- He Jeevan Sundar Aahe || Ravindra Sathe & Anjali Marathe || 4:21
|- Mi Asa Kasa Vegla Vegla || Ashok Saraf  || 4:42
|}

==References==
 

 

 
 

 