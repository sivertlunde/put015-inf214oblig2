Baadada Hoo
{{Infobox film|
| name = Baadada Hoo
| image = 
| caption =
| director = K. V. Jayaram
| writer = Saisuthe
| starring = Ananth Nag  Padmapriya   K. S. Ashwath
| producer = K. S. Narayan Vaidi
| cinematography = B. S. Basavaraj
| editing = P. Bhaktavatsalam
| studio = KSN Movies
| released = 1982
| runtime = 138 minutes
| language = Kannada
| country = India
| budgeBold textt =
}}
 Kannada film directed by K. V. Jayaram and produced by K. S. Narayan. The story is based on the novel of the same name written by Saisuthe.  The film starred Ananth Nag, Padmapriya and K. S. Ashwath in lead roles. 
 Vaidi and it fetched the Third Best Film award at the Karnataka State Film Awards for the year 1981-82.


== Cast ==
* Ananth Nag 
* Padmapriya
* K. S. Ashwath 
* Loknath
* Shivaram
* Dingri Nagaraj
* Advani Lakshmi Devi
* Uma Shivakumar
* Sudharani credited as Sudha Sindhoor Ramakrishna  (guest appearance)

== Soundtrack == Ashwath - Vaidi duo with lyrics by Chi. Udaya Shankar and Prof. Doddarangegowda. 

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 = Hoova Nodu Entha Andavagide
| extra1 = S. P. Balasubramanyam, S. Janaki
| music1 = 
| length1 = 
| title2 = Neenendu Baadada Hoo
| extra2 = S. P. Balasubramanyam, S. Janaki
| music2 = 
| length2 = 
| title3 = Nalivina Baalige
| extra3 = S. P. Balasubramanyam
| music3 =
| length3 = 
| title4 = Aase Noorase
| extra4 = S. Janaki
| music4 =
| length4 = 
| title5 = Prema Nouke
| extra5 = Vani Jayaram
| music5 =
| length5 = 
}}

==Awards== Karnataka State Award for Third Best Film

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 