Kick In (1917 film)
{{Infobox film
| name           = Kick In
| image          =
| caption        =
| director       = George Fitzmaurice
| producer       = Astral Films
| writer         = Ouida Bergere(scenario)
| based on       = play, Kick In by Willard Mack William Courtenay
| music          =
| cinematography = Arthur C. Miller John W. Boyle
| editing        =
| distributor    = Pathé Exchange
| released       = January 14, 1917
| runtime        = 50 minutes
| country        = USA
| language       = Silent
}} William Courtenay.  It is based on the 1914 Broadway play of the same name by Willard Mack. 

==Cast== William Courtenay - Chick Hewes
*Robert Clugston - Benny
*Mollie King - Molly Cary
*Richard Taber - Charlie(*as Richard Tabor)
*Suzanne Willa - Myrtle Sylvester
*John W. Boyle - Commissioner Garvey

==See also== Kick In (1922) Kick In (1931)

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 
 