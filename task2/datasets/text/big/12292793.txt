Bosnia Diaries
{{Infobox film
| name           = Bosnia Diaries
| image          = Diários da Bósnia.jpg
| caption        = Theatrical release poster
| director       = Joaquim Sapinho
| producer       = Maria João Sigalho Amândio Coroado
| screenplay     = Joaquim Sapinho
| based on       =
| starring       = 
| music          = Hugo Alves
| cinematography = Luís Correia
| editing        = Vítor Alves
| studio         = Rosa Filmes
| distributor    = 
| released       = 2005
| runtime        = 77 minutes
| country        = Portugal
| language       = Portuguese Serbian
| budget         = 
}} Portuguese Documentary documentary Film directed by Joaquim Sapinho, produced at Rosa Filmes, which had its world premiere in 2005, at the Pusan International Film Festival.

The film narrates Joaquim Sapinhos experience in Bosnia during and after the Yugoslav Wars.

== Reception == partition with North Korea resulted in a special interest on the film by the festival. The film was part of the official selection of various film festivals, including the Torino Film Festival, and the Portuguese documentary festival DocLisboa. 

== Production ==
Released in 2005, Bosnia Diaries was shot between 1996 and 1998 in Bosnia. During its editing process, Sapinho shot another feature, The Policewoman, which would be released first, in 2003, becoming his second as director. Delaying Bosnia Diaries release to 2005, the two films were post-produced simultaneously. They would influence each other, both in style as in theme, as Bosnia Diaries edition would suffer a complete conceptual change in consequence of the things Sapinho lived and discovered throughout the shooting and editing of The Policewoman, whose, on the other hand, darkness and realism are clearly  echoes of the Bosnian War experienced by Sapinho, which would cause a great change on his vision of the world. 

== Concept ==
The film is a cinematic reflexion on war, on Europe and on civilization, structured in the form of a film-diary, in which Joaquim Sapinho reports his experiences lived in the two times he went to Bosnia. The film is composed of two sets of images, from two different shooting periods. One corresponds to the first time Sapinho went to Bosnia, in 1996, the other to the second time, in 1997. Both sets of images are, nevertheless, shown edited together, being only distinguishable by the somewhat sepia tone of the images from the first trip.

==References==
 

== External links ==
*   (click on "ENG", then on "DIRECTORS", then on "JOAQUIM SAPINHO", then on "BOSNIA DIARIES")
*  

 

 
 
 
 
 
 
 
 


 