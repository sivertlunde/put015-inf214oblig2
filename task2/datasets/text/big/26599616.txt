The Romance of Book and Sword
 
 
{{Infobox film name = The Romance of Book and Sword image = The Romance of Book and Sword.jpg caption = VCD cover traditional = 書劍恩仇錄 simplified = 书剑恩仇录 pinyin = Shū Jiàn Ēn Chóu Lù}} director = Ann Hui producer = Shen Minhui Guo Fengqi story = Louis Cha screenplay = Chun Sai-sam music = Gu Guanren Law Wing-fai cinematography = Bill Wong editing = Chau Muk-leung studio = Sil-Metropole Organisation distributor =  released =   runtime = 94 minutes country = Hong Kong language = Mandarin budget =  gross = HK$5,991,706.00
}} Louis Chas Princess Fragrance, released later in the same month and also directed by Ann Hui. It is also one of the earliest Hong Kong films to be completely shot in mainland China with a full mainland Chinese cast. 

==Plot==
The film covers the first half of the novel and ends with the truce between the Qianlong Emperor and the Red Flower Society at Liuhe Pagoda. The two major subplots in the novel are omitted: Li Yuanzhi and Zhou Qi do not appear in the film.

==Cast==
*Zhang Duofu as Chen Jialuo
**Jiang Wei as young Chen Jialuo
*Chang Dashi as Qianlong Emperor
*Liu Jia as Huoqingtong
*Ding Cuihua as Luo Bing
*Lü Yongquan as Taoist Wuchen
*Yu Dalu as Zhao Banshan
*Guo Bichuan as Wen Tailai
*Wang Jingqiu as Zhang Jin
*Hou Changrong as Yu Yutong
*Chen Youwang as Xu Tianhong
*Ren Naichang as Shi Shuangying
*Zhang Jun as Chang Bozhi
*Wang Wei as Chang Hezhi
*Zheng Jianhua as Jiang Sigen
*Fu Yongcai as Wei Chunhua
*Sun Chenxi as Xinyan
*Wang Hongtao as Yu Wanting
*Wu Chunsheng as Zhang Zhaozhong
*Yang Junsheng as Heshen
*Ding Tao as Zhaohui
*Deng Jie as Chen Jialuos mother
*Ge Lili as Qinghua
*Zhu Yi as Prince Su
*Zhang Xuehao as Li Kexiu
*Wang Wensheng as Long Jun
*Shi Wei as Wet nurse

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 