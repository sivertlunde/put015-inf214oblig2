The Duke (film)
{{Infobox Film
| name = The Duke
| image = 
| caption = 
| director = Philip Spink
| producer =
| screenplay = Craig Detweiler (and story), Anne Vince, Robert Vince,  John Neville James Doohan Courtnee Draper Jeremy Maxwell Oliver Muirhead Judy Geeson
| music = 
| cinematography = 
| editing = 
| production_company = 
| distributor = 
| released = 1999
| runtime = 88 min.
| country = United Kingdom Canada
| awards =
| language = English
| budget = 
| gross = 
}} English country mansion.

==Plot==
When the kind hearted duke of the manor dies, he leaves his estate to his Black and Tan Coonhound, Hubert, with Charlotte the Butlers niece as his guardian. However, two greedy relatives have it in for the dog, as they scheme to take over the manor!

==Credits==

Executive Producers, Anne Vince, Robert Vince

Producer, Ian Fodie

Director, Philip Spink

Story, Craig Detweiler

Screenplay, Craig Detweiler, Anne Vince, Robert Vince,

Story Consultant, Nat Christian

==Trivia==
Although he appeared in a video in 2005, this was the last full-length movie that James Doohan, best known for his role as Scotty in Star Trek, appeared in.

==External links==
*  
*  

 
 
 
 
 
 


 