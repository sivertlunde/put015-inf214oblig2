Disco Dancer
 
{{Infobox film name = Disco Dancer  image = Discodancerfilm.jpg caption = Poster
|image_size= director = Babbar Subhash writer = Dr. Rahi Masoom Reza Deepak Balraj Vij   starring = Mithun Chakraborty Rajesh Khanna Kim Yashpal Om Puri Gita Siddharth Om Shivpuri producer = Babbar Subhash Tilotima Babbar Subhash  music = Bappi Lahiri
|released= 10 December 1982 runtime = 135 minutes studio = B. Subhash Movie Unit cinematography = Nadeem Khan  editing = Mangesh Chavan, Shyam Gupte	 	 language = Hindi country = India 
|}}

Disco Dancer is a 1982 Indian Hindi/Bollywood feature film directed by Babbar Subhash, starring Mithun Chakraborty in the lead role and Rajesh Khanna in a special appearance plays the mentor of the lead hero. The film tells the rags-to-riches story of a young street performer. It is especially known for its filmi disco songs composed by Bappi Lahari and dance of Mithun Chakraborthy. Songs including "I am a Disco Dancer" and "Yaad Aa Raha Hai" (picturized on Mithun and sung by Vijay Benedict and Bappi Lahiri) and "Goro Ki Na Kaalo Ki" (picturised on Rajesh Khanna and sung by Suresh Wadkar with Usha Mangeshkar) became very popular.
 Southern and Eastern and Western Africa, the Middle East, East Asia, Turkey and Soviet Union. It was one of the most successful Indian films in the Soviet Union, drawing an audience of 40 to 63 million viewers there. Disco Dancer established Mithun as a household name in Southern Asia and the Soviet Union. In China, the soundtrack was a success and received an Music recording sales certification|award.
 Disco King with Nandamuri Balakrishna.

==Plot==

Anil (Mithun Chakraborty), a street performer and wedding singer, is scarred by the memory of the rich P.N. Oberoi (Om Shivpuri) beating his mother (Gita Siddharth) in an incident during his childhood. When David Brown, the manager (Om Puri) of current Indian disco is fed up of champion Sam (Karan Razdan) and looks for some new talent, he happens across Anil who is dance|dance-walking down a street. Rebranded as Jimmy, the rising disco star must take the throne from Sam and win the heart of Rita (Kim Yashpal), Oberois daughter. 

All seems to be going well until Oberoi hires men to connect Jimmys electric guitar to 5,000 volts of electricity, causing Jimmys mother to die in a tragic accident. With his legs broken by Oberois goons and guitar phobia from the incident with his mother, Jimmy must claim first place for Team India at the International Disco Dancing Competition amidst strong competition from Team Africa and Team Paris.

==Cast==

* Mithun Chakraborty as Anil / Jimmy
* Kim Yashpal as Rita Oberoi
* Rajesh Khanna as Master Raju
* Om Puri as David Brown
* Om Shivpuri as P.N. Oberoi
* Gita Siddharth as Radha
* Karan Razdan as Sam
* Kalpana Iyer as Nikki Brown
==Production==
The title song "I Am a Disco Dancer" was shot at Natraj Studio in Mumbai over three days, where scenes featuring Mithun Chakrobartys signature moves were filmed. Thereafter, the shooting featuring crowds scenes at Filmistan Studio in Mumbai. 

==Soundtrack==
The music for the movie was directed by Bappi Lahiri. The tracks on the 1982 soundtrack album are as follows:

# "Goron Ki Na Kaalon Ki": Suresh Wadkar & Usha Mangeshkar
# "Auva Auva – Koi Yahaan Nache": Usha Uthup & Bappi Lahiri
# "Ae Oh Aa Zara Mudke": Kishore Kumar
# "Krishna Dharti Pe Aaja Tu": Nandu Bhende
# "I Am a Disco Dancer": Vijay Benedict
# "Jimmy Jimmy Jimmy Aaja": Parvati Khan
# "Yaad Aa Raha Hai" Bappi Lahiri

The song "Auva Auva" was influenced by the 1979 English synthpop hit "Video Killed the Radio Star" by The Buggles. 
{{Listen
| filename    = Disco Dancer - Yaad Aa Raha Hai.ogg
| title       = "Yaad Aa Raha Hai"
| description =  .
| pos         = right
}}
 Charanjit Singh.

The song "Krishna Dharti Pe Aaja Tu" was influenced from "Jesus" by Tielman Brothers. This version was used in the movie where Mithun is practicing dance.
 Russian artists Tibetan artist Kelsang Metok.

==In popular culture==

In 2010, the songs "I Am a Disco Dancer" and "Yaad Aa Raha Hai" were used in the 2010 Bollywood comedy film, Golmaal 3, directed by Rohit Shetty. The songs were relevant to the performance of Mithun Chakrabortys character Pritam, who reflected on his past as a young mega-hit disco dancer. The first song was the inspiration to Devos 1988  "Disco Dancer."

The music from "Jimmy Jimmy Aaja Aaja" was used in the final fight scene in the Adam Sandler film You Dont Mess with the Zohan. 
 Imran Khan Delhi Belly is inspired by Mithun Chakrabortys role in Disco Dancer.

"Jimmy Jimmy Aaja Aaja" and "I Am a Disco Dancer" are very popular in countries such as Mongolia and post-Soviet states such as Russia, Azerbaijan, Ghana, Nigeria and Uzbekistan. 

Baimurat Allaberiyev, an ethnic Uzbek from Tajikistan, became an internet sensation by singing "Goron Ki Na Kalon Ki" and "Jimmy Aaja" in a warehouse. The 2008 video recorded on a mobile phone got over 1 million views on YouTube. He landed an acting role in a Russian comedy film, Six Degrees of Celebration (2010).

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 
 
 
 