Neti Bharatam
{{Infobox film
| name           = Neti Bharatam
| image          = Neti Bharatam poster.jpg
| caption        = Movie poster
| director       = T. Krishna
| producer       = P. Venkateswara Rao
| writer         = T. Krishna   (story)  M.V.S. Harnatha Rao  
  (Dialogues ) 
| narrator       = Suman Nagabhushanam P. L. Narayana Rajyalakshmi S. Varalakshmi
| music          = K. Chakravarthi
| cinematography =
| editing        =
| studio         =
| distributor    =
| released       = 1983
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}
Neti Bharatam is a 1983 film written and directed by T. Krishna and produced by Eetharam Films. It is a musical hit film promoting liberal ideals. Lyrics for the songs were penned by revolutionary poet Srirangam Srinivasa Rao, Adrushta Deepak who was then an unknown lyricist, and music scored by K. Chakravarthy. The movie is most famous for the song "Maanavatvam Parimalinche", which got the lyricist Adrushta Deepak noticed. Suman and Vijayasanthi played lead roles, with Nagabhushanam and Rajyalakshmi in assisting roles.

==Songs==
* Ardharatri Swatantram Andhakaara Bandhuram Angangam Dopidaina Kannatalli Jeevitam Ide Ide Neti Bharatam Bharatamata Jeevitam Bharata Matanu Nenu Bandinai Padiunnaanu (Lyrics: Sri Sri; Music: K. Chakravarthi)
* Chitti Potti
* Dammu Thoti Daggu Thoti Chalijoramoste Atto Podam Rave Manavoori Davakhanaku (Lyrics: B. Krishna Murthy; Music: K. Chakravarthi)
* Manavatvam Parimalinche Manchi Manasuku Swagatam Bratuku Artham Teliyajesina Manchi Manishiki Swagatam (Lyrics: Adrushta Deepak; Music: K. Chakravarthi)

==Awards==
* Filmfare Best Film Award (Telugu) - P. Venkateswara Rao.
* This film won Nandi Award for Best Feature Film.
* K. Chakravarthy won Nandi Award for Best Music Director.
* P. L. Narayana won Nandi Award for Best Supporting Actor.

==External links==
*  
*  

 
 
 
 
 
 

 