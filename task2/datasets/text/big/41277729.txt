Mera Jiwan
 

{{Infobox film
| name           = Mera Jiwan
| image          =  
| image_size     =
| caption        = 
| director       = Bidoo Shukla
| producer       = Hasmukh Kothari
| starring       = Dushyanath, Ambika Johar, Satyendra Kapoor Vidya Sinha
| music          = Sapan Jagmohan
| lyrics         = M. G. Hashmat
| distributor    = Everest Entertainment Pvt. Ltd.
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}

Mera Jiwan ( : My Life) is a Hindi drama film released on January 1, 1976.  Produced by Hasmukh Kothari and directed by Bidoo Shukla. The film stars Dushyanath, Ambika Johar, Satyendra Kapoor and Vidya Sinha. The films music is by Sapan Jagmohan. 

It is a story concerning life, depicting life through the lives of certain characters of the film. It explains to us the meaning of life, whether its lone, devotion or human good.

==Plot==
The story starts in a small village Belapur, situated in deep picturesque interior of country. In this village a sweet girl Sandhya is suffering from a heart disease. Her brother Biharilal takes the girl to a big city hospital. They happen to meet young doctor Anand in this hospital. He is a devoted young man very much interested in medical research. He is in the best books of senior doctors of the hospital.

Sandhya had lost hope but Anand kindles the ray of hope in her mind. Once, while she is sitting sad and forlorn he gives her a flower telling her life though short is yet very sweet. Sandhya is highly impressed, but due to her brothers fear of risking her life, she could not be operated there. They returned. Anand had to face an unexpected tragedy. He was rusticated from medical college for breaking college rules and regulations and his studies were disrupted for three years.

He was going home when he came across. Dr. Tyagi, an old retired doctor who was spending last years of his life attending the sick from village to village. He was like a moving hospital. Dr. Tyagi took Anand along with him, but after few days Tyagis death gave Anand another shock of his life. Now Anand had no future. He was like a lost traveler. Stresses and strains of fortune led him to Belapur where he had to work as a doctor in a small hospital. He was a doctor without degree and was working in the hospital illegally.

He happened to meet Sandhya under a barren tree in the village. Anand remembered how deeply he had studied the case of this girl and how she left the city hospital without being operated. Sandhyas life was like a barren tree in Anands company. She felt a new hope of spring vibrating in her being. One day Sandhya had a severe heart attack. In the village hospital there was no equipment for her heart operation, yet her operation was a must. She was in a very critical condition and she could not be taken to the city due to the long journey. Anand felt Sandhyas life could be saved but other doctors of the hospital were not in the favor of operation being done, owing to lack of equipment.

==Cast==
* Dushyanath
* Ambika Johar
* Satyendra Kapoor
* Vidya Sinha

==Crew==
*Director - Bidoo Shukla
*Producer - Hasmukh Kothari
*Music Director - Sapan Jagmohan
*Lyricist - M. G. Hashmat

==Soundtrack==
The music has been directed by Sapan Jagmohan, while the lyrics have been provided by M.G.Hashmat. 

===Track listing===
{{Track listing
| title1 = Koi Mere Haathon Mein Mehandi Lagaa De | length1 = 4:58
| title2 = Kuch Kaam Na Aaya | length2 = 4:32
| title3 = Yeh Suraj Yeh Chanda Yeh Taare | length3 = 5:15
| title4 = Tera Jogi Aaya, Tu Daal De Bhiksha Pyaar Ki | length4 = 5:24
}}

== References ==
 
 

== External links ==
*  
*  
*  

 
 
 