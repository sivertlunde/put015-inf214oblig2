High and Low (1933 film)
{{Infobox film
| name           = High and Low
| image          = 
| image_size     = 
| caption        = 
| director       = Georg Wilhelm Pabst
| producer       = 
| writer         = Leslie Bush-Fekete
| starring       = Ariane Borg
| music          = 
| cinematography = Eugen Schüfftan
| editing        = Jean Oser
| distributor    = 
| released       = 8 December 1933
| runtime        = 79 minutes
| country        = France
| language       = French
| budget         = 
}}

High and Low ( ) is a 1933 French drama film directed by Georg Wilhelm Pabst.   

==Cast==
* Ariane Borg as (as Olga Muriel)
* Pauline Carton as Seamstress
* Janine Crispin as Marie de Ferstel (as Jeannine Crispin)
* Christiane Delyne
* Jean Gabin as Charles Boulla
* Catherine Hessling as Girl in Love Margo Lion as Mme Binder as la propriétaire
* Peter Lorre as Beggar
* Milly Mathis as Poldi
* Mauricet as M. Binder
* Michel Simon as M. Bodeletz
* Vladimir Sokoloff as M. Berger

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 