Grand Canyon (1991 film)
{{Infobox film
| name           = Grand Canyon 
| image          = Grand canyon poster.jpg
| caption        = Theatrical release poster 
| writer         = Lawrence Kasdan Meg Kasdan 
| starring       = Danny Glover Kevin Kline Steve Martin Mary McDonnell Mary-Louise Parker Alfre Woodard 
| director       = Lawrence Kasdan 
| producer       = Michael Grillo Lawrence Kasdan Charles Okun 
| music          = James Newton Howard 
| cinematography = Owen Roizman 
| editing        = Carol Littleton 
| distributor    = 20th Century Fox
| released       =   
| runtime        = 137 minutes 
| country        = United States
| language       = English 
| gross          = $40,991,329 (worldwide)   Box Office Mojo. Accessed Dec. 23, 2011. 
}}
 drama film The Big Chill for the 90s", in reference to an earlier Kasdan film.

==Plot== Lakers basketball muggers when his car breaks down in a bad part of Los Angeles late at night. The muggers are talked out of their plans by Simon (Danny Glover), a tow truck driver who arrives just in time. Mack sets out to befriend Simon, despite their having nothing in common.
 producer of adopt her. profits after steal his watch, vowing to devote the remainder of his career to eliminating violence from the Film|cinema.

The film chronicles how these characters—as well as various acquaintances, co-workers and relatives—are affected by their interactions in the light of life-changing events. In the end, they visit the Grand Canyon on a shared vacation trip, united in a place that is philosophically and actually "bigger" than all their little separate lives.

==Cast==
*Kevin Kline as Mack
*Danny Glover as Simon
*Steve Martin as Davis
*Mary McDonnell as Claire
*Mary-Louise Parker as Dee
*Alfre Woodard as Jane
*Jeremy Sisto as Roberto
*Tina Lifford as Deborah Patrick Malone as Otis
*Randle Mell as The Alley Baron
*Sarah Trigger as Vanessa Twin sisters Candace and Lauren Mead as Abandoned baby Shaun Baker as Rocstar

===Additional cast===
*Marlee Matlin plays an uncredited role; she is a mom using sign language to communicate with her child as several youths are being sent off to camp.
*Randle Mell, real-life husband of Mary McDonnell, plays a homeless man that Claire encounters while jogging.
*Marley Shelton can be seen in one of her first roles as Robertos girlfriend at camp.
*Director Lawrence Kasdan plays an uncredited role; he is berated by Davis for cutting out a "money shot" (a close-up of blood and brains hitting a window) from a particularly violent action scene in one of Davis movies.

==Production==
* The footage of the   (Jan. 10, 1992).  
*The character Davis is based on aggressive action film producer Joel Silver. 

==Reception==

===Critical response===
Grand Canyon received generally positive reviews from critics; it has a 7/10 "fresh" rating at Rotten Tomatoes and a critical rating of 81% based on 32 reviews.   Janet Maslin of The New York Times wrote,
 

In a similar vein, Washington Post critic Rita Kempley wrote, 
 

Owen Gleiberman of Entertainment Weekly chided the film for its "... solemn zeitgeist chic," and called it "... way too self-conscious," but ultimately decided that "Grand Canyon is finally a very classy soap opera, one that holds a generous mirror up to its audiences anxieties. Its the sort of movie that says: Life is worth living. After a couple of hours spent with characters this enjoyable, the message — in all its forthright sentimentality — feels earned." 

Film critic Roger Ebert gave the film four out of four stars, and wrote, "In a time when our cities are wounded, movies like Grand Canyon can help to heal."  Eberts television reviewing partner Gene Siskel also loved the film,  with Ebert placing it at the #4 and Siskel at #6 on their 1991 top ten lists. 

===Awards===
The film won the Golden Bear for Best Film at the 42nd Berlin International Film Festival.    The screenplay was nominated for the Oscar (as Best Original Screenplay), the Golden Globe and the Writers Guild of America.

===Box office===
Grand Canyon was considered a minor failure at the box office,  taking in $40.9 million,   and did not reap notable profits until it was released on video. 

==Legacy==
Phil Collins 1993 song "Both Sides of the Story" references the scene from Grand Canyon where the young mugger tells Simon (played by Danny Glover) that he carries a gun to make sure people respect (and fear) him. 

==See also==
* 1992 Los Angeles riots
* Crash (2004 film)|Crash

==References==
 

==External links==
 
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 