Warlords of the 21st Century
{{Infobox film
| name            = Battletruck
| image           = BattletruckMoviePoster.jpg
| caption         = Official movie poster  (New Zealand version)  Harley Cokliss
| producer        = Lloyd Phillips Rob Whitehouse Harley Cokliss James Wainwright Bruno Lawrence John Bach Randy Powell John Ratzenberger
| cinematography  = Chris Menges
| narrator        =
| music           = Kevin Peek
| editing         =
| studio          = Battletruck Films Ltd.
| distributor     = New World Pictures
| released        = April 1982
| runtime         = 91 min.
| country         = United States/New Zealand
| language        = English
| budget          =
| gross           = $3,000,000 USD ($ }} in todays currency)
}}
Battletruck (also known as Warlords of the 21st Century in New Zealand and Destructors in Italy) is a 1982 New Zealand science fiction film directed by Harley Cokeliss (credited under his birth name Harley Cokliss) and starring Michael Beck.
 Hollywood release.

== Plot ==
All remaining reserves of petroleum have finally run out; forcing people to ration the few remaining containers of gasoline left on the earth against mercenaries and warlords. Straker and his men (accompanied by his daughter Corlie - played by Annie McEnroe) find a vast supply of diesel fuel in a compound once thought to be radioactive. When Corlie refuses to execute the previous owners, she runs away from base camp. Hunter (Michael Beck), on his amazing (possibly ex-future-military) motorcycle, rescues the girl and takes her to his farm. After keeping her on the farm on a temporary basis, Hunter sends her off to live in a walled city (known in-film as Clearwater Farm) governed by a strict old-fashioned democracy (similar to the Religious Society of Friends|Quakers) where she is quickly accepted by the community. However, she is soon discovered by the mercenaries commanded by her father, Colonel Straker (James Wainwright), who moves to attack the Clearwater community.  In the chaos that ensues, Corlie manages to escape back to Hunters remote hideout.  Straker terrorises the residents of Clearwater, taking their weapons, medicines, and other supplies and handing the women over to his men.

Within a short time, one of the residents of Clearwater betrays the Clearwater mechanic/fabricator, Rusty, who knows the secret location of Hunters hideout.  Straker & Co. use torture to get the information out of Rusty, then move in to attack Hunters base and recapture Corlie.  Hunter and Corlie escape on his bike, Straker, in a minor rage, plows through Hunters place with the truck.  Hunter takes Corlie back to the Clearwater people and asks Rusty to build him an armored car to attack Strakers "battle truck".  While Rusty and Hunter and a few others are thus occupied, the traitor who betrayed Corlie before knocks her out, puts her in a wagon and heads out to deliver her back to Straker.  Hunter tries to stop him, but the traitor sets an ambush for him and wounds him with a crossbow.  Believing that he has killed Hunter, he appears at Strakers HQ with Corlie in the wagon and very pleased with himself.  Meanwhile, Hunter regains consciousness and manages to limp back to Clearwater on his bike.  While getting patched up there, Rusty finishes the armored car and shortly Hunter takes off in it, despite the fact that he is wounded.  He attacks Strakers HQ, plowing through buildings and tents and eventually dropping a grenade into Strakers 50,000 litre diesel supply.  He then runs and Straker, now in a towering rage, takes off after him.  In the process, he forces the driver to overdo it in the truck, overheating the turbines.  This stresses out the driver, (who loves the truck), and leads to dissension between him and Straker.  Hunter meanwhile gets some distance ahead, jumps out of the car and climbs to a high place overlooking the road and it is now revealed that the whole attack on Strakers HQ was a ruse to lure the truck into an ambush.  The Clearwater people are at the high place waiting for Hunter with his motorcycle and a rocket launcher which Hunter had given them earlier in the movie.  Hunter fires a couple of rockets at the truck, one causes slight damage and a small fire, which causes more stress between Straker and the truck driver.  The driver attempts to kill Straker, who he feels is uselessly destroying the truck, Straker kills the driver, who slumps over the wheel and now the truck, throttles set to full, is more or less out of control.

Back on the bike again, Hunter manages to jump onto the truck through a hole in the top that one of his rockets had made.  A gun battle/slugfest ensues, the truck still careening wildly back and forth while Corlie tries to control it with the body of the driver slumped over the wheel and Straker furiously shouting commands to everybody.  Eventually, Hunter fights his way to the front, temporarily stuns and maybe blinds Straker, grabs Corlie, and jumps with her from the back of the still wildly out of control "battle truck" (leaving his bike on the truck).  Now Straker is the last one alive on the truck, still screaming, making threats, and bumbling about in the smoking ruins of the interior.  Finally, the truck, which has long since left the road and has been roaming wildly across the open desert, goes over a cliff and rolls over and over and over in a dramatic, slow-motion crash scene.  The truck explodes and is well and truly destroyed, (shedding axles and other heavy bits all the way down), and part of it ends up in a lake at the bottom.

Hunter and Corlie end up back at Clearwater, where Corlie apparently settles for good as part of the community.  Ever the loner, Hunter rides off into the sunset on a horse, promising Corlie that hell be back "sometime".

== Production Notes ==
Battletruck was filmed on the Central Otago plains in New Zealand. Despite being produced by a Hollywood studio and being considered a Hollywood release, the film largely used New Zealand crew and actors. It followed the success of films such as Mad Max and was made in New Zealand in part due to the Hollywood writers strike.   

==References==
 


== External links ==
*  
*   at New Zealand Feature Film Database
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 