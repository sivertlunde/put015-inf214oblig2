Julian Bond: Reflections from the Frontlines of the Civil Rights Movement
{{Infobox film
| name           = Julian Bond: Reflections from the Frontlines of the Civil Rights Movement
| image          = Julianbondmovie.jpg
| caption        = Julian Bond by Eduardo Montes-Bradley
| director       = Eduardo Montes-Bradley
| producer       = Soledad Liendo
| writer         = Eduardo Montes-Bradley
| starring       = Julian Bond
| music          = Various
| editing        = Eduardo Montes-Bradley
| distributor    = Filmakers Library / Alexander Street Press
| released       =  
| runtime        = 34 minutes
| country        = United States
| language       = English
}}
Julian Bond: Reflections from the Frontlines of the Civil Rights Movement is a 2012 documentary film, a portrait of social activist and former Georgia legislator Julian Bond approaches the story of the Civil Rights Movement in the United States from a personal perspective. “Bonds father was the first African-American president of Pennsylvanias Lincoln University, and the family hosted black luminaries in education and the arts, but Bond recalls growing up in the era of "separate but equal" laws”.  Bond also talks about his early involvement with the Civil Rights Movement, his nomination at the age of 28 for vice president of the United States, and the Georgia legislatures efforts to prevent him from being seated as a representative on the grounds that he had not supported the Vietnam War. The film explores the 1963 March on Washington, Martin Luther King Jr., the assassinations of King and John F. Kennedy, and Lyndon B. Johnsons impact on U.S. race relations. Bond also offers his own insights, and adds some personal revelations, such as the fact that he was a published poet during his college years. The film closes with a montage of major African-American figures from Frederick Douglass and Karl Marx, to Abraham Lincoln and Spike Lee. Julian Bond, premiered at the Virginia Film Festival on November 4, 2012. "Les boîtes ouvertes de l’Amérique numérique. Aveux d’un documentariste indocile" Revue Annuelle de Lassociation Rencontres Cinémas DAmerique Latine de Toulouse. Toulouse, France. Issue Number 21. p. 171    

==Synopsis== Sixth & I Synagogue in Washington, D.C., along with the last few lectures that he delivered, as a member of faculty, at the University of Virginia in May 2012. The interviews are bolstered by a barrage of photographs and archival footage taken from different sources. These images help define and illustrate the different historical eras beginning with the American Civil War and running up to the 2008 US presidential election. 

Through interviews and archival footage, the film documents Bonds life and, in particular, the role he played in the civil rights movement.  The first part of the film concentrates on the historical factors that led to the March On Washington in August 28, 1963. These factors are brought to light through the telling of the sagas of Bonds grandfather, James Bond—a man born in slavery who went on to graduate from Berea College and Oberlin College—and Jane Arthur Bond, Julians great-grandmother. Julians father, Horace Mann Bond, one-time president of Lincoln College in Pennsylvania, is also featured. The family-centered segment of the film is illustrated with photos from the Bond family albums that were loaned to the producers by members of the Bond family.

The second act begins with the March on Washington and Bonds entrance into politics at age 23, and concludes with his manifest opposition to the Vietnam War.

The conclusion of the film begins by showing Bonds formal acceptance as an elected representative in the Georgia House of Representatives, after finally winning a three-year court battle against the legislative body that had originally refused him his seat due to positions he had taken on issues relating to the Vietnam War. This is followed by segments that show Bonds nomination for Vice President of the United States at the 1968 Democratic National Convention in Chicago; his failed attempt to obtain the nomination for the presidency in 1976; and a succession of events leading to the 2008 presidential election when Barack Obama became the first African American president of the United States.

According to Giles Morris of the C-Ville weekly, the gems of the film are the "off-guard moments" when Julian Bond explains to Montes-Bradley how Jim Crow was perceived by a child in the early 1950s: "I knew this was a condition. I couldnt understand who made it happen, who was in charge of it, what it really meant, but I knew there was a difference between myself and the other people I saw whose skin was not the same color," Bond says. 

==Filming locations== Sixth & I Synagogue in Washington D.C., and at the University of Virginia in Charlottesville. Additional filming was done at the Lincoln Memorial in Washington, D.C. 

==Film screenings==
* Virginia Film Festival. Premiered on November 4, 2012 at Nau Auditorium on the campus of the University of Virginia.  The Paramount Theatre in Charlottesville, Virginia|Charlottesville, VA. 
* The Jefferson-Madison Regional Library | The Crozet Library. February 27, 2014. Black History Month - Special guest Julian Bond and director Eduardo Montes-Bradley. 

==See also==
* African-American Civil Rights Movement (1954–68) in popular culture

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 