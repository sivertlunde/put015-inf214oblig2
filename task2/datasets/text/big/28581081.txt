Dropout (1970 film)
{{Infobox film
| name = Dropout
| image =Dropout (1970 film).jpg
| caption = Promotional poster
| director = Tinto Brass
| producer = Franco Nero
| writer = Tinto Brass Roberto Lerici Franco Longo
| starring = Franco Nero Vanessa Redgrave Gigi Proietti Frank Windsor
| music = Don Fraser
| cinematography = Silvano Ippoliti
| editing = Tinto Brass
| distributor =
| released =  :  February 22, 1971 United States:  January, 1975
| runtime = 109 min
| country = Italy Italian English English
| budget =
| gross =
}}
Dropout is a 1970 Italian romantic drama directed by Tinto Brass. It stars real-life couple, Franco Nero and Vanessa Redgrave. They also worked with Brass a year later on the drama, La vacanza. Dropout was released in France on December 18, 1970, followed by a theatrical release in Italy on February 22, 1971.

==Plot==
Mary (Redgrave) is a disillusioned English banker’s wife who meets a troubled Italian immigrant, Bruno (Nero).    Mary is captivated by Bruno and they set off on a voyage together. In the course of their voyage, they meet a series of societys dropouts; the unemployed, drug addicts, drag queens, alcoholics and anarchists. They both learn a great deal about life from these misfits.   Tinto Brass Collection. Retrieved on August 27 

==Cast==
*Franco Nero as Bruno
*Vanessa Redgrave as Mary
*Gigi Proietti as Cieco
*Frank Windsor
*Carlo Quartucci
*Gabriella Ceramelli
*Patsy Smart Darcus
*Giuseppe Scavuzzo
*Mariella Zanetti
*Zoe Incrocci
*Sam Dorras

==Production==
Carlo Ponti had originally agreed to produce the film but when he pulled out, Brass, Nero and Redgrave decided to cover the production costs themselves. Subsequently shooting commenced on 1 June 1970. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 


 