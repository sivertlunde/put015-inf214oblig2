The Postman from Longjumeau
{{Infobox film
| name           = The Postman from Longjumeau
| image          = 
| image_size     = 
| caption        = 
| director       = Carl Lamac
| producer       = Moritz Grünstein   Ernst Schmid-Arens
| writer         = Julia Serda   Karl Peter Gillmann   Charles Amberg
| narrator       = 
| starring       = Carl Esmond Rose Stradner Alfred Neugebauer   Thekla Ahrens
| music          = Anton Profes    
| editing        = Else Baum   Erwin Dressler
| cinematography = Eduard Hoesch 
| studio         = Atlantis-Film   Thekla-Film
| distributor    = Hammer-Tonfilm
| released       = 14 January 1936
| runtime        = 95 minutes
| country        = Austria   Switzerland
| language       = German
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    =  Swiss musical musical comedy film directed by Carl Lamac and starring Carl Esmond, Rose Stradner and Alfred Neugebauer. The film is known by several alternative titles including Der König lächelt - Paris lacht (The King Smiles - Paris Laughs). It is loosely based on the 1836 opera Le postillon de Lonjumeau by Adolphe Adam. In Eighteenth Century France a Postilion from Longjumeau is summoned by  Madame de Pompadour to sing in her opera company, forcing him to be separated from his wife.

==Cast==
*  Carl Esmond - Chapelou, der Postillon von Lonjumeau 
* Rose Stradner - Madelaine 
* Alfred Neugebauer - König Ludwig XV 
* Thekla Ahrens - Marquise de Pompadour 
* Leo Slezak - Graf de Latour 
* Lucie Englisch - Lucienne 
* Rudolf Carl - Bijou 
* Hans Thimig - Pierre Touche, Dorfbarbier 
* Richard Eybner - Marquis de Corcy 
* Fritz Imhoff - Bürgermeister 
* Joe Furtner - Faviere, Corcys Sekretär 
* Tibor Halmay - Balletmeister 
* Carl Hauser - Wachtmeister 
* Irmgard Alberti - Alte Frau mit dem Liebestrank 
* Hans Heinz Bollmann - Singer

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 