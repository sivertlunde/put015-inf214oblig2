The Little Boss
{{Infobox film
| name           = The Little Boss
| image          = The Little Boss (1919) - 1.jpg
| alt            = 
| caption        = Bessie Love in The Little Boss David Smith
| producer       =  
| screenplay     = Rida Johnson Young
| story          = Rida Johnson Young
| starring       = {{Plain list|
*Bessie Love
*Wallace MacDonald}}
| music          = 
| cinematography = Clyde De Vinna   
| editing        = 
| studio         = Vitagraph Studios 
| distributor    =  
| released       =    reels 
| country        =  Silent (English intertitles)
| budget         = 
| gross          =  
}} silent Romantic romantic comedy David Smith, with a story and screenplay by Rida Johnson Young.    It stars Bessie Love and Wallace MacDonald.

==Production==
Exterior scenes were filmed at the Little River Redwood Company, an actual lumber camp in Eureka, California.     Scenes with log flumes were filmed in Fresno, California.  

==Plot==
Peggy (Love) is the owner of a lumber camp, and she falls for Clayton, a man from the city (McDonald), who comes to the camp. Claytons sister invites Peggy to come to the city, where she attends school, and becomes a "modern woman." When Peggy returns to the camp, it is revealed that she was never the true owner of the lumber camp, but this does not matter to Clayton, who is love with Peggy. 

==Cast==
*Bessie Love as Peggy, The Little Boss     
*Wallace MacDonald as Clayton Hargis
*Otto Lederer as Sandy MacNab
*Harry Russell as Red ORourke
*J. Morley as Richard Leicester
*Joe Rickson as Pete Farley
*Clara Knight as Chloe
*Karl Formes as Old Farley

==Release== Burton Holmes Travelogue and the comedy Taking a Chance in some theaters;  The Heart Punch and The Little Widow were shown in others.   

==Reception==
Clyde De Vinnas photography of the redwood forests was highly praised, although the overall reception of the film was negative. 

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 