Devata (1941 film)
 
{{Infobox film
| name           = Devata
| image          = Devatha 1941.JPG
| image_size     =
| caption        = Poster of Devata 1941 film
| director       = B. N. Reddi
| producer       = Moola Narayana Swamy B. N. Reddi
| writer         = Samudrala Raghavacharya K. Ramnoth
| narrator       = Kumari Mudigonda Lingamurthy C. H. Narayana Rao Tanguturi Suryakumari
| music          = Chittor V. Nagaiah
| cinematography = K. Ramnoth
| editing        =
| studio         = Vauhini Studios
| distributor    =
| released       = 1 January 1941
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Devata is a 1941 Telugu drama film directed by Bommireddy Narasimha Reddy|B. N. Reddi. Huge success of this film lead to the making of more such films with the same title in 1964 by B. Padmanabham and in 1982 by D. Ramanaidu. 

==The plot==
Venugopala Murthy returns to the native village from England after completing Bar-at-Law. His mother Mangamma and sister Seetha are very happy. Venu is attracted towards Lakshmi, daughter of Venkaiah, who works as servant in the family. He takes advantage of the sleeping Lakshmi and promises to marry her. He leaves to city and stays with his uncle Balaramaiah. Venu marries his daughter Vimala. Mangamma and Seetha brings pregnant Lakshmi to the city. Venu refuses to accept her and offers some money to Lakshmi. She leaves the house when her father arranges for the marriage. She take refuge with Haridasu family and delivers a child. Meanwhile Vimala elopes with Sukumar. Mangamma sends Venu to bring back Lakshmi. After several hardships, Lakshmi reunites with Venu.

==Cast==
* Chittor V. Nagaiah  as Venugopala Murthy Kumari  as Lakshmi, daughter of Venkaiah
* Mudigonda Lingamurthy as Venkaiah   
* Bezawada Rajarathnam as Vimala, daughter of Balaramiah
* Goberu Subba Rao as Balaramaiah, uncle of Venu   
* C. H. Narayana Rao as Sukumar
* Tanguturi Suryakumari as Seeta, sister of Venu
* Master Ashwathama as Rangadu
* Parvathi Bai as Mangamma, mother of Venu
* R. Satyanarayana

==Soundtrack==
There are about 14 songs in the film. The lyrics are written by Samudrala Raghavacharya and music score is provided by Chittor V. Nagaiah. 
# "Adigo Andiyala Ravali" - Bezawada Rajaratnam
# "Bhajane Modajanakamura" - G. Vishweswaramma and Suryakumari
# "Ee Vasanthamu Nityamu Kadoyi" - M. S. Rama Rao
# "Enno Nomulu Nochinagani" - G. Vishweswaramma
# "Evaru Makinka Saati" - Bezawada Rajaratnam
# "Jagela Verapela Travumu" - Bezawada Rajaratnam
# "Kroora Karmamulu Neraka Chesiti" - G. Vishweswaramma and Suryakumari
# "Lokamantha Lobhule Kanare" - Ashwathama
# "Nijamo Kado Yamuna Thatilo" - Bezawada Rajaratnam
# "Oogeda Uyyala" - Tanguturi Suryakumari
# "Rade Cheli Nammarade Cheli" - Bezawada Rajaratnam
# "Raitu Janamula Panduga" - Tanguturi Suryakumari group
# "Rave Rave Bangaru Papa" - Nagaiah, Kumari, Suryakumari
# "Vendi Kanchalalo" - Tanguturi Suryakumari

==References==
 

==External links==
*  

 
 
 
 