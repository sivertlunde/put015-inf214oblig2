Dust (2001 film)
{{Infobox film
| name = Dust
| image = Dust2.jpg
| caption = DVD cover
| director = Milcho Manchevski
| producer = Chris Auty Vesna Jovanoska Domenico Procacci
| writer = Milcho Manchevski
| starring = Joseph Fiennes David Wenham Rosemary Murphy Vera Farmiga Adrian Lester Anne Brochet
| music =  Kiril Džajkovski
| cinematography = Barry Ackroyd
| editing = Nicolas Gaster
| distributor = Lionsgate Films Madman Entertainment Pathé
| released =  
| runtime = 127 minutes
| country = Macedonia United Kingdom English Macedonian Macedonian
| budget = 
}} Western drama film, written and directed by Milcho Manchevski, and starring Joseph Fiennes, David Wenham, Adrian Lester, Anne Brochet, Vera Farmiga and Rosemary Murphy.   The film premiered at the Venice Film Festival on August 29, 2001.

==Plot==
Shifting periodically between two parallel stories, Dust opens in present-day New York City with a young criminal, Edge (Adrian Lester), being confronted at gunpoint by an ailing old woman, Angela (Rosemary Murphy), whose apartment he is attempting to burglarize. While he awaits an opportunity to escape, she launches into a tale about two outlaw brothers, at the turn of the 20th century, who travel to Ottoman Empire|Ottoman-controlled Macedonia (region)|Macedonia. The two brothers have transient ill will between them, and they become estranged when confronted with a beautiful woman, Lilith (Anne Brochet).
 
In the New York storyline, Edge hunts for Angelas gold to pay back a debt, and gradually grows closer to her. In the Macedonian story, the brothers end up fighting for opposite sides of a revolution, with the religious Elijah (Joseph Fiennes) taking up sides with the Ottoman sultan and gunslinger Luke (David Wenham) joining "the Teacher" (Vlado Jovanovski), a Macedonian rebel. 

==Cast==
* Joseph Fiennes as Elijah
* David Wenham as Luke
* Adrian Lester as Edge
* Anne Brochet as Lilith
* Rosemary Murphy as Angela
* Vera Farmiga as Amy Matt Ross as Stitch

==Production==

===Development and filming===
The film was written and directed by Milcho Manchevski, and produced by Chris Auty, Vesna Jovanoska and Domenico Procacci. The music for the film was composed by Kiril Džajkovski. Principal photography took place in a number of countries and locations, including Cologne, Germany, New York City, United States, and Republic of Macedonia|Macedonia. 

===Release===
Dust opened at the Venice Film Festival on August 29, 2001, and was later released in Italy on April 5, 2002.  Pathé distributed the film in the United Kingdom on May 3, 2002. In Spain, the film was released on July 12, 2002 by Alta Classics. It was given a limited release in the United States on August 22, 2003, where it was distributed by Lionsgate Films. 

==Reception==

===Critical response=== film critics. On the review aggregator website Rotten Tomatoes, the film holds a 21% rating based on 14 critical reviews.  David Stratton of Variety (magazine)|Variety, gave the film a poor review, writing: "Essentially a Euro Western, spectacularly lensed in Macedonia   film borrows freely and unwisely from superior predecessors in the genre, while struggling to explore interesting themes involving the personal legacy we hand down to our descendants.   main problem in positioning itself commercially is that it straddles the genres: Its too arty to cut it as a violent action pic and too gore-spattered to appeal to the arthouse crowd." 

  of  s stylized western, Dust, is a potent, assured and ambitious piece of filmmaking brought down by weighted dialogue and, playing Americans, the British actors Adrian Lester and Joseph Fiennes and the Australian David Wenham. This dazzling and dazed movie begins on the streets of contemporary New York, as a camera moseys down a street and then crawls up the side of a building, peering into several windows as various apartment dwellers play out their lives. Its as if Mr. Manchevski were thumbing through a selection of stories as we watch, deciding which appeal to him the most."  

===Accolades===
{| class="wikitable sortable"
|-
! Year
! Award
! Category
! Recipient(s)
! Result
|-
| 2004 Golden Reel Award
| Best Sound Editing in a Foreign Feature Film
| Peter Baldock, Jack Whittaker, Philip Alton, Tim Hands, Daniel Laurie, Richard Todman
|  
|}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 