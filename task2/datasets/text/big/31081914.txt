Crime Without Passion
{{Infobox film
| name           = Crime Without Passion
| image_size     =
| image	         = Crime Without Passion FilmPoster.jpeg
| caption        =
| director       = Ben Hecht Charles MacArthur
| producer       = Ben Hecht Charles MacArthur
| writer         = Ben Hecht Charles MacArthur
| narrator       = Margo Whitney Bourne
| music          =
| cinematography = Lee Garmes
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70 minutes
| country        = United States English
| budget         =
| gross          =
}} American drama film directed by Ben Hecht and Charles MacArthur, starring Claude Rains. It is the first of four pictures written, produced and directed by Hecht and MacArthur for Paramount Pictures. Sixty to seventy percent of the film was directed by cinematographer Lee Garmes. 

The plot centers around a clever and suave but unscrupulous and dishonest lawyer, Lee Gentry (Rains) who boasts that he "lives by lies". His attempts to finish his affair with a clinging, besotted cabaret artist do not go according to plan. Rains is on top form and turns in a marvellously wicked performance.

==Cast==
* Claude Rains as Lee Gentry Margo as Carmen Brown
* Whitney Bourne as Katy Costello
* Stanley Ridges as Eddie White
* Leslie Adams as District Attorney OBrien

:Uncredited:
* Fanny Brice as Extra
* Helen Hayes as Extra
* Esther Dale as Miss Keeley
* Greta Granstedt as Della
* Paula Trueman as Buster Malloy

==Bibliography==
* EAMES, John Douglas, The Paramount Story, London: Octopus Books, 1985
* MATTOS, A. C. Gomes de, Hollywood Anos 30, Rio de Janeiro: EBAL - Editora Brasil-América, 1991  

==External links==
* 
*   montage sequence of Crime Without Passion, special effects by Slavko Vorkapich

 
 

 
 
 
 
 
 
 
 
 


 