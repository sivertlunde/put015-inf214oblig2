The Evolved
  UK in American distribution The Toxic Avenger. The film is notable for its outrageous content that includes violence presented comically. Notable also are the high production values for a film produced for only £1000. It is rumoured that the director, Andrew Senior, decided to cast a ventriloquists dummy in the lead female role in order to save money  . There is extensive use of masks and other characters include a talking fetus and a shark monster.  The film was a great success when it was screened in the film market at Cannes 2006.  An enormous hit with the audience at the Lausanne Underground Film Festival in Switzerland, October 2006, with a sold out screening and people turned away unable to get in.  The film will also show at the prestigious Gothenburg Film Festival in Sweden, January 2007, a major coup for a bizzarro underground movie and a first for this type of film at the festival.   The team are said to be in early production of a second feature film.  Initially titled The Beefeaters the two heros of the title must save England from Welsh pop sensation Tom Jones plan to destroy the country.  The movie is also available on Xbox 360 in downloadable movie content.  It features a preview including the "evil puppet" character.

*Cast:
**Jamie D. Allen ....  Lame0
**Michael Down   ....  Danny Glover
**Ellen Mellon   ....  Herself
**John Turner    ....  Sonny Boy

*Directed by:
**Andrew Senior
**John Turner

  
*Writing credits:
**Phillip Barron
**Andrew Senior

*Produced by:
**Rust Z. Fortune

*Original Music by: 
**Wrong Animal

  
*Film Editing by:
**Ace Maroon

== See also ==
*List of Troma films

==External links==
* 

 
 
 