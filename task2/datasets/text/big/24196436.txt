Barrister Parvateesam (film)
{{Infobox film
| name           = Barrister Parvateesam
| image          =
| image_size     =
| caption        =
| director       = H. M. Reddy
| producer       =
| writer         = Mokkapati Narasimha Sastry
| narrator       =
| starring       = Lanka Satyam G. Varalakshmi
| music          = Kopparapu Subba Rao
| cinematography =
| editing        =
| studio         = Motion Pictures Producers Combines
| distributor    =
| released       = 7 August 1940
| runtime        =
| country        = India
| language       = Telugu
| budget         =
}}

Barrister Parvateesam is a 1940 Telugu comedy-drama film directed by H. M. Reddy. It is based on the Telugu novel Barrister Parvateesam (1924) written by Mokkapati Narasimha Sastry. 

==Casting==
The title role of Parvateesam is played by Lanka Satyam.  , Mokkapati Part 2-I  This was his second film. His first was Amma, directed by Niranjan Pal. He worked as assistant to director R. Prakash. G. Varalakshmi got the female lead role unexpectedly. Director Prakash saw her, when she was acting in the stage play Sakkubai in Rajahmundry, called her for a make-up test, and gave her the role of wife of Parvateesam. She was 12 years old. She sang two songs in the film. Kasturi Sivarao did two roles as a dentist and a rickshaw puller.  , plot line discussion of Barrister Parvatesam 

==Reception==
Barrister Parvateesam was a flop at the box-office as it could not live up to the expectations of the audience owing to the popular, positive reception of the novel.

==References==
 

==External links==
*  
 
 
 
 
 


 
 