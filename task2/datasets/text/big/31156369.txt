Panitheeratha Veedu
{{Infobox film 
| name           = Panitheeratha Veedu
| image          = Panitheeratha Veedu.jpg
| caption        =
| director       = KS Sethumadhavan
| producer       =
| writer         = Parappurathu
| screenplay     = Parappurathu
| starring       = Prem Nazir Nanditha Bose Roja Ramani Jose Prakash
| music          = M. S. Viswanathan
| cinematography = Melli Irani
| editing        = TR Sreenivasalu
| studio         = Chithrakalakendram
| distributor    = Chithrakalakendram
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by KS Sethumadhavan . The film stars Prem Nazir, Nanditha Bose, Roja Ramani and Jose Prakash in lead roles. The film had musical score by M. S. Viswanathan.    It is an adaptation of Parappuraths 1964 novel of the same name. It won the Kerala State Film Awards for Best Film, Best Direction, Best Screenplay and Best Singer (Jayachandran).  It also won the National Film Award for Best Feature Film in Malayalam.

==Cast==
 
*Prem Nazir as Jose
*Nanditha Bose as Rachel
*Roja Ramani as Leela
*Jose Prakash as Haris Father
*Prem Prakash as Hari
*Sam
*Abbas as Muthalaali
*Adoor Bhavani
*Adoor Pankajam as Rosi
*Alummoodan as Vasu Baby Sumathi as Roshni
*Bahadoor as Moideen Kakka, Hameed
*E Madhavan
*Junior Sheela as Joses Sister
*N. Govindankutty as Thankayyan
*PO Thomas
*Philomina as Joses Mother
*S. P. Pillai as Joses Father
*Saraswathi as Sarasu
*V Govindankutty
*Veeran as Kunjikkannan Prema as Sister
 

==Soundtrack==
The music was composed by M. S. Viswanathan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aniyam Maniyam || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Kaattumozhukkum || P Jayachandran, Latha Raju || Vayalar Ramavarma || 
|-
| 3 || Kannuneerthulliye || M. S. Viswanathan || Vayalar Ramavarma || 
|-
| 4 || Maaril syamanthakarathnam chaarthi || LR Eeswari || Vayalar Ramavarma || 
|-
| 5 || Suprabhaatham || P Jayachandran || Vayalar Ramavarma || 
|-
| 6 || Suprabhatham   || P Jayachandran || Vayalar Ramavarma || 
|-
| 7 || Va Mammy Va Mummy || Latha Raju || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 