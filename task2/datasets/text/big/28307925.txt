Prisoners (1981 film)
{{Infobox film
| name = Prisoners
| image = 
| caption = 
| director = Peter Werner
| producer = Antony I. Ginnane John Barnett executive Keith Barish David Hemmings Craig Baumgarten associate Brian W. Cook
| screenplay = Meridith Baer Hilary Henkin
| based on = story by Meredith Baer
| starring = Tatum ONeal Colin Friels Shirley Knight David Hemmings Bruno Lawrence Ralph Cotterill Peter Sullivan
| cinematography = James Glennon
| editing = Adrian Carr
| studio = Endeavour Productions Lemon Crest
| distributor = 20th Century Fox
| released = 
| runtime = 95 mins
| budget = 
| gross =
| country = New Zealand United States
| language = English
| italic title = force
}}
 New Zealand drama film directed by Peter Werner and starring Tatum ONeal, Colin Friels and David Hemmings.  
 completion guarantor and 20th Century Fox and Fox elected not to release the film as it protected their financial status. 

==Plot==
An American moves his family to New Zealand where he takes charge of a prison in Wellington, New Zealand|Wellington. His young daughter begins to have a love affair with one of the prisoners in his charge. 
==Cast==
* Tatum ONeal - Christie
* Colin Friels - Nick
* Shirley Knight - Virginia
* David Hemmings - Wilkens
* Bruno Lawrence - Peeky
* Ralph Cotterill - Holmby
* John Bach - Bodell
* Michael Hurst - Sciano
* Reg Ruka - Monkey
* Rob Jayne - Maslow
* Norman Fairley - Lewitt
* Peter Rowley - Hapstood
* Karl Bradley - Steel
* Richard Moss - Dunham
* Timothy Lee - Watts

==Production==
The film was shot in Auckland from 29 June to 14 August 1982. 
 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 