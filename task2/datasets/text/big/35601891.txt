Risto (film)
{{Infobox film
| name           = Risto
| image          = Risto film.jpg
| caption        = Film poster
| director       = Tuomas Summanen
| producer       = Olli Haikka Samuli Norhomaa
| writer         = Mikko Reitala
| starring       = Risto Kaskilahti
| music          = 
| cinematography = Arno Launos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Finland
| language       = Finnish
| budget         = 
}}
Risto is a 2011 Finnish comedy film directed by Tuomas Summanen.    

==Cast==
* Risto Kaskilahti as Risto
* Krista Kosonen as Anna
* Aku Hirviniemi as Pasi G. Happonen
* Elena Leeve as Inka Jarkko Niemi as Akseli
* Mika Nuojua as Tuukka
* Jaakko Saariluoma as Erik
* Ville Myllyrinne as Lahnajärvi
* Jussi Vatanen as Agentti
* Elina Knihtilä as Pankinjohtaja
* Kari Ketonen as Apulaisohjaaja
* Joanna Haartti as Ala-asteen opettaja

==References==
 

==External links==
*  

 
 
 
 
 
 
 