Chaja & Mimi
{{Infobox film
| name           = Chaja & Mimi
| image          = Chaja_%26_Mimi_Poster.png
| alt            = English poster of "Chaja & Mimi"
| caption        = English film poster
| director       = Eric Esser
| producer       = Eric Esser
| starring       = {{Plain list|
*Chaja Florentin
*Mimi Frons}}
| cinematography = Albrecht von Grünhagen
| editing        = Benjamin Laser
| distributor    = MakeShiftMovies
| released       =  
| runtime        = 10 minutes
| country        = Germany German / Hebrew
}} director Eric Esser from Berlin, Germany. The work consists primarily of an interview with two Jewish Israelis, Chaja Florentin and Mimi Frons, about their ambivalent relationship to the city of their birth, Berlin. The first version of the film was completed and released in 2009 in Germany only; a second version of the film, fully revised for the international film market, was completed and released in 2013.

== Plot ==

The Jewish protagonists in this documentary, so-called Yekkes, share memories of their childhood days in the Scheunenviertel and Berlin-Mitte, as they were growing up in the 1920s. They tell about their harrowing flight from Nazi Germany to Palestine in 1934. Chaja Florentin relates her experience of a subsequent return to postwar Germany to visit Berlin, which was very painful encounter for her. The emotional pinnacle of the film is reached with the joy and excitement that both women express when they are shown old historical photographs of the neighborhood streets where they grew up. To the final question in this filmed interview, of whether either woman would consider returning to Berlin for good, both answer with an emphatic no, stating on no uncertain terms that Israel is their home now. They express displeasure at many things in Israel, but they say that it is like a difficult child—one that cries all of the time, but one that you love unconditionally.

== Background ==

The first version of Chaja & Mimi debuted on 6 February 2009 at the Directors Lounge film festival in Berlin, as an entry in the Memories category  of the festival program. The film was reviewed by Sabrina Small for the Directors Lounge festival blog. 
The fully revised international version of the film premiered on 22 June 2013 at the Walter Benjamin Short Film Competition in Portbou, Spain.  The film festival was supported by the Goethe-Institut of Barcelona.

The interviews were conducted in the Café Mersand in Tel Aviv, Israel, the favorite café and regular haunt of the film’s protagonists. This Café was founded in 1958 by a German emigrant to Israel, Walter Mersand; it was considered to be the main meeting place for Yekkes in Tel Aviv.  Anne Frank Center in Berlin has incorporated the film into its educational/informational work since 2011, within the framework of its project, Nicht in die Schultüte gelegt (What they forgot to put in your schoolbag). The project’s learning materials are designed for schoolchildren aged 10 and older. The project, like the film, Chaja & Mimi, encourages retrospection based upon the recollections of Jewish survivors of the Holocaust. 

== Reception ==
 German Film Quality Assessment Board (FBW) in the summer of 2013. The FBW’s jury justified its rating with the observation that Chaja & Mimi enhanced the collection of dwindling oral history contributions by survivors of the Third Reich, whose recollections are an indispensable source of crucial knowledge for future generations. The film is deemed to have contributed greatly to honoring these two interesting and fascinating women, who have made their home in Tel Aviv, with a worthy monument. 

== Awards ==
 ICCL Human Rights Film Award in Dublin, Ireland. 
* In May 2014, the film received an Award of Merit from the Best Short Awards in La Jolla, California, U.S.A. 
* Chaja & Mimi was voted third place by the viewing public in the movie theater, Cinemayence, on the occasion of the Langen Nacht des politischen Kurzfilms (Long night of political short films), organized by the Landeszentrale für Politische Bildung Rheinland-Pfalz, in Mayence, Germany, on 23 October 2013. 
* The German Film Quality Assessment Board (FBW) rated the movie with their highest Seal of Approval especially valuable; in addition the FBW named it Documentary Short Film of the Month for August 2013. 
* On 22 June 2013, the festival jury of the Walter Benjamin Short Film Competition in Portbou, Spain awarded the film second place. 

== External links ==

*  
*  

== References ==

 

 
 
 
 
 
 
 
 
 
 
 