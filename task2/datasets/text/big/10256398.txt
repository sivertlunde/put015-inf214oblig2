Speak Up! It's So Dark
{{Infobox film
| name           = Speak Up! Its So Dark
| image          = SpeakUpItsSoDark.jpg
| caption        = Video tape cover
| director       = Suzanne Osten
| producer       = Christer Nilson
| writer         = Niklas Rådström
| starring       = Etienne Glaser Simon Norrthon
| music          = 
| cinematography = Peter Mokrosinski
| editing        = Michal Leszczylowski
| distributor    = First Run Features (USA VHS)
| released       =  
| runtime        = 83 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Best Actor Best Screenplay.   

==Plot==
An elderly Jewish man (Etienne Glaser) befriends a young neo-nazi (Simon Norrthon) on a train and invites him to his home. Through a series of discussions the two gradually come to understand each other better.

==Cast==
* Etienne Glaser as Jacob
* Simon Norrthon as Sören the skinhead
* Anna-Yrsa Falenius as Raped Woman
* Anders Garpe as Sörens Father
* Gertrud Gidlund as Sörens Mother
* Pia Johansson as Nurse

==Distribution==
Speak Up! Its So Dark was shown at the 1993 Toronto Film Festival. It was distributed on video in the United States by First Run Features.

==Awards==
* 1993: Créteil International Womens Film Festival - Grand Prix (Suzanne Osten)

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 