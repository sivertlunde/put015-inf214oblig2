Honeycomb (film)
{{Infobox film
| name           = Honeycomb
| image          = MadrigueraLa.jpg
| caption        = Spanish promotional poster
| director       = Carlos Saura
| producer       = Elías Querejeta
| writer         = Rafael Azcona Geraldine Chaplin Carlos Saura
| starring       = Geraldine Chaplin Per Oscarsson 
| music          = 
| cinematography = Luis Cuadrado
| editing        = Pablo González del Amo
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Honeycomb ( ) is a 1969 Spanish drama film directed by Carlos Saura. The film stars Geraldine Chaplin and Per Oscarsson as a complicated married couple. It was entered into the 19th Berlin International Film Festival.   

==Plot==
Shortly after Teresa (Chaplin) sets fire to her husbands hair, the antagonized and reserved businessman agrees to participate in his pretty young wifes personality games. Teresa soon fills their contemporary home with family heirlooms she retrieved from the basement, and a sense of isolation takes over the house as the couple lock the doors and draw the shades away from the prying eyes of neighbours. However, all too soon these games reach a feverish intensity and fantasy soon blurs into reality. 

==Cast==
* Geraldine Chaplin as Teresa
* Per Oscarsson as Pedro
* Teresa del Río as Carmen
* Julia Peña as Águeda
* Emiliano Redondo as Antonio
* María Elena Flores as Rosa
* Gloria Berrocal as La Tía

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 