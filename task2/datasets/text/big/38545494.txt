Ganges: River to Heaven
{{Infobox film
| name = Ganges: River to Heaven
| image = 
| caption = 
| writer = 
| editing = Keiko Deguchi
| starring = 
| director = Gayle Ferraro
| producer = Gayle Ferraro
| distributor = Aerial Productions
| country= USA
| released =  
| runtime = 77 minutes 
| language = 
| budget =  
}}
Ganges: River to Heaven, is a 2003 American documentary film by Boston filmmaker, Gayle Ferraro. The film document the culture and beliefs of the Hindu religion across the Ganges River which has been considered holy in India.

==Production==

The five person film crew spent almost four weeks living at Assi Ghat, Varanasi on the banks of the Ganges River and during that time fully documented the religious culture and rituals associated with both life and death in this photogenic region of the sub continent of India. The film crew included some of the most experienced and talented professional technicians available in India. 

==Reception==
Dennis Harvey of variety (magazine)|Variety reviewed "At once lyrical and smell-the-stench gritty, "Ganges: River to Heaven" provides a respectful, often engrossing overview of the water body thats among the worlds holiest sites. Visually handsome package is enlightening on several levels, resulting in an attractive fest item with good prospects for public tubecast and possible limited theatrical exposure".  Devin D. OLeary of Alibi said "The films only major drawback is that it never digs quite deep enough into its subject matter. The film is undeniably beautiful, shot with an eye for everyday detail—something which India is overflowing with.Still, as a simple, gorgeously shot primer on Hindu spirituality, Ganges: River of Heaven works, delivering the worthwhile message that maybe death can lead to a greater life. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 

 