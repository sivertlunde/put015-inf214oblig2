Wedding Crashers
 
 
{{Infobox film
| name           = Wedding Crashers
| image          = Wedding crashers poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster David Dobkin
| producer       =  
| writer         =  
| starring       =  
| music          = Rolfe Kent
| cinematography = Julio Macat
| editing        = Mark Livolsi
| studio         = Tapestry Films
| distributor    = New Line Cinema
| released       =  
| runtime        = 119 minutes 127 minutes  
| country        = United States
| language       = English
| budget         = $40 million 
| gross          = $285.2 million   
}} American romantic David Dobkin. Bob Fisher and produced through New Line Cinema.

The film opened on July 15, 2005.  The DVD was released on January 3, 2006, including an unrated version, and the Blu-ray version was released on December 30, 2008. 

==Plot== mediators in Washington D.C. The two friends frequently Wedding crashing|"crash" wedding parties to meet and bed women, working from a set of rules taught to them by a retired crasher, Chazz Reinhold (Will Ferrell). 
 reception by using their charm and lies to avoid being caught. Their goals are to enjoy the free food and drinks and ultimately to charm their way into bed with women from the wedding for a one-night stand. After a sequence of successful crashes, Jeremy takes John to a wedding for the daughter of the U.S. Secretary of the Treasury, William Cleary (Christopher Walken). Once inside, the pair set their sights on Clearys other daughters, Gloria (Isla Fisher) and Claire (Rachel McAdams). Jeremy ends up having sex with Gloria on a nearby beach while the reception is taking place. Gloria is possessive and quickly becomes obsessed with Jeremy. She claims to have been a virgin until the encounter, which shocks Jeremy, and he urges John to bail on the reception with him.
 touch football, which ends with Jeremy being hurt by an over-aggressive Sack. Gloria tends to him, while trying to engage him in sexual intercourse at the same time. 
 Jane Seymour), makes John fondle her surgically enhanced breasts, then rebuffs his actions, much to Johns confusion. 
 seduce him, but is interrupted by his father. The next morning, Jeremy asks John if they can go home, but John convinces him to stay, which helps Jeremy realize that John is falling for Claire. That afternoon, the family (minus Sack) go sailing, where John and Claire continue to bond. Following the sailing trip, the family (with Sack now joining them) go on a hunting trip, where Sack intentionally shoots in the direction of Jeremy and John which startles them and ends up with Jeremy being shot in the buttocks. 

After the hunting trip, John and Claire go on a bike ride through the country, and they end up at a secluded beach. Claire finally admits she isnt sure how she feels about Sack and ends up kissing John passionately. Meanwhile, Gloria is tending to Jeremys wounds and reveals to Jeremy that she was not a virgin when they met and that she only said that because thats what she thought he would like to hear. Jeremy realizes that he may be in love with Gloria. Later that day while eating lunch, Sack suspects that Claire may be falling in love with John and announces (untruthfully) that he and Claire are engaged. This leaves her at a loss for words and John is convinced that Sack is lying. 
 privately investigated. John reluctantly reveals their true identities, and William kicks them out. While leaving, Todd wants his portrait back, but Jeremy tells him that hes keeping it as a gift; this gives Todd some relief knowing that someone took him seriously as an artist. Jeremy and Gloria briefly confess their love to each other.

John and Jeremy return to their normal lives. During that time, Jeremy is trying to keep his relationship with Gloria a secret when John comes in to talk to him. He has formulated a new way to try and reconcile with Claire by trying to be a waiter at her engagement party. Jeremy tries to convince him that his past attempts to reconcile with her were unsuccessful and John needs to let it go by coming to terms with the fact that Claire doesnt want anything to do with him. He refuses to give up even when Jeremy warns him that the engagement party will be heavily guarded by the Secret Service and their best chances to avoid being spotted is to stay away.
 nihilistic and suicidal. Meanwhile, Jeremy continues his relationship with Gloria, culminating in an accepted marriage proposal, and Claire begins to doubt her relationship with Sack. Jeremy attempts to reconcile with John, asking him to be his best man, but John refuses. 

John then goes to meet their mentor, Chazz Reinhold (an uncredited Will Ferrell), to reaffirm Jeremys apparent stupidity in getting engaged. During the visit, Chazz talks him into crashing a funeral, as Chazz believes that grief is the best aphrodisiac. While there, John takes notice of a woman who is truly grieving for her husband because she loved him. He reconsiders his stance on love and marriage and rushes to Jeremys wedding.
 abuse of his ex-wife Tina Turner|Tina). Sack loses control of himself and rushes at John, but Jeremy intervenes and knocks him out.

The film ends with Jeremy getting married to Gloria, John uniting with Claire, and the two couples driving away talking about crashing another wedding together.

==Cast==
* Owen Wilson as John Beckwith, one half of the wedding crasher duo; a bachelor attorney riding high on his success with Jeremy.
* Vince Vaughn as Jeremy Grey, the other half of the wedding crasher duo; an unrepentant womanizer. He remains a loyal friend to John.
* Christopher Walken as United States Secretary of the Treasury William Cleary, a big sailing fan and oblivious head of the dysfunctional but loving Cleary household; a generally affable man, but punishes anyone who insults his wife or four children.
* Rachel McAdams as Claire Cleary, middle daughter of William Cleary; sweet girl and full-time environmental activist.
* Isla Fisher as Gloria Cleary, youngest daughter of William Cleary; an emotionally unstable nymphomaniac who is obsessed with Jeremy. Jane Seymour as Kathleen "Kitty Kat" Cleary, the "social alcoholic" wife of Secretary Cleary; she appears to try and hit on younger men, including John and an ex-boyfriend of Claire (this last one revealed by Todd).
* Ellen Albertini Dow as "Grandma" Mary Cleary, Williams mother, Kathleens mother-in-law, and the siblings grandmother who lives in the compound; she is kind but obscene.
* Keir ODonnell as Todd Cleary, William and Kathleens Recluse|reclusive, hyper-sensitive son; a homosexual artist and family black sheep.
* Bradley Cooper as Sack Lodge, Claires testosterone-fueled, preppy, and violent boyfriend who is obsessed with winning at any cost.
* Henry Gibson as Father ONeil, the priest officiating at several weddings.
* Ron Canada as Randolph, the Clearys Jamaican butler; he seems completely aware of Glorias obsessive behaviors.
* Jenny Alden as Christina Cleary, the eldest daughter of William Cleary.
* Will Ferrell (uncredited) as Chazz Reinhold, the sage who passed on the wedding crashing rules to his protege Jeremy in 1993. When John actually meets him, Chazz is revealed to be a lazy man who still lives with his mother.
* Diora Baird as Vivian
* Dwight Yoakam and Rebecca De Mornay in the beginning mediation scene as Mr. and Mrs. Kroger
* Kathryn Joosten as Mrs. Reinhold
* Richard Riehle (uncredited) as Funeral guest

Arizona Senator and 2008 Republican presidential nominee John McCain had a brief cameo appearance in the film towards the beginning, where he and Democratic strategist and CNN contributor James Carville are seen congratulating the secretary and his wife on their daughters wedding.

==Production== Bob Fisher and produced through New Line Cinema, with shooting taking place on location in Washington, D.C. and Marylands Eastern Shore. 
Principal photography began on March 22, 2004 and the movie had a 52 day shooting schedule.
The main Cleary wedding reception scene was filmed at the Inn at Perry Cabin in Saint Michaels, Maryland. 

==Reception==

===Critical response===
  
Wedding Crashers received mainly positive reviews and has a 75% rating on Rotten Tomatoes; 182 reviews were counted: 137 fresh and 45 rotten. 

Carina Chocano of the Los Angeles Times wrote a favorable review and said "underneath the diarrhea gags, the long lens at close range of Vaughns pants and the handcuffs, its really just a love story about a couple of buddies who live happily ever after. And it couldnt have happened to a nicer, more charming couple". 
Manohla Dargis of the New York Times said "Its crude, yes, but also funny; too bad these lost boys cant stay lost.  Like clockwork, the film soon mutates from a guy-oriented sex comedy into a wish-fulfillment chick flick". 

British Movie magazine Empire (film magazine)|Empire awarded it three out of five stars and were complimentary to Vaughn and Wilson, saying "Sharing an easy chemistry and free of the usual joker/straight-guy dynamic, Wilson and Vaughn quip, riff and banter to hilarious effect. And both get their fair share of money moments, the latter’s muggings are particularly hysterical in a raunchy dinner-party sequence, The laidback stars are funny and sweet, but they’re let down by a patchy script which squanders some potentially priceless set-ups." 

===Box office=== Charlie and the Chocolate Factory. Fairly well received by critics, the film eventually grossed over $209,255,921 domestically, narrowly outgrossing Charlie. Considering its modest budget ($40 million) and competition with heavily advertised blockbusters during the summer season, the studio did not expect the movies astounding level of success. It grossed $75,920,820 overseas, totaling $285,176,741 worldwide. 

===Accolades===
 
On April 24, 2006, Wedding Crashers topped the nominations for the years MTV Movie Awards with five including Best Movie. It won Best Movie, On-Screen Team (Vaughn and Wilson), and Breakthrough Performance (Isla Fisher). The financial and award success of the film has been credited along with The 40-Year-Old Virgin for reviving the popularity of adult-aimed R-rated comedies.
 The Karate Kid, whom he would later play in a Saturday Night Live sketch in 2009. 

===Home media===
  
 audio commentaries Budweiser Wedding Crashers commercials, a track listing for the official soundtrack on 20th Century Fox Records, a music video by The Sights, and a jump-to-a-song sample feature. 

==Television version==
 
The creators of the film made a reality TV version, called The Real Wedding Crashers and shown on NBC in April and May 2007. NBC only showed four episodes. {{cite web 
| title = The Real Wedding Crashers NBC TV Show: Funny Marriage Prank Series and Jokes 
| url = http://www.nbc.com/The_Real_Wedding_Crashers/ 
| work = NBC Official Site
}} 

==See also==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 