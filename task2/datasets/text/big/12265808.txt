Juken Sentai Gekiranger: Nei-Nei! Hou-Hou! Hong Kong Decisive Battle
{{Infobox film
| name      = Juken Sentai Gekiranger: Nei-Nei! Hou-Hou! Hong Kong Decisive Battle
| film name = {{Film name| kanji = 電影版 獣拳戦隊ゲキレンジャー ネイネイ!ホウホウ!香港大決戦
| romaji = Deneiban Jūken Sentai Gekirenjā: Nei-Nei! Hō-Hō! Honkon Daikessen}}
| image     =DenGeki Poster.jpg
| caption = Film poster for both Juken Sentai Gekiranger: Nei-Nei! Hou-Hou! Hong Kong Decisive Battle and  
| director    = Shōjirō Nakazawa
| producer    = Toei Company
| writer     = Naruhisa Arakawa
| narrator    = Kei Grant Masashi Ishibashi
| music     = Kazunori Miyake
| cinematography = Fumio Matsumura Ren Sato Shaw Brothers
| released    =  
| runtime    = 33 minutes
| country    = Japan, Hong Kong
| language    = Japanese, Cantonese, English
| budget     =
| italic title = force
}}
  is the theatrical film adaptation of the  . The movies title comes from the Cantonese pronunciation of Ni hao (Mandarin for "hello"), as Cantonese is the de facto dialect of Hong Kong.

==Plot==
While in the middle of fighting, the Gekirangers and Mere are teleported to Hong Kong, along with Rio and various other martial artists around the world. Just as Rio and Jyan were about to fight, they meet the mysterious Lao Fan before seeing others brought to the island as well. They all then meet Miranda, the secretary to Yang, a media mogul who brought them all together for the   to prove whos the strongest of them all. The first round of fighting leaves the Gekirangers, their Rinjūken rivals, Lao, Barnard Koyama, and Big the Goto as the winners. At the banquet hall, Ran, Retu, Rio, and Mere meet Yang Lo himself while Jyan follow Lao Fan and learns shes actually a member of the Hong Kong police who is investigating Yang, who founded Mechung Fu and used his tournament to gather strongest fighters as part of his plan to rule the world, finding the ideal Ki he needs: from the Jūken users. Geki Blue, Geki Yellow, Rio, and Mere attempt to fight off Miranda as Yang takes his leave, finding Jyan and Lao in his base of operations before they restrained him. However, the Ki Miranda collected before she died transfers to Yangs computer, transforming Yang into a cyborg Beastman before he starts absorbing more Ki to become a supreme being. The Gekirangers battle Yang Lo, refusing to give up in spite of him being stronger than them. Evading his defeat, Yan activates his giant robot Mechannon to terrorize the city. When GekiTohja proves no match for Mechannon, Rio and Mere take offense to Yangs comments on the Jūken style and are forced to help the Gekirangers by summoning RinLion and RinChameleon to combine with GekiTohja to form GekiRinTohja to destroy Mechannon, killing Yang in the process. Once the fight is over, in spite of the Gekirangers amazement at the power of the united Jūken schools, Rio takes his leave with Mere telling them that nothing changed between them. The next day, Lao thanks the Gekirangers as they were about to leave for Japan when Miki, Natsume, and Xia Fu arrive for a vacation in Hong Kong.  

==Characters==
===From series===
*   /  
*   /  
*   /  
*  
*  
*  
*   /  
*  
*  

===Film only===
*  : A local Interpol officer who meets the Gekirangers, Rio, and Mere on Yans island where she is trying to discover Yans plans. She uses nunchaku to fight, like Jyan.

====Mechung Fu====
Practitioners of   are composed of robots with the exception of their founder Yang, who claims his creation is superior to Jūken.
*  : Founder of Mechung Fu and master of  . He uses his position to hide his true intent: gathering the Qi of the fighters as part of his plan to become the strongest being and rule the world. He succeeds and becomes a cyborg Beastman as a result. His fighting style includes  , creating a sandpit to drag his opponents into. He ends up dying in Mechanon when it was destroyed by GekiRinTohja.
*  : Yangs assistant who is actually a Machine-Man that mastered the  , able to use her   for offense and to find martial artists to bring to Yang. Form there, she uses her   to drain her opponents dry of their qi, to power Yangs transformation, getting the last amount from Rio as he killed her.
*  : Robots created by Yang.
*  : The giant antlion robot piloted by Yang, hidden in plain sight as stone statue of Guan Yin.   and   are used before Geki RinTohja scrapped it.

==Cast==
* Jan Kandou/Geki Red:  
* Ran Uzaki/Geki Yellow:  
* Retsu Fukami/Geki Blue:  
* Xia Fu (Voice):  
* Miki Masaki:  
* Natsume Masaki:  
* Rio:  
* Mere:  
* Bae (Voice):  
* Lao Fan:  
* Miranda:  
* Yang:  
* Narrator:  

==References==
 

==External links==
*   

 

 
 
 