Masam Masam Manis
 
 
 
{{Infobox film
| name           = Masam Masam Manis
| image          =
| caption        = Sourness and Sweetness
| director       = P. Ramlee
| producer       =
| writer         =
| narrator       =
| starring       = P. Ramlee Sharifah Hanim
| music          = P. Ramlee
| cinematography =
| editing        =
| distributor    =
| released       = 1965
| runtime        =
| country        = Malaysia
| language       = Malay
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}

Masam Masam Manis (Sourness and Sweetness) is a 1965 Malaysian comedy film directed by and starring P. Ramlee.

==Plot==
The story takes place in Kuala Lumpur in 1965. This story is about a teacher, Shaari who is also a musician at night. When caught sleeping in class, Shaari promised not to perform at night clubs again on weekday nights. Later, a young woman named Norkiah rents the room next to his. Norkiah works at a night club with her friend, played by Mariani, as a dancer. Shaari and Norkiah frequently quarrel and annoy each other. One day, while on a bus, Shaari helps Norkiah to retrieve her stolen purse. They fall in love with each other. Norkiah lies to Shaari, telling him that she works as a teacher at a cooking school at night.By that time, Norkiah has already started singing at the club where she was working. One day, Norkiahs mother and brother came to visit her. Shaari ignores the old woman and her young son.While Norkiah, unknown to Shaari as his own irritating neighbour, is forced to take her mother and brother out shopping, thus, unable to meet Shaari as promised. Later, when Norkiah gets home, she tries to dry her clothes by hanging them on a pole which crosses over to Shaaris room. Shaari gets fed up and pushes the pole down. Norkiah gets even by drenching him with a pail of water. Shaari climbs up the wall to give her a piece of his mind only to find out that Norkiah is the woman that he has fallen in love with. They get married. Shaari proposes that Norkiah quits her job, but she could not due to her contract. One night, Shaari receives an invitation to perform at the club where he used to work. There, somebody suggested that he sings with a female night club singer called Norkiah Hanum. Curious, Shaari goes to the other club only to find out that his wife is a singer and not a teacher as she had claimed. Shaari gets upset and starts to build a wall separating their rooms like they used to. Norkiah tries to apologise, but he insults her by calling her a cabaret singer of low class. Norkiahs friends try to trick Shaari into believing that Norkiah is now divorcing Shaari and having an affair with her colleague, Rashid, played by Mahmud June. Shaari kicks Norkiahs door open only to find that Norkiah is alone with a tape recorder. He apologises and they get back together again. The story ends with Shaari promoted as a headmaster of his school and has five pairs of twins with Norkiah.

==Songs==
* Saat Yang Bahagia
* Dalam Ayer Ku Terbayang Wajah
* Perwira by Saloma

==See also==
* P. Ramlee
* List of P. Ramlee films

 

 
 
 
 