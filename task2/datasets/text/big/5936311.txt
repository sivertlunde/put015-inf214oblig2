Killer: Dead or Alive
{{Infobox film
| name           = Killer: Dead or Alive
| image          = Killer DVD Cover.jpg
| caption        = Killer Dead or Alive DVD Cover
| director       = Scott Shaw
| producer       = Scott Shaw
| writer         = Scott Shaw
| starring       = Scott Shaw   Hae Won Shin   Kevin Thompson   Dana Davidson   Marciya K. Lau
| music          =  Jake Dharma   Hae Won Shin Jake Dharma
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Killer: Dead or Alive is a 2006 thriller film that was directed by Scott Shaw.

This film stars Scott Shaw and Kevin Thompson as Michael Black and Raphael X. This film begins in the jungle where the two lead characters are sent to rescue a girl who is being stalked by a monster. Once she has been rescued the three return to civilization and enter the world of professional Hitmen.

Similar to virtually all Scott Shaw films, this feature is made up of bizarre story twists and is filmed with a backdrop of several spectacular locations. This, combined with the use of unexpected editing techniques, causes the film to be presented in a very visual manner.

This feature was filmed in Hollywood, California and Hong Kong.

==External links==
*  
*  

 