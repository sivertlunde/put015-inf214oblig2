La Tía Alejandra
{{Infobox film
| name           = La tía Alejandra
| image          =
| image size     =
| caption        =
| director       = Arturo Ripstein
| producer       = Lucero Isaac
| writer         = Delfina Careaga Sabina Berman
| narrator       =
| starring       = Isabela Corona
| music          = Luis Hernández Bretón
| cinematography = José Ortíz Ramos
| editing        = Rafael Ceballos
| distributor    = Estudios Churubusco
| released       = 
| runtime        = 98 minutes
| country        = Mexico
| language       = Spanish
| budget         =
| preceded by    =
| followed by    =
}} horror film of 1979, starring by Isabela Corona.

==Plot==
The plot is based on the arrival of Aunt Alejandra to a familiar household consisting of two parents and three children. A woman who is loving, in principle, suffers severe mood swings and strange things happen in her room quite regularly and that seems to be surrounded by an aura of mystery. Rejected by the eldest child, she only serves to bring misfortune to them since coming home. Auntie has a fortune that will surely help her relatives, but really begins to destroy the whole family with diabolical acts, and attempts to teach children witchcraft. When one of the children mocks her, she caused his death. When her nephew dismisses of his house, she chokes him in his own bed. When the older girl burns her face, Alejandra burns an entire room with the girl inside. Only surviving Lucía, the wife, and her young daughter, but she seems to have learned the secrets of Alejandra... 

==Cast==

* Isabela Corona (Tía Alejandra)
* Diana Bracho (Lucía)
* Manuel Ojeda (Rodolfo)
* Maria Rebeca (Martha)
* Lilan Davis (Malena)
* Adonay Somoza Jr. (Andres)
* Ignacio Retes (Médico)

==Comments==
La Tía Alejandra is a curious and unusual, a rarity in the genre of Mexican Cinema. Not strictly a horror film, but conveys concern in many of its passages. It is not explicit in what it shows but rightly fiddles with witchcraft, curses, black Magic and Satanism. Not when graphic violence on screen display, but in a subtle way the sample with an elegant ferocity that causes discomfort even viewer.
The staging of Ripstein is austere. But his control over the narrative tempo is excellent. With the atmosphere and the magnetic presence of a large Isabela Corona that from a physical get feeble and helpless to convey a menacing tone that seems unfeasible but for the good work between director and actress.
 

==References==
 

==External links==
* 
*  
*  
*  

 
 
 
 
 
 