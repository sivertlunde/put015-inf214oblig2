Murphy's Romance
{{Infobox film
| name           = Murphys Romance
| image          = Murphys romance.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Martin Ritt
| producer       = George Justin (associate producer) Jim Van Wyck (associate producer) Laura Ziskin (producer)
| screenplay     = Harriet Frank, Jr. Irving Ravetch
| story          = Max Schott
| starring = {{Plainlist|
* Sally Field
* James Garner
* Brian Kerwin
* Corey Haim
}}
| music          = Carole King
| cinematography = William A. Fraker
| editing        = Sidney Levin
| studio         = Fogwood Films
| distributor    = Columbia Pictures
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         = $13 million  . - IMDb 
| gross          = $30,762,621 
|}}
Murphys Romance is a 1985 romantic comedy film adapted by Harriet Frank Jr. and Irving Ravetch from a 1980 novel by Max Schott and directed by Martin Ritt. The film stars Sally Field (also executive producer), James Garner, Brian Kerwin, and Corey Haim.

The films theme song, "Love for the Last Time," is performed by Carole King.

==Plot synopsis==
Emma Moriarty (Sally Field) is a 33-year-old, divorced mother who moves to a rural Arizona town to make a living by training and boarding horses. She becomes friends with the towns druggist (pharmacist), Murphy Jones (James Garner), but a romance between them seems unlikely due to Murphys age and because Emma allows her ex-husband, Bobby Jack Moriarty (Brian Kerwin), to move back in with her and their 12-year-old son, Jake (Corey Haim).

Emma struggles to make ends meet, but is helped by Murphy, an idiosyncratic widower who drives an otherwise immaculate antique automobile plastered with political bumper slogan stickers Murphy terms his "causes".  While refusing to help her outright with charity or personal loan, Murphy buys a horse and pays to board it with Emma, while encouraging others to do the same.  He also introduces Emma to the towns local politics and provides much-needed emotional support for Emma as well as Jake, who is looking for a father figure to emulate.  A rivalry soon develops between Murphy and Bobby Jack.  This contest of wills continues until a character shows up from Bobby Jacks recent past that surprises everyone, while forcing Murphy and Emma to reevaluate the nature of their relationship.

==Cast==
* Sally Field as Emma Moriarty
* James Garner as Murphy Jones
* Brian Kerwin as Bobby Jack Moriarty
* Corey Haim as Jake Moriarty
* Dennis Burkley as Freeman Coverly
* Carole King as Tillie
* Georgann Johnson as Margaret
* Dortha Duckworth as Bessie
* Michael Prokopuk as Albert
* Billy Ray Sharkey as Larry Le Beau
* Michael Crabtree as Jim Forrest Anna Levine as Wanda Charles Lane as Amos Abbott Bruce French as Rex Boyd
* John C. Becher as Jesse Parker
* Henry Slate as Fred Hite

==Production==
Sally Field and director Martin Ritt had to fight Columbia Pictures in order to cast Garner, who was viewed at that point as primarily a television actor despite having enjoyed a flourishing film career in the 1960s (and more recently having co-starred in the box office hit Victor Victoria|Victor/Victoria opposite Julie Andrews two years earlier).

Columbia didnt want to make the picture at all, because it had no "sex or violence" in it. But because of the success of Norma Rae (1979), with the same star (Field), director, and screenplay writing team (Harriet Frank, Jr. and Irving Ravetch), and with Fields new production company (Fogwood Films) producing, Columbia agreed. But, Columbia then wanted Marlon Brando, or someone with "greater box-office allure," to play the part of Murphy, so Field and Ritt had to insist on Garner. 
 The Record. - January 17, 1986. - Retrieved: 2008-08-03 

Part of the deal from the studio, which at that time was owned by The Coca-Cola Company, included an eight line sequence of Field and Garner saying the word "Coke," and also having Coke signs appear prominently in the film. 

On the A&E television program Biography (TV series)|Biography of Garner, "James Garner: Hollywood Maverick," Field reported that her on-screen kiss with Garner was the best cinematic kiss she had ever experienced. Nelson, Ted. - "James Garner: Hollywood Maverick." - A&E Biography. - October 2, 2000. - New York, NY: A & E Home Video. - ISBN 978-0-7670-3361-9 

Filming took place on location in   

The film was originally scheduled for general release during the 1985 Christmas Day weekend, but Columbia moved it to the weekends of January 17 and January 31, 1986, when they saw the holiday lineup of films. They did a limited, selected, release December 25, 1985. 

==Reception==
Reviews were generally favorable. Film critic Roger Ebert gave the film 3 stars (out of 4) stating "Much depends on exactly what Emma and Murphy say to each other, and how they say it, and what they dont say. The movie gets it all right."   The film holds a 93% on the film review aggregator website Rotten Tomatoes. 

==Divergence from the novella==
The screenplay is very different from the Max Schott novella. In the Schott story, Murphy and Emma stay just platonic friends. Murphy marries someone else, and then tries to find Emma a suitable husband. 

==Awards== Academy Award Best Actor Best Cinematography.

==References==
 

==External links==
*  
*  
*  
*  
*   interview on the Charlie Rose Show
*   interview at the Archive of American Television

 
 
 
 
 
 
 
 
 
 
 