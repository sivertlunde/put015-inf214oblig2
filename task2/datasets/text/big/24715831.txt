I Prefer the Sound of the Sea
{{Infobox film
| name           = I Prefer the Sound of the Sea
| image	         = I Prefer the Sound of the Sea FilmPoster.jpeg
| caption        = Film poster
| director       = Mimmo Calopresti
| producer       = Donatella Botti Roberto Cicutto Luigi Musini Francesco Bruni Mimmo Calopresti Heidrun Schleef
| starring       = Silvio Orlando Michele Raso
| music          = 
| cinematography = Luca Bigazzi
| editing        = Massimo Fiocchi
| distributor    = 
| released       =  
| runtime        = 84 minutes
| country        = Italy France
| language       = Italian
| budget         = 
}}

I Prefer the Sound of the Sea ( ,  ) is a 2000 Italian-French drama film directed by Mimmo Calopresti. It was screened in the Un Certain Regard section at the 2000 Cannes Film Festival.   
 Heart and works in a bookshop named Franti.

==Cast==
* Silvio Orlando - Luigi
* Michele Raso - Rosario
* Fabrizia Sacchi - Serena
* Paolo Cirio - Matteo
* Mimmo Calopresti - Don Lorenzo
* Andrea Occhipinti - Massimo
* Enrica Rosso - Elisabetta
* Marcello Mazzarella - Vincenzo
* Eugenio Masciari - Cappabianca
* Raffaella Lebboroni - Miriam
* Palma Valentina Di Nunno - Adele
* Lorenzo Ventavoli - Umberto
* Concettina Luddeni - Madre di Luigi
* Laura Curino - Maria (governante)
* Antonio Ferrante - Pasquale (padre di Rosario)
* Stefano Venturi - Giovanni (Mr. Pleigin)
* Giovanni Bissaca - Magistrato
* Elena Turra - Segretaria di Luigi

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 