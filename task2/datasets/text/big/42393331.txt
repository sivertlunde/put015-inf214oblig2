The Marchers
{{Infobox film
| name           = The Marchers
| image          = La_Marche_poster.jpg
| alt            =
| caption        =
| director       = Nabil Ben Yadir
| producer       = Diana Elbaum Benoit Roland Hugo Sélignac
| writer         = Nabil Ben Yadir Nadia Lakhdar Ahmed Ahmidi
| starring       = Jamel Debbouze Olivier Gourmet Lubna Azabal Hafsia Herzi
| music          =
| cinematography = Danny Elsen
| editing        = Damien Keyeux
| studio         = Chi-Fou-Mi France 3 Europa Corp Entre Chien et Loup Belgacom TV
| distributor    = EuropaCorp. Distribution  
| released       =    }}
| runtime        = 120 minutes
| country        = France Belgium
| language       = French Arabic
| budget         =
| gross          =
}}
The Marchers ( ) is a 2013 French comedy-drama film by Nabil Ben Yadir. It is loosely based on the events surrounding the 1983 March for Equality and Against Racism.

The films release in November 2013 coincided with the 30th anniversary of the march.   

==Plot==
In 1983 France, teenaged Mohamed (Jallab) is shot by a policeman, but survives. Rejecting his friends proposal of violent Revenge|retribution, he proposes political action inspired by Gandhi and Martin Luther King.  With two friends, and support from Dubois (Gourmet), the priest of Minguettes (Lyon), they embark on a non-violent March for Equality and Against Racism between Marseille and Paris.

==Cast==
*Olivier Gourmet as Dubois
*Tewfik Jallab as Mohamed
*Vincent Rottiers as Sylvain
*MBarek Belkouk as Farid
*Nader Boussandel as Yazid
*Lubna Azabal as Kheira
*Hafsia Herzi as Monia
*Charlotte Le Bon as Claire
*Philippe Nahon as René Ledu
*Jamel Debbouze as Hassan RG
*Simon Abkarian as Farids father
*Corinne Masiero as Dominique Rufus as François, pastor
*Benjamin Lavernhe as Thomas
*Kévin Azaïs as Rémi 
*Françoise Miquelis as  

==Comparison to historical events==
Answering a question about taking "liberties with the narration when telling a true story", director and co-writer Ben Yadir said: "You focus on the great History: the towns, the march of the torches, the return to Lyon, the  , all these images that pull you back to reality... But at the start in Marseilles, there was a group of 32, and we obviously could not make a movie with 32 characters. We thus created 10 characters around which we built short stories."   

==Release==
The Marchers had theatrical showings in North America as part of the Rendez-vous with French Cinema series 2014 program.   

==Reception==
Boyd van Hoeij of The Hollywood Reporter said " he film’s message of equality is loud and sincere but Yadir, here directing his second feature, struggles to maintain a workable entente between the downbeat story   with misplaced-feeling broad humor."   

Peter Debruge of Variety (magazine)|Variety called it "uplifting story of racial tolerance   should travel well."   

Le Parisien gave it a positive review. 

==Accolades==
{| class="wikitable" style="font-size: 100%;"
|-l
! Award
! Category
! Recipient(s)
! Result
|- 19th Lumières Awards|Lumières Awards Best Screenplay
| Nabil Ben Yadir
|  
|- Most Promising Actor
| Tewfik Jallab
|  
|- 5th Magritte Magritte Awards Best Film
| The Marchers
|  
|- Best Director
| Nabil Ben Yadir
|  
|- Best Screenplay
| Nabil Ben Yadir
|  
|-
| Best Supporting Actor
| Olivier Gourmet
|  
|-
| Best Supporting Actress
| Lubna Azabal
|  
|-
| Best Editing
| Damien Keyeux
|  
|}

==References==
 

==External links==
*  
*  
*  
*   at Allocine
*   (with English subtitles)

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 