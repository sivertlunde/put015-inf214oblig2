Everywhen
 
 
{{Infobox film
| name = Everywhen
| image = 
| alt = 
| caption = 
| director = Jarand Breian Herdal
| producer = Jarand Breian Herdal
| writer = Jarand Breian Herdal Eirik Moe
| starring = Harald Evjan Furuholmen Hugo Hermann
| music = William Edward
| cinematography = Jarand Breian Herdal Jens Peder Hertzberg
| editing = Jens Peder Hertzberg
| studio = Jarandandjens
| distributor = Cinematekne
| released =  
| runtime = 65 minutes
| country = Norway
| language = English NOK 55,000 (United States dollar|$8,000)
}} directorial debut and starring Harald Evjan Furuholmen and Hugo Hermann.

==Plot==
Set in 2077 Norway, 17-year-old Ian Finch travels from one reality to another to save his suicidal 5-year-old brother Dylan when three billion people go missing all at once

==Cast==
* Harald Evjan Furuholmen as Ian Finch
* Hugo Hermann as The Helper
* Brathen Elin Synnøve as Jane Scott
* Graeme Wittington as Thomas Wilfred
* Hauk Phillip Bugge as Dylan
* Rune Dennis Tønnessen as Michael Brossman
* Ruben Løfgren as Ridley Doug
* Gjermund Gjesme as Brad

==Release==
The film was released in Norway on 28 January 2013.

==References==
 

==External links==
*  
*  
*   at YouTube

 
 
 
 
 
 
 
 
 
 
 
 
 

 