Bellman and True
{{Infobox film
| name           = Bellman and True
| image          = Bellman and true.jpg
| caption        = Theatrical release poster
| director       = Richard Loncraine Christopher Neame Denis OBrien Michael Wearing
| writer         = Desmond Lowden (from his novel)
| narrator       = Richard Hope Frances Tomelty Derek Newark
| music          = Colin Towns
| cinematography = Ken Westbury Paul Green
| distributor    =  
| released       = 1987
| runtime        = 112 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded by    =
| followed by    =
}} Richard Hope.

==Plot==
Hiller, a computer expert, was bribed by a group of bank robbers to obtain details of the security system at a newly built bank. Having obtained the information, he thought hed seen the last of the robbers. But now theyve traced him and his son to London. They hold the son hostage and force Hiller to decode the information about the alarm and then to take part in the robbery.

The film Bellman and True was originally a 3-part TV series, with a runtime of 150 minutes (approximately); a cut-down version of the series was released as a 112-minute feature film.

==External links==
* 
* 

 

 
 
 
 
 
 
 


 