Gwyneth of the Welsh Hills
{{Infobox film
| name           = Gwyneth of the Welsh Hills
| image          =
| caption        =
| director       = Floyd Martin Thornton 
| producer       = 
| writer         = Edith Nepean (novel)   Leslie Howard Gordon Lewis Gilbert    Harvey Braban
| music          = 
| cinematography = Percy Strong
| editing        = 
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       = November 1921
| runtime        = 6,000 feet   
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance Lewis Gilbert. It was based on a novel by Edith Nepean.

==Cast==
* Madge Stuart as Gwyneth 
* Eille Norwood as Lord Pryse  Lewis Gilbert as Davydd Owen 
* Harvey Braban as Gwylim Rhys 
* R. Henderson Bland as Shadrack Morgan 
* Elizabeth Herbert as Megan Powers 
* Gladys Jennings as Blodwen 
* Dalton Somers as Denis 
* Joseph R. Tozer as Evan Pryse 
* Robert Vallis as But Lloyd 
* Sam Wilkinson as Shores 
* Mrs. Hubert Willis as Jan Rhys

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

 

==External links==
* 
 
 
 
 
 
 
 
 
 
 

 