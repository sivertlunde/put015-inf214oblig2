Saajan Bina Suhagan
{{Infobox film
| name           = Saajan Bina Suhagan
| image          =
| caption        =
| director       = Sawan Kumar Tak
| producer       = Sawan Kumar Tak
| screenplay     = Kamleshwar
| narrator       =
| story          = S. Kumar
| starring       = Rajendra Kumar Nutan Vinod Mehra
| music          = Usha Khanna Indivar  (lyrics) 
| cinematography = K.H. Kapadia
| editing        = David Dhawan
| distributor    =
| released       =  
| runtime        = 145 minutes
| country        = India
| language       = Hindi
| budget         =
}}

Saajan Bina Suhagan (meaning bride without bridegroom) is a 1978 Hindi drama film produced and directed by Saawan Kumar Tak. The film stars Rajendra Kumar, Nutan, Vinod Mehra, Padmini  Kohlapure and Shreeram Lagoo, the films music is by Usha Khanna. It has a popular song "Madhuban Khushboo Deta Hai" sung by Yesudas, Anuradha Paudwal and Hemlata. The film was remade in Tamil as Mangalanayaki.
==Cast==
* Nutan as Asha Chopra
* Vinod Mehra as Anand
* Shreeram Lagoo as Gopal Chopra  
* Radha Bartake  as Basanti Chopra (as World Teen Princess Kasturi)
* Aarti Chopra as  Barkha Chopra
* Padmini Kolhapure as BulBul Chopra
* Rajendra Kumar as  Raj Kumar
* Leela Mishra as Chopras neighbor
* Satyendra Kapoor as Dr. Malhotra
* Nana Palsikar as Ashas Father

==Music==
Film has music by Usha Khanna and lyrics by Indivar  

* "Madhuban Khushboo Deta Hai, Saagar Saawan Deta hai" -  K.J. Yesudas
* "O Mamma, Dear Mamma, Happy Birthday To You" - Aarti Mukherjee, Chandrani Mukherjee, Shivangi Kapoor
* "Kaise Jeet Lete Hain Log Dil Kisi Ka" - Mohammad Rafi
* "Sata Sata Ke Khush Hote ho" - 
* "Jijaji Jijaji, Honewale Jijaji" - Anuradha Paudwal, Suresh Wadkar, Dilraj Kaur 
* "O Jaani, Jaani Tum O Jaane Tum" - 
* "Madhuban Khushboo Deta Hai, Saagar Saawan Deta hai" - K.J. Yesudas, Anuradha Paudwal

== References ==
 
== External links ==
*  

 
 
 
 
 