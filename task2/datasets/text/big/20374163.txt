Wild Geese II
{{Infobox film
| name           = Wild Geese II
| image          = Wild geese 2 film poster.jpg
| image size     =
| caption        = Theatrical release poster Peter Hunt
| producer       = Euan Lloyd
| screenplay     = Reginald Rose
| based on       = The Square Circle by Daniel Carney Edward Fox Laurence Olivier
| music          = Roy Budd Michael Reed
| editing        = Keith Palmer
| distributor    = Thorn EMI
| released       = 18 October 1985
| runtime        = 125 min.
| country        = United Kingdom English
| budget         =£11 million Andrew Yule, Hollywood a Go-Go: The True Story of the Cannon Film Empire, Sphere Books, 1987 p137 
}} British Action thriller film Peter Hunt, Edward Fox) as one of the mercenaries. No characters from the original are featured in the sequel.

==Prologue==
Africa 1977
 Dakota aeroplane. Simbas are virtually upon him, Janders calls for Faulkner to kill him, which he regretfully does.

==Plot==
London 1982

As the only surviving Nazi leader in captivity, Rudolf Hess (Laurence Olivier) has secrets that could destroy the careers of prominent political figures, secrets an international news network will pay any price to get. 
 Edward Fox) arrives for a meeting, Robert McCann (Robert Webber) is arguing with Michael Lukas about the delay of a planned rescue of Rudolf Hess. 
 John Terry Palestinian hitmen in London. Later network executives Kathy and Michael Lukas hire Haddad to free Hess and get him safely out of West Berlin.

When Haddad arrives in West Berlin he stakes out the outside of Spandau Prison as a jogger while being spied on. He drafts plans of the outside of the prison including guard towers and entrances. The next day Haddad joins a construction team and sneaks away to get into the prison guard entrance. Carefully eluding the guards by studying their timed patrols he drafts floor plans of the hallways and cell blocks.
 East German spy Karl Stroebling. Stroebling and his thugs smother Haddad with a plastic bag over his head to torture him into disclosing details about his mission. Haddad escapes and survives by overpowering the thugs and rolls across the street being barely missed run over from an oncoming truck as the police arrive as witnesses to the incident.

While recovering in hospital, Haddad is visited by British Colonel Reed-Henry (Kenneth Haigh). Reed-Henry questions Haddad but to no avail; he leaves Haddad but suspects he is there to rescue Hess. Haddad leaves the hospital and along with Kathy goes to Bavaria to plan the mission without interference from Stroebling. 
 assassin and is an expert marksman. As romance between Haddad and Kathy blossoms, the trio return to West Berlin to find that Reed-Henry will help Haddad release Hess. Once again Stroeblings thugs attempt to kill Haddad, but this time Faulkner helps him kill all but one of them.
 British Royal Military Police. Stroebling offers to remove a contract on Haddads life in exchange for Hess and the death of Faulkner. Haddad refuses and Stroebling leaves, frustrated.
 Derek Thompson) IRA ambush he participated in. Murphy shoots Hourigan dead, putting Haddad in a dilemma over Kathys existing safety. Haddad enlists his final team members, Arab businessman Mustapha El Ali (Stratford Johns) and his employees, to do a couple of minor parts of the rescue. To appease Stroebling, Haddad offers Michael as extra insurance.

Launching a coup that will change the shape of the world, Haddad must also rescue Michael and Kathy from the clutches of Stroebling. Michael creates a diversion for him and Kathy to escape but he is killed during the struggle when the guard retrieves his handgun and shoots him. Moments later Haddad kills the guards and rescues Kathy. The plan goes ahead as scheduled but Pierre is killed in the flaming wreck from the staged accident. Hess is sedated with an anasthetic, and switched with the look-alike corpse from the other ambulance and placed into a waiting jeep. At the rendezvous point at the Kaiser Wilhelm memorial Reed-Henry tries to intercept Hess, but discovers that he has been duped into killing Stroebling disguised as a guard. Kathy, Haddad and Faulkner take a drugged Hess to and from a soccer game with international passengers to their plane flight, and escape from being caught by murdering a customs officer. Reed-Henry confesses to his superiors that Hess has escaped with his rescuers and is nowhere to be found. He accepts execution via being shot with his own pistol from his superiors as his punishment.

==Epilogue==
 Spandau to live the rest of his life. The following day Haddad, Kathy and Faulkner take Hess to the French embassy where he turns himself in. An article of a newspaper in the following days tells a story about a false rumour of Hesss escape.

==Cast==
* Scott Glenn - John Haddad
* Barbara Carrera - Kathy Lukas Edward Fox - Alex Faulkner
* Laurence Olivier - Rudolf Hess
* Robert Webber - Robert McCann
* Kenneth Haigh - Colonel Reed-Henry
* Stratford Johns - Mustapha El Ali John Terry - Michael Lukas
* Robert Freitag - Karl Stroebling
* Ingrid Pitt - Hooker
* Patrick Stewart - Russian General
* Paul Antrim - RSM. Murphy Derek Thompson - Patrick Hourigan
* Michael N. Harbour - KGB Man David Lumsden - Joseph
* Malcolm Jamieson - Pierre  
* Billy Boyle - Devenish   Richard Burton - Allen Faulkner (archive footage)
* Richard Harris - Rafer Janders (archive footage)
* Winston Ntshona - Julius Limbani (archive footage)
* Brook Williams - Private Samuels (archive footage)
* Glyn Baker - Private Esposito (archive footage)

==Production== Edward Fox as Faulkners brother.

The 77 year-old Laurence Olivier, who portrayed Rudolf Hess, was in poor health during filming requiring a nurse to accompany him during production.  He was also beginning to suffer with memory problems. Edward Fox recalled him labouring for hours on his one long speech. Ingrid Pitt, who acted in the film but didn’t have any scenes with him, did have dinner with Olivier during the production and described him as "very old and frail by this time but very gallant".  Hess’s son Wolf Rudiger Hess said afterwards that Olivier’s likeness of his father was "uncannily accurate". 

Roger Moore was asked to reprise his role from the first film, but did not like the sequels script.  Lewis Collins claimed he was originally signed to play Haddad due to a contract with producer Euan Lloyd  but the role went to the American Scott Glenn.

==Notes==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 