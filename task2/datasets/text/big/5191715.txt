Khel Khel Mein
 
{{Infobox film
| name           = Khel Khel Mein
| image          = Khel khel mein.jpg
| image_size     =
| caption        = Poster
| director       = Ravi Tandon
| producer       = Ravi Malhotra
| writer         = Sachin Bhowmick
| narrator       =
| starring       = Rishi Kapoor   Neetu Singh  Rakesh Roshan  Aruna Irani
| music          = R.D. Burman  Gulshan Bawra
| cinematography = M.R Vasudev
| editing        = Waman B Bhosle, Gurudutt Shirali
| distributor    =
| released       = 1975
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| preceded_by    =
| followed_by    =
}}

Khel Khel Mein ( ) is a 1975 Indian Hindi film directed by Ravi Tandon. The films music (Ek main aur ek tu, and also Khullam khulla pyaar karenge) was composed by R.D. Burman and the lyrics written by Gulshan Bawra.
The film stars Rishi Kapoor, Neetu Singh and Rakesh Roshan as college students, who play a prank and get involved with a notorious criminal. The film turns fast from fun and frolic to a taut thriller. Iftekhar and Aruna Irani also star in the film.

The story has been adapted from the English novel "Good children dont kill", written by Louis Thomas, the blind writer, and published in 1967.

==Cast==

*Rishi Kapoor: Ajay
*Rakesh Roshan: Vikram
*Neetu Singh: Nisha
*Iftekhar: Inspector Bhupendra Singh / Black Cobra
*Yunus Parvez: Charlie
*Aruna Irani: Sherie

== Songs ==

The music for the movie was directed by R. D. Burman.

# Aye lo pyaar ke din: Kishore Kumar & Asha Bhonsle
# Ek main aur ek tu: Kishore Kumar & Asha Bhonsle Shailendra Singh
# Khullam khulla pyaar karenge hum dono: Kishore Kumar & Asha Bhonsle
# Sapna mera toot gaya: Asha Bhonsle & R. D. Burman

==Song Popularity==
The film songs (Ek main aur ek tu, Hamne tumko dekha, Khullam khulla pyaar karenge ) had high popularity among youth after release of the film:


== Summary ==

Ajay, Vikram & Nisha are three college friends who are always up to some prank. One day they spot a rich man & send him a typewritten extortion letter, just for fun. The fun turns too real when they find out that the same night the man (Jankidas) has been murdered & that the extortionists are the main suspects. As if this is not enough, their typewriter suddenly disappears & Vikram is mysteriously killed. To top it, a stranger(Dev Kumar) suddenly starts stalking them. To add to this, one police officer Bhupendra Singh(Iftekhar) frequently meets them threatening that very soon they are going to land in trouble. Can Ajay & Nisha save themselves from a game that has become deadly?

==Plot==

Ajay joins a college in Shimla for further education. He meets Vikram & Nisha, two slackers in the college who just love playing pranks. Ajay is way different from them & an easy target. But after some initial hiccups, Ajay becomes friends with them. Soon, the trio are playing pranks on unsuspecting people. One day, they spot a stingy Seth(a wealthy man) & send him a fake typewritten extortion note, hoping to relieve him of his money.

However, the next day, they find out through the newspaper that the man is dead. Based on the circumstances, they realize that they might end up as prime suspects in the case. They decide to destroy their incriminating typewriter, only to find it missing. Also, they realize that a stranger(Dev Kumar) is stalking them. Ajay & Nisha decide to tell police the truth, whether they believe it or not. But even before they can tell the truth, they are horrified to find Vikram dead.

They meet Inspector Bhupendra Singh, who is investigating the case. The Inspector is sceptical, but gives them one chance to prove their innocence. From Vikrams personal belongings, they find out about a club singer named Sherie. Anticipating that Sherie may know as to why Vikram was killed, they go to meet Sherie. But when they go to her dressing room after her performance, they find her dead. The duo realize that Sherie was involved in some shady deals and Vikram was her partner.

After trying to find out some information, they learn that Sherie acted as a middleman between a dreaded criminal named Black Cobra & the people who paid him. After learning that Black Cobra is an extortionist & the dead man was also on his payroll, blocks suddenly start falling into places. Ajay & Nisha deduce that the Seth thought the note was from Black Cobra & confronted him. The Seth thought that Black Cobra became more greedy and threatened to expose him, following which he was killed by latter.

Black Cobra thought that Vikram & Sherie were extorting people in his name, behind his back, so he killed them. He also stole the typewriter to frame Ajay & Nisha. It becomes clear that the person following them is either Black Cobra himself, or his henchman. Though they have no concrete information about Black Cobra, they find some information secretly hidden by Sherie, that might unmask Black Cobra. The stranger confronts them, but the duo overpower him & escape. They inform the Inspector about their findings. The Inspector calls them to meet him in an abandoned place.

Ajay give him all the incriminating evidence, but to his astonishment, the Inspector burns all of it. He shocks him by telling that he is none other than Black Cobra himself. Suddenly Ajay realizes that he played right into his hands. Black Cobra coolly goes on to declare that since he knows pretty much everything, he has to kill him & pin the blame of all the killings on him. He denies that he sent any man behind them. Just he is about to pull the trigger, the stranger makes an entry & after some fight the Black Cobra is put behind the bars.

To a bewildered Ajay & Nisha, the stranger reveals that he is Charlie, an undercover police officer working on the murder cases. He tells that he suspected Vikram & Sherie of running the extortion racket much before the first murder. He already knew about the prank, but was unsure of the killers identity. So he stole their typewriter. Then Cobra killed Vikram & Sherie to keep his secret intact. Then he knew that Cobra is behind all this & used the duo to track down Cobra. He reassures Ajay & Nisha that even though Black Cobra destroyed all the proof, there is no proof against the duo either. He tells the duo to collect the typewriter from him the next day. The duo oblige, vowing never to play such pranks again.



==External links==
*  

 
 
 
 