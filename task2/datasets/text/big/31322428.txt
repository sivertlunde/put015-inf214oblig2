The Council of the Gods
{{Infobox film
| name           =Der Rat der Götter
| image          = 
| image size     =
| caption        =
| director       =Kurt Maetzig
| producer       = Adolf Fischer Friedrich Wolf, Philipp Gecht
| narrator       =
| starring       =Fritz Tillamnn
| music          = Hanns Eisler
| cinematography =Friedl Behn-Grund
| editing        =Ilse Voigt
| studio        =DEFA
| distributor    = PROGRESS-Film Verleih
| released       = 1950
| runtime        =107 minutes
| country        = East Germany German
| budget         =3,000,000 East German Mark 
| gross          =
| preceded by    =
| followed by    =
}} East German black-and-white film, directed by Kurt Maetzig. It was released in List of East German films|1950.

==Plot== May Day rally.

==Cast==
*Paul Bildt as privy councilor Mauch
*Eva Pflug as Mabel Lawson
*Laya Raki as dancer
*Fritz Tillmann Dr. Hans Scholz
*Willy A. Kleinau as Mr. Lawson
*Hans-Georg Rudolph as Tilgner
*Albert Garbe as uncle Karl
*Helmuth Hinzelmann as Schirrwind
*Inge Keller as Edith Scholz
*Yvonne Merin as Claudia Mauch
*Käthe Scharf as Mrs. Scholz
*Herwart Grosse as von Decken
*Theodor Vogeler as Dr. Hüttenrauch
*Arthur Wiesner as Scholzs father
*Karl-Heinz Deickert as Dieter Scholz
*Agnes Windeck as Mrs. Mauch
*Helene Riechers as Scholzs mother

==Production==
Friedrich Wolf and his Soviet co-author, Phillip Gecht, began writing the script in the summer of 1948, shortly after the end of the    .  . friedrichwolf.de.  He was also determined to discredit Germanys old elites, both due to personal convictions and the ideological requirements of the Socialist Unity Party.  

The characters in Wolfs story were modeled on the real directors of IG Farben, and even their names sounded much alike: the films arch-villain, privy councillor Mauch, was based on Carl Krauch.  Wolf sought a director to create the film himself, and eventually chose Kurt Maetzig. The authors son, Konrad Wolf, served as an assistant-director.  The work on The Council of the Gods lasted for two years.  Maetzig later claimed that the film was made as a "documentary feature film": while the characters were basically fictional, it was based on real events. He maintained that he viewed the IG Farben trial, that was run only by the US, as the beginning of the rift between the wartime Allies and to an extent - even of the Cold War, and tried to depict it as such in the film. Seán Allan, John Sandford. DEFA: East German cinema, 1946-1992. ISBN 978-1-57181-753-2. pp. 66-67, 77.     

The filming took place on the background of the escalating Cold War. At 1949, the Socialist Unity Partys Politburo established a DEFA Commission to directly oversee all films produced in East Germany, after it found those made during 1946-7 as "lacking a saying on the matters of society". The Council was East Germanys first "massive propaganda film."   Maetzig, who had directed several socially critical pictures at that time and reprimanded by the establishment, turned to making more politically pleasing works. The Council of the Gods was "intended to be a propaganda super-production," and its style was inspired by Mikheil Chiaurelis Stalinist epics.   It was officially dedicated "to all peace-loving people of the world" and intended to show how "the IG Farben were the originators of the war".  

Principal photography took place in Halle an der Saale. As many as 500 extras were used to make the crowd scenes. Although DEFA director-general Joseph Schwabb demanded that the film would be Socialist Realist in style, only one common worker - Uncle Karl, played by Albert Grabe - was featured in the picture.   The lack of working-class heroes displeased the SED, and State Secretary for Press and Agitation Hermann Axen criticized "The Council of the Gods for over-emphasizing the roles of the capitalists. 

==Reception== National Prize, 1st degree, for their work on the film.  

The Council of the Gods was praised by the SED, and defined by it as the "most important film" of 1950;  a politburo resolution stated that it was "up to the standards required by our countrys democratic public opinion". Dagmar Schittly. Zwischen Regie und Regime. Die Filmpolitik der SED im Spiegel der DEFA-Produktionen. ISBN 978-3-86153-262-0. pp. 58-59.  Although it had a premiere in West Berlin,  and a West German distributor sought to purchase it, it was not released in the Federal Republic of Germany, which rejected as communist propaganda.  The Soviet magazine Art of Cinema claimed that the military commandants of West Berlins three occupation sectors registered an official complaint to the Soviets, claiming that the picture had such an influence on the public that it was undermining their authority.  Ivor Montagu, who watched it in East Germany, received a copy to his home in London on 22 June 1951.  

In a contemporary review of the film, West German journalist Curt Riess wrote that "almost everything in it is a fraud." Hans Günther Pflaum, Hans Helmut Prinzler. Cinema in the Federal Republic of Germany: The new German film, origins and present situation : with a section on GDR cinema : a handbook. Inter Nationes (1993). ASIN B0006F6CN8. p. 142.  The Federal Republics Catholic Film Service cited it as "noteworthy political drama made by DEFA, the finale of which culminates in a scene worthy of the peace movement à la Moscow."  At 1961, American critics Scott MacDonald and Amos Vogel cited it as a "hard-hitting propaganda film", but also as "the most important East German picture made up to date."  During 1977, film scholars Miera and Antonin Liehm claimed that it was "propaganda", as well. Miera Liehm, Antonin J. Liehm . The Most Important Art: Soviet and Eastern European Film After 1945. ISBN 0-520-04128-3. pp. 77, 88.   

David Caute noted that "The Council of the Gods was the first picture to "fully embrace the ideological animosities of the Cold War", and that is tried to demonstrate that beside their wartime collaboration, IG Farben, Standard Oil and the capitalists dominating both were preparing a new war.   pp. 263-264.  Alexander Stephan pointed out that it was the first to "articulate anti-capitalist economical positions" and aimed them against America.  Daniela Berghan shared this view, writing that the film asserted that the capitalist economical structures brought about WWII, and that they remained intact both in the United States and in West Germany.  Bernd Stöver claimed that the film was part of a propaganda campaign run by the East German government in the early stages of the Cold War, the message of which was not only that capitalism is aggressive by nature, but also that the post-Nazi magnates of the Federal Republic were planning to resume "Hitlers great crusade against socialism" in the immediate future, with the aid of their Western allies.   p. 54.  Ralf Schenk wrote that the American representative was portrayed as a "latent fascist", keen to fight a new war against the Soviet Union.  

Ursula Heukenkamp noted that the portrayal of the main protagonist, Dr. Scholz, and the main antagonist, Mauch, was typical to communist cinema: the first was merely an insignificant part of a huge company, who could not stop developing chemicals even when he realized they were used to gas millions, and only became free when he embraced socialism; the second was interested in profit alone, with no regard from which side the money came. She also wrote that the film was the last to depict the horrors of the Second World War from the viewpoint of passive victims; henceforth, East German cinema turned to concentrate on the active resistance of the anti-fascists.   

On a 2006 interview, Kurt Maetzig told Markus Wolf that he still regarded the film as an important work, and not as one of those he regretted making. 

==References==
 

==External links==
*  
*  on ostfilm.de.

 

 
 
 
 
 
 