Léon la lune
 

{{Infobox film
| name           = Léon la lune
| director       = Alain Jessua
| writer         = Robert Giraud and Alain Jessua
| starring       = Léon la Lune
| music          = Henri Crolla and André Hodeir 
| cinematography = Wladimir Ivanov
| producer = A.J. Films
| released       =  
| runtime        = 16 minutes
| country        = France
| language       = French
}}

Léon la lune ( ) is a 1956 French short documentary film directed by Alain Jessua. The film won the Prix Jean Vigo in 1957. The film documents an old drifter in Paris in the poetic realist style. 

Jessua was inspired by Jean-Paul Cléberts book "Paris Insolite"  (1952) and decided to make a film about a clochard  or tramp. The poet and novelist Robert Giraud, an expert on the Parisian underworld, introduced Jessua to Léon la Lune, a vagrant whose real name was Leon Boudeville and suggested they follow him from day to night. After completing the film Giraud showed it to the poet and screenwriter Jacques Prévert who wrote an introduction and asked Henri Crolla to contribute some music to the film.

Léon la lune also appeared in the series Clochards by Robert Doisneau, the pioneer of humanist photojournalism. 

==Cast==
* Léon la Lune aka Leon Boudeville

==References==
 

==External links==
* 
* Prix Jean Vigo, February 10–December 30, 2006, Moma - http://www.moma.org/visit/calendar/film_screenings/3140
* Robert Giraud, scénariste d’Alain Jessua, Le Monde blog, 1 January 2007, http://robertgiraud.blog.lemonde.fr/2007/01/01/robert-giraud-scenariste-dalain-jessua/
* Forum Des Images, http://www.forumdesimages.fr/Collections/notice/VDP1024&usg=ALkJrhgKWD8aFGj58ZKVeDBFHw7RHc9R9A

Poetic realism

 
 
 
 
 
 
 
 
 
 


 
 