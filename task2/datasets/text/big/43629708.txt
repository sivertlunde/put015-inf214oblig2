Darling (2015 film)
{{Infobox film
| name           = Darling
| image          = Darling_Movie_Poster.jpg
| alt            =  
| caption        = 
| director       = Sam Anton
| producer       = Allu Aravind K. E. Gnanavel Raja
| writer         = Maruthi Dasari Sam Anton
| starring       = G. V. Prakash Kumar Nikki Galrani Karunas Bala Saravanan
| music          = G. V. Prakash Kumar
| cinematography = Krishnan Vasant
| editing        = Ruben
| studio         = Geetha Arts Studio Green
| distributor    = Dream Factory
| released       =  
| runtime        = 126 minutes
| country        = India
| language       = Tamil
| budget         = 
| gross          =
}} Tamil horror comedy film directed by debutant Sam Anton and produced by Allu Aravind and Studio Green|K. E. Gnanavel Raja. The film features composer G. V. Prakash Kumar, in his acting debut, and Nikki Galrani in the lead roles, with the former also composing the films music. A remake of the 2013 Telugu film Prema Katha Chitram, the film was released on 15 January 2015. It got positive reviews and became a blockbuster at the box office. 

==Plot==
 
Kathir (G. V. Prakash Kumar), Kumaran and Nisha plan a combined and flawless suicide because of their failures in their respective lives. Firstly they try to fulfill their last wishes before leaving this world. Nisha wants to steal a new car and Kathir wants to slap the local MLA in his own house.

After stealing a new car and slapping the local MLA, the trio flee away from there to a nearby resort after escaping from the police. There Athisaya Raj, yet another suicide aspirant joins them and the four reach the beach house. There, Kumaran and Nisha, who is in love with Kathir, plan to postpone their suicide for 3 days so that they can prevent Kathir from committing suicide due to a Love Failure.

Their plan turns successful and in that interval of 3 days, all the four get close to each other. Kathir develops feelings for Nisha but hesitates to tell to her for fear of being humiliated by her. At the end of the 3rd day, Kumaran asks Nisha to kiss Kathir to divert from death, as per male psychology. He meanwhile requests Kathir to kiss Nisha as she wants to experience her first kiss before dying to which Kathir accepts with shame and shyness.

As he tries to kiss her, a ghost enters the body of Nisha, who forces Kathir out of there. A flabberghasted Kathir runs out of the room. Whenever Nisha shows her feelings to Kathir and Kathir reacts to it by hugging or touching her, the ghost enters her body and scares Kathir away. Unaware of the ghosts entry into her body, Nisha feels depressed.

When Kumaran hears Kathir describing Nisha as a ghost, he rushes to Nisha s room to scold her and ask for an apology to Kathir, only to see the ghost enter her body and thrash him. That night, Raj too experiences the Ghosts fury at its villainous best. The trio decide to distance themselves from her and always stay together.

From the next morning, they try to get the ghost vacate Nisha body and call upon Ghost Gopal Varma an exorcist but the ghost thrashes him too.Afterwards Kathir brings up the courage and asks the ghost the reason for vengeance, and the ghost narrates her story to the trio.The ghost is Shruthi who comes with her newly wed husband, Shiva to the beach house.Five guys enter the beach house and rape the girl and kill them.

Kathir is emotionally touched and before he could help the ghost, Nisha, who seeing Kathir avoid her, slits her wrist to make Kathir get rid of the ghost. Kathir seeing this brings her out of the house to get her to the hospital, only to be confronted by 5 men who had previously raped Shruthi. Kathir puts on a fight with them, only in vain. The ghost too has lost her powers due to the slit wrist. But once the ghost in Nisha calls for her husband, we see Kathir possessed by Shiva who fights the villains.

The film ends with Kathir and Nisha hugging in front of a refrigerator. We see the refrigerator showing Shruthi and Shiva in the reflection meaning that their ghosts are still inside Kathir and Nisha.

==Cast==

* G. V. Prakash Kumar as Kathirvelan / Kathir
* Nikki Galrani as Nisha
* Bala Saravanan as James Kumaran
* Karunas as Athisaya Raaj / Athisayam
* Rajendran as Ghost Gopal Varma (Cameo Appearance)
* Kalaiyarasan as Shiva
* Srushti Dange as Swathi, Kathirs Ex-Girlfriend

==Production==
The film marks the return of Telugu film producer Allu Aravind to the Tamil film industry after 25 years,  and he collaborates with K. E. Gnanavel Raja of Studio Green, who works outside the production house for the first time. The film was distributed by Dream Factory.  The team features a predominantly new technical crew with director Sam Anton making his debut, by choosing to remake the Telugu horror film Prema Katha Chitram (2013). G. V. Prakash Kumar was signed on to play the lead role and compose the music, and the film was completed and will release earlier than his two other ongoing projects which had started earlier, Pencil (film)|Pencil and Trisha Illana Nayantara. Nikki Galrani and Srushti were also signed on to play pivotal roles, with the shoot of the film being completed in just over a months time. 

==Soundtrack==
Soundtrack was composed by G. V. Prakash Kumar while lyrics were written by Na. Muthukumar and Arun Kamaraj. The album was released at Suryan FM Studios on November 26, 2014.  Behindwoods rated the album 2.7 out of 5 and called it "Melancholic GV".  Ibtimes reviewed the album as one of the finest of his career and opined that the album consists of different genres of music sung by versatile singers.  Indiaglitz rated 2.5 out of 5 and called it "middling album with two gorgeous melodies". 

{{tracklist
| headline        = Tracklist 
| extra_column    = Singer(s)
| total_length    = 28:16
| title1          = Vandha Mala
| extra1          = Gaana Bala, Karunas, Arun Raja Kamaraj
| length1         = 4:06
| title2          = Unnale
| extra2          = Shankar Mahadevan, Shreya Ghoshal
| length2         = 5:45
| title3          = Sattena Idi Mazhai Megha
| length3         = 4:17
| title4          = Un Vizhigalil
| extra4          = Harini
| length4         = 5:04
| title5          = Anbe Anbe
| extra5          = G. V. Prakash Kumar
| length5         = 6:16
| title6          = The Dead are Back
| extra6          = Theme Music
| length6         = 2:40
}}

==Release== Sun TV. The film released on 15 January 2015 coinciding with Pongal in over 400 screens worldwide alongside S. Shankars  I (film)|I  and Sundar C.s Aambala. 
  I and Sundar.Cs Aambala.

===Critical reception=== IANS wrote, that it was "better than its original for the simple reason that it manages to entertain with a new set of actors", going on to add, "Despite its flaws, Darling turns out to be a crowd-pleasing tale of horror. While there arent many thrills to send chills down your spine, the comedy gives you company throughout".  Behindwoods gave 2.75 out of 5 and wrote, "Darling is an entertainer and can make an enjoyable Pongal outing for people who enjoy scary teasers".  The Hindu wrote, "The problem with Darling is that despite the jokes coming thick and fast, the story seems tedious...while the scenes, for the major part, are admittedly funny, you can’t wait to move on. On some level, you also know that a back story of the ghost is waiting. And even as you break into occasional laughter, you are twiddling your thumbs in impatience, waiting for it".  The two awesome melodies reached audista tamiltop10 from the day of release of soundtracks 

===Box office===
The film opened to average note on its opening day because of I and Aambala release, the film collected  4.1 crore in its first day which is the biggest opening ever for debut hero.The film collected   in its final worldwide collection.The film declared blockbuster at the box office. 

==References==

 

==External links==
*  

 
 
 
 
 
 
 