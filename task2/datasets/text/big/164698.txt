Reign of Fire (film)
 
{{Infobox film
| name           = Reign of Fire
| image          = Reign of Fire movie.jpg
| caption        = Theatrical release poster
| alt            = A dragon flying over the British Houses of Parliament breathing fire on the city below.  Rob Bowman
| producer       = Richard D. Zanuck  Lili Fini Zanuck  Roger Birnbaum  Gary Barber  Dean Zanuck
| writer         = 
| story          = Gregg Chabot  Kevin Peterka
| screenplay     = Matt Greenberg  Gregg Chabot  Kevin Peterka
| starring       = Christian Bale  Matthew McConaughey  Izabella Scorupco  Gerard Butler Ed Shearmur  Brad Wagner  Mad at Gravity
| cinematography = Adrian Biddle
| editing        = Declan McGrath  Thom Noble
| studio         = Touchstone Pictures  Spyglass Entertainment Buena Vista Pictures
| released       = July 12, 2002
| runtime        = 102 minutes 
| country        = United Kingdom United States 
| language       = English
| budget         = $60 million 
| gross          = $82.1 million   
}} action fantasy Rob Bowman and starring Christian Bale, Matthew McConaughey and Gerard Butler. It takes place in the year 2020 in England, after dragons have reawakened.
The film grossed about $82 million on a $60 million budget. 

==Plot== hibernating European dragon awakens, incinerating the construction workers. The only survivor is a boy named Quinn Abercromby (Ben Thornton) whose mother, Karen (Alice Krige), was crew chief on the project. She is crushed to death while protecting Quinn as the dragon makes its way to the surface. The dragon flies out and more dragons begin appearing, multiplying rapidly. It is shown through various newspaper clippings and a voice-over that scientists discovered that dragons are a lost species that are responsible for the dinosaurs extinction by burning them all to ash. The speculation was that dragons instinctively hibernate after destroying most of the Earths living creatures, waiting until the Earth repopulates. In the months and years following the awakening of the dragons, humanity tried to resist using their combined military might, including nuclear weapons in 2010, but this only hastened the planets destruction, and within a few years, humanity was almost entirely wiped out.
 David Kennedy) steals a truck to take his group to harvest tomatoes. Picking the crops too early wont allow the seeds to germinate so they can be used the next season. The group is attacked by a lone dragon, one is killed and the rest are surrounded by fire. They are rescued by Quinn, Creedy (Gerard Butler), and Jared (Scott Moutter) using old fire engines and firesuits. But while trying to escape, the dragon kills Eddies son, who was manning a water cannon.

Later, a group of United States|Americans, the Kentucky Irregulars, arrive, led by Denton Van Zan (Matthew McConaughey), including a Chieftain tank and an Agusta A109 helicopter, piloted by Alex Jensen (Izabella Scorupco). Van Zan and his heavily armed soldiers have an elaborate tracking system to hunt dragons, and Van Zan knows their one weakness—poor vision just before sunset. Quinn is initially distrustful and surprised that anyone can kill dragons. After an intense discussion, Van Zan convinces Quinn to cooperate. Working together, Quinn and Van Zan kill the dragon that destroyed the crops.

The inhabitants of the castle celebrate, only to be chastised by Van Zan, who lost three men in the hunt. Later, Van Zan tells Quinn that all the dragons they have encountered have been female. They have also found that all females carry unfertilized eggs and that their metabolism is so high that they live only a few months. The Americans believe there is only one male and that if they kill it, the species will be unable to reproduce. Quinn knows about the male dragon since it is the one that killed his mother. He refuses to help since the male has attacked and destroyed any group who went after it before.

Van Zan orders his soldiers to conscript the best men at the castle. Quinn becomes enraged and attacks Van Zan, but loses the fight. Quinn announces that if Van Zans group finds the male it will kill them and backtrack to the castle. Ignoring Quinn, Van Zans group head to London, but the male dragon sets a trap for them and attacks them on the road, killing most of the soldiers. The male then finds the castle and kills most of its residents. Some survive in the underground bunker. Quinn tries to get more people to safety but Creedy stops him and goes in his place, but is killed when the male attacks for a second time. 

Van Zan and Jensen return to the castle and free the residents trapped in the bunker. Quinn tells Van Zan hell help them hunt the male in London, which is the main nesting ground. Quinn knows the males nest is near the construction site where his mother was killed. They fly to London, where they find hundreds of dragons. They witness the smaller dragons being cannibalized by the much larger male. Van Zans plan is to fire a magnesium and C-4 explosive charge with a crossbow bolt down the dragons throat during the brief moment it opens its mouth to inhale and breathe out fire. Van Zan fires but the male destroys the arrow before it can hit. Knowing he missed, Van Zan jumps off the building to attack the male with his giant axe, but is caught in mid-air and eaten. Quinn and Alex lure the dragon to street level, where Quinn is able to fire his own explosive crossbow bolt into the dragons mouth, killing it.

Quinn and Alex are later seen sitting on a sunny hill overlooking the North Sea, Quinn telling her there have been no dragon sightings for over three months. They are there to erect a radio tower. Jared rides up to tell them they have made radio contact with another group of survivors in France, who wish to speak to their leader. Quinn tells Jared he is now the leader and sends him back to talk to the French. Quinn then announces his dedication to rebuilding. He views the possibility of the dragons return as remote; he says that, if they ever return, "Theyll burn. Well build. Maybe Ill just kill em".

==Cast==
* Christian Bale as Quinn Abercromby
* Matthew McConaughey as Denton Van Zan
* Gerard Butler as Creedy
* Izabella Scorupco as Alex Jensen
* Scott Moutter as Jared Wilke David Kennedy as Eddie Stax
* Alexander Siddig as Ajay
* Ned Dennehy as Barlow
* Rory Keenan as Devon
* Terence Maynard as Gideon
* Doug Cockle as Goosh
* Randall Carlton as Burke
* Chris Kelly as Mead
* Ben Thornton as Young Quinn
* Alice Krige as Karen Abercromby

==Production== outbreak of foot and mouth disease in Europe which, due to restrictions in place, meant that many planned sequences could not be shot. The design and construction of the dead dragon were provided by Artem and the special visual effects provided by The Secret Lab.

==Soundtrack==
{{Infobox album
| Name = Reign of Fire: Original Motion Picture Soundtrack 
| Type = Film score
| Artist = Edward Shearmur  Digital download / Audio CD)
| Cover = 
| Released = July 23, 2002
| Length = 50:30
| Label = Varèse Sarabande
}}
# "Prologue" – 3:22
# "Enter the Dragon" – 3:20
# "An Early Harvest" – 2:42
# "Field Attack" – 4:11
# "Marauders" – 2:47
# "Meet Van Zan" – 3:49
# "Archangels" – 3:58
# "Dawn Burial" – 3:02
# "A Battle of Wills" – 5:31
# "The Ruins at Pembury" – 2:11
# "Inferno" – 3:23
# "Return to London" – 4:11
# "Magic Hour" – 5:23
# "Rebirth" – 2:40

==Reception==
The film opened at #3 in the U.S. box office in the weekend of July 12, 2002, taking in $15,632,281 USD in its first opening weekend, behind Road to Perdition and Men in Black II, which was on its second week at the top.

Rotten Tomatoes gives the film 40% on the based on 154 reviews, with the sites consensus calling it "an enjoyable B-movie if you dont use your brain".  Metacritic gives it a score of 39% based on 30 reviews from critics.
 ABC had terrorist attack on London that day, it was replaced with another movie, Big Fat Liar. 

==Awards== Saturn Award, one award from Festival de Cine de Sitges, and won one other Sitges award: 
{| class="wikitable"
|-
! Group !! Award/Nomination
|- Academy of Science Fiction, Fantasy & Horror Films (Saturn Awards|2002) Best Fantasy Film
|- 
|rowspan=2| Festival de Cine de Sitges (2002)
| Best Visual Effects
|-
| Best Film
|}

==Video Game== Reign of Xbox and Gamecube, and received mediocre reviews. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 