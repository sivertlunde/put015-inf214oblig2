The Trial of Mary Dugan (film)
{{infobox film
| name           = The Trial of Mary Dugan
| image          =
| caption        =
| director       = Bayard Veiller
| producer       = Louis B. Mayer
| writer         = Bayard Veiller (play) Becky Gardiner (screenplay)
| starring       = Norma Shearer
| music          =
| cinematography = William H. Daniels
| editing        = Blanche Sewell
| distributor    = Metro Goldwyn Mayer
| released       =  
| runtime        = 12 reels (10,621 feet)
| country        = United States
| language       = English
}}
The Trial of Mary Dugan is a 1929 talking film produced and distributed by MGM and starring Norma Shearer. The film is based on the 1927 Broadway stage play, The Trial of Mary Dugan, by Bayard Veiller who also directed this film. On stage the play had starred Ann Harding (in Shearers role), who would come to Hollywood a few years later at the beginning of talkies. This was Veillers first and only sound film directorial effort as he had directed several silent films before 1922.   

==Cast==
*Norma Shearer- Mary Dugan
*Lewis Stone - Edward West
*H. B. Warner - District Attorney Galway
*Raymond Hackett - Jimmy Dugan
*Lilyan Tashman - Dagmar Lorne
*Olive Tell - Mrs. Gertrude Rice
*Adrienne DAmbricourt - Marie Ducrot
*DeWitt Jennings - Inspector Hunt
*Wilfrid North - Judge Nash
*Landers Stevens - Dr. Welcome
*Mary Doran - Pauline Agguerro
*Westcott Clarke - Captain Price
*Charles R. Moore - James Madison
*Claud Allister - Henry James Plaisted
*Myra Hampton - May Harris

 

Cast notes:
* Thomas A. Curran the early American silent film star plays an uncredited bit part.

==References==
 

==External links==
* 
* 
* 
* 

 
 
 
 
 