Scarface (1932 film)
{{Infobox film
| name           = Scarface
| image          = Scar2.gif
| caption        = Theatrical release poster Richard Rosson
| producer       = Howard Hughes Howard Hawks
| based on       =  
| screenplay     = Seton I. Miller John Lee Mahin W. R. Burnett
| story          = Ben Hecht
| starring       = Paul Muni Ann Dvorak Osgood Perkins Karen Morley Boris Karloff
| cinematography = Lee Garmes L.W. OConnell
| editing        = Edward Curtiss
| studio         = The Caddo Company
| distributor    = United Artists
| released       =  
| music          = Shelton Brooks
| runtime        = 93 minutes
| country        = United States gross = $600,000 {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | isbn = 978-0-299-23004-3
 }} p111 
| language       = English Italian
}}
 Richard Rosson, and based on Armitage Trails 1929 novel of the same name, which is loosely based on the rise and fall of Al Capone. The film features Ann Dvorak as Capones sister, and Karen Morley, Osgood Perkins, and Boris Karloff. One of a number of pre-Code crime films, the film centers on gang warfare and police intervention when rival gangs fight over control of Chicago. A version of the St. Valentines Day massacre is also depicted.
 1983 film of the same name starring Al Pacino.

==Plot==
  1920s Chicago, Italian immigrant South Side. speakeasies and Irish gangs North Side.  Tony soon starts ignoring these orders, shooting up bars belonging to OHara, and attracting the attention of the police and rival gangsters.  Johnny starts realizing that Tony is out of control and has ambitions to take his position.  

Meanwhile, Tony pursues Johnnys girlfriend Poppy (Karen Morley) with increasing confidence.  At first, she is dismissive of him but pays him more attention as his reputation rises, at one point visiting his "gaudy" apartment where he shows her his view of an electric billboard advertising the slogan for Thomas Cook & Son|Cooks Tours that has inspired him: "The World is Yours."
 impersonating police officers to gun down several rivals in a garage. Tony also kills Gaffney as he makes a strike at a bowling alley. Believing that his protegé is trying to take over, Johnny arranges for Tony to be assassinated while driving in his car.  Tony manages to escape this attack, and he and Guino kill Johnny, leaving Tony as the undisputed boss of the city.

Tonys actions have provoked a public outcry, and the police are slowly closing in. After seeing his beloved sister Francesca "Cesca" (Ann Dvorak) with Guino, Tony kills his friend before either he or Cesca can inform him of their secret marriage.  His sister runs out distraught and tells the police what he has done. The police move to arrest Tony for Guinos murder.  Tony holes up in his house and prepares to shoot it out.  Cesca comes back, planning to kill him, but ends up helping him to fight the police. Moments later, however, she is killed by a stray bullet. As the apartment fills with tear gas, Tony leaves down the stairs, and the police confront him. Tony pleads for his life, but then makes a break for it, only to be gunned down by the police. Outside, the electric billboard blazes, "The World is Yours."

==Cast==
* Paul Muni as Antonio "Tony" Camonte
* Ann Dvorak as Francesca "Cesca" Camonte
* Karen Morley as Poppy
* Osgood Perkins as John "Johnny" Lovo
* C. Henry Gordon as Inspector Ben Guarino
* George Raft as Guino Rinaldo
* Vince Barnett as Angelo
* Boris Karloff as Gaffney
* Purnell Pratt as Garston
* Tully Marshall as Managing Editor
* Inez Palange as Mrs. Camonte
* Edwin Maxwell as Chief of Detectives
* Harry J. Vejar as Big Louis Costillo
* Douglas Walton as Cescas Boyfriend

==Production notes==
 
===Background===
The film was adapted by Ben Hecht in only 11 days   from Armitage Trails 1929 novel Scarface and then Seton I. Miller, John Lee Mahin, and W. R. Burnett did additional writing related to continuity and dialogue. Trail, whose real name was Maurice Coons, wrote for a number of detective-story magazines during the early 20s, but died of a heart attack at the age of 28, shortly before the release of the 1932 film. 

The film is loosely based upon the life of Al Capone whose nickname was "Scarface". Capone was rumored to have liked the film so much that he owned a print of it.  Ben Hecht also said that Capones men came to visit him to make sure that the film was not based on Capones life.  When he said the film was fictitious, the two men working for Capone left Hecht alone. The introduction for the films screening on Turner Classic Movies even stated that Hecht convinced the men to work as consultants for him.

The most obvious references to Capone and actual events from the Chicago gang wars—especially to audiences at the time of the films release—are: 
* Like the Paul Muni character, Al Capone had a large scar on the side of his face. We also learn in the film that the Muni character got the scar in a barroom brawl, which is essentially how Capone received his.
* Tonys killing of his boss, "Big Louie" Costillo, in the lobby of his club (Capone was involved in the murder of his first boss, Big Jim Colosimo|"Big Jim" Colosimo, in 1920).  Charles Dion OBannion in his flower shop in 1924).
* Gaffney leads a caravan of cars in a drive-by shooting at Tony in a restaurant (Capones rival, Hymie Weiss, did the same thing to him in 1927).  Angelo Genna was murdered following a car chase in 1925).
* The shooting murder of seven men in a garage, with two of the gunmen costumed as police officers (the St. Valentines Day Massacre|St. Valentines Day Massacre of 1929). Also, the leader of this rival gang in the film (played by Boris Karloff) narrowly escapes the shooting, which is precisely what happened to gang leader Bugs Moran in the actual St. Valentines Day Massacre.

===Issues with censors===
The original script had Tonys mother loving her son unconditionally, accepting his lifestyle, and even accepting money and gifts from him. In addition, there was a politician who despite campaigning against gangsters on the podium, is shown partying with them after hours. The script ending had Tony staying in the building, unaffected by tear gas and a multitude of bullets fired at him. It is not until the building is on fire that Tony is forced to exit the building, guns blazing. He is sprayed with police gun fire but appears unfazed. Upon noticing the police officer whos been arresting him throughout the film, he fires at him, only to hear a single "click" noise implying that his gun is empty. He is then killed after being shot several times by said police officer. A repeated clicking noise is heard on the soundtrack implying that he was still attempting to fire while he was dying. 

After repeated demands for a script rewrite from the Hays Office, Howard Hughes ordered Hawks to shoot the film, "Screw the Hays Office, make it as realistic, and grisly as possible." 
 Little Caesar (January 1931) and The Public Enemy (April 1931). As is the case with Scarface, both of these films were also based on earlier novels.

===Filming=== Metropolitan Studios, Harold Lloyd Studios and the Mayan Theater in Los Angeles. Shooting took three months with the cast and crew working seven days a week. Several accidents happened on the set. Comedian Harold Lloyds brother Gaylord Lloyd lost an eye when he visited the set and was accidentally shot with live ammunition. George Raft also received a head injury during the death scene of his character when he accidentally hit the door frame while he was slumping to the floor. 

====Alternate ending====
The first version of the film (Version A) was completed on September 8, 1931, but censors would not allow its release because of concerns that it glorified the gangster lifestyle and showed too much violence. Several scenes had to be edited, the subtitle "The Shame of the Nation" as well as a text introduction had to be added, and the ending had to be modified.

The alternate ending (Version B) differs from the original ending in the manner that Tony is caught and in which he dies.  Unlike the original ending in which Tony escapes the police and dies after getting shot several times, the alternate ending starts with Tony reluctantly handing himself over to the police. After the encounter, Tonys face is not shown again. A scene follows in which a judge is addressing Tony during sentencing. The next scene is the finale, in which Tony (seen from a birds eye view) is brought to the gallows, where he is finally put to an end by being hanged.

However, Version B still did not pass the New York censors, so Howard Hughes disowned it and finally in 1932 released Version A—with the added text introduction—in states that lacked strict censors (Hughes also attempted to take the New York censors to court). This 1932 release version led to bona-fide box office status and positive critical reviews.  Hughes also made an attempt to release the film under the title "The Scar" when the original title was disallowed by the Hays office.

==="X" motif===
Hawks used an "X" motif throughout the film (seen first in the opening credits) that was chiefly associated with death -  appearing many times (but not all) whenever a death is portrayed; the motif shows up in numerous places, most prominently as Tonys "X" scar on his left cheek.

===Cultural references===
The serious play that Tony and his friends go to see, leaving at the end of Act 2, is John Colton and Clemence Randolphs Rain, based on W. Somerset Maughams story "Miss Sadie Thompson". The play opened on Broadway in 1922 and ran throughout the 1920s (A film version of the play, also titled Rain (1932 film)|Rain and starring Joan Crawford, was released by United Artists the same year as Scarface).

===Source music=== Wreck of the Old 97."

==Legacy== 1983 remake starring Al Pacino. The 2003 DVD "Anniversary Edition" limited edition box set of the 1983 film included a copy of its 1932 counterpart. At the end of the 1983 film, a title reading "This film is dedicated to Ben Hecht and Howard Hawks" appears over the final shot.

In 1994, Scarface was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". The character of Tony Camonte ranked at number 47 on AFIs 100 Years... 100 Heroes and Villains list. 
  in a later film trailer.]]
The movie launched George Rafts lengthy career as a leading man. Raft, in the films second lead, had learned to flip a coin without looking at it, a trait of his character, and he made a strong impression in the comparatively sympathetic but colorful role. (It was Howard Hawks idea to get Raft to use this in the film to camouflage his lack of acting experience.)    A reference is made in Rafts later role as gangster Spats Columbo in Some Like it Hot (1959), wherein he asks a fellow gangster (who is flipping a coin) "Where did you pick up that cheap trick?"

The film was named the best American sound film by critic and director Jean-Luc Godard in Cahiers du Cinéma.

In June 2008, the American Film Institute revealed its AFIs 10 Top 10|"Ten Top Ten"—the best ten films in ten "classic" American film genres—after polling over 1,500 people from the creative community. Scarface was acknowledged as the sixth best in the gangster film genre. 

On the review aggregate website Rotten Tomatoes, Scarface holds a 100% "Fresh" rating with all 27 reviews being positive. 

Universal announced in 2011 that the studio is developing a new version of Scarface. The studio claims that the new film is neither a sequel nor a remake, but will take elements from both this and the 1983 version, including the basic premise of a man who becomes a kingpin in his quest for the American Dream. Martin Bregman, who produced the remake, will produce this version,  and David Ayer will pen the screenplay. 

==References==
 

==External links==
 
*  
*  
*  
*  
*   by Stephen Jacobs at Creativescreenwriting.com
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 