Legend of Tianyun Mountain
{{Infobox film image          =  caption        =  name           = Legend of Tianyun Mountain writer         = Lu Yanzhou (novel) starring       = Wang Fuli Shi Jianlan Shi Weijian Zhong Xinghuo director       = Xie Jin cinematography = Xu Qi music          = Ge Yan producer       =  distributor    =  runtime        =  released       = 1980 country        = Peoples Republic of China language  Mandarin
|budget         =
}}
Legend of Tianyun Mountain ( ) is a 1980 Chinese film about Chinese peoples sufferings from the long-term political campaigns from "Anti-rightists" in 1950s until the fall of Gang of Four. It depicted the individuals hardship in the political turmoils and critically reflected the impact of that special history after the formation of the Peoples Republic of China. The film was based on the novel written by Lu Yanzhou, and was directed by Xie Jin, starring Wang Fuli, Shi Weijian, Zhong Xinghuo and Shi Jianlan.

==Plot==
In the early 1950s, Song Wei and Feng Qinglan, two female newly graduates from school, join Tianyun Mountain Exploration Team. Song falls in love with Luo Qun, the newly appointed political commissar. In spring of 1957, Song is sent to study in the Party school and joins the Communist Party of China|CPC. When these two plan to get married, the Anti-Rightist Movement outbreaks, and Luo is classified as "rightist" and deprived of his post. Wu Yao, who leads the political campaign in Tianyun Mountain, forces Song to break up with Luo. Under the political pressure, Song sends Luo a farewell letter, and later, marries Wu. Luo is sent to do drudgery in a rural village. On the other hand, Feng, who has hidden her admiration and love for Luo, leaves the exploration team and comes to Luo. They get married, despite outside duress, and live an impoverished but content life. 20 years later and after the fall of Gang of Four, Wu becomes the vice Party chief and head of the organization department of the region, and Song is also elevated to vice director of the organization department, though she has no real power. One day, a young girl, Zhou Yuzhen, tells Song about her encounter with a coachman named Luo Qun in Tianyun Mountain, who is still a non-rehabilitated rightist. Song feels guilty and decides to rectify Luos injustice by herself. But her husband, Wu, viciously interferes with the case. Song stands up to the resistance and appeals to higher-ranking officers, and finally, Luos case is straightened out. When the good news comes, however, the flame of Fengs life extinguishes due to long-drawn-out hardship. In the end, from a distance Song sees Luo standing in silent tribute in front of the grave of Feng.

==Cast==
*Wang Fuli as Song Wei;
*Shi Weijian as Luo Qun;
*Shi Jianlan as Feng Qinglan;
*Zhong Xinghuo as Wu Yao;
*Hong Xuemin as Zhou Yuzhen

==Criticism==
This was the first time director Xie Jin focused on Anti-Rightist Movement, and faithfully displayed that particular history on the screen. The film boldly and intensively revealed that historic tragedy that honest people were mistakenly condemned to "rightist". The film tried to analyze the historic lessens from various aspects including politics, ethics and morality, and address the origins of the tragedy. It mixed the individuals personality, emotional changes with the political atmosphere, social problems and history development, underscoring the theme. The film won the Best Picture, Best Director, Best Cinematography, Best Art Design awards in the first Golden Rooster Awards in China in 1981.

== External links ==
* 
* 
*  at the Chinese Movie Database

 
 
 
 
 
 
 