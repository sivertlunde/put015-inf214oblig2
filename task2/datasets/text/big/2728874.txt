Happiness Is in the Field
{{Infobox film
| name         = Happiness Is in the Field  (Le bonheur est dans le pré) 
| caption      =
| image        =
| writer       = Florence Quentin Catherine Jacob Yolande Moreau
| narrator     =
| director     = Étienne Chatiliez
| producer     =
| music        =
| cinematography =
| editing      =
| distributor  =
| released     =  
| runtime      = 106 min French
| country      = France
| budget       = $10,200,000
| gross        = $61,889,708 
}}

Happiness Is in the Field (French: Le bonheur est dans le pré) is a French comedy directed by Étienne Chatiliez in 1995.

== Plot ==
Francis Bergeade, owner of a toilet seats and brushes factory in Dole, Jura|Dole, has just turned 65 and his life is a misery. Tax services are harassing him, his snobby wife Nicole despises him, and his daughter whimsically wants to have an expensive wedding. Francis knows only moments of relief while having lunches or dinners in fancy restaurants with his best friend, car dealer Gérard. Stress become too overwhelming and while on a lunch, he suffers an attack from a blocked nerve. 

During his convalescence, his family watch a reality television show about long-lost relationships and disappearances called "Où es-tu?" ("Where are you?"). Amongst the cases, a Spanish-born woman, Dolorès Thivart, and her daughters, "Zig" and "Puce", producers of duck foie gras from Condom, Gers|Condom, are looking for their husband and father, Michel, who vanished 27 years ago. Michel Thivart happens to be Franciss exact lookalike.

For Francis, it means more problems to come...or is it the start of a brand new life?

==Cast==
* Michel Serrault as Francis Bergeade 
* Eddy Mitchell as Gérard Thulliez
* Sabine Azéma as Nicole Bergeade 
* Carmen Maura as Dolores Thivart 
* François Morel as Pouillaud 
* Guilaine Londez as Zig Thivart 
* Virginie Darmon as Puce Thivart 
* Alexandra London as Géraldine Bergeade
* Christophe Kourotchkine as Rémi 
* Jean Bousquet as Père Léonard 
* Eric Cantona as Lionel 
* Joël Cantona as Nono  Catherine Jacob as Madame André 
* Daniel Russo as André 
* Roger Gicquel as Charles
* Yolande Moreau as Lucette
* Isabelle Nanty as A Worker

==Awards==
Eddy Mitchell received the 1996 César Award for Best Actor in a Supporting Role for his role in the film.

==References==
 

== External links ==
*  
*  

 
 
 
 

 