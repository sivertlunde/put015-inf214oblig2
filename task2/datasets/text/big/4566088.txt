The Great Macarthy
 
{{Infobox Film
| name           = The Great Macarthy
| image          =
| caption        = A promotional poster for the film David Baker
| producer    = Richard Brennan David Baker
| writer         = John Romeril
| based on = the novel A Salute to the Great Macarthy by Barry Oakley
| starring       = John Jarratt   Barry Humphries   Judy Morris
| cinematography = Bruce McNaughton
| editor = John Scott music = Bruce Smeaton
| studio    = Stoney Creek Films
| distributor = Seven Keys
| released       = 7 August 1975
| country        = Australia
| runtime        = 90 minutes
| language       = English
| budget = A$250,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p289 
| gross          =
}}

The Great Macarthy is a 1975 comedy about Australian rules football. It was an adaptation of the novel A Salute to the Great Macarthy by  Barry Oakley. It stars John Jarratt as the title character (in his film debut) as a local footballer who is signed up (or more appropriately, kidnapped) by the South Melbourne Football Club (now Sydney Swans).  It also stars Barry Humphries and Judy Morris. It was released at a time when Australian films were starting to re-emerge. It was not very successful despite its high profile cast.

==Plot==
MacArthy is a county football player who is kidnapped by the South Melbourne Football Club and made a star player in the city. The Club Chairman, Colonel Ball-Miller, give MacArthy a job in one of his companies and makes him attend night school. He is seduced by his English teacher, Miss Russell, and has an affair with Ball-Millers daughter, Andrea.

MacArthy and Andrea get married but then divorce. MacArthy goes on strike to claim the family four tine.

==Cast==
*John Jarratt as MacArthy
*Judy Morris as Miss Russell
*Kate Fitzpatrick as Andrea
*Sandy Macgregor as Vera
*Barry Humphries as Colonel Ball-Miller
*John Frawley as Webster
*Colin Croft as Tranter
*Chris Haywood as Warburton
*Colin Drake as Ackerman
*Ron Frazer as Twentyman
*Max Gillies as Stan
*Dennis Miller as MacGuinness
*Lou Richards as Lou Arnold
*Jack Dyer as Jack Diehard
*Jim Bowles as Les
*Bruce Spence as Bill Dean
*Peter Cummins as Rerk

==Production==
David Baker was an emerging director who was interested in Barry Oakleys novel. Richard Brennan optioned it for him and they agreed to make the film together, hiring playwright John Romeril to do the adaptation. According to Brennan, Romerils second draft was "fantastic" but later drafts included too much sex and slapstick to make it more like other successful Australian films at the time such as The Adventures of Barry McKenzie and Alvin Purple. David Stratton, The Last New Wave: The Australian Film Revival, Angus & Robertson, 1980 p223-224 

Philip Adams later claimed he always knew the film would struggle "because of its idiosyncratic and complex nature". Gordon Glenn & Scott Murray, "Phil Adams: Producer", Cinema Papers, March–April 1976 p343 

The film was shot in mid 1974. Half the budget was provided by the Australian Film Development Corporation. 

==Release==
The film performed poorly critically and at the box office. 

==References==
 

==External links==
* 
* 
*  at Oz Movies

 
 
 
 
 
 
 
 
 