Nobody's Child (film)
{{Infobox film name = Nobodys Child image =  border =  caption =  director = Lin Wen Hui producer =  writer =  starring = Wang Lirong Joshua Ang Justin Peng Shui-gan Chen Huiling Allen Lew Yongqiang May Oon Yan Bingliang music = 《有缘熟悉无缘爱 》 cinematography = editing = studio =  distributor = released = 2004 runtime =  country = Singapore language = Mandarin Chinese|Mandarin&nbsp;/&nbsp;Singaporean Hokkien&nbsp;/&nbsp;English budget = 
}}
Nobodys Child (simplified Mandarin: 谁来爱我  ) is a 2004 Singaporean film, directed by Lin Wenhui.
==Story==
Together with her parents and younger brother, Shuangshuang left Malaysia for Singapore. Danping and her husband rented a food stall. But poor business forced Shuangshuang’s father to find a job back in Malaysia, leaving Danping to run the stall with Shuangshuang. Danping never appreciated Shuangshuang’s competence. Whenever Shuangshuang made a mistake or she was in a foul mood, she’d take it out on the young girl. Wei, an assistant at a neighbouring coffee stall, would always speak up for Shuangshuang whenever she got abused. He and Shuangshuang became good friends over time.

Shuangshuang’s father soon stopped remitting money and even had an affair. He asked for a divorce when his lover became pregnant. In her quest to get back at her husband, Danping slept with eating house owner Orbit  when the latter’s wife was away. Wei tips off Orbit’s wife, Orbiang, about the affair. Instead of thanking him, Orbiang chided Wei for not telling her sooner and fired him.

The emboldened Danping decided to go on a holiday to Phuket with Orbit. Shuangshuang secretly approached Obit to ask the latter to leave her mother. She even offered herself to Orbit in exchange. Orbit pretended to accede to her request and molested her. Shuangshuang went home to tell her mother about what happened. But Danping didn’t believe her, took her luggage and left.

Shuangshuang heard on the news about how Phuket was affected by a tsunami. Worried for her mum’s safety, she tried calling her in vain. Shuangshuang told Wei about how her mum could be a victim in the tsunami and how Orbit molested her. Wei discovers Orbit didnt go on the vacation with Danping. Enraged, Wei confronted Orbit in the woods. Shuangshuang was stabbed  as she tried to separate the two. Obit manages to escape with his life. When Shuangshuangs younger brother realizes she has not come back yet, he asks around for her. The film ends as Wei turns himself in.

==Cast==
*Wang Lirong as Shuangshuang
*Joshua Ang as Wei-ge
*Justin Peng as Dandan
*Allen Lew as Orbit
*May Oon as Obiang
*Yan Bingliang as drunkard

==External links==
* 

 
 
 