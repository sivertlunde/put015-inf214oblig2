From Beyond (film)
{{Infobox film
| name           = From Beyond
| image          = Frombeyondposter.jpg
| caption        = Theatrical release poster
| director       = Stuart Gordon
| producer       = Brian Yuzna
| story          = Brian Yuzna Dennis Paoli Stuart Gordon
| screenplay     = Dennis Paoli
| based on       =  
| starring       = Jeffrey Combs Barbara Crampton Ken Foree Ted Sorel Carolyn Purdy-Gordon
| music          = Richard Band
| cinematography = Mac Ahlberg
| editing        = Lee Percy
| distributor    = Empire Pictures
| released       =  
| runtime        = 80 minutes 85 minutes (unrated cut)
| country        = United States
| language       = English
| budget         = US$ 4,500,000
| gross          = US$ 1,261,000 
}} body horror short story of the same name by H. P. Lovecraft. It was written by Dennis Paoli, Gordon and Brian Yuzna, and stars Jeffrey Combs, Barbara Crampton, Ken Foree and Ted Sorel.

From Beyond centers on a pair of scientists attempting to stimulate the pineal gland with a device called The Resonator. An unforeseen result of their experiments is the ability to perceive creatures from another dimension that proceed to drag the head scientist into their world, returning him as a grotesque shape-changing monster that preys upon the others at the laboratory.

== Plot ==
Dr. Edward Pretorius (Ted Sorel) is a scientist who has developed the Resonator, a machine which allows whoever is within range to see beyond normal perceptible reality. His assistant, Dr. Crawford Tillinghast (Jeffrey Combs), activates the machine and soon sees strange creatures in the air. When he is bitten by one of them, he urges Pretorius to turn the machine off. However, the crazed Pretorius refuses. Events transpire to the point where Crawford panics, fleeing outside. When the police arrive, they find Pretorius decapitated. Crawford is subsequently arrested and accused of murder.

Crawford is committed to a psychiatric ward, where he is treated by Dr. Katherine McMichaels (Barbara Crampton). After Crawford gives his account of Pretorius death, Katherine orders that Crawford undergo a CAT scan, showing that Crawfords pineal gland is enlarged and growing.  Convinced of Crawfords innocence, Katherine has him released to her custody, and plans on taking him back to Pretoriouss house and the Resonator. They are accompanied by Detective Bubba Brownlee (Ken Foree), who investigated Pretorius death.

Upon returning to the house, Katherine and Crawford rebuild the Resonator. Crawford reactivates the machine, which causes more creatures to appear. A severely deformed Pretorius, still alive, appears in the attic and tells the trio of a world beyond that is more pleasurable than normal reality. A panicking Crawford shuts off the Resonator, making Pretorius and the creatures vanish.

The next morning, Katherine insists that the Resonator could possibly cure schizophrenia and suggests that they turn the machine back on, but Bubba and Crawford disagree. While Bubba and Crawford are asleep, Katherine gets back up to feel the pleasure from the machine and turns it back on, bringing forth a worried Crawford and the now almost unrecognizable and mutated Edward. Bubba enters the scene as Edward grabs Katherine, preparing to eat her mind and take her to the world of beyond. Crawford and Bubba go down into the basement to shut off the power, but encounter a giant worm-monster. Bubba succeeds in shutting off the power, rescuing Crawford and Katherine and sending Edward away.

When Bubba decides that they should leave the house, all of a sudden, Edward somehow returns and the Resonator turns back on, as all three of them run up into the attic to deactivate it. Katherine and Crawford are attacked by little bee-like creatures, and as Bubba pushes them out of the way, he is devoured to the bone. Crawford fights off Edward and succeeds in freeing Katherine but then his enlarged pineal gland pops out of his forehead. Katherine short circuits the machine by spraying it repeatedly with a fire extinguisher.

She then takes Crawford back to the hospital, where she is evaluated for insanity and schizophrenia, since her story was just like Crawfords. As Katherine is being prepared for shock treatment by a sadistic staff member, Crawford has an overwhelming hunger for human brains and kills Dr. Bloch (Carolyn Purdy-Gordon). Katherine escapes and drives back to the house with a bomb and a crazed Crawford following her.

Katherine puts the bomb on the Resonator and goes to leave when Crawford attacks her. As he is about to eat her brain, she bites off his pineal gland, reverting him to his senses. However, Crawford is pulled away and eaten by a completely deformed, mutated Edward. Katherine finally escapes, right as the whole attic explodes.

== Cast ==
* Jeffrey Combs as Crawford Tillinghast
* Barbara Crampton as Dr. Katherine McMichaels
* Ted Sorel as Dr. Edward Pretorius
* Ken Foree as Bubba Brownlee
* Carolyn Purdy-Gordon Dr. Bloch

== Production ==
Gordon had previously worked with both Combs and Crampton on   in 2001, and the second episode of the Masters of Horror television series, H. P. Lovecrafts Dreams in the Witch-House, in 2005. Many members of the production staff for Re-Animator also held similar roles in the production of From Beyond, including screenwriter Dennis Paoli, producer Brian Yuzna, executive producer Charles Band, director of photography Mac Ahlberg, and special effects artists John Carl Buechler and John Naulin. Lukeman, Adam. Fangorias 101 Best Horror Movies Youve Never Seen: A Celebration of the Worlds Most Unheralded Fright Flicks, Random House Digital, Inc., 2011. ISBN 0307523470 

Albert Band, who served as the production manager of From Beyond, also makes an uncredited cameo appearance as a wino. 

From Beyond was shot in Italy with an Italian crew in order to save money. Gordon says that the film would have cost fifteen million dollars to make in the United States, whereas the foreign production enabled him to hold costs to approximately two and a half million dollars. Gallagher, p. 94.  It was shot on a soundstage called Dinocitta just outside of Rome. Gallagher, p. 95.  Dinocitta was originally constructed by Dino DeLaurentiis, but was seized by the government for nonpayment of taxes, and then sold to Empire Studios.  From Beyond was one of the first films shot at that venue during its period of ownership by Empire.  Gordon shot his film Dolls (1987 film)|Dolls at the same time, and it was released the following year. 

As with his earlier film Re-Animator, Gordon made use of medical advisors to be sure that the actions taken by the doctors and nurses of the film followed proper medical procedures.  Four separate special effects teams worked on the effects for From Beyond.  According to Yuzna, the production ran out of money before the effects on the finale could be finished. 

According to Gordon, securing an "R" rating from the MPAA was a challenging ordeal. He quotes them as initially saying his presented cut of the film had "ten times too much of everything".  He was ultimately able to get away with making small trims, and without removing any entire sequences from the film.  The Special effects was created by John Carl Buechler. 

== Release == MPAA had Monsters HD Channel. This longer, "directors cut" version was also then subsequently released on DVD by MGM on September 11, 2007.

On March 26, 2013, Scream Factory released a Collectors Edition of From Beyond on Blu-ray Disc/DVD combo pack. 

== Awards and reception ==
The film score by Richard Band won the award for Best Original Soundtrack at the Catalonian International Film Festival in Sitges, Spain. 

The film received generally positive reviews from critics. On the review website Rotten Tomatoes, the film maintains a rating of 71% approval from critics.

AllRovi|Allmovies review of the film was favorable, writing "Gordon is that rare breed who truly finds inspiration in another creator and uses that inspiration to craft a film that captures the essence of that creator while still being totally and uniquely his own", calling it a "gory thrill ride of a movie." 

In their book  , Andrew Migliore and John Strysik write that "From Beyond is a visual treat," but add that the films "gross sexual excess may displease hardcore Lovecraft fans." 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 