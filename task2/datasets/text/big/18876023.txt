Dragonworld
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Dragonworld
| image_size     = 
| image	=	Dragonworld FilmPoster.jpeg
| caption        = Promotional VHS-release Poster
| director       = Ted Nicolaou
| producer       = Albert Band Charles Band
| writer         = Suzanne Glazener Naha Ted Nicolaou  Stuart Campbell
| music          = Richard Band
| cinematography = Alan M. Trow
| editing        = D. Brett Schramm
| studio         = Moonbeam Entertainment
| distributor    = Full Moon Features Paramount Pictures
| runtime        = 99 minutes
| released       = July 21, 1994
| country        = United States   Scotland   United Kingdom   Romania
| language       = English 
}}
Dragonworld is a 1994 film that was released Direct-to-Video. It is the third film to be released by Moonbeam Entertainment, the childrens video division of Full Moon Entertainment.

==Plot==
Set in the modern times, a young five-year-old boy named "Johnny McGowan" travels to Scotland to live at his grandfathers castle after he loses both his parents in a traffic accident. At the magical wishing tree on his grandfathers estate, he conjures up a friend which is an infant dragon whom he nicknames "Yowler". They grow up together as 15 years go by. One day after the years go by, a documentary film maker Bob Armstrong, his daughter Beth, and his pilot Brownie McGee stumble upon Yowler. Eager for fame and money, Bob convinces John to "rent" Yowler to local corrupt businessman, Lester McIntyre. John, who is coerced in part by the offer to have the mounting taxes on the castle paid off, allows Lester to take Yowler in. He does so also partly because of his growing interest in Beth. Yowler is miserable and harassed in the new theme park built for him, and when it becomes clear that McIntyre has duped them in order to exploit the dragon, John and his new friends take action.

==Cast==
* Alastair Mackenzie  (credited as Sam Mackenzie)  as Johnny McGowan
* Courtland Mead as Young Johnny McGowan
* Richard Trask as Yowler the Dragon
* Janet Henfrey as Miss Twittingham Stuart Campbell as Porter
* Brittney Powell as Beth Armstrong
* John Calvin as Bob Armstrong
* John Woodvine as Lester MacIntyre
* Jim Dunk as Brownie McGee
* Lila Kaye as Mrs. Cosgrove
* Andrew Keir as Angus McGowan

==Soundtrack==
{{Infobox album
| Name = Dragonworld: Soundtrack
| Type = Film score Digital download)/Audio CD
| Artist = Richard Band  
| Cover =
| Released = June 12, 2012
| Length = 60:32 Intrada
| Reviews =
}}
{{Track listing
| collapsed    = yes
| headline     = Track listing
| all_music    = Richard Band
| total_length = 60:32
| title1       = Main Title
| length1      = 2:39
| title2       = Grandpas Legend
| length2      = 6:59
| title3       = Mrs. Cosgrove
| length3      = 2:03
| title4       = Behold The Manor
| length4      = 5:34
| title5       = Baby What?
| length5      = 0:54
| title6       = Dads Old Room
| length6      = 3:18
| title7       = The Envelope For Taxes
| length7      = 0:55
| title8       = They Land
| length8      = 4:10
| title9       = Bagpipes And Gravesite
| length9      = 3:20
| title10      = Baby Dragon
| length10     = 2:20
| title11      = Showtime
| length11     = 6:50
| title12      = Yowler Goes Ape
| length12     = 2:49
| title13      = Getting Yowler Back
| length13     = 10:39
| title14      = All Is Well
| length14     = 8:06
}}

==Home media== videocassette and laserdisc in 1994 by Paramount Home Video. As of July 21, 2014, it has still yet to see a DVD and Blu-ray disc release.

==Sequel==

A sequel to the film,   was released directly to video in 1999, although filmed in 1996. The film bears little relation to the first movie and has received mainly negative reviews from critics.

==References==
 

==External links==
 
*  
*  
*  
*   (including plot detail)
*  
 
 
 
 
 
 
 
 
 