Panoptikum 59
{{Infobox Film
| name           = Panoptikum 59
| image          = 
| image size     = 
| caption        = 
| director       = Walter Kolm-Veltée
| producer       = 
| writer         = Walter Kolm-Veltée
| narrator       = 
| starring       = Michael Heltau
| music          = 
| cinematography = Hanns König
| editing        = 
| distributor    = 
| released       = June 1959
| runtime        = 94 minutes
| country        = Austria
| language       = German
| budget         = 
| preceded by    = 
| followed by    = 
}}

Panoptikum 59 is a 1959 Austrian drama film directed by Walter Kolm-Veltée. It was entered into the 9th Berlin International Film Festival.   

==Cast==
* Michael Heltau - Werner
* Alexander Trojan - Klinger
* Elisabeth Berzobohaty - Kora
* Heiki Eis - Ein junger Mensch
* Paula Elges - Thea Gutwell
* Melanie Horeschowsky - Alte Dame
* Erna Korhel - Mutter Felizitas
* Helmut Kraus - Dr. Schmuck
* Ulla Purr - Felizita
* Herbert Schmid - Schalk
* Alma Seidler

==References==
 

==External links==
* 

 
 
 
 
 
 
 