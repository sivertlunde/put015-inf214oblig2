Manassakshi
{{Infobox film 
| name           = Manassakshi
| image          =  
| caption        = 
| director       = S. K. A. Chari
| producer       = A L Srinivasan
| writer         = Remake of Thirudadhe (Tamil) (1961)
| screenplay     =  Rajkumar Bharathi Bharathi Narasimharaju Narasimharaju M. P. Shankar
| music          = G. K. Venkatesh
| cinematography = V Selvaraj
| editing        = V P Krishnan Shanmugam
| studio         = A L S Productions
| distributor    = A L S Productions
| released       =  
| runtime        = 140 min
| country        = India Kannada
}}
 1968 Cinema Indian Kannada Kannada film, Narasimharaju and M. P. Shankar in lead roles. The film had musical score by G. K. Venkatesh.  

==Cast==
  Rajkumar
*Bharathi Bharathi
*Sowcar Janaki in Guest Appearance Narasimharaju
*M. P. Shankar
*Ranga
*Nagappa
*Suryakumar
*Ramachandra Shastry
*Guggu
*Shivaji
*Govindaraj
*Shailashree
*B. V. Radha
*Sadhana
*B. Jayashree
*Shanthamma
*Baby Nagamani
 

==Soundtrack==
The music was composed by GK. Venkatesh. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Alealeyali Vuliyulididhe || PB. Srinivas || Vijaya Narasimha || 03.07
|- Susheela || Vijaya Narasimha || 03.46
|}

==References==
 

==External links==
*  
*  
*  

 
 
 
 


 