Torrance Rises
{{Infobox Film |
  name=Torrance Rises |
  image= |
  caption = |
  writer= |
  starring=Spike Jonze 
Will Smith   Madonna 
Chris Rock 
Janeane Garofalo 
Sofia Coppola 
Regis Philbin 
Fatboy Slim|
  director=Lance Bangs   Spike Jonze |
  music= | 	 
  distributor= Palm Pictures|
  released= 1999 |
  runtime=34 min. |
  language=English |
  movie_series=|
  awards=|
  producer=Vincent Landay |
  budget= |}}

Torrance Rises is a 1999 mockumentary directed by and starring Spike Jonze.

The film is based on a dance group in Torrance, California, and traces their journey to the MTV Video Music Awards presentation. The music video for Fatboy Slims 1999 song "Praise You", also directed by Jonze, features a street performance by this group.

Torrance Rises also appears in the compilation The Work Of Director Spike Jonze (Palm Pictures).

== External links ==
*  

 

 
 
 
 
 
 


 