Exile Express
{{Infobox film
| name           = Exile Express
| image_size     =
| image	         = Exile Express FilmPoster.jpeg
| caption        = Film poster
| director       = Otis Garrett
| producer       = Eugene Frenke (producer)
| writer         = Ethel La Blanche (writer) Edwin Justus Mayer (writer)
| narrator       = Alan Marshal   Jerome Cowan   Walter Catlett
| music          = George Parrish
| cinematography = John J. Mescall
| editing        = Edward Curtiss Robert Bischoff
| studio         = Grand National Pictures
| distributor    = Grand National Pictures
| released       = May 27, 1939
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 Alan Marshal and Jerome Cowan. 

==Plot==
After being wrongly implicated in the murder of her scientist boss by foreign agents, a young immigrant woman is placed on board an "exile express" from California to New York City where she is to be deported from Ellis Island. With the help of a journalist who has fallen in love with her, she jumps the train and sets out to prove her innocence.

==Production== A Woman Alone (1936) in Britain. Exile Express was made by the small Grand National Pictures, which went out of business the same year after producing several large-budgeted films which didnt recoup their costs.

== Cast ==
*Anna Sten as Nadine Nikolas Alan Marshal as Steve Reynolds
*Jerome Cowan as Paul Brandt
*Walter Catlett as Gus
*Jed Prouty as Hanley Stanley Fields as Tony Kassan
*Leonid Kinskey as David
*Etienne Girardot as Caretaker
*Irving Pichel as Victor Harry Davenport as Dr. Hite
*Addison Richards as Purnell
*Feodor Chaliapin Jr. as Kaishevshy
*Spencer Charters as Justice of the Peace Henry P. Smith
*Byron Foulger as Serge
*Don Brodie as Mullins
*Henry Roquemore as Constable
*Vince Barnett as Deputy Constable
*Maude Eburne as Mrs. Smith

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 