Let My People Go! (2011 film)
 
{{Infobox film
| name           = Let My People Go!
| image          = Movie_Poster_Let_My_People_Go.jpg
| alt            =
| caption        =
| director       = Mikael Buch
| producer       =     Geraldine Michelot
| director of photography = Celine Bozon
| editing        = Simon Jacquet
| studio         = Les Films Pelleas
| distributor    = Les Films du Losange and Zeitgeist Films
| released       =  
| runtime        = 86 minutes
| country        = France
| language       = French
}}
Let My People Go! is a 2011 film directed by Mikael Buch. It premiered at the 2011 Montreal World Film Festival and was released in December 2011 in France.  It was released in the United States in 2013 by Zeitgeist Films and grossed $18,529 domestically.

==Synopsis==
Ruben is a French-Jewish gay mailman is living in fairytale Finland (where he got his MA in “Comparative Sauna Cultures”) with his gorgeous Nordic boyfriend. Just before Passover, a series of mishaps and a lovers’ quarrel exile the heartbroken Reuben back to Paris and his zany family—including Carmen Maura as his ditzy mom, and Truffaut regular Jean-François Stévenin as his lothario father. Scripted by director Mikael Buch and renowned arthouse auteur Christophe Honoré, Let My People Go! both celebrates and upends Jewish and gay stereotypes with wit, gusto and style to spare. 

==Main cast==
* Nicolas Maury as Ruben
* Carmen Maura as Rachel
* Jean-François Stévenin as Nathan
* Amira Casar as Irène
* Clément Sibony as Samuel Jarkko Niemi as Teemu
* Jean-Luc Bideau as Maurice
* Kari Väänänen as Monsieur Tilikainen
* Olavi Uusivirta as Fredrik

==Awards==
* 2012: Philadelphia QFest: Best Comedic Film  
* 2012: Asheville QFest: Best Cinematography 

==Reception==
"A fairy-tale romance whose title acknowledges both a saturation in and longing to be free of Jewish cultural baggage, Mikael Buchs Let My People Go!cross-breeds cultures that are rarely paired onscreen. International box-office prospects are fair in urban arthouses, where the presence of Almodóvar collaborator Carmen Maura may tip moviegoers off to the pop-inflected, comic semi-scandals in store."
-John DeFore, The Hollywood Reporter   

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 