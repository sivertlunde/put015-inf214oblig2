Edward, My Son
{{Infobox film
| name           = Edward, My Son
| image          = EdwardMySon.jpg
| image_size     =
| caption        = Original poster
| director       = George Cukor
| producer       = Edwin H. Knopf
| writer         = Donald Ogden Stewart
| narrator       =
| starring       = Spencer Tracy Deborah Kerr
| music          = John Wooldridge
| cinematography = Freddie Young
| editing        = Raymond Poulton
| distributor    = Metro-Goldwyn-Mayer
| released       = March 1, 1949 (UK) June 2, 1949 (US)
| runtime        = 112 minutes
| country        = United States United Kingdom
| language       = English
| budget         = $2,421,000  .  gross = $2,142,000 
| preceded_by    =
| followed_by    =
}}
Edward, My Son is a 1949 American/British drama film directed by George Cukor that stars Spencer Tracy and Deborah Kerr. The screenplay by Donald Ogden Stewart is based on the play by Noel Langley and Robert Morley.

==Plot== Canadian Arnold Boult and his wife Evelyn are celebrating the first birthday of their son Edward with their friend, physician Larry Woodhope, in their London home shortly after World War I. Arnold is about to embark upon a new career in finance with Harry Simpkin, who has been released from prison after serving time on fraud charges.

Five years later, Edward is diagnosed with a serious illness requiring a costly operation abroad. With his retail credit business doing poorly, Boult decides to burn down the building in order to finance the surgery with the insurance money. Despite reservations about his partners scheme, Harry goes along with the plan.

As the years pass, Boult evolves into a wealthy, titled financier who will do anything to protect his son. When Edward is threatened with expulsion from his prep school, Lord Boult assumes its mortgage. Time passes, and Evelyn confides in Larry her concern that Edward drinks too much and appears to have no sense of morality. Larry strongly suggests that something be done to control Edward, but Lord Boult feels the young man can do no wrong.

Having served another sentence for fraud, Harry comes to Boult and asks for a job. When he is put off, Harry commits suicide by leaping from the roof of his former partners office building. When the police investigate, Boults secretary Eileen Perrin lies that Harry did not come to the office that day. She and Boult become lovers.

A year later, during a tryst in Eileens apartment, the two discover they are being observed by a detective working for Evelyns attorney. Anxious to avoid scandal, Boult breaks up with Eileen, who later kills herself with an overdose of pills. Boult departs for Switzerland to see his wife and Edward. Evelyn threatens to expose him so their son will see his true nature, but in return Boult promises he will destroy Larry, who loves her, unless she remains silent.

Evelyn acquiesces. As the years pass, she becomes increasingly unhappy and begins to drink heavily. Edward also has become an alcoholic and is engaged to socialite Phyllis Mayden, although young Betty Foxley, who is pregnant with Edwards child, believes he will marry her. Boult subtly suggests Larry abort the child, but the doctor refuses and offers Betty medical and financial assistance instead.

Edward, serving as a Royal Air Force pilot during World War II, crashes his plane while stunting and is killed along with his crew. Lord Boult, now a widower, beseeches Larry to tell him the whereabouts of Betty and her child. Larry refuses, leaving his obsessed old friend determined to do whatever is necessary to find his grandchild.

==Cast==
*Spencer Tracy ..... Arnold Boult
*Deborah Kerr ..... Evelyn Boult Ian Hunter ..... Larry Woodhope
*Mervyn Johns ..... Harry Sempkin
*Leueen MacGrath ..... Eileen Perrin
*Felix Aylmer ..... Mr. Hanray
*Walter Fitzgerald ..... Mr. Kedner
*Tilsa Page ..... Betty Foxley
*Ernest Jay ..... Walter Prothin  
*Colin Gordon ..... Ellerby
*Harriette Johns ..... Phyllis Mayden

==Production==
In the play, the title character never is seen, and director George Cukor opted to do the same in the film adaptation. The screenplay closely adhered to the original script, the only major change being Arnold Boults conversion from British to Canadian so Spencer Tracy wouldnt have to struggle with an accent. Tracy initially resisted playing such an unsympathetic character but later told Cukor, "Its rather disconcerting to me to find out how easily I play a heel."  

Cukor originally wanted his close friend and Tracys lover Katharine Hepburn for the role of Eileen, but the two were sensitive about working together too frequently. Cukor also feared casting a major star in the relatively small role would throw the picture off balance and draw attention away from leading lady Deborah Kerr. Levy, Emanuel, George Cukor: Master of Elegance. New York: William Morrow & Company, Inc. 1994. ISBN 0-688-11246-3 

Leueen MacGrath ended up as Eileen, reprising a role she had played on stage.

==Box Office==
According to MGM records the film earned $1,267,000 in the US and Canada and $875,000 overseas resulting in a loss to the studio of $1,159,000. 

==Critical reception==
Bosley Crowther of the New York Times observed, "Shallow, perhaps, as a study of the accumulation of power, this drama is nonetheless gripping in its expose of the intimate life of a man in his ruthless rise from poverty to fabulous position and wealth . . . And not only is the story intriguing in its details, but some of the people in it are consistently interesting . . . However, it must be acknowledged that Mr. Tracy . . . fails to give clear definition or consistency to this ruthless man . . . as Mr. Tracy plays him, he is a really decent sort who sells his soul for the sake of his beloved son and whose defection seems to haunt him for the rest of his life. His moments of hard and ruthless dealing, in which his eyes narrow coldly and his jaw sets, are heavily interlarded with gay and smiling gobs of Tracy charm. There is nothing sardonic about him. He is even dull as a personality . . . Say this, however, for the film folks: they havent put Edward on the screen. That major restraint is most welcome." 

==Awards and nominations==
Deborah Kerr was nominated for the Academy Award for Best Actress and the Golden Globe Award for Best Actress – Motion Picture Drama but lost both to Olivia de Havilland in The Heiress.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 