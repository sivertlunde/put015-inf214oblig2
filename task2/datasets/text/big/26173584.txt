Komal Gandhar
 
 
{{Infobox film
| name           = Komal Gandhar  (A Soft Note on a Sharp Scale)
| image          =Komal Gandhar DVD cover.jpg
| image size     = 200px
| border         =
| alt            =Komal Gandhar DVD cover
| caption        =Komal Gandhar DVD cover
| director       = Ritwik Ghatak
| producer       = Ritwik Ghatak
| writer         =
| screenplay     = Ritwik Ghatak
| story          =
| based on       =  
| narrator       =
| starring       =
| music          =Jyotirindra Moitra Lyrics: Rabindranath Tagore
| cinematography =Dilip Ranjan Mukhopadhyay
| editing        =Ramesh Joshi
| studio          =
| distributor    =
| released       = 31 March 1961
| runtime        =
| country        = India Bengali
| budget         =
| gross          = }}
 Bengali film Hindustani equivalent IPTA and the fallout of the partition of India. 

==Overview==
The title was taken from the line of a poem Book: Punascha, poem: Komal–Gandhar;    by Rabindranath Tagore that meant a sur or note, E♭ (musical note)|E-flat. As in other films by Ghatak, music plays a pivotal role in the movie.
 IPTA workers, Ghatak with his signature style touches on varied issues of Partition of Bengal (1947)|partition, idealism, corruption, the interdependence of art and life, the scope of art, and class-struggle. Unlike his other films, this one runs along an upbeat mood with the lead pair of lovers (Vrigu and Anusua) being reunited.

==Cast==
* Abinash Bannerjee as Bhrigu
* Abhi Bhattacharya
* Bijon Bhattacharya as Gagan
* Satindra Bhattacharya as Shibnath
* Debabrata Biswas
* Chitra Sen as Jaya
* Anil Chatterjee as Rishi
* Satyabrata Chattopadhyay
* Supriya Devi as Anusuya
* Gita Dey as Shanta

==Soundtrack==
Music was by Jyotirindra Moitra, from Indian Peoples Theatre Association|IPTA, and a noted Rabindra Sangeet exponent who had previously given music in Ghataks Meghe Dhaka Tara (1960), and had song by singers like, Bijon Bhattacharya, Debabrata Biswas, Hemanga Biswas. Bahadur Khan played sarod in the soundtrack. The film is noted for its wedding songs and also contrapuntal use of sound. 

== See also ==
* List of works of Ritwik Ghatak

==References==

===Citation===
 

===Notes===
 

==External links==
*  
*  

 

 
 
 
 
 
 