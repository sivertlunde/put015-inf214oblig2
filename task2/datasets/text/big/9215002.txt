Bolivia (film)
{{Infobox film
| name           = Bolivia
| image          = Boliviaposter.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Israel Adrián Caetano
| producer       = Executive Producer: Matías Mosteirín Producer: Roberto Ferro Associate Producer: Lita Stantic
| writer         = Screenplay: Israel Adrián Caetano  Story: Romina Lafranchini
| narrator       =
| starring       = Freddy Flores Enrique Liporace Rosa Sánchez
| music          = Los Kjarkas
| cinematography = Julián Apezteguia
| editing        = Santiago Ricci Lucas Scavino
| distributor    = Cinema Tropical
| released       =  
| runtime        =  75 minutes Argentina Cinema Netherlands
| Spanish
| budget         =
}} Argentine and Dutch drama film directed by Israel Adrián Caetano, his first feature-length film. The screenplay is written by Caetano, based upon the Romina Lafranchini story, about his wife. The motion picture features Freddy Flores and Rosa Sánchez, among others. 

The film was photographed in "gritty" 16mm black-and-white, and was shot by cinematographer Julián Apezteguia.  Bolivia was filmed entirely in Buenos Aires.

==Plot==
The mostly plot-free film is confined to a café-bar in the lower-middle class Buenos Aires neighborhood of Villa Crespo, with few trips outside.

Bolivia tells the story of Freddy (Freddy Flores), a Bolivian with a gentle disposition, who, after Americans burn down the coca fields where he is employed, loses his job. With little work opportunities in Bolivia, he leaves his wife and three daughters and travels to Argentina to search for employment as an undocumented worker.  He hopes to make money and later return to his family.

He lands a job as a grill cook in a seedy Villa Crespo café where the brutish owner (Enrique Liporace) is happy to skirt Argentinian immigrant laws in order to secure cheap labor.
 mixed heritage; Héctor (Héctor Anglada), a traveling salesman from the province of Córdoba Province (Argentina)|Córdoba whos gay; a Porteño taxi driver (Oscar Bertea), and one of the drivers buddies.

Freddy also has to deal with various Argentine café patrons who view all Paraguayans and Bolivians with disdain due to their ethnicity.

==Cast==
* Freddy Flores as Freddy
* Rosa Sánchez as Rosa
* Oscar Bertea as Oso
* Enrique Liporace as Enrique Galmes
* Marcelo Videla as Marcelo
* Héctor Anglada as Héctor, the Salesman
* Alberto Mercado as Mercado

==Background==

===Production===
The motion picture was financed partly by the Rotterdam International Film Festivals Hubert Bals Fund and the INCAA (Instituto Nacional de Cine y Artes Audiovisuales de la Argentina).

The filming was a stop-and-go production and required three years of discontinuous shooting.  It was shot on different days and at different times.  According to director Caetano, he was never able to film for more than three days at a time. 

===Basis of film===
Caetano said, " hen writing the script, what interested me was the story; the issue of racism was not very present. However, it is inevitable that when addressing those characters and setting the story in that particular social strata, there is a series of themes that appear on their own and impose themselves." Cine Las Americas, ibid. 

Caetano believes that, " he film’s main theme is the collision among people of the same social class, they are workers about to be left out of any class at all, and thus they are intolerant towards one another. Basically, they are trapped in a situation they can not escape." 

===Casting===
Caetano, in Neorealism (art)|neo-realist fashion, used both professional and non-professional actors. &nbsp;&nbsp;Freddy Flores, the main character, for example, is a non-professional actor. 

==Distribution==
The film was first featured at the Cannes Film Festival in May 2001 where it won the Best Feature Young Critics Award. It opened in the Netherlands on January 24, 2002 and in Argentina on April 11, 2002.

The film was also shown at various film festivals, including: the Donostia-San Sebastián International Film Festival, the London Film Festival, the Rotterdam International Film Festival, the Karlovy Vary Film Festival, the Festivalissimo Montréal, the Cinémas dAmérique Latine de Toulouse, the Cleveland International Film Festival, and the Film by the Sea Film Festival.

In the United States, the movie opened in New York City on February 26, 2003.

==Critical reception==
Film critic Elvis Mitchell, writing for The New York Times, liked the direction of the film, and wrote, "Mr. Caetanos work is most telling and gripping...  has an emotional integrity thats concise and direct." 

Film critics Frederic and Mary Ann Brussat of the website Spirituality and Practice were touched by the story they viewed, and wrote, "Bolivia is a riveting slice-of-life drama...  hits the mark with its harrowing depiction of urban poverty and the divisive and explosive impact of the hatred of foreigners." 

Manohla Dargis, film critic for the Los Angeles Times, makes the case that the film sub silento informs of what is happening in Argentina (in 2001) both economically and culturally.  She wrote, "Life in Bolivia, a parable about contemporary Argentina, is even grittier than the films churning black-and-white cinematography...  offers up characters in a state of ongoing crisis. Underpaid and overwhelmed, financially unmoored and spiritually adrift, these are men and women for whom the tanking economy is, finally, just the most obvious manifestation of a deeper malaise." 

Currently, the film has a 100% "Fresh" rating at Rotten Tomatoes, based on twelve reviews. 

==Awards==
Wins
* Cannes Film Festival: Young Critics Award Best Feature, Israel Adrián Caetano; 2001.
*   Prize, Israel Adrián Caetano, for its direct, sentimental treatment of one of the most important social questions facing urban societies everywhere; 2001.
* Donostia-San Sebastián International Film Festival: Made in Spanish Award, Israel Adrián Caetano; 2001.
* Rotterdam International Film Festival: KNF Award, Israel Adrián Caetano; 2002.
* Argentine Film Critics Association Awards: Silver Condor; Best Screenplay, Adapted, Israel Adrián Caetano; Best Supporting Actor, Enrique Liporace; 2003.

Nominated
* Argentine Film Critics Association Awards: Silver Condor; Best Cinematography, Julián Apezteguia; Best Editing, Lucas Scavino, Santiago Ricci; Best Film; Best New Actor, Freddy Waldo Flores; 2003.

==References==
 

==External links==
*  
*   at the cinenacional.com  
*   film review at Cineismo by Guillermo Ravaschino  
*  

 
 
 
 
 
 
 
 
 