To the Ladies
{{infobox film
| name           = To the Ladies
| image          =
| imagesize      =
| caption        =
| director       = James Cruze
| producer       = Adolph Zukor Jesse L. Lasky
| based on       =  
| writer         = Walter Woods (scenario)
| starring       = Edward Everett Horton Theodore Roberts Louise Dresser
| music          = Karl Brown
| editing        =
| distributor    = Paramount Pictures
| released       = November 25, 1923
| runtime        = 6 reels
| country        = United States
| language       = Silent (English intertitles)
}} 1923 American silent comedy film produced by Famous Players-Lasky and released by Paramount Pictures. It is based on a 1922 Broadway play, To the Ladies, by George S. Kaufman and Marc Connelly. 

The film was directed by James Cruze and starred Edward Everett Horton, Theodore Roberts, and Louise Dresser. Also in a bit part is young Mary Astor.

==Cast==
*Edward Everett Horton - Leonard Beebe
*Theodore Roberts - John Kincaid
*Helen Jerome Eddy - Elsie Beebe
*Louise Dresser - Mrs. Kincaiid
*Z. Wall Covington - Chester Mullin
*Arthur Hoyt - Tom Baker
*Jack Gardner - Bob Cutter
*Patricia Palmer - Mary Mullin
*Mary Astor - Bit

==Preservation status==
This is now considered a lost film.  

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 


 