Kaaviya Thalaivan (2014 film)
 
 
 
{{Infobox film
| name           = Kaaviya Thalaivan
| image          = Kaaviya thalaivan.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Vasanthabalan
| producer       = Varun Manian S. Sashikanth
| writer         = Jeyamohan  (Dialogue) 
| screenplay     = Vasanthabalan  Jeyamohan
| story          = Vasanthabalan Siddharth Prithviraj Prithviraj Nassar Vedhicka Anaika Soti
| music          = A. R. Rahman
| cinematography = Nirav Shah
| editing        = Praveen K. L.
| studio         = Y NOT Studios Radiance Media Group
| distributor    = Dream Factory 
| released       =  India
| runtime        = 150 minutes 
| country        = India Tamil
| budget         =      release date    = 
| gross          =
}} Tamil historical Siddharth and Prithviraj and the competition that exists between them in a small drama troupe.    The former is the main lead whereas the latter one is the antagonist in the film.  Nassar plays the role of a guru to these actors and the role of actress Vedhicka is inspired by the stage artist and singer K. B. Sundarambal.  The original songs and film score were composed by A. R. Rahman. The film was dubbed into Malayalam as Pradhi Nayagan.

==Plot==
Thavathiru Sivadas Swamigal (Nassar) runs a drama troupe in which Thalaivankottai Kaliappa Bhagavathar (Siddharth (actor)|Siddharth) and Melachivilberi Gomathi Nayagam Pillai(Prithviraj Sukumaran|Prithviraj) are his direct disciples. Gomathi is left by his father in the troupe of Swami. Kali is found by Swami during a train trip where the boy is singing and begging alms. Impressed by his singing talents, Swami takes the boy with him. Gomathi and Kaliappa have relationship like brothers. They grow up together and stage many plays where Gomathi often plays "Sthripart" - the female role and Kali plays "sidepart"- supporting characters. While Vadivambal (Vedhicka), the female member in the group falls in love with Kali, the latter loves Rangamma (Anaika Soti), the princess of the zameen. Bhairava (Ponvannan) who is another student of Swami is a popular "Rajapart" - performing main title characters of a stage play. He becomes headstrong over his acting capabilities and does not attend the rehearsals for which he gets scoldings from Swami. After a quarrel, he leaves the troupe as he could not bear the torture of Swami. Swami does not give up and conducts an audition for the main character role of Soorapadman in which Kali and Gomathi perform. Kali gets selected, which leaves Gomathi angry. The reason behind Swamis bias towards Kali is never answered clearly to Gomathi which leaves him more frustrated.

After following Kali secretly, Gomathi informs to Swami about Kalis love to take revenge on him. Swami scolds Kali and curses him that he will never play any characters in stage. But Kali wants Swami to pardon him and keep him with the troupe as a servant. Swami agrees and asks him to forget the princess if he wants to continue in the troupe. Kali promises but Swami agrees to keep him with the troupe like a servant but does not take back the curse. The following day, the entire troupe leave the village and head to another place. Kali cannot forget Rangamma and she is also shown to be searching Kali and ultimately a man from the village comes to Kali to inform that Rangamma has died due to the forcing of her parents to get married. The man also says that she was pregnant when she died. Kali fully drunk comes to the troupe and curses Swami that he is the reason for Rangammas and his unborn childs death. Swami gets ill suddenly and dies. But before dying he blesses Kali who pardons to forgive him. After Swamis death, Gomathi takes charge of the troupe and he wants Kali to be out of it. Kali fights with Gomathi and leaves.

It is shown that the troupe travels to Ceylon, Malaysia, Singapore in five years time and becomes popular. Now, Gomathi is a famous "Rajapart" as he wished to be and Vadivu is known to be the famous "Ganakokilam" Vadivambal. They return to Madurai and join hands with Mansoor Ali Khan who produces the plays. When they are getting ready to stage the play "Karnamotcham", Gomathi falls severely ill. He asks the people from his troupe to go in search of other drama actors to perform his role. A man goes and brings Kali who is a drunkard now. Feeling delighted to see his troupe members, he agrees to play the role of "Karnan" though he is given the role of "Arjuna" by Gomathi who says that it has been long since they performed together. Gomathi still feels ill and could not perform in the play meanwhile Kali fills the gap and earns good reputation as "Rajapart" which makes Gomathi jealous. He knows the interest of Kali towards freedom movement stage plays and arranges a person to get Kali arrested for performing in plays which is against the British. But Gomathi visits him in jail on pretext of taking him in bail.

Inside the jail, Kali meets other people involved in freedom movement and encouraged by them he agrees to do stage plays on that topic. Gomathi is interested in Vadivu and pesters her to marry him. Her mom makes arrangements to make her the mistress of a Jamin king. Distressed Vadivu leaves the troupe and waits for a call from Kali to join his "Bharatha Gana Sabha" in which he stages plays based on freedom movement due to which he frequently goes to jail. Meanwhile, audience become interested in Kalis plays than Gomathis epic plays. Vadivu gets a call after so much persuasion for which she was waiting and joins there eagerly. She expresses her love and it is lately accepted by Kali. Gomathis life changes and he turns from a rich man to poor. Kali is informed about Gomathis situation. Therefore, he approaches Gomathi and asks him to join his troupe and offers him the "Rajapart" role with fondness for which Gomathi agrees. They get ready to stage a play on Bhagath singh for which the British announce shooting warrant. But a brave Kali performs on stage and when the play ends police comes and chaos starts. The troupe people asks Gomathi, Kali and Vadivu to escape. Police follow them, Kali goes alone, Vadivu and Gomathi escape together inside a forest. A gun sound is heard and Gomathi leaves back Vadivu to go and see what it was. He meets Kali who is safe. Kali tells a plan to meet tomorrow morning. While he leaves, he is shot by none other than Gomathi.

Kali is shocked and asks why he did this to his brother. Gomathi vents out his anger for the first time to Kali saying that Kali snatched the "Rajapart" role of Soorapadman, his love interest Vadivu, regained his "Rajapart" status when he fell ill and states that he is now poor because of the increased popularity of Kalis plays. Kali says that he knows it was Gomathi who told Swami about his frequent visits to Rangammas place and he also says that he knows the person who made him sent to jail was the arrangement of Gomathi. But he took all these as good deeds that his elder brother did for him for positive changes in his life and forgave Gomathi each and every time. Gomathi now feels bad but Kali asks Gomathi to shoot him to death and fights with Gomathi. During the quarrel accidentally the trigger is pressed and Kali dies. In the last rites ceremony, when everyone are mourning, Vadivu informs that she is pregnant with Kalis child and says he will be reborn again. Gomathi takes the ashes to Varanasi and while dipping in the Ganges, it is shown that Gomathi never comes out of the water.

==Cast==
   Siddharth as Thalaivankottai Kaliappa Bhagavathar Prithviraj as Melachivilberi Gomathi Nayagam Pillai
* Vedhicka as Ganakokilam Vadivambal
* Anaika Soti as Princess Rangamma
* Nassar as Thavathiru Sivadas Swamigal
* Thambi Ramaiah as Koduvaai
* Ponvannan as S V Bairava Sundaram
* Babu Antony as Zaminder (Rangammas Father)
* Singampuli as Palavesam Mansoor Ali Khan as Contract Kannaiah Kuyili as Vadivambals mother
*Anjalidevi as Zamindharini (Rangammas Mother)
*Karikalan as Inspector Nanjandu Rao
 

==Production==

===Development=== old drama Salangai Oli by director Kasinathuni Viswanath.    

Kaaviya Thalaivan was announced in September 2012 as a venture by the production house Y NOT Studios teaming with Vasanthabalan and Siddharth (actor)|Siddharth, the latter as the lead actor of the film.  The team signed A. R. Rahman to compose the music for the film in December 2012.  Research work for the final script was carried out for nearly a year wherein facts, references and whereabouts were collected from veteran theatre artists belonging to different theatre clubs existing in Madurai, Dindigul, Karur and Pudukottai. As the film portrays the lives of theatre artists, the facial looks of characters were given prime importance for which make-up artist Pattanam Rasheed was signed. Perumal Selvam and Niranjani Agathiyan have designed the costumes for the characters.  In May 2013, convinced by the script, Prithviraj Sukumaran was signed to play a parallel leading role alongside Siddharth.  In a press release of June 2013, the team revealed that actresses Vedhicka and Anaika Soti were also added to the cast thereafter, as were supporting actors Nassar and Thambi Ramaiah.  Lakshmi Rai was also linked to play a role in the film but her inclusion remained unconfirmed.  Pre-production works had begun by May 2013.  Niranjani Agathiyan did additional make-up for the film. 

===Filming===
Principal photography for the film started in early June 2013, wherein reports suggested that the film would be based on the based on the lives of yesteryear singers  . Retrieved 17 September 2014. 

==Music==
 

The original songs and background score were composed by A. R. Rahman. Rahman underwent six months of research for the music of the film to relate it with the era of 1920s.    The soundtrack has seven original songs and fourteen bit songs that form the score of the film. The track "Vanga Makka Vaanga" was released as a single on 11 July 2014    and the second single "Yaarumilla" was released exactly a month later.    The second single made its debut on eight position on iTunes–India single tracks. The album was digitally released on 18 August 2014 to positive reviews.  The album also released in Malayalam as Pradhi Nayagan on 26 October 2014

==Release==
The satellite rights of the film were sold to STAR Vijay. 

===Distribution===
Dream Factory was selected to distribute the film in other countries, including India. 

===Marketing=== Siddharth were released. 

===Critical reception=== International Business Oneindia Entertainment rated the film 3.5 out of 5 and stated "Kaaviya Thalaivan is a classic from Vasanthabalan which is simply brilliant except for the song placement of Yaarumilla song which disturbs the pace of the movie. But that shouldnt stop anyone from witnessing Kaaviya Thalaivans magic in theaters. Watch out for the dialogue between Siddharth and Nassar before the interval and the conversation between Siddharth and Prithviraj in the climax which are the highlights of this extraordinary film." 

S. Saraswati of Rediff.com felt that the film is worth a watch, giving it a rating of 3.5 out of 5 and wrote "This is a role of a lifetime for Siddharth, a truly memorable performance. He sports innumerable looks in the film and is perfect in every one of them, totally at ease with the character and the body language. Equally adept are Prithviraj, Vedhika, Nassar and Thambi Ramaiah. On the downside, the spotlight seems to be focused a little longer than necessary on Siddharth, strengthening his character. Prithviraj and Vedhika are left holding the short end of the stick." Behindwoods rated the film 3 out of 5 and stated "Technically very sound, Kaaviya Thalaivan is marked by excellent performances and production values. Balan has brought alive in front of us the stage, the makeup, the color and the myriad other things that go into a drama production of those days. Credits are definitely due to Vasanthabalan for his efforts to showcase the tradition of performing arts in a bygone era to the WhatsApp generation. Kaaviya Thalaivan sure is a different viewing experience, give it your time and patience."  At Behindwoods, Dilani Rabindran wrote "Kaaviyathalaivan deserves to be seen widely and appreciated internationally, not just because it is richly made – but because it is differently made, and the results are reverentially entertaining. It is truly sad that we don’t see more ambitious films like this in South Indian cinema, but perhaps that is what makes it such a gem. I highly suggest you do not miss seeing this rare commodity on the big screen." 

However, Baradwaj Rangan of The Hindu remarked that the film had a great premise which doesnt fulfill its potential. He felt that Siddharth and Prithviraj struggled with ill-defined parts and extremely predictable events and after criticizing the film for the modernization of sequences and dialogues, selection of singers and lack of proper confrontation scenes between the protagonists, he concluded "There is no denying Vasanthabalan’s desire to make good cinema, but like his other films, Kaaviya Thalaivan makes us give him an A for effort, even as we rummage down the alphabet when it comes to aspects of the execution".  Haricharan Pudipeddi of Indo-Asian News Service|IANS stated "At the end of Kaaviya Thalaivan, theres a side in you that wants to laud the effort of auteur G. Vasanthabalan, who has always strived to give us good cinema, especially for recreating a bygone era in grand style on screen. Then, theres another side that will ask why all his films with great potential dont get their due treatment. You would salute the effort that has gone into its making, but never the final product" and rated the film 3 out of 5 and praised Vasanthabalan for extracting good performances from the principal cast and the films offbeat theme. However, he felt that the film had a heavy reference from Mahabharata and compared the characters of Nassar, Prithviraj and Siddharth with Drona, Arjuna and Ekalavya respectively; though he felt that Arjuna and Ekalavya are pitted against each other in this film. 
Cinemalead rated a 3.5 out of 5 and wrote,"A classic from Vasantha Balan!" 

==References==

 

==External links==
*  

 

 
 
 
 
 
 
 