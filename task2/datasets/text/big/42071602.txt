Gorbaciof
{{Infobox film
 | name = Gorbaciof
 | image = Gorbaciof.jpg
 | caption =
 | director = Stefano Incerti
 | writer =   
 | starring =  
 | music =   Teho Teardo
 | cinematography =   	Pasquale Mari
 | editing =   
 | producer = Massimo Vigliar
 | released =  
 }} 2010 Cinema Italian drama film directed by Stefano Incerti. It premiered out of competition at the 67th Venice International Film Festival.  

== Plot ==
Marino Pacileo is a solitary man living in the Vasto area of Naples, a multiethnic suburb close to the train station. He is nicknamed "Gorbaciof" as, like the Soviet leader, has a port-wine stain on his forehead. He is a cashier in the prison at Poggioreale, using his job as a means of funding his gambling addiction. He plays poker in a makeshift gambling house at the back of a Chinese restaurant, and here he meets Lila, daughter of the restaurants owner. Gorbaciof is fascinated by Lila, whom he starts stalking; and Lila finds herself attracted to this strange man who rescued her from two violent teenagers. In spite of the language barrier (Lila doesnt speak Italian), they start seeing each other.

Gorbaciof gives the girls father a large sum of money in order for him to pay off his debts, thus avoiding Lila becoming a prostitute. However, a bad hand at poker has Gorbaciof owing a large sum of money to a corrupt lawyer and he is forced to ask a prison guard for a loan. The guard asks Gorbaciof to provide him with several illegal services, in exchange for the money. Gorbaciof then plans to leave Naples with Lila, but needs to complete a dangerous assignment first: be a lookout during a risky robbery.

The robbery goes well and Gorbaciof manages to escape, along with two other accomplices, but they are soon followed by the gun-wielding cashier. While everyone in the car is rejoicing, tragedy happens: one of the accomplices two gun accidentally fires a shot, which mortally wounds Gorbaciof. Here we are shown parallel scenes of Gorbaciof dying and Lila waiting for him at the airport. The movie ends with the last things Gorbaciof sees: Naples buildings that fly past him, as the car speeds through traffic. 

== Cast ==

*Toni Servillo: Gorbaciof
*Yang Mi: Lila
*Nello Mascia: Vanacore  
*Geppy Gleijeses: the lawyer

==References==
 

==See also==
*Movies about immigration to Italy

==External links==
* 

 
 
 
 


 
 