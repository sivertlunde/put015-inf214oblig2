Amma Ammaayiyamma
{{Infobox film
| name = Amma Ammaayiyamma
| image =
| image_size =
| caption =
| director = Sandhya Mohan
| producer = Somasekharan
| writer = Udaykrishna Sibi K Thomas
| screenplay = Udaykrishna Sibi K Thomas Mukesh KPAC Meera
| music = M. S. Viswanathan
| cinematography = Vijayakumar
| editing = K. Rajagopal
| studio = Classic Cine Vision
| distributor =
| released =  
| country = India Malayalam
}}
 1998 Cinema Indian Malayalam Malayalam film, Meera in lead roles. The film had musical score by M. S. Viswanathan.   

==Cast==
  
*Sukumari  Mukesh 
*KPAC Lalitha  Meera 
*Tony Anju 
*Harishree Ashokan 
*Jose Pellissery 
*Krishnaprasad
*N. F. Varghese  Sukanya 
*Usharani 
*Meera
*Kanya Bharathi
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Mele Ponveyil || M. G. Sreekumar, Sangeetha (New) || MD Rajendran || 
|- 
| 2 || Mele Ponveyil || M. G. Sreekumar || MD Rajendran || 
|- 
| 3 || Vaayil vellikkarandiyumaayi || M. G. Sreekumar, MS Viswanathan || MD Rajendran || 
|- 
| 4 || Velicham Vilakkine || Jayachandran || MD Rajendran || 
|- 
| 5 || Velicham Vilakkine || Arundhathi || MD Rajendran || 
|}

==References==
 

==External links==
*   
*  

 
 
 


 