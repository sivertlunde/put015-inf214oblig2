Bojhena Shey Bojhena
 
 
 

{{Infobox film
| name = Bojhena Shey Bojhena
| image = It is the poster of the film Bojhena Shey Bojhena.jpg
| caption = Theatrical release poster
| image_size =
| director = Raj Chakraborty
| producer = Srikant Mohta Mahendra Soni
| writer = M. Saravanan (film director)|M. Saravanan
| starring = Soham Chakraborty Mimi Chakraborty Abir Chatterjee Payel Sarkar
| music = Arindom Chatterjee
| cinematography = Subhankar Bhor
| editing =  Bodhaditya Banerjee Saikat Sengupta
| studio = Shree Venkatesh Films
| released =  
| choreography = Baba Yadav
| country = India
| runtime = 145 minutes Bengali
| budget = 2 cr
| gross =  4 cr
}} Bengali romantic drama film directed by Raj Chakraborty and produced by Shree Venkatesh Films.This film is a remake of Tamil film named Engaeyum Eppothum (Anywhere Anytime) released in 2011. The film illustrates two love stories – with Soham Chakraborty & Mimi Chakraborty playing the characters in one of the stories and Abir Chatterjee & Payel Sarkar playing the other – which join together in the climax. The film was initially titled Prem Amar 2 but eventually the director changed the name because of the entry of two new characters and a different storyline. The films budget was  . The films music is by debutante Arindom Chatterjee.

The films theatrical trailer was unveiled on the films official YouTube channel on 8 December 2012. 

==Plot==

The film starts with an accident. Then it shifts back several months before when Joyita (Payal Sarkar), a girl from the village of Balurghat comes to Kolkata to give an interview for her work. But her sister who was supposed to guide her could not come due to the stroke of her father-in-law. She takes help from a stranger named Avik (Abir Chatterjee) who bunks his job in order to help her but ends up the entire day with her. The next day Joyita returns to her village and soon realizes her love for Avik. Meanwhile in the city Avik also realizes the same and started planning to go to Balurghat to meet her.
 Malda to find a suitable work. Noor falls in love with his neighbour Riya (Mimi Chakraborty), who is a nurse by profession. He watches her every morning for the last six months. One day Noor meets Riya, and expresses his love for her. Riya, however, is bold and forthcoming, agrees to his love and commences to command him mercilessly. She puts him through several tests such as getting him to agree to organ donation, HIV testing, talking him to a police commissioner who is her father, fixing meeting with her former one-sided lover and so on. She wants him to decide on the basis of all that shes put him through as to whether he wants to marry her and spend the rest of his life with her. Noor responds affirmatively and the two grow to love each other unconditionally with the consent of their families.

Then the story flashes back to the present where Noor takes Riya to meet his parents. They ride the same bus which is boarded by Avik too who went to meet Joyita albeit he is returning as he could not find her address. Then another bus shows Joyita who is returning to Balurghat from Kolkata, where she went again to meet Avik. Besides, it also shows a glimpse of a mother and her child, a girls hockey team, a newly married couple, two college students who are attracted to each other and a man returning from Dubai to see his five-year-old daughter for the first time. As they are about to reach their destination, the buses collide head-on, creating a devastating accident. People rush to their aid. Noor ends up with a severe head injury and Riya forces him to go to the hospital by an ambulance which was leaving. She stays back to help others as being a nurse, it was her job. Avik also starts helping others. Finally he sees Joyita injured in another bus. At the hospital, Avik confesses his love to Joyita at her bedside, and she manages to regain consciousness. The film ends with a tragic part as showing Noor and Joyita dead. The concluding scene shows Noors body being taken away by a hysteric Riya and his grieving parents while Avik affectionately caresses the palm of a lifeless Joyita inside the ICU only to leave the hospital later, in grief. The site of the crash is declared an accident prone-area, and the film ends with few messages on road safety. One can never understand the reason behind what life has in store for him/her. Hence, the name:  Bojhena Shey Bojhena.

== Cast ==
*Soham Chakraborty as Noor Islam
*Mimi Chakraborty as Riya
*Abir Chatterjee as Avik
*Payel Sarkar as Joyita
*Ena Saha as Priyanka
* Raj Chakraborty as cameo appearance

== Soundtrack == Indraadip Dasgupta, V Music.

=== Track listing ===
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center" Duration
|-
| 1
| Bojhena Shey Bojhena
|  Anwesha Datta Gupta
| Indradeep Dasgupta
| 5:28
|-
| 2
| Bhogoban
|  Somlata Acharya Chowdhury, Timir Biswas 
| Arindom Chatterjee
| 3:47
|-
| 3
| Kothin
|  Ash King, Sayani Ghosh 
| Arindom Chatterjee
| 4:41
|-
| 4
|  Saajna
|  Prashmita Paul Arindom Chatterjee
| 5:22
|-
| 5
| Bojhena Shey Bojhena
|  Arijit Singh, Rananjay
| Indradeep Dasgupta
| 5:28
|-
| 6
|  Sajna – Reprise
|  Arijit Singh  Arindom Chatterjee
| 3:18
|-
| 7
| Na Re Na
| Arijit Singh 
| Arindom Chatterjee
| 4:31
|}

== References ==
 

==External links==
* 
* 

 

 
 
 

 