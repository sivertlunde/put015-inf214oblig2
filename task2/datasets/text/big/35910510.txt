Pandi Oliperukki Nilayam
{{Infobox film
| name = Pandi Oliperukki Nilayam
| image =
| caption =
| director = Rasu Madhuravan
| producer = Rasu Maduravan
| writer = Rasu Madhuravan
| starring = Sabarish Sunaina
| music = 
| cinematography = U. K. Senthilkumar
| editing = 
| studio = 
| distributor =
| released =  
| runtime = 
| country = India
| language = Tamil
| budget =
| gross =
}}
Pandi Oliperukki Nilayam is a 2012 Tamil romantic comedy film written and directed by Rasu Madhuravan.  The film will have Sabarish of Markandeyan fame  and Sunaina in the lead roles.   The movie was originally called Mike Set Pandi later renamed to Pandi Oli Perukki Nilayam. 

==Cast==
* Sabarish as Pandi
* Sunaina as Valarmathi
* Karunas
* Thambi Ramaiah
* Singam Puli Soori as Soori Rajkapoor
* K. Selva Bharathy
* Vaiyapuri
* Vatsan  as Siluva
* Prabu as paramen
* Devaraj
* Pandian
* Veerasamar
* Nagendran
* Madurai Market Muthu
* Pasumpon Suresh
* Periyasamy as childhood pandi

==Production==

===Casting===
Sabarish, son of popular stunt choreographer FEFSI Vijayan, selected for the male lead.  Singam Puli and Vatsan will be seen in important roles. Deva, Pandian, Veerasamar, Nagendran, Madurai Market Muthu and Pasumpon Suresh are selected for Sunainas brothers role. 

==Critical reception==
Rohit Ramachandran of Nowrunning.com rated it 1/5 stating that "Pandi Oli Perukki Nilayam might not be consistently bathed in sentimentality but the melodrama makes up for its late arrival by being one-hundred-percent trite."  Behindwoods Review Board rated it 0.5/5 deeming it to be "an outdated romance-revenge." 

==References==
 

 

 
 
 
 
 


 