So Well Remembered
{{Infobox film
| name           = So Well Remembered
| image          = So-Well-Remembered.jpg
| caption        = Theatrical poster
| director       = Edward Dmytryk 
| producer       = Adrian Scott James Hilton (novel) John Paxton
| starring       = John Mills Martha Scott Trevor Howard
| music          = Hanns Eisler
| cinematography = Freddie Young
| editing        = Harry W. Gerstad
| distributor    = RKO Radio Pictures
| released       = July 8, 1947 (UK) November 4, 1947 (US)
| runtime        = 114 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
 James Hilton novel of the same name and tells the story of a reformer and the woman he marries in a fictional Lancashire mill town. Hilton also narrated. The movie, shot on location in England, is faithful to the novel in many particulars, but the motivations of the main female character and the tone of the ending are considerably altered.

The first screening was in Macclesfields Majestic Theatre on August 9, 1947, after which the film disappeared.  It was only discovered some 60 years later by Macc Lad Muttley (Tristan ONeill) of The Macc Lads infamy in Tennessee, USA. 

==Plot==
At the end of World War II in the Lancashire mill town of Browdley, town councillor, newspaper editor, and zealous reformer George Boswell (John Mills) recalls the past 26 years of his life. In 1919, he defends Olivia Channing (Martha Scott) when she applies for a library job. Her cotton mill owner father John Channing (Frederick Leister) had been sent to prison for almost 20 years for speculating with and losing many peoples money. 

George falls in love with Olivia, though it scandalises the townspeople, and eventually proposes to her. That night, she has an argument with her father. He has Dr. Richard Whiteside (Trevor Howard) drive him into town to speak to George, but they crash on a washed-out road and John is killed. Olivia then agrees to marry George.

Trevor Mangin (Reginald Tate), Browdleys most influential businessman, asks George to run for Parliament. Seeing an opportunity to further his reforming efforts, George agrees, much to Olivias delight. 

Whiteside brings George an alarming report about the danger of an epidemic in the towns filthy slums. Mangin, who owns many of them, produces a more optimistic one. Given that Whiteside has taken to drinking heavily since the accident, George accepts Mangins report, causing the council to vote to do nothing. However, a diphtheria epidemic breaks out, just as Whiteside feared. A free clinic is opened to inoculate the healthy children and treat the sick. George tells Olivia to take their son there, but she cannot bear to do it, resulting in the childs death. 
 Richard Carlson). Meanwhile, Whiteside takes in a baby girl, Julie Morgan (Patricia Roc), orphaned at birth. George helps raise her.

Many years pass. A widowed Olivia returns, takes up residence in her fathers mansion, and reopens the Channing mill. Her son becomes a flier in the Royal Air Force in World War II. On leave, he meets Julie and they fall in love. However, Olivia does not want to relinquish her son. Charles is seriously injured in combat, his face disfigured. This enables Olivia to isolate and retain control of him, until George manages to convince him to break free and marry Julie. When Olivia arrives, looking for her son, George reveals that he has figured out that Olivia did nothing to prevent her father from driving to his death, though she must have known that the road was washed out. Whiteside had overheard the Channings argument; knowing his daughter, John Channing intended to warn George against her.

==Cast==
* John Mills as George Boswell
* Martha Scott as Olivia Channing Boswell
* Patricia Roc as Julie Morgan
* Trevor Howard as Dr Richard Whiteside Richard Carlson as Charles Winslow
* Reginald Tate as Trevor Mangin
* Beatrice Varley as Annie, Georges loyal servant
* Frederick Leister as John Channing
* Ivor Barnard as Spivey
* Julian DAlbie as Wetherall, the outgoing Member of Parliament
 Juliet and Hayley played Julie as a young girl and a baby respectively.

The music for this film was composed by Hanns Eisler.

==Production==
The film was mainly shot at Denham Film Studios in Denham, Buckinghamshire|Denham, Buckinghamshire. Exteriors were filmed in Macclesfield, Cheshire, forming the backdrop of a Lancashire mill town.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 