Apple Trees
 
 
{{Infobox film
| name           = Apple Trees
| image          =
| caption        =
| director       = Helma Sanders-Brahms
| producer       = Alfred Hürmer
| writer         = Helma Sanders-Brahms
| narrator       =
| starring       = Johanna Schall
| music          =
| cinematography = Jürgen Lenz
| editing        = Monika Schindler
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Germany
| language       = German
| budget         =
}}

Apple Trees ( ) is a 1992 German drama film directed by Helma Sanders-Brahms. It was screened in the Un Certain Regard section at the 1992 Cannes Film Festival.   

==Cast==
* Johanna Schall - Lena
* Thomas Büchel - Heinz
* Udo Kroschwald - Sienks
* Anna Sanders - Lena - jung
* Steffie Spira - Großmutter
* Andrea Meissner - Sienkes Frau
* Roland Kuchenbuch - Stasimann
* Harald Engelmann - Stasimann
* Peter Pauli - Parteisekretär
* Dieter Schindelhauer - Parteifunktionär (as Dieter Schindelbauer)
* Cathlen Gawlich - Susi
* Anne K. Rathsfeld - Helga
* Nana Wolfing - Ingrid
* Marcel Struwe - Markus
* Eva Schäfer - 1. ältere Frau
* Katharina Rothärmel - 2. ältere Frau
* Wilfried Körner - Musiker
* Joahnnes Roeder - Dorfjunge

==References==
 

==External links==
* 
 
 
 
 
 
 


 
 