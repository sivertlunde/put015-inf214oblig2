Strange Wilderness
 
 
{{Infobox film
| name           = Strange Wilderness
| image          = Strange wilderness.jpg
| caption        = Promotional poster Fred Wolf
| producer       = Adam Sandler Jack Giarraputo Allen Covert Peter Gaulke
| writer         = Peter Gaulke Fred Wolf
| starring       = Steve Zahn Allen Covert Jonah Hill Robert Patrick Also Starring Justin Long Jeff Garlin Ernest Borgnine
| music          = Waddy Wachtel
| cinematography = David Hennings
| editing        = Tom Costain
| studio         = Level 1 Entertainment Happy Madison Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 87 minutes
| country        = United States
| language       = English
| budget         = $20 million 
| gross          = $6,964,734 
}} Kevin Heffernan, and Jonah Hill. The film received negative reviews and was a box office bomb, only making nearly $7 million against a $20 million budget.

==Plot==
Peter Gaulke is the host of an unsuccessful nature program called Strange Wilderness. When the show is threatened with cancellation, he goes in search of the elusive bigfoot in order to restore ratings.

==Cast==
*Steve Zahn as Peter Gaulke
*Allen Covert as Fred Wolf
*Justin Long as Junior
*Jonah Hill as Lynn Cooker
*Robert Patrick as Gus Hayden
*Ashley Scott as Cheryl
*Harry Hamlin as Sky Pierson
*Ernest Borgnine as Milas
*Jeff Garlin as Ed Lawson Kevin Heffernan as Whitaker John Farley as a mountain doctor
*Peter Dante as Danny Guiterrez
*Oliver Hudson as TJ / animal handler
*Blake Clark as Dick
*Seth Rogen as a ranger in a helicopter (voice)

==Critical reception==
The film received highly negative reviews.  , it was given a 12% (ranking "overwhelming dislike") based on 12 reviews, placing it at 63rd place for the worst-reviewed films ever.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 