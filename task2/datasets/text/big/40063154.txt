The Face of Love (film)
{{Infobox film
| name           = The Face of Love
| image          = The Face of Love poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Arie Posin
| producer       = Bonnie Curtis Julie Lynn
| writer         = Arie Posin Matthew McDuffie
| starring       = Annette Bening Ed Harris Robin Williams Amy Brenneman Jess Weixler Linda Park
| music          = Marcelo Zarvos
| cinematography = Antonio Riestra
| editing        = Matt Maddox
| studio         = Mockingbird Pictures
| distributor    = IFC Films
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $1,500,147 
}}
 American Romance romantic drama film directed by Arie Posin and co-written by Matthew McDuffie. The film stars Annette Bening, Ed Harris, Robin Williams, Amy Brenneman, Jess Weixler and Linda Park. It was screened in the Special Presentation section at the 2013 Toronto International Film Festival.   

==Plot== Harris in a dual role) whose resemblance to her husband stuns her.  She returns to the gallery several more times, hoping to see him.  Eventually, Nikki identifies him as a professor at a small local college.  She finds "Tom" on the internet and visits an art class he is teaching; but she is emotionally overwhelmed and flees.  Eventually, Nikki calms down and invites Tom to give her private, at home,  tutoring in painting.  She doesnt disclose to him his physical resemblance to her late husband.

Nikki and Tom become lovers, to the emotional distress of her neighbor (Robin Williams), who has hoped to attract her interest romantically since the death of Garret, his friend.  The strain of keeping Tom a secret from her friends and family, who would immediately recognize the uncanny resemblance, begins to wear on Nikki, Tom and the relationship.  Nikki is not alone in keeping secrets in the relationship.  Tom, who is divorced for 10 years from his ex-wife yet still maintains a close friendship with her, does not tell Nikki he suffers from a severe heart ailment.   When Nikki’s adult daughter Summer visits unexpectedly, finding Tom at the house, she flies into a rage repulsed by Tom’s appearance.  Tom believes her outrage is triggered simply by his presence.   
Tom, puzzled by the daughters reaction, accepts when Nikki asks him to fly off to Mexico with her. At a resort often visited by Garett and Nikki when they were married, Tom discovers a photo of Nikki and her husband by the bar, notices the resemblance and also realizes that a psychologically imbalanced Nikki has brought him to the very place her husband died.  Tom confronts her with the resemblance and Nikki runs off into the raging surf, perhaps to commit suicide.  She is rescued by Tom and they later hold one another in bed with the knowledge the relationship isnt based in anything more than his physical similarities to Garret and is doomed.

A year later, Summer finds an art gallery card in Nikkis mail that turns out to be an invitation to a memorial display of Toms art.  Tom has died of his heart condition but produced an outpouring of art in his last year.  Nikki attends the reception hosted by Toms ex-wife and tearfully sees Toms painting of the two of them titled "The Face of Love".

== Cast ==
* Annette Bening as Nikki
* Ed Harris as Tom/Garrett
* Robin Williams as Roger
* Amy Brenneman as Ann, Toms ex-wife
* Jess Weixler as Summer
* Linda Park as Jan
* Jeffrey Vincent Parise
* Kim Farris as Hostess
* Leah Shaw as Farmers market shopper
* Chelsea OConnor as Gallery Waitress
* Deana Molle as Couple #2
* Yuki Bird as Waitress

== Production ==

=== Filming ===
The film was shot in Los Angeles in 2012, and produced by Mockingbird Pictures.

=== Release ===
In May 2013 IFC Films acquired the rights of the film to distribute in the United States.  The film was released in September 2013. 

==Reception==
The Face of Love received negative reviews from critics. On Rotten Tomatoes, the film has a rating of 43%, based on 68 reviews, with a rating of 5.2/10. The sites critical consensus reads, "Perhaps worth checking out if only for the opportunity to see reliably powerful work from Annette Bening and Ed Harris, The Face of Love undermines its leads performances with a scattershot script and aimless direction."  On Metacritic, the film has a score of 51 out of 100, based on 24 critics, indicating "mixed or average reviews ". 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 
 
 
 
 