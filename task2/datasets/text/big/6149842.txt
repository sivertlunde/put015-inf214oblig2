Saved from the Titanic
{{Infobox film
| name = Saved from the Titanic
| image = SavedFromthetitanicposter.PNG
| director = Étienne Arnaud
| writer = 
| starring = {{Plainlist|
* Dorothy Gibson
* Alec B. Francis
* Julia Stuart
* John G. Adolfi
}}
| distributor = Eclair (camera)|Éclair Film Company
| released =  
| runtime = 10 min. (300 m) Silent (English Intertitles)
| country = United States
}}
 silent film|motion short starring sinking of first lifeboat Edward Smith. To add to the films authenticity Gibson wore the same clothes as on the night of the disaster and her rescue. The filming took place in a New Jersey studio and aboard a derelict ship in New York Harbor. It was the first film to be made about the disaster and premiered in the United States just 29 days after the ship sank.

The film was released internationally and attracted large audiences and positive reviews, though some criticized it for commercializing the tragedy so soon after the event. It is now regarded as a lost film, as the last known prints were destroyed in a studio fire in March 1914. A few printed stills are now all that is known to survive of it. It is also the last film that Dorothy Gibson ever made, as she suffered a mental breakdown after completing it, apparently due to the mental strain that it caused her.

== Gibsons voyage on the Titanic ==
 predecessor to Bridge (card game)|todays game) in a first-class saloon before retiring to the cabin that she shared with her mother.   The game was later credited with saving the lives of the players who had stayed up late to finish it, despite it being (as one American writer put it) "a violation of the strict Sabbath rules of English vessels."  The collision with the iceberg at 11:40 pm sounded to Gibson like a "long, drawn, sickening scrunch". After going to investigate, she fetched her mother when she saw Titanic s deck beginning to list as water flooded into the ships boiler rooms. 
 RMS Carpathia and taken to New York. 

== Production ==
  Edward Smith RMS Olympic, Titanic s sister ship, images of the launch of Titanic in 1911 and stock footage of icebergs. On April 22, the resulting newsreel was released as part of the studios Animated Weekly series.  It was an enormous success with sold-out showings across America. President William Howard Taft, whose friend and military aide Archibald Butt was among the victims of the disaster, received a personal copy of the film. 

The success of the newsreel appears to have convinced Brulatour to capitalize further with a drama based on the sinking. He had a unique advantage – a leading actress who was a survivor and eyewitness to what had happened. Gibson later described her decision to participate as an "opportunity to pay tribute to those who gave their lives on that awful night."  Jeffrey Richards suggests that it was more likely that Brulatour persuaded her that the disaster offered an opportunity to advance her career.  The filming took place at Éclairs studio in Fort Lee, New Jersey  and aboard a derelict transport vessel in New York Harbor.  It was completed in only a week and the entire process of filming, processing and distribution took only half the time normally required for a one-reel film – a sign of the producers eagerness to get the film onto screens while news of the disaster was still fresh.  The film was only ten minutes long  but this was typical of the time, as feature films had not yet become the norm. Instead, a program typically consisted of six to eight short films, each between ten and fifteen minutes long and covering a range of genres. Although newsreels were the main vehicle for presenting current events, dramas and comedies also picked up on such issues. There was very little footage of Titanic herself, which hindered the ability of newsreels to depict the sinking; however, the disaster was an obvious subject for a drama. 

Gibson was plainly still traumatized – a reporter from the Motion Picture News described her as having "the appearance of one whose nerves had been greatly shocked" – and she was said to have burst into tears during filming. To add to the films air of authenticity, she even wore the same clothes that she was rescued in.  Nonetheless, as well as starring as "Miss Dorothy" – herself, in effect – Gibson is said to have co-written the script, which was based around a fictionalized version of her own experiences. Her parents and (fictional) fiancé, Ensign Jack, are shown waiting anxiously for her return after hearing news of the disaster. She arrives safely back home and recounts the events of the disaster in a long flashback (narrative)|flashback, illustrated with newsreel footage of Titanic and a mockup of the collision itself. Titanic sinks but Dorothy is saved. When she concludes her story, her mother urges Dorothys fiancé to leave the navy as it is too dangerous a career. Jack ultimately rejects the mothers advice, deciding that he must do his duty to flag and country. Dorothys father is moved by his patriotism and the film ends with him blessing the marriage. 

The films structure aimed to promote its storys authenticity and credibility through the integration of newsreel footage and the presence of a genuine survivor as the "narrator". Audiences had previously seen survivors of disasters only as unspeaking "objects" shown as part of a story told by someone else. Gibson, by contrast, was a survivor given voice as the narrator of what was ostensibly her personal story. 

== Release and reception ==
 
Saved from the Titanic was released in the United States on May 14, 1912   and was also released internationally, in the United Kingdom as A Survivor of the Titanic  and in Germany as Was die Titanic sie lehrte ("What the Titanic Taught Her").  It attracted a positive review in Motion Picture World of May 11, 1912, which described Gibsons performance as "a unique piece of acting in the sensational new film-play of the Éclair Company ...   creating a great activity in the market, for the universal interest in the catastrophe has made a national demand."  The review went on:

 }}

The Moving Picture News commended the films "wonderful mechanical and lighting effects, realistic scenes, perfect reproduction of the true history of the fateful trip, magnificently acted. A heart-stirring tale of the seas greatest tragedy depicted by an eye-witness." However, some criticized the questionable tastefulness of portraying a disaster that had so recently occurred. "Spectator" in the New York Dramatic Mirror condemned the venture as "revolting":

 }}

== Fate ==

Saved from the Titanic is now considered a lost film, as the only known prints were destroyed in a fire at Éclair Studios in March 1914. Its only surviving visual records are a few production stills, printed in the Moving Picture News and Motion Picture World, showing scenes of the family and a still of Dorothy standing in front of a map of the North Atlantic pointing to the location of the Titanic.  Frank Thompson highights the film as one of a number of "important movies that disappeared", noting that it was unique for having "an actual survivor of the Titanic playing herself in a film" while wearing "the very clothes . . . in which she abandoned ship":

 }}

It was also Dorothy Gibsons last film, as the effort of making it appears to have brought on an existential crisis for her. According to a report in the Harrisburg Leader, "she had practically lost her reason, by virtue of the terrible strain she had been under to graphically portray her part." 

== Cast ==
* Dorothy Gibson as Miss Dorothy
* Alec B. Francis as Father
* Julia Stuart as Mother
* John G. Adolfi as Ensign Jack
* William R. Dunn as Jacks pal
* Guy Oliver as Jacks pal 

== See also ==
* In Nacht und Eis (1912), the first European film about the disaster
* List of lost films List of films about the RMS Titanic

== Footnotes ==

 

== References ==

 

* {{cite book
| last = Bottomore
| first = Stephen
| year = 2000
| title = The Titanic and Silent Cinema
| publisher = The Projection Box
| location = Hastings, UK
| isbn = 978-1-903000-00-7
| ref = harv
}}
* {{cite book
  | last = Davenport-Hines
  | first = Richard
  | year = 2012
  | title = Titanic Lives: Migrants and Millionaires, Conmen and Crew
  | publisher = HarperCollins UK
  | location = London
  | isbn = 978-0-00-732165-0
  | ref = harv
  }}
* {{cite book
  | last = Howells
  | first = Richard
  | year = 1999
  | title = The Myth of the Titanic
  | publisher = MacMillan Press
  | location = United Kingdom
  | isbn = 978-0-333-72597-9
  | ref = harv
  }}
* {{cite book
| last = Koszarski
| first = Richard
| year = 2004
| title = Fort Lee: The Film Town
| publisher = John Libbey Publishing
| location = Rome, Italy
| isbn = 978-0-86196-653-0
| ref = harv
}}
* {{cite book
| last = Leavy
| first = Patricia
| year = 2007
| title = Iconic Events: Media, Politics, and Power in Retelling History
| publisher = Lexington Books
| location = Lanham, MD
| isbn = 978-0-7391-1520-6
| ref = harv
}}
* {{cite book
  | last = Mowbray
  | first = Jay Henry
  | year = 1912
  | title = Sinking of the Titanic
  | publisher = The Minter Company
  | location = Harrisburg, PA
  | oclc = 9176732
  | ref = harv
  }}
* {{cite book
  | last = Richards
  | first = Jeffrey
  | title = A Night to Remember: The Definitive Titanic Film
  | year = 2003
  | publisher = I.B.Tauris
  | location = London
  | isbn = 978-1-86064-849-6
  | ref = harv
  }}
* {{cite book
  | last = Shapiro
  | first = Marc
  | year = 1998
  | title = Total Titanic
  | publisher = Byron Preiss
  | location = New York
  | isbn = 978-0-671-01202-1
  | ref = harv
  }}
* {{cite book
  | last = Spignesi
  | first = Stephen J.
  | title = The Titanic For Dummies
  | year = 2012
  | publisher = John Wiley & Sons
  | location = Hoboken, NJ
  | isbn = 978-1-118-20651-5
  | ref = harv
  }}
* {{cite book
  | editor1-last = Bergfelder
  | editor1-first = Tim
  | editor2-last = Street
  | editor2-first = Sarah
  | last = Wedel
  | first = Michael
  | year = 2004
  | chapter = Early German Cinema and the Modern Media Event
  | title = The Titanic in Myth and Memory : Representations in Visual and Literary Culture
  | publisher = I.B. Tauris
  | location = London
  | isbn = 978-0-7524-6210-3
  | ref = harv
  }}
* {{cite book
  | last= Thompson
  | first= Frank
  | title=Lost Films: Important Movies That Disappeared
  | year=1996
  | publisher=Carol Publishing Group
  | location=New York
  | isbn=9780806516042
  | ref = harv
  }}
* {{cite book
  | last = Wilson
  | first = Andrew
  | title = Shadow of the Titanic
  | year = 2011
  | publisher = Simon & Schuster Ltd
  | location = London
  | isbn = 978-1-84737-730-2
  | ref = harv
  }}
* {{cite book
  | last1 = Wormstedt
  | first1 = Bill
  | last2 = Fitch
  | first2 = Tad
  | year = 2011
  | chapter = An Account of the Saving of Those on Board
  | title = Report into the Loss of the SS Titanic: A Centennial Reappraisal
  | editor-last = Halpern
  | editor-first = Samuel
  | publisher = The History Press
  | location = Stroud, UK
  | isbn = 978-0-7524-6210-3
  | ref = harv
  }}
 

== External links ==
 
*  
*  
*  , an account of Dorothy Gibsons experiences
 

 

 
 
 
 
 
 
 