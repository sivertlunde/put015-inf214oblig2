Kids World (film)
 

{{Infobox film
| name           = Kids World (aka Honey, the Kids Rule the World)
| image          = kidsworld.jpeg
| alt            = 
| caption        = early DVD coverart
| director       = Dale G. Bradley
| producers      =  
| writer         = Michael Lach
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       =  
| music          = Bruce Lynch
| cinematography = Neil Cervin
| editing        = Douglas Braddock
| production companies =  
| distributors   =  
| released       =   
| runtime        = 93 minutes
| country        =  
| language       = English
| budget         = 
| gross          = $19,354 
}}

Kids World is a 2001 childrens film written by Michael Lach and directed by Dale G. Bradley. Though the story is set in Oregon, the project was filmed in Auckland, New Zealand.   The film had limited release in the US in 2001, before its New Zealand release in 2002.  In the United Kingdom its DVD title was Honey, the Kids Rule the World.  In 2007 it had DVD release under that title by Third Millennium Distribution and in 2008 by Boulevard Entertainment.     It aired in 2007 in Romania on Kanal D television.

==Plot==
12 year old Ryan Mitchell (Blake Foster) and his friends Stu (Anton Tennet) and Twinkie (Michael Purvis) are tired of being told what to do. They have to do their homework, eat their vegetables, wear a coat when they go outside, wear a helmet when they ride their skateboards, and arent allowed to come and go as they please like their older brothers and sisters. One day, Ryan and his pals find an ancient Native American burial ground, where they discover a magical wishing glass. Using the glass, Ryan wishes that all the grow-ups and teenagers in the world would simply disappear—and suddenly, his wish comes true! Its party time for Ryan and all his friends, until they discover theres a fly in the ointment—the moment anyone turns 13, they suddenly vanish!

==Main cast==
 
* Christopher Lloyd as Leo
* Blake Foster as Mitchell
* Anton Tennet as Stu
* Olivia Tennet as Nicole Mitchell Anna Wilson as Holly
* Michael Purvis   as Twinkie
* Todd Emerson as Detloff
 

==Recognition==

===Reception===
The film was most likely panned by critics, being noted as one of the worst films ever made. Variety (magazine)|Variety panned the film calling it "Charmless and exceptionally tasteless pre-teen time-filler", and "the sort of movie that seems conceived more out of tax-credit incentives than from any real desire to engage childrens imaginations." The story is set in Oregon but, as exemplified by the principle casts accents, shot in New Zealand. The work of writer Michael Lach and director Dale G. Bradley was pointed out as a "slack setup, in which recycle decades-old cliches about why kids don’t get along with their parents."   They found Christopher Lloyds presence as a mentally handicapped man with the mind of a child inexplicable, in that it was a "thankless role, which requires little of him, except to sit on a porch, playing a didgeridoo (another good hint that we’re not really in Oregon) until, in a few moments of convenient lucidity, he helps to save the day."   

Conversely, Australias Urban Cinefile gave a positive review, offering that it was "refreshing to see a film where kids actually behave like kids", calling the film old fashioned, "in that it relies on its storyline and engaging performances by a terrific young cast".  They felt the film was a colorful adventure for pre-teens and that it was "jam packed with humour, action and enough twists and turns to keep us engaged.  "   The also felt the music score was both fast paced and up-beat, and praised the entire cast, noting that Christopher Lloyds character was credible.   

===Awards and nominations===

* 2000, Won Best Juvenile Performer for Olivia Tennet at New Zealand Film and TV Awards
* 2002, Best Performance in a Feature Film - Leading Young Actor nomination for Blake Foster at Young Artist Awards
* 2002, Best Family Feature Film - Comedy nomination at Young Artist Awards

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 