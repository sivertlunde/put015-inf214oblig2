Don't Call Me Girlie
 
Dont Call Me Girlie is a 1985 documentary about women in the Australian film industry in the 1920s and 1930s. There was an accompanying book of the same name by Andree Wright.

It features Aileen Britton, Charlotte Francis, Nancy Gurr, Jean Hatton, Jocelyn Howarth, Louise Lovely, Lottie Lyell, McDonagh Sisters, Marjorie Osborne, Shirley Ann Richards and Helen Twelvetrees.

The title is taking from a line spoken by Shirley Ann Richards in Dad and Dave Come to Town (1938).

==External links==
*  at TCMDB
*  at IMDB
*  at Peter Malone
*  at Ronin films

 