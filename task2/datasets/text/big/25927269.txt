Calendar (2009 film)
 
{{Infobox film
| name           = Calendar
| image          = Calendar (2009 film).jpg
| alt            = 
| caption        =  Mahesh
| producer       = Saji Nanthyattu
| writer         = Babu Janardhanan Mukesh Zarina Wahab  Navya Nair  Prithviraj Sukumaran
| music          = Afzal Yusuf
| cinematography = Vipin Mohan
| editing        = Vijay Sankar
| studio         = Nanthiyattu Films
| distributor    =
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Malayalam romance film starring  Mukesh (actor)|Mukesh, Navya Nair, Zarina Wahab and Prithviraj Sukumaran, Prithvirajs real-life mother Mallika Sukumaran, Jagathi Sreekumar.

==Plot==
Thankom George is a college lecturer. After her husbands death, she is a widowed single mother to her daughter (20-year-old Kochurani). Meanwhile, Sojappan is a high school drop-out and businessman struggling to make money with his friend Manjooran. Sojappan and Kouchrani fall in love with each other, although Thankom doesnt approve of Sojappan. Three years later, Dr. Roy Philip falls in love with Kouchrani.

==Cast== Mukesh as Dr. Roy Philip
*Zarina Wahab as Thankam George
*Navya Nair as Kochurani
*Prithviraj Sukumaran as Sojappan
*Jagathi Sreekumar as Manjooran
*Mallika Sukumaran as Annamma
*Kottayam Nazeer
*Maniyan Pilla Raju as priest 
*Sidhartha Siva Ashokan
*Archana Menon as a nun
*Renjusha Menon as Clara
*Sangeetha Shivan as Pulikkapparambil Variyachans daughter
*Ambika Mohan as Pulikkapparambil Variyachans wife
*Joy John Antony as Joy

== External links ==
*  

 
 
 
 
 
 
 
 