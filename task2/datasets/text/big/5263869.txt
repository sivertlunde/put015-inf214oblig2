Sans Soleil
{{Infobox film
| name           = Sans Soleil
| image          = Sans Soleil.jpg
| caption        = Film poster
| director       = Chris Marker
| producer       =
| writer         = Chris Marker (as Sandor Krasna)
| narrator       = Florence Delay (French version) Riyoko Ikeda (Japanese version) Charlotte Kerr (German version) Alexandra Stewart (English version)
| music          = Chris Marker (as Michel Krasna)
| cinematography = Chris Marker (as Sandor Krasna)
| editing        = Chris Marker
| distributor    = Argos Films
| released       =  
| runtime        = 100 minutes
| country        = France
| language       = French
| budget         =
}} 1983 French film directed by Chris Marker, a meditation on the nature of human memory, showing the inability to recall the context and nuances of memory, and how, as a result, the perception of personal and global histories is affected. In a 2014 Sight and Sound poll, film critics voted Sans Soleil the third best documentary film of all time.     The title Sans Soleil is from the song cycle Sunless (song cycle)|Sunless by Modest Mussorgsky.

==Description== documentary genre, this experimental essay-film is a composition of thoughts, images and scenes, mainly from Japan and Guinea-Bissau, "two extreme poles of survival".  Some other scenes were filmed in Iceland, Paris, and San Francisco. A female narrator reads from letters supposedly sent to her by the (fictitious) cameraman Sandor Krasna. 

Sans Soleil is often labeled a documentary, travelogue, or essay-film. Despite the films modest use of fictional content, it should not be confused with a mockumentary (mock documentary). The fictional content derived from the juxtaposition of narrative and image adds meaning to the film along with occasional nondescript movement between locations and lack of character-based narrative.

==Introductory quotations==
The original French version of Sans Soleil opens with the following quotation by Jean Racine from the second preface to his tragedy Bajazet (play)|Bajazet (1672):
 "Léloignement des pays répare en quelque sorte la trop grande proximité des temps." 
(The distance between the countries compensates somewhat for the excessive closeness of the times.) 
 Ash Wednesday (1930) for the English version of the film:

 "Because I know that time is always time
 And place is always and only place
 And what is actual is actual only for one time
 And only for one place".  

==Production== Beaulieu silent synchronous sound.  Some of the stock footage shots were colourized with a Spectron video synthesizer.  A number of sequences in Sans Soleil are borrowed from other filmmakers who are not mentioned until the films credits, except the footage of the Icelandic volcano which is accredited in narration to Haroun Tazieff. 

The filmmakers who were not mentioned in Markers film whose footage was used were Sana Na Nhada, Jean-Michel Humeau, Mario Marret, Eugenio Bentivoglio and Daniele Tessier. Pierre Camus was an assistant director; Anne-Marie L Hote and Catherine Adda, assistant editors; Antoine Bonfanti and Paul Bertault, mixing.  

The film was assembled largely in the 1970s, a period when Marker was part of a political commune and preferred to downplay his authorial signature, which may partly explain why he is represented in the film by Sandor Krasnas letters. The title "Conception and editing: Chris Marker," buried in a long list of credits, is the only indication that Sans Soleil is his film.   

==Influences==
The sequence in San Francisco references Alfred Hitchcocks Vertigo (film)|Vertigo and Markers own earlier film La jetée. Markers use of the name "The Zone" to describe the space in which Hayao Yamanekos images are transformed is a homage to Stalker (1979 film)|Stalker, a film by Andrei Tarkovsky, as noted in one of the letters read in the film.  English rock band Kasabian used a sound clip from the documentary in their West Ryder Pauper Lunatic Asylum album at the beginning of the song "West Ryder Silver Bullet".

==References==
 

==External links==
 
* 
*  
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 