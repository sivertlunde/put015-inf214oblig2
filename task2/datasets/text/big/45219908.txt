Passing Through (film)
{{Infobox film
| name           = Passing Through
| image          = Passing Through (1921).jpg
| alt            = 
| caption        = Film still
| director       = William A. Seiter
| producer       = Thomas H. Ince
| screenplay     = Agnes Christine Johnston Joseph F. Poland Fred Gamble Bert Hadley Margaret Livingston
| music          = 
| cinematography = Bert Cann 	
| editing        = 
| studio         = Thomas H. Ince Corporation Famous Players-Lasky Corporation
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent comedy Fred Gamble, Bert Hadley and Margaret Livingston. The film was released on August 14, 1921, by Paramount Pictures.  Its survival status is classified as unknown,  which suggests that it is a lost film.
 
==Plot==
 

== Cast ==
*Douglas MacLean as Billy Barton
*Madge Bellamy as Mary Spivins 
*Otto Hoffman as James Spivins
*Cameron Coffey as Willie Spivins Fred Gamble as Hezikah Briggs
*Bert Hadley as Henry Kingston
*Margaret Livingston as Louise Kingston
*Louis Natheaux as Fred Kingston
*Willis Robards as Silas Harkins
*Edith Yorke as Mother Harkins 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 