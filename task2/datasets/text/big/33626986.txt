The Year of Getting to Know Us
 
{{Infobox film
| name           = The Year of Getting to Know Us
| image          = The Year of Getting to Know Us (2008).jpg
| caption        = Theatrical release poster
| director       = Patrick Sisam
| producer       = Kia Jam
| writer         = Patrick Sisam  Rick Velleu Tom Arnold Sharon Stone Jimmy Fallon Lucy Liu
| music          = John Swihart
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Capitol Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Tom Arnold, Jimmy Fallon, Lucy Liu and Sharon Stone. It premiered at 24 January 2008 at the Sundance Film Festival. 

==Plot==
Christopher Rocket is a man loyal to his work but unable to fulfill his girlfriend Anne or anything else in his personal life. He came back to his homeland after his estranged father Tom Rocket suffers a stroke. Jane Rocket, his mother, is a central figure in his dysfunctional past.

==Cast== Tom Arnold as  Ron Rocket
*Jimmy Fallon as Christopher Rocket
*Chase Ellison as Young Christopher Rocket
*Sharon Stone as Jane Rocket
*Lucy Liu as Anne
*Paul Spicer as Security Guard

==Reception==
The film was not very well received by film critics, resulting in mostly negative reviews, with Rotten Tomatoes giving it an 18% "Rotten" rating. 
http://www.rottentomatoes.com/m/year_of_getting_to_know_us/ 

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 