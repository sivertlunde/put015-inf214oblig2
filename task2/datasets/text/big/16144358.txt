Pushcarts and Plantations: Jewish Life in Louisiana
{{Infobox film
| name           = Pushcarts and Plantations: Jewish Life in Louisiana
| image          = Pushcarts and Plantations screenshot.jpg
| image_size     =
| caption        =
| director       = Brian Cohen
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1998
| runtime        = 54 min.
| country        = USA English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 documentary about Louisiana Jewry from award-winning director Brian Cohen. The documentary combines interviews with historians and locals to tell the 300-year old history of different Jewish communities found in the North, South, and New Orleans. The documentary shares anecdotes about local heroes, little known facts, and personal accounts.

==Summary==
Sephardic traders were the first Jews to settle in Louisiana, and, despite antisemitic legislation, their community thrived. Over a hundred years later, in the 1830s and ’40s a large number of Jews from Alsace-Lorraine settled in Northern Louisiana.
 Creole cookbook, which shows how observant Jews can whip up Fake Frog’s Legs or Oysters Mock-a-Feller — a twist on the famous New Orleans dish Oysters Rockefeller that replaces the oysters with gefilte fish.

The documentary explores what it means to be Jewish in a community where it’s generally assumed that everyone is Christian. “Lafayette is not New York,” one woman says, “you’re not surrounded by Jews. So you can’t just decide, I’ll be Jewish and just be.” If you want to be Jewish, she explains, you have to be an active Jew. One mother in Southern Louisiana recalls that in order to have her children bar and bat mitzvahed she had to teach them Hebrew herself and then drive them hours away to New Orleans for the ceremony.

==Trivia==
The first king of Mardi Gras, Lewis Salomon, was Jewish. But since that celebration in 1872 there has yet to be another Jewish king.

==Reception==
"Poignant...with evocative voices and image," said Museum of the Southern Jewish experience.

"A thoughtful and compassionate documentary," said Toronto Jewish Film Festival

==See also==
*New Orleans
*Louisiana
Other documentaries about Jewish communities in Amierica:
*Island of Roses
*A Home on the Range
* 
*Jews and Buddhism

==References==
*  

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 