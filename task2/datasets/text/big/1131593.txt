Kiss the Girls and Make Them Die
{{Infobox film
| name = Kiss the Girls and Make Them Die
| image = Kissgirldie.jpg
| image_size =
| caption = Original film poster
| director = Henry Levin
| producer = Dino De Laurentiis
| writer = Story by Dino Maiuri Screenplay by Jack Pulman Dino Maiuri
| narrator = Michael Connors Margaret Lee and Terry-Thomas
| music = Mario Nascimbene
| cinematography = Aldo Tonti
| editing = Ralph Kemplen
| distributor =
| released =  
| runtime = 106 minutes
| country = Italy English
| budget =
}}
 Michael Connors, Dorothy Provine and, as the villain, Raf Vallone, it was originally filmed from January to March 1966 under the title Operation Paradise  and distributed in some parts of the English-speaking world as If All the Women in the World. 

==Plot== Michael Connors), an aristocratic female MI6|MI-6 agent (Dorothy Provine), and her chauffeur (Terry-Thomas), driving a Rolls-Royce car filled with spy gadgets, team up to stop the madman. 

==Cast==
   Michael Connors: Kelly
* Dorothy Provine: Susan Fleming
* Raf Vallone: Mr. Ardonian Margaret Lee: Grace
 
* Nicoletta Machiavelli: Sylvia
* Beverly Adams: Karin
* Marilù Tolo: Gioia
* Seyna Seyn: Wilma Soong
* Oliver McGreevy: Ringo
* Sandro Dori: Omar
* Jack Gwillim: British ambassador
* Andy Ho: Ling
 
* and featuring Renato Terra George Leech
* Hans Thorner: Kruger
* Nerio Bernardi: Papal envoy
* Roland Bartrop
* Michael Audley: Major Davis
* Edith Peters: Maria
* and Terry-Thomas: Lord Aldric / James
 

==Music credits==
*Music and special musical effects by Mario Nascimbene
*Conducted by Roberto Pregadio
*Harmonica soloist John Sebastian
*The song "Kiss the Girls and Make Them Die" 
:is sung by Lydia MacDonald
:Lyrics by Howard Greenfield 

==Background== mod Eurospy Michael Connors private eye, The Roaring Margaret Lee and comedy relief Terry-Thomas, who was given a special "and" billing at the end of the actors credits. 
 Casino Royale.  The plot of Kiss the Girls and Make Them Die is similar to the James Bond film Moonraker (film)|Moonraker, which was released 13 years later.   

==Production== Rolls Royce driven by an Aloysius Parker-type chauffeur played by Terry-Thomas. The movie was filmed on location in Rio de Janeiro and Rome with Dino De Laurentiis spending a lot of money on production. 
 Christ the Redeemer statue in Rio when the local stuntman refused to do it.  Connors said that they were the only film company ever granted permission to film at the landmark.

==Evaluation in film guides==
Leonard Maltins Movie Guide (2014 edition) gives Kiss the Girls and Make Them Die its lowest rating, BOMB, describing it as a " ull spy spoof" and commenting that "a satellite capable of sterilizing the world" is "something Bond, Flint, and Matt Helm wouldnt mind. Awful film." Steven H. Scheuers Movies on TV and Videocassette (1993–1994 edition) had a barely higher opinion, allowing 1½ stars (out of 4) and dispatching it with the final line, " ongue-in-cheek secret-agent stuff doesnt come off: a yawn."

As in Maltin, The Motion Picture Guide assigned its lowest ranking of 1 star (out of 5), proclaiming that " here is little to recommend in this secret-agent spoof" and pointing out that " verything from the sloppy special effects to the irritating music radiates an uncanny cheapness. Dubbed in English."

Two specialized guidebooks arrive at a split vote. Michael Weldons Psychotronic Encyclopedia of Film (1983 edition, page 407) agrees with the denigrators, giving it a plot outline which ends with the words, " he worst Bond imitation known to man. With Terry-Thomas, Margaret Lee, Beverly Adams, Marilu Tolo, Nazi scientists, and Communist Chinese. Filmed in Rio De Janeiro. Connors did Mannix next." On the other hand, John Stanleys Creature Features The Science Fiction, Fantasy, and Horror Movie Guide (2000 edition) decided that " lthough a pale copy of 007s exploits, it has a sparkle to its comedy, gorgeous women in figure-flattering wardrobes, and scenic action set against picturesque Rio." After describing the plot, Stanley concludes with, "Michael Connors walks somnambulistic through his role as super agent Kelly who has minipistols hidden in his clothing and is always eating bananas. A standout is Terry-Thomas as a chauffeur secret agent. Directed by Henry Levin and Dino Maiuri. Dorothy Provine portrays Connors charming, sexy contact."

Another specialized collection, Varietys Complete Science Fiction Reviews (1985), includes the trade publications entire review (bylined Murf.) published in the January 11, 1967 issue (the review is dated "Hollywood, Dec. 27"). Murf.s initial two sentences set the tone: "Undoubtedly, there will be some people who will enjoy "Kiss the Girls And Make Them Die," but fact is that producer Dino De Laurentiis has made a limp, banal spy spoof, inept in all departments. Pace is plodding, dialog pallid, direction pedestrian, acting an embarrassment, and technical composition awkward." The remainder of the review, which consists of six full paragraphs, continues to denigrate the film with such terms as "dullsville", "slapdash", "irritating", "gauche", "terrible", "ennui", "cheap" and "too long". The write-up ends with: " e-editing might help via faster pacing. Film demonstrates the vagaries of the business: producer made "The Bible" and the editor trimmed "A Man For All Seasons."     

The sole British cinema compiler to acknowledge the film, Leslie Halliwell in his 1985 Film and Video Guide 5th edition, gave no stars (Halliwells top rating is 4), dismissing it as a " atchy James Bond spoof."

==Bibliography==
* 

==See also==
* Outline of James Bond
* Eurospy

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at TV Guide (revised form of this 1987 write-up was originally published in The Motion Picture Guide)
*  

 

 
 
 
 
 
 
 
 
 
 
 
 