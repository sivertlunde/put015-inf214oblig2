Waterboys (film)
 
{{Infobox film
| name           = Waterboys
| caption        =
| director       = Shinobu Yaguchi
| producer       = Yoshino Sasaki Daisuke Sekiguchi Akifumi Takuma
| writer         = Shinobu Yaguchi
| starring       = Satoshi Tsumabuki Hiroshi Tamaki Akifumi Miura Koen Kondo Takatoshi Kaneko Kei Tani
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 90 min. Japanese
| country        = Japan
| budget         =
}} 2001 Japanese written and directed by Shinobu Yaguchi, about five boys who start a synchronized swimming team at their high school.  The film stars Satoshi Tsumabuki (Suzuki, the leader of the team), Hiroshi Tamaki (Sato), Akifumi Miura (Ohta), Koen Kondo (Kanazawa), Takatoshi Kaneko (Saotome) and Naoto Takenaka (the dolphin trainer).
 Japan Academy Prize, winning awards for Best Newcomer and Best Music Score.  A spin-off television series entered its third season in 2005.

==Plot==
Suzuki is a high-school student who aspires to become a great swimmer; however, he is the only person in the schools swimming team. Soon, a beautiful new swimming teacher starts work at the high school. Dozens of boys decide to join the swimming team, but when they realize that she teaches synchronized swimming (which is generally a womens sport), all but Suzuki and four other boys drop out.  The teacher soon takes maternity leave, and for some time the team is on hiatus. However, when Suzuki watches a dolphin show he decides to ask the dolphin trainer to be their coach.
  
The dolphin trainer exploits them as free labour and has no intention of training them, and in an attempt to ditch them, leaves all his cash with them to have to practice "rhythm" using Dance Dance Revolution. When he runs out of gas, he returns to get some money and discovers that they are totally synchronized and doing well with the game. He takes them back and they continue to train. Later, while training in the sea, a tourist with a video camera films them, thinking that they are drowning, and the boys appear on TV. Seeing this, the boys who had quit rejoin, and are taught synchronized swimming by the five boys.

Before the festival, the pool is drained when the water is used to fight a fire, but the neighboring girls school allows them to use their pool. The performance turns out to be a great success.

== Cast ==
* Satoshi Tsumabuki - Suzuki
* Hiroshi Tamaki - Sato
* Akifumi Miura - Ohta
* Koen Kondo - Kanazawa
* Takatoshi Kaneko - Saotome
* Aya Hirayama - Shizuko Kiuchi
* Kaori Manabe - Mrs. Sakuma
* Takashi Kawamura - Ikeuchi
* Hiroshi Matsunaga - Mochizuki
* Yuya Nishikawa - Sakamoto
* Katsuyuki Yamazaki - Yama-chan
* Taiyo Sugiura - Taiyo
* Koutarou Tanaka - Koutarou
* Makoto Ishihara - Makoto
 
==Spin-offs==

===TV series=== Water Boys
* Water Boys 2
* Water Boys 2005 Natsu

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 