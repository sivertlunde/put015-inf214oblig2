Escuela para suegras
{{Infobox film
| name           =Escuela para suegras
| image          = 
| image size     =
| caption        =
| director       =  Gilberto Martínez Solares
| producer       =Fernando de Fuentes
| writer         =  María Luisa Algarra (story), Gilberto Martínez Solares
| narrator       =
| starring       =  Germán Valdés, Martha Mijares, Blanca de Castejón 
| music          = Luis Hernández Bretón
| cinematography = Jorge Stahl Jr.
| editing        = Pedro Velázquez	 
| distributor    = 
| released       = 12 February 1958 (Mexico)
| runtime        = 85 min
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} 1958 Mexico|Mexican film. It was produced by 
Fernando de Fuentes.

==Cast==

* 	 Germán Valdés	 ...	(as Tin Tan German Valdes)
* 	 Martha Mijares		
* 	 Blanca de Castejón		
* 	 Óscar Pulido		
* 	 Prudencia Grifell	 ...	(as Prudencia Griffel)
* 	 Julio Monterde		
* 	 Marcelo Chávez	 ...	(as Marcelo)
* 	 Pompín Iglesias		
* 	 Eduardo Alcaraz	 ...	(as Eduardo Arcaraz)
* 	 Óscar Ortiz de Pinedo		
* 	 Joaquín García Vargas	 ...	(as Borolas)
* 	 Jorgito Kreutzmann	 ...	(as Jorge Kreutzman)
* 	 Altia Michel		
* 	 Nacho Contla	 ...	(as Ignacio Contla) Magda Monzón
		

==External links==
*  

 
 
 


 