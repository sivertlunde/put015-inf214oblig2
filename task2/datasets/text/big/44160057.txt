Redemption: The No Kill Revolution in America
{{multiple issues|
 
 
 
}}
 
{{Infobox film
| image = 
| caption = 
| director = Russ Barry
| producer = Bonnie Silva
| writer = Nathan Winograd
| starring = Michael G. Sayers Dorothy McKeon
| music = Sean Hathaway
| cinematography = Sam Patton Jeff Melanson
| editing = 
| studio = 
| distributor = 
| released =  
| runtime = 60 minutes 
| country = United States
| language = English
| budget =
| gross = 
}}Redemption: The No Kill Revolution in America is a film which tells the story of animal sheltering in the United States. The film was written by Nathan Winograd of the No Kill Advocacy Center.

==Plot== cruelty after animal cruelty was prosecuted, and advocating for the citys stray dog population.  Before his death, he said, "I hate to think what will befall this Society when I am gone."  Against his express instructions, the ASPCA agreed to run the dog pound, trading its mission of protecting animals for the role of killing them,  to the point where shelter killing has become the leading cause of death of healthy dogs and cats in the United States. 
 
The film goes on to tell the story of the no kill movement in the United States, as told in Nathan Winograds book Redemption: The Myth of Overpopulation and the No Kill Revolution in America,  and the development of the movement in the eight years since the book was first published.   Redemption is found when clear-cut solutions are provided to stop the killing of animals in shelters. 

==Production==
The film was written by Nathan Winograd, who was also executive producer, along with the No Kill Advocacy Center. The film was produced in partnership with Sagacity Productions,  No Kill Nation, and benefactors Debi Day and Lincoln Day. 
===Cast===
The cast includes: 
*Michael G. Sayers as Henry Bergh
*Dorothy McKeon as the wife of Henry Bergh
*Cliff Blake as Peasant
*Matthias Lupri as Mayor
*Steve Provizer as Carriage Driver
*Jesse Kamien as Judge
*Maeve Power as Maid Nellie
*L. Hastings as Dog Catcher
*Gabriel Soule as Young Thief
*Mark J. Millman as Rail Operator
*Larry Hastings as Criminal
*John W. Watson as Fiery Aristocrat
*Kimberley Miller as Shelter Manager
*Vera Farina as Kitten Adopter
*Lisa Roche as Shelter Assistant
*Richard Zawbroski as Man on Street
*Mozart as Dog
===Music===
Original music was composed and performed by Sean Hathaway. 

==Screenings== San Pedro International Film Festival in October 2014.  Other film festivals and screenings were expected to be added as of October 2014. 
 
==Viewer warning==
The film website advises parental discretion. It also cautions that "While a small number of images may be difficult, they are not gratuitous. We expect people who see it to come away with hope, inspiration, empowerment, and redemption."  

==Trailer==
For some time, a 12 minute trailer was published, but it is no longer available.  A transcript of the original trailer in English and French was prepared by an international no kill organization. 

==Awards==
In October 2014, the film won the Audience award for best film in the San Pedro International Film Festival in San Pedro, California.  

==See also==
*Henry Bergh
*No Kill Equation
*Nathan Winograd
*No Kill Advocacy Center

==External Links==
* 

==References== 2 |refs=
 
 , Redemption, accessed Oct. 19, 2014. 

 
 , Redemption, accessed Oct. 19, 2014. 

 
 , Redemption, accessed Oct. 19, 2014. 

 
 , Redemption, accessed Oct. 19, 2014. 

 
 , No Kill Advocacy Center Facebook Page, Oct. 15, 2014 status update.   

 
 , Nathan J. Winograd, Jan. 15, 2014. 

 
 , No Kill Canada: envers un pays sans euthanasie non essentielle Facebook Page, Sept. 5, 2013. 

 
 , Maria B., yelp.com, Sept. 27, 2014 event. 

 
 , Jody, Bark & Swagger, Aug. 19, 2014. 

 
 , Sagacity Productions, accessed Oct. 19, 2014. 

 
 , Amber Taufen, Denver Westword, July 10, 2014. 
}}