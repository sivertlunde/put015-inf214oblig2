Thalavattam
{{Infobox film
| name           = Thalavattam
| image          = thalavattam1.jpg
| writer         = Priyadarshan Karthika Lizy Lizy
| director       = Priyadarshan
| released       =  
| runtime        = 147 minutes
| country        = India
| language       = Malayalam
| music          = Songs:  
| lyrics         = Poovachal Khader Panthalam Sudhakaran
| distributor    = Seven Arts International LTD 
| studio         = Seven Arts International LTD
| editing        = N Gopalakrishnan
| cinematography = S Kumar
| writer         = Nedumudi Venu Priyadarshan Priyadarshan (dialogues)
| producer       = GP Vijayakumar 
| Background Music = Johnson
| awards         =
| budget         =
}} One Flew Prabu and Saranya playing the lead.

==Plot==
Vinod (Mohanlal) becomes mentally ill after his girlfriend Anitha (Lizy (actress)|Lizy) dies because of an electric short circuiting accident during a rock concert. Vinod is admitted into an institution managed by Dr. Ravindran (M. G. Soman).

With the help of Dr. Savithri (Karthika (1980s actress)|Karthika), who is Dr. Ravindrans daughter, and Dr. Unnikrishnan (Nedumudi Venu), Vinod slowly regains his memory and mental equilibrium. Savithri and Vinod fall in love. Dr. Ravindran has already arranged Savithris marriage with Hari (Mukesh (Malayalam actor)|Mukesh), so he opposes the lovers.

When he finds that Savithri and Vinod are adamant, Dr. Ravindran lobotomises Vinod and puts him in a state of coma. Dr. Unnikrishnan feels that death would be preferable over a vegetative life and kills Vinod. He confronts Dr. Ravindran and confesses to the euthanasia. Savithri overhears the conversation, and loses her mental equilibrium. She is admitted into the same institution as a patient.

==Cast==
*Mohanlal as Vinod
*Nedumudi Venu as Dr. Unni Krishnan
*M. G. Soman as Dr. Ravindran Karthika as Savithri Lizy as Anitha Mukesh as Hari
*Jagathi Sreekumar as Narayanan
*Sukumari - nurse

==Soundtrack==
The music was composed by Raghu Kumar and Rajamani and lyrics was written by Poovachal Khader and Panthalam Sudhakaran.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kalabham Chaarthum || MG Sreekumar || Poovachal Khader ||
|-
| 2 || Konchum nin imbam || K. J. Yesudas, KS Chithra || Panthalam Sudhakaran ||
|-
| 3 || Koottil Ninnum || K. J. Yesudas || Poovachal Khader ||
|-
| 4 || Pon veene || KS Chithra || Poovachal Khader ||
|-
| 5 || Pon Veene || KS Chithra, MG Sreekumar || Poovachal Khader ||
|}

==Reception==
Thalavattam was one of the superhits in the year 1986.

==References==
  

==External links==
 
*  

 

 
 
 
 
 
 

 