Transformers (film)
 
  
{{Infobox film
| name           = Transformers
| image          = Transformers07.jpg
| caption        = Theatrical release poster
| director       = Michael Bay
| producer       = {{plainlist|
* Don Murphy
* Tom DeSanto
* Lorenzo di Bonaventura
* Ian Bryce
}}  
| screenplay     = {{plainlist|
* Roberto Orci
* Alex Kurtzman
}}
| story          = {{plainlist| John Rogers
* Roberto Orci
* Alex Kurtzman
}}
| based on       =  
| starring       = {{plainlist|
* Shia LaBeouf
* Tyrese Gibson
* Josh Duhamel
* Anthony Anderson
* Megan Fox
* Rachael Taylor
* John Turturro
* Jon Voight
}}
| music          = Steve Jablonsky
| cinematography = Mitchell Amundsen
| editing        = {{plainlist|
* Tom Muldoon
* Paul Rubell
* Glen Scantlebury
}}
| studio         = {{plainlist|
* di Bonaventura Pictures
* Hasbro
}}
| distributor    = {{plainlist| DreamWorks Pictures (United States)
* Paramount Pictures (International)
}}
| released       =   
| runtime        = 144 minutes
| country        = United States
| language       = English
| budget         = $150 million   
| gross          = $709.7 million 
}}
 science fiction Megatron respectively.

The film was produced by Don Murphy and Tom DeSanto. They developed the project in 2003, and DeSanto wrote a film treatment|treatment. Steven Spielberg came on board the following year, hiring Roberto Orci and Alex Kurtzman to write the screenplay. The United States Armed Forces|U.S. Armed Forces and General Motors (GM) loaned vehicles and aircraft during filming, which saved money for the production and added realism to the battle scenes.

 ,  , and books, as well as product placement deals with General Motors|GM, Burger King, and eBay.

Transformers received mixed to positive reviews and was a box office success.  It is the forty-fifth  , was released on June 24, 2009. Despite mostly negative reviews, it was a commercial success and grossed more than its predecessor. A third film,  , was released on June 29, 2011, in  , was released on June 27, 2014, which also grossed over $1 billion, though it got mixed to negative reviews. A fifth film as of now titled Transformers 5 is scheduled for a summer 2017 release.  

==Plot==
  AllSpark so Archibald Witwicky accidentally activated Megatrons navigational system and his eyeglasses were imprinted with the coordinates of the AllSparks location, an incident that left him blind and mentally unstable. Sector 7, a secret government organization created by President Herbert Hoover, discovered the AllSpark in the Colorado River and built the Hoover Dam around it to mask its energy emissions. The still-frozen Megatron was moved into this facility and was used to advance human technology through reverse engineering.
 Blackout attacks William Lennox Sam Witwicky Mikaela Banes. Later, Sam catches a glimpse of Bumblebees true form when he signals the other Autobots.
 Frenzy infiltrates the plane and tries to hack into the network again, only this time is more successful until he is stopped by the U.S. intelligence operatives before he can retrieve the file information. Frenzy is then picked up by his partner Barricade (Transformers)|Barricade, and they pursue Sam after discovering he has the glasses with the AllSparks coordinates. Witwicky is rescued by Bumblebee, and Mikaela also learns of the Transformers existence. Bumblebee fights Barricade and manages to subdue him while Sam and Mikaela decapitate Frenzy although he still survives and disguises himself as Mikaelas phone.

Meanwhile, Scorponok, who was sent by Blackout, fights Captain Lennox and his team, killing Donnelly. and injuring Figueroa. During the battle, Scorponok is forced to retreat when he gets injured by sabot rounds fired on him by the U.S. Air Force. Lennox and his team then return to the United States and report their findings on the Decepticons to the Pentagon.
 Seymour Simmons Maggie Madsen Glen Whitmann, two hackers who were captured by the Federal Bureau of Investigation for trying to decipher the information Frenzy stole while working in the Pentagon.

Frenzy, who was transported to the dam while in disguise, finds the AllSpark and contacts the other Decepticons, Starscream, Bonecrusher (Transformers)|Bonecrusher, Brawl (Transformers)|Brawl, Barricade, and Blackout. Starscream attacks the dam and Frenzy frees Megatron from his frozen prison, where he joins his cohorts into pursuing Sam and the Autobots. Bumblebee shrinks the cube to a reasonable size so they can escape with it. They then arrive at Mission City, where a large battle ensues. Working together, the Autobots and human soldiers defeat and kill Bonecrusher, Brawl, and Blackout. However, Bumblebee is crippled, and Jazz is killed by Megatron. When Megatron begins to gain the upper hand in the fight, Optimus urges Sam to put the AllSpark in his chest, which will destroy them both. Instead, Witwicky inserts the cube into Megatrons chest, which kills him and destroys the AllSpark.

In the aftermath, the dead Transformer bodies are dumped into the  .

==Cast==
 

===Humans===
  Sam Witwicky, a young descendant of an Arctic explorer who stumbled on a big secret which becomes the last hope for Earth. Mikaela Banes, a classmate of Sam who assists Sam in his mission by using skills she learned as a juvenile car thief. William Lennox, the captain of a Special Operations team based at the Special Operations Command Central|U.S. SOCCENT base in Qatar. Combat Controller Robert Epps, a member of Captain Lennoxs team. Maggie Madsen, a Pentagon analyst recruited by the United States Department of Defense|U.S. Defense Department. Glen Whitmann, a hacker friend of Maggie. John Keller, the United States Secretary of Defense|U.S. Secretary of Defense. Seymour Simmons, a member of Sector 7 Advanced Research Division. Michael ONeill Tom Banachek, head of Sector 7. Ron Witwicky, Sams father. Judy Witwicky, Sams mother. Jorge "Fig" Figueroa, a Special Operations soldier who survives the destruction of the SOCCENT base in Qatar and was also a member of Captain Lennoxs team.
* Zack Ward as First Sergeant List of Transformers film series characters#Fig and Donnelly|Donnelly, a member of Captain Lennoxs team. Archibald Witwicky, Sams great-great-grandfather who accidentally activates Megatrons navigational system.
* Bernie Mac as Bobby Bolivia, a used cars salesman. John Robinson as Miles Lancaster, Sams best friend.
* Travis Van Winkle as Trent, Mikaelas boyfriend.
* Glenn Morshower as Colonel Sharp (credited as "SOCCENT sergeant")

===Transformers===
  used to portray Optimus Prime]]
  used to portray Bumblebee]]
  used to portray Jazz]]
  used to portray Ironhide]]

====Autobots====
* Peter Cullen as Optimus Prime, the leader of the Autobots. Peter Cullen had previously voiced Optimus Prime in the original 1980s cartoon and was chosen to reprise his role, which was warmly welcomed by audiences and considered one of the films best aspects. Mark Ryan as Bumblebee (Transformers)|Bumblebee, the young scout of the Autobots and best friend of Sam.
* Darius McCrary as Jazz (Transformers)|Jazz, the second-in-command to Optimus Prime.
* Robert Foxworth as Ratchet (Transformers)|Ratchet, the Autobots medic.
* Jess Harnell as Ironhide, the Autobots weapons expert.

====Decepticons====
* Hugo Weaving as Megatron (Transformers)|Megatron, the leader of the Decepticons. Originally Frank Welker (voice of Megatron in the original series) was considered but according to DVD commentary, Bay thought his voice didnt fit, so Weaving was chosen instead.
* Jim Wood as Bonecrusher (Transformers)|Bonecrusher, the rampaging mine sweeper of the Decepticons.
* Reno Wilson as Frenzy (Transformers)|Frenzy, a hacker for the Decepticons.
* Charlie Adler as Starscream (Transformers)|Starscream, Megatrons second-in-command. Adler had previously voiced several characters in the original series, most noticeably Silverbolt.
* Jess Harnell as Barricade (Transformers)|Barricade, a Decepticon who transforms into a police car.
* Blackout (Transformers)|Blackout, Megatrons third-in-command.
* Brawl (Transformers)|Brawl, a vicious Decepticon demolition specialist who transforms into an army tank.

==Production==

===Development===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"In all the years of movie-making, I dont think the image of a truck transforming into a twenty-foot tall robot has ever been captured on screen. I also want to make a film thats a homage to 1980s movies and gets back to the sense of wonder that Hollywood has lost over the years. It will have those Spielberg-ian moments where you have the push-in on the wide-eyed kid and you feel like youre ten years old even if youre thirty-five."
|-
|style="text-align: left;"| — Tom DeSanto on why he produced the film 
|} invasion of Transformers franchise Creation Matrix film series treatment from a human point of view to engage the audience,  while Murphy wanted it to have a realistic tone, reminiscent of a disaster film.  The treatment featured the Autobots Optimus Prime, Ironhide, Jazz (Transformers)|Jazz, Prowl (Transformers)|Prowl, Arcee, Ratchet (Transformers)|Ratchet, Wheeljack, and Bumblebee (Transformers)|Bumblebee, and the Decepticons Megatron (Transformers)|Megatron, Starscream (Transformers)|Starscream, Soundwave (Transformers)|Soundwave, Ravage (Transformers)|Ravage, Laserbeak, Rumble (Transformers)|Rumble, Skywarp and Shockwave (Transformers)|Shockwave. 

 . 

Michael Bay was asked to direct by Spielberg on July 30, 2005,    but he dismissed the film as a "stupid toy movie".    Nonetheless, he wanted to work with Spielberg, and gained a new respect for the concept upon visiting Hasbro.  Bay considered the first draft "too kiddie", so he increased the militarys role in the story.     The writers sought inspiration from G.I. Joe for the soldier characters, being careful not to mix the brands.    Bay based Lennox struggle to get to the Pentagon phoneline while struggling with an unhelpful operator from a real account he was given by a soldier when working on another film. 
 female Transformer introduced by Orci and Kurtzman, but she was cut because they found it difficult to explain robotic gender; Bay also disliked her motorcycle form, which he found too small.  An early idea to have the Decepticons simultaneously strike multiple places around the world was also dropped.   

===Design===
 s robotic body within his truck mode is seen here.]]
The filmmakers created the size of each robot with the size of their vehicle mode in mind, supporting the Transformers rationale for their choice of disguise on Earth. Their War, 2007 DVD documentary  The concept of traveling   Transformers.    Another major influence in the designs was samurai armor, returning full-circle to the Japanese origins of the toy line.  The robots also had to look alien, or else they would have resembled other cinematic robots made in the image of man. 

A product placement deal with General Motors supplied alternate forms for most of the Autobots, which saved $3 million for the production.  GM also provided nearly two hundred cars, destined for destruction in the climactic battle scene.  The  s, F-117s, and V-22 Ospreys, the first time these aircraft were used for a film; soldiers served as extras, and authentic uniforms were provided for the actors.  A-10 Thunderbolt IIs and Lockheed AC-130s also appear. Captain Christian Hodge joked that he had to explain to his superiors that the filmmakers wanted to portray most of their aircraft as evil Decepticons: however, he remarked "people love bad guys". 

===Filming===
  filming at Holloman Air Force Base]] sweep was broken down AWACS aircraft, who improvised dialogue as if it were an actual battle.  
 Megatron is Los Angeles, Universal Studios backlot and at Detroit, Michigan|Detroits Michigan Central Station.   The crew was allowed to shoot at Griffith Observatory, which was still closed for renovations begun in 2002.  Filming wrapped on October 4, 2006. 
 Pearl Harbor. 

===Effects=== Kup jumps on Blitzwing. 
{|class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"I just didnt want to make the boxy characters. Its boring and it would look fake. By adding more doo-dads and stuff on the robots, more car parts, you can just make it more real."
|-
|style="text-align: left;"| — Michael Bay on the level of detail he wanted for the robots 
|}
ILM created computer-generated transformations during six months in 2005, looking at every inch of the car models.  Initially the transformations were made to follow the laws of physics, but it did not look exciting enough and was changed to be more fluid.  Bay rejected a liquid metal surface for the characters faces, instead going for a "Rubiks Cube" style of modeling.  He wanted numerous mechanical pieces visible so the robots would look more interesting, realistic, dynamic and quick, rather than like lumbering beasts.     One such decision was to have the wheels stay on the ground for as long as possible, allowing the robots to cruise around as they changed.    Bay instructed the animators to observe footage of two martial artists and numerous martial arts films to make the fights look graceful. 
 ray tracing was the key to making the robots look real; the CG models would look realistic based on how much of the environment was reflecting on their bodies.  Numerous simulations were programmed into the robots, so the animators could focus on animating the particular areas needed for a convincing performance.   

===Music===
  The Island, scored music for the trailers before work began on the film itself. Recording took place in April 2007, at the Sony Scoring Stage in Culver City, California. The score, including the teaser music, uses six major themes across ninety minutes of music.  The Autobots have three themes, one named "Optimus" to represent the wisdom and compassion of the Autobot leader, and another played during their arrival on Earth. The Decepticons have a chanted theme which relies on electronics, unlike most of the score. The AllSpark also has its own theme.  Hans Zimmer, Jablonskys mentor, also helped to compose the score. 

==Marketing==
  Starscream were released in the United States on May 1, 2007, and the first wave of figures was released on June 2.  The line featured characters not in the film, including Arcee.  A second wave, titled "AllSpark Power", was set for release late 2007, which consisted of repaints and robotic versions of ordinary vehicles in the film.  The toys feature "Automorph Technology", where moving parts of the toy allow other parts to shift automatically.  Merchandise for the film earned Hasbro $480 million in 2007. 

Deals were made with 200 companies to promote the film in 70 countries.  Michael Bay directed tie-in commercials for General Motors, Panasonic, Burger King and PepsiCo,  while props&nbsp;– including the Camaro used for Bumblebee and the AllSpark&nbsp;– were put up for charity sale on eBay.  A viral marketing alternate reality game was employed through the Sector 7 website, which presented the film and all previous Transformers toys and media as part of a cover-up operation called "Hungry Dragon," perpetrated by a "real life" Sector 7 to hide the existence of genuine Transformers. The site featured several videos presenting "evidence" of Transformers on Earth, including a cameo from the original Bumblebee. 

==Release and reception==
Transformers had its worldwide premiere at  .  The film was released in IMAX on September 21, 2007,  with additional footage that had not been included in the general theatrical release. 

===General===
{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:28em; max-width: 40%;" cellspacing="5"
|style="text-align: left;"|"From the king movie geek Harry Knowles of AintItCool.com to newspaper film critics and regular Joe (and Jane) comments, there is general raving about the mechanical heroes and general grumbling about the excessive screen time given to some of the human characters played by Shia LaBeouf, Anthony Anderson, Tyrese Gibson and Jon Voight. Optimus Prime, the leader of the good-guy Autobots, doesnt appear until midway through the film."
|-
|style="text-align: left;"| — USA Today 
|}
Transformers fans were initially divided over the film due to the radical redesigns of many characters, although the casting of Peter Cullen was warmly received.  Transformers comic book writer Simon Furman and Beast Wars script consultant Benson Yee both considered the film to be spectacular fun, although Furman also argued that there were too many human storylines.  Yee felt that being the first in a series, the film had to establish much of the fictional universe and therefore did not have time to focus on the Decepticons. 
 Knight Rider Buffalos – the vehicle used for Bonecrusher – after various Transformer characters. 

After the films 2009 sequel was titled  , Roberto Orci was asked if this film would be retitled, just as Star Wars was titled   when re-released. He doubted the possibility, but said if it was retitled, he would call it Transformers: More Than Meets the Eye. 

===Critical reception=== rating average The Advertiser ABC presenter Margaret Pomeranz was surprised "that a complete newcomer to the Transformers phenomenon like myself became involved in the fate of these mega-machines".  Aint It Cool Newss Drew McWeeny felt most of the cast grounded the story, and that "it has a real sense of wonder, one of the things thats missing from so much of the big CGI lightshows released these days".  Author Peter David found it ludicrous fun, and said that "  manages to hold on to his audiences suspension of disbelief long enough for us to segue into some truly spectacular battle scenes".  Roger Ebert gave the film a positive review, giving it 3 stars out of four. 
 Daily Herald  Matt Arado was annoyed that "the Transformers   little more than supporting players", and felt the middle act was sluggish.  CNNs Tom Charity questioned the idea of a film based on a toy, and felt it would "buzz its youthful demographic   but leave the rest of us wondering if Hollywood could possibly aim lower". 

===Box office=== Harry Potter and the Order of the Phoenix, Spider-Man 3, and Shrek the Third.  The film was released in ten international markets on June 28, 2007, including Australia, New Zealand, Singapore and the Philippines. Transformers made $29.5 million in its first weekend, topping the box office in ten countries.  It grossed $5.2 million in Malaysia, becoming the most successful film in the countrys history.  Transformers opened in China on July 11, and became the second highest-grossing foreign film in the country (behind Titanic (1997 film)|Titanic), making $37.3 million.  Its opening there set a record for a foreign language film, making $3 million.  The film was officially released in the United Kingdom on July 27, making £8.7 million, and helped contribute to the biggest attendance record ever for that weekend. It was second at the UK box office, behind The Simpsons Movie.  In South Korea, Transformers recorded the largest audience for a foreign film in 2007, and recorded highest foreign revenue of the movie. 
 The Amazing first weekend, amounting to a $155.4 million opening week, giving it the record for the biggest opening week for a non-sequel.  The openings gross in the United States was 50 percent more than Paramount Pictures expected. One executive attributed it to word of mouth that explained to parents that "it   OK to take the kids". A CinemaScore poll indicated the film was most popular with children and parents, including older women, and attracted many African American and Latino viewers.  Transformers ended its theatrical run in the United States and Canada with a gross of $319.2 million, making it the third highest-grossing film of 2007 in these regions behind Spider-Man 3 and Shrek the Third. 

===Accolades=== best movie". Sound Editing, Sound Mixing Kevin OConnell, Visual Effects The Bourne The Golden Alvin and the Chipmunks.  The film received a Jury Merit Award for Best Special Effects in the 2007 Kuala Lumpur International Film Festival.  Visual effects supervisor Scott Farrar was honored at the Hollywood Film Festival and Hollywood Awards Gala Ceremony on October 22, 2007 for his work on the film. 

In 2008, the   and  ,  ) at the 28th Golden Raspberry Awards. 

===Home media=== Mark Ryan, Target copy was packaged with a transforming Optimus Prime DVD case and a prequel comic book about the Decepticons.  The DVD sold 8.3 million copies in its first week, making it the fastest-selling DVD of 2007, in North America, and it sold 190,000 copies on HD DVD, which was the biggest debut on the format.  The DVDs sold 13.74 million copies, making the film the most popular DVD title of 2007. 

It was released on  . The content includes three exclusive clips from Revenge of the Fallen, behind-the-scenes footage from both films, and never-before-seen deleted scenes from the first film.  As of July 2012, in North America, the DVD of the film has sold 16.23 million copies, earning $292,144,274. 

==Sequels==
Main articles:  ,  ,  , and Transformers 5

The second film, Revenge of the Fallen was released June 24, 2009. The third film, Dark of the Moon was released June 29, 2011. The fourth film, Age of Extinction was released June 27, 2014. And the fifth as of now titled Transformers 5 is scheduled for a summer 2017 release.

== See also ==
 

==References==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
; Concept art
*  
*  

{{navboxes
|title=Transformers navigation boxes
|list1= 
 
 
 
 
}}
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 