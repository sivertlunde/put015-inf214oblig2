West 32nd
 
{{Infobox film
| name           = West 32nd
| image          = West 32nd film poster.jpg
| caption        = US theatrical poster Michael Kang
| producer       = Teddy Zee
| writer         = Michael Kang Edmund Lee Grace Park Nathan Larson
| cinematography = Simon Coull
| editing        = David Leonard
| distributor    = CJ Entertainment
| released       =  
| runtime        = 86 minutes
| country        = United States South Korea
| language       = English Korean USD 2.5 million
| film name = {{Film name
 | hangul         = 웨스트 32번가
 | rr             = Weseuteu 32beonga
 | mr             = Wesŭt‘ŭ 32bŏnga}}
}} 2007 film Michael Kang. Grace Park, Jane Kim and Jeong Jun-ho.
The film revolves around John Kim, an ambitious young lawyer in New York, who takes on a pro-bono case involving a fourteen-year-old Korean boy accused of murder. Upon investigating the case, he meets Mike Juhn, a street level Mobster from Flushing. The two try to use each other to climb their respective ladders.

==Plot==
The film starts by showing the city streets of K-Town in New York, then goes straight into one of the most common places in Korean Culture. A Room salon (RS) where Korean Mobster Jin Ho Chun (Jun-ho Jeong) is running the place as a money launderer for the Korean mafia. As Chun is making his usual errands, such as making sure the Room salon is running smoothly and greeting customers. Chun goes and checks on the VIP members, where also his mistress Sook Hee (Jane Kim) is at. Chun then grabs the bag full of cash and makes his way into his car. While Chuns driving off, another car blocks his way and he gets shot to death in the back.

An ambitious young lawyer is introduced, John Kim (John Cho) who has just taken on a case that could push him up the ladder to becoming partner at his law firm. The Case revolves around a young teenager, misfit youth Kevin Lee who is accused of murdering Mobster Chun. Kim takes on the case pro-bono for the Lee family. Kim is discussing the case with Kevins older sister Lila Lee (Grace Park), who is still in shock over her brothers case.

Kim assures Lee that they will win the case and theres nothing to worry about. During investigating the case Kim comes upon a name, Mike Juhn (Jun-seoung Kim) by a low level street thug Danny who on occasion plays basketball with Kim and runs with Juhns Crew. Kim sees this as an opportunity to learn more about the case and approaches Juhn. Juhn who has now taken Chuns old position in running the Room salon meets Kim. Juhn is hesitant at first when approached by Kim but then starts opening up to him, when he sees Kim as a way for him to become a legitimate business man. The Two men start using each other to get to the top of their profession. Juhn takes Kim to the Room salon hes running to show Kim the Korean culture. At the room salon Juhns hyung (older brother AKA Boss). Detective Park approaches him and asks him who the outsider is. Juhn lies but Det. Park catches him in it and lectures Juhn about bringing outsiders. Juhn disobeys, saying that Kim could be good for the organization. For this, Juhn is demoted back to is old position.

While Juhns being demoted, Kim meets Hee, the mistress of the recently deceased Chun. Kim has a light conversation with Hee before being interrupted by Det. Park who tells Kim that he doesnt belong here and that Kim should leave. Juhn goes down to a restaurant known as the Korean Mobs headquarters for business. Where he meets with the street bosses for his reckless behavior, one of the bosses tells Juhn that another mobster Kyuc will be taking over and that Juhn is not to set foot into the Room salon until they give the okay. The angry Juhn disobeys orders and brings Kim to the Room salon again where Kim meets Hee for a second time and is able to give Hee his business card before getting kicked out. After the two are kicked out Kim And Juhn go to a bar where Juhn starts talking about old times and how he knows Lee from back in the day when they both went to high school together. Kim then starts pressing Juhn to talk about the case. Juhn finally gives in and talks about the murder telling Kim that Chun was a snake. And that anyone would have wanted him dead hinting that Kevin might have not been involved in the murder but the organization.

While Juhn is having problems of his own Kim is in the same situation. Whereas Kims Bosses at his law firm are telling him to cut a deal with the District Attorneys office saying that they dont want to pursue the case in fear of losing. Kim tells Lee about the situation and Lee disagrees saying that theres something more they have to do. Kim then sets a meeting with Lee and Hee. Hee, who cannot speak a word of English and Kim who cannot speak a word of Korean, needs Lee to translate for them, Kim starts the recorder and asks Lee to have Hee begin. During the conversation Hee starts to reveal shocking facts about the murder. Such as how she saw a young boy come out of Chuns car after the shooting. Hee then reveals that she also saw Juhn at the night of the murder. Lee who is shocked from hearing all the details finally realizes that her brother was the one who committed the murder. She then keeps the information about her brother being there from Kim telling him that it was Juhn and only Juhn who was there at the night of the murder. Lee who now is distraught calls Juhn feeling she has nowhere else to go.

Saeng, Juhns right hand man and Danny are outside a restaurant with other members of Juhns crew. Saeng then gets a call from Juhn saying its time. Saeng gathers all the men and heads with Juhn to the Room salon Juhn used to run and raid the place. Having a big shootout with Kyuc and his men leading to Kyuc and Det Parks demise and Dannys leg being shot. Juhn then tells Saeng to get the money and says that hes going to handle Hee. When searching for Hee she was nowhere to be found, Juhn and Saeng then take the injured Danny to there headquarters. Leaving Saeng to take care of Danny while Juhn goes to find Hee, Juhn goes to Hees house and finds her packing her clothes Juhn rapes her and beats her to death.

While at the headquarters the scared Danny calls Kim and tells him about the situation. Kim quickly rushes down to where Danny is and pushes Saeng out of the way and calls the cops and advises Danny to dont tell the cops anything. Then Kim realizes Hee might be in danger to and rushes to her house once he gets there Kim finds Hees dead body. After along day Kim retreats to his home and is ambushed by Juhns men. Where Juhn starts to reveal more about the murder to Kim how he ordered Kevin to murder Chun saying that Kevin wanted to prove his loyalty and how Lee called him after the conversation Kim had with Lee and Hee. After the confession Juhn pulls out his gun and gets ready to murder Kim, Kim tells Juhn there will be consequences if Juhn pulls the trigger. Kim tells Juhn that he has the recording of the conversation he had with Lee and Hee and theres away for the recording to find its way into the polices hands if Juhn were to pull the trigger. Juhn thinks about the consequences and gives Kim a pass telling Kim to stay out of Flushing or there will be no more passes. Kim then quickly sets a meeting up with Lee and confronts her about the situation. Lee tells Kim she had no choice and that Kim would have done the same thing if it was his brother.

Kim sets another meeting up but with Juhn at a local bar that Kims a regular at once the meeting started Kim asked Juhn for a favor. Juhn refused until Kim convinces him otherwise. Revealing to Juhn that Danny a member of Juhns crew is now Kims client and that he can make Danny go either way. Juhn now realizes the situation and agrees to help Kim. Kim tells Juhn that he doesnt care now, whether Kevin murdered Chun or not he just wants to win the case and tells Juhn to find someone who can be a witness that doesnt put Kevinat the scene of the crime. Juhn agrees, Kim then gives Juhn the recorder with the recording on it. Kim is seen in a new office talking to friend about making partner, Lee walks in with a box of tangerines to thank Kim for getting Kevin out of prison After Lee leaves Kim throws the box in the trash. Juhn is now running the Room salon again. Juhn comes to pick up the bag of money as hes walking to his car his cellphone rings he picks up but theres no answer he hangs up and gets into his car and adjust the rear view mirror and starts to glare at the mirror and the screen goes black.

==Cast==
* John Cho as John Kim, an ambitious lawyer who took on the murder case of Lila Lees Brother Kevin
* Jun-seong Kim (credited as Jun Sung Kim) as Mike Juhn, a mobster in the Korean mob whos willing to do anything to get on top Grace Park as Lila Lee, the older sister of Kevin Lee
* Jeong Jun-ho as Chun Jin-ho, a mobster in the Korean Mob who was murdered
* Jane Kim as Kim Sook-hee, a prostitute mistress of Jin-ho Chun
* Lanny Joon as Saeng, Juhns right hand man
* Hans Kim as Kyuc, the Korean mobster who takes over for Juhn at the Room salon after Juhn is demoted
* Dante Han (Heejun Han) as Danny, a Low level street thug who runs with Juhns crew
* Chil Kong as Detective Park, a dirty cop whos on the payroll of the Korean mob and also Juhns hyung (older brother AKA boss)

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 