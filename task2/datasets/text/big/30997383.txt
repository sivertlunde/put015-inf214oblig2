Nadakame Ulakam
{{Infobox film
| name           = Nadakame Ulakam
| image          = Nadakame Ulakam.jpg
| image size     = 
| alt            = 
| caption        = Film poster
| director       = Viji Thampi
| producer       = M. K. Raveendran M. K. Suresh
| writer         = Sajan Cholayil Saseendran Vadakara ( dialogues )
| narrator       =  Mukesh Vinu Sarayu Saranya Mohan
| music          = Songs:  
| cinematography =  Manoj
| studio         = Sooryakanthi Creations
| distributor    = Kalpaka Films
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Johnson with lyrics penned by Kaithapram Damodaran Namboothiri. The film released on 25 February.

==Plot==
Omanakuttan (Mukesh), is a Bank Manager by profession. But he is very interested stage plays and he himself has directed and acted in many plays. He is approached by a film crew from town headed by Pavan (Suraj Venjaramoodu), to produce their debut venture. Omanakuttan who is a cinema aspirant, agrees to finance the film after he was offered the lead role. Eventually, he learns that neither Pavanan nor his associates know anything about film making. Omanakuttan decides himself to script and direct the film. He completes the film successfully with a limited cast but fails to find a distributor. To find a distributor, Omanakuttan goes to Chennai but fate had something else planned for him there. His bag gets stolen and while chasing the thief, he gets hit by a car. Fortunately the car was that of an accomplished film producer and distributor (Siddhiq). After hearing the story of Omanakuttan, he decides to distribute the film, which became a super hit in the box office.

==Cast== Mukesh as Omanakuttan
* Vinu Mohan as Murali
* Saranya Mohan as Nandana Sarayu as Usha
* Saranya Mohan as Nandana
* Jagadeesh as Pappan, Omanakuttans friend
* Suraj Venjaramoodu as Pavanan
* Jagathy Sreekumar as Labham Lambodaran, Omanakuttans father-in-law
* Salim Kumar as Omanakuttans brother-in-law
* Sona Nair as Remani, Omanakuttans sister Janardhanan as Aanni Kurup Siddique as Producer Ram Mohan
* Dharmajan as Sundaran
* Indrans as Dasettan
* Babu Namboothiri
* Chali Pala
*Nandhu
* Bindu Panicker as Milma Girija, Nandanas mother
* K. P. A. C. Lalitha

==References==
*  

==External links==
*  
*  
*   at Oneindia.in

 
 
 
 

 