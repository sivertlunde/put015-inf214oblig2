The Silver King (film)
{{Infobox film
| name           = The Silver King
| image          =
| caption        =
| director       = T. Hayes Hunter
| producer       =
| writer         = Henry Herman Fenn Sherie Henry Arthur Jones
| starring       = Percy Marmont Harold Huth Jean Jay Chili Bouchier
| cinematography = Bernard Knowles
| editing        =
| studio         = Welsh-Pearson-Elder Paramount British
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| language       = Silent English intertitles
}} 1929 British silent drama The Silver King by Henry Arthur Jones. It was made at Cricklewood Studios and Lime Grove Studios.  

==Plot==
Wilfred Denver (Marmont) wins the heart of the beautiful Nellie (Jean Jay) and marries her, earning the ongoing festering resentment of Nellies former beau Geoffrey Ware (Huth).  A few years later, Wilfred and Nellie have a daughter, but Wilfreds financial recklessness has left him facing large debts.  Geoffrey sees an opportunity for revenge by giving his desperate former friend a surefire insiders tip on a horse running at generous odds in that years Epsom Derby.  Wilfred lays a large bet which he can ill afford, only to watch in dismay as the horse straggles home at the back of the field.  Geoffrey feigns an apology, then urges Wilfred to drown his sorrows.  He gets Wilfred completely befuddled and dishevelled with drink, then takes him home to Nellie, gloating to her about the dissolute wretch she chose over him.

As Geoffrey makes his way home later that evening, he is robbed and murdered by a band of crooks.  The police learn of the events of the day and interview Wilfred, who is unable to provide a satisfactory account of his movements that evening.  Realising that he faces arrest, trial and possible execution, Wilfred flees to America, leaving Nellie behind to fend for herself and their child as best she can.

Years pass, and Wilfred hits the jackpot in America with a silver mine.  Now a rich man known to all as the "Silver King", he returns to England incognito.  He sets about investigating the circumstances of Geoffreys murder, and identifies the guilty individuals.  He takes his evidence to the police, who exonerate him of all charges.  He then engineers a reconciliation with Nellie and their now grown daughter Olive (Chili Bouchier).

==Cast==
* Percy Marmont as Wilfred Denver
* Harold Huth as Geoffrey Ware
* Jean Jay as Nellie Denver
* Chili Bouchier as Olive
* Bernard Nedell as Spider Skinner
* Hugh E. Wright as Jaikes
* Henry Wenman as Cripps
* Ben Field as Coombes
* Donald Stuart as Corkitt

== External links ==
*  
*   at BFI Film & TV Database

 
 
 
 
 
 
 
 
 
 
 

 