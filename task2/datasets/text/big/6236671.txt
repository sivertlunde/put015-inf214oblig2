Meridian: Kiss of the Beast
 
 
{{Infobox film
| name           = Meridian: Kiss Of The Beast
| image          = Meridian Kiss of the Beast.jpg
| image_size     =
| caption        =
| director       = Charles Band
| producer       = Charles Band Debra Dion
| writer         = Charles Band (story) Dennis Paoli
| narrator       =
| starring       = Sherilyn Fenn Malcolm Jamieson Charlie Spradling Hilary Mason
| music          = Pino Donaggio
| cinematography = Mac Ahlberg
| editing        = Ted Nicolaou
| distributor    = Full Moon Features
| released       =  
| runtime        = 85 mins
| country        = United States English Italian Italian
| budget         =
}} 1990 horror film and romance film produced and directed by Charles Band. It starred Sherilyn Fenn, Malcolm Jamieson, Hilary Mason and Charlie Spradling.

==Plot summary==
 
Catherine Bomarzini (Sherilyn Fenn), travels back to her family castle in Italy after her fathers death. Overwhelmed with excitement, Catherine invites her best friend Gina (Charlie Spradling) to spend the weekend. Gina and Catherine discover a local carnival outside the castle gates. Curious they wander to the carnival to enjoy the show and acts. Pleased with the performance, Gina invites the head magician Lawerence (Malcolm Jamieson) and his crew to the castle for dinner. Drugged and seduced, Catherine finds herself drawn into a mysterious love triangle with the handsome magician and a creature of the night whose gentle eyes and touch reveal his infinite love.
Is this creature real or an illusion? Guided by the ghost of a slain ancestor and the advice of the castle caretaker Martha (Hilary Mason), Catherine discovers the ancient curse that enshrouds the Bomarzini Castle... a curse that only she can dispel.

==Cast==
* Sherilyn Fenn (Catherine Bomarzini)
* Malcolm Jamieson (Lawrence/Oliver)
* Hilary Mason (Martha)
* Charlie Spradling (Gina)
* Alex Daniels (Beast)
* Phil Fondacaro (Circus Dwarf)
* Vernon Dobtcheff (Village Priest)
* Isabella Celani (Castle Ghost)
==DVD release==
  Crash and Burn, Doctor Mordrid and Head of the Family.

It was released on DVD again on November 9, 2010 by Echo Bridge Home Entertainment, as a double feature with Decadent Evil.
==External links==
* 
*  

 

 
 
 
 
 
 
 


 
 