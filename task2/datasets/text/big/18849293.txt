Saalivaahanan
{{Infobox film
| name = Saalivaahanan
| image = Salivahanan.jpg
| director = B. N. Rao
| writer = B. S. Ramaiah (story) Kambadasan (dialogue) Ranjan T. R. Rajakumari M. G. Ramachandran K. L. V. Vasantha
| producer = Bhaskar Pictures
| music = 
| distributor = 
| released = 16 February 1945
| runtime = 
| country = India Tamil
| budget = 
}}

Saalivaahanan ( ) is a Tamil language film starring Ranjan (actor)|Ranjan, T. R. Rajakumari, M. G. Ramachandran and K. L. V. Vasantha. The film was released in the 1945.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Ranjan || Saalivahanan
|-
| T. R. Rajakumari || Princess Chandralekha
|-
| M. G. Ramachandran || Vikramaditya
|-
| K. L. V. Vasantha || 
|-
| M. R. Santhanalakshmi || 
|-
| T. S. Balaiah || 
|-
| N. S. Krishnan || 
|-
| T. A. Madhuram ||
|-
| Nagercoil K. Mahadevan || 
|}

==Crew==
* Producer: 
* Production Company: Bhaskar Pictures
* Director: B. N. Rao
* Music: Nagercoïl K.Mahadevan
* Lyrics: 
* Story: B. S. Ramaiah 
* Screenplay: 
* Dialogues: Kambadasan
* Art Direction:
* Editing: 
* Choreography: 
* Cinematography:  
* Stunt:
* Dance:

==Soundtrack==
# Kayirai edutthukko by N. S. Krishnan & T. A. Madhuram

==Reception==
Saalivaahanan did not do well at the box office and the producers lost heavily in the process.

Remembered for the coloured sequence and the comedy of Krishnan-Mathuram.

==References==
http://www.hindu.com/cp/2011/06/26/stories/2011062650391600.htm
 

 
 
 
 


 