L'Alpagueur
{{Infobox film |
  image          = Alpagueurposter.jpg |
  name     = LAlpagueur|
  caption        = Theatrical release poster |
  writer         = Philippe Labro |
  starring       = Jean-Paul Belmondo, Bruno Cremer |
  director       = Philippe Labro |
  cinematography = Jean Penzer |
  distributor    = Cerito film |
  released   = March 25, 1976 |
  runtime        = 110 min. |
  language = French |
gross = 1,533,183 admissions (France) |
  music          = Michel Colombier |
}}

LAlpagueur is a film adaptation of the original scenario from Philippe Labro directed by him and featuring Jean-Paul Belmondo in the title role and Bruno Cremer as LEpervier.
 thriller from the 1970s, it is one of many Belmondos movie where he is playing the title role. Like in Le Magnifique, LIncorrigible or Le Marginal, Belmondo is the real star of the show, playing a solitary character and pursuing bad guys. The score from Michel Colombier is typical from this period, mixing piano, modern rhythms and brass instruments. The film was the 19th highest earning film of the year in France with 1,533,183 admissions. http://www.jpbox-office.com/fichfilm.php?id=8110&affich=france 

== Plot ==
As one of the character is saying at the beginning of the movie:

Lalpagueur cest un chasseur de tête, cest un mercenaire, un marginal. Lalpagueur cest lastuce qua trouvé un haut fonctionnaire pour passer au-dessus de la routine policière.

The alpagueur is a head hunter, a mercenary, a marginal. The alpagueur is a trick made up by a state employee to be above the cops routine.

Originally a deer hunter, lAlpagueur became a head hunter working for the police, paid by them with money stolen from criminals. The main plot revolves around lAlpagueurs pursuit of lÉpervier, (Eurasian Sparrowhawk|Sparrowhawk) a bank robber and an assassin, who kills whoever sees him commit a crime. His technique is to pay a young and naive man to be his accomplice and kill him right after. One of his accomplices, Costa Valdez, is only wounded during one of his hold ups, and with his help, lAlpagueur manages to find lÉpervier at the end.

==Cast==
* Jean-Paul Belmondo - Roger Pilard aka "lAlpagueur"
* Bruno Cremer - Gilbert ala "LEpervier"
* Claude Brosset - Granier
* Patrick Fierry - Costa Valdes
* Jean Negroni - Spitzer
* Victor Garrivier - Inspector Doumecq
* Jean-Pierre Jorris - Salicetti

==References==
 

==External links==
*  

 
 
 
 
 
 

 