Four Shall Die
{{Infobox film
| name           = Four Shall Die
| image          = Four Shall Die 1940 Poster.jpg
| caption        = Theatrical release poster
| director       = William Beaudine Leo C. Popkin
| producer       = 
| writer         = 
| starring       = Niel Webster
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}

Four Shall Die is a 1940 American supernatural crime film directed by William Beaudine.   

==Cast==
* Niel Webster as Pierre Touissant
* Mantan Moreland as Beefus, Touissants Chauffeur
* Laurence Criner as Roger Fielding
* Dorothy Dandridge as Helen Fielding
* Vernon McCalla as Doctor Webb
* Monte Hawley as Dr. Hugh Leonard (as Monty Hawley)
* Reginald Fenderson as Hickson (as Reggie Fenderson) Jack Carr as Lew Covey
* Jess Lee Brooks as Bill Summers Edward Thompson as Sgt. Adams Earle Hall as Jefferson
* Harry Levette as Thomas

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 