Flaming Feather
{{Infobox film
| name           = Flaming Feather
| image          = 
| caption        = 
| director       = Ray Enright
| producer       =
| writer         = 
| starring       = Sterling Hayden Forrest Tucker
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross = $1.25 million (US rentals) 
}} Western film directed by Ray Enright and starring Sterling Hayden.

==Plot==
A mysterious outlaw, known only as The Sidewinder, is terrorizing Arizona settlers. A rancher whose property was raided, Tex McCloud, and a U.S. Cavalry officer named Blaine both decide to seek justice. They even make a friendly wager over which one will get to The Sidewinder first.

A wealthy saloon entertainer, Carolina, tries to persuade Tex to also go after Lucky Lee, a mine owner who owes her $20,000. She also tries to seduce Tex, but hes not interested.

After he changes hotel rooms with Luckys longtime sweetheart, Nora Logan, an ambush is attempted by gambler Showdown Calhoun and his partner, who come to the wrong room. Nora is the one theyre after, and she becomes a kidnap victim on the stagecoach. For the second time, though, Tex rides to her rescue.
 Ute woman who loves Lucky, knows for a fact hes the outlaw.

Now the marshal for the territory, Tex and a posse go after Lucky, who has snatched Nora and ridden off to a hideout. Lucky conspires with a band of Utes to attack the posse. Carolina and Showdown are killed. Tex and Blaine get to the hideout, but the jealous Turquoise has already killed Lucky, beating them to the punch. The men call off their wager.

==Cast==
* Sterling Hayden as Tex McCloud
* Forrest Tucker as Lt. Tom Blaine
* Arleen Whelan as Carolina
* Barbara Rush as Nora Logan
* Victor Jory as Lucky Lee aka Sidewinder
* Richard Arlen as Eddie Showdown Calhoun
* Edgar Buchanan as Sgt. ORourke
* Carol Thurston as Turquoise

==See also==
* Sterling Hayden filmography

==References==
 

==External links==
*  at IMDB

 

 
 
 
 
 
 


 