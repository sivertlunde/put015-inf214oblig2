Intramural (film)
{{Infobox film
| name           = Intramural
| image          =
| border         =
| alt            =
| caption        =
| director       = Andrew Disney
| producer       = Russell Wayne Groves Andrew Lee David Ward Red Sanders Tucker Moore Bradley Jackson
| writer         = Bradley Jackson
| starring       = Jake Lacy Beck Bennett Jay Pharoah Nikki Reed Kate McKinnon
| music          = Alice Wood
| cinematography = Jeffrey Waldron
| editing        = Kody Gibson
| studio         = Ralph Smyth Entertainment Red Productions
| distributor    =
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Nick Kocher, Brian McElhaney, Nick Rutherford and Gabriel Luna, and focuses on a group of college seniors that decide to form an intramural football team before graduating.  The film stars cast members of comedy groups Saturday Night Live, Derrick Comedy, BriTANicK, and Good Neighbor.  The film is planned to be released in 2014. The film premiered in the Midnight section    at the Tribeca Film Festival on April 19, 2014.   

==Synopsis==
Caleb (Jake Lacy) is a fifth-year senior preparing to graduate. Not quite ready to settle into the life, expectations, and realities outside of his college, he decides that he wants to reassemble The Panthers, an intramural football team that had to shut down after one of the team members ended up getting partially paralyzed for life. Doing so is going to be far more difficult than he expected.

==Cast==
*Jake Lacy as Caleb Fuller
*Nikki Reed as Meredith Downs
*Kate McKinnon as Vicky
*Beck Bennett as Dick Downs Nick Kocher as Grant Brian McElhaney as Chance
*Gabriel Luna as Vinnie
*Will Elliott as George
*Kirk C. Johnson as Ace
*Sam Eidson as Jimmy
*Nick Rutherford as Hank
*Jay Pharoah as Dan Albert
*DC Pierson as Bill Costas Michael Hogan as Mr. Albrecht
*Clint Howard as Philip

==Production== Michael Hogan.    Jackson and Disney raised funds for the film through a successful Kickstarter campaign,     and filming began on July 12, 2013 in Austin, Texas and continued for six weeks,  ending on August 22, 2013.   

==Reception==
The film was met with positive reviews from film critics at the Tribeca Film Festival, currently holding a 83% "fresh" rating on Rotten Tomatoes, based on 6 reviews.  It won both the Cinema Dulce Best of Fest and Best Actor (Jake Lacy) awards at the Hill Country Film Festival  and the Best Guilty Pleasure Audience Award at the Seattle International Film Festival. 

==See also==
* Searching for Sonny, the 2011 film directorial debut of Andrew Disney

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 