The Cynic, the Rat and the Fist
{{Infobox film
 | name =The Cynic, the Rat and the Fist 
 | image = The Cynic, the Rat and the Fist.jpg
 | caption =
 | director =  Umberto Lenzi
 | writer =Ernesto Gastaldi  Dardano Sacchetti  Umberto Lenzi John Saxon Tomas Milian
 | music =   Franco Micalizzi
 | cinematography =  Federico Zanni
 | editing =
 | producer =Luciano Martino
 | distributor =
 | released = 1977
 | runtime =
 | awards =
 | country = Italy
 | language = English / Italian
 | budget =
 }} Italian Crime crime action thriller directed in 1977 by Umberto Lenzi. The movie is the sequel to Rome Armed to the Teeth, with Maurizio Merli reprising the role of Commissioner Leonardo Tanzi. 

The title of the movie inspired the book Cinici infami e violenti (2005), written by Daniele Magni and Silvio Giobbio, a book guide about "Poliziotteschi". 

== Plot ==
Luigi Maietto aka "Chinaman" escapes from prison. As soon as he is free he assigns immediately two henchman to murder the inspector whose testimonal once led to his prison sentence. Inspector Tanzi is left for dead but survives. The local newspapers cover up for him and pretend the assassination had succeeded. When Tanzi gets better, his superior wants him to hide in Switzerland. But Tanzi defies him because he intends to make sure himself that Maietto is put back in prison. He goes for it.

== Cast ==
*Maurizio Merli: Leonardo Tanzi
*Tomas Milian: Luigi Er Cinese Maietto John Saxon: Frank Di Maggio
*Renzo Palmer: Commissioner Astalli
*Gabriella Lepori: Nadia
*Robert Hundar: Dario
*Bruno Corazzari: Cesare Ettore
*Marco Guglielmi: Marchetti, Di Maggios lawyer
*Gabriella Giorgelli: Maria Balzano
*Guido Alberti: Tanzis uncle
*Gianni Musy: Nicola Proietti
*Massimo Bonetti: "Cappuccino" Riccardo Garrone: Natali

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 
 