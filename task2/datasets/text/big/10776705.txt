Uphaar
{{Infobox film
| name           = Uphaar
| image          = 
| caption        = 
| director       = Sudhendu Roy
| producer       = Tarachand Barjatya
| writer         =
| based on       =  
| starring       = Jaya Bhaduri Kamini Kaushal
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
| budget         = 
}}
 Best Foreign Language Film at the 45th Academy Awards, but was not accepted as a nominee. 

==Plot==

Anoop studies law in Calcutta, while his widowed mom lives in a small town in West Bengal. He has a sister, Sudha, who is married to Anil and lives in Calcutta. Since Anoop is of marriageable age, his mom has selected a prospective bride for him in her neighbourhood. The girls name is Vidya. When Anoop returns home, his mother asks him for his approval, but he says he wants to see the girl first. He goes to see Vidya, and also gets to meet another village belle by the name of Minoo, the daughter of Sharda and Ramchandra. He returns home, tells his mom that he cannot marry Vidya, and will marry only Minoo. His mother reluctantly agrees and the marriage takes place. It is then that they find out that Minoo has no household skills. Neither is she educated, nor mature enough to understand her relationship with Anoop. Her only interests appear to be stealing mangoes and other fruit, and playing with children much younger than her. Anoops mom is quite exasperated with Minoo and is compelled to keep the new bride under lock and key. When the time comes for Anoop to return to Calcutta, he asks Minoo to come with him, but she refuses.

His mother cannot handle Minoos childishness and refuses to let her stay with her. Anoop accordingly leaves Minoo with her mom, Sharda. Once Anoop leaves her and heads back to Calcutta, Minoo starts realising she misses him. All her earlier activities of fooling around and playing with the village kids loses their charm and, in her loneliness, she realises her love for Anoop. She then tells her mom that she wants to go back to Anoops house, reconcile with her mother-in-law and live with her. Minoo goes back, a changed person. Anoops mom welcomes her daughter-in-law. Minoo excels in her household duties. But Anoop does not visit even for his holidays. Minoo realises that, when she had refused to accompany him to Calcutta, Anoops ego was bruised. He had promised that he would come only when she wrote to him to come back. So she writes a letter to Anoop telling him to come home. But she does not have his address, so Anoop never gets the letter. In the meantime, Anoops mom, realizing that Minoo is  truly missing her husband, suggests a trip to Calcutta to visit him. It is in Anoops sisters house in Calcutta that the love-lorn couple finally comes together.

==Cast==
* Swarup Dutt as Anoop (as Swaroop Dutt)
* Jaya Bhaduri as Minoo a.k.a. Mrinmayee
* Suresh Chatwal as Anil, Sudhas husband
* Nandita Thakur as Sudha
* Nana Palsikar as Ramchandra 
* Ratnamala (actress)| Ratnamala as Sharda 
* Leela Mishra as Kaki
* Kamini Kaushal as Anoops mother
   Yunus Pervez   as Banwari

==See also==
* List of submissions to the 45th Academy Awards for Best Foreign Language Film
* List of Indian submissions for the Academy Award for Best Foreign Language Film

==References==
 

== External links ==
*  
*   on YouTube (Official movie page)
 

 
 
 
 
 
 
 
 
 
 