The Unfrocked One
{{Infobox film
| name           = The Unfrocked One
| image          = 
| image size     = 
| caption        = 
| director       = Léo Joannon
| producer       = Alain Poiré Roger Ribadeau-Dumas
| writer         = Léo Joannon Denys de La Patellière
| starring       = Pierre Fresnay
| music          = 
| cinematography = Nikolai Toporkoff
| editing        = Monique Isnardon
| distributor    = 
| released       = 26 February 1954
| runtime        = 107 minutes
| country        = France
| language       = French
| budget         = 
}}
 French drama film directed by Léo Joannon. At the 4th Berlin International Film Festival it won the Bronze Berlin Bear award.   

==Cast==
* Pierre Fresnay - Maurice Morand
* Pierre Trabaud - Gérard Lacassagne
* Nicole Stéphane - Catherine Grandpré
* Marcelle Géniat - Mme. Morand
* Jacques Fabbri - Lordonnance
* Abel Jacquin - Le père supérieur
* Georges Lannes - Le colonel
* Renaud Mary - Lantiquaire
* Guy Decomble - Le père Mascle
* René Havard - Un officier
* Christian Lude - Le chanoine Jusseaume
* Léo Joannon - Le chanoine Jousseaume
* Olivier Darrieux - Edoard
* Sylvie Février - La soeur de Gérard
* René Blancard - M. Lacassagne

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 