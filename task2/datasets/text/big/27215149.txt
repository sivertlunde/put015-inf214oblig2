London Melody
{{Infobox film
| name =  London Melody
| image =
| image_size =
| caption =
| director = Herbert Wilcox
| producer =Herbert Wilcox  
| writer =  Monckton Hoffe   Florence Tranter
| narrator = Robert Douglas   Horace Hodges Geraldo
| cinematography = Freddie Young 
| editing = Frederick Wilson
| studio = Herbert Wilcox Productions 
| distributor = General Film Distributors
| released = 4 February 1937   
| runtime = 75 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 Robert Douglas. Elstree and Pinewood Studios by Wilcoxs independent production company and distributed by J. Arthur Ranks General Film Distributors.  It was also released with the alternative title Look Out for Love.

==Synopsis== musical with 1937 by Neagles future husband, Herbert Wilcox. This time around, Carminatti is cast as Marius Andreani, a cultured Italian diplomat. While in London on business, Marius makes the chance acquaintance of boisterous cockney street entertainer Jacqueline (Neagle). Its love at first sight, but hero and heroine must undergo a dizzying series of roadblocks and misunderstandings before the climactic clinch. Meanwhile, Jacqueline rises to the top of show-business success, never dreaming (until the end, at least) that its all the secret handiwork of faithful Marius.

==Cast==
* Anna Neagle as Jacqueline
* Tullio Carminati as Marius Andreani Robert Douglas as Nigel Taplow
* Horace Hodges as Father Donnelly
* Grizelda Harvey as Friend of Marius
* Miki Hood as Friend of Marius
* Davina Craig as Maid
* Joan Kemp-Welch as Maid
* Arthur Chesney as Marius Butler
* Leonard Snelling as Organ Grinders Son
* Henry Wolston as Alfred Snodgrass
* A. Bromley Davenport as General Taplow
* Ronald Shiner as the Pickpocket On Trial  Geraldo as Bandleader
* Linden Travers as Woman in Night Club  
* Raymond Huntley as Policeman Outside Nightclub
* Sebastian Smith as Gambler

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927–1939. British Film Institute, 1986.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 