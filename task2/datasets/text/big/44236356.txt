Les Braqueuses
{{Infobox film
| name           = Les Braqueuses
| image          = 
| caption        =
| director       = Jean-Paul Salomé
| producer       = Monique Annaud Gilles Sacuto
| writer         = Laurent Bénégui Joëlle Goron Gérard Mordillat Jean-Paul Salomé Catherine Jacob Clémentine Célarié Alexandra Kazan Nanou Garcia Annie Girardot Bill Baxter Olivier Lanneluc Claude Sitruk	
| cinematography = Patrick Duroux
| editing        = Michèle Robert-Lauliac
| distributor    = Chrysalide Film
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
| gross          = $1,346,917  
}}
Les Braqueuses is a 1994 French comedy crime film, directed by Jean-Paul Salomé.

==Plot==
Four young women from Montélimar, France resort to robbing a sex shop to help make ends meet. Unfortunately, the 1500 franc (€ 230) loot isnt enough to cover their bills. Hoping for a bigger payoff, they carefully plan a bank robbery. Will Cécile, Muriel, Bijou and Lola thwart the police and succeed in making off with the money?

==Cast== Catherine Jacob as Cécile
* Clémentine Célarié as Bijou
* Alexandra Kazan as Muriel
* Nanou Garcia as Lola
* Annie Girardot as Céciles mother
* Jean-Claude Adelin as Xavier
* Jacques Gamblin as Thierry
* Laurent Spielvogel as Monsieur Leroux
* Abbes Zahmani as Monsieur Ted
* Harry Cleven as Max
* Roland Amstutz as Bernachon

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 