Hit the Deck (1930 film)
 
{{Infobox film
| name           = Hit the Deck
| image          = Movie_Poster_for_Hit_the_Deck.jpg
| caption        = Film poster
| director       = Luther Reed Fred Fleck (assistant)
| producer       = William LeBaron
| writer         = Hubert Osborne Herbert Fields Luther Reed
| starring       = Jack Oakie Pollie Walker
| music          = Victor Baravalle (director) Vincent Youmans (music)
| cinematography = Robert Kurrle William Hamilton
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =    | ref2= }}
| runtime        = 93 minutes  
| country        = United States 
| language       = English
| budget         = $542,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56  
| gross = $1.1 million 
}}
 Hit the Deck, which was itself based on the play, Shore Leave by Hubert Osborne. It was one of the most expensive productions of RKO Radio Pictures up to that time, and one of the most expensive productions of 1930. This version faithfully reproduced the stage version of the musical. 

==Cast==
* Jack Oakie as Bilge
* Polly Walker as Looloo Roger Gray as Mat
* Franker Wood as Bat
* Harry Sweet as Bunny
* Marguerita Padula as Lavinia
* June Clyde as Toddy
* Wallace MacDonald as Lieutenant Allen
* George Ovey as Clarence
* Ethel Clayton as Mrs. Payne
* Nate D. Slott as Dan (as Nate Slott)
* Andy Clark as Dinty
* Dell Henderson as Admiral Smith Charles Sullivan as Lieutenant Jim Smith

==Plot==
Looloo (Walker) runs a diner which is frequented with U.S. Navy sailors on shore leave, including officers.  Two officers, Admiral Smith (Henderson) and Lieutenant Allen (MacDonald) accompany a wealthy socialite, Mrs. Payne (Clayton), to the establishment.  

Mrs. Payne is an heiress, and when she engages in conversation with Looloo, she expresses admiration for the necklace Looloo is wearing.  She offers to purchase it for a substantial sum, but it is a family heirloom and Looloo refuses.  Later, two sailors arrive at the diner, Bilge (Oakie) and Clarence (Ovey), looking for Lavinia, Clarences sweetheart who has run away.  Bilge, is smitten with Looloo, and begins to romance her. Opening up to her, he reveals his desire to become the captain of his own ship after he leaves the navy.  Before things go too far, Bilges shipmates drag him back to his ship, which is scheduled to set sail.

Based on her conversation with Bilge, Looloo decides to sell her necklace to Mrs. Payne, in order to get the funds necessary to buy a ship for Bilge.  When Bilges ship docks once again, the two lovers are re-united, and Bilge proposes to Looloo, who happily accepts.  However, when she tells him about the money, and the plans shes made to help him buy his own ship, his pride makes him indignant and he storms off.  However, he later returns and the two agree to marry.

==Songs==
* "Sometimes Im Happy" - words by Irving Caesar, music by Vincent Youmans; performed by Jack Oakie and Polly Walker 
* "Keepin Myself for You" - words by Sidney Clare, music by Vincent Youmans; performed by Jack Oakie and Polly Walker 
* "An Armful of You" - words by Leo Robin and Clifford Grey, music by Vincent Youmans; performed by Marguerita Padula and chorus
* "Hallelujah" - words by Leo Robin and Clifford Grey, music by Vincent Youmans; performed by Marguerita Padula and chorus
* "Harbor of My Heart" - words by Leo Robin and Clifford Grey, music by Vincent Youmans; performed by Jack Oakie and chorus
* "Join the Navy" - words by Leo Robin and Clifford Grey, music by Vincent Youmans; performed by Jack Oakie and chorus
* "Nothing Could Be Sweeter" - words by Leo Robin and Clifford Grey, music by Vincent Youmans; performed by Jack Oakie and Polly Walker

==Reception==
The film made a profit of $145,000.   Mordaunt Hall, of the New York Times, gave the film a lackluster review. 

==Notes== Shore Leave, starring Richard Barthelmess and Dorothy Mackaill.  

Osbornes play would also be remade into another musical version, Follow the Fleet, in 1936, starring Fred Astaire and Ginger Rogers.  The film was choreographed by Pearl Eaton. 

==Preservation status==
The film is also considered a lost film.  The last known copy was destroyed in an RKO fire in the 1950s. 

==See also==
*List of lost films
*List of incomplete or partially lost films
*List of early color feature films

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 