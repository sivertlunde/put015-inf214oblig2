Chirag
 
{{Infobox film
| name = Chirag
| image =Chirag-1969-Indian_movie.jpg
| caption = DVD cover
| director = Raj Khosla
| producer = Premji
| writer = Akhtar ul Iman
| narrator =
| starring = Sunil Dutt Asha Parekh Chirag Dhariwal
| music = Madan Mohan Majrooh Sultanpuri (lyrics)
| cinematography =
| editing =
| distributor =
| released =
| runtime =
| country = India
| language = Hindi
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Indian Bollywood film directed by Raj Khosla. The film stars Sunil Dutt and Asha Parekh in lead roles. Apart from other plus points, it has the famous song teri aankhon ke siwa sung separately by Mohd. Rafi and Lata Mangeshkar. It also has mere bichhade sathi sunta ja and chhayi barakha bahar.. both sung by Lata Mangeshkar.

==Plot==
Ajay Singh (Sunil Dutt) meets Asha Chibber (Asha Parekh), and is led to believe that she belongs to a wealthy family. After a few misunderstandings, he does find out that she is a simple girl living a poor lifestyle. Both do fall in love, and Ajay goes to meet with Ashas brother, Dr. O.P. Chibber (Om Prakash), and his wife, Shanti (Sulochana Latkar). They are pleased to meet him, and Ajays mother, Gayetridevi (Lalita Pawar) visits them and approves of Asha. Ajay and Asha get married and settle down. After marriage Asha is expected to conceive, but she is unable to do so. After a certain incident she also loses her eyesight and becomes dependent on Ajay. Frustrated Gayetridevi decides to ask Asha to leave the house, and arranges a second marriage for Ajay, an act which will change the lives of everyone in the house.

==Cast==
* Sunil Dutt ...  Ajay Singh
* Asha Parekh ...  Asha Chibber
* Om Prakash ...  Dr. O.P. Chibber
* Kanhaiyalal ...  Singhs employee
* Mukri ...  Tingu
* Sulochana Latkar ...  Shanti Chibber (as Sulochana) Dulari ...  Malinya
* Lalita Pawar ...  Gayetridevi Singh

==Awards==
*Nominated, Filmfare Best Actress Award - Asha Parekh {{cite web|title=Awards for
Chirag|publisher=IMDb|url=http://www.imdb.com/title/tt0064164/awards|accessdate=2011-05-10}} 

==Music==
{{Infobox album|
| Name = Chirag
| Type = Album
| Artist = Madan Mohan
| Cover =
| Released =   1969 (India)
| Recorded = Feature film soundtrack
| Length =
| Label =  Sa Re Ga Ma
| Producer = Madan Mohan
| Reviews =
| Last album = Ek Kali Muskai (1968)
| This album = Chirag (1969)
| Next album = Dastak (1970)
}}

The soundtrack of the film contains 6 songs. The music is composed by Madan Mohan, with lyrics authored by Majrooh Sultanpuri.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Singer(s)
|-
| "Bhor Hote Kaga"
| Lata Mangeshkar
|-
| "Chhai Barkha Bahar"
| Lata Mangeshkar
|-
| "Chirag Dil Ka Jalao"
| Mohammed Rafi
|-
| "More Bichhade Saathi"
| Lata Mangeshkar
|-
| "Teri Aankhon Ke Siva"
| Lata Mangeshkar
|-
| "Teri Aankhon Ke Siva"
| Mohammed Rafi
|}

==References==
 

==External links==
* 

 
 
 
 
 