Love 86
{{Infobox film
| name           = Love 86
| image          = Love 86.jpg
| image_size     = 
| caption        = 
| director       = Esmayeel Shroff
| producer       = Pranlal V. Mehta
| writer         = Moin-ud-din 
| narrator       =  Neelam and Govinda (actor)|Govinda. 
| music          = Laxmikant Pyarelal
| cinematography = Russi Billimoria
| editing        = A.R. Rajendran
| distributor    = 
| released       = 1986
| runtime        = 
| country        = 
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Love 86 is a 1986 Bollywood film directed by Esmayeel Shroff and starring Tanuja, Govinda (actor)|Govinda, Rohan Kapoor, Farha Naaz and Neelam Kothari|Neelam.

==Cast==
*Tanuja as  Laxmidevi
*Rohan Kapoor as  Omi
*Farha Naaz as  Leena Neelam as  Esha Govinda as  Vicky
*Shafi Inamdar as  Ramniwas Tilak
*Satish Shah as  Havaldar Sandu
*Ravi Baswani as  Havaldar Pandu
*Johnny Lever as  Uttam
*Asrani as  Hanuman
*Birbal
*Dinesh Hingoo as  Surendra Nath
*Rita Rani Kaul as  Mrs. Surendra Nath
*Guddi Maruti
*Anjan Srivastav as  Dr. Dilip Sen

==Plot==
Laxmidevi (Tanuja), a strict disciplinarian, wants her daughters, Leena (Farha Naaz) and Esha (Neelam Kothari), to marry brothers from a wealthy family, so that both girls can stay in the same household. But Leena and Esha fall in love with Omi (Rohan Kapoor) and Vicky (Govinda)respectively. The boys are poor orphans who have taken to petty crimes in order to survive. How the matter is resolved forms the rest of the story.

==Music==

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)


| title1 = O Miss De De Kiss 
| extra1 = Suresh Wadkar, Shailendra Singh (singer)
| length1 = 04:57
| title2 = Main Jab Bhi 
| extra2 = Shabbir Kumar, Kavita Krishnamurthy
| length2 = 04:42
| title3 = Pyar Ki Had Se
| extra3 = Mohammed Aziz, Kavita Krishnamurthy
| length3 = 05:36
| title4 = Mehboob Se Hamare
| extra4 = Mohammed Aziz, Kavita Krishnamurthy
| length4 = 07:08
| title5 = Aayee Hai Barat
| extra5 = Shabbir Kumar, Suresh Wadkar
| length5 = 05:53
| title6 = Kismat Apni Khul Gayi
| extra6 = Kavita Krishnamurthy, Anuradha Paudwal
| length6 = 05:25

}}

== External links ==
*  

 
 
 
 
 
 


 
 