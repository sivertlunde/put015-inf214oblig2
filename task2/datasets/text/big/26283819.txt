Whale (film)
{{Infobox film
| name = Кит ( )
| image = TheFilmWhaleCover.jpg
| imagesize = 50px
| caption = the new DVD cover
| director = Petar B. Vasilev
| writer = Hristo Mihov - Cheremuhin 
| starring = Georgi Kaloyanchev Dimitar Panov Georgi Partsalev Grigor Vachkov Tsvyatko Nikolov
| music = Atanas Boyadzhiev
| cinematography = Emil Vagenshtain
| studio = 
| released = 1970
| country = Bulgaria
| runtime = 76 min. Bulgarian
}}
 Bulgarian satirical comedy film directed by Petar B. Vasilev and written by Cheremuhin. The film stars Georgi Kaloyanchev, Dimitar Panov, Georgi Partsalev, Grigor Vachkov and Tsvyatko Nikolov.

This film had one of the most scandalous and at the same time mythological destiny in the history of the Bulgarian cinema. It was filmed in 1967 but didnt released until 1970 when was shown at few small cinema halls as an expurgated by the communist authority edition.  Wale satirize the extant defects in the economic and social structure of the state of those years. Moreover the film unambiguous specify the exact carriers of the negative effects. The shooting mark is the bureaucratic pathos at the different ruling levels. The pathos by means of which various data and information are manipulated in the name of non-existing achievements. 

In the film was born one of the most popular quotes in the Bulgarian cinema: 
 

==Plot==
A small fishing ship after an unsuccessful quest for some draught got not more than a single fish on top of everything the smallest one -  a sprat. Despite the miserable catch the captain (Vachkov) reports through the transmitter to the superior of the fishing base Petrov (Nikolov). Of course he mentions a bigger species of fish and doesnt particularize the quantity. After months without any production from the enterprise, the superior of the base in his turn reports personally to the big local boss Kalcho Kalchev (Panov). One more time the species are enlarged and even the presence of a considerable shoal is entangled. Sitting to the table the two men have a drink or two of some cognac to celebrate the occasion. They dont stop and decide to specify the catch as a draught of dolphins as more massy kind of sea creature.
Being in private later the local boss has a colourful monologue with the portrait on the wall portraying the minister Parushev (Kaloyanchev) of the field they work in. Finally the Kalchevs right hand - the lead engineer (Partsalev) insinuate that the dolphin, as a matter of fact, is actually a kind of an whale. 

 
So one sunny day in the head department in the capital the message is received - "...&nbsp;We got an whale&nbsp;...". The state employees cant wait to inform the minister. Naturally he spends his days in the nice villa around the city. The pathos spread to all of them. In a fit of euphoria Parushev proposes a new name of their state department - The Ministry of Whales. The ideas of some whales festival are born even a whaling flotilla is mentioned. Leading the procession Parushev, Kalchev and all of the involved are formed up at the pier to welcome the inbound ship. The festive meeting of the bewildered fishermen is the culmination of the bureaucratic farce.

==Production==
===The Idea===
 
In the beginning of the 1950s the screenwriter Hristo Mihov, nicknamed Cheremuhin, followed his wife in the town of Aitos where she was allocated as a doctor. The town is not far from the seaside so Cheremuhin heard many maritime stories. Once, one of the locals told him a story about a fisherman who caught some sprats but boasted that it were mackerels. Every time when the story was told and the fish became bigger and bigger until in the end it was a story about a shark. The people then replied that there are no sharks in this sea. But the fisherman said: "...&nbsp;How do you know, I has even saw an whale out there&nbsp;...". Mihov undertook a ten days sailing amidst the fishermen so that he could learn the terminology and details. Afterwards he sat behind the typewriter and in the 1955 the short novel was completed. P. Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008 

===Screenplay and filming=== The Past-Master, and Cheremuhin retired in the ex-royal residence Sitnyakovo which was turned into the Writers Union base. In two months the screenplay was completed.
 Cinema Center near the Sofia suburb Boyana. There was situated the villa of the minister Parushev. 

The song that was sung by the state employees while going to the Parushevs villa was composed by Atanas Boyadzhiev. The lyric was by the poet Plamen Tsonev:

 

This song became a hit but the censorship came after instead of some gala premiere.

===Censorship===

 
 Republic of that time Todor Zhivkov. It was also perceived some relation between the character of Kalcho Kalchev (Panov) and Stanko Todorov another communist leader. In the film Kalchev is the general executive of the local enterprise. But before that it became clear that he was simply a hatter. As a matter of fact Stanko Todorov was a tailor in his youth. 
 Vasilev and the cinematographer Emil Vagenshtain bravely struggled with the censoring. The reel stayed more than a two years in the basement stores. Interestingly at that time the big scandal with some local communist leader burst out in the country. He reported a great crop of wheat for what he was awarded. Subsequently it turned out that the crop was below the average. 

 
On top of everything the screenwriters brother emigrated to USA. He was a nuclear physicist and after a specialization in France he left for North America instead of returning to Bulgaria.
The appearance of Radoy Ralin in an episode as an ichthyologist also added oil to the flames. Being famous writer satirist and a poet he was also a well known dissident during the communist regime. 

 
Finally the film was released but at first in only two cinema halls in the city of Plovdiv. Even without any advertisement the tickets are sold out. Afterwards Whale was shown in the small Levski cinema in the capital of Sofia again with no preliminary announcement. There were cues and frames missing even the whole episodes were cut off. 

The non censored edition was released on DVD in 2000s.

==Cast==
The film features eminent Bulgarian comic actors. Kaloyanchev, Panov and Partsalev are in theirs best. Unfortunately Whale is the last film for the Tsvyatko Nikolov. He died shortly when the film was released in 1970. In some episodes we can see the director Vasilevs wife the actress Valentina Borisova. The acting of the memorable Grigor Vachkov should be also pointed out. It is known that one of the fishermen in the ship around Vachkov was his wifes brother in the real life. 
 
{| class="wikitable"
|-
! Character
! Actor
! Role
|-
| Parushev
| Georgi Kaloyanchev
| the minister
|-
| Kalcho Kalchev
| Dimitar Panov
| the general executive
|-
| 
| Georgi Partsalev
| the lead engineer
|-
| Gerdzhikliyski
| Grigor Vachkov
| the captain of the boat
|-
| Petrov
| Tsvyatko Nikolov
| the superior of the fishing base
|-
| 
| Evstati Stratev
| the boat sailing superior 
|-
| Baba Stoyna
| Stoyanka Mutafova
| the captains mother
|-
| 
| Nikola Dinev
| the editor
|-
| 
| Georgi Naumov
| the telegraph operator 
|-
| 
| Naicho Petrov
| first assistant director general
|-
| 
| Georgi Popov
| second assistant director general
|-
| 
| Radoy Ralin
| Bostandzhiev, the ichthyologis
|-
| 
| Renata Kiselichka
| the Komsomol leader
|-
| 
| Nadya Topalova
| driver 
|-
| 
| Lyubomir Kiselichki
| the young research worker 
|-
| 
| Kremena Trendafilova
| 
|-
| 
| Katya Chukova
| 
|-
| 
| Valentina Borisova
| 
|-
| 
| Bogdana Vulpe
| 
|-
| 
| Valentin Rusetzki
|
|-
| 
| Gerasim Mladenov
| 
|-
|  Borislav Ivanov
| 
|-
| 
| Lambi Poryazov
| 
|}

==Response==
A reported 599,350 admissions were recorded for the film in cinemas throughout Bulgaria.  Taking into consideration that, because of the censorship, it was released in small cinema halls with no advertisement the film Whale attracted considerable audience. 
 liberation in 1989/90 the film was at first broadcast on the Bulgarian National Television. Naturally, it gave rise to a broad interest and took his due place among the Bulgarian notable films of those years. Subsequently, during the 2000s, it was released on DVD.

The film was subsumed among the 50 golden Bulgarian films in the book by the journalist Pencho Kovachev. The book was published in 2008 by "Zahariy Stoyanov" publishing house.

==Notes==
 

==References==
*    
* Pencho Kovachev, 50 Golden Bulgarian Films, Zahariy Stoyanov 2008
*    

==External links==
*  

 
 
 