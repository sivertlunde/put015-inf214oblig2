State's Attorney (film)
{{infobox film
| name           = States Attorney
| image          = States-attorney film poster.jpg
| imagesize      =
| caption        = Theatrical Poster
| director       = George Archainbaud
| producer       = James Kevin McGuinness (associate producer) David O. Selznick (executive producer)
| writer         = Louis Stevens story) Gene Fowler (script) Rowland Brown (script)
| starring       = John Barrymore Helen Twelvetrees
| music          = Max Steiner Ray Heindorf
| cinematography = Leo Tover
| editing        = Charles L. Kimball
| distributor    = RKO
| released       = May 5, 1932 (New York)  May 20, 1932 (nationwide; US)  October 16, 1933 (Portugal)  March 26, 1937 (France)
| runtime        = 73 minutes
| country        = United States
| language       = English
}}

States Attorney is a 1932 talking drama film made at RKO and starring John Barrymore. George Archainbaud directed and the film could more or less be considered a warm up for Barrymore when he later went to Universal to film the similar Counsellor at Law. Remade in 1937 and 1951 both titled Criminal Lawyer.  The story was supposedly based on the life of criminal lawyer William J. Fallon, who defended 126 homicide cases without any convictions. 

==Plot summary== William Boyd of Hopalong Cassidy fame).

Powers thinks it would be a good idea for Cardigan to become Attorney General so his friend could do an occasional favor in return for Powers delivering the votes. Cardigan warns him that if Cardigan goes over to "the other side," Powers can expect no favors from him.

Meanwhile, Cardigan decides to defend a homeless woman, June Perry (Helen Twelvetrees), accused of “tapping at the window” and, after secretly fitting her with a wedding ring he keeps in his pocket, frees her by noting the presence of said ring (inferring she therefore could not be loitering for prostitution). He takes her home and, in a plot twist the Production Code would not allow, June stays there overnight. And every night thereafter.

Cardigans success as Attorney General makes him a likely candidate for Governor. A political kingmaker thinks its possible and his daughter, Lillian (Jill Esmond), begins dating Cardigan. During a drunken spree, they get married and he then goes home to tell June the bad news. During his explanation, he realizes he has made a terrible mistake and that he loves June, not Lillian. Nonetheless, June leaves and Cardigan goes on a honeymoon bender for several days, alone.

Meanwhile, June has returned to her old friends in the Powers mob at a bar. Unfortunately, she walks outside just in time to see Powers murder a man in cold blood. She turns and walks quickly away. Powers catches up with her and threatens to kill her unless June keeps her mouth shut. She agrees but an off-stage policeman overhears her agreement and jails her as a material witness.

An Italian tenor, Mario (Albert Conti), confronts Cardigan as he is sobering up, saying he wants to marry Lillian. Breathing a sigh of relief, Cardigan says he will annul his marriage. Later, Cardigan interviews the material witness and finds its June (who refuses to return to him, thinking he has betrayed his values so he can become Governor). She adamantly maintains she did not see the murder so he releases her as a witness.

At Powers trial, the defense springs June as a surprise witness, forcing her to admit that she could see the killer but didnt see the murder and didnt recognize Powers. Shocking his assistants, Cardigan decides not to cross-examine her. Powers laughs heartily, stopping Cardigan in his tracks. The Attorney General then withdraws his waiver, whispering “that laugh is going to cost you your neck” to Powers and promptly badgers and confuses June so that she blurts out an identification of Powers as the killer.

Begging the courts indulgence, Cardigan abruptly announces that his assistants will handle the rest of the case. He then confesses that he had been sent to reform school—with Powers—for burglary and will therefore not run for governor, returning to his defense attorney status immediately. (Powers had threatened to blackmail him if Cardigan prosecuted him.) Outside, Helen congratulates him for his courage and for choosing his values over his ambition. They embrace and leave hand in hand.

==Cast==
* John Barrymore as Tom Cardigan
* Helen Twelvetrees as June Perry
* Jill Esmond as Lillian Ulrich
* William "Stage" Boyd as Valentine Vanny Powers
* Mary Duncan as Nora Dean
* Ralph Ince as Second Trial Defense Attorney
* Albert Conti as Mario Frederick Burton as Second Trial Judge
* C. Henry Gordon as Attorney Graves Paul Hurst as Police Captain Morgan
* Oscar Apfel as Mr. Ulrich
* Leon Ames as First Trial Prosecutor

==References==
 

==External links==
*  
*  
*  
*  
 
 
 
 
 
 
 
 
 
 
 

 