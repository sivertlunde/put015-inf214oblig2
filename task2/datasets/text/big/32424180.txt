A Roman Scandal (film)
 
{{Infobox film
| name = A Roman Scandal
| image = Ad for roman scandal mpn.JPG
| image_size =
| caption = A magazine ad featuring Colleen Moore as a new actress for Christie Film Co. starring in the film A Roman Scandal.
| director = Al Christie
| producer =
| writer = Frank Roland Conklin (story) Scott Darling (scenario)
| narrator = Eddie Barry
| music =
| cinematography =
| editing =
| distributor = Christie Film Company
| released = November 30, 1919
| runtime =
| country = United States Silent English intertitles
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
 short Silent silent American comedy film starring Colleen Moore, and directed by Al Christie.

==Story==
Mary is stage struck and will not marry until she makes it in show business. Her fiance is distraught that they might never marry. The actors of the local stage company go on strike, leaving management with nobody to fill all the roles. Mary volunteers herself and her fiance, and in the confusion of the production, chaos follows. In the end, Mary abandons her dreams for domestic bliss.

==Background==
Moore went to work with Al Christie to develop her comedy skills.  Prior to her work with Christie, she was strictly a dramatic actress. In her autobiography Silent Star, she said she had read a quote that the greatest dramatic actresses had gotten their starts in comedy.
 Anne of Green Gables, a Mary Miles Minter film,  and with Cosmo Hamiltons The Miracle of Love, at the Rivoli Rialto Theater in New York. 

==Footnotes==
 

==Bibliography==
*Jeff Codori (2012), Colleen Moore; A Biography of the Silent Film Star,  ,(Print ISBN 978-0-7864-4969-9, EBook ISBN 978-0-7864-8899-5).

==External links==
*  

 
 
 
 
 
 
 
 
 


 