River's End (2005 film)
{{Infobox film
| name = Rivers End
| image = 
| caption = 
| director = William Katt
| producer = Glen Stephens Scott Duthie Clifford Matthews
| writer = Glen Stephens
| starring = Barry Corbin Sam Huntington Caroline Goodall William Katt Charles Durning Clint Howard Jackson Rathbone
| music = Paul Christo Jay Michael Ferguson
| cinematography = John-Paul Beeghly
| editing = Steven Dieveney
| studio = Molding Clay Productions
| distributor = American World Pictures
| released = 2005 ( ) February 20, 2007 ( )
| runtime = 90 minutes
| country = United States
| language = English
| budget = $1 million
}}
Rivers End is a 2005  American Western film|Western-drama film directed by William Katt.

==Plot==
Filmed on location in Central, South, and West Texas. Corbin plays a fictional Menard County sheriff who uses country savvy and cowboy logic to straighten out his angry teenage grandson, Clay, a high school senior who cant seem to stay out of trouble.

==Cast==
* Barry Corbin as Sheriff "Buster" Watkins
* Sam Huntington as Clay Watkins
* Caroline Goodall as Sarah Watkins
* William Katt as Ed Kennedy
* Charles Durning as Murray Blythe
* Clint Howard as Mr. Powell
* Jackson Rathbone as Jimmy
* Amanda Brooks as Regina Kennedy
* Greg Siff as Doug Barton

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 