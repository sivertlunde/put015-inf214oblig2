A Kiss for Cinderella (film)
{{Infobox film
| name            = A Kiss For Cinderella
| image           = A Kiss for Cinderella 1925.jpg
| image size      = 180px
| caption         = 1925 lobby poster
| director        = Herbert Brenon
| producer        = Adolph Zukor Jesse L. Lasky
| writer          = James M. Barrie (play) Willis Goldbeck (scenario) Townsend Martin (scenario) Tom Moore Esther Ralston Dorothy Cumming
| music           =
| cinematography  = J. Roy Hunt
| editor          =
| distributor     = Paramount Pictures
| released        =  
| runtime         = 10 reels; 9,686 feet
| country         = United States
| language        = Silent film (English intertitles)
| budget          =
}} silent fantasy stage play Tom Moore Astoria Studios play which starred stage actress Maude Adams in the Bronson role.  
 the 1924 Goldwyn in 1917 alongside Mae Marsh.

Surviving print of this film is preserved at the Museum of Modern Art, New York, George Eastman House and UCLA Film and Tv as well as a foreign archive Cinematheque Royale de Belgique(Brussels). 

==Cast==
* Betty Bronson - Cinderella (Jane)
* Esther Ralston - Fairy Godmother
* Dorothy Cumming - Queen Tom Moore - Policeman
* Flora Finch - Customer
* Ivan Simpson (as Ivan F. Simpson) - Mr. Cutaway
* Edna Hagen - Gretchen
* Dorothy Walters - Mrs. Maloney

==References==
 

==External links==
*  at IMDb
* 
* 
* 

 
 

 
 
 
 
 
 
 


 