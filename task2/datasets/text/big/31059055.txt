I Want to Be a Soldier
{{multiple issues|
 
 
}}

{{Infobox film
| name           = I Want to Be a Soldier
| border         = yes

| alt            =  
| image          = I Want to Be a Soldier FilmPoster.jpeg
| caption        = 
| director       = Christian Molina
| producer       = Ferran Monje  Carlos Gari  Marivi de Villanueva
| writer         = Cuca Canals, Christian Molina
| starring       = Fergus Riordan
| music          = Federico Jusid
| make up        = Betty de Villanueva
| cinematography = Juan Carlos Lausín
| editing        = Alberto de Toro
| studio         = 
| distributor    = Canónigo Films Stars Pictures Trees Pictures Black Flag Cinema
| released       =  
| runtime        = 88 minutes
| country        = Spain
| language       = English
| budget         = 
| gross          = 
}}
I Want to Be a Soldier is a drama film released in Spain and at the Rome Film Festival in 2010. It is the fourth feature film of director Christian Molina.

==Plot==

 
Alex, a 10-year-old child, develops an unhealthy obsession with morbid imagery, which marks him asocial. Unable to befriend his peers, he creates an imaginary friend and role model, whom he names Astronaut Captain Harry. Upon the birth of his twin baby brothers, Alex feels more neglected than usual. He emotionally extorts his father into buying him a TV for his room, and starts spending hours watching violent programmes. As his antisocial behavior deepens, he also takes on a new imaginary friend, Sergeant John Cluster who will teach him how to realize his new dream of being a great soldier.

==Cast==
* Fergus Riordan as Alex
* Ben Temple as Astronaut Captain Harry / Sergeant John Cluster
* Andrew Tarbet as Father
* Jo Kelly as Mother
* Valeria Marini as teacher
* Cassandra Gava
* Josephine Barnes
* Robert Englund as psychiatrist
* Danny Glover as the principal

==References==
 

==External links==
*  
*  

 
 
 

 