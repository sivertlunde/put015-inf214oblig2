Reality (2014 film)
{{Infobox film
| name           = Reality
| image          = 
| caption        =
| director       = Quentin Dupieux
| producer       = Gregory Bernard Diane Jassem Josef Lieck  Kevin Van Der Meiren 
| writer         = Quentin Dupieux  John Glover Eric Wareheim Jon Heder
| music          = Quentin Dupieux 
| cinematography = Quentin Dupieux  		 
| editing        = Quentin Dupieux 
| studio         = Realitism Films Rubber Films
| distributor    = Diaphana Films  
| released       =  
| runtime        = 87 minutes
| country        = France Belgium
| language       = English French 
| budget         = 
| gross          = 
}}

Reality ( ) is a 2014 French-Belgian comedy-drama film written and directed by Quentin Dupieux. The film premiered in the Horizons section at the 71st Venice International Film Festival on 28 August 2014. 

== Cast ==
* Alain Chabat as Jason Tantra
* Jonathan Lambert as Bob Marshal
* Élodie Bouchez as Alice
* Kyla Kenedy as Reality 
* Eric Wareheim as Henri   John Glover as Zog 
* Jon Heder as Denis 
* Lola Delon as Zogs Assistant 
* Matt Battaglia as Mike 
* Susan Diol as Gaby 
* Erik Passoja as Billie 
* Jonathan Spencer as Blue  
* Bambadjan Bamba as Tony  
* Serge Hazanavicius as Award Presenter  
* Roxane Mesquida as Awards Hostess   
* Brad Greenquist as Jacques 
* Patrick Bristow as Klaus 
* Sandra Nelson as Isabella 
* Axelle Cummings as The Receptionist 
* Brandon Gage as Serge  
* Raevan Lee Hanan as Luci  
* Carol Locatell as Lucienne

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 

 