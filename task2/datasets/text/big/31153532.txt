Rachana (film)
{{Infobox film
| name           = Rachana
| image          =
| caption        = Mohan
| producer       = Sivan Kunnampilly John Paul (dialogues) John Paul
| starring       = Srividya Nedumudi Venu Bharath Gopi Mammootty
| music          = MB Sreenivasan
| cinematography = Vasanth Kumar
| editing        = G Murali
| studio         = Thushara Films
| distributor    = Thushara Films
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Mohan and produced by Sivan Kunnampilly. The film stars Srividya, Nedumudi Venu, Bharath Gopi and Mammootty in lead roles. The film had musical score by MB Sreenivasan.    Bharath Gopi won the Kerala State Film Award for Best Actor for his performance in the film.

==Cast==
*Srividya as Sarada
*Nedumudi Venu as Achyuthanunni
*Bharath Gopi as Sreeprasad
*Mammootty as Gopi
*Jagathy Sreekumar as Thomas
*Vijay Menon as Rajan
*Poornima Jayaram as Thulasi
*Thrissur Elsy as Jagathys wife

==Soundtrack==
The music was composed by MB Sreenivasan and lyrics was written by Mullanezhi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaalamayoorame  || S Janaki || Mullanezhi || 
|-
| 2 || Onnaanaam kaattile || S Janaki, Unni Menon || Mullanezhi || 
|}

==References==
 

==External links==
*  

 
 
 

 