Shallow Ground
{{Infobox film
| name           = Shallow Ground
| image          = Shallow Ground poster.jpg
| caption        =
| director       = Sheldon Wilson
| producer       = Jason Ninness Martin Mathieu John P. Tarver Sheldon Wilson
| writer         = Sheldon Wilson
| starring       = Timothy V. Murphy Stan Kirsch Lindsey Stoddart Patricia McCormack Rocky Marquette Tara Killian
| music          = Steve London
| cinematography = John P. Tarver
| editing        = Sheldon Wilson
| distributor    = Imagination Worldwide
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
}}
Shallow Ground is a 2004 horror film written and directed by Sheldon Wilson and starring Timothy V. Murphy, Stan Kirsch, Lindsey Stoddart, Patty McCormack, and Rocky Marquette.  A naked teenage boy (Rocky Marquette) appears at a soon-to-be abandoned sheriffs station, drenched in blood, on the one-year anniversary of the murder of a local girl that remains unsolved. Sheriff Jack Shepherd (Timothy V. Murphy) searches for the boys true identity and intentions, while dealing with his guilt over failing to save the girl or solve her murder.

==Plot summary==
Sheriff Jack Shepherd is tasked with investigating a mysterious blood-covered boy who shows up at his station one day. The boy appears to be supernatural in nature, and manifests unusual powers such as the ability to read minds or show other people his memories by touching them.
 Michael Myers.

It is revealed near the end of the movie that the dead across the world are rising up and seeking revenge on the people who killed them. These dead manifest as blood-covered humans that seek out and kill their original killers. If a killer has more than one victim, all the victims merge into a single composite being. The blood-covered boy is one such being, made up of 6 people from the community who were killed by the stalker some time before. He is going around reading peoples minds to learn the identity of his killer.

The killer stalker turns out to be a local older woman, who has been murdering people in the community for some time. Her family was killed when the local dam broke, and in revenge she has been killing everyone involved with the dam, as well as their families. The bloody boy eventually tracks her down and kills her, just as she is about to kill Jack.
 shock ending. The bloody boy is walking through the woods, when he is suddenly attacked and killed by another blood creature, this one much larger and more demonic-looking. The identity of the second blood creature is never explicitly revealed, but from the creatures appearance it is implied to be the old woman from earlier after that the film ends.

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 