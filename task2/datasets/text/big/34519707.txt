Judge Barrister Police Commissioner
{{Infobox film
| name = Judge Barrister Police Commissioner
| image  = 
| caption = 
| director = F I Manik
| producer = F I Manik
| writer = Jonto Purnima Abdur Razzak Sohel Rana Alamgir
| distributor = Fim Films Int.
| music = Aalauddin Ali
| cinematography = Mostafa Kamal
| editing = 
| released = 
| runtime = 
| country = Bangladesh
| language = Bengali
| budget         = 
| gross          = 
}} Purnima in the lead roles.   The film released on May 10, 2013.  
==Cast==
* Shakib Khan Purnima
* Razzak
* Alamgir
* Sohel Rana 
* Suchirita
* Misa Shodugor
* Shadek Bacchu
* Ahmed Sharif
* Sujirita
* Shiba Sanu
* Rahena Joli
==Production==
The movie was expected to hit screens on 2007, but due to some financial and casting problems, it failed to do so. 

== Soundtrack ==
The soundtrack of Judge Barrister Police Commissioner was composed by  Alauddin Ali.

===Track listing===
{{Track listing
| title1 = Kiser Avishap
| extra1 = Andrew Kishore & Sabina Yasmin
| title2 = Valobasha Fire Alo
| extra2 = Andrew Kishore & Sabina Yasmin
| title3 = Donia Jar Hater Mutho
| extra3 = Biplop
| title4 = Ke Tumi
| extra4 = S I Tutul and Sabina Yasmin
| title5 = Ore Chok Keno Kadhe
| extra5 = Monir Khan
| title6 = Toke sui
| extra6 = S I Tutul, Andrew Kishore & Sabina Yasmin
}}

==References==
 

 
 
 
 
 
 


 
 