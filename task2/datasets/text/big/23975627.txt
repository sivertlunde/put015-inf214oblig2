No One Can Achieve Perfection
{{Infobox Film 
| name           = No One Can Achieve Perfection
| image          = 
| caption        = 
| director       = Katrin Ottarsdóttir
| producer       = Hugin Eide
| writer         = Katrin Ottarsdóttir
| narrator       = 
| starring       = 
| music          = 
| cinematography = Katrin Ottarsdóttir
| editing        =[Katrin Ottarsdóttir
| distributor    = Blue Bird Film
| released       = 30 January 2008
| runtime        = 85 minutes
| country        = Denmark Faroese
| budget         = 
}} Faroese sculptor Hans Pauli Olsen (born 1957).

==Synopsis==
The film follows the creation of sculptor Hans Pauli Olsens biggest sculpture so far. We take part in the process when the artist is working in his backyard studio together with his young nude model. We are the fly on the wall, when the artist forgets about the camera and exists only for creating his art with his bare hands. And we experience the contrast between art as subtile, exhibited works in distinguished halls of art and as a simple lump of clay in a dirty garage.

==External links==
*  Website of filmmaker Katrin Ottarsdóttir

 
 
 
 
 
 
 
 


 
 