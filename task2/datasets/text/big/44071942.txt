Laure (film)
{{Infobox film
 | name =Laure
 | image =Laure (film).jpg
 | caption =
 | director =Louis-Jacques Rollet-Andriane  Roberto DEttorre Piazzoli
 | writer =   Louis-Jacques Rollet-Andriane  Ovidio G. Assonitis
 | starring =  
 | music = Franco Micalizzi
 | cinematography =Roberto DEttorre Piazzoli
 | editing =  	 
 | language =  Italian
 | released =  1976	
 }} 1976 Cinema Italian erotic film directed by Louis-Jacques Rollet-Andriane and Roberto DEttorre Piazzoli, even if the film was advertised as directed by Emmanuelle Arsan.    The first choice for the title role was Linda Lovelace, but due to her personal problems at the time, she was first recast in the secondary role of Natalie Morgan, and eventually put out of the film.    AA. VV. Beyond the Screen. Il cinema di Ovidio G. Assonitis. "Nocturno dossier" N. 82, May 2009, Cinemabis.  It was shot in the Philippines and in Rome.    AA. VV. Beyond the Screen. Il cinema di Ovidio G. Assonitis. "Nocturno dossier" N. 82, May 2009, Cinemabis. 

== Cast ==
*Annie Belle: Laure
*Al Cliver: Nicola
*Orso Maria Guerrini: Professor Gualtier Morgan
*Michele Starck: Natalie Morgan
*Emmanuelle Arsan: Myrte

==References==
 

==External links==
* 
 
  
 
 
 
 
 

 
 