Pan (1995 film)
{{Infobox film
| name           = Pan
| image          = 
| caption        = 
| director       = Henning Carlsen
| producer       = Axel Helgeland
| writer         = Knut Hamsun (novel) Henning Carlsen (script)
| starring       = Sofie Gråbøl, Lasse Kolsrud, Bjørn Sundquist, Anneke von der Lippe, Per Schaanning
| music          = Hilmar Ørn Hilmarsson
| cinematography = Henning Kristiansen
| editing        = Anders Refn
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Denmark, Norway, Germany
| language       = Danish and Norwegian
| budget         = 
}}
Pan (also released under the title Two Green Feathers) is a 1995 Danish/Norwegian/German film directed by the Danish director  .   It is the fourth and most recent film adaptation of the novel—the novel was previously adapted into motion pictures in Pan (1922 film)|1922, Pan (1937 film)|1937, and Pan (1962 film)|1962.

==Production==
In 1966 Carlsen had directed an  , November 5, 1995.   The film was produced primarily with Norwegian resources, and classified as a Norwegian film; Carlsen later expressed his dissatisfaction with the films promotion by the Norwegian Film Institute, saying that the Institute had preferred to promote films with Norwegian directors.  Carlsen said that he had decided to incorporate the "forgotten" material from "Glahns Death" in order to find a "new angle" for filming the book.   The Glahns Death portion was filmed in Thailand, standing in for the India location in the novel (the 1922 film version had placed this material in Algeria). 

The film had its American premiere at the Museum of Modern Art in New York City as part of MOMAs 1995 retrospective of films based on Hamsuns work. 

==Awards==
Anneke von der Lippe won the 1995 Norwegian International Film Festivals Amanda Award for Best Actress for her work in Pan and another film, Over stork og stein. She also won the 1996 Danish Bodil Award for Best Supporting Actress for Pan.  

==Cast==
*Sofie Gråbøl as Edvarda Mack
*Lasse Kolsrud as Lieutenant Thomas Glahn
*Bjørn Sundquist as Ferdinand Mack
*Anneke von der Lippe as Eva
*Per Schaanning as the Doctor
*Peter Striebeck as the Baron
*Shaun Lawton as the Hunter

==References==
 

==External links==
* 
* , National Library of Norway

 
 
 
 
 
 
 
 
 
 
 