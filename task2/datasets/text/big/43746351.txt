Accused (2014 film)
 
{{Infobox film
| name           = Accused
| image          = Accused (2014 film).jpg
| caption        = Film poster
| director       = Paula van der Oest
| producer       = 
| writer         = Moniek Kramer Tijs van Marle
| starring       = Ariane Schluter
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} Best Foreign Language Film at the 87th Academy Awards,    making the January Shortlist.   

==Cast==
*Ariane Schluter as Lucia de Berk
*Barry Atsma as Jaap van Hoensbroeck
*Amanda Ooms as Jenny
*Marwan Kenzari as Rechercheur Ron Leeflang
*Fedja van Huêt as Quirijn Herzberg
*Annet Malherbe as Ernestine Johansson
*Sallie Harmsen as Judith Jansen

==See also==
*List of submissions to the 87th Academy Awards for Best Foreign Language Film
*List of Dutch submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 