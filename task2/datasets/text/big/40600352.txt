Legend (2014 film)
 
 
{{Infobox film
| name           = Legend 
| image          = Legend Telugu movie poster.jpg
| caption        = Movie poster
| director       = Boyapati Srinu Sai Korrapati   
| writer         = M. Ratnam  
| story          = Boyapati Srinu
| screenplay     = Boyapati Srinu Kalyani
| music =Devi Sri Prasad {{cite web |url=http://timesofap.com/cinema/devi-sri-prasad-bags-balakrishna-s-new-movie.html
| title          = Devi Sri Prasad bags Balakrishnas New Movie|publisher= timesofap.com | accessdate= 23 September 2013 }} 
| cinematography = Ram Prasad Anthony
| studio         = 14 Reels Entertainment Varahi Chalana Chitram
| released       =  
| runtime        = 161 minutes
| country        = India
| language       = Telugu
| budget         =  )
| gross           =  ) (share)  
}}
 Telugu action action film Sai Korrapati was presented the film on Varahi Chalana Chitram  directed by Boyapati Srinu. Starred Nandamuri Balakrishna, worked for the second time with Boyapati Srinu after Simha (film)|Simha, Jagapathi Babu first time in negative shade.  alongside Radhika Apte, Sonal Chauhan, in lead roles and music composed by  Devi Sri Prasad.  Ram Prasad handled the cinematography.  The film released on 28 March 2014 worldwide.   Upon release, the film received positive reviews from critics. The film recorded as Super Hit at box office.  

==Plot==
Krishna (Balakrishna) lives in Dubai, while his family is in India. His grandmother is against him who settled in India. Krishna falls for Sneha (Sonal Chauhan) and comes to India to seek the blessings of his grandmother. Jitender (Jagapati Babu) is a man with pride and haughtiness. He cannot tolerate anyone who comes across his way. Once, he goes to a nearby village for a marriage proposal to him. There he gets involved in a quarrel with the village headman (Suman (actor)|Suman) due to some unfortunate events and gets jailed. From then on wards, he tries to take revenge on that family. He kills the family members of the village headman one-by-one. As soon as Krishna arrives in India, he gets involved in quarreling with the gang of Jitendra. When Krishna and his family members are travelling in a bus to visit a temple Jitenders hitmen shoot Krishna and his family members are beaten badly by his henchmen. Krishnas elder brother Jai Dev The Legend (Balakrishna) who remained exiled obeying his grandmother, saves them and Krishna is admitted in a hospital. Then the past is revealed that twenty years back there was a quarrel between Jai Dev and Jitender in which Jai Devs cousin (Radhika Apte) whom he loved was killed by Jitender. At present he comes to know that Jitender is going to become Chief Minister of the state.How he stops Jitender from claiming this throne forms the rest of the story.

==Cast==
 
* Nandamuri Balakrishna as Jaidev (Legend) & Krishna (Dual role)
* Jagapati Babu as Jitendra
* Radhika Apte as Jaidevs cousin 
* Sonal Chauhan as Sneha Kalyani as Jitendras wife Suman as Jaidevs father
*Suhasini Maniratnam as Jaidevs mother
* Ahuti Prasad as Snehas father
* Rao Ramesh as Jaidevs uncle
* Brahmanandam as Manikyam
* Jaya Prakash Reddy as M.P.
* Brahmaji as Yadav
* Sekhar as Simhachalam
* Ramaraju as Jitendras father
* G. V. Sudhakar Naidu as Jitendras henchman
* L.B. Sriram
* Chalapathi Rao as Raghavaiah
* Sameer as Jaidevs brother-in-law
* Vizag Prasad as C.M.
* Sujata Kumar as Jaidevs grand mother
* Easwari Rao as Jaidevs aunt Sithara as Jaidevs younger sister
* Hamsa Nandini as Menaka in item number
* Anjali as Jaidevs niece
* Sudeepa Pinky as Jaidevs niece Ajay as Jitendras younger brother
 

== Soundtrack ==
{{Infobox album|  
| Name       = Legend
| Type       = Film
| Artist     = Devi Sri Prasad
| Released   =  
| Recorded   = 2014
| Genre      = Soundtrack
| Length     =  
| Label      = Lahari Music
| Producer   = Devi Sri Prasad
| Last album = Bramman (2014)
| This album = Legend (2014)
| Next album = Alludu Seenu  (2014)
}}
 Hyderabad with the films theatrical trailer.

{{tracklist
| headline = Tracklist
| extra_column   = Singer(s)
| total_length   = 23:36
| lyrics_credits = 

| title1  = Legend 
| extra1  = M. L. R. Karthikeyan
| length1 = 3:00

| title2  = Nee Kanti Choopullo  Chitra
| length2 = 4:04

| title3  = Tanjavuru
| extra3  = Sooraj Santhosh, Harini
| length3 = 4:21

| title4  = Time Bomb  Rita
| length4 = 4:13

| title5  = Om Sravani
| extra5  = M.M.Manasi
| length5 = 3:45

| title6  = Lasku Tapa
| extra6  = Sagar, Malathi
| length6 = 4:13
}}

==Production==

===Casting=== Kalyani paired up with Jagapathi Babu as his wife.  Anil Sunkara confirmed that Radhika Apte was the heroine.    Hamsa Nandini appeared in a special song with Balakrishna.  

===Filming===
The muhurtham ceremony of the film was held on 3 June 2013  in Hyderabad.    The film was supposed to start its shoot in Dubai, but the production team decided to start off because Some permissions did not come through in time.  The regular shooting of the film started in Ramoji Film City on 13 July 2013 at Hyderabad.The introduction scene of the hero was shot under the action choreography of Ram Laxman. {{cite web |url=http://www.idlebrain.com/news/today/balakrishna-boyapati-rfc.html
|title= Bala Krishna’s film shoots in RFC|publisher= idlebrain.com | date= 13 July 2013|accessdate= 23 September 2013 }}  A special car chase sequence of the film was shot in the deserts of Dubai   and a song was canned on Balakrishna and Sonal Chauhan in this schedule. 

==Box office== Simha 5th film to complete double century.  The Film turned out to be one of the Biggest Blockbusters of all time in Nandamuri Balakrishnas career. 

The film completed 365 days run in two theatres and is running towards 400 days in Mini Shiva theatre in Emmigannur, Kurnool district with direct four shows and it also completed 365 days in Proddutur (56 days in Arveti theatre and 310 days (single shift 4 shows) at Archana theatre). 

==Pre-Release business==
The first look of the film was released on 1 January 2014  and the satellite rights of the film were reportedly bought for  8.5 crore by Gemini TV. 
  
Legend had a Pre-release revenues up to   with distribution,music and satellite rights. http://timesofindia.indiatimes.com/entertainment/telugu/movies/news-interviews/Balakrishnas-Legend-first-day-collections/articleshow/32919076.cms  

The film was released across the World on 28 March 2014.    

Legend was released in 700 Theaters across the state and over 1200 theaters worldwide.
Legend had a Pre-release revenues up to   with distribution,music and Satellite rights. 

==Awards==
Nandhamuri Balakrishna won the best actor SICA award for his powerful performance in this film.

{| class="wikitable"
|-
! S.no
! Ceremony
! Year
! Category
! Nominee
! Result
|- 1
|SICA award  2015
|Best actor Nandamuri Bala Krishna
|  
|- 2
|rowspan="2"|GAMA Awards 2014 Best Background score Devi Sri Prasad
|    
|- 3
|Best Title Song Devi Sri Prasad
|  
|-
|}

== External links ==
*  

== References ==
 

 

 
 
 
 
 
 
 
 