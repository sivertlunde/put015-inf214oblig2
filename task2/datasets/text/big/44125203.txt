Here Comes Trouble (film)
{{Infobox film
| name           = Here Comes Trouble
| image          = Here Comes Trouble poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Fred Guiol
| producer       = Fred Guiol 
| screenplay     = George Carleton Brown Edward E. Seabrook 	
| starring       = William Tracy Joe Sawyer Emory Parnell Betty Compson Joan Woodbury
| music          = Heinz Roemheld
| cinematography = John W. Boyle 	
| editing        = Art Seid 
| studio         = Hal Roach Studios
| distributor    = United Artists
| released       =  
| runtime        = 55 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Here Comes Trouble is a 1948 American comedy film directed by Fred Guiol and written by George Carleton Brown and Edward E. Seabrook. The film stars William Tracy, Joe Sawyer, Emory Parnell, Betty Compson and Joan Woodbury. The film was released on March 15, 1948, by United Artists.  

==Plot==
 

== Cast == 
*William Tracy as Dorian Dodo Doubleday
*Joe Sawyer as Officer Ames
*Emory Parnell as Winfield Windy Blake
*Betty Compson as Martha Blake
*Joan Woodbury as Bubbles LaRue Paul Stanton as Attorney Martin Stafford
*Beverly Lloyd as Penny Blake 
*Patti Morgan as Ester Dexter
*Thomas E. Jackson as Chief McClure

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 