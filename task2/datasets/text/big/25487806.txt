Paper Chasers
{{Infobox film
| name           = Paper Chasers
| image          = Paperchasersonesheet.jpg
| caption        = DVD cover
| director       = Maxie Collier
| producer       = Holly Becker Maxie Collier Yvette Plummer
| writer         = Maxie Collier Nivea Prince Prince Paul Chris Robinson Russell Simmons Sway Calloway
| music          = Charlie "Parker" Bucknall
| cinematography = Eric McClain
| editing        = Maxie Collier Davey Frankel Ethan Lader Brad Mays
| distributor    = Independent Film Channel Koch/E1
| released       =  
| runtime        = 87 minutes
| country        = United States
| awards         =
| language       = English
| budget         =
| gross          =
}}
Paper Chasers is an American independent documentary film produced by Holly Becker and Yvette Plummer, and directed by Maxie Collier.

==Plot== Chris Bridges, 57th Dynasty), had its world premiere at the 2003 Tribeca Film Festival, and was subsequently released on DVD in 2005 by Koch Home Video Entertainment One the and broadcast numerous times on the Independent Film Channel (IFC). 

Between 1999 and 2008, Collier conducted more than 200 interviews with entrepreneurs, artists, and executives across the country. In 2012, he launched Paper Chasers TV to share these archive and prepare for the 10 year anniversary film Paper Chasers 2.

==References==
 

==External links==
* 
* 

 
 
 
 
 

 