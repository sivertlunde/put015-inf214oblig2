Chinnodu
{{Infobox Film |
  name     = Chinnodu |
  image       = |
    writer      = Kanmani |
  starring        = Sumanth Charmme Rahul |
  director        = Kanmani |
  editing         = Kotagiri Venkateswara Rao |
  producer        = Katragadda Lokesh C.V Srikanth |
  music           = Ramana Gogula |
  Cinematography  = Jaswanth
  released        = 27 October 2006 | Telugu |
  budget      = |
}} Tollywood film that stars Sumanth, Charmme, and Rahul. It is directed by Kanmani (director). It was a moderate success at the box office. It was later dubbed into Hindi under the title Ek aur Ilzaam.  It was later copied into Dhallywood under the title Nishpap Munna starred Shakib Khan and Shahara.

== Plot ==

Chinna (Sumanth) is born in jail to a mother who dies during childbirth. The Jailer (Chandra Mohan) feels sorry for Chinna and adopts him. He brings up Chinna along with his son, Sanjay (Rajiv Kanakala) and daughter. In a dramatic situation, Chinna kills the Jailer’s brother and is put in jail. Upon release, the jailer and his family tell Chinna to stay away from them.  So Chinna moves into a rough neighborhood that happens to be under the control of the local mafia there. He overpowers  the mafia there eventually and becomes a protector for the people there. A clerk in the commissioner’s office (Charmy) enters as a co-tenant in Chinnas house. Love sprouts between them. However, she mistakenly assumes that Chinna is a soft natured guy. Shes unaware of his reputation in that locality nor of his murderous past. Their relationship breaks when she finally discovers the truth about Chinna. How do they get back together? Why did Chinna kill the jailer’s brother?  Will Chinna become a part of the jailer’s family again?  All these questions are answered towards the end when the shocking truth is revealed by the Jailers wife.

==External links==
* 

 
 
 


 