Budrus (film)
 
{{Infobox film
| name           = Budrus
| image          = budrus.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Julia Bacha
| producer       =  
| writer         = Julia Bacha
| starring       =  
| music          = Kareem Roustom
| cinematography = Julia Bacha
| editing        =  
| studio         = Just Vision
| distributor    = Balcony Releasing Typecast Releasing
| released       =  
| runtime        = 70 minutes United States}}
| language       =  
| budget         = 
| gross          = 
}}
Budrus is a 2009  , March 11, 2004.  Amira Haas,  , Haaretz, March 11, 2004. 

Budrus debuted on the festival circuit at the Dubai International Film Festival on December 13, 2009.  Its theatrical release was on September 24, 2010,  in the UK.  and on October 8, 2010, in the United States (New York).    

==Plot== The Jewish Daily Forward states that:
:Budrus   a documentary by Julia Bacha that examines one West Bank town’s reaction to Israel’s construction of the security barrier. The town, with a population of 1,500, was set to be divided and encircled by the barrier, losing 300 acres of land and 3,000 olive trees. These trees were not only critical for economic survival but also sacred to the town’s intergenerational history. The film tells the story of Ayed Morrar, a Palestinian whose work for Fatah had led to five detentions in Israeli jails, but whose momentous strategic decision that the barrier would be best opposed by nonviolent resistance had far-reaching ramifications. 

==Cast== Occupied Palestinian Territories and demonstrate in support of his village."    soldiers or Israeli settlement|settlers." 
*Kobi Snitz:   who participated in the demonstrations in Budrus.
*Ahmed Awwad: A member of Hamas who aided Ayed in promoting "nonviolence as a strategic tool, best suited to achieve the village’s aims." In response to the involvement of Israeli activists in the Budrus demonstrations, Awwad stated, "We had already heard that there were some Israelis who wanted peace with the Palestinians. But these demonstrations exceeded expectations. . . . In these marches I saw these Israeli voices in real life; it wasn’t just something I heard about." 
*Yasmine Levy: Squad commander of the Israel Border Police who "develops a complex relationship with the women in the village who call her by name in their chants." 
*Doron Spielman: Spokesman for the Israeli army who doesnt believe that the non-violent protests will result in change.

==Themes and background==
{| class="toccolours" style="float: left; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | Nonviolent, civil disobedience is not new to the Palestinian territories. It has been used, historians say, in various forms and with mixed results from the early 1920s through the second intifada. But what makes the Palestinian peaceniks of Budrus different is that they explicitly define their movement in opposition to violence, condemning even stone—throwing, long a symbol of Palestinian resistance. Also unique: women in Budrus asked to march at the front of the protests, as did Israeli and international peace activists. All, analysts say, complicated the efforts of the Israeli security forces, which resorted to using force against unarmed women, foreign activists, and Israeli civilians.
|-
| style="text-align: Right;" | – R.M. Schneiderman and Joanna Chen, Newsweek 
|}
In 2004,  , June 14, 2004.  In response, the residents began to hold non-violent protests. Haaretz stated that although curfews were established to prevent the protests, "mainly young men violated the curfew and walked to the olive grove, to prevent the bulldozers from doing their work. Up to this week, the bulldozers have not returned to work – after they already uprooted about 60 olive trees." Amira Haas,  , Haaretz, November 2, 2004. 

In a 2010 interview with The Jewish News Weekly of Northern California, Budrus producer Ronit Avni stated that the film was made in response to questions concerning the existence of Palestinian non-violence movements. She further argued that, "often the phrase that followed   question was something along the lines of, ‘If only Palestinians adopted nonviolence, there would be peace.’ The film explores what it looks like when a Palestinian nonviolence movement emerges. And what is the Israeli response?"  In another interview with the Montreal Mirror, she added that after investigating instances of Palestinians and Israelis working together towards peace through non-violence, "All of our research kept leading us back to Budrus   And most Israelis and Palestinians did not know the story of this village." 

==Release==
Budrus initially debuted on the festival circuit in 2009 at the Dubai International Film Festival.     It also played at the Tribeca Film Festival. Jordana Horn,  , The Jewish Daily Forward, April 27, 2010. 

Budrus was the Official Selection at film festivals worldwide including the International Thessaloniki Film Festival, Hot Docs Canadian International Documentary Festival, Sydney Film Festival, Dokufest, Rhode Island International Film Festival, EBS International Documentary Festival, Festival do Rio, Bergen International Film Festival, Mumbai International Film Festival, and The St. Louis International Film Festival. 

It has screened at other festivals such as the San Francisco Jewish Film Festival  and the 14th Annual DocuWeeks film festival in Los Angeles and New York City. 

==Reception==

===Critical response===
As of January 15, 2011, Budrus received on Rotten Tomatoes an overall rating of 95% from all critics (19 fresh and 1 rotten). 
 Little White Lies – Independent Film Magazine gave Budrus three out of five stars and states, "By incorporating interviews with an Israeli Border Police captain and a spokesman from the military police, Budrus presents a relatively balanced view of the dispute, particularly for those unfamiliar with the regional politics. And yet from the very beginning, it’s difficult to fathom how one country can enter a region and declare parts of it their own, without expecting some kind of resistance. The people of Budrus, and their choice of peaceful protest, can only be admired."  Derek Malcolm of The Evening Standard gave the film four out of five stars and notes that the "film seems designed to prove what can be done by relatively peaceful means. But it is at its best when we see how Morror gradually moves into a position of moral strength. You can scarcely believe the foolishness of the Israeli authorities, but you never get the sense that the film is taking sides."  In addition, Budrus received four stars from The Daily Telegraph  and the Financial Times. 
 Slant Magazine Time Out New York gave the film two out of five stars and argued that, "documentaries warrant viewpoints, but they should also provide perspective on more than one side of a contentious issue." 

===Accolades===

====2011====
*Won: Amnestys Matter of Act Human Rights Award, Movies That Matter Film Festival
*Won: Jury Award for Excellence in Documenting a Human Rights Issue, Bellingham Human Rights Film Festival
*Won: The Henry Hampton Award for Excellence in Film and Digital Media
*Won: The Ridenhour Documentary Film Prize

====2010====
*Won: Tribeca Film Festival – Special Jury Mention
*Won: 60th Berlin International Film Festival –  Panorama Audience Award Second Prize
*Won: Bahamas International Film Festival – Spirit of Freedom Documentary Award
*Won: San Francisco International Film Festival – Audience Award
*Won: Jerusalem Film Festival –  Honorable Mention for Best Documentary in the Spirit of Freedom Award
*Won: Silverdocs –  Witness Award
*Won: Traverse City Film Festival –  Founders Prize, Best of Fest, Nonfiction
*Won: Bergen International Film Festival –  Checkpoints Award
*Won: Documenta Madrid 10 – Honorable Mention of the Jury
*Won:  Festival des Libertés – Festival des Libertés Prize Pesaro Film Festival – Amnesty Italia Award   

====2009====
*Cultural Bridge Gala, 6th Dubai International Film Festival  

==See also==
*Budrus

==Further reading==
* , October 14, 2010.
*Fadi Elsalameen|Elsalameen, Fadi. " ." Huffington Post, June 18, 2010.
*indieWire. " . April 22, 2010.
*Morrar, Ayed. " ." Huffington Post, November 1, 2010.
*Romero, Angie. " . February 23, 2010.

==References==
 

==External links==
*  
*  
*   – slideshow by Der Spiegel

 
 
 
 
 
 
 
 
 
 
 