The Exam
{{Infobox Film
| name           = The Exam
| image          = sinavposter.jpg
| image_size     =
| caption        = 
| director       = Ömer Faruk Sorak
| producer       = 
| writer         = Yiğit Güralp
| narrator       = 
| starring       =   
| music          = Ozan Çolakoğlu
| cinematography = 
| editing        = 
| distributor    = Böcek Film
| released       =  
| runtime        = 120 minutes
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

The Exam ( ) is a 2006 Turkish  comedy-drama film directed by Ömer Faruk Sorak, about five Turkish highschool students preparing to sit for the university entry exam, who enlist the services of a professional thief, played by Jean-Claude Van Damme, to steal the papers. The film went on nationwide general release across Turkey on  .

== Plot ==
Five friends (Mert, Sinan, Gamze, Kaan and Uluç) are set to sit for the daunting nationwide university entry exam. Desperate to get a passing grade, they try to pool their resources together and enlist the services of a professional thief/assassin (Charles) for hire to steal the papers.

All five of them have good reasons to do so. For Mert, it is essential to get into university, as he promised to his mother dying of cancer. He is the head of the family, working hard to make a living, even stopped attending school. Because of that, Mert surely cannot pass the entrance exam, so he needs to find other ways. He is in love with Gamze, whom he had cheated on before and so the girl doesnt approve of him anymore. Gamzes parents are in a continuous fight with each other which frustrates the young girl, making her want to flee from home - she needs to get into a university. Sinans police officer father treats him badly, sure if he cannot pass the exam his cruel father would beat him up. The honest and good student Kaan is under pressure also by his family who want him to be a doctor - but no matter much he works, it is never enough for his maximalist father, not to mention Kaan doesnt want to be a doctor but rather would like to become a bass guitarist. Uluçs father is always telling his son to study, not to become a poor man like himself. 

First the friends try the legal way of getting things done: study. But however hard they study, they cannot please their parents. It seems to be impossible to study for their lessons and for the exam as well, which demands different knowledge. They - and especially outcast Mert - are also harassed by the corrupt headmaster, Rafet, who only wants to make as much money out of extra lessons given to the students, as much is possible. The only person who wants to do good for the school is Zeynep, the assistant of the headmaster.

The friends finally decide to steal the Mathematics test from the teachers bag, when she is out to lunch. They precisely plan the operation and finally succeed. Nonetheless they discover that this wont help them at the ÖSS, the university entrance exam. So Sinan, frustrated by his policeman fathers corruptness, tells the others to plan the theft of the exam papers as well. First they visit a former student of their school, Levent, who has become a famous and wealthy man and was rumoured to have been close to steal the ÖSS exam papers. He senses their plans and tells them off, offering the worried Zeynep his help to teach a lesson to the guys. So the heroes have to find another way. 

Uluç tells them a story how he saved the life of a British secret agent when he was a little boy and how he got a cup from this Charles as a reward, telling Uluç that he "is obliged to him". So Uluç keeps sending emails to every Charles in Britain hoping to find the agent. Finally he gets an answer from the Charles he was seeking. The friends start collecting money (by doing various jobs) to pay for Charless expenses and invite him to steal the exam papers. Charles (Jean-Claude Van Damme) arrives and tells his plan of the theft. He enters the building at night and manages to steal the documents and give them to the overjoyed students. Kaan, however, refuses to have a look at the papers, he says that his sense of honesty doesnt allow this. He had studied hard and done the questions of the previous years exam. The others start memorising the answers and finally the "great day" arrives and they sit for the exam. The parents are waiting outside for the results, proudly watching their children enter the exam building. As soon as they start reading the questions they realise that they are different from what Charles had given them. It turns out that Charles is really Jean-Claude Van Damme (as Kaan had suspected when the plane landed saying: "He looks like Jean-Claude Van Damme, are you sure this is Charles?"), and that he was asked to play for the role of Charles by Levent, the wealthy former student of their school, as he and Jean-Claude are old friends. So the students - except Kaan, who really did study - are taught a very difficult lesson about honesty and fairplay. In the end we see Merts mother fall off the bench in the park opposite the window at which Mert is sitting. Mert hurriedly leaves the exam and runs out only to find his mother dead. The film ends with Mert taking his dead mother into his arms.

Throughout the film we can see moments of a horserace - the rush for the university entrance is like a race for horses where only the best fed and best trained horses can win and the jockeys do everything to rush the horses, just like the teachers and parents push the children. 

The film shows how important the university entrance exam is in  ? Because their minds might awake".

== Cast ==
* Yağmur Atacan (Sinan)
* İsmail Hacıoğlu (Mert)
* Rüya Önal (Gamze)
* Tuba Büyüküstün (Zeynep Erez)
* Caner Özyurtlu (Kaan)
* Altan Erkekli (Almancı Sedat)
* Volkan Demirok (Uluç)
* Güven Kıraç (Rafet 69)
* Altan Erkekli (Almancı Sedat)
* Itır Esen (Anna)
* Okan Bayülgen (Levent)
* Hümeyra (Ms. Fatma)
* Jean-Claude Van Damme (Charles)

== External links ==
*  
*  

 
 
 
 
 
 
 