Anna and the King of Siam (film)
 
{{Infobox film 
| name           = Anna and the King of Siam
| image          = Anna and the king of siam75.jpg
| caption        = Original U.S. Poster John Cromwell	
| producer       = Louis D. Lighton
| screenplay     = Talbot Jennings Sally Benson
| based on       =  
| starring       = Irene Dunne Rex Harrison Linda Darnell
| music          = Bernard Herrmann	
| cinematography = Arthur C. Miller
| editing        = Harmon Jones
| distributor    = Twentieth Century Fox
| released       = June 20, 1946 (U.S. release)
| runtime        = 128 min
| language       = English
| budget         =
}}
 John Cromwell. novel of Siam (now galleys and immediately bought the film rights.
 Victorian values Best Cinematography Best Art Direction (Lyle R. Wheeler, William S. Darling, Thomas Little, Frank E. Hughes).    Nominations also went to Bernard Herrmanns score, to the screenplay and to supporting actress Gale Sondergaard.
 film of the same name. American film director Andy Tennant remade the film in 1999 as Anna and the King with Jodie Foster and Chow Yun-fat.

The portrayal of Tuptim in Anna and the King of Siam, is considerably less sympathetic than in the musical version The King and I, as the 1946 film shows animosity between Tuptim and Anna, while the musical makes her into a romantic character.  Also, Tuptim is ultimately executed cruelly by the king, following an episode in Leonowenss book, while in the musical, her fate is made ambiguous. Ma, Sheng-mei. "Rodgers and Hammersteins Chopsticks musicals". Literature/Film Quarterly, Vol. 31, Number 1 (2003), pp. 17–26. 

==Plot summary== Richard Lyon) arrive in Bangkok in 1862 to tutor the Kings (Rex Harrison) children. She believes she is sufficiently acquainted with Asian customs to know what is proper in Siam, having read a book summarizing the same. However, when the Kralahome or Prime Minister (Lee J. Cobb) comes out to welcome her, he asks her a number of personal questions, and she does not know that this is common courtesy in Siam. Her letter from the King asking her to come to Siam includes a promise that she will have a house of her own away from the Palace, but the Kralahome says she will have to stay in the harem for now (although shell have a private room there).
 prostrate oneself before the King; Anna refuses, and says she will bow as she would to her own Queen.

Mongkut (Rex Harrison) challenges her with personal questions; she responds with nonsense answers. Liking her spirit, he introduces her to his many wives and his 67 children, asking that she instruct the wives in English as well as the children. She is enchanted, but reminds him that he promised her a house. He refuses to remember that he promised such a thing and insists she live in the palace, where she will be more accessible in case students (or himself) have questions. When she insists, she is shown a sleazy house in the fishmarket, but rejects it and stays in the palace, starting her school there. Lady Thiang, the head wife (Gale Sondergaard) knows English and translates. Among other things, Anna teaches proverbs and songs about promises and home or houses. Soon even the royal secretary is singing "Home! Sweet Home!" under his breath as he works.

Meanwhile the Kralahome comes in and tells Mongkut that Cambodia, once a part of Siam, has sold out to the France|French, who have established a protectorate. The King says his plan is to hold onto Siam, to save what he can. He finally cedes to Anna on the matter of the house; she likes it, but plans to leave. However, the Kralahome tells her to stay, because Mongkut is a complex man who needs her influence.

Mongkut begins summoning Anna in the middle of the night to discuss how the Bible should be interpreted, and other scholarly matters. On the way back from one of these sessions, she discovers a chained slave with a baby. This is LOre, who belongs to Lady Tuptim (Linda Darnell), the new favorite. Tuptim is very young and very bitter about being brought to the Palace and shut up behind the walls, even though the King likes her. She refuses to let LOre go, even though LOres husband has offered to pay for her. As he has done several times in the past, Crown Prince Chulalongkorn (Tito Renaldo) questions her about these matters, but she puts him off. Lady Thiang, the crown princes mother, is concerned, but Anna gives her the brush-off too, saying they will talk "later, when she has time".

Anna tells the King about LOre, reminding him that its his own law that slaves must be freed if the money is offered. This law protects all. The King asks if Queen Victoria is above the law. Anna explains that she is not and neither is President Lincoln. She tells about the fight against slavery in America, and about the Civil War. He writes to Lincoln offering to send pairs of elephants that can be used as army transport (an actual incident); Lincoln writes back, thanking him for the offer but explaining that elephants would not do well in American climates. Tuptim shows Anna a jeweled glass pomegranate the King gave her for freeing her slave, but then believes that the King listened to Anna about this, not to her. "If I am not first here, what is left for me?"

Mongkut expects English visitors and asks Anna to dress some of his prettiest wives in European style and to provide English-style decor and utensils to show that he is not a barbarian. Much is at stake - foreign papers have written very biased things about Siam, and Britain is thinking about establishing a protectorate. Anna suggests that the King invite consuls to come from other countries at the same time. The party is a great success, combining British, European and Siamese traditions and convincing the visitors that Siam is indeed a civilized nation with a very old and very proud history.

Lady Tuptim, whos been missing for some time, is found in a Buddhist temple, disguised as a young man. She is put on trial and explains; she couldnt stand being shut up, and so disguised herself and went to the monastery because she had nowhere else to go. She was accepted as a novice and studied with Phra Palat (Neyle Morrow), her former fiancé, whod taken holy vows when Tuptim was presented to the king. No one believes that she was simply in disguise and that Phra Palat had no idea who she was.

Anna runs to the King and begs his help, but hes very insulted that Anna even knows about what happened—its a private matter as well as something that harms his dignity. Anna unwisely loses her temper and tells the king he has no heart and that hes a barbarian. Protesting her innocence and Phra Palats, Tuptim is burned at the stake and he with her.

Anna decides that she has had enough and says goodbye to the children. The royal wives read her a letter pleading with her to stay. Lady Thiang is disappointed with Anna, explains her life story through the illustrations on her wallpaper, and says that the crown prince may not grow up to be a good king if Anna doesnt stay to educate him. At the same time, Annas own son dies in a riding accident. The Kralahome comes to her and reads a proclamation from the King granting the child royal funeral honours. He explains that the King does this by way of apology for what happened with Tuptim. But when the King asks Anna to continue secretarial duties, she says "Its the children I want," and goes on with her school.

The British open a consulate in 1865, the French in 1867, and the USA in 1870. Many years pass, and the crown prince is now a young man. Anna is summoned to the bedside of the King, who is dying. The King says that Anna spoke the truth to him and was a good influence on the children. He expresses his gratitude and dies. The Kralahome asks Anna to stay and help the prince. When Chulalongkorn is crowned, his first act is to abolish the prostration, so that everyone can respect each other and work together.

==Cast==
* Irene Dunne as Anna Leonowens
* Rex Harrison as King Mongkut
* Linda Darnell as Tuptim
* Lee J. Cobb as Kralahome
* Gale Sondergaard as Lady Thiang
* Mikhail Rasumny as Alak
* Dennis Hoey as Sir Edward
* Tito Renaldo as The Prince (older) Richard Lyon as Louis Leonowens

==Historical inconsistencies==
* Anna was Anglo-Indian, and raised in India, not Welsh, as she claimed; she had never even visited Britain before becoming a governess in the court of Siam. Also, she was the widow of a civilian clerk and hotel-keeper, not a British army officer.
 Anna and the King of Siam, which was in turn based on Leonowens somewhat fictionalised accounts of her experiences. Landon further fictionalised the story and, like Leonowens herself, made up incidents to make the story more accessible. Both women were dedicated to the womens rights movement and thus present a distorted, prejudiced view of Mongkut and Siamese palace life. To correct the record, well-known Thai intellectuals Seni Pramoj and Kukrit Pramoj wrote The King of Siam speaks in 1948. (ISBN 9748298124)

* Tuptims torture and execution by burning at the stake is disputed by a great-granddaughter of the King who claimed also to be Tuptims granddaughter.

* Mongkut really did write a letter to Washington offering elephants to be used as stock for breeding American elephants, but the offer was unrelated to the Civil War. His letter, accompanied by some gifts, was addressed to President Buchanan during the last month of his term, "or to whomsoever the people have elected anew as Chief ruler in place of President Buchanan".  The response, dated almost a year later, came from Lincoln, thanking the king for the gifts and good wishes but declining the elephants on the grounds that the latitude of the US made raising elephants impractical. 
 Thai traditional court music in the mahori style, particularly the use of the ranat xylophone.

* Annas son Louis dies as a child in riding accident in the film, and Annas decision to remain in Siam is prompted both by the Kings sincere regret for her loss and her own maternal instincts: Prince Chulalongkorn becomes a sort of foster son for Anna. The historical Louis Leonowens did not die as a child, and in fact outlived his mother. 

* In the film, Anna is present at the death of King Mongkut. The historical Anna had been granted a leave of absence for health reasons in 1867 and was in England at the time of the Kings death in 1868; she was not invited to resume her post by the new king.

==References==
 

==External links==
*  
*  
*  
*  
*   on Lux Radio Theater: January 20, 1947

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 