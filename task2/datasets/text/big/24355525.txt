Zone Troopers
{{Infobox Film
| name         = Zone Troopers
| image        = Zone Troopers 1985 Poster.jpg
| caption      = Theatrical film poster
| director     = Danny Bilson
| writer       = Danny Bilson Paul De Meo
| starring     = Tim Thomerson Timothy Van Patten Art LaFleur Biff Manard William Paulson
| producer     = Roberto Bessi
| cinematography = Mac Ahlberg
| music        = Richard Band
| editing      = Ted Nicolaou
| studio       = 
| distributor  = Empire International Pictures
| released     = October 1, 1985
| runtime      = 88 min.
| country      = United States Italy
| language     = English
| budget       = 
| gross        = 
}}

Zone Troopers is an American 1985 science fiction film, directed by Danny Bilson and starring Tim Thomerson. The original music score was composed by Richard Band.

==Plot==
In Italy during World War II, an American military patrol discovers a spaceship that has crash-landed in the woods, along with its alien crew. A nearby Nazi unit also sends a patrol to investigate the crash and to capture the aliens if possible.

==Availability==
The film was released on videocassette by Lightning Video in 1986. The film has never been released on DVD but in 2005, due to the films cult following, Metro-Goldwyn-Mayer renewed the copyrights to the film and aired a widescreen print of the film on Showtime (TV network)|Showtime, fueling speculation that a DVD release may be possible in the near future. The film can be viewed on Netflixs "Watch Instantly" service.

The movie will be released on a manufactured-on-demand DVD-R by MGM as part of their Limited Edition Series on December 6, 2011.

Shout! Factory announced the retail version of Zone Troopers on DVD.

==Development==
This film reunited a good number of the cast and creators of the 1985 film Trancers.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 


 