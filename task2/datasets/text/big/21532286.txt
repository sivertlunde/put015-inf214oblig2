Beder Meye Josna
 
{{Infobox film
| name = Beder Meye Josna  Beder Meye Jyotsna বেদের মেয়ে জোসনা (মূলত জন্মস্থান বাংলাদেশে)
| image =Beder Meye Josna.jpg
| image size=
| caption = DVD cover of Movie Beder Meye Josna
| director =Motiur Rahman Panu
| writer =
| starring =ইলিয়াস কাঞ্চন অন্জু মিঠুন ফারজানা ববি সাইফুদ্দিন নাসির খান শওকত আকবর প্রবীর মিত্র রওশন জামিল দিলদার
| producer =Jai Khemka Ajoy Films
| distributor =
| cinematography =Rafiqul Bari Chowdhury
| editing =
| released = 23 January 1991
| country        =Bangladesh
| preceded by    =
| followed by    =
| runtime = 120 minutes Bengali
| music = Abu Taher
}}
 Bengali film Bengali film. 
   The original movie is also reputed to be the highest grossing Bangladeshi film of all time. 

==Plot==
In the Bengali language, "Bede" means a caste or group of people who make their living by catching 
snakes and entertaining people by making the snakes dance to the tune of there flutes. 
Joytsna (Anju Ghose) is a girl from this community. One day a poisonous snake bites the 
foot of a local prince (Chiranjit). A bede is called to cure the prince. He sees the wound and declares that only Jyotsna can extract the poison from the princes blood. The king calls Jyotsna and asks her to save his son, in exchange for which he agrees to give her anything she wants. Jyotsna cures the prince but becomes ill in the process. After her mother and the queen pray for her, she recovers and demands the hand of the prince as her reward, but the king balks. When the prince, now recovered, comes to know of everything that has transpired, he falls in love with Jyotsna. After a long tug-of-war, the couple persuade the king to consent to their union and they marry.

This story was taken from a very old rural Bengali play of the same name. The tune of the title song, "Beder meye Jyotsna amay katha Phagun (1958).

==Cast==
* Chiranjit
* Abhishek Chatterjee
* Nasir Khan
* Shambhu Bhattacharya
* Subhendu Chattopadhyay
* Anamika Saha Abbas
* Anju Ghosh
* Bulbul Chowdhury
* Saifuddin
* Shambhu Dildar

== Crew ==
* Director: Motiur Rahman Panu
* Producer : Jai Khemka Ajoy Films
* Presenter :
* Music Director: Abu Taher
* Cinematographer: Rafiqul Bari Chowdhury
* Editor :
* Playback Singer : Andrew Kishore, Khurshid Alam

==References==
 

==External links==
* 
* 

 
 
 
 
 
 