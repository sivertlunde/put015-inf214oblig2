Écoute voir
{{infobox film
| name = Écoute voir
| director = Hugo Santiago
| starring = Catherine Deneuve   Sami Frey   Anne Parillaud   Florence Delay
| music = Michel Portal, with additional music by Franz Schubert, Mozart, Johann Sebastian Bach
| runtime = 120 minutes
| country = France
| language = French
| released =  
}} French film, a drama-thriller,  directed by Hugo Santiago, with a screenplay by Santiago and Claude Ollier, released in 1979. It stars Catherine Deneuve, Sami Frey and Anne Parillaud.

==Synopsis==
Arnaud de Maule, young lord of the manor, distinguished scholar, has resort to the detective Claude Alphand (Catherine Deneuve), so that she might make enquiries into some individuals who are getting into his estate in Yvelines. Claude discovers  that it is a question of the members of a strange sect, the Church of the Final Revival, which has recently won over Chloé, the young mistress of Arnaud.

==Cast==
* Catherine Deneuve : Claude Alphand
* Sami Frey : Arnaud de Maule
* Florence Delay : Flora Thibaud
* Anne Parillaud : Chloé/Moune
* Didier Haudepin : Claudes secretary
* Antoine Vitez : delegate of the sect
* Jean-François Stévenin : Inspector Mercier
* François Dyrek : Inspector Daloup

==External links==
*  

 
 
 
 
 
 

 