All My Sons
 
{{Infobox play
| name       = All My Sons
| image      =
| image_size =
| caption    =
| writer     = Arthur Miller
| characters = Joe Keller   Kate Keller    Chris Keller   Ann Deever    George Deever    Frank Lubey    Lydia Lubey    Jim Bayliss    Sue Bayliss   Bert
| setting    = The Kellers yard in late August, 1946
| premiere   =
| place      =
| orig_lang  = English
| series     =
| subject    =
| genre      =
| web        =
}} play by Arthur Miller.    The play was twice adapted for film; in 1948, and again in 1987.
 Broadway at New York Arthur Kennedy, and Karl Malden and won both the Tony Award for Best Author and the Tony Award for Best Direction of a Play. The play is also known for being the inspiration for the name of the band Twenty One Pilots.

==Background==
Miller wrote All My Sons after his first play The Man Who Had All the Luck failed on Broadway theatre|Broadway, lasting only four performances. Miller wrote All My Sons as a final attempt at writing a commercially successful play; he vowed to "find some other line of work"  if the play did not find an audience.

All My Sons is based upon a true story, which Arthur Millers then mother-in-law pointed out in an Ohio newspaper. Meyers, Jeffrey, The Genius and the Goddess: Arthur Miller and Marilyn Monroe, University of Illinois Press, ISBN 9780252035449 (2009), pp. 92–93    The news story described how in 1941–43 the  s congressional investigative board after several Wright aircraft assembly workers informed on the company; they would later testify under oath before Congress.    In 1944, three Army Air Force officers, Lt. Col. Frank C. Greulich, Major Walter A. Ryan, and Major William Bruckmann were relieved and later convicted of neglect of duty. Hinton, Harold B., Air Victory: the men and the machines, New York: Harper & Bros. (1948), pp. 249–251   Three Air Officers Guilty of Neglect, Ordered Dismissed, The Milwaukee Journal, 26 April 1944, p. 33 

Henrik Ibsens influence on Miller is evidenced from the Ibsen play The Wild Duck, where Miller took the idea of two partners in a business where one is forced to take moral and legal responsibility for the other. This is mirrored in All My Sons. He also borrowed the idea of a character’s idealism being the source of a problem. 
 Red Scare.  

==Characters==
Joe Keller — Joe, 60, was exonerated after being charged with knowingly shipping damaged aircraft engine cylinder heads (for P-40s) from his factory during World War II|WWII, causing the deaths of twenty-one pilots.  For three and a half years he has placed the blame on his partner and former neighbor, Steve Deever.  When the truth comes out, Joe justifies his actions by claiming that he did it for his family. At the end of the play he commits suicide in a sad attempt to rid his family of the problems he has caused them and perhaps also to stop Kate from hating him.
 MIA for three years. She refuses to believe that Larry is dead and maintains that Ann Deever — who returns for a visit at the request of Larrys brother Chris — is still "Larrys girl" and also believes that he is coming back.

Chris Keller — Chris, 32, returned home from World War II two years before the play begins, disturbed by the realization that the world was continuing as if nothing had happened. He has summoned Ann Deever to the Keller house in order to ask her hand in marriage, but theyre faced with the obstacle of Kates unreasonable conviction that Larry will someday return.  Chriss idolization of his father results in his devastation when he finds out the truth about what Joe did.

Ann Deever — Ann, 26, arrives at the Keller home having shunned her "guilty" father since his imprisonment. Throughout the play, Ann is often referred to as pretty, beautiful, and intelligent-looking and as "Annie". She had a relationship with Larry Keller before his disappearance, and has since moved on because she knows the truth of his fate. She hopes that the Kellers will consent to her marriage to Larrys brother, Chris, with whom she has corresponded by mail for two years. Ann soon finds out that the neighbors all believe that Joe is guilty, and eventually finds out the truth after a visit from her older brother George. Ann is the knowledge-bearer in the play: finally, unable to convince Kate that Larry is gone forever, Ann reveals a letter from Larry stating his intention to commit suicide having learned of his father’s implied guilt.
 WWII veteran, and a childhood friend of Chris.  He initially believed in his father’s guilt, but upon visiting Steve in jail, realizes his innocence and becomes enraged at the Kellers for deceiving him. He returns to save his sister from her marriage to Chris, creating the catalyst that destroys the Keller family.

Jim Bayliss — Jim is a successful doctor, but is frustrated with the stifling domesticity of his life. He wants to become a medical researcher, but continues in his job as it pays the bills. He is a close friend to the Keller family and spends a lot of time in their backyard.

Sue Bayliss — Sue is Jims wife: needling and dangerous but affectionate, she too is a friend of the Keller family, but is secretly resentful of what she sees as Chriss bad idealistic influence on Jim. Sue confronts Ann about her resentment of Chris in a particularly volatile scene, revealing to Ann that the neighbors all think Joe is guilty.

Frank Lubey — Frank, 33, was always one year ahead of the draft, so he never served in World War II, instead staying home to marry Georges former sweetheart, Lydia.  He draws up Larrys horoscope and tells Kate that Larry must still be alive, because the day he died was meant to be his "favorable day".  This strengthens Kates faith and makes it much harder for Ann to reveal the letter to her.

Lydia Lubey — Lydia, 27, was Georges love interest before the war; after he went away, she married Frank and they quickly had three children. She is a model of peaceful domesticity and lends a much-needed cheerful air to several moments of the play.

Bert — Bert is a little boy who lives in the neighborhood; he is friends with the Bayliss son Tommy and frequently visits the Kellers yard to play "jail" with Joe. He only appears twice in the play. The first time he appears, his part seems relatively unimportant, but the second time he appears his character gets more important as he sparks a verbal attack from mother when mentioning "jail," which highlights Joes secret.

===Unseen characters===
Larry Keller — Larry has been MIA for some years at the start of the play. However he has a significant effect in the play through his mothers insistence that he is still alive and his brothers love for Larrys childhood sweetheart, Ann. Comparisons are also made in the story between Larry and Chris; in particular, their father describes Larry as the more sensible one with a "head for business". At the end of the play, Ann reveals a letter written by Larry saying that, out of shame for his fathers actions, he intends to commit suicide.

Steve Deever — George and Anns father. Steve is sent to prison for the shipping of faulty aircraft parts — a crime which not only he but also the exonerated Keller committed.

==Synopsis==

===Act I===
In August 1946, Joe Keller, a self-made businessman, and his wife Kate are visited by a neighbor, Frank. At Kates request, Frank is trying to figure out the horoscope of the Kellers missing son Larry, who disappeared three years earlier. While Kate still believes Larry is coming back, the Kellers other son, Chris, believes differently. Furthermore, Chris wishes to propose to Ann Deever, who was Larrys girlfriend at the time he went missing and who has been corresponding with Chris for two years. Joe and Kate react to this news with shock but are interrupted by Bert, the boy next door. In a game, Bert brings up the word "jail", making Kate react sharply. When Ann arrives, it is revealed that her father, Steve Deever, is in prison for selling cracked cylinder heads to the Air Force, causing the deaths of twenty-one pilots in plane crashes. Joe was his partner but was exonerated of the crime. Ann admits that neither she nor her brother keep in touch with their father any more and wonders aloud whether a faulty engine was responsible for Larrys death. After a heated argument, Chris breaks in and later proposes to Ann, who accepts. Chris also reveals that, while leading a company, he lost all his men and is experiencing survivors guilt. Meanwhile, Joe receives a phone call from George, Anns brother, who is coming there to settle something.

===Act II===
Although Chris and Ann have become engaged, Chris avoids telling his mother. Their next door neighbor Sue emerges, revealing that everyone on the block thinks Joe is equally guilty of the crime of supplying faulty aircraft engines. Shortly afterwards, George Deever arrives and reveals that he has just visited the prison to see his father Steve. The latter has confirmed that Joe told him to cover up the cracked cylinders and to send them out. George insists his sister Ann will not marry Chris Keller, son of the man who destroyed the Deevers. Joe Keller and the rest of his family deny this.  However, although Joes story is that, on the fateful day of dispatch, the flu laid him up, Kate reveals that Joe hasnt been sick in fifteen years. George then redoubles his attacks. Joe also seeks to defend himself by explaining that he was building a business for his sons. Chris has never accepted this and remains furious with his father.  Meanwhile, Frank announces his horoscope, implying that Larry is alive, which is just what Kate wants to hear.

===Act III===
Chris has gone missing. Reluctantly accepting the ubiquitous accusations, Kate says that, should Chris return, Joe must confess his guilt to the authorities. As he only sought to make money at the insistence of his family, Joe is stunned at the way in which they are now turning on him. Soon after, Ann emerges and reveals to Kate that she has a letter from Larry. She hadnt wanted to share it, but knows that Kate must face reality. In the letter, it is revealed that, because of his fathers guilt, Larry plans to commit suicide. With this final blow, Joe finally agrees to turn himself in, goes inside to get his coat but then kills himself with a gunshot to the head. At the end, when Chris reveals what has happened, Kate tells him to not blame himself.

==Timeline of events in the play==
The precise date of events in the play are unclear. However it is possible to construct a timeline of All My Sons using the dialogue. The action takes place in August 1946, in Midwestern United States with the main story taking place on a Sunday morning over the following 24 hours. 

*Autumn 1943: Joe encourages Steve to supply the USAAF with faulty cylinder heads for aircraft engines
*Autumn 1943: After 21 planes crash, Joe and Steve are arrested
*November 25, 1943: Having read about his fathers arrest, Larry crashes his plane off the coast of China
*1944: Joe is released
*August 1946, a Sunday morning at 4am: Larrys memorial blows down
*August 1946, the same Sunday morning: Ann arrives at the Keller home
*August 1946, the same Sunday morning: George visits Steve in prison (opening)

==Links to Greek tragedy==
 
Arthur Miller’s writing in All My Sons often shows great respect for the great Grecian tragedies of the likes of Aeschylus, Sophocles and Euripides. In these plays the tragic hero or protagonist will commit an offence, often unknowingly, which will return to haunt him, sometimes many years later. The play encapsulates all the fallout from the offense into a 24-hour time span. During that day, the protagonist must learn his fault and suffer as a result, and perhaps even die. In this way the gods are shown to be just and moral order is restored.
In All My Sons, these elements are all present; it takes place within a 24-hour period, has a protagonist suffering from a previous offense, and punishment for that offense. Additionally, it explores the father-son relationship, also a common theme in Grecian tragedies. Ann Deever could also be seen to parallel a messenger as her letter is proof of Larrys death.

In Joe Keller, Arthur Miller creates just a representative type. Joe is a very ordinary man, decent, hard-working and charitable, a man no one could dislike. But, like the protagonist of the ancient drama, he has a flaw or weakness. This, in turn, causes him to act wrongly. He is forced to accept responsibility – his suicide is necessary to restore the moral order of the universe, and allows his son, Chris, to live free from guilt and persecution. Arthur Miller later uses the everyman in a criticism of the American Dream in Death of a Salesman, which is in many ways similar to All My Sons.

==Arthur Miller quotation on All My Sons==

In his Collected Plays, Miller commented on his feelings on watching an audiences reaction to a performance of his first successful play:
 
The success of a play, especially ones first success, is somewhat like pushing against a door which is suddenly opened from the other side. One may fall on ones face or not, but certainly a new room is opened that was always securely shut until then. For myself, the experience was invigorating. It made it possible to dream of daring more and risking more. The audience sat in silence before the unwinding of All My Sons and gasped when they should have, and I tasted that power which is reserved, I imagine, for playwrights, which is to know that by ones invention a mass of strangers has been publicly transfixed. 

==Productions==
===1987 Broadway===
In 1987,  the Broadway Revival of All My Sons won the Tony Award for Best Revival of a Play starring Richard Kiley (Tony Award Nominee Best Actor in a Play), Joyce Ebert, Jamey Sheridan (Tony Award Nominee Best Featured Actor in a Play) and Jayne Atkinson. It was produced by Jay H. Fuchs and Steven Warnick in association with Charles Patsos. It was originally produced by The Long Wharf Theatre (M. Edgar Rosenblum, Executive Director, Arvin Brown, Artistic Director). The production was directed by Arvin Brown, scenic design by Hugh Landwehr, costume design by Bill Walker, lighting design by Ronald Wallace. It opened on April 22, 1987 at the John Golden Theatre and closed May 17, 1987.

===2008 Broadway===
A Broadway revival began previews at the Gerald Schoenfeld Theatre on September 18, 2008 and officially opened on October 16, 2008.  The limited engagement ran through until January 4, 2009.  
 Patrick Wilson, and Katie Holmes, in her Broadway debut. The other featured actors were Becky Ann Baker, Christian Camargo, Jordan Gelber, Danielle Ferland, Damian Young, and Michael DAddario. It was directed by Simon McBurney. According to his biography, McBurneys "work on the production of All My Sons grew out of a meeting with Arthur Miller in 2001, shortly after the playwright saw the New York premiere of Mnemonic."

The creative team consisted of scenic and costume design by Tony Award nominee Tom Pye, lighting design by Paul Anderson, sound design by Christopher Shutt and Carolyn Downing, projection design by Finn Ross, and wig and hair design by Paul Huntley.
 Anonymous staged an anti-Scientology protest at the first night of preview performances in New York City (due to cast member Katie Holmes). 

The cast dedicated their performance on September 27 to the legendary actor Paul Newman, who died the day before.

===2010 London (West End)===
David Suchet and Zoë Wanamaker (both stars of the British TV series Agatha Christies Poirot) starred in a revival production at the Apollo Theatre in Londons West End.  Suchet played Joe Keller and Wanamaker played his wife Kate.  The production also featured Jemima Rooper as Ann Deever and Stephen Campbell Moore as Chris Keller.  The show ran from May until September 11, 2010;   one performance was captured live and can be viewed online.  

===2015 UK Tour===
Ray Shell and Doña Croll led this revival by Talawa Theatre Company for a national tour of the UK.  Shell played Joe Keller and Croll played his wife Kate.  The production also featured Kemi-Bo Jacobs as Ann Deever and Leemore Marrett Jr as Chris Keller.  The tour started in February and runs until April 25, 2015;    

==Adaptations== a film Frank Conroy. 

In 1950, Lux Radio Theater broadcast a radio play of All My Sons with Burt Lancaster as Joe. The play was adapted by S. H. Barnett and, in an interesting twist, featured the character of Steve Deever in a speaking role. 

In 1958, the play was adapted for television by Stanley Mann and directed by Cliff Owen. This production starred Albert Dekker as Joe Keller, Megs Jenkins as Kate Keller, Patrick McGoohan as Chris Keller and Betta St. John as Ann Deever.

  Jack OBrien.   Unlike the 1948 version, this version refers to Georges father as Steve as in the play rather than Herb or Herbert.

In 1998, L.A. Theatre Works put on an unabridged radio production for broadcast on Voice of America and NPR.  This recording is widely available on CD and as a pay-per download. 

==See also==
*Hollywood blacklist
*Twenty One Pilots

==References==
 

==External links==
;1947 play
*  
*  
*  for GCSE exams
* 
* 
*  – All My Sons plot summary and character descriptions from  
* 
* 

;1948 film
* 
* 
Trivia:
In 1950, the stock units from the "Kellar home" sound stage sets were reconstructed on the new Colonial Street.
In 1964, Universal studios tour guides called the sets the "house used in the movie Desperate Hours (1955)" (Kellar home).
The home was used in the TV Series "Delta House" (1979).
Today the sets are located on Wisteria Lane – 4347 Wisteria Lane.
* 

;1986 film
* 
* 

;2008 play
* 
* 
* 

 
 

 
 
 
 