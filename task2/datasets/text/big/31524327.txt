The Last Coupon
The British comedy film directed by Frank Launder and starring Leslie Fuller, Mary Jerrold and Molly Lamont.  A man believes he has won the football pools and radically alters his spending habits only to find he has forgotten to post his coupon. It was based on a play by Ernest Bryan and was a success at the box office. 

==Cast==
* Leslie Fuller - Bill Carter
* Mary Jerrold - Polly Carter
* Molly Lamont - Betty Carter
* Binnie Barnes - Mrs Meredith
* Gus McNaughton - Lord Bedlington Jack Hobbs - Doctor Sinclair
* Harry Carr - Jocker
* Jimmy Godden - Geordie Bates
* Marian Dawson - Mrs Bates
* Hal Gordon - Rusty Walker

==References==
 

==Bibliography==
* Shafer, Stephen C. British popular films, 1929-1939: The Cinema of Reassurance. Routledge, 1997.
* Sutton, David R. A chorus of raspberries: British film comedy 1929-1939. University of Exeter Press, 2000.

==External links==
* 

 
 
 
 
 
 
 
 


 