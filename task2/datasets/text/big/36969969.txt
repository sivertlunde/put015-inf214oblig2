No One Lives
{{Infobox film
| name        = No One Lives
| image       = Noonelivesposter.jpg
| caption     = 
| director    = Ryuhei Kitamura Harry Knapp Kami Naghdi
| writer      = David Cohen Luke Evans Adelaide Clemens Lee Tergesen Laura Ramsey Derek Magyar Beau Knapp America Olivo Brodus Clay Lindsey Shaw
| music       = Jerome Dillon Daniel Pearl
| editing     = Toby Yates
| studio      = WWE Studios 
| distributor = Anchor Bay Films
| released    =  
| runtime     = 86 minutes 
| country     = United States
| language    = English
| budget      = $2,900,000
| gross       =  $74,918 
}} Luke Evans and Adelaide Clemens. The film premiered at the 2012 Toronto International Film Festival on September 8, 2012 and had a limited release on May 10, 2013.

==Plot== Luke Evans) encounter a gang of robbers. The group, led by dedicated criminal Hoag (Lee Tergesen), his daughter Amber (Lindsey Shaw), girlfriend Tamara (America Olivo), Ambers boyfriend Denny (Beau Knapp), and the psychopathic Flynn (Derek Magyar). Suspecting the couple to be wealthy and wanting to redeem himself for a robbery he botched, Flynn has them kidnapped and interrogated about accessing their money by Ethan (Brodus Clay) in a gas station. However, Betty commits suicide by cutting her throat on a knife Ethan had against her neck, which leads to the Driver breaking out of his handcuffs and killing Ethan.

Meanwhile, Flynn, having brought the Drivers car to the groups hideout, finds a girl in the trunk of the vehicle. Amber realizes the girl is Emma Ward (Adelaide Clemens), a wealthy heiress who disappeared after 14 of her friends were murdered at a party, and the kidnapped man is the one responsible for the massacre. Amber attempts to be kind toward Emma; however, Emma angrily spits in her face. Following Hoags orders, Denny and Tamara head to the gas station to contact Ethan, only to find his and Bettys bodies and the Driver missing. They bring Ethans corpse back to their hideout and inadvertently bring the Driver along with them, who had been hiding in Ethans body.

The Driver begins his assault on the robbers by first destroying their van and capturing Hoag, whom he later kills by dropping him into a meat grinder. After the group argues over what to do next, Denny volunteers to get their old jeep working so they can escape. Though he succeeds, the Driver shoves him into the open car engine, badly mangling his face. The Driver then chases and injures Amber, but lets her live when he realizes the surviving gang members are leaving. Nevertheless, Flynn accidentally hits Amber with the jeep when she stumbles onto the road. Emma comments on how the only one of them with a soul was killed.

After dropping Denny off at the hospital, Flynn, Tamara, and Emma head to a motel to stay the night. When Flynn uses the Drivers credit card to pay for a room, he inadvertently causes Harris, the motel owner (Gary Grubbs), to call the police, as the Driver had previously checked himself into the same motel earlier in the day. The Driver himself also arrives at the motel and nearly strangles Tamara to death in the bathroom, but stops when he hears Flynn shoot the sheriff responding to Harris call. Flynn euthanatizes Tamara when he discovers her, which leads to Emma attempting to escape. Though Flynn manages to stop her, he is promptly run over by the Driver in a police car. Emma flees into a nearby junkyard.

When the Driver confronts Emma, she beats him with a metal pipe until Flynn appears with a shotgun and shoots the Driver in the chest. The Driver survives due to his Kevlar vest and the two engage in a brutal fight. Ultimately, Flynn manages to grab his weapon, but is knocked out by Emma before he can fire it. Emma explains she wants to be one who finally kills the Driver and manages to aim the shotgun at him and pull the trigger. However, because a new shell had not been pumped into the chamber, the firearm fails to operate. Impressed, the Driver cuts out a tracking device he placed inside her stomach and announces that she is free. He then finishes Flynn off with a shotgun blast to the face and also shoots Harris for knowing his real name.

The next day, the Driver murders Denny in his hospital bed with a clipboard while disguised as a doctor. As he leaves, he notices Emma being wheeled into the hospital on a stretcher. He touches her arm before finally departing.

==Cast== Luke Evans as Driver
*Adelaide Clemens as Emma Ward
*Lee Tergesen as Hoag
*Derek Magyar as Flynn
*America Olivo as Tamara
*Beau Knapp as Denny
*Lindsey Shaw as Amber
*Brodus Clay as Ethan
*Laura Ramsey as Betty
*Gary Grubbs as Harris
*Dalton E Gray as son

==Release==
The film premiered at the 2012 Toronto International Film Festival on September 8, 2012. It was planned for a worldwide release in January 2013, however that release date was scrapped when Anchor Bay Films picked up the distribution rights to the film, they will release the film theatrically in North America, the U.K, and Australia and handle the home video release across various platforms. 
The film will  have a limited theatrical release in those countries and the new release date will be May 10, 2013. The film is Rated R for strong bloody violence, disturbing images, pervasive language, and some sexuality/nudity  On April 13, 2013, a red band trailer for the film was released.  In the US the film will be released in select cities like New York, Los Angeles, Chicago, San Francisco, Dallas, Philadelphia, Miami, Boston, Detroit, Houston and Baltimore. 

==Reception==

===Critical response===
The film premiered at the 2012 Toronto International Film Festival on September 8, 2012 and has received early mixed reviews. One review from William Brownridge states "Completely pointless and full of terrible dialogue, No One Lives still manages to be one of the most entertaining films around. This is a film that is best enjoyed among a crowd of genre fans. Extremely violent and bloody, audiences will cheer as Driver eliminates the gang one after another. The title isn’t just a suggestion, it’s a rule, and watching the madness that explodes from the screen is sure to please crowds looking for non-stop action and bloody violence."  
Another review from the Toronto Star states "While parts of it are deliberately campy, the low-budget production veers onto the amateurish and some of the jump cuts make it look like it was edited with a switchblade. Or make that a butter knife."
 
Another review from Shocktillyoudrop.com states
"Even if it’s nothing to write home about, at 78 minutes sans credits and with plenty of over-the-top carnage that will have you squirming, this is mindless entertainment done right. It gets the job done quickly and effectively before calling it a day. Sometimes that’s all one can ask for", and it gives the shock score a 6 
On rotten tomatoes.com the film has an 49% rating with 17 fresh reviews and 18 rotten reviews of the film 

===Box office===
No One Lives opened on May 10, 2013 in 53 theatres and in its opening weekend grossed $47,800 for an average of $902 per theatre. As of May 19, 2013 the film has grossed $74,918. The budget for the film was an estimated $2,900,000. However, it has performed much better than other past WWE studios limited releases including The Day, Legendary, The Reunion and The Chaperone.  

==Home media==
On June 11, 2013 it was announced that No One Lives will be released on DVD/Blu-ray on August 20, 2013. 

==References==
 

==External links==
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 