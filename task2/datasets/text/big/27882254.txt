Turk's Head
{{Infobox film
| name           = Turks Head
| image          = Tetedeturcposter.jpg
| caption        = Promotional poster
| director       = Pascal Elbé
| producer       = Françoise Galfré Patrick Godeau
| writer         = Pascal Elbé
| starring       = Roschdy Zem Pascal Elbé Ronit Elkabetz
| music          = Bruno Coulais
| cinematography = Jean-François Hensgens
| editing        = Luc Barnier
| distributor    = Warner Bros. France 2 Cinéma La Banque Postale Image 3 Sofica Europacorp Canal+ CinéCinéma Classic|CinéCinéma France Télévisions Angoa-Agicoa Fonds Images de la Diversité
| released       =  
| runtime        = 87 minutes
| country        = France
| language       = French
| budget         = €5.5m
| gross          =
}}
Turks Head (  - the expression also has the colloquial meaning of scapegoat or fall guy in French) is a 2010 French thriller film directed and starring Pascal Elbé. Roschdy Zem and Israel actress, Ronit Elkabetz also star. It was released in France on 31 March 2010. The film was inspired by the real-life incident in 2006 in Marseille where a group of impoverished teenagers torched a bus, severely burning a female passenger.   It received its international premiere in competition on 31 August 2010 at the Montreal World Film Festival, this will be followed by a theatrical release in Canada on 10 September 2010.

==Plot==
Set amidst Marseilles immigrant suburbs, a devastating chain of events unfolds when 14-year-old Turk, Bora, hurls a Molotov cocktail at a car. The occupant is an emergency doctor who has been called out to attend to a woman with cardiac problems. Regretful, the teenager rescues the injured doctor who ends up in a coma. The saga continues with the doctors brother seeking to find the perpetrator.    Montreal Gazette. 1 September 2010 

==Cast==
*Roschdy Zem as Atom
*Pascal Elbé as Simon
*Ronit Elkabetz as Sibel, la mère de Bora
*Samir Makhlouf as Bora
*Simon Abkarian as Le veuf
*Florence Thomassin as Mouna
*Valérie Benguigui as Yelda
*Monique Chaumette as Nora, la mère de Atom et Simon
*Laure Marsac as Claire, lamie de Atom
*Stéphan Guérin-Tillié as Samuel Levasseur
*Brigitte Catillon as La maire
*Gamil Ratib as Aram
*Moussa Maaskri as Irfan
*Léo Elbé as Nuri
*Adèle Exarchopoulos as Nina

==Production== Alejandro Gonzalez Inarritu. I told myself it was risky, but there was no reason not to try this in France." 

The director also researched the real-life perpetrators of a Marseille bus attack that influenced his narrative, "What shocked me most was that one year later, the youths didn’t speak up during the trial and never asked for forgiveness", explained the director. "As though their prison sentence had served no purpose. And as though our social pact had been shattered to pieces." 

Character-driven roles were an important element for Elbe, as he explained the development of his preferred character, Sybil (Elkabetz). "I think of the mother as a cowboy in a skirt. She keeps her dignity, because if she falls, everything else does. Through her, I wanted to pay homage to all those mothers in the banlieues who raise the kids alone because the husband has gone. I chose an actress who reminds me of those great Italian stars of the postwar period, like Anna Magnani." 

At a cost of €5.5m, the film was produced by Alicéleo and France 2 Cinéma co-produced the project. The film also received financial backing from the National Film and Moving Image Centre (CNC).   Cineuropa. 31 March 2010 

==Reception== The Gazette gave the "remarkable drama" 4 out of 5 stars. 

==References==
 

==External links==
* 
* 
 

 
 
 
 
 
 
 
 
 
 