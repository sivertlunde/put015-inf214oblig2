Nuummioq
{{Infobox film
| name           = Nuummioq
| image          = Nuummioq.JPG
| caption        = Theatrical poster
| director       = Otto Rosing, Torben Bech 
| producer       = Mikisoq H. Lynge 
| writer         = Torben Bech
| story          = Otto Rosing 
| starring       = Lars Rosing, Julie Berthelsen
| music          = Jan de Vroede 
| cinematography =
| editing        = Henrik Fleischer and Niels Ostenfeld 
| distributor    = The Works International 
| released       =  
| runtime        = 95 minutes
| country        = Greenland
| language       =  
| budget         = 
}}
Nuummioq is a 2009 Greenlandic drama film directed by Otto Rosing and Torben Bech and produced by Mikisoq H. Lynge. Nuummioq means "a man from Nuuk" in the Greenlandic language. Nuummioq premiered in Nuuk on 31 October 2009.

== Plot ==
Malik, a 35 year old construction worker from Nuuk discovers the love of his life at the time when he is diagnosed with cancer. He faces the choice of staying in Greenland with Nivi, the woman he has come to love − or leaving for Denmark in search of medical treatment.    

== Production ==
Nuummioq is the first feature film produced entirely in Greenland.  Filming began on 4 August 2008 in the area around Nuuk.    The script was written by Torben Bech based on an original idea by Otto Rosing.
 depression Bech later assumed directorial duties and completed the film with producer Mikisoq H. Lynge. He is co-credited Director with Otto Rosing.   

The film was produced by Mikisoq H. Lynge of 3900 Pictures, sponsored by Royal Arctic Line, the state-owned, Greenlandic freight company.  

== Cast ==
The film cast included professional and amateur actors from Greenland and Denmark:   

*Lars Rosing as Malik
*Julie Berthelsen as Nivi
*Angunnguaq Larsen as Mikael
*Maius Olsen as Sheep Farmer
*Makka Kleist as Grandma
*Amos Egede as Grandfather
*Morten Rose as Carsten
*Else Danis as Medical
*Anja Jochimsen as Tourist
*Ulrikka Holm as a Girl in bar
*Maria P. Kjærulff as Nurse
*Arnatsiaq Reimer Eriksen as Lilly
*Sorya Paprajong as Waiter at a restaurant
*Johannes Madsen, as a Woman in greenhouse
*Karsten Sommer as Customer in greenhouse

== Reception ==
Nuummioq premiered at the 2010 Sundance Film Festival selected in the World Cinema Dramatic Competition.   The reception of the film was positive, focusing on the moody atmosphere reminiscent of the works of Ingmar Bergman.  

It became the first Greenlandic film to be submitted for the Academy Award for Best Foreign Language Film,  but it didnt make the final shortlist. 

==See also==
* List of submissions to the 83rd Academy Awards for Best Foreign Language Film
* List of Greenlandic submissions for the Academy Award for Best Foreign Language Film

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 