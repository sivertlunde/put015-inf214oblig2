The Fatal Warning
 
{{Infobox film
| name           = The Fatal Warning
| image          = Poster of the movie The Fatal Warning.jpg
| image size     = 
| caption        = 
| director       = Richard Thorpe
| producer       = Nat Levine
| writer         = Wyndham Gittens
| starring       = Helene Costello Ralph Graves Mascot Pictures
| released       =  
| runtime        = 10 episodes 
| country        = United States 
| language       = Silent with English intertitles
}}
 mystery film serial directed by Richard Thorpe. The film is considered to be a lost film, with no prints known to exist.   

==Cast==
* Helene Costello - Dorothy Rogers
* Ralph Graves - Russell Thorne
* George Periolat - William Rogers
* Phillips Smalley - Leonard Taylor
* Boris Karloff - Mullins
* Lloyd Whitlock - Norman Brooks
* Syd Crossley - Dawson
* Thomas G. Lingham - John Harman
* Symona Boniface - Marie Jordan
* Martha Mattox - Mrs. Charles Peterson
* Gertrude Astor

==See also==
*List of lost films

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 

 
 