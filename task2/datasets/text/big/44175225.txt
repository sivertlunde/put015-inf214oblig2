Song of the Land
{{Infobox film
| name           = Song of the Land
| image          = 
| alt            = 
| caption        = 
| director       = Henry S. Kesler 
| producer       = Henry S. Kesler Joseph Henry Steele
| screenplay     = Joseph Henry Steele
| story          = F. Herrick Herrick Henry S. Kesler 
| starring       = 
| music          = Serge Dupré 
| cinematography = Ed N. Harrison Frances Roberts 
| editing        = Henry S. Kesler 	
| studio         = Henry S. Kesler Productions
| distributor    = United Artists
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Song of the Land is a 1953 American documentary film directed by Henry S. Kesler and written by Joseph Henry Steele. The documentary consists of film edited from over 975,000 feet of footage shot by naturalists Ed N. Harrison and Frances Roberts in North and South America over a span of several years. The film was released on November 17, 1953, by United Artists. 

==Plot==
 

== References ==
 

== External links ==
*  

 
 
 
 
 
 