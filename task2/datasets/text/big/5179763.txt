Namak Halaal
 
{{Infobox film
| name           = Namak Halaal
| image          = Namak Halaal 1982 film poster.jpg
| caption        = Film poster
| director       = Prakash Mehra
| producer       = Satyendra Pal
| writer         = 
| narrator       = 
| starring       = Amitabh Bachchan Smita Patil Parveen Babi Shashi Kapoor
| music          = Bappi Lahiri
| cinematography = 
| editing        = 
| distributor    = 
| released       = 30 April 1982
| runtime        = 164 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Namak Halaal ( ) is a 1982 Hindi language comedy film, directed by Prakash Mehra. Music is by Bappi Lahiri and lyrics by Anjaan (lyricist)|Anjaan. The film stars Amitabh Bachchan, Smita Patil, Shashi Kapoor, Parveen Babi, Om Prakash, Waheeda Rehman, Ranjeet, Satyen Kappu, Suresh Oberoi and Ram Sethi. The film became a "Blockbuster" at the box office and went on to be a golden jubilee hit (which was common to all Bachchan films at that time).
 Tamil as Velaikaran by veteran director S.P. Muthuraman in 1986 with Rajnikanth and Sarath Babu in Amitabh Bachchan and Shashi Kapoors role respectively and was a huge hit.

==Synopsis==

Young Arjun (Amitabh Bachchan) is brought up by his Daddu (paternal grandfather) (Om Prakash). His Daddu decides for him to go to the city in search of a new job and life. While in the city he meets up with Bhairon (Ram Sethi) who guides him into an interview in a 5-star hotel owned by Raja (Shashi Kapoor). 

While in the hotel, Arjun meets Poonam (Smita Patil) and falls in love with her. During this time, many attempts are made to kill Raja by Girdhar Singh (Satyendra Kapoor) and his son (Ranjeet). The blame for these murders falls on Rajas mother (Waheeda Rehman), who is actually Arjuns mother. Realising all this Arjun takes an oath, no matter what happens, he will not let any harm come to Raja. As these attempts are being made, can Arjun thwart them all?

==Cast==
* Amitabh Bachchan  as  Arjun Singh
* Smita Patil  as  Poonam
* Shashi Kapoor  as  Raja Singh
* Parveen Babi  as  Nisha
* Om Prakash  as  Dashrath Singh, Arjuns grandfather
* Waheeda Rehman  as  Arjuns mother
* Suresh Oberoi  as  Bhim Singh, Arjuns father
* Satyen Kappu  as  Girdhar Singh
* Ranjeet  as  Ranjit Singh, Manager
* Viju Khote  as  Talvar Singh
* Ram Sethi  as  Bhairon

==Soundtrack==
{{Infobox album
| Name = Namak Halaal
| Longtype = to Namak Halaal
| Type = Soundtrack
| Artist = Bappi Lahiri
| Cover = Lp-Namak Halaal.JPG
| Border = yes
| Alt = 
| Caption = Original CD Cover
| Released = 1982
| Recorded = 
| Genre = Feature film soundtrack
| Length = 
| Language = Hindi
| Label = EMI Records
| Producer =
| Last album = 
| This album = Namak Halaal (1982)
| Next album = 
}}

{{Track listing
| extra_column    = Singer(s) Anjaan
| all_music       = Bappi Lahiri
| title1          = Aaj Rapat Jaayen To | extra1 = Kishore Kumar, Asha Bhosle
| title2          = Jawan Jan-E-Man | extra2 = Asha Bhosle
| title3          = Pag Ghungroo Baandh Meera Nachi Thi | extra3 = Kishore Kumar
| title4          = Raat Baaqi Baat Baaqi | extra4 = Bappi Lahiri, Asha Bhosle
| title5          = Thodi Si Jo Pee Li Hai| extra5 = Kishore Kumar
}}

== External links ==
*  

 
 
 
 
 
 