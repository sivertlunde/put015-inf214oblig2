Pinneyum Pookkunna Kaadu
{{Infobox film 
| name           = Pinneyum Pookkunna Kaadu
| image          =
| caption        =
| director       = Srini
| producer       = M Mani
| writer         = Perumpadavam Sreedharan
| screenplay     = Perumpadavam Sreedharan Madhu Sukumari Prameela Ratheesh Shyam
| cinematography = S Kumar
| editing        = G Venkittaraman
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film,  directed by Srini and produced by M Mani. The film stars Madhu (actor)|Madhu, Sukumari, Prameela and Ratheesh in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Madhu
*Sukumari
*Prameela
*Ratheesh
*Sankaradi
*Jayamalini Latha
*Ravi Menon
*Silk Smitha

==Soundtrack== Shyam and lyrics was written by Poovachal Khader.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Enthaanu chetta nenjilakum nottam || P Susheela || Poovachal Khader || 
|-
| 2 || Kudichu njan dukhangale || K. J. Yesudas || Poovachal Khader || 
|-
| 3 || Paadaatha gaanam || Vani Jairam || Poovachal Khader || 
|-
| 4 || Sushame ninnil ushassukal kandu || K. J. Yesudas || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 


 