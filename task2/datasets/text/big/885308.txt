Laws of Attraction
 
 
 Law of attraction}}
{{Infobox film
| name           = Laws of Attraction
| image          = Laws of attraction poster.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Peter Howitt
| producer       =   Toby Emmerich Mark Gordon Elie Samaha Bob Yari Robert Harling Aline Brosh McKenna
| story          = Aline Brosh McKenna
| starring       = Pierce Brosnan Julianne Moore Parker Posey Michael Sheen Frances Fisher Nora Dunn
| music          = Edward Shearmur
| cinematography = Adrian Biddle
| editing        = Tony Lawson Intermedia Productions Deep River Irish DreamTime Initial Entertainment Group (IEG)
| distributor    = New Line Cinema
| released       =  
| runtime        = 90 minutes  
| country        = Ireland United Kingdom Germany
| language       = English
| budget         = $32 million 
| gross          = $30,016,165   
}}
 story by Robert Harling and McKenna. It stars Pierce Brosnan and Julianne Moore.

==Plot==
High-powered divorce attorneys Audrey Woods (Julianne Moore) and Daniel Rafferty (Pierce Brosnan) have seen love go wrong in many scenarios—so, how good could their own chances be? As two of the top divorce lawyers in New York, Audrey and Daniel are a study in opposites. She practises law strictly by the book; he seems to win by the seat of his pants, or by "cheap theatrics," as Audrey says in one scene. 

Soon the two lawyers are pitted against one other in several high-profile divorce cases, including a nasty public split between rock star Thorne Jamison (Michael Sheen) and his dress-designer wife, Serena (Parker Posey). The settlement hinges on an Irish castle, Caisleán Cloiche, or "Rock Castle," that each spouse wants. Audrey and Daniel travel to Ireland to chase down depositions, and both stay in the castle; although Audrey, at least, is reluctant to acknowledge their mutual attraction, they find themselves attending a romantic Irish festival together. After a night of wild celebration, they wake up the next morning to discover they have wed. Audrey is shocked, though Daniel takes their apparent marriage in his stride.

The pair return to New York and find news of their wedding printed on Page 6 of the New York Post the following day. Audrey suggests that the two maintain the semblance of a marriage for the sake of their careers, and Daniel moves into the guest room of Audrey’s apartment. Although  in the courtroom they continue to fight the Jamisons’ high-profile divorce case with the gusto they have always shown, at home they settle into domestic life together. While disposing of garbage one day, Daniel accidentally discovers some sensitive information about Audreys client, Thorne Jamison, which he reveals in the next days court proceedings. Audrey feels betrayed and asks for a divorce, which Daniel agrees to give, citing his love for her.

Next, their famous clients each return to the castle in Ireland, even though they are not permitted to be there pending division of assets. Judge Abramovitz (Nora Dunn) sends their respective counsellors to Ireland to inform them of this, but on arrival they discover that the celebrity couple has reunited on the anniversary of their wedding. Audrey and Daniel then learn that the “priest" who performed their own marriage ceremony is in fact the Jamisons butler, and the “weddings" he presided over at the festival were simply romantic celebrations.

Daniel returns immediately to New York, alone, but with Audrey fast on his heels, as she realizes that she has fallen in love with him. Confronting him in the grocery store below Daniel’s Chinatown office, Audrey asks Daniel if he is willing to fight to save their relationship. In the romantic final scenes, the couple are married in a private ceremony in Judge Abramovitz’s chambers, with Audreys mother as the sole witness.

==Cast==
* Pierce Brosnan as Daniel Rafferty
* Julianne Moore as Audrey Woods
* Parker Posey as Serena
* Michael Sheen as Thorne Jamison
* Frances Fisher as Sara Miller
* Nora Dunn as Judge Abramovitz Mike Doyle as Michael Rawson
* Allan Houston as Adamo Shandela
* Johnny Myers as Ashton Phelps
* Heather Ann Nurnberg as Leslie
* Brette Taylor as Mary Harrison
* Sara Gilbert as Gary Gadgets assistant

==Reception==
Laws of Attraction received generally negative reviews from critics, as it holds an 17% rating on Rotten Tomatoes where the site calls the film "a bland and forgettable copy of Adams Rib."  On Metacritic, the film holds a 38/100 rating, indicating "generally unfavorable" reviews. 

The film opened at No. 5 in the US box office in the weekend of 30 April 2004, raking in US$6,728,905 in its first opening weekend. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 