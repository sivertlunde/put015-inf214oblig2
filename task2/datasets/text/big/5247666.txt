The Princess Comes Across
{{Infobox film
| name           = The Princess Comes Across
| image          = Princess Comes Across poster.jpg
| image_size     = 225px
| caption        = theatrical release poster
| director       = William K. Howard
| producer       = Arthur Hornblow, Jr. Francis Martin Don Hartman Claude Binyon (uncredited) J.B. Priestley (uncredited)
| story          = Philip MacDonald (adaptation)
| based on       = Louis Lucien Rogger (novel)
| starring       = Carole Lombard Fred MacMurray
| music          = Jack Scholl Phil Boutelje
| cinematography = Ted Tetzlaff
| editing        = Paul Weatherwax
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         =
}}

The Princess Comes Across is a 1936 mystery film|mystery/comedy film directed by William K. Howard and starring Carole Lombard and Fred MacMurray, the second of the four times they were paired together.  Lombard, playing an actress from Brooklyn pretending to be a Swedish princess, does a "film-length takeoff" on MGMs Swedish star Greta Garbo. Gehring, Wes D. Carole Lombard: The Hoosier Tornado Indianapolis: Indiana Historical Society, 2003. ISBN 978-0871951670 pp.136-137 

==Plot==
Wanda Nash (Carole Lombard), an actress from Brooklyn, decides to masquerade as "Princess Olga" from Sweden in order to land a film contract with a big Hollywood studio.  On board the liner Mammoth bound for New York, she runs into King Mantell (Fred MacMurray), a concertina-playing band leader with a criminal record in his past. Both are blackmailed by Robert M. Darcy (Porter Hall), and after Darcy is killed, they become two of the prime suspects for the murder, and must find the real killer before the five police detectives traveling on the ship can pin it on them.

==Cast== 
  
*Carole Lombard as Wanda Nash / "Princess Olga"  
*Fred MacMurray as King Mantell
*Douglass Dumbrille as Inspector Lorel
*Alison Skipworth as Lady Gertrude Allwyn George Barbier as Captain Nicholls
*William Frawley as Benton
 
*Porter Hall as Robert M. Darcy
*Lumsden Hare as Inspector Cragg
*Sig Ruman as Inspector Steindorf
*Mischa Auer as Inspector Morevitch
*Bradley Page as The Stranger
*Tetsu Komai as Inspector Kawati
 

==Production==
The Princess Comes Across &ndash; which began with the working title Concertina   on   

Aside from casting concerns, filming was also delayed by the need for additional dialogue, which caused a change of directors from Harold Young to William K. Howard, and, after filming had begun in February 1936, by conflict between Howard and the producers assistant.

==Reception==
Both the film and the stars received good notices.  Variety (magazine)|Variety called her Garbo impersonation a "swell characterization and makes a highly diverting   contrast when the princess lapses into her real self and unloads a line of Brooklynese."   Howard Barnes of the New York Herald Tribune praised Lombards " assured and restrained portrayal -   is resourceful in exploiting its comic possibilities" The New York Post s Thornton Delehanty called her Princess the " first role in which we have admired her since the early days of her picture career." Lombard herself liked the film because  it "allowed her to do what she had first practiced in childhood days back in Indiana - mimic a figure from the silver screen."   However, Frank S. Nugent in his New York Times review called the film a "mild-to-boresome comedy." 

==Adaptations==
The Lux Radio Theater presented a one hour radio adaptation of the film in December 1938, with Fred MacMurray repeating his role and Madeleine Carroll playing Wanda Nash/Princess Olga. 

==References==
Notes
 

== External links ==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 