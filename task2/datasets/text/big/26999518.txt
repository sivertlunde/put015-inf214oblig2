Tuesday, After Christmas
{{Infobox film
| name           = Tuesday, After Christmas
| image          = Tuesday, After Christmas.jpg
| caption        = 
| director       = Radu Muntean
| producer       = 
| writer         = Alexandru Baciu Radu Muntean Răzvan Rădulescu
| starring       = Dragoş Bucur Maria Popistașu Victor Rebengiuc Mimi Brănescu
| music          = 
| cinematography = Tudor Lucaciu
| editing        = Alma Cazacu
| studio         = Multimedia Est
| distributor    = 
| released       =  
| runtime        = 
| country        = Romania
| language       = Romanian
| budget         = 
| gross          = 
}}
Tuesday, After Christmas ( ) is a 2010 Romanian film written and directed by Radu Muntean. The film was selected for the Un Certain Regard section of 2010 Cannes Film Festival.   

==Cast==
* Maria Popistașu as Raluca
* Mimi Brănescu as Paul Hanganu
* Mirela Oprişor as Adriana Hanganu
* Dragoş Bucur as Cristi
* Victor Rebengiuc as Nucu

==Reception==

===Awards and nominations===
Leeds International Film Festival
*Won: Golden Owl Award   

==See also==
* Romanian New Wave

==References==
 

==External links==
*  
*  

 
 
 
 
 
 

 
 