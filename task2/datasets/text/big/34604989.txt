Bag It (film)
{{Infobox film
| name           = Bag It
| image          = BagIt2010Poster.jpg
| alt            = 
| caption        = Film poster
| director       = Suzan Beraza
| producer       = Michelle Hill
| writer         = Michelle Curry Wright
| starring       = Jeb Berrier, Jared Blumenfeld, Martin Bourque
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Paramount Classics
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Bag It: Is Your Life Too Plastic? is a 2010 American documentary film exposing the effects of plastic bags and other plastic consumer merchandise, and its effects on land ecosystems, the marine environment and the human body.

==Synopsis==
Jeb Berrier is exploring the effects of plastic bags, opting to stop using them as his wife enters pregnancy. After conducting research with the help of an environmental scientist, Berrier analyzes environmental problems such as the  . Plastic fragments are found to outnumber plankton 60-to-1, and contributes to around 100,000 marine animal deaths, including birds, every year. Calls to the American Chemical Society prove to be fruitless. He later discovers that plastics contain chemicals such as  s that can carry adverse effects to newborn babies, including decreased anogenital distance. Berrier reflects on the rising rates of male infertility and other such as diabetes, obesity and attention deficit disorder, linking it to the ever-present problem of plastics. The average consumer household products such as baby shampoos are also found to contain unsafe levels of phthalates. When Jebs partner, Anne, gives birth, the film gets personal and looks at recently controversial chemicals such as Bisphenol A found in plastics. The film concludes with a review of recycling labels and shows ways that average citizens and consumers can minimize the harmful impact of plastic by reducing its use.

==Recycling tags==
 
The film shows that not all plastics that have recycling tags are actually recyclable. Plastics tagged as #3, #6 and #7 have low rates of recycling. Number three, polyvinyl chloride, is shown to be recycled 0% of the time.

==Advocacy==
The films director and producer have toured California to raise awareness about a proposed state ban on plastic bags at grocery and convenience stores.   

==Reception==
The film received the Best of Festival Award from the Blue Oceans Film Festival, an Audience Choice Award from the Flagstaff Mountain Film Festival, the Best Documentary Audience Award from the Ashland Independent Film Festival, the Audience Choice Award from Mountainfilm in Telluride, and second place Audience Award at the Wild & Scenic Film Festival. 

==See also==
*Ecotoxicity
*Obesogen
*The Cove (film)

==References==
 

==External links==
* 
* 

 
 
 
 
 