Las Vegas Nights
{{Infobox film
| name           = Las Vegas Nights
| image          = Las Vegas Nights poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Ralph Murphy
| producer       = William LeBaron
| screenplay     = Ernest Pagano Harry Clork Eddie Welch Phil Regan Bert Wheeler Constance Moore Virginia Dale Lillian Cornell Betty Brewer Hank Ladd
| music          = Phil Boutelje Walter Scharf
| cinematography = William C. Mellor
| editing        = Arthur P. Schmidt
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
 Phil Regan, Bert Wheeler, Constance Moore, Virginia Dale, Lillian Cornell, Betty Brewer and Hank Ladd. The film was released on March 28, 1941, by Paramount Pictures.  

==Plot==
 

==Cast== Phil Regan as Bill Stevens
*Bert Wheeler as Stu Grant
*Constance Moore as Norma Jennings
*Virginia Dale as Patsy Grant
*Lillian Cornell as Mildred Jennings
*Betty Brewer as Katy
*Hank Ladd as Hank Bevis
*Henry Kolker as William Stevens Sr.
*Francetta Malloy as Gloria Stafford
*William Red Donahue as Red Donahue 
*Tommy Dorsey as Tommy Dorsey 

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 