Twelfth Night: Or What You Will (1996 film)
 
 
 
{{Infobox film
| name = Twelfth Night: Or What You Will
| image_size =
| image	=	Twelfth Night- Or What You Will FilmPoster.jpeg
| caption =Film poster
| director = Trevor Nunn Stephen Evans David Parfitt
| screenplay = Trevor Nunn
| based on  =  
| narrator =Ben Kingsley
| starring = Imogen Stubbs Steven Mackintosh Nicholas Farrell Ben Kingsley Helena Bonham Carter Nigel Hawthorne Mel Smith Imelda Staunton Toby Stephens Richard E. Grant
| music = Shaun Davey
| cinematography = Clive Tickner Peter Boyle
| studio = BBC Films Summit Entertainment
| distributor = Fine Line Features
| released =  
| runtime = 134 minutes
| country = United Kingdom
| language = English
| budget = US$5 million
| gross = US$588,621
}} Polish army officers of the time. It was filmed on location in Cornwall including scenes shot at Padstow and Lanhydrock House, Bodmin.

==Plot==
Viola (Imogen Stubbs) and Sebastian (Steven Mackintosh) are young twins who, on Twelfth Night (holiday) are performing on a ship and use their likeness to tease their audiences. During their journey, they are caught in a storm, shipwrecked and separated. Viola and other survivors end up on the shore of Illyria. A devastated Viola believes her brother dead. She later takes his appearance to join the court of the local Duke Orsino (Toby Stephens). The young woman has her long, beautiful hair cut by the sailor, conceals her breasts, and dresses like a young man. After that, Viola becomes a Page (occupation)|page, using the name "Cesario".

Orsino is madly infatuated by Countess Olivia (Helena Bonham Carter), who is in mourning due to her brothers recent death. She uses the tragedy as an excuse to avoid seeing the Duke, whom she does not love. He sends "Cesario" to do his wooing and Olivia falls in love with the messenger, unaware of "Cesario"s real gender. Realising Olivias feelings for her alter ego, Viola is caught in even more of quandary in that she is in love with Orsino.
 steward Malvolio Maria (Imelda Sir Toby Belch (Mel Smith).
 Sir Andrew Aguecheek (Richard E. Grant) to court Olivia, but she purposely ignores him. Sir Toby pushes Sir Andrew into challenging "Cesario" to a duel, which goes very badly for Aguecheek.

Furthermore, Violas twin, Sebastian, has in fact survived the wreck and has also arrived in Illyria, accompanied by Antonio (Nicholas Farrell), who saved him from drowning. Antonio, who has "many enemies in Orsinos court", is forced to flee when he is recognised and comes across "Cesario", whom he mistakes for Sebastian, and is outraged when "Cesario" fails to help him out.

Arriving at her estate, Sebastian meets Olivia, who, mistaking him for "Cesario", talks him into marrying her. When he learns of this, Orsino is furious and dismisses his page, whom he had made a friend and confidante. However, the matter is soon cleared up when Sebastian and "Cesario" come face-to-face and the latter reveals her real nature and identity of Viola. Orsino marries Viola.

The film ends with both couples holding a party to celebrate their marriages, while the supporting players, including the humiliated Sir Andrew and Malvolio, leave the estate with their heads held high and Feste sings his song, "The Wind and the Rain".

==Cast== Viola
*Toby Orsino
*Helena Olivia
*Steven Sebastian
*Imelda Maria
*Mel Sir Toby Belch Sir Andrew Aguecheek
*Nigel Hawthorne as Malvolio
*Ben Kingsley as Feste Peter Gunn as Fabian
*Nicholas Farrell as Antonio

==Adaptation==
For purposes of length, Shakespeares original script was subjected to considerable cuts. Extra dialogue was added at the beginning with Feste narrating the events surrounding the shipwreck and the separation of the twins and of a conflict between Violas native country and Orsinos.

Viola and the other survivors hail from Messaline and when they end up in Illyria they are forced to hide and live like fugitives since "Messaline with this country is at war" over some trading disputes: the Captain (Sydney Livingstone) mentions "The war between the merchants here and ours" while hiding in the cave from Orsino and his men. In the original play the Captain claims to have been "bred and born not three hours travel from" where they got washed ashore.

The conflict is not mentioned again in the film (though it could be connected to Antonios past as "Orsinos enemy"), and the Captain appears openly at the party at the end where he is embraced by Viola.

==Reception==

===Critical response=== Lady Jane) Much Ado About Nothing, solid performances and a lucid interpretation keep it afloat." Berardinelli calls it "solid entertainment." 

==References==
 

==External links==
*  
 
 

 
 
 
 
 
 
 
 
 
 
 