A Rumor of Love
 
A Rumor of Love (aka The Love Rumor; Arabic: إشاعة حب – Eshaet Hob or Ishayat hub) is a 1960 Egyptian film by the director Fatin Abdel Wahab. The movie tells a story of a young man and the trouble he goes through to court the girl he is in love with. With the help of his uncle, the father of the girl, he manages to make her fall in love with him – but not after the entire town hears rumors about his womanizing ways.

== Plot == Youssef Wahby), and his eccentric family – his outspoken wife, Bahiga (Ehsan Elsherif), his two nephews, Hussein (Omar Sharif) and Mahroos (Abdel-Menam Ibrahim), and his beautiful daughter, Samiha (Soad Hosny). Hussein, a lowly writer turned-manager in his uncle’s firm, is desperately in love with Samiha who in turn is infatuated with multilingual playboy, Luci (Gamal Ramses). Seeing as she is of marrying age, her parents plan to marry her off. Her father wants to set her up with Hussein, who is an upstanding gentleman with a promising future. On the other hand, her mother encourages her to become engaged to Luci. The difference in opinion leads to a dispute that prompts Samiha’s father to concoct a seduction plan with which Hussein would woo Samiha.
 
Complicated, convoluted, and complex, the plan relies heavily on making Samiha jealous by “exposing” Hussein’s various relationships with different women, thus making him desirable to Samiha. Abdel Qader fabricates love letters and phone calls, with the help of Mahroos, claiming that they are from the Egyptian star Hind Rostom (Hind Rostom). Effectively jealous, Samiha, Luci and their friends search Hussein’s room for evidence of Hussein’s lady killer ways. Samiha’s father, acting enraged, threatens to throw Hussein out of the house they share declaring that Hussein was soiling the family’s reputation.

Due to the gossipy nature of Samiha’s friends, the people of Port Saeed – the city that they live in – believe that Hussein is actually in a relationship with Hind Rostom. Trouble bubbles up when Hind Rostom visits the city to perform a show. Luci meets a friend of his at a club, who later turns out to be Hind Rostom’s overly jealous fiancé, Adel (Adel Hekal). Spurned by Samiha’s feelings for Hussein, Luci decides to expose Hussein and Hind Rostom’s relationship to Adel, who gets furious at the deception. This results in a fight between Hussein and Adel, which culminates in Hind Rostom storming into the fight, intent on teaching her fiancé a lesson. With the help of a child performer, Hind Rostom deceives Adel, Samiha and Bahiga into believing that she had a child with Hussein. Samiha, heart-broken, ends her relationship with Hussein and listens to her mother’s advice about marrying Luci.

Samiha receives a phone call from Hind Rostom explaining the ploy which patches up Samiha’s relationship with Hussein. After a couple of comedic moments, all secrets are aired out, finally leaving Samiha and Hussein to have their happy ending.

==External links==
* 
*  

 
 
 
 

 