Headrush (film)
{{Infobox film
| name           = Headrush
| image          =
| image size     =
| alt            =
| caption        =
| director       = Shimmy Marcus
| producer       = Edwina Forkin
| writer         = Shimmy Marcus
| screenplay     =
| story          =
| based on       =  
| narrator       =
| starring       =
| music          = Fun Lovin Criminals, Adam Lynch
| cinematography = Owen McPolin
| editing        = Joe Marcus
| studio         =
| distributor    =
| released       = 2003  
| runtime        =
| country        =
| language       =
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 crime comedy, written and directed by Shimmy Marcus about two young guys, Charlie and T-Bag, who hope to solve all their problems by working for a notorious Underworld Criminal, The Uncle.
The movie has been criticised for its unoriginality and similarities to other movies. 

==Plot==
Down on his luck Charlie has been kicked off the dole, had his electricity cut off, and been dumped by his girlfriend Vicky. Despite claiming to be an expert on women, his best friend T-Bag has never had a girlfriend and the boys are convinced only money will change their depressing situation. The boys hear through their dealer Blowback that The Uncle is looking for new drug mules, so Charlie conceives an elaborate scam to smuggle a consignment of Cocaine back from Amsterdam. They meet The Uncles nephew Razor Rupert and convince him that theyre up for the job. As they lay their plans, each one egging the other on, each one refusing to admit to any fear, a series of comic coincidences begin to unravel their carefully laid plans.

Headrush is a story about frustration, fear, and friendship. Charlie is a lost soul in a society riddled with corruption, political distrust and a preoccupation with making money. Trying to cope with problems he brings on himself, he seems on an inevitable slide that takes him deeper and deeper into a world with fatal consequences.

==Cast==
* Wuzza Conlon as Charlie
* Gavin Kelty as T-Bag
* B.P. Fallon as Blowback
* Huey Morgan as The Yank
* Karl Argue as Flem
* Steven Berkoff as The Uncle
* Amanda Brunker
* Donncha Crowley as Detective Insp. Dunne
* Moira Deady as Mrs. Macroom
* Mark Doherty as Detective Walsh
* Dermot Doran as Bambi Tom Hickey as Malcom Nobel
* Pat Kinevane as Razor Rupert
* Winston Nairne as Milk
* Mick Nolan as Detective ODonoghue
* Gerry OBrien as Mr. Doublehorn Gavin OConnor as Neville
* Jer OLeary as Tramp Snitch
* Sarah Pilkington as Jacinta
* Laura Pyper as Vicky
* Rachel Rath as Junkie
* Viviana Verveen as Biker
* Aonghus Weber as Knob

==Production==
 

==Reception==

===Critical response===
 

===Awards===
* Winner - Best Feature - Braunschweig Film Festival (2005)
* Winner - Best Unreleased Feature - East Lansing Film Festival (2005)
* Nominated - Best Feature Film - IFTA (2005)
* Winner - Best Feature - NY Film Fleadh (2004)
* Winner - Best Soundtrack - Foyle Film Festivak (2003)
* 2nd Place - Best Feature - Galway Film Fleadh (2003)
* Winner - Miramx Scriptwriting Award (1999)

==References==
 

==External links==
*  
*  
*    

 
 
 
 
 
 