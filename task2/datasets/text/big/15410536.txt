In the Affirmative
{{Infobox film
| name           = In the Affirmative
| image          = 
| caption        =  
| director       = Claude Lelouch
| producer       = 
| writer         =  Guy Mairesse Janine Magnan
| music          = Daniel Gérard
| cinematography = Jean Collomb
| editing        = 
| distributor    = 
| released       = 1962
| runtime        = 85 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
|}}

In the Affirmative ( ) is a 1962 French film by Claude Lelouch. Though Lelouch had experienced failure with his debut feature Le Propre de lhomme, he managed to gain favourable exposure when the film was sent to be exhibited in Sweden and earned compliments from Swedish film director Ingmar Bergman. It was entered into the 14th Berlin International Film Festival.   

==Plot== sadist who is known to prey on young women and children. Lelouch often cuts away from the main story, if only briefly, to parallel events that are not necessarily crucial to the story but illustrate what is suggested by the radio.

==Cast== Guy Mairesse - Robert Blam
* Janine Magnan - Lauto-stoppeuse (hitchhiker)
* Jean Franval - Un policier (policeman) (as Franval)
* Richard Saint-Bris - Le commissaire (superintendent) (as Saint-Bris)
* France-Noëlle - La patronne de lhôtel (hotelier) Jacques Martin - Le journaliste (journalist)
* Jean Daurand - Le patron du relais routier (truck-stop owner) (as Daurand)
* Bernard Papineau - (as Papineau) Mosin
* Joëlle Picaud - La victime du bois (victim in the woods)
* Lyonnais
* Jacqueline Morane - (as Morane)
* Rita Maiden - La servante du restaurant (restaurant waitress)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 