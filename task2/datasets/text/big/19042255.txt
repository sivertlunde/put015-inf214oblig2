The Girl in the Crowd
{{Infobox film
| name = The Girl in the Crowd
| image = Girl in the Crowd.jpg
| caption = Googie Withers (seated, left)
| director = Michael Powell
| producer = Michael Balcon Brock Williams Patricia Hilliard Googie Withers Harold French
| cinematography = Basil Emmott
| editing = Bert Bates Warner Brothers-First First National Productions
| released = 20 May 1935 UK
| runtime = 52 min
| country = United Kingdom English
| budget =
}} Patricia Hilliard and Googie Withers.

The film has been declared "Missing, Believed Lost" by the British Film Institute. 

==Plot==
The wife of a bookseller gives advice about picking up woman to her husbands friend (whom she has never met) over the phone. She advises him simply to follow the first pretty woman he sees. Unfortunately, when he takes her advice, she is the girl in the crowd he ends up following, leading to his arrest.

==Cast==
* Barry Clifton as David Gordon Patricia Hilliard as Marian
* Googie Withers as Sally. Withers was just an extra, until the second female lead quit and she took over. 
* Harold French as Bob
* Clarence Blakiston as Mr. Peabody
* Margaret Gunn as Joyce
* Richard Littledale as Bill Manners
* Phyllis Morris as Mrs.Lewis
* Patric Knowles as Tom Burrows
* Marjorie Corbett as Secretary
* Brenda Lawless as Policewoman
* Barbara Waring as Mannequin
* Eve Lister as Ruby
* Betty Lyne as Phyllis
* Melita Bell as Assistant Manageress John Wood as Harry

==References==

===Notes===
 

===Bibliography===
 
* Chibnal, Steve. Quota Quickies : The Birth of the British B Film. London: BFI, 2007. ISBN 1-84457-155-6
* Powell, Michael. A Life in Movies: An Autobiography. London: Heinemann (publisher)|Heinemann, 1986. ISBN 0-434-59945-X.
 

==External links==
*  
*  
*   reviews and articles at the  

 

 
 
 
 
 
 
 
 
 


 