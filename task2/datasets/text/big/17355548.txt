The Ghost in the Swamp
 

{{Infobox film name           = Duh u močvari image          = caption        = Movie poster director       = Branko Ištvančić producer       = writer         = Edi Mužina, Silvio Mirošničenko starring       = Ivo Gregurević, Dejan Aćimović, Marko Pavlov music          = Dalibor Grubačević cinematography = editing        = distributor    = Croatian Radiotelevision (HRT) released       = 2006 runtime        =  90 min. country        = Croatia website        = language  Croatian (during Hungarian (in some small parts of the film) budget         = gross          = followed_by    =
}}
The Ghost in the Swamp (Duh u močvari) is a movie filmed by Branko Ištvančić. It is based on the book Duh u močvari by Anto Gardaš.

==Cast==

* Ivo Gregurević (Vučević)
* Dejan Aćimović (Levay)
* Marko Pavlov  (Miron)
* Ena Ikica  (Melita)
* Robert Váss  (Liptus)
* Vlatko Dulić  (Farkaš)
* Mladen Vulić  (Kovačević)

==Plot summary==

After Liptus arrives in a small village from the city, he calls his best friends, Miron and Melita, on winter holidays in Kopačevo. That same night, while they are all sleeping cozily, from their deep sleep Miron and Melita are woken by the disturbing sounds of villagers, carrying flares and disappearing into the darkness at the end of the street. At the docks, they find Halasz, a boy known for his bravery. He is cold, pale from shock, and babbling a white ghost. While most of the villagers dont believe Halaszs story, older citizens recall a long time ago when a forgotten spirit would scare people at night in dark and foggy swamps. Everyone locks themselves in their houses and Halasz ends up in a hospital where the doctors cant help him. Miron, Liptus, and Melita, now alone, take matters into their own hands, revealing the secret of the ghost and saving their friend.

==Score==

The films soundtrack was composed by Dalibor Grubačević.   It was distributed under Croatia Records 10 October 2006.

==References==

 

==External links==
*   
*  
*  

 
 
 
 
 


 
 