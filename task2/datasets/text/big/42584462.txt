Huddle (film)
{{Infobox film 
| name           = Huddle
| image          =
| caption        =
| director       = Sam Wood
| producer       = Francis Wallace Robert Lee Johnson Arthur S. Hyman (adaptation) Crilly Butler Elbridge Anderson (technical detail)
| starring       = Ramon Novarro Madge Evans Una Merkel Ralph Graves John Arledge Kane Richmond Martha Sleeper Henry Armetta
| music          =
| cinematography = Harold Wenstrom
| editing        = Hugh Wynn
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 103 min
| country        = United States
| language       = English
| budget         = $293,000
| gross          =
}} 1932 American Mata Hari.

==Plot==
Tony Ametto, a young steel-worker with immigrant parents, gets a scholarship to Yale, where he becomes a football star and finds romance with a young heiress.

==Production==
Director Sam Wood was known for directing college-themed films such as One Minute to Play (1926) and So This is College (1929). The film showcases the popularity of the college film subgenre of culture-clash plot elements that were the focus of comedies like Hold Em Yale (1928) with Rod La Rocque, and the drama Redskin (film)|Redskin (1929) with Richard Dix.

The film was shot on a considerable budget of $514,000,  and included recreating Yales Derby Day on the MGM backlot.  Shooting began on on February 12, 1932, and lasted for nine weeks. Real college students for the film were recruited as extras for $5-a-day; while presumably providing authentic atmosphere to the crowd scenes, they were also cheaper to hire than the average professional film extra in Los Angeles, whose pay was set at $7.50.  One of the extras in the film was a pre-fame Buster Crabbe, who appeared in a few scenes before being fired because of "inadvertent rudeness to the star (Novarro) during rehearsals."  All American football stars Merger Asplit, Gene Clark, Dale Van Sickel, Jess Hibbs, Ernie Pinkert and Manfred Vezie appeared in the film as well to lend authenticity to the football game plays.  
 Robert Montgomery Call of the Flesh (1929). 

==Release== Capitol Theatre a month later on June 16. In general release, the film wa a box office failure, with a loss of $28,000.  Critics almost universally described it as too long and agreed that Novarro was miscast.

Huddle has been broadcast on television occasionally since at least 1989 and has played several times on the cable channel Turner Classic Movies. It was released on DVD through Warner Archive Collection on October 5, 2010. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 