The Internet Must Go
{{Infobox film name = The Internet Must Go
| image          = Internet-must-go-black.png
| image_size     = 
| border         = 
| alt            = 
| caption        = Movie logo director = Gena Konstantinakos  producer = {{Plainlist |
* Gena Konstantinakos
* Brendan Colthurst
* and 10 more 
}} writer = Billy Kimball and 3 more  
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  starring = {{Plainlist |
* Brian Shortall
* John Hodgman
* Al Franken
* Alexis Ohanian
* Larry Lessig
* Susan Crawford
* and others 
}}
| music          = {{Plainlist |
* Rebecca Gates (music supervisor)  featured songs
}}
| cinematography = {{Plainlist |
* Gena Konstantinakos
* and 5 others 
}}
| editing        = {{Plainlist | Greg Wright
* Lauren Beck
* Dave Marcus Adam Barton 
}}
| studio         = Konstant Films LLC 
| distributor    = {{Plainlist |
* YouTube
* vimeo
}}
| released       =   runtime = 30 minutes country = USA language = English
| budget         = 
| gross          = 
}}
The Internet Must Go is {{cite web
|url=http://www.huffingtonpost.com/2013/09/09/net-neutrality-mockumentary_n_3882726.html
|title=Net Neutrality Mockumentary Slams ISPs Case For Playing Unfair
|publisher=Huffington Post
|first=
|last=
|date=9 September 2013
|accessdate=13 January 2014}}   {{cite web
|url=http://www.networkworld.com/news/2013/090913-mockumentary-skewers-isps-as-verizon-fcc-273636.html
|title=Mockumentary skewers ISPs as Verizon-FCC net neutrality case goes to court
|publisher=Network World
|first=Bob
|last=Brown
|date=9 September 2013
|accessdate=13 January 2014}}  {{cite web
|url=http://www.muninetworks.org/content/well-worth-watching-internet-must-go
|title=Well Worth Watching: The Internet Must Go
|publisher=Community Broadband Networks
|first=Christopher
|last=Mitchell
|date=9 September 2013
|accessdate=13 January 2014}}  {{cite web
|url=http://www.crackajack.de/2013/09/12/the-internet-must-go-mockumentary-about-anti-net-neutrality-wannabe/
|title=The Internet Must Go: Mockumentary about Anti-Net Neutrality-Wannabe
|publisher=crackajack.de
|first=
|last=
|date=9 September 2013
|accessdate=13 January 2014}}  independent docufiction short web film about net neutrality (the principle that Internet service providers (ISPs) should not favour either type of content over another ), directed by Gena Konstantinakos.
 Second City alum Brian Shortall) as he attempts to sell Internet Service Provider|ISPs vision for what they call a "faster, cleaner" Internet.      However, learning about net neutrality from several (non-fictional) people in the process, he ultimately opts to re-consider his standpoint.  

Designed specifically   to reach an audience not versed in technology or policy, The Internet Must Go has reached nearly a quarter of a million viewers and about 5k Facebook fans in its first 2 months online.       

The release of the short was made to coincide with the opening arguments in Verizon v. F.C.C {{cite web
|url=https://www.fcc.gov/document/verizon-v-fcc-no-11-1355-dc-cir-1
|title=Verizon v. FCC, No. 11-1355 (D.C. Cir.)
|publisher=FCC.gov
|first=
|last=
|date=7 February 2013
|accessdate=13 January 2014}}  
about Net Neutrality.  {{cite web
|url=http://www.washingtonpost.com/blogs/the-switch/wp/2013/09/09/can-this-mockumentary-make-the-public-care-about-net-neutrality/
|title=Can this mockumentary make the public care about Net Neutrality?
|publisher=The Washington Post Interview with the director
|first=Andrea
|last=Peterson
|date=9 September 2013
|accessdate=13 January 2014}}  

==Plot==
The docufiction short centers on John Wooley, a (fictional) market researcher who "has been dispatched to help the big Internet service providers sell their vision of a faster, cleaner Internet".  He embarks on the journey, believing hes doing something great and important. Over the course of his journey he interviews several people (notably non-fictional people whose business depends on net neutrality), who one by one help him to understand why his mission is misguided.

Then he ventures to North Carolina where he interacts with (non-fictional) people who, stifled by a lack of broadband altogether, have attempted to build community broadband. {{cite web
|url=http://www.muninetworks.org/content/new-hope-north-carolina-broadband-struggle-continues
|title=With New Hope, North Carolina Broadband Struggle Continues
|publisher=Community Broadband Networks
|first=Christopher
|last=Mitchell
|date=22 March 2011
|accessdate=13 January 2014}}  However, he finds that North Carolina now has barriers to building community broadband. While he interacts with those people, he ultimately has a change of heart,  and decides to "leak" his research publicly to the internet, in favour of net neutrality.

== Production ==

===   Cast ===
The movie stars Brian Shortall as (fictional) market researcher John Wooley, who interviews several people who happen to be open-internet advocates. These interviewees are, however, non-fictional people, who appear in the movie as themselves.

====Fictional cast====
* Brian Shortall as (fictional) market researcher John Wooley

====Non-fictional cast====
* Tim Wu (professor at Columbia Law School who coined net neutrality) as himself junior United United States Senator from Minnesota) as himself
* Rashad Robinson (Executive Director of Color of Change) as himself
* Alexis Ohanian (co-founder of the social news website reddit) as himself
* Eli Pariser (co-founder of Avaaz.org and Upworthy) as himself
* Farnum Brown (investment manager (at Trillium Invest Management,  unnoted) and AT&T shareholder ) as himself
* Gigi Sohn (of Public Knowledge ) as himself
* Harold Feld (of Public Knowledge ) as himself
* Robin Chase (founder and CEO of Buzzcar and former CEO of Zipcar) as herself
* Ricken Patel (Executive Director of Avaaz)
* Craig Aaron (President and CEO of Free Press (organization)), as himself Susan Crawford (professor at the Cardozo School of Law and former ICANN Board Member), as herself
* Larry Lessig (American academic best known as founding member of Creative Commons) as himself
* Catharine Rice (broadband consultant with Action Audits, North Carolina {{cite web
|url=http://www.natoa.org/catharine-rice-biography.html
|title=Catharine Rice Biography
|publisher=National Association of Telecommunications Officers and Advisors
|first=
|last=
|date=
|accessdate=13 January 2014}} ) as herself
* John Hodgman (American author, actor and humorist) as himself

===Crew, Partners and Funding===
The movie was fiscally sponsored by the non-for-profit Women Make Movies. Funders include The Ford Foundation, The Open Society Foundation, Media Democracy Fund, and Wyncote Foundation.
 independent short, the films website lists contributors to the movie, in notable detail. This includes cast/the cameo appearance|cameos, several kinds of producers, writers, funders, editors, consultants, advisors, production managers, animators, camera, legal counsel, post-production, voiceover, assistants, pod-editors, website, graphics design, campaign, press, associatees, partners and others.    Links to the contributors online identities are provided where applicable.

===   Soundtrack ===
The movie has no stand-alone soundtrack album, but it features music from the following artists. Rebecca Gates was credited as music supervisor for the film.

*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

==Website==
The movie was designed as a short (termed "clickable" by the director ). Its website, theinternetmustgo.com, then augments its functionality. Apart from featuring the movie, it provides information about the movie such us directors notice, creators, press, etc. This information also allows a watcher to verify {{cite web
|url=http://www.theinternetmustgo.com/about/directorsnote.php
|title=Directors Note
|publisher=The movies own website
|first=
|last=
|date=16 September 2013
|accessdate=13 January 2014}}  the docufiction nature of the movie, which is not vocalized in the work itself.

The site also features a section called "Take Action" providing activism support for net neutrality. It links to some of the non-fictional organization and people presented.

==="Bonus leaks"===
In a section named "Bonus leaks", the site features 13 extra videos made with the interviewees, as well as a list of real-world articles featured in the film (called "Wooleys Research"), and a map of US states "that have erected barriers to community broadband".   

==Aftermath==
  sponsored data" to companies, who would be allowed to pay for the bandwidth their customers use. This arguably undermines net neutrality, as noted by several associations connected to the movie, e.g. Fight for the Future ,    who also supports the prediction that other carriers will most likely try to follow    and details some implications on individuals and on the open internet.
 Wired noted that previously "By a 3-2 vote in 2010, the FCC adopted net neutrality rules, which became effective a year later." 

==See also==
* Net neutrality
* Data discrimination
* Net bias
* Wireless community network
* Community informatics
* Freedom of information
* Free Culture movement
* Brian Shortall
* Women Make Movies

==References==
 

==External links==
*  
*  
*  
*   with the director on Washington Post.
*  

 
 
 
 
 
 
 
 
 
 