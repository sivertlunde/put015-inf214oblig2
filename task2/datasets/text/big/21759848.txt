Loving Couples (1964 film)
{{Infobox film
| name           = Loving Couples
| image          = Loving Couples (1964 film).jpg
| caption        = Swedish cover
| director       = Mai Zetterling
| producer       = Göran Lindgren Rune Waldekranz David Hughes Agnes von Krusenstjerna Mai Zetterling
| starring       = Harriet Andersson
| music          =
| cinematography = Sven Nykvist
| editing        = Paul Davies
| distributor    =
| released       =  
| runtime        = 118 minutes
| country        = Sweden
| language       = Swedish
| budget         =
}}

Loving Couples ( ) is a 1964 Swedish drama film directed by Mai Zetterling. It was entered into the 1965 Cannes Film Festival.   

==Cast==
* Harriet Andersson as Agda Frideborg
* Gunnel Lindblom as Adele Holmström - née Silfverstjerna
* Gio Petré as Angela von Pahlen
* Anita Björk as Petra von Pahlen
* Gunnar Björnstrand as Dr. Jacob Lewin
* Eva Dahlbeck as Mrs. Landborg
* Jan Malmsjö as Stellan von Pahlen
* Lissi Alandh as Bell
* Bengt Brunskog as Tord Holmström
* Anja Boman as Stanny, Bernhards sister
* Åke Grönberg as The fat man
* Margit Carlqvist as Dora Macson Heinz Hopf as Lt. Bernhard Landborg
* Märta Dorff as Alexandra Vind-Frija
* Jan-Eric Lindquist as Peter von Pahlen

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 