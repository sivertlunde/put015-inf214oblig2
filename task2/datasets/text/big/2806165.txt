Willy the Sparrow
{{Infobox film
| name           = Willy the Sparrow
| image          =
| image size     =
| caption        = Willy the Sparrow DVD
| director       = József Gémes
| producer       =
| writer         =József Gémes József Nepp
| narrator       =
| starring       = Sarah Schaub  Barta Heiner  Rick Macy  Aaron Bybee
| music          = Zsolt Pethõ
| cinematography = Árpád Lossonczy Erzsébet Nemes György Varga 
| editing        = Magda Hap
| studio         = PannóniaFilm|Pannónia Filmstúdió
| distributor    = Feature Films for Families
| released       = 16 November, 1989
| runtime        = 76 minutes
| country        = Hungary
| language       = Hungarian
| budget         =
| preceded by    =
| followed by    =
}}
Willy the Sparrow ( ) is a  . Voice actors included Sarah Schaub, Barta Heiner, Rick Macy and Aaron Bybee. The film was released on DVD in 2004.

==Synopsis==
The story is about a 10-year old boy named Willy who enjoys pretending to shoot birds with a BB gun until he is turned into a Sparrow by the Sparrow Guardian. Willy becomes lost outside without knowing how to fly, and he is taken in by a sparrow named Cipur; Cipur teaches Willy how to fly in exchange for Willy teaching Cipur how to read. Later, Willy helps a group of sparrows retake their home in a barnyard from a cat.

==Plot==
 
The movie begins with Willy at home sick and is pretending to be a hunter. Using his imagination to see his cat, Sissy, as the tiger, he torments the cat with his water gun. He then decides to shoot a BB Gun at a flock of sparrows outside at the local park, stunning them away, but angers an elderly lady who in reality is the Sparrow Guardian, who magically enters Willys apartment unnoticed. She then transforms Willy into a sparrow in hopes of teaching him a lesson of respecting all living things. However, she didnt have enough spray to make Willy have the ability to fly, rendering him defenseless. The Sparrow Guardian quickly leaves to refill her magic hair spray. While Willy was making himself comfortable in his sparrow form, Sissy appears and sees Willy as lunch instead of her master. Not used to walking like a sparrow, Willy was nearly eaten by his own cat, but was saved and placed outside by his little sister, Tonya. He soon meets two sparrows, Red and TJ, who discovers Willys inability to fly and then calls help on an elder sparrow named Cipur to help Willy learn to fly. The three sparrows then carries the flightless Willy to safety, escaping an incoming attack from a persistent, hungry Sissy.

Meanwhile, the Sparrow Guardian is looking for Willy. Sissy is also looking for him.

In an attic where Cipurs nest was in, Cipur tells Willy that he wanted to read and write like a human because he was fascinated by their knowledge and technology, but he keeps this as a secret in fear of being shunned by the other sparrows. He makes a deal with Willy that if he teaches the young sparrow how to fly, Willy will teach Cipur how to read. As the flying lesson was underway, Sissy finds Willy and Cipur and attacks. Before she could eat Willy, Cipur intervenes and lures an angry Sissy atop the roof where he trapped her head under the weight of a ceiling hatch. Under Cipurs care, Willy is taught how to fly, and in return, Willy teaches Cipur how to read and write. One day, when Cipur went to find some food, Willy decides to venture out in the open to explore the outside world after learning how to fly properly. He flies back to the park and joins a flock of young sparrow, led by Red. They decided to show Willy the barn they used to hang out, until it was taken over by a big, black cat named Blackie.

The flock of sparrows, including Willy, flew across the city to the barn where they used to live, unaware that they were being trailed by a dog and Sissy who was following them to the barn. Once in the barn, Willy catches sight of a mouse waking up Blackie, and alerts him about the sparrows in the barn. Blackie attacks the sparrows, and nearly eats one of the sparrows named Amy. Not abandoning his new friend, Willy drops a light bulb on Blackies head, distracting him long enough for Amy to escape unharmed. Willy then escapes the barn, and Sissy appears and becomes acquainted with Blackie since they both share a taste for sparrows, and want to eat Willy. 

Meanwhile, Willy flies his way back to his apartment and writes a note to his worried family that he is okay and that hell return soon, before flying back to Cipurs nest. After Willy was writing his note to his worried family, he flies back to Cipurs nest only to see the elder sparrow angry at him for not telling about an item called The Elixir of Knowledge. Willy was confused and didnt know about this elixir, but was driven away by Cipur who angrily tells him to go away, he decides to fly away from his nest. Willy follows the elder sparrow, and finds out that he has been drinking liquor along with two rats who had convinced the old bird that the liquor will give him knowledge. However, it only made Cipur druggish and made him feel worse. Willy quickly carries Cipur back to safety, but the old sparrow was still angry with him for leaving him. Willy sadly leaves and he sees Amy flying to him. So, Willy flies to Amy and discovers through her that Red is angry at Willy because he believes Willy is the one who woke up Blackie. So Willy follows Amy into an indoor roof nest where they will be safe from a storm.

The next day, Willy and Amy were flying back to the park to find their friends, who were all waiting for them, only to see Red feeling angry at Willy. Willy supposed to lose two feathers as punishment. When Willy refuses to take punishment, Red angrily fights Willy to make him submit, but Willy, still used to fighting as a human boy, easily beats Red and is promoted leader of the flock. The Sparrow Guardian and Sissy finds Willy, but Willy doesnt want to turn back into a boy yet until he helps his new friends. Willy then leads the flock back to the barn, with the Sparrow Guardian and Sissy trailing after them.

Under Willys leadership, the sparrows silenced the mouse who was working with Blackie by tying him up, and finally tied up a sleeping Blackie in a sneak attack. They began eating the grain, but Sissy arrives at the barn way ahead of the Sparrow Guardian and releases Blackie. The two cats then team up, with Blackie fighting and knocking the sparrows unconscious, and Sissy catching and placing them on a sheet to prepare to eat the sparrows, eventually leaving only Willy to fight Blackie. When all seemed lost for Willy, Cipur arrives and assists the young sparrow into fighting against Blackie, but the black cat overpowers the two and prepares to eat them. The Sparrow Guardian saves Willy and Cipur by repeatedly hitting Blackie with a broom, driving the black cat away from the barn for good.

The Sparrow Guardian is prepared to turn Willy back into a boy, so he can return to his home where his family is worried about him. However, Willy refuses to be turned back into a human boy, and would rather stay a sparrow if Cipur isnt turned into a human too. Willy is then granted to be the Sparrow Guardian. With little hesitation, Willy accepts. The Sparrow Guardian then uses her magic spray to turn both Willy and Cipur into humans. Cipur, who now wants to learn more about the human world, is joined by the retired Sparrow Guardian for something to eat and leaves the barn together.

The film ends with Willy, along with the reconciled Sissy, making their way back home, followed by the flock of young sparrows.

==Characters==
Willy/Vili - A young boy who is turned into a sparrow by the Sparrow Guardian after attempting to shoot at a flock of sparrows with his BB gun out of boredom. As a sparrow, his world is quite different and he soon learns kindness and respect for all living creatures.

Sissy/Cili - Willys pet cat. In the original version of the film, Sissy saw Willy as a meal after he was turned into a sparrow. She becomes the primary antagonist in the film, wanting to eat Willy instead of helping the Sparrow Guardian teach the boy a valuable lesson, and attempted to eat him in some parts of the movie. In the end, Sissy reconciled and was taught a lesson about respecting living creatures, including sparrows, along with Willy (she is shown releasing the other sparrows instead of eating them). In the English version, Sissy came after Willy because she wanted to teach him a lesson and also settle a score with him for tormenting her at the beginning of the film.

The Sparrow Guardian Verbena/Sparina - An elderly woman who protects and watches over sparrows. She turns Willy into a sparrow as a test to teach the boy a lesson about respecting living things.

Cipur/Cifur - An old sparrow who secretly wants to read and write like a human. He teaches Willy how to fly, who in turn, teaches Cipur how to read and write. Cipur serves as Willys teacher while he was a sparrow.

Blackie - An evil black cat who likes to eat sparrows. He took over the barn where the sparrow flock used to hang out, and whenever they enter the barn, a mouse who works with Blackie wakes up the big, black cat and alerts him about the sparrows.

Tonya - Willys little sister. She saved Willy from being eaten by Sissy the  Cat.

==Version Differences==
In the English version, Sissy came after Willy because she wanted to teach him a lesson and also settle a score with him for tormenting her at the beginning of the film. In the original version of the film, Sissy saw Willy as a meal, since cats eats sparrows, and seemingly became the films primary antagonist when she was intent on eating her owner while he was in sparrow form. In the English version, the scene when Sissy is about to devour Willy is cut off and rushed to the part when Willys sister Tonya comes in and stops Sissy from eating him, possibly because it might frighten the younger audience.

In the original Willy actually takes a shot at the sparrows with his b.b. gun, in the English version he just yells "Bang!, Bang!".

In the original when the Sparrow Guardian first sprays Willy with her magic spray he turns into an alligator which frightens her then she changes him into a mouse, in the English version this was cut for possibly being too frightening.

In the English version, Cipur was coaxed into drinking liquor by a pair of rats who tricked the elder sparrow into thinking its the Elixir of Knowledge. In the original version, they are drinking alcohol.

==See also==
* PannóniaFilm, Hungarian animation studio which made the film
* List of animated feature films

==References==
 

==External links==
*  

 
 
 
 
 
 
 