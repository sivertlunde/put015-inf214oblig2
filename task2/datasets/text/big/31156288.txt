Pulliman (1972 film)
{{Infobox film
| name = Pullimaan
| image =
| caption =
| director = E. N. Balakrishnan
| producer = Ponnappan
| writer = SK Pottakkad Thikkodiyan (dialogues)
| screenplay = Thikkodiyan Madhu Devika Vijayanirmala Alummoodan
| music = M. S. Baburaj
| cinematography = EN Balakrishnan
| editing = MS Mani Vaira Moorthi
| studio = Deepthi Films
| distributor = Deepthi Films
| released =  
| country = India Malayalam
}}
 1972 Cinema Indian Malayalam Malayalam film, directed by E. N. Balakrishnan and produced by Ponnappan. The film stars Madhu (actor)|Madhu, Devika, Vijayanirmala and Alummoodan in lead roles. The film had musical score by M. S. Baburaj.  
  

==Cast==
   Madhu 
*Devika 
*Vijayanirmala 
*Alummoodan 
*Kaduvakulam Antony 
*Kottarakkara Sreedharan Nair  Master Raghu 
*Philomina 
 

==Soundtrack==
The music was composed by M. S. Baburaj. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || Sreekumaran Thampi || 
|- 
| 2 || Chandrabimbam || K. J. Yesudas || Sreekumaran Thampi || 
|- 
| 3 || Kaaveri Kaaveri || K. J. Yesudas || Sreekumaran Thampi || 
|-  Janaki || Sreekumaran Thampi || 
|-  Susheela || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 


 