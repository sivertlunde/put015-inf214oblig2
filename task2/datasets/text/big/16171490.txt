The Boy and the King
 
{{Infobox film
| name           = The Boy and the King
| image          = The Boy and the King DVD cover.jpg
| image size     = 200px
| caption        = DVD cover
| director       = Dervis Pasin
| producer       = Osama Ahmed Khalifa Ella Animation
| writer         = Serdar Canaslan
| starring       = 
| distributor    = Astrolabe Pictures
| released       = 1992
| runtime        = 88 minutes
| country        = Egypt
| language       = English
}}
The Boy and the King is a 1992 feature-length animated film made in Egypt by the Islamic film company Astrolabe Pictures. It includes a soundtrack of Islamic songs in English, performed by young Muslims.

== Plot summary ==
The story surrounds a young boy called Obaid. He is confronted with a choice to live an easy life in this world or to struggle for reward in the Akhirah|hereafter. The story takes place during the tyrannical rule of King Narsis, who controls his people by encouraging them to worship idols and frightening them with the magic of Cinatas, his evil sorcerer. Cinatas chooses Obaid to be his student, someone who will assist him in his magic. At first, the boy is tempted by dreams of the power and the influence he will wield as the kings next sorcerer. But, soon after he starts to question his priorities. He begins talking to a righteous and a very holy man, who opens his mind to the true meaning of life.

This righteous man tells Obaid that there is only one God, Allah Almighty and that He created mankind to worship Him alone. Though these words appeal to Obaids heart and mind, he is still pretty much confused. The righteous man advises him to seek the truth on his own – and thus begins the boy’s quest for the real meaning of life.

== Sources == Islamic prophet Surat Al-Burooj from the Qur’an (85:4). The Quranic story is about a boy who believes in Allah Almighty, and he is blessed with divine protection from the plotting of a wicked king and a sorcerer.

== See also ==
* List of animated Islamic films
* Fatih Sultan Muhammad
*  

== External links ==
*    
*  

 
 
 
 
 


 