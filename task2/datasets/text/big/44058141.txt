Balapareekshanam
{{Infobox film
| name = Balapareekshanam
| image =
| caption =
| director = Anthikkad Mani
| producer = Babu Jose
| writer = Thoppil Bhasi (dialogues)
| screenplay = Thoppil Bhasi
| story = K. Balachander
| starring = Sukumari Jayabharathi Adoor Bhasi Babu Jose
| music = M. K. Arjunan
| cinematography = C Namas
| editing = K Sankunni
| studio = Ragam Pictures
| distributor = Ragam Pictures
| released =  
| country = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film, directed by Anthikkad Mani and produced by Babu Jose. The film stars Sukumari, Jayabharathi, Adoor Bhasi and Babu Jose in lead roles. The film had musical score by M. K. Arjunan.    The film was based on Tamil film Poova Thalaiya.

==Cast==
  
*Sukumari 
*Jayabharathi 
*Adoor Bhasi 
*Babu Jose
*Kottayam Santha 
*Pattom Sadan  Shubha 
*Sreemoolanagaram Vijayan Raghavan 
*Paul Vengola 
*K. P. Ummer 
*Pala Thankam 
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Mankombu Gopalakrishnan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF"  
! No. !! Song !! Singers !! Lyrics !! Length (m:ss) 
|- 
| 1 || Jeevitham Swayam || Jolly Abraham || Mankombu Gopalakrishnan || 
|- 
| 2 || Kaalindi Theerathe || KP Brahmanandan || Mankombu Gopalakrishnan || 
|- 
| 3 || Pullippuli Pole || P Jayachandran, Vani Jairam || Mankombu Gopalakrishnan || 
|- 
| 4 || Vennilaa puzhayile || P Susheela, Ambili || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 
 

 