Bill (2015 film)
 
 
{{Infobox film
| name = Bill
| image = billposter.jpg
| caption = Promotional poster
| director = Richard Bracewell
| producer = Charles Steel Alasdair Flind Tony Bracewell
| story    = Laurence Rickard Ben Willbond
| screenplay = Laurence Rickard Ben Willbond
| starring = Mathew Baynton Simon Farnaby Martha Howe-Douglas Jim Howick Laurence Rickard Ben Willbond
| editing  = David Freeman
| cinematography = Laurie Rose
| studio = BBC Films, Punk Cinema and Cowboy Films
| distributor = Koch Media (UK, Germany & Scandinavia) Independent (other international sales)
| music = Andrew Hewitt
| released =  
| runtime = 
| country = United Kingdom
| language = English
| budget = 
| gross = 
}} Horrible Histories and Yonderland. Produced by Punk Cinema and Cowboy Films for BBC Films, it is scheduled for UK release on 21st August 2015.    The film is a fictional take on the young William Shakespeares search for fame and fortune, as written by Laurence Rickard and Ben Willbond and directed by Richard Bracewell. It will feature the six lead performers playing several different roles each.

==Plot==
 Queen Elizabeth I."     Writer Rickard further explained that this "very different" version of the future Bard has already tried "everything from contemporary dance to playing lute in a band. Hes never found his calling."   

==Cast==

The six members of the starring troupe have been announced as playing 40 total roles, in the manner of the Monty Python films.     Confirmed named roles    are listed below:

*Mathew Baynton as Bill Shakespeare
*Martha Howe-Douglas as Anne Hathaway
*Ben Willbond as Philip II of Spain
*Simon Farnaby as Juan
*Jim Howick as Gabriel
*Laurence Rickard as Lope

It was subsequently confirmed at the 2014 Cannes film festival that Homeland (TV series)|Homeland star Damian Lewis would be joining the cast as Sir Richard Hawkins.     Listed in the Internet Movie Database.  Other supporting players include:

*Helen McCrory as Queen Elizabeth I  Rufus Jones as Sir Walter Raleigh Confirmed by the producers on the films official Twitter account.  Justin Edwards as Sir Francis Drake 
*David Crow as Ramon 
*John Henry Falle as Miguel 
*Jamie Demetriou as Sergio 
*Richard Atwill as Sevi 
*Richard Glover as Catholic Contact 

==Production==

 
 BFI (British Film Institute) Film Fund had invested £1 million in the production, with further undisclosed amounts coming from BBC Films, LipSync and Screen Yorkshire, through its Yorkshire Content Fund. 

Principal filming began on 10 February 2014 at locations around Yorkshire, including York Minster, Skipton Castle, Marske-by-the-Sea, Bolton Castle and Selby Abbey. Other filming locations included Stowe School, Caroline Gardens Chapel in Peckham, London, and Weald and Downland Open Air Museum in West Sussex. The closing scenes were filmed at Shakespeares Globe, a modern recreation of the theatre where many of Shakespeares plays were first performed.     

Former Horrible Histories co-stars Rickard and Willbond are credited with the Bill screenplay, and are also the top-billed performers along with the four other members of the HH starring cast (Baynton, Howe-Douglas, Farnaby and Howick).  Despite this connection, and sharing a similar subject matter, the film has no official affiliation with the earlier TV series. It is the second project (after Yonderland) created by the sextet as a means to continue working together as a troupe after Horrible Histories ceased production in 2012, while maintaining the familiar character-and-costume driven comedy style.  Making the move to film was "a bit scary," Willbond said, "but we cooked up a really nice plot."    Rickard described the overall tone of the new project as "a hundred different brands of idiocy, really... We staunchly defend the idiocy." 

In contrast to the conscientious grounding in accuracy that was the trademark of Horrible Histories, Bill is by design a purely imaginary tale. In the initial press release for the film, the co-writers noted that "Were playing with history, just as Shakespeare did, for the entertainment of the audience."  Commenting on the choice of subject, Rickard added that in fact "the joy of the "lost years" is we can tell a fun story without trampling on the facts—it gives us licence to take William Shakespeare on a truly ridiculous caper, yet end with him becoming the man the world knows."   

The first full-length trailer for the film was released in December 2014, at which point the films theatrical release date—originally scheduled for late February 2015—was confirmed to have been pushed out to March 27 of the same year.     This was subsequently revised again--with no official explanation--to August 21st. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 