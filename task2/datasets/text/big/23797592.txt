The Bacchae (film)
{{Infobox film
| name           = The Bacchae
| image          = Bacchae dionysus opening.jpg
| caption        = Dionysus (Richard Werner) in The Bacchae, directed by Brad Mays, 2000.
| director       = Brad Mays
| producer       = Lorenda Starfelt, John Morrissey
| writer         = Brad Mays, adapted from Euripides
| starring       = Richard Werner, Jonathan Klein, Lynn Odell, William Dennis Hunt, Will Shepherd, Ramona Reeves, Elyse Ashton, Kiersten Morgan
| music          = Peter Girard
| cinematography = Jacob Pinger
| editing        = Brad Mays
| distributor    =
| released       =
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| followed_by    =
}}
The Bacchae is an independent film adaptation of Euripides classic Play (theatre)|play, produced by Lorenda Starfelt and John Morrissey, and directed by Brad Mays.

==Production==
The Bacchae was shot in the autumn of 2000, three years after director Brad Mays highly successful, award-nominated theatrical production of Euripides play at the Complex in Los Angeles. Originally conceived on a larger scale than what eventually went into production, the film suffered tremendously from budget cuts and artistic differences, most particularly between the director and co-producer John Morrissey (American History X). {{cite journal
  | last = Hall
  | first = Edith
  | authorlink =
  |author2=Macintosh, Fiona |author3=Wrigley, Amanda 
   | title =Dionysus since 69: Greek Tragedy at the Dawn of the Third Millennium
  | journal =
  | volume =
  | issue =
  | pages =
  | publisher =Oxford University Press
  | location =
  | year =2005
  | url =
  | issn =
  | doi =
  | id =
  | accessdate = }} 

==Story==
Having established his divinity in eastern lands, Dionysus - the god of wine - returns to Thebes, land of his birth as well as his mortal mother Semeles horrible and shameful death. Angered over his homelands refusal to acknowledge his divine nature, the son of Zeus intends to establish the worship which he insists is now his due. 
Having put a spell on all the local women, a great celebration of dance and wine takes place in the nearby Glens of Cithaeron, attended even by the former king Cadmus and blind prophet Teiresias. When word of this outlandishness reaches Pentheus, the young and rational King of Thebes, he orders the immediate arrest of the blonde stranger responsible for the mayhem. Unaware that his strange prisoner is a god, Pentheus refuses to even consider the possibility that Bacchic worship has a place in the modern world. 
Unable to endure such an affront, the god Dionysus casts a spell over the young king and leads him into the mountains, where he is ultimately torn limb from limb by the ecstatic worshippers, whose number now includes Pentheus own mother, Agave.

==Background==
 , 2000.]] 
  (Jonathan Klein) in drag The Bacchae, directed by Brad Mays, 2000.]] 

Director Brad Mays 1997 stage production of The Bacchae had been a surprise hit in Los Angeles, drawing large audiences and earning excellent reviews. {{cite journal
  | last = Brandes
  | first = Phillip
  | authorlink =
  | title = Daring Bacchae Delves Into Modern Psyche (Review)
  | journal = Los Angeles Times
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = July 4, 1997
  | url =
  | issn =
  | doi =
  | id =
  | accessdate = }}]  {{cite journal
  | last = Morris
  | first = Steven Leigh
  | authorlink =
  | title = Primal Time - Euripides Revisited (Featured Review)
  | journal = LA Weekly
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = July 11–17, 1997
  | url =
  | issn =
  | doi =
  | id =
  | accessdate = }}]  {{cite journal
  | last = Corcoran
  | first = Patrick
  | authorlink =
  | title = A Bacchanalian Delight (Review)
  | journal = LA New Times
  | volume =
  | issue =
  | pages =
  | publisher =
  | location =
  | date = July 10–16, 1997
  | url =
  | issn =
  | doi =
  | id =
  | accessdate = }}  It was ultimately nominated for three LA Weekly Theatre Awards, for Production Design, Original Musical Score, and Direction.  Considered particularly noteworthy was the productions use of ample though non-exploitive full-frontal nudity, most particularly in scenes portraying ritualized pagan worship and, ultimately, the violent ritual killing of the character Pentheus, king of Thebes. A major contributing factor in the productions effectiveness was the movement scoring by choreographer Kim Weild, a practitioner of the Suzuki method of dance/movement. In the months which followed the productions closing, its creators began to ponder the feasibility of an independently produced film version. Several of the stage productions cast were invited to re-create their roles for the film which, according to Eric Grode in an article written for the entertainment industry publication Playbill, was also to include British actors Brian Blessed and Alan Bates. Citing audience "fist fights" over theatre tickets for the stage production, Grode anticipated an exciting, hard-hitting contemporary film version, "as edgy as any Tarantino knock-off," promising "nudity, gore and rock music galore."  Unfortunately, the notion of a contemporary film adaptation of a 2,500-year-old play was eventually seen as impossibly risky, and much of the committed funding was ultimately withdrawn. A stripped-down version of the script was shot and edited, with decidedly uneven results. Director Mays and producer Starfelt decided to let the film go and, despite intense interest and numerous rumors to the contrary, have yet to seek distribution.
 Annenberg Medias educational website in September, 2010.  The series, produced by Annie Wong for WGBH Boston, also began airing nationally on PBS in October, 2010. Clips from Mays film were heavily used in the program.

==Related articles and reviews==
*Critic Neal Weaver discusses onstage nudity 
*"Primal Time - Euripides Revisited" - Article by Steven Leigh Morris, LA Weekly, July 11–17, 1987
*"Daring Bacchae Delves Into Modern Psyche" - Phillip Brandes, Los Angeles Times, July 4, 1997

==References==
 

==External links==
 
*  
*Video Clips from  
*Still Photos & Full Video Stream from  
*Stratford Shakespeare Festival Background Book 2008 includes  

 

 
 
 
 