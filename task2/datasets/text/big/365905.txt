Babes in Toyland (1934 film)
{{Infobox film
| name           = Babes in Toyland
| image          = L&H_Babes_in_Toyland_1934.jpg
| caption        = theatrical release poster (1934)
| director       = Gus Meins Charley Rogers
| producer       = Hal Roach Babes in Toyland (1903 operetta) by Glen MacDonough Anna Alice Chapin Frank Butler Nick Grinde Henry Brandon Florence Roberts Virginia Karns
| music          = Victor Herbert (operetta) Frank Churchill Ann Ronell
| cinematography = Francis Corby Art Lloyd
| editing        = Bert Jordan William H. Terhune
| studio         = Hal Roach Studios
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 77 minutes 67 minutes (Britannic cut)
| language       = English
| country        = United States
| budget         =
}}
Babes in Toyland is a Laurel and Hardy musical film released on December 14, 1934. The film is also known by its alternate titles Laurel and Hardy in Toyland, Revenge Is Sweet (the 1948 European reissue title), March of the Wooden Soldiers and Wooden Soldiers (in the United States).
 Babes in computer colorized versions. 

Although the 1934 film makes use of many of the characters in the original play, as well as several of the songs, the plot is almost completely unlike that of the original stage production. In contrast to the stage version, the films story takes place entirely in Toyland, which is inhabited by Mother Goose (Virginia Karns) and other well known fairy tale characters.

==Plot==
 
 Bo Peep Henry Brandon), who is looking to marry Bo Peep. Knowing the Widow Peep is having a difficult time paying the mortgage, Barnaby offers the old woman an ultimatum – unless Bo Peep agrees to marry him he will foreclose on the shoe. Widow Peep refuses, but is worried about where shell get the money to pay the mortgage. Ollie offers her all the money he has stored away in his savings can, only to learn that Stannie has taken it to buy peewees (a favored toy consisting of a wooden peg with tapered ends that rises in the air when struck with a stick near one end and is then caused to fly through the air by being struck again with the stick). He and Stannie set out to get the money for the mortgage from their boss, the Toymaker (William Burress). But Stannie has mixed up an order from Santa Claus (building 100 wooden soldiers at six feet tall, instead of 600 soldiers at one foot tall) and one of the soldiers, when activated, wrecks the toy shop. Stannie and Ollie are fired without getting the money.

The two then hatch a plan to sneak into Barnabys house and steal the mortgage, but are again foiled by their incompetence. Barnaby has them arrested on a burglary charge, and the two are sentenced to be dunked in the ducking stool and then banished to Bogeyland. But Barnaby agrees to drop the charges if Bo Peep will marry him. She reluctantly agrees, but not before Ollie suffers the dunking.

Stannie and Ollie come up with a new scheme. At the wedding, Ollie is present to give the bride away. After the nuptials, but before the ceremonial kiss, Ollie asks for the "wedding present" (the mortgage) from Barnaby. After inspecting it, Ollie tears it up, and then lifts the brides veil — to reveal Stannie, who had worn Bo Peeps wedding dress to the ceremony. Bo Peep is still free, and the mortgage is gone. Ollie teases Stan about having to live with Barnaby as Stan cries saying "I dont LOVE him".

Enraged, Barnaby plots his revenge, eventually hitting on the idea of framing Bo Peeps true love, Tom, Tom, the Pipers Son (Felix Knight), on a trumped-up charge of "pignapping", and getting him banished to Bogeyland. Barnaby proceeds to abduct Little Elmer (Angelo Rossitto), one of the Three Little Pigs, and then has a henchman plant false evidence (including sausage links) in Tom-Toms house. Tom-Tom is put on trial, convicted, and banished to Bogeyland, which he is taken to on a raft by two hooded executioners across an alligator infested river. A distraught but brave Bo Peep follows him.

Meanwhile, Ollie and Stannie find evidence implicating Barnaby in the pignapping, including the fact that the alleged sausage links presented as evidence at Tom-Toms trial are made of beef. They later find the kidnapped pig alive in Barnabys cellar.

A manhunt commences for Barnaby, who flees to Bogeyland through a secret passageway at the bottom of an empty well. Stannie and Ollie eventually follow Barnaby down the well. Meanwhile, Bo Peep crosses the river to Bogeyland, finds Tom-Tom and explains Barnabys trickery to him.

In a sequence cut from many of the later television prints (the version shown on WPIX in New York retains this sequence), Tom-Tom sings Victor Herberts Go to Sleep, Slumber Deep to Bo-Peep in an enormous cave set with giant spider webs. Barnaby catches up to Tom-Tom and Bo-Peep, and attempts to abduct Bo-Peep but gets into a fight with Tom-Tom, who gives Barnaby a well-deserved thrashing.  An enraged Barnaby grabs a large stick and beats a stalactite to summon an army of Bogeymen, who chase Bo-Peep and Tom-Tom through the caverns of Bogeyland. The lovers run into Stannie and Ollie, who help them escape back through the well and are welcomed by the town, who now realize Barnabys treachery.  Barnaby leads an invasion of Toyland on a fleet of rafts in a scene reminiscent of the painting of Washington Crossing the Delaware.

Ollie and Stan tell their story to Old King Cole (Kewpie Morgan), the King of Toyland, and the townspeople as two Bogeymen scale the wall and open the gate. The crowd flees in panic as the army of torch-wielding Bogeymen attacks Toyland. Ollie and Stannie run and hide in the toy shop. There they discover boxes of darts and use them to fight off the Bogeymen, thanks to Stans skill with the game of "peewees" (and help from the three little pigs and the mouse). Stan and Ollie then empty an entire box of darts into a cannon, but as the two search for the last remaining darts, they realize instead that they should activate the wooden soldiers. The "march" alluded to in the films title begins as the soldiers march out of the toy shop (filmed in a stop-motion animation sequence by Roy Seawright ). The scene changes to live action as the soldiers attack the Bogeymen with the bayonets of their rifles. Barnaby is defeated and trapped and covered by blocks that spell "rat", while the Bogeymen are routed and driven back into Bogeyland, where alligators appear to feast on them, although this is never made clear. The kingdom of Toyland is saved. Stan and Ollie decide to give the Bogeymen a parting shot with the dart-filled cannon. As Stan aims the cannon and lights the fuse, and Ollie turns away to avoid the loud blast, the barrel of the cannon flips backwards and unleashes the barrage of darts on Ollie, covering his back with darts. The film ends with Stan pulling them out one by one as Ollie winces.

==Cast==
 

  
* Stan Laurel as Stannie Dum
* Oliver Hardy as Ollie Dee
* Charlotte Henry as Little Bo-Peep
* Felix Knight as Tom, Tom, the Pipers Son|Tom-Tom Piper Henry Brandon as Silas Barnaby Mother Widow Peep
* Virginia Karns as Mother Goose Marie Wilson Mary Quite Contrary
* Johnny Downs as Little Boy Blue Queen of Hearts
 
* Kewpie Morgan as Old King Cole
* Ferdinand Munier as Santa Claus Bobby Shaftoe Simple Simon
* Jean Darling as Goldilocks
* Billy Bletcher as the Chief of Police
* William Burress as the Toymaker Tom Tucker
* Alice Dahl as Little Miss Muffet
* Sumner Getchell as Little Jack Horner
 

==Songs==
The film featured only six musical numbers from the enormous stage score, though that was fitting for a musical with only a 78-minute running time. Included in the film, in the order in which they were performed, were "Toyland" (opening), "Never Mind Bo-Peep", "Castle in Spain", "Go to Sleep (Slumber Deep)", and "March of the Toys" (an instrumental piece). Also included was an instrumental version of "I Cant do the Sum" for the running theme of Laurel and Hardys scenes. The opening song was performed by Mother Goose and an offscreen chorus; most of the rest were sung by Bo Peep and/or Tom-Tom. While none of the songs were performed by Laurel and Hardy, the two briefly danced and marched in a memorable scene to "March of the Toys". Another song, "Whos Afraid of the Big Bad Wolf?" was not one of the original stage songs, but did appear in the Three Little Pigs segment, heard only as an instrumental piece.

==Reception== The Wizard 1961 Disney Technicolor remake, so that those who saw it and then saw the Disney version over the Christmas holiday had the experience of seeing two different versions of the same work within a few weeks of each other.

A holiday staple, many television stations in the United States showed this film near Thanksgiving/Christmas holiday season each year during the 1960s and 1970s. In New York City, it continues to run (as of 2014) on WPIX as March of the Wooden Soldiers, airing on that station in daytime on Thanksgiving Day and Christmas Day.  It also runs nationally on occasion on This TV.

==Bogeyland== remake from cypress trees grow, and many stalactites and stalagmites protrude from its rocky landscape. 

Citizens of Toyland who commit serious crimes are banished to Bogeyland.  Those banished to Bogeyland never return; they are inevitably eaten alive by the bogeymen. The miserly Silas Barnaby (a character based on the English nursery rhyme "There Was A Crooked Man") has a secret tunnel to Bogeyland at the bottom of his well. In the climax of the film, Barnaby leads an army of bogeymen out of Bogeyland in an attempt to conquer Toyland, but is thwarted by Toylands army of wooden soldiers. 

==Alternative versions==
 
 public domain (in the United States), even though the original Babes in Toyland is still under copyright.  When the film was sold to television, the TV prints retained the opening credits music but used specially made for TV opening credits showing puppets of Laurel and Hardy in traditional 1930s attire and hats.

The complete film was restored and colorized for TV showings and video release in 1991 by The Samuel Goldwyn Company.     In 2006, the complete print was again restored and colorized by Legend Films, using the latest technology.      Although the Legend Films release was advertised under its reissue title, both the color and black-and-white prints featured the original title and opening credits.   Both colorized versions correctly depict Laurels hair as being red hair|red, not medium brown as it appears in other colorized Laurel and Hardy movies.

The film has been distributed by many home video companies over the decades. Thunderbird Films released 16&nbsp;mm prints in the 1970s drawn from a heavily spliced (and incomplete) master. An "official" version has been released on DVD by MGM, the films original distributor and now the ancillary rights holder (having inherited the film from the Samuel Goldwyn Company, the former owners of the picture). On September 14, 2010, Legend Films released the movie on Blu-ray titled March of the Wooden Soldiers With Laurel & Hardy. This Blu-ray also contains the animated 1948 short Rudolph the Red-Nosed Reindeer.

==See also==
* Parade of the Wooden Soldiers

==References==
Notes
 

Bibliography
 
* William K. Everson|Everson, William K. The Complete Films of Laurel and Hardy. New York: Citadel, 2000, (first edition 1967). ISBN 0-8065-0146-4.
* Louvish, Simon. Stan and Ollie: The Roots of Comedy. London: Faber & Faber, 2001. ISBN 0-571-21590-4.
* John McCabe (writer)|McCabe, John. Babe: The Life of Oliver Hardy. London:  Robson Books Ltd., 2004. ISBN 1-86105-781-4.
* McCabe, John with Al Kilgore and Richard W. Bann. Laurel & Hardy.  New York: Bonanza Books, 1983, first edition 1975, E.P. Dutton. ISBN 978-0-491-01745-9.
* McGarry, Annie. Laurel & Hardy. London: Bison Group, 1992. ISBN 0-86124-776-0.
 

==External links==
*  
*  
*  
*  
*  
*  
* original film trailer http://www.youtube.com/watch?v=VPf8HTipQxc
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 