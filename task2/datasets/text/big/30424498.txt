Don't Look Back (1999 film)
{{Infobox film
| name           = Dont Look Back
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Akihiko Shiota
| producer       = 
| writer         = 
| screenplay     =
| story          =
| based on       = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1999
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
  is a 1999 Japanese coming-of-age film, directed by Akihiko Shiota, that revolves around a series of incidents that tests the friendship of two ten-year-old best friends.

== Plot Summary ==

Best friends Akira and Koichi, known to get into mischief, are put in different classes when the new school term starts. Akira and Koichi arent concerned as theyre sure they could continue enjoying their mischievous adventures together.

Over next few weeks, Akira makes friends with Shun, a quiet nerd whos socially shunned for not having a father, while Koichi hangs out with Samajima, a charismatic thug whos been transferred from another town. Increasingly concerned that Koichi is spending more time with Samajima than him, Akira turns down Shuns birthday-party invitation to spend time with his best friend Koichi. Meanwhile, Koichi and Samajima go on a petty-crime spree, which excludes and irritates Akira.

When Akira learns that Shuns mentally ill mother has killed him before killing herself, he feels remorse for not being there when Shun needed him. He attempts to reach out to Koichi, but Koichi has problems of his own as he gets in trouble while Samajima abandons him. The final incident Koichi and Akira drives them to confront each other as a test of their friendship.

== Title ==
 Que será, será.

== Awards ==
*Won: 1999 Directors Guild of Japan New Directors Award - Akihiko Shiota 

== References ==
 

== External links ==
*  

 

 
 
 


 