America (2011 film)
 
{{Infobox film
| name           = America
| image          = 
| caption        = 
| director       = Sonia Fritz
| producer       = Frances Lausell
| writer         = Sonia Fritz Miguel Machalski Lymari Nadal
| starring       = Lymari Nadal
| music          = 
| cinematography = Willie Berrios
| editing        = 
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Puerto Rico
| language       = Spanish
| budget         = 
}}
 Best Foreign Language Film at the 84th Academy Awards, but was disqualified because of a rule change.   

==Cast==
* Lymari Nadal as America
* Yancey Arias as Correa
* Yareli Arizmendi as Maria
* Talia Rothenberg as Rosalinda
* Marisé Alvarez as Elena
* Teresa Hernández as Paulina
* Eyra Aguero Joubert as Lourdes
* Edward James Olmos as Mr. Irving
* Frank Perozo as Dario
* Tony Plana as Leopoldo
* Isaac Santiago as NYC Cop#1
* Monica Steuer as Karen Leverett
* Rachel Ticotin as Esther

==See also==
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Puerto Rican submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 

 
 