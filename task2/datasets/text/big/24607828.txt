Veritas, Prince of Truth
{{Infobox Film
| name           = Veritas, Prince of Truth
| image          =Veritas, Prince of Truth.jpeg
| caption        = 
| director       = Arturo Ruiz-Esparza
| producer       = Valentino Lanus Estrella Medina Arturo Ruiz-Esparza Deborah Ruiz-Esparza
| writer         = Billie Letts Deborah Ruiz-Esparza Kate Walsh Danny Strong 
| music          = John Massari
| cinematography = Arturo de la Rosa    
| editing        = Todd Jenkins    
| distributor  =   Vivendi Entertainment Lightyear Entertainment 
| released       = 2007
| runtime        = 93 minutes
| country        = United States Mexico
| language       = English
}} Kate Walsh, Danny Strong and directed by Arturo Ruiz-Esparza.

==Plot==
A young boy must battle an evil menace that could destroy the Earth when Veritas, his favorite comic book hero, comes to life and seeks his help.

==Characters==
Below is a list of the main characters and the actors that play them. 
{| class="wikitable"
|-
! Actor !! Character !! Description
|-
| Sean Patrick Flanery || Veritas/Harvey Wilkins || Veritas The main hero from a comic book. Harvey The creator of the Veritas comics.
|-
| Bret Loehr || Kern Williams || The young boy who Veritas asks for help from.
|-
| Amy Jo Johnson || Marty Williams || Kerns loving mum and animal rescuer.
|-
| Tyler Posey || Mouse Gonzalez || Best friend to Kern and Veritas fan.
|- Kate Walsh || Nemesii || The Main nemesis to Veritas.
|-
| Danny Strong || Raymond Wilkins || Brother to Harvey Wilkins.
|-
| Anthony Zerbe || Porterfield || CEO of Vision Comics. Distributor of the Veritas Comics.
|-
| Arath de la Torre || Danny || 
|-
| Carmen Salinas || Elva Maria || 
|}

== External links ==
* 
* 
*  at Home Media Magazine
*  at dove.org
*  at Rotten Tomatoes (no ratings as of January 2010)
* 

==References==
 

 
 
 
 
 


 