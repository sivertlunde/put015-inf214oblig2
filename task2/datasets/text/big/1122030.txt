Midnight Run
 
{{Infobox film
| name           = Midnight Run
| image          = Midnight Run.jpg
| caption        = Theatrical poster
| director       = Martin Brest
| producer       = Martin Brest
| writer         = George Gallo John Ashton Dennis Farina Joe Pantoliano
| music          = Danny Elfman
| cinematography = Donald E. Thorin
| editing        = Chris Lebenzon Michael Tronick Billy Weber
| distributor    = Universal Pictures
| released       = July 20, 1988
| runtime        = 126 min.
| country        = United States
| language       = English
| budget         = $30 million
| gross          = $81.6 million
|}} John Ashton, Dennis Farina, Joe Pantoliano and Philip Baker Hall play supporting roles.

The film was followed by three made-for-TV sequels in 1994, which did not feature any of the principal actors, although a few characters are carried over from the first film.

==Plot==
Bounty hunter Jack Walsh (De Niro) is enlisted by bail bondsman Eddie Moscone (Pantoliano) to bring accountant Jonathan "The Duke" Mardukas (Grodin) back to L.A. The accountant had embezzled $15 million from Chicago mob boss Jimmy Serrano (Farina) before skipping the $450,000 bail Moscone has posted for him. Jack must bring Mardukas back within five days, or Eddie defaults. Eddie says the job is easy, a "midnight run", but Jack demands $100,000. Jack is then approached by FBI Agent Alonzo Mosely (Kotto), who wants Mardukas to be a witness against Serrano and orders Jack to keep away from Mardukas, but Jack takes no notice and instead steals Moselys ID, which he uses to pass himself off as an FBI agent along his journey. At Kennedy airport in New York, Serrano’s henchmen Tony (Foronjy) and Joey (Miranda) offer Jack $1 million to turn Mardukas over to them, but he turns them down.

Jack takes custody of Mardukas in New York and calls Eddie from the airport, not knowing that Eddies line is tapped by the FBI and Jerry (Kehoe), Eddies assistant, is secretly tipping Serranos men off for money. However, Mardukas has a panic attack on the plane, forcing him and Jack to travel via train. When Jack and Mardukas fail to show up, Eddie brings in rival bounty hunter Marvin Dorfler (Ashton) to find them. Marvin uses Jacks credit card number to find out where they are and then has the card cancelled. Jack is able to get the drop on Marvin and leaves the train, but without funds, he is forced to rely on other means to get across the country, including stealing cars, borrowing his ex-wife’s (Phillips) car in Chicago, and hitchhiking. Meanwhile, the skirmish on the train reaches Moselys ears and he leads a task force to find Jack and Mardukas.

Mardukas tries to get to know Jack, who eventually reveals that, ten years before, he was an undercover officer in Chicago trying to get close to a drug dealer who had almost the entire force on his payroll. Eventually, just as Walsh was going to make the arrest, the drug dealer had some heroin planted in his house by corrupt cops. In order to avoid prison and working for the dealer, Walsh left Chicago and became a bounty hunter, while his wife divorced him and married the corrupt lieutenant who fired him. Since then, however, Jack has lived with a sense of hope that he will one day be reunited with his ex-wife. Later on, Mardukas learns that the drug dealer was Serrano himself.

In Arizona, Marvin catches up with them and takes Mardukas away from Jack, who is found by Mosely. While arguing with Moscone over the phone, Jack realizes that Marvin intends to turn Mardukas over to Serrano for $2 million, though Marvin is betrayed and knocked out by Serranos men after accidentally revealing where he was keeping Mardukas. Jack calls Serranos men and bluffs that he has computer disks created by Mardukas with enough information to put Serrano away, but promises to hand the disks over if Serrano returns Mardukas to him unharmed. At McCarran Airport, Jack meets up with Serrano while wearing a wire and being watched by the FBI. Marvin, at the airport to fly home, spots Mardukas and interrupts the exchange. Marvin pushes Jack and unknowingly disables the wire. At the last minute, Jack yells that Serrano has the disks; the FBI closes in, arresting Serrano and his henchmen. Mosely turns Mardukas over to Jack with enough time to return him to L.A. by the deadline.

In L.A., Jack calls Eddie to tell him that he has Mardukas, but that he is letting him go. Before parting, Mardukas gives Jack $300,000 in a money belt he had been hiding, while Jack gives Mardukas his old watch that his wife gave him before their marriage, symbolizing that he has finally let go of her. Jack flags down a taxi and asks the driver if he has change for a $1,000 bill, but the taxi drives away, so he starts walking home.

==Cast==
* Robert De Niro as Jack Walsh
* Charles Grodin as Jonathan "The Duke" Mardukas
* Yaphet Kotto as FBI Special Agent Alonzo Mosely John Ashton as Marvin Dorfler
* Dennis Farina as Jimmy Serrano
* Joe Pantoliano as Eddie Moscone
* Richard Foronjy as Tony
* Robert Miranda as Joey
* Jack Kehoe as Jerry Geisler
* Wendy Phillips as Gail
* Danielle DuClos as Denise Walsh
* Philip Baker Hall as Sidney
* Tom McCleister as Bill "Red" Wood

==Production== The Untouchables, De Niro wanted to try something different and decided on appearing in a comedy. {{cite news
| last = Parker
| first = John
| coauthors =
| title = De Niro
| work =
| pages =
| publisher = Victor Gollancz
| date = 1995
| url =
| accessdate =  UIP partner Universal Studios became interested in the project.  Paramount president Ned Tanen claimed that the budget became too high and he decided that "it wasnt worth it". {{cite news
| last =
| first =
| coauthors =
| title = De Niro is Making the Publicity Rounds
| work = St. Petersburg Times
| pages = 3D
| publisher =
| date = May 23, 1988
| url =
| accessdate = 
}} 

To research for his role, De Niro worked with real-life bounty hunters and police officers. {{cite news
| last = ORegan
| first = Michael
| coauthors =
| title = The Private De Niro
| work = Sunday Mail
| pages =
| publisher =
| date = July 17, 1988
| url =
| accessdate = 
}}  As Jack uncuffs the Duke on the train, the Duke says, "Thanks, cause theyre starting to cut into my wrists." In fact, Grodin has permanent scars resulting from the handcuffs he had to wear for most of the film. {{cite news
| last = Grodin
| first = Charles
| coauthors =
| title = It Would Be So Nice If You Werent Here
| work =
| pages =
| publisher = William & Morrow & Company, Inc.
| date = 1989
| url =
| accessdate = 
}}  The scene where Grodin fell off a cliff was shot on location in the Salt River Canyon in White Mountain, Arizona and the conclusion, taking place in rapids, was shot in New Zealand because the water was too cold in Arizona. {{cite news
| last = van Gelder
| first = Laurence
| coauthors =
| title = Off a Cliff, Across an Ocean: Splash!
| work = The New York Times
| pages = 19
| publisher =
| date = July 21, 1988
| url =
| accessdate = 
}} 

Universal invested $15 million in a print and television advertising campaign. 

==Soundtrack==
The films score was composed by Danny Elfman, with the album released by MCA Records.
# Walsh Gets the Duke (1:47)
# Main Titles (2:21)
# Stairway Chase (:54)
# J.W. Gets a Plan (1:41)
# Gears Spin I (:54)
# Dorflers Theme (1:24)
# F.B.I. (1:16)
# Package Deal (1:07)
# Mobocopter (2:42)
# Freight Train Hop (1:18)
# Drive to Reds (1:04)
# In the Next Life (1:06)
# The River (1:19)
# The Wild Ride (1:31)
# Amarillo Dawn (:26)
# Potato Walk (1:09)
# Desert Run (1:09)
# Diner Blues (1:19)
# Dorflers Problem (1:01)
# Gears Spin II (1:30)
# The Confrontation (2:30)
# The Longest Walk (1:32)
# Walsh Frees the Duke (2:44) Mosley & The B-Men (4:16)

Note: The end credits track as heard in the film is instrumental.

==Reaction==
===Box office=== USD $5.5 million in its opening weekend. It went on to make $38.4 million in North America and $43.2 million in the rest of the world for a worldwide total of $81.6 million. {{cite news
| last =
| first =
| coauthors =
| title = Midnight Run
| work = Box Office Mojo
| pages =
| publisher = IMDb
| date =
| url = http://www.boxofficemojo.com/movies/?id=midnightrun.htm
| accessdate = 2008-12-18 
}} 

===Reviews===
Film critic Roger Ebert gave the film 3.5 out of 4 stars and wrote, "What Midnight Run does with these two characters is astonishing, because its accomplished within the structure of a comic thriller ... Its rare for a thriller to end with a scene of genuinely moving intimacy, but this one does, and it earns it." {{cite news
| last = Ebert
| first = Roger
| coauthors =
| title = Midnight Run
| work = Chicago Sun-Times
| pages =
| publisher =
| date = July 20, 1988
| url = http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/19880720/REVIEWS/807200301/1023
| accessdate = 2008-12-18 
}}  In his review for   stopped playing himself". {{cite news
| last = Scott
| first = Jay
| coauthors =
| title = Midnight Run
| work = The Globe and Mail
| pages =
| publisher =
| date = July 20, 1988
| url =
| accessdate = 
}}  Vincent Canby, in his review for The New York Times, wrote, "Mr. De Niro and Mr. Grodin are lunatic delights, which is somewhat more than can be said for the movie, whose mechanics keep getting in the way of the performances". {{cite news
| last = Canby
| first = Vincent
| coauthors =
| title = De Niro and Grodin in Cross-Country Chase
| work = The New York Times
| pages =
| publisher =
| date = July 20, 1988
| url = http://movies.nytimes.com/movie/review?res=940DE5DA1E30F933A15754C0A96E948260
| accessdate = 2009-03-16 
}}  In his review for The Washington Post, Hal Hinson says of the director that, "carrying the dead weight of George Gallos script, Brest isnt up to the strenuous task of transforming his uninspired genre material in   something deeper, and so the attempts to mix pathos with comedy strike us merely as wild and disorienting vacillations in tone". {{cite news
| last = Hinson
| first = Hal
| coauthors =
| title = Random Bounty
| work = The Washington Post
| pages =
| publisher =
| date = July 20, 1988
| url = http://www.washingtonpost.com/wp-srv/style/longterm/movies/videos/midnightrunrhinson_a0a8d0.htm
| accessdate = 2008-12-18 
}}  David Ansen, in his review for Newsweek, wrote, "The outline of George Gallos script—odd-couple antagonists become buddies under perilous circumstances— was stale five years ago, and the outcome offers no surprises. Too bad: a lot of good work has been wasted on an unworthy cause". {{cite news
| last = Ansen
| first = David
| coauthors =
| title = Reactivating Action Heroes
| work = Newsweek
| pages =
| publisher =
| date = July 25, 1988
| url =
| accessdate = 
}}  Midnight Run has a 95% score at Rotten Tomatoes based on 44 reviews. 

==Sequels==
===TV===
* Another Midnight Run
* Midnight Runaround
* Midnight Run for Your Life

===Proposed second film===
In 2010 it was announced that Universal Pictures has hired Tim Dowling to write a sequel with Robert De Niro reprising his role as Jack Walsh. In addition to starring, the actor will also be producing the film with Jane Rosenthal. Charles Grodin may be reprising his role. Martin Brest might be returning to direct the sequel. {{cite news
| last = Kit
| first = Borys
| coauthors =
| title = Universal taking another Midnight Run
| work = The Hollywood Reporter
| pages =
| language =
| publisher =
| date = March 5, 2010
| url = http://www.hollywoodreporter.com/hr/content_display/news/e3ie0341f942810261f8e6808e11c31e407
| accessdate = 2010-03-08 
}}   
However, as of 2014, no further announcements have been made.

==See also==
* List of American films of 1988

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 