Hair (film)
{{Infobox film
| name           = Hair
| image          = Hairmovieposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster by Bill Gold
| director       = Miloš Forman Michael Butler Lester Persky
| screenplay     = Michael Weller
| based on       =   John Savage Treat Williams Beverly DAngelo
| music          = Galt MacDermot
| cinematography = Miroslav Ondříček
| editing        = Alan Heim Stanley Warnow
| distributor    = United Artists
| released       =  
| runtime        = 121 minutes  
| country        = United States
| language       = English
| budget         = $11 million
| gross          = $15,284,643
}} musical War war comedy-drama musical of draftee who meets and befriends a tribe of long-haired hippies on his way to the army induction center. The hippies introduce him to their environment of marijuana, LSD, unorthodox relationships and draft dodging.
 John Savage, Richard Bright, Best Motion New Star of the Year in a Motion Picture (for Williams).

==Plot== Army and serve in the Vietnam War. In Central Park, he meets a troupe of free-spirited hippies led by George Berger, a young man who introduces him to debutante Sheila Franklin when they crash a dinner party at her home. Inevitably, Claude is sent off to recruit training in Nevada, but Berger and his band of merry pranksters - including Woof Daschund, LaFayette "Hud" Johnson, and pregnant Jeannie Ryan - follow him to give a sendoff. They are met at the bases main gate by a surly MP, who doesnt like their looks and demands that they leave. Accordingly, Sheila flirts with an off-duty Sergeant in order to steal his uniform, which she gives to Berger. He uses it to extract Claude from the base for a last meeting with Sheila, taking his place. However, while Claude is away, the unit is suddenly rallied and flown out to Vietnam; Berger, whose ruse is somehow never detected, is taken with them. The film ends with the main cast singing at Bergers grave, followed by scenes of a large anti-war protest outside the White House in Washington, DC.

==Cast== John Savage as Claude Hooper Bukowski
* Treat Williams as George Berger
* Beverly DAngelo as Sheila Franklin
* Annie Golden as Jeannie Ryan
* Dorsey Wright as LaFayette "Hud" Johnson
* Don Dacus as Woof Daschund
* Nell Carter as Central Park singer ("Aint Got No" & "White Boys")
* Cheryl Barnes as Huds fiancée Richard Bright as Fenton
* Ellen Foley as Black Boys
* Charlotte Rae as Lady in Pink
* Laurie Beechman as Black Boys
* Nicholas Ray as The General
* Michael Jeter as Woodrow Sheldon

==Differences from original version== psychedelically inspired peace movement, as well as the love relationships among the Tribe members, while the film focuses on the carefree antics of the hippies. 

The film omits the songs "The Bed", "Dead End", "Oh Great God of Power", "I Believe in Love", "Going Down", "Air", "My Conviction", "Abie Baby", "Frank Mills", and "What a Piece of Work is Man" from the musical. The latter five songs were originally recorded for the film, but were eventually cut, as they slowed the pace of the film. They can be found on the  .  A few verses from the songs "Manchester, England" and a small portion of "Walking in Space" have been removed. While the songs "Dont Put It Down" and "Somebody to Love" are not sung by characters in the film, they are both used as background or instrumental music for scenes at the army base. A new song written by MacDermot for the film is "Somebody to Love". There are several other differences from songs in the movie and as they appear on the soundtrack, mainly in omitted verses and different orchestrations. One notable difference is that the Broadway version used only a jazz combo while the movie soundtrack boasts orchestrations that make ample use of full horn and string sections. Ruhlmann, William.  . Allmusic.com,  Many of the songs have been shortened, sped up, rearranged, or assigned to different characters to allow for the differences in plot.

==Reaction== original musical peace movement. Horn, pp. 117–18   They stated: "Any resemblance between the 1979 film and the original Biltmore version, other than some of the songs, the names of the characters, and a common title, eludes us."  In their view, the screen version of Hair has not yet been produced. 
 A Hard Time Out, National Lampoon parody of some ghastly Swinging Sixties compendium." 

The film was shown out of competition at the 1979 Cannes Film Festival.   

==Awards== Best Motion New Star Best Foreign 1980 César Awards, losing to Woody Allens Manhattan (film)|Manhattan.
 moral rights to the film to the studio as eventually leading to his 1997 John Huston Award for Artists Rights {{cite web| url=
http://www.variety.com/article/VR1117434463 | date= April 20, 1997| title= Artists vs. Solons: Helmer Forman feted for rights fight | publisher= Variety |accessdate= 2011-07-05}}  from the Film Foundation:  syndicated television where I didnt have that right.  What happened:  the film played on 115 syndicated stations practically all over the United States, and its a musical. Out of 22 musical numbers, 11 musical numbers were cut out from the film, and yet it was still presented as a Milos Forman film, Hair. It was totally incomprehensible, jibberish, butchered beyond belief...

==Soundtrack==
{{Track listing
| headline   = Disc One
| all_lyrics = Gerome Ragni, Jim Rado
| all_music  =  Galt MacDermot title1 = Aquarius
|note1 = Ren Woods length1 = 4:47 title2 = Sodomy length2 = 1:30 title3 = Donna/Hashish length3 = 4:19 title4 = Colored Spade length4 = 1:34 title5 = Manchester note5 = John Savage length5 = 1:58 title6 = Abie Baby/Fourscore note6 = Nell Carter length6 = 2:43 title7 = Im Black/Aint Got No length7 = 2:24 title8 = Air length8 = 1:27 title9 = Party Music length9 = 3:26 title10 = My Conviction length10 = 1:46 title11 =  I Got Life note11 = Treat Williams length11 = 2:16 title12 = Frank Mills length12 = 2:39 title13 = Hair
|length13 = 2:43 title14 = L.B.J. length14 = 1:09 title15 = Electric Blues/Old Fashioned Melody length15 = 3:50 title16 = Hare Krishna length16 = 3:20
}}

{{Track listing
| headline   = Disc Two title1 = Where Do I Go? length1 = 2:50 title2 = Black Boys length2 = 1:12 title3 = White Boys note3 = Nell Carter length3 = 2:36 title4 = Walking in Space (My Body) length4 = 6:12 title5 = Easy to Be Hard note5 = Cheryl Barnes length5 = 3:39 title6 = Three-Five-Zero-Zero length6 = 3:49 title7 = Good Morning Starshine note7 = Beverly DAngelo length7 = 2:24 title8 = What a Piece of Work is Man length8 = 1:39 title9 = Somebody to Love length9 = 4:10 title10 = Dont Put It Down length10 = 2:25 title11 = The Flesh Failures/Let the Sunshine In length11 = 6:06
}}

==See also==
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   from trailerfan.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 