Margin for Error
 
 
{{Infobox film
| name           = Margin for Error
| image          = Margin for Error 1943 poster.jpg
| image_size     = 200px
| caption        = US Theatrical Poster
|director=Otto Preminger
|producer=Ralph Dietrich 
|writer= Lillie Hayward Samuel Fuller Based on the play by Clare Boothe Luce  
|starring=Joan Bennett Milton Berle Otto Preminger
|music=Leigh Harline  
|cinematography=Edward Cronjager  
|editing=Louis R. Loeffler
|distributor=20th Century Fox 
|released= 
|country=United States
|runtime=74 minutes
|language=English
}}         

Margin for Error is a 1943 American drama film directed by Otto Preminger. The screenplay by Lillie Hayward and Samuel Fuller is based on the 1939 play of the same title by Clare Boothe Luce.

==Plot== consul Karl Baumer by the mayor of New York City, he turns in his badge because the man is a Nazi. The mayor tells Moe although he personally is opposed to Adolf Hitler and his regime, he is under special orders from the Berlin government to halt demonstrations against Nazi sympathizers and organizers, and he feels he will demonstrate the American system of democracy if he obeys their wishes.

Moe quickly discovers Baumer is in trouble with Berlin for having squandered money intended to finance sabotage. His secretary, Baron Max von Alvenstor, has become disenchanted with his boss and refuses to stall the delivery of a damaging financial report to Berlin. Baumers Czechoslovakian wife Sophia confesses to Moe she loathes her husband and married him only to secure her fathers release from prison. Also at odds with Baumer is Otto Horst, who has been ordered to procure false identification cards for German saboteurs assigned to blow up an American port at the end of a radio broadcast delivered by Hitler.

Under orders from Berlin to dispense with Horst, Baumer plots to frame Max for the mans murder and tries to enlist Sophias help, but she warns Horst of the scheme, so he begins to carry a gun for protection. While listening to the radio speech with her husband, Horst, and Max, Sophia grabs Horsts gun and kills Baumer. Max urges Sophia to escape before anyone sees her.

Moe discovers the body and begins to question suspects, including Sophia, who readily confesses to the crime, but Max insists it was he who killed Baumer. Moe reveals Baumer not only was shot, but stabbed and poisoned as well. Meanwhile, Max rushes to the port where the saboteurs are concealed and orders them to dismantle the bomb. With only minutes to spare, the bomb is dismantled and the saboteurs are captured. Returning to the consulate, Max identifies Horst as an accomplice to the saboteurs, and Horst is arrested.

A coroners report determines Baumer died because he accidentally drank from a glass he had laced with poison intended for Max.

==Cast==
*Milton Berle ..... Moe Finkelstein 
*Joan Bennett ..... Sophia Baumer 
*Otto Preminger ..... Karl Baumer 
*Carl Esmond ..... Baron Max von Alvenstor 
*Howard Freeman ..... Otto Horst
*Clyde Fillmore ..... Dr. Jennings

==Sources==
The original play was based on an incident that occurred in 1938, when New York Mayor Fiorello La Guardia appointed Police Captain Max Finkelstein to head a special squad of Jewish officers tasked with protecting the German consulate in the city from protestors. The police officer characters name was originally Max Finkelstein, but was changed to Moe Finkelstein after the real Finkelsteins suicide in May 1940. 

==Production== Broadway production Plymouth Theatre, where it ran for 264 performances,  and he reprised the role for a national tour in the summer of 1940. 

According to the New York Times, 20th Century Fox purchased the screen rights for $25,000 in the spring of 1941 but temporarily shelved the property because studio executives felt Boothes "statement of the opposition between fascism and democracy had become self-evident to the point of banality."   In April 1942, William Goetz, serving as interim studio head while Darryl F. Zanuck was fulfilling his military duty, greenlighted the project and assigned it to director Ernst Lubitsch. Goetz wanted Preminger to reprise his role of Baumer, but Preminger insisted he wanted to direct as well. When Goetz refused, Preminger offered to direct for free and agreed to withdraw from helming the film but remain as Baumer if Goetz was unhappy with his work at the end of the first week of filming, and Goetz agreed. 
 flashback to the period prior to Americas entry in the war. Principal photography began on September 28, 1942, and at the end of the first week, Goetz told the director he was so pleased with the dailies he was offering him a seven-year contract as director and actor. Preminger requested producing rights as well, and the deal was sealed. He completed filming on November 5, on schedule and only slightly over budget. 

==Critical reception==
Theodore Strauss of the New York Times observed, "Less than brilliant when done on Broadway, the script is now painfully dated. The Nazis certainly are not less villainous, but as they are shown in the film they are much less interesting. Practically every character and situation has long been a cliché of anti-Nazi films generally . . . There are other examples of worn conventions. Margin for Error tells us nothing new and tells it very dully . . . As a story the film has practically no suspense. It is not greatly helped by the tediously bombastic style of Otto Preminger as the consul nor by Joan Bennett as his suffering wife. Poor Milton Berle . . . is forced to forsake his comic antics and make sweet speeches on the benefits of democracy, a role for which Mr. Berle seems way out of line. For that matter, Margin for Error is way out of line as well."  

Alexander Larman of Channel 4 rated the film three out of five stars and noted, "Otto Preminger is rightly regarded as one of the most talented émigré directors to have had a successful career in post-war American film. However, Margin For Error, while undeniably entertaining in a B-movie manner, is hardly indicative of his talent, suffering from a plot that alternates between cliche and head-scratching reversals, some unimpressive acting and a limp denouement."  

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 