Bright Eyes (1934 film)
{{Infobox film
| name           = Bright Eyes
| image          = Brighteyes.jpg
| caption        = Theatrical poster David Butler
| producer       = Sol M. Wurtzel
| writer         = David Butler  Edwin Burke
| screenplay     = William Conselman Henry Johnson
| based on       =  James Dunn
| music          = Richard A. Whiting  Samuel Kaylin Arthur Miller
| editing        = Fox Film
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = United States dollar|US$190,000   
}}

Bright Eyes is a 1934 American comedy drama film directed by David Butler.  The screenplay by William Conselman is based on a story by David Butler and Edwin Burke, and focuses on the relationship between bachelor aviator James "Loop" Merritt (James Dunn) and his orphaned godchild, Shirley Blake (Shirley Temple).  Merritt becomes involved in a custody battle for her with a rich, elderly gentleman.  The film featured one musical number, "On the Good Ship Lollipop".

Bright Eyes was the first film to be written and developed specifically for Temple,  and the first in which her name was raised above the title.  In February 1935, she received a special Academy Award for her 1934 contributions to film,  particularly Little Miss Marker and Bright Eyes.  In 2009, the film was available on VHS and DVD in both black and white and colorized versions.

==Plot== Lois Wilson), James Dunn), Toto in The Wizard of Oz.  After Christmas morning she hitches a ride to the airport. The aviators bring her aboard a ship and taxi her around the runways, where she serenades them with her rendition of On the Good Ship Lollipop
 traffic accident, the Smythes make plans to send Shirley to an orphanage. When Loop hears about this he takes her up to Heaven for a short amount of time. She learns that her mother died, despite hearing this she starts crying and they go back down to earth. However, Uncle Ned (Charles Sellon), the cranky, wheelchair-bound patriarch of the Smythes, is fond of little "Bright Eyes" (as he calls her) and insists that she remain in the house. His relatives grudgingly comply with his wishes, although they make her feel unwelcome. A custody battle for her ensues between Loop and Uncle Ned. The impasse is resolved when Loop, his fiancée, Adele (Judith Allen), Uncle Ned, and Shirley all decide to live together.

==Cast==
* Shirley Temple as Shirley Blake, the five-year-old daughter of Mary Blake  James Dunn as James "Loop" Merritt, a bachelor pilot and Shirley’s godfather   Lois Wilson as Mary Blake, a widow, Shirley’s mother, and a maid in the Smythe familys home  
* Judith Allen as Adele Martin, a socialite, Loop’s estranged sweetheart, and eventually his fiancée and Shirleys godmother 
* Charles Sellon as Uncle Ned Smith, the Smythes cranky patriarch   Theodor von Eltz as J. Wellington Smythe, a haughty nouveau-riche
* Dorothy Christy as Anita Smythe, J. Wellington Smythe’s wife
* Jane Withers as Joy Smythe, J. Wellington and Anita Smythes spoiled and obnoxious daughter
* Brandon Hurst as Higgins, the Smythes butler 
* Jane Darwell as Elizabeth Higgins, the Smythes cook  
* Walter Johnson as Thomas, the Smythes chauffeur  George Irving as Judge Thompson Terry the dog as Rags, Loops dog

==Production==
American Airlines and the Douglas Aircraft Company, recognizing the potential of the film in advertising air travel, cooperated in the production and distribution. They provided a DC-2 aircraft for the exterior shots while a true to scale mock up was provided for the interior scenes. In the famous Good Ship Lollipop scene, members of the University of Southern California football team served as extras. In the second flying scene where Temples character sneaks aboard the plane and they were forced to bail out of it, both Temple and Dunn were strapped into a harness hoisted up into the studio rafters. They were supposed to drift down with the aid of a wind machine. In the first take, someone inadvertently opened an airproof door just as they landed, creating a vacuum that sucked out the parachute and dragged them both across the studio floor. This movie would also be the first movie in which Mary Lou Isleib would serve as a stand-in for Temple. She would remain as Temples stand-in for the rest of her tenure at 20th Century Fox. 

When Temple’s mother, Gertrude, read the script, she tried to persuade Fox Film production head Winfield Sheehan to trim the role of Joy Smythe, a rich, mean, snobbish child and the complete opposite of Shirley’s winsome, lovable character. He, however, would not do so, believing the contrast between the two girls would enhance audience sympathy for Temple’s character. 

Thirty girls auditioned for the role of Joy with the part being given to eight-year-old Jane Withers, an experienced stage performer but a relative newcomer to films. Gertrude Temple hovered ever closer to Shirley as filming began and ordered Withers to wash her hands before performing in any scene with her daughter.  Director Butler later told Withers, "You stole the picture".     In a 2006 interview on Turner Classic Movies|TCMs  , Withers recalled that she was hesitant to take the role because she had to be so "mean" to Temple and the public would hate her for it.

==Release==

===Critical reception===
Andre Sennwald in his December 21, 1934 New York Times review praised Dunn, Wilson, and Withers. Sellon was singled out for his "great humorous skill" in portraying crotchety Uncle Ned. Sennwald thought the film was at its best during Temple’s delivery of the Lollipop song and at its worst in the scenes involving the villainous Smythes, who, for him, were so over-the-top as to be unrecognizable as human beings. He decided the film was composed of "old standbys of the hearts-and-flowers drama", and noted that, “Shirley romps through all her assignments with such persuasive charm and enkindling naturalness that she succeeds in being refreshing even in her most painfully arranged scenes." 

Film commentator Hal Erickson writes the film is "arguably the best of Shirley Temples 1930s vehicles", and thinks Jane Withers "terrific" as the film’s villainess.  He notes that some critics believed Withers stole the show, and it was this  "as much as anything else, that earned Withers her own starring series at 20th Century-Fox". 

===Awards===
Temple received a miniature Oscar on February 27, 1935 for her contributions to film entertainment in 1934, chiefly for Little Miss Marker and Bright Eyes. She was the first child actor to receive an Academy Award.  

===Home media===
The original black and white film and a colorized version were available on both videocassette and DVD in 2008.   Some versions included theatrical trailers and other special features.

==Soundtrack==
* On the Good Ship Lollipop (1934) (uncredited)
** Music by Richard A. Whiting
** Lyrics by Sidney Clare

* Silent Night (1818) (uncredited) Franz Gruber
** Lyrics by Joseph Mohr

* The Man on the Flying Trapeze (1867) (uncredited)
** Music by Gaston Lyle
** Lyrics by George Leybourne
** Sung a cappella by Charles Sellon

* Jingle Bells (1857) (uncredited) James Pierpont

==See also==
* Shirley Temple filmography

==References==
;Notes
 

;Footnotes
 

;Works cited
 
*  
*  
*   
 

;Bibliography
 
*    The author expounds upon father figures in Temple films.
*   In the essay, "Cuteness and Commodity Aesthetics: Tom Thumb and Shirley Temple", author Lori Merish examines the cult of cuteness in America.
*    The author presents an examination of social class in Bright Eyes.
 

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 