Adventures of Serial Buddies
{{Infobox film
| name           = Adventures of Serial Buddies
| image          = SerialBuddiesPoster.jpg
| caption        = Original film poster
| director       = Keven Undergaro
| producer       = John Comerford  Ian Keiser Thomas Lynch Maria Menounos Aaron Strongoni 
| writer         = Keven Undergaro
| starring       = Henry Winkler Christopher Lloyd Beth Behrs Christopher McDonald Maria Menounos Kathie Lee Gifford Artie Lange David Proval Cassidy Gifford Gheorghe Muresan Richard Christy Paul Ashton Hal Rudnick Gian Molina Todd Wilson John Comerford
| music          = Giulio Carmassi Kush Mody
| cinematography = Anka Malatynska
| editing        = John Comerford Phil Svitek Keven Undergaro
| studio         = Underman/Omegagirl 
| distributor    = 
| released       =  
| runtime        = 91 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          =
}}
Adventures of Serial Buddies is a 2011 film written and directed by Keven Undergaro. 

==Synopsis==
Gregory is a catholic who is determined to rid the world of evil people, Gary assembles trophies and wants to kill possible trophy winners, Graham serves as Garys understudy and will stop at nothing to become a trophy maker himself, while Vinny Van Go has an affinity for visiting accident scenes with the purpose of making friends. These idiots are forced to join forces and embark on a riotous road trip journey—which crosses paths with everything from skinheads and frat jocks to bikers and babes. And dont forget about the clowns!  

==Cast==
*Henry Winkler as the Narrator
*Christopher Lloyd as Dr. Von Gearheart
*Beth Behrs as Brittany   
*Christopher McDonald as Father Christopher
*Maria Menounos as Katelyn 
*Kathie Lee Gifford as Kathie Lee Gifford 
*Artie Lange as Golden Graham 
*David Proval as Big Chicken 
*Cassidy Gifford as Hannah 
*Gheorghe Muresan as Paul of the Shed
*Richard Christy as Skinhead leader 
*Paul Ashton as Gregory
*Hal Rudnick as Gary
*Gian Molina as Vinny
*Todd Wilson as Graham
*John Comerford as Garth / Lil Bozo / Ron A Roll

==Production==
The idea for the film was inspired by a man director Undergaro was friends with, who unknowingly turned out to be a sociopath. Shooting was completed in a brisk four-week span, and the film  is known for discovering actress Beth Behrs before landing her breakout role on CBS Two Broke Girls. 

It is also credited with being the first "serial killer buddy comedy". 

==Reception==
John Campea of AMC Movie News has described the film as "Dexter meets Dumb and Dumber." 

Critic Scott Mantz has called the film "seriously funny. Wacky twisted fun." And entertainment journalist Ben Lyons has said the film is a "fearless farce for fans of funny." 

==References==
 

== External links ==
* 

 
 
 
 