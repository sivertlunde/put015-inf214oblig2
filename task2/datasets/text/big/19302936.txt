Mary, Queen of Tots
 
{{Infobox film
| name           = Mary, Queen of Tots
| image          = 
| caption        = 
| director       = Robert F. McGowan
| producer       = Hal Roach F. Richard Jones
| writer         = Hal Roach H. M. Walker
| starring       = 
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 42nd Our Gang short subject released.

==Cast==

===The Gang===
* Joe Cobb - Joe
* Jackie Condon - Jackie
* Mickey Daniels - Mickey
* Allen Hoskins - Farina
* Mary Kornman - Mary
* Pal the Dog as Himself

===Additional cast===
* Charles A. Bachman - Officer
* May Beatty - Governess
* Richard Daniels - Kindly gardener James Finlayson - Radio station actor
* Helen Gilmore - Dollmakers wife
* Lyle Tayo - Mrs. Newman, Marys mother
* Charley Young - Dollmaker

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 