Le Bossu (1959 film)
{{Infobox film
| name           = Le bossu
| image          = 
| caption        = 
| director       = André Hunebelle
| producer       = Paul Cadeac dArbaud
| writer         = Jean Halain     Pierre Foucaud  André Hunebelle 
| based on       =  | }}
| starring       = Jean Marais Bourvil Sabine Sesselmann
| music          =  Jean Marion
| cinematography = Marcel Grignon
| editing        = Jean Feyte
| distributor    = 
| released       =       
| runtime        = 104 minutes
| country        = France  Italy
| awards         = 
| language       = French language| French
| budget         = 
}}
 Captain Blood. 
 
==Plot==
Duke Philippe de Nevers (Hubert Noël) is an influential and popular man who is married to a beautiful wife called Isabelle (Sabine Sesselmann). His rival Philippe de Gonzague (François Chaumette) hates him enough to organise an attempt on him. The Duke is accompagnied by Henri de Lagardère (Jean Marais) when de Gonzagues henchmen altogether attack him. Lagardère cannot save his friend because the both of them are hopelessly outnumbered. He has to escape in order to save the Dukes daughter and swears revenge. Together with his old buddy Passepoil (Bourvil) he raises the little girl in Spain. At the same time he returns frequently to France where he detects confronts his friends murderers and puts them to the sword one by one until only their former leader is left. Finally he discovers that Philippe de Gonzague is the man for whom he is looking.

==Production==
The film was shot between May 19 and July 28 in the "Franstudio" of Saint-Maurice, Val-de-Marne|Saint-Maurice and in the  Pyrénées-Orientales. The scene showing how three henchmen are sent to Spain for Lagardère and the dukes daughter were  shot at the Pont du Diable (Céret). 
Maître darmes André Gardère was the accountable instructor concerning the  Stage combat|  choreography of all  French school of fencing | fencing scenes.  
Guy Delorme, who would play Comte de Rochefort in Bernard Borderies  The Three Musketeers (1961 film)| 1961 version of The Three Musketeers appears here as a henchman. 
Sabine Sesselmann was dubbed by Gilberte Aubry (as Aurore de Nevers) and Jacqueline Porel (as Isabelle de Caylus).

==Cast==
{|class="wikitable"
|-
!Actor!!Character
|- Jean Marais|| Henri de Lagardère alias le Bossu
|-
|Bourvil|| Passepoil
|- Sabine Sesselmann Aurore de Nevers   Isabelle de Caylus
|-
|François Chaumette|| Philippe de Gonzague
|- Jean Le Monsieur de Peyrolles
|- Hubert Noël || Duke Philippe de Nevers
|- Paulette Dubost || Lady Marthe
|- Paul Cambo  || Philippe II, Duke of Orléans
|- Edmond Beauchamp || Don Miguel
|- Georges Douking  || Marquis de Caylus
|- Guy Delorme || henchman
|-
|}

==References==
 

This article incorporates information from the French Wikipedia.

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 