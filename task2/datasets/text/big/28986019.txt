Casanovva
 
 
{{Infobox film
| name           = Casanovva
| image          = Casanovva film poster.jpg
| alt            =  
| caption        = Theatrical poster
| satellite rights = surya tv
| director       = Rosshan Andrrews
| producer       = C. J. Roy Antony Perumbavoor
| writer         = Bobby Sanjay Roma Asrani Sanjana
| music          = Gopi Sundar Alphons Joseph Gowri Lakshmi
| cinematography = Ganesh Rajavelu 
| editing        = Mahesh Narayanan
| studio         = Confident Entertainment Aashirvad Cinemas
| lyrics         = Jelu Jeyaraj
| distributor    = PJ Entertainments Maxlab Cinemas and Entertainments
| released       =  
| runtime        = 169 min
| country        = India
| language       = Malayalam
| budget         =  
| gross          = 
}} romantic action Malayalam film Shankar forms an ensemble cast along with five debutantes, Abhishek, Shamsi, Arjun, Vikram and Midlaj. The films songs were composed by Gopi Sundar, Alphons Joseph and Gowri Lakshmi and the background score was done by Gopi Sundar. It is director Rosshan Andrrewss third film with Mohanlal after Udayananu Tharam and Evidam Swargamanu.  The film was released on 26 January 2012 in India.    The Hindi remake rights of the film was bought by Ketan Shah  for  .    Marketed as the costliest Malayalam film.

==Plot==
The film is about Casanovva, a serial womaniser. Casanovva is the owner of Casanovva’s Eternal Spring, an international chain of flower boutiques. He has at his beck and call a faithful array of female followers including his staff, friends and former girlfriends, who will do anything for him. The film starts with a robbery carried out by the four criminals and they decided their next target in a wedding hall. They entered the wedding place as Casonnovvas (Mohanlal) guests. Cassonovva recognises them but allows them to attend the marriage function keeping some plan in his mind. The young criminals ( Alexi, Salim, Arjun, Kiran ) took some photos of their targeted rich persons and collect their details. Casanovva watches this all and does not respond at the spot. The next day was the wedding ceremony. Casanovva, with the aid of his friend in a TV channel (without revealing his plan), planned for a live telecast program to capture the young criminals robbery. But unfortunately, the wedding was postponed due to some unexpected reasons. Thus the robbers pulled off from their plan. But within this time the TV channel peoples came and ready for the telecast. Suddenly Casanovva got an idea and announced a new live romance reality show through the channel named FALL IN LOVE. Then he created some plans to made the two peoples in the robbery gang to fall in love with Hanna (Lakshmi Rai) and Ann Mary (Roma Asrani|Roma) and telecast their love scenes using hidden cameras through the live reality show FALL IN LOVE. Hanna loves one of the young robber by hearing the words of Casonovva, who was her boss. The four robbers tried to steal a sword, but casanovva (wearing a mask in face) interrupts them and took the sword from them. By this time, the TV show became popular and cause some problems to Ann Mary, the robbers and even Hanna. Hanna asks his boss, Casanovva what his plan was. At the moment, Casanovva reveals his flashback to Hanna.

Some years back, cassanova came to Dubai and enjoyed with some girls. Zacariah (Lalu Alex) was a magazine reporter who publish the stories of Casanovva and his mingle with girls. Sameera Zacharia (Shriya Saran), daughter of Zacharia helped Casanovva from a danger and eventually they became friends. But gradually Casanovva understands that he has some different feeling to Sameera than other women. He realizes that he is in love with her. She also has love interest in Casanovva. But her father requests Cassanovva to leave his daughter. He replied that he was in real love with his daughter. Casanovva called Sameera for a meeting to reveal his love to her. She was also been ready to tell her love. She put a mask in her face to make some surprise to Casanovva. But suddenly four robbers rushed to the road with the same mask which Sameera have. The police tried to capture them. The four robbers escaped and Sameera was caught by the police. Suddenly one of the robber shoots Sameera and she dies at the spot. Casanova waited for her and return to his room so depressed. On the way, he saw the four robbers escaping in a car. But at that time he didnt get any idea about the incident happened. He knew the death of Sameera by watching TV news at his room. He suddenly rushes to the police to inform about the robbers. But they didnt spend time to hear what Casanovva had to tell. Here end the Flashback.

He said to Hanna that the aim of the reality show was to understand the robbers about the feeling of love. He then captured the robbers and made them to explain that Sameera was not their gang member to public through the TV show. Thus Casanovva succeed in showing the innocence of his lover Sameera to the public and police. Then he asked the robbers that who killed Sameera. At the end, Casanovva kills the one who shoots his lover and surrenders to the police.

==Cast==
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Actor !! Role 
|-
| Mohanlal ||   Casanova
|-
| Shriya Saran || Sameera Zacharia
|-
| Lakshmi Rai || Hanna
|-
| Roma Asrani || Ann Mary
|-
| Vikramjeet Virk || Alexi ( Robber )
|-
| Abhishek Vinod || Arjun ( Robber )
|-
| Shahid Shamzy || Salim (Robber)
|-
| Dr.Arjun Nandakumar || Kiran ( Robber )
|-
| Riyaz Khan || Joseph
|-
| Jagathy Sreekumar || Lucka
|-
| Lalu Alex || Zacariah
|- Shankar || Ajoy
|-
| Chithra Iyer || Zameeras Dance Teacher
|- Sister Margratt
|}

==Production== Suresh Kumar, Antony Perumbavoor, and the film unit members. 

===Filming=== China Town, which is leading Casanovva to a delayed release.  The second schedule has started from the last week of April 2011 in Dubai.  The shooting at Dubai has ended by the first week of May 2011 and the next location is Bangkok  will start shooting from 14 September,  Fourth schedule Start shooting from 20 October at Bangalore,  The remaining song was shot in the Last schedule that commenced from 12 December at Cape Town, South Africa.

==Release==
 
Casanovva released in 202 theatres in India on 26 January 2012; 138 theatres in Kerala and 64 theatres outside Kerala making it the widest release for a Malayalam film, with simultaneous opening in Mumbai, Chennai, Bangalore and other Indian cities. The film released overseas in February 2012.

==Reception==

===Critical response===
The film received scathing reviews from critics.  
* Moviebuzz from Sify called the film as "below average" while commented that "Casanovva perhaps has some nice moments but you would need real patience to spot them". 
* Nowrunning.com rated  , describing the movie as a "wish-wash film". 
* Rediff gave the movie   and said that "Casanovva disappoints big time". 
* Metromatinee.com review said "if Casanovva works even just a bit, its because of Mohanlal". 
* IndiaGlitz.com called the film as "a stylish flick" and said that "Casanovva is one film that may satisfy the fans of the star and those who like technically sound, slickly made films". 
* Oneindia.in said "Even the glamorous looking babes in short and skimpy clothes could not save Casanovva from the disastrous plot of the film". 
 Rediff listed Casanovva #1 in their lists of Most Disappointing Malayalam Films of 2012. http://www.rediff.com/movies/slide-show/slide-show-1-south-the-most-disappointing-malayalam-films-of-2012/20130118.htm  http://www.sify.com/movies/10-disappointing-films-of-2012-imagegallery-Malayalam-nbdlvqeidie.html?post_ad=1 

===Box office===
The film had a large opening, collecting   4.12&nbsp;crore, thanks to the 1,000 first-day shows, the highest ever for a Malayalam movie.  However, as word of mouth spread, the collections suffered a huge drop    in the subsequent days. The gross collection was no match for the huge budget of the film.      

==Soundtrack==
{{Infobox album
| Name = Casanovva
| Type = Soundtrack
| Artist = Gopi Sundar Alphons Joseph Gowri Lakshmi
| Cover = Casanovva_audio.jpg
| Caption = Album cover for Casanovva
| Released =  
| Recorded = Sunsa Digital Work Station, Chennai Feature film soundtrack
| Length = 21.06 mins
| Online Promotion =  
| Label = Satyam Audios 
| Producer = Dr. C. J. Roy
}}
 Rosshan and Sanjay. The background score of the film has been composed by Gopi Sundar. The audio rights were acquired by Satyam Audio on a price of  . 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Artist(s) !! Composer !! Lyricist !! Duration
|-
| 1 || Omanichumma || Karthik (singer)|Karthik, Vineeth Sreenivasan, Najeem Arshad, Roopa Revathi, Kalyani, Gopi Sundar || Gopi Sundar || Gireesh Puthenchery || 5.11
|-
| 2 || Hey Manohara || Blaaze, Gopi Sundar, Pop Shalini, Balu Thankachan, Priya, Feji || Gopi Sundar || Blaaze, Gopi Sundar, Jelujay, Rosshan Andrrews|Rosshan, Sanjay || 3.27
|- Shweta || Gowri Lakshmi, Gopi Sundar || Gowri Lakshmi || 3.13
|-
| 4 || Kanna Neeyo || Sayanora || Alphons Joseph || Vayalar Sarath Chandra Varma || 3.24
|-
| 5 || Casanovva Theme Song || Mohanlal, Priya Himesh, Ranina Reddy, Gopi Sundar || Gopi Sundar || || 3.18
|- Shweta || Gowri Lakshmi, Gopi Sundar || Gowri Lakshmi || 3.13
|}

==References==
 

==External links==
*  
*  

 
 

 
 
 