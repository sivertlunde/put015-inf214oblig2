Oru Painkilikatha
{{Infobox film
| name = Oru Painkilikatha
| image =
| image_size =
| caption =
| director = Balachandra Menon
| producer = Varada Balachandra Menon
| writer = Balachandra Menon
| screenplay = Balachandra Menon Madhu Srividya Rohini
| music = A. T. Ummer
| cinematography = Vipin Mohan
| editing = Hariharaputhran
| studio = V&V Productions
| distributor = V&V Productions
| released =  
| country = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, Rohini in lead roles. The film had musical score by A. T. Ummer.    The film was remade in Tamil as Thaiku Oru Thalattu.

==Cast==
   Madhu as Madhavankutty 
*Srividya as Rajeswari 
*Balachandra Menon as Kannan  Rohini as Vilasini 
*Kaviyoor Ponnamma 
*Venu Nagavally 
*Bharath Gopi as Swami 
*Kundara Johny 
*Manochithra 
*Uma
 
 
==Soundtrack==
The music was composed by A. T. Ummer. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Aanakoduthaalum || Srividya, Balachandra Menon || Bichu Thirumala || 
|- 
| 2 || Ennenneykkumaay || Venu Nagavally || Bichu Thirumala || 
|- 
| 3 || Painkiliye || Venu Nagavally, Janakidevi, Sindhu, Bharath Gopi || Bichu Thirumala || 
|}
 
==References==
 

==External links==
*  

 
 
 
 

 