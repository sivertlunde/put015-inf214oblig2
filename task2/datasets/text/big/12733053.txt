The Gay Bride
{{Infobox film
| name           = The Gay Bride
| image          = The-Gay-Bride-1934.jpg
| image size     =
| caption        = Film poster Jack Conway
| producer       = John W. Considine Jr. Bella Spewak Sam Spewak
| based on       =  
| starring       = Carole Lombard Chester Morris
| music          = Jack Virgil R.H. Bassett (uncredited)
| cinematography = Ray June Frank Sullivan
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 80 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 gangster screwball Jack Conway and written by the husband-and-wife team of Sam and Bella Spewak, based on the story "Repeal" by Charles Francis Coe.

 

==Plot== Prohibition is repealed, and Shoots is knocked off by rival Daniel Dingle (Sam Hardy).  Mary, looking for a new sugar-daddy, hooks up with Dingle, and when Dingle is removed from the scene by Mickey "The Greek" Mikapopoulis (Leo Carrillo), transfers her attention to him in return for a "trust fund".  All the time, fast-talking straight-shooter Jimmy "Office Boy" Burnham (Chester Morris), Shoots former bodyguard and errand boy, has looked after Mary, passing her advice and snappy remarks whenever theyre needed.  In the end, Mary and Office Boy end up together, but only after "Merry Widow Mary" gives away all the dirty money she was given.  

==Cast==
 
*Carole Lombard as Mary Magiz
*Chester Morris as Jimmy "Office Boy" Burnham
*Zazu Pitts as Mirabelle
*Leo Carrillo as Mickey "The Greek" Mikapopoulis
*Nat Pendleton as William T. "Shoots" Magiz
*Sam Hardy as Daniel J. Dingle Walter Walker as MacPherson, the lawyer

 

;Cast notes:
*Eddie "Rochester" Anderson has a small uncredited part as a bootblack. 
*Veteran character actor Gene Lockhart also has a small part as a crooked politician, Jim Smiley. 

==Production==
The Gay Bride began shooting on 20 September 1934 and finished on 23 October.  It used the working title of "Repeal", which was the title of the short story by Charles Francis Coe it was based on, TCM    which had been published in the Saturday Evening Post at the end of 1933.  The title refers to the repeal of Prohibition in the United States|Prohibition, which ruined the business of the bootlegger played by Nat Pendelton.

When casting the film,   broadcast of The Gay Bride (7 October 2008). 

==Reception==
The film was released in the United States on 14 December 1934,  and did not earn praise from the critics, nor was it successful at the box office.   Variety (magazine)|Variety wrote that Gangster pictures are gone, and this one won’t do anything to bring them back. It’ll do more to consign them permanently to Davey Jones’ locker down where   grosses don’t count. It deals with hoodlums in a post-prohibition era, but fails to freshen up the story and the conventional means taken to carry it out.  Photoplay commented that the film was "A good story loaded with plot complications and blurry character drawings. Even ZaSu Pitts seems more bewildered than usual." Lombard considered the picture one of her worst, and co-star Chester Morris was aware that the film was a "turkey" while they were filming it. 

==Notes==
 

==External links==
*  
*  
*  

 

 
 

 
 
 
 
 
 
 
 