Black Mask (film)
 
 
{{Infobox film
| name           = Black Mask
| image          = BlackMaskPoster.jpg
| image size     = 
| caption        = Theatrical release poster
| description    = Hong Kong theatrical poster
| film name      = {{Film name
| traditional    = 黑俠
| simplified     = 黑侠
| pinyin         = Hēi Xiá
| jyutping       = Hak1 Hap6 }}  Daniel Lee
| producer       = Tsui Hark Joe Ma
| story          = Lee Tat Chi Pang Chi Ming Lau Ching-Wan Anthony Wong Mike Ian Lambert Francoise Yip
| music          = Teddy Robin Kwan
| cinematography = Tony Cheung Cheung Ka-Fai
| studio         = Film Workshop Wins Entertainment Tommy Boy
| distributor    = China Star Entertainment Group  (Hong Kong)   Wins Entertainment  (Hong Kong)  Artisan Entertainment  (US) 
| released       = 9 November 1996
| runtime        = 102 minutes (Hong Kong) 89 minutes (USA)
| country        = Hong Kong
| language       = Cantonese
| gross          = HK$13,286,788 ( ) US$12,504,289 ( )
}} action film Lau Ching-Wan, Anthony Wong. Daniel Lee dubbed and released in the US by Artisan Entertainment. 

The film is based an adapted version of the  , in 2002.
 Kato from the series. The Black Mask is even compared to Kato in a news reporter scene.

==Plot==
Tsui Chik(or Simon in the US version) tries to lead a quiet life as a librarian. However, he is really a former test subject for a highly secretive supersoldier project and the instructor of a special commando unit dubbed "701". The 701 squad is used for many government missions, but after one of the agents kills a team of policemen in an uncontrollable rage, the government decides to abort the project and eliminate all the subjects. Tsui Chik helped the surviving 701 agents flee the extermination attempt. Having escaped, Tsui Chik went separate ways from his team and lived in Hong Kong. Later at night he discovers that the rest of the team were responsible for a violent crime spree that was beyond the capability of the local police. He sets out to get rid of them, donning a mask and hat using the superhero alias of The Black Mask. Having lost the ability to feel pain due to the surgery performed on the super-soldiers by the military, Black Mask is almost invulnerable.

==Alternate versions== Taiwanese (Mandarin Chinese|Mandarin), English (export), UK (produced by Sony BMG|BMG) and US (produced by Artisan Entertainment|Artisan).

===Hong Kong/Taiwanese versions===
For the Cantonese version, the original Mei Ah DVD is cut for gore, but the remastered DVD restores a scene of blood-spurting. The French DVD features the Hong Kong version in the correct form, but contains no English subtitles.

Featured on DVDs distributed in Taiwan by Long Shong, Ritek and Thundermedia, there is approximately 100 seconds footage exclusive to the Taiwanese version:

# Extended dialogue between Tracy and other library staff where they discuss what she would do with Tsui Chik if they cohabited.
# An entire scene where King Kau and a woman are together – he watches her dance, proceeds to have sex and then excitedly shoots her with a water gun and smashes a light. 
# An exchange of gestures between Inspector Sek and King Kau.
# King Kau saluting Inspector Sek in an exasperated form.
# A member of the 701 squad impaling himself onto metal sticks and spitting blood. Shortly after trying to retaliate and getting swiftly defeated, Black Mask entangles the metal sticks (whilst still in his body).
# Shots of bullet-impacting when Black Mask shoots the 701 thugs to save Tracy.
# A shot of a bullet hitting the arm of a 701 thug whilst choking Tracy.
# During the hospital sequence, a scene where a 701 thug grabs two cops arms and breaks them.
# The thug smashes a cops head with his hands.
# More footage of the thug getting shot by police.
# A shot of Yuk Lans shoulder being shot with blood shooting out.
# Some more bullet hits on masked cops when the other zombie grabs his gun.
# Inspector Sek removes a severed thugs arm that had been clutching onto his shoulder.
# Shots of Black Mask bleeding after being stabbed.
# More footage of Tracy obtaining a blood bag.
# A shot of Black Masks blood after being stabbed with a pipe tube.

Both Hong Kong and Taiwanese releases maintain the green-tinting of the film.

===English-language versions===
An English version similar to the Hong Kong version was produced for export (featured on the Spanish DVD), but BMG and Artisan decided to make their own. Whilst only occasionally replacing music on the UK release, Artisan commissioned a brand-new English Hip Hop soundtrack – therefore, removing any reference to the original (despite using excerpts from it in their trailers). Despite a tendency of trimming non-action scenes, the Artisan and BMG versions not only contain all gory content, but also some non-violent scenes not found in any other version:

# Tsui Chik looking for Inspector Sek.
# A few shots of Black Masks hide-out.
# Inspector Sek advicing Yuk Lan to seek Tsui Chik.

==Reception==
Released on 9 November 1996, Black Mask grossed a moderate HK $13,286,788 during its Hong Kong box office run. 

On 1999, Artisan made a direct-to-DVD version of the same name. It grossed a reasonable US$4,449,692 ($4,545 per screen) in its opening weekend, and grossed a total of US $12,504,289. Black Mask was released in the United States cinemas and Hong Kong in 1996 and released on DVD in 26 October 1999 to present this movie in English and cash in on Jet Lis recent appearance in the Hollywood film Lethal Weapon 4. 

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 