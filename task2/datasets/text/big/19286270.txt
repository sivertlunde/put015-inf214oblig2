Vikramaadhithan
{{Infobox film
| name = Vikramaadhithan விக்ரமாதித்தன்
| image = Vikramaadhithan.jpg
| director = T. R. Raghunath & N. S. Ramadas
| writer = En Thangai Nadarajan
| screenplay = En Thangai Nadarajan Padmini Sriranjani, Sriranjini P. S. Veerappa K. A. Thangavelu
| producer = M. A. Ethirajulu Naidu S. V. Namasivayam
| music = S. Rajeswara Rao
| cinematography = R. Sampath V. Rajamani
| editing = C. P. Jambulingam S. Murugesh
| distributor = Jaya Bharat Productions
| studio = Jaya Bharat Productions
| released = 27 October 1962 
| runtime = 162 mins
| country        =   India Tamil
| budget = 
}}
 Padmini and Sriranjini in the lead roles. The film was released in the 1962. The film only ran for 70 days and flopped at box-office. 

==Plot==

 

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || King Vikramaadhithan
|- Padmini || Princess Rathnamalai
|- Sriranjini || 
|-
| P. S. Veerappa || 
|- Ragini || Vairamalai
|-
| K. A. Thangavelu || 
|-
| G. Sakunthala || 
|-
| M. Saroja ||
|-
| O. A. K. Devar || 
|-
| Rushyendramani || 
|-
| C. T. Rajakantham || 
|-
| C. K. Saraswathi || 
|}

==Crew==
*Producer: M. A. Ethirajulu Naidu & S. V. Namasivayam
*Production Company: Jaya Bharat Productions
*Director: T. R. Raghunath & N. S. Ramadas
*Screenplay: En Thangai Nadarajan
*Dialogue: En Thangai Nadarajan
*Music: S. Rajeswara Rao
*Lyrics: Pattukottai Kalyanasundaram, Kannadasan, A. Maruthakasi, M. K. Athmanathan, En Thangai Nadarajan, Puratachidasan & Kavi Lakshmanadas.
*Art Direction: 
*Editing: C. P. Jambulingam & S. Murugesh
*Choreography: P. S. Gopalakrishnan, K. Thangaraj, B. Hiralal & A. K. Chopra
*Cinematography: R. Sampath & V. Rajamani
*Stunt: Stunt Somu
*Dance:

==Soundtrack== Playback singers are T. M. Soundararajan, M. L. Vasanthakumari, P. Leela, P. Suseela & T. V. Rathinam.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Adhisayam Ivanadhu || M. L. Vasanthakumari || || 02:46
|- 
| 2 || Vannam Paadudhe || T. M. Soundararajan & P. Suseela || || 03:27
|- 
| 3 || Kalai Ennam || P. Leela & T. V. Rathinam || ||
|- 
| 4 || Venmugile Konja Neram Nillu || P. Suseela || M. K. Aathmanathan || 04:26
|- 
| 5 || Theermaanam Saiyaaga Aaadaavittaal || T. M. Soundararajan || || 03:54
|- 
| 6 || Mugatthai Paarthu Muraikkaatheenga || P. Leela & T. M. Soundararajan || || 03:50
|- 
| 7 ||  ||  || || 
|- 
| 8 ||  ||  || || 
|- 
| 9 ||  ||  || || 
|}

==References==
 

==External links==
 

 
 
 
 


 