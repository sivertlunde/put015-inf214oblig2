Delirium (1987 film)
{{Infobox film
| name = Delirium
| image = Delirium1987.jpg
| alt =
| caption = Italian theatrical release poster
| director = Lamberto Bava
| producer = Massimo Manasse  Marco Grillo SPina
| screenplay = Gianfranco Clerici Daniele Stroppa
| story = Luciano Martino
| starring = Serena Grandi Daria Nicolodi
| music = Simon Boswell
| cinematography = Gianlorenzo Battaglia
| editing = Mauro Bonanni
| studio = Dania Film Devon Film Filmes International National Cinematografica
| released = 3 April 1987
| runtime = 94 min.
| country = Italy
| language = Italian
}}

Delirium (Italian: Le foto di Gioia; also known as Delirium: Photo of Gioia) is a 1987 Italian giallo film directed by Lamberto Bava and starring Serena Grandi and Daria Nicolodi.

== Plot ==
A former prostitute is in charge of a successful mens magazine. An obsessed admirer systematically slaughters her models (which also increases the magazines sales) and supplies the mistress with pictures of their disfigured corpses taken in front of her semi-nude posters visible in the background. The more he kills, the closer the killer gets to slaughtering his obsession.

== Cast ==
* Serena Grandi as Gloria
* Daria Nicolodi as Evelyn
* Vanni Corbellini as Tony
* David Brandon as Roberto George Eastman as Alex
* Katrine Michelsen as Kim
* Karl Zinny as Mark
* Lino Salemme as Inspector Corsi
* Sabrina Salerno as Sabrina
* Capucine as Flora

== Production ==
Famed Italian director Dario Argento was attached to direct early in production but pulled out due to script changes.  He was replaced by his protégé Lamberto Bava.

== Releases == region 1 Beyond the Darkness.

== In popular culture ==
A poster for the film is seen in an episode of the British television comedy series Coupling (UK TV series)|Coupling. 

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 


 
 