Stitches (2011 film)
{{Infobox film
| name           = Stitches 
| image          = 
| alt            = 
| caption        = 
| director       = Adiya Imri Orr
| producer       = Adi Druker
| writer         = Adiya Imri Orr
| screenplay     = Adiya Imri Orr
| story          = Adiya Imri Orr
| based on       =  
| narrator       = 
| starring       = {{Plainlist |
* Riki Blich
* Itzik Golan
* Shira Katznlanbogen
}}
| music          = 
| cinematography = Meidan Arama
| editing        = Or Lee-Tal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 18 minutes
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}}
Stitches ( ) is a 2011 Israeli short film. The film was directed by Adiya Imri Orr, and has won the 2012 Student Visionary Award in the Tribeca Film Festival.

==Plot==
Amit (Riki Blich) and Noa (Shira Katznlanbogen), spouses in their 30s, one night in the hospital, after the birth of their first daughter, finally tell the truth one to another.

==Cast==
* Riki Blich - Amit
* Itzik Golan - Shaul - Piper
* Shira Katznlanbogen - Noa

==External links==
*  

 
 
 
 
 
 
 
 

 