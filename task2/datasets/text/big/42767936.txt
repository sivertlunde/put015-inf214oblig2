Crazy That Way
{{Infobox film
| name =  Crazy That Way
| image =
| image_size =
| caption =
| director = Hamilton MacFadden
| producer = George E. Middleton    
| writer = Vincent Lawrence  (play)   Marion Orth   Hamilton MacFadden
| narrator =
| starring = Kenneth MacKenna   Joan Bennett  Regis Toomey   Jason Robards Sr. 
| music = 
| cinematography = Joseph A. Valentine 
| editing = Ralph Dietrich  
| studio = Fox Film Corporation 
| distributor = Fox Film Corporation 
| released = March 20, 1930
| runtime = 64 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Crazy That Way is a 1930 American comedy film directed by Hamilton MacFadden and starring  Kenneth MacKenna, Joan Bennett and Regis Toomey,   and based on the play In Love With Love by Vincent Lawrence.

==Cast==
*  Kenneth MacKenna as Jack Gardner  
* Joan Bennett as Ann Jordan  
* Regis Toomey as Robert Metcalf  
* Jason Robards Sr. as Frank Oakes
* Sharon Lynn as Marion Sars 
* Lumsden Hare as Mr. Jordan 
* Baby Mack as Julia  

==Preservation status==
This film is now thought to be a lost film.

==See also==
*List of lost films

==References==
 

==Bibliography==
* Kellow, Brian. The Bennetts: An Acting Family (University Press of Kentucky, 2004)

==External links==
* 

 
 
 
 
 
 
 
 

 