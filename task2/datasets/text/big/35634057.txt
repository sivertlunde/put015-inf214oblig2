Shaitaan (film)
 
 
{{Infobox film
| name           = Shaitaan
| image          = 
| caption        = 
| producer       = A.A. Nadiadwala
| director       = Firoze Chinoy
| writer         = K.A. Narayan
| screenplay     = Kaushal Bharati
| story          = K.A. Narayan
| dialogue       = Kaushal Bharati Bindu Aruna Irani Jagdeep Padma Khanna Nivedita
| language       = R.D. Burman
| country        = India
| language       = Hindi
}}

Shaitaan is a 1974 Hindi thriller film, directed by Firoze Chinoy starring Shatrughan Sinha and Sharmila Tagore in main roles. Majrooh Sultanpuri wrote the songs for this film while Rahul Dev Burman was the music director.

==Plot==
Anand (Shatrughan Sinha) and Munish (Anil Dhawan) are two best friends. Anand is an honest police officer whereas Munish is the most famous lawyer of the city. Munishs sister Nisha is in love with Anand.

There are series of rapes and murders in the film. Anand discovers that all the victim girls were connected with Munish. One girl Shabnam, miraculously escapes from the hands of Munish. Anand arrests Munish on the charges of rape and murder but Shabnam identifies Anand as the killer and the rapist.

==Cast==
* Shatrughan Sinha as Inspector Anand/Ashok (Double Role)
* Anil Dhawan as Advocate Manish
* Sharmila Tagore as Nisha Bindu as Sabrina
* Kumud Chuggani as Lily Fernandez - Air Hostess
* Aruna Irani as Banjaran
* D.K. Sapru as Police Commissioner
* Tun Tun as Meena Advani
* Sulochana Latkar as Radha
* Padma Khanna as Shabnam
* Jagdeep as Maqtulla Komal (Poonam Sinha) as Munishs date

== External links ==
*  

 
 
 
 


 