Saradha Bullodu
 
{{Infobox film
| name           = Saradha Bullodu
| image          =
| caption        =
| director       = Ravi Raja Pinisetty
| producer       = T.Trivikrama Rao
| writer         =
| narrator       = Venkatesh Nagma Koti
| cinematography =
| editing        =
| studio         = Vijayalakshmi Art Pictures
| distributor    =
| released       =  
| runtime        = 136 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          =
}}
 Tollywood family Venkatesh and Nagma played the lead roles and music composed by Saluri Koteswara Rao|Koti.   The film was flop at the box office. 

==Plot==
Vijay (Daggubati Venkatesh|Venkatesh) an engineer in AP Oils stays with his widowed mother Shanta (Sangeeta (Telugu actress)|Sangeeta), he finds out that there are oil wells at Attamadugu village surroundings. Sangliyana (Mohan Raj) a business man offers him huge bribe & threatens him to conquer those oil wells but he will not yield to it, he is gives deadly warning to Sangliyana and reaches Attamadugu and starts his work.

Rajeswari Devi (Manjula Vijayakumar|Manjula) a greedy, cruel & crooked woman head of Attamadugu, whose word is law for the villagers. Actually she is enjoying the property of her brother Jaya Chandra (Murali Mohan) who became a drunkard. Rajeswari Devi locked him in a room and treating his daughter Lakshmi (Madhurima) as servant. Vijay always gives tough fight to Rajeswari Devi, teases her daughter Nirmala Devi (Nagma). But somehow Vijay feels Lakshmi as his own sister.

Meanwhile, Rajeswari Devi mixes hands with Sangliyana to destroy Vijay. They make a plan to kill Vijay, Lakshmi listens their plan and informs Vijay before they attack him and protects Vijay. Notwithstanding by Rajeswari Devi, she decides to punish Lakshmi by making her marriage arrangements with a servant. Vijay stops this event and scolds Rajeswari Devi, then she says that no one will marry her because her mother is a prostitute and shows her photo which crashes Vijay, because that photo is none other than his mother Shanta. Vijay goes back to his home town and asks his mother why she is living like a widow when her husband is alive then she reveals the past.

Jaya Chandra was the original arbitrator of the village and treated as God by villagers. Rajeswari Devi was jealous on his popularity and eager to achieve is authority. So, she plays a game on Shanta, proves before all villagers that she is having an illegal affair with somebody. For that reason Jaya Chandra gave a judgement that she should be widowed by removing her wedding chain (Mangalsutra), bangles and sectarian mark (Bottu). Shanta tried to commit suicide but Vijay saved her and both of them left the village.

Now, Vijay decides to teach Rajeswari Devi a lesson. Meanwhile, Nirmala Devi also comes to know the truth and starts loving Vijay, with her help Vijay windups the same play with Rajeswari Devi and makes her to admit the mistake which she had done to his mother before everyone. Jaya Chandra also felling remorse and all of them go to bring back Shanta home, at the same time Sangliyana kidnaps her. Finally, Vijay destroys Sangliyana, in the attack Jaya Chandra is injured and he gives sectarian mark to Shanta with his blood. Rajeswari Devi also realizes, says sorry to everyone and movie ends with marriage of Vijay & Nirmala Devi.

==Cast==
  Venkatesh as Vijay	
* Nagma as Nirmala Devi 	 		
* Sanghavi as Yamini Satyanarayana
* Murali Mohan as Jaya Chandra
* Mohan Raj as Sangliyana Srihari as Sangliyanas Henchmen			
* Kota Srinivasa Rao as Siva Rao
* Bramhanandam as Brahmam Ali as Siva Raos Son
* M. S. Narayana as Passenger
* Mannava Balayya|M.Balaiyah as Chief Engineer
* Narra Venkateswara Rao as Minister
* Dham as T.C. Manjula as Rajeswari Devi Sangeeta as Shanta
* Madhurima as Lakshmi
* Y.Vijaya as Siva Raos Wife
 

==Soundtrack==
{{Infobox album
| Name = Sarada Bullodu
| Tagline =
| Type = Soundtrack Koti
| Cover =
| Released = 1996
| Recorded =
| Genre = Soundtrack
| Length = 29:18
| Label = T-Series  Koti
| Reviews =
| Last album = Hello Neeku Naaku Pellanta   (1996)
| This album = Sarada Bullodu   (1996) 
| Next album = Akka Bavunnava   (1996)
}}

Music composed by Saluri Koteswara Rao|Koti. Music released on T-Series Audio Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:18
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Ranga Ranga Singaranga Sirivennela Sitarama Sastry SP Balu, Chitra
| length1 = 5:18

| title2  = Daani Vayyaram
| lyrics2 = Sirivennela Sitarama Sastry
| extra2  = SP Balu, Chitra
| length2 = 4:22

| title3  = Chitha Karthelo Chinukulu
| lyrics3 = Bhuvanachandra
| extra3  = SP Balu, Chitra
| length3 = 5:05

| title4  = Mogindoyammo Sruthi
| lyrics4 = Sirivennela Sitarama Sastry
| extra4  = SP Balu, Chitra
| length4 = 5:23

| title5  = Naatu Kodi Pettoyamma
| lyrics5 = Bhuvanachandra
| extra5  = SP Balu, Chitra
| length5 = 4:52

| title6  = Gilele Gilele Gilele  
| lyrics6 = Sirivennela Sitarama Sastry
| extra6  = SP Balu, Chitra
| length6 = 4:18
}}

 

== References ==
 

==External links==
* 

 

 
 
 