The Space Movie
{{Infobox film
| name           = The Space Movie
| image          = The Space Movie.jpg
| alt            = 
| caption        = The cover of the 2007 DVD release
| film name      =  
| director       = Tony Palmer
| producers      = Richard Branson Simon Draper
| writer         = Tony Palmer
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Mike Oldfield
| cinematography = 
| editing        = Graham Bunn
| studio         = Virgin Films
| distributor    = International Harmony
| released       =  
| runtime        = 78 minutes
| country        = United Kingdom United States
| language       = English
| budget         = 
| gross          =  
}}

The Space Movie is a documentary film produced in 1979 by Tony Palmer at the request of NASA, to celebrate the tenth anniversary of the Apollo 11 moon landing.

The 78 minute film was released theatrically in 1980, on VHS in 1983 and on DVD in 2007. Richard Branson and Simon Drapers Virgin Films produced the film. Ed Bishop provided some narration for the film. 

== Soundtrack == Hergest Ridge, Ommadawn and "Portsmouth (Mike Oldfield instrumental)|Portsmouth".

It also featured excerpts from what was Oldfields then-new album, Incantations (album)|Incantations. The film also made use of the orchestral arrangements of Oldfields first two albums, The Orchestral Tubular Bells and notably The Orchestral Hergest Ridge, which has never been released.  The Space Movie soundtrack was also intended to be released as an album. 

A short section of The Space Movie featuring Incantations is available as bonus material on the 1993 video collection, Elements – The Best of Mike Oldfield (video)|Elements – The Best of Mike Oldfield.

== References ==
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 

 