H4 (film)
{{Infobox film
| name           = H4
| image          = 
| caption        = Paul Quinn
| producer       = Harry Lennix Giovanni Zelko
| writer         = William Shakespeare (play) Ayanna Thompson (adaption for screen)
| starring       = Harry Lennix Angus Macfadyen Nick Gillie Victoria Tilford Geno Monteiro Keith David Jeryl Sayles Terrell Tilford Keir Thirus Heavy D Mark Nichols
| cinematography = John Pedone  Tony Rudenko Bob Allen Art Olmos (additional editing)
| distributor    =
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         =
}}
 Paul Quinn; actor Harry Lennix produced the film and stars in the title role.  It had its world premiere in competition at the 49th Chicago International Film Festival in 2013.   In November 2014, the film was shown at the Indie Memphis Film Festival. 

== Plot ==
The film is an adaptation that combines William Shakespeares plays Henry IV, Part 1 and Henry IV, Part 2 and sets them in contemporary Los Angeles. The screenplay was adapted by Dr. Ayanna Thompson, Professor of English at Arizona State University, retaining Shakespeares original language.    According to Lennix, the film is told "from the perspective of black people, acted primarily by black people, and it’s from the black point of view";  he nevertheless describes Shakespeares themes, and the films, as "universal".   Thompson describes the script as being "about the politics and political manoeuvring within the African-American community".   

== Production ==
Post-production of the film was partly funded by a $25,000 Kickstarter campaign. 

In August 2012, a rough cut of the film was previewed at the International Shakespeare Conference in Stratford-upon-Avon to international Shakespeare scholars.  

The film is scheduled for release in US cinemas in 2015. 

== References ==
{{reflist| refs=

 
{{cite news
|url=http://variety.com/2014/film/news/harry-lennix-exponent-media-movie-production-company-1201273437/
|title=‘Blacklist’ Actor Harry Lennix Launches Movie Production Company  Variety
|date=August 4, 2014 
|accessdate=September 6, 2014
|author=Dave McNary
}}
 

 
{{cite news
|url=http://www.ebony.com/entertainment-culture/harry-lennix-channels-black-comic-rage-in-indie-hit-mr-sophistication-interview#.VAtfma13_FU
|title=Harry Lennix Channels Black-Comic Rage in Indie Hit, ‘Mr. Sophistication’ 
|date=29 November 2012
|author=Sergio Mims Ebony
|accessdate=September 6, 2014
}}
 

 
{{cite web
|url=http://www.rhodes.edu/shakespeare/27087.asp 
|date=November 2, 2013
|title=H4 Screening
|accessdate=6 September 2014
|publisher=Rhodes College
|author=Scott L. Newstok
}}
 

 
{{cite news
|url=http://www.huffingtonpost.com/xaque-gruber/the-many-faces-of-harry-l_b_3677457.html
|date=30 July 2013
|author=Xaque Gruber
|title=The Many Faces of Harry Lennix
|newspaper=Huffington Post
}}
 

 
{{cite web 
|url=http://showbizchicago.com/2013/10/14/harry-lennixs-h4-world-premieres-at-the-2013-chicago-international-film-festival/#.VAtGeUjlCBA 
|title=Harry Lennix H4 world Premiers at the 2013 Chicago International Film Festival
|author=Michael Roberts
|publisher=Showbiz Chicago
|date=October 14, 2013
|accessdate=6 September 2014
}}
 

 
{{cite web
|url=http://blogs.indiewire.com/shadowandact/shakespeare-film-h4-starring-harry-lennix-to-screen-at-chicago-intl-film-fest-next-month
|title=Shakespeare Film ‘H4 Starring Harry Lennix To Screen At Chicago Intl Film Fest Next Month
|author=Sergio
|date=September 20, 2013
|website=Indiewire
|accessdate=6 September 2014
}}
 

 
{{cite web
|url=http://blogs.indiewire.com/shadowandact/harry-lennix-starts-kickstater-campaign-for-his-h4-film-project
|title=Harry Lennix Starts Kickstater Campaign For his H4 Film Project
|author=Sergio
|publisher=Indiewire
|date=June 27, 2012 
|accessdate=6 September 2014
}}
 

}}

== External links ==
*  
*   - completed Kickstarter campaign  

 