Call Her Savage
{{Infobox Film
| name           = Call Her Savage
| image          = Callhersavage.jpg
| image_size     = 
| caption        =  John Francis Dillon
| producer       = Sam E. Rork
| writer         = Tiffany Thayer (novel) Edwin J. Burke
| starring       = Clara Bow Gilbert Roland
| music          = Peter Brunelli Arthur Lange
| cinematography = Lee Garmes
| editing        = Harold D. Schuster
| distributor    = Fox Film Corporation
| released       = November 24, 1932
| runtime        = 82-92 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Call Her Savage (1932) is a Pre-Code drama film directed by John Francis Dillon and starring Clara Bow.  The film was Bows second-to-last film role.

==Plot summary==
A wild young woman, born and raised in Texas, rebels against the man she believes to be her father.  Moving to Chicago, she marries badly, loses her child in a boardinghouse fire, is nearly forced to become a prostitute, and is renounced by her father, who tells her he never wishes to see her again.  

Upon learning that her mother is dying, she hurries home to Texas.  There she learns that she is a so-called "half-breed," half white and half Indian.  The assertion is made that this explains why she had always been "untameable and wild", which played into the stereo types of the 1920s for Native Americans.  This knowledge of her lineage would supposedly allow her the possibility for happiness in the arms of a handsome young Indian who has long loved her from afar.

==Cast==
* Clara Bow as Nasa Springer
* Gilbert Roland as Moonglow
* Thelma Todd as Sunny De Lane
* Monroe Owsley as Lawrence Crosby
* Estelle Taylor as Ruth Springer
* Weldon Heyburn as Ronasa
* Willard Robertson as Pete Springer
* Anthony Jowitt as Jay Randall
* Fred Kohler as Silas Jennings Russell Simpson as Old Man In Wagon Train
* Margaret Livingston as Molly
* Carl Stockdale as Mort
* Dorothy Peterson as Silas Wife
* Marilyn Knowlden as Ruth (as a girl) Douglas Haig (uncredited) as Pete as a Boy {{cite book
|url=http://books.google.com/books?id=bsoUXGZSxZcC
|title=American Film Institute Catalog
|editor=Alan Gevinson
|publisher=University of California Press
|year=1997
}} 

==Preservation status==
The film was restored in 2012 by the Museum of Modern Art and premiered at the third annual Turner Classic Movies Film Festival in Hollywood. 

==Notes==
This is a film that is about the status of women in the 1920s and racism against American Indians. The film is really a prologue to modern feminism and the centers on the humanity of Native Americans, hence the title of the film.  Among the stereotypes confronted in the film was an attempt by "Dynamites" father to force her into a marriage, and her cat fight in a social club.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 