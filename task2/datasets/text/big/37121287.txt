ZomBees
 

 
{{Infobox film
| name           = ZomBees
| director       = Fred Wilder
| producer       = Fred Wilder
| screenplay     = Fred Wilder, Candy Wilder
| starring       = Phil Bottoms
| cinematography = Fred Wilder
| editing        = Fred Wilder
| studio         = St. Croix Studios
| released       = 2008
| runtime        = 5 minutes
| country        = United States English 
}}
ZomBees is a 2008 short film by Fred Wilder. It revolves around a photographer investigationg the decline of the honeybee population.

==Synopsis==
ZomBees is a surreal serendipity into the chaos of mans folly in genetic engineering and the Colony Collapse Disorder (C.C.D.) that is devastating the Worlds honeybee population. Mans childish grasp of science and its arrogant tinkering with genetic engineering is to blame and ZomBees are the result. The issue of CCD is very real and this film explores the possible catastrophic results from blindly triggering an ecological imbalance that may some day soon doom us all. 

==Cast==
* Phill Bottoms as Phil the Photographer

==References==
 

==External links==
*  

 
 


 