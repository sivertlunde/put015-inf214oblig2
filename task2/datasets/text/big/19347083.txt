Saturday's Lesson
 
{{Infobox film
| name           = Saturdays Lesson
| image          = 
| caption        = 
| director       = Robert F. McGowan
| producer       = Hal Roach
| writer         = Robert F. McGowan H. M. Walker
| starring       = 
| music          = 
| cinematography = Art Lloyd
| editing        = Richard C. Currier MGM
| released       =  
| runtime        = 17 40" 
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent comedy Little Mother and Cat, Dog & Co., Saturdays Lesson was withheld until after several sound Our Gang films had been released.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jean Darling as Jean
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Harry Spear as Harry
* Pete the Pup as Himself

===Additional cast===
* Orpha Alba as Joes mom
* John B. OBrien as The Devil
* Emma Reed as Farinas mom
* Adele Watson as The Other Kids mom
* Charley Young as Dr. A.M. Austin
* Allan Cavan - Pedestrian #2
* Ham Kinsey - Pedestrian #1

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 