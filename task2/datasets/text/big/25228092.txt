Chosen Survivors
{{Infobox Film
| name           = Chosen Survivors
| image          = Chosensurvivors.jpg
| image_size     = 
| caption        = Original poster art
| director       = Sutton Roley
| producer       = Leon Benson Charles W. Fries
| writer         = H.B. Cross
| narrator       = 
| starring       = Jackie Cooper Diana Muldaur Richard Jaeckel Bradford Dillman Lincoln Kilpatrick Alex Cord Barbara Babcock
| music          = Fred Karlin
| cinematography = Gabriel Torres
| editing        = John F. Link Dennis Virkler	
| distributor    = 
| released       = 1974
| runtime        = 99 minutes
| country        = United States Mexico
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 science fiction/Horror horror film directed by Sutton Roley and starring Diana Muldaur and Jackie Cooper.

==Plot==
After being selected at random by a computer to seek safety in an underground  . And since going back above ground isnt an option, theyre forced to stay and fight for their lives.

==Releases==
The film was released theatrically in the United States on May 22, 1974.  The original distributor was Columbia Pictures.

The film was released on DVD in a double feature with The Earth Dies Screaming as part of MGMs Midnite Movies series.

== See also ==
* Midnite Movies

==External links==
* 

 
 
 
 
 
 
 

 