Association of Wrongdoers
{{Infobox film
| name           = Association of Wrongdoers
| image          = Association de malfaiteurs.jpg
| alt            = 
| caption        = 
| film name      =  
| director       = Claude Zidi
| producer       =  
| writer         = {{plainlist|
*Michel Fabre
*Simon Michaël
*Claude Zidi }}
| screenplay     = 
| story          = 
| based on       =  
| starring       = 
| narrator       =  
| music          = Francis Lai 
| cinematography = Jean-Jacques Tarbès 
| editing        = Nicole Saunier 
| production companies = {{plainlist|
*FR3 Cinema
*Movies 7 }}
| distributor    = AMLF (Paris) 
| released       =   
| runtime        = 
| country        = France 
| language       = 
| budget         = 
| gross          =  
}}
Association of Wrongdoers ( ) is a 1987 French comedy film directed by Claude Zidi. 

==Plot==
Gérard and Thierry  are business partners who are accused of stealing a safe from a wealthy tycoon in this situation comedy. A practical joke backfires when the two make their colleague Daniel believe he has won the lottery. The owner of the safe calls the police, who chase after the scheming duo. The two steal the safe a second time to cover the loss of the money taken in the first burglary. Monique is the sultry police commissioner and former flame of the robbery victim who investigates the bizarre case.

==Cast==
* François Cluzet as Thierry
* Christophe Malavoy as Gérard
* Véronique Genest as Monique Lemercier
* Jean-Claude Leguay as Daniel
* Jean-Pierre Bisson as Bernard Hassler
* Claire Nebout as Claire
* Gérard Lecaillon as Francis

==Production==
The film was shot between September 18 and October 31, 1986.    

==References==
 

==External links==
 

 

 
 
 
 
 
 


 
 