Dennis the Menace Strikes Again
{{Infobox film
| name           = Dennis the Menace Strikes Again
| image          = Dennis the Menace Strikes Again.jpg
| image_size     =
| caption        =
| director       = Charles T. Kanganis
| producer       = Robert Newmyer Jeffrey Silver
| writer         = Tim McCanlies Jeff Schechter
| based on       =  
| narrator       = Justin Cooper Don Rickles George Kennedy Betty White Brian Doyle-Murray Carrot Top Dwier Brown Heidi Swedberg
| music          = Graeme Revell
| cinematography = Christopher Faloona
| editing        = Jeffrey Reiner Hank Ketcham Enterprises
| distributor    = Warner Bros. Family Entertainment
| released       =  
| runtime        = 71 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} Dennis the Menace. It was produced by Outlaw Productions and released by Warner Bros. on July 14, 1998.

None of the cast from the first film appear in this sequel.

== Plot == Justin Cooper) is worse than ever. At the beginning of the movie, he goes over to Mr. Wilsons (Don Rickles) house to offer him one of several gifts for his birthday. These include lizards, snakes, bugs, and other creatures. This ordeal ends with Mr. Wilson unintentionally riding down a flight of stairs in Dennis red wagon and accidentally getting his birthday cake thrown in his face by Martha (Betty White). Soon after this incident, Mr. Johnson, Dennis grandfather and Alices father (George Kennedy), shows up and announces that he is moving in with the Mitchells. Dennis starts spending more time with him than his annoyed neighbor. Mr. Wilson, upset that hes getting older, gets tricked by two con men (Brian Doyle-Murray and Carrot Top) who try to talk him into buying a "rare" root used to make tea to make people younger. 

Mr. Wilson is about to pay $10,000 when Dennis comes by. Dennis then reveals that he owns a root of the same kind, which he says he found on a place where those abound. Soon afterwards, the two impostors return and sell Mr. Wilson a machine that allegedly makes people younger. Suddenly, the attitudes of him and Mr. Johnson reverse as the latter feels Georges pain of living in the same neighborhood as Dennis, while he starts to feel youthful and happy. 

While Dennis is trying to clean up a pile of garbage that he accidentally threw on Grandpas car while he was taking out the trash, he accidentally destroys Mr. Wilsons machine. As a result of this, the Wilsons plan on moving away to be away from him for good, whereupon Mr. Johnson decides to move into their house, although no one seems to really want to carry out this plan. 

Dennis helps the police (unintentionally) catching the con men, who were pretending to be several different workmen at the Wilson house when they were planning to move, attempting yet again to drain his bank account by stockpiling a hoard of his as yet unendorsed checks by claiming that the house needed several repairs before it could be sold. Dennis, who was a "menace" throughout the whole movie, ends up being a hero. The police return the uncashed checks, and Mr. Wilson decides not to move. Mr. Johnson, however, is now planning to move out of the Mitchell house because of everything Dennis has put him through.

The film ends as Dennis and his grandfather are in his camper in the Grand Canyon and Dennis, wanting to take a rock home to Mr. Wilson as a present, accidentally takes the one from under the camper, causing it to roll down the incline it is parked on top of with Mr. Johnson still in it. The others see on the news what has happened as Dennis explains to the camera and his grandfather is being airlifted to safety. Dennis gives a shoutout to Mr. Wilson, which leaves the latter flabbergasted.

== Cast ==
*   in the first movie.
*   in the first movie. Although George and Dennis make up at the end of the first film, Mr.Wilsons hatred of Dennis returns in this sequel.
*   in the first movie.
* George Kennedy as Mr. Johnson: Dennis grandfather, Alices father, who moves in with the Mitchells.
* Brian Doyle-Murray as Professor Scott Carrot Top Thompson as Sylvester
*   in the first movie.
*   in the first movie.
*   in the first movie.
*   in the first movie.
*   in the first movie.
* Alexa Vega as Gina
* Brooke Candy as little girl on diving board

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 