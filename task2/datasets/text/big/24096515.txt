The Italian Barber
{{Infobox film
| name           = The Italian Barber
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         =
| narrator       =
| starring       = Joseph Graybill
| cinematography = G. W. Bitzer
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States Silent English English intertitles
| budget         =
| website        =
}}
 1911 short short silent silent drama film directed by D. W. Griffith, starring Joseph Graybill and featuring Mary Pickford.  The film, by the Biograph Company, was shot in Fort Lee, New Jersey when many early film studios in Americas first motion picture industry were based there at the beginning of the 20th century.    

==Cast==
* Joseph Graybill - Tony
* Mary Pickford - Alice
* Marion Sunshine - Florence
* Mack Sennett - Bobby Mack
* Kate Bruce - Mother
* Robert Harron - man buying papers John T. Dillon - man buying papers
* W. C. Robinson - in shop
* Donald Crisp
* Adolph Lestina
* Vivian Prescott Edward Dillon
* Jeanie MacPherson
* Lottie Pickford
* Claire McDowell

==See also==
* D. W. Griffith filmography

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 