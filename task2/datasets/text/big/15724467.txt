The Phantom Foe
 
{{Infobox film
| name           = The Phantom Foe
| image          = Thephantomfoe-1921-newspaperad.jpg
| caption        = Newspaper advertisement. 
| director       = Bertram Millhauser
| producer       = 
| writer         = George B. Seitz Frank Leon Smith
| starring       = Warner Oland Juanita Hansen
| cinematography = 
| editing        =  Astra Films
| released       =  
| runtime        = 15 episodes 
| country        = United States 
| language       = Silent (English intertitles)
| budget         = 
}}
 adventure film serial directed by Bertram Millhauser.

==Cast==
* Warner Oland - Uncle Lew Selkirk
* Juanita Hansen - Janet Dale
* Wallace McCutcheon Jr. - Steve Roycroft William Bailey - Bob Royal (as William Norton Bailey)
* Nina Cassavant - Esther, Janets Cousin
* Tom Goodwin - Jeremiah Dale (as Thomas Goodwin)
* Harry Semels - The Phantom Foe
* Joe Cuny - Andre Renoir
* Al Franklin Thomas

==See also==
* List of film serials
* List of film serials by studio

==External links==
* 
*  at Silentera.com

 

 
 
 
 
 
 
 


 