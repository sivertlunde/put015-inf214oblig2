Little Athens
{{Infobox Film
| name           = Little Athens
| caption        = 
| image	=	Little Athens FilmPoster.jpeg
| director       = Tom Zuber
| producer       = Tom Zuber Larry Romano Chad Marshall Josh Lawler
| writer         = Tom Zuber Jeff Zuber
| starring       = John Patrick Amedori Erica Leerhsen DJ Qualls Rachel Miner Michelle Horn Michael Peña Jill Ritchie  Chad Marshall
| music          = Barak Moffitt
| cinematography = Lisa Wiegand
| editing        = Aaron Toaso Tom Zuber
| distributor    = Legaci Pictures 
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
}}
Little Athens is a 2005 American independent film directed by Tom Zuber, which stars John Patrick Amedori, Erica Leerhsen, DJ Qualls, Rachel Miner, Eric Szmanda, Michael Peña, and more. Despite premiering at Toronto Film Festival in 2005, it wasnt released on DVD until November 21, 2006.

==Plot==
Little Athens follows a whirlwind day in the hapless lives of small town youth caught in a dead-end post-high school void. The journeys of four groups of late teens/early twenty-somethings unfold through four different storylines, their separate trails converging at an explosive house party.

==Cast==
* John Patrick Amedori as Jimmy
* Erica Leerhsen as Heather
* DJ Qualls as Cory
* Rachel Miner as Allison
* Eric Szmanda as Derek
* Shawn Hatosy as Carter
* Michelle Horn as Emily
* Jorge Garcia as Pedro
* Jill Ritchie as Jessica
* Michael Peña as Carlos
* Kenny Morrison as Aaron
* Leonardo Nam as Kwon
* R.J. Knoll as Brad
* Forrest Landis as Kevin
* Esteban Powell as Troy
* Mary-Pat Green as Mrs. Carlson
* Tory Kittles as Sinjin

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 