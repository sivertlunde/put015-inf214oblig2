Clowning Around
 
 
{{Infobox Film
| name           = Clowning Around 
| image          = 
| image_size     = 
| caption        = 
| director       = George Whaly
| producer       = 
| writer         = 
| narrator       = 
| starring       = Clayton Williamson Annie Byron Jean-Michel Dagory Ernie Dingo Van Johnson Rebecca Smart Noni Hazlehurst Jill Perryman Heath Ledger Peter Best
| cinematography = 
| editing        = 
| distributor    = Australian Broadcasting Corporation
| released       = 1991
| runtime        = 165 minutes
| country        = Australia France United States
| language       = English
| budget         = 
| gross          = 
}}
Clowning Around (1991) is an Australian family film that was shot on location in Perth, Western Australia|Perth, Western Australia and Paris, France. It was based on the novel Clowning Sim by David Martin. Albert Moran, Morans Guide to Australian TV Series, AFTRS 1993 p 119 

The film was produced by independent film company Barron Entertainment Films in Western Australia and educational film company WonderWorks in the United States, was directed by George Whaley. It was distributed by Australian Broadcasting Corporation. It featured Australian actors such as Clayton Williamson, Noni Hazelhurst, Ernie Dingo, Rebecca Smart, and Jill Perryman, and also featured veteran American actor Van Johnson in his final film role, as well as French actor Jean-Michel Dagory. This film also features the first film role for Heath Ledger, who would later appear in Blackrock (film)|Blackrock (1997) with Smart and Candy (2006 film)|Candy (2006) with Hazlehurst. Ledgers role appeared in the last ten minutes; he played an orphan clown who closes the film by delivering its final lines. His role was so minor he was not even mentioned in the films credits.  This series was followed up with a sequel entitled Clowning Around 2, which was filmed in 1992.

==Synopsis==
Simon Gunner (Clayton Williamson), is a starstruck kid who aspires to become a circus clown. With the help of veteran funster Jack Merrick (Ernie Dingo), Simon ultimately fulfills his goal.

==Cast==
*Clayton Williamson - Simon Gunner
*Annie Byron - Una Crealy
*Jean-Michel Dagory - Anatole Tollin
*Ernie Dingo - Jack Merrick
*Van Johnson - Mr. Ranthow
*Rebecca Smart - Linda Crealy
*Noni Hazlehurst -  Sarah Gunner
*Jill Perryman - Miss Gabhurst
*Steve Jodrell - Skipper Crealy
*Heath Ledger - orphan clown (uncredited)

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 

 
 