How Are You? (film)
{{Infobox film
| name           = How Are You?
| image          = KakoSiFilmPoster.jpg
| image size     =
| alt            =
| caption        = Theatrical poster
| director       = Özlem Akovalıgil
| producer       = Özlem Akovalıgil
| writer         = Özlem Akovalıgil
| narrator       =
| starring       = Semahat Görüşanin   Mesut Akutsa   Deniz Çakır   Kemal Okur   Atilla Oener
| music          = Murat Yasin
| cinematography = Cengiz Uzun
| editing        = Özlem Akovalıgil
| studio         =
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = Turkey
| language       = Turkish
| budget         =
| gross          =
}}
How Are You? ( ) is a 2009 Turkish drama film directed by Özlem Akovalıgil about two groups of travelers as they travel to Europe from Istanbul.

== Plot ==
Semahat, whose parents emigrated to İstanbul from Sarajevo many years ago, decides to take a journey to her homeland in spite of her late age. Fatih, a film director, finds her story very interesting and wants to shoot the entire journey in the form of a documentary. Ufuk, a friend of Fatih’s, feels this journey will be an opportunity to leave behind the mess in his life and joins to help them. He records the story of the family on his camera while they are headed to Sarajevo. When they arrive, the journey will make them face the dreadful truths of the most recent war. There is no meaning of the past and no hope for the future for those who are struggling to survive.

== Release ==

=== Premiere ===
The film premiered on   at the Istanbul International Film Festival.

=== Festival screenings ===
*28th Istanbul International Film Festival (4-19 Apr, 2009)
*33rd Mostra International de Cinema-São Paulo International Film Festival (23 Oct-5 Nov, 2009)
*11th Mumbai Film Festival (29 Oct-5 Nov, 2009)
*1st Falstaff International Film Festival (14-20 Nov, 2009)
*4th Bursa International Silk Road Film Festival (14-22 Nov, 2009)   
*40th International Film Festival of India (23 Nov- 3 Dec, 2009)
*11th Dhaka International Film Festival (14-22 Jan, 2010)
*21st Ankara International Film Festival (11-21 Mar, 2010)    

== Reception ==

=== Reviews ===
Emrah Güler, writing for Hürriyet Daily News describes the film as, a bizarre road movie, which follows two groups of travelers as they head to Europe from Istanbul, and, plunges deep into the ethnic conflicts of the last decades in Europe.   

== See also ==
* 2009 in film
* Turkish films of 2009

==External links==
*   for the film

==References==
 

 
 
 