Rosario Tijeras
{{Infobox film 
 | name = Rosario Tijeras 
 | image =Rosario Tijeras.jpg
 | caption = Theatrical release poster
 | director = Emilio Maillé 
 | writer = Jorge Franco Ramos ( novel ) Marcelo Figueras
 | starring = Flora Martínez Manolo Cardona   Unax Ugalde
 | producer = Matthias Ehrenberg 
 | distributor = Columbia TriStar 
 | budget =
 | released =  
 | runtime = 126 minutes
 | country = Colombia
 | language = Spanish 
  | }} Jorge Franco. Goya Award for best foreign film. Rosario Tijeras is reportedly the second highest-grossing film in Colombian history.   

== Plot ==
The movie, based on a book by Jorge Franco, deals with the life of a beautiful woman involved with the subculture of sicarios, the motorcycle-riding hitmen of the slums of Medellín, Colombia, in the late 1980s-early 1990s.  Rosario, a dangerous woman who was molested and raped as a child, and now claiming to be owned by no man, lives a life on the edge while trying to come to terms with her past and the men in her life, not making the best choices along the way.

The film is told in flashback from the point of view of Antonio, a young man from the upper class of Medellín.  While at a Medellín disco one night, both he and his best friend, Emilio, simultaneously lay eyes upon the beautiful Rosario.  Emilio pursues her and begins an affair with her.  However, Emilios family rejects the relationship, which crushes Emilio.  Antonio continues his own platonic relationship with Rosario as her confidante.  When Rosarios brother, Jonhefe, with whom Rosario lives, is killed, Antonio and Rosario grow closer.  However, Rosario, who specializes in murdering men while kissing them, is a marked woman, and she eventually meets her comeuppance. Antonio takes her to the emergency room, from where he relates her story.

==Cast==
*Flora Martinez - Rosario Tijeras
*Manolo Cardona - Emilio
*Unax Ugalde - Antonio

==TV series==
RCN produced a TV series that is currently being shown on its network in Colombia. Telefutura broadcast the series in the United States from July 12, 2010 to October 4, 2010 after replacing Las muñecas de la mafia. María Fernanda Yépez, a Colombian actress, stars as Rosario Tijeras in the TV series. Currently, Telefuturas successor, Telefutura|Unimás, is rebroadcasting the novela in place of the recently cancelled ¿Quién Eres Tú?.

The TV series tells the story of Rosario Tijeras (Maria Fernanda Yepez), a woman raised in a slum that was 18 years old when she was raped by a local gang, and which causes her to change for the better after her classmate was killed by a drug lord, El Papa. Rosario avenges her rape by cutting El Caches testes with scissors (hence the name, Rosario Tijerias) and purposely goes to the market of her stepfather to get picked up to go to El Papas house so she can execute her plan. Once Rosario is alone in his bedroom, she grabs a gun he hid in a cabinet and kills him behind a pillow. After news spreads she killed El Papa, Rosario Tijeras gains notoriety and is sought out by El Rey De Los Cielos (The King of the Heavens) to become an assassin. Along the way, she has a stormy love affair with her two inseparable friends from wealthy families, Antonio de Bedout (Andres Sandoval) and Emilio Echegaray (Sebastián Martínez).

The story takes a five year jump from 1999 to 2004, where Rosario meets Emilio at a nightclub after she killed a target. Antonio notices her for the first time in five years and seeks to be with her once again. Their lives are separated by Rosario, which traps them and takes them to a world full of dangers, where she is a victim and instrument of death and they receive a deadly fate thanks in part to a woman they loved before Rosario, Paula Restrepo, who seeks to destroy Rosario for ruining her love life and at the end she is left without her best friend and husband. Rosario pays the ultimate price when she kills the brother of El Rey, El Teo, who killed her brother (Jhonefe), her close friend (Ferney) who tried to kill her by Teos orders, her unborn baby in a shootout, and Emilio at the cemetery. Rosario and Antonio plan their escape to Spain but are stopped short by El Reys men who fulfill his orders and kill both. The series ends with Rosario having a makeshift memorial made near her old home where she lived.

==Music== Rosario Tijeras

===TV Series Soundtrack===
*Si vos no estas aqui - El Vampiro
*Rosario - Mary Hellen, Wolfine, Q´sko
*Amor a la fuerza - Ultrajala, Q´sko, Kiño
*Soy fuego - Mugre T.O, Goez T.O, Mary Hellen
*El barrio - Wolfine, Pipe Bega, Goez T.O, Mary Hellen
*Triste y vacia - Goez T.O, Kiño
*El que a hierro mata - Ill Wonder, Alias Ramírez, Ultrajala
*Bang-Bang - T.O, Mary Hellen, Wolfine
*Mundos   - Avendaño, Ultrajala, Ill Wonder, Drazz
*Sed de venganza - Tribu Omerta, Kiño
*Maldita Mujer - Tito – Fatto (Caña. Brava), El Amarillo
*El rey de los cielos - Q´sko, Oso, Ebratt, Felpa
*Así somos - Q´sko, Pipe Bega, Laberinto, DJH

== See also ==
*Movies depicting Colombia
*List of Colombian films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 