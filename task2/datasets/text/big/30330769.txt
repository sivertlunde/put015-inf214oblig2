Dawson Isla 10
{{Infobox film
| name           = Dawson Isla 10
| image          = Dawson Isla 10.jpg
| alt            = 
| caption        =
| director       = Miguel Littín
| producer       = Miguel Littín Cristián de la Fuente Miguel Ioan Littín
| writer         = Miguel Littín
| starring       = Benjamín Vicuña Cristián de la Fuente Bertrand Duarte Pablo Krögh
| music          = Juan Cristóbal Meza
| cinematography = Miguel Ioan Littín
| editing        =
| studio         = Azul Films
| distributor    = 
| released       =  
| runtime        = 117 minutes
| country        = Chile
| language       = Spanish
| budget         = 2 million
| gross          = 
}}
Dawson Isla 10 is a 2009 Chilean drama film, written and directed by Miguel Littín, a Chilean film director. The screenplay is based on Isla 10, a book by Sergio Bitar about his experiences as a political prisoner; "Isla 10" was the substitute name their guards imposed him during his imprisonment.

==Plot== President Salvador political prison, specially designed as a concentration camp on Dawson Island, Tierra del Fuego. The camp had been used in the early 20th century to house Selknam and other indigenous peoples, moving them from the main island to end their interference with the large sheep ranches that had been established, as they persisted in hunting in their former territories.

In 1973 hundreds of other suspected communists and political dissidents were also imprisoned on Dawson Island by Pinochets government. Under the strict control of the Chilean Navy, these men struggled to survive the freezing temperatures and harsh conditions.

== Cast==
* Benjamín Vicuña as Sergio Bitar
* Bertrand Duarte as Miguel Lawner 
* Pablo Krögh as José Tohá
* Cristián de la Fuente as Teniente Labarca  Sergio Hernández as Comandante Sallay 
* Luis Dubó as Sargento Figueroa 
* Caco Monteiro as Fernando Flores 
* Horacio Videla as Dr. Arturo Jiron
* Matías Vega as Osvaldo Puccio 
* Andrés Skoknik as Orlando Letelier
* Elvis Fuentes as Clodomiro Almeyda
* Sergio Allard as Aristóteles España

==Submissions== Best Spanish 24th edition of Goya Awards.

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 