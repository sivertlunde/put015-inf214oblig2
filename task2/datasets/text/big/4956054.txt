Grand Slam (1967 film)
{{Infobox film
| name           = Grand Slam Original Title: Ad ogni costo
| image          =GrandSlam67.jpg
| image_size      =
| caption        = Grand Slam film poster
| director       = Giuliano Montaldo
| producer       = Arrigo Colombo Giorgio Papi
| writer         = Paolo Bianchini Augusto Caminito Mino Roli
| starring       = Janet Leigh Klaus Kinski Edward G. Robinson Adolfo Celi Robert Hoffmann Riccardo Cucciolla
| music          = Ennio Morricone
| cinematography = Antonio Macasoli
| editing        = Nino Baragli
| distributor    = Paramount Pictures
| released       = 
| runtime        = 119 min.
| country        = Italy Spain West Germany
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Italian crime film directed by Giuliano Montaldo and stars Edward G Robinson, Klaus Kinski and Janet Leigh. 

==Plot==
A seemingly mild-mannered teacher, Professor James Anders (Robinson), is an American working in Rio de Janeiro. Bored with years of teaching, Anders decides to put together a team to pull off a diamond heist during the Rio Carnival in Brazil.

A team of four international experts is brought together to carry out the robbery: a safe cracking specialist, a master thief, a mechanical genius, and a playboy whose job it is to seduce the only woman with a key to the building holding the diamonds, the lovely Mary Ann (Leigh).  Anders also hires a "professional", whose task is to kill the other members of the team after the job is finished.

The team develops a series of mechanical devices to defeat the layers of protection built into the "Grand Slam 70", the safe system in which the diamonds are stored.  Lasers criscross the entry corridor and a sensitive microphone listens for sounds while the safe and its environs are secured.

The team successfully enters the safe using a pneumatic trestle to bypass the lasers by crawling over them. One by one, they are killed while making their escape.

Anders ends up with the diamonds in a small letters case, sitting in an outdoor cafe...but loses them in the films last scene to a thief on a motorcycle.

==Cast==
* Janet Leigh - Mary Ann
* Robert Hoffmann - Jean-Paul Audry
* Klaus Kinski - Erich Weiss
* Riccardo Cucciolla - Agostino Rossi
* George Rigaud - Gregg
* Adolfo Celi - Mark Milford
* Edward G. Robinson - Prof. James Anders Jussara - Stetuaka
* Miguel Del Castillo - Manager
* Luciana Angiolillo
* Valentino Macchi
* Anny Degli Uberti
* Aldo Bonamano

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 