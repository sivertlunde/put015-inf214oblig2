Hilde (film)
{{Infobox Film
| name           = Hilde
| image          = 
| caption        = 
| image_size     = Kai Wessel
| producer       = Judy Tossell Jens Meurer
| writer         = Maria von Heland
| based on       =  
| starring       =Heike Makatsch
| music          = Martin Todsharow
| cinematography = Hagen Bogdanski
| editing        = Tina Freitag
| distributor    = 
| released       =   
| runtime        = 137 min
| country        = Germany
| language       = German budget = 
| gross          = 
}}
 German biographical Kai Wessel
and starring Heike Makatsch, Dan Stevens and Monica Bleibtreu.  It depicts the life of the German actress Hildegard Knef.

==Plot==
In 1966 Hildegard Knef returns to Germany. While she prepares for a concert she thinks back to the beginnings of her career. Flashbacks show how she became an actress and then started a second career as a singer.

==Cast==
*Heike Makatsch: Hildegard Knef
* 
*Monica Bleibtreu: Else Bongers
* 
*Johanna Gastdorf: Frieda Knef
*Trystan Wyn Puetter: Kurt Hirsch
*Michael Gwisdek: grandfather
*Roger Cicero: Ricci Blum
* 
* 
*Fritz Roth: step-father
* 
* 
* 

==Reception==
Kirk Honeycutts wrote "Hilde" was an "outstanding biopic about Hildegard Knef with a captivating performance" by Heike Makatsch but also felt the screenplay was "at times superficial".   Varietys Derek Elley attested Heike Makatsch a "remarkably cohesive performance" which was true to each "physical mannerism" of Hildegard Knef. "Easy on the eyes but rarely going more than skin-deep" was his roundup. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 


 
 