And the Violins Stopped Playing
 
{{Infobox film
| name           = And the Violins stopped playing  Original title: I Skrzypce Przestaly Grac  
| image          = Violinsstoppedplaying.JPG 
| caption        = English language poster
| director       = Alexander Ramati 
| producer       = Alexander Ramati 
| screenplay     = Alexander Ramati 
| based on       =  
| narrator       = 
| starring       =  
| music          =  
| cinematography = Edward Klosinski
| editing        = Miroslawa Garlicka
| studio         =    Orion Television Distribution 
| released       =  
| runtime        = 116 min. 
| country        =  
| language       = English 
| budget         = 
| gross          = 
}}
 historical drama film written produced and directed by Alexander Ramati and based upon his biographical novel about an actual group of Romani people who were forced to flee from persecution by the Nazi regime at the height of the Porajmos (Romani holocaust), during World War II.     

==Synopsis==
The story opens in 1941 in Brest-Litovsk, with Dymitr Mirga (Horst Buchholz), a prominent Gypsy violin player, entertaining a group of Nazis in a restaurant. At first the Nazis enjoy the entertainment and assure the musicians that the ongoing removal of the regions Jews is being conducted for the sake of the Romani. However, Dymitr soon realizes the truth, and asks the head of the Gypsy community to lead its evacuation into Hungary, which at that time was still independent. The leader is reluctant to comply, and the communitys council eventually forces him to resign, giving his position instead to Dymitr Mirga.  The son of the deposed leader had been betrothed to a beautiful Romani named Zoya Natkin (Maya Ramati), who instead chose to marry Dymitrs son, Roman (Piotr Polk). 

On their journey to Hungary, some of the Gypsies desert the group and are killed by the Nazis. Others voluntarily split off, in hopes that by having smaller numbers they will appear to be merchants rather than Gypsies. Dymitrs small company eventually performs the sacrifice of selling their jewels to buy horses from another Romani community, allowing their group to move more quickly. Many are nevertheless killed by the Nazis. The sympathetic population gives them burials and provides a chance for their comrades to meet and mourn their loss. In time, the resolute Dymitr reaches Hungary with his much-diminished group of followers, including his wife Wala (Didi Ramati), his son Roman and daughter-in-law Zoya, Zoyas family and Romans "rival," the son of the former leader, who was killed by Nazis. All Dymitrs efforts prove futile when the Nazis finally invade Hungary in 1944. 

A Nazi column takes the captive Romani to Auschwitz concentration camp|Auschwitz, where the infamous Col. Kruger (Jan Machulski) has been performing medical experiments conducted on prisoners. Before their arrival, Dymitrs daughter escapes out through the window of one of the cattle trucks. At the camp. Dymitr Mirga is forced to play for the Nazis, whilst his son Roman receives minor privileges because of his skill as a translator. However, when Romans wife Zoya dies, the young man begins to consider his fathers urging that he escape. Roman approaches his friend and former rival, and recognizing that their families are marked for death, the two agree to make an attempt. The attempt succeeds, and they manage to reconnect with Romans younger sister who escaped from the cattle truck.

The film ends with the war over. As three Romani carriages head off into a sunset, carrying—we assume—Roman, his friend and his younger sister, the narrator concludes that the "Gypsy nation has yet to receive any compensation."

==Cast==
 
 
* Horst Buchholz as Dymitr Mirga
* Piotr Polk as Roman Mirga 
* Marne Maitland as Sandu Mirga 
* Jan Machulski as Col. Krüger 
* Aleksander Bardini as Greczko Szura
* Jerzy Nowak as DoctorProfessor Epstein
* Wladyslaw Komar as Dombrowski
* Wiktor Zborowski as Tomasz
* Aleksander Ford as Zenon 
* Didi Ramati as Wala Mirga
* Maya Ramati as Zoya Natkin/Mirga
* Kasia Siwak as  Mara Mirga
 
* Bettine Milne as Rosa Mirga
* Aldona Grochal as Walas Sister
* Wieslaw Wójcik as Bora Natkin
* Ernestyna Winnicka as Zoyas mother
* Marcin Tronski as Dr. Josef Mengele
* Zitto Kazann as Mikita
* Wojciech Pastuszko as Koro
* Jacek Sas-Uhrynowski as Pawel
* Marek Barbasiewicz as Count Paszkowski
* Krzysztof Swietochowski as Franko
* Judy Hecht Dumontet as Zenons wife
* Ewa Telega as Ira
 

==Production==
The film was shot on Polish locations in Łańcut, Łódź, and Kraków. 

==Releases== Finish theaters as Salahanke and Finish television as Ja viulut vaikenivat, and in West Germany as Ja viulut vaikenivat. 
DVD release in 2003 included DVD extras of Orion trailers, video clips speaking about the film and its history, and clips about the films stars.   The film was exhibited in 2008 as part of a retrospective of the works of cinematographer Edward Klosinski.     In Łódź the film was centerpoint and focus of a 2009 exhibition celebrating the 65th anniversary of the liquidation of the Litzmannstadt Ghetto. 

==References==
 

==External links==
*  

 
 
 
 
 
 