Perils of the Royal Mounted
{{Infobox film
| name           = Perils of the Royal Mounted
| image          = Perils_of_the_Royal_Mounted.jpg
| image_size     =
| caption        =
| director       = James W. Horne
| producer       = Larry Darmour
| writer         = Basil Dickey Scott Littleton Louis E. Heifetz Jesse Duffy Original screenplay
| narrator       = Knox Manning Robert Stevens Kenneth MacDonald Herbert Rawlinson Richard Fiske
| music          = Lee Zahler
| cinematography = James S. Brown Jr. Black and white Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 serial released by Columbia Pictures.

==Plot== Northern genre elements including trappers, lumberjacks, trading posts, rebellious braves, forest fires, avalanches, and a wide range of dangerous wildlife.

==Cast==
{|
|- Robert Kellard|Robert Stevens || Sgt. Mack MacLane, RCMP 
|- Nell ODay || Diane Blake 
|- Kenneth MacDonald Kenneth MacDonald &nbsp;  &nbsp; || Mort Ransome 
|- Herbert Rawlinson || Richard Winton 
|- John Elliott John Elliott || Factor J. L. Blake 
|- Richard Fiske || Constable Brady 
|- Forrest Taylor || Preacher Hinsdale 
|- George Chesebro || Gaspard, chief thug
|- Jack Ingram Jack Ingram || Baptiste, chief thug
|-
|I. Stanford Jolley || Pierre, thug
|- Al Ferguson || Mike, thug
|- Charles King Charles King || Curly, thug
|- Bud Osborne || Jake, thug
|- Justin Cousson || Ricky, Radio operator
|- Nick Thompson Nick Thompson Black Bear, medicine man
|- Art Miles || Chief Flying Cloud
|- Rick Vallin|Richard Vallin || Little Wolf
|- Hank Bell || Martin, trapper-townsman
|- Tom London || Gaynor, trapper-townsman
|- Kermit Maynard || Constable Collins
|- Stanley Price || Hood, phony Mountie thug
|- Harry Harvey Harry Harvey || Denny Burke, telegrapher
|- Ed Cassidy Ed Cassidy || Jenson, crooked warehouseman
|- Kenneth Harlan || John Craig, phony commissioner
|-
|C. Montague Shaw || Commissioner Phillips
|- Ted Adams Ted Adams || Henchman
|- Robert Barron Robert Barron || Henchman
|- Iron Eyes Cody || Indian, pursuit party leader
|-
|}

===Cast notes===
*Actor Robert Kellard appears billed as Robert Stevens.

==Chapter titles==
# The Totumsic|  Talks
# The Night Raiders
# The Water Gods Revenge
# Beware, the Vigilantes
# The Masked Mountie
# Underwater Gold
# Bridge to the Sky
# Lost in the Mine
# Into the Trap
# Betrayed by Law
# Blazing Beacons
# The Mounties Last Chance
# Painted White Man
# Burned at the Stake
# The Mountie Gets His Man
 SOURCE:  

==Other versions==
*This serial was released in Latin America in March 1943, under the title Los Valientes de la Guardia, in English with Spanish subtitles.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 