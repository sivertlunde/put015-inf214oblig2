The World, the Flesh and the Devil (1914 film)
 The World, the Flesh and the Devil}}
{{Infobox film
| name           = The World, The Flesh and the Devil
| image          =
| caption        =
| director       = F. Martin Thornton
| producer       = Charles Urban
| writer         = Laurence Cowen
| starring       = Frank Esmond Stella St. Audrie Warwick Wellington
| music          =
| cinematography = Natural Colour Kinematograph
| released       =  
| runtime        = 50 minutes
| country        = United Kingdom
| language       = English
}}
 silent drama film. The film, now considered a lost film,  was made using the additive color Kinemacolor process.

The title comes from the Book of Common Prayer: "From all the deceits of the world, the flesh, and the devil, spare us, good Lord." 

==Plot==
An intensely unhappy woman hatches a plot to switch the babies of a poor family and a rich family. But the nurse hired to pull off this transfer refuses to go through with it, leaving each baby with its proper family.  When the babies are grown, the man from the poor family (who has been led to believe that he did come from the rich family) goes to the house of the other and throws him out. The remainder of the movie deals with the frustrations of mistaken identity.

==Cast==
* Frank Esmond — Nicholas Brophy
* Stella St. Audrie — Caroline Stanger
* Warwick Wellington — Sir James Hall
* Charles Carter — Rupert Stanger / Dyke
* Rupert Harvey — Robert Hall
* Jack Denton — George Grigg
* Gladys Cunningham — Mrs. Brophy
* Frances Midgeley — Gertrude Grant
* Mercy Hatton — Lady Hall
* H. Agar Lyons — The Devil
* Nell Carter — Beatrice Cuthbert
* Frank Stather — Inspector Toplin
* Roger Hamilton — Wylde

==See also==
* List of early color feature films
* List of lost films

==External links==
*  
*  

 

 
 
 
 
 
 
 
 

 