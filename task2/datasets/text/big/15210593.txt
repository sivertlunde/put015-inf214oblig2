Krodhi
 
{{Infobox film
| Name           = Krodhi
| Image          =
| Image Size     =
| Caption        =
| director       = Subhash Ghai
| producer       = Ranjit Virk
| writer         = Subhash Ghai Nabendu Ghosh
| narrator       =  Pran Sachin Sachin Ranjeeta Kaur Amrish Puri Moushumi Chatterjee
| music          = Laxmikant-Pyarelal
| cinematography = Kamalakar Rao
| editing        = Waman Bhonsle Gurudutt Shirali
| studio         = 
| distributor    = Ranjit Films Shemaroo Video Pvt. Ltd
| released       = 3 February 1981
| runtime        = 161 mins
| country        = India
| language       = Hindi
}}
 Hindi film directed by Subhash Ghai and starring Dharmendra, Shashi Kapoor, Zeenat Aman, Pran (actor)|Pran, Premnath, Sachin (actor)|Sachin, Ranjeeta and Hema Malini in an extended special appearance. The movie was produced by Ranjit Virk and the music was composed by Laxmikant-Pyarelal.

==Summary==
Unaware of his parents, as he was abandoned as a young child, Vikramjit Singh alias Vicky grows up amidst hatred. The only affection he receives is from the local school-teacher, Shakarbaba, and a young girl named Aarti. Vicky educates himself by reading all possible books. He keeps in touch with Aarti, and they grow up in love with each other. On the day Vicky decides to propose for marriage to Aarti, she is sold by her lecherous uncle for a few thousand rupees, to a group of men who attempt to rape her, but she kills herself. Enraged by this, Vicky hunts down her uncle, and her molesters and kills them by throwing from a high-rise building. Vickys anger prompts him to more daredevil deeds, and he ends up becoming the ultimate underworld don with a private army of his own. Then one of his associates, Neera, gets married to CBI Officer Kumar Sahni, and Vicky is on the run, and believed to be dead when his bullet-proof crashes. Years later Neera comes to visit her in-laws in a small town, and is introduced to a kind and saintly person with special powers for healing named Shradhanand. She is shocked to find that Shradhanand is none other than Vicky. Now Vicky will have to decide to run again, or kill this only witness, and continue living without any trouble for the rest of his life.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Phoolmati Ka Gajra"
| Asha Bhosle
|-
| 2
| "Chal Chameli Baag Mein"
| Suresh Wadkar, Lata Mangeshkar
|-
| 3
| "Ladkiwalo Ladki Tumhari Kunwari Reh Jaati"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Woh Masiha Aaya Hai"
| Lata Mangeshkar
|}
== External links ==
*  

 

 
 
 
 

 