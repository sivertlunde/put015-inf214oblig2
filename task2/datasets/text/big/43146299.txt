America: Imagine the World Without Her
 
{{Infobox film
| name           = America: Imagine the World Without Her
| image          = America Imagine a World Without Her.jpg
| alt            =
| caption        = Theatrical release poster
| director       = {{Plainlist |
* Dinesh DSouza
* John Sullivan
}}
| producer       = {{Plainlist |
* Dinesh DSouza
* Gerald R. Molen
}}
| writer         = {{Plainlist |
* Dinesh DSouza
* John Sullivan
* Bruce Schooley
}}
| based on       =  
| starring       = Dinesh DSouza
| music          = Bryan E. Miller
| cinematography = Benjamin Huddleston
| editing        = {{Plainlist |
* Rickie Lee
* Jeffrey Linford
}} Lionsgate
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $14.4 million
}}
America: Imagine the World Without Her is a 2014 political documentary film by   (2012). In the follow-up, the conservative filmmaker seeks to portray United States history in a more positive light, in contrast to perceived liberal critiques of its history, including theft of Native American and Mexican lands, black slavery, contemporary foreign policy, and its capitalist system.  DSouza collaborated with John Sullivan and Bruce Schooley to adapt his book of the same name into a screenplay. DSouza produced the documentary with Gerald R. Molen and directed it with Sullivan. The film combined historical reenactments with interviews with different political figures.
 Independence Day on July 4, 2014. CinemaScore reported that the opening-weekend audiences gave the film an "A+" grade. The film grossed  , which made it the highest-grossing documentary in the United States in 2014, though DSouzas previous documentary 2016 had grossed over  . Most professional film critics panned the film as partisan. Political commentators analyzed DSouzas rebuttal of Howard Zinns criticisms, the filmmakers treatment of Saul Alinsky, Barack Obama, and Hillary Clinton, and DSouzas depiction of his prosecution. Conservative commentators expressed a mix of full and qualified support for the documentary and DSouzas intentions. Following the documentarys theatrical release, Republican state legislators in Florida have proposed legislation to require students to see the documentary.

==Synopsis== counterfactual histories Michael Eric Ward Churchills wealth inequality that has helped shape the political careers of Barack Obama and Hillary Clinton.

DSouza argues that Americas wealth has been created, not stolen. He says the $700 used to purchase colonial Manhattan from American Indians could buy many desolate parcels globally today, but that individual industry has made New York real estate worth billions. He states that in Europe, India, and elsewhere most countries have been founded on conquest, and observes that the American pattern of wealth creation hasnt been the universal norm. He cites examples like Arab historian Ibn Khaldun preferring looting to trade and says that merchants form Hinduism’s second-lowest social caste.
 Reconquista advocate Charles Truxillo is contrasted with an interviewed American of Mexican descent who says he has no desire to return to a poverty and crime ridden Mexico and instead wants to live the "American Dream".

DSouza says that slavery impeded American development, rather than boosting it. The film argues that slavery was an omnipresent phenomenon for most of human history, but that its abolition was "uniquely Western", noting the rarity of a "great war fought to end slavery" like the American Civil War. According to the film the Declaration of Independence essentially says “liberty is the solution to injustice,” a “promissory note” cashed throughout history by Americans such as Martin Luther King, Jr.. Madam C.J. Walker|C.J. Walker, the black entrepreneur and daughter of slaves who is regarded as Americas first self made female millionaire, is cited as an example of the type of individual success story the American system allows that is ignored by historians like Zinn because it undermines their leftist narrative.  Columbia University economist Jagdish Bhagwati is shown saying that the “world is embracing the free market,” for which there is “no reason for us to be apologetic.” The film outlines how free enterprise, consumer choice rather than coercion, has raised living standards by making existing goods cheaper and creating new ones.
 Hanoi Hilton captivity is interviewed discussing his desire to liberate Vietnam. DSouza reflects on Lincolns assassination and the continuing cost of freedom, saying that Americans no longer have past heroes like Washington, Lincoln and Reagan, but  "we do have us” in “our struggle for the restoration of America.”
 

==Cast==
* John Koopman ... General George Washington
* Don Taylor ... President Abraham Lincoln
* Josh Bonzie ... Frederick Douglass
* Janitta Swain ... Madame C. J. Walker
* Michelle Swink ... Mary Todd Lincoln
* Rett Terrell ... Alexis de Toqueville
* Rodney Luis Aquino ... Hernan Cortes
* Michael D. Arite ... Major Henry Rathbone
* Chad Baker ... Gustave de Beaumont
* Rich Bentz ... Saul D. Alinsky

==Interviews==

Dinesh DSouza conducted interviews with the following individuals: 

* Charmaine White Face, a Native American activist
* Noam Chomsky, a political commentator and anarcho-syndicalism|anarcho-syndicalist activist
* Michael Eric Dyson, a professor of sociology at Georgetown University junior United United States Senator for Kentucky
* Ted Cruz, the junior United States Senator from Texas
* Alan Dershowitz, a scholar on United States constitutional law and criminal law

==Production==

America: Imagine the World Without Her is directed by   allowed the filmmakers to raise financing for America. Re-enactment scenes were filmed in Camden, South Carolina, Tampa Bay Area|Tampa-St. Petersburg, Florida, and Oklahoma City, Oklahoma.  Actor John Koopman III, a resident of Colchester, Connecticut who had portrayed General George Washington at state and national parks throughout the United States, was cast to portray Washington in the documentary. Koopman brought his own historical wardrobe and horse for filming, which took place in Camden over the course of four days. 

The filmmakers chose to feature clips of celebrities including Woody Harrelson, Matt Damon, and Bono to illustrate the documentarys points to audiences who may be unfamiliar with historical figures like Frederick Douglass. Harrelson is shown condemning the United Statess treatment of Native Americans. The film also shows Howard Zinns history book A Peoples History of the United States being mentioned by Damons character in the film Good Will Hunting as well as in the TV series The Sopranos. A clip featuring Bono, who did not participate in the production, is shown to illustrate support for American exceptionalism.   

The filmmakers also sought to license the song "Its America (song)|Its America" recorded by Rodney Atkins, but the licensing was denied by one of the songwriters due to the political premise of the documentary. They instead involved Dave Mustaine, founder of Megadeth, who recorded a heavy metal guitar version of "The Star Spangled Banner" for the film. The filmmakers also licensed the song "America" by Imagine Dragons and "Home (Phillip Phillips song)|Home" by Phillip Phillips. 

In addition to the theatrical cut, DSouza edited an 80-minute cut for educational purposes, removing interviews with political pundits. He said, "Its all purely historical content now." 

==Marketing==

Dinesh DSouza released the trailer for America: Imagine the World Without Her on  , 2014.  He later screened the trailer to   at the Conservative Political Action Conference in Washington, DC on  , 2014.  The filmmakers hired Christian marketing firms to create a sermon, replete with video clips, based on the documentary, and make it available for download. The Hollywood Reporter said on  , 2014 that over 1,000 preachers had downloaded the sermon and that insiders expected the number to reach 5,000 before the films release. The filmmakers also emailed 120,000 evangelical churches asking them to promote the film and hired the company Faithit.com to contact 80,000 Christian consumers.  In the following July, DSouza and fellow filmmaker Bruce Schooley traveled to the state of Texas to promote the documentary on radio and television programs owned by Glenn Beck.   

DSouza wrote the book America: Imagine the World Without Her, on which the documentary is based. When the warehouse club Costco pulled the book from its shelves shortly before the films release, conservative media and fans on social media criticized the move. Costco said it pulled the book due to low sales. DSouza disputed the explanation, saying the book had only been out a few weeks and had surged to #1 on Amazon.com, while Costco stocked hundreds of much lower-selling books. He and other conservatives asserted it was pulled because one of Costcos co-founders, James Sinegal, supported Obamas politics.   Costco reordered the book and cited the documentarys release and related interest for the reorder. 

Since America: Imagine the World Without Her and its predecessor 2016: Obamas America share "America" in their titles, several film websites, including   and  , were being shown instead. They expressed concern that a lack of results, including showtimes, would affect the documentarys gross.  DSouza claimed that the lack of search results was politically motivated on Googles part.  A preliminary fix stopped listing results for either of DSouzas documentaries. Google said the term "America" being common in film titles prevented specific results, and it updated its Knowledge Graph to show results for the 2014 documentary.  

In August 2014, the nonprofit organization Movie to Movement invited President Barack Obama and members of the United States Congress to a free screening of America. The Hollywood Reporter said the organization "helps to promote small, wholesome movies, many of which seem to have a Christian or conservative theme to them though the group is non-partisan". Movie to Movements founder and CEO said he budgeted $5,500 to pay for the politicians tickets and would secure delivery of a digital copy if the documentary was not available in a theater near a politician. 

==Release==
 , acquired rights to distribute America: Imagine the World Without Her in theaters in the United States. Historically, it distributed in theaters two political documentaries, Fahrenheit 9/11 (2004) and Religulous (2008).  The UK-based Manifest Film Sales acquired rights to distribute America outside the United States with the goal of screening the documentary at the 2014 Cannes Film Festival, but the screening did not take place. 
 Independence Day on  , 2014.  On  , 2014, Lionsgate expanded the release to  . For the weekend of  , 2014, it grossed $2,743,753 and ranked 11th at the box office. The film concluded its theatrical run after   with a total gross of $14,444,502.  The Hollywood Reporter said the gross was "a very strong showing for a documentary film".  For 2014, America was the highest-grossing documentary in the United States.  The film did not perform as well as 2016: Obamas America, which grossed over  .    To date, America ranks as the sixth highest-grossing political documentary in the United States. 

Lionsgate released the film on Digital HD on  , 2014 and on DVD and Blu-ray on  , 2014,    a week before the   and Mr. Peabody & Sherman. It ranked seventh in Blu-ray sales with 26% of discs sold being Blu-ray. 

==Reception==
===Critical response===
 
The Times-Picayune reported, "America wasnt widely screened for critics, but the first handful of reviews are... not particularly glowing," saying that the reviews essentially labeled the film as "partisan".  The News-Press reported, "America has been criticized by some as offensive, right-wing propaganda."  The film review website Metacritic surveyed   and assessed 10 reviews as negative and 1 as mixed, with none being positive. It gave an aggregate score of 15 out of 100, which indicates "overwhelming dislike".    The similar website Rotten Tomatoes surveyed   and, categorizing the reviews as positive or negative, assessed 22 as negative and 2 as positive. Of the  , it determined an average rating of 2.9 out of 10. The website gave the film an overall score of 8% and said of the consensus, "Passionate but poorly constructed, America preaches to the choir."  The Hollywood Reporter s Paul Bond said the film performed well in its limited theatrical release, "overcoming several negative reviews in the mainstream media".  USA Today s Bryan Alexander said, "America was savaged by mainstream critics... It received an 8% critical score on RottenTomatoes.com... But the film received an 88% positive audience score on the same website."  Bond reported, "Conservatives... seem thrilled with the movie."  CinemaScore reported that its sample of opening night audiences gave the film what The Wrap identified as a rare A+ grade on a scale of A+ to F.  

Joe Leydon, reviewing for Variety (magazine)|Variety, called America "a slick, sprawling celebration of American exceptionalism that could, much like its predecessor, make a bundle by rigorously reinforcing the deeply held beliefs and darkest suspicions of its target audience". Leydon said the acting in the historical re-enactments was of inconsistent quality. The critic found that DSouza gave screen time to those with whom he disagreed, but said, "For the most part, however, DSouza gives the impression of someone obsessed with whitewashing any and all dark chapters in U.S. history books." Leydon commended the documentarys "tech values" as well as composer Bryan E. Millers opening theme.    The Hollywood Reporter s Stephen Farber said DSouza overstated "anti-American tenets ostensibly running rampant in our society" and that his responses to critiques of America "arent very convincing". Farber said of the films production quality, "The battle scenes are competent but no more than that, and the performances are perfunctory at best." Farber said the historical re-enactments would not impress moviegoers who had seen many other historical films, though he called Ben Huddlestons cinematography "striking". The critic concluded, "Here is one more dubious piece of agitprop that will delight the author’s fans and have very little impact on his opponents."  Metacritic scored each trade papers review of the film to be 30 out of 100. 

Metacritic assessed reviews from The A.V. Club, Indiewires The Playlist, Slant Magazine, and TheWrap as fully negative with no merit given.  TheWrap s James Rocchi said the documentary had straw man arguments favoring DSouza and had anecdotes in place of data, "The film is intellectually and factually spurious, in addition to being... deeply self-serving." Rocchi called America "technically inept" with "clumsy" editing and added, "The sound mix is incomprehensibly sloppy. Graphics look slapdash; historical recreations are either cheap-looking, unintentionally funny, or both." The critic said while liberal filmmaker Michael Moore "may be self-important at his worst", that he could direct a better film than DSouza and Sullivan.  The A.V. Club s David Ehrlich also said America had straw man arguments, "  hellbent on pacifying the American guilt they believe was responsible for Obama’s election, desperately attempting to assuage the national conscience about the evils of colonialism, capitalism, and racism." Ehrlich said, "Its admirable that DSouza is so willing to engage people who dont share his perspective, but his editing and the instructive music with which he pushes it suggest that hes not particularly interested in what they have to say." 

Rob Humanick, reviewing for Slant Magazine, said "The cynically opportunistic America descends into another one-note attack on the sitting president, beholden to the same plethora of taboos, half-truths, and outright lies traded en masse by mainstream conservatism for the last seven years." Humanick called the documentary "a carefully cultivated collection of false equivalencies, hyperbolic pronouncements, blatant recontextualizations of others arguments, and shameless appeals to patriotism, all within a vaguely fear-mongering framework of demonizing the other". The critic said exceptions were cited to excuse Americas history and that DSouzas criticism of Obama did not ask "greater fundamental questions". Humanick concluded, "Anyone whos ever actually studied history outside of public education, or read the texts alluded to throughout America (such as Howard Zinns A Peoples History of the United States), will understand the degree to which history has been flattened and narratives simplified for the sake of lending greater legitimacy to these binary-reliant lessons."  Gabe Toro, reviewing for The Playlist, said, "The film plays out like more of a bullet-point presentation than an actual film, taking each argument he thinks liberal minds are having and dissecting each, cherry-picking anomalies in order to confront some sort of liberal truth that doesnt exist." Toro called the documentary "artless propaganda, uninformed, sensationalistic and devoted to buzzphrases, ...simplicity, ...and grandstanding". The critic said, "Insidiously, these are some of the ways DSouza and co-director John Sullivan keep the film brisk and conventionally entertaining... Filled with soaring guitars, pointless blacksmith montages and recreations with porn-level production values... its all fist-pumping anti-thought, consisting of baseless revisionist history and idle contrarianism." 

Los Angeles Times s Martin Tsai, whose review Metacritic scored to be 40 out of 100, the highest of its sample of 11,  said, "He attempts to debunk   Zinn through apagoge, as if finding an exception to Zinns every rule will invalidate Zinns entire argument... DSouza makes some cogent points yet will not concede the existence of any gray area. The possibility that he and Zinn could both be right seems unfathomable." Tsai said, "America seems more intent on editorializing, razzling and dazzling than on stimulating civic debate." He summarized, "Its far more invested in elaborate historical reenactments, hypothetical dramatizations and special effects than interviews, research and data." 

Christian Toto, reviewing for Breitbart (website)|Breitbart, wrote that America gives "context to some of the countrys sins in a fashion rarely heard in popular culture. Each rebuttal needs more time, more explanation, but for those weaned on Howard Zinns A Peoples History of the United States it will be eye opening." 

===Accolades===
 Warsaw Uprising. 

==Political commentary==
 
 black slaveholders. Zuylen-Wood also compared DSouza to liberal filmmaker Michael Moore in how both use their roots to convey their messages and how they are both central characters in their documentaries, introducing "one ideological pathology after another" to moviegoers. 

Mark Stricherz of The Atlantic said that DSouza message suffered "the intellectual pitfalls of ignoring the critics", finding that he did not contextualize Obamas phrase "You didnt build that" in America. Stricherz said, "At times, America lives up to DSouza’s old intellectual standards. He meets in person with left-wing critics... He argues persuasively that Alexis de Tocqueville is a more reliable guide than Howard Zinn to troubling episodes in early American history such as slavery and the treatment of Native Americans." Stricherz concluded, "DSouzas pride, his belief he needs neither intellectual nor moral critics, has brought about his fall from the first rank of conservative intellectuals."  John Tamny of Forbes said, "D’Souzas America is noble in its effort to discredit myths about the U.S. as a genocidal, thieving, racist, capitalistically rapacious nation, but really, who believes this? It’s popular in the victimized portion of the conservative movement to assert that those who love the U.S., freedom, and the prosperity it delivers do so in silence out of fear that the majority haters will persecute them for having those views, but let’s be serious. This extreme kind of thinking is all too rare as we all well know." 

  said, "Dinesh is the anti-Moore: taking to the big screen to press conservative points... The shame narrators (let’s call them) focus on maybe 20 percent of the American story. Dinesh simply puts the other 80 percent back in."    Nordlinger bisected the documentary, "The first   deals with the shame narrative. The second deals with todays politics, and in particular presidential politics." The conservative commentator said, "The second movie confirms for me that one of Dinesh’s great advantages is that he is absolutely clear-eyed about the Third World. While liberal Americans romanticize it, he has lived it."   

In the liberal   called the film "racially charged agitprop".  In Salon.com|Salon, Elizabeth Stoker Bruenig called it a "laughable embarrassment" which ranges "from atrociously bad argumentation to humiliating propaganda". 

Kate OHare, writing for Breitbart.com, said, "None of these stories negates the horrors and injustices of the past, but thats not DSouzas point. Instead, hes offering a different context in which to view them, and hes done it before."   Commenting on the reception, Breitbart editor Ben Shapiro said “It is absurd to have   movie critics critiquing the politics of documentaries professionally; they seem unable to separate their artistic sensibilities from their political ones.” 

===Rebuttal of Howard Zinn===
 , author of A Peoples History of the United States, was a key focus in DSouzas documentary]]
In the documentary, DSouza counters four "indictments" of the United States made by historian   was part of mainstream education was incorrect, "Though influential, the book was hardly hegemonic. It was even sharply criticized by prominent historians." Hemmer said Eric Foners textbook Give Me Liberty! was more common than Zinns book and was even critical of the book as pessimistic. 

===Treatment of Alinsky, Obama, and Clinton===

In the documentary, DSouza says  s The Playlist, said DSouza "flat-out compares" Alinsky to the Devil and then suggests Alinskys influence on Clinton and Obama.  National Journal s Zuylen-Wood said despite Alinsky dying when Obama was a teenager in Hawaii, the film portrayed Obama as one of Alinskys "most famous disciples".  U.S. News & World Report s Hemmer said Alinsky was a focus in DSouzas film because President Obama was not up for reelection in 2016, so his argument about Obamas heritage could not apply to Hillary Clinton if she became a Presidential candidate. With Obama and Clinton both having links to Alinsky, Hemmer said Alinsky "has become the natural conduit to transfer criticisms of Obama to Clinton". 
 2016 U.S. presidential election. Beaumont-Thomas said the TV networks NBC and CNN avoided producing miniseries about Clinton, "Both right- and leftwing voices expressed concern that the series would be either too favourable to Clinton or too politically cautious. Liberal voices will now likely clamour for a counterweight to DSouzas film." 

===Filmmakers prosecution===
 his criminal conviction for violating election campaign finance laws. Joseph Amodeo, a political scientist and policy researcher for The Huffington Post, said the scene "appears to be an apology to his fans and an awkward show of penance for recent improprieties on his part."  Michael Berkowitz, also writing for The Huffington Post, said of the scene, "  suggestion that his own criminal conviction and his cheating on his wife are the result of political targeting are embarrassing and without support."  National Review s Fund said of the scene, "He clearly conveys his view that he was selectively prosecuted. But viewers should take the film on its own merits, he says, regardless of what they think of him." 

==Proposed legislation==
  Republican member Republican member of the Florida House of Representatives, to support his bill. After Combee watched the documentary and discussed it, he agreed to file a companion bill.     Combee filed the House bill in the following December, and the Times said the companion bill increased the likelihood of the legislation being adopted.  The proposed bill requires all of Floridas eighth and eleventh graders to watch America. The bill includes an option for parents to opt their children out of the film screening. 
 Collier Countys Libertarian Party, Jared Grifoni, did not contest the content but the attempted requirement, "We should be working to get rid of political and social engineering in schools regardless which side of the aisle is pushing it. This is the right side of the aisle pushing their agenda on students while accusing the left of the same thing." 

==See also==
 
* Political cinema
* List of documentary films
 

==References==
 

==Further reading==

* 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 