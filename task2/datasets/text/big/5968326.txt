Michael Clayton (film)
{{Infobox film
| name           = Michael Clayton
| image          = Michael clayton.jpg
| caption        = Promotional film poster
| alt            = A blurred pictured of a man with the words "The Truth Can Be Adjusted" superimposed
| director       = Tony Gilroy
| producer       = {{Plainlist| 
* Sydney Pollack
* Steven Samuels
* Jennifer Fox
* Kerry Orent
}}
| writer         = Tony Gilroy
| starring       = {{Plainlist| 
* George Clooney
* Tom Wilkinson
* Tilda Swinton
* Sydney Pollack
}}
| music          = James Newton Howard
| cinematography = Robert Elswit John Gilroy
| studio         = {{Plainlist| 
* Section Eight Productions
* Mirage Enterprises
* Castle Rock Entertainment
* SMC
}}
| distributor    = Warner Bros.
| released       =    
| runtime        = 119 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = $92.9 million  . Retrieved October 21, 2013. 
}} Tom Wilkinson, attorney Michael Clayton to cope with a colleagues apparent mental breakdown, and the corruption and intrigue surrounding a major client of his law firm being sued in a class action case over the effects of toxic agrochemicals.
 Academy Awards, Best Picture, Best Director Best Actor Best Supporting Actress.

==Plot== hit and run, Michael drives despondently and stops at a remote field, where some horses are standing on a hill. When he leaves his car to climb up the hill to admire the horses, the car explodes behind him.
 deposition in Robert Prescott, bugs in his apartment and phone. When they report that Arthur is building a case to expose his own client, Karen asks that Arthur be eliminated.  The same two men proceed to do as she directs. They secretly murder Arthur in a very sophisticated way designed to resemble a heart attack, possibly brought on by a drug overdose.
 settlement just a few days before and that Arthur had booked a flight to New York for one of the plaintiffs, Anna (Merritt Wever). He learns from Anna that she told no one of her conversations with Arthur, not even her attorney, arousing in Michael further suspicion about how his firm came to know of Arthurs conversations with the U-North plaintiffs. With the help of his other brother Gene (Sean Cullen) in the police department Michael gets access to Arthurs sealed apartment. There he discovers that Arthur seemed to be expecting Anna, as there is a bottle of champagne with two glasses in the refrigerator. There is a copy of a fantasy novel called Realm and Conquest that Michaels eleven-year-old son Henry has been absorbed in, and has recommended it to Arthur to read. Arthur has clearly read the book, having made annotations and highlighted numerous passages in the book. One of the illustrations in the book is of a horse standing on a hillside. Also in the book Michael finds a receipt from a copy store. At this point Michael is arrested for trespassing on sealed premises, and his brother Gene has to arrange for his release from custody. Next Michael discovers that Arthur had ordered three thousand copies of the confidential U-North document. Michael takes a copy with him, but the two hit men are tailing him and inform Karen of the situation. Michael is about to show his boss, Marty Bach (Sydney Pollack), what he has discovered, only to be offered a renewal of his employment contract as well as an $80,000 bonus he had requested to cover his debt, although it comes with a confidentiality agreement to prevent him from ever shaking down the firm, with Marty already knowing the shady nature of U-North, though not of the murder.
 Westchester County who had committed a Hit and run (vehicular)|hit-and-run, as seen at the start of the movie. He is being followed by the two men, who have trouble trailing him, but eventually get near Michaels trail and detonate the remote bomb while he is out of the car standing in the field with the horses, which he had stopped to look at since it resembled the scene from Realm and Conquest. An unharmed but startled Michael runs back to his now-burning car and throws his personal effects into the fire to fake his death and escapes into the woods.
 board meeting, settlement agreement. Michael is waiting for her when she exits the conference room, and informs her that he has access to copies of the U-North memo and that he knows about her role in Arthurs murder, and her subsequent unsuccessful attempt to kill him. He goads Karen into offering him $10 million for his silence. Karen reluctantly agrees, prompting Michael to reveal the phone in his pocket that has conveyed their conversation to the police. As he walks away, Karen falls to her knees in shock while detectives rush forward to arrest her and question the directors. Michael hands the U-North memo to Gene, then leaves the building, hails a cab, passes the driver 50 dollars and tells him to "just drive."

==Cast==
* George Clooney as Michael Raymond Clayton. As the "fixer" for Kenner, Bach, and Ledeen,  it is his job to clean up messes for the firm.  He refers to himself as a "janitor", and he does not appear to enjoy his job.
* Tom Wilkinson as Arthur Edens, senior litigation partner in the firm and a good friend of Michael Clayton and Marty Bach.  He has bipolar disorder and is the lead attorney for U-North in a class-action case.  Oscar for Best Supporting Actress for her performance.
* Sydney Pollack as Marty Bach, Head of Kenner, Bach, and Ledeen and a friend of Michaels.

==Release==

===Theatrical===
The film premiered August 31, 2007, at the Venice Film Festival and was shown at the American Films Festival of Deauville on September 2, 2007, and at the Toronto International Film Festival on September 7, 2007. It opened in the United Kingdom on September 28, 2007, and at the Dubai Film Festival in December 2007. The film opened in limited release in the United States on October 5, 2007, and opened in wide release in the U.S. on October 12, 2007. The film grossed USD $10.3 million on the opening week. It was re-released on January 25, 2008. The film has grossed $49 million at the North American domestic box office, and a total of $92.9 million worldwide. 

===Home media===
The film was released on DVD and Blu-ray Disc on February 19, 2008. The DVD contains deleted scenes and a commentary by writer/director Tony Gilroy. On March 11, 2008 the movie was also released on HD DVD.

==Reception==

===Critical response===
Michael Clayton received critical acclaim, and has a "certified fresh" score of 90% on   based on 36 critics indicating "universal acclaim". 

Owen Gleiberman of Entertainment Weekly gave it an "A" saying that it was "better than good, it just about restores your faith". Roger Ebert gave it a 4-star review and Richard Roeper named it the best film of the year.      It was also Richard Schickels top film of 2007, and he called it "a morally alert, persuasively realistic and increasingly suspenseful melodrama, impeccably acted and handsomely staged by Tony Gilroy".    Time magazine said that "Michael Clayton is not an exercise in high-tension energy; youll never confuse its eponymous protagonist with Jason Bourne. But it does have enough of a melodramatic pulse to keep you engaged in its story and, better than that, it is full of plausible characters who are capable of surprising—and surpassing—your expectations". 

===Top ten lists===
The film appeared on many critics top ten lists of the best films of 2007. 

* 1st – Claudia Puig, USA Today
* 1st – Richard Roeper,  At the Movies with Ebert & Roeper 
* 1st – Richard Schickel, Time (magazine)|TIME magazine
* 2nd – Peter Hartlaub, San Francisco Chronicle
* 3rd – Owen Gleiberman, Entertainment Weekly
* 5th – Rene Rodriguez, The Miami Herald
* 6th – Ann Hornaday, The Washington Post
* 6th – Ray Bennett, The Hollywood Reporter
* 7th – Jack Mathews, New York Daily News
* 7th – Kenneth Turan, Los Angeles Times Lady Chatterley) 
* 7th – Ty Burr, The Boston Globe
* 8th – A.O. Scott, The New York Times (tied with The Lives of Others)
* 8th – Kevin Crust, Los Angeles Times Shawn Levy, The Oregonian
* 8th – Steven Rea, The Philadelphia Inquirer
* 9th – Dennis Harvey, Variety (magazine)|Variety (tied with Romantico)
* 9th – Frank Scheck, The Hollywood Reporter

===Accolades===

====Awards====
* 80th Academy Awards (Academy of Motion Picture Arts and Sciences)
** Best Supporting Actress (Tilda Swinton)

* British Academy of Film and Television Arts
** Best Supporting Actress (Tilda Swinton)

* Dallas-Fort Worth Film Critics Association Awards
** Best Supporting Actress (Tilda Swinton)

* Kansas City Film Critics Circle Award
** Best Supporting Actress (Tilda Swinton)

* Vancouver Film Critics Circle Award
** Best Supporting Actress (Tilda Swinton)

* National Board of Review
** Best Actor (George Clooney)

* San Francisco Film Critics Circle
** Best Actor (George Clooney)

* Washington D.C. Area Film Critics Association
** Best Actor (George Clooney)

* Satellite Award
** Best Supporting Actor-Drama (Tom Wilkinson)

* London Film Critics Association
** Best British Actor of the year (Tom Wilkinson)

* Edgar Award
** Best Motion Picture Screenplay 

====Nominations====

* 80th Academy Awards
** Best Actor (George Clooney)
** Best Supporting Actor (Tom Wilkinson)
** Best Director (Tony Gilroy)
** Best Picture
** Best Original Screenplay (Tony Gilroy)
** Best Original Score (James Newton Howard)

* 61st British Academy Film Awards
** Best Actor in a Leading Role (George Clooney) John Gilroy)
** Best Original Screenplay (Tony Gilroy)
** Best Actor in a Supporting Role (Tom Wilkinson)

* 65th Golden Globe Awards   
** Best Motion Picture – Drama
** Best Performance by an Actor in a Motion Picture – Drama (George Clooney)
** Best Performance by an Actor in a Supporting Role in a Motion Picture (Tom Wilkinson)
** Best Performance by an Actress in a Supporting Role in a Motion Picture (Tilda Swinton)

* Broadcast Film Critics Association Awards
** Best Picture
** Best Actor (George Clooney)
** Best Supporting Actor (Tom Wilkinson)
** Best Supporting Actress (Tilda Swinton)
** Best Writer (Tony Gilroy)

* Chicago Film Critics Association Awards
** Best Actor (George Clooney)
** Best Director (Tony Gilroy)
** Best Picture
** Best Screenplay, Original (Tony Gilroy)
** Best Supporting Actor (Tom Wilkinson)
** Best Supporting Actress (Tilda Swinton)
** Most Promising Filmmaker (Tony Gilroy)

* London Film Critics Circle Awards
** Actor of the Year (George Clooney)
** British Actor of the Year (Tom Wilkinson)
** British Supporting Actress of the Year (Tilda Swinton)

* Satellite Awards
** Best Actress in a Supporting Role – Drama (Tilda Swinton)
** Best Original Screenplay (Tony Gilroy)

* Screen Actors Guild Awards
** Outstanding Performance by a Female Actor in a Supporting Role (Tilda Swinton)
** Outstanding Performance by a Male Actor in a Leading Role (George Clooney)
** Outstanding Performance by a Male Actor in a Supporting Role (Tom Wilkinson)

* Venice Film Festival
** Golden Lion (Tony Gilroy)

==Soundtrack==
 
 Best Original Score.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at Soundtrack Collector

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 