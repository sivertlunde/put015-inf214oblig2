The Secret Heart
{{Infobox film 
| name           = The Secret Heart
| image          = Secretposternew.jpg
| caption        = Theatrical release poster
| director       = Robert Z. Leonard
| producer       = Edwin H. Knopf William Brown Meloney 
| starring       = Claudette Colbert Walter Pidgeon June Allyson 
| music          = Bronislau Kaper
| cinematography = George J. Folsey
| editing        = Adrienne Fazan
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 97 minutes
| country        = United States
| awards         =
| language       = English
| budget         =$1,735,000  . 
| gross = $2,657,000 
}} 
The Secret Heart is a 1946 film directed by Robert Z. Leonard, and starring Claudette Colbert, Walter Pidgeon and June Allyson.

==Plot==

Lee is engaged to marry Larry Adams, a spendthrift widower with two children, son Chase and daughter Penny. Lee had been living in England with her guardian aunt, who didnt approve of the match since Larry was an alcoholic, and while returning to America on an ocean liner, she meets Chris Matthews, a close friend of Larrys. Despite her loving feelings for Chris, she marries Larry, and moves to his farm in Rhode Island. Larrys talent is playing the piano, which he teaches Penny, but he gave up this ambition to work in a bank, to please his father. This frustrated ambition has ruined his life, and over the next two years Lee tries to confront his alcoholism, while trying to win Pennys confidence. While Lee is out for the night with Chris, Larry dies, his body found at the bottom of a cliff. He had committed suicide after two years of marriage, and on his death, it is reported that Larry had embezzled money from his clients. Lee sends Chris away and moves the family away from the farm, to New York where she takes a job to pay off Larrys debts, and withholds the truth from Penny, wanting to shield her from the stigma of scandal. Penny makes a hero out of Larry, who she believes died of a heart attack, and is unable to embrace Lee, who is now left to look after them alone.

Ten years later, Penny, who behaves strangely, has dropped out of school and plays the piano incessantly for her fathers memory when nobody else is around, is the patient of psychiatrist Dr. Rossiger. Lee goes to see him, concerned about Pennys behaviour, and the story up to this point is recalled in flashback. The doctor advises that they move back to the farm for the summer, since that is where the death occurred, and he believes that confronting the past will help cure Penny. Chase returns from the navy after three years and seeks a job with Chris, who now owns a shipyard. He introduces Penny to his navy friend Brandon Reynolds. They all move to the farm, together with Chases friend Kay Burns, where Chris reenters Lees life after a ten-year absence, and Lee realizes that it was Chris she loved all along and let get away. Once at the farm, Penny becomes disenchanted with her fathers memory when Chase tells her the truth, and becomes despondent, feeling that Chris is the only person she can confide in. Although Brandon is interested in Penny, she loves Chris, and is devastated when she finds him in Lees arms. Penny then tries to kill herself by jumping off a cliff, as Larry had done, but Lee intervenes in time to prevent it. The healing process begins and when Lee tells Penny the complete story of her fathers life, Penny is finally able to embrace Lee. At the end Penny graduates, having adopted Chris as her father, and resumes her romance with Brandon

==Cast==
*Claudette Colbert as  Leola Lee Addams 
*Walter Pidgeon as  Chris Matthews 
*June Allyson as  Penny Addams 
*Lionel Barrymore as  Dr. Rossiger 
*Robert Sterling as  Chase N. Addams 
*Marshall Thompson as  Brandon Reynolds  Elizabeth Patterson as  Mrs. Stover 
*Richard Derr as  Larry Addams 
*Patricia Medina as  Kay Burns 
*Eily Malyon as  Miss Hunter 
*Dwayne Hickman as  Chase (as a Child)

==Reception==
The film earned $2,591,000 in the US and Canada and $1,309,000 elsewhere, resulting in a profit of $891,000. 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 