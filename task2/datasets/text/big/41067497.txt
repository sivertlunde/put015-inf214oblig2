Malaiyoor Mambattiyan
{{Infobox film
 | name = Malaiyoor Mambattiyan
 | image = MMambattiyan.jpg.jpg
 | caption = DVD Cover Rajasekhar
 | producer = Siva. Ramadoss W. S. Sivashankar K. M. Ravi K. Muthukumaran Rajasekhar
 | story = Raghavan Thambi Senthil
 | music = Ilaiyaraaja
 | cinematography = V. Ranga
 | editing = R. Vittal
 | studio = Sri Devi Bagavathy Films
 | distributor = Sri Devi Bagavathy Films
 | released =  
 | runtime = 135 minutes
 | country = India
 | language = Tamil Rs 85 lakhs
 }} 1983 Cinema Indian feature directed by Rajasekhar (director)|Rajasekhar, starring Thiagarajan, Saritha and Silk Smita. The film was remade in Hindi by Rajasekhar himself as Gangvaa with Rajinikanth and Shabana Azmi in the lead. Later in 2011, Thiagarajan remade this film with his son Prashanth (actor)|Prashanth, titled as Mambattiyan.

==Plot==

Malaiyoor Mambattiyan is the story of a group of youngsters in the village headed by Mambattiyan, who lead life in a forest by robbing the rich and distributing the wealth to the poor; thus Mambattiyan becomes the local Robin Hood.

==Cast==

* Thiagarajan
* Saritha
* Jaishankar
* Silk Smita
* Senthamarai
* Goundamani
* Jayamalini
* Sangili Murugan
* Muthu Bharathi Senthil
* Jayashree

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Vaali || 04:39
|- Vairamuthu || 04:11
|-
| 3 || Kaattu Vazhi Pogum || Ilaiyaraaja || 03:41
|-
| 4 || Vellarikka || Gangai Amaran, S. P. Sailaja || Gangai Amaran || 04:57
|}

==Remakes==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" class=sortable
|- bgcolor="#CCCCCC" align="center"|-

! Year
! Film
! Language
! Cast
! Director
|-
| 1984
|Gangvaa Hindi
| Rajinikanth, Shabana Azmi, Suresh Oberoi, Amrish Puri, Sarika Rajasekhar (director)|Rajasekhar
|-
| 2011
|Mambattiyan Tamil cinema|Tamil Prashanth Thiagarajan|Prashanth, Meera Jasmine, Mumaith Khan, Prakash Raj Thiagarajan
|-
|}

==References==
 
*http://articles.timesofindia.indiatimes.com/2011-06-25/news-interviews/29702192_1_film-songs-villagers
*http://www.imdb.com/title/tt/

==External links==
*  

 
 
 
 
 
 