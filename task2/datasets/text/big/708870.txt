Don Juan DeMarco
 
{{Infobox film
| name           = Don Juan DeMarco
| image          = Don_juan_demarco.jpg
| caption        = Theatrical release poster
| director       = Jeremy Leven
| producer       = Francis Ford Coppola Michael De Luca Fred Fuchs
| writer         = Lord Byron   Jeremy Leven
| starring       = Marlon Brando Johnny Depp Faye Dunaway
| music          = Michael Kamen
| cinematography = Ralf Bode Tony Gibbs
| studio         = American Zoetrope
| distributor    = New Line Cinema
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English Spanish
| budget         = $25 million
| gross          = $68,792,531
}} romantic comedy-drama film starring Johnny Depp as John Arnold DeMarco, a man who believes himself to be Don Juan, the greatest lover in the world. Clad in a cape and domino mask, DeMarco undergoes psychiatric treatment with Marlon Brandos character, Dr. Jack Mickler, to cure him of his apparent delusion. But the psychiatric sessions have an unexpected effect on the psychiatric staff, some of whom find themselves inspired by DeMarcos delusion; the most profoundly affected is Dr. Mickler himself, who rekindles the romance in his complacent marriage.
 version of the legend.
 Ed Wood while the films theme song, "Have You Ever Really Loved a Woman?", co-written and performed by Bryan Adams, was nominated for the Academy Award for Best Song|Oscar, Grammy Award for Best Song Written for Visual Media|Grammy, and Golden Globe Award for Best Original Song.

==Plot==
Psychiatrist Jack Mickler (Marlon Brando) dissuades a would-be suicide—a 21-year-old, costumed like Zorro and claiming to be Don Juan (Johnny Depp), who is then held for a ten-day review in a mental institution. Mickler, who is about to retire, insists on doing the evaluation and conducts it without medicating the youth. "Don Juan" tells his story—born in Mexico, the death of his father, a year in a harem, and finding true love (and being rejected) on a remote island. Listening enlivens Micklers relationship with his own wife, Marilyn (Faye Dunaway). As the ten days tick down and pressure mounts on Mickler to support the youths indefinite confinement, finding reality within the romantic imagination becomes Jacks last professional challenge.

==Cast==
* Marlon Brando as Dr. Jack Mickler
* Johnny Depp as John Arnold DeMarco/Don Juan
* Faye Dunaway as Marilyn Mickler
* Selena as ranchera singer
* Géraldine Pailhas as Doña Ana
* Franc Luz as Don Antonio
* Bob Dishy as Dr. Paul Showalter
* Rachel Ticotin as Doña Inez
* Talisa Soto as Doña Julia
* Tommy Tiny Lister, Jr.|Tiny Lister, Jr. as Rocco Compton
* Richard C. Sarafian as Detective Sy Tobias
* Tresa Hughes as Grandma DeMarco
* Stephen Singer as Dr. Bill Dunmore
* Marita Geraghty as woman in restaurant scene

==Music== Jose Hernandez Oscar for Best Original Song at the 68th Academy Awards, but lost to "Colors of the Wind" from Pocahontas (1995 film)|Pocahontas. 

Selena recorded other songs for the soundtrack, including El Toro Relajo. The score was composed, orchestrated, and conducted by Michael Kamen and was performed by the London Metropolitan Orchestra.

==Release==

===Critical reception===
The film currently holds 74% on review aggregator Rotten Tomatoes, averaging a 6 out of 10 rating. The film also currently holds a 6.8 rating on IMDB.  

===Box office=== Bad Boys Tommy Boy. 

==See also==
 
* The Brave, a film which Depp directed and in which he again acted alongside Brando
* Zorro

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 