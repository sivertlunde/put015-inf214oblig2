Legend of Dinosaurs & Monster Birds
 
 
{{Infobox film
  | name = Legend of Dinosaurs and Monster Birds
| film name = {{Film name| kanji = 恐竜・怪鳥の伝説
| romaji = Kyōryū Kaichō no Densetsu}}
  | image = 
  | caption = 
  | director = Junji Kurata
  | producer = Keiichi Hashimoto 
  | writer = Masaru Igami Isao Matsumoto Ichirô Ôtsu
  | starring = Tsunehiko Watase Nobiko Sawa Shotaro Hayashi Tomoko Kiyoshima Fuyukichi Maki
  | music = Masao Yagi
  | cinematography = Shigeru Akatsuka 
  | editing =
  | distributor = Toei Company
  | released =   
  | runtime = 92 minutes
  | language = Japanese
  | budget =
}}

  (1977 in film|1977) is a Japanese science fiction film, released by Toei Company.

==Plot==
The year is 1977. A young woman wanders barefoot in the lush Aokigahara (青木ヶ原), also known as the Sea of Trees (樹海 Jukai) region of Mt. Fuji, and suddenly falls into an underground cavern. When she comes to her senses, she discovers that she is in an icy cave full of large eggs. To her horror, one of the eggs begins hatching, revealing a large yellow eye within. She goes into hysterics, runs for her life, and is eventually discovered by a construction crew. Though she goes into a coma, the girl apparently managed to babble about what she saw to a reporter.

Her story airs on a televised news report that is seen by Takashi Ashizawa, an employee of the Universal Stone Company. Upon hearing of the report of a fossilized egg, Takashi skips his plane trip to Mexico and heads to his office. He packs his gear, leaves his boss in the lurch, and heads off to Mt. Fuji to get a look-see at the fossilized dinosaur egg. When he arrives at the small village bordering Fujis Saiko Lake, Takashi immediately heads into the heavily forested Jukai. A sudden earthquake appears and is knocked out. He later awakens in his fathers old cabin near Saiko Lake, and discovers that he was rescued by Shohei Muku, an old friend of the family.

As the two converse, it is revealed that Takashi is intent on discovering, and making a profit from, any and all fossils he finds. Shohei is not keen on the idea and refuses to help Takashi seek out the stack of fossil eggs. (Essentially, Shohei believes that looking for fossils to make a profit is inherently wrong, and he wants no part of it.) Takashi decides to get back to fossil-hunting and heads toward the Jukai once again. As hes cruising through the nearby village, he sees Akiko and Junko, and slams on his brakes. Moments later, hes having a tender moment with Akiko in her Winnebago and it seems like Mr. Ashizawa is about to get a little action. The mood is quickly ruined however, by a randomly placed box full of slimy eels.

Other bizarre things start happening around the Saiko Lake community. A young couple in a paddle boat disappear without a trace, an injured diver is pulled from the lake, and livestock begin to mysteriously vanish. Takashi begins developing a theory that perhaps a dinosaur is alive and well in Saiko lake. His theory gains a little more momentum after he rescues Junko on a foggy afternoon. While chasing her dog Kuma down a dirt road, ends up falling in a large puddle of blood with a headless horse-corpse lying nearby.

She begins to scream for help, Takashi just happened to be in the neighborhood. He brings Junko back to Akikos RV, and waits with the two girls until nightfall. Eventually two local schmucks happen by and tell the confused trio that they must have imagined the headless horse. Takashi is baffled by this and decides to go see for himself. He finds the exact spot and begins probing around with a flashlight. Takashi doesnt see the horse anywhere, but he does discover some strange tracks in the mud and photographs them. His photo shoot is instantly interrupted after some blood drips onto the back of his neck. He quickly points his flashlight up and is shocked to see that the headless horses remains are lodged in the branches above.

The following day, Takashi sits in his fathers cabin and develops a possible theory as to what type of creature could bite off a horses head, and then place the equines remains in a tree for safe-keeping. He decides that the creature must be a living Plesiosaurus and shares his minimal proof and hypothesis with a very skeptical Shohei. In the meantime, the annual Dragon Festival is being held at Saiko Lake with the highlight being a country folk band performing on a floating stage. As the band strikes up a cheerful tune, the crowd begins clapping along. Everyone enjoys the occasion until a large, dark-colored ominous object is spotted in the water and the attendees flee in terror..

The object rams the stage, causing it to break apart, and several band members tumble into the water. The confusion gets the attention of Takashi and Shohei, so they hightail it to the Dragon Festival to see what all the hoopla is about. Its right at this time that Jiro and his friends make their move. Jiro hops into a boat and points to the center of the lake, exclaiming that a monster is heading towards shore. Everyone begins to panic and rush back to dry land, except for Akiko and Junko. They hop into a small boat and begin photographing the lopsided fin that is slowly moving through the water. Using her zoom lens, Akiko discovers that two men are pushing the fin through the water.

Once the crowd realizes that the fin is just a lousy stunt, they all get back into clapping mode. Seeing that his prank has failed, Jiro rushes off to meet his two pals, Susumu and Hiroshi. He arrives at the rendezvous spot in time to see his two buddies swimming to shore with the fake fin. They take a short break, ditch the fake fin, then hop into a small raft and paddle towards Jiro. Susumu and Hiroshi make it about halfway before they are large tail rises out of the water and knocks them out of the raft, and both men are pulled underwater. A horrified Jiro watches, from the relative safety of dry land, as the true monster rises its head from the bloodied water with one of his companions sticking out of its jaws.

Jiro rushes into town, charges into the mayors office, and begins rambling about what he just saw. Everyone thinks he is making it up and they all try to ignore him and/or chase him off. Luckily for Jiro, a foreign news correspondent named Harold Tucker shows up. Mr. Tucker has photographed the monster in Saiko Lake, and assures the mayor that "Nessie is in Lake Sai! This is super big news!"

Elsewhere in the Saiko Lake area, the Plesiosaur heads to a summer camp. The creature peeks in on a woman getting dressed, then smashes its face through the roof to snack on her. Then the Plesiosaurus manages to get back into the lake, in time to feast upon Junko. As the Plesiosaur actually seems to be toying with its victim. It plucks her off her raft and dangles her over the water before releasing her. After Junko plummets into the lake, she tries to swim to safety, but she soon finds that there is no escape from her prehistoric attacker.

Akiko comes back up on the raft but she is at first perplexed as to why Junko is missing, but she soon has a good laugh when she sees a hand grasping for aid at the far end of the raft. Thinking that Junko merely fell in, Akiko grabs her friends hand, and with a great heave, slings the Junkos upper torso into the rubber craft. Junkos death finally gets the ball rolling, and soon a local chapter of the JSDF is combing Saiko Lake with the latest sonar and radar technology. The search continues for three days, and surprisingly, no dinosaur is discovered. The search is called off, and a conference is held.

It is at this conference, we also learn that Takashis father, Bunkichi, had a theory that if dinosaurs were ever to walk the earth again, that would mean a cataclysmic event was about to occur. To prove this point, a scientist that monitors earthquakes in the region claims that something very big is on the horizon: the eruption of Mt. Fuji! The following day, Takashi decides to go looking for the Plesiosaur. To make sure Akiko doesnt follow, he attempts to empty the air out of her scuba tanks.

When Akiko tries to stop Takashi, he slaps her around a bit, then makes a strange confession. He is not seeking out the dinosaur for money, or to finally prove his fathers crazy theories, but to see it and "burn the memory into his mind forever." Apparently this is a good enough reason to risk his life, so Akiko sees Takashi off on his scuba run with no further complaints. But wouldnt you know it, the local officials have decided to drop depth charges into the lake to see if they can scare the lakes unwelcome denizen up to the surface. Akiko rushes back to her RV and puts on her scuba gear, then heads back to the lake in time to save her shell shocked lover. But instead of heading back to the safety of shore, the adventurous duo continue the search for the Plesiosaurus.

They eventually discover an underwater cavern and decide to venture inside after a disembodied head floats by. Takashi and Akiko swim on through and find that the cave leads to the egg chamber from the beginning of the film. While this would seem like the find of a lifetime, Takashis excitement is marred by the discovery of Shohei Mukus mutilated remains. And how did Shohei meet such a messy end? Well an unnamed gentleman with a random theory that a Rhamphorhynchus could also come out of suspended animation, hired Shohei as a guide. They ventured into the cave, and were quickly mauled by a gigantic claw that burst out of an egg. 

The winged terror descends from the skies above Saiko Lake and dive-bombs civil defense soldiers and helpless civilians that are crowded around a large stockpile of depth charges. The Rhamphorhynchus causes a bit of collateral damage before being shot at by the panic-stricken soldiers below. The assault on the scurrying humans ends after one unlucky soldier fires his weapon into a depth charge. The resulting explosion causes a chain reaction and everyone around the explosive barrels is reduced to ash.

The Rhamphorhynchus flies off in search for more prey. Elsewhere on Mt. Fuji, Takashi and Akiko have exited the accursed ice cavern and run into the Plesiosaur. They retreat back into the cave and put a row of stalactites between themselves and the drooling maw of the Plesiosaurus. All seems lost until a strange sound outside of the cave distracts the hungry Plesiosaur. As the long-necked beast pulls its head out of the cave entrance, Akiko and Takashi attempt to escape, only to find themselves trapped between two warring monsters. Just then, Mt Fuji begins to erupt. As the Monsters battle, they are thrown into a chasm where they apparently die. The movie ends with Takashi reaching out for Akikos hand while shes hanging on to a tree during the lava flow.

==Trivia==
* Despite its title, neither of the creatures in this film are actually classified as dinosaurs. The Plesiosaur is actually classified as a marine reptile, while the rhamphornycus (then-classified as a dinosaur along with other Pterosaurs at the time of the films production) is classified as a Pterosaur (flying reptile).

* At the time of its release, this film itself was the most expensive feature ever produced by Toei.

* Stock footage of the movie was briefly shown in episode 25 of Dai Sentai Goggle-V, a television series part of Toeis long running Super Sentai franchise.

 
 
 
 
 
 
 
 
 
 