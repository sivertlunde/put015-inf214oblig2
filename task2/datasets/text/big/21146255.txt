The Evil Forest
{{Infobox film
| name           = The Evil Forest
| image          = 
| caption        = 
| director       = Daniel Mangrané Carlos Serrano de Osma
| producer       = Daniel Mangrané
| writer         = Daniel Mangrané Carlos Serrano de Osma Francisco Naranjo José Antonio Pérez Torreblanca
| starring       = Gustavo Rojo
| music          = 
| cinematography = Cecilio Paniagua
| editing        = Antonio Cánovas
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

The Evil Forest ( ) is a 1951 Spanish drama film directed by Daniel Mangrané. It was entered into the 1952 Cannes Film Festival.     

==Cast==
* Gustavo Rojo - Parsifal
* Ludmilla Tchérina - Kundry
* Félix de Pomés - Klingsor
* Jesús Varela - The Dwarf
* Ángel Jordán - Roderico
* José Luis Hernández (actor)|José Luis Hernández - Parsifal - as a young boy
* Teresa Planell - The Old Woman
* Alfonso Estela - Anfortas
* Carlo Tamberlani - Gurnemancio
* José Bruguera - Titurel
* Ricardo Fusté - Alisan
* Nuria Alfonso - Rage
* Tony Domenech - Gluttony
* Rosa Monero - Sloth
* Elena Montevar - Envy
* Carmen Zaro - Greed
* Carmen de Lirio - Pride
* Josefina Ramos - Lust

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 