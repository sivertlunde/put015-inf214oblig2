Witness (1985 film)
 
{{Infobox film
| name           = Witness
| image          = Witness_movie.jpg
| caption        = Original poster
| alt            = 
| director       = Peter Weir
| producer       = Edward S. Feldman
| screenplay     = Earl W. Wallace William Kelley
| story          = William Kelley Pamela Wallace Earl W. Wallace
| starring = {{Plainlist|
* Harrison Ford
}} 
| music          = Maurice Jarre
| cinematography = John Seale
| editing        = Thom Noble
| studio         = 
| distributor    = Paramount Pictures
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English, German
| budget         = $12 million
| gross          = $68.7 million   
}}

Witness is a 1985 American thriller film directed by Peter Weir and starring Harrison Ford and Kelly McGillis. The screenplay by William Kelley, Pamela Wallace, and Earl W. Wallace focuses on a detective protecting a young Amish boy who becomes a target after he witnesses a murder in Philadelphia.
 Best Original Best Film Edgar Award for Best Motion Picture Screenplay presented by the Mystery Writers of America.

==Plot==
Rachel Lapp (McGillis), a young Amish widow, and her 8-year-old son Samuel (Haas) are traveling by train to visit Rachels sister. Samuel is amazed by the sights in the outside world, but at 30th Street Station in Philadelphia, he witnesses two men attack and murder a third. Detective John Book (Ford) is assigned to the case and he and his partner, Sergeant Elton Carter (Jennings), question Samuel. Samuel is unable to identify the perpetrator from mug shots or a police lineup, but notices a newspaper clipping with a picture of narcotics officer James McFee (Glover) and recognizes him as one of the killers. John remembers that McFee was previously responsible for a drug raid on expensive chemicals used to make amphetamines, but the evidence had mysteriously disappeared.

John confides his suspicions to his superior officer, Chief Paul Schaeffer (Sommer), who advises John to keep the case secret so they can work out how to move forward. While in parking garage, John is ambushed by McFee and badly wounded before McFee escapes. Since only Schaeffer knew of McFees involvement, John realizes Schaeffer is also corrupt and had warned McFee.

John calls Carter and orders him to remove the Lapp file from the records. He then hides his car and uses his sisters car to return Rachel and Samuel to Lancaster County. After the Lappss safe arrival, John passes out in the vehicle in front of their farm.
 shunned by the community if she continues on this path.

Johns relationship with the Amish community grows as they learn he is skilled at carpentry. He is invited to participate in a barn raising for a newly married couple and gains Hochleitners respect. However, the attraction between John and Rachel is evident and clearly concerns Eli and others, especially when she serves John first at a meal. That night, John comes upon Rachel as she bathes, and she stands half-naked before him, but he walks away, explaining the next morning that if they had made love the night before he would have to stay or she would have to leave.

John goes into town with Eli to use the phone. John is informed via a call from a payphone that Carter has been killed; he deduces that it was Schaeffer and McFee, who intensify their efforts to find him and are joined by a third corrupt officer, "Fergie" Ferguson (MacInnes), who helped McFee commit the murder at the station. In town, Hochleitner and the other Amish men are harassed by locals. Breaking with the Amish tradition of nonviolence, John retaliates. The fight is reported to the local police, and the news reaches Schaeffer.

Because of the publicity the fight has gotten, John knows he must leave. Rachel is upset at the news. When she is alone she removes her bonnet and goes to John, and they passionately kiss.
 suffocates him under tons of corn. He retrieves Fergies shotgun and kills McFee. Schaeffer then forces Rachel and Eli out of the house at gunpoint; Eli signals to Samuel (who had returned unseen) to ring the warning bell. Although Schaeffer forces John to surrender, the loud clanging summons other Amish. With so many witnesses, Schaeffer realizes he cant escape, and gives up. The local police arrive and arrest Schaeffer.

As John prepares to leave, he says goodbye to Samuel, Rachel, and Eli in turn, who wish him well "out there among the English." Driving away, he passes Hochleitner and exchanges a wave of farewell.

==Cast==
* Harrison Ford as Det. John Book
* Kelly McGillis as Rachel Lapp
* Josef Sommer as Chief Paul Schaeffer
* Lukas Haas as Samuel Lapp
* Jan Rubeš as Eli Lapp
* Alexander Godunov as Daniel Hochleitner
* Danny Glover as Lt. James McFee
* Brent Jennings as Sgt. Elton Carter
* Patti LuPone as Elaine
* Angus MacInnes as Fergie
* Frederick Rolf as Stoltzfus
* Viggo Mortensen as Moses Hochleitner

==Production==
Producer Edward S. Feldman, who was in a "first-look" development deal with 20th Century Fox at the time, first received the screenplay for Witness in 1983. Originally entitled Called Home (which is the Amish term for death), it ran 182 pages long, the equivalent of three hours of screen time. The script, which had been circulating in Hollywood for several years, had been inspired by an episode of Gunsmoke William Kelley and Earl W. Wallace had written in the 1970s.   

Feldman liked the concept, but felt too much of the script was devoted to Amish traditions, diluting the thriller aspects of the story. He offered Kelley and Wallace $25,000 for a one-year option and one rewrite, and an additional $225,000 if the film actually was made. They submitted the revised screenplay in less than six weeks, and Feldman delivered it to Fox. Joe Wizan, the studios head of production, rejected it with the statement that Fox didnt make "rural movies". 
 Phil Gersh, who contacted the producer four days later and advised him his client was willing to commit to the film. Certain the attachment of a major star would change Wizans mind, Feldman approached him once again, but Wizan insisted that as much as the studio liked Ford, they still werent interested in making a "rural movie." 

Feldman sent the screenplay to numerous studios and was rejected by all of them, until Paramount Pictures finally expressed interest. Feldmans first choice of director was Peter Weir, but he was involved in pre-production work for The Mosquito Coast and passed on the project. John Badham dismissed it as "just another cop movie", and others Feldman approached either were committed to other projects or had no interest. Then, as financial backing for The Mosquito Coast fell through, Weir became free to direct Witness, which was his first American film. It was imperative filming start immediately, because a Directors Guild of America strike was looming on the horizon. 
 Strasburg and extras actually were Mennonites. Halfway through filming, the title was changed from Called Home to Witness at the behest of Paramounts marketing department, which felt the original title posed too much of a promotional challenge. Principal photography was completed three days before the scheduled DGA strike, which ultimately failed to materialize. 
 Pennsylvania Germans, High German, Pennsylvania Germans in general.

==Reception==
===Critical response   ===
Witness was generally well received by critics and earned eight Academy Award nominations (including Weirs first and Fords sole nomination to date).

Roger Ebert of the Chicago Sun-Times rated the film four out of four stars, calling it "first of all, an electrifying and poignant love story. Then it is a movie about the choices we make in life and the choices that other people make for us. Only then is it a thriller—one that Alfred Hitchcock would have been proud to make." He concluded, "We have lately been getting so many pallid, bloodless little movies—mostly recycled teenage exploitation films made by ambitious young stylists without a thought in their heads—that Witness arrives like a fresh new day. It is a movie about adults, whose lives have dignity and whose choices matter to them. And it is also one hell of a thriller." 

Vincent Canby of the New York Times said of the film, "Its not really awful, but its not much fun. Its pretty to look at and it contains a number of good performances, but there is something exhausting about its neat balancing of opposing manners and values&nbsp;... One might be made to care about all this if the direction by the talented Australian film maker, Peter Weir&nbsp;... were less perfunctory and if the screenplay&nbsp;... did not seem so strangely familiar. One follows Witness as if touring ones old hometown, guided by an outsider who refuses to believe that one knows the territory better than he does. Theres not a character, an event or a plot twist that one hasnt anticipated long before its arrival, which gives one the feeling of waiting around for people who are always late." 

Variety (magazine)|Variety said the film was "at times a gentle, affecting story of star-crossed lovers limited within the fascinating Amish community. Too often, however, this fragile romance is crushed by a thoroughly absurd shoot-em-up, like ketchup poured over a delicate Pennsylvania Dutch dinner." 

Time Out New York observed, "Powerful, assured, full of beautiful imagery and thankfully devoid of easy moralising, it also offers a performance of surprising skill and sensitivity from Ford." 

Halliwells Film Guide described the film as "one of those lucky movies which works out well on all counts and shows that there are still craftsmen lurking in Hollywood." 

Radio Times called the film "partly a love story and partly a thriller, but mainly a study of cultural collision&nbsp;– its as if the world of Dirty Harry had suddenly stumbled into a canvas by Pieter Bruegel the Elder|Brueghel." It added, " ts Weirs delicacy of touch that impresses the most. He ably juggles the various elements of the story and makes the violence seem even more shocking when its played out on the fields of Amish denial." 

The film was screened out of competition at the 1985 Cannes Film Festival.   

===Controversy===
The film was not well received by the Amish communities where it was filmed. A statement released by a law firm associated with the Amish claimed that their portrayal in the movie was not accurate. The National Committee For Amish Religious Freedom called for a boycott of the movie soon after its release, citing fears that these communities were being "overrun by tourists" as a result of the popularity of the movie, and worried that "the crowding, souvenir-hunting, photographing and trespassing on Amish farmsteads will increase." After the movie was completed, Pennsylvania governor Dick Thornburgh agreed not to promote Amish communities as future film sites. A similar concern was voiced within the movie itself, where Rachel tells a recovering John that tourists often consider her fellow Amish something to stare at, with some even being so rude as to trespass on their private property. 

===Box office===
The film opened in 876 theaters in the United States on February 8, 1985 and grossed $4,539,990 in its opening weekend, ranking No. 2 behind Beverly Hills Cop. It remained at No. 2 for the next three weeks and finally topped the charts in its fifth week of release. It eventually earned $68,706,993 in the United States. 

===Awards and nominations  === Academy Award for Best Original Screenplay (winner) Academy Award for Best Film Editing (winner)
* Academy Award for Best Picture (nominee)
* Academy Award for Best Director (nominee)
* Academy Award for Best Actor (Harrison Ford, nominee)
* Academy Award for Best Art Direction (Stan Jolley and John H. Anderson, nominees)
* Academy Award for Best Cinematography (John Seale, nominee) Academy Award for Best Original Score (Maurice Jarre, nominee)
* BAFTA Award for Best Film (nominee)
* BAFTA Award for Best Original Screenplay (nominee)
* BAFTA Award for Best Actor in a Leading Role (Harrison Ford, nominee)
* BAFTA Award for Best Actress in a Leading Role (Kelly McGillis, nominee)
* BAFTA Award for Best Film Music (Maurice Jarre, winner)
* BAFTA Award for Best Cinematography (John Seale, nominee)
* BAFTA Award for Best Editing (nominee)
* Golden Globe Award for Best Motion Picture – Drama (nominee)
* Golden Globe Award for Best Director (nominee)
* Golden Globe Award for Best Screenplay (nominee)
* Golden Globe Award for Best Actor – Motion Picture Drama (Harrison Ford, nominee)
* Golden Globe Award for Best Supporting Actress – Motion Picture (Kelly McGillis, nominee)
* Golden Globe Award for Best Original Score (Maurice Jarre, nominee)
* Kansas City Film Critics Circle Award for Best Film (winner)
* Kansas City Film Critics Circle Award for Best Actor (Harrison Ford, winner)
* Writers Guild of America Award for Best Original Screenplay (winner)
* Directors Guild of America Award for Outstanding Directing – Feature Film (nominee)
* Grammy Award for Best Score Soundtrack Album for a Motion Picture, Television or Other Visual Media (nominee)
* American Cinema Editors Award for Best Edited Feature Film (winner)
* Australian Cinematographers Society Award for Cinematographer of the Year (winner)
* British Society of Cinematographers Award for Best Cinematography (nominee)

===American Film Institute===

* AFIs 100 Years...100 Movies - Nominated
* AFIs 100 Years...100 Thrills - Nominated
* AFIs 100 Years...100 Passions - #82
* AFIs 100 Years...100 Heroes and Villains
** John Book - Nominated Hero
* AFIs 100 Years...100 Songs
** Wonderful World - Nominated
* AFIs 100 Years...100 Movies (10th Anniversary Edition) - Nominated

==Home media  ==
The film was released on Region 1 DVD on June 29, 1999. It is in letterboxed (anamorphic|non-anamorphic) widescreen format with audio tracks in English and French. The sole bonus feature is an interview with director Peter Weir.

The film was released on Region 2 DVD on October 2, 2000. As with the Region 1 release, it is in letterboxed non-anamorphic widescreen format. The audio tracks are in English, French, German, Italian, Czech, Spanish, and Polish and subtitles in English, Spanish, German, French, Italian, Portuguese, Swedish, Turkish, Danish, Hungarian, Dutch, Finnish, and Croatian. Bonus features include an interview with Weir and the original trailer.

A Special Collectors Edition was released on Region 1 DVD on August 23, 2005. It is now in anamorphic widescreen format with audio tracks in English and French and subtitles in English and Spanish. Bonus features include the five-part documentary Between Two Worlds: The Making of Witness (63 mins), a deleted scene, the original theatrical trailer, and three television ads. The Special Collectors Edition was released on Region 2 DVD on February 19, 2007, with different cover art and more extensive language and audio/subtitle options for European countries.

==References==
 

==External links==
 
*  
*  
*   at  

 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 