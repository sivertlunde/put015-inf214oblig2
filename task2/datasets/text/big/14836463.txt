Pyaasa Sawan
{{Infobox film
| name           = Pyaasa Sawan 
| image          = 
| caption        =
| director       = Dasari Narayana Rao
| producer       = Prasan Kapoor
| writer         = 
| starring       = Jeetendra Reena Roy Vinod Mehra Moushmi Chatterji
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        =
| distributor    = 
| released       = 28 August 1981
| runtime        = approx 180 min 
| country        = India
| language       = Hindi
| Budget         = 
| preceded_by    =
| followed_by    =
| awards         =
| Gross          = 
}}
Pyaasa Sawan is 1981 Hindi language movie directed by Dasari Narayana Rao and starring Jeetendra, Reena Roy,  Vinod Mehra, Moushmi Chatterji, Madan Puri, Aruna Irani, Deven Verma, Ashalata Wabgaonkar. 

==Cast==
* Jeetendra - Chandrakant/Ravi
* Reena Roy - Manorama
* Vinod Mehra - Manoramas father
* Moushmi Chatterji - Shanti
* Madan Puri - Prabhudas
* Ashalata Wabgaonkar 

==Plot==
Chandrakant is looking for job. Prabhudas mistakenly thinks that Chandrakant is son of a wealthy man. Prabhudas wants Chandrakant to marry his daughter Shanti. By the time Prabhudas learns the truth, Chandrakant and Shanti have fallen in love. They marry but go through a lot of troubles.

Chandrakant builds a small empire by hard labour. Shanti gives birth to their son, Ravi. Shanti passes away after one year. Chandrakant always regrets that he could not give much time to Shanti. Ravi grows up and the same story of Chandrakant-Shanti is about to repeat between Ravi and Manorama.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Tera Saath Hai To"
| Lata Mangeshkar
|-
| 2
| "Main Wahan Hoon"
| Kishore Kumar
|-
| 3
| "Megha Re Megha Re"
| Suresh Wadkar, Lata Mangeshkar
|-
| 4
| "O Meri Chhammak Chhallo"
| Kishore Kumar, Asha Bhosle
|-
| 5
| "Tera Saath Hai To"
| Kamlesh Avasthi 
|-
| 6
| "In Haseen Vadiyon Se"
| Suresh Wadkar, Lata Mangeshkar
|}

==References==
 

== External links ==
*  

r

 
 
 
 
 


 