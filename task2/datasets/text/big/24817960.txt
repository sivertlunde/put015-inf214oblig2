Double Take (1998 film)
 
 
{{Infobox film
| name           = Double Take
| image          =
| image_size     =
| caption        =
| director       = Mark L. Lester
| writer         = Edward and Ralph Rugoff
| narrator       =
| starring       = Craig Sheffer
| music          = Paul Zaza
| cinematography = Peter Benison
| editing        = Stephen Fanfara
| distributor    =
| released       = 1998
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
Double Take is a 1998 thriller film directed by Mark L. Lester. It follows a writer who believes he helped wrongly convict a man in an assassination, pulling him into a world of espionage and murder.

==Cast==
* Craig Sheffer as Connor McEwan
* Costas Mandylor as Hector
* Brigitte Bako as Nikki Capelli
* Dan Lett as Detective Hardaway
* Torri Higginson as Peggy Maurice Godin as Fritz
* Peter Keleghan as Robert Mead
* Frank Pellegrino as Frankie

==External links==
* 

 

 
 
 
 
 
 


 