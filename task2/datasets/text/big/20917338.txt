Fantastic Animation Festival
{{Infobox film
| image=
| image size=
| caption=
| director=Dean A. Berko Christopher Padilla
| producer=
| writer=
| narrator= Paul Frees (Opening Narration) Spike Milligan (Moonshadow (song)|Moonshadow)
| starring=
| music= Richard Audd
| cinematography =
| editing=
| studio=Voyage Productions
| distributor=
| released= November 25, 1977
| runtime=91 min. 80 min. (USA TV version)
| country=   USA
| language=English
| budget=
| gross=
| preceded by=
| followed by=
}} package film Computer Animation Festival and Spike and Mikes Sick and Twisted Festival of Animation.

Included in its 16 segments was the first national appearance of Will Vintons Claymation (Closed Mondays), Bambi Meets Godzilla, and a Superman cartoon from the 1940s.

==Segments==
(The following are in alphabetical, rather than running order.)
*Introduction; voice-over ("Welcome to the world of animation") by Paul Frees Paul Driessen
*"Bambi Meets Godzilla"; by Marv Newland
*"Closed Mondays"; Claymation by Will Vinton (Academy Award Winner for Best Animated Short Film)
*"Cosmic Cartoon"; Animated and directed by Eric Ladd and Steven Lisberger One of These Days"
*"Icarus"; French clay animation (by Mihail Badica)
*"Kick Me" (by Robert Swarthe)
*"The Last Cartoon Man"; by Derek Lamb & Jeffrey Hale
*"Light" (by Jordan Belson)
*"Mirror People"; by Kathy Rose
*"Moonshadow (song)|Moonshadow"; Cat Stevens story of Teaser and the Firecat, narrated by Spike Milligan
*"Mountain Music"; Claymation by Will Vinton
*"Oiseau de Nuit" ("Nightbird"); by Bernard Palacios Room and Board" (by Randy Cartwright)
*"A Short History of the Wheel"; by Loren Bowie
*"Superman and The Mechanical Monsters"; a 1941 Fleischer Studios cartoon
*"Uncola"; a 1975 7Up commercial; by Robert Abel and Associates
*"Stranger"; a 1971 Levi Strauss Jeans commercial; by Snazelle Films, narrated by Ken Nordine 
All segments citation:   

==External links==
* 

==Citations==
 

 
 


 