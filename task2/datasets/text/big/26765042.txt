The Dawn Express
{{Infobox film
| name           = The Dawn Express
| image          = Poster of the movie The Dawn Express.jpg
| image_size     =
| caption        =
| director       = Albert Herman Arthur Alexander Max Alexander (producer) George M. Merrick (producer)
| writer         = Arthur St. Claire (writer)
| narrator       =
| starring       = See below
| music          =
| cinematography = Edward Linden
| editing        = Leete Renick Brown
| distributor    =
| released       = 1942
| runtime        = 54 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Dawn Express is a 1942 American film directed by Albert Herman. The film is also known as Dawn Express (American poster title).

==Plot Summary==

It is in the middle of World War II and Nazi Captain Gemmler is in need of a powerful chemical formula to improve the energy output of ordinary gasoline. In his quest for this formula he finds two candidates to help him with a solution: the chemist and playboy Tom Fielding and his co-worker Robert Norton, who is engaged to Toms sister Nancy. They are already involved in such a project for another employer, and Captain Gemmler decides to kidnap them for his own use, using his secret agents.  

The unsuspecting Tom goes to the nearby Alpine-style tavern one night, and meets a Polish refugee named Linda Pavlo. They strike up a conversation, and the following morning Tom is late for work. Robert tells him to stay away from unknown women he meets at the bar by chance like that - nothing good will come out of it. The president of the chemical company where they work, Franklyn Prescott, gets a visit from a government agent, James Curtis, and is told that there has been a double murder in another chemistry plant. Prescott isnt worried, but refers to his security plan: the plant is only working on one half of the precious formula, while another sister-plant on the East Coast is working on the other half. Curtis breaks the news to Prescott, that the East Coast plant has been compromised, and sabotaged by one of its own chemists. The chemist abandoned the plant with half of the formula, and has now been identified as working for the Nazis, under the name of Karl Schmidt.

In the night, when Tom and Robert returns to the same tavern as the night before, Curtis agents follow them there. Tom meets Linda again, and since she is a Nazi agent, he is lured into a room where Gemmler is waiting. Gemmler threatens to hurt both Toms mother and his sister Nancy if they dont play along and give him the formula. When Tom leaves the tavern that night, he is again followed by one of Curtis agents, but one of Gemmlers spies murders the agent. 

The following day Prescott and Curtis talk to Robert, explaining their theory that Tom, in an act of treason, has murdered the agent. Robert doesnt buy the theory, claiming Tom is innocent. What none of them are aware of is that Toms sister Nancy is eavesdropping on their conversation. After Prescott and Curtis has left, Tom and Robert talk about whats going on, and later that night he meets once more with Linda at the bar. This time she offers him $100,000 in exchange for the formula.   

After the meeting with Linda, Tom, Robert and Nancy team up to try to frame the Nazi spies and get rid of them once and for all. Tom goes to the chemical plant to retrieve the formula, and gives it tor Robert. He in turn brings the formula to his meeting with Linda and is escorted to Gemmlers office. Suddenly Tom appears at Gemmlers office and starts arguing over the payment. This results in Gemmler withholding the payment entirely. Robert is knocked out cold by Gemmlers goons, and the spies bring Tom with them to the airport, where Gemmlers contact, Karl Schmidt is about to arrive.  

The government agents have followed in the heels of Tom and his party, but are too late to catch Gemmler before he leaves for the airport. they do however arrest Linda and a few other spies lingering by Gemmlers office. At the airport Gemmler forces Tom into the airplane, and the plane leaves the ground. Schmidt decides to test the formula while they are in mid air. Tom, who knows that the future of his country is at stake, makes a deliberate mistake in mixing the chemical ingredients. The plane explodes, leaving no survivors, and Tom sacrifices himself to prevent the formula from reaching its destination. 

== Cast == Michael Whalen as Robert Norton
*Anne Nagel as Nancy Fielding
*William Bakewell as Tom Fielding
*Constance Worth as Linda Pavlo
*Hans Heinrich von Twardowski as Capt. Gemmler
*Jack Mulhall as Chief Agent James Curtis
*George Pembroke as Prof. Karl Schmidt
*Kenneth Harlan as Agent Brown
*Robert Frazer as John Oliver
*Hans von Morhart as Heinrich, kidnapper
*Michael Vallon as Argus, blind spy
*Willy Castello as Otto - Tavern spy

== External links ==
* 
* 

==References==
 

 
 
 
 
 
 
 