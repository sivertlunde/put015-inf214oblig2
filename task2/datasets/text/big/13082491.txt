Jaadugar
{{Infobox film
| name           = Jaadugar
| image          = Jaadugar.jpg
| image_size     = 
| caption        = 
| director       = Prakash Mehra
| producer       = Prakash Mehra
| writer         = 
| narrator       = 
| starring       = Amitabh Bachchan Amrita Singh Jaya Prada Amrish Puri Aditya Pancholi Ram Sethi
| music          = Kalyanji-Anandji
| cinematography = 
| editing        = 
| studio         = 
| distributor    =Eros Entertainment Prakash Mehra Productions
| released       = 25 August 1989
| runtime        = 173 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 Hera Pheri, Muqaddar Ka Sikander, Laawaris, Namak Halal and Sharaabi). This was last collaboration between Prakash Mehra and Amitabh Bachchan.

==Plot==

Returned American Shankar Narayan is baffled to witness his former businessman and jailbird dad posing as a soothsayer by the name of Mahaprabhu Janak Sagar Jagat Narayan, in a small town called Dharampur. He comes to know that his dad has learned a few magicians tricks from a fellow inmate, and is able to fool the entire township with his "divine" powers. Shankar enlists the assistance of noted magician Goga, and brings him to Dharampur. Goga challenges the Mahaprabhu; he is able to dethrone him, ousts him from his temple, and takes over. His task over, Shankar asks Goga to leave; but power-hungry Goga, who now calls himself Gogeshwar, refuses to let go of his new-found position.

Actually Goga is not power-hungry as he seems to be. Mahaprabhu, however, refuses to give up and uses all deceitful methods to expose the fact that Goga is just a mortal, and it is he who is actually divine. He tries to poison the Prasad that Goga gives to his followers but they all escape. In the end, Goga completely exposes Mahaprabhu in front of the whole public along with the fact that he himself is a common man who posed as a Divine man just to match and teach Mahaprabhu. He then returns to his best role&nbsp;– being the peoples magician.

==Production notes==
This was the final film of Prakash Mehra with Amitabh Bachchan, which, although it did not do well at the box-office, was praised for its unique story and great performances by the total cast. The film is also best remembered for its many confrontational scenes between Amitabh Bachchan and Amrish Puri.
Though it was not successful during its first release but later it became popular in video circuit and recovered its money by numerous re release.
Prakash Mehera production recovered the entire money by earning profit sharing  from the satellite rights later.

==Cast==

*Amitabh Bachchan ...  Goga 
*Amrita Singh ...  Mona
*Jaya Prada ...  Meena 
*Amrish Puri ...  Mahaprabhu Jagatsagar Chintamani
*Raza Murad ...  Rajbharti 
*Aditya Pancholi ...  Shankar Narayan Pran ...  Gajender
*Harbans Darshan M. Arora ...  Police Inspector (as Harbans Darshan) 
*Vikas Anand ...  Kanchan
*Bharat Bhushan ...  Gyaneshwar 
*Ram Sethi ...  Pyarelal
*Bob Christo ...  Bob 
*Leena Das ...  Florie 
*C.S. Dubey ...  Pujari 
*K.S. Ramesh ...  Himself (Magician)
*Gorilla ...  Bajrangi 
*Satyendra Kapoor ...  Police Commissioner 
*Johnny Lever ...  Nilkant 
*Ram Mohan ...  M.L.A. 
*Mukri ...  Nathulal 
*Babbanlal Yadav...  Gambler

==Soundtrack==
The Soundtrack composed by Kalyanji Anandji. Singers Kumar Sanu bollywood singing career second film 1989, after aandhiyaan 1987 but not complete film. 
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aaye Hain Duaen Dene" 
| Kumar Sanu, Jolly Mukherjee, Sadhana Sargam, Sapna Mukherjee
|-
| 2
| "Kya Samjhen Aap Ko"
| Sapna Mukherjee
|-
| 3
| "Main Jaadugar"
| Kumar Sanu, Jolly Mukherjee
|-
| 4
| "Naach Meri Radha"
| Kumar Sanu, Alka Yagnik
|-
| 5
| "Padosan Apni Murgi"
| Amitabh Bachchan
|}

==External links==
* 

 
 
 
 
 


 