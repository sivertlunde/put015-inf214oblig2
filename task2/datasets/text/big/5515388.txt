The Killer (2006 film)
{{Infobox film
| name           = The Killer
| image          = The_Killer_poster.jpg
| caption        = 
| director       = Hasnain Hyderabadwala   Raksha Mistry
| producer       = Mukesh Bhatt
| writer         =  Irfan Khan
| music          = Sajid-Wajid
| country        = India
| cinematography = Sanjay Malkar
| editing        = Sanjay Malkar
| distributor    = Vishesh Films
| released       =  
| runtime        = 120 minutes
| language       = Hindi
| budget         = 
| gross          = 
}}
 Michael Mann thriller, Collateral (film)|Collateral.

==Plot==
The story follows one serial killer who uses the help of innocent taxi drivers to murder his targets. When The Central Bureau of Investigation (CBI), India, has been made aware of a killer cab-driver in Malaysia who killed several people in a matter of hours, and then crashed his taxi and killed himself. The same thing happened again, this time in Hong Kong, several people were killed by another cab-driver, who also ended up being killed. Now, this time in Dubai, taxi driver Nikhil Joshi (Emraan Hashmi). Nikhil is an Indian living in Dubai for a good life, and is madly in love with a cabaret dancer named Ria (Nisha Kothari). He is willing to do anything for Ria, and so is she. However, life has other plans for him when one night, a suave businessman named Vikram (Irrfan Khan), hails his cab. 
 Zakir Hussain), who is at risk of being deported to India to face charges against him. As Nikhil helplessly becomes witness to one killing after another, he finds his life and dreams crumbling around him. Faced with the prospect of losing everything he has been working towards, he finally takes control of the steering wheel of his life. He stops dreaming and starts acting, as he begins to pit his wits against the assassin. 

Killing one after another, Nikhil seems to be the prime suspect, thats why Vikram plans to kill him in his taxi and flee, however Nikhil fights back, and therefore Vikram escapes and kidnaps his girlfriend Ria. Only way Ria could survive is if Nikhil would take the blame of the murders Vikram has committed. Will Nikhil blame himself for something he didnt do, or will he stand his ground and fight Vikram the killer himself?

==Similarities==
The films plot is almost entirely based on the 2004 Hollywood action-thriller film Collateral (film)|CollateralCollateral (film)|.
==Cast==
* Irrfan Khan as Vikram / Roopchand Swaroopchand Solanki 
* Emraan Hashmi as Nikhil Joshi
* Priyanka Kothari as Ria 
* Bharti Achrekar as  Zakir Hussain as Jabbar
* Avtar Gill as Nikhils boss
* Sanjay Batra as Police Officer

==Tracks== KK & Shreya Ghoshal. The music is composed by Sajid Wajid. The music was released on Saregama in May 2006.

{| class="wikitable"
|-
! Track No !! Song !! Singer !! Music Director
|- KK & Shreya Ghoshal || Sajid Wajid
|-
| 2 || O Sanam (Duet) || KK (singer)|KK& Shreya Ghoshal || Sajid Wajid
|-
| 3 || Hibbaki || Suzanne DMello, Hamza Faruqui & Earl || Sajid Wajid
|-
| 4 || Abhi Toh Main Jawan Hoon || Alisha Chinoy || Sajid Wajid
|-
| 5 || Yaar Piya || Sunidhi Chauhan || Sajid Wajid
|-
| 6 || Hibbaki (Remix) || Suzanne DMello, Hamza Faruqui & Earl || Sajid Wajid
|- KK & Shreya Ghoshal|| Sajid Wajid
|- KK || Sajid Wajid
|}

==External links==
* 

 
 
 
 
 