Aap Ki Parchhaiyan
{{Infobox film
| name           = Aap Ki Parchhaiyan
| image          = Aap Ki Parchhaiyan.jpg
| image_size     =
| caption        =
| director       = Mohan Kumar
| producer       = Mohan Kumar
| writer         = Mohan Kumar, Sarashar Sailani (dialogue)
| narrator       =
| starring       = Dharmendra Shashikala Om Prakash Supriya Choudhury
| music          = Madan Mohan
| cinematography = K. H. Kapadia
| editing        = Nand Kumar
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
}} 1964 Bollywood film. Produced and directed by  Mohan Kumar the film stars Dharmendra, Shashikala, Supriya Choudhury, Om Prakash and Manorama (Hindi actress)|Manorama. The films music is by Madan Mohan. A couple of songs Main nigahen tere chehre se hataaoon kaise by Mohd. Rafi and Agar mujhse mohabbat hai by Lata Mangeshkar are plus points of this family drama.

==Cast==
* Dharmendra        as  Chandramohan Chopra Channi
* Supriya Choudhury as	Asha
* Vijayalaxmi
* Shashikala        as	Rekha
* Suresh            as	Baldev Chopra
* Nazir Hussain     as	Dinanath Chopra
* Leela Chitnis     as  Mrs. Dinanath Chopra Mumtaz Begum  as	Rekhas Mother
* Om Prakash        as  Vilayti Ram
* Moppet Rajoo		
* Manorama          as  Mrs. Vilayti Ram
* Brahm Bhardwaj    as  Rekhas Father
* Khairati
* Madhu Apte		
* Santosh Kapoor		
* Sadhu Singh		
* Moolchand         as   Lala Khushiram
* Nazir Kashmiri		
* Sayyed
* Harbans Darshan M. Arora   as  Chaman Lal

== External links ==
*  

 
 
 

 