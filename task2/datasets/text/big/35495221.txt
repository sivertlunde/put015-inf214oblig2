World of Plenty
{{Infobox film
| name           = World of Plenty
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Paul Rotha
| producer       = Yvonne Fletcher
| writer         = Eric Knight Paul Rotha John Orr
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = William Alwyn
| cinematography = Wolfgang Suschitzky
| editing        = 
| studio         = Paul Rotha Productions
| distributor    = 
| released       =  
| runtime        = 43 mins
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}

World of Plenty is a 1943 British documentary film directed by Paul Rotha for the Ministries of Food and Agriculture. It discusses problems with, and possible improvements to, global food distribution.   

==Synopsis==
An opening narration explaining that the films purpose is to examine the "world strategy of food", in terms of its production, distribution and consumption. The film is then divided into three parts: "Food - As It Was", "Food - As It Is" and "Food - As It Might Be".

==References==
 

==External links==
*   at BFI Screenonline

 
 
 
 
 


 
 