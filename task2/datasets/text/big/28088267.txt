Operace Silver A
 
{{Infobox film
| name           = Operace Silver A
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Jiří Strach
| producer       = 
| writer         = Josef Konás Lucie Konasova
| narrator       = 
| starring       = Klára Issová
| music          = 
| cinematography = Martin Sec
| editing        = 
| studio         = 
| distributor    = 
| released       = 15 April 2007
| runtime        = 149 minutes
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech drama film directed by Jiří Strach. It was released in 2007.

==Cast==
* Klára Issová - Hana Kroupová
* Tatiana Vilhelmová - Tána Hladíková
* Jiří Dvořák - Alfréd Bartos
* Ivan Trojan - Frantisek Hladík
* David Švehlík - Václav Kroupa
* Miroslav Táborský - Arnost Kostál
* Aleš Háma - Josef Valcík
* Matěj Hádek - Jirí Potucek
* Marek Taclík - Hubert Freylach
* Viktor Preiss - Wilhelm Schultze
* Jan Vondráček - Gerhard Clages
* Jana Hlaváčová - Nohýnková
* Tatjana Medvecká - Andela
* Alois Švehlík - Kroupa
* Dana Syslová - Kroupová

==External links==
*  

 
 
 
 

 