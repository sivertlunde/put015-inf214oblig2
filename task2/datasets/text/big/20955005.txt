Silencing Mary
{{Infobox film
| name           = Silencing Mary
| image          = Silencing Mary.jpg
| image_size     =
| caption        =
| director       = Craig R. Baxley
| producer       = Kay Hoffman
| writer         = Steve Johnson
| narrator       = 
| starring       = Melissa Joan Hart
| music          = Gary Chang
| cinematography = João Fernandes
| editing        = Sonny Baskin
| studio         =
| distributor    = NBC
| released       = March 8, 1998
| runtime        = 90 minutes USA
| English
| budget         =
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1998 television film directed by Craig R. Baxley. The film was made to warn people about rape and popular teen actress Melissa Joan Hart was cast in the lead to draw a young audience. 

== Plot ==
Mary Stuartson is a college student who works as a reporter for the college newspaper. She is involved with a case, regarding possibly a few football players having hacked the computer of a professor to steal an upcoming test. Her best friend Holly is involved with one of the hackers, Billy, and one night at a party, she is raped by Clay Roberts, one of the athletes. She admits to Mary what happened, telling her that she tried to push him away and scream. She refuses to report it to the police and starts to think it was her own fault. She does go to the dean, however, but he seems to respond in Clays favor. Holly took a test after the rape, for further proof, but she made the mistake to first take a shower. Because of this, there is no physical evidence of proof that she was raped.

Holly tries to forget what happened, but Mary is determined that justice is served. She goes to the police, but they cant help her because the university system found Clay innocent. She discovers that last year, there were twenty-six reports of rapes, but only two of them resulted in charges. The mild university system frustrates Mary, and she thinks that Clay is especially treated softly because he is an important quarterback. She confronts him, but he states that Holly wanted to sleep with him and that he turned her down. She soon starts writing an article about the rape, but one of the student reports finds it and shows it to Clay. Not much later she is first attacked by a group of men waring Balaclava (clothing)|balaclavas. She is convinced Clay is responsible, but they all have alibis.

Meanwhile, Holly starts receiving threat letters and she is almost going over the edge. Mary decides to quit the investigation, but she soon starts to feel as if she has failed. Her parents suggest her to apply for another university, offering their retirement money. Mary refuses, however, explaining she cant leave everything behind. It doesnt take long for Mary to start reporting again and from there on, the threats start coming again. This time Marys boyfriend David is the victim, getting hard treatment from his fellow basketball players. Meanwhile, Mary angers the dean by printing an article in which she reveals that most rape reports last year didnt result into charges. As a response, her car is dumped at a nearby lake.

Later, Holly announces she will drop out of college. Clay spots Billy talking to Mary and later gets mad at him. Billy confronts him with the rape, but Clay assures him he isnt guilty. Meanwhile, Mary finds the dress Holly wore on the night of the rape and brings it to the police. It is soon discovered that there are sperm samples on the dress. On the night of a big football game, Clay is arrested and Mary is able to stay in college.

==Cast==
*Melissa Joan Hart as Mary Stuartson
*Corin Nemec as David McPherson
*Lisa Dean Ryan as Holly Sherman
*Peter MacNicol as Lawrence Dixon
*James McDaniel as Professor Thiel
*Josh Hopkins as Clay Roberts
*Lochlyn Munro as Billy
*Teryl Rothery as Brubaker Garry Chalk as Gardner
*Colin Lawrence as Augie
*Dale Wilson as Jefferson Susan Hogan as Helen Stuartson
*Tobias Mehler as Jesse Kevin McNulty as Tom Stuartson
*Emily Hart as Bobbi Stuartson

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 