The River King
{{Infobox film
 | name = The River King
 | image = The River King VideoCover.jpeg
 | image_size = 
 | caption = 
 | producer = Michele Camarda Michael Cowan Jason Piette Marion Pilowsky Christopher Zimmer
 | director = Nick Willing
 | writer = novel Alice Hoffman writer David Kane
 | starring = Edward Burns Rachelle Lefèvre Jennifer Ehle
 | music = Simon Boswell
 | cinematography = Paul Sarossy
 | editing = Jon Gregory
 | distributor = Kismet Film Company
 | released = October 21, 2005
 | runtime = 99 minutes
 | country = Canada United Kingdom
 | language = English
 | imdb = 
}} 2005 film starring Edward Burns, Rachelle Lefèvre and Jennifer Ehle as a policeman, student and teacher all searching for the truth behind the apparent suicide of a young man at a small private school. Lefevre plays Carlin Leander, the young mans only real friend, who is now haunted by memories of their increasingly difficult relationship.

The film is based on a book by Alice Hoffman.

==Sources==
*  The River King by Alice Hoffman

 
 
 
 
 
 
 
 


 