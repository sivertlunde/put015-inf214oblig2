Close to You (film)
{{Infobox film
| name = Close to You
| image = Close_To_You_Film.jpg
| caption = Theatrical movie poster
| Img_size =
| director = Cathy Garcia-Molina
| producer = Kara Kintanar Marizel V. Samson Charo Santos-Concio Malou N. Santos
| writer = John Paul Abellera   Mel Mendoza-Del Rosario
| starring = John Lloyd Cruz Bea Alonzo Sam Milby
| music =
| cinematography = Gus Cruz
| editing = Marya Ignacio
| distributor = Star Cinema
| released =  
| runtime = 110 minutes
| country = Philippines Filipino
| gross = ₱77 million
}} Filipino film starring John Lloyd Cruz, Bea Alonzo, and  introducing  Sam Milby.
The movie was released on February 15, 2006 under Star Cinema, directed by Cathy Garcia-Molina. The year 2006 ended with 5 Star Cinema films grossing more than ₱100 million including Close To You.

==Plot==
Manuel and Marian have been best friends since they were kids. They are so close that they know each others secrets...well, almost. For the last 16 years, Manuel never had the courage to tell Marian how much he loves her.

On the other hand, Marian sees Manuel as no more than her best friend. Marians greatest love has always been Lance (Sam), a classmate who protected her from bullies back in grade school. Ever since Lance and his family migrated abroad ten years ago, Marian never had the chance to establish contact with him again. The only thing that Marian knows is that Lance has become the lead singer of the new rock band, Orion.

Good fortune smiles on Marian when Orion decides to tour the Philippines. Marian is intent on seeing Lance again, and she brings Manuel with her on a chase that brings them around the Philippines and abroad. Manuel believes that the whole chase is futile because a popular star like Lance will not remember a simple girl like Marian, but Marian does not believe him.

When Lance and Marian finally meet again, sparks fly between them. Manuel now has to decide - will he let his best friend be happy with her Prince Charming, or will he fight for the love that has kept him alive for the last 16 years?

==Cast and characters==

===Main cast===
*John Lloyd Cruz as Manuel Soriano/Palits/Nuel
*Bea Alonzo as Marian Hermosa /Bru
*Sam Milby as Lance Guerzon

===Supporting cast===
*Tetchie Agbayani
*Nova Villa
*Melanie Marquez
*Buboy Garovillo

===Introducing===
*Carinne Cabebe
*Reyson Yap

==Soundtrack==
*"Close to You"
*:Performed by Sam Milby
*"Friend of Mine"
*:Performed by Metafour

==External links==
* 

 
 
 
 
 
 
 
 
 

 
 