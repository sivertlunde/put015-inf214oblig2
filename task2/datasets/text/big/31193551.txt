Nasihat
 
{{Infobox film
| name           = Nasihat
| image          = Nasihatfilm.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Aravind Sen
| producer       = Aarvind Sen
| writer         = Story: Prafulla Roy Screenplay: Suhrid Kar Dialogues: Kader Khan
| starring       = Rajesh Khanna Shabana Azmi Mithun Chakraborty Deepti Naval 
| music          = Kalyanji Anandji
| cinematography = Ishan Arya
| editing        = Ramesh Pai
| studio         = Lalit Kala Mandir
| distributor    = 
| released       =  
| runtime        = 155 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =
}} Hindi film directed and produced by Aravind Sen. Rajesh Khanna, Shabana Azmi play the lead roles and supported by Mithun Chakraborty and Deepti Naval.

==Plot==
Indrani, a daughter of a multimillionaire Seth Ratanlal, falls in love with an ordinary but honest Dinesh and vows to marry him. Dishonest and cunning servant Mohanlal gets hurt to know this and starts bittering the cars of Seth Ratanlal against Indrani. Indrani confessing all this, gives a shocking news that she is going to be a mother of Dineshs child.

Mohanlal gets beaten by Dineshs hired gangsters who take him underground and tell Seth Ratanlal that Dinesh absconded to London by giving false promise of marriage to Indrani.

To get rid of defaming, Seth Ratanlal gives the charge of his office and business to Mohanlal, and leaves the city with his daughter for few days.

Honest accountant Jagdish seeing Mohanlal in the chair of owner Seth Ratan Lal, exposes his true figure of dishonesty and unfaithfulness. This insult hurts Mohanlal very much and as a revenge he causes Jagdish to be in a truck accident. Rajesh, son of Jagdish, takes a vow of revenge for the one who made his father lame.

After returning, Seth Ratan Lal comes to know that cunning Mohanlal has captured all his business. Circumstances compel Indrani to marry Mohanlal.

Mohanlals son Ranjeet, who is very helpful to the poor and very much aloof from the character of his father, gets released union leader Bajrangee from the jail. Sunita with a view to take revenge works as a private secretary to Mohanlal.

==Reception==
The film had collections worth 8.5 crores at the box office in 1986 in India. It received thee and a half  stars in the Bollywood guide Collections.

==Music==
#"Ye Hawayen Sard Sard Hain" – Suresh Wadkar, Asha Bhonsle picturised on Rajesh Khanna and Shabana Azmi
#"Tum Bahut Haseen Sahi" – Kishore Kumar, Alka Yagnik picturised on Rajesh Khanna and Shabana Azmi
#"Jhunak Jhunak Jhanjhar Baaje" – Kishore Kumar, Mahendra Kapoor, Alka Yagnik picturised on Rajesh Khanna, Mithun and Deepti
#"Tere Mere Pyar Ki Kundali" – Kishore Kumar, Hemalata picturized on Mithun Chakraborthy and Deepti Naval
#"Mera Mann Dekhe Sapna" – Sadhana Sargam picturised on Shabana Azmi
#"Zindagi Hai Kitne Din Ki" – Kishore Kumar, Asha Bhonsle picturized on Mithun Chakraborthy and Deepti Naval

==Cast==
* Rajesh Khanna
* Shabana Azmi
* Mithun Chakraborty
* Deepti Naval
* Amjad Khan
* Kader Khan
* Satyen Kappoo
* Shriram Lagoo
* Aruna Irani
* Dinesh Hingoo
* Krishnakant
* Shubha Khote
* Dinesh Thakur
* Tanuja

==References==
*  

==External links==
*  

 
 