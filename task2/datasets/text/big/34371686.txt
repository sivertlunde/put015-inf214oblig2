The Sun Behind the Clouds
 
{{Infobox film
| name = The Sun Behind the Clouds
| image = TheSunBehindTheClouds2010Poster.jpg
| caption = US Film poster
| director = Ritu Sarin Tenzing Sonam
| writer = Tenzing Sonam
| starring = The Dalai Lama John Sergeant Caroline Behar Tenzin Tsetan Choklay Lavinia Currier Babeth M. VanLoo Francesca von Habsburg
| released =  
| studio = 
| distributor = Zeitgeist Films 
| country = India United Kingdom 
| language = English, Tibetan, and Mandarin
| runtime = 79 minutes
}}
The Sun Behind the Clouds looks at Chinas occupation of Tibet from the perspective of the vocally secessionist Tibetan youth, and from that of their spiritual leader, the Dalai Lama, whose reaction to the Chinese presence has been markedly less confrontational. Directed by Ritu Sarin and Tenzing Sonam, the film features interviews with the Dalai Lama and Tenzin Tsundue. The Sun Behind the Clouds premiered in the United States at the 2010 Palm Springs International Film Festival before playing at Film Forum in New York City.

The film covers the 2008 Tibetan unrest, including Buddhists protests in Lhasa and the 2008 Beijing Olympics.

== External links ==
*  
*  
*  
*   Zeitgeist Films synopsis

 
 
 
 
 
 
 
 


 