The Tall Blond Man with One Black Shoe
{{Infobox film
| name           = Le Grand Blond avec une chaussure noire
| image          = The Tall Blond Man with One Black Shoe.jpg image size      = 
| caption        = 
| director       = Yves Robert
| producer       = Alain Poiré Yves Robert
| writer         = Francis Veber
| starring       = Pierre Richard Bernard Blier Jean Rochefort
| music          = Vladimir Cosma
| cinematography = René Mathelin
| editing        = Ghislaine Desjonqueres Gaumont
| released       =  
| runtime        = 90 minutes
| country        = France
| language       = French
| budget         = 
}} Le Retour du Grand Blond, was released in 1974.

The film was remade in English as The Man with One Red Shoe.

==Plot== Orly airport at 9:30AM the next morning, making Milan (who has been listening) believe that Perrache has gone to meet a master spy who will expose Milans treachery. However, Toulouse secretly instructs Perrache to choose someone at random from the crowd of travelers arriving at that time. 

After considering several possibilities from the flight arriving at the specified time, Perrache selects François Perrin, an unsuspecting violinist, who is noticeable because, as the result of a practical joke played on him by his fellow orchestra members, he has arrived wearing a black shoe on one foot and a reddish-brown one on the other. Milan takes the bait and immediately begins a series of attempts to find out what Perrin knows—never ever realizing the fact that Perrin knows nothing at all about espionage (although he is an expert on music). Milans machinations involve Perrin in a series of increasingly peculiar adventures which he either avoids or escapes from by pure luck (which only confirm Milans increasingly paranoid suspicions), and although Perrin is largely oblivious to the mayhem occurring around him he cant help noticing Milans top agent, the beautiful femme fatale Christine.  Adding to the confusion is the fact that Perrin is having an affair with Paulette Lefebvre, the wife of his best friend Maurice (both of whom are also in the same orchestra as Perrin), and Maurice, upon accidentally hearing a recording  of Perrin and Paulette having torrid sex (made by Milans agents and listened to inside a floral delivery truck), jumps to the mistaken conclusion that Paulette is having an affair with a florist. All the time, Toulouse and Perrache watch the chaos serenely, although Perrache is troubled by his chiefs callousness towards Perrins possibly being killed.
 Le Retour du Grand Blond). A slapstick love scene (watched by Milan and his cohorts on a television monitor) ensues, concluding with Milans decision (despite Christines belief that Perrin couldnt possibly be an agent) to have Perrin eliminated. More mayhem (including Maurices learning the truth about his wifes affair) and treachery (including Christines defection from Milans group to save Perrin, with whom she has fallen in love) follow, climaxing in the deaths of not only agents from both Toulouses and Milans groups but also Milan himself, who only learns the truth about Perrin from Perrache just before he dies. Realizing how he has been fooled, Milan dies with a smile of appreciation. Maurice, who has repeatedly walked in on the aftermaths of the shoot-outs in Perrins apartment, suffers a total mental breakdown.

The film ends as it began, at Orly airport. Perrin pushes a huge Louis Vuitton steamer trunk in an airport luggage cart, talking softly to Christine, who is hidden inside. Their destination is Rio de Janeiro|Rio.  Toulouse, who has been watching Perrins departure on a monitor, instructs Perrache to contact Perrin when Perrin returns, remarking "After all, he handles himself pretty well."

==Awards==
The film won the Silver Bear award at the 23rd Berlin International Film Festival in 1973.    It was nominated as Best Foreign Film at the National Board of Review (1973). 

==Cast==
* Pierre Richard — François Perrin 
* Bernard Blier — Bernard Milan  
* Jean Rochefort — Colonel Louis Toulouse  
* Mireille Darc — Christine  
* Tania Balachova — The mother of Louis Toulouse  
* Jean Carmet — Maurice Lefebvre
* Colette Castel — Paulette Lefebvre
* Paul Le Person — Perrache 
* Jean Obé — Botrel  
* Robert Castel —  Georghiu  
* Jean Saudray — Poucet  
* Roger Caccia — Mr. Boudart  
* Arlette Balkis — Mrs. Boudart  
* Robert Dalban — The False Deliveryman
* Jean Bouise — The Minister

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 