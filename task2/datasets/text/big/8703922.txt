Paloh (film)
 
 
{{Infobox Film
| name           = Paloh
| image          = Paloh2003.jpg
| caption        =
| director       = Adman Salleh
| producer       =
| starring       =   Gibran Agi Ellie Suriaty Hasnul Rahmat Ani Maiyuni Thor Kah Hong A. Samad Said M. Amin
| music          =
| distributor    = Filem Negara Malaysia National Film Development Corporation Malaysia (FINAS)
| released       =  
| runtime        =
| country        = Malaysia
| awards         = Malay Mandarin Mandarin Cantonese Cantonese
| budget         = RM 4.01 million
| gross          = RM 140,000
}}

Paloh is a 2003 Malaysian historical film directed by Adman Salleh. This film is produced by Filem Negara Malaysia and the National Film Development Corporation Malaysia (FINAS). This film is rated 18PL for strong brutal violence and terror throughout, sexual assaults, gruesome images and pervasive strong language.

== Plot == Japanese occupation in 1944, the movie is about the confrontation between the Japanese occupying force and the Communist Party of Malaya. To survive, four friends - Ahmad, Osman, Puteh and Harun choose to serve the Japanese Police Force. Amidst a sea of uncertainties, Ahmad falls in love with Siew Lan and Puteh falls in love with Fatimah - both girls from different worlds, different cultures, even opposing sides. And Osman - a friend of theirs and a spy for the Japanese Police - chooses to serve his own agenda. Swee Lans father is a local leader of the Bintang Tiga sympathizers and has asked Swee Lan to establish a love relationship with Ahmad so as to gather intelligence of the Japanese movements. However, Swee Lan really falls in love with Ahmad and thus begins a deep struggle within her. She has to either obey her father or betray her lover. Ahmad, on the other hand, sees the high handedness of the Japanese and he does not want to be a part of the Japanese scheme any more. As one character says in the movie, "Whether it is the British , the Japanese or the communist, they are all the same. But at least the communist are fighting to free us."

== Cast ==
* Janet Khoo - Siew Lan  
*   - Ahmad
* Gibran Agi - Osman
* Ellie Suriaty - Fatimah
* Hasnul Rahmat - Puteh
* Ani Maiyuni
* Thor Kah Hong
* Steve Ng Boon Cheng
* A. Samad Salleh
* Rohani Yusuff
* Meoki Tong May Lan
* Kamaliah Mat Dom
* Adlin Aman Ramli
* Lee Yoke Lan
* Zack Taipan
* Yalal Chin
* M. Amin

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 