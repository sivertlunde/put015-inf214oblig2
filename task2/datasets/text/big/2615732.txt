The Joy of Life
 
{{Infobox film name            = The Joy of Life image           =The Joy of Life.jpg caption         = director        =Jenni Olson producer        =Scott Noble and Julie Dorf writer          =Jenni Olson starring        = music           = cinematography  =Sophia Constantinou editing         =Marc Henrich distributor     = released        = runtime         = 65 min language        = English budget          =
|}}

The Joy of Life (2005) is an experimental landscape documentary film about the history of suicide and the Golden Gate Bridge, and the adventures of a butch lesbian in San Francisco, California. Since its January 2005 premiere at the  Sundance Film Festival, this innovative feature film played a pivotal role in renewing debate about the need for a suicide barrier on The Golden Gate Bridge as well as garnering praise and earning awards for its unique filmmaking style.

The film combines 16mm landscape cinematography with a lyrical voiceover (performed by LA-based artist/actor Harriet “Harry” Dodge) to share two San Francisco stories: the history of the Golden Gate Bridge as a suicide landmark, and the story of a butch dyke in San Francisco searching for love and self-discovery.

The two stories are punctuated by Lawrence Ferlinghettis reading of his ode to San Francisco, "The Changing Light" and bookended by opening and closing credits music from legendary 50s icon (and probable Golden Gate suicide) Weldon Kees.

==Reception and aftermath==
The film was awarded several prizes including: the 2005 Marlon Riggs Award (for courage & vision in Bay Area filmmaking) by the San Francisco Film Critics Circle; the 2005 Outstanding Artistic Achievement Award by Outfest, the Los Angeles Gay and Lesbian Film Festival and the 2005 Best US Narrative Screenplay Award from The New Festival, New York Lesbian and Gay Film Festival.
 The Bridge, which would be released in 2005. A week later "The Joy of Life" world premiered at the Sundance Film Festival and video copies of the film were circulated to members of the Golden Gate Bridge District board of directors (with the help of the Psychiatric Foundation of Northern California.

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 