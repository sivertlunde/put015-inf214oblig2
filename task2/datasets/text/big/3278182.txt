3 Ninjas Knuckle Up
{{Infobox film
| name           = 3 Ninjas Knuckle Up
| image          = 3 ninjas knuckle up poster.jpg
| caption        = Theatrical release poster
| director       = Sang-ok Shin
| producer       = Martha Chang James Kang
| writer         = Alex S. Kim Victor Wong Charles Napier Michael Treanor Max Elliott Slade Chad Power
| music          = Gary Stevan Scott Louis Febre
| cinematography = Eugene Shluglet
| editing        = Pam Choules
| studio         = Sheen Productions, inc. Leeds / Ben-Ami Productions, inc.
| distributor    = TriStar Pictures
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          = United States dollar|$407,618 (U.S.) (sub-total)
}} martial arts 3 Ninjas and 3 Ninjas Kick Back. The film was directed by Sang-ok Shin.

The film was shot in 1992, the same year the first film was released, but was not released until 1995.

==Plot==
Rocky (Michael Treanor), Colt (Max Elliott Slade), Tum Tum (Chad Power) defend "Truth, Justice and the American Way", once more - this time, protecting a Native American village and the rest of society against a Toxic Waste Company.

During a summer the boys are staying with Grandpa Mori, the boys encounter a group assaulting a girl named Jo at a pizza parlour, after fending off the men they are praised for their martial arts techniques, which gives them big heads. Despite their efforts, they are put to work by Mori and the pizza owner to work off damages, Mori tries to teach them a lesson in humility but the reference of a flower blooming goes over their heads. Jo comes to the boys later and explains that the men are under the employ of Jack Harding, an industrialist who is illegally dumping toxic contents into the reserve, without proof, they can do nothing. Jos father had gone to investigate but had not returned. Colt, who is seemingly attracted to Jo says that they will help, and they mount an escape plan for her father that night, which is successful. They spend the night celebrating with the tribe and getting thanks for helping them. Jos father appeals for a court date with significant evidence to put Jack out of business for good, undeterred, Jack arranges to have Jo kidnapped and convince her father to falsify his evidence, which he has no other choice.

Rocky and the others get information to where Jo is being held and drive out to free her and return before the court case is dismissed and all of her fathers hard work accounts for nothing. After working through a small band of armed men, they find Jo and return her to the court house just before her father turns the real evidence over to Jack he admits his mistake and hands the evidence to the judge who deems the case and shuts down the company producing the waste. Jo looks around for the heroes of the day, but they are nowhere to be found. Rocky realizes Moris words of what a flower says when it blooms and they return to Mori, sharing that they say that a flower doesnt show off its beauty, its there for someone to find, and Mori is ecstatic at the realization.

==Cast==
* Michael Treanor - Samuel Rocky Douglas
* Max Elliott Slade - Jeffrey Colt Douglas
* Chad Power - Michael Tum Tum Douglas Victor Wong - Grandpa Mori Tanaka
* Crystle Lightning - Jo
* Patrick Kilpatrick - J.J.
* Donal Logue - Jimmy Scott MacDonald - Eddy Charles Napier - Jack
* Vincent Schiavelli - Mayor
* Don Stark - Sheriff

==Reception==

The movie received mostly negative reviews.   Many fans attribute the movies lack of success to its PG-13 rating which drove away younger viewers.

==References==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 