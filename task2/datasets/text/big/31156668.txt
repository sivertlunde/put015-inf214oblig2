Kattuchembakam
{{Infobox film
| name           =Kattuchembakam
| image          = Kattuchembakam.jpg
| image size     =
| caption        =
| director       =Vinayan
| producer       =M. Mani
| writer         =Vinayan
| narrator       = Sonia
| music          =Mohan Sithara
| cinematography = 
| editing        =
| distributor    = 
| released       =2002
| runtime        =
| country        =India Malayalam
| budget         =
| gross          =
| preceded by    =
| followed by    =
}} Malayalam movie love based film starring Jayasurya and Charmy Kaur.  The film was directed by Vinayan. 

==Plot==

The movie revolves around a tribe who are living isolated in the forest area and their lives.
Chandru and Shivan are two close friends lives in a tribal village in the midst of the forest with their sisters Karthika Mathew and Chembagam (Charmy Kaur) respectively. Chandru is in love with Chembagam and Shivan is in love with Karthika Mathew.

They always have trouble with Kankani Rajappan who steal their food products such as Rice, Sugar etc., When the village people find about the theft ask Kankani Rajappan they start fighting with each other. When police come to the spot they accuse Chandru and Shivan and arrest them. Chembagam is the most beautiful girl in the village and the police inspector who has an eye on her for a very long time tries to rape her. S.P Rajendran newly transferred police commissioner saves Chembagam from the inspector and becomes the saviour for the village people.

Astonished by the beauty of nature he decides to take off and stay in the village guest house. He also make a proposal to marry Chembagam and take her back to town thereby providing her a luxurious life. In spite of knowing Chandru is in love with Chembagam Shivan agrees to give his sisters hand in marriage to S.P Rajendran thinking that her sister will be fine for the rest of her life.

Chandru confronts him at the river bank and they both fight. Shivan being stronger than Chandru manages to control him & says that no one can stop this marriage and if Chandru stand against it he will kill him.

Chandru tries to kill S.P Rajendran but it fails. The night before the marriage Chembagam elopes with Chandru & they both decide to die, but only to be caught by the villagers red handed. Chandru is held as captive & Chembagam was married to S.P Rajendran the same night without the knowledge of others.

The next day they find the dead body of S.P Rajendran and assume that Chandru is the killer. Chandru goes to the guest house and finds that Chembagam is missing and fights with the friends of S.P Rajendran. He returns to the village and learns S.P Rajendran is dead.

Shivan confronts him and fights with him thinking that he ruined his sisters life. Chembagam come there with the blood stained clothes and confesses that she is the one who killed S.P Rajendran. She also reveals that S.P Rajendran married her only to make her a feast to his friends. They raped her in the name of marriage and she killed him after knowing this ugly truth.

She confess to Chandru that she is no more a suitable girl for him to live with. Chandru replies that he love her for her character and not for her beauty and they embrace each other.

==Cast==
 
* Anoop Menon as  Shivan
* Manoj K. Jayan as S.P Rajendran
* Jayasurya as Chandru
* Charmy Kaur as Chempakam
* Karthika Mathew
* Kottayam Nazeer as Ponnappan
* Cochin Haneefa as Kankani Rajappan
* Harishree Ashokan as Antappan
* Indrans as Aandi
* Narayanankutty as Kaduva Kuruppu
* Poornitha Janardhanan as D.G.P Meon
* Mala Aravindan as Kattumooppan
*Machan Varghese as Karumban
*Kozhikode Sarada as Chandrus mother
*Prem Kumar as Supply Officer Shivaji as Vishwanathan  Sonia as Paaru
* Chali Pala as police
*Bindu Ramakrishnan as Subadramma
* Beena Sabu as Dakshyayani
 

== References ==
 

==External links==
*  

 
 
 