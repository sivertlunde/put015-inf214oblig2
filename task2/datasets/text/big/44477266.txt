Hum Hai Teen Khurafaati
{{Infobox film
| name           = Hum Hai Teen Khurafaati
| image          = Hum Hai Teen Khurafaati poster.jpg
| alt            =
| caption        = Theatrical release poster
| film name      = 
| director       = Rajeshwar Chauhan
| producer       = Jaivindra Singh Bhati
| writer         = 
| screenplay     = Rajeev Gupta
| story          = 
| based on       =  
| starring       = 
| narrator       = 
| music          = Kashi−Richard
| cinematography = 
| editing        = 
| studio         = Shape Entertainment
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          =  
}} drama film directed by Rajeshwar Chauhan and produced by Jaivindra Singh Bhati under the banner of Shape Entertainment. It features Heena Panchal, Rahul Dasgupta, Mausam Sharma, Pranshu Kaushal, Shrey Chhabra and Nikkita Butola in the lead roles. Kashi−Richard scored the music for the film.      

== Cast ==
 
* Heena Panchal
* Rahul Dasgupta
* Mausam Sharma
* Pranshu Kaushal
* Shrey Chhabra
* Nikkita Butola
* Vrajesh Hirjee
* Hemant Pandey
* Razak Khan
* Brijendra Kala
* Manoj Bakshi
* Honey Shergil
* Soniya Kaur
* Poonam Sood
 

== Music ==
{{Infobox album  
| Name       = Hum Hai Teen Khurafaati
| Type       = soundtrack
| Artist     = Kashi−Richard
| Cover      =
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =   Zee Music Company
| Producer   = 
| Last album = 
| This album = Hum Hai Teen Khurafaati (2014)
| Next album = 
}}
The soundtrack of the film has been given by Kashi−Richard.  The first single "Khurafaati" released on 19 September 2014 on iTunes. 

{{Track listing
| collapsed       = 
| headline        = Hum Hai Teen Khurafaati (Original Motion Picture Soundtrack)
| extra_column    = Singer(s)
| total_length    = 
| all_lyrics      = Deepak Noor, Shweta Raaj and Satyaprakash
| all_music       = Kashi−Richard
| lyrics_credits  = yes
| title1          = Khurafaati
| lyrics1         = Shweta Raaj
| extra1          = Vishal Mishra, Amitabh Narayan, Jayshankar & Yashita Yashpal
| length1         = 3:30
| title2          = Dil Jugaadu
| lyrics2         = Shweta Raaj
| extra2          = Arijit Singh
| length2         = 3:05
| title3          = Mohalle Mein Hookah 
| lyrics3         = Deepak Noor
| extra3          = Amitabh Narayan, Sunidhi Chauhan
| length3         = 3:57
| title4          = Chupke Se
| note4           = Male Version
| lyrics4         = Satyaprakash
| extra4          = Aishwarya Nigam, Shreya Ghoshal
| length4         = 5:18
| title5          = Chupke Se
| note5           = Female Version
| lyrics5         = Satyaprakash
| extra5          = Shreya Ghoshal
| length5         = 
}}

== References ==
 

== External links ==
*  