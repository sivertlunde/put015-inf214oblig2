Queer Boys and Girls on the Shinkansen
{{Infobox Film
  | name = Queer Boys and Girls on the Shinkansen
  | image =
  | caption = A scene from the opening act
  | director = Hasegawa Kenjiro iri Kang Yen-Nien Woolala Satoko Hata Tomoaki Takasaki Keiichi Hirai Yuko Imaizumi Koichi Taguchi Hiroki Akira the Hustler
  | producer = habakari-cinema+records
  | screened = July 17, 2004
  | runtime = 58 minutes English subtitles
}}
 Japanese movie English title is Queer Boys and Girls on the Bullet Train.

==Description== Habakari chose ten filmmakers to make a five-minute work each, developed around a gay or lesbian theme, and compiled the resulting shorts in random order to create this omnibus film. The result is a queer film, by queer filmmakers, for a queer audience. Each short is its own short story, and the styles range from drama and experimental film and Japanese Animation|animation.

==Acts==
The movie consists of twelve Act (theater)|acts.

*00 - Opening Act, "Lets take a trip".
*01 - "Parallel Contact", written and directed by Hasegawa Kenjiro.
*02 - "I Hum, and Shes Dashing When She Walks", written and directed by Iri (film author)|iri.
*03 - "KEY", directed by Kang Yen-Nien.
*04 - "wrap! rap! -10cs 3 -", written and directed by Woolala Satoko (うらら さとこ).
*05 - "JUICY!", written and directed by JohnJ Heart|JohnJ♥.
*06 - "Techniques for Deadly Blows in 199X", written, directed, and animated by Takasaki Keiichi.
*07 - "machi27", directed by Hirai Yuko.
*08 - "I Want You to Kiss Me", written and directed by Imaizumi Koichi.
*09 - "One Brilliant Moment", directed by Taguchi Hiroki.
*10 - "Bye-Bye Over the Rainbow", written and directed by Akira the Hustler.
*11 - Closing Act.

==External links==
* 

 
 
 
 


 
 