Xingu (film)
{{Infobox film
| name           = Xingu
| image          = Xingu Film Poster.jpg
| caption        = 
| director       = Cao Hamburger
| producer       =Fernando Meirelles Andrea Barata Ribeiro Bel Berlinck
| writer         = Cao Hamburger Elena Soarez Anna Muylaert
| starring       = João Miguel (actor)|João Miguel Felipe Camargo Caio Blat
| music          = Beto Villares
| editing        = Gustavo Giani
| cinematography = Adriano Goldman
| studio         = O2 Filmes Globo Filmes
| distributor    =  Downtown Filmes  RioFilme Amazonas Film Festival|ref1= |2012|04|06|Brazil|ref2= {{cite web |url=http://www1.folha.uol.com.br/multimidia/videocasts/1070081-veja-bastidores-do-filme-xingu-que-estreia-no-dia-6-de-abril.shtml |title=Veja bastidores do filme "Xingu", que estreia no dia 6 de abril  |date=31 March 2012 |work=Folha de S. Paulo
 |publisher=Grupo Folha |language=Portuguese |accessdate=9 December 2012}} |df=yes}}
| runtime        = 102&nbsp;minutes
| country        = Brazil Portuguese Tupi Tupi
| budget         = R$14–16 million    
| gross          = R$3,898,283  
}}
Xingu is a 2011 Brazilian drama film directed by Cao Hamburger and scripted by him, Elena Soárez and Anna Muylaert. Starring João Miguel (actor)|João Miguel, Felipe Camargo and Caio Blat, the film tells the Villas-Bôas brothers trajectory from the moment in which they joined the Roncador-Xingu expedition, part of the Westward March of Getúlio Vargas, in 1943.

It was shot in Tocantins, Xingu National Park, and in the Greater São Paulo.   The film was exhibited for the first time in 2011, at the 8th Amazonas Film Festival. The official premiere took place on April 6, 2012. The film was watched by about 370.000 spectators and has raised more than four million reais in box office.    A television adaptation in four episodes was aired on Rede Globo between 25 and December 28, 2012.   

==Plot==
The story takes place in the 1940s when the Villas-Bôas brothers—Claudio (João Miguel (actor)|João Miguel), Leonardo (Caio Blat) and Orlando (Felipe Camargo)—start an exploratory expedition into the Xingu River.    They make contact with the local tribes, learn to live in the rainforest, and persuade a reluctant government to found the Xingu National Park.

==Cast==
*João Miguel (actor)|João Miguel as Claudio Villas Boas
*Felipe Camargo as Orlando Villas Boas
*Caio Blat as Leonardo Villas Boas
*Maiarim Kaiabi as Prepori
*Awakari Tumã Kaiabi as Pionim
*Adana Kambeba as Kaiulu
*Tapaié Waurá as Izaquiri
*Totomai Yawalapiti as Guerreiro Kalapalo
*Maria Flor as Marina
*Augusto Madeira as Noel Nutels
*Fábio Lago as Bamburra

==Awards==
*Jury Award for Best Cinematography - 2012 Prêmio Contigo Cinema
*3rd place Panorama Audience Award for Fiction Film - 2012 Berlin International Film Festival

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 