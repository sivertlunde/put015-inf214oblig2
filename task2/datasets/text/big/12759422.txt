Swing Wedding
{{Infobox Hollywood cartoon|
| cartoon_name = Swing Wedding
| series = Happy Harmonies
| image =
| caption = A controversial scene from the cartoon
| director = Hugh Harman
| story_artist = Larry Martin (unc.) Tom McKimson (unc.)
| voice_actor =
| musician =
| producer = Hugh Harman   Rudolph Ising
| distributor = Metro-Goldwyn-Mayer
| release_date = February 13, 1937 (USA)
| color_process = Technicolor
| runtime = 8 min (one reel)
| movie_language = English
}} animated Musical musical short short by Metro-Goldwyn-Mayer. Directed by Hugh Harman, it is part of the Happy Harmonies cartoon series.

The cartoon portrays a wedding celebrated by a group of frogs in a swamp. The frogs are designed as caricatures of various African American celebrities of the 1930s, such as Ethel Waters, Stepin Fetchit, Louis Armstrong, Cab Calloway, Fats Waller and the Mills Brothers.
 valve as a syringe. The scene plays on the stereotype of black jazz musicians using drugs before performing. 

This cartoon was re-released in a shorter version called "Hot Frogs" in 1942.

==Notes==
 

==External links==
*  at the Internet Movie Database.
*  on DailyMotion

 
 
 
 


 