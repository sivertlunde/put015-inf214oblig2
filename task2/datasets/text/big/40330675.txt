New Delhi (1988 film)
{{Infobox film
 | name = New Delhi
 | image = NewDelhi1988.jpg
 | caption = Promotional Poster
 | director = Joshiy
 | producer = M.Sudhakar Reddy Dr.M.Thirupathi Reddy Rajeev Kumar
 | writer = Dennis Joseph
 | dialogue = 
 | starring = Jeetendra Sumalatha Raza Murad Thiagarajan Shyam
 | cinematography = Jayanan Vincent
 | lyrics = 
 | associate director = 
 | art director = 
 | choreographer = 
 | released = December 16, 1988
 | runtime = 135 min.
 | language = Hindi Rs 2 Crores
 | preceded_by = 
 | followed_by = 
 }}
 1988 Hindi Indian feature directed by Joshiy, starring Jeetendra, Sumalatha, Raza Murad, Thiagarajan

==Plot==
 Malayalam film New Delhi released in 1987.

==Summary==

Vijay Kumar is an honest Delhi-based Journalist later expose the wrongdoings of two corrupt politicians to help his Keralite girlfriend, Maria Fernandez, is beaten to such an extent that he becomes partly paralyzed is imprisoned. The two politicians, Kendriya Mantri Shankar Babu, and Desbandhu Sharma, ensure everything that Vijay Kumar does not get pardoned. Tortured Vijay Kumar complete his sentence and his girlfriend Maria helps him to start his own publication New Delhi Diary. He uses information and material sent to him exclusively by his ace reporter, Vishwanath. Then the people who had harmed him start dying violent deaths one by one arousing suspicion amongst his staff, as well as his sister, Uma and her boy friend Suresh, who decide to investigate Vijays background and who the mysterious Vishwanath really is.

==Cast==

*Jeetendra - Vijay VK Kumar 
*Sumalatha - Maria Fernandez  
*Raza Murad - Janseva Partys Leader Deshbandhu Sharma  
*Thiagarajan - Nataraj Vishnu
*Suresh Gopi - Suresh Devan - Shankar   Urvashi - Uma   Siddique - Siddique  Vijayaraghavan - Ananth
*Mohan Jose - Johny
*Prathapachandran - Jailor

==Box-Office==

Even though the original version was a blockbuster, this remake version failed mainly due to the casting. Apart from Jeetendra and Raza Murad, all the actors were from the original Malayalam version and this decision by the makers backfired as North Indian audience could nt relate with all the characters played by South Indian actors.

==References==
*http://www.imdb.com/title/tt0363525/
*http://ibosnetwork.com/asp/filmbodetails.asp?id=Newdelhi

==External links==
*  

 
 
 
 