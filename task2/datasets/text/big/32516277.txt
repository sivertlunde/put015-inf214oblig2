Black Lightning (2009 film)
 
{{Infobox film
| name           = Black Lightning
| image          = Black Lightning.jpg
| border         = 
| alt            = 
| caption        = 
| director       = Alexandr Voitinsky Dmitry Kiseilov
| producer       = Timur Bekmambetov
| writer         = Dmitry Aleinikov Aleksandr Talal Aleksandr Voitinskiy Mikhail Vrubel
| starring       = Grigoriy Dobrygin Ekaterina Vilkova Sergei Garmash Viktor Verzhbitsky
| music          = 
| cinematography = 
| editing        = 
| studio         = Bazelevs Universal Pictures
| released       =  
| runtime        = 100 minutes
| country        = Russia
| language       = Russian
| budget         = 
| gross          =
}}
Black Lightning ( ; Transliteration|translit.&nbsp;Chornaya Molniya) is a 2009 Russian superhero film directed by Alexandr Voitinsky and Dmitry Kiseliov, and produced by Timur Bekmambetov.

== Plot ==
 GAZ 21 Soviet defense contract, and is equipped with a rocket engine. Dima, however, suspects none of this. Hes not even excited when he finds an old record and a photo of three scientists in the glove box of the car.

Dima is in love with his classmate Nastya Svetlova (Yekaterina Vilkova). Dima believes that the only way to achieve success is to be egotistical. This belief is strengthened when Dima sees the success of Nastyas friend, Max (Ivan Zhidkov), who also happens to have a Mercedes-Benz|Mercedes.

Dima begins working as a flower delivery man. He finds out about the special features of his car completely by accident, while Kuptsovs men are chasing him. Losing Dima of the flying car, Dima crash lands in an abandoned building. With Maxs help, Dima is able to listen to the record from the glove box. From the record, he finds the scientists from the photo. Two of them, Pavel Perepelkin and Olga Romantseva turn out to be husband and wife. Dima, pretending to be a newspaper reporter, learns from them everything about the car. Romantsova gives Dima the cars manual, which the third scientist, Mikhail Yelizarov, wrote. At first, Dima enjoys his flights in his flying car and makes decent money flying to avoid the legendary Moscow traffic jams. But his egotism leads Dima to not help a stabbed man on the street, who turns out to be his own father.

The death of his father changes Dimas outlook on life. He decides to use his car to help people and becomes a superhero. Soon all the people of the city became his fans, and journalists give him the name "Black Lightning."
 Volga and Volga under the ice of the Moscow River.
 Mercedes runs out of nanofuel. Kuptsov floats in the Earths orbit, left for dead.

Dima and Nastya meet on the ground and celebrate the New Year together.

== Cast ==
* Grigoriy Dobrygin as Dima Maykov/Black Lightning
* Yekaterina Vilkova as Nastya Svetlova
* Viktor Verzhbitsky as Prof. Viktor Kuptsov
* Sergei Garmash as Pavel Maykov (Dmitrys father)
* Yelena Valyushkina as Nastasia Maykova (Dmitrys mother)
* Katia Starshova as Tania Maykova (Dmitrys sister)
* Ivan Zhidkov as Maxim
* Igor Savochkin as Boria Ivanovich
* Valeri Zolotukhin as Pavel Perepelkin
* Yekaterina Vasilyeva as Olga Romantseva
* Juosas Budraitis as Mikhail Yelizarov
* Dato Bakhtadze as Bahram Makhamedovich Mikhail Efremov as the Drunk guy

==References==
*   //interfax.ru
*   //Lenta.ru

== External links ==
* 

 
 
 
 
 
 
 
 
 