Happiness Ahead
{{Infobox film
| name           = Happiness Ahead
| image          = 
| image_size     = 
| caption        = 
| director       = Mervyn Le Roy
| producer       =
| writer         = Brian Marlow Harry Sauber
| based on       = 
| starring       = Dick Powell Josephine Hutchinson
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Warner Bros.
| released       = October 27, 1934
| runtime        = 86 minutes
| country        = United States English
| budget         =
| gross          =

}}
Happiness Ahead is a 1934 film directed by Mervyn LeRoy and starring Dick Powell with Josephine Hutchinson.

==Plot==
Joan Bradford is a society heiress who rebels against her mothers choice of a future husband by masquerading as a working class girl and dating a window washer.

==Cast (in credits order)==
*Dick Powell as Bob Lane
*Josephine Hutchinson as Joan Bradford

==Critical reception==
unknown

==References==
 

== External links ==
* 

 

 
 
 
 
 


 