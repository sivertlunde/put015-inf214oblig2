The Return of the Rat
{{Infobox film
| name           = The Return of the Rat
| image          = 
| image_size     = 
| caption        = 
| director       = Graham Cutts
| producer       = Michael Balcon C. M. Woolf Edgar C. Middleton A. Neil Lyons
| starring       = Ivor Novello Isabel Jeans Mabel Poulton   Gordon Harker
| music          = W.L. Trytel
| cinematography = Roy F. Overbaugh
| editing        = 
| studio         = Gainsborough Pictures
| distributor    = Woolf & Freedman Film Service
| released       = 1929
| runtime        = 84 minutes
| country        = United Kingdom
| language       = English
}}
 silent drama film directed by Graham Cutts and starring Ivor Novello, Isabel Jeans and Mabel Poulton. It was made by Gainsborough Pictures at their Islington Studios.

==Background==
The Return of the Rat is the last of a trilogy of films starring Novello as Pierre Boucheron (The Rat) and Jeans as his sometime lover, sometime nemesis Zélie.  Marie Ault also appears in all three films.  Like its predecessor The Triumph of the Rat, The Return of the Rat does not take the story up from where the previous film had ended, instead opening with a scenario which does not seem to flow logically from the previous films conclusion.  The Triumph of the Rat closes with Pierre destitute and on the street, a situation brought about mainly by Zélies spite and vindictiveness, yet by the start of The Return of the Rat the couple are married.  Contemporary reviewers  noted that it seemed implausible in the extreme that the character of Pierre as previously portrayed would have sought charity from the very architect of his downfall, let alone married her.

The Return of the Rat is generally considered the weakest of the trilogy, consisting largely of recycled plot devices and set pieces from the earlier films.  Another weakness is cited as the jarring, and to all appearances random, interpolation of a pair of comedy Cockney characters whose presence in the Parisian setting seems highly incongruous and does not appear to serve any particular dramatic purpose in the film.  The films technical values are well-regarded however, with film historian Christine Gledhill citing it as "Cutts at the height of his visual powers". 

==Plot==
Pierre and Zélie are now married, but it is clear that the union has hit trouble.  The latest manifestation of Zélies restless, capricious nature is heavy gambling, to Pierres disapproval.  He nags her about her habit, but is ignored.  Having tamed Pierre, Zélie now finds herself bored with him and starts to flirt openly with the wealthy Henri (Bernard Nedell).  Her dismissive attitude towards Pierre, coupled with her continual desire to put him down and belittle him, finally causes him to leave her and return once again to his old haunt, the White Coffin Club.

The White Coffin is now ruled by Morel (Gordon Harker), and Pierre has to outwit him in order to reassert his dominance.  In the process he attracts the devotion of barmaid Lisette (Poulton).  Pierre and his followers hatch an elaborate scheme to humiliate Zélie and gain revenge on her for her transgressions.  Matters take a sinister turn when Zélie is murdered during a party she is throwing for her society friends to celebrate her new association with Henri.  The police believe they have evidence incriminating Pierre for the crime, and once again he finds himself pursued through the back streets of Paris by the forces of the law.  Pierre has to use all his wits and cunning to evade the police while finding evidence to prove the real identity of Zélies killer.

==Cast==
* Ivor Novello as Pierre Boucheron
* Isabel Jeans as Zélie de Chaumet
* Mabel Poulton as Lisette
* Gordon Harker as Morel
* Marie Ault as Mère Colline
* Bernard Nedell as Henri
* Harry Terry as Alf
* Scotch Kelly as Bill

==References==
 

== External links ==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 