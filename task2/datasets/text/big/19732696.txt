Wolf Devil Woman
{{Infobox film name = Wolf Devil Woman image = alt = caption = traditional = 狼女白魔 simplified = 狼女白魔}} director = Pearl Chang producer = Pearl Chang Alan Wu writer = Pearl Chang starring = music = Tseng Chung-ching cinematography = Chan Sin-lok editing = Wong Chau-kwai studio = distributor = released =   runtime = country = Taiwan language = Mandarin budget = gross =
}} Taiwanese fantasy horror film starring and directed by Pearl Chang.
 dubbed version of the film is notable for its bizarre vocal performances, particularly the vocal performance for the films antagonist, "The Devil", who speaks like American cartoon character Yosemite Sam. 

The film was followed by a 1983 sequel, Wolf Devil Woman 2, which also starred and was directed by Pearl Chang.

==Cast==
*Pearl Chang
*Shih Feng
*Shih Ying
*Wang Hsieh
*Pa Ke
*Yi Hsiao-man
*Chang Yu-hsiang
*Yeh Chao-hsu
*Wang Pei-yi
*He Hsing-nan
*Hu Chung
*Su Yuen-feng

==See also==
*Story of the White-haired Demon Girl
*White Hair Devil Lady
*The Bride with White Hair
*The Bride with White Hair 2
*The Romance of the White Hair Maiden (1986 TV series)
*The Romance of the White Hair Maiden (1995 TV series)
*Romance of the White Haired Maiden (1999 TV series)

==External links==
* 
* 
*   

 
 
 
 
 


 
 