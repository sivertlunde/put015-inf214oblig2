Terminal USA
Terminal USA(1993) is a controversial film by Jon Moritsugu that explores themes of family, drugs, violence and Asian American stereotypes. Its run time is 54 minutes and it is estimated to have had a budget of $365,000. 

== Beginnings and controversy ==
Terminal USA started out as an entry for a script competition for public broadcasting service|PBS. The script won $12,000 from that competition and the film was shown on television and created quite an uproar. "I was pushing the envelope trying to get crazy shit on TV. For a while we thought we were going to get a lawsuit. The producer said, ‘Lets work it. This is our publicity."(Director Jon Moritsugu)     When people were questioning the National Endowment for the Arts for funding projects considered controversial, Terminal USA was one of the films used as an example for one of the controversial projects funded.

Terminal USA was almost put on Japan/US flights for Northwest Airlines, but they decided against it at the last minute. 

==Festivals and awards==
*Museum of Modern Art, NYC
*Toronto Film Festival
*Rotterdam Film Festival
*Goteberg Film Festival
*Copenhagen Film Festival
*Viennale, Austria
*The Fringe Fest, Australia
*Musee dArt Moderne, Paris
*Institute of Contemporary Arts, London
*Yerba Buena Center for the Arts, SF
*Microcinefest, Baltimore
*SF Asian American Film Festival
 

== Credits ==
* Written and directed by Jon Moritsugu
 

=== Cast ===
* Sharon Omi - Ma
* Ken Narasaki - Dad
* Kenny Lang - Grandpa
* Jenny Woo - Holly
* Jon Moritsugu - Marvin/Katsumi
* Amy Davis - Eightball
* Victor of Aquitaine - Tom Sawyer the Lawyer
* Timothy Innes - Fagtoast
* Peter Friedrich - Rex
* Bonnie Dickenson - Sally
* Jacques Boyreau - Tabitha the Skinhead
* Gregg Turkington - Six
* Elizabeth Canning - Muffy
* Joshua Pollock - Pizzaboy
* Lady - Hot Lust Operator
* Issa Bowser - Buffy (Head Cheerleader)
* Kathleen Blihar - Cheerleader
* Tami Lipsey - Cheerleader
* Desi Del Valle

=== Crew ===
* Alberto Garcia - executive producer
* Marlina Gonzalez - executive producer
* Timothy Innes - associate producer
* James Schamus - executive producer
* Andrea Sperling - producer
* Brian Burman - original music	 	
* Michelle Handelman - original music	 	
* Sugarshock - original music
* Todd Verow - cinematography
* Gary Weimberg - film editor
* Jennifer M. Gentile - production design
* Jason Rail - hair stylist/make-up artist
* Tracy Thompson - production manager
* Erica Marcus - first assistant director
* Monte Cezazza - sound designer
* Michelle Handelman - sound designer
* Edwin Schenderlein - gaffer
* Elizabeth Canning - costumer
* Steve Bloom - first assistant editor
* Monte Cezazza - sound supervisor

== References ==
 

== External links ==
*  

 