Pavana Ganga
{{Infobox film|
| name = Pavana Ganga
| image = 
| caption =
| director = Y. R. Swamy
| writer = Krishnamoorthy Puranik (novel) Balakrishna  Ashok
| producer = C. Jayaram
| music = Rajan-Nagendra
| cinematography = R. Chittibabu
| editing = P. Bhaktavatsalam
| studio = Vijaya Vinayaka Enterprises
| released = 1977
| runtime = 137 minutes Kannada
| country = India
| budgeBold textt =
}}
 Ashok in lead roles. 

The film was a musical blockbuster with all the songs composed by Rajan-Nagendra considered evergreen hits.

==Plot==
Ganga is a village girl who runs away with her brother Narsimha because of her aunty mistreating her. She comes to Banglore in search of a job and after several years she falls in love with Annaya Narsimhas friend. Narsimsha gets married to Yamuna a young business tycoons daughter. Yamuna does not like Gangas interference in her married life and so asks her to leave the house. Will she leave the house or stay? Watch this movie to know more
== Cast ==
* Srinath 
* Aarathi  Ashok
* Dwarakish 
* B. V. Radha Balakrishna
* Sampath
* Musuri Krishnamurthy
* B. Jaya (actress)|B. Jaya
* M. N. Lakshmi Devi

== Soundtrack ==
The music was composed by Rajan-Nagendra with lyrics by Chi. Udaya Shankar.  All the songs composed for the film were received extremely well and considered as evergreen songs.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| music_credits = no
| collapsed = no
| title1 =  Aakasha Deepavu Neenu
| extra1 = S. P. Balasubramanyam, S. Janaki
| music1 = 
| length1 = 
| title2 = Aakasha Deepavu Neenu (sad)
| extra2 = S. Janaki
| music2 = 
| length2 = 
| title3 = Modalane Dinave
| extra3 = S. P. Balasubramanyam, S. Janaki
| music3 =
| length3 = 
| title4 = Hoovondu Beku Ballige
| extra4 = S. Janaki
| music4 =
| length4 = 
}}

== References ==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 