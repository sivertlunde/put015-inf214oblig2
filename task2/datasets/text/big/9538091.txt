Town of Evening Calm, Country of Cherry Blossoms
 
{{Infobox animanga/Header
| name            = Town of Evening Calm,   Country of Cherry Blossoms
| image           =  
| caption         = Cover of the first tankōbon edition of the manga, published by Futabasha
| ja_kanji        = 夕凪の街 桜の国
| ja_romaji       = Yūnagi no machi, Sakura no kuni
| genre           = Historical fiction|Historical, Drama
}}
{{Infobox animanga/Print
| type            = manga
| author          = Fumiyo Kōno
| publisher       = Futabasha
| publisher_en    =  
| demographic     = Seinen manga|Seinen
| magazine        = Weekly Manga Action
| first           = September 2003
| last            = July 2004
| volumes         = 1
| volume_list     = 
}}
{{Infobox animanga/Audio
| director        = 
| writer          = 
| studio          = 
| station         = NHK
| released        = 5 August 2006
| episodes        = 1
| episode_list    =
}}
{{Infobox animanga/Print
| type            = novel
| author          = Kei Kunii
| publisher       = Futabasha
| published       = 3 July 2007
}}
{{Infobox animanga/Video
| type            = live film
| title           = Yunagi City, Sakura Country
| director        = Kiyoshi Sasabe
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = 28 July 2007
| runtime         = 118 minutes
}}
 
 atomic bombing of Hiroshima. The author based the characters on people who were in Hiroshima or Nagasaki, Nagasaki|Nagasaki.

Town of Evening Calm, Country of Cherry Blossoms was adapted as a live-action film directed by Kiyoshi Sasabe released in 2007, called Yunagi City, Sakura Country in English. It has also been adapted as a novel by Kei Kunii and as a radio drama produced in 2006.

The manga has received international praise for its simple but beautiful artwork and its quiet but "humane" anti-war message.       It received the Grand Prize for manga at the 2004 Japan Media Arts Festival and the 2005 Tezuka Osamu Cultural Prize Creative Award. Kumiko Aso won several acting awards for her portrayal of Minami Hirano, one of the two protagonists, in the film adaptation.

==Plot==

Town of Evening Calm, Country of Cherry Blossoms comprises two stories, "Town of Evening Calm" and "Country of Cherry Blossoms".

===Town of Evening Calm (Yūnagi no Machi)===
 atomic bomb survived the bomb when she was in her early teens. She lives with her mother, Fujimi, in a shanty-town near downtown, having lost her father and two sisters to the bomb; her younger brother, Asahi, was evacuated to Mito, Ibaraki, where he was adopted by his aunt. Fujimi is a seamstress while Minami, who is "all thumbs" as her mother puts it, works as a clerk in an office, and together they are saving money for the trip to Mito to visit Asahi.
 flashback where she sees the victims of the bombing where they now stand and pushes him away. She runs off, still reliving the horrors of the attack, including finding her little sisters blackened body and watching her older sister die of radiation poisoning two months later. The next day at the office, she apologizes to Uchikoshi, saying she wants to somehow put the past behind her. She spends the day exhausted, however, and the next day becomes bedridden from the long-term effects of radiation poisoning. As Minamis condition worsens, she is visited by Uchikoshi and other co-workers, and dies just as her brother and aunt arrive.

===Country of Cherry Blossoms (Sakura no Kuni)===
 atomic bomb cherry blossom experience of spring that hes missing. Nanami is scolded by her grandmother for visiting Nagio when shes not supposed to. That summer, her grandmother, Asahis adoptive mother in Mito, dies, and that fall Nanami loses touch with Toko after her family moves closer to Nagios hospital.
 Peace Park. family grave, Peace Memorial Museum, and Nanami finds them a hotel room and cares for her, even though she has flashbacks to seeing her mothers last illness when she was a young child. In another flashback, Asahi proposes to a grown Kyoka, against his mothers wishes, as Kyoka was exposed to the bomb. On the ride back to Tokyo, Nanami arranges for Nagio to meet her and Toko, then leaves them alone together. Asahi tells Nanami he visited Hiroshima for the 49th anniversary of Minamis death, and that Nanami resembles her.

==Development==
  survivor of the atomic bomb, and growing up she found the subject upsetting and had tried to avoid it ever since. She decided to tackle the subject because she felt it was "unnatural and irresponsible for me to consciously try to avoid the issue." Living in Tokyo, she had come to realize that people outside of Hiroshima and Nagasaki didnt know about the effects of the bomb, not because they were avoiding the subject but because it is never talked about, and so she attempted the story because "drawing something is better than drawing nothing at all."   

Kōno described "Country of Cherry Blossoms" as "what I most needed to hear two years ago, when I still avoided anything to do with the atomic bomb." 

==Media==

===Manga===

Town of Evening Calm, Country of Cherry Blossoms was serialized in Japan by Futabasha in two parts in the seinen (aimed at younger adult men) manga magazine Weekly Manga Action in September 2003 and July 2004.  The two serial numbers were collected in a tankōbon edition published on 12 October 2004 (ISBN 4-575-29744-5).    It was reprinted in a bunkoban edition on 8 April 2008 (ISBN 978-4-575-71343-5).     
 Kana as Country of Cherry ( ),    in Portuguese by Editora JBC as Hiroshima - A City of Calm ( ),  in Spanish by Glénat (publisher)|Glènat España 

===Radio drama===
 radio drama broadcast by NHK on 5 August 2006.   It won the 2006 Agency for Cultural Affairs Art Festival Award in the Radio Division,  and was shortlisted for the 2007 Prix Italia. 

The drama was directed by Kenji Shindo and produced by Shinya Aoki, with the script adapted by Hirofumi Harada. Music was provided by Jun Nagao, and sounds were provided by Ai Sato. The three lead actors were Isao Natsuyagi, Tomoko Saito, and Kayu Suzuki.  Kenji Anan was a supporting cast member. 

;Cast     

* Isao Natsuyagi as Asahi Ishikawa
* Kayu Suzuki as Nanami Ishikawa
* Tomoko Saito as Minami Hirano
* Kenji Anan as Yutaka Uchikoshi
* Toshie Kobayashi as Fujimi Hirano
* Shin Yazawa as Toko Tone
* Kenta Chabana as younger Asahi Ishikawa
* Yuna Mimura as younger Kyoka Ota
* Yukie Ito as Kyoka Ota
* Kanako Obayashi as Sachiko Furuta

===Novel===

A novelization by  , released in conjunction with the film adaption with a cover featuring the films actors, was published by Futabasha on 3 July 2007 (ISBN 978-4-575-23582-1).   

===Film===

Town of Evening Calm, Country of Cherry Blossoms has been adapted into a live-action film directed by Kiyoshi Sasabe,    which is called Yunagi City, Sakura Country in English.  The outdoor sets were filmed in Kawaguchi, Saitama, including a recreation of part of the Hiroshimas shantytown c. 1958.    The story of "Country of Cherry Blossoms", filmed in every place in Hiroshima.    The film premiered at the Cannes Film Festival in May 2007,    then released in theaters in Japan on 28 July 2007.  It was released on DVD in Japan on 28 March 2008.     A soundtrack was also released.   

;Key items

* An attractive handmade short-sleeved dress: there is the reason that Minami cant wear the short-sleeves.
* The white barrette (hair clip): Minamis father gave white one to Minamis sister Midori and red one to Minami, and soon Midori liked red one so Minami keep the white one. And the barrette will be kept by her sister in law Kyoka and her niece Nanami.
* The river and an old water tank for fire fighting on the street: People at the time wanted water.
* A white handkerchief with two red goldfish: The family used live in a house with a pond in the yard. Asahi tried to catch the goldfish and was scolded by father.
* A family picture: Only one Minamis family picture is left with Asahi in Mito, but all pictures were burn out with Minamis house.
* An acacia-tree on the river side: Asahi and Yutaka will meet under the acacia-tree. People in Hiroshima, they take care left standing a-bombed trees now, because Hiroshima was called "the city where no plants can be grown for seventy-five years".
* The name of the characters: Their names come from the name of the towns in Hiroshima. (Nagio from Yūnagi(Evening Calm)).
* Dialects: Minami, Fujimi, Kyoka and Yutaka speaks Hiroshima dialect. Asahi speaks Mito dialect. Nanami, Nagio and Toko speaks Tokyo dialect.

;Cast

* Kumiko Aso as Minami Hirano, the heroine "Town of Evening Calm".  Minami lives in Hiroshima with her mother Fujimi. She wears a white barrette, a memento of her father and sister. Minami eventually dies as a result of radiation sickness.
* Rena Tanaka as Nanami Ishikawa, Minamis niece and the heroine of "Country of Cherry Blossoms". Nanami grew up in Tokyo and speaks the Tokyo dialect, unlike the Hiroshima-dwelling characters of Town of Evening Calm who speak in the local dialect. Throughout the story, Nanami comes to know the family history of her relatives in Hiroshima.
* Mitsunori Isaki as the younger Asahi Ishikawa, Minamis little brother. Asahi lives with his aunt in Mito, where he was raised, and thus speaks in the Mito dialect. He and his aunt lives far away from Minami and her mother, only arriving in Hiroshima to visit as her illness finally claims her life. After Minamis death, he decides to stay with his mother Fujimi in Hiroshima and attends Hiroshima University.
* Masaaki Sakai as the adult Asahi Ishikawa, Minamis brother and Nanamis father. When Nanami follows him, she discovers he has returned to Hiroshima for the fifteenth anniversary of Minamis death.
* Yû Yoshizawa as the younger Yutaka Uchikoshi, Minamis coworker who falls in love with her. She refuses his affections, but he stays by her side as she dies of radiation sickness.
* Ryosei Tayama as the adult Yutaka Uchikoshi. Yutaka and Asahi meet under the acacia tree when Asahi returns to Hiroshima.
* Shiho Fujimura as Fujimi Hirano, Minamis mother and Nanamis grandmother, who appears in both stories.
* Yuta Kanai as Nagio Ishikawa, Nanamis younger brother, who works as an intern at a hospital. He has feelings for Toko.
* Noriko Nakagoshi as Toko Tone, Nanamis childhood friend who works as a nurse in the same hospital as Nagio.
* Rina Koike as the young Kyoka Ota, a girl from Minamis neighborhood who sometimes comes over to help out around the house.
* Urara Awata as Kyoka Ota, Nanamis mother. She and Asahi move from Hiroshima to Tokyo after their marriage.
* Asami Katsura as Sachiko Furuta, Minamis coworker who helps her make a copy of the dress in the shop window.

;Soundtrack

The soundtrack of the film was released  on 16 July 2007. It is composed by Takatsugu Muramatsu, with performances on the harp by Naori Uchida and harmonica by Masami Ohishi.

==Reception==
The story was praised by the award jury of the Japan Media Arts Festival for its brevity and its "depiction of the dark shadow of war".    According to Izumi Evers, one of the editors of the English edition, the Japanese edition created a sensation despite being little-promoted because it made readers want to talk about the bombing of Hiroshima, which is a controversial topic.  The manga sold over 180,000 copies in Japan. 

The simple artwork and storys "light, ephemeral touch" was praised by   called the work "a beautiful manga and an understated antiwar statement", praising its "dreamlike and evocative" stories and its artwork, especially the "lovely" backgrounds.   

Publishers Weekly named the English translation one of best ten manga of 2007, calling it "too important to pass up" and comparing it to Barefoot Gen,  and New York Magazine named it one of the top five comics in English of 2007.  The American Library Association placed the manga on its list of "Great Graphic Novels for Teens 2008".  It was nominated for an Eisner Award in two categories, Best Short Story for "Town of Evening Calm" and Best U.S. Edition of International Material—Japan,  but lost to "Mr. Wonderful" by Dan Clowes and Tekkonkinkreet respectively.  

According to Patrick Macias, an English editor of the manga, the film was "kind of ignored" at Cannes because of competition from other high-profile Japanese films.  According to a review in Asia Pacific Arts, during the sequence when the father returns to Hiroshima, the film shows "an interaction" of the past, present, and future, which is indicative of "the slipperiness of memory", a technique that "makes more visible and effective the ongoing resonance of the past in the present" but that "rejects the politics of the bombing" of the first part of the film.    Kumiko Aso won several acting awards in Japan for her role as Minami Hirano.   

===Awards===
 Tezuka Osamu Cultural Prizes Creative Award.   

The film won several awards, including the 17th Japanese Film Critics Award for Best Picture,  placing sixth in the top ten films list of the 3rd Osaka Cinema Festival,  and placing ninth in the top ten domestic films list of 2007 by Kinema Junpo.  The Blue Ribbon Awards also named the film one of the top ten films of the year.    Kumiko Aso won several "Best Actress" awards for her portrayal of Minami Hirano in the film, including that of the 32nd Hochi Film Awards,    the 62nd Mainichi Film Awards,    and the 50th Blue Ribbon Awards. 

==References==
 

==External links==
*    
*    
*  
*    
*    
*    
*    
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 