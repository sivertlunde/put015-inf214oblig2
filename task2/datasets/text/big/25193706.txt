The Bronze Buckaroo
{{Infobox film
| name           = The Bronze Buckaroo
| image          = The Bronze Buckaroo.jpg
| image_size     =
| caption        =
| director       = Richard C. Kahn
| producer       = Richard C. Kahn Jed Buell
| writer         = Richard C. Kahn (original story) Richard C. Kahn (screenplay)
| narrator       =
| starring       = See below
| music          = Lew Porter
| cinematography = Roland Price Clark Ramsey
| editing        =
| studio         = Hollywood Productions
| distributor    = Sack Amusements
| released       = 1 January 1939
| runtime        = 58 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Bronze Buckaroo is a 1939 American film directed by Richard C. Kahn. It is one of a number of race films, made by African-American directors and performers for African-American audiences. The Bronze Buckaroo stars black cowboy singer Herb Jeffries, here billed as Herbert Jeffrey.

==Plot==
Cowboy Bob Blake receives a letter from his friend Joe Jackson, asking for help. Blake and his men travel to Jacksons ranch, only to discover from Jacksons sister Betty that Joe has been missing for three weeks. Meanwhile Jacksons ranch hand (Slim Perkins) is learning to use ventriloquism to make the farm animals talk, and tries to convince the gullible Dusty to buy a talking mule. 
 
Blake discovers that Jackson is being held by a local land grabbing rancher, Buck Thorne, who (with his partner Pete) has discovered gold on Jacksons ranch. They killed Joe and Bettys father, and are trying to force Joe to deed the land over to Thorne. Blake develops a plan to rescue Jackson from where he is being held above the saloon, but runs into trouble. Betty sends Blakes men into the saloon as backup and is kidnapped by Thorne, who then threatens to kill Betty and Joe if they do not sign the deed. While Dusty rides for the sheriff, Blake and his men backtrack Bettys horse (who arrived home riderless). A gun battle ensues, with the sheriff arriving in the nick of time. The villains are hauled off to jail, and Blake rides into the sunset with Betty.

==Cast==
*Herb Jeffries as Bob Blake
*Artie Young as Betty Jackson
*Rollie Hardin as Joe Jackson
*Clarence Brooks as Buck Thorne
*F. E. Miller as Slim Perkins
*Lucius Brooks as Dusty Spencer Williams as Pete
*Lee Calmes as Lee
*Earle Morris as Bartender
*The Four Tones as Singing Quartet

==Soundtrack==
* Herb Jeffries - "Im a Happy Cowboy"
* The patrons - "Almost Time for Roundup"
* Herb Jeffries and The Four Tones - "Got the Payday Blues"
* The Four Tones - "Get Along Mule"

==External links==
* 
* 

 

 
 
 
 
 
 
 