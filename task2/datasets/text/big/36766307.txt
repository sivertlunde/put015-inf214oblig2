Born Reckless (1937 film)
{{Infobox film
| name           = Born Reckless
| image          = 
| caption        =  Malcolm St. Clair Gustav Machatý
| producer       = 
| writer         = Jack Andrews (story) John Patrick (screenplay) Robert Ellis (screenplay) Helen Logan (screenplay) Robert Kent Harry Carey Pauline Moore Chick Chandler William Pawley Francis McDonald George Walcott Joseph Crehan
| cinematography = Daniel B. Clark
| editor         = Alex Troffey
| released       = June 25, 1937
| distributor    = Twentieth Century Fox
| runtime        = 59 minutes English
| budget         =  
}} 1937 mob gangster film Malcolm St. Clair and Gustav Machatý (St. Clair received sole directorial credit) and starring Brian Donlevy and Rochelle Hudson. Donlevy plays a race-car champion who infiltrates a mob-run taxi cab company. Barton MacLane plays the chief mobster.

==Production== Fox studio Union Station in downtown Los Angeles. During filming, Donlevy injured his hand, necessitating that two fingers be bandaged. The bandaged hand had to be concealed throughout filming, though it is visible at the end of the film.  Former stars of the silent era Jack Mulhall, Joyce Compton and Mary McLaren have uncredited bit parts.

Union Station did not open until 1939.

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 

 