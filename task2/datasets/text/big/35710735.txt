The Girl Who Wouldn't Work
{{infobox film
| name           = The Girl Who Wouldnt Work
| image          =
| imagesize      =
| caption        =
| director       = Marcel De Sano
| producer       = B. P. Schulberg
| writer         = Lois Hutchinson (scenario)
| based on       =  
| starring       = Lionel Barrymore Marguerite De La Motte Henry B. Walthall
| cinematography = Allen Siegler
| editing        =
| distributor    = Preferred Pictures
| released       =  
| runtime        = 6 reel#Motion picture terminology|reels; 5,979 feet
| country        = United States
| language       = Silent film English intertitles
}} silent film produced by B. P. Schulberg and starring Lionel Barrymore and Marguerite De La Motte. Preferred Pictures and Al Lichtman handled the distribution of this film directed by one Marcel De Sano. It is preserved at the Library of Congress.   

==Cast==
*Lionel Barrymore - Gordon Kent
*Marguerite De La Motte - Mary Hale
*Henry B. Walthall - William Hale
*Lilyan Tashman - Greta Verlaine
*Forrest Stanley - William Norworth
*Winter Hall - District Attorney
*Tom Ricketts - The Rounder

==See also==
*Lionel Barrymore filmography

==References==
 

==External links==
* 
* 
*  (*if page doesnt download simply click-> the   link then return and click)

 
 
 
 
 
 
 


 