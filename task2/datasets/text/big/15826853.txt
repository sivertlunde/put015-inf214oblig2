Banana Joe (film)
{{Infobox film
| name           = Banana Joe
| image_size     = 
| image	=	Banana Joe FilmPoster.jpeg
| caption        = 
| director       = Stefano Vanzina
| producer       =
| writer         = Mario Amendola Bruno Corbucci Bud Spencer Stefano Vanzina
| narrator       = 
| starring       = Bud Spencer
| music          = Guido De Angelis Maurizio De Angelis
| cinematography = Luigi Kuveiller
| editing        = Raimondo Crociani
| distributor    =  1982
| runtime        = 96 minutes
| country        = Italy West Germany
| language       = Italian
| budget         = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1982 Italy|Italian-Germany|German action movies|action-comedy film starring Bud Spencer.

==Plot==
Bud Spencer plays Banana Joe, a brawny yet friendly man who lives in a small rainforest village called Amantido with a huge number of his own children and regularly delivers bananas to a South American river port (hence his name). One day, the henchmen of a local gangster boss named Torsillo come ashore in Amantido to initiate the construction a banana processing plant. Of course, Joe (in typical direct-approach manner) evicts the goons, who promptly return to their boss.
 con man named Manuel, who has a gift of the gab which has placed him in favor in several high positions, even with the countrys President for his help in developing a remedy for a genetic defect in the Presidential family.

Eventually Joe meets Dorianne, an attractive singer, in a bar owned by Torsillo, where he gets a temporary job as a bouncer. Torsillo also runs into Joe personally time and again, and after having seen Joe easily finishing off five of his toughest goons, the gangster boss has taken to jumping out of the next window in a panic the instant he lays an eye on Joe.

In pursuit of his license, Joe finds out that he must get himself registered with the authorities in order to "exist" legally. Since he has no proper official records, however, this proves highly difficult, and the constrictions and loop-holes of bureaucracy provide no help in resolving the matter. Joe even has to enlist in the Army, but after driving his drill sergeant to the point of despair and even to degradation, he deserts and lands himself in prison when in his impatience he takes matters (literally) into his own hands. In prison he re-encounters Manuel, whom he intends to pay back for his schemes, but it turns out that the con man actually has a heart of gold: Having taken pity on Joes plight, he has used his connections to get Joe the much-needed license. From Dorianne, who visits him in prison, Joe learns that Torsillo has used his absence to facilitate the construction of the banana plant.

Joe and Manuel promptly break out of prison and return to Amantido, where Joe proceeds to thrash Torsillos thugs and a newly built casino. Afterwards the police arrive, but not to arrest Joe. Instead theyve been looking for Manuel: his remedy he had concocted for the President has worked, and Manuel (and Joe as well) receive amnesty as well as the fulfilment of a wish (Manuel decides to become Minister of Finance). To top it off, Torsillo is revealed to be no stranger to the authorities, and he and his entourage get arrested immediately. Dorianne decides to stay with Joe in Amantido, where she opens a school which Joe also attends, and Joes life goes otherwise back to normal.

==Cast==
*Bud Spencer: Banana Joe
*Marina Langner: Dorianne
*Mario Scarpetta: Manuel
*Gianfranco Barra: Torsillo
*Enzo Garinei: Eng. Moreno
*Gunther Philipp: the Tailor 
*Giorgio Bracardi: Sergeant Josè Felipe Maria Martiño
*Nello Pazzafini: The Torsillos truck driver

== Production ==
*The script to this movie was written by Bud Spencer himself, under his civilian name Carlo Pedersoli.
*Parts of the movie were filmed in Cartagena, Colombia, and it included extras from Cartagena.

==External links==
*   
 

 
 
 
 
 
 