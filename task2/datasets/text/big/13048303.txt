Pyaar Ka Mandir
{{Infobox film
| name           = Pyaar Mandir
| image          = PyarKaMandir.jpg
| image_size     = 
| caption        = Vinyl Record Cover
| director       = K. Bapaiah
| producer       = Shabnam Kapoor
| writer         = 
| narrator       =  Madhavi Raj Raj Kiran Shoma Anand Kader Khan Aruna Irani
| music          = Laxmikant Pyarelal  Anand Bakshi   (Lyrics)  
| cinematography =
| editing        = 
| distributor    =
| released       = 1 April 1988
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Indian Bollywood Raj Kiran, Shoma Anand, Kader Khan and Aruna Irani.

==Synopsis== IAS and law respectively.

Meena is to be married to a young man named Satish, and Laxmi goes to withdraw her chit fund from Gopal Khaitan, only to find out that he has duped her. The devastated family gather to confront this, only to have their mom lose her teaching job. Vijay swears to apprehend Gopal, a confrontation ensues, and Gopal is killed. In this way Vijay gets the money so that Meenas wedding takes place, while his brothers continue to study. Vijay is arrested, found guilty and sentenced to seven years in prison.

After his discharge, Vijay returns home to find that their family home has been sold. Ajay, who is now the collector, has married Sapna, a very wealthy woman. They have a daughter, Geeta, and are living in a government bungalow; Sanjay is now a lawyer and is living in a palatial home. Both brothers reject Vijay and have nothing to do with him. Devastated he starts driving an auto-rickshaw for a living, hoping that he meets with his mother, who, as he has been told by his brothers, is away on a religious pilgrimage. Vijay does meet his mom, who is working as a common laborer at a construction site, and confronts his brothers. The truth will be revealed as to why Vijay confessed to killing Gopal.

==Cast==
*Mithun Chakraborty ... Vijay  Madhavi ... Radha  Raj Kiran ... Ajay  Sachin ... Sanjay
*Shoma Anand ... Sapna 
*Kader Khan ... Dr. Bhuljaanewala 
*Aruna Irani ... Shanti 
*Asrani
*Bharat Kapoor ... Gopal Seth 
*Shakti Kapoor ... Dilip 
*Raza Murad ... Adam Khan 
*Nirupa Roy ... Mother
*Dhumal (actor)|Dhumal... Patient

==Music==
*Aye Duniya Tujhko Salam - Kishore Kumar
*Jhopad Patti Zindabad - Kishore Kumar
*Log Jahan Per Rahate Hain - Mohammad Aziz, Suresh Wadkar, Kavita Krishnamurthy, Udit Narayan
*O Meri Jaan Meri Jaan - Mohammad Aziz, Alka Yagnik 
*Pyar Ke Pahle Kadam Pe - Kishore Kumar, Alka Yagnik

==External links==
* 

 
 
 
 