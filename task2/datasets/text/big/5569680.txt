Tonight for Sure
{{Infobox film
| name           = Tonight for Sure
| image          = Tonight for Sure.jpg Francis Coppola
| producer       = Francis Coppola
| writer         = {{Plainlist |
* Francis Coppola
* Jerry Shaffer
}}
| starring       = {{Plainlist |
* Don Kenney
* Karl Schanzer
}} Carmen Coppola
| cinematography = Jack Hill
| editing        = Ronald Waller
| studio         = Searchlight Productions
| distributor    = Premier Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
}} 1962 Western Western Softcore softcore Comedy comedy film Carmen Coppola. short Sexploitation sexploitation film of Coppola), and an unfinished Western set in a nudist colony.

==Plot==
On the Sunset Strip, two unlikely men rendezvous: Samuel Hill, an unkempt desert miner, and Benjamin Jabowski, a John Birch Society dandy from the city. Intent on some sort of mayhem, they enter the Herald Club before the burlesque show starts, and they wire something to the electrical box, set to blow at midnight. They sit at the back of the club to get to know each other. As they drink and glance at the stage, Sam tells of a partner driven mad by visions of naked women in the sagebrush; Ben tells a tale of trying to rid his neighborhood of a pin-up studio. As they get drunker and the clock ticks toward midnight, they pull their chairs closer to the women on stage.

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 

 
 