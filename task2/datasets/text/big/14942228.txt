Another Cinderella Story
{{Infobox film
| name           = Another Cinderella Story
| image          = AAnotherCinderellaStory.jpg
| caption        = DVD cover
| director       = Damon Santostefano
| producer       = Dylan Sellers
| writer         = Erik Patterson Jessica Scott
| starring       = Selena Gomez Drew Seeley Jane Lynch Emily Perkins Katharine Isabelle Jessica Parker Kennedy Marcus T. Paulk Nicole LaPlaca
| music          = John Paesano
| cinematography = Jon Joffin
| editing        = Tony Lombardo
| studio         = Dylan Sellers Productions
| distributor    = Warner Premiere
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
}}
Another Cinderella Story (also known as A Cinderella Story 2) is a 2008   in 2011.

The film premiered on ABC Family on January 18, 2009 and was ranked as the number one cable movie in several key demographics when aired on the television channel.  The film won the 2010 Writers Guild of America Award for Childrens Script-Long or Special. 

==Plot==
This film is a retelling of the Cinderella fairy tale in a modern setting, with Mary Santiago (Selena Gomez), a high school student with ambitions of becoming a dancer, taking the role of Cinderella; Tami (Jessica Parker Kennedy) Marys only friend as her fairy godmother; Dominique Blatt (Jane Lynch) taking the role of the stepmother; Britt (Emily Perkins) and Bree (Katharine Isabelle) as the two stepsisters; and Joey Parker, (Drew Seeley), now a famous celebrity that has returned to school for his senior year and to remember why he started dancing, as the prince.  A school dance substitutes for the ball, with the role of the glass slipper filled by a Zune.  

==Cast==
* Selena Gomez as Mary Santiago.  Marys mother was a dancer and died when she was young. Dominique and her two daughters took her in, made her their maid, and treat her horribly.
** Nicole Muñoz as Mary,  age 11 
** Mia Aida Duran as Mary,  age 3 
* Drew Seeley as Joey Parker.  Joey is a famous pop star and is taking a break from being on tour in order to finish high school.  He falls in love with Mary after they dance at the ball. 
* Jane Lynch as Dominique Blatt.  Dominique is a washed up pop star and took in Mary after her mother died and makes Mary be her maid.
* Emily Perkins as Britt Blatt.  Britt is Dominiques daughter.  She has braces and often fights with her sister Bree.
* Katharine Isabelle as Bree Blatt.  Bree is Dominiques daughter. 
* Jessica Parker Kennedy as Tami.  Tami is an aspiring fashion designer and Marys best friend.
* Marcus T. Paulk as Dustin.  Dustin is Joeys best friend and manager.  He starts a relationship with Tami and is a skilled dancer and rapper.
* Nicole LaPlaca as Natalia Faroush.  Natalia is Joeys ex-girlfriend who is still not over him and wants to get back together.

==Production==
The film was shot in Vancouver, British Columbia, Canada from November to December 2007.  The school scenes took place  at Sullivan Heights Secondary in Surrey, British Columbia. 

==Reception==
===Critical response===
Amber Wilkinson of Eye for Film gave the film, four out of five stars and praised the musical aspects, saying that "the song and dance numbers are so well-handled and catchy, its a shame there arent more of them."  However, she also said that the "characters are so wafer thin they barely cast a shadow."    While Wilkinson says that the film is completely different from A Cinderella Story, Lacey Walker, reviewing for Christian Answers, notes several aspects of the two films that were directly parallel to each other.  Walker also gave it three out of five stars, praising the script, saying the writers "peppered this story with a surprising dose of humor and some pleasing plot twists."  However, Walker specifically criticized the "glaringly obvious" age difference between the 15 year old Gomez and the 25 year old Seeley. 

===Ratings===
ABC Family presented the television premiere of the film on January 18, 2009.   The premiere was watched by 5.3 million viewers .

==Soundtrack==
 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 