Those Who Love Me Can Take the Train
{{Infobox film
| name           = Those Who Love Me Can Take the Train  (Ceux qui maiment prendront le train)
| image          = 
| image_size     = 
| caption        = 
| director       = Patrice Chéreau
| producer       = Charles Gassot Jacques Hinstin
| writer         = Danièle Thompson Patrice Chéreau Pierre Trividic
| narrator       = 
| starring       = Pascal Greggory
| music          = 
| cinematography = Eric Gautier
| editing        = François Gédigier
| distributor    = 
| released       = 15 May 1998
| runtime        = 122 minutes
| country        = France
| language       = French
| budget         = 
}} 1998 French French drama film directed by Patrice Chéreau and 
written by Chéreau, Danièle Thompson and Pierre Trividic. It stars Pascal Greggory, Vincent Perez, Charles Berling and Dominique Blanc.

==Plot==
The film follows the friends of a recently deceased minor painter Jean-Baptiste Emmerich as they take a train from Paris to Limoges, where he is to be buried, attend his funeral, then gather at the home of his twin brother, Lucien. The mourners include François, who spends the journey listening to a series of taped conversations with the painter; Jean-Marie and Claire, a couple whose marriage has broken down; Emmerichs former lover Lucie;  Louis, a close friend of François, and Bruno a young man with whom he has fallen in love. As the train heads south, the travellers watch the car carrying Emmerichs coffin being driven recklessly alongside the train by their friend Thierry. 

At the funeral Jean-Marie makes a speech condemning  family life, and declares, to Claires anger, that he will never become a father. At the gathering after the funeral the guests argue about  which of them was closest to Emmerich. Claire discovers that a young woman present, Viviane, was actually Emmerichs son Frédéric, who has become a woman.   

==Background and filming==
The inspiration for the film, and its title, came from a request made by the documentary film-maker François Reichenbach to those attending his funeral. 

The sequences on the train were filmed over 14 days in two carriages on trains running between Paris and Mulhouse. Interviewed in The Guardian, Patrice Chereau said "You cannot really fabricate the movement of a train in a studio - the actors and the camera moving at the same time. We needed to have the real energy of that journey".    Reviewing the film for Sight & Sound, Chris Darke said "the journey to Limoges is a triumph both of exposition and choreography.....Éric Gautiers use of handheld Scope cinematography gives the feeling of both buffeting movement and swooping detail." 

==Cast==
* Pascal Greggory – François
* Valeria Bruni Tedeschi – Claire (as Valeria Bruni-Tedeschi)
* Charles Berling – Jean-Marie
* Jean-Louis Trintignant – Lucien Emmerich / Jean-Baptiste Emmerich
* Bruno Todeschini – Louis
* Sylvain Jacques – Bruno
* Vincent Perez – Viviane
* Roschdy Zem – Thierry
* Dominique Blanc – Catherine
* Delphine Schiltz – Elodie
* Nathan Kogen – Sami (as Nathan Cogan)
* Marie Daëms – Lucie
* Chantal Neuwirth – Geneviève
* Thierry de Peretti – Dominique
* Olivier Gourmet – Bernard

==Awards and nominations==
*British Independent Film Awards (UK)
**Nominated: Best Foreign Language Film

*1998 Cannes Film Festival (France)
**Nominated: Golden Palm (Patrice Chéreau)   

*24th César Awards (France)
**Won: Best Actress &ndash; Supporting Role (Dominique Blanc)
**Won: Best Cinematography (Eric Gautier)
**Won: Best Director (Patrice Chéreau)
**Nominated: Best Actor &ndash; Leading Role (Pascal Greggory)
**Nominated: Best Actor &ndash; Supporting Role (Vincent Perez)
**Nominated: Best Actor &ndash; Supporting Role (Jean-Louis Trintignant)
**Nominated: Best Editing (François Gédigier)
**Nominated: Best Film 
**Nominated: Best Production Design (Sylvain Chauvelot and Richard Peduzzi)
**Nominated: Best Sound (Guillaume Sciama and Jean-Pierre Laforce)
**Nominated: Best Writing (Patrice Chéreau, Danièle Thompson and Pierre Trividic)

*Étoiles dOr (France)
**Won: Best Actor &ndash; Leading Role (Charles Berling)
**Won: Best Director (Patrice Chéreau)

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 