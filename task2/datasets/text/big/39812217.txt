The Man with the Frog
{{Infobox film
| name           = The Man with the Frog
| image          =
| image_size     = 
| caption        = 
| director       = Gerhard Lamprecht
| producer       = 
| writer         = Luise Heilborn-Körbitz   Gerhard Lamprecht 
| narrator       =  Hans Junkermann   Evelyn Holt   Walter Rilla
| music          = Artur Guttmann   
| editing        = 
| cinematography = Karl Hasselmann
| studio         = Gerhard Lamprecht Filmproduktion  
| distributor    = National-Film
| released       = 4 February 1929
| runtime        = 80 minutes
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent crime Hans Junkermann and Evelyn Holt.  The films art direction was by Otto Moldenhauer.

==Cast==
* Heinrich George as Der Mann mit dem Laubfrosch  Hans Junkermann as Kunsthändler Bruneaux 
* Evelyn Holt as Yvette Bruneaux seine Tochter 
* Walter Rilla   
* Olga Limburg as Lou 
* Harry Nestor as René 
* Alexander Murski as Henris Vater 
* Julia Serda   
* Hugo Werner-Kahle as Polizei Präfekt 
* Rudolf Biebrach as Kommissar 
* Karl Hannemann as Hausdiener 
* Maria Forescu as Zimmermädchen 
* Hilde Schewior as  Zimmermädchen 
* Johannes Bergfeldt as Zimmerkellner 
* Gerhard Bienert   
* Heinrich Gretler   
* Franz Schönfeld   
* Wolfram Anschütz  
* Erika Dannhoff  
* Harry Grunwald

==References==
 

==Bibliography==
* Caneppele, Paolo. Entscheidungen der Wiener Filmzensur: 1929-1933. Film-Archiv Austria, 2003.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 