La trattativa
{{Infobox film
| name           = La trattativa
| image          = La trattativa.jpg
| caption        =
| director       = Sabina Guzzanti
| producer       = 
| writer         = Sabina Guzzanti 
| starring       = Sabina Guzzanti 
| music          = 
| cinematography = Daniele Ciprì
| editing        = 
| studio         = Secol Superbo e Sciocco Produzioni QMedia
| distributor    = BIM Distribuzione 
| released       =  
| runtime        = 108 minutes
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}

La trattativa is a 2014 Italian satirical film written and directed by Sabina Guzzanti, who also starred in the film. It has been selected to be screened out of competition at the 71st Venice International Film Festival.   

== Cast ==
* Sabina Guzzanti
* Enzo Lombardo
* Ninni Bruschetta
* Filippo Luna
* Franz Cantalupo
* Claudio Castrogiovanni

== References ==
 

== External links ==
*  
*   

 
 
 
 
 
 
 

 