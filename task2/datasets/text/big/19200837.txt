Crazylegs (film)
{{Infobox film
| name           = Crazylegs
| image_size     =
| image	         = Crazylegs FilmPoster.jpeg
| caption        =
| director       = Francis D. Lyon
| producer       = Hal Bartlett
| writer         = Hal Bartlett
| narrator       = Elroy Crazylegs James Brown John Brown Bill Brundige Win Hirsch Melvyn Arnold
| music          = Leith Stevens
| cinematography = Virgil Miller
| editing        = Cotton Warburton
| distributor    =
| released       =  
| runtime        = 87 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Crazylegs is a 1953 film about Elroy Hirschs football career. In college (University of Wisconsin and University of Michigan) his unconventional dynamic running style allowed him to change directions in a multitude of ways. The media dubbed him "Crazylegs". The name stuck all through his professional career and life. The bulk of this film is centered on his college days.  One of the highlights of the film is when his Michigan team was informed it would play in the Rose Bowl on Jan 1, 1948. At that time, the Rose Bowl chose various teams from non-Western conferences to play their Pacific Coast Conference (Now the Pac-10) Champion.  That game was the first of the long relationship pitting the two champions of the Big 10 and Pacific Coast conferences in the Rose Bowl. Crazylegs later became part of the foundation of the "Three End" with the LA Rams.  This film captures the genuine quality of Hirsch’s personality, with Hirsch playing himself in the part.

The film premiered in Wausau, Wisconsin, Hirschs hometown. Cotton Warburtons editing was nominated for the Academy Award for Best Film Editing.

==External links==
*  

 
 
 
 
 


 
 