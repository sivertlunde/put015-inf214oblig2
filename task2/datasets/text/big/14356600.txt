Qivitoq
{{Infobox film
| name           = Qivitoq
| image          = Qivitoqposter.jpg
| caption        = 1956 Movie Poster by Kai Rasch
| director       = Erik Balling
| producer       = 
| writer         = Leck Fischer
| starring       = Poul Reichhardt Astrid Villaume
| music          = Svend Erik Tarp
| cinematography = Poul Pedersen
| editing        = Carsten Dahl
| distributor    = Nordisk Film| Nordisk Film Kompagni
| released       = 1956
| runtime        = 118 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Qivitoq is a 1956 Danish film directed by Erik Balling and starring Poul Reichhardt and Astrid Villaume. An intense romantic drama, the movie was filmed entirely on location in Greenland. It was nominated both for the Academy Award for Best Foreign Language Film    and for the Palme DOr at the 1957 Cannes Film Festival.   

==Plot==
A young teacher, Eva Nygaard (Astrid Villaume), arrives in Greenland from Denmark to surprise her fiance, the Doctor Erik Halsøe, but is crushed to find he has not waited for her and he is about to be married to his assisting nurse. Eva travels to a small fishing village to await the next ship back to Denmark. There she enters into a tense and often confrontational relationship with Jens (Poul Reichhardt), a quiet moody Dane who manages a trading company outpost. Meanwhile, Jens is trying to persuade a Greenlander named Pavia (Niels Platou) to become a company fisherman, despite Pavias fear of alienating his fellow villagers and upsetting the spirit, Qivitoq. After Eva helps Jens treat a boy who has been attacked by dogs, Jens opens to her and realizes he loves her. Before he can tell her, he learns that Pavia in his new boat is in danger. Jens and Pavias girlfriend, Naja (Dorthe Reimer), travel overland to rescue him, but Jens falls into an icy crevice and is rescued by Pavia instead. In this way, Pavia returns to a heros welcome in the village. Jens hastens back only to discover he is too late, that the boat that Eva had been awaiting has already sailed. Dejected, Jens goes home and discovers Eva is there waiting for him.

==Cast==
*Poul Reichhardt as Jens Lauritzen
*Astrid Villaume as Eva Nygaard
*Gunnar Lauring as Marius Mariboe
*Randi Michelsen as Fru Mariboe
*Bjørn Watt-Boolsen as Dr. Erik Halsøe
*Kirsten Rolffes as Sygeplejerske Kirsten Prage
*Niels Platou as Pavia
*Dorthe Reimer as Naja
*Justus Larsen as Nuka
*Johanne Larsen as Cæcilie
*Edward Sivertsen as Zakarias

==See also==
* List of submissions to the 29th Academy Awards for Best Foreign Language Film
* List of Danish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 