Gaucho Serenade
{{Infobox film
| name           = Gaucho Serenade 
| image          = Gaucho_Serenade_Poster.jpg
| border         = yes
| caption        = Theatrical release poster Frank McDonald
| producer       = William Berke
| screenplay     = {{Plainlist|
* Betty Burbridge
* Bradford Ropes
}}
| starring       = {{Plainlist|
* Gene Autry
* Smiley Burnette
* June Storey
}}
| music          = Raoul Kraushaar (supervisor)
| cinematography = Reggie Lanning
| editing        = Tony Martinelli
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 67 minutes Magers 2007, p. 162. 
| country        = United States
| language       = English
| budget         = $77,885 Magers 2007, p. 163. 
| gross          = 
}} Western film Frank McDonald and starring Gene Autry, Smiley Burnette, and June Storey. Written by Betty Burbridge and Bradford Ropes, the film is about a singing cowboy who goes up against a group of businessmen who plot to kidnap the son of a former partner so he wont testify against them.    

==Plot==
Down and out rodeo stars Gene Autry (Gene Autry) and Frog Millhouse (Smiley Burnette) leave New York City for California with their car and horse trailer. Along the way they discover young Ronnie Willoughby (Clifford Severn), a school boy from London, has accidentally stowed away in their car. Having just arrived from London by steamship, Ronnie climbed into the car and fell asleep, believing they were Jenkins and Carter, sent by his father to take him to his fathers "ranch" in California.

Ronnie does not know that his father, Frederick Willoughby (Lester Matthews), is actually in San Quentin State Prison, framed for embezzlement by his employer, Edward Martin (Joseph Crehan) of the Western Packing Company, to cover his own criminal activities. Willoughby agrees to appear as a witness on behalf of the small independent ranchers of California in a lawsuit against Martin, who has been driving them out of business to gain a monopoly. To prevent Willoughby from testifying, Martin schemed to kidnap Fredericks son Ronnie by sending the boy the cable that invited him to visit his fathers large spread, "Rancho San Quentin." The plot was momentarily derailed when the boy mistook Gene and Frog for his escorts.
 Mary Lee), whose car goes into a lake. Joyce is a socialite, a wealthy ranch owner, and a runaway bride who is wanted by the police for taking the jilted bridegrooms car. She and Patsy stow away in Genes horse trailer, and after several spats, Gene agrees to drive them to their California ranch. Along the way, Jenkins and Carter, having picked up Ronnies trail, try to kidnap the boy, but Gene and Frog foil the attempt. 

After Gene and his group arrive at Joyces ranch, she offers Ronnie refuge, pretending it is Rancho San Quentin. Gene sends Willoughby a wire telling him that Ronnie is with him, but the convict mistakes it for a kidnapping note and breaks out of prison to rescue his son. Martin sends his henchmen to join the posse in pursuit of the fugitive, which has orders to "shoot to kill." When he arrives at Joyces ranch, Willoughby learns that Ronnie is safe and surrenders to the sheriff. When Martin and his henchmen attempt to hijack the train carrying the Willoughby back to prison, Gene saves the day. Frederick is exonerated and returns to Joyces ranch claim his son, while Gene agrees to stay at the ranch as Joyces new foreman. 

==Cast==
* Gene Autry as Gene Autry
* Smiley Burnette as Frog Millhouse
* June Storey as Joyce Halloway
* Duncan Renaldo as Gaucho Don José Mary Lee as Patsy Halloway
* Clifford Severn as Ronnie Willoughby
* Lester Matthews as Frederick Willoughby
* Smith Ballew as Buck Benson
* Joseph Crehan as Edward Martin
* William Ruhl as Henchman Carter
* Wade Boteler as Rancher Ted Adams as E.J. Jenkins
* Wendell Niles as Radio Announcer
* The Velascos as Mexican Dancers
* José Eslavas Orchestra as Cantina Musicians
* Frankie Marvin as Gas Station Attendant #2 (uncredited) Walter Miller as George Blake (uncredited) Fred Snowflake Toones as New York City Pier Worker (uncredited)
* Champion as Genes Horse (uncredited)    

==Production==
===Casting=== Walter Miller was one of the top paid leading men in the silent era before transitioning to villain roles in talkies. Magers 2007, pp. 165–166.  He was born Walter Corwin Miller on March 9, 1892 in Dayton, Ohio, and grew up in Atlanta, Georgia. He was educated in Brooklyn, New York. Miller started his acting career doing stagework before being signed as a leading man by Reliance, a small independent film company.  By 1912 he was working for D. W. Griffith, and later appeared in feature films for 20th Century Fox and MGM.  He made ten popular serials with Allene Ray before being signed to Mascot Pictures by Nat Levine, for whom he made six serials. Later he made ten serials for Universal Studios, two for Columbia Pictures, and two for Republic Pictures.  During the filming of a fight scene in Gaucho Serenade, Miller and a fellow actor agreed to do the fight for real. For the next two days he complained of back pain, and while doing some closeup shots on the Republic backlot, he suffered a heart attack and died a short time later.  Although all of his scenes had been completed and remained in the film, Republic omitted his name from the credits. 

Wendell Niles was a prominent radio announcer, best known for his work on The Don Ameche Show and Gene Autrys Melody Ranch. Magers 2007, p. 165.  In 1932, he began working in films, mainly in the role of a radio newscaster or police broadcaster. In his later career, he was known for his television work as announcer on Lets Make a Deal and Truth or Consequences. 

===Filming and budget===
Gaucho Serenade was filmed March 21 to April 8, 1940. The film had an operating budget of $77,885 (equal to $ }} today), and a negative cost of $83,633. Magers 2007, p. 163. 

===Stuntwork===
* Joe Yrigoyen (Gene Autrys stunt double)
* Jack Kirk (Smiley Burnettes stunt double)
* Nellie Walker (June Storeys stunt double)
* Bill Yrigoyen   

===Filming locations===
* Jack Garner Ranch, State Highway 74, San Bernardino National Forest, California, USA
* Lake Hemet, Riverside County, California, USA   

===Soundtrack===
* "The Gaucho Serenade" (James Cavanaugh, John Redmond, Nat Simon) by Gene Autry with José Eslavas Orchestra at the Cantina
* "Headin for the Wide Open Spaces" (Gene Autry, Johnny Marvin, Harry Tobias) by Gene Autry and Smiley Burnette with an offscreen orchestra
* "Give Out with a Song" (Connie Lee) by Mary Lee with an offscreen orchestra
* "A Song at Sunset" (Gene Autry, Johnny Marvin, Harry Tobias) by Gene Autry and Mary Lee with an offscreen orchestra
* "Cielito Lindo" (Traditional) at the Cantina
* "Junta Plata-Latin Dance Music" (Jose Eslava) danced by The Velascos with José Eslavas Orchestra at the Cantina
* "Wooing of Kitty MacFuty" (Smiley Burnette) by Smiley Burnette with José Eslavas Orchestra at the Cantina
* "The Singing Hills" (Mack Davis, Sammy Mysels, and Dick Sanford) by Gene Autry with an offscreen backup and orchestra
* "Keep Rollin Lazy Longhorns" (Gene Autry, Johnny Marvin) by Gene Autry, Mary Lee, Smiley Burnette, and chorus    

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 