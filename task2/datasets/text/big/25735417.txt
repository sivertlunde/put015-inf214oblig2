Just One Time (film)
  feature length comedy film, written and directed by Lane Janger. It is also a 1998 short film by the same director on which the long feature was based on.

==1998: Just One Time (short)==
{{Infobox film
| name           = Just One Time  (short) 
| image          = 
| caption        = 
| director       = Lane Janger
| screenplay     = Lane Janger 
| story          = Lane Janger Guillermo Díaz Jennifer Esposito
| producer       = Lane Janger and Exile Ramirez (producers), Blake Baldwin (associate producer)
| music          = David Michael Frank	
| cinematography = Timothy Naylor	 	
| editing        = François Keraudren
| distributor    = 
| released       = 1998
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The film Just One Time is based on an 8-minute short film of the same name produced in 1998 in which a man wants his girlfriend to have a sexual relationship with another girl, just one time. It was written and directed by Lane Janger and includes Guillermo Díaz (as Victor), Jennifer Esposito (as Michelle), Joelle Carter (as Amy) and 
Lane Langer (as Anthony)

;Award
The short won aGLIFF Award in "Best Boys Short" category at the Austin Gay & Lesbian International Film Festival.
 

==1999: Just One Time (feature film)==
{{Infobox film
| name           = Just One Time
| image          = Just-one-time-by-lane-janger.jpg
| caption        = Original film poster
| director       = Lane Janger
| screenplay     = Lane Janger  Jennifer Vandever
| story          = Lane Janger Guillermo Díaz Jennifer Esposito
| producer       = Jasmine Kosovic, Lane Janger and Exile Ramirez (producers), Jeff Roth (co-producer), David R. Ginsburg, Marcus Hu and Charlotte Mickie (executive producers)
| music          = Edward Bilous and David Michael Frank	
| cinematography = Timothy Naylor and Michael St. Hilaire	 	
| editing        = François Keraudren and Mitchel Stanley
| distributor    = 
| released       = 1999
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}} Guillermo Díaz as Victor, and Jennifer Esposito as Michelle.

;Synopsis
Film is about sexual orientation plays. The fiancee of a fireman reluctantly agrees to participate in a "menage a trois" with another woman, a sexual fantasy of his. She agrees to the arrangement, but insists to turn the table on him. She suggests that he agrees to reciprocate by having a relationship with another man for one time. This  ultimately puts the impending marriage of the two in jeopardy.

;Cast
*Lane Janger as Anthony
*Joelle Carter as Amy Guillermo Díaz as Victor
*Jennifer Esposito as Michelle
*Vincent Laresca as Nick
*Domenick Lombardozzi as Cyrill
*David Lee Russek as Dom
*Mickey Cottrell as Father Sebastian
*Jerran Friedman as Young Man in Bathroom (credited as Jerran Marshall)
*Anthony Gestone as Club dancer
*Hazelle Goodman as Discussion leader in Ladies bookstore
*Harley Kaplan as Fireboys Friend
*Susan Kellermann as Nava Hannibal (Amys Mom)
*Priscilla Lopez as Victors Mother
*Ajay Mehta as Husan NY Deli owner
*Pat Moya as Mona
*Chris Stewart as Bartender
*André Vippolis	as Fireboy

;Award nomination
*1999: Nomination for "Best Producer" for film producer Jasmine Kosovic during the Independent Spirit Awards. It was jointly for the film Just One Time and another film she had produced, namely The Adventures of Sebastian Cole

==External links==
* 
* 

 
 
 
 


 