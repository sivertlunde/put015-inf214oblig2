Gamera 3: The Revenge of Iris
{{Infobox film
| name           = Gamera 3: The Revenge of Iris
| image          = Gamera-3-poster.jpg
| alt            = 
| caption        = Japanese film poster for Gamera 3
| film name      = 
| director       = Shusuke Kaneko
| producer       = {{plainlist|
*Miyuki Nanri
*Naoki Sato
*Tsutomu Tsuchikawa}}
| writer         = {{plainlist|
*Kazunori Itō
*Shusuke Kaneko   }}
| starring       = {{plainlist|
*Shinobu Nakayama Ai Maeda
*Yukijiro Hotaru}}
| music          = Kow Otani 
| cinematography = Junichi Tozawa 
| editing        = Isao Tomita 
| studio         = Daiei Film 
| distributor    = Toho
| released       =  
| runtime        = 108 minutes 
| country        = Japan 
| language       = Japanese 
| budget         = 
| gross          = $15,000,000
}} Ai Maeda), an emotionally troubled young girl who forms a psychic bond with a highly aggressive parasitic creature known as Iris that feeds upon her rage and hate for the giant fire-breathing turtle monster called Gamera, who had unwittingly killed Hirsakas parents. Gamera later is defending Japan from a swarm of monsters called Gyaos when he is confronted by Iris.

Gamera 3: The Revenge of Iris was shown at the 1999 Toronto International Film Festival and received the Mainichi Film Concours award for Best Sound Recording in Japan. The film received positive reviews from film critics who praised its special effects with many praising it as one of the best in the Gamera film series.

==Plot==
Three years have passed since Gamera defeated the Legion, and the world is once again plagued by the Gyaos, which have evolved into Hyper Gyaos. Mayumi Nagamine, noted ornithologist, returns to aid the Japanese government in addressing this threat. A graveyard of Gamera fossils is found at the bottom of the sea. Shadowy government agents, occultist Miss Asukura and Kurata Shinji, are meanwhile working to a different agenda, with Asukura believing Gamera to be an evil spirit.

A pair of Gyaos glide across Tokyos Shibuya district, but are destroyed by Gamera at the cost of twenty thousand human lives, causing the Japanese government to order Gameras immediate destruction. Meanwhile, a young girl named Ayana, whose parents were inadvertently killed by Gamera during his previous battle with the Gyaos in 1995, discovers a stone egg sealed within her village temple. The egg hatches a small tentacled creature, whom the girl names "Iris." Iris forms a link with Ayana through an orihalcum pendant, and becomes the focus of Ayanas quest for revenge, as she seeks to raise her own monster and take vengeance against Gamera. Iris however attempts to absorb Ayana in the process of his growth. The girls foster brother manages to free her from Iris Pupa#Cocoon|cocoon, but leaves his lair and kills half of the villages populace, later growing into his adult form. The military attempts to destroy him, but fails.

Iris flies toward the city of Kyoto, where Ayana has been taken by Asukura and Kurata, with Asukura deliberately trying to use the girl to summon Iris. Iris is intercepted in mid-flight by Gamera, and the two engage in an aerial battle, but the Japanese army intervenes and knocks Gamera out of the sky with a tactical missile strike. Nagamine and Asagi, the girl once psychically linked with Gamera, retrieve her and attempt unsuccessfully to get her out of Kyoto. Kurata expresses a belief that Iris had been deliberately created to defeat Gamera so that the Gyaos could wipe out modern humanity.

The two monsters engage continue their fight, but Iris easily gains the upper hand, impaling his foe and leaving Gamera for dead. Iris then makes his way to the train station and absorbs Ayana, killing Asukura and Kurata in the process. From within Iris body, Ayana experiences the creatures memories, and realises that her hatred and bitterness motivated him. Just as she has her epiphany, Gamera plunges his hand deep into Iris chest and wrenches the girl free, robbing Iris of its human merge. Miss Nagamine and Asagi, trapped within the train stations wreckage, watch helplessly as Iris impales Gameras hand and begins to syphon his blood, creating fireballs with its tentacles. Gamera blasts off his injured hand, and absorbs Iriss fireballs, forming a fiery plasma fist, which he drives into Iris wounded chest.

Iris explodes, blowing the roof off the crumbling train station. The comatose Ayana still clutched in his fist, Gamera sets the girl down where Nagamine and Asagi are hiding. The women are unable to revive her, but Gamera lets out a roar and Ayana awakens. Gamera leaves the girl wondering why he would save her life after all she had done. A swarm of Gyaos, thousands strong, begins to descend on Japan intent on destroying their greatest foe once and for all, as Gamera lets out a final roar of defiance, as he stands his ground in the center of a blazing city.

==Cast==
* Shinobu Nakayama as Mayumi Nagamine – One of the main characters of the series. Mayumi is a scientist who is intrigued by Gamera, and forms a partnership with Asagi due to the latters past experiences with the monster. Ai Maeda as Ayana Hirasaka – Ayana is a tormented young girl whose parents were unintentionally killed by Gameras previous feud with the Gyaos. She forms a bond with Irys, and her rage fuels the creatures appetite for destruction.
* Ayako Fujitani as Asagi Kusanagi - Asagi is Gameras former human companion, who sacrificed their bond during his fight with the Legion three year earlier.
* Senri Yamasaki as Mito Asakura - A sadistic woman who believes that Gamera is an evil spirit bent on destruction, and that Iris is the key to his downfall.
* Toru Tezuka as Shinya Kurata - A mysterious man who claims to be a descendant of the advanced civilization that created Gamera and the Gyaos.
* Takasaki Nayami as GF Colonel Takoshi - The colonel of the Japanese Self-Defense Force.
* Hakosaki Sato as GF General
* Kenji Soto as Dr. Sato
* Yukijirō Hotaru as Inspector Osako
* Masahiko Tsugawa as Commander in Chief of the Air Defence Command (Lieutenant General)
* Hirofumi Fukuzawa as Gamera - The movies titular kaiju and the Guardian of the Universe, Gamera is a giant, fireball-breathing turtle and an ancient biological constructed monster created to destroy the Gyaos, a race of vampiric avian-like creatures.
* Akira Ohashi as Irys - The shows main kaiju antagonist, Irys is a creature that must feed on bodily fluids to survive, and forms a spiritual bond with Ayana, using her hatred for Gamera to feed his strength. He is Gameras ultimate foe.
*Kei Horie as Shigeki Hinohara - Ayanas Cousin
*Aki Maeda as Young Ayana
*Yukie Nakama as Female Camper

==Production==
  (pictured) has composed the music for all three Gamera films directed by Shusuke Kaneko.]]
Many members of the crew who worked on Gamera 3: The Revenge of Iris had previous work in the Gamera film series. Director   (1995) and   (1996).  Gamera 3 marks the first Gamera film that Kaneko had screenwriting credits on as he co-wrote the film with Kazunori Ito who had previously wrote the previous two 1990s Gamera films.    The music composer Kow Otani and special effects director Shinji Higuchi was also a regular with the series, previously working on both films.   Galbraith, 2008. p.389 

==Release== 1999 Toronto Film Festival.    The film did not have a wide-release in the North America, and was released direct-to-video on DVD on June 10, 2003 by A.D.V. Films.     The film was released on Blu-ray Disc|Blu-ray by Mill Creek Entertainment on September 27, 2011. 

Gamera 3: The Revenge of Iris was followed up by Gamera the Brave directed by Ryuta Tasaki in 2006.    The films plot ignores the events of the three films directed by Kaneko. 

==Reception==
In Japan, Gamera 3: The Revenge of Iris won the award for Best Sound Recording at the 54th Mainichi Film Concours ceremony. 

Western reviewers praised the film as one of the best in the Gamera series commenting on the special effects in the film.   (1995) and   (1996) as well stating that the monster Gamera appeared "more threatening".    Variety also described the special effects in the film as "good by model/miniature/animated standards" but felt that were not up to the standards of American special effects.  The   felt that the film was stronger than Gamera 2: Attack of Legion, stating the film is stronger "because it has much less Gamera; theres only so much character richness, let alone fun, to be found in shell, teeth, eyes, claws, scales, etc. But the movie has thrills for those who need em. Toward the end, a young scientist faces Iris and his doom and, a moment before he dies, screams like a cheerleader at his own immolation: "Oh boy, is this scary? Yes!" I second that notion." 

==Notes==
 

===References===
*  

==See also==
 
* List of giant-monster films
* List of Japanese films of 1999
* List of science fiction films of the 1990s

==External links==
*  
*  at  

 
 

 
 
 
 
 
 
 
 
 
 
 
 