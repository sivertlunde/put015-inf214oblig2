Hare Rama Hare Krishna (1971 film)
{{Infobox film name = Hare Rama Hare Krishna image =Hare Rama Hare Krishna (1971 film).jpg caption = Dev Anand and Zeenat Aman director = Dev Anand writer = Dev Anand starring = Mumtaz
|producer = Dev Anand
|cinematography=Fali Mistry music = Rahul Dev Burman, Anand Bakshi (Lyrics) released = 9th December 1971 runtime = 149 min. language = Hindi
}} 1971 Indian Mumtaz and BFJA Award for Best Actress.   The movie dealt with the decadence of the Hippie culture. It aimed to have an anti-drug message and also depicts some problems associated with Westernization such as divorce. It is loosely based on the 1968 movie Psych-Out.
 Dum Maro Dum", which was a huge hit. The music of the film was composed by Rahul Dev Burman and the lyrics were written by Anand Bakshi.

The story for Hare Rama Hare Krishna actually came to Dev Anands mind when he saw hippies and their fallen values in Kathmandu, Nepal where he was on a visit after protests against his previous film Prem Pujari in Calcutta. He was low in spirits because his film had been opposed and some had burnt Prem Pujaris posters. 

== Plot == International Hare Krishna movement in the 1970s, is a Montreal-based family of the Jaiswals, consisting of mom, dad, son, Prashant, and daughter, Jasbir. Due to differences, Mr and Mrs Jaiswal separate, leaving Jasbir with dad, and Prashant with his mom. Eventually Prashant and his mom travel to India, leaving father and daughter behind in Montreal. Mr. Jaiswal remarries, and brings his new wife to live in his home. Jasbir is told by her nanny that her mother & brother are dead. Back in India Prashant is sent to a boarding school and his father makes sure that none of Prashants letters reach Jasbir, so that it would be easy on her part to get over emotional trauma. Jasbir is upset with her inconsiderate step-mother and ignorant father who is deeply immersed in his business.

Years later, Prashant has grown up to be a pilot. He has received a letter from his dad that Jasbir, who had rebelled and left home, is now located in Kathmandu, Nepal, with a group of hippies. Prashant decides to find his sister and hopefully get her back to the family. When Prashant lands in Kathmandu he does not find Jasbir, but instead finds Janice, who is indeed his sister with a new name. Janice has no recollection of her childhood, and is always in the company of hippies spending most of her time consuming alcohol & drugs with them.

Janice lives with the hippies in the property rented out by local landlord Drona. Dronas real business is stealing ancient artifacts from Kathmandu and selling it to foreign nationals. Michael, one of the hippies, is the one who does all the dirty work for him. 
Janices boyfriend Deepak misunderstands that Prashant is trying to woo Janice, hence they exchange a few blows every time they meet. Meanwhile Drona has an eye on Shanti, a local salesgirl working in one of the shops owned by him. Shanti has feelings for Prashant which creates one more enemy for him. Later Prashant and Shanti elope and get married. At the same time a precious idol is stolen from local temple by Michael, which he hides in Janices house. Prashant secretly observes all this. Drona tries to frame Shanti for theft by secretly planting another stolen artifact in her house. Later he spreads the word that since the day Prashant has arrived idols are being stolen and he is stalking local girls.

The police commissioner is a friend of Prashants father, and has already received a letter stating the purpose of Prashants visit to Kathmandu. He suspects that Drona is trying to frame Prashant because he has married Shanti. He gets a search warrant for the entire property of Drona and recovers a diary which has contact details of his friends abroad who help him sell the stolen artifacts. The police also recover the stolen artifact from Shantis home, squarely blaming Prashant for it. Shanti is deeply hurt by this and looks around for Prashant. Prashant meanwhile is with Janice, trying to convince her that he is her brother, who she had been told dead long back. Michael overhears the conversation & conspires to put the blame on the brother-sister duo. Taking advantage of the situation Drona and Michael instigate the locals against Prashant by framing him for the theft and duping Shanti under pretext of marriage. The hippies and the locals are now ready to bash Prashant the moment they come across him.

When Prashant again tries to meet Janice, the hippies give him a solid thrashing. The police commissioner intervenes and Prashant is saved. At the same time real face of Drona is uncovered and he meets his end trying to run away from police. Janice sees that both her parents have arrived to meet her and realizes that Prashant is indeed her bother. Janice is deeply hurt that her parents had to see her in this state. She runs away from them and commits suicide. In her suicide note she tells Prashant how deeply she loved him and she never intended him to find her in this state and suicide was the only way out for her.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1 Dum Maro Dum"
| Asha Bhosle
|- 
| 2
| "Ghungroo Ka Bole"
| Lata Mangeshkar
|-
| 3
| "Hare Rama Hare Krishna"
| Asha Bhosle, Usha Uthup
|-
| 4
| "Kanchi Re Kanchi Re"
| Kishore Kumar, Lata Mangeshkar
|- 
| 5
| "Phoolon Ka Taron Ka (I)"
| Lata Mangeshkar
|-
| 6
| "Phoolon Ka Taron Ka (II)"
| Kishore Kumar
|-
| 7
| "Ram Ka Naam Badnam Na Karo"
| Kishore Kumar
|}

==See also==
*Hare Krishna in popular culture

== References ==
 

== External links ==
*  
* 
* 
* 

 
 
 
 
 
 
 
 
 
 
 