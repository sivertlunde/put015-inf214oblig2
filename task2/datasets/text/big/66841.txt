Fly Away Home
 
{{Infobox film
| name           = Fly Away Home
| image          = Fly_away_home_poster.jpg Theatrical release poster
| based on       =  
| writer         = Robert Rodat Vince McKewin
| starring       = Jeff Daniels Anna Paquin Dana Delany Terry Kinney Holter Graham Jeremy Ratchford
| director       = Carroll Ballard
| producer       = Carol Baum
| music          = Mark Isham
| cinematography = Caleb Deschanel
| editing        = Nicholas C. Smith
| studio         = Columbia Pictures Sandollar
| distributor    = Columbia Pictures
| released       =  
| runtime        = 107 minutes
| country        = Canada United States New Zealand English
| gross          = $25,143,818
}}
 family Drama drama film directed by Carroll Ballard. The film stars Anna Paquin, Jeff Daniels and Dana Delany. Fly Away Home was released on September 13, 1996 by Columbia Pictures.
 imprint on migration route ultralight aircraft that are used to guide the birds to sanctuary.

The story dramatizes the actual experiences of Bill Lishman, who in 1986 started training geese to follow his ultralight and succeeded in leading their migration in 1993.

The film has mostly positive critical reviews, receiving an 85% approval rating from Rotten Tomatoes.   rottentomatoes.com. Retrieved: May 9, 2011. 

==Plot==
In New Zealand, 13-year-old Amy Alden (Anna Paquin) is involved in an automobile accident with her mother, which results in her mother’s death. Amy is taken back to her new home in Ontario by her estranged father Thomas Alden (Jeff Daniels), an inventor, artist and sculptor, who later introduces her to his girlfriend, Susan (Dana Delany). While Amy is initially hostile towards her father and Susan, she eventually grows fond of both.
 incubate them. When she comes back from school, she discovers that the eggs have hatched and Thomas allows her to keep the birds.
 domestic geese pinioned (clipped) in order to render them flightless. When Glen attempts this on one of the geese, it upsets Amy. Thomas throws the game warden off his property. Seifert threatens that if the birds start flying, he will have to confiscate them.

Thomas begins doing research and learns that if the birds aren’t taught to fly properly, as they have no goose parents to teach them, they will not survive during the oftentimes brutal Canadian winter. He enacts a plan to use a pair of   as their mother. Susan is disgusted at hearing the news as, the previous day, Amy had hopped in one of Thomas’s aircraft and almost killed herself when the aircraft crashed.

 ) attempts to get the geese to follow him.|250px|left|thumb]]

Thomas, Susan, David and Thomas’s friend Barry (Holter Graham) attempt to teach the birds to fly, with success. David travels to North Carolina, to talk to a friend who owns a bird sanctuary about the plan.  His friend initially finds the plan both ludicrous and impossible, but also mentions that if no birds reach the sanctuary by November 1, it will be torn down by developers who plan to turn it into a coastal housing development.

Amy and Thomas practice flying the aircraft, but Igor (the weakest of the geese, and who has a limp) accidentally hits the front of Amy’s aircraft,  injuring him. The goose falls down and lands in an isolated forest. While the group goes off to search for the bird, Glen, keeping his promise to confiscate the birds and who witnessed the Aldens practicing for the big flight, comes to the Alden barn and takes the geese. The next day, after breaking the geese out of the game wardens headquarters, the group set off on their quest to migrate them.

After making an emergency landing at a U.S. Air Force base in upstate New York on the south shore Lake Ontario and almost getting arrested, Amy and Thomas become national news, with residents cheering them on and offering the two a place to stay at night at each of their stops. Thirty miles before reaching the bird sanctuary, Thomas’s aircraft crashes in a cornfield and he commands Amy to finish the journey by herself. After Amy takes off and begins to head toward the sanctuary, Thomas hitchhikes a ride with a local group of hippies who take him to the bird sanctuary. While waiting for Amy, Thomas, Susan, Barry and many hippies, tree huggers, townspeople and animal enthusiasts stand up to the large crowd of developers who are waiting to start the excavation of the site. Amy eventually appears with the geese, much to the joy of the townspeople and Amy’s family, but to the dismay of the developers. The townspeople and the Aldens celebrate their victory. All 16 geese, including Igor, returned to the Aldens farm the next spring safely and all on their own.

==Cast==
As appearing in screen credits (main roles identified, Listed in order of screen credits:   imdb.com. Retrieved: March 20, 2010. )
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- Jeff Daniels || Thomas "Tom" Alden
|- Anna Paquin || Amy Alden
|- Dana Delany || Susan Barnes
|- Terry Kinney || David Alden
|- Holter Graham || Barry Stickland
|- Jeremy Ratchford || Glen Seifert
|- Deborah Verginella || Amys Mother

|- Michael J. Michael J. Reynolds || General
|- David Hemblen || Dr. Killian
|- Ken James Ken James || Developer
|- Nora Ballard || Jackie
|- Sarena Paton || Laura
|- Carmen Lishman || Older Girl
|- Christi Hill || Older Girl
|- Judith Orban || Teacher
|- Jeff Braunstein || Chairman
|- John Friesen || Smalltown Businessman
|- Chris Benson || Farmer
|- Kevin Jubinville || M.P.
|- Philip Akin || Air Force Reporter
|- Gladys OConnor || Farm Woman
|- Geoff McBride || Clerk
|}

  to publicize the film.]]

==Production== Lindsay in southeastern Ontario. The township had also been the setting for A Christmas Story (1983), and later A Cool Dry Place (1998). The blacksmith shop constructed onsite for the filming of The Last Buffalo at Purple Hill, Ontario was re-used as part of the Alden homestead. 
 Cosmos Trike. The Easy Riser first appears as a foot-launched biplane hang glider. True to Lishmans real-life saga, modifications were made to improve the design including the addition of a motor and seat. Anna Paquins character instead flies an A-frame Cosmos Trike with a mock goose head mounted to the noseplate of the airframe and a fabric wing covering painted to resemble feathers. The Cosmos Trike was reportedly chosen for its safety, superior engine power, and increased wing size (a feature that was needed to fly slow enough for the birds).   williamlishman.com. Retrieved: March 20, 2010. 

The four-day trip home for the geese that would take them to Lake Ontario, over the Appalachians to Pennsylvania, Maryland, finally settling on the North Carolina Shores, had principal photography actually filmed nearly entirely at   off the coast of Georgetown, South Carolina. 

While in production, the film was at first titled Flying Wild but was changed to Fly Away Home just weeks before its release in movie theaters. The original trailer has the title Flying Wild and can be found on certain copies of the Columbia Tri-Star  , September 13, 1996. 

Director Carroll Ballard and cinematographer Caleb Deschanel previously collaborated on The Black Stallion and Never Cry Wolf with Fly Away Home being their third family film. Anna Paquin, who plays daughter to Jeff Daniels in Fly Away Home, would later play a love interest in The Squid and the Whale (2005 in film|2005). Upon the release of the Squid and the Whale, Jeff Daniels said that having had the previous experience made the filming a bit awkward for both of them. 

==Reception==
For a modest budget, Fly Away Home returned US$25 million in the U.S. box office and US$31 million internationally. Audiences and critics enjoyed the evocative, uplifting family-oriented film which also received critical acclaim.   from The New York Times was similarly effusive, "Mr. Ballard turns a potentially treacly childrens film into an exhilarating 90s fable." Maslin, Janet.   The New York Times, September 13, 1996.  The uplifting theme of the film was often cited; Frederic and Mary Ann Brussat extolled: "The movie adds excitement and emotion, turning into a celebration of the creative ways human beings and animals can serve, assist, and love one another." 

The movie holds an 85% approval rating on the movie site Rotten Tomatoes indicating positive reviews from critics. 

==Awards== 1996 Academy Awards and from the American Society of Cinematographers. Although unsuccessful in both competitions, Fly Away Home went on to win the 1997 Broadcast Film Critics Association Critics Choice Award as the Best Family Film, the 1997 Christopher Award (for family films), 1997 Young Artist Award in the category of Best Family Feature – Drama, and the 1997 Genesis Award for Feature Films. Anna Paquin was also nominated for the Young Artist Award for Best Performance in a Feature Film – Leading Young Actress (the film was also nominated as Best Family Feature – Drama) and the 1997 YoungStar Award for Best Performance by a Young Actress in a Drama Film. 

==Home media release==
Along with the VHS release of Fly Away Home in December 1996, a later 2001 special edition DVD included the exclusive featurette by Bill Lishman, Operation Migration: Birds of a Feather, along with two documentaries: The Ultra Geese and the HBO special Leading the Flock. The DVD also provided a link to Lishmans "Operation Migration" website.  A companion CD audio recording of the music featured in the soundtrack was released in 1996.  A Blu-ray Disc|Blu-ray edition of Fly Away Home was released on April 7, 2009.

==See also==
*List of American films of 1996
*Imprinting (psychology)
*Winged Migration

==References==
Notes
 
Bibliography
 
* Fly Away Home (Special Edition DVD). Culver City, CA: Columbia/Tristar Home Video, 2001.
* Hermes, Patricia. Fly Away Home: The Novelization and Story Behind the Film. New York: Newmarket, 2005. ISBN 1-55704-489-9.
* Lishman, Bill (as stated). Father Goose & His Goslings (Light Up the Mind of a Child Series). St. Louis, Missouri: San Val, 1992. ISBN 978-1-4176-3444-6.
* Bill Lishman|Lishman, William Alwyn. Father Goose: One Man, a Gaggle of Geese, and Their Real Life Incredible Journey South. New York: Crown, 1996. ISBN 0-517-70182-0.
 

==External links==
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 