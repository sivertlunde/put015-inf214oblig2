Cicakman 2: Planet Hitam
 
 
 
{{Infobox film
| name           = Cicakman 2: Planet Hitam
| image          = Cicakman2posterkecik.jpg
| caption        = Cicakman 2: Black Planet poster Yusry Abdul Halim Norman Abdul Yusry Abdul Edry Abdul Halim
| starring       = Saiful Apek Fasha Sandha Aznil Hj Nawawi Tamara Bleszynski  AC Mizal Adlin Aman Ramlie Sharifah Amani
| distributor    = Grand Brilliance Sdn Bhd (Malaysia and Brunei) PT Tripar Multi Vision Plus (Indonesia) Cathay-Keris Films Pte Ltd (Singapore)   
| released       =  
| country        = Malaysia
| runtime        = 107 minutes   
| language       = Malay
| budget         = Malaysian ringgit|MYR3.8 million (USD 1.02 million) gross          = MYR 2.85 million (USD 780 thousands)
}}

Cicakman 2: Planet Hitam ( ) is a film sequel of Cicak-Man that had been released in 11 December 2008 by KRU Studios with collaboration with Grand Brilliance Sdn. Bhd. in Malaysia, Singapore and Brunei and released in Indonesia in January 2009.   
 Yusry Abdul Halim with script by the director and Meor Shariman, this action-comedy saw the return of the cast from the first movie and new characters were introduced.   

==Plot==
The evil Professor Klon (Aznil Nawawi) is back. This time, not only to overthrow the government and becomes the President of Metrofulus, but also to control the worlds supply of fresh water through his ingenious plan; "Black Planet". When our blue planet has only 72 hours before turning black, Cicakman (Saiful Apek) comes to rescue.
 Yusry Abd Halim) his demised best friend, a powerful feng shui master, Miss Chee (Louisa Chong) and an unlikely party.

Apart from his heavy responsibilities to save the world, he also has his own personal dilemmas to address; that is Hairi vs Cicakman. He has to resolve his personal feelings towards Tania (Fasha Sandha), who is seeking the true identity of Cicakman and he also has to choose whether to sacrifice his own life or save Iman (Sharifah Amani), Dannys blind sister.   

==Production==
The shooting that took place in Penang, Gua Tempurung, Kampar, Perak|Kampar, Universiti Teknologi Petronas, and Kuala Lumpur costs 2.5 million Malaysian ringgit|Ringgit. The costume used in the sequel is higher in quality than the prequel as well as the price cost MYR50 thousand per set while in the prequel the costume used costs only MYR20 thousand per set.   

===Cast===
* Saiful Apek as Cicak-Man/Hairi
* Fasha Sandha as Tania
* Aznil Nawawi as Professor Klon
* Tamara Bleszynski as Rrama
* Adlin Aman Ramlie as Ginger Ghost 1
* AC Mizal as Ginger Ghost 2
* Sharifah Amani as Iman
* Louisa Chong as Miss Chee
* Linda Onn as Amarr Yusry Abdul Halim as Danny
* Aziz M. Osman as  Fulus News Boss
* Julia Ziegler as Kindergarten teacher
* Edry Abdul Halim as Robber 1
* Munir (Phyne Ballerz) as Robber 2
* Ridzuan Hashim as Abu Bakar
* Jalaluddin Hassan as President Ramlan
* Azwan Ali as Neighbour 1
* Dee as Neighbour 2
* Julie Dahlan as Neighbour 3
* Fahrin Ahmad as Cameraman
* Hafidzuddin Fazil as Chief fireman
* Nadia Mustafar as Woman at supermarket 1
* Mak Jah as Woman at supermarket 1
* Zack Taipan as Japanese guy   

==Release==
The film was first released at the 13th   in Asia segment.    Other two sessions was held on 7 October at the festival.  In Malaysia, Singapore and Brunei, the film had been released on 11 December 2008  with the goal by the producer of a million tickets sold compared to 670 thousand tickets sold for Cicak Man.  The film is expected to be released in Indonesia in January 2009. 

== Soundtrack ==
# Dafi - Lemah
# Pianka - Mau Yang Besar Tyco - Planet Hitam Edry - Sumpah Takkan Cari Yang Lain
# Anita Sarawak - Ular Flava - Berhenti Mencinta
# Adam - Jika Tak Kerna Kamu Yusry & Melly Goeslaw - Dibius Cinta   

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 