Uyire Unakkaga
{{Infobox film
| name = Uyire Unakkaaga
| image = Uyiraeunakkakae.png
| caption = LP Vinyl Records Cover
| director = K. Rangaraj
| producer = Kovai Thambi
| writer = M. G. Vallabhan (dialogues)
| screenplay = K. Rangaraj
| story = Motherland Pictures Story Unit Mohan Nadhiya Vijayakumar
| music = Laxmikant-Pyarelal
| cinematography = Dinesh Baboo
| editing = R. Bhaskaran, B. Krishnakumar
| studio = Motherland Pictures International
| distributor =
| released =  
| country = India Tamil
}}
 1986 Cinema Indian Tamil Tamil film, Vijayakumar in lead roles. The film had musical score by Laxmikant-Pyarelal.  

==Cast== Mohan 
*Nadhiya 
*Chinni Jayanth  Vijayakumar 
*Charle 
*Vinu Chakravarthy  Senthil 
*Kovai Sarala  Tinku
*ARS
*V. Gopalakrishnan Thyagu
*Meena Baby Meena as Nadhiya

==Soundtrack==
{{Infobox album Name     = Uyire Unakkaga Type     = film Cover    = Released =  Artist   = Laxmikant-Pyarelal Genre  Feature film soundtrack Length   = 24:48 Label    = T-Series
}}
The music is composed by Laxmikant-Pyarelal. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Janaki || || 6.01 
|-  Janaki || || 06.59 
|- 
| 3 || Ododi || S. P. Balasubrahmanyam, S. Janaki|Janaki, Dinesh Babu || || 05.18 
|-  Janaki || || 04.42 
|-  Janaki || || 06.58 
|}

==References==
 

==External links==
*  

 
 
 
 
 


 