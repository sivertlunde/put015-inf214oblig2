Fanny and Alexander
 
{{Infobox film
| name           = Fanny and Alexander
| image          = Fanny&Alexander.jpg
| caption        = Original Swedish release poster
| director       = Ingmar Bergman
| producer       = Jörn Donner
| writer         = Ingmar Bergman
| starring       = Pernilla Allwin Bertil Guve Börje Ahlstedt Anna Bergman Gunn Wållgren Kristina Adolphson Erland Josephson Mats Bergman Jarl Kulle
| music          = Daniel Bell
| cinematography = Sven Nykvist
| editing        = Sylvia Ingemarsson Gaumont
| Sandrew Film & Teater   Gaumont  
| released       =  
| runtime        = {{plainlist|
* 312 minutes  
* 188 minutes   
}}
| country        = Sweden France West Germany German
| budget         =
| gross          = US$6,783,304 
}} longest cinematic films in history.

==Plot==
The story is set during 1907–09 (with an epilogue in 1910), in the Swedish town of Uppsala where Alexander (Bertil Guve), his sister Fanny (Pernilla Allwin) and their well-to-do family, the Ekdahls, live. The siblings parents are both involved in theater and are happily married until their father, Oscar (Allan Edwall), suddenly dies from a stroke. Shortly thereafter, their mother, Emilie (Ewa Fröling), marries Edvard Vergérus (Jan Malmsjö), the local bishop and a widower, and moves into his ascetic home where he lives with his mother, sister, aunt and maids.

Emilie initially expects that she will be able to carry over the lavish, joyful qualities of her previous home into the marriage, but realizes that Edvards harsh authoritarian policies are unshakable. The relationship between the bishop and Alexander is especially cold, as Alexander invents stories, for which Edvard punishes him severely. Edvard immediately confines the children to their bedroom. 
As a result, Emilie asks for a divorce, which Edvard will not consent to; though she may desert the marriage, doing so would place the children in his custody, including the infant from her recent pregnancy. Meanwhile, the rest of the Ekdahl family has begun to worry about their condition. When Emilie secretly visits her former mother-in-law, Helena (Gunn Wållgren), to explain what happened, their friend Isak Jacobi (Erland Josephson), helps smuggle the children from the house. They live temporarily with Isak and his nephews in their store.

Emilie, now in the later stages of her pregnancy, refuses to restore the children to the home. Edvard insists she do so. Emilie gives Edvard a large dosage of her sleeping pills. She explains to him, as he shows signs that the medication is working, that she intends to flee the home as he sleeps. He claims that he will follow her family from city to city and ruin their lives, then blacks out. After Emilie gets away, Edvards dying aunt knocks over a gas lamp, which sets her bedroom and nightgown on fire. She runs through the house in flames to Edvards room and falls on him. Despite the sedative, he is able to get her off him, but dies shortly thereafter.

Alexander had fantasized about his stepfathers death while living with Isak. Isaks mysterious nephew, Ismael Retzinsky (Stina Ekblad), explains that fantasy can become true as he dreams it.

The story ends on a happy, life-affirming note, with the christening celebration of Emilies and the late bishops daughter as well as the extra-marital daughter of Alexanders uncle and the family maid, Gustav Adolf Ekdahl (Jarl Kulle) and Maj (Pernilla August). During the festivities, however, Alexander encounters the ghost of the bishop who knocks him to the floor, and tells him that he will never be free.

==Cast==
;The Ekdahl house
 
* Gunn Wållgren as Helena Ekdahl (grandmother)
* Jarl Kulle as Gustav Adolf Ekdahl
* Mona Malm as Alma Ekdahl (Gustavs wife)
* Angelica Wallgren as Eva Ekdahl (Gustav and Almas daughter)
* Maria Granlund as Petra
* Kristian Almgren as Putte
* Emelie Werkö as Jenny
* Allan Edwall as Oscar Ekdahl
* Ewa Fröling as Emelie Ekdahl (Oscars wife)
* Bertil Guve as Alexander Ekdahl
* Pernilla Allwin as Fanny Ekdahl
* Börje Ahlstedt as Carl Ekdahl
* Christina Schollin as Lydia Ekdahl (Carls wife)
* Sonya Hedenbratt as Aunt Emma
* Käbi Laretei as Aunt Anna von Bohlen
* Majlis Granlund as Miss Vega
* Svea Holst as Miss Ester
* Kristina Adolphson as Siri
* Siv Ericks as Alida
* Inga Ålenius as Lisen
* Eva von Hanno as Berta
* Pernilla August as Maj
* Lena Olin as Rosa
* Patricia Gélin as statue
* Gösta Prüzelius as Dr. Fürstenberg
* Hans Strååt as priest
* Carl Billquist as Jespersson, police officer
* Axel Düberg as witness
* Olle Hilding as witness
 
;The Bishops house
 
* Jan Malmsjö as Bishop Edvard Vergérus
* Kerstin Tidelius as Henrietta Vergérus
* Hans Henrik Lerfeldt as Elsa Bergius
* Marianne Aminoff as Blenda Vergerus
* Harriet Andersson as Justina
* Mona Andersson as Karna (maid)
* Marianne Nielsen as Selma (maid)
* Marrit Ohlsson as Tander (kitchen maid)
* Linda Krüger as Pauline
* Pernilla Wahlgren as Esmeralda
* Peter Stormare as young man
* Krister Hell as young man
 
;Jacobis house
 
* Erland Josephson as Isak Jacobi
* Stina Ekblad as Ismael Retzinsky
* Mats Bergman as Aron Retzinsky
* Viola Aberlé as Japanese woman 
* Gerd Andersson as Japanese woman
* Ann-Louise Bergström as Japanese woman
* Marie-Hélène Breillat as Japanese woman
 
;The Theatre
 
* Gunnar Björnstrand as Filip Landahl Heinz Hopf as Tomas Graal
* Sune Mangs as Mr Salenius
* Nils Brandt as Mr Morsing
* Per Mattsson as Mikael Bergman
* Anna Bergman as Hanna Schwartz
* Lickå Sjöman as Grete Holm
* Georg Årlin
* Gus Dahlström
* Ernst Günther as Rector Magnificus
* Hugo Hasslo as the singer
 

==Production==
Bergman intended the film to be his last feature, although he wrote several screenplays afterward and directed a number of TV specials. This most personal of his feature films was to some extent based on his and his sister Margareta Bergman|Margaretas unhappy childhood under their extremely strict father, a Lutheran pastor.  

The film simultaneously documents many of Bergmans earlier star actors and a wide array of prominent Swedish film and stage actors of its era. Liv Ullmann and Max von Sydow who, as leading Bergman actors, are conspicuously absent in this respect, had been his original intended stars as Emilie and Bishop Vergerus, but Ullmann was eventually unable to join due to other work obligations, while von Sydow didnt receive notification in time, apparently through mismanagement by his American agent. Bergman instead recruited newcomer Ewa Fröling and Jan Malmsjö, who is more widely known in Sweden as a highly gifted song and dance man, but who has also done many serious character parts on stage and on the screen. Bertil Guve, who gave a widely acclaimed performance as the boy Alexander, did not choose to pursue acting, but instead became a doctor of economics. However Pernilla Wallgren (later known as Pernilla August), who played the attractive nanny Maj, went on to star in other films, including The Best Intentions which Bergman wrote. 

===Two versions===
There are two versions of Fanny and Alexander: a shorter 3-hour (188 minutes) version, and a long 5-hour (312 minutes) version. The shorter version was released first, and the longer version was not released until a year later, even though it had been completed first. The long version was used for a four-part miniseries for television.

The shorter version had its theatrical premiere in Sweden on 17 December 1982. The American premiere for the shorter version was on 17 June 1983. The long version had its theatrical premiere in Sweden on 17 December 1983. The four-part miniseries of the long version later aired on Swedish television.

==Critical reception== 1984 and Best Director Best foreign language film (won).

Its merits are still widely discussed among film critics, provoking both strong admiration and strong criticism.

Rick Moody commented retrospectively that the film   

 

The Observer quoted actor Matthew Macfadyen as saying "it featured just the most extraordinary acting Id ever seen". As a student, the film "was screened to us as an example to follow – an example of people acting with each other. They all knew each other well in real life, the cast, and they rehearsed for a long time and shot it very quickly. The result is extraordinary."   
 
Xan Brooks, in The Guardian s Film Season, chose the film as his "No 8 best arthouse film of all time". He described it as Bergmans "self-styled farewell to cinema", "an opulent family saga, by turns bawdy, stark and strange". Few films, Brooks observes, "boast as many indelible supporting characters". He concludes that "by the time this film pitches towards that astonishing climax (bedsheets burning; magic working) one might even make a case for Fanny and Alexander as Bergmans most mature, clear-sighted and fully realised work".   

Vincent Canby in The New York Times begins by noting that the film "it has that quality of enchantment that usually attaches only to the best movies in retrospect, long after youve seen them, when theyve been absorbed into the memory to seem sweeter, wiser, more magical than anything ever does in its own time. This immediate resonance is the distinguishing feature of this superb film, which is both quintessential Bergman and unlike anything else he has ever done before." Canby finds it a "big, dark, beautiful, generous family chronicle"; the cast "are uniformly excellent". All of the film "has the quality of something recalled from a distance   events remembered either as they were experienced or as they are imagined to have happened. In this fashion Mr. Bergman succeeds in blending fact and fantasy in ways that never deny what we in the audience take to be truth." And, Canby emphasizes, Bergman repeatedly refers "to this little world, which in the film refers to the Ekdahls theater, a place of melodrama, comedy, dreams, magic, and moral order, in contrast to the increasing chaos of life outside".   

==Awards==
The film was released in the United States in 1983 and won four Academy Awards:      
* Academy Award for Best Foreign Language Film (Director Ingmar Bergman) Best Cinematography (Sven Nykvist) Best Art Direction (Anna Asp, Susanne Lingheim) Costume Design (Marik Vos-Lundh)
 Directing and Writing Original Screenplay but was not awarded. The film also received the Golden Globe Award for Best Foreign Film.
 Best Film, Best Director Best Actor (Jarl Kulle).   

==Home media==
The uncut TV version of the film is available in DVD editions released by   as well as other supplements; and a two-disc set that includes only the 188-minute theatrical version and fewer supplements. The Criterion release marked the first time the television version of Fanny and Alexander had been available in North America. 

==See also==
* Cinema of Sweden
* List of longest films by running time
* List of submissions to the 56th Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 