Big House, U.S.A.
{{Infobox film
| name           = Big House, U.S.A.
| image          = Big House, U.S.A. poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Howard W. Koch
| producer       = Aubrey Schenck
| screenplay     = John C. Higgins
| story          = George W. George George F. Slavin  William Talman Lon Chaney, Jr. Charles Bronson
| music          = Paul Dunlap
| cinematography = Gordon Avil 	
| editing        = John F. Schreyer 
| studio         = Bel-Air Productions
| distributor    = United Artists
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 William Talman, Lon Chaney, Jr. and Charles Bronson. The film was released on March 3, 1955, by United Artists.  

==Plot==
 

== Cast ==
*Broderick Crawford as Rollo Lamar
*Ralph Meeker as Jerry Barker
*Reed Hadley as Special FBI Agent James Madden William Talman as William Machine Gun Mason
*Lon Chaney, Jr. as Alamo Smith 
*Charles Bronson as Benny Kelly
*Felicia Farr as Emily Euridice Evans
*Roy Roberts as Chief Ranger Will Erickson
*Willis Bouchey as Robertson Lambert
*Peter J. Votrian as Danny Lambert
*Robert Bray as Ranger McCormick 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 