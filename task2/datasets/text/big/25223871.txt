List of Bollywood films of 2010
 
  
  highest grossing Hindi films at the Indian box office. The total net amount earned by the top ten films of the year was  , compared to Bollywood films of 2009|2009s  , a percentage increase of 11.71%. 2010 marks the first time that the top ten films alone crossed the   mark. This year also marks the first time in Bollywood history that two films (Dabangg and Golmaal 3) grossed more than  . The following list is of the top 10 highest grossing Bollywood films of 2010. 

==2010 releases==
===January–March===

{| class="wikitable"
|- style="background:#b0e0e66; text-align:center;"
! colspan="2" | Opening
! Title
! Director
! Cast
!Genre
|-
| rowspan="9"   style="text-align:center; background:#ffa07a; textcolor:#000;"|   J A N
| rowspan="2" style="text-align:center;"| 8 Romantic comedy
|- Romantic comedy
|-
| rowspan="3" style="text-align:center;"| 15 Romance
|- Comedy
|- The Waiting Thriller
|-
| style="text-align:center;"| 22
| Veer (film)|Veer || Anil Sharma || Salman Khan, Zarine Khan, Mithun Chakraborty, Sohail Khan, Jackie Shroff || Epic film
|-
| rowspan="3" style="text-align:center;"| 29 Romance
|- Drama
|- Drama
|-
| rowspan="9"   style="text-align:center; background:thistle; textcolor:#000;"|   F E B
| rowspan="2" style="text-align:center;"| 5 The Hangman Drama
|- Action
|-
| rowspan="2" style="text-align:center;"| 12 Drama
|- Drama
|-
| rowspan="3" style="text-align:center;"| 19 Action
|- Horror
|- Comedy
|-
| rowspan="2" style="text-align:center;"| 26 Thriller
|- Teen Patti Thriller
|-
| rowspan="21"   style="text-align:center; background:#98fb98; textcolor:#000;"|  M A R
| rowspan="5" style="text-align:center;"| 5 Comedy
|- Drama
|- Drama
|- Thriller
|- Barry John, Drama
|-
| rowspan="5" style="text-align:center;"| 12 Romantic Comedy
|- Thriller
|- Rahul Aggarwal Rahul Aggarwal, Comedy
|- Action
|- Trump Card Thriller
|-
| rowspan="4" style="text-align:center;"| 19 Drama
|-
| Lahore (film)|Lahore || Sanjay Puran Singh Chauhan || Aanaahad, Shraddha Das, Farooq Sheikh, Nafisa Ali || Sports/Social
|- Found footage
|- horror
|-
| rowspan="6" style="text-align:center;"| 26 Comedy
|- Social
|- Romance
|-
| My Friend Ganesha 3 || Rajiv S. Ruia || Rahul Pendkalkar, Baba Sehgal, Eva Grover, Sayaji Shinde || Animation
|- Arbaaz Khan, Comedy
|-
| Well Done Abba || Shyam Benegal || Boman Irani, Minissha Lamba, Sameer Dattani, Ravi Kissen || Social
|-
|}

===April–June===

{| class="wikitable"
|- style="background:#b0e0e66; text-align:center;"
! colspan="2" | Opening
! Title
! Director
! Cast
! Genre
|-
| rowspan="16"   style="text-align:center; background:#b4cae3;"| A P R
| rowspan="4" style="text-align:center;"| 2 Social
|- Romance
|-
|- Drama
|-
| rowspan="3" style="text-align:center;"| 9
| Jaane Kahan Se Aayi Hai || Milap Zaveri || Ritesh Deshmukh, Jacqueline Fernandez || Comedy film|Comedy/Romance film|Romance/Fantasy
|- Thriller
|- Romance
|-
| rowspan="2" style="text-align:center;"| 16 Social
|- Horror
|-
| rowspan="5" style="text-align:center;"| 23 Thriller
|- Animation
|- City of Social
|-
| Kuchh Kariye || Jagbir Dahiya || Sukhwinder Singh, Vikrum Kumar, Rufy Khan ||
|- Romantic Comedy
|-
| rowspan="2" style="text-align:center;"| 30 Chase || Action
|- Comedy
|-
| rowspan="6"   style="text-align:center; background:#98fb98; textcolor:#000;"|   M A Y
| rowspan="2" style="text-align:center;"| 7 Drama
|-u Drama
|-
| rowspan="3" style="text-align:center;"| 14 Drama
|- Drama
|- Romance
|-
| style="text-align:center;"| 21 Romance
|-
| rowspan="6"   style="text-align:center; background:#ffa07a;u textcolor:#000;"|   J U N
| style="text-align:center;"| 4 Sarah Thompson || Drama film|Drama/Political Thriller
|-
| style="text-align:center;"| 11 Thriller
|-
| style="text-align:center;"| 18 Romance
|-
| rowspan="2" style="text-align:center;"| 25 Action
|- Drama
|}

===July–September===

{| class="wikitable"
|- style="background:#b0e0e66; text-align:center;"
! colspan="2" | Opening
!Title
! Director
! Cast
! Genre
|-
| rowspan="8"   style="text-align:center; background:#98fb98; textcolor:#000;"|   J U L
| style="text-align:center;"| 2 Imran Khan, Romance
|-
| rowspan="2" style="text-align:center;"| 9 Romance
|- Action
|-
| rowspan="3" style="text-align:center;"| 16 Action
|- Barry John,Nikhil Ratnaparkhi

|- Drama
|-
| style="text-align:center;"| 23 Khatta Meetha Romance
|-
| style="text-align:center;"| 30 Crime
|-
| rowspan="10"   style="text-align:center; background:#ffa07a; textcolor:#000;"|   A U G
| style="text-align:center;"| 6 Romance
|-
| rowspan="2" style="text-align:center;"| 13 Horror
|-
| Peepli Live || Anusha Rizvi || Omkar Das Manikpuri, Raghuveer Yadav || Political cinema|Political, Comedy
|-
| rowspan="2" style="text-align:center;"| 20 Romance
|- Comedy
|-
| rowspan="5" style="text-align:center;"| 27 Comedy
|- Crime
|- Life Express Drama
|- John Abraham, Drama
|- Bhupinder Singh Suspense
|-
| rowspan="4"   style="text-align:center; background:#b4cae3;"| S E P
| rowspan="3" style="text-align:center;"| 2 We Are Romance
|- Horror
|- Drama
|-
| style="text-align:center;"| 10 Arbaaz Khan, Action
|}

===October–December===

{| class="wikitable"
|- style="background:#b0e0e66; text-align:center;"
! colspan="2" | Opening
!Title
! Director
! Cast
! Genre
|-
| rowspan="15"   style="text-align:center; background:#98fb98; textcolor:#000;"|   O C T
| rowspan="3" style="text-align:center;"| 1 Science fiction
|- Romance
|- Comedy
|-
| rowspan="2" style="text-align:center;"| 8 Thriller
|- Comedy
|-
| rowspan="4" style="text-align:center;"| 15 Thriller
|- Knock Out Action
|- Romance
|- Animation
|-
| rowspan="3" style="text-align:center;"| 22 Thriller
|- John Abraham, Romance
|- Rakta Charitra Biographical
|-
| rowspan="3" style="text-align:center;"| 29 Kapil Sharma, Romance
|-
|  
|- Romance
|-
| rowspan="11"   style="text-align:center; background:thistle; textcolor:#000;"|   N O V
| rowspan="2" style="text-align:center;"| 5 Comedy
|- Comedy
|-
| rowspan="2" style="text-align:center;"| 12 A Flat Thriller
|- Adventure
|-
| rowspan="3" style="text-align:center;"| 19 Romance
|- Thriller
|- Romance / Thriller
|-
| rowspan="4" style="text-align:center;"| 26 Thriller
|- Thriller
|- Imran Khan Imran Khan, Romance
|- K C Sunny Deol, Action
|-
| rowspan="16"   style="text-align:center; background:#b4cae3; textcolor:#000;"|  D E C
| rowspan="3"  style="text-align:center;"| 3 Period
|- Comedy / Crime
|- Rakta Charitra Biographical
|-
| rowspan="2"  style="text-align:center;"| 10 Drama Romance Romance
|- No Problem Comedy
|-
| rowspan="4"  style="text-align:center;"| 17 Ali Asgar, Thriller
|- Payback || Thriller
|- Horror
|- Drama
|-
| rowspan="3" style="text-align:center;"| 24 Action comedy
|- Drama
|- Animated
|-
| rowspan="2" style="text-align:center;"| 31 Thriller
|- Drama
|}

==References==
 

 
 
 

 
 
 
 
 