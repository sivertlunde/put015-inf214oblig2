Dinner for Schmucks
 
{{Infobox film
| name           = Dinner for Schmucks
| image          = Dinner for schmucks ver2.jpg
| caption        = Theatrical poster
| alt            = Steve Carell grinning maniacally stares from over Paul Rudds shoulder
| director       = Jay Roach
| producer       =  
| screenplay     =  
| based on       =  
| starring       =   Theodore Shapiro
| cinematography = Jim Deult
| editing        = Alan Baumgarten Reliance BIG BIG Pictures}}
| distributor    = Paramount Pictures released        =  
| runtime        = 114 minutes
| country        = United States
| language       = English
| budget         = $69 million  
| gross          = $86,406,677 
}}
Dinner for Schmucks (also known as Dinner with Schmucks) is a 2010 American   and The 40-Year-Old Virgin. The film was released theatrically on July 30, 2010.
 Comedy Award for "Best Comedy Actor – Film" for his role as Therman Murch in the film.  The elaborate mouse dioramas and "mouseterpieces" were created by The Chiodo Brothers. 

==Plot summary==
Tim Conrad (Paul Rudd) is a mid-level financial executive, who comes up with a way (special novelty lamps) to invite wealthy Swiss businessman Martin Mueller (David Walliams) to become a client of his firm. Impressed by Tims ingenuity, his boss Lance Fender (Bruce Greenwood) invites him to a "dinner for winners" in which he must find and bring an eccentric person with a special talent; the winner earns a trophy and the executive that brought him or her gets glory. He soon learns it is more of a "dinner for idiots", and the partygoers will be mocked mercilessly. Meanwhile, Tims girlfriend Julie (Stephanie Szostak) lands a curator deal for eccentric artist Kieran Vollard (Jemaine Clement), and Tim unsuccessfully proposes to her, as he has done several times before. After learning of the cruel nature of the dinner party, Julie becomes upset and asks him not to attend and he agrees.

The next day, Tim accidentally hits IRS employee Barry Speck (Steve Carell) with his car when Barry tries to retrieve a dead mouse in the road. Witnessing Barrys bizarre behavior (he taxidermies mice and arranges them into elaborate dioramas), Tim realizes he is the perfect loser for the dinner. That night, Barry shows up at Tims apartment unannounced and accidentally invites over Darla (Lucy Punch), who had a one-night stand with Tim years ago and has been stalking him. Barry decides to guard Tims apartment from Darla to make up for inviting her. When Julie arrives at his apartment, Barry mistakes her for Darla and sends her away, giving Julie the mistaken impression that Tim is cheating on her. Barry decides to call Vollard and finds the number saved in the phone. While talking to him, he and Tim think they hear Julie in the background and head to Vollards apartment. Barry tosses Tims keys through Vollards apartment window and must break in, only to find Kieran preparing to make love to two girls dressed like animals (neither of them are Julie) and head back home.

Afterwards, Barry opens the door to find Darla waiting outside. Julie calls and asks Tim if hes having an affair and Darla steals the phone and stuffs it down her pants. While Tim hides in his bedroom, Darla and Barry get into a bar-styled fight, scaring Darla away. Julie accidentally left her phone at Tims apartment and a voice message from Vollard reveals to Tim and Barry that Julie is leaving for his ranch. Barry works for the IRS and offers to take Tim to work to find Vollards ranch address, where they meet up with Barrys boss and rival, Therman Murch (Zach Galifianakis) (who stole Barrys wife), who displays his "mind control" power over Barry. Tim leaves the next day for his brunch with the Muellers but Barry crashes, with Darla pretending to be Julie, when he gets a call from Tims assistant telling him to bring a still missing Julie. Tim is pushed into asking Darla to marry him by Barry and Julie walks in during. Tim and Barry chase Julie down to Vollards ranch, where Tim accidentally offends Barry (which further hurts his and Julies relationship).

To his surprise, Tim found Barry already at Fenders dinner party. Barry is a hit with the group and is a shoo-in for the trophy, but unexpectedly, Therman arrives, embarrassing Barry with his mind control. Tim then tells Barry everything and, after some encouragement, gets Barry to win with "brain control", before causing his boss and fellow executives to reveal the truth. A guest reacts badly to the news and inadvertently causes chaos in the process, with Mueller losing a finger. A fire breaks out, while a bird brought by one of the guests flies away with Muellers finger. Tim is fired, as is Julie after Barry makes Vollard realize it would be a problem to have her continue working for him. In the end, Tim marries Julie, Barry enters into a relationship with Darla, does some artwork with Vollard, and hosts a monthly "breakfast for champions" for all of the losers. Therman writes a new book in the mental hospital and Tim gets a new museum started in Switzerland for Mueller. After the closing credits, it is revealed Fenders company has gone under and the Forbes magazine has named him "Wall Streets Biggest Loser."

==Cast==
 
* Steve Carell as Barry Speck
* Paul Rudd as Timothy J. "Tim" Conrad
* Stephanie Szostak as Julie
* Jemaine Clement as Kieran Vollard
* Lucy Punch as Darla
* Zach Galifianakis as Therman Murch
* Bruce Greenwood as Lance Fender
* Ron Livingston as Caldwell
* Andrea Savage as Robin
* David Walliams as Mueller
* P. J. Byrne as Davenport
* Octavia Spencer as Madame Nora
* Jeff Dunham as Lewis / Diane
* Chris ODowd as Marco
* Kristen Schaal as Susana
* Patrick Fischler as Vincenzo
* Randall Park as Henderson
* Larry Wilmore as Williams
* Blanca Soto as Catherine
* Nick Kroll as Josh
 

==Production==
The budget for the film was split between the distributor Paramount Pictures, as well as DreamWorks Pictures and Spyglass Entertainment. The production budget was
$69 million, but with tax credits the cost came in at $62.7 million. {{cite news
| date = July 29, 2010
| last = Fritz | first = Ben
| title = Movie projector: Schmucks, cats, dogs and Zac Efron will all open behind Inception
| url = http://latimesblogs.latimes.com/entertainmentnewsbuzz/2010/07/schmucks-cats-dogs-and-zac-efron-will-all-open-behind-inception.html
| work = Los Angeles Times
| publisher = Tribune Company
| accessdate = August 5, 2010
}} 
 1960s Batman television series. {{cite news
| date = May 2, 2010
| author = Yvonne Villarreal
| title = Summer Sneaks: ‘Dinner for Schmucks’. Steve Carell and Paul Rudd, together again
| url = http://articles.latimes.com/2010/may/02/entertainment/la-ca-schmucks-20100502
| newspaper = Los Angeles Times
| pages = 2
}} 
 Le Dîner de Cons (literally, "The Dinner of Idiots"). The film retains many familiar elements of the original, with the basic plot, including the involvement of the taxation authorities and the love triangle around the main character Tim. In the remake, however, Tim is made much more sympathetic (this is the first dinner he has participated in, and he is not in fact having an affair or deliberately cheating on his taxes), and the actual dinner is shown. Director Roach describes the film as "inspired by" the original rather than a remake. {{cite news
| date = July 26, 2010
| last = O’Connell | first = Sean
| title = Interview: "Dinner for Schmucks" director Jay Roach on Steve Carell, Paul Rudd and Sacha Baron Cohen
| url = http://www.hollywoodnews.com/2010/07/26/interview-dinner-for-schmucks-director-jay-roach-on-steve-carell-paul-rudd-and-sacha-baron-cohen/
| work = hollywoodnews.com
| accessdate = August 26, 2010
}} 

===Title=== schmucks which literally means male genitals.
Debbie Schlussel asked whether the title should have been Dinner for List of English words of Yiddish origin|Schlemiels as it would better describe the clumsy character played by Steve Carell. {{cite web
| date = April 7, 2010
| author = Debbie Schlussel
| title = "Dinner For Schmucks": Hollywood Brings Us More Garbage for Summer Movie Season
| url = http://www.debbieschlussel.com/20212/dinner-for-schmucks-hollywood-brings-us-more-garbage-for-summer-movie-season/
| accessdate = August 3, 2010
}}
 
Responding in The New York Times, critic Michael Cieply determined that the intent was to be ambiguous as to which of the two main characters, played by Steve Carell and Paul Rudd, was the intended Idiot (usage)|idiot. {{cite news
| url= http://www.nytimes.com/2010/05/04/movies/04dinner.html
| title= Much Movie Title Meshugas
| author = Michael Cieply
| publisher= The New York Times
| date= May 4, 2010
| accessdate= August 3, 2010
}}
 
In The Forward, Laura Hodes suggested that schmucks correctly referred instead to the behavior of the films antagonists, the bosses of Rudds character. {{cite news
| url= http://www.forward.com/articles/129781/
| title= Of Schmucks and Schlemiels
| author= Laura Hodes
| publisher= The Forward
| date= August 3, 2010
| accessdate= August 3, 2010
}}
 
Schmucks may be fitting after all because the original French play and movie, Le Dîner de cons, which was originally translated as The Dinner Game {{cite web
 |url= http://www.imdb.com/title/tt0119038/
 |title= The Dinner Game
 |publisher= IMDB
 |accessdate=September 9, 2011}}  when released in the USA, would more literally translate to "The Dinner of Cunts . {{cite news
 |url= http://www.guardian.co.uk/film/1999/jul/02/4
 |title=Con trick 
 |work=The Guardian
 |accessdate=January 22, 2011
 |location=London
 |date=July 2, 1999
}} 

==Release==
Dinner for Schmucks was pushed back a week to July 30, 2010, to avoid competition with Salt (2010 film)|Salt and Inception.  

===Marketing===
As part of promoting the film, the website Funny or Die featured videos of Jemaine Clement in character as Kieran Vollard.  
 Death at A Nightmare The A-Team, Grown Ups, and Inception.

==Reception==

===Critical response===
 
The film has received mixed reviews. Review aggregation website Rotten Tomatoes gives a normalized score of 43% based on 183 reviews, with an average score of 5.4/10.  The sites consensus is: "It doesnt honor its source material – or its immensely likable leads – as well as it should, but Dinner for Schmucks offers fitfully nourishing comedy." {{cite web
| url = http://www.rottentomatoes.com/m/dinner_for_schmucks/
| title = Dinner for Schmucks Movie Reviews, Pictures
| work = Rotten Tomatoes
| publisher = Flixster
| accessdate= November 30, 2011
}}
 
Metacritic, which gives a weighted average score out of 100, gives the film a 56% based on 37 reviews. {{cite web
| url = http://www.metacritic.com/film/titles/dinnerforschmucks
| title = Dinner for Schmucks
| work = Metacritic
| publisher = CBS
}}
 

===Box office===
The film made $8.4 million on its first day, ranking number two at the box office, behind Inception.
The film earned $23.5 million on its opening weekend, placing it second overall for the weekend of July 30 to August 1. {{cite web
| title = Dinner for Schmucks (2010)
| url = http://boxofficemojo.com/movies/?id=dinnerforschmucks.htm
| work = Box Office Mojo
| publisher = Amazon.com
| accessdate = November 30, 2011
}}
 
Dinner for Schmucks ultimately grossed $73 million in North America and $13.4 million internationally for a total of $86.4 million worldwide.

===Home media===
Dinner for Schmucks was released on DVD and Blu-ray Disc on January 4, 2011.

==Accolades==
  
"Dinner for Schmucks" was nominated for one award

* 2010 nominated for the Satellite Award for Best Actor.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 