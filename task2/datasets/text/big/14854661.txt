Aashiq (2001 film)
{{Infobox film
| name           = Aashiq
| image          = Aashiqposter.jpg
| image size     =
| caption        =
| director       = Indra Kumar
| producer       = Anil Sharma
| writer         =
| narrator       =
| starring       = Bobby Deol  Karisma Kapoor
| music          = Sanjeev Darshan
| cinematography = K.K. Mahajan
| editing        = Sanjay Sankla
| distributor    = Eros Entertainment
| released       = 26 January 2001
| runtime        = 160 minutes
| country        = India Hindi
| gross          =    
}}

Aashiq (  Bollywood movie directed by Indra Kumar. The film stars Bobby Deol and Karisma Kapoor in the lead. This is a remake of Telugu movie Gulabi (film).

The film premiered on 26 January 2001 in Mumbai, Aashiq was declared Below Average at the Indian box-office. 

==Plot==

Chander (Bobby) helps his friend marry his lover (Sapna) against Sapnas brothers wishes. Sapnas friend Pooja (Karhisma Kapoor) is with Sapna when Chander helps Sapna run away from home. From that moment Pooja falls madly in love with Chander. Sapna and Chanders friend get married. This enrages Sapnas brother who harbors a grudge against Chander & hence creates trouble for him.

Pooja was living in a big house with unlovely parents. Every time her parents met they argued and fought; thinking of Chander made her happy. Pooja secretly called Chander as a "dream girl" and flirted with him, making him happy and curious to find out who she is. Chanders father (Anuphem Kher) was an advocate and loved his only son. It just two of them in the house so their relationship was really close. Chanders father helped him to find out who was the "dream girl" by tricking her to come to a venue. Chander was happy when he learned that the dream girl is Pooja; from then on they were dating.

When Poojas parents come to know their relationship, they dont approve and force Pooja to be engaged with somebody else. There was a misunderstanding between Pooja and Chander when she saw him hit her father. It was an accident, but it made her said harsh things to him. Chander was insulted and angry. When Pooja learned the truth she wanted to make it up to Chander. She asked his childhood friend Jai to bring her to Chander. Jai sold her to a brothel instead. Pooja was missing and the police asked Chander where hes hiding Pooja. Chander didnt know anything and tried to find Pooja until he learned that Jai sold her to a brothel.

Chander saves Pooja when she was almost boarding a plane after being bought by a boss from a Middle East country. Chander fought the ruthless brothels owner, then the brothels owner shoots Chander. Chander could move faster so Jai got shot. The brothels owner was shot by the police inspector, Sapnas brother, who separated duty from personal matters.

==Cast==
*Bobby Deol as Chander Kapoor
*Karisma Kapoor as Pooja
*Rahul Dev as  Baburao (brothels owner)
*Smita Jaykar as  Poojas mom
*Anupam Kher as Dilip Dev Kapoor
*Mrinal Kulkarni as  Neena
*Anjan Srivastav as  Poojas dad
*Johnny Lever
*Dina Pathak as  Dai Maa
*Mukesh Rishi as Neenas dad
*Vrajesh Hirjee
*Ashok Saraf
*Kashmira Shah
* Jasbir Thandi As Jasbir chanders Friend

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Gore Gore Gaal"
| Alka Yagnik
|- 
| 2
| "Aashiq Mujhe Aashiq"
| Roop Kumar Rathod, Alka Yagnik
|- 
| 3
| "Gori Tera Nakhra"
| Alka Yagnik, Udit Narayan
|- 
| 4
| "Teri Aankhon Mein"
| Alka Yagnik, Udit Narayan
|- 
| 5
| "Tum Kya Jaano"
| Alka Yagnik, Udit Narayan
|-
| 6
| "Chhed Do"
| Udit Narayan
|- 
| 7
| "O Mere Dholna" 
| Udit Narayan, Anuradha Paudwal
|-
| 8
| "Charche Hain Hamare"
| Udit Narayan
|-
| 9
| "Mohabbat Ke Din"
| Udit Narayan, Anuradha Paudwal
|}

==References==
 

==External links==
*  

 
 
 
 
 
 
 