Bad Charleston Charlie
{{Infobox film
| name     = Bad Charleston Charlie
| image    = Bad Charleston Charlie movie poster.jpg
| caption  = Style A US movie poster Ivan Nagy 
| producer = Ross Hagen 
| writer   = Ross Hagen Ivan Nagy
| starring = Ross Hagen Kelly Thordsen Hoke Howell Dal Jenkins Carmen Zapata John Carradine
| music    = Luchi De Jesus
| cinematography = Michael Neyman
| editing  = Richard Garritt Walter A. Thompson
| distributor = International Cinema Corp.
| released = 1973
| runtime  = 90 minutes
| language = English
| budget   =
}}
 Ivan Nagy. PG by the Motion Picture Association of America, was distributed by International Cinema Corporation.  The film is loosely based on the life and death of 1920s gangster Charles Birger. Poorly received by both critics and audiences, the film has yet to be released on DVD or Blu-ray Disc.

==Plot==
Set during the 1920s in the Midwestern United States, the movie revolves around two coal miners, Charlie Jacobs (Ross Hagen) and Thad (Kelly Thordsen), who decide to follow a life a crime like their role model, Al Capone.  Jacobs adopts the gangster persona "Bad Charleston Charlie", an anachronistic reference to the 1962 song "Charleston Charlie".  The duo has to deal with forming a gang, learn to handle "wild women", bribe corrupt officials, and battle rival gangs plus the Ku Klux Klan.   They find that they are no better at being gangsters than they were at mining coal.   The film features a cameo from legendary actor John Carradine as a drunken reporter. 

==Tagline==
The films advertising tagline was "The most desperate gang of all...ALMOST!"

==Cast==
*Ross Hagen ... Charlie Jacobs     
*Kelly Thordsen ... Thad
*Hoke Howell ... Claude
*Dal Jenkins ... Ku Klux Klan Leader
*Carmen Zapata ... Lottie
*Mel Berger ... Fat Police Chief
*John Carradine ... Alcoholic reporter 
*Ken Lynch ... Sheriff Koontz
*Tony Lorea ... Criminal
*Christopher George ... special appearance
 Ultra Violet. 

==Production info== Ivan Nagy and produced by Ross Hagen.     The independent film was produced through a joint venture between Triforum Inc., a company wholly owned by Hagen and Nagy, and Studio 9 Productions.   The production encountered financing problems when, in May 1972, the U.S. Securities and Exchange Commission filed a complaint against Studio 9 Productions, Bad Charleston Charlie Associates, and others for "violations of the registration and anti-fraud provisions of the Federal securities laws in connection with the offer and sale of Studio 9 Productions common stock and Bad Charleston limited partnership interests."   On June 5, 1972, Bad Charleston Charlie Associates and Studio 9 Productions "consented to a finding" (without admitting guilt) that they had violated Federal law and they accepted a final judgment of permanent injunction against future such sales. 
 PG (parental guidance suggested) rating from the Motion Picture Association of America.  

==The Real Charlie==
  Shachna Itzik Prohibition when Williamson County. The rival gangs worked together to violently purge the KKK from the area, gunning down its local leaders in 1925 and 1926. With the Klan gone by late 1926, Birger returned to his fight with the Shelton Brothers. When he discovered that Joe Adams, the mayor of West City, Illinois, was assisting the Sheltons, Birger threatened Adams and ultimately ordered his murder. Birger was arrested in June 1927, convicted, and hanged in April 1928. He was to be the last man executed by public hanging in the state of Illinois.   (Another Illinois convict, Charles Shader, was executed by hanging in October 1928 but that execution was not open to the public.) 

==Reception==
===Release=== benefit screening Westwood neighborhood of Los Angeles. The event raised money for the Oakhill School for Emotionally Handicapped Children and the Reiss-Davis Clinic.  The film began playing in the greater Los Angeles area soon after with a national rollout in June 1973. 

===Critical reception===
Reviewing the film in May 1973 on its initial release,   rates the film at zero-stars citing "incompetent" direction and noting that Bad Charleston Charlie "is said to have been   401st movie" but that he "should have quit at 400"." 
 evening dress with beaded hip drape and circle sleeves", a cloche hat "with contrasting pleated band and accented with feathers, sequins, and a flower", plus a "single-strand beaded necklace". 

===Home video===
Bad Charleston Charlie was first released on home video on September 16, 1987, by Home Cinema Corporation. The movie was made available in both the VHS and Betamax videocassette formats. 

In July 2001, LSI Communications Inc., a company then best known for releasing the Karl Malones Body Shop series of workout videos, acquired the rights to Bad Charleston Charlie (along with Gregorio and His Angel starring Broderick Crawford and The Caged Man) from Cinevision Inc. The terms of the deal were not disclosed.  LSI Communications merged with Peregrine Inc. using a reverse stock swap in February 2002 and shed a number of assets in the ensuing reorganization.   In late 2008, the Los Angeles Times reported that this "lousy 1970s movie" was available on videotape but not on DVD. 

Current rights holder Troma Entertainment notes that Bad Charleston Charlie is "Currently unavailable on video".   , the film has yet to be released on DVD or Blu-ray disc. Selected clips from the film can be seen as part of the Ross Hagen-directed 1985 compilation horror movie Reel Horror, available on DVD from Peacock Films. 

==References==
{{reflist|2|refs=
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}}

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 