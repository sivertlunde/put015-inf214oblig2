Roberto Succo (film)
{{Infobox film
| name           = Roberto Succo
| image          = Roberto Succo.jpg
| image_size     = 
| caption        = 
| director       = Cédric Kahn
| producer       = Ruth Waldburger
| writer         = Cédric Kahn Pascale Froment
| narrator       = 
| starring       = Stefano Cassetti
| music          = 
| cinematography = Pascal Marti
| editing        = Yann Dedet
| distributor    = 
| released       = 14 May 2001
| runtime        = 124 minutes
| country        = France
| language       = French
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 2001 film eponymous serial killer. The film was adapted from the book Je te tue. Histoire vraie de Roberto Succo assassin sans raison by Pascale Froment. The film was entered into the 2001 Cannes Film Festival.   

==Cast==
* Stefano Cassetti - Roberto Succo
* Isild Le Besco - Léa
* Patrick DellIsola - Thomas
* Viviana Aliberti - Swiss teacher
* Estelle Perron - Céline
* Leyla Sassi - Cathy
* Catherine Decastel - Patricia
* Olivia Carbonini - Girl at the Etna
* Basile Vuillemin - Lenfant
* Brigitte Raul - Childs mother
* Marius Bertram - Cab driver
* Aymeric Chauffert - Aelaunay
* Vincent Dénériaz - Denis
* Yelda Reynaud - Mylène
* Philippe Bossard - Magistrate

==External links==
* 

==References==
 

 
 
 
 
 
 
 

 
 