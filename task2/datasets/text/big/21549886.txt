The Lady with the Dog (film)
 
{{Infobox film name = The Lady with the Dog image	= The Lady with the Dog FilmPoster.jpeg caption = Film poster director = Iosif Kheifits producer =  writer = Anton Chekhov (story) Iosif Kheifits starring = Iya Savvina music = Nadezhda Simonyan cinematography = Dmitri Meskhiyev Andrei Moskvin editing = Ye. Bazhenova distributor = Lenfilm released =   runtime = 83 minutes country = Soviet Union language = Russian budget = 
}}

The Lady with the Dog ( , Transliteration|translit.&nbsp;Dama s sobachkoy) is a 1960 Soviet drama film directed by Iosif Kheifits. It was entered into the 1960 Cannes Film Festival.     

==Plot==
The film is set in the 19th Century at Yalta, the popular Russian  resort on the Black Sea. Dmitri  Gurov, Moscow banker, meets Anna Sergeyovna from Saratov, also vacationing.  Beautiful Anna walks her dog daily to the delight of the men who observe her.  Both Dmitri and Anna are married and both are unhappy in their situations.  Both have come to Yalta without their spouses.  A romance soon blooms into an affair.  After their summer romance ends, both return to their marriages.

Dmitri returns to his former life, bored with working and going to his club to play cards.  He is haunted by Anna’s memory.  At Christmas, Dmitri tells his wife he is going to St. Petersburg on business but actually goes to Saratov where he finally locates Anna who is attending an opera with her husband.  Seizing an opportunity, he surprises her.  She fears detection and promises to meet him in Moscow in a few weeks. Anna meets Dmitri in Moscow, their love fully rekindles, but they are frustrated with the reality that their marriages cannot be dissolved and they must meet secretly from then on.  They make tentative plans to meet in the future.  The closing scene has Anna looking from the window of her Moscow room while Dmitri leaves in the cold of the Russian winter.  

==Cast==
* Iya Savvina as Anna Sergeyеvna
* Aleksey Batalov as Dimitri Gurov
* Nina Alisova as Madame Gurov
* Pantelejmon Krymov as von Didenitz Yuri Medvedev
* Vladimir Erenberg
* Yakov Gudkin
* D. Zebrov as Frolov
* Mariya Safonova as Natasha
* G. Barysheva
* Zinaida Dorogova
* Kirill Gun Mikhail Ivanov
* G. Kurovsky
* Svetlana Mazovetskaya Aleksandr Orlov
* Pavel Pervushin
* T. Rozanov
* Lev Stepanov
* Yuri Svirin

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 