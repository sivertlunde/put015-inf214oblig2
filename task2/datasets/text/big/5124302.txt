Footpath (2003 film)
 
 

{{Infobox film
| name = Footpath
| image = Foothpath 2003.jpg
| caption = DVD cover
| director = Vikram Bhatt
| producer = Mahesh Bhatt Mukesh Bhatt
| writer = Mahesh Bhatt Girish Dhamija
| starring = Aftab Shivdasani  Bipasha Basu  Rahul Dev  Emraan Hashmi
| music = Nadeem-Shravan  Himesh Reshammiya
| cinematography = Pravin Bhatt
| editing = Akiv Ali
| distributor = Tips Films Pvt. Ltd.
| released =  
| runtime = 160 minutes
| country = India
| language = Hindi
| gross = 
}} State of Grace.

==Plot==
Arjun Singh (Aftab Shivdasani) and the Srivastav brothers, Raghu (Emraan Hashmi) and Shekhar (Rahul Dev), are neighbors in a gangster-prone area in Mumbai. When Arjuns union leader father is killed, the brothers urge him to avenge his death. They get a sword and find the killers and kill them. Arjun is the prime suspect in this homicide and the brothers get him to run to Delhi, where he begins a new life as a Real Estate Agent, Mohan Kumar Sharma. Years later, Arjun returns to Mumbai and is welcomed with open arms by Raghu and Shekhar, who are now leading gangsters by their own right. Arjun also renews his romance with the estranged Srivastavs sister, Sanjana (Bipasha Basu). Sanjana would like Arjun and her brothers to go straight, and Arjun agrees with her and he starts to work on Raghu - the more flexible of the two - and partially succeeds - especially since Raghu is romantically involved with a school-teacher, who will have nothing to do with him unless he gives up all criminal activity. Raghu is seriously considering going straight when Shekhar gives him the devastating news, that Arjun is not who he claims to be - but a plainclothes police officer, who is out to get them by hook or by crook.

==Cast==
*Aftab Shivdasani as Arjun Singh / Mohankumar Sharma
*Emraan Hashmi as Raghu Srivastav
*Rahul Dev as Shekhar Srivastav
*Bipasha Basu as Sanjana Srivastav
*Anup Soni as Police Inspector Singh
*Irrfan Khan as Sheikh
*Aparna Tilak as the English Teacher
*Anupama Verma as Pamela
*Arif Zakaria as Police Officer

==Soundtrack==
The films music was composed by Nadeem-Shravan and Himesh Reshammiya with lyrics by Sameer (lyricist)|Sameer. The songs were popular with " Zara Dekh Mera Deewanapan" becoming a rage. 
# Saari Raat Teri Yaad - Udit Narayan, Alka Yagnik
# Dil To Milte Hain - Alka Yagnik
# Dosti Miltey Hai - Kumar Sanu KK music by Himesh Reshammiya
# Kitna Pyara Hai Sama - Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
# Zara Dekh Mera Deewanapan - Udit Narayan, Alka Yagnik
# Chaain Aapko Mila - S.P Balasubhramanyam, Asha Bhosle.(The same song also featured in Priyadarshans Hungama, with Shaan and Sadhana Sargam as vocalists)

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 