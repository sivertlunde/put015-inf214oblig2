Nameless Gangster
{{Infobox film
| name           = Nameless Gangster: Rules of the Time
| image          = NamelessGangster2012Poster.jpg
| caption        = Promotional poster
| film name = {{Film name
 | hangul         =    
 | hanja          =  와의  
 | rr             = Bumjoewaui Junjaeng
 | mr             = Pŏmjoewa-ŭi Chŏnjaeng}}
| director       = Yoon Jong-bin 
| producer       = {{plainlist |
* Park Shin-gyoo
* Yoo Jeong-hoon}}
| writer         = Yoon Jong-bin
| starring       = Choi Min-sik Ha Jung-woo
| music          = Jo Yeong-wook
| cinematography = Go Nak-seon
| editing        = Kim Sang-beom Kim Jae-beom
| studio         = Palette Pictures Showbox
| distributor    = Showbox
| released       =  
| runtime        = 133 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =  
}} gangster film directed by Yoon Jong-bin starring Choi Min-sik and Ha Jung-woo. The film is set in the 1980s and ’90s in Busan when corruption and crime was so rampant that the government declared war on it in 1990. 

Time (magazine)|Time praised the film, calling it "the Korean mob film Scorsese would be proud of." 

== Plot == hiropon (crystal Choi Bon-gwan|family clan from Gyeongju and the two form a close relationship. Ik-hyun leaves his customs job and becomes a full-time businessman, with Hyung-bae taking care of the underworld side and Ik-hyun protecting him with his high-level contacts. In the mid-80s the two forcibly take over a nightclub run by Miss Yeo (Kim Hye-eun) that is on the turf of Pan-ho. Following his humiliation, Pan-ho has the club raided by the police and Hyung-bae arrested, though Ik-hyun gets Hyung-bae released by using the Choi clan connection with Seoul public prosecutor Choi Joo-dong (Kim Eung-soo). In May 1987, Ik-hyun and Hyung-bae take their business to the next level, formally linking up with Japans yakuza and having a connection with a hotel-casino, the Daedong, that is also on Pan-hos turf. Pan-ho threatens a gang war with Hyung-bae, and Ik-hyun is forced to decide where his loyalties and self-survival lie. 

== Cast ==
* Choi Min-sik as Choi Ik-hyun   
* Ha Jung-woo as Choi Hyung-bae, a gang boss
* Cho Jin-woong as Kim Pan-ho, a gang boss
* Kwak Do-won as Jo Beom-seok, a public prosecutor
* Ma Dong-seok as Mr. Kim, brother-in-law of Ik-hyun
* Kim Sung-kyun as Park Chang-woo, Hyung-baes gang underboss
* Kim Jong-goo as Jo Bong-goo, a customs chief officer
* Kim Jong-soo as Mr. Jang, a customs officer
* Kwon Tae-won as Heo Sam-shik, co-owner of the nightclub
* Kim Hye-eun as Miss Yeo, co-owner of the nightclub Kim Eung-soo as Choi Joo-dong, a public prosecutor
* Song Young-chang as Mr. Han, a lawyer
* Takeshi Nakajima as Jaidoku Ganeyama, a yakuza boss
* Lee Cheol-min as Hyung-baes gang member
* Go In-beom as Choi Moo-il, Hyung-baes father
* Kim Young-seon as wife of Ik-hyun
* Park Byung-eun as adult Choi Joo-han, son of Ik-hyun

== Production ==
Choi Min-sik reportedly put on 10 kilograms for his role and Ha Jung-woo spent 8 hours putting on the fake body tattoos featured in the film. 

== Reception ==
Nameless Gangster attracted over 4 million admissions in 26 days of release. It drew 1 million in four days, 2.5 million in 11 days, 3 million in 17 days, and 3.5 million in 20 days.  According to data provided by Korean Film Council (KOFIC) it topped the list of ten most-watched films in South Korea in the first quarter of 2012, with a total of 4.6 million admissions. 

Its success is notable as it was screened in February (traditionally a low season for movies), rated R, and screened less frequently due to its long running time of 133 minutes. Korean Film Biz Zone stated "the film has elements working against it for box office success. Despite that, "the return of" Choi Min-sik melded with top-rated actor of the day Ha Jung-woo is impressive. The two actors presence is backed up by a solid supporting cast, giving the film a distinct flavor of a gangster classic."   

The film ranked #1 for three weeks, two of which were consecutive. It grossed   in its first week of release,  and grossed a total of   after nine weeks of screening. 

== Awards and nominations == 2012 Baeksang Arts Awards
* Best New Actor  - Kim Sung-kyun  
* Nomination - Best Film
* Nomination - Best Director - Yoon Jong-bin
* Nomination - Best Actor - Choi Min-sik
* Nomination - Best New Actress - Kim Hye-eun
* Nomination - Best Screenplay - Yoon Jong-bin

2012 Buil Film Awards
* Best Actor - Choi Min-sik
* Best Supporting Actor - Cho Jin-woong
* Best New Actor - Kim Sung-kyun
* Nomination - Best Film
* Nomination - Best Director - Yoon Jong-bin
* Nomination - Best Actor - Ha Jung-woo
* Nomination - Best Supporting Actor - Kwak Do-won
* Nomination - Best New Actress - Kim Hye-eun
* Nomination - Best Screenplay - Yoon Jong-bin
* Nomination - Best Cinematography - Go Nak-seon
* Nomination - Best Art Direction - Cho Hwa-sung
* Nomination - Best Music - Jo Yeong-wook

2012 Grand Bell Awards
* Nomination - Best Actor - Choi Min-sik
* Nomination - Best Supporting Actor - Kim Sung-kyun
* Nomination - Best New Actor - Kim Sung-kyun

2012 Korean Association of Film Critics Awards 
* Best Screenplay - Yoon Jong-bin

2012 Asia Pacific Screen Awards  Best Actor - Choi Min-sik film prize)
 2012 Blue Dragon Film Awards   
* Best Actor - Choi Min-sik  
* Best Screenplay - Yoon Jong-bin
* Best Music - Jo Yeong-wook
* Popularity Award - Ha Jung-woo
* Nomination - Best Film
* Nomination - Best Director - Yoon Jong-bin
* Nomination - Best Actor - Ha Jung-woo
* Nomination - Best Supporting Actor - Kwak Do-won
* Nomination - Best New Actor - Kim Sung-kyun
* Nomination - Best Cinematography - Go Nak-seon
* Nomination - Best Art Direction - Cho Hwa-sung
* Nomination - Best Lighting - Lee Seung-won

2012 Korean Culture and Entertainment Awards  
* Best New Actor - Kim Sung-kyun

2012 Busan Film Critics Awards
*Best Film

2013 KOFRA Film Awards (Korea Film Reporters Association)   
*Best Actor - Choi Min-sik
 2013 Asian Film Awards   Best Actor - Choi Min-sik Best Supporting Actor - Ha Jung-woo Best Newcomer - Kim Sung-kyun
* Nomination - Best Screenplay - Yoon Jong-bin
* Nomination - Best Art Direction - Cho Hwa-sung
* Nomination - Best Original Score - Jo Yeong-wook

== Soundtrack ==
{{Infobox album  
| Name       = Original Motion Picture Soundtrack from Nameless Gangster: Rules of the Time
| Type       = soundtrack
| Artist     = Jo Yeong-wook
| Cover      = 
| Alt        = 
| Released   =  
| Recorded   =  rock
| Length     =  
| Label      = Ohgam Entertainment
| Producer   = Jo Yeong-wook Hong Dae-seong Jeong Hyun-soo Seok Seung-hee    
| Last album = 
| This album = 
| Next album = 
}} end credits sequence of the film, and was released on January 13, 2012 as a Single (music)|single.  

=== Track listing ===
# War on Crime Part 1 (범죄와의 전쟁 Part 1) – 2:33
# Year 1982 (1982년) – 2:28
# Ourselves (우리끼리) – 1:55
# Thugs (양아치) – 2:53
# Memories of a Beach (해변의 추억) – 4:03
# Road (길) – 1:52
# Gangsters (건달들) – 2:28
# War on Crime Part 2 (범죄와의 전쟁 Part 2) – 2:16
# Pedigree (족보) – 2:48
# Road of the War – 1:45
# Crossroad (교차로) – 2:38
# Target (타겟) – 2:15
# To Another World (또 다른 세상으로) – 3:02
# Cold Wall (차가운 벽) – 1:33
# God of Lobbying (로비의 신) – 2:03
# War on Crime Part 3 (범죄와의 전쟁 Part 3) – 4:10
# Profitable Suggestion (유익한 제안) – 2:52
# Dead-end Road (막다른 길) – 2:05
# War on Crime Part 4 (범죄와의 전쟁 Part 4) – 5:49
# Sun Filled (태양은 가득히) – 1:54
# In My Soul of Souls – 4:21
# War on Crime Part 5 (범죄와의 전쟁 Part 5) – 2:44
# I Heard a Rumor (풍문으로 들었소) – 3:16

== In popular culture == parodied in many popular variety shows on TV, such as Gag Concert and Infinite Challenge. Comedians aped the film characters hairstyle and fashion, and the use of the phrase "Sara Itne" ( , akin to meaning fresh) and the song "I Heard a Rumor." 

It was also parodied by members of the boy band   concerts in August 2013. 

== References ==
 

== External links ==
*    
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 