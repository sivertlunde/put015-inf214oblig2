Rurouni Kenshin (film)
{{Infobox film
| name = Rurouni Kenshin
| image = Rurouni Kenshin (2012 film) poster.jpg imagesize =  
| caption = Theatrical release poster
| alt = 
| director = Keishi Ōtomo   
| producer = Osamu Kubota 
| screenplay = Kiyomi Fujii Keishi Otomo  
| based on = Rurouni Kenshin by Nobuhiro Watsuki
| starring = Takeru Satoh Emi Takei Yosuke Eguchi Munetaka Aoki Teruyuki Kagawa Koji Kikkawa
| music = Naoki Satō 
| cinematography = Takuro Ishizaka 
| editing = Tsuyoshi Imai  
| distributor = Warner Bros. 
| released =    
| runtime = 135 minutes 
| country = Japan
| language = Japanese
| budget = US$20 million
| gross = US$61.7 million 
}} manga of Hitokiri Battōsai. After participating in the Bakumatsu war, Kenshin wanders the countryside of Japan offering protection and aid to those in need as atonement for the murders he once committed as an assassin.

Rumors circulated of a live action adaptation of the manga before it was announced. The Sankei Sports newspaper adds that the staff aims to release the film internationally and eventually make a series. This will be the first live-action adaptation of the manga. During the production, Nobuhiro offered his ideas for the movie, which were used in the filming.  The film was distributed internationally by Warner Bros.

Rurouni Kenshin was theatrically released on August 25, 2012 in Japan, grossing over $36 million in that country and over $60 million worldwide as of November 2012. It was released in DVD on December 26, 2012.    The film has been licensed for distribution in over 60 countries in Europe, Latin America and Asia.  The movie premiered in North America as an opening selection for the 2012 LA EigaFest in December 14, 2012.   

==Plot==
As the Imperialist forces celebrated their victory in the Battle of Toba-Fushimi, the participant Hitokiri Battōsai walks away from the battlefield, abandoning his sword. But, the Battōsais old katana is not left alone. It is claimed by one of the fallen, Udo Jin-e.

A decade later, Saitō Hajime (Rurouni Kenshin)|Saitō Hajime and his fellow policemen investigate the murder of an undercover cop supposedly by the hands of the Battōsai. But, Saitō is not convinced and suspects Takeda Kanryū, a wealthy, but cruel businessman.

Meanwhile, the former Hitokiri Battōsai (now calling himself Himura Kenshin) arrives in Tokyo. While roaming its streets, he meets Kamiya Kaoru, the owner of her late fathers Kendo school. With her dojos name smeared by one bearing the name of Battōsai, she attacks him believing him to be the famed killer, but is proven wrong when Kenshin reveals he only carries a  .

Elsewhere, a woman Takani Megumi forced to make opium for Takeda Kanryū, escapes and turns to the police for a safe haven after witnessing the deaths of the other opium makers. However, Udō Jin-e, under the service of Kanryū, hunts her down, slaying everyone in the police station. Luckily, she escapes in the ensuing chaos.

Kaoru crosses paths with Jin-e, the actual perpetrator for killing under her dojos style of swordsmanship. Utterly no match for him, she is injured in the fight, but Kenshin appears out of nowhere and saves her. Jin-e immediately realizes Kenshins hidden identity as the true Battōsai, before a swarm of policemen rush onto the scene, giving Kenshin and Kaoru a chance to flee. Kaoru leads Kenshin to her dojo where they would be safe.

Next, a group of thugs under Takeda Kanryū attempt to take over the dojo. After revealing his identity as the former Battōsai, Kenshin beats down the entire gang without killing a single one before the police arrive. Kenshin blamed himself on the incident and got himself arrested help Kaorus dojo from being blamed for the violence. Soon, Saitō recognizes him and releases him without charge. After his release, he is greeted by Kaoru who knows Kenshin is not the Battōsai who had defamed her dojo and takes him back to the dojo. Kenshin afterward moves in with Kaoru and her only student, the boy Myōjin Yahiko.

Still running on the streets for her life, Megumi runs into Yahiko who helps hide her and brings her to the dojo where she is introduced to everyone. Kaoru treats everyone to a sukiyaki dinner at the Akabeko restaurant, only to have the occasion spoiled by Kanryū coming and offering to hire Kenshin, who humbly declines. Here, he is challenged by Sagara Sanosuke for the job and they leave the establishment to fight.

Later that evening, Jin-e goes on a killing spree leaving many corpses for the police to find the following day. Kenshin witnesses the horror, as well as a woman mourning the death of her lover. This invokes a memory for Kenshin from his years as an assassin when he witnessed a woman mourning a man that he had killed, a sword fight that left a scar on his face. Later that night, a masked man working for Kanryū warns Megumi of coming dangers.

The next day the people around the dojo fall ill from rat poison contaminating the community wells. Megumi suspects its Kanryūs doing and helps by providing medication for the victims. Angry at Kanryū, Megumi attempts to kill him, but fails and is, instead, held hostage by the wealthy drug-dealer. Besting all his men, Kenshin and Sanosuke attack Kanryūs mansion in hopes of rescuing Megumi. Saitō assists them to subdue Kanryū, who is armed with a Gatling gun. They rescue Megumi, but discover that Jin-e, the fake Battōsai and Kanryūs man, has kidnapped Kaoru.

Kenshin pursues Jin-e. To further provoke Kenshin, Jin-e uses a special technique that paralyzes Kaorus lungs and can only be undone with his death. After an intense battle, Kenshin critically injures Jin-e by shattering his elbow with his scabbard. Before Kenshin could land the killing blow, Kaoru overcomes the paralysis and stops Kenshin from killing Jin-e. Jin-e commits suicide, telling Kenshin before his last breath that he who lives by the sword must die by the sword, a re-occurring theme, counter to Kenshins vow never to kill again.

Kenshin, tired and wounded, carries the unconscious Kaoru back to the dojo. They rest, in the care of Megumi and Yahiko. Upon waking up, Kaoru does not see Kenshin and goes in search for him. She finds him carrying back some vegetables, and is relieved of her fear that he may have left permanently.

==Cast==
* Takeru Satoh as Himura Kenshin, a former assassin turned wanderer who has made a vow never to kill again.
* Emi Takei as Kamiya Kaoru, the owner of a Kendo school left to her by her father.
* Munetaka Aoki as Sagara Sanosuke, a street fighter who befriends Kenshin.
* Teruyuki Kagawa as Takeda Kanryū, a ruthless businessman bent on controlling organized crime.
* Yu Aoi as Takani Megumi, one of Kanryūs forced opium-makers, though from a famed family of doctors and healers. 
* Koji Kikkawa as Udō Jin-e, one of Kanryūs men, and a survivor of the Battle of Toba-Fushimi.  Gou Ayano as Rurouni Kenshin characters#Gein|Gein, another of Kanryūs men.
* Genki Sudo as Inui Banjin, another of Kanryūs men.
* Taketo Tanaka as Myojin Yahiko, Kaorus only student at the dojo.
* Yosuke Eguchi as Saitō Hajime (Rurouni Kenshin)|Saitō Hajime, a former member of the Shinsengumi who now works for the Meiji government as a police officer under the name of Fujita Goro.
* Eiji Okuda as Yamagata Aritomo, a ranking member of the Meiji government and Saitōs superior.

==Production==
On June 28, 2011, a live-action film adaptation was announced.  Produced by Warner Bros., with actual film production done by Studio Swan, the film was directed by Keishi Ōtomo and stars Takeru Satoh (of Kamen Rider Den-O fame) as Kenshin, Munetaka Aoki as Sanosuke Sagara and Emi Takei as Kaoru.  The film was released on August 25, 2012,  and the staff "aims to release the film internationally and eventually make a series.

After choosing Takeru Satoh as the lead, Producer Shinzō Matsuhashi commented, "Satoh has the looks and stature to be a proper Kenshin." Watsuki added that when this project was just starting, he and his wife were discussing who should play Kenshin, and decided that Satoh topped the list. 

Satoh later commented, “The role of Kenshin is that of a well-known character, therefore, I think fascinating acting is needed. I would like to create the Kenshin image with the staff, while staying true to the details. I will try my best, so please look forward to it.”

Nobuhiro Watsuki praised on Satoh being cast for the role: “When this project just started, my wife and I were talking about who would suit the role of Kenshin, and Satoh Takeru-san was the one who came up on our mind first. So, when it was confirmed (that Satoh will be taking the role), I was surprised, but was also very happy. I’m looking forward to seeing his wonderful acting.”

==Release==
Rurouni Kenshin was theatrically released on August 25, 2012 in Japan. The film was released in South Korea for the Busan International Film Festival on October 5, 2012. Released for Spain in the annual Sitges Film Festival on October 10, 2012. The film debuted in Hong Kong on December 6, 2012. It was also theatrically released in the Philippines on December 5, 2012 (SM Cinema) gaining second place in the Philippine Box Office on its first week.

It was released in DVD on December 26, 2012.  The film has been licensed for distribution in over 60 countries in Europe, Latin America and Asia. Limited edition came in a special box, with special digipack, a soundtrack, and a Rurouni Kenshin notebook. Other content also include cast and staff commentary, TV spots, behind-the-scenes, and all the trailers included, plus One OK Rocks PV of their song "The Beginning".

The film was released in North America in December 14, 2012 for the LA EigaFest 2012 and was held in conjunction with the American Cinematheque at the Egyptian Theatre in Hollywood. The director, Keishi Ōtomo, attended the premiere and opening red carpet ceremony. In addition to Rurouni Kenshin, the 2012 line up features some of the films to come out of Japan over the last year. A special screening of four selected short films will be presented in collaboration with the Short Shorts Film Festival & Asia. 

The film was screened in the UK at October 4, 2013.

==Reception==

===Box office===
The film has performed extremely well, earning $36,798,585 in Japan in its debut and US$25 million in a limited international release, for a total gross of US$61.7 million.     The film garnered to become Japans sixteenth highest-grossing film of the year. 

===Critical reception===
The film received positive reviews from critics.
Deborah Young from The Hollywood Reporter praised the film in the Busan Film Festival,  saying that the "choreography is fast and furious and the sword fights ably showcase Battosais incredible skills. Sato Naoki’s energetic score pounds out the action scenes to a barbarian beat." Ko Ransom of Anime News Network gave the film a "B" rating, describing it as "an accessible, crowd-pleasing hit for the end of summer, and it certainly has managed to do that. Its action scenes are fun but not too intense, its characters are appealing, and it presents a very polished product overall. While it wont let down most fans looking for a recreation of their beloved characters and scenes, it still works as a standalone movie, though it does do plenty to set up a likely sequel. While its hard to believe that Kenshin will be the trailblazer its advertising makes it out to be, its certainly a good way to spend a hot summer afternoon." 

Nobuhiro Watsuki overall praised the film. During an interview with Weekly Shonen Jump Alpha, he commented," It was right on! Takeru Satoh played Kenshin’s dual personality well. He truly was Kenshin. Yosuke Eguchi who played Saitō Hajime, Koji Kikkawa who played Udō Jin-e, and Munetaka Aoki who played Sagara Sanosuke really took on their characters’ expressions and movements in the action sequences. It drew me in, especially the scene with Saitō’s Gatotsu pose. It sent chills down my spine! I thought Emi Takei playing Kamiya Kaoru was really cute and Yu Aoi playing Takani Megumi was also fantastic", and that the film itself has "parts I felt I didn’t explain well enough in the manga were improved. There were actually lines in the movie that made me think “I want to use that line in the manga!"

==Sequels==
 
When the film was first announced, it was reported that the production team had hopes to create a series. 
 Nenji Kashiwazaki/Okina, Toshimichi Ōkubo, Hirobumi Itō, Yumi Komagata and Chō Sawagejō. Kaito Ōyagi has replaced Taketo Tanaka as Yahiko.  On August 30, 2013, Tao Tsuchiya was announced as popular character Makimachi Misao.  On September 20, 2013, executive producer Hiroyoshi Koiwai revealed that model Lisa Ulliel is cast in a secret role. 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 