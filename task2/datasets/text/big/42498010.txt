Reloaded (film)
{{Infobox film
| name           = Reloaded
| image size     = 
| image	         = 
| alt            = 
| caption        = 
| director       =  
| producer       = Emem Isong
| writer         = 
| screenplay     =  
| story          = Emem Isong
| narrator       = 
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}
Reloaded is a 2009 Nigerian romantic drama film directed by Lancelot Oduwa Imasuen & Ikechukwu Onyeka, starring
Ramsey Nouah, Rita Dominic, Desmond Elliot, Stephanie Okereke, Ini Edo and Nse Ikpe Etim.   It received 3 nominations at the 5th Africa Movie Academy Awards. 

==Cast==
*Ramsey Nouah as Femi
*Desmond Elliot as Osita
*Rita Dominic as Chelsea
*Stephanie Okereke as Weyinmi
*Ini Edo as Tayo
*Van Vicker as Bube
*Uche Jombo as Tracy
*Nse Ikpe Etim as Omoze
*Monalisa Chinda as Abbey
*Enyinna Nwigwe as Edwin
*Mbong Amata (née Odungide) as Nira
*Temisan Isioma Etsede as Otis
*Emeka Duru as Gabriel
*Princess Anazodo as Bube’s Mum
*Ahmed Aitity as Shola
*Martha Iwoo as Ifeyinwa
*Ikechukwu Onyeka as Doctor

==Reception==
Nollywood Reinvented gave it a 3 out of 5 star rating. The reviewer remarked that although he has seen the film several times, he never gets tired of it.  NollywoodForever gave it an 93% rating. The reviewer praised the movies pacing and flow, and enjoyed the dance scene at the end. 

==References==
 

 
 
 
 


 