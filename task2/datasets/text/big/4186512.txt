"Weird Al" Yankovic: The Ultimate Collection
  }}
{{Infobox film
  | name = "Weird Al" Yankovic: The Ultimate Collection
  | image = Weird Al Yankovic The Ultimate Collection.jpg
  | caption = VHS cover
  | director = Francis Delia Janet Greek Jay Levey Scott Norlund Mark Osborne Dror Soref Robert K. Weiss "Weird Al" Yankovic
  | producer =
  | writer =
  | starring = "Weird Al" Yankovic
  | music = "Weird Al" Yankovic
  | cinematography =
  | editing =
  | distributor =
  | released =  
  | runtime =
  | language = English
  | budget =
}}

"Weird Al" Yankovic: The Ultimate Collection is a  .

The VHS contains 16 music videos:
* "Fat (song)|Fat" (from Even Worse album)
* "Smells Like Nirvana" (from Off the Deep End album) Like a Surgeon" (from Dare to Be Stupid album)
* "Eat It" (from "Weird Al" Yankovic in 3-D album)
* "Living with a Hernia" (from Polka Party! album) Dare to Be Stupid" (from Dare to Be Stupid album) This Is the Life" (from Dare to Be Stupid album)
* "I Lost on Jeopardy" (from "Weird Al" Yankovic in 3-D album)
* "I Love Rocky Road" (from "Weird Al" Yankovic (album)|"Weird Al" Yankovic album)
* "Christmas at Ground Zero" (from Polka Party! album)
* "Ricky (song)|Ricky" (from "Weird Al" Yankovic (album)|"Weird Al" Yankovic album)
* "One More Minute" (from Dare to Be Stupid album) Jurassic Park" (from Alapalooza album)
*"Bedrock Anthem" (from Alapalooza album)
*"UHF (song)|UHF" (from UHF - Original Motion Picture Soundtrack and Other Stuff|UHF album) You Dont Love Me Anymore" (from Off the Deep End album)

 

 
 
 
 
 
 
 
 


 