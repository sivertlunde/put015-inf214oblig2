The Girl Hunters (film)
  Roy Rowland, novel of the same name.

Spillane himself stars as private detective Mike Hammer, one of the few occasions in film history in which an author of a popular literary hero has portrayed his own character. It also starred Bond girl Shirley Eaton (Goldfinger (film)|Goldfinger),  veteran actor Lloyd Nolan, and syndicated newspaper columnist Hy Gardner as himself.

The film features examples of product placement when Spillane and Nolan share a couple of cans of Pabst Blue Ribbon.  Also, during the first newsstand scene, Mad Magazine #77 (March 1963) is clearly visible.
 Billy Hill in London and invited him to the film set.  According to Spillane, Hill provided firearms that were used in the film.  Spillane also noted that the producers surrounded him with actors who were shorter than he was.
 The Snake but it was never made. 

==Plot==
Ever since his loved and lovely assistant Velda has gone missing, private detective Mike Hammer has been drinking steadily. For seven years he hasnt been on a case, but that changes when his longtime pal from the police, Capt. Pat Chambers, asks his assistance on a job.

Hammer is needed to talk with Richie Cole, a dying sailor who refuses to speak with anybody else. According to federal agent Art Rickerby, not only has Richie been shot by the same gun recently used to kill a politician, he is actually an undercover federal agent.

The trail leads to Laura Knapp, the late senators widow. She is beautiful and she is seductive, but Hammer does not trust her, even making sure a shotgun that ends up in her hands is rigged by mud blocking the two barrels to backfire on anybody who pulls the trigger. Laura fires the shotgun at him, and Hammer gets his answer.

==Cast==
* Mickey Spillane as Mike Hammer
* Shirley Eaton as Laura
* Lloyd Nolan as Rickerby
* Hy Gardner as Himself
* Scott Peters as Pat Chambers

==Quotes==
I never hit dames...I always kick them - Mike Hammer

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 


 
 