Restoration (2011 film)
{{Infobox film
| name           = Restoration בוקר טוב אדון פידלמן
| image          = FIDELMAND700 100.jpg
| alt            =  
| caption        = Theatrical poster
| director       = Yossi Madmoni
| producer       = 
| writer         = Erez Kav-El Henry David Nevo Kimhi Rami Danon
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 
| country        = Israel
| language       = Hebrew
| budget         = 
| gross          = 
}}
Restoration is a 2011 Israeli film directed by Yossi Madmoni.  The Hebrew title is בוקר טוב אדון פידלמן (transliterated "Boker Tov Adon Fidelman", literally "Good Morning Mr Fidelman").

==Plot==
The film concerns a small furniture-restoration business in downtown Tel Aviv, Malamud & Fidelman. As the film begins, one of the partners (Malamud, played by Rami Danon) has died, and bequeathed his share in the business not to his longtime partner Yaakov Fidelman (Sasson Gabai), but to Yaakovs son Noah Fidelman (Nevo Kimhi).

The aging Yaakov is a skilled craftsman, who loves his work and keeps exacting standards. However, he has little sense for financial issues, which had always been taken care of by his dead partner. The business is going down, demand for Yaakovs services is falling off and banks refuse to give him loans. Noah, who had refused to follow in his fathers footsteps and became a successful lawyer, is pressing Yaakov to retire and sell off the workshop – which could bring a lucrative profit as the area is undergoing a real-estate boom.

Yaakov is on the verge of reluctantly giving in when a mysterious young man named Anton (  piano, which Anton discovered among old junk in the workshop, can be repaired and sold for a considerable sum.

Anton – who is also a gifted pianist – throws himself into the repair job, so determined to succeed that he resorts to stealing peoples wallets in the street to gain money needed to buy materials. In effect, he stakes a claim to being Yaakov Fidelmans true son and heir, the one who continues the old mans lifework which his biological son had cast aside. As work on the piano progresses, the frustrated Noah steals into the workshop, but cannot bring himself to smash the piano.

The stakes in this rivalry are raised higher when Anton starts an affair with Noahs wife, Hava (Sarah Adler) – a sensitive and artistic young woman who is neglected by her busy husband, and who is heavily pregnant with Noahs child, Yaakovs grandchild.

Eventually, it is Yaakov Fidelman who must make the crucial choice between Antons piano project and Noahs real-estate deal – and in effect, which of them does he recognize as his true son.

==Reception==
At the 2011 Jerusalem Film Festival, Restoration won the Haggiag Family Award for Best Full-Length Feature Film.  At the Karlovy Vary International Film Festival, it won the top award, the Grand Prix Crystal Globe. 

At the Sundance Film Festival, writer Erez Kav-El  won the World Cinema Screenwriting Award (Dramatic) for Restoration.  

In Israel, Restoration received 11 nominations for the Ophir Awards. 

==References==
 
 

==External links==
* 
* 
* 

 

 
 
 
 