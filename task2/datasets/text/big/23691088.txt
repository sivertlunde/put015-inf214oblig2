Bells of Rosarita
{{Infobox film
| name           = Bells of Rosarita
| image_size     =
| image	=	Bells of Rosarita FilmPoster.jpeg
| caption        = Frank McDonald
| producer       = Edward J. White (associate producer)
| writer         = Jack Townley (screenplay)
| narrator       =
| starring       = Roy Rogers
| music          = Joseph Dubin Mort Glickman Charles Maxwell Ernest Miller Arthur Roberts
| distributor    =
| released       = 1945
| runtime        = 68 minutes (original version) 54 minutes (edited version)
| country        = United States English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 American musical Frank McDonald.

==Plot==
Sue Farnum (Dale Evans) is cheated out of her inheritance by her late fathers business partner Ripley (Grant Withers). Rogers, with the help of others, rescues her. 

==Cast==
*Roy Rogers as Roy Rogers Trigger as Trigger, Roys Horse
*George Gabby Hayes as "Gabby" Whittaker
*Dale Evans as Sue Farnum
*Adele Mara as Patty Phillips
*Grant Withers as William Ripley
*Addison Richards as Slim Phillips
*Roy Barcroft as Henchman Maxwell
*Janet Martin as Rosarita The Robert Mitchell Boy Choir as Boys choir
*Bob Nolan as Bob Nolan, Band Leader
*Sons of the Pioneers as Musicians Bill Elliott as Wild Bill Elliott
*Allan Lane as Allan Lane
*Don Red Barry as Don Barry Robert Livingston as Bob Livingston
*Sunset Carson as Sunset Carson

==Soundtrack== John Elliott as Jack Elliott) Robert Mitchell Robert Mitchell and Betty Best)
* Bob Nolan and the Sons of the Pioneers - "Trail Herdin Cowboy" (Written by Bob Nolan)
* Adele Mara (dancing) - "Aloha"
* Roy Rogers and the Sons of the Pioneers - "Im Going To Build a Big Fence Around Texas" (Written by Cliff Friend, Katherine Phillips and George Olsen)
* Roy Rogers and the Sons of the Pioneers - "When the Circus Comes To Town" (Written by Jerry Eaton and Terry Shand)
* "Singing Down the Road" (Music by Raymond Scott, lyrics by Charles Tobias)
* "Michael Finnegan"
* Dale Evans - "Under a Blanket of Blue" (Written by Jerry Livingston, Marty Symes and Al Neiburg as Al J. Neiburg)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 