Kindred of the Dust
{{Infobox film
| name           = Kindred of the Dust
| image          = Kindred of the dust poster 1922.jpg
| image_size     =
| caption        = Movie poster
| director       = Raoul Walsh
| producer       = R. A. Walsh Co.
| writer         = James T. ODonohoe (screenplay) Peter B. Kyne (novel)
| narrator       =
| starring       = Miriam Cooper Ralph Graves
| music          =
| cinematography = H. Lyman Broening   Charles Van Enger
| editing        =
| distributor    = First National Pictures
| released       =  
| runtime        = 80 minutes 
| country        = United States Silent English English intertitles
| budget         =
}} American silent film directed by Raoul Walsh, and starring his wife Miriam Cooper. The film was the last independent picture for Walshs production company, and the last film he and Cooper would make together.   Today it is one of Walshs earliest surviving features, and is one of only two non-D. W. Griffith features of Coopers that still is known to survive.

==Plot==
Discovering that her husband is a bigamist, Nan (Cooper) returns with her child to her Puget Sound logging town. She is treated as an outcast by all save Donald (Graves), her childhood sweetheart who happens to be the son of a millionaire. Their romance is thwarted by his parents, but after she nurses him to recovery from an apparently fatal illness they are married. The subsequent arrival of a son prompts a family reconciliation.

==Cast==
*Miriam Cooper as Nan of the Sawdust Pile 
*Ralph Graves as Donald McKaye 
*Lionel Belmore as The Laird of Tyee 
*Eugenie Besserer as Mrs. McKaye 
*Maryland Morne as Jane McKaye 
*Elizabeth Waters as Elizabeth McKaye 
*William J. Ferguson as Mr. Daney
*Caroline Rankin as Mrs. Daney 
*Patrick Rooney as Dirty Dann OLeary
*John Herdman as Caleb Brent 
*Bruce Guerin as Little Donald

==Production==
During filming Cooper accidentally gazed into a stage light causing her permanent eye damage that lasted until the end of her life.   The film ended up being Walshs final independent production and was the last time Cooper and Walsh (who had made several films together) worked together.   The film was one of Coopers last films as she retired in 1923.

==Release==
The film was released on February 27, 1922.  Cooper felt it was mediocre but the film performed decently at the box office.   The film still exists and was restored in 2004, is one of the few films from Walshs early years to survive, and is also one of only two surviving films from Coopers starring years. The film has been screened at a few film festivals since its restoration but has not been released for home video.

==References==
 

==External links==
* 
* 
* 

 
 

 
 
 
 
 
 
 