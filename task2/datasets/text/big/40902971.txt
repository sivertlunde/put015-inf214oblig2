The Leather Pushers
 
{{Infobox film
| name           = The Leather Pushers
| image          = The Leather Pushers (1922) - Ad 1.jpg
| image_size     = 190px Reginald Denny Mal St. Clair
| producer       =
| based on       = stories by H. C. Witwer
| screenplay     = Darryl F. Zanuck
| starring       =
| music          =
| cinematography =
| editing        = Universal Film Manufacturing Company FBO
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Reginald Denny Billy Sullivan in the fourth series), and based on boxing stories by H. C. Witwer originally published in Colliers Weekly. The screenplays were written by a young Darryl F. Zanuck. Gussow, Mel (1971), Darryl F. Zanuck: Dont Say Yes Until I Finish Talking, Da Capo Press (p. 29) 

==List of Episodes==
*1: Lets Go  
*2: Round Two  
*3: Payment Through the Nose  
*4: A Fool and His Honey  
*5: The Taming of the Shrewd 
*6: Whipsawed
*7: Young King Cole 
*8: He Raised Kane  
*9: The Chickasha Bone Crusher  
*10: When Kane Met Abel  
*11: Strike Father, Strike Son  
*12: Joan of Neward 
*13: The Wandering Two  
*14: The Widowers Mite 
*15: Don Coyote 
*16: Something for Nothing  
*17: Columbia, the Gem and the Ocean  
*18: Barnabys Grudge

==Preservation status==
Only episodes 2 and 3 exist, along with a trailer for the series. 

==See also==
*List of film serials

==References==
 

==External links==
 
*  
*  

 
 
 
 


 