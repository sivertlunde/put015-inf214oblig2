Moth and Rust
{{Infobox film
| name           = Moth and Rust
| image          =
| caption        =
| director       = Sidney Morgan 
| producer       = 
| writer         = Sidney Morgan
| starring       = Sybil Thorndike   Malvina Longfellow   Langhorn Burton   Cyril Raymond
| music          = 
| cinematography = 
| editing        = 
| studio         = Progress Films
| distributor    = Butchers Film Service
| released       = November 1921  
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Sidney Morgan and starring Sybil Thorndike, Malvina Longfellow and Langhorn Burton.

==Cast==
* Sybil Thorndike as Mrs Brand  
* Malvina Longfellow as Janet Black  
* Langhorn Burton as Ray Meredith  
* Cyril Raymond as Fred Black   George Bellamy as MacAlpine Brand  
* Malcolm Tod as Sir George Trefusis  
* Ellen Nicholls as Lady Trefusis 
* Phyllis le Grand as Lady Anne Varney

==References==
 

==Bibliography==
* Low, Rachael. The History of the British Film 1918-1929. George Allen & Unwin, 1971.

==External links==
*  

 

 
 
 
 
 
 
 

 