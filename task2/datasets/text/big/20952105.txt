Homesdale
 
{{Infobox film name           = Homesdale image          = Homesdale.jpg caption        = Title card producer  Richard Brennan director       = Peter Weir  writer         = Piers Davies Peter Weir starring       = Geoff Malone Kate Fitzpatrick music          = Rory ODonoghue Grahame Bond cinematography = Anthony Wallis editing        = Wayne Le Clos distributor    =   released       = June 1971 runtime        = 52 minutes country        = Australia language       = English  budget         = $3,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998 p257 
}}
Homesdale is a 1971 Australian film directed by Peter Weir. Homesdale is a black comedy about visitors at a guest-house acting out their violent private fantasies and games under the control of the house staff.

==Plot==
Several people gather at the Homesdale Hunting Lodge including butcher/rock singer Mr Kevin, war veteran Mr Vaughan, an octogenarian Mr Levy. All are tormented by Homesdales staff and forced to participate in a series of games about death and murder in which the true character of the guests starts to emerge.

==Cast==
*Geoff Malone as Mr.Malfry
*Grahame Bond as Mr. Kevin
*Kate Fitzpatrick as Miss Greenoak
*Barry Donnelly as Mr. Vaughan
*Doreen Warburton as Mrs. Sharpe
*James Lear as Mr.Levy
*James Dellit as Manager
*Kosta Akon as Chief Robert
*Richard Brennan as Robert 1
*Peter Weir as Robert 2
*Shirley Donald as Matron
*Phillip Noyce as Neville

==Production== The Cat and the Canary.  The budget was covered by a grant from the Experimental Film and Television Fund. The film was shot at Peter Weirs own home in Sydney in March 1971. 

==Release==
The film premiered at the Sydney Film Festival in June 1971 and won the Grand Prix AFI Award in November. It was screened in universities, schools, film societies and occasionally commercial cinemas, as well as on the Seven network. 

==References==
 
* 

==External links==
* 
*  at Oz Movies
 
 

 
 
 
 
 
 
 
 


 
 