Oomakkuyil Padumbol
{{Infobox film
| name           = Oomakkuyil Padumbol
| image          = OomakkuyilPadumbol.jpeg
| image size     =
| border         =
| alt            =
| caption        =
| director       = Siddique Chennamangalloor
| producer       = Siddique Chennamangalloor
| writer         = Siddique Chennamangalloor
| screenplay     = 
| story          = 
| based on       =  
| narrator       = Shankar  Sangeetha Rajendran Dhanraj 
| music          = M. R. Rison
| cinematography = Noushad Sheriff
| editing        = Nishad Yousef
| studio         = Century Visual Media
| distributor    = Sreekrishna Films
| released       =  
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}} Shankar and  Sangeetha Rajendran in the lead roles.         

==Plot== Malayalam and poetry. The film is about her mental trauma once she was moved to an English medium school by her parents, for their social status. 

==Cast==
* Malavika Nair
* Akash Roshan Shankar
* Sangita
* Nilambur Ayisha
* Dhanraj

==Awards==
;Kerala State Film Awards Second Best Actress Best Child Artist

; Kerala Film Critics Awards
* Special Jury Prize: Oomakkuyil Padumbol
* Best Debutant Director: Siddique Chendamangalloor
* Best Child Artist – Female: Malavika Nair
* Best Playback Singer – Male: Vidhu Prathap

==References==
 

 
 
 


 