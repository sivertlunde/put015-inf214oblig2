The Public Defender
 
 
{{Infobox film
| name           = The Public Defender
| image          = The Public Defender poster.jpg
| caption        = Film poster James Anderson (assistant)
| producer       = William LeBaron Louis Sarecky
| writer         = George Goodchild Bernard Schubert
| starring       = Richard Dix Shirley Grey
| music          = 
| cinematography = Edward Cronjager
| editing        = Archie Marshek RKO Radio Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States 
| language       = English
| budget         = 
}}

The Public Defender is a 1931 Pre-Code crime film directed by J. Walter Ruben, starring Richard Dix and featuring Boris Karloff. Rich playboy Pike Winslow dons the mantle of The Reckoner, a mysterious avenger, when he learns that his lady friend Barbara Gerrys father has been framed in a bank embezzlement scandal. Using meticulous planning and split-second timing, Pike, along with his associates, the erudite Professor and tough-guy scrapper Doc, attempt to find proof that will clear Gerry and identify the real culprits. 

==Cast==
* Richard Dix as Pike Winslow
* Shirley Grey as Barbara Gerry
* Purnell Pratt as John Kirk
* Ruth Weston as Rose Harmer
* Edmund Breese as Frank Wells
* Frank Sheridan as Charles Harmer
* Alan Roscoe as Inspector Malcolm ONeil
* Boris Karloff as Professor
* Nella Walker as Aunt Matilda Paul Hurst as Doc
* Carl Gerard as Cyrus Pringle
* Robert Emmett OConnor as Detective Brady
* Phillips Smalley as Thomas Drake (as Phillip Smalley)

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 