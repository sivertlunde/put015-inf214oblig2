Are You Scared?
{{Infobox Film
| name        = Are You Scared|
| image       = 
| caption     =
| director    = Andy Hurst
| writer      = Andy Hurst
| producer    = Michael Feifer
| starring    = Alethea Kutscher Carlee Avers Brad Ashten Erin Consalvi Soren Bowie Brent Fidler Caia Coley
| distributor = Revolver Entertainment 
| released    =  
| runtime     = 79 minutes 
| country     = United States
| language    = English 
| budget      =
}}
Are You Scared? is a 2006 American horror film directed by Andy Hurst, and released by Revolver Entertainment. It stars Carlee Avers, Brad Ashten, and Soren Bowie. An unrelated sequel, called Are You Scared 2, was released in 2009.

==Plot==
Six young people wake up in an abandoned building, with no idea of what is going on or how they got there. A mysterious figure appears to them over a PA system and tells them that they are on a game show called Are You Scared?  They will have to face their deepest fears in order to win the contest.  However, their challenges are real – and deadly.  The show involves several "contests", each of which result in the grisly deaths of the contestants, including death by acid, explosion, shotgun, hungry rats, strangulation, power drill, and decapitation.

==Cast==
* Alethea Kutscher as Kelly
* Erin Consalvi as Cherie
* Brad Ashten as Brandon
* Carlee Avers as Laura
* Kariem Markbury as Jason
* Soren Bowie as Dylan
* Eric Francis as Det. Jay Bowman
* Jennifer Cozza as Christine Robinson
* Brent Fidler as Shadow Man/Kellys Father
* Madison Petrich as Tara

==Reception==
Professional critiques for Are You Scared? have been predominantly negative. Scott Foy called the film "Saw for Dummies" and a "shameless knock-off" of the more successful Saw (franchise)|Saw series. He ended his review with the statement, "No, I was not scared." {{cite web|url=http://www.dreadcentral.com/reviews/are-you-scared-dvd|title=Are You Scared? (DVD)
|work=Dread Central|last=Foy|first=Scott|date=2006-08-18|accessdate=2013-12-30}}  Slasherpool, however, was "surprised by how decent it turned out to be," citing it as "idiotic" but "mildly entertaining," "as far as rip-offs go."   Steve Anderson of AcidLogic.com called it "a mixed bag of entertainment options" that adds a small amount of originality by including a reality show aspect with its influence from Saw. 

== References ==
 

==External links==
*  
*  
*  

 
 
 
 
 


 