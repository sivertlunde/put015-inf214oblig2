Gadbad Gondhal
{{Infobox film
| name = Gadbad Gondhal
| image = 
| caption = 
| director = Yogesh Dattatraya Gosavi
| starring = Santosh Juvekar  Smita Gondkar
| producer = Abhijeet Morey. Balkrishna Wanjale
| writer   = Yogesh Dattatraya Gosavi   Kaustubh Savarkar   Deepak Bhagwat
| music    = Rohit Nagbhide
| released = Unreleased
| runtime  = 124 minutes
| language = Marathi
| country = India
}}
 Marathi Romance Romantic - Comedy film directed by Yogesh Dattatraya Gosavi. It is a love story of a couple Sanmitra (Santosh Juvekar)and Gargi (Smita Gondhkar),Sanmitra and Gargi. The film also highlights the generation gap but in a light comedy manner.  

==Cast==
* Santosh Juvekar as Sanmitra 
* Smita Gondkar as Gargi
* Anand Abhyankar
* Jayant Savarkar
* Neelam Shirke
* Milind Phatak
* Suhasini Deshpande
* Sunil Tawade
* Pratibha Bhagat
* Asit Rediz
* Shekhar Phadke
* Rutuja Deshmukh
 

==Crew==
* Producers : Abhijeet Morey, Balkrishna Wanjale
* Story - Editing - Direction : Yogesh Dattatraya Gosavi
* Screenplay - Dialogues : Kaustubh Savarkar - Deepak Bhagwat
* Art Director : Rangarao Chaougule
* Cinematography : Dr.Vikram Srivastav
* Music : Rohit Nagbhide
* Makeup : Atul Shidhaye
* Lyrics : Vaibhav Joshi
* Costume : Chaitrali Dongre
* Genre : Romantic, Comedy, Drama
* Studio : Alisha Films 

==Music==

===Soundtrack===
{{tracklist
| lyrics_credits  = yes
| music_credits   = yes
| extra_column    = Singer
| title1          = Alis Tu 
| extra1          = Swapnil Bandodkar, Vaishali Samant
| lyrics1         = Vaibhav Joshi
| music1          = Rohit Nagbhide
| length1         = 5:15
| title2          = Sang na 
| extra2          = Swapnil Bandodkar, Vaishali Samant
| lyrics2         = Vaibhav Joshi
| music2          = Rohit Nagbhide
| length2         = 4:46
| title3          = Gadbad Gondhal  
| extra3          = Avdhoot Gupte
| lyrics3         = Vaibhav Joshi
| music3          = Rohit Nagbhide
| length3         = 3:08
| title4          = Bidai
| extra4          = Rohit Nagbhide
| lyrics4         = Vaibhav Joshi
| music4          = Rohit Nagbhide
| length4         = 3:04
}}

==In The News==

===Gadbad Gondhal postponed===
MUMBAI: The release of Marathi film Gadbad Gondhal has been postponed till January or February 2013. The film’s director Yogesh Gosavi informed to RangMarathi that they decided to postpone the film’s release due to some changes in the post-production work. “We are engaged with some advanced technical treatment to the film. So we are left with no other option but to delay the release,” Yogesh said. 

This film has been completed in December 2012 but hadnt released till date.

==References==
 

==External links==

 
 