Abandoned Mine
{{Infobox film
| picture =  
| name = Abandoned Mine
| story = Scott Woldman
| screenplay = Jeff Chamberlain
| director = Jeff Chamberlain
| starring = Alexa Vega Reiley McClendon Saige Thompson Charan Prabhakar Adam Hendershott
| producer = Scott Woldman Jeff Chamberlain Mark Victor Steve Zacharias
| runtime  = 95 minutes
| music  = Russ Howard III
| editor = Michael R. Fox Steve Haugen
| studio = Mountainbridge Films
| distributor = Gravitas Ventures Millennium Entertainment Relay Releasing Co.
| released =  
}}
Abandoned Mine is a 2013 horror film written and directed by Jeff Chamberlain.

==Plot==
Five friends explore a supposedly haunted mine to celebrate Halloween, exactly one-hundred years after a family was murdered in the mine. They soon find to their horror that the ghostly rumors may be true as they fight for survival. 

==Cast==
* Alexa Vega as Sharon, Brads new girlfriend
* Reiley McClendon as Brad, the trickster
* Saige Thompson as Laurie, Brads ex-girlfriend
* Charan Prabhakar as Ethan, an Indian friend of Laurie
* Adam Hendershott as Jim, Brads best friend
* Valerie C. Walker as Kelly, a store clerk
* Joseph Batzel as Jarvis, the ghost of the mine
* Jordan Chamberlain as Store customer, a customer at Sharons store
* Cody Walker as Thomas, an always-happy store clerk

==Development==
The film was originally titled The Mine, before the title was changed to Abandoned Mine.  Filming occurred in Utah and California.

The first clips from the film were revealed on August 7, 2013.  The first poster was revealed on June 18, 2013. The film will be distributed by Gravitas Entertainment. The trailer was released on June 17, 2013, along with information about the release date. 

==Release==
The film will have a simultaneous theatrical and video on demand release.  It will be released theatrically in fifteen cities. 

==References==
 

==External links==
*  
*  
*  

 
 


 