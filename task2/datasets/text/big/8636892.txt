The Story of the Fourteenth Air Force
The Story of the 14th Air Force was a 40-minute propaganda film produced by the US Army Air Force circa 1945.

The film opens by showing China as it was before the Japanese occupation, the industry, professions etc. and then moves on to its conquest by the Japanese, and the struggling Nationalists in the hinterland. The Flying Tigers and General Chenault are briefly introduced and it is explained that they started out as a business operation - $500 a month and an extra $500 for each plane shot down. 

The tortuous supply routes through Assam and the Himalayas are described as well as the substandard equipment that is used by the Flying Tigers. Then the move into Eastern China in 1943 is covered, and the building of the South Eastern air bases and their use against Japanese shipping is described in detail. along with rare combat footage of Flying Tigers sinking a Japanese merchant ship and engaging enemy pilots over the Chinese mainland.

The latter half of the film however concerns the Japanese infantry offensive against the bases in 1944 and the American and Chinese attempts to hold them off before ultimately destroying their own bases and going west with thousands of Chinese refugees.

== See also ==
*List of Allied propaganda films of World War II

== External links ==
* 

 
 
 
 
 


 
 