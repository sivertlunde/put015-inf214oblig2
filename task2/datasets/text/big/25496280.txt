Manchu Pallaki
{{Infobox film
| name           = Manchu Pallaki
| image          = Manchu Pallaki.jpg
| caption        =
| director       = Vamsy
| producer       = Mandapaka Ravi Prasad Rao
| writer         = Rajasekhar   (Story)     Yandamuri Veerendranath   (Dialogues)  Vamsy  (Screenplay)  Rajendra Prasad Narayan Rao
| music          = Rajan-Nagendra
| cinematography = Hari Anumolu
| editing        = Anil Dattatreya
| distributor    = Usha Pictures
| released       = November 19, 1982
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu film Rajendra Prasad, Tamil film Paalaivana Solai (1981).

==Plot== Rajendra Prasad), Vasu (Narayana Rao) and two other friends are the unemployed youth living in a colony who are looking for jobs and are unable to make a living. Geeta (Suhasini Mani Ratnam) moves into their colony with her father and every body falls for her one trying to make a fool of each other. They tease her but she teaches them a lesson and later they become friends. Sekhar also likes her but does not express it. Geeta changes the lives of all of them by making them earn their living by what they know and implementing it successfully. When Sekhar confesses his love to her he comes to know of a shocking truth that she has a terminal disease and will die soon. But before dying, Geeta helps Vasu by marrying his sister to Sekhar.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Chiranjeevi || Sekhar
|-
| Suhasini || Gita
|- Narayana Rao || Vasu
|-
| Saichand || Kumar
|-
| Rajendra Prasad || Hari
|- Girish || Gandhi
|-
| Sakshi Ranga Rao || Gitas father
|-
| P. L. Narayana || Vasus colleague
|-
| Bhimeswara Rao || Doctor
|-
| Devadas Kanakala || Sekhars colleague
|- Mallikarjuna Rao || Cine director
|-
| Master Ali || Vasus brother
|- Annapurna || Samalamma
|}

==Crew==
*Producer: M. R. Prasad Rao
*Screenplay, Direction: Vamsy
*Story: Robert Rajasekar
*Dialogues: Yandamuri Veerendranath
*Music Director: Rajan-Nagendra
*Playback Singers: S.P. Balasubrahmanyam, S. Janaki
*Lyrics: Srirangam Srinivasa Rao, Veturi Sundararamamurthy, Gopi
*Art: Chandra
*Associate Director: Madhuchuri Doraswamy Reddy
*Editing: Anil Dattatreya
*Camera: Hari Anumolu
*Presenter: Vemuri Satyanarayana

==Lyrics==
* pagalu rEyilO jaaraka mundE, velugu cheekaTiga maaraka mundE kalusukunTaam  (Lyrics: Sree Sree; Playback: S.P. Balasubrahmanyam)
* nee kOsamE mEmandaram  (Lyrics: Gopi; Playback: S.P. Balasubrahmanyam, S. Janaki)
* Manishe Manideepam  (Lyrics: Sri Sri; Playback: S.P. Balasubrahmanyam)
* mEghamaa, dEhamaa, meravakE ee kshaNam!  (Lyrics: Veturi Sundararamamurthy; Playback: S. Janaki)

==External links==
* 
* 
*  
*  

 

 
 
 
 
 
 