Dead Men Walk
{{Infobox film
| name           = Dead Men Walk
| image          = Deadmenwalkposter.jpg
| caption        = Film poster for Dead Men Walk
| director       = Sam Newfield
| producer       = Sigmund Neufeld
| writer         = Fred Myton
| starring       = Dwight Frye George Zucco Mary Carlisle Nedrick Young Forrest Taylor
| music          = Leo Erdody
| cinematography = Jack Greenhalgh
| editing        = Holbrook N. Todd
| distributor    = Producers Releasing Corporation
| released       =  
| runtime        = 64 min
| country        = United States
| awards         = English
| budget         =
}} United States, black-and-white horror film produced by Sigmund Neufeld for Producers Releasing Corporation (aka PRC).  It is an original story and screenplay by Fred Myton, starring George Zucco, Mary Carlisle, Nedrick Young and Dwight Frye, directed by Sam Newfield.  It was originally distributed by PRC and reissued in the USA in 1948 by Madison Pictures Inc..

==Plot Summary== magician Elwyn, whom he has secretly murdered because of the latters deep involvement in the occult sciences. Evil Elwyns hunchback assistant Zolarr (Dwight Frye) suspects the good doctor of doing away with his master and confronts him on this matter, but the doctor swears that he only acted in self-defense when his brother had become a danger to society. Unfortunately, the evil twin brother had gone too far meddling with the dark arts before his demise, and with the help of his assistant he returns to life as a vampire. The doctor and his beautiful young niece, Gayle Clayton (Mary Carlisle) soon discover that evil forces still are at work even after Elwyns funeral and are horrified to learn that the resurrected Elwyn is set on a gruesome revenge against his brother and his niece. 

==Cast==
* George Zucco as Dr. Lloyd Clayton / Dr. Elwyn Clayton
* Mary Carlisle as Gayle Clayton
* Nedrick Young as Dr. David Bentley
* Dwight Frye as Zolarr
* Fern Emmett as Kate
* Robert Strange as Wilkins (Harper in credits)
* Hal Price as Sheriff Losen
*   as Minister

==Production and Critical reception==
The film was shot in 6 days. It was the final film of Mary Carlisle. 

Dead Men Walk holds a low 4.3/10 on the Internet Movie Database and a 12% on Rotten Tomatoes. 

==See also==
* List of films in the public domain

==References==
 

==External links==
*  
*  
*  
*  


 
 
 
 
 
 
 
 


 