Song About the Merchant Kalashnikov
{{Infobox film
| name           = Song About the Merchant Kalashnikov (Песнь про купца Калашникова)
| image          =
| caption        =
| director       = Vasily Goncharov
| producer       = Aleksandr Khanzhonkov
| writer         = Vasily Goncharov
| narrator       =
| starring       = Pyotr Chardynin Aleksandra Goncharova Andrey Gromov Ivan Potemkin
| music          = Mikhail Ippolitov-Ivanov
| cinematography = Vladimir Siversen
| editing        =
| studio         = Khanzhonkov and Co
| distributor    =
| released       =  
| runtime        = 250 metres
| country        = Russian Empire Russian intertitles
| budget         =
| gross          =
}}
Song About the Merchant Kalashnikov ( , Transliteration|translit.&nbsp;Pesn pro kuptsa Kalashnikova) is a 1909 Russian silent film directed by Vasily Goncharov. The film is believed to be lost.

==Plot==
The film was loosely based on the  , the assault of Kalashnikovs wife by oprichnik Kiribeevich, the argument between Kalashnikov and Kiribeevich, and the fistfight between the protagonists.

==Production==
Song About the Merchant Kalashnikov was filmed in 1908 with actors from the   of Vvedensky Narodny Dom. Pyotr Chardynin, who later become one of the leading directors of the Russian Empire, debuted in the film as an actor.

==Cast==
*Pyotr Chardynin as Merchant Kalashnikov
*Aleksandra Goncharova as Kalashnikovs wife	
*Andrey Gromov as Oprichnik Kiribeevich	
*Ivan Potemkin as Ivan the Terrible

==References==
*  

==External links==
 

 
 
 
 
 
 
 


 
 