The Cradle of Courage
{{Infobox film
| name           = The Cradle of Courage
| image          = The Cradle of Courage (1920) - 1.jpg
| alt            = 
| caption        = Still with William S. Hart
| director       = William S. Hart Lambert Hillyer
| producer       = William S. Hart 
| screenplay     = Frederick Bradbury Lambert Hillyer 
| starring       = William S. Hart Ann Little Tom Santschi Gertrude Claire Frank Thorwald George Williams
| music          =  
| cinematography = Joseph H. August
| editor         = Le Roy Stone 
| studio         = William S. Hart Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States
| language       = Silent (English intertitles)
| budget         = 
| gross          =
}}
 silent Drama drama film directed by William S. Hart and Lambert Hillyer and written by Frederick Bradbury and Lambert Hillyer. The film stars William S. Hart, Ann Little, Tom Santschi, Gertrude Claire, Frank Thorwald, and George Williams. The film was released on September 19, 1920, by Paramount Pictures.   A copies of the film are in the Museum of Modern Art and at other film archives. 

==Plot==
 

==Cast==
*William S. Hart as	Square Kelly
*Ann Little as Rose Tierney
*Tom Santschi as Tierney
*Gertrude Claire as Mother Kelly
*Frank Thorwald as Jim Kelly
*George Williams as Lieutenant Riley Barbara Bedford as Nora

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 

 