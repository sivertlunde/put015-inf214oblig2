No Way Out (1987 film)
{{Infobox film
| name            = No Way Out
| image           = No Way Out (1987 film) poster.jpg
| alt             =
| caption         = Theatrical release poster
| director        = Roger Donaldson Robert Garland Laura Ziskin
| screenplay      = Robert Garland
| based on        =  
| starring        = Kevin Costner Gene Hackman Sean Young
| music           = Maurice Jarre
| cinematography  = John Alcott
| editing         = William Hoy Neil Travis Orion Pictures
| released        =  
| runtime         = 114 minutes
| country         = United States
| language        = English
| budget          = $15 million
| gross           = $35,509,515
}} thriller film. Iman appear in supporting roles.
 The Big Academy Award-winning composer Maurice Jarre.

==Plot==
At an inaugural ball, US Navy Lieutenant Commander Tom Farrell meets a woman, Susan Atwell, and they begin an affair, although she is involved with another man. Later, Farrell begins to work at the Pentagon for the US Secretary of Defense, David Brice.

Soon, Farrell learns that the other man in Susans life is Brice, who in turn learns of Susans infidelity. While demanding the name of her lover, Brice accidentally kills her in a jealous rage. Ready to turn himself in, Brice is persuaded by his General Counsel, Scott Pritchard, to cover it up and blame it on someone else. They concoct a story that Susans other lover was a suspected but unconfirmed KGB sleeper agent code-named "Yuri."

Brice appoints Farrell to lead the investigation to find Susans other lover, placing him in the position of seeking evidence that could implicate himself. Farrell sets about proving Brice was involved with Susan by searching computer files for evidence that Brice gave her a government-registered gift he received from the Moroccan foreign minister. Farrell presents the gift-registry printout to Brice, who shifts the blame to Pritchard, arguing that Pritchard (a gay man) was jealous of his relationship with Susan. A devastated Pritchard commits suicide and is falsely exposed as "Yuri" to the Pentagon police by Brice, hoping to avoid blame for Susans death.
 
As the film draws to its conclusion, it is revealed that Farrell is the real "Yuri" and was the KGBs mole in the Department of Defense. Aware of Brices affair, the Soviet Union had assigned Yuri to seduce his mistress and gather intelligence from her. Farrell tells his Soviet contacts that he is finished being a KGB agent after the tragic events he has been through, but they tell each other after he leaves the KGB safehouse that "hell be back, he has nowhere else to go".

==Cast==
 
* Kevin Costner as Lieutenant Commander Tom Farrell, US Navy
* Gene Hackman as Secretary of Defense David Brice
* Will Patton as Scott Pritchard
* Sean Young as Susan Atwell
* George Dzundza as Sam Hesselman
* Howard Duff as Senator Duvall CID
* Fred Dalton Thompson as CIA Director Marshall Iman as Nina Beka
 

==Release==

===Box office===
The film debuted at number 2 at the box office after Stakeout (1987 film)|Stakeout.  The films budget was an estimated $15 million; its total U.S. gross was over $35 million. 

===Critical reception===
The film was very well received by critics and as of July 30, 2013, holds a 92% "Fresh" rating on Rotten Tomatoes based on 38 reviews. 
 The Big Clock."  Desson Thomson of The Washington Post wrote, "The film makes such good use of Washington and builds suspense so well that it transcends a plot bordering on ridiculous." 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 