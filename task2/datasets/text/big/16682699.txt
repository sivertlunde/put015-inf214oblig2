Impulse (1990 film)
{{Infobox Film
| name           = Impulse
| image          = Impulse90poster.jpg
| image_size     = 
| caption        = Theatrical release poster
| director       = Sondra Locke
| producer       = Andre Morgan	
| writer         = John DeMarco Leigh Chapman
| narrator       = 
| starring       = Theresa Russell Jeff Fahey George Dzundza
| music          = Michel Colombier
| cinematography = Dean Semler
| editing        = John W. Wheeler	
| distributor    = Warner Bros.
| released       = April 6, 1990
| runtime        = 109 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 prostitute on Los Angeles. The film was directed by Sondra Locke, and stars Theresa Russell, Jeff Fahey, and George Dzundza. 

==Plot==
Lottie Mason is a Los Angeles cop, working vice. She has a number of problems at work, being under Internal Affairs investigation after a shooting incident and being sexually harassed by Lt. Morgan, her superior officer.

Forced by the department to see a psychiatrist, Mason reveals fantasies about the pleasures of losing control. She has begun a romantic relationship with a dependable district attorney named Stan Harris, but seems to yearn for a more thrilling experience.

Mason goes undercover in Hollywood as a prostitute, dressing provocatively and living dangerously. After a dangerous undercover operation, Lottie goes to a bar where she meets a man who offers her a large sum of money to go back to his house. The man, it turns out, is a mob boss under investigation by the Stan Harris.

While Lottie is at the mob boss house, she has second thoughts and is poised to leave, when she hears a gunshot. She hides from the unknown killer and discovers the mob boss has been murdered after he leaves. Lottie quickly wipes away any evidence of her presence in the house and finds a key to an airport locker in his wallet after she searches him. She then anonymously reports the murder and flees the house. After discovering a briefcase containing $1 million at the airport, Lottie becomes a key suspect in his murder.

==Cast==

*Theresa Russell as Lottie Mason
*Jeff Fahey as Stan Harris
*George Dzundza as Lt. Joe Morgan
*Alan Rosenberg as Charley Katz

==Reception==

The film received negative reviews by respected critics.   

===Home video===

The film was not a success at the box office. It was more profitable on home video. 

==References==
 

==External links==
*  

 
 
 
 
 
 


 