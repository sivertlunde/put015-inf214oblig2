Body of Evidence (1993 film)
{{Infobox film
| name           = Body of Evidence
| image          = Bodyofevidence1.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Uli Edel
| producer       = Dino De Laurentiis
| writer         = Brad Mirman Madonna Willem Dafoe Joe Mantegna Anne Archer
| music          = Graeme Revell
| cinematography = Douglas Milsome
| editing        = Thom Noble Dino De Neue Constantin Films Guild Film Distribution  
| released       =  
| runtime        = 99 minutes
| country        = Germany United States
| language       = English
| budget         = $30 million {{cite web
  | url = http://www.imdb.com/title/tt0106453/business
  | title = Body of Evidence (1993)
  | publisher = Imdb Business
  | accessdate = 2008-01-13
}} 
| gross          = $13,275,426 
}}
Body of Evidence is a 1993 American erotic thriller film produced by Dino De Laurentiis and distributed by Metro-Goldwyn-Mayer and originally had the rare NC-17 rating. {{cite news
 | url= http://articles.latimes.com/1992-08-31/entertainment/ca-6022_1_work-rated-nc-17
 | title= Madonna Set to Push Limits Once More With NC-17 Movie
 | author = David J. Fox
 | work= The Los Angeles Times
 | date=1992-08-31
 | accessdate=2012-04-17
}}  Madonna and Willem Dafoe,  with Joe Mantegna, Anne Archer, Julianne Moore and Jürgen Prochnow in supporting roles.
 R rating, reducing the films running time from 101 to 99 minutes.   The video première, however, restored the deleted material. Bloodhounds of Broadway. 
 Dangerous Game was released there as Body II even though the films have nothing in common nor are related to each other in narrative.

==Plot==
In Portland, Oregon|Portland, Oregon, a wealthy man, Andrew Marsh, has died, while apparently viewing a homemade pornographic tape. The police suspect that the woman having sex with Marsh in the film, Rebecca Carlson (Madonna (entertainer)|Madonna), may have murdered her older lover for personal gain. Her lawyer is Frank Dulaney (Willem Dafoe), a happlily married man.

Rebecca claims to have been unaware that Marsh had a heart condition or that he had altered his will, leaving her a large sum of money. District attorney Robert Garrett (Joe Mantegna) believes that she, aware of both facts, deliberately induced a heart attack in the victim with vigorous sexual activity.

Rebecca is arrested and charged with the crime. Released on bail, she justifies her sex life to Frank, who goes home and has passionate sex with his wife (Julianne Moore).

As the trial begins, DA Garrett tells the jury that it is of no consequence if it was Rebeccas sexual prowess which killed Marsh; what matters is whether she intended to murder him. Frank argues that the states case is ludicrous, that "it is not a crime to be a beautiful woman who falls in love with a wealthy older man."

Frank is clearly lusting after Rebecca, who allows him to see her naked while she is receiving acupuncture and flirts with him before spurning his advances. Eventually, the two embark on a sadomasochistic affair.

During their first sexual encounter, Frank notices too late that Rebecca is tying his arms behind his back using his own belt, telling Frank that they will have sex "my way." Rebecca removes Franks underwear, but leaves hers on, clearly demonstrating her dominance. Rebecca then teases and torments Frank by pouring hot candle wax onto his chest, stomach, and finally his penis, before relieving the burn with champagne, while Frank can only watch helplessly. Rebecca is both amused and aroused by the pain and humiliation she is causing Frank. Rebecca then mounts Frank and they have rough sex with Rebecca in complete control, an obvious counterpoint to their relationship in the courtroom, where Frank is the one in control. The next morning, while showering, Frank is shocked by how obvious the burn marks from the wax are on his skin. He rushes to cover them before his wife notices.

A key witness for the prosecution, Marshs doctor, Alan Paley (Jürgen Prochnow), is revealed in court by Frank to be an extremely unreliable witness, as he was (apparently) trying to blackmail Rebecca into having a sexual relationship with him. Pleased with Franks performance in court, Rebecca allows him to perform oral sex on her in an underground carpark.

The testimony of Marshs private secretary, Joanne Braslow (Anne Archer), reveals that she also had a sexual relationship with Marsh that could have contributed to his death, casting a reasonable doubt as to Carlsons guilt.

A new witness, unexpected by Frank, proves extremely damaging to the defense. Jeffery Roston (Frank Langella), a wealthy older man with a bad heart just like Marsh, testifies that he had a relationship with Rebecca during which he made her the primary beneficiary in his will. When asked to describe their sex life, Roston states that Rebecca would always force him to have sex "her way", and on one occasion had tied him to the bed with his belt, then not allowed him to orgasm, despite him begging her, testimony that clearly resonates with Frank. Roston claims that after he had heart surgery and was well again, Rebecca abruptly left, with no explanation.

Frank and Rebecca clash angrily after the testimony in court. Frank goes to see his wife at the restaurant she works at,  only to discover that she is aware of his affair. He tries to lie, but learns Rebecca phoned his wife, who also has noticed the burn marks on his body.

Frank rushes to confront Rebecca at her houseboat. Initially she claims that she called because she wanted to know if Frank still intended to represent her. Rebecca then taunts him with certain things she could have told his wife, including advice on her sex life. Frank shoves Rebecca to the ground. She opens her robe, revealing her half naked body, and begins to masturbate. Frank cannot resist, but when Rebecca tries to handcuff Frank, he is furious at having been manipulated by her again, and still unable to control his lust for Rebecca but now with their power positions reversed for the first time, he manacles her wrists to a bedpost, yanks down her briefs, and anally rapes her, yet she very quickly enjoys it.

Having found evidence which indicates that Joanne Braslow might have murdered Marsh because of jealousy over Rebeccas relationship with him, Rebecca insists that Frank allow her to testify on her own behalf. Frank relents.

Rebeccas testimony convinces the jury, who acquit her. Before leaving court, she mockingly thanks her attorney for getting a guilty client off, fully aware that he cannot repeat what she said.

Frank goes that night to Rebeccas home, where he finds her with Alan Paley, discussing the manner in which they conspired to kill Marsh, with Paley as a "reverse character witness" (he makes her look guilty, only to have his testimony completely undermined). Rebecca bluntly tells Frank and Paley that she used her sexual prowess to get whatever she wanted from both of them, as well as Marsh.

Now free and clear because she cannot be tried for the same crime twice, Rebecca proceeds to taunt Paley, telling him to leave because "Ive forgotten you already." An enraged Paley lashes out at her and, after Frank pulls him off, Paley shoots her twice. She plunges from a window to her death.

==Main cast== Madonna as Rebecca Carlson 
*Willem Dafoe as Frank Dulaney
*Joe Mantegna as Robert Garrett
*Anne Archer as Joanne Braslow
*Julianne Moore as Sharon Dulaney
*Stan Shaw as Charles Briggs
*Charles Hallahan as Dr. McCurdy
*Lillian Lehman as Judge Burnham
*Mark Rolston as Detective Reese Jeff Perry as Gabe
*Richard Riehle as Detective Griffin
*Jürgen Prochnow as Dr. Alan Payley
*Frank Langella as Jeffrey Roston

==Reception and controversy== Golden Raspberries, including Worst Picture, Worst Actor (Willem Dafoe), Worst Director, Worst Supporting Actress (Anne Archer) and Worst Screenplay, with Madonna winning Worst Actress.  It also appeared on the 2005 list of Roger Eberts most hated films. The screenplay and performances were especially disparaged.  Body of Evidence has a 6% rating over at Rotten Tomatoes based on 34 reviews.
 softcore coffee anal and public intercourse. Bad Girl", released at the same time.

===Box office===
Body of Evidence performed poorly at the box office.  In its second week it experienced a 60% drop. 

==References==
 

==External links==
* 
* 
* 
* 

 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 