Chaalis Chauraasi
 
 
{{Infobox film
| name           = Chaalis Chauraasi
| image          = Chaalis Chauraasi.jpg
| caption        = Theatrical release poster
| director       = Hriday Shetty
| producer       = Sachin Awasthee Uday Shetty Anuya Mhaiskar
| writer         = 
| released       = 13 January 2012
| starring       = Naseeruddin Shah Kay Kay Menon Atul Kulkarni Ravi Kissen
| music          = Lalit Pandit & Vishal Rajan
| cinematography = Vincent fernandez
| editing        = Vincent fernandez
| distributor    = 
| language       = Hindi
| Budget         = 13.75 crore
| Grossed        = 
}}
Chaalis Chauraasi ( : Forty Eighty Four), is a Bollywood crime comedy film directed by Hriday Shetty and starring Naseeruddin Shah, Atul Kulkarni, Kay Kay Menon and Ravi Kishan. Most of the film was shot in Mumbai. It was released on 13 January 2012. 

==Cast==
* Naseeruddin Shah as Pankaj Purushottam Suri (Sir)
* Kay Kay Menon as Albert Pinto (Pinto)
* Atul Kulkarni as Bhaskar Sardesai (Bobby)
* Ravi Kissen as Shakti Chinappa (Shakti) Rajesh Sharma as Mahesh Naik
* Reetu Jain as BadmastItem Girl
* Shweta Bhardwaj as Madhoo Zakir Hussain as Tony Bisleri
* Manoj Pahwa
* Arbaaz Ali Khan
* Yuri Suri as Tau

==Box Office==
In its first weekend, the film netted around 75.75 lakh. 

==Soundtrack==
{{Infobox album
| Name        = Chaalis Chauraasi (4084)
| Type        = Soundtrack
| Artist      = Lalit Pandit & Vishal Rajan
| Cover       = Chaalis Chauraasi Album Cover.png
| Released    = December 2011
| Genre       = Film soundtrack
| Length      = 20:29
| Label       = T-Series
}}

The soundtrack of the movie was composed by Lalit Pandit & Vishal Rajan. The music rights were sold to T-Series and released on December 2011. The rights of the song "Hawa Hawa" was acquired from Pakistani Singer Hassan Jahangir.. But the Pakistani song itself was ripped off from  In fact, the song was produced in two versions, one sung by Neeraj Shridhar and the other by Hassan Jahangir himself.
 plagiarized from Koursh Yaghmaies Irani number Havar Havar.

{{track listing
| headline = Track Listing 
| extra_column = Singer(s)
| music_credits = no
| total_length = 20:29

| title1 = Setting Zaala
| extra1 = Sonu Nigam, Amit Kumar
| length1 = 5:07

| title2 = Badmast
| extra2 = Daler Mehndi, Mamta Sharma
| length2 = 5:09

| title3 = Hawa Hawa (Version 1)
| extra3 = Neeraj Shridhar, Amitabh Narayan
| length3 = 3:23

| title4 = Chaalis Chauraasi (4084) (Theme)
| extra4 = Vishal Rajan
| length4 = 3:25

| title5 = Hawa Hawa (Version 2)
| extra5 = Hassan Jahangir
| length5 = 3:23
}}

==References==
 

==External links==
* 
*  

 
 
 
 
 


 