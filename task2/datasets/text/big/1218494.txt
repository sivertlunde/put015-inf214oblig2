Atanarjuat: The Fast Runner
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Atanarjuat: The Fast Runner
| image          = Atanarjuatposter.jpg
| border         = yes
| caption        =
| director       = Zacharias Kunuk Norman Cohn Zacharias Kunuk Germaine Wong
| writer         = Paul Apak Angilirq
| starring       = Natar Ungalaaq Sylvia Ivalu Peter-Henry Arnatsiaq
| music          = Chris Crilly
| cinematography = Norman Cohn
| editing        = Norman Cohn Zacharias Kunuk Marie-Christine Sarda
| distributor    = Sony Pictures Cannes premiere) February 1, 2002 (UK) April 12, 2002 (Canada) June 7, 2002 (USA, limited) August 22, 2002 (Australia)
| runtime        = 172 minutes
| country        = Canada
| language       = Inuktitut
| budget         = CAD 1,960,000 (est.)
| gross         =  $5,998,310
| preceded_by    =
| followed_by    =
}}
 Inuit legend passed down through centuries of oral tradition.
 Isuma Igloolik Productions, the film was Canadas top-grossing release of 2002, outperforming the mainstream comedy Men With Brooms. In 2004, it was included in the Toronto International Film Festivals list of the Top 10 Canadian Films of All Time.

==Plot==
The film is set in Igloolik ("place of houses") in the Eastern Arctic wilderness at the dawn of the first millennium. 

===The shamans curse=== shaman by the name of Tungajuaq, who comes from up north, is singing playfully to the gathered community and camp leader Kumaglak. But among the spectators there are some mistrustful faces.

Flash forward to another day. Qulitalik is bidding goodbye to his sister Panikpak, wife of Kumaglak, promising to come if she calls for help in her heart. She gives him her husbands rabbits foot for spiritual power. Qulitalik tells his sister, "Tulimaq is the one theyll go after now." It seems that Qulitalik is leaving to escape a threat, and the hope is that one day he will return to help. As Panikpak watches him leave, we hear a voice-over from a woman (which we will recognise later as the voice of Panikpak when she is older): "We never knew what he was or why it happened. Evil came to us like Death.  It just happened and we had to live with it."

Flash back to the original scene in the stone house. The visitor and the camp leader Kumaglak are in a "friendly" spiritual duel involving binding in leather thongs. But Panikpak is startled when the stone lamp in front of her breaks in half, and, to the horror of those present, Kumaglak falls over dead. The visitor removes the leaders walrus-tooth necklace from Kumaglaks body, and, passing by Tulimaq, he puts the necklace around the neck of Sauri, the son of the murdered leader Kumaglak, saying, "Be careful what you wish for" (suggesting that Sauris ambition had a part to play in events). Tulimaq leaves, shouting at Sauri "You helped him murder your own father!"

Time passes; the shamans curse has poisoned the camp. Tulimaq, now the local laughing stock, is having trouble feeding his family because of "bad luck" hunting. But Panikpak secretly brings meat for Tulimaqs children, Amaqjuaq and Atanarjuat, hoping that one day they will grow strong and be able to make things right.

===The rivals=== dogs and Dog sled|sled. But Atanarjuat, the fast runner, chases them down, and Amaqjuaq, the strong one, throws the gang off the sled. Later, during a game of "wolf tag" at camp, Atanarjuat chases the beautiful Atuat, for whom he has an obvious liking, provoking jealous anger from Oki, who was betrothed to Atuat when they were children. To complicate matters further, Okis sister Puja also openly shows her tender feelings for Atanarjuat. The scene is set for a duel. Even Okis grandmother Panikpak does not want Atuat to marry Oki, because, she says, Oki is a cruel man, just like his father Sauri has been since the evil shamans visit. (Panikpak calls Atuat "little mother" because Panikpak long ago "recognised" the child and named her after Panikpaks own mother Atuat.)

In a large igloo they are singing and playing games of strength as a prelude to a feast. Atanarjuat and Oki warm up for a head-punching duel by singing mocking songs at each other. As the duel begins, Oki is startled with a vision of the evil shaman, and Atanarjuat is protected by a prayer by Panikpak to her dead husband. Atanarjuat ends up knocking Oki into helpless spasms, thus winning the right to marry Atuat.

===Broken love===
Some time later, Atanarjuat is a happy husband to the pregnant Atuat. Tulimaq tells Atanarjuat that he should leave to hunt caribou, and an elder suggests that Atanarjuat stop at Sauris camp, joking about the women that he will find there. On his trip Atanarjuat does stop at Sauris camp, where Oki and Sauri suggest that he should take Puja to help with the hunt. Atanarjuat naively allows himself to be persuaded. At a waterfront camp, Atanarjuat and Puja pass the evening singing and flirting and eventually end up in the tent having sex.

Time passes; it is summer. Atanarjuat is now in an unhappy marriage with two wives, Atuat and Puja, and has a young son by Atuat. Atuat and Amaqjuaqs wife Uluriaq complain that Puja is not helping with the daily work. Later, in the tent, under the communal fur shared by the family, Puja and her brother in law Amaqjuaq softly slide together and begin to have sex.  Uluriaq wakes up and screams, and Atanarjuat cuffs Puja on the head. Puja flees to Sauris camp and arrives there black-eyed, crying that Atanarjuat tried to kill her for no reason. Oki vows revenge, Panikpak however remains suspicious of her accusations.

===A murderous attack=== boot against the tent to indicate who is sleeping on that side. She leaves to join the other women as Oki and his gang appear over the hilltop. From inside the tent we hear Amaqjuaq admit to his brother that he has done him wrong — the last words he will ever speak. Oki and his two henchmen sneak up and plunge their spears through the tent wall. Okis spear comes out stained with the blood of Amaqjuaq. But Oki is startled and distracted at a vision of his grandfather Kumaglak shouting angrily "Atanarjuats brother is coming after you!". At that instant, Atanarjuat, naked, barefoot, and unharmed, bursts out of the tent and runs off across the ice.

===Atanarjuats run===
Oki and his two sidekicks give chase on foot, but Atanarjuat, the fast runner, keeps well ahead of them, running steadily for miles barefoot on the ice. Oki knows there is an open crack in the ice which will stop Atanarjuat. But as Atanarjuat runs he sees a vision of an old man calling to him. Atanarjuat follows the vision and gains the power to make a soaring leap over the open crack. Oki is forced to turn back to get his dog sled.

An old man is picking eggs and looking around uneasily. It is the man whose spirit appeared to Atanarjuat on the ice. He returns to his wife and adopted daughter with more eggs than they could possibly eat themselves.

Atanarjuat, freezing cold and with feet raw and bloody, finally collapses on the ice. He wakes up wrapped in furs with the old man and his family staring down at him. Atanarjuat tells them he is being chased, and the daughter spots a sled team approaching. The family hide Atanarjuat under some dry seaweed.  Oki and his brothers arrive, and in spite of Okis threats and questions, the family profess to have seen no one. Oki urinates on the seaweed under which Atanarjuat is hiding.  The family feeds their visitors eggs until they are stuffed. After the meal Oki tells the old man that he knows he is Qulitalik, Okis great uncle, who left Igloolik many years ago (as we witnessed at the beginning of the film) and that they had all thought he had died. Oki eventually leaves in the hope of finding Atanarjuats body on the ice.

===Community in fear===
Back at Igloolik, Oki is angry because Sauri refuses to let him have Atuat. One day, while Atuat is spending time alone, Okis henchmen grab her and pin her down while Oki rapes her. Later, Panikpak comes to Atuat to offer her comfort.

Winter has arrived again, and the ice is becoming solid. Atanarjuat, having healed with help from his rescuers, is impatient to return to Igloolik. He is not scared of Oki, but the evil spirit behind Oki. "Only you know when you are ready", Qulitalik tells him — spiritual healing is as important as the physical.

The men of Igloolik are out on the ice hunting seals. Oki approaches Sauri and stabs him in the stomach with the words "Get out of my way, Atuat is mine now." Oki tells the others that Sauri tripped and stabbed himself, and they bring the body of Sauri back to Igloolik on a sled. Amidst the mourning and crying, the leaders walrus-tooth necklace is removed from Sauris body and placed around Okis neck: Oki is now the camp leader. He later tells Atuat that he wants nothing to do with her, and as time passes he provides her with no food or care. She becomes almost desperate enough to beg him for help. In her heart, Panikpak calls out to her brother Qulitalik (calling him anik, "brother") to summon him supernaturally, as they agreed so long ago.

===A daring plan===
Sensing his sisters call, Qulitalik tells his family that they will all go to Igloolik with Atanarjuat. Outside, Qulitalik performs a magic ritual using rabbits feet, and back at Igloolik Oki sees a rabbit in the snow that he is able to catch with his bare hands. When he eats the rabbit meat he falls under a happy spell that makes him forget all his grievances.

Having made the long sled journey across the ice, Atanarjuat, Qulitalik, and the family approach Igloolik. Atuat runs out and is joyfully reunited with her husband. Puja runs out too, but Atanarjuat humiliates her by cutting open her jacket and turns her away. Oki, still under the spell, does not defend Puja, but instead suggests that they prepare food for the visitors.

Atanarjuat builds an igloo with smooth ice floor and invites Oki and his pals to an early start to the feast. They begin eating and talking as old friends. Then Atanarjuat steps out for a moment and returns with a caribou antler club in his hand and caribou antler spikes attached to his feet. He attacks while his enemies slide helplessly on the ice. Pinning Oki down, Atanarjuat smashes the club into the ice beside Okis head and declares that the killing will stop now. Emerging from the igloo victorious, Atanarjuat finds the community gathered outside. Qulitalik takes the walrus tooth necklace off Okis neck and gives it to Panikpak to take care of until the evening, when they will meet to finally confront the evil that has been with them for so long.

===Return of the shaman===
With the community gathered that evening, Qulitalik chews on a walrus-skin bag to call forth the spirits. The evil shaman Tungajuaq appears, blowing and grunting with the eerie echo of a polar bear. The others recoil in fear, but Qulitalik places a carved pair of tusks in his mouth and confronts Tungajuaq with the powerful spirit of the walrus. Panikpak joins him, shaking the walrus tooth necklace. Feeling the force of their spiritual onslaught, the shaman backs up. Qulitalik throws some magic soil into the shamans face, and the evil shaman screams and disappears into thin air.

Panikpak speaks to the gathered group in the stone house. It is time for forgiveness, she says, and to rid the community of the evil that has plagued them for so many years. Oki and Puja and their friends are forgiven for their evil deeds, she says, However they will not be unpunished and they must leave Igloolik immediately and never return. As Puja cries in grief the troublemakers all leave, with one final furious and agonised look from Oki.

Joy sweeps the gathered community. Then the voice of the old leader of the camp, Kumaglak, is heard coming from his namesake Kumaglak, the little son of Atanarjuat and Atuat. Old Kumaglak asks his wife Panikpak to sing his old song, which she does, as everyone joins in.

==Adaptation==
The names of Atanarjuat and his brother first appeared in writing in the journals of the explorer Captain  . Atanarjuat, the Fast Runner. Coach House Books and Isuma Publishing, 2002.ISBN 1-55245-113-5.    The main elements of the original story are that two brothers are betrayed by their wives and help set up a sneak attack. Rivals plunge their spears through the walls of the brothers tent, but the fast runner makes an escape across the ice, naked and barefoot. After being rescued and healing, the fast runner sets up his own ambush and succeeds in killing his rivals.  (See the book for a two-page treatment.)

The writers had to flesh out the bare bones of the legend with more detailed characterization and a social and religious setting. Stories in Inuit society are told for entertainment and teaching. One of the lessons of the legend of Atanarjuat is the evil that can occur when personal ambition (Sauris and Okis, in the film) is put before the needs of the community. Another lesson is that someone who defies the camp leader may end up fleeing alone across the ice.
 treatment (story). The final script was developed by the team of Angilirq, Norman Cohn (producer and cinematographer), Zacharias Kunuk (director), Herve Paniaq (tribal elder), and Pauloosie Qulitalik (tribal elder, who also plays the shaman Qulitalik). Angilirq died of cancer during film production in 1998. The last version of his Inuktitut screenplay is in the book Atanarjuat, side by side with a version by Cohn that appears to be a precursor to the final English script. Cohn explained, "In the final edited version of the film, sometimes the actors are speaking the lines from Apaks script and we are subtitling them with the lines from mine." 

Despite the emphasis on accuracy, the film takes liberties with the original Inuit myth: "At the films core is a crucial lie," wrote Justin Shubow in The American Prospect,  which is that the original legend ended in a revenge killing, whereas in the film Atanarjuat stops short of shedding blood. "A message more fitting for our times," explained director Zacharias Kunuk. 

==Production== Bernard Saladin dAnglure the biggest challenge was resurrecting the beliefs and practice of Shamanism among Eskimo peoples|shamanism, "the  major frame of reference for Inuit life."  Research into historical sources — often the journals of European explorers  — provided the basis for the reconstruction of clothes and customs. Elders were consulted every step of the way; in an interview, Paul Apak Angilirq said
:"We go to the elders and ask information about the old ways, about religion, about things that a lot of people have no remembrance of now... They are helping us write down what people would have said and acted in the past, and what the dialogue would have been like... We speak baby talk compared to the elders. But for Atanarjuat, we want people speaking real Inuktitut... When we are writing the script, they might jump in and say, Oh, we wouldnt say such a word to our in-law! We wouldnt say anything to our brothers wives! It was against the law!" 

All animal carcasses shown in the film were used for food or for their hides. 

==Reception==

===Reviewers===
 
Critics generally gave high praise to the film and found the details of the traditional Inuit way of life fascinating.
*"... succeeds as a mythic drama of good versus evil ... yet its also an impressively vivid and detailed depiction of a particular way of life." Tom Dawson, BBC. 
*"A ripping yarn and a spectacularly new and odd vision." Richard Corliss, Time (magazine)|Time.   
*"... not some aboriginal folk-art curio but an enthralling and sophisticated work that fully exploits cinemas potential as a visual medium." Jason Anderson, Eye Weekly. 
*"Everything combines in The Fast Runner to create a film that does not feel acted and rather as if it is simply happening in front of our eyes." Kenneth Turan, Los Angeles Times. 
*"Cohns camera work situates the viewer in Atanarjuats family grouping... The goal of Atanarjuat is to make the viewer feel inside the action looking out rather than outside looking in." Jennifer L. Gauthier, CineAction.   
There were comments by some critics about the "somewhat confusing first 30 minutes" of the film (David Ansen, Newsweek)  and about the slow pace of the film: "A much shorter cut surely would have resulted in a smoother, more focused narrative" wrote Mark Dujsik in Mark Reviews Movies. 

===Inuit community===
The goals of the film were first to show how for thousands of years Inuit communities had survived and thrived in the Arctic, and second to introduce the new storytelling medium of film to help Inuit communities survive long into the future.  Atanarjuat "is an important step for an indigenous people who have, until recently, seen their culture recorded by outsiders" wrote Doug Alexander in the Canadian historical magazine   are playing Atanarjuat in the streets," said producer Norman Cohn in a 2002 interview.    At one point the production company was considering making Atanarjuat action figures.  The film production pumped more than $1.5 million into the local economy of Igloolik and employed about 60 people. 

==Cast==

===Atanarjuats family===
*Natar Ungalaaq as Atanarjuat, the fast runner
*Pakak Innuksuk as Amaqjuaq, the strong one, Atanarjuats older brother
*Neeve Irngaut as Uluriaq, wife of Amaqjuaq
*Felix Alaralak as Tulimaq, Atanarjuats father
**Stephen Qrunnut as Young Tulimaq
*Kumaglaq, the young son of Atanarjuat and Atuat, namesake of the old camp leader

===Okis family===
*Peter-Henry Arnatsiaq as Oki, Atanarjuats rival
*Lucy Tulugarjuk as Puja, Okis spoiled sister
*Apayata Kotierk as Kumaglak, the old camp leader
*Madeline Ivalu as Panikpak, wife of Kumaglak, mother of Sauri, grandmother of Oki and Puja, and sister of Qulitalik
**Mary Angutautuk as Young Panikpak
*Pauloosie Qulitalik as Qulitalik, brother of Panikpak
**Charlie Qulitalik as Young Qulitalik
*Mary Qulitalik as Niriuniq, wife of Qulitalik
*Eugene Ipkarnak as Sauri, camp leader
**Eric Nutarariaq as Young Sauri

===Others===
*Sylvia Ivalu as Atuat, sought as a wife by Atanarjuat and Oki
*Abraham Ulayuruluk as Tungajuaq, the evil shaman
*Luke Taqqaugaq as Pittiulak, Okis sidekick
*Alex Uttak as Pakak, Okis sidekick

==Awards==

===Awards won===
* American Indian Film Festival, 2002 - Best Film, Best Actor, Best Actress
* Banff Mountain Film Festival, 2002 - Best Feature Film Cannes Film Golden Camera   
* Cinemanila International Film Festival, 2002 - Lino Brocka Award
* Edinburgh International Film Festival, 2001 - Guardian Award for Best New Director (co-winner)
* Flanders International Film Festival, 2001 - Golden Spur, FIPRESCI Prize
* Genie Awards, 2002 - Best Motion Picture, Best Director, Best Screenplay, Best Music - Original Score, Best Editing, Claude Jutra Award
* Hawaii International Film Festival, 2001 - Special Mention in the Best Feature Film category
* ImagineNATIVE Media Arts Festival, 2001 - Best Film
* Lake Placid Film Forum, 2002 - Audience Award
* Festival International de Films de Montréal, 2001 - Special Jury Prize, Prix du Public
* Newport International Film Festival, 2002 - Audience Award for Best Feature CTV Best of Fest Award
* San Diego International Film Festival, 2002 - Festival Award for Best Feature Film
* Santa Fe Film Festival, 2001 - Luminaria for Best Feature Film
* Toronto Film Critics Association, 2002 - Best Canadian Film, Best First Feature
* Toronto International Film Festival, 2001 - Best Canadian Feature Film

===Nominations received===
Atanarjuat was nominated for the following awards, but did not win them.
* Chicago Film Critics Association, 2003 - Best Foreign Language Film, Most Promising Director
* Genie Awards, 2002 - Best Costume Design, Best Sound
* Golden Trailer Awards, 2003 - Best Independent Film
* Independent Spirit Awards, 2003 - Best Foreign Film
* Online Film Critics Society, 2002 - Best Foreign Language Film

==Further reading==
*  Describes the intercultural marriage between filmmaking and traditional Inuit storytelling in the film.

==See also==
*Kabloona
*The Journals of Knud Rasmussen (film)
*Aboriginal peoples in Northern Canada aboriginal ancestral legend involving the sexual jealousy of brothers, entirely in the language and with the actors of the region

==References==
 

==External links==
*  
*  
*  
*  
* 
* 
* 
*  — production company
* 
*  in The Canadian Encyclopedia
*  on the film at the National Film Board of Canada
*Discussions on Atanarjuat DVD quality      

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 