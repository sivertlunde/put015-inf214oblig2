Oysters at Nam Kee's
{{Infobox Film
| name           = Oysters at Nam Kees
| image          = 
| image_size     = 
| caption        = 
| director       = Pollo de Pimentel 
| producer       = 
| writer         = Kees van Beijnum (novel)  Hans de Wolf (screenplay)  
| starring       = Katja Schuurman  Egbert Jan Weeber
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 2002
| runtime        = 92
| country        = Netherlands
| language       = Dutch 
| budget         = € 2,2 million
| preceded_by    = 
| followed_by    = 
}}
  at the Zeedijk in Amsterdam]]
 Dutch drama film, directed by Pollo de Pimentel, starring Katja Schuurman and Egbert Jan Weeber.

The film received a Golden Film (75,000 visitors) in 2002.

==Plot==
Berry (Egbert Jan Weeber) is a boy who plays fast and loose and likes to hang around with his friends. One day he meets Thera (Katja Schuurman), a girl whos a little bit older, and she turns his head around. They start a romance culminating in a meetup at Nam Kee, a Chinese restaurant in Amsterdam where they have oysters for dinner. Suddenly Thera disappears and Berry becomes crazy of the silence and his unrequited love.

==Main cast==
* Katja Schuurman - Thera
* Egbert Jan Weeber - Berry Kooijman
* Johnny de Mol - Otto
* Edwin Jonker - Felicio
* Mohammed Chaara - Jamal
* Hans Dagelet - Mr. Kooijman, Berrys father
* Guusje Eijbers - Mrs. Kooijman, Berrys mother
* Cees Geel - Barber
* Touriya Haoud - Moroccan girl

==External links==
* 

 
 
 
 
 


 
 