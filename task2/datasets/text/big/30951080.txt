The Great Gatsby (2013 film)
 
 
{{Infobox film
| name           = The Great Gatsby
| image          = TheGreatGatsby2012Poster.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = Theatrical release poster
| director       = Baz Luhrmann Catherine Martin Douglas Wick
| screenplay     = Baz Luhrmann Craig Pearce
| based on       =   Jason Clarke Jack Thompson Amitabh Bachchan Craig Armstrong
| cinematography = Simon Duggan
| editing        = Jason Ballantine Jonathan Redmond Matt Villa Bazmark Productions Red Wagon Entertainment Warner Bros. Pictures Roadshow Entertainment  
| released       =   
| runtime        = 142 minutes  
| country        = Australia United States
| language       = English
| budget         = $105 million 
| gross          = $351 million   }}
 3D drama novel of the same name. The film was co-written and directed by Baz Luhrmann, and stars Leonardo DiCaprio, Tobey Maguire, Carey Mulligan, Joel Edgerton, and Elizabeth Debicki.  It follows the life and times of millionaire Jay Gatsby and his neighbour Nick, who recounts his encounter with Gatsby at the height of the Roaring Twenties. The film was originally going to be released on December 25, 2012, but moved to May 10, 2013 in 3D. Best Production Best Costume Design, winning both.

==Plot==
 
In the winter of 1929, Nick Carraway, a Yale University graduate and World War I veteran, is staying at a psychiatric hospital to treat his alcoholism. He talks about Jay Gatsby, describing him as the most hopeful man he had ever met. When he struggles to articulate his thoughts, his doctor, Walter Perkins, suggests writing it down, since writing is Nicks true passion.
 bond salesman after abandoning writing. He rents a small house on Long Island in the (fictional) village of West Egg, next door to a lavish mansion belonging to Jay Gatsby, a mysterious business magnate who often holds extravagant parties. One day, Nick drives across the bay to the old money East Egg to have dinner with his cousin, Daisy Buchanan, and her husband, Tom, a college acquaintance of Nicks. They introduce Nick to Jordan Baker, a cynical young golfer with whom Daisy wishes to couple Nick.

Jordan tells Nick that Tom has a mistress who lives in the "valley of ashes," an industrial dumping ground between West Egg and New York City. Not long after, Nick travels with Tom to the valley, where they stop by a garage owned by George Wilson and his wife, Myrtle, who is the mistress Jordan mentioned.

As the summer progresses, Nick receives an invitation to one of Gatsbys parties. Upon arriving, he learns that he is the only one who received an invitation, and that none of the guests have ever met Gatsby. There are multiple theories as to who he is: a German spy, a prince, even an assassin. Nick encounters Jordan, and they meet Gatsby, who is surprisingly young and rather aloof. Gatsbys butler later informs Jordan that Gatsby wishes to speak with her privately.

Gatsby seemingly takes a liking to Nick, and one day Gatsby offers to drive Nick to town in his expensive yellow car. On the road, Gatsby tells Nick that he is an Oxford man and a war hero, who was born into a wealthy family in the Midwest who have all since died. Gatsby takes Nick to a speakeasy, where he introduces him to Meyer Wolfsheim, a mob boss and business partner Gatsby. Jordan later tells Nick that Gatsby had a relationship with Daisy five years earlier, and is still in love with her. Gatsby had been throwing the extravagant parties in the hopes Daisy will attend. Gatsby later asks Nick to invite Daisy to tea at his house, without mentioning that Gatsby will be there.

After an awkward reunion, Gatsby and Daisy begin an affair. Gatsby is rather dismayed that Daisy wants to run away from New York with him, as his initial plan being for them was to live in his mansion. Nick tries to explain to Gatsby that the past cannot be repeated, but he dismisses the remark. Trying to keep the affair a secret, Gatsby fires the majority of his servants and discontinues the parties. Eventually, he phones Nick and asks that he and Jordan accompany him to the Buchanans, where they plan to tell Tom that Daisy is leaving him. Nick is hesitant, but Gatsby insists they need him.

During the luncheon, Tom becomes increasingly suspicious of Gatsby when he sees him staring passionately at Daisy. Daisy stops Gatsby from revealing anything about their relationship, and suggests they all go into town. Everyone leaves for the Plaza Hotel|Plaza, Tom driving Gatsbys yellow car with Nick and Jordan, while Gatsby and Daisy take Toms car, which is blue. Out of gas, Tom stops at George and Myrtles garage, where George says he and his wife are moving west, telling Tom he suspects Myrtle is cheating on him, not knowing that Tom is Myrtles lover.

At the Plaza, Gatsby tells Tom that he and Daisy are together, claiming that she never loved him. Outraged, Tom accuses Gatsby of making his fortune illegally through bootlegging with his mobster friends. Daisy tells Gatsby that she loved him and still loves him, but she cannot claim that she never loved Tom even once. Tom promises that he loves Daisy and that he will take better care of Daisy as Daisy reminds him of his faults in their marriage. As Tom tells Gatsby that he is different from them due to his dubious background, Gatsby lashes out at Tom, frightening Daisy. She leaves with Gatsby, this time in his car.

Later that night, Myrtle rushes out into the street after a fight with her husband about her infidelity. She sees Gatsbys yellow car approaching and runs toward it, believing Tom is driving and had come for her. Myrtle runs out to the street, shouting Toms name. Wilson sees this and calls out for her. Myrtle continues shouting and waves her arms in the air. Gatsbys car comes faster. Myrtle screams as the car strikes her, instantly killing her. George witnesses this. Myrtles corpse thuds, her body torn open and bloodied. Gatsbys car fled the scene. Afterwards, Tom, Nick, and Jordan stop by the garage when they see a large crowd has gathered and learn about Myrtles death. Tom tells a distraught George the yellow car belongs to Gatsby and that he suspects Gatsby was the one sleeping with Myrtle.

Nick finds Gatsby lingering outside the Buchanans mansion, where Gatsby accidentally reveals that Daisy was the driver, though he intends to take the blame. Nick eavesdrops on Daisy and  Tom, where he hears Daisy accept Toms promise that he will take care of everything. Nick is disappointed, but decides not to tell Gatsby since his friend hopes for Daisys call. Gatsby invites Nick over for the night and tells him the truth about his origins: he was born penniless, his real name is James Gatz, and he had asked Daisy to wait for him after the war until he had made something of himself. Hed then met Meyer Wolfsheim and entered his "business."

The next morning, Nick leaves for work and Gatsby decides to go for a swim before the pool is drained for the season. He hears the phone ringing, and, believing it is Daisy, climbs out of the pool as the butler answers the phone. Gatsby is then shot and killed by George, who proceeds to kill himself.

Nick invites Daisy to Gatsbys funeral, only to learn that she, Tom, and their daughter are leaving New York. The funeral is attended only by reporters and photographers, whom Nick angrily chases out. The media accuses Gatsby of being Myrtles lover and the one who killed her, leaving Nick the only person knowing the truth. Nick realizes that he is the only person who actually cared about Gatsby. Disgusted with both the city and its people, he leaves New York, but not before taking a final walk through Gatsbys deserted mansion; reflecting on Gatsbys unique ability to hope, and how he lost everything. Back in the sanatorium, Nick finishes his memoir and titles it "Gatsby," then takes out a pen to re-title it, "The Great Gatsby."

==Cast==
 
* Leonardo DiCaprio as Jay Gatsby
* Tobey Maguire as Nick Carraway
* Carey Mulligan as Daisy Buchanan
* Joel Edgerton as Tom Buchanan
* Elizabeth Debicki as Jordan Baker
* Isla Fisher as Myrtle Wilson  Jason Clarke as George Wilson
* Amitabh Bachchan as Meyer Wolfsheim Jack Thompson as Dr. Walter Perkins
* Adelaide Clemens as Catherine
* Callan McAuliffe as Young Jay Gatsby Richard Carter as Herzog
* Max Cullen as Owl Eyes
* Heather Mitchell as Daisys mother
* Gus Murray as Teddy Barton
* Steve Bisley as Dan Cody
* Vince Colosimo as Michaelis
* Felix Williamson as Henri
* Kate Mulvany as Mrs. Mckee
* Eden Falk as Mr. Mckee iOTA as Trimalchio
* Brendan Maclean as Klipspringer
 

==Production==

===Development=== opera and novel of the same name.    In December 2008, Variety (magazine)|Variety magazine reported that this film adaptation was to be made with Baz Luhrmann to direct it.
 Chelsea in Consumer Electronics Show in January 2011, he told The Hollywood Reporter that he had been workshopping The Great Gatsby in 3D, though he had not yet decided whether to shoot in the format.  In late January 2011, Luhrmann showed doubt about staying on board with the project,  before deciding to stay.

In 2010, it was reported that the film was being set up by Sony Pictures Entertainment  but by 2011, Warner Bros. was close to acquiring a deal to finance and take worldwide distribution of The Great Gatsby. 

===Casting=== Catherine Martin at the premiere of The Great Gatsby in Sydney, May 22, 2013]] Deadline Hollywood Michelle Williams, and Scarlett Johansson, as well as considering Natalie Portman, for Daisy.    Soon after, with her commitment to Cameron Crowes We Bought a Zoo, Johansson pulled out. 

On November 15, Luhrmann announced that Carey Mulligan had been cast to play Daisy after reading for the part on 2 November in New York.    She got the role shortly after Luhrmann showed her audition footage to Sony Pictures Entertainment executives Amy Pascal and Doug Belgrad, who were impressed by the actresss command of the character.  Mulligan burst into tears after learning of her casting via a phone call from Luhrmann, who informed her of his decision while she was on the red carpet at an event in New York. Luhrmann said "I was privileged to explore the character with some of the worlds most talented actresses, each one bringing their own particular interpretation, all of which were legitimate and exciting. However, specific to this particular production of The Great Gatsby, I was thrilled to pick up the phone an hour ago to the young Academy Award|Oscar-nominated British actress Carey Mulligan and say to her: Hello, Daisy Buchanan." 
 Luke Evans was a major contender.  Isla Fisher was cast to play Myrtle Wilson.    Australian newcomer Elizabeth Debicki won the part of Jordan Baker, right after graduating from Victorian College of the Arts.      
 Jason Clarke was cast as George B. Wilson.    Indian actor Amitabh Bachchan makes a cameo appearance as Meyer Wolfshiem; this was his first Hollywood role.   

===Filming=== Red Epic Zeiss Ultra 66th Cannes RealD 3D and 2D formats.

====Sets====
  in 1922, during the period that Fitzgerald would have known it]] Catherine Martin stated that the team styled the interior sets of Jay Gatsbys mansion with gilded opulence, in a style that blended establishment taste with Art Deco.    The long-destroyed Beacon Towers, thought by scholars to have partially inspired Fitzgeralds Jay Gatsby estate, was used as a main inspiration for Gatsbys home in the film.     The filming for the exterior of Jay Gatsbys mansion was the college building of the International College of Management, Sydney,  Some inspiration was also drawn from other Gold Coast, Long Island mansions, including Oheka Castle and La Selva Mansion.    Features evoking the Long Island mansions were added in post-production. 
 .]]
The inspiration for the film version of the Buchanan estate came from Old Westbury Gardens.  The mansion exterior was built on a soundstage, with digital enhancements added.  The interior sets for the Buchanan mansion were inspired by the style of Hollywood Regency. 
 American Arts and Crafts, with Gustav Stickley-type furnishings inside and an Adirondack chair|Adirondack-style swing out. 

The opening scene was filmed from Rivendell Child, Adolescent and Family Unit in Concord, Sydney, only a few kilometres from Sydney 2000 Olympic Stadium.

====Costumes==== Catherine Martin and Miuccia Prada were behind the wardrobe and worked closely together to create pieces with "the European flair that was emerging amongst the aristocratic East Coast crowds in the 1920s" 

Costume historians of the period, however, said that the costumes were not authentic, but instead modernized the 1920s-era fashions to look more like modern fashions. Most prominently, the women were clothed to emphasize their breasts, such as Daisys push-up bra, in contrast to the flat-chested fashions of the era. While the book was set in 1922, the movie included fashions from the entire decade of the 1920s and even the 1930s. Many of the fashions from archives were concepts from runways and fashion magazines that were never worn by women in real life. Martin says that she took the styles of the 1920s and made them sexier, and was trying to interpret 1920s styles for a modern audience. Alice Jurow, of the Art Deco Society of California, said that she loved the movie, but most of their members prefer more period-perfect films. The mens costumes were more authentic, except that the pants were too tight. 

==Marketing== Happy Together" Back to Young and Bedroom Hymns" and "Over the Love", performed by Florence and the Machine.   

On April 15, 2013, Brooks Brothers premiered "The Gatsby Collection", a line of mens clothing, shoes and accessories "inspired by the costumes designed by Catherine Martin for Baz Luhrmanns The Great Gatsby". According to Fashion Weekly, "The looks werent simply based on 1920s style: the new duds were designed based on the brands actual archives   Brooks Brothers was one of the initial arbiters of Gatsby-era look. The actual costumes, designed by Catherine Martin, will be on display in select Brooks Brothers boutiques."  

On April 17, 2013,  , a headpiece (both reportedly based on archival Tiffany designs), a necklace, and four different rings, including one in platinum with a 5.25-carat diamond, priced at $875,000.   

==Soundtrack==
  Target exclusive release also features three extra tracks.  The film score was executive-produced by Jay-Z    and The Bullitts.   
 Young and Hip hop references the Back to EDM wobble. Jamie Smith telling MTV that the bands contribution to the soundtrack sounds like "despair",  and revealing that it utilizes a 60-piece orchestra.

Speaking of his goals for the movies musical backdrop, Baz Luhrman expressed his desire to blend the music of the Jazz Age associated with the 1922 setting of the story with a modern spin. Much like his modern twists applied in Moulin Rouge! and Romeo + Juliet, Baz uses the movies music not as a background, but instead prominently in the foreground, which takes on a character of its own. 

==Reception==

===Box office===
The Great Gatsby earned $144.8 million in North America, and $206.2 million in other countries, for a worldwide total of $351 million.  Calculating in all expenses, Deadline.com estimated that the film made a profit of $58.6 million.  

In North America, The Great Gatsby earned $19.4 million on its opening Friday, including $3.25 million from Thursday night and midnight shows.    It went on to finish in second place, behind Iron Man 3, during its opening weekend, with $51.1 million.  This was the sixth-largest opening weekend for a film that didnt debut in first place,  the second largest opening weekend for a film starring Leonardo DiCaprio behind Inception,  and Luhrmanns highest grossing movie. 

===Critical response===
 
The Great Gatsby received mixed reviews from critics. Review aggregation website Rotten Tomatoes gives a rating of 48%, based on 257 reviews, with an average rating of 5.8/10. The sites critical consensus reads, "While certainly ambitious—and every bit as visually dazzling as one might expect—Baz Luhrmanns The Great Gatsby emphasizes visual splendor at the expense of its source materials vibrant heart."  Metacritic gives the film a score of 55 out of 100, based on 45 critics, indicating "mixed or average reviews". 

Among major critics,   review felt "Luhrmann is exactly the wrong person to adapt such a delicately rendered story, and his 3D feature plays like a ghastly Roaring 20s blowout at a sorority house." J.R. Jones,  , Chicago Reader, accessed May 10, 2013. 

The positive reviews included   of The Boston Globe reserved special praise for DiCaprios performance, saying "magnificent is the only word to describe this performance&nbsp;— the best movie Gatsby by far, superhuman in his charm and connections, the host of revels beyond imagining, and at his heart an insecure fraud whose hopes are pinned to a woman." 

The Scene Magazine gave the movie a "B-" rating, but praised the actors performances, in particular saying that "the stand-out actor is Joel Edgerton as Tom Buchanan doing an excellent job of showing the character’s gruffness, despite the one-dimensionality given to him".  A granddaughter of Fitzgerald praised the style and music of the film. 

Tobey Maguires role as Nick was given mixed to negative reviews from critics, with Philip French of The Guardian calling him "miscast or misdirected;"  Ann Hornaday of The Washington Post saying "Tobey Maguire is his usual recessive presence, barely registering as either a dynamic part of the events he describes or their watchful witness;"  and Elizabeth Weitzman of The New York Daily News saying despite "the wry-observational skills needed for Nicks Midwestern decency", the character is "directed toward a wide-eyed, one-note performance".  Rick Groen of The Toronto Star star was more positive of Maguires character, saying "our narrator,   prone to his occasionally purple rhetoric. But that imposed conceit, the image of a talented depressive writing from inside the bauble of his imagination, seems to validate his inflated prose and, better yet, lets us re-appreciate its inherent poetry." 

While the movie has inspired many Gatsby themed parties, the original novel was actually deeply critical of the self-indulgent lifestyle of rich people. Due to its critical tone and tragic ending, the story has been called a cautionary tale of the decadent downside of the American dream. A few years before the movie was released, Prince Harry attended a Gatsby-themed 21st birthday party that cost $25,000 to throw. The following year, Paul McCartney threw his own expensive Gatsby birthday gala. As Zachary Seward of The Atlantic puts it, "Its like throwing a Lolita-themed childrens birthday party."

Audiences polled by the market research firm CinemaScore gave The Great Gatsby a "B" grade on average. 

===Accolades===
{| class="wikitable sortable" style="width:95%;"
|- style="background:#ccc; text-align:center;"
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipients and nominees
! Result
|- Academy Awards   86th Academy March 2, 2014 Best Production Design Catherine Martin Beverley Dunn (Set Decoration)
|  
|- Best Costume Design Catherine Martin
|  
|-
| rowspan="14"| AACTA Awards January 30, 2014 Best Film
| Baz Luhrmann, Catherine Martin, Douglas Wick, Lucy Fisher, and Catherine Knapman
|  
|- Best Direction
| Baz Luhrmann
|  
|- Best Adapted Screenplay
| Baz Luhrmann and Craig Pearce
|  
|- Best Actor in a Leading Role
| Leonardo DiCaprio
|  
|- Best Actress in a Leading Role
| Carey Mulligan
|  
|- Best Actor in a Supporting Role
| Joel Edgerton
|  
|- Best Actress in a Supporting Role
| Elizabeth Debicki
|  
|-
| Isla Fisher
|  
|- Best Cinematography
| Simon Duggan
|  
|- Best Editing
| Matt Villa, Jason Ballantine, and Jonathan Redmond
|  
|- Best Original Music Score Craig Armstrong
|  
|- Best Sound
| Wayne Pashley, Jenny Ward, Fabian Sanjurjo, Steve Maslow, Phil Heywood, and Guntis Sics
|  
|- Best Production Design
| Catherine Martin, Karen Murphy, Ian Gracie, and Beverley Dunn
|  
|- Best Costume Design
| Catherine Martin, Silvana Azzi Heras, and Kerry Thompson
|  
|- AACTA International Awards January 10, 2014 Best Supporting Actor
| Joel Edgerton
| 
|- Best Direction
| Baz Luhrmann
| 
|- Art Directors Guild  February 8, 2014 Excellence in Production Design - Period Film Catherine Martin
| 
|- British Academy Film Awards  February 16, 2014 Best Costume Design Catherine Martin
| 
|- Best Make-up and Hair Maurizio Silvi, Kerry Warn
| 
|- Best Production Design Catherine Martin, Beverley Dunn
| 
|-
| Costume Designers Guild 
| February 22, 2014
| Excellence in Period Film
| Catherine Martin
|  
|- Gay and Lesbian Entertainment Critics Association 
| rowspan="2"| January 21, 2014
| Campy Flick of the Year
|
|  
|-
| Visually Striking Film of the Year
|
|  
|-
|-
| Empire Awards  March 30, 2014 Best Female Newcomer
| Elizabeth Debicki
|  
|-
| rowspan="3"| Grammy Awards  56th Annual January 26, 2014 Grammy Award Best Compilation Soundtrack for Visual Media Baz Luhrmann
| 
|-  Grammy Award Best Song Written For Visual Media  Young and Young and Beautiful Music by Lana Del Rey and Rick Nowels, Lyrics by Lana Del Rey
|  
|- Grammy Award Best Score Soundtrack For Visual Media Craig Armstrong 
| 
|-
| International 3D Societys Creative Arts Awards 
| January 28, 2014
| Outstanding Live Action 3D Feature Film
|
|  
|- Motion Picture Motion Picture Sound Editors Golden Reel Awards   February 16, 2014 Best Sound Editing: Music Score in a Feature Film Jason Ruder
| 
|- 
| rowspan="3"| Satellite Awards February 23, 2014 Best Art Direction and Production Design
| Catherine Martin (Art Direction); Beverley Dunn (Set Decoration)
|  
|- Best Costume Design
| Catherine Martin
|  
|- Best Original Song Young and Beautiful Music by Lana Del Rey and Rick Nowels, Lyrics by Lana Del Rey
|  
|-
| rowspan="3"| St. Louis Gateway Film Critics Association December 14, 2013
| Best Cinematography
| Simon Duggan
|  
|-
| Best Art Direction
|
|  
|-
| Best Soundtrack
|
|  
|- Visual Effects Society Awards  February 12, 2014 Outstanding Supporting Visual Effects in a Feature Motion Picture Chris Godfrey, Prue Fletcher and Joyce Cox
| 
|-
| rowspan="3"| Washington D.C. Area Film Critics Association December 9, 2013 Best Director
| Baz Luhrmann
|  
|- Best Art Direction
| Catherine Martin and Beverley Dunn
|  
|- Best Cinematography
| Simon Duggan
|  
|- Young Artist Awards    35th Young May 4, 2014 35th Young Best Supporting Young Actor in a Feature Film Callan McAuliffe
| 
|}

==See also==
 
Other film adaptations of The Great Gatsby include:
* The Great Gatsby (1926 film)|The Great Gatsby (1926 film), a silent film starring Warner Baxter and Lois Wilson
* The Great Gatsby (1949 film)|The Great Gatsby (1949 film), starring Alan Ladd and Betty Field
* The Great Gatsby (1974 film)|The Great Gatsby (1974 film), starring Robert Redford as Gatsby and Mia Farrow as Daisy
* The Great Gatsby (2000 film)|The Great Gatsby (2000 film), a TV film starring Paul Rudd as Nick Carraway, Toby Stephens as Gatsby, and Mira Sorvino as Daisy
* G (2002 film)|G, a loosely adapted hip hop musical starring Richard T. Jones

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  : novel and film information

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 