Howard Cantour.com
{{Infobox film
| name = Howard Cantour.com 
| image          = File:HowardCantourMoviePoster2012.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Film festival poster
| director = Shia LaBeouf Thomas Lennon  Portia Doubleday
| released =  
| runtime = 11 minutes
| country = United States
| language = English
}} did not seek permission from Clowes, or acknowledge Clowes in the films credits.

==Synopsis==
The film opens with Howard Cantour (Jim Gaffigan) reflecting on being a critic and how a review can either promote or devastate a films chances of survival. The movie then cuts to a scene of Howard sitting in a restaurant with Dakota (Portia Doubleday), a young review critic. Howard is dismissive of her, saying that he wouldnt have bothered talking to her if she wasnt attractive. The two discuss an upcoming film preview and Dakota states that she believes that the two of them should help the movie out as the films production company is not going to heavily promote it. Howard states that he hopes that the film fails, as he has grown jaded with how directors overall deal with reality in a film. He goes on to argue that he grew disgusted with the films director as he had filmed a scene in a previous film where a kid stole his mothers ring, which Howard believes was done only to show how horrible people can be.
 Thomas Lennon) and is told that only specific film critics (namely newspaper critics) will be allowed into the discussion with the director. Rocco disdainfully says that he thinks that the films production company believes that the online film critics will treat the film like a newspaper critic would, meaning that they would review the film more positively. Dakota then approaches the two men and asks their opinion about the film, to which Rocco says that he would be posting a mixed review. Hes surprised when Dakota says that Howard would be writing a negative review, as at one point Howard had given the director an overwhelmingly positive review. However it is this same review that gains Howard entrance into the discussion. Hes nervous, but correctly assumes that that the newspaper critics would try to commandeer the entire proceedings. Howard wishes that his younger self that had written the positive review were there and reminisces how he had written the review in order to impress his then-wife Ellen.

He leaves without talking to the director, only for Dakota to ask if he had asked the director about the shot that they had discussed earlier about the stolen ring. Dismayed that he did not talk to the director, she brings him over. The director then explains that the film in question had a deleted scene that explained the reasons behind the scene, but Howard doubts the truthfulness of this claim. Howard debates over whether or not to post his scathing review and briefly thinks about how his wife Ellen left him before ultimately choosing to upload his review.

==Cast==
*Jim Gaffigan as Howard Cantour Thomas Lennon as Rocco Eppley
*Portia Doubleday as Dakota
*Dito Montiel as Holly Pondyoke
*Caroline Morahan as Abigail
*Joseph Alfieri as Mr. Baldwin / Press
*Skoti Collins as British Film Critic
*Sarah Hester as Press
*Colin Molloy as Round Table Critic / Press Junket
*Spence Nicholson as Film Crew Electrician
*Heidi Niedermeyer as Courtney
*Caitlin OConnor as Concession Stand Girl
*Caileigh Scott as Video Game Girl
*Deanna Lynn Walsh as Round Table Critic / Press Junket
*Mir Wave as Press Critic

==Plagiarism==
Shortly after the films online release, bloggers and newspapers familiar with indie comics noticed its resemblance to Justin M. Damiano, a 2007 comic by Ghost World author Daniel Clowes from the anthology The Book of Other People.    Wired (website)|Wired noted the similarities between the film and the comic, writing:

{{Quote|text=How closely does the film, which appeared at several film festivals, hew to the comic? Well, both open with exactly the same monologue from their eponymous leads: "A critic is a warrior, and each of us on the battlefield have the means to glorify or demolish (whether a film, a career, or an entire philosophy) by influencing perception in ways that if heartfelt and truthful, can have far-reaching repercussions."

Both stories then switch to a scene wherein the titular critic discusses a film with a freelance critic he dislikes, who asks whether he’s attending a junket where the director will be present. In Clowes, the freelance critic explains that the director "so perfectly gets how we’re really all like these aliens who can never have any meaningful contact with each other because we’re all so caught up in our own little self-made realities, you know?" In LaBeouf’s short, she says the director "so perfectly gets how we’re all like these aliens to one another, who never have any meaningful contact with one another because we’re all so caught up in our little self-made realities, you know?" |sign=Graeme McMillan|source=Wired (website)|Wired  }}

LaBeouf removed Howard Cantour.com from his website, claiming that he was not copying Clowes, but rather was inspired by him and "got lost in the creative process."  He later issued several apologies to Clowes via Twitter, who responded by saying that "The first I ever heard of the film was this morning when someone sent me a link. Ive never spoken to or met Mr. LaBeouf ... I actually cant imagine what was going through his mind."    The AV Club remarked on the apology, which they found was identical to a Yahoo! Answers post on the subject of plagiarism posted in 2010.    This raised allegations about further plagiarism on LeBoufs part, as various sites noted that some of LaBeoufs other works appear to have been copied without attribution and that further apologetic tweets from LaBeouf appeared to have been fashioned after unrelated apologies by other public figures.       A lawyer for Clowes later issued a cease and desist letter to LaBeouf through his lawyer, which LaBeouf later posted on Twitter. 

==Reception==
Upon its initial premiere at the 2012 Cannes Film Festival Howard Cantour.com received a large amount of critical acclaim.    IndieWire posted a review that stated that the film was "ultimately a surprisingly successful movie on the experience of watching a movie that should only serve to encourage LaBeouf to further test the directorial waters".  Howard Cantour.com also received some praise when it was released to the Internet,  but has since experienced a backlash due to LaBeoufs plagiarism of Clowes comic.

==References==
 

==External links==
* 

 
 
 
 
 