Blinky Bill (film)
 
 
{{Infobox film
|name=Blinky Bill
|image=Blinkyfilm.jpg
|image_size=
|caption=Blinky Bill poster
|director=Yoram Gross
|producer=Yoram Gross Sandra Gross  (executive producer)  Tim Brooke-Hunt  (executive producer)  John Palmer Leonard Lee Yoram Gross Robin Moore Keith Scott
|music=Guy Gross
|cinematography=Frank Hammond Paul Ozerski Lee Smith
|distributor=Beyond Distribution Yoram Gross Film Studio Film Finance Corporation Australia
|released=  September 24, 1992   and   December 25, 1992   March 30, 1994   February 18, 1995
|runtime=90 minutes
|country=Australia
|language=English
}}  Yoram Gross Film Studio in 1992. It was released in Australia on 24 September of the same year.
 Australian bush. The peace and charm of their existence is shattered by the destruction of their homes by humans. Blinky Bill rallies his friends, in a series of exciting adventures, as they battle to protect their homes from destruction and as he rescues his mother from captivity. The animation is superimposed over live action footage of the Australian bush.
 CGI movie would be released at the end of 2015. 

==Plot==
The local woodlanders are carrying with their everyday life as normal until the next morning, when two men Harry and Joe start to take down the entire forest with their tractor. The animals evacuate as many trees fall down, and more trees are cut down including Mrs Koalas home, and a tree knocks Blinky Bill unconscious.

At sunrise nothing is left of the bush and every animal is left homeless. They move out of the grounds to search for a new home, including Mr. Wombat. Blinky Bill, dazed and confused, calls for his mother, but she is nowhere in sight. Blinky rescues a young female koala named Nutsy from a cluster of fallen trees. They both run into Mr. Wombat. Nutsy tells him that Blinky has amnesia and Mr. Wombat explains to him about his life so far.

Blinky was a very mischievous sort causing trouble and receiving a scolding from Mayor Pelican, his teacher Miss Magpie and his mother for his many antics. He once encountered and escaped Ms. Pym at her general store. His mother then disciplines him and cries about the grief he is causing her. By the end of this story, Blinky is feeling very guilty about the trouble he caused his mother, so decides to find her.

Blinky Bill and Nutsy make their way towards a riverbank where they meet up with a hard of hearing Granny Grunty Koala. Down the river, they meet up with Splodge and his family who tell them about what happened to Mrs Koala who refused to evacuate out of pride of her own home. Blinky and Nutsy make their way to the waiting line where Mayor Pelican is assigning all the woodlanders to their places. Blinky Bill approaches the mayor who is less than pleased, and orders Nurse Angelina to take the children to the leaking North Cave. Unfortunately Blinkys mother isnt here.

Blinky asks Ruffs mother for directions to the woodchip mill. Ignoring the fact there is danger there, Blinky goes to find his mother at the mill, and Nutsy follows him as they make their way though a rampaging river and the woods. They finally make it to the mill. The wood chip mill is home to the very woodcutters that felled their homes.

Next morning Blinky and Nutsy witness the woodcutters reducing the animals former homes into sawdust, narrowly escaping the circular buzz-saw. Then they stay in hiding till night time, but as they try to escape, they alert the dogs. When Blinky slingshoots the security light, Harrys wife Joan urges her husband to investigate. Blinky manages to slip out of the place, but Nutsy is trapped and makes a run for it, climbing into the bedroom of the familys daughter Clara.

Blinky tearfully goes back to the tell the others about the tragic news. Blinkys gang form a rescue party, forcing Marcia to join them. Blinky has Jacko para-drop Marcia down the chimney of the woodcutters house taking notes of the house layout and items. Meanwhile Nusty climbs into Claras bed who wakes up happy to find a real koala in her bed and manages to get her parents to let to her keep the koala temporarily.

Blinkys gang sneak up to the house at night, then break into the house, but then they start up a commotion. Clara immediately hides herself and Nutsy in her bedroom cupboard. Splodge manages to fend off both the dogs and Joe, while Blinky keeps Harry and Joan locked in their bedroom. In the chaos that follows, the wood chip machines starts up. Blinky gets Nutsy out of the house and they both fight off Harry. The chain of chaotic events against Harry and Joe end them up in a water tank. Blinky locates his mother and they all leave the place in the woodcutters truck, while Clara waves tearfully goodbye to her new-found koala friends.

==Movie characters==

===Animal characters===
*Blinky Bill - a young koala and the titular movie character.
*Nutsy - a female koala and Blinkys adoptive sister. She was by taken in by mistake by Clara, Harrys small daughter, who likes koalas.
*Splodge - Blinkys best friend. he is a kangaroo, who lives with his parents.
*Marcia - One of Blinkys friends. She is a tomboyish marsupial mouse.
*Flap - A brown platypus and one of Blinky Bills friends.
*Mrs Koala - Blinky Bill and Nutsys mother. She was kidnapped by Harry and his assistant Joe.
*Mr Wombat - Blinkys mentor. He doesnt like when Blinky calls him "Wombo".
*Miss Magpie - Blinkys school teacher.
*Mayor Pelican - The Greenpatch mayor. He hates it when Blinky gets up to mischief.
*Mr Emu - Mayor Pelicans helper.
*Jacko - A spoiled kookaburra, who often laughs.
*Nurse Angelina - A wallaby, who wears a nurse uniform.

===Humans===
*Harry - a fat woodcutter, Joes boss, Joans husband and Claras father.
*Joe - Harrys assistant.
*Joan - Harrys wife and Claras mother.
*Clara - Harry and Joans six-year-old blonde daughter, who loves animals unlike her parents.
*Ms. Pym - a shopkeeper, who has a dislike for koalas.

==Cast==
{|class="wikitable"
|-
!Actor!!Role
|- Robyn Moore Robyn Moore||Blinky Bill, Nutsy, Marcia, Mrs Koala, Miss Magpie, Nurse Angelina, Joan, Clara, Ms. Pym 
|- Keith Scott Keith Scott||Splodge, Flap, Mr Wombat, Mayor Pelican, Mr Emu, Jacko, Harry, Joe 
|- Ross Higgins||Frogs
|}

==Soundtrack== Robyn Moore Keith Scott except where indicated.

#"You and Me" - Robyn Dunn and Geoff Robertson, with backing vocals of Kevin Bennett
#"I’m Old Man Wombo"
#"I’m Blinky Bill"
#"Arithmetic Song"
#"Gribbet" - Ross Higgins and Guy Gross
#"I Am the Mayor"
#"Nutsy’s Ballad"
#"Motherhood"
#"How I Hate Koalas"
#"Snakes are So Superior"
#"Country Girls"
#"Don’t Try to Sing Underwater" - Guy Gross
#"Home"
#"Who’s on the Menu Tonight"
#"Whistle Song" Julie Anthony

==Box Office==
Blinky Bill grossed $1,903,659 at the box office in Australia. 

==References==
 

==External links==
* 
*  at the National Film and Sound Archive
*  at Screen Australia
*  at Music Australia
 
 

 
 
 
 
 
 
 
 