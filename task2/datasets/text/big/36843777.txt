Those Who Dance
{{Infobox film
| name           = Those Who Dance
| image          = ThoseWhoDance1930Poster.jpg
| imagesize      =
| caption        =
| director       = William Beaudine
| producer       = Robert North Joseph Jackson
| story          = George Kibbe Turner
| writer         = 
| starring       = Monte Blue Lila Lee Betty Compson Sid Hickox
| editing        = George Amy
| distributor    =Warner Bros.
| released       =  
| runtime        = 75 mins.
| country        = United States
| language       = English
}}
 Pre code 1924 silent film that starred Bessie Love and Blanche Sweet.  The story, written by George Kibbe Turner, was based on events which actually took place among gangsters in Chicago.

==Synopsis==
Monte Blue plays as a police detective who is after a famous gangster (played by William Boyd). He disguises himself and lives in the very house of the famous gangster by pretending he is an out-of-town gangster who has just murdered someone. He pretends he is the sweetheart of an innocent girl (played by Lila Lee) who suspects her brother has been framed for murder by Monte Blue. Blues moll, played by Betty Compson, is also in on the conspiracy as she had become fed up with his cheating, lying and brutal treatment. The life of Lees brother depends, who has been sentenced to death in the electric chair, depends on them getting evidence against Boyd.

==Foreign-language versions== 1924 silent version upon which the 1930 versions were based.

==Pre-code material==
The film contains a lot of pre-code material. Some examples include: Lila Lees character is called "a professional virgin." Two unmarried couples live together, with the unspoken understanding that this is not true love forever, just a temporary situation for the sake of convenient sex for all parties concerned and hot meals on the table for the men as long as the situation lasts. There is also a gay reference about a man being "that way" about Tim Brady (played by William Janney), etc.

==Preservation==
The film survives complete. It was transferred unto 16mm film by Associated Artists Productions in the 1956 and shown on television. A 16mm copy is housed at the Wisconsin Center for Film & Theater Research.  Another print exists at the Library of Congress. 

==Cast==
 , April 1924]]

*Lila Lee - Nora Brady
*Monte Blue - Dan Hogan
*William "Stage" Boyd - Diamond Joe Jennings
*Betty Compson - Kitty
*William Janney - Tim Brady
*Wilfred Lucas - Big Ben Benson
*Cornelius Keefe - Pat Hogan
*DeWitt Jennings - Captain OBrien
*Gino Corrado - Tony
*Bob Perry - Bartender
*Charles McAvoy - Prison Guard
*Kernan Cripps - Detective
*Richard Cramer - Steve Daley
*Harry Semels - Hood
*Nick Thompson - Hood

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 