The Light Thief
{{Infobox film
| name           = The Light Thief (Svet-Ake)
| image          = TheLightThief2010Poster.jpg
| alt            =  
| caption        = US film poster
| director       = Aktan Arym Kubat
| producer       = Altynai Koichumanova Cedomir Kolar Thanassis Karathanos MarcBaschet Karl Baumgartner Denis Vaslin
| writer         = Aktan Arym Kubat
| starring       = Aktan Arym Kubat Taalaikan Abazova Askat Sulaimanov
| music          = 
| cinematography = Hassan Kydyraliyev
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 80 minutes
| country        = Kyrgyzstan
| language       = Kyrgyz
| budget         = 
| gross          = 
}} Best Foreign Language Film for the 83rd Academy Awards,  but did not make the final shortlist.   

==Cast==
* Aktan Arym Kubat as Svet-Ake 
* Taalaikan Abazova as Bermet
* Askat Sulaimanov as Bekzat
* Asan Amanov as Esen
* Stanbek Toichubaev as Mansur

==Reviews==
“The Light Thief might be a simple story, but its ultimately a complex film” –Montreal Gazette 

“European funds contributing to the production and with a burning subject such as energy control high on the world’s priority list, this is a surefire festival item, with definite chances for niche art house distribution.” – 

==Awards==
*Locarno International Film Festival - Piazza Grande
*Toronto International Film Festival - Contemporary World Cinema
*Cannes Film Festival - Directors Fortnight
*Eurasia International Film Festival - FIPRESCI Prize

== References ==
 

==External links==
*  - The Global Film Initiative
*  
*  - The Guardian UK
*  - Variety
*  - Hollywood Reporter

 
 
 
 

 