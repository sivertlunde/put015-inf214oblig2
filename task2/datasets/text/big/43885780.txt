Ponkathir
{{Infobox film
| name           = Ponkathir
| image          =Ponkathir.jpg
| image_size     =
| caption        =
| director       = ER Cooper
| producer       = P Subramaniam
| writer         = Muthukulam Raghavan Pilla, KP Kottarakkara
| screenplay     = Muthukulam Raghavan Pilla, KP Kottarakkara
| music          = Br Lakshmanan
| released       =  
| cinematography = NS Mani
| editing        = KD George
| studio         = Neela
| lyrics         =
| distributor    = A Kumaraswamy Release
| starring       = Prem Nazir, Thikkurissi Sukumaran Nair
| country        = India Malayalam
}}
 Indian Malayalam Malayalam film, directed by ER Cooper and produced by P Subramaniam.  The film stars Prem Nazir, Thikkurissi Sukumaran Nair in lead roles.  The film had musical score by V Dakshinamoorthy. 

==Cast==
* Prem Nazir
* Pankajavalli
* S. P. Pillai
* Thikkurissi Sukumaran Nair
* TR Omana
* TS Muthaiah
* CI Parameswaran Pillai
* SR Pallatt
* CS Radhadevi
* Adoor Pankajam
* Kumari Thankam
* Santhi
* Muthukulam Raghavan Pilla
* Aranmula Ponnamma
* Lalitha
* Ragini

==References==
 

==External links==
*  

 
 


 