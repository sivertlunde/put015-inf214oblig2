Juno (film)
 Juno}}
 
{{Infobox film
| name = Juno
| image = Junoposter2007.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Jason Reitman
| producer = Lianne Halfon John Malkovich Mason Novick Russell Smith
| writer = Diablo Cody
| starring = Ellen Page Michael Cera Jennifer Garner Jason Bateman Allison Janney J. K. Simmons
| music = Mateo Messina
| cinematography = Eric Steelberg
| editing = Dana E. Glauberman
| studio  = Mandate Pictures Mr. Mudd
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 96 minutes
| country = Canada United States
| language = English
| budget = $6.5 –$7 million 
| gross = $231,411,584 
}}
Juno is a 2007 Canadian-American comedy-drama film directed by Jason Reitman and written by Diablo Cody. Ellen Page stars as the title character, an independent-minded teenager confronting an unplanned pregnancy and the subsequent events that put pressures of adult life onto her. Michael Cera, Jennifer Garner, Jason Bateman, Allison Janney, and J. K. Simmons also star. Filming spanned from early February to March 2007 in Vancouver, British Columbia. It premiered on September 8 at the 2007 Toronto International Film Festival, receiving a standing ovation.

Juno won the   and 20th Century Foxs first number one soundtrack since Titanic (soundtrack)|Titanic. Juno earned back its initial budget of $6.5 million in twenty days, the first nineteen of which were when the film was in limited release.  It went on to earn $231 million. Juno received positive reviews from critics, many of whom placed the film on their top ten lists for the year. It has received criticism and praise from members of both the United States pro-life movement|pro-life and Abortion-rights movements|pro-choice communities regarding its treatment of abortion.

==Plot==
Sixteen-year-old Minnesota high-schooler Juno MacGuff (Ellen Page) discovers she is pregnant with a child fathered by her friend and longtime admirer, Paulie Bleeker (Michael Cera). She initially considers an abortion. Going to a local clinic run by a womens group, she encounters outside a schoolmate who is holding a rather pathetic one-person pro-life vigil.  Once inside, however, Juno is alienated by the clinic staffs authoritarian and bureaucratic attitudes. She is particularly offended by their calling her "sexually active", a term which she feels demeans the highly emotional event by which she became pregnant. All of this decides her against abortion, and she decides to give the baby up for adoption. With the help of her friend Leah (Olivia Thirlby), Juno searches the ads in the Pennysaver and finds a couple she feels will provide a suitable home. She tells her parents, Mac (J.K. Simmons) and stepmother, Bren (Allison Janney), who offer their support. With Mac, Juno meets the couple, Mark and Vanessa Loring (Jason Bateman and Jennifer Garner), in their expensive home and agrees to a closed adoption.

Juno visits Mark a few times, with whom she shares tastes in punk rock and horror films. Mark, who has set aside his rock band youth (now confined to memorabilia displayed in the one room of the house that Vanessa has designated for Marks personal belongings), works at home composing commercial jingles. Juno and Leah happen to see Vanessa in a shopping mall being completely at ease with a child, and Juno encourages Vanessa to talk to her baby in the womb, where it obligingly kicks for her.

As the pregnancy progresses, Juno struggles with the emotions she feels for the babys father, Paulie, who is clearly in love with Juno. Juno maintains an outwardly indifferent attitude toward Paulie, but when she learns he has asked another girl to the upcoming prom, she angrily confronts him. Paulie reminds Juno that it is at her request they remain distant and tells her that she broke his heart.

Not long before her baby is due, Juno is again visiting Mark when their interaction becomes emotional. Mark then tells her he will be leaving Vanessa. Juno is horrified by this revelation, with Mark asking Juno "How do you think of me?" Vanessa arrives home, and Mark tells her he does not feel ready to be a father and there are still things he wants to do first. Juno watches the Loring marriage fall apart, then drives away and breaks down in tears by the side of the road. Returning to the Lorings home, she leaves a note and disappears as they answer the door.

After a heartfelt discussion with Mac, Juno accepts that she loves Paulie. Juno then tells Paulie she loves him, and Paulies actions make it clear her feelings are very much reciprocated. Not long after, Juno goes into labor and is rushed to the hospital, where she gives birth to a baby boy. She had deliberately not told Paulie because of his track meet. Seeing her missing from the stands, Paulie rushes to the hospital, finds Juno has given birth to their son, and comforts Juno as she cries. Vanessa comes to the hospital where she joyfully claims the newborn boy as a single adoptive mother. On the wall in the babys new nursery, Vanessa has framed Junos note, which reads: "Vanessa: If youre still in, Im still in.&nbsp;—Juno." The film ends in the summertime with Juno and Paulie playing guitar and singing together, followed by a kiss.

== Cast ==
* Ellen Page as Juno MacGuff
* Michael Cera as Paulie Bleeker, the father of Junos child
* Jennifer Garner as Vanessa Loring, Marks wife and the prospective adoptive mother of Junos child
* Jason Bateman as Mark Loring, Vanessas husband and the prospective adoptive father of Junos child
* Allison Janney as Bren MacGuff, Junos stepmother
* J. K. Simmons as Mac MacGuff, Junos father
* Olivia Thirlby as Leah, Junos friend
 
* Candice Accola as Amanda
* Rainn Wilson as Rollo

==Themes==
{{Quote box
| quote  = "You can look at it as a film that celebrates life and celebrates childbirth, or you can look at it as a film about a liberated young girl who makes a choice to continue being liberated. Or you can look at it as some kind of twisted love story, you know, a meditation on maturity."
| source = Diablo Cody
| width  = 30em
| align  = right
}}
Along with Knocked Up and Waitress (film)|Waitress, two other 2007 films about women facing unplanned pregnancies, Juno was interpreted by some critics as having a pro-life theme. Ann Hulbert of Slate (magazine)|Slate magazine believed that Juno "  both pro-life and pro-choice purism."  Jeff Dawson of The Sunday Times believed that the film was inevitably placed in the "unwanted pregnancy subgenre" with Knocked Up and Waitress due to its subject matter but thought that its interpretation as a pro-life film only "muddied the waters".  Hadley Freeman of The Guardian criticized Juno for "complet  a hat-trick of American comedies in the past 12 months that present abortion as unreasonable, or even unthinkable—a telling social sign", though she noted, "I dont believe any of these films is consciously designed to be anti-abortion propaganda."  A. O. Scott, writing for The New York Times, agreed that Juno has "an underlying theme, a message that is not anti-abortion but rather pro-adulthood".  Ellen Page commented, "What I get most frustrated at is when people call it a pro-life movie, which is just absurd... The most important thing is the choice is there, and the film completely demonstrates that."    Cody and Page have openly stated that they are pro-choice;   Reitman thought that it was "fantastic" that pro-life and pro-choice groups were embracing the film.    He said that "Juno seems to be a mirror, and people   see themselves in it". 

Other critics labeled Juno as  ".  She criticized the media perception of her character as a "strong woman", arguing that if Juno were a male character, the "strength" of the character would not be considered remarkable.    Reitman was interested in the personal/political conflict for Vanessas character: "Feminism has paved the way for Vanessa’s career, but ultimately Vanessa wants to be a full time mother." 

==Production==

===Development===
  wrote the film based on many of her own high school experiences.]] Target store in Crystal, Minnesota,    Cody compared writing to breathing, seeing Juno as an extension of herself.   
 Thank You for Smoking, had not been released yet, so he did not have any feature film credits.  Other directors, including Jon Poll,  were considered, but Reitman was chosen and he interrupted work on his own spec script in order to direct Juno.  Cody says she had a cynical attitude when writing Juno ("I didnt ever think this film would be produced")  and, indeed, the film was delayed by financial problems.  After its controversial nature scared off a number of major studios, John Malkovichs production company, Mr. Mudd, took on the project.  It was later brought to production company Mandate Pictures by co-producer Jim Miller. 

===Casting===
  as Juno when he first read the script.]] Hard Candy, Thank You for Smoking, not telling him that he intended Simmons to play Mac. Simmons says that, after reading the script, he would have been happy to play even the high school teacher who has no speaking lines.  Other cast members Reitman had "in mind right from the start" were Olivia Thirlby&mdash;who had originally unsuccessfully auditioned for the role of Juno&mdash;and Michael Cera.  He took them with Page and Simmons to a Panavision stage in California and shot 45 pages of the script on 35 mm film against a black backdrop. He presented this footage to Fox Searchlight as the initial cast.    Reitman highlighted the importance of doing a screen test instead of individual auditions, saying: "This is a movie that’s all about relationships and the idea of auditioning people outside of each other, one-on-one with the casting director, didn’t make sense." 
 The Kingdom, DJ and record producer, makes a cameo appearance as Juno and Paulies chemistry teacher. McFadden was doing scoring work for Reitman when he received the Juno screenplay and asked McFadden to appear in the film;  Reitman thought that it was "perfect irony" for the chemistry teacher to be played by Cut Chemist.   

===Filming===
  stood in for the fictional Dancing Elk High School.]] White Rock as Mark and Vanessas home, Eric Hamber Secondary School as Dancing Elk High School,  and South Surreys Athletic Park track as Dancing Elk High Schools athletics track.   

After minimal rehearsal,  filming spanned from early February across to March 2007  on a six-week schedule,  of which 30 days were designated to filming.  The crew was planning to import snow for the films winter events,  but it snowed on location, and they were able to re-schedule filming to shoot the winter scenes during snowfall, which second assistant director Josy Capkun says resulted in much wider snow shots than originally planned.  Although the film was shot out of sequence,  the final scene was scheduled for the final day and, after a long period of rain, the crew was intending to shut down production and resume months later to shoot the scene, set in summer.  However, the rain stopped and they were able to shoot the scene in the sun.  That final scene depicted Juno and Paulie singing The Moldy Peaches "Anyone Else but You", and band member Kimya Dawson visited the set to speak to Ellen Page and Michael Cera while they were practicing the song. 

===Music===
The movie features several songs performed by Kimya Dawson in her solo, Antsy Pants and The Moldy Peaches guises.  This was due to a suggestion by lead actress Ellen Page.  Director Jason Reitman explains:
 
"At one point, I asked Ellen Page before we started shooting, ‘ hat do you think Juno listens to?’ And she said   She went on my computer, played the songs, and I fell in love with it. Diablo and I discussed putting a Moldy Peaches song in it where the characters would sing to each other. I got in touch with Kimya Dawson of The Moldy Peaches  and she started sending me her work, which was beautiful, and that became a lot of the soundtrack." {{cite news |title=Juno Soundtrack: Best Soundtrack of The Year? |last=Lucy |date=September 12, 2007
|work=Product-Reviews |publisher=Dansway Communications Ltd |url=http://www.product-reviews.net/2007/09/12/juno-soundtrack-best-soundtrack-of-the-year/ }} 
 
  provided both solo songs and songs from two of her former bands.]] Thank You incidental film score|score.  He gave Messina a collection of Dawsons songs and asked him to create "the sound of the film" through an instrumental score that replicated the recording quality, tone, feel and innocence of her music.  Messina decided to implement an "acoustic guitar feel that was jangled and was really loose, like Juno."    Experimenting with different guitars, he ended up using "Stella," a second-hand guitar belonging to guitarist Billy Katz that he described as "kind of tinny, not perfectly in tune, but   has all kinds of character." Katz was hired to perform acoustic and classical guitar for the movies score, using "Stella" extensively throughout. 
 Sea of 1989 film Sea of Iggy Pop and the Stooges.   He felt that the Sonic Youth cover of "Superstar (Delaney and Bonnie song)|Superstar" defined Juno and Marks relationship—Juno preferring the classic 1971 version by The Carpenters while Mark preferred Sonic Youths 1994 cover.  "A Well Respected Man" by The Kinks was a song Reitman had associated with a character from another of his screenplays and says it was "heart-breaking" when he decided to include the song as an introduction for Paulie instead, despite feeling it suited the scene perfectly.  He found childrens music|childrens songwriter Barry Louis Polisars "All I Want Is You" after "surfing iTunes for hours on end" using different words and names as search terms and thought that the handmade quality was perfect for the opening titles, which were afterwards made to correspond to the song.   The "Brunch Bowlz" jingle, Mark writes in the film, was composed by advertisement writer Chris Corley with whom Reitman had previously worked on a set of commercials for Wal-Mart. 

===Design=== composited onto a fake tree outside Junos house and cherry blossom trees outside Leahs house were touched up in a lighter shade of pink to depict autumn; a fan was used to blow leaves around in some scenes as if the leaves were falling from trees. Fake flowers were used in front of Paulies house at the end of the film to give the impression of summer.  Reitman used different colors to inform character, such as the burgundy and gold Dancing Elk High School track uniforms and an early scene with Juno in a red hooded jacket "walking through a world of somber greens and browns."
 Excellence in supervising sound designer Scott Sanderss son Matthew and was embedded into the scene in post-production.

===Opening title sequence===
  opening title rotoscoped Juno Los Angeles. Xerox machine to degrade their quality until the pictures appeared hand-drawn.  The pictures were cut-out and scanned back onto the computer then layered onto the background drawn by Lee with compositing software  to create a stop motion animation sequence that corresponded to "All I Want Is You" by Barry Louis Polisar, the song Reitman had chosen.  Shadowplay also designed the titlecards for each of the seasons for the film,  hand-made a custom typeface for the opening title sequence and the closing credits, and collaborated on the design of the soundtrack and the DVD. 

==Distribution==

===Theatrical release===
With a well-received preview first screened on September 1, 2007 at the Telluride Film Festival,   Juno premiered on September 8 at the 2007 Toronto International Film Festival and received a standing ovation, which prompted film critic Roger Ebert to say "I don’t know when Ive heard a standing ovation so long, loud and warm."  It went on to feature at the Austin Film Festival, Rome Film Festival, London Film Festival, Bahamas International Film Festival, St. Louis International Film Festival, Stockholm International Film Festival, International Thessaloniki Film Festival, Gijón International Film Festival, Palm Springs International Film Festival and the International Film Festival Rotterdam, earning awards and nominations at several.      
 Los Angeles and New York City.    It opened in an additional thirteen cities and around 25 theaters on December 14, expanding further on December 21 before entering wide release on December 25. 

===Promotion===
  online stores.     In the month after the films release, sales of the phone on eBay increased by 759 percent  and it was named one of the "10 Cool Gifts for Film Buffs" by Entertainment Weekly.   

===Home media===
The film was released on   Presents: Juno World Premiere" and "Fox Movie Channel Presents: Casting Session". 

==Reception==

===Box office performance===
In limited release and playing in only seven theaters in   nominees for the 80th Academy Awards. 

===Critical reaction===
The film benefited from an extremely positive critical reception; as of March 15, 2008 on the review aggregator Rotten Tomatoes, 94% of critics gave the film positive reviews, based on 205 reviews,  making it the best reviewed comedy film on the website in 2007.  On Metacritic, the film had an average score of 81 out of 100, based on 38 reviews.  Roger Ebert of the Chicago Sun-Times gave the film four stars and called it "just about the best movie of the year.   Has there been a better performance this year than Ellen Pages creation of Juno? I dont think so."  Ebert went on to place Juno at number one on his annual best of list. The film also ranks at number 463 in Empire (magazine)|Empire magazines 2008 list of The 500 Greatest Movies of All Time.  Juno MacGuff also ranked number 56 on Empires list of The 100 Greatest Movie Characters of All Time.  Paste Magazine named it one of the 50 Best Movies of the Decade (2000–2009), ranking it at number 15.  In June 2010, Entertainment Weekly named Juno one of the 100 Greatest Characters of the Last 20 Years. 
 New York magazine felt that the film was desperate to be "a movie that confers hipness on teens, that makes kids want to use the same slang and snap up the soundtrack".  Music reviewer Jim DeRogatis criticized the films stylized dialogue and what he saw as a casual take on abortion and Junos naïveté in becoming pregnant, claiming: "As an unapologetically old-school feminist, the father of a soon-to-be-teenage daughter, a reporter who regularly talks to actual teens as part of his beat and a plain old moviegoer, I hated, hated, hated this movie." 

===="The Juno Effect"====
In 2008, after 17 students under sixteen years of age at a Gloucester High School (Massachusetts)|Gloucester, Massachusetts high school became pregnant, Time (magazine)|Time magazine called it the "Juno Effect".  Time stated that some adults dismissed the statistic as an outlier while others accused films such as Juno and Knocked Up for glamorizing teenage pregnancy. Kristelle Miller, an Adolescent Psychology Professor at University of Minnesota-Duluth stated that " he  Juno effect is how media glamorizes pregnancy and how its also...&nbsp;pregnancy is also redemptive of any past problems". 

After Senator John McCain named Alaska Governor Sarah Palin as his running mate on the Republican presidential ticket, it was revealed in September 2008 that Gov. Palins daughter, Bristol, age 17, was pregnant with the child of another teenager. News reports and editorials termed Bristol Palins pregnancy as the latest episode in the debate over teen pregnancy of which Juno was a part,   while conservative commentators made comparisons between Bristol Palins pregnancy and the film.   Noted New Republic literary editor Leon Wieseltier, "The Republicans wanted a new conversation, and they got one. Juno in Juneau, Alaska|Juneau!"     Fox News Roger Friedman wondered, "Juno at once violated and vindicated conservative values. The question is, will the public rally ‘round Bristol Palin the way it did Juno? Or will it reject her for getting in this situation in the first place?"   

Juno actor Jason Bateman defended the film. "Unfortunately," he said, "we’ve had these instances where guys kill people because of what they hear in rock ‘n roll lyrics or some garbage like that. Look, if you’re going to blame a movie or song for your actions, whether they be good or bad, I think you’re looking at the wrong things to influence your life. I think people should look to other areas of their life for lessons and guidance, mainly parents, or teachers, or friends, or whomever. That should probably be where you should point your eyes and ears." 

Amy Benfer of Salon.com wrote in 2010 that, according to figures released by the Centers for Disease Control and Prevention, pregnancy rates for all teenagers dropped 2 percent between 2007 and 2008, meaning that "the slight uptick in teen pregnancy rates between 2005 and 2006 were probably just an anomaly and not some heinous trend brought about by pop culture", and that if there had been such a thing as a "Juno effect", it would have caused pregnancies to go down, not up. She criticized proponents of the theory, stating that they believed that teenagers "somehow lose all ability to evaluate any nuance or context in that womans particular situation, and instead make some sort of primitive cause-and-effect connection" and that "by talking about pregnant girls, and most of all, by daring to portray some of them as ordinary, even likable, wed get way more babies having babies." She concluded that "depicting teen parents may not glamorize them, so much as humanize them. You know, that thing that happens when one person recognizes that someone else is a person too? So, now that we can firmly state that realistically depicting the lives of the tiny percentage of girls who do become pregnant wont necessarily contaminate the rest of them, its time to stop worrying and ask what we can do to help."   

===Top ten lists===
The film appeared on critics top ten lists of the best films of 2007:
 
 
*1st&nbsp;– Roger Ebert, Chicago Sun-Times 
*1st&nbsp;– Paste (magazine)|Paste magazine staff 
  USCCB Office for Film and Broadcasting (tied with Bella (film)|Bella) 
*3rd&nbsp;– David Germain, Associated Press 
*3rd&nbsp;– Moviefone staff 
*4th&nbsp;– James Berardinelli, ReelViews 
*4th&nbsp;– Lou Lumenick, New York Post   
 
*4th&nbsp;– Richard Roeper, Chicago Sun-Times 
*6th&nbsp;– Claudia Puig, USA Today 
 
*6th&nbsp;– Desson Thomson, The Washington Post 
*6th&nbsp;– Joe Morgenstern, The Wall Street Journal 
*6th&nbsp;– Liam Lacey and Rick Groen, The Globe and Mail 
*6th&nbsp;– Marc Savlov, The Austin Chronicle 
*7th&nbsp;– Corina Chocano, Los Angeles Times 
*7th&nbsp;– Carrie Rickey, The Philadelphia Inquirer 
*10th&nbsp;– A. O. Scott, The New York Times (tied with Knocked Up and Superbad (film)|Superbad) 
*10th&nbsp;– Peter Travers, Rolling Stone (tied with Knocked Up) 
*10th&nbsp;– Stephen Holden, The New York Times 
 

===Awards=== Best Picture, Best Director, Best Actress for Ellen Page.   

Reitman expressed disappointment that Juno was ruled ineligible for the Genie Award nominations:
  Sara Morton, the head of the Academy of Canadian Cinema and Television, issued a statement explaining that the film had never been submitted for Genie Award consideration by its studio.    The Hollywood Reporter explained that Genie rules define Canadian films as financed at least in part by Canadian sources, and because American companies Mandate Pictures and Fox Searchlight were the sole funders, Juno was ineligible.  Nonetheless, Genie spokesman Chris McDowall said that while the film was not evaluated for eligibility since it was not submitted, "Financing is one of the criteria, but its not everything."  Despite this, the film was eligible for the 2008 Canadian Comedy Awards, receiving two wins from three nominations.      

====Wins====
 
 

* 80th Academy Awards  Best Original Screenplay&nbsp;– Diablo Cody

* BAFTAs    Best Original Screenplay

* 13th Critics Choice Awards|Critics Choice Awards   
**Best Comedy

* Canadian Comedy Awards (2008) 
**Best Actress&nbsp;– Ellen Page
**Best Director&nbsp;– Jason Reitman

* National Board of Review     
**Best Breakthrough Performance&nbsp;– Female (Ellen Page)
**Best Original Screenplay (Diablo Cody)

 
* Satellite Awards
**Best Actress&nbsp;– Musical or Comedy (Ellen Page)
**Best Film&nbsp;– Musical or Comedy

* Rome Film Festival 
**Best Film

* Writers Guild of America Awards Best Original Screenplay (Diablo Cody)

* Independent Spirit Awards 2007 
**Best Feature
**Best Female Lead&nbsp;– Ellen Page Best First Screenplay&nbsp;– Diablo Cody

*The National Movie Awards 2008
** Best Comedy
 

====Nominations====
* 80th Academy Awards  Best Picture No Country for Old Men) Best Director&nbsp;– Jason Reitman (Lost to Joel and Ethan Coen for No Country for Old Men) Best Actress&nbsp;– La Vie en Rose)

* 65th Golden Globe Awards   
**  ) Best Performance by an Actress in a Motion Picture&nbsp;– Musical or Comedy (Ellen Page) (Lost to Marion Cotillard) Best Screenplay&nbsp;– The Coen Brothers)

* 61st British Academy Film Awards  Best Leading Actress&nbsp;– Ellen Page (Lost to Marion Cotillard)

*2008 Canadian Comedy Awards
** Best Actor&nbsp;– Michael Cera; Cera received two nominations and did win the award, but for his work in Superbad.  

* Critics Choice Awards
**Best Actress&nbsp;– Ellen Page (Lost to Julie Christie)
** Best Acting Ensemble&nbsp;– Ellen Page, Michael Cera, J. K. Simmons, Olivia Thirlby, Allison Janney, Jennifer Garner, and Jason Bateman (Lost to Hairspray (2007 film)|Hairspray)

* 14th Screen Actors Guild Awards Outstanding Performance by a Female Actor in a Leading Role&nbsp;– Ellen Page (Lost to Julie Christie)

* Independent Spirit Awards 2007
**Best Director&nbsp;– Jason Reitman (Lost to Julian Schnabel)

* Online Film Critics Society Award
**Best Supporting Actress&nbsp;– Jennifer Garner (Lost to Amy Ryan)

==Soundtrack==
 
Juno  s  , 20th Century Foxs first number one soundtrack since the Titanic (1997 film)|Titanic Titanic (soundtrack)|soundtrack, and Rhinos first number one album, topping the American Billboard 200|Billboard 200 music charts in its fourth week of release. 
 Deluxe Edition, on November 25, 2008, containing both the original soundtrack as well as B-Sides in a two-disc set, along with storyboards from the film and additional liner notes from Reitman. 

Although uncredited not featured on the soundtrack, Page and Batemans characters perform an acoustic version of Hole (band)|Holes "Doll Parts" in the film.

=== Track listing ===
#"All I Want is You" - Barry Louis Polisar
#"My Rollercoaster" - Kimya Dawson
#"A Well Respected Man" - The Kinks
#"Dearest" - Buddy Holly
#"Up the Spout" - Mateo Messina
#"Tire Swing" - Kimya Dawson
#"Piazza, New York Catcher" - Belle and Sebastian
#"Loose Lips" - Kimya Dawson
#"Superstar" - Sonic Youth
#"Sleep" - Kimya Dawson
#"Expectations" - Belle and Sebastian
#"All the Young Dudes" - Mott the Hoople
#"So Nice So Smart" - Kimya Dawson
#"Sea of Love" - Cat Power
#"Tree Hugger" - Kimya Dawson and Antsy Pants
#"Im Sticking with You" - The Velvet Underground
#"Anyone Else But You" - The Moldy Peaches
#"Vampire" - Antsy Pants
#"Anyone Else But You" - Michael Cera and Ellen Page 

== See also ==
 

==References==
 

== External links ==
 
 
*  
*  
*  
*  

 
 
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 