Night of the Living Dead (1990 film)
 
{{Infobox film
| name                      = Night of the Living Dead
| image                     = Notld1990.jpg
| caption                   = Theatrical release poster
| director                  = Tom Savini
| producer                  = {{plainlist|
* John A. Russo
* Russell Streiner
}}
| screenplay                = George A. Romero
| based on                  =  
| starring                  = {{plainlist|
* Tony Todd
* Patricia Tallman
}} 
| music                     = Paul McCollough
| cinematography            = Frank Prinzi
| editing                   = Tom Dubensky
| studio                    = 21st Century Film Corporation
| distributor               = Columbia Pictures
| released                  =  
| runtime                   = 92 minutes
| country                   = United States
| language                  = English
| budget                    = $4.2 million   
| gross                     = $5.8 million 
}} George A. of the same name. Romero rewrote the original 1968 screenplay co-authored by John A. Russo.    

==Plot==
The film begins as siblings Barbara and Johnnie visit their mothers grave in a remote, rural cemetery. During their visit, Barbara is attacked by a zombie. Her brother comes to her defense but is killed in the struggle. Barbara flees the cemetery and discovers what at first seems to be an abandoned farmhouse. She seeks shelter there, only to find a mutilated corpse and another pack of zombies. She is joined there shortly after by Ben, and the two clear the house of the dead and begin the process of barricading the doors and windows.

They discover that other survivors are already hiding in the cellar of the house: Harry Cooper, his wife Helen, their daughter Sarah who was bitten on the arm by a zombie and has fallen seriously ill; teenager Tom Bitner and his girlfriend Judy Rose Larson. What follows are the attempts by the survivors to defend the house under siege from the undead. The group is left divided over what their next course of action should be. Harry believes everyone should retreat to the cellar and barricade the door to wait for the authorities. Ben thinks the cellar is a "death trap" and that they would be better served fortifying the house, which at least has alternate escape routes, and Barbara suggests that they should all leave the house on foot while they still can after she notices that the zombies are very slow and that they can just "walk right past them". An argument between Ben and Harry leaves the Coopers in the basement to tend to their ailing daughter and the remaining members of the group upstairs to continue their work reinforcing the doors and windows. The loud sound of hammers hitting the nails into the wood attracts more zombies to the house.

A plan is ultimately devised to escape using Bens truck, which is out of fuel. There is a gas pump on the property, but it is locked. A search of a corpse on the property produces a set of keys. Judy Rose, Tom, and Ben proceed up the hill toward the gas pump but their plan begins to unravel when Ben falls from the back of the truck and is left to defend himself against the undead. After discovering the key to the gas pump is not among the bunch they brought with them, Tom attempts to shoot the lock off. The falling gasoline is ignited, trailing after them in the truck. The resulting explosion kills both Tom and Judy.

Ben returns to the house to find things beginning to dissolve into chaos. Harry has wrestled Barbaras gun away from her and is now armed. Unknown to the survivors upstairs, the Coopers daughter Sarah has died from the bite on her arm and has become a zombie; she attacks and bites her distraught mother, who does not defend herself. When Sarah makes her way upstairs she triggers a shootout between her father, who is trying to protect her, and Ben and Barbara, who are trying to protect themselves. Both Ben and Harry are badly wounded, and Sarah is shot by Barbara. Harry retreats upstairs to the attic, while Ben makes his way to the cellar, where he shoots a reanimated Helen. Barbara leaves the house, now being overrun by the undead, to attempt to find help.

Barbara eventually joins a posse of locals who are attempting to clear the area of the undead. She returns with them to the farmhouse the next day to find Ben has died of his wounds and reanimated. He is shot in the head by one of the posse members. When Harry emerges from the attic alive, Barbara kills him in a fit of rage and turns to leave the house, telling the vigilantes they have "another one for the fire". The film ends as Barbara watches the bodies being burned in the fire.

==Cast==
* Tony Todd as Ben
* Patricia Tallman as Barbara
* Tom Towles as Harry Cooper
* McKee Anderson as Helen Cooper
* Heather Mazur as Sarah Cooper William Butler as Tom
* Katie Finneran as Judy Rose
* Bill Moseley as Johnnie

==Production==
Romero said that the remake came about in part because of issues over profits of the original film.  A lengthy court battle over the rights to the film, plus an oversight that caused the copyright notice not to be included, caused Romero to see little in the way of profit.  Romeros production company, Image 10, eventually won the lawsuit, but the distributor went out of business before they could collect any money.  Another issue was the fact that the filmmakers were worried that someone else might make an unauthorized remake.  Romero contacted Menahem Golan when he heard that 21st Century Film Corporation was interested in a remake, and Romero, Russo, and Steiner collaborated for the first time in twenty years.   Savini was initially hired to perform the special effects but was persuaded to direct by Romero.   Savini was drawn to the remake because he was unavailable to do special effects on the original.   

The special effects team intentionally kept the effects restrained, as they felt that excessive gore would be disrespectful to the original film.  In order to keep the effects realistic, they used as inspiration a real autopsy, forensic pathology textbooks, and Nazi death camp footage.  Savini said that he wanted to keep the film artistic despite his reputation as "the king of splatter".   The zombie extras were recruited easily, as the films reputation drew them from as far away as Kentucky. 

The production was not easy for Savini, who described it as "the worst nightmare of my life".  Savini said that only 40% of his ideas made it into the final film.  Without Romero on set, he clashed with the producers, who did not allow him to explore his vision for the film. 

==Release==
To avoid an NC-17 rating, Savini had to cut several scenes from the film, which Savini attributed to the films lack of popularity among horror fans.  Savini sometimes shows the entirety of the cut scenes at conventions.  A Blu-ray version was released in a limited edition of 3,000 in October 2012 by Twilight Time. 

==Reception== Kevin Thomas of the Los Angeles Times wrote, "While this "Night" hasnt the chilling, almost  cinema-verite  credibility of the original, it is certainly a well-sustained entertainment".   Richard Harrington of The Washington Post criticized the film as a purely financial effort that lacks the shock of the original film now that zombie film tropes have become cliched.   Dave Kehr of the Chicago Tribune rated it three out of four stars and wrote that although Savinis direction is a bit too literal, the film "contains some intriguing further development of the ideas of the first film, as well as some mistakes corrected and dramatic relationships tightened." 

More recent criticism has been more appreciative.   As of 2015, Rotten Tomatoes, a review aggregator, reports that 68% of 31 surveyed critics gave the film a positive review; the average rating is 6.3/10.   Bloody Disgusting rated it four-and-a-half out of five stars and wrote, "This film works on so many levels. Normally re-makes are horrible, and diverse so much from the original film. This one is so close to the original its scary."   Reviewing the Twilight Time Blu-ray, Adam Tyner of DVD Talk rated it 3.5/5 stars and wrote, "Well never get a chance to see the remake that Tom Savini set out to direct. Still, despite the many missteps of this severely compromised version, Night of the Living Dead manages to distinguish itself as one of the more effective horror remakes out there."   Reviewing the same disc, Patrick Naugle rated it 83 out of 100 and called it "one of the superior zombies movies available".  

==References==
 

*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 