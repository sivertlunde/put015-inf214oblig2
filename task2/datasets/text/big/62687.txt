All This, and Heaven Too
 
{{Infobox film
| name           = All This, and Heaven Too
| image          = All_this_heaven_movieposter.jpg
| caption        = Theatrical release poster
| director       = Anatole Litvak David Lewis Anatole Litvak
| screenplay     = Casey Robinson
| based on       =  
| starring       = Bette Davis Charles Boyer Barbara ONeil
| music          = Max Steiner Ernie Haller
| editing        = Warren Low
| distributor    = Warner Bros.
| released       =  
| runtime        = 141 minutes
| country        = United States
| language       = English
}}
 Ernie Haller.
 Harry Davenport, George Coulouris, Montagu Love, Janet Beecher and June Lockhart.

Rachel Fields novel is based on the true story of Fields great-aunt, Henriette Deluzy Desportes, a French governess who fell in love with the Duc de Praslin, her employer. When Praslins wife, the Duchesse, was murdered, Henriette was implicated. It was a real-life scandal that brought down Frances King Louis-Philippe in 1847.  

==Plot==
  and Bette Davis from the films original trailer]]

Mademoiselle Henriette Deluzy-Desportes (Bette Davis), a French woman, starts teaching at an American girls school. She is confronted by the tales and gossip about her that circulate among her pupils and, thus provoked, she decides to tell them her life story. 
 Duc de Praslin (Charles Boyer) and the Duchesse de Praslin (Barbara ONeil) in Paris during the last years of the Orleans monarchy. As a result of the Duchesses constantly erratic and temperamental behavior, all that remains is an unhappy marriage, but the Duc remains with his wife for sake of their children.

Through her warmth and kindness the governess wins the love and affection of the children and their father, but also the jealousy and hatred of their mother. She is forced to leave and the Duchess refuses to give her a letter of recommendation to future employers. The Duc confronts his wife and she invents alternate letters taking opposite attitudes, which in fact she has not written and does not intend to write. Her account enrages him and leads to her murder.
 Harry Davenport), a kindly old man who had early warned the governess to escape the de Praslin household. She is released by the authorities. 

Henriettes French class is moved by her account. She had been recommended for the teaching position "in the land of the free" by an American minister, Henry Field (Jeffrey Lynn), to whom she had expressed a loss of faith while in prison. He proposes marriage and Henriette accepts.

==Cast==
* Bette Davis as Mlle. Henriette Deluzy-Desportes Charles Laure Hugues Théobald, Duc de Choiseul-Praslin
* Barbara ONeil as Frances Altarice Rosalba Sébastiani, Duchesse de Choiseul-Praslin
* June Lockhart as Isabelle de Choiseul-Praslin
* Virginia Weidler as Louise de Choiseul-Praslin
* Ann E. Todd as Berthe de Choiseul-Praslin
* Richard Nichols as Reynald de Choiseul-Praslin
* Jeffrey Lynn as Reverend Henry Martyn Field
* Sybil Harris as Mlle. Hortense Maillard, the Duchesss attendant
* George Coulouris as Charpentier, the valet Harry Davenport as Pierre, the grounds keeper Horace François Bastien Sébastiani de la Porta
* Helen Westley as Mme. LeMaire
* Henry Daniell as Broussais
* Walter Hampden as Pasquier
* Ann Gillis as Emily Schuyler
* Marilyn Knowlden as Marianna Van Horn

==Awards and nominations== Best Supporting Best Cinematography.

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 