Pain & Gain
 
{{Infobox film
| name           = Pain & Gain
| image          = Pain & Gain film poster.jpg
| alt            = Three musclebound men standing in front of a large American Flag
| caption        = Theatrical release poster
| director       = Michael Bay
| producer       = {{Plain list |
* Donald De Line
* Michael Bay
* Ian Bryce
}} Christopher Markus Stephen McFeely
| based on       =  
| starring       = {{Plain list | 
* Mark Wahlberg
* Dwayne Johnson
* Anthony Mackie
* Tony Shalhoub
* Ed Harris
}}
| music          = Steve Jablonsky
| cinematography = Ben Seresin
| editing        = Joel Negron Thomas A. Muldoon
| studio         = De Line Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 129 minutes  
| country        = United States
| language       = English
| budget         = $26 million   
| gross          = $96.2 million 
}} included a number of bodybuilders affiliated with Sun Gym.   The films title is a play on the common exercising adage of "No pain, no gain".

The film was released on April 11, 2013, to mixed reviews. Critics praised the acting, comedic chemistry, and script while criticizing the violence, directing, and historical inaccuracies. Against a $26 million budget, the film grossed over $96 million worldwide.

==Plot== Daniel Lugo (Mark Wahlberg) is a recently released convict who served time for Medicare fraud. Sun Gym owner John Mese (Rob Corddry) hires him to increase membership and make the gym more fitness-based. Lugo triples the gyms membership within six weeks and befriends trainer Adrian Doorbal (Anthony Mackie). Doorbal, a bodybuilder, uses steroids, which render him Impotency|impotent. Lugo soon lusts after the earnings and lifestyle achieved by Victor Kershaw (Tony Shalhoub), a sleazy, new gym member he begins to train who Lugo believes is a crook. Inspired by motivational speaker Johnny Wu (Ken Jeong), Lugo decides to be a "doer" and hatches a plan to extort Kershaw for his assets by kidnapping and torturing him.

Lugo recruits accomplices Doorbal and Paul Doyle (  documents in Kershaws absence by presenting documents signed by Kershaw and using Kershaws money to sponsor the Sun Gym.

The Sun Gym gang is able to collect Kershaws money and assets, but they realize releasing him is a bad idea. Therefore, Lugo concocts a plan to kill Kershaw by forcing him to drink liquor and crash his BMW, making it appear like a drunk driving accident. When Kershaw survives the crash, the gang burns the car with Kershaw in it. Kershaw escapes the blazing vehicle, so the gang runs over his body and leaves him for dead. Unbeknownst to them, Kershaw survives and is hospitalized. The Sun Gym gang members spoil themselves with Kershaws riches. Lugo takes over Kershaws car and his home in a ritzy Miami suburb; Doorbal marries his nurse, Robin (Rebel Wilson), and uses his cut to purchase penile erection treatments; and Doyle abandons his restraints of religion and sobriety and blows away his money on cocaine and his new stripper girlfriend.

Kershaw reports what happened to the police, but they are turned off by his unpleasant manner and do not believe his bizarre story even when he gives them Daniel Lugos name, particularly because of Kershaws blood alcohol level. He then contacts Ed Du Bois, III (Ed Harris), a retired private investigator, who declines to take the case but warns Kershaw to quickly leave the hospital before the gang returns to kill him. Kershaw takes his advice and hides in a cheap motel. Upon reflection, Du Bois takes Kershaws case and tails the Sun Gym gang. He visits the Sun Gym and meets with Lugo, who becomes suspicious after Du Bois mentions Kershaws name.
 dye packs planted in the money bag explode, and he narrowly escapes the police, getting his toe shot off in the process. He and Doorbal (who depleted his share on payments for treatments, his and Robins wedding, and a new home) explain to Lugo they need more money, and they propose another kidnapping.

They target the wealthy Frank Griga (Michael Rispoli), who owns a phone sex operation. After a promising discussion at Grigas mansion, the gang invites Griga and his wife Krisztina Furton (Keili Lefkowitz) to Doorbals home to propose an investment scheme. Griga insists on meeting with someone more experienced and questions Lugos amateurish business savvy. This angers Lugo, who attacks Griga and accidentally kills him. Krisztina discovers this and tries to shoot Lugo, but Doorbal injects her with a potent horse tranquilizer. Lugo and Doyle try to use codes obtained from a heavily sedated Krisztina to open a safe at her and Grigas home, but the codes do not work. When Krisztina rouses and tries to escape, Doorbal gives her a second injection, accidentally causing her to fatally overdose.

Lugo and Doorbal purchase equipment to dismember the bodies and dump the body parts in oil drums, which they sink in a secluded lake outside Miami, while Doyle incinerates their hands (on a barbecue grill) to eliminate their fingerprints. Doyle, realizing what hes become over the violence and gore, leaves the gang and returns to the priests church. The police learn of Griga and Krisztinas disappearances, and with evidence from Ed Du Bois, they set a plan to arrest the Sun Gym gang. The film returns to June 17: The police arrest Doyle at the church, Doorbal at home, and Mese at the Sun Gym. Lugo, also at the gym, spots the approaching police force and flees. Although struck by a police cruiser, he escapes and heads out by sea in Kershaws speedboat. Kershaw and Du Bois deduce Lugo is going after Kershaws hidden bank account in Nassau, Bahamas|Nassau, Bahamas and accompany the police to capture him. Lugos pursuers catch up with him and Lugo attempts to escape. Du Bois shoots him, Kershaw chases Lugo in a car and incapacitates him, and the authorities arrest him. Lugo is brought back to the United States and stands trial with Doyle, Doorbal, and Mese. At the trial, Doyle rolls over on Doorbal and Lugo with a full confession while Robin divorced Doorbal the night before she testifies against him. Ultimately, the four are convicted.

* Daniel Lugo was sentenced to death, plus 30 days for "being an asshole" to a guard.
* Adrian Doorbal was sentenced to death.
* Paul Doyle, for his full confession, was sentenced to 15 years, serves 7 years, and goes free and tries to apologize and converts back to Christianity.
* John Mese was sentenced to 15 years and died in prison.
* Victor Kershaws name was changed to protect the survivor.
* Sorina Luminitas name was changed to protect the survivor. Shes currently not a movie star.

The movie ends with Lugos saying: "Thats the American dream".

==Cast==
  Daniel Lugo
* Dwayne Johnson as Paul Doyle (based on Carl Weekes, Stevenson Pierre, and Jorge Delgado)
* Anthony Mackie as Adrian "Noel" Doorbal
* Tony Shalhoub as Victor Kershaw 
* Ed Harris as Det. Ed Du Bois, III
* Rob Corddry as John Mese
* Rebel Wilson as Robin Peck 
* Ken Jeong as Johnny Wu 
* Bar Paly as Sorina Luminita 
* Michael Rispoli as Frank Griga
* Tony Plana as Captain Lopez
* Emily Rutherfurd as Cissy DuBois
* Yolanthe Sneijder-Cabau as Analee Calvera
* Larry Hankin as Pastor Randy
* Peter Stormare as Dr. Bjornson
* Brian Stepanek as Brad McCallister
* Kurt Angle as Benjamin Rowe (Prison inmate)
 

==Production==
  (2009). Bay stated he wanted to do Pain & Gain between the second and third  , a 2011 release date. 

On February 13, 2012, it was confirmed that the budget for the film would be funded by   had officially joined the cast and Rob Corddry was rumored to play John Mese, a former competitive bodybuilder who now owns the gym where Wahlbergs character works as a personal trainer.  On February 23, 2012, it was confirmed that Anthony Mackie had joined the cast as "a bodybuilder and workout partner of Wahlbergs character Adrian Doorbal, who has little to show for his time in the gym and decides to get involved with the twisted plan." 

On February 28, 2012, it was reported that Israeli model Bar Paly and Lebanese-American actor Tony Shalhoub had joined the cast. Paly is cast as "an illegal immigrant and former beauty queen who dreams of becoming the next Marilyn Monroe. Wahlbergs character promises to make her a star, and she in turn agrees to do whatever he asks in the service of her new country."  Shalhoubs role in the film is that of "Marc Schiller, the target of the kidnapping scheme."  On March 5, 2012, it was reported that Scott Rosenberg was brought on board to punch up the script. Rosenberg had worked with Bay previously on Armageddon (1998 film)|Armageddon (1998). 

In a statement on March 7, 2012, Bay reported the budget was $22 million and said he was taking Screen Actors Guild#Standardized pay and work conditions|directors scale for the film. 

On March 27, 2012, Rebel Wilson joined the cast as Robin Peck.  and principal photography began in Miami on March 31, 2012. On April 4, 2012, Dutch actress Yolanthe Sneijder-Cabau joined the cast as Wahlbergs characters object of desire.   On April 5, 2012, Ken Jeong joined the cast as a character named Johnny Wu.  The official trailer for the film was released on December 19, 2012. 
 computer monitors can be seen, which did not exist at the time. Also, during the police raid which takes place on June 17, 1995, the police vehicles are marked "Miami-Dade Police". Miami-Dade County did not exist under that name until 1997. The actual events took place in Dade County and were investigated by Metro Dade Police. The film further makes the error of using the Ford Crown Victoria Police Interceptor, model year 1998, as well as police raid vehicles that did not exist in 1995.

==Reception==
Pain & Gain received mixed reviews from critics. Rotten Tomatoes gives the film a 50% rating, based on 181 reviews, with an average rating of 5.4/10. The sites consensus reads, "It may be his most thought-provoking film to date, but Michael Bays Pain & Gain ultimately loses its satirical edge in a stylized flurry of violent spectacles."  Metacritic gave the film a score of 45 put of 100, based on 42 critics, indicating "mixed or average reviews". 

British critic Mark Kermode described the film as "grotesquely inappropriate" and "every bit as pumped up and steroidal as the appalling  characters it is attempting to portray".  {{cite video | url = http://www.youtube.com/watch?v=GcHvRhq36hw | title = 
Mark Kermode reviews Pain & Gain | work = BBC 5 Live | date = August 30, 2013}} 

Philip French writing for The Observer said of the violent black comedy that he "rather enjoyed it" with particular praise for Ed Harris. 

 

  

==Real-life outcomes==
 
Francisco Alvarados Miami New Times article, "Pain & Gain: Where the Real-Life Sun Gym Gang Characters Are Now" (April 4, 2013), details the actual crimes, the real-life gang members sentences, and the characters post-trial experiences and current status. 

Additionally, the Florida State Commission on Capital Cases publishes and regularly updates the trial summaries, court information, and information about the offenses, criminal sentences, and post-trial legal and prison developments for defendants. This information is cross-posted with that of the Florida Department of Corrections, Prison Offender Network, which posts regularly updated Inmate Population Information Detail and the Death Row Roster.    Details from those sources pertinent to the Sun Gym gang are summarized below:
* Noel Doorbal (DC# M16320, DOB: December 21, 1971, Eleventh Judicial Circuit, Dade County Case # 95-17381-B);    death sentence received August 31, 1998     
 
* Daniel Lugo (DC #M16321, DOB: June 4, 1963, Eleventh Judicial Circuit, Dade County, Case# 95-17381C);  death sentence received August 31, 1998  

* John Mese (DC# M15699): "Mese was indicted on October 2, 1996 (Case# 95-17381-F) for the kidnapping, extortion, and murders of Griga and Furton and the kidnapping and extortion of Marc Schiller.   A jury returned guilty verdicts on all counts of the indictment; however, the judge set aside the convictions pertaining to the Griga/Furton crimes.  On July 20, 1998, Mese was sentenced to 56 years imprisonment for the kidnapping and extortion of Schiller.  
 
: "Mese appealed and the State cross-appealed the sentence to the Florida District Court of Appeal, Third District.  On 06/19/02, the DCA ruled that the trial judge improperly set aside the two RICO convictions and ordered a new sentencing hearing to be conducted on those counts. 
 
: "On January 15, 2003, Mese was sentenced to 30 years imprisonment for one count of Conspiracy to Commit Racketeering."  
 nolle declined to prosecute all but the kidnapping charge, and Raimondo was convicted and sentenced to eight years imprisonment for the crime."  

* Jorge Delgado (DC# 198219): "In return for testifying for the State, Delgado was sentenced to two prison terms of 15 and 5 years for his role in the murders of Griga and Furton and the attempted murder of Schiller."  
 charged with a fraudulent Medicare scheme that generated $14 million. It’s stunning and a little bit heartbreaking. As Collins writes,  ll he could think was that   had sold him out. For three years she had used him, forced him to relive every excruciating detail of his confinement: the starvation, the burns and electric shocks, the beatings, the abject terror, the absolute physical and psychological mortification. She had extracted everything she could, and then she had disposed of him."   

==Comparisons between the film and actual events==

Multiple media properties compare and contrast details shown in the film and actual events. As David Haglund and Forrest Wickman wrote in Slate (magazine)|Slates culture blog, "Browbeat": "the film more or less adheres to a very rough outline provided by the novella-length, three-part, highly detailed series written by Pete Collins and published in the Miami New Times over a decade ago. Not surprisingly, many details, and a number of significant characters, are dropped from the movie. A lot of new, fictional detail – and one largely made-up character – takes its place. When the movie first tells us that its a true story, were seeing something that didnt happen. When were told its still a true story, were watching one invented character watch a semi-fictional character do something that sorta kinda took place." 

The film portrayed the Sun Gym gang as consisting of three primary members (Daniel Lugo, Adrian Noel Doorbal, and Paul Doyle) and two accomplices  . In reality, the gang was larger; Doyles character is a composite of several real life individuals who were not depicted in the film, such as Carl Weekes, Jorge Delgado and Stevenson Pierre; and Doorbals real-life girlfriend (Cindy Eldridge) helped scrub blood off Doorbals condominium walls after Doorbal had dismembered Griga and Furtons bodies. Additionally, unlike Sorina, who in the film Lugo passes on to Doyle, Sabina and Lugo remained together as a couple and became engaged, and they fled together to the Bahamas (with Lugos parents).     Additional real-life players in the events are detailed in the Miami New Times article, "Sidebar: Cast of Characters". 

In the film, Victor Kershaw states he was born in Bogotá, Colombia. His real life counterpart, Marc Schiller, was born in Argentina. 

Schiller and Lugo did not befriend each other; Schiller actually distrusted Lugo. It was Delgado (who worked for Schiller, as did Delgados wife), who befriended Lugo and targeted Schiller, and it was at Delgados (not Schillers) warehouse where the kidnappers held and tortured Schiller for a full month, while extorting him and before trying to kill him. 

Sabina Elena Petrescu (portrayed onscreen by Bar Paly as Sorina Luminita), was in fact a Solid Gold center-stage stripper. She was a one-time Miss Romania and also a former Penthouse (magazine)|Penthouse model. 

In the film, a blindfolded Victor Kershaw recognized Lugo by his cologne. In reality, Marc Schiller recognized his voice.   

The car with which the gang tried to kill Schiller, by crashing it (into a construction vehicle, in the film; into a utility pole, in reality) and then by setting Schiller and it ablaze, was a Toyota 4Runner, not a BMW. In contrast to the film, the gang did not secure Schillers seatbelt before crashing the car, and Schiller did not survive the crash from inside the car; rather, Schiller  bailed out of the car, rolling onto the ground, before it hit the pole. When crashing the car and setting Schiller ablaze failed, the gang ran over Schillers body twice, but with a Toyota Camry, not a van.    

The movie portrays "Paul Doyle" as first running into a demeaning Frank Griga at a strip club. In reality, Griga was first discovered by Doorbal when he spotted a picture of a Lamborghini Diablo in a photo album belonging to his Hungarian stripper girlfriend, Beatriz Weiland. He asked her who owned it. It turned out that Griga was one of Weilands former generous boyfriends. It was she who introduced Griga to the Gang.

Frank Griga was Hungarian and therefore lacked an American accent

The gang did meet at Frank Grigas home as in the movie (and three times), then met the last time at Doorbals Miami Lakes apartment, where the murders actually took place.

In reality, Lugo did not kill Griga—Doorbal killed him, by first cracking the side of his head with a blunt object, then strangling him with a headlock, and finally injecting him with Xylazine|Rompun. Krisztina Furton ran to see what had happened, and screamed. Lugo covered her mouth and tackled her. She had no gun, contrary to how the movie portrayed. She was bound, then Doorbal injected her with the drug. Overall, Doorbal injected her three separate times, instead of twice.
 Home Depot to cut off body parts but forgot to fill it with motor oil, so it broke the first time they cranked the power tool on. Lugo returned the chainsaw to Home Depot, demanding a refund. He left the home-improvement store with an electric Remington Power Cutter, which came with a one-year guarantee to handle all your cutting chores quickly and easily. He went back to the warehouse and handed the chainsaw to Doorbal, who took charge of the grisly dismemberment. When the power tools teeth got caught in Furtons hair, Doorbal had Lugo chop off her head with a hatchet. The two murderers then used a curved blade and pliers to remove the faces and teeth off the heads."  

The movie depicted Lugo and Doorbal dumping the body parts in several barrels into a lake located somewhere in what appears to be the Everglades. In reality, Lugo, Doorbal, and "Little Mario" Gray dumped Griga and Furtons torsos-in-drums into a drainage ditch in southwest Miami.

Details in the scene in which Paul was shown incinerating the victims severed hands on a barbecue grill (to remove the fingerprints) were changed; in reality, Daniel Lugo did the grilling, and Lugo used a steel drum with an iron grate laid on top, not a barbecue grill. Lugo tossed Frank Griga and Krisztina Furtons hands, feet, and skull fragments onto the grate, doused them in gasoline, and began to grill. When Jorge Delgado returned to the warehouse, he yelled at Lugo, who reluctantly agreed to move his operation from in front of the warehouse to the rear alley.  

In the film, Paul Doyle robbed an armored truck and got his toe shot off while escaping. That sequence is entirely fictional; no member of the Sun Gym gang actually robbed an armored truck or had their toe shot off. 

Robin Peck (Rebel Wilsons character), Doorbals future wife in the film, is based on Cindy Eldridge, who did refer Doorbal to a doctor. Contrary to events in the film, they did not meet at a medical office, they did not have a whirlwind courtship or marry at home, and Doorbal did not need to commit further crimes to fund his injections. Furthermore, Doorbal was violent and sadistic in real life, unlike Anthony Mackies mild-mannered character in the movie. 

===Arrests===
In the movie, the police arrest:
* Mese at the Sun Gym—In reality, Mese was arrested at his own bodybuilding competition in Downtown Miami.
* Doyle at the church—Doyles real composite counterparts were all arrested at home.

In the film, Daniel Lugo escapes in Victor Kershaws (real: Marc Schiller) go-fast boat. In reality, Schiller did not own a boat. Only Frank Griga owned a boat, and it was a tall yacht named Foreplay.

Near the end of the movie, Daniel Lugo is seen getting hit by a car driven by Victor Kershaw, in the Bahamas. This event did not happen. In reality, Lugo fled to the Bahamas with his fiancee and his parents, and neither Marc Schiller nor detective Ed Du Bois were there during his capture. Instead, a multi-agency task force apprehended Lugo at the Hotel Montague in Nassau, Bahamas|Nassau. 

At the end of the movie, "Paul Doyle" has an attack of conscience, confesses, and testifies against Lugo. In reality, Jorge Delgado had an attack of self-preservation, testifying against everyone to receive a more lenient sentence. Instead of the death penalty, he got 15 years, but only served 7½ years. Carl Weekes, the religious and recurring drug-abuser part of Doyles composite, drove the car that ran over Schiller, and got 10 years for attempted murder. He served 7 years.

In the film, Ed DuBois is portrayed as a retired police officer who takes over his "old mans detective agency" when he accepts Kershaws case. Ed Du Bois has been a licensed private investigator since 1960 and took over his fathers agency in 1968. Du Bois continues in this capacity to this day. 

Slash Film writer David Chen notes multiple major differences between the film and the real-life story. One relates to the real-life gang member whose temperament is most like that of the character played by Dwayne Johnson: 
:In reality, the third man in the Sun Gym Gang was a man named Carl Weekes, who most closely resembles the Paul Doyle character in the film — both are trying to make a new life in Miami, and both are born again Christians. But Weekes is a weakling; Collins describes him as "a lightweight" who weighed only 140. Moreover, he’s almost totally excluded from the later events in the story, in which sex mogul Frank Griga is killed.    

Chen also noted that all references to the homosexual relationships between the real life counterparts were deliberately omitted by the filmmakers in order to increase the movies mainstream box office appeal.    

==Controversy==
The Associated Press published an article interviewing survivors and investigators of the Sun Gym gang. Miami-Dade Police Sgt. Felix Jimenez stated: "You are talking about real people. And in this particular case, especially when youre talking about the murder victims, these were innocent victims." Zsuzsanna Griga, whose brother and his girlfriend the gang killed and dismembered, said she didnt want the American public to sympathize with the killers. 

David Haglund and Forrest Wickman of Slate (magazine)|Slate wrote, in a post titled "How True is Pain & Gain?":  "In addition to the usual Hollywood streamlining and the amping up of certain scenes, the changes seem largely designed to make the central criminals more sympathetic.  Whether you think thats a respectable thing to do will depend on what you think of their actual story – and perhaps, of the movies in general." 

==In popular culture==
* Writer Pete Collins parlayed his three-part Miami New Times article series, that inspired the film, into a consultancy for films screenwriting team, {{cite news|journal=Miami New Times|url= http://www.miaminewtimes.com/2013-04-04/news/pain-and-gain-where-the-real-life-sun-gym-gang-characters-are-now/full/ |title= Pain & Gain: Where the Real-Life Sun Gym Gang Characters Are Now
|author= Francisco Alvarado |date= April 4, 2013}}  a book titled Pain & Gain – This Is a True Story (April 5, 2013 from Premier Digital Publishing, Inc.),  multiple post-film articles (such as those in Miami New Times),  and media appearances.

* Private Investigator Ed Du Bois, III (portrayed by Ed Harris in the film) released a single on iTunes titled " " which he wrote and recorded in 2012 as an homage to his client, Marc Schiller.
 Marc Schillers Medicare fraud trial,  is a former local policeman and attorney who stars in the arbitration-based reality court television show, Judge Alex. 

* Marc Schiller (portrayed in the film as Victor Kershaw) has published multiple books about the ordeal and its aftermath. 
** He published his account of the events in Pain and Gain – The Untold True Story (January 2013). 
** He discussed his ensuing experiences and lessons learned since the first memoirs publication in Pain and Gain – How I Survived and Triumphed (March 2013). 

==Home media== UltraViolet in the United States, on August 27, 2013. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*   

The original Miami New Times articles by Pete Collins
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 