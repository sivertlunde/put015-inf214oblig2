The Spikes Gang
{{Infobox film
| name           = The Spikes Gang
| image          = The Spikes Gang poster.jpg
| caption        = Theatrical release poster by Tom Jung
| director       = Richard Fleischer
| producer       = Richard Fleischer Walter Mirisch
| writer         = Giles Tippette (novel)
| based on       = The Bank Robber
| screenplay     = Irving Ravetch Harriet Frank, Jr.
| starring       = Lee Marvin Gary Grimes Charles Martin Smith Ron Howard Arthur Hunnicutt Noah Beery, Jr.
| music          = Fred Karlin Brian West
| editing        = Frank J. Urioste Ralph E. Winters
| distributor    = United Artists Optimum Releasing
| released       =  
| country        = United States
| runtime        = 96 min.
| language       = English
| budget         =
}}

The Spikes Gang is a 1974 Mirisch Company motion picture adaptation of the Giles Tippette novel The Bank Robber. It was directed by Richard Fleischer and starred Lee Marvin, Gary Grimes, Charles Martin Smith and Ron Howard.

==Overview==
 , Charles Martin Smith, Lee Marvin and Ron Howard star in The Spikes Gang.]] scene stealing" performances.    

It was filmed in Tabernas, Almeria, and Andalucia, in Spain.

The production style of director Richard Fleischer received generally favorable reviews.    "It was only the second matchup for the director and Marvin and the actor gives a seething, unpredictable performance as the untrustworthy Harry Spikes. You can see why a trio of farmhands, well-played by his co-stars, would want to emulate him; you can also see why they come to resist the lethal charm of his coercion.", wrote one critic. 

Ron Howard later praised producer Walter Mirisch saying, "When I...acted in one of his productions, The Spikes Gang, I learned that a prolific and brilliant producer could also be a terrific guy and a wonderful teacher." {{cite web|url=http://uwpress.wisc.edu/Presskits/Mirisch.html|title=I Thought We Were Making Movies, Not History 
|last=Mirisch|first=Walter|publisher=UW Press|accessdate=2009-10-25}} 

==Synopsis==
Harry Spikes (Lee Marvin) is an aging bank robber of the fading "Old West." Injured and near death, he is found and mended back to health by three impressionable youths who are lifelong friends -- Wil (Gary Grimes), Tod (Charles Martin Smith), and Les (Ron Howard). Later, encouraged by Spikess reminiscences, they run away from home seeking excitement and easy living.

Reunited with Spikes on several occasions, the boys follow his tutoring, ultimately resulting in the gravest of consequences.

==Cast==
* Lee Marvin as Harry Spikes
* Gary Grimes as Wilson Young
* Charles Martin Smith as Tod Hayhew
* Ron Howard as Les Richter
* Arthur Hunnicutt as Kid White (aka Billy Blanco)
* Noah Beery, Jr. as Jack Basset Marc Smith as Abel Young
* Don Fellows as Cowboy
* Susan Coyne as Clara Cratchitt
* Elliott Sullivan as Billy

==Reception==
  and Soylent Green. The entire enterprise is as convincing as the Spanish landscapes, which are meant to suggest the American Southwest but dont." 

Keith Bailey of The Unknown Movies said, "Although the movie was filmed in Spain, you wouldnt know it, since Fleischer shot the outdoor scenes in remarkably drab locations that all look the same. And there is a breakdown in the natural flow of the story in the last twenty minutes, becoming more like a series of vignettes with little tying them together. Its therefore surprising the few times Fleischer breaks out of his mediocrity and puts in some effort."  

==Home media==
The film is available in Region 1 manufactured on demand DVD-R format, MGM on Demand via the MGM Limited Edition Collection label; also available in Region 2. The VHS version is long out of print.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 