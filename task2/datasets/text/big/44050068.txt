Dark Hazard
{{Infobox film
| name           = Dark Hazard
| image          =
| caption        =
| director       = Alfred E. Green
| producer       = First National Pictures Hal B. Wallis Jack L. Warner
| writer         = Ralph Block Brown Holmes
| based on       =  
| starring       = Edward G. Robinson
| music          = Leo F. Forbstein
| cinematography = Sol Polito
| editing        = Herbert Levy
| distributor    = First National(thru Warner Brothers)
| released       =  
| runtime        = 70 minutes; 8 reels
| country        = United States
| language       = English
}}

Dark Hazard is 1934 film starring  . It was produced by First National Pictures and released through Warner Brothers. 

A copy is held at the Library of Congress. 

==Cast==
* Edward G. Robinson as Jim "Buck" Turner
* Genevieve Tobin as Marge Mayhew
* Glenda Farrell as Valerie Wilson
* Robert Barrat as Tex Willis
* Hobart Cavanaugh as George Mayhew
* Gordon Westcott as Joe
* Sidney Toler as John Bright
* War Cry as Dark Hazard
* George Meeker as Pres. Barrow
* Emma Dunn as Mrs. Mayhew
* Willard Robertson as Fallen
* William V. Mong as Mr. Plummer
* Henry B. Walthall as Schultz (*IMDb reports Walthall not in film)
* Barbara Rogers as Miss Dolby

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 


 