The Magnificent Two
{{Infobox film
|  name          = The Magnificent Two
|  image         = "The_Magnificent_Two"_(1967).jpg
|  caption       = British quad poster by Arnaldo Putzu 
|  director      = Cliff Owen Peter Blackmore
| based on       = a story by Michael Pertwee
|  starring      = Eric Morecambe Ernie Wise Hugh Stewart
|  music         = Ron Goodwin
|  cinematography= Ernest Steward
|  editing       = Gerry Hambling
|  studio        = Rank Organisation Rank
|  released      = 5 July 1967	(London)  (UK)  (premiere) 
|  runtime       = 100 min.
|  language      = English 
}}
The Magnificent Two is a 1967 British comedy film directed by Cliff Owen and starring Morecambe and Wise in the third and final of their 1960s trio of films.   They play two British salesmen sent to South America to sell their goods. While there they become involved in a revolution to oust a brutal dictator. 

==Cast==
* Eric Morecambe as Eric
* Ernie Wise as Ernie
* Margit Saad as Carla Virgilio Teixeira as Carillo
* Cecil Parker as British ambassador
* Isobel Black as Juanita Martin Benson as President Diaz
* Tyler Butterworth as Miguel - Presidents Younger Son
* Sandor Elès as Armandez
* Victor Maddern as Drunken soldier
* Michael Gover as Doctor

==Critical reception== Time Out wrote, "in which the comedians special talents are woefully misused. At least Cliff Owen keeps it pacy, making it the least awful of the trio of movies in which the duo failed to take the cinema by storm" ;   and TV Guide described it as a "fair comedy."   

==References==
 

 

 
 
 
 
 
 
 
 
 
 

 
 