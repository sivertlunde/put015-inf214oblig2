Tokyo Raiders
 
 
 
{{Infobox film
| name           = Tokyo Raiders
| image          = Tokyo-Raiders-poster.jpg
| caption        = Film poster
| director       = Jingle Ma
| producer       = Raymond Chow
| screenplay     = Susan Chan Felix Chong Hiroshi Abe Kumiko Endoh 
| music          = Peter Kam
| cinematography = Jingle Ma Chan Chi-ying
| editing        = Kwong Chi-leung Golden Harvest Pictures (China) Ltd.
| released       =  
| runtime        = 118 minutes
| country        = Hong Kong Japanese English English
| budget         =
| gross          = HK$28 million   
}}
Tokyo Raiders ( ) (  set in Hong Kong and Tokyo, directed by Jingle Ma and starring Tony Leung Chiu Wai, Ekin Cheng and Kelly Chen. The success of the film led to the making of its sequel, Seoul Raiders, in Hong Kong films of 2005|2005. The film was the last film released on LaserDisc in Japan. {{cite news
     | publisher = Laserdisc Database
     | title = Tokyo Raiders on LDDB url = http://www.lddb.com/laserdisc/12189/PILF-2870/Tokyo-Raiders-%282000%29
    }} 

==Plot==
 

==Cast==
* Tony Leung Chiu-Wai
* Ekin Cheng
* Kelly Chen
* Cecilia Cheung Toru Nakamura Hiroshi Abe
* Kumiko Endô
* Maju Ozawa
* Yûko Moriyama
* Minami Shirakawa
* Takeshi Yamato

==Reception== Time Out London wrote, "Made to order for Chinese New Year release, this charmless action-comedy would like to be Charade but hasnt a clue how to handle plot structure, characterisation or secret-identity twists."   Keith Phipps of The A.V. Club wrote that it "delivers the mix of humor, action, and style promised, though not delivered, by the big-screen version of Charlies Angels".   Beyond Hollywood wrote, "Aside from an overbearing soundtrack and a hackneyed plot with too many silly twists, Tokyo Raiders is good for a laugh." 

==References==
 

==External links==
*  
*  
*   at  

 

 
 
 
 
 
 
 
 
 
 
 


 
 