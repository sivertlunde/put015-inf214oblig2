The Fantasy Film Worlds of George Pal
{{Infobox Film |
  name           = The Fantasy Film Worlds of George Pal|
  image          = ffwgp(2).jpg|
  caption        = |
  writer         = Arnold Leibovit|
  starring       = |
  director       = Arnold Leibovit|
  producer       = Arnold Leibovit|
  editing        = |
  movie_music    = |
  distributor    = |
  released       = 1985|
  runtime        = 93 minutes|
  country        = USA|
  language       = English |
  music          = |
  budget         = |
}}
The Fantasy Film Worlds of George Pal is a documentary film about Academy Award winning producer/director George Pal. It was written, directed, and produced by Arnold Leibovit and released in 1985.  The film was premiered at the Academy of Motion Pictures Arts and Sciences as part of the annual George Pal Lecture on Fantasy in Film. 

 

It follows Pal’s career, beginning with his early life in Hungary and Germany, and covering his progression from cartoon artist to creator of stop-motion animated short films (known as Puppetoons) to full length feature motion pictures. Pal was a visionary and innovator in the world of motion pictures, especially in the area of stop motion animation, which he pioneered. His work earned him eight Academy Awards and served as an inspiration for Gene Roddenberry, Steven Spielberg and George Lucas, among others. The film includes interviews with Pals cast members, crew, and peers, as well as Pal himself.

It was released on DVD on August 29, 2000. 

==Cast==
 
*Robert Bloch
*Chesley Bonestell
*Ray Bradbury
*Wah Chang
*Tony Curtis
*Jim Danforth
*Joe Dante Roy Edward Disney
*Barbara Eden
*Paul Frees
*Duke Goldstone
*Gae Griffith
*Ray Harryhausen
*Charlton Heston
*Walter Lantz
*Janet Leigh
*Albert Nozaki
*David Pal
*Tony Randall
*Ann Robinson
*Gene Roddenberry
*Russ Tamblyn
*Rod Taylor William Tuttle
*Gene Warren
*Robert Wise
*Alan Young
*Ward Kimball
*Yvette Mimieux
*George Pal
 

==External links==
* 

==References==
 

 
 
 
 

 