Women for Golden Hill
{{Infobox film
| name = Women for Golden Hill
| image =
| image_size =
| caption =
| director = Erich Waschneck
| producer = Hermann Grund 
| writer =  Hans Bertram   George Hurdalek   Wolf Neumeister
| narrator =
| starring = Kirsten Heiberg   Viktor Staal   Elfie Mayerhofer
| music = Werner Eisbrenner   
| cinematography = Werner Krien   
| editing =  Erich Kobler       UFA 
| distributor = UFA
| released = 30 December 1938 
| runtime = 92 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Women for Golden Hill (German:Frauen für Golden Hill) is a 1938 German drama film directed by Erich Waschneck and starring Kirsten Heiberg, Viktor Staal and Elfie Mayerhofer. The fims sets were designed by the art directors Gustav A. Knauer and Alexander Mügge. The all-male inhabitants of an Austrian mining camp send off for some mail order brides from Sydney. Two men refuse to join in, but their friend secretly arranges for two wives for them. Unfortunately one of them proves to be his own abandoned wife, who takes up with him again. This means a love triangle develops between the two men around the remaining woman.
 on location at Kurische Nehrung in East Prussia, which stood in for Australia. The film premiered on 30 December in Frankfurt, but was not a major box office hit. 

==Cast==
*   Kirsten Heiberg as Violet  
* Viktor Staal as Douglas  
* Elfie Mayerhofer as Kitty  
* Grethe Weiser as Mdm. Doolittle  
* Hubert von Meyerinck as Tanzmanager  
* Olaf Bach as Bully  
* Ernst Waldow as Cocky  
* Otto Gebühr as Kirkwood  
* Anna Grandi as Gwendolin  
* Wolfgang Kieling as Pat  
* Hans Adalbert Schlettow as Thomas Trench  
* Paul Dahlke as Barryman 
* Lotte Rausch as Alice Bedford 
* Wilhelm König as ONeilly  
* Waltraut von Negelein as Dorothy  
* Ilse Petri as Margaret  
* Jack Trevor as Larry  
* Albert Venohr as Algenon  
* Emmy Harold as Louise 
* Hans Joachim Schölermann as Frank 
* Lilly Towska as Barbara  
* Gustav Püttjer as Bill  
* Erika Glässner as Miss Kellington  
* Margot Erbst as Elisabeth  
* Kurt Iller as Pierre 
* Edith von Donat as Emmy  
* Kurt Weisse as Doran  
* Grete Reinwald as Brigitt  
* Wolfgang Kuhle as Managan  
* Alfred Köhler as Josua  
* Josefine Bachert as Mammy  
* Hannelore Dworski as Daisy  
* Erich Ziegler as Reverend Jones  
* Erich Walter as Staatsbeamter
* Albert Florath as Secretary  
* Werner Schott as Flughafenkommandant  
* Harry Gondi as Pilot Jim  
* Karl Martell as Stanley  

== References ==
 

== Bibliography ==
* OBrien, Mary-Elizabeth. Nazi Cinema as Enchantment: The Politics of Entertainment in the Third Reich. Camden House, 2006.

== External links ==
*  

 
 
 
 
 
 
 
 
 

 