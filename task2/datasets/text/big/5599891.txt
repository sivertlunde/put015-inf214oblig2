Cinderella (1994 film)
{{multiple issues|
 
 
}}

{{Infobox Film |
  name           = Cinderella |
  image          = Cinderella 1994.JPG |
  size           = 250px |
  director       = Toshiyuki Hiruma Takashi |
  producer       = Mark Taylor | George Bloom |
  based on       =  |
  starring       = |
  distributor    = GoodTimes Entertainment |
  released = 1994 (USA) |
  runtime        = 48 minutes | English |
  country        =   Japan     United States |
  budget         = |
  music          = Andrew Dimitroff Nick Carr Ray Crossley Joellyn Copperman (lyrics)|
  awards         = |
}}
 directly to video in 1994, is a 48-minute animated film adapted from the classic fairy tale, "Cinderella" by Charles Perrault.  The movie was produced by Jetlag Productions and was distributed to DVD in 2002 by GoodTimes Entertainment as part of their "Collectible Classics" line.

==Story==
A wealthy and kind-hearted gentleman has all he could ever ask for, a loving and loyal wife and a most beautiful daughter who was kind, gentle and sweet. The three of them live happily in an opulent mansion on a hill overlooking the town. Life, however, is not always predictable and the mans wife fell gravely ill, dying a short time after leaving the man a widower and his daughter without a mother.  A year later the man then marries a new woman, who brings along her two plain-looking daughters who, along with their mother, possess selfish, vain and cruel hearts. They resent the mans girl for her beauty and her kind nature.  When the man leaves on a business trip, the woman and her daughters turn on the poor girl, forcing her into a life of drudgery and hard work. They kick her out of her own room and make her sleep on a straw mat in the kitchen, force her into wearing rags and wait on them hand and foot. As she sleeps by the fireside, she is constantly covered in ashes from the hearth and the Stepmother and her daughters nickname the girl, "Cinderella."

However, Cinderella realises she has a Fairy Godmother living in the snowglobe in her kitchen. Life turns around for the girl the day a grand ball is announced in which the prince will choose his bride. The Stepmother and her two daughters are invited, and the Stepmother takes her daughters into town to buy them new gowns. Cinderella wishes to go but the Stepmother refuses and orders her to help her daughters prepare for the ball. Once the Stepsisters are ready, Cinderella asks again to go to the ball, but the Stepmother refuses, unless Cinderella can sort a dish of lentils before they depart. With the help of her Fairy Godmother, she almost succeeds, but the Stepmother finds one lentil on the floor and refuses to allow Cinderella to go with them to the ball. After her Stepmother and Stepsisters abandon her with the help of the Fairy Godmother, Cinderella is provided with a magnificent gown and a luxurious coach and sets off for the ball, with a warning from the Fairy Godmother to leave before midnight or else the magic will cease and everything will return to normal. Cinderella attends the glorious ball and wins the heart of the Prince, who up until then had refused to wed until he found a woman he truly loved. When midnight strikes, Cinderella flees through the palace gardens, leaving one glass slipper behind in the rush. The Prince vows to scour the land to find the girl whose foot fits the slipper, as he will marry no other. When the Prince arrives at Cinderellas house, the Stepmother orders her into the attic so as not to spoil her own daughters chances. The elder Stepsister tries the shoe, but her foot is too fat; the younger Stepsisters foot is too small. But the Prince hears Cinderella singing and demands to see her. The shoe fits her foot perfectly, but to make sure it is not a coincidence, Cinderella brings forth the other slipper to prove who she is.
 happily ever after.

==Voices==
*Tony Ail
*Nathan Aswell
*Cheralynn Bailey
*Kathleen Barr
*Garry Chalk
*Lilliam Carlson
*Ian James Corlett
*Michael Donovan

==Songs==
*"Dream On, Cinderella"
*"The Chance of a Lifetime"
*"Where Have You Been All My Life?"

==See also==
* List of animated feature films
* Cinderella, the original fairy tale
* Charles Perrault
* Jetlag Productions

==External links==
*  
*  
*   at the Big Cartoon Database

 
 

 
 
 
 
 
 
 
 
 
 
 