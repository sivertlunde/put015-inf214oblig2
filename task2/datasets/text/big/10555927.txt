Souten
:For the movie of a similar name released in 2006 starring Mahima Chaudhry, see  .
{{Infobox film
| name           = Souten
| image          = gggggg.jpg
| image_size     = 
| caption        = 
| director       =Sawan Kumar Tak
| producer       =Sawan Kumar Tak
| writer         = 
| narrator       = 
| starring       = Rajesh Khanna Padmini Kolhapure Tina Munim
 | music          = Usha Khanna Sawan Kumar (lyrics)
| cinematography = 
| editing        = David Dhawan
| distributor    = 
| released       = June 3, 1983
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
}}
 1983 Hindi Indian feature directed by Sawan Kumar Tak, starring Rajesh Khanna, Padmini Kolhapure, Tina Munim, Prem Chopra and Pran (actor)|Pran. The film is a family drama. It was written by Kamleshwar, with music by Usha Khanna.  The song "Shayad Meri Shaadi" became especially memorable.

==Plot==

Shyam is an ambitious man who meets Rukmini, the daughter of a millionaire, and falls in love with her. They get married despite strong opposition from Rukmanis family. Now Shyam wants to become a father, but Rukmani is not interested in becoming a mother; hence, the relationship between Shyam and Rukmini worsens. Shyam and Rukmini separate. Radha, daughter of Shyams employee, brings a ray of hope into Shyams life. What happens next forms the climax of the story.

==Cast==
*Rajesh Khanna	... 	Shyam Mohit
*Padmini Kolhapure	... 	Radha
*Tina Munim	... 	Rukmani Mohit (Ruku) Pran	... 	        Raisaheb Prannath Pasha
*Prem Chopra	... 	Sampatlal
*Shreeram Lagoo	... 	Gopal
*Shashikala	... 	Renu Prannath Pasha
*Paresh Rawal	... 	Mr. Jain
*Vijay Arora	... 	Vijay Satyendra Kapoor		
*Roopesh Kumar	... 	Bingo (Photographer)
*Yunus Parvez	... 	Defending Lawyer
*T. P. Jain     ... Gunwant Lal

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Shayad Meri Shaadi"
| Kishore Kumar, Lata Mangeshkar
|-
| 2
| "Jab Apne Ho Jayen"
| Asha Bhosle
|-
| 3
| "Main Teri Chhoti Bahna Hoon"
| Lata Mangeshkar
|-
| 4
| "Meri Pahle Hi"
| Kishore Kumar, Anuradha Paudwal
|-
| 5
| "Sasu Tirath Sasura Tirath"
| Kishore Kumar
|-
| 6
| "Zindagi Pyar Ka Geet Hai (I)"
| Lata Mangeshkar
|-
| 7
| "Zindagi Pyar Ka Geet Hai (II)"
| Kishore Kumar
|}
==Box-Office==

Souten is a Major success of 1983.and one of the favorite movie of Rajesh Khanna  

==Nominations==
*Filmfare Nomination for Best Supporting Actress--Padmini Kolhapure
*Filmfare Nomination for Best Music--Usha Khanna
*Filmfare Nomination for Best Lyricist--Sawan Kumar for the song "Zindagi Pyar Ka Geet Hai"
*Filmfare Nomination for Best Lyricist--Sawan Kumar for the song "Shayad Meri Shaadi"
*Filmfare Nomination for Best Male Playback Singer--Kishore Kumar for the song "Shayad Meri Shaadi" 

==Trivia==
The movie is loosely based on the historical novel "The Immigrants" by Howard Fast.

==References==
 
* http://www.bollywoodhungama.com/movies/cast/4583/index.html
* http://ibosnetwork.com/asp/filmbodetails.asp?id=Souten

==External links==
*  

 
 
 
 
 