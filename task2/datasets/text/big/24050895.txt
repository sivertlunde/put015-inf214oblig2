Water Rustlers
{{Infobox film
| name           = Water Rustlers
| image_size     =
| image	=	Water Rustlers FilmPoster.jpeg
| caption        =
| director       = Samuel Diege
| producer       = George A. Hirliman (executive producer) Don Lieberman (producer)
| writer         = Lawrence Meade (story) Don Laurie (story) Arthur Hoerl (screenplay)
| narrator       =
| starring       = See below
| music          =
| cinematography = Mack Stengler (director of photography)
| editing        = Guy V. Thayer Jr. (as Guy Thayer)
| distributor    = Grand National Films
| released       = 6 January 1939
| runtime        = 54 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
 Dorothy Page singing cowgirl film for Grand National Films.

== Cast == Dorothy Page as Shirley Martin Dave OBrien as Bob Lawson
*Vince Barnett as Mike, the cook
*Stanley Price as Robert Weylan
*Ethan Allen as Tim Martin
*Leonard Trainor as Andy Jurgens, rancher
*Warner Richmond as Wiley, crooked foreman
*Edward R. Gordon as Henchman Herman
*Edward Peil Sr. as Lawyer
*Lloyd Ingraham as Judge
*Merrill McCormick as Sheriff

== Soundtrack ==
*Dorothy Page -"Lets Go On Like This Forever" (Written by Al Sherman)
*Dorothy Page - "When a Cowboy Sings a Dogie Lullaby" (Written by Walter Kent)
*Dorothy Page - "I Feel at Home in the Saddle" (Written by Milton Drake)
Sung by

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 