Snowden (film)
{{Infobox film
| name           = Snowden
| image          = 
| alt            = 
| caption        = 
| director       = Oliver Stone
| producers      = Moritz Borman Eric Kopeloff Philip Schulz-Deyle Fernando Sulichin
| writers        = Kieran Fitzgerald Oliver Stone
| based on       =   and  
 
| starring       = Joseph Gordon-Levitt Shailene Woodley Scott Eastwood Melissa Leo Timothy Olyphant Zachary Quinto Tom Wilkinson
| music          = 
| cinematography = Anthony Dod Mantle
| editing        = Alex Marquez
| production companies = Endgame Entertainment KrautPack Entertainment Onda Entertainment Vendian Entertainment
| distributor    = Open Road Films
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
 biographical political thriller film directed by Oliver Stone and written by Stone and Kieran Fitzgerald. The film is based on the books The Snowden Files by Luke Harding and Time of the Octopus by Anatoly Kucherena. The film stars Joseph Gordon-Levitt, Shailene Woodley, Scott Eastwood, Melissa Leo, Timothy Olyphant, Zachary Quinto, Nicolas Cage and Tom Wilkinson. Filming began on February 16, 2015 in Munich, Germany. Snowden is scheduled to be released in North American theaters on December 25, 2015.

== Plot ==
Edward Snowden, an American computer professional, leaked classified information from the National Security Agency (NSA) to the mainstream media starting in June 2013.

== Cast ==
* Joseph Gordon-Levitt as Edward Snowden
* Shailene Woodley as Lindsay Mills
* Scott Eastwood
* Melissa Leo as Laura Poitras
* Timothy Olyphant
* Zachary Quinto as Glenn Greenwald
* Tom Wilkinson as Ewen MacAskill
* Nicolas Cage
* Keith Stanfield
* Rhys Ifans
* Joely Richardson
* Ben Schnetzer

== Production ==
=== Development === Wild Bunch was set to handle foreign sales.  Deadline confirmed on November 10, 2014 that Endgame Entertainment had boarded the film to produce.  In April 2015, WikiLeaks revealed that Stone paid $700,000 for the rights to Hardings book and $1 million for rights to Kucherenas novel. 

=== Casting ===
On September 21, 2014, Joseph Gordon-Levitt was in talks to play Edward Snowden, the American computer professional who leaked classified information from the National Security Agency (NSA) to the mainstream media starting in June 2013.  On November 10, 2014, news confirmed that Levitt would be starring in the lead role.    On November 14, 2014, Shailene Woodley was in final talks to join the film to play Snowdens girlfriend, Lindsay Mills.    On February 2, 2015, Scott Eastwood joined the cast to play an NSA agent.    On February 4, 2015, three more actors joined the cast; Melissa Leo will play documentary filmmaker Laura Poitras, Zachary Quinto will play Glenn Greenwald, the journalist chosen by Snowden to leak sensitive information, and Tom Wilkinson will play Ewen MacAskill, defense and intelligence correspondent for The Guardian, who helped report the Snowden story.    On February 13, 2015, Variety reported that Ben Schnetzer had also joined the film.    On February 19, 2015, Timothy Olyphant joined the film to star as a CIA agent who befriended Snowden before he fled to Russia,    and Rhys Ifans and Joely Richardson were added to the cast of the film on February 20, 2015.  Nicolas Cage also signed on to play the role of a former US Intelligence official on February 23, 2015.    Keith Stanfield was added to the cast on February 25, 2015 to play a National Security Administration co-worker and a close friend to Snowden.   

=== Filming ===
Filming began on February 16, 2015 in Munich, Germany.  Shooting was underway in Washington, D.C. in early-April.  Shooting in Hawaii began on April 15th and lasted until April 18th. The house used to film is on the same street Edward Snowden lived on. At the end of April, Hong Kong press reported that crews started filming in The Mira Hong Kong, followed by outdoor filming in some old buildings in To Kwa Wan. {{cite news

| last =
 | first =
 | title = Hollywood film "Snowden" crews have arrived Hong Kong for shooting
 | newspaper = Local Press Hong Kong
 | location = Hong Kong
 | pages =
 | language = Hong Kong Chinese
 | publisher = Local Press
 | date =2015-04-27
 | url =http://localpresshk.com/2015/04/%E8%8D%B7%E9%87%8C%E6%B4%BB%E9%9B%BB%E5%BD%B1snowden%E6%8A%B5%E9%A6%99%E6%B8%AF%E5%8F%96%E6%99%AF/
| accessdate =2015-04-28 }}   Shooting will last until mid-May. 

== Release ==
On February 20, 2015, Open Roads set the film for a December 25, 2015 domestic release.    Pathé would release the film in France on December 30, 2015 and Universum Film would release in Germany on January 7, 2016. 

== References ==
 

== External links ==
*  
*  


 

 
 
 
 
 
 
 
 