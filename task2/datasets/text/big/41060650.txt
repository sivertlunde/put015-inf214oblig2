The Carpet from Bagdad
 
 
{{Infobox film
| name        = The Carpet from Bagdad
| image       = CarpetfromBagdad.jpg
| caption     = Wheeler Oakman, Kathlyn Williams, and Charles Clary in The Carpet from Bagdad
| starring    = {{Plainlist|
* Kathlyn Williams
* Wheeler Oakman
* Guy Oliver
}}
| based on    =   Colin Campbell
| writer      = Harold MacGrath
| studio      = Selig Polyscope Company
| distributor = V-L-S-E
| released    =  
| country     = United States
| runtime     = 5 reels  Silent (English intertitles)
}} silent adventure Colin Campbell and based on Harold MacGraths 1911 eponymous novel. In the story, Horace Wadsworth (played by Guy Oliver), one of a gang of criminals also planning a bank robbery in New York, steals the titular prayer rug from its Baghdad mosque. He sells the carpet to antique dealer George Jones (Wheeler Oakman) to fund the robbery scheme. But the theft places both men and Fortune Chedsoye (Kathlyn Williams), the innocent daughter of another conspirator, in danger from the carpets guardian.
 tinted desert RMS Lusitania in 1982.

==Plot==
Horace Wadsworth, disinherited brother of New York banker Arthur Wadsworth, joins a gang of international criminals. He plots to rob his brothers bank by constructing a tunnel from the nearby home of antique dealer George Jones, who is currently on a trip to Cairo to purchase antique rugs. Horace follows him there, and learning of the Sacred Carpet of Bagdad, joins the caravan of its sworn guardian, Mohamed. Meanwhile, Horaces confederates Major Callahan and Mrs. Chedsoye arrive in Cairo along with Mrs. Chedsoyes daughter, Fortune, who is unaware of her mothers illicit activities.

When the criminals meet with Jones in Cairo, he becomes enamored with Fortune. Horace steals the Sacred Carpet from Mohameds mosque and sells it to Jones to fund the robbery plan. Fortune, becoming suspicious of her mother and the surrounding events, steals the prayer rug from Jones and hides it in her mothers effects. Unable to locate the stolen carpet, Mohamed kidnaps Horace, Jones, and Fortune. Meanwhile, Mrs. Chedsoye and Major Callahan return to New York, where a fourth member of the conspiracy, Wallace, has acquired forged paperwork to gain access to the Jones residence.

The captives escape from Mohameds planned torture and flee to Damascus. Horace immediately returns to New York to rejoin his compatriots. Fortune and Jones, who have fallen in love, also travel back to New York. Once there, Jones learns of the forgery, and returns home to confront the gang, who still have the Sacred Carpet and who have completed their tunnel into the vaults of Arthur Wadsworths bank. Sympathetic to Horace after their shared experiences, Jones offers the robbers a two-hour lead before he notifies the police, but keeps the prayer rug. Meanwhile, Mohamed resigns himself to the loss of the carpet.  

==Cast==
* Kathlyn Williams as Fortune Chedsoye
* Wheeler Oakman as George P.A. Jones
* Guy Oliver as Horace Wadsworth
* Eugenie Besserer as Mrs. Chedsoye Frank Clark as Major Callahan
* Charles Clary as Mohamed Harry Lonsdale as Arthur Wadsworth
* Fred Huntley as Wallace  

==Production and marketing==
The Carpet from Bagdad is a film adaptation of Harold MacGraths 1911 novel of the same name.  MacGrath was a well-traveled, successful author of over a dozen novels.  Stories with Asian settings were in vogue at the time,  and both The Carpet from Bagdad and the Selig Polyscope Companys previous adaptation of MacGraths work, the popular serial The Adventures of Kathlyn, are set in part in the Near East. 
 Colin Campbell hand tinted.  Production costs exceeded $35,000,  the equivalent of over $ }} in present-day terms. 
 William Selig screened in a special invitation-only showing at the art gallery of the Bobbs-Merrill Company, publisher of MacGraths novel, an early example of a sponsored exhibition of a feature film in a location other than a theater. 

==Reception and legacy==
  Peter Milne The Spoilers.  Clarence Caines review in Motography also compared the The Carpet from Bagdad favorably to The Spoilers, but he viewed the films color as its best feature, especially the closing scene of a desert sunset.  Several newspaper reviews also complimented the tinted desert scenes,   with New Zealands The Levin Chronicle describing the film as "a gem of the cinematographers art" for its use of color.  The Chicago Daily Tribune offered a more mixed opinion on the film; reviewer Kitty Kelly found it difficult to care about characters "overshadowed by environment", and considered the 35-year-old Williams unconvincing as an ingenue (stock character)|ingenue. 

Despite the acclaim from many contemporary reviewers, modern scholars of the silent film era would not consider The Carpet from Bagdad a masterpiece, according to the British Film Institutes Clyde Jeavons. 

===Partial rediscovery=== RMS Lusitania, print of The Carpet from Bagdad was probably being taken to London as a film distributors preview, as was the case for several other films known to have been on board. 

==See also==
*List of incomplete or partially lost films

==References==
{{reflist|colwidth=30em|refs=
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
waller   
}}

==Bibliography==
*  
*  
*  
*  
*  

==External links==
 
* 

 
 
 
 
 
 
 
 
 
 