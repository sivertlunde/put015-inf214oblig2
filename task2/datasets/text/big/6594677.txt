Mojave Moon
{{Infobox film name           =Mojave Moon image_size     =
| image	=	Mojave Moon FilmPoster.jpeg caption        = director  Kevin Dowling producer       =Matt Salinger writer         =Leonard Glasser starring       =Danny Aiello Anne Archer Angelina Jolie Michael Biehn Alfred Molina music          =Johnny Caruso Denys Gawronski cinematography =James Glennon editing        =Susan R. Crutcher distributor    =Initial Entertainment Group released       =1996 runtime        =91 minutes country        =USA language  English
|budget         =
}}
 American road Kevin Dowling.

==Plot==
Al McCord is hanging out at his favourite restaurant when he meets an attractive young woman, Ellie, who is looking for a ride from the city into the Mojave Desert, where her mother Julie lives. Al discovers romance with the free-spirited Julie despite the nearby presence of her boyfriend Boyd, who seems likely to go berserk at any moment. Strange events follow and its up to Al to find an explanation.

In the desert, Als car develops a flat tire. He opens the trunk and discovers an apparent dead body (Ellies boyfriend Kaiser, whom Al had not yet been aware of). Al heads to a gas station for repairs, following a near run-in with a highway patrolman, when he is interrupted by an attempted robbery. The confusion created by the gun-happy gas station worker allows Al to escape unhurt.

Back home, Al is visited by Ellie, followed by Julie. Unknown to him at this point, his car is being stolen. Outside they notice Boyd and flee to a nearby motel. Just after Al leaves to search for his car, Boyd arrives at the motel, taking the women back to Mojave and locking them in a shed.

Al and actor buddy Sal mount a rescue mission but are themselves knocked out by Boyd. Kaiser, still alive, arrives at Mojave unseen as Boyd drives off with Al, Sal, Julie and Ellie in the car atop his truck, intending on tipping the car and passengers over the edge of a quarry. They eventually escape uninjured while Boyd plummets to his doom, signaled by an ostentatious explosion.

Ellie returns to Los Angeles with Sal, while Al remains for the night, at least, with Julie.

==Cast==
*Danny Aiello - Al McCord
*Angelina Jolie - Ellie Rigby
*Anne Archer - Julie, Ellies mother
*Michael Biehn - Boyd, Julies boyfriend
*Alfred Molina - Sal Santori
*Jack Noseworthy - Kaiser, Ellies boyfriend
*Zach Norman - Terry
*Michael Massee as the Fifth Guy

==References==
 

==External links==
* 

 
 
 

 