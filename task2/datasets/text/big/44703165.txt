Musthaffaa
{{Infobox film
| name           = Musthaffaa
| image          =
| image_size     =
| caption        =
| director       = R. Aravindraj
| producer       = P. G. Shrikanth
| writer         = K. Prasanna
| starring       =   Vidyasagar
| cinematography = K. Rajpreeth
| editing        = R. T. Annadurai
| distributor    =
| studio         = S.G.S. Cinearts International
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1996 Tamil Tamil action action drama Napoleon and Mansoor Ali Charmila playing Vidyasagar and was released on 16 February 1996. The film was later remade into Hindi as Ghulam-E-Mustafa starring Nana Patekar and Raveena Tandon in the lead roles.    

==Plot==

Musthaffaa (Napoleon (actor)|Napoleon) is the trusted henchman of the godfather Periyavar (Sooriya). When politicians need help, they notify Periyavar and Musthaffaa in turn solves the problem with his sidekick Chellappa (Goundamani). Musthaffaa considers Periyavar as his own father and he calls him Vappa (father in Tamil Muslim) whilst he considers Chellappa as his own elder brother.

In the meantime, Sundaresan (K. Prasanna) is appointed as an office worker in a government office and he has persistently refused bribes. Being from a middle class orthodox Brahmin family, Sundaresan lives his sick wife Bhagyalakshmi (Lakshmi (actress)|Lakshmi), his daughter Lalitha (Charmila (actress)|Charmila) and his son Ramkumar (Ganeshkar). His daughter falls in love with Lakshmanan (Babloo Prithiveeraj) while his son wants to become an engineer. So Lakshmanans father asks them a huge dowry for the wedding and the engineer school principal asks them a huge amount to enrol their son. Without enough revenue, they refuses both proposals. Knowing their problems, Musthaffaa pressures Sundaresan to sign some contracts without examining them in exchange of bribes but they still refuse.
 Mansoor Ali Khan) which wants to kill Periyavar. Meanwhile, a dancer Kavita (Ranjitha), who works in Kaalaiyas dance club, is saved by Musthaffaa from Kaalaiyas henchmen. Thanks to Musthaffaa, Kavita becomes a Bharata Natyam teacher. Kavita develops a soft corner for Musthaffaa and they finally decide to get married. In the meantime, Bhagyalakshmis asthma has worsened and she is admitted to the hospital. To treat her, Sundaresan must disburse a huge amount. The next day, the vigilance officer Rajaram (Kazan Khan) clothes as a civilian, tries to corrupt the officers but only Sundaresan accepts. Sundaresan is subsequently arrested for corruption.

Kaalaiya plans to kill Periyavar so his son puts a bomb in his car. Unfortunately, Kavita gets killed in the car blast before their wedding. Thereafter, Musthaffaa turns berserk and kills Kaalaiyas son. Musthaffaa decides to become a good man so he helps Sundaresans family and decides to live with them. Thereby, Periyavar becomes upset over Musthaffaas decision. First, the relationship between Musthaffaa and Sundaresans family was very tense but later they lived together in total harmony respecting their customs.

During the Legislative Assembly election, the violence is in full swing between the ruling party and the opposition party.  Periyavar, on the side of the ruling party, cannot control it without Musthaffaa while Kaalaiya, on the side of the opposition party, is gaining in power with Rajarams aid. So Musthaffaa decides to take this last job to finance dowry for Lalithas wedding, to finance Ramkumars education, to give again Sundaresans job and to treat Bhagyalakshmis asthma. Finally, Musthaffaa kills Kaalaiya and the ruling party wins the election. The minister Sathyanathan (Prathapachandran) congrats him for his work, as for Musthaffaa, he requests the minister to deal the problems peacefully but it does not please Sathyanathan. During Lalithas wedding, Musthaffaa is arrested by the police, as a transformed person, he accepts the sentence. There, in a twist of fate, Periyavars henchman shoots Musthaffaa in the back and he died in Periyavars arms. Periyavar orders it because of fear of reprisal and self-interest. In turn, Chellappa in tears shoots Periyavar and Chellappa is immediately arrested.

==Cast==
 Napoleon as Musthaffaa
*Ranjitha as Kavitha
*Goundamani as Chellappa Mansoor Ali Khan as Kaalaiya
*K. Prasanna as Sundaresan Lakshmi as Bhagyalakshmi
*Kazan Khan as Rajaram Alex
*Sooriya as Vappa/Periyavar
*Rajeshkumar as Kaalaiyas son
*Babloo Prithiveeraj as Lakshmanan
*Ganeshkar as Ramkumar Charmila as Lalitha
*Suryakanth
*Prathapachandran as Sathyanathan
*Kumarimuthu
*T. S. Raghavendra as Lakshmanans father
*Vaithi
*Swaminathan
*Murali Kumar
*Janaki as Kavithas mother
*P. R. Varalakshmi as Lakshmanans mother
*Mythili
*Jayamani

==Soundtrack==

{{Infobox album |  
| Name        = Musthaffaa
| Type        = soundtrack Vidyasagar
| Cover       = 
| Released    = 1996
| Recorded    = 1996 Feature film soundtrack
| Length      = 21:06
| Label       =  Vidyasagar
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Vidyasagar (music director)|Vidyasagar. The soundtrack, released in 1996, features 5 tracks with lyrics written by Vairamuthu.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Kadhalar Mattum || Mano (singer)|Mano, Swarnalatha || 4:33
|- 2 || Kalapu Mayile || Mano, Malgudi Subha, Vidyasagar || 4:22
|- 3 || Kannukkum Kannukkum || Hariharan (singer)|Hariharan, Gopal Sharma || 4:51
|- 4 || Vallavanda Vallavanda || Mano, Chorus || 2:49
|- 5 || Vaya Mappillai || Sadhana Sargam || 4:31
|}

==Production== Vidyasagar composed Napoleon accepted to play the title role. Napoleon has acted in P. G. Shrikanths previous venture Seevalaperi Pandi which was a blockbuster and a turning point in his career.    

==Reception== Napoleon and said : "whatever inadequacies Musthaffaa had in the first half is redeemed in the second half". 

==References==
 

 
 
 
 