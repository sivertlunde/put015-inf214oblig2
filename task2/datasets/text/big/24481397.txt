Defenders of Riga
 
{{Infobox film
| name           = Defenders of Riga
| image          = Defenders of Riga DVD.jpg
| image_size     =
| caption        = Cover of Scandinavian release
| director       = Aigars Grauba
| producer       = Andrejs Ēķis
| writer         = Andris Kolbergs Lisa Eichhorn
| narrator       =
| starring       = Jānis Reinis Elita Kļaviņa Artūrs Skrastiņš
| music          = Aigars Grauba
| cinematography = Gvido Skulte
| editing        = Līga Pipare
| distributor    =
| released       = 11 November 2007
| runtime        =
| country        = Latvia
| language       = Latvian LVL 2.2 million 
| gross          =
}} Riga during 1919 struggle for independence.

Already by the fifth week of screening, the film had become the most-watched Latvian film produced after the return of independence in 1991. By then, it had been viewed by over 123,000 people. 

The outdoor scenes of the film were shot at the Cinevilla backlot in Tukums, Latvia, a backlot built especially for Defenders of Riga.

==Cast==
*Jānis Reinis ..... Mārtiņš
*Elita Kļaviņa ..... Elza
*Ģirts Krūmiņš ..... Pavel Bermondt
*Romualds Ancāns ..... Rüdiger von der Goltz
*Indra Briķe ..... Countess
*Vilis Daudziņš ..... Paulis
*Uldis Dumpis ..... Priest
*Kestutis Jakstas ..... Kārlis Ulmanis
*Andris Keišs ..... Ernests Savickis
*Ģirts Ķesteris ..... Arnolds
*Artūrs Skrastiņš ..... Jēkabs
*Agris Māsēns ..... Augusts Savickis

==External links==
*  

===References===
 

 
 
 
 


 