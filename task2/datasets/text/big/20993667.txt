The Canyon
 
{{Infobox film
| name           = The Canyon
| image          = The Canyon.jpg
| caption        = Theatrical release poster
| director       = Richard Harrah
| producer       = Michael A. Pierce Mark Williams
| writer         = Steve Allrich
| starring       = Yvonne Strahovski Will Patton Eion Bailey
| music          = Heitor Pereira
| cinematography = Nelson Cragg
| editing        = Peter Fandetti
| studio         = Pierce/Williams Entertainment 
| distributor    = Magnolia Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = $10 million
| gross          =
}}

The Canyon is a 2009 thriller film written by Steve Allrich and directed by Richard Harrah.

==Plot==
The story starts with Nick and Lori, a newlywed couple who, after eloping, want to take a mule ride down into the canyon with a guide, Henry. The next day the trio head down into the canyon. After traveling for a while the guide offers to take them to old petroglyphs (rock carvings) which are a half days ride away. After some convincing from Nick, Lori eventually agrees to the journey. On the way Henry is bitten twice by rattlesnakes and in the process the trios mules flee. They are forced to make camp and begin heading back the next day. The guide is delirious and later in the day succumbs to the venom and dies. The couple bury him and are left to struggle on to find a way out. After a days hike the couple eventually reach a dead end with petroglyphs, realizing that they have spent the last day going the wrong way, they decide to attempt a climb up the rock face in the hope of getting a signal on Loris phone. Lori manages to call the emergency services but only manages to say they are in trouble before losing signal. Nick loses his grip in the crevice and the couple fall, in the process breaking Loris phone and Nicks leg, which gets trapped in a crevice at the base of the rock face.

Nicks leg is completely trapped and after a day of waiting and hoping for help to arrive he suggests that they cut his leg off so they can keep going. Lori disagrees with it but eventually she agrees and goes in search of the body of their guide to get his knife and returns to Nick to cut the leg. By this point infection has set in and Nick is in very bad shape, barely being able to move at all. Lori succeeds in freeing Nicks leg and cauterizes the wound with the blade of the knife after it being placed in the fire. When night falls wolves get attracted to the smell of blood and try to attack but Lori successfully fights them off with fire and a knife.

The next day dawns and Lori fashions a stretcher to move Nick, but the wolves track the smell and keep following them. The stretcher breaks but Lori attempts to drag Nick on, rejecting his pleas to leave him behind. The wolves are relentless in their attack and exhausted from days without food or water Lori loses the ability to defend him, as she fends off wolves from one direction, wolves attack Nick while her back is turned. The wolves surround them, and to prevent Nick from suffering through being eaten alive she performs Euthanasia on him by suffocating him. She breaks down and goes into shock as a rescue helicopter rounds the edge of the canyon with her kneeling next to Nicks body.

==Cast==
* Yvonne Strahovski as Lori Conway  
* Will Patton as Henry
* Eion Bailey as Nick Conway
* Wendy Worthington as Motel Manager
* Andrea Marcellus as Park Ranger
* Philip Salick as Elvis Impersonator

==Production==
Filmed on locations in Arizona and Utah. 

==Release==
The film opened in cinemas for a limited release on October 23, 2009.  The DVD of the film was released on November 17, 2009. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 