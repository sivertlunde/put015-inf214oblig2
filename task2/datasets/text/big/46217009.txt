Captain China
{{Infobox film
| name           = Captain China
| image          = Captain China poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Lewis R. Foster
| producer       = William H. Pine William C. Thomas
| screenplay     = Lewis R. Foster Gwen Bagni 
| story          = John Bagni Gwen Bagni John Payne Michael OShea Ellen Corby
| music          = Lucien Cailliet Josef Marais
| cinematography = John Alton
| editing        = Howard A. Smith
| studio         = Pine-Thomas Productions
| distributor    = Paramount Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 John Payne, Michael OShea and Ellen Corby. The film was released on February 2, 1950, by Paramount Pictures.  

==Plot==
 

== Cast == John Payne as Charles S. Chinnough / Capt. China
*Gail Russell as Kim Mitchell
*Jeffrey Lynn as Capt. George Brendensen
*Lon Chaney Jr. as Red Lynch
*Edgar Bergen as Mr. Haasvelt Michael OShea as Trask
*Ellen Corby as Miss Endicott Robert Armstrong as Keegan
*John Qualen as Geech
*Ilka Grüning as Mrs. Haasvelt
*Keith Richards as Alberts
*John Bagni as Sparks
*Ray Hyke as Michaels
*Paul Hogan as Speer
*Lawrence Tibbett Jr. as Wilkes
*Zon Murray as Gus 
*Don Gazzaniga as Tony
*Denver Pyle as Steve
*Wallace Scott as Scotty 
*Lee Roberts as Marsh
*Reed Howes as Blake
*Charles Regan as Wade

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 