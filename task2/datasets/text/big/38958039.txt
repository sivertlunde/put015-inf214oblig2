The Red (film)
{{Infobox film
| title = The Red
| image = File:The Red Movie Poster.jpg
| caption = Theatrical release poster
| studio = Borderline Films SS+K
| starring = Amy Northup Marisa Parry Drew Lewis Philip Chorba Jean Liuzzi Adam Byrd Laura Peterson
| music = Stenfert Charles
| editing = Andrew Marcus
| released =  
| country = United States
| language = English Theatrical distribution by = Hollywood Branding International}}

The Red is an 8-minute psychological thriller created by Borderline Films (makers of Martha Marcy May Marlene and Simon Killer) and advertising agency SS+K on behalf of SALT, a free resource created by the non-profit American Student Assistance. 

The Red is aimed towards making college students and recent graduates aware of their student loans and the inescapable anxiety and dread that it creates.   The film is part of a sustained, multi-media campaign created to compel and empower young people to take control of their debt on a practical, day-to-day level. 
 Hollywood Branding Hollywood Branding International hosted VIP movie premiere events in conjunction with the theatrical broadcast in each city.  The entire film short is now streaming online at www.facethered.com|FaceTheRed.com.

== Cast ==
* Amy Northup as Kate
* Marisa Parry as Clara
* Drew Lewis as Charlie
* Philip Chorba as Boss
* Jean Liuzzi as Teller
* Adam Byrd as ATM Man
* Laura Peterson as Waitress
* Tommy Maher as himself

== Production credits ==
Station Film Presents A Borderline Film The Red starring Amy Northup And Marisa Parry Directed By Borderline Films Edited By Andrew Marcus Music by Stenfert Charles Director Of Photography Joe Anderson Special Effects By MPC Art Directed, Written And Produced By SS+K.

== References ==
 

== External links ==
*  

 
 
 
 