Clouds of Sils Maria
 
{{Infobox film
| name           = Clouds of Sils Maria
| image          = Clouds of Sils Maria film poster.png
| alt            = 
| caption        = French theatrical poster
| director       = Olivier Assayas
| producer       = Charles Gillibert
| writer         = Olivier Assayas
| starring       = {{Plain list |
* Juliette Binoche
* Kristen Stewart
* Chloë Grace Moretz
}}
| music          = 
| cinematography = Yorick Le Saux
| editing        = Marion Monnier
| studio         = CG Cinéma Pallas Film CAB Productions Vortex Sutra Arte France Cinéma Orange Studio Radio Télévision Suisse SRG SSR idée suisse          
| distributor    = {{Plain list |
* Les Films du Losange  (France) 
* NFP Marketing & Distribution  (Germany) 
* Filmcoopi Zürich  (Switzerland) 
* IFC Films  (USA)  
* Artificial Eye  (United Kingdom)  
}}
| released       =  
| runtime        = 123 minutes  
| country        = {{Plainlist|
* Germany
* France
* Switzerland
}}
| language       = {{Plainlist|
* English
* French
* German
* Swiss German
}}
| budget         =  $6.6 million 
| gross          = $2.4 million 
}}
Clouds of Sils Maria is a 2014 drama film written and directed by Olivier Assayas, and starring Juliette Binoche, Kristen Stewart, and Chloë Grace Moretz. The film is a German-French-Swiss co-production.  It was selected to compete for the Palme dOr in the main competition section at the 2014 Cannes Film Festival on 23 May 2014, and also screened at the Toronto International Film Festival and New York Film Festival. It won the Louis Delluc Prize for Best Film in December 2014 and a best supporting actress César Award for Kristen Stewart in February 2015.

==Plot==
Maria Enders (Juliette Binoche) has a successful acting career and a loyal assistant Valentine (Kristen Stewart). She owes her career to having been cast in both the play and film versions of Maloja Snake by Wilhelm Melchior, which centers on the tempestuous relationship between a young girl and an older woman who is eventually driven to suicide. While traveling to Zurich to accept an award on behalf of Melchior, and planning to visit him the following day in nearby Sils Maria, Maria learns of his sudden death. His widow Rosa later confides in her that Wilhelms death was suicide and that he had been terminally ill. During the awards ceremony, Maria is approached by a popular theatre director who is trying to persuade her to appear on stage in Maloja Snake again, but this time in the role of the older woman.

To prepare for the role she accepts Rosas offer of the Melchiors house in Sils Maria, which Rosa is leaving to avoid her memories of Wilhelm. Marias discussions with Valentine and their read-throughs of the plays scenes combine to evoke uncertainty about the nature of their actual relationship. A young American actress (Chloë Grace Moretz) who has been chosen to interpret Marias old role in the play surfaces via Google searches, YouTube videos and tidbits of contemporary cultural knowledge as relayed by Valentine, are generally dismissed as irrelevant by Maria.

Questions regarding aging, time, culture and the thin line between the characters of the-play-in-the-film and those we are seeing on the screen multiply priming the denouement in London. The key to the resolution is offered by a young filmmaker who visits Maria by appointment 5 minutes before curtain on the opening night of Maloja Snake, but she seems preoccupied and dismisses the offering as "too abstract for me." The final scene of the film indicates however that the penny may have dropped ... or not; she is on stage, smoking and waiting for Sigrid to pass through the offices collecting outgoing folders. 

==Cast==
 , Olivier Assayas and Chloë Grace Moretz promoting the film at the 2014 Cannes Film Festival.]]
 
* Juliette Binoche as Maria Enders
* Kristen Stewart as Valentine
* Chloë Grace Moretz as Jo-Ann Ellis Johnny Flynn as Christopher Giles
* Lars Eidinger as Klaus Diesterweg
* Hanns Zischler as Henryk Wald   
* Brady Corbet as Piers Roaldson
* Aljoscha Stadelmann as Urs Kobler
* Benoit Peverelli as Berndt
* Luise Berndt as Nelly
* Angela Winkler as Rosa Melchior
* Gilles Tschudi as Mayor of Zurich
* Caroline de Maigret as Chanel Press Attache
* Claire Tran as Marias London Assistant (Mei-Ling)
* Jakob Kohn as Jo-Anns manager
 

==Production==
Principal photography of Clouds of Sils Maria began on August 22, 2013 and ended on October 4th.  The film was shot on location in the titular village of Sils Maria, Switzerland as well as Zurich, Leipzig, Germany and South Tyrol, Italy.

Chanel debuted in film financing and supplied the actresses with clothes, jewelry, accessories and makeup, while also providing some of the budget to allow Olivier Assayas to fulfill his dream of shooting a film on 35-mm film instead of digitally. 

The American title of the film is Clouds of Sils Maria, but in France the film is known by the original name, Sils Maria. 

==Marketing and festivals==
The first trailer for the film was released on May 22, 2014.  Another international trailer followed on July 7. 

It was selected to compete for the Palme dOr in the main competition section at the 2014 Cannes Film Festival on May 23, 2014.    It also screened at the Toronto International Film Festival and New York Film Festival.  

==Release==
===Critical reception===
Clouds of Sils Maria premiered at the Cannes Film Festival to positive reviews. The review aggregator Rotten Tomatoes reports a "Certified Fresh" score of 89% based on 104 reviews with an average rating of 7.6 out of 10. The sites consensus reads, Bolstered by a trio of powerful performances from its talented leads, Clouds of Sils Maria is an absorbing, richly detailed drama with impressive depth and intelligence.   On Metacritic, which assigns a weighted mean rating out of 100 reviews from film critics, the film holds an average score of 79, based on 36 reviews, indicating a  "generally favorable reviews" response. 

Robbie Collin of the The Daily Telegraph stated, "This is a complex, bewitching and melancholy drama, another fearlessly intelligent film from Assayas" and, praising the performance, said, "Binoche plays the role with elegance and melancholic wit – her character slips between fiction and fact in a way that recalls her role in Abbas Kiarostami’s Certified Copy. But it’s Stewart who really shines here. Valentine is probably her best role to date: she’s sharp and subtle, knowable and then suddenly distant, and a late, surprising twist is handled with a brilliant lightness of touch." 

Peter Debruge of Variety (magazine)|Variety said it was Assayas "daring rejoinder, a multi-layered, femme-driven meta-fiction that pushes all involved — including next-gen starlets Kristen Stewart and Chloë Grace Moretz — to new heights."  Matt Risley of Total Film called it "an elegant, intelligent drama, enlivened by strong performances by Binoche, Moretz and especially Stewart, for whom this will surely usher in a new dawn." 

  wrote: "This recalls Ingmar Bergmans chamber dramas in the intensity and psychological complexity of the central relationship, yet the filmmaking is breathtakingly fluid, evoking a sense of romantic abandon." 

However, Kyle Smith of New York Post writes: “A backstage drama that has all the sizzle of a glass of water resting on the windowsill,   Clouds of Sils Maria mistakes lack of dramatic imagination for smoldering subtlety.”  

===Box office===
Clouds of Sils Maria opened in France on August 20, 2014 in 150 theaters for a $3,663 per theater average and a box office total of $549,426 as of 24 August 2014.  The film expanded to 195 theaters in its second week of release and the box office increased to an estimated $1,150,090.

"Clouds of Sils Maria" opened in the United States on April 10, 2015 in three theaters and grossed $69,729 on its opening weekend for an average of $23,243 per. As of April 26, 2015, the film has grossed an estimated $545,000 after expanding to 69 theaters.  

==Awards and nominations==
The film won the Louis Delluc Prize for Best Film in December 2014.  The film received six César Award nominations including best film, best director, best actress, best original screenplay, and best cinematography, while Stewart won for best supporting actress, becoming the first American actress to win a César and the second American actor to win after Adrien Brody in 2003.  

==Soundtrack==
* "Kowalski (song)|Kowalski" by Primal Scream
* "Largo de Xerxes" (Handels Largo) - Georg Friedrich Handel
* "Canon and Gigue in D Major for 3 Violins and Basso Continuo" - Johann Pachelbel
* "Paavin of Albarti (Alberti)" - Hesperion XXI
* "Concert in Waldhaus"
* "Sonate No. 2 in D Minor" - Georg Friedrich Handel

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 