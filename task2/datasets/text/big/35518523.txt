Out of the Furnace
 
{{Infobox film
| name = Out of the Furnace
| image = Out of the Furnace Poster.jpg
| caption = Theatrical release poster Scott Cooper Michael Costigan
| writer = Brad Ingelsby Scott Cooper
| starring = Christian Bale Woody Harrelson Casey Affleck Forest Whitaker Willem Dafoe Zoe Saldana|Zoë Saldana Sam Shepard
| music = Dickon Hinchliffe
| cinematography =Masanobu Takayanagi
| editing =David Rosenbloom Appian Way Productions Scott Free Productions Relativity Media Red Granite Pictures
| distributor = Relativity Media
| released =  
| runtime = 116 minutes
| country = United States
| language = English
| budget = $22 million 
| gross = $15.4 million 
}}
 thriller film, Scott Cooper, from a screenplay written by Cooper and Brad Ingelsby. Produced by Ridley Scott and Leonardo DiCaprio, the film stars Christian Bale, Casey Affleck, Woody Harrelson, Zoe Saldana, Forest Whitaker, Willem Dafoe and Sam Shepard. The film received a limited release in Los Angeles and New York City on December 4, 2013, followed by a wide theatrical release on December 6.

==Plot==
After getting off work at a North Braddock, Pennsylvania steel mill, Russell Baze (Christian Bale) catches his brother Rodney (Casey Affleck) at a horse racing simulcast where Rodney had just bet on a losing horse.  Rodney reveals John Petty (Willem Dafoe) loaned the money to him.  Petty owns a bar and runs several illegal games.  Russell offers to pay off some of Rodneys debt, promising to pay Petty the rest with his next paycheck.  Driving home intoxicated, Russell hits another car, killing its occupants, including a little boy.  He is incarcerated for vehicular manslaughter. While in prison he is informed that his father has passed away, and that his girlfriend Lena (Zoe Saldana|Zoë Saldana) has left him for the small town police chief, Wesley Barnes (Forest Whitaker).

Upon his release from prison, Russell returns home and resumes his job at the mill. He wants Lena back but she is pregnant with Wesleys baby. Russell unsuccessfully feigns happiness to Lena, saying she will be a great mom, but both know that her pregnancy makes their getting back together an impossibility.

The same day, Rodney is shown participating in a bare-knuckle fight, which he wins. Rodney was supposed to take a dive as a way to repay some of the gambling debt he owes Petty but wins the match as he enters a state of rage from his war experiences causing him to knock out the other fighter. The next morning, Russell finds Rodneys bloodied wrist tapes in the trash and confronts him about it.  Russell wants him to work in the mill but Rodney, a four-tour Iraq veteran, is too proud and his war experiences have scarred him mentally.

Rodney tells Petty the "nickel and dime" fights will never give him enough money to pay Petty back. Rodney then insists that Petty call and organize a more lucrative fight in New Jersey. Petty reluctantly arranges a fight with Harlan DeGroat (Woody Harrelson), a sociopathic drug dealer from New Jersey to whom Petty owes a lot of money.

Rodney is told he must lose the fight. When DeGroat seeks assurances Rodney will lose, Petty promises he will. Rodney almost knocks out his opponent, but when hearing Petty pleading with him, Rodney helps the fighter get up and then proceeds to let the man pummel his face.

After the fight, Petty tells DeGroat that this fight made them even, reminding him that was their deal. DeGroat drops the subject. While driving back home, Petty and Rodney are ambushed by DeGroat and his men. DeGroat first shoots and kills Petty, and after having Rodney dragged into the woods, kills him as well. Unbeknownst to anyone, when reaching into his pocket for a rag that he gave to Rodney, Petty accidentally dialed his cell phone and it fell onto the car seat. The call connected to his bartender Dans voice mail and provided proof that DeGroat was the killer.

That night, Russell finds a letter from Rodney, stating that this will be his last fight and that he wants to work with Russell at the mill. Wesley informs Russell about his brothers disappearance and Russell sets off to find him. Arriving in DeGroats town, he is escorted out by the sheriff and informed that DeGroat has a large following and is not easily apprehendable.

Upon returning to the mill, Russell is visited by Wesley who confirms Rodneys death. Russell goes to Pettys office, finds a phone number for DeGroat and calls him without identifying himself, enticing him to come collect Pettys debt. At the bar, Russell sabotages DeGroats van to prevent escape and confronts him.  DeGroat is able to escape to a nearby shutdown mill where Russell shoots him in the thigh. Russell then follows DeGroat as he hobbles off and shoots him in the back. Russell informs DeGroat that he is Rodneys brother as Wesley approaches the mill. Wesley pleads for Russell to put down his gun but Russell proceeds to carefully aim his hunting rifle and shoots DeGroat in the head.

The film cuts to Russell sitting at home at the dining table, implying that he faced no criminal prosecution for what happened, and the film fades to black.

==Cast==
 
 
 
*Christian Bale as Russell Baze
*Woody Harrelson as Harlan DeGroat
*Casey Affleck as Rodney Baze Jr.
*Forest Whitaker as Wesley Barnes
*Willem Dafoe as John Petty Tom Bower as Dan Dugan
 
*Zoe Saldana|Zoë Saldana as Lena Taylor
 
*Sam Shepard as Gerald "Red" Baze

==Production==
"This is a man who is battling his soul and living with the consequences of violence. When he went to kill Harlan DeGroat, he thought he could have gone to prison or be killed by the police. He never thought he would get off and that the sheriff would ultimately say, Let me make this right. And he does," Cooper said of the finale of his film, which takes place at an undated point after Russell has murdered DeGroat. "This is a man who, whether he is prison or not, hes in prison for the rest of his life. Hopefully he will find peace and contentment at some point. Im a very optimistic person, I hope that, ultimately, Russell Baze does find that." 

===Development=== Scott Cooper John Fetterman. Thomas Bell, set in Braddock.    The Hollywood Reporter reported the films budget to be $22 million.   

===Filming=== North Braddock, Kodak 35 Northern Panhandle State Penitentiary Beaver County, Independence Township John Fetterman, who has the same design on his arm. 

===Music===
The musical score to Out of the Furnace was composed by Dickon Hinchliffe. Originally, it was announced that Alberto Iglesias had reached an agreement to compose the score for the film.  However, Hinchliffe later took over scoring duties.  Pearl Jam frontman Eddie Vedder also recorded a new song for the film.    A soundtrack album featuring Hinchliffes score was released digitally on December 3, 2013 by Relativity Music Group. 

==Release==
The film premiered at the TCL Chinese Theatre on November 9, 2013 in Hollywood|Hollywood, California as part of the American Film Institutes AFI Fest.  It received a limited release in Los Angeles and New York City on December 4, 2013, followed by a wide theatrical release in the U.S. on December 6.  Director Scott Cooper won the award for best first or second film for Out of the Furnace at the 2013 Rome Film Festival. 

The Pearl Jam song "Release" is featured during the opening title and features in a newly recorded edition during the end credits.

==Reception==

===Critical response===
Out of the Furnace has received generally mixed  reviews. On  , based on 40 critics. 

===Box office===
Out of the Furnace was the only new film to receive a  , which had $27 million in ticket sales that weekend. Relativity Media had pre-sold the film to foreign distributors for $16 million, which offset its costs.   

===Lawsuit===
Town officials from Mahwah, New Jersey urged a boycott of the film due to negative depictions of the Ramapough Mountain Indians, an indigenous people living around the Ramapo Mountains  characterizing the film as a hate crime.  Relativity Media responded that the film "is not based on any one person or group" and is "entirely fictional".  Nine members of the group, eight of whom have the surname of the movies lead character, DeGroat, filed suit against the makers and other involved parties, claiming that Out of the Furnace portrays a gang of "inbreds" living in the Ramapo Mountains who are "lawless, drug-addicted, impoverished and violent."  The lawsuit asserts that "The Defendants, and each of them, knew or should have known that their actions would place  Plaintiffs, and/or any person so situated in a false light." The suit continues that "The connection between the ethnic slur of Jackson Whites, with the location of the Ramapo Mountains of New Jersey, with a Bergen County Police patrol car, with the surnames DeGroat and Van Dunk, is too specific to the Ramapough plaintiffs to be chance, coincidence or happenstance, and implies an element of knowledge on the part of the Defendants, or some of them." 

On May 16, 2014, U.S. District Court Judge William Walls, sitting in Newark, New Jersey dismissed the lawsuit, saying that the film did not refer directly to any of the plaintiffs. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 