The Empty Home
 
{{Infobox film
| name           = The Empty Home
| image          = 
| caption        = 
| director       = Nurbek Egen
| producer       = 
| writer         = Ekaterina Tirdatova
| starring       = Maral Koichukaraeva
| music          = 
| cinematography = Dmitry Ermakov
| editing        = 
| distributor    = 
| released       =  
| runtime        = 98 minutes
| country        = Kyrgyzstan
| language       = Kyrgyz
| budget         = 
}}
 Best Foreign Language Oscar at the 85th Academy Awards, but it did not make the final shortlist.     

==Cast==
* Maral Koichukaraeva as Ascel
* Cecile Plage as Virginie
* Atai Omurbekov as Marat
* Asan Amanov as Tynchtyk
* Bolot Tentimyshov as Sultan
* Denis Sukhanov as Arkady
* Kseniya Lavrova-Glinka as Masha
* Françoise Michaud as Virginies mother
* Maxim Glotov as Igor Roman Nesterenko as Victor
* Michele Levieux as Midwife

==See also==
* List of submissions to the 85th Academy Awards for Best Foreign Language Film
* List of Kyrgyz submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 