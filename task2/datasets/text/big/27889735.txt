The Heritage of the Desert
:For the 1932 remake directed by Henry Hathaway, see Heritage of the Desert.
{{Infobox film
| title          = Heritage of the Desert (1924)
| image          = The-Heritage-of-the-Desert-1924.jpg
| caption        = Lobby card
| director       = Irvin Willat
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Zane Grey (novel) Albert S. LeVino (adaptation)
| starring       = Bebe Daniels Ernest Torrence Noah Beery
| cinematography = Charles Schoenbaum
| editing        = Howard Hawks
| distributor    = Paramount Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States English intertitles
| budget         =
| gross          =
}}
 Western film based on the novel by Zane Grey, and starring Bebe Daniels, Ernest Torrence, and Noah Beery.

The film was released by Paramount Pictures with sequences filmed in an early Technicolor process. A print is reportedly preserved in the Gosfilmofond archive in Moscow.  

==Cast==
*Bebe Daniels - Mescal
*Ernest Torrence - August Naab Noah Beery - Holderness
*Lloyd Hughes - Jack Hare
*Anne Schaefer - Mrs. Naab (*as Anne Schaeffer) Jim Mason - Snap Naab(as James Mason)
*Richard Neill - Dene (*as Richard R. Neill)
*Tom London - Dave Naab (*as Leonard Clapham)

==See also==
*List of early color feature films
*List of lost films

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 


 
 