Hero: The Superstar
{{Infobox film
| name           = Hero The Superstar
| image          = Hero The Superstar.jpg
| caption        = Theatrical poster
| director       = Bodiul Alam Khokon
| producer       = Shakib Khan
| writer         = Kasem Ali Dulal
| starring       = Shakib Khan   Eamin Haque Bobby   Apu Biswas   Amit Hasan   Misha Sawdagor
| music          = Ali Akram Shuvo   Arfin Rumi  Showkat Ali Emon Rebel by Raghava Lawrence  Naayak by V. V. Vinayak
| cinematography = 
| editing        =   SK Films
| distributor    = SK Films
| runtime        = 160 minutes
| released       =  
| country        = Bangladesh
| language       = Bengali
| budget         =
| gross          = 
}}

Hero The Superstar is an   of 29 July 2014 in 120 screens which was the most wide release for any Bangladeshi film 
The film broke several records on its opening days in Bangladesh and currently is the highest grossing film of Dhallywood. 

==Plot==
Hira (Shakib Khan) is a software engineer by profession and very candid at heart. He is smitten by Priya(Apu Biswas) and he falls in love with her. Salma Khan(Nothun), Priyas Sister and a local rowdy, comes to learn about Hira, and wishes to kill him.

Salma Khan puts her men on Hiras trail and arrives at a place where they plan to kill him, but things dont go as planned. Salma Khan witnesses the killing of a senior police official by Hira, only to return home in complete shock and fear. Puzzled at the sight of the murder, Salma Khan sends her entourage to investigate about Hira.

Meanwhile, a CBI officer(Shiba Shanu) comes barging into Salma Khans residence and takes her into custody, where she is told that Hira is wanted for the murder of several rowdies much to the astonishment of everybody. Who is Hira and what prompted him to kill? This forms the rest of the story.

==Cast==
* Shakib Khan as Hero/Hira
* Eamin Haque Bobby as Bobby
* Apu Biswas as Priya
* Amit Hasan as Billa/Dollar
* Uzzol as Heros Father 
* Misha Sawdagor as Jafor Zoardar/David 
* Nothun as Salma Khan
* Shiba Shanu as CID Officer

==Music==
{{Infobox album
| Name = Hero: The Superstar
| Type = Soundtrack
| Artist =Ali Akram Shuvo Arfin Rumi Showkat Ali Emon
| Cover = Hero- The Superstar.jpg
| Border =
| Alt =
| Caption =
| Released = 
| Recorded = 2014
| Genre = Film soundtrack
| Length =  Bengali
| Label = Tiger Media
| Producer =Shakib Khan
}}
The soundtrack of Hero: The Superstar composed by Ali Akram Shuvo, Arfin Rumi and Showkat Ali Emon with the lyrics penned by Sudip Kumar Dip & Kobir Bokul. The soundtrack features 5 tracks overall. 
{{track listing
| headline=Hero: The Superstar Album: Track listing
| extra_column=Singer(s)
| music_credits=yes
| lyrics_credits=no
| total_length = 21:43
| title1         = Hero The Super Star
| extra1         = S I Tutul & Pulok
| music1         = Ali Akram Shuvo
| length1        = 4:18
| title2         = Jekhne Jabe Amake Pabe
| extra2         = Arfin Rumey & Naumi
| music2         = Arfin Rumey
| length2        = 5:02
| title3         = Dhake Tor Mayibe Hasy
| extra3         = Asif & Mimi
| music3         = Ali Akram Shuvo
| length3        = 3:54
| title4         = I Love You Hero 
| extra4         =  Polash, Tanjna Ruma and Moon
| music4         = Ali Akram Shuvo
| length4        = 3:57
| title5         = Tor Sate Badeci Mon
| extra5         = Rupom & Ruma
| music5         = Showkat Ali Emon
| length5        = 4:32
}}

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 



 
 
 