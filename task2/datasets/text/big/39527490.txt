The Crowded Sky
{{Infobox film
| name           = The Crowded Sky
| image          = Crowded sky.jpg
| image size     = 200px
| caption        = Theatrical poster
| director       = Joseph Pevney Michael Garrison
| screenplay     =  Charles Schnee
| based on       = The Crowded Sky novel by Hank Searls 
| starring       = Dana Andrews Rhonda Fleming Efrem Zimbalist Jr.
| music          = Leonard Rosenman
| cinematography = Harry Stradling Sr.
| editing        = Tom McAdoo
| studio         = Warner Bros
| distributor    = Warner Bros.
| released       =  
| runtime        = 105 minutes
| country        = United States English
}}
 drama film directed by Joseph Pevney, starring Dana Andrews, Rhonda Fleming and Efrem Zimbalist Jr.  The film is based on the novel of the same name by Hank Searls. 

The Crowded Sky follows the back stories of the crew and passengers of a US Navy jet and a commercial airliner carrying a full load of passengers during a bout of severe weather. Other issues such as a malfunctioning radio make it nearly impossible to communicate with air traffic control and sets the two aircraft on a collision course. Nixon, Rob.   Turner Classic Movies. Retrieved: August 26, 2014. 

==Plot== Lockheed TV-2 jet piloted by Captain Dale Heath (Efrem Zimbalist Jr.), with an enlisted man (Troy Donahue) as a passenger, runs into trouble as soon as it is in the air. Both Heaths radio and his navigation system become disabled, with no way to correctly determine their altitude. At the same time, a Douglas DC-7 airliner piloted by veteran Dick Barnett (Dana Andrews), is carrying a full load of passengers, each with their own worries and problems to deal with. 
 John Kerr), who has his own demons, including his relationship with his father and an affair with head stewardess Kitty Foster (Anne Francis).

Both aircraft, through various errors in their flight path, are on a collision course that air traffic controllers on the ground are unable to avert. When the crash occurs, Heath sacrifices himself and his passenger, making amends for a past tragedy he had caused. The airliner is badly damaged with Louis Capelli (Joe Mantell), the flight engineer, being sucked out of the aircraft to his death, and the rest of the passengers and crew fighting for their lives. Even with one engine destroyed and a wing on fire, Barnett brings the airliner down safely, but accepts responsibility for the collision during the accident investigation. In the aftermath of the crash, Mike and Kitty are not only survivors but are also planning a future life together.

==Cast==
  
* Dana Andrews as Dick Barnett
* Rhonda Fleming as Cheryl Heath
* Efrem Zimbalist Jr. as Dale Heath John Kerr as Mike Rule
* Anne Francis as Kitty Foster
* Keenan Wynn as Nick Hyland
* Troy Donahue as McVey
* Joe Mantell as Louis Capelli
 
* Patsy Kelly as Gertrude Ross
* Donald May as Norm Coster
* Louis Quinn as Sidney Schreiber
* Ed Kemmer as Caesar (as Edward Kemmer)
* Tom Gilson as Rob Fermi
* Hollis Irving as Beatrice Wiley
* Paul Genge as Samuel N. Poole
* Nan Leslie as Bev
 
==Production==
 
Film rights to the novel The Crowded Sky - written by a former US Navy flyer - were sold before it was even published.   The screenplay extensively employs the device of characters thinking aloud. Screenwriter Charles Schnee made this decision because he felt audiences at the time required more subtlety in characterization and that more dialogue helped provide that. 
 Broadway in Bridge of San Luis Rey of the air".  

In order to prepare for his role, Zimbalist trained for 20 hours in a jet flight simulator to familiarize himself with the controls that his character would use.  For Troy Donahue, his role was a departure from the usual "teen heartthrob" films he had made for Warner Bros.  Principal photography took place from mid-October to mid-November 1959. 

==Reception==
The Crowded Sky was received by audience and critics alike with mixed reviews. As a progenitor of the disaster films of the 1970s, it had some of the elements of the genre, but relied heavily on dialogue to the detriment of the impact of an aerial disaster. The Los Angeles Times called it "interesting but uneven." Variety (magazine)|Variety gave a mixed review upon the films release, criticizing Pevneys directing, but praising the aerial scenes. 

In a more critical review in The New York Times, Eugene Archer called The Crowded Sky "reprehensible" as it exploited human tragedy. His review noted, "Possibly a meaningful film could be developed from this theme, but as directed with an emphasis on sensationalism by Joseph Pevney, the effect is as meretricious as it is harrowing." 

Later reviews were more favourable. Glenn Erickson in DVD Talk gave a mostly positive review, but commented that The Crowded Sky came off more as an "unintentional comedy" than a serious drama film.  Reviewer Leonard Maltin called it a "slick film focusing on emotional problems aboard jet liner and Navy plane bound for fateful collision; superficial but diverting." 

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Maltin, Leonard.  Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 0-525-93635-1.
* Maltin, Leonard. Leonard Maltins Movie Guide 2009. New York: New American Library, 2009 (originally published as TV Movies, then Leonard Maltin’s Movie & Video Guide), First edition 1969, published annually since 1988. ISBN 978-0-451-22468-2.
 

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 