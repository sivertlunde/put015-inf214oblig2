The Condemned Village
{{Infobox film
| name           = Das verurteilte Dorf
| image          = 
| image size     =
| caption        =
| director       = Martin Hellberg
| producer       =Adolf Fischer
| writer         = Jeanne Stern Kurt Stern 
| narrator       =
| starring       = Helga Göring
| music          = Ernst Roters
| cinematography =Karl Plintzner, Joachim Hasler 
| editing        =Johanna Rosinski
| studio    = DEFA
| distributor    = PROGRESS-Film Verleih
| released       =  
| runtime        = 107 minutes
| country        = East Germany
| language       = German
| budget         =
| gross          =
}} East German propaganda film directed by Martin Hellberg. It was released in List of East German films|1952.

==Plot==
Farmer Heinz Weimann returns to his small Bavarian village of Bärenweiler after several years in Soviet captivity. He tells his neighbors, who have been subject to anti-Soviet propaganda disseminated by the Nazis and the Americans, that the Soviets have treated him well. His old sweetheart Käthe has married another man, Fritz Vollmer, but he is not concerned with that. His joy on returning home is interrupted when the mayor announces that the American Army intends to destroy the village and to build an airfield on its lands, in preparation for a confrontation with the Soviet Union. 

The people turn to the government and to the local bishop, but receive no assistance. Led by Heinz, they turn to peaceful protests. All residents refuse to leave their homes, except Vollmer. Heinz is arrested and imprisoned. Trade unions from throughout the Federal Republic of Germany mobilize to aid the villagers. When the United States Military Police arrives to evict the inhabitants, thousands of workers arrive in Bärenweiler, and the Americans are forced to leave and abandon their plans to build the airfield.

==Cast==
*Helga Göring as Käthe Vollmer
*Günther Simon as Heinz Weimann
*Wolf Kaiser as American colonel
*Hans Finohr as American general
*Eduard von Winterstein as priest
*Albert Garbe as mayor
*Marga Legal as Mrs. Rühling
*Albert Doerner as Fritz Vollmer
*Charlotte Crusius as Mrs. Weimann
*Otto Eduard Stübler as Meisel
*Friedrich Gnaß as Scheffler
*Ulrich von der Trenck as teacher
*Aribert Grimmer as Riebnitz
*Helmuth Hinzelmann as minister
*Paul Paulsen as Bishop
*Heinz Dhein as Klaus Meitner
*Hermann Stövesand as Anton Reinhard
*William Gade as postman
*Werner Pledath as director
*Josef Peter Dornseif as second director
*Albert Venohr as American captain
*Heinz Rosenthal as government councilor

==Production==
In 1951, the state control over the DEFA film studio was tightened, as manifested in the creation of the DEFA commission in the Socialist Unity Party of Germanys Politburo. On the backrougnd of the nascent Cold War, an emphasis was put on the creation of anti-Western films; all the six pictures released by DEFA in 1952 were dedicated to this theme.  

Writers Jeanne and Kurt Stern wrote the draft of the script in early 1951, after reading a newspaper report about a protest against American military presence that took place in the West German village of Hammelburg. The draft was submitted to DEFA on 14 March 1951. The National Film Board dubbed it "a remarkable agitational work in our campaign against re-militarization, for the unity of Germany and for peace." The final version was completed on 16 May; the writers took care not to highlight the importance of communism but rather, the demand for peace. a positive figure of a cleric, the villages priest, was included in the plot; DEFA director-general Sepp Schwab decided that it would be unwise to portray the church in a wholly negative light. A happy ending was added, as well. In the original draft, the village was evicted.   on the German institute for Church and State Studies website. 

The SED considered the film as one of the most important  cinematic projects produced during 1951. The partys DEFA Commission praised the script as "one of the best written this year." State Secretary of Press and Agitation Hermann Axen had personally made many adjustments to the plot, and demanded that the Americans would be presented as aggressors. Dagmar Schittly. Zwischen Regie und Regime. Die Filmpolitik der SED im Spiegel der DEFA-Produktionen. ISBN 978-3-86153-262-0. p. 64. 

Two directors who were approached by DEFA -  Erwin Wilhelm Fiedler and Falk Harnack - declined to work on the film. Eventually, the manager of the Dresden Theater, Martin Hellberg, who had no experience in the field of cinema, was selected to direct The Condemned Village. Principal Photography began on 28 August 1951 and ended in early December. 

==Reception== National Prize, Gold Medal in 1953, as well. 

Authors Antonin and Miera Liehm regarded the film as "one of the pinnacles of the propaganda art of its time" that managed to "circumvent the complete artificiality" of the Soviet pictures based on similar themes. Miera Liehm, Antonin J. Liehm . The Most Important Art: Soviet and Eastern European Film After 1945. ISBN 0-520-04128-3. p. 91.  Melvyn P. Leffler and Odd Arne Westad cited it as one of the Cold War films that presented the American troops in West Germany as oppressors.  
 classical German "homeland" pictures - depicting the pastoral countryside, and demonstrating the threat levied against it by the Americans - they were used only as a pretext to resist any Western military presence in the Federal Republic.   pp. 265-267.  This manipulation of the genre was noted also by Johannes von Moltke, who claimed that the film presented the "politicization of the homeland film."  Caute also pointed out that the characterization of the villains was in line with the East German political line: the Americans helpers were the local bishop and an aristocrat who fled the GDR after his lands were nationalized and handed over to his former serfs. 

During 1952, the rejection of the  . The film now inspired resistance to the evictions; in one settlement about to be leveled down, a man was sentenced to six years in prison after exhorting his neighbors to "act as the protesters in The Condemned Village had." Due to those events, the picture was removed from circulation in May 1953. 

==References==
 

==External links==
*  
*  original poster on ostfilm.de.

 
 
 
 
 
 