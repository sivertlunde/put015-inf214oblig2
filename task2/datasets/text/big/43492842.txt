Gayathri Maduve
{{Infobox film
| name           = Gayathri Maduve
| image          =
| image_size     =
| caption        =
| director       = B Mallesh
| producer       = Lakshman
| writer         = B Mallesh
| screenplay     = B Mallesh Ambika Roopadevi Tiger Prabhakar
| music          = Rajan-Nagendra
| cinematography = B C Gowrishankar
| editing        = P Bhakthavathsalam
| studio         = Gayathri Art Film
| distributor    =
| released       =  
| country        = India Kannada
}}
 1983 Cinema Indian Kannada Kannada film,  directed by  B Mallesh and produced by Lakshman The film stars Anant Nag, Ambika (actress)|Ambika, Roopadevi and Tiger Prabhakar in lead roles. The film had musical score by Rajan-Nagendra. 

==Cast==
 
*Anant Nag Ambika
*Roopadevi
*Tiger Prabhakar
*Jayamalini
*K. S. Ashwath
*Vajramuni
*Dinesh
*Badada Hoo Prathap
*Aravind
*Suresh S
*Shivaprakash
*Bheema Rao
*Mysore Lokesh
*Siddartha
*Shivalingaraju
*Pranavamurthy
*B Mallesh
*Shashikala
*Shantha Ramesh
*Nandini S Pal
*Mangala Gowri
*Latha
 

==Soundtrack==
The music was composed by Rajan-Nagendra. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ninna Jaathaka || S. Janaki|Janaki, S. P. Balasubrahmanyam || Chi. Udaya Shankar ||
|- Janaki || R. N. Jayagopal ||
|- Janaki || Chi. Udaya Shankar ||
|- Janaki || Chi. Udaya Shankar ||
|}

==References==
 

 
 
 
 


 