Aitbaar
 
{{Infobox film
| name           = Aitbaar
| image          = Aitbaarfilm80.jpg
| image_size     =
| caption        = 
| director       = Mukul Anand
| producer       = Romesh Sharma
| writer         =  Vinay Shukla
| narrator       =
| starring       = Dimple Kapadia Raj Babbar Suresh Oberoi
| music          = Bappi Lahiri Hasan Kamaal (lyrics) Farukh Kaiser (lyrics)
| cinematography = Pravin Bhatt
| editing        =
| distributor    =
| released       = 22 March 1985
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}} 1985 Bollywood|Hindi film, directed by Mukul Anand and starring Raj Babbar, Dimple Kapadia, Suresh Oberoi, Danny Denzongpa and Sharat Saxena. It was a remake of Alfred Hitchcocks 1954 thriller, Dial M For Murder.

Films music is by Bappi Lahiri and lyrics by Hasan Kamaal and Farukh Kaiser, who gave Kisi Nazar Ko Tera Intezaar, a memorable ghazal duet by Asha Bhosle and Bhupinder. 

==Differences with the Original==
Raj Babbar plays the husband, Dimple Kapadia is the wife having an affair with Suresh Oberois character. Sharat Saxena plays the assassin while Danny Denzongpa is an Inspector. While Mark Halliday is a crime writer, Suresh Oberois character is a Ghazal singer. In the original film, when Tony Wendice realizes that he has been cornered, he accepts his crime gracefully, offering drinks to all around; in the Hindi remake, Raj Babbar attacks the police officer and everyone around, but is finally cornered.

==Plot==
Neha is the only daughter of wealthy and widowed Mr. Khanna, and he would like to see her married and well settled before he passes on. Neha is in love with Sagar, who is not prepared for marriage, so she marries the man of her dads choice, a tennis player, Jaideep. Soon after her marriage, her dad passes away, and she finds that Jaideep does not love her anymore, and is, in fact, very abusive toward her. This leads her back to Sagars arms, who is now a successful singer.

Then Neha notices a remarkable change in Jaideeps behavior, he abstains from alcohol, starts taking an interest in her dads business, appears apologetic for his past abusive behavior, and adores her. Then her life turns upside down when she receives a letter from a blackmailer, asking her to pay   1 lakh or a love letter written to her by Sagar will be mailed to Jaideep. Neha delivers the money, but is unable to get the letter back. She confides about this to Sagar. While Sagar and Jaideep are out at a stag party, Nehas house is broken into, and a man named Vikramjit attempts to kill her, but instead ends up getting killed by her.

The Police, summoned by Jaideep, find the letter from Sagar in Vikramjits pocket. As a result Neha is arrested, tried in court, and sentenced to death under section 302 of the Indian Penal Code. Eventually, Neha is proved innocent and Jaideep is found guilty. He commits suicide unable to bear his defeat.

==Cast==
* Raj Babbar - Jaideep
* Dimple Kapadia - Neha Khanna
* Suresh Oberoi - Sagar
* Danny Denzongpa - Inspector Barua
* Sharat Saxena - Vikramjit Vicky
* Anupam Kher - Public Prosecutor

==References==
 

==External links==
*  
*  

 
 
 
 