Carefree (film)
{{Infobox film
| name           = Carefree 
| image          = Carefree poster.jpg
| image_size     = 185px
| caption        = theatrical release poster
| producer       = Pandro S. Berman 
| director       = Mark Sandrich 
| writer         = Original idea: Marian Ainslee     Ernest Pagano
| starring       = Fred Astaire Ginger Rogers
| music          = Irving Berlin (songs) Victor Baravalle (score)
| cinematography = Robert De Grasse William Hamilton
| distributor    = RKO Radio Pictures 
| released       = September 2, 1938
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $1,253,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p41 
| gross          = $1,731,000 
}}
 screwball comedies of the period, Carefree is the shortest of the Astaire-Rogers films, featuring only four musical numbers. Carefree is often remembered as the film in which Astaire and Rogers shared a long on-screen kiss at the conclusion of their dance to "I Used to Be Color Blind," all previous kisses having been either quick pecks or simply implied.  
 Shall We Dance and six other previous RKO pictures. The next film in the series, The Story of Vernon and Irene Castle (1939), would be their final RKO film together, although they would reunite in 1949 for MGMs The Barkleys of Broadway.

 

==Plot==
 
Psychiatrist Dr. Tony Flagg (Fred Astaire) does his friend Stephen Arden (Ralph Bellamy) a favor by taking on his fiancee, Amanda Cooper (Ginger Rogers), as a patient. Amanda, a singer on the radio, cant seem to make a decision about Stephens many proposals of marriage, so Tony probes her subconscious, but in the process Amanda falls in love with him. He brings her interest back to Stephen with hypnosis, but then realizes that he also loves her and tries to hypnotize her again, leading to conflict with Stephen. Five minutes away from the wedding, he breaks into her dressing room with his assistant Connors (Jack Carson) and gets the chance to talk to her subconscious again when Stephen accidentally hits her instead of Tony. In the end, the two marry, much to the surprise of the guests.

==Cast==
* Fred Astaire as Tony Flagg
* Ginger Rogers as Amanda Cooper
* Ralph Bellamy as Stephen Arden
* Luella Gear as Aunt Cora
* Jack Carson as Thomas Connors
* Clarence Kolb as Judge Joe Travers
* Franklin Pangborn as Roland Hunter
* Walter Kingsford as Dr. Powers
* Kay Sutton as Miss Adams

;Cast notes
* Hattie McDaniel appears briefly as a maid
* RKO borrowed Ralph Bellamy from Columbia Pictures for this film. 

==Production==
Carefree was in production from 14–15 April 1938 (the golf-ball number) and from 9 May to 21 July.   Location filming was done at Busch Gardens in Pasadena, California,  and at the Columbia Ranch. 

The film was supposed to be filmed in Technicolor, but the extra cost per foot was too much. 

Astaire didnt like "mushy love scenes," and preferred that lovemaking between him and Rogers be confined to their dances. Because rumors sprang up that Astaires wife wouldnt let him kiss onscreen, or that Rogers and Astaire didnt like each other, Astaire agreed to the long kiss at the end of "I Used to Be Color Blind", "to make up for all the kisses I had not given Ginger for all those years." Margarita Landazuri   

Besides the number ""Lets Make the Most of Our Dream," another scene that was dropped from the released film was one where Astaire tries to analyze a scatter-brained patient, played by Grace Hayle. IMDB   
 Shall We Dance, had been released in May 1937,  and the 16 month gap between the films was the longest between Astaire-Rogers films to that date. TCM   

==Songs==
The songs in Carefree were all written by Irving Berlin,  and with the exception of "Change Partners," which he had written for Astaire and Rogers years before, he wrote them all over the course of a few days, while on vacation in Phoenix, Arizona.    
An army of uncredited orchestrators contributed to the catchy settings of the tunes, principally among them Broadways Robert Russell Bennett and future MGM stalwart Conrad Salinger.
 Hermes Pan. {{cite book
  | last = Mueller
  | first = John
  | title = Astaire Dancing - The Musical Films
  | publisher = Hamish Hamilton
  | year = 1986
  | location = London
  | isbn = 0-241-11749-6 }}   In preparation for The Story of Vernon and Irene Castle, the Astaire-Rogers film which was already scheduled to follow Carefree, the choreography for this film contains more lifts than usual. 

* "Since They Turned Loch Lomond into Swing" - Fred Astaire came up with the idea of hitting golf balls for this number, and spent two weeks rehearsing it.  It was shot three weeks before the rest of the film, with Astaire performing to a piano track &ndash; the orchestrated arrangement was added later.  Because of the difficulty of the action, the performance was pieced together from multiple takes, which was very unusual for Astaire, who preferred his dance numbers to be made from a minimum number of long takes. 

* "I Used to Be Color Blind" - The dance for this number was shot at four times normal speed to create the slow-motion effect seen when the film is shown at normal speed.

* "The Night Is Filled With Music" (instrumental) - RKO had hired Ray Hendricks to sing this song, but it was dropped from the production and survived only as an instrumental. 

* "The Yam" - Fred Astaire reportedly thought this song was silly, and refused to sing it, which is why Ginger Rogers sings it alone &mdash; although they do dance together after the vocal section.  Eventually he made a record of it, which can be heard in his collected works. 

* "Change Partners" - The only song from this film which had an afterlife, "Change Partners" was nominated for an Academy Award.

* Another number, "Lets Make the Most of Our Dream," a second dream sequence, was filmed but deleted. 

==Reception==
===Critical===
Carefree received generally mixed reviews when it was released, although the critic for the Motion Picture Herald, William R. Weaver, called it "the greatest Astaire-Rogers picture."  
===Box Office===
The film earned $1,113,000 in the US and Canada and $618,000 elsewhere, but according to RKO records still lost the studio $68,000.   It was the first Astaire and Rogers films not to show a profit upon its original release.
===Awards===
Carefree was nominated for three  ), Best Musical Scoring (Victor Baravalle) and Best Song "Change Partners", 
written by Irving Berlin. 

==Notes==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 