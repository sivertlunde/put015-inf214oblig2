Berlin: Live at St. Ann's Warehouse
{{Infobox Film
| name           = Berlin
| image          = Lou reeds berlin.jpg
| caption        = Original film poster
| director       = Julian Schnabel
| producer       = Jon Kilik Tom Sarig
| writer         = 
| starring       = Lou Reed Emmanuelle Seigner
| music          = Lou Reed
| cinematography = Ellen Kuras
| editing        = Benjamin Flaherty
| distributor    = Third Rail Releasing
| released       =  
| runtime        = 81 minutes
| country        = United States United Kingdom
| language       = English
| budget         = 
| gross          =  $114,860 
}}

{{Infobox album |  
  Name        = Berlin: Live At St. Anns Warehouse |
  Type        = Live album |
  Artist      = Lou Reed |
  Cover       = Lou reed berlin live cd cover.jpg  |
  Released    = November 4, 2008 |
  Recorded    = December 15–16, 2006 |
  Genre       = Rock music|Rock|
  Length      = 79:30 |
  Label       = Matador Records |
  Producer    = Bob Ezrin, Hal Willner |
  Last album  = Hudson River Wind Meditations (2007) |
  This album  = Berlin: Live At St. Anns Warehouse (2008) |
  Next album  = The Creation of the Universe (2008) |
|}}
{{Album ratings
| rev1      = Allmusic
| rev1Score =    
| rev2      = Rolling Stone
| rev2Score =   
| rev3      = Spectrum Culture
| rev3Score = (3.5/5.0)  
| rev4      = Pitchfork Media
| rev4Score = (7.2/10) 
}} Brooklyn during Lola Schnabel.
 original album was a critical and commercial flop. Individual songs had been played, but not the whole thing. 
 Candy Says", Rock Minuet" and "Sweet Jane".

==Track listing==
All tracks composed by Lou Reed

# "Intro" – 1:51
# "Berlin" – 2:34
# "Lady Day" – 4:12
# "Men Of Good Fortune" – 6:35
# "Caroline Says (I)" – 4:31
# "How Do You Think It Feels?" – 5:37
# "Oh, Jim" – 8:16)
# "Caroline Says (II)" – 4:33
# "The Kids" – 8:08
# "The Bed" – 5:59
# "Sad Song" – 8:21
# "Candy Says" – 6:04
# "Rock Minuet" – 7:18
# "Sweet Jane" – 5:31

==Personnel==
*Lou Reed – lead vocals, guitar
*Steve Hunter – guitars
*Fernando Saunders – bass guitar, synthesizer, guitar, backing vocals
*Tony "Thunder" Smith – drums, backing vocals
*Rupert Christie – keyboards, backing vocals
*Rob Wasserman – double bass Sharon Jones – vocals
*Antony Hegarty – vocals Steven Bernstein – flugelhorn, trumpet
*Curtis Fowlkes – trombone Paul Shapiro – saxophone, flute
*Doug Wieselman – bass clarinet, clarinet
*David Gold – viola
*Eyvind Kang – viola
*Jane Scarpantoni – cello
*Brooklyn Youth Chorus – choir
*Bob Ezrin - conductor

==References==
  

== External links ==
*  
*  
*  
*   at setlist.fm

 
 

 
 
 
 
 
 
 
 
 
 
 
 