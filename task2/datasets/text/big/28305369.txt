The Price of a Song
{{Infobox film
| name           = The Price of a Song
| image          = priceofasong.jpg
| image_size     =
| caption        = Production still of Marjorie Corbett and Gerald Fielding in The Price of a Song
| director       = Michael Powell
| producer       = Michael Powell
| writer         = Michael Barringer
| starring       = Campbell Gullan Marjorie Corbett Gerald Fielding
| music          = 
| cinematography = James Wilson
| editing        = 
| distributor    = Fox British
| released       =  
| runtime        = 67 minutes
| country        = United Kingdom
| language       = English
}}

The Price of a Song (1935) is a British crime film, directed by Michael Powell.  It is one of 23 quota quickies Powell directed between 1931 and 1936.    I features a largely forgotten cast &ndash; only Felix Aylmer, here in a minor role, would go on to a significant film career.

==Preservation status== 75 Most Wanted" list of missing British films. 

==Plot==
Impecunious bookmakers clerk Arnold Grierson, seeing a way to easy money, forces his daughter Margaret to marry wealthy but obnoxious songwriter Nevern, ignoring her romance with local newspaper editor Michael Hardwick.  Soon after the wedding, Grierson requests the loan of a significant sum of money from Nevern and is furious and humiliated to be flatly turned down.  He begins to make elaborate plans to murder Nevern on the assumption that Margaret will then inherit her husbands estate.  Meanwhile the desperately unhappy Margaret has rekindled her relationship with Hardwick.  Nevern finds them in a café together and causes a public scene.  Margaret determines that her only course of action is to divorce Nevern, a prospect which horrifies her father.

Nevern is in the process of composing a new song, and lodges a draft manuscript with his publisher.  Making sure he has set up a foolproof alibi, Grierson goes to Neverns house and kills him as he is finalising his new composition.  As he leaves through one door, Hardwick, intending to ask Nevern to divorce Margaret, arrives through another.  Hardwick finds the body and alerts the police, who in the circumstances do not believe his story and arrest him on suspicion of murder.

The interested parties later gather at Neverns home to hear the reading of the will.  Margaret is declared the sole inheritor of all her husbands money and assets, to the delight of her father.  He is so happy that he begins to whistle, and gives himself away because it is Neverns finished composition, which he could only have heard by being in the house on the night of the murder.

==Cast==
* Campbell Gullan as Arnold Grierson
* Marjorie Corbett as Margaret Nevern
* Gerald Fielding as Michael Hardwick
* Dora Barton as Letty Grierson
* Eric Maturin as Nevern
* Charles Mortimer as Oliver Bloom
* Oriel Ross as Elsie
* Henry Caine as Stringer
* Sybil Grove as Mrs. Bancroft
* Felix Aylmer as Graham

==References==
 

== External links ==
*  , with extensive notes
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 