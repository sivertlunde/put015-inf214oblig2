Mattoru Seetha
{{Infobox film
| name           = Mattoru Seetha
| image          = Mattoru Seetha.jpg
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = P. Bhaskaran
| producer       = A. G. Films
| writer         = K. Viswanathan Sreekumaran Thampi
| narrator       =
| starring       = Kamal Haasan Roja Ramani
| music          = V. Dakshinamoorthy
| cinematography =
| editing        =
| distributor    =
| released       = November 27, 1975
| runtime        =
| country        = India
| language       = Malayalam
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
 Indian feature Vincent and M. G. Soman. 

==Remake== Tamil version.

==Cast==
  released in 1976]]
*Kamal Haasan
*Roja Ramani Vincent
*M. G. Soman
*Sheela
*Adoor Bhasi
*Bahadoor
*Jose Prakash
*Prema Menon
* Sreelatha Namboothiri Master Raghu
*Kaviyoor Ponnamma
*Sukumari
*Paul Vengola

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by P. Bhaskaran. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eechayum Poochayum || Jayashree || P. Bhaskaran ||
|-
| 2 || Kaamini Mouliyaam || P. Leela, Ayiroor Sadasivan, T. R. Omana || P. Bhaskaran ||
|-
| 3 || Kattakkattakkayarittu || Ambili, Chorus || P. Bhaskaran ||
|-
| 4 || Muttathoru Panthal || K. J. Yesudas, P Susheela || P. Bhaskaran ||
|-
| 5 || Thattaamburathunni || Jayashree || P. Bhaskaran ||
|-
| 6 || Udayathaaraka || P. Leela, Ayiroor Sadasivan, T. R. Omana || P. Bhaskaran ||
|}

==References==
 

==External links==
*  

 
 
 
 


 