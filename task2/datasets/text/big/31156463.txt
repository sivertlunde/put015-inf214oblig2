Pachanottukal
{{Infobox film 
| name           = Pachanottukal
| image          =
| caption        =
| director       = AB Raj
| producer       = KP Kottarakkara
| writer         = Muttathu Varkey KP Kottarakkara (dialogues)
| screenplay     = Muttathu Varkey
| starring       = Prem Nazir Vijayasree Adoor Bhasi Jose Prakash
| music          = M. K. Arjunan
| cinematography = PBS Mani
| editing        = K Sankunni
| studio         = Ganesh Pictures
| distributor    = Ganesh Pictures
| released       =  
| country        = India Malayalam
}}
 1973 Cinema Indian Malayalam Malayalam film, directed by AB Raj and produced by KP Kottarakkara. The film stars Prem Nazir, Vijayasree, Adoor Bhasi and Jose Prakash in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Prem Nazir as Paulose
*Vijayasree as Leelamma
*Adoor Bhasi as Thommi Aashaan
*Jose Prakash as Mathews Muthalali Prema as Lovelamma
*Sankaradi
*Paul Vengola as Lonappi
*CR Lakshmi as Shoshamma
*Kaduvakulam Antony as Puluvan Thoma Khadeeja as Mariamma
*Kottarakkara Sreedharan Nair as Rappael
*Leela as Lalitha
*N. Govindankutty as Anthappan
*Radhamani as Monimma
*Rani Chandra as Rosey Sadhana
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Deva Divyadarshanam || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 2 || Karakaviyum Kinginiyaaru || S Janaki || Sreekumaran Thampi || 
|-
| 3 || Pachanottukal || KP Brahmanandan || Sreekumaran Thampi || 
|-
| 4 || Pandu Pandoru Sanyaasi || P. Leela, Chorus || Sreekumaran Thampi || 
|-
| 5 || Paribhavichodunna || K. J. Yesudas || Sreekumaran Thampi || 
|-
| 6 || Thaamaramotte || K. J. Yesudas, B Vasantha || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 

 