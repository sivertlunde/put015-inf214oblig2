Student Bodies
 
 
{{Infobox film
| name           = Student Bodies
| image          = Student Bodies.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = {{plainlist|
* Mickey Rose Michael Ritchie  
}} Allen Smithee)
| writer         = Mickey Rose
| starring       = {{plainlist|
* Kristen Riter
* Matt Goldsby
* Cullen Chambers
}}
| music          = Gene Hobson
| cinematography = Robert Ebinger
| editing        = Kathryn Ruth Hope
| distributor    = Paramount Pictures
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $5.2 million
}} Michael Ritchie spoof of slasher horror Friday the Prom Night., superimposed onscreen whenever a death occurs.

==Plot==
Student Bodies is about a serial killer who stalks female students at Lamab High School, while at the same time, voyeuristically watching them. The killer calls himself "The Breather," presumably because the killer is always breathing heavily.
 Friday the 13th films, he hates seeing youngsters having sexual intercourse|sex. The Breather uses many unusual objects to kill his female victims such as a paper clip, a chalkboard eraser, and a horse-head bookend.  He kills his male victims by placing them in trash bags alive.

The film itself ends with several twists: initially, it is revealed that the Principal and his elderly female assistant are working as a duo as "The Breather", even though they are shown at one point in the film in the same room as other characters when the Breather contacts the school to threaten to commit further murders. The film then goes to reveal that the entire film was a fevered dream, caused by the main character Toby being sick and consumed by overwhelming   cadet is a hippie.

After being released from the hospital, Toby and her boyfriend are about to have sex, at which point he puts on gloves similar to the ones worn by the Breather and strangles Toby, as he has lost respect for her. However, in a homage to the nightmare-ending of Carrie (1976 film)|Carrie, Tobys hands rise up from the freshly dug grave after her funeral to attack her killer.

==Cast==
* Kristen Riter as Toby Badger
* Matt Goldsby as Hardy
* Cullen Chambers as Charles Ray
* Jerry Belson as The Breather (credited as Richard Brando)
* Joe Flood as Mr. Dumpkin
* Joe Talarowski as Principal Harlow Hebrew Peters
* Mimi Weddell as Miss Mumsley
* Dario Jones as Mawamba
* Carl Jacobs as Dr. Sigmund
* Peggy Cooper as Ms. Van Dyke
* Janice E. OMalley as Nurse Krud
* Kevin Mannis as Scott
* Sara Eckhardt as Patti Priswell
* Oscar James as Football Coach/Sheriff
* Kay Ogden as Ms. Leclair
* Patrick Varnell as Malvert the Janitor
* Brian Batytis as Wheels
* Joan Browning Jacobs as Mrs. Hummers
* Angela Bressler as Julie
* Keith Singleton as Charlie

==Production notes== Michael Ritchie Allen Smithee instead. Mickey Rose was given writing and directing credit, but actually co-wrote and co-directed the film; Jerry Belson was the actual head writer, but couldnt be credited due to union rules.

===R rating=== R rating, fuck you." A slide indicating that the film has been indeed given an R rating by the MPAA appears for a few seconds, then the film continues.

===Parodies===
The film parodies a handful of previous slasher films and horror films, including Black Christmas, When a The Shining, Friday the Prom Night.

==="The Stick"=== pee red!" being one the films more memorable lines), and moves about in a herky-jerky fashion. (At films end, Malvert is revealed to be something of a sophisticate; when Toby informs him that he was a janitor in her dream, he responds, "Absurd!")
 Out Of Control. Several online reviews give the film itself a mixed reaction but praise The Sticks performance.   

An obituary from the Corsicana Daily Sun dated May 8, 1989, confirms The Sticks real name as Patrick Boone Varnell, born in Lawton, Oklahoma on January 22, 1941. He died at the Medical City Hospital in Dallas, Texas May 7, 1989, aged 47. His body was donated to science, by his sister who was a lab technician in Corsicana at the time of his death.  

===Filming locations=== Taylor High School in Katy, Texas was used for the films football stadium, exterior and some interior scenes.  The parade scene was filmed in downtown Houston, Texas on Main Street.

==Release==
One of a group of films directed towards teenaged audiences during the late 1970s and early 1980s, Student Bodies grossed $5.2 million its release.  It became famous as a late-night cult favorite on cable afterwards.  The DVD was released on June 3, 2007. The HD Blu-ray version was released May 3, 2011.

==Critical reception==
 AllMovie wrote, "Student Bodies, though occasionally very funny, is not consistent enough to recommend as a comedy or scary enough to be an effective horror film." 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 