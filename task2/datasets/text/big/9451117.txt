One Piece Movie: The Desert Princess and the Pirates: Adventures in Alabasta
{{Infobox film
| name           = One Piece Movie: The Desert Princess and the Pirates: Adventures in Alabasta
| film name = {{Film name| kanji          = 劇場版ワンピース エピソードオブアラバスタ 砂漠の王女と海賊たち
| romaji         = Gekijōban Wan Pīsu: Episōdo obu Arabasuta Sabaku no Ōjo to Kaizoku-tachi}}
| image          = One Piece - Movie 8.png
| caption        = Cover art of the English DVD release
| director       = Takahiro Imamura
| screenplay     = Hirohiko Uesaka
| story          = Kōnosuke Uda Eisaku Inoue Ken Ootsuka Kenji Yokoyama Takahiro Imamura
| based on       =   Yuriko Yamaguchi
| music          = Kohei Tanaka (composer)|Kōhei Tanaka Shirō Hamaguchi Yasunori Iwasaki Kazuhiko Sawaguchi Minoru Maruo
| editing        = Masahiro Goto
| studio         = Toei Animation Fuji Television Shueisha Bandai Entertainment Toei Company
| distributor    = Toei Company
| released       =  
| runtime        = 90 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          = $7,090,891   
}} adapting a story arc from the original manga by Eiichiro Oda, wherein the Straw Hat Pirates led by Monkey D. Luffy travel to the Kingdom of Alabasta to save the war- and drought-plagued country from Sir Crocodile and his secret criminal organization Baroque Works.
 Kohei Tanaka and Yasunori Iwasaki are again credited for the films musical score, joined this time by Shirō Hamaguchi, Kazuhiko Sawaguchi, and Minoru Maruo. The films theme song was written and sung by Ai Kawashima.

In Japan, the film was released on March 3, 2007, where it was shown alongside the Dr. Slump short "Dr. Mashirito & Abale-chan". It peaked at second place of the weekend box office and grossed $7,075,924.   Worldwide, the film has grossed a total of $7,090,891.  The film was briefly shown at select theaters across the United States, before it was released on DVD in North America on February 19, 2008  and the Blu-ray released on January 27, 2009.

==Plot==
The film opens with Nefeltari Vivi flying with Pell in a brief flashback. Returning to the present, Vivi and the Straw Hats meet Crocodiles subordinate Mr. 2 Bon Clay. Mr. 2 shows the Straw Hats his devil fruit ability, which allows him to assume the form and voice of anyone whose face he has touched. Vivi describes a brief history on how the Baroque Works leader Crocodile has used Dance Powder while posing as the countrys hero. He has also tricked the rebel and royal armies into fighting each other. Once in Alabasta and after crossing the desert, the Straw Hats find the rebels base deserted, while the rebel army, led by Vivis childhood friend Koza, witnesses the port town Nanohana being burned by members of Baroque Works disguised as soldiers of the royal army. The rebels decide to attack Alubarna, where at the same time Mr. 2 impersonates the king, Nefeltari Cobra, and orders the royal army to engage. Meanwhile in the desert, the Straw Hats are intercepted by Crocodile and his partner, Nico Robin|Ms. All Sunday. Crocodile aims for Vivi, but Luffy stays behind and distracts him, while the other Straw Hats escape. In the ensuing fight, Crocodile defeats Luffy by impaling him through the chest with his hook and buried alive.

The Straw Hats arrive at Alabastas capital city, Alubarna, where the officer agents of Baroque Works are already waiting for them. The Straw Hats lure them into the city, allowing Vivi to try and stop the approaching rebels. Vivis attempt fails and she rushes to the palace. Meanwhile, Usopp and Chopper defeat the officer agents Mr. 4 and Miss Merry Christmas, while Sanji manages to defeat Mr. 2. Vivi finally reaches the palace and convinces the acting royal army captain, Chaka, to blow up the palace to make the fighting sides listen to her. However, Crocodile and Ms. All Sunday arrive and interfere with her plan. Back in the streets, Nami defeats Ms. Doublefinger and Zoro learns to cut steel by defeating the blade-bodied Mr. 1. Back at the palace, Koza witnesses Crocodile questioning Vivis father who stands nailed to the wall, about the ancient weapon Pluton. He and Chaka attack Crocodile, but are quickly defeated. With the two armies leaders in his control, Crocodile engulfs the palace plaza in a sandstorm, making it even harder to stop the fighting. After that, he follows his partner and the king into the royal mausoleum. Luffy arrives and follows Crocodile. In the streets, Vivi and the remaining Straw Hats try to find a bomb set by Baroque Works to wipe out both armies. Luffy and Crocodile fight in the mausoleum. Crocodile hits Luffy with his poisonous hook, but Luffy is not stopped. The Straw Hats find the bomb, as well as Mr. 7 and Miss Fathers Day in the citys clock tower. Vivi takes out the agents and prevents the bomb from being fired; however, Vivi discovers that the bomb has a timer. Pell arrives and supposedly sacrifices himself to save Alabasta. Meanwhile in the mausoleum, an enraged Luffy breaks Crocodiles poisonous hook and defeats him.

Back at the plaza, it starts to rain and, with Chaka and Koza presenting the defeated Crocodile as the rebellions orchestrator, the fighting stops. It rains for three days. After that, Luffy wakes up and a banquet is given for the pirates. News arrives that a marine fleet is on their way to Alabasta. The Straw Hats decide to leave as fast as possible, leaving Vivi with a choice. The next day, she appears at the coast, to say farewell to the crew. From there, she uses a transponder snail to broadcast a speech through the country. The film ends with Vivi asking whether she was still their friend, but the Straw Hats show the sign of their friendship on their arms. In the end credits, Vivi finds Pell alive and peace eventually returns to Alabasta.

== Cast ==
*Mayumi Tanaka/Colleen Clinkenbeard as Monkey D. Luffy
*Kazuya Nakai/Christopher R. Sabat as Roronoa Zoro Nami
*Kappei Yamaguchi/Sonny Strait as Usopp Sanji
*Ikue Ohtani/Brina Palencia as Tony Tony Chopper
*Misa Watanabe/Caitlin Glass as Nefertari Vivi
*Ryūzaburō Ōtomo/John Swasey as Sir Crocodile (Mr. 0) Yuriko Yamaguchi/Stephanie Young as Nico Robin (Miss All Sunday)
*Iemasa Kayumi/Kyle Hebert as Nefertari Cobra
*Takeshi Kusao/Todd Haberkorn as  Koza
*Kenji Nojima/Kevin M. Connolly as Pell
*Kihachirō Uemura/Robert McCollum as Chaka
*Kazuki Yao/Barry Yandell as Bon Clay (Mr. 2)
*Tetsu Inada/Brett Weaver as Daz Bones (Mr. 1)
*Yuko Tachibana/Leah Clark as Miss Doublefinger
*Masaya Takatsuka/Scott Hinze as Mr. 4
*Mami Kingetsu/Wendy Powell as Miss Merry Christmas
*Keisuke/Anthony Bowling as Mr. 7
*Tomoko Naka/Cynthia Cranz as Miss Fathers Day
*Keiichi Sonobe/Antimere Robinson as Terracotta
*Takeshi Kusao/Mark Stoddard as Jaguar D. Saul
*Takeshi Aono/Christopher Bevins as Lasso

==Adaptations==

  and a light novel, both titled  . The film comic (ISBN 978-4-08-874236-6) was released on the 4th, the light novel (ISBN 978-4-08-703178-2) on the 7th of March 2007.    

==Soundtrack==

The score on the films soundtrack was composed by Kohei Tanaka (composer)|Kōhei Tanaka, Shiro Hamaguchi, Yasunori Iwasaki, Minoru Maruo and Kazuhiko Sawaguchi. The ending theme "Compass" was written and performed by Ai Kawashima. For the English release, the score was used, and an English remake of "Compass" was created for use in the English dub, but a defect in the DVD caused the Japanese version to play instead. However, the English version was used in the theatrical release as well as the Blu-ray release.

==Reception==

In its first week of showing, the film entered the Japanese weekend box office on place two.    In its second and third week, it placed fourth and ninth, respectively,   before falling out of the Top 10 the week after.  In its fifth week of showing, the film re-entered the Top 10 for a final ninth place.  In the Japanese market, the films gross revenue summed up to $7,075,924.    Including non-North American, foreign markets, the film made at total of $7,084,304  and after Funimation Entertainments limited showing in the US, the figure rose to a worldwide total of $7,090,891. 

Funimation Entertainments DVD and Blu-ray releases of the film were also the subject of several reviews by a number of publications for films and anime. Carl Kimlinger of Anime News Network described the film as "a feature-length recap with slightly revised editing and a heavy layer of theatrical gloss" that is "Squeezing an enormous plot into a teeny little film like a man in mid-life crisis trying to squeeze into high-school jeans", but noted that "The soundtrack is a joy to listen to, rousing and fun".  Bamboo Dong, another reviewer of Anime News Network, said that the films pace is "anything but smooth" and that the battles are "cobbled together". She commented that the film is "syrupy good fun" for those who are fans of the series, but noted that for non-fans it will hold only "limited appeal".  Although Todd Douglass Jr. of DVD talk said that "you really have to be affirmed in One Piece lore in order to full   appreciate  " and commented that it felt "incomplete and unbalanced", he still recommended the film, stating that it is "short on story but ... a lot of fun." In regard to Funimation Entertainments adaptation, he said that "  team does a great job of capturing the spirit and personalities of the shows characters." 

Bryce Coulter of Mania Entertainment said that the film "cannot be recommended for someone who is not familiar with the series" but otherwise recommends it, cautiously, while commenting that it gives "a neat perspective" on its source material, but ultimately does not do it "any justice". In reference to the English adaptation he said that "the ... voice actors ... did a great job of portraying the Japanese cast."  Davey C. Jones of Active Anime said that he liked the animation, in particular the backgrounds, and commented that the film "takes the character designs right out of the manga and TV show and ups them with theatrical quality shine."  N.S. Davidson of IGN rated the film with 7 out of 10, stating that the animation is only "Slightly more sophisticated ... than in the television series" and that the film itself is "most likely ... for One Piece fans only."  Dustin Somner of Blu-ray.com agreed on the film being primarily for fans, stating that the films lack of context makes it "frustrating (with a capital F)" for those unfamiliar with the series. 

==References==
 

==External links==
*  of Toei Animation  
*  of Funimation Entertainment
* 
* 
* 

 

 
 
 
 
 