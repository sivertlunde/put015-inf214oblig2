Marta of the Lowlands
{{Infobox film
| name           = Marta of the Lowlands
| image          = 
| alt            = 
| caption        = 
| director       = J. Searle Dawley
| producer       = Daniel Frohman
| screenplay     = Àngel Guimerà
| starring       = Bertha Kalich Wellington A. Playter Hal Clarendon Frank Holland Lillian Kalich
| music          = 
| cinematography = H. Lyman Broening 	
| editing        = 	
| studio         = Famous Players Film Company
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

Marta of the Lowlands is a 1914 American drama film directed by J. Searle Dawley and written by Àngel Guimerà. The film stars Bertha Kalich, Wellington A. Playter, Hal Clarendon, Frank Holland and Lillian Kalich. The film was released on October 5, 1914, by Paramount Pictures.  

==Plot==
 

== Cast ==
*Bertha Kalich as Marta
*Wellington A. Playter as Manelich 
*Hal Clarendon as Sebastien 
*Frank Holland as Sebastiens Servant
*Lillian Kalich as Muri
*George Moss as Villager 

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 