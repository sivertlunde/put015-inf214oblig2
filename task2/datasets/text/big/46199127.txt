Gaiety George
{{Infobox film
| name = Gaiety George
| image = 
| image_size =
| caption =   George King   Leontine Sagan
| producer = George King Richard Fisher   Katherine Strueby   Basil Woon
| narrator = Peter Graves   Hazel Court Eric Rogers
| cinematography = Otto Heller Hugh Stewart
| studio = Embassy Pictures 
| distributor = Warner Brothers
| released = 2 July 1946
| runtime = 98 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} historical musical George King Peter Graves. Victorian music hall, when an Irish impresario arrives in London..

==Cast==
* Richard Greene as George Howard 
* Ann Todd as Katharine Davis  Peter Graves as Henry Carter 
* Morland Graham as Morris 
* Hazel Court as Elizabeth Brown 
* Charles Victor as Danny Collier 
* Jack Train as Hastings  
* Leni Lynn as Florence Stevens  
* Ursula Jeans as Isobel Forbes  
* Daphne Barker as Miss de Courtney  
* Maire ONeill as Mrs. Murphy  
* Frank Pettingell as Grindley  
* Phyllis Robins as Chubbs  
* John Laurie as McTavish  
* Frederick Burtwell as Jenkins  Anthony Holles as Wade   David Horne as Lord Mountsbury  
* Patrick Waddington as Lt. Travers  
* Claud Allister as Archie  
* Graeme Muir as Lord Elstown  
* Evelyn Darvell as Maisie   Paul Blake as Lord Royville   John Miller as Rosie  
* Richard Molinas as Laurient  
* Gerhard Kempinski as Muller  
* Wally Patch as Commissionaire 
* Carl Jaffe as Kommandant 
* Everley Gregg as Landlady 
* Roger Moore as Member of the Audience   Hugh Morton as King (on stage)  
* Maxwell Reed as Prince (on stage)

==References==
 

==Bibliography==
* Harper, Sue. Picturing the Past: The Rise and Fall of the British Costume Film. British Film Institute, 1994.
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain, 1939-1949. Routledge, 1989.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 