Ecce bombo
{{Infobox film
| name           = Ecce bombo
| image          =
| caption        =
| director       = Nanni Moretti
| producer       = Mario Gallo Enzo Giulioli
| writer         = Nanni Moretti
| narrator       =
| starring       = Nanni Moretti Fabrio Traversa Luisa Rossi
| music          = Franco Piersanti
| cinematography = Giuseppe Pinori
| editing        = Enzo Meniconi
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = Italy
| language       = Italian
| budget         =
}}
Ecce Bombo  is a 1978 Italian  comedy film, written, starring and directed by Nanni Moretti. It was filmed in 16&nbsp;mm but released in 35&nbsp;mm. It was Morettis first  commercial success.

==Plot==
Michele Apicella, Goffredo, Mirko and Vito are four friends who have participated in the battles of the student in Sixties. Now in the Seventies, the four friends dont know what to do, though young and with so many possibilities to find a job in life. Intellectuals marginalized and misunderstood, the four friends find themselves when they can in a restaurant to discuss their outlandish theories. A girl named Olga disrupts their life, but Michele is his favorite, although he does not know what to do with the girls.

==Cast==
* Nanni Moretti - Michele Apicella
* Luisa Rossi - Micheles mother
* Lina Sastri - Olga
* Piero Galletti - Goffredo
* Susanna Javicoli - Silvia
* Cristina Manni - Cristina
* Lorenza Ralli - Micheles sister Valentina
* Maurizio Romoli - Flaminias husband Cesare
* Carola Stagnaro - Flaminia
* Fabio Traversa - Mirko
* Giorgio Viterbo - Reporter for Telecalifornia
* Paolo Zaccagnini - Vito
* Sandro Conte - Student
* Maurizio Di Taddeo - Student
* Mauro Fabretti - Student

==Awards==
* Nastro dArgento: Best Story
* Nominated for the Golden Palm at the 1978 Cannes Film Festival   

==References==
 

==External links==
*  

 

 
 
 
 
 
 


 
 