I'm Here (film)
{{Infobox film
| name           = Im Here
| image          = Im Here.jpg
| caption        = Promotional poster
| director       = Spike Jonze
| producer       = Vincent Landay
| writer         = Spike Jonze
| starring       = Andrew Garfield Sienna Guillory Sam Spiegel
| cinematography = Adam Kimmel
| editing        = Stephen Berger Eric Zumbrunnen
| distributor    =
| released       =  
| runtime        = 31 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Im Here is a 2010 sci-fi romance short film written and directed by Spike Jonze.  The film is a love story about two robots living in Los Angeles where humans and robots co-exist. The plot is based on The Giving Tree, and the main character is named after Shel Silverstein.

The film was funded by and is a promotion for Absolut Vodka, featuring the tagline "A Love Story in an Absolut World" on the promotional poster. Music from the band Sleigh Bells is prominently featured. The film made its debut at the 2010 Sundance Film Festival.

== Plot ==
 
Sheldon (Andrew Garfield) is a gray robot with a head shaped like an old PC tower. Every day, he rides the bus to the public library then rides home again at the end of the day to recharge himself in his apartment. He appears unhappy and forlorn until one day, while waiting for the bus, he sees Francesca (Sienna Guillory), a sleekly-designed female robot, driving a car (despite an apparent ban on robots driving). He sees her again the next day, driving with several other robots and one shirtless human. Though she passes by him at first, she turns her car around and offers Sheldon a ride home, which he accepts.

Francesca stops at a mall and she, and Sheldon, walk off together while she sticks pieces of paper on an "Exit" sign and a palm tree. The papers show a drawing of long hair and eyebrows with the words "Im here". Francesca falls off a ledge and injures her knee. Sheldon repairs her knee with his built-in toolkit, and they listen to "There Are Many of Us" by ASKA & The Lost Trees on her car radio.

Sheldon and Francesca grow closer; and one night, while they are lying together in Sheldons apartment, Francesca tells Sheldon about her dreams, something Sheldon thought was impossible for robots to have. The two later go to a rock concert, where Francesca gets lost in the crowd and loses her left arm. Sheldon takes her to safety then goes back to retrieve her arm. Finding it smashed on the floor, he replaces it with his own arm, transplanting it onto Francescas body.

Later, Sheldon finds Francesca lying in the hallway outside his apartment, her right leg missing. He removes his own leg to replace it; and though Francesca initially protests, he convinces her to take it by telling her he had a dream about her needing a leg and choosing his over all the legs that were offered to her.

Some time after, Francesca fails to pick up Sheldon at the library (as she normally does), leaving him to take the bus. When he gets home, he receives a phone call summoning him to the hospital. He arrives to find Francescas body broken and lifeless on an operating table, and he saves her life by having the rest of his body surgically transferred to her, with only his head remaining. Francesca is taken out in front of the hospital in a wheelchair, cradling Sheldons head in her lap. The two smile and look out towards the setting sun as a taxi pulls up to pick them up.

== Cast ==
* Andrew Garfield as Sheldon
* Sienna Guillory as Francesca
* Lyle Kanouse as Robot Neighbor
* Nancy Munoz as Lady at Bus Stop
* Annie Hardy as Annie
* Daniel London as Jack
* Michael Berry as Adam
* Christopher Wonder as Magician Richard Penn as Doctor
* Quinn Sullivan as Nurse

== Crew ==
 
* Spike Jonze – director, writer
* Vincent Landay – producer
* Mark Figliulo, Matt Bijarchi and David Zander – executive producers
* Adam Kimmel – director of photography
* Eric Zumbrunnen and Stephen Berger – editors
* Floyd Albee – production designer
* Justine Baddeley and Kim Davis-Wagner – casting
* Casey Storm – costume designer
* Sonny Gerasimowicz – robot designer
* Ren Klyce – sound designer
* Ben Gibbs – visual effects supervisor
* Method Studios – visual effects
 

== References ==
 
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 