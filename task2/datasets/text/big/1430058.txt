My Name Is Joe
 
 
 
{{Infobox film
| name           = My Name Is Joe
| image          = My name is Joe.png
| image_size     =
| caption        = dvd cover
| director       = Ken Loach
| producer       = Ulrich Felsberg Rebecca OBrien
| writer         = Paul Laverty
| narrator       =
| starring       = Peter Mullan
| music          = George Fenton
| cinematography = Barry Ackroyd
| editing        = Jonathan Morris
| distributor    =
| released       = 20 November 1998
| runtime        = 105 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} British film David McKay plays his troubled friend Liam. The films title is a reference to the ritualised greeting performed in Alcoholics Anonymous meetings, as portrayed in the films opening scene.

The movie was mainly filmed in the council estates of Glasgow and filling small roles with local residents, many of whom had drug and criminal pasts. The natural Scottish accents of some of the actors are unfamiliar to most American television viewers and as such the film is often shown subtitled.  (Ken Loach has a policy that actors should speak in their natural accent on film, and his early film Kes (film)|Kes faced a similar problem with the South Yorkshire dialect). 
 Best Actor for Mullan at the 1998 Cannes Film Festival.   

==Plot summary==
 
The film begins with Joe Kavanagh at an Alcoholics Anonymous meeting, relaying an experience from his past. He states the ritualised greeting: My name is Joe and Im an alcoholic. He feels that he is not in a position to drink any more with safety. He tells the group that he copes by praying and states that he is grateful to be at the meeting. He receives a round of applause for his admissions.
 football game to attend.

The next shot is of the football match. Joe is very encouraging as a coach. The other team appears in white and black, which are the colours that Joes team wears. Joes team express their anger at this and respond by taking their shirts off so that they are able to distinguish between players. The other team scores the first goal of the game.

Joe drives Liam home and tells Liam to take things one day at a time. Liam abruptly tells Joe to pull over and Liam runs up to a male who acts aggressively towards him. Joe does not hear what transpires between Liam and the man. Later, Joe sees Sarah, who is seen struggling with wallpaper in her car. Joe appears to fancy her, as Sarah tells him her name and he flirts with her.

Joe then helps an acquaintance of Sarahs to complete a wall papering and paint job in Sarahs flat. Sarah then leaves the lads to it. She later brings them tea as they paint the ceiling and sing to themselves. They then look outside and notice someone taking photos of them through the window. Joe runs outside with his can of white paint and brush to confront the man who was taking the photos. The photographer tells Joe that he was just doing his job and that he has a bad heart. Joe then paints all over the photographers car. The car speeds away and manages to knock over a pile of rubbish by the side of the road.

Later, Sarah and Joe have dinner together. They are able to make comfortable conversation with one another. There is a bottle of red wine on the table, but Joe does not drink any of the alcohol. Sarahs telephone rings and she leaves the kitchen to answer it. Joe is then left alone with the wine. He clears his plate and appears somewhat nervous. When Sarah finishes with her telephone conversation, Joe asks her about the photographs on her wall. Joe later reveals to Sarah that he does not drink, to which Sarah replies: Why didnt you say? Joe reveals that he is an alcoholic and he has not had a drink in ten months. Joe is happy that Sarah knows and that she has been direct in her responses to him. Joe thanks Sarah for her company. Sarah pays Joe for the work that he completed on her flat and they part company.
 Health Centre. The practice is very busy and the receptionist tries to cope with a patient who is complaining because he has been waiting a long time to receive his Medical prescription|prescription. Joe tells the receptionist that he would like to see Sarah. The receptionist does not appear to be surprised by his request and says that Joe is free to pop his head around the door. He sees Sarah advising parents on dealing with the changing of Diaper|nappies. Outside the Health Centre, Joe asks Sarah if she would like to go ten-pin bowling with him and to ring him if she fancies going. They then part company. Sarah speaks with a female colleague, Maggie, and says that Joe seems a bit wild. She states that she is not going to go bowling with Joe.

The next shot is of a bowling alley, with Spirit in the Sky playing in the background. Sarah and Joe bowl together and laugh at their many errors. They occupy Aisle 16 and neither of them are spectacular players. They go back to Sarahs place where she asks him in for a cup of tea. Joe politely says no. Sarah offers him money for a taxi. A friendly argument ensues, at the end of which Sarah finds herself locked out of her flat. Sarah says that she can sleep in her car. Joe states that Sarah can sleep at his place, but not necessarily together.
 got pissed. He describes the experience as being magical. When Sarah asks him what made him stop drinking, Joe asks her why she wants to know. He is not hostile, but curious. Joe says that he is scared to tell her and that there is a strong chance that she will hate him if he tells.

Joe explains that there was a girl he used to drink with and they loved one another. They were both just tangled up and they used to get into terrible arguments and tear one another apart. The audience then sees a flashback where Joe and the girl are returning home from a night out. He states that a cloud just descended... really, really dark. And suddenly I hated her. I hated me. He describes how he tried to open the door, but was unable to; when the partner whom he was with laughed at him, Joe lost control because she found it funny. He beat her by punching and kicking her, while she lay helpless on the ground. He screamed abuse at her. The next day, he had a blackout. Now, he remembers every punch and kick. He states how ashamed and disgusted he is with himself. He is obviously contrite and he has not yet forgiven himself for his actions. In the next shot, Sarah comforts Joe and tells him that it is okay.

The next scene is of a football match. Joe sees three men beating up Liam. Joe attempts to intervene. He helps Liam up and asks him if he is still Drug detoxification|clean. Liam states that he is. Liam confronts his wife when she comes home. He suspects that she has been using and asks her if she takes him for a prick. Liam then yells that he will leave her to rot, grabs her by her neck and forces her to look at herself in the mirror.
 shooting up in the bathroom. An argument erupts between them. Joe offers to help her. She does not want the kind of help that Joe offers. Joe then reluctantly assists her to tie off one of her veins so that she can shoot up. Joe tries to make sense of her incoherent mumbles as she lies prostrate in the bathtub. Joe then goes to a snooker club where he asks after Liams whereabouts. Joe tells one of the men that he encounters that Liam has a four-year-old boy in an attempt to mitigate matters. Liam says that he does not want them to take liberties. He comes across Liam and is cross because Liam told him that he had stopped dealing. A tearful Liam confesses that he still owes money. Joe also tells Liam that he suspects that his wife is on the game. He is worried that people are going to break his legs.
 B & B to deal with the matter of Liam. In a later scene, Joe gives Sarah a pair of earrings, as well as a ring. Joe tells Sarah that he loves her. Sarah tells him that she does not want it and Joe cannot understand what he has done wrong. Joe is confused as to why Sarah has behaved in this fashion. In the next shot, we see a copy of the Alcoholics Anonymous small blue Bible as Joe listens to Beethovens Violin Concerto in D Major and makes notes in a book. When Sarah arrives at his door, she apologises. They drink tea together and later embrace.
 budgies because of their blue and yellow striped uniforms. Meanwhile, Sarah reveals to her co-worker Maggie that she is pregnant. Maggie congratulates her.

At Sarahs flat, she expresses her reservations about Joes dealings with Liam. Joe tells her that there are some things that she does not need to know. Joe outlines the limited choices that Liam faces, most of which are not pleasant. Sarah accuses Joe of being a drug dealer and asks where he got the money for her earrings and ring. Joe replies that the gang gave him some money. Sarah becomes enraged and asks, Are you going to hit me too, Joe?

Joe attends Liams flat and states that Liam must deal with things himself. Joe returns to his flat with bottles of Smirnoff Vodka. He pours himself a drink, and takes a sip. Liam then goes to Joes flat, where Joe is very inebriated. Joe does not understand why Liam has come to see him. Joe calls Liam a stupid wee prick. The alcohol has overtaken Joe and he is hostile to Liam, who needs his help. As Liam witnesses members of the gang coming towards the premises, Liam realises that Joe has passed out and he kisses Joe on his forehead. Joe suddenly snaps awake and realises that Liam has taken his own life by hanging himself.

The film concludes with Liams funeral.

==Cast==
*Peter Mullan – Joe Kavanagh
*Louise Goodall – Sarah Downie David McKay – Liam
*Anne-Marie Kennedy – Sabine (as Annemarie Kennedy)
*David Hayman – McGowan Gary Lewis – Shanks
*Lorraine McIntosh – Maggie
*Scott Hannah – Scott David Peacock – Hooligan
*Gordon McMurray – Scrag
*James McHendry – Perfume
*Paul Clark – Zulu
*Stephen McCole – Mojo
*Simon Macallum – Robbo
*Paul Gillan – Davy

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 