Vaali (film)
 
{{Infobox film
| name = Vaali
| image = Vaali poster.JPG
| director = S. J. Surya
| writer = S. J. Surya Simran Jyothika
| producer = S. S. Chakravarthy Deva
| Jeeva
| editing = B. Lenin V. T. Vijayan
| released = 1 May 1999
| runtime = 159 minutes
| language = Tamil
| country = India
| box office = 20 crs
}}
 Tamil thriller thriller film Simran appearing Livingston and Deva while Jeeva was in charge of the cinematography. The film released on 1 May 1999 to critical acclaim and went on to become a commercial success. 

==Plot==
Deva and Shiva (both played by Ajith Kumar) are the twins. Deva, the elder, is deaf and mute. But he is a genius, an expert at lip-reading and the head of a successful advertising company. Shiva loves and trusts his brother. Priya (Simran (actress)|Simran) wants to marry only someone who is an ex-smoker, an ex-drunkard and ditched by a girl but still pining for her. Learning this, Shiva invents an old romance between him and Sona (Jyothika) and finds his way into Priyas heart.

Deva meanwhile chances upon Priya and becomes obsessed with attaining her. His obsession continues even after his younger brother gets married to the girl of his dreams and he devises various means of getting close to Priya and keeping Shiva and her separated. Some of the methods Deva uses to woo Priya are masochistic (wounding his hand by the running car engine to stop the couple’s first night) and psychotic (trying to kill his brother in so many ways).

Priya realises the not-so-honourable intentions Deva has towards her but Shiva refuses to believe her and has full faith in his brother. He even goes as far as to take Priya to a psychiatrist. To get away from it all, Shiva and Priya go on a long-delayed honeymoon. But Deva shows up there too. Shiva watches Deva kissing the photo of Priya and realises Priya was right all along. While Shiva is away, Priya has to take care of Deva. Deva beats Shiva mercilessly, packs the unconscious Shiva in a gunny bag and throws him in a lorry.

Deva disguises himself as Shiva and goes near Priya. Priya comes to know he is Deva and escapes from him before shooting him with her revolver. Deva falls in pool and when Shiva comes she narrates the whole incident to him. Suddenly, Deva comes back to life, but Shiva immediately kills him with the revolver. Devas soul talks about his inability to express his feeling as he was mute. His grave with lots of flowers grown on it is shown.

==Cast==
* Ajith Kumar as Deva and Shiva Simran as Priya
* Jyothika as Sona Livingston
* Vivek as Vicky
* Pandu as Velu Nair
* Radha Bai as Deva and Shivas Grandmother
* Sujitha as Maheshwari
* Indhu as Geetha Rajeev as Geethas husband
* Devipriya as Deepa

==Production== Simran before Roja would play another role, although this proved to be untrue.  Jyothika, sister of actress Nagma, made her debut in the film as an imaginary character, Sona, narrated by Ajiths character in which she won "Amudha Surabhi" Best Actress New Comer Award. 

About the making of the film, Ajith mentioned that Vaali "was very close to my heart and I gave it everything I had", revealing that he had initially received a lot of bad publicity and scepticism for doing a dual role too early in his career. 

==Release==
Vaali received A certificate from Central Board of Film Certification. The Deccan Herald described it as "definitely worth seeing" saying it "has something for all tastes&nbsp;— a pleasant love angle, some suspense, complex psychological nuances, good acting, pleasing songs" while praising Ajiths performance.  The reviewer from Indolink.com labelled the film as "a classic in its own right", praising the performances of Ajith Kumar and Simran while describing Suryaah as "a new young director to the cine field who can make Tamil Cinema be proud once again".  The New Indian Express labelled Simrans portrayal as "outstanding" while mentioning Surya does a "fairly good job and succeeds".  the film ran 215 days The film became a commercial success and provided a breakthrough for both Ajith and Simrans  career.  

==Awards==

Filmfare Awards South Best Actor - Ajith Kumar - Won. Best Actress Simran - Nominated.

Cinema Express Awards  Best Actor - Ajith Kumar - Won. Best Actress Simran - Won. Deva - Won.
* Best Comedian - Vivek - Won.

==Remakes==
It was later dubbed into Telugu as Vaali and Hindi as Vaalee, as a result of the success of the Tamil version. The film was remade in Kannada as Vaalee (film)|Vaalee with Sudeep playing lead role.

==Soundtrack==
{{Infobox album  
| Name = Vaali
| Type = soundtrack Deva
| Cover =
| Alt = Album cover
| Released =  
| Recorded = Jay Jay Studios Chennai
| Genre = Film soundtrack
| Length = Deva
| Label = Sa Re Ga Ma Vega Music
| Last album = Unnaruge Naan Irundhal (1999)
| This album = Vaali (1999) Appu (2000)
}}
{{Track listing
| extra_column = Singer(s) Vairamuthu | length1 = 04:50 Vairamuthu | length2 = 04:48 Vairamuthu | length3 = 04:01
| title4 = April Maathathil | extra4 = Unni Krishnan, Harini | Vairamuthu | length4 = 04:04 Vairamuthu | length5 = 05:05
}}

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 