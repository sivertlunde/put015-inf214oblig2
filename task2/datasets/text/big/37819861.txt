Lucky to Me
{{Infobox film
| name = Lucky to Me
| image =
| image_size =
| caption =
| director = Thomas Bentley
| producer = Walter C. Mycroft Arthur Rigby (play)   Clifford Grey
| narrator =
| starring = Stanley Lupino   Phyllis Brooks   Barbara Blair   Gene Sheldon
| music = Noel Gay   Harry Acres
| cinematography = Derick Williams
| editing = Monica Kimick
| studio = Associated British Picture Corporation
| distributor = Associated British Film Distributors
| released = November 1939
| runtime = 69 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} musical comedy ABPC at its Elstree Studios. It was the last film of Lupino who had made a string of successful musical comedies during the Thirties. 

==Cast==
* Stanley Lupino as Potty Potts
* Phyllis Brooks as Pamela Stuart
* Barbara Blair as Minnie Mousey Jones
* Gene Sheldon as Hap Hazard
* Antoinette Cellier as Kay Summers
* David Hutcheson as Peter Malden
* Bruce Seton as Lord Tiny Tyneside
* Geoffrey Sumner as Fanshaw
* Rosamund John as Girl Gordon McLeod as Doherty
* Julien Mitchell as Butterworth
* Edward Underdown as Maldens friend

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film: Filmmaking in 1930s Britain. George Allen & Unwin, 1985 .

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 