Beyond the Game
{{Infobox Film name           = Beyond the Game image           = Beyond the Game.jpg director       = Jos de Putter producer       = Wink de Putter writer         = Jos de Putter starring  Li Xiaofeng Fredrik Johansson Manuel Schenkhuizen Jang Jae Ho music          = Paul van Brugge
| cinematography = Vladas Naudzius Richard van Oosterhout   Jackó van t Hof editing        = Sander Vos released       =   runtime        = 75&nbsp;minutes country        = The Netherlands photography    = Vladas Naudzius, Richard van Oosterhout, Jackó van t Hof sound          = David Spaans, Tom d Angremond, Alex Booij language       =  Dutch, English, French, Spanish, Swedish, Mandarin & Korean
}} Xiaofeng Li Fredrik Johansson (MaDFroG) prominently. It is directed by award winning Dutch documentary filmmaker Jos de Putter. Filming took place in China, France, The Netherlands, USA and Sweden. Languages spoken in the documentary include Dutch language|Dutch, English language|English, French language|French, Spanish language|Spanish, Swedish language|Swedish, Standard Chinese and Korean language|Korean.

Beyond the Game is the motto of the World Cyber Games, considered the olympics of gaming, which is central to the documentary as the main characters compete to defend or regain the events championship title at the global finals of the 2007 World Cyber Games in Seattle, Washington.

It has premiered at the International Documentary Film Festival Amsterdam    in Pathé de Munt, and had the cinema premiere in Tuschinski|Pathé Tuschinski, and has now been released in cinemas throughout The Netherlands.   

== Historical background ==
In 2002 the videogame   is released which sells millions of copies and quickly becomes a leading title in the world of competitive gaming (in 2003 an expansion on the game,  , is released). Fredrik Johansson Blizzard Worldwide Invitational and is recognized as the ESports Award|worlds most successful gamer.  After his return to Sweden he becomes runner-up of the Electronic Sports World Cup again in 2004, but subsequently loses his motivation for the sport and qualifies for one more event in the 2004 World Cyber Games before retiring a few months later. There he is defeated prematurely and a new European star takes over from him as 16-year old Manuel Schenkhuizen wins the event. 
 Suntec City, Singapore  and cement his name as the best player ever; but is defeated at the event by Dennis "Shortround-" Chan, a 22&nbsp;year old in college semi retired pro gamer.
 Player of the Year in all of competitive gaming like Johansson two years before him. The award most important to both players however is that of World Cyber Games champion as the two qualify for the 2006 global finals in Monza, Italy and meet in the quarter-finals. Winning a second title would mean inclusion in the World Cyber Games Hall of Fame for either of them. This clash is observed by documentary filmmaker Jos de Putter who senses the deep emotions involved in the encounter, not only by the players but also by the spectators.   

Afterwards the filmmaker decides to follow both players on their way to their next World Cyber Games encounter, where one player will try to defend and the other to regain the World Cyber Games title, and portray this new world. To provide insight in the background of the gaming scene and the mind of competitors he also contacts Fredrik Johansson who is developing a new life in Sweden and is now regarded as a legend in the world of competitive gaming.

== References ==
 

== External links ==
* 
* 
 

 
 
 
 
 
 