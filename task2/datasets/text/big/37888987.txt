Nanbargal
{{Infobox film
| name           = Nanbargal
| image          = Nanbargal DVD cover.jpg
| image_size     =
| caption        = DVD Cover
| director       = Shoba Chandrasekhar Vijay
| writer         = Shoba Chandrasekhar
| screenplay     = S. A. Chandrasekhar
| starring       =  
| music          = Songs: Babul Bose Film score: Sangeetha Rajan
| cinematography = S. Jayachandran
| editing        = S. Shyam Mukherjee
| distributor    =
| studio         = V. V. Creations
| released       =  
| runtime        = 140 minutes
| country        = India
| language       = Tamil
}}
 1991 Tamil Tamil romance film directed by Shoba Chandrasekar. The film features Neeraj, Mamta Kulkarni, Vivek (actor)|Vivek, Dinesh, Sundar and Shily Kapoor in lead roles. The film, produced by Vijay (actor)|Vijay, was released on 14 February 1991. The film songs was composed by Babul Bose and the film score was composed by Sangeetha Rajan. The film did well at the box-office and it was later remade in Hindi as Mera Dil Tere Liye by S. A. Chandrasekhar which flopped.  {{cite web|url=http://www.rediff.com/entertai/1998/apr/14mam.htm|title=The reluctant sex symbol
|author=V. S. Srinivasan|accessdate=2012-12-10|date=1998-04-14|publisher=rediff.com}}  

==Plot==

Vijay (Neeraj), Gopi (Vivek (actor)|Vivek), Salim, Beeda and Bheema are good friends and classmates. Priya, a rich and very arrogant girl, has first quarrels with Vijay. Vijay first loved her but she ridicules him and hurts him. When his friends tries to teach a lesson to Priya, Vijay saves her and they both fall in love. Whereas, Priyas father (Prathapachandran), an influential businessman, is ready to break their relationship.

==Cast==

*Neeraj as Vijay
*Mamta Kulkarni as Priya Vivek as Gopi
*Dinesh as Salim
*Sundar as Beeda
*Shily Kapoor as Bheema
*Nagesh as Rajasekhar Manorama
*Prathapachandran as Priyas father Sangeeta as Priyas mother
*Venniradai Moorthy as Sidambaram (guest appearance)
*Ajay Rathnam
*Kumarimuthu

==Soundtrack==

{{Infobox album |  
| Name        = Nanbargal
| Type        = soundtrack
| Artist      = Babul Bose
| Cover       = 
| Released    = 1991
| Recorded    = 1991 Feature film soundtrack |
| Length      = 44:52
| Label       = T-Series
| Producer    = Babul Bose
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Babul Bose. The soundtrack, released in 1991, features 7 tracks with lyrics written by Vairamuthu and Pulamaipithan. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Achu Vellam Tharean Machinichiye || Mano (singer)|Mano, K. S. Chithra || 6:45
|- 2 || Atheri Paacha Pachadhan || Mano, K. S. Chithra || 6:23
|- 3 || Ennuyerea Ennuyerea  (solo)  || K. S. Chithra || 5:57
|- 4 || Ennuyerea Ennuyerea  (duet)  || K. S. Chithra, Satheesh || 6:51
|- 5 || Kaadhal En Paavam Endraal || Mano, Shoba Chandrasekar || 6:38
|- 6 || Kalangalaal Azhiyadhadhu || Mano, Vijayramani, Satheesh, Prabhakar || 5:53
|- 7 || Vellai Rojavea || K. S. Chithra, Satheesh || 6:25
|}

==References==
 

 
 
 
 
 
 
 


 
 