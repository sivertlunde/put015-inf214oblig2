The Cracksman
{{Infobox Film
| name           = The Cracksman
| image_size     = 
| image	=	The Cracksman FilmPoster.jpeg
| caption        = 
| director       = Peter Graham Scott
| producer       = W.A. Whittaker
| writer         = 
| narrator       = 
| starring       = Charlie Drake Nyree Dawn Porter George Sanders Dennis Price
| music          = Ron Goodwin
| cinematography = Harry Waxman Richard Best
| studio         = Associated British Picture Corporation
| distributor    = Associated British Picture Corporation|Warner-Pathé Distributors
| released       = 1963
| runtime        = 
| country        = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 British comedy film directed by Peter Graham Scott.   

==Plot==
Charlie Drake plays honest locksmith Ernest Wright whose problem is that he cannot resist the challenge of a lock. First he is duped by a debonair con man into opening a car. He is caught by the police but is released on probation. Next the same man fools him into breaking into a house, and he lands in jail for a year. When he is released, he gets tricked into opening a safe, for which he receives a three year jail sentence and an undeserved reputation as a master thief. Upon his release he finds himself as a pawn being manipulated by two gangs into a safe-cracking scheme but, with the help of undercover police woman Muriel played by Nyree Dawn Porter, he helps trap the crooks and clear his name.

==Cast==
* Charlie Drake as Ernest Wright 
* Nyree Dawn Porter as Muriel 
* George Sanders as Guvnor 
* Dennis Price as Grantley  Percy Herbert as Nosher Jenkins
* Eddie Byrne as Domino 
* Finlay Currie as Feathers 
* Geoffrey Keen as Magistrate 
* George A. Cooper as Fred 
* Patrick Cargill as Museum Guide 
* Norman Bird as Policeman  Neil McCarthy as Van Gogh 
* Christopher Rhodes as Mr. King 
* Ronnie Barker as Yossle 
* Wanda Ventham as Sandra 
* Jerold Wells as Chief Prison Officer 
* Tutte Lemkow as Choreographer 
* Richard Leech as Detective Sergeant  Robert Shaw as Moke

==Additional credits==
* Delia Derbyshire created the sound for the "In a Monastery Garden" sequence. The instrument is, in her words, "an E♭ safe-unlocking mechanism".

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 

 