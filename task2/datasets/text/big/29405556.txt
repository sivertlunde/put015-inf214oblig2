Snegithiye
{{Infobox film
| name           = Snegithiye
| image          = Snegithiye.jpg
| director       = Priyadarshan
| story          = Chandrakant Kulkarni
| screenplay     = Priyadarshan
| writer         = Maharajan   (Dialogues ) 
| producer       = Mukesh R. Mehta Tabu Ishita Arun  Manasi Scott Vidyasagar Sangeetharajan Jeeva
| editing        = N. Gopalakrishnan
| studio         = Surya Cine Arts
| released       = April 14, 2000
| runtime        = 154 minutes
| country        = India Tamil
}}
 Marathi film Tabu and Ishita Arun  music was composed by Vidyasagar (music director)|Vidyasagar. The film, released in 2000, proved unsuccessful at the box office. Originally planned to be made as a bilingual, in Tamil and in Malayalam, the film released first in Tamil only, while the Malayalam version, Raakilipattu, as well as the dubbed Hindi version, Friendship, released only seven years later.   

==Plot==
Vani Subramaniyam (Jyothika), her best friend Radhika (Sharbani Mukherjee) and their friends are pranksters in a ladies college. Like other college girls, they sing songs, break rules and wreak havoc. They also have a grudge against Gita (Ishita Arun), another girl from the college.

Lakshmi, Radhikas aunt, wants her niece to get married. To escape, Vani and Radhika fake a boyfriend whose name is Ramesh. However, this fantasy takes on a whole new meaning when a person named Ramesh calls and sends Radhika letters claiming to be her boyfriend. To end this nuisance, Vani and Radhika invite Ramesh to the hostel during the college dance program. Vani instructs Radhika to bring a gun for their safety.

Mysteriously, without them shooting him, Ramesh (Major Ravi) dies. At the same time, Radhika loses her chain. Suspicion eventually falls on the two friends. Gayathri (Tabu (actress)|Tabu) is a man-hating police officer, and is put on the case. To make matters worse, Gita suddenly disappears. Vani and Radhika find themselves as fugitives, running from the law in a bid to prove their innocence. Meanwhile, the real killer is still on the loose.

==Cast==
  Jyothika as Vani Subramaniam
* Sharbani Mukherjee as Radhika Tabu as Gayathri
* Ishita Arun as Geetha
* Major Ravi as Ramesh
* Shweta Menon Lakshmi as Malathi Manorama as Vanis mother
* Vadivukkarasi in a cameo
* Bharati Achrekar
* Mita Vasisht as the under cover cop
* Sukumari as the hostel warden
* Jomol in a cameo as gayathris sister
* Deepti Bhatnagar in a Special Appearance
* Manasi Scott as Vanis friend Suchithra as Advacate
* Manju Pillai as police constable
 

==Production==
The original story idea for the film was taken from Marathi writer Chandrakant Kulkarni, who had made the successful Marathi film, Bindhaast, based on the story last year. Priyadarshan heard about the film and asked producer Mukesh Mehta to see it and decide whether he would like to produce the movie and thus Mehta went to Bombay, saw the film and liked it. However the producer reiterated that they have taken only the main thread from the Marathi story with the rest of the film, its sub-plots and the treatment are completely different from the original. Initially, it was planned as a Malayalam film with a cast familiar to the Kerala audience. But as Priyadarshans excitement over the project grew, the canvas got wider and the producer decided to look for a bigger audience. Thus Jyothika, Sharbani Mukherjee and Ishita Arun, daughter of Ila Arun, stepped in to give it a more national flavour. Seasoned actresses like Manorama, Lakshmi, Lalitha, Sukumari, Mita Vasisht, Tabu and Dipti Bhatnagar also joined the all-female team. Cinematographer Jeeva and art director Sabu Cyril also joined the team and they shot 90 per cent of the film in a college in Mysore and the rest in Madras, also featuring live coverage of the Dussera festival of Mysore.  The film was subsequently made simultaneously in Tamil and Malayalam with a title of Raakilipattu, while a Hindi version was also planned in 2000.  The title of the Tamil version of the film was taken from a song from the 2000 Mani Ratnam film Alaipayuthey. 

Jyothika prioritised her work in the project and thus delayed her schedules for Uyirile Kalanthathu in order to ensure the film was released as early as possible.  The songs of the film were released at Devi Theatre with Kamal Haasan appearing as the chief guest. 

==Release== Monal and Abhinayashree would be a "small-scale Snegithiye". 
 that version only released in 2007.

==Soundtrack==
{{Infobox album|  
  Name        = Snegithiye
|  Type        = Soundtrack Vidyasagar
|  Cover       =
|  Released    = 2000
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Snegithiye (2000)
|  Next album  = 
}}
The soundtrack of the film was composed by Vidyasagar (music director)|Vidyasagar, except the song Kannukulle which was composed by Raghunath Seth as mentioned in the original audio cassette and was well received by the audience. 

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Othayadi Padhayile
| extra1          = Manasi, Ila Arun
| length1         = 
| title2          = Radhai Manathil
| extra2          = K. S. Chithra, Sujatha Mohan|Sujatha, Sangeetha Sajith
| length2         = 
| title3          = Doora Desam Sujatha
| length3         = 
| title4          = Kalluri Malare
| extra4          = K. S. Chithra, Sujatha Mohan|Sujatha, Sangeetha Sajith
| length4         = 
| title5          = Kannukulle Srinivas
| length5         = 
| title6          = Devadhai Vamsam Sujatha
| length6         = 
}}

==See also==
* Raakilipattu

==References==
 

==External links==
*  

 

 
 
 
 
 
 