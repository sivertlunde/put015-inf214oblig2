The First Power
{{Infobox Film
| name           = The First Power
| image          = Firstpowergerman.jpg
| image_size     = 
| caption        = German poster
| director       = Robert Resnikoff
| producer       = David Madden
| writer         = Robert Resnikoff
| starring       = Lou Diamond Phillips Jeff Kober Mykelti Williamson Tracy Griffith Dennis Lipscomb Carmen Argenziano Clayton Landey
| music          = Stewart Copeland
| cinematography = Theo van de Sande
| editing        = Michael Bloecher Nelson Entertainment Interscope Communications
| distributor    = Orion Pictures
| released       = April 6, 1990
| runtime        = 98 minutes
| country        =  
| language       = English
| budget         = $10,000,000 (estimated)
| gross          = $22,424,195 (USA)
| preceded_by    = 
| followed_by    = 
}} American horror film/neo-noir, directed by Robert Resnikoff and starring Lou Diamond Phillips, Tracy Griffith, Jeff Kober and Mykelti Williamson.

==Plot== sacrifice to MO is engraving a pentagram symbol into the flesh of his victims before killing them.

Detective Russell Logan (played by Lou Diamond Phillips) is determined to bring the Pentagram Killer to justice. He receives an anonymous phone call from a psychic called Tess Seaton (played by Tracy Griffith). After getting his promise that the man will not be executed, she tells him where the killer is going to strike next. With time running out for the next victim Logan decides to take Tess on her word. Logan goes on a stakeout and successfully tracks down Channings lair. During a struggle in which Logan apprehends Channing, Logan receives a severe stab wound to his torso.

Logan manages to recover from his stomach injury and breaks his word, turning Channing over to the death penalty authorities. Tess makes another phone call to Logan pleading with him to keep his promise that Channing must not be executed. Logan refuses; he is satisfied that Channing is now caught and cannot harm another person, is not interested in sparing the serial killer from what he believes is a much deserved fate. Channing is later convicted and sentenced to be killed in the gas chamber. However, since Channing was a worshipper of Satan, Satan seemingly grants Channing The First Power -- resurrection. This is the first of three special powers Channing is attempting to gain, and is directly stated in the movie that Jesus Christ also possessed all three of these powers. Channing returns from beyond the grave and is able to appear or disappear at will, as well as possess others. His main objective now becomes to get his revenge on Russell Logan as well as continue his work. Logan must then team up with Tess in order to find a way to defeat Channing once and for all.

===Cast===
{| class="wikitable"
|-
! Actor / Actress
! Character
|-
| Lou Diamond Phillips
| Russell Logan
|-
| Mykelti Williamson (Mykel T. Williamson)
| Det. Oliver Franklin
|-
| Jeff Kober
| Patrick Channing
|-
| Dennis Lipscomb
| Cmdr. Perkns
|-
| Carmen Argenziano
| Lt. Grimes
|-
| Clayton Landey
| Mazza
|-
| Tracy Griffith
| Tess Seaton
|-
| Sue Giosa
| Carmen
|-
| Oz Tortora
| Antonio
|-
| Dan Tullis Jr.
| Cop at Arrest
|-
| Hansford Rowe
| Father Brian
|-
| Grand L. Bush
| Reservoir Worker
|-
| Bill Moseley
| Bartender
|- David Gale
| Monsignor
|-
| Philip Abbott
| Cardinal
|}

==Reception==
The film received negative reviews, including Desson Howes in the Washington Post, which called it "shopworn and imitative."  As of April 2012, The First Power has a 13% "rotten" rating on Rotten Tomatoes. 


==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 