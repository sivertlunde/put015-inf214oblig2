La morte risale a ieri sera
{{Infobox film
| name           =La morte risale a ieri sera
| image           =La morte risale a ieri sera.jpg
| director       = Duccio Tessari
| writer = Biagio Proietti
| based on=  
| starring       =  
| music          = Gianni Ferrio
| cinematography = Lamberto Caimi
| editing        = Mario Morra
| released       =  
| runtime        =   
| country        = Italy 
| language       = Italian 
| budget         = 
| gross          = 
}}
 Frank Wolff. The films score was composed by Gianni Ferrio, whose baroque and psychedelic contributions to the soundtrack were described by one critic as inconsistent with the tone of the film.

==Plot==
 Frank Wolff) investigates the citys pimps and prostitutes for clues, eventually finding the girls burnt body in a field. Berzaghi vows to find the girls murderer, eventually tracking down his quarry. Berzaghi exacts his revenge, but finds no satisfaction from having done so.

==Cast==
* Frank Wolff as detective Duca Lamberti
* Raf Vallone as  Amanzio Berzaghi Gabriele Tinti as  Mascaranti
* Gillian Bray as Donatella Berzaghi
* Eva Renzi as the wife of Lamberti
* Gigi Rizzi as  Salvatore
* Beryl Cunningham as Herrero

==Production==

La morte risale a ieri sera was written by Biagio Proietti, the film is based on Giorgio Scerbanencos novel I milanesi ammazzano al sabato (The Milanese Kill on Saturday).  The films soundtrack was composed by Gianni Ferrio, who had worked with director Duccio Tessari on several other films, including his 1971 giallo Una farfalla con le ali insanguinate  and the 1969 spaghetti western Vivi o, preferibilmente, morti.    Ferrios score spans several musical styles, incorporating psychedelic rock, baroque pop and jazz. 

==Release and reception==

La morte risale a ieri sera was released in Italy on September 5, 1970.  The film has also been distributed internationally under the titles Death Occurred Last Night and Death Took Place Last Night.   Film & TV Database   La morte risale a ieri sera (1970)  |publisher=British Film Institute |accessdate=May 9, 2013}} 

Writing for AllRovi, Robert Firsching rated La morte risale a ieri sera two-and-a-half stars out of five. Firsching felt that the films plot featured "a great deal more humanity than is typical for the genre" of giallo, finding that Tessaris focus on characterisation over plot was its key strength. However, he responded negatively to Ferrios score, considering its "bouncy" tone to be inconsistent with the film.   

==Footnotes==

 

===References===

* 
* 

==External links==
*  
 
 
 
 
 
 
 
 
 
 
 