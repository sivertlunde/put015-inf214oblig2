Ormakalundayirikkanam
{{Infobox film
| name           = Ormakalundayirikkanam
| image          = Ormakal Undayirikkanam.jpg
| image size     = 
| alt            =  Nitin in a scene from the film
| director       = T. V. Chandran
| producer       = Salam Karassery
| writer         = T. V. Chandran Sreenivasan
| Johnson
| Venu
| editing        = Venugopal
| studio         = Navadhara Movie Makers
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Vimochana Samaram (Liberation Struggle) of 1959.   

==Plot== Communist Party Congress party.

Jayan with knowledge dawning upon him relaises how Bhasi and his near and dear ones live on the verge of fear of being annihilated by the henchmen of Congress and upper castes who seem to usurp power by unsure means and violence. Jayans intimate rapport with Dr. Tharakan, a scientist of sorts, engaged in the theory of impending End time|doomsday, helps him see things clearly. Jayan realises the truth besmeared by blood, murder and revenge.

==Cast==
* Mammootty as Tailor Bhaskaran (Bhasi) 
* Master Nithin as Jayan 
* Bharath Gopi as Tharakan 
* Priyambada Ray
* Nedumudi Venu as Nambiar, Jayans father
* Kukku Parameswaran Sreenivasan as Barber Naanu
* M. G. Soman as Chattampi Velayudhan
* Bindu Panicker
* V. K. Sreeraman as Tea Shoper
* Priyanka

==Awards==
* National Film Award for Best Feature Film in Malayalam - T. V. Chandran and Salam Karassery 
* Kerala State Film Award for Best Child Artist - Master Nithin   
* Kerala State Film Award (Special Jury Award) - T. V. Chandran

==References==
 

==External links==
*  

 
 

 
 
 
 