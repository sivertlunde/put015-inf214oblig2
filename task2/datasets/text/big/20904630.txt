Lok Parlok
{{Infobox film
| name           = Lok Parlok
| image          = Lok Parlok.jpg
| caption        =
| director       = Rama Rao Tatineni
| producer       = Sree Pallavi Productions
| writer         = Charandas Shokh
| screenplay     = Rama Rao Tatineni
| starring       = Jeetendra, Jayapradha, Amjad Khan, Pradeep Kumar, Prem Nath, Deven Verma
| music          = Laxmikant-Pyarelal Anand Bakshi (lyrics)
| cinematography = S. Venkataratnam
| editing        = Sanjeeva Rao Akkineni
| studio         =
| distributor    =
| released       =  
| runtime        = 112 minutes
| country        = India
| language       = Hindi
| budget         =
| gross          =
}}
Lok Parlok is a 1979 Bollywood action film directed by Rama Rao Tatineni. It is remake of the blockbuster Telugu film Yamagola (1977), which also starred Jayaprada.

==Cast==
*Jeetendra &ndash; Shyam/Ram Ghulam
*Jayapradha &ndash; Savitri
*Amjad Khan &ndash; Ram Shastri/Boston Strangler/Raman Raghav/Ram Ghulam
*Pradeep Kumar &ndash; Devraj Indra
*Prem Nath &ndash; Yamraj
*Deven Verma &ndash; Chitragupt Sharma
*Padma Khanna &ndash; Menaka
*Madan Puri &ndash; Kalicharan Agha &ndash; Traffic Constable
*Rajan Haksar &ndash; Gopi
*Roopesh Kumar &ndash; Manager, Punjab Darbar Hotel
*Sunder &ndash; Rude car driver
*Tun Tun &ndash; Sundari
*Pandharibai
*Vijayalalitha
*Aparna
*Vijaya Bhanu
*Manju Bhargavi
*Jayamalini
*N. Salim Khan

==Crew==
*Director &ndash; Rama Rao Tatineni
*Screenplay &ndash; Rama Rao Tatineni
*Dialogue &ndash; Charandas Shokh
*Cinematographer &ndash; S. Venkataratnam
*Editor &ndash; Sanjeeva Rao Akkineni
*Production Company &ndash; Sree Pallavi Productions
*Music Director &ndash; Laxmikant-Pyarelal
*Lyricist &ndash; Anand Bakshi
*Playback Singers &ndash; Asha Bhosle, Kishore Kumar

==Soundtrack==
{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singer(s)
| total_length    =

| all_writing     =
| all_lyrics      = Anand Bakshi
| all_music       = Laxmikant-Pyarelal

| writing_credits =
| lyrics_credits  =
| music_credits   =

| title1          = Aise Nacho Aise Gao
| note1           =
| writer1         =
| lyrics1         =
| music1          =
| extra1          = Kishore Kumar
| length1         = 7:30

| title2          = Amma Ri Amma Ye To Diwana
| note2           =
| writer2         =
| lyrics2         =
| music2          =
| extra2          = Kishore Kumar, Asha Bhosle
| length2         = 4:45

| title3          = Badal Kab Barsoge
| note3           =
| writer3         =
| lyrics3         =
| music3          =
| extra3          = Kishore Kumar, Asha Bhosle
| length3         = 4:30

| title4          = Bedardi Piya
| note4           =
| writer4         =
| lyrics4         =
| music4          =
| extra4          = Asha Bhosle
| length4         = 5:30

| title5          = Ham Tum Jeet Gaye
| note5           =
| writer5         =
| lyrics5         =
| music5          =
| extra5          = Kishore Kumar, Asha Bhosle, Chorus
| length5         = 5:50

| title6          = Yeh Kahdo Yamraj Se
| note6           =
| writer6         =
| lyrics6         =
| music6          =
| extra6          = Kishore Kumar, Chorus
| length6         = 4:10
}}

==External links==
*  

 
 
 
 
 
 
 


 
 