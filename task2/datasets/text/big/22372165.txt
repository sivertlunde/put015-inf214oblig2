The Virgin and the Gypsy (film)
 
{{Infobox film
| name           = The Virgin and the Gypsy
| image          = The Virgin and the Gypsy (film).jpg
| caption        = DVD cover
| director       = Christopher Miles
| producer       = Kenneth Harper
| writer         = D. H. Lawrence Alan Plater
| starring       = Joanna Shimkus
| music          = 
| cinematography = Robert Huke
| editing        = Paul Davies
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
 same name. It was screened at the 1970 Cannes Film Festival, but wasnt entered into the main competition.   

==Plot==
Based on a 1926 work by D. H. Lawrence, the film follows two sisters, Lucille and Yvette, who come to the English countryside to vacation. Along the way, Yvette is interested in a gypsy who lives not far from her familys home. Slowly, Yvette becomes attracted to the gypsy, leading her to consider whether she wants to have a mundane life or a romance with someone her family may not accept.

==Cast==
* Joanna Shimkus as Yvette
* Franco Nero as The Gypsy
* Honor Blackman as Mrs Fawcett Mark Burns as Major Eastwood
* Fay Compton as Grandma
* Maurice Denham as The Rector
* Kay Walsh as Aunt Cissie
* Imogen Hassall as The gypsys wife
* Harriet Harper as Lucille
* Norman Bird as Uncle Fred
* Jeremy Bulloch as Leo
* Ray Holder as Bob
* Margo Andrew as Ella Janet Chappell as Mary
* Helen Booth as Cook
* Laurie Dale as Thomas
* Lulu Davies as Gypsy grandmother

==References==
 

==External links==
* 

 
 

 

 
 
 
 
 
 
 
 
 

 
 