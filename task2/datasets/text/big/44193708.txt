The Missionaries
{{Infobox film
| name           = The Missionaries
| image          = 
| caption        =
| director       = Tonie Marshall
| producer       = Tonie Marshall   Bruno Pésery 
| writer         = Tonie Marshall   Sophie Kovess-Brun   Erwan Augoyard   Nicolas Mercier 
| starring       = Sophie Marceau   Patrick Bruel 
| music          = Philippe Cohen-Solal Christophe Chassol Loïk Dury Christophe "Disco" Minck
| cinematography = Pascal Ridao 	 
| editing        = Jacques Comets Stan Collet
| studio         = Tabo Tabo Films   Arena Productions   Cinéfrance 1888   TF1 Films Production  Entre Chien et Loup
| distributor    = Warner Bros.
| released       =  
| runtime        = 111 minutes
| country        = France
| language       = French
| budget         = €10.51 million 
| gross          = 
}}

The Missionaries ( ), also titled Sex, Love & Therapy,  is a 2014 French romantic comedy film written, produced and directed by Tonie Marshall. The film stars Sophie Marceau and Patrick Bruel.

== Cast ==
* Sophie Marceau as Judith Chabrier 
* Patrick Bruel as Lambert Levallois 
* André Wilms as Michel Chabrier 
* Sylvie Vartan as Nadine Levallois 
* François Morel (actor)|François Morel as Alain 
* Philippe Lellouche as Bruno  
* Jean-Pierre Marielle as himself  
* Patrick Braoudé as Lécureuil 
* Claude Perron as Fabienne Lavial 
* Pascal Demolon as Christian Lavial 
* Marie Rivière as Martine 
* Philippe Harel as Jacques 
* Scali Delpeyrat as Pierre Joubert 
* Camille Panonacle as Valérie Joubert
* Fanny Sidney as Véronique 
* Thomas Sagols as Luc 
* Laurent Heynemann as Le barbu  
* Fabienne Galula as Annie  
* Alexia Barlier as Daphné

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 
 

 