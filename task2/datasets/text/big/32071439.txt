Wreck-It Ralph
 
 
 
{{Infobox film
| name           = Wreck-It Ralph
| image          = Wreckitralphposter.jpeg
| caption        = Theatrical release poster
| alt            = Theatrical release poster depicting Ralph along with various video game characters
| director       = Rich Moore
| producer       = Clark Spencer Jennifer Lee 
| story          = Rich Moore Phil Johnston Jim Reardon
| starring       = John C. Reilly Sarah Silverman Jack McBrayer Jane Lynch
| music          = Henry Jackman
| cinematography =
| editing        = Tim Mertens studio          = Walt Disney Pictures Walt Disney Animation Studios distributor  Walt Disney Studios Motion Pictures
| released       =   
| runtime        = 101 minutes 
| country        = United States
| language       = English
| budget         = $165&nbsp;million   
| gross          = $471.2 million 
}}
 3D Computer-animated Walt Disney Jennifer Lee and Phil Johnston from a story by Moore, Johnston and Jim Reardon. John Lasseter served as the executive producer. The film features the voices of John C. Reilly, Sarah Silverman, Jack McBrayer, and Jane Lynch. The film tells the story of the eponymous arcade game villain who rebels against his role and dreams of becoming a hero. He travels between games in the arcade, and ultimately must eliminate a dire threat that could affect the entire arcade, and one that Ralph himself inadvertently started.

Wreck-It Ralph premiered at the El Capitan Theatre on October 29, 2012,    and went into general release on November 2. The film has earned $471 million in worldwide box office revenue, $189 million of which was earned in the United States and Canada; it was met with critical and commercial success, winning the Annie Award for Best Animated Feature and receiving nominations for the Golden Globe Award for Best Animated Feature Film and the Academy Award for Best Animated Feature.        Wreck-It Ralph was released on Blu-ray Disc|Blu-ray and DVD on March 5, 2013.

==Plot==
  Arcade closes first person rail shooter Heros Duty and enters the game.  He collects the medal between game sessions, but accidentally hatches a Cy-Bug, one of the games enemies, which clings to Ralph as he stumbles into an escape pod that launches him out of the game. Meanwhile, with Ralph missing, a player reports Fix-It Felix, Jr. to Mr. Litwak, who marks it as malfunctioning. Fearing that the game will be unplugged, leaving its characters homeless, Felix leaves to find Ralph.
 glitchy character who takes the medal and uses it to buy entry into a race that determines the games daily roster. The other racers, including the games ruler King Candy, refuse to let Vanellope participate, saying that she was never intended to be part of the game. Sympathetic toward the friendless Vanellope, and with the promise of regaining the medal once she wins the race, Ralph helps build Vanellope a kart and teaches her how to drive. Meanwhile, Felix enters Heros Duty and encounters Sergeant Calhoun, the games no-nonsense leader, who warns that Cy-Bugs will destroy any game they enter. As the pair search for Ralph and the Cy-Bug in Sugar Rush, they separate when Felix, enamored with Calhoun, inadvertently reminds her of her late fiancé. Calhoun finds an enormous clutch of Cy-Bug eggs underground, and Felix becomes imprisoned in King Candys castle.
 hacks the newer game out of jealousy, causing both to be unplugged. As the two race, the Cy-Bug eggs suddenly hatch and begin to consume the game. Vanellope escapes from Turbo, who is consumed by a Cy-Bug.
 attract and eruption to use as a beacon. Before he can finish, Turbo, merged with the Cy-Bug that had consumed him, carries Ralph into the sky. Ralph breaks free and dives into the mountain, intending to sacrifice himself to start the eruption on impact. Vanellope uses her glitching to save Ralph, and the eruption draws Turbo and the Cy-Bugs to their destruction. Vanellope crosses the finish line, restoring her memory and status as Princess Vanellope, the games ruler and lead character. Felix and Ralph return to their game in time to save it from being unplugged. Calhoun and Felix marry, and the characters of Fix-It Felix, Jr. gain a new respect for Ralph.

==Cast==
  and John C. Reilly presenting Wreck-It Ralph at the 2012 San Diego Comic-Con International]]
 
* John C. Reilly as Wreck-It Ralph, the villain of Fix-It Felix, Jr. 
* Sarah Silverman as Vanellope von Schweetz, a racer/glitch in Sugar Rush 
* Jack McBrayer as Fix-It Felix, Jr., the hero of Fix-It Felix, Jr.  
* Jane Lynch as Sergeant Tamora Jean Calhoun, the lead character of Heros Duty 
* Alan Tudyk as King Candy, the ruler of Sugar Rush/Turbo, the former star racer of TurboTime. King Candys vocal stylings are based on comedian Ed Wynn. 
* Mindy Kaling as Taffyta Muttonfudge, a racer in Sugar Rush   
* Joe Lo Truglio as Markowski, a soldier from Heros Duty that Ralph meets in Tappers
* Ed ONeill as Mr. Litwak, owner of Litwaks Family Fun Center & Arcade 
* Dennis Haysbert as General Hologram, a general in Heros Duty
* Adam Carolla as Winchells Donuts|Wynnchel, an Éclair (pastry)|éclair who is a member of the Sugar Rush police station 
* Horatio Sanz as Dunkin Donuts|Duncan, a doughnut who is a member of the Sugar Rush police station 
* Rich Moore as Sour Bill, King Candys sour ball henchman    
 Beard Papa, Kano from Martin Jarvis as Saitine, a devil-like villain, who attend the Bad-Anon support group; Tucker Gilmore as the Sugar Rush Announcer; Brandon Scott as Kohut, a soldier in Heros Duty; and Tim Mertens as Dr. Brad Scott, a scientist and Sgt. Calhouns fiancé in Heros Duty (voiced by Nick Grimshaw in the UK release). 
 Clyde (Kevin Deters) from Pac-Man;  and Yuni Verse (Jamie Sparer Roberts) from Dance Dance Revolution.    A character modeled after dubstep musician Skrillex makes an appearance in Fix-It Felix, Jr. as the DJ at the anniversary party of the game. 

==Video game cameos and references==
 , Ghosts (Pac-Man)|Clyde, Doctor Eggman, M. Bison, Neff, and Zangief.]]
 
 Bowser from Sonic the Hedgehog,     and Neff from Altered Beast.    Donkey Kong arcade game, with Ralph and Felix taking similar poses as Donkey Kong and Mario, respectively.
Characters from Q*bert, including Q*bert, Coily, Slick, Sam, and Ugg, are shown as "homeless" characters and later taken in by Ralph and Felix into their game (Q*bert also speaks to Felix at one point using the signature synthesized gibberish and word-balloon symbols from his game, called Q*bert-ese).     Scenes in Game Central Station and Tappers bar include Chun-Li, Cammy, and Blanka from Street Fighter,     Pac-Man (character)|Pac-Man, Ghosts (Pac-Man)|Blinky, Pinky, and Inky from Pac-Man,     the Paperboy from Paperboy (video game)|Paperboy,   the two paddles and the ball from Pong,    Dig Dug, a Pooka, and a Fygar from Dig Dug,  The Qix from Qix,  Frogger from Frogger, and Peter Pepper from BurgerTime.  Additionally, Lara Croft and Mario are referenced, but not seen.    
 NES controller to access the programming of Sugar Rush.  Throughout Game Central Station is graffiti that includes "Aerith lives" (referencing the character of Aerith Gainsborough from Final Fantasy VII),     "All your base are belong to us" (an Engrish phrase popularized from the game Zero Wing), "Sheng Long Was Here" (referencing an April Fools joke around a made-up character Sheng Long from Street Fighter), and "Jenkins" (a nod to the popular Leeroy Jenkins meme from World of Warcraft).    There is also a reference to the Metal Gear series when Ralph is searching for a medal in Tappers Lost and found, finding first a Super Mushroom from the Mario franchise, and then Metal Gears "Exclamation point" (with the corresponding sound effect from the game).    Mr. Litwak wears a black and white striped referees shirt, a nod to the iconic outfit of Twin Galaxies founder Walter Day.  One of the songs in the credits is an original work from Buckner and Garcia, previously famous for writing video game-themed songs in the 1980s.  The Walt Disney Animation Studios opening logo is animated in an 8-bit pixelated fashion,  whereas the Walt Disney Pictures closing production logo appears in a glitched state, a reference to the kill screen from many early arcade games such as Pac-Man. 

==Production==
The concept of Wreck-It Ralph was first developed at Disney in the late 1980s, under the working title High Score. Since then, it was redeveloped and reconsidered several times: In the late 1990s, it took on the working title Joe Jump, then in the mid-2000s as Reboot Ralph.  

  at the 2012 San Diego Comic-Con International]] Toy Story Clyde from Sonic the Bowser from Super Mario Bros.  Rich Moore, the films director, had determined that for a film about a video-game world to feel authentic, "it had to have real characters from real games in it."    Moore aimed to add licensed characters in a similar manner as cultural references in Looney Tunes shorts, but considered "having the right balance so a portion of the audience didn’t feel they were being neglected or talked down to."  However, Moore avoided creating the movie around existing characters, feeling that "there’s so much mythology and baggage attached to pre-existing titles that I feel someone would be disappointed," and considered this to be a reason why movies based on video game franchises typically fail.    Instead, for Ralph, the development of new characters representative of the 8-bit video game was "almost like virgin snow," giving them the freedom to take these characters in new directions. 
 Bowser as a major character within the scene; according to Moore, Nintendo was very positive towards this use, stating in Moores own words, "If there is a group that is dedicated to helping the bad guy characters in video games then Bowser must be in that group!"    Nintendo had asked that the producers try to devise a scene that would be similarly appropriate for Mario for his inclusion in the film. Despite knowing they would be able to use the character, the producers could not find an appropriate scene that would let Mario be a significant character without taking away the spotlight from the main story, and opted to not include the character.   Moore debunked a rumor that Mario and his brother character Luigi were not included due to Nintendo requesting too high a licensing fee, stating that the rumor grew out of a joke John C. Reilly made at Comic-Con. 
Dr. Wily from Mega Man was going to appear, but was cut from the final version of the film.  Overall, there are about 188 individual character models in the movie as a result of these cameo inclusions. 
 Grand Theft Auto, according to Moore.    Ralph would go there to, wallowing in his depression, and would find happiness by gaining "Like It" buttons for doing acceptable actions in the party-like nature of the place. Moore stated that while it was difficult to consider dropping this new game world, they found that its introduction in the second half of the film would be too difficult a concept for the viewer to grasp.  They further had trouble working out how a social game would be part of an arcade, and though they considered having the game be running on Litwaks laptop, they ultimately realized that justifying the concept would be too convoluted.  Line art sketches and voiceover readings of the scene were included on the home media release of the film. 

The film introduced Disneys new bidirectional reflectance distribution functions, with more realistic reflections on surfaces, and new virtual cinematography Camera Capture system, which makes it possible to go through scenes in real-time.  To research the Sugar Rush segment of the film, the visual development group traveled to trade fair ISM Cologne, a Sees Candy factory, and other manufacturing facilities. The group also brought in food photographers, to demonstrate techniques to make food appear appealing. Special effects, including from "smoke or dust," looks distinct in each of the segments.   

==Release== arcade cabinet.]]

The film was originally scheduled for a release on March 22, 2013, but it was later changed to November 2, 2012 due to it being ahead of schedule.   The theatrical release was accompanied by Disneys Oscar-winning, animated short film Paperman.  

===Marketing=== Rock of arcade cabinet for the fictional Fix-It Felix, Jr. game on display on the show floor.  Disney also released a browser-based Adobe Flash|Flash-based version of the Fix-It Felix, Jr. game as well as iOS and Android versions, with online Unity (game engine)|Unity-based versions of Sugar Rush and Heros Duty.  A second trailer for the film was released on September 12, 2012, coinciding with Finding Nemo 3D and Frankenweenie (2012 film)|Frankenweenie.

To promote the Blu-ray/DVD release of Wreck-It Ralph, director Rich Moore produced a short film titled Garlan Hulse: Where Potential Lives. Set within the movies universe, the mockumentary film was designed as a parody of The King of Kong. 

===Home media===
Wreck-It Ralph was released on Blu-ray Disc Founders|Blu-ray Disc (2D and 3D) and DVD in North America on March 5, 2013 from Walt Disney Studios Home Entertainment.  The film was made available for digital download in selected regions on February 12, 2013.  Wreck-It Ralph debuted at #1 in Blu-ray and DVD sales in the United States. 

==Reception==

===Box office performance===
Wreck-It Ralph grossed $189,422,889 in North America, and $281,800,000 in other countries, for a worldwide total of $471,222,889.  It is the 14th-highest-grossing film of 2012,  the fourth-highest-grossing 2012 animated film, and the fifth-highest-grossing film produced by Walt Disney Animation Studios. 

In North America, the film debuted with $13.5 million, an above-average opening-day gross for an animated film released in November.  During its opening weekend, the film topped the box office with $49.0 million, making it the largest opening for a Walt Disney Animation Studios film until 2013, when it was surpassed by Frozen (2013 film)|Frozen ($67.4 million).  

Outside North America, Wreck-It Ralph earned $12 million on its opening weekend from six markets.  Among all markets, its three largest openings were recorded in the UK, Ireland and Malta ($7.15 million), Brazil ($5.32 million with weekday previews), and Russia and the CIS ($5.27 million).    In total grosses, the three largest markets were the UK, Ireland and Malta ($36.2 million), Japan ($29.6 million), and Australia ($24.0 million). 

===Critical response=== normalized rating out of 100 top reviews from mainstream critics, calculated a score of 72 based on 36 reviews, indicating "generally favorable reviews".  The film earned an "A" from audiences polled by CinemaScore. 
 Christopher Orr of The Atlantic found it "overplotted and underdeveloped." 

===Accolades===
{| class="wikitable plainrowheaders sortable" style="width:100%;"
|- style="background:#ccc; text-align:center;"
! colspan="4" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! scope="col"| Award
! scope="col"| Category
! scope="col"| Recipients and nominees
! scope="col"| Result
|- Academy Awards  Best Animated Feature
| Rich Moore
|  
|-
| rowspan=10 | Annie Awards   Best Animated Feature
|
|  
|-
| Animated Effects in an Animated Production
| Brett Albert
|  
|-
| Character Design in an Animated Feature Production
| Bill Schwab, Lorelay Bove, Cory Loftis, Minkyu Lee
|  
|-
| Directing in an Animated Feature Production
| Rich Moore
|  
|-
| Music in an Animated Feature Production
| Henry Jackman, Skrillex, Adam Young, Matthew Thiessen, Jamie Houston, Yasushi Akimoto
|  
|-
| rowspan=2 | Storyboarding in an Animated Feature Production
| Leo Matsuda
|  
|-
| Lissa Treiman
|  
|-
| Voice Acting in an Animated Feature Production
| Alan Tudyk
|  
|-
| Writing in an Animated Feature Production Jennifer Lee
|  
|-
| Editorial in an Animated Feature Production
| Tim Mertens
|  
|-
| Chicago Film Critics Association Best Animated Feature
|
|  
|-
| Critics Choice Movie Awards  Broadcast Film Best Animated Feature
|
|  
|- Golden Globe Awards  Best Animated Feature Film
|
|  
|- Golden Reel Awards 
| Best Sound Editing: Sound Effects, Foley, Dialogue and ADR in an Animation Feature Film
|
|  
|-
| Golden Trailer Awards 
| Best Animation/Family
| "Dreams"
|  
|-
| rowspan=5| IGNs Best of 2012 Awards
| Best Movie
|
|  
|-
| Best Animated Movie
|
|  
|-
| IGN Peoples Choice Award for Best Animated Movie
|
|  
|-
| Best 3D Movie
|
|  
|-
| Best Movie Poster
|
|  
|- National Board of Review Awards  Best Animated Feature
|
|  
|- Nickelodeon Kids Choice Awards Favorite Animated Movie
|
|  
|-
| Online Film Critics Society Award Best Animated Feature
|
|  
|-
| Producers Guild of America Award
| Best Animated Motion Picture
| Clark Spencer
|  
|- Satellite Awards  Best Animated or Mixed Media Feature
| Rich Moore
|  
|- 39th Saturn Saturn Awards  Saturn Award Best Animated Film Rich Moore
| 
|- Visual Effects Society   Outstanding Animation in an Animated Feature Motion Picture
| Sean Jenkins, Scott Kersavage, Rich Moore, Clark Spencer
|  
|-
| Outstanding Animated Character in an Animated Feature Motion Picture
| John Kahwaty, Suzan Kim, Michelle Robinson, Tony Smeed (for Vanellope)
|  
|}

==Soundtrack==
The films score was composed by Henry Jackman. The soundtrack also features original songs by Owl City, AKB48, Skrillex, and Buckner & Garcia.    

{{Infobox album
| Name        = Wreck-It Ralph: Original Motion Picture Soundtrack
| Type        = soundtrack
| Artist      = Various artists
| Cover       =
| Released    = October 30, 2012 Sony Scoring Stage  (Score) 
| Genre       =
| Length      = 70:36 Walt Disney
| Producer    = {{flat list|
* Chris Montan
* Tom MacDougall}}
| Chronology  = Walt Disney Animation Studios
| Misc         = {{Extra chronology
 | Artist     = Walt Disney Animation Studios
 | Type       = soundtrack Winnie the Pooh (2011)
 | This album  = Wreck-It Ralph (2012)
 | Next album  = Frozen (soundtrack)|Frozen (2013)
}}
{{Extra chronology
 | Artist     = Henry Jackman
 | Type       = soundtrack
 | Last album =   (2012)
 | This album = Wreck-It Ralph (2012)
 | Next album =   (2013)
}}
{{Singles
 | Name            = Wreck-It Ralph
 | Type         = soundtrack
 | Single 1      = When Can I See You Again?
 | Single 1 date = 2012
}}
}}

{{Track listing
| total_length = 70:36
| all_music    = Henry Jackman (except 1–6) 
| music_credits = no
| writing_credits  = yes
| headline     =
| extra_column = Artist
| title1       = When Can I See You Again?
| extra1       = Owl City Brian Lee
| length1      = 3:38
| title2       = Wreck-It, Wreck-It Ralph
| extra2       = Buckner & Garcia
| writer2      = Jamie Houston
| length2      = 2:59 Celebration
| extra3       = Kool & the Gang
| length3      = 3:40 Sugar Rush
| extra4       = AKB48
| writer4      = Yasushi Akimoto and Jamie Houston
| length4      = 3:14
| title5       = Bug Hunt (Noisia Remix)
| extra5       = Skrillex
| writer5      = Skrillex
| note5        = featuring John C. Reilly
| length5      = 7:04 Shut Up and Drive
| extra6       = Rihanna
| length6      = 3:32
| title7       = Wreck-It Ralph
| length7      = 1:33
| title8       = Life in the Arcade
| length8      = 0:43
| title9       = Jumping Ship
| length9      = 1:06
| title10      = Rocket Fiasco
| length10     = 5:48
| title11      = Vanellope von Schweetz
| length11     = 2:57
| title12      = Royal Raceway
| length12     = 3:23
| title13      = Cupcake Breakout
| length13     = 1:12
| title14      = Candy Vandals
| length14     = 1:39
| title15      = Turbo Flashback
| length15     = 1:42
| title16      = Laffy Taffies
| length16     = 1:35
| title17      = One Minute to Win It
| length17     = 1:17
| title18      = Vanellopes Hideout
| length18     = 2:33
| title19      = Messing with the Program
| length19     = 1:20
| title20      = King Candy
| length20     = 2:11
| title21      = Broken-Karted
| length21     = 2:49
| title22      = Out of the Penthouse, Off to the Race
| length22     = 2:51
| title23      = Sugar Rush Showdown
| length23     = 4:15
| title24      = Youre My Hero
| length24     = 4:16
| title25      = Arcade Finale
| length25     = 3:19
}}

==Video games== Flash version Disney Interactive and Activision and serves as a "story extension" to the film. Taking place following the events of the film, players may play as Wreck-It Ralph or Fix-It Felix, causing or repairing damage, respectively, following another Cy-Bug incident. Game levels are based on the locations in the film like the Fix-It Felix, Jr., Heros Duty, and Sugar Rush games as well as Game Central Station. It was released in conjunction with the films release, in November 2012. 
 Android systems,  with a Windows Phone 8 version following almost a year later.  Initially, the game consisted of three mini-games, Fix-it Felix Jr., Heros Duty and Sweet Climber, which were later joined by Turbo Time and Flight Command.   The game was retired on August 29, 2014. 

Ralph also appears in Segas Sonic & All-Stars Racing Transformed as a playable guest character.  Ralph and Vanellope appear as playable characters in Disney Infinity as well; the Disney Store released their individual figures on January 7, 2014.            A combo "toy box pack" of the two figures with Sugar Rush customization discs was released April 1, 2014 from the Disney Store.    

==Future plans== online gaming console gaming. Tron in the sequel.       In a 2014 interview, the films composer Henry Jackman said that a story for the sequel is being written. 

==References==
 

==External links==
 
 
 
*  
*  
*  
*  
*  
*   at Walt Disney Animation Studios

 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 