Napoleon Dynamite
 
{{Infobox film
| name           = Napoleon Dynamite
| image          = Napoleon dynamite post.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster Jared Hess
| producer       = {{Plainlist |
*Jeremy Coon Chris Wyatt
*Sean C. Covel
*Jory Weitz
}}
| screenplay     = {{Plain list |
*Jared Hess Jerusha Hess
}}
| based on       =  
| starring       = {{Plainlist |
*Jon Heder
*Jon Gries
*Efren Ramirez
*Tina Majorino
*Aaron Ruell
*Diedrich Bader
*Haylie Duff
}}
| music          = John Swihart
| cinematography = Munn Powell
| editing        = Jeremy Coon
| studio         = {{Plainlist |
*MTV Films Napoleon Pictures
}}
| distributor    = {{Plainlist |
*Fox Searchlight Pictures  
*Paramount Pictures  
}}
| released       =  
| runtime        = 95 minutes 
| country        = United States
| language       = English
| budget         = $400,000   
| gross          = $46.1 million 
}}
Napoleon Dynamite is a 2004 American independent art-house comedy film written by Jared and Jerusha Hess and directed by Jared. The film stars Jon Heder in the role of the title character, for which he was paid $1,000. After the films runaway success, Heder re-negotiated his compensation and received a cut of the profits.    The film was Jared Hess first full-length feature and is partially adapted from his earlier short film, Peluca.

Napoleon Dynamite was acquired at the Sundance Film Festival by Fox Searchlight Pictures and Paramount Pictures, in association with MTV Films. It was filmed in and near Franklin County, Idaho in the summer of 2003. It debuted at the Sundance Film Festival in January 2004, and in June 2004 was released on a limited basis. Its widespread release followed in August. The films total worldwide gross revenue was $46,140,956.  The film has acquired a cult following.

==Plot== cage fighter; their grandmother leads a secret life involving mysterious boyfriends and All-terrain vehicle|quad-biking in the desert. Napoleon daydreams his way through school, doodling ligers and fantasy creatures, and reluctantly deals with the various bullies who torment him, particularly the obnoxious sports jock Don and the selfish, out-of-shape Randy. Napoleon likes to make up stories about himself whilst assuming a sullen and aloof attitude.

Napoleons grandmother breaks her coccyx in a quad-bike accident and asks their Uncle Rico to look after the boys while she recovers. Rico, a middle-aged and flirtatious former athlete who lives in a  campervan, uses the opportunity to team up with Kip in a get-rich-quick scheme to sell numerous items door-to-door. Kip wants money to visit his Internet girlfriend LaFawnduh, while Rico believes riches will help him get over his failed dreams of NFL stardom and the loss of his girlfriend.

Napoleon becomes friends with two students at his school: Deb, a shy girl who runs various small businesses to raise money for college; and Pedro, a quiet transfer student from Ciudad Juárez. A friendship soon develops between the three outcasts. During this time, preparations begin for the high school dance. Pedro asks Deb to be his partner, which she accepts, with Napoleon choosing and attempting to persuade a reluctant classmate called Trisha. Inspired by a poster at the dance, Pedro decides to run for class president, pitting him against Summer Wheatley, a popular and snobby girl at the school. The two factions put up flyers and give out accessories to the students to attract voters. Kip, meanwhile, is head-over-heels in love since LaFawnduh has come to visit him from Detroit. She gives Kip an urban makeover, outfitting him in hip hop regalia.

Rico and Kips ongoing scheme causes friction with Napoleon when Rico begins spreading embarrassing rumors about him, to evoke sympathy from his prospective customers. Rico later tries to sell Deb a breast-enhancement product, claiming it was Napoleons suggestion, which causes her to break off their friendship. His scheme ends when he visits the wife of the towns martial arts instructor, Rex, to try and sell her the breast enhancements, but Rex walks in and beats him up after witnessing Ricos demonstration.
 Canned Heat" by Jamiroquai. The routine receives a standing ovation from everyone except for Summer and her boyfriend Don.

The film concludes with Pedro becoming the class president, LaFawnduh leaving with Kip on a bus for Michigan, Rico reuniting with his estranged girlfriend, Grandma returning from the hospital, and Napoleon and Deb making up and playing tetherball.

In a post-credits scene, Kip and LaFawnduh are married in an outdoor ceremony in Preston. Napoleon, absent for the vows, arrives riding a horse, claiming that it is a "wild honeymoon stallion" that he has tamed himself. Kip flicks an accessory of LaFawnduhs dress as a "keepsake" towards Napoleon, Rico and Pedro, before he and LaFawnduh ride off across the fields.

==Cast==
 
* Jon Heder as Napoleon Dynamite
* Efren Ramirez as Pedro Sánchez
* Tina Majorino as Deb Bradshaw
* Aaron Ruell as Kip Dynamite
* Jon Gries as Rico Dynamite
* Haylie Duff as Summer Wheatley Emily Kennard as Trisha Jenner
* Shondrella Avery as LaFawnduh
* Sandy Martin as Grandma
* Diedrich Bader as Rex
* Trevor Snarr as Don

==Background==

===Setting===
Preston is a real town in Southeastern Idaho, located near the Utah border. Since the release of Napoleon Dynamite, it has become a tourist attraction of sorts, with the high school being a main feature. Preston held a Napoleon Dynamite Festival every summer from 2004 through 2008 to celebrate the filming of Napoleon Dynamite in Preston and nearby towns. Napoleon Dynamite was shot in the towns of Preston, Idaho and Richmond, Utah. 

==Production==

===Opening sequence=== Lemonheads box or a tube of ChapStick had the credits printed on them. Hess explains: 

 

===Origin of the name "Napoleon Dynamite"=== missionary work for the Church of Jesus Christ of Latter-day Saints (LDS Church).    

Costello believes that Hess got the name from him, whether directly or indirectly. Costello said, "The guy just denies completely that I made the name up&nbsp;... but I invented it. Maybe somebody told him the name and he truly feels that he came about it by chance. But its two words that youre never going to hear together." Contact Music article: " ".  Costello has taken no legal action against the film.

==Reception==
  magazine complimented the film, saying, "Hess and his terrific cast — Heder is geek perfection — make their own kind of deadpan hilarity. Youll laugh till it hurts. Sweet."  The Christian Science Monitor called the film "a refreshing new take on the overused teen-comedy genre" and said that the film "may not make you laugh out loud — its too sly and subtle for that — but it will have you smiling every minute, and often grinning widely at its weirded-out charm."  Michael Atkinson of The Village Voice praised the film as "an epic, magisterially observed pastiche on all-American geekhood, flooring the competition with a petulant shove."  In a mixed review, The New York Times praised Heders performance and the "films most interesting quality, which is its stubborn, confident, altogether weird individuality", while criticizing the films resolution that comes "too easily."  Film critic Roger Ebert gave the film one-and-a-half stars, writing that he felt that "the movie makes no attempt to make   likable" and that it contained "a kind of studied stupidity that sometimes passes as humor".  At the time, Entertainment Weekly gave the film a C-. 

Entertainment Weekly later ranked Napoleon #88 on its 2010 list of The 100 Greatest Characters of the Last 20 Years, saying, "A high school misfit found a sweet spot, tapping into our inner dork."  The film was on several year-end lists. Rolling Stone placed it at number 22 of the 25 Top DVDs of 2004. 
 Lost in Translation and I Heart Huckabees prove difficult for researchers to create algorithms that are able to predict whether or not a particular viewer will like the film based on their ratings of previously viewed films. 
 limited initial release, Napoleon Dynamite was a commercial success. It was filmed on an estimated budget of a mere $400,000, and less than a year after its release, it had grossed $44,940,956. It also spawned a slew of merchandise, from refrigerator magnets to T-shirts to Halloween costumes. Probably the biggest boost was obtained from the "Vote for Pedro" T-shirts that coincided with the popularity of pitcher Pedro Martínez in 2004. 

==Awards== major distributor, Fox Searchlight Pictures, Fox supplied additional funds for the post-credits scene.
* In 2005, the film — itself an MTV Films production — won three MTV Movie Awards, for Breakthrough Male Performance, Best Musical Performance, and Best Movie. The film is #14 on Bravo (US TV channel)|Bravos "100 Funniest Movies".
* It won the 2005 Golden Trailer Awards for Best Comedy.
* It won the 2005 Golden Satellite Award for Best Original Score (John Swihart).
* Four awards at the Teen Choice Awards. Best Movie Breakout Performance - Female for Haylie Duff, Best Movie Dance Scene, Best Movie Hissy Fit for Jon Heder, and Best Comedy.
* The 2004 Film Discovery Jury Award for Best Feature
* April 2005, the Idaho Legislature approved a resolution commending the filmmakers for producing Napoleon Dynamite, specifically enumerating the benefits the movie has brought to Idaho, as well as for showcasing various aspects of Idahos culture and economy.  

==Soundtrack==
 

==Animated series==
 
  Fox announced Fox had canceled the series.  The complete series was released on DVD on November 4, 2014 by Olive Films. 

==Lawsuit==
On August 30, 2011, Napoleon Pictures filed a lawsuit against Fox Searchlight for $10 million for underreporting royalties and taking improper revenue deductions. In its term sheet, Fox agreed to pay 31.66 percent of net profits on home video. The lawsuit says that a 2008 audit revealed that Fox was only paying net royalties on home videos at a 9.66 percent rate, and there were underreported royalties and improper deductions.   

Napoleon Pictures also alleges that Fox has breached the agreement in multiple other respects, including underreporting pay television license fees, failing to report electronic sell-through revenue, charging residuals on home video sales, as well as overcharging residuals on home video sales, deducting a number of costs and charges Fox has no right to deduct and/or for which there is no supporting documentation. 

In May 2012, Fox went to trial after failing to win a summary judgment on the case. The trial began on June 19, 2012.   On November 28, 2012, a 74-page decision sided with Fox on 9 of the 11 issues. Napoleon Pictures was awarded $150,000 based on Fox accounting irregularities. 

==See also== Napoleon Dynamite Festival
*  

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 