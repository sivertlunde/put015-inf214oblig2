Dead Rising: Watchtower
{{Infobox film
| name           = Dead Rising: Watchtower
| image          = Dead Rising Watchtower poster.jpg
| alt            =
| caption        = Release poster
| director       = Zach Lipovsky
| exec producer  = Tomas Harlan 
| producer       = Tim Carter
                 Tomas Harlan
| writer         = Tim Carter
| based on       = Dead Rising (video game)
| starring       = Jesse Metcalfe   Meghan Ory   Virginia Madsen   Dennis Haysbert    Keegan Connor Tracy
| music          = Oleksa Lozowchuk
| cinematography = Mahlon Todd Williams
| editing        = Mike Jackson
| studio         = Legendary Digital Media   Contradiction Films   Dead Rising Productions Crackle
| released       =  
| runtime        = 118 mins
| country        = Canada   United States
| language       = English
| budget         = 
| gross          =  
}}
 action horror horror zombie of same Crackle on March 27, 2015.

== Plot ==
Set in A small town where A zombie outbreak purges the town and infects its inhabitants after a failed government vaccine.

== Cast ==
* Jesse Metcalfe as Chase Carter
* Meghan Ory as Crystal ORourke
* Virginia Madsen as Maggie
* Keegan Connor Tracy as Jordan
* Aleks Paunovic as Logan
* Dennis Haysbert as General Lyons Gary Jones as Norton
* Carrie Genzel as Susan Collier Frank West
* Reese Alexander as Shearson
* Harley Morenstein as Pyro
* Julia Benson as Amy
* Peter Benson as Bruce Jen and Sylvia Soska as Zombies (cameo; uncredited) Ryan Connolly as  Zombie (for Film Riot episode; uncredited) 

== Production == Crackle would handle domestic rights of the film and which would be distributed internationally by Content Media Corp, and it would also be released on SVOD, DVD, VOD and TV after its exclusive stream on Crackle.  On August 20, visual effects artist Zach Lipovsky was set to direct the film. 
 Frank West, Frank West , Paunovic would play Logan, the head of a biker gang, Morenstein would play his second-in-command, Pyro, while Tracy would play the role of a straight-laced journalist. 

== Filming ==
The principal photography on the film began in Vancouver, British Columbia, on September 30, 2014.  On October 21, a photo from the set was revealed by Crackle. 

==Release== Crackle on March 27, 2015. 

==Sequel==
A sequel was announced for Spring 2016 on Crackle.  

== References ==
 

== External links ==
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 