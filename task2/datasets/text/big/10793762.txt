Bruce Gentry – Daredevil of the Skies
 
{{Infobox film
| name           = Bruce Gentry – Daredevil of the Skies
| image size     =
| image	         = Bruce Gentry FilmPoster.jpeg
| caption        = Thomas Carr
| producer       = Sam Katzman
| writer         = Lewis Clay George H. Plympton Joseph F. Poland Ray Bailey (character)
| narrator       = Ralph Hodges Tristram Coffin
| music          = Mischa Bakaleinikoff
| cinematography = Ira H. Morgan Earl Turner
| distributor    = Columbia Pictures
| released       =  
| runtime        = 15 chapters
| country        = United States English
| budget         =
}}
 movie serial Bruce Gentry comic strip created by Ray Bailey.  It features the first cinematic appearance of a flying saucer, as the secret weapon of the villainous Recorder. 

==Plot== mysterious enemy agent, "The Recorder" who only issues orders through recordings. Benson is used to perfect the villains flying saucers, launched and controlled by electronic means. Industrialist Paul Radcliffe (Hugh Prosser) hires Bruce to investigate the saucers as he thinks they may have a commercial use.
 Ralph Hodges), has run dry and he needs to steal supplies from the US Government.
 Tristram Coffin), one of his henchmen, releases a deadly flying saucer on an attack against the Panama Canal. In his aircraft, Bruce intercepts the saucer, crashing into it, and escaping the resultant explosion by taking to his parachute. Back at The Recorders headquarters, the saucer controls explode, killing all the enemy agents.

===Cliffhangers===
At the end of chapter 14, Gentry drives over a cliff on a motorbike.  In the resolution at the beginning of chapter 15, Gentry is replaced by an animated sequence which shows him escaping death by use of a parachute hidden under his jacket. The cliffhangers, and their resolutions, in chapters one and 12 are almost identical. 

==Cast==
* Tom Neal as Bruce Gentry, "Daredevil of the Skies" and charter pilot
* Judy Clark as Juanita Farrell, Young rancher whom The Recorder is trying to chase away from her land Ralph Hodges as Frank Farrell, Young rancher whom The Recorder is trying to chase away from his land
* Forrest Taylor as Dr Alexander Benson, Kidnapped scientist
* Hugh Prosser as Paul Radcliffe, Industrialist who hires Bruce Gordon to investigate Tristram Coffin as Krendon, Lead henchman of The Recorder Jack Ingram as Allen Terry Frost as Chandler
* Eddie Parker as Gregg Charles King as Ivor Stephen Carr as Adrian Hill
* Dale Van Sickel as Gregory, US Government Agent

==Production==
The flying disc is described by Harmon and Glut as "an embarrassingly bad animated cartoon drawn over the action scenes." Animation also appears in the resolution of a cliffhanger, in which an animated Gentry is used instead of a stuntman. Harmon 1973, pp. 158–159. 

The flying disc, however, may be the first cinematic appearance of a flying saucer. Greer 2009, p. 33. 

==Chapter titles==
# The Mysterious Disc
# The Mine of Menace
# Fiery Furnace
# Grande Crossing
# Danger Trail
# A Flight for Life
# A Flying Disc
# Fate Takes the Wheel
# Hazardous Heights
# Over the Falls
# Gentry at Bay
# Parachute of Peril
# Menace of the Mesa
# Bruces Strategy
# The Final Disc
 Source:  

==Critical reception==
According to Harmon and Glut, Bruce Gentry was "one of Columbias closest attempts at imitating the serials of Republic, a studio known for superbly staged action sequences" but it did not equal Republics standards. 

Film historian William Cline describes the serial as a "pretty good airplane adventure." Cline 1984, p. 27. 

==References==

===Notes===
 

===Bibliography===
 
* Cline, William C. "2. In Search of Ammunition". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Cline, William C. "Filmography". In the Nick of Time. Jefferson, North Carolina: McFarland & Company, Inc., 1984. ISBN 0-7864-0471-X.
* Greer, John Michael. The UFO Phenomenon: Fact, Fantasy and Disinformation. Woodbury, Minnesota: Llewellyn Publications, 2009. ISBN 978-0-73871-319-9.
* Harmon, Jim and Donald F. Glut. "7. The Aviators "Land That Plane at Once, You Crazy Fool". The Great Movie Serials: Their Sound and Fury. London: Routledge, 1973. ISBN 978-0-7130-0097-9.
* Weiss, Ken and Ed Goodgold. To be Continued ...: A Complete Guide to Motion Picture Serials. New York: Bonanza Books, 1973. ISBN 0-517-166259
 

==External links==
*  
*  

 
{{Succession box Columbia Serial Serial
| Congo Bill (1948 in film|1948)
| years=Bruce Gentry – Daredevil of the Skies (1949 in film|1949) Batman and Robin (1949 in film|1949)}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 