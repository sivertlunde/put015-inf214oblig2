The Lazarus Project
 
 
{{Infobox Film
| name           = The Lazarus Project
| image          = TheLazarusProject.JPG
| caption        = DVD cover John Patrick Glenn
| producer       = David Hoberman Todd Lieberman  Travis Adam Wright John Patrick Glenn Evan Astrowsky
| starring       = Paul Walker Linda Cardellini Piper Perabo Malcolm Goodwin Tony Curran Bob Gunton Lambert Wilson Brian Tyler
| cinematography = Jerzy Zielinski
| editing        = Fred Raskin
| distributor    = Sony Pictures
| released       =   Monsters and Critics }}
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} John Patrick Glenn. It stars Paul Walker as Ben, a former criminal who gets a second chance at life and mysteriously ends up working at a psychiatric hospital.   Sony Pictures Entertainment  Piper Perabo, Linda Cardellini, Malcolm Goodwin, Tony Curran and Bob Gunton also star in the film, which was released on DVD on October 21, 2008. 

==Plot== Sentenced to cheated death or if he has become part of a far more sinister plan for him.    Reuters 

==Cast==
*Paul Walker as Ben Garvey, an ex-con who gets a second chance at life and begins work at a psychiatric hospital.
*Linda Cardellini as Julie Ingram, the hospitals psychiatrist who takes interest in helping Ben.   BeyondHollywood.com 
*Malcolm Goodwin as Robbie, one of the hospitals patients. 
*Tony Curran as William Reeds, an extremely violent psychopath who claims to know why Ben is at the hospital. 
*Bob Gunton as Father Ezra, a priest who also runs the hospital. 
*Piper Perabo as Lisa Garvey, Bens wife. 
*Lambert Wilson as Avery, a mysterious figure that keeps appearing to offer unwanted guidance. 
*Shawn Hatosy as Ricky Garvey, Bens brother.
*Brooklynn Proulx as Katie Garvey, Bens daughter.

==Themes==
Relevant issues in The Lazarus Project place emphasis on cheating death. The film closely relates to the Biblical story of Lazarus of Bethany which is found in chapter 11 of the Gospel of St.John. 

==Production==
Written and directed by John Patrick Glenn in his directorial debut, production was originally slated for September 2005 with Mandeville Films  but was ultimately delayed. Production finally began in March 2007 with filming beginning on April 27, 2007 in Brandon, Manitoba|Brandon, Manitoba, Canada. 

The film marked the first company credit for Be Good Productions, a production company created by Paul Walker, Brandon Birtell and Tim Whitcome.   Variety 
 Brian Tyler recorded the score for the film. He stated that, "This one is going to be an ensemble type of score, very introspective, very heartfelt, it is a movie about purgatory and its going to be different than anything Ive done before, and I am really happy and very excited doing it." 

===Music=== Brian Tyler specifically for the film. 
{{Track listing
| collapsed       = 
| headline        =

| writing_credits = yes

| title1          = Jaybird
| writer1         = Brian Tyler
| length1         = 3:30

| title2          = The Lazarus Project
| writer2         = Brian Tyler
| length2         = 3:00

| title3          = Discovery
| writer3         = Brian Tyler
| length3         = 5:56

| title4          = The Divide
| writer4         = Brian Tyler 
| length4         = 2:33

| title5          = Cold Harbor
| writer5         = Brian Tyler
| length5         = 1:52

| title6          = Avery
| writer6         = Brian Tyler
| length6         = 3:07

| title7          = Julie in the Cabin
| writer7         = Brian Tyler
| length7         = 3:20

| title8          = A New Life
| writer8         = Brian Tyler
| length8         = 3:20

| title9          = The Break In
| writer9         = Brian Tyler
| length9         = 4:08

| title10         = Passagesscapia
| writer10        = Brian Tyler
| length10        = 3:01

| title11         = The Jumper
| writer11        = Brian Tyler
| length11        = 2:46

| title12         = Glimpses
| writer12        = Brian Tyler
| length12        = 2:27

| title13         = The Revelation
| writer13        = Brian Tyler
| length13        = 3:02

| title14         = Voices from the Past
| writer14        = Brian Tyler
| length14        = 2:53

| title15         = The Forest
| writer15        = Brian Tyler
| length15        = 2:33

| title16         = Portraits
| writer16        = Brian Tyler
| length16        = 3:30

| title17         = I Want to Forget
| writer17        = Brian Tyler
| length17        = 2:30

| title18         = The Lazarus Project End Title
| writer18        = Brian Tyler
| length18        = 3:52
}}

==Reception==
The Lazarus Project generally received mixed reactions from critics. The critic review Rotten Tomatoes reported that 44% of audience critics gave the film a positive review. The overall audience gave the film a rating of 3.9 out of 5. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 