The Weight (film)
{{Infobox film
| name           = The Weight
| image          = File:The_weight_poster.jpg
| film name =   
 | rr             = Muge
 | mr             = Muge}}
| director       = Jeon Kyu-hwan
| writer         = Jeon Kyu-hwan
| starring       = Cho Jae-hyun   Park Ji-a
| music          = Ju Dae-gwan
| cinematography = Kim Nam-gyun
| editing        = Kim Mi-yeong   Park Hae-oh
| producer       = Kim Woo-taek   Choi Min-ae
| distributor    = Next Entertainment World
| released       =  
| runtime        = 108 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          = 
}}
The Weight ( ) is a 2012 South Korean film about a hunchback mortician and his transgender stepsister.
 Venice Days sidebar of the 69th Venice International Film Festival,    where it won the 2012 Queer Lion, an award for the "best film with a homosexual and queer culture theme."  It is the first Korean film to have won the prize.   It also won a Special Award at the 2013 Fantasporto Orient Express Awards.  Jeon Kyu-hwan was awarded Best Director at the 16th Tallinn Black Nights Film Festival,    and the Silver Peacock award for best director at the International Film Festival of India.  Cho Jae-hyun won Best Actor at the 2013 Fantasia Festival.   

Most of director Jeon Kyu-hwans previous films, including Berlinale-featured Varanasi and Dance Town, have dealt with the underbelly of society. The Weight is his fifth feature-length film. 

==Plot==
Jung is the mortician at the morgue who has to heavily rely on medicine for his severe tuberculosis and arthritis. Despite his illness, cleansing and dressing the dead is a noble and even beautiful work to him. Jung is the last living person who silently takes care of the dead. So for him, his life at the morgue is both a reality and a fantasy while the corpses are his models and friends for his paintings, his sole living pleasure.

Born with a hunchback and left at an orphanage, Jung was adopted by a woman who hid him away in the attic only to use him as a child slave for her dress shop. The womans own child Dong-bae is younger than Jung; she has always wanted to become a woman, loathing her own male body. While Jung feels affection and sympathy for his younger stepsister, he feels burdened by Dong-baes struggles. Under the weight of life and death carried by the dead bodies that he faces each day coupled with his love-hate relationship with Dong-bae, Jung endures the pain and thirst that he feels like a camel crossing a desolate desert in silence. Then he quietly prepares his biggest, his last gift for his sister.

==Cast==
*Cho Jae-hyun - Mr. Jung / Han Hae-woon
*Park Ji-a (Zia) - Dong-bae
*Lee Jun-hyeok - man in motorcycle helmet
*Ra Mi-ran
*Ahn Ji-hye
*Oh Seong-tae Kim Sung-min (cameo)
*Yoon Dong-hwan (cameo)
*Darcy Paquet - minister (cameo)

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 