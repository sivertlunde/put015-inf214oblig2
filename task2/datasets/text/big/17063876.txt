I'm Mad
 
 
{{Infobox Hollywood cartoon
| cartoon name = Im Mad
| series = Animaniacs
| image =
| image size =
| alt =
| caption =
| director = Rich Arons Dave Marshall Audu Paden
| producer = Senior:  
| story artist = Tom Ruegger  Randy Rogel
| narrator =
| voice actor = Rob Paulsen Jess Harnell Tress MacNeille
| musician = Richard Stone
| animator =
| layout artist =
| background artist =
| studio = Warner Bros. Animation Amblin Entertainment
| distributor = Warner Bros. Family Entertainment
| release date = March 30, 1994
| color process =
| runtime = 5 minutes
| country = United States
| language = English
| preceded by =
| followed by =
}} Dave Marshall and Audu Paden. The short made its debut on March 30, 1994, as part of the theatrical release of Don Bluths animated film Thumbelina (1994 film)|Thumbelina, and was eventually integrated into the television series as a part of Episode 69.

== Synopsis ==
Most of the dialogue in this short is spoken in time with the music, with the rest being sung.

It all starts at the Warner Bros. studio. In the water tower Yakko, Wakko and Dot are asleep in their bunk beds. The music starts when Dr. Scratchansniff runs in and wakes up the three Warner siblings because they are running late and going on a day trip, cajoling them to get something to eat before they go. An argument starts between Yakko and Dot after Yakko hits the ladder while Dot is coming down. Meanwhile, Wakko complains about breakfast, saying he wants pancakes or waffles instead of the scrambled eggs hes served, and then complains about his clothes. In the bathroom Dot accuses Yakko of using her toothbrush, creating another argument that is only broken up by Scratchansniff to pull them out so they can go.

Down at the parking lot the Doctor has great trouble getting the siblings into the car and when he finally gets them inside, they lock him out. On the road Dot, states "Im mad, Im mad, Im really, really, really mad" because Yakko nudged her with his elbow. Yakko claims to be innocent and they start to argue once again while Scratchansniff begs them to be quiet because he is trying to drive the car. However, that doesnt work so Scratchansniff forbids them both to say another word. Wakko meanwhile complains himself by wondering "Are we there yet? Im tired, Im hungry. How far? My nose is snotty, need to move my body. Gotta use the potty, better stop the car."

Yakko and Dot have once again started fighting. When asked to stop they both blame each other for starting it, making Scratchansniff lose his temper and yelling at them both to be quiet. With Yakko and Dot fighting in the backseat Scratchansniff complains that every time they take a trip its "always just the same". They all fight each other and when they finally arrive everyones a wreck.

They finally arrive at their destination, which is a circus. Scratchansniff wonders if they arent happy and the Warner siblings quickly state that indeed they are. They all run into the amusement park and come out again when the sun is setting. Dot states that "Im glad, Im glad. What a really great time we had". Both Yakko and Dot seem to be getting along well again and they treat each other nicely. They all get into the car again they and drive of into the sunset. All of a sudden Dot breaks the silence, telling Yakko that he hit her. He claims to be innocent and the whole fight starts all over again.

==Cast== Yakko Warner and List of Animaniacs characters#Dr. Otto Scratchansniff|Dr. Scratchansniff Wakko Warner Dot Warner

== External links ==
*  
 
 

 
 
 