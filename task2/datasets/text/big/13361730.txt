Girls Nite Out
{{Infobox Film
| name           = Girls Nite Out
| image_size     = 
| image          = Girls Nite Out FilmPoster.jpeg
| caption        =
| director       = Robert Deubel
| producer       = Anthony N. Gurvis
| writer         = Joe Bolster
| narrator       =
| starring       = Julia Montgomery Hal Holbrook Rutanya Alda James Carroll
| music          =
| cinematography = Joe Rivers
| editing        = Arthur Ginsberg
| distributor    = Independent International Pictures
| released       = June 20, 1982
| runtime        = 96 min.
| country        = USA English
| budget         = Unknown
| gross          =
| preceded_by    =
| followed_by    =
}}
Girls Nite Out (also known as The Scaremaker) is a 1982 American slasher film written and produced by Anthony N. Gurvis, directed by Robert Deubel and stars Julia Montgomery, Suzanne Barnes, Rutanya Alda, and Hal Holbrook. The film focuses on a group of college girls who are targeted by a killer in a bear mascot costume during an all-night scavenger hunt on their campus.

==Plot synopsis==
At Weston Hills Sanitarium, the body of Dickie Cavanaugh is found hanging in his cell by an orderly nurse. Cavanaughs sister gives permission to two gravediggers to bury the body. While the two men are digging the hole for Cavanaughs body, they are attacked and murdered by an unseen assailant who throws their corpses into the burial plot and buries them.
Meanwhile, at nearby DeWitt University, the students are in a festive mood. The basketball team has just won a championship game, and an all night scavenger hunt is set to take place the next evening for the female students. Among them are Lynn (Julia Montgomery) and her boyfriend, the star player, Teddy Ratliff (James Carroll), who celebrate the game victory at the on-campus diner. Barney (Rutanya Alda), the friendly waitress there, is thrilled for Teddy and the team. Lynn and Teddy, among many other students, attend a party later that evening.

At the party, the story of Dickie Cavanaugh circulates among freshman, who are unaware of his recent death; they are told that Cavanaugh murdered his girlfriend, Penny, in a jealous rage years ago and is locked away in the sanitarium. At the party, Lynn becomes jealous over Teddys apparent attraction to Dawn Sorenson (Suzanne Barnes), and later, misfit Mike Pryor (David Holbrook) gets into a fight with his girlfriend Sheila (Lauren-Marie Taylor). Michael Benson, the school mascot, is then murdered in his dorm room after arriving back from the party, and his bear mascot costume is stolen by his killer.

The following day, Mike Pryor is questioned by campus security officer Jim MacVey (Hal Holbrook) over the fight with his girlfriend. MacVeys daughter Penny, he explains, was Dickie Cavanaughs girlfriend and victim. Later that evening, the campus radio DJ broadcasts the clues to the scavenger hunt, which are received by the sorority girls on their portable radios. Meanwhile, the killer, dressed in the mascot suit, arms themselves with serrated knives mimicking bear claws.

Jane (Laura Summer) is murdered in the girls locker room where she discovers the first item of the hunt, and her body is found tied up in the showers by her friend Kathy, who is also attacked by the killer and murdered as well. The DJ at the radio station begins receiving odd phone calls from the killer, who tallies his victims. The killer also places calls to officer MacVey, and claims to be Dickie Cavanaugh himself. Meanwhile, Sheila goes down to the pond to search for another item on the hunt, and runs into the killer dressed in the bear suit, whom she believes to Benson. After teasing him, she goes into a shed by the pond, where she is killed.
While Lynn is searching for items on the scavenger hunt, Teddy has sex with Dawn at her house and then returns home. Lynns friend Leslie (Lois Robbins) goes to search for an item in the attic of the old chapel, where she is confronted by the killer and murdered. Lynn finds her body posed alongside a Virgin Mary statue stored in the attic.

Lynn frantically calls campus security, and the police arrive to the campus where they find the bodies. They are immediately suspicious of Mike Pryor, and question several of the students. After the news reporters have left, Dawn gets into an argument with her boyfriend, who kicks her out of their house after he tells her he knows about her affair with Teddy. As this is occurring, officer MacVey is studying the phone calls placed to the radio station as well as files and photographs of Dickie Cavanaugh, whose death he has just become aware of by Dickies doctor.

After Dawn is kicked out of her house, she becomes panicked and senses that someone is following her on the campus. She makes a call from the cafeteria phone to Teddys house, where he is consoling Lynn. Teddy leaves Lynn to go get Dawn, and finds her in the cafeteria, severely wounded and covered in blood.

As Teddy is comforting her, he is stabbed by Barney with a kitchen knife. Before she can continue to stab him, officer MacVey enters the cafeteria and confronts Barney, who he addresses as Katie Cavanaugh, Dickies twin sister. Speaking in different voices, she seems to be suffering from multiple personalities, and denies that she is Katie, claiming to be Dickie. When MacVey tells Katie that Dickie had committed suicide, she shifts back into her usual speaking voice, and confusedly tells him that Dickie is not dead, claiming to have brought him home from the hospital herself. She opens the freezer door, revealing Dickies frozen body.
==Cast==
* Julia Montgomery as Lynn Connors
* James Carroll as Teddy Ratliff
* Suzanne Barnes as Dawn Sorenson
* Rutanya Alda as Barney/Katie Cavanaugh
* Hal Holbrook as Jim MacVey
* Al McGuire as Coach Kimble
* Lauren-Marie Taylor as Sheila Robinson
* David Holbrook as Mike Pryor
* Laura Summer as Jane
* Mart McChesney as Pete Maniac Krizaniac
* Carrick Glenn as Kathy
* John Didrichsen as Ralph Bostwick
* Lois Robbins as Leslie Peterson
* Mathew Dunn as Michael Benson
* Susan Pitts as Trish
* Paul Christie as Dancer
* Gregory Salata as Hagen
* Tony Shultz as Bud Remington
* Larry Mintz as Charlie Kaiser
* Richard Bright as Detective Greenspan
* Kevin Mulvey as Sergeant Parker
* Richard Voigts as Dean Kemper (as Richard Voights)

==Production==
The film itself was filmed in 1982, but was not released until the year 1984 for a release.

== Critical reception ==

Allmovie wrote "Girls Nite Out might be one of the most forgettable of the early 80s slashers", calling it "dull" and "routine". 

==Soundtrack== John Fred & The Playboy Band and others.

== References ==

 

==External links==
* 
*  

 
 
 
 
 
 
 