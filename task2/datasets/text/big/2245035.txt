Brainstorm (1965 film)
{{Infobox film
| name           = Brainstorm
| image          = Brainstorm1965.jpg
| caption        = 1965 US Theatrical Poster
| director       = William Conrad
| producer       = William Conrad
| writer         =
| screenplay     = Mann Rubin
| story          = Lawrence B. Marcus
| based on       =  
| narrator       =
| starring       = Jeffrey Hunter Anne Francis Dana Andrews Viveca Lindfors
| music          =  George Duning
| cinematography = Sam Leavitt
| editing        =
| studio         =
| distributor    = Warner Bros.
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Brainstorm, released in 1965, is a late film noir. The film stars Jeffrey Hunter (credited as Jeff) and Anne Francis and was produced and directed by William Conrad, who became better known as an actor in such television series as Cannon and Jake and the Fat Man.

==Plot==
On a lonely highway, Jim Grayam spots a car stopped on the tracks of a railroad crossing. Inside he finds a sleeping or unconscious woman with the car doors locked. Unable to wake her, Grayam smashes a window and drives the car to safety just ahead of a speeding train.

Identifying her from a drivers license as Lorrie Benson, the wife of Beverly Hills millionaire Cort Benson, he drives her to their mansion. Grayam is a systems analyst for Benson Industries, widely admired throughout the company for his intelligence. When she wakes up, Lorrie makes it clear that she had parked the car on the tracks on purpose, a suicide attempt, and resents Grayam for saving her. Grayam declines a $1,000 reward.

Impressed that he didnt rescue her for his own advancement, the hard-drinking and wild-partying Lorrie recruits him for a scavenger hunt, then begins a romantic affair with him. When the relationship turns serious, Cort Benson begins to sully Grayams reputation at work, making it appear the valued employee is having a nervous breakdown, similar to one from his youth. Dr. Elizabeth Larstadt, a therapist, is asked to examine Grayam and finds him to have a volatile personality.

Lorrie wants to leave her husband, but Cort makes it clear he wont permit her to take their young child. Grayam hatches a diabolical plot. He will kill Cort, but not before studying how to give the appearance of insanity, so that he will be sentenced to a psychiatric institution rather than to the gas chamber for murder.

The scheme works. Dr. Larstadts testimony lands Grayam in a sanitarium rather than a prison cell. Grayams intention is to gradually prove to doctors that he is safe to be released back into society. In the weeks to come, however, the other inmates behavior drives Grayam slowly out of his mind. And when he finally is visited by Lorrie, he discovers to his horror that she now has another man in her life.

A desperate Grayam plans to escape. He confides in Dr. Larstadt, now believing her to be the only one who truly understands him, and even professes his love for her. The doctor, however, is only making sure that she was correct in her assessment that Grayam is unbalanced. Hospital security guards carry him away.

==Production==
 Temple Houston for Warner Bros. Television. William Conrad had also directed Hunter in the pilot episode of that series.
 King of Kings (1961).

Francis had a surge of new popularity in 1965 due to the premiere of her television series Honey West. She made two films during the year, but the TV show was dropped after one season.

Victor Rodman, formerly of NBCs Noahs Ark (1956 TV series)|Noahs Ark, made his last screen appearance in Brainstorm in the uncredited role of a prison inmate. Veteran character actors Strother Martin, John Mitchum and Richard Kiel are among the sanitariums patients.

==Reception==
In June 1965, critic Howard Thompson of The New York Times called Brainstorm "a so-so package of suspense ... Up to a point the story cuts ice. Then it slips into absurdity."  Judith Crist called the film "a sub-B potboiler for those who find comic books too intellectual"; Leslie Halliwell called it an "overlong thriller which starts off agreeably in the Double Indemnity vein, but goes slow and solemn around the half way mark." 

Some critics held the film in higher regard with the passage of time. The editors of Film Noir: An Encyclopedic Reference to the American Style (1979) described Brainstorm as "a minor masterpiece of the 1960s": Nightmare Alley Double Indemnity, which in a narrative sense it closely resembles, Brainstorm follows a typical noir pattern from romance, to melodrama, to crime, and finally to horror. But unlike these films, which deal only tangentially with insanity, Brainstorm is primarily an exploration of that theme. 

"Brainstorm is undoubtedly the last of the truly great black-and-white films noirs," wrote Nicholas Christopher in his book, Somewhere in the Night: Film Noir and the American City (1997): 
:It is unquestionably the most nihilistic film noir of either the classic or present-day cycle. ... Brainstorm is a film conceived and shot in the year following the Kennedy assassination, and its insistent references to madness, murder, mayhem, and conspiracy reflect the tenor of those times. ...   is the final, essential entry in that long line of films noirs that begins at the end of the Second World War. 
 Michael Atkinson of The Village Voice wrote that "the book does evoke a few forgotten beauts, notably William Conrads 1965 schizonoir Brainstorm. For Christopher, this truly nutty film is to other film noirs what Lobachevskis geometry — in which parallel lines eventually intersect — is to Euclids, and insofar as noir seems to reflect back what the viewer brings to it, hes not far off target." 

For the 2002 Sight & Sound poll of the top ten films of all time, critic Jack Stevenson included Brainstorm in his list. 

==Home video releases==
In March 2009 Warner Bros. released Brainstorm on Region 1 DVD, as part of the Warner Archive Collection. 

==Cast==
* Jeffrey Hunter (credited as Jeff Hunter) as Jim Grayam
* Anne Francis as Lorrie Benson
* Dana Andrews as Cort Benson
* Viveca Lindfors as Dr. Larstadt
* Kathie Browne as Angie DeWitt

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 