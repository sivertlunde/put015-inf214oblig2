Trapped in the Closet Chapters 1–12
{{Infobox film
| name           = Trapped in the Closet Chapters 1-12
| image          = Trapped in the Closet.jpg
| caption        = Teaser poster
| director       = R. Kelly Jim Swaffield
| producer       = R. Kelly Ann Carli Barry Weiss Elliot Lewis Rosenblatt Rick Johnson
| writer         = R. Kelly
| starring       = R. Kelly Cat Wilson Rolando A. Boyce LeShay Tomlinson Malik Middleton
| music          = R. Kelly
| cinematography = David Hennings
| editing        = Victor Mignatti Jim Swaffield Kenneth Wachtel	
| based on       =  
| studio         = Fuzzy Bunny Productions Jive Zomba Zomba
| released       =  
| runtime        = 43 minutes
| country        = United States
| language       = English
| budget         = US dollar|$1 million  }}
| gross          = 
}} musical comedy-drama song of the same name. Released in April 4, 2005, the film follows protagonist Sylvester, a man who in order not to get caught cheating decides to hide in his affairs closet.

==Background==
"Trapped in the Closet" originally appeared on Kellys album TP.3 Reloaded, divided into five "chapters", which appear as the final five tracks on the album. The first five chapters of "Trapped in the Closet" were recorded by Andy Gallas during the 2004-2005 recording sessions for TP.3 Reloaded. R. Kelly wrote and produced all five chapters. The first of these five chapters was released as the lead single from the album in 2005, by Jive Records. Kelly and Jive Records promoted the single release by releasing each of the first five chapters of the song set to radio stations one at a time.

Following the success and popularity of the "Trapped in the Closet" song series, on August 28, 2005, R. Kelly lip-synched a "new chapter" at an appearance at the 2005 MTV Video Music Awards. Finally, in November 2005, Jive Records released a DVD titled Trapped in the Closet, which aside from containing the five chapters of the song included on TP.3 Reloaded, also included seven more new chapters, bringing the song series to a total of twelve chapters. The material previewed by Kelly at the MTV Video Music Awards was revealed an early version of some parts of the twelfth chapter in the series.

Nearly two years later, in August 2007, Kelly and Jive released ten more chapters on another Trapped in the Closet DVD. These ten chapters were also shown on the Independent Film Channel (IFC), and were also streamed on IFCs Web site. The release of the DVD brings the series to a total of 22 chapters.

Kelly has stated that more chapters would be written and released. In December 2007, all 23 chapters were released in a DVD entitled The Big Package, which also includes a "commentary remix" featuring a preview of chapter 23. No official release dates have been announced, however, for more chapters of the series. On December 21, 2011, Kelly told TMZ that he has written thirty two more chapters, and is currently seeking investors in order to continue the saga. 
 hip hopera, and said, "Its now too long to be called a song."  When asked about the writing of the song, Kelly stated: "I dont know how to explain how I wrote it. It just keeps rhyming and rhyming." He has also stated that "Trapped in the Closet" seems to have taken on "a life, mind and body of its own", and has called the series an "alien". 

==Plot==
;Chapter 1 Oscar for vibrate but gun out in preparation. The chapter ends with the husband opening the closet.

;Chapter 2
The second chapter starts with the husband now knowing what his wife was doing behind his back. Sylvester tries calming him down but to no avail as he begins arguing with his wife. The husband nearly attacks Sylvester but stops due to the fact Sylvester is holding a gun. When the husbands cell phone rings, he tells Sylvester that he is a pastor, which Sylvester thinks would be reasonable to not resort to a violent outburst. When Sylvester decides to leave, the husband tells him to stay because he wants to reveal a "secret" to him and his wife. He then calls someone on his cell phone telling them to "turn the car around". When the person alarms him that he is at "the apartment", the husband calls the person up. Sylvester and the pastors wife are both anxious as to what the husband is about to reveal. When the husband does not answer Sylvesters questions quickly enough on who he has been talking to, he threatens to shoot both him and his wife. Just as Sylvester gets to the count of four, someone knocks on the door. The husband opens the door and the person turns out to be another man.

;Chapter 3

Chapter three starts with all four people in the room. A shocked and confused Sylvester demands to leave after getting this news but the wife tells him to stay. She then argues with her husband, whose name is revealed to be Rufus, over his own infidelity and they argue over whose cheating was worse. The wife berates Rufus for revealing his cheating to upset her after he had busted her with Sylvester. When Sylvester quiets them and demands more explanation, the other man, Chuck, begins explaining how their affair came about. Sylvester stops him, however, from revealing any more details. After the wife berates Rufus again, Rufus reveals her real name, which is Cathy, which shocks Sylvester, who was told her name was Mary. When Sylvester again threatens to shoot them, Rufus, Cathy and Chuck all begin yelling at each other, pushing Sylvester to shoot his gun in the air to quiet them, causing them to lie on the floor. Sylvester then calls his home on his cell phone to his home but is stunned to find another man is on the phone. He then quickly bolts out of the apartment while Cathy, Rufus and Chuck remain lying on the floor.

;Chapter 4
Sylvester is seen in chapter four rushing home, angered and belligerent over the events of the previous night. As he drives home, he is pulled over by a police officer, who gives him a ticket for speeding. When he arrives home, he starts asking his wife who had answered his phone. She responds that it was her brother Twan, whom she had reminded Sylvester earlier was coming home from prison. Sylvester remembers and then apologizes to his wife before they begin engaging in sexual intercourse. In the middle of it, however, Sylvesters wife, whose name is revealed to be Gwendolyn, sees a used condom on the side of the bed, covers it up, and then jumps on top of a shocked Sylvester, who tries to get Gwendolyn to stop because he is afraid he might have a seizure. When she demands him to keep going, Sylvester tries reminding her that his leg was beginning to cramp. When Gwendolyn finally gets off him, Sylvester flips the bed cover to find the used condom that she had tried to hide from him.

;Chapter 5
Chapter five starts with Sylvester now knowing his wife had cheated on him and he starts demanding answers. After a few more angry outbursts, Gwendolyn returns with her own knowledge of Sylvesters infidelity. He quickly turns it back around to her infidelity. When he berates her for not giving him an answer, she quickly comes up with certain names including friends of hers, named "Roxanne and Tina", and also mentions "Rufus and Chuck", which confuses Sylvester. She then mentions she and Cathy went to high school and were friends, and it was Cathy who introduced Gwendolyn to the police officer that stopped Sylvester, confirming that the police officer was her secret lover.

;Chapter 6
After the previous chapters revelation at the end, Sylvester begins laughing at the entire situation. After a second, Gwendolyn joins him in the laughter and Sylvester begins explaining the events the previous night. Meanwhile the cop who pulled Sylvester over and the man Gwendolyn admits cheating on Sylvester with, whose name is revealed to be James, turns his car back around concerned for Gwendolyns safety. When he sees Sylvesters car parked in a crooked space with the lights still on and sees the back door busted in, he picks up his gun and goes in to investigate. When he hears Sylvester and Gwendolyn laughing, James mistakes it for domestic violence|abuse. When Gwendolyn yells, "Sylvester, youre killing me", he busts in the door with Sylvester noticing the cop who pulled him over. Sylvester then pulls his gun out demanding the cop leave. After James constant demand to freeze and Gwendolyns pleas to Sylvester, he puts the gun down and puts his hands up. When James winks and smiles at him, an angry Sylvester rushes to James and they both wrestle with control of the gun, before thinking of it, a shot accidentally goes off.

;Chapter 7
This chapter reveals it was Twan, Gwendolyns brother who was just released from prison, was the one who was shot. After realizing who it was, Sylvester and James argue at who was at fault with the shooting, with Sylvester blaming the entire incident on the cop. Twan is presumed to be dead but in the middle of Sylvester, James and Gwendolyn arguing, Twan coughs, assuring them he is okay and that he was only non-fatally shot in the shoulder. As Twan recovers in the bathroom, Sylvester begins telling him what caused the incident when someone knocks on the door. After being hesitant with answering the door, Sylvester grabs his gun and aims it at the door, Twan joins him snatching James gun. At the count of three, Twan opens the door and it is revealed to be the next door neighbor, Rosie, who is shown with a spatula in her hand. A relieved Sylvester and Gwendolyn invite Rosie in while James snatches his gun back from a bewildered Twans hands and leaves.

;Chapter 8 Southern accent.) James tells a concerned Bridget that he was heading home in which Bridget happily tells him she had baked him a cherry pie. Meanwhile at Sylvesters house, Rosie, the next door neighbor, tells Sylvester, Twan and Gwendolyn that she could not stand the cop that just left their house causing Gwendolyn, Sylvester and Twan to laugh. Back at James house, he pulls up to the garage. A panicked Bridget rushes to the door and kisses James. When James asks her why she looked jittery, Bridget excuses it as "Menstrual cycle|its that time of the month". When Bridget tries getting him upstairs for pears, James insists on heating some leftover chicken. Bridgets nervous breakdown finally leads to James demanding a straight answer from Bridget. James then starts to wonder if Bridget had also cheated on him. Unbeknownst to him and from the looks of a nervous Bridget, it becomes clear another man is still in the house.

;Chapter 9 allergic to breaking the fourth wall, tells the audience that the man, hiding in the cabinet, is a midget.

;Chapter 10
In this chapter, the midget jumps out of the cabinet and fights with James. After James constant roughing up on the midget, Bridget runs upstairs where she puts out a number from her purse. Meanwhile back downstairs, James puts the midget on the table and demands to know why was he at his house. The midget continues telling him that he was paid not to tell, which only angers James, who puts his gun out on him. When he does, the midget commits bowel movements. Back at Sylvesters house, he, Twan and Gwendolyn are playing cards when the phone rings. When Gwendolyn answers it, she hears a panicked Bridget, who tells her she found her number in James pocket. After a minute, Gwendolyn hangs up and gives Sylvester and Twan the address to the house presumably to stop the fight. Back at James house, James and the midget continue fighting until Bridget comes back to the kitchen with a double barrelled shotgun. When James points the gun to Bridget, the midget takes his inhaler out. A few seconds later, Sylvester and Twan bust open the door and Sylvester points his gun at James. Sylvester and Twan notice a peculiar odor in the house. Due to Sylvester and Twans entrance, the midget faints on the table.

;Chapter 11
In this chapter, the midget wakes up peculiar to seeing three guns and tries to get out of the situation. Sylvester then begs Bridget and James to put their guns down, which they adamantly refuse. Bridget then tells Sylvester she will drop the gun if James does not hurt the midget, whose name is revealed as "Big Man", a stripper at a club called Dixies. When Sylvester asks why he was named that, the midget points down to his pants and tell him he is "well-endowed|blessed". After seeing Sylvester and Big Man communicating, James shuts them up calling them "Chuck and Rufus", which confuses Sylvester, who demands to know how James knew of them, though James acts bewildered to Sylvesters question. Just as James looks as if he was going to answer, Bridget starts getting sick. It is then revealed that Bridget is three-months pregnant, presumably with James baby. After James constant prompts to Sylvester, Twan and Big Man with his gun to leave, Bridget stops him and admits paying Big Man and even admitted that she knew that James had been cheating on her with Gwendolyn saying that she had followed him around. When James demands to know what Bridget is really telling him, Bridget says that Big Man is her babys father, which negates her previous excuse from chapter 9 as being "that time of the month". The midget again faints at the news.

;Chapter 12
Back at Cathys house, she, Rufus and Chuck get up from the floor. After Cathy closes the door, she and Rufus argue about the reasons why Sylvester almost shot at them from the previous chapters, with Cathy angered about Rufus "creeping" around with Chuck, who is the deacon of Rufus church, telling him "aint no telling what Ive got", referring to sexually transmitted diseases. Chuck gets angry at this and threatens Cathy with a knife, which prompts Cathy to force him to do it. Rufus then calms them down and tries to resolve the matter but neither Cathy or Chuck listen, as they continue arguing, even as the phone rings. When it rings a second time, Cathy answers it and it is Gwendolyn and she begins telling Cathy about the drama that had gone on. When Gwendolyn mentions that she saw Sylvester in Pajes club with "some crusty wig-wearing ass ho", Cathy realizes the man she had slept with that night was Sylvester. When Cathy tries telling her who the "ho" was, Gwendolyn stops her and tells her more about James and his situation. When an angry Rufus demands Cathy to finish their conversation, Cathy shuts Gwendolyn up and tells her she was the "ho" that had been with Sylvester.

==Cast== Robert Kelly as Sylvester, Randolph, the narrator
* Cat Wilson as Gwendolyn
* Rolando A. Boyce as Rufus
* LeShay Tomlinson as Cathy
* Malik Middleton as Chuck Michael Kenneth Williams  as James
* Drevon Cooks as Big Man
* Rebecca Field as Bridget Eric Lane as Twan
* LaDonna Tittle as Rosie

==External links==
 
*  

==References ==
 

 
 
 

 
 
 
 
 
 