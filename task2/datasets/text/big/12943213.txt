Song of the Fishermen
{{Infobox film
| name           = Song of the Fishermen
| image          = Song of the Fishermen.jpg
| caption        = 
| director       = Cai Chusheng
| producer       = 
| writer         = Cai Chusheng
| starring       = Wang Renmei, Luo Peng, Yuan Congmei, Han Langen
| music          = 
| cinematography = 
| editing        = 
| studio         = Lianhua Film Company
| released       =  
| country        = China
| runtime        = 57 minutes Chinese intertitles
| budget         = 
| film name = {{Film name| jianti         = 渔光曲
| fanti          = 漁光曲
| pinyin         = Yú guāng qǔ}}
}} Chinese silent film directed by Cai Chusheng in 1934 in film|1934, and produced by the Lianhua Film Company. The film, like many of the period details the struggle of the poorer classes, in this case a family of fishermen who are forced to sing on the streets in order to survive.

A successful film, Song of the Fishermen played for 84 straight days in Shanghai.  It was the first Chinese film to win a prize in an international film festival (Moscow Film Festival in 1935). 

==References==
 

==External links==
* 
* 
*  at the Chinese Cinema Web-based learning center at UCSD

 
 
 
 
 
 
 


 