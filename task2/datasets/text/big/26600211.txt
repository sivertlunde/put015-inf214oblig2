Seetha Kalyanam (2009 film)
{{Infobox film
| name           = Seetha Kalyanam
| image          = 
| caption        = 
| director       = T. K. Rajeev Kumar Menaka
| writer         = 
| narrator       = 
| starring       = Jayaram  Bheeman Raghu   Jyothika  Geethu Mohandas Sreenivas
| cinematography = Rajeev Ravi
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
Seetha Kalyanam is a 2009 Malayalam movie by T. K. Rajeev Kumar starring Jayaram, Jyothika and Geethu Mohandas.

The film was released in 2009, but Jyothikas film career ended in 2007 with the film Manikanda. Seetha Kalyanam was made before Jyothika gave up acting, but released in 2009.

== Plot ==
Sreeni (Jayaram), a simpleton has a high paying job in Bangalore. His mother, who is traditional to the core, is looking for the perfect match for him. Sreeni is a butt of the joke among his female colleagues and they tease him by pretending to flirt with him. The leader among them is Nimisha (Jyothika). Their relationship is purely platonic. Meanwhile, his mother succeeds in finding a traditional girl in Abhirami (Geethu Mohandas); brought up in an Aghraharam.  

Later, Nimisha realizes all on a sudden one day, that she had been long in love with Sreeni. Abhi, on the other hand has her own personal battles to deal with, since she had always been in love with Ambi (Indrajith Sukumaran|Indrajith).

== Cast ==
* Jayaram as Sreeni
* Bheeman Raghu as Uslampetti Vasu
* Jyothika as Nimisha
* Geethu Mohandas as Abhirami Siddique as Parameswaran
* Bindu Panicker as Madurammal Kalpana as Ramani Manorama as Sreenis mother
* Indrajith Sukumaran as Ambi Janardhanan as Thiruvattar Thankavelu
* Jagadish as Thampi
* Sukumari as Abhiramis mother
*Nandhu
* Oduvil Unnikrishnan as Nellayi Ramaswami
* Poojappura Ravi as the Priest
* Devi Ajith as Devi, Abhiramis elder sister
* Subbalakshmi

==External links==
* http://movies.rediff.com/report/2009/nov/09/south-malayalam-movie-review-seetha-kalyanam.htm
* http://entertainment.oneindia.in/malayalam/top-stories/2009/seetha-kalyanam-release-291009.html
* http://www.nowrunning.com/movie/4283/malayalam/seetha-kalyanam/review.htm
* http://popcorn.oneindia.in/title/2181/seetha-kalyanam.html

 
 
 


 