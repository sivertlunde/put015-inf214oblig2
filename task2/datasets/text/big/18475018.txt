Pontianak Menjerit
 
 
{{Infobox film
| name = Pontianak Menjerit
| image = Pontianak-menjerit.jpg
| caption =
| director = Yusof Kelana
| writer = Yusof Kelana
| starring = Ziana Zain Azlee Senario Faizal Hussein Zed Zaidi Sheila Rusly Juliana Banos Mazlan Pet Pet
| producer(s) = MH Noor, Fetty Ismail, Yusof Kelana, Dato Yusof Haslam
| editing  = Sallehan Shamsuddin
| distributor = Skop Production, ME Communications
| budget = RM 1.05 million
| cross  = RM 0.90
| released = 16 June 2005
| runtime = 105 Minutes
| country = Malaysia Thai
| preceded_by =
| followed_by =
|
}}

Pontianak Menjerit, directed by Yusof Kelana in 2005, is a comedy horror flick about siblings quarrel over a will worth million dollars to be inherited by their belated father.

==Synopsis==
Datuk Pengiran Abdul Rahman has asked his lawyer to find the people to inherit his fortune worth RM30 million. Besides his two sons, Azlee, a businessman and Mazlan, a fashion designer, there are still two more people on the list. One of them is Saiful, an orphan and a mechanic who do not see that he will be inheriting his dads million-dollar fortune, because he has never seen his dad before. The other is Ratnapuri, a Siamese woman. Azlee, Datuks eldest son, cannot accept the presence of Saiful and also Ratnapuri when his fathers will is read at Datuks residence.

==Cast==
* Faizal Hussein – Yassin
* Zed Zaidi – Saiful
* Ziana Zain – Ziana
* Azlee Senario – Azlee
* Sheila Rusly – Ratana Puri
* Juliana Banos – Julia
* Mazlan Pet Pet – Mazlan
* Zarina Zainoordin – Zarina
* Nursyella – Syella
* Jalaluddin Hassan|Dato Jalaluddin Hassan – Mr. Salleh (Lawyer)
* Angelina Tan – Inspector Angelina
* Osman Kering – Tok Bomoh
* Amran Tompel – Pak Man
* Corrie Lee – Somchai
* Julia Hana – Pontianak
* Raja Noor Baizura – Datin

==Release==
The film was released on 16 June 2005 and went box office.

==Awards and nominations==
18th Malaysian Film Festival, 2005
* Best Cinematography - Indra Che Muda (Won)
* Best Actress in Supporting Role - Sheila Rusly - Nominated
* Best Editor - Nominated
* Best Film - Nominated
* Best Music Score - Nominated
* Best Screenplay - Nominated
* Best Sound - Nominated

==External links==
*   at Sinema Malaysia

 
 

 
 
 
 
 
 