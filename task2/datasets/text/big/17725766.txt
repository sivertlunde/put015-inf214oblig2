What a Whopper
{{Infobox Film
| name           = What a Whopper
| image          = "What_a_Whopper".jpg
| caption        = Theatrical release poster
| director       = Gilbert Gunn
| producer       = Edward Joseph
| writer         = Terry Nation
| narrator       = 
| starring       = Adam Faith Sid James Carole Lesley
| music          = Laurie Johnson
| cinematography = Reginald H. Wyer	
| editing        = Bernard Gribble
| distributor    = Regal Films International
| released       =   October 17, 1961
| runtime        = 90 min.
| country        = UK English
| budget         = 
| preceded_by    = 
| followed_by    =  1961 British comedy film, written by Terry Nation, from a story by Jeremy Lloyd and Trevor Peacock.

It treats the subject of the Loch Ness Monster in a tongue-in-cheek fashion.  It stars the pop star Adam Faith as one of a group of Englishmen who travel to Loch Ness to fake sightings of the monster.
 Charles Hawtrey and Terry Scott.  The TV reporter Fyfe Robertson appears briefly as himself, covering the alleged sightings of the monster.

==Cast==
* Adam Faith as Tony Blake
* Sid James as Harry Sutton
* Carole Lesley as Charlotte Charlie Pinner
* Terence Longdon as Vernon
* Clive Dunn as Mr. Slate
* Freddie Frinton as Gilbert Pinner
*  Marie-France as Marie Charles Hawtrey as Arnold
* Spike Milligan as Tramp
* Wilfrid Brambell as Postman
* Fabia Drake as Mrs. Pinner
* Harold Berens as Sammy
* Ewan Roberts as Jimmy Archie Duncan as Macdonald
* Terry Scott as Sergeant

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 