Hooked on You
 
 
 
{{Infobox film
| name           = Hooked on You
| image          = HookedOnYouPoster.jpg
| caption        = Teaser poster
| director       = Law Wing-Cheong
| producer       = Johnnie To 
| writer         = Fung Chi Chiang
| starring       = Miriam Yeung Eason Chan
| music          = Dennie Wong
| cinematography = O-Sing Pui
| editing        = Law Wing-Cheong Media Asia Films Beijing Silver Dream China Film Media Asia Milkyway Image Media Asia Distribution Ltd.
| released       =  
| runtime        = 97 minutes
| country        = Hong Kong English
| budget         =
}}
  was temporarily turned into "Fortune Market" for the purpose of the film]]
Hooked on You ( ) is a 2007 Hong Kong comedy-drama film directed and edited by Law Wing-Cheong and produced by Johnnie To and his production company Milkyway Image. Featuring an ensemble cast, the film stars Miriam Yeung as a fishmonger who strives to fulfill her lifelong dreams before she turns 30.

==Plot==
Miu, a fishmonger at the Prosperity Market. Because of her father’s debt she gives herself three years to work at the Wet market. She promises herself that she’s going to leave the wet market and find a man worthy of her. At the market she always quarrel with her neighbor stall Mr. Fish (Eason Chan). But when a new supermarket threatens their business at the Prosperity Market they work together to fight against it.

==Cast==
* Miriam Yeung - Miu
* Eason Chan - Fishman
* Huang Bo - Porky Fung Shui-Fan - Mius father
* Stephanie Cheng
* Wong You-Nam - Joe
* Jo Kuk - Salesgirl at accessory shop
* Charmaine Fong
* Lam Ka-Tung - Beautician Hui Shiu-Hung - Bro Hung Raymond Wong - Man at bowling parlour
* Carl Ng - Mius date
* Wayne Lai - Porky (voice)

==See also==
* Johnnie To filmography

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 


 
 