Corpus Callosum (2007 film)
{{Infobox Film
| name           = Corpus Callosum
| image          =
| image_size     =
| caption        = Original Poster
| director       = Sarah Nagy Bryan Felber
| producer       = Sarah Nagy Bryan Felber
| writer         = Sarah Nagy
| starring       = Evan Lewis Sarah Nagy Sam France Jamie Alcroft Tommy Franklin
| music          = John Garcia Shaun Fleming
| editing        = Sam France Bryan Felber Sarah Nagy
| studio         = Apple Mescaline Productions Naa Jhi
| released       = December 13, 2007
| runtime        = 56 minutes
| country        = United States English
| budget         = $3,000
}}

Corpus Callosum is a 2007 film by Sarah Nagy and Bryan Felber. It debuted in the United States on December 13, 2007. It is named after the bridge of neural connections between the left and right sides of the brain.

==Plot==
The film follows Cross Carlton, a   of the brain to the Lateralization of brain function|right. Whether or not Cross can recognize the difference between the American Dream and her own ambitions is up to whether or not she can make this transition; that is, to cross the corpus callosum.

==Cast and characters== Cross brand TaB cola and holding a Cross pen, both symbolic of the 1980s (although the film takes place in 2007). 
 Evan Lewis street of the same name in Los Angeles. 

{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Evan Lewis || Kyle Hutchinson
|-
| Sarah Nagy || Cross Carlton
|-
| Sam France || Francisco Stanley
|-
| Jamie Alcroft || Randy
|-
| Tommy Franklin || Crevit
|-
| Maggie Anderson || Alexandra
|-
| Bryan Felber || Rob Hathaway
|-
| Jonathan Rado || Mysterious Guitar Player
|-
| Mike OConnor || Homeless Man
|-
| Steve Bacharach || History Teacher
|-
| John Garcia || Painter in Library
|}

==Production==
Sarah Nagy and Bryan Felber first met at the   and Metronome (film)|Metronome, respectively. Soon after, Nagy wrote a twelve-page prototype for the script, which was originally titled, The Cross Conspiracy. After reading it, however, Felber suggested the title Corpus Callosum, which Nagy liked better. 
 MiniDV tapes P2 cards to lend contrast to the dual themes of the left and right sides of the brain. Production lasted over eight months due to complications involving Nagys decision to attend UCLA in the fall. 

Filming primarily took place in Agoura Hills, California, Malibu, California, and Beverly Hills, California.
 Edwin Telford, John Garcia, and Shaun Fleming (lead singer of the band First from the Sea), both of whom Felber knew from Agoura High School. Unconventional recording methods and instruments, such as the rhythmic sound of water droplets falling, were intended to give Corpus a spacey, surreal feeling.

==Major Themes==
The Brain
The symbol of the corpus callosum is represented in most shots involving Cross and Kyle, with Cross on the left and Kyle on the right, each representing their respective sides of the brain. The show Rob Hathaway: Interactive Childrens Programming eventually addresses the corpus callosum in one of the episodes Cross watches later on in the film.

Robots
The recurring theme of TaB cola was conceived due to the hot pink color of the can. Nagy thought that hot pink was the most unnatural color of the spectrum, so Crosss constant consumption of the cola is intended to convey her mental status as robotic. The robots that Cross begins to hallucinate also reflect her resentment against the machine-like status society has relegated her to.

==References==
 

==External links==
* 

 
 
 