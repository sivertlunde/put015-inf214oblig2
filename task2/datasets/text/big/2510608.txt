Nayak (1966 film)
{{Infobox film
| name           = Nayak (The Hero)
| image          = Nayak Indian film poster.jpg
| caption        = Original Indian poster
| director       = Satyajit Ray
| producer       = R. D. Banshal
| writer         = Satyajit Ray
| starring       = Uttam Kumar Sharmila Tagore
| studio = R. D. Banshal & Co.
| distributor    = Edward Harrison (US)
| music          = Satyajit Ray
| released       =   
| runtime        = 120 minutes
| country        = India Bengali
| budget         = 
}} Bengali drama film written and directed by Satyajit Ray. It was Rays second entirely original screenplay, after Kanchenjungha (1962). The story revolves around a matinee idol on a 24-hour train journey from Kolkata to Delhi to receive a national award. However, he ends up revealing his mistakes, insecurities and regrets to a young journalist, who realises that behind all the glitter is a deeply lonely man. Her initial contempt for people like him turns into empathy, and she decides not to publish what he has revealed. His life journey is gradually revealed through seven flashbacks and two dreams during the train ride.    

==Plot== Bengali films, Arindam Mukherjee (Uttam Kumar), has been invited to the capital to receive a prestigious award. As all the flights are booked, he is forced to travel by a train from Calcutta to New Delhi. He is in a foul mood as the mornings papers are filled with his being involved in an altercation and his latest film is slated to become his first flop.In the restaurant car, he meets Aditi Sengupta (Sharmila Tagore), a young journalist who edits a modern womens magazine, Adhunika.  Filled with contempt for the likes of him, she secretly plans to interview him because she thinks it would make a saleable copy. It soon leads to him unwittingly pouring out his life history. The interaction also brings to surface the inner insecurities of Arindams character and his consciousness of the limitations of his powers. Aditi initially takes notes, surreptitiously, but later on, out of empathy almost bordering on pity, abandons it.  However, critical of the star, she interrogates him and the star ends up re-examining his life. In a series of conversations with Aditi, he also reveals his past and guilt.

Arindam talks about Shankar-da, his mentor, taking us back to his early youth. His selling out to films and giving up theatre against the wishes of his old teacher... His first days shoot, and he being snubbed by the successful actor Mukunda Lahiri... A few years later Mukunda Lahiri, now a forgotten actor after a series of flops, comes to him to beg for a small part. He rejects the ageing actor in revenge. His taking refuge in alcohol. And his refusing to help a friend in politics.

Toward the end of the train journey, Arindam is drunk and feels a need to confide his wrongdoings. He asks the conductor to fetch Aditi. He begins to confess an affair with a married woman. But Aditi stops him. It was an affair with Promila, which ended in a brawl with her husband. He even contemplates suicide, but Aditi sends him to his cubicle before he can do anything about his plan.

As the star re-lives and examines his life with Aditi, a bond develops between them. Aditi realises that in spite of his fame and success, Arindam is a lonely man, and needs her sympathy and understanding. Out of respect for his frank confession, she chooses to suppress the story and tears up the notes she has written. She lets the hero preserve his public image.

==Cast==
 
* Uttam Kumar - Arindam Mukherjee
* Sharmila Tagore - Aditi
* Bireswar Sen - Mukunda Lahiri
* Somen Bose - Sankar
* Nirmal Ghosh - Jyoti
* Premangshu Bose - Biresh
* Sumita Sanyal - Promila Chatterjee
* Ranjit Sen - Haren Bose
* Bharati Devi - Manorama (Mr. Boses wife)
* Lali Chowdhury - Bulbul (Mr. Boses daughter)
* Kamu Mukherjee - Pritish Sarkar
* Susmita Mukherjee - Molly (Mr. Sarkars wife)
* Subrata Sensharma - Ajoy
* Jamuna Sinha - Sefalika (Ajoys wife)
* Satya Banerjee - Monk of WWWW organisation
==Soundtrack==
{{infobox album
| Name          = Nayak
| Type            = Soundtrack
| Artist           = Satyajit Ray
}}
{{Track listing
| headline         = Songs
| extra_column           = Playback
| all_lyrics       = Satyajit Ray
| all_music           = Satyajit Ray

| title1          = Arindam Theme
| extra1          = no singer
| length1          = 1:52
}}

==Production==
Ray wrote the screenplay of the film, at Darjeeling in May, where he went during off-season from filming. Even then he had Uttam Kumar in his mind for the lead, but not as an actor, rather a "phenomenon". The film was shot the later half of 1965.   

==Awards==
*National Film Award for Best Feature Film in Bengali, 1967
*Bodil Award for Best Non-European Film, 1967 Berlin International Film Festival, 1966   
*Critics Prize (UNICRIT Award), Berlin International Film Festival, 1966
*Bengal Film Journalists Association – Best Actor Award|B.F.J.A Best Actor Award: Uttam Kumar
*Bengal Film Journalists Association – Best Director Award|B.F.J.A Best Director Award: Satyajit Ray

==Nominations== Golden Bear for Best Film, Berlin International Film Festival, 1966

==Digital restoration==
The film is one of four Ray films which were digitally restored and set for a re-release in January 2014.   

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 