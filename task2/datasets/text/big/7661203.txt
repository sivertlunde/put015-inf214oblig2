Kagero-za
 
{{Infobox film
| name           = Kagerō-za
| image          = Kagero-za poster.jpg
| image_size     = 190px
| caption        =
| director       = Seijun Suzuki
| producer       = Genjirō Arato
| writer         = Yōzō Tanaka Kyōka Izumi (novel)
| narrator       =
| starring       = Yūsaku Matsuda Michiyo Okusu Katsuo Nakamura Eriko Kusuda
| music          = Kaname Kawachi
| cinematography = Kazue Nagatsuka
| editing        = Akira Suzuki
| distributor    = Cinema Placet Genjirō Amato Pictures
| released       =  
| runtime        = 139 minutes
| country        = Japan Japanese
| budget         =
| gross          =
}} independent Cinema Japanese film directed by Seijun Suzuki and based on a novel by Kyōka Izumi. It forms the middle section of Suzukis Taishō Roman Trilogy, preceded by Zigeunerweisen (film)|Zigeunerweisen (1980) and followed by Yumeji (1991), surrealistic psychological dramas and ghost stories linked by style, themes and the Taishō period (1912–1926) setting. All were produced by Genjirō Arato.

==Cast==
* Yūsaku Matsuda as Shunko Matsuzaki
* Michiyo Okusu as Shinako
* Katsuo Nakamura as Tamawaki
* Yoshio Harada as Wada
* Eriko Kusuda as Ine
* Mariko Kaga as Miyo
* Ryūtarō Ōtomo as Shishō

==External links==
*  
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 


 