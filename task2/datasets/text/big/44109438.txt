Champion Thomas
{{Infobox film
| name           = Champion Thomas
| image          =
| caption        =
| director       = Rex
| producer       = Rex
| writer         = Jagathy Sreekumar
| screenplay     = Jagathy Sreekumar Innocent Mukesh Mukesh
| music          = MG Radhakrishnan
| cinematography = Prathapan
| editing        = L Bhoominathan
| studio         = Jayjeevan Productions Of India
| distributor    = Jayjeevan Productions Of India
| released       =  
| country        = India Malayalam
}}
 1990 Cinema Indian Malayalam Malayalam film, Innocent and Mukesh in lead roles. The film had musical score by MG Radhakrishnan.   

==Cast==
 
*Jagathy Sreekumar as Velayudhan
*Sreeja as Selin
*Sunny Augustine as Thomas Innocent as Dr. Unnithan Mukesh as Dr. Mathews
*Thilakan as PC Nair
*Nedumudi Venu as Madhavan
*Aranmula Ponnamma
*Bobby Kottarakkara as Paramu
*Janardanan
*Kanakalatha
*Mamukkoya
*Philomina
*Ajayan Adoor as Raghavan
 

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by K Jayakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Lillyppoomizhi || K. J. Yesudas || K Jayakumar || 
|-
| 2 || Lillyppoomizhi (F) || KS Chithra || K Jayakumar || 
|-
| 3 || Mey thalarnnaalum || K. J. Yesudas, KS Chithra || K Jayakumar || 
|}

==References==
 

==External links==
*  

 
 
 

 