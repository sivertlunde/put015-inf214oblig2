Help (film)
 
 
 
{{Infobox film
| name= Help
| image= Help official poster art.jpg
| caption= Theatrical Poster
| director= Rajeev Virani
| producer=  
| writer=  
| starring=  
| music= Ashutosh Phatak
| cinematography= Dhimant Vyas
| editing=  
| distributor= Rupali Aum Entertainment Pvt. Ltd.
| released=  
| country= India
| language= Hindi
}}
Help ( ) is a 2010 Bollywood horror film directed by Rajeev Virani in his directional debut. It stars Bobby Deol and Mugdha Godse in the lead roles and marks the debut of Sophia Handa in a supporting role. Shreyas Talpade also has a special role in the film although it was not marketed as his film. It released on 13 August 2010, making it the first Bollywood horror film to release on Friday the 13th, the day of bad luck.  The film was shot in Mauritius.

==Plot==
A woman, Susan Alves, is in a mental asylum on the island of Mauritius in 1990. She frantically scribbles demonic images on her cell walls. Her husband, Joe Alves, comes to visit and they discuss their daughter, Pia. Susan panics and says she cant bear it and commits suicide in front of Joe.

In present-day Mumbai, Pia (Mugdha Godse) is now married to horror film director Vic (Bobby Deol). However, their marriage is strained. When Pias friend Carol (Sophia Handa) from Mauritius informs the couple that Pias father, Joe, has suffered a heart attack, they travel to the island and stay at Pias childhood home, where its revealed that Pia had a twin named Dia, who she was very close with. That night, Pia has a nightmare of a girls ghost and inanimate objects in the house are seen moving around. The next morning, Pia has a flashback of Joe carrying a dead Dia out of the pool. She is very disturbed by this memory as she recalls that normally she was with her sister at all times, but the day Dia drowned, she wasnt. During dinner, when Pia goes outside to check on Tracy, the dog, who is barking ferociously at something invisible, she has another encounter with the girls ghost.

Pia discovers she is pregnant. Meanwhile, Joe shares old photos with Vic and points out Dias favourite doll, which was buried with Dia. Pia tells Vic of her pregnancy; the news mends their relationship and the couple is once again happy. However, unusual events continue occurring in the house. One night, Dias ghost jumps down on Pia from the ceiling. The next morning, Pia is clearly not herself. When Tracy is found dead, Pia seems completely unfazed, which concerns Carol, as Pia loved the dog. Carol contacts her friend Aditya (Shreyas Talpade), a parapsychologist for help, informing him about the unnatural happenings in the house and Pias unusual behaviour. 

Carol has an encounter with the ghost and flees but is hit by a truck and dies. At the funeral, Aditya senses that Pias life is in danger and tells Vic. After encountering the ghost himself, Vic seeks Aditya for help. He searches the attic and finds the doll that was buried with Dia, a video cassette, Susans diaries and some extremely morbid paintings by Susan, featuring images of suicide and inner turmoil. Vic visits the mental asylum and the janitor there tells him that Susan wasnt insane but in fact possessed by an evil spirit.

Vic and Aditya watch the video cassette, which shows Pia drowning Dia before giving the camera an evil smile. However, they suspect the clip only shows half the truth. After repeated viewings of the video, Aditya realises Susan was already possessed even before Dias death. He also notices the reflection of a third girl in the mirror: it cannot be Pia, as she is the one videotaping and it is not Dia, who is in front of the camera. It is the spirit, meaning Pia was not the one who killed her, and that through her uncovered doll, Dia was trying to send them a message about her true murderer.

Vic discovers Susan was pregnant with a third child and it seems that the women in the family become possessed when pregnant. A possessed Pia kills Joe. It is revealed that Susan was pregnant not with twins but with triplets. Dia and Pia came out fine, but there were major complications with the third child, and Joe had to take the decision to save Susan over the child. After realising it is the triplets spirit taking revenge against the family, Vic and Aditya try to destroy the spirit. Aditya is killed and but Vic manages to trap the spirit in the other realm.

7 months later, Pia delivers a healthy baby. However, while Pia sleeps, the baby is seen squirming and writhing; its eyes suddenly go blank like the spirits, implying the spirit is now in the baby.

==Cast==
* Bobby Deol as Vic
* Mugdha Godse as Pia,Dia,and the evil spirit
* Shreyas Talpade as Aditya
* Sophia Handa as Carol
* Salim Fatehi as Joe Alves
* Jyoti Dogra as Susan Alves
* Anaushka Borzeen Dantra as Young Pia
* Ashoi Borzeen Dantra as Young Dia 

==Release==

===Critical reception===
Help received mixed reviews, but many praised the performances of Mugdha Godse and Jyoti Dogra, as well as the make-up by Vikram Gaikwad. Taran Adarsh from Bollywood Hungama gave the film 2/5 stars saying, "On the whole, Help is at best an ordinary attempt."  Sanskriti Media from AOL Bollywood gave the film a glowing review of 3.5/5 stars saying, "Mugdha comes up with a fabulous performance as a possessed lady in the film. Although she has donned a bikini in the film, her slim persona does not make her look vulgar. Bobby is natural and simple. Shreyas has also performed his character with great intensity as Help will come as a surprise to the film industry as well as the audiences. The producers surely are confident about their product hence they are releasing it with a much-hyped movie like Peepli Live. Help definitely is a must watch film!"  Sampurn from Entertainment Daily gave the film 3/5 stars saying that "...Help has enough genuinely spooky moments to keep you on the edge of your seats. Just like Bhoot, this film succeeds in scaring you in broad day light sequences too as Mugdha enacts her difficult role fairly well. Bobby Deol is earnest and does well in his part. Shreyas Talpade is very impressive, especially in the climax sequence. Salim Fatehi is good and so is Sophie Handa who plays Mugdhas best friend. Jyoti Dogra playing Mugdhas mom is terrific and gives you the chills whenever she is on screen."  Fenil Seta from Admanya gave the film 2.5/5 stars saying, "Help works but unfortunately only in parts. The plot is brilliant but the execution and some loopholes in the narrative spoil the show!". He also said, "Every actor does a nice job but no performance was extra ordinary. Mugdha Godse looks charming and does a fine job especially in scenes wherein shes possessed. Another fine performance from her after Fashion and Jail! It was great to see Bobby Deol after a long time and does a fine job as Jyoti Dogra leaves a mark while Salim Fatehi was okay. The twin girls, Anushka and Ashoi, play their part well. Sophia Handa and others were good as Special mention should also go to make-up guy, Vikram Gaikwad." 
 Bhoot and Phoonk beat. Currently on Bollywood Hungama, the film has an average of 4.32/5 stars by user reviews. 

==Production== IIFA press conference.  The film was originally slated for release on 9 July 2010, but was later pushed to 13 August 2010 to have more time to promote the film.

==Soundtrack==
{{Infobox album 
| Name= Help
| Type= soundtrack
| Artist= Ashutosh Phatak
| Cover=
| Released=  
| Recorded=
| Genre = Film soundtrack
| Length= 33:01
| Label= Pen Music
| Producer= Vinay Chowksey
| Last album= Sigh of an Angel (2008) 
| This album= Help (2010)
| Next album= Petri Dish (2010)
}}
The soundtrack of the film is composed by Ashutosh Phatak, commonly known as Ashu, from Blue Frog Media and the lyrics were penned by Irshad Kamil. The soundtrack is composed of seven songs. The music released on 23 July 2010.

===Tracklist===
{{Track listing
| extra_column = Singer(s)
| title1 = Kehna Hai | extra1 = Suzanne DMello, Joi Barua | length1 = 5:24
| title2 = Kyun Gum Hai Khushiyan | extra2 = Ashutosh Phatak|Ashu, Kirti Sagathia | length2 = 5:18 Rana Mazumder, Barkha | length3 = 4:54
| title4 = Incubated, Just for Death | extra4 = Ashu | length4 = 2:12
| title5 = Kehna Hai | note5 = Remix | extra5 = Suzanne DMello, Joi Barua | length5 = 5:22
| title6 = Kyun Gum Hai Khushiyan | note6 = Remix | extra6 = Ashu, Kirti Sagathia | length6 = 5:34
| title7 = Help | note7 = Remix | extra7 = Rana Mazumder, Barkha | length7 = 4:55
}}

===Reception===
Joginder Tuteja from Bollywood Hungama gave the soundtrack 1/5 stars saying "As expected, Help doesnt have much to offer and hence the reason why one hasnt heard much about it either. Earlier this year, there have been soundtracks that have been created for the horror flicks. However, this one is clearly the weakest of all." Tuteja listed one of the reasons of the flop soundtrack was because of bad promotion of the film and the music is nowhere to be found on music stands. "Kehna Hai" was the only song from the soundtrack appreciated by Tuteja. "Due to depressing nature of the song, as conveyed in Irashad Kamils lyrics, and the way it is sung by Ashu and Kirti Sagathia, you are not much charged", said Tuteja about the track "Kyun Gum Hai Khushiyan". The title track, "Help" was called "ordinary". 

However, from an audience point of view, the music was a hit. The films soundtrack currently has an average of 3.33/5 stars on Bollywood Hungama by user reviews.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 