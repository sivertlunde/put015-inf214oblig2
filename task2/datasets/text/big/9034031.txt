Hathkadi
{{Infobox film
| name           = Hathkadi
| image          = Haathkadigovinda.png
| caption        = Promotional Poster
| director       = Rama Rao Tatineni
| producer       = Venkata Subba Rao Anumolu
| writer         = Govinda Shilpa Shetty Shakti Kapoor
| music          = Anu Malik
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Hindi
| budget         =
| gross          =
}} 1995 Bollywood action film directed by Rama Rao Tatineni starring Govinda (actor)|Govinda, Shilpa Shetty and Shakti Kapoor. The movie was an average grosser at the box office

==Plot ==
Assistant Commissioner of Police, Suraj Chauhan (Govinda (actor)|Govinda) is an honest and diligent police officer. These qualities in him are engrossed in him due to the presence of corrupt politicians and police officers like the Home Minister Bhavani Shankar (Shakti Kapoor), Surajs Deputy Inspector General (Kiran Kumar). Suraj has a brother (Arun Chauhan), who is a crime reporter for the Indian Times. Next day, Home Minister Bhavani Shankar goes to a function held by an adoption center for only girls. There he encounters a pretty girl and instantly feels infatuated with her. He asks the DIG to ask the Mayors wife to bring her to him since the Mayors wife is the owner of the adoption center. At first, the Mayors wife resists and says no but when the DIG threatens to tell the truth about her past endeavours about dealing with prostitutes to her husband, she agrees. That night, when the Mayors wife brings the girl to Bhavani Shankar, little does he know that Arun is on an assignment for more scoop for his newspaper. Arun discovers and records a video of Bhavani Shankar raping the same girl from the adoption center. The next night, Arun goes to see the Mayor only to show him the misdeed that Home Minister Bhavani Shankar has committed. Filled with anger and disgust, the Mayor and Arun head to the police department to have Home Minister Bhavani Shankar arrested for this. But unfortunately, the Mayors wife overhears them and informs the DIG about this. On the way to the police station, Arun and the Mayor are blocked and then ruthlessly killed by Chakku Pande (Puneet Issar), a special hired goon of Bhavani Shankar. Suraj is enraged and aggrieved at the loss of his brother and swears to avenge his death. As Suraj finally starts coming more in contact with Bhavani Shankar, he realizes his bad character and with time that Bhavani Shankar is the one behind his brothers killing. After that, Suraj goes to Chakku Pande to get him to confess the killing he did according to the order given by Bhavani Shankar. But Chakku Pande denies it and thus is beaten up by Suraj. Chakku Pande gets sent to jail by Suraj until he decides to confess his crime. Then one night, Bhavani Shankar hires a few goons to have Chakku Pande killed. But Chakku Pande survives due to Suraj and the police force and claims he will protest against Bhavani Shankar. To Surajs surprise, when he takes Chakku Pande to a huge public function to confess this truth, Chakku Pande puts the blame on Suraj. After that, the lights go out and a gunshot is heard. When the lights come back on, Chakku Pande is dead and a possible suspect is seen running away through the crowd by Suraj. Presuming that Suraj is the killer (which he is not), the evil police officers of Bhavani Shankar arrest Suraj and send him to jail for the murder of Chakku Pande. But when he reaches jail, he is surprised to see that he has a lookalike, Rajnikant. Rajnikant is a simple man with strong positive morals who came to jail because he killed a man who tried to rape his wife. And when Rajnikant realizes that the evil politicians put Suraj in jail in the first place, he suggests that Suraj and Rajnikant can switch places so that Suraj can leave as Rajnikants since Rajnikants sentence is almost over. And from here onwards, starts a fun and action filled story of how Suraj and Rajnikant join forces to finally accomplish Surajs goal of avenging his brother.

==Main cast==
  as Neha]] Govinda as ACP Suraj Chauhan/Rajnikant 
*Shilpa Shetty as Neha 
*Madhoo as Rani 
*Shakti Kapoor as Bhavani Shankar (Home Minister) 
*Kiran Kumar   
*Tej Sapru as Inspector Prabhakar 
*Alok Nath as Chief Minister 
*Satyen Kappu as Chandraprakash (Mayor) 
*Arun Govil as Arun Chauhan (Surajs brother) 
*Puneet Issar as Chakku Pande, the killer 
*Vijayalalitha as Lata (Mayors wife)  
*Laxmikant Berde as Pyarelal (Havaldar)  Jayalalitha as Sub-inspector Phoolan Devi (as Jaya Lalita)

==External links==
*  

 
 
 
 