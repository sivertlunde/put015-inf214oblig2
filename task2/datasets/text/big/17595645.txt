Mostly Ghostly: Who Let the Ghosts Out?
 
{{Infobox film 
| name = Mostly Ghostly: Who Let the Ghosts Out?
| caption = DVD release cover
| image = Mostly Ghostly- Who Let the Ghosts Out? FilmPoster.jpeg
| director = Rich Correll
| producer = Yvonne M. Bernard Steve Stabler Arthur I. Cohen P. Jayakumar
| screenplay = Rich Correll Pat Proft
| story = Jana Godshall
| based on =   
| starring = Sterling Beaumon Madison Pettis Luke Benward Brian Stepanek David DeLuise Kim Rhodes Adam Hicks Ali Lohan Sabrina Bryan  Noah Cyrus Travis T. Flory
| music = Peter Neff
| cinematography = Barry Wilson
| editing = Ryan Correll
| studio = Universal Studios Family Productions
| released = September 30, 2008 (DVD) October 11, 2008 (TV)
| runtime = 92 min.
| country = United States
| language = English
| budget = US$3.6 million 
}}
 horror comedy fantasy film book series created by R.L. Stine of Goosebumps fame. It was released on September 30, 2008 on DVD. It also premiered on the Disney Channel on October 11, 2008. It reruns each October on Disney Channel as part of their Halloween theme programming, as well as on Disney XD.

The film was shown on the Disney Channel that year as part of their "Wiz-Tober Celebration". The movie features appearances by Sterling Beaumon, Ali Lohan, Madison Pettis, Luke Benward,  Brian Stepanek, David DeLuise, Adam Hicks, Kim Rhodes, Sabrina Bryan and, Noah Cyrus.

A sequel,  , was released in September 2014.

== Plot ==
Max Doyle (Sterling Beaumon) is an 11-year-old whose love of performing magic disappoints his father (David DeLuise) and draws ridicule from his older brother, Colin (Adam Hicks). While doing laundry in the basement, Max hears voices. While investigating the source of the sound, he sees a hand come out of the wall. A hidden tunnel behind the wall happens to harbor the evil Phears (Brian Stepanek) and his cadre of ghosts. Phears is intent on freeing himself and his minions from the world of ghosts to inhabit the physical world but will be able to do so only on Halloween.

Later, Max finds the ghosts of two children, Nicky (Luke Benward) and Tara Roland (Madison Pettis), have suddenly come to occupy his room. They explain that they need his help in learning who they are, how they came to be ghosts, and what has happened to their parents. Although they cannot be seen by anyone but Max, they are able to interact with objects in the physical world. This allows them to frighten a boy who has been bullying Max at school. Shortly thereafter, Tara and Nicky learn it was Phears who killed their parents and now holds the ghosts of their parents captive. During rehearsal of Maxs magic show, Tara is captured by Phears.

Max tells Nicky about it and suggests Nicky throw him into the basement tunnel to find her. The same way (most) people dont see ghosts, ghosts cannot see Max. He retrieves a box from Tara that contains a ring to defeat the evil ghosts, but Phears prevents her from escaping with him. Traci (Ali Lohan), a girl Max has been crushing on, becomes his assistant for his magic show. They perform with the help of Nicky moving objects around, making it appear as if it was Max moving them with his magic. On Halloween, Phears finally breaks the tunnel wall and crashes the show.

There, Max chants the spell (From the light of earth the dark descends, should they return that all depends, when hands point up to moonlit skies, on 10-31 the darkness dies) and Phears minions are sent back to the depths of the earth. Max receives applause from the audience who believed that everything was part of the show, while Phears himself escapes in the form of a roach without anyone seeing him.

== Cast ==
* Sterling Beaumon as Max Doyle - An eleven-year-old boy who is forced to keep Nicky and Tara away from their enemy, Phears, because only he can see them. 
* Madison Pettis as Tara Roland - A nine-year-old ghost of a young girl who likes Max more than her brother, Nicky. She is the youngest. She lived in the house Max lives before the Doyles moved in. 
* Luke Benward as Nicolas "Nicky" Roland - Taras older brother who sometimes takes a dislike to Max. 
* Brian Stepanek as Phears -  An evil ghost who is after Nicky and Tara after taking their parents. 
* Ali Lohan as Traci Walker - A popular girl
* Adam Hicks as Colin Doyle - Maxs older brother
* David DeLuise as John Doyle - Max and Colins father
* Kim Rhodes as Harriet Doyle - Max and Colins mother

== Sequel ==
A sequel,  , was released on DVD on September 2, 2014. Madison Pettis reprised her role as Tara, with new additions Bella Thorne, Calum Worthy, Ryan Ochoa, Eric Allan Kramer and Roshon Fegan signed onto the film.    Phears will return, with Charlie Hewson replacing Brian Stepanek. Filming for the sequel was completed on April 17, 2014.  

==Music==
The title track "Magic in Me" was composed by Dan Yessian, Robert Saltzburg and Liz Larin and performed by Leland Grant.  

==See also==
*List of ghost films

== References ==
 

== External links ==
*  
*  
*   
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 