The Dawn Patrol (1938 film)
{{Infobox film
| name           = The Dawn Patrol
| image          = The Dawn Patrol 1938 poster.jpg
| image_size     = 200px
| caption        = 1938 US Theatrical Poster
| director       = Edmund Goulding
| producer       = Jack Warner (executive producer) Hal B. Wallis (executive producer) Robert Lord (associate producer)
| writer         = John Monk Saunders (story) Seton I. Miller Dan Totheroh
| narrator       = 
| starring       = Errol Flynn Basil Rathbone David Niven
| music          = Max Steiner
| cinematography = Tony Gaudio
| editing        = Ralph Dawson
| distributor    = Warner Bros.
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = $500,000 (approx) 
| gross          = 
}}
 
The Dawn Patrol is a 1938 American war film, a remake of the  . Finnie, Moira.  Skeins of Thought, 2004. Retrieved: April 2, 2009.  The book of short stories, War Patrol by A.S. Long published in the 1930s also bears a striking resemblance in plot and characters to the Flynn/Niven version of the film, although it is never credited as a source.

The film, directed by Edmund Goulding, stars Errol Flynn, Basil Rathbone and David Niven as Royal Flying Corps fighter pilots in World War I. Of the several films that Flynn and Rathbone appeared in together, it is the only one in which their characters are on the same side. Although sparring as in their other roles, their characters are fast friends and comrades in danger.

The Dawn Patrols story romanticizes many aspects of the  ." Twatio, Bill.   Esprit de Corps, Volume 12, June 2004. Retrieved: April 1, 2009.  However, The Dawn Patrol also has a deeper and more timeless theme in the severe emotional scarring on a military commander who must constantly order men to their deaths.   This theme underlies every scene in The Dawn Patrol. Howard, Ed.   Only the Cinema, 2009. Retrieved: April 2, 2009. 

==Plot==
In 1915, at the airdrome in France of the Royal Flying Corps 59th Squadron, Major Brand (Basil Rathbone), the squadron commander, and his adjutant Phipps (Donald Crisp) anxiously await the return of the dawn patrol.  Brand is near his breaking point. He has lost 16 pilots in the previous two weeks, nearly all of them young replacements with little training and no combat experience. Brand is ordered to send up tomorrow what amounts to a suicide mission. Captain Courtney (Errol Flynn), leader of A Flight, and his good friend "Scotty" Scott (David Niven) return, but two of the replacements are not so lucky, and another, Hollister, is severely depressed by having witnessed the death of his best friend. The survivors repair to the bar in their mess for drinks and fatalistic revelry. Courtney does his best to console Hollister, but the youngster breaks down in grief. 

When Brand announces the next days dawn patrol, Courtney tells Brand he does not have enough men. Brand retorts that more replacements are on their way. From the four green pilots, Courtney picks the two with the most flying hours to go on the mission. Only four return this time; Scott has been lost along with the two new men. Courtney tells a sympathetic Brand that Scott went down saving Hollister. Just then, British troops bring in the German who downed Scott, Hauptmann Von Mueller (Carl Esmond). Courtney overcomes his initial rage when Brand informs Von Mueller that it was Courtney who shot him down, and the German graciously acknowledges him. Courtney then offers the German a drink. The guilt-ridden Hollister tries to attack the prisoner, but is restrained. Then, a grimy Scott appears. His fighter crashed, but he survived.
 Michael Brooke), informs the squadron that the dreaded Von Richter is now their foe, an enemy aircraft flies low over their airdrome and drops a pair of trench boots. Attached is a taunting note telling the British pilots that they will be safer on the ground. Brand warns his men that the boots are intended to incite inexperienced pilots into trying to retaliate. He forbids any takeoffs without his express orders. Courtney and Scott disregard the prohibition, taking off in the dawn mist after stealing the boots from Brands room. They fly to Von Richters airfield, where the black-painted fighters are being readied for the day. Courtney and Scott bomb and strafe the field, destroying most of the German aircraft, and shoot down two which try to take to the air. Courtney then drops the boots. Von Richter retrieves them and shakes his fist at the departing British. Courtney is shot down recrossing the lines, then rescued by Scott, whose aircraft is also hit by Anti-aircraft warfare|anti-aircraft fire. When leaking oil blinds Scott, Courtney talks him down to a crash landing behind their own trenches.

Brands outrage at their disobedience dissipates when headquarters congratulates him for the success of the attack and appoints him "up to Wing (air force unit)|Wing." Brand takes cruel pleasure in naming Courtney to take command of the 59th. Soon, Courtney is forced to acquire all the qualities he hated in Brand. When Scotts younger brother Donnie is posted as a replacement, Scott begs Courtney to give him a few days so that he can teach his brother the ropes. Courtney tells him there can be no exceptions. Unbeknownst to Scott, Courtney calls headquarters to plead for a few days of training for his replacements, but is turned down. Von Richter shoots down Donnie in flames the next morning, for which Scott blames Courtney.

Brand personally gives Courtney orders for a very important mission. A single aircraft must fly low and bomb a huge munitions dump 60 kilometers behind the lines. Brand bans Courtney from flying the mission, so Scott disdainfully volunteers. They reconcile and Courtney gets his friend too drunk to fly, then blows up the dump himself. Afterward, Von Richter intercepts Courtney. Although Courtney outduels and shoots down two of the enemy, including Von Richter, he is killed by a third pilot. Command of the squadron devolves on Scott. He lines up the decimated squadron for orders just as five replacements arrive. He stoically tells A Flight to be ready for the dawn patrol.

==Cast==
As credited, with screen roles identified:   IMDb. Retrieved: May 21, 2013. 
* Errol Flynn as Captain (later Major) "Court" Courtney
* Basil Rathbone as Major Brand
* David Niven as Lieutenant (later Captain) "Scotty" Scott
* Donald Crisp as Phipps
* Melville Cooper as Sergeant Watkins
* Barry Fitzgerald as Bott
* Carl Esmond as Hauptmann Von Mueller
* Peter Willes as Hollister
* Morton Lowry as Donnie Scott Michael Brooke as Captain Squires James Burke as Flaherty (motorcycle driver)
* Stuart Hall as Bentham Herbert Evans as Scotts mechanic
* Sidney Bracey as Major Brands orderly (credited as Sidney Bracy)
* John Rodion as Lieutenant Russell 
* Leo Nomis as Aeronautic supervisor 

==Production==
The screenplay from the first Dawn Patrol was reprised by original screenwriter Seton Miller, even though its dialogue had been limited because it had been one of the first sound pictures. Miller, in conjunction with director Edmund Goulding, although following the original closely and crediting original co-writer Dan Totheroh, primarily rewrote dialogue to incorporate the characters played by Flynn, Rathbone and Niven. 
 German annexation of Austria the month before.  Goulding was available to direct after being taken off the filming of Jezebel (film)|Jezebel in favor of William Wyler.  Warner had hired Goulding in 1937 on a per-film basis after Louis B. Mayer had fired him from MGM Studios in a sex scandal,  and offered him the Dawn Patrol assignment to keep him interested while he completed preparations for filming Dark Victory, which he also wanted Goulding to direct.  Although Goulding detested remakes, he had successfully filmed a remake of one of his own films as his first Warner Brothers project and agreed.  
 The Adventures Matthew Kennedy wrote:
 Everyone remembered a set filled with fraternal good cheer ... The filming of Dawn Patrol was an unusual experience for everyone connected with it, and dissipated for all time the legend that Britishers are lacking in a sense of humor ... The picture was made to the accompaniment of more ribbing than Hollywood has ever witnessed. The setting for all this horseplay was the beautiful English manners of the cutterups. The expressions of polite and pained shock on the faces of Niven, Flynn, Rathbone et al., when (women) visitors were embarrassed was the best part of the nonsense.  

Principal photography began on August 6, 1938, and ended six weeks later on September 15. Airfield exteriors were shot at the Warner Brothers Ranch near Calabasas, California.  

===Aircraft===
 s for shots of entire squadrons, some of which were blown up in explosions, and   fitted with a round cowl over its Comet engine to resemble the Nieuports. Stunt pilots included Leo Nomis, Rupert Symes Macalister, Frank Tomick and Roy Wilson.   Aerofiles.com. Retrieved: April 1, 2009. 
 national insignia, had the marking "NIEU 24" painted on their tail fins, and displayed a cartoon Hornet painted on each side of the fuselage just behind the cockpit.  

For scenes at the German airdrome in which aircraft were moved or had engines turning, Goulding used "Wichita Fokkers" painted black with German markings. His "Pfalzes" had their wings painted in a large and striking red and white checkerboard pattern. Goulding also acquired two genuine Pfalz D.XII fighters for static closeup shots of parked fighters, with at least one repainted white in a later scene to "expand" their numbers. Actual Nieuport 28s and Pfalz D.XIIs were used much later in the war than the 1915 setting of The Dawn Patrol, while the model 28 Nieuport was not used by the RFC at all, but their familiarity of appearance to American audiences gave a verisimilitude to both films. 

==Theme==
The original script developed for Howard Hawks, which Edmund Goulding followed closely, stressed thematic elements that came to be associated with the "Hawksian world" of "pressure cooker" situations: a professional group of men who live by a code and face death with bravado and camaraderie; the responsibility of leadership in perilous situations; a preference for individual initiative over orders; suicidal missions; and the grandeur of aerial flight.  

===Visual motifs===
The Dawn Patrol uses four scene elements as visual motifs to describe a cyclical nature to war and a nightmarish quality in being in command. Each scene places the viewer in the commanders place, waiting with dread for the inevitable consequences. Nearly all action scenes in The Dawn Patrol cut away before the conclusion is played out to heighten the sense of dread. Each scenes recurrence has differences that accentuate those consequences: 
* Counting the sounds of the motors of the returning aircraft to determine how many have died. In the final scenario, when Scott waits to hear Courtneys motor in the dark, his initial joy that Courtney has returned is crushed by the dropping of his dead comrades goggles and helmet.
* Receiving orders for the next mission by telephone, with the generals voice barely audible but impatient in tone. The apparent insanity of the orders is emphasized by the callous implacability of the distant, detached headquarters.
* Arrival of replacements in time to enable the next mission but never with time to train them to protect themselves. The poignancy of their eager youthfulness is symbolized by their arrival in an open touring car in a seemingly endless parade.
* Assembling the pilots to issue the orders, with new men ushered into line just after their arrival. Brand, Courtney and Scott, all of whom would rather be flying the mission than ordering it, assume an identical stoicism as they undergo the ordeal. 

===Music=== gramophone in Plague in India. Finally, each arrival of new replacements is announced by their cheerful singing of "Pack Up Your Troubles in Your Old Kit-Bag", demonstrating their callow eagerness.     IMDb. Retrieved:  March 31, 2009. 

In addition, the flyers carousing nature is emphasized in one scene with music. After Scotts return, he and Courtney drive off in a motorcycle sidecar boisterously singing "Plum and Apple" with their enlisted driver. This was an enlisted mens/other ranks ditty about the class distinctions of rations at the front. 

==Reception==
With another world war threatening, the pacifistic script of The Dawn Patrol was well received by audiences and critics, and it succeeded at the box office.   filmreference.com. Retrieved: April 1, 2009.  Variety (magazine)|Variety echoed many of the critics reactions: "Dawn Patrol sparkles because of vigorous performances of the entire cast and Edmund Gouldings sharp direction. Story   is reminiscent of previous yarns about the flying service at the front during the World War. Yet it is different in that it stresses the unreasonableness of the brass hats - the commanders seated miles from the front who dispatched the 59th Squadron to certain death in carrying out combat assignments." 

==DVD release==
On March 27, 2007, Warner Bros. released The Dawn Patrol on DVD.  Special features include Warner Night at the movies 1938: Vintage Newsreel, musical shorts "The Prisoner of Swing" and "Romance Road."  Also included is the vintage Looney Tunes cartoon What Price Porky?

==References==

===Notes===
 

===Citations===
 

===Bibliography===
 
* Kennedy, Matthew. Edmund Gouldings Dark Victory: Hollywoods Bad Boy Genius, Madison, Wisconsin: University of Wisconsin Press, 2004. ISBN 0-299-19770-0.
* McCarthhy, Todd. Howard Hawks: The Gray Fox of Hollywood. New York: Grove Press, 2000. ISBN 0-8021-3740-7.
* York, Dorothea. Mud and Stars: An Anthology of World War Songs and Poetry. London: Stewart Press, 2007, First edition 1931. ISBN 1-4067-3895-6.
 

==External links==
*  
*  
*  
*   This aviation site section has a publicity photo on its front page of Niven, Flynn, and Rathbone as their characters in The Dawn Patrol.

 

 
 
 
 
 
 
 
 
 
 
 
 
 