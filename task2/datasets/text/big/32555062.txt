Salvation Nell (1931 film)
{{infobox film
| name           = Salvation Nell
| image          =
| imagesize      =
| caption        =
| director       = James Cruze
| producer       = James Cruze Samuel Zierler
| writer         = Edward Sheldon (play)
| starring       = Ralph Graves Helen Chandler
| cinematography = Charles Schoenbaum|C. Edgar Schoenbaum
| editing        = Rose Loewinger
| distributor    = Tiffany Pictures
| released       = July 1, 1931
| runtime        = 8 reels (7,652.8 feet)
| country        = United States
| language       = English 
}} Tiffany Films, Edward Sheldons 1908 Broadway play which starred Minnie Maddern Fiske. 
 silent film. After years of being thought a lost film, a print was shown at a film festival in 2001.  

==Cast==
*Ralph Graves - Jim Platt
*Helen Chandler - Nell Saunders
*Sally ONeil - Myrtle Jason Robards - Major Williams
*DeWitt Jennings - McGovern Charlotte Walker - Maggie
*Matthew Betz - Mooney
*Rose Dione - Madame Cloquette
*Wally Albright - Jimmy

==Plot==
Young Nell (Chandler) loses her job and home and her father is sent to prison. She joins the Salvation Army and tries to redeem him when he comes out of prison, bent on continuing his life of crime.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 

 