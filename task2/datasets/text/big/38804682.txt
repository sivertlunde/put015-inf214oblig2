Reefer and the Model
{{Infobox film
| name           = Reefer and the Model
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Joe Comerford
| producer       = Lelia Doolan
| writer         = Joe Comerford
| starring       = {{Plainlist |
* Ian McElhinney
* Carol Scanlan
* Sean Lawlor }}
| music          = Andy Roberts, Johnny Duhan
| cinematography = Breffni Byrne
| editing        = Sé Merry Doyle
| studio         = {{Plainlist |
* Berber Films Film Four International
* Raidió Teilifís Éireann Bord Scannán na hÉireann
* Metro Pictures }}
| distributor    = Metro Pictures
| released       =  
| runtime        = 93 minutes Ireland
| language       = English
}}
 Irish film Galway coast and the Aran Islands; his friends Spider and Badger; and the pregnant Teresa ("the Model"), who has abandoned a life of drugs and prostitution in England. The group become involved in the armed robbery of a post office and are pursued by the Garda Síochána|Gardaí. The film premièred in Galway in August 1988. Screening Ireland: Film and Television Representation by Lance Pettitt, Manchester University Press (2000), p. 110. ISBN 0 7190 5269 6. 

== Awards ==
 Celtic Film Festival in Wales.  At the 1988 European Film Awards, the film was nominated for Best Young Film, Carol Scanlan was nominated for Best Actress, and Ray McBride was nominated for Best Supporting Actor.

==Cast==

* Ian McElhinney – Reefer
* Carol Scanlan – Teresa the Model
* Sean Lawlor – Spider
* Ray McBride – Badger
* Eve Watkinson – The Mother
* Birdy Sweeney – Instant Photo
* Fionn Comerford – Messenger Boy
* John Lillis – Porter
* Henry Comerford – Waiter
* Paraic Breathnach – Quayside Fisherman
* Maire Chinsealach – Island Woman
* Dave Duffy – Sergeant
* Rosina Brown – The Blonde
* Little John Nee – Boy Soldier
* Seán Ó Coisdealbha – Rossaveal Skipper
* Noel Spain – Boatman
* Peter Fitzgerald – Bank Guard
* Dick Donaghue – Bank Teller
* Máire Ní Mháille – Bank Teller
* Michael Rowland – Older Bank Guard (as Mick Rowland)
* Patrick Blackaby – 1st Tinker Guard
* Uinseann Mac Thomáis – 2nd Tinker Guard
* Deirdre Lawless – Policewoman
* Gary McMahon – Young Guard

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 