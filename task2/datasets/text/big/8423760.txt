Adela (2000 film)
{{Infobox film
| name           = Adela
| image          = Adela2000.jpg
| alt            = 
| caption        = 
| director       = Eduardo Mignogna
| producer       = Eduardo Mignogna
| writer         = Eduardo Mignogna François-Olivier Rousseau
| based on       =  
| starring       = Eulalia Ramón Grégoire Colin
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Argentina Spain
| language       = Spanish
| budget         = 
| gross          = 
}}

Adela is a 2000 Argentine thriller film directed and written by Eduardo Mignogna. The film was based on the novel Le Coup de lune by Georges Simenon, adapted by François-Olivier Rousseau.  The film starred Eulalia Ramón and Grégoire Colin.

==Plot==
 

==Cast==
* Eulalia Ramón as Adèle
* Grégoire Colin as Timar
* Martin Lamotte as Eugène
* Mario Gas as Kruger
* Isabel Vera as María
* Martín Adjemián as Enríquez
* Jordi Dauder as Gerente

==Release==
The film was released in Canada and Spain.

==External links==
*  

 
 
 
 
 
 
 
 
 


 
 