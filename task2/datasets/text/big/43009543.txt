My Old Lady (film)
 
{{Infobox film
| name           = My Old Lady
| image          = My_Old_Lady_-_US_Theatrical_Poster.jpg
| alt            = 
| caption        = US theatrical poster
| director       = Israel Horovitz
| producer       = Rachael Horovitz Gary Foster Nitsa Benchetrit David C. Barrot   co-producer  David Atrakchi Gael Cabouat Boris Mendza
| writer         = Israel Horovitz
| starring       = Maggie Smith Kevin Kline Kristin Scott Thomas Dominique Pinon
| music          = Mark Orton
| cinematography = Michel Amathieu
| editing        = Jacob Craycroft Stephanie Ahn
| studio         = BBC Films Deux Chaveaux Films Fulldawa Films
| distributor    = United Kingdom: Curzon Film World United States: Cohen Media Group
| released       =  
| runtime        = 107 minutes  
| country        = United Kingdom United States France
| language       = English
| budget         = 
| gross          = $8,651,792 
}}
My Old Lady  is a British–French–American comedy-drama film written and directed by Israel Horovitz in his feature directorial debut.  He had previously directed 3 Weeks After Paradise, a 51-minute testimonial from 2002 about his familys experiences following the September 11, 2001, terrorist attacks on New Yorks World Trade Center. The film stars Maggie Smith, Kevin Kline, Kristin Scott Thomas, and Dominique Pinon.  It was screened in the Special Presentations section of the 2014 Toronto International Film Festival. 

==Plot==
Mathias, a down-and-out New Yorker, travels to Paris to liquidate a huge, valuable apartment he has inherited from his estranged father. Once there, however, he discovers a refined old woman, Mathilde, living in the apartment with her daughter, Chloé.

Mathias quickly learns that the apartment is a "viager" — an ancient French system for buying and selling apartments — and that he will not actually get possession of the apartment until Mathilde dies, and that he, in addition to that, owes her a life annuity of € 2400 a month. All this is a surprise to him, since his father never told him and he had language problems with the French lawyer.

Mathias has no money and no place to live, but Mathilde allows him to stay in the apartment with her. However, to pay for the next life annuity payment he steals and sells furniture from the apartment, and embarrassingly asks a prospective buyer of his contract for an advance payment.

Mathias discovers that Mathilde and his father had a long-lasting affair while both were married. Mathias and Chloé fall in love with each other, and since Chloé wants to stay in the apartment, Mathias decides at the last minute to decline a multimillion offer for the apartment/contract. Mathilde points out that Mathias does not have to worry about money: he himself can sell en viager, allowing him to live in the apartment while also providing him a life annuity (albeit a modest one, because of his fairly young age of 57).

==Cast==
* Maggie Smith as Mathilde Girard 
* Kevin Kline as Mathias "Jim" Gold 
* Kristin Scott Thomas as Chloé Girard 
* Dominique Pinon as Monsieur Lefebvre 
* Stéphane Freiss as François Roy
* Noémie Lvovsky as Dr. Florence Horowitz
* Stéphane De Groodt as Philippe

==Reception==
 
My Old Lady received mixed to positive reviews, garnering a 59% rating on review aggregator website Rotten Tomatoes based on 74 reviews. The sites consensus reads, "Although My Old Lady doesnt quite live up to its stars talents, Kevin Kline and Maggie Smith carry the film capably whenever theyre together onscreen."  On Metacritic, the film has a 53/100 rating based on 19 critics, indicating "mixed or average reviews". 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 