Héroes de Otra Patria
{{Infobox Film
| name = Héroes de Otra Patria
| image = Dvd heroes de otra patria small.jpg
| caption = 
| writer = Iván Dariel Ortíz
| starring = Jorge Castillo Jimmy Navarro Alba Raquel Barros Adamari López
| director = Iván Dariel Ortíz
| producer = Iván Gonzalo Ortíz
| distributor = 
| released = 1998
| runtime = 90 minutes
| country = USA (Puerto Rico)
| language = Spanish, English
| music = 
| awards =
| budget = 
|}} Puerto Rican film, written and directed by Iván Dariel Ortíz. 

The film follows the lives of two Puerto Rican soldiers in the middle of the Vietnam War. After being sent on a reconnaissance mission, their squad is ambushed and they get lost in the jungle. Meanwhile, we see the struggles being suffered by their relatives after their departure from the island.

Tagline: Cuando usaron de arma la inocencia. (When they used innocence as a weapon.)

==Cast== Jorge Castillo - Carlos
* Jimmy Navarro - Raúl
* Alba Raquel Barros - Doña Pura
* Adamari López - Esther
* Domingo Quiñones - Sgt. Miller

==Awards==
The film received an honorary mention at the Viña del Mar Festival in Chile.

==See also==
*Cinema of Puerto Rico
*List of films set in Puerto Rico
*List of Puerto Ricans in the Academy Awards

==External links==
*  

 
 
 
 


 
 