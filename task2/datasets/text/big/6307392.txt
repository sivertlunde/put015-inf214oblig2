Thanks a Million
{{Infobox film
| name           =Thanks a Million
| image          =Thanks a Million.jpg
| caption        =theatrical poster
| director       =Roy Del Ruth
| writer         =   Melville Crossman Uncredited: Fred Allen James Gow Edmund Gross Harry Tugend
| starring       =Dick Powell Ann Dvorak Fred Allen
| released       = 
| producer       =Darryl F. Zanuck
| studio         =20th Century Pictures
| distributor    =20th Century Fox
| music          =Arthur Lange
| cinematography =J. Peverell Marley
| editing        =Allen McNeil
| runtime        =87 minutes
| country        =United States
| language       =English
}}

Thanks a Million is a 1935 musical film produced and released by 20th Century Fox and directed by Roy Del Ruth.  It stars Dick Powell, Ann Dvorak and Fred Allen, and features Patsy Kelly, David Rubinoff and Paul Whiteman and his band with singer/pianist Ramona.  The script by Nunnally Johnson was based on a story by producer Darryl F. Zanuck (writing as Melville Crossman) and contained uncredited additional dialogue by Fred Allen, James Gow, Edmund Gross and Harry Tugend.

Thanks a Million was nominated for the Academy Award for Sound (E. H. Hansen) in 1935.    It was remade in 1946 as If Im Lucky, with Perry Como and Phil Silvers in the Dick Powell and Fred Allen roles. 

==Plot==
Stranded in a small town in a downpour, the manager of a traveling musical show (Fred Allen) convinces the handlers of a boring long-winded local judge running for governor (Raymond Walburn) to hire his group to attract people to the politicians rallies.  When the shows crooner, Eric Land (Dick Powell), upstages the Judge, hes fired, but on a return visit he saves the day by standing in for the Judge, who is too drunk to speak.  Impressed by his poise, the partys bosses ask Eric to take over as candidate, and the singer, knowing he has no chance to win, agrees for the exposure and the radio airtime in which he can showcase his singing. Soon, though, his girlfriend Sally (Ann Dvorak) becomes annoyed at the amount of time Eric is spending with the wife of one of the bosses, and she leaves when she thinks he has lied to her.  When the bosses ask Eric to agree to patronage appointments that will lead to easy graft for all of them, he exposes them on the radio, telling the voters that voting for him would be a huge mistake and urging them to vote for his opponent.  At the end Eric is, of course, elected governor, and re-united with Sally.

==Cast==
*Dick Powell as Eric Land
*Ann Dvorak as Sally Mason
*Fred Allen as Ned Allen
*Patsy Kelly as Phoebe Mason
*Raymond Walburn as Judge Culliman
*Benny Baker as Tammany
*Alan Dinehart as Mr. Kruger
*Andrew Tombes as Mr. Grass Paul Harvey as Maxwell
*Edwin Maxwell as Mr. Casey Charles Richman as Gov. Wildman
*Paul Whiteman as Himself Ramona as Herself
*David Rubinoff as Himself
*Yacht Club Boys

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 