Kanana Sundari
{{Infobox film 
| name           = Kanana Sundari
| image          =
| caption        =
| director       = P Chandrakumar
| producer       = S Kumar
| writer         =
| screenplay     =
| starring       = P Sukumar Abhilasha
| music          = Jerry Amaldev
| cinematography =
| editing        =
| studio         = Sastha Productions
| distributor    = Sastha Productions
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, directed by P Chandrakumar and produced by S Kumar. The film stars P Sukumar and Abhilasha in lead roles. The film had musical score by Jerry Amaldev.   

==Cast==
*P Sukumar
*Abhilasha

==Soundtrack==
The music was composed by Jerry Amaldev and lyrics was written by Devadas. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Prayamayya || S Janaki || Devadas || 
|-
| 2 || Thamburaane || Chorus || Devadas || 
|}

==References==
 

==External links==
*  

 
 
 

 