Love, Speed and Thrills
 
{{Infobox film
| name           = Love, Speed and Thrills
| image          =
| caption        =
| director       = Walter Wright
| producer       = Mack Sennett
| writer         =
| starring       = Mack Swain Chester Conklin Minta Durfee
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 13 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| website        =
}}

Love, Speed and Thrills is a 1915 American short comedy film produced by Mack Sennett, directed by Walter Wright, starring Mack Swain, Chester Conklin and Minta Durfee, and featuring Billy Gilbert, Charley Chase and the Keystone Cops in supporting roles.

== Plot summary ==
 
Love, Speed and Thrills involves a loving husband and a wife-stealing wolf, both after the same woman.

== Cast ==
*Mack Swain as Ambrose
*Minta Durfee as Ambroses Wife
*Chester Conklin as Mr. Walrus
*Billy Gilbert
*Josef Swickard as Chief of the Keystone Kops
*Charley Chase

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 


 