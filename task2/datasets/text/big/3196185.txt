Fire on the Amazon
{{Infobox film
| name           = Fire on the Amazon
| image          = Fireonamazon.jpg
| image_size     =
| caption        = Fire on the Amazon DVD cover
| director       = Luis Llosa
| producer       = Luis Llosa
| writer         = Luana Anders Catherine Cyran Beverly Gray
| narrator       = Juan Fernández Judith Chapman Ramsay Ross
| music          = Roy J. Ravio
| cinematography = Pili Flores-Guerra
| editing        = Robert L. Goodman Michael Thibault
| distributor    = Concorde-New Horizons
| released       =  
| runtime        = 78-102 min (depending on cut)
| country        = United States Peru English Spanish Spanish
| budget         =
| gross          =
}} Peruvian drama film|drama-adventure film directed by Luis Llosa and starring Craig Sheffer and Sandra Bullock.

The low-budget film features an early appearance of Bullock, before her breakout role in Speed (1994 film)|Speed.

== Plot ==
In Bolivias Amazon basin, corporate cattle ranches are replacing the rain forest. When Santos, charismatic leader of the union of rubber tappers, forges an alliance with Indians to protest deforestation, he is assassinated. OBrien, a US photo-journalist who has no skills as an investigator, wants a story when he thinks the police have framed and murdered an innocent Indian as the assassin. In his search for the truth, he involves Lysa Rothman, who worked for Santos and with whom he falls in love. As he gets deeper into trouble with the cops and the real assassin, he not only needs Lysas help but that of the Indians leader. How many will die so OBrien can get his story?.

== Cast ==
* Craig Sheffer as R.J.
* Sandra Bullock as Alyssa Rothman Juan Fernández as Ataninde
* Judith Chapman as Sandra
* Ramsay Ross as Pistoleiro
* David Elkin as Lucavida
* Jorge García Bustamante as Valdez
* Baldomero Cáceres as Pedro
* Carlos Victoria as Miguel Reynaldo Arenas as Djamori

== Production ==
As reported on A&E Network|A&Es Biography (TV series)|Biography on November 18, 2005, in an interview clip with the films director, the film was a harrowing experience in many ways. Bullock was concerned that the canoe she was riding could have possibly tipped over and dumped her in dangerous waters. Bullock also did her only "nude" scene wearing strategically placed duct tape, which the director admiringly pointed out was never seen in the film (because of the way Bullock positioned herself), thus "preserving the illusion".

== External links == New Horizons Pictures
*  

 

 
 
 
 
 
 
 


 