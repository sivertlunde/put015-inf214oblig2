Hardboiled Rose
{{Infobox film
| name           = Hardboiled Rose
| director       = F. Harmon Weight
| producer       = Joseph Jackson (titles)
| starring       = Myrna Loy William Collier, Jr. John Miljan William Rees William Holmes
| distributor    = Warner Bros.
| released       = March 30, 1929
| country        = United States
| language       = English
}}

Hardboiled Rose (1929) is a 62-minute part-talkie released by Warner Bros. and starring Myrna Loy, William Collier, Jr. and John Miljan.

==Plot==
A Southern belle (Loy) must work in a gambling house to pay off her fathers debts, which drove him to suicide. She then meets a man who sweeps her off her feet and takes her away from it all.

==Cast==
* Myrna Loy as Rose Dunhamel
* William Collier, Jr. as Edward Malo
* John Miljan as Steve Wallace
* Gladys Brockwell as Julie Malo
* Lucy Beaumont as Grandmama Dunhamel
* Ralph Emerson as John Trask
* Edward Martindel as Jefferson Dunhamel
* Otto Hoffman as Apyton Hale
* Floyd Shackelford as butler

==Production== The Desert Under a Texas Moon (1930, the second all color-all talking movie to be filmed outdoors).
 The Thin Man.

==Sound==
According to TV Guide.coms review of Hardboiled Rose, the talking sequences were added to the movie later in production. All studios were converting to sound, so major studio releases had to be at least a part-talkie.

==Film preservation==
The film elements for Hardboiled Rose still survive, but the soundtrack which was recorded on Vitaphone discs, is lost except the fourth reel disc. 

==See also==
*Vitaphone
*Myrna Loy filmography
*Silent movies
*List of Warner Bros. films

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 