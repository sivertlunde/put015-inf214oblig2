Amar Shakti
{{Infobox film
| name           = Amar Shakti
| image          = AmarShakti.jpg
| image size     =
| caption        = Poster
| director       = Harmesh Malhotra
| producer       = A.K. Nadiadwala
| writer         =
| narrator       =
| starring       = Shashi Kapoor Shatrughan Sinha Sulakshana Pandit Alka
| music          = Laxmikant-Pyarelal
| cinematography =
| editing        =
| distributor    =
| released       = 1978
| runtime        =
| country        = India Hindi
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 1978 Bollywood|Hindi film produced and directed by Harmesh Malhotra. It stars Shashi Kapoor, Shatrughan Sinha, Sulakshana Pandit and Alka.

==Cast==
*Shashi Kapoor as  Chhota Kumar / Amar Singh
*Shatrughan Sinha as  Bada Kumar / Shakti Singh
*Alka as  Yuvrani Roopa G. Singh
*Birbal as  Gopal
*Indrani Mukherjee as  Leela
*Rajan Haksar as  Yuvraj Gopal Singh
*Jankidas as  Hakim
*Jeevan as Diwan Nahar Singh
*Pradeep Kumar as  Maharaj Shamsher Singh
*Roopesh Kumar as  Yuvraj Kunver Ranjit Narayan Singh
*Manjula as  Chamki
*Ram Mohan as  Shaktis friend
*Murad as Maharaj
*Sulakshana Pandit as  Rajkumari Sunita.
*Ranjeet as  Sardars son
*Mohan Sherry as Kotwal Sher Singh
*Om Shivpuri as  Sardar

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Amar Hai Shakti"
| Mohammed Rafi, Kishore Kumar, Anuradha Paudwal, Chandrani Mukherjee
|-
| 2
| "Mohabbat Mein Nighahon Se"
| Mohammed Rafi, Sulakshana Pandit
|-
| 3
| "Sahebo Hum Aapko"
| Kishore Kumar, Asha Bhosle
|-
| 4
| "Thehro Thehro"
| Asha Bhosle
|}
==External links==
*  

 
 
 
 

 