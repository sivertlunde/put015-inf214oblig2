Mary Stevens, M.D.
{{Infobox film
| name           = Mary Stevens, M.D. 
| image          = 
| image_size     = 
| caption        = 
| director       = Lloyd Bacon
| producer       = Hal B. Wallis (uncredited)
| writer         = Rian James Robert Lord
| story          = Virginia Kellogg
| based on       =
| narrator       = 
| starring       = Kay Francis Lyle Talbot Glenda Farrell
| music          =  Sid Hickox
| editing        = Ray Curtiss
| studio         = Warner Bros.
| distributor    = Warner Bros.
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Mary Stevens, M.D. is a 1933 American drama film starring Kay Francis, Lyle Talbot and Glenda Farrell. A female doctor has romantic troubles.

==Cast==
 
* Kay Francis as Mary Stevens
* Lyle Talbot as Don
* Glenda Farrell as Glenda
* Thelma Todd as Lois
* Harold Huber as Tony Una OConnor as Mrs. Arnell Simmons Charles Wilson as Walter Rising
* Hobart Cavanaugh as Alf Simmons George Cooper as Pete
* John Marston as Dr. Lane
* Christian Rub as Gus Walter Walker as Dr. Clark

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 

 