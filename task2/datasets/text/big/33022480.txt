Kiss and Tell (1945 film)
 
{{Infobox film
| name           = Kiss and Tell
| image          = 
| image_size     = 
| caption        =  Richard Wallace
| producer       = Sol C. Siegel
| writer         = F. Hugh Herbert from his own play
| starring       = Shirley Temple Jerome Courtland Walter Abel Katharine Alexander
| music          = Werner R. Heymann
| cinematography = Charles Lawton Jr. Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
}}

Kiss and Tell is a 1945 American comedy film starring then 17-year-old Shirley Temple as Corliss Archer. In the film, two teenage girls cause their respective parents much concern when they start to become interested in boys. The parents bickering about which girl is the worse influence causes more problems than it solves.
 Broadway play Kiss and Tell, which was based on the Corliss Archer short stories. The stories, play and movie were all written by F. Hugh Herbert. A sequel film, A Kiss for Corliss, was released in 1949 and also starred Temple, but was not written by Herbert.

==Plot Summary==

To boost sales and attract customers at the local bazaar, fifteen-year-old Corliss Archer and seventeen-year-old Mildred Pringle decide to start selling kisses.

When their booth at a USO bazaar fails to attract customers, teenager Corliss Archer suggests to her best friend, Mildred Pringle, that they sell kisses. The idea becomes a success among the soldiers visiting the bazaar, and business is booming, until the girls mothers find out about it. Despite the fact that it was Corliss idea, Mrs. Archer blames Mildred for the girls behavior. Mrs. Archers assumptions greatly upset Mr. Pringle when he hears about them at the Pringle dinner table. He decides the family, including Mildred and their son Raymond, will refrain from socializing with the Archers in the future.

That same night, Corliss pretends to be older than she is and starts flirting shamelessly with young Private Jimmy Earhart, who has been invited into the Archer home for dinner. Corliss is actually dating the boy next door, Dexter Franklin.

Late that night, Lenny Archer returns home on a short leave from his Air Force service. He goes directly to his girlfriend Mildred and proposes to her, suggesting they elope and marry right away, before he is shipped off overseas. By the next morning the couple has married across the state line. They go home to inform their respective families about their activities and plans, but find that the Pringles and the Archers no longer are on speaking terms since the feud has intensified.

Lenny only tells his sister Corliss of what they have done, and make her swear not to tell anyone. The feud gets worse when Mr. Pringle and Mr. Archer start a fist fight and punch each other in the face. The injuries from the fight result in numerous lawsuits from both families against each other. The feud drags on for months.

Eventually Mildred finds out that she is pregnant, and she goes to see a doctor. Corliss comes with her, and is spotted by Mrs. Wilcox, the worst blabbermouth in town. Corliss is seen talking to Jimmy after she comes out from the doctor, and Mrs. Wilcox instantly and eagerly passes the information on to Mrs. Pringle.

Mrs. Pringle sees her chance to get back at the Archers, so she goes there and claims that Corliss is pregnant and that Jimmy is the father. Corliss doesnt want Mildred and her brother to get into trouble, so she admits to being pregnant. When Mrs. Archer tries to call Jimmys superior officer to scold him, Corliss says Dexter is the father. Then she intercepts her mother by telling Dexter what she has said, asking him to help her out by lying.

Corliss tries to soften the blow for her parents by lying again, telling them that she and Dexter are already married. Mr. Archer doesnt believe her, so she tells him they were married across the state line. When Mr. Archer calls the justice who performed the ceremony, the man confirms that an Archer was married there months ago. Soon the Franklins are informed of their sons endeavours, and both families gather at Archers house. Uncle George Archer, who is a Navy chaplan, insists on performing another wedding ceremony for the young couple.

The same night, Mildred hears news that Lenny is coming home soon, and she gets the courage to tell her family about her marriage and pregnancy. She also tells her mother about Corliss going with her to the doctor, which makes Mrs. Archer realize Mrs. Wilcox was wrong.

The Pringles run over to tell the news to the Archers, and enters the house to wedding bells. Mr. Archer starts chasing Mr. Pringle but soon finds out that they are both to be grandparents. The two families finally reconcile. 

==Cast==
*Shirley Temple as Corliss Archer
*Jerome Courtland as Dexter Franklin
*Walter Abel as Mr. Archer
*Katharine Alexander as Mrs. Archer
*Robert Benchley as uncle George Archer
*Porter Hall as Mr. Franklin
*Virginia Welles as Mildred Pringle
*Tom Tulley as Mr. Pringle
*Darryl Hickman as Raymond Pringle
*Mary Philips as Mrs. Pringle
*Scott McKay as Private Jimmy Earhart
*Scott Elliott as Lenny

==External links==
*  

==References==
 

 

 
 
 
 
 
 
 
 