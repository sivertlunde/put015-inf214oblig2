The Santa Clause 2
{{Infobox film
| name           = The Santa Clause 2
| image          = Santa_Clause_2.jpg
| caption        = Theatrical release poster
| director       = Michael Lembeck
| producer       = Robert Newmyer Brian Reilly Jeffrey Silver Cinco Paul Ken Daurio Ed Decter John J. Strauss
| based on       =  
| starring       = Tim Allen Eric Lloyd Elizabeth Mitchell Judge Reinhold Wendy Crewson David Krumholtz Liliana Mumy Spencer Breslin
| music          = George S. Clinton Adam Greenberg
| editing        = David Finfer Edward A. Warschilka
| studio         = Walt Disney Pictures Outlaw Productions Boxing Cat Films Buena Vista Pictures Distribution
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $65 million   
| gross          = $172,855,065 
}}

The Santa Clause 2 (also known as The Santa Clause 2: The Mrs. Clause) is a 2002 American  , released in 2006.

==Plot==
Eight years after  ), is on the naughty list. The film then cuts to him one night defacing the walls of the school gymnasium from the skywindow until he is caught by Principal Carol Newman (Elizabeth Mitchell).
 Tooth Fairy (Art LaFleur), and the Sandman (Michael Dorn).

In the meantime, Scott stays with his ex-wife Laura (Wendy Crewson) and her husband Neil (Judge Reinhold). The two have grown to adore and idolize him simply because hes Santa. They now have a daughter named Lucy (Liliana Mumy), who sees Scott as an uncle figure, but suspects that hes Santa Claus.

To cover for Santas prolonged absence, Curtis helps him create a life-size toy replica of him, much to Bernards horror and doubt, and Bernard reluctantly tells the other elves that Santa has simply had a change of looks. At first everything seems to be going okay, until the larger-than-life legalist demands to double check the naughty-nice list. As he cannot find any grace, mercy, or slack in his plastic heart for minor infractions by children all over the world, having followed the handbook too literally and strictly, he thinks that every child in the world, is naughty (just by petty mistakes). Toy Santa takes control of the North Pole, turns it into a strict and evil government, with a duplicated army of life-size toy soldiers, and he plans on giving the entire world lumps of coal. Bernard cracks and exclaims that the Toy Santa is a fake, and Toy Santa immediately puts him under house arrest.

Because of the impending end of his contract, Scott undergoes a "de-Santafication process" which gradually turns him back into Scott Calvin. As Scott, he has a limited amount of magic to help him perform miracles; if he uses up all his magic, then he would no longer be able to return to the North Pole. He attempts to reconcile with Charlie, who keeps vandalizing his school to get attention. They both hit the cold hard wall of Principal Newman when Charlie defaces the lockers.

Charlie confesses to Scott how hard it is for him that Scott is never around like other fathers. Also, he reveals the stress he is under to conceal the secret that his father is Santa. He claims that when other kids get to talk about their dads and their jobs, it hurts because Scott has the best job ever and nobody knows, and that nobody believes in Charlies father, because of him being Santa. Scott vows to try harder as a dad, and they reconcile.

After a few failed dates, Scott finds himself falling for Carol. He accompanies her in a sleigh via his magic to her school faculty Christmas party, which turns out to be dull and boring. Using a little more of his Christmas magic, he livens it up by presenting everyone with their childhood dream gifts (much like he did for Laura and Neil in the first film). He makes a special presentation to Carol, and, with his last remnant of magic, wins her over and they kiss passionately. At Carols home, Scott has difficulty explaining to her his life as Santa. However, when Scott explains his reason for dating her as Santa Claus, Carol balks, believing that he is mocking her childhood (where she was teased for believing in Santa), until Charlie later manages to convince her by showing her his magic snow globe.

Curtis flies in to deliver the dreadful news about the Evil Toy Santas coal binge and urges him to return to the North Pole to save Christmas. Unfortunately, Scott has used up the last of his magic wooing Carol, Comet has eaten too many chocolate bars (since Lucy introduced him to the sweets), and Curtis jetpack has been destroyed upon arrival. The only method of heading back to the North Pole is for Scott to lose a tooth for the Tooth Fairy; after two failed attempts, Lucy manages to lose one of her baby teeth. After convincing the Tooth Fairy that he is Santa Claus, the Tooth Fairy flies Scott and Curtis back to the North Pole but they are captured by the toy soldiers.

Toy Santa wastes no time in tying Scott up with Curtis, but Charlie and a now-believing Carol spring him free by summoning the Tooth Fairy to fly them there after Charlie, purposely, lost a tooth. Scott single-handedly goes after Toy Santa, who has already left with the sleigh filled with coal. Scott takes Chet, a young reindeer-in-training, and they both battle around the reindeer.

With an army of elves, Carol, Bernard, Charlie, and Curtis lead a snowball war (among other tactics) to overthrow the toy soldiers. During the battle, Toy Santa and Scott accidentally causes the sleigh to crash back into the village (as well as crashing on the rest of the toy soldiers). Toy Santa is forcibly restrained by the elves and is reduced to his normal six-inch height.

Carol reconciles with Scott. Scott marries Carol in a ceremony presided over by Mother Nature herself. Upon the newlywed couple kissing, Scott transforms back into Santa, and Christmas proceeds as it always has. He tells Carol they can go on a three-month honeymoon of her choice, anywhere but tropical because Santa will not look good in swimwear; she tells him to not return home late.

In addition, Scott allows permission to Charlie to reveal the truth to Lucy that he is Santa Claus. This occurs when Scott stops at Laura and Neils house. Santa/Scott then heads off to deliver the rest of the toys to the world (with an overweight Comet in the back of the sleigh and Chet taking his place).

In a post-credits scene, Toy Santa is up for sale and seen joyfully dancing on a store shelf with other dancing toy Santas (all are inanimate) who look just like him; he unsuccessfully tries to convince them to dance more elaborate like him.

==Cast==
* Tim Allen as 46 year old Santa Claus/Scott Calvin
* Eric Lloyd as Charlie Calvin
* Elizabeth Mitchell as Principal Carol Newman
* Wendy Crewson as Laura Miller, Scotts ex-wife. She is Lucy and Charlies mother.
* Judge Reinhold as Dr. Neil Miller, Lauras second husband and Lucys father. 
* Liliana Mumy as Lucy Miller, Charlies half-sister. 
* David Krumholtz as Bernard the Arch-elf. 
* Spencer Breslin as Curtis, the teenage elf Keeper of the Handbook of Christmas.
* Danielle Woodman as Abby the Elf
* Aisha Tyler as Mother Nature
* Peter Boyle as Father Time; Boyle was also in the first film playing Scotts boss.
* Jay Thomas as the Easter Bunny
* Kevin Pollak as Cupid
* Art LaFleur as the Tooth Fairy (the Molinator/ reference to The Terminator)
* Michael Dorn as the Sandman

* Bob Bergen as Comet (voice)
* Kath Soucie as Chet (voice)

==Soundtrack==
Original Release Date: November 1, 2002
* 1. Everybody Loves Christmas - Eddie Money
* 2. Santa Claus Lane - Hilary Duff
* 3. Santas Got A Brand New Bag - SHeDAISY
* 4. Jingle Bells - Brian Setzer
* 5. Run Rudolph Run - Chuck Berry
* 6. Zat You, Santa Claus? - Louis Armstrong
* 7. Santa Claus Is Coming to Town - Smokey Robinson & the Miracles
* 8. Blue Holiday - The Shirelles
* 9. Unwritten Christmas - Unwritten Law & Sum 41
* 10. Im Gonna Lasso Santa Claus - Brenda Lee
* 11. Santa Claus Is Coming to Town - Steve Tyrell Elmo & Patsy

==Critical reception==
The Santa Clause 2 received mixed reviews from critics, garnering a 54% "rotten" critical approval rating from Rotten Tomatoes, as opposed to the 75% "certified fresh" rating of the first film. The sites consensus is that "Though its harmless as family entertainment and has moments of charm, The Santa Clause 2 is also predictable and forgettable."
 

==Home media==
The film was released on DVD and VHS on November 18, 2003. It was re-released as 3-Movie Collection DVD set in 2007 and first released as a 3-Movie Collection Blu-ray set on August 29, 2013.

==References==
 

==External links==
 
*  
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 