Goodbye, New York
{{Infobox film
| name           = Goodbye, New York
| image          = Goodbye New York Poster.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Theatrical poster
| director       = Amos Kollek
| producer       = Amos Kollek
| writer         = Amos Kollek
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Julie Hagerty Amos Kollek Shmuel Shilo Aviva Ger Dudu Topaz
| music          = Michael Abene
| cinematography = Amnon Salomon
| editing        = Alan Heim
| studio         = Kole-Hill Productionsd
| distributor    = Castle Hill Productions (US)
| released       =  
| runtime        = 90 minutes
| country        = Israel US
| language       = English
| budget         = 
| gross          = 
}} 1985 Israeli-American comedy-drama produced, directed and written by Amos Kollek, who also co-stars in his directorial debut.

==Plot==
A ditzy New Yorker (Julie Hagerty) is devastated to learn that her husband has been unfaithful and impulsively decides to go to Paris to escape. When she consumes too many sedatives and oversleeps on the plane, missing her connection, she winds up in Tel Aviv, penniless and with no luggage or friends.  After connecting with a cabdriver and part-time soldier (Amos Kollek), she finds herself stranded on a kibbutz near the Golan Heights where she must learn to cope with a series of misadventures and a very unfamiliar lifestyle.

==Cast==
*Julie Hagerty as Nancy Callaghan
*Amos Kollek as David
*Shmuel Shilo as Moishe
*Aviva Ger as Illana
*Dudu Topaz as Albert
*Jennifer Prichard as Lisa
*Christopher Goutman as Jack
*Hanan Goldblatt as Avi
*Mosko Alkalai as Papalovski
*Joseph Kaplanian as himself (Elderly Man turning round in close up shot by Church of the Holy Sepulchre)

==Critical reception== Time Out was more unequivocal, dismissing the "thin and clichéd material", its "predictable plot and dismal propaganda about the values of kibbutz culture." 

==References==
 

==External links==
* 

 
 
 