Oru Cheru Punchiri
{{Infobox film
| name           = Oru Cheru Punchiri
| image          = OruCheruPunchiri.jpg
| caption        =
| alt            =
| director       = M. T. Vasudevan Nair
| producer       = Jisha John
| writer         = Sriramana (Story)  M. T. Vasudevan Nair (Screenplay)
| starring       = Oduvil Unnikrishnan  Nirmala Sreenivasan Johnson
| cinematography = Sunny Joseph
| editing        = Beena Paul
| studio         = Chithranjali Studio, Thiruvananthapuram
| distributor    =
| released       = September 2000
| runtime        = 89 mins
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
 Telugu writer Sriramanas short story Mithunam from the novel of the same title.

==Plot==
Oru Cheru Punchiri tells the story of a retired estate manager Krishna Kuruppu (Oduvil Unnikrishnan) in his mid seventies and his wife Ammalukutty (Nirmala Sreenivasan) in her mid sixties continuing their married life in a honeymoon mood. This couple wake up to  romantic mornings dense with sweet herbal aroma with the melodious music of birds as the background. They spend their time engaged in games of mischief and even some social activities that they could manage. They make it clear that they would never surrender to the plea of their children coated in love, to sell the ancestral property in the village and move to the city with them. They are happy doing agriculture in the land they own. They have some good neighbours in Janu (Roslyn), her daughter Malathi (sindhu) and helper-boy Kannan (Master Vignesh). Krishna Kuruppu was instrumental in Janu getting a sweepers job in the Urban bank there. He also sponsors for Kannans education. Krishna Kuruppu helps Bhaskaran (Jayakrishnan) get a job in the estate he worked. He also arranges the marriage of Bhaskaran and Malathi. He also supports the love affair of his granddaughter Beena (Lena Abhilash|Lena) with a Muslim youth.

The movie ends with Ammalukuttys decision to continue with this celebration of life even after the death of Krishna Kuruppu.

==Cast==
* Oduvil Unnikrishnan as Krishna Kuruppu
* Nirmala Sreenivasan as Ammalukutty
* Jayakrishnan as Bhaskaran
* Sindhu as Nirmala
* P. K. Venukuttan Nair as Govindettan
* Aliyar as Postman Ramankutty
* Master Vignesh as Kannan
* Thampi Kannanthanam as Ravi
* Sreedevi Unni as Radha
* Paul as Kesu
* Manka Mahesh as Subhadra Lena as Beena
* Mukundan as Jayan
* Roslyn as Janu
* Vijayan Peringode as Paulose
* Sarada as Chinnamani

==Filming==
The film was shot on the banks of the Periyar River at Parappuran, Puthiyedam, Chowwara, Sreemoolanagaram, and Aluva in Kochi, India.

==Awards and recognitions== FIPRESCI awards in 2000. Indian National Best Film on Environment Conservation/Preservation in 2001.
* The Kerala State Film Award for the best director of 2000.
* Screened at the third Mumbai International Film Festival in November 2000.
* Screened at the International Film Festival of Kerala in 2001.
* Selected for the Munich International Film Festival. Asianet Satellite television channel during Onam 2000.

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 