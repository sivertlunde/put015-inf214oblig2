Fuchsia (film)
{{multiple issues|
 
 
}}

{{Infobox film
| name           = Fuchsia
| image          =
| director       = Joel Lamangan
| producer       = Antonio P. Tuviera Ricardo Lee Gloria Romero Eddie Garcia Robert Arevalo
| music          = Von de Guzman
| cinematography = Carlos S. Montano Ricardo Lee
| editing        = Marya Ignacio
| distributor    = APT Entertainment
| released       =  
| runtime        =
| country        = Philippines
| language       =  
| budget         =
| gross          =
}}
 Gloria Romero, Eddie Garcia, and Robert Arevalo.

==Cast== Gloria Romero as Mameng
*Eddie Garcia as Marcelino "Mars"
*Robert Arevalo as Generoso "Gener"
*Armida Siguion-Reyna as Juana
*Celia Rodriguez as Priscilla
*Gina Alajar as Nena
*Iza Calzado as Elizabeth
*Tony Mabesa as Mayor Sunga
*Raquel Villavicencio as Mrs. Sunga
*Richard Quan as Milo
*Jim Pebanco as Poloy
*Nanding Josef as Kandong
*Allan Noble as Mayor Sungas Men
*John Lapid as Mayor Sungas Men
*Ed Tambunting as Mayor Sungas Men
*Dante Javier as Chief of Police
*Nido De Jesus as Policemen
*Noel Elmido as Policemen
*Jojo Lopez as Policemen
*Vic Romano as Policemen
*Josie Tagle as Susan
*Josie Del Rosario as Mamengs relative
*Marivic Mejay as Mamengs relative
*Lourdes Serrano as Mamengs relative
*Cecil Magcamit as Mamengs maid

 


 