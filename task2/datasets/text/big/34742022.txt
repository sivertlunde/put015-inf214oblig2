Ablakon
{{Infobox film
| name           = Ablakon
| director       = Roger Gnoan MBala
| producer       = Films du Koundoun
| writer         = Roger Gnoan MBala
| starring       = Kodjo Eboucle Issa Sanogo Mathieu Attawa Joel Okou Bitty Moro Adjei Zoubly
| music          = Les Mystics
| sound editor   = Johnson Yao
| photography    = Paul Kodjo
| editing        = Ahoussy Djangoye 
| distributor    = Marfilmes
| released       =  
| runtime        = 81 minutes
| country        = Ivory Coast
| language       = French
}}
 1984 drama film directed by Roger Gnoan MBala.

==Festivals==
* Milan Film Festival (1997)
* Venice Film Festival (1985)

==Awards==
* Price for best actor FESPACO -Panafrican Film and Television Festival of Ouagadougou, Burkina Faso (1985)

==See also==
*  

==External links==
*   - IMDb page about Ablakon
*  

 
 

 