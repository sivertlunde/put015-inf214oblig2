Smokescreen (film)
{{Infobox film
| name           = Smokescreen
| image          = "Smokescreen"_(1964).jpg
| image_size     =
| caption        = Lobby card autographed by Deryck Guyler
| director       = Jim OConnolly
| producer       = Ronald Liles John I. Phillips
| writer         = Jim OConnolly
| narrator       = John Carson Yvonne Romain
| music          = Johnny Gregory
| cinematography = Jack Mills
| editing        = Henry Richardson
| studio         =
| distributor    = Butchers Film Service
| released       = 1964
| runtime        = 70 minutes
| country        = United Kingdom
| language       = English
}}
 1964 British crime thriller, written and directed by Jim OConnolly and starring Peter Vaughan. 

==Plot==
Mr. Roper, a senior insurance investigator, travels to Brighton to assess the mysterious death of a businessman, killed when his car crashes over a cliff. The insurance company are suspicious, as the man was insured for a large amount, but no body has been found.

==Cast==
* Peter Vaughan as Roper John Carson as Trevor Bayliss
* Yvonne Romain as Janet Dexter
* Gerald Flood as Graham Turner
* Glynn Edwards as Inspector Wright
* John Glyn-Jones as Player
* Sam Kydd as Hotel waiter
* Deryck Guyler as Station master
* Penny Morrell as Helen - Turners secretary
* David Gregory as the Smudger
* Jill Curzon as June
* Barbara Hicks as Miss Breen
* Bert Palmer as Barman Tom Gill as Reception clerk
* Edward Ogden as Police Sergeant
* Anthony Dawes as John Dexter
* Romo Gorrara as Taxi Driver
* Derek Francis as Dexters doctor
* Damaris Hayman as Mrs. Ropers nurse

==Critical reception==
The Radio Times wrote, "this above-average programme filler has a passable plot (involving a little bit of skulduggery in suburban Brighton) thats kept moving swiftly and painlessly by director Jim OConnolly...Vaughan plays with a dogged determination that is efficient, engaging and quite at odds with the more sinister characterisations he would essay later in his career";  while BFI Screenonline wrote, "an utterly charming B-film comedy-thriller that emphasises character as much as plot and makes full use of extensive location footage."  

==External links==
*  
* http://www.screenonline.org.uk/film/id/1361609/synopsis.html

==References==
 

 

 
 
 
 
 
 


 
 