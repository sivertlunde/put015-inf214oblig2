Yeh Dillagi
 
 
{{Infobox film
| name = Yeh Dillagi
| image = Yeh_Dillagi.jpg
| caption = Movie Poster
| writer = Sachin Bhowmik
| starring = Akshay Kumar Saif Ali Khan Kajol
| director = Naresh Malhotra
| producer = Yash Chopra
| distributor = Yash Raj Films
| released = 6 May 1994
| runtime = 154 min.
| language = Hindi
| country = India
| music = Dilip Sen Sameer Sen
| budget =  
| gross =   
}}
 romantic film, produced by Yash Chopra and directed by Naresh Malhotra under the banner of Yash Raj Films. It stars Akshay Kumar, Kajol and Saif Ali Khan. Karisma Kapoor has a special appearance. It was the sixth highest grossing film of 1994 and was declared "Super Hit". Akshay Kumar was nominated as Best Actor and Kajol for Best Actress at the Filmfare Awards. It is a remake of the 1954 American romantic comedy Sabrina (1954 film)|Sabrina, starring Audrey Hepburn and William Holden. This was the first of several films in which Akshay Kumar and Saif Ali Khan acted together.

== Synopsis ==

Sapna (Kajol), the daughter of the Saigal familys driver, is a simple fun-loving girl, but she dreams about riches.

Vijay (Akshay Kumar) and Vicky Saigal (Saif Ali Khan) are both heirs to Saigal Industries. Vijay spends all his time working and Vicky is a flirt. Vicky notices Sapna when she becomes a successful model. But Sapnas success does not change that she is the drivers daughter. Mrs. Shanti Devi may overlook her sons affairs with rich girls but not with a drivers daughter. As Vijay tries to help, he finds himself falling in love with Sapna, too. Before its too late Sapna falls in love with him, too, and the pair decide to get married.

On Vickys birthday, Vijay comes back with Sapna and sees his brother has changed. The man who used to be a flirt now doesnt drink liquor, smoke cigarettes, or flirt with girls. The only one he sees is Sapna. The boys talk: both want to marry Sapna. Vicky mistakenly thinks that his brother supports his pursuit of Sapna.

One afternoon, Vicky goes out for lunch with Vijay and Sapna. In the middle of their journey, they have a flat tire, and Vijay tries to fix it. Instead, the jackbox breaks and Vicky offers to get it from a garage. Sapna realizes that Vicky wants to marry her at that point and starts to cry, so Vijay tries to calm her down by giving her a hug. Just then, Vicky arrives and realises that there is a connection between his brother and Sapna. He returns home drunk. Their mother has had enough and tells Sapnas father to tell her to go back to Bombay or get fired.

Sapna storms off with her dad to the train station. Vicky threatens to commit suicide if his mother does not accept Sapna as her daughter-in-law; his mother accepts. Sapna returns and Vicky sacrifices his love for his brother. Vicky is seen driving and comes across a girl (Karisma Kapoor). He instantly falls for her.

==Cast==
* Akshay Kumar: Vijay Saigal
* Saif Ali Khan: Vicky Saigal
* Kajol: Sapna Bannerjee alias Sajna
* Reema Lagoo: Shanti Saigal
* Saeed Jaffrey: Bhanupratap Saigal
* Deven Verma: Gurdas Bannerjee
* Karisma Kapoor: Special appearance
* Pankaj Udhas: Special appearance as himself in song "main deewana hoon"

==Music== Abhijeet was a hit.

{| class="wikitable"
|-
! Song
! Singer(s)
! Picturisation
|-
|"Dekho Zara Dekho" Lata Mangeshkar &  Kumar Sanu Akshay Kumar &  Kajol
|-
|"Gori Kalai" Lata Mangeshkar &  Udit Narayan Akshay Kumar &  Kajol
|-
|"Honton Pe Bas" Lata Mangeshkar &  Kumar Sanu Saif Ali Khan &  Kajol
|-
|"Lagi Lagi Dil Ki" Lata Mangeshkar, Abhijeet
|Akshay Kumar, Saif Ali Khan & Kajol
|-
|"Main Deewana Hoon" Pankaj Udhas Pankaj Udhas & Saif Ali Khan
|-
|"Naam Kya Hai" Lata Mangeshkar &  Kumar Sanu Saif Ali Khan &  Kajol
|-
|"Ole Ole" Abhijeet
|Saif Ali Khan
|}

==Awards==
Nominations Best Actor – Akshay Kumar Best Actress - Kajol Best Music Director – Dilip Sen-Sameer Sen Best Male Singer – Abhijeet for the song "Ole Ole"

==References==
 

==External links==
* 
 

 
 
 
 
 