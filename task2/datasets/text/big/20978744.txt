Ullasa Utsaha
{{Infobox film
| name = Ullasa Uthsaha
| image = Uallasa_Uthsaha.jpg
| caption = The Rain of Joy
| director = Devaraja Palan
| writer = A. Karunakaran
| producer = B.P. Thyagaraju
| co-producer = vinay aryan Ganesh Yami Gautam
| music = G. V. Prakash Kumar
| language = Kannada
| lyrics =
| cinematography = Seetharam
| country = India
| released = 2009
| website =
}}
 Ganesh and Yami Gautam. A remake of the Telugu film Ullasamga Utsahamga, it was produced by B.P. Thyagaraju and co-produced by vinay aryan and had the original music by G. V. Prakash Kumar. 

==Cast== Ganesh
* Yami Gautam
* Rangayana Raghu
* Sadhu Kokila
* Tulasi Shivamani
* Preethi Chandrashekar
* Doddanna
* Vithika Sheru

==Plot==
The protagonist (Ganesh) is an irresponsible young son of a garage owner (Rangayana Raghu). He falls in love with a girl (Yami Gautam) who stays in his locality and does everything possible to woo her. She is not in the least interested with him, rather thinks of him as a nuisance. She is in love with an old childhood friend she hasnt met since a long time, and has run away from a marriage arranged by her step mother to get her money. Many fights with the hero later, she eventually trusts him to take her to meet her childhood friend.

==Making==
 
Ullasa Utsaha is the remake of the recent Telugu hit, Ullasanga Utsahanga. The hero Yasho Sagar, a Kannadiga by birth recommended Ganesh to be the hero of the remake. Ganesh felt it was a tailor made role for him and agreed to act. The film later ran into some trouble when papers reported that its heroine Shraddha Arya dropped out of the film three days into the first schedule due to personal reasons. However, the producers and Ganesh maintained that she was never involved with the film. For a short while the film progressed without a heroine. A few weeks later Yami - a television artist from Chandigarh - was cast. GV Prakash who scored the original film will also be scoring the remake.

==Box Office==

The film was an average grosser at box office by completing 25 days.  

==Soundtrack==
{{Infobox album    Name        = Ullasa Utsaha Type        = film Artist      = G. V. Prakash Kumar Cover =      UU_audio.jpg Released    =  16 August 2009 (CD release) Recorded    = Genre       = Film score Length      = 33:07 Label       =   ADITYA Music Producer    =	G. V. Prakash Kumar Reviews     = Last album  = Seval (2008) This album  = Ullasa Utsaha (2009) Next album  = Angaadi Theru (2009)
}}

The official soundtrack contains seven songs, out of which five were re-used from the original Telugu version, composed by G. V. Prakash Kumar. The audio of the film was released on 16 August 2009.

{{Track listing
| collapsed       =
| headline        =
| extra_column    = Singers
| total_length    = 33:07
| all_writing     =
| all_lyrics      =
| all_music       =
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Chalisuva Cheluve
| note1           =
| writer1         =
| lyrics1         = Jayanth Kaikini
| music1          =
| extra1          = Sonu Nigam
| length1         = 5:37
| title2          = Hello Namaste
| note2           =
| writer2         =
| lyrics2         = Kaviraj
| music2          = Karthik
| length2         = 4:31
| title3          = Love Maade Nanne
| note3           =
| writer3         =
| lyrics3         = Ram Narayan
| music3          =
| extra3          = Naresh Iyer
| length3         = 4:59
| title4          = Chakkori Chakkori
| note4           =
| writer4         =
| lyrics4         = Kaviraj
| music4          =
| extra4          = Tippu (singer)|Tippu, Rita, Benny Dayal
| length4         = 4:48
| title5          = Kanasinolage Minchondu
| note5           =
| writer5         =
| lyrics5         = Ram Narayan
| music5          =
| extra5          = G. V. Prakash Kumar, Andrea Jeremiah
| length5         = 5:34
| title6          = Laali Haada Laavani
| note6           =
| writer6         =
| lyrics6         = Ram Narayan
| music6          = Karthik
| length6         = 3:11
| title7          = O Prema Yaakadaru Innu
| note7           =
| writer7         =
| lyrics7         = Ram Narayan
| music7          = Karthik
| length7         = 4:27
}}

==References==
 

==External links==
*  

 
 
 
 
 