Ask Me Anything (film)
 
 
{{Infobox film
| name = Ask Me Anything
|image=Ask Me Anything (Film) dvd.jpeg caption = DVD cover border = yes
| director = Allison Burnett
| producer = {{Plainlist|
* Lauren Avinoam
* Nicholas Emiliani
* Lauren Hogarth}}
| writer = Allison Burnett based on his novel Undiscovered Gyrl
| starring = {{Plainlist|
* Britt Robertson
* Justin Long
* Martin Sheen
* Christian Slater
* Robert Patrick
* Max Carver}}
| music = Jon Ehrlich
| cinematography = Patrice Lucien Cochet
| editing = Adam Lichtenstein
| studio = {{Plainlist |
* Decipher Entertainment
* Presque Isle Films
* Tait Productions}}
| distributor = Phase 4 Films
| released =  
| runtime = 100 minutes
| country = United States
| language = English
| budget = $950,000
}}
Ask Me Anything is a 2014 American drama written and directed by Allison Burnett,   based on his novel Undiscovered Gyrl.  The film stars Britt Robertson, Justin Long, Martin Sheen, Christian Slater, Robert Patrick, and Max Carver.   

Ask Me Anything had its world premiere at the Nashville Film Festival, before it was released on December 19, 2014, by Phase 4 Films theatrically and via video on demand and other online platforms.  The film was released on DVD March 3, 2015.

==Plot==
Katie Kampenfelt, a beautiful, funny, free-spirited, lost soul decides to take a year off before attending college. She chronicles her romantic and sexual adventures in an anonymous blog, but as her fearless narrative eventually gives way, dark secrets from her past begin to emerge.

==Cast==
 
* Britt Robertson  as Katie Kampenfelt
** Sophia Tabor as Young Katie Kampenfelt
* Justin Long  as Dan Gallo
* Martin Sheen  as Glenn Warburg
* Christian Slater  as Paul Spooner
* Robert Patrick  as Doug Kampenfelt
* Kimberly Williams-Paisley  as Margaret Spooner
* Max Carver  as Rory
* Andy Buckley   as Mark Aubichon
* Molly Hagan as Caroline Kampenfelt
* Gia Mantegna as Jade
* Max Hoffman as Joel Seidler
* Zuleikha Robinson as Affie
* Beatrice Rosen as Martine
* Lorraine Toussaint as Dr. Sherman
* Cathryn de Prume as Carol
* Kim Estes as Detective Owens
 

==Reception==
Gary Goldstein, of the Los Angeles Times, said: "The edgy coming-of-age tale Ask Me Anything begins with a snarky, bubble-gum vibe that gives way to something far deeper and meaningful.”  Adding that it ”masterfully immerses us into the life of a floundering teenager with often-stirring, unexpected results.”  As for the film’s surprising, unique ending, he added "Its all beautifully paid off in the movies haunting coda." 

Ask Me Anything premiered at the Nashville Film Festival where it received a Best Actress award for Britt Robertson as well as the award for Best Music in a Feature Film. 

==Home media==
The film was released to video on demand as well as other online platforms on December 19, 2014. The DVD was released March 3, 2015.   

==Soundtrack==
Director Allison Burnett held a contest asking for undiscovered female singers, age 21 and under, to submit songs for the soundtrack.  Approximately one hundred songs were submitted, with fourteen showing up on the official soundtrack released to iTunes and other online distributors. 

==References==
 