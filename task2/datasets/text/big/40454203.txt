Buffalo Rider
{{Infobox film
| name           = Buffalo Rider
| image          = Buffalo Rider poster.jpg
| director       = John Fabian George Lauris Dick Robinson
| writer         = Mollie Gregory
| starring       = Rick Guinn
| narrator       = C. Lindsay Workman
| producer       = Dick Robinson
| cinematography = Bill Farmer George Griner Greg McKay
| editing        = John Fabian
| music          = Al Capps
| distributor    = 
| released       =  
| language       = English
| country        = United States
}}
Buffalo Rider is a 1978 action film directed by John Fabian, George Lauris, and Dick Robinson.

==Plot==
Buffalo Rider is the dramatization of the life of Buffalo Jones, with Rick Guinn playing the role of Jones.

==Reception==
The film has received negative reviews. Chris Higgins of Mental floss wrote that "given all the animal stunts (including what sure looks like actually shooting buffalo and various cross-species animal fights) it wouldnt pass muster today. The movie itself is pretty crappy, featuring an extreme over-reliance on narration and a sort of meandering documentary-ish treatment with some buffalo-related dramatic elements tossed in."  The film was featured in a Gizmodo article of "the weirdest thing on the internet tonight" where Andrew Tarantola wrote, "Enjoy the heartwarming tale of a man, conveniently named Buffalo Jones, and his buffallo, named Buffalo. No wait. Its name is Samson, because thats so much more original. Whatever you call them, the two chum around the American frontier, saving babies and stuff for an hour and a half (even though the script was apparently only about 15 pages long)." 
 Kevin Murphy, and Bill Corbett. 

==In popular culture==

In 2011, the Austin-based band The Possum Posse created "Guy on a Buffalo", a narrated song set to clips from the movie Buffalo Rider. 

==References==
 

==External links==
* 

 
 
 
 


 