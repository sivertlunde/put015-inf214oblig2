Never Say Never Again
 
 

 
{{Infobox film
| name = Never Say Never Again
| image = Never Say Never Again – UK cinema poster.jpg
| caption = British cinema poster for Never Say Never Again, illustrated by Renato Casaro
| alt=A poster at the top of which are the words "SEAN CONNERY as JAMES BOND in". Below this is a head and shoulders image of man in a dinner suit. Inset either side of him, are smaller scale depictions of two women, one blonde and one brunette. Underneath the picture are the words "NEVER SAY NEVER AGAIN"
| starring = {{Plainlist|
* Sean Connery
* Klaus Maria Brandauer
* Max von Sydow
* Barbara Carrera
* Kim Basinger
* Bernie Casey
* Alec McCowen Edward Fox
}}
| screenplay =   Ian La Frenais
| story = Kevin McClory Jack Whittingham Ian Fleming
| based on = Thunderball (novel)|Thunderball by Ian Fleming
| director = Irvin Kershner
| producer = Jack Schwartzman
| cinematography = Douglas Slocombe
| music = Michel Legrand
| editing = Ian Crafford
| studio = Taliafilm Producers Sales Organization
| distributor = Warner Bros.
| released =  
| runtime= 134 minutes
| country = United Kingdom United States
| language = English
| budget = $36 million
| gross = $160 million
}} under that the majority long legal battle dating from the 1960s.
 James Bond, Diamonds Are Forever. The films title is a reference to Connerys reported declaration in 1971 that he would  "never again" play that role. As Connery was 52 at the time of filming, the storyline features an ageing Bond, who is brought back into action to investigate the theft of two nuclear weapons by SPECTRE. Filming locations included France, Spain, the Bahamas and Elstree Studios in England.

Never Say Never Again was released by Warner Bros. in the autumn of 1983. It opened to positive critical reviews and was a commercial success, grossing $160 million at the box office, although this was less overall than the Eon-produced Bond film released in June of the same year, Octopussy. In 1997 the distribution rights of Never Say Never Again were purchased by Metro-Goldwyn-Mayer, which distributes Eons Bond films, and the company has handled subsequent home video releases of the film.

==Plot== sadomasochistic beating scans his eye. Bond is seen by Blush and an attempt is subsequently made to kill him in the clinic gym, but Bond manages to defeat the assassin.
 iris recognition American military warheads in nuclear warheads; SPECTRE then obtains the warheads to extort billions of dollars from NATO governments. Blush subsequently murders Petachi.
 Prime Minister, M reluctantly reactivates the 00 agent|double-0 section and Bond is assigned the task of tracking down the missing weapons. He meets Domino Petachi, the pilots sister, and her wealthy lover, Maximillian Largo, a SPECTRE agent. Bond follows Largo and his yacht to the Bahamas, where he spars with Blush and Largo.
 CIA counterpart, Felix Leiter. Bond goes to a beauty salon where he poses as an employee and, whilst giving Domino a massage, is informed by her that Largo is hosting an event at a casino that evening. At the charity event, Largo and Bond play a 3-D video game called Domination, the loser having to take an electric shock of higher intensity or pay a corresponding cash bet, which Bond ultimately wins; Bond then informs Domino of her brothers death. Bond returns to his villa to find Nicole, his French contact, dead, having been killed by Blush. After a vehicle chase on his motorbike, Blush captures Bond. Forced to write his memoirs putting her as his "Number One" sexual partner, Bond uses his Q-branch-issue fountain pen to shoot Blush.
 Flying Saucer, in search of the missing nuclear warheads. Bond becomes trapped and is taken, with Domino, to Palmyra, Largos base of operations in North Africa. Largo punishes Domino for betraying him by auctioning her off to some passing Arabs. Bond subsequently escapes and rescues Domino.
 US Navy submarine and track Largo to a location known as the Tears of Allah, below a desert oasis. Bond and Leiter infiltrate the underground facility and a gun battle erupts between Felixs team and Largos men in the temple. In the confusion Largo makes a getaway with one of the warheads. Bond catches and fights Largo underwater. Just as Largo tries to detonate the last bomb, he is killed by Domino, taking revenge for her brothers death. Bond then returns to the Bahamas with Domino.

==Cast== James Bond, MI6 agent 007. Domino Petachi, sister of Jack Petachi and mistress of Maximillian Largo.
* Klaus Maria Brandauer as Maximillian Largo; based on the character Emilio Largo, a senior member of SPECTRE.
* Barbara Carrera as Fatima Blush; based on Fiona Volpe and a member of SPECTRE.
* Bernie Casey as Felix Leiter, Bonds CIA contact and friend.
* Max von Sydow as Ernst Stavro Blofeld, the head of SPECTRE. Edward Fox as M (James Bond)|M, Bonds superior at MI6.
* Rowan Atkinson as Nigel Small-Fawcett, Foreign Office representative in the Bahamas.
* Gavan OHerlihy as Jack Petachi, a pilot used by SPECTRE to steal the nuclear missiles. He is Domino Petachis brother. Q (also known as Algy)
* Pamela Salem as Miss Moneypenny, Ms secretary
* Saskia Cohen Tanugi as Nicole, Bonds MI6 contact in France
* Prunella Gee as Patricia Fearing, a physiotherapist at the clinic
* Valerie Leon as Lady in Bahamas
* John Stephen Hill as Communications Officer
* Milow Kirek as Kovacs
* Pat Roach as Lippe
* Anthony Sharp as Lord Ambrose

==Production== controversy over High Court in London for breach of copyright  and the matter was settled in 1963.    After Eon Productions started producing the Bond films, they subsequently made a deal with McClory, who would produce Thunderball, and then not make any further version of the novel for a period of ten years following the release of the Eon-produced version in 1965. 

In the mid-1970s McClory again started working on a project to bring a Thunderball adaptation to production and, with the working title Warhead, he brought writer Len Deighton together with Sean Connery to work on a script.  The script ran into difficulties after accusations from Eon Productions that the project had gone beyond copyright restrictions, which confined McClory to a film based on the Thunderball novel only, and once again the project was deferred.  

Towards the end of the 1970s developments were reported on the project under the name James Bond of the Secret Service,  but when producer Jack Schwartzman became involved and cleared a number of the legal issues that still surrounded the project  he brought on board scriptwriter Lorenzo Semple Jr  to work on the screenplay. Connery was unhappy with some aspects of the work and asked Tom Mankiewicz who had rewritten Diamonds are Forever to work on the script; however Mankiewicz declined as he felt he was under a moral obligation to Cubby Broccoli.  Connery then hired British television writers Dick Clement and Ian La Frenais  to undertake re-writes, although they went uncredited for their efforts because of a restriction by the Writers Guild of America. 

The film underwent one final change in title: after Connery had finished filming Diamonds Are Forever he had pledged that he would "never" play Bond again.  Connerys wife, Micheline, suggested the title Never Say Never Again, referring to her husbands vow  and the producers acknowledged her contribution by listing on the end credits "Title "Never Say Never Again" by: Micheline Connery". A final attempt by Flemings trustees to block the film was made in the High Courts in London in the spring of 1983, but these were thrown out by the court and Never Say Never Again was permitted to proceed. 

===Cast and crew===
When producer Kevin McClory had first planned the film in 1964 he held initial talks with Richard Burton for the part of Bond,  although the project came to nothing because of the legal issues involved. When the Warhead project was launched in the late 1970s, a number of actors were mentioned in the trade press, including Orson Welles for the part of Blofeld, Trevor Howard to play M and Richard Attenborough as director. 

In 1978 the working title James Bond of the Secret Service was being used and Connery was in the frame once again, potentially going head-to-head with the next Eon Bond film, Moonraker (film)|Moonraker.  By 1980, with legal issues again causing the project to founder,  Connery thought himself unlikely to play the role, as he stated in an interview in the Sunday Express "when I first worked on the script with Len I had no thought of actually being in the film".    When producer Jack Schwartzman became involved, he asked Connery to play Bond: Connery agreed, asking (and getting) a fee of $3 million, ($ }} million in   dollars ) a percentage of the profits, as well as casting and script approval.  Subsequent to Connery reprising the role, the script has several references to Bonds advancing years – playing on Connery being 52 at the time of filming  – and academic Jeremy Black has pointed out that there are other aspects of age and disillusionment in the film, such as the Shrublands porter referring to Bonds car ("they dont make them like that any more"), the new M having no use for the 00 section and Q with his reduced budgets. 
 Best Supporting Actress,  which she lost to Cher for her role in Silkwood. 
Micheline Connery, Seans wife, had met up-and-coming actress Kim Basinger at a hotel in London and suggested her to Connery, which he agreed upon.  For the role of Felix Leiter, Connery spoke with Bernie Casey, saying that as the Leiter role was never remembered by audiences, using a black Leiter might make him more memorable.  Others cast included comedian Rowan Atkinson, who would later parody Bond in his role of Johnny English. 
 director of Stephen Grimes.  

===Filming=== gun barrel sequence.| alt=The outlines of row upon row of "007 007 007 007 007" fill the screen. A view of countryside, heavily obstructed can be seen in through the gaps.]]
 
Filming for Never Say Never Again began on 27 September 1982 on the French Riviera for two months  before moving to Nassau, the Bahamas in mid-November  where filming took place at Clifton Pier, which was also one of the locations used in Thunderball.  The Spanish city of Almería was also used as a location.  Largos Palmyran fortress was actually historic Fort Carré in Antibes.  For Largos ship, the Flying Saucer, the yacht Nabila, owned by Saudi billionaire, Adnan Khashoggi, was used. The boat, now owned by Prince Al-Waleed bin Talal, has subsequently been renamed the Kingdom 5KR.  Principal photography finished at Elstree Studios where interior shots were filmed.  Elstree also housed the Tears of Allah underwater cavern, which took three months to construct.  Most of the filming was completed in the spring of 1983, although there was some additional shooting during the summer of 1983. 

Production on the film was troubled  with Connery taking on many of the production duties with assistant director David Tomblin.  Director Irvin Kershner was critical of producer Jack Schwartzman, saying that whilst he was a good businessman "he didnt have the experience of a film producer".  After the production ran out of money, Schwartzman had to fund further production out of his own pocket and later admitted he had underestimated the amount the film would cost to make. 

Many of the elements of the Eon-produced Bond films were not present in Never Say Never Again for legal reasons. These included the gun barrel sequence, where a screen full of 007 symbols appeared instead, and similarly there was no "James Bond Theme" to use, although no effort was made to supplement another tune.  Never Say Never Again did not use a pre-credits sequence, which was filmed but not used;  instead the film opens with the credits run over the top of the opening sequence of Bond on a training mission. 

===Music=== jazz pianist. Alan and Marilyn Bergman—who had also worked with Legrand in the Academy Award winning song "The Windmills of Your Mind"   —and was performed by Lani Hall  after Bonnie Tyler, who disliked the song, had reluctantly declined. 

Phyllis Hyman also recorded a potential theme song, written by Stephen Forsyth and Jim Ryan, but the song—an unsolicited submission—was passed over given Legrands contractual obligations with the music. 

==Release and reception==
Never Say Never Again premiered in New York on 7 October 1983,  grossing $9.72 million ($ }} million in   dollars ) on its first weekend,    which was reported to be "the best opening record of any James Bond film"  up to that point and surpassing Octopussy s $8.9 million ($ }} million in   dollars ) from June that year.  The film went on general release in the US in 1,500 cinemas on 14 October 1983  and had its UK premiere at the Warner West End cinema in Leicester Square on 14 December 1983.  Worldwide, Never Say Never Again grossed $160 million    in box office returns, which was a solid return on the budget of $36 million. 

Warner Bros. released Never Say Never Again on VHS and Betamax in 1984,  and on laserdisc in 1995.  After Metro-Goldwyn-Mayer purchased the distribution rights in 1997 (see #Legacy|Legacy, below), the company has released the film on both VHS and DVD in 2001,  and on Blu-ray in 2009. 

===Contemporary reviews===
Never Say Never Again was broadly welcomed and praised by the critics:   summed up Never Say Never Again saying "The actions good, the photography excellent, the sets decent; but the real clincher is the fact that Bond is once more played by a man with the right stuff." 
 From Russia with Love."  Malcolms main issue with the film was that he had a "feeling that a constant struggle was going on between a desire to make a huge box-office success and the effort to make character as important as stunts."  Malcolm summed up that "the mix remains obstinately the same-up to scratch but not surpassing it."  Writing in  The Observer, Philip French noted that "this curiously muted film ends up making no contribution of its own and inviting damaging comparisons with the original, hyper-confident Thunderball".    French concluded that "like an hour-glass full of damp sand, the picture moves with increasing slowness as it approaches a confused climax in the Persian Gulf." 

Writing for Newsweek, critic Jack Kroll thought the early part of the film was handled "with wit and style",    although he went on to say that the director was "hamstrung by Lorenzo Semples script".  Richard Schickel, writing in Time (magazine)|Time magazine praised the film and its cast. He wrote that Klaus Maria Brandauers character was "played with silky, neurotic charm",    whilst Barbara Carrera, playing Fatima Blush, "deftly parodies all the fatal femmes who have slithered through Bonds career".  Schickels highest praise was saved for the return of Connery, observing "it is good to see Connerys grave stylishness in this role again. It makes Bonds cynicism and opportunism seem the product of genuine worldliness (and world weariness) as opposed to Roger Moores mere twirpishness." 

Janet Maslin, writing in The New York Times, was broadly praising of the film, saying she thought that Never Say Never Again "has noticeably more humor and character than the Bond films usually provide. It has a marvelous villain in Largo."    Maslin also thought highly of Connery in the role, observing that "in Never Say Never Again, the formula is broadened to accommodate an older, seasoned man of much greater stature, and Mr. Connery expertly fills the bill."  Writing in The Washington Post, Gary Arnold was fulsome in his praise, saying that Never Say Never Again is "one of the best James Bond adventure thrillers ever made",    going on to say that "this picture is likely to remain a cherished, savory example of commercial filmmaking at its most astute and accomplished."  Arnold went further, saying that "Never Say Never Again is the best acted Bond picture ever made, because it clearly surpasses any predecessors in the area of inventive and clever character delineation". 

The critic for The Globe and Mail, Jay Scott, also praised the film, saying that Never Say Never Again "may be the only instalment of the long-running series that has been helmed by a first-rate director".    According to Scott, the director, with high quality support cast, resulted in the "classiest of all the Bonds".  Roger Ebert gave the film 3½ out of 4 stars, and wrote that Never Say Never Again, while consisting of a basic "Bond plot", was different from other Bond films: "For one thing, theres more of a human element in the movie, and it comes from Klaus Maria Brandauer, as Largo."    Ebert went on to add, "there was never a Beatles reunion&nbsp;... but here, by God, is Sean Connery as Sir James Bond. Good work, 007." 

===Reflective reviews===
Because Never Say Never Again is not an Eon-produced film, it has not been included in a number of subsequent reviews. Norman Wilner of   lists the film with a 60% rating from 40 reviews.  The score is still more positive than some of the Eon films, with Rotten Tomatoes ranking Never Say Never Again 16th among all Bond films in 2008.  Empire (magazine)|Empire gives the film three of a possible five stars, observing that "Connery was perhaps wise to call it quits the first time round".  IGN gave Never Say Never Again a score of 5 out of ten, claiming that the film "is more miss than hit".    The review also thought that the film was "marred with too many clunky exposition scenes and not enough moments of Bond being Bond". 

In 1995 Michael Sauter of Entertainment Weekly rated Never Say Never Again as the ninth best Bond film to that point, after seventeen films had been released. Sauter thought the film "is successful only as a portrait of an over-the-hill superhero."    He did admit, however that "even past his prime, Connery proves that nobody does it better".  James Berardinelli, in his review of  Never Say Never Again, thinks the re-writing of the Thunderball story has led to a film which has "a hokey, jokey feel,   is possibly the worst-written Bond script of all".    Berardinelli concludes that "its a major disappointment that, having lured back the original 007, the film makers couldnt offer him something better than this drawn-out, hackneyed story."  Critic Danny Peary wrote that "it was great to see Sean Connery return as James Bond after a dozen years".  He also thought the supporting cast was good, saying that Klaus Maria Brandauers Largo was "neurotic, vulnerable&nbsp;... one of the most complex of Bonds foes"  and that Barbara Carrera and Kim Basinger "make lasting impressions."  Peary also wrote that the "film is exotic, well acted, and stylishly directed&nbsp;... It would be one of the best Bond films if the finale werent disappointing. When will filmmakers realize that underwater fight scenes dont work because viewers usually cant tell the hero and villain apart and they know doubles are being used?" 

==Legacy== Casino Royale.  This move prompted a round of litigation from MGM, which was settled in an out-of-court settlement in which Sony gave up all claims on Bond, although McClory still claimed he would proceed with another Bond film,  and continued his case against MGM and Danjaq;    on 27 August 2001 the court rejected McClorys suit.  McClory died in 2006. 

On 4 December 1997, MGM announced that the company had purchased the rights to Never Say Never Again from Schwartzmans company Taliafilm.   The company has since handled the release of both the DVD and Blu-ray editions of the film.    

==See also==
 
* Outline of James Bond
 

==References==
 

==Bibliography==
 
*  
*  
*  
*  
*  
*  
*  
*  
*  
* 
*  
*  
*  
*  
*  
 

==External links==
 
*  
*  
*  
*  
*   at Metro-Goldwyn-Mayer

 
 
 
 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 