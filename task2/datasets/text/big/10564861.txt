Love Stories
 
{{Infobox film
| name = Love Stories
| image = LoveStoriesFilm.jpg
| image_size =
| caption = DVD cover
| director = Jerzy Stuhr
| producer = Jacek Bromski Juliusz Machulski Jacek Moczydłowski
| writer = Jerzy Stuhr
| narrator =
| starring = Jerzy Stuhr Dominika Ostałowska
| music = Adam Nowak
| cinematography = Paweł Edelman
| editing = Elżbieta Kurkowska
| distributor =
| released = September 8, 1997
| runtime = 87 mins.
| country = Poland Polish
| budget = PZL 1,500,000 (estimated)
| gross =
| preceded_by =
| followed_by =
}} Polish film about four men with unresolved romances. Jerzy Stuhr plays all four characters and wrote and directed the film.

The film is dedicated to Krzysztof Kieślowski, who gave Stuhr important roles in his films.

==Plot==
 

The four characters played by Jerzy Stuhr all arrive at the same place at different times in different vehicles: a college professor in his own car, a priest in a taxicab, an army officer in a government vehicle and a prisoner in a police van accompanied by two police officers. As the opening credits roll, the four characters walk about the same building.

The professor collects the written exams of his students, and is surprised that one, Ewa Bielska, has written on hers, "I love you." He resists her advances but eventually gives in. For a crucial oral exam with the dean, Ewa asks for the professors help, because she doesnt know anything at all. When the time comes, Ewa decides to resign, and the dean jokes with the professor that this is Poland, not America, and he wouldnt have been kicked out for having an affair with a student.

The priest is at the confessional when a young girl shows up claiming to be his daughter. Her story checks out: he knows her mother from Radom, 11 years ago. The mother died six years ago, the girl says. The girl returns to the orphanage she escaped from. Soon the parish learns but wants the priest to stay on. At the end, he decides to quit the church to be a father to his daughter.

The army officer, Colonel Matałowski of the Polish Land Forces, goes home to a house where almost everything is under lock and key, even the fridge and he is estranged from his wife. An old love shows up, Tamara. They have drinks and talk about their love letters. His superiors disapprove of the affair because shes Russian. Matałowski sees her one last time, driving her to the train station.

 

The prisoner, Zdzisław Filip, is convicted of trafficking Pakistani heroin and sentenced to five years in jail and a fine of 5,000 zloty. As the police haul him away, he tells his wife Kryska to go to hell. Four years later, during a conjugal visit, Zdisław gives her directions to the buried cash by tapping on her back. Kryska finds the money, but instead of using it to pay Zdisławs fine, she tries to leave the country. Zdisław is upset but claims to have expected her to do that. Since he cant pay the fine, he has to spend another two years in jail.

All four characters wind up at one point seeing a man in an archives. The teacher and the colonel both go down an elevator and are left alone in a long corridor.

==Cast==
*Jerzy Stuhr - The university teacher / The priest / Col. Jerzy Matałowski / Prisoner Zdzisław Filip / Petent
*Dominika Ostałowska - Ewa Bielska - the student
*Irina Alfyorova - Tamara
*Karolina Ostrożna - Magda Jarzębowska - Priests Daughter
*Katarzyna Figura - Kryśka - Zdzisławs wife
*Jerzy Nowak - The Accountant
*Andrzej Hudziak  - The Assistant Investigator
*Jerzy Trela - General
*Katarzyna Łaniewska - Filips mother
*Artur Barciś - Lawyer
*Sebastian Konrad - Assistant on the university
*Krzysztof Stroiński - Decan

==Awards==
Love Stories won the Golden Lion award at the Polish Film Festival,  the Jury Prize and the Silver at the Festróia - Tróia International Film Festival, and the Grand Jury Award at the Newport Beach Film Festival.

 

Jerzy Stuhr also picked up four of the five prizes he was nominated for at the Venice Film Festival.

==DVD release==
The MGE Region 1 DVD release is in "telewizorow" 4:3 aspect ratio. It has the bare minimum: scene access and English subtitles.

==References==
{{reflist|refs=
}}

==External links==
*  
*  
*   at the Polish Movie Database  

 
 
 