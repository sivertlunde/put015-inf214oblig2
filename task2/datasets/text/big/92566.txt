Picnic (1955 film)
{{Infobox film
| name           = Picnic
| image          = Original movie poster for the film Picnic.jpg
| image_size     =
| border         =
| alt            =
| caption        = Theatrical release poster
| director       = Joshua Logan
| producer       = Fred Kohlmar
| screenplay     = Daniel Taradash
| based on       =  
| starring       = William Holden Kim Novak Betty Field Rosalind Russell
| music          = George Duning
| cinematography = James Wong Howe
| editing        = William A. Lyon Charles Nelson
| distributor    = Columbia Pictures
| released       =  
| runtime        = 115 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $6,300,000    
}}
 romantic comedy-drama nominated for six Academy Awards and won two.

The film dramatizes twenty-four hours in the life of a rural Kansas town set in mid-20th century United States. It is the story of the proverbial outsider who blows into town and subsequently manages to upturn complacency, shake convention, disrupt, rearrange lives and reset the fates of all those with whom he comes into contact.

==Plot==
  Nick Adams) from pestering neighbor Madge Owens (Kim Novak), who happens to be dating Alan. Her single-parent mother (Betty Field) is hoping Madge will marry Alan, which would thus raise both Madge and herself into the towns highest, respectable social circles. Alan wants to marry Madge, but his father thinks she is beneath him. Madge tells her mother she doesnt love Alan and is weary of being liked only because she is pretty.

Hal gets along wonderfully with almost everyone, and tries very hard to be accepted. Alan is very happy to see "same old Hal", whom he takes to the familys sprawling grain elevator operations. He promises Hal a steady job as a "wheat scooper" (though Hal has unrealistic expectations of becoming an executive, and is disappointed) and invites Hal to the towns Labor Day picnic. Hal is wary about going to the picnic, but Alan nudges him into it, saying Hals "date" for the picnic will be Madges bookish younger sister Millie (Susan Strasberg), who is quickly drawn to Hals cheerful demeanor and charisma. Alan reassures Mrs. Owens that although Hal flunked out of college and lost his football scholarship because he did not study, there are no worries about him. The afternoon carries on very happily, until Hal carelessly starts talking about himself too much and Alan stops him with a cutting remark. As the sun goes down, everyone wanders off. Millie draws a sketch of Hal and tells him she secretly writes poetry. Hals behavior towards her is friendly and utterly trustworthy, but his replies show he has no understanding of her world at all. Madge is named the towns annual Queen of Neewollah ("Halloween" spelled backwards), and Hal longingly gazes at her as she is brought down the river in a swan-shaped pedal-boat. They shyly say "Hi" to each other as she glides by.
 Roman gladiator. When Hal tries to ward off the schoolteacher, she rips his shirt then bitterly calls him a bum. Mrs. Owens and Alan show up and think Hal has caused a messy scandal, made all the worse when Millie breaks down, screaming, "Madge is the pretty one!" and becomes ill from the whiskey. Rosemary, still blinded by her anger, tells Mrs. Owens that Hal gave Millie the whiskey, while Howards plea that he brought the whiskey seems to fall on deaf ears. By now a crowd is watching, and Hal flees into the darkness.

Madge follows Hal to Alans car and gets in with him. He angrily tells her to go home. However, she wont budge, so he drives off with her to town. By the river he tells her he was sent to reform school as a boy for stealing a motorcycle and that his whole life is a failure. Madge kisses Hal, which astonishes him. They promise to meet after she gets off work at six the next evening. Hal drives back to Alans house to return the car, but Alan has called the police and wants Hal arrested. After trying to talk things out, Hal flees the house in Alans car with the police following close behind. Leaving the car back by the river, Hal goes into the water, gets away from them and shows up at Howards apartment, asking to spend the night there. Howard is very understanding and now has his own worries: a highly distraught, desperate and remorseful Rosemary has begged him to marry her. Back at the Owens house, Madge and Millie cry themselves to sleep in their shared room.

The next morning, Howard comes to the Owens house, intending to tell Rosemary he wants to wait, but at the sight of him she becomes overjoyed, thinking he has come to take her away. Flustered in front of the whole household and other schoolteachers, Howard wordlessly goes along with this. As he passes Madge on the stairs, he tells her Hal is hiding in the backseat of his car. Hal is able to slip away before the other women gleefully paint and attach streamers and tin cans to Howards car, throwing rice and asking him where hell take Rosemary for their honeymoon. As Howard and Rosemary happily drive off to the Ozarks, Hal and Madge meet by a shed behind the house. He asks her to meet him in Tulsa, where he can get a room and a job at a hotel as a bellhop and elevator operator. Mrs. Owens finds them by the shed and threatens to call the police. Hal runs to catch a passing freight train, crying out to Madge, "You love me! You love me!"

Upstairs in their room, Millie tells Madge to "do something bright" for once in her life and go to Hal. Madge packs a small suitcase and, despite her mothers tears (but also nudged on by Mrs. Potts), boards a bus for Tulsa. 

==Main cast==
* William Holden as Hal Carter
* Kim Novak as Marjorie Madge Owens
* Betty Field as Flo Owens
* Susan Strasberg as Millie Owens
* Cliff Robertson as Alan Benson
* Rosalind Russell as Rosemary
* Arthur OConnell as Howard Bevans
* Verna Felton as Helen Potts
* Reta Shaw as Irma Kronkite
* Raymond Bailey as Mr. Benson Nick Adams as Bomber

==Production==
  contract with such a prestigious project", even though the contract paid him $30,000 instead of the $250,000 he was earning free lance.   Picnic was one of Novaks earliest film roles, and this movie made her a star. In the film Holden keeps his hair combed in an untidy fringe over his forehead and has the sleeves of his shirt rolled up throughout the film. He shaved his chest for the shirtless shots and was reportedly nervous about his dancing for the "Moonglow" scene. Logan took him to Kansas roadhouses where he practiced steps in front of jukeboxes with choreographer Miriam Nelson. Heavy thunderstorms with tornado warnings repeatedly interrupted shooting of the scene on location, and it was completed on a backlot in Burbank, California|Burbank, where Holden (according to some sources ) was "dead drunk" to calm his nerves.
 Broadway production. 
 Continental Trailways bus separately bearing the two leading characters.

===Locations===
 
The extensive use of Kansas locations highlighted the naturalistic, small-town drama. The Labor Day picnic scenes were shot and edited like a documentary film.    Picnic was shot mostly around Hutchinson, Kansas.   Other locations include the following: 
* Halstead, Kansass Riverside Park is where all the Labor Day picnic scenes were filmed. The park and many landmarks still existed at the time of the movies 50th anniversary..The merry-go-round and cable suspension footbridge, which spans the Little Arkansas River, are still located there. Nickerson is the location of the two adjacent houses where Madge (Kim Novak) and her family live, with Mrs. Potts next door; also where Hal (William Holden) "jumps a freight" to go to Tulsa and where Madge boards a bus in the last scene.
* Salina, Kansas|Salina, where Hal jumps off a train in the opening scene and meets Alan (Cliff Robertson) at Alans fathers large house. This also is where Madge kisses Hal by the Saline River and where he escapes from the police by running under a waterfall.
* Sterling, Kansas|Sterling, where the pre-picnic swim in the lake was filmed.

==Reception== cover story. 

The film was restored in the mid-1990s,  which brought many art-house bookings.  Stephen Holden, in a 1996 review of the restored film, began by noting: 
 "Today it probably wouldnt be worth more than a PG-13 rating (if even that), but in 1955, the "Moon glow" and "torn shirt" sequences from the movie Picnic were about as steamy as Hollywood could get in evoking explosive sex." 
According to Holden, "Rosalind Russell is vividly scary as an older schoolteacher who foolishly lunges after Hal. Betty Field is just right as Madges wistful, once-beautiful mother, who years earlier ran away with a man like Hal, and Susan Strasberg does well in the role of Madges shrill, tomboyish younger sister. George Dunings wistful, Aaron Copland|Copland-influenced score captures the mood of heated yearning that not only engulfed the movie, but also defined the countrys romantic ethos in the mid-50s."  
 

===Awards=== Best Art Best Film Best Actor Best Director, Best Music, Best Picture. Grand Prix of the Belgian Film Critics Association.

In 2002 Picnic was ranked #59 in AFIs 100 Years... 100 Passions.

==Music== Theme From Picnic" was a hit song which reached number one on the 1956 Billboard (magazine)|Billboard charts and was number 14 overall that year. Composed by George Duning and Steve Allen (although Allens lyrics were not used in the film), the song is featured in the famous dance scene between Holden and Novak, wherein Columbias musical director Morris Stoloff blended "Theme From Picnic" with the 1930s standard "Moonglow (song)|Moonglow". The two songs were often paired in later recordings by other artists. The soundtrack album reached #23 on the Billboard charts. 

===Subliminal marketing hoax===
In 1957, marketing researcher James Vicary said he had included subliminal messages such as eat popcorn and drink Coca-Cola in public screenings of Picnic for six weeks, claiming sales of Coca-Cola and popcorn increased 18.1% and 57.8% respectively. However, Vicary later admitted there had never been any such messages and his announcement was itself a marketing trick.   

==Remakes== remade for television twice, first in 1986, directed by Marshall W. Mason and starring Gregory Harrison, Jennifer Jason Leigh, Michael Learned, Rue McClanahan and Dick Van Patten. The second remake was in 2000, starring Josh Brolin, Gretchen Mol, Bonnie Bedelia,  Jay O. Sanders and Mary Steenburgen. The screenplay adaptation by Shelley Evans was directed by Ivan Passer.

==References==
{{reflist|colwidth=32em|refs=
    
   
}}

==External links==
 
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 