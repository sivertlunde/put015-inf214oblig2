Somers Town (film)
 
 
 
{{Infobox film
| name           = Somers Town
| image          = Somers town british quad.jpg
| caption        = Original British cinema poster
| director       = Shane Meadows
| producer       = Barnaby Spurrier
| writer         = Paul Fraser
| starring       = Thomas Turgoose Piotr Jagiello
| music          = Louise Knight Sue Pocklington
| cinematography = Natasha Braier
| editing        = Richard Graham
| studio  = Big Arty Productions
| distributor    = The Works (UK) Film Movement (US)
| released       =  
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          = £566,616
}}
 2008 film directed by Shane Meadows, written by Paul Fraser and produced by Barnaby Spurrier. It stars Thomas Turgoose, Piotr Jagiello, Kate Dickie, Perry Benson, and Elisa Lasowski. It was entirely funded by Eurostar. 
 Somers Town area of London, largely shot in black and white.

==Plot==
Somers Town follows several days in the lives of two teenage boys, Tomo and Marek, who develop a mutual trust and form an unlikely friendship. Marek, a Polish immigrant, lives with his father, who drinks too much. The film begins with Tomo running away to London from a lonely, difficult life in Nottingham. When Tomo arrives at his destination, he attempts to enjoy cans of Carling that he asked a stranger to purchase for him. His luck changes when three boys approach him and ask him questions. The gang robs Tomo of his bag, which contains all of his money and clothes. They also beat him up, leaving him severely bruised.

In a local café Tomo approaches Marek who has taken photographs of the beautiful French waitress. To tease Marek, Tomo runs away with Mareks photographs, but then gives them back, and they become friends. Marek explains that the woman in the photographs is his girlfriend, and her name is Maria. Since they have neither kissed nor done anything remotely sexual, Tomo thinks she is just a friend of Mareks.

Tomo then lives clandestinely at Mareks place. The latter is adamant that his father not discover Tomos existence at the flat. When Tomo and Marek find a wheelchair left in the street as rubbish, they offer to take Maria home in it. They describe it as being her special taxi. She enjoys the ride,and she kisses both boys upon arriving at her flat. Maria tells them she likes them equally. The next scene shows Marek outside the door of the toilet. He encourages Tomo to hurry up. Tomo is in the bathroom and cannot be huried. Marek is worried that his father has only gone to the shops and will catch Tomo. Marek then catches a naked Tomo sitting on the edge of the bathtub masturbating while looking at a picture of Maria. Tomo, however, is not embarrassed and laughs at the interruption.

Tomo and Marek then pass by the café where they learn that Maria has gone home to France because one of her family members fell ill. The boys are upset that Maria did not inform them, and that they bought food for her that she will not be able to eat. Troubled, the boys become drunk on the wine that they bought for Maria and make a mess of Mareks flat. Mareks father catches the pair, throws Tomo out of the flat, and tells his son to clean up the mess. Marek then expresses to his father how lonely he is. A neighbour lets Tomo live with him, on the condition that Tomo perform whatever tasks he demands. Tomo and Marek come up with an idea to save up money to travel to France together.

A hand-held camera reveals the two boys travelling to France. They meet the waitress, who embraces them and is very affectionate towards them. The last shot of the film shows Marek and Tomo each giving the French waitress a kiss on the cheek, as she smiles. The trip to France is filmed in colour.

==Production== St Pancras, which was just coming to the end of a £800 million re-build. Applying for Eurostar funding had been conceived by the Mother Advertising agency. The initial idea had been to make a short film, but it developed into a feature length script. The Mother agency approached Shane Meadows about directing the film; Meadows co-opted his regular script writer Paul Fraser and Tomboy Films produced it.

==Location== Somers Town, an area of the London Borough of Camden south of Camden Town. The story is set immediately around Phoenix Court, a low-rise council property in Purchese Street.

==Reception== Berlin and Tribeca Film Festivals. At Tribeca, stars Thomas Turgoose and Piotr Jagiello jointly shared the award for Best Actor in a Narrative Feature Film. It premiered in the UK at Edinburgh International Film Festival on 20 June 2008, where it won the Michael Powell Award, the festivals highest award.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 