Morozko (1924 film)
{{Infobox film
| name           = Morozko
| writer         =
| starring       =
| director       = Yuri Zhelyabuzhsky
| producer       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 40 minutes
| language       = Silent film
| country        = Soviet Union
| budget         =
}} 1924 silent Father Frost.

==Plot summary==
An old woman has a daughter she loves and a step-daughter, she tells her husband to take the step-daughter into the forest and leave her there for Father Frost. When Father Frost arrives he takes to her and leaves her riches. When the old man returns to collect the body of the step-daughter he is astonished, and relieved, to find her still alive. They return to the village where the old woman is horrified that the step-daughter is not only still alive but rich. She orders the old man to take her beloved daughter to the forest so that Father Frost can bestow wealth on her. When Father Frost arrives the daughter is rude to him and Father Frost leaves her to die. The old man returned to the forest and brings the dead girl back to the village and her distressed mother. The step-daughter marries a neighbour.

==Cast==
*Boris Livanov		
*Varvara Massalitinova		
*Vasili Toporkov		
*Klavdiya Yelenskaya

==References==
 

==External links==
* 

 
 
 


 