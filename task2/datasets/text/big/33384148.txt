Dekh Indian Circus
{{Infobox film
| name           = Dekh Indian Circus
| image          =
| alt            =  
| caption        = Theatrical release poster
| director       = Mangesh Hadawale
| producer       = Anil Lad Mahaveer Jain Chirag Shah Vivek Oberoi
| story          = Mangesh Hadawale
| starring       = Tannishtha Chatterjee Nawazuddin Siddiqui Virendra Singh Rathod Suhani Oza
| music          = Shankar-Ehsaan-Loy
| cinematography = Laxman Utekar
| editing        = James J Valiakulathil
| studio         = Sundial Pictures
| distributor    =
| released       =  
| runtime        = 101 minutes
| country        = India
| language       = Hindi  Rajasthani
| budget         =
| gross          =
}}
 16th Busan Film Festival in the New Currents section,    opening to positive review from critics     and winning the Audience Choice award.   
Nawazuddin Siddiqui & Tannishtha Chatterjee won the best actor and actress awards respectively for this movie at the 12th annual New York Indian Film Festival (NYIFF)    

==Plot==
Kajro (Tannishtha Chatterjee) and Jethu (Nawazuddin Siddiqui) are investing their life in fulfilling the eternal Indian dream of educating their children Ghumroo (Virendra Singh Rathod) and Panni (Suhani Oza).  Kajro, as the resilient mother, in the face of adversity inculcates great values in her children; While Jethu as the mute patriarch of the family suffers the day-to-day heartbreaks in silence.

Set in the present day deserts of barren Rajasthan, the storyline progresses about the daily trials and tribulations of this family and at a larger level depicts the aspirations of rural India. Where aspirations have arrived at every doorstep but the means are still far behind.
In the backdrop of the State elections, a Circus has come to a nearby town that maybe Panni and Ghumroo’s only escapade to a fantasy world without any heartbreak. A flag on the door could be the key to this escape. But would Jethu and Kajro find it so easy to see a small dream of their children come true or would they themselves become puppets in the circus of life. Be a part of this cinematic journey which shows a circus at every nook and corner of living and existence.

==Cast==

*Tannishtha Chatterjee as Kajaro, who though lives in the rural part of the country, one can still compare her to the modern, urban, empowered woman of today. Her thoughts and values are deeply rooted in Indian tradition and culture. From a very young age she has been independent and strong willed.

*Nawazuddin Siddiqui as Jethu,  belongs to the ‘Rabari’ community of Rajasthan. He resembles the ‘son of the soil’. His community comes under the broad classification of the nomadic tribe that moves from place to place. Jethu is a very loving and caring husband and a father. He and Kajaro have been childhood sweethearts and has tremendous faith and trust in her.

*Virendra Singh Rathod as Ghumroo the 7 years old elder child of Jethu and Kajaro. He is mostly in his school uniform or the Rabari costume. He also sports some Rabari accessories.He has big bright eyes. They reflect a lot of honesty and purity. They are deep and intense and are a mirror of his soul that is equally pure and genuine.

*Suhani Oza as Panni, the younger daughter of Kajaro and Jethu. She resembles her mother in many ways especially the sharp features and is 5 years old. Panni is a very cute and an innocent looking girl. She exudes a lot of sweetness from the way she talks and child like mannerisms.

==Music==
The soundtrack of the film is composed by Shankar-Ehsaan-Loy, while the lyrics are penned by Prasoon Joshi. The score of the film is done by Wayne Sharp.

{{Track listing
| headline  = Dekh Indian Circus: Original Motion Picture Soundtrack
| extra_column = Singer(s)
| title1 = Lambuda Kakka
| extra1 = Rasika Shekar
| title2 = Dekh Indian Circus
| extra2 = Kailash Kher
}}

==Reception==
The film received overwhelmingly positive reviews from the critics. Richard Kuipers of Variety (magazine)|Variety praised the film for "bringing the themes of inequality and class divisions together in the highly entertaining visit to the big top."     Kirk Honeycutt         in his review for the Hollywood Reporter, praised director Mangesh Hadawale for portraying the issues of third-world through family comedy that contains a stinging satire of contemporary India and its rampant corruption. 

==Awards==
; 12th New York Indian Film Festival 
* Won- Best Actor - Nawazuddin Siddiqui
* Won- Best Actress - Tannishtha Chatterjee
; 16th Busan International Film Festival 
* Won- Audience Choice Award
; 60th National Film Awards   
* Won- National Film Award for Best Childrens Film
* Won- National Film Award for Best Child Artist - Virendrasingh Rathore
* Won- National Film Award – Special Jury Award / Special Mention (Feature Film) - Nawazuddin Siddiqui
* Won- National Film Award – Special Jury Award / Special Mention (Feature Film) - Tannishtha Chatterjee

==References==
 
 

 
 
 
 
 
 