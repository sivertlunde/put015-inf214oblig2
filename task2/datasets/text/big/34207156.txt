Boarding House Blues
{{Infobox film
| name           = Boarding House Blues
| image_size     = 
| image	         = Boarding House Blues FilmPoster.jpeg
| caption        = Original film poster
| director       = Josh Binney
| producer       = E.M. Glucksman (producer)
| writer         = Hal Seeger (writer)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Sydney Zucker
| editing        = 
| studio         = All-American News
| distributor    = 
| released       = 1948
| runtime        = 90 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Boarding House Blues is a 1948 American race film directed by Josh Binney. It was the penultimate feature film of All-American News, a company that made newsreels for black Americans.  

== Plot summary ==
When Moms boarding house is threatened with closure and eviction of the tenants due to non payments, Moms tenants get together to put on a show to raise the money needed to save Mom and their home.

== Cast ==
*Moms Mabley as Moms
*Dusty Fletcher as Dusty
*Marcellus Wilson as Jerry
*Marie Cooke as Lila Foster Augustus Smith as Norman Norman
*John D. Lee Jr. as Stanley
*Emory Richardson as Simon
*Harold Cromer as Moofty
*Sidney Easton as Boo Boo
*Freddie Robinson as Freddie
*John Mason as Boarders (with "company")
*John Riano as Steggy (the ape)
*Lucky Millinder as Himself (bandleader)
*Una Mae Carlisle as Herself (singer)
*Bull Moose Jackson as Himself (singer)
*Warren Berry as One of Berry Brothers
*Nyas Berry as One of Berry Brothers
*Anistine Allen as Herself (singer)
*Paul Breckenridge as Himself (singer)
*James Cross as Stump
*Eddie Hartman as Stumpy
*Lee Norman as Themselves
*Crip Heard as Himself (one-legged dancer)
*Edgar Martin as Joe

== Soundtrack ==
*John Mason and Company - "Gimme"
*The Berry Brothers - "Youll Never Know" (Written by Harry Warren, lyrics by Mack Gordon)
*Una Mae Carlisle - "Throw It out of Your Mind" (Written by Louis Armstrong and Billy Kyle)
*Una Mae Carlisle - "It Aint Like That" (Written by Hot Lips Page)
*Stump and Stumpy - "Weve Got Rhythm to Spare" 
*Paul Breckenridge with Lucky Millinder band "We Slumber" 
*Anistine Allen with Lucky Millinder band - "Let It Roll"
*Bull Moose Jackson with Lucky Millinder band - "Yes I Do"

==Notes==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 


 