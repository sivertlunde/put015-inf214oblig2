Don't Play Us Cheap
{{Infobox film
| name           = Dont Play Us Cheap
| image          = Dont Play Us Cheap DVD.jpg
| caption        = DVD cover.
| director       = Melvin Van Peebles
| producer       = 
| eproducer      =
| aproducer      =
| writer         = Melvin Van Peebles
| starring       = 
| music          = Melvin Van Peebles
| cinematography = Bob Maxwell
| editing        = Melvin Van Peebles
| distributor    = 
| released       = 
| runtime        = 100 minutes
| country        = United States
| awards         =
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Dont Play Us Cheap is a 1972 musical written, produced, and directed by Melvin Van Peebles, about an imp and a devil who take human form and try to break up a Harlem house party. A film version was produced in 1973.

==Plot==

Trinity and Brother Dave are a pair of devil-bats looking for a party to break up. They come across a party in Harlem. Although Trinity is eager, Dave warns him not to touch it. "When black folks throw a party, they dont play!" Trinity joins the party, already in progress, thrown by Miss Maybell in honor of her niece Earnestines birthday.
 records ("you cant have a party without music"), but finds that they are unbreakable. He drinks an entire bottle of liquor, thinking he has depleted their supply of alcohol, but finds out that all of the guests have brought their own bottles, and when he tries to eat all of the sandwiches, another plate is brought in.

Trinity finds himself unwilling to continue being mean after he insults Earnestine, making her cry. Trinity apologizes to her, and tells her that he has fallen for her. Three more guests show up, Mr. and Mrs. Johnson, and their college-educated son Harold. Earnestine ignores Trinity for Harold. Trinity becomes jealous.

Brother Dave arrives in human form, eager to break up the party, but Trinity is unwilling to. Mr. Johnson tells Harold not to get involved with Earnestine, because her family is too "common," and he cant risk the big future he has ahead of him. Earnestine approaches both Harold and Trinity to dance, but they are pulled back by Mr. Johnson and Dave.

Dave persuades Trinity to try to break up the party before midnight, when they will both be turned into the thing that they pretend to be: human beings. As time runs short, Dave and Trinity find themselves at the dinner table with the rest of the guests. Dave insults Mrs. Johnson, prompting her to leave with her husband and son. The rest of the guests tell Dave that theyre glad that they left.

After the dinner, Trinity stands up and announces that he and Earnestine are getting engaged, an announcement which infuriates Dave. Dave makes one last attempt to break up the party by trying to make a move on Miss Maybell. When Dave finds that she is all too willing, he turns himself into a cockroach and tries to sneak out the door before being smashed by Miss Maybell.

==Cast== Thomas Anderson as Mr. Percy
:Jay Van Leer as Mrs. Johnson (as Jay Vanleer) Robert Dunn as Brother Bowser
:Mabel King as Guest
:George Ooppee McCurn as Brother Washington
:Joshie Jo Armstead as Guest
:Frank Carey as Mr. Johnson
:Nate Barnett as Harold Johnson
:Esther Rolle as Miss Maybell
:Avon Long as Brother Dave
:Rhetta Hughes as Earnestine
:Joe Keyes Jr. as Trinity (as Joseph Keyes)

==Conception==
At the time Melvin Van Peebles came up with the story for Dont Play Us Cheap, he was living in Paris, France|Paris, but had gotten a summer job in New York making a documentary. Along with the job, Van Peebles was given an apartment in a posh neighborhood on the lower east side of Manhattan.

On a very hot day, Van Peebles was lounging out in front of the apartment, and an old black lady came down the street and told Van Peebles that she wanted some water and to use the bathroom. The woman thanked Van Peebles, and a few days later, Van Peebles received a telephone call from her inviting him to a party she was throwing for her niece.

When he returned to France, he thought of what would happen if these wonderful, kind, open people were invaded by imps bent on destroying their party. The story became the basis for a novel, Harlem Party, and later a French-language musical play, which Van Peebles later translated into English, and made as a film in 1973. 

==Songs==
* You Cut Up the Clothes in the Closet of My Dreams
* Break That Party & Opening
* Saturday Night
* The Bowsers Thing
* The Book of Life
* Quittin Time
* Aint Love Grand
* Im a Bad Character
* Know Your Business
* Feast on Me
* Aint Love Grand
* Break That Party
* Someday it Seems That it Aint Just Dont Even Pay to Get Out of Bed
* Quartet
* The Phoney Game
* It Makes No Difference
* Bad Character Bossa Nova
* Quartet
* The Washingtons Thing
* If You See a Devil, Smash Him

==Response==
The stage musical was nominated for two Tony Awards. The film received only a minimal release in 1973, and was largely unseen until it was released on videotape in the mid-1990s. It was released on DVD in 2006.

==Broadway Run==
The Broadway production ran from May 16, 1972 to September 30, 1972 at the Ethel Barrymore Theatre. 

==Notes==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 