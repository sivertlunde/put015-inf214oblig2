The Dead (1987 film)
{{Infobox film
| name     = The Dead
| image          = Thedeadposter1987.jpg
| image_size     = 195px
| caption        = Theatrical release poster
| writer         = Tony Huston
| based on       =  
| starring       = Anjelica Huston Donal McCann Dan OHerlihy Donal Donnelly Helena Carroll Cathleen Delany
| director       = John Huston Fred Murphy
| music          = Alex North
| editing        = Roberto Silvi
| producer       = Chris Sievernich Wieland Schulz-Keil
| distributor    = Vestron Pictures
| released       =  
| runtime        = 83 min
| language       = English
| budget         = $3.5 million   
| gross          = $4,370,078
}}
The Dead is a 1987 feature film directed by John Huston, starring his daughter Anjelica Huston. The Dead was the last film that Huston directed, and it was released posthumously.
 The Dead" by James Joyce (from his short works collection Dubliners), and nominated for an Academy Award for Writing Adapted Screenplay. It was also nominated for an Academy Award for Costume Design.
 Epiphany party held by two elderly sisters. The story focuses attention on the academic Gabriel Conroy (Donal McCann) and his discovery of his wife Grettas (Anjelica Huston) memory of a deceased lover.

==Film adaptation==
This film adaptation by John Hustons son Tony Huston can be considered a close adaptation of Joyces short story, with some alterations made to the dialogue to aid the narrative for cinema audiences.

The most significant change to the story was the inclusion of a new character, a Mr Grace, who recites an eighth-century Middle Irish poem, "Donal Óg".   The effect of this is to act as catalyst for the "Distant Music" that provokes the memories Gretta and Gabriel discuss at the end of the film.

==Cast==
*Gretta Conroy - Anjelica Huston
*Gabriel Conroy - Donal McCann
*Aunt Julia Morkan - Cathleen Delany
*Aunt Kate Morkan - Helena Carroll
*Lily - Rachael Dowling
*Mary Jane - Ingrid Craigie
*Mrs. Malins - Marie Kean
*Theodore Alfred “Freddy” Malins - Donal Donnelly
*Bartell DArcy - Frank Patterson
*Miss Daly - Lyda Anderson Kate OToole
*Miss Higgins - Bairbre Dowling
*Molly Ivors - Maria McDermottroe
*Mr. Bergin - Colm Meaney
*Mr. Browne - Dan OHerlihy
*Mr. Grace - Sean McClory
*Mr. Kerrigan - Cormac O’Herlihy
*Miss O’Callaghan - Maria Hayden
*Young Lady - Dara Clarke Paul Grant
*2nd Young Gentleman - Paul Carroll Patrick Gallagher
*Carman - Brendan Dillon
*Nightporter - Redmond M. Gleeson

==Production==
Chris Sievernich and Weiland Schulz-Keil had raised money for Under the Volcano and would do likewise for The Dead. Screen rights to the story were purchased from the Joyce estate for $60,000. Shooting began 19 January 1987. 

According to Pauline Kael, "Huston directed the movie, at eighty, from a wheelchair, jumping up to look through the camera, with oxygen tubes trailing from his nose to a portable generator; most of the time, he had to watch the actors on a video monitor outside the set and use a microphone to speak to the crew. Yet he went into dramatic areas that hed never gone into before - funny, warm family scenes that might be thought completely out of his range. Huston never before blended his actors so intuitively, so musically." 

==Release==
The Dead was initially released on DVD by Lionsgate on November 3, 2009. However, the DVD had nearly ten minutes of the film missing.  When word of this was posted on various websites, Lionsgate eventually released a complete version.

==Awards==
Winner
*1987 Tokyo International Film Festival - Special Achievement Award, John Huston
*1988 National Society of Film Critics Awards (USA) - Best Film
*1988 Independent Spirit Award for Best Director - John Huston
*1988 Independent Spirit Award for Best Supporting Female - Anjelica Huston
*1989 Bodil Awards (Danish Film Critics) for Best Non-European Film
*1989 Fotogramas de Plata (Spain) for Best Foreign Film
*1989 London Critics Circle Film Awards for Director of the Year - John Huston

Nominated
*1988 Academy Award for Costume Design - Rachael Dowling and Dorothy Jeakins Academy Award for Best Writing, Screenplay Based on Material from Another Medium - Tony Huston
*1988 Independent Spirit Award for Best Cinematography - Fred Murphy
*1988 Independent Spirit Award for Best Screenplay - Tony Huston

==References==
 

==External links==
* 
*  
*  	

 

 
 
 
 
 
 
 
 
 
 
 

 