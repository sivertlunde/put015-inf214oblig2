The Doberman Gang
 
{{Infobox film
| name           = The Doberman Gang
| image          = The Doberman Gang Poster.jpg
| caption        = American film poster
| director       = Byron Chudnow
| producer       = David Chudnow Irving Temaner
| writer         = Louis Garfinkle Frank Ray Perilli
| narrator       =
| starring       = Byron Mabe Hal Reed Julie Parrish Simmy Bow JoJo DAmore John Tull Jay Paxton
| music          = Alan Silvestri Bradford Craig
| cinematography = Robert Caramico
| editing        = Herman Freedman
| studio         = Rosamond Productions Dimension Pictures Columbia Broadcasting System International Film Distributors Lorimar Productions Sofradis Warner Bros. (2010, DVD)
| released       =  
| runtime        = 87 minutes
| country        = United States English
| budget         = $100,000 (estimated)
}}
 animal trainer Dobermans to commit a bank robbery. The six dogs were all named after famous bank robbers. Their names were Dillinger (John Dillinger), Bonnie (Bonnie Parker), Clyde (Clyde Barrow), Pretty Boy Floyd, Baby Face Nelson, and Ma Barker.

Its score was the first composed by Alan Silvestri, who found later success with the soundtracks for more notable films such as the Back to the Future (franchise)|Back to the Future trilogy and Forrest Gump.

The film was shot completely on location in Simi Valley, California.
 Key Video label used superior quality magnetic soundtrack elements from Lorimar Productions, whose film library they were issuing on video at the time.

==Remakes==
In 2003 it was reported that producers Dean Devlin and Charles Segars obtained the film rights in hopes of creating a remake, with Byron Chudnow acting as executive producer. In October 2010 it was announced that producer Darren Reagan of 11eleven Entertainment, along with Cesar Millan, was developing the remake. {{Cite web
  | last = McNary
  | first = Dave  | title = Dog Whisperer digs up Doberman films
  | publisher= Variety (magazine)|Variety 
  | url= http://www.variety.com/article/VR1118026237?refCatId=13
  | date = 25 October 2010
  | accessdate = 5 July 2011 }}
 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 