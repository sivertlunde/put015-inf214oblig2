The Mother of Invention
{{Infobox film
| name           = The Mother of Invention
| image          = TMOI_Poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Joseph M. Petrick   Andrew Bowser
| producer       = Joseph M. Petrick Andrew Bowser
| screenplay     = Joseph M. Petrick
| starring       = Andrew Bowser Jimmi Simpson Kevin Corrigan Mark Boone Junior Dee Wallace Craig Anton Ruby Wendell F. Jason Whitaker
| music          = 
| cinematography = David Kalani Larkins
| editing        = Andrew Bowser
| studio         = 
| distributor    = One Small Instrument Pictures
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Dave Allen, Ron Lynch.  The film premiered at the 2009 Sci-Fi-London film festival internationally and the Hollywood Film Festival domestically. The original score was composed and performed by Jim Hanft and the credits feature an original song by the band Copeland (band)|Copeland.
 

The film was released on DVD and digitally on June 14, 2011 and can be purchased at the iTunes Store and Amazon.com. It became available on Netflix for instant streaming on August 2, 2011. 
 

==Plot==
The film follows Vincent Dooly (Andrew Bowser), an aspiring inventor who dreams of winning the Thomas Alva Edison Award for Young Inventors (or "Eddy"). Each year, he enters and each year he humiliates himself with an invention that malfunctions in one way or another. A documentary crew follows Vincent in the last year that he is eligible to compete for the award, at the same time following Martin Wooderson (Jimmi Simpson), a smug wunderkind with a long history of winning the award with dull but marketable inventions. Through the months leading up to the Eddys, Vincent mentions (in a clandestine manner) an invention he is working on that he is sure will change his losing streak but refuses to reveal more. We meet Vincents best friend Gunter (F. Jason Whitaker), his endlessly supportive mother (Dee Wallace), his tweaked-out father (Mark Boone Junior), the object of his affection (Ruby Wendell) and his hero, a surly junkyard worker (Kevin Corrigan).

As the story moves along we see Vincents attempt at perfecting his many inventions and see him emotionally prepare for his last chance to win the coveted award while attempting to win the heart of Jenny (Wendell), a waitress at the local diner he frequents. The film culminates at the award ceremony where Vincent and Martin unveil their final inventions and vie for the prize.
 

== Trivia ==
The film gained momentum when a clip that had been created for Andrew Bowser to test Doolys signature underbite became viral after website eBaums World posted the video and many mistook Vincent for a real person. 

== References ==
 
 
*http://www.imdb.com/title/tt1274589/
*http://www.hollywoodawards.com/comedy.html
*http://www.imdb.com/title/tt1274589/plotsummary
*http://www.ebaumsworld.com/video/watch/80447834/
*http://www.quietearth.us/articles/2009/04/06/SciFi-London-Film-Festival-is-rarin-to-go-from-April-29th-to-May-4th
*http://www.quietearth.us/articles/2009/01/02/Hialrious-trailer-for-new-mockumentary-THE-MOTHER-OF-INVENTION
*http://videogum.com/40151/the_mothers_of_invention_2009s/movies/trailer/
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 