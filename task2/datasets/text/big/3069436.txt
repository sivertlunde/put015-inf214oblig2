The Night the Lights Went Out in Georgia (film)
{{Infobox film
| name           = The Night the Lights Went Out in Georgia
| image          = the_night_the_lights_went_out_in_georgia1981.jpg
| caption        = Theatrical release poster
| director       = Ronald F. Maxwell
| producer       = Bill Blake
| writer         = Bob Bonney
| starring       = Kristy McNichol Dennis Quaid Mark Hamill Don Stroud Arlen Dean Snyder Barry Corbin
| music          = Keith Allison Bobbie Gentry Mark Lindsay David Shire Bill Butler
| editing        = Anne Goursand
| distributor    = AVCO Embassy Pictures
| released       =  
| runtime        = 120 minutes
| country        = United States English
| budget         =
}}

The Night the Lights Went Out in Georgia is a 1981 film, starring Kristy McNichol, Dennis Quaid, Mark Hamill, and Don Stroud, directed by Ronald F. Maxwell.
 song of the same name (it shares almost no plot elements with the original song). In 1981, Tanya Tucker recorded a different version for the films soundtrack and new lyrics related to the plot of the film were written. These altered lyrics were based on the plot line of the movie, which is not the same as the story of the original song.

The film was shot on location in Dade County, Georgia and Manchester, TN.

== Plot summary ==
A young singer and his sister/manager travel to Nashville in search of stardom. As they journey from one grimy hotel to another, it becomes increasingly obvious that only one of them has what it takes to become a star.

Travis Child (Quaid) is a country singer who had one hit song and then faded from the scene. His ambitious younger sister, Amanda (McNichol), is determined to get them to Nashville where Travis can once again become a star. Her plans are derailed by Traviss lack of ambition and easy distraction by women and booze.

The two are separated in one town and by the time they find each other in the next one, Travis has been arrested for public drunkenness. To pay the fine he takes a job bartending at a roadside tavern called Andys, where he meets and falls for a young lady with a very jealous ex-boyfriend—who happens to be the deputy sheriff.

==Main cast==
* Kristy McNichol - Amanda Child
* Dennis Quaid - Travis Child
* Mark Hamill - Conrad
* Sunny Johnson - Melody
* Don Stroud - Seth James
* Barry Corbin - Wimbish
* Arlen Dean Snyder - Andy
* Ellen Saland - Nellie

"The Night the Lights Went Out in Georgia" proves to be a film of considerable feeling and tenderness."
Kevin Thomas, Los Angeles Times, Aug 7, 1981



==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 