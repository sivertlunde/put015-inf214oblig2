Terror from the Year 5000
 
{{Infobox Film
| name           = Terror from the Year 5000
| image          =Terror5000.jpg
| caption        = film poster by Albert Kallis
| director       = Robert J. Gurney Jr.
| producer       = Robert J. Gurney Jr. Samuel Z. Arkoff James H. Nicholson Gene Searchinger
| writer         = Robert J. Gurney Jr. Henry Slesar
| narrator       =  John Stratton Salome Jens Fred Herrick
| music          = Richard DuPage
| cinematography = Arthur Florman
| editing        = Dede Allen
| distributor    = 
| released       = 1958
| runtime        = 66 min.
| country        = USA English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 1958 United American science fiction film directed by Robert J. Gurney Jr. starring Ward Costello, Joyce Holden, John Stratton, Salome Jens and Fred Herrick. American International Pictures released the film as a double feature with The Screaming Skull.

== Plot ==
Working in the privacy of his Florida island estate, nuclear physicist Professor Howard Erling constructs a machine that breaks the time barrier. With his assistant Victor, Howard conducts a number of experiments and successfully transports a small statue from the future. Concerned over the vast amounts of energy needed to conduct the experiments and realizing that their unique work needs formal verification from another professional, Howard calls a halt to the research, despite Victors protests. The statue is then sent to premiere archeologist Robert Hedges, who runs a carbon dating test on it and discovers it comes from the year 5,000 A. D. Bob also learns that the statue is radioactive and attempts to contact Howard to question him about it. When he is unable to reach Howard by phone, Bob flies to Florida to see him. Upon leaving the airport in a rental car, Bob realizes that he is being followed. After a wild chase, Bob confronts his pursuer only to discover it is Claire Erling, Howards daughter and Victors fiancée. Claire is delighted by Bobs arrival and admits to sending the statue in hopes of accelerating the verification of her fathers long work. Claire assures Bob that neither the Erlings or Victor were aware of the radiation contamination. As Claire and Bob pilot a small motorized boat out to the island, the motor dies and Claire reveals this is caused by the power drain from her fathers lab. Unknown to Claire and Howard, Victor is conducting further experimentation in secret, against Howards orders. Howard receives Bob at the house graciously, but Victor is resentful, suspecting that the archeologist intends to refute their work. When the guest room is discovered locked and the key missing, Victor offers to share his room with Bob until the handyman, Angelo, can provide a new key. That night, however, Bob hears Victor enter the guest room next door and follows as Victor removes two metal suitcases from the guest room, then takes them outside where he throws them into a pond. The next morning, Bob accepts Claires swimming invitation and dives at the spot where Victor disposed of the suitcases. Underwater, Bob locates one suitcase, opens it and before he is forced to surface, finds the carcass of a small animal. Later, Howard shows Bob the lab and demonstrates how he and Victor have succeeded in "trading objects with the future." Howard sends a small bottle through the machine and receives a similar object back. Bob then suggests sending something unusual and submits his fraternity key. The men are startled when a coin from the future materializes with the words "save us" in Greek engraved upon it. That night after the others retire for the night, Victor sneaks down to the lab and continues his experiment by increasing the power level. The higher power results in a human form materializing in the machine that then grabs Victor. Meanwhile, Howard and Bob investigate when Claire sees something unusual from her window. Victor manages to return the figure to the future, although he suffers a serious gash on his arm. Howard and Bob discover that Angelo has been voyeuristically watching Claire and after Bob tells Claire, he comes across spots of blood outside of the guest room door. When Bob then reports Victors unusual behavior to Howard the next morning, Howard demands evidence. Bob goes to the pond to retrieve the suitcases, but Victor intercepts him and tries to stop him. Howard, Angelo and Claire break up the mens fight and when Howard requests an explanation, Bob reveals Victors radiation burned arm. Despite Victors objections, Howard, Bob and Claire take him to the hospital, where the doctor asks them to return in a few hours to give him time to examine Victor. While the Erlings take Bob to a movie, Victor flees from the hospital and drinks at a bar until he is thrown out. Bolstered by the drinks, Victor returns to the island, breaks into the lab and resumes working furiously with the machine at high power levels. Meanwhile, Howard, Bob and Claire stop at the bar and Howard is incredulous when the bartender tells them that Victor was there most of the afternoon. When the bars television reception goes out, however, Howard realizes that Victor has turned on the time machine. As the Erlings and Bob race back to the island, Victor succeeds in bringing a human form from the future, but is knocked out by the mysterious arrival. Later, upon finding Victor dazed, Howard summons a doctor who then orders a nurse to come out and tend to Victor. The time traveler explores the island surroundings, but when confronted by Angelo, it kills him. Unaware of the travelers presence, Bob succeeds in retrieving one of the metal suitcases from the pond, which contains the cadaver of a four-eyed cat. Returning to the house, Howard and Bob are stunned when they stumble upon Angelos body. After Victor tells them about the arrival of the time traveler, Howard confronts Victor with the cat, declaring it the likely result of radiation mutation. Meanwhile, the nurse has arrived on the island and is confronted by the traveler, a woman, who addresses the nurse in Greek. When the nurse flees, the traveler attacks and kills her, then uses a device to duplicate the nurses facial features, allowing her to masquerade as the nurse. After gaining entrance into the Erling house, the traveler sits with Victor while the men don radiation proof gear and search for the time traveler. The traveler hypnotizes Victor to induce him to return with her to the future where his healthy genes are needed to save her people from extinction. Attracted by noises in the lab, Claire is shocked to find Victor and the traveler reviving up the time machine. In an attempt to break the hypnotic spell on Victor, Claire attacks the traveler and pulls the mask from her face, revealing her radiation-scarred features. Having discovered the faceless body of the real nurse, Howard and Bob return to the lab. They find Victor defending Claire from the traveler until he and the traveler tumble against the machine, which electrocutes and kills them both. Afterward, Bob and Claire wonder if one of them should go to the future to help. Howard insists that by changing the present they can battle against an atomic holocaust future. 

== Cast ==
*Ward Costello as Dr. Robert Hedges
*Joyce Holden as Claire Erling
*Frederic Downs as Prof. Howard Erling John Stratton as Victor
*Salome Jens as Future Woman / Nurse
*Fred Herrick as Angelo
*Beatrice Furdeaux as Miss Blake
*Jack Diamond as First Lab Technician
*Fred Taylor as Second Lab Technician
*Bill Downs as Dr. Blair
*William Cost as Joe the Bartender

== Production ==
Outdoor shots were filmed in and around Dade County, Florida. The working title of the film was "The Girl from 5000 A.D. 

== Reception ==
 
The movie was featured on Mystery Science Theater 3000 during its eighth season in 1997.
The film currently holds a low 2.4/10 on the Internet Movie Database and it holds only 1 audience review on Rotten Tomatoes.

== References ==
 

==External links==
* 

 
 
 
 
 
 