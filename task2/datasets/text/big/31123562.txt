When You Come Home
{{Infobox film
| name = When You Come Home
| image =
| image_size =
| caption = John Baxter
| producer = John Baxter
| writer = Frank Randle   Geoffrey Orme   David Evans
| narrator =
| starring = Frank Randle   Leslie Sarony   Leslie Holmes   Diana Decker
| music = Percival Mackey
| cinematography = Geoffrey Faithfull
| editing = Ted Richards
| studio = Butchers Film Service
| distributor = Butchers Film Service
| released = 1948
| runtime = 97 minutes
| country = United Kingdom English
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} British comedy John Baxter and starring Frank Randle, Leslie Sarony and Leslie Holmes.  When the music hall where he works is threatened with closure, a handyman organises an effort to save it. It had a larger production budget than Randles previous films made in Manchester. 

==Cast==
* Frank Randle as Frank
* Leslie Sarony as Maestro
* Leslie Holmes - Fingers
* Diana Decker as Paula Ryngelbaum
* Fred Conyngham as Mike OFlaherty
* Jack Melford as Doctor Dormer Franklyn
* Linda Parker as Singer
* Lily Lapidus as Mrs. Ryngelbaum
* Tony Heaton as Joshua Ryngelbaum
* Hilda Bayley as Lady Langfield
* Lesley Osmond as Delia
* Gus Aubrey as Designer
* Ernest Dale as Fireman
* Rene Sibley-Grant as Mrs. Randle

==References==
 

==Bibliography==
* Murphy, Robert. Realism and Tinsel: Cinema and Society in Britain 1939-48. Routledge, 1992.

==External links==
* 

 

 
 
 
 
 
 


 
 