Savannah (film)
{{Infobox film
| name           = Savannah
| image          = Savannah_2013_movie_poster.jpg
| caption        = Movie poster
| director       = Annette Haywood-Carter
| producer       = Annette Haywood-Carter Randall Miller Jody Savin
| writer         = Annette Haywood-Carter Ken Carter
| based on       =  
| starring       = Jim Caviezel Jaimie Alexander Chiwetel Ejiofor Jack McBrayer Sam Shepard
| music          = Gil Talmi
| cinematography = Mike Ozier
| editing        = John David Allen Jonathan Alvord Dan OBrien
| studio         = Unclaimed Freight Productions Meddin Studios
| distributor    = Ketchup Entertainment
| released       =  
| runtime        = 110 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Savannah is a 2013 family history  , Jaimie Alexander, Chiwetel Ejiofor, Jack McBrayer and Sam Shepard. It was released by Ketchup Entertainment on 23 August 2013 in the US.   

==Summary== Savannah back in the early 20th century. Ward was born into privilege in the mid-1800s but settled for a life of duck hunting, which found him in front of Judge Harden (Holbrook) often. He was also found quite drunk, a point of tension with his wife, Lucy (Alexander) who married him to spite her father (Shepard) who wanted her to marry Sir Graham (McBrayer). Their marriage is later tested by his hard-drinking ways, his devotion to his hunting, and her breakdown after a stillbirth.  

==Cast==
*Jim Caviezel as Ward Allen   
*Jaimie Alexander  as Lucy Stubbs 
*Chiwetel Ejiofor as Christmas Moultrie, a former slave, becomes fast friends with Ward 
*Jack McBrayer as Sir Graham, wealthy man Mr. Stubbs set Lucy up to marry 
*Sam Shepard as Mr. Stubbs, Lucys overbearing father 
*Bradley Whitford as Jack Cay 
*Hal Holbrook as Judge Harden 
*Tracey Walter as Mathias 
*Simone Griffeth as Mrs. Stubbs, Lucys mother
*Stratton Leopold as John Elliot Ward

==Production==
Annette Haywood-Carter signed on as the director and also helped produce the film, along with Randall Miller and Jody Savin. With the film based on the book Ward Allen: Savannah River Market Hunter, Haywood-Carter co-wrote the screenplay with Kenneth Carter. Gil Talmi signed on to do the music for the film while Mike Ozier was in charge of cinematography and John David Allen with editing. Ketchup Entertainment released the film with Unclaimed Freight Productions production in association with Meddin Studios. 

===Filming=== Savannah locations. 

==See also==
*List of films featuring slavery

==References==
 

==External links==
* 

 
 
 
 
 
 