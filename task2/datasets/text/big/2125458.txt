Blood and Sand (1922 film)
{{Infobox film
| name           = Blood and Sand
| image          = Blood and Sand 1922 poster.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Theatrical release poster
| director       = Fred Niblo
| producer       = Fred Niblo (uncredited) Jesse L. Lasky
| screenplay     = June Mathis
| based on       = based on the novel Blood and Sand by Vicente Blasco Ibáñez and the play by Thomas Cushing Walter Long
| music          = 
| cinematography = Alvin Wyckoff
| editing        = Dorothy Arzner (uncredited)
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 80 minutes 9 reels (8,110 feet)
| country        = United States Silent English intertitles
| budget         = 
| gross          = $1,250,000 (US/Canada)   accessed 19 April 2014 
}}
  American silent silent drama film produced by Paramount Pictures, directed by Fred Niblo and starring Rudolph Valentino, Lila Lee, and Nita Naldi.

The film was based on the Spanish novel Blood and Sand (Sangre y arena) by Vicente Blasco Ibáñez (1909) and the play version of the book by Thomas Cushing.

==Plot==
Juan Gallardo (Valentino), a village boy born into poverty, grows up to become one of the greatest matadors in Spain. He marries a friend from his childhood, the beautiful and virtuous Carmen (Lee), but after he achieves fame and fortune he finds himself drawn to Doña Sol (Naldi), a wealthy, seductive widow.
 sadomasochistic overtones, but Juan, feeling guilty over his betrayal of Carmen, tries to free himself of Doña Sol. Furious at being rejected, she exposes their affair to Carmen and Juans mother, seemingly destroying his marriage. Growing more and more miserable and dissipated, Juan becomes reckless in the arena. He is eventually killed in a bullfight but does manage to reconcile with Carmen moments before he dies.

There is also a subplot involving a local outlaw whose career is paralleled to Juans throughout the film by the village philosopher: Juans fatal injury in the bullring comes moments after the outlaw is shot by the police.

==Cast==
* Rosa Rosanova - Angustias
* Leo White - Antonio
* Rosita Marstini - Encarnacion
* Rudolph Valentino - Juan Gallardo(billed Rodolph Valentino)
* Lila Lee - Carmen Charles Belcher - Don Joselito
* Fred Becker - Don Jose George Field - El Nacional
* Jack Winn - Potaje
* Harry Lamont - Puntillero
* Gilbert Clayton - Garabato Walter Long - Plumitas
* Nita Naldi - Doña Sol
* George Periolat - Marquis of Guevera
* Sidney De Gray - Dr. Ruiz

==Production notes==
The film was produced by Famous Players-Lasky Corporation and Paramount Pictures, and distributed by Paramount. June Mathis, who has been credited as discovering Valentino, adapted the novel for the screen. 

Dorothy Arzner worked as the films editor. Arnzer, who would later become one of the first female film directors, used stock footage of bullfights filmed in Madrid interspersed with close-ups of Valentino. Her work on the film helped to solidify her reputation of being a resourceful editor as her techniques also saved Paramount money. She would later say that working on the film was the "first waymark to my claim to a little recognition as an individual." 

==Reception== The Sheik Four Horsemen of the Apocalypse (both 1921), helped to establish Valentino as a star and was one of the most successful films of his career.  

==Other adaptations==
An earlier version of Blood and Sand was released in 1916 in film|1916, and filmed by Blasco Ibáñez himself, with the help of Max André. This earlier version was restored in 1998 by the Filmoteca de la Generalitat Valenciana (Spain).
  1941 version Spanish remake was directed by Javier Elorrieta and stars Chris Rydell, Sharon Stone, and Ana Torrent.

==In popular culture== Johnny "Blood" McNallys nickname - he started playing professional under an alias to protect his remaining college eligibility.  He and a friend passed a theater where Blood and Sand was playing. Suddenly, McNally exclaimed to his friend, "Thats it! You be Sand. Ill be Blood". 
 parodied by Stan Laurel in Mud and Sand (1922). In the film, Laurel portrays a character named Rhubarb Vaselino.  Will Rogers also parodied Blood and Sand in the Hal Roach short film Big Moments From Little Pictures (1924). 
 Blood and Sand.

==References==
 

==External links==
 
*  
*  
*  
*   (abridged:26 minutes)
*   available for free download at   (full)

 

 
 
 
 
 
 
 
 
 
 
 
 
 