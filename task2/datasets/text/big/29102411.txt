Obey The Law
{{Infobox film
| name           = Obey The Law
| image          = Obey The Law film poster.jpg
| caption        = Obey The Law theatrical poster
| director       = Al Raboch
| producer       = Al Raboch Harry Cohn
| writer         = Max Marcin(play) Al Raboch(adaptation)
| starring       = Bert Lytell Edna Murphy
| cinematography = J. O. Taylor
| editing        =
| distributor    = Columbia Pictures
| released       =  
| runtime        = 6 reels
| country        = United States
| language       = Silent film(English intertitles)
}}
 1926 silent film adventure-drama made by the Cohn brothers, Jack and Harry Cohn, and Al Raboch. The picture stars Bert Lytell and was released through the Cohns fledgling company Columbia Pictures. The Library of Congress holds a print of this film.  

==Cast==
* Bert Lytell – Phil Schuyler
* Eugenia Gilbert – The Girl(billed Eugenie Gilbert)
*Edna Murphy – The Daughter
*Hedda Hopper – Society lady Larry Kent – The Friend
*Paul Panzer – The Crook
*Sarah Padden – The Mother William Welsh – The Father

==References==
 

==External links==
*  at the Internet Movie Database
* 

 
 
 
 
 
 
 
 


 
 