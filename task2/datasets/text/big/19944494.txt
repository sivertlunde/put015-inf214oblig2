The Secret Diary of Sigmund Freud
 
{{Infobox film
| name           = The Secret Diary of Sigmund Freud
| image          = 
| caption        = 
| director       = Danford B. Greene
| producer       = Milos Antic Wendy Hyland Peer J. Oppenheimer
| writer         = Linsa Howard Roberto Mitrotti
| starring       = Bud Cort Carol Kane Klaus Kinski
| music          = Vojkan Borisavljevic
| cinematography = Djordje Nikolic
| editing        = Frank Mazzola	
| distributor    = 20th Century Fox
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
}}

The Secret Diary of Sigmund Freud is a 1984 American comedy film directed by Danford B. Greene and starring Bud Cort.   

==Cast==
* Bud Cort - Sigmund Freud
* Carol Kane - Martha Bernays
* Klaus Kinski - Dr. Max Bauer
* Marisa Berenson - Emma Herrmann
* Carroll Baker - Mama Freud
* Ferdy Mayne - Herr Herrmann
* Dick Shawn - The Ultimate Patient
* Nikola Simic - Papa Freud
* Rade Marković - Dr. Schtupmann
* Stevo Žigon - Professor Eberhardt
* Borivoje Stojanović - Professor von Schmertz
* Janez Vrhovec - Professor Gruber
* Frankie LaPlaca - Young Siggy

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 

 