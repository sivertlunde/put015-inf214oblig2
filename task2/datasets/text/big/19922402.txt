Venus in Furs (1969 Franco film)
 
{{Infobox film
| name           = Venus in Furs
| image          = Paroxismus.jpg
| caption        = Poster under Italian title
| director       = Jesús Franco
| producer       = Harry Alan Towers
| writer         = Milo G. Cuccia Carlo Fadda Jesús Franco Bruno Leder Malvin Wald
| starring       = James Darren Stu Phillips
| cinematography = Angelo Lotti
| editing        = Henry Batista Michael Pozen
| distributor    = American International Pictures
| released       =  
| runtime        = 86 minutes
| country        = Italy West Germany
| language       = English
| budget         =
| gross          =
}}
 erotic thriller film directed by Jesús Franco and starring James Darren.   

The film (also known as Paroxismus and Black Angel) bears only a superficial resemblance to the 1870 Venus in Furs novel by Leopold von Sacher Masoch. The title and character names in Francos original script were changed to the novels for commercial reasons. Francos film is a surreal supernatural erotic thriller about unattainable love and how far one is willing to go for the person they desire.  It is not a study in masochism as portrayed in the novel. 

==Plot==
James Darren plays a jazz musician who becomes obsessed to the point of madness with the mysterious fur-clad Wanda (Maria Rohm), only to find her dead body washed up on the beach.

==Cast==
* James Darren as Jimmy Logan
* Barbara McNair as Rita
* Maria Rohm as Wanda Reed
* Klaus Kinski as Ahmed Kortobawi
* Dennis Price as Percival Kapp Margaret Lee as Olga
* Adolfo Lastretti as Insp. Kaplan (as Aldo Lastretti)
* Jesus Franco as Jazz musician (uncredited)
* Manfred Mann as Jazz musician (uncredited) Paul Muller as Hermann (uncredited)
* Mirella Pamphili (uncredited)

==Reception==
The New York Times wrote a negative a review of Venus in Furs on its initial release, stating that the film "features much inept fancy moviemaking (including echoes of "La Dolce Vita" and even "Vertigo (film)|Vertigo"), some semi-nudity, and virtually endless confusion". {{cite news first       = Roger last        = Greenspun title       = Venus in Furs (1969) The Screen: 2 Rated R:Swappers Shares Bill With Venus in Furs url         = http://movies.nytimes.com/movie/review?_r=1&res=9402E7D91731EE34BC4852DFBF66838B669EDE&scp=3&sq=Venus%20in%20Furs&st=cse work        = The New York Times location    = New York date        = 10 September 1970 accessdate  = 18 July 2009 quote       =
}}  Glenn Erickson was more positive. {{cite web
|url=http://www.dvdtalk.com/dvdsavant/s1560venu.html
|first=Glenn
|last=Erickson
|title=DVD Savant Review: Venus in Furs
|date=27 March 2005
|accessdate=29 January 2014
}} 

==See also==
* Klaus Kinski filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 