Lady Snowblood 2: Love Song of Vengeance
{{Infobox film
| name = Lady Snowblood 2: Love Song of Vengeance 修羅雪姫 怨み恋歌
| image = Lady Snowblood 2 Love Song of Vengeance.jpg
| caption = Poster Toshiya Fujita
| producer = Kikumaru Okuda
| writer = Kazuo Uemura Kazuo Koike
| starring = Meiko Kaji Juzo Itami
| music = Kenjirō Hirose
| cinematography = Masaki Tamura
| editing =
| distributor = Toho
| released =  
| runtime = 89 minutes
| country = Japan
| language = Japanese
| budget =
| gross =
}} Toshiya Fujita Shurayukihime and Lady Snowblood.

==Story==
Kashima Yuki is surrounded by policemen on a beach. She fights and kills several of them but is overwhelmed. She is quickly tried and sentenced to death by hanging, but suddenly rescued by the mysterious Kikui Seishiro, head of Secret Police. Inside his headquarters, he propositions Kashima to spy on an "enemy of the State", the anarchist Tokunaga Ransui, and retrieve a book. Ransui is in possession of a critical document which Kikui seems quite obsessed with, deeming it highly dangerous to the stability of the government. If Kashima can obtain and deliver the document to Kikui, he will grant her immunity from her charges.

Kashima infiltrates Ransuis home posing as a maid, and sets about looking for the document. But the more she observes Ransui, the more she questions the path Kikui has put her on. When Ransui confides in Yuki, knowing full well who she is, asking her to deliver the document to his brother Shusuke, Kashima will be forced to decide her allegiance. 

==Cast==
* Meiko Kaji - Shurayuki-hime
* Juzo Itami - Ransui Tokunaga
* Kazuko Yoshiyuki - Aya Tokunaga
* Yoshio Harada - Shusuke Tokunaga
* Shin Kishida - Seishiro Kikui
* Toru Abe - Terauchi Kendo

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 

 
 