Bulletproof (1996 film)
 
{{Infobox film
| name           = Bulletproof
| image          = Bulletprooftp.jpg
| caption        = Bulletproof poster
| director       = Ernest R. Dickerson
| producer       = Bernie Brillstein
| screenplay     = Joe Gayton Lewis Colick
| story          = Joe Gayton
| starring       = Damon Wayans Adam Sandler James Farentino James Caan
| music          = Elmer Bernstein Steven Bernstein
| editing        = George Folsey Jr.
| distributor    = Universal Pictures      
| released       = September 6, 1996
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = $25 million
| gross          = $22,611,954
}}
 Jeep Swenson.  It is Adam Sandlers last film from Universal Pictures until 2007s I Now Pronounce You Chuck and Larry.

==Plot== James Caan). He is unaware that his best friend Rock Keats (Damon Wayans) is actually an LAPD undercover cop, with a real name being Jack Carter, who is seeking evidence against Colton, and he befriended Moses only to infiltrate Coltons gang.

One night, Jack goes out with Moses and gets him drunk, and manages to get info from Moses, and he finds an address for one of Coltons fronts. During a raid on Coltons warehouse, Archie realizes that Jack is a cop, and he tries to escape the warehouse during the police raid, during which an out of control crane hits Moses in the back and he accidentally shoots Jack in the head, but he miraculously survives and makes a full recovery with the aid of his physical therapist Dr. Traci Flynn (Kristen Wilson). Moses then flees the state, and is subsequently found and arrested later on. Moses is brought into custody, and he agrees to testify against Colton, but the trial is at the other side of the country. After Jacks recovery, his superior officer Jensen (James Farentino) orders him to get Moses to transfer him personally to the courtroom.

Having been bitter about the accident, Carter harbors resentment against Moses, and problems escalate since a simple transfer has gone awry when Colton learns through bribed federal agents and Los Angeles Police Department officers of Mosess attempt to testify against Colton. Carter and Moses slowly mend their friendship and are successful in returning to their local police department. However, Colton apparently holds Flynn hostage, and blackmails Carter into turning Moses over in order to save Flynn. Carter and Moses pretend to comply with Colton and shoot their way through Coltons guards. It is later revealed that Flynn was responsible for leaking Carters and Moses whereabouts to Colton and was actually on Coltons payroll. Carter manages to distract Colton after Moses gets shot in the shoulder by Colton and after apprehending Flynn and Moses manages to kill Colton. Moses gives the incriminating documents on Colton to Carter, and he heads to Mexico, where he becomes a bullfighter, with Carter and Moses mother later accompanying him.

==Cast==
* Damon Wayans as Rock Keats/Jack Carter
* Adam Sandler as Archie Moses
* James Caan as Frank Colton Jeep Swenson as Bledsoe
* James Farentino as Capt. Will Jensen
* Kristen Wilson as Traci Flynn
* Bill Nunn as Finch
* Allen Covert as Detective Jones Larry McCoy as Detective

==Reception== Tom Arnold and Pauly Shore.

Ernest Dickerson was very critical about the movie and says: "Theres a movie I did a couple years ago called Bulletproof. Id like to just erase that whole experience. You know, Im proud of a lot of the films Ive done, but theres some situations that happened that in retrospect maybe I could have handled them a little differently if I had been a little smarter about it. But thats all second-guessing. Bulletproof was a case where we shot an R-rated film and the studio made us cut it to PG-13. So Bulletproof is a big sore point with me because it’s the only time I’ve ever kicked a hole in an editing room wall. Because of the frustration of dealing with the studios and dealing with a producer who when the studio said “Jump!” he said “How high?” You make a movie with an R-rating, and then they tell you to cut it to PG-13. So it meant butchering performances, storylines, and really simplifying the film. The story is scripted by Lewis Colick, who is much deeper. It was about the responsibilities of friendship. The relationship that Damon Wayans had with Kristen Wilson was much stronger and all that was taken out. The movie was castrated."

==Box office==
Bulletproof grossed $6,014,400 its opening weekend placing it at #1 at the box office.   By the end of its theatrical run, it pulled in $21,576,954 in North America, and $1,035,000 internationally for a worldwide total of $22,611,954, falling short of recouping its $25 million budget.   

==Soundtrack==
 
A soundtrack containing mostly hip hop and R&B music was released on September 3, 1996 by MCA Records. It reached #85 on the Billboard 200|Billboard 200 and #23 on the Top R&B/Hip-Hop Albums.

In addition, Varese Sarabande released an album of Elmer Bernsteins score.

# Buddies (:59) 
# The Bust (3:17) 
# Shots (1:05) 
# Gurney (:37) 
# Flying (2:56) 
# In The Desert (2:18) 
# Cliff (2:02) 
# Phone (4:25) 
# Darryls Rescue (3:13) 
# Fighting (1:06) 
# Thugs And Hugs (1:22) 
# Mistakes (3:36)

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 