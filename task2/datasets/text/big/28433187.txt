The Victim (2011 film)
 
{{Infobox film
| name           = The Victims
| caption        = Film poster, North America
| image	         = The Victim FilmPoster.jpeg
| director       = Michael Biehn
| producer       = Jennifer Blanc Lucien Flynn Lorna Paul Travis Romero
| writer         = Michael Biehn Reed Lackey Denny Kirkwood Danielle Harris
| music          = Jeehun Hwang
| cinematography = Eric Curtis
| editing        = Vance Crofoot
| studio         = Anchor Bay
| distributor    = BlancBiehn Productions
| released       =  
| country        = United States
| language       = English
| budget         = $800,000
}}

The Victim is a 2011 American horror film directed, written and starring Michael Biehn    and produced and co-starring Jennifer Blanc.   

The film was produced and shot in less than two weeks  in Los Angeles in 2010. Most of the principal photography took place in the Topanga Canyon, close to Malibu, California.

==Cast==
* Michael Biehn as Kyle
* Jennifer Blanc as Annie
* Ryan Honey as Harrison
* Danielle Harris as Mary Denny Kirkwood as Coogan

==Reception==
  and Jennifer Blanc promoting the film during an August 23, 2012 appearance at Midtown Comics in Manhattan.]]
Alan Cerny of Aint It Cool News characterized The Victim as "a fun sleazy grindhouse film", in which Biehn was lauded for both giving a good performance and getting good performances out of his actors. Though Cerny stated that Biehns first-time director effort exhibited imperfections such as a driving montage scene that he felt was too long, he appreciated that Biehn understood the genre in which he was working, commenting, "Biehn has a clear path to what hes shooting for, and for much of the films running time, he gets it", and "Its a specific genre with a specific style, and working from that, Biehn gets way more right than he does wrong." 

The New York Times said, "Directing his own screenplay, Mr. Biehn (working from a story by Reed Lackey) pays more attention to genitals than spatial coherence, unaware that labeling a film grind house doesn’t excuse soap-opera emoting and laughable dialogue. Wait, what am I saying? Of course it does."   The film currently has a 35% rating on Rotten Tomatoes.

==References==
 

==External links==
 
*  
*  
*  

 
 
 
 
 
 
 


 