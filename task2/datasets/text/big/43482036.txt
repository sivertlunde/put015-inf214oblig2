Holding Hope
{{Infobox film
| name               = Holding Hope
| image              = Holding Hope poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Desmond Elliot
| producer           = Emem Isong   Uche Jombo
| story            = Emem Isong   Uche Jombo
| writer         = Uduak Isong Oguamanam
| starring       =  
| music          = Slam (Composer)   Austin Erowele (score)
| cinematography = Austin Nwaolie
| editing        = Uche Alexmoore
| studio         = Royal Arts Academy   Denziot Studios
| distributor   = Royal Arts Academy
| released       =  
| runtime        = 135 minutes
| country        = Nigeria
| language       = English
| budget         = 
| gross          =
}}
 Bursting Out and was met with mixed to positive critical reviews.  Nadia Buari and Moyo Lawal were nominated for "Best actress in a supporting role in an English film" and "Most Promising Act (female)" respectively at the 2012 Best of Nollywood Awards for their roles in the film.   

==Plot summary==
The film narrates the story of Mrs Badmus (Abiola Segun Williams) who owns a multimillion business empire (Da Costa Holdings), but her son Olumide (Desmond Elliot) is very wayward and is not interested in running the business. Mrs Badmus eventually reveals to her son that she is dying of cancer and she has few months left to live. She gives a condition that if he wants her to be happy, he needs to get married to Hope (Uche Jombo), the financial director of the company and a very dedicated employee. Mrs Badmus also feels Hope is the perfect wife that can keep her sons wasteful spending in check when she is no more. Olumide orchestrates a plan with her girlfriend, Sabina (Nadia Buari) and Olumide starts to fake his love for Hope, waiting for the day his mother will die. However, Mrs Badmus Will states that Olumide has to be married to Hope to have her properties; If anything otherwise happens, Hope should have the properties. Olumide has no other choice than to marry Hope, but however frustrates her with everything after wedlock, while still going out with Sabina. Hope, who continues to play a good and faithful wife despite her husbands attitude, finds out that she is also suffering from Leukaemia. Olumide eventually falls deeply in love with Hope, but it is too late because she now has few months left to live.

==Cast==
*Desmond Elliot as Olumide Sydney Badmus
*Nadia Buari as Sabina
*Uche Jombo as Hope
* Ngozi Nwosu as Sabinas mother
*Abiola Segun Williams as Mrs Badmus
*Rukky Sanda as Chioma
*Moyo Lawal as Hopes Cousin
*Ken Odurukwe as Mr Rotimi
*Emeka Duru as Manager

==Production==
The script of Holding Hope was inspired by the account of three cancer survivors; Desmond Elliot and Uche Jombo believed so much in the film, so they both joined Emem Isong to executively produce the film.    Speaking on the film, Jombo states: "we want to make people cry, laugh and perhaps pause and think at the same time".     Uche Jombo had to loose a significant amount of weight to play the role of Hope in the film;   She had to go on a diet and practically "starve" herself. During the course filming, she was made to sleep for only about 2 hours a day, so she can have a natural pale appearance, some little makeup effects were added on her as well. She also worked out excessively and was able to burn up to 50 pounds of fat. Jombo stepped out on some redcarpet events before the film was officially announced which sparked rumours about her and her health status.     The film was shot in Lagos. One of the cancer patients whose story inspired the making of the film and is being described in one of the characters died during the course of filming. 

==Release== Bursting Out, Guilty Pleasures US premiere on 5 September 2010 in Dallas, Texas.  

==Reception==

===Critical reception===
The film received mixed to positive reviews. It was mostly praised for its cinematography, the emotional message, music and Uche Jombos performance, while criticized for its bad sound quality, poor editing, occasional inappropriate music score and generally poor production.       Nollywood Reinvented gave a 52% rating, praised Uche Jombos performance, the films soundtrack and commented: "For the most part, it   was made up of motifs that we had seen before. There were very obvious signs of potentially great cinematography within this movie, but I couldn’t understand why suddenly the color and hue would just change when it wasn’t a flashback, dream or anything of the sort, the voices were also out of sync a lot. Slam is a musical genius and Ego’s “Bia Nulu”" was amazing!  NollywoodForever.Com gave 84% rating, praised the cinematography and concluded: "I was taken on an emotional roller coaster ride from feeling anger at the way that Olumide was treating his wife, to sadness for her and then happy when the relationship was back on track. I loved Uche and Desmond acting alongside each other, there is a nice easy comfortable chemistry that translates perfectly on screen".  Aghwana Amelia gave a rating of 8 out of 10, stating: "Holding Hope is a movie with a lot of morals. The director is very creative using different types of angles, shots, effects, movement and not limiting himself. The actors were able to show good acting skills, which portrayed the characters as they should be seen". 

===Accolades===
Nadia Buari was nominated for "Best actress in a supporting role in an English film" at the 2012 Best of Nollywood Awards, while Moyo Lawal was also nominated for "Most Promising Act (female)" at the same Awards. 

==Home Media== VOD platforms Iroko TV. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 