The Raging Moon
{{Infobox film
| name           = The Raging Moon
| image          =
| image_size     =
| caption        =
| director       = Bryan Forbes
| producer       = Peter Marshall Bryan Forbes
| narrator       = Georgia Brown Barry Jackson
| music          =
| cinematography = Tony Imi
| editing        = Timothy Gee EMI Elstree
| distributor    = EMI Films#MGM-EMI|MGM-EMI
| released       = 1971
| runtime        = 110 min.
| country        = United Kingdom
| language       = English
| budget         = £260,000 Bryan Forbes, A Divided Life, Mandarin Paperbacks, 1993 p 174, 180-183 
| preceded_by    =
| followed_by    =
}}
The Raging Moon is a British film released in the UK during 1971. It is based on the book by British novelist Peter Marshall and starring Malcolm McDowell and Nanette Newman. In the United States, this motion picture was released as Long Ago, Tomorrow.

Adapted and directed by Bryan Forbes (Newmans husband), this romance in wheelchairs was considered unusual in its time owing to the sexual nature of the relationship between McDowell and Newman, who play disabled people. It may have been the first film in which a person in a wheelchair is shown having sex.

The film received two Golden Globe nominations, for Best Foreign Film (English Language), and Best Song, "Long Ago Tomorrow".

==Plot==
 
Bruce Pritchard is a 24-year-old working-class man and amateur soccer player with a passion for life. All this changes when he suddenly finds himself struck down by an incurable degenerative disease and confined for the rest of his life to a wheelchair. He makes a self-imposed exile to a church-run home for the disabled, believing that it is best for his immediate family to forget about him the way he is now. His bitterness at his fate and his dislike of the rules and regulations of the place only serve to make him more withdrawn and angry at his enforced imprisonment. Then he gets to know a fellow patient, Jill Mathews, a 31-year-old woman from a wealthy family, also confined to a wheelchair through polio. Bruce begins to harbour romantic affections for Jill but before he can make his feelings known in a letter, she leaves the institution to return home and marry long-time fiancé Geoffrey. But Jill quickly realizes the relationship is half-hearted on Geoffreys part, and after breaking off the engagement she returns to the institution. Gradually she is able to get through Bruce’s shell of cynicism and lack of respect for authority, bringing back life to his existence. In the process, the two begin to fall in love and admit their feelings for each other, consummating a relationship. But their sexual encounter is physically dangerous for Jill, who dies the day after the couple makes love for the first time.

==Cast==
* Malcolm McDowell as Bruce Pritchard 
* Nanette Newman as Jill Matthews  Georgia Brown as Sarah Charles  Barry Jackson as Bill Charles 
* Gerald Sim as Reverend Carbett 
* Michael Flanders as Clarence Marlow 
* Margery Mason as Matron 
* Geoffrey Whitehead as Harold Pritchard 
* Chris Chittell as Terry 
* Jack Woolgar as Bruces Father 
* Patsy Smart as Bruces Mother
* Norman Bird as Dr. Matthews 
* Constance Chapman as Mrs. Matthews 
* Michael Lees as Geoffrey
* Bernard Lee as Uncle Bob 
* Geoffrey Bayldon as Mr. Latbury

==Production==
Producer Bruce Curtis initially tried to get up the project at Columbia, but was turned down. Dropping the Scalpel: Film Notes Columbia Frowns Speeds the Turnover Refuge From Roles
By Judith Martin. The Washington Post, Times Herald (1959-1973)   28 Feb 1969: B12. Turn on hit highlighting for speaking browsers by selecting the Enter buttonHide highlighting 

Bryan Forbes was in the unusual position of being able to green light his own film as he was head of production for EMI at the time. Forbes commented that he was highly criticised in some quarters for directing a film while running the studio, even though he did not take any extra salary as the director. Once the film was made some executives at EMI did not want it released but Forbes held a successful test screening which secured company support. 

==Reception==
The film was not a success at the box office. The eclipse of the moon man
Malcom, Derek. The Guardian, 26 March 1971: 15.  

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 