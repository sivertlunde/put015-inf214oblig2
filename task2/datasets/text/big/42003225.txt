Shed No Tears (2013 film)
{{Infobox film
| name               = Shed No Tears
| image              = 
| director           = {{Plainlist|
* Måns Mårlind
* Björn Stein}}
| producer           = {{Plainlist|
* Malcolm Lidbeck
* David Olsson}}
| based on           = Songs and lyrics by Håkan Hellström
| writer             = Cilla Jackert
| starring           = {{Plainlist|
* Adam Lundgren
* Jonathan Andersson
* Reine Brynolfsson
* Tomas von Brömssen
* Josefin Neldén
* Gunilla Nyroos
* Marie Richardson
* Disa Östrand}}
| music              = Adam Nordén
| cinematography     = Erik Sohlström
| editing            = Håkan Wärn
| studio             = Acne Drama SF
| runtime            = 
| country            = Sweden
| language           = Swedish
| released           =   }} SEK   
| gross              = 20,9 million SEK   }}

Shed No Tears ( ) is a 2013 Swedish film directed by Måns Mårlind and Björn Stein and starring Adam Lundgren and Jonathan Andersson. The plot is based on the lyrics and music by Swedish artist Håkan Hellström, and the script is written by Cilla Jackert. The film premiered on 19 July 2013  and was receiving critical acclaim. It won the Guldbagge Award for Best Sound Editing at the 49th Guldbagge Awards.   

The main role is played by Adam Lundgren, while Tomas von Brömssen, Gunilla Nyroos and Josefin Neldén appear in supporting roles. Even Håkan Hellström himself appears in a minor role.  The films title is based on Hellströms debut single "Känn ingen sorg för mig Göteborg", on the album with the same name.

The film received extensive good reviews, and 27,766 Swedes saw it at the cinema on the opening weekend, which took it up to Biotoppens second place.    The following week it had dropped down to sixth place,    and at the end of the year was the years fifth highest grossing Swedish film. 

==Plot==
Pål (Adam Lundgren)s biggest dream is to get involved with music. His childhood friends Lena (Josefin Neldén) and Johnny (Jonathan Andersson) knows this, and his grandfather Rolle (Tomas von Brömssen), although he most of all would see that Pål gained an orderly job. When Eva (Disa Östrand) turns up as a summer night and captures Påls attention, she discovers that she shares his dream. The problem is that the only thing standing between Pål and the dream is his own obsessions that once after another leads him to derail just when most are at stake.

==Cast==
 , together with the actors from the film. From the left: Håkan Hellström, Disa Östrand, Adam Lundgren, Josefin Neldén, Jonathan Andersson, Tomas von Brömssen and Gunilla Nyroos.]]
* Adam Lundgren as Pål
* Disa Östrand as Eva
* Josefin Neldén as Lena
* Jonathan Andersson as Johnny
* Tomas von Brömssen as Rolle, Påls grandfather
* Marie Richardson as Lisbeth Lindén
* Reine Brynolfsson as Bosse
* Gunilla Nyroos as Hopp
* Daniel Larsson as Isse Eric Ericson as Priest
* Kim Lantz as Denis
* Hanna Hedin Hillberg as Samba Dancer
* Gregers Dohn as Pusher
* Darko Savor as Thug
* Lucas Miklin as Hooligan
* Freddie Wadling as A-lagare
* Ebbot Lundberg as A-lagare
* Håkan Hellström as a Street musician

==Critical response==
The film received overwhelmingly positive reviews from Swedish critics. Aftonbladet called it "Beats the most ever seen in the Swedish film", and gave it a rating of 5 out of 5.  The rival newspaper, Expressen called it "Hellström knew what he was doing", and gave it a rating of 4 out of 5.  The film site Moviezine, called it a "Musical treat", and gave it a rating of 4 out of 5. 

==Awards and nominations==
===49th Guldbagge Awards===
For the 49th Guldbagge Awards, Shed No Tears was nominated for nine Guldbagge Award|awards, some of which it won in the categories Best Editing and Best Sound Editing:  

{| class="wikitable" style="text-align: left"
|----- bgcolor="#bfd7ff"
|----- bgcolor="#ebf5ff" 
!width="50"|Year
!width="300"|Category
!width="300"|Receiver(s)
!width="100"|Result
|- 2014
| Best Film
| Malcolm Lidbeck and David Olsson (producers)
|  
|- Best Director
| Björn Stein and Måns Mårlind
|  
|- Best Supporting Actress
| Josefin Neldén (as Lena)
|  
|- Best Screenplay
| Cilla Jackert
|  
|-
| Best Sound Editing
| Mattias Eklund
|  
|-
| Best Cinematography
| Erik Sohlström
|  
|-
| Best Costume Design
| Ulrika Sjölin
|  
|-
| Best Art Direction
| Wilda Wiholm
|  
|-
| Best Editing
| Håkan Wärn
|  
|}

===MovieZine Awards 2013===
MovieZine did a poll at the end of 2013, in which readers got to vote for winners in various categories with substance Movie Year 2013, which Shed No Tears won in the category Best Swedish film, with 49% of the 2555 votes that was sent in.  Adam Lundgren, also won the vote in the category Best Breakthrough for his role as Pål in the film. 

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 