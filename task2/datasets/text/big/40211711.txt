Farewell (1983 film)
{{Infobox film
| name           = Farewell (Russian: Прощание), Proshchanie)
| director       = Elem Klimov
| writer         = German Klimov, Larisa Shepitko, Rudolf Tyurin
| starring       = Stefaniya Staniyuta Lev Durov Alexey Petrenko Leonid Kryuk Vadim Yakovenko Yuri Katin-Yartsev
| music          = Vyacheslav Artyomov, Alfred Schnittke
| cinematography = Vladimir Chukhnov, Aleksei Rodionov, Yuri Skhirtladze, Sergei Taraskin
| editing        = Valeriya Belova
| studio         = Mosfilm
| distributor    = Sovexportfilm
| released       = 1983
| runtime        = 112 minutes
| country        = Soviet Union
| language       = Russian
}} Soviet film based on Valentin Rasputins novel Farewell to Matyora and directed by Elem Klimov.

== Plot ==

The existence of the village of Matyora, located on a small island of the same name, is threatened with flooding by the construction of a dam to serve a hydroelectric power plant. The villagers oppose their displacement and the loss of their traditions, but are eventually forced to bid farewell to their homeland.

== Production history ==

While scouting locations in June 1979 for her planned adaptation of the ecological fable, original director Larisa Shepitko died in a car accident along with four members of her shooting team. After a delay the project was finally completed in 1981 by her widower Elem Klimov and although shelved for a further two years, was eventually given a limited release in the Soviet Union in 1983. Originally chosen to open the 1984 Berlin Film Festival, it was initially refused an export licence until three years later when it was screened in Berlin.

==External links==
*  
* 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 
 