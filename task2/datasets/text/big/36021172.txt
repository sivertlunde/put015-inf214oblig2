La revolución es un sueño eterno
{{Infobox film
| name           = La revolución es un sueño eterno
| image          = 
| alt            = 
| caption        = 
| director       = Nemesio Juárez
| producer       = 
| writer         = Nemesio Juárez
| based on       =   
| starring       = Lito Cruz Juan Palomino Luis Machín
| music          = 
| cinematography = 
| editing        = 
| studio         = INCAA and San Luis Cine
| distributor    = 
| released       =  
| runtime        = 
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}
La Revolución es un sueño eterno ( ) is a 2012 Argentine film, based on the eponymous book by Andrés Rivera. It is based on the life of Juan José Castelli, who led the May Revolution and the early steps of the Argentine War of Independence.

The main actor is Lito Cruz as Juan José Castelli. Other actors are Juan Palomino as Bernardo Monteagudo, Luis Machín as Manuel Belgrano, Adrián Navarro as Mariano Moreno and Edward Nulkievwicz as Agrelo.     

It is a joint production of INCAA and San Luis Cine. Most of the movie was shot in the San Luis Province, including a stage similar to the 1810 Buenos Aires Cabildo, and the speech of Castelli to the natives of the Upper Peru was filmed at the Sierra de las Quijadas National Park. The scenes of the British invasions of the Río de la Plata were filmed in Luján, Buenos Aires Province|Luján. 

It was filmed in 2007, and the filming took seven weeks. 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 


 