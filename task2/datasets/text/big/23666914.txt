The Thief of Venice
{{Infobox film
| name           =Il Ladro di Venezia
| image          =
| image_size     =
| caption        =
| director       =John Brahm
| producer       =Robert Haggiag Dario Sabatello
| writer         = John Brahm Salvatore Cabasino Jesse Lasky Jr. based on = story by Michael Pertwee
| narrator       = Paul Christian Massimo Serato Faye Marlow Aldo Silvani
| music          =Alessandro Cicognini
| cinematography = Anchise Brizzi
| editing         =Renzo Lucidi Terry Morse studio = Sparta Films
| distributor    =20th Century-Fox (US)
| released       =1950 (Italy) November 1952 (US)
| runtime        =91 min
| country        = Italy US Italian English
| budget         = $3 million  gross = 1,745,680 admissions (France) 
}}
 1950 Italy|Italian film directed by John Brahm. The US title was "The Thief of Venice".

It was released in the US two years after being made. 
==Plot==
In Venice during the Middle Ages, a beautiful tavernkeeper finds herself caught up in intrigue and a war between Italy and Turkey. Naval officer Christian turns into a thief to oppose craft Serato.
==Cast==
*Maria Montez	  ... 	Tina
*Paul Hubschmid	  ... 	Alfiere Lorenzo Contarini
*Massimo Serato	  ... 	Scarpa the Inquisitor
*Faye Marlowe	  ... 	Francesca Disani
*Aldo Silvani	  ... 	Capt. von Sturm
*Luigi Saltamerenda	  ... 	Alfredo
*Guido Celano	  ... 	Polo
*Umberto Sacripante	  ... 	Durro
*Camillo Pilotto	  ... 	Adm. Disani
*Ferdinando Tamberlani...	Lombardi
*Liana Del Balzo  	  ... 	Duenna
*Paolo Stoppa	  ... 	Marco
*Mario Tosi 	  ... 	Mario
*Vinicio Sofia	  ... 	Grazzi
*Leonardo Scavino	  ... 	Sharp Eye
==Production==
The movie was an Italian-American co production. Revue Beckoning Webb; Lesser Planning Series; Rains Barricade Star
Schallert, Edwin. Los Angeles Times (1923-Current File)   17 Mar 1949: 23.   Originally Edmond OBrien and his wife Olga San Juan were mentioned as possibly starring in the movie which was being produced by Monte Schaff and Lou Appleton. MOVIELAND BRIEFS
Los Angeles Times (1923-Current File)   09 June 1949: B11.   Douglas Fairbanks Jnr was also mentioned as a possible lead. Israel Bids for Adler, Muni and Hecht Play; Rogers Seeks Star Packet Paul Christian would star. Jesse Lasky Jr. Plans Production in Europe; Bromfield Gets New Deal
Schallert, Edwin. Los Angeles Times (1923-Current File)   12 Aug 1949: A7.  

John Brahm signed to direct and Faye Marlowe and George Sanders were to play support roles, with filming to start in Italy on 1 November 1949. Faye Marlowe Returning as George Sanders Lead; Ball-Arnaz Deal Sighted
Schallert, Edwin. Los Angeles Times (1923-Current File)   26 Oct 1949: 23.   Sanders eventually pulled out. Corey Pursues Romantic Course in Furies; Italy Expedition Launching
Schallert, Edwin. Los Angeles Times (1923-Current File)   28 Oct 1949: 21.  

The movie was shot on location in Italy with studio work done at Scalera Studios. Of Local Origin
New York Times (1923-Current file)   24 Nov 1952: 19.  
==Reception==
The Christian Science Monitor said that "a series of coups, captures and escapes take place with a great deal of running about but very little inventiveness." Maria Montez Seen in Film Made in Italy
R.N.. The Christian Science Monitor (1908-Current file)   29 Jan 1953: 6.  

The Washington Post called it "a rip snorting Western" style film. Thief of Venice Provides Lots of Action -- In 1575
By Dorothea Pattee Post Reporter. The Washington Post (1923-1954)   14 Mar 1953: 4.  
==References==
 
==External links==
*  at IMDB

 

 
 
 
 
 
 
 

 