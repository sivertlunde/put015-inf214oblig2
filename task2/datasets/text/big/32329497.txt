Wanted: A Husband
{{infobox film
| name           = Wanted: A Husband
| image          = Billie Burke in Wanted a Husband by Lawrence Windom Film Daily 1920.png
| imagesize      =
| caption        = Ad for film
| director       = Lawrence C. Windom
| producer       = Adolph Zukor Jesse Lasky
| based on       =  
| writer         = Clara Beranger(scenario)
| starring       = Billie Burke
| cinematography = Roy Overbaugh
| editing        =
| distributor    = Paramount Pictures|Paramount-Artcraft
| released       =  
| runtime        = 5 reel#Motion picture terminology|reels; 4,596 feet
| country        = United States
| language       = Silent film (English intertitles)

}}
Wanted: A Husband is a 1919 silent film comedy starring Billie Burke. It was produced by Famous Players-Lasky and distributed by Paramount Pictures|Paramount-Artcraft.  The film is based on the short story "Enter DArcy" by Samuel Hopkins Adams. The relatively unknown Lawrence C. Windom directed this lost film.  

==Cast==
*Billie Burke - Amanda Darcy Cole
*James Crane - Jack Remsen
*Margaret Linden - Gloria Green Charles Lane - Tom Harmon 
*Edward Lester - Paul Wood
*Bradley Barker - Holcomb Lee
*Helen Greene - Maude Raynes
*Gypsy OBrien - Helen Bartlett
*Kid Broad - Andy Dunn
*Mrs. Priestly Morrison - Veronica
*Frank Goldsmith - Hiram

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 


 