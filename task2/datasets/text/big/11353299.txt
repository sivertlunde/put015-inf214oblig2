Disney Princess Enchanted Tales: Follow Your Dreams
 
{{Infobox film
| name           = Disney Princess Enchanted Tales: Follow Your Dreams
| image          = Disneyprincessenchantedtalescase.jpg
| caption        = DVD Cover David Block
| producer       = Kurt Albrecht Douglas Segal
| writer         = Shirley Pierce
| narrator       = Susan Blakeslee Jeff Bennett Roger Craig Smith Russi Taylor Tress MacNeille Tara Strong Zack Shada Flo Di Re Frank Welker
| music          = Jeff Danna (score) Amy Powers Russ DeSalvo Denise Gruska Shirley Pierce
| editing        = Kevin Locarro
| studio         = DisneyToon Studios 
| distributor    = Walt Disney Home Entertainment
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 animated feature produced by DisneyToon Studios.  It was the first film in a planned new Disney Princess Enchanted Tales series of direct-to-video films, each featuring new stories about the Disney Princesses. It was released on September 4, 2007 by Walt Disney Studios Home Entertainment.
 Princess Aurora Sleeping Beauty and Princess Jasmine from 1992s Aladdin (1992 Disney film)|Aladdin.

== Plot ==
 

==Production== Belle of Beauty and Cinderella story Mulan story. It also was never released, due to poor sales of Follow Your Dreams.    Antagony & Ecstasy argues that this specific project was the catalyst for newly appointed Chief Creative Officer for Disney animated projects John Lasseter shutting down and halting all DisneyToon Studios sequel projects that werent too far into production. 

==Segments==

===Keys to the Kingdom=== Sleeping Beauty. King Stefan Queen Leah King Hubert Jeff Bennett), Prince Phillip The good fairies, Flora (Barbara Dirikson), Fauna (Russi Taylor), and Merryweather (Tress MacNeille), offer to help her, but Aurora declines their offer, because she believes she can do it alone. Merriweather gives Aurora her wand in case she needs any assistance and warns her to be very careful with it. Her tasks include planning banquets, dealing with peasants, and organizing servants who look after the kitchens and the gardens. Aurora believes she can do her job without the use of the wand, but later at night before bed, Aurora cant help but play with it and uses magic to make herself a big yellow ballgown. Eventually, after a long day dealing with complaints, Aurora gives in and uses the wand to help a local farmer in need of new chickens and pigs. Her magical inexperience leads to unusual consequences, including massive chickens, green pigs, and transforming the farmer into a duck. After she realizes that using the wand was a mistake, she promptly comes up with ideas to solve the problems on her own.

===More Than a Peacock Princess=== The Sultan Jeff Bennett) Rajah (Frank Abu (Frank Welker), and Iago (Aladdin)|Iagos (Gilbert Gottfried) help, to find Sahara and return him to the Palace.

==Song numbers==
* Keys To The Kingdom - Words and Music by Amy Powers and Russ DeSalvo, Performed by Cassidy Ladden
* Peacock Princess - Words and Music by Amy Powers and Russ DeSalvo, Performed by Lea Salonga and Gilbert Gottfried
* Ive Got My Eyes On You - Words and Music by Amy Powers and Russ DeSalvo, Performed by Lea Salonga

==Critical reception==
Common Sense Media assessed that the film had "perseverance lessons for princess fans ages 3-6" and gave it a rating of 2 out of 5 stars. It noted the prevalent themes of "follow your dreams and never give up", the "plucky, brave and determined" role model nature of the princess protagonists, and the notion that "as a Disney property, this film inevitably works as brand reinforcement for the Disney Princess line of products."  CineMagazine gave the film a rating of 2 out of 5 stars, noting: "It is unfortunate that the two stories have such varying quality. If it had been a little more balanced then   become a great movie. Now it remains weak due to the Sleeping Beauty segment being entirely mediocre and barely worthy of Disney". It concluded that this project was focused on turning a profit than upholding artistic integrity". 

Antagony & Ecstasy described it as "the first in an aborted attempt to create a new series of cheap-even-by-the-standards-of-cheapquels videos", and concluded "I cannot entirely hate this dreadful little cast-off. Its too short; its too ebulliently random; and it might very well be the reason that the Disney sequels were finally strangled to death."  AnimatedReviews said "This is Disney Product with a capital P"    and "I thought Disney had turned a corner in getting away from this low-level quality, but this is just poor, poor, poor".  It added "Personally, I’d like to see this kind of thing where it belongs" which is on a television show called "Disney Princesses, with a new episode with a different Princess every time", as opposed to dressing up things like this, Cinderella II, and Belle’s Magical World as movies. 

DVDizzy said "It is hard to praise a pairing of two half-hour "movies", created with standards not much higher than those of a Saturday morning cartoon, that are being marketed as a full-length movie"  In a review of the DVD, InsidePulse said "The special features with the games are aimed at girls and Lord knows you won’t enjoy them unless you’re under the age of...6 years."  It added that it "does provide a modicum in fun in that it lets us see these winning characters again and more of their lives. But in contrast to the excitement and entertainment of their big screen outings, their lives here are a bit boring and didactic." 

Mary Costa, the original voice of Aurora, was not fond of this film and felt that it did not work. 

===Awards===
 
|- 2008
| Amy Powers, Russ DeSalvo, Jeff Danna
| Annie Award for Music in a Feature Production
|   
|}

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 