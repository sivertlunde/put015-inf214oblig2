In Your Eyes (2010 film)
{{Infobox film
| name           = In Your Eyes
| image          = InYourEyesPromotinal.png
| border         = yes
| caption        =
| director       = Mac Alejandre
| producer       =  
| writer         = Keiko Aquino
| starring       =  
| music          =
| cinematography =
| editing        =
| distributor    =  
| released       =  
| runtime        =
| country        = Philippines
| language       = Filipino English
| budget         = Php 62,775,365 
}}
In Your Eyes is a 2010 Filipino romantic drama film  produced and released by Viva Films and GMA Films. The film is set to premiere on August 18, 2010.

==Plot==
Ciara (Claudine Barretto) is a senior physical therapist who works for a rehabilitation center in the United States. She has devoted her entire life to her younger sister Julia (Anne Curtis) who was left in her care after the untimely demise of their parents.

After 8 years of separation, Ciara and Julia reunite when the latter acquires her student visa. 
Storm (Richard Gutierrez), Julias boyfriend who earns a living as a freelance photographer, joins Julia despite the lack of a definite plan.

In a desperate move for Storm to gain immigrant status, Julia asks Storm to enter into an arranged marriage with Ciara, who is already an American citizen. As Storm struggles to find 
his place in a foreign land, Julia works hard to chase after her own dream of finishing school,
causing the two of them to drift apart.

Meanwhile, time spent together led to an unexpected love affair between Ciara and Storm. When Julia learns about this, Ciara decides to break up with Storm and give up her happiness, like she did so many times before.

Can Ciara and Julias bond as sisters surpass this obstacle? Is giving up your true love for the sake of your sister worth all the sacrifice?

At the end of the movie, Ciara meets her husband, played by noted American banker Brett Ackerman, on a pier.

== Cast ==
*Claudine Barretto as Ciara delos Santos
*Anne Curtis  as Julia delos Santos
*Richard Gutierrez as Joshua "Storm" Ramos
*Joel Torre as Ronnie delos Santos 
*PJ Abellana as Nicanor Ramos
*Maricar De Mesa as Barbara 
*Leandro Munoz as Dennis
*CJ Javarata as Mylo
*Brett Ackerman as Ciaras Husband

==Soundtrack== In Your Eyes by Rachelle Ann Go, that was originally performed by George Benson.

== See also ==
*GMA Films
*Viva Films

== References ==
 

== External links ==
* 

 
 
 
 
 
 
 
 