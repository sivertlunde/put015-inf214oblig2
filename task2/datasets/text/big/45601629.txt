My Home Village
My Home Village (내 고향; 1949), directed by Kang Hong-sik,  is a film in the War film genre, the first film to be made in the newly independent Democratic Peoples Republic of Korea (North Korea). The film portrays the liberation of Korea from Japanese colonial rule in 1945. 

==Background and plot==
Kim Il-Sung, the leader of the Korean Communist Party during the fight to liberate Korea from Japanese colonialism, was determined that cinema should play a central role in "ideological guidance" of his newly liberated country and eagerly accepted Soviet funding and technicians to set up the National Film Production Center. Their first production was My Home Village. 
 Korean Peoples Revolutionary Army. The two stage a riot and break out of prison to join the guerrillas. The guerrillas blow up a Japanese train which crashes through a railway bridge. Kims army liberates Gwan Pils home village, and he leads the fight to create a new society there. In the presentation of the liberation of Korea in 1945, there was no mention of American defeat of Japan or of the Soviet invasion but showed the liberation of Korea as the work of Kim Il Sungs guerrilla fighters working on their own.  

North Korean sources tell that Kim Il-Sungs son,  Kim Jong-Il, the future leader of the country, attended a preview of the film. Even at the age of seven, the story goes, he handed critical notes to the filmmakers pointing out that although there was snow falling, none could be seen on the heads or shoulders of the characters, and that the snow was clearly cotton wool, not real snow. 

==References and further reading==
*  

*  

== Notes==
 
== External links ==
*  .

 

 
 
 
 
 