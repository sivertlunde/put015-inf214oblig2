Portrait Werner Herzog
{{Infobox film
| name           = Portrait Werner Herzog
| image          = herzog eisner.jpg
| caption        = Herzog with Lotte Eisner in Portrait Werner Herzog
| director       = Werner Herzog
| producer       =
| writer         = Werner Herzog
| narrator       = Werner Herzog
| starring       = Werner Herzog Reinhold Messner Lotte Eisner
| music          =
| cinematography = Jörg Schmidt-Reitwein
| editing        = Maximiliane Mainka
| distributor    =
| released       =
| runtime        = 28 min.
| country        = West Germany German  English
| budget         =
}}

Portrait Werner Herzog ( ) is an autobiographical short film by Werner Herzog made in 1986. Herzog tells stories about his life and career.
 Signs of Heart of Fata Morgana, Aguirre, the Wrath of God, The Great Ecstasy of Woodcarver Steiner, Fitzcarraldo, and the Les Blank documentary Burden of Dreams. 

Notable is footage of a conversation between Herzog and his mentor Lotte Eisner, a photographer. In another section, he talks with mountaineer Reinhold Messner, in which they discuss a potential film project in the Himalayas to star Klaus Kinski. 

== External links ==
*  

 

 
 
 
 
 
 
 
 


 