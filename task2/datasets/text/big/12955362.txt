The Lottery Man
 
{{Infobox film
| name           = The Lottery Man
| image          = The Lottery Man.jpg
| caption        =
| director       = Leopold Wharton Theodore Wharton
| producer       = F. Ray Comstock Leopold Wharton Theodore Wharton
| writer         = Theodore Wharton Rida Johnson Young
| starring       = Thurlow Bergen
| music          =
| cinematography = Levi Bacon
| editing        = Bess Spencer Whartons Studio
| released       =  
| runtime        =
| country        = United States English intertitles
| budget         =
}}
 silent comedy Whartons Studio in Ithaca, New York. A print of the film exists in the film archive of the Library of Congress.   

==Cast==
* Thurlow Bergen - Jack Wright-a son
* Elsie Esmond - Miss Helen Heyer-foxeys cousin
* Carolyn Lee - Lizzie Roberts-Mrs Peytons pet goat
* Allan Murnane - Foxey Peyton-his chum
* Lottie Alter - Mrs. Wright-little mother
* Ethel Winthrop - Mrs. Peyton-foxeys mother
* Mary Leslie Mayo - Hedgwig Jensen-physical instructor
* F.W. Stewart - Mcquire the newspaper publisher and the local constable
* Oliver Hardy - Maggie Murphy
* Edward OConnor - The Butler
* Malcolm Head - Vegetable cart man in accident
* Louis A. Fuertes - Lottery drawing host
* Clarence Merrick - Chauffeur
* Joseph Urband - Newspaper workroom clerk
* Frances White - Mrs. Peytons maid

==See also==
* List of American films of 1916
* Oliver Hardy filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 

 