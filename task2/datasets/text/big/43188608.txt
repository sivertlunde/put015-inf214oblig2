Naguva Hoovu
{{Infobox film name           = Naguva Hoovu image          =  image_size     =  caption        =  director       = R. N. K. Prasad producer       = R. N. Sudarshan writer         = Shylashri narrator       =  starring  Sudarshan Shylashri K. S. Ashwath music          = G. K. Venkatesh cinematography = R. N. K. Prasad editing        = Venkatraman   Ramesh studio         = Sri Sudarshan Chitralaya released       = 1971 runtime        = 139 mins country        = India language  Kannada
|budget         =  preceded_by    =  followed_by    = 
}}

Naguva Hoovu ( ) is a 1971 Indian Kannada language film directed by cinematographer R. N. K. Prasad,  produced by actor R. N. Sudarshan and written by actress Shylashri. Besides Sudarshan and Shylashri the film stars K. S. Ashwath, Ranga, R. Nagendra Rao and B. V. Radha in the pivotal roles.  The film won the National Film Award for Best Feature Film in Kannada at the 18th National Film Awards. 
The film had the musical score and soundtrack composed by G. K. Venkatesh to the lyrics of R. N. Jayagopal.

==Cast== Sudarshan
* Shylashri 
* Ranga
* K. S. Ashwath  
* R. Nagendra Rao
* B. V. Radha Balakrishna
* Ganapathi Bhat
* Thoogudeepa Srinivas
* Vijayashree
* B. Jayashree
* Master Chandrashekar
* Hanumanthachar
* Vijayakumar
* B. Jaya (actress)|B. Jaya

==Soundtrack ==
{| class="wikitable sortable"
|-
! Title !! Singers !! Lyrics 
|- Gulaabi Oh Gulaabi || P. Susheela ||  R. N. Jayagopal
|- Onde Ondu Hosa Haadu || P. B. Sreenivas, S. Janaki || R. N. Jayagopal
|- Ee Shubhadinade || P. Susheela ||  R. N. Jayagopal
|- Irabeku Irabeku Sudarshan ||  R. N. Jayagopal
|-
|}

==Awards==
* 18th National Film Awards|1970-71 - National Film Award for Best Feature Film in Kannada
; This film screened at IFFI 1992 Kannada cinema Retrospect.

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 


 