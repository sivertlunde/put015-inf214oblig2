Canada's Sweetheart: The Saga of Hal C. Banks
{{Infobox film
| name           = Canadas Sweetheart: The Saga of Hal C. Banks
| image          = 
| alt            =  
| caption        = 
| director       = Donald Brittain
| producer       = Donald Brittain Adam Symansky
| writer         = Donald Brittain Richard Nielsen Jason Dean
| music          = Eldon Rathburn
| cinematography = Andreas Poulsson
| editing        = Rita Roy Richard Todd
| studio         = 
| distributor    = 
| released       =  
| runtime        = 115 minutes
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}} Canadian docudrama, written and produced by Donald Brittain. It aired in 1985 on CBC Television.
 American labour union leader who came to Canada in 1949 to lead a sectarian fight between rival shipping unions. Banks left Canada in 1962 after being brought up on criminal charges.

Banks appeared before the Norris Commission, which was set up to investigate his strongarm tactics and links to beatings of opposition unions, and non signed shipping companies. In particular was the ULS and the Maritime Union run by his former lieutenant Michael Sheehan, who had testified before the Norris Commission against Banks, and led the push to loosen the SIU grip on the Great Lakes.

Maury Chaykin played the role of Banks in dramatic reenactments.

==Awards==
Winner of five awards, including Gemini Awards for best screenplay and best direction. 

==See also==
 

==External links==
* 
* 
*   

 

 

 

 
 
 
 
 
 
 
 
 
 


 