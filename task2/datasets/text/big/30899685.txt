The Lesser Evil (1998 film)
{{Infobox film
| name           = The Lesser Evil
| image          =
| caption        = David Mackay
| producer       = Daniel Helberg David Mackay
| writer         = Jeremy Levine and Stephan Schultze
| narrator       = Adam Scott Jonathon Scarfe Marc Worden Mason Adams
| music          =
| cinematography = Stephan Schultze
| editing        =
| distributor    =
| released       = 1998
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         =
}}
 David Mackay.

==Plot==
Four old friends, Derek (Colm Feore), George (David Paymer), Ivan (Arliss Howard), and Frank (Tony Goldwyn), are reunited when they become suspects in a double murder that took place twenty years before, when they were teenagers. The four covered it up and have now become successful professionals with seemingly normal lives. The story switches back and forth between the present day and the time of the killings. As the police investigation closes in on the four men, they turn on each other. It soon becomes clear that one of them will have to take the blame, or they will all be convicted.

==Cast==
*Colm Feore as Derek Eastman
*David Paymer as George
*Arliss Howard as Ivan Williams
*Tony Goldwyn as Frank OBrian
*Steven Petrarca as Young Frank Adam Scott as Young George
*Jonathon Scarfe as Young Derek
*Marc Worden as Young Ivan
*Jack Kehler as Detective Hardaway
*Mason Adams as Dereks father

==Reception== Variety gave a positive review, calling the film "a crafty and well-crafted drama",  and Film Threat praised it as "an exceptionally well told tale". 

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 


 