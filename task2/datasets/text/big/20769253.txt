Step Lively (1944 film)
{{Infobox film
| name           = Step Lively
| image          = Poster of Step Lively (1944 film).jpg
| image_size     = 250px
| alt            =
| caption        = Theatrical release lobby card
| director       = Tim Whelan
| producer       = Robert Fellows John Murray
| starring       = Frank Sinatra George Murphy Adolphe Menjou Gloria DeHaven Walter Slezak Eugene Pallette
| music          =
| cinematography = Robert De Grasse
| editing        = Gene Milford
| distributor    = RKO
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         =
}}
 Room Service, John Murray. This was a remake of the 1938 RKO movie "Room Service", starring the Marx Brothers, Lucille Ball, and Ann Miller. 

==Cast==
* Frank Sinatra - Glenn Russell
* George Murphy - Gordon Miller
* Adolphe Menjou - Wagner
* Gloria DeHaven - Christine Marlowe
* Walter Slezak - Joe Gribble
* Eugene Pallette - Simon Jenkins
* Wally Brown - Binion
* Alan Carney - Harry Grant Mitchell - Dr. Gibbs
* Anne Jeffreys - Miss Abbott Richard Davies

==Awards== Best Art Direction (Albert S. DAgostino, Carroll Clark, Darrell Silvera, Claude E. Carpenter).   

==See also==
* Frank Sinatra filmography

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 