Pranayamanithooval
{{Infobox film
| name           = Pranayamanithooval
| image          = Pranayamanithooval.jpg
| alt            = 
| caption        = 
| director       = Thulasidas
| producer       = P.K.R Pillai
| writer         = Kaloor Dennis   Thulasidas
| starring       = Jayasurya  Vineeth Kumar  Gopika  Harisree Asokan
| music          = Rajamani   Mohan Sitara
| cinematography = Anil Gopinath
| editing        =  G. Murali  
| studio         = 
| distributor    = 
| released       = 2002
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Pranyamanithooval ( ) is 2002 Malayalam film by Thulasidas starring Jayasurya, Vineeth Kumar and Gopika. This movie is inspired from the Tamil movie Nee Varuvai Ena. 

== Synopsis ==
This movie has as its central character, three young people - Vinod, Balu and Meera. Vinod(Vinu) (Jayasurya) is a smart young college student. Meera (Gopika) is Vinus girlfriend .Their marriage was almost fixed.But at that time Vinu dies in an accident and eyes where transferred to Balu (Vineet Kumar).Meera tries to see Balu for seeing Vinus eyes, but Balu mistook her and thinks that she loves him . Meera tells him the truth.Now his friends give him a new idea to make Meera love with him. They tells him to have a drama as he is going to commit suicide by jumping from a building but while standing there Balu accidentally fall down and was admitted in the hospital.doctor tells that Balu was dead but after some time they realizes that he was not died. At last Meera falls in love with Balu.

==Cast==
*Jayasurya ...  Vinod 
*Vineeth Kumar ...  Balu 
*Gopika ...  Meera 
*Harisree Asokan ...  Ponnappan 
*Salim Kumar ...  Sundaran 
*Cochin Hanifa ...  Moosakka 
*Ravi Vallathol ...  Vishwanath 
*Jose Pellissery ...  Principal 
*Manka Mahesh ...  Subhadra 
*Abhinayasree ...  Herself 
*Zeenath
*Manka Mahesh
*Deepika Mohan ... Meeras mother

== References ==
 

==External links==
*  
* http://popcorn.oneindia.in/title/2950/pranayamanithooval.html
* http://www.weblokam.com/cinema/market/2002/11/pranayamanithooval.htm

 
 
 
 
 


 
 