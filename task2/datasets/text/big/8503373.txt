The Astronaut
{{Infobox Film
| name           = The Astronaut
| director       = Robert Michael Lewis
| producer       = Harve Bennett
| writer         = Harve Bennett Robert Biheller Gerald Di Pego Charles Kuenstle
| starring       = Jackie Cooper Monte Markham
| music          = Gil Melle
| cinematography = Alric Edens
| editing        = Les Green John Kaufman
| studio         = Universal Television
| distributor    = American Broadcasting Company
| released       = January 8, 1972
| runtime        = 73 mins
| country        = United States
| awards         = 
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Astronaut is a 1972 science fiction film starring Jackie Cooper and Monte Markham. This television film|made-for-television film follows a man who has been hired to impersonate an astronaut who died during the first manned mission to Mars. Following the failure of the mission NASA (who fear their project will be cancelled) fools the public by faking the landing on Mars. Only the wife of the man used by NASA in this scheme suspects the man claiming to be her husband just might not be.

==Cast==

{| class="wikitable"
! Actor
! Role
|- 
| Jackie Cooper || Kurt Anderson
|-
| rowspan=2| Monte Markham || Eddie Reese
|-
| Col. Brice Randolph
|-
| Richard Anderson || Dr. Wylie
|- Robert Lansing || John Phillips
|-
| Susan Clark || Gail Randolph
|-
| John Lupton || Don Masters
|-
| Walter Brooke || Tom Everett
|-
| James Sikking || Astronaut Higgins
|- Paul Kent || Carl Samuels
|-
| Loretta Leversee || Toni Scott
|-
| Wally Schirra ||
|}

==Bibliography==
* Wingrove, David. Science Fiction Film Source Book (Longman Group Limited, 1985) ISBN 0582892392

==External links==
*  
*  

 
 
 
 
 


 