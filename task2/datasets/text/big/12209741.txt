An Affair of Honor
{{Infobox film
| name           = An Affair of Honor
| image          =
| image size     =
| caption        =
| director       =
| producer       =
| writer         = Lubin Studios
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = May 18, 1901
| runtime        =
| country        = United States English intertitles
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Affair of Honor is a silent film that was inspired by a famous painting. It was made 18 May 1901 in Philadelphia, Pennsylvania  by Lubin Studios.

A woman and her male friend (Arthur Marvin) are seated at a table when another woman makes romantic overtures to the man. What follows is a duel with swords and one of the women is killed.

==See also==
* List of American films of 1901
*Lubin Studios
*Silent film
*Duel

==External links==
* 

 
 
 
 
 


 