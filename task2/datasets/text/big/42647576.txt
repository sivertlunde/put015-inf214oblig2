Farmland (film)
{{Infobox film
| name = Farmland
| image = Farmland (2014) documentary film poster.jpg
| alt = Farmland
| caption = Poster for Farmland
| director = James Moll
| producer = James Moll Christopher Pavlick
| writer = 
| starring =  Brad Bellah  Leighton Cooley   David Loberg  Sutton Morgan  Margaret Schlass  Ryan Veldhuizen 
| music = Nathan Wang
| cinematography = Harris Done
| editing = Tim Calandrello Allentown Productions
| distributor = D&E Entertainment
| released =  
| runtime = 77 minutes
| country = United States
| language = English
| budget =
| gross = 
}} ranchers across modern agriculture.  Critics view the farmers and ranchers as sincere and what they do as interesting, but they are critical of what is left out of the documentary and that it was funded by the U.S. Farmers and Ranchers Alliance.

==Plot==
The goal of the film is to bridge the gap between food growers and food consumers by presenting farmers and ranchers perspectives on producing food.     The film aims to do this by focusing on the lives of six farmers in their 20s who describe their experiences of and views on modern farming and ranching in the United States. antibiotics in treatment of farm animals.    
   
   
     

==Cast==
The main cast of the documentary consists of six farmers and ranchers from different rural areas in the United States.
* Brad Bellah from Throckmorton, Texas is a sixth generation cattle rancher.   
* Leighton Cooley from Crawford County, Georgia is a fourth generation poultry farmer.    corn and soybeans.    organic onions. 
* Margaret Schlass from near Pittsburgh, Pennsylvania started an organic farming business called "One Woman Farm, Inc."     hogs and grows corn and soybeans to feed them.   

==Critical reception== eggs and poultry is done mainly by corporate farming and not small family farms.  Controversial issues such as the use of genetically modified crops, the use of pesticides, the use of antibiotics in animal feeds, and the treatment of animals on farms are largely dismissed in the film according to reviewers.  Finally, reviewers question the aims of the movie because it was funded by U.S. Farmers and Ranchers Alliance.         

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 