List of horror films of 1995
 
 
A list of horror films released in 1995 in film|1995.

{| class="wikitable sortable" 1995
|-
! scope="col" | Title
! scope="col" | Director
! scope="col" class="unsortable"| Cast
! scope="col" | Country
! scope="col" class="unsortable"| Notes
|-
!   | Addicted to Murder
| Kevin T. Lindenmuth || Mick McCleery, Laura McLauchlin, Sasha Graham ||   || Direct-to-video 
|-
!   | The Addiction
| Abel Ferrara || Lili Taylor, Christopher Walken, Annabella Sciorra ||   ||  
|-
!   | Alien Terminator
| Dave Payne || Maria Ford, Rodger Halston, Emile Levisetti ||   ||  
|-
!   | Burial of the Rats
| Dan Golden || Kevin Alber, Adrienne Barbeau, Eduard Plaxin ||    ||  
|-
!   | Castle Freak
| Stuart Gordon || Jeffrey Combs, Jonathan Fuller, Barbara Crampton ||   ||  
|-
!   |  
| Bill Condon || Tony Todd, Kelly Rowan, Timothy Carhart ||   ||  
|-
!   | Carnosaur 2 John Savage, Cliff De Young ||   ||  
|-
!   | Creep
| Tim Ritter || Dika Newlin, Joel D. Wynkoop, Lee Pinder ||   ||  
|-
!   | The Day of the Beast
| Álex de la Iglesia || Alex Angulo ||   ||  
|-
!   | Demon Knight Bill Sadler, Jada Pinkett Smith ||   ||  
|-
!   | Dr. Jekyll and Ms. Hyde
| David F. Price || Sean Young, Tim Daly, Lysette Anthony ||    ||  
|-
!   | Eko eko azaraku
| Shimako Sato || Miho Kanno, Shu-Ma, Ryôka Yuzuki ||   ||  
|-
!   | Freakshow
| Paul Talbot, William Cooke || Jasi Cotton Lanier, Gene Aimone, Rand Courtney ||   ||  
|-
!  | Gakkō no Kaidan (film)|Gakkō no Kaidan
| Hideyuki Hirayama || ||  
|-
|!  | Godzilla vs. Destoroyah
| Takao Okawara ||  || 
|-
!   |  
| Joe Chappelle || Donald Pleasence, Mitchell Ryan, Marianne Hagan ||   ||  
|-
!   | Haunted (1995 film)|Haunted
| Lewis Gilbert || Aidan Quinn, Kate Beckinsale, Anthony Andrews ||   ||  
|-
!   | The Haunting of Helen Walker
| Tom McLoughlin || Aled Roberts, Valerie Bertinelli, Diana Rigg ||   ||  
|-
!   |  
| Clive Turner || Elizabeth Shé, Ernest Kester, Bonnie Lagassa ||   || Direct-to-video 
|-
!   |  
| Mark S. Manos || Jenna Bodnar, Blair Valk, Diana Marcu ||    ||  
|- Ice Cream Man David Naughton, David Warner ||   ||  
|-
!   | Leprechaun 3
| Brian Trenchard-Smith || Warwick Davis, Marcelo Tubert, Leigh Allyn Baker ||   || Direct-to-video 
|-
!   | Lord of Illusions Kevin J. OConnor, Famke Janssen ||   ||  
|- The Mangler
| Tobe Hooper || Sean Taylor, Larry Taylor, Ron Smerczak ||   ||  
|-
!   | Mosquito (film)|Mosquito Steve Dixon ||   ||  
|-
!   | Mutant Species Ted Prior, Denise Crosby, Jack Forcinito ||   ||  
|-
!   | Night of the Scarecrow John Hawkes, Gary Lockwood ||   ||  
|-
!   | Piranha (1995 film)|Piranha
| Scott P. Levy || William Katt, Alexandra Paul ||   || Television film 
|-
!   | The Prophecy
| Gregory Widen || Christopher Walken, Elias Koteas, Eric Stoltz ||   ||  
|-
!   | Serpents Lair
| Jeffrey Reiner || Jeff Fahey, Heather Medway, Anthony Palermo ||   ||  
|-
!   | Species (film)|Species
| Roger Donaldson || Ben Kingsley, Michael Madsen, Alfred Molina ||   ||  
|-
!   | Tales from the Hood
| Rusty Cundieff || Clarence Williams III, Joe Torry, Corbin Bernsen ||   ||  
|-
!   |  
| Kim Henkel || Renée Zellweger, Matthew McConaughey, Robert Jacks ||   ||  
|-
!   | Tokyo Fist
| Shinya Tsukamoto || Shinya Tsukamoto, Kaori Fujii, Kohji Tsukamoto ||   ||  
|-
!   | Vampire in Brooklyn
| Wes Craven || Eddie Murphy, Angela Bassett, Allen Payne ||   ||  
|- Village of the Damned
| John Carpenter || Christopher Reeve, Kirstie Alley, Linda Kozlowski ||   || Film remake 
|-
!   | Visitors of the Night
| Jorge Montesi || Markie Post, Pam Hyatt, Melyssa Ade ||   ||  
|-
!   | The Wasp Woman Jennifer Rubin ||   || Film remake 
|-
!   | Witchboard III: The Possession
| Peter Svatek || David Nerman, Elizabeth Lambert ||   ||  
|-
!   | Witchcraft 7: Judgement Hour
| Michael Paul Girard || Kimberly Blair, Jack Valan, Alisa Christensen ||   ||  
|-
!   |  
| Harry Bromley Davenport || Sal Landi, Andrew Divoff, Robert Culp ||   ||  
|}

==References==
 

==Citations==
 
*  <!--
-->
 

 
 
 

 
 
 
 