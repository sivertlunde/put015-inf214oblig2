Saved by the Pony Express
{{infobox_film
| title          = Saved by the Pony Express
| image          =
| imagesize      =
| caption        =
| director       =
| producer       = Selig Polyscope Company William Selig
| writer         =
| starring       = Tom Mix
| music          =
| cinematography =
| editing        =
| distributor    = General Film Company
| released       = August 1, 1911
| runtime        = 1 reel; 12 minutes
| country        = USA
| language       = Silent...(English intertitles)
}}
Saved by the Pony Express is a 1911 short silent film western starring Tom Mix and produced by the Selig Polyscope Company. It is also known as Pony Express Rider. 

Its preserved at the Library of Congress. 


==Cast==
*Tom Mix - Pony Express Rider
*Thomas Carrigan - Happy Jack
*Old Blue - A horse


==See also==
*Tom Mix filmography

==References==
 

==External links==
* 


 
 
 

 