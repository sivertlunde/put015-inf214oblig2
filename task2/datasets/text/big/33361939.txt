Set Up (2005 film)
 
 
 
{{Infobox film
| name           = Set Up
| image          = SetUp2005.jpg
| alt            = 
| caption        = DVD cover
| film name = {{Film name| traditional    = 凶男寡女
| simplified     = 凶男寡女
| pinyin         = Xiōng Nán Guǎ Nǚ
| jyutping       = Hung1 Naam4 Gwa2 Neoi2}}
| director       = Billy Chung
| producer       = Wong Jing
| writer         = Wong Jing
| starring       = Christy Chung Michael Tse Roy Cheung
| music          = Alan Wong
| cinematography = Joe Chan
| editing        = Poon Hung
| studio         = 
| distributor    = China Star Entertainment Group
| released       =  
| runtime        = 95 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          =
}}
Set Up is a 2005 Hong Kong horror film produced and written by Wong Jing, directed by Billy Chung and starring Christy Chung, Michael Tse and Roy Cheung.

==Plot==
Moon Siu is a horror story writer who got refractive surgery the day before her wedding. That night she stays at an unfurnished house in the suburb with her sister Yan. When Moon was sleeping, three robbers Pau, Kei and Man, who were hiding out there, kills Yan and her boyfriend and stuffs her corpse in the refrigerator. Later Moon finds Yans corpse and arrested Kei when her fiance Ted arrived. While unknown to Moon, Ted actually belongs to the same gang.

==Cast==
*Christy Chung as Moon Siu
*Michael Tse as Ted
*Roy Cheung as Pau
*Tony Ho as Kei
*Winnie Leung as Yan
*Johnny Lu as Edmund
*Marco Lok as Man
*Alan Ng as Red Ox
*Pang Mei Seung as provision store boss

==External links==
* 
*  at Hong Kong Cinemagic
* 

 
 
 
 
 
 
 
 
 
 


 
 