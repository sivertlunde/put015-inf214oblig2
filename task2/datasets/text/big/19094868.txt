Dead Space: Downfall
 
{{Infobox film
| name        = Dead Space: Downfall
| image       = Dead Space Downfall poster.jpg
| caption     =
| director    = Chuck Patton
| producer    = Joe Goyette Robert Weaver Ellen Goldsmith-Vein
| writer      = Justin Gray Jimmy Palmiotti
| starring    = Nika Futterman Bruce Boxleitner Keith Szarabajka Jim Cummings Kelly Hu Kevin Michael Richardson Jim Piddock
| editing     =
| music       = Seth Podowitz
| studio         = Film Roman Electronic Arts
| distributor = Electronic Arts Manga Entertainment Anchor Bay Entertainment
| released    = October 28, 2008
| runtime     = 75 minutes
| language    = English
| country     = United States
| budget      =
| gross       =
| website     =
}}

Dead Space: Downfall is an American  , while the Necromorphs invade the USG Ishimura after it receives the Marker. The film was released on DVD and Blu-ray on October 28, 2008 and broadcast on the Syfy Channel on December 2, 2008.

==Plot==
An alien artifact, identified as a possible "second Marker", is discovered on the planet Aegis VII. The Church of Unitology uses its influence to send the mining ship USG Ishimura to retrieve this holy relic.

While the ship approaches, they receive radio reports of an outbreak of violent behavior among the colonists. The Marker is brought aboard by order of Captain Benjamin Mathius (Jim Cummings). Head of Security Alissa Vincent (Nika Futterman) asks to investigate the colony, but Mathius believes nothing is amiss. While the Ishimura extracts a chunk of the planet to mine, a man in the ships sick bay, Hans Leggio, (Jeff Bennett) struggles against his restraints and resists sedation, begging to be killed.

Meanwhile, two miners on Aegis VII check an outpost that has gone silent. One man, Farum (Cummings) vanishes during a blackout, while the other, Colin Barrow, sees his wife Jen (Lia Sargent) commit suicide. Barrow takes her body and flees the facility in a shuttle, unaware of an alien presence attached to the corpse.
 Phil Morris), Dobbs (Bennett), Pendleton (Kevin Michael Richardson), and Shen (Kelly Hu) are dispatched to arrest and quarantine the people on the shuttle, but they find nothing aboard. Leggio awakens to discover alien creatures mutating the corpses in the morgue, which then kill him. The security team follows trails of blood towards the morgue, where Dobbs is killed by an infected Leggio. Tensions flare as Unitologist crew members demand to see the Marker; they are quickly calmed and dispersed by engineer Samuel Irons (Richardson), a Unitologist.

The security team fight the infestation as it spreads through the ship, with Irons joining them after saving them from the Necromorphs in the ships mess hall where Pendleton is swarmed and killed. Dr. Terrence Kyne (Keith Szarabajka) tells Mathius that the Marker is responsible for the colonists going insane, killing each other and causing the infestation and must not be taken to Earth, fearing that the Necromorphs will wipe out every last human on Earth, but the captain claims religious persecution and mutiny. Mathius is accidentally killed by Kyne in an attempt to sedate him, who flees the bridge. The escape pods are launched empty and the communications systems destroyed.
 scuttle the ship on the planet to stop the Necromorphs from spreading.

On their way to confront Kyne, the team finds survivors (Issac Clarkes girlfriend Nicole among them) trapped by Necromorphs. Irons buys time for Vincent and Ramirez to extract the scared crew members by using himself as bait, and is killed. Ramirez sacrifices himself getting Vincent into the control room. She confronts Kyne, who claims that his actions are necessary to stop the alien outbreak from leaving the system. Vincent fails to restore the engines, and Kyne escapes with her weapon.

Vincent finds herself surrounded by Necromorphs, but learns that the creatures cannot come into close proximity with the artifact, which kept them imprisoned on Aegis VII. Spurred on by a vision of Ramirez, Vincent leaves a video log that details the entire sequence of events, adding that the Marker and the Ishimura must be destroyed. She uploads the video log to a distress beacon, opens the airlock and launches the beacon from the downed shuttle. Vincent and the Necromorphs are blown out into outer space|space.
 Dead Space.

==Voice Cast==
* Nika Futterman - Alissa Vincent
* Jim Cummings - Captain Mathius, Farum
* Hal Sparks - Ramirez Phil Morris - Hansen, Glenn
* Kelly Hu - Shen
* Keith Szarabajka - Dr. Terrence Kyne
* Kevin Michael Richardson - Samuel Irons, Pendleton, Miner
* Jeff Bennett - Leggio, Dobbs, Jackson
* Grey DeLisle - Heather, Donna Fawkes
* Bruce Boxleitner - Colin Barrow
* Lia Sargent - Jen Barrow, Walla/ADR 1
* Maurice LaMarche - White, Bavaro
* Bob Neill - Cartusian, Walla/ADR 2
* Jim Piddock - Chic
* Shelly ONeill - Walla/ADR 5
* Kirk Baily - Walla/ADR 4
* David Allen Kramer - Walla/ADR 3

==Sequel==
A second film based on the   was released on January 25, 2011, alongside Dead Space 2.

==External links==
*  
*  
*   listed on The Numbers

 

 
 
 
 
 
 
 
 
 
 