Raja Chinna Roja
{{Infobox film
| name            = Raja Chinna Roja
| image           = Raja Chinna Roja.jpg
| director        = SP. Muthuraman
| writer          = Panju Arunachalam
| starring        =  
| producer        = M. Saravanan (film producer)|M. Saravanan M. Balasubramaniam
| cinematography  = T. S. Vinayagam
| editing         = R. Vittal C. Lancy Chandrabose
| studio          = AVM Productions
| distributor     = AVM Productions
| released        = July 20, 1989
| runtime         = 180 minutes
| country         = India Tamil
| budget          = 1 Crore
| Gross           = 2 Crore
| website         =
}}
 Tamil childrens The Sound of Music with a song of the original fully used in Tamil. The film was the first Indian film to use animated characters with actors.  

==Plot==
An aspiring young wannabe actor Raja (Rajinikanth) from a village get into a fast city which is full of crooks and drug dealers. As he searching for a place in the dream factory, he falls for the daughter of the house owner (Jaiganesh). Charmed by his looks and character, the girl (Gowthami) also falls for him. One day he accidentally meet his childhood friend Raghu (Raghuvaran) who is a spoiled rich guy. He offers Raja an acting job, then takes him to his uncles house. Raja is to be in charge of the administration of the household and take care of five children (Raghus nieces and nephews) each of whom have issues (such as being lazy, not studying etc.). Raja finds out that Raghu is cheating his uncle out of funds and using him to do the same. How he tackles these problems and helps the children become better is the rest of the movie.

==Cast==
* Rajinikanth as Raja, a youth who wants to become an actor.
* Gautami Tadimalla|Gouthami as Roja, the house owners daughter who falls in love with Raja.
* Raghuvaran as Bhaskar, a friend of Raja who uses him to cheat his rich uncle.
* Ravichandran (Tamil actor)|Ravichandran as Bhaskars uncle.
* V. K. Ramaswamy (actor)|V. K. Ramasamy as Ravicnahdrans uncle.
* Chinni Jayanth as Rajas friend.
* S. S. Chandran as servant.
* Kovai Sarala as housemaid.
* Baby Shalini as Chithra, Ravichandrans middle daughter.

==Production== live action AVM Saravanans AVM to become the first Indian studio to do this. The idea of blending live action with animation was inspired by Who Framed Roger Rabbit (1988).   

==Soundtrack==
The soundtrack consist of seven songs composed by Chandrabose. The song "Superstar Yaarunu Ketta" was remixed by Chandraboses son Santhosh Bose for the film Kalayatha Ninaivugal (2005). 

{{Infobox album  
| Name        = Raja Chinna Roja
| Type        = Album
| Artist      = Chandrabose
| Cover       =
| Released    = 1989
| Recorded    = Feature film soundtrack
| Length      =
| Label       = AVM Audio AVM
| Reviews     =
| Last album  =
| This album  =
| Next album  =
}}

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers || Length (min:sec) ||Lyrics ||Notes
|-
| 1 || Super Staru || SP. Balasubramaniam, SP. Sailaja || 04:25 || Vairamuthu ||
|-
| 2 || Raja Chinna Roja || SP. Balasubramaniam || 04:46 || Vairamuthu ||
|-
| 3 || Varungala Mannargale || SP. Balasubramaniam || 04:23 || Vairamuthu ||
|-
| 4 || Oru Panbadu || Yesudas || 04:32 || Vairamuthu ||
|-
| 5 ||  Ongappanukkum Pe Pe || Malaysia Vasudevan, SP. Sailaja || 03:43 || Vairamuthu ||
|-
| 6 ||  Poo Poo Pol || Mano || 05:14 || Vairamuthu ||
|-
| 7 ||  Devaadhi Devar Ellaam || Malaysia Vasudevan || 04:57 || Vairamuthu ||
|}

==References==
 

==External links==
* 
 
 
 

 
 
 
 
 
 