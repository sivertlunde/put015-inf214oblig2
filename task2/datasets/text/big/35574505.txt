This Space Between Us
{{Infobox film
| name           = This Space Between Us
| image          = 
| image_size     = 
| caption        = 
| director       = Matthew Leutwyler
| writer         = 
| narrator       = 
| starring       = Jeremy Sisto
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 2000
| runtime        = 98 mins.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
This Space Between Us is a 2000 film directed by Matthew Leutwyler. It stars Jeremy Sisto and Poppy Montgomery.  It won the Moxie Award at the 2000 Santa Monica Film Festival. 

==Cast==
*Jeremy Sisto as Alex Harty
*Poppy Montgomery as Arden Anfield
*Erik Palladino as Jesse Fleer
*Clara Bellar as Zoe Goddard
*Vincent Ventresca as Sterling Montrose

==References==
 

==External links==
* 

 

 
 
 
 

 