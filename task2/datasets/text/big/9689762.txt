Rasigan
 
 
{{Infobox film name       = Rasigan image      = caption    = director   = S. A. Chandrasekhar producer   = B. Vimal screenplay = S. A. Chandrasekhar story      = Shoba Chandrasekhar starring   =   music  Deva
|cinematography = Ravishankar editing    = Gautham Raju studio     = B. V. Combines distributor = B. V. Combines released   = 8 July 1994 runtime    =  country    = India language   = Tamil
| budget    =  1.1 crore
}}
 Tamil film Vijay and Sanghavi.  

==Plot== Manorama while Anitha (Sanghavi) is the daughter of Raghavan(Vijayakumar (actor)|Vijayakumar) and Bhagyalakshmi (Sri Vidya). Anithas family rent one of Manoramas houses. Vijay and Anitha pretend to hate each other while they actually  are in love. But later Anithas father came to know the love matter and he requests transfer to Chennai.Before going Anitha leaves her new address on several envelopes and she tells those are love letters given by Vijay, Anithas father gets angry and takes the letters and went to Vijays house and tells him to take his useless love letters.After that Vijay takes it and as he looks at it  he knows that those arent love letters, theyre all Anithas new address in Chennai.

Vijay tries to find her. One day Vijay goes to a restaurant to eat and he asked the server where is the washroom.The server told him that there is two washrooms, one on the upstairs and another one is here.The server tells him to not go upstairs because its dangerous.But, Vijay goes upstairs to the bathroom,where he finds the "Blue Film" illegal shooting happening.

By this, they blame Vijay for the death of the restaurant owner (who tripped) and Vijay just has to escape. This news reaches Anithas father who is a police officer. He tries to capture Vijay. The hero fights, runs some more until Raghavan finds out the truth and accepts the marriage between Vijay and Anitha.

==Cast== Vijay as Vijay
*Sanghavi as Anitha Raghavan
*Srividya as Bhagyalakshmi Vijayakumar as Raghavan Manorama as Vijays grandmother
*Goundamani as Ekamabram Senthil as Kapooram
*S. P. Rajkumar
*Vichitra as Rani

==Crew==
* Screenplay, Dialogue and Directed by: S. A. Chandrasekhar
* Story: Shoba Chandrasekhar
* Produced by: B. V. Combines Deva
* Cinematography: Ravishankar
* Editing: Gautham Raju
* Art: P. L. Shanmugam
* Fights: Super Subbarayan Vaali and P. R. C. Balu
* Choreography: Raju Sundaram, D. K. S. Babu and Chinni Prakash
* Costumes: S. Rajendran
* Banner: B. V. Combines

==Soundtrack==
{{Infobox album|  
  Name        = Rasigan
|  Type        = Soundtrack Deva
|  Cover       =
|  Released    = 1994
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = 
|  This album  = Once More (1994)
|  Next album  = 
}}
There are 6 songs composed by Deva (music director)|Deva.

*1. Silena Silena - S. N. Surendar, Swarnalatha
*2. Auto Rani -Mano (singer)|Mano, Swarnalatha
*3. Beechoram - S. P. Balasubrahmanyam, K. S. Chithra
*4. Love Love Mama - S. P. Balasubrahmanyam, Shoba Chandrasekar
*5. Bombai City - Vijay (actor)|Vijay, Swarnalatha Deva

==References==
 

==External links==
*  

 

 
 
 
 
 