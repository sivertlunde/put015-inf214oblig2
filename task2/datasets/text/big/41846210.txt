Raaja Raajathan
{{Infobox film
| name = Raaja Raajathan
| image = RaajaRaajathan.jpg
| caption = LP Vinyl Records Cover
| director = Ramdass
| writer =
| starring = Ramarajan Gouthami
| producer = Smt. Shoba   P. S. Nivas
| music = Illayaraja
| editor =
| released = 1989
| runtime = Tamil
| budget =
}}
 directed by Ramdass, starring Ramarajan and Gouthami in lead roles. 

==Cast==
*Ramarajan
*Gouthami

==Soundtrack==
{{Infobox album Name     = Raaja Raajathan Type     = film Cover    = Released =  Artist   = Illayaraja Genre  Feature film soundtrack Length   = 24:48 Label    = Echo
}}
The music was composed by Illayaraja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eerettu Pathinaaru || Mano (singer)|Mano, S. Janaki || Vaali || 05:36
|-
| 2 || Innum Enna || Malaysia Vasudevan, K. S. Chithra || M. Metha || 04:33
|-
| 3 || Maina Mayakkama || Malaysia Vasudevan || Gangai Amaran || 04:56
|-
| 4 || Mamarathu Kuyilu || Illayaraja, K. S. Chithra || Pulamaipithan || 04:32
|}

==References==
 

==External links==

 
 
 
 
 


 