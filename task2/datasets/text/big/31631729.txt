Innaleyude Baakki
{{Infobox film
| name           = Innaleyude Baakki
| image          =
| caption        =
| director       = PA Bakker
| producer       =
| writer         = KL Mohanavarma
| screenplay     = Devan Geetha Geetha Sreenath
| music          = G. Devarajan
| cinematography =
| editing        =
| studio         = Samskara Films
| distributor    = Samskara Films
| released       =  
| country        = India Malayalam
}}
 1988 Cinema Indian Malayalam Malayalam film, Geetha and Sreenath in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Captain Raju Devan
*Geetha Geetha
*Sreenath

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Yusufali Kechery. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Neelambari || P. Madhuri || Yusufali Kechery || 
|}

==References==
 

==External links==
*  

 
 
 

 