Jagathalaprathapan (1944 film)
{{Infobox film|
  name           = Jagathalaprathapan|
  image          = Jagathalaprathapan.jpg|
  image_size= 150px|
  caption        = A still from Jagathalaprathapan|
  director       = |
  writer         = |
  starring       = P. U. Chinnappa,  M. S. Sarojini,  M. R. Santhanalakshmi,  P. B. Rangachari,  U. R. Jeevaratnam,  S. Varalakshmi,  T. A. Jayalakshmi,  T. S. Balaiah,  N. S. Krishnan,  T. A. Madhuram  Yogambal, Mangalam |
  producer       = Pakshiraja Films|
  distributor    = |
  music          = |
  cinematography =  |
  editing        = |
  released       = 1944|
  runtime        = | Tamil }}

Jagathalaprathapan was a 1944 Tamil-language film starring P. U. Chinnappa and M. S. Sarojini in the lead roles. Dubbed the 12 Ministers Tale, Jagathalaprathapan was a box-office success and sealed Chinnappas position as one of the top stars in Tamil cinema.

== Plot ==

Jagathalaprathapan was based on the fantasy story of a prince who is to be punished from his kingdom for expressing his desire for four celestial maidens, Indrani, Nagakumari, Agnikumari and Varunakumari.  The prince, however, escapes punishment and roams in disguise in the company of a friend when he meets a damsel who is Indirakumari in disguise. He marries her and settles down in a kingdom whose king falls in love with his wife and sends the prince away to Nagaloka in order to covet her.  The prince, however, succeeds in finding Nagakumari in Nagaloka, Agnikumari in Agniloka and Varunakumari in Varunaloka and succeeds in winning them as wives.

== Cast ==

* P. U. Chinnappa as Prince Jagathalaprathapan
* M. S. Sarojini as Indirakumari
* P. B. Rangachari as Jagathalaprathapans father
* M. R. Santhanalakshmi as Jagathalaprathapans mother
* U. R. Jeevarathnam as Nagakumari
* S. Varalakshmi as Agnikumari
* T. A. Jayalakshmi as Varunakumari
* P. Saradambal as Avvai
* N. S. Krishnan as Jagathalaprathapans friend.

== Reviews ==

Jagathalaprathapan was acclaimed for its stunning dance sequences and technical excellence.

== References ==

*  

 
 
 
 


 