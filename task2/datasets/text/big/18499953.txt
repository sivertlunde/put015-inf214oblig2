Mosquito Squadron
{{Infobox Film |  
| name           = Mosquito Squadron
| image          = Mosquito Squadron.jpg
| caption        = Theatrical poster
| director       = Boris Sagal
| producer       = 
| writer         =  , 15 February 2011. Retrieved: 24 February 2011.  Joyce Perry
| starring       = David McCallum Suzanne Neve [[Charles Gray (actor)|
Charles Gray]]
| music          = Frank Cordell
| cinematography = Paul Beeson
| editing        = 
| studio         = Oakmont Productions
| distributor    = United Artists
| released       = January 1969 
| runtime        = 86 min.
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
#

 1969 British war film made by Oakmont Productions, directed by Boris Sagal and starring David McCallum, with a memorable music score (starting with 29 pounding bass drum beats to background the V-1 (flying bomb)|V-1 flying-bomb theme of the film), which was composed and conducted by Frank Cordell.

==Plot== Second World German V-1 flying bomb installations during the early summer of 1944.   The de Havilland Mosquito fighter-bomber aircraft of Squadron Leader David "Scotty" Scott (David Buck) is shot down during a low-level bombing raid on a V-1 launching site, and Scott and his navigator/bomb-aimer are reportedly killed. His wingman and friend, then-Flight Lieutenant (later insignia Royal Canadian Air Force squadron leader) Quint Munroe (David McCallum) comforts Scotts wife Beth (Suzanne Neve) and a romance soon develops, rekindling one that they had had years earlier. 
 Charles Gray), Maquis resistance fighter who supposedly talked under torture, Allied prisoners, including a very-much-alive Scott and men from their group, are held as "human shields." This is seen in a disturbing film dropped by a Luftwaffe Messerschmitt Bf 109 fighter that, in tandem with one other, raided the base, strafing the airfield and killing many personnel.
 German army RAF men to go back to their cells. The senior RAF officer amongst the captives, Squadron Leader Neale (Bryan Marshall) is killed by German machine-pistol fire during the breakout as fellow comrades make their way with the help of the resistance fighters out of the chateau grounds while the bombing raid continues with a second wave of Mosquito bombers dropping conventional bombs with the intention of completely destroying the building.

Munroe and Scott are briefly reunited after the formers aircraft is brought down by flak, though Scott, still suffering from amnesia and unable to remember even his own name (hence, he sports a chalked "X" on his uniform), rebuffs Munroes attempt to get him to remember who he is, ignoring mention of even his wifes name. Scott then sacrifices himself while stopping a German Panzer|tank, saving Munroe and others, but too late to save Munroes navigator, Flight Sergeant Wiley Bunce (Nicky Henson), but not before Scott says his wifes name. 
 Wing Commander David Dundas), an ex-pilot who had lost his right hand on operations (he sports a hook in its place) but now serving with the same squadron and in charge of training. However, he deliberately still conceals the secret from her that her now-dead husband had survived the crash that he had witnessed, although, thanks to the German film, both he and Shelton had, in fact, known for some time that he had not been killed as first generally believed.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- RCAF
|-
| Suzanne Neve || Beth Scott 
|- Charles Gray|| Air Commodore Hufford, RAF
|-
| David Buck || Squadron Leader David ("Scotty") Scott, RAF
|- David Dundas ||  Flight Lieutenant Douglas Shelton, RAF
|-
| Dinsdale Landen || Wing Commander Clyde Penrose, RAF
|-
| Nicky Henson || Flight Sergeant Wiley Bunce
|-
| Bryan Marshall || Squadron Leader Neale, RAF
|-
| Michael Anthony || Father Bellaguere
|-
| Peggy Thorpe-Bates || Mrs. Scott  
|-
| Peter Copley || Mr. Scott
|-
| Vladek Sheybal || Lt. Schack 
|}

==Production== Operation Crossbow. Bovingdon Airfield Farnborough in Hampshire, Southern England. The raid echoes Operation Jericho, a co-ordinated RAF/Maquis raid which freed French prisoners from Amiens jail in which the Mosquito took part.
 Upkeep bomb, and the footage seen in the film of Mosquitoes dropping Highballs on land is genuine archive film. Charles Grays character mentions Barnes Wallis during his briefing, in such a way as to imply that the name was well-known to the RAF men. The special Highball bombsight seen in the film is also a genuine representation of the sight used for dropping Highball.

==Reception==
Most reviewers concentrated on the low-budget production values, but the script and cast have also come in for some severe criticism in some quarters.  

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
*  
* Lindsey, Brian.   Eccentric Cinema. Retrieved: 20 February 2011.
 

== External links ==
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 
 