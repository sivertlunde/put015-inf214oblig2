The Drop Kick
 

{{Infobox film
| name           = The Drop Kick
| image_size     =
| image	         = The Drop Kick FilmPoster.jpeg
| caption        =
| director       = Millard Webb
| producer       = Richard A. Rowland Ray Rockett
| writer         = Winifred Dunn (scenario)
| based on       = 
| starring       = Richard Barthelmess Barbara Kent
| cinematography = Arthur Edeson Alvin Knechtel
| editing        = First National Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = Silent film
| budget         =
}} football player (Richard Barthelmess) who finds his reputation on the line when he pays an innocent visit to a woman whose husband kills himself. It was one of the early films of John Wayne who was only aged 20 in the film. He too played a college footballer.     

==Cast==
*Richard Barthelmess as Jack Hamill 
*Barbara Kent as Cecily Graves 
*Dorothy Revier as Mrs. Hathaway 
*Eugene Strong as Brad Hathaway 
*Alberta Vaughn as Molly 
*Brooks Benedict as Ed Pemberton 
*Hedda Hopper as Mrs. Hamill 
*Mayme Kelso as Mrs. Graves 
*George C. Pearce as The Dean  USC Football Player 

==References==
 

==External links==
* 
*  
* 
* 


 
 
 
 
 
 
 

 
 