Starman (film)
{{Infobox film
| name           = Starman
| image          = Starman film poster.jpg
| caption        = Starman theatrical poster
| director       = John Carpenter
| starring       = {{plainlist|
* Jeff Bridges
* Karen Allen
* Charles Martin Smith
* Richard Jaeckel
}}
| writer         =  
| cinematography = Donald M. Morgan
| editing        = Marion Rothman
| producer       = Larry J. Franco Michael Douglas
| music          = Jack Nitzsche
| distributor    = Columbia Pictures
| released       =  
| country        = United States
| language       = English
| budget         = $24 million
| gross          = $28,744,356
| runtime        = 115 minutes
}} romantic science alien (Jeff gold phonograph record installed on the Voyager 2 space probe.
 television series Michael Cavanaugh.

==Plot==
  gold phonographic alien ship. first contact with Earth. But instead of greeting the craft, the government shoots the alien down. Crashing in Chequamegon Bay, Wisconsin, the alien, looking like a blue ball of energy, finds the home of recently widowed Jenny Hayden (Karen Allen). While there, the alien uses a lock of hair from her deceased husband, a house-painter named Scott Hayden, to clone a new body which a stunned Jenny witnesses. The Starman (Jeff Bridges) has seven small silver spheres which provide energy to perform miraculous feats. He uses the first to send a message to his people about his craft being destroyed and that the environment is hostile. He arranges to rendezvous with them at "landing area one" in three days time. He then uses the second sphere to create a holographic map of the United States, coercing Jenny into taking him to Arizona.

As the shock wears off, Jenny turns both hostile and frightened of him. After repeatedly attempting to escape, she finally implores the Starman to shoot her with her pistol. But instead he releases the pistols magazine and tells her he means no harm. As they continue on their journey, the Starman, who had a rough understanding of English syntax from the Voyager 2 disk, learns to communicate with Jenny, who in turn teaches him that humanity is not completely savage.

He explains to Jenny that he has three days to get to the rendezvous point, Arizonas Barringer Crater, or he will die. She teaches him how to drive a car and use credit cards, intending on escaping so he can continue his journey alone. However, as she is about to make her escape, she witnesses him miraculously resurrect a dead deer. Deeply moved, she resolves to help him at whatever cost.

Arranging a distraction, the two fugitives escape, but one of the officers shoots Jenny, critically wounding her. During the escape, the Starman crashes the car into a gas tanker and uses a sphere to protect the two of them from the explosion. They escape the area by taking refuge in a mobile home that is being towed. Down to his last two silver spheres, the Starman uses one to heal Jenny. While stowing away on a boxcar train, the couple have sex. Later that night, the Starman tells Jenny "I gave you a baby tonight." Jenny attempts to explain to him that she is infertile and cannot conceive a child, but the Starman insists, saying, "Believe what I tell you." He explains to the stunned Jenny that the baby will also be the son of her dead husband, because he is a clone of Scott. He also explains that the baby will know all that the Starman knows and when he grows up he will become a teacher. He tells her that he will stop the gestation from going further if she wishes, but the joyful Jenny embraces him, accepting the gift.
 Las Vegas. To make matters worse, Jenny has lost her wallet. The Starman uses one of their last quarters in a slot machine, which he manipulates in order to win the jackpot. The couple use their winnings to buy a new car to complete the drive to Winslow, Arizona, which is near Barringer Crater.

Meanwhile, Fox learns from NORAD that the Starmans flight trajectory, prior to being shot down, was to Barringer Crater. He figures out that the Starman would show up there in the next day or so. To Shermins dismay, he orders the Army helicopters to carry live ammunition and announces his intention to capture the alien alive or dead. This is made horribly clear when Shermin sees pathology equipment being unloaded, along with an operating table with straps; Shermins protests to Fox that Earth had invited the Starman are curtly dismissed.

The couple reach the crater as Army helicopters pursue them. Just as they are surrounded, a large, spherical landing craft appears and descends into the crater. Light surrounds the couple, and the Starman is instantly restored to health. He tells Jenny he will never see her again. Jenny confesses her love and begs him to take her with him, but he says she would die on his world. He then gives her his last silver sphere, telling her that their son will know what to do with it. As she watches in silence, the ship rises, carrying the Starman away.

==Cast==
* Jeff Bridges as Starman/Scott Hayden
* Karen Allen as Jenny Hayden
* Charles Martin Smith as Mark Shermin
* Richard Jaeckel as George Fox
* Robert Phalen as Major Bell
* Tony Edwards as Sergeant Lemon
* John Walter Davis as Brad Heinmuller Ted White as Deer Hunter
* Dirk Blocker as Cop number 1
* M. C. Gainey as Cop number 2
* George Buck Flower as Cook (as Buck Flower)
* Ralph Cosham as Marine Lieutenant
* Lu Leonard as Roadhouse waitress
* Mickey Jones as truck driver

==Production== The 39 Steps, and It Happened One Night over special effects. Riesner dropped the "heavy political implications" from the script in order to comply with this. 

==Release and reception==
Starman grossed $2,872,022 in its opening weekend, debuting at number 6.  The film grossed a total of $28,744,356 from its domestic (US and Canada) run.

Review aggregator Rotten Tomatoes reports that Starman gained an 80% approval rating based on 25 reviews from critics. 

==Awards and nominations== Golden Globe Best Actress Best Science Golden Globe nomination for his Golden Globe Award for Best Original Score|score. 

*AFIs 100 Years...100 Passions - Nominated 
*AFIs 10 Top 10 - Science Fiction Film (Nominated)

==Soundtrack==
{{Infobox album 
| Name        = Starman: Original Motion Picture Soundtrack
| Type        = Soundtrack
| Artist      = Jack Nitzsche
| Cover       = StarmanSoundtrackCover.jpg
| Released    = December 14, 1984
| Recorded    =
| Genre       = Soundtrack
| Length      = 33:05
| Label       = Varèse Sarabande
| Producer    =
| Last album  =
| This album  = Starman: Original Motion Picture Soundtrack (1984)
| Next album  =
}}
{{Album ratings|title=Soundtrack
| rev1      = AllMusic
| rev1Score =    
}}

The soundtrack to Starman was released on December 14, 1984.   The album also contains a rendition of "All I Have to Do Is Dream" performed by stars Jeff Bridges and Karen Allen.

{{Track listing
| all_music = Jack Nitzsche (except "All I Have to Do Is Dream," written by Felice and Boudleaux Bryant)

| title1 = Jenny Shot
| length1 = 1:30

| title2 = Here Come the Helicopters
| length2 = 5:04

| title3 = Honeymoon
| length3 = 0:56

| title4 = Road Block
| length4 = 1:38

| title5 = Do You Have Somebody?
| length5 = 1:18

| title6 = Pickup Truck
| length6 = 3:01

| title7 = Whats It Like up There?
| length7 = 1:46

| title8 = All I Have to Do Is Dream 
| length8 = 3:29

| title9 = Lifting Ship
| length9 = 1:22

| title10 = I Gave You a Baby
| length10 = 2:11

| title11 = Morning Military
| length11 = 1:04

| title12 = Define Love
| length12 = 1:33

| title13 = Balls
| length13 = 1:10

| title14 = Starman Leaves
| length14 = 7:04
}}

==In popular culture== King Kong.

==See also==
*Starman (TV series)

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 