Karuppampatti
{{Infobox film
| name           = Karuppampatti
| image          = Karuppampatti Poster.jpg
| caption        = 
| director       = Tha. Prabhu Raja Cholan
| producer       = Sundar K
| writer         = Tha. Prabhu Raja Cholan Ajmal Aparnaa Archana M. S. Bhaskar Jagan
| cinematography = Santhosh Sriram Sanjeevi Sathyaseelan Kannan
| released       =  
| editing        = B. Lenin
| studio         = Sundar Pictures
| country        = India
| language       = Tamil
}} Archana in lead roles.  The film was released in 22 March 2013.

== Cast == Ajmal as Kothai Cocopardo/Manohar
* Aparnaa Bajpai as Kaveri/Shanthini
* M. S. Bhaskar as Don Stanlee
* Jagan as Karuppu Srinath as Anglee
* Devadarshini as Sivagami Chetan
* Alice Tantardini as Melanie Sarkozy
* Mahadevan Archana (Special appearance)

==Production== Karuppampatti village near Trichy to Paris, France.   A song titled "Oh Indira" was shot with Ajmal and Archana in May 2010 at AVM Studios, Chennai with a 1970s bar set out up. 

The team shot a schedule in Pazhani in December 2010 and then returned in August 2011, when Ajmal refused to engage in a scene where there is a kiss sequence with Aparnaa Bajpai and eventually ended up kissing her on the cheek instead.  The final portions of the film were canned in Paris, France and in Valsassina and Lecco, Italy in February 2012 where scenes featuring Ajmal and Italian model Alice Tantardini were canned.   In April 2012, it was announced that Bappi Lahiri would sing a song for the film titled "Naughty Raja Raja" and the team audaciously attempted to bring in international pop star Katy Perry to feature in the music video.    To make most of use of Ajmals popularity in Tamil, Telugu and Malayalam film industries, the director confirmed that the film would be dubbed and released in the latter two languages.  Ajmal also completed his dubbing for the film in French after taking lessons in the language to ensure his voice remains constant in the film. 

==Critical Reception==
in.com rated the film 2.5 out of 5 and wrote Karuppampatti is a decent attempt and doesnt live up to its expectations because of its half-baked script about portraying family melodrama. Further adding that the screenplay lacked the much needed depth to convey the emotional bonding and the newbie director failed in detailing and his lazy execution only adds to its woes. 

==Soundtrack==
{{Infobox album|  
  Name        = Karuppampatti
|  Type        = Soundtrack
|  Artist      = Kannan
|  Cover       =
|  Released    = 3 July 2012
|  Recorded    = Feature film soundtrack
|  Length      =
|  Label       =
|  Producer    =
|  Reviews     =
|  Last album  = Azhagan Azhagi (2012)
|  This album  = Karuppampatti (2012)
|  Next album  = Rendavathu Padam (2012)
}}
The soundtrack of the film was composed by Kannan, while lyrics were written by Kabilan. The songs released on 3 July 2012.

{{tracklist
| headline        = Track-list
| extra_column    = Singer(s)
| total_length    = 
| title1          = Adaleru Kaalaiyelam
| extra1          = Mukesh
| length1         = 3:42
| title2          = Disco Naughty Naughty Raja
| extra2          = Bappi Lahiri
| length2         = 4:08
| title3          = Hai Papapa
| extra3          = Karthik (singer)|Karthik, Sayanora
| length3         = 2:49
| title4          = Kannamma Chinna Ponnamma
| extra4          = Suchitra, Priya, Thilaka, Anitha  
| length4         = 0:57
| title5          = Karupampatti Karupampatti
| extra5          = Naveen, Mukesh, Suchitra
| length5         = 4:02
| title6          = Oh Indira
| extra6          = Karthik (singer)|Karthik, Sayanora
| length6         = 4:32
}}

== References ==
 

==External links==
*  
*  

 
 
 
 