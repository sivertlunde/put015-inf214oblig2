Still the Water
 
{{Infobox film
| name           = Still the Water
| image          = Still the Water poster.jpg
| caption        = Film poster
| director       = Naomi Kawase
| producer       = Rémi Burah Takehiko Aoki Masamichi Sawada Naomi Kawase
| writer         = Naomi Kawase
| starring       = Nijiro Murakami Jun Yoshinaga
| music          = Hasiken
| cinematography = 
| editing        = Tina Baz 
| distributor    = Asmik Ace
| released       =  
| runtime        = 110 minutes
| country        = Japan
| language       = Japanese
| budget         = 
}}

  is a 2014 Japanese romance film directed by Naomi Kawase. It was selected to compete for the Palme dOr in the main competition section at the 2014 Cannes Film Festival.    It has been selected to be screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.   
 Camera dOr Grand Prix, there is nothing I want more than the Palme dOr. I have my eyes on nothing else." 
 Amami City, Amami Ōshima, in Kagoshima Prefecture, Japan in 2013.  The music was produced by Hasiken,  a male singer-songwriter from Chichibu, Saitama.

==Cast==
 .]]
* Nijiro Murakami
* Jun Yoshinaga
* Tetta Sugimoto
* Miyuki Matsuda
* Makiko Watanabe
* Jun Murakami
* Hideo Sakaki
* Fujio Tokita

==Reception==
Reviewing it at Cannes, Nikola Grozdanovic at Indiewire gave it a B+, stating that "Still The Water is a spectacle for the senses, which, if there is any justice, will be remembered as one of the greater films of the competition."  In The Guardian, Peter Bradshaw gave it 3 out of 5 stars and stated, "Kawases film is sometimes beautiful and moving but I couldnt help occasionally finding it a little contrived and self-conscious."  In Film Business Asia, Derek Elley gave the film a rating of 2 out of 10, calling it "more empty, pretentious ramblings from self-styled auteur Kawase Naomi". 

==References==
 

==External links==
*  (in Japanese)
*  

 

 
 
 
 
 
 
 
 