Nua Bou
{{Infobox Film  name = Nua Bou image = caption = Movie poster for Nua Bou starring = Sarat Pujari Prashant Nanda Dhira Biswal Lila Dulali director = Prabhat Mukherjee producer = Dhira Biswal   Rama Krushna Tripathy music = Balakrishna Das distributor = Panchasakha Films  released       = 1962  
  country =   language = Oriya
}}

Nua Bou ( ),   is a 1962 Indian Oriya film directed by Prabhat Mukherjee, which presents the sociocultural  scenario of rural village in India in the 1970s. 

== Synopsis ==
 
Sura Babu lives with his wife Parbati, brother Raju & son Debi. Parbati loves Rabi more than his own son. Parbati falls ill and Raju comes to Puri believing that Lord Jaganaths prasad can cure Parbati and brings prasad.The illness continues and native doctor Das is called in. Parbati gradually cured in. Later Parbatis mother and sister Champa come to stay with her. parbatis mother is not well disposed towards mischievous Raju  and tries to influence Parbati against him. Her efforts fail as Champa take side of Raju. Dr Dass assistant Mukund and doctor Rabi, a new comer to the village compete for Champas hand. Dr Das makes the proposal to Sura Babu on behalf of Mukund for Champas hand. Sura agrees with a misunderstand that Doctor Das himself wants to marry Champa. In the meantime Mukund decided that he is not suitable for Champa. On the marriage day sura Babu forces Dr. Das to marry Champa. At the last moment Raju is found in bridal attire.Mukund comes forward and explain that Champa is in love with Dr. Rabi. The marriage of Rabi & Champa is celebrated

==Cast==
* Sarat Pujari
*Prashant Nanda
*Mani Mala
*Dhira Biswal
*Leela Dulali

== Music ==
*Balakrishna Das  has arranged music for this film
* Melodious song by Sikandar Alam & Shipra Bose

==Awards== National Film Awards
*     

== References ==
 

==External links==
*  

 
 
 


 