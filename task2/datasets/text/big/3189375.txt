Himalaya (film)
{{Infobox film
| name = Himalaya
| image = Himalaya film.jpg
| caption = Theatrical release poster
| director = Eric Valli 
| writer = Jean-Claude Guillebaud Eric Valli Louis Gardel
| starring = Tsering Dorjee Thinle Lhondup Gurgon Kyap Lhakpa Tsamchoe 
| music = Bruno Coulais
| producer = Christophe Barratier Jacques Perrin Kino International
| budget =
| released =    
| runtime = 108 minutes
| country = Nepal German
}} 1999 Nepalese Best Foreign Film category at the 72nd Academy Awards.
 rock salt from the high plateau down to the lowlands to trade for grain. An annual event, the caravan provides the grain that the villagers depend on to survive the winter. The film unfolds as a story of rivalry based on misunderstanding and distrust, between the aging chief and the young daring herdsman, who is both a friend and a rival to the chiefs family, as they struggle for leadership of the caravan. 

The film is a narrative on the both traditions and the impermanent nature of human struggle to retain and express power in the face of the gods. "The gods triumph" is the call that echoes at the end of the film and expresses the balancing of karmic destinies. The extreme environment of the Himalayas is magnificently contrasted to the delicacy of humanity and the beauty of Tibetan culture.
 National Geographic, GEO and Life magazines.

The film depicts not only the life style of the upper Dolpo people of the mid western uphills of Nepal but also their traditional customs, for example celestial burial.

== Reception == SBS gave the film 3.5/5, saying that "The story of Himalaya is a timeless one. French director Eric Valli tells it like a legend, and its one he knows well", and that it is "Its a simple but quite affecting saga".   

==See also==
* Himalayan salt

==References==
 

==External links==
 

* 

 
 
 
 
 
 
 
 
 