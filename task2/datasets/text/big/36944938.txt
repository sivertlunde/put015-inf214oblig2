Ball of Wool
{{Infobox film
| name = Ball of Wool
| image = Ball of Wool.jpg
| caption =
| Background = DVD1
| director = Nikolai Serebryakov
| producer =
| executive producer = Nathan Bitman
| writer = Ovsey Driz
| art director = Alina Speshneva
| story = Ovsey Driz
| based on = a tale by Ovsey Driz
| narrator =
| starring =
| music = Eduard Artemyev
| sound director = Boris Filchikov
| cinematography = Vladimir Sarukhanov
| editing = Natalia Abramova, V. Kuranov, Roman Gurov, Marina Chesnokova, Liliana Lutinskaya, Pavel Gussev, V. Kalashnikova, Svetlana Znamenskaya, Valery Petrov, Victor Grishin
| animator = Ye. Umanskaya, Youry Klepatsky, Joseph Douksha, Vladimir Puzanov, Pavel Petrov
| cutter = Nadezhda Treshcheva
| studio = Soyuzmultfilm
| distributor =
| released =  
| runtime = 9:25
| country = USSR
| language =
| budget =
}}

Ball of Wool ( ) is a stop motion animation directed by Nikolai Serebryakov and based on a tale by Ovsey Driz. The story is about a babushka who one day finds a wool dog. The animation was included in Masters of Russian Animation.

==Plot==
Based on a tale by Ovsey Driz, the story is about a poor babushka (old woman) who one day finds a wool dog. She discovers that she can use its wool to knit everything she wants. First she knits clothes, then a house. Despite having the most needful things, she needs more and more. She wants a young face and a new life, but the dogs wool did not suffice, thus breaking the charm. The dog took all its wool together with the babushka, absorbing her greed.

==External links==
* 

 
 
 
 
 


 