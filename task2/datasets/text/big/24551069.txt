Loudspeaker (film)
 
{{Infobox film
| name           = Loudspeaker
| image          = Loudspeaker (Film).jpg
| caption        = Theatrical poster 
| director       = Jayaraj
| producer       = Jayaraj M.P. Surendranth   (co-producer) 
| screenplay     = Jayaraj
| story          = Jayaraj  P. Y. Jose Sasi Kumar  Gracy Singh Jagathy Sreekumar Cochin Hanifa  Saleem Kumar  KPAC Lalitha
| music          = Bijibal
| cinematography = Gunashekhar
| editing        = Vijai Sankar
| studio         = New Generation Cinema
| distributor    = P. A. Sebastine Time Ads Entertainment
| released       =  
| runtime        = 121 minutes 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Sasi Kumar, Gracy Singh, Jagathy Sreekumar, Cochin Hanifa, Saleem Kumar, and KPAC Lalitha.

==Plot==
The movie is all about the relationships between two rare bred men from extremely different strata of society, but more in common than is apparent from the first look. They might have never run into each other, with their stark contrast in nature and life styles and would have hardly given each other a second look, if they hadnt met in these defined circumstances. But the movie throws them together, intertwined in worst of their days and progressively built in the rapport between the extremes, and infuses the spirit of worthy and meaningful life to all in the screen and those who  watches them on screen.

Mammootty  plays the lead role Mike Philippose, a barefooted uneducated simpleton who lands in the city, armoured with his loud voice, opinions and dazzling wit to stand up with the different level of etiquettes he find in the flats of the city. His arrival is as a kidney donor to ailing Anand Menon, an astrophysicist who returned to his home nation after working in US for the last forty six years. A dejected man who wants to run away from his memories, Menon is turned into the reverse track by Mike, who lives with his memories and snobs about his deceased father and about the silent hamlet of Thopramkudy. 
                 
The movie packs a lot of characters like the old man (Janardhanan) in legal tussle with his children, the fighting couple (Kalpana and Reghu) who even dont like to see face to face, a secretary of the flat residents (Jagathy) who only has a dog for his company, a group of loud bachelors and a kid who turns into a brat, left alone and away from her parents.

==Cast==
{| class="wikitable" border="1"
|-
!Characters
!Artists 
|-
|Peelipose/Mike  || Mammootty
|- Menon  Sasi Kumar
|- Annie || Gracy Singh
|- Sarangadharan || Cochin Haneefa
|- KP || Saleem Kumar
|- Secretary || Jagathy Sreekumar
|- Kunjannama || KPAC Lalitha
|- Ouseph || Babu Swami
|- Grandpa || Janardhanan
|- Madhavan Nair || Bheeman Raghu
|- Rugmini || Kalpana
|-
|Dr. Ollurkkaran || Anoop Menon
|-
|Dr. Philson || Subair, Alapuzha
|- Watch Man Augustine
|- Agent || Guinnes Pakru
|-
|Annies mother || Valsala Menon
|- Head Nurse || Sukumari
|-
|Counselor|| Suraj Venjaramood
|- Bachelors || Sreejith Ravi, Suraj,  
Romeo, Suraj Devaraj
|- Valliya Ammavan || Gopi Asan
|- Muthassan || Unnikrishnan Namboothiri
|- Muthassi || Rugmini Warassyar
|- Angela || Baby Nayanthara
|- Kathanar || Harisree Ashokan
|- Ambika Mohan
|- Abu Salim
|-
|}

== Reception==
The film was appreciated for its refreshing simplicity and did well at the boxoffice. 

==Soundtrack==
The songs of Loudspeaker were composed by Bijibal and lyrics were written by Anil Panachooran. "Alliyambal", a famous song from Rosy composed by Job Master and sung by K. J. Yesudas, is adopted in this film. The new version of the song is sung by his son, Vijay Yesudas. 

*"Changazhi Muthumayi" - Sung by Ganesh Sundaram
*"Kattarinu Thorathoru"- Jayachandran. Composer: Bijibal Manayil Lyrics: Anil Panachooran
Singer: P.Jayachandran
*"Manjinte Mamala"
*"Alliyambal" - Sung by Vijay Yesudas
*"Kattarinu Thorathoru (Female)"
*"Changazhi Muthumayi (Male)"

== References==
 

== External links ==
*  
*  

 
 
 
 