Were the World Mine
 
{{Infobox film
| name           = Were the World Mine
| image          = Were the world mine.jpg
| image_size     = 215px
| alt            = 
| caption        = Promotional film poster
| director       = Tom Gustafson
| producer       = Tom Gustafson Cory James Krueckeberg Peter Sterling
| executive producer       = Gill Holland
| writer         = Tom Gustafson Cory James Krueckeberg
| starring       = Tanner Cohen Wendy Robie Judy McLane Zelda Williams Jill Larson Ricky Goldman Nathaniel David Becker Christian Stolte David Darlow
| music          = Jessica Fogle   Tim Sandusky  
| cinematography = Kira Kelly
| editing        = Jennifer Lilly
| studio         = The Group Entertainment
| distributor    = SPEAKproductions
| released       =  
| runtime        = 95 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $123,789 
}} romantic Musical musical fantasy film directed by Tom Gustafson and written by Gustafson and Cory James Krueckeberg.

Were the World Mine is a story of gay empowerment, inspired by William Shakespeare|Shakespeare’s A Midsummer Night’s Dream. Were the World Mine stars Tanner Cohen, Wendy Robie, Judy McLane, Zelda Williams, Jill Larson, Ricky Goldman, Nathaniel David Becker, Christian Stolte, and David Darlow.

==Plot== rugby team, sexuality and with getting a job, and his father who is not a part of his life.

Timothy is cast as  ), guides Timothy towards the question of whether his actions have caused more harm than good.

==Cast==
* Tanner Cohen as Timothy
* Wendy Robie as Ms. Tebbit
* Judy McLane as Donna
* Zelda Williams as Frankie
* Jill Larson as Nora Bellinger
* Ricky Goldman as Max
* Nathaniel David Becker as Jonathon
* Christian Stolte as Coach Driskill
* David Darlow as Dr. Lawrence Bellinger
* Parker Croft as Cooper
* Brad Bukauskas as Cole
* Reid Dawson as Russ
* Alexander Aguilar as Taylor
* Yoni Solomon as Bradley
* Colleen Skemp as Becky
* Waymon Arnette as Henry
* Zach Gray as Ian
* Julia Black as Crystal
* Peggy Roeder as Coles Mother
* Paul Lisnek as a Newscaster

==Musical numbers==
# "Oh Timothy" – Jonathon
# "Pity" – Frankie
# "Audition" – Timothy
# "Be As Thou Wast Wont" – Timothy, Ms. Tebbit
# "Hes Gay" – Frankie
# "Were the World Mine" – Timothy, Jonathon
# "The Course of True Love" – Timothy, Frankie, Coach Driskill, Nora, Max, Donna
# "All Things Shall Be Peace" – Ms. Tebbit, Timothy
# "Sleep Sound" – Timothy
# "Pyramus and Thisbe" – Frankie, Cooper

==Production==
The film is a feature-length version of director Gustafsons 2003 short film, Fairies, which also stars Wendy Robie. The film was executive produced by Gill Holland, in association with The Group Entertainment.

==Release==

===Film festivals===
Were the World Mine has played or was scheduled to play many film festivals in prominent slots in 2008. Awards already won include: Audience Award for Best Narrative Feature at the   and Switzerland. The film was screened as the opening night gala at the 2009 Melbourne Queer Film Festival.

===Theatrical===
Were the World Mine had a limited release in North American theaters on November 21, 2008. 

===Reception===
The film currently holds a 68% Fresh rating on Rotten Tomatoes. 

==Soundtrack== The Magic Position"  by Patrick Wolf and "Rock Star" by The Guts – sung by Tanner Cohen. 

==Home media==
Were the World Mine was released on DVD in Europe on May 18, and in North America on June 9, 2008.

==Awards and nominations==
* Grand Jury Award for Outstanding U.S. Dramatic Feature (Heineken Red Star Award): Outfest 2008
* James Lyon Editing Award for Narrative Feature: 2008 Woodstock Film Festival
* Scion Award for First-Time Director: 2008 Philadelphia Intl Gay & Lesbian Film Festival
* Best Music in a Narrative Feature Film and Best LGBT Feature Film:  2008 Nashville Film Festival
* Directors Award: 2008 Connecticut Gay & Lesbian Film Festival
* Jury Award for Best Overall Film: 2008 Fort Worth Gay & Lesbian Film Festival
* Adam Baran Rainbow Award for Best Narrative Feature: 2008 Honolulu Rainbow Film Festival
* Jury Award for Best Feature Film: 2008 Outflix Film Festival

;Audience Awards
* Best Narrative Feature: 2008 Florida Film Festival
* Best Narrative Feature: 2008 Turin International Gay & Lesbian Film Festival
* Best Feature: 2008 Inside Out Toronto
* Best Feature: 2008 Kansas City Gay & Lesbian Film Festival
* Best Feature: Cinema Diverse 2008: Palm Springs GLFF
* Grand Prize Best Feature: Rhode Island International Film Festival 2008
* Best Feature: 2008 Vancouver Queer Film Festival

==References==
 

==Further reading==
* Padva, Gilad. Uses of Nostalgia in Musical Politicization of Homo/Phobic Myths in Were the World Mine, The Big Gay Musical, and Zero Patience. In Padva, Gilad, Queer Nostalgia in Cinema and Pop Culture, pp.&nbsp;139–172 (Palgrave Macmillan, 2014, ISBN 978-1-137-26633-0).

==External links==
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 