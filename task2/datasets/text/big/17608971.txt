Nuvvu Nenu
{{Infobox Film
| name        = Nuvvu Nenu Teja  Dasarath Gopimohan Anita  Sunil
| Teja
| producer    = P.Kiran
| distributor =
| cinematography = Rasool Ellore
| released    =  
| runtime     =
| editing     =
| language    = Telugu
| country     = India
| music       = R. P. Patnaik
|}}
 Teja with Anita played South Filmfare Awards and five Nandi Awards.

== Plot ==
Ravi, played by Uday Kiran, is the son of a multimillionaire in Hyderabad, India|Hyderabad. Vasundhara (Anita Hassanandani|Anita), is the daughter of a milk supplier. Ravi and Vasundhara both study in same college. The film starts with Vasundhara showing hatred towards Ravi, as he is a sportsman who is not good in his studies. Over a period of time, her hatred turns into love. Finally, they both fall in love, but their parents do not agree for their marriage. Parents plan to avoid the marriage by giving the couple a clause that they should not meet each other for one year. If their love remains even after one year then their parents would agree to let them marry.

Father of Ravi puts him under house arrest in Mumbai. While Vasundharas Father takes her to his hometown and arranges a forced marriage. However both Ravi and vasundara manage to escape. The rest of the story is about how the pair unites against all odds.

==Cast==
*Uday Kiran as Ravi Anita as Vasundhara Sunil as College Youth
*Banerji as Killer
*Anita as Yadav Girl
*Tanikella Bharani
*Radhika Chaudhari as Priya
*Supriya Karnik as Rajendars Friend
*M. S. Narayana
*Ahuti Prasad Rallapalli
*K.P.V. Prasad Rao as Rajendra
*Telangana Shakuntala Sangeetha
*Dharmavarapu Subramanyam

==Awards==
;Filmfare Awards South Best Film – P. Kiran Best Director Teja
* Best Actor – Uday Kiran Best Music Director – R. P. Patnaik

;Nandi Awards Best Director Teja
* Best Music Director - R. P. Patnaik Best Male Sunil
* Best Character Actor - Tanikella Bharani Best Cinematographer - Rasool Ellore

==Remakes== Madurai Veeran (2007) and in Bengali as Dujone (2009). While the  box office performances of Hindi and Bengali as poor, the later got blockbuster success. Anita Hassanandani  reprised her role in the Hindi version.

==References==
* http://www.fullhyderabad.com/profile/movies/1373/2/nuvvu-nenu-movie-review
* http://www.thehindu.com/arts/cinema/article496752.ece

==External links==
*  
 

 
 
 
 
 
 
 


 