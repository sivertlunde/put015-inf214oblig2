Dangerous Game (1987 film)
 
 
{{Infobox film
| name           = Dangerous Game
| image          = DangerousGame1987.jpg
| image_size     =
| caption        = DVD cover Stephen Hopkins
| writer         = John Erzine David Groom Stephen Hopkins Michael Ralph Peter West
| narrator       =
| starring       = Miles Buchanan Marcus Graham Steven Grives Kathryn Walker Sandy Lillingston John Polson
| music          = Peter Levy
| editing        = Tim Wellburn
| distributor    =
| released       = 1987
| runtime        = 98 minutes
| country        = Australia
| language       = English
| budget         = A$4.7 million 
| gross          = A$31,802 (Australia) 
| preceded_by    =
| followed_by    =
| website        =
| amg_id         =
}} Stephen Hopkins.

==Plot==
A young computer expert and his friends manage to disable a department stores security system so they can break in for the thrill of it. Once inside they find themselves stalked by a killer - a deranged police officer (Steven Grives) that the teens had caused to be suspended from his job earlier that day.

==Production==
The set built by Igor Nay was one of the largest ever built for an Australian film. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p244 

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 