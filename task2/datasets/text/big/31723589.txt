List of Kannada films of 1998
 
 
 Kannada film industry in India in 1998 in film|1998, presented in alphabetical order.

{| class="wikitable"
|-
! Title
! Director
! Cast
! Music
|-
| A (Kannada film)|A || Upendra || Upendra, Chandini || Gurukiran
|-
| Amar Akbar Anthony || Saagar || Arun Pandian, Thriller Manju, Vinod Alva, Sri Durga, Reshma || Rajesh Ramanath
|- 
| Andaman (film)|Andaman || P H Vishwanath || Shivarajkumar, Baby Nivedita, Sumanth, Shruthi Raj, Vinaya Prasad || Sadhu Kokila
|-
| Arjun Abhimanyu || Ramesh Babu || Jaggesh, Tiger Prabhakar, Payal Malhotra, Srikanya, Avinash || Rajan-Nagendra
|-
| Baaro Nanna Muddina Krishna || Prakash || Shashikumar, Anusha, Doddanna, Umashri, B. V. Radha || Rajesh Ramnath 
|-
| Bayalu Deepa || D A Upadhya || Sridhar (Kannada actor)|Sridhar, Sithara (actress)|Sithara, Bank Janardhan || Rajan-Nagendra
|- Shruti || Rajesh Ramnath
|- Vijayalakshmi || V. Manohar
|- Shilpa || Sadhu Kokila
|-
| Chor Guru Chandaal Shishya || A. R. Babu || Kashinath (actor)|Kashinath, Dheerendra Gopal, Charanraj, Charulatha || Sadhu Kokila
|-
| Daayadi || S. Umesh || Devaraj, Charanraj, Vajramuni, Vinaya Prasad, Swarna || Hamsalekha
|-
| Doni Saagali || Rajendra Singh Babu || Shashikumar, Soundarya, Suman Nagarkar, Shankar Ashwath || V. Manohar
|- Tara || Hamsalekha
|-
| Goonda Mathu Police || Vasu || Malashri, Bhanuchander, Doddanna, Avinash || Sadhu Kokila
|-
| Hello Yama || A. R. Babu || Kashinath (actor)|Kashinath, Doddanna, Sadhu Kokila, Rinki, Monisha, Lavanya || Sadhu Kokila
|- Ramakrishna || V. Manohar
|-
| Hoomale || Nagathihalli Chandrashekar || Ramesh Aravind, Suman Nagarkar, Kashi, Baby Hema || Ilayaraja
|-
| Hrudayanjali || Bhaskar || Raj Kamal, Charulatha, Umashree, Mandya Ramesh || Rajan-Nagendra
|-
| Jagadeeshwari || Rama Narayanan || Sai Kumar (Telugu actor)|Saikumar, Shruti (actress)|Shruti, Shamili, Archana || Sankar Ganesh
|-
| Jagath Kiladi || Chikkanna || Jaggesh, Charulatha, Pavitra Lokesh || Rajan-Nagendra
|-
| Jaidev || H Vasu || Jaggesh, Charulatha, Ashok (Kannada actor)|Ashok, Srinath, Doddanna || Rajesh Ramanath
|-
| Jai Hind || S S David || Devaraj, Nivedita Jain, B. C. Patil, Raghuvaran || V. Manohar
|-
| Kanasalu Neene Manasalu Neene || Nanjunda K || Vineeth, Prakash Raj, Ayesha Jhulka, S. P. Balasubramanyam || Chaitanya
|-
| Karnataka Police || J G Krishna || Devaraj, Bhanuchander, Yamini, Bhawyasri Rai || Sadhu Kokila
|-
| Kaurava || S. Mahendar || B. C. Patil, Prema (actress)|Prema, Bank Janardhan, Tennis Krishna || Hamsalekha
|- Deva
|-
| Kurubana Rani || D. Rajendra Babu || Shivarajkumar, Nagma, Lokesh|| V. Manohar
|-
| Maathina Malla || Yogesh Hunsur || Jaggesh, Vijayalakshmi (Kannada actress)|Vijayalakshmi, Charulatha || V. Manohar
|-
| Mangalyam Tantunanena || V S Reddy || V. Ravichandran, Ramya Krishna, S. P. Balasubramaniam || V. Manohar
|-
| Mari Kannu Hori Myage || Kumar || Jaggesh, Suvarna, Archana, Uttara, Dheerendra Gopal, Tennis Krishna || Rajesh Ramnath
|-
| Marthanda || Chikkanna || Tiger Prabhakar, Shruti (actress)|Shruti, Lokanath || Sadhu Kokila
|-
| Megha Bantu Megha || S. Mahendar || Ramesh Aravind, Shilpa (actress)|Shilpa, Archana, Ashalatha, Doddanna || V. Manohar
|-
| Mr. Putsami || V. Umakanth || Shivarajkumar, Suman (actor)|Suman, Laali, Srinath, Lokesh || Hamsalekha
|- Jayanthi || V. Manohar
|- Ramakrishna || Stephen - Dharma
|- Kasthuri || Sadhu Kokila
|-
| Preethsod Thappa || V. Ravichandran || V. Ravichandran, Shilpa Shetty, Lakshmi (actress)|Lakshmi, Lokesh, Prakash Raj, Vinaya Prasad || Hamsalekha
|-
| Shanti Shanti Shanti || Srinivas || R. Madhavan, Abbas (actor)|Abbas, Prema (actress)|Prema, Satish Shah, Bhavana Pani || Sandeep Chowta
|-
| Simhada Guri || U. Narayan Rao || Vishnuvardhan (actor)|Vishnuvardhan, Amulya, Charulatha || Shiva
|-
| Sri Siddharooda Mahatme || Bheemsen || Rajesh, Tara Venu|Tara, Sadashiva Brahmavar || G K
|-
| Suvvi Suvvalali || S. Mahendar || Ramesh Aravind, Shilpa (actress)|Shilpa, Charulatha || Hamsalekha
|-
| Tavarina Kanike || B. Gururaj || Ramkumar, Shruti (actress)|Shruti, Vanisri || V. Manohar
|-
| Thriller Killer || Thriller Manju || Thriller Manju, Akhila || Sadhu Kokila
|-
| Thutta Mutta || Kishore Sarja || Ramesh Aravind, Prema (actress)|Prema, Kasthuri (actress)|Kasthuri, Sujatha (actress)|Sujatha, Kashi || Hamsalekha
|-
| Vajra || V. Prabhakar || Ramkumar, Rasika, Doddanna, Bank Janardhan || Upendra Kumar
|-
| Veeranna || H Vasu || Jaggesh, Ravali (actress)|Ravali, Srinath, Tennis Krishna || V. Manohar
|- Vishnuvardhan || Hamsalekha
|-
| Yamalokadalli Veerappan || H. P. Prakash || Dheerendra Gopal, Sadhu Kokila, Akhila || Sadhu Kokila
|-
|}

==References==
 
 

== See also ==

* Kannada films of 1997
* Kannada films of 1999

 
 

 
 
 
 