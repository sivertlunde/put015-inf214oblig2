Bass Ackwards
{{Infobox film
| name           = Bass Ackwards
| image          = BassackwardsPOSTER.JPG
| image size     =
| alt            =
| caption        =
| director       = Linas Phillips
| producer       = Mark Duplass Marian Koltai-Levine
| writer         = Linas Phillips Davie-Blue Jim Fletcher
| starring       = Linas Phillips Davie-Blue Jim Fletcher
| music          = Lori Goldston Tara Jane ONeil
| cinematography = Sean Porter
| editing        = Brett Jutkiewicz Linas Phillips
| studio         = Furnace Films
| distributor    =
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
Bass Ackwards is a film written, starring and directed by Linas Phillips and also starring Davie-Blue, Jim Fletcher and Paul Lazar. The film has a running time of 103 minutes. 

The film stars Phillips as a man who embarks on a lyrical, strange and comedic cross-country journey in a modified VW bus after ending a disastrous affair with a married woman. 

Bass Ackwards was named an official selection in the 2010 Sundance Film Festival for inclusion in NEXT, a new category that recognized films for their innovative and original work in low-and-no-budget filmmaking, and is part of a wave of films that showcases the diversity of independent cinema. 

==Cast and crew==
*Linas Phillips (director, writer, "Linas") 
*Mark Duplass (executive producer) 
*Thomas Woodrow (producer) 
*Sean Porter (cinematographer, co-writer)
*Paul Lazar ("Paul", co-writer)
*Jim Fletcher ("Jim", co-writer)
*Davie-Blue  ("Georgia", co-writer)

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 


 