Love by the Light of the Moon
 
{{Infobox film
| name           = Love by the Light of the Moon
| image          = Still from 1901 film Love by the Light of the Moon.jpg
| image_size     =
| caption        =
| director       = Edwin S. Porter
| producer       =
| writer         =
| narrator       =
| starring       =
| cinematography =
| editing        =
| distributor    = Edison Manufacturing Company
| released       =  
| runtime        =
| country        = United States
| language       = Silent film
| budget         =
}}
Love by the Light of the Moon is a 1901 film by Edwin S. Porter, produced by the Edison Manufacturing Company. Niver, Kemp (1967). Motion Pictures From The Library of Congress Paper Print Collection 1894-1912. University of California Press, ISBN 978-0520009479  It mixes animation and live action and predates the man in the moon theme of the 1902 French science fiction film A Trip to the Moon by Georges Méliès.

==Plot==
The moon, painted with a smiling face hangs over a park at night. A young couple walking past a fence learn on a railing and look up. The moon smiles. They embrace, and the moons smile gets bigger. They then sit down on a bench by a tree. The moons view is blocked, causing him to frown. In the last scene, the man fans the woman with his hat because the moon has left the sky and is perched over her shoulder to see everything better.

==See also== By the Light of the Moon, a 1911 film also by Porter

==References==
 

==External links==
* 

 
 
 
 

 