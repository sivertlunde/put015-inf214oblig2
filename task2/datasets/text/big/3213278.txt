Surf Nazis Must Die
{{Infobox film
|  name           = Surf Nazis Must Die
|  image          = Surf Nazis.jpg
|  caption        = Poster for Surf Nazis Must Die
|  director       = Peter George
|  producer       = Robert Tinnell
|  writer         = Jon Ayre
|  starring       = Gail Neely Barry Brenner Robert Harden Tom Demenkoff
|  music          = Jon McCallum
|  cinematography = Rolf Kestermann
|  editing        = Craig A. Colton
|  distributor    = Troma Entertainment
|  released       = 1987
|  runtime        = 83 minutes
|  country        = United States
|  language       = English
|  budget         =
|  followed_by    =
|}}

Surf Nazis Must Die is a 1987 American comedy film directed by Peter George and starring Gail Neely, Barry Brenner, and Robert Harden. It was produced by The Institute, a production company formed by George, Craig A. Colton and Robert Tinnell, and distributed by Troma Entertainment, a company known for its low-budget exploitation films.

==Plot==
An earthquake leaves the California coastline in ruins and reduces the beaches to a state of chaos. A group of Neo-Nazis led by Adolf (Brenner), the self-proclaimed "Führer of the new beach," takes advantage of the resulting chaos by fighting off several rival surfer gangs to seize control of the beaches. Meanwhile, an African American oil well worker named Leroy (Harden) is killed by the surf Nazis while jogging on the beach. Leroys mother, "Mama" Washington (Neely), devastated by the loss of her son, vows revenge. After arming herself with a handgun and grenades, she breaks out of her retirement home and exacts vengeance on the Surf Nazis.

==Cast==
*Gail Neely as Eleanor "Mama" Washington
*Robert Harden as Leroy
*Barry Brenner as Adolf
*Dawn Wildsmith as Eva
*Michael Sonye as Mengele
*Joel Hile as Hook
*Gene Mitchell as Brutus
*Tom Shell as Smeg
*Bobbie Bresee as Smegs Mom
*Tom Demenkoff as Ariel

==Soundtrack==
The soundtrack to Surf Nazis Must Die was scored by Jon McCallum and features heavy use of synthesizers.  The soundtrack had an official vinyl release by Strange Disc records in September 2014 with cover artwork also by McCallum.

==Reception==
Surf Nazis Must Die was criticized by reviewers as boring and hard to follow, and its acting, dialogue, and camerawork were widely panned. Janet Maslin wrote "Not even the actors relatives will find this interesting."  Roger Ebert stated that he walked out of the film after 30 minutes. 

==Notes==
 
#  Maslin, Janet (October 2, 1987). "Film: Surf Nazis Must Die." The New York Times.
#  Ebert, Roger (May 11, 1987). "Surf Nazis washes up at Cannes." Chicago Sun-Times.
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 
 