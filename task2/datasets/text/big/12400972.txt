Please Vote for Me
 
{{Infobox film
| name           = Please Vote for Me
| image          = Please Vote for Me.jpg
| alt            =
| caption        =
| director       = Weijun Chen
| producer       = Don Edkins
| writer         =
| starring       =
| music          =
| cinematography =
| editing        = Jean Tsien
| studio         = Steps International
| distributor    = First Run Features
| released       =  
| runtime        = 58 minutes
| country        = China, Denmark
| language       = Mandarin
| budget         =
| gross          =
}}
Please Vote for Me is a 2007 documentary film following the elections for class monitor in a 3rd grade class of eight-year-old children in the Evergreen Primary School in Wuhan, China. The candidates, Luo Lei, Xu Xiaofei, and Cheng Cheng, compete against each other for the coveted role and are egged on by their teachers and doting parents. This was reported to be the first election of its type for a class monitor held in a school in China, as well as an interesting use of classic democratic voting principles and interpersonal dynamics. 
 documentary feature final five.
 PBS (as part of Independent Lens) in the United States.

== Notes ==
 

== Bibliography == The Asian Reporter, V17, #42 (October 16, 2007), pages 16 & 20.

== External links == PBS
*  
*   – official entry in "Why Democracy?" website
*  
*   page on MySpace

 
 
 
 
 
 


 