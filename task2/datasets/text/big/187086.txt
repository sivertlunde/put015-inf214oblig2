Caged
 
{{Infobox film
| name           = Caged
| image          = Caged1 1950.jpg
| image_size     = 250px
| alt            =
| caption        = Theatrical release lobby card John Cromwell
| producer       = Jerry Wald
| writer         = Bernard C. Schoenfeld Virginia Kellogg
| screenplay     = Virginia Kellogg
| based on       = Women Without Men by Kellogg and Schoenfeld
| starring       = Eleanor Parker Agnes Moorehead Ellen Corby Hope Emerson
| music          = Max Steiner
| cinematography = Carl E. Guthrie
| editing        = Owen Marks
| distributor    = Warner Bros.
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 John Cromwell and starring Eleanor Parker, Agnes Moorehead, Ellen Corby and Hope Emerson. It was nominated for three Academy Awards.

The movie tells the story of a teenage newlywed sent to prison for being an accessory to a robbery. Her experiences while incarcerated, along with the killing of her husband, change her from a frightened young girl into a hardened convict. 

Caged was adapted by Virginia Kellogg from the story "Women Without Men" by Kellogg and Bernard C. Schoenfeld. The studio had originally intended it as a vehicle for Bette  Davis and Joan Crawford, but reportedly Davis had said she didnt want to make a "dyke movie" (a movie with partial homosexual content) and turned it down.

==Plot==
A married 19-year-old, Marie Allen (Eleanor Parker), is sent to prison after a botched armed robbery attempt with her equally young husband, Tom, who is killed. While receiving her initial prison physical, she finds out that she is two months pregnant.

Marie has trouble adjusting to the monotonous and cutthroat world of the Incarceration of women|womens prison.  She meets Kitty Stark (Betty Garde), a murderous shoplifter, who says once Marie gets out, Kitty will get her a job "boosting," or shoplifting.  Marie does not want to get involved in crime, but Kitty explains the realities of prison life: "You get tough or you get killed. You better wise up before its too late."
 kills herself given the hopeless situation.  This saps Maries hopes of getting out early.
 sadistic matron Evelyn Harper (Hope Emerson), Marie gives birth to a healthy baby and wants to "temporarily" grant full custody to her mother. The intent is to get the baby back after she is released. Maries callous step-father has decided not to allow the baby into his house. Maries mother uses the excuses that shes "too old" and "hasnt a penny in   name" as reasons why she cant help Marie. Marie is denied a parole. She half-heartedly tries to escape, but is not punished. The prison forces her to permanently give the child up for adoption.
 Lee Patrick) sets off a rivalry with Kitty.  Elvira bribes Harper to put Kitty in solitary confinement, where Kitty is beaten.  When a kitten is found in the jail yard, Marie attempts to make it a pet.  When Harper tries to take the kitten away, a riot ensues and Marie is put into solitary confinement as well.

Before taking Marie to solitary confinement, Harper shaves Maries head, symbolically stripping her of her innocence. Harper has disagreements with the sympathetic reformist prison superintendent, Ruth Benton (Agnes Moorehead), especially after this latest incident with Marie. However, since Harper is a political appointee, the police commissioner refuses to fire her, and asks for Bentons resignation instead.  When Benton declares that shell request a public hearing, the resignation issue is dropped.

Out of solitary after a month, Kitty is distraught and mentally ill. After being picked on by Harper in the cafeteria, Kitty stabs Harper to death as the inmates watch and make no attempt to stop it. After her exposure to hardened criminals and sadistic prison guards, Marie actually urges Kitty on. 

Up for parole once again, Marie has found a "cashiers job" outside—actually just a ruse to join Elvira Powells shoplifting gang—and leaves prison a hardened woman after 15 months. Benton asks Marie why she is going into crime when she could go back to school. Marie says she got all the education she needed in prison.

==Cast==
 
 
* Eleanor Parker - Marie Allen
* Agnes Moorehead - Ruth Benton
* Ellen Corby - Emma Barber
* Hope Emerson - Evelyn Harper
* Betty Garde - Kitty Stark
* Sheila MacRae - Helen
 
* Jan Sterling - Jeta Kovsky aka "Smoochie" Lee Patrick - Elvira Powell
* Jane Darwell - Solitary Confinement Matron Gertrude W. Hoffmann - Millie
* Olive Deering - June Roberts
* Gertrude Michael - Georgia Harrison
 

==Critical reception==
Film critic Emanuel Levy praised the film stating: "master of melodrama, John Cromwell directs in a taut style, coaxing excellent performances from his female-dominated cast...Caged walks a fine line between a socially-conscious drama (and wake-up call) and exploitative, borderline campy fare." 

This film has an audience approval score of 85% on Rotten Tomatoes.
 Academy Awards==
;Nominations  Best Actress: Eleanor Parker Best Supporting Actress: Hope Emerson
*  , Bernard C. Schoenfeld

==References==
 

==External links==
 
*  
*  
*  
*  
*  

===Streaming audio===
* Eleanor Parker also stars in the radio drama version, episode #112 of the Screen Directors Playhouse which is a free download at the  

 

 
 
 
 
 
 
 
 