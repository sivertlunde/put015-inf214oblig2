Kutob
 
{{Infobox film
| name = Kutob
| image = Kutob.jpg
| caption = DVD cover
| director = Jose Javier Reyes
| producer = Canary Films
| writer = Jose Javier Reyes
| starring = Rica Peralejo Alessandra de Rossi
| distributor =
| released =  
| runtime = 101 minutes
| language = Tagalog
| country = Philippines
| budget =
}} 2005 Philippines|Filipino suspense horror film directed by Jose Javier Reyes; the title translates into "foreboding" in English. The film was a box office success in the Philippines, with both Reyes and Agustin also winning the Best Director and Best Actor awards respectively at the 2005 Metro Manila Film Festival; the film itself came third at the festival. The film contains elements of horror and the supernatural without being overtly of those genres, drawing comparisons in places to two US films; namely, Psycho (1960 film)|Psycho and Carrie (1976 film)|Carrie. 

==Awards==
{|| class="wikitable sortable"
|-
! width="10%"| Year
! width="30%"| Award-Giving Body
! width="25%"| Category
! width="25%"| Recipient
! width="10%"| Result
|- 2005
| rowspan="6" align="left"| Metro Manila Film Festival   Best Director
| align="center"| José Javier Reyes
|  
|- Best Actor
| align="center"| Marvin Agustin
|  
|- Best Editing
| align="center"| Vito Cajili
|  
|- Best Original Theme Song
| align="center"| Thor ("Kasalanan Nga Ba?") 
|  
|- Best Musical Score
| align="center"| Jaime Fabregas
|  
|-
| align="left"| Gatpuno Antonio J. Villegas Cultural Awards
| align="center"|Kutob
|  
|}

==References==
 

==External links==
* 
*  at Flix Unlimited


 
 
 
 
 

  