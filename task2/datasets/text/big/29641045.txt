Daayen Ya Baayen
{{Infobox film
| name           = Daayen Ya Baayen
| image          = DaayenYaBaayen.jpg
| caption        = Promotional poster
| director       = Bela Negi
| producer       = Sunil Doshi
| starring       = Deepak Dobriyal Manav Kaul
| writer         = Bela Negi
| music          = Vivek Philip
| cinematography = Amlan Datta
| editing        = Bela Negi
| studio         = Alliance Media & Entertainment Pvt. Ltd.
| distributor    = 
| released       =  
| country        = India
| language       = Hindi
}}
Daayen Ya Baayen  is a Hindi drama film, directed by Bela Negi and produced by Sunil Doshi. The film released on 29 October 2010 under the Alliance Media & Entertainment Pvt. Ltd. banner.  

==Synopsis==
A luxury car comes out of the blue into the life of a schoolteacher in a remote hilly village and takes him on an unexpected journey.

==Cast==
* Aditi Beri	         ...	Meena
* Bharti Bhatt         	 ...	Hema
* Jeetendra Bisht	 ...	Jwar Singh
* Dhanuli Devi		
* Aarti Dhami         	 ...	Deepa
* Deepak Dobriyal	 ...	Ramesh Majila
* Badrul Islam        	 ...	Basant
* Dilawar Karki 	 ...	Jwars Cronies
* Manav Kaul    	 ...	Sundar
* Dhananjay Shah	 ...	Brother
* Pratyush Sharma	 ...	Baju
* Girish Tiwari          ...	School Principal
* Manoj pathak           ...   junior artist

==References==
 

==External links==
*   at Bollywood Hungama
*  

 
 


 