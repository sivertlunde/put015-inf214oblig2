Tag 26
{{Infobox film
| name           = Tag 26
| image          =
| caption        =
| director       = Andreas Samland
| producer       =
| writer         = Andreas Samland
| narrator       = Peter Beck Ronnie Marzillier
| music          =
| cinematography = Max Penzel
| editing        =
| distributor    =
| released       =  
| runtime        = 18 min.
| country        = Germany German
| budget         =
| gross          =
}}
 Stalker as the main influence for his film.

Cast and Crew:  	
* Cast: Ronnie Marzillier, Peter Beck
* Screenwriter/Director: Andreas Samland
* Editor: Andreas Samland, Wolfgang Gessat
* Cinematographer: Max Penzel
* Sound: Martin Frühmorgen
* Production Design: Doerte Maria Schreiterer
* Producers: M. Knapheide

== Plot ==

Twenty six days after an undescribed biological disaster, two survivors thereof are forced to live in cumbersome protective biohazard suits and scavenge for food, water and fuel in a desolate dead land. The two follow the incomprehensible sounds of the radio in search of fellow survivors. They stop for supplies at a farm house. One of the characters suits is accidentally ripped, and when he is tested positively for poisoning he decides to remove his suit.  The other survivor prepares to leave, but when confronted with the prospect of spending the rest of his life alone and confined inside the biohazard suit, he chooses to return to his companion, who is already dying.  As his companion dies, he takes off the mask of his biohazard suit, dooming himself to die as well, but so he can share one final moment of humanity by letting his companion see him face to face before he dies.

==Awards==
*Grand Chameleon Award 2003 - Brooklyn International Film Festival
*Best Short 2003 (nominated) - Max Ophüls Festival
*European Broadcasters Award (Short Film) 2003 - Brussels International Festival of Fantasy Film
*Grand Prize 2004 (nominated) - Brussels International Festival of Fantasy Film.

==See also==

* Der Blindgänger
* Red Gourmet Pellzik

==External links==
*  
*  
*  

 
 
 

 