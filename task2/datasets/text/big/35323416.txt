A Respectable Life
{{Infobox film
| name           = A Respectable Life
| image          = 
| border         = 
| alt            = 
| caption        = 
| director       = Stefan Jarl
| producer       = Stefan Jarl
| writer         = 
| screenplay     = Stefan Jarl
| story          = 
| based on       =  
| narrator       =  Kenneth Gustafsson Gustav "Stoffe" Svensson
| music          = Ulf Dageby
| cinematography = Per Källberg
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 102 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
| gross          = 
}} Best Film Best Director. Best Foreign Language Film at the 52nd Academy Awards, but was not accepted as a nominee. 

==Synopsis== Dom kallar oss mods, has disappeared. Kenta is an alcoholic and lives with his girlfriend Eva. Together they have a son, Patric. Kentas mom is in jail for manslaughter and Kenta goes to Kronoberg to greet her. Heroin also comes to play and Stoffe is one of those who falls victim to it. He lives with his girlfriend Lena and their young son, Janne. Lena later throws Stoffe out their home when she gets enough of his abuse, and he is forced to live with his mother. Kenta calls Stoffe and decides to meet him, and he tries to persuade him to give up heroin, but the two have a falling out and they separate. This film features other users from the previous film, such as Jajje and Kenta Bergkvist. The film ends with the death of a prominent person in the trilogy.

==See also==
* List of submissions to the 52nd Academy Awards for Best Foreign Language Film
* List of Swedish submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 


 
 