The Wing or the Thigh
{{Infobox film
| name           = The Wing or the Thigh
| image          = Laile ou la cuisse.jpg
| writer         = Claude Zidi Michel Fabre
| starring       = Louis de Funès Coluche Julien Guiomar Martin Lamotte
| director       = Claude Zidi
| producer       = Christian Fechner
| cinematography = Wladimir Ivanov Claude Renoir
| editing        = Robert Isnardon Monique Isnardon
| distributor    = Films Christian Fechner
| released       = 27 October 1976
| runtime        = 104 minutes
| country        = France
| language       = French
| gross          = $43,814,670  
}}

The Wing or the Thigh, from the French Laile ou la cuisse (  is a 1976 French comedy film directed by Claude Zidi, starring Louis de Funès and Coluche.

==Plot==
Charles Duchemin (Louis de Funès) is the editor of an internationally known restaurant guide. After being appointed to the Académie française, Duchemin decides to retire as a restaurant critic and trains his son Gérard (Coluche) to continue the family business. However, Gérard Duchemin is more interested in his true passion—the circus—than high cuisine. Soon, however, Charles plans to retire are complicated by the arrival of Jacques Tricatel (Julien Guiomar), the owner of a company of mass-produced food. Fearing for the future of high cuisine, Charles and his son strive to ruin Tricatels company in any way they can. This movie is an allegory of the antagonism between USA culture and French culture as it was seen by French people in the 70s.

==Reception==
===Critical reception===
The film received a mixed critical reception.

===Awards===
The film received a Goldene Leinwand (Golden Screen) in Germany in 1978. 

== Trivia ==
  in the film.]]
* The Duchemin Guide is likely a reference to the real life Michelin Guide.
* Jacques Tricatel is likely a reference to  . Le Crocodile, a film that ended up never being made.

==References==
 

==External links==
*  
*  
*  
*  
*  at  

 

 
 
 
 
 
 
 
 


 
 