Back to the Beach
 
{{Infobox film
| name           = Back to the Beach
| image          = Back to the Beach film poster.jpg
| caption        = Theatrical release poster
| director       = Lyndall Hobbs
| producer       = Frank Mancuso, Jr.
| story          = James Komach Bruce Kirschbaum Bill L. Norton Christopher Thompson
| narrator       =
| starring       = Frankie Avalon Annette Funicello Lori Loughlin Connie Stevens Demian Slade
| music          = Steve Dorff
| cinematography = Bruce Surtees
| editing        = David Finfer
| distributor    = Paramount Pictures
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $13,110,903 
}}
 Siskel and Ebert.

The film is an open parody of the beach party movies made popular in the 1960s, especially those in which Avalon and Funicello had appeared. The plot is merely the means of connecting the various sight gags, homages and in-jokes.  All character names are taken from those earlier films.
 Private Domain.

==Plot==

Frankie Avalon and Annette Funicello are husband and wife living in Ohio — far from the surf and sand of their earlier lives together. Frankie is a stressed out car salesman and former "Big Kahuna" of the surf scene in California while Annette bottles her own sense of angst up in a bevy of shopping, and they are both raising a son, Bobby, who is in the throes of rebellion against his seemingly square folks. One day, the family decides to take a vacation to Hawaii, deciding to stop in California to visit their daughter Sandi (Lori Loughlin). Frankie and Annette are appalled to learn that she has been making time with surfer Michael (Tommy Hinkley) throughout her time there. The family misses their flight to Hawaii, and ultimately end up staying in California, much to the chagrin of Frankie. Frankie and Annette get caught up with the lives of their old friends and their old beach, and thus their last beach adventure begins. Along the way, Frankie must work together with a new generation of younger surfers while nearly ruining his marriage by dallying with Connie Stevens — one of several pop-culture icons appearing in the film, including Fishbone, Don Adams, Bob Denver, Alan Hale, Jr., Edd Byrnes, Jerry Mathers, Tony Dow, Barbara Billingsley, Dick Dale, Stevie Ray Vaughan, O.J. Simpson, and Pee-wee Herman. In the end The Big Kahuna overcomes his own fears and proves that he is still the king of surfers, as he takes back his title and saves the beach from a gang of beach punks.

==Soundtrack==

The soundtrack for this film was released in 1987 on CBS Records (CK-40892).  Track listing (key performers in parentheses):

# "Catch a Ride"
# "Pipeline (instrumental)|Pipeline" (Stevie Ray Vaughan & Dick Dale) (This track also appears on the Stevie Ray Vaughan album Solos, Sessions & Encores, and on King of the Surf Guitar: The Best of Dick Dale & The Del-Tones (Rhino 1989).)
# "Sign of Love" (Til Tuesday & Aimee Mann)
# "Absolute Perfection"
# "Surfin Bird" (Pee-wee Herman)
# "Sun, Sun, Sun, Sun, Sun"
# "Jamaica Ska" (Funicello & Fishbone) Wipe Out"
# "California Sun" (Avalon)
# "Wooly Bully"

Another song, "Well Go on Forever", sung by the cast, is not included on the album.

==Cast==
* Frankie Avalon - Annettes Husband
* Annette Funicello - Annette
* Lori Loughlin - Sandi
* Tommy Hinkley - Michael
* Demian Slade -  Bobby
* Connie Stevens - Connie
* Joe Holland - Zed
* John Calvin - Troy David Bowe - Mountain
* Laura Urstein - Robin
* Linda Carol - Bridgette
* Dick Dale - Himself
* Stevie Ray Vaughan - Himself
* Fishbone - Themselves
* Don Adams - Harbormaster
* Barbara Billingsley - Announcer
* Edd Byrnes - Valet
* Bob Denver - Bartender
* Tony Dow - Judge #1
* Alan Hale Jr. - Bartenders Buddy
* Jerry Mathers - Judge #2
* Paul Reubens - Pee-wee Herman

==Production==
The film was the idea of Frankie Avalon and was in development for a number of years. He hired several screenwriters and shopped the screenplay around town. Paramount was attracted to the project but did not like the script. Because Orion Pictures owned the rights to the original AIP beach party movies, Paramount wanted to make (in the words of one spokesperson) "an entirely original screenplay not based on any prior beach movies. It parodies all beach movies." 

Writer-director James Komack shared the same agent as Avalon; when he became attached Paramount agreed to finance. Komack:
 I met with Ned Tanen (Paramounts production chief) and we agreed it would be about a middle-age marital life crisis which, through a series of happy events, allows the couple to recapture their youth and renew the relationship. Eventually, they wanted a picture I couldnt deliver. They wanted to camp it up and I felt it wasnt necessary.  
Eventually Paramount Lyndall Hobbs to direct; she had never made a feature before but had directed numerous music videos. Various writers were hired, seventeen in all, including Jeff Buhia & Steve Zacharias, Robert Kaufman, David Obst and Bill Norton Jr.. This cost an estimated $2 million in writers fees.   05 July 1987: K21.] accessed 18 December 2014 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 