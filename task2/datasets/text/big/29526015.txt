Les Morfalous
{{Infobox Film
 | name = Les Morfalous
 | image = Lmorpos.jpg
 | caption = Original film poster
 | director = Henri Verneuil
 | producer = 
 | writer = Michel Audiard Pierre Siniac
 | starring = Jean-Paul Belmondo Michel Constantin Marie Laforêt Michel Creton Jacques Villeret
 | music = Georges Delerue
 | cinematography = Jacques Saulnier
 | editing = Pierre Gillette
 | distributor = Alain Belmondo Tarak Ben Ammar
 | released = 28 March 1984
 | runtime = 95 minutes
 | country = France
 | language = French
 | budget = 
 | gross = 3,612,400 admissions (France) 
}}
Les Morfalous (literally The Greedy-Guts, in French argot) is a 1984 French adventure film, starring Jean-Paul Belmondo and directed by Henri Verneuil, featuring the French Foreign Legion during the Second World War.

== Plot ==
In French Tunisia, during the Second World War, a convoy of the French Foreign Legion is charged to recover gold bars of six billion francs from a bank in El Ksour in order to bring them into a safe place for the French government.

The 4 April 1943, the contingent of the Foreign Legion enters the town of El Ksour which is partially destroyed. A German platoon, who holds the town, ambush the FFL convoy and kill most of them. Only 5 légionnaires survive the attack and take refuge in a hotel in ruins. At night, Légionnaire Borzik is killed while trying to bring weapons and ammunitions back to the rest of the team.

The 4 remaining légionnaires find the corpulent and pusillanimous artilleryman Beral (portrayed by Jacques Villeret) sitting in the toilets, suffering from dysentery. However, with his brave assistance, Sergent Augagneur (Jean-Paul Belmondo) used an abandoned French cannon to kill or drive out the Germans.
 Sergent Augagneur Adjudant Mahuzard (Michel Constantin) who wants to accomplish the mission.

Trying to steal the gold bars by heading South with a tank, Sergent Augagneur runs in an entire French army, marching North, and successfully pretends he was looking for them to give the gold bars back. Sergent Augagneur is finally awarded with the Légion dHonneur for this action.

==Cast==

* Jean-Paul Belmondo (Sergent Pierre Augagneur)
* Michel Constantin (Adjudant Edouard Mahuzard)
* Marie Laforêt (Hélène de La Roche Freon)
* Michel Creton (Légionnaire Boissier)
* Jacques Villeret (Brigadier Beral)
* François Perrot (François de La Roche Freon)
* Pierre Semmler (Capitaine Ulrich Dieterle)
* Maurice Auzel (Légionnaire Borzik)

==See also==

*French Foreign Legion in popular culture
*French cinema
==References==
 
== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 

 
 