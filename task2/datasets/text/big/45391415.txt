Super Hero Taisen GP: Kamen Rider 3
{{Infobox film
| italic title   = force
| name           = Super Hero Taisen GP: Kamen Rider 3
| film name = {{Film name| kanji = スーパーヒーロー大戦GP 仮面ライダー3号
| romaji = Supā Hīrō Taisen Guranpuri: Kamen Raidā Sangō}}
| image          = SHTGPposter2.jpg
| alt            =
| caption        = Theatrical poster
| director       = Takayuki Shibasaki
| producer       =  
| writer         = Shōji Yonemura
| narrator       = Tomokazu Seki
| starring       =  
| music          =  
| cinematography = Goro Miyazaki
| editing        = Naoki Osada Toei
| Toei Co. Ltd
| released       =  
| runtime        = 95 minutes
| country        = Japan
| language       = Japanese
| budget         =
| gross          =
}}
 Yuichi Nakamura (Kamen Rider Den-O), Kousei Amano, Takayuki Tsubaki, Ryoji Morimoto and Takahiro Hojo (Kamen Rider Blade), and Kento Handa (Kamen Rider 555) reprise their roles in the film, which is scheduled to open in theaters on March 21, 2015.   A new actor Mitsuhiro Oikawa, confirmed to perform his role as Kamen Rider 3, as well as the cast of Shuriken Sentai Ninninger will also appear.
==Plot== Shocker alters 1 and 2 back Shinnosuke Tomari Kiriko Shijima, remembers the truth and joins forces with the remaining Kamen Riders who are still at large to make a stand against Shocker and put history back on its proper course.

==Cast==
;Kamen Rider Series cast
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
* :  

;Super Sentai Series cast
* :  
* :  
* :  
* :  
* :  
* :  

;Super Hero Taisen GP cast
* :  
* :  
* :  

;Voice Cast
* :  
* , Drive Driver Equipment Voice:  
* :  
* :  
* :  
* :  
* :  
* :  
* :  
*Faiz Driver Equipment Voice:  
*Rouzer Voice:  
*Ninninger Equipment Voice:  
*Narration,  ,  ,  ,  ,  ,  ,  ,  ,  ,  :  

==Theme song==
*"Whos That Guy".
**Lyrics: Shoko Fujibayashi
**Composition & Arrangement: SAKOSHIN
**Artist: Mitsuhiro Oikawa

==Kamen Rider 4==
The first million ticket buyers will also receive a special DVD featuring the first episode of a three episode spin-off, titled  . d-Video will begin distribution of the first two episodes on March 28, 2015 and the final episode on April 4, 2015. Mitsuru Karahashi (Kamen Rider 555) reprises his role in the spin-off. Mitsuru Matsuoka performs his voice role as Kamen Rider 4 and also performs the spin-offs theme song "time" under the special band Mitsuru Matsuoka EARNEST DRIVE. The spin-off is directed by Kyohei Yamaguchi and written by Nobuhiro Mouri.

==References==
 

 
 
 
 
 
 


 
 
 
 
 