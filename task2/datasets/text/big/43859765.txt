Walt Before Mickey
{{Infobox film
| name           = Walt Before Mickey
| image          = Walt Before Mickey film poster.png
| image_size     = 
| alt            =
| caption        = Film poster
| director       = Khoa Le
| producer       = {{Plain list |
*Armando Gutierrez 
*Arthur L. Bernstein
}}
| screenplay     = {{Plain list |
*Arthur L. Bernstein
*Armando Gutierrez 
*Diane Disney Miller
}}
| based on       =  
| starring       = {{Plain list |
*Thomas Ian Nicholas
*Jon Heder
*Armando Gutierrez
*Jodie Sweetin
*David Henrie
*Hunter Gomez
* Arthur L. Bernstein
}}
| cinematography = Ian Dudley
| editing        = Lauren Young
| website = url|http://www.waltbeforemickey.com
| studio         = {{Plain list |
*Conglomerate Media
*Lensbern Entertainment 
}}
| released       = August 3rd 2015
| Distributor   = Mission Pictures International 
                         Voltage Pictures 
| runtime        = 110 minutes 
| country        = United States
| language       = English
}}
 Roy Disney. Roy Disney.  

==Plot==
Walt Before Mickey is about Walt Disney (Thomas Ian Nicholas) and his early beginnings in Marceline, Missouri. The story follows his poor beginnings and eventual move to Kansas City, where he created his first animation studio Laugh-O-Gram Studio. There he hired Rudy Ising, (David Henrie), and Ub Iwerks (Armando Gutierrez), to help start his business. Walt had various minor successes. Frank Newman (Arthur L. Bernstein) promoted Walts animations as Newmans Laugh O Grams which were an instant success; however due to Disneys lack of knowledge for making the animations at no profit he was forced to have Laugh-O-Gram file for bankruptcy. Iwerks then gives Walt his camera which he later sold to purchase a train ticket to Los Angeles.
 Roy Disney, (Jon Heder), to help fund his new company, Disney Brothers. Disney Brothers quickly sold an animation called Alice Comedies to a distributor named Margaret Winkler (Flora Bonfanti). The animation provided the Disney brothers with minor success. Walt then rehired his original team of animators which included Ub Iwerks, Fred Harmon, Rudy Ising, Hugh Harmon (Hunter Gomez), and Friz Freleng (Taylor Gray). The Disney brothers then began work on both the Alice Comedies and Oswald the Lucky Rabbit for their distributor Margaret Winkler. The company shortly thereafter began having disagreements with its distributor and lost the rights to both the Alice Comedies and Oswald the Lucky Rabbit. Various employees left Disney Brothers except for Ub Iwerks.

Walt Disney visited New York in an attempt to secure a deal for his company, but failed. On a train ride back with his wife Lillian Disney he created the concept of a mouse named Mortimer, which would become Mickey Mouse. Upon arriving in Los Angeles, Walt Disney gave the draft to Ub Iwerks who then completed the design for Mickey Mouse.

The film concludes with the premiere of Plane Crazy and with Walt Disney, Roy Disney, and Ub Iwerks leaving the movie premiere of their creation.

==Cast==

http://www.waltbeforemickey.com/cast.html

* Thomas Ian Nicholas as Walt Disney Roy Disney
* Armando Gutierrez as Ub Iwerks
* David Henrie as Rudy Ising
* Taylor Gray as Friz Freleng Charlotte Disney
* Hunter Gomez as Hugh Harmon
* Ayla Kell as Bridget
* Arthur L. Bernstein as Frank L. Newman
* Kate Katzman as Lillian Disney
* Conor Dubin as Charles Mintz
* Flora Bonfanti as Margaret Winkler
* Vicco von Bülow as Animator  Roy
* Walt  Walt  Ruth Disney
* Beatrice Taveras as Virginia Davis 
* Maralee Thompson as Dawn Paris Edna Disney
* Sheena Colette as Dessie 
* Gus Bilirakis as job applicant Tom Rooney as farmer  
*Bucky Dent as a father Uncle Robert Disney
* Donn Lamkin as Elias Disney

==Background==
Armando Gutierrez originally approached Diane Disney Miller and Timothy Susanin with the idea of
converting the book into a feature film in 2013.

The script took Arthur L. Bernstein and Armando Gutierrez one year to write.  The original title was called "The Dreamer".

===Setting=== Deland and Orlando, Florida.

==Production==

===Origin of the name "Walt Before Mickey"===
The film is based on the book with the same name "Walt Before Mickey" written by Timothy Susanin and Diane Disney Miller "Walt Disneys Daughter". The book was published by the University of Mississippi. The Original title of the movie was named the Dreamer which was written by Arthur L. Bernstein and Armando Gutierrez.

==References==
 

==External links==
 
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 