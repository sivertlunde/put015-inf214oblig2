Gini Avi Saha Gini Keli
{{Infobox film
| name           = Gini Avi Saha Gini Keli
| image          = Gini_Avi_Saha_Gini_Keli_Movie_Main_Banner.tiff
| caption        = Title Card 
| director       = Udayakantha Warnasuriya
| producer       = Ranjith Jayasuriya
| screenplay     = Anura Horatious Udayakantha Warnasuriya
| story          = Anura Horatious
| starring       = Jackson Anthony Sriyantha Mendis Palitha Silva Mahendra Perera Srinath Maddumage Rangana Premarathne Raja Sumanapala
| music          = Kala Keerthi Premasiri Khemadasa
| cinematography = Suneth Nandalal
| editing        = Stanley de Alwis
| casting        = Sanjaya Nipunaarachchi
| released       =  
| runtime        = 90 minutes
| country        = Sri Lanka
| language       = Sinhala
}}
Gini Avi Saha Gini Keli ( ,   and Fireworks) is a 1998 Sri Lankan epic crime film directed by Udayakantha Warnasuriya and produced by Ranjith Jayasuriya, based on a popular fictitious novel written by Anura Horatious. The plot revolves around the ascension and downfall of Padmasiri (Jackson Anthony), a gang leader in Sri Lanka. The film also provides a clear overview of the underworld thuggery and political corruption during the latter part of the 1980s in Sri Lanka.

Gini Avi Saha Gini Keli is widely regarded as one of the greatest films ever produced in Sri Lanka, mainly due to its depiction of the emerge of gang-based violence in the country. It was also selected to represent Sri Lanka in the 1998 Singapore International Film Festival.

==Plot==
Since the entire film is focused on the rise and demise of Padmasiri, the storyline will be described according to important milestones of his life.

===The Rise of Padme===
It was the 1980s. Padmasiri or "Padme" as known by his colleagues is having a friendly gathering with a mudalali businessman in his village. Padme whilst polishing his shotgun mentions to mudalali, his desire to shoot a fat-bellied man. Mudalali with a laugh tells Padme that it could easily be done by making a visit to the town at night time. Padme then shoots mudalali with anger, shouting only he, himself can have a butchery in town. This marks the beginning of a criminal; a criminal who would later take several lives to fulfil his lifelong ambition; that is to become the grandfather of Sri Lankan mafias.

Meanwhile a new inspector of police (Rangana Premarathne) was appointed to the village. Unlike previous police chiefs, he was a young, brave and a strict officer of law who would go to great lengths to bring law and order. Soon Padme receives a message to go and visit the new inspector. With anger Padme visits the police station. He was asked politely by the inspector to stop selling illicit liquor. Padme nods with a smile and leaves the station. Next day, the inspector receives an invitation by Padme to come and visit him in his residence. Ignoring warnings and comments the inspector bravely visits Padme, because it was a very polite invitation. After offering some refreshments to the Policemen, Padme takes them to his garden. A car key was handed over to the inspector, mentioning that he always treats well policemen who visits Padme in person. He also mentions that the car itself was bought using money generated through his illicit businesses. The inspector with anger, shouts at Padme in-front of Padmes gang, whilst putting him down on top of the car. With vigour, the inspector yells at Padme to stop immediately all his illicit businesses. He also threatens to set the whole place on fire, if his order is not carried out within 24 hours.

Soon after the police jeep left, Padme yells at villagers to make a pile of all cans and containers in front of his house. He then starts making a public speech, criticising the Police department. The news of Padme having a public gathering without permission travels to the Police station immediately. The inspector with several constables decides to visit Padme once again, however armed with firearms. Soon a violent clash occurs between the inspector and Padme, soon after Padme refused to cancel his speech and surrender to Police. The inspector however wins from the clash with Padme. He was put behind bars, soaked in his own blood.

Meanwhile Padmes uncle (Raja Sumanapala) visits the local member of parliament (Member of Parliament|MP), someone whom Padme has helped to win the election to release Padme from the Police Detention of suspects|remand. Soon Padme was bailed-out. The inspector was also transferred to another Police station, however with a promotion. On the day the inspector was leaving the village for good, he pays a private visit to Padmes residence. The ex-inspector advises  Padme to stop selling illicit liquor to people and try and become a good person. Padme with a smile praises the ex-inspector, and finally tells him that they promoted him, mainly because the ex-inspector is a valued true hero.

===Padme vs. Addarawaththe Lokayaa (the World-Thug)=== custody by the Police when caught with drugs.

After a series of confrontations between the two, Padme befriends a petty thug in the area named "Rambo"; in order to kill Lokayaa. Padmes efficient exploitation of Rambos anger against Lokayaa is evident when the thug was cut into pieces by Rambo whilst walking home alone on a night after visiting a brothel.

===Politicising crimes===
Soon after the killing of Lokayaa, Padme was approached by many politicians to kill their opponents. Padme eagerly takes such contracts in order to earn quick money. He did many violent killings under the safety net of politicians. The anti-government youth movement during the time was also blamed for many of Padmes executions.

However Padme finds out soon that he cannot protect himself without the help of even more powerful MPs, when Rambo was killed violently by gang maintained by another politician. With hesitation he leaves the local MP who was in the opposition party (to whom Padme worked so far) and joins a ruling party MP. This however angers the gang maintained by the same MP. It is further revealed at the latter part of the film that Padme was also made a Justice of the Peace (JP) by the MP he works for.

===The Demise of Padme===
The now defunct gang, previously maintained by the same MP to whom Padme now works for; hires an assassin to kill Padme under the financial backing of a disgruntled social club owner. Disguised as a Police messenger, they ask Padme to report to the inspector in the nearby Police station. Padme then visits the station the next day with his nephew, only to find out that such a message was never sent.

On his way out, Padme was interrupted by a youth, dressed in white. After getting the confirmation from Padme of his identity, the young man shoots directly at Padme using a revolver many times, one at his head at and a few at his chest. Whilst Padmes young nephew tries to wake the dead body of Padme, the smiling assassin tries to escape by getting onto his motorcycle. However he too gets shot by the Police.

The film ends with Padmes nephew crying at his uncles slain body. The audience is notified in ending credits that the story continues.

==Cast==
*Jackson Anthony as Padmasiri (a.k.a. Padme)
*Sriyantha Mendis as Addarawaththe Lokayaa
*Palitha Silva as the leader of the gang maintained by a powerful politician (the same politician to whom Padme will later report to)
*Mahendra Perera as the assistant leader of the same gang which Palitha Silva belongs to
*Srinath Maddumage as Rambo
*Rangana Premarathne as the young law abiding inspector of Police who remands Padme for the first and only time
*Raja Sumanapala as the uncle who encourages and helps Padme. He is the link between Padme and politicians.

==Soundtrack==
The score was composed by Kala Keerthi Premasiri Khemadasa and includes a song popular to this day; sung by Amarasiri Peiries named "Divimakulu Esa" (a poisonous spiders eye). The song was written by Lucian Bulathsinghala and composed by Premasiri Khemadasa.The majority of the score consists of strong orchestral violin music, mixed with soft flute riffs which play along major events in the film.

==See also==

 

==External links==
* 
* 
*  at the Internet Movie Database
* 

 