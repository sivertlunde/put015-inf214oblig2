Breaking the Rules (film)
{{Infobox film
| name           = Breaking the Rules
| alt            =  
| image	=	Breaking the Rules FilmPoster.jpeg
| caption        = 
| director       = Neal Israel
| producer       = Kent Bateman Jonathan D. Krane
| writer         = Paul W. Shapiro
| starring       = Shawn Phelan Jackey Vinson Marty Belafsky Jason Bateman C. Thomas Howell Jonathan Silverman
| music          = David Kitay
| cinematography = James Hayman
| editing        = Tom Walls
| studio         = 
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Breaking the Rules is a 1992 drama film directed by Neal Israel, executive produced by Larry A. Thompson,  starring Jason Bateman, C. Thomas Howell, Jonathan Silverman and Annie Potts.  Jasons father, Kent Bateman, has a role in the movie as well.

==Plot==
Phil (Jason Bateman) a cancer stricken man tricks his two best friends whom he hasnt seen in a long time to go on a road trip, by inviting them to a fake engagement party. This has the potential for problems because Gene (C. Thomas Howell) once stole Robs girlfriend. Phil gets them to be friends again. He tells them of his illness and all three decide to go to Los Angeles for Phils dying wish; to be a contestant on "Jeopardy." On the way they meet an attractive wild woman with a heart of gold.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 


 