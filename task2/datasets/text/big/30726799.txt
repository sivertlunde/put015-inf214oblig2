Angaaray (1986 film)
{{Infobox film
| name           = Angaaray
| image          = Angaaray1986film.jpg
| caption        =
| director       = Rajesh Sethi
| producer       = Gurdip Singh
| writer         = Salim Khan (dialogues)
| story          = Salim Khan Bindu Iftekar Alankar Joshi Satyendra Kapoor Dina Pathak Pradeep Kumar Nirupa Roy Prema Narayan
| music          = Anu Malik
| dialogue = Salim Khan
| screenplay       = Salim Khan
| lyrics   = Rajendra Krishan Indeevar
| released       =  
| runtime        =
| country        = India Hindi
| budget         =
| gross          =  8.79 Crores
}}
Angaaray ( ;  ) is a 1986 Hindi language|Hindi-language film directed by Rajesh Sethi and starring Rajesh Khanna, Smita Patil and Raj Babbar in the main lead roles.The dialogue, screenplay and story has been written by Salim Khan.  

==Plot==
Aarthi (Smita Patil) is an educated girl living with her young brother Sanjay Verma (Alankar Joshi) and her mother. She joins as the personal secretary to Jolly (Shakti Kapoor). Meanwhile Vijay (Raj Babbar) develops a desire that he should marry Aarthi, although he does not propose to her directly. One day Vijay finds that Aarthis brother is a drug addict and promptly informs Aarthi. She convinces her brother to join the army so that he becomes a disciplined person in his life. After a few days Jolly tries to rape Aarthi and Vijay comes to the rescue.

Meanwhile Aarthi participates in a function where she is supposed to dance and there she is noticed for her dancing skills by the audience and from the audience section. After seeing her performance, Vijays friend Ravi falls in love with Aarthi. Ravi even secures permission from his and her parents to marry her and gets engaged to her and after that Ravi goes abroad. However, Jolly manages to find a chance to rape Aarthi traumatizing her. On learning the truth, Ravis parents cancel Ravis marriage to Aarthi and at this juncture Aarthi finds out that Vijay had loved her the whole time. Vijay offers to marry her but his own mother disapproves of this alliance upon knowing that Aarthi is a girl who was raped in the past. Disillusioned, Aarthi goes and meets the courtesan Hirabai (Bindu (actress)|Bindu) and becomes a Tawaif. After few months her brother returns to the town and after becoming aware of the events which took place, decides to kill Jolly. The plan backfires and Jolly kills him. To take revenge, Aarthi murders Jolly. The rest of the story is about how the court cases happens and what sentence is pronounced for Aarthi and whom will she marry at last.

==Reception==
The box office collections were Rs. 8.79 crores in 1986.It received three and half stars in Bollywood guide Collections.

==Cast==
*Rajesh Khanna  as  Ravi
*Smita Patil  as   Aarthi
*Raj Babbar  as  Vijay Bindu  as  Hirabai
*Shakti Kapoor  as  Jolly
*Alankar Joshi  as  Sanjay Verma
*Iftekar  as  Khan Chacha
*Pradeep Kumar  as  Police Commissioner

==Soundtrack==
{{Track listing
| extra_column = Singer(s)
| all_lyrics        = 
| all_music       = Anu Malik
| title1              = Ek Tahzeebon Ka Sangam Hai | extra1 = Kavita Krishnamurthy
| title2              = Mubarak Ho Mubarak Ho | extra2 = Suresh Wadkar, Mohammad Aziz, Anuradha Paudwal
| title3              = Mujhe Zindagi Ne Mara | extra3 = Asha Bhosle
| title4              = Pyar Kahte Hai Jise | extra4 = Kishore Kumar
| title5              = Tauba Tauba Karoge | extra5 = Asha Bhosle
}}

==References==
 

 
 
 
 