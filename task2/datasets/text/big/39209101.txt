Lummox (film)
{{infobox film
| name           = Lummox
| image          =
| imagesize      =
| caption        =
| director       = Herbert Brenon Ray Lissner (assistant)
| producer       = Joseph M. Schenck
| writer         = Fannie Hurst (novel, dialogue) Elizabeth Meehan (adaptation)
| starring       =  Winifred Westover
| music          = Jack Danielson Hugo Riesenfeld
| cinematography = Karl Struss
| editing        = Marie Halvey
| studio         = Feature Productions
| distributor    = United Artists
| released       = January 18, 1930
| runtime        = 88 minutes
| country        = USA
| language       = English intertitles

}}
Lummox is a 1930 sound film directed by Herbert Brenon, released through United Artists, and based on a 1923 novel by Fannie Hurst.   

==Cast==
*Winifred Westover - Bertha Oberg
*Dorothy Janis - Chita (*billed as Dorothy King)
*Lydia Yeamans Titus - Annie Wennerberg
*Ida Darling - Mrs. Farley
*Ben Lyon - Rollo Farley
*Myrta Bonillas - Veronica Neidringhous
*Cosmo Kyrle Bellew - John Bixby
*Anita Bellew - Mrs. John Bixby
*Robert Ullman - Paul Bixby
*Clara Langsner - Mrs. Wallenstein Sr.
*William Collier, Jr. - Wally Wallenstein
*Edna Murphy - May Wallenstein
*Torben Meyer - Silly Willie
*Fannie Bourke - Mrs. McMurtry
*Myrtle Stedman - Mrs. Ossetrich
*Danny OShea - Barney
*William Bakewell - Paul Charvet
*Sidney Franklin  - Mr. Meyerbogen Dickie Moore - Bit
*Billy Seay - Petey

==Preservation status== Movietone soundtrack, however, discs were prepared for theaters not yet wired for sound-on-film.

==References==
 

==External links==
* 
* 

 

 
 
 
 
 


 