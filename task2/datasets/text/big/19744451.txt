Gopura Vasalile
{{Infobox film		
| name = 	Gopura Vasalile	
| producer = M. K. Muthu|Mu. Ka. Thamizharasu
| image = 	Gopura Vasalile dvd.jpg	
| caption = 	Official DVD Cover	
| director = 	Priyadarshan   
| writer = Gokula Krishna (dialogues)
| screenplay = Priyadarshan Sreenivasan
| narrator =		 Karthik  Suchitra  Janagaraj    Charle   Junior Balaiya	
| music = Ilaiyaraaja		
| cinematography = P. C. Sreeram		
| editing = K. S. Gopalakrishnan
| studio = Arul Nithi Films
| distributor = Arul Nithi Films 		
| released = 22 March 1991
| runtime = 		
| country = India
| language = 	Tamil	
| budget = 		
| gross = 			
}}		 Indian Tamil Karthik and Bhanupriya in the lead with Suchitra (actress)|Suchitra, Nassar, Janagaraj (actor)|Janagaraj, Charle and Junior Balaiya forms an ensemble cast while V. K. Ramasamy (actor)|V. K. Ramasamy, Sukumari and Poornam Vishwanathan appearing in supporting roles.

Malayalam actor Mohanlal made a special appearance in a song. Music by Ilaiyaraja while the cinematography by P. C. Sreeram.

The story of the film borrows some subplots from the Malayalam film Pavam Pavam Rajakumaran.  The film is one of the earliest examples of black humour in Tamil cinema. The film was well received at the box office and ran more than 100 days at many centres in Tamil Nadu.

==Plot== Suchitra in a car accident. His friends are wastrels who roam about the town, flirting with the local girls. During one such dalliance with an army officer, VK Ramaswamys daughter, Bhanupriya, they are put behind the bars for a few days. Karthik also gets peeved with their behaviour and reprimands them for their uncouth behaviour.

Humiliated, the friends decide to wreak their revenge on Karthik. They collude with Janagaraj, a clerk in a local bank and attempt to trick Karthik into believing that Bhanupriya is in love with him. A few anonymous letters later, the plan comes out successful.
 subterfuge and cancels the plans of marriage.
 foul play was decoded. Karthik pardons the friends as they had played some role in developing a love with his wife  and invites them to a banquet at his new house atop a hill.

==Cast== Karthik as Manohar
* Bhanupriya as Kalyani Suchitra as Kasthuri
* Nassar Janagaraj
* Charle
* Junior Balaiya
* V. K. Ramasamy (actor)|V. K. Ramasamy
* Nagesh
* Sukumari
* Poornam Vishwanathan
* Mohanlal in special appearance in Kehladee En song

==Soundtrack==
{{Infobox album Name     = Gopura Vasalile Type     = film Cover    =  Released =  Artist   = Ilaiyaraja Genre  Feature film soundtrack Length   = 26:32 Label    = Echo
}}
 Vaali and Piraisoodan.

*Kadhal Kavithaigal - S. P. Balasubramaniam, K. S. Chitra
*Kehladee En - S. P. Balasubramaniam
*Naatham - K. J. Yesudas, S. Janaki
*Priyasagi - Mano (singer)|Mano, S. Janaki
*Thevadhai Pohl - Malaysia Vasudevan
*Thalattum Pongkaatru - S. Janaki

==References==
 
*  
*  

==External links==
* 
* 
*  -   -  			
* 

 

 
 
 
 
 
 
 