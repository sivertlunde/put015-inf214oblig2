Champagne for Caesar
{{Infobox film
| name           = Champagne for Caesar
| image          = Champagne-for-caesar-1950.jpg
| caption        = Film poster
| director       = Richard Whorf
| producer       = George Moskov Leo C. Popkin
| writer         = Fred Brady and Hans Jacoby
| narrator       = 
| starring       = Ronald Colman Celeste Holm Vincent Price
| music          = Dimitri Tiomkin
| cinematography = Paul Ivano
| editing        = Hugh Bennett
| distributor    = United Artists
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Champagne for Caesar is a 1950 American comedy film about a television quiz show, directed by Richard Whorf and written by Fred Brady and Hans Jacoby. The movie stars Ronald Colman, Celeste Holm, Vincent Price, Barbara Britton and Art Linkletter. The film was produced by Harry M. Popkin for his Cardinal Pictures and released by United Artists.

==Plot==
Beauregard Bottomley (Ronald Colman) is an unemployed PhD physicist who lives in Los Angeles with his piano-instructor sister Gwenn (Barbara Britton) and the alcohol-guzzling parrot of the films title, Caesar.  Beauregard and Gwenn live in a bungalow court, surrounded by books.  Beauregard is an omnivorous reader, and is highly knowledgeable on virtually any subject - except, as he admits, how to hold a job.  Beauregard obtains word of a mysterious job opportunity at the Milady Soap Company.  He meets the eccentric company owner, Burnbridge Waters (Vincent Price), for the job interview.  Waters disapproves of Beauregards humour and turns him down for the job, humiliating him in the process.
 The $64 Question. If anyone misses just one question, the contestant loses all that was won to that point.  Beauregard is contemptuous of television and what he deems as its anti-intellectual nature, after seeing Masquarade for Money.

However, after seeing the contestants struggle to answer relatively easy questions, Beauregard plots revenge on Burnbridge Waters.  He goes on the program dressed as an encyclopedia to let the host know he will answer any question given to him by the shows host, Happy Hogan (Art Linkletter).  Starting from an initial $5 prize, Beauregard easily answers the maximum five questions, then asks for a sixth question.  The show runs out of time, but Beauregard promises to come back next week if people will write in to Milady Soap demanding he be allowed to return.

An avalanche of mail inspires Burnbridge Waters.  He decides to call Bottomley back for one question per show - and when theyve reached a new high in soap sales, the show will drop him.  Masquerade for Money becomes a ratings triumph.  The country tunes in each week to watch Beauregard, who appears with Caesar on the covers of Look and Life Magazines.  The questions become more erudite and challenging, but Bottomley keeps answering them with ease.

With Beauregards continuing success and increasing prize amount, Waters becomes uneasy, as the winnings will potentially erode Miladys profits. Visiting the soap factory one day, Beauregard finally explains to Waters that his ultimate goal is, first, to break Waters by winning $40 million - the entire worth of Milady Soap - and thus drive Waters into bankruptcy.  As Beauregard continues to answer the questions, Waters calls in "Flame" ONeill (Celeste Holm) to try to weaken Waters via seduction, to find a subject where his knowledge is lacking.  In parallel, Happy Hogan tries to work his charms on Gwenn to see if he also can persuade her to have Beauregard simply pocket his winnings to date, by taking piano lessons with her and asking her out.

Beauregard has become ill with a cold, and Flame insinuates herself into the Bottomley household as Beauregards nurse, pretending to be a member of his fan club.  With little past experience with women, Beauregard initially succumbs to Flames charms, and loses his general sang-froid in her presence.  Gwenn is suspicious of Flames motives, as Beauregard is suspicious of Hogans motives in suddenly taking piano lessons with her.  Neither sibling is to be deterred by the other from their respective romantic attachments.  Flame, a la Delilah, intends to seduce Bottomley to find out his Achilles heel, a subject that will stump him.  Slowly, Beauregard becomes suspicious of Flame, after his recovery.  He reveals to her on one date that he (supposedly) never quite mastered Albert Einsteins theory of relativity.

With that knowledge, the next question on Masquerade for Money is for Beauregard to explain Einsteins theory.  Now realizing Flames role in the situation, Beauregard does struggle for an answer, which he provides.  However, Happy Hogan indicates that, by his answer card, the response is wrong.  Waters rejoices as the studio audience groans, and Beauregard wanders into the wings, apparently defeated.  However, a phone call arrives a moment later, from Einstein himself, who has been watching the show.  Einstein explains that in fact, Bottomley gave the correct answer. Hogan announces the news to a now jubilant audience.  Waters faints, and Bottomley is to return next week.

In the meantime, Happy appears to have genuinely fallen in love with Gwenn.  Flame also has apparently developed some remorse at her part in the plot against Beauregard, and also affection for him.  Waters decides to rent the Hollywood Bowl for the final question of the final show.  Before the last show, with the prize stakes at $20 million, Beauregard and Gwenn decide to test each others romantic partners and respective engagements.  Each sibling telephones the respective partner to see if they would be willing to be married before the big prize day.  Happy and Flame each come up with excuses for not marrying Gwenn and Beauregard before the last prize show, which deflates both siblings.

At the Hollywood Bowl, after a sardonic introduction from Waters, Beauregard comes onstage, where Happy asks for his wallet.  Happy then asks a very simple question about Beauregard himself, and not about any subject in a book, namely, what is Beauregards Social Security number.  Beauregard answers incorrectly, to the tremendous relief of Waters and the disappointment of the crowd.  Back home, Beauregard and Gwenn appear disappointed.  However, later that evening, Happy and Flame appear, willing to marry Gwenn and Beauregard respectively after all.  Waters also appears, with gifts for Beauregard.  It turns out that Caesar used to be Waters pet parrot.  As Beauregard and Flame drive off to be married, Beauregard reveals that he and Waters agreed to a backroom deal where he would lose, but receive his own radio show, as well as Milady Soap stock.  However, Beauregard then admits that he genuinely didnt know the answer to the simple question posed to him at the Hollywood Bowl.  Even so, most anyone watching the movie might well think Beauregard must have known the answer, or else he could not have completed any job applications requiring that information.

==Discussion==
In his 1950 review of the film in The New York Times, Bosley Crowther speculated on the origin of the films premise:
 New Yorker cartoon in which a triumphant quiz-show contestant, surrounded by loot which she had won, was ruefully being announced the winner of control of the sponsors company. It was a very funny cartoon — a very shrewd and fantastic conceit — in which the monstrous extravagance of quiz shows was swiftly and neatly ridiculed. And apparently Hans Jacoby and Fred Brady remembered it, too, for their screen play for Champagne for Caesar is a mere elaboration of this idea."  

However, contemporary criticism of the story indicated that the film violated Beauregards criticism of the nature of quiz shows in its final plot twist, with Beauregard being given his own quiz show   - unless Beauregard were to use the opportunity to reform quiz shows by having one that is truly intellectually stimulating.

Nicholas Laham has analysed the treatment of Beauregard as a highly educated, yet unemployable, character in the context of how scholars were regarded in the 1950s, and in anticipation of the unemployment of information-based, highly educated people in later decades in the information age/"new economy".  Laham also places Champagne for Caesar in the historical lineage of screwball comedy, over a decade after that genre had reached its peak before World War II. 

==Cast==
{|
|valign="top"|
* Ronald Colman (Beauregard Bottomley)
* Celeste Holm (Flame ONeill)
* Vincent Price (Burnbridge Waters)
* Barbara Britton (Gwenn Bottomley)
* Art Linkletter (Happy Hogan)
* Gabriel Heatter (Announcer)
* George Fisher	(Announcer)
* Byron Foulger (Gerald)
* Ellye Marshall (Frosty)
* Vicki Raaf (Waters Secretary)
* John Eldredge	(Executive #1)
|valign="top"|
* Lyle Talbot (Executive #2)
* George Leigh (Executive #3)
* John Hart (Executive #4)
* Mel Blanc	(Caesar (voice))
* Peter Brocco (Fortune Teller)
* Brian OHara (Buck (T Man))
* Jack Daly (Scratch (T Man))
* Gordon Nelson	(Lecturer)
* Herbert Lytton (Chuck Johnson)
* George Meader	(Mr Brown)
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 