The Jackeroo of Coolabong
 
{{Infobox film
| name           = The Jackeroo of Coolabong
| image          = 
| image_size     =
| caption        = 
| director       = Wilfred Lucas
| producer       = E. J. Carroll Snowy Baker
| writer         = Bess Meredyth
| narrator       =
| starring       = Snowy Baker Kathleen Key
| music          =
| cinematography = 
| editing        = Dudley Blanchard Southern Cross Picture Productions 
| distributor    = Union Theatres
| released       = 16 October 1920
| runtime        =5,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         =
| preceded_by    = 
| followed_by    = 
}}

The Jackeroo of Coolabong is a 1920 Australian silent film starring renowned Australian sportsman Snowy Baker. It was the last of three films he made with the husband and wife team of director Wilfred Lucas and writer Bess Meredyth, both of whom had been imported from Hollywood.

It is considered a lost film. 

==Synopsis==
Brian OFarrell (Snowy Baker), is an English new chum who takes a job at an Australian cattle station. He is teased by station hands because of his appearance (including spats and a monocle) but he soon impresses them with his skills at riding and boxing. The station manager, John MacDonald (Wilfred Lucas), takes OFarrell to Sydney to meet his daughter Edith (Kathleen Key) who is working in the slums. Edith is kidnapped by criminals after witnessing a crime but OFarrell rescues her. It is later revealed he is the owner of the station. 

==Cast==
*Snowy Baker as Brian OFarrell
*Kathleen Key as Edith MacDonald
*Wilfred Lucas as John MacDonald
*Arthur Tauchert
*Bernice Vere
*Arthur Greenaway

==Production==
Kathleen Key was imported from the US to play the female lead. 

Shooting took place in June 1920. During filming a kangaroo hunt in Narrabri, an extra, Nellie Park, fell off her horse and died of a fractured skull several days later. 

==Release==
The film was re-edited and released in the USA as The Fighting Breed. Like all the Baker-Meredyth-Lucas collaborations, it was successful at the box office overseas, but returns did not come in quickly. 

During filming E. J. Carroll clashed with Wilfred Lucas over the cost of films. Lucas soon returned to Hollywood with Bess Meredyth, taking Baker with them. Raymond Longford took over Carrolls Palmerston studio. 

==See also==
*List of lost films

==References==
 

==External links==
* 
*  at the National Film and Sound Archive
* 
*  at IMDB

 
 
 
 
 
 