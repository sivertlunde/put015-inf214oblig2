Talaash: The Hunt Begins...
 
{{Infobox film
| name           = Talaash: The Hunt Begins...
| image          = Talaash The Hunt Begins.jpg
| caption        = DVD Cover
| Director       = Suneel Darshan
| writer         = Suneel Darshan Robin Bhatt
| starring       = Akshay Kumar  Kareena Kapoor
| producer       = Pahlaj Nihalani
| distributor    = Chiragdeep International
| music          = Sanjeev Darshan
| cinematography = 
| editing        = 
| studio         = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
Talaash: The Hunt Begins... is a Bollywood action film starring Akshay Kumar and Kareena Kapoor in title roles. It is directed by Suneel Darshan and produced by Pahlaj Nihalani.

== Plot Summary ==
Babu works for three underworld dons and must go to prison to protect them. When his sentence is over, he returns home to find that Purnima (his wife), Arjun (his son) and Pooja (his daughter) live a destitute life. When he confronts his bosses, he is beheaded in front of his wife and children, and his daughter is abducted.

Purnima (Raakhee) loses her mind, and is hospitalized in a mental hospital. Growing up, Arjun (Akshay Kumar) becomes a high profile vigilante. As for Purnima, she becomes terribly ill and can be saved only if she sees her daughter again. Thus, Arjun decides to re-unite his mother and sister. In order to do so, he must discover the identity of his fathers killers.

At the beginning of his mission, he meets the exuberant and glamorous Tina (Kareena Kapoor) who will be the one to help him get what he wants. However, Tina goes to South Africa with her father. Now on top of looking for his sister Pooja, Arjun must also find Tina. He begins by going to South Africa in search of Tina.

== Cast ==

* Akshay Kumar ... Arjun
* Kareena Kapoor ... Tina
* Pooja Batra ...Kamini
* Raakhee ... Purnima (Arjuns mother)
* Kabir Bedi ... Chhote Pathan
* Gulshan Grover ... Sardarji
* Shakti Kapoor ... Upadhyay
* Dalip Tahil ... D.K.
* Arbaaz Ali Khan... Rocky (Chhote Pathans henchman) pie
* Anirudh Agarwal
* Upasana Singh

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)!! Length
|-
| 1
| "Yaar Badal Na Jaana"
| Alka Yagnik, Udit Narayan
| 05:25
|-
| 2
| "Masoom Chehra"
| Alka Yagnik
| 06:24
|-
| 3
| "Dil Le Gaya Pardesi" 	
| Alka Yagnik
| 06:07
|-
| 4
| "Rabba Pyaar Se Mila De"
| Vasundhara Das
| 04:20
|-
| 5
| "Zindagi Se Jung"
| Alka Yagnik
| 04:27
|-
| 6
| "Tune Kaha Jab Se Haan" Shaan
| 05:48
|-
| 7
| "Baaga Ma Jab Mor Bole"
| Alka Yagnik
| 05:52
|-
| 8
| "Bhagra Paa Le"
| Udit Narayan, Anuradha Sriram
| 05:52
|-
| 9
| "Masoom Chehra 2"
| Kumar Sanu
| 06:24
|}

==External links==
*  

 
 
 
 