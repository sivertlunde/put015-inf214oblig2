Night Trap (film)
{{Infobox film
| name           = Night Trap/Mardi Gras for the Devil
| image          = Nighttrapfilm1993.jpg
| caption        = 
| director       = David A. Prior  
 overview of film at New York Times Website  David Winters 
| writer         = David A. Prior Mike Starr Mickey Jones Lesley-Anne Down
| music          = Christopher Farrell
| cinematography = Don E. FauntLeRoy
| editing        = Tony Malanowski
| distributor    = Prism Pictures Echo Bridge Home Entertainment West Side Studios
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
}}
 Mike Starr, Lesley-Anne Down and Mickey Jones.

==Plot== satanic homicides. He soon finds himself along with Capt. Hodges (John Amos) seeking to catch a murderous psycho called Bishop (Michael Ironside) whos gone on a killing spree during New Orleans Mardi Gras. Only problem is that this killer has lost his soul to the devil and is no longer human... but a seemingly indestructible demonic being, intent on destroying the lives of everyone around Mike; and shall not stop until he has cost Mike not just his life... but his soul.

==Cast==
{|  class="wikitable"
|+Cast 
|-
! Actor / Actress!! Character

|-
| Robert Davi
| Mike Turner
|-
| Michael Ironside
| Bishop
|-
| John Amos
| Capt. Hodges
|- Mike Starr
| Det. Williams
|-
| Mickey Jones
| Bartender
|-
| Lesley-Anne Down
| Christine Turner
|-
| Lydie Denier
| Valerie
|-
| Margaret Avery
| Miss Sadie
|-
| Lillian Lehman
| Mrs. Hodges
|-
| Jack Forcinito (as Jack Verell)
| Stevens
|-
| David Dahlgren
| Johnson
|-
| Earl Jarrett
| Guard
|-
| Keri-Anne Bilotta
| Michelle
|-
| Butch Robbins
| Driver
|-
| Michael J. Anderson (as Michael Anderson)
| Police Officer
|-
| Lynwood Robinson
| Police Officer
|-
| Portia Bennett Johnson
| Dancer
|-
| John Neely
| Face in the Fire
|-
| Douglas Harter(as Doug Harter)
| Face in the Fire
|-
| Mario Opinato
| Face in the Fire
|}

==Critical reaction==
:{{cquote Nine dead bodies. Twelve breasts. Blood-drinking. Wrist-slitting. Two bodies flung through plate-glass windows. Hooker torture. Exploding house. Four motor vehicle chases, with four crashes, explosion and fireball. Drive-In Academy Award nominations for Michael Ironside, as the you-know-who, for saying "Whose body would you like to hold next to you in bed, while the other lies rotting in a grave?" Two and a half stars.

Joe Bob says check it out. {{cite web
|url= http://www.joebobbriggs.com/drivein/1993/nighttrap.htm
|title= Joe Bobs Drive In Report
|date= 1993-04-25
}} 

|40px|40px}}
Reviewers in this genre very often focus on numbers.  The numbers of people killed;  The numbers of exposed body parts;  The number of explosions; {{Quote box
 | quote  = Seriously I’ve never seen such a dull movie.  There isn’t a single scene in the movie that feels at all natural.  The entire movie feels staged and awkward, especially the love scenes that are just terrible.
 | source =    --John "El Juan" Shatzer
 | width  = 55%
 | align  = right
}} are all very important aspects of the review of a low budget action picture.  But some reviewers demand more.  El Juan Shatzer of Bloodtype Online was disappointed that the talented cast was wasted on a tepid script and flat direction by David A. Prior.  None of those shortcomings prevented Joe Bob Briggs from recommending it

==See also==
*The First Power Shocker
*Night Visitor
*The Believers
*The Horror Show
*Deal with the Devil
*Spree Killer Satanic
*New Orleans
*Mardi Gras

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 


 