Paappi Devataa
{{Infobox film
| name           = Paappi Devataa
| image          = Paappi Devataa.jpg
| alt            =  
| caption        = Poster
| director       = Harmesh Malhotra   
| producer       = M.M. Malhotra   Baldev Pushkarna  
| writer         = 
| based on       =  
| starring       = 
| music          = Laxmikant Pyarelal
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1995 Indian film.

==Plot==
Searching for work, Ram Kumar Singh (Jeetendra), comes to Bombay. But on arrival, his wallet and other belongings are stolen. He takes shelter in a taxi belonging to Rahim Khan (Dharmendra), who takes him home, provides him food-shelter as well as a taxi so that he can earn money. Rahim and Ram become close friends. One day Ram meets Reshma (Madhuri Dixit), who studies in Mithibai College and both fall in love with each other. Ram gets job and he quits his taxi driving. A fact about Rahim that he works for an underworld Don, Ratan Seth (Amrish Puri) is unknown to Ram. Rahim has also murdered a man named Niranjan Das (Satyendra Kapoor). And another fact that Ram is Deputy Commissioner of Police is unknown to Rahim. Ram also comes to know that Reshma is Rahims sister. His friendship and love bring problems in his official duty.

==Cast==
* Dharmendra as Rahim Khan 
* Jeetendra as Ram Kumar Singh 
* Jayapradha as Rosie 
* Madhuri Dixit as Reshma 
* Amrish Puri as Ratan Seth 
* Ashalata Wabgaonkar as Rosies mom 
* Satyendra Kapoor as Niranjan Das 
* Roopesh Kumar as Kundan 
* Sharat Saxena as Ratans employee 
* Rakesh Bedi   
* Gurbachchan Singh as Gulzar Singh 
* Yunus Parvez as Kelaram 
* Shail Chaturvedi as Train passenger 
* Urmila Bhatt as  Mrs. Khan 
* Birbal as Havaldar 
* Vinod Nagpal as Pandu

==Soundtrack==
The music of the film is composed by Laxmikant Pyarelal.

{| class="wikitable"
|-
! # !! Title !! Singer(s)
|-
| 1
| "Uska Naam Hai Piya"
| Alka Yagnik
|-
| 2
| "Dur Desh Se Ek Pardesi Aayega"
| Shobha Joshi
|-
| 3
| "Kuch Main Kahoon"
| Mohammad Aziz
|-
| 4
| "Jhoom Raha Hai"
| Shabbir Kumar, Alka Yagnik
|-
| 5
| "Sawan Ke Badlo Ki Niyat"
| Mohammad Aziz, Alka Yagnik
|-
| 6
| "Sawan Ke Badlo Ki Niyat (Instrumental)"
|
|-
| 7
| "Dur Desh Se Ek Pardesi Aayega (Instrumental)"
|
|}

== External links ==
*  

 
 
 
 


 