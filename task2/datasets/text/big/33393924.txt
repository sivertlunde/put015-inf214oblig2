The White Moth
{{infobox film
| name           = The White Moth
| image          =
| imagesize      =
| caption        =
| director       = Maurice Tourneur
| producer       = Maurice Tourneur M. C. Levee
| writer         = Izola Forrester(magazine story) Barbara La Marr(uncredited) Albert S. Le Vino(adaptation)
| starring       = Barbara La Marr
| music          =
| cinematography = Arthur L. Todd
| editing        = Frank Lawrence
| distributor    = First National Pictures
| released       =   reels
| country        = United States
| language       = Silent film (English intertitles)
}}
The White Moth is a 1924 silent film drama produced and directed by Maurice Tourneur and distributed by First National Pictures. Barbara La Marr was the female lead supported by young Ben Lyon. This film survives at the Library of Congress, Museum of Modern Art and Gosfilmofond Moscow.   

==Cast==
*Barbara La Marr - Mona Reid/ The White Moth
*Conway Tearle - Vantine Morley
*Charles de Rochefort - Gonzalo Montrez (*billed Charles de roche)
*Ben Lyon - Douglas Morley
*Edna Murphy - Gwendolyn Dallas
*Josie Sedgwick - Ninon Aurel
*Kathleen Kirkham - Mrs. Delancey
*William Orlamond - Tothnes

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 


 