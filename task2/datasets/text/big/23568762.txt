Gopi Gopika Godavari
{{Multiple issues
| 
 
 
}}

{{Infobox film
| name           = Gopi Gopika Godavari
| image          = 
| caption        =
| director       = Vamsy
| producer       = 
| writer         = 
| starring       = Kamalinee Mukherjee Venu Thottempudi  Krishna Bhagavan Sana Jayalalitha (actress) Chakri
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 162 minutes
| country        = India
| language       = Telugu
| budget         =
| gross          = 
}}
Gopi Gopika Godavari is a Telugu language|Telugu-language film released on 10 July 2009. The film, starring Kamalinee Mukherjee, Venu Thottempudi, Krishna Bhagavan and Sana, was directed by Vamsy.

==Cast==
* Kamalinee Mukherjee as Gopika
* Venu Thottempudi as Gopi
* Krishna Bhagavan
* Sana Jayalalitha

== Soundtrack ==
{{Infobox album
| Name = Gopi Gopika Godavari
| Type = Soundtrack Chakri
| Recorded = 2009 Feature film soundtrack
| Length = 22.22 Telugu
| Label = Aditya Music
| Producer = Chakri
}}

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 22.22
| lyrics_credits = yes
| title1 = Go Go Rye Rye
| lyrics1 = Ramajogayya Sastry
| extra1 = Chakri & Vamshi 
| length1 = 4:38
| title2 = Sundari
| lyrics2 = Ramajogayya Sastry
| extra2 =  Venu & Madhumitha
| length2 = 4:35
| title3 = Nuvvakkadunte
| lyrics3 = Ramajogayya Sastry
| extra3 = Chakri & Kousalya
| length3 = 5:18
| title4 = Bala Godavari
| lyrics4 = Ramajogayya Sastry
| extra4 = Karthik &  Kousalya & Vamshi 
| length4 = 4:16
| title5 = Maavidaaku
| lyrics5 = Ramajogayya Sastry
| extra5 = Vasu & Geetha Madhuri 
| length5 = 4:15
}}

==External links==
* http://popcorn.oneindia.in/title/3570/gopi-gopika-godavari.html Gopi Gopika Godavari
* http://www.raaga.com/channels/telugu/album/A0001478.html

 

 
 
 
 

 