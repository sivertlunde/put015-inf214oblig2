Monika (1938 film)
{{Infobox film
| name = Monika
| image =
| image_size =
| caption =
| director = Heinz Helbig
| producer = Gustav Althoff   Walter Tost
| writer =  Erich Ebermayer  (novel)   Heinz Helbig 
| narrator =
| starring = Maria Andergast   Iván Petrovich   Carmen Lahrmann   Theodor Loos Gerhard Winkler  
| cinematography = Edgar S. Ziesemer   
| editing =      
| studio = Aco-Film 
| distributor = Various
| released =  5 January 1938 
| runtime = 
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
Monika or A Mother Fights for Her Child (German:Eine Mutter kämpft um ihr Kind) is a 1938 German drama film directed by Heinz Helbig and starring  Maria Andergast, Iván Petrovich and Carmen Lahrmann. 

==Cast==
*  Maria Andergast as Barbara Daalen 
* Iván Petrovich  as Dr. Michael Holt 
* Carmen Lahrmann  as Monika 
* Theodor Loos as Professor Waldeck 
* Willi Schaeffers as Film Director 
*Rudolf Platte as Camera Man 
* Julia Serda as Frau von Schadow 
* Ilse Petri as Inge, Monikas Friend 
* Walter Steinbeck as Manager Wallen 
* Erich Dunskus as Hoteldirector 
* Gustav Püttjer as Oberleutnant 
* Karl Platen as Diener 
* Peer Baedecker as Knapp 
* Max Wilmsen as Geheimrat 
* Ilse Abel as Krankenschwester 
* Herbert Ernst Groh as Singer
* Kurt Iller as Gast der Kaschemme  Oskar Höcker as Thielmann  
* Peter Busse as Der Wirt  
* Fanny Cotta as Krankenschwest  
* Gerhard Dammann as Der Schalplattenverkäufer 
* Fritz Draeger as Statist bei d.Aufnahme in Hotel  
* Eugen von Bongardt as Dr. Scherber  
* Wilmo Schäfer 
*   Raffles Bill 

== References ==
 

== Bibliography ==
* Hake, Sabine. Popular Cinema of the Third Reich. University of Texas Press, 2001.

== External links ==
*  

 

 
 
 
 
 
 