Hotel Chelsea (film)
{{Infobox film
| name           = Hotel Chelsea
| image          = HotelChelseaCover.jpg
| alt            =  
| caption        = 
| director       = Jorge Valdés-Iga
| producer       = Hiro Masuda Tokuji Iuchi Yoishi Kobayashi
| writer         = Hiro Masuda
| starring       = Nao Nagasawa Justin Morck Hiro Masuda Sawa Suzuki Anthony Laurent
| music          = Kazushi Miyakoda
| cinematography = Corey Eisenstein
| editing        = Alphonse du Fleur
| studio         = 
| distributor    = Ichigo Ichie Films Ace Deuce Entertainment
| released       =  
| runtime        = 74 minutes
| country        = Japan
| language       = Japanese English
| budget         = 
| gross          = 
}}
Hotel Chelsea is a thriller of 2009. The production traveled to New York for filming on the classic Hotel Chelsea, a place regularly visited by celebrities, and gives name to the film.

== Plot ==
A newlyweds Japanese couple traveling to the Hotel Chelsea in New York to enjoy their honeymoon, but one night, the wife found the lifeless body of her husband and a video of the brutal murder. A police detective arrived at the scene and tries to reconstruct the events surrounding the mystery. 

==Festivals==
::• Radar Hamburg International Independent Film Festival (Nov 2, 2009)
::• Queens International Film Festival (Nov 15, 2009)
::• Myrtle Beach International Film Festival (Dec 4, 2009)
:::- Best Foreign Picture
:::- Best Actress
::• Indie Spirit Film Festival (Apr 23, 2010) 
::• Seattles True Independent Film Festival (Jun 9, 2010)
::• Okanagan International Film Festival (Jul 23, 2010)
::• Rhode Island International Film Festival (Aug 14, 2010) 
::• Eerie Horror Film Festival (Oct 10, 2010)
::• Japan Film Fest Hamburg (May 29, 2011)

== Release ==
Early reviews for Hotel Chelsea at the 2009 Radar Hamburg International Independent Film Festival drew applause at its premiere. Hotel Chelsea official premiered in Tokyo on May 8, 2009 to critical acclaim and commercial success.  In July 2010 was released on DVD.  earthquake and tsunami relief fund for Japan. 

==References==
 

== External links ==
*  
*  

 
 
 
 
 