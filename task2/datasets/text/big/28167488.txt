Enemies of the People (film)
{{Infobox film
| name           = Enemies of the People
| image          = enemies_of_the_people.jpg
| alt            =  
| caption        = Theatrical release poster
| director       =  
| producer       =  
| writer         =  
| narrator       = Thet Sambath
| music          = Daniel Pemberton
| cinematography =  
| editing        = Stefan Ronowicz
| studio         = Old Street Films
| distributor    = International Film Circuit
| released       =  
| runtime        = 94 minutes
| country        =  
| language       =  
| budget         = 
| gross          = 
}}
Enemies of the People is a 2009 British/Cambodian documentary film written and directed by Rob Lemkin and Thet Sambath. The film depicts the ten year quest of co-director Sambath to find truth and closure in the Killing Fields of Cambodia. The film features interviews by former Khmer Rouge officials from the most senior surviving leader to the men and women who slit throats during the regime of Democratic Kampuchea between 1975 and 1979.

==Synopsis==
An estimated 1.7 million Cambodians died during the rule of the Khmer Rouge, a radical communist movement led by Pol Pot. Among the victims were Thet Sambath’s mother, father and brother. He says he did not understand why the Khmer Rouge unleashed such violence on their compatriots. In 1999 he decided to seek confessions and explanations from former Khmer Rouge officials at all levels. None had previously admitted any killings. 

In 2001, he met Nuon Chea, Pol Pot’s deputy, also known as Brother Number Two. Nuon Chea was then living as a private citizen in Prum, a small town on the Thai-Cambodian border. Sambath visited Nuon Chea most weekends for around three years. 

During that time, Nuon Chea was willing to talk about all phases of his political career except the three and half years of the Khmer Rouge regime. He had previously been interviewed by Western and Japanese journalists but had always denied responsibility for killing anyone in the Cambodian genocide.

In 2004, Nuon Chea made his first admission to Sambath concerning decisions to kill that he made with Pol Pot.   Sambath continued to interview Nuon Chea on his role in the Killing Fields for three more years. During that time the United Nations-backed Extraordinary Chambers in the Courts of Cambodia (ECCC) was constituted to investigate the alleged crimes of Democratic Kampuchea. Sambath interviewed Nuon Chea until September 2007 when the latter was arrested and charged with war crimes and crimes against humanity by the ECCC.

During the same period, Sambath also built up a network of less senior former Khmer Rouge officials and cadres who were prepared to acknowledge and detail their role in the Killing Fields.  The film focuses on two Khmer Rouge perpetrators in the northwest of Cambodia. Khoun and Suon take Sambath to the scene of their massacres and introduce him to their superior officer, a woman known as Sister ‘Em’.  They give graphic accounts of the massacres they perpetrated. They also give voice to their own feelings of guilt, trauma and remorse.   Towards the end of the film Sambath brings Khoun and Suon to meet Nuon Chea and the three former Khmer Rouge comrades try to fathom the history of which they were each a lethal part.

Throughout his three years of research, Sambath omitted to tell Nuon Chea of his family’s fate in Democratic Kampuchea. At the end of the film and just before Nuon Chea’s arrest, Sambath tells the whole story to the former Khmer Rouge leader.  

The film also features appearances by Pol Pot, US President Richard Nixon and Deng Yingchao, the widow of Chinese Premier Zhou Enlai.

==Production==

The film was written, directed and filmed by Rob Lemkin and Thet Sambath. The pair worked on the film together from September 2006 until November 2009.

The film was edited in London by Lemkin and film editor Stefan Ronowicz. The score was composed by Daniel Pemberton.

The film’s world premiere was at the 2009 International Documentary Festival of Amsterdam (IDFA). The film was slightly altered for its US premiere at the 2010 Sundance Film Festival.

It was released theatrically in the US by International Film Circuit on July 31, 2011 and went on to play in over 40 American cities. It has also been theatrically released in Britain, Thailand and the countries of former Yugoslavia. It is the first Cambodian film to be released in Thailand for 40 years.   Thai distributors Extra Virgin’s Managing Director Pimpaka Towira said her decision to release the film was difficult at a time when Thailand and Cambodia were engaged in a military conflict. 

It has also played more than 80 film festivals worldwide.
 PBS documentary series POV on 12 July 2011. 

It has not yet been granted a distribution licence by the Cambodian government, although the film has been shown occasionally at an arthouse cinema in Phnom Penh.   In interviews the filmmakers have speculated this is because the history is still too sensitive for the current Cambodian government. 

==The Film and The Khmer Rouge Tribunal==

Following the European and American premieres    of the film the Co-Investigating Judge Marcel Lemonde requested the use of the film as evidence in the case he was investigating against Nuon Chea.

The filmmakers declined to depose the film with court on the basis that it would be a breach of the understanding reached with all Khmer Rouge sources.    All sources were happy for any and all material to be put in the public domain, but only on the basis that neither filmmaker was an officer or agent of the court. The filmmakers were widely criticised for not handing over their material to the court.     On April 9, 2010, the court issued an Order stating that it would not seek to obtain the material by international rogatory letter but instead wait until the material came into the public domain and seek to use it in the trial then. 

On June 27, 2011, the trial of Case 002 against Nuon Chea, Khieu Samphan, Ieng Sary and Ieng Thirith began in Phnom Penh. A number of newspaper reports raised the issue of the use of the film in the trial. The prosecution told Agence France Presse that it wanted the “candid admissions” to be used in the trial. Thet Sambath speculated that Nuon Chea may not make the same admissions during the trial. “Before the court he may say something else. But what he told me was the truth.” 

==Distribution in Cambodia==

In July 2010 the Cambodian government declined to give the film a permit for distribution in Cambodia. Sin Chin Chaya, Director of the Ministry of Culture’s Cinema Department, said this was because the film was not in Khmer.    In fact, much of the film is in Khmer.  Many Phnom Penh cinema owners told VOA they were keen to exhibit the film, but needed official permission. 

The film made its Phnom Penh premiere at the German-owned arthouse cinema Meta House on July 21, 2010.   The screening was attended by staff from the court, journalists, NGO workers and politicians including Mu Sochua.    Despite its limited release, the film has generated a level of discussion about the Khmer Rouge that is rare in Cambodia. 

In March 2011, Sin Chin Chaya told the Wall Street Journal that the permit had not yet been issued because a “formal permission request” had not been submitted. He remarked that since the Khmer Rouge trial was now underway the decision to approve is at ministerial level. 

==Reception==

In January 2010 as a sidebar to the Sundance Festival premieres, Sambath and Lemkin showed excerpts of the film to a group of Cambodians living in Salt Lake City, Utah. Many in the audience were refugees from the Killing Fields. Several expressed surprise at their own reactions. Instead of feeling anger and hatred of the perpetrators featured, they had feelings of compassion and forgiveness. 
 Long Beach, California, the largest urban Cambodian population outside Phnom Penh, in September 2010. Due to the film’s sensitive nature, the United Cambodian Community organised a pre-screening meeting to prepare elderly survivors of the Killing Fields for what they would see.  The US’s second largest Cambodian population, in Lowell, Massachusetts|Lowell, Massachusetts saw the film in November 2010.  Another large diaspora community in Minneapolis also saw the film in November.  The film is reported to have  galvanised ‘ the diaspora to reexamine their countrys history and to rethink how to bring reconciliation to the war-torn nation.’  

Sambath and Lemkin have attended scores of screenings and live film discussions in Asia, Europe and the US. At the 2010 Sundance Film Festival, they participated in a panel on social justice and documentary entitled “Speaking Truth to Power”. 

==Victims / Perpetrators Videoconference==

In October 2010 Sambath and Lemkin and organised an unprecedented dialogue between survivors of the Killing Fields living in California and three former Khmer Rouge perpetrators.  All three had appeared in the film. Khoun, Suon and Choeun travelled to Bangkok in Thailand with Sambath for a live 3 hour discussion held via videoconference with the refugees who attended a legal office in Long Beach, California for the event. According to Lemkin, it was “an end of the Cold War moment.   have seen the perpetrators as monsters for 30 years. I think they saw that they were just people.” 

On December 15, 2010 the Los Angeles Times published a major article by Joe Mozingo entitled ‘Coming to terms with Sadism’ which told the story of one man’s experience of the October videoconference. Long Beach resident Bo Uce was 4 when the Khmer Rouge took power in 1975. He lost several members of his family in the Killing Fields. Uce believed he achieved a ‘certain peace’ in watching the film and then engaging with the perpetrators in the videoconference. 

==Critical Response==

Enemies of the People currently holds a 100% fresh rating on the film site Rotten Tomatoes based on 28 reviews. 

Andrew Marr host of BBC Radio 4’s Start The Week programme called Enemies of the People “a stunning film – one of the most amazing films I’ve seen ... it’s one of the most gripping and moving films I have ever seen.”  

Stephen Holden of The New York Times reviewed the film twice. In his first review he called it “an inspiring film.”  In his second review, he writes “Enemies of the People is extraordinary on several fronts. At times, Mr. Thet Sambath suggests a one-man Cambodian Truth and Reconciliation Commission. Instead of affixing blame, he seeks the healing power of confession.”  

Andrew Schenker in  , but as if we had access to the directors methods and motivations instead of just the astonishing results.” 

Diego Costa in Slant Magazine concentrated on the film’s journey into the psychological dimension of the violence: “This is an extraordinary historical document, an archive of confessions with potential for closure, atonement, and belated punishment from one single man on a mission.” 

Gary Goldstein of The Los Angeles Times found the film “fascinating”. “How the genial Sambath remains so circumspect throughout his taut sessions with Chea is remarkable, as is so much of this must-see exposé.” 

Elliot V. Kotek in Moving Pictures wrote: “The slow, painful truth is fully revealed in this burning documentary crafted courageously by Sambath and his co-director Lemkin.” 

In the Financial Times, film critic Nigel Andrews gives the film four out of five stars and sees it in the context of Vietnam War cinema. “War crimes cinema gets a new wrinkle, or scar of honour, in Rob Lemkin and Thet Sambath’s enthralling investigative documentary. Shades of Heart of Darkness and Apocalypse Now, as this new Kurtz is tracked to his lair. For Conrad and Coppola the landscape traversed was a jungle. Here it is a jungle of the mind.”    In a review from Sundance, the same critic wrote:”If vengeance is a dish best served cold, this dish is at once cold and scalding hot.” 

Derek Malcolm in This is London wrote: “This astonishing documentary by Cambodian journalist Thet Sambath, whose entire family was killed by the Khmer Rouge, gets as near as anyone has done to discovering how and why the killing fields happened.” 

Derek Adams of Time Out gives the film five out of five stars: “This is patient, persistent, probing and fearless journalism of the highest order and it shocks to the core.” 

David Parkinson in Radio Times sounds a note of caution: The methodology employed by Thet and co-director Rob Lemkin is occasionally manipulative and the truth often remains elusive. But this is absolutely compelling, nonetheless. 

David Edwards in The Daily Mirror is moved by the film’s denouement: “The moment when Sambath reveals the price his own family paid is stirring stuff.” 
 The Sun Grant Rollings goes even further: “Sambath deserves a Nobel Peace Prize. If you see one factual film this year, make it this one” 

Ron Wilkinson in Monsters and Critics pays tribute to the journalism: One of the most amazing investigative documentary films of all time. The interviews are few but of the highest quality. 

Jared Ferrie focuses on the probative and forensic value of the journalism in The Christian Science Monitor: “In the film, Nuon admits publicly, for the first time, that he ordered the killing of thousands of political opponents, which is probably evidence enough to convict him for war crimes – if he ever makes it to trial.” 

Patrick Barta in The Wall Street Journal found it to be “not only … a historical document, but also … a work of art in its own right.” 

==Film festivals==

===Awards===
*2010  Sundance Film Festival World Jury Special Prize
*2010 True/False Film Festival True Life Award 
*2010 Santa Barbara International Film Festival Best Documentary
*2010  Santa Barbara International Film Festival Social Justice Award
*2010 Vera Film Festival,Finland Best Documentary	
*2010 One World Festival (Prague)  Grand Jury Prize
*2010 Full Frame Festival Anne Dellinger Grand Jury Award
*2010 Full Frame Festival Charles E Guggenheim Emerging Artist	
*2010 Hong Kong International Film Festival Outstanding Documentary Award 
*2010 Beldocs, Belgrade Best Documentary Award
*2010 OxDox (UK) Best Documentary Award
*2010 Norwegian Documentary Festival Best Documentary Award	
*2010 Human Rights Watch Film Festival New York  Nestor Almendros Award 
*2010 Kraków International Flm Festival Silver Horn Award				
*2010 Jerusalem International Film Festival “In the Spirit of Freedom” Ostrovsky Award	
*2010 Dokufest Kosovo Human Rights Award
*2010 Batumi International Art-House Film Festival Grand Prix		
*2010 Ojai Film Festival Best Documentary
*2010 Moet British Independent Film Awards (BIFA) Best Documentary
*2011 DocsBarcelona TV3 Human Rights Award
*2011 DocEdge NZ Best International Feature
*2011 DocEdge NZ Best Directing
*2011 Makedox, Macedonia, Best Human Rights Presentation
*2011 Makedox, Macedonia, Award for Best Moral Approach
*2011 Knight International Journalism Award

===Nominations===

*2011 Writers Guild of America Best Documentary Screenplay 
*2009 International Documentary Festival of Amsterdam (IDFA) Best Feature Documentary 
*2010 Grierson Award for Best Cinema Documentary

==Responses==
Enemies of the People was shortlisted for the Oscar for Best Documentary Feature in 2011. 
 Knight International Journalism Award for his career achievement. In its press release the ICFJ called Enemies of the People ‘arguably the most important film about the Khmer Rouge’.  ICFJ President and member of the jury Joyce Barnathan said Sambath’s work was ‘an amazing, amazing journalistic feat’. 

==Further Investigation==

Thet Sambath and Rob Lemkin have announced they are making a second film based on the material shot for their first film with new material being gathered from new Khmer Rouge sources. The film is entitled “Suspicious Minds” and will investigate the political conflict inside the Khmer Rouge movement that drove the violence.     Lemkin has further stated this film will explain for the first time why the Killing Fields happened.    
    
Sambath and Lemkin continue to blog on their researches, adding material on Nuon Chea’s early career and Pol Pot’s rise to power in the Khmer Rouge. 

In June 2011, they began to upload further interview excerpts with Nuon Chea. Topics so far include: revolution, killing traitors, confessions, smashing, Year Zero, the nation. 

==Interviews==
*Nuon Chea aka "Brother Number Two"
* Khoun
* Suon
* Sister "Em"

==See also==
*Kang Kek Iew

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 