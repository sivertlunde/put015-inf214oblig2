Sex Drive (film)
{{Infobox film
| name           = Sex Drive
| image          = Sex drive ver2.jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| director       = Sean Anders
| producer       = Bob Levy Leslie Morgenstein John Morris Michael Nelson
| screenplay     = Sean Anders John Morris
| based on       =   Josh Zuckerman Amanda Crew Clark Duke Seth Green James Marsden  
| music          = Stephen Trask
| cinematography = Tim Orr
| editing        = George Folsey Jr.
| studio         = Alloy Entertainment
| distributor    = Summit Entertainment
| released       =  
| runtime        = 109 minutes   130 minutes    
| country        = United States
| language       = English
| budget         = $19 million 
| gross          = $18,755,936   
}} hook up Josh Zuckerman, Dave Sheridan, and David Koechner appear in supporting roles. It was released in North America on October 17, 2008, and in the United Kingdom on January 9, 2009.

==Plot== Josh Zuckerman) 1969 Pontiac GTO Judge borrowed without permission from Ians arrogant homophobic older brother Rex (James Marsden).

On the way to Knoxville, they come across a hitchhiker (David Koechner), as the radiator in the Judge overheats. They attempt urinating in the radiator, which only works briefly as they try to leave the hitchhiker in the dust. The hitchhiker, frustrated at Ians lack of concern for his well being, leaves, but not before urinating on the car window. As Ian and Felicia wander to find help, Lance is waiting with the car as Ezekiel (Seth Green) happens by in his horse-drawn buggy. Ezekiel and his Amish buddies repair the car while they join a Rumspringa party where Fall Out Boy are playing a concert, and at which Lance meets an attractive Amish girl, Mary (Alice Greczyn). The three promise to come again on the way back to do some work in return for fixing the car.
 Dave Sheridan) puts a gun to Ians head. It becomes apparent that they work at a chop shop and attempt to steal the Judge.

Lance and Mary arrive after having sex, as well as a redneck named Rick (Michael Cudlitz) whose girlfriend Brandy (Andrea Anders) slept with Lance earlier. Felicia, however, is hiding in the car when Bobby Jo tries to steal it. Soon, a green car that has been continuously drag-racing with the Judge throughout the movie arrives. Ian manages to save Felicia, who then is able to run off and report to the police. Ms. Tasty tries to escape, but is stopped by the green car, whose drivers turn out to be Andy and Randy (Charlie McDermott and Mark L. Young), two dim-witted self-declared "womanizers" from Ians school, who Ms. Tasty tried to manipulate into giving her the car. Bobby Jo is treated after being shot by Ian in self-defense. Felicia tells the police about the chop shop location and the couple is arrested.

Upon finding out that if Mary leaves the Amish community, she will be shunned, Lance refuses to come back home and stays behind to marry Mary, while Ian and Felicia realize their love for each other. Ian and Felicia drive to a tree where Ian throws his shoes up into the tree. A few weeks later Ian is Felicias date to her cousins wedding. At Thanksgiving dinner, Rex tells his family that hes gay. On New Years Eve Ian and Felicia have sex in Ians basement under a blanket on the sofa. In the final frame of the film, a picture is shown of Lance and Mary getting married, accompanied by Ian. Lance is shown sporting a beard exactly like Ezekiels. During the credits, a short scene is shown of Ezekiel and Fall Out Boy arguing over the fact that the Amish fixed Fall Out Boys tour bus for just "a five song set" in form for compensation, referring to a running gag throughout the movie.

==Cast==
  Josh Zuckerman as Ian Lafferty
* Amanda Crew as Felicia Alpine
* Clark Duke as Lance Johnson
* Seth Green as Ezekiel
* James Marsden as Rex Lafferty
* Katrina Bowden as Ms. Tasty
* Alice Greczyn as Mary
* Michael Cudlitz as Rick
* David Koechner as Hitchhiker Dave Sheridan as Bobby Jo
* Mark L. Young as Randy
* Charlie McDermott as Andy
* Cole Petersen as Dylan Lafferty
* Allison Weissman as Becca
* Andrea Anders as Brandy
* José Duarte as Brandys father
* Kim Ostrenko as Ians stepmother
* Brett Rice as Mr. Lafferty
* Susie Abromeit as Tiffany
* Brian Posehn as Carny
* John Ross Bowie as Dr. Teddescoe 
* Sasha Ramos as Kimberly
* Jessica Just as Lindsay
* Caley Hayes as Sandy
* Fall Out Boy as themselves
* Kyle Gass as Trucker
 

==Soundtrack==
* "Porcupine Jacket" by Tramps & Thieves
* "Time to Pretend" by MGMT
* "Fa-Fa-Fa" by Datarock Airbourne
* Gabin
* The band Fall Out Boy also makes a cameo within the film, playing "Fame < Infamy", as well as, "Grand Theft Autumn/Where Is Your Boy Tonight (Acoustic)" Life Is Beautiful" by Vega4
* "Lets Get It Up" by AC/DC
* "Message from Yuz" by Switches Danger Zone" by Kenny Loggins
* The film also features songs from Donovan, Jem (singer)|Jem, Hot Hot Heat, Switches (band)|Switches, Pornosonic and Nitty (musician)|Nitty. The Flys I Dont Care" End credits by Fall Out Boy
* "You Love Me" by Teddy Tala

==Reception==
The film received mixed reviews from critics. On the review aggregator Rotten Tomatoes, based on 105 reviews, 46% of the critics had given the film a positive review.  On Metacritic the film got a score of 49 from 24 critics, marking "mixed to average" reviews.

==Box Office==
Sex Drive grossed $3.6 million opening weekend, finishing in 9th place. It went on to gross $8.4 million in the United States and $10.4 million in other countries, for a total gross of $18.8 million, against its 19 million budget, making it a box office bomb.

==Home release== the F-word), a topless woman running across a few random scenes, etc.

==References==
 

==External links==
 
*   (dead link)
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 