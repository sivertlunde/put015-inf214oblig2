Killer Nerd
{{Infobox film
| name           = Killer Nerd
| image          = Killer Nerd.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Mark Steven Bosko Wayne Alan Harold
| producer       = 
| writer         = Mark Steven Bosko Wayne Alan Harold
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Toby Radloff
| music          = 
| cinematography = 
| editing        = 
| studio         = Riot
| distributor    = Troma Entertainment
| released       = 1991
| runtime        = 90 min
| country        = United States
| language       = English
| budget         = $12,000
| gross          = 
}}
Killer Nerd is a 1991 comedy horror film. It was directed by Mark Steven Bosko and Wayne Alan Harold and stars Toby Radloff in his first film.

==Plot==
Harold Kunkle is a nerd with an overbearing mother. He is the target of bullies and also fails at getting a girlfriend. At a dance, drug dealers beat him and in retaliation, he begins a murder spree. Harold murders the people who did him wrong, including his overbearing mother. His murder weapons include using acid, dynamite, and an axe.

==Production==
The film was produced with $12,000 and was frequently rented in major film rental stores. Troma Entertainment bought the rights to the film.  A sequel titled Bride of Killer Nerd was released.  A review in VideoHounds Cult Flicks & Trash Pics said that, along with Killer Nerd, the film "deserves your neglect".  It was filmed in Ravenna, Ohio, with a prosumer grade camcorder.  

==Reception==
Bill Gibron, writing for DVD Verdict, said that the film "is a certifiable classic, a perfectly executed premise of such outrageous originality that its amazing no one had thought of doing it before".    Ian Jane, of DVD Talk, said that this film and its sequel are "typical bad horror-comedy hybrids" which are "quite dull", but went on to say that "while the movies are pretty much bottom of the barrel entertainment, they do have their own special kind of retarded charm".   

A TV Guide review said, "The story is so empty that it doesnt really end; it just runs out of people to kill". 

==Home media==
The film was released on DVD in 2004, in a double feature with its sequel on one disc. The special features are two commentaries, an interview, four trailers, promo videos, and a music video.  

==References==
 

==External links==
*  

 
 
 
 
 