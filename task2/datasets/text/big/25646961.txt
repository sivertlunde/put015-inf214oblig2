Titsiana Booberini
 
{{Infobox film
| name           = Titsiana Booberini
| director       = Robert Luketic
| producer       =
| writer         = Robert Luketic
| starring       = Tania Lacy Sophie Lee Roz Hammond
| distributor    =
| released       = 1997
| runtime        = 11 minutes
| country        = Australia
| language       = English
| budget         =
| gross          =
}}
 
Titsiana Booberini is a short film written and directed by Robert Luketic and released in 1997. 

Initially entered at the Telluride Film Festival and the Sundance Film Festival, the films success led to it being screened at film festivals all over the world.  It won "Best Film" at the Aspen Shortsfest,  and lead actress Tania Lacys performance won her the "Best Actress Award" at the Exposure Film Festival in Brisbane.  Its positive reception at Sundance caught the interest of several high-profile studios, leading to Luketic being offered the directing position for Legally Blonde.    

The film is a musical comedy,    with the story focusing on Titsiana Booberini, an Italian check-out girl at a suburban supermarket who is ridiculed by her fellow employees for her slightly hirsute upper lip. Titsiana gains new confidence and acceptance when she discovers a hair removal treatment.

==Full cast==
*Tania Lacy - Titsiana Booberini
*Sophie Lee - Francine Pickles
*Roz Hammond - Carol Johnson
*David J. Berman - Chubus Zarbo
*Marc Savoia - Stock boy

==References==
 

==External links==
* 

 

 
 
 
 

 