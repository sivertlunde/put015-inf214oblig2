A Delicate Balance (film)
{{Infobox film
| name           = A Delicate Balance
| image          = Adelicatebalancedvd.jpg
| caption        = DVD cover
| director       = Tony Richardson
| producer       = Ely A. Landau
| writer         = Edward Albee
| starring       = Katharine Hepburn Paul Scofield Lee Remick Kate Reid Joseph Cotten
| music          = David Watkin
| editing        = John Victor-Smith
| distributor    = American Film Theatre
| released       = December 10, 1973
| runtime        = 133 minutes
| country        = United States Canada United Kingdom
| language       = English
| budget         =
}} Pulitzer Prize-winning play of the same name. 

The film was the second in a series produced by Ely A. Landau for his American Film Theatre,    a subscription-based program of screen adaptations of notable stage plays shown in five hundred theaters in four hundred cities.

==Plot== alcoholic sister dry martinis.

The seemingly peaceful facade of their existence is shattered with the arrival of longtime friends Harry and Edna who, suddenly overcome by a nameless terror, fled their home in search of a safe haven. The couple is followed by Agnes and Tobias bitter, 36-year-old daughter Julia, who has returned to the family nest following the collapse of her fourth marriage. psyches and confront the demons hidden there.

==Cast==
*Katharine Hepburn as Agnes
*Paul Scofield as Tobias
*Lee Remick as Julia
*Kate Reid as Claire
*Joseph Cotten as Harry
*Betsy Blair as Edna

Kim Stanley was originally offered the role of Claire, and accepted. After arriving to the first rehearsal severely intoxicated, she was subsequently fired and replaced by Kate Reid.

==Critical reception==
Roger Ebert of the Chicago Sun-Times called the film "a fine, tough, lacerating production" and added, "Richardsons cast could hardly be better." 

TV Guide rated the film two out of four stars, calling it "unfortunately stiff, dull, and extremely stagy."  

==Awards and nominations== The Exorcist.

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 