Nakharam
{{Infobox film
| name           = Nakharam
| image          = Nakharam.jpg
| caption        = Nakharam theatrical poster
| director       = T. Deepesh
| producer       = Ismail V.C.
| writer         = Dr. Valsalan Vadhussery Ganesh Kumar   Architha
| music          = Paris Chandran Dr. Prasanth Krishnan
| cinematography = Sinu
| editing        = Viji Abraham
| studio         = 
| distributor    = VCI Release
| released       =   
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}} Ganesh Kumar, Architha, Madhupal, Mamukkoya, Jose Thettayil and Sreeletha.  Written by Valasalan Vadhussery and produced by Ismail V. C., the film  released in Kerala on January 7, 2011. Jose Thettayil, the current Minister for Transport in the Government of Kerala, played the role of a judge in the film.  It was the first DSLR feature film in India. 

==Plot==
Nakharam tells the story of a paan vendor Raghavan and his family. Janaki is his wife who sells pappads for a living, and they have a daughter who is in school. Raghavan finds himself at the cross roads when he is forced to give up his shop to help city development. He tries to make ends meet through other ways but unfortunately none of them help much. Raghavan gets attracted to other easy means to make money, and ultimately it leads to the destruction of his own family.

==Cast== Ganesh Kumar
* Architha
* Madhupal
* Mamukkoya
* Jose Thettayil
* Sreeletha

==References==
 

==External links==
*   at Nowrunning.com
*   at Oneindia.in

 
 
 


 