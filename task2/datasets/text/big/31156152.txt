Karinizhal
{{Infobox film 
| name           = Karinizhal
| image          = Karinizhal.jpg
| caption        =
| director       = JD Thottan
| producer       = Kovai Ramaswamy
| writer         = P Madhav Parappurathu (dialogues)
| screenplay     = Parappurathu Sathyan Sheela Sukumari
| music          = G. Devarajan
| cinematography = P Ramaswami
| editing        = VP Krishnan
| studio         = Sakthi Productions
| distributor    = Sakthi Productions
| released       =  
| country        = India Malayalam
}}
 1971 Cinema Indian Malayalam Malayalam film, directed by JD Thottan and produced by Kovai Ramaswamy. The film stars Prem Nazir, Sathyan (actor)|Sathyan, Sheela and Sukumari in lead roles. The film had musical score by G. Devarajan.   

==Cast==
*Prem Nazir Sathyan
*Sheela
*Sukumari
*Kaviyoor Ponnamma
*Adoor Bhasi
*William Thomas
*Alummoodan
*K. P. Ummer

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar Ramavarma. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Abhinandanam || P Susheela || Vayalar Ramavarma || 
|-
| 2 || Kaamaakshi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 3 || Nirakudam Thulumbi || K. J. Yesudas || Vayalar Ramavarma || 
|-
| 4 || Vallabhan Praana vallabhan || P. Madhuri || Vayalar Ramavarma || 
|-
| 5 || Vennakallu Kondalla || K. J. Yesudas || Vayalar Ramavarma || 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 