Ra Ra... Krishnayya
{{Infobox film
| name           = Ra Ra Krishnaya
| image          = Ra Ra Krishnayya poster.jpg
| writer         = Mahesh P Kalyani
| director       = Mahesh P
| producer       = Vamsi Krishna Srinivas
| studio         = SVK Cinemas
| released       =   
| music          = Achu Rajamani
| cinematography = Sai Sriram
| editing        = Marthand K. Venkatesh
| runtime        =
| language       = Telugu
| country        = India
| budget         = 
| gross          = 
}} Telugu Romantic Kalyani in lead roles. The films story is based on the 2012 Hindi film Tere Naal Love Ho Gaya directed by Mandeep Kumar which itself was inspired by the 1997 English film A Life Less Ordinary directed by Danny Boyle. Achu Rajamani composed the music. Sai Sriram and Marthand K. Venkatesh handled the cinematography and editing of the film respectively.
 Hyderabad and locales of Kerala and Vishakhapatnam. The films shooting completed in the month of May 2014.  The film released on July 4, 2014 in theaters.  This film was a big flop and gained negative reviews. 

==Plot==
Kittu / Krishna (Sundeep Kishan) is a cab driver who saves his money with the cab owner (Tanikella Bharani). When the owner refuses to return his money back, Kittu kidnaps owner’s daughter Nandu (Regina Cassandra). Nandu who is fed up with her father’s unilateral decision about her marriage is happy with the kidnapping. They fall in love with each other in the process. When they were about to accept the ransom money, she gets kidnapped by another kidnapper. The rest of the story is all about who kidnapped her and the kidnapper’s connection with Kittu!

==Cast==
*Sundeep Kishan as Krishna / Kittu
*Regina Cassandra as Nandu
*Jagapathi Babu as Jaggu Bhai Kalyani
*Tanikella Bharani
*Chalapathi Rao
*Brahmaji
*Ravi Babu
*Thagubothu Ramesh
*Satyam Rajesh
* Jai Venu



==Soundtrack==
{{Infobox album
| Name = Ra Ra Krishnayya
| Longtype = To Ra Ra Krishnayya
| Type = Soundtrack
| Artist = Achu Rajamani
| Cover = Ra Ra Krishnayya Audio.jpg
| Released = May 30, 2014
| Recorded = 2013 Feature film soundtrack
| Length = 21:53 Telugu
| Label = Aditya Music
| Producer = Achu Rajamani
| Last album = Pandavulu Pandavulu Tummeda (2014)
| This album = Ra Ra Krishnayya (2014)
| Next album = Current Teega (2014)
}}

Achu Rajamani composed the music for the film who marked his second collaboration with Sundeep Kishan after DK Bose. The soundtrack was released on Aditya Music label on May 30, 2014 for which a grand promotional event was held at a star hotel in Hyderabad on the same day.    

The soundtrack received positive response from critics. The Times of India gave a review stating "Music director Achu has dished out a peppy album for this romantic entertainer borrowing varied music elements like classic rock, blues, electronic dance music and desi beats."  IndiaGlitz gave a review stating "Ra Ra Krishnayya by a less known music director like Achu does deliver goods. Although the tunes are not imaginative, the musician makes the package attractive with his creative inputs. The selection of the singers is good, especially Shreya Ghoshal and Achu himself. The lyricists are there to give the songs the much needed icing on the cake."  Milliblog gave a review stating "Mixed bag, but the good ones showcase immense promise for this composer!". 

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 21:53
| writing_credits = 
| lyrics_credits  = yes
| music_credits   = 
| title1          = Hero Hero
| lyrics1         = Ramajogayya Sastry
| extra1          = Karthik (singer)|Karthik, Achu Rajamani
| length1         = 03:56
| title2          = Ra Ra Krishnayya
| lyrics2         = Ramajogayya Sastry
| extra2          = Achu Rajamani, Shreya Ghoshal, Yazin Nizar
| length2         = 03:29
| title3          = Onam Onam
| lyrics3         = Sri Mani
| extra3          = Achu Rajamani, Chinmayi
| length3         = 03:42
| title4          = Vadarey Machan
| lyrics4         = Sri Mani
| extra4          = Suchitra, Achu Rajamani
| length4         = 03:47
| title5          = Come on Baby
| lyrics5         = Bhaskarabhatla
| extra5          = Soumya, Sree Chitra, Sooraj Santhosh
| length5         = 03:28
| title6          = Seetha Kalyanam
| lyrics6         = 
| extra6          = Mahathi
| length6         = 03:28
}}

==Production==

===Development=== Krishna Vamsis Venkatadri Express.  The project was titled as Ra Ra Krishnayya which was said to be produced by Vamsi Krishna Srinivas on SVK Cinema banner who was known for his previous film Solo (2011 film)|Solo directed by Parasuram and was launched at Film Nagar in Hyderabad on August 11, 2013. While the story, screenplay and direction were handled by Mahesh, Arjun Chelluri was recruited as the executive producer.    It was later reported that Achu Rajamani would compose the music, Sriram would handle the cinematography, Marthand K Venkatesh would be the editor of the film while Ramanjaneyulu would be the art director of the film. 

===Casting===
Sundeep Kishan and Regina Cassandra were a part of the principal cast even before the commencement of the project in its development stages marking the couples second film after Routine Love Story which released in 2011.  The film was supposed to be Sundeep Kishans first attempt which is a pure love story though he played a few romantic comedies in the past.  It was also Reginas first glamorous role till date according to her.   On February 12, 2014 it was confirmed that Jagapathi Babu is playing the role of Sundeeps elder brother in the film while Kalyani (actress)|Kalyani, Tanikella Bharani, Chalapathi Rao and Kasi Viswanath would play crucial roles in the film.  In the end of May 2014, it was reported that Sundeep and Regina shared a sensuous lip-lock in the film.   The reports were confirmed when the films trailer featured a glimpse of a deep lip-lock between the leads.  

===Filming===
During the launch of the movie on August 11, 2013 it was reported that the first shooting schedule would begin on August 12 and would continue till August 26 in Hyderabad followed by the second schedule from September 10 to 25 in Kerala where some talkie part and two songs on hero and heroine were said to be shot.  It was then estimated that the entire shooting would be wrapped up with the third and final schedule in October 2013.  However in mid-October 2013, during the shoot at a Temple in Alapuzha at Kerala, the cast and crew of the film including the director and Sundeeps manager were attacked by Kerala RSS workers. The unit was attacked as they hired local people to act in a Telugu film and Sundeeps 57 year old manager who attained permission was a Muslim.  The shooting then continued at a rapid pace and in the end of February 2014, it was reported that the films shoot is complete except for 2 songs and the climax scenes which would be shot in the schedule commencing in March 2014.    Then, a song on the lead pair was shot at Hyderabad.  After much silent shoot, in the end of April 2014 a song was shot on Sundeep and Regina near the beach road in Vishakhapatnam and Regina wrapped up the shoot for her part in the film on April 29, 2014.  In the May, it was confirmed that the films shooting and post production activities were completed aiming for a June 2014 release.   

==Release==
After the end of the shoot in May 2014, it was declared by the makers that the film would release in June 2014.  On June 4, 2014 CineGalaxy Inc., issued a press note which stated that they acquired the overseas theatrical screening and distribution rights of the film and then, it was specified that the film is tentatively scheduled for a release on June 20, 2014.  However in the end of June, it was confirmed that the film is scheduled for a worldwide release on July 4, 2014 in theaters. 

==References==
 

==External links==
*  

 
 
 