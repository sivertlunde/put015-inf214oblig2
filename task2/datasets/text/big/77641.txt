Random Harvest (film)
{{Infobox film
| name           = Random Harvest
| image          = Random-harvest-1942.jpg
| image_size     = Sidney Franklin
| director       = Mervyn LeRoy James Hilton (novel)
| based on       = Random Harvest
| screenplay     = Arthur Wimperis George Froeschel Claudine West
| starring       = Ronald Colman Greer Garson Philip Dorn Susan Peters
| music          = Herbert Stothart
| cinematography = Joseph Ruttenberg
| editor         = Harold F. Kress
| distributor    = Metro-Goldwyn-Mayer
| released       = December 17, 1942
| runtime        = 125 minutes
| budget         = $1,210,000  . 
| gross          = $4,650,000 (Domestic earnings)  $3,497,000 (Foreign earnings)  English
}}
 James Hilton novel of Academy Award nomination. The film departed from the novel in several significant ways, as it proved nearly impossible to translate to film otherwise. It starred Ronald Colman as a  Combat stress reaction|shellshocked, amnesiac World War I soldier and Greer Garson as his love interest.
 Academy Award Best Picture. Garson, whose performance was well-received, was ineligible for the Academy Award for Best Actress, as she had already been nominated that year for her role in Mrs. Miniver (film)|Mrs. Miniver.

==Plot== gassed and trenches during the First World War. He is confined to an asylum as an unidentified inmate because he has lost his memory and has trouble speaking. When the war ends, jubilation erupts in the nearby town of Melbridge and the gatekeepers abandon their posts to join the celebration. With no one to stop him, Smith simply wanders off.

 
In town, he is befriended by singer Paula (Greer Garson). She guesses he is from the asylum but as he seems harmless, she arranges for him to join her travelling theatrical group. After an incident that threatens to bring unwanted attention, Paula takes Smith away to a secluded country village, where they marry and are blissfully happy.

"Smithy", as Paula calls him, discovers a literary talent and tries writing to earn a living. Paula remains home with their newborn baby while Smithy goes to Liverpool, for a job interview with a newspaper. There, he is struck by a taxi. When he regains consciousness, his past memory is restored but his life with Paula is now forgotten. He is Charles Rainier, the son of a wealthy businessman. None of his meagre possessions, including a key, provide any clue how he got there from the battleground of France.

Charles returns home on the day of his fathers funeral, to the familys amazement as he had been given up for dead. Fifteen-year-old Kitty (Susan Peters), the stepdaughter of one of Charles siblings, becomes infatuated with her "uncle".

Charles wants to return to college but the mismanaged family business needs him and he puts off his own desires to safeguard the jobs of the many employees and to restore the family fortune. After a few years, a newspaper touts him as the "Industrial Prince of England".

Meanwhile, Paula has been searching for her Smithy. Their son having died as an infant, she returns to work as a secretary. One day, she sees Charless picture in a newspaper and manages to become his executive assistant, calling herself Margaret (Paula being her stage name), hoping that her presence will jog his memory. Her confidante and admirer, Dr. Jonathan Benet (Phillip Dorn), warns her that revealing her identity would only cause Charles to resent her.
 declared legally dead, seven years having elapsed since he left her, dissolving their marriage. However, a hymn that Kitty is considering for their upcoming wedding triggers a vague memory in Charles. Kitty realizes that he still loves someone else and heartbroken, breaks off the engagement.
 
When Margaret hears Charles is in Liverpool, trying one last time to piece together his lost years, she rushes there. They recover his suitcase from a hotel but he recognizes nothing. Charles is then approached to stand for Parliament of England|Parliament. After his election, in which Margaret provided invaluable assistance, he feels the need for a wife in his new role. He proposes to her, more as a business proposition than a romantic one and she accepts.

They become an ideal couple, at least to all outward appearance. She is the perfect society hostess. They sometimes discuss his lost past and at one point, she tells him of her own lost love, without disclosing that it is Charles. He hopes their life together can fill the void they both feel. Desperately unhappy, Margaret decides to take an extended vacation abroad by herself. Before her liner sails, she revisits the hamlet where she and Smithy lived.

Meanwhile, Charles is called upon to mediate a strike at the Melbridge Cable Works. He succeeds. Walking through town, the familiar surroundings and the celebrating workers begin to unlock his lost memories and eventually lead him to the cottage he and Paula shared. Hesitantly, he tries the old key he kept, and finds that it unlocks the door.  

Margaret, who had been about to leave for the boat train, makes a casual remark to the current innkeeper about the former owner. The innkeeper remarks that someone else had just that morning asked about the same woman. Margaret goes to the cottage and calls "Smithy!"; he turns, memories flooding back; he cries out "Paula!" and they embrace.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Ronald Colman || Charles Rainier
|-
| Greer Garson || "Paula Ridgeway"/Margaret Hanson
|-
| Philip Dorn || Dr. Jonathan Benet
|-
| Susan Peters || Kitty Chilcet
|-
| Henry Travers || Dr. Sims
|-
| Reginald Owen || Biffer
|-
| Bramwell Fletcher || Harrison
|- Rhys Williams || Sam
|- Una OConnor || Tobacco Shopkeeper
|-
| Aubrey Mather || Sheldon
|-
| Margaret Wycherly || Mrs. Deventer
|-
| Arthur Margetson || Chetwynd Rainier
|-
| Melville Cooper || George Rainier
|-
| Alan Napier || Julian Rainier
|-
| Jill Esmond || Lydia Rainier
|-
| Ivan F. Simpson || Vicar
|-
| Ann Richards || Bridget
|-
| Norma Varden || Julia
|-
| David Cavendish || Henry Chilcet
|-
| Marie De Becker || Vicars Wife
|-
| Charles Waldron || Mr. Lloyd
|-
| Elisabeth Risdon || Mrs. Lloyd
|}

==Difference from the novel==
The differences between novel and film are many but principally:

* The novel does not reveal that Mrs. Rainier and Paula are the same person until the last line of the narrative. Of course it could not have been filmed that way, since the actress playing Paula would be seen to be Mrs. Rainier as well.

* The film does not have Kitty die; she simply leaves to join her mother in travelling abroad.

* Parson Blampied does not appear in the film.

* Melbury, a quiet north London outer suburb, becomes Melbridge, a booming industrial town in the Midlands.

* In the film, Charles inherits the family house and a substantial portion of the family wealth. He completes university and graduates. In the novel, he does not do so; neither does he inherit anything, save what the family donates to him.

* Smithy and Paula live in Blampieds London parsonage, not a small Devon village.

==Reception==
According to MGM records, the film earned $8,147,000 and made a profit of $4,384,000.  Scott Eyman, Lion of Hollywood: The Life and Legend of Louis B. Mayer, Robson, 2005 p. 365 

Despite its box office success, critics were not impressed. James Agee wrote, "I would like to recommend this film to those who can stay interested in Ronald Colmans amnesia for two hours and who can with pleasure eat a bowl of Yardleys shaving soap for breakfast."  In his New York Times review, Bosley Crowther was of the opinion that "for all its emotional excess, Random Harvest is a strangely empty film."    "Miss Garson and Mr. Colman are charming; they act perfectly. But they never seem real."  Jonathan Rosenbaum of the Chicago Reader allowed that it had "a kind of deranged sincerity and integrity on its own terms".  Variety (magazine)|Variety praised the performances of the two leads, in particular Garson, but noted that Colman seemed older than the role. 

==Academy Awards==

===Nominated=== Best Picture    Best Director – Mervyn LeRoy Best Actor in a Leading Role – Ronald Colman Best Actress in a Supporting Role – Susan Peters Best Writing, Screenplay – Claudine West, George Froeschel and Arthur Wimperis Best Music, Scoring of a Dramatic or Comedy Picture – Herbert Stothart Best Art Direction-Interior Decoration, Black-and-White – Cedric Gibbons, art direction; Edwin B. Willis, sets, Randall Duell, Jack D. Moore

==In popular culture == As Time Goes By. Lionel and Jean attend a meeting in Los Angeles about a script he has written, and co-executive creative consultants Josh and Lisa come up with a "mangled version" of Random Harvest, about "Lionel being shot in the head every five minutes."

==DVD release==
Warner Home Video released a restored and remastered version in DVD format in 2005.

==References==
 

==External links==
 
*  
*  
*  
*  
*   on Lux Radio Theater: January 31, 1944

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 