Sound and Fury (film)
 
{{Infobox film
| name           = Sound and Fury
| image          = Sound and Fury.jpg
| image_size     =
| caption        = DVD cover
| director       = Josh Aronson
| producer       = Josh Aronson Jackie Roth Julie Sacks Roger Weisberg
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Ann Collins
| distributor    =
| released       = 2000
| runtime        = 80 minutes
| country        =   English
| budget         =
| preceded_by    =
| followed_by    =
}} deaf children deaf identity. Best Documentary Feature.   

==Synopsis== hearing — and their wives and children. Chris and Mari Artinian (who is a Child of Deaf Adult) find out that one of their newborn twins is deaf.  They begin to research the cochlear implant and its advantages and disadvantages..  While this is going on, Heather, Peter and Nitas oldest child, starts asking for an implant as well. The brothers, along with grandparents on both sides, become embroiled in a bitter argument over the importance of deafness, the best form of education for their kids, and the controversy of cochlear implants for young children.  For Peter and his wife, Nita, its their fear of losing a child to the "hearing world", and her losing the importance of Deaf culture, which concerns them.  
They were pleased with Maryland, and decided to move there, and forget the Implant. 
Chris and Maris infant, Peter, is given cochlear implant surgery.

==6 years later==
In the follow-up documentary Sound and Fury: 6 Years later, Heather is now 12 years old, and she, her 2 deaf siblings, her mother and members of her extended deaf family have all opted for the implant device. The article summarizing the documentarys events describes her as having clear speech, living in a mainstreamed world, interacting with hearing people, and earning high grades in school. Heather is depicted as moving between the hearing and Deaf worlds comfortably, and embracing Deaf culture as well as having friends who are hearing.

==References==
 

==External links==
* 
*  
*  

 
 
 
 
 
 
 
 
 