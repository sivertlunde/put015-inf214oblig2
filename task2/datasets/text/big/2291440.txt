Twisted Desire
{{Infobox film
| name           = Twisted Desire
| image          = Twisted Desire 1996.jpg
| image_size     = 200px
| caption        = Promotional advertisement
| director       = Craig R. Baxley
| producer       = Judy Cairo Chad Hayes
| narrator       =  Jeremy Jordan   David Lascher
| music          = Gary Chang
| cinematography = João Fernandes
| editing        = Sonny Baskin
| studio         =
| distributor    = NBC
| released       = May 13, 1996
| runtime        = 96 minutes
| country        = US
| language       = English
| budget         =
| gross          = 
}}
 thriller film Chad Hayes Jeremy Jordan. The film made its debut on May 13, 1996 9/8c on National Broadcasting Company |NBC.

The film is based on the 1990 murders of the parents of 14-year-old Jessica Wiseman. Jessica had her 17-year-old boyfriend, Douglas Christopher Thomas, shoot and kill her parents. Thomas was executed in 2000.

==Plot==
 
Jennifer Stanton (Melissa Joan Hart |Hart) is a rebellious teen who constantly argues with her parents. She feels that they are overly protective of her and that they are exceedingly strict. Her father William (Daniel Baldwin |Baldwin) disapproves of her clothes and friends. Williams aggressive attitude has a negative impact upon his daughters relationship with Brad (David Lascher |Lascher), the captain of the high school football team. When Jennifer tells Brad that she is unable to attend a concert with him because her father refused to give her permission, Brad decides to break up with her. He feels that William is exerting too much control over the relationship. Although Jennifer is shocked, the fact that Brad chooses to display interest in another girl at school makes her feel even more frustrated.

After meeting Nick Ryan (Jeremy Jordan (singer)|Jordan) at a gas station, they soon form a close relationship and begin going out with one another. Nick is infamous in his neighborhood for having spent time in jail on an assault charge. When Jennifers parents decide to spend a weekend away from the house, Jennifer uses this as an opportunity to get closer to Nick. Her parents decide to return early and she is caught in her parents bed with Nick. William is unable to contain his fury, threatens Nick and chases him out of the house. She claims she loves Nick and decides to see him secretly. She applies makeup to her own eye to make it appear bruised. When Nick notices her "black eye", he expresses concern and asks Jennifer to stay with him. Although she refuses, she is touched by Nicks concern. Back at home, Jennifer is caught by her mother, who is disgusted by the fact that her daughter had sex in the parents bed. Jennifers mother tells Jennifer that she will no longer protect her from her father, nor take her side.

Nick visits Jennifers house, and states that he loves her. He also warns William not to treat Jennifer in such a harsh manner. William is shocked by Nicks audacious behavior and threatens to sue him for statutory rape. The next morning, William is shocked to find his car vandalized. A furious William contacts the police and Nick is soon questioned. He, however, claims he didnt have anything to do with the damage. Jennifer overhears her father saying he is determined to get Nick behind bars. Fearing her father, she tells Nick they cant sneak around any longer. She convinces him to use his grandfathers pistol to shoot and kill William. While he is planning the murder, she goes back to her normal life. She tells her friends that she broke up with Nick. She also presents herself to her parents as someone who has changed and wishes to embark upon a more truthful life. On the night of the murder as Jennifer and Nick walk towards Jennifers parents bedroom, Nick changes his mind and is adamant that he is unable to go through with the murders. Jennifer vehemently encourages him to complete the task he promised to perform.

After the killing, Jennifer is sent to live with her grandparents, and the police presume the murder was motivated by a robbery. Nick becomes the prime suspect and is constantly harassed by the police. Jennifer rekindles her relationship with Brad. She calls the police anonymously with information that leads to the discovery of the murder weapon implicating Nick. The police think, with Nick in custody, that the case is now closed. However, Detective Daniels (Eric Laneuville) suspects that Jennifer is more involved in the case that she led the police to believe when she was being questioned. Upon confronting her, she claims that Nick was angry at Jennifers parents for not letting him see her, thereby giving an explanation of what might have motivated Nick to commit the murders. Meanwhile, Jennifers best friend Karen (Meadow Sisto |Sisto) finds her diary, in which she admits her involvement and that she used Nick to get her parents out of the way so she could have Brad.

Karen decides to visit Nick in jail, trying to convince him that Jennifer set him up. He later talks to Jennifer about it, but she gets mad at him, blaming him entirely for the murder. Upset, he admits to the police that he killed her parents, but that it was Jennifers idea. Not much later, the police claim her diary, but she has rewritten it in the meantime, preventing an arrest. Frustrated about the injustice, Karen sneaks into Jennifers house to collect evidence, but she is almost caught. Meanwhile, a trial against Nick has started. Jennifer gives false testimony, claiming that Nick harassed her. Karen is determined to stop her, but Jennifer threatens to give false proof that she was also involved with the murder. Eventually, Brad manipulates her into admitting that she is responsible for the murder. Having recorded their conversation, she is soon arrested for murder. In the end, it is announced that she was tried as a juvenile, and would be released from prison at age 21.

==Cast==
*Melissa Joan Hart as Jennifer Stanton
*Daniel Baldwin as William Stanton Jeremy Jordan as Nick Ryan
*Meadow Sisto as Karen Winkler
*Kurt Fuller as Detective Becker
*David Lascher as Brad
*Eric Laneuville as Detective Daniels Collin Wilcox Paxton as Rose Stanton
*Isabella Hofmann as Susan Stanton

 

==External links==
* 

 

 
 
 
 
 
 
 