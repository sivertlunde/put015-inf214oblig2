The Key of the World
{{Infobox film
| name           = The Key of the World
| image          =
| caption        =
| director       = J.L.V. Leigh 
| producer       = 
| writer         = Dorin Craig (novel)   Helen Blizzard Eric Harrison   Lionel dAragon
| music          = 
| cinematography = 
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Distributors 
| released       = October 1918
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent romance Eric Harrison. It was made by British Gaumont at Lime Grove Studios in Shepherds Bush.  It was based on a novel by Dorin Craig.

==Cast==
* Eileen Molyneux as Honesty Vethick
* Heather Thatcher as Dina Destin Eric Harrison as Garth Berry
* Pat Somerset as Evelyn Carew
* Lionel dAragon as Liston Crawley
* Cecil Morton York as Adam
* Hamilton Stewart as Earl of Carne
* Florence Nelson as Lady Boddy Frank Harris as Farmer Berry

==References==
 

==Bibliography==
* Warren, Patricia. British Film Studios: An Illustrated History. Batsford, 2001.

==External links==
* 

 
 
 
 
 
 
 
 


 
 