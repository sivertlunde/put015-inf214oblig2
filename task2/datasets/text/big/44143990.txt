Vanitha Police
{{Infobox film
| name           = Vanitha Police
| image          =
| caption        =
| director       = Alleppey Ashraf
| producer       = Alleppey Ashraf
| writer         = Priyadarshan
| screenplay     = Priyadarshan Seema Sukumari Mohanlal
| music          = Gopan
| cinematography = Dhananjayan
| editing        = A Sukumaran
| studio         = Indukala
| distributor    = Indukala
| released       =  
| country        = India Malayalam
}}
 1984 Cinema Indian Malayalam Malayalam film, directed by Alleppey Ashraf and produced by Alleppey Ashraf. The film stars Prem Nazir, Seema (actress)|Seema, Sukumari and Mohanlal in lead roles. The film had musical score by Gopan.   

==Cast==
 
*Prem Nazir as Pilla Seema as Sasikala
*Sukumari
*Mohanlal
*Jagathy Sreekumar
*Kollam Gopi
*Sankaradi as Chelleppan Pilla
*Alleppey Ashraf as Director Keshavan
*Aroor Sathyan
*Mala Aravindan
*Master Suresh as Vichu Meena
*Poojappura Ravi
*Santhakumari
*Sathyachithra
*Sathyakala
*Soorya as Kousalya
*Vanitha Krishnachandran as Rathnamma
*Nalinikanth
*Thavakkala as Undapakru
*Rajan Paul Shankar as Actor Shankar
 

==Soundtrack==
The music was composed by Gopan and lyrics was written by Madhu Alappuzha. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eettappuliyo || K. J. Yesudas || Madhu Alappuzha || 
|-
| 2 || Kanne karale || K. J. Yesudas, KS Chithra || Madhu Alappuzha || 
|}

==References==
 

==External links==
*  

 
 
 

 