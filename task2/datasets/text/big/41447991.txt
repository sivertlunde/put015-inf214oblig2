Evil Thoughts
{{Infobox film 
 | name = Evil Thoughts
 | image = Evil Thoughts.jpg
 | caption =  
 | director = Ugo Tognazzi
 | writer =    
 | starring =   
 | music =   Armando Trovajoli
 | cinematography =  Alfio Contini
| editing        =  Nino Baragli
 | producer = Edmondo Amati
 | language = Italian 
 }}
Evil Thoughts, originally titled Cattivi pensieri and also known as Who Mislaid My Wife? is a 1976 Italian comedy film written, directed and starred by Ugo Tognazzi.      

== Plot ==

The Milanese lawyer Mario Marani (Tognazzi), due to fog at the airport, is forced to return home because his flight was canceled. At home he finds his wife Francesca half asleep and realizes that a person is hiding in the closet of shotguns. 
Convinced that he is the lover of Francesca, he closes the utility room and the next morning he departes with his wife for work purposes, continuing to mull over the alleged infidelity of his wife.

== Cast ==
*Ugo Tognazzi: Mario Marani
*Edwige Fenech: Francesca Marani
*Paolo Bonacelli: Antonio Marani
*Piero Mazzarella: portinaio
*Yanti Somer: Paola
*Mara Venier: Miss Bocconi
*Luc Merenda: Recrosio
*Veruschka von Lehndorff|Veruschka: lover of Mario
*Laura Bonaparte: lover of Recrosio
*Mircha Carven: Lorenzo Macchi
*Orazio Orlando: lawyer Borderò 
*Massimo Serato: Carlo Bocconi 
*Pietro Brambilla: Duccio
*Beppe Viola:  police commissioner 
*Guido Nicheli: ospite Riccardo Tognazzi: Gino

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 