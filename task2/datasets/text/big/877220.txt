The Crow: City of Angels
 
{{Infobox film
| name           = The Crow: City of Angels
| image          = The Crow 2.jpg
| caption        = Theatrical release poster
| director       = Tim Pope
| producer       = {{plainlist|
* Jeff Most
* Edward R. Pressman
}}
| screenplay     = David S. Goyer
| based on       =  
| starring       = {{plainlist|
* Vincent Pérez
* Mia Kirshner Richard Brooks
* Iggy Pop
* Thuy Trang
* Thomas Jane
* Ian Dury
}}
| music          = Graeme Revell
| cinematography = Jean-Yves Escoffier
| editing        = {{plainlist|
* Michael N. Knue
* Anthony Redman
}}
| studio         = Dimension Films
| distributor    = Miramax Films
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $17.9 million (US)   
}} The Crow.

==Plot== Richard Brooks) has mechanic Ashe Corven (Vincent Pérez) and his eight-year-old son Danny (Eric Acosta) killed after they witness a gang of Judahs thugs murdering a fellow drug dealer.
 first film (Mia Kirshner) is now an adult, working in a tattoo parlor by day, and painting haunting, surreal images of death and resurrection in her apartment at night. She is haunted by disturbing dreams about Ashe and Danny, and after a days work in the tattoo parlor, Sarah is visited in her apartment by a large crow as she contemplates a ring that Eric Draven gave her years before.

Sarah follows the crow to the harbor at night on All Saints Day, and witnesses Ashes resurrection and frantic escape from his watery grave. She takes him to her apartment. When Sarah tells Ashe he is dead, he panics and runs screaming into the night, ending up at his own home, where he relives the final moments of his life.
 peeping booth. Ashe appears in the booths and kills him. Nemo is then discovered with a doll stuffed in his pants, and a paper crow in his mouth.

Judah has in his employ a blind prophetess named Sybil (Tracey Ellis) who is able to ascertain Ashes link to Sarah and to the crow that is the source of his powers. Judah captures Sarah in order to draw Ashe to him and steal his power.

One of the murderers, Kali (Thuy Trang), goes to Sarahs apartment to draw Ashe out. While battling her, Ashe realizes that Kali is the one who killed Danny; enraged, he throws her against a wall and then out a window, leaving a crow-shaped blood pattern. Ashe then pursues Judahs right-hand-man, Curve (Iggy Pop), in a motorcycle chase. Ashe shoots Curves motorcycle, which blows up and throws Curve onto the road. Ashe then drags Curve into the nearby river, leaving him to die as local parishioners cast down flower petals in the shape of a crow.

On the day of the annual Day of the Dead festival. Judah captures the crow and impales its wings with knives before killing it. He then ingests the crows blood, stealing Ashes power. Suddenly mortal, Ashe nearly dies from the shock, but is revived after seeing a vision of Danny telling him to keep fighting. Ashe must now attempt to rescue Sarah by seeking out Judah in his lair, an abandoned church. Judah gets the best of the weakened Ashe in the ensuing fight. Judah ties a rope around Ashe and savagely whips him, intending to hang him.

Sarah rushes up and stabs Judah in the forehead, causing Judah to drop Ashe. Judah pulls out the knife and starts moving toward Ashe. Sarah gets in the way, and Judah stabs her in the stomach. Ashe gets up and impales Judah on a metal pipe, but this does not kill Judah either. Judah gloats that he cannot be killed. While Judah is still impaled, Ashe calls upon a murder of crows, which devour Judah. Sarah dies in Ashes arms, a tableau reminiscent of a painting she had completed earlier in the film. Ashe returns to death, knowing that he can rest in peace with Sarah, and his son.

==Cast== Ashe Corven/The Crow
*Mia Kirshner as Sarah Richard Brooks as Judah Earl
*Thuy Trang as Kali
*Iggy Pop as Curve
*Thomas Jane as Nemo
*Vincent Castellanos as Spider Monkey
*Eric Acosta as Danny
*Ian Dury as Noah
*Tracey Ellis as Sybil
*Alan Gelfant as Bassett

==Production==
 
After the success of The Crow, Miramax commissioned a sequel, and production began in August 1995. The Weinstein Brothers offered directing duties to Tim Pope on the basis of his work on a short film, Phone, he made in 1991. David S. Goyer was brought on to write the script. Wanting to avoid comparisons with the first film and Brandon Lee, Goyer originally intended to have the character of Sarah return as a female crow. Another idea was to set the story in 19th-century England. Eventually, it was decided the story would be centred on two brothers who are murdered in Los Angeles. In that script, Ashe and Danny were the brothers, with Ashe being the one brought back to avenge their deaths. The original script also featured Sarah, Grange and Top Dollar, the last two resurrected to fight Ashe. Goyer was unhappy about reviving Grange and Top Dollar and rewrote the script removing them entirely. Alex McDowell, who worked on the previous film and had also worked with Tim Pope on music videos, was brought back as production designer and both aimed to give the film a distinguished look. McDowell took inspiration for the design of Los Angeles by looking at architecture from the 1920s and 1940s.
 Dark City, La Reine Margot. For inspiration, Pérez looked to Jim Morrison and Prince Hamlet|Hamlet. A young Thomas Jane was picked to play the villain Nemo. While the filmmakers and studio originally intended to create a substantially different film to the first one (out of respect for Brandon Lee), Miramax ordered the film to be re-edited so as to resemble the earlier one as much as possible. Tim Pope refused and he, along with Goyer, eventually disowned the film, as it did not represent their vision.

==Release==
 

===Box office===
The Crow: City of Angels opened at #1 at the domestic box office and grossed $9,785,111 in the opening weekend which accounted for 54.6% of its total grossing. It also opened at #1 in the UK.  The US gross stands at $17,917,287,and $7,500,000 (worldwide). The budget was $13,000,000.  While the film was a minor success, it paled in comparison to the previous films US earnings of $50 million. As a result of the films lackluster box office results, future Crow sequels were released direct-to-video.

===Reception===
The film has a 12% approval rating on Rotten Tomatoes based on 33 reviews; the average rating is 3.4/10.  
 high definition standards.  It was further criticized for being erroneously labeled as 1080p when the film was, in fact, presented in 1080i.

===Home media===
The film was released on VHS in 1996. In 1998, the film was released on DVD. in 2001 collectors edition was released labeled "exclusive Directors Cut", restoring 11 minutes of extended footage, including two featurettes and audio commentary.

2011 saw the films reissue. It was re-released by  .  The only special feature was the widescreen format. There was also a single feature release under the same company.

The film was released on Blu-ray in May, 2011.

The film was released as a part of a boxset with The Crow: Salvation in the UK with 1080p picture and DTS-MA 5.1 sound. The only special features were 2 featurettes from the collectors edition.

On September 11, 2012, Echo Bridge released another  .)  It was already being sold at Walmart stores before its official release date had been reached.  This version contained bonus material not present in the original Blu-ray release.

On 10/7/2014, it was released on DVD by Lionsgate in a triple feature edition with the other two Crow sequels. 

===Pay-per-view/workprint version===
The Crow: City of Angels was heavily cut by Miramax, who wanted the film to be more like the first one. The films running time was originally 160 minutes long, while the theatrical version was just 84 minutes. A supposed directors cut was released in 1996, but only contained ten minutes of new footage. The uncut version did however surface on Request Pay-Per-View in the late 1990s. 

==Other media==
The screenplay for City of Angels was adapted into a novel by Chet Williamson as well as a three-issue comic book series published by Kitchen Sink Press. Both feature the original ending of the story, in which Ashe wanders the earth as an undead spirit.

A   was also produced.

==References==
 

==External links==
*  
*  
*  
*  
*  
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 