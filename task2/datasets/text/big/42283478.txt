Ramadasu (1964 film)
{{Infobox film
| name = Ramadasu
| image =
| caption =
| director =Chittor V. Nagaiah
| producer =Chittor V. Nagaiah
| writer = Gummadi Shivaji Ganesan Kannamba Mudigonda Lingamurthy
| music = Ashwathama 
| cinematography =
| editing =
| studio =
| distributor =V. N. Films
| released = 1 February 1964
| runtime =
| country = India
| language = Telugu
| budget =
}}

Ramadasu is a 1964 Telugu cinema|Telugu, devotional biographical film. The ensemble cast film was directed by veteran Chittor V. Nagaiah, who has also enacted the role Kancharla Gopanna. The blockbuster film has garnered the National Film Award for Best Feature Film in Telugu, and has garnered several state awards.   

==Soundtrack==
{| class="wikitable"
|-  style="background:#cccccf; text-align:center;"
| Track # || Song || Singer(s)
|-
| 1 Adigo Badradri Ghantasala P. B. Srinivas A. P. Komala
|-
| 2 Dandakam
|Chittor V. Nagaiah
|-
| 3 Dhanyudanaithini O Deva Chittor V. Nagaiah 
|-
| 4 E desamanununduvaru
|M. Satyam
|-
| 5 Jai Seetharama Raghu Rama Chittor V. Nagaiah
|-
| 6 Kaheka rona Mohammad Rafi
|-
| 7 Kondanda Rama Chittor V. Nagaiah
|-
| 8 Maa Bava Manchivadu
|P. Susheela
|-
| 9 Mohanakaara Rama Sulamangalam Sisters
|-
| 10 Naraharini Nammaka Chittor V. Nagaiah
|- 11
|O saadhulara Chittor V. Nagaiah Kamala Devi
|- 12
|Padyams Chittor V. Nagaiah
|- 13
|Padyams Chittor V. Nagaiah 
|- 14
|Pahimam Sri Ram ante Sulamangalam Sisters
|- 15
|Ram Naam Se Jyaada Mohammad Rafi
|- 16
|Rama Dasu Garu Chittor V. Nagaiah M. Satyam
|- 17
|Rama Pahimam Sulamangalam Sisters 
|- 18
|Rama Rama Anarada Chittor V. Nagaiah
|- 19
|Rama Rama yanuarada Chittor V. Nagaiah 
|- 20
|Ye Desam munununduvaru
|M. Satyam
|-
|}

==Awards== National Film Awards
*National Film Award for Best Feature Film in Telugu - 1964 

== References ==
 

 

 
 
 
 
 
 
 


 
 