Nobunaga Concerto
{{Infobox animanga/Header
| name            = Nobunaga Concerto
| image           =  
| caption         = Cover of the first volume of Nobunaga Concerto
| ja_kanji        = 信長協奏曲
| ja_romaji       = Nobunaga Kontseruto Historical
}}
{{Infobox animanga/Print
| type            = manga
| title           = 
| author          = Ayumi Ishii
| illustrator     = 
| publisher       = Shogakukan
| publisher_en    = 
| demographic     = Shōnen manga|Shōnen
| imprint         = 
| magazine        = Monthly Shōnen Sunday
| magazine_en     = 
| published       = 
| first           = June 2009
| last            =
| volumes         = 11
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = tv series
| title           = 
| director        = Yūsuke Fujikawa
| producer        = 
| writer          = Natsuko Takahashi
| music           = Masaru Yokoyama
| studio          = 
| licensor        =  Fuji TV, OHK
| first           = July 12, 2014
| last            = September 20, 2014
| episodes        = 10
| episode_list    = Nobunaga Concerto#Episode list
}}
{{Infobox animanga/Video
| type            = drama
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensor        =  Fuji TV
| network_en      = 
| first           = October 13, 2014
| last            = December 22, 2014
| episodes        = 11
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = 
| director        = 
| producer        = 
| writer          = 
| music           = 
| studio          = 
| licensor        = 
| released        = December 2015
| runtime         = 
}}
 

  is a Japanese manga series by Ayumi Ishii. It began serialization in Shogakukans Monthly Shōnen Sunday from 2009. It was announced on June 2014 issue of Monthly Shonen Sunday magazine that the historical manga is inspiring a television anime, a live-action television series, and a live-action film. Fuji TV.  The television drama aired on October 2014 starring Shun Oguri as Saburo and Nobunaga.  The live-action film will be released on December 2015.

==Plot==
The story centers around Saburō, a high school boy who time-travels to Japans Sengoku Era. He must become Oda Nobunaga, the famed warlord who helps unite Japan.   

==Characters==

===Main characters===
; 
: 
:Saburo is a high school boy who finds himself taken back in time to Japans Sengoku Era. He then meets Oda Nobunaga who asks him to become Oda Nobunaga because of their same look.
; 
: 
:The real Oda Nobunaga. He works under Saburō (fake Nobunaga) as Akechi Mitsuhide.
; 
: 
:Kichō is Nobunagas wife
; 
: 
; 
: 
:Oichi is Nobunagas younger sister.

===Other characters===
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 
; 
: 

==Anime adaptation==
The manga received an anime adaptation that started airing during summer 2014. Narrated by Shun Oguri.

===Episode list===

{|class="wikitable" style="width:98%; margin:auto; background:#FFF; table-layout:fixed;"
|- style="border-bottom: 3px solid #CCF;"
! style="width:3em;"  | No.
! Title
! style="width:12em;" | Original airdate
|-
{{Japanese episode list EpisodeNumber   = 1 EnglishTitle    = Saburou Nobunaga KanjiTitle      = サブロー信長 RomajiTitle     = Saburou Nobunaga OriginalAirDate = July 12, 2014 ShortSummary    = 
}}
{{Japanese episode list EpisodeNumber   = 2 EnglishTitle    = preparing to die in an effort to dissuade ones lord  KanjiTitle      = 諫死 RomajiTitle     = Kanshi OriginalAirDate = July 19, 2014 ShortSummary    = 
}}
{{Japanese episode list EpisodeNumber   = 3 EnglishTitle    = Viper of Mino KanjiTitle      = 美濃のマムシ RomajiTitle     = Mino no Mamushi OriginalAirDate = July 26, 2014 ShortSummary    = 
}}
{{Japanese episode list EpisodeNumber   = 4 EnglishTitle    = Battle of Okehazama KanjiTitle      = 桶狭間の戦い RomajiTitle     = Okehazama no Tatakai OriginalAirDate = August 2, 2014 ShortSummary    = 
}}
{{Japanese episode list EpisodeNumber   = 5 EnglishTitle    = Love Letter KanjiTitle      = らぶれたあ RomajiTitle     = Raburetaa OriginalAirDate = August 9, 2014 ShortSummary    = Saburou tries to take Mino by force and fails because of Inabayama castle. Hes rejoined by Matsudaira Takechiyo the womanizer. Saburou is in need of a stratagem to scare Minos people: Tokichirou comes up with the idea of building a fortress in one night. Saburou has also been sending letters to Minos main retainers who then take Inabayama and side with Owari. Matsudaira changes his name to Tokugawa Ieyasu. 
}}
{{Japanese episode list EpisodeNumber   = 6 EnglishTitle    = Mitsuhide Akechi KanjiTitle      = 明智光秀 RomajiTitle     = Akechi Mitsuhide OriginalAirDate = August 16, 2014 ShortSummary    = The original Nobunaga contacts Saburou and enters his service under the name of Akechi Mitsuhide while keeping his face hidden. Through his connections, they enter Tokyo to petition Ashikaga Yoshiaki to be the new shougun, and succeed, Mitsu taking care of the court formalities in place of Saburou. He also convinces Oichi to accept Saburous proposal of marrying Azai to strengthen the Oda clan. 
}}
{{Japanese episode list EpisodeNumber   = 7 EnglishTitle    = Kill Nobunaga KanjiTitle      = 信長を討て！ RomajiTitle     = Nobunaga wo ute! OriginalAirDate = August 23, 2014 ShortSummary    = Oichi, now married to Azai Nagamasa, has a daughter Chacha. Saburou names four generals: Shibata Katsuie, Niwa Nagahide, Mitsuhide and Tokichirou, who changes his name to Hashiba Hideyoshi for the occasion. Saburou orders Hideyoshi against the Asakura clan. Saburou meets Matsunaga Hisahide, also a time traveler. The shougun asks various daimyou to destroy Nobunaga: the Azai clan answers the call. 
}}
{{Japanese episode list EpisodeNumber   = 8 EnglishTitle    = An Unthinkable Betrayal KanjiTitle      = まさかの裏切り RomajiTitle     = Masaka no Uragiri OriginalAirDate = August 30, 2014 ShortSummary    = After conquering Kanegasaki during the Asakura campaign, the Oda forces are facing a pincer manoeuvre due to the treacherous Azai who attacks from Oumi. Saburou quickly decides to retreat to Kyoto: Hideyoshi volunteers to lead the rear-guard so he can finally exact revenge on the Oda. But Mitsuhide and Takenaka Hanbei from Mino accompany Hideyoshi, which leaves him no choice but to fight valiantly. 
}}
{{Japanese episode list EpisodeNumber   = 9 EnglishTitle    = A Thorny Road KanjiTitle      = イバラの道 RomajiTitle     = Ibara no Michi OriginalAirDate = September 13, 2014 ShortSummary    = The Oda move out against Azai HQ, Odani Castle, but its too well fortified so they target Yokoyama instead. Azai receives reinforcement lead by Asakura Kagitake and sorties. Ieyasus army comes to support Oda and defeats Asakura. Hideyoshi and Takenaka Hanbei win and occupy Yokoyama. Saburou goes to Settsu to subjugate the Miyoshi 3: he leaves the rear position of Usayama Castle in Mori Yoshinaris hands. Usayama is attacked by Asakura/Azai on their way to Kyoto. Mitsuhide is sent to Kyoto to strengthen their defense; Shibata handles the rear-guard of Settsu while the rest of Oda return to Usayama. Mori Yoshinari buys them time with his sacrifice. The Asakura/Azai army retreats to Enryaku Temple on Mount Hiei. 
}}
{{Japanese episode list EpisodeNumber   = 10 EnglishTitle    = Two Nobunagas KanjiTitle      = 二人の信長 RomajiTitle     = Futari no Nobunaga OriginalAirDate = September 20, 2014 ShortSummary    = The younger sons of Mori Yoshinari become Saburous pages (including Ranmaru), the eldest son Nagayoshi becoming a general. Saburou meets another time traveler who becomes his bodyguard Yasuke. Several targets are available for the campaign: Azais Odani Castle, Ishiyama Hongan Temple (Osaka), the Takeda, Mount Hiei (Kyoto). On the counsel of Mitsuhide, Saburou picks the Enryaku Temple of Mount Hiei.
}}
|}

==Reception==
It won the 2011 Shogakukan Manga Award for Shōnen manga.  It was nominated for the 5th Manga Taisho.  As of 19 February 2012, the 6th volume has sold 73,877 copies.  As of 19 August 2012, the 7th volume has sold 92,838 copies. 

==References==
 

==External links==
*   
* 

 
 

 
 
 
 
 
 
 
 
 
 


 
 