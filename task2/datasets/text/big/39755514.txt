Departmental
{{Infobox film
| name           = Departmental
| image          = 
| image size     =
| caption        = 
| director       = Keith Wilkes
| producer       = Alan Burke
| writer         = Mervyn Rutherford
| based on = 
| narrator       = Gary Day
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = ABC 
| released       = 1980
| runtime        = 70 mins
| country        = Australia
| language       = English
| budget         = 
| gross = 
| preceded by    =
| followed by    =
}}
Departmental is a 1980 Australian TV movie based on a play by Mervyn Rutherford. It was part of the ABCs Australian Theatre Festival. Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p43  Reviews were poor. 

==Plot==
The disappearance of money from a safe in a police station leads to an internal inquiry.

==References==
 

==External links==
* 
*  at AustLit
*  at AusStage

 
 
 


 