Whirlwind (1988 film)
 
{{Infobox film
| name           = Whirlwind
| image          =
| image size     =
| caption        =
| director       = Bako Sadykov
| producer       =
| writer         = Bako Sadykov Chingiz Aitmatov
| narrator       =
| starring       = Vladimir Msryan
| music          =
| cinematography = Rifkat Ibragimov
| editing        = V. Sadykova
| distributor    =
| released       = 1988
| runtime        = 98 minutes
| country        = Soviet Union
| language       = Russian
| budget         =
}}

Whirlwind ( ) is a 1988 Soviet action film directed by Bako Sadykov. It was screened in the Un Certain Regard section at the 1989 Cannes Film Festival.   

==Cast==
* Vladimir Msryan
* Dumitru Fusu
* Mukhamadali Makhmadov
* Makhmud Takhiri
* Isfandiyor Gulyamov - (as I. Gulyamov)
* Alfiya Nisambayeva
* Rustam Nugmagambetov
* Bobosaid Yatimov
* Dilorom Kambarova
* Rano Kubayeva
* Pyotr Krasichkov
* German Nurkhanov
* Dinmukhamet Akhimov

==References==
 

==External links==
* 

 
 
 
 
 


 
 