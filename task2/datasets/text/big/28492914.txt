Infection (2003 film)
{{Infobox film
| name           =Infection
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Krsto Papić
| producer       = 
| writer         = Mate Matišić Krsto Papić
| narrator       = 
| starring       = Leon Lučev Lucija Šerbedžija Sven Medvešek
| music          = Alan Bjelinski Mate Matišić
| cinematography = Goran Trbuljak
| editing        = Robert Lisjak
| studio         = 
| distributor    = 
| released       = 22 July 2003
| runtime        = 105 min
| country        = Croatia
| language       = Croatian
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Infection (Infekcija) is a 2003 Croatian film directed by Krsto Papić. It is a remake of The Rat Savior, Papićs 1976 film.

==Cast==
*Leon Lučev – Ivan Gajski
*Lucija Šerbedžija – Sara
*Sven Medvešek – Professor Bošković
*Filip Šovagović – The Mayor
*Dražen Kuhn – The Director
*Ivo Gregurević – Muller
*Božidar Alić – General Genz
*Vili Matula – Karlo
*Dejan Aćimović – Master of Ceremony
*Boris Miholjević – The Inspector
*Ana Karić – Mrs. Rudolph
*Slavko Juraga
*Vanja Drach
*Nada Gačešić

==External links==
*  

 

 
 
 
 

 