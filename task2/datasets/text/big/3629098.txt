Hated: GG Allin and the Murder Junkies
{{Infobox Film 
| name = Hated: GG Allin and the Murder Junkies 
| image = GG_Allin_Hated.JPG
| writer = 
| starring = GG Allin Merle Allin Donald "Dino Sex" Sachs Shireen Kadivar Dee Dee Ramone Geraldo Rivera Unk
| director = Todd Phillips 
| producer = Todd Phillips
| distributor = Skinny Nervous Guy Prod. 
| released   = 1993
| runtime = 53 minutes
| country = United States
| language = English 
| music = GG Allin 
| budget = 
}}
 Old School, Starsky & The Hangover trilogy.

==Plot== Geraldo Riveras talk show. After the credits, his funeral and corpse are also briefly included.

==Reception==
===Reaction from GG Allin===
GG Allin attended a screening of Hated just days before his death.   at SuicideGirls  While the film was being shown, a heavily intoxicated Allin threw several beer bottles at the movie screen, one of which injured a woman. As a result, the screening was halted and Allin fled just before police arrived. YouTube video: " ."  Although he never saw the complete film, Allin was happy with the light in which Todd Phillips had portrayed him, and gave the director positive feedback and a hug the day after the screening. 

===Reviews===
Hated has received positive reviews from Revolver (magazine)|Revolver magazine, Combustible Celluloid, eFilmCritic.com, Rotten Tomatoes page: " ."  DVDTalk,  Digitally Obsessed,  and Monsters at Play.  The film has received few reviews from mainstream publications.

==Home media==
Hated was released on DVD in December 1999 that features the last show Allin performed in New York on the evening before his death, the band rehearsal session prior to it and the ensuing carnage afterwards. It was re-released with new footage in 2007. The first 5,000 copies came with free temporary tattoos based on some of Allins real tattoos, and a mail-in offer for a poster featuring artwork of Allin painted by serial killer and acquaintance John Wayne Gacy. A bonus feature on the region 1 DVD shows a still of Allin lying in his coffin. He is dressed in a jacket and jock strap.

==Soundtrack==
{{Infobox album 
|  Name        = Hated
|  Type        = studio
|  Artist      = GG Allin
|  Cover       = GG_Allin_Hated_Soundtrack_Front_Cover.jpg 
|  Background  =
|  Released    = 1993
|  Recorded    = Various Locations
|  Genre       = 
|  Length      = 34:01
|  All Music   = GG Allin
|  All Lyrics  = GG Allin
|  Label       = 
|  Producer    = 
}}
All songs by GG Allin except for "Carmelita (song)|Carmelita" by Warren Zevon
{{tracklist
| title1   = Die When You Die
| length1  = 1:59
| title2   = Bite It You Scum
| length2  = 3:43 Carmelita
| length3  = 3:24
| title4   = Snakemans Dance
| length4  = 3:21
| title5   = Young Little Meat
| length5  = 1:55
| title6   = Fuck Authority
| length6  = 3:38
| title7   = Gypsy Motherfucker 
| length7  = 3:52
| title8   = Suck My Ass It Smells
| length8  = 1:00
| title9   = Outlaw Scumfuc
| length9  = 2:36
| title10   = Cunt Sucking Cannibal
| length10  = 3:16
| title11   = I Wanna Kill You
| length11  = 3:20
| title12   = When I Die
| length12  = 4:07
}}

==References==
 

==External links==
*  
*  

 
 

 
 
 
 
 
 