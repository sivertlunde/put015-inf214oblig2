Sindhu Bhairavi (film)
{{Infobox film
| name = Sindhu Bhairavi
| image = Sindhu Bhairavi dvd.jpg
| caption = Official DVD Box Cover
| director = K. Balachander    
| producer = Rajam Balachander   Pushpa Kandaswamy
| writer = K. Balachander  
| narrator = Suhasini  Sulakshana  Janagaraj  
| music = Ilaiyaraaja    
| cinematography = Raghunatha Reddy 
| editing = Ganesh-Kumar
| studio = Kavithalayaa Productions
| distributor = Kavithalayaa Productions
| released = 11 November 1985 
| runtime = 159 min
| country = India
| language = Tamil
| budget =
| gross =  1.7 crore
| preceded_by =
| followed_by =
}}
 National Awards Best Actress, Best Music Best Female Playback Singer.

The plot of the film revolves around the three lead characters J. K. Balaganapathi (JKB), Sindhamani aka Sindhu and Bhairavi. A Carnatic singer (JKB) at the peak of his career loses his credibility as he gets addicted to alcohol due to the loss of an intimate companion (Sindhu). Bhairavi (JKBs wife) tries to help him get rid of the addiction and fails. As a last resort she requests Sindhus help and succeeds. The status of relationship between the three after his makeover is the climax of the film.
 television series named Sahana produced by K. Balachander. The film ended with Sindhu leaving her son with JKB and Bhairavi. The serial Sahana (Sindhu Bhairavi 2) starts 19 years later. Sindhus son Surya is now 19 years old. Bhairavi has given birth to a daughter, but they have no clue where Sindhu is.   

== Plot == tambura musician, and he is a compulsive liar. The film starts with an introduction of all characters of the film followed by JKB going to a concert where he finds Gurumoorthy (Delhi Ganesh), who works for JKB as a Mridangam musician, drunk. He instructs Gurumoorthy to leave the premises and performs in the concert without the use of Mridangam which is considered a basic/compulsory instrument to perform a carnatic concert. When Gurumoorthy is asked to permanently leave the band, he promises not to drink again. While Bhairavis grandfather is going to receive his pension, which he does on the first of every month, Gajapathi lies to him that his pension has been cancelled. The old man starts crying and JKB intervenes to confirm that his pension is not canceled and that he can go and collect it. JKB then scolds Gajapathi for it and tells him the old man could have died on hearing such a lies, as he is very particular about his pension. Following this, when JKB finds out that Gajapathi has lied to him too and others causing problems he asks him to promise to speak the truth.
 Telugu Language and she finds some the audience speaking to each other instead of listening to the music. Hence, she asks JKB to translate the song he is singing into Tamil language|Tamil, which everybody in the audience can understand. He then gets angry and challenges her to demonstrate. She successfully demonstrates it and is applauded by the audience. In a few days, Sindhu apologizes to JKB and he finds out that she is an intellectual equal and admires her knowledge of music. However this intellectual attraction is misunderstood based on the cultural limitations. Gajapathi, who has promised not to lie, informs Bhairavi that her husband is going out with Sindhu.

Meanwhile, Sindhu really has fallen for JKB and both of them get intimate with each other. One day, JKB is caught coming out of Sindhus apartment by Bhairavi. Sindhu is seen as the home wrecker and is forced to quit her association with JKB. He pines for her and leans on alcohol for support, which sadly leads to his downward spiral ending in an embarrassing barter of his musical knowledge for a drink. He even steals money from Bhairavis grandfather, which leads to his death. Sindhu reenters his life and brings him around, however she has a secret of her own, the secret of her pregnancy. After Sindhu brings back JKB from his alcoholic state, JKBs friends pressurize Sindhu to leave the town and to never come back, which she does the very next day. Both Bhairavi and JKB are distraught as Bhairavi had agreed to marry Sindhu to JKB.

A couple of months later, during one of JKBs concerts, Sindhu is seen coming back and sitting down to listen to his music. However, she refuses to marry JKB saying that she wouldnt deny Bhairavi her rights. Instead she leaves them with a "present"; Sindhu gives up her child to be brought up by Bhairavi in a classic act of defiance to societys rules and leaves town to pursue and share her knowledge of music with the less fortunate.

== Cast ==
* Sivakumar as J. K. Balaganapathi. JKB is a popular Carnatic singer who is known for his knowledge of music. Suhasini as Sindhu. Sindhu works as a music teacher in a school and is a fan of JKB Sulakshana as Bhairavi. Bhairavi is the wife of popular singer JKB
* Delhi Ganesh as Gurumoorthy. Works as a Mridangam musician for JKB Janagaraj as tambura musician for JKB
* Prathap Pothan as Sanjeevi
* T. S. Raghavendra as Sindhus father
* Manimala as Sindhus mother

==Production==
Sivakumar recalled the instance when he had to grapple with high tides when he sat on the rocks at the Visakhapatnam beach during the shoot. He recalled "Nothing would deter KB   from extracting the best out of the actors. He would ask us to get ready for a retake even if he finds a slightest slip in our performance". 

== Awards ==
The film has won the following awards since its release:
 National Film Awards (India) Best Actress Suhasini     Best Music Direction - Ilaiyaraaja   Best Female Playback Singer - K. S. Chithra  

== Sahana == teleserial produced by K. Balachander. It is a sequel of the popular film Sindhu Bhairavi which K. Balachander also directed and produced.   The first episode was aired on "Jaya TV" on 24 February 2003.    The characters of the film are now much older in the serial. Balachandar usually names his female lead characters after Carnatic music ragas — Srividya was called "Bhairavi (Carnatic)|Bhairavi" and Jayasudha as "Ranjani" in Apoorva Raagangal, Sulakshana was "Bhairavi" in Sindhu Bhairavi and Khusboo was "Sriranjani" in Jathi Malli and now comes Sahana (raga)|Sahana.    Though Balachander had been toying with the idea of making a sequel to Sindhu Bhairavi for the last 10 years, something stopped him from doing so. "I feared people would compare the two and say the sequel was not as good. I didnt want to take such a big commercial risk" he once said. He has been producing television serials through Minbimbangal, the television offshoot of his movie production outfit Kavithalaya. Towards the end of his successful series Anni, he went through his old files looking for an idea for his next serial and chanced on the rough sequel to the film he had written 10 years ago. 
 Suhasini was willing to play Sindhu but Balachander decided to have a different face—he felt if Suhasini played Sindhu, viewers would expect Sivakumar to play JKB. "You cannot separate JKB and Sindhu. Sir (Balachander) felt if we are going to have a different JKB, lets have a different Sindhu too," says Subha. This is when Y. Gee. Mahendra, a well-known theater and film personality was selected to play JKB. "It is a challenge for a comedy artist like me to do a character role like JKB. I am enjoying both the challenge and the role," he said. He, who has acted in more than 300 films, said it is a kind of homecoming for him. "I acted in a Balachander film 26 years ago!". He was not worried about the fact that JKB was once successfully portrayed by Sivakumar. "Had it been done by Sivaji (Sivaji Ganesan), I would not have accepted the role. Only two people inspire me, and they are K. Balachander and Sivaji . When I have KB on the sets to guide me, why should I worry? I also have my other guru Sivaji in mind".
 Sulakshana who Dr M Balamurali Krishna and Sudha Ragunathan will render the songs. 

==Themes and influences==
The films theme is based on extra-marital affairs intervowen with carnatic music. 

Ajayan Bala stated: "most compatible minds in Sindhu Bhairavi, the singer and his fan, were not allowed to live together, even after the wife reconciled herself to that".  Kandhasamy, Balachanders son-in-law stated that: "Sindhu Bhairavi established the sanctity of the institution of marriage and the dignity of the concubine". 

== Soundtrack ==
{{Infobox album  
| Name = Sindhu Bhairavi
| Type = soundtrack
| Longtype =
| Artist = Ilaiyaraaja
| Cover = Sindhu Bhairavi cd.jpg
| Cover size =
| Caption =
| Released =  
| Recorded =
| Genre =
| Length = 38:29   Tamil
| Label =
| Producer =
| Reviews =
| Compiler =
| Misc =
}}
 National Award for her prolific singing in the song "Padariyen Padippariyen". Ilaiyaraaja won National Film Award for Best Music Direction. The song "Naanoru Sindhu" is based on Sindhubhairavi Raga,  "Kalaivaniye" is based on Kalyani Raga,   "Poomaalai" is based on Kaanada Raga,  "Paadariyen" is based on Saramati Raga  while "Aanantha Nadanam" is based on Rathipatipriya Raga. 

Sindhu Bhairavi was Balachanders first collaboration with Ilaiyaraaja.  Balachander recalled that for the film he wanted a different composer, so he approached Ilayaraja after seeking permission from Vishwanathan. When the song ‘Padariyein Padipariyein was to be set to tune, Raja asked Balachander for a days time. The next day he was ready with the tune. 

This was a film where Balachander was considered successful in reflecting the thoughts of Ilayaraaja.     Innovatively there is no use of Mridangam in the song "Mahaganapathim".  This is picturized in the film as a scene where Gurumoorthy (Mridangam musician) arrives drunk to a concert and is asked to leave the orchestra and hence, the days concert performance has to happen without Mridangam.

The way the folk song "Paadariyen" merges with the Carnatic song "Mari Mari" is admirable. This is picturized in the film as a scene where Sindhu challenges JKB at a carnatic concert. Ilayaraaja is said to have believed that all the emotions, feelings and depth found in the carnatic songs are adequately, if not, equally available in the folk tunes. When Balachander is said to have narrated the situation to him that Sindhu sings a folk song and most unexpectedly switches to a carnatic tune. Within 24 hours he came with up the song which became the biggest controversy in his career, he faced staunch opposition from certain classical musicians for having changed the original raga from Khamboji to Saramati 

{{tracklist
| collapsed =
| headline =
| extra_column = Singers
| total_length =
| all_writing =
| all_music =
| writing_credits =
| lyrics_credits =
| music_credits =
| title5 = Kalaivaniye
| note5 =
| writer5 =
| lyrics5 = Vairamuthu
| music5 =
| extra5 = K. J. Yesudas
| length5 = 3:54
| title1 = Mahaganapathim
| note1 =
| writer1 = Muthuswamy Dikshitar
| lyrics1 = Muthuswamy Dikshitar
| music1 = Muthuswamy Dikshitar
| extra1 = K. J. Yesudas
| length1 = 4:32
| title2 = Mari Mari Ninne
| note2 =
| writer2 = Thyagaraja
| lyrics2 = Thyagaraja
| music2 = Thyagaraja
| extra2 = K. J. Yesudas
| length2 = 2:48
| title4 = Moham Ennum
| note4 =
| writer4 = Subrahmania Bharati
| lyrics4 = Subrahmania Bharati
| music4 =
| extra4 = K. J. Yesudas
| length4 = 2:41
| title3 = Poomaalai Vaangi Vanthaan
| note3 =
| writer3 =
| lyrics3 =
| music3 =
| extra3 = K. J. Yesudas
| length3 = 4:30
| title6 = Naan Oru Sindhu
| note6 =
| writer6 =
| lyrics6 =
| music6 =
| extra6 = K. S. Chithra
| length6 = 4:03
| title7 = Paadariyen
| note7 =
| writer7 =
| lyrics7 =
| music7 =
| extra7 = K. S. Chithra
| length7 = 5:29
| title8 = Thanni Thotti
| note8 =
| writer8 =
| lyrics8 =
| music8 =
| extra8 = K. J. Yesudas
| length8 = 6:00
}}

== References ==
 

== External links ==
*  

==Bibliography==
*  

 
 
 

 
 
 
 
 
 
 
 