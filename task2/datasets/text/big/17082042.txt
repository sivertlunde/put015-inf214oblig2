5th Ave Girl
{{Infobox film
| name           = 5 th  Ave Girl
| image          = 5thAveGirlPoster.jpg
| caption        = Theatrical release poster
| director       = Gregory La Cava
| producer       = Gregory La Cava Allan Scott   Gregory La Cava (uncredited)   Morrie Ryskind (uncredited story outline)
| starring       = Ginger Rogers   Walter Connolly
| music          = Robert Russell Bennett
| cinematography = Robert De Grasse
| editing        = Robert Wise RKO Radio Pictures
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $607,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p55 
| gross          = $1,370,000 
}}

 
5 th  Ave Girl is a 1939 comedy film about a millionaire who feels neglected by his family, so he hires a young woman to stir things up. It stars Ginger Rogers and Walter Connolly.

==Plot== Kathryn Adams), and Tim have all forgotten or do not care. 

Feeling lonely, he goes to Central Park, where he meets Mary Grey (Ginger Rogers), a young, out-of-work woman. Seeing that she has only a meager meal to last the day, he invites her to dine with him at a fancy nightclub. They get drunk, start dancing, and are spotted by Martha and her boyfriend. The next morning, he awakes with a hangover and a black eye, to discover that he had apparently invited Mary to spend the night in a guest room.
 James Ellison). Embarrassed by the resulting newspaper gossip column items and shunned by her friends, Martha first calls family psychiatrist Dr. Kessler (Louis Calhern), but he finds nothing wrong with her now-cheerful and carefree husband. She starts staying home, plotting ways to drive Mary out. She has Tim try to buy her off, but that fails. Tim makes no effort to hide his contempt for the interloper, but eventually, he falls in love with her. Meanwhile, Mary tries to help Katherine, who is in love with an unnoticing Mike. 

Finally, Mary can no longer continue with the charade and tearfully confesses the truth. Katharine shows up and announces she has married Mike, who has decided to quit and open a repair shop. At first, Martha is aghast, but then Borden reminds her that they started their own marriage in about the same way, and she grudgingly accepts her new son-in-law. Borden then retreats to his bedroom, but Martha invites him into hers. Mary leaves, but Tim finds her, picks her up, and carries her back into the mansion. When a policeman tries to interfere, Mary tells him to mind his own business.

==Cast==
* Ginger Rogers as Mary Grey
* Walter Connolly as Alfred Borden
* Verree Teasdale as Martha Borden James Ellison as Mike
* Tim Holt as Tim Borden Kathryn Adams as Katherine Borden
* Franklin Pangborn as Higgins, the butler
* Ferike Boros as Olga, another servant
* Louis Calhern as Dr. Kessler
* Theodore von Eltz as Terwilliger (as Theodor Von Eltz)
* Alexander DArcy as Maitre D
* Bess Flowers as Woman with Mr. Pape (uncredited)

==Reception==
The film was a hit and earned a profit of $314,000.  However it was generally believed that Tim Holt was miscast and he made few comedies for the rest of his career. Richard Jewell & Vernon Harbin, The RKO Story. New Rochelle, New York: Arlington House, 1982. p134 

==References==
 
==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 