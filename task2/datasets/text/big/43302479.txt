Tentacolino
{{Infobox film
| name = Tentacolino
| image =
| image_size =
| caption =
| director = Kim J. Ok 
| producer = Orlando Corradi
| writer = Clelia Castaldo Orlando Corradi Loris Peota
| starring = Jane Alexander Anna Mazzotti Francis Pardeilhan Gregory Snegoff
| music = Gianni Sposito
| cinematography =
| editing = David Pollini
| studio = Mondo TV
| distributor = Mondo TV
| released =  
| country = Italy
| runtime = 88 minutes
| language = Italian
| budget = €4,000,000
| gross =
}}

Tentacolino ( ), also known as In Search of the Titanic, is a 2004 animated film. It is a sequel to the 1999 Italian animated film The Legend of the Titanic directed by Kim J. Ok.

==Plot==
In search of the Titanic, a group of friends find themselves inside a bathysphere and awake in Atlantis. With the help of the King of Atlantis, they do their best to recover the Titanic.

==Cast==
*Jane Alexander - Elizabeth
*Anna Mazzotti - Ronnie
*Francis Pardeilhan - Don Juan
*Gregory Snegoff - Smile

==References==
 

==External links==
* 

 

 
 
 
 
 

 