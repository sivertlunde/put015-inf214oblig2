Wings in the Dark
{{Infobox film
| name           = Wings in the Dark
| image          = Poster - Wings in the Dark 01.jpg
| image_size    = 225px
| caption        = Theatrical release poster
| director       = James Flood
| producer       = Arthur Hornblow, Jr.
| story          =
| screenplay     = James Kirkland (screenplay) John Partos (screenplay) Dale Van Every (adaptation) E.H. Robinson (adaptation)
| writer         =
| narrator       =
| starring       = Myrna Loy Cary Grant
| music          = Heinz Rohmheld
| cinematography = William C. Mellor William Shea
| distributor    = Paramount Pictures
| released       =  
| runtime        = 75 minutes
| country        = United States English
| budget         =
}} blind protagonist (played by Cary Grant) during the 1930s, and is also known for its accomplished aerial photography.   

Nell Shipman, one of the writers of the original story "Eyes of the Eagle," which pivoted upon a fictionalized version of Amelia Earhart, whom Shipman knew personally, was extremely disappointed by Myrna Loys performance and the virtual exclusion of a seeing eye dog as one of the main characters.     Graham Greene, in a review, called the film "as sentimental as it is improbable," but "as exciting as it is naive."     Wings in the Dark was one of a number of poorly done aviation movies    made during the early part of the Great Depression|Depression.   

The film was directed by James Flood and produced by Arthur Hornblow, Jr.

==Cast==
* Myrna Loy as Sheila Mason
* Cary Grant as Ken Gordon
* Roscoe Karns as Nick Williams
* Hobart Cavanaugh as Mac
* Dean Jagger as Top Harmon
* Russell Hopton as Jake Brashear
* Matt McHugh as 1st Mechanic
* Graham McNamee as Radio Announcer

==References==
 

==External links==
  at the Internet Movie Database

 
 
 
 