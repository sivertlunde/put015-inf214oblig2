The Railroad Man
 
{{Infobox film|
image=Il ferroviere.jpg|
name=The Railroad Man|
director=Pietro Germi|
writer=Pietro Germi Alfredo Giannetti Luciano Vincenzoni|
producer=Carlo Ponti|
music=Carlo Rustichelli|
cinematography=Leonida Barboni|
starring=Pietro Germi Saro Urzì Luisa Della Noce Sylva Koscina Edoardo Nevola|
distributor=|
released=11 August 1956 (Italian release)|
runtime=118 minutes|
country=Italy|
language=Italian|
}}
The Railroad Man ( ) is a 1956 Italian drama film directed by Pietro Germi.  
==Plot==
Train operator Andrea Marcocci has to witness the suicide of a desperate man who jumps in front of his train. Under the influence of this shock he starts making mistakes. A check up by a doctor reveals that hes at the brink of becoming an alcoholic. Due to this evaluation he is degraded and must accept a salary cut.

==Cast==
* Pietro Germi - Andrea Marcocci
* Luisa Della Noce - Sara Marcocci
* Sylva Koscina - Giulia Marcocci (as Silva)
* Saro Urzì - Gigi Liverani
* Carlo Giuffrè - Renato Borghi
* Renato Speziali - Marcello Marcocci
* Edoardo Nevola - Sandro Marcocci (as il piccolo Edoardo Nevola)

==Awards==
*1956 Cannes Film Festival : OCIC Award - Special Mention    
*Nastro dArgento : Best Director, Best Producer.
*San Sebastian Film Festival : Best Film, Best Director, Best Actress (Luisa Della Noce)

==References==
 

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 
 