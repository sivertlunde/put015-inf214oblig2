The Love Match
{{Infobox film
| name           = The Love Match
| image          = 
| caption        = 
| director       = David Paltenghi 
| producer       = Maclean Rogers
| writer         = 
| starring       = Arthur Askey 
| music          = Wilfred Burns Arthur Grant
| editing        = Joseph Sterling
| studio         = 
| distribution   = British Lion Films
| released       = 1955
| runtime        = 
| country        = United Kingdom
| language       = English
| gross = £174,991 (UK)  
}} British comedy film directed by David Paltenghi and starring Arthur Askey, Glenn Melvyn, Thora Hird and Shirley Eaton.  Two football-mad railway engine drivers are desperate to get back in time to see a match. It was based on a play by Glenn Melvyn.

==Cast==
* Arthur Askey ...  Bill Brown
* Glenn Melvyn ...  Wally Binns
* Thora Hird ...  Sal Brown
* Shirley Eaton ...  Rose Brown
* James Kenney ...  Percy Brown Edward Chapman ...  Mr. Longworth Danny Ross ...  Alf Hall
* Robb Wilton ...  Mr. Muddlecombe
* Anthea Askey ...  Vera
* Patricia Hayes ...  Emma Binns
* Iris Vandeleur ...  Mrs. Entwhistle
* William Franklyn ...  Arthur Ford Leonard Williams ...  Aggressive Man
* Peter Swanwick ...  Mr. Hall
* Dorothy Blythe ...  Waitress
* Reginald Hearne ...  Police Constable Wilfred
* Maurice Kaufmann ...  Harry Longworth

==References==
 

==External links==
* 

 
 
 
 
 
 
 

 
 