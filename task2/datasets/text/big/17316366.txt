Young@Heart (film)
{{Infobox film
| name           = Young@Heart
| image          = Young at heart poster08.jpg
| caption        = Theatrical release poster Stephen Walker
| producer       = Sally George
| writer         = 
| starring       = 
| music          = 
| cinematography = Ed Marritz Simon Poulter 
| editing        = Chris King
| distributor    = Fox Searchlight Pictures
| released       =  
| runtime        = 107 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} Stephen Walker. Its focus is Young@Heart (group)|Young@Heart, a New England chorus of senior citizens that sings contemporary and classic rock and pop songs.

==Synopsis==
Young@Heart, a chorus of twenty-two senior citizens with an average age of eighty, is directed by a frequently demanding Bob Cilman. In preparation for a concert in their hometown of Northampton, Massachusetts, they spend two months learning new material ranging from James Brown to the Pointer Sisters to The Clash and Sonic Youth, a task thats daunting for them and frequently frustrating for Cilman. At one point they take a break to visit a nearby low-security prison and entertain the inmates, who literally and figuratively embrace them after they perform. The groups determination to succeed increases with the passing of two of their members in rapid succession, and the concert proves to be a major success with the community.

==Production==
The film was produced for British television   and edited for its theatrical release. 

It premiered at the Los Angeles Film Festival, where it won the Audience Award for Best International Feature, and was shown at South by Southwest, the Florida Film Festival, and the Philadelphia International Film Festival before going into limited release in the United States.

==Critical reception==
Young@Heart received largely favorable reviews from critics. Rotten Tomatoes reports 88% of critics gave the film positive reviews, based on 78 reviews,  and Metacritic reports the film had an average score of 75 out of 100, based on 23 reviews. 

Stephen Holden of the New York Times observed, "At moments the movie . . . risks being a cloying, rose-colored study of happy old folks at play, and the cheer sounds forced. But the lives of the several members it examines at some depth are too real and complicated to resemble a commercial starring Wilford Brimley as a Norman Rockwell grandpa. The movie offers an encouraging vision of old age in which the depression commonly associated with decrepitude is held at bay by music making, camaraderie and a sense of humor."  

Steve Davis of the Austin Chronicle rated the film 3½ out of four stars. He felt that "despite an occasional lapse into nudge-nudge jokes about geriatric sex, incontinence, and the driving skills of the elderly," it "eschews the clichés about old people for something that we can all relate to: our own mortality."  
 New York Daily News and The Hollywood Reporter. 

==Box office performance==
The film opened in four theaters in the United States on April 9, 2008 and grossed $50,937 in its opening weekend. It eventually grossed a total of $3,992,189 in the US. 

==Awards and nominations==
The film won the Rose dOr for Best Art Documentary and the Audience Awards at the Atlanta Film Festival and the Warsaw International Film Festival.

==DVD release==
20th Century Fox Home Entertainment released the film on DVD on September 16, 2008. It is in anamorphic widescreen format, with audio tracks in English and Spanish and subtitles in English, Spanish, and French. Bonus features include Young@Heart Goes to Hollywood, eight deleted scenes, and two music videos, one a medley of the Bee Gees "Stayin Alive" and Gloria Gaynors "I Will Survive", the other "Road to Nowhere" by Talking Heads.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 