Irréversible
 Irreversible}}
 
{{Infobox film
| name = Irréversible
| image = Irreversible_ver2.jpg
| alt = 
| caption = Theatrical release poster
| director = Gaspar Noé
| producer = {{Plainlist|
* Brahim Chioua
* Vincent Cassel}}
| writer = Gaspar Noé
| starring = {{Plainlist|
* Monica Bellucci
* Vincent Cassel
* Albert Dupontel}}
| music = Thomas Bangalter
| cinematography = {{Plainlist|
* Benoît Debie (lighting)
* Gaspar Noé (camera)}}
| editing = Gaspar Noé
| studio = {{Plainlist|
* Les Cinémas de la Zone
* StudioCanal}}
| distributor = Mars Distribution
| released =  
| runtime = 97 minutes  
| country = France
| language = {{Plainlist|
* French
* English
* Italian
* Spanish}}
| budget = 
| gross = €4.5 million  
}} mystery Thriller thriller film soundtrack was composed by the electronic musician Thomas Bangalter, who is best known as half of the duo Daft Punk.

American film critic Roger Ebert called it "a movie so violent and cruel that most people will find it unwatchable."  Irréversible competed at the 2002 Cannes Film Festival and won the Stockholm International Film Festivals award for best film.

Irreversible has been associated with a series of films defined as the cinéma du corps ("cinema of the body"), which according to Palmer share affinities with certain avant-garde productions: an attenuated use of narrative, assaulting and often illegible cinematography, confrontational subject material, a pervasive sense of social nihilism or despair.  Irreversible has also been associated with the New French Extremity movement.

==Plot==
Irréversible contains thirteen scenes presented in reverse chronological order. They are discussed here in the films chronological order. 7th Symphony strobe effect, accompanied by a pulsing, roaring sound. A rapidly spinning image of the cosmos can be dimly perceived. A title card reads: "Le Temps Detruit Tous" ("Time destroys everything") a phrase uttered in the films first scene. The film ends.
# Alex sits on the bed clothed, her hand on her belly. A poster for  , with the tagline "The Ultimate Trip," is above the headboard.
# Alex lies in bed with Marcus (Vincent Cassel) after having sex. Alex reveals she might be pregnant, and Marcus is pleased with the possibility. They prepare to go to a party, and Marcus leaves to buy wine.  Alex takes a shower, then uses a home pregnancy test that confirms she is pregnant. She is elated.
# At a nearby Paris Métro station and aboard a subway train, Alex, Marcus, and Pierre (Albert Dupontel) are on their way to a party. They discuss sex and Pierre refers to the fact that he and Alex were once dating, but are no longer in a relationship. He implies that Marcus stole Alex from him.
# Alex, Marcus, and Pierre have arrived at the party. Alex is annoyed by Marcuss unrestrained use of drugs and alcohol and his flirtatious behavior with other women, and consequently decides to leave the party alone. 
# On her way home, Alex sees a pimp called "Le Tenia" ("The Tapeworm") (Jo Prestia) beating a transsexual prostitute named Concha (Jaramillo) in a pedestrian underpass. Once the man sees Alex, he releases Concha and turns his attention to Alex, who attempts to flee, but Le Tenia catches her and threatens her with a knife. Le Tenia pins Alex to the ground and anally rapes her for several minutes of screentime, after which he brutally beats her into unconsciousness.
# Marcus and Pierre leave the party and encounter commotion on the street. Marcus wails as he discovers Alexs bloodied body being wheeled on a stretcher into an ambulance by paramedics.
# Alex is hospitalized and revealed to be comatose. Marcus and Pierre are questioned by the police. They then talk to a street thug named Mourad (Mourad Khima) and his friend Layde (Hellal). The two gangsters promise, if they get paid, to help them find the rapist, whom Mourad claims is Le Tenia. Marcus and Pierre go looking for the man who raped Alex. Marcus is still high on drugs and very agitated. Everything in the story moves at a frenetic pace.
# The men track down Concha, Le Tenias last victim. At first, she refuses to talk to them. After Marcus threatens to slash her with a piece of broken glass, she identifies Le Tenia as the rapist and says he can be found at a gay BDSM nightclub called The Rectum. They are soon chased by angry sex workers seeking to defend Concha. Mourad and Layde run in a separate direction.
# Marcus and Pierre hail a taxi. Following a row, Marcus assaults the taxi driver and steals the car. Thinking the other man is Le Tenia, he assaults him, but the man wrestles Marcus to the ground, breaks Marcuss arm, and attempts to rape Marcus on the club floor. Pierre defends Marcus by grabbing a fire extinguisher and using it brutally to crush the mans skull to a pulp, thus killing him. Le Tenia the source of all the havoc stands there groggily, perhaps not believing he got away. homophobic insults at them. The murdered man is revealed not to be Le Tenia after all. Rather, the man standing next to him in the club was the real Le Tenia. I Stand Alone. In a drunken monologue, the Butcher reveals that he was arrested for having sex with his own daughter. The subject of their discussion shifts to the commotion in the streets outside. Without looking out the window, they derisively attribute the commotion to the patrons of The Rectum. Outside, Mourad is seen talking to a police officer.

==Cast==
 
* Monica Bellucci as Alex 
* Vincent Cassel as Marcus
* Albert Dupontel as Pierre
* Jo Prestia as Le Tenia
* Mourad Khima as Mourad
* Hellal as Layde
* Jaramillo as Concha
* Michel Gondoin as Mick
* Jean-Louis Costes as Fistman
* Philippe Nahon as the man (The Butcher)
* Stéphane Drouot as Butchers friend
* Stéphane Derdérian and Gaspar Noé as Rectum Visitors
 

==Production==
 
Irréversible was shot using a widescreen 16mm process. Many of the scenes were shot with multiple takes that were then edited together using digital processing, creating the illusion that the scene was filmed all in one shot, with no cuts or edits. This included the rape scene, portrayed in a single, unbroken shot. Although the penis can be seen after the rape, this was later digitally added in editing with computer-generated imagery. Another example is with the scene where Pierre bludgeons a man to death, crushing his skull. Computer graphics were brought in to augment the results, as initial footage using a conventional latex dummy proved unconvincing. The process can be watched in the bonus material of the films DVD. The film uses extremely infrasound|low-frequency sound during the opening scenes to create a state of disorientation and unease in the audience.

==Reception==
  Ansen stated that "If outraged viewers (mostly women) at the Cannes Film Festival are any indication, this will be the most walked-out-of movie of 2003." In the same review, Ansen suggested that the film displayed "an adolescent pride in its own ugliness". 

Critical response to the film was divided. As of 2011, it held a score of 57% positive verdict from 120 reviews at Rotten Tomatoes, with an average rating of 5.7/10.  The American film critic Roger Ebert argued that the films structure makes it inherently moral; that by presenting vengeance before the acts that inspire it, we are forced to process the vengeance first, and therefore think more deeply about its implications. 

The film won the top award, the Bronze Horse for best film, at the 2002 Stockholm International Film Festival. It was nominated for the Best Foreign Language Award by the Film Critics Circle of Australia. It was voted Best Foreign Language Film by the San Diego Film Critics Society, tied with Les Invasions Barbares.  It grossed $792,200 from theatrical screenings. 

==Controversy==
Film critic David Edelstein argues that "Irreversible might be the most homophobic movie ever made."  Noés depiction of gay criminal Le Tenia inexplicably raping the female lead, Alex, remains the films most controversial image. In his defense, Noé has stated, "I’m not homophobic," further stating that "I also appear in Irreversible, masturbating at the gay club," as a means of showing that "I didnt feel superior to gays." 

==See also==
 
* Irréversible (soundtrack)|Irréversible soundtrack Human reactions to infrasound
* New French Extremity
 

==References==
 

==External links==
*  
*  
*  
*  
*   on  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 