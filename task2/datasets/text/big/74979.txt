My Man Godfrey
{{Infobox film
| name           = My Man Godfrey
| image          = My man godfrey.jpg
| image_size     = 225px
| caption        = Theatrical Film Poster
| director       = Gregory La Cava
| producer       = Charles R. Rogers Eric Hatch Morrie Ryskind Gregory LaCava (uncredited)
| starring       = William Powell Carole Lombard
| music          = Charles Previn Rudy Schrager (both uncredited)
| cinematography = Ted Tetzlaff
| editing        = Ted J. Kent Russell F. Schoengarth
| distributor    = Universal Pictures
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = $656,000 (est.)
| gross          =
}} Eric Hatch.  The story concerns a socialite who hires a derelict to be her familys butler, only to fall in love with him, much to his dismay. The film stars William Powell and Carole Lombard. Powell and Lombard were divorced years earlier, but were good friends.
 remade in 1957 with June Allyson and David Niven in the starring roles.  In 1999, the original version of My Man Godfrey was deemed "culturally significant" by the United States Library of Congress and selected for preservation in the National Film Registry.

==Plot==
Source: 

During the Great Depression, Godfrey "Smith" Parke (William Powell) is living alongside other men down on their luck at the city dump. One night, spoiled socialite Cornelia Bullock (Gail Patrick) offers him five dollars ($83 in 2013) to be her "forgotten man" for a scavenger hunt. Annoyed, he advances on her, causing her to retreat and fall on a pile of ashes. She leaves in a fury, much to the glee of her younger sister, Irene (Carole Lombard). After talking with her, Godfrey finds her to be kind, if a bit scatter-brained. He offers to go with Irene to help her beat Cornelia.

In the ballroom of the Waldorf-Ritz Hotel, Irenes long-suffering businessman father, Alexander Bullock (Eugene Pallette), waits resignedly as his ditsy wife, Angelica (Alice Brady), and her mooching "protégé" Carlo (Mischa Auer) play the frivolous game. Godfrey arrives and is "authenticated" by the scavenger hunt judge as a "forgotten man". He then addresses the idle rich, expressing his contempt for their antics. Irene is apologetic and offers him a job as the family butler, which he gratefully accepts.

The next morning, Godfrey is shown what to do by the sardonic, wise-cracking maid, Molly (Jean Dixon), the only servant who has been able to put up with the antics of the family. She warns him that he is just the latest in a long line of butlers. Only slightly daunted, he proves to be surprisingly competent, although Cornelia still holds a sizable grudge. On the other hand, Irene considers Godfrey to be her protégé, and is thrilled by his success.

A complication arises when a guest, Tommy Gray (Alan Mowbray), greets Godfrey familiarly as an old friend.  Godfrey quickly ad-libs that he was Tommys valet at school. Tommy plays along, mentioning Godfreys non-existent wife and five children. Dismayed, Irene impulsively announces her engagement to the surprised Charlie Van Rumple (Grady Sutton), but she soon breaks down in tears and flees after being politely congratulated by Godfrey.

Over lunch the next day, Tommy is curious to know what one of the elite "Parkes of Boston" is doing as a servant. Godfrey explains that a broken love affair had left him considering suicide, but the optimistic, undaunted attitude of the men living at the dump rekindled his spirit.

Meanwhile, when everything she does to make Godfreys life miserable fails, Cornelia sneaks into his room and plants her pearl necklace under his mattress. She then calls the police to report her "missing" jewelry. To Cornelias surprise, the pearls do not turn up, even when she suggests they check Godfreys bed. Mr. Bullock realizes his daughter has orchestrated the whole thing and sees the policemen out.

The Bullocks then send their daughters off to Europe to get Irene away from Godfrey. When they return, Cornelia implies that she intends to seduce Godfrey. Worried, Irene stages a fainting spell and falls into Godfreys arms. He carries her to her bed, but while searching for smelling salts, he realizes shes faking when he sees her (in a mirror) sit up briefly. In revenge, he puts her in the shower and turns on the cold water full blast. Far from quenching her attraction, this merely confirms her hopes: "Oh Godfrey, now I know you love me...You do or you wouldnt have lost your temper."
 sold short, using money raised by pawning Cornelias necklace and then buying up the stock that Bullock had sold. He gives the endorsed stock certificates to the stunned Mr. Bullock, thus saving the family from financial ruin. He also returns the necklace to a humbled Cornelia, who apologizes for her attempt to frame him.  Afterwards, Godfrey takes his leave.

With the rest of his stock profits and reluctant business partner Tommy Grays backing, Godfrey has built a fashionable nightclub at the dump called "The Dump", "...giving food and shelter to fifty people in the winter, and giving them employment in the summer." Godfrey tells Tommy he quit being the Bullocks butler because "he felt that foolish feeling coming along again." Later on, though, Irene tracks him down and bulldozes him into marriage and the movie ends with her saying, "Stand still, Godfrey, itll all be over in a minute."

==Cast==
 , Mischa Auer and Alice Brady in My Man Godfrey.]]

* William Powell as Godfrey 
* Carole Lombard as Irene Bullock
* Alice Brady as Angelica Bullock
* Gail Patrick as Cornelia Bullock
* Eugene Pallette as Alexander Bullock
* Jean Dixon as Molly
* Alan Mowbray as Tommy Gray
* Mischa Auer as Carlo Pat Flaherty as Mike Flaherty
* Robert Light as Faithful George
* Grady Sutton as Charlie Van Rumple (uncredited)

Cast notes:
* Jane Wyman appears, uncredited, as a party guest
* Franklin Pangborn has a small role, uncredited, as the judge for the scavenger hunt
 Best Actress]]

==Production==
My Man Godfrey was in production from April 15 to May 27, 1936, and then had retakes in early June of the year.  Its estimated budget was $656,000. 
 Universal borrowed William Powell from Metro-Goldwyn-Mayer|MGM.  Powell, for his part, would only take the role if Carole Lombard played Irene.  Powell and Lombard had divorced three years earlier. TCM   

La Cava, a former animator and freelancer for most of his film career, held studio executives in contempt, and was known to be a bit eccentric.  When he and Powell hit a snag over a disagreement about how Godfrey should be portrayed, they settled things over a bottle of Scotch.  The next morning, La Cava showed up for shooting with a headache, but Powell didnt appear.  Instead, the actor sent a telegram stating: "WE MAY HAVE FOUND GODFREY LAST NIGHT BUT WE LOST POWELL. SEE YOU TOMORROW." Genevieve McGillicuddy   

Eric S. Hatch wrote the screenplay, assisted by Morrie Ryskind.

Due to insurance considerations a stand-in stuntman (Chick Collins) was used when Godfrey carried Irene over his shoulder up the stairs to her bedroom. 

When tensions hit a high point on the set, Lombard had a habit of inserting four letter words into her dialogue, often to the great amusement of the cast. This made shooting somewhat difficult, but clips of her cursing in her dialogue and messing up her lines can still be seen in blooper reels. 

==Release==
My Man Godfrey premiered on September 6, 1936, and was released in the United States on the 17th of September.  It was a runaway hit and earned huge profits for the studio. 

==Awards and recognition==
My Man Godfrey was nominated for six Academy Awards:
 Best Director – Gregory La Cava Best Actor – William Powell Best Actress – Carole Lombard Best Writing, Screenplay – Eric Hatch and Morrie Ryskind Best Supporting Actor – Mischa Auer Best Supporting Actress – Alice Brady
 nominated in all four acting categories, in the first year that supporting categories were introduced.  Its also the only film in Oscar history to receive a nomination in all four acting categories and not be nominated for Best Picture, and was the only film to be nominated in these six categories and not receive an award until 2013, when American Hustle did the same thing. 
 100 funniest Premiere magazine voted it one of "The 50 Greatest Comedies Of All Time" in 2006. It is one of the few movies that hold 100% rating on Rotten Tomatoes.

American Film Institute recognition
* 2000: AFIs 100 Years... 100 Laughs #44

==Sequels and adaptations==
My Man Godfrey was adapted for radio and broadcast on  , again starring William Powell. When the film was remade in 1957, David Niven played Godfrey opposite June Allyson, directed by Henry Koster.  

== Video availability == 20th Century colorized version. The original film has lapsed copyright and is freely available to download.

==Notes==
 

==External links==
 
 
*  
*  
*  
*  
*  
*  
*  

; Streaming audio
*   on Lux Radio Theater: May 9, 1938 Academy Award Theater: October 2, 1946
*   on Theater of Romance: July 11, 1944
*   on Theater of Romance: July 21, 1947

 

 
 
 
 
 
 
 
 
 
 
 
 