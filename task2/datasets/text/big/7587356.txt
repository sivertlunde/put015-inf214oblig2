Romeo.Juliet
{{Infobox Film
| name           = Romeo.Juliet
| image          = 
| caption        = 
| director       = Armando Acosta, also credited as Armondo Linus Acosta and Armand Acosta
| producer       = Paul Hespiel (exec. prod.), Andree Castagnee (ass.), Paul Celis (ass.), Greet Ooms (ass.)
| writer         = Armando Acosta, Koen Van Brabant,
| starring       = John Hurt, Robert Powell, Francesca Annis, Vanessa Redgrave, Ben Kingsley, Quentin Crisp, Dame Maggie Smith, Victor Spinetti, John Haggart 
| music          = Armando Acosta, Emanuel Verdi
| cinematography = Armando Acosta
| editing        = Jan Reniers, Armando Acosta
| distributor    = Loeb & Loeb
| released       = 
| runtime        = 130 minutes
| country        = Belgium
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Romeo.Juliet is the title of a 1990 film version of William Shakespeares classic play Romeo and Juliet.  It was made by American producer, director and cinematographer, Armando Acosta (also credited as Armondo Linus Acosta and Armand Acosta) using the feral cats of Venice, New York, and Ghent as actors, with the voices dubbed by some of the greats of the English theatre including Ben Kingsley, Maggie Smith, Vanessa Redgrave, Robert Powell, Francesca Annis, Victor Spinetti, Quentin Crisp, and John Hurt.  The score of the film features Serge Prokofievs Romeo and Juliet Ballet as performed by the London Symphony Orchestra, André Previn conducting and an original theme composed by Armando Acosta and Emanuel Verdi, performed by the London Symphony Orchestra conducted by Barry Wordsworth. 
 bag lady (played by John Hurt), the only human being in the film, who takes the cats of Venice and puts them on a boat, which sails to the New World.

The world premiere was held at the 1990 Venice Film Festival.  Former festival director, Guglielmo Biraghi invited the film to be screened out of competition.  Romeo.Juliet was later screened at the Flanders Film Festival and Cologne Film Festival.  In January 1992, the film was screened in Los Angeles at the Directors Guild Theatre, Writers Guild Theatre, and at Warner Brothers studio.

The motion picture was conceived and created as a film-in-concert with a live orchestra performing the soundtrack with the projection of the movie. The World Premiere of the Romeo.Juliet Film-in-Concert was held at the Palais des Beaux Arts in Brussels in June 1992. British born conductor, Nicholas Cleobury, led the National Orchestra of Belgium in three performances.  John Hurt attended the premiere along with Oleg Prokofiev, son of composer, Serge Prokofiev.  Oleg Prokovfiev stated in an interview, "...its not simply a film, its a poem.  Its a higher art than cinema, its super cinema.  A special cinema which does not follow a classical story line, but harmoniously blends my fathers ballet music, Shakespeares text and the magical images of the film."  A second series of film concerts was held in Japan in February 1993 at the NHK Hall with Yoko Matsuo conducting the New Japan Philharmonic Orchestra.

The film is described as having been difficult to produce, requiring over 400 hours of footage of the feline cast to assemble the images for the final film. The movie was shot entirely on video and then successfully transferred to 35mm.  It is also remarkable for being one of the few major releases in which several members of the production team gave up their salaries to produce the film.    It has not been re-released for the home video market. This lack of availability, which has been described as making it "more rare than the dinosaur"   has made it a sought-after item in some circles.

==References==
*Andrew, Geoff.  
*Jacobs, Patricia A.  
* 
* 

== External links ==
 
*  
*  

 

 
 
 
 
 