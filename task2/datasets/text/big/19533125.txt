De Dhakka
 
 

{{Infobox film
| name           = De Dhakka 
| image          = DeDhakka.jpg
| caption        = Theatrical poster of De Dhakka
| director       = Sudesh Manjrekar, Atul Kale
| producer       = Satya Films, Zee Talkies
| writer         = Mahesh Manjrekar
| Dialogue       =  ]
| narrator       =
| starring       = Shivaji Satam Makarand Anaspure Siddarth Jadhav Medha Saksham Kulkarni Gauri Vaidya
| music          = Ajit-Atul-Sameer Arati Ankalikar-Tikekar (Playback)
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Marathi
| budget         = 
| gross          =   (lifetime)
}}
De Dhakka (  film released in India in 2008 inspired by the Hollywood film Little Miss Sunshine (2006).

Arati Ankalikar-Tikekar, the play back singer of this film, received Maharashtra State Award, V Shantaram Award and Maharashtra Times award in the category of Best Play back singer. {{cite web
|author=Limaye, Yogita date       = 7 June 2008  title      = Marathi film industry gets a push with De Dhakka  url        = http://ibnlive.in.com/news/marathi-film-industry-gets-a-push-with-de-dhakka/66758-8.html  publisher  = CNN-IBN accessdate = 20 June 2011
}} 

==Cast==
*Shivaji Satam
*Makarand Anaspure
*Siddarth Jadhav
*Medha
*Saksham Kulkarni
*Gauri Vaidya

==Plot==
The story revolves around the Jadhav family. Makarand(Makarand Anaspure) after spending all his wealth has invented an auto part which he claims will drastically lower the fuel consumption of vehicles. But being from a rural background and having no formal education he is never taken seriously. Subhanrav(Shivaji Satam) makarands father leaves no opportunity to blame his son for selling all his land on failed pursuits. Sumi (Medha) is makarands humble 2nd wife. While the family is going through an economic crisis, a golden opportunity is presented when makarands daughter is selected for the final round of a dance competition with a huge prize money. The family scrapes their last resources and leaves on a life changing journey to reach the competition venue.

==Reception==
De Dhakka was a success at the box office. The movie garnered Rs. 40&nbsp;million in its third week. {{cite web
|author=Nivas, Namita date       = 13 June 2008 title      = De Dhakka’s success, a high for Marathi films  url        = http://www.screenindia.com/news/De-Dhakkas-success-a-high-for-Marathi-films/321911/  publisher  = Screen India accessdate = 20 June 2011
}}  Critics received the film well noting the movie had a good storyline and performances. {{cite web
|author=Patil, Pradip title      = Review - De Dhakka  url        = http://www.marathimovieworld.com/review/de-dhakka-review.php  publisher  = Marathi World.com accessdate = 20 June 2011
}} 

==References==
 

==External links==
* 

 
 
 
 


 