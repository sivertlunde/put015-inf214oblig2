The World's Best Prom
{{Infobox Film |
  name           = The Worlds Best Prom |
 
  director       = Ari Vena Chris Talbott |
  producer       = Ian Rosenberg Chris Talbott |
  distributor    = Matson Films |
  runtime        = 80 min. | English
}}
The Worlds Best Prom is a 2006 documentary film about a high school prom in Racine, Wisconsin, a city that for the past 50 years has been annually gripped by prom mania. Prom was originally released as a short film and was expanded to feature-length documentary length. It was released in its longer form on April 4, 2006. It was filmed on location in Racine and was shot entirely in digital video. This film was originally a 17-minute short film of the same name which won Best Documentary at the Wisconsin Film Festival. The film was also re-edited as mini-episodes for Truth, but the episodes never aired. A version for CurrentTV is available.

==Plot==
The film focuses on the prom-obsessed residents of Racine and, in particular, two very different girls and one boy (Tonya, Dori, and Ben) who are followed in the days and nights leading up to their prom night. The "mega prom" was introduced to Racine almost 50 years ago by the citys Rotary Club after an alcohol-related car accident. As a result, the Rotary decided to sponsor a post-prom party for the citys high schools as a safe alternative for prom-goers.

In the film, the city of Racine is portrayed as a racially mixed population with a similar socio-economic status. The film paints an honest, humorous portrait of this post-industrial Midwestern town and the long and rich history of its one-of-a-kind prom.

Some of the students are going to college after graduation; others are headed to the military. We are given an inside look at everything from the students selection of gowns to dinner. The celebration begins with a rowdy parade where students are shown riding fire engines, 18-wheelers, and even elephants through the city streets. Prom-goers from seven city high schools converge on one city-wide prom to make red carpet entrances.

As the credits roll, we are given an update almost five years later about some of the people featured in the film. Several have not achieved their high school goals. Others have lost contact with their high school sweethearts. One heartfelt scene shows a couple going off to war.

==People==
*Tonya Witherspoon is a shy student from Racines inner city who dreams of leaving Racine and becoming a forensic pathologist. 
*Dori Sorensen is an outgoing, rebellious Catholic school student with big prom expectations.
*Ben is a soft-spoken football player with a trouble-making, but protective, older brother. For Ben and his mother, graduation and prom will be a significant milestone in their lives: Ben has expectations of leaving Racine and starting his life anew.

==Trivia==
* Producer Ian Rosenberg and Director/Producer Chris Talbott grew up in Racine. Both went to the prom.

==DVD Extras==
* Celebrity Prom Stories - With Susan Sarandon, Bob Balaban, Sam Seder, Seth Herzog, and others.
* Photo Gallery
* Racine Prom 1953
* Portrait of Racine 1951
* Conversation with the Filmmakers - With Chris Talbott, Ian Rosenberg, and Ari Vena

==Crew==
* Editor Ari Vena
* Co-Producers  , Hillevi Loven, Karen Sorensen, Ari Vena, Mary Wigmore
* Executive Producer Debra Meltzer
* Principal Photography Hillevi Loven
* Musical Score  
* Original Music Matthew Fanuelle, Saturday Music Group, Genji Siraisi, Wheeliebar

==Media==
* 
* 
* 

==External links==
* 
* 
*  - Production company
*  - Distributor
*  - NPR story from Day to Day, June 24, 2005

 
 
 
 
 
 
 
 
 
 