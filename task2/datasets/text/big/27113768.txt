Bernie (1996 film)
{{Infobox film name           = Bernie director       = Albert Dupontel writer         = Albert Dupontel Gilles Laurent starring       = Albert Dupontel Roland Blanche Hélène Vincent Claude Perron released       =   runtime        = 87 minutes country        = France language       = French
}}
Bernie is a 1996 movie from French director and actor Albert Dupontel. 

==Plot==
It tells the story of Bernie Noël, a 29-year old man whos been raised all his life in an orphanage in Paris suburbs. He was found in a garbage can when he was only a few months old. His first name comes from the man who found him there (Bernie, the buildings janitor) and his last name comes from the time of year when he was found (Noël, "Christmas" in French).

At age 29, Bernie decides to leave the orphanage to explore a world that he knows only through television and what his friends have told him. On his own, roaming a Paris-by-night hostile environment, he goes through several madly epic adventures searching for his parents, before eventually finding them and "saving" them from an imaginary government conspiracy.

This neurotic and maladjusted young man will bring mischief and mayhem in his trail, which will lead him and his loved ones to a dramatic conclusion...

==Film info==
* Directed by : Albert Dupontel
* Story : Albert Dupontel and Gilles Laurent
* Dialogues : Albert Dupontel
* Production : Canal+, Contre Prod, Kasso Inc. Productions, Rézo Films, Ulysse Films
* Category: Comedy (?)
* Running time: 87 min
* Country: France
* Sound: Dolby Digital
* Language: French
* Release :
* November 27, 1996 (France)
* January 8, 1997 ( Belgium)
* Rated: PG13

==Cast==
* Albert Dupontel: Bernie Noël
* Roland Blanche: Donald Willis, Bernies father
* Lucia Sanchez: Maria
* Paul Le Person: Bernie, the janitor
* Hélène Vincent: Bernies mother
* Claude Perron: Marion
* Roland Bertin: Ramonda
* Catherine Samie: Granny
* Alain Libolt: Dr. Clermont
* Pascal Ternisien: Edouard Clermont
* Emmanuelle Bougerol: Marie-Solange Clermont
* Philippe Uchan: Vallois
* Nicolas Marié: the police captain
* Éric Elmosnino: the salesman
* Antoinette Moya: the real-estate agent
* Michel Vuillermoz: the transvestite
* Loïc Houdré: the dealer
* Yves Pignot: the orphanages manager

==Reception & critics==
The film has been awarded the "Best First Cinematographic Work" at the French "César" event.

==References==
 

==External links==
*  

 
 
 


 