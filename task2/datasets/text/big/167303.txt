A Canterbury Tale
 
 
{{Infobox film
| name           = A Canterbury Tale
| image          = Canterburytaleposter.jpg
| image_size     =
| caption        = US theatrical poster (1949)
| director       = Michael Powell Emeric Pressburger
| producer       = Michael Powell Emeric Pressburger
| writer         = Michael Powell Emeric Pressburger
| narrator       = UK:  
| starring       = Eric Portman Sheila Sim Dennis Price John Sweet (actor)|Sgt. John Sweet Kim Hunter (US release) Allan Gray
| cinematography = Erwin Hillier
| editing        = John Seabourne Sr.
| distributor    = General Film Distributors  Eagle-Lion Films
| released       = 21 August 1944 (UK)  21 January 1949 (US)
| runtime        = 124 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}}
 British film Michael Powell and Emeric Pressburger. It stars Eric Portman, Sheila Sim, Dennis Price and John Sweet (actor)|Sgt. John Sweet; Esmond Knight provided narration and played several small roles. For the postwar American release, Raymond Massey narrated and Kim Hunter was added to the film. The film was made in black and white, and was the first of two collaborations between Powell and Pressburger and cinematographer Erwin Hillier. 

A Canterbury Tale takes its title from  The Canterbury Tales of Geoffrey Chaucer, and loosely uses Chaucers theme of eccentric characters on a religious pilgrimage to highlight the wartime experiences of the citizens of Kent, and encourage wartime Anglo-American friendship and understanding.

==Plot==
  (modern photograph)]]
 John Sweet), Land Girl, Miss Alison Smith (Sheila Sim). The group arrive at the railway station in the fictitious small Kent town of Chillingbourne (filmed in Chilham, Fordwich, Wickhambreaux and other villages in the area), near Canterbury, late on Friday night, 27 August 1943. Peter has been stationed at a nearby Army camp, Alison is due to start working on a farm in the area, and Bob left the train by mistake, hearing the announcement "next stop Canterbury" and thinking he was in Canterbury.
 war games.
 Home Guard uniform kept in the town hall to carry out his attacks.
 WAC in cinema organist Canterbury police, as he had planned to do. However, Colpeper still has to do penance; his initial dislike of Alison having turned into affection, his hopes are dashed by the news of her boyfriends survival.

==Cast==
* John Sweet (actor)|Sgt. John Sweet, US Army as Bob Johnson
* Dennis Price as Peter Gibbs
* Sheila Sim as Alison Smith
* Eric Portman as Thomas Colpeper Charles Hawtrey as Thomas Duckett
* Esmond Knight as Narrator / Seven-Sisters Soldier / Village Idiot
* Eric Maturin as Geoffreys Father
* Kim Hunter as Johnsons Girl (US release)
* Raymond Massey as Narrator (US version) (voice)

==Production==
The film is notable for its many exterior shots showing the Kent countryside, as well as extensive bombsites in Canterbury itself, so soon after the infamous Baedeker raids of May/June 1942 which had destroyed large areas of the city centre. Many local people, including a lot of young boys, were recruited as extras for the extensive scenes of childrens outdoor activities such as river battles and Childrens den|dens. The Cathedral itself was not available for filming as the stained glass had been taken down, the windows boarded up and the organ, an important location for the story, removed to storage, all for protection against air raids. By the use of clever perspective, large portions of the cathedral were recreated within the studio by art director Alfred Junge. {{cite book
  | last =Powell
  | first =Michael
  | authorlink =Michael Powell
  | title =A Life in Movies: An Autobiography Heinemann
  | year =1986
  | location =London
  | isbn = 0-434-59945-X}} 
 Selling Station appears in the film as "Chilingbourne" Station at the beginning of the film. Bob and Alison ride on a cart through the village, the local Wickhambreaux Mill can be clearly seen. The local village pub "The Red Lion" was used for exterior shots of "The Hand of Glory" Inn where Joe stays whilst in the village. 

==Style and themes== German Expressionist style. But this is harnessed to a neo-romanticism|neo-romantic sense of the English landscape. This sense that the past always haunts the present in the English landscape was a powerful theme that would be mined by countless British novelists and film-makers from the 1960s onwards.

Described as morally weird but forever English , its characters, rare for mainstream cinema, play out their moral choices instead of merely verbalising them. 

===Anglo-American (mis)understandings===
 
A major theme is Johnsons problems with, and gradual acceptance of, the differences and common ground of American and British 1940s life and heritage, along with the townsfolks acceptance of him. These include:
* His surprise at the station that so small a settlement as Chillingbourne is a town
* His lack of acquaintance with the British blackout (wartime)|blackout, particularly in his use of a torch that is far too bright for it, at the station and in chasing the "glue man" (after Johnson brags about his "flashlight", Gibbs later satirically calls it a "searchlight")
* American sergeants stripes (chevrons) being upside-down compared with British ones (a repeated joke) G Men" London policemen but they know their ways and do their duty, and that with regard to guns "This is Chillingbourne ... not Chicago.")
* The identical ways of woodworking between Oregon and Kent in his chat with Mr Horton that gives a mutual understanding and respect between Johnson and Mr Horton. quarter (quarter-dollar) being equivalent to a shilling, to the boy standing on the hay cart near the beginning and to the boys in the mill
* Drug stores being called grocers, just after the river battle
* Telephones, mirrors, tea drinking, left-hand driving, as he lists when struggling with a British telephone River Stour after climbing a hill towards the end of the film
* When offered tea, he asks Gibbs how he can "drink the stuff". Gibbs replies that Johnson has joined the tea drinkers of the world, for the only countries that havent been defeated and conquered yet by the Nazis and Japanese are the tea-drinking ones, England, Russia, and China.
* At the films end, Johnson reluctantly accepts an offer of tea from his American friend Mickey who says "its a habit you just fall into – like marijuana" (to which Johnson replies, “I’ll take marijuana.” (Or possibly, “I don’t take marijuana,” with ‘don’t’ slurred into take. Midwestern accent. The actor was from Minnesota.) “You’ll drink tea and like it.” “I’ll drink it, but I won’t like it.” (1 hour 57 minutes) Oregon red cedar, cedar shingles, 1887...well, that was a good job too".
 A Matter of Life and Death.

==Reception== John Sweet in October 2000.  The film initially had very poor reviews in the UK press,  and only small audiences.
 A Matter flashback similar to the openings of The Way to the Stars and 12 OClock High. Sweet was actually filmed in New York with the sequences combined.  The film was fully restored by the British Film Institute in the late 1970s and the new print was hailed as a masterwork of British cinema. It has since been reissued on DVD in both the UK and USA.

==Music featured== Allan Gray for the film, musical works featured include:
* Angelus ad Virginem mid-15th century polyphony heard as a peal of bells in orchestral guise under the opening titles
* Commando Patrol by Allan Gray, Stan Bowsher, Walter Ridley – quickstep heard in the background during Johnson and Gibbss scene in the lobby of the Hand of Glory
* I See You Everywhere by Allan Gray, Stan Bowsher, Walter Ridley – slow foxtrot heard in the background during Johnson and Gibbss scene in the lobby of the Hand of Glory
* Turkey in the Straw – folksong heard as Agnes leaves Bobs bedroom
* Come to the Church in the Wild Wood – Bob sings as he washes
* Hear my prayer, O Lord by Henry Purcell – the ethereal choral music heard as Gibbs pauses on entering the cathedral
* Bond of Friendship – Regimental March of the Kings Division. Played as the band nears the Cathedral
* Toccata and Fugue in D minor, BWV 565 by Johann Sebastian Bach|J.S. Bach (the original while inside the cathedral and the orchestration by Leopold Stokowski outside the cathedral) and the hymn Onward Christian Soldiers – played on the organ by Gibbs

==Acknowledgements==
Before the credits, the following plays over an image of the cathedral from the Christ Church Gate:
 

==Major characters==
===Sgt. Bob Johnson===
 ) is asleep]]
(John Sweet)  Three Sisters American Army invasion of Europe. He becomes more and more willing to learn something about England during his visit.

The original script mentioned that Johnson was on his way to Canterbury as his ancestors had come from there.  The producers had originally planned to use Burgess Meredith in the role but changed their mind in favour of an unknown. Meredith acted as a script editor for Johnsons character. 

===Sgt. Peter Gibbs===
(Dennis Price)  cinema organist from London. He has been conscripted into the British Army and has just been stationed at the military camp outside Chillingbourne, where his unit is engaged in training manoeuvres.

He disembarks from the train at Chillingbourne and, as he and Bob Johnson are escorting Alison Smith from the station to the town hall, he witnesses the attack by the "glue man". A cynical young Londoner, he initially has no time for any thoughts about Kentish history of the land or its people, but is converted by the end of the film, just as his unit leave the camp and are deployed to an unnamed location.

===Alison Smith===
(Sheila Sim)  JP in caravan with RAF and is missing in action at the outset of the film. (He is reported at the end as alive and in Gibraltar.) Alison is determined to solve the mystery of the "glue man" and seeks the help of Bob Johnson to do so. Johnson replies "You need about as much help as a Flying Fortress"

===Thomas Colpeper, JP===
 
(Eric Portman)  magistrate in Chillingbourne. He is a bachelor, living with his mother and, being very keen on the local history of the area, wants to share that knowledge with everyone around him, particularly with the soldiers from elsewhere in England who have been billeted nearby.

===Narrator / Seven-Sisters Soldier / Village Idiot===
(Esmond Knight)  Canterbury Tales, followed by a piece in Chaucerian style on the changes to Kent since Chaucers time (both only in the non-US version).

The Seven Sisters Road|Seven-Sisters Soldier is the British sergeant at the lecture who gets into conversation with Bob and then joins Peter and Alison.

The Village-Idiot supplies some information for Peter after the lecture, and is mocked for his speech impediment.

===The Boys===
(Leonard Smith, James Tamsitt and David Todd) 
The film uses an adventure and river battle between a group of boys as part of the bucolic setting. The boys were all local to the Canterbury area. Three of them were selected for more important, speaking roles. Leonard Smith played "General" Leslie, James Tamsitt played "General" Terry and David Todd played "Commander" Todd, the boy crying in the boat after the river battle.

The boys also help with the hunt for the "glue Man" by providing some local information, distracting Colpeper so that Peter Gibbs may search a bit more thoroughly and by handing in the receipt book from the grocers which shows that Colpeper had been purchasing gum and other ingredients of glue long-distance from Rymans in Canterbury. For their reward in obtaining evidence in the manner of Sherlock Holmess Baker Street Irregulars, Johnson buys the boys a football, seen in the films final scene in the end credits where they are no longer playing war games.

===The Hortons===
(George Merritt and Edward Rigby) 
An important scene takes place in the yard of the local wheelwright and blacksmith. This serves to remind us of the importance of the horse and cart and the knowledge of the old ways of doing things that have served the British countryside for generations. The blacksmith, Ned Horton, was played by George Merritt. The wheelwright, Neds brother, Jim Horton, was played by Edward Rigby. The real Horton brothers, Ben and Neville, are seen acting as assistants to the actors. Alison doesnt seem to be able to communicate properly with these country folk despite she and they both speaking British English (indeed, he initially tries to make fun of her for her lack of knowledge of obscure wheelwrighting terms). In contrast, although hes a foreigner, Bob can talk to them because he and Jim Horton both know about woodworking and felling and can speak as equals on that topic.

==Legacy==
Margaret Mitchell, the author of Gone With the Wind, was killed by a speeding automobile whilst walking to a screening of this film in Atlanta, Georgia, USA in 1949.

There is now an annual festival based around the film, in which film fans tour the films locations. 

Several video artists have recut the more visionary sections of the film as video-art. 

The film was shown in the nave of Canterbury Cathedral on 19 September 2007 to help raise money for the  . 

The film was shown in Chilham village hall to help raise money for the restoration of the war memorial. It was shown on 11 May 2014, the 70th anniversary of the films première in Canterbury. Chilham was one of the villages around Canterbury where the majority of the film takes place.

===Parody===
The theme of the film was used by Spike Milligan for the Goon Show The Phantom Head Shaver of Brighton in 1954. 

==References==
 

===Bibliography===
 
*  , 1986. ISBN 0-434-59945-X.
* Powell, Michael. Million Dollar Movie. London: Heinemann, 1992. ISBN 0-434-59947-6.
* Tritton, Paul.  . Canterbury: Tritton Publications, August 2000. ISBN 0-9524094-2-9.
 

==External links==
*  
*  
*  
*  
*   at the  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 