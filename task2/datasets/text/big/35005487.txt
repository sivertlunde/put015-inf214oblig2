Zelal
{{multiple issues|
 
 
}}
{{Infobox film
| name           = Zelal
| image          = 
| alt            =  
| caption        = 
| director       = Marianne Khoury Mustapha Hasnaoui
| producer       = Gabriel Khoury Marianne Khoury
| screenplay     = Marianne Khoury Mustapha Hasnaoui
| starring       = 
| music          = 
| cinematography = Tamer Joseph Victor Credi
| editing        = Doaa Fadel
| studio         = 
| distributor    = 
| released       =   
| runtime        = 90 minutes
| country        = Egypt France Morocco United Arab Emirates
| language       = 
| budget         = 
| gross          = 
}}
 documentary film.

== Synopsis ==
Zelal is an invitation to delve into the world of psychiatry and "madness" in Egypt. It meets the ordinary madmen and women banished to mental institutions by Egyptian society and offers more than just a journey into their world of shadows. The hospitals end up becoming the only place patients can conceive, not because they are truly "crazy", but because they fear the outside world. The film forces viewers to put their own preconceptions and interpretations to the test, reminding us that freedom is precarious in a society that does not tolerate any differences.

== Awards ==
* Dubai 2010

== References ==
 

 
 
 
 
 
 
 
 
 
 


 
 