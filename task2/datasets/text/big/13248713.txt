Kiss Them for Me (film)
{{Infobox film
| name           = Kiss Them for Me
| image          = KissthemformeDVD.jpg
| caption        = DVD cover of the film
| director       = Stanley Donen
| producer       = Jerry Wald
| writer         = Luther Davis
| based on       = Kiss Them for Me (play)
| screenplay     = Julius J. Epstein Frederic Wakeman Leif Erickson Larry Blyden
| music          = Lionel Newman Cyril J. Mockridge
| cinematography = Milton R. Krasner Robert L. Simpson
| studio         = Jerry Wald Productions Twentieth Century-Fox Film Corporation
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $1,945,000 
| gross          = $1,800,000 (US rentals) 
}}
 1957 comedy Twentieth Century-Fox Broadway Kiss play of Leif Erickson, and Larry Blyden.

==Plot== Navy pilots finagle a four-day leave in San Francisco. They land a posh suite at a hotel where Commander Andy Crewson (Cary Grant), a master of procurement, arranges to populate it with wine, women and song. 

Blonde bombshell Alice Kratzner (Jayne Mansfield) is one of these women, lured to the suite under the false pretense that Crewson has a stash of nylon stockings. Once there, she is naturally attracted to Crewson, but later turns her attention to Lieutenant McCann (Ray Walston), a married man who also is in the process of running for a Congressional seat back home in Massachusetts. If he is elected, McCann can leave the Navy immediately and return to civilian life.
 Leif Erickson), so that Turnbill will vouch for the men with the Navy and also to grease a lucrative job for himself upon leaving the service. Crewson and his cohorts, however, are physically and mentally exhausted from the war and simply want to enjoy a few days away from it.

Suffering from combat stress and confronted with a number of reminders of the horrors of war, Crewson tries to amuse himself by making a play for Turnbills attractive fiancée, Gwinneth Livingston (Suzy Parker). She resists his advances at first, but ultimately throws her engagement ring in Turnbills face. Crewson declares his love for her shortly before he and his mates board a plane leaving San Francisco to return to duty.

==Cast==
 
* Cary Grant as Lieutenant Commander Andy Crewson
* Jayne Mansfield as Alice Kratzner Leif Erickson as Eddie Turnbill
* Suzy Parker as Gwinneth Livingstone
* Ray Walston as Lieutenant McCann
* Larry Blyden as Mississip
* Nathaniel Frey as Chief Petty Officer Ruddle
* Werner Klemperer as Lieutenant Walter Wallace
* Jack Mullaney as Ens. Lewis
* Kathleen Freeman as Nurse Wilinski
* Harry Carey, Jr. as Lt. Chuck Roundtree
* Maudie Prickett as Chief Nurse Richard Deacon as Bill Hotchkiss Frank Nelson as Hotel Manager R.L. Nielson
 

==Reception==
When released in late 1957, Kiss Them for Me was greeted with negative reviews. Critics called the film "vapid" and "ill-advised"; not to mention "no good". When the film didnt regain its production costs, Twentieth Century-Fox appeared to punish cast members, especially Jayne Mansfield, whose career was tossed on the back burner by the studio. Mansfield was resilient, however, and after several more years of starring roles landed on the Top 10 list of Box Office Attractions for 1963.

Kiss Them for Me was also the first film for actor Ray Walston, launching a long career.

==In popular culture== Kiss Them Biloxi to New Orleans in 1967.

==References==
 

At the end of the film,  , an Essex Class aircraft carrier, can be seen launching a Douglas A-1 Skyraider type aircraft. (The Skyraider type was not actually used in WW2.)

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 