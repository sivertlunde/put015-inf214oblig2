Earth (1996 film)
{{Infobox film
| name           = Earth
| image          = Tierra_poster.jpg
| image_size     =
| caption        = Theatrical release poster by Oscar Mariné
| director       = Julio Médem
| producer       = Fernando de Garcillán
| writer         = Julio Médem
| narrator       =
| starring       = Carmelo Gómez
| music          =
| cinematography = Javier Aguirresarobe
| editing        = Iván Aledo
| distributor    =
| released       =  
| runtime        = 125 minutes
| country        = Spain
| language       = Spanish
| budget         =
}} 1996 cinema Spanish film directed by Julio Médem, starring Carmelo Gómez and Emma Suárez. It was entered into the 1996 Cannes Film Festival.   

The story centres on a small rural town whose wine industry is being plagued by grubs in the soil. Ángel (played by Gómez), an exterminator recently released from mental hospital, arrives to deal with the pests and becomes involved with two of the local women.

==Cast==
* Carmelo Gómez - Ángel Bengoetxeo
* Emma Suárez - Ángela
* Karra Elejalde - Patricio Silke - Mari
* Nancho Novo - Julio
* Txema Blasco - Tomás
* Ane Sánchez - Hija de Ángela
* Juan José Suárez - Manuel
* Ricardo Amador - Charly
* César Vea - Miñón (as Cesare Vea)
* Pepe Viyuela - Ulloa
* Alicia Agut - Cristina
* Miguel Palenzuela - Tío de Ángel
* Vicente Haro - Mayor (as Vicente Haro Marón)
* Adelfina Serrano - Concha

==References==
 

==External links==
*  

 

 
 
 
 
 


 
 