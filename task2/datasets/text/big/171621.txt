Quills
 
 
 
{{Infobox film
| name = Quills
| image = Quills poster.JPG
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Philip Kaufman Nick Wechsler
| writer = Doug Wright
| starring = Geoffrey Rush Kate Winslet Joaquin Phoenix Michael Caine
| music = Stephen Warbeck
| cinematography = Rogier Stoffers Peter Boyle
| studio = Industry Entertainment Walrus & Associates
| distributor = Fox Searchlight Pictures
| released =  
| runtime = 124 minutes    
| country = United States United Kingdom Germany
| language = English Latin
| budget = $13.5 million 
| gross = $17,989,227 
}} period film adapted from Obie award-winning play by Doug Wright, who also wrote the original screenplay. Inspired by the life and work of the Marquis de Sade, Quills re-imagines the last years of the Marquis incarceration in the insane asylum at Charenton (asylum)|Charenton. It stars Geoffrey Rush as the Marquis de Sade, Joaquin Phoenix as the Abbé de Coulmier|Abbé du Coulmier, Michael Caine as Dr. Royer-Collard, and Kate Winslet as laundress Madeleine "Maddie" LeClerc.
 Golden Globe. The film was a modest art house success, averaging $27,709 per screen its debut weekend, and eventually grossing $17,989,277 internationally. Cited by historians as factually inaccurate, Quills filmmakers and writers said they were not making a biography of de Sade, but exploring issues such as censorship, pornography, sex, art, mental illness, and religion. It was released with an 18 rating from the British Board of Film Classification due to "strong horror, violence, sex, sexual violence, and nudity". 

== Taglines ==
* There are no bad words… only bad deeds.
* Meet the Marquis de Sade. The pleasure is all his.

== Plot ==
Quills begins during the Reign of Terror, with the incarcerated Marquis de Sade (Geoffrey Rush) penning a story about the libidinous Mademoiselle Renard, an aristocrat who meets the preeminent sadist in her executioner.

Several years later, the Marquis is confined to the asylum at  ) to look in at Charenton and silence the Marquis. Meanwhile the Abbé teaches Madeleine to read and write and resists his growing attraction to her.

Dr. Royer-Collard arrives, informing the Abbé that the Marquis "therapeutic writings" have been distributed for public consumption. Horrified, the Abbé rejects Royer-Collards offers of several archaic "treatments" and asks to speak with the Marquis himself, who promptly swears obedience (winking at Madeleine through a peephole). Royer-Collard takes his leave for the time being and travels to the Panthemont Convent in Paris to retrieve his promised bride, the underage Simone. They are given a run-down chateau by the Emperor, with a handsome young architect, Prioux, on hand for its renovation.

The hasty marriage incites much gossip at the asylum, prompting the Marquis to write a farce to be performed at a public exhibition. The audacious play, titled "The Crimes of Love", is interrupted when the inmate Bouchon molests Madeleine off-stage, prompting her to hit him in the face with an iron. Royer-Collard shuts down the public theater and demands that the Abbé do more to control the Marquis. Infuriated, the Abbé confiscates the Marquis quills and ink, prompting more subversive behavior, including a story written in wine on bedsheets and in blood on clothing. This results in further deprivation, eventually leaving the Marquis naked in an empty cell. One of the maids reveals that Madeleine has been helping the Marquis. Madeleine is whipped on the order of Dr. Royer-Collard until the Abbé stops him by offering himself instead and declaring that she will be sent away. That night she visits his chamber to beg him to reconsider sending her away and confesses her love for him in the process, prompting him to kiss her passionately. He abruptly breaks away at the realization of what he is doing. Madeleine is angry with him the next day and the Abbé is shown as sexually frustrated.

While this is occurring at the asylum, Simone has been violently introduced to the adult world by her husband. She unrepentantly purchases a copy of the Marquis de Sades Justine, seduces Prioux, and the young lovers run off together. She leaves behind a letter explaining her actions and her copy of Justine. Upon finding this, Dr. Royer-Collard seizes on the Marquis as the source of his troubles and embarks upon a quest for revenge.

About to be sent away from Charenton for her role in assisting the Marquis, Madeleine begs a last story from him, which is to be relayed to her through the asylum patients. Bouchon, the inmate at the end of the relay, is excited by the story, breaks out of his cell, and kills Madeleine. The asylum is set afire by the pyromaniac Dauphin and the inmates break out of their cells.

Madeleines body is found by her blind mother and the Abbé in the laundry vat. The Abbé is completely devastated by Madeleines death and Bouchon is captured and imprisoned inside an iron dummy. The Abbé blames the Marquis for Madeleines death and prods him into a fury. The Marquis claims he had been with Madeleine in every way imaginable, only to be told she had died a virgin. The Abbé cuts out the Marquis tongue as punishment for his involvement. In a later scene, the Abbé has sex with the corpse of Madeline in a dream. The Marquis health declines severely, though perverse as ever, he decorates his oubliette with a story, using feces as ink. As the Abbé finishes reading the last rites, he offers the Marquis a crucifix to kiss, which he swallows and chokes on, thus committing suicide.

A year later, the new Abbé du Maupas arrives at Charenton and is given the grand tour. The asylum has been converted into a print shop, with the inmates as its staff. The books being printed are the works of the Marquis de Sade. At the end of the tour, the new Abbé meets his predecessor, who resides in the Marquis old cell. Yearning to write, he begs paper and a quill from the Abbé, who is herded off by Royer-Collard, now overseer of the asylum. However, the peephole opens, and Madeleines mother thrusts paper, quill, and ink through. The Abbé begins to scribble furiously, with the Marquis providing the narration. 

== Cast ==
*  .   
* Kate Winslet as Madeleine "Maddy" LeClerc, the feisty laundress and romantic interest for both the Abbé and the Marquis. In love with the Abbé, who is extremely in love with her but feels restricted, she is fascinated by the Marquis and his intelligence and experience. Screenwriter Doug Wright called Winslet the "patron saint" of the movie for being the first big name to back it,  expressing interest as early as April 1999. 
* Joaquin Phoenix as the Abbé de Coulmier|Abbé du Coulmier, the well-loved administrator at Charenton asylum. A profoundly religious man, he treats his wards with kindness and allows them to express themselves artistically. He is intensely in love with Maddy, though he does not entirely admit it to her or himself, he becomes devastated by her death and eventually this led to his madness. Before settling on Joaquin Phoenix, casting directors considered Jude Law, Guy Pearce, and Billy Crudup for the role. 
* Michael Caine as Antoine-Athanase Royer-Collard|Dr. Royer-Collard, the traditionalist foil for the Abbé who was sent by Emperor Napoléon to silence the Marquis, though he proves as sadistic as the Marquis himself. Kaufman drew comparisons between Royer-Collard and Kenneth Starr, particularly the publication of de Sades works at the Charenton Printing Press and the release of Starrs report online.  lye of the laundry vats.
* Stephen Marcus as Bouchon, the inmate who attempts to rape Madeleine backstage during "The Crimes of Love" and ultimately kills her during the climax of the film.
* Amelia Warner as Simone, Royer-Collards child bride who elopes with architect Prioux.
* Stephen Moyer as Prioux, a promising architect sent by Emperor Napoléon to renovate the Royer-Collard chateau, Prioux falls in love with Simone and runs away with her.
* Jane Menelaus (Rushs real-life spouse) as Renée Pelagie, the Marquis de Sades long-suffering wife.
* Ron Cook as Napoleon|Napoléon I Bonaparte, the Emperor of the French, who ordered the anonymous author of Justine (Sade)|Justine (the Marquis) arrested in 1801. This was Cooks second appearance as Emperor Napoléon, the first being in the Sharpe (TV series)|Sharpe series in 1994.
* Patrick Malahide as Delbené, Napoléons most trusted advisor; is responsible for sending Dr. Royer-Collard to Charenton.
* Elizabeth Berrington as Charlotte, a meddlesome chambermaid who betrays Madeleine to Royer-Collard and eventually becomes his lover and assistant at the Charenton Printing Press.
* Tony Pritchard as Valcour, Charentons prefect, Valcour performs much of the physical work necessary at the asylum.
* Michael Jenn as Cleante, a madman who thinks he is a bird. He stars in "The Crimes of Love" in the Royer-Collard-inspired role of The Libertine and helps pass the Marquis story to Madeleine later in the film.
* Edward Tudor-Pole as Franval, an obsessive-compulsive.

== Production ==
Principal photography began in England on 5 August 1999,  with Oxfordshire, Bedfordshire, and London standing in for early 19th century France.  Academy Award for Best Art Direction#1990s|Oscar-winning production designer Martin Childs (Shakespeare in Love) imagined the primary location of Charenton as an airy, though circuitous place, darkening as Royer-Collard takes over operations. The screenplay specifies the way the inmates rooms link together, which plays a key role in the relay of the Marquis climactic story to Madeleine.    Screenwriter/playwright Doug Wright was a constant presence on set, assisting the actors and producers in interpreting the script and bringing his vision to life.   
 Rising Sun. For Joaquin Phoenixs Abbé, costumers designed special "pleather" clogs to accommodate the actors veganism. In one scene, Rushs Marquis de Sade wears a suit decorated in bloody script, which West described as "challenging" to make. It features actual writings of de Sade and costumers planned exactly where each sentence should go on the fabric. Before production began, West gave Winslet a copy of French painter Léopold Boillys "Woman Ironing" to give her a feel for the character, which Winslet said greatly influenced her performance.   

Casting directors Donna Isaacson and Priscilla John recruited a number of actors from a disabled actors company to play the parts of many of the inmates at Charenton. 

== Music ==
The Quills soundtrack was released by RCA Victor on 21 November 2000 featuring the music of 71st Academy Awards|Oscar-winning composer Stephen Warbeck (Shakespeare in Love). Featuring experimental instrumentation by The Quills Specialist Band  on such instruments as the Serpent (instrument)|serpent, shawm, and Bucket mute|bucket,    most reviewers were intrigued by the unconventional and thematic score. Cinemusic.net reviewer Ryan Keaveney called the album a "macabre masterpiece," with an "addicting and mesmerizing" sound.  Urban Cinephile contributor Brad Green described the album as a "hedonistic pleasure" that "captures the spirit of an incorrigible, perverse genius."  Soundtrack.nets Glenn McClanan disliked the "lack of unifying unified themes and motifs" that may have served each individual scene, but made the film feel "incoherent." 

=== Au Clair de la Lune ===
Though not included on the soundtrack, the opening notes of "Au Clair de la Lune", a traditional French childrens song, recur throughout the film, usually hummed by the Marquis. The song is originally sung by John Hamway during the opening scene of a beheading which was filmed in Oxford. The English translation provides some illumination as to its selection as a theme for the Marquis:
{{Listen
|pos=right
|filename=Au_clair_de_la_lune_mode_do.mid
|title=Au Clair de la Lune
|description=The opening notes to the traditional French childrens song "Au Clair de la Lune" Midi
|play=no
|ogg=no
}} At thy door Im knocking by the pale moonlight 
Lend a pen I pray thee, Ive a word to write 
Guttered is my candle, burns my fire no more 
For the love of heaven, open now the door  
Pierrot cried in answer by the pale moonlight 
"In my bed Im lying, late and chill the night 
Yonder at my neighbours, someone is astir 
Fire is freshly kindled - get a light from her."  
To the neighbours house then by the pale moonlight 
Goes our gentle Lubin to beg a pen to write 
"Who knocks there so softly?" Calls a voice above 
"Open wide your door now, tis the god of love"  
Seek they pen and candle in the pale moonlight 
They can see so little, dark is now the night 
What they find in seeking, that is not revealed  Anonymous }}

;Track listing
# "The Marquis and the Scaffold" – 3:08
# "The Abbe and Madeleine" – 2:19
# "The Convent" – 2:22
# "Plans for a Burial" – 1:18
# "Dream of Madeleine" – 4:42
# "Royer-Collard and Bouchon" – 4:15
# "Aphrodisiac" – 2:59
# "The Last Story" – 7:35
# "The Marquis Cell at Charenton" – 4:38
# "The End: A New Manuscript" – 7:32
# "The Printing Press" – 2:22

== Release ==
=== Box office === premiered in wider release following on 15 December 2000. The film earned $249,383 its opening weekend in nine theaters,  totaling $7,065,332 domestically and $10,923,895 internationally, for a total of $17,989,227.   

=== Home media === Region 1 Region 2 commentary track by screenwriter/playwright Doug Wright and three featurettes: "Marquis on Marquee," "Creating Charenton," and "Dressing the Part." Also included are the theatrical trailer, a television spot, a photo gallery, a music promotional spot, and a feature called "Fact & Film: Historical and Production Information."

== Reaction ==
=== Critical reception ===
Reviews were generally positive, with a 75% "fresh" rating at the review aggregate site Rotten Tomatoes and an average score of 70/100 at Metacritic.  

Elvis Mitchell of The New York Times complimented the "euphoric stylishness" of Kaufmans direction and Geoffrey Rushs "gleeful...flamboyant" performance.  Peter Travers for Rolling Stone wrote about the "exceptional" actors, particularly Geoffrey Rushs "scandalously good" performance as the Marquis, populating a film that is "literate, erotic, and spoiling to be heard."  Stephanie Zacharek of Salon.com enthused over the "delectable and ultimately terrifying fantasy" of Quills, with Rush as "sun king," enriched by a "luminous" supporting cast. 

The film was not without its detractors, including Richard Schickel of Time (magazine)|Time magazine, who decried director Philip Kaufmans approach as "brutally horrific, vulgarly unamusing," creating a film that succeeds only as "soft-gore porn."  Eleanor Ringel Gillespie of the Atlanta Journal-Constitution concurred, finding Quills "shrill, pretentious, sophomoric and often just plain dumb."  Kenneth Turan of the Los Angeles Times dismissed the film as an "overripe contrivance masquerading as high art",  while de Sade biographer Neil Schaeffer in The Guardian criticized the film for historical inaccuracies and for simplifying de Sades complex life (see below). 

=== Accolades === Golden Globes, Best Actor Best Screenplay Douglas Wright). Best Film of 2000. 

== Historical inaccuracy ==
Neil Schaeffer, whose The Marquis de Sade: A Life    was used by Director Philip Kaufman as reference,  in a review published in The Guardian, criticized the film for historical inaccuracies and for simplifying de Sades complex life.   

Schaeffer especially criticized the depiction of the de Sade as a "martyr to the oppression and censorship of church and state" and the films sacrificing facts "to a surreal and didactic conclusion that has no connection with the truth, and is probably overwrought even as a twist of a fictional plot", namely that "the seemingly good people are all bad underneath, are all hypocrites, while the seemingly bad person, de Sade, probably has some redeeming qualities". 

Schaeffer detailed a number of disparities between fact and film:

Schaeffer relates that de Sades initial incarceration "had nothing to do with his writing" but with sexual scandals involving servants, prostitutes and his sister-in-law. He also criticized the opening scenes implication that the reign of terror caused the "sanguinary streak" of de Sades writing, when "his bloodiest and best work, 120 Days of Sodom, was written in the Bastille - obviously before the revolution" and not at Charenton, as suggested by the film. In contrast to the film, the historical de Sade was "not at the height of his literary career nor of his literary powers" while at Charenton, nor did he cut the "tall, trim figure of the Australian actor Geoffrey Rush" but was of middling height and, at the time, of a "considerable, even a grotesque, obesity". 
 real Dr. Royer-Collard had any influence at Charenton.    

Schaeffer criticized also the films treatment of de Sades personal relations regarding his wife (who had formally separated from him after the revolution), the chambermaid (who did not serve as a liaison to a publisher but with whom he had a sexual relationship from her early teens until shortly before his death) and his "companion of many years", who had a room at Charenton (and actually smuggled out the manuscripts) but is ignored by the film. Furthermore, "De Sades hideous death in the movie is nothing like the truth, for he died in his sleep, in his 74th year, as peacefully as any good Christian".  

Schaeffer argues that the main point of de Sades life and writing was not, "as movie-makers and reviewers alike seem to think   to oppose censorship" but "to push the limits - sexual, spiritual, and political - as a means of feeling out the limits of his times and of his own mind." Schaeffer criticized that the film "simplifies de Sade into a modern "victim" and over-emphasises his potential as a focus for liberal-political meanings when, in fact, his life and perhaps his literary intentions - if you think of him as a satirist - can be seen as an object lesson, warning against the excesses of cultural relativism and nihilism; a very modern lesson, it would seem." 

Schaeffer advised the viewer to distinguish between de Sade and the protagonist of the film: "To see if Quills is valid in its own terms, let the viewer imagine it is about someone else, let us say the Marquis de Newcastle, and that the scene is Bedlam and then see if the movie makes any sense." 

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 