Let Katie Do It
{{Infobox film
| name           = Let Katie Do It
| image          =
| caption        = Sidney Franklin Millard Webb(assistant director)
| producer       = D. W. Griffith
| writer         = Granville Warwick(Griffith alias) Bernard McConville(scenario) Jane Grey Tully Marshall
| music          = William Furst
| cinematography = Frank B. Good
| editing        =
| distributor    = Triangle Film Corporation
| released       = January 6, 1916
| runtime        = 50 minutes
| country        = USA
| language       = Silent..English titles
}} Chester and Sidney Franklin and was produced by D. W. Griffiths Fine Arts company. It is also known as Let Katy Do It.  

A copy is preserved in the Library of Congress collection and UCLA Film & TV.  

==Cast== Jane Grey - Katie Standish
*Tully Marshall - Oliver Putnam
*Luray Huntley - Priscilla Standish (*Luray Huntley, wife of Walter Long) Charles West - Caleb Adams Ralph Lewis - Uncle Dan Standish
*George C. Pearce - Father Standish(*George Pearce) Walter Long - Pedro Garcia Charles Gorman - Carlos
*Violet Radcliffe - Adams Child
*Georgie Stone - Adams Child
*Baby Carmen De Rue - Adams Child (*as Carmen De Rue)
*Francis Carpenter - Adams Child
*Ninon Fovieri - Adams Child
*Lloyd Perl - Adams Child (*as Lloyd Pearl)
*Beulah Burns - Adams Child

uncredited
*George Beranger - Accident Witness
*Virginia Lee Corbin - Child

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 