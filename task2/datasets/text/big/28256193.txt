Puncture (film)
{{Infobox film
| name           = Puncture
| image          = Puncture Poster.jpg
| caption        = Theatrical release poster
| director       = Adam Kassen Mark Kassen
| producer       = Adam Kassen Mark Kassen Jordan Foley
| writer         = Chris Lopata
| story          = Ela Thier  Paul Danziger Chris Evans Mark Kassen Vinessa Shaw Brett Cullen Michael Biehn Marshall Bell
| music          = Ryan Ross Smith
| cinematography = Helge Gerull
| editing        = Chip Smith
| studio         = Millennium Entertainment
| distributor    = Millennium Entertainment
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $68,945   
}}
 Chris Evans, Michael David Mike Weiss and Paul Danziger. It was chosen as one of the spotlight films for the 2011 Tribeca Film Festival, premiering on April 21, 2011 in New York City. 

== Cast == Chris Evans Michael David Mike Weiss
* Mark Kassen as Paul Danziger
* Marshall Bell as Jeffrey Dancort
* Michael Biehn as Red
* Vinessa Shaw as Vicky
* Jesse L. Martin as Daryl King
* Brett Cullen as Nathaniel Price Kate Burton as Senator OReilly
* Roxanna Hope as Sylvia
* Jennifer Blanc as Stephany
* W. Mark Lanier as himself

== Plot == pricked by a contaminated needle. As Weiss and Danziger dig deeper into the case, a healthcare and pharmaceutical conspiracy teeters on exposure and heavyweight attorneys move in on the defense. Out of their league but invested in their own gain, the mounting pressure of the case pushes the two underdog lawyers and their business to the breaking point. 

== Production ==
The original story was written by Danziger. Filming began on February 10, 2010 in Texas.  The film was directed by Adam Kassen and Mark Kassen. Adam Kassen was quoted as saying, "From the moment we heard about this story, we connected to what it says about the current state of our medical industry and the flawed hero that tries to fix it." 

== Release ==
After the film premiered at the 2011 Tribeca Film Festival, Millennium Films acquired the distribution rights.  It had a limited release on September 23, 2011, and played in five theaters.  The total domestic gross was $68,945. 

Puncture was released on DVD and Blu-ray on January 3, 2012. 

== Reception ==
  rated it 54/100 based on 17 reviews.  Roger Ebert rated it 3/4 stars and wrote that Evans performance upstages the issues raised in the film.  Kirk Honeycutt of The Hollywood Reporter wrote, "The film is chock-a-block with extraordinary performances and no one will fault the filmmaking either. This is a well-made movie, make no mistake. It just suffers from a dysfunctional hero."  Ronnie Scheib of Variety (magazine)|Variety wrote, "Though conceptually intriguing, the mix of downward drug spiral with uphill struggle for good never really coalesces."  Jeannette Catsoulis of The New York Times wrote, "Notable at least in part for its fumbled potential, this health-care-industry melodrama possesses all the right ingredients: an idealistic young lawyer, a corrupt corporate villain and a sympathetic victim. It just fails to assemble them into a compelling whole." 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 