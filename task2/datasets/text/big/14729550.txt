Adam's Rib (1923 film)
 
{{Infobox film
| name           = Adams Rib
| image          = Adams Rib - 1923.jpg
| caption        = Lobby card for Adams Rib (1923)
| director       = Cecil B. DeMille
| producer       = Cecil B. DeMille
| writer         = Jeanie MacPherson
| starring       = Milton Sills
| cinematography = L. Guy Wilky Alvin Wyckoff
| editing        = Anne Bauchens
| studio         = Famous Players-Lasky Corporation
| distributor    = Paramount Pictures 
| released       =   reels (9,526 feet)
| country        = United States Silent English intertitles
| budget         = $400,000
}} silent drama film directed by Cecil B. DeMille.   

==Cast==
* Milton Sills as Michael Ramsay
* Elliott Dexter as Prof. Nathan Reade
* Theodore Kosloff as Monsieur Joromir, King of Moravia
* Anna Q. Nilsson as Mrs. Michael Ramsay
* Pauline Garon as Mathilda Ramsay
* Julia Faye as The Mischievous One
* Clarence Geldart as James Kilkenna
* Robert Brower as Hugo Kermaier
* Forrest Robinson as Kramer
* Gino Corrado as Lt. Braschek
* Wedgwood Nowell as Ministers Secrety
* Clarence Burton as Cave Man George Field as Minister to Moravia William Boyd as (uncredited)
* Robert St. Angelo as Extra (uncredited)

==Preservation status==
A print of the film exists in the George Eastman House film archive.    

==References==
 

==External links==
 
* 
* 
* 

 

 
 
 
 
 
 
 
 


 