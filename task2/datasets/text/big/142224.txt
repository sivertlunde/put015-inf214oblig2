A.I. Artificial Intelligence
 
 
{{Infobox film
| name           = A.I. Artificial Intelligence
| image          = AI Poster.jpg
| caption        = Theatrical release poster
| director       = Steven Spielberg Kathleen Kennedy Steven Spielberg Bonnie Curtis
| screenplay     = Steven Spielberg Ian Watson
| based on       =  
| narrator       = Ben Kingsley
| starring       = Haley Joel Osment Jude Law Frances OConnor Brendan Gleeson William Hurt 
| music          = John Williams
| cinematography = Janusz Kamiński Michael Kahn Stanley Kubrick Productions
| distributor    = Warner Bros. Pictures  (United States theatrical and International home video releases)  DreamWorks Pictures  (International theatrical and United States home video releases) 
| released       =  
| runtime        = 146 minutes   
| country        = United States
| language       = English
| budget         = $100 million    
| gross          = $235.9 million 
}}
 android uniquely programmed with the ability to love.
 Ian Watson, and Sara Maitland. The film languished in development hell for years, partly because Kubrick felt computer-generated imagery was not advanced enough to create the David character, whom he believed no child actor would believably portray. In 1995, Kubrick handed A.I. to Spielberg, but the film did not gain momentum until Kubricks death in 1999. Spielberg remained close to Watsons film treatment for the screenplay. The film was greeted with generally favorable reviews from critics and grossed approximately $235 million. A small credit appears after the end credits, which reads "For Stanley Kubrick."

==Plot== flooded coastlines, prototype model imprinting protocol, which irreversibly causes David to have an enduring childlike love for her. He is also befriended by Teddy (Jack Angel), a robotic teddy bear, who takes it upon himself to care for Davids well-being.

A cure is found for Martin and he is brought home; as he recovers, it becomes clear he doesnt want a sibling and soon makes moves to cause issues for David. First he attempts to make Teddy choose who he likes more. He then makes David promise to do something and in return Martin will tell Monica that he loves his new "brother", making her love him more. The promise David makes is to go to Monica in the middle of the night and cut off a lock of her hair, which of course upsets the parents (not just through the act itself, but David isnt supposed to go into their room at night). At a pool party, one of Martins friends activates Davids self-protection programming by poking him with a knife. David grabs Martin and they both fall into the pool, where the heavy David sinks to the bottom while still clinging to Martin. Martin is saved from drowning, but Henry mistakes Davids fear during the pool incident as hate for Martin. Henry persuades Monica to return David to Cybertronics, where he will be destroyed. However, Monica cannot bring herself to do this, and instead tearfully abandons David in the forest (with Teddy) to hide as an unregistered Mecha. David is captured for an anti-Mecha Flesh Fair, an event where obsolete and unlicensed Mecha are destroyed in front of cheering crowds. David is nearly killed, but the crowd is swayed by his fear (since Mecha dont plea for their lives) into believing he is real and he escapes with Gigolo Joe (Law), a male prostitute Mecha on the run after being framed for murder.
 Blue Fairy, whom David remembers from the story The Adventures of Pinocchio. He is convinced that the Blue Fairy will transform him into a human boy, allowing Monica to love him and take him home. Joe and David make their way to Rouge City. Information from a holographic answer engine called "Dr. Know" (Robin Williams) eventually leads them to the top of Rockefeller Center in partially flooded Manhattan. There, David meets an identical copy of himself, and believing he is not special, becomes filled with anger and destroys the copy Mecha. David then meets his human creator, Professor Allen Hobby (Hurt), who excitedly tells David that finding him was a test, which has demonstrated the reality of his love and desire. However, David learns that he is the namesake and image of Professor Hobbys deceased son. It also becomes clear that many copies of David, along with female versions, are already being manufactured. David sadly realizes he is not unique. A disheartened David attempts to commit suicide by falling from a ledge into the ocean, but Joe rescues him with their stolen amphibicopter. David tells Joe he saw the Blue Fairy underwater, and wants to go down to her. At that moment, Joe is captured by the authorities with the use of an electromagnet, but sets the amphibicopter on submerge. David and Teddy take it to the fairy, which turns out to be a statue from a submerged attraction at Coney Island. Teddy and David become trapped when the Wonder Wheel falls on their vehicle. Believing the Blue Fairy to be real, David asks to be turned into a real boy, repeating his wish without an end, until the ocean freezes in another ice age and his internal power source drains away.

Two thousand years later, humans are extinct and Manhattan is buried under several hundred feet of glacial ice. The now highly advanced Mecha have evolved into an intelligent, silicon-based form. On their project to studying humans — believing it was the key to understanding the meaning of existence — they find David and Teddy and discover they are original Mecha who knew living humans, making the pair very special and unique. David is revived and walks to the frozen Blue Fairy statue, which cracks and collapses as he touches it. Having downloaded and comprehended his memories, the advanced Mecha use these to reconstruct the Swinton home and explain to David via an interactive image of the Blue Fairy (Streep) that it is impossible to make him human. However, at Davids insistence, they recreate Monica from DNA in the lock of her hair which Teddy had saved for unknown reasons. One of the futuristic Mecha tells David that the clone can only live for a single day, and the process cannot be repeated. But David keeps insisting, so they fast forward the time to the next morning, and David spends the happiest day of his life with Monica and Teddy. Monica tells David that she loves him, and has always loved him, as she drifts to sleep for the last time. David lies down next to her, closes his eyes and goes "to that place where dreams are born". Teddy enters the room, climbs onto the bed, and watches as David and Monica lie peacefully together.

==Cast== Mecha created by Cybertronics and programmed with the ability to love. He is adopted by Henry and Monica Swinton, but a sibling rivalry ensues once their son Martin comes out of suspended animation. Osment was Spielbergs first and only choice for the role. Osment avoided blinking his eyes to perfectly portray the character, and "programmed" himself with good posture for realism. Haley Joel Osment, A Portrait of David, 2001, Warner Home Video; DreamWorks 
* Jude Law as Gigolo Joe, a male prostitute Mecha programmed with the ability to mimic love, like David, but in a different sense. To prepare for the role, Law studied the acting of Fred Astaire and Gene Kelly. 
* Frances OConnor as Monica Swinton, Davids adopted mother who reads him The Adventures of Pinocchio. She is first displeased to have David in her home but soon starts loving him.
* Sam Robards as Henry Swinton, an employee at Cybertronics, husband of Monica and Davids adopted father. Henry eventually sees David as dangerous to his family.
* Jake Thomas as Martin Swinton, Henry and Monicas first son, who was placed in suspended animation and Davids adopted brother. When Martin comes back, he convinces David to cut off a lock of Monicas hair.
* William Hurt as Professor Allen Hobby, responsible for shepherding the creation of David. He resides in New York City, which is crippled by the effects of global warming but still functioning as Cybertronics headquarters. David is modeled after Hobbys own son, also named David, who died at a young age.
* Brendan Gleeson as Lord Johnson-Johnson, the owner and master of ceremonies of the Flesh Fair.
* Ashley Scott as Gigolo Jane
;Voices
* Jack Angel as Teddy, Davids robotic teddy bear.
* Ben Kingsley as the narrator and the leader of the futuristic Mecha. He also appears briefly as one of the technicians who repairs David after he eats spinach.
* Robin Williams as Dr. Know, a holographic answer engine. (Cameo)
* Meryl Streep as The Blue Fairy. (Cameo)
* Chris Rock as a Mecha killed at the Flesh Fair. (Cameo)

==Production==
===Development=== Ian Watson was hired as the new writer in March 1990. Aldiss later remarked, "Not only did the bastard fire me, he hired my enemy   instead." Kubrick handed Watson The Adventures of Pinocchio for inspiration, calling A.I. "a picaresque robot version of Pinocchio".  
 GI Mecha, kiddie market." Jurassic Park (with its innovative use of computer-generated imagery), it was announced in November 1993 that production would begin in 1994.  Dennis Muren and Ned Gorman, who worked on Jurassic Park, became visual effects supervisors,  but Kubrick was displeased with their previsualization, and with the expense of hiring Industrial Light & Magic. 

  }}

===Pre-Production===
In early 1994, the film was in pre-production with Christopher "Fangorn" Baker as concept artist, and Sara Maitland assisting on the story, which gave it "a feminist fairy-tale focus".    Maitland said that Kubrick never referred to the film as A.I., but as Pinocchio.    Chris Cunningham became the new visual effects supervisor. Some of his unproduced work for A.I. can be seen on the DVD, The Work of Director Chris Cunningham.  Aside from considering computer animation, Kubrick also had Joseph Mazzello do a screen test for the lead role.  Cunningham helped assemble a series of "little robot-type humans" for the David character. "We tried to construct a little boy with a movable rubber face to see whether we could make it look appealing," producer Jan Harlan reflected. "But it was a total failure, it looked awful." Hans Moravec was brought in as a technical consultant.  Kathleen Kennedy, Harry Potter Minority Report Memoirs of a Geisha.     When he decided to fast track A.I., Spielberg brought Chris Baker back as concept artist. 
===Filming=== Spruce Goose Dome in Long Beach, south LA. 
The Swinton house was constructed on Stage 16, while Stage 20 was used for Rouge City and other sets.   Spielberg copied Kubricks obsessively secretive approach to filmmaking by refusing to give the complete script to cast and crew, banning press from the set, and making actors sign confidentiality agreements. Social robotics expert Cynthia Breazeal served as technical consultant during production.     Haley Joel Osment and Jude Law applied prosthetic makeup daily in an attempt to look shinier and robotic-like.  Costume designer Bob Ringwood (Batman (1989 film)|Batman, Troy (film)|Troy) studied pedestrians on the Las Vegas Strip for his influence on the Rouge City extras.  Spielberg found post production on A.I. difficult because he was simultaneously preparing to shoot Minority Report. 

==Soundtrack==
  score was composed by John Williams and featured singers Lara Fabian on two songs and Josh Groban on one. Ministry appears in the film playing the song "What About Us?" (but the song does not appear on the official soundtrack album).

==Release==

===Marketing=== The Beast Topie (Thuc) Xbox video game console that followed the storyline of The Beast, but they went undeveloped. To avoid audiences mistaking A.I. for a family film, no action figures were created, although Hasbro released a talking Teddy following the films release in June 2001. 

In November 2000, during production, a video-only webcam (dubbed the "Bagel Cam") was placed in the craft services truck on the films set at the Queen Mary Dome in Long Beach, California. Steven Spielberg, producer Kathleen Kennedy and various other production personnel visited the camera and interacted with fans over the course of three days.  

A.I. had its premiere at the Venice Film Festival in 2001. 

===Box office===
The film opened in 3,242 theaters in the United States on June 29, 2001, earning $29,352,630 during its opening weekend. A.I went on to gross $78.62 million in US totals as well as $157.31 million in foreign countries, coming to a worldwide total of $235.93 million. 

===Critical response===
The film received generally positive reviews. Based on 190 reviews collected by Rotten Tomatoes, 73% of the critics gave the film positive notices with a score of 6.6 out of 10. The website described the critical consensus perceiving the film as "a curious, not always seamless, amalgamation of Kubricks chilly bleakness and Spielbergs warm-hearted optimism.   is, in a word, fascinating."  By comparison, Metacritic collected an average score of 65, based on 32 reviews, which is considered favorable. 

Producer Jan Harlan stated that Kubrick "would have applauded" the final film, while Kubricks widow   heavily praised Spielbergs direction, as well as the cast and visual effects.    compared A.I. to Solaris (1972 film)|Solaris (1972), and praised both "Kubrick for proposing that Spielberg direct the project and Spielberg for doing his utmost to respect Kubricks intentions while making it a profoundly personal work."  Film critic Armond White, of the New York Press, praised the film noting that "each part of David’s journey through carnal and sexual universes into the final eschatological devastation becomes as profoundly philosophical and contemplative as anything by cinema’s most thoughtful, speculative artists&nbsp;– Frank Borzage|Borzage, Yasujirō Ozu|Ozu, Jacques Demy|Demy, Andrei Tarkovsky|Tarkovsky."  Filmmaker Billy Wilder hailed A.I. as "the most underrated film of the past few years."   When British filmmaker Ken Russell saw the film, he wept during the ending. 

Mick LaSalle gave a largely negative review. "A.I. exhibits all its creators bad traits and none of the good. So we end up with the structureless, meandering, slow-motion endlessness of Kubrick combined with the fuzzy, cuddly mindlessness of Spielberg." Dubbing it Spielbergs "first boring movie", LaSalle also believed the robots at the end of the film were aliens, and compared Gigolo Joe to the "useless" Jar Jar Binks, yet praised Robin Williams for his portrayal of a futuristic Albert Einstein.   Peter Travers gave a mixed review, concluding "Spielberg cannot live up to Kubricks darker side of the future." But he still put the film on his top ten list that year for best movies.  David Denby in The New Yorker criticized A.I. for not adhering closely to his concept of the Pinocchio character. Spielberg responded to some of the criticisms of the film, stating that many of the "so called sentimental" elements of A.I., including the ending, were in fact Kubricks and the darker elements were his own.  However, Sara Maitland, who worked on the project with Kubrick in the 1990s, claimed that one of the reasons Kubrick never started production on A.I. was because he had a hard time making the ending work.  James Berardinelli found the film "consistently involving, with moments of near-brilliance, but far from a masterpiece. In fact, as the long-awaited collaboration of Kubrick and Spielberg, it ranks as something of a disappointment." Of the films highly debated finale, he claimed, "There is no doubt that the concluding 30 minutes are all Spielberg; the outstanding question is where Kubricks vision left off and Spielbergs began." 
 Ian Watson aliens (whereas they were robots of the future who had evolved themselves from the robots in the earlier part of the film) and also thinking that the final 20 minutes were a sentimental addition by Spielberg, whereas those scenes were exactly what I wrote for Stanley and exactly what he wanted, filmed faithfully by Spielberg."  

In 2002, Spielberg told film critic Joe Leydon that "People pretend to think they know Stanley Kubrick, and think they know me, when most of them dont know either of us". "And whats really funny about that is, all the parts of A.I. that people assume were Stanleys were mine. And all the parts of A.I. that people accuse me of sweetening and softening and sentimentalizing were all Stanleys. The teddy bear was Stanleys. The whole last 20 minutes of the movie was completely Stanleys. The whole first 35, 40 minutes of the film&nbsp;– all the stuff in the house&nbsp;– was word for word, from Stanleys screenplay. This was Stanleys vision." "Eighty percent of the critics got it all mixed up. But I could see why. Because, obviously, Ive done a lot of movies where people have cried and have been sentimental. And Ive been accused of sentimentalizing hard-core material. But in fact it was Stanley who did the sweetest parts of A.I., not me. Im the guy who did the dark center of the movie, with the Flesh Fair and everything else. Thats why he wanted me to make the movie in the first place. He said, This is much closer to your sensibilities than my own.  {{cite news |last=Leydon |first=Joe |authorlink=Joe Leydon  |url=http://www.movingpictureshow.com/dialogues/mpsSpielbergCruise.html |title=Minority Report looks at the day after tomorrow&nbsp;-- and is relevant to today  
 |work=Moving Picture Show |date=2002-06-20 |accessdate=2009-04-29}} 

Upon rewatching the film many years after its release, BBC film critic Mark Kermode apologized to Spielberg in an interview in January 2013 for "getting it wrong" on the film when he first viewed it in 2001. He now believes the film to be Spielbergs "enduring masterpiece". 

===Accolades=== John Williams Best Original for his Performance by Best Science Fiction Film and for its DVD release. Frances OConnor and Spielberg (as director) were also nominated. 

American Film Institute lists
*AFIs 100 Years of Film Scores&nbsp;– Nominated 
*AFIs 10 Top 10&nbsp;– Nominated Science Fiction Film 

==References==
 

==Further reading==
* 

==External links==
 
*   
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 