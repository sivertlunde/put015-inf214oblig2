The Warped Ones
{{Infobox Film
| name           = The Warped Ones
| image          = The Warped Ones poster.jpg
| alt            = A close-up of a man screaming and another man and woman obscured by text. A medium shot of a woman casually looking over her shoulder is overlaid.
| caption        = Original theatrical poster
| director       = Koreyoshi Kurahara Takeshi Yamamoto
| writer         = Nobuo Yamada
| starring       = Tamio Kawachi Eiji Go Yuko Chishiro Noriko Matsumoto
| music          = Toshiro Mayuzumi
| cinematography = Yoshio Mamiya
| editing        = Akira Suzuki
| distributor    = Nikkatsu
| released       =  
| runtime        = 76 minutes
| country        = Japan Japanese
| budget         = 
}}
  is a 1960 Japanese Sun Tribe film directed by Koreyoshi Kurahara and starring Tamio Kawachi, Eiji Go, Yuko Chishiro and Noriko Matsumoto. It was produced and distributed by the Nikkatsu Company. The story concerns the young hoodlum Akira, his friends, their transgressions and specifically their revenge on the couple that got him sent to jail, a reporter and his fiancée, by means of assault, vehicular and sexual. When the fiancée finds herself pregnant by Akira she enlists his help with her finance who has become distant since the attack.
 Black Sun (1964), featuring many of the same cast, crew and characters, with the addition of acclaimed drummer Max Roach to the soundtrack. Audubon Films released The Warped Ones in the United States in 1963 where it was marketed as a sexploitation film.

==Plot== prostitute girlfriend Yuki (Yuko Chishiro) are arrested when they are spotted fleecing foreigners in a jazz club by a reporter named Kashiwagi (Hiroyuki Nagato). In jail, Akira meets Masaru (Eiji Go) and on their release they and Yuki resume criminal activities. They spot Kashiwagi and his artist fiancée, Fumiko (Noriko Matsumoto), hit him with a stolen car and kidnap her. They take her to a remote beach where Akira rapes her while Masaru and Yuki fornicate in the ocean.

Soon after, the three rent an apartment with money earned from fencing the stolen car. Masaru and Yuki commit to starting a family, while he joins a yakuza gang, to the derision of Akira. Fumiko tracks Akira down and informs him that she is pregnant. Kashiwagi has become distant and haughty and she pleads with Akira for help. Akira arranges for Yuki to seduce Kashiwagi so that the couple might again be on equal terms. Masaru is killed by a rival yakuza. Yuki discovers that she too is pregnant but without Masarus support she resolves to get an abortion and resume her prostitution career. Akira and Yuki meet Kashiwagi and Fumiko by chance at an abortion clinic where Akira reveals that each woman was impregnated by the other man, to the amusement of the former couple and befuddlement of the latter. 

==Production==
The Nikkatsu Company made three popular Sun Tribe films in 1956, a genre based on a contemporary youth subculture whose interests revolved around beach life, jazz music and their progressive attitudes towards sex. The films met with moral public outcries and a fourth production was shut down at the behest of Eirin (The Motion Picture Code of Ethics Committee). {{cite book
| last = Schilling
| first = Mark
| title = No Borders, No Limits: Nikkatsu Action Cinema
| publisher = FAB Press
| year = 2007
| isbn = 978-1-903254-43-1
| url = http://www.fabpress.com/vsearch.php?CO=FAB080
| pages = 30–32
}}  However, the genre later experienced a resurgence which included The Warped Ones. {{cite web
| title = Velvet Hustlers & Weird Lovemakers: Japanese Sixties Action Films
| publisher = American Cinematheque
|date=April 2007
| url = http://www.americancinematheque.com/archive1999/2008/Egyptian/Japanese_60s_Films.htm
| accessdate = 2009-05-30
}}   The film marked director Koreyoshi Kuraharas first collaboration with screenwriter Nobuo Yamada. They reused a many elements of Kuraharas earlier Sun Tribe film The Time of Youth (1959), including abortion, a near fatality via an opened gas cock and a criminal act near water, an explosion beside a stream in the former and the rape on the beach in the latter. {{cite web
| last = Palevsky
| first = Nick
| title = Koreyoshi Kurahara, Part II: The Warped Ones and Its Antecedents
| publisher = The Auteurs
|date=January 2009
| url = http://www.theauteurs.com/notebook/posts/418
| accessdate = 2009-06-08
}} 

Nikkatsu was promoting lead actor Tamio Kawachi as one of its Bad Boy Trio, along with Akira Kobayashi and Tadao Sawamoto. {{cite book
| last = Schilling
| first = Mark
| title = No Borders, No Limits: Nikkatsu Action Cinema
| publisher = FAB Press
| year = 2007
| isbn = 978-1-903254-43-1
| url = http://www.fabpress.com/vsearch.php?CO=FAB080
| pages = 108
}}  Kurahara asked him to think of his character as a "hungry lion roaring at the sun." {{cite web
| title =  The Warped Ones aka: Kyonetsu no kisetsu 
| publisher = Fantastic Fest
| year = 2007
| url = http://fantasticfest.bside.com/2007/?mediaTab=filmDetails&_view=_filmdetails&filmId=31556608
| accessdate = 2009-06-02
}}  He turned in what writer Mark Schilling described as his most unusual, and one of his best, performances of the period.  Supporting actress Noriko Matsumoto came to the film as a relative unknown. Hiroyuki Nagato had starred in The Time of Youth.  Eiji Go was the younger brother of future Diamond Line star Joe Shishido. {{cite book
| last = Schilling
| first = Mark
| title = No Borders, No Limits: Nikkatsu Action Cinema
| publisher = FAB Press
| year = 2007
| isbn = 978-1-903254-43-1
| url = http://www.fabpress.com/vsearch.php?CO=FAB080
| pages = 75–81
}}  The film was completed on August 18, 1960. {{cite web
| script-title=ja:狂熱の季節 (きょうねつのきせつ)
| publisher = Nikkatsu
| url = http://www.nikkatsu.com/movie/detail.html?mid=20475
| language = Japanese
| accessdate = 2007-07-20
}} 

==Style==
As writer Mark Schilling put it, "the soundtrack drives the action," {{cite web
| title =  Mark Schilling on "Nikkatsu Action"  Japan Society, New York
| year = 2008
| url = http://www.japansociety.org/mark_schilling_on_nikkatsu_action
| accessdate = 2009-06-05 editor Akira freeze frames narcissists and sensationalistic and it contains much incident in its short run time.  The overall style is matched to the characters verve and the storys frantic pace.   
 tenements in which the youths reside are depicted as inhospitable and sterile. Lacking education, proper role models and moral codes, critic Bryan Hartzheim posited, crime and base pleasures are their most open recourse. They seem aware of the injustices in their environments and rail against society at large. However, Akira is illustrated as being capable of innocent pleasure, particularly in one fleeting scene in which he and his black friend Gil (Chico Rolands), whom he views as a fellow Stigma (sociological theory)|outcast, frolic in the ocean. 

==Reception==
  theatrical poster employed simple artwork and a suggestive tagline to promote the film as being exploitative, contrary to the films actual content. ]]

The Warped Ones was originally released in Japan by the Nikkatsu Company on September 3, 1960. {{cite web
| script-title=ja:狂熱の季節
| publisher = Japanese Movie Database
| url = http://www.jmdb.ne.jp/1960/cj004110.htm
| language = Japanese
| accessdate = 2009-05-30 The Burmese Harp (1956) and were to be distributed by Kanji or sold to other distribution companies. {{cite web
| title = Kanji to Distribute 10 Japanese Films 
| work = 19610717 Boxoffice (magazine)|Boxoffice/ July 17, 1961
| publisher = Issuu
| url = http://issuu.com/boxoffice/docs/boxoffice_071761/12
| accessdate = 2009-07-20 dubbed version of The Warped Ones was then released in the United States on December 18, 1963, by Radley Metzgers Sexploitation film|sexploitation-centric Audubon Films, initially as the The Weird Lovemakers,  {{cite web
| last = Lucas
| first = Tim
| authorlink = Tim Lucas
| title = An Artful Penetration of THE WEIRD LOVEMAKERS 
| publisher = Video WatchBlog
|date=July 2007
| url = http://videowatchdog.blogspot.com/2007/07/artful-penetration-of-weird-lovemakers.html
| accessdate = 2009-05-30
}}  {{cite book
| last = Krafsur
| first = Richard P.
|author2=American Film Institute 
| title = The American Film Institute Catalog of Motion Pictures Produced in the United States: Feature Films, 1961–1970
| publisher = University of California Press
| year = 1997
| isbn = 978-0-520-20970-1
| url = http://www.ucpress.edu/books/pages/8611.php
| pages = 1204
}}  then The Warped Ones became the more common title. {{cite web
| last = Hartzheim
| first = Bryan
| title = The Warped Ones
| publisher = Midnight Eye
|date=September 2008
| url = http://www.midnighteye.com/reviews/the-warped-ones.shtml
| accessdate = 2009-05-30
}}  It was marketed as an American film, and misleadingly implied to contain sexually explicit material, in order to appeal to a wider audience. {{cite web
| title = Design
| publisher = X-Rated Collection
| url = http://www.xratedcollection.com/adult-movie-posters-design.htm
| accessdate = 2009-06-23
}} 

The original film resurfaced some four decades later at the 2005  . Mark Schilling curated the retrospective in order to expose international audiences to 1960s Nikkatsu Action films which, aside from the films of Seijun Suzuki, remained predominately unseen outside of Japan. {{cite book
| last = Schilling
| first = Mark
| title = No Borders, No Limits: Nikkatsu Action Cinema
| publisher = FAB Press
| year = 2007
| isbn = 978-1-903254-43-1
| url = http://www.fabpress.com/vsearch.php?CO=FAB080
| pages = 5–10
}}  Schilling originally titled the film Season of Heat—a literal translation of the Japanese title—but it was retitled The Warped Ones for subsequent incarnations of the retrospective, which included runs in Austin and New York.  {{cite web
| title = No Borders, No Limits: 1960s Nikkatsu Action Cinema
| publisher = Japan Society, New York
|date=December 2007
| url = http://www.japansociety.org/events/event_detail.cfm?id_event=828148013&id_performance=1449785411
| archiveurl = http://web.archive.org/web/20071218234027/http://www.japansociety.org/events/event_detail.cfm?id_event=828148013&id_performance=1449785411
| archivedate = 2007-12-18
| accessdate = 2009-05-30
}}  {{cite web
| title = No Borders, No Limits: 1960s Nikkatsu Action Cinema Retrospective
| publisher = Fantastic Fest
|date=September 2007
| url = http://fantasticfest.blogspot.com/2007/09/nikkatsu-action-retrospective.html
| archiveurl = http://web.archive.org/web/20071112225050/http://fantasticfest.blogspot.com/2007/09/nikkatsu-action-retrospective.html
| archivedate = 2007-11-12
| accessdate = 2009-05-30
}}  It also appeared in a 12-film retrospective of Koreyoshi Kuraharas Nikkatsu films at the 2008 Tokyo Filmex International Film Festival in Japan. It was screened with English Subtitle (captioning)|subtitles. {{cite web
| title = National Film Center Presents Special Program: KURAHARA Yoreyoshi Retrospectiv 
| publisher = Tokyo Filmex
| year = 2008
| url = http://www.filmex.net/2008/special_pk-e.htm
| accessdate = 2009-06-04
}} 

Critics have most often compared the film to landmark youth films Breathless (1960 film)|Breathless (also 1960)—released in France five months earlier—directed by Jean-Luc Godard and Nicholas Rays Rebel Without a Cause (1955), although, Bryan Hartzheim found it takes its youths more seriously and with less sympathy. He stated, "  a wrecking ball to what can be considered the indulgencies of the   genre, an exhibition of the horrors of uninhibited youth taken to its carnal extremes and matched by a visual accompaniment akin to the abstract and improvised style of a Miles Davis score."  Tim Lucas of Video Watchdog magazine called the film "an important rediscovery on many fronts... one of the great jazz films, and possibly the best illustration the cinema has ever given us of the jazz buff. Its the only film Ive ever seen that makes jazz seem scarier than the darkest heavy metal, that makes jazz seem dangerous."  For TokyoScope: The Japanese Cult Film Companion, Alvin Lu commended the score as "stunning" and Kawachis performance as "ferocious, the very incarnation of the kind of social chaos that could be engendered by too much exposure to jazz, Coke, and hot dogs." {{cite book
| last = Macias
| first = Patrick
| authorlink = Patrick Macias
| title = TokyoScope: The Japanese Cult Film Companion
| publisher = Cadence Books
| year = 2001
| isbn = 1-56931-681-3
| url = 
| page = 178
}}  The Boston Globes Wesley Morris wrote, "  Kurahara takes the movie to extremes of behavior and style, merging the two until the form seems as violently unstable as the characters. He makes a wave that in Europe was called French and new. But with all due respect to Jean-Luc Godard, this is breathless - and more interesting, too." {{cite web
| last = Morris
| first = Wesley
| authorlink = Wesley Morris
| title = Putting Nikkatsu back on the map
| publisher = The Boston Globe
|date=April 2008
| url = http://www.boston.com/ae/movies/articles/2008/04/19/putting_nikkatsu_back_on_the_map/
| accessdate = 2009-06-04
}}  Morris further qualified that while Breathless may appeal to contemporary viewers academically, The Warped Ones retains a spontaneous, documentary feel.  Schilling discerned the film, "Among   boldest departures from studio convention." {{cite book
| last = Schilling
| first = Mark
| title = No Borders, No Limits: Nikkatsu Action Cinema
| publisher = FAB Press
| year = 2007
| isbn = 978-1-903254-43-1
| url = http://www.fabpress.com/vsearch.php?CO=FAB080
| pages = 135–138
}} 

Reviewer Peter Martin confided, "The Warped Ones baffled and mystified me, but I liked it very much." {{cite web
| last = Martin
| first = Peter
| title = Fantastic Fest Report: Spiral, The Warped Ones, Inside
| publisher = Twitch
|date=September 2007
| url = http://twitchfilm.net/site/view/fantastic-fest-report-spiral-the-warped-ones-inside/
| accessdate = 2009-06-08 The Readers Guide to Arts & Entertainment, found the film "actually celebrates the values its supposed to be condemning," but recommended it for its kineticism and action. {{cite web
| last = Jones
| first = J.R.
| title = The Warped Ones 
| publisher = Chicago Reader
| url = http://onfilm.chicagoreader.com/movies/capsules/33001_WARPED_ONES_KYONETSU_NO_KISETSU
| accessdate = 2009-06-08
}}  TV Guide and Allmovie did not recommended it, both gave it one star in their respective four and five star rating systems. {{cite web
| title = The Weird Love Makers: Review
| publisher = TV Guide
| url = http://movies.tvguide.com/weird-love-makers/review/122542
| accessdate = 2009-08-11
}}  {{cite web
| last = Brennan
| first = Sandra
| title = The Warped Ones 
| publisher = Allmovie
| url = http://allmovie.com/work/the-warped-ones-116228
| accessdate = 2009-08-11
}} 

==Legacy== Black Sun (1964) which again featured Tamio Kawachi, who reprised his role from The Warped Ones, as did several of the other actors, and a lot of jazz music. In it, Kawachis Akira shelters a black GI (military)|G.I., Gil, played by Chico Rolands, who goes Desertion|A.W.O.L. after killing a white man in a bar fight.  The film explores the two mens friendship and race relations. It was also the first reversal on rashamen-themed films, post-war, often American-Japanese Co-production (filmmaking)|co-productions focusing on friendships or romances between a Japanese and an American. Rashamen films were intended as to promote goodwill between the two nations but were generally less well received in Japan, seen as unrealistic or patronizing. Film historian Tadao Sato described Black Sun as the first film of this sort where Japanese pity Americans instead of the reverse as Akiras preconceptions of black Americans are undone.  {{cite book
| last = Sato
| first = Tadao
| authorlink = Tadao Sato
| coauthors = Gregory Barrett (Translator)
| title = Currents in Japanese Cinema
| publisher = Kodansha International
| year = 1982
| isbn = 0-87011-507-3
| pages = 203–207
}}  Mark Schilling characterized Kawachi as bringing an "explosive energy" to the film and Roland a "piping screech of fear and desperation."  Acclaimed American jazz musician Max Roach contributed to the score. {{cite web
| title = Black Sun (Kuroi Taiyo) 
| publisher = Tokyo Filmex
| year = 2008
| url = http://www.filmex.net/2008/sakuhin/spk09-e.htm
| accessdate = 2009-06-05
}} 
 A Clockwork Orange (1971). Lucas drew comparisons between The Warped Ones   main character Akira and A Clockwork Orange  s Alex DeLarge, including their respective obsessions with hard jazz and the music of Ludwig van Beethoven, the former with a framed copy of Ornette Colemans album The Shape of Jazz to Come next to his bed, the latter an engraving of Beethoven. Also, scenes of the formers verbal deconstructions by a group of art students versus the latters by the government. Akiras attacks on abstract art and DeLarges on pop art–lined homes. Finally, the characters regular hangouts, both painted with black walls, the formers adorned with portraits of jazz legends, the latters with advertisements for "Vellocet" and "Drencrom"—the fictional drugs DeLarge and his gang use to invigorate themselves before their criminal acts. Lucas concluded, "Kubrick simply had to have seen it." 
 sexploitation title, Tucson punk band The Weird Lovemakers assumed the name in 1994 and held it until their disbandment in 2000. {{cite web
| last = Jennings
| first = Don
| title =  The Weird Lovemakers 1992-2000 / The Knockout Pills
| publisher = KXCI Community Radio
|date=May 2009
| url = http://www.publicbroadcasting.net/kxci/.artsmain/article/17/218/814983/Tucson.Music.Scene/The.Weird.Lovemakers.1992-2000..The.Knockout.Pills/
| accessdate = 2009-05-30
}}  The Oakland, California–based electropop band The Lovemakers planned to use the same name on their inception in 2002 but dropped the "Weird" upon their discovery of the former band having taken the name. {{cite web
| last = Limon
| first = Enrique
| title =  Make love, not war
| publisher = San Diego CityBeat
|date=March 2009
| url = http://sdcitybeat.com/cms/story/detail/make_love_not_war/7848/
| accessdate = 2009-05-30
}} 

==Home video==
In North America, an abridged, Dubbing (filmmaking)|dubbed, VHS version of the film is available from Something Weird Video under the moniker The Weird Lovemakers. {{cite web
| title = The Warped Ones Screening At Fantastic Fest 2007
| publisher = TexasGeek.tv
|date=January 2007
| url = http://blip.tv/file/455169
| accessdate = 2008-08-09
}}  {{cite web
| title = Weird Lovemakers - VHS
| publisher = Something Weird Video
| url = http://www.somethingweird.com/cart.php?target=product&product_id=20383
| accessdate = 2008-08-09
}}  In 2007, a DVD-R version was also made available. {{cite web
| title = Supplement 24!
| publisher = Something Weird Video
| url = http://www.somethingweird.com/cart.php?page=supplement_24_
| accessdate = 2008-08-09
}}  A DVD for The Warped Ones will be released by the Criterion Collection on August 23, 2011 as part of their "The Warped World of Koreyoshi Kurahara" compilation. 

==Soundtrack==
{{Infobox album  
| Name        = The Warped Ones / Black Sun
| Type        = Soundtrack
| Longtype    = 
| Artist      = Toshirō Mayuzumi
| Cover       = Black Sun The Warped Ones soundtrack.jpg
| Alt         = On the left, Eiji Go and Tamio Kawachi laugh. On the right, Chico Rolands holds a gun and Kawachi holds his arm over his head.
| Released    = February 23, 2007
| Recorded    = 
| Genre       = Soundtrack, Jazz
| Length      = 
| Label       = Think
| Producer    = 
| Reviews     = 
| Last album  = 
| This album  = 
| Next album  = 
}} label Think! Black Sun Eddie Kahn Black Sun and The Warped Ones. {{cite web
| script-title=ja:和製ジャズ・ビートニク映画音楽傑作撰（日活編）」発売
| publisher = Jazz Tokyo
|date=March 2007
| url = http://www.jazztokyo.com/hotline/archive-local07.html#loc070310e
| language = Japanese
| accessdate = 2008-07-21
}}  {{cite web
| script-title=ja:黒い太陽」／「狂熱の季節」オリジナル・サウンドトラック／黛敏郎
| publisher = CD Journal
| url = http://www.cdjournal.com/main/cd/disc.php?dno=4106121277
| language = Japanese
| accessdate = 2008-07-21
}}  The second disc features the Nikkatsu Jazz Group 

===Track listing===
{| cellpadding=2 style="text-align: right; border-width: 0px; border-collapse: collapse;"
|- style=background-color:#ddf;
! !! colspan=2 align=left | Black Sun !! !! colspan=5 align=left | The Warped Ones 
|- style=background:#eee;
! width=2% | # !! align=left | Title !! Length !! width=5% | # !! align=left | Title !! Length !! width=5% | # !! align=left | Title !! Length
|- style=background:#fff;
| 1.
| align=left | "Scene A"
| 5:04
| 1.
| align=left | "Scene 1"
| 2:02
| 11.
| align=left | "Scene 11"
| 1:42
|- style=background:#f7f7f7;
| 2.
| align=left | "Scene A2"
| 4:52
| 2.
| align=left | "Scene 2"
| 2:05
| 12.
| align=left | "Scene 12"
| 0:47
|- style=background:#fff;
| 3.
| align=left | "Scene B"
| 5:27
| 3.
| align=left | "Scene 3"
| 1:32
| 13.
| align=left | "Scene 13"
| 1:30
|- style=background:#f7f7f7;
| 4.
| align=left | "Scene C"
| 7:02
| 4.
| align=left | "Scene 4"
| 2:21
| 14.
| align=left | "Scene 14"
| 4:25
|- style=background:#fff;
| 5.
| align=left | "Scene D"
| 2:30
| 5.
| align=left | "Scene 5"
| 0:44
| 15.
| align=left | "Scene 15–1"
| 0:21
|- style=background:#f7f7f7;
| 6.
| align=left | "Scene E2"
| 10:36
| 6.
| align=left | "Scene 6"
| 0:56
| 16.
| align=left | "Scene 15–2"
| 0:36
|- style=background:#fff;
| 7.
| align=left | "Scene E3"
| 8:28
| 7.
| align=left | "Scene 7"
| 3:54
| 17.
| align=left | "Scene 16"
| 0:40
|- style=background:#f7f7f7;
| 8.
| align=left | "Scene F2"
| 4:55
| 8.
| align=left | "Scene 8"
| 1:33
| 
| 
| 
|- style=background:#fff;
| 9.
| align=left | "Scene G"
| 2:53
| 9.
| align=left | "Scene 9"
| 0:50
| 
| 
| 
|- style=background:#f7f7f7;
| 10.
| align=left | "Scene H"
| 2:15
| 10.
| align=left | "Scene 10"
| 0:21
| 
| 
| 
|}

==References==
 

==External links==
*  
*  
*     at the Japanese Movie Database
*  by Chuck Stephens

 
 

 
 
 
 
 
 
 
 
 