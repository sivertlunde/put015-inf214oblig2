Goofy Goat Antics
 
{{Infobox Hollywood cartoon|
| cartoon_name = Goofy Goat Antics
| image =  
| caption = 
| director = Ted Eshbaugh
| story_artist = Ted Eshbaugh
| animator =  Ted Eshbaugh and Pete Burness
| voice_actor = unknown
| producer = Amadee J. Van Beuren
| studio = Van Beuren Studios 
|distributor= RKO Radio Pictures
|release_date= 1931 (est.)
| color_process = Two-strip Technicolor  , Black and white
| runtime = 7 min 47 sec (one reel) English
| country = USA
| budget = unknown
}}
 
Goofy Goat Antics is a 1931 Ted Eshbaugh cartoon produced by Van Beuren Studios.

== Plot ==
The goat is driving his car, which is stuck in a traffic jam behind a fat pig. Goofy manages to make it to the Glee Club, where he and his friends put on a little show.

== Trivia ==
The film was banned by many theatres because of the scene where the pig exclaims "damn!" after the goat passes him.
This film is said to be among the first color cartoons made.

== References ==
 
 
* 
 

 
 
 
 


 