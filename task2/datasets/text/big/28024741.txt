Hole in the Paper Sky
{{Infobox film
| name           = Hole in the Paper Sky
| image          = Hole in the Paper Sky.jpg
| caption        = Official film poster
| director       = Bill Purple	
| producer       = Jessica Biel Michelle Purple
| writer         = Bill Purple	
| screenplay     = Howard Kingkade Jason Clarke Jessica Biel Garry Marshall Stephen Collins
| music          = Kerry Muzzey
| cinematography = Jim Orr
| editing        = George Folsey Jr.
| studio         = Iron Ocean Films
| distributor    = 
| released       =  
| runtime        = 34 minutes
| country        = U.S.
| language       = English
| budget         =  $50,000 (estimated) 
| gross          = 
}}
 short drama Jason Clarke, Stephen Collins and Jeff Nordling and features an original score by Kerry Muzzey. It was released August 7, 2008 at the HollyShorts Film Festival.

==Plot== laboratory dog.

==Cast== Jason Clarke as Howard Ferp
*Jessica Biel as Karen Watkins
*Garry Marshall as Warren
*Stephen Collins as Mr. Benson
*Jeffrey Nordling as Prof. Cory

==Awards==
Completed in 2008 and released on iTunes and ShortsTV in 2010, Hole in the Paper Sky has garnered numerous awards and honors including:
*Beverly Hills Film Festival
**Best Short Film 
**Best Screenplay 
*Florida Film Festival
**Best Short Film 

and has been honored at the Holly Shorts Film Festival and Australia’s Flickerfest.

== References ==
*  

==External links==
* 
*http://pro.imdb.com/news/ni0239086/

 
 