Udhayam NH4
 
 

{{Infobox film
| name           = Udhayam NH4
| image          = Udhayam NH4 first look.jpg
| alt            =  
| caption        =
| director       = Manimaran
| producer       = Dayanidhi Azhagiri Vetrimaaran  (creative producer) 
| writer         = Vetrimaaran  (dialogue) 
| screenplay     = Vetrimaaran Manimaran
| story          = Vetrimaaran
| starring       =  
| music          = G. V. Prakash Kumar
| cinematography = Velraj
| editing        = Kishore Te.
| Makeup Artist  = Suresh Kumar
| studio         = Dayanidhi Azhagiri | Meeka Entertainment Vetrimaaran | Grass Root Film Company
| distributor    = Red Giant Movies
| released       =   
| runtime        = 113 minutes
| country        = India
| language       = Tamil
| budget         =    
| gross          = 
}}
 Tamil romantic Siddharth and Telugu version titled NH4 on 19 April 2013.  

== Plot == route from Bangalore to Chennai and plan to kidnap Rithika (Ashrita Shetty). Meanwhile her dad appoints encounter specialist Manoj Menon (Kay Kay Menon) who questions Rithikas male friend Deepak (Deepak) about Prabhu. The guy claims that Prabhu is a drug addict and was always trying to woo Rithika, who started to avoid him because of Prabhu. After that the plot continues when Manoj tries to track Prabhu and his friends. In the attempt, they track down a message from another friend Umesh asking Prabhu and friends to come to Bangarpet bus stand only to find Manoj and his team arresting the guy for more information. Manoj goes to a railway station where the train in which Prabhu and Rithika are travelling to Chennai. He finds Rithika and forcefully drags her into his jeep. As the flashback winds again (this time by Rithika), she fell for Prabhu after seeing him on many instances showing his presence of mind. As the flashback ends, Manoj finds Prabhu on the highway where he doesnt stop until he runs over pieces of glass put down by Prabhu. They get into a brawl where Prabhu loses and Manoj continues in the highway but has to stop in a railway signal. Prabhu takes his chance and threatens Manoj keeping his throat under a blade and taking Rithika with him. After a cat and mouse chase, Manoj calls Prabhu saying that his friend needs immediate treatment after an accident. He goes in search of Manoj and involves in a fight with him again. Now, after the stroke of midnight Rithika turns 18 giving her the right for choosing her partner. After that Manoj leaves the Highway as he couldnt do anymore according to law.

== Cast == Siddharth as Prabhu
*Ashrita Shetty as Rithika
*Kay Kay Menon as Manoj Menon
*Avinash as Avinash Patel
*Aadukalam Naren as Prabhus brother-in-law
*Surekha Vani as Prabhus sister
*Achutha Kumar
*Karthik Sabesh as Karthik
*Deepak as Deepak
*Ajay
*Rohit Balaiah 
*P. Kalaivanan
*Yash Bothra as Umesh
*Gaana Bala in a special appearance
*Kaali Venkat

== Production ==
While working with Balu Mahendra for Adhu Oru Kana Kaalam (2004), producer Vetrimaaran prepared a script for Dhanush, who was the lead hero of the film he worked on, and Dhanush immediately accepted the offer after hearing the story. The film titled was Desiya Nedunchalai 47 was initially launched with Yuvan Shankar Raja as the music director and Ekambaram as the cinematographer.  However, he found trouble finding producers with A. M. Rathnam and Salem Chandrasekhar leaving the project after initial interest, Dhanushs sister Dr. Vimala Geetha agreed to produce the film, but she also dropped the film. Dhanushs father Kasthuri Raja finally agreed to produce the film and Kirat Bhattal was signed as heroine, while Harris Jayaraj was selected as music director. However, after two days of shoot the film was shelved and Dhanush opted to pursue other films after the surprise success of his Thiruvilayadal Arambam (2006).    The films collapse saw Vetrimaaran approach producer Kadiresan and narrated to him the stories he had prepared but the producer did not like Desiya Nedunchaalai 47, but later agreed to work on another project titled Polladhavan (2007 film)|Polladhavan.
 Siddharth for the lead role, who immediately agreed to it.  Kishore (actor)|Kishore, who was initially roped in for a role in 2006, was re-approached but was unable to commit dates for the film. Subsequently Kay Kay Menon, who Vetrimaaran had been discussing a role in his next film Vada Chennai with, was signed on to play a pivotal role in the film. Debut actress Ashrita Shetty was selected to play the lead female role.  Ramya will appear in a cameo appearance in the film.  Newbies Ajai, Kalai and Karthi play Siddharths friends.  Velraj is the cinematographer and editing will be by Kishore Te.  The film will be produced jointly by Dayanidhi Azhagiri and Vetrimaaran as a tie-up between their companies Meeka Entertainment and Grass Root Film Company.  The filming took place only on real locations, where crowds delayed shootings up to four days, making hidden cameras mandatory. 

== Soundtrack ==
{{Infobox album 
| Name       = Udhayam NH4
| Type       = Soundtrack
| Artist     = G. V. Prakash Kumar
| Cover      =
| Released   =
| Recorded   = 2013 Feature film soundtrack Tamil
| Label      = Sony Music
| Producer   = 
| Last album = Annakodi (2013)
| This album = Udhayam NH4 (2013)
| Next album = Thalaivaa (2013)
}}
 Suryan FM. Siddharth sang the Telugu version of this song. 

Tracklist
{{tracklist
| extra_column   = Singer(s)
| total_length   = 18:47
| lyrics_credits = yes

| title1    = Yaaro Ivan
| extra1    = G. V. Prakash Kumar & Saindhavi
| length1   = 04:41
| lyrics1   = Na. Muthukumar

| title2    = Maalai Pon Maalai
| extra2    = Bela Shende & S. P. B. Charan
| length2   = 05:22
| lyrics2   = La. Rajkumar

| title3    = Ora Kannala
| extra3    = Gaana Bala
| length3   = 04:47
| lyrics3   = Gaana Bala

| title4    = Indrodu Thadaigal Srinivas & Ramya NSK
| length4   = 03:57 Vaali

| title5    = Vaa Iravugal
| extra5    = Ajmal Khan, Amrith Vishwanath
| length5   = 05:30 Kabilan
}}

== Release == Sun TV.  The film was given a "U/A" certificate by the Indian Censor Board. On 14 February 2013, the first film trailer was released at Sun Music. Udhayam NH4 released in around 250 theatres across Tamil Nadu along with Gouravam (2013 film)|Gouravam (2013).

== Reception ==
Upon release Udhayam NH4 generally received favourable reviews. Baradwaj Rangan of The Hindu thanked the film crew for a well treated, but slight story, "  the constant infusion of colour keeps us hooked. I don’t think I’ve laughed so much at a cop telling his nagging wife he loves her."  Behindwoods gave 3 of 5 stars and concluded, "To sum it all, Udhayam NH 4 comes across as a well packaged product that has many things going in its favor and is sure to have patrons from the youth along with other sections of the audience too."  Sify recommended the film, "The presentation is unique as it doesn’t have the trappings of usual masala potboilers."  in.com rated the film 3.5 out of 5 and stated that the story is simple but the taut screenplay with unexpected twists and turns at the right time makes Udhayam a good watch.  Mahalakshmi Prabhakaran of Daily News and Analysis criticized the performance of Ashrita Shetty heavily, "As the character who’s the prize catch for both the cop and the boyfriend, the role’s every debutante actress dream. But Hrishita just doesn’t match up. We are not even going to comment on her poor delivery of Tamil. Yes, she might be a Kannadiga in the movie but that’s no excuse!", giving the film an average rating of 3.  S. Saarawathi of Rediff wrote, "Udhayam NH4 is a racy entertainer that keeps you glued to the screen."  Sidharth Varma of The Times of India rated it 3.5 out of 5 stars and said, "The movie brings in a whiff of fresh air in the dialogue department. The characters switch between the four south Indian languages and English depending on the place where the scene is set and the situation, which adds to the authenticity of the movie. The drama is injected in the right doses through the incidents on the highway, and flashbacks tell us more about each character."  Sangeetha Devi Dundoo of The Hindu recommended the film, "Take a ride down this highway just to see how a talented director can make a simple story engaging."  Review Raja found the film to be "just an alright movie", and thought, that Ashrita Shetty wasnt capable enough to play the female lead role.  Bangalore Mirror gave the film 3.5 out of 5 saying that the film is well crafted. 

== Box office ==
Udhayam NH4 had a fantastic opening at the box office.It collected   in its first week.The film grossed   by the end of the second week.Udhayam NH4 became a hit, grossing    at the box office within 30 days of its release.

==References==
 

== External links ==
* 

 

 
 
 
 
 
 
 
 
 
 