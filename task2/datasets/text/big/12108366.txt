Spider Lilies (film)
{{Infobox film
| name           = Spider Lilies
| image          = Spider Lilies film poster.jpg
| caption        = Spider Lilies poster
| film name = {{Film name| traditional    = 刺青
| pinyin         = Cì Qīng}}
| director       = Zero Chou
| producer       = Lin Yun-hou
| writer         = Singing Chen
| starring       = Rainie Yang Isabella Leong Shen Jian-hung Kris Shie Shih Yuen-chien
| music          = Hwang Chien-hsun Chang Chien-yu
| cinematography = Hoho Liu
| editing        = Hsiao Ju-kuan
| studio         = 
| distributor    = Central Motion Pictures Corporation
| released       =  
| runtime        = 94 minutes
| country        = Taiwan Mandarin
| budget         = 
| gross          =
}}
Spider Lilies ( ) is a 2007 Taiwanese lesbian drama film. It is the second feature-length film by director Zero Chou, and stars Rainie Yang and Isabella Leong in the lead roles. Spider Lilies was screened at the 2007 Berlin International Film Festival, where it won the Teddy Award for best feature film.   It was released in the United States by Wolfe Video on 6 May 2008. 
 Golden Horse Awards. 

== Plot summary == spider lilies—on Takekos arm. She wants the same design, but Takeko refuses, telling her that the flowers are cursed.

Takekos father, who was killed in an earthquake, had the same tattoo on his arm. Her younger brother witnessed the incident and was traumatised by it, left with no memory except for the image of the flowers. Takeko decided to get the same tattoo, in the hope that it would help her brothers recovery.

Nevertheless, Takeko finds herself drawn to Jade, and begins designing a new tattoo for her.

Meanwhile, a young police officer is trying to ambush Jade and the rest of the girls working in the same website. However, he takes to speaking to her, listening to her childhood stories and connecting with her, thereby slowing down the investigation he is supposed to be working on. Eventually he falls in love with her, trying to tell her to get out before its too late and before shes caught. He blurts out that he loves her, and Jade, mistaking him for Takeko, goes to her. 

When one of Takekos customers gets into a fight and loses his arm, Takeko sends him to the hospital and forgets to pick up her brother. Desperate and frightened, he goes out into the street to look for her, and recovers his memory just before falling down a steep hill.

Takeko finds her brother in the hospital, where he has slipped into a severe coma.  Devastated and guilt-stricken, she sends a farewell message to Jade saying that she will not be able to finish Jades tattoo. 

Later, Jade decides to go online and wait for Takeko. At this time, the policeman finally confesses his true identity to Jade and tells her that she must get offline immediately. She cries, realizing it was not Takeko who had confessed love to her earlier. 

Eventually, Takekos brother awakes from his coma with his memory intact.  Joyful Takeko sends Jade another message apologizing, and saying that she will wait for her in the tattoo shop.  The last image of the film is footage of Jade, coming to meet Takeko.

== Cast ==
* Rainie Yang as Jade
* Isabella Leong as Takeko
* Shen Jian-hung
* Kris Shie
* Shih Yuen-chien

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 