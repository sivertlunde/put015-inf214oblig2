Pajama Party (film)
 
{{Infobox film
| name           = Pajama Party
| image          = Pajama party.jpg
| caption        = theatrical poster
| image_size     =
| director       = Don Weis
| producer       = Samuel Z. Arkoff James H. Nicholson
| writer         = Louis M. Heyward Jesse White
| music          = Score: Les Baxter Songs: Jerry Styner Guy Hemric
| cinematography = Floyd Crosby
| editing        = Eve Newman
| distributor    = American International Pictures
| released       =  
| runtime        = 82 minutes
| country        = United States
| language       = English
| budget         = $200,000 Weaver, Brunas and Brunas p 160 
| gross          =
}}
Pajama Party is a 1964 beach party film starring Tommy Kirk and Annette Funicello. This is the fourth in a series of seven beach films produced by American International Pictures. The other films in this series are Beach Party (1963), Muscle Beach Party (1964), Bikini Beach (1964), Beach Blanket Bingo (1965), How to Stuff a Wild Bikini (1965), and The Ghost in the Invisible Bikini (1966).

This fourth entry has not always been considered a follow-up to the three films that preceded it.  Several sources have noted, however, that while it is not a proper sequel, it is indeed a part of what is now termed AIPs ‘Beach Party series.’ Moreover, AIP marketed it as a sequel in its trailer, stating "The Bikini Beach Party Gang is Warming Up! – For the ‘Party’ that Takes Off – Where others Poop Out!" and "All the ‘Beach Party’ Fun … in Pajamas!"

Additional links that tie this film to the others are the return of Eric von Zipper and his Rat Pack (who previously appeared in Beach Party and Bikini Beach) and the return of Candy Johnson as Candy for the fourth time in as many films.

Regulars Frankie Avalon, Don Rickles, Annette Funicello, Jody McCrea and Donna Loren all appear (albeit with character name changes – not the first time this happens in the series, nor the last); Susan Hart makes the first of three appearances in the AIP brand of the genre; Buster Keaton makes the first of four appearances, and Bobbi Shaw makes the first appearance of five.
 Mike Nader, Salli Sachse, Luree Holmes, Ronnie Dayton, Ed Garner, Ray Atkinson, Linda Benson, and Laura Nicholson) also appear in three or more films in the AIP brand of the genre.

In the fashion store scene (in which Dorothy Lamour sings Where Did I Go Wrong, while having models show the latest fashions, both Teri Garr, and Toni Basil appear as (dancing) models; Ms. Garr in a tennis outfit, while Ms. Basil - in a silver lamé bikini - is the last model.

Ms. Basil was the choreographer of the American musical variety series, Shindig!, and Ms. Garr - a friend of Ms. Basil - was a series regular on it as well.

The final credit in the original titles gives the name of the next film in the series, Beach Blanket Bingo.

The film is not to be confused with the 1963 novel Pajama Party about lesbian activities among college girls, which was banned on the grounds of obscenity. BOOK SELLER GETS 1 YEAR, $1,000 FINE
Chicago Tribune (1963-Current file)   13 May 1965: a2.  
 
==Plot==
Teen aged Martian intelligence officer 006, named Gogo (Tommy Kirk), is sent to Earth wearing an ushers uniform to prepare the way for a Martian invasion. The first earthling he meets, Aunt Wendy (Elsa Lanchester), is an eccentric widow who runs a dress shop. She has Gogo, now calling himself George, get dressed in a swimsuit and sends him to the beach. There he meets her nephew, Big Lunk (Jody McCrea), who enjoys volleyball but has little interest in romance, which is frustrating for his girlfriend Connie (Annette Funicello).
 Jesse White), and his gang (with Buster Keaton as Chief Rotten Eagle and Bobbi Shaw as the Swedish Helga) concoct a scheme to separate Aunt Wendy from her cash. Meanwhile Eric von Zipper (Harvey Lembeck) and his motorcycle gang, the Rat Pack want to get revenge against the beach teenagers. George and Connie fall for each other, while Big Lunk is attracted to Helga, and for the fourth time in as many films, a giant fight breaks out at the end.

==Cast==
 
*Tommy Kirk	as Go-Go
*Annette Funicello as Connie
*Elsa Lanchester as Aunt Wendy
*Harvey Lembeck as Eric Von Zipper Jesse White as J. Sinister Hulk
*Jody McCrea as Big Lunk
*Ben Lessy as Fleegle
*Donna Loren as Vikki
*Susan Hart as Jilda
*Bobbi Shaw as Helga
*Cheryl Sweeten as Francine
*Luree Holmes as Perfume Girl
*Dorothy Kilgallen as herself
*Candy Johnson as Candy
*Buster Keaton as Chief Rotten Eagle
*Dorothy Lamour as Head Saleslady
*Don Rickles as "Big Bang" the Martian
*Frankie Avalon as Socum
*Teri Garr as Pajama Girl
*Toni Basil as Pajama Girl
 

==Production notes==
Famed animator Joseph Barbera wrote a romantic comedy play which debut on stage in Los Angeles in 1952 called The Maid and the Martian. It was about Captain Derro, a scout from Mars, who goes to Earth to help plan an invasion, but falls in love with a girl from Earth. The Los Angeles Times said the play "has strong elements and might even go to Broadway... provided it gains more completeness in plot and situation." Maid and Martian Hits Spicy Vein of Comedy
Schallert, Edwin. Los Angeles Times (1923-Current File)   17 Oct 1952: B6.   The production was directed by Gordon Hunt and starred Pat Prest and ran successfully for seven weeks. Maid, Martian Triple-Threat Team Showing Promise of Brilliant Future: Maid, Martian Team Showing Great Promise
Schallert, Edwin. Los Angeles Times (1923-Current File)   07 Dec 1952: D1.   The play was revived in 1954 with James Arness in the lead. STUDIO BRIEFS
Los Angeles Times (1923-Current File)   20 Mar 1954: 12.  

In 1961 AIP announced they would make The Maid and the Martian from a script by Al Burton and Gordon Hunt, based on the play. Lancaster, Garson Win New Awards: Golden Globes Distributed Lavishly by Foreign Press
Scheuer, Philip K. Los Angeles Times (1923-Current File)   17 Mar 1961: A12.   Stanley Frazen was to produce. Abby Dalton, Signs With Jack Arnold: Aldrich Script Writer Found; King of Kings Cast Reprised
Scheuer, Philip K. Los Angeles Times (1923-Current File)   13 May 1960: A11.   However none of those people are credited on Pajama Party despite the fact the plot seems to share strong similarities with the final film.
===Script===
This was the first movie Louis M Heyward worked on for AIP. He wrote the script in two weeks, saying he tried to do it as a cartoon "and if you look at it, its done almost in cartoon cuts, in four-strips." Weaver, Brunas & Brunas, p 157  Heyward says the film was firmly aimed at the 15 to 25 year old demographic. "These youngsters have the numbers, the buying power and the discrimination to make or break any film product." The Teen-age Set---Or Moola Behind the Hairdo Curtain
SEIDENBAUM, ART. Los Angeles Times (1923-Current File)   22 Nov 1964: Q2.  

Heyward went on to write several other films for the studio, and became a leading executive for them.
===Director===
Pajama Party is one of only two Beach Party films not directed by William Asher. Pajama Party and The Ghost in the Invisible Bikini were both directed by Don Weis.
===Cast=== David Winters of West Side Story fame, who choreographed the picture and were the "David Winters dancers" appearing in most of the films he choreographed; they are listed in the end credits as "Pajama Girls".    Frankie Avalon appears in the film in all the scenes with Don Rickles, but only the back of Avalons head is seen until the final moments.

During the entire Beach Party series, this was the one and only time Donna Loren was seen in a speaking role.  

Syndicated newspaper columnist Dorothy Kilgallens son, Kerry Kollmar, has a recurring role throughout the film as a little boy who declares disgustedly "Mush!" whenever he spies romance in action.  Kilgallen herself, whose newspaper column was not accessible in Los Angeles and who was better known there as a TV game show panelist, has a tiny cameo as a woman who falls on J.D.s motorcycle during the car chase sequence. She introduces herself saying, "My name is Dorothy – whats yours?" Discotheque Lures Jack Jr. and Jill
Dorothy Kilgallen:. The Washington Post, Times Herald (1959-1973)   22 Aug 1964: C11.  

Cheryl Sweeten, who was the 1963 Miss Colorado and played Francine in this film, made only this one film, but she received prominent billing in the end credits.

This was the first movie Susan Hart made for AIP under her four picture contract with the studio.  She was one of a number of young players in the film who were under a long-term deal with AIP, the others including Donna Loren, Bobbi Shaw, Cheryl Sweeten, Mary Hughes, Michael Nader and Edward Garner. Young Players Enact Roles in Pajama Party
Los Angeles Times (1923-Current File)   27 Nov 1964: D24.  

It was also the first movie Buster Keaton made for AIP. Louis M Heyward claims casting Keaton was his idea as they had worked together previously on The Faye Emerson Show.  and the first movie  Bobbi Shaw, is playing her "ya, ya" Swedish bombshell part, Keatons partner. 

===Choreography=== David Winters of Shindig! and Hullabaloo (TV series)|Hullabaloo fame. Both Teri Garr and Toni Basil were Winters students at the time.

===Production===
Filming started on 10 August 1964. 
 Surfrider Beach in Malibu.

Susan Hart claimed second unit footage was later shot where her legs were substituted by another persons. 

===Product placement=== Ford Mustang is also featured in several scenes.

==Music==
Guy Hemric and Jerry Styner wrote all the songs heard in the film, and several melodies were picked up and used for the films score by composer Les Baxter. The music supervisor was Al Simms.

Annette Funicello performs "Its That Kind of Day", with the cast and also sings "Stuffed Animal" as well as the title track.

Funicello and Tommy Kirk sing "There Has to Be a Reason"; Dorothy Lamour sings "Where Did I Go Wrong"; Donna Loren sings "Among the Young."
 The Nooney Rickett 4 (who appeared in Columbia Studios|Columbias beach party film, Winter A Go-Go the following year) play backup for "Among the Young", and are shown playing backup for "Pajama Party."  The band also performs an instrumental version of "Among the Young" in the film - entitled "Beach Ball" - and are shown performing an instrumental of "Its That Kind of Day."
==Reception==
The Los Angeles Times said "AIPs stock company puts on a frantic, funny show. Individual performances are standard for this type of picture." Pajama Party Slanted for Young Audiences
Scott, John L. Los Angeles Times (1923-Current File)   04 Dec 1964: E6.  

The popularity of the film saw Buster Keaton appear in a number of AIP movies before his death. Comedy Not What It Used to Be, Says Buster
Alpert, Don. Los Angeles Times (1923-Current File)   28 Mar 1965: B4.  

Don Weis, Heyward and Kirk collaborated on another AIP beach party film which was actually a pajama party movie, The Ghost in the Invisible Bikini.
==Legacy==
A sequence in the 1996 film That Thing You Do! makes an overt reference to the Nooney Rickett 4s saxophone-heavy Beach Ball scene in Pajama Party, as well as to the beach party genre in general. The band in the film, The Wonders, mime a live performance of an instrumental song during the filming of a beach party film titled Weekend at Party Pier. The film is referred to as "Rick & Anita film" (the AIP films are often called "Frankie & Annette films"), and includes a Deadhead/Bonehead character called "Goofball." The four-man Wonders band - including the guitarist now playing a saxophone - are seen playing a song on an elevated wooden stage surrounded by a wildly dancing crowd in various bathing suit attire, as in Pajama Party. Although in That Thing You Do! the Wonders are being forced by the studio to pretend they are a band called "Capn Geech and the Shrimp Shack Shooters," in reality all acts performing in the AIP beach party films appeared as themselves. 

== References ==
 
*Weaver, Tom, Michael Brunas, John Brunas, "Louis M. Heyward", Science Fiction Stars and Horror Heroes: Interviews with Actors, Directors, Producers and Writers of the 1940s through 1960s, McFarland 1991

==External links==
* 
* 
* 
*  at Brians Drive-in Theatre
* 
 
 

 
 
 
 
 
 
 
 
 
 
 