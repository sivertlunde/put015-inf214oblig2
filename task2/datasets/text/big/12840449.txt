Outwitting Dad
 
{{Infobox film
| name           = Outwitting Dad 
| image          = 
| caption        = 
| director       = Arthur Hotaling
| producer       = Lubin Manufacturing Company
| writer         = Frank Griffin	
| starring       = Billy Bowers Oliver Hardy
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 10 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}

Outwitting Dad is a 1914 American comedy film that features Oliver Hardys first onscreen appearance.

==Cast==
* Billy Bowers as Mr. Gross
* Oliver Hardy as Reggie Kewp (credited as O.N. Hardy)
* Raymond McKee as Bob Kewp
* Frances Ne Moyer as Lena Gross

==See also==
* List of American films of 1914
* Oliver Hardy filmography

==External links==
* 

 
 
 
 
 
 
 
 
 

 