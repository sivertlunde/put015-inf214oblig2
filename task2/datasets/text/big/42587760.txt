Pau Brasil (film)
 
{{Infobox film
| name           = Pau Brasil
| image          = Pau Brasil Film Poster.jpg
| caption        = Theatrical release poster
| director       = Fernando Belens
| producer       = Sylvia Abreu Luciano Floquet
| writer         = Fernando Belens Dinorath do Valle
| starring       = Bertrand Duarte Osvaldo Mil Fernanda Paquelet Arany Santana
| music          = Bira Reis
| cinematography = Hamilton Oliveira
| editing        = André Bendocchi-Alves
| studio         = Truque Produtora de Cinema  Studio Brasil  40° Filmproduktion
| distributor    = Caliban Cinema e Conteúdo
| released       =  
| runtime        = 98 minutes
| country        = Brazil
| language       = Portuguese
| budget         = 
| gross          = 
}}Pau Brasil is a 2009 Brazilian drama film, the first feature film by director Fernando Belens, an experienced short filmmaker, with about 20 short films made since the 70s.   

The film was screened at various film festivals, including the  Mostra do Filme Livre, Mostra Internacional de São Paulo, the Panorama Internacional Coisa de Cinema and the Los Angeles Brazilian Film Festival. Although it was completed in 2009, Pau Brasil only managed to reach the Brazilian commercial circuit on April 2014. 

==Plot==
In a small, lost village in the heart of Brazil, families of Joaquim and Nives live side by side. Although they live together with the same perverse structure by the social oppression, they deal with life in a radically different way.   

==Cast==
*Bertrand Duarte as Nives
*Osvaldo Mil as Joaquim
*Fernanda Paquelet as Juraci
*Arany Santana as Leandra

==References==
 

== External links ==
*  

 
 
 


 
 