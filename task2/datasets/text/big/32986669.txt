Robbery Under Arms (1907 film)
 
{{Infobox film
  | name     = Robbery Under Arms
  | image    = 
  | caption  =  Charles MacMahon		
  | producer =  Charles MacMahon
  | based on = novel by Rolf Boldrewood
  | starring = Jim Gerald
  | music    = 
  | cinematography = C. Byers Coates 
  | editing  = 
  | distributor = Tait brothers
  | released = 1 October 1907   
  | runtime  = 5,000 feet (over 60 min.) 
  | language = Silent film English intertitles
  | country = Australia
  | budget   = £1,000 Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 7-8  
  }}
 novel by Rolf Boldrewood about two brothers and their relationship with the bushranger Captain Starlight.  It was the first film version of the novel and the third Australian feature ever made. 

It is considered a lost film. 

==Synopsis==
Key scenes of the film included the branding of the stolen cattle by the Marstons, the stealing of the horse Marquis of Lorne, the capture of Starlight and Dick Marston, the robbery of the mail coach, the bail up of the gold escort, the sticking up of Whitmans, the attack on Keightley station, the ride of Mrs Keightley to raise the ransom, the escape from Berrima Gaol and Starlights last stand. 

==Cast==
*J Williams as Starlight 
*Jim Gerald ("S. Fitzgerald") as Warrigal 
*Mrs W.J. Ogle as Mrs Keighley
*George Merriman as warder
*Lance Vane as Inspector of Police
*William Duff as trooper
*Arthur Guest as curate
*Rhoda Dendron

==Production== Charles MacMahon made the movie after working for five years in New Zealand. It seems likely that the script was taken directly from the novel, and not any stage adaptation of the book (which was the case with the 1911 version, Captain Starlight, or Gentleman of the Road).  

Shooting took place over six weeks with a cast of twenty five.  Locations included Narrabeen, Hornsby, Moss Vale, Wollongong racecourse, Bathurst, the Turon, and Flemington sale yards, among other places. 

The cinematographer was C Byers Coates, who worked for the film firm of Osborne and Jerdan. Coates shot 10,000 feet of film all up which was later processed at Osborne and Jerdans premises in George Street, Sydney.   

The budget has been given as £900    or £1,000. 

The role of Warrigal, the aboriginal tracker, was played by Jim Gerald who later became a major vaudeville star; it was one of his few film roles.  (He may have been billed as "Fitzgerald".) 

==Reception==
The movie was often shown on a double bill with a live vaudeville show. It was a popular success at the box-office, seen by 30,000 people in Sydney ("hundreds turned away" ) and ran for 12 weeks in Melbourne during its initial season.   It ran in cinemas for three years. 

The critic for the West Australian said that "some of the bush scenes are very beautiful, and at the same time intensely interesting. Mrs. Keightleys ride to Bathurst in quest of the ransom for her husbands life and the "sticking up" of the Engowra gold escort were realistic items." 

==See also==
*List of Australian films before 1910

==References==
 

==External links==
*  
*   at National Film and Sound Archive

 
 

 
 
 
 
 
 