Thou Wast Mild and Lovely
{{Infobox film
| name           = Thou Wast Mild and Lovely
| image          = 
| caption        = 
| director       = Josephine Decker
| producer       = Laura Heberton Laura Klein Interests Lavalette Russell Sheaffer
| writer         = Josephine Decker
| starring       = Joe Swanberg Sophie Traub Robert Longstreet
| music          = Molly Herron Jeff Young
| cinematography = Ashley Connor
| editing        = David Barker Josephine Decker Steven Schardt
| distributor    = Cinelicious
| released       =  
| runtime        = 94 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Thou Wast Mild and Lovely is a 2014 experimental thriller film written and directed by Josephine Decker.

==Cast==
*Joe Swanberg as Akin
*Sophie Traub as Sarah
*Robert Longstreet as Jeremiah
*Kristin Slaysman Drew
*Matt Orme as Caren
*Geoff Marslett as Richard

==Production== East of Eden as inspiration for elements of the film, though David Rooney of The Hollywood Reporter has compared the visuals of the film to the works of Terrence Malick.   

==Release==
===Media=== VOD distribution by Cinelicious Pics along with Deckers 2013 film Butter on the Latch with a release set for November 2014.  

==Reception==
===Critical response===
Thou Wast Mild and Lovely received a moderately positive response from critics.  I currently holds a 67% positive rating on the review aggregator Rotten Tomatoes. 

In his review of the film, Eric Kohn of  s The Grand Budapest Hotel.     Deckers other 2014 film, Butter on the Latch, also made the Brodys top ten, clocking in at tenth place. 

==References==
 

==External links==
* 

 
 
 
 