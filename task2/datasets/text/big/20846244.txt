Murder in Harlem
{{Infobox film
| name           = Murder in Harlem
| image          = Murder in Harlem (1935 film).jpg
| image_size     = 
| caption        =  Clarence Williams (cabaret sequence)  (uncredited)
| producer       = Alice B. Russell (producer) Oscar Micheaux (producer)  (uncredited) Clarence Williams (cabaret sequence)  (uncredited)
| narrator       = 
| starring       = See below
| music          = 
| cinematography = Charles Levine
| editing        = 
| distributor    = 
| released       = 1935
| runtime        = 102 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1935 American 1921 silent film The Gunsaulus Mystery.

Basing the works on the 1913 trial of Leo Frank for the murder of Mary Phagan,  Micheaux used the detective genre to introduce different voices and conflicting accounts by his characters.

== Plot summary ==
 
An African-American man is Framed of the murder of a white woman, but a white man is found to be responsible.  

== Cast ==
*Clarence Brooks as Henry Glory
*Dorothy Van Engle as Claudia Vance
*Andrew Bishop as Anthony Brisbane
*Alec Lovejoy as Lem Hawkins
*Laura Bowman as Mrs. Epps
*Bee Freeman as The Catbird
*Alice B. Russell as Mrs. Vance
*Eunice Wilson as Singer
*Lorenzo McClane as Arthur Vance
*David Hanna as Undetermined Role
*"Slick" Chester as Detective

== Soundtrack ==
*"Harlem Rhythm Dance" (Music and lyrics by Clarence Williams)
*"Ants in My Pants" (Music and lyrics by Clarence Williams)

==See also==
* List of films in the public domain

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 