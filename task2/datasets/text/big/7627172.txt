Dr. Ehrlich's Magic Bullet
{{Infobox film
| name           = Dr. Ehrlichs Magic Bullet
| director       = William Dieterle
| image          = Dr Ehrlichs Magic Bullet 1940 poster.jpg
| image_size     = 220
| caption        = 1940 Theatrical Poster Wolfgang Reinhardt (executive producer)
| writer         = Norman Burnstine Heinz Herald John Huston
| starring       = Edward G. Robinson Ruth Gordon Otto Kruger Donald Crisp
| music          = Max Steiner
| cinematography = James Wong Howe
| editing        = Warren Low
| distributor    = Warner Bros. Pictures
| released       =  
| country        = United States
| runtime        = 103 minutes
| language       = English
}} German doctor and scientist Dr. Paul Ehrlich. The film was released by Warner Bros., with some controversy considering the subject of syphilis in a major studio release. It was nominated for an Academy Award for its original screenplay (by Norman Burnstine, Heinz Herald and John Huston), but lost to The Great McGinty.

==Plot== bureaucratic red Emil von optical microscopy. After attending a medical presentation of one Dr. Robert Koch (Albert Basserman) showing that tuberculosis is a bacterial disease, Ehrlich is able to obtain a sample of the isolated bacterium. After an intense time of research and experimentation in his own lab, paired with a portion of luck, he is able to develop a staining process for this bacterium. This result is honored by Koch and medical circles as a highly valuable contribution to diagnostics.

During his work, Dr. Ehrlich is infected with tuberculosis, a disease still known as being deadly. Therefore, Ehrlich travels with his wife Hedwig (Ruth Gordon) to Egypt for recovery and relief. There he starts to discover the properties of the human body with regard to Immunity_(medical)|immunity. This discovery helps Ehrlich and colleague Dr. von Behring to fight a diphtheria epidemic that is killing off many children in the country. The two doctors are rewarded for their efforts.
 magic bullets" - chemicals injected into the blood to fight various diseases. Ehrlichs laboratory has the help of a number of scientists like Sahachiro Hata (Wilfred Hari). The medical board, headed by Dr. Hans Wolfert (Sig Ruman), believes much of Ehrlichs work is a waste of money and resources and fight for a reduction, just as Ehrlich begins to work on a cure for syphilis. Ehrlich is financially backed by the widow of Jewish banker Georg Speyer, Franziska Speyer (Maria Ouspenskaya) and after 606 tries he finally discovers the remedy for the disease. This substance, first called "606", is now known as Arsphenamine or Salvarsan.
 cures by chemicals), who was called by the defense to denounce 606, instead states that he believes that 606 is responsible for the death of syphilis itself, the 39th death as he calls it. Ehrlich is exonerated, but the strain and stress from the trial are too much for his ill body and he dies shortly thereafter, first telling his assistants and colleagues about taking risks with regard to medicine.

==Politics and self-censorship==
The production of Dr. Ehrlichs Magic Bullet presented concerns for Warner Bros. Studios due to the political situation at home and abroad and the focus on Ehrlichs triumph over a venereal disease, syphilis. 

Ehrlich was (and still is) arguably the greatest of the many great Jewish medical doctors in history, and he is beyond question one of the greatest doctors of any background  . The Nazi regime in Germany had systematically expunged all memory of Ehrlich from public buildings and street signs and censored books referring to him. The Second World War had already begun but the United States was not yet directly involved. Jack Warner, like other Hollywood moguls, was wary of criticism of pursuing any supposedly "Jewish" agenda on the screen. A memorandum circulated by the studio bosses stated with regard to the forthcoming Ehrlich movie: "It would be a mistake to make a political propaganda picture out of a biography which could stand on its own feet." So the words "Jew" and "Jewish" went entirely unmentioned in the film. Anti-semitism in Ehrlichs life was no more than hinted at, and then only once or twice. In addition, the original version of the deathbed scene was changed so that Ehrlich no longer would refer to the Pentateuch (books of Moses in the Bible). Nevertheless, the films story writer Norman Burnside declared "There isnt a man or woman alive who isnt afraid of syphilis, and let them know that a little kike named Ehrlich tamed the scourge. And maybe they can persuade their hoodlum friends to keep their fists off Erhlichs coreligionists."

Working under the puritanical restraints of the Production Code of the Motions Pictures Producers and Distributors of America, Warner executives furthermore seriously considered not mentioning the word "syphilis" in the movie. However, Hal B. Wallis, president of the association, while advising caution, wrote to Warner Bros. that "to make a dramatic picture of the life of Dr. Ehrlich and not include this discovery   among his great achievements would be unfair to the record."

The movies title role star Edward G. Robinson, himself Jewish, was keenly aware of the increasingly desperate situation of the Jews in Germany and Europe. He personally met with Paul Ehrlichs daughter who had fled to the US and he corresponded with Ehrlichs widow, who was a refugee in Switzerland. (Robinson furthermore welcomed the opportunity to break out of the police and gangster roles in which he was in danger of being forever stereotyped.) "During the filming," Robinson later recalled with regard to his role as Ehrlich: "I kept to myself, studied the script, practiced gestures before the mirror, read about his life and times, studied pictures of the man, tried to put myself in his mental state, tried to be him."

==Cast==
* Edward G. Robinson as Dr. Paul Ehrlich
* Ruth Gordon as Hedwig Ehrlich
* Otto Kruger as Dr. Emil Von Behring
* Donald Crisp as Minister Althoff
* Maria Ouspenskaya as Franziska Speyer
* Montagu Love as Professor Hartman
* Sig Ruman as Dr. Hans Wolfert
* Donald Meek as Mittelmeyer
* Henry ONeill as Dr. Lentz
* Albert Bassermann as Dr. Robert Koch
* Edward Norris as Dr. Morgenroth Harry Davenport as Judge
* Louis Calhern as Dr. Brockdorf
* Louis Jean Heydt as Dr. Kunze
* Charles Halton as Sensenbrenner

==See also==
* List of American films of 1940
* Tuberculosis (TB)
* Diphtheria
* Side-chain theory
* Syphilis

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 