Night Games (film)
{{Infobox film
| name           = Night Games
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Roger Vadim
| producer       = 
| screenplay     =
| based on       = 
| narrator       = 
| starring       = Cindy Pickett Joanna Cassidy John Barry
| cinematography = 
| editing        = 
| studio         = Pan Pacific Golden Harvest
| released       = 1980
| runtime        = 
| country        = United States
| language       = English
| budget         = 
}} 1980 film directed by Roger Vadim. It was released in France as Jeux de Nuit. 

The film stars  Cindy Pickett as a woman still traumatised after being raped as a teenager, and is unable to have a satisfactory sex life with her husband, who eventually leaves her. Left alone in a large house, Valerie drifts into sexual fantasies, while facing danger from another potential rapist.

Joanna Cassidy also stars, as Valeries best friend. The film was released on VHS in 1989, with a running time of 104 minutes. A subsequent DVD release has a running time of only 96 minutes. 

==Cast==
* Cindy Pickett as Valerie St. John
* Joanna Cassidy as Julie Miller
* Barry Primus as Jason St. John Paul Jenkins as Sean Daniels

==Production== And God Created Woman and Barbarella (film)|Barbarella, whose respective stars Vadim also married, Brigitte Bardot and Jane Fonda.

He was going to make a vampire film starring an ex-girlfriend, Catherine Deneuve, but postponed it to make Night Games. Vadim said he was "looking for a new Vivien Leigh, about 27 years old" to star in the film. Capricorn? Pays the Feed Bills
Los Angeles Times (1923-Current File)   11 May 1978: g23.  

Part of the funding came from the Philippines and some of the movie was shot there. Tom Jones and the Green Grass of TV Golden Harvest. CHOW: ASIAS GOOD PROVIDER OF FILMS: RAYMOND CHOW RAYMOND CHOW
Thomas, Kevin. Los Angeles Times (1923-Current File)   15 July 1979: o37.  

Pickett was a relative unknown when Vadim cast her for Night Games, best known for appearing for three years on Guiding Light.  "I went away from the experience transformed," said Pickett. "Vadim has the power to make an actor feel so good about himself, and when that happens you grow. Its part of his magic." A Flock of Money for Sheppard: RODERICK MANN
Los Angeles Times (1923-Current File)   11 Oct 1979: g23.  

Pickett later recalled:
 Vadim had to fight to get me into the picture. Chow had wanted someone with long hair and a big bosom - another Bardot, presumably. And here I was with short hair and a boyish figure. Vadim had to explain that fashions had changed since the Bardot era. And in the end, Chow accepted this. INDY PICKETT: A BARDOT SHES NOT
Mann, Roderick. Los Angeles Times (1923-Current File)   27 Jan 1981: g1.   
Pickett and Vadim dated during the making of the film but the relationship ended when the film did. 

==Reception==
This film was neither a commercial nor critical success, and Pickett went on to become one of the stars of an American television series set in a hospital, St. Elsewhere, and as the title characters mother in the film Ferris Buellers Day Off.

"I was probably in every press release that year," said Pickett. " The London press was so cruel. One article ran a headline, Bardot, Deneuve, Fonda... Picket??" Pickett grapples with an issue role in Steven
Sanello, Frank. Chicago Tribune (1963-Current file)   21 May 1989: I3.  

Pickett later reflected:
 Perhaps the film wouldve done better if theyd used someone different - a Bo Derek for example. Because people did go to see Vadims latest discovery and obviously I wasnt the sort of person they expected. The film was a disappointment, too. It was supposed to be a psychological thriller, but it wound up as a soft porn film. That wasnt Vadims fault; he didnt have final cut. Many of my best scenes were cut out altogether. When I saw the finished version I knew it would do nothing for me. Nobody would walk away impressed. In fact, not many would even sit through the picture, which was a pity because my best work came at the end. I was sorry not just for myself but also for Vadim. I wanted it to ork for him.  
Pickett said she was never meant to be typed as a sex goddess. "I didnt have the body for it," she said. 
==References==
 

==External links==
* 
 
 
 
 


 