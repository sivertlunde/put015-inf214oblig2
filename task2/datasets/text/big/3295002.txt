Going Places (1974 film)
{{Infobox film
| name           = Going Places
| caption        = 
| image	=	Les Valseuses.jpg
| director       = Bertrand Blier
| producer       = Paul Claudon
| writer         = Bertrand Blier Philippe Dumarçay
| starring       = Gérard Depardieu Patrick Dewaere Miou-Miou Jeanne Moreau Isabelle Huppert
| music          = Stéphane Grappelli
| cinematography = Bruno Nuytten
| editing        = Kenout Peltier
| distributor    =
| released       = 20 March 1974
| runtime        = 113 minutes
| country        = France
| language       = French
| budget         =
}}
Going Places ( ) is a 1974 French comedy-drama film directed by Bertrand Blier, adapted from a novel by Blier, and starring Miou-Miou, Gérard Depardieu and Patrick Dewaere. The French title translates into English as "The Waltzers", a French vulgar term for the testicles.   The film had a total of 5,726,031 admissions in France where it was the 3rd highest grossing film of the year. http://www.jpbox-office.com/fichfilm.php?id=8388&affich=france 

==Summary==
Jean-Claude and Pierrot spend their days harassing women and committing petty crimes.  One day, they impulsively steal a hairdressers car and go on a joyride.  When they return the car, the hairdresser confronts them with a gun.  Attempting to flee, Pierrot is shot in the groin.  Jean-Claude disarms the hairdresser and grabs his pistol as well as his female assistant, Marie-Ange. The three flee in the hairdressers car.  Not wanting to risk arrest by going to a hospital, they break into the home of a local doctor and force him to treat Pierrots wound, then rob him of all his cash.  The trio then drive to a chop-shop outside of Paris.  Jean-Claude offers Marie-Ange to the owner as payment for his services. The chop-shop owner rapes Marie-Ange, and is disappointed to discover that she doesnt mind being used as a sex object and doesnt fight back.  Nonetheless, he agrees to sabotage the hairdressers car so that, after they return it to the hairdresser, he will have a fatal accident. They return the car and drop off Marie-Ange, then go on the run.

They take a train to the coast.  On the train, they encounter a lone mother with her baby, and sexually assault her.  They arrive at a coastal holiday town, deserted at this time of year.  They break into a local house and live there for several days.  Pierrot is worried that his sex life has been ruined due to his groin injury.

==Cast==
* Gérard Depardieu as Jean-Claude
* Patrick Dewaere as Pierrot
* Miou-Miou as Marie-Ange
* Jeanne Moreau as Jeanne Pirolle
* Brigitte Fossey as Woman in the train
* Christian Alers as Jacquelines father Henri
* Michel Peyrelon as The Doctor
* Gérard Boucaron as Carnot
* Jacques Chailleux as Jacques Pirolle
* Eva Damien as Doctors wife
* Dominique Davray as Ursula
* Isabelle Huppert as Jacqueline
* Marco Perrin as Supermarket inspector
* Jacques Rispal as Maton
* Claude Vergues as Merian
* Thierry Lhermitte as the Doorman
* Gérard Jugnot as Holyday-Maker with Family

==See also==
* Isabelle Huppert filmography

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 
 