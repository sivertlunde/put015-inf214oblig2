The Age of Innocence (1993 film)
 
{{Infobox film
| name = The Age of Innocence
| image = The_Age_Of_Innocence.jpg
| caption = Theatrical release poster
| director = Martin Scorsese
| producer = Barbara De Fina
| screenplay = Jay Cocks Martin Scorsese
| based on =  
| narrator = Joanne Woodward
| starring = Daniel Day-Lewis Michelle Pfeiffer Winona Ryder
| music = Elmer Bernstein
| cinematography = Michael Ballhaus
| editing = Thelma Schoonmaker
| distributor = Columbia Pictures
| released =  
| runtime = 139 minutes
| country = United States
| language = English
| budget = $34 million   
| gross = $32.3 million  
}} novel of the same name. The film was released by Columbia Pictures, directed by Martin Scorsese, and stars Daniel Day-Lewis, Michelle Pfeiffer, and Winona Ryder.
 Best Actress Best Adapted Best Original Best Art Direction.   

The film was dedicated to Martin Scorseses father, Luciano Charles Scorsese, who died before it was completed.

==Plot==
Newland Archer (Daniel Day Lewis) is planning to marry the respectable May Welland (Winona Ryder). Mays cousin, the Countess Ellen Olenska (Michelle Pfeiffer) has returned to New York which causes a shock in society circles. The Countess unwisely married a Polish Count, who took her fortune and mistreated her; she left him to return to New York.

Mays family is boldly and publicly standing by the Countess in the face of malicious gossip, and Archer admires it. Archer prematurely announces his engagement to May, but as he comes to know the Countess, he begins to appreciate her unconventional views on New York society and he becomes increasingly disillusioned with his new fianceé May and her innocence, lack of personal opinion, and sense of self.

After the Countess announces her intention of divorcing her husband, Archer supports her desire for freedom, but he feels compelled to act on behalf of the family and persuade the Countess to remain married. When Archer realizes that he is in love with the Countess, he abruptly leaves the next day to be reunited with May and her parents who are in Florida on vacation. Archer asks May to shorten their engagement, but May becomes suspicious and asks him if his hurry to get married is prompted by the fear that he is marrying the wrong person. Archer reassures May that he is in love with her. When back in New York, Archer calls on the Countess and admits that he is in love with her, but a telegram arrives from May announcing that her parents have pushed forward the wedding date.

After their wedding and honeymoon, Archer and May settle down to married life in New York. Over time, Archers memory of the Countess fades. When the Countess returns to New York to care for her grandmother (Miriam Margolyes), she and Archer agree to consummate their affair. But then suddenly, the Countess announces her intention to return to Europe. May throws a farewell party for the Countess, and after the guests leave, May announces to Archer that she is pregnant and that she told Ellen her news two weeks earlier.

The years pass: Archer is 57 and has been a dutiful, loving father, and a faithful husband. The Archers have had three children. When May died of infectious pneumonia, he had honestly mourned her. Archers son, Ted (Robert Sean Leonard) convinces him to travel to France. There, Ted has arranged to visit the Countess Olenska at her Paris apartment. Archer has not seen her in over 25 years. Ted confides to his father Mays deathbed confession that "... she knew we were safe with you, and always would be. Because once, when she asked you to, you gave up the thing you wanted most." Archer confesses that she never asked him. That evening outside the Countess apartment, Archer sends his son alone to visit her. While sitting outside the apartment, he thinks about their time together and gets up and walks away.

==Cast==
 
 
* Daniel Day-Lewis as Newland Archer
* Michelle Pfeiffer as Ellen Olenska
* Winona Ryder as May Welland
* Geraldine Chaplin as Mrs. Welland
* Michael Gough as Henry van der Luyden
* Richard E. Grant as Larry Lefferts
* Mary Beth Hurt as Regina Beaufort
* Robert Sean Leonard as Ted Archer
  Norman Lloyd as Mr. Letterblair 
* Miriam Margolyes as Mrs. Mingott
* Alec McCowen as Sillerton Jackson
* Siân Phillips as Mrs. Archer
* Jonathan Pryce as Rivière
* Alexis Smith as Louisa van der Luyden Stuart Wilson as Julius Beaufort
* Joanne Woodward as the narrator
 

* Domenica Cameron-Scorsese (credited as Domenica Scorsese), the daughter from director Martin Scorseses marriage to writer Julia Cameron, has a small role as Katie Blenker. Charles and cameo during the sequence when Archer meets the Countess at the Pennsylvania Terminus in Jersey City. 
* Martin Scorsese has a cameo as the "fussy bustling photographer who later takes the official wedding photographs."   
* Tamasin Day-Lewis, sister to Daniel Day-Lewis, has a cameo admiring Mays engagement ring. 

==Production==
Daniel Day-Lewis, Michelle Pfeiffer and Winona Ryder were all Martin Scorseses first choices for the lead roles.    

The Age of Innocence was filmed on location primarily in   at Rensselaer Polytechnic Institute. Formerly known as the Paine Mansion, after its completion in 1896 (then-estimated to cost one-half million dollars), it was heralded as the grandest house in all of Troy.   The scenes depicting Newland Archers parents house were filmed in the formal lounges of the McMurray Spicer Gale House of Russell Sage College, also in Troy.    Only one major set was built, for an ornate ballroom sequence at the Beaufort residence. 

The opening title sequence was created by Elaine and Saul Bass.

The famous paintings featured in the film were high-quality reproductions by Troubetzkoy Paintings Ltd. 
 fade out were inspired by Michael Powells Black Narcissus and Alfred Hitchcocks Rear Window. 

==Reception==

===Box office===
The film was not a commercial success, making a domestic total of $32.3 million from a $34 million budget. 

===Critical response===
The Age of Innocence currently holds a score of 80% on Rotten Tomatoes,  and a score of 83 on Metacritic,  indicating largely positive reviews from critics.

Vincent Canby in The New York Times wrote: "Taking The Age of Innocence, Edith Whartons sad and elegantly funny novel about New Yorks highest society in the 1870s, Martin Scorsese has made a gorgeously uncharacteristic Scorsese film...The film is the work of one of Americas handful of master craftsmen."  

Roger Ebert in the Chicago Sun-Times wrote: "The story told here is brutal and bloody, the story of a mans passion crushed, his heart defeated. Yet it is also much more, and the last scene of the film, which pulls everything together, is almost unbearably poignant."  He then added the film to his "Great Movies" collection, and defined the film as "one of Scorseses greatest films". 

Peter Travers in Rolling Stone wrote: "A superlative cast catches Whartons urgency. Ryder, at her loveliest, finds the guile in the girlish May – shell use any ruse that will help her hold on to Archer. Day-Lewis is smashing as the man caught between his emotions and the social ethic. Not since Olivier in Wuthering Heights has an actor matched piercing intelligence with such imposing good looks and physical grace. Pfeiffer gives the performance of a lifetime as the outcast countess." 
 Scorsese shows he can flex an entirely different set of muscles and still make a great movie. 

Todd McCarthy in Variety (magazine)|Variety wrote: "For sophisticated viewers with a taste for literary adaptations and visits to the past, there is a great deal here to savor....Day-Lewis cuts an impressive figure as Newland... The two principal female roles are superbly filled.... Scorsese brings great energy to what could have been a very static story, although his style is more restrained and less elaborate than usual." 

Rita Kempley, also in the Washington Post, wrote: "Perhaps it shouldnt come as such a grand surprise that he   is as deft at exploring the nuances of Edwardian manners as he is the laws of modern-day machismo." 

 s most poignantly moving film." 

But not all the critics had positive remarks. Marc Savlov in the Austin Chronicle wrote: "At two hours and 13 minutes, Scorsese has allowed himself enough time to follow Whartons book to the letter, and also enough time to include long stretches of painfully wearisome society functions and banter. As a period piece, its a joy to behold, but with such an indecisive little newt of a protagonist, its just hard to give a damn what happens." 

===Accolades=== Best Supporting Best Adapted Best Original Best Art Direction (Dante Ferretti, Robert J. Franco). 
 Best Motion Best Director Best Actress – Motion Picture Drama (Michelle Pfeiffer).
 Best Cinematography Best Production Design (Dante Ferretti).

In addition to her Academy Award and BAFTA Award nominations and Golden Globe Award win, Winona Ryder won the National Board of Review Award for Best Supporting Actress and the Southeastern Film Critics Association Award for Best Supporting Actress.

In addition to his Academy Award and Golden Globe Award nominations, Martin Scorsese won the National Board of Review Award for Best Director and the Elvira Notari Prize at the Venice Film Festival (shared with Michelle Pfeiffer), as well as a nomination for the Directors Guild of America Award for Outstanding Directing - Feature Film.
 Grammy Award for Best Instrumental Composition Written for a Motion Picture or Television.   

{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Awarding Body
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee
! style="background:#B0C4DE;" | Result
|-
| rowspan="5" | Academy Awards Best Supporting Actress
| Winona Ryder
|  
|- Best Adapted Screenplay 
| Jay Cocks, Martin Scorsese
|  
|- Best Original Score
| Elmer Bernstein
|  
|- Best Art Direction
| Dante Ferretti, Robert J. Franco
|  
|- Best Costume Design
| Gabriella Pescucci
|  
|-
| rowspan="4" | British Academy Film Awards Best Actress in a Supporting Role
| Miriam Margolyes
|  
|- Best Actress in a Supporting Role
| Winona Ryder
|  
|- Best Cinematography
| Michael Ballhaus
|  
|- Best Production Design
| Dante Ferretti
|  
|-
| Directors Guild of America Awards Outstanding Directorial Achievement in Motion Pictures
| Martin Scorsese
|  
|-
| rowspan="4" | Golden Globe Awards Best Motion Picture – Drama
| 
|  
|- Best Director – Motion Picture
| Martin Scorsese
|  
|- Best Actress – Motion Picture Drama
| Michelle Pfeiffer
|  
|- Best Supporting Actress – Motion Picture
| Winona Ryder
|  
|-
| Grammy Awards Grammy Award for Best Instrumental Composition Written for a Motion Picture or Television
| Elmer Bernstein
|  
|-
| rowspan="2" | National Board of Review Best Director
| Martin Scorsese
|  
|- Best Supporting Actress
| Winona Ryder
|  
|-
| Southeastern Film Critics Association
| Southeastern Film Critics Association Award for Best Supporting Actress
| Winona Ryder
|  
|-
| Venice Film Festival
| Elvira Notari Prize
| Martin Scorsese, Michelle Pfeiffer
|  
|}

==Soundtrack==
{{Infobox album  
| Name        = The Age of Innocence
| Type        = Film
| Artist      = Elmer Bernstein
| Cover       = 
| Alt         = 
| Released    = September 14, 1993
| Recorded    = June 1993 The Hit Factory, New York City, New York
| Genre       = Film score
| Length      = 01:04:25
| Label       = Epic Soundtrax
| Producer    = Elmer Bernstein, Emilie A. Bernstein
| Last album  = Mad Dog and Glory (1993)
| This album  = The Age of Innocence (1993) The Good Son (1993)
}} musical score Cape Fear (1991).

;Track listing
{{Track listing
| title1       = The Age of Innocence
| length1      = 4:37
| title2       = At the Opera
| length2      = 3:11
| note2        = A portion from the opera Faust (opera)|Faust; Charles Gounod to a libretto by Jules Barbier and Michel Carré
| title3       = Radetzky March
| length3      = 2:16 Berlin Radio Symphony Orchestra Emperor Waltz, Op. 437/Tales from the Vienna Woods
| length4      = 2:26
| note4        = featuring London Philharmonic Orchestra
| title5       = Mrs. Mingott
| length5      = 1:42
| title6       = Dangerous Conversation
| length6      = 2:13
| title7       = Slighted
| length7      = 0:58
| title8       = Van Der Luydens
| length8      = 2:17
| title9       = First Visit
| length9      = 2:28
| title10      = Roses Montage
| length10     = 1:19
| title11      = Ellens Letter
| length11     = 2:05
| title12      = Archers Books
| length12     = 2:08
| title13      = Mrs. Mingotts Help
| length13     = 3:49
| title14      = Archer Pleads
| length14     = 1:48
| title15      = Passage of Time
| length15     = 2:44
| title16      = Archery
| length16     = 1:28
| title17      = Ellen at the Store
| length17     = 2:14
| title18      = Blenker Farm
| length18     = 2:38
| title19      = Boston Common
| length19     = 0:53
| title20      = Parker House
| length20     = 1:16
| title21      = Pick up Ellen
| length21     = 2:12
| title22      = Conversation with Letterblair
| length22     = 2:33
| title23      = Archer Leaves
| length23     = 1:03
| title24      = Farewell Dinner
| length24     = 2:04
| title25      = Ellen Leaves
| length25     = 2:42
| title26      = In Paris
| length26     = 1:12
| title27      = Ellens House
| length27     = 0:48
| title28      = Madame Olenska
| length28     = 2:17
| title29      = End Credits
| length29     = 5:04
}}

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 