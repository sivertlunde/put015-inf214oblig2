Burned at the Stake
 
{{Infobox film
| name           = Burned at the Stake
| image          = 
| image_size     = 
| caption        = 
| director       = Bert I. Gordon
| writer         = 
| narrator       = 
| starring       = Susan Swift
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1981
| runtime        = 88 min.
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Burned at the Stake is a 1981 film directed by Bert I. Gordon. It stars Susan Swift and Albert Salmi.  

==Plot==

In the Salem of 1692, a group of witches are burned at the stake. Now, in the 1980s, a witch comes back from the dead, possesses one of her descendants, and goes hunting for the occupants of the town to avenge her death.

==Cast==
*Susan Swift as Loreen Graham / Ann Putnam 
*Albert Salmi as Captain Billingham
*Guy Stockwell as Dr. Grossinger
*Tisha Sterling as Karen Graham
*Beverly Ross as Merlina

==References==
 

==External links==
* 

 

 
 
 
 