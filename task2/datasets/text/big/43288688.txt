The Magic Mountain (1982 film)
 
 
{{Infobox film
| name           = The Magic Mountain 
| image          = The Magic Mountain (1982 film).jpg
| image_size     = 
| caption        = 
| director       = Hans W. Geißendörfer
| producer       = 
| writer         =   
| narrator       = 
| starring       = Christoph Eichhorn Marie-France Pisier Rod Steiger Charles Aznavour 
| music          =   Jürgen Knieper
| editing        = 
| cinematography = Michael Ballhaus
| studio         =
| distributor    = 
| released       = 1982
| runtime        = 
| country        =   German 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
The Magic Mountain ( ) is a 1982 German film adaptation of Thomas Manns 1924 novel The Magic Mountain directed by Hans W. Geißendörfer.

==Cast==

* Christoph Eichhorn : Hans Castorp 
* Marie-France Pisier : Claudia Chauchat 
* Flavio Bucci : Ludovico Settembrini 
* Alexander Radszun : Joachim Ziemssen 
* Hans Christian Blech : Hofrat Behrens 
* Charles Aznavour : Naphta 
* Rod Steiger : Mynheer Peperkorn
* Helmut Griem : James Tienappel
* Margot Hielscher: Karoline Stöhr
* Gudrun Gabriel: Fräulein Marusja
* Irm Hermann: Fräulein Engelhart 
* Ann Zacharias: Fräulein Elly
* Rolf Zacher: Herr Wehsal 
* Kurt Raab: Dr. Edhin Krokowski 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 
 

 