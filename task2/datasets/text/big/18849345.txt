Malaikkallan
{{Infobox film
| name = Malaikkallan மலைக்கள்ளன்
| image = Malaikkallan Poster.jpg
| caption = Film poster
| director = S. M. Sriramulu Naidu 
| writer = Karunanidhi |Mu. Karunanidhi
| screenplay = Karunanidhi |Mu. Karunanidhi
| story = Namakkal Kavignar Va. Ramalingam Pillai
| starring = M. G. Ramachandran Bhanumathi Ramakrishna|P. Bhanumathi Sriram M. G. Chakrapani
| producer = S. M. Sriramulu Naidu
| music = S. M. Subbaiah Naidu
| cinematography = Sailen Bose
| editing = Velusami
| studio = Pakshiraja Studios
| distributor = Pakshiraja Studios
| released = 22 July 1954
| runtime =
| country = India Tamil
| budget =
| gross =   90 lakhs
}}

 Malaikkallan  (aka Malaikallan) ( ,  ) is a Tamil language film starring M. G. Ramachandran in the lead role. The film was released on 22 July 1954, and was "an astounding success".  It was the first Tamil film to win a Presidents Silver Medal.  

==Production== The Mark of Zorro, written by Namakkal Kavignar Va. Ramalingam Pillai (Namakkal Kavignar). A well-known writer, poet, artist and freedom fighter, he was nominated as the Poet Laureate of the Madras Government in 1949. Malaikkallan had been prescribed as the non-detailed text for the high school curriculum in the early 50s, and the story had become very popular.

S.M. Sriramulu Naidu of Pakshiraja Studio in Coimbatore secured the rights to the story and decided to make a movie of it, in 6 languages- Tamil (Malaikkallan/ MGR), Telugu (Aggi Ramudu/ N. T. Rama Rao), Malayalam (Thaskaraveeran/ Sathyaneshan Nadar|Sathyan), Kannada (Bettada Kalla/ Kalyan Kumar), Hindi (Azaad (1955 film)|Azaad/ Dilip Kumar) and Sinhalese (Surasena). Sriramulu Naidu booked Bhanumati to play the role of heroine, Poonkothai (Tamil) and Saradha (Telugu).

Except Azaad that had music by C. Ramchandra, S. M. Subbaiah Naidu composed music for the movie in all the other languages.

During filming, a leopard went missing on the sets. A. Pattabhiraman, director of Raja Theatre locked himself in a cage saying, "This is the last place where the leopard will find me!" 

==Plot==
Vijayapuram is a beautiful hillside hamlet appears serene and restful to a casual passerby. But the happenings there are far from tranquil. Dacoities, burglaries and even kidnappings seem to be commonplace occurrences. One established perpetuator of at least some of the crimes is Kaathavarayan, his secret accomplices being some well-known public figures like the rich young wastrel Veerarajan and the Kuttipatti Zamindar.

The other dacoit is apparently the mysterious Malaikkallan. Legends are galore on his fabulous wealth, awe-inspiring exploits, contempt for the unprincipled rich, concern for the poor and needy indeed he seems to be running a veritable empire in some hidden hillock no one actually seen him.

There is also the wealthy middle-aged bachelor Abdul Kareem, who seems to disappear at regular intervals from Vijayapuram, claiming business calls at far-off places. In this hotbed of intrigue and suspicion blooms an innocent rose Poonkothai, daughter of the upright Sokkesa Mudaliar. Veerarajan is the cousin of Poonkothai and desires to marry her, but his evil reputation ensures the impossibility of such an alliance. Having lost her mother at an early age, Poonkothai is brought up by her widowed aunt Kamakshi Ammaal. Kamakshi Ammals only son Kumaraveeran went missing many years back.

Faced by stringent public criticism for their failure to tackle the audacious crimes, Sub-Inspector Arumugam arrives in Vijayapuram. But his assistant Constable Karuppiah is a bungling coward and is more a hindrance than a help in his investigations. It is at this juncture that one night when mudaliar is away, Poonkothai is kidnapped. The happenings of that eerie night keep the village tongues wagging for many days thereafter. Two sidekicks of Kathavarayan are found tied and hanging upside down, and a piece of Poonkothai’s jewellery is recovered from them. Kamakshi AmmaaL is found tied-up and unconscious, and a mysterious errand-boy hands over to the attending doctor a herb that revives her at once. Poonkothai is said to be in the custody of Malaikkallan, who has cleverly waylaid Kathavarayan’s men and taken away Poonkothai. Kathavarayan faces the ire and ridicule of Veerarajan at the behest of whom he had engineered Poonkothai’s kidnapping. Goaded by this humiliation, he now sends his men far and wide in search of Poonkothai. Meanwhile Poonkothai is safe in the magnificent hideout of Malaikkallan perceiving his genuine concern for the downtrodden and the reverence with which he is held by his people, her contempt and mistrust turn gradually into admiration and leads to love.

Several confounding twists and turns later the truant pieces of the puzzle fall in place. Kathavarayan and Veerarajan get their well-deserved comeuppance. Malaikkallan and Abdul Kareem both turn out to be the same person who is the long missing Kumaraveeran. All is well that ends with the happy marriage of Poonkothai and Kumaraveeran.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || Kumaraveeran / Abdul Kareem
|-
| Bhanumathi Ramakrishna|P. Bhanumathi || Poonkothai
|-
| Sriram || Veerarajan
|-
| M. G. Chakrapani || Sub-Inspector
|-
| P. S. Gnanam || Kamakshi
|-
| D. Balasubramaniyam || Sokkesa Mudaliar
|-
| T. S. Durairaj || Karuppiah
|-
| Surabhi Balasaraswathi || Janaki
|-
| V. M. Ezhumalai || Sadaiyan
|- Sandhya || Parvathi alias Chinni
|-
| E. R. Sahadevan || Kathavarayan
|}

==Crew==
*Producer: S. M. Sriramulu Naidu
*Production Company: Pakshiraja Studios
*Director: S. M. Sriramulu Naidu
*Music: S. M. Subbaiah Naidu
*Lyrics: Namakkal Kavignar Va. Ramalingam Pillai, Namakkal R. Balasubramaniam, Tanjai N. Ramiah Doss, Makkalanban & Kovai A. Ayyamuthu
*Story: Namakkal Kavignar Va. Ramalingam Pillai
*Dialogue: M. Karunanidhi
*Art Director: Chelliah
*Editing: Velusami
*Choreography: Muthusami Pillai & T. C. Thangaraj
*Cinematography: Sailen Bose
*Stunt: R. N. Nambiar
*Dance: Sai Subbulakshmi

==Soundtrack==
{{tracklist	
| headline     = Tracklist
| extra_column = Singer(s)
| lyrics_credits = yes
| total_length = 
| title1       = Yethanai kaalamthan yematruvar
| extra1       = T. M. Soundararajan
| lyrics1      =
| length1      = 
| title2       = Neeli magan nee allava
| extra2       = P. A. Periyanayaki
| lyrics2      = 
| length2      = 
| title3       = O amma o ayya
| extra3       = P. A. Periyanayaki
| lyrics3      = 
| length3      = 
| title4       = Unnai azhaithathu yaaro
| extra4       = Bhanumathi Ramakrishna|P. Bhanumathi
| lyrics4      = 
| length4      = 
| title5       = Pengale Ulangalile
| extra5       = Bhanumathi Ramakrishna|P. Bhanumathi
| lyrics5      = 
| length5      = 
| title6       = Nalla sagunam nokki
| extra6       = Bhanumathi Ramakrishna|P. Bhanumathi
| lyrics6      = 
| length6      =
| title7       = Naane inba roja
| extra7       = Bhanumathi Ramakrishna|P. Bhanumathi
| lyrics7      =
| length7      = 
| title8       = Naalai
| extra8       = Bhanumathi Ramakrishna|P. Bhanumathi
| lyrics8      = 
| length8      = 
| title9       = Thamizhan endroru inam
| extra9       = T. M. Soundararajan
| lyrics9      =
| length9      = 
}}

==Box office==
*The film claims to have grossed $3,80,000 at the box office and was graded as the first All-Time Blockbuster in M. G. Ramachandrans career.
*This film released in 6 languages.
*This film established M. G. Ramachandran as another superstar.

==Trivia==
*The song Eththanai Kaalam Thaan Yemaatruvaar became so popular that MGR then decided to insert at least one such philosophical song in his future movies.
*When Sriramulu Naidu was still undecided on the actor to play the lead role in Tamil, it was Subbiah Naidu who urged him to pick MGR.

==Awards==
* The film won National Film Award for Best Feature Film in Tamil - Presidents Silver Medal in 1954 at 2nd National Film Awards.   

==References==
 

==Additional sources==
* , "About Homage"
* , "Malaikallan for Diwali"
* , "Rajkiran revives Malaikallan"
* 
* 

==External links==
* 

 

 
 
 
 
 
 