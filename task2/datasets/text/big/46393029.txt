The Truants (film)
{{Infobox film
| name           = The Truants
| image          =
| caption        =
| director       = Sinclair Hill
| producer       =
| writer         = A.E.W. Mason (novel) Kinchen Wood George Bellamy br>Lewis Gilbert
| cinematography =
| editing        =
| studio         = Stoll Pictures
| distributor    = Stoll Pictures
| released       =  
| runtime        =
| country        = United Kingdom
| awards         =
| language       = Silent English intertitles
| budget         =
}} silent drama George Bellamy The Truants by A.E.W. Mason.  It was made by Britains largest film company of the era Stoll Pictures, The films sets were designed by art director Walter Murton.

==Synopsis==
An officer deserts from the French Foreign Legion to return home to his wifes assistance.

==Cast==
*  Joan Morgan as  Millie Stretton  George Bellamy as Sir John Stretton  Lewis Gilbert as Captain Taverney
* Phillip Simmons as Tony Stretton 
* Lawford Davidson as Lionel Callam  Robert English as John Mudge

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 

 