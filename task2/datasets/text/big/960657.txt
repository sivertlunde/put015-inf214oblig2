The Life and Death of Colonel Blimp
 
 
{{Infobox film
| name           = The Life and Death of Colonel Blimp
| image          = Colonel Blimp poster.jpg
| caption        = Cinema poster
| director       = Michael Powell Emeric Pressburger
| producer       = Michael Powell Emeric Pressburger
| writer         = Michael Powell Emeric Pressburger
| starring       =Deborah Kerr Roger Livesey Anton Walbrook Allan Gray Georges Perinal
| editing        = John Seabourne Sr. The Archers
| distributor    = General Film Distributors (UK) United Artists (US)
| released       =   (UK)
| runtime        = 163 minutes
| country        = United Kingdom
| language       = English
| budget         = Pound sterling|£200,000 gross = $275,472 (US) Sarah Street, Transatlantic Crossings: British Feature Films in the USA, Continuum, 2002 p 97 
}} war film British film Michael Powell The Archers. David Low but the story itself is original. The film is renowned for its Technicolor cinematography.

== Plot == Home Guard during the Second World War. Before a training exercise, he is captured in a Turkish bath by soldiers led by Lieutenant "Spud" Wilson, who has struck pre-emptively. He ignores Candys outraged protests that "War starts at midnight!" They scuffle and fall into a bathing pool.
 flashback ensues.

Boer War  Boer War. He has received a letter from Edith Hunter (Deborah Kerr), who is working in Berlin. She complains that a German named Kaunitz is spreading anti-British propaganda, and she wants the British embassy to do something about it. When Candy brings this to his superiors attention, they refuse him permission to intervene, but he decides to act anyway.

In Berlin, Candy and Edith go to a café, where he confronts Kaunitz. Provoked, Candy inadvertently insults the Imperial German Army officer corps. The Germans insist he fight a  ). Candy and Theo become friends while recuperating from their wounds in the same nursing home. Edith visits them both regularly and, although it is implied that she has feelings for Clive,  she becomes engaged to Theo. Candy is delighted, but soon realises to his consternation that he loves her himself.

First World War 
Candy, now a Brigadier General, believes that the Allies won the First World War because "right is might". While in France, he meets nurse Barbara Wynne (Kerr again). She bears a striking resemblance to Edith. Back in England, he courts and marries her despite their twenty-year age difference.

Candy tracks Theo down at a prisoner of war camp in England. Candy greets him as if nothing has changed, but Theo snubs him. Later, about to be repatriated to Germany, Theo apologises and accepts an invitation to Clives house. He remains sceptical that his country will be treated fairly.

Barbara dies between the world wars, and Candy retires in 1935.

Second World War 
In 1939, Theo relates to an immigration official how he was estranged from his children when they became Nazis. Before the war, he had refused to move to England when Edith wanted to; by the time he was ready, she had died. Candy vouches for Theo.
 MTC driver, Angela "Johnny" Cannon (Kerr again), personally chosen by the Englishman; Theo is struck by her resemblance to Barbara and Edith.
 retreat from the Nazis: his talk is cancelled at the last minute. Theo urges his friend to accept the need to fight and win by whatever means are necessary, because the consequences of losing are so dire.
 Home Guard. Candys energy and connections are instrumental in building up the Home Guard. His house is bombed in the Blitz and is replaced by an emergency water supply cistern. He moves to his club, where he relaxes in a Turkish bath before a training exercise he has arranged.

The film has now come full circle. The brash young lieutenant who captures Candy is in fact Angelas boyfriend, who used her to learn about Candys plans and location. She tries to warn Candy, but is too late.

Afterward, Theo and Angela find Candy sitting across the street from where his house once stood. He recalls that after being given a severe dressing down by his superior for causing the diplomatic incident, he had declined the mans invitation to dinner, and had often regretted doing so. He tells Angela to invite her boyfriend to dine with him.

Years before, Clive had promised Barbara that he would "never change" until his house is flooded and "this is a lake". Seeing the cistern, he realises that "here is the lake and I still havent changed". The film ends with Candy saluting the new guard as it passes by.

== Cast ==
{| width="100%"
|-
| width="50%" valign="top" |
*Roger Livesey as Clive Candy
*Deborah Kerr as Edith Hunter / Barbara Wynne / Angela "Johnny" Cannon
*Anton Walbrook as Theo Kretschmar-Schuldorff
*Ursula Jeans as Frau von Kalteneck
*James McKechnie as Spud Wilson
*David Hutcheson as Hoppy
*Frith Banbury as Baby-Face Fitzroy
*Muriel Aked as Aunt Margaret
*John Laurie as Murdoch
*Neville Mapp as Stuffy Graves
*Vincent Holman as Club porter (1942)
*Spencer Trevor as Period Blimp
*Roland Culver as Colonel Betteridge
*James Knight as Club porter (1902)
*Dennis Arundell as Café orchestra leader
*David Ward as Kaunitz
*Valentine Dyall as von Schönborn
| width="50%" valign="top" |
*A. E. Matthews as President of Tribunal
*Carl Jaffe as von Reumann
*Albert Lieven as von Ritter
*Eric Maturin as Colonel Goodhead
*Robert Harris as Embassy Secretary
*Arthur Wontner as Embassy Counsellor
*Theodore Zichy as Colonel Borg
*Jane Millican as Nurse Erna
*Reginald Tate as van Zijl
*Captain W. Barrett as The Texan
*Corporal Thomas Palmer as The Sergeant
*Yvonne Andre as The Nun
*Marjorie Gresley as The Matron
*Felix Aylmer as The Bishop
*Helen Debroy as Mrs.Wynne
*Norman Pierce as Mr. Wynne
*Harry Welchman as Major Davies
*Edward Cooper as BBC Official
|}

Cast notes: A Matter of Life and Death (US: Stairway to Heaven, 1946). 

== Production == David Low but from a scene cut from their previous film, One of Our Aircraft Is Missing, in which an elderly member of the crew tells a younger one, "You dont know what its like to be old." Powell has stated that the idea was actually suggested by David Lean (then an film editor|editor) who, when removing the scene from the film, mentioned that the premise of the conversation was worthy of a film in its own right. 
 49th Parallel and The Volunteer) to play Candy however the Ministry of Information refused to release Olivier, who was serving in the Fleet Air Arm, from active service telling Powell and Pressburger, "...we advise you not to make it and you cant have Laurence Olivier because hes in the Fleet Air Arm and were not going to release him to play your Colonel Blimp". 

Powell wanted Wendy Hiller to play Kerrs parts but she pulled out due to pregnancy. The character of Frau von Kalteneck, a friend of Theo Kretschmar-Schuldorff, was played by Roger Liveseys wife Ursula Jeans; although they often appeared on stage together this was their only appearance together in a film.
 Ministry of Information and War Office officials had viewed a rough cut, objections were withdrawn in May 1943. Churchills disapproval remained, however, and at his insistence an export ban, much exploited in advertising by the British distributors, remained in place until August of that year. 
 Denton Hall in Yorkshire. Filming was made difficult by the wartime shortages and by Churchills objections leading to a ban on the production crew having access to any military personnel or equipment. But they still managed to "find" quite a few Army vehicles and plenty of uniforms.

Michael Powell once said of The Life and Death of Colonel Blimp that it is  

At other times he also pointed out that the designer was German, and the leads included Austrian and Scottish actors. 

== Original release and contemporary reception == Lady Margaret 49th Parallel, the latter of which was also made during the war.

The film provoked an extremist pamphlet,  , by "right-wing sociologists E. W. and M. M. Robson", members of the obscure Sidneyan Society:
 

Due to the British governments disapproval of the film, it was not released in the United States until 1945 and then in a modified form, as The Adventures of Colonel Blimp or simply Colonel Blimp. The original cut was 163&nbsp;minutes. It was reduced to a 150-minute version, then later to 90&nbsp;minutes for television. One of the crucial changes made to the shortened versions was the removal of the films flashback structure. 

==Restorations== Kevin Macdonald Carlton Region 2 DVD featurette, considered Blimp the best of his and Powells works.
 The Red Shoes. The fundraising was spearheaded by Martin Scorsese and his long-time editor, Thelma Schoonmaker, also Michael Powells widow. Restoration work was completed by the Academy Film Archive in association with the BFI, ITV Studios Global Entertainment Ltd. (the current copyright holders), and The Film Foundation, with funding provided by The Material World Charitable Foundation, the Louis B. Mayer Foundation, Cinema per Roma Foundation, and The Film Foundation. The restored version premièred in New York on 6 November 2011, followed by a screening in London on 30 November.

The restored film is being shown around the world  and was released on DVD & Blu-ray on 22 October 2012. 

== Reputation and analysis ==

Although the film is strongly pro-British, it is a satire on the British Army, especially its leadership. It suggests that Britain faced the option of following traditional notions of honourable warfare or to "fight dirty" in the face of such an evil enemy as Nazi Germany.   There is also a certain similarity between Candy and Churchill and some historians have suggested that Churchill may have wanted the production stopped because he had mistaken the film for a parody of himself (he had himself served in the Boer War and the First World War).   Churchills exact reasons remain unclear, but he was acting only on a description of the planned film from his staff, not on a viewing of the film itself.

Since the highly successful re-release of the film in the 1980s, The Life and Death of Colonel Blimp has been re-evaluated.  The film is praised for its dazzling Technicolor cinematography (which, with later films such as   saw the film as addressing "what it means to be English", and praised it for the bravery of taking a "longer view of history" in 1943.  Anthony Lane of the New Yorker said in 1995 that the film "may be the greatest English film ever made, not least because it looks so closely at the incurable condition of being English". 

The film currently has a rating of 95% on Rotten Tomatoes.

==See also==
* BFI Top 100 British films

== References ==

;Notes
 

;Bibliography
 
*Chapman, James. " " Historical Journal of Film, Radio and Television 03/95 15(1) pp.&nbsp;19–36.
* Christie, Ian. "The Colonel Blimp File." Sight and Sound, 48. 1978
:Includes the contents of Public Record Office file on the film
* Christie, Ian. The Life and Death of Colonel Blimp (script) by Michael Powell & Emeric Pressburger. London: Faber & Faber, 1994. ISBN 0-571-14355-5.
:Includes the contents of Public Record Office file on the film, memos to & from Churchill and the script showing the difference between the original and final versions
* Kennedy, A.L. The Life and Death of Colonel Blimp. London: BFI Film Classics, 1997. ISBN 0-85170-568-5.
*  , 1986. ISBN 0-434-59945-X.
* Powell, Michael. Million Dollar Movie. London: Heinemann, 1992. ISBN 0-434-59947-6.
* Vermilye, Jerry. The Great British Films. Secaucus, NJ: Citadel Press, 1978. pp66–68. ISBN 0-8065-0661-X.
 

== External links ==
*  
*  
*  
*  
*  . Full synopsis and film stills (and clips viewable from UK libraries).
*  
*   at the Powell & Pressburger Appreciation Society Ian Christie.]

;DVD reviews Carlton DVD
:* 
:* 
 Warner Home Vidéo /  
:*  by John White at DVD Times (UK)

:Region 1 USA&nbsp;– Criterion Collection
:* 
:* 
:* 

;DVD comparisons
*  comparison of Carlton & Criterion releases
*  comparison of Carlton & Criterion releases

 

 
 
 
 
 
 
 
 
 
 
 
 
 