I Am David (film)
{{Infobox film
| name           = I Am David
| image          = I Am David.jpg
| caption        = Theatrical release poster
| director       = Paul Feig
| producer       = Davina Belling Lauren Levine Clive Parsons
| screenplay     = Paul Feig
| based on       =  
| starring       = Jim Caviezel Ben Tibber Joan Plowright
| music          = Stewart Copeland
| cinematography = Roman Osin
| editing        = Steven Weisberg
| studio         = Walden Media Film and General Lionsgate
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $7 million 
| gross          = $292,376
}}

I Am David is a 2003  film directed by Paul Feig. It is based on the novel I Am David (originally published in the USA under the name "North to Freedom") by Anne Holm. The film was produced by Walden Media and Lions Gate Entertainment.

==Plot== Stalinist labor camp in Bulgaria where he has spent his entire life. He sets out on a risky journey to Denmark, initially believing he is on an important mission to deliver a letter, but eventually discovering that the "mission" was to reunite him with his mother, of whom he only has faint memories. Along his journey, he faces danger, fear, loneliness, hunger, and encounters various people.

Johannes (Jim Caviezel), his friend and mentor in the camp, who prepares him for escape, is killed by a guard, leaving David to face escape on his own. David is helped by a guard to escape, who gives him a compass and tells him he must go southwest to Greece, take a boat to Italy and finally go north to Denmark, a peaceful and neutral country. Since David was locked in a camp all his life, he has repressed feelings and trusts no one, and so feels lost and disoriented in the world.

Along his journey, though he is mistreated by some people, he is well-treated by others. Gradually he learns that some people can be trusted, and to open up and experience his own feelings.  Finally, with the help of decent people whom he has learned to trust, David is reunited with his mother in Denmark.

==Reception==
The film generally received negative reviews from critics. Based on 34 reviews collected by the film review aggregator Rotten Tomatoes, 38% of critics gave I Am David a positive review, with an average rating of 5.2/10.  Roger Ebert of the Chicago Sun-Times wrote: "I couldnt believe a moment of it, and never identified with little David." 

The film grossed $288,552 domestically in 226 theaters. In the rest of the world, the film grossed $3,824. 

==Awards==
The film won several awards in 2003, including the Crystal Heart Award in the Heartland Film Festival, the Queens Festivals Best Feature Film prize, and Best Film and Most Promising Actor for Ben Tibber. 

==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 