Shape of the Moon
 
{{Infobox film
| title          = Shape of the Moon
| image          = 
| director       = Leonard Retel Helmrich
| producer       = Scarabeefilms
| writer         = Hetty Naaijkens-Retel Helmich Leonard Retel Helmrich
| starring       = 
| music          = 
| editor         = 
| cinematography = Leonard Retel Helmrich
| distributor    = Cinema Delicatessen
| released       =  
| runtime        = 109 minutes
| language       = Indonesian
| country        = Netherlands
| budget         = 
}}
Shape of the Moon ( ) is a Dutch/Indonesian documentary film from 2004 directed by Leonard Retel Helmrich. The documentary released on 24 November 2004 as opening film of IDFA (International Documentary Festival Amsterdam).

The documentary is the continuation of Eye of the Day and follows again the family Sjamsuddin, consisting of three generations living in the slums of Jakarta, Indonesia.

==Writers==
*Leonard Retel Helmrich
*Hetty Naaijkens-Retel Helmich

==Awards== Amsterdam International Documentary Film Festival
*Sundance World Documentary Award for Best Documentary - Sundance Film Festival

==External links==
*  

 
 
{{Succession box
| title= 
| years=2005
| before=n/a
| after=In the Pit}}
 

 
 
 
 
 
 
 
 

 
 