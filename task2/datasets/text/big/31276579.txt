Helen (film)
 
{{Infobox film
| name           = Helen
| image          = Helen 2009 movieposter.jpg
| alt            =  
| caption        = Movie poster
| director       = Sandra Nettelbeck
| producer       = Christine Haebler Judy Tossell
| screenplay     = Sandra Nettelbeck
| starring       = Ashley Judd Goran Višnjić Lauren Lee Smith
| music          = James Edward Barker Tim Despic
| cinematography = Michael Bertl
| editing        = Barry Egan
| studio         = 
| distributor    = E1 Entertainment Distribution
| released       =  
| runtime        = 120 minutes
| country        = United States
| language       = English
| budget         = $8 million
| gross          = 
}}

Helen is a 2009 American drama film starring Ashley Judd and directed by Sandra Nettelbeck. It follows a professor (Judd) who overcomes severe depression after a massive breakdown, with the help of new friend Matilda (Smith). Filming took place late 2007 in Vancouver.

==Cast==
* Ashley Judd as Helen Leonard, a college professor who is married to David, the ex-wife of Frank and mother of Julie.
* Goran Višnjić as David Leonard, Helens husband and Julies stepfather.
* Lauren Lee Smith as Mathilda, a young woman who Helen befriends during her treatment. 
* Alexia Fast as Julie, Helen and Franks daughter and Davids stepdaughter.
* Alberta Watson as Dr. Sherman
* Leah Cairns as Susanna
* David Hewlett as Frank, Julies biological father and Helens ex-husband. It is later revealed that Frank has been aware of Helens fragile mental state and her illness for quite some time which presumably later caused their marriage to fail.
* David Nykl as John
* Chelah Horsdal as Kara
* Conrad Coates as Stephen

==Release==
The film was released at the Sundance Film Festival in January 2009, and later at the Atlantic Film Festival in Canada. Lauren Lee Smith won the award for Best Supporting Actress at the 2010 Leo Awards, beating co-star Alexia Fast. It was nominated for Best Feature and Production Design. Helen was released to DVD in August 2010. It currently holds a score of 44% on Rotten Tomatoes, based on 9 reviews.

==External links==
*  
*  
*  
*  
*  

 

 
 
 
 