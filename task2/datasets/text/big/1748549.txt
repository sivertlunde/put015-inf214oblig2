The Rookie (1990 film)
{{Infobox film
| name           = The Rookie
| image          = The Rookie.jpg
| caption        = Theatrical release poster 
| alt            = A black poster with three horizontal red lines. Above are the faces of two serious-looking men. The man on the left, is young and black-haired. The man on the right, is older and lighter-haired. The right sides of their faces are invisible because of darkness. Below that in bold white letters are the lines: "The Rookie", "Clint Eastwood" and "Charlie Sheen". To the left of this text is a closeup of a revolver, and to the right and below are police cars, an aircraft, and a fiery explosion. The film credits appear at the bottom, with a line in bold white letters that reads: "Starts Friday December 7th Everywhere".
| director       = Clint Eastwood
| writer         = Scott Spiegel Boaz Yakin
| starring       = Clint Eastwood Charlie Sheen Raúl Juliá Sônia Braga Tom Skerritt Lara Flynn Boyle
| producer       = Howard G. Kazanjian Steven Siebert David Valdes
| cinematography = Jack N. Green
| editing        = Joel Cox
| studio         = Malpaso Productions
| distributor    = Warner Bros.
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $21,633,874 
| music          = Lennie Niehaus
}}

The Rookie is a 1990 buddy cop film directed by Clint Eastwood and produced by Howard G. Kazanjian, Steven Siebert and David Valdes. It was written from a screenplay conceived by Boaz Yakin and Scott Spiegel. The film stars Charlie Sheen, Clint Eastwood, Raul Julia, Sonia Braga, Lara Flynn Boyle, and Tom Skerritt. Eastwood plays a veteran police officer teamed up with a younger detective played by Sheen (the rookie of the title), whose intent is to take down a German crime lord in downtown Los Angeles following months of investigation into an exotic car theft ring. 
 CGI special effects during production. Distributed by Warner Bros., the film never spawned a sequel.
 Home Alone, which opened in theaters three weeks earlier and ended up being one of the top 100 highest grossing films of all time. Although considered a mild financial success, The Rookie was met with generally lackluster reviews before its initial screening in cinemas. After its theatrical run, it failed to receive award nominations for acting or production merits from accredited motion picture organizations in any category.

==Plot== grand theft auto and chop shop operations. During an encounter with Strom and his men, who are loading a semi-trailer truck with stolen cars, Powell is murdered. Nick, despite efforts to catch the criminals on the highway, ends up losing them.

Consequently, Nick is removed from the case by his superior, Lt. Raymond Garcia, and given a new partner, David Ackerman, a young officer recently promoted to rookie Detective, who has continual nightmares and is plagued by guilt over his brothers death, believing it was his fault as a child. Against regulations, Nick and David start investigating the Strom case. Nick tracks down a man working for Strom, Morales, whom he forces to cooperate in return for immunity. Morales, under Nicks instructions, manages to plant a two-way radio inside Stroms house without getting caught, but is still murdered later on by Stroms companion, Liesl. Feeling pressured by the authorities, Strom deduces that there is a connection between Morales and another one of his operatives, Little Felix, who has also been secretly working as an informant for Nick regarding the disclosure of his illegal activities at his wrecking yards. 

Through the radio, Nick and David learn that Strom is planning to leave the country after robbing a local casino of two million dollars. Stroms men set off smoke bombs inside the casino and capture the general manager, Alphonse, forcing him to open the vault for them. Inside the vault however, are Nick and David, who had conspired with Alphonse to capture Strom. Yet Liesl, knowing of Davids naïveté, dares him and distracts Nick long enough for Strom to pin the detective down to the floor. Liesl shoots David in the back; although he is not killed due to his bulletproof vest. But moments later, one of Stroms men, Cruz, discovers that the vault is empty. With police surrounding the building, Strom takes Nick hostage and demands the two million dollars as a ransom. Strom, however, has his hideout rigged with explosives, planning on destroying it with Nick inside while he escapes the country with the money.

In trying to determine Nicks whereabouts, David tracks down one of Stroms henchmen, Loco Martinez, who previously pinched his ID badge at a shady bar. Loco, however, manages to elude David following a violent encounter with him at a dry cleaners where Little Felix is also found murdered. David then decides to turn to his estranged father, Eugene, for help in securing the ransom money, which he reluctantly agrees to. While at his fathers office, Davids girlfriend Sarah informs him over the phone that Lt. Garcia is at their house waiting to interrogate him. Just then, David is visited by two detectives, who claim that Garcia is at headquarters and has sent them to retrieve him over his alleged forbidden involvement in the Strom case and police brutality. David realizes that one of Stroms men, Loco, who previously stole his badge, is posing as him. David evades the detectives and races home on a borrowed motorcycle. During an ensuing physical confrontation between the two men, Sarah grabs Davids gun and kills Loco. David is distraught, as he needed Loco alive to tell him where Nick is being held captive. However, David later notices Locos car outside their door, a bizarrely colored Lotus, which he and Nick spotted earlier at a warehouse in which one of Stroms mechanics, Max, was working on.

After killing Max in a struggle within an elevator shaft, Nick is rescued by David. The pair only barely escape the warehouse before the explosion by driving a Mercedes convertible through a wall, and later capture Cruz, whom Strom sent to collect the ransom. As the group arrives at the airport, Cruz gives Strom the money and is murdered. Nick and David open fire at the scene as a situation is created where one of Stroms men, Blackwell, in charge of flying him out of the country aboard a private jet, is killed following a collision with a commercial airliner. David later pursues Liesl into the airport and kills her, while Nick attempts to shoot Strom during his own chase, but ends up out of ammunition. Strom shoots Nick and prepares to finish him when David arrives and shoots Strom in the shoulder. Seriously injured, Strom collapses onto a luggage conveyor belt. Nick climbs on, and to avenge Powell and end the homicide spree there, kills him despite Stroms requests for medical attention. 

Sometime later, Nick, David and Garcia have been promoted. David is now a detective and Nick the new Lieutenant (though Garcias new position is left unrevealed). In a scene reminiscent of Nicks first meeting with David, Nick introduces David to another "rookie" cop, Heather Torres, as his new partner.

==Cast==
*Clint Eastwood as Nick Pulovski
*Charlie Sheen as David Ackerman
*Raúl Juliá as Strom
*Sônia Braga as Liesl
*Tom Skerritt as Eugene Ackerman
*Lara Flynn Boyle as Sarah
*Pepe Serna as Lieutenant Raymond Garcia Marco Rodriguez as Loco Martinez
*Xander Berkeley as Blackwell
*Roberta Vasquez as Heather Torres
*Hal Williams as Powell

==Production==
===Sets and equipment=== San Jose Interstate 680 State Route 87 in San Jose for the opening chase sequence featuring the semi-tractor trailer, the famous Villa Montalvo mansion for the henchmen meeting scene in the Santa Cruz Mountains of Saratoga, and the San Jose International Airport as well as the Mojave Air & Space Port for the final action climax scene, which author Laurence F. Knapp described as "both purgative and objectionable—a vivid, personal exchange of camera angles and vantage points that complicate, rather than conclude."   A furniture warehouse on the corner of 4th and Hewitt streets in downtown Los Angeles, stood in as the hideaway for Stroms illegal theft operation.    But in relation to same hideaways demise later in the film, a different building was used in the City of Commerce. A warehouse previously occupied by an auto agency slated for demolition on Flower near 12th Street, stood in for the impending explosion-filled destruction.    During an introductory scene, where Eastwoods character pulls up in an unmarked squad car to foil the plans of the car thieves, a z-shaped thoroughfare called Santa Fe Street, provided the perfect secluded background at night which also happened to overlook the Los Angeles skyline.  For the action sequences involving aircraft at the San Jose airport, a Hansa twin-engine jet was used to collide with a Convair 880 that was briefly disguised as a 150-passenger Evergreen International Airlines Boeing 727. 

In keeping with the continuity of the subject matter and storyline, the filming was punctuated with the use—and in some cases the destruction—of expensive and alluring foreign automobiles; including a Ferrari Daytona, a Porsche 928, a Jaguar XJ, as well as brief appearances of a Cadillac Allante  and a Rolls-Royce Silver Spirit.  A vintage Gull-wing door|gull-wing Mercedes-Benz 300SL is also presented in the film, being personally driven by Bragas character in an introductory sequence before a large-scale climactic car chase scene.  In addition to driving a newly redesigned Mercedes-Benz R129|Mercedes-Benz 500SL for the 1990 model year, Eastwoods character is also seen conveying his distaste for the Lime Green color on the Lotus Esprit and later driving said vehicle during a criminal pursuit.  Sheens character also takes part in a scene involving an older, rare Harley-Davidson, riding head-on through the front door of a residential home. 

===Filming===
 
Coordination of the lighting along with the capturing of all other photographic elements in the film were achieved by cinematographer Jack N. Green. Stunt coordinator Terry Leonard and second unit director Buddy Van Horn, oversaw the task of integrating the scope of stunt people working to produce the action which numbered over twice as many actors in the film (said to be over 80 stuntmen),   while supervisor John Frazier controlled the special effects.  Green, Van Horn, Leonard, Frazier, and production manager Valdez began close pre-production discussions three months before principal photography.  To meet his stylistic lighting objectives in shaping the scenery environment, Green utilized powerful Musco Lighting developed during the 1980s.  Commenting on a production scene surrounding the character of Pulovski at his residence, Green noted the home was characteristic of "359 degrees" of accessible turns of the camera. He detailed how the crew would "go in with these little ceiling units, as close to the ceiling as we could get them, little accent lights onto the place the actors would be. It would look like it was coming from those practicals but, again, at a dramatic angle."  Describing a stunt-related sequence early in the film performed by Eastwood himself, Van Horn who had been a stunt associate for almost 35 years, took the opportunity to commend the actor on his contributions saying, "Clint likes to do everything live, ... When you read the script, you know everything is going to be pretty much live action. Sometimes you have to talk him out of something that just might be a little too risky. Not that he couldnt do it, but if something even minor should happen, you couldnt afford to suspend the production."  The sequence which Van Horn alluded to, was a scene that involved Eastwood behind the drivers seat racing a Chevrolet Blazer through stop and go traffic, while swerving to avoid upcoming cars from the opposite direction. The scene included 20 other stunt drivers operating a carefully rehearsed formation through a head on collision course. According to Van Horn who engineered the sequence with Leonard, he noted, "The whole thing is like a football play, ... We all sit down and figure out where the cars are, where Clint makes the break out of traffic, where the other cars are going, and just the whole cause and effect for how and why he pulls into (the intersection) and decides to head on through. Thats all worked out ahead of time."  Leonard added, "In a situation where your rehearsal time is extremely limited, it becomes that old expression: experience, ... It becomes a seat of the pants kind of thing, about 20 drivers and Clint who know where the close calls are going to be and whos going to be in what position when. But once you get going, theres always the element of surprise, where maybe a car is 10 feet closer than it was expected to be, and a driver must react to that."     

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"Why Eastwood thought that the Puerto Rican Juliá and the Brazilian Braga would make convincing German schweinehund is difficult to determine, with Braga in particular suffering the indignities of awful dialogue."
|-
|style="text-align: left;"|—Daniel OBrien, author
|}
Speaking on the origins of the film, Eastwood mentioned "I have a project for this spring that will be full of action. Its another cop picture, very different from this one. It has its own character and if its done well it can turn out to be something good. Charlie Sheen will play the rookie and Ill play the mature cop."  During the initial production phase of the film, Sheen was also dealing with alcohol and drug related issues, as Eastwood took it upon himself as a fatherly figure, in trying to discipline the younger actor into proper behavior and responsibility.  Certain critics such as author Daniel OBrien, failed to understand why director Eastwood used Hispanic actors Juliá and Braga, to portray German villains. Puzzled, he mused, "Why Eastwood thought that the Puerto Rican Juliá and the Brazilian Braga would make convincing German schweinehund is difficult to determine, with Braga in particular suffering the indignities of awful dialogue ..."  In summing up the filming experience, Frazier said, "You know, things went really well, but you have to give credit to everyone involved. Clint Eastwood and David Valdez really gave us the time and their confidence to do it right. We were never really rushed, which is so important. We were able to do every one of the major shots in one take: the car out of the building, the carrier turning over and the planes colliding. That says something. These guys respect the crew and every job being done. It makes a big difference." 

In an interview with  . p. 79. ISBN 978-1-84511-902-7.  (Ironically, while the rape scene attracted considerable attention, little apparent notice was paid to the scene in which protagonist Pulovski commits murder by summarily executing Strom.

===Stunts===
The major stunt scenes were executed before the camera with no miniatures, no blue screens, while mostly being shot at night.  Completed during May 1990, the centerpiece stunt of the film involved a Mercedes-Benz convertible driving through the fourth floor window of an exploding warehouse. For the scene, a 1,500 pound Mercedes auto mock-up was connected by 150 feet of quarter-inch pulleyed cable to a Ford 4×4 pickup on the ground.  The truck would drive straight ahead and pull the Mercedes through the windows as the cable that connected the two vehicles exhibited a tensile strength of 8,000 pounds.  Once the pull on the cable started, both vehicles would move in precise proportion to one another. After the mock-up launched through the windows, the hook connecting the Mercedes would drop. As Frazier explained, "The hook is very similar to the kind used to launch jets on aircraft carriers, ... Once the hook falls away, the Mercedes is propelled by its own momentum."  Aerodynamics played a key role in the execution, as Frazier referred to the stunt saying, "When the car left the building it was very important that it exit and travel flat, ... A lot of times it doesnt matter. In this case, if the car didnt travel flat, you would know that the car could not have survived into the next sequence where it landed on the rooftop. We had to set up aerodynamics on that car, so that every time it went out the window, including the tests, it flew out without the nose dipping down."  The explosion filled destruction of the warehouse was produced by 9 separate 18-inch steel mortars on each of the 4 floors. When fully discharged, all 36 mortars produced the largest orchestrated explosion ever allowed in Los Angeles city limits permitted at the time. 

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"When the car left the building it was very important that it exit and travel flat, ... A lot of times it doesnt matter. In this case, if the car didnt travel flat, you would know that the car could not have survived into the next sequence where it landed on the rooftop. We had to set up aerodynamics on that car, so that every time it went out the window, including the tests, it flew out without the nose dipping down."
|-
|style="text-align: left;"|—John Frazier, stunt supervisor
|}
Another major stunt sequence consisted of a  , 21-ton two-tiered automobile carrier flipping over on its side, flinging its automotive cargo onto a freeway of traffic. In the sequence, the carrier is scripted to disconnect from the primary cab at highway speed, so that the carrier veers off to one side while eventually flipping over. Frazier explained how "Clint did not want the car carrier rolling over and over down the freeway, ... He just wanted it to go over on its side and then slide until it stopped. It would have been easier to load that thing up (with cars), get up to speed and barrel roll it down the freeway. But it wouldnt have looked realistic for his character to have survived it."  A semi-truck which was trailing 150 feet behind the carrier, held on to it by means of a cable. After the carrier became detached from the passenger cab, the semi-truck came to a stop yanking the carrier from the cab.  Frazier demonstrated how gravity played a significant and key role during the scene. He noted, "By itself, the carrier would not have come off the (cab), ... It could ride there forever, even though its been released, just due to the weight of the carrier. So we needed the holdback cable to the truck behind, and on cue, the driver locked his brakes."  After the carrier disengaged from the cab, steel castor wheels attached to the underside of the carrier directed it to the center embankment. A stuntman who was riding in the carrier fired 3 cannons to physically lift it over on its side.  

A small jet aircraft pursuing the lead characters played by Eastwood and Sheen, compliments another tightly coordinated stunt scene (aerial coordinators James W. Gavin and Eric W. Gray). After chasing the two detectives through a grassy area, a Hansa twin-engine jet collides with a Convair 880 traveling at 100&nbsp;mph.  Both the San Jose and Mojave airports were scripted as LA International Airport during filming of the scene.  Right before the impact, the Hansa was actually stationary being pulled by a 150-foot steel cable attached to a 4-wheel drive pickup truck right before the explosion erupted. Frazier explained, "The reason we did that is because if the planes collide first, its likely theyll upend all our wires and the explosion wouldnt occur at all. Another reason is that had the Hansa not demolished before impact, it could have spun the other plane around, and we might have ended up with the 880 in our shot instead.  The ensuing explosion after the Convair dissects the Hansa was powered by 10 gallons of gasoline and 4 separate 18-inch mortars. The detonation device included a 125-foot electrical cable set off by a bull switch to help achieve the desired effect. 

===Music=== score for the film which included elements of jazz music and considerable use of the trumpet, was originally composed by American saxophonist Lennie Niehaus.  The music score was mixed by Robert Fernandez and edited by Donald Harris. The sound effects in the film were supervised by Robert G. Henderson and Alan Robert Murray.  The mixing of the sound effects were orchestrated by Donald F. Johnson and Michael Evje.  Although not officially released, music from the soundtrack included songs entitled "All The Things You Are" written by Jerome Kern and Oscar Hammerstein II as well as the "Red Zone" written by Kyle Eastwood and Michael Stevens. 

==Reception==
===Critical response=== Pat Collins of WWOR-TV, enthusiastically proclaimed the film to be "The best buddy cop movie of the year."  The Variety (magazine)|Variety staff however, added to the general dismay with the film saying, "Overlong, sadistic and stale even by the conventions of the buddy pic genre, Clint Eastwoods The Rookie is actually Dirty Harry 5  since Eastwoods tough-as-nails cop Nick Pulovski could just as easily be named Harry Callahan, ..."  

{|class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; color:black; width:40em; max-width: 35%;" cellspacing="5"
|style="text-align: left;"|"jammed with material and the budget was obviously large, but somehow not much pays off. Its all there on the screen, but lifeless."
|-
|style="text-align: left;"|—Roger Ebert, writing in The Chicago Sun Times
|} Time Out in London commented, the "movie is full of caricatured cops and robbers, and punctuated with interminably dull car-chases."  
Alternatively though, Dave Kehr of the Chicago Tribune felt the quality of the stunt work was superb, commenting that they were, "the most spectacular action sequences Eastwood has ever filmed."  Noted critic Leonard Maltin gave the film a star and a half, somewhat approving of the stuntwork by figuratively mentioning, "theres one good freeway crackup" but in the end, felt the theme amounted to "Formula filmmaking that even bored its intended audience." 

Other movie critics, like   from Entertainment Weekly posted, "The Rookie is like a series of garish exploitation set pieces jammed into the shape of a buddy movie." He went further in his criticism saying, "as moviemaking goes, The Rookie is on the slovenly side. The plot makes almost no sense, and Eastwood directs in his usual toneless fashion." But on a lighter note, relating to the films comedic appeal, he stated, "in this case, the fact that you cant always tell the intentional comedy from the unintentional isnt necessarily a drawback."  In agreement on the lack of plausibility surrounding the plot, author Marshall Julius still offered though an almost entirely positive review, giving the film three and a half guns, exclaiming, "As directed by Eastwood, The Rookie is a deliberately silly, knockabout adventure which aims for outrageous and hits a bullseye. Were talking good, dumb, fun. Get your brains out and the beers in, and youre all set." 

===Box office=== theaters grossing Home Alone 1990 as a whole, the film would cumulatively rank at a box office performance position of 56. 

===Home media=== Region 1 DVD in theatrical trailer.  The film was released on Blu-ray Disc on June 1, 2010. 

===Novelization===
In January 1991, a novelization based on the screenplay was released. Distributed by Warner Books, it was written by Tom Philbin. 

==References==
 

==External links==
 
* 
*  at the Movie Review Query Engine
*  at Rotten Tomatoes
*  at Box Office Mojo
 

 
 
 
 
 
 
 
 
 
 
 
 
 