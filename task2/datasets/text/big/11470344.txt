Antikiller
{{Infobox film name = Antikiller image = AntikillerDvdCover.jpg image_size = caption = Russian DVD cover director = Egor Konchalovsky producer = Yusup Bakshiyev Vladimir Kilburg Viktor Taknov writer = Daniil Koretsky starring = Gosha Kutsenko Mikhail Ulyanov music =  cinematography = Anton Antonov editing = Bénédicte Brunet Olga Grinshpun distributor = Central Partnership released =   runtime = 114 minutes country = Russia language = Russian budget = United States dollar|US$5 million gross = $340,312 (Russia) followed_by = 
}}
Antikiller ( ) is a 2002 Russian crime film directed by Egor Konchalovsky. It portrays a brutal war between obnoxious crime gangs and a one man vigilante, a former police officer. The movie is based on Daniil Koretsky’s novel of the same name, which has sold five million copies in the countries of the former Soviet Union and has acquired cult status among readers of Russian pulp fiction. Like the novel, the film became the box office leader among Russian films in 2002.

==Plot==
Former criminal investigator, Major Korenev, nicknamed Fox, gets out of jail, where he spent many years after being betrayed by his corrupt colleagues, and settles scores with his old and new enemies. These enemies are so numerous that some viewers and critics found the film confusing and considered the 114 minutes allotted by the filmmaker for the enemies’ annihilation somewhat excessive.

The film uncovers the anatomy of the beginning of Russian economical boom which began the in 1990s and the many varieties of crime which came with it. Fox (Gosha Kutsenko)  went to jail when the Soviet Union was still alive, but returns from prison to a new country, Russia, which the film portrays as a lawless post-industrial wasteland ruled by competing criminal gangs. Fox settles accounts with Shaman (Aleksandr Baluev), the criminal boss who sent him to jail; kills Ape (Viktor Sukhorukov), a sadistic gang leader who kills and rapes randomly for art’s sake; topples the city’s major gangs; and reestablishes his version of the rule of law. The only criminal boss who survives the war among the major gangs is "Cross" (Sergei Shakurov), whose ascetic and down to earth style helps him to unseat the kingpin "Father" (Mikhail Ulyanov) modeled on Don Vito Corleone.

==Cast==
*Gosha Kutsenko as Major Korenev, aka Fox.
*Mikhail Ulyanov as Father
*Sergey Shakurov as Cross Aleksandr Belyavskiy as King
*Ivan Bortnik as Bedbug
*Valentin Golubenko as Gangreen
*Aleksandr Baluev as Shaman
*Yevgeni Sidikhin as Barcass
*Viktor Sukhorukov as Ape Mikhail Yefremov as Banker
*Viktoriya Tolstoganova as Tamara
*Vyacheslav Razbegaev as Metis

==External links==
*  

 
 
 
 
 
 
 
 


 
 