House of Mystery (1934 film)
{{Infobox film
| name           = House of Mystery
| image	=	House of Mystery FilmPoster.jpeg
| caption        = DVD cover
| director       = William Nigh
| producer       = Paul Malvern (supervising producer)
| writer         = Albert DeMond (writer) Adam Shirk (playwright, The Ape)
| narrator       =
| starring       = See below
| music          =
| cinematography = Archie Stout
| editing        = Carl Pierson 
| distributor    =
| studio         = Monogram Pictures
| released       =  
| runtime        = 62 minutes
| country        = United States
| language       = English}}

House of Mystery is a 1934 American film directed by William Nigh.

==Cast==
*Ed Lowry as Dylan "Jack" Armstrong
*Verna Hillie as Ella Browning John Sheehan as Harry Smith
*Brandon Hurst as Hindu Priest
*Joyzelle Joyner as Chanda
*Fritzi Ridgeway as Stella Walker
*Clay Clement as John Prendergast aka John Pren
*George Gabby Hayes as David Fells Dale Fuller as Mrs. Geraldine Carfax Harry C. Bradley as Prof. Horatio Potter
*Irving Bacon as Police Insp. Ned Pickens
*Mary Foy as Mrs. Hyacinth Potter

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 