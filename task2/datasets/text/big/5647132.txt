Adventures of a Dentist
{{Infobox film name           = Adventures of a Dentist image          = caption        = Scene from the film director       = Elem Klimov producer       = writer         = Aleksandr Volodin (play & screenplay) starring  Andrei Petrov music          = Alfred Shnitke cinematography = Samuil Rubashkin editing        = Valeriya Belova studio         = Mosfilm released       = 1965  (USSR)  1987  (second release)  runtime        = 82 min. country        = Soviet Union language       = Russian budget         =
}} Soviet Black dark comedy/Comedy-drama|drama feature film directed by Elem Klimov on Mosfilm.  It is currently unavailable on video or DVD for any audience, but is occasionally screened at film festivals.

==Plot==
A dentist is derided (and eventually has his life ruined) by his colleagues for his natural talent of painlessly pulling out teeth.

==Background==
The films implication, that society inevitably ostracizes those who are gifted, horrified censors who told Klimov to change it. When Klimov refused, the film was given the lowest classification: "category three", which meant that it was shown in only 25-78 movie theatres.  Only about half a million viewers saw the film when it premiered.  In the West, the film has gained recognition due to it being directed by Klimov (most known for his film Come and See), and it has been screened at several film festivals in the last few years.

==External links==
* 
* 
* 
*  at official Mosfilm site

 

 
 
 
 
 
 
 
 
 
 


 
 