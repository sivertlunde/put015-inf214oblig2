The Little Escapade
{{Infobox film
| name = The Little Escapade
| image =
| image_size =
| caption =
| director = Reinhold Schünzel
| producer = Günther Stapenhorst     
| writer =  Emeric Pressburger    Reinhold Schünzel
| narrator =
| starring = Renate Müller   Hermann Thimig   Hans Brausewetter
| music = Ralph Erwin   
| cinematography = Werner Brandes   
| editing =      UFA 
| distributor = UFA
| released =14 August 1931  
| runtime = 88 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Little Escapade (German:Der kleine Seitensprung) is a 1931 German comedy film directed by Reinhold Schünzel and starring Renate Müller, Hermann Thimig and Hans Brausewetter.  A separate French-language version was also made.

A woman pretends that she is having an affair as a joke on her husband, but as he decides to start divorce proceedings she realises she has taken the joke too far.

==Cast==
*  Renate Müller as Erika Heller  
* Hermann Thimig as Walter Heller  
* Hans Brausewetter as Dr. Max Eppmann, Transportleiter  
* Otto Wallburg as August Wernecke, Fabrikant 
* Hilde Hildebrand as Lona Wernecke, seine Frau   Diana as Wahrsagerin  
* Hermann Blaß as Klavierspieler 
* Oscar Sabo as Lohndiener  
* Martha Ziegler as Zofe  
* Paul Westermeier as Tänzer in der Bar 
* Otti Dietze
* Olga Engl
* Berta Gast    
* Ruth Jacobsen   
* Hildegard Kohnert   
* Michael von Newlinsky   
* Gertrud Wolle    
* Louise Lagrange

== References ==
 

== Bibliography ==
* Scott Salwolke. The Films of Michael Powell and the Archers. Scarecrow Press, 1997. 

== External links ==
*  

 

 
 
 
 
 
 
 

 