The Split
 

 
{{Infobox Film
| name           = The Split
| image          = TheSplitPoster.jpg
| image_size     = 
| caption        = 
| director       = Gordon Flemyng
| producer       = Robert Chartoff Irwin Winkler
| writer         = Donald E. Westlake  (novel)  Robert Sabaroff
| based on     =  
| narrator       = 
| starring       = Jim Brown Diahann Carroll Julie Harris Ernest Borgnine
| music          = Quincy Jones
| cinematography = Burnett Guffey
| editing        = 
| distributor    = Metro-Goldwyn-Mayer October 1968
| runtime        = 91 min.
| country        = United States English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Parker novel The Seventh by Richard Stark (a pseudonym of Donald E. Westlake).

==Plot synopsis==
Thieves fall out when over a half million dollars goes missing after the daring and carefully planned robbery of the Los Angeles Coliseum during a football game, each one accusing the other of having the money.
 getaway driver Harry Kifka to a race, picks a fight with thug Bert Clinger, imprisons electrical expert Marty Gough in an wire-controlled vault to watch him fashion an escape, and has a shooting match with marksman Dave Negli before pulling off the job.

Together, the thieves make off with over $500,000. With the five men having carried out the heist and Ellie having financed it, the plan is to split the money six ways the next day.  McClain stashes the money for the night with Ellie, his ex-wife. While his partners impatiently await their split of the loot, Lt. Walter Brill takes charge of the case. Ellie is attacked and killed by Herb Sutro, her landlord, who steals the money as well.

The rest of the gang members hold McClain accountable for the lost money and demand that he retrieve it.  Brill quickly solves the murder and is well aware of the connection to the robber.  He kills Sutro, but keeps the money for himself.  With Ellies murderer identified, but still no trace of the money, the gang members all turn on McClain, assuming hes hiding it.  This leads to a confrontation that ends with the deaths of Negli and Gladys.

McClain escapes and visits Brill, threatening to reveal that Brill has the money.  He and Brill decide to divide it up between themselves, but the rest of McClains gang has other ideas.  After a shoot-out at the docks, only McClain and Brill are left—Brill decides to take a small part of the money, giving McClain his rightful sixth, and plans to return the rest to win a promotion.  McClain, with his money, is about to board a flight out of town, when he seems to hear Ellies voice calling his name.

==Cast==
*Jim Brown as McClain
*Diahann Carroll as Ellie Kennedy
*Ernest Borgnine as Bert Clinger
*Julie Harris as Gladys
*Gene Hackman as Detective Lt. Walter Brill
*Jack Klugman as Harry Kifka
*Warren Oates as Marty Gough
*James Whitmore as Herb Sutro
*Donald Sutherland as Dave Negli
*Joyce Jameson as Jenifer
*Harry Hickox as Detective
*Jackie Joseph as Jackie
*Warren Vanders as Mason

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 


 