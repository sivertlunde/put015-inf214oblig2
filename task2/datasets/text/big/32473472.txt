The Young Girl
 

{{Infobox film
| name           = The Young Girl
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Souleymane Cissé
| producer       = 
| writer         = Souleymane Cissé
| screenplay     = 
| story          = 
| based on       = 
| narrator       = 
| starring       = Dounamba Dany Coulibaly Fanta Diabate
| music          = Wandé Kuyaté
| cinematography = 
| editing        = Andrée Davanture
| studio         = Cissé Films
| distributor    = trigon-film
| released       = 1975
| runtime        = 88 minutes
| country        = Mali
| language       = Bambara
| budget         = 
| gross          =
}}

The Young Girl is a 1975 Mali film, directed by Souleymane Cissé.

==Plot==
A young mute woman is raped and becomes pregnant, with disastrous consequences within her family. The film also sketches the social/economic situation in urban Mali in the 1970s, particularly in relation to the treatment of women.

==Cast==
*Dounamba Dany Coulibaly
*Fanta Diabate
*Omou Diarra
*Balla Moussa Keita
*Mamoulou Sanogo

==External links==
 

 
 
 

 