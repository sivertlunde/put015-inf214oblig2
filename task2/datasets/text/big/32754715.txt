7 Hati 7 Cinta 7 Wanita
 
{{Infobox film
| name           = 7 Hati 7 Cinta 7 Wanita
| image          = 7 Hati 7 Cinta 7 Wanita.jpg
| caption        = 
| director       = Robby Ertanto
| producer       = 
| writer         = Robby Ertanto
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| music          = 
| cinematography = 
| editing        = 
| studio         = Anak Negeri Film
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
| gross          = 
}} Indonesian film. Written and directed by Robby Ertanto, it stars Jajang C. Noer, Marcella Zalianty, Happy Salma, Olga Lydia, Intan Kieflie, and Henky Solaiman. It follows the story of seven women connected through their obstetrician, Kartini. It was nominated for 6 awards at the 2010 Indonesian Film Festival as well as eight at the 2011 Indonesian Movie Awards, where it won two.

==Production==
7 Hati was written by Robby Ertanto, a student at the Jakarta Art Institute, over a period of two years. After hearing that his mother was ill, he started filming "to give something special to   mother as a woman" He found his inspiration in the daily struggles of women. It was Ertantos directorial debut. 

Happy Salma was approached to play the role of Yanti, a prostitute. Salma accepted the role, on the condition that she not have any form of physical contact with the male performers. She later said that "it’s okay to play a sensual character, but   want to do such scenes". 

==Plot==
7 Hati follows the story of seven women who are unknowingly interconnected. The lead character, Kartini (Jajang C. Noer), is a 45-year-old obstetrician who finds herself increasingly sympathetic with her patients. These patients include Ratna (Intan Kieflie), a hijab-wearing woman who is pregnant for the first time; Rara (Tamara Tyasmara), Ratnas 14-year-old sister; Yanti (Happy Salma), a prostitute; Lili (Olga Lydia), a pregnant Chinese Indonesian woman; and Lastri (Radia), an obese woman. 
 sexually abused by her husband, but thinks of it as an indication of his love. Lastri is newly married and trying to get pregnant. Kartini herself has had bad experiences in her former relationships, making her afraid of commitment. 

==Themes==
7 Hati is seen as a "take on women’s issues", both societal and interpersonal, similar to Berbagi Suami. 

==Release and reception==
7 Hati was released on 18 May 2011 to critical acclaim. 

The Jakarta Post called 7 Hati "A heart-warming drama staring  an outstanding cast and offering a genuine reality check on Indonesian women".  KapanLagi.com praised its visuals, music, scenario, and character development. 

==Awards==
7 Hati was nominated for 6 awards at the 2010 Indonesian Film Festival. It was also nominated for eight awards at the 2011 Indonesian Movie Awards, winning two; Happy Salma won Best Supporting Actress, while Rangga Djoned won Best Newcomer. 

==References==
; Footnotes
 

; Bibliography
* {{cite news
  | title = Happy Salma returns to silver screen as prostitute
  | author =
  | newspaper = The Jakarta Post
  | date = 1 July 2010
  | url = http://www.thejakartapost.com/news/2010/07/01/happy-salma-returns-silver-screen-prostitute.html
  | accessdate =14 August 2011
  | ref =  
  }}
* {{cite news
  | last = 
  | first = 
  | date = 29 May 2011
  | title = The high and low of Seven Women 
  | newspaper = The Jakarta Post
  | url = http://www.thejakartapost.com/news/2011/05/29/the-high-and-low-seven-women.html
  | accessdate =14 August 2011
  | ref =  
  }}
* {{cite web
  | last = 
  | first = 
  | date = 18 May 2011
  | title = 7 HATI 7 CINTA 7 WANITA, Potret Wanita Dari Segala Rupa
  | publisher = KapanLagi.com
  | language = Indonesian
  | trans_title = 7 Hearts 7 Loves 7 Women, Portrait of Women from All Angles
  | url = http://www.kapanlagi.com/film/indonesia/7-hati-7-cinta-7-wanita-potret-wanita-dari-segala-rupa.html
  | accessdate =14 August 2011
  | ref =  
  }}

 
 
 
 