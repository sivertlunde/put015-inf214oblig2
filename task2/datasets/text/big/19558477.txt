Poeten og Lillemor i forårshumør
{{Infobox film
| name           = Poeten og Lillemor i forårshumør
| image          = 
| caption        = 
| director       = Erik Balling
| producer       = Carl Rald
| writer         = Erik Balling
| starring       = Henning Moritzen
| music          = 
| cinematography = Jørgen Skov
| editing        = Birger Lind	
| distributor    = Nordisk Film
| released       =  
| runtime        = 100 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Poeten og Lillemor i forårshumør is a 1961 Danish comedy film directed by Erik Balling and starring Henning Moritzen.

==Cast==
* Henning Moritzen - Poeten
* Helle Virkner - Lillemor
* Eva Hast Nystad - Lotte (as Eva Nystad)
* Ove Sprogøe - Anton
* Lis Løwert - Vera
* Anders Thornberg - Tvilling
* Kristian Thornberg - Tvilling
* Karl Stegger - Slagteren
* Bodil Udsen - Jordmoderen
* Dirch Passer - Bageren
* Judy Gringer - Bagerjomfru Lise
* Poul Bundgaard - Bagersvenden
* Helge Kjærulff-Schmidt - Bilforhandleren
* Palle Huld - Rejseføreren
* Birte Bang - Forårspigen
* Suzanne Bech - Pin-up girl
* Carl Ottosen - Togpassager
* Kirsten Passer - Togpassager

==External links==
* 

 
 
 
 
 
 
 


 
 