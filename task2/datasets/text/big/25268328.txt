Ways to Live Forever (film)
{{Infobox film
| name           = Ways to Live Forever
| image          = waysToLiveForever2010Poster.jpg
| alt            =  
| caption        = Spanish film poster
| director       = Gustavo Ron
| producer       = Martyn Auty
| writer         = Sally Nicholls Gustavo Ron
| starring       = Robbie Kay Alex Etel Ben Chaplin Emilia Fox Greta Scacchi
| music          = Cesar Benito
| cinematography = Miguel P. Gilaberte, Production Design Jason Carlin
| production design = Jason Carlin
| editing =
| studio         = Life&Soul Productions El Capitan Pictures Formato Producciones World Wide Motion Pictures Corporation (North America)
| released       =  
| runtime        = 
| country        = United Kingdom Spain
| language       = English
| budget         = 
| gross          = 
}} novel of World Wide Motion Pictures Corporation for North America  and InTandem for the rest of the world,. 

== Production ==

Ways to Live Forever was filmed in studio and on location in and around  , Tynemouth, North Shields, Cullercoats and Whitley Bay. 

== Cast ==
*Sam (Robbie Kay): A 12-year-old boy who likes to know facts.  He is dying of leukemia.  He has a list of things he wants to do before he dies.  The book "Ways to Live Forever" is presented as the book he is writing.
*Felix (Alex Etel): Sams friend who has cancer.  He has a wheelchair, and is straightforward.
*Daniel (Ben Chaplin): Sams father, a quiet man.
*Amanda (Emilia Fox): Sams mother.
*Ella (Eloise Barnes): Sams younger sister.
*Gran (Phyllida Law): Sams grandmother.
*Mrs Willis (Greta Scacchi): Both Sam and Felixs tutor, who likes to teach "fun stuff".
*Annie (Natalia Tena): A nurse who rides around on a pink scooter and calls herself "Dracula", on account that she often takes kids blood samples.
*Kaleigh (Ella Purnell): Felixs cousin, who helps Sam and Felix into her uncles pub for a drink and a kiss.

==References==
 

== External links ==
*  
* http://www.waystoliveforevermovie.com

 
 
 
 
 
 
 
 


 