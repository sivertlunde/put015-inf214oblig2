Ek Ladka Ek Ladki
 
 

{{Infobox film
| name = Ek Ladka Ek Ladki एक लड़का एक लड़की  
| image = EkLadkaEkLadkinew.jpg
| writer = Saleem Agha
| starring = Salman Khan, Neelam, Anupam Kher
| director = Vijay Sadanah
| producer = D.D.Yande
| released = 18 July 1992
| cinematography = S.L. Sharma
| editing = Omkar Bhakri
| country = India
| language = Hindi
| music = Anand-Milind
| awards = 
| budget = 
}}

Ek Ladka Ek Ladki ( ,  }},   Bollywood film directed by Vijay Sadanah, starring Salman Khan and Neelam Kothari. The film was released on 18 July 1992. It is a loose adaptation of the American film Overboard (film)|Overboard starring Kurt Russell and Goldie Hawn.

== Plot ==
Bhagwati Prasad (Anupam Kher) looks after the vast estate of his deceased brother, assisted by his wife, and brother-in-law, Markutey. Bhagwati has got into debt and has been embezzling money in order to pay his debtors. His spoiled and rude niece, Renu (Neelam), who resides in the United States decides to pay them a visit. Bhagwati welcomes her and makes her feel at home, but Renu is not easily satisfied until she gets a speed boat so that she can be on her own. Alerted by her lawyer, she starts to scrutinize recent transactions and finds 1.5&nbsp;million rupees missing. She asks Bhagwati to provide her with an explanation within 2 days. Then she gets into her speed boat, only to get involved in an accident, planned by Bhagwati,and disappears. Bhagwati is all set to take over the estate, when the estates lawyer gets a Court Order freezing all the cash, bank, and assets until such time Renu is found or her body is located. The months go by, Bhagwati hires men to look for Renu, and they soon find her and lead him to her. Bhagwati finds out that she has lost her memory, and is living with a man named Raja(Salman Khan) and three children. This time Bhagwati will make sure that she will not escape alive, even if it means to kill Raja and the three children as well.

== Cast ==
* Salman Khan: Raja
* Neelam: Renuka /Rani 
* Asrani: Markutey
* Tiku Talsania: Police Inspector
*  Mrs. Prasad
* Anupam Kher: Bhagwati Prasad

==Soundtrack==
{{Infobox album  
| Name = Ek Ladka Ek Ladki
| Type = Soundtrack
| Artist = Anand-Milind
| Cover = 
| Released = 1992 
| Recorded =  Feature film soundtrack
| Length =  Tips
| Producer = Anand-Milind
| Last album = Dil Aashna Hai  (1992)
| This album = Ek Ladka Ek Ladki (1992)
| Next album = Ghar Jamai  (1992)
}}

{| class="wikitable sortable"
|-
! No. !! Title !! Singer(s)
|-
| 1 || "Kitna Pyar Tumhein Karte Hain" || Kumar Sanu, Sadhana Sargam
|-
| 2 || "Ek Ladka Ek Ladki" || Udit Narayan, Neelam
|-
| 3 || "Aao Jhoomein Naachein" || Udit Narayan, Sadhana Sargam
|-
| 4 || "Ande Se Aayi Murgi" || Amit Kumar, Sadhana Sargam
|-
| 5 || "Choti Si Duniya Mohabbat Ki" || Udit Narayan, Sadhana Sargam
|-
| 6 || "Phool Yeh Nahin Armaan" || Suresh Wadkar, Kavita Krishnamurthy
|}

==External links==
* 

 
 
 
 