Hell (2005 film)
 
{{Infobox film
| name           = Hell
| image          = Lenfer.jpg
| caption        = Promotional poster|alt=Promotional poster for Lenfer
| director       = Danis Tanović
| producer       = Marc Baschet Marion Hänsel Čedomir Kolar Yuji Sadai Rosanna Seregni
| writer         = Krzysztof Kieślowski Krzysztof Piesiewicz
| starring       = Emmanuelle Béart Marie Gillain Carole Bouquet
| music          = Duško Segvić Danis Tanović
| cinematography = Laurent Dailland
| editing        = Francesca Calvelli
| distributor    = Diaphana Films 01 Distribuzione
| released       =  
| runtime        = 102 minutes
| country        = France Italy Japan
| language       = French
| budget         =
| gross          = $595,618 http://boxofficemojo.com/movies/intl/?page=&wk=2005W48&id=_fLENFER01 
}} French film, released in 2005 and directed by Danis Tanović. It is based on a script originally drafted by Krzysztof Kieślowski and Krzysztof Piesiewicz, which was meant to be the second film in a trilogy with the titles Heaven (2002 film)|Heaven, Hell and Krzysztof Kieślowski#Legacy|Purgatory. The script was finished by Piesiewicz after Kieślowski died in 1996. The movie stars Emmanuelle Béart, Marie Gillain and Carole Bouquet.

==Plot==
The film is set in Paris and is about three sisters: Celine, Anne and Sophie. It starts with a scene in which a woman and her young daughter (who we later find out is Celine) walk into an office and see two people: her father, and a young man who is naked.

After Sebastian has met Celine a few times much later in their adulthood, following a misunderstanding in which she strips for him as she believes him to be an admirer, he confides to her that he was the young man, and that her fathers imprisonment for this crime was actually his fault. He said that he had fallen in love with her father and, finally being alone with him and not knowing what else to do, took off his clothes.

It is revealed that the girls father tried to see his daughters on his release from prison when they were young. He broke into his ex-wifes apartment and locked her in the kitchen to try to see his daughters and attempts to see them, but they, having been told by their mother what their father has done, have locked themselves in their bedroom. The mother breaks out of the kitchen and he assaults her, leaving her with a brain injury and an inability to speak. He then jumps from the window, killing himself.

In the present day, Anne has an affair with the father of her best friend a Sorbonne professor, by whom she becomes pregnant (while the professor is still married). Sophies marriage falls apart as her husband has an affair.

Celine contacts her sisters, whom she has not seen for some time even though all three live in Paris, and explains the truth of her fathers innocence as revealed by Sebastian. They visit their mother in her beautiful aged care home, and explain that their fathers conviction was a mistake, and she was wrong to vilify him. She replies by writing I have no regrets, implying an ulterior motive in denouncing her then-husband. The audience is left wondering if he cheated on her, long ago, and thus each of the four women share a tortured history of men in their lives.

==Cast==
*Emmanuelle Béart – Sophie
*Karin Viard – Céline
*Marie Gillain – Anne
*Guillaume Canet – Sébastien
*Jacques Gamblin – Pierre
*Jacques Perrin – Frédéric
*Carole Bouquet – Marie, the mother
*Miki Manojlović – Antoin, the father
*Jean Rochefort – Louis
*Maryam dAbo – Julie
*Gaëlle Bona – Joséphine
*Tiffany Tougard – young Céline
*Marie Loboda – young Sophie
*Emma Cuzon – young Anne
*Julian Ciais – young Sébastien

==References==
 

==External links==
*  
*  
*  
 
 

 
 
 
 
 
 