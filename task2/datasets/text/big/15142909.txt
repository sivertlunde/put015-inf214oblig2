King of the Cowboys
{{Infobox Film
| name           = King of the Cowboys
| image_size     = 190px
| image	         = King of the Cowboys FilmPoster.jpeg
| caption        = 
| director       = Joseph Kane
| producer       = 
| writer         = 
| starring       = Roy Rogers Smiley Burnette Peggy Moran Gerald Mohr
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 67 minutes
| country        = United States
| language       = English
| budget         = 
}}

King of the Cowboys is a 1943 film directed by Joseph Kane and starring Roy Rogers and Smiley Burnette. It is set in Texas during World War II.

== Cast==
* Roy Rogers as Roy Rogers
* Smiley Burnette as Frog Millhouse
* Bob Nolan as Singer
* Sons of the Pioneers as Themselves
* Peggy Moran as Judy Mason
* Gerald Mohr as Maurice, the Mental Marvel
* Dorothea Kent as Ruby Smith
* Lloyd Corrigan as William Kraley, Governors Secretary
* James Bush as Dave Mason Russell Hicks as Texas Governor Shuville
* Irving Bacon as Alf Cluckus
* Norman Willis as Henchman Buxton
* Forrest Taylor as Lawman with Tex

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 