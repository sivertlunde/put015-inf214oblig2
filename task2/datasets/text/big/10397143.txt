Royal Affairs in Versailles
{{Infobox Film   
| name           = Royal Affairs in Versailles 
| image          = Royal Affairs in Versailles poster.jpg
| caption        = Theatrical release poster
| director       = Sacha Guitry
| producer       = Sacha Guitry Ignace Morgenstern Clément Duhour (executive producer)
| writer         = Sacha Guitry (scenario & dialogue)
| starring       = 
| music          = Jean Françaix
| cinematography = Pierre Montazel
| editing        = Raymond Lamy
| studio         = Cocinor Mondial (UK) 
| released       = 9 March 1954 (France)  8 March 1957 (USA)  1960 (UK)
| runtime        = 165 minutes
| country        = France
| language       = French
| budget         = 
| gross = $45,414,122 
}}

Royal Affairs in Versailles (French title: Si Versailles métait conté) is a 1954 French historical drama directed by Sacha Guitry, which tells some episodes through portrayal of the personalities who lived in Versailles castle. Its sister films are Napoléon (1955 film)|Napoléon (1955) and If Paris Were Told to Us (1956). 

The film is notable for the presence of a great number of well-known French actors, often appearing in short parts. One unknown actor playing a major character is Gilbert Bokanowski (credited as Gilbert Boka) portraying Louis XVI. Bokanowski was actually the films production manager and was cast because of his strong resemblance to the monarch.

Its English translation title is If Versailles Were Told to Me. Despite French production, the film is best known by its English title Royal Affairs in Versailles. 

==Plot== Chateau of Versailles.

==Principal cast==
*Michel Auclair as Jacques Damiens
*Jean-Pierre Aumont as Cardinal de Rohan
*Jean-Louis Barrault as François Fénelon
*Jeanne Boitel as Madame de Sevigné
*Gilbert Bokanowski as Louis XVI
*Bourvil as museum guide
*Annie Cordy as Madame Langlois
*Gino Cervi as Cagliostro
*Jean Chevrier as Turenne
*Aimé Clariond as Rivarol
*Claudette Colbert as Madame de Montespan
*Nicole Courcel as Madame de Chalis
*Danièle Delorme as Louison Chabray Yves Deniaud as Le paysan
*Daniel Gélin as Jean Collinet
*Fernand Gravey as Molière
*Sacha Guitry as old Louis XIV
*Pierre Larquey as Un guide du musée de Versailles
*Jean Marais as Louis XV
*Georges Marchal as young Louis XIV
*Lana Marconi as Marie-Antoinette
*Mary Marquet as Madame de Maintenon
*Gaby Morlay as Madame de la Motte
*Giselle Pascal as Louise de la Vallière
*Jean-Claude Pascal as Axel de Fersen
*Édith Piaf as Woman of the People
*Gérard Philipe as DArtagnan
*Micheline Presle as Madame de Pompadour
*Jean Richard as Du Croisy/Tartuffe
*Tino Rossi as Le gondolier
*Raymond Souplex as Le commissaire-priseur
*Jean Tissier as Le guide du musée de Versailles
*Charles Vanel as Monsieur de Vergennes
*Orson Welles as Benjamin Franklin
*Pauline Carton as La Voisin
*Jean Desailly as Marivaux
*Gilbert Gil as Jean-Jacques Rousseau
*Marie Mansart as Madame de Kerlor
*Nicole Maurey as Mademoiselle de Fontanges
*Jean Murat as Louvois
*Jean-Jacques Delbo as Monsieur de la Motte
*Louis Seigner as Lavoisier

==References==
 

==External links==
*  
* 
*  
*  at Films de France

 
 
 
 
 
 
 
 
 
 
 
 