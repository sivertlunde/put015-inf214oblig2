Two Angels and a Sinner
{{Infobox film
| name           =Two Angels and a Sinner
| image          =
| image_size     =
| caption        =
| director       = Luis César Amadori 
| producer       =
| writer         =Carlos A. Olivari   Sixto Pondal Ríos
| narrator       =
| starring       = Zully Moreno   Pedro López Lagar   Fanny Navarro   Florindo Ferrario
| music          =Juan Ehlert 
| cinematography = Alberto Etchebehere    
| editor       = Jorge Gárate 
| studio       = Argentina Sono Film 
| distributor    = Argentina Sono Film 
| released       = 1945
| runtime        = 85 minutes
| country        = Argentina Spanish
| budget         =
}}
Two Angels and a Sinner (Spanish:Dos ángeles y un pecador) is a 1945 Argentine comedy drama film directed by Luis César Amadori and starring Zully Moreno, Pedro López Lagar and Fanny Navarro. The films sets were designed by art director Mario Vanarelli.

==Cast==
* Zully Moreno
* Pedro López Lagar
* Fanny Navarro
* Florindo Ferrario
* Liana Moabro
* Miriam Sucre
* Adriana Alcock
* José A. Paonessa
* Roberto Bordoni
* Nora Loubet
* Margarita Burke
* Leda Urbi
* Warly Ceriani

==External links==
*  

 
 
 
 
 
 
 
 

 