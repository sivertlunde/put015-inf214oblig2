Young Cassidy
 
 
{{Infobox film name            = Young Cassidy
| image          = Young Cassidy 1965 poster.jpg
| image_size     = 220px
| caption        = 1966 Theatrical Poster director        = Jack Cardiff John Ford producer        = Robert Emmett Ginna Robert D. Graff writer          = John Whiting based on        = Mirror in My House by Sean OCasey starring        = Rod Taylor Julie Christie Maggie Smith music           = Sean ORiada cinematography  = Edward Scaife editing         = Anne V. Coates distributor     = Metro-Goldwyn-Mayer released        = 22 April 1965 runtime         = 110 min. language  English
|country         = United Kingdom budget          = $1 million (est.) 
|}}
 1965 film directed by Jack Cardiff and John Ford.  The film stars Rod Taylor, Julie Christie, and Maggie Smith. The film is a biographical drama based upon the life of the playwright Sean OCasey.

==Plot==
Set in 1911 and the growing protest against British rule in  , which he submits to the Abbey Theatre (which had already rejected another of his plays, The Shadow of a Gunman), and is surprised when W.B. Yeats, the founder of the Abbey, accepts and produces his new play. The opening of the play causes the audience to riot, and he loses many friends; but he is undeterred and is soon acclaimed as Irelands outstanding young playwright.
 The Playboy of the Western World resulted in riots by theatergoers, which had to be quelled by police.  The real W.B Yeats, a friend of both authors, said similar words at or after both riots.   Young Cassidy brings this parallel history (the riots of The Plough and the Stars and The Playboy of the Western World) to vivid life, tied together by the character of Yeats.

==Cast==
{| class="wikitable"
! Actor
! Role
|-
| Rod Taylor John Cassidy
|-
| Maggie Smith
| Nora
|-
| Julie Christie
| Daisy Battles
|-
| Michael Redgrave
| W.B. Yeats
|-
| Edith Evans
| Augusta, Lady Gregory
|-
| T. P. McKenna
| Tom
|-
| Jack MacGowran
| Archie
|-
| Siân Phillips
| Ella
|-
| Flora Robson
| Mrs. Cassidy
|}

==Production==
Based on Sean OCaseys autobiography Mirror in my House (the umbrella title under which the six autobiographies he published from 1939 to 1956 were republished, in two large volumes, in 1956),  the movie began production in 1964, changing his name in the film to John Cassidy. OCasey had read earlier drafts of the movie, and gave his approval to the script, as well as to the choice of lead actor, Rod Taylor.   

Rod Taylor stepped in when the original choice for the role, Sean Connery, had to drop out. Prior to Connery, Richard Harris was attached. 
 movie version The Plough and the Stars in 1936), but he fell ill about two weeks into production and was replaced by Jack Cardiff. Filming was held up for two weeks. Only one member of the cast was replaced - Sian Phillips came in for Siobhan McKenna.  OCasey died shortly before production on the film finished. 

==Sean OCasey: The Spirit of Ireland==
During the making of the movie, a behind-the-scenes documentary, Sean OCasey: The Spirit of Ireland, was filmed looking at the making of Young Cassidy. Narrated by Herschel Bernardi, the film intersperses footage from Young Cassidy with footage of the actors preparing for their roles. 

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 