Luis Martinetti, Contortionist
{{Infobox film
| name           = Luis Martinetti, Contortionist
| image          = 
| alt            = 
| caption        = 
| film name      =  
| director       =William K.L. Dickson
| producer       =  
| writer         =  
| screenplay     = 
| story          = 
| based on       =  
| starring       = Luis Martinetti 
| narrator       =  
| music          = 
| cinematography = William Heise
| editing        = 
| studio         = Edison Manufacturing Company
| distributor    =  
| released       = 1894
| runtime        = 
| country        = USA
| language       = 
| budget         = 
| gross          =  
}}
Luis Martinetti, Contortionist is an 1894 short film produced by the Edison Manufacturing Company. The film, which runs 12.5 seconds, consists of a contortionist act performed by Luis Martinetti of the Martinetti Brothers trapeze act. Martinetti wears tiger-striped tights and performs contortionist poses on a pair of trapeze rings. 

The film was shot on October 11, 1894 at the Edison Black Maria studio in West Orange, New Jersey.  
The film is preserved by the Academy of Motion Picture Arts and Sciences, and was released on the 2000 DVD box set Treasures from American Film Archives, which was compiled by the National Film Preservation Foundation. 

==Credits==
*Directed by William K.L. Dickson
*Cast: Luis Martinetti as Himself
*Cinematography by William Heise

==References==
 

==External links==
* 
*   on YouTube
* 

 
 
 
 
 


 