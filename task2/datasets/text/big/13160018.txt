The Nickel-Hopper
{{Infobox film
| name           = The Nickel-Hopper
| image          = 
| caption        = 
| director       = F. Richard Jones Hal Yates
| producer       = Hal Roach
| writer         = Frank Butler Stan Laurel Hal Roach H. M. Walker (titles)
| starring       = Mabel Normand Oliver Hardy Boris Karloff 
| music          = 
| cinematography = Floyd Jackman Len Powers William Wessling
| editing        = Richard C. Currier	
| distributor    = Pathé Exchange
| released       =  
| runtime        = 37 mins.
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent comedy film starring Mabel Normand and featuring Oliver Hardy and Boris Karloff in minor uncredited roles.   

==Cast==
* Mabel Normand - Paddy, the nickel hopper
* Michael Visaroff - Paddys father
* Theodore von Eltz - Jimmy Jessop, Paddys rich beau
* Jimmy Anderson - Cop
* Margaret Seddon - Paddys mother
* Mildred Kornman - Edsel
* William Courtright - Mr. Joy, the landlord (uncredited) James Finlayson - Resident of 625 Park St. (uncredited)
* Oliver Hardy - Jazz band drummer (uncredited)
* Boris Karloff - Dance Hall Masher (uncredited)
* Gus Leonard - Blind man (uncredited)
* Sam Lufkin - Dance hall extra (uncredited)

==See also==
* List of American films of 1926
* Filmography of Oliver Hardy
* Boris Karloff filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 