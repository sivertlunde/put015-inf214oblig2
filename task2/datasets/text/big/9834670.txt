My Girlfriend's Boyfriend
 
{{Infobox film
| name           = My Girlfriends Boyfriend
| image          = My Girlfriends Boyfriend.jpg
| image_size     =
| caption        =
| director       = Kenneth Schapiro
| producer       = Kenneth Schapiro   Evan Seplow   L. Cyre Rodriguez (associate producer)
| writer         = Kenneth Schapiro
| narrator       =
| starring       = Deborah Gibson   Sean Runnette   Jill Novick   Valerie Perrine   Chris Bruno   Linda Larkin  Jack Koenig
| music          = Deborah Gibson
| cinematography = T.W. Li
| editing        = Evan Seplow
| studio         =
| distributor    =
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}}
My Girlfriends Boyfriend is a 1998 screwball comedy film written and directed by Kenneth Schapiro. It was filmed on Long Island, New York.

==Plot==
Gay soap actor Cliff (Bruno) is about to marry an unsuspecting girl (Larkin) for the sake of his image, to the chagrin of his boyfriend Wes (Koenig). However, a prowling reporter (Gibson) has some interesting photographs which could cause general consternation.

The main romance is between the reporter Melissa and the maid of honours uncoordinated date Jake (Runnette).

==Cast==
{| class="wikitable" cellpadding="2"
|-
! style="background-color:silver;" | Actor
! style="background-color:silver;" | Role
|-
| Deborah Gibson || Melissa Stevens
|-
| Sean Runnette || Jake
|-
| Jill Novick || Liberty, the maid of honor
|-
| Valerie Perrine || Rita Lindross
|-
| Chris Bruno || Cliff, the groom
|-
| Linda Larkin || Cory Lindross, the bride
|-
| Jack Koenig || Wes, the best man
|}

==Music==
Singer-songwriter Deborah Gibson sang the theme tune.

==External links==
*  

 
 
 
 
 
 

 