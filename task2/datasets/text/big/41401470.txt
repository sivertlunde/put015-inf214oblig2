Amar Sangi
{{Infobox film
| name           = Amar Sangi
| image          = Amarsangi.jpeg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sujit Guha
| producer       = Dipti Pal 
| writer         = 
| narrator       = 
| starring       = Prosenjit Chatterjee   Vijeta Pandit    Subhendu Chatterjee   Robi Ghosh
| music          = Bappi Lahiri
| lyrics         = 
| editor         = 
| studio         = 
| releasing date = 1987
| country        = India Bengali
}}

Amar Sangi is a 1987 Bengali film directed by Sujit Guha produced by Dipti Pal.The film features actors Prosenjit Chatterjee and Vijeta Pandit in the lead roles. Music of the film has been composed by Bappi Lahiri.This is Biggest Blockbuster film and also the remarkable film for Bengali film Industry.    

== Plot ==
Sagar (Prosenjit Chatterjee) is a boy from a rich family. Sagars father Indranil is a very big businessman. Sagar goes off to study and when he returns to his home he meets Jhilik (Vijeta Pandit), who was his friend when both were very young. When they meet each other they fall in love. When Sagars father comes to know about their relationship he disagrees because Jhilik is the daughter of Anuradha, who is their maid servant. Indranil wanted his son to marry his friends daughter Tumpa instead. Tumpas brother Suvankar wanted his sister to marry Sagar so as to seize the assets of Indranil & Sagar. When all his plans fail, Suvankar tries to kill Indranil by sabotaging his car. In the mean time, Sagar comes to know about this and finally saves his father. Later Suvankar is handed over to the police for his foul crime. Finally Indranil understands everything and agrees to have Jhilik as his daughter-in-law. In this way both Jhilik and Sagar were re-united.

==Cast==
* Prosenjit Chatterjee as Sagar
* Vijeta Pandit as  Jhilik
* Subhendu Chatterjee as Sagars Father
* Ruma Guha Thakurta as Jhiliks Mother
* Sakuntala Barua as Sagars Mother
* Robi Ghosh
* Sumitra Bandyopadhyay
* Ishani Bandyopadhyay

== Soundtrack ==
{{Infobox album  
| Name       = Amar Sangi
| Type       = Soundtrack
| Artist     = Bappi Lahiri
| Cover      = Amarsangi.jpeg
| Alt        = 
| Released   = 1987 
| Recorded   = 1987 Feature film soundtrack
| Length     =  
| Label      = 
| Producer   = Dipti Pal
| Last album = 
| This album = 
| Next album = 
}}
{| border="4" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f1f5fc; border: 1px #abd5f5 solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor=" #d0e5f5" align="center"
! Track !! Song !! Singer(s) !! Duration (min:sec) !! Music !!  Lyricist
|-
| 1 || "Jekha Nei Thako" || || || Bappi Lahiri || Pulak Banerjee 
|-
| 2 || "Mon Ta Amar Hariye Gachhe" || || || Bappi Lahiri || Pulak Banerjee
|-
| 3 || "Chiro Dini Tumi Je Amar" || || 6:45 || Bappi Lahiri || Pulak Banerjee
|-
| 4 || "Aami Mon Diyechi" ||   || Pulak Banerjee
|-
| 5 || "Tomar Mukh Ta Ki Sundar" || || || Bappi Lahiri || Pulak Banerjee
|-
| 6 || "Chirodini Tumi Je Amar(F)" || || || Bappi Lahiri || Pulak Banerjee
|}
==Awards==
 
|-
| 1988
| Lata Mangeshkar
| BFJA Awards For Best Playback Singer (Female)  
|  
|}

==References==
 

==External links==
*  
 

 
 
 
 


 