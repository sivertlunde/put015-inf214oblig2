High Hell
 
{{Infobox film
| name           = High Hell
| image          = High Hell.jpg
| caption        = 
| director       = Burt Balaban 
| producer       = Arthur Mayer
| writer         = 
| starring       = John Derek Elaine Stewart
| music          = Phil Cardew  James Wilson
| editing        = 
| distributor    = 
| released       =  
| runtime        = 87 minutes
| country        = United States United Kingdom
| language       = English
| budget         = 
}}
High Hell is a 1958 American film set in the Canadian Rockies.

==Plot==
Craig Rhodes and Frank Davidson are partners in a gold mine in Canada. Franks wife Lenore falls for Craig and i slusted after by Luke Fulgham. 

==Cast==
* John Derek as Craig Rhodes 
* Elaine Stewart as Lenore Davidson
* Patrick Allen as Luke Fulgham
* Jerold Wells  as Charlie Spence
* Al Mulock as Frank Davidson
* Rodney Burke as Danny Rhodes
* Colin Croft as Malvern, mine financier
* Nicholas Stuart as Jed, mob leader
* Dick James as Singer (voice) 

==References==
 

==External links==
* 

 