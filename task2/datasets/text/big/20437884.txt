Hardcore (1977 film)
 
{{Infobox film
| name           = Hardcore
| image          =
| image_size     =
| caption        =
| director       = James Kenelm Clarke
| producer       = Brian Smedley-Aston
| writer         = James Kenelm Clarke   Michael Robson
| cinematographer       = Mike Molloy Anthony Steel Victor Spinetti
| distributor    = 
| released       = March 1977
| runtime        =   80 minutes
| country        = United Kingdom
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}} Anthony Steel, Ronald Fraser and Harry H. Corbett. It depicts a highly fictionalised account of the life of Richmond, who was a leading pin-up in the 1970s. 

In the US the film was known as Fiona.

==Cast==
 
* Fiona Richmond as Fiona Anthony Steel as Robert Charlton
* Victor Spinetti as Duncan Ronald Fraser as Marty Kenelm-Smedley
* John Clive as Willi
* Roland Curram as Edward
* Graham Crowden as Lord Yardarm
* Graham Stark as Inspector Flaubert Percy Herbert as Hubert
* Jeremy Child as Tenniel
* John Hamill as Daniel
* Harry H. Corbett as Art
* Donald Sumpter as Mark
* Arthur Howard as Vicar
* Joan Benham as Norma Blackhurst
* Linda Regan as Secretary
 

==References==
 

 
 
 
 
 


 
 