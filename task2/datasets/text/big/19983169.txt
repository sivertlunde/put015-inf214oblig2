Old Scores
 
 
{{Infobox film
| name           = Old Scores
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Alan Clayton
| producer       = Don Reynolds
| writer         = Greg McGee Dean Parker
| narrator       = 
| starring       = 
| music          = Wayne Warlowe
| cinematography = Allen Guilford
| editing        = Michael Horton Jamie Selkirk
| studio         = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = New Zealand
| language       = English
| budget         = 
| gross          = 
}} Welsh and New Zealand international rugby players in supporting roles. Old Scores was primarily intended as theatrical release in New Zealand but was shown as a television movie in Wales.

Old Scores was directed by Alan Clayton, and the screenplay was by New Zealanders Dean Parker and Greg McGee. McGee has a strong history of dramatic works revolving around rugby, most notably his play Foreskins Lament.

==Plot==
Old Scores revolves around a controversial (fictional) rugby match between Wales and New Zealand which was won by Wales. On his death-bed, the touch judge confesses to failing to disallow the winning try for an infringement by the Welsh scorer. The Welsh Rugby Union President announces that in order to set the record straight, there should be a rematch between the two teams - using the same players who had played the match 25 years earlier. 

The teams are forced to re-assemble, each bringing along not only their 25 years of unfitness, but also various skeletons in the closet - most notably the major falling-out between two of Waless star players, Bleddyn Morgan and David Llewellyn, whose friendship had ended acrimoniously many years earlier. Morgan, had since moved to New Zealand, and initially refuses to play the match. It is later revealed that this is because of a love triangle which had developed between the two and Llewellyns fiancée Bronwen. He is persuaded to play, for the sake of his country, but there is considerable acrimony between the two players which threatens to disrupt the teams performance.
 Fred Allen). The film culminates in the replayed game, played at Cardiff Arms Park. 

According to the Helen Martin and Sam Edwards book New Zealand Film 1912 - 1996: "The dialogue is witty and characterisations are fine, if deliberately overplayed, but the ending turns the film into a shaggy-dog story."  The match ball is replaced by Waless "lucky ball", an antique taken from the Welsh Rugby Museum by Price. With the scores tied, a shot is taken at goal, but the ancient leather of the ball is not strong enough and it deflates, landing limply on the crossbar where it remains. The final scene of the film shows an official review into the match deciding that it should be replayed again.

==Cast==
{| class="wikitable"
|- 
! Cast !! Role
|-
| John Bach ||  Ewen Murray
|-
| Tony Barry ||  Barry Brown
|-
| Roy Billing ||  Frank ORiordan
|-
| Alison Bruce ||  Ngaire Morgan
|- Robert Bruce ||  Jock McBane
|-
| Terence Cooper ||  Eric Hogg
|-
| Windsor Davies ||  Evan Price
|-
| Dafydd Emyr ||  Owen Llewellyn
|-
| Howell Evans ||  Lloyd Thomas
|- John Francis ||  David Llewellyn
|-
| Peter Gwynne ||  Winston Macatamney
|-
| Glyn Houston ||  Aneurin Morgan
|-
| John Moreno ||  Referee
|-
| Beth Morris ||  Bronwyn Llewellyn
|-
| Robert Pugh ||  Bleddyn Morgan
|-
| Martyn Sanderson ||  "Acid" Aitken
|-
| Stephen Tozer ||  Jim Farquhar
|-
| Jack Walters ||  Clifford 
|-
|}
 Alex "Grizz" Dennis Hughes, Allan Martin, David Morris, David Price, Mike Roberts, J.J. Williams, and Bobby Windsor. Despite the films plot indicating that these were the teams from 1966, many of these players did not play international rugby until after this time.
 Keith Quinn.

==Advertisement==
Two of the cast of the film, Grizz Wyllie and Windsor Davies, appeared together in an advertisement for hardware chain Mitre 10 as a result of appearing in Old Scores. 

==References==
 

==Sources==
* Martin, H., & Edwards, S. (1997) New Zealand film, 1912-1996. Auckland: Oxford University Press (NZ).

==External links==
* 

 
 
 
 
 
 
 