Thanthu Vitten Ennai
{{Infobox film
| name           = Thanthu Vitten Ennai
| image          =
| image_size     =
| caption        =
| director       = C. V. Sridhar
| producer       = C. V. Sridhar
| writer         = C. V. Sridhar (dialogues)
| story          = C. V. Sridhar Vikram Rohini Rohini Manorama Manorama Bharathiraja
| music          = Ilaiyaraaja
| cinematography = Siva
| editing        = Chandran
| studio         = Chithraalaya Movies
| distributor    = Chithraalaya Movies
| released       =  
| runtime        = 134 minutes
| country        = India Tamil
}}
 Vikram and Rohini in the lead roles. This was the last film directed by the legend C. V. Sridhar.

==Soundtrack==

The film had six songs composed by Maestro Ilaiyaraaja. The lyrics by Mu. Mehta, Pulamaipithan, Thyagarajar.

{| class="wikitable"
|-
! Song title !! Singers !! Length (m:ss) 
|-
| Kangalukul  || S. Janaki || 4:37
|-
| Mannavane || S. Janaki, S. P. Balasubrahmanyam || 4:36
|-
| Muthamma || Arunmozhi, Uma Ramanan || 4:47
|-
| Thenral Nee  || S. Janaki, S.P.Balasubramaniyam || 4:26
|-
| Manasuloni  || S. Janaki || 5:50
|-
| Aathadi Allikodi  ||  || 4:34
|-
|}

 

 
 
 
 
 
 
 


 