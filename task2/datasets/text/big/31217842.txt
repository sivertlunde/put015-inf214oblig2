Bruna Surfistinha (film)
{{Infobox film
| name = Bruna Surfistinha
| image = Bruna-surfistinha-o-filme.jpg
| caption =  Official poster
| director = Marcus Baldini
| producer = Marcus Baldini Roberto Berliner Rodrigo Letier
| writer = José de Carvalho Homero Olivetto Antônia Pellegrino
| starring = Deborah Secco Cássio Gabus Mendes Drica Moraes
| music = Gui Amabis Tejo Rica Amabis
| cinematography = Marcelo Corpanni
| editing = Manga Campion Oswaldo Santana
| studio = Damasco Filmes
| distributor = Imagem Filmes
| released =  
| runtime =
| country = Brazil
| language = Portuguese
| budget = R$4–6 million   
| gross = $12,356,515 
}}
Bruna Surfistinha (also known Confessions of a Brazilian Call Girl) is a 2011 Brazilian  .  Starring Deborah Secco and Cássio Gabus Mendes, it was shot in Paulínia and São Paulo. 

==Plot==
Raquel is a girl, adopted by an upper-middle-class family, who rebelled at 17 and left her family and studies at a traditional college in São Paulo to become a prostitute, and later call girl. Shortly after starting work, she decided to write a blog about her experiences. Since some clients thought she looked like a surfer she adopted the name "Surfistinha", which means "little surfer girl". This blog became a sensation, and quickly became one of the most popular blogs in Brazil. Becoming famous, her life changed significantly. She went on to be interviewed on Brazilian talk shows similar to Oprah and David Letterman, all the while continuing her blog about her racy exploits. She wrote a book about these experiences "O Doce Veneno Do Escorpião" ("Diary of a Brazilian Call Girl").

==Cast==
* Deborah Secco as Bruna Surfistinha
* Cássio Gabus Mendes as Huldson
* Cristina Lago as Gabi
* Drica Moraes as Larissa
* Fabiula Nascimento as Janine
* Guta Ruiz as Carol
* Clarisse Abujamra as Celeste
* Luciano Chirolli as Otto
* Sérgio Guizé as Rodrigo
* Simone Iliescu as Yasmin
* Érika Puga as Mel
* Brenda Lígia as Kelly
* Gustavo Machado as Miguel
* Juliano Cazarré as Gustavo
* Rodrigo Dorado as Rominho
* Roberto Audio as Gian
* Plínio Soares as Publicitário
* Sidney Rodrigues as Tomás

== Awards and nominations ==
{| class="wikitable"
|- Award ||Category Nominated ||Result
|- 2011
|rowspan=4|Prêmio Contigo Cinema
| Best Film ||rowspan=2| Marcus Baldini ||   
|-
| Best Director ||  
|-
| Best Actress || Deborah Secco ||  
|-
| Best Supporting Actress || Fabíula Nascimento ||  
|-
|rowspan=10| 2012 Grande Prêmio Brasileiro de Cinema
| Best Film || Marcus Baldini ||  
|-
| Best Actress || Deborah Secco || 
|-
| Best Actor || Cássio Gabus Mendes ||  
|-
|rowspan=2| Best Supporting Actress|| Drica Moraes ||  
|-
| Fabíula Nascimento ||  
|-
| Best Costume Design || Letícia Barbieri ||  
|-
| Best Makeup and Hairstyling || Gabi Moraes ||  
|-
| Best Visual Effects || Eduardo Souza e Rodrigo Lima ||  
|-
| Best Adapted Screenplay || Antonia Pellegrino, Homero Olivetto e José de Carvalho ||  
|-
| Best Editing for Fiction || Manga Campion e Oswaldo Santana ||  
|}

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 