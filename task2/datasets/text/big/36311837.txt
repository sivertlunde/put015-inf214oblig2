Krazy Spooks
{{Infobox Hollywood cartoon|
| cartoon_name = Krazy Spooks
| series = Krazy Kat
| image = Krazyspooks Poster.jpg
| caption = Theatrical poster
| director = Manny Gould Ben Harrison
| story_artist = 
| animator = Allen Rose Preston Blair
| voice_actor = 
| musician = 
| producer = Charles Mintz
| studio = 
| distributor = Columbia Pictures
| release_date = October 13, 1933
| color_process = Black and white
| runtime = 5:59 English
| preceded_by = Whacks Museum
| followed_by = Stage Krazy
}}

Krazy Spooks is a short animated film distributed by Columbia Pictures and is among the theatrical cartoons featuring Krazy Kat.

==Plot==
Krazy and his spaniel girlfriend are in an automobile, riding through the countryside one evening. Suddenly their journey is cut short when their vehicle breaks down. Unable to fix it, the cat and the dog have no choice but to spend the night at an abandoned house nearby.

The house has not been inhabited for a very long period. Also, theres no water or electricity, and the place is dilapidated. While Krazy and the spaniel are walking in one of the halls, something runs underneath, flipping the loose lumbers of the floor. To their relief, it was a happy little pet bloodhound which pops out and befriends them.

The bloodhound pup steps into a certain room of the house where a skeleton falls on him. He then frantically runs to his new friends who see him as a set of living bones. The runaway skeleton goes on moving until it steps up a ladder where it gets tangled into a ceiling fan. The bloodhound finally drops out as a result.

Krazy and the spaniel continued walking in the halls, not wanting to come across more bizarre things. This was until a floating bed sheet appears before them. Krazy then takes a broom and smashes the sheet, finding out whats under the cloth was merely a daze parrot. But real trouble was met when Krazy opens a door with a vicious gorilla waiting inside.

Krazy and the spaniel try to keep the gorilla at bay but the attacking ape proves too powerful. The bloodhound comes to assist them but in vain. After the pup receives some roughhousing, however, the bloodhounds fleas, were most disturbed and therefore decided to get back at the gorilla. The gorilla started itching so much that Krazy and the spaniel started hurling pots and pans at their incapacitated foe. Driven to insanity, the gorilla finally flees the house. Krazy, the spaniel, and the little bloodhound celebrate their win with a dance.

==Availability==
*Columbia Cartoon Collection: Volume 5. {{cite web
|url=http://theshortsdepartment.webs.com/columbiacartoons.htm
|title=The Columbia Cartoons
|accessdate=2012-06-17
|publisher=the shorts development
}} 

==References==
 

==External links==
* 
*  at the Big Cartoon Database

 

 
 
 
 
 
 
 
 
 
 


 