Jerry Maguire
{{Infobox film
| name           = Jerry Maguire
| image          = Jerry Maguire movie poster.jpg
| caption        = Theatrical release poster
| director       = Cameron Crowe
| producer       = Cameron Crowe James L. Brooks Laurence Mark Richard Sakai
| writer         = Cameron Crowe
| starring       = Tom Cruise Cuba Gooding Jr. Renée Zellweger Jonathan Lipnicki Kelly Preston Jerry OConnell Jay Mohr Regina King Bonnie Hunt Nancy Wilson
| cinematography = Janusz Kamiński
| editing        = Joe Hutshing
| studio         = Gracie Films Vinyl Films
| distributor    = TriStar Pictures
| released       =  
| runtime        = 139 minutes
| country        = United States
| language       = English
| budget         = $50 million
| gross          = $273.5 million   
}}
Jerry Maguire is a 1996 American  romantic comedy-drama sports film written, produced and directed by Cameron Crowe, and stars Tom Cruise, Cuba Gooding Jr. and Renée Zellweger. It was inspired by sports agent Leigh Steinberg, who acted as Technical Consultant on the crew.     It was released in North American theaters on December 13, 1996, distributed by Gracie Films and TriStar Pictures.
 ninth top-grossing film of 1996.
 Best Picture Best Actor for Tom Cruise, with Cuba Gooding Jr. winning the Academy Award for Best Supporting Actor. The film was also nominated for three Golden Globes, with Tom Cruise winning for Best Actor, and three Screen Actors Guild Awards, with Cuba Gooding Jr. winning Best Supporting Actor.

==Plot== single mother Dorothy Boyd (Renée Zellweger) agrees. Meanwhile, Frank "Cush" Cushman (Jerry OConnell), a superstar quarterback prospect who expects to be the number one pick in the NFL Draft, also stays with Jerry after he makes a visit to the Cushman home. However, Sugar is able to convince Cushman at the last minute to sign with SMI.

After an argument, Jerry breaks up with his disgruntled fiancée. He then turns to Dorothy, becoming closer to her young son, Ray (Jonathan Lipnicki), and eventually starts a relationship with her. However, Dorothy contemplates moving to San Diego as she has a secure job offer there. Jerry concentrates all his efforts on Rod, now his only client, who turns out to be very difficult to satisfy. Over the next several months, the two direct harsh criticism towards each other with Rod claiming that Jerry is not trying hard enough to get him a contract while Jerry claims that Rod is not proving himself worthy of the money for which he asks. Jerry marries Dorothy to help them both stay afloat financially and to keep her from moving away. He is emotionally and physically distant during the marriage but is clearly invested in becoming a father to Ray. Although Dorothy loves Jerry, she breaks up with him because she believes that he does not love her.

Before the start of a Monday Night Football game between the Cardinals and the Dallas Cowboys, Rod plays well but appears to receive a serious injury when catching a touchdown. He recovers, however, and dances for the wildly cheering crowd. Afterwards, Jerry and Rod embrace in front of other athletes and sports agents and show how their relationship has progressed from a strictly business one to a close personal one, which was one of the points Jerry made in his mission statement. Jerry then flies back home to meet Dorothy. He then speaks for several minutes, telling her that he loves her and wants her in his life, which she accepts. Rod later appears on Roy Firestones sports show. Unbeknownst to him, Jerry has secured him an $11.2 million contract with the Cardinals allowing him to finish his pro football career in Arizona. The visibly emotional Rod proceeds to thank everyone and extends warm gratitude to Jerry. Jerry speaks with several other pro athletes, some of whom have read his earlier mission statement and respect his work with Rod.

The movie ends with Ray throwing a baseball up in the air surprising Jerry. Jerry then discusses Rays possible future career in the sports industry with Dorothy.

==Cast==
 
* Tom Cruise as Jerry Maguire
* Cuba Gooding Jr. as Rod Tidwell
* Renée Zellweger as Dorothy Boyd
* Kelly Preston as Avery Bishop
* Jerry OConnell as Frank "Cush" Cushman
* Jay Mohr as Bob Sugar
* Regina King as Marcee Tidwell
* Bonnie Hunt as Laurel Boyd
* Jonathan Lipnicki as Ray Boyd
* Lisa Stahl Sullivan as Jerrys Ex-Fiance
* Todd Louiso as Chad the Nanny
* Jeremy Suarez as Tyson Tidwell
* Aries Spears as Teepee Tidwell
* Mark Pellington as Bill Dooler
* Jared Jussim as Dicky Fox
* Glenn Frey as Dennis Wilburn
* Drake Bell as Jesse Remo Christina Cavanaugh as Mrs. Remo
* Toby Huss as Steve Remo
* Eric Stoltz as Ethan Valhere
* Beau Bridges as Matt Cushman
* Ingrid Beer as Anne-Louise
* Roy Firestone as Himself
 
 Poetic Justice. {{cite web|url=http://screencrush.com/jerry-maguire-cast-then-and-now/|title= What Have You Done for Me Lately?", paying homage to Jacksons hit of the same name.  

===Cameos===
  NFL quarterbacks Drew Bledsoe, Troy Aikman, and Warren Moon, German ice skater Katarina Witt, then-current Dallas Cowboys head coach Barry Switzer, and former Detroit Lions coach Wayne Fontes play themselves in the film.
 Rob Moore, Ki-Jana Carter, Herman Moore, Art Monk, Kerry Collins, and Dean Biasucci.

Sportscasters Al Michaels, Frank Gifford, Roy Firestone, Mike Tirico, and Dan Dierdorf also make cameos.

Former NBA basketball player Brent Barry is featured in the film as an athlete who refuses to sign an autograph for a young boy.

Actresses portraying ex-girlfriends of Maguire include Lucy Liu, Ivana Miličević, Alison Armitage, Emily Procter and Stacey Williams. Reagan Gomez-Preston also had a minor role in the film as part of the Tidwell family.

Alice in Chains guitarist Jerry Cantrell makes a brief appearance in the film as a copier store clerk.

Indianapolis Colts owner Jim Irsay makes a cameo as Jerry Maguires boss.

Rolling Stone publisher Jann Wenner is seen briefly as an SMI CEO as Maguire departs the company.

Artie Lange filmed a scene for the film, but it was cut from the final version of the movie.

==Product placement== commercial into the film and depicting the Reebok brand within certain agreed-upon standards; when the film was theatrically released, the commercial had been left out and a tirade including "broadsides against Reebok" was included.     When the film aired on television, the Reebok commercial had been embedded into the film as originally agreed upon. 

==Release==

===Box office=== ninth top grossing film of 1996 and the fourth highest-grossing romantic drama film of all time. 

===Critical response=== football player Best Actor Best Screenplay Best Film Editing awards.

Roger Ebert of the Chicago Sun-Times gave the film 3/4 stars, writing that there "are so many subplots that Jerry Maguire seems too full" and also commented that the film "starts out looking cynical and quickly becomes a heartwarmer."  Todd McCarthy of Variety (magazine)|Variety wrote "An exceptionally tasty contempo comedic romance, Jerry Maguire runs an unusual pattern on its way to scoring an unexpected number of emotional, social and entertaining points. Smartly written and boasting a sensational cast, Cameron Crowes shrewdly observed third feature also gives Tom Cruise one of his very best roles..."  Former Green Bay Packers vice president Andrew Brandt stated that the film "accurately portrayed the cutthroat nature of the agent business, especially the lengths to which agents will go to retain or pilfer clients. It also captured the financial, emotional and psychological investment that goes far beyond negotiating contracts."   

===Accolades===
 
Academy Awards
* Best Actor (Cruise, nominated)
* Best Editing (Hutshing, nominated)
* Best Picture (nominated)
* Best Screenplay&nbsp;– Original (Crowe, nominated)
* Best Supporting Actor (Gooding Jr., won)

Chicago Film Critics Association
* Best Supporting Actor (Gooding Jr., won)

Directors Guild of America
* Outstanding Directing&nbsp;– Motion Pictures (Crowe, nominated)

Golden Globe Awards
* Best Actor&nbsp;– Motion Picture Musical or Comedy (Cruise, won)
* Best Film&nbsp;– Musical or Comedy (nominated)
* Best Supporting Actor&nbsp;– Motion Picture (Gooding Jr., nominated)

 
Image Awards
* Outstanding Actor&nbsp;– Motion Picture (Gooding Jr., nominated)

Satellite Awards
* Best Actor&nbsp;– Motion Picture Musical or Comedy (Cruise, won)
* Best Supporting Actor&nbsp;– Motion Picture Musical or Comedy (Gooding Jr., won)
* Best Supporting Actress&nbsp;– Motion Picture Musical or Comedy (Zellweger, nominated)

Screen Actors Guild
* Outstanding Actor&nbsp;– Motion Picture (Cruise, nominated)
* Outstanding Supporting Actor (Gooding Jr., won)
* Outstanding Supporting Actress (Zellweger, nominated)

Writers Guild of America
* Best Screenplay&nbsp;– Original (Crowe, nominated)
 

===Home media===
Jerry Maguire was first released on VHS and Laserdisc on May 29, 1997. It is the best-selling non-Disney VHS tape of all time, with over 3 million copies sold on the first day and another 1 million on the second day and sold 1 million copies each day, including  , Men in Black and Starship Troopers. it was re-released on VHS around late 1999, without any of the aforementioned previews.
 Secret Garden." The film was later released onto Blu-ray on September 9, 2008, with the same special features found on the second disc of the DVD "Special Edition." 

==Legacy==
Jerry Maguire spawned several popular quotations, including "Show me the money!" (shouted repeatedly in a phone exchange between Rod Tidwell and Jerry Maguire), "You complete me," "Help me help you," "The key to this business is personal relationships" and "You had me at hello" (said by  , director and screenwriter of the film. Zellweger said of filming the famous "hello" line, "Cameron had me say it a few different ways. Its so funny, because when I read it, I didnt get it — I thought it was a typo somehow. I kept looking at it. It was the one thing in the script that I was looking at going, Is that right? Can that be right? How is that right? I thought, Is there a better way to say that? Am I not getting it? I just dont know how to do it."  , August 26, 2009.  .  Brandt stated in 2014 that "I definitely noticed an uptick of young people becoming interested in the agent business after Jerry Maguire". 

A video blog "Everything is Terrible!" is running a campaign to salvage remaining VHS copies of the movie. 
 sports genre. list of list of 100 Movie Quotes, ranked #25 and #52 respectively. 

American Film Institute Lists
*AFIs 100 Years...100 Movies&nbsp;– Nominated
*AFIs 100 Years...100 Laughs&nbsp;– Nominated
*AFIs 100 Years...100 Passions&nbsp;– #100
*AFIs 100 Years...100 Songs:
**Secret Garden&nbsp;– Nominated
*AFIs 100 Years...100 Movie Quotes:
**"Show me the money!"&nbsp;– #25
**"You had me at "hello.""&nbsp;– #52
**"You complete me."&nbsp;– Nominated
*AFIs 100 Years...100 Cheers&nbsp;– Nominated
*AFIs 100 Years...100 Movies (10th Anniversary Edition)&nbsp;– Nominated
*AFIs 10 Top 10&nbsp;– #10 Sports Film (also nominated Romantic Comedy)

In June 2010, Entertainment Weekly named Jerry Maguire one of the 100 Greatest Characters of the Last 20 Years. 

==Soundtrack==
The motion picture soundtrack CD includes:
{{Track listing
| total_length    = 54:32
| writing_credits = yes The Magic Bus
| writer1         = The Who
| length1         = 7:35
| title2          = Sitting Still Moving Still Staring Outlooking
| writer2         = His Name is Alive
| length2         = 3:21
| title3          = Gettin in Tune
| writer3         = The Who
| length3         = 4:46
| title4          = Pocket Full of Rainbows
| writer4         = Elvis Presley
| length4         = 3:15 World on a String
| writer5         = Neil Young
| length5         = 2:25
| title6          = We Meet Again" (theme from Jerry Maguire) Nancy Wilson
| length6         = 3:04
| title7          = The Horses
| writer7         = Rickie Lee Jones
| length7         = 4:48 Secret Garden
| writer8         = Bruce Springsteen
| length8         = 4:28 Singalong Junk
| writer9         = Paul McCartney
| length9         = 2:36
| title10         = Wise Up
| writer10        = Aimee Mann
| length10        = 3:29
| title11         = Momma Miss America
| writer11        = Paul McCartney
| length11        = 4:05
| title12         = Sandy Nancy Wilson
| length12        = 4:40
| title13         = Shelter from the Storm (alternate version)
| writer13        = Bob Dylan
| length13        = 6:00
}}

===Music not on the soundtrack===
Includes: 
* AC/DC&nbsp;– "For Those About to Rock (We Salute You)"
* Herb Alpert & The Tijuana Brass&nbsp;– "The Lonely Bull" 
* The Durutti Column&nbsp;– "Requiem Again"
* Nirvana (band)|Nirvana&nbsp;– "Something in the Way"
* Tom Petty&nbsp;– "Free Fallin" The Replacements&nbsp;– "Ill Be You"
* The Rolling Stones&nbsp;– "Bitch (The Rolling Stones song)|Bitch"
* Merrilee Rush&nbsp;– "Angel of the Morning"
* a clip of John Coltrane, Miles Davis, and Charles Mingus performing (Mingus piece is "Haitian Fight Song") 

"Secret Garden," originally a Bruce Springsteen track from 1995, was re-released in 1997 after its exposure in the film and on the soundtrack, and peaked at No. 19 on the Billboard Hot 100|Billboard Hot 100.  
 Nancy Wilson,  who was a member of the rock band Heart (band)|Heart.

==References==
 

==External links==
 
 
*  
*  
*  , a log kept by Crowe during the films production and published in Rolling Stone in December 1996.
*  , the memo that led Maguire to establish his own agency. Archived from   on November 9, 2012.
*  , film script (text document)
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 