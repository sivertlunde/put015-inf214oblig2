Bata, Bata… Pa'no Ka Ginawa?
{{Infobox book 
| name           = Bata, Bata… Pa’no Ka Ginawa?
| image          = Bata, Bata… Pa’no Ka Ginawa by Lualhati Bautista Book Cover.jpg
| caption  = Book cover for Lualhati Bautistas Bata, Bata… Pa’no Ka Ginawa?
| author         = Lualhati Bautista
| country        = Philippines Tagalog
| genre          = Fiction
| media_type     = 
| publisher      = Carmelo & Bauermann
| release_date   = 1988
| pages          = 
| isbn           =  
}}
 Tagalog by the Filipino female writer, Lualhati Bautista. Bautista uses "Taglish" – a mixture of Tagalog and English, instead of pure Tagalog – as a stylistic device for her works.  , an article from Firefly – Filipino Short Stories (Tulikärpänen – filippiiniläisiä novelleja), 2001 / 2007; "...Some writers like Lualhati Bautista consciously use Taglish as a stylistic device..."  The novel is about the role of a woman, like its author, with Filipino society wherein the males were, in the past, assuming more dominant roles in society.      , retrieved on: March 15, 2008    The translation of the title is literally, "Child, Child… How were you made?" although figuratively it actually surpasses its allusion – or reference – to the process of reproduction through the revelation of its true, symbolic question-message: "Child, Child… How were you molded to become a mature, grown-up person?"
 role of women were just to act upon their role as mothers who perform household chores, take care of the children, and take care of the needs of their husbands. They don’t and should not – based on previous customs – get involved with subject matters and discussions about livelihood and political issues. But the face and ambience of the perceived role of women in society changed, as society itself was transformed. The doors of offices were opened to give way to women workers. They were given a place where their complaints regarding women rights could be heard, as well as their concept about life and livelihood, earning them a voice within and outside the boundaries of home.     

This is the subject discussed and revealed by Lualhati Bautista’s novel which has 32 chapters. The work narrates the life of Lea, a working mother, who has two children – a young girl and a young boy. And for this reason, the novel depicts the society’s view of women, how it is to be a mother, and how a mother executes this role through modern-day concepts of parenthood.     

==Cast==

===Main Characters===
*Vilma Santos as Lea Bustamante - A working mother, who has two children and brought up them with her own effort. She is a woman of courage in facing her problem, and strongly believes that for every problem there is always a solution.  Lea represents the society’s view of women, how it is to be a mother, and how a mother executes this role through modern-day concepts of parenthood. 
*Ariel Rivera as Raffy - The first husband of Lea and the father of Ogie.  He is a type of person who is quiet and does not express his feelings that much.  After he left his family for his job, he came back to see Ogie, which begins the conflict of the story.
*Albert Martinez as Ding - The live-in partner of Lea and the father of Maya. He is a person who was still very close to his mother despite his age to the point of dependency.  Though he may not have performed his duties well as a replacement father for Ogie, he acted well for his role as a father of Maya.
*Raymond Bagatsing as Johnny - Closest Friend of Lea, and goes with her in any trip except the trip to baguio due to some conflicts with shifts.
*Carlo Aquino as Ojie - The only son of Lea and Raffy and the eldest child of Lea.  He was at a young age when his father left him.  He asked so many questions to his mother on why she and his father did not live together with them.  Being a teenager, he became conscious, experiments and discovers what life really is.
*Serena Dalrymple as Maya - The daughter of Lea and Ding; the youngest child of Lea.  A fresh graduate from kindergarten school and possesses intelligence and beauty.  She is a smart and outspoken six-year-old child and acts as more matured from the other girls at her age.
*Cherry Pie Picache as Sr. Ann - a nun and boss of Lea in the office where she works, and part of the woman at crisis non government group
*Angel Aquino as Elinor - Wife after lea of ogies father raffy Gascon, loving sweet and caring to her family, and willing to do everything for her family.
*Rosemarie Gil as Mrs. Zalameda
*Dexter Doria as Mrs. Gatmaitan
*Cita Astals as Mrs. Olivarez
*Andrea del Rosario as Jinky
*Lucy Quinto as Lola Sylvia
*Menggie Cobarrubias as Mr. Olivarez
*Carmen Serafin as Rosita

===Other Cast===
*Josie Tagle - Mother of Girl Crying
*Ronalisa Cheng - Girl Crying
*Jeralyn Narciso - Ms. Talent
*Cory Dela Cruz - Nun
*Joy Santos - Nun
*Marivic Suspine - Nun
*Girlie Alcantara - Lawyer
*Emma Hizola - Leas Baguio Companion
*Monette G. Quioge - Leas Baguio Companion (as Monette Quiogue)
*Loy Rabor - Leas Baguio Companion
*Nora Protacio - Rally Speaker
*Dheng Foz - Hairstylist
*Medy Sordan - Manicurist

==Plot summary==
The novel began with an introductory chapter about the graduation day from kindergarten of Maya, Lea’s daughter. A program and a celebration were held. In the beginning, everything in Lea’s life was going smoothly – her life in connection with her children, with friends of the opposite gender, and with her volunteer work for a human rights organization. But Lea’s children were both growing-up – and Lea could see their gradual transformation. There were the changes in their ways and personalities:  Maya’s curiosity was becoming more obvious every day, while Ojie was crossing the boundaries from boyhood to teenage to adulthood.     

A scene came when Lea’s former husband came back to persuade Ojie to go with him to the United States. Lea experienced the fear of losing both her children, when the fathers of her children decide to take them away from her embrace. She also needed to spend more time for work and with the organization she was volunteering for.     

In the end, both of Lea’s children decided to choose to stay with her – a decision that Lea never forced upon them. Another graduation day of students was the main event in the novel’s final chapter, where Lea was the guest-of-honor. Lea delivered a speech that discusses the topic of how life evolves, and on how time consumes itself so quickly, as fast as how human beings grow, change, progress and mature. Lea leaves a message to her audience that a graduation day is not an end because it is actually the beginning of everything else that will come in a person’s life.     

==Translation==
The excerpts from Lualhati Bautista’s novels were included in the anthology, Tulikärpänen, a book of short stories collectively written by Filipino women and was published in Finland by The Finnish-Philippine Society (FPS), a non-governmental organization established in 1988. Tulikärpänen was edited and translated by Riitta Vartti in collaboration with other authors. In Firefly: Writings by Various Authors, the English-language version of the Finnish-language collection, the featured excerpt from Bata, Bata, Pano Ka Ginawa? was given the title Childrens Party.  , retrieved on May 27, 2007    Tulikärpänen was the first book of writings by Filipino women to be published in Finland.  Vartti, Riitta (editor).  : Helsinki, Finland 2001/2007, retrieved on: April 14, 2008 

==Filmography==
This long narrative also became a film, where the Philippine actress Vilma Santos (who went on to become the first female governor of Batangas) took the role of the character Lea in 1998, together with Filipino actor Raymond Bagatsing. The transformation of the story from novel to film was under the direction of Chito S. Roño.          , retrieved on March 15, 2008 

After winning recognition by the Filipino Academy Awards (Best Picture, Best Actress, Best Screenplay and Best Director) in the Philippines, Lea’s Story – the film version of Bata, Bata… Pa’no Ka Ginawa? – was shown in Manhattan in 2000 as a part of a “bi-monthly series of Asian and Asian American film screenings at the Anthology Film Archives in New York City”. 

===Film Awards===
* Gawad URIAN
** Best Film
**  Best Actress – Vilma Santos
**  Best Supporting Actress – Serena Dalrymple
**  Best Screenplay – Lualhati Bautista
* PMPC STAR Awards
**  Best Actress – Vilma Santos
* Young Critics Circle
**  Best Film
**  Best Director – Chito Rono
**  Best Performer – Vilma Santos
**  Best Screenplay – Lualhati Bautista
* Gawad Pasado
**  Best Actress – Vilma Santos
* FAP
**  Best Film
**  Best Actress - Vilma Santos
**  Best Supporting Actor-Carlo Aquino
**  Best Supporting Actress-Serena Dalrymple
**  Best Production Design
*Brussels International Independent Film Festival
**  Best Actress - Vilma Santos

==See also==
 
*Philippine literature

==References==
 

==External links==
*  at LibraryThing.com
* 

 
 
 
 
 
 
 
 