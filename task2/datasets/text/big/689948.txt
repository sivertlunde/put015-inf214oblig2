The Horse Soldiers
{{Infobox film
| name           = The Horse Soldiers
| image          = Horse Soldiers 1959.jpg
| caption        = 1959 movie poster
| director       = John Ford
| producer       = John Lee Mahin Martin Rackin
| writer         = John Lee Mahin (screenplay) Martin Rackin (screenplay) Harold Sinclair (novel)
| starring       = John Wayne William Holden Constance Towers Althea Gibson
| music          = David Buttolph
| cinematography = William H. Clothier Jack Murray
| studio         = The Mirisch Company
| distributor    = United Artists
| released       =  
| runtime        = 115 min.
| country        = United States English
| gross          = $4 million (est. US/ Canada rentals) 
}}

:Horse Soldiers (2009) is also a book written by Doug Stanton
The Horse Soldiers is a 1959 film set during the American Civil War. It was directed by John Ford and starred John Wayne, William Holden and Constance Towers.  John Lee Mahin and Martin Rackin produced the movie and wrote its screenplay.

The film was loosely based on Harold Sinclairs novel of the same name, which in turn drew from the true story of Griersons Raid and the climactic Battle of Newtons Station.  In April 1863, Colonel Benjamin Grierson led 1700 Illinois and Iowa soldiers from LaGrange, Tennessee to Baton Rouge, Louisiana, through several hundred miles of enemy territory, to destroy railroad and supply lines between Newton, Mississippi|Newtons Station and Vicksburg, Mississippi. Griersons raid was part of the Vicksburg campaign, culminating in the Battle of Vicksburg.     The disruption of the Confederate-controlled railroad upset the plans and troop deployments of Confederate General John C. Pemberton.  General William Tecumseh Sherman reportedly considered Griersons daring mission "the most brilliant of the war". 

==Plot summary== Union cavalry Confederate lines to destroy a railroad and supply depot at Newton Station. Ironically, before the war, Marlowe had been a railroad building engineer. With the troop is a new regimental surgeon, Major Henry Kendall (William Holden) who seems to be constantly at odds with his commander. Kendall is torn between duty and the horror of war. 

Complicating matters, while the unit rests at Greenbriar Plantation, Miss Hannah Hunter (Constance Towers), the plantations mistress, and her slave Lukey (Althea Gibson) eavesdrop on a staff meeting wherein Marlowe discusses his plans. To protect the mission, Marlowe is forced to take the two women with him. Initially hostile to her Yankee captor, Miss Hunter gradually warms to him. In addition to Kendall and Miss Hunter, Marlowe also must contend with Col. Phil Secord (Willis Bouchey), who continually second-guesses Marlowes orders and command decisions.
 Confederate forces in pursuit, Marlowe and his command reach a bridge which must be stormed in order to access the Union lines. Dr. Kendall is forced to choose between remaining behind with some badly wounded men (and being captured with them), or leaving the men without medical care until the Confederates arrive.  Marlowe, wounded, is able to lead his troops over the bridge after they have set charges under it.  First, he lights the fuse and the bridge blows up to halt the Confederates once again.  He and his command continue on to their destination having successfully completed their mission.

==Cast==
* John Wayne as Colonel John Marlowe
* William Holden as Major Henry Hank Kendall
* Constance Towers as Miss Hannah Hunter of Greenbriar
* Althea Gibson as Lukey, Miss Hunters fiercely loyal black maid
* Judson Pratt as Sergeant Major Kirby
* Ken Curtis as Cpl. Wilkie
* Willis Bouchey as Col. Phil Secord
* Bing Russell as Dunker, Yankee Soldier Amputee
* O.Z. Whitehead as Otis Hoppy Hopkins (medical assistant)
* Hank Worden as Deacon Clump
* Chuck Hayward as Union captain
* Denver Pyle as Jackie Jo (rebel deserter)
* Strother Martin as Virgil (rebel deserter)
* Basil Ruysdael as The Reverend (Jefferson Military Academy)
* Carleton Young as Col. Jonathan Miles, CSA
* William Leslie as Maj. Richard Gray William Henry as Confederate lieutenant
* Walter Reed as Union officer
* Anna Lee as Mrs. Buford William Forrest as Gen. Steve Hurlburt
* Ron Hagerthy as Bugler Russell Simpson as Acting Sheriff Henry Goodbody
* Hoot Gibson as Sgt. Brown

==Production==
Exterior scenes were filmed in Natchitoches Parish, Louisiana, along the banks of Cane River Lake, and in and around Natchez, Mississippi.    A bridge was built over the Cane River for the pivotal battle scene, and many locals were used as extras.   
 The Alamo. Wimbledon and U.S. National tennis championship|U.S. National tennis champion who played the role, found offensive. She informed Ford that she would not deliver her lines as written.  Though Ford was notorious for his intolerance of actors demands,  he agreed to modify the script. 

During filming of the climactic battle scene, veteran stuntman Fred Kennedy suffered a broken neck while performing a horse fall and died. "Ford was completely devastated," wrote biographer Joseph Malham.  "  felt a deep responsibility for the lives of the men who served under him."   Though the film was scripted to end with the triumphant arrival of Marlowes forces in Baton Rouge, Ford "simply lost interest" after Kennedys death, and ended it with Marlowes farewell to Hannah at the bridge.  

Despite its two major stars, the film was a commercial failure, due largely to Waynes and Holdens high salaries, and the complex participation of multiple production companies.  The audience and critical response was "lackluster". 

==Historical accuracy==
Though based loosely on Griersons Raid, The Horse Soldiers is a fictional account that departs considerably from the actual events.  The real-life protagonist, a music teacher named Benjamin Grierson, becomes railroad engineer John Marlowe in the film.  Hannah Hunter, Marlowes love interest, has no historical counterpart.  Numerous other details were altered as well, "to streamline and popularize the story for the non-history buffs who would make up a large part of the audience." 

==See also==
*John Wayne filmography

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 