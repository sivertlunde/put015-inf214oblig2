Great Expectations (1917 film)
 
 
{{Infobox film
| name           = Great Expectations
| image          = Great Expectations (1917 film).jpg
| image_size     =
| caption        = Theatrical release poster Paul West
| producer       =
| writer         = Paul West
| based on       =   
| starring       = Jack Pickford William Marshall
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States Silent English intertitles
| budget         =
| gross          =
}}
 silent drama film directed by Robert G. Vignola and Paul West, based on the novel Great Expectations by Charles Dickens. Jack Pickford stars as Pip and Louise Huff as Estella.

==Plot==
A young boy, Pip, runs into an escaped convict at his local churchyard. Pip does favours for Magwitch (the convict), such as bringing him food etc. until Magwitch is eventually arrested and deported to Australia.
 

==Cast==
*Jack Pickford as Pip Estella
*Frank Losee as Abel Magwitch, alias Provis William Black as Joe Gargery
*Marcia Harris as Mrs. Gargery
*Grace Barton as Miss Havisham
*Herbert Prior as Mr. Jaggers

==References==
 

==External links==
 
* 
*  

 

 
 
 
 
 
 
 
 
 
 


 