Mach 2 (film)
 
{{Infobox film
| name           = Mach 2
| image          = 
| image_size     = 
| border         = mach 2 2
 
001 poster.jpg
| alt            = 
| caption        = 
| director       = Fred Olen Ray
| producer       = Robyn Stevens
| writer         = Steve Latshaw
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Brian Bosworth Cliff Robertson Shannon Whirry
| music          = Eric Wurst David Wurst
| cinematography = Thomas L. Callaway
| editing        = Randy Carter
| studio         = 
| distributor    = 20th Century Fox Home Entertainment
| released       =  
| runtime        = 94 minutes
| country        = USA
| language       = English
| budget         = $4 million
| gross          = 
}}

Mach 2 is a 2001 drama film, directed by Fred Olen Ray.

==Plot==

Stuart Davis (David Hedison), running against the Vice-President, plans a trip to the Balkans to negotiate the release of American servicemen being held hostage by terrorists. Before he leaves, he receives a disk documenting evidence that the Vice-President has been trying to revive the American economy by causing a war in the Balkans. He announces his plans at Washington Dulles International Airport to show it to both sides in the hopes of ending the situation. He boards Concorde flight 209 to Paris along with some Air Force Officers and news employees. Unexpected Secret Service agents turn up and board the Concorde. After takeoff, the secret service agents led by Barry Rogers (Michael Dorn) shoot an Air Force officer and his men hijack the Concorde in mid-flight. Rogers takes Stuart Davis and the others hostage and forces him to hand over the disk in order to prevent the war being abolished. At the cockpit, Rogers announces to the Dulles International Airport air traffic controller that the agents are armed with a nuclear device and threaten to crash the Concorde into Paris.

Air Force Officer Jack Tyree (Brian Bosworth) arrives at the cockpit (where Davis is held hostage) and overpowers one of Rogers men, accidentally shooting the co-pilot in the process. He then helps fly the Concorde and later frees Davis. Later, another agent arrives and shoots Captain Roman and takes Tyree hostage. Having brought with them two pairs of parachutes, the men jump of the plane, after revealing that there was no nuclear device aboard the aircraft. The two men land and steal a car and escape along some cliffs. On the other hand, two French Secret Service Agents that have apparently overheard the hijacking, chase Rogers and his men down the road towards a pre-prepared road block. Rogers sets an electromagnetic pulse bomb to destroy the French car, until the officer at the roadblock fires a shell at the agents, causing them to skid off the road, violently roll down the cliffs and break up, killing Rogers and his men instantly.

On the Concorde, Jack Tyree and news employee Shannon Carpenter (Shannon Whirry) attempt to fly the Concorde but Jack cant fly an aircraft and is nicknamed "washout". The aircrafts radio is damaged due to the previous fight with one of Rogers men. Since Shannon is a former mechanic, she repairs the radio and restores contact with Paris Air Traffic control. Meanwhile, having being rumoured that the agents planted a nuclear device on the Concorde, Dulles air traffic control orders a nearby aircraft carrier to launch a fighter aircraft and ordered to intercept the Concorde before it reaches Paris. On the aircraft, Shannon announces to Dulles and Paris Orly airport that there is no device on the aircraft and barely orders the fighter aircraft to abort after two near misses of its missile fires. Because of the explosion of a nearby missile fired from the fighter, the Concordes fuel line is torn and is leaking fuel, compromising the arrival to Paris.

With the plane low on fuel, Jack Tyree is instructed via radio to land the Concorde at the airport. The landing is successful and the passengers depart safely. Shannon, having hidden the actual disk in the trash, hands over the disk containing confidential files to Stuart Davis, making it possible for the war to be abolished. Jack and Shannon finally engage in a passionate kiss and the film ends, displaying the Concorde.

==Cast==
*Brian Bosworth ... Jack Tyree, an air force officer.
*Shannon Whirry ... Shannon Carpenter, a former mechanic and news employee.
*Cliff Robertson ... Vice President Pike
*Bruce Weitz ... Phil Jefferson
*Robert Pine ... Captain Roman
*Andrew Stevens ... Commander Stevens
*Michael Dorn ... Barry Rogers, leader of the Secret Service that hijack the Concorde.
*David Hedison ... Stuart Davis, a presidential candidate.
*Jennifer Hammon ... Gina Kendall
*John Putch ... Tim Mandell, a news employee
*Charles Cyphers ... Harry Olson
*Ron Chaney ... Captain Wallace
*Sondra Currie ... 	Courtney Davis, Stuart Davis wife
*Lance Guest ... Keith Dorman, a passenger
*Richard Partlow ... Jefferson Baker, Jack Tyrees old friend and one of the agents.
*Richard Gabai ... Co Pilot

==Scenery==

The scenes of the Concorde were used from the 1979 film The Concorde ... Airport 79. The interior was a full scale set replica of a Boeing 747s cabin, making the interior appear larger on the inside than the outside. If the cabin on the real Concorde was that size, its total length would have been over 120 meters and a total wingspan of at least 50 meters. The scenes of the control tower was actually an aerodrome (a small airport) control tower, not the tower from the actual Washington Dulles International Airport, as seen because of its low height and small size. On the Concorde, there were many scenes of the United States Presidential Seal on the cabin divider walls. Several movie errors were shown: the steering column clearly shown onscreen is a vertical type, unlike Concordes one is shaped like the letter "m". It also features advanced LCD primary flight displays, but Concordes display is just dials or analog style.

==External links==
* 
* 
* 
* New info on Scenery reference is http://www.imdb.com/title/tt0222020/trivia?tab=gf&ref_=tt_trv_gf

 
 
 
 