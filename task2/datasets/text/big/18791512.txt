Martial Law 2: Undercover
 
{{Infobox film
| name           = Martial Law 2: Undercover
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Kurt Anderson
| producer       = Steven Cohen 
| writer         = Richard Brandes
| narrator       = 
| starring       = Jeff Wincott Cynthia Rothrock Paul Johansson
| music          = Elliot Solomon
| cinematography = Peter Fernberger
| editing        = Michael Thibault
| studio         = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = United States
| language       = 
| budget         = 
| gross          = 
}}
 Martial Law. 

==Synopsis==
Investigating the mysterious death of a colleague, LAPD cops Sean Thompson, who is now a detective, and "Billie" Blake begin to uncover a deadly ring of murder and corruption. Their search leads to a nightclub, where the rich and powerful are entertained by a stable of beautiful girls and protected by martial arts experts hired by a ruthless crime lord. When Thompson is called off the investigation by his commander, Billie goes undercover and infiltrates the ring on her own; soon, both are facing impossible odds in a climactic battle.

==Release dates== P 14 for orchestra seats and P 18 for the balcony.  The movie would be released on videocassette in August 1992 by MCA/Universal Home Video. The film was released on DVD in Europe, by Bellevue Entertainment. It is part of a movie package that also contains Savate, Martial Law and Mission of Justice, but the film has yet to arrive onto DVD in the United States.

==Cast==
* Jeff Wincott: Det. Sean Thompson
* Cynthia Rothrock: Billie Blake
* Paul Johansson: Spencer Hamilton
* Evan Lurie: Tanner
* Charles Taylor: Dobbs (as L. Charles Taylor)
* Sherrie Rose: Bree
* Billy Drago: Captain Krantz
* Deborah Driggs: Tiffany
* Conroy Gedeon: Jones
* Kimber Sissons: Celeste
* Leo Lee: Han
* Max Thayer: Captain Banks
* John Vidor: Sonny
* Nicholas Hill: Jorge
* Dagny Hultgreen: Kristine Richards
* Ken Duncan: Brad Hamilton
* Lou Palumbo: Al Murphy
* Pat Asanti: George
* Oscar Dillon: Jones Bodyguard #1
* Rico McClinton: Jones Bodyguard #2
* Addison Cook: Bob (as Addison Cook Porter IV)
* Bridget Carney: Flash Dancer
* Michael Anthony Taylor: Talking Cop (as Michael-Anthony Taylor)
* Gregg Brazzel: Bo
* Jeffrey Scott Jensen: Plainclothes #1
* Christopher Ursitti: Cop #1
* Denice Duff: Nancy Borelli (as Denice Marie Duff)
* Lumpy Strathmore: Officer Dickens
* Matthew Powers: Rick
* Lou Voiler: Dude
* Fritz Lieber: Lenny

==References==
{{reflist|refs=
   
   
}}

 
 
 
 
 