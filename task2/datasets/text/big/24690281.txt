5 Card Stud (2002 film)
{{Infobox Film
| name           = 5 Card Stud
| image          =
| caption        = 5 Card Stud
| director       = Hank Saroyan
| producer       = Chantel Sausedo  
| writer         = Lawrence H. Toffler 
| starring       = Khrystyne Haje Lawrence H. Toffler Brian Everett Doris Hess Steven Houska Kevin McClatchy Jeffrey Vincent Parise  
| music          = 
| cinematography = Bry Thomas Sanders
| editing        = David B. Baron    
| distributor    = Warner Bros. Lightyear Entertainment
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
}}
5 Card Stud is a 2002 American romantic film starring Khrystyne Haje, Lawrence H. Toffler, Brian Everett, Doris Hess, Steven Houska, Kevin McClatchy, Jeffrey Vincent Parise, Brian Everett and directed by Hank Saroyan.

==Plot==
A bartender with a fear of commitment has limited his social life to a weekly poker game with the guys. His best friend sets him up with sure thing one night stand and the two fall in love.

==External links==
*http://www.dvdtalk.com/reviews/17897/5-card-stud/
*http://www.dvdverdict.com/reviews/5cardstud.php
* 
*http://movies.nytimes.com/movie/332181/Five-Card-Stud/overview
* 

 
 
 


 