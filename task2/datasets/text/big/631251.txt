Scum (film)
 
 
 
{{Infobox film
| name     = Scum
| image    = ScumDVD.jpg
| director = Alan Clarke
| producer = Clive Parsons Don Boyd (executive producer)
| writer   = Roy Minton John Blundell Phil Daniels Alan Igbon Ray Burdis
| distributor = Blue Underground
| released =  
| runtime = 98 minutes
| country = United Kingdom
| language = English
| budget = £250,000 Alexander Walker, National Heroes: British Cinema in the Seventies and Eighties, Harrap, 1985 p 155 
}}
Scum is a 1979 British   remade it as a film, first shown on Channel 4 in 1983.  By this time the borstal system had been reformed and eventually allowed the original TV version to be aired.

The film tells the story of a young offender named Carlin as he arrives at the institution and his rise through violence and self-protection to the top of the inmates pecking order, purely as a tool to survive. Beyond Carlins individual storyline, it is also cast as an indictment of the borstal systems flaws with no attempt at rehabilitation. The warders and convicts alike are brutalised by the system. The films controversy was derived from its graphic depiction of racism, extreme violence, rape, suicide, many fights and very strong language. 

Scum would be one of the most controversial British films of the early 1980s, but has since become regarded as a popular classic.

==Plot==
Three young men arrive at Borstal by prison van: Carlin, who has taken the blame for his brothers theft of scrap metal; Angel for stealing a car; and Davis, sent here after escaping from an open institution. All three are allocated to their rooms; Angel and Davis sent to private rooms, and Carlin sent to a dormitory.

Carlin wants to keep a low profile, having been transferred for assaulting a warder. He meets and befriends Archer, an eccentric and intellectual inmate intent on using non-violent means to cause as much of a nuisance to the staff as possible, and is informed his reputation is already known; Banks, the current "Daddy" (the inmate who "runs" the Borstal) is seeking Carlin for a fight.

Carlin struggles to settle into the dormitory, and after having watched the timid and bullied Davis be attacked by Banks, is eventually beaten by Banks and his friends in an unprovoked attack.

Carlin eventually gets his revenge on Banks. He uses a makeshift cosh from a long sock with two snooker balls inside to beat Bankss crony, then confronts Banks in the bathroom and replaces him as the "Daddy" of the ward. Carlin later acquires power over the adjacent wing of the borstal by administering a vicious beating to the adjacent wings Daddy.

Life improves for the inmates under Carlin, with victimisation of younger, weaker prisoners prevented, along with racially-motivated violence. Carlin gains status with the warders. He persuades them to move him from the dormitory to a single cell in return for an agreement to be responsible in his status as a "natural leader". Goodyear offers Carlin a position of leadership in the borstal to help him develop his leadership skills.

Another inmate, Toyne, learns through a letter from his in-laws that his wife has died, and sinks into despair, eventually slashing his wrists. After being moved to another prison, word reaches the inmates that he has killed himself in a second suicide attempt. Davis, meanwhile, is framed for theft by Eckersley and placed on report. Carlin advises Davis to avoid them; but Davis is subsequently gang-raped by three youths in a potting shed. This is seen by warder Sands who merely smiles at the rape. Davis slips into despair, and kills himself when he slashes himself in his cell at night. Whilst bleeding to death, he presses the button in his cell for help, but is ignored by warder Greaves.

Davis’ suicide causes mass hysteria within the prison, with the inmates refusing to eat their food at dinner. Carlin initiates a full-scale riot in the dinner hall. Carlin, Archer and Toynes friend Meakin are shown being dragged bleeding and unconscious into solitary confinement. The Borstals Governor later informs them the damage to the dinner hall will be repaid through lost earnings. The film ends with The Governor declaring a minutes silent prayer for Davis and Toyne.

==Cast==
 
 
*Ray Winstone – Carlin
*Mick Ford – Archer
*Julian Firth – Davis John Blundell – Banks
*Phil Daniels – Richards John Judd – Sands Philip Jackson – Greaves Peter Howell – Governor
*John Grillo – Goodyear
*Ray Burdis – Eckersley
*Alan Igbon – Meakin John Fowler – Woods
*Bill Dean – Duke
*P. H. Moriarty – Hunt
*Nigel Humphreys – Taylor
*Jo Kendall – Matron Patrick Murray – Dougan
 
*Herbert Norville – Toyne George Winter – Rhodes
*Alrick Riley – Angel
*Philip DaCosta – Jackson Peter Francis – Baldy
*Andrew Paul – Betts
*Sean Chapman – James
*Ozzie Stevens – Smith
*Ricky Wales – Chambers James Donnelly – Whittle
*Joe Fordham – Reg
*Ray Jewers – Gym Instructor
*Ian Liston – White James Lombard – Jameson
*Charles Rayford – Philpott John Rogan – Escort
*Perry Benson – Formby
 

==Deviations from original BBC production==
The film differs from the original BBC production in many respects. The film contains strong language as opposed to the milder BBC version. The violent scenes were far more graphic in the 1979 remake. Support cast members David Threlfall and Martin Philips from the original are replaced with Mick Ford and Julian Firth in the film. Also missing in the film version are Colin Mayes and Michael Deeks. The story was also changed. The BBC version features a homosexual relationship between Carlin and another inmate, which was dropped from the film. Minton later said that this was a pity as it would have expanded Carlins character and made him vulnerable in an area where he could not afford to be vulnerable.

The television play version of the film also features less graphic rape and suicide scenes. An additional scene shows Davis trying to talk to Carlin about the incident. Carlin dismisses him when he told him to talk in front of his missus (partner) but Davis refuses to because it is too personal, then he  commits suicide. In the remake, the relationship between Carlin and his missus doesnt feature. Instead, during the mess, Davis looks up at Carlin from the dining table as if about to confide in him, but Carlin unwittingly chooses that moment to get up and leave.

Also in the television play, its made clear Banks is in hospital – resulting from the beating administered by Carlin when ousting him as the "daddy" – at the end of the film; in the theatrical version he is present at the end moment of silence.

==Response== High Court case against Channel 4 for showing the film, British morality campaigner Mary Whitehouse initially won her private prosecution,  but the decision was later reversed on appeal. The Independent Broadcasting Authority had approved the films transmission.

In 2010, a Canadian film heavily based on Scum, titled Dog Pound (film)|Dog Pound, was released. 

==Home media releases==
The film was first released on VHS video in the UK in 1983, where it was immediately caught up in the UK Video nasty controversy of the early 1980s.
 BBC version with an audio commentary and two interviews. Disc Two instead featured the theatrical remake with an audio commentary, several interviews and featurettes and two trailers. It was digitally remastered from a widescreen print. This special edition DVD was sold in amaray slipcase packaging and also in a limited edition tin case. A Region 0 DVD boxset featuring both the theatrical version and the 1977 BBC-banned television version on separate discs followed in the US, released by Blue Underground. 

==References==
 

==External links==
*  at the British Film Institute|BFIs Screenonline
* 

 

 

 
 
 
 
 
 
 
 
 