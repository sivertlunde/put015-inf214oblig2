A Reason to Live, a Reason to Die
{{Infobox film
| name = A Reason to Live, a Reason to Die
| image = Reason to live.jpg
| alt = Una Ragione Per Vivere E Una Per Morire
| caption =  A Reason to Live, a Reason to Die DVD cover
| director = Tonino Valerii
| producer = Michael Billingsley  Tullio Odevaine  Alfonso Sansone  Arthur Steloff
| writer = Rafael Azcona Ernesto Gastaldi Tonino Valerii
| starring = James Coburn Telly Savalas Bud Spencer
| music = Riz Ortolani
| cinematography = Alejandro Ulloa
| editing = Franklin Boll Franco Fraticelli
| distributor = K-Tel International Corporation
| released =  
| runtime = USA: 92 min
| country = Italy English
| box office gross =
| followed_by =
}}
 1972 Technicolor Italian spaghetti western movie starring James Coburn.
 800 Bullets. The Deserter. 

==Cast==
* James Coburn as Col. Pembroke
* Bud Spencer as Eli Sampson
* Telly Savalas as Major Ward
* Reinhard Kolldehoff as Sergeant Brent
* José Suarez as Major Charles Ballard
* Georges Géret as Sergeant Spike
* Ugo Fangareggi as Ted Wendel
* Guy Mairesse as Donald MacIvers
* Benito Stefanelli as Piggott
* Adolfo Lastretti as Will Fernandez
* Ángel Álvarez as Scully the Monger

==Plot==
This is a story derivative of The Dirty Dozen, but set during the American Civil War. Eli is to be hanged for looting, when he and some other men condemned to death are pardoned, providing that they follow former US colonel Pembroke in an attempt to recapture Ft Holman, which he earlier had surrendered to the Confederate army.

The "volunteers" are a deserter who have killed two sentries, a man who has murdered his commanding officer and raped the wife of the victim, a horse-thief, two looters, one who has stolen medicine so soldiers have died, and an Indian ”bastard” who has killed a white man that sold liqueur to the Apaches. However, the one presented as ”the worst of the bunch” – a religious pacifist agitator - declines the offer and is hanged!

Pembroke holds the group together by saying that he really is after a treasure of gold hidden inside the fort. At Ft Holman Eli gains entrance as a Confederate orderly in a stolen uniform. He realises that there is no gold, and also learns that the present commander Ward used Pembroke’s son to blackmail him into giving up the fort. Then Ward killed the son anyway. When Eli is exposed he produces a paper (also stolen) to show that he is a security officer, and criticises the lack of security. Ward plans to save his own hide by executing the "security officer,” but the others are let in by Eli and attack the garrison. After the battle only Pembroke and Eli stand as survivors. They leave together.

==Reception==
In his investigation of narrative structures in Spaghetti Western films, Fridlund discusses A Reason to Live, a Reason to Die mostly in terms of the "infiltrator" plot introduced in A Fistful of Dollars, where The Man With No Name joins a gang with hidden agendas of his own. Eli is an infiltrator entering the fort and piling one false motive on top of the other to cover his true intentions. In fact, the same goes for Pembroke - pitting his rather involuntary companions against the Confederates with a false monetary motive beside the official, to re-conquer Ft Holman for the Union, while his real hidden motive is vengeance. 

==Home media==
Wild East has released the full uncut version with around 30 minutes extra footage on an out-of-print limited edition R0 NTSC DVD in the films original widescreen aspect ratio with the title A Reason to Live, a Reason to Die.

==See also==
List of films shot in Almería

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 