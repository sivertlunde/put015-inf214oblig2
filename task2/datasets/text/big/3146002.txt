Sunday Bloody Sunday (film)
 
 
 
{{Infobox film
| name = Sunday Bloody Sunday
| image = Sundaybloodysunday.jpg
| alt = 
| caption = Theatrical release poster
| director = John Schlesinger
| producer = Joseph Janni Edward Joseph
| writer = Penelope Gilliatt
| starring = Murray Head Glenda Jackson Peter Finch Peggy Ashcroft
| music = Ron Geesin Billy Williams
| editing = Richard Marden
| studio = Vectia
| distributor = United Artists
| released =  
| runtime = 110 minutes
| country = United Kingdom
| language = English
}}
Sunday Bloody Sunday is a 1971 British drama film written by Penelope Gilliatt, directed by John Schlesinger and starring Murray Head, Glenda Jackson and Peter Finch. It tells the story of a free-spirited young bisexual artist (played by Head) and his simultaneous relationships with a female recruitment consultant (Jackson) and a male Jewish doctor (Finch).

The film is significant for its time in that Finchs homosexual character is depicted as successful and relatively well-adjusted, and not particularly upset by his sexuality. In this sense, Sunday Bloody Sunday was a considerable departure from Schlesingers previous film Midnight Cowboy, which had portrayed its gay characters as alienated and self-loathing.
 Bloody Sunday."

==Synopsis==
A Jewish doctor, Daniel Hirsh (Peter Finch) and a young woman, Alex Greville (Glenda Jackson) are both involved in a love triangle with contemporary sculptor Bob Elkin (Murray Head). Not only are Hirsh and Greville aware that Elkin is seeing the other but they know one another through mutual friends. Despite this, they are willing to put up with the situation through fear of losing Elkin, who switches freely between them.

For Greville, the relationship is bound up with a growing disillusionment about her life, failed marriage and uneasy childhood. For Hirsh, it represents an escape from the repressed nature of his Jewish upbringing. Both realise the lack of permanence about their situation and it is only when Elkin decides to leave the country that they both come face to face (for the first time in the narrative and at the end). Despite their opposed situations, both come to realize that it is time to move on.

==Cast==
* Peter Finch as Dr. Daniel Hirsh
* Glenda Jackson as Alex Greville
* Murray Head as Bob Elkin
* Peggy Ashcroft as Mrs. Greville
* Tony Britton as George Harding
* Maurice Denham as Mr. Greville
* Bessie Love as Answering Service Lady
* Vivian Pickles as Alva Hodson
* Frank Windsor as Bill Hodson
* Thomas Baptiste as Professor Johns Richard Pearson as Patient
* June Brown as Woman Patient 
* Hannah Norbert as Daniels Mother
* Harold Goldblatt as Daniels Father
* Russell Lewis as Timothy Hodson
* Marie Burke as Aunt Astrid
* Caroline Blakiston as Rowing Wife 
* Peter Halliday as Rowing Husband 
* Jon Finch as Scotsman
* Robert Rietti as Daniels Brother
* Douglas Lambert as Man at Party 
* Nike Arrighi as Party Guest Edward Evans as Husband at Hospital 
* Gabrielle Daye as Wife at Hospital
* Petra Markham (uncredited) as Designers girlfriend 
* Daniel Day-Lewis (uncredited) as Child vandal John Warner (uncredited) as Party Guest

==Production notes==
* Alan Bates was the original choice made by John Schlesinger for the role of Daniel Hirsh, the gay doctor. However he was held up filming The Go-Between (1970) and was replaced first by Ian Bannen, who dropped out after two weeks filming, and later by Peter Finch. However, the role of Daniel was written as that of a much younger man.
* Several actresses (including Dame Edith Evans and Thora Hird) politely refused the part of Glenda Jacksons mother, Mrs. Greville, because they thought the project was too risqué. Peggy Ashcroft accepted after the director explained to her the elements of the story and she gladly signed on.
* Ian Bannen was fired from the role of Daniel Hirsh shortly after filming began. Apparently, he was so nervous about what kissing another actor onscreen might do to his career, he could not concentrate enough to even get going with the part. He later said that losing the role set back his career, and regretted it till his death. his local church.

==Music== source music trio Soave sia il vento from Wolfgang Amadeus Mozart|Mozarts opera Così fan tutte.

==Reception==
The film currently holds a 91% Fresh rating on Rotten Tomatoes. 

This film appeared on both Roger Eberts and Gene Siskels Top 10 list of 1971. Listed as No. 5 and No. 6 respectively. Roger Ebert commented, "The official East Coast line on John Schlesingers Sunday Bloody Sunday was that it is civilized. That judgment was enlisted to carry the critical defense of the movie; and, indeed, how can the decent critic be against a civilized movie about civilized people? My notion, all the same, is that Sunday Bloody Sunday is about people who suffer from psychic amputation, not civility, and that this film is not an affirmation but a tragedy...I think Sunday Bloody Sunday is a masterpiece, but I dont think its about what everybody else seems to think its about. This is not a movie about the loss of love, but about its absence."

==Awards and nominations== Best Actress Best Director Best Original Screenplay (Penelope Gilliatt). Gilliatt won several Best Screenplay awards for the film, including the New York Film Critics Circle Award, Writers Guild of America, and Writers Guild of Great Britain.
 Best Director for Schlesinger.

==References==
 

==External links==
*  
*  
*  

 
 
{{succession box Golden Globe for Best Foreign Film
| years=1972 Women in Love The Emigrants}}
 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 