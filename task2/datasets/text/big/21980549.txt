Conquest of the Air
{{Infobox film
| name           = Conquest of the Air
| image          = 
| border         = 
| caption        = 
| director       = Zoltan Korda John Monk Saunders Alexander Esway William Cameron Menzies Alexander Shaw Peter Bezencenet
| producer       = Alexander Korda
| writer         = John Monk Saunders Hugh Gray Antoine de Saint-Exupery
| starring       = Laurence Olivier
| music          = Arthur Bliss
| cinematography = 
| editing        = 
| studio         = Alexander Korda Productions
| distributor    = London Films (UK) United Artists (US)
| released       = December 1936 (UK) 20 May 1940 (US release of revised edition)
| runtime        = 71 minutes
| country        = United Kingdom
| language       = English 
| budget         = 
| gross          = 
}} commercial and military aviation; including the early stages of technology developments in design, propulsion, and air navigation aids. The film was a London Films production, commissioned by the Air Ministry of the British Government.

==Production background== navigational equipment, and the transitions between civil and military developments, including heavy bombers; fast fighter aircraft; and the advent of naval aviation (aircraft carrier), plus the initial experiments with vertical rotary flight (helicopters).

An updated version was released in 1940, and released in the United States on 20 May 1940. 

==References==
 

==See also==
*List of films in the public domain

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 


 
 