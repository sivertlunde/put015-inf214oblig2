The Life Before Her Eyes
{{Infobox film
| name           = The Life Before Her Eyes
| image          = Life before her eyes.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Vadim Perelman
| screenplay     = Emil Stern
| based on       =  
| starring       = Uma Thurman Evan Rachel Wood Eva Amurri Brett Cullen Gabrielle Brennan
| music          = James Horner
| cinematography = Paweł Edelman David Baxter
| studio         = 2929 Entertainment
| distributor    = Magnolia Pictures
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = $13 million
| gross          = $7,248,490
}}
 thriller film directed by Vadim Perelman. The screenplay was adapted by Emil Stern from the Laura Kasischke novel of the same name. The film stars Uma Thurman and Evan Rachel Wood. It was released on April 18, 2008, and revolves around a survivors guilt from a Columbine High School Massacre|Columbine-like event that occurred 15 years previously, which causes her present-day idyllic life to fall apart.

==Plot==
Imaginative, impetuous, and wild Diana McFee (Evan Rachel Wood) cannot wait for her adult life to begin. While awaiting the final days of high school in the lush springtime, Diana tests her limits with sex and drugs as her more conservative friend Maureen (Eva Amurri) watches with concern. Then the two teens are involved in a Columbine High School massacre|Columbine-like shooting incident at their school and are forced to make an impossible choice.

The film mostly focuses on Diana’s adulthood (Uma Thurman). She leads an apparently normal life as an art history university professor. She has a daughter, Emma (Gabrielle Brennan), and she’s married to the professor who once gave a speech in her school about the power of visualization, how one can shape one’s own future in this way. We learn, however, that she feels guilty about something that doesn’t let her sleep.

One day she gets a call from Emma’s school. The nuns who run the school complains about her behavior. Later, at an ice cream parlor, Diana asks Emma not to hide any more as she is always doing. “You hate me,” Emma replies to her mothers reproaches. They leave the parlor abruptly and as they’re about to get into the car, Diana sees her husband, the professor, with another woman. She hesitates about confronting him and instead remains in the middle of the street where she is hit by a pickup truck. On her way to the hospital she imagines that blood is escaping from her body. However in fact she hasn’t been hurt by the accident. What is happening is that she is remembering the complications she had following an abortion in her high school days.

The day of the 15th anniversary of the shooting, a memorial is held at the school. Diana drives in front of the school several times until she finally decides to stop and bring in some flowers. As she enters the school she’s asked whether she’s one of the survivors. She smiles and walks inside, first leaving flowers on some desks and then moving on to the rest rooms where one of the shootings took place. At that moment she gets a call from Emma’s school; Emma is missing. Diana is told that a pink piece of clothing has been found in the woods. She drives there and walks through the woods, shouting out her daughters name. Emma appears before Diana’s eyes for a moment but then vanishes almost as soon as she has appeared.

Back to the rest rooms in the school where Diana left the flowers. This is the place where she and Maureen were forced to decide which of them should survive. Maureen had offered herself first, but the shooter, Michael Patrick (John Magaro), asks Diana why it should not be her; she agrees and asks to be killed. Diana is then shot and dies, followed by Michael, who offs himself. All that we’ve seen earlier in the film is what Diana had dreamed her adult life would be like. Emma is the girl she never had, the girl that she aborted.  As the film ends Diana is asked once again if she is a survivor and she replies "No" with a smile, with a sense of relief that she did the right thing by dying and having her friend live her life.

==Cast==
* Uma Thurman as Diana McFee
* Evan Rachel Wood as young Diana McFee
* Eva Amurri as Maureen
* Brett Cullen as Paul McFee
* Gabrielle Brennan as Emma McFee
* Adam Chanler-Berat as Ryan Haswhip
* Oscar Isaac as Marcus
* Maggie Lacey as Amanda
* Nathalie Paulding as young Amanda
* Jewel Donohue as Mother Tanner Max Cohen as Nate Witt
* Lynn Cohen as Sister Beatrice
* John Magaro as Michael Patrick
* Molly Price as Dianas mother
* Isabel Keating as Maureens mother
* Mike Slater as young Tom

==Reception==
The Life Before Her Eyes received a generally negative response; as of August 2011, Rotten Tomatoes reported that 24% of critics had given the film positive reviews, based on 35 reviews – with the consensus being the film is "Despite earnest performances, Life Before Her Eyes is a confusing, painfully overwrought melodrama."  Metacritic reported the film had an average score of 32 out of 100, based on 15 reviews. 

===Box office===
The film opened in limited release on April 18, 2008, in the United States and grossed $20,220 in eight theaters its opening weekend, averaging $2,527 per theater. As of Jun 27–29, 2008, it had a domestic total gross of $303,439, and a production budget of $13 million. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 