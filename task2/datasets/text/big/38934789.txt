Strictly Modern
{{Infobox film
| name           = Strictly Modern
| image          = Strictly_Modern_1930_Poster.jpg
| caption        = Theatrical poster
| director       = William A. Seiter
| producer       =
| writer         = Play: Hubert Henry Davies Screenplay: Ray Harris Gene Towne
| starring       = Dorothy Mackaill Sidney Blackmer Julanne Johnston Warner Richmond
| music          =
| cinematography = Sidney Hickox
| editing        =
| distributor    =  
| released       = March 2, 1930 (USA)
| runtime        = 63 minutes
| country        = United States English
| budget         =
}}
Strictly Modern  (1930 in film|1930) is an talkie|all-talking Pre-Code Hollywood|pre-code comedy film, produced and released by First National Pictures, a subsidiary of Warner Bros. and directed by William A. Seiter. The movie stars Dorothy Mackaill and Sidney Blackmer. The film was based on the play, entitled Cousin Kate, written by Hubert Henry Davies.

==Cast==
*Dorothy Mackaill as Kate 
*Sidney Blackmer as Heath Desmond 
*Julanne Johnston as Aimee Spencer 
*Warner Richmond as Judge Bartlett 
*Mickey Bennett as Bobby Spencer 
*Kathrin Clare Ward as Mrs. Spencer 

==Synopsis==
Kate is a novelist who writes "modern" novels about sex, romance and relationships. She thinks that since she is a strictly modern women she knows everything about men. When she falls in love, she plans to act exactly like the heroines in her novels and expects her future boyfriend to do likewise. Kate attempts to apply the methods that uses in his books to her own life and the lives of those around her. As the film begins, we find out that Mackaills cousin, Aimee (Johnston) is about to be married to Heath Desmond (Blackmer). 

Two days before their marriage Aimee, apparently a prude, tells Heath that there will be no passion in their marriage and that they will strictly observe the sanctity of the Sabbath. Heath, quickly realizing what is in store for him, deserts Aimee and takes the next train out of town. Judge Bartlett (Richmond), who was to marry the couple, consoles Aimee. Meanwhile, Kate, who is on her way to the expected wedding, meets Heath on the train. Not knowing who he is, Kate quickly falls in love with him. 

When Kate learns that Heath is her cousins fiancé, she pretends that she had only been flirting because she thinks that falling in love with a man who is about to marry someone else is not appropriate. She vows to act in the proper way, the way in which the characters in her "modern" novels would. She does her best to bring Heath and Aimee back together again, even though she still loves Heath. 

Kate manages to get them to the altar, but just before the marriage is solemnized Kate, realizing that Aimee is in love with Judge Bartlett, gives the judge a drug. The judge faints and Aimee declares her love for the judge. When the judge recovers, he is married to Aimee, leaving Kate and Heath free to pursue their romance.

==Preservation status==
No film elements are known to survive. The soundtrack, which was recorded on Vitaphone disks, may survive in private hands.

==See also==
*List of lost films

==External links==
* 

 

 
 
 
 
 
 
 
 