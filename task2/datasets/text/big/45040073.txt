Homely Meals
{{Infobox film
| name           = Homely Meals
| image          = 
| narrated by  = 
| caption        = Film poster
| director       = Anoop Kannan
| producer       = Hussain Illias Zakkariya
| writer         = Vipin Atley
| starring       = Vipin Atley Rajesh Sharma Neeraj Madhav Srinda Ashab
| music          = Sartaj Bijibal
| cinematography = Georgy Joseph 
| editing        = Babu Ratnam
| narrator       = 
| studio         = Recard Pictures
| distributor    = LJ Films
| released       = 
| runtime        = 142 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}

Homely Meals is a 2014 Malayalam film written by Vipin Atley and directed by Anoop Kannan (who previously directed Jawan of Vellimala), starring Vipin Atley, Rajesh Sharma, Neeraj Madhav and Srinda Ashab. The film received positive reviews upon release. It was appreciated for its novel theme, directorial value and technical expertise. 

==Plot==

Homely Meals is about the journey of Alan (Vipin Atley), an odd looking youngster who is crazy about films and wants to make a mark in the visual media. However, his pedestrian looks place stumbling blocks towards his dream.

==Cast==
* Vipin Atley as Alan
* Srinda Ashab as Nanditha
* Rajesh Sharma as Mosappan
* Neeraj Madhav as Arun
* Shreerej as Praveen
* Thommy as Dicrooze
* Jain Paul as Sree Sankar
* Basil Joseph as Editor Basil
* Manoj K. Jayan as Sarath Chandran
* Nedumudi Venu as Father Dickson Kailash as Sajith Ram
* Sabitha Anand as Alans mother
* Sreelatha Namboothiri as Alans grandmother
* Babu Antony as himself
* Sunil Sukhada as Parameswaran Nair
* Sudheer Karamana as Aruns father
* Sasi Kalinga as Chourone Chettan
* Sreejith Ravi as Bike passenger
* Anil Murali as CI
* Shivaji Guruvayoor as Roy Chemmachen
* Vaishak Uthaman as a man in BAR
* Dinesh as Lalan

==Reception==
The film received positive reviews upon release. It was appreciated for its novel theme, directorial value and technical expertise. Veeyen of Nowrunning.com rated the film   and wrote: "Frequently delightful, insanely insightful and brimming with tongue in cheek humour, Homely Meals carries us along on the journey of a man, out to make a mark in the world."  Paresh C Palicha of Rediff.com rated the film   and wrote, "Homely Meals is watchable for its earnest performances."  Deepa Soman of The Times of India gave a   rating and said, "Its through comedy that the film scores most of its brownie points. The actors, many of them new, have gotten under the skin of their characters. Overall, the film does not disappoint and definitely is a one-time watch." 

==References==
 

==External links==
*  

 