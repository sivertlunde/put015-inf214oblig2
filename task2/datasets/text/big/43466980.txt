The Sawdust Trail
{{infobox film
| title          = The Sawdust Trail
| image          =
| imagesize      =
| caption        =
| director       = Edward Sedgwick
| producer       = Carl Laemmle Hoot Gibson
| writer         = William Dudley Pelley(story) Raymond L. Schrock(adaptation) E. Richard Schayer(scenario)
| starring       = Hoot Gibson
| music          =
| cinematography = Virgil Miller
| editing        =
| distributor    = Universal Pictures
| released       =  
| runtime        = 60 minutes; 6 reels
| country        = US
| language       = Silent (English intertitles)

}}
The Sawdust Trail is a lost  1924 silent Western film produced and distributed by Universal Pictures and starring Hoot Gibson. Edward Sedgwick directed. 

==Cast==
*Hoot Gibson - Clarence Elwood Butts
*Josie Sedgwick - "Calamity" Jane Webster
*David Torrence - Jonathan Butts
*Charles K. French - Square Deal McKenzie
*Harry Todd - Quid Jackson
*G. Raymond Nye - Gorilla Lawson
*Pat Harmon - Ranch Foreman

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 


 
 