Howrah Bridge (film)
{{Infobox Film
| name           = Howrah Bridge
| image          = Howrah Bridge 1958 film poster.jpg
| image_size     = 
| caption        = Film poster
| director       = Shakti Samanta
| producer       = 
| writer         = Vrajendra Gaur
| narrator       = 
| starring       = Madhubala Ashok Kumar K. N. Singh Om Prakash
| music          = O. P. Nayyar
| lyrics         = Qamar Jalalabadi
| cinematography = 
| editing        = 
| distributor    = Shakti Films
| released       = 1958
| runtime        = 153 min.
| country        = India
| language       = Hindi
| budget         = 
}}
Howrah Bridge is a 1958 film directed by Shakti Samanta.  Its name is a reference to Howrah Bridge, which connects Howrah to Kolkata over the Hooghly River. It features the well known Hindi song "Mera Naam Chin Chin Chu"which was reused in the film Salaam Bombay!. This song was sung by Geeta Dutt and picturised on Helen Jairag Richardson|Helen.  Director Samanta also said that the sensuous club number "Aaiye Meherbaan" picturized on Madhubala was also very memorable.  The Music Director is O. P. Nayyar. The songs of this film were penned by the famous lyricist and Urdu poet Qamar Jalalabadi. The film performed "above average" at the box office. 

There is an intertextual reference to the song  "Mera Naam Chin Chin Chu" in Karan Johars 1998 film "Kuch Kuch Hota Hai" (just before the song Koi Mil Gaya).

==Story==
Rakesh lives with his brother, Madan, and his dad in Rangoon. Madan has fallen into bad company and steals the family heirloom, in the shape of a dragon, some cash and runs away to India. Shortly thereafter, his dead body is found under Calcuttas Howrah Bridge. Rakesh travels to Calcutta in order to find out the mystery behind Madans death, and also try to recover the family heirloom. His investigations take him to a shady hotel run by Uncle Joe, where an attractive dancer named Edna performs a dance every night. Rakesh finds himself attracted to Edna, and he confides in her why he is here and she decides to assist him. Rakesh comes to know that a man named Bhiku had witnessed his brothers murder, and sets off to find him. Before he could meet him, Bhiku goes missing and no one knows about his whereabouts. Watch what Rakesh does next in order to solve this mystery, recover the heirloom, continue to woo Edna, and catch his brothers killers.

==Cast==

* Madhubala	 ...	Edna
* Ashok Kumar	 ...	Prem Kumar / Rakesh or Vikram
* K. N. Singh	 ...	Pyarelal
* Om Prakash	 ...	Shyamu, Tangewala
* Madan Puri	 ...	John Chang Dhumal	 ...	Uncle Joe
* Kammo	         ...	Chhamia
* Sunder	 ...	Bhiku / Bhikarilal
* Brahm Bhardwaj ...	Madan & Rakeshs dad
* Helen (actress)| Helen Jairaj Richardson  ...	Dancer / Singer
* Mehmood	 ...	Wedding Dancer
* Minoo Mumtaz	 ...	Wedding Dancer
* Chaman Puri	 ...	Madan
* Nirmal Kumar   ...   CID officer.

== Music ==
* Aaiye Meherbaan  (Asha Bhosle)
* Dekh Ke Teri Nazar  (Mohammed Rafi & Asha Bhosle)
* Eent Ki Dukki Paan Ka Ikka  (Mohammed Rafi)
* Gora Rang Chunariya Kaali  (Mohammed Rafi & Asha Bhosle)
* Main Jaan Gayi Tujhe Saiyan  (Mohammad Rafi & Shamshad Begum)
* Mera Naam Chin Chin Chu (Geeta Dutt)
* Mohabbat Ka Hath Jawaani Ka Palla  (Mohammed Rafi & Asha Bhosle)
* Yeh Kya Kar Daala Tune (Asha Bhosle)

== References ==
 

==Other links== BFI Film & TV Database

== External links ==
*  

 
 
 
 
 