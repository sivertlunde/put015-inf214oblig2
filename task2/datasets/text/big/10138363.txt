Old Bones of the River
{{Infobox Film
| name           = Old Bones of the River
| image          = Old bones river.JPG
| image_size     =
| caption        = 
| director       = Marcel Varnel
| producer       = 
| writer         = Marriott Edgar Val Guest J.O.C. Orton
| narrator       = 
| starring       = Will Hay Graham Moffatt  Moore Marriott
| music          = 
| cinematography = Arthur Crabtree
| editing        = R.E. Dearing   Alfred Roome
| distributor    = 
| released       = 1938
| runtime        = 90 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} 1938 starring British actor Will Hay with Moore Marriott and Graham Moffatt and directed by Marcel Varnel. The film is a spoof of the 1935 movie, Sanders of the River, and has been described as "the most comprehensive trashing of the British Empire ever put on celluloid." 

==Synopsis==
Hay plays Professor Benjamin Tibbetts, representative of the Teaching & Welfare Institution for the Reformation of Pagans (otherwise known as T.W.I.R.P for short), and dedicated to spreading education amongst the natives of colonial Africa.
 gin still into the country by a local prince.  Eton collars alongside their native garb. Tibbetts dons a mortarboard and safari shorts due to the heat.

When the Commissioner falls ill with a dose of malaria, Tibbetts is forced to take over his duties, which include collecting the taxes. Upriver, he finds an old paddlesteamer operated by Harbottle (Moore Marriott) and Albert (Graham Moffatt); the threesome rescue a baby from death by sacrifice.

They also manage to precipitate a native uprising...

==Cast==
*Will Hay as Professor Benjamin Tibbetts
*Moore Marriott as Jerry Harbottle
*Graham Moffatt as Albert Robert Adams as Bosambo Jack London as MBapi
*Wyndham Goldie as Commissioner Sanders
*Jack Livesey as Captain Hamilton

==Notes==
 

==External links==
* 
*  

 
 

 
 
 
 
 
 
 
 
 
 
 


 