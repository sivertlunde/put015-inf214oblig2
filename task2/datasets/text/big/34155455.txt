Dhavani Kanavugal
{{Infobox film
| name = Dhavani Kanavugal
| image = DhavaniKanavugalfilm.jpg
| caption = LP Vinyl Records Cover
| director = K. Bhagyaraj
| writer = K. Bhagyaraj Radhika
| producer = K. Bhagyaraj
| music = Ilaiyaraaja
| cinematography = A. Varmakrishnan
| editing = A. Selvanathan
| studio = Praveena Film Circuit
| distributor = Praveena Film Circuit
| released = 14 September 1984
| runtime = 148 minutes
| country = India Tamil
| budget =
}}
 1984 Tamil film written, directed and produced by K. Bhagyaraj and starring Sivaji Ganesan and K. Bhagyaraj.  

==Plot==
This story revolves around an unemployed youngster who tries to become rich for his big family. Bhagyaraj is an unemployed gold medalist with 5 younger sisters, and a mother. He is unable to find a job in spite of his education, and is supported by his sisters, and landlord, Sivaji Ganesan. After many failures, he goes in Chennai in search of job, and meets Raadhika. Is he able to find a job? Is he able find bridegrooms for his sisters? The story portrays many of the existing social conditions in the society including unemployment of educated youth and dowry.

==Cast==
* K. Bhagyaraj Radhika
* Nithya (actress)
* Ilavarasi
* Poornima Rao
* Sivaji Ganesan  
* P. Bharathiraja   Parthiban  

==Soundtrack==
The music composed by Ilaiyaraaja. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Maamoi Maamoi || S. P. Balasubrahmanyam || Vairamuthu || 03:38
|- Vaali || 05:33
|-
| 3 || Sengamalam Sirikuthu || S. P. Balasubrahmanyam, S. Janaki || Kuruvikkarambai Shanmugam || 04:22
|-
| 4 || Vaanam Niram || S. P. Balasubrahmanyam, S. Janaki || Muthulingham || 04:14
|}

==References==
 

 

 
 
 
 
 


 