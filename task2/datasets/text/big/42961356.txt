Alt (film)
{{Infobox film
| name = Alt
| image = Alt2013poster.jpg Alejandro Hernández
| producer = Cecilia Nava	 
| writer = Alejandro Hernández
| starring = Edmary Fuentes Gustavo Finol
| music  = Alejandro Hernández
| cinematography = Luis Betancourt
| editing = Alejandro Hernández
| studio = Mestizo / Roto Producciones
| released =  
| runtime = 6 minutes
| country = Venezuela
| language = Spanish
}} Alejandro Hernández.  It tells the story of a woman whose perfect life spirals out of control after everything around her starts to mysteriously vanish.

== Plot ==
Sofía is living the life she always wanted: a beautiful child, a successful husband and a nice house. One day, she wakes up with a series of visions she cant explain. Eventually, her family and every object that represents an emotional connection to her starts to dematerialize one by one.

==Cast==
*Edmary Fuentes as Sofía
*Gustavo Finol as Jorge

== Reception ==
===Critical response===
The film received mostly favorable reviews from critics. Richard Propes of The Independent Critic gave the film 3.5 stars out of 4, stating that Alt was "unquestionably what happens when you assemble the right cast and crew to bring a story to life in a way that is both intelligent and inspired", and that Hernández "transcends budgetary limitations with clarity of vision and a clear and concise story."  Loida Garcia of Rogue Cinema praised the film, calling it "one of the best short films I have ever watched", while also complimenting the films quality and wrote that it is "on par with all major motion pictures."  Mark Bell of Film Threat gave the film a "fresh" review on Rotten Tomatoes  and said "A film with an intriguing idea at its core." He criticized the films ambiguousness and said that "the audience is left to do quite a bit of interpretation, and I can imagine some running wild with the option, while others just remaining confused." 

== References ==
 

== External links ==
*  
* 
* 

 
 
 
 
 
 
 

 