Bambai Raat Ki Bahon Mein
{{Infobox film
| name           = Bambai Raat Ki Bahon Mein
| image          = 
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| film name      = 
| director       = Khwaja Ahmad Abbas
| producer       = Khwaja Ahmad Abbas
| writer         = 
| screenplay     = 
| story          = Khwaja Ahmad Abbas
| based on       =  
| narrator       =  David Abraham Jalal Agha
| music          = J.P. Kaushik	  Hasan Kamal  (lyricist) 
| cinematography = Ramachandra
| editing        = Mohan Rathod 
| studio         = Naya Sansar
| distributor    = 
| released       = 1967 
| runtime        = 136 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1968 suspense crime-triller Hindi film written, produced and directed by Khwaja Ahmad Abbas. The film starred Vimal Ahuja, Surekha, David, Irshad Panjatan, A.K. Hangal, Madhukar, Kuljit Pal and debutantes Jalal Agha and Persis Khambata in major roles.    
 1967 National Film Awards the film won the National Film Award for Best Cinematography for its cinematographer Ramachandra   

==Cast==
* Vimal Ahuja  as  Amar Kumar
* Surekha  as  Asha
* Madhavi  as  Rosy David Abraham  as  Barrister Rameshchand
* Persis Khambatta  as Lily / Leela 
* Prakash  as  Tikam / Toto
* Yunus Parvez as Chaiwala
* Akhtar Siraj 
* Jalal Agha as Johnny/Joseph 
* A.K. Hangal as Sonadas Doleria
* Prithviraj Kapoor as Himself
* Irshad Panjatan as Sevakram

==Soundtrack==
The film has music by J.P. Kaushik and lyrics by Hasan Kamal. 

* Bambai raat ki bahon mein - Asha Bhosle
* Jalti hui jawanian yeh unkahi kahaniyan  - Mahendra Kapoor
* Usne jo kaha mujse ek geet suna do na - Sulakshana Pandit

== References ==

 
*  

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 