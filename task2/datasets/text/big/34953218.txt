Issa le Tisserand
 

{{Infobox film
| name           = Issa le Tisserand
| image          =
| caption        =
| director       = Idrissa Ouedraogo
| producer       = Mario Gariazzo Les Films de lAvenir Ministère de la Coopération (Francia)
| writer         =
| starring       = Ernest Ouedraogo Assétou Sanfo
| distributor    =
| released       =  
| runtime        = 20 minutes
| country        = Burkina Faso France
| language       =
| budget         =
| gross          =
| screenplay     =
| cinematography = Sékou Ouedraogo
| sound          = Issa Traoré
| editing        = Arnaud Blin Christine Delorme
| music          = Moustapha Thiombiano
}}

Issa le Tisserand  is a 1983 Burkinabé film.

== Synopsis ==
Issa is a traditional weaver in Burkina Faso who loves his trade. However, so as not to lose his clientele and keep on making enough to keep his wife, Issa finds himself forced to sell Western clothes. He buys a mask, ready-to-wear clothing and… makes a fortune. Shot in 1984, a year after Thomas Sankara’s rise to power, the film perfectly illustrates the Chief of State’s motto: “Let’s use products from Burkina Faso!” A political and philosophical tale intended to warn his fellow-citizens.

== References ==
 
*  

 
 
 
 
 