The Unnamable II: The Statement of Randolph Carter
 
{{Infobox Film |
| name = The Unnamable II:  The Statement of Randolph Carter
| image = 
| caption = 
| director = Jean-Paul Ouellette
| writer = Jean-Paul Ouellette H. P. Lovecraft (short story)
| starring = Mark Kinsey Stephenson Charles Klausmeyer Maria Ford John Rhys-Davies Julie Strain
| music by = David Bergeaud
| producer = Jean-Paul Ouellette Alexandra Durrell
| distributor = Prism Entertainment Corporation (VHS) Lions Gate Films Home Entertainment (DVD)
| released = March, 1993
| runtime = 104 min. English
| budget = 
}} The Unnamable, short story of the same name, also by Lovecraft.
 Professor Warren, David Warner plays the dean of the university.

David Warner is also featured in the film Necronomicon (film)|Necronomicon, starring alongside Jeffrey Combs, who plays Lovecraft himself.

==Plot==
The film opens outside the Winthrop house from the first film, only this time it is swarming with police officers and medical technicians. Howard is being wheeled into an ambulance because he has three deep gashes in his chest, Tanya is put into a police car, and Randolph is carrying Joshua Winthrops book of spells, which he gives to Howard for safe keeping. Randolph confronts the Dean of the university about the house, who tells him not to dabble in things that he could never understand. Then Randolph goes to Professor Warren, who agrees to help.

Howard is dragged along and the three go the spot where Randolph erupted from the ground in the first film. Howard is to stay near the car to keep guard. Eventually, Warren and Carter find Alyda, Joshua Winthrops demon daughter (Joshua Winthrop appears to Howard in a dream at some point to confess that he caused his daughters evilness) wrapped up in the roots of the tree that dragged Alyda out from the house in the first film. Warren injects the monstrous being with insulin to rid her body of the demon. This plan works, and she transforms into a beautiful woman, naked and wrapped in the tree roots. She is given sugar to bring her out of the insulin overdose, and the pair free her from her bonds. The demon is still in the caves, though, and it begins to hunt Alyda down so that they can be one again. After a showdown in the Arkham Library, Randolph manages to defeat the demon, but Alyda dies simultaneously.

==Critical reaction==

In their book  ,   fairy tale. That said, Unnamable II is still a fun ride for anybody who has ever tried smuggling a member of the opposite sex into their dorm room after a long night in a graveyard." 

==References==
 

==External links==
* 

 

 
 
 
 
 
 