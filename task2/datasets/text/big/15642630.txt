Porky's Last Stand
{{Infobox Hollywood cartoon
|cartoon_name=Porkys Last Stand
|series=Looney Tunes (Daffy Duck/Porky Pig)
|image=
|caption=
|director=Bob Clampett
|story_artist=Warren Foster
|animator=Izzy Ellis Dave Hoffman Norm McCabe Vive Risto	
|background_artist=
|layout_artist=
|voice_actor=Mel Blanc
|musician=Carl Stalling, Milt Franklyn
|producer=Leon Schlesinger
|distributor=Warner Bros. Pictures The Vitaphone Corporation
|studio=Warner Bros. Cartoons
|release_date=1940
|runtime=
|movie_language=English
}}
Porkys Last Stand is a 1940 public domain cartoon directed by Bob Clampett.

==Plot==
Porky Pig owns a restaurant with the help of his assistant, Daffy Duck. But its trouble when the mice steal all their food. Daffy tries to get a calf for food, but he accidentally grabs a bull. The bull chases Daffy around and Daffy tries to stop him. Daffy goes to tell Porky and Porky sees he is not kidding. So Porky and Daffy do all they can to stop him.

==See also==
*Daffy Duck
*Porky Pig
*Looney Tunes and Merrie Melodies filmography

==External links==
 

 
 
 
 
 
 
 
 
 