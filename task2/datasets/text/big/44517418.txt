Woman Against Woman
{{Infobox film
| name           = Woman Against Woman
| image          = Woman Against Woman poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Robert B. Sinclair 	
| producer       = Edward Chodorov
| screenplay     = Edward Chodorov
| story          = Margaret Culkin Banning
| starring       = Herbert Marshall Virginia Bruce Mary Astor Janet Beecher Marjorie Rambeau
| music          = William Axt
| cinematography = Ray June
| editing        = George Boemler 
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Woman Against Woman is a 1938 American drama film directed by Robert B. Sinclair and written by Edward Chodorov. The film stars Herbert Marshall, Virginia Bruce, Mary Astor, Janet Beecher and Marjorie Rambeau. The film was released on June 24, 1938, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Herbert Marshall as Stephen Holland
*Virginia Bruce as Maris Kent
*Mary Astor as Cynthia Holland
*Janet Beecher as Mrs. Holland
*Marjorie Rambeau as Mrs. Kingsley
*Juanita Quigley as Ellen
*Zeffie Tilbury as Grandma
*Sarah Padden as Dora
*Betty Ross Clarke as Alice
*Dorothy Christy as Mrs. Morton
*Morgan Wallace as Morton
*Joseph Crehan as Senator Kingsley

== References ==
 

== External links ==
*  

 
 
 
 
 
 

 