A Short Film About John Bolton
{{Infobox film | name = A Short Film About John Bolton
 
 | caption = American DVD Cover
 | director = Neil Gaiman
 | producer = Matthew Vaughn David Reid Adam Bohling |
 | writer = Neil Gaiman
 | starring =John OMahony Marcus Brigstocke
 | music =Chris Ewan
 | cinematography = John Pardue
 | editing = David Martin
 | distributor = 2003 (US) April 2003 (UK) |
 | runtime = 27 minutes
 | language = English
 | budget =
 }}
 2003 film John Bolton (but played in the film by actor John OMahony). It was released direct to video, along with several bonus features.

== Plot ==
  Jonathan Ross (playing himself)).

Bolton appears to review the placement of the paintings before the opening. Eccentric and detached, Bolton is uncomfortable with the amount of attention being paid to him. Forced to give a speech at that evenings party (where guests include the real John Bolton in a cameo appearance), Bolton quietly states that he simply "paints what he sees."

Following the gala, Bolton is interviewed at home by Brigstocke. Bolton again proves elusive with answers about his art, though he does (reluctantly) agree to have his work habits filmed for the first time (though only by the Interviewer, working without his crew).

As dusk approaches, Bolton takes the Interviewer to his studio, located in the basement of an ancient monastery and graveyard. As the hours drag on, Bolton shows no signs of getting started (he says he is waiting), and the Interviewer finally leaves. Filming himself as he walks out of the graveyard, the Interviewer spots two ghostly women (one with zebra stripes running up her leg) moving towards him. The camera falls to the ground, and the film closes on Boltons latest work: a pale woman, with zebra stripes running up her leg, feasting on human flesh.

== Development ==
* Gaiman claimed to have got the idea for the film after writing an introduction to a collection of Boltons art, which took the form of a fictional biography of the artist.
* Bolton gave permission for the fictional film, and not only provided all of the paintings shown in the movie, but painted a new one based on the films finale.
* The zebra stripes on the womans leg are actually a tattoo.
* The film is similar to H. P. Lovecrafts short story "Pickmans Model."
* Guests at the art launch were friends and colleagues of Gaimans who he asked to take part, among them SF writer Colin Greenland and Starburst writer Anthony Brown. Little acting was required as the wine served was real.

== External links ==
*  
 

 
 
 
 
 
 
 
 


 