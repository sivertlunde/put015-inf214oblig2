Attack of the Normans
{{Infobox film
| name           = Attack of the Normans 
| image          = Attack-of-the-normans-movie-poster-1962-1020558137.jpg
| caption        = 
| director       = Giuseppe Vari
| producer       = 
| writer         = Nino Stresa
| screenplay     = 
| story          = 
| based on       =   Cameron Mitchell Geneviève Grad Ettore Manni Philippe Hersent
| music          = Roberto Nicolosi
| cinematography = Marco Scarpelli Vittorio Storaro
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Italy
| language       = Italian
| budget         = 
| gross          = 
}}
Attack of the Normans ( ) is a 1962 Italian film set in England in the early 9th century.  Viking incursions play a central role in the plot; "Normans" in the title is used in its original continental sense, meaning Viking.

This film was written by Nino Stresa and directed by Giuseppe Vari.  

==Cast== Cameron Mitchell as "Wilfred, Duke of Saxony"
*Geneviève Grad as "Svetania"
*Ettore Manni as "Olivier DAnglon"
*Philippe Hersent as "James"
*Piero Lulli as "Barton" Paul Müller as "Thomas"
*Franca Bettoia as "Queen Patricia"

==References==
 

==See also==
* list of historical drama films

==External links==
*  
   

 
 
 
 
 
 
 
 


 