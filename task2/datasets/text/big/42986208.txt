¡Tango!
{{Infobox film
| name           = ¡Tango!
| image          = Tango 1933 film poster.jpg
| alt            = 
| caption        = Poster for the film
| director       = Luis Moglia Barth
| producer       = 
| writer         = Luis Moglia Barth and Carlos de la Pua 
| starring       = Libertad Lamarque, Pepe Arias and Tita Merello
| narrator       = 
| music          = Roberto Firpo, Homero Manzi, Sebastián Piana, Rodolfo Sciammarella and Freddy Scorticati
| cinematography = Alberto Etchebehere
| editing        = 
| studio         = Argentina Sono Film
| distributor    = 
| released       =  
| runtime        = 
| country        = Argentina
| language       = Spanish
| budget         = 
| gross          = 
}}
¡Tango! is a 1933 Argentine musical romance film, the first film to be made in Argentina using optical sound technology (but not the first sound film.) 
Many existing stars of the Argentine stage and radio appeared in the film, but its success was limited due to poor sound quality and weak acting. 
¡Tango! established a formula that would be used by many subsequent tango films.

==Production==

The 80-minute black and white film was directed by Luis Moglia Barth, who co-wrote the script with Carlos de la Pua. 
It was Argentina Sono Films first production.  
The film showed strong Hollywood influence in its cinematic techniques. 
It was the first optical sound feature film to be produced in Argentina, at Argentina Sono Films new optical sound studio.   }}

The stars included the singer and actress Libertad Lamarque, the stage actor Pepe Arias, the singer Azucena Maizani and the comedienne Tita Merello, all well-known theatre or tango performers. 
The film featured the tango orchestras of Juan de Dios Filiberto, Osvaldo Fresedo and Pedro Maffia.  
¡Tango! was released in Argentina on 27 April 1933. 

==Synopsis==

¡Tango! follows a formula established by Carlos Gardel with films such as Luces de Buenos Aires (The Lights of Buenos Aires, 1931) in which a melodramatic story is interspersed with tango songs. 
However, the film had less dialog and more music, making it more like a musical revue.
This format would be copied by many subsequent films. 

The plot is derived from tango songs. 
Many of these songs tell of the seduction of an innocent slum girl by a rich man who promises her a glamorous life, but who abandons her when her looks fade. 
The stylized and sentimental plot of ¡Tango!  revolves around a young man who is abandoned by his girlfriend for an older rich man and is heartbroken.
The film follows his misfortunes. 
The final scene has the hero, dressed as a typical compadrito, singing Milonga del 900.  The song, by Carlos Gardel, ends:
  I love her because I love her
and therefore I forgive her.
There is nothing worse than rancor
to live in bitterness.   

==Reception==

The approach of hiring well known performers ensured that devotees of popular theater and of radionovelas would form a ready audience for sound films.
Luis Sandrini, who played "the poor kid from the barrio, immature and insecure," became the first Argentine film star. 
However, ¡Tango!  had poor sound quality, which made it less successful than it should have been given its star-studded cast.   This seems to be incorrect.  }}

==Complete cast==
The complete cast was: 
 
* Libertad Lamarque as Elena
* Pepe Arias as Pepe el Bonito
* Tita Merello as Tita
* Alberto Gómez as Alberto
* Alicia Vignoli as Alicia
* Luis Sandrini as Berretín
* Meneca Tailhada as Mecha
* Juan Sarcione as Malandra
* Azucena Maizani as herself
* Mercedes Simone as herself
* Juan dArienzo 
* Juan de Dios Filiberto 
* Edgardo Donato 
* Osvaldo Fresedo 
* Pedro Maffia 
 

==References==
Notes
 
Citations
 
Sources
 
*{{cite book|ref=harv
 |last=Falicov|first=Tamara Leah|title=The Cinematic Tango: Contemporary Argentine Film
 |url=http://books.google.com/books?id=tQMVumtKr9YC&pg=PA11|accessdate=2014-06-07
 |year=2007|publisher=Wallflower Press|isbn=978-1-904764-92-2}}
*{{cite web|ref=harv|url=http://letras.com/carlos-gardel/396373/|year=1933
 |last=Gardel|first=Carlos|title=Milonga Del 900|accessdate=2014-06-07}}
*{{cite book|ref=harv
 |last=Hernandez-Rodriguez|first=R.|title=Splendors of Latin Cinema
 |url=http://books.google.com/books?id=10GU26GSKDAC&pg=PA140|accessdate=2014-06-07
 |date=2009-11-19|publisher=ABC-CLIO|isbn=978-0-313-34978-2}}
*{{cite book|ref=harv
 |last1=Karush|first1=Matthew B.|last2=Chamosa|first2=Oscar|title=The New Cultural History of Peronism: Power and Identity in Mid-Twentieth-Century Argentina
 |url=http://books.google.com/books?id=IXQ-EW2IVBsC&pg=PA39
 |date=2010-04-30|publisher=Duke University Press|isbn=0-8223-9286-0}}
*{{cite book|ref=harv
 |last=Karush|first=Matthew B.|title=Culture of Class: Radio and Cinema in the Making of a Divided Argentina, 1920–1946
 |url=http://books.google.com/books?id=6-1DK1CwGeAC&pg=PA75|accessdate=2014-06-07
 |date=2012-05-15|publisher=Duke University Press|isbn=0-8223-5264-8}}
*{{cite book|ref=harv
 |last1=Kuhn|first1=Annette|last2=Radstone|first2=Susannah|title=The Womens Companion to International Film
 |url=http://books.google.com/books?id=pjqOM04aGJ8C&pg=PA20
 |year=1990|publisher=University of California Press|isbn=978-0-520-08879-5}}
*{{cite book|ref=harv
 |last=Rist|first=Peter H.|title=Historical Dictionary of South American Cinema
 |url=http://books.google.com/books?id=PrOMAwAAQBAJ&pg=PA548|accessdate=2014-06-07
 |date=2014-05-08|publisher=Rowman & Littlefield Publishers|isbn=978-0-8108-8036-8}}
* |url=http://www.imdb.com/title/tt0024642/
 |title=¡Tango!|work=IMDb|accessdate=2014-06-06}}
 

 

 
 
 
 