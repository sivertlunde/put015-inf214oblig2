Days of Wrath (2013 film)
{{Infobox  film name           = Days of Wrath  image          = File:Days_of_Wrath(2013)_poster.jpg director       = Shin Dong-yeob  producer       = Oh Pil-jin   Cho Yoon-jung writer         = Shin Dong-yeob   Yoon Joon-hee starring       = Yang Dong-geun   Joo Sang-wook  music          =   cinematography =    editing        =  distributor    = 9ers Entertainment released       =   runtime        = 103 min. country        = South Korea language       = Korean budget         =  admissions     =  gross          =    
}}
Days of Wrath ( ; lit. "The Punisher") is a 2013 South Korean film about a man who, having been bullied as a teenager, seeks revenge against the former classmate who ruined his life.   

==Plot==
Chang-sik bullied Joon-seok relentlessly during high school. And his girlfriend was raped by Chang-sik and committed suicide on the next day. Fifteen years later, the two encounter each other again. Chang-sik is working for a chaebol|conglomerate, and preparing for his wedding. On the other hand, because of his traumatic experience, Joon-seok has a difficult time getting a decent job even though he graduated from a prestigious university; he works part-time at a convenience store. Unable to forget, Joon-seok prepares for revenge to make Chang-sik pay.

==Cast==
*Yang Dong-geun - Chang-sik
*Joo Sang-wook - Joon-seok
*Lee Tae-im - Ji-hee
*Jang Tae-sung - Doo-joon
*Ban Mi-jung - Mi-ok
*Na Hyun-joo - Hyun-joo
*Kim Kwon - young Joon-seok
*Kang Dae-hyun - young Chang-sik
*Seo Joon-yeol - young Doo-joon 
*Kang Bok-eum - So-eun 
*Kim Ram-ho - Scar 
*Choi Hong-il - Uncle
*Han Cheol-woo - Department head 
*Son Kang-gook - Home room teacher 
*Kim Ji-eun - Seon-mi 
*Han Yoo-mi - Hye-jin 
*Kim Bom - Yoon-kyeong 
*Jeon Gook-hwan - Chang-siks father (cameo)
*Seo Dong-soo - History teacher (cameo)

==Film Festival==
In July 2014, the film was selected to be shown in the 2014 Fantasia International Film Festivalat Montreal, Canada.> {{cite web|title=International Premiere: Days of Wrath 
("Eungjingja")|url=http://www.fantasiafestival.com/2014/en/films-schedule/366/days-of-wrath}} 

==References==
 

==External links==
*    
*  
*  

 
 
 
 
 
 


 