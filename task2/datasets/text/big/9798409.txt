Theresienstadt (film)
{{Infobox Film
| name           = Theresienstadt. Ein Dokumentarfilm aus dem jüdischen Siedlungsgebiet
| image          = 
| image_size     = 
| caption        =  Hans Günther & Karl Rahm
| producer       = Karel Peceny (Aktualita Prag) for the SS-Central Office for the Settlement of the Jewish Question in Bohemia and Moravia
| writer         = Kurt Gerron using drafts by Jindrich Weil and Manfred Greiffenhagen 
| narrator       = 
| starring       = 
| music          = 
| cinematography = Ivan Fric and Cenek Zahradnícek
| editing        = 
| distributor    = 
| released       = 1944 (unreleased)
| runtime        = ca. 90 minutes (surviving footage: 20 minutes)
| country        = Nazi Germany German
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Theresienstadt. Ein Dokumentarfilm aus dem jüdischen Siedlungsgebiet ( ) was a black-and-white projected Nazi propaganda film shot in the concentration camp of Theresienstadt.
 Danish Red Cross by taking them on a tour of the Theresienstadt concentration camp in the occupied Czech Republic. They "beautified" and cleaned the camp prior to arrival and arranged cultural activities to give the appearance of a happy, industrious community. To cover up the endemic overpopulation of the camp, numerous inmates were deported to Auschwitz before the arrival of the Red Cross delegation. 
 Hans Günther attempted to expand on it by having Kurt Gerron, a Jewish actor-director, make a short film about the camp to assure audiences that the inmates kept there were not being abused.  In return, the Nazis promised that he would live. Shooting took 11 days, starting September 1, 1944. {{Cite web
|url=http://www.yadvashem.org/yv/en/holocaust/about/03/terezin.asp
|title=This day in Jewish history / Filming in Theresienstadt
|publisher= Haaretz Daily Newspaper Ltd 
|work= 
|accessdate=2013-06-12
}} 

Shortly after Gerron finished shooting the film, however, both he and other cast members were "evacuated" to Auschwitz, where they were gassed upon arrival. 
 Vatican would be given screenings. However, the progress of the war in late 1944 to early 1945 made that impossible. After an initial screening in early April 1945 to senior members of the government and SS, there were a few other screenings to international humanitarian groups in Theresienstadt in April 1945. Further distribution was halted by the defeat of Germany. Brad Prager, "Interpreting the Visible Traces of Theresienstadt", Journal of Modern Jewish Studies, 7:2, 175-194, 2008, p.178. 
 
The film was mostly destroyed, but about 20 minutes of sequences from it have survived.  The surviving footage features a childrens opera, Brundibar, and two musical performances on a wooden pavilion in the town square. One is of Karel Ančerl conducting a work by Pavel Haas, and the other is of the jazz band leader Martin Roman and his Ghetto Swingers. Ančerl and Roman both survived Auschwitz; most of their musicians and the children from the opera did not.

The "Beautification", the Red Cross tour, and the making of the film are dramatized extensively in the novel and mini-series War and Remembrance. Austerlitz (novel)|Austerlitz, a novel by W.G. Sebald, features discussion of and a still from the film. It is also explored in documentary film The Given Town. 

== See also == List of films made in the Third Reich
*Potemkin village
*Ioanid gang|Reconstituirea, a Romanian Communist film in which Jewish prisoners were made to reenact their alleged crimes.

==References==
 

== External links ==
* 
* 
* 
* 
*  

 
 
 
 
 
 