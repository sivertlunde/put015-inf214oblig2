Jeewan Jyoti (1953 film)
{{Infobox film
| name           = Jeewan Jyoti
| image          = Jeewan Jyoti 1953.jpg
| image_size     = 
| caption        = 
| director       = Mahesh Kaul
| producer       = 
| writer         =Mahesh Kaul, G.D. Madgulkar
| narrator       = 
| starring       =Shammi Kapoor Chand Usmani Shashikala Leela Mishra 
| music          = Sachin Dev Burman
| cinematography = Dwarka Divecha, Anwar Pabani
| editing        = Sri Anekar, M.S. Haji
| distributor    = 
| released       = 1953
| runtime        = 98 min
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1953 Bollywood film directed by Mahesh Kaul. It is the debut film of Shammi Kapoor.    It stars Shashikala, Leela Mishra, and Chand Usmani. 

==Cast==
*Shammi Kapoor as Sham Sunder/Shammi
*Shashikala as Leela	
*Leela Mishra as Ganga&nbsp;– Shammis Mother
*Chand Usmani as Kishori
*Amir Banu Dulari as Jamuna 	
*S.N. Banerjee as Chhote Lal
*Nazir Hussain as Dr. Abdul Hamid
*Moni Chatterjee as Master Dinanath&nbsp;– Kishoris Father Sarita 			
*Rekha 	 		
*Jagdish 		
*Jyoti  			

== Soundtrack ==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#eeeeee" align="center"
! Song !! Singer (s)
|-
| Saari khushiyan saath aayi aap jab
| Shamshad Begum
|-
| Chhayi kaari badariya, bairaniya ho raam
| Lata Mangeshkar
|-
| Man sheetal naina sufal jodi yugal
| Lata Mangeshkar
|-
| Chandni ki paalki mein baithkar koi
| Asha Bhonsle
|-
| So ja re so ja meri ankhiyon ke
| Lata Mangeshkar
|-
| Lag gayi ankhiyan o mere baalam
| Geeta Dutt, Mohd. Rafi
|-
| Balma ne man har leena
| Asha Bhonsle
|}
==References==
 

==External links==
*  

 

 
 
 
 