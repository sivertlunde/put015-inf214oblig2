The Missing Gun
{{Infobox film
| name           = The Missing Gun
| image          = The Missing Gun poster.jpg
| image_size     =
| caption        = Promotional poster for The Missing Gun
| director       = Lu Chuan
| producer       = Wang Zhonglei
| writer         = Lu Chuan based on a novelette by Fan Yiping
| narrator       =
| starring       = Jiang Wen Ning Jing Wu Yujuan
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 8 May 2002
| runtime        = 87 min. China
| language       = Guizhou dialect
| budget         =
| preceded_by    =
| followed_by    =
}}
 Chinese black directorial debut of Lu, the film premiered during the 9th Beijing Student Film Festival on 21 April 2002. A pioneer digital screening was subsequently held in Shanghai on 28 April, making The Missing Gun the first film screened in China with digital cinema technology.  The film was officially released on 8 May in Beijing.

Adapted from a novelette by Fan Yiping, the film revolves around a small-town policeman who embarks on a search for his missing gun. The film also explores the themes of self-identity and self-respect, as well as addresses a number of pertinent social issues, such as counterfeits, in China.

==Plot==
 Shi Liang). Ma knocks on Zhous door but is greeted by Li Xiaomeng (Ning Jing), Mas former lover who married and moved to Guangzhou, but has since returned. Li claims that Zhou is not home. Ma leaves but manages to run into Zhou a moment later. After searching Zhous car, he ascertains that Zhou has not taken his gun either.

Ma is left with no choice but to report the missing gun to his superiors. He is stripped of his uniform but is allowed to continue the search on his own. During his search, Ma comes across a thief who has snatched a womans handbag and gives chase. The desperate thief, seeing no escape, pulls out a gun. Mistaking the gun for his own, Ma refuses to back down. The thief fires, only to reveal that the gun he holds is a fake.

Meanwhile, a murder has taken place in town. A man has sneaked into Zhous home and shoots Li with the missing gun. Upon interrogation, Zhou insists that he was not the intended target. However, Ma investigates on his own and reveals that Zhou runs an underground factory which produces counterfeit wine. Ma then retains Zhou, exchanges clothes with him, and boards a bus heading out of town. True to his prediction, the criminal trails Ma, thinking that he is Zhou, shoots him from behind at the railway station but fails to kill him instantly. As the criminal approaches to confirm his kill, Ma turns over and locks the two of them together with his handcuffs.
 freeze frame.

==Cast==
* Jiang Wen as Ma Shan, a smalltown policeman who wakes up one day to find his gun missing
* Ning Jing as Li Xiaomeng, a resident of the town, Mas first love
* Wu Yujuan as Han Xiaoyun, Mas wife, a teacher at a local school Shi Liang as Zhou Xiaogang, a local businessman

==Reception==
The Missing Gun first premiered during the   on 8 May, the film recorded more than  .  It was subsequently selected for the 59th Venice Film Festival, though it failed to clinch any awards.
 Chinese National Language and Character Committee for using the Guizhou dialect instead of Standard Chinese. The committees director Yuan Guiren disapproved Jiang Wens selection of dialects as language media for two consecutive films (the previous being Devils on the Doorstep), claiming that such a choice misguides the audience on the importance of learning Standard Chinese, a standard adopted by the Chinese government. 

===Awards and nominations===
* Beijing Student Film Festival, 2002
** Best Directorial Debut
* Golden Trailer Awards, 2003
** Best Foreign Film (nominated)

==DVD release== French and Portuguese was released on 4 May 2004 and distributed by Sony Pictures Entertainment in North America.

==References==
 

==External links==
*  
*  
*  

 

 

 
 
 
 
 
 
 
 
 
 