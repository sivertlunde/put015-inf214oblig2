Mr. Robinson (film)
 
{{Infobox film
 | name = Mr. Robinson -Il Signor Robinson: mostruosa storia damore e davventure-
 | image =   Mr. Robinson (film).jpg
 | caption =
 | director = Sergio Corbucci
 | writer =   Paolo Villaggio   Castellano e Pipolo  Sergio Corbucci
 | starring =  Paolo Villaggio, Zeudi Araya
 | music =  Guido & Maurizio De Angelis
 | cinematography = Marcello Gatti
 | editing = Amedeo Salfa
 | producer =   Franco Cristaldi  runtime = 107 min country = Italy language =Italian released = 1976
 }}
 1976 Cinema Italian comedy film directed by Sergio Corbucci. It is a parody of the Daniel Defoes novel Robinson Crusoe.      

== Plot ==
The Milanese fashion guru Roberto Minghelli embarks with his wife on a cruise. One morning he wakes up in a now-sunken ship and, able to escape to an island, there he finds an abandoned hut, which had a famous owner, i.e. Robinson Crusoe. Although the city slickers seems to be completely unsuitable for survival on a deserted island, Roberto arranges over time with its location and even leads a relatively happy life. But then he discovers that he is not alone on the island: A young pretty woman natives, who he calls Friday, joins him, and things start to be more complicated.

== Cast ==

* Paolo Villaggio: Roberto Minghelli / Robinio / Robinson 
* Zeudi Araya: Venerdì 
* Anna Nogara: Magda
* Percy Hogan: Mandingo

==References==
 

==External links==
* 
  
 

 
 
 
 
 
 
 
 
 
 


 
 