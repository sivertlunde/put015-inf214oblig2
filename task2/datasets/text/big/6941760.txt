30 Days of Night (film)
{{Infobox film
| name           = 30 Days of Night
| image          = 30 Days of Night teaser poster.jpg
| caption        = Teaser poster
| director       = David Slade
| producer       = Sam Raimi Ted Adams Rob Tapert Brian Nelson
| based on       =  
| starring       = Josh Hartnett Melissa George Danny Huston Mark Boone Junior Ben Foster
| music          = Brian Reitzell
| cinematography = Jo Willems
| editing        = Art Jones
| studio         = Dark Horse Entertainment Ghost House Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 109 minutes
| country        = United States
| language       = English
| budget         = $30 million 
| gross          = $102,254,137  }} 
}}
 miniseries of the same name. The film is directed by David Slade and stars Josh Hartnett, Melissa George, and Danny Huston. The story focuses on an Alaskan town beset by vampires as it enters into a thirty-day long polar night.

30 Days of Night was originally pitched as a comic, then as a film, but was rejected. Years later  , was released on October 5, 2010 straight to home video. A prequel mini-series,  , was released on FEARnet|FEARnet.com and FEARnet On Demand in 2007.

==Plot== Eben Oleson, Stella Oleson, missed the last plane and must stay the 30 days. Although they try to avoid one another, when Eben confronts the stranger in the town diner, Stella helps to subdue him and take him to the station house.

At the station, the stranger taunts the townsfolk, telling them that death is coming. Just then, unknown creatures attack the local telecommunications center and power supply, rendering the town dark and cut off from the outside world. Eben goes to the telecommunications center and finds the operators head on a spike. Suspecting impending danger, Eben and Stella go through town, warning all the residents. A coven of feral vampires, led by Marlow, slaughters most of the townspeople, including Ebens grandmother, while the remaining survivors congregate in the diner. As Eben and Stella recover ammunition and supplies, vampires attack, but Beau Brower, the town snowplow driver, rescues them. Everyone takes shelter in a boarded-up house with a hidden attic. Marlow finds the stranger in the station and thanks him for doing what he asked and then snaps his neck.
 righthand man, Arvin, feasts on her. Eben attempts to help John. However, upon discovering that he is a vampire, Eben cuts off his head.
 whiteout to go to the general store. There, a child vampire attacks, wounding Ebens friend Carter Davies before the others overpower her and Jake cuts off her head. The whiteout ends during the struggle, preventing the survivors from going back to the attic. Eben decides that everyone should go to the police station and provides a diversion by running to his grandmothers house for an ultraviolet lighting system. He makes it to the house, jump-starts the generator, and turns the light on the vampires, wounding Marlows lover, Iris, so badly that Marlow is forced to kill her to put her out of her misery. Eben escapes the house, but the vampires follow him. Beau rescues him, killing many vampires with his tractor. He crashes into a hotel and ignites a box of flares, trying to kill himself before the vampires can. Unsuccessful, Beau survives the blast and his skull is crushed under Marlows feet. This allows Eben to reach the station. There, Carter reveals he has turned into a vampire. He tells the group of the recent deaths of his wife and children at the hands of a drunk driver. Horrified at the idea of living forever without his family, Carter asks Eben to kill him so he can be with them again. Eben agrees, and they go into his office where Eben cuts off Carters head.

Two weeks later, Stella and Eben see Deputy Billy Kitka signaling them with a flashlight and bring him back to the station. When the vampires first attacked, Billy had killed his wife and daughters so that they wouldnt die painfully like the rest. He then tried to shoot himself but his gun jammed.

Eben, Stella and Billy find that the others have made for the utilidor, a power and sewage treatment station that still has power. They begin to sneak towards it and spot a girl, Gail Robbins, being pursued by the vampire Zurial. Stella helps the girl while Eben and Billy distract Zurial. Stella and Gail manage to hide under an abandoned truck; Eben and Billy both make it to the utilidor, where they find the other survivors, but Arvin gets inside. Arvin attacks Billy, ripping into his neck, but as he turns to Eben, Billy knocks Arvin into the sewer systems heavy-duty shredder, shredding him, also accidentally shredding his own hand. Also due to the wounds the vampire inflicted, Billy begins to turn into a vampire himself, forcing Eben to cut off his head.

As the month comes to an end, with the sun due to rise, the vampires start to burn down the town to destroy evidence of their presence, and prevent any survivors from telling the world what happened. Realizing he cannot beat the vampires in his current state, Eben turns himself into a vampire via injection with Billys infected blood. He confronts Marlow who accepts Eben as the "pack leader" of the humans (referring to him as "the One who fights"). The two engage in a vicious battle for supremacy which ends with Eben punching a hole through Marlows skull. Leaderless and with the sun about to rise, the remaining vampires flee.

Eben and Stella watch the sunrise together. While Stella rests on Ebens shoulder, they share one last kiss. Stella holds Eben tightly in her arms as the sun brightens the sky, and Eben dies, his body burning to ashes, while Stella looks at the sky, coming to grips with what has happened.

==Cast==
 
* Josh Hartnett as Sheriff Eben Oleson
* Melissa George as Stella Oleson
* Danny Huston as Marlow
* Ben Foster as The Stranger
* Mark Rendall as Jake Oleson
* Manu Bennett as Deputy Billy Kitka
* Mark Boone Junior as Beau Brower
* Nathaniel Lees as Carter Davies Craig Hall as Wilson Bulosan
* Chic Littlewood as Isaac Bulosan
* Amber Sainsbury as Denise
* Joel Tobeck as Doug Hertz
* Elizabeth Hawthorne as Lucy Ikos
* Andrew Stehlin as Arvin
* Megan Franich as Iris
* Peter Feeney as John Riis John Rawls as Zurial
* Jared Turner as Aaron
* Kelson Henderson as Gabe
* Pua Magasiva as Malekai Hamm
* Abbey-May Wakefield as Little Girl Vampire
* Grant Tilly as Gus
* Rachel Maitland-Smith as Gail Kate Elliott as Dawn
* Jacob Tomuri as Seth
 

==Production==
30 Days of Night author Steve Niles conceived of the story in the form of a comic but after meeting a lack of interest in initial pitches tried to pitch it as a film. When this did not work out Niles shelved the idea until he showed it to IDW Publishing. IDW published the comic and Ben Templesmith provided the artwork.  When Niles and his agent, Jon Levin, shopped the comic around again as a potential film adaptation, Niles found that the idea "went shockingly well," with Sam Raimi and Senator International picking up the property rights based on the original concept and Templesmiths unique mood and concepts for the vampires. 
According to Raimi, the potential project was "unlike the horror films of recent years". 
 MGM and Senator International, bid in the $1 million range for rights to a potential vampire film based on the story. Director and producer Sam Raimi expressed interest in adapting the miniseries and was negotiating a production deal with his producing partner Robert Tapert to establish a label with Senator Entertainment, of which Senator International is the sales division.  In July 2002, Senator International acquired the rights for 30 Days of Night in a seven figure deal with Raimi and Tapert attached as producers.

By October 2002, Niles was working on adapting 30 Days of Night for the big screen, keeping the film true to the miniseries, though fleshing out the characters more significantly in the adaptation process.  In February 2003,  , was rewriting Niless 30 Days of Night draft for production.  Niles was pleased with Beatties faithfully rewritten script, which was submitted to the studio in October 2004. 
 Hard Candy, was writing a new draft of the 30 Days of Night script, replacing Beatties draft.     The director said that filming would begin in summer 2006 in Alaska and New Zealand. 

In June 2006, it was announced that Josh Hartnett was cast as the husband of the married couple that serves as the towns sheriff team.  Melissa George joined the 30 Days of Night cast as the wife of Hartnetts character.   Danny Huston joined the cast as the leader of the vampires.  Filming did not begin immediately, but in a September 2006 interview, executive producer Mike Richardson said that 30 Days of Night would be shot on 35 mm film, though there had been discussion to shoot the film on Genesis (Panavision)|Genesis.   In an interview prior to filming, Slade explained that the illustrations of the graphic novels illustrator, Ben Templesmith, would be reflected in production design. Slade also considered Nelsons draft to be the most faithful to the graphic novel. He also stated his intention to make a "scary vampire film", of which he didnt think there were many. "The rest of them, they fall into all kinds of traps. Were going to try to do our best... and one of the ways we have to do it is be more naturalistic than the graphic novel, because its very over-the-top," said Slade.   There was also concern expressed that while the vampires needed to communicate, talking might lessen the effect.  To counter this, a fictional vampire language, with click consonants, was constructed with the help of a professor of linguistics and the nearby University of Auckland.  Slade explained "we designed this really simple language that didnt sound like any particular accent that you would be aware of, that was based around really simple actions, eating, hunting, yes, no, really basic, because thats what vampires do." 

By February 2007, the production phase was completed, and a rough cut of the film was prepared.   In April, composer Brian Reitzell was hired to score the film. 

==Release and reception==
30 Days of Night was released in 2,855 cinemas in the United States and Canada on October 19, 2007. In its opening weekend, the film grossed $15,951,902,    placing first in the box office.  The film grossed $39,568,996 in the United States and Canada and $35,735,361 overseas. It has grossed $75,304,357 worldwide.  On the review aggregation site Rotten Tomatoes, 51% of the 157 film critics gave the film positive reviews.  On Metacritic, the film received a score of 53 out of 100 from 29 reviews, considered to be mixed or average reviews. 

To coincide with the films release, a novelization by Tim Lebbon was published by Pocket Star on September 25.  It is one of six novels based on the franchise.

===Home video===
30 Days of Night was released February 26, 2008 on DVD, Blu-ray and UMD for PlayStation Portable in the United States. DVD sales brought in $26,949,780 in revenue, from 1,429,600 sold DVD units. This does not include Blu-ray sales.  The DVD is a single disc and includes eight featurettes, one of which is a full episode of the anime Blood+. The UK Region 2 release is a two disc special edition, released in April 2008. Despite being exactly the same as the theatrical release, the BBFC re-rated the film from a 15 to an 18. Even though it still only has the eight featurettes on the second disc, it includes a bonus 30 Days of Night graphic novel.

==Sequel==
A   was released on 5 October 2010.  The script for the sequel was written by Steve Niles and Ben Ketai with Ketai also positioned as director. When filming began on October 20, 2009, Rhys Coiro and Mia Kirshner were named as leads, with Kirshner playing the lead vampire villain Lilith (30 Days of Night)|Lilith.   Other cast named included Harold Perrineau, Kiele Sanchez, Diora Baird, Rhys Coiro, and Monique Ganderton.   Three days after filming began, Niles revealed that Kiele Sanchez replaced Melissa George in the role of Stella Oleson.  The sequel was produced on a lower budget, but being straight-to-video allowed the writers to more closely follow the comic book. {{Cite news
| last = Nicholson| first = Max| title = Dark Days Interview: Steve Niles| work = IGN| accessdate = 2010-08-18| date = 2010-07-23
| url = http://dvd.ign.com/articles/110/1107554p1.html}} 

== Soundtrack == Official Soundtrack, with an artwork by Marc Bessant in summer 2015 on Vinyl. {{Cite news
| last = Barkan| first = Jonathan| title = Invada Announces ’30 Days Of Night’ Vinyl For Record Store Day 2015| work = BD| accessdate = 2015-01-29| date = 2010-01-29
| url = http://bloody-disgusting.com/news/3330058/invada-announces-30-days-night-vinyl-record-store-day-2015/}} 

==Novel==
A novelization was released in 2007:

*  

==See also==
*  
*  
* Frostbite (2006 film)|Frostbite, a Swedish 2006 vampire film with similar theme.
* Vampire film

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   at Ghost House Pictures
*   for 30 Days of Night at  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 