Shake, Rattle & Roll 12
{{Infobox film
| name           = Shake, Rattle and Roll 12
| image          = 
| alt            =  
| caption        = 
| director       = Zoren Legaspi (Mamanyika) Topel Lee (Isla) Jerrold Tarog (Punerarya)
| producer       = Roselle Monteverde-Teo Lily Monteverde
| writer         = 
| screenplay     = 
| story          = 

| starring       = Shaina Magdayao Ricky Davao Andi Eigenmann Rayver Cruz John Lapus Regine Angeles Kristel Moreno Carla Abellana Sid Lucero Mart Escudero Nash Aguas
| music          = 
| cinematography = 
| editing        = 
| studio         =  Regal Entertainment, Inc. Regal Multimedia, Inc.
| released       =  
| runtime        = 124 minutes
| country        = Philippines
| language       = Tagalog English
| budget         = 
| gross          = P50,000,000
}}
 horror Suspense suspense film Regal Entertainment, Inc. and Regal Multimedia, Inc. It was an official entry in the 2010 Metro Manila Film Festival.

==Synopsis==

===Mamanyiika===

====Overview====
SRR 12s first episode "Mamanyiika" plays on the words "mama" and "manyika" which literally means "mother doll". The story of a possessed doll is being starred by Shaina Magdayao, child star Elijah Alejo and Ricky Davao. Directed by Zoren Legaspi, it also features Jackie Lou Blanco, Malou Crisologo, Rita Avila, Jed Madela, and Mahal as the voice of the doll. Carmina Villarroel has a cameo role on the episode.

====Plot====
Some years ago, Abel (Ricky Davao) was incapacitated by a passing car while his wife Gail (Jackie Lou Blanco) was killed in a hit-and-run accident, after a serious fight, leaving Abel widowed with his two children, Ara (Shaina Magdayao) and Abigail (Elijah Alejo).

After visiting her mothers grave, Abigail picked up a seemingly-disposed-of doll in a nearby barrow. Ara, being suspicious of the doll, demanded her sister throw it away. This soon culminated in a sisterly fight, on which one instance, the doll sprang to life and comforted Abigail. It was revealed that Gails spirit possessed the doll.

Traumatized after being assaulted by the doll, Ara destroyed her little sisters toy, despite of Abigail watching the scene horribly. Abel, deciding this was the final straw for Ara, ordered the doll to be disposed of in the creek. Soon, the doll assassinated two garbage men and returned to the Federicos house, seeking vengeance against Abigail, who was given a new set of dolls by her father.

A subplot shows that the doll is formerly Dorothy Cruzs, an infertile woman who succumbed to insanity after learning her medical condition. Her husband Manuel (Jed Madela) refuses to give any comments to the doll. It was not Gails spirit who was in the doll, but Dorothys after all. 

One night, the mother doll launched its final execution against the family, resulting with Ara gaining the upper hand. Taunted by the doll imitating her mothers voice, she   shot the doll squarely after it attempted to murder Abigail. The doll sank under the pool, but recovered and was adopted by a street child.

Thus, a new chapter for the dolls terror began.

===Isla (Engkanto)===

====Overview====
Sometimes referred to as "Isla", Topel Lee takes the helm of SRR XIIs second episode, which is the tale of three girls, one of them the apple of the eye of a terrible engkanto, who then captures the girl. This stars Andi Eigenmann for her SRR debut role. Rayver Cruz, John Lapus, Kristel Moreno, Solo Kiggins, and Regine Angeles accompany Andi on the episode with Niña Jose having a special participation.

====Plot====
A lost girl (Nina Jose) was separated from her friends while on a vacation. Lost in the woods, she was incapacitated by an unknown entity, which transforms her into an unimaginable creature.

In the present, Andrea (Andi Eigenmann) was accompanied by her friends Ces (Kristel Moreno) and Belle (Regine Angeles) on a vacation in a seemingly-remote island. Upon setting foot on the island, rumors circulated the trio that a man named Ray (Rayver Cruz) had lost his girlfriend during a holiday in the very same place. Dismissing the hearsay, the trio spent the night with a bonfire. Ces, who has a knowledge of the occult, performs a ritual to help the brokenhearted Andrea find the man for her.

Tensions rose as disappearances of Andrea startled the group, with their friend reappearing in the morning with muddy feet. Scenes showed that Andrea was led by fairies into a cave and was offered delicious food by an elemental or "engkanto" as well as asking her to stay and offering her to give her everything she wants if she decides to do so. Counseled by a "witch doctor", Malay (John Lapus), Andrea was the next target of the "engkanto" of the island, whom Malay said was the king of the isle. Afterwards, they met Ray himself. The girls seemed to be cold with the guy, as he was cold to them either.

No sooner than the disappearances worsen, a group of fairies dubbed as the "Lambana" attacked the groups cottage, assassinating Belle and Ces in the havoc. The engkanto, having a chance to permanently abduct Andrea, held the girl hostage, with Ray and Malay in hot pursuit. In the end, Malay was killed, but Ray managed to overpower the engkanto, saving Andreas life.

In a post-plot scene, Andrea woke aboard a boat with Ray; however, she notices that the boat was not moving away from the island. It was revealed that it was just an illusion and that Andrea was still in the engkantos cave and that the engkanto was disguised as Ray.

===Punerarya===

====Overview====
Punerarya episode, commandeered by independent film director Jerrold Tarog, concludes the latest installment of SRR, with the story of a tutor teaching the children of the ghoul entrepreneurs of a funeral parlor. Carla Abellana and Sid Lucero helm the cast, with Mart Escudero, child stars Anna Vicente and Nash Aguas and champion racer Gaby dela Merced as supporting characters.

====Plot====
Dianne (Carla Abellana) works as a part-time tutor for the Gonzales, the owners of a local funeral parlor, which is said to be the lair of rumored werewolves.

On her first day, she met the patriarch Carlo (Sid Lucero), the children - Ryan (Nash Aguas) and Sarah (Anna Vicente) -, the familys butlers Aludia (Odette Khan) and Simeon (Jess Evardone), and the secretary Anna (Gaby dela Merced). After a series of successful tutorial sessions, it gradually became clear to Dianne the nature of the family: being photophobia|photophobic, having a desire for human entrails, and, worst of all, the unusual activity in and out of the parlor.

Dennis (Mart Escudero), Diannes brother, dismissed his sisters stories; meanwhile, Ryan was accused of divulging the truth to Dianne, creating a tension between Carlo and the tutor. However, Dianne could not brought herself to quit, not only because of being friends with Ryan, but also this was her only work for a living.

As a plot, the family lured Dennis, who was waiting for her sister outside. Unknown to him, he was used as a bait to force Diane to continue her work, but at the same time, sacrificing her life as prey to the human-dog creatures. Carlo then revealed that they were once part of an ancient tribe, whose homes were ravaged by fire. Desperate, he Carlo, being widowed, and his family set up the funeral parlor to cover up their true nature and to earn for a living. This explained their photophobic features and the fact of dismissing Dianne every evening before sundown.

Dianne confronted the group, demanding they surrender Dennis. Throwing formaldehyde and setting the parlor aflame as a diversion, Dianne and her brother narrowly escaped Sarahs and Carlos assaults, while Ryan, the only aswang member of the family that refuses to harm anyone, begged to go with her. Left with no choice, Dianne brought Ryan home, as the funeral parlor burned to the ground.

As Dianne finds the first-aid kit to treat Dennis, she overhears his screams. She rushes to the den and finds Ryan eating Dennis entrails. Ryan reveals that his family only eats living humans on h their birthdays and just eats entrails from corpses to survive everyday. However, Ryan wanted more and prefers to eat living humans everyday. Dianne realizes that she was used by Ryan so that he can get rid of his family and their restrictions as he growls and pounces on her.

== Cast ==

 

== Mamanyika ==
*Shaina Magdayao as Ara
*Ricky Davao as Abel
*Malou Crisologo as Yaya Miley
*Rita Avila as Dorothy Cruz
*Jackie Lou Blanco as Gail
*Elijah Alejo as Abi
*John Feir as Fermin
*Carmina Villaroel as Head Nurse
*Jed Madela as Garbage Man
*John Apacible as Garbage Man

== Isla ==
*Andi Eigenmann as Andrea
*Rayver Cruz as Ray
*John Lapus as Malay
*Niña Jose as Aira 
*Regine Angeles as Belle
*Kristel Moreno as Ces
*Solo Kiggins as Jok
*Richard Quan as Domeng

== Punerarya ==
*Carla Abellana as Dianne
*Sid Lucero as Carlo
*Nash Aguas as Ryan
*Mart Escudero as Dennis
*Gaby Dela Merced as Anna
*Odette Khan as Aludia
*Jess Evardone as Simeon
*Anna Vicente as Sarah
*Dido dela Paz as Rolly
*Mercedes Cabral as Pregnant Woman

== Deaths ==

Everyone in this scene was killed or possessed

==Mamanyika==
Gail (Died in Hit and Run Accident)

Manuel/Garbage Man 1 (Killed by the Doll)

Garbage Man 2 (Killed by the Doll)

Yaya Miley (Killed by the Doll)

Fermin (Killed Via Electrocuted by the Doll)

Abel Fedrico (Killed by the Doll)

Doll (Killed by Ara But Later Revive)

==Isla==
Aira (Killed by the engkanto,transformed into engkanto)

Malay (Killed by the engkanto)

Belle (Slammed to bed with a mosquito killer on face)

Ces (Radio fall on head)

Ray (Possessed by the engkanto)

Andrea (Killed By Possessed Ray)

==Punerarya==

Pregnant Woman (Killed by the Shape-Shifting Wolves)

Rolly (Killed By Shape-Shifting Wolves)

Owners Of The "Punerarya" (Killed By Dianne)

Sarah (Killed By Dianne)

Carlo (Killed By Dianne)

Dennis (Killed By Ryan)

Dianne (Killed By Ryan)

==Accolades==
|-
| 2010 || Carla Abellana was   in Best Festival Actress || 36th Metro Manila Film Festival

==Trivia==
Carla Abellanas Father, Rey PJ Abellana also starred in the very first film series of Shake Rattle and Roll segment "Baso".

==Reaction==

===Commercial===
By the end of the festival, the movie grossed P50 million. 

===Critical===
Althea Lauren Ricardo of The Freeman gave the film a rating of 7 out of 10 (70%) stating that while it continued the traditional 3-in-1 format, it was "infused with a dash of indie spirit". The second episode - Isla - was described as the "weirdest segment in the trio" due to how its scenes were put together. 

==References==
 

 
 

 
 
 
 
 
 
 
 
 