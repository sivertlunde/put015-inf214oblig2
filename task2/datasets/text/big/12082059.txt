Star Spangled Girl
{{Infobox film
| name           = Star Spangled Girl
| image          = DVD cover of the movie Star Spangled Girl.jpg
| image_size     = 
| caption        = DVD cover
| director       = Jerry Paris
| producer       = 
| writer         = Arnold Margolin Jim Parker Neil Simon (play)
| narrator       =  Tony Roberts Todd Susman Sandy Duncan
| music          = 
| cinematography = 
| editing        = 
| distributor    = Paramount Pictures
| released       = 22 December 1971
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Tony Roberts, Todd Susman and Sandy Duncan.

==Plot== Tony Roberts) Elizabeth Allen), and his printer, Mr. Karlson (Artie Lewis). To appease Mrs. MacKaninee, he accompanies her motorcycling, waterskiing, and surfing. He lives with talented, but nerdy Norman Cornell (Todd Susman), who writes the entire newspaper.
 Olympics with the best swimming coach in the country. Norman falls instantly in love with her (or rather with the way she smells) and neglects his writing, causing Andy untold headaches.

Norman drives Amy to distraction with his misguided, over-the-top attempts to win her affection. When he unintentionally gets her fired from her day job, Andy hires her as a secretary, just to keep Norman happy (and writing). Then something unexpected happens. Despite despising everything Andy stands for, the conservative Amy discovers (to her great disgust) that she is physically attracted to him and likes the way he smells, which is rather awkward, since she is scheduled to marry another swimmer in a few weeks. When she informs Andy, he is uncertain how to react. She gets him to kiss her to see how it feels, at which point Norman walks in.

Amy decides to go back to Florida because Andy isnt interested in her. Norman quits over what he considers a betrayal, but quickly changes his mind and goes back to work, cured of Amy. Meanwhile, Andy discovers to his horror that the smell of Amy permeates the room even after shes gone. He chases after her and gets her to come back.

==Casting==
Britt Ekland, Ali MacGraw, Cybill Shepherd and Goldie Hawn were offered the lead role, but turned it down.

Sandy Duncan hired a professional trainer to help improve her swimming while auditioning for the part of Amy.

==Production==
Elizabeth Allen shot her scenes in 5 days.

The theme song of the film is "Girl", sung by  " version of the song during a cameo appearance in The Brady Bunch Movie.

Star Spangled Girl was a box-office flop. 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 