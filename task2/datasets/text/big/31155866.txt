Kallichellamma
{{Infobox film 
| name           = Kallichellamma
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = Sobhana Parameswaran Nair
| writer         = G Vivekanandan
| screenplay     = G Vivekanandan Madhu Sheela Adoor Bhasi
| music         = K. Raghavan
| cinematography = U Rajagopal
| editing        = K Narayanan K Sankunni
| studio         = Roopavani Films
| distributor    = Roopavani Films
| released       =  
| country        = India Malayalam
}}
 1969 Cinema Indian Malayalam Malayalam film, Kerala state film award for best actress for the year 1969 for her role in this movie.  The movie was produced by renowned film producer Sobhana Parameswaran Nair. 

==Plot==

Chellamma (Sheela) is an orphan living in a small house situated in the compound of a landlord in the village. She was an illegitimate child of her late mother. Her mother in her death bed requested her friend Valli Akka (Adoor Bhavani) to take care of Chellamma  and advised Chellamma never loose her chastity like her mother. As a grown up Chellamma is a bold and independent women who works hard for a living. Many men in the village tries in vein to make some sexual advances towards her. Men who covet chellamma includes the landlord and Asarakannu Muthalali(Madhu), a rowdy.

On a rainy day a dam which separates the paddy fields and the lake breaks and paddy field gets flooded with water. To pump the water out of the fields villagers bring a pumping machine from the town. A christian man(Prem Nazir) arrives the village as the pump operator. One day the landlord tries to evict Chellamma from her house, she resists and the pump operator helps her to protect her hut from the landlord. Hereafter Chellamma feels a positive affection towards the pump operator. One night the pump operator comes to chellammas house and force her to have sex with him. Chellamma initially resists but eventually lets him satisfy his libido on her. Next morning he leaves the village after promising Chellamma that he would come back soon to marry her.

Chellamma becomes pregnant. Valli Akka advises her to abort the pregnancy but she refuses. All people in the village starts hating Chellamma because of her illegitimate pregnancy. Vally Akka and one of her friend help Chellamma during these tough days of her life. Chellamma faces some medical complexities during parturition. Some good hearted people in the village brings her to the hospital. Chellamma survives but her infant dies. Asarakannu Muthalali visits Chellamma in the hospital and helps her financially and shows some sympathy towards her.

Chellamma comes back home after recovering her health. One day Asarakannu Muthalali visits Chellamma in her home and tries to rape her. While resisting him she tells him that she is considering him as her brother, on hearing this Asarakannu feels bad about himself. When he comes out of Chellammas house the pump operator arrives there in a drunken state. On seeing Chellamma with Asarakannu he loses his mind. He and Asarakannu fights and he stabs Asarakannu. On hearing Chellammas cries people gather around her house. Villagers catches the pump operator and ties him in a coconut tree in Chellammas yard. They bring Asarakannu to the hospital. On seeing the police coming to fetch the pump operator he seeks the help of Chellamma to get released from the knot. Chellamma cuts the rope and releases him. He escapes, on seeing this people become more furious towards her.

Asarakannu Muthalali survives the injury. He comes back to the village in search of the pump operator to take his revenge. He enquirers Chellamma about the pump operator and threatens her that he will kill the pump operator one day. One day the pump operator now working as a quarry worker gets fatally injured in an accident. Chellamma comes to know this and visits him in the hospital. One of his arm is broken in the accident. Chellamma goes with him leaving behind her house. She works and earns money to satisfy his belly. Some days he gets drunk and accuses Chellamma an illegitimate relationship with Asarakannu. Chellamma painfully endures his bad words.

One day Asarakannu sees Chellamma selling fruits. On seeing him she flees, he follows her and reaches her house. Asarakannu starts physically fighting with the pump operator. Asarakannu wins the fight and tries to kill the pump operator, Chellamma hold his legs and begs to spare her husbands life. On seeing her tears Asarakannu leaves him unharmed.

One day a weak lady and her two children come to Chellammas house. Chellamma realizes that they are the wife and children of the pump operator. On seeing them the pump operator becomes furious and tries to evict them from the house. Chellamma interferes and saves the poor woman and her children, she brings them inside. The woman says all that happened was her mistake. Chellamma consoles her and says "our mistake is nothing but being women in this world". The pump operator arrives the house in a drunken state, he tries to kill his first wife but on seeing his daughter his mind changes. He hugs the child and starts crying in remorse. On seeing the happy reunification of the family Chellamma feels both joy and pain at the same time. She leaves the house in the midnight.

She arrives her old home. She fetches a poisonous fruit and eat it deliberately. In her last moment she feels like her mother calling her. She tells her mother that she is thirsty. Her mothers sound tells her to fetch a coconut from her Chudalathengu(a coconut tree planted on her mothers grave immediately after the funeral) and drink the coconut water. She fetches a coconut from the tree using a long stick. While trying to break the coconut the landlord and his people arrives the scene and accuses her theft of coconuts from their property. They starts parading her to the police station with the fallen coconut bunch on her head. She collapses and dies thirsty with the coconuts from the chudalathengu scattered around her.


==Cast==
 
*Prem Nazir Madhu
*Sheela as Chellamma
*Adoor Bhasi
*Kottayam Santha
*Sankaradi
*Jesey
*KP Pillai
*Latheef
*Abbas
*Adoor Bhavani
*K. P. Ummer Khadeeja
*Kuttan Pillai
*ML Saraswathi
*Master Pramod Meena
*Nabeesa
*Pala Thankam
*Paravoor Bharathan
*Sankar Menon
*Thodupuzha Radhakrishnan
*Thomas Mani
*Veeran
 

==Soundtrack==
The music was composed by K. Raghavan and lyrics was written by P. Bhaskaran and Traditional. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ashokavanathile || Kamukara, B Vasantha || P. Bhaskaran || 
|-
| 2 || Kaalamenna Kaaranavarkku || P. Leela, CO Anto, Kottayam Santha, Sreelatha Namboothiri || P. Bhaskaran || 
|-
| 3 || Karimukilkkaattile || P Jayachandran || P. Bhaskaran || 
|-
| 4 || Kunnamkulangare || Adoor Bhavani || Traditional || 
|-
| 5 || Maanathekkaayalin || KP Brahmanandan || P. Bhaskaran || 
|-
| 6 || Unniganapathiye || MG Radhakrishnan, Chorus, CO Anto || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 

 