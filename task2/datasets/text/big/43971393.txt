Dragonheart 3: The Sorcerer's Curse
 
{{Infobox film
| name           = Dragonheart 3: The Sorcerers Curse
| image          = File:Dragonheart_3_Poster.jpg
| caption        = 
| director       = Colin Teague
| producer       = Raffaella De Laurentiis
| writer         = Matthew Feitshans
| screenplay     = Matthew Feitshans
| story          =
| starring       = Julian Morris Jassa Ahluwalia Jonjo ONeill Jake Curran Tamzin Merchant Dominic Mafham Ben Kingsley
| music          = Mark McKenzie
| cinematography = David Luther
| editor         = Fiona Colbeck  Charlene Short   Eric Strand  Universal 1440 Entertainment
| distributor    = Universal Studios Home Entertainment
| released       =  
| runtime        = 97 minutes
| country        = United States United Kingdom Romania
| language       = English
| budget         = 
| gross          = 
}}

Dragonheart 3: The Sorcerers Curse is a 2015  .

==Synopsis==
When young squire Gareth goes in search of a fallen comet, he believes it holds gold that he can use to pay for his knighthood. Instead of finding a comet, Gareth finds the dragon Drago (voice of Ben Kingsley) who has fallen to earth and is being hunted by an evil sorcerer. After Drago saves Gareths life, the two become intricately bonded and they must work together to defeat the sorcerer and stop his reign of terror. Along the way, Gareth learns the true meaning of being a knight. 

==Cast==
*Julian Morris as Gareth 
*Ben Kingsley as the voice of Drago the Dragon  
*Jassa Ahluwalia as Lorne
*Jonjo ONeill as Brude 
*Jake Curran as  Traevor 
*Tamzin Merchant as Rhonu
*Dominic Mafham as Sir Horsa
*Christopher Fairbank as Potter
*Ozama Oancea as Begilda
*Harry Lister Smith as Kalin
*Daniel Everitt-Lock as Cuthbert
*Serban Celea as Sir Wulfric
*Duncan Preston as Elisedd
*Ioan Coman as Vendor
*Edouard Philipponat as Squire Edouard
*Pavel Ulici as Toothless Pict
*Vlad Radescu as Villager
*Matthew Feitshans as Matthaeus
*Denis Stefan as Lead Pict

==Development==
Although for years, a third installment has been long rumored and speculated by fans of the previous two films and many others , it was officially announced by Universal Studios Home Entertainment during the summer of 2014   that a third film was in the works. The budget for the CGI effects work for Drago alone was done on a budget of U.S. Dollar|$7,000,000.

Raffaella De Laurentiis who has previously produced the first two films has reprised her role in development as a producer for the third installment of the trilogy. Her stepson, Matthew Feitshans is a screenwriter and development executive regarding storytelling and he has been chosen in position as the writer for the film. In addition, this is actually his first film that hes writing for.

On August 28, 2014, it was announced that Julian Morris has been chosen for the main lead character Gareth.

===Music and sound===
 
The score was composed by Mark McKenzie, who previously composed the score for Dragonheart: A New Beginning.

==Filming==
The filming began around 2013 with filming locations taking place in Romania.

==Release== Hindi Dubbing audio dub Latin Spanish European French Japanese DVD and Blu-Ray release is slated for June 3, 2015.

==Reception==
 
After its digital release, the film received mixed but generally positive reception, with reviews noting impressive CGI for a direct-to-video release, the film being a vast improvement over Dragonheart 2, and being on par with the original film.  

==Sequel==
According to Matthew Feitshans, a Dragonheart 4 is currently in the works. 

==References==
 

==External Links==
* 
* 
*http://dh-the-sorerers-curse.tumblr.com/

 
 
 
 
 
 
 
 
 
 