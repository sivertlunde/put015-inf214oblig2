The Vikings (1958 film)
{{Infobox film
| name           = The Vikings
| image          = Vikings_moviep.jpg
| caption        = Film poster by Reynold Brown
| director       = Richard Fleischer Jerry Bresler
| writer         = Calder Willingham (screenplay) Dale Wasserman (adaptation)
| based on       =  
| narrator       = Orson Welles
| starring       = Kirk Douglas Tony Curtis Janet Leigh Ernest Borgnine James Donald Alexander Knox Frank Thring
| music          = Mario Nascimbene
| cinematography = Jack Cardiff
| editing        = Elmo Williams
| Studio         =Bavaria Film Brynaprod S.A. Curtleigh Productions
| distributor    = United Artists 
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $3.5 million Tino Balio, United Artists: The Company The Changed the Film Industry, Uni of Wisconsin Press, 1987 p 132 
| gross = $6 million (est. US/ Canada rentals)  
}}
 sagas of Ragnar Lodbrok his sons. Maurangerfjorden and Fort de la Latte in north-east Brittany and also on the location of the Lim Bay (Fiord) in Croatia.

Despite being derisively called a "Norse Opera" by New York Times critic Bosley Crowther, the film proved a major box office success and spawned the television series Tales of the Vikings, directed by the films editor, Elmo Williams, which included none of the original cast or characters.

== Plot == Ragnar (Ernest Aella (Frank enslave him.
 Erik (Tony pommel stone on an amulet around Eriks neck, placed there by Eriks mother when he was a child, but tells no one.

Erik incurs the wrath of his half-brother Einar (Kirk Douglas), Ragnars legitimate son and heir, after the former orders his falcon to attack Einar, taking out one of his eyes. Erik is saved from immediate execution when the royal Court Völva Kitala (who loves Erik as a son) warns that Odin will curse whoever kills him. He is left in a tidal pool to drown with the rising tide by Ragnars decree to avoid the curse, but after Kitala  calls out to Odin making Erik himself to invoke his mercy, a strong wind shifts and forces the water away, saving him. Lord Egbert then claims him as his slave property to protect his rights, before Einar keenly aware of the weather shift can return and finish him. Egbert hopes to find an opportunity to take advantage of Eriks unknown claim to the Northumbrian kingdom. 
 s on Kornati]]
 s at Fort-la-Latte]]
The enmity between Erik and Einar is exacerbated when they both fall in love with the Christian Princess Morgana (Janet Leigh), who was to marry King Aella but is captured in a raid suggested by Egbert, to demand ransom and bring shame and political unpopularity pressure upon the Northumbrian monarch. During a drunken feast in the "great hall", Einar confesses his feelings to Ragnar, who tells Einar that women often need to be taken by force and grants his son to take the prisoner as his. Einar throws the guards off the ship Morgana is being held on, and begins to rape her — defying his expectations and hope for resistance, she offers none, denying him his wish to take her by aggressive force. Before things can go any further, Erik grabs Einar from behind and knocks him out easily as he was very drunk, then takes Morgana away on a small ship he had constructed for Egbert.

Erik and Morgana flee to England, along with Sandpiper (Eriks friend and fellow slave), Kitala and Morganas maid Bridget (Dandy Nichols). Einar regains consciousness and gives the alarm, and several pursuing longships quickly gain on the fugitives. In thick fog, Ragnars longship hits a rock and sinks, while Eriks boat is guided safely by a primitive compass, a piece of magnetite in the shape of a fish that Sandpiper obtained in a distant land. Einar, in another longship, believes Ragnar to be dead and grudgingly abandons the chase. Ragnar, however, is rescued by Erik and taken prisoner to Aella. Erik and Morgana become lovers during the trip, and she agrees to seek release from her pledge to marry Aella.

Aella orders the Viking leader bound and thrown into a pit filled with starved wolves. To give Ragnar a Vikings death (so that he can enter Valhalla), Erik, who is granted the honour of forcing him into the pit, cuts the prisoners bonds and gives him his sword. Laughing, Ragnar jumps to his death. In response to Eriks "treason", Aella cuts off his left hand, puts him back on his ship and casts him adrift.

Erik returns to Einars settlement, and tells his half-brother how his father died, and what had been Aellas reward for allowing Ragnar to die a Vikings death. With this revelation, and the promise that Erik will guide their ships through the fog (thus making a surprise attack possible), Einar is finally able to persuade the other Vikings to mount an invasion of Northumbria. Putting their mutual hatred aside for the moment, Einar and Erik sail for England.

The dragon longships land and the Vikings begin to move inland in force. The alarm is sounded and the terrified peasants abandon their fields and flocks and flee to take refuge within the castle. Soon the Vikings are arrayed in front of the fortress in full battle armour.

Shouting the name of "Odin!", the Vikings storm Aellas castle. In a bold move, Einar has several Vikings throw axes at the closed drawbridge that bars entrance to the castles keep. Several of the axe-throwers are killed, but enough survive to throw their axes that a "ladder" is created for Einar to climb after he leaps across the moat to the drawbridge. He gains entry to the keep and lowers the drawbridge so that the other Vikings can overwhelm the outnumbered English. Erik and Einar both set off in search of Morgana. Erik encounters Aella instead and shoves him into the wolf pit.

Einar finds Morgana in the highest tower of the keep, he grabs and accosts her, telling her she will be his Queen. Morgana tells Einar she hates him, and loves Erik. Enraged, Einar drags her outside and calls Erik to their long-delayed battle. The two bitter rivals engage in a swordfight on top of the tower. Erik is defeated, his sword broken, but as Einar prepares to deliver the killing blow, he hesitates, having learned the truth from Morgana, and suddenly seeing Ragnar in Eriks defiant face. This gives Erik (who does not yet know they are brothers) the opportunity to fatally stab Einar with his swords broken blade. Echoing the scene with Ragnar, Erik gives Einar a sword, so that he too can enter Valhalla. In the final scene, Einar is given a Viking funeral: his body is placed on a longship, which is set on fire by flaming arrows.

== Cast ==
* Kirk Douglas as Einar  
* Tony Curtis as Erik  
* Ernest Borgnine as  Ragnar Lodbrok  
* Janet Leigh as Morgana   Egbert 
* Alexander Knox as Father Godwin  
* Maxine Audley as Enid  
* Frank Thring as Aella of Northumbria 
* Eileen Way as Kitala  
* Edric Connor as Sandpiper  
* Dandy Nichols as Bridget  
* Per Buckhøj as Björn Ironside
* Orson Welles as The Narrator

=== Casting ===
Ernest Borgnine plays Ragnar, the father of Einar, played by Kirk Douglas. Borgnine was born almost two months after Douglas.

The Vikings was the third of five films in which husband-and-wife Tony Curtis and Janet Leigh starred together during their 11 year marriage from 1951 to 1962.

==Reception==
The film was the 3rd most popular film at the British box office in 1958. 

==Awards==
*Nominee Best Director - Directors Guild of America (Richard Fleischer)
*Winner Best Actor - San Sebastian International Film Festival (Kirk Douglas)

==Legacy== Erik the The Long Ships. 

==See also==
*List of historical drama films
* For an analysis of the films use of the Bayeux Tapestry, see Richard Burt, Medieval and Early Modern Film and Media (Palgrave, 2010) http://us.macmillan.com/medievalandearlymodernfilmandmedia/RichardBurt 
<!--
== References ==
 	
-->
==Biography==
* 

== References ==
 

== External links ==
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 