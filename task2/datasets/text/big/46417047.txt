The Endless Road
 
{{Infobox film
| name = The Endless Road
| image =
| image_size =
| caption =
| director = Hans Schweikart
| producer =  Gerhard Staab
| writer =  Walter von Molo (novel and screenplay)   Ernst von Salomon
| narrator =
| starring = Eugen Klöpfer   Eva Immermann  Hedwig Wangel
| music = Oskar Wagner    Franz Koch  
| editing =     Ludolf Grisebach 
| studio = Bavaria Film
| distributor = Deutsche Filmvertriebs 
| released =24 August 1943 
| runtime = 96 minutes
| country = Germany  German 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}}
The Endless Road (German:Der Unendliche Weg) is a 1943 German biographical film directed by Hans Schweikart and starring  Eugen Klöpfer, Eva Immermann and Hedwig Wangel. It portrays the life of Friedrich List, a German who emigrated to the United States in the nineteenth century. Unusually the film was overtly pro-American at a time when the two countries were at war. This was possibly because the Nazi leadership hoped to shortly join the Americans in an anti-Soviet alliance and wanted to encourage warmer feelings between the two nations. Another pro-American (and anti-British) film about Thomas Paine was planned, but never made. 

It was made by Bavaria Film, one of the four major German film companies of the era. The films sets were designed by the art director Hans Sohnle.

==Cast==
*  Eugen Klöpfer as Friedrich List 
* Eva Immermann as Mila List, seine Tochter 
* Hedwig Wangel as Susan Harper 
* Alice Treff as Helen Harper 
* Kurt Müller-Graf as von Runge 
* Friedrich Domin as Fürst Metternich  
* Ernst Fritz Fürbringer as General Jackson  
* Lisa Helwig as Karoline List  
* Viktor Afritsch as Hofrat Gentz  
* Joseph Offenbach as Seybold  
* Adolf Gondrell as Ritter  
* Günther Hadank as König Friedrich Wilhelm IV. von Preussen  
* Gustav Waldau as König Wilhelm I. von Würtemberg  
* Walter Holten as Kommandant von Hohenasperg  
* Herbert Hübner as Handelsgerichtsbeisitzender   Oskar Höcker as Würtembergischer Kammerpräsident  
* Fritz Reiff as Preussischer Minister 
* Walter Buhse 
* Heinz Burkart 
* Wolfgang Dohnberg 
* Peter Doming 
* Karl Graumann 
* Karl Hanft 
* Hannes Keppler
* Herbert Kroll 
* Else Kündinger 
* Walter Lantzsch 
* Eduard Loibner 
* Philipp Manning
* Emil Matousek
* Sepp Nigg   
* Julius Riedmueller  
* Sonja Gerda Scholz   
* F.W. Schröder-Schrom
* Otz Tollen   
* Anni Trautner    
* Adalbert von Cortens 

== References ==
 

== Bibliography ==
* Hull, David Stewart. Film in the Third Reich: A Study of the German Cinema, 1933-1945. University of California Press, 1969.

== External links ==
*  

 
 
 
 
 
 
 
 
 
 

 