Unfaithfully Yours (1984 film)
 
{{Infobox film
| name           = Unfaithfully Yours
| image          = Unfaithfully Yours 1984 poster.jpg
| image_size     =
| caption        = theatrical poster
| director       = Howard Zieff
| producer       = Daniel Melnick (exec) Joe Wizan Marvin Worth
| writer         = Preston Sturges (previous screenplay) Valerie Curtin Barry Levinson Robert Klane
| starring = {{Plainlist|
* Dudley Moore
* Nastassja Kinski
* Armand Assante
* Cassie Yates
* Richard Libertini
* Albert Brooks
}} Steven Bishop (song)
| cinematography = David M. Walsh
| editing        = Sheldon Kahn
| studio         = 20th Century Fox
| distributor    = 20th Century Fox
| released       = February 10, 1984
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $19,928,200
}}
 1984 romantic 1948 film Stephen Bishop.

==Plot==
Claude Eastman (Dudley Moore) is a composer and the conductor of a prestigious symphony who has recently married beautiful Daniella (Nastassja Kinski), a much younger woman.  While travelling, he sends a message to his friend Norman Robbins (Albert Brooks) to keep an eye on his wife, but the message is garbled by Claudes Italian valet Giuseppe (Richard Libertini), and instead of looking after Daniella, Norman hires a private detective named Keller (Richard B. Shull) to investigate her.
 Argyle socks, appears to be Maxmillian Stein (Armand Assante), a handsome violinist with the orchestra &ndash; and Claudes protégé &ndash; who is well known as a ladies man.

Claude at first doesnt directly confront Max. When Max eventually meets Daniella, it is at a restaurant where Claude, overwhelmed with jealousy, duels Max with violins by playing a Csárdás (Monti)|Csárdás, the famous composition of Vittorio Monti.
 Violin Concerto", an elaborate plan to kill Daniella and frame Max for the murder runs through his mind, but afterwards, when he tries to carry out his plan, unforeseen circumstances intervene.   

==Cast==
* Dudley Moore as Claude Eastman
* Nastassja Kinski as Daniella Eastman
* Armand Assante as Maxmillian Stein
* Albert Brooks as Norman Robbins
* Cassie Yates as Carla Robbins
* Richard Libertini as Giuseppe
* Richard B. Shull as Jess Keller
* Jan Triska as Jerzy Czyrek
* Jane Hallaren as Janet
* Bernard Behrens as Bill Lawrence Leonard Mann as Screen lover

Cast notes:
* Betty Shabazz, the widow of Malcolm X has a small part in the film. 

==Production==
The project to remake Preston Sturges 1948 Unfaithfully Yours (1948 film)|film, which was an artistic success but not a financial one, was originally intended for Peter Sellers, before his death in 1980. 

==References==
 

==External links==
*  
*  
*  
*  
*   in the New York Times

 
 

 
 
 
 
 
 
 
 
 