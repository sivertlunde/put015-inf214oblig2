Rose of the Golden West
{{Infobox film
| name           = Rose of the Golden West
| image          = Rose of the Golden West (1927) film poster.jpg
| imagesize      = 190px
| caption        = Theatrical release poster
| director       = George Fitzmaurice
| producer       = Richard A. Rowland
| writer         = Minna Caroline Smith (short story: Rose of the Golden West) Eugenie Woodward (short story) Bess Meredyth (scenario, adaptation) Philip Bartolomae (scenario, adaptation) Mort Blumenstock (intertitles)
| starring       = Mary Astor Gilbert Roland Montagu Love
| music          =
| cinematography = Lee Garmes
| editing        =
| distributor    = First National Pictures
| released       = September 25, 1927 (NY Premiere) October 2, 1927 (US nationwide)
| runtime        = 7 reels; 6,477 feet
| country        = United States Silent (English intertitles)
}}
Rose of the Golden West is a surviving 1927 American silent film produced by Richard A. Rowland and released by First National Pictures. It was directed by George Fitzmaurice and starred Mary Astor and Gilbert Roland.  

==Cast==
*Mary Astor - Elena Vallero
*Gilbert Roland - Juan
*Gustav von Seyffertitz - Gomez
*Montagu Love - General Vallero
*Flora Finch - Senora Comba Harvey Clark - Thomas Larkin
*Roel Muriel - The Mother Superior
*Andre Cheron - The Russian Prince
*Romaine Fielding - Secretary
*Thur Fairfax - Orderly
*William Conklin - Commander Sloat
*Christina Montt - Senorita Gonzalez
*Cullen Tate

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 


 
 