Jonas Brothers: The 3D Concert Experience
{{Infobox film
| name           = Jonas Brothers: The 3D Concert Experience
| image          = Jonas Brothers The 3D Concert Experience (poster).jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Bruce Hendricks Johnny Wright Phil McIntyre
| starring       = Kevin Jonas Joe Jonas Nick Jonas Demi Lovato Taylor Swift Robert "Big Rob" Feggans
| music          = Kevin Jonas Joe Jonas Nick Jonas Reed Smoot
| editing        = Michael Tronick
| studio         = Jonas Films Walt Disney Pictures Walt Disney Studios Motion Pictures
| released       =  
| runtime        = 76 minutes  85 minutes   
| country        = United States
| language       = English
| budget         = 
| gross          = $23.2 million   
}}
 RealD 3D Joe and Nick Jonas, also known as the Jonas Brothers, in their big screen debut.

==Plot== third studio This Is Me", Taylor Swift on "Shouldve Said No" and Robert "Big Rob" Feggans, their bodyguard, on "Burnin Up (Jonas Brothers song)|Burnin Up", with a new studio recording "Love Is On Its Way". The film also includes a behind-the-scenes look at the lives of the Jonas Brothers while in New York City, as they do interviews, television performances, along with the midnight release of their third studio album, A Little Bit Longer.

==Cast==
Main
* Kevin Jonas
* Joe Jonas
* Nick Jonas
* Demi Lovato
* Taylor Swift
* Robert "Big Rob" Feggans

Additional
* Kevin Jonas Sr.
* Denise Jonas
* Frankie Jonas

Band members John Taylor - Musical director/guitar
* Jack Lawless - Drums
* Ryan Liestman - Keyboards
* Greg Garbowsky - Bass

==Set list==
# "Intro"/"Waking Up" ("Lovebug (Jonas Brothers song)|Lovebug" instrumental)
# "Opening Credits" ("Tonight (Jonas Brothers song)|Tonight" studio recorded vocals/live band in background)
# "Thats Just the Way We Roll" Hold On" BB Good"
# "Goodnight and Goodbye"
# "Video Girl"
# "Gotta Find You"
# "A Little Bit Longer"   This Is Me"  (with Demi Lovato) 
# "Play My Music" (studio recording)
# "Hello Beautiful"
# "Still in Love with You"
# "Pushin Me Away"
# "Shouldve Said No"  (with Taylor Swift) 
# "Love Is On Its Way"
# "SOS (Jonas Brothers song)|S.O.S."
# "Burnin Up (Jonas Brothers song)|Burnin Up"  (with Robert "Big Rob" Feggans) 
# "Tonight (Jonas Brothers song)|Tonight" (studio recording)    
# "End credits" ("Shelf (song)|Shelf" live)

==Soundtrack==
  soundtrack was released on February 24, 2009, three days before the films release. It debuted at number #3 on the Billboard 200|Billboard 200.

==Reception and box office==
The film received mostly negative reviews from critics; it holds a 25% "Rotten" rating on the review aggregator website Rotten Tomatoes based on 68 critics reviews (15 fresh, 53 rotten).  On Metacritic, the film has a score of 45 out of 100 based on 14 reviews.  In general, critics panned the film for lacking appeal to any people outside the groups fan base.

It is the #6 highest-grossing concert film following  ,   (in which the Jonas Brothers guest starred),   and  .   
  Worst Actor".

==Home media== 3D version, with both one and two-disc DVD editions including only the 2D version of the film. However, for the UK and Australia DVD releases, Disney chose to release the film with the 3D version on DVD, as there was no Blu-ray release in either country.

Blu-ray Extended Edition  3D version of the film
* Two performances not seen in theaters - "Cant Have You" and "A Little Bit Longer"
* Up Close and Personal: Behind the Scenes with the Jonas Brothers

==See also==
* Jonas Brothers
* List of Jonas Brothers concert tours#Burnin Up Tour|Burnin Up Tour
* Demi Lovato
* Taylor Swift
* Camp Rock

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 