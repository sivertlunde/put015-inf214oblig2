Make a Wish (2011 film)
{{Infobox film
| name           = Make a Wish
| image          = 
| caption        = 
| director       = Romana Carén
| producer       = Bruno Santos, Henrietta McCormick
| writer         = Pascoe Foxell Romana Carén
| starring       = Jean-Philippe Heon Lia McQuiston
| music          = Geoffrey Dawes
| cinematography = Alan Besedin
| editing        = Pascoe Foxell
| distributor    = 
| released       =  
| runtime        = 6 minutes
| country        = United Kingdom
| language       = English
| budget         = 
}}
Make a Wish  is a 2011 British short film directed by Romana Carén, about a trunk with the note "Make a wish" on it. It was screened in the Short Film Corner of the Cannes Film Festival  2011. http://www.shortfilmcorner.com/sfcfilm/filmfiche2.Aspx?id=53650461  

==Plot==
Make a Wish is a dramatic comedy with dark undertones. Its a simple cautionary tale which is symbolic for how thoughts can influence our lives and our responsibility for our deeds. John, the protagonist receives a trunk that has the power to fulfill all of his dreams. Gradually he becomes greedier and greedier. He gets what he wishes for, but he is made to pay the price. What seemed like an ideal world, soon turns into something rather unexpected.

==Cast==
* Jean-Philippe Heon- John
* Lia McQuiston - Melanie

==References==
 

==External links==
*  

 
 
 
 
 
 

 