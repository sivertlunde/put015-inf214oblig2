I Like Your Nerve
{{Infobox film
| name           = I Like Your Nerve
| image          = 
| caption        = 
| director       = William C. McGann
| producer       = 
| writer         = Roland Pertwee Houston Branch
| starring       = Douglas Fairbanks, Jr. Loretta Young
| music          = 
| cinematography = Ernest Haller
| editing        = Peter Fritch
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States 
| language       = English
| budget         = 
}}

I Like Your Nerve is a 1931 American romantic comedy film directed by William C. McGann, starring Douglas Fairbanks, Jr. and Loretta Young. Boris Karloff has a small role.  

==Plot==
In Latin America, Larry OBrien  (Douglas Fairbanks, Jr.) sees Diane Forsythe (Loretta Young) and quickly falls in love. She, however, is engaged to marry the much older Clive Lattimer (Edmund Breon). Larry discovers that her motive is to save her stepfather, Areal Pacheco (Henry Kolker), from being shot. Pacheco, the Minister of Finance, has embezzled $200,000 from the national treasury, and an audit is scheduled soon. Lattimer is extremely wealthy and willing to make up the shortfall in exchange for Diane.

To keep Larry from disrupting the arrangement, Pacheco has his butler Luigi (Boris Karloff) arrange Dianes kidnapping (to Lattimers country estate). Larry rescues Diane and leaves her in the care of his friend, Archie Lester (Claud Allister). Then he telephones a newspaper to report his "ransom" demand of $200,000. Citizens demand Pacheco pay the sum, but Larry points out that Dianes fiance is the only person in the country with access to that much money. Lattimer refuses at first, but soon caves in to the outrage. Larry collects the money at the arranged dropoff point and later presents it to Pacheco, who then cancels Dianes engagement to Lattimer.

==Cast==
* Douglas Fairbanks, Jr. as Larry OBrien
* Loretta Young as Diane Forsythe
* Henry Kolker as Areal Pacheco
* Claud Allister as Archie Lester
* Edmund Breon as Clive Lattimer
* Boris Karloff as Luigi, Pachecos butler
* Luis Alberni as Sao Pedro Waiter
* Peter Gawthorne as Roberts, Lattimers Butler
* George Chesebro as Captain of the Guards

==See also==
* Boris Karloff filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 