Scattergood Survives a Murder
{{Infobox film
| name           = Scattergood Survives a Murder
| image          = Scattergood Survives a Murder poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Christy Cabanne Ridgeway Callow (assistant)
| producer       = Jerrold T. Brandt Frank Melford (associate)
| writer         = 
| screenplay     = Michael L. Simmons
| story          = 
| based on       =   John Archer Margaret Hayes Wallace Ford Spencer Charters Eily Malyon
| narrator       = 
| music          = Paul Sawtell
| cinematography = Jack MacKenzie
| editing        = Richard Cahoon
| studio         = Pyramid Pictures RKO Radio Pictures
| released       =   }}
| runtime        = 66 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}

Scattergood Survives a Murder is a 1942 American mystery film directed by Christy Cabanne from a screenplay by Michael L. Simmons, based on the series of short stories about "Scattergood Baines", penned by Clarence Budington Kelland.

==Cast==
*Guy Kibbee as Scattergood Baines John Archer as Dunker Gilson
*Margaret Hayes as Gail Barclay
*Wallace Ford as Wally Collins
*Spencer Charters as Sheriff
*Eily Malyon as Mrs. Grimes
*John Miljan as Rolfe
*George Chandler  as Sam Caldwell
*Dick Elliott as Mathew Quentin
*Florence Lake as Phoebe Quentin
*Sarah Edwards as Selma Quentin
*Willie Best as Hipp
*George Guhl as Deputy
*Eddy Waller as Lafe Allen
*Margaret Seddon as Cynthia Quentin
*Margaret McWade as Lydia Quentin
*Frank Reicher as Thaddeus Quentin
*Earle Hodgins as Coroner 

==References==
 

 
 
 
 
 
 
 
 
 