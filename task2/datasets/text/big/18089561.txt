A Man's Work (film)
 
{{Infobox film
| name           = A Mans Work
| image          = A Mans Work poster.jpg
| caption        = Theatrical release poster
| director       = Aleksi Salmenperä
| producer       = Petri Jokiranta Tero Kaukomaa
| writer         = Aleksi Salmenperä
| starring       = Tommi Korpela Maria Heiskanen Jani Volanen
| music          = Ville Tanttu
| cinematography = Tuomo Hutri
| editing        = Samu Heikkilä
| studio         =
| distributor    = Sandrew Metronome
| released       =  
| runtime        = 97 minutes
| country        = Finland
| language       = Finnish
| budget         =
| gross          = 
}}
A Mans Work ( ), also known as Mans Job in the United States, is a 2007 Finnish drama film directed by Aleksi Salmenperä. It was Finlands submission to the 80th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.    It was also entered into the 29th Moscow International Film Festival.   

==Cast==
* Tommi Korpela as Juha
* Maria Heiskanen as Katja
* Jani Volanen as Olli
* Stan Saanila as Jamppa
* Konsta Pylkkönen as Akseli
* Noora Dadu as Nuori nainen 1
* Joanna Haartti as Nuori nainen 2
* Kaarina Hazard as Asiakas / lääkäri 1
* Vilma Juutilainen as Oona
* Helmi Kaartinen as Ida
* Arttu Kapulainen as Nuori taksikuski
* Leea Klemola as Katrin assistantti

==See also==
*Cinema of Finland
*List of submissions to the 80th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 