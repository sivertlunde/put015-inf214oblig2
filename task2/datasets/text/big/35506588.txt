Skyscraper (2011 film)
{{Infobox film
| name           = Skyscraper
| image          = 
| caption        = 
| director       = Rune Schjøtt
| producer       = Morten Kjems Juhl Birgitte Skov
| writer         = Rune Schjøtt
| starring       = Lukas Schwarz Thorsteinsson
| music          = 
| cinematography = Marcel Zyskind
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Denmark
| language       = Danish
| budget         = 
}}

Skyscraper ( ) is a 2011 Danish drama film written and directed by Rune Schjøtt.    

==Cast==
* Lukas Schwarz Thorsteinsson as Jon
* Marta Holm Peschcke-Køedt as Edith (as Marta Holm)
* Lucas Schultz as Ben
* Morten Suurballe as Farmand
* Rikke Louise Andersson as Vivi
* Lars Brygmann as Helge
* Mads Riisom as Fyr
* Emil Haugelund as Gut
* Julie Grundtvig Wester as Gravid pige
* Helena Wagn Ivansdottir as Tøs (as Helena Wagn Ivandóttir)
* Jeff Pitzner as Pølse Anders Hove as Buschauffør

==References==
 

==External links==
*  

 
 
 
 
 
 
 