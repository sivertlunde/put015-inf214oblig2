Crimes of the Future
{{Infobox film
| name           = Crimes of the Future
| image          = Stereo+Crimes of Future.jpg Stereo and Crimes of the Future
| director       = David Cronenberg
| producer       = David Cronenberg
| writer         = David Cronenberg 
| narrator       = Ronald Mlodzik
| starring       = Ronald Mlodzik Jon Lidolt Tania Zolty Jack Messinger Paul Mulholland William Haslam William Poolman
| music          = 
| cinematography = David Cronenberg
| editing        = David Cronenberg
| distributor    = New Cinema Enterprises
| released       = June, 1970 (Australia)
August 10, 1984 (USA)
| runtime        = 70 minutes
| country        = Canada
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} edited and directed by David Cronenberg.    The film stars Ronald Mlodzik. Also like Stereo (1969 film)|Stereo it was shot silent with a commentary added afterwards. The commentary is spoken by the character Adrian Tripod. This film is set in 1997.

==Summary==

Crimes of the Future details the wanderings of Tripod (Mlodzik), sometime director of a dermatological clinic called the House of Skin, who is searching for his mentor, the mad dermatologist Antoine Rouge. Rouge has disappeared following a catastrophic plague resulting from cosmetic products, which has killed the entire population of sexually mature women. Tripod joins a succession of organisations including Metaphysical Import-Export and the Oceanic Podiatry Group, and meets various individuals and groups of men who are trying to adjust themselves to a defeminized world. One man parodies childbirth by continually growing new organs which are removed from his body. Eventually Tripod comes upon a group of paedophiles which is holding a 5 year-old girl, and they urge him to mate with her. He senses the presence of Antoine Rouge.

==Reception==
Kim Newman, in his 1988 book Nightmare Movies, has described Crimes of the Future as being "more fun to read about in synopsis than to watch", and as proving, along with Stereo, that "its possible to be boring and interesting at the same time."

The film has received a fresh rating of 60% on Rotten Tomatoes.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 


 