Aap Kaa Surroor (film)
 
 
{{Infobox film
| name           = Aap Kaa Surroor
| image          = AKS Moviee Poster.jpg
| image size     =
| caption        = Theatrical release poster
| director       = Prashant Chadha
| producer       = Vijay Taneja
| story          = Vibha Singh
| narrator       =
| starring       = Himesh Reshammiya Hansika Motwani Mallika Sherawat
| music          = Himesh Reshammiya
| cinematography = Manoj Soni
| editing        = Sudhir Mehan
| studio         = Mehboob Studio
| released       = 29 June 2007
| runtime        = 127 minutes
| country        = India
| language       = Hindi
| budget         =  
| gross          =  
| preceded by    =
| followed by    =
}} 2007 Bollywood film directed by Prashant Chadha and starring popular singer Himesh Reshammiya in his movie debut as an actor, alongside Hansika Motwani and Malika Sherawat. Reshammiya has claimed the story is based on his own life and was named after his music album Aap Kaa Surroor.  It was shot mostly in Germany  and was released on 29 June 2007. Critics panned the film although it turned out to be the biggest surprise hit of 2007. 
Seeing the immense response at the box-office, Karan Johar invited Himesh Reshammiya to his show Koffee With Karan.

==Synopsis==
The movie begins with a dead body of a TV journalist, Nadia Merchant, being found in a remote area, somewhere in Germany. Soon after, Indian singer Himesh Reshammiya (HR) is arrested after a concert for murdering the journalist. HR is put behind bars and the incidents begin to unravel as the movie goes into a flashback. While on a concert in Germany, performing with his best friend Shravan, HR meets the event organiser Khurana (Darshan Jariwala) and his partner Ruby (Mallika Sherawat). He also meets the event planner Ria (Hansika Motwani), and its love at first sight. After initial reservations, Hansikas father (Sachin Khedekar) approves of the match.

Things take a turn when HR is arrested. He asks Ruby, a lawyer, to bail him out. But hell hath no fury like a woman scorned: Ruby is in love with HR, but the rock star loves Ria. He escapes from his prison cell after taking the murdered TV journalists father Feroz (Raj Babbar) captive. He has to find the actual murderer in one day or else his sweetheart will be married to someone else. To stop this from happening, he needs Rias help. He approaches her; she is currently ignoring him.  
 Gurbani Judge) approach Ruby (on HRs hunch that Ruby is innocent of the murders) for help. They browse through invoices and other documents in Khuranas office to find that acute losses on a previously finalised world tour deal gave Khurana the motive to frame HR. HR then proceeds to confront Khurana and, after a car chase, he catches up with him. In the final scene HR tricks him into revealing his motives, while a Hindi-speaking tourist/native translates for the police. HR is declared innocent. Khurana reveals that he wore a face mask to appear like HR and committed the murder to frame him.

HR is released from jail, Khurana is arrested, and Ria marries HR. Shravan then agrees to feature Ruby in one of HRs music video. The movie ends to HR singing the song "Mehbooba" from the classic Bollywood film, Sholay, with Ruby dancing while being recorded.

==Cast==
* Himesh Reshammiya as Himesh Reshammiya/HR
* Hansika Motwani as Riya Bakshi
* Mallika Sherawat as Ruby James
* Darshan Jariwala as Khurana
* Raj Babbar as Feroz Merchant Shravan as Shravan Kumar
* VJ Bani as Bani, Riyas friend
* Sachin Khedekar as Riyas father
* Anant Mahadevan in a special appearance
* Lovepreet Aujla Himeshs friend
* Marrissa Lawrence as Nadia F. Merchant
* Ishita Chauhan as Trishnu
* Sanjay Sharma as Raju

==Box office and reception==
The film received jose james award.  According to Boxoffice India, Aap Ka Surroor grossed   and was given the final verdict of a Super Hit. 

==Soundtrack==
{{Infobox album| 
| Name        = Aap Ka Surroor
| Type        = soundtrack
| Artist      = Himesh Reshammiya
| Cover       = 
| Released    = 
| Recorded    = 
| Producer =  Feature film soundtrack
| Length      = 44:02
| Label       = T-Series
}}
The music was composed by Himesh Reshammiya; the lyricist was Sameer (lyricist)|Sameer.

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Song !! Singer(s)!! Duration
|-
|Assalam Vaalekum
| Himesh Reshammiya
| 6.00
|-
| Tera Mera Milna
| Himesh Reshammiya, Shreya Ghoshal
| 5.50
|-
| Jhoot Nahin Bolna
| Himesh Reshammiya, Shreya Ghoshal
| 6.10
|-
| Tanhaiyaan
| Himesh Reshammiya, Sunidhi Chauhan
| 5.06
|-
| Ya Ali
| Himesh Reshammiya, Sunidhi Chauhan
| 4.32
|-
| Tere Bina
| Himesh Reshammiya
| 5.35
|-
| Kya Jeena
| Himesh Reshammiya
| 5.14
|-
| Mehbooba Mehbooba
| Himesh Reshammiya, Asha Bhosle
| 4.54
|-
| Tanhaiyaan (unplugged)
| Himesh Reshammiya, Sunidhi Chauhan
| 2.00
|-
| Tera Tera Tera Surroor 
| Himesh Reshammiya
| 4.05
|}

==Sequel==
A sequel has been announced: Aap Kaa Surroor 2 – Ae Himesh Bhai! The film will be directed by Prashant Chadha. Himesh Reshammiya, Mallika Sherawat and Hansika Motwani have been offered a role. 

== References ==
 

==External links==
*  

 
 
 