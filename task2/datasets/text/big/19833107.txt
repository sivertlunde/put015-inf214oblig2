Morituri (1948 film)
 
{{Infobox film
| name           = Morituri
| image          = Morituri_Film_poster.jpg
| caption        = Original German poster
| director       = Eugen York
| producer       = Artur Brauner
| writer         = Artur Brauner (idea) Gustav Kampendonk
| starring       = Walter Richter   Winnie Markus   
| music          = Wolfgang Zeller
| cinematography = Werner Krien
| editing        = Walter Wischniewsky
| studio         = CCC Film
| distributor    = Schorcht Filmgesellschaft mbH
| released       =  
| runtime        = 88 minutes
| country        = Germany 
| language       = German
| budget         = 
}}

Morituri is a 1948 German black and white drama film produced by Artur Brauners  CCC Film. The film was directed by Eugen York and starred Walter Richter, Winnie Markus and  . It features the onscreen debut of German actor Klaus Kinski    as a Dutch concentration camp prisoner. 

==Plot==
As the end of the Second World War approaches and the Soviet Red Army is advancing, a group of concentration camp inmates is helped to escape by a Polish doctor. They hide in a wood where they meet other fugitives, who have been there for months, constantly in fear of being discovered. Out of fear of the German army patrols, they do not dare to leave the forest, even as the food supplies run low. The Polish doctor blows up a bridge, attracting the German troops attention to the forest. The soldiers come perilously close to the hidden fugitives, but in the last moment have to retreat before the approaching Red Army units.

==Cast==
* Walter Richter as Dr. Leon Bronek
* Winnie Markus as Maria Bronek
*   as Lydia
* Hilde Körber as Insane Woman
* Catja Görna as Stascha Sokol
* Josef Sieber as Eddy
* Carl-Heinz Schroth as Armand
* Siegmar Schneider as Gerhard Tenborg
* Peter Marx as Pjotr, Russian
* Alfred Cogho as Roy, Canadian
* Joseph Almas as Dr. Simon (as Josef Almas)
* Ellinor Saul-Gerlach as Lucie, his daughter (as Ellinor Saul)
* Ursula Bergmann as Ruth, his daughter
* Willy Prager as Father Simon
* Annemarie Hase as Mother Simon
* Karl Vibach as Georg, German Soldier
* Bob Kleinmann as Janek, 12 years
* Michael Günther as Wladek, 16 years
* Erich Dunskus as Sokol, Polish Farmer
* David Minster as The Invalid
* Franja Kamienietzka as Mrs Steppan
* Klaus Kinski as Dutch Prisoner
* Gabriele Heßmann as The Pregnant Woman

==Production==
The title comes from the Latin expression Ave Imperator, morituri te salutant. 

Making this film was a very personal project for Artur Brauner. The script is based on an idea of his and this was only the second film made by his company CCC Film.   

Exteriors were shot near Berlin in Brandenburg, interiors in Berlin-Tempelhof. Principal cinematography was from September 1947 to January 1948.  

==Reception==
The film was first shown on 28 August 1948 at the Venice Film Festival on the Lido di Venezia, Italy. 

It premiered in the Waterloo-Theater,   Institut / Cinematography of the Holocaust. Retrieved 2 March 2012 

Morituri was aired on German television station ZDF on 7 April 1991.  

In 2009 Artur Brauner donated the film to Yad Vashem along with 20 other Holocaust-related films he had produced.  

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 
 