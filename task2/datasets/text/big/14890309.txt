Hong Kong Dreaming
   British actress model and beauty queen Fiona Man. Kai Wong    has worked with Academy Award winners such as Ismail Merchant and had previously been shortlisted for a starring roles in Dark Matter co-starring Meryl Streep after withdrawing from Army Daze, and also approached to guest star as Meadow Sopranos Asian-American boyfriend on The Sopranos.

==Principal photography== prestigious Kowloon Central such as Ladder Street, other enclaves south of the Island and Hong Kong Jockey Club.

==Plot==
The film traces the journey of Fiona, a young girl who returns to Hong Kong from London to attend her estranged fathers funeral. She seeks to get a closure on the relationship but finds out that though he died penniless and alone, surprisingly he also left something for her. In the meanwhile she meets her best friend from school Charlie, played by Kai Wong, as a nerdy, endearing and angst-filled ex-classmate. Together, they seek to find her inheritance. But they also find in each other something that was lost from within. While Charlie was helping Fiona find the memories of her dad father, Fiona eventually finds her inheritance of her lost father in the growing love in Charlies heart.

==Genre==
The film is a romantic comedy.

== References ==
 
 

== External links ==
* http://www.imdb.com/name/nm0580337/bio
* http://bengaloored.com/
*  A Small Article in a Daily English Newspaper in India on the film.

 
 
 
 


 