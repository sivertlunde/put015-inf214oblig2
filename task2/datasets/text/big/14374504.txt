Three (1965 film)
{{Infobox film
| name           = Three
| image          = 
| alt            = 
| caption        =  Aleksandar Petrović
| producer       = 
| writer         = Aleksandar Petrović ( ) Antonije Isaković ( ) Milan Jelić
| music          = 
| cinematography = Tomislav Pinter
| editing        = Mirjana Mitić
| studio         = 
| distributor    = 
| released       = 1965
| runtime        = 80 minutes
| country        = Yugoslavia
| language       = Serbo-Croatian
| budget         = 
| gross          = 
}}
 Aleksandar Petrović. It was nominated for the Academy Award for Best Foreign Language Film at the 39th Academy Awards.   

==Theme==
The theme of the film is the death, in three forms: as witness of it, as a victim of it, and as an executor.
Three was the first Yugoslav movie released in the United States (in 1966).

Aleksandar Petrovićs films Three and I Even Met Happy Gypsies provided the world an introduction to Yugoslav cinema. Unlike ‘Three’ it was very well received and translated in over 100 languages.

==Statement by director== The movie Three  (Tri)  is anti-war.  It depicts war’s utter bestiality, waste and absurdity. Death is the indisputable protagonist. She  takes  three different appearances; as punishment, as victim and as the expression of the senselessness of war.
You  must  be  against  war,  but  really, fully,  against  all  the  actors  of  the  war. And  against  those  who  create  reasons for war. |source=Aleksandar Petrović}}.

==Cast==
* Bata Živojinović as Miloš Bojanić
* Nikola-Kole Angelovski
* Stole Aranđelović
* Dragomir Bojanić Milan Jelić
* Branislav Jerinić as Komandir
* Laza Jovanović
* Mirjana Kodžić
* Vesna Krajina as Vera
* Voja Mirić as Partizan
* Slobodan Perović as Nevino optuženi
* Ali Raner as Mladić

== Awards, Honors ==
* 	Academy Awards Nomination for Best Foreign Language Film-  1966 (39th edition)
* -	XIIth Pula Film Festival (Yugoslavia), 1965: GRAND PRIX (Golden Arena) for Best Film, 
* -	XIIth Pula Film Festival (Yugoslavia), 1965: GRAND PRIX (Golden Arena) for Best Director
* -	XIIth Pula Film Festival (Yugoslavia), 1965: GRAND PRIX (Golden Arena) for Best Actor
* -	XIIth Pula Film Festival (Yugoslavia), 1965: Critics Award “Milton Manaki” 
* -	Bronze Plaque (Bronzana Plaketa) – BUNINOVA  VRATA, Yugoslav award, 1965
* -	XVth Karlovy Vary (Karlsbad) International Festival,1966 GRAND PRIX for Best Film (HLAVNA CENA I)
* -	IXth Acapulco Festival of Best Movies, 1966 - Award "PALENKA", Golden Inca Head
* -	Festival of Italian neorealism – Avellino, 1966 - Award LACENO DORO
* -	In 1979, in a survey organized by the Yugoslav Film Institute for The Best Film in the history of the Yugoslav Cinema, the Yugoslav Film Critics and Artists put Three in second place behind I Even Met Happy Gypsies, from Aleksandar Petrović, declared the Best Film in the History of Yusgoslav Cinema.

==See also==
* List of submissions to the 39th Academy Awards for Best Foreign Language Film
* List of Yugoslav submissions for the Academy Award for Best Foreign Language Film

== Press excerpts ==
A review from the New York Times from 1967 when it was nominated for Best foreign film at the Academy Awards
War’s utter bestiality and waste, usually illustrated by armies, is brought into sharp focus by a talented few in “Three,” a prize-winning Yugoslav drama that treats its bleak and harrowing subject with a grim but poetic artistry. It had a showing at the New York Film Festival last year, and is now at the Studio Cinema and 72d Street Theaters. The film is mystifyingly abrupt in its transitions, but its effects, physical and intellectual, are unmistakably forceful and chilling. The director, Aleksandar Petrovic, with the aid of a sparse script and stunning photography by Tomislav Pinter, has pointed up war’s ravages as it affects one partisan’s fights in one small sector of the conflict. In each of three events he is part of, needless death brought about by fear, despair and defeat.

==References==
 

==External links==
* Official Website   
* Vlastimir Sudar:  Portrait de l’artiste en tant que dissident politique: La vie et l’œuvre d’Aleksandar Petrovic (The life and work of Aleksandar Petrovic: A portrait of the artist as a political dissident – INTELLECT, Bristol, INTELLECT, Chicago 2013)
* 

 
 

 
 
 
 
 
 
 
 