Evilspeak
{{Infobox film
| name           = Evilspeak
| image          = Evilspeak-poster.jpg
| image_size     = 
| caption        = Theatrical poster
| director       = Eric Weston
| producer       = Eric Weston Sylvio Tabet Gerald Hopman
| writer         = Eric Weston  Joseph Garofalo
| starring       = Clint Howard R.G. Armstrong Joseph Cortese Claude Earl Jones
| music          = Roger Kellaway
| cinematography = Irv Goodnoff
| editing        = Charles Tetoni
| studio         = Leisure Investment Company Coronet Film
| distributor    = Moreno Films
| released       = August 22, 1981  (Japan)  February 26, 1982  (USA) 
| runtime        = 89 min.
| country        = United States
| language       = English
| budget         = $1,000,000
}}

Evilspeak (also known as Evilspeaks) is a 1981 American horror film written by Eric Weston and Joseph Garofalo and directed by Weston.
 Video Nasties" banned in the United Kingdom in the 1980s.

== Plot ==

Stanley Coopersmith (Clint Howard), a young cadet at an American military academy, is a social outcast bullied by his classmates.
  Satanic leader Father Estaban (Richard Moll) and his followers on the shore of Spain during the Dark Ages. They are approached by a church official who tells them they are banished from Spain and denied Gods grace unless they renounce Satan and their evil ways.
 
In the present, Coopersmith is being punished by being forced to clean the church cellar. While cleaning the darkened cellar he finds a room belonging to Father Estaban which contains books of black magic along with Father Estabans diary. Coopersmith becomes fascinated with the book and uses his computer skills to translate it. In the translation he discovers that Estaban was a Satanist and the book contains rituals for performing the Black Mass along with a promise by Estaban "I Will Return". Coopersmith plots his revenge on his classmates using black magic. After arriving the mess hall too late for lunch, he befriends the schools cook who makes a meal for him and shows him a litter of puppies that his dog just had. Coopersmith takes the smallest pup for himself, names him Fred and hides him in the church cellar. After consulting the translation of Estabans diary he discovers he needs a consecrated host and blood to complete the list of items needed for the Black Mass. Coopersmith steals the host from the church, he then notices Estabans portrait on the wall. Using the translation he attempts the ritual and is suddenly attacked by his classmates wearing masks and robes. After knocking him unconscious they leave. Coopersmith, thinking he has successfully performed the ritual is told by his computer that the ritual was incomplete and a pentagram appears on the computer screen. When Coopersmith accidentally wakes the drunken caretaker he is accused of being a thief for stealing his crowbar the caretaker attempts to break Freds neck, Coopersmith then kicks him the groin. The caretaker attacks Coopersmith who screams for help and the computer flares to life with a red pentagram on it. An unseen force then takes the caretakers head and turns it completely around breaking his neck. Coopersmith then discovers a catacomb filled with decapitated skeletal remains and the crypt of Father Estaban. Hiding the caretakers body he leaves.

When hes sent to the office Coopersmith accidentally leaves the diary on the desk of the school secretary who hides it. While Coopersmith is being made to clean the stables the office secretary begins to finger the jewels on the front of the diary. Trying to pry the jewels out of their settings causes the pigs in the stable to attack Coopersmith. Unable to remove them, the secretary takes the book home with her. She then disrobes, gets into a shower and is attacked and devoured by black boars that manifest out of nowhere.

After watching a beauty pageant at the schools pep rally hes told by his classmates if tries to play in the big game tomorrow theyll find and kill Fred. After witnessing his beating the school principal kicks him off the soccer team.

After a night of drinking Coopersmiths classmates make their way into Estabans hidden room and find Coopersmiths computer program. After killing Fred the computer says that the blood used must be human blood. After finding Freds mutilated body  Coopersmith becomes completely enraged. The diary then appears laying on Estabans casket. When a teacher catches Coopersmith in the church stealing the host he follows him to the catacombs where Coopersmith is translating the rest of the diary. Coopersmith pledges his life to Satan then kills his teacher on a spiked wheel and collects his blood.

Unaware of the ritual being performed below Coopersmiths classmates,coach, principal and minister are all in attendance at a service above. After successfully performing the ritual Father Estabans soul then possesses Coopersmiths body and takes up a sword. Meanwhile a nail from the large crucifix hanging over the churchs altar is pried out by an unseen force and flies across the room and is driven into the ministers skull. Coopersmith then rises from the cellar below engulfed in flames and wielding a sword. A pack a large black boars pours out of the hole in the floor where Coopersmith now hovers above everyone else.

He then decapitates his principal and his coach. His classmates try to flee only to be devoured by the boars. In the catacombs the lead bully tries to escape only to have the caretaker come back to life and remove the boys heart while it is still beating. The caption at the end says that Stanley Coopersmith survived the attack but after witnessing the fiery death of his classmates went catatonic from shock and was sentenced to Sunnydale asylum where he remains. The end of the film shows Coopersmiths true fate as his face appears on the computer screen in the cellar with the words "I Will Return".

== Cast ==

* Clint Howard as Stanley Coopersmith
* R. G. Armstrong as Sarge
* Joseph Cortese as Reverend Jameson
* Claude Earl Jones as Coach
* Haywood Nelson as Kowalski
* Don Stark as Bubba
* Charles Tyner as Colonel Kincaid
* Hamilton Camp as Hauptman
* Louie Gravance as Jo Jo
* Jim Greenleaf as Ox
* Lynn Hancock as Miss Friedemeyer
* Lenny Montana as Jake
* Richard Moll as Father Esteban

== Production ==

The story was originally titled The Foundling – a script written by Joseph Garofalo. Garofalo later reworked it in conjunction with actor Eric Weston who tightened up the storylines and added the computer elements. 

A budget of $1,000,000 was raised, half from Lebanese producer Sylvio Tabet who had made the The Beastmaster (film)|Beastmaster series of films and half from a group of doctors looking to invest some money. 
 Santa Barbara and a condemned church in South Central Los Angeles.  According to DVD commentary the dilapidated church was superficially renovated for the movie shoot, confusing a priest who previously worked there and causing him to get on his knees and pray to God. The church was burned to the ground some three days later.

== Release and controversy ==

Evilspeak was released on August 22, 1981 in Japan, and February 26, 1982 in the United States.

The movie was cited as a video nasty in the UK following its release on the Videospace label. It remained banned for a number of years as part of the Video Recordings Act 1984, thanks to its gory climax and themes of Satanism.

The film was reclassified and re-released in 1987 but with over three minutes of cuts, which included the removal of most of the gore from the climax and all text images of the Black Mass on the computer screen. It was then subsequently passed uncut by the BBFC in 2004 and is now available in both an uncut form and a version re-edited by the distributors to tighten up the dialogue. 

  of the Church of Satan, was a great fan of the film and considered it to be very Satanic. 

Actor Clint Howard said that director Eric Westons original version of the film that was submitted to the MPAA was longer and contained more blood, gore and nudity than the unrated version of the film, especially during the shower/pig attack scene and the final confrontation. 

== Critical reception ==

Evilspeak has been generally well-received by critics, with many comparing it to the 1976 Stephen King film adaptation of Carrie (1976 film)|Carrie, for better or worse. AllMovie called it "essentially a gender-bent rip-off of Carrie", though "there is enough in the way of spooky atmosphere and well-staged shocks to keep less discriminating horror fans interested."  PopMatters gave the film a 7/10 grade, despite writing "What started as a standard wish fulfillment/revenge scheme mixed with Satanism flounders with a lack of focus."  DVD Verdict wrote "Evilspeak is a crazy movie. Like, crazy. In a good way. Unfortunately, its also kind of boring at times, taking well over an hour to get where its going.   Despite the slower spots—and there are plenty of slower spots—Evilspeak remains an enjoyably overlooked horror film just for its eccentricities." 

A slightly more favourable review came from TV Guide, who wrote "The directorial debut of Eric Weston, Evilspeak is remarkably engaging, imaginative and well-crafted. It contains a strong performance from Howard, plus a deliciously over-the-top nasty turn by veteran character actor R.G. Armstrong." 

== References ==

 

== External links ==

*  

 
 
 
 
 
 