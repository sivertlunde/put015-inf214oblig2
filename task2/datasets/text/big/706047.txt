Spirit: Stallion of the Cimarron
 
{{Infobox film
| name        = Spirit: Stallion of the Cimarron 
| image       = Spirit Stallion of the Cimarron poster.jpg
| caption     = Theatrical release poster
| director    = Kelly Asbury Lorna Cook
| producer    = Max Howard Mireille Soria Jeffrey Katzenberg
| writer      = John Fusco
| story = Jeffrey Katzenberg
| starring    = Matt Damon James Cromwell Daniel Studi
| music       = Hans Zimmer  
| editing     = Nick Fletcher Clare De Chenu 
| studio      = DreamWorks Animation   
| distributor = DreamWorks Pictures 
| released    =  
| runtime     = 83 minutes
| country     = United States
| language    = English
| budget      = $80 million   
| gross       = $122,563,539 
}}
 animated Western western Drama drama film anthropomorphic style in other animated features, Spirit and his fellow horses communicate with each other through sounds and body language. Spirits thoughts are narrated by his voice actor Matt Damon, but otherwise he has no dialogue. 

==Plot==
  US National Parks.
 army is Indian Wars and taking over the soon-to-be western United States. Frightened and confused, Spirit sees horses used as slaves all around him. There, he encounters "The Colonel". He decides to tame the mustang, refusing to believe the idea of Spirit being too stubborn, but Spirit manages to fight off the brander even when heavily restrained and throws off all who attempt to ride him, impressing the other horses and reigniting their spirit. To punish Spirit, The Colonel orders him to be tied to a post for three days with no food or water.
 Lakota Indigenous Native American ransacks the vicious battle, Transcontinental Railroad, where they are put to work pulling a steam locomotive. Not understanding, Spirit takes it as another challenge when he realizes that if the track extends along its present course, it will infringe on his homeland. Spirit fakes injuries resulting from exhaustion, then proceeds to break free from the sledge and breaks the chains holding the other horses. They escape, and the locomotive falls off its sledge and rolls down the hill chasing Spirit. When they reach the work site where the locomotive demolishes a building as it plows through it, then it crashes into another locomotive parked on the tracks and smashes into it, but because the locomotive is already under steam, its boiler explodes as its being crushed. After the locomotive explosion, Spirit rushes out of the work site as the spreading flame starts to set it on fire. As it does, it lights a shed aflame which explodes because it was stored with explosives. The explosion causes a forest fire, and Spirit starts to gallop away as fast as he can, but the end of the chain that is wrapped around his neck gets snagged on a fallen tree. Little Creek appears in time and saves Spirit from the wildfire. The next morning, the Colonel and his men find Spirit and Little Creek playing with each other. A climactic chase scene ensues on rock passages that lead to the Grand Canyon, where the two again outsmart the soldiers. Eventually, they are trapped by a gorge and forced to summit onto a cliff which faces over the gorge (beyond it is open space). Little Creek gives up, but Spirit manages to gain the bravery to leap off the cliff and onto the open land.

Spirits move amazes the Colonel; he stops his men from shooting the two. Knowing its for the best, the Colonel exchanges nods of respect to the two before walking off with the rest of the soldiers. Spirit returns to the rebuilt Lakota village with Little Creek and finds Rain still alive, nursed back to health by the Lakota people. Little Creek decides to name Spirit the "Spirit-Who-Could-Not-Be-Broken" and sets him and Rain free. He also removes the feather out of Rains mane and promises her that she will always be in his heart. Spirit and Rain travel day and night to his homeland. Eventually, the two horses joyfully meet up with Spirits own herd. The eagle then appears as he flies up into horse-shaped clouds.

==Voice cast==
* Matt Damon as Spirit
* James Cromwell as the Colonel
* Daniel Studi as Little Creek
* Chopper Bernet as Sgt. Adams
* Jeff LeBeau as Murphy, and the Railroad Foreman
* Richard McGonagle as Bill
* Matt Levin as Joe
* Robert Cait as Jake Charles Napier as Roy
* Zahn McClarnon as one of Little Creeks friends
* Michael Horse as a friend of Little Creek
* Donald Fullilove as the Train Pull Foreman

==Production==

===Development=== Young Guns and Young Guns II), was hired by DreamWorks to create an original screenplay based on an idea by Jeffrey Katzenberg. Fusco began by writing and submitting a novella to the studio, and then adapted his own work into screenplay format. He remained on the project as the main writer over the course of four years, working closely with Katzenberg, the directors, and artists.

===Animation=== traditional hand-drawn tradigital animation."  DreamWorks SKG purchased a horse as the model for Spirit and brought the horse to the animation studio in Glendale, California for the animators to study. In the sound department, recordings of real horses were used for the sounds of the many horse characters hoof beats as well as their vocalizations. None of the animal characters in the film speak English beyond occasional reflective narration from the protagonist mustang (voice of Matt Damon). Many of the animators who worked on Spirit also worked on Shrek 2, and their influence can be seen in the horses in that film, such as Prince Charmings horse from the opening sequence and Donkeys horse form.
 Glacier National Grand Teton Bryce Canyon and the Grand Canyon.

==Music==
  Here I Am" was written by Bryan Adams, Gretchen Peters, and Hans Zimmer. It was produced by Jimmy Jam and Terry Lewis. Another song, not included in the film itself (although it can be heard in the ending credits), is "Dont Let Go", which is sung by Bryan Adams with Sarah McLachlan on harmonies and piano. It was written by Bryan Adams, Gavin Greenaway, Robert John "Mutt" Lange, and Gretchen Peters. Many of the songs and arrangements were set in the American West, with themes based on love, landscapes, brotherhood, struggles, and journeys. Garth Brooks was originally supposed to write and record songs for the film but the deal fell through.

The Italian versions of the songs were sung by Zucchero.
 Paulo Ricardo. The Norwegian versions of the songs were sung by Vegard Ylvisåker of the Norwegian comedy duo Ylvis.
The French version of the movie soundtrack were sung in French by Bryan Adams.

==Release==

===Reception===
Based on 126 reviews collected by   score of 6.4/10.  Review aggregator Metacritic gave the film a score of 52 based on 29 reviews, indicating "mixed or average reviews".    Critic Roger Ebert, said in his review of the film, "Uncluttered by comic supporting characters and cute sidekicks, Spirit is more pure and direct than most of the stories we see in animation – a fable I suspect younger viewers will strongly identify with." Leonard Maltin considered the film as "One of the most beautiful and exciting animated features ever made". Reel Source News called Spirit, "The Best Animated Story Since The Lion King". Clay Smith of Access Hollywood considered the film "An Instant Classic".

The film was screened out of competition at the 2002 Cannes Film Festival.   

Rain received an honorary registration certificate from the American Paint Horse Association (APHA), which has registered more than 670,000 American Paint Horses to date. She is the first animated horse to be registered by this organization.

===Political orientation claims===
Critics have claimed the film has a liberal slant. Conservative pundits claim that view of Native Americans is revisionist.  Christopher Miller, of the JMP Press said, "Despite the terrific animation, the film has a clear message that Caucasian settlers, the logging industry and any technological advance is meant to cage and kill anyone or anything that gets in their way." 

===Box office===
When the film opened on Memorial Day Weekend 2002, the film earned $17,770,036 on the Friday-Sunday period, and $23,213,736 through the four-day weekend for a $6,998 average from 3,317 theaters. The film overall opened in fourth place behind  , Spider-Man (2002 film)|Spider-Man and Insomnia (2002 film)|Insomnia. In its second weekend, the film retreated 36% to $11,303,814 for a $3,362 average from expanding to 3,362 theaters and finishing in fifth place for the weekend. In its third weekend, the film experienced a good hold of only an 18% slippage to $9,303,808 for a $2,767 average from 3,362 theaters. 
The film closed on September 12, 2002 after earning $73,280,117 in the United States and Canada with an additional $49,283,422 overseas for a worldwide total of $122,563,539, against an $80 million budget, making it a moderate box office success.

===Accolades===
{| class="wikitable" style="width:100%;"
|-
! Award !! Category !! Winner/Nominee recipient(s) !! Result
|-  ASCAP Film and Television Music Awards  || Top Box Office || Hans Zimmer Bryan Adams ||  
|- Best Animated Feature || Jeffrey Katzenberg ||  
|- Animated Theatrical Feature || ||  
|-
| rowspan=3 | Individual Achievement in Storyboarding || Ronnie Del Carmen ||  
|-
| Larry Leker ||  
|-
| Simon Wells ||  
|-
| Individual Achievement in Production Design || Luc Desmarchelier ||  
|-
| Individual Achievement in Character Design || Carlos Grangel ||  
|-
| rowspan=2 | Individual Achievement in Effects Animation || Yancy Landquist ||  
|-
| Jamie Lloyd ||  
|- Critics Choice Best Animated Feature || ||  
|-
| Genesis Awards  || Feature Film || ||  
|- Best Original Song – Motion Picture || Hans Zimmer (music) Bryan Adams (lyrics) Gretchen Peters (lyrics)   ||  
|-
| Nickelodeon Kids Choice Awards|Kids Choice Awards  || Favorite Voice from an Animated Movie || Matt Damon||  
|- Golden Reel Award  || Best Sound Editing in Animated Features || Tim Chau (supervising sound editor) Carmen Baker (supervising sound editor) Jim Brookshire (supervising dialogue editor/supervising adr editor) Nils C. Jensen (sound editor) Albert Gasser (sound editor) David Kern (sound editor) Piero Mura (sound editor) Bruce Tanis (sound editor) ||  
|-
| Best Sound Editing in Animated Features – Music || Slamm Andrews (music editor/scoring editor) Robb Boyd (music editor) ||  
|- Online Film Critics Society Awards  || Best Animated Feature || ||  
|-
| Phoenix Film Critics Society Awards || Best Animated Film || ||  
|- Golden Satellite Awards  || Best Motion Picture, Animated or Mixed Media || ||  
|- Visual Effects James Baxter ||  
|- Western Heritage Awards  || Theatrical Motion Picture || Mireille Soria (producer) Jeffrey Katzenberg (producer) Kelly Asbury (director) Lorna Cook (director) John Fusco (writer) Matt Damon (principal actor) James Cromwell (principal actor) Daniel Studi (principal actor)  ||  
|- World Soundtrack Awards  || Best Original Song Written for a Film || Hans Zimmer Bryan Adams (lyricist/performer) R.J. Lange (lyricist)   ||  
|- 
| Best Original Song Written for a Film || Hans Zimmer Bryan Adams (lyricist/performer) Gretchen Peters (lyricist)   ||  
|-
| Young Artist Awards  || Best Family Feature Film – Animation || ||  
|}

===Home media===
Spirit: Stallion of the Cimarron was released on VHS and DVD on November 19, 2002. It was re-released on DVD on May 18, 2010.  The film was released on Blu-ray in Germany on April 3, 2014, in Australia on April 4, and in the United States and Canada on May 13, 2014. 

The film was re-issued by 20th Century Fox Home Entertainment on Blu-ray and DVD on October 19, 2014.  It includes a movie ticket to Penguins of Madagascar.

==Video games==
Two video games based on the film were released on October 28, 2002 by   game Spirit: Stallion of the Cimarron — Search For Homeland. 

==See also==
 
*List of animated feature-length films
*Kiger Mustang, the horse breed of Spirit, the protagonist of the story
 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 