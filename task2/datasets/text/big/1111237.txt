Yuva
 
 
 
{{Infobox film
| name           = Yuva
| image          = Yuva (movie poster).jpg
| caption        = Theatrical release poster
| director       = Mani Ratnam Shekhar Kapoor Mani Ratnam G. Srinivasan
| screenplay     = Mani Ratnam
| narrator       =
| starring       = Ajay Devgn Rani Mukerji Abhishek Bachchan Vivek Oberoi Kareena Kapoor Esha Deol Sonu Sood
| music          = A. R. Rahman
| cinematography = Ravi K. Chandran
| editing        = A. Sreekar Prasad
| based on         = Aaytha Ezhuthu by Mani Ratnam
| distributor    = Madras Talkies
| released       = 21 May 2004
| runtime        = 162 minutes
| country        = India
| language       = Hindi
| awards         = 
| budget         =  
| gross          =  
}}
 political drama Tamil film Aaytha Ezhuthu and is based on the storyline of students entering politics. 
 hyperlink format. Yuva was declared average grosser at the Indian box-office. 

==Plot== IAS officer. He wants to relocate to the U.S. for a better future. He falls in love with Mira (Kareena Kapoor), whom he just met. One day, Arjun and Mira get into a quarrel on the road and Mira gets into an auto. Arjun gets lift from Michael to catch up with Mira, who is going that way. Suddenly, Michael is hit by a bullet and he is saved by Arjun. After talking to Michael, Arjun changes his mind and wants to be a politician. Prosenjit is worried when he hears news of students standing in the election. He uses every possible way to get them out of Politics.Firstly he provides scholarship of a prestigious foreign university to Michael. When Michael refuses the bribe, he orders his goon Gopal Singh to take control. Gopal beats some students but faces very strong retaliation from Michael and his fellow students. After that Lallan Singh takes charge and kills Gopal. He kidnaps Arjun and other candidates. However, they manage to escape with the help of Lallan Singhs ally. Lallan follows Arjun and beats him up. While running, Arjun manages to call Michael for help. He comes to rescue Arjun at Howrah Bridge.
Then Lallan is handed to the police. Michael wins the four seats he and his fellow students have contested for. Shashi leaves for her hometown while Lallan remains in prison. Michael, Arjun and two friends enter into politics.

==Cast==
* Ajay Devgn as Michael Mukherjee
* Rani Mukerji as Shashi Biswas
* Abhishek Bachchan as Lallan Singh
* Vivek Oberoi as Arjun Balachandran
* Kareena Kapoor as Mira
* Esha Deol as Radhika 
* Om Puri as Prosonjit Bannerjee
* Sonu Sood as Gopal Singh
* Karthik Kumar as Vishnu
* Saurabh Shukla
* Vijay Raaz as a Friend of Lallan Singh
* Anant Nag as Arjuns Father
* Abhinav Kashyap as Trilok
* Tanusree Chakraborty as Arjuns classmate

==Soundtrack==
{{Infobox album  
| Name        = Yuva
| Type        = Soundtrack
| Artist      = A. R. Rahman
| Cover       =
| Released    = 2004 (India)
| Recorded    = Panchathan Record Inn
| Genre       = Film soundtrack|
| Length      =
| Label       = Venus
| Producer    = A.R. Rahman
| Reviews     =
| Last album  =    (2004)
| Next album  = Aayutha Ezhuthu  (2004)
| This album  = Yuva  (2004)
|}}

The soundtrack features six songs by A. R. Rahman, with lyrics by Mehboob. The rap and lyrics for the song Dol Dol were by Blaaze.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Song !! Artist(s)!! Duration
|-
| "Dhakka Laga Bukka"
| A. R. Rahman, Karthik (singer)|Karthik, Mehboob
| 04:59
|-
| "Khuda Hafiz"
| Sunitha Sarathy, Lucky Ali, Karthik
| 05:02
|-
| "Kabhi Neem Neem"
| Madhushree, A. R. Rahman
| 04:57
|-
| "Dol Dol"
| Blaaze, featuring ethnic vocals by Shahin Badar
| 03:59
|-
| "Baadal"
| Adnan Sami, Alka Yagnik
| 05:25
|-
| "Fanaa (song)|Fanaa" Tanvi Shah
| 04:41
|-
| "Anjanaa Anjani"  (Additional song as a background score) 
| Sunitha Sarathy, Karthik
| 01:04
|}

==Reception==

===Critical reception===
Abhishek Bachchan was praised by the critics for hints of a rustic angry young man type performance in the film. 

===Box office===
Yuva grossed   at Indian box office. Yuva did well in multiplexes. But it has not done well in single screen theatres.
Compared to other parts of the country, it has fared better in Mumbai. The Mumbai distributor will recover the cost of the film, but his sub-territory distributors in places like Surat and Gujarat lost money.In places like Delhi, Uttar Pradesh, Punjab, even the south India, distributors lostaround   50 lakh (Rs 5 million) to   1 crore (  10 million). Overseas, too, the film has done average business.   It was sold to distributors at price of a   20 crore-film. Despite a good opening, the box office response to Yuva was mixed, with several distributors losing money. It is another matter that the producers, Ratnams Madras Talkies, had more than recovered their investments in this   20-crore three-language project.

==Awards==

===2004 Filmfare Awards===
 Best Supporting Actor – Abhishek Bachchan Best Supporting Actress – Rani Mukerji Critics Award Best Movie – Mani Ratnam Best Screenplay – Mani Ratnam Best Art Direction – Sabu Cyril Best Action – Vikram Dharma

==References==
 

==External links==
* 

 
 
 

 
 
 
 
 
 
 
 
 