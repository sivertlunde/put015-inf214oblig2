Breathe (2009 film)
{{Infobox film
| name           = Breathe
| image          = Breathe film.jpg
| caption        =
| director       = Nicholas Winter
| producer       = Gina Lyons
| writer         = Nicholas Winter
| starring       = Ricci Harnett Zara Dawson Lee Otway Jing Lusi
| cinematography = Nicholas Winter
| music          = Greg Harwood
| distributor    = Porcelain Film
| released       =  
| runtime        = 86 minutes
| country        = United Kingdom
| language       = English
| budget         = £20,000
}}
Breathe is a 2009 British independent film directed by Nicholas Winter and starring Ricci Harnett, Zara Dawson, Lee Otway and Jing Lusi.   The film was shot on location in London and Somerset in 2007, and debuted at the Genesis Cinema in London in 2009. Breathe was released on DVD in March 2010 and was well received at the London Independent Film Festival 2010, with Nicholas Winter taking the award for Best Director.  The film has an accompanying soundtrack, composed by Greg Harwood.  

== Plot ==
Bailey (Harnett) is buckling under the weight of financial pressure, family responsibilities and his own ambitions and desires. After endless parties, too many dead end jobs and family arguments Bailey becomes self-destructive&mdash;resorting to violence and severing all ties of friendship and love.

After a self-imposed exile, Bailey returns to London to apologise. However, things have changed, and not everyone is sympathetic to his sudden re-appearance and there are other who will stop at nothing to make Bailey’s life a living hell. Faced with a relationship lost amongst affluence and drug fuelled parties, Bailey must accept his own limitations and confront the demons of his past.

== Cast ==
* Ricci Harnett as Bailey
* Zara Dawson as Lynn
* Lee Otway as Scott
* Jing Lusi as Lauren
* Ewan David Alman as Fraser Tom Bennett as Marty James Cooper as Policeman Forbes KB as Iain

== Production ==
Breathe was director Nicholas Winters debut feature film. Winter also wrote the screenplay, shot and edited the film. As the storyline is based around Baileys life in London, the film features much of Londons picturesque skylines and scenery. Breathe was also filmed in Scotland and Somerset.
 British talent as well as up and coming actors. The film went into production late 2007, with additional photography taking place in 2008. Breathe was funded and distributed by Porcelain Film.

Breathe was nominated for Best Feature Film at the London Independent Film Festival 2010.  Nicholas Winter won the award for Best Director.  

==References==
 

== External links ==
*  
*  
*   on Cinando

 
 
 
 