Cisco Pike
 
{{Infobox film
| name        = Cisco Pike
| image       = Cisco Pike poster.jpg
| caption     = Theatrical release poster
| writer      = Bill L. Norton Viva
| director    = Bill L. Norton
| producer    = Gerald Ayres
| distributor = Sony Pictures
| released    =  
| runtime     = 95 minutes
| country     = United States
| music       =
| awards      =
| language    = English
| budget      =
}}

Cisco Pike is a 1972 drama written and directed by Bill L. Norton. It stars Kris Kristofferson as a musician fallen on hard luck who turns to dealing marijuana as a means of income. The film also stars Karen Black, Harry Dean Stanton, Antonio Fargas, Gene Hackman, Viva (Warhol Superstar)|Viva, and Texas musician Doug Sahm. This film was not widely embraced by audiences on its initial release but has become a cult movie. Much of its cult status comes from fans of Kris Kristofferson and Doug Sahm, but it also carries a cult status because of its dated (and unintentionally funny ) take on the subject of drugs, dealers, and the lifestyle they lead.

==Plot==
The film centers on Cisco Pike (played by Kris Kristofferson), an out of luck and out of work musician, and Sergeant Leo Holland (played by Gene Hackman). Cisco is a former drug dealer who has been busted several times by Sgt. Holland, and one night Holland comes to the door and demands that Cisco move a large amount of marijuana for him or suffer jail. He has several misadventures selling the marijuana, meets up with Doug Sahm at a recording studio, has his friend Jesse Dupree visit (played by Harry Dean Stanton, billed as H.D. Stanton for this film), meets a pregnant rich girl and her friend, and tries to make good with the Sergeant in the time allotted to him.

The movie was filmed in and around Hollywood, including a concert scene at the Troubador Club in West Hollywood. Numerous scenes were also filmed in Venice, CA.

==Soundtrack==
The soundtrack has most of Kristoffersons The Silver Tongued Devil and I album, and a song by Doug Sahm, along with the Sonny Terry/Brownie McGhee song "Hootin and Hollerin"

==DVD Availability==

The film is available on DVD (R1) from Sony Pictures.

== External links ==
*  

 

 

 
 
 
 
 
 


 