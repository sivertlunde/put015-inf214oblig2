The Pokrovsky Gate
{{Infobox Film name            = The Pokrovsky Gate image           = Pokrovskie_Vorota_back.png caption         = back cover of Russian-language DVD director        = Mikhail Kozakov producer        = writer          = Leonid Zorin starring        = Oleg Menshikov Inna Ulyanova Anatoly Ravikovich Leonid Bronevoy music           = Georgi Garanyan Bulat Okudzhava cinematography  = Nikolay Nemolyayev editing         = Irma Tsekavaya distributor     = Mosfilm released        = 1982 runtime         = 140 min. country         = Soviet Union language        = Russian budget          =
}} stage play by Leonid Zorin.

Featured in the film are three songs written and performed by renowned " "). 
 square on Moscows Boulevard Ring near which the films main characters reside. See   for a historical perspective.  An illustration of the historical Pokrovsky Gates in old Moscow can be seen here:  
 

==Plot summary==
The story takes place in the 1950s.  Konstantin “Kostik” Romin (Oleg Menshikov) has come to Moscow to study history and is staying with kindly aunt Alisa (Sofya Pilyavskaya), who lives in a "communal apartment" building there. His life soon becomes intertwined with those of the other residents. Among them are Margarita Pavlovna (Inna Ulyanova) and both her former husband Lev Khobotov (Anatoly Ravikovich), a publisher of foreign poetry, and her new beau, WW2 veteran and engraver-turned-teacher Savva Ignatevich (Viktor Bortsov).
 musical comedian Arkady Velyurov (Leonid Bronevoy), who is trying to revive his faltering career and escape from his own loneliness. He has become enamored of a young competitive swimmer, Svetlana (Tatyana Dogileva), who rebuffs his advances but takes a fancy to the opportunistic Kostik.
 playboy lifestyle. In the end he becomes the catalyst for both Khobotov and Velyurov to find some measure of happiness: the former elopes with Lyudochka with the help of Kostiks pal Savransky, and the latter is elated that Svetlana attends one of his concerts in response to a telegram Kostik had urged him to send.

==Cast==
 
 
 

{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| 1 Yelena Koreneva || Lyudochka
|- 
| 2 Oleg Menshikov || Konstantin “Kostik” Romin
|- 
| 3  ) || Margarita Pavlovna
|- 
| 4 Viktor Bortsov || Savva Ignatevich
|- 
| 5 Yevgeny Morgunov || Soyev (Velyurovs friend and collaborator)
|- 
| 6 Leonid Bronevoy || Arkadi Velyurov
|- 
| 7  ) || Svetlana Popova
|- 
| 8  ) || Rita
|- 
| 9  ) || Lev Yevgenevich Khobotov
|}
 <!--
    * Елена Коренева — Людочка
    * Олег Меньшиков — Константин Ромин, он же Костик
    * Инна Ульянова — Маргарита Павловна Хоботова
    * Виктор Борцов — Савва Игнатьевич
    * Игорь Дмитриев — Глеб Орлович
    * Леонид Броневой — Аркадий Велюров
    * Татьяна Догилева — Светлана Попова
    * Валентина Воилкова — Рита
    * Анатолий Равикович — Лев Евгеньевич Хоботов

    * Эммануил Геллер — дедушка Савельич (озвучивает Георгий Вицин)
    * Марина Дюжева — студентка Анна Адамовна
    * Михаил Козаков — Константин Ромин много лет спустя
    * Наталья Крачковская — Ольга Яновна — жена Соева
    * Римма Маркова — врач
    * Евгений Моргунов — поэт Соев
    * Елизавета Никищихина — Нина Орлович
    * Софья Пилявская — тётя Костика, Алиса Витальевна
    * Владимир Пицек — Леонтий — армейский друг Саввы Игнатьевича
    * Александр Пятков — пациент Людочки
-->

==Notes/References==
 

==External links==
* 
* 
*   
*   

 
 
 
 
 
 
 