In My Life (2009 film)
{{Infobox film
| name           = In My Life
| image          = In My Life (2009 film).jpg
| caption        = Theatrical movie poster
| director       = Olivia Lamasan
| producer       = Tess Fuentes Carmi Raymundo Marizel Samson Vanessa Valdez Charo Santos-Concio Malou Santos
| writer         = Raymond Lee Olivia Lamasan
| screenplay     = Senedy Que Raymond Lee Olivia Lamasan Vilma Santos-Recto John Lloyd Cruz Luis Manzano
| music          = Nonong Buencamino
| cinematography = Charlie Peralta
| editing        = Marya Ignacio
| studio         = Star Cinema
| distributor    = Star Cinema
| released       =  
| runtime        = 120 minutes
| country        = Philippines
| language       = Tagalog   English
| budget         =
| gross          = ₱135.73-M
}} Vilma Santos-Recto, highest grossing Filipino film of all time.

==Plot== Vilma Santos-Recto) is a ruthless woman.  Many people deal with her strong attitude only for fear of her cold stare, or her outlash.  She works as a librarian in a school, and lives in a compound that is owned by her ex-husband, Benito Salvacion (Tirso Cruz III).  Many of the Salvacion family members that live near the compound, even including her eldest daughter, Dang (Dimples Romana), plead with her to sell it so they can make a profit, and she can move to a more suitable living area.  Hard-headed as she is, Shirley refuses and feels betrayed by her two daughters, Dang and Cherry, for even siding with their father who left them nearly fifteen years ago.  Even more upset, she finds out that Dang will want to move out of the Philippines to Australia.  Leaving her alone in the Philippines since all her children moved out, Shirley decides to move to New York City with her youngest and only son, Mark Salvacion (Luis Manzano).  Mark is unaware, however, that this supposed vacation of his mother is actually a permanent visit.

Upon her arrival to the States, Shirley is picked up at the airport by Noel Villanueva (John Lloyd Cruz).  Thinking he was only hired help, she rudely offers him payment for his services, but he declines.  As she walks around the apartment, she notices pictures of Mark and Noel being affectionate toward each other.  She then realizes that Noel is, in fact, Marks new boyfriend.  Although she was aware of Marks homosexuality since his high school years, she gives Noel plenty of trouble and hard times.  The story reflects how Shirley changes her attitudes and views from two men that become a big part of her life, and how she accepts the reality that has been presented to her.  Unfortunately, unforeseen tragedies occur, and a rift between Shirley and Noel arise.  But as she understands Marks reasons for having her be around Noel all this time, she resolves her issues with him and soon embraces him as a member of her family.

==Cast== Vilma Santos-Recto as Shirley Templo
* John Lloyd Cruz as Noel Villanueva, Marks lover.
* Luis Manzano as Mark Salvacion, Shirleys son.
* Vice Ganda as Hillary
* Nikki Valdez as Mia
* Dimples Romana as Dang Salvacion
* Rafael Rosell as Vince
* Tirso Cruz III as Benito Salvacion
* Paw Diaz as Cherry Salvacion

==Production==
Initially, Manzano turned down the role of Mark Salvacion, and admitted he was uncomfortable with the intimate scenes between the two male characters. He accepted the role after reading the script and understanding the character. His mother and co-star Vilma Santos meanwhile made it clear that she did not influence Manzanos decision. Cruz, Marinel. (2009-09-08).  . Philippine Daily Inquirer. Retrieved 2010-1-4. 

Cruz stated that he had no problem with the gay scenes, saying that he would do it as an actor.

==Reception==
In My Life was generally well received by critics and at the box office. Many people praised the storyline, meaning, and acting of Vilma Santos, John Lloyd Cruz, and Luis Manzano.

The film garnered an unexpected ₱20,000,000 on its first day of release, and nearly ₱80,000,000 on its opening weekend. Sirkulo.com    Retrieved-2010-1-15. 
 Typhoon Ondoy.

The film had its television premiere on ABS-CBN on Christmas Day 2010.

==Awards==
*8th Gawad Tanglaw awards (March 3, 2010)
:*Best Film
:*Best Actress - Vilma Santos 
:*Best Actor - John Lloyd Cruz 
:*Best Supporting Actor - Luis Manzano 
:*Best Director - Olivia Lamasan

*Gawad Suri Awards (March 2010)
:*Best Film
:*Best Director - Olivia Lamasan
:*Best Actress - Vilma Santos
:*Best Actor - John Lloyd Cruz
:*Best Supporting Actor - Luis Manzano

*26th PMPC Star Awards for Movies (April 24, 2010)
:*Best Picture
:*Best Director - Olivia Lamasan
:*Best Actress - Vilma Santos
:*Best Actor - John Lloyd Cruz
:*Best Supporting Actor - Luis Manzano
:*Best Screenplay
:*Best Cinematography
 41st GMMSF Box-office Entertainment Awards (June 22, 2010)  . ABS-CBN News. Retrieved 2014-05-21. 
:*Best Actress - Vilma Santos
:*Best Actors - John Lloyd Cruz and Luis Manzano 
:*Best Screenplay - Raymond Lee, Senedy Que, and Olivia Lamasan

*1st MTRCB Film Awards (June 25, 2010)
:*Best Actress -Vilma Santos 
:*Best supporting actor - John Lloyd Cruz

*4th Genio Awards (January 2, 2011)
:*Best Actress - Vilma Santos
:*Best Actor - John Lloyd Cruz
:*Best Supporting Actor - Luis Manzano
:*Best Screenplay

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 