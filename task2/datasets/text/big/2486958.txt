Pink Five
 
{{Infobox film
| name        = Pink Five
| image       = PinkFivePoster.jpg
| director    = Trey Stokes
| writer      = Trey Stokes
| starring    = Amy Earhart AtomFilms
| released    =  
| runtime     = 4 minutes
| language    = English
| budget      = 
}}

Pink Five is a Star Wars fan film that made its debut on the Internet in 2002 and was written and directed by Trey Stokes {{cite news | last = Rowe  | first = Peter  | title = Pink Five alert 
  | publisher = San Diego Union-Tribune  | date = July 7, 2008  | url = http://legacy.signonsandiego.com/uniontrib/20080727/news_1a27starfilm.html  | accessdate =  }}  and stars Amy Earhart as Stacey (aka Pink Five), a fast-talking   from a very different point of view.

== Reception ==
 Official Star Wars Fan Film Awards, and even played at the 2005 Cannes Film Festival. In August 2010, Time (magazine)|Time magazine listed it as one of the Top 10 Star Wars fanfilms. 

== Sequels and other appearances ==

Two sequels have been made: Pink Five Strikes Back (2004)  and Return of Pink Five (2006). 

Stacey appears in Timothy Zahns 2007 Star Wars novel Allegiance (novel)|Allegiance, making her one of the few fan-created Star Wars characters ever to become part of the Star Wars Expanded Universe.

Stacey also has a brief cameo in the fan film Sith Apprentice, directed by John E. Hudgens.

Additionally, Stacey has now been immortalized on a Topps 30th Anniversary Trading Card.  Card #117 ("Fan Films") details the exploits of the Valley Girl X-wing Pilot and her faithful droid, R2-DD.

An original Pink 5 poster also appears in The Star Wars Vault by Steve Sansweet.

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 