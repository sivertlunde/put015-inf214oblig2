The Hole (1957 film)
 The Hole}}
{{Infobox film
| name           = The Hole 穴 Ana
| image          =
| caption        = Original Japanese poster featuring Machiko Kyō
| director       = Kon Ichikawa Hideo Nagata 
| writer         = Kon Ichikawa (writer) Natto Wada (writer)
| narrator       = 
| starring       = 
| music          = Yasushi Akutagawa
| cinematography = Setsuo Kobayashi
| editing        = Tatsuji Nakashizu
| distributor    = Daiei (1957), Kadokawa Pictures (current distributor)
| released       =   
| runtime        = 103 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| preceded_by    = 
| followed_by    = 
}} Japanese film directed by Kon Ichikawa. The genres of the film are comedy and mystery.

==Plot==

A female reporter, Nagako Kita (Machiko Kyo) is fired for writing about police corruption. To make money she hides while a weekly magazine publishes photos of her, and offers a prize to the person who discovers her. A group of three bank embezzlers, So Yamamura, Eiji Funakoshi, and Sotoji Mukui (Fujio Harumoto) employ Mukuis younger sister Fukiko as a fake employee at the bank and plan to make her disappear when the real woman appears again and blame the crime on her.

When she contacts Mukui about the crime, she finds him dead and Fukiko pulls a gun on her. She contacts the policeman who was fired, who is now a private detective.

==Staff==

The art director was Tomoo Shimogawara. 

== Cast ==
* Machiko Kyō as Nagako Kita
* Eiji Funakoshi as Koisuke Sengi
* So Yamamura as Keikichi Shirasu
* Kenji Sugawara as police Sarumaru
* Jun Hamamura as taxi driver
* Fujio Harumoto as Sotoji Mikui
* Sumiko Hidaka as Takeko Nakamura
* Shintarō Ishihara as writer
* Ryuichi Ishii as Shuta Torigai
* Yasuko Kawakami as Fukiko Mukui
* Tanie Kitabayashi as Suga Akabane
* Bontarō Miyake as chief editor Oya
* Mantarō Ushio as Sahei

==References==
 

==Critical reception==

 
)

 
 
 
 
 
 
 
 
 
 
 
 


 
 
 