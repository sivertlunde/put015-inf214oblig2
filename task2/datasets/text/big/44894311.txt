Heaven's Hell
{{Infobox film
| name               = Heavens Hell
| image              = Heavens Hell Poster.jpg
| alt                = 
| caption            = Theatrical release poster
| director           = Katung Aduwak
| producer           = Tenyin Ikpe Etim   Katung Aduwak
| writer         = Tenyin Ikpe Etim   Uyai Ikpe Etim
| starring       =  
| music          = 
| cinematography = 
| editing        = Sammie Amachree
| studio         = One O Eight Media   BGL Asset Management Ltd   Hashtag Media House
| distributor   = FilmOne Distribution
| released       =  
| runtime        = 
| country        = Nigeria
| language       = English
| budget         =
| gross          =
}}

Heavens Hell is an upcoming 2015 Nigerian psychological drama film, co-produced and directed by Katung Aduwak;  it stars an ensemble cast which includes Nse Ikpe Etim, Fabian Adeoye Lojede, Bimbo Akintola, Chet Anekwe, Damilola Adegbite, OC Ukeje, Kalu Ikeagwu, Femi Jacobs, Bimbo Manuel and Gideon Okeke.   It is majorly financed by BGL Asset Management Ltd, with the support of other partners such as Hashtag Media House, Del-York International and Aberystwyth University. 

The film, which was inspired by a true story,    is set in Lagos city and tells the story of two housewives whose bond of friendship seem unbreakable, but is filled with deceit and betrayal; in the midst of the darkness that hovers above their relationships with their spouses. The film was initially scheduled for release on 23 January 2015,    but has been delayed due to censorship.   

==Cast==
*Fabian Adeoye Lojede as Edward Henshaw
*Nse Ikpe Etim as Alice Henshaw
*Chet Anekwe as Jeff Aliu
*Bimbo Akintola as Tsola Aliu
*Damilola Adegbite as
*OC Ukeje as
*Kalu Ikeagwu as
*Gideon Okeke as
*Femi Jacobs as
*Bimbo Manuel as
*Katherine Obiang as
*Linda Ejiofor as Secretary Waje Iruobe as

==Production==
Heaven’s Hell has been flagged as a film that intends to help fight domestic violence against women and children.  Aduwak states: "...as it seems, domestic violence is still handled with kids gloves in this part of the world. Ultimately, I want Heaven’s Hell to liberate people. I want it to inspire someone to get out of a bad relationship   whatever it can accomplish to make the world a saner place".   The development of the film took a year,    after which principal photography commenced on 9 April 2013 in Lagos with the major cast.   A couple of scenes were shot in Kirikiri Maximum and Medium Security Prisons in Lagos.  Filming in Lagos lasted over three weeks,  after which shooting was moved to Wales, where some scenes were also filmed.  The film was shot using Sony F55 cameras,    and the films production was led by Jeffrey Smith.    The project was majorly financed by BGL Asset Management Ltd, with the support of other partners such as Hashtag Media House, Del-York International and Aberystwyth University.  

===Soundtrack===
The official soundtrack from the film, titled "3rd World War", was performed by Jesse Jagz and Femi Kuti, and was released on 7 August 2013. 

==Promotions and release==
Promotional images from the set of the film was released to the public during the cause of filming in April through May.     A press conference for the film was held on 8 April 2013 at Clear Essence, Ikoyi, Lagos, where it was announced that the film would be released in the third quarter of 2013.   It was however postponed due to unknown reasons. The official trailer of Heavens Hell was officially released on YouTube on 29 December 2014.  In December 2014, FilmOne Distribution officially announced that the film would be released on 23 January 2015;    it has however been delayed by the Nigerian Films and Video Censors Board due to the presence of some "explicit and inciting content". The filmmakers have been advised to re-edit the film before it can be released. 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 