One Night Only (film)
 
 
{{Infobox film
| name           = One Night Only
| image          = 
| alt            = 
| caption        = 
| director       = Jose Javier Reyes
| producer       = 
| writer         = Jose Javier Reyes
| starring       = 
| music          = Jesse Lucas
| cinematography = Rodolfo Aves Jr.
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = Philippines
| language       = Filipino Tagalog English
| budget         = 
| gross          = ₱2,700,000 (Philippine peso|PHP) $63,156.90 (US)
}}
One Night Only (Filipino language|Filipino: Isang Gabi Lang) is a 2008 comedy-romance film by OctoArts and Canary. It was starred by ABS-CBN, GMA and TV5 actors/actresses.  It was released/ premiered on December 25, 2008.

==Cast==
*Katrina Halili   ... Jasmine
*Diana Zubiri	 ...	Vivian
*Ricky Davao	 ...	Congressman Facundo
*Jayson Gainza	 ...	Barney
*Joross Gamboa   ... Nestor
*Maria Teresa Martinez	 ...	Lovely
*Chokoleit	 ...	Edward
*Bacci Garcia	 ...	Edwards Assistant
*Tessie Villarama	 ...	Inday
*Jon Avila	 ...	Pons
*Paolo Contis	 ...	Diego
*Joey David	 ...	Masahistang Larry
*Alessandra de Rossi	 ...	Angela
*Lani Tapia	 ...	Extrang Pearly
*Valerie Concepcion	 ...	Vicky
*Jennylyn Mercado	 ...	Elvie

==Awards==
{|class="wikitable"
|- Metro Manila Film Festival
|-
!Year!!Category!!Recipient(s)!!Result
|- 2008 Metro Metro Manila Best Supporting Manilyn Reynes|| 
|- Metro Manila Best Original Jose Javier Reyes|| 
|}

==References==
 

==External links==
*  

 
 
 
 
 

 