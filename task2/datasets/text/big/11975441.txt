Two-Fisted Law
{{Infobox film
| name           = Two-Fisted Law
| image          = Two-FistedLaw.jpg
| caption        = Original poster
| director       = D. Ross Lederman
| producer       = Irving Briskin
| writer         = {{plain list|
*William Colt MacDonald
*Kurt Kempler
}}
| starring       = {{plain list|
*Tim McCoy
*John Wayne
*Walter Brennan
*Alice Day
*Wheeler Oakman
*Tully Marshall
*Wallace MacDonald
}}
| music          = Mischa Bakaleinikoff
| cinematography = Benjamin H. Kline Otto Meyer
| distributor    = Columbia Pictures
| released       =  
| country        = United States
| language       = English
}} Western film directed by D. Ross Lederman and starring Tim McCoy and  John Wayne. The picture also features Walter Brennan, Alice Day, Wheeler Oakman, Tully Marshall, and Wallace MacDonald.

==Plot==
Rancher Tim Clark borrows money from Bob Russell, who then rustles Clarks cattle so he will be unable to repay the money. Thus Russell is able to cheat Clark out of his ranch. Clark becomes a prospector for silver and ultimately comes to settle accounts with Russell and crooked deputy Bendix.

==Cast==
*Tim McCoy as Tim Clark
*John Wayne as Duke 
*Walter Brennan as Deputy Sheriff Bendix  
*Alice Day as Betty Owen 
*Wheeler Oakman as Bob Russell 
*Tully Marshall as Sheriff Malcolm 
*Wallace MacDonald as Artie  Richard Alexander as Zeke Yokum 
*Merrill McCormick as Green, the Agent (uncredited) 
*Bud Osborne as Henchman Jiggs Tyler (uncredited) 
*Arthur Thalasso as Bartender Jake (uncredited)

==Trivia==
Tim McCoy later married Inga Arvad, who had been Adolf Hitlers escort at the 1936 Munich Olympic Games then had a torrid affair in Washington D.C. at the beginning of World War II with John F. Kennedy.

John Wayne later remade several Tim McCoy movies, incorporating some of the original footage.

==See also==
* John Wayne filmography

==External links==
*  
*  

 
 
 
 
 
 
 
 
 

 