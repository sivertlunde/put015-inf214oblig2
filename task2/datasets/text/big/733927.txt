Outbreak (film)
{{Infobox film
| name = Outbreak
| image = Outbreak movie.JPG
| caption = Theatrical release poster
| director = Wolfgang Petersen
| producer = Gail Katz Arnold Kopelson Anne Kopelson Wolfgang Petersen
| writer = Laurence Dworet Robert Roy Pool
| starring = Dustin Hoffman Rene Russo Morgan Freeman Cuba Gooding Jr. Patrick Dempsey Donald Sutherland Kevin Spacey
| music = James Newton Howard
| cinematography = Michael Ballhaus
| editing = Neil Travis
| studio = Punch Productions, inc.
| distributor = Warner Bros.
| released = March 10, 1995
| runtime = 127 minutes
| country = United States
| language = English
| budget = $50 million
| gross = $189,859,560
}} medical disaster Richard Prestons non-fiction book The Hot Zone.   The film stars Dustin Hoffman, Rene Russo, and Morgan Freeman and co-stars Cuba Gooding Jr., Kevin Spacey, Donald Sutherland, and Patrick Dempsey.
 USAMRIID and the Centers for Disease Control and Prevention|CDC, and the fictional town of Cedar Creek, California. Outbreaks plot speculates how far military and civilian agencies might go to contain the spread of a deadly contagion.

The film was released on March 10, 1995 and proved a box office success. It was nominated for various awards but failed to garner any major award nominations. It also raised various "what-if" scenarios: media outlets began to question what the government would really do in a similar situation and if the CDC has plans in case an outbreak ever does occur.  A real-life outbreak of the Ebola virus was occurring in Zaire during the time of the films release. 

==Plot== deadly fever, African jungle in 1967. To maintain the virus as a viable biological weapon, two U.S. Army officers, Donald McClintock (Donald Sutherland) and William Ford (Morgan Freeman), destroy the camp where it was found after taking blood samples from the dying victims.
 USAMRIID virologist, is sent to investigate. He and his crew, Lieutenant Colonel Casey Schuler (Kevin Spacey), and new recruit Major Salt (Cuba Gooding Jr.) gain information about the virus and return to the United States, where Daniels asks his superior, now-Brigadier General William Ford, to put out an alert. Ford knows that the virus is not new, but he tells Daniels it is unlikely to show up. Daniels ex-wife and former crew-member Roberta Keough (Rene Russo), has left USAMRIID to take the lead role of a similar team at the United States Center for Disease Control and Prevention in Atlanta, Georgia and is convinced by Daniels to recommend an alert from the CDC, but her superior balks as well.

Meanwhile, we learn that the virus arrived via a host animal, a white-headed capuchin monkey, that was smuggled into the United States. James "Jimbo" Scott (Patrick Dempsey), an employee at the Bio-Test animal holding facility, bribed a security guard and took the monkey to Cedar Creek, California, to sell on the black market. During the drive, Jimbo is infected with the virus through facial contact with the monkeys saliva.

Jimbo unsuccessfully tries to sell the monkey to a crooked pet store owner, Rudy Alvarez (Daniel Chodos).  Before parting ways, however, the monkey scratches Alvarez and infects him, and shares a banana with another monkey already in the store, infecting that monkey as well.  Not able to care for the monkey, Jimbo releases it into the woods. Jimbo starts to show signs of infection while flying to Boston, where he gets off the plane and kisses his girlfriend Alice (Kellie Overbey), infecting her as well just before collapsing. They are both hospitalized. Keough investigates the infections but finds that no one other than Jimbo, his girlfriend or Rudy – all three of whom die of hemorrhagic fever – in the Boston area was infected.

Meanwhile, the technicians at a Cedar Creek hospital run tests on Rudys blood. But Henry (Leland Hayward III), one of the technicians, accidentally breaks a vial, splattering the contents, infecting and killing him. The virus mutates into a new strain, capable of spreading like flu, and numerous Cedar Creek citizens are exposed to Motaba. Daniels learns of the infection and flies to Cedar Creek alongside Schuler and Salt, against Fords orders, joining Keoughs team.

As Daniels and his team begin a search for the host animal, a state of martial law is declared in Cedar Creek, and the U.S. Army has quarantined the town to contain the outbreak. During their research, Schuler is infected when his suit tears. Keough follows after she accidentally stabs herself with a contaminated needle while collecting samples due to Schuler suddenly convulsing. A mystery serum, E-1101, is introduced to those suffering from Motaba. Daniels soon realizes that the serum is not experimental, but was designed to cure Motaba, and that Ford knew about the virus beforehand. However, the serum does not help the residents of Cedar Creek, who are infected by a mutated strain. Daniels confronts Ford, who admits that he withheld information on the virus due to national security and Motabas potential to be turned into a biological weapon.

Daniels learns from Ford of Operation Clean Sweep, a plan by the military to bomb the town of Cedar Creek, with approval from the President of the United States. Now-Major General Donald McClintock was Fords partner at the African camp in 1967 and was responsible for its destruction. He now plans to use the bombing to cover up the viruss existence. To prevent Daniels from finding a cure, McClintock has him arrested by implicating Daniels as a carrier of the virus. This leads Daniels and Salt to search for the host animal to save the town. Flying a helicopter to the ship that carried the host animal, Daniels obtains a picture of Betsy and broadcasts it on the news. Mrs. Jeffries (Gina Menza) realizes that this is the animal her daughter Kate (Kara Keough) is playing with in their backyard. She calls the station, and the two men arrive at the familys house. Kate coaxes out Betsy, whom Salt tranquilizes. Learning from Daniels that the host animal is captured, Ford delays the bombing.
 antibodies with the E-1101 to create an anti-serum in time to save Keough, but not Schuler, who has already succumbed to the virus. Daniels discovers that Operation Clean Sweep is in progress and becomes aware that McClintock will not call off the bombing. He and Salt take it upon themselves to fly in the way of the bomber, commanded by a pilot with the call sign of Sandman One (Maury Sterling), to stop it. With support from Ford, Daniels is able to stay in the way of the plane long enough to convince Sandman One and his co-pilot (Michael Emanuel) that information was withheld from them. Sandman One deliberately detonates the bomb over water instead of the town. Ford, having had enough of McClintocks single-minded obsession, relieves McClintock of command and places him under arrest for withholding information from the President. Daniels and Keough reconcile, and the remaining residents of the town are successfully cured.

==Cast==
 
 
* Dustin Hoffman as Colonel Sam Daniels
* Rene Russo as Roberta "Robby" Keough
* Morgan Freeman as Brigadier General Billy Ford
* Donald Sutherland as Major General Donald "Donnie" McClintock
* Kevin Spacey as Major Casey Schuler
* Cuba Gooding Jr. as Major Salt
* Patrick Dempsey as James "Jimbo" Scott
* Zakes Mokae as Dr. Benjamin Iwabi
* Malick Bowens as Dr. Raswani
* Susan Lee Hoffman as Dr. Lisa Aronson Benito Martinez as Dr. Julio Ruiz
 
* Bruce Jarchow as Dr. Mascelli
* Leland Hayward III as Henry Seward
* Daniel Chodos as Rudy Alvarez
* Dale Dye as Lieutenant Colonel Briggs
* Kara Keough as Kate Jeffries
* Gina Menza as Mrs. Jeffries
* Maury Sterling as Sandman One
* Michael Emanuel as Sandman One Co-Pilot
* Kellie Overbey as Alice
* J. T. Walsh as the White House Chief of Staff
 

==Production==
Scenes in "Cedar Creek" were filmed in Ferndale, California where tanks and helicopters became a common feature of daily life during the nearly two months of filming.   Other locations used were Dugway Proving Ground and Kauai. 

==Release==

===Box office=== Tommy Boy s release.  The film would go on to gross a $67,659,560 domestic total, and with an international $122,200,000, totaled $189,859,560 worldwide.  Measuring box office against its $50 million budget, the film is considered a commercial success. 

===Reception===
Outbreak received mostly mixed reviews. Review aggregator Rotten Tomatoes reports that 59% of 44 film critics have given the film a positive review, with a rating average of 5.6 out of 10. 

Roger Ebert of the Chicago Sun-Times gave the film three and a half out of four stars, calling the premise "one of the great scare stories of our time, the notion that deep in the uncharted rain forests, deadly diseases are lurking, and if they ever escape their jungle homes and enter the human bloodstream, there will be a new plague the likes of which we have never seen."  Rita Kempley of the Washington Post also praised the films story, saying, "Outbreak is an absolute hoot thanks primarily to director Wolfgang Petersens rabid pacing and the great care he brings to setting up the story and its probability." 
 New York magazine that the opening scenes were well-done, but "somewhere in the middle ... Outbreak falls off a cliff" and becomes "lamely conventional".   Janet Maslin of the New York Times also found the subject matter compelling but the treatment ineffective, observing, "The films shallowness also contributes to the impression that no problem is too thorny to be solved by movie heroics." 

===Accolades===
*ASCAP Award: Top Box Office Film (Won) 
*Saturn Award: Best Science Fiction Film (Nominated) 
*  – Outstanding Supporting Actor in a Motion Picture (Nominated) 
*  – Best Supporting Actor (Won) 
*  – Best Supporting Actor (Won) – This nomination also included the films Seven (film)|Se7en and The Usual Suspects  

==References==
{{reflist|refs=
   
   
   
   
   
   
   
   
   
   
   
  }} 
   
   
   
   
}}

==External links==
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 