Black Field
{{Infobox film
| name           = Black Field
| image          = 
| caption        = 
| director       = Danishka Esterhazy
| producer       = Jeff Skinner Kent Ulrich David Antoniuk Ashley Hirt Polly Washburn
| writer         = Danishka Esterhazy
| starring       = Sara Canning Darcy Fehr Mathieu Bourguet Ferron Guerreiro
| music          = Joe Silva
| cinematography = Paul Suderman
| editing        = Joni Church
| studio         = Two Lagoons Productions Super Channel
| released       =  
| runtime        = 
| country        = Canada
| language       = English
| budget         = 
| gross          = 
}}
Black Field is a 2009 Canadian historical drama film and the debut of filmmaker Danishka Esterhazy.        It is set in the 1870s and tells the story of a love triangle between a man and two sisters Maggie (Sara Canning) and Rose McGregor (Ferron Guerreiro).

==Production== Super Channel.      

==Plot==
Black Field is an historical drama set in the 1870s that tells of a love triangle about two sisters Maggie (Sara Canning) and Rose McGregor (Ferron Guerreiro) and the man that comes between them.

==Cast==
*Sara Canning as Maggie McGregor
*Darcy Fehr as Anderson
*Mathieu Bourguet as David Latouche
*Ferron Guerreiro as Rose McGregor
*Adriana ONeil as Mrs Kravchenko
*Robert Huculak as Mr Kravchenko
*Jefferson Bruyere as Native Trapper

==Reception==
Of its filming, Aaron Graham of Uptown (magazine)|Uptown wrote "writer/director Danishka Esterhazys feature-length debut, Black Field, is shaping up to be a striking period piece".   Reel West magazine gave the cover spot and presented a featured article on Black Field.     After its premiere at the Vancouver International Film Festival, Marina Antunes of Row Three wrote "The film is notable for both its visuals and Canning’s performance but also for its score.." and summarized "Black Field is a gorgeous film which delivers a remarkable story of survival". 

== References ==
 

==External links==
*  
*  

 
 
 