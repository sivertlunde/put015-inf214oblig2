Atlantic Rhapsody
 
{{Infobox film
| name           = Atlantic Rhapsody
| image          =
| caption        =
| director       = Katrin Ottarsdóttir
| producer       =
| writer         =
| narrator       =
| starring       = Páll Danielsen Erling Eysturoy Elin K. Mouritsen Katrin Ottarsdóttir
| music          = Heðin Meitil
| cinematography =
| editing        =
| distributor    =
| released       = 1989
| runtime        = 80 minutes
| country        = Faroe Islands
| language       = Faroese
| budget         =
}}
Atlantic Rhapsody is a 1989 Faroese documentary film by Katrin Ottarsdóttir. The original Faroese title is Atlantic Rhapsody - 52 myndir úr Tórshavn, where the second part means "52 pictures from Tórshavn". The film presents a day in the life of some inhabitants of Tórshavn, the capital of the Faroe Islands. The narrative is structured as a relay race, in which a person, a thing or something out of a scene brings the audience into another scene with new people and events. It is the first ever Faroese feature-length film.

==Synopsis==
A trip through 24 hours of the smallest capital in the world, Tórshavn, the film begins with a father and a daughter who are having breakfast when some fire trucks drive by. A woman and her child are looking at the fire and meet a married couple. The couple say hello to a man who is going out with his boat etc. Out of this emerges a kaleidoscopic story of both daily occurrences and dramatic situations, which in an entertaining and ironic way acts as a commentary on the Faroese, foreigners in the islands, as well as Faroese society in general.

==Production==
Atlantic Rhapsody is the first feature film ever to be produced in the Faroes. It is a no-budget film made against all odds for 1.7 million DKR.

==Accolades==
In 1989, the film received the prize of the Nordische Filminstitute at the film festival Nordische Filmtage in Lübeck.

==External links==
*  
*  

 
 
 
 
 
 
 


 
 