Iris and the Lieutenant
 
{{Infobox film
| name           = Iris and the Lieutenant
| image          = Iris och löjtnantshjärta (film).jpg
| caption        = Film poster
| director       = Alf Sjöberg
| producer       = Harald Molander
| writer         = Olle Hedberg Alf Sjöberg
| starring       = Mai Zetterling
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 86 minutes
| country        = Sweden
| language       = Swedish
| budget         = 
}} Grand Prix at the 1946 Cannes Film Festival.    The film boosted Zetterlings international profile. Shortly afterwards she moved to Britain to make Frieda (film)|Frieda (1947).

The film is adapted from a 1934 novel by the same title, written by Olle Hedberg.

==Cast==
* Mai Zetterling as Iris Mattson
* Alf Kjellin as Robert Motander
* Åke Claesson as Oscar Motander
* Holger Löwenadler as Baltzar Motander
* Stig Järrel as Harald
* Einar Axelsson as Frans (Marys husband)
* Gull Natorp as Mrs. Asp
* Margareta Fahlén as Greta Motander
* Ingrid Borthen as Mary Motander Peter Lindgren as Svante (engineer)
* Magnus Kesster as Emil Gustell

==References==
 

==External links==
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 