5 Superfighters
 
 
{{Infobox film
| name           = 5 Superfighters
| image          = 5Superfighters1978Poster.jpg
| alt            =  
| caption        = Hong Kong Poster
| director       = Mar Lo
| producer       = Mona Fong
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Chiu-sing Hau
| music          = Eddie Wang
| cinematography = Chao Lin
| editing        = Hsing-lung Chiang Bao-Hua Fang
| studio         = 
| distributor    = 
| released       =    
| runtime        = 
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}}
5 Superfighters is a 1978 Shaw Brothers martial arts film about a kung fu expert who goes around to different schools correcting "bad kung fu".

==Plot==
Trouble arrives in town when a cloaked kung-fu expert sets about embarrassing local masters with his exceptional ability. After meeting a humble, unassuming master and his three eager students, the fighter continues his arrogant mission and leaves another few reputations in tatters. The defeated master stumbles away with his pupils, but is left shattered by his complete loss of face. It is this dejected state that persuades his three students to look for revenge against the mysterious wanderer. However, they are well aware of their limitations and decide to split up in the search for worthy masters. 

The first avenger meets an eccentric Crane style master who easily beats him during a fight. After some persuasion, the master accepts the newcomer and decides to teach him the precise Crane movements. The second meets a bean curd seller and her blind father. An initial misunderstanding is turned into a teaching opportunity when the second student is taught legwork by the high-kicking woman. Finally, the third is given a chance to learn pole-fighting from a fisherman after a failed attempt to steal his fish. This opening animosity also turns into a fruitful teacher/student relationship. Meanwhile the original shamed master is also preparing his skills and, in between drinking bouts, sharpens up his swordplay skills. With each of the four now galvanized by this period of teaching, they are now ready to meet their tormentor. 

==Cast==
*Hau Chiu Sing as Master Wan
*Tony Lung as Chang Tien
*Austin Wai as Wang Fu Chung
*Kuan Feng as Ma
*Wu Yuan Chun as Chen Liu Chi

== External links ==
*  

 
 
 
 

 