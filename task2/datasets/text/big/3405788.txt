Seemabaddha
 
 
 
{{Infobox film
| name           = Seemabaddha (Company Limited)
| image          = Dvd seemabaddha sat.jpg
| director       = Satyajit Ray
| producer       = Chitranjali (Bharat Shamsher Jung Bahadur Rana)
| writer         = Satyajit Ray, based on the novel Seemabaddha by Mani Shankar Mukherjee
| starring       = Barun Chanda Harindranath Chattopadhyay Sharmila Tagore Haradhan Bandopadhyay Parumita Chowdhury Indira Roy Promod Ganguli
| distributor    =
| music          = Satyajit Ray
| cinematography = Soumendu Roy editing = Dulal Dutta
| released       = 24 September 1971
| runtime        = 112 minutes Bengali
| country        = India
| budget         =
}}
Seemabaddha (    film directed by Satyajit Ray. It is based on the novel Seemabaddha by Mani Shankar Mukherjee. It stars Barun Chanda, Harindranath Chattopadhyay, and Sharmila Tagore in lead roles. The film was second film the Rays Calcutta trilogy, which included  Pratidwandi  (The Adversary) (1970) and Jana Aranya  (The Middleman) (1976). The films deal with rapidly modernizing city, rising corporate culture and greed, and the futility of the rat race.      

==Plot==
 
 fan manufacturing firm in Calcutta, where he is expecting a promotion shortly. He is married to Dolan and lives in a company flat. He aspires to become the company director.

His sister-in-law, Tutul (Sharmila Tagore), arrives from Patna to stay with them for a few days. She is given a tour of the life they lead and the many upscale spaces they inhabit &mdash; the restaurants, the beauty parlours, clubs and race courses. Tutul, whose father Shyamal had once been a student under, greatly admires him and his idealism. Secretly she is envious of her sisters marriage with him.

Life goes on smoothly for Shyamal until he learns that a consignment of fans meant for export is defective just before the shipment of a prestigious order. The problem is that the fans were painted with a flaw. The company is under a contract requiring the shipment be delivered on time. There is a clause permitting delay in case of civil disturbance. To escape blame Shyamal hatches a plan with the labour officer to provoke a strike at the factory. A factory watchman is badly injured, a false riot is organised and a lock-out declared. The delay caused by the strike and riot are used by the company to allow strikebreakers to make needed repairs.

For his efficient handling of the crisis, Shyamal is promoted, and there is congratulations all around. However Shyamalendu has fallen in the eyes of Tutul and himself. He is finally at the top, successful &mdash; and desolate.

==Cast==
* Sharmila Tagore as Tutul (Sudarsana)
* Barun Chanda as Shyamal (Shyamalendu) Chatterjee
* Paromita Chowdhury as Dolan (Shyamals wife)
* Harindranath Chattopadhyay as Sir Baren Roy
* Ajoy Banerjee as labour officer
* Haradhan Bandopadhyay as Talukdar
* Indira Roy as Shyamals mother
* Promod Ganguli as Shyamals father

==Awards==
*  : Satyajit Ray 

== References ==
 
==External links==
* 
* 
*  

 
 

 
 
 
 
 
 
 
 