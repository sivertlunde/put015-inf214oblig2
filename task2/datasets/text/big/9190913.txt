Possessed (1931 film)
{{Infobox film
| name           = Possessed
| image          = Possessed31.jpg
| image_size     =
| caption        =
| director       = Clarence Brown
| producer       = Clarence Brown Harry Rapf Irving Thalberg
| writer         = Edgar Selwyn
| based on       = play: The Mirage
| screenplay     = Lenore J. Coffee
| narrator       =
| starring       = Joan Crawford Clark Gable Wallace Ford Joseph Meyer
| cinematography = Oliver T. Marsh
| editing        = William LeVanway
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 76 minutes
| country        = United States
| language       = English
| budget         = $378,000   
| gross          = $1,522,000 
}}
 1920 Broadway Broadway play The Mirage by Edgar Selwyn. Possessed was the third of eight movie collaborations between Crawford and Gable.

== Plot == New Yorker who gives her champagne and writes down his address, telling her to look him up if she ever makes it to New York. Marian, now tipsy from the champagne, happily returns home. Giggling, she tells Al and her mother that she was drinking down by the railroad tracks.

Al, who was waiting for her and accuses her of being drunk, spots the piece of paper containing Wallys address in Marians hand, grabs it from her, and tears it up. He then tells Marian that her actions are inappropriate and that shes staying with him. Marian lashes out, telling Al and her mother that no one owns her and that her life belongs to herself. She grabs the torn paper shreds up from the floor and pastes them back together, then leaves for New York City. There, she looks up Wally who gives her some advice on meeting and keeping wealthy men, which Marian uses to begin a relationship with his friend Mark Whitney (Clark Gable), a divorced attorney.

She eventually becomes Marks mistress and he provides her with a complete make-over, educating her in the arts and culture of his social set. Three years pass and the two entertain with brio and style. Marian and Mark fall in love. To cover the fact of Marian being his kept woman, Mark devises a made up back story of her being "Mrs. Moreland", a wealthy divorcee living comfortably off her alimony.

Some time later, Al, now running a prosperous cement business, comes to the city hoping to land a big contract. He sees Marian and asks her to marry him, but she refuses. When Al learns that Marian is friends with Mark, Al hopes he can use Mark to help land that contract. Al has no idea of Marian and Marks true relationship. When Mark decides to run for gubernatorial office, however, friends caution him that his relationship with Marian is a serious liability. When she overhears Mark talking with some politicians, she learns that he now plans to marry her, despite the fact that their relationship would cause a scandal. To support his gubernatorial bid, she lies to Mark, telling him that she no longer loves him. She tells him that she is going to marry Al instead.

Marian decides to tell Al the truth. He rebuffs her, saying that he could never marry such a woman. He changes his mind when he realizes that in shutting her out of his life, he is also burning his bridges with Mark and that highway contract.

A political rival learns of Marians true identity and plans to leak that information at one of Marks political rallies. At that rally, Mark has the crowd generally on his side. No one is aware that Marian is in the audience. His political rivals then drop shards of paper from the auditorium ceiling, each piece of paper with the text, "Who is Mrs. Moreland?" written on it. Seeing that text on the paper, Mark has a worried look on his face, he not knowing what to do. As the crowd rumbles, Marian steps up from the audience and tells them that she is Mrs. Moreland, and that Mark has always been an honorable man, who once belonged to her, but now belongs to them. The crowd cheers as she, sobbing, leaves. Outside, Mark catches up to her and tells her that from now on they will be together no matter what. Mark legitimizes their relationship by proposing marriage.

==Cast (in credited order)==
* Joan Crawford as Marian Martin
* Clark Gable as Mark Whitney
* Wallace Ford as Al Manning
* Richard "Skeets" Gallagher as Wally Stuart Frank Conroy as Horace Travers
* Marjorie White as Vernice LaVerne
* John Miljan as John Driscoll
* Clara Blandick as Marians Mother

== Reception ==
Critic Mordaunt Hall, writing for The New York Times, liked the film and the direction of Clarence Brown.  He wrote

 Through Clarence Browns able direction, handsome settings and a fairly well-written script, "Possessed," …is a gratifying entertainment. …The familiar theme of a small-town factory girl who becomes the mistress of a wealthy New Yorker is set forth with new ideas which result in surprises, if not in a measure of suspense.  

===Box office===
According to MGM records the film earned $1,030,000 in the US and Canada and $492,000 elsewhere resulting in a profit of $611,000. 

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 