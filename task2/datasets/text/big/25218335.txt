The Petty Girl
{{Infobox film
| name           = The Petty Girl
| image          = Poster of The Petty Girl.jpg
| image_size     =
| caption        =
| director       = Henry Levin
| producer       = Nat Perrin Mary McCarthy
| based on       = Unpublished story by Mary McCarthy   
| screenwriter   = Nat Perrin
| starring       = Robert Cummings Joan Caulfield Elsa Lanchester
| music          = George Duning Werner R. Heymann William E. Snyder
| editing        = Al Clark
| studio         = Columbia Pictures
| distributor    = Columbia Pictures
| released       =  
| runtime        = 86–88 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

The Petty Girl (1950 in film|1950) is a musical romantic comedy Technicolor film starring Robert Cummings and Joan Caulfield. Cummings portrays painter George Petty who falls for Victoria Braymore (Caulfield), the youngest professor at Braymore College who eventually becomes "The Petty Girl".

==Plot== cheesecake paintings in favor of more respectable portraits.

Meanwhile, Victoria Braymore (Joan Caulfield), the youngest professor at Braymore College, attends a conference in New York to defend the school against charges that it is outdated and old-fashioned. She has led a sheltered life, raised by the older professors after the death of her parents (the founders of the college), and is only allowed to go with a Chaperone (social)|chaperone, her friend Dr. Crutcher (Elsa Lanchester).

George meets Victoria in an art museum. She resists his attempts to become better acquainted, but finally agrees to dinner, provided he finds a date for Dr. Crutcher. Desperate, he gets Beardsley to pretend to be his uncle Ben. The dinner is a disaster; Beardsley gets drunk, Dr. Crutcher thinks she is Georges date, and Victoria is distant. Finally, George decides to leave, when Victoria surprises him by accompanying him to a nightclub frequented by artists. When a drink is spilled on her dress, she goes to the powder room, where the attendant offers to iron it. However, the police raid the establishment; in the rush of escaping people, Victoria ends up getting arrested dressed only in her slip. Her picture is published on the front page of the newspaper. It should be noted that an almost identical scene occurred in the movie Together Again (1944).    

When she gets out of jail, she cuts short her trip and returns to Braymore. George follows her and gets a job as a busboy at the faculty residence. Using the newspaper photograph, he blackmails her into going out with him. Their first two "dates" end badly. Then, when he has her sneak out to pose for a painting in his room, she is seen by nosy Professor Whitman (Mary Wickes), who misinterprets the situation. Though the other professors are inclined to leniency, Victoria cheerfully quits, finally agreeing with Georges view that she is being stifled there.

Victoria goes to Georges apartment, where she meets her rival for his affections, Mrs. Dezlow. She tries to persuade George that Mrs. Dezlow is doing the same thing to him that the professors did to her, namely molding him to satisfy her wishes and expectations, but he does not agree. Victoria then sneaks into the art museum and replaces one of the paintings with the one George painted of her in a bathing suit. The resulting publicity lands her a starring role in the burlesque. Embarrassed, George gets an injunction preventing her from performing as the "Petty Girl".

Since the injunction only applies to public places, Victoria crashes the stuffy private party being given by Mrs. Dezlow to promote George. There, she, a male quartet, and twelve beautiful women (including an uncredited Tippi Hedren in her film debut), each representing a month, perform a musical number, much to the appreciation of B. J. Manton. The businessman changes his mind about Georges initial proposal. George realizes Victoria is right, and they kiss and make up.

==Cast==
* Robert Cummings as George Petty
* Joan Caulfield as Professor Victoria Braymore
* Elsa Lanchester as Dr. Crutcher
* Melville Cooper as Beardsley
* Audrey Long as Mrs. Connie Manton Dezlow
* Mary Wickes as Professor Whitman
* Frank Orth as Moody, process server
* John Ridgely as Patrolman
* Dorothy Abbott as December Petty Girl
* Joan Larkin as July Petty Girl
* Tippi Hedren as Ice Box Petty Girl
* Vivian Mason as Lovey

==Musical numbers==
For all songs, the music was composed by Harold Arlen, with lyrics by Johnny Mercer.

* "Fancy Free", performed by Joan Caulfield (dubbed by an uncredited Carol Richards)
* "Calypso Song", performed at the beginning by Movita Castaneda|Movita, segueing to Caulfield in a daydream sequence (both dubbed by Richards)
* "I Loves Ya", performed by Robert Cummings (dubbed by Hal Derwin) and Caulfield (Richards)
* "The Petty Girl", performed by Caulfield (Richards), the Petty Girls, and a male quartet

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 