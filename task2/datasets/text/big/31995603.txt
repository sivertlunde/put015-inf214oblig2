The Agony and Sweat of the Human Spirit
{{Infobox film
| name           = The Agony and Sweat of the Human Spirit
| image          = 
| alt            =  
| caption        = 
| director       = Joe Bookman D. Jesse Damazo
| producer       = 
| writer         = Joe Bookman D. Jesse Damazo
| starring       = Joe Bookman D. Jesse Damazo
| music          = D. Jesse Damazo
| cinematography = Richard Wiebe Florina Titz Laura Iancu Craig Webster
| editing        = Joe Bookman D. Jesse Damazo
| studio         = 
| distributor    = 
| released       =  
| runtime        = 15 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
The Agony and Sweat of the Human Spirit is a 2011 short film by D. Jesse Damazo and Joe Bookman. It was screened at the 2011 Cannes Film Festival in the Cinéfondation section.   The film was made as part of the directors coursework at The University of Iowa. The title is a reference to William Faulkners Nobel Prize acceptance speech. The film consists of only five shots, each lasting several minutes.

==Plot==
A quiet ukulele player and his talkative manager struggle to realize their artistic vision in a comic story of loss and friendship.

==Cast==
* Joe Bookman as The Manager
* D. Jesse Damazo as The Ukuleleist

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 


 
 