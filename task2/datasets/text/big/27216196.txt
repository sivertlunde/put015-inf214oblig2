Wesley (film)
{{Infobox film
| name           = Wesley
| image          = WESLEY-MOVIE-POSTER-640.jpg
| caption        = Promotional movie poster for the film
| director       = John Jackman
| producer       = Lovinder Gill Geoff Thompson
| writer         = John Jackman Carolyn Haywood Kevin McCarthy R.Keith Harris
| music          = Bruce Kiesling
| cinematography = Arledge Armenaki
| distributor    = Vision Video
| released       = November 14, 2009 (premiere) July 15, 2010 DVD (US)
| runtime        = 117 minutes
| language       = English
}} 2009 biopic about John Wesley and Charles Wesley, the founders of the Methodist movement. The movie is based largely on the Wesley brothers own journals, including Johns private journal which was kept in a shorthand-like code that was not translated until the 1980s by Dr. Richard Heitzenrater at Duke Divinity School.
 Aldersgate experience and the early development of the Methodist movement.

The movie was filmed in a number of authentic 18th century locations in and around Winston-Salem,NC., including St. Pauls Episcopal Church. 

Unusual for a lower-budget independent film, the movie features an original orchestral score recorded by a full orchestra.  The score, composed by Bruce Kiesling, uses snippets of Wesley hymns and portions composed to echo authentic 18th century style. Kiesling, who has composed scores for a number of other films, is currently conductor of the Tulare County Orchestra in California. 
 Comenius Foundation, the movie was directed by John Jackman.

==Cast==
*Burgess Jenkins - John Wesley
*R. Keith Harris - Charles Wesley
*June Lockhart - Susanna Wesley Kevin McCarthy - Bishop Ryder
*Michael Huie - Samuel Wesley
*Rusty Martin - Young John Wesley
*Carrie Anne Hunt - Sophy Hopkey
*Leanne Bernard - Grace Murray
*Lloyd Arneach - Tomochichi
*Hilary Russo -	Mary Musgrove
*Bill Oberst Jr. - Peter Böhler
*Erik Nelson - Mr. Delamotte
*Roger Willie -	Cusseta
*James France - Wealthy Mine Owner

==Festivals & Awards==
Wesley has been featured in numerous international film festivals, including:

* 2010 Bayou City Inspirational Film Festival 
* 2010 Phoenix International Christian Film Festival 
* 2010 CEVMA Film Festival, Milan, Italy
* 2010 International Christian Film Festival, Port Talbot, Wales
* 2010 Heart of England Film Festival, Worcester, UK
* 2010 Gideon Film Festival 
 
The film has won several awards, including:

* 2010 Platinum FEXY Award (motion graphics/CGI special FX) 
* 2010 Silver Telly Award (Religion & Spirituality) 
* 2010 Bronze Telly Award (History & Biography) http://www.tellyawards.com/winners/list/entries/?l=inspirata&event=&category=2&award=3 
* 2010 Bronze Telly Award (Lighting) 
* 2010 Bronze Telly Award (CGI/Special Effects) 
* SILVER AWARD for "Best Drama over $250,000," 2010 International Christian Visual Media Association CROWN AWARDS
* BRONZE AWARD for "Best Picture," 2010 International Christian Visual Media Association CROWN AWARDS
* 2010 First Place Feature Film Competition, Bayou City Inspirational Film Festival 

==References==
 
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 