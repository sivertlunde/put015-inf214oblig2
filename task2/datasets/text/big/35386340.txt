Inside America
{{Infobox film
| name           = Inside America
| image          = 
| caption        = 
| director       = Barbara Eder
| producer       = Constanze Schumann
| writer         = Barbara Eder
| starring       = Patty Barrera
| music          = 
| cinematography = Christian Haake
| editing        = 
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = Austria
| language       = English
| budget         = 
}}

Inside America is a 2010 Austrian drama film written and directed by Barbara Eder.  The film is Eders debut and it won the Special Jury Prize at the Max Ophüls Film Festival.   

==Cast==
* Patty Barrera as Patty
* Raul I. Juarez as Manni
* Carlos Benavides as Carlos
* Edward K. Bravo as Lalo
* Luis De Los Santos as Ricky
* Zuleyma Jaime as Zuly
* Roberto A. Perez as Fuego
* Aimee Lizette Saldivar as Aimee
* Carolyn Sanchez as Carol

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 