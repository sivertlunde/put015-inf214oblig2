Secret Nation
  thriller written Mike Jones and starring Cathy Jones.

The film tells the story of Frieda Vokey, a graduate history student working to complete her history thesis on Canadian Confederation|confederation. She returns to her home in Newfoundland to investigate evidence that Newfoundland’s 1949 entry into Canada was a conspiracy. Following the death of a politician, whose papers contain evidence, she begins to believe that the referendum results were faked, with British spies helping the cause.

This film was produced by nearly $2 million, with $1.8 million provided by First Choice and Canadian Broadcasting Corporation|CBC. The film was invited to New York City’s Museum of Modern Art, and screened as part of This Film is From Canada Series.

== Plot ==

The film opens with the death of the elderly and wealthy Leo Cryptus (Denys Ferry), the Chief Returning Officer for the 1948 confederation referenda. The film then shifts to Montreal, introducing Frieda Vokey, a graduate student of McGill University completing her history thesis on confederation titled “the decline of the sovereign Newfoundland state”. After academic advisors in Montreal tell Frieda that her thesis is a “joke” she then returns to her hometown of St. John’s, Newfoundland.
 Mary Walsh), Andy Jones) about seeing Joey Smallwood (Kevin Noble) before his death.

While paying her respects for the Cryptus family, Frieda is introduced to Michael Cryptus (Géza Kovács), son of Leo Cryptus. Frieda is then warned by Dr. Furey (Brian Hennessey) that Newfoundland is not the best place to study Newfoundland history because the people here are still very involved with Newfoundland history.

Frieda intends on meeting a local Newfoundland journalist, Daniel Maddox (Ron Hynes), about discussing her thesis paper. Upon this meeting, Dan suggests that Frieda come to his home for dinner and further discussion of her thesis. Following dinner, Frieda stays later than expected, sneaking into Dan’s office finding classified information regarding records of cheques sent to the British that were declined, and also letters sent to Dan declining his request for more information about the apparent conspiracy surrounding Newfoundland’s joining of confederation. Dan then awakens to find Frieda has left his home, but his belongings have been searched. Both Frieda and Dan are later involved in an interview over the radio, in which Frieda discusses her findings discovered in Dan’s office. Frieda continues her investigation, after the interview, meeting with Michael, believing he has the information she is looking for regarding his father and the confederation conspiracy. Michael informs Frieda he knows nothing of this and becomes worried that his father was a political monster.

Frieda’s father later decides to help bringing her to an archive containing information on confederation. Frieda questions her father about British spies who helped the apparent scam. Her father states that he has no knowledge on that situation. Frieda then meets with Mr. Joey Smallwood at the hospital, she extensively questions him but he is not willing to speak to her because of his illness.

While attending a benefit banquet, Frieda’s brother Chris steals the keys from a key holder for the public record office containing secret and potentially damaging files about confederation. Chris gives the keys to Frieda. She then leaves the banquet, sneaks in to the office and begins investigating the files. She finds cheque receipts issued to her father, Lester. Two men, along with the key holder, Doris(Mary Lewis), and Dan arrive at the building requesting to see the files because they believe the files are being tampered with. Frieda hears footsteps coming and escapes unnoticed.

While on her way home, Premier Valentine presents Frieda with more files. When she arrives home she confronts her father about the issued cheques, where he shows her the un-cashed cheques, and admits to working with the British in helping the confederation cause. The film ends with the voice of Leo Cryptus discussing how the votes were a majority for confederation, after three people including him had recently stated otherwise.

== Cast ==

The cast of secret nation consisted of 40 cast members:
 
• Frieda Vokey (Cathy Jones) - A smart, young doctoral student living in Montreal, Quebec. Frieda teaches at McGill University decides to return home to Newfoundland to work on her history thesis, "The decline of the sovereign Newfoundland State". She looks at both the political and psychological process in which Newfoundland endured during confederation in 1949.

• Oona Vokey (Mary Walsh) - The mother of Frieda and Chris Vokey. Oona is a real estate agent who is very compassionate about her family.

• Chris Vokey (Rick Mercer) - Brother of Frieda and the son of Oona and Lester Vokey. Chris is a taxi dispatcher in St. Johns and an aspiring Newfoundland artist.

• Lester Vokey (Michael Wade) - An evening telegram editor. He was also a private advisor to the premier. Lester was also an alcoholic.

• Dan Maddox (Ron Hynes) - A local Newfoundland journalist and history professor at Memorial University.

• Margie (Kay Anonsen) - A friend of Frieda and later a love interest of Chris.
 
• Michael Cryptus (Géza Kovács) - The son of Leo Cryptus.

• Leo Cryptus (Denys Ferry) - Chief returning officer for the 1948 confederation referenda. He is also a well-known Newfoundland lawyer.
 
• Doris (Mary Lewis) - The key holder for the secret archives of Newfoundland.

• Cecil Parkinson (Ken Campbell) - A British lawyer who works for the Smallwood family. His role was to keep the Smallwoods illness out of the eye of the public.
 
Remaining cast:

Brian Hennessey as Dr. Furey

John Doyle as Friedas Thesis Advisor

Michael Chiasson as Montreal Professor

Kevin Noble as Joseph R. Smallwood

Richard Cashin as Peter

J. Cashin Peter Miller as Radio Interviewer

Michael Jones as Leo Cryptus (voice)

Andy Jones as Premier Aylward

Edward Riche as Reporter Ed Riche
 
Joey Smallwood as Joey Smallwood -Signs Confederation Agreement (archive footage) (uncredited) 
Louis St. Laurent as Louis St.Laurent - Signs Confederation Agreement (archive footage) (uncredited)

== Production ==

The film was produced by Paul Pope, with executive producer Don Haig.

The film was shot in Petty Harbour and St. Johns, Newfoundland, Canada in the late Fall. Filming took just over 6 weeks. Production companies involved were Black Spot and Newfoundland Independent Filmmakers Co-Operative.

The cast and crew consisted of upwards of 80 people. They were very well taken care of. Members had comfortable facilities with regular catered breaks. The process was very well organized. Cast and crew members where easily transported to locations and all fees where a quick and easy system.

The cost of filming was nearly $2 million with $1.8 million coming from pre-sale provided by First Choice and CBC.

== Awards and Achievements ==

• At the 1992 Genie Awards, songwriter Ron Hynes won the category for Best Original Song for the song " The Final Breath" from the motion picture Secret Nation.
 
• Michael Jones took home a Special Achievement award for Secret Nation at the East Coast Music Awards in. The film featured many local artist’s such as Ron Hynes, Thomas Trio, The Red Albino and Jeff Johnston.

• Michael Jones was invited with his film, Secret Nation, to the very prestigious Museum of Modern Art in New York City. While there, he delivered speeches and sought out to find residents with Newfoundland background to invite them to the screening of the movie.

== References ==
 
 
1.Fitzgerald, John Edward. “Newfoundland Politics And Confederation Revisited: Three New    Works.” Newfoundland Studies Vol.9 No.1(1993): 109-114. Print.

2.John Perlin.  “More truth than fiction in Secret Nation?”  The Evening Telegram 18 Oct.1992:21.Print.

3.J.M. Sullivan. “Secret Nation: On Location.” The Sunday Telegram 18 Nov. 1990:22.Print.

4.“Secret Nation.” The Sunday Telegram . Nov.1990:Print.

5.“Secret Nation/delving into the past.”  The Express. Oct 28. 1992. Print

6. "Secret Nation Goes to the Big Apple." The Express. March 3. 1993. Print.

== External links ==
*  

 
 
 
 