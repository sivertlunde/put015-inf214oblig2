Not My Type
{{Infobox film
| name           = Not My Type
| image          = Not My Type.jpg
| caption        =
| director       = Lucas Belvaux
| producer       = Patrick Quinet   Patrick Sobelman 
| screenplay     = Lucas Belvaux 
| based on       =  
| starring       = Émilie Dequenne   Loïc Corbery 
| music          = Frédéric Vercheval 
| cinematography = Pierric Gantelmi d’Ille 	 
| editing        = Ludo Troch
| studio         = AGAT Films   Artémis Productions
| distributor    = Diaphana Film
| released       =  
| runtime        = 111 minutes
| country        = France Belgium
| language       = French
| budget         = 
| gross          = 
}}

Not My Type ( ) is a 2014 French-Belgian romance film directed by Lucas Belvaux and starring Émilie Dequenne and Loïc Corbery. It was based on the 2011 novel Pas son genre by Philippe Vilain.  It was screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.   

== Cast ==
* Émilie Dequenne as Jennifer 
* Loïc Corbery as Clément Le Guern
* Sandra Nkake as Cathy 
* Charlotte Talpaert as Nolwenn 
* Anne Coesens as Hélène Pasquier-Legrand  
* Daniela Bisconti as Madame Bortolin 
* Didier Sandre as Cléments father
* Martine Chevallier as Cléments mother
* Florian Thiriet as Johan Bortolin 
* Annelise Hesme as Isabelle 
* Amira Casar as Marie 
* Tom Burgeat as Dylan 
* Kamel Zidouri as Antoine

==Accolades==

{| class="wikitable plainrowheaders sortable"
|- style="background:#ccc; text-align:center;" Award / Film Festival Category
! Recipients and nominees Result
|- 2014 Cabourg Film Festival  Best Film
|Not My Type
| 
|- Best Actor
|Loïc Corbery
|  
|- Best Actress Emilie Dequenne
| 
|- 40th César Awards Best Actress
|Émilie Dequenne 
|  
|- Best Writing – Adaptation Lucas Belvaux
|  
|- Globes de Cristal Award Best Actress
|Émilie Dequenne 
|  
|- 20th Lumières Awards  Best Film
|Not My Type
| 
|- Best Director Lucas Belvaux
|  
|- Best Actress Emilie Dequenne
| 
|- 5th Magritte Awards  Best Film
| Not My Type
|  
|- Best Director
| Lucas Belvaux
|  
|- Best Screenplay
| Lucas Belvaux
|  
|-
| Best Actress
| Émilie Dequenne
|  
|-
| Best Supporting Actress
| Anne Coesens
|  
|-
| Best Sound
| Henri Morelle and Luc Thomas
|  
|-
| Best Original Score
| Frédéric Vercheval
|  
|-
| Best Editing
| Ludo Troch
|  
|}

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 
 


 
 