Avale Nanna Hendthi
 
{{Infobox film
| name = Avale Nanna Hendthi, ಅವಳೆ ನನ್ನ ಹೆಂಡ್ತಿ
| image =
| caption = VCD cover
| director = S. Umesh, K. Prabhakar
| producer = K. Prabhakar
| released = 1988
| runtime =
| language = Kannada
| music = Hamsalekha
| cinematography = Mallikaarjuna
| editing = S.Umesh
| studio = Vijaya Films Taara
}}

Avale Nanna Hendthi ( ) is a 1988 Kannada hit movie starring Kashinath (actor)|Kashinath, Bhavya and Tara (Kannada actress)|Taara. The film was directed by S. Umesh and K. Prabhakar for Vijay Films banner.

The film was declared a musical hit with the songs and lyrics composed and tuned by Hamsalekha.

This movie was remade in Hindi as Jawani Zindabad starring Aamir Khan and in Tamil as Jaadikketha Moodi in the same year starring Pandiarajan with Hamsalekha as music director.

==Cast== Kashinath
* Bhavya
* Mukhyamantri Chandru Tara
* N. S. Rao
* Sihi Kahi Chandru
* Kaminidharan
* Gora Bheema Rao

==Soundtrack==
{| class="wikitable"
|-
! SL.No
! Song
! Lyricist
! Artist
|-
| 1
| Manasidu Hakkiya Goodu
| Hamsalekha
| S P Balasubramanyam, Lata Hamsalekha
|-
| 2
| Neenu Hatthiravididdare
| Hamsalekha
| S P Balasubramanyam, Vani Jayaram
|-
| 3
| Hey Hudugi
| Hamsalekha
| S P Balasubramanyam, Vani Jayaram
|-
| 4
| Meese Hothha Gandasige 
| Hamsalekha
| S P Balasubramanyam
|-
| 5
| Poorvadalli Chandra Bandaru
| Hamsalekha
| (Chorus)
|-
|}

==External links==
*  
* 

 
 
 
 
 
 


 