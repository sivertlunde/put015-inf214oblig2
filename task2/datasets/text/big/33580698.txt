Sidewalks of New York (1931 film)
{{Infobox film
| name           = Sidewalks of New York
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Zion Myers, Jules White
| producer       = Lawrence Weingarten
| writer         = Dialogue by Robert E. Hopkins, Eric Hatch, and Willard Mack. Story by George Landy and Paul Girard Smith
| narrator       = 
| starring       = Buster Keaton Anita Page Cliff Edwards Frank Rowan Norman Phillips, Jr.
| music          = Domenico Savino
| cinematography = Leonard Smith
| editing        = Charles Hochberg
| studio         = 
| distributor    = MGM
| released       =  
| runtime        = 74 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 1931 Cinema American comedy film starring Buster Keaton.

==Plot==
Harmon (Keaton) is a wealthy landlord.  When he goes to visit one of his tenements, he gets caught in the middle of a brawl between groups of kids, one of whom, Clipper Kelly (Phillips) starts to attack Harmon.  When Harmon defends himself, he is seen by Clippers sister, Margie (Page).  Harmon falls in love at first sight and begins to woo her following his trial for attacking Clipper.  In order to demonstrate that he is okay, Harmon opens a gymnasium for the street boys, but Clipper, who has fallen in with a small-time gangster, Butch (Rowan), wants nothing to do with Harmon and turns the other boys against him.

Harmon tries to win them over by staging a wrestling match with his friend Poggle (Edwards) and a rigged boxing match with Mulvaney (Saylor).  In the meantime, Butch has gotten Clipper involved in a series of robberies with Clipper dressed as a woman.  When Butch and Clipper believe Harmon has learned of their activities, Butch orders Clipper to kill Harmon during a stage play that is being performed at the gymnasium, but Clipper gets cold feet.  Butch grabs Harmon, who is dressed in Clippers drag costume, and heads up to Harmons mansion to rob it.  Butchs gang joins them and Clipper and the other boys come to Harmons rescue.

==Cast==
* Buster Keaton as Harmon
* Anita Page as Margie
* Cliff Edwards as Poggle
* Frank Rowan as Butch
* Norman Phillips Jr. as Clipper
* Syd Saylor as Mulvaney
* Clark Marshall as Lefty

==Reception==
Keatons most commercially successful film, Sidewalks of New York grossed $885,000 with a net profit of nearly $200,000. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 


 