Princess O'Rourke
{{Infobox film
| name           = Princess ORourke
| image          = Princessorourkeposter.jpg
| image_size     = 200px
| caption        = Theatrical release poster
| director       = Norman Krasna
| producer       = Hal B. Wallis
| writer         = Norman Krasna
| narrator       =
| starring       = Olivia de Havilland Robert Cummings Charles Coburn
| music          = Friedrich Hollaender
| cinematography = Ernest Haller
| editing        = Warren Low
| distributor    = Warner Bros. Pictures
| released       =  
| runtime        = 93 or 94 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Princess ORourke is a 1943  . Erickson, Glenn.   DVD Savant, February 20, 2011. Retrieved: August 27, 2013. 

Although conceived as a vehicle for de Havilland, Princess ORourke turned out to be a troubled project that led to the de Havilland Law, that changed the status of contracts in the cinema of the United States|U.S. film industry. Filmed in 1942, the release was held up for one year due to legal issues that resulted from the production. 

==Plot==
  with sleeping bunks for passengers.]]
During World War II, Princess Maria (Olivia de Havilland) and her uncle Holman (Charles Coburn), exiles from their conquered European country, live in New York City.  Holman hopes that his niece will marry and produce a male heir as soon as possible, but she is not interested in his preferred choice, Count Peter de Chandome (Curt Bois) or the other candidates that her uncle discusses.
 Douglas DST airliner returns to New York because of bad weather, the crew cannot wake her. The pilot, Eddie ORourke (Robert Cummings), takes care of her. She wakes up the next morning in his apartment wearing his pajamas.

To explain her absence, Maria tells her uncle that she slept at the airport. She spends the day with Eddie, his friend and co-pilot, Dave Campbell (Jack Carson), and Daves wife Jean (Jane Wyman) (who had put Maria to bed). "Mary" tells them she is a war refugee and was traveling to California to work as an upstairs maid. The two young people quickly fall in love. With both Eddie and Dave about to join the United States Army Air Forces, Eddie impulsively proposes to Mary. She accepts, but sadly believes that as a princess, she cannot marry him.
 Secret Service agent assigned to protect Maria tells her uncle of the relationship. Holman is not opposed to Maria marrying a commoner, and is pleased to learn that Eddie is one of nine brothers and his father one of 11. Holman also knows that his niece marrying an American would strengthen his countrys vital relationship with the United States. To Marias surprise and joy, he permits the marriage, and Eddie is stunned to learn that his poor European refugee is actually royalty.
 State Department, Eddie becomes increasingly uncomfortable with becoming a prince consort in a morganatic marriage, financially supported by his wife, with no career other than fathering an heir. While discussing the prenuptial agreement, he refuses to surrender his American citizenship. After making an impassioned speech about how lucky he is to be an American, Eddie asks "Mary" to choose between him and her family. Maria obeys her uncle and leaves the room; a disappointed Eddie calls her a "slave". Holman locks her in the Lincoln Bedroom.
 Supreme Court Harry Davenport) to marry Eddie and Maria. Afterward, the newlyweds sneak out of the White House. On the way out, Eddie tips a butler he bumps into. Outside, Maria tells him that the "butler" was actually the President. 

==Cast==
As appearing in Princess ORourke, (main roles and screen credits identified):  Turner Classic Movies. Retrieved: August 24, 2013. 
* Olivia de Havilland as Princess Maria (aka Mary Williams) (credited as "Olivia DeHavilland")
* Robert Cummings as Eddie ORourke
* Charles Coburn as Holman (listed as "Uncle")
* Jack Carson as Dave Campbell, Eddies friend and co-pilot
* Jane Wyman as Jean Campbell, Daves wife Harry Davenport as Supreme Court Judge
* Gladys Cooper as Miss Haskell (governess and secretary)
* Minor Watson as Mr. Washburn, the State Department diplomat who tutors Eddie
* Nan Wynn as singer in Chinese restaurant 
* Curt Bois as Count Peter de Chandome Julie Bishop as stewardess
* Ray Walker as "G-Man" (Secret Service)

==Production== Warner Bros. Burbank Airport. 
   Turner Classic Movies. Retrieved: August 26, 2013. 
 Warner Bros.   Feeling that being cast in a lightweight role was limiting her future in Hollywood features, de Havilland also began to have medical problems that compounded her anxiety. During her suspension, Alexis Smith was tested as a replacement for the role of Princess Maria. The other lead role was originally to be played by Fred MacMurray who dropped out, citing prior commitments to Paramount Pictures|Paramount. Claude Rains campaigned to be in the film, but the casting of Charles Coburn solidified the main cast choices.   Turner Classic Movies. Retrieved: August 24, 2013. 

To complicate production, Cummings often was unavailable, as he was simultaneously at work on Between Us Girls at Universal Studios, forcing de Havilland  to deliver lines to a stand-in. Aged actor Coburn also frequently forgot his lines, leading to many retakes which sapped her energy further. 

As the production struggled on, tensions came to a head as de Havilland fought openly with Warner Bros.  Tired and suffering from low blood pressure, the formerly steady and hard-working actress began reporting late for work, leaving the set abruptly and going home when her frustrations became too much. She would eventually file a lawsuit against the studio in a landmark case, known as the de Havilland Law (California Labor Code Section 2855), that set a seven-year limit on studio system|studio-player contracts.  The film was completed 10 days behind schedule, and due to the legal issues, was eventually released a year after the production wrapped. Princess ORourke became the penultimate film that de Havilland completed while on contract to Warner Bros. Fristoe, Roger.   Turner Classic Movies. Retrieved: January 27, 2013.   
 Edward "Yip" Harburg and Ira Gershwin who had earlier written the  featured song, "Honorable Moon" (1941), donated the money they received from Warner Bros. to a China Relief organization.  Nan Wynn sings the song during a Chinese restaurant scene. 

===Government involvement=== Bureau of Motion Pictures (BMP) screened a copy of Princess ORourke and strenuously objected to the film. Unlike other feature productions, a script had not been pre-approved by the BMP. Nelson Poynter, the director of the liaison office in Hollywood, stated that the film was an example of studios "... recklessly using the war for background incidents in an opportunistic attempt to capitalize on the war rather than interpret it."  Poynter was particularly upset about the "ridiculous" caricatures of Red Cross workers, European nobility, the Secret Service, and even the president (described as a "busybody"). With the film already finished, however, no attempt was made to censure or restrict its release. 

==Reception==
Princess ORourke was seen as a light comedy, and generally received favorable contemporary reviews. Bosley Crowther of The New York Times was intrigued by a story that he thought could only be possible in America, and that "... it happens with such spirit and humor that youll be bound to concede it might be."  The review in Variety (magazine)|Variety was even more effusive. "Princess ORourke is a spritely, effervescing and laugh-explosive comedy-romance. Credit for general sparkle and excellence of the picture must be tossed to Norman Krasna, who handled the writing and directing responsibilities. Its Krasnas initial directing assignment." 

More recent reviews, however, have been far more critical, with Leonard Maltin, noting, "  very dated comedy starts charmingly with pilot Cummings falling in love with Princess de Havilland, bogs down in no longer timely situations, unbearably coy finale involving (supposedly) F.D.R. himself."  Film historian Thomas G. Aylesworth stated, "  supporting cast of real professionals probably saved the movie."  For Jane Wyman, the film marked a turning point in her career as she displayed her comedic talents, sparring capably with her foil, Jack Carson.  

Although largely forgotten today, and its importance as a factor in changing the status of contract players, relegated to a minor   (1953) directed and produced by William Wyler, starring Gregory Peck as a reporter and Audrey Hepburn as a royal princess out to see Rome on her own.  Biographer Daniel Bubbeo characterized Princess ORourke as a "fluffier" antecedent of Roman Holiday. 

==Awards==
Norman Krasna won the 1944 Academy Award for Best Writing (Original Screenplay) for Princess ORourke. 

==References==

Notes
 

Citations
 

Bibliography
 
* Aylesworth, Thomas G. The Best of Warner Bros. London: Bison Books, 1986. ISBN 0-86124-268-8.
* Bubbeo, Daniel. The Women of Warner Brothers: The Lives and Careers of 15 Leading Ladies. Jefferson, North Carolina: Mcfarland & Co. Inc. Publishers, 2001. ISBN 978-0-78641-137-5. 
* Freedland, Michael. The Warner Brothers. Edinburgh: Chambers, 1983. ISBN 978-0-24553-827-8.
* Harrison, P. S. Harrisons Reports and Film Reviews, 1919-1962. Hollywood, California: Hollywood Film Archive, 1997. ISBN 978-0-91361-610-9.
* Koppes, Clayton R. and Gregory D. Black. Hollywood Goes to War: How Politics, Profits and Propaganda Shaped World War II Movies. New York: The Free Press, 1987. ISBN 0-02-903550-3.
* Maltin, Leonard.  Leonard Maltins Movie Encyclopedia. New York: Dutton, 1994. ISBN 0-525-93635-1.
* Morella, Joe and Edward Z. Epstein. Jane Wyman: A Biography. New York: Delacorte Pr Books, 1985. ISBN 978-0-38529-402-7.
* Sperling, Cass Warner, Cork Millner and Jack Warner.Hollywood be Thy Name: The Warner Brothers Story. Lexington, Kentucky: University Press of Kentucky, 1998. ISBN 978-0-81310-958-9.
* Thomas, Tony. The Films of Olivia de Havilland. New York: Citadel Press, 1983. ISBN 978-0-80650-988-4.
* Wallis, Hal B. and Charles Higham. Starmaker: The Autobiography of Hal Wallis. London: MacMillan Publishers, 1980. ISBN 0-02-623170-0.
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 