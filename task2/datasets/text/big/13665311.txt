Night Train to Paris
 
 
{{Infobox Film
| name           = Night Train To Paris
| image          = Nighttraintoparis.jpg
| image_size     =
| caption        = DVD Cover Robert Douglas	 Jack Parsons Henry Cross
| narrator       = 
| starring       = Leslie Nielsen Aliza Gur Dorinda Stevens Eric Pohlmann Kenny Graham
| cinematography = Arthur Lavis Robert Winter
| distributor    = 20th Century Fox
| released       = 1964
| runtime        = 65 Min.
| country        = United Kingdom English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

Night Train to Paris is a 1964 British spy film starring Leslie Nielsen, Aliza Gur and Dorinda Stevens.

==Plot== OSS officer Hugh Latimer) also a former OSS officer who served with Holiday during the war.

Lemoine wants Holiday to go to Paris on a secret mission: to deliver a reel of tape, containing defense information, while Lemoine keeps a fake reel himself to deceive enemy agents. When Lemoine is killed and the fake tape stolen Holiday decides to go to Paris.

He poses as an assistant to photographer Louis Vernay (André Maranne), and they take three models along to further the ruse. 

==Cast==
* Leslie Nielsen : Alan Holiday
* Aliza Gur : Catherine Carrel
* Dorinda Stevens : Olive Davies
* Eric Pohlmann : Krogh
* Edina Ronay : Julie
* André Maranne : Louis Vernay
* Cyril Raymond : Insp. Fleming Stanley Morgan : Plainclothesman Hugh Latimer : Jules Lemoine Jenny White : Vernays Model
* Jack Melford : PC inspector
* Trevor Reid : Policeman on train

==Reviews==
A review in The Film Daily 1964 vol. 125 had this to say "Night Train to Paris is a neat little suspense film that will be a fine addition to any double bill. Its length probably automatically relegates it to second feature…".  Howard Thompson, in the New York Times for 3 December 1964, disparaged the film: "Night Train to Paris — theres an intriguing title. But, believe us, this thumpingly mediocre little suspense melodrama that drifted into neighborhood theaters yesterday can go back to where it came from. There have been worse plots but few more familiar..." and "...starchy dialogue is neatly matched by Robert Douglas’s flat-footed direction". "The most attractive thing about the whole picture is a nifty blonde named Dorinda Stevens. The woman can act, too, which is more than can be said for most of the others".

==Translations==
Italy: Tren Nocturo A Paris   

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 