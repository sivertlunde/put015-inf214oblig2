Fred 2: Night of the Living Fred
{{Infobox film
| name           = Fred 2: Night of the Living Fred
| alt            =  
| image	=	Fred 2- Night of the Living Fred FilmPoster.jpeg
| caption        = DVD cover of the movie
| director       = John Fortenberry
| producer       =  
| screenplay     = David A. Goodman
| story          = Lucas Cruikshank
| based on       =  
| starring       = Lucas Cruikshank Seth Morris Daniella Monet Jake Weary Siobhan Fallon Hogan Ariel Winter John Cena Carlos Knight
| music          = 
| cinematography = 
| editing        =  The Collective
| distributor    = Lionsgate
| released       =  
| runtime        = 84 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Fred 2: Night of the Living Fred is a 2011  , based on the adventures of Fred Figglehorn, a character created and played by Lucas Cruikshank for Cruikshanks YouTube channel. It is a Halloween themed sequel. Jennette McCurdy, who played Bertha in the original Fred, did not return and is replaced by Daniella Monet. John Cena returns as Mr. Figglehorn (Freds imaginary father), and Jake Weary returns to play Kevin. Supah Ninjas star Carlos Knight co-stars as Kevins friend and partner. Pixie Lott, who played Judy in the first film, did not return and therefore her character was written out of the plot; it is revealed by Fred that they broke up and she was the one to break up with him, proving they went out following the events of the previous film.

==Plot==

Fred arrives home screaming and shares his flashback with the audience. The day prior, Fred went to music class to play Fur Elise, and his hearing-impaired music teacher Mrs. Felson told him that he plays beautifully. The next day he comes in to play, expecting Mrs. Felson, but finds out that she has been replaced by the new music teacher, Mr. Devlin. He then sees the intimidating teacher throwing away a hearing aid, making Fred suspicious. Later, he is walking home from school and notices a girl named Talia following him. Believing that he is being stalked, he runs home screaming.

The next day at school, Devlin encourages students to "Join the World of Music" by taking his piano lessons for a recital coming up. That night Fred spies on Devlin and sees him burying something, which Fred suspects is the body of Mrs. Felson.

The next day at Freds house, theres a knock on the door with Kevins mother inviting them to a party for Mr. Devlin. Despite Freds rejection, both Fred and his mother attend the party, where Freds mother falls for Mr. Devlin, shocking Fred. Inside Kevins house, Fred notices Talia and, to his terror, learns that Talia is Kevins sister, causing him to run home screaming.  

The next day, Fred becomes extremely suspicious of Devlin and reaches the conclusion that he is a vampire. Mr. Devlin takes Freds mother on a date, making Fred extremely uneasy, causing him to enlist the help of Bertha to lend a hand in getting proof that Devlin is a vampire. Incognito, they spy on Devlin and Freds mother during their date, learning that Devlin requested to not have garlic fries, making both Bertha and Fred scared. Later that night, Fred attempts to spy on Devlin more, but he falls from his window. Suddenly Freds "dad" pulls him up and brings him to a wrestling arena, where they tag team against Mr. Devlin and Kevin. Freds dad beats up Mr. Devlin and Fred eventually pins Kevin. At this point, Fred wakes up, suggesting the wrestling match was all a dream.

The next day Fred is horrified to discover Mr. Devlin running a blood drive at school. Hes even more worried to discover Bertha taking personal music lessons from Devlin, terrified to realize that he is on his own in his mission to expose Devlin. Fred runs around gathering items to use as weapons against vampires, including a street sign with a cross on it
and a bucket of garlic sauce he gets from a Chinese food restaurant. He plans to take it all to the piano recital the next day, where Mr. Devlin will be performing.

Fred fills up squirt guns with the garlic sauce and arms himself with various other tools hes gathered up for the big vampire eradication. He then goes on a shooting spree at the recital, soaking everyone except Devlin as he enters, including Talia. The next day, to Freds horror, Mr. Devlin invites him into his house for dinner after the fiasco, so that they can bury the hatchet. Fred sets up a live video streaming from his cell phone so he can prove that Devlin is a vampire, and enters Devlins house. After showing off Devlins living room and digging for "bodies", Fred discovers a secret butchers room behind a wall filled with meat and bones. As Fred investigates, Devlin creeps in with a long knife and a tall head-dress, scaring Fred; Fred then drops his phone into a pot of boiling liquid, freezing on Devlin, and the frozen image of Devlin in his headdress causes everyone watching the video to think that he is a vampire. But after Devlin explains every weird hobby he has, Fred starts to relate to him, and he comes to realize Devlin is not a vampire, just an eccentric and cultural music teacher.
 depressed to answer. When Fred sees Mr. Devlin got fired and put up a "for sale" sign for his house, he feels guilty and tries to fix it all by making everyone think he himself is the vampire. Bertha and Talia both help, as Talia reveals how much she dislikes her brother. Kevin and his friends go up to Devlins house because they think he has Talia. Devlin tells them that vampires are not real, but Bertha comes out and says, "Vampires are real, but its not Devlin. Its him!" Then, Fred walks out of the garage carrying Talia, threatening to turn her into a vampire too. Bertha makes Kevin stab Fred, and he gets sprayed with fake blood. Devlin sees Fred and accepts his apology, revealing that he quit his job and sold his house of his own choice, but still sees Fred as his one true friend. In the end, as Freds mother and Devlin go out for a good-bye snack, Fred looks at the mirror in his house and sees that Mr. Devlin has no reflection, revealing that he was a vampire after all. The movie ends with Fred screaming inaudibly as a rock n roll version of Fur Elise plays.

==Cast==
*   sixteen-year-old living with his mother. Derf is a mysterious stranger who resembles the opposite of Fred.
* Seth Morris as Mr. Jake Devlin, the new music teacher and Freds new neighbor.
* Siobhan Fallon Hogan as Hilda Figglehorn, Freds rather insane mother.
* Daniella Monet as Bertha, Freds best friend who wears bizarre clothing, but cares little for what others think. bullies Fred.
* Ariel Winter as Talia, the new girl in school and Kevins sister, and Talia wants Kevin to be nice to Fred.
* Stephanie Courtney as Kevins mother.
* John Cena as Mr. Figglehorn, Freds imaginary dad who appears almost out of nowhere to offer Fred advice and support throughout the film. He lives in the familys refrigerator.
* Carlos Knight as Diesel, Kevins best friend.
* Matthew Scott Montgomery as Teen Manager.
* Talon Reid as Kevins Friend
* Haji Nasir Khushnawa as Khushnawa in cameo rule
* Jay Jay Warren as Younger Kevin
* Pixie Lott as Judy. The pretty English girl. (archive footage only and singing voice heard)

==Music== Blood used The Brood.

==Television Broadcast==
The initial American television premiere on the channel Nickelodeon drew 5.7 million viewers, down from the previous films premiere of 7.6 million.  However it ranked sixth in the most watched for overall weekly cable programming.  The second showing, the following Sunday at 11:00 AM, drew 3.7 million viewers. Enough to rank it as the fourteenth most watched weekly cable programming.  The final primetime broadcasting on October 30 drew in 3.2 million views, which ranked the program within the top 20 of cable programming for the night. 

==Sequel==
A third film was released on July 28, 2012  called  . 

==Home media==
Fred 2: Night of the Living Fred was released on DVD on February 6, 2012.

==References==
 

==External links==
*  
*  
*   at The Movie Insider

 

 
 
 
 
 
 
 
 
 
 
 
 