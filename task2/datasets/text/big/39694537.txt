Wreckers (2011 film)
{{Infobox film
| name = Wreckers 
| image = Wreckers film poster.png
| caption = Film poster
| director = D.R. Hood
| producer = Brian Eagles Olivier Lauchenauer Simon Onwurah
| writer = D.R. Hood
| starring = Benedict Cumberbatch Claire Foy Shaun Evans
| music = Andrew Lovett
| cinematography = Annemarie Lean-Vercoe
| editing = Claire Pringle
| distributor = 
| released =  
| runtime = 81 minutes
| country = United Kingdom
| language = English
| budget = 
| gross = 
}} Peter McDonald and Sinead Matthews. 

==Plot==
A married couple moves back to his childhood village to start a family but a surprise visit from the husbands brother ignites sibling rivalry and exposes the lies embedded in the couples relationship.

==Cast==
*Claire Foy as Dawn
*Benedict Cumberbatch as David
*Shaun Evans as Nick Peter McDonald as Gary
*Sinead Matthews as Sharon

==References==
 

==External links==
* 

 
 
 
 


 
 