Borderline (2008 film)
{{Infobox Film name            = Borderline image           = caption         = DVD Cover director        = Lyne Charlebois writer          = Lyne Charlebois Marie-Sissi Labrèche starring  Isabelle Blais Jean-Hugues Anglade Angèle Coutu Sylvie Drapeau Laurence Carbonneau Pierre-Luc Brillant Marie-Chantal Perron Antoine Bertrand Hubert Proulx music           = Benoît Jutras cinematography  = Steve Asselin producer        = Roger Frappier Luc Vandal editing         = Yvann Thibaudeau studio          = Max Films Productions distributor     = TVA Films released        = February 8, 2008 (Canada) runtime         = 110&nbsp;minutes country         = Canada language        = French budget          = gross           = followed_by     =
}}
Borderline is a 2008 Canadian film directed by Lyne Charlebois and co-written with Marie-Sissi Labrèche, based on her novels Borderline and La Brèche. It has won ten awards and one nomination.

== Synopsis == Isabelle Blais) has borderline personality disorder. She is a young student in literature at a university in Montreal who is lovesick. She takes refuge in alcohol and sex with anonymous strangers she picks up at a bar she frequents to forget her past; an unknown father, an institutionalized mother (Sylvie Drapeau), raised by her grandmother (Angèle Coutu), who is on the verge of death. But as she turns thirty, she meets her most painful love: herself.

==Cast== Isabelle Blais as Kiki (20 & 30 years old)
* Jean-Hugues Anglade as Tcheky
* Angèle Coutu as Mémé
* Sylvie Drapeau as Mère de Kiki / Kikis Mother
* Laurence Carbonneau as Kiki (10 years old)
* Pierre-Luc Brillant as Mikael Robin
* Marie-Chantal Perron as Caroline
* Antoine Bertrand as Eric
* Hubert Proulx as Antoine

==External links==
*  

 
 
 
 
 
 
 
 
 