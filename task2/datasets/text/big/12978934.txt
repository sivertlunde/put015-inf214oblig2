The Deep End of the Ocean (film)
{{Infobox film
| name           = The Deep End of the Ocean
| image          = DeepEndOcean.jpg
| caption        = Theatrical poster
| director       = Ulu Grosbard
| producer       = Frank Capra III
| screenplay         = Stephen Schiff
| based on       =   Jonathan Jackson Ryan Merriman
| music          = Elmer Bernstein
| cinematography = Stephen Goldblatt John Bloom
| distributor    = Columbia Pictures
| released       =  
| runtime        = 105 minutes
| country        = United States
| awards         =
| language       = English
| budget         = United States dollar|$38,000,000 USD (estimated) 
| gross          = $28,121,100
}} American motion Jonathan Jackson novel of the same name by Jacquelyn Mitchard, a bestseller that was the very first novel selected by Oprah Winfrey to be discussed on Oprahs Book Club in 1996. 

==Plot== Jonathan Jackson) and Kerry (Alexa Vega). 

After nine years, the family has seemingly accepted that Ben has gone forever, when a familiar-looking boy (Ryan Merriman) turns up at their house, introducing himself as Sam and offers to mow their lawn. Beth is convinced that Sam is actually her son, and begins an investigation that culminates in the discovery that Ben was kidnapped at the ill-fated high school reunion years ago, by a mentally unstable woman who was a high school classmate of Beths. This woman brought up Ben as her own child, until she committed suicide. The attempted re-integration of Ben back into the Cappadora family produces painful results for all involved. 

Eventually, the family decides that whats best for Ben is to return him to his adoptive father, but one night, Vincent finds him playing basketball outside. Ben reveals that he remembered something from before his abduction, playing with Vincent and Vincent finding him, causing him to feel safe. Vincent, who has carried guilt for letting go of Ben at the reunion, is forgiven by Ben who decides to return to living with his real family, but first plays a game of basketball with his brother with their parents secretly watching from their bedroom window.

==Cast==
*Michelle Pfeiffer as Beth Cappadora
*Treat Williams as Pat Cappadora
*Whoopi Goldberg as Det. Candy Bliss Jonathan Jackson as Vincent Cappadora - Age 16
*Cory Buck as Vincent Cappadora - Age 7
*Ryan Merriman as Ben Cappadora/Sam Karras - Age 12
*Alexa Vega as Kerry Cappadora - Age 9
*Michael McGrady as Jimmy Daugherty
*Brenda Strong as Ellen
*Michael McElroy as Ben Cappadora - Age 3
*Tony Musante as Grandpa Angelo
*Rose Gregorio as Grandma Rosie
*John Kapelos as George Karras
*Lucinda Jenney as Laurie
*John Roselius as Chief Bastokovich

==Reception==
The Deep End of the Ocean holds a rating of 42% on Rotten Tomatoes,  and a score of 45 on Metacritic,  indicating mixed reviews.
 Grosbard mercifully avoids melodrama. And he paces the film so simply and determinedly that its early scenes are like a string of picture postcards, each one depicting a new phase of the familys ordeal. Only when the film seeks tidy resolution for a tangled set of problems does this restraint seem overwhelmed by the complexity of the situation. But the only real false notes are musical ones, from a score by Elmer Bernstein that turns familiar and trite when the film does not." 
 Grosbard has always coaxed strong performances from his handpicked casts, but Deep Ends technical sheen places this outing at the top of his oeuvre. Stephen Goldblatts clean lensing, Elmer Bernsteins evocative score, Dan Davis crafty production design, Susie DeSantos authentic costumes and, particularly, John Blooms fluent editing serve as models for efficient storytelling, representing mainstream cinema at its best." 
 One Fine A Thousand Jonathan Jackson brings such confidence, maturity and self-possession that he seems to belong in another movie. And Whoopi Goldberg - all-purpose, you-got-a-part-Ill-play-it Whoopi - shows up as a helpful detective named Candy Bliss." 

In  s best-selling novel about parents who find their lost son nine years after his abduction, benefits from a customarily fine performance by Michelle Pfeiffer as the boys mother. Treat Williams excels as the husband, as does Whoopi Goldberg, a detective who helps the parents in their search. Director Ulu Grosbard (Georgia (1995 film)|Georgia) and screenwriter Stephen Schiff (Lolita (1997 film)|Lolita) commendably try to avoid the usual kidnapping clichés in favor of family dynamics, but the film ultimately gives in to a case of TV-movie blahs." 
 Williams as parents whose 3-year-old son vanishes, is almost unbearably wrenching... Far less effective, however, is the rest of the story, set nine years later, when the boy resurfaces... But if the film was less than satisfying as a big-screen event, its still worth renting for Pfeiffer, who valiantly portrays the devastating complexities of grief and guilt." 
 Jonathan Jackson and Ryan Merriman, the movies a floating longboat that ought to be ignited and pushed out to sea, Viking style." 

==Awards and nominations==
Ryan Merriman won a Young Artist Award for Best Performance in a Feature Film - Supporting Young Actor. 

==References==
 

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 