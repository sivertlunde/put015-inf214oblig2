Joe the Little Boom Boom
 
{{Infobox television
 | show_name = Joe the Little Boom Boom
 | image =
 | caption =
 | rating =
 | runtime = 5 minutes approx.
 | creator =
 | starring =
 | country = France ORTF (France) IBA1 )Israel)
 | first_aired = December 1960
 | last_aired = 1963
 | num_episodes = 39
 |}}

Joe the Little Boom Boom (  television series first produced between 1960 to 1963 and later remade into an animated feature film in 1973 (the English title for the film was Johnny in the Valley of the Giants).

The show and the film were created by Jean Image, one of the leading French animators of his time.

== The original series (1960 - 1963) ==
Joe is a kid who one day sees two youths trying to steal honey out of a beehive. Furious, he succeeds in stopping them from doing so. To thank him, the queen bee, Reine Fleur de Miel 145 (Queen Honey Flower 145), requests Bzz, her favorite adviser, to give him the size of an insect with help of a magic puncture in order to give him the opportunity to visit her kingdom of the bees as a sign of gratitude. Accompanied by his new friend Bzz, Joe discovers a kingdom full of surprises and dangers.

==The film (1973)==
{{Infobox film
| image =
| caption =
| director = Jean Image
| producer =
| writer = Michel Emer France Image
| starring = Roger Carel Laurence Badie Christiane Legrand Linette Lemercier
| music = Michel Emer
| cinematography =
| editing = Per Olaf Csongova
| studio = Films Jean Image
| distributor = Premiere Entertainment International
| released = April 1973
| runtime = 60 minutes
| country = France
| language = French
| budget =
| gross =
}}
In the 60-minute-long film, a group of boys encounter a spooky castle during a camping trip. The boys discover a very large man living there. The man traps the boys in a shrinking machine and reduces them to the size of flies. One boy, Joe, escapes and has an adventure in a beehive. When he gallantly saves the hive, the Queen Bee, Reine Fleur de Miel 145 (Queen Honey Flower 145) knights him. Eventually, they organize the bees and other woodland animals to attack the castle and rescue (and restore) his trapped friends.

==Alternative titles==
* בזיק ויויו (Bzik VeYoyo) (Israeli Title)
* Joë chez les Abeilles / Joë petit boum-boum (French title)
* Joe e le api / Joe e le formiche (Italian title)

==See also==
*List of animated feature films

==External links==
* 

 
 
 
 
 
 
 


 