Nandhavana Theru
{{Infobox film
| name           = Nandhavana Theru
| image          = Nandhavana Theru DVD cover.jpg
| caption        = DVD cover
| director       = R. V. Udayakumar
| producer       = T. Siva A. Selvaraj
| story          = Sujatha Udayakumar
| screenplay     = R. V. Udayakumar
| writer         = Gokula Krishna (dialogues)
| starring       =  
| music          = Ilaiyaraaja
| cinematography = R. Ganesh
| editing        = B. S. Nagaraj Amma Creations
| distributor    =
| released       =  
| country        = India
| runtime        = 145 minutes
| language       = Tamil
}}
 1995 Tamil Tamil drama Karthik and newcomer Srinidhi in lead roles. The film, produced by T. Siva and A. Selvaraj, had musical score by Ilaiyaraaja and was released on 11 May 1995. The film did not well at the box office.    

==Plot==

Seenu (  singer. When she becomes a famous singer, she builds a new house and she reveals to Seenu that shes in love with Suresh (Siva), her hardcore fan. Sureshs father releases Rajasekhar to separate them before their marriage. Seenu manages to save them and the couple elope. To keep his neighbouring (Manorama (Tamil actress)|Manorama) promise after her death, Seenu decide to carry the widow woman and her son.

==Cast==
 Karthik as Seenu (Srinivasan)
*Srinidhi as Gayatri
*Anandaraj as Rajasekhar Devan as Adhiseshan Janagaraj as Albert Vivek as Beedi
*Vadivelu as Theekuchi Manorama as Seenus neighbouring Siva as Suresh (Guest appearance)
*Madhan Bob as a music instrument seller (Guest appearance)

==Soundtrack==

{{Infobox Album |  
| Name        = Nandhavana Theru
| Type        = soundtrack
| Artist      = Ilaiyaraaja
| Cover       = 
| Released    = 1995
| Recorded    = 1995 Feature film soundtrack |
| Length      = 32:31
| Label       = 
| Producer    = Ilaiyaraaja
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Ilaiyaraaja. The soundtrack, released in 1995, features 8 tracks with lyrics written by R. V. Udayakumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s)!! Duration
|- 1 || "Adichu Pudichu" || Arunmozhi, Mano (singer)|Mano, SN. Surendar || 4:51
|- 2 || "Anniya Kaattu Annaney" || Mano (singer)|Mano, Swarnalatha || 4:53
|- 3 || "Enna Varam Vendum" || Mano (singer)|Mano, Lekha, Sindhu Devi || 5:29
|- 4 || Mano || 5:19
|- 5 || "Ramanaa Sri" || Shobhana || 2:10
|- 6 || "Unn Munnai Naanum Paada Vantha" || Arumozhi || 1:05
|- 7 || "Velli Nilave" || S. P. Balasubrahmanyam, Uma Ramanan || 5:10
|- 8 || "Viralil Suthi Meettavaa" || K. S. Chitra || 3:34
|}

==References==
 

==External links==
* 

 

 
 
 
 
 