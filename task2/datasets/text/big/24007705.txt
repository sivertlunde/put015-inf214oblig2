The Phantom Light
{{Infobox film
| name           = The Phantom Light
| image          = 
| image_size     = 
| caption        = 
| director       = Michael Powell Jerome Jackson J Jefferson Farjeon Ralph Smart Austin Melford
| narrator       =  Ian Hunter
| distributor    = Gainsborough Pictures
| released       = 5 August 1935
| runtime        = 76 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} British thriller quota quickie", Herbert Lomas. Welsh coast, in attempt to distract him. 

==Cast==
* Binnie Hale as Alice Bright  
* Gordon Harker as Sam Higgins  
* Donald Calthrop as David Owen  
* Milton Rosmer as Dr. Carey   Ian Hunter as Jim Pearce   Herbert Lomas as Claff Owen  
* Reginald Tate as Tom Evans  
* Barry ONeill as Captain Pearce  
* Mickey Brantford as Bob Peters  
* Alice ODay as Mrs. Owen  
* Fewlass Llewellyn as Griffith Owen  
* Edgar K. Bruce as Sergeant Owen  
* Louie Emery as Station Mistress

==DVD== MPI along Red Ensign (1934) and The Upturned Glass (1947).

The film has been released on Region 2 DVD by Opening in the "Les films de ma vie" series. The DVD has non-removable French subtitles for the original English soundtrack.

==Location==
The opening scenes were filmed at Tan y Bwlch station on the Festiniog Railway.  The station is actually 7.5 miles from the coast.

==References==
 

== External links ==
* 
* 
* 
*   at the  

 

 
 
 
 
 
 
 
 
 

 