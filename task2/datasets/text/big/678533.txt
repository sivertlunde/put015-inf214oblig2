Precious Images
{{Infobox film
| name           = Precious Images
| image          =
| caption        =
| director       = Chuck Workman
| producer       = Chuck Workman
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        = Chuck Workman
| distributor    = Directors Guild of America (DGA)
| released       =  
| runtime        = 8 minutes
| country        = United States
| language       = English
| budget         =
}} 1986 short American film. The Great Train Robbery (1903) to Rocky IV (1985), and range in subject from light comedies to dramas and horror films.

==Production==
Precious Images was commissioned by the Directors Guild for its 50th anniversary. MacDonald, Scott (2005) A critical cinema: interviews with independent filmmakers, University of California Press, p238-239  Workman had previously produced two documentaries, The Director and the Image (1984) and The Director and the Actor (1984), for the Guild.  Editing took two or three months to complete. 

Precious Images features half-second-long splices from approximately 470 American films. Chuck Workman described the films editing structure as "a sprint. You take a breath and you go." 
 “Of course, I had so many movies I wanted to include that the time constraint forced me to compress the film more and more. The cutting got faster and faster, but I realized that the film was still working. And I was moving things around, and it was still working. I started finding these wonderful little combinations of shots, the kind of edits that I’d been doing for years in other things, but suddenly in this film I wasn’t selling anything. It was a wonderful moment for me.”  

==Release== Live Action 1987 ceremony,    where it was featured in its entirety. In 1996, the film was reissued with new scenes from more contemporary films up to that point. "Precious Images" was shown every 15 minutes within Londons Museum of the Moving Image (opened 1988) but this very popular attraction was closed in 1999.

The film was screened out of competition at the 1986 Cannes Film Festival.   

Because of the numerous copyrights involved with each of the four hundred-plus films, Precious Images cannot be sold commercially.

==Recognition==
In 2009, Precious Images was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 