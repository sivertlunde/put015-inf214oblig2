Sweet Lavender (film)
{{Infobox film
| name           = Sweet Lavender 
| image          =
| caption        =
| director       = Cecil M. Hepworth
| producer       = 
| writer         = Arthur Wing Pinero (play)
| starring       = Henry Ainley   Chrissie White   Alma Taylor
| cinematography = 
| editing        = 
| studio         = Hepworth Pictures
| distributor    = Hepworth Pictures
| released       = October 1915
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}}
Sweet Lavender is a 1915 British silent romance film directed by Cecil M. Hepworth and starring Henry Ainley, Chrissie White and Alma Taylor.  It is based on the play Sweet Lavender by Arthur Wing Pinero.

==Cast==
* Henry Ainley as Dick Phenyl  
* Chrissie White as Lavender  
* Alma Taylor as Ruth Rolfe  
* Stewart Rome as Geoffrey Wedderburn  
* J.V. Bryant as Clement Hale  
* Violet Hopson 

==References==
 

==Bibliography==
* Palmer, Scott. British Film Actors Credits, 1895-1987. McFarland, 1988. 

==External links==
* 

 
 
 
 
 
 
 
 
 

 