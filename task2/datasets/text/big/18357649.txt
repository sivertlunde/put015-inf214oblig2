20 30 40
{{Infobox film
| name           = 20 30 40
| image          = 20.30.40.2004Poster.jpg
| image size     =
| alt            =
| caption        = Hong Kong Poster
| director       = Sylvia Chang
| producer       =
| screenplay     =Sylvia Chang
| narrator       =
| starring       = Sylvia Chang Rene Liu Angelica Lee
| music          = Kay Huang
| cinematography = Chien Hsiang
| editing        = Ching-Song Liao
| studio         =
| distributor    =
| released       =  
| runtime        = 113&nbsp;minutes
| country        = Taiwan
| language       = Mandarin
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}
 Taiwanese film directed by Sylvia Chang.  It was nominated for the Golden Bear at the 2004 Berlin International Film Festival, and Taiwans submission to the 77th Academy Awards for the Academy Award for Best Foreign Language Film, but was not accepted as a nominee.      

==Cast==
* Sylvia Chang as Lily
* Rene Liu as Xiang Xiang
* Angelica Lee as Xiao Jie
* Kate Yeung as Tong Yi
* Tony Leung Ka-fai as Shi-Jie Jerry Zhang
* Anthony Wong Chau-sang as Shi Ge
* Richie Ren as Wang, the tennis instructor
* Bolin Chen as Rock musician
* Chang Hung-Liang

==Plot==
The movie is telling the stories of three women aged 20, 30 and 40, and shows what women want in different stages of their lives.

Xiao Jie, a Malaysian girl who just turned twenty, has arrived in Taipei for the first time to make her dream of being a pop star come true.  Xie Jie makes friends with Yi Tong, a Hong Kong girl who pursues a music career as well. When they meet with reverses and realize the fact of life, they find themselves in the mire...
 
Xiang Xiang is 30 years old, and she is a flight attendant. In her conversation with her colleague, Xiang Xiang says that she is caught between two men. One is a mature doctor but already married, and the other one is a young recorder who is still ignorant.  However, she still retains the memory of her ex-boyfriend in New York. Xiang Xiang is confused and she does not know who her true love is.

Lily is a 40-year-old woman who got divorced with her husband and goes back to single life again.  Facing life as a single woman in the middle-age, Lily tries out new lifestyle and has fun for her own. In the new life that she has been away for decades, Lily gets to know how to get along with her own company, and what a middle-aged woman really needs before love and success. 

==Music==
The theme song of the movie which is called "20 30 40" as well was sung by Sylvia Chang, Rene Liu and Angelica Lee.   The song expresses how women want and need in modern society, telling the audience that no matter what stage of life you are arriving, the way you choose to live is the most important. Happiness and beauty are not restricted by age.

==Commercial releases==
The film was originally released in 2004 in Germany, and then were released on screens in Taiwan, Hong Kong and Korea.
The film was  released on DVD in 2005 in Argentina by Columbia TriStar Films de Argentina, Italy and Hungary. 

==See also==
 
* Cinema of Taiwan
* List of submissions to the 77th Academy Awards for Best Foreign Language Film

==References==
 

==External links==
*  

 

 
 
 
 
 