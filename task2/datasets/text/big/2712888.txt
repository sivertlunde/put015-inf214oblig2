Biwi No.1
 
 
{{Infobox film
| name           = Biwi No.1
| image          = Biwin1.jpg
| image size     =
| caption        = DVD cover
| director       = David Dhawan
| producer       = Vashu Bhagnani
| story          = Kamal Hassan Crazy Mohan
| narrator       = Tabu
| music          = Anu Malik
| cinematography =
| editing        =
| distributor    =
| released       = 28 May 1999
| runtime        = 159 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          =    . Box Office India. 
}} 1999 Bollywood Sathi Leelavathi The plot has inspired the 2005 comedy hit Kannada movie Rama Shama Bhama starring Kamal Hassan,Ramesh Aravind,Urvashi and Daisy Bopanna. 

In 1999 Salman Khan appeared in the three biggest blockbusters and highest grossing movies of the year - Biwi No.1, Hum Dil De Chuke Sanam and Hum Saath Saath Hain.

==Plot==
Prem (Salman Khan) is a man who is married to Pooja (Karishma Kapoor) with two kids. He gets involved in a roaring affair with an aspiring model, Rupali (Sushmita Sen). He prefers her to his wife because Rupali is modern and wears fashionable clothes. On Karva Chauth, Pooja discovers him at Rupalis house and asks him to choose between his mistress and her. He moves in with Rupali.

Pooja, meanwhile, with the help of Prems friend Lakhan (Anil Kapoor) has a make-over. She becomes a modern woman who does many modeling assignments. Pooja sends her mother-in-law and children to live with her husband and Rupali. They trouble Rupali and convince Prem that Rupali keeps them hungry and wants to poison and kill them.

In the end, Prem realises that Rupali only came to him since he gave her material things while his wife stuck with him through thick and thin. Rupali realises her mistake and gets back together with her ex-boyfriend, Deepak (Saif Ali Khan). In a twist, Lakhan makes Rupali his sister and is hugging her when his wife Lovely (Tabu (actress)|Tabu) walks upon the scene and misunderstands them. She leaves the house crying and drives away in a car, which their son has tampered with and has disabled the brakes. Lakhan chases after and makes up with her. The show ends with Lakhan saying she is his "Biwi no. 1."

==Cast==
*Anil Kapoor as Lakhan
*Salman Khan as Prem
*Karisma Kapoor as Pooja
*Sushmita Sen as Rupali Tabu as Lovely
*Himani Shivpuri as Prems Mom
*Saif Ali Khan as Deepak (Guest appearance)
*Amitabh Bachchan (Guest appearance)

==Reception== List of Top 50 Bollywood movies adjusted for inflation.

==Awards==
*Filmfare Award for Best Supporting Actress – Sushmita Sen
*Screen Award for Best Supporting Actor – Anil Kapoor
*Screen Award for Best Supporting Actress – Sushmita Sen

===Nominations===
*Filmfare Award for Best Film – David Dhawan
*Filmfare Award for Best Director – David Dhawan
*Filmfare Award for Best Actress – Karishma Kapoor
*Filmfare Award for Best Performance in a Comic Role – Anil Kapoor
*Filmfare Award for Best Performance in a Comic Role – Salman Khan Filmfare Award for Best Music – Anu Malik

==Music==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#3366bb; text-align:center;"
! # !! Song !! Singer(s) !! Picturised on !! Length
|-
| 1
| "Chunari Chunari" Abhijeet & Anuradha Sriram
| Salman Khan & Sushmita Sen
| 05:27
|-
| 2
| "Biwi No 1" Poornima
| Salman Khan & Karishma Kapoor
| 07:19
|-
| 3
| "Jungle Hai Aadhi Raat"
| Kumar Sanu & Hema Sardesai
| Salman Khan & Karishma Kapoor
| 06:12
|-
| 4
|"Ishq Sona Hai" Shankar Mahadevan & Hema Sardesai
| Salman Khan & Sushmita Sen
| 06:15
|-
| 5
| "Hai Hai Mirchi"
| Sukhwinder Singh & Alka Yagnik
| Anil Kapoor & Karishma Kapoor
| 05:36
|-
| 6
| "Mehboob Mere"
| Sukhwinder Singh & Alka Yagnik Tabu
| 05:03
|-
| 7
| "Mujhe Maaf Karna"
| Abhijeet, Anmol, Aditya Narayan & Alka Yagnik
| Salman Khan, Karishma Kapoor, & Children
| 05:13
|-
| 8
| "Aan Milo"
| Udit Narayan, Kavita Krishnamurthy
| Excluded from film
| 07:47
|}

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 