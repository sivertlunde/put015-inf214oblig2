Dheerasameere Yamuna Theere
{{Infobox film 
| name           = Dheerasameere Yamuna Theere
| image          =
| caption        = Madhu
| producer       = M Mani
| writer         = Cheri Viswanath
| screenplay     = Cheri Viswanath Madhu Kaviyoor Ponnamma Thikkurissi Sukumaran Nair Unnimary Shyam
| cinematography = U Rajagopal
| editing        = G Venkittaraman
| studio         = Sunitha Productions
| distributor    = Sunitha Productions
| released       =  
| country        = India Malayalam
}}
 1977 Cinema Indian Malayalam Malayalam film, Madhu and produced by M Mani. The film stars Madhu (actor)|Madhu, Kaviyoor Ponnamma, Thikkurissi Sukumaran Nair and Unnimary in lead roles. The film had musical score by Shyam (composer)|Shyam.   

==Cast== Madhu
*Kaviyoor Ponnamma
*Thikkurissi Sukumaran Nair
*Unnimary
*T. P. Madhavan
*Vidhubala

==Soundtrack== Shyam and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aanandam Brahmaanandam || P Jayachandran, LR Eeswari, Chorus, Pattom Sadan || ONV Kurup || 
|-
| 2 || Ambili Ponnambili || P Jayachandran || ONV Kurup || 
|-
| 3 || Dheerasameere Yamunaatheere || K. J. Yesudas, S Janaki || ONV Kurup || 
|-
| 4 || Manassinte Thaalukalkkidayil || S Janaki || ONV Kurup || 
|-
| 5 || Njaattuvelakkili || P Susheela || ONV Kurup || 
|-
| 6 || Puthilanji Chillakalil || P Susheela || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 