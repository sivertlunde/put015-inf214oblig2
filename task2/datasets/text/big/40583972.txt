Kara Murat Şeyh Gaffar'a Karşı
{{Infobox film
| name           = Kara Murat Şeyh Gaffara Karşı
| image          = 
| image_size     =
| caption        =
| director       = Natuk Baytan
| producer       = Türker İnanoğlu Željko Kunkera
| writer         = Fuat Özlüer Erdoğan Tünaş
| based on       =  
| narrator       =
| starring       = Cüneyt Arkın
| music          = Piero Piccioni
| cinematography = Hans Jura		
| editing        = 
| distributor    = 
| released       = 1 February 1977 (Turkey) 27 January 1978 (Italy) 	
| runtime        = 85 minutes
| country        = Turkey Italy Italian
| budget         =
}}
Kara Murat Şeyh Gaffara Karşı ( -action film directed by Natuk Baytan.

==Background==
Kara Murat Şeyh Gaffara Karşı is the fifth film in the series based on the comic strip character Kara Murat, a Janissary spy in the service of Mehmed the Conqueror, created by journalist and cartoonist Rahmi Turan. Turkish action film star Cüneyt Arkın played Kara Murat in all films of the series.

==Plot==
By the year 1456, Mehmed the Conqueror (Bora Ayanoğlu) has decided to extend the borders of his empire to the east, at the lands of the Aq Qoyunlu. However, Sheikh Gaffar (Pasquale Basile), ruler of a heretic sect near Kharput defies the Sultans rule and imprisons the ambassadors sent by him. Sultan Mehmed commissions Kara Murat to assassinate Gaffar.

==Cast==
*Cüneyt Arkın: Kara Murat (Karamurat)
*Pasquale Basile: Gaffar (Mustafà)
*Bora Ayanoğlu: Mehmed the Conqueror (Mammaluch)
*Daniela Giordano: Zeynep (Suleima)
*Turgut Özatay: Eşkiya Abdo (il bandito Alì Babà)

==External links==
* 
* 
* 

 
 
 
 
 
 
 


 