I Can't Think Straight
 
{{Infobox film 
| name = I Cant Think Straight
| image =I Cant Think Straight Poster.jpg
| caption = Theatrical release poster
| director = Shamim Sarif
| producer = Hanan Kattan
| writer = Shamim Sarif Kelly Moss
| starring = Lisa Ray Sheetal Sheth
| music = Raiomond Mirza
| cinematography = Aseem Bajaj	
| editing = David Martin
| distributor = Enlightenment Productions Regent Releasing
| released =  
| runtime = 80 minutes
| country = United States United Kingdom India
| language = English
}}

I Cant Think Straight is a 2008 romance film adapted from a same name novel about a London-based Jordanian of Palestinian descent, Tala, who is preparing for an elaborate wedding. A turn of events causes her to have an affair and subsequently fall in love with another woman, Leyla, a British Indian. The movie is distributed by Enlightenment Productions. It was released in different theatres between 2008 and 2009.  The DVD was released on 4 May 2009.  The movie is directed by Shamim Sarif and stars Lisa Ray and Sheetal Sheth.

The two actresses star in another movie with lesbian characters, The World Unseen, released in 2008.

==Plot==

In the upper echelons of traditional Middle Eastern society, wealthy Christian Palestinians Reema and Omar prepare for the marriage of their visiting daughter Tala to Hani in Jordan. But back at work in London, Tala encounters Leyla, a young British Indian Muslim woman who is dating Talas best friend Ali. Tala sees something unique in the artless, clumsy, sensitive Leyla who secretly works to become a writer. And Talas forthright challenges to Leylas beliefs begins a journey of self-awareness for Leyla. After a weekend getaway into the countryside, Tala and Leyla sleep together and the two women begin to fall in love. However, Talas own sense of duty and cultural restraint cause her to pull away from Leyla and fly back to Jordan where the preparations for an ostentatious wedding are well under way. 

As family members descend and the wedding day approaches, the pressure mounts until Tala finally cracks and extricates herself. Back in London, Leyla is heartbroken but learns to break free of her own self-doubt and her mothers expectations, ditching Ali and being honest with her parents about her sexuality. When Ali and Leylas feisty sister, Yasmin, help try to get Tala and Leyla together again, Tala finds that her own preconceptions of what love can be is the final hurdle she must jump to win Leyla back.

==Cast==
* Lisa Ray as Tala
* Sheetal Sheth as Leyla
* Antonia Frering as Reema
* Dalip Tahil as Omar
* Nina Wadia as Housekeeper
* Ernest Ignatius as Sam
* Kimberly Jaraj as Zina
* Sam Vincenti as Kareem
* Anya Lahiri as Lamia
* Rez Kempton as Ali Daud Shah as Hani
* Ishwar Maharaj as Sami
* Amber Rose Revah as Yasmin

== Awards ==
* Best Feature, Audience Award - Miami Gay & Lesbian Film Festival 2009
* Best Feature - International Gay and Lesbian Film Festival Of Canary Islands, 2009
* Best Feature - Afterellen Visibility Awards
* Best Feature, Audience Award - Melbourne Queer Film Festival 2009
* Best Feature, Audience Award - Pink Apple 2009
* Audience Award Best Feature Film - Fairy Tales International Queer Diversity Film Festival (Calgary) 2009
* Jury Winner Best Feature Film - Festival Del Mar, Majorca 2009
* Audience Award, Best Feature - Vancouver Queer Film Festival 2009
* Best lesbian movie - The Holebifilmfestival Vlaams-Brabant 2009, Belgium
* Jury award for Best Womens Feature - Tampa International Gay & Lesbian Film Festival 2009
* Best Feature Film - Gay Film Nights International Film Festival 2009

== International releases ==
* New York, Los Angeles, Toronto, November 21, 2008
* San Francisco, November 28, 2008
* Portland, December 12, 2008
* Germany, Switzerland, Austria Lichtenstein and Süd Tirol, April 16, 2009
* United Kingdom, April 3, 2009
* United Kingdom (DVD), April 10
* India, September 11, 2009
* The Netherlands (DVD), October 13, 2009

==References==
 

==External links==
*  
*  

 
   
 
 
 
 
 
 
 
 