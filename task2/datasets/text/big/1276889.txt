Room Service (1938 film)
{{Infobox film
| name           = Room Service
| image          = File:Room Service lobby card.jpg
| caption        =Lobby card James Anderson (assistant)
| producer       = Pandro S. Berman
| writer         = Glenn Tryon Philip Loeb
| based on       =  
| screenplay     = Morrie Ryskind
| starring       = Groucho Marx Chico Marx Harpo Marx Lucille Ball  Ann Miller Frank Albertson
| music          = Roy Webb
| cinematography = J. Roy Hunt
| editing        = George Crone
| studio         = RKO Radio Pictures
| distributor    = RKO Radio Pictures
| released       =  
| runtime        = 78 min.
| language       = English
| budget         = $884,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p57 
| gross          = $875,000 
}}
 same name John Murray. It also features Lucille Ball, Ann Miller, Alexander Asro, and Frank Albertson.

==Plot outline==
This is the only Marx Bros. film that was not written especially for the team. Both Chico and Harpo did not play the piano or the harp in the film at all. Less frenetic and more physically contained than their other movies, the plot revolves around the shenanigans of getting a stage play, Hail and Farewell, produced and funded by mysterious backer Zachary Fisk, while evading paying the hotel bill.
 Groucho plays producer Gordon Miller, whose staff includes Harry Binelli (Chico Marx|Chico) and Faker Englund (Harpo Marx|Harpo). They have assembled the cast and crew of the play in the hotel ballroom, as well as a substantial debt to the hotel. Miller is planning on skipping out on the hotel without paying the bill when he receives word that one of his actresses, Christine Marlowe (Lucille Ball), has arranged for a backer. Miller must keep his room and hide the cast and crew until the meeting with the backer can take place.

At the same time, a troubleshooter for the hotel chain, Gregory Wagner (Donald MacBride) discovers the debt. Assured by hotel manager Joe Gribble (Cliff Dunstan), who happens to be Millers brother-in-law, that Miller had skipped, Wagner is surprised to find Miller still in his room, now joined by the plays author, Leo Davis (Frank Albertson), who has arrived in town and checked into Millers room.

When Wagner threatens to evict Miller before the backer can arrive, Miller and Binelli convince Davis to pretend to be sick. To obtain food, Miller promises waiter Sasha Smirnoff (Alexander Asro) a part in the play. When Davis leaves to spoon with girlfriend Hilda Manney (Ann Miller), Englund takes over as the sick patient examined by a doctor brought in by Mr. Wagner. Wagner leaves to confront the crowd in the ballroom, while the doctor examines the patient. To delay the doctor giving his report to Wagner, Binelli and Miller tie him up, gag him, and lock  him in the bathroom. The agent for Mr. Fisk arrives to sign over the cheque, the doctor breaks free in the bathroom, and the agent is hit on the head accidentally as Englund chases a flying turkey around with a baseball bat. The agent just wants to escape the madness, but reluctantly signs over the cheque, and leaves.

Davis returns and says he heard the agent saying hell cancel the cheque, and just signed it to get out of the room. Wagner is fooled into believing all is okay, and upgrades the boys to a fancier room. Later, as the play is about to open, the cheque from Fisk bounces, Miller, Binelli, and Englund manipulate Wagner into believing hes driven the plays author to take poison. They pretend to give Davis large quantities of Ipecac which is actually drank by Englund, who eventually Davis pretends to die. Wagner is bluffed into believing its all his fault and helps take the "body" down to the alley. As Miller and Wagner prop Englund on a crate, a passing policeman asks whats going on. Miller bluffs their way out of the situation, so he and Wagner make an escape, leaving Englund "asleep". They go to watch the end of the play, which is a scene where the miners are bringing a body from out of the mine. The body on the stretcher is Englunds. Wagner realizes hes been duped as the play is greeted with thunderous applause and a revived Davis appears next to Wagner at the back of the theatre.

==Main cast==
{| class="wikitable" width="50%" style="text-align: center"
|- Character
!colwidth="50%"|Actor
|-
|- Leo Davis Frank Albertson
|- Sasha Smirnoff Alexander Asro*
|- Timothy Hogarth  Philip Loeb
|- Joseph Gribble Clifford Dunstan*
|- Hilda Manney Ann Miller
|- Faker Englund Harpo Marx
|- Gordon Miller Groucho Marx
|- Harry Binelli Chico Marx
|- Gregory Wagner Donald MacBride*
|- Christine Marlowe Lucille Ball
|-
|Dr. Glass Charles Halton
|- House Detective Max Wagner
|- Simon Jenkins Philip Wood*
|}
 * Indicates the actor created the role on Broadway.

==Production== Duck Soup and was now representing his brothers, brokered a deal with RKO to produce the version of the Broadway play Room Service by John Murray and Allan Boretz. The play was adapted for the screen by Morrie Ryskind. This was the only Marx Brothers film in which the main characters were not created especially for Groucho, Chico and Harpo. 

This was the second Marx Brothers film in which neither Chico played the piano, or Harpo played the harp. disappointing many fans of the Marx Brothers.

Ann Miller was only 15 years old when she made this film. She had lied about her age and obtained a fake birth certificate when she was about 14 years old, which stated that she was 18, just prior to signing with RKO. She had been discovered by her co-star in Room Service, Lucille Ball, who later bought the RKO Pictures production facilities (where Room Service was filmed) with her husband Desi Arnaz and renamed it Desilu Productions|DesiLu. 

==Reception==
The film recorded a loss of $330,000. 

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 