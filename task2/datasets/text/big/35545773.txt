The City of Masks
{{infobox film
| name           = The City of Masks
| image          = Thecityofmasks1920-newspaperad.jpg
| imagesize      = 
| caption        = A newspaper advertisement.
| director       = Thomas N. Heffron
| producer       = Adolph Zukor Jesse Lasky Walter Woods(scenario)
| based on       =   Lois Wilson Theodore Kosloff
| music          = Karl Brown
| editing        =
| distributor    = Paramount Pictures
| released       =   reels (4,708 feet)
| country        = United States
| language       = Silent film (English intertitles)
}} lost  1920 silent film comedy-drama produced by Famous Players-Lasky and distributed by Paramount Pictures. The film was directed by Thomas Heffron and starred stage star Robert Warwick.  

==Cast==
*Robert Warwick - Tommy Trotter Lois Wilson - Miss Emsdale
*Theodore Kosloff - Bosky Edward Jobson - Corr McFadden
*J. M. Dumont - Stuyvesant Smith
*Robert Dunbar - Mr. Smith-Parvis
*Helen Dunbar - Mrs. Smith-Parvis
*Anne Schaefer - Mrs. Jacobs
*Frances Raymond - Madam Deborah William Boyd - Carpenter
*George Berrell - Bramble
*Snitz Edwards - Drouillard Richard Cummings - Moody
*T. E. Duncan - The Detective

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 


 