Married to the Mob
 
{{Infobox film
| name = Married to the Mob
| image = married_to_the_mob_movie_poster.jpg
| caption = Theatrical release poster
| director = Jonathan Demme
| producer = Edward Saxon
| writer = Barry Strugatz   Mark R. Burns
| starring = Michelle Pfeiffer Matthew Modine Dean Stockwell Mercedes Ruehl Alec Baldwin David Byrne
| cinematography = Tak Fujimoto Craig McKay
| distributor = Orion Pictures
| released =  
| runtime = 104 minutes
| country = United States
| language = English
| budget = $10 million
| gross = $21,486,757
}}
Married to the Mob is a 1988 American comedy film directed by Jonathan Demme, starring Michelle Pfeiffer and Matthew Modine.

Michelle Pfeiffer, in something of a departure from her previous roles, gave an acclaimed lead performance as a gangsters widow from Brooklyn, opposite Matthew Modine as the undercover FBI agent assigned the task of investigating her mafia connections. 

As a slippery mob boss romantically pursuing Angela, Dean Stockwell was nominated for the Academy Award for Best Supporting Actor.

==Plot==
Angela de Marco (Michelle Pfeiffer) is the wife of mafia up-and-comer Frank "The Cucumber" de Marco (Alec Baldwin), who gets violently dispatched by Mob boss Tony "The Tiger" Russo (Dean Stockwell) when he is discovered in a compromising situation with the latters mistress Karen (Nancy Travis). Angela wants to escape the mafia scene with her son, but is harassed by Tony who puts the moves on her at Franks funeral. This clinch earns her the suspicion of FBI agents Mike Downey (Matthew Modine) and Ed Benitez (Oliver Platt), and also of Tonys wife Connie (Mercedes Ruehl), who repeatedly confronts Angela with accusations of stealing her husband. To further complicate things, Mike Downey is assigned to monitor all of Angelas movements as part of an undercover surveillance operation, but cannot resist becoming romantically involved with Angela himself. Angelas attempts to break away from the Mob result in comic mayhem and a climactic showdown in a honeymoon suite in Miami.

==Cast==
* Michelle Pfeiffer as Angela de Marco
* Matthew Modine as Agent Michael "Mike" Downey
* Dean Stockwell as Anthony "Tony the Tiger" Russo
* Mercedes Ruehl as Connie Russo
* Paul Lazar as Tommy Boyle
* Alec Baldwin as Frank "The Cucumber" de Marco
* Joan Cusack as Rose
* Ellen Foley as Theresa
* O-Lan Jones as Phyllis
* Oliver Platt as Agent Edward "Ed" Benitez
* Nancy Travis as Karen Lutnick
* Sister Carol as Rita Hello Gorgeous Harcourt
* Tracey Walter as Mr. Chicken Lickin
* Chris Isaak as The Clown Obba Babatunde as The Face of Justice Charles Napier as Ray, Angelas hairdesser
* Frank Ferrera as Vinnie "The Slug"
* Gary Howard Klar as Al The Worm
* Gary Goetzman as The Guy at the Piano
* Jonathan Demme (uncredited, unconfirmed) as Man getting off elevator

==Reception==
===Critical response===
Married to the Mob received a largely positive response from critics, and currently holds an 91% rating on Rotten Tomatoes  and a score of 71 on Metacritic. 
 Time Out wrote that although the film was "relentlessly shallow, the performances, music and gaudy visuals provide a fizzy vitality for which many other directors would give their right arm."  Roger Ebert in the Chicago Sun-Times gave a more lukewarm review, but ended positively: "Still, Married to the Mob is loaded with wonderful offbeat touches...   most assuredly doesnt lack soul."   

Jonathan Demmes direction was praised for its idiosyncrasy. The New York Times called him "American cinemas king of amusing artifacts: blinding bric-a-brac, the junkiest of jewelry, costumes so frightening they take your breath away."  The Washington Post wrote that "Jonathan Demme has nailed one with this playful, but dangerous, gangster farce." 

The acting performances were widely acclaimed, especially that of Michelle Pfeiffer in a star-making turn, "her best performance to date."  Richard Corliss in Time (magazine)|Time wrote that Pfeiffer was the "emotional anchor to his   vertiginous sight gags." {{cite news|url=http://www.time.com/time/magazine/article/0,9171,968171-1,00.html
 |first=Richard |last=Corliss |title=Cinema: Mafia Princess, Dream Queen, MARRIED TO THE MOB - TIME |publisher=time.com |date=August 22, 1988}}  Variety claimed the "enormous cast is a total delight, starting with Pfeiffer."  The Washington Post called Pfeiffer a "deft comedian... Its her movie, and she graces it."  Matthew Modine was "winning," according to Variety. 

Supporting players Dean Stockwell and Mercedes Ruehl also received praise for their performances. The Washington Post described Ruehls character as "majestic in her jealousy, stealing scenes but never the show from the sweetly determined Pfeiffer."  Maslin in the New York Times thought that Pfeiffer and Modine were "readily upstaged by Miss Ruehl and, especially, by Mr. Stockwell. His shoulder-rolling caricature of this suave, foppish and thoroughly henpecked kingpin is the films biggest treat."  Variety described Stockwell as "a hoot." 

===Accolades===
Michelle Pfeiffer was nominated for the Golden Globe Award for Best Actress - Motion Picture Musical or Comedy.   

  (1988). He also shared the   for Rain Man (1988). 

Mercedes Ruehl won the National Society of Film Critics Award for Best Supporting Actress. 

{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Awarding Body
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Nominee
! style="background:#B0C4DE;" | Result
|-
| Academy Awards Best Supporting Actor
| Dean Stockwell
| nomination
|-
| Golden Globe Awards Best Actress - Motion Picture Musical or Comedy
| Michelle Pfeiffer
| nomination
|-
| Kansas City Film Critics Circle Best Supporting Actor
| Dean Stockwell
| winner (tied with Martin Landau and Tom Cruise)
|-
| rowspan="2" | National Society of Film Critics Best Supporting Actor
| Dean Stockwell
| winner
|- Best Supporting Actress
| Mercedes Ruehl
| winner
|-
| New York Film Critics Circle Best Supporting Actor
| Dean Stockwell
| winner
|}

==See also==
* List of American films of 1988
* Q Lazzarus

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 