1000 Roses
 
{{Infobox film
| name           = 1000 Rosen
| image          = 1000_rosen_poster.jpg
| image_size     = 
| alt            = 
| caption        = Dutch Film Poster
| director       = Theu Boermans
| producer       = Matthijs van Heijningen
| writer         = Gustav Ernst
| narrator       = 
| starring       = Marieke Heebink   Jaap Spijkers
| music          = Lodewijk de Boer
| cinematography = 
| editing        = 
| distributor    = Shooting Star Film Distribution
| released       =  
| runtime        = 95 minutes
| country        = Netherlands
| language       = Dutch fl 2.000.000
| gross          = 
| preceded by    = 
| followed by    = 
}} 1994 Netherlands|Dutch drama film directed by Theu Boermans.

==Cast==
*Marieke Heebink	... 	Gina
*Jaap Spijkers	... 	Harry
*Tessa Lilly Wyndham	... 	Liesje
*Marianne Rogée	... 	Ginas Mother
*Marisa Van Eyle	... 	Rita
*Bert Geurkink	... 	Kernstock
*Hannes Demming	... 	Oom / Bankdirekteur
*Rik Launspach	... 	Mr. Marshall
*Busso Mehring	... 	Clochard
*Georg Bühren	... 	Bankemployee
*Michiel Mentens	... 	Mijnheer Offermans
*Camilla Mercier	... 	Oude vrouw
*Jean Vercoutere	... 	Fabrieksdirekteur
*Clement Franz	... 	Burgemeester
*Christian Deuson	... 	Politie commissaris

== External links ==
*  

 
 
 
 

 