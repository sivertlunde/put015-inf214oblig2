The Mattei Affair
{{Infobox film
| name           = The Mattei Affair (Il Caso Mattei)
| image          = Mattei_locandina.jpg
| image_size     =
| producer       =  Franco Cristaldi
| director       = Francesco Rosi
| writer         =  Tito Di Stefano Tonino Guerra
| starring       = Gian Maria Volonté Luigi Squarzina Gianfranco Ombuen
| music          = Piero Piccioni
| cinematography = Pasqualino De Santis
| editing        =
| distributor    = Paramount Pictures
| released       = Ma, 1972 (premiere at Cannes Film Festival|Cannes) 20 May 1973 (New York City only)
| runtime        = 116 minutes
| country        = Italy
| language       = Italian
}} seven sisters for oil and gas deals in northern African and Middle Eastern countries.
 Grand Prix with The Working Class Goes to Heaven at the 1972 Cannes Film Festival.    Italian star Gian Maria Volonté was the leading actor in both films.
 Salvatore Giuliano (1962). Rosi remains faithful to his Italian neorealism|neo-realist roots with on-location shooting and non-professional actors. The film is interspersed with footage of the director trying to find his friend, the investigative journalist Mauro De Mauro, who disappeared while doing research for the film. He was killed by the Sicilian Mafia, but like the death of Mattei, De Mauro’s case was never solved. 

==Cast==
* Gian Maria Volonté - Enrico Mattei
* Luigi Squarzina - Journalist
* Gianfranco Ombuen - Ing. Ferrari
* Edda Ferronao - Mrs. Mattei
* Accursio Di Leo - Sicilian important man #1
* Giuseppe Lo Presti - Sicilian important man #2
* Aldo Barberito - Official
* Dario Michaelis - Carabinieri official Peter Baldwin - McHale (journalist)
* Franco Graziosi - Minister
* Elio Jotta - Head of commission
* Luciano Colitti - Bertuzzi
* Terenzio Cordova - Police official
* Camillo Milli - Change teller
* Jean Rougeul - American official

==References==
 

==External links==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 

 