Slayers Return
{{Infobox film name = Slayers Return image = File:Slayers Return.png writer = Hajime Kanzaka director = Hiroshi Watanabe music = Takayuki Hattori producer = Kiminobu Sato starring = Megumi Hayashibara Maria Kawamura Akiko Hiramatsu Kenji Utsumi Kazuki Yao studio = J.C.Staff
| distributor = Toei Company   Bandai Visual   released =   country = Japan language = Japanese runtime = 61 minutes
}}
 Hiroshi Watanabe. Slayers Perfect that was itself followed by Slayers Great in 1997.

In Slayers Return, the mercenary heroines Lina Inverse and Naga the Serpent are hired to rescue a village from an evil wizard who is searching there for a legendary treasure that might be an invincible magical weapon instrumental for his plan of world domination. The film was met with a positive reception and was also adapted into a manga version by Kanzaka and Shoko Yoshinaka.

==Plot==
Young sorceresses   and she would like to put her hands on it. Because of Linas suspicious behavior, the equally greedy Naga decides to follow them.

Once in Biaz, the two ask for a reward for saving it but the village chief and Saleenas father is not able to give them a reward high enough, so Naga takes and puts on a orihalcon bracelet she finds there. Lina and Naga embark to defeat the Zein megalomaniac leader Galev and discover is a fluke as his wizard powers are mostly a bluff, and his organization currently consists of just himself and his sole underling Zahhard. The girls and Saleena confront Galevs arriving collection of henchmen but they want to get Galev themselves because he did not pay their promised salary. Turns out that Galev planned to take hold of a long-lost legendary superweapon that find out about in a old magic book and, if he was able to take possession of it, conquer and rule to world. 

When the weapon is unearthed, what Lina thought would immense treasure turns out to be a massive golem made whole of orihalcon. Nagas bracelet is its control device but she can not either control it properly or take it off. The golem attacks them all and a chase and running battle begins, in which everyone join the forces to defeat the golem and the mini-golems it spawns. To solve destroy the crazed golem, Lina asks Naga to create one of her golems and surround it with a barrier, so Lina can cast a destructive Dragon Slave spell and send the second golem right against the first, finally defeating it but ruining all the orihalcon. The film ends with the girls and Biazs people running after Galev, because he did not only take money from them and spent for useless fancy costumes for his organization, but now it was also impossible for the chief to pay Lina and Naga.

==Cast==
{| class="wikitable"
|-
! Character
! Japanese voice actor
! English voice actor
|-
| Lina Inverse
| Megumi Hayashibara
| Cynthia Martinez
|-
| Naga the Serpent
| Maria Kawamura
| Kelly Manison
|-
| Galev
| Kenji Utsumi
| Jason Douglas
|-
| Zahhard
| Kazuki Yao
| Kurt Stoll
|-
| Saleena
| Akiko Hiramatsu
| Nora Stein
|-
| Becker
| Chafurin
| David Born
|-
| Warrior
| Daisuke Gori
| Jason Konopisos
|-
| Amazon
| Maya Okamoto
| Luci Christian
|-
| Sorcerer
| Minoru Inaba
| Marty Fleck
|-
| Saleenas father
| Akio Ōtsuka
| Mike Kleinhenz
|}

==Release==
Slayers Return was originally released theatrically in Japan on August 3, 1996, distributed by Toei Animation and screened in as a double feature with the film version of X (manga)|X. Bandai Visuals home version was released on the VHS and LaserDisc in April 1997,   and re-released on the DVD as part of the EMOTION the Best Slayers Movie Edition DVD-BOX (EMOTION the Best スレイヤーズ 劇場版 DVD-BOX) collection of all Slayers films in 2010.  It will be included in the collection of digitally remastered Slayers films and OAV series,  to be released on Blu-ray in Japan on February 27, 2015. 

The film was released by ADV Films on the DVD in North America on November 25, 2003 as Slayers Movie 2 - The Return;  ADV also acquired distribution rights for it in the UK.  Return was later released by ADV with the other four Slayers movies in a "Movie" boxset,  and with the other four movies and both OVA series in a "Movies and OVAs" box set. 

The English dubbing version was written by Dan Kanemitsu and directed and produced by Sandra Krasa. The film was also broadcast in the English version by ADVs Anime Network, and was released in Australia and New Zealand by Madman Entertainment, in France by Déclic Images, in Italy by Yamato Video (dubbed to Italian by DEA Digital Editing Audio), and in Germany by ACOG and OVA Films (dubbed to German by Circle of Arts).

===Books===
A companion guide book was published by Fujimi Shobō in the Dragon Magazine Collection in August 1996 (released also in the low-budget "miniartbook" version ), followed by Slayers Return Anime Comic in November 1996. The scripts for Return, Slayers Great|Great and Slayers Gorgeous|Gorgeous were published in the book Slayers Original.

The films 1997 manga adaptation   was written by Hajime Kanzaka and illustrated by Shoko Yoshinaka.   It is the fourth volume of the main Slayers series, although it represents a break in the continuity of the story. It was published in North America by Central Park Media on February 4, 2004. 

===Soundtrack=== King Records on August 28, 1996. It was later released in North America by ADV Films on November 19, 2002 (B00007BH7R). 

The soundtrack contains all of the films background music (BGM) composed by Takayuki Hattori, the ending song (as well as its special version called "Movie Size Version"), and two characters songs ("image songs") along with a special version of one of them (called "Fireball Groove Mix") and a karaoke version of the other. All vocalized songs were performed by Megumi Hayashibara and Maria Kawamura.

The films theme song "Just Be Conscious" was released as a single CD (KIDA-136) by Starchild Records on July 5, 1996. Both "Just Be Conscious" and "Run All the Way!" were featured in Hayashibaras 1997 album Iráváti. The songs from the film were also later included in the CD collection The Best of Slayers Vol. 2 (From OVA, Movie & Game).

{{Infobox album 
| Name       = Slayers Return: Motion Picture R
| Type       = Soundtrack
| Artist     = Takayuki Hattori
| Cover      =
| Released   = August 28, 1996 (Japan)   November 19, 2002 (North America)
| Recorded   = 1996
| Genre      = Orchestral, J-pop King Records  (Japan)   ADV Films (North America)
}}

{{tracklist
|title1=Just Be Conscious
|length1=4:40
|title2=Run All the Way!
|length2=4:58
|title3=Galef (恐るべき野望ガレフ GAREFU osorubeki yabou)
|length3=0:53
|title4=Main Title (メインタイトル Taitoru mein) 
|length4=0:16
|title5=Shadow of the Enemy, and the Smell of Money (敵の影とお宝の匂い Teki no kage to otakara no nioi)
|length5=1:00
|title6=Spearhead for World Conquest (世界制覇の先兵 Sekaiseiha no senpei)
|length6=1:42
|title7=Lina and Naga Are Here! (リナとナーガ,ここにあり RINA to NĀGA, kokoni ari)
|length7=1:17
|title8=Seriously! (本気! Honki!!)
|length8=3:48
|title9=Testimony of the Promise (約束の証 Yakusoku no akashi)
|length9=1:03
|title10=Zein Full Picture of the Threat (ツァイン」脅威の全貌 "SHAIN" Kyoui no zenbou)
|length10=0:53
|title11=Foreboding of Atrocity (惨劇への予兆 Sangeki e no yochyou)
|length11=1:46
|title12=Lost Spell, Now (幻の呪文,ここに Maboroshi no jumon, kokoni)
|length12=1:19
|title13=Saleenas Determination (サリーナの決意 SARĪNA no ketsui)
|length13=2:13
|title14=When I Hold High the Sword of Pride (誇りという剣をかざす時 Hokori toiu ken wo kazasu toki)
|length14=0:59
|title15=Ancient Power (古の力 Inishie no chikara)
|length15=2:50
|title16=And to the Battle (そして闘いへ Soshite tatakai e)
|length16=0:27
|title17=Golem, Charge! (ゴーレム突進せよ GŌREMU tosshin seyo)
|length17=1:41
|title18=Rune Blas, the Demon Beast (魔獣王ルーンガスト Majuu-ou RŪN GASUTO)
|length18=2:50
|title19=Worthy Opponent (倒しがいのある相手 Taoshigai no aru aite)
|length19=1:11
|title20=Lina (リナ ~ 美しき鼓動 RINA ~ utsukushiki kodou)
|length20=2:09
|title21=Just be Conscious (Movie Size Version)
|length21=3:15
|title22=Run All the Way! (Fireball Groove Mix)
|length22=7:38
|title23=Seriously! (Karaoke Version)
|length23=3:47
}}

==Reception==
The film was well received by most Western anime critics, including a score of 79% from Patrick King of Animefringe.  Dani Moure of Mania.com gave this "hilarious little movie with endearing characters" a B+ and positively compared it to the first Slayers film, writing that Return, despite a shorter run time than its predecessor, is "still immensely enjoyable, and actually manages to be a complete story that doesnt totally alienate those who have no prior experience with the series."  Mania.coms Chris Beveridge too praised the film, especially for how it "tells a fun little tale with a fair number of quirky twists and physical comedy that it has something that would make just about anyone laugh," also giving it a B+ and declaring it "definitely a keeper."  Zac Bertschy of Anime News Network recommended it as a classic of its era, stating, "anime was somehow different back in the mid-90s, and Slayers Return is a perfect time capsule back to those days. Dont miss it." 

According to The Digital Fix, "if youve seen any of the other Slayers films/OVAs, youll know what to expect from Slayers Return; its certainly an enjoyable way to spend an hour (and doubly so if you prefer the solitary Lina/Naga vibe to the party dynamic found in the TV series);" the film received a score of 7/10.  Animetion gave it four stars out of five, opining that "if you want serious and meaningful fantasy check out Escaflowne (film)|Escaflowne, but if you want love-struck golems, disenfranchised henchmen, villains with delusions of grandeur, plenty of pointless explosions and a good laugh then Slayers Return is just what youre looking for."  Polish fantasy writer Aleksandra Janusz, writing for the magazine Kawaii, noted the film to be funnier than its predecessor, which she called "a very good movie".  In a relatively rare dissenting review, Mania.coms Megan Lavey gave Return a score of C, calling it "the weakest movie out of the  , simply because the plot is not memorable and even Lina and Naga arent very funny themselves in it, especially when compared to Slayers Great|Great." 

==References==
 

==External links==
*  (Madman Entertainments Slayers Movie Collection)
* 
* 

 
 

 
 
 
 
 
 
 
 