Flipper (1996 film)
{{Infobox film
| name           = Flipper
| image          = Flipper_Movie.jpg
| image_size     =
| caption        = Theatrical release poster
| director       = Alan Shapiro
| producer       = James McNamara Perry Katz
| co-producer    = Conrad Hool
| writer         = Ricou Browning Jack Cowden
| starring       = Paul Hogan Elijah Wood
| music          = Joel McNeely Bill Butler
| editing        = Peck Prior The Bubble Factory
| distributor    = Universal Pictures
| released       =  
| runtime        = 95 Minutes
| country        = United States
| language       = English
| budget         = $25 million    
| gross          = $20,080,020
}} adventure film film of Gold Coast. Although he expects to have another boring summer, he encounters an bespectacled dolphin whom he names Flipper and with whom he forms a friendship.

==Plot==
Sandy Ricks is sent off for the summer to stay with his uncle Porter in the seaside town of Coral Key. Initially, Sandy is unenthusiastic, and is upset that hes going to miss a Red Hot Chili Peppers concert which he already bought tickets for.
 pod of dolphins is frolicking near Dirks boat. As a big game fisherman, Dirk hates just about every other fish eating animal on earth. 
Later that evening, Sandy heads to the docks so that he can catch the ferry back to the mainland so he can see the concert. 

The next day, Porter and Sandy are paid a visit by the sheriff Buck, who explains that they cant keep the dolphin unless he is in captivity.
That night, Sandy and Kim set out on a dinghy to look for Flipper.  They failed to locate the dolphin, but see men dumping barrels off of Dirk Morans boat.
The next morning, Kim arrives looking for Sandy when Pete, Porters pet brown pelican, comes running up to her, almost as if asking her to follow him. Pete leads Kim to Flipper who is beached on the shore and sick.  echolocation and a special camera attached to his head to help them find the toxic waste. Their plan works, and are successfully able to locate barrels of the toxic waste. However, Flipper also manages to locate the rest of his pod, and drops the camera and reunites with them. Porter heads back to alert the sheriff. Sandy, however becomes concerned that something has happened to Flipper, and without telling anyone except for Marvin, sets off on the dinghy in order to find him. 

Sandy barely survives an encounter with Dirks boat which dismantles the dinghy. He sees a dorsal fin and thinks it is Flipper. It is actually Scar, the Hammerhead shark! Sandy swims for his life, but Scar is faster. As Scar is about to attack Sandy, Flipper appears and starts nose butting Scar in the gills, but the shark is stronger than Flipper. Scar is eventually driven off when Flippers dolphin pod comes to his aid.  Dirk Moran is arrested by Buck the sheriff for 

The next day, Sandys mom and sister arrive to pick Sandy up.Sandy decides to check out the commotion. It is Flipper, who has come to see Sandy off. The camera then slowly zooms out and the screen then fades to black.

==Cast==
*Paul Hogan as Porter Ricks
*Elijah Wood as Sandy Ricks
*Jessica Wesson as Kim
*Jonathan Banks as Dirk Moran
*Bill Kelley as Tommy
*Chelsea Field as Cathy
*Isaac Hayes as Sheriff Buck Cowan
*Jason Fuchs as Marvin
*Robert Deacon as Bounty Fisherman #1
*Ann Carey as Fishermans Wife
*Mark Casella as Bounty Fisherman #2
*Luke Halpin as Bounty Fisherman #3

==Production==
The film was shot in the Bahamas.    Animatronic dolphins, designed by Walt Conti and his team, had to be used extensively, such as in scenes where Flipper is needed to interact with the human characters, or swimming along; Conti stated that using real dolphins does not work out so well as many might think.   

==Reception== Boxoffice noted National Geographic, may be shocking for very young children. 

The films tagline, "This summer its finally safe to go back in the water." references the tagline of the 1978 feature film Jaws 2, "Just when you thought it was safe to go back in the water..."

===Box office===
The film debuted at No. 2 with $4.5 million.  Flipper ultimately grossed $20 million domestically, on a $25 million budget.

==DVD release== The Little Rascals, Casper (film)|Casper, and Leave It to Beaver (film). Dubbed "Family Favorites 4 Movie Collection: Franchise Collection", all four films are based off popular TV shows. Flipper was later released on Blu-ray on February 8, 2011.

 

==References==
 

==External links==
 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 