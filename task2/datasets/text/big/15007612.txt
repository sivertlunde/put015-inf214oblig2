W.C. Fields and Me
{{Infobox film
| name           = W.C. Fields and Me
| image          = Theatrical poster for W.C. Fields and Me.png
| image_size     =
| caption        = Theatrical Poster
| director       =  Arthur Hiller
| producer       = Jay Weston
| writer         = Bob Merrill
| narrator       =
| starring       = Rod Steiger Valerie Perrine
| music          = Henry Mancini
| cinematography = David M. Walsh
| editing        = John C. Howard
| studio         = Universal Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 111 mins
| country        = United States English
| budget         =
| gross          =
}} American biographical film directed by Arthur Hiller and starring Rod Steiger and Valerie Perrine. The screenplay by Bob Merrill is based on a memoir by Carlotta Monti, mistress of legendary actor W.C. Fields for the last 14 years of his life.

==Plot==
The story begins in 1924 in New York City, where W.C. Fields is a Ziegfeld Follies headliner, and ends with his 1946 death in California at age 66. In between, it dramatizes his life and career with emphasis on the latter part of both, when the "Me" of the title, Carlotta Monti, played a prominent role, with a number of fictionalized events added for dramatic impact.

Having lost his girlfriend Melody to another man and most of his life savings due to careless investments by his broker, Fields heads west to Santa Monica, where he operates a wax museum until hes offered a film role. He quickly becomes a major screen presence and a notorious drinker.
 Dave Chasen, Fields is introduced to starlet Carlotta Monti, whom he hires as a live-in secretary. In order to stifle her theatrical aspirations, he arranges a screen test. The studio boss Harry Bannerman decides she has some talent, but Fields threatens to quit Paramount Pictures unless shes discouraged from pursuing a career in films. When she learns the truth, Carlotta leaves him and goes to New York.

When Barrymore passes away, she returns to Hollywood to comfort Fields. On the set of  , a holiday he had despised with a passion.

==Principal cast==
*Rod Steiger ..... W.C. Fields 
*Valerie Perrine ..... Carlotta Monti 
*Jack Cassidy ..... John Barrymore 
*John Marley ..... Bannerman
*Bernadette Peters ..... Melody 
*Dana Elcar ..... Dockstedter  Paul Stewart ..... Florenz Ziegfeld 
*Billy Barty ..... Ludwig 
*Allan Arbus ..... Gregory LaCava 
*Milt Kamen ..... Dave Chasen 
*Louis Zorich ..... Gene Fowler 
*Andrew Parks ..... Claude Fields
*Dennis Alwood ..... Edgar Bergen

==Production notes== period films set in Los Angeles prior to its appearance in this film. In 2001 it was refurbished to operate on a rail line serving San Pedro, California. 

The mansion used for this film was located at 700 Berkshire Avenue, LaCanada Flintridge, Los Angeles County, was previously owned by William Joseph Connery, a silent-movie era independent movie producer. He called the home "Villa Ardaree" and it was known before that as Dryborough, after the original owners.
{{Infobox film
| image          = WCFields&Me.jpg 
| caption           = W.C. Fields and Me Soundtrack
}}

==Critical reception==
In his review in the  ." 

Time Out London calls it a "witless biopic   leaps through pseudo-history with cretinous inaccuracy. Sloppily slung together, hell-bent on wringing hearts with the drama of the last, lonely, drink-sodden years, it cant get even the simplest facts straight, and doesnt do much of a job on the tear-jerking either . . . Steiger makes a brave stab at the part, but the reality and genius of Fields never get a look in." 

However, TV Guide awarded it three out of a possible four stars with the comment, "Though the great comedian would have hated this film  , this movie biography . . . has a certain appeal thanks to Steigers handling of the lead role . . . Rather than ape Fields,   creates his own interpretation of the man, capturing subtle nuances that create a better-rounded character." 
==References==
 

==External links==
*  
*  
*   at Rotten Tomatoes

 

 
 
 
 
 
 
 
 
 
 