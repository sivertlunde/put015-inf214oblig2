Jericho (2000 film)
 
{{Infobox film
| name           =  Jericho
| image          = 
| caption        = 
| director       = Merlin Miller
| producer       = Merlin Miller Gil Dorland
| writer         = Frank Dana Frankolino George Leonard Briggs Robert Avard Miller
| starring       = Mark Valley Leon Coffee R. Lee Ermey Mason McWilliams Kateri Walker
| music          = Mark Haffner
| cinematography = Jerry Holway
| editing        = Agustin Rexach
| distributor    = 
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| budget         = 
}}
Jericho is a 2000 American western film starring Mark Valley and directed by Merlin Miller.  The film deals with the adventures of an amnesiac and former sheriff, played by Mark Valley, as he roams across the old west in search of the answers to his lost past.

== Cast ==
*Mark Valley as Jericho
*Leon Coffee as Joshua
*R. Lee Ermey as Marshall
*Lisa Stewart as Mary
*Mark Collie as Johnny O
*Morgana Shaw as Mildred Flynn
*Buck Taylor as Pap Doolin

== Reception ==
Joe Leydon of Variety (magazine)|Variety called it a non-Political correctness|P.C., bland throwback to Saturday matinee fare. 

== References ==
 

== External links ==
*  

 
 
 
 
 
 