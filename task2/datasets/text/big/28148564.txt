Václav (film)
{{Infobox film
| name           = Václav
| image          = Vaclavfilm.jpg
| image size     = 
| alt            = 
| caption        = 
| director       = Jiří Vejdělek
| producer       = Jaroslav Bouček
| writer         = Marek Epstein   Jiří Vejdělek 
| narrator       = Tomáš Kudrna
| starring       = Ivan Trojan  Emília Vášáryová  Jan Budař  Soňa Norisová  Jiří Lábus
| music          = Jan P. Muchow
| cinematography = Jakub Šimůnek
| editing        = Jan Daňhel
| studio         = BUC Film
| distributor    = SPI International
| released       =  
| runtime        =  97 min
| country        = Czech Republic
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}
 Czech drama film, directed by Jiří Vejdělek
in 2007.

==Cast==
*Ivan Trojan as Václav Vingl
*Emília Vášáryová as Václavs mother
*Jan Budař as František
*Soňa Norisová as Lída
*Jiří Lábus as chief magistrate
*Petra Špalková as Majka
*Martin Pechlát as Father
*Martina Delišová as young mother
*Jan Vlasák as Pilecký
*Zuzana Kronerová as hostelry
*Gregor Bauer as little Václav
*Hynek Bečka as little František
*Miroslav Moravec - voice of little Václavs 
*Jan Šťastný - voice of little František

==External links==
*  
*  

 
 
 


 