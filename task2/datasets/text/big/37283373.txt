Island (1989 film)
 
 {{Infobox film
| name           = Island
| image          = Island (1989 film).jpg
| image size     =
| caption        = 
| director       = Paul Cox
| producer       =
| writer         = Paul Cox
| based on = 
| narrator       =
| starring       = Irene Pappas Eve Sitta Anoja Weerasinghe Chris Haywood
| music          = 
| cinematography = 
| editing        = 
| studio = Illumination Films
| distributor    = 
| released       = 1989
| runtime        = 95 mins
| country        = Australia English Greek
| budget         = A $2 million 
| gross = A $114,764 (Australia) 
| preceded by    =
| followed by    =
}}
Island is a 1989 Australian film directed by Paul Cox starring Irene Papas.

It is not to be confused with the 1975 short film of the same title also made by Cox.

==Plot==
A Czech-born woman arrives on a Greek island having fled Australia to sort out her problems. She becomes friends with a deaf mute and two other women, a Sri Lankan abandoned by her husband and an older Greek woman. 

==Production==
The film was shot on a Greek island in 1988. It was plagued with money shortages during production, making the shoot extremely difficult. David Stratton, The Avocado Plantation: Boom and Bust in the Australian Film Industry, Pan MacMillan, 1990 p127-128  
==Reception==
Irene Papas was nominated in 1989 for Best Actress in her role for the Australian Film Institute.

==References==
 

==External links==
*  at IMDB
 

 
 
 

 