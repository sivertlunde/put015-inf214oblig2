The Gambling Ghost
 
 
{{Infobox film
| name           = The Gambling Ghost
| image          = GamblingGhost DVDcover.jpg
| image_size     = 
| caption        = UK DVD cover
| director       = Clifton Ko
| producer       = Clifton Ko Sammo Hung
| writer         = Clifton Ko
| narrator       =  James Wong
| music          = 
| cinematography = Lee Yau-tong Ng Cho-wah
| editing        = 
| distributor    = 
| released       =  
| runtime        = 90 min Hong Kong
| language       = Cantonese
| budget         =  HK $7,729,690.00
| preceded_by    = 
| followed_by    = 
}} 1991 Hong Hong Kong action comedy film directed by Clifton Ko. It stars Sammo Hung in three roles as different generations of the same family - son, father and ghostly grandfather. Hungs co-star, Man Hoi also worked as the films action director.

The opening scene of the film parodies another Hong Kong film, God of Gamblers.

==Cast==
* Sammo Hung - Fatty Big Brock / Fat Bao (Fattys father) / Hung Kau/Gao
* Nina Li Chi - Miss Lily (as Nina Li)
* Mang Hoi - Hoi Siu-Hon (as Man Hoi) James Wong - Rich Brother Dragon 
* Teddy Yip Wing-Cho - Yau Mo-Leung (Kaus Partner) (as Ip Wing Cho)
* Wu Ma - Uncle Ng (as Ng Ma)
* Corey Yuen - Gambler Wah
* Chung Fat - Driver 
* Stanley Fung - Motorcycle Cop (as Fung Shu Fan)
* Lam Ching-ying - Exorcist 
* Richard Ng - Roadblock Cop #1 (as Ricky Ng)
* Billy Ching Sau Yat - Roadlock Cop #2
* Billy Chow - Yaus Bodyguard
* Robert Samuels - Yaus Bodyguard James Tien - Monk Taoist Foster (as Tin Chuen) Paul Chun - Gambler (as Chun Pui)
* Jameson Lam - Wedding Thug
* Garry Chan - Wedding Thug
* Hon Ping - Thug
* Tsim Siu-Ling - Thug
* Jue Wan-Sing - Thug
* Kwan Kwok-Chung - Thug
* Kwan Yung - Thug
* Chan Sek - Thug
* Sha-fei Ouyang - Mahjong Player
* Man Wah Tsui - Lottery Hostess
* Clifton Ko - TV Crew Member (as Clifton C.S. Ko)
* Norman Ng
* Huang Kai-Sen
* Frank Liu
* Jobic Wong
* Yuet Yue Chan
* Simon Yip
* Chiu Sek-Man
* Wong Man-Shing
* Leung Sam

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 