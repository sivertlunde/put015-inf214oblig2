Tell No One (2012 film)
{{Infobox film
 | name = Tell No One (Come non detto)
 | image = Tell No One (2012 film).jpg
 | caption =
 | director = Ivan Silvestrini
 | writer =  Roberto Proia 
 | starring =  Josafat Vagni 
 | music =  Leonardo Rosi
 | cinematography =  Rocco Marra 
 | editing = Alessia Scarso 
 | released =  
 | country = Italy
 | language =  Italian
| runtime = 81 min
 }}
Tell No One ( ) is a 2012 Italian comedy film, directed by Ivan Silvestrini.    It is based on a novel with the same title written by Roberto Proia. 

== Plot summary ==
Mattia is a homosexual guy from Rome, the lover of a young Spanish student named Eduard. He cannot tell his parents about his love affair, because of their very conservative opinions and the particular situation in Rome - everyone Mattia meets in the City hate gay people, and condemn them as unclean beings. 
Mattia plans to secretely run away with Eduard while telling his parents that he intends to leave Italy in order to find work.  Mattias plan seems to work, but the troubles begin when he discovers that Eduard is coming to Italy in order to meet Mattias parents. Given the relative liberal attitudes in Spain towards gay people, Eduard thinks that Mattia has already told his parents and his environment. Mattia tries to get along with everyone for a while, but in the end is forced to reveal his homosexuality to his parents, who understand him. 
Finally, Mattia can fulfil his need for love by going to Spain with Eduard.

== Cast ==

*Josafat Vagni: Mattia
*Valeria Bilello: Stefania Francesco Montanari: Giacomo / Alba Paillettes
*Monica Guerritore: Aurora
*Ninni Bruschetta: Rodolfo 
*Victoria Cabello: herself

==References==
 

==External links==
* 

 
 
 
 
 
 


 
 