Support Your Local Sheriff!
{{Infobox film
| name           = Support Your Local Sheriff!
| image          = Poster of the movie Support Your Local Sheriff!.jpg
| caption        =
| director       = Burt Kennedy
| producer       = William Bowers Bill Finnegan
| writer         = William Bowers
| starring       = James Garner Joan Hackett Walter Brennan Harry Morgan Jack Elam Bruce Dern
| music          = Jeff Alexander
| cinematography = Harry Stradling Jr.
| editing        = George W. Brooks
| distributor    = United Artists
| released       =  
| runtime        = 92 min.
| country        = United States
| language       = English
| gross          = $5 million (US/ Canada rentals) 
}}

Support Your Local Sheriff! is a 1969 American Technicolor comedic western film distributed by United Artists, directed by Burt Kennedy, written by William Bowers, and starring James Garner, Joan Hackett, Walter Brennan, Harry Morgan, Jack Elam and Bruce Dern. 

The film parodies the often-filmed scenario of the iconoclastic western hero who tames a lawless frontier town. Its title was derived from a popular 1960s campaign slogan "Support Your Local Police". 

==Plot==
The Old West frontier town of Calendar, Colorado, springs up almost overnight when the rather klutzy and hotheaded Prudy Perkins (Joan Hackett) discovers gold in a freshly dug grave during a funeral. Her father Olly (Harry Morgan) becomes mayor of the new settlement. He and the other members of the town council bemoan the twin facts that the place has become a drunken round-the-clock free-for-all, and that to ship out all the gold they are mining, they must pay a hefty fee to the Danbys, a family of ranchers/bandits who control the only shipping route out of town. Most people are too busy digging to take time out to be sheriff, and those who are willing to put down their shovels quickly die. 

This all changes with the arrival in town of Jason McCullough (James Garner), a calm and exceptionally competent man from "back East" who says he is only passing through town on his way to Australia. While in the town bar (establishment)|saloon, he sees young Joe Danby (Bruce Dern) gun a man down. Needing money after discovering the towns ruinous rate of inflation, McCullough demonstrates his uncanny firearms ability to the mayor and town council, and becomes the new sheriff. He breaks up a street brawl and while at the Perkins house meets Prudy under (for her) mortifying circumstances. McCullough then arrests Joe and tosses him in the town jail, which has everything a sheriff needs - except iron bars for the cell doors and windows. McCullough keeps the none-too-bright Joe imprisoned through the use of a chalk line, some dribbles of red paint, and applied psychology.

In the midst of all this, McCullough acquires a reluctant deputy in the form of the extremely scruffy Jake (Jack Elam), who had theretofore been nothing more than the "town character." The arrest of Joe Danby ignites the wrath of the patriarch of the Danby family; while the rest of the town immediately quiets down under McCulloughs reign, Pa Danby (Walter Brennan) mounts various efforts to get Joe out of jail. None of them work, so he then brings in a string of hired guns, who are equally unsuccessful. Meanwhile, Prudy spars romantically with McCullough, McCullough and Jake go on an unsuccessful search for gold, and, much to Joes relief, bars are finally installed in the jail.

Pa Danby summons a host of his relatives to launch an all-out assault. The sheriffs first impulse is simply to leave town and resume his trip to Australia, but when Prudy expresses her sincere approval of this sensible idea, he announces that it sounds cowardly and decides to stay. The rest of the townsfolk announce their disapproval of his new plan, and officially vote not to help in any way. Thus, the Danby clan rides in faced only by McCullough, Jake, and Prudy. After a lengthy gunfight, McCullough bluffs his way to victory using hostage Joe and the old cannon mounted in the center of town. As all the Danbys are marched off to jail, the cannon fires, smashing the town brothel and scattering the resident prostitutes and visiting civic leaders. 

Sheriff McCullough later makes his peace with the mayor, town council, and townsfolk and he and Prudy get engaged, carrying her home. In a closing monologue, Jake breaks the films fourth wall and directly informs the audience that they get married and McCullough goes on to become governor of the state of Colorado, never making it to Australia, while he, Jake, becomes sheriff and then "one of the most beloved characters in western folklore."

==Cast==
* James Garner as Jason McCullough
* Joan Hackett as Prudy Perkins
* Walter Brennan as Pa Danby
* Harry Morgan as Olly Perkins
* Jack Elam as Jake Henry Jones as Henry Jackson
* Bruce Dern as Joe Danby. Dern had not done a comedic role before, and thoroughly enjoyed playing against type. His portrayal of a man who is not nearly as fearsome as he thinks he is was one of the films strong points.   
* Willis Bouchey as Thomas Dever
* Kathleen Freeman as Mrs. Danvers 
* Walter Burke as Fred Johnson
* Chubby Johnson as Brady 
* Gene Evans as Tom Danby
* Dick Peabody as Luke Danby
* Dick Haynes as Bartender

==Reception==
The film was the 20th most popular movie at the U.S. box office in 1969. 

Its comedic handling of the classic western formula has made it a cult film among Westerns. 

==Follow-up==
In 1971 director Burt Kennedy re-teamed with Garner, Morgan, and Elam to make another western comedy of a similar tone, Support Your Local Gunfighter. Many of the original supporting actors re-appeared, as well.

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 