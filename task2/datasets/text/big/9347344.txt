Deadly Outlaw: Rekka
{{Infobox film
| name           = Deadly Outlaw: Rekka
| image          = Deadly Outlaw Rekka.jpg
| image_size     =
| caption        =
| director       = Takashi Miike
| producer       =
| writer         = Shigenori Takechi
| narrator       =
| starring       =
| music          = Joe Yamanaka
| cinematography = Kiyoshi Itô
| editing        = Yasushi Shimamura
| distributor    =
| released       = 2002
| runtime        =
| country        =
| language       =
| budget         =
| preceded_by    =
| followed_by    =
}} Japanese yakuza film directed by Takashi Miike starring Riki Takeuchi and Sonny Chiba.

==Plot==
After Kunisada (Riki Takeuchi)s yakuza leader and father figure is brutally murdered, he and his best friend (Kenichi Endo) go on a two-man mission to avenge his death, killing other Yakuza leaders leading to a final confrontation by the old mans killers.

==Cast==
*Riki Takeuchi as Kunisada
*Ryôsuke Miki
*Kenichi Endo
*Mika Katsumura
*Sonny Chiba
*Yuya Uchida
*Tetsuro Tamba
*Renji Ishibashi Lily as Kunisadas Godmother
*Miho Nomoto
*Joe Yamanaka
*Rikiya Yasuoka

==Other credits==
*Produced by
**Akira Ando - planner
**Mitsuru Kurosawa - executive producer: Toei Video
**Fujio Matsushima - associate planner
**Tsuneo Seto - producer
**Shigenori Takechi - associate planner
*Sound Department: Fusao Yuwaki - sound
*Supervisor: Noboru Ando

==Notes==
The entire soundtrack to this film is the album Satori (Flower Travellin Band album)|Satori by Flower Travellin Band, whose lead singer Joe Yamanaka and producer Yuya Uchida have small roles in the film.

== External links ==
*  

 

 

 
 
 
 
 


 