Charly
 

{{Infobox film
| name = Charly
| image = Charly 1968.jpg
| image_size =
| caption = 1968 Theatrical Poster
| director = Ralph Nelson
| producer = Ralph Nelson
| screenplay = Stirling Silliphant
| based on = Flowers for Algernon by Daniel Keyes
| starring = Cliff Robertson Claire Bloom Leon Janney Lilia Skala Dick Van Patten
| music = Ravi Shankar
| cinematography = Arthur Ornitz
| editing = Fredric Steinkamp
| studio = ABC Motion Pictures Robertson and Associates Selmur Productions Cinerama
| released =  
| runtime = 106 minutes   
| country = United States
| awards = English
| budget = $2,225,000 "ABC Motion Pictures|ABCs 5 Years of Film Production Profits & Losses", Variety (magazine)|Variety, 31 May 1973 p 3 
| gross = $8,500,000  (rentals)  
}}
 Academy Award-winning intellectually disabled human intelligence. Stirling Silliphant adapted the movie from the novel Flowers for Algernon by Daniel Keyes.

== Plot ==

Charly Gordon (Cliff Robertson), an intellectually disabled man with a strong desire to make himself smarter, has been attending night school for two years where he has been taught by Alice Kinnian (Claire Bloom) to read and write. However, his spelling remains poor and he is even unable to spell his own name.

Alice takes Charly to the "Nemur-Straus" clinic run by Dr. Richard Nemur and Dr. Anna Straus . Nemur and Straus have been increasing the intelligence of laboratory mice with a new surgical procedure and are looking for a human test subject. As part of a series of tests to ascertain Charlys suitability for the procedure, he is made to race Algernon, one of the laboratory mice. Algernon physically runs through a maze while Charly uses a pencil to trace his way through a diagram of the same maze. Charly is disappointed that he consistently loses the races. Nevertheless, he is given the experimental surgery.
 bottom and sexually assaults her, pulling her to the floor and kissing her forcefully until she breaks free by slapping him.
 counterculture &ndash;  wearing a mustache and goatee, riding a motorcycle, kissing a series of different women, smoking and dancing. At the end of the sequence, Charly has returned home and Alice comes to visit him, both having learned during their time apart that they want to be together. A further montage sequence shows Charly and Alice running through woods and kissing under trees accompanied by a voice-over of the two of them talking about marriage.

Straus and Nemur present their research to a panel of scientists, including a question-and-answer session with Charly. Charly is aggressive during the session and then reveals that Algernon has just died, causing Charly to believe that his own increased intelligence is only temporary. After suffering visions of his intelligence fading and of his pre-operative self following him, Charly decides to work with Nemur and Straus to see if he can be saved. Charly discovers that there is nothing that can be done to prevent his own intelligence from fading. Alice visits Charly and asks him to marry her, but he refuses and tells her to leave.

In the films final scene, Alice watches Charly playing with children in a playground, having reverted to his former self.

== Cast ==

* Cliff Robertson&nbsp;– Charly Gordon
* Claire Bloom&nbsp;– Alice Kinian
* Lilia Skala&nbsp;– Dr. Anna Straus
* Leon Janney&nbsp;– Dr. Richard Nemur Ruth White&nbsp;– Mrs. Apple
* Dick Van Patten&nbsp;– Bert (as Richard Van Patten)
* Edward McNally&nbsp;– Gimpy (as Skipper McNally)
* Barney Martin&nbsp;– Hank
* William Dwyer&nbsp;– Joey
* Dan Morgan&nbsp;– Paddy

== Production history ==

  in the role of Alice.]] Days of Wine and Roses. He bought the rights to the story, hoping to star in the film version as well.  

He originally hired William Goldman to write the screenplay on the strength of Goldmans novel No Way to Treat a Lady, paying him $30,000 out of his own pocket. Dennis Brown, Shoptalk, Newmarket Press, 1992 p 63  Robertson was unhappy with Goldmans work and then hired Stirling Silliphant to do a draft. 

==Box office== theatrical rentals during its release in North America, and it earned an additional $1.25 million in theatrical rentals overseas. It is the 1968 in film|16th-highest grossing film of 1968. After all costs were deducted (including $1,325,000 paid to profit share), the film reported a profit of $1,390,000, making it one of the most successful movies ever made by ABC Pictures. 

== Critical reception ==
 mod film The Knack. " 

Roger Ebert gave the film three stars out of four, saying "The relationship between Charly (Cliff Robertson) and the girl (Claire Bloom) is handled delicately and well. She cares for him, but inadequately understands the problems hes facing. These become more serious when he passes normal IQ and moves into the genius category; his emotional development falls behind. It is this story, involving a personal crisis, which makes Charly a warm and rewarding film. " By contrast, Ebert pointed out "the whole scientific Hocus Pocus (magic)|hocus-pocus, which causes his crisis, is irrelevant and weakens the movie by distracting us. " 

In 2009, Entertainment Weekly listed Charly among its "25 Best Movie Tearjerkers Ever. " 

== Awards ==
 40th National Board of Review Awards, Charly was fourth in their list of "Top Ten Films" of 1968, and Cliff Robertson was chosen the years "Best Actor." 

At the  , losing to  . 

== Proposed sequel ==

In the late 1970s, following a period of extended unemployment that followed an act of whistleblower|whistle-blowing against David Begelman, the then-president of Columbia Pictures, Robertson wrote and attempted to produce Charly II, to no avail. 

== Home video release ==

Charly was released on Region 1 DVD by MGM Home Entertainment on March 31, 2005. 

== References ==
 

== External links ==

*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 