The Pretender (film)
{{Infobox film
| name           = The Pretender
| image          = Pretender1.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = W. Lee Wilder
| producer       = W. Lee Wilder
| writer         = Don Martin Doris Miller
| narrator       = 
| starring       = Albert Dekker Charles Drake
| music          = Paul Dessau
| cinematography = John Alton
| editing        = Asa Boyd Clark John F. Link Sr.
| distributor    = Republic Pictures
| released       =  
| runtime        = 69 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Pretender is a 1947 American crime film noir directed by W. Lee Wilder and written by Don Martin, with additional dialogue by Doris Miller.  The drama features Albert Dekker, Catherine Craig, Charles Drake, among others. 

==Plot==
The story tells of Kenneth Holden (Dekker), a crooked investment businessman who embezzles a large sum of money from an estate.  He hopes to cover his crime by marrying the estates heiress Claire Worthington (Craig).

However, Worthington is already engaged, so Holden arranges for her fiancee to be killed. The hired hit mans only means of identifying the victim is the picture in the society columns. When Claire Worthington changes her mind and agrees to marry Holden, however, it means that it is his picture that will appear in the newspaper, thereby condemning him to death.

Desperately trying to contact the hit man, Holden discovers that the man is dead, but his successor is still at large.

==Cast==
* Albert Dekker as Kenneth Holden
* Catherine Craig as Claire Worthington
* Charles Drake as Dr. Leonard Koster
* Alan Carney as Victor Korrin
* Linda Stirling as Flo Ronson Tom Kennedy as Fingers
* Selmer Jackson as Charles Lennox Charles Middleton as William the butler
* Ernie Adams as Thomas the butler
* Ben Welden as Mickie
* John Bagni as Hank Gordon
* Stanley Ross as Stranger

==Critical reception==
Critic Dennis Schwartz liked the film and wrote, "Billy Wilders lesser known elder brother William Lee Wilder...directs this striking film noir about a successful man becoming paranoiac and placing himself in entrapment. In one amazing characteristic noir scene, the protagonist is seated on the floor of his unlit, locked room eating crackers and canned food, afraid of being poisoned. This is one of the first movies to score for theremin, an effectively chilling mood music which later became a cliché for many 1950s sci-fi films about aliens. John Altons dark film noir photography sets the proper mood for the melodrama. The film noir is absorbing despite stilted dialogue and flat direction." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 