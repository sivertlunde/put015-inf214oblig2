Bhaag Johnny
{{Infobox film
| name              = Bhaag Johnny
| image             = 
| alt               = 
| caption           = 
| director          = Shivam Nair
| producer          = Bhushan Kumar Krishan Kumar Vikram Bhatt
| Co-producer       = Ajay Kapoor
| writer            = 
| starring          = Kunal Khemu Zoa Morani Mandana Karimi
| music             = Mithoon  Ankit Tiwari
| choreographer     = Aadil Shaikh
| cinematography    =
| editing           =  Super Cassettes BVG Films
| distributor       = 
| released          =  
| runtime           =
| country           = India
| language          = Hindi
| budget            = 
| gross             = 
}}

Bhaag Johnny is a 2014 Bollywood film directed by Shivam Nair under the banner of BVG Films and SCIL, Bhaag Johnny is a thriller film.  Starring Kunal Khemu  and Zoa Morani,  the film also marks the debut of Iranian actress Mandana Karimi.   The movie is produced by Bhushan Kumar, Krishan Kumar & Vikram Bhatt and Co-produced by Ajay Kapoor

Shot in parts of India and Thailand, the film is apparently expected to release in August 2014. Vikram Bhatt   is seen doing a cameo as a genie in the film.

==Cast==
* Kunal Khemu as Johnny
* Zoa Morani
* Mandana Karimi
*Vikram Bhatt in a cameo

==Plot==
Bhaag Johnny is a film about a man who is forced to lead two lives and the chain of events that follow when he has to opt for one over the other. The thought behind Bhaag Johnny is what if you could live two lives borne out of different choices at the same time? What if you could both kill someone or not kill someone? What if you fall in love and not fall in love? How it would be to live the consequences of both sides of the coin? 

==Soundtrack==
The songs are composed by Mithoon and Ankit Tiwari.

==Casting==
Earlier, Deepak Tijori was first to direct the film with Indian Television actor Karan Singh Grover. Later on, Vikram Bhatt cleared the doubts that Grover and Tijori will not be a part of project instead of this, Kunal Khemu will play leading role. 

==References==
 

==External links==
* 

 
 
 


 