New Morals for Old
{{Infobox film
| name           = New Morals for Old
| image          = New Morals for Old - Film Poster.jpg
| image_size    = 225px
| caption        = Theatrical release poster
| director       = Charles Brabin
| producer       = 
| writer         = Zelda Sears (Dialogue) Wanda Tuchock (Additional dialogue)
| screenplay     = 
| story          = 
| based on       =   
| narrator       =  Robert Young
| music          = 
| cinematography = John J. Mescall William S. Gray
| studio         = Metro-Goldwyn-Mayer
| distributor    = 
| released       =   
| runtime        = 74 mins.
| country        = United States
| language       = English
| budget         = 
| gross          =
}}

New Morals for Old is a 1932 American Pre-Code Hollywood|Pre-code romance-drama produced and distributed by MGM. The film was based on a 1931 Broadway play After All. Humphrey Bogart had a significant role in the play. His part was played by David Newell in the film.  

The film is noteworthy for having elements that would later be forbidden under the Motion Picture Production Code. There is very brief nudity, albeit in shadows and by a non-speaking character (the model in the painters studio).  Also, one of the female characters is in a relationship with a married man, and this is portrayed sympathetically.

==Plot== Robert Young) and Phyl Thomas (Margaret Perry), spend so many evenings at parties instead of spending time with family.  Their disapproval deepens when they discover both children want to move out to pursue lifestyles that the parents deem unacceptable: Phyl moves into her own apartment so that she can conduct an affair with a married man, Duff Wilson (David Newell). Her brother, Ralph, goes to Paris to pursue his dream of being a painter, thus disappointing his father who expected him to remain in the family wallpaper business. Mrs. Thomas repeatedly tries to invoke guilt in both children for not being with her, especially after Mr. Thomas dies of a stroke. 

Eventually, Phyl marries her paramour and Ralph returns to New York, having failed as an artist. Mrs. Thomas dies shortly after Ralph’s return. At the end of the film, Phyl, her twin infants, her husband Duff, and her brother Ralph are all living in the family home, with a newfound appreciation for the benefits of family life. In the film’s last scene, Ralph and Duff are laughing together about how Phyl has evolved into a protective maternal figure, much like her own mother.

==Cast== Robert Young - Ralph Thomas
*Margaret Perry - Phyl Thomas
*Lewis Stone - Mr. Thomas
*Laura Hope Crews - Mrs. Thomas
*Myrna Loy - Myra
*David Newell - Duff Wilson
*Jean Hersholt - James Hallett
*Ruth Selwyn - Estelle
*Kathryn Crawford - Zoe Atkinson
*Louise Closser Hale - Mrs. Warburton
*Mitchell Lewis - Bodwin Elizabeth Patterson - Aunty Doe

==DVD release==
New Morals for Old was released on DVD by the Warner Archive Collection. Laura Hope Crewss name is misspelled in MGMs original poster advertising and the films opening credits (see AllRovi link below).

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 