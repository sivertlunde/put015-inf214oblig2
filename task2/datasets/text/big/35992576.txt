Comin' Round the Mountain (1936 film)
{{Infobox film
| name           = Comin Round the Mountain
| image          = Comin_Round_the_Mountain_1936_Poster.jpg
| caption        = Theatrical release poster
| director       = Mack V. Wright
| producer       = Nat Levine
| screenplay     = {{Plainlist| Oliver Drake
* Dorrell McGowan
* Stuart E. McGowan
}}
| story          = Oliver Drake
| starring       = {{Plainlist|
* Gene Autry
* Ann Rutherford
* Smiley Burnette
}}
| music          = Harry Grey (supervisor)
| cinematography = William Nobles
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 54 minutes
| country        = United States
| language       = English
}} Western film Oliver Drake, the film is about a Pony Express rider who is robbed and left to die in the desert, where he is saved by a wild horse he captures and later uses to round up other horses to be used in the race for a government contract.   

==Plot==
While delivering mail in California in 1880, Pony Express rider Gene Autry (Gene Autry) is ambushed by two men who steal his saddlebags and leave him to die in the desert. In addition to the mail, the saddlebags contain money being sent to ranch owner Dolores Moreno (Ann Rutherford) who desperately needs the funds to save her land from being sold for back taxes. At Dolores Vista Grande ranch, Marshal John Hawkins (Robert McKenzie) posts a notice that the ranch will soon be auctioned off.

After selling her cattle to raise the necessary money for the taxes, Dolores plans to breed horses, using a wild stallion named El Diablo to start a good bloodline. One of her neighboring ranchers, Matt Ford (LeRoy Mason), watches as Dolores men try to break the stallion. Ford, who has been breeding thoroughbreds, expects to sell his horses to Pony Express horse buyer Caldwell (Raymond Brown). He offers to loan Dolores money if her cattle money does not arrive in time. She doesnt know that it was Ford who sent his henchmen, Slim and Butch, to steal her money from the Pony Express rider so she would be forced to marry him and he could gain control of her ranch.

After breaking free, El Diablo bolts into the desert, where the lariat around his neck gets caught in some rocks. Gene comes across the wild stallion and frees him. The next day he is able to break the stallion to the saddle and rides him to the Vista Grande ranch, where he tells Dolores and Caldwell about the robbery. Knowing the thieves must have had knowledge of the money in those saddlebags, Gene grows suspicious of Ford. He asks Caldwell to be relieved of duty while he searches for the culprit. Ford orders Slim and Butch to kill Gene, but their attempt to shoot him fails.

Gene tells Dolores that they can raise the money she needs by breaking her wild mustangs and selling them to Caldwell. Gene sends El Diablo back to the herd to help control it, and while the horse fights with a pinto for leadership of the herd, Gene rushes to Fords house and prevents him from signing a contract with Caldwell. Ford agrees to race twenty of his horses against twenty of Genes horses, with the winner getting the contract. During the next few days, Gene and his men spend long hours breaking and training the wild mustangs. One night, Fords men free the horses from their corral, leaving only El Diablo and four other horses. Ford also captures the pinto (El Diablos nemesis) and paints it to resemble a thoroughbred.

Undaunted by the loss of his horses, Gene still enters the race with El Diablo. As the race begins, Fords men quickly cheat their way into first place. Ford is in the lead and switches to the painted pinto during the final stretch. Angered by the presence of his pinto rival, El Diablo catches up and carries Gene to victory. After the race, Gene exposes Fords trick, and Frog reveals that he found Butch on the trail with Dolores money. After Butch confesses that Ford was behind the scheme, the men are taken away. Sometime later, riders at the Pony Express station sing a song to congratulate Gene and Dolores on their recent marriage.

==Cast==
 
* Gene Autry as Gene Autry
* Ann Rutherford as Dolores Moreno
* Smiley Burnette as Frog
* LeRoy Mason as Matt Ford
* Champion as Gene Autrys Horse
* Raymond Brown as Caldwell
* Ken Cooper as Henchman Slim
* Tracy Layne as Henchman Butch
* Robert McKenzie as Marshal John Hawkins
* Laura Puente as Dancer   

==Production==
===Stunts=== 
* Ken Cooper
* Joe Yrigoyen 

===Filming locations===
* Alabama Hills, Lone Pine, California, USA
* Olancha Dunes, Olancha, California, USA Whitney Portal Road, Lone Pine Creek Canyon, Lone Pine, California, USA   

===Soundtrack===
* "Shell Be Comin Round the Mountain" (Traditional) played during the opening credits
* "Shell Be Comin Round the Mountain" (Traditional) by Gene Autry and Cowboys
* "Shell Be Comin Round the Mountain" (Traditional) by Gene Autry, Artie Ortego, and others at the end
* "La Cucaracha" (Traditional Spanish folk song) by a band at the ranch and danced by Laura Puente
* "Don Juan of Sevillio" (Sam H. Stept, Oliver Drake) by Smiley Burnette and others at the ranch
* "Chiquita" (Sam H. Stept, Oliver Drake) by Gene Autry (vocal and guitar)
* "When the Campfire Is Low on the Prairie" (Sam H. Stept) by Gene Autry, Smiley Burnette, and Cowboys   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 