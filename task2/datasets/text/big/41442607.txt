Pathimoonam Number Veedu
{{Infobox film
| name           = Pathimoonam Number Veedu
| image          = 
| image_size     =
| caption        = 
| director       = Baby
| producer       = Aasha Creations
| writer         = Baby Erukoor Elavarasan  (dialogues) 
| starring       =  
| music          = Sangeetha Rajan
| cinematography = K. B. Dhayalan C. E. Babu
| editing        = G. Murali
| distributor    =
| studio         = Aasha Creations
| released       =  
| runtime        = 110 minutes
| country        = India
| language       = Tamil
}}
 1990 Tamil Tamil horror Sadhana and Lalitha Kumari in lead roles. The film, produced by Aasha Creations, had musical score by Sangeetha Rajan and was released on 15 June 1990. Became a blockbuster hit and It was remade in Hindi as House No. 13 (1991) directed by Baby.    

==Plot==

Selvam (Nizhalgal Ravi), an estate manager, and his family move into a new house based in a village. There, Selvam falls in love with Annam (Shari (actress)|Sadhana), a jolly village girl. Disturbed by a ghost (Lalitha Kumari), Selvams grandfather dies from an heart attack. One day, Murali (Jaishankar), Selvams brother, goes to an isolated home and treats an old lady. The next day, he realize that the old lady was a ghost and he also dies from an heart attack. The family, in mourning, decides to arrange Selvams wedding with Annam to forget this tragedy. Thereafter, Annam becomes pregnant and even after 10 months she doesnt deliver. A Gurukkal (Ra. Sankaran) comes to the aid of the family and feels that something slows her delivery. He prepares a ritual. Finally, the ghost appears and tells her past.

The ghost was Rekha. She lived happily with her husband (Ravikanth) but her husband had bad habits and also had lot of debts. Under pressures, he forced his wife Rekha to have a sexual relation with the house owner. She refused and killed the house owner. Her angry husband killed her in tour. As a ghost, she then killed her husband. Since that day, she killed every male who stayed in the house.

==Cast==

*Nizhalgal Ravi as Selvam Sadhana as Annam
*Lalitha Kumari as Rekha / Ghost
*Jaishankar as Murali
*Sripriya as Muralis wife
*Ravikanth as Rekhas husband
*Nalinikanth
*Achamillai Gopi as Muthu
*Ra. Sankaran as Swamy
*S. N. Parvathi as Parvathi, Selvam and Muralis mother
*Baby Manju as Manju

==Soundtrack==

{{Infobox album |  
| Name        = Pathimoonam Number Veedu
| Type        = soundtrack
| Artist      = Sangeetha Rajan
| Cover       = 
| Released    = 1990
| Recorded    = 1990 Feature film soundtrack |
| Length      = 10:09
| Label       = 
| Producer    = Sangeetha Rajan
| Reviews     = 
}}

The film score and the soundtrack were composed by film composer Sangeetha Rajan. The soundtrack, released in 1990, features 3 tracks with lyrics written by Pulaimaipithan.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! Track !! Song !! Singer(s) !! Duration
|- 1 || Koko Koko Koko || Suchithra || 4:28
|- 2 || Kandathu Ellam Mayyama || K. S. Chithra || 1:26
|- 3 || Kalyana Penne || Mano (singer)|Mano, S. P. Balasubrahmanyam || 4:15
|}

==References==
 

 
 
 
 
 
 
 