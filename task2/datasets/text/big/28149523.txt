Já, truchlivý bůh
 
{{Infobox film
| name           = Já, truchlivý bůh
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Antonín Kachlík
| producer       = 
| writer         = Antonín Kachlík Milan Kundera
| narrator       = 
| starring       = Milos Kopecký
| music          = 
| cinematography = Jan Nemecek
| editing        = Jaromír Janácek
| studio         = 
| distributor    = 
| released       = 17 October 1969
| runtime        = 82 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech comedy film directed by Antonín Kachlík. Based on stories from Milan Kunderas book Laughable Loves, it stars Milos Kopecký as Adolf, who relates a tale of spurned love to his friend Apostol (Pavel Landovský). Adolf has his friend to pose as an opera conductor to seduce the young woman (Hana Lelitová), as she loves opera, and then spurn her.

==Cast==
* Milos Kopecký - Adolf
* Hana Lelitová - Janicka Malátová
* Pavel Landovský - Apostol Certikidis
* Jiřina Jirásková - Mrs. Stenclová
* Ivana Mixová - Cantatrice
* Kvetoslava Houdlová - Oculist
* Pavla Marsálková - Mrs. Malátová - mother
* Zdenek Kryzánek - Mr. Malát - father
* Jirí Prichystal - Singer
* Ilona Jirotková - Girlfriend
* Daniela Pokorná - Girlfriend
* Boleslava Svobodová - Ruzena
* Milivoj Uzelac - Conductor
* Helena Bendová - Housekeeper
* Vladimír Klemens - Music-master

==External links==
*  

 
 
 
 
 
 


 
 