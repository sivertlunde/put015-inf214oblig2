The Party (film)
{{Infobox film
| name           = The Party
| image          = Party moviep.jpg
| image_size     =  Jack Davis
| director       = Blake Edwards
| producer       = Blake Edwards
| writer         = Blake Edwards Frank Waldman Tom Waldman
| starring       = Peter Sellers Claudine Longet Gavin MacLeod J. Edward McKinley Denny Miller Steve Franken Don Black Henry Mancini
| cinematography = Lucien Ballard
| editing        = Ralph E. Winters
| studio         = The Mirisch Corporation
| distributor    = United Artists
| released       =  
| runtime        = 99 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2,900,000 (US/ Canada) 
| preceded_by    =
| followed_by    =
}}

The Party is a   premise, in which a bungling Indian actor accidentally gets invited to a lavish Hollywood dinner party and "makes terrible mistakes based upon ignorance of Western ways." Lehman, Peter, Luhr, William (1981). Blake Edwards, p. 140. Ohio University Press, ISBN 978-0-8214-0605-2  

The Party is considered a classic comedic cult film. Stafford, Jeff. neorealist comedy, purposefully lacking a directors guiding eye: look here, look there. The screen is crammed full of activity, and the audiences eyes are left to wander where they may." Austerlitz, Saul (2010). Another Fine Mess: A History of American Film Comedy, p. 198. Chicago Review Press, ISBN 978-1-55652-951-1 

==Plot== Gunga Din-style Indian actor set rigged with explosives. The director fires Bakshi immediately and calls the studio head, General Fred R. Clutterbuck (J. Edward McKinley), about the mishap. Clutterbuck writes down Bakshis name to blacklist him, but he inadvertently writes Bakshis name on the guest list of his wifes upcoming dinner party. 

Bakshi then receives his invitation and drives to the party. Upon arrival at Clutterbucks home, Bakshi tries to rinse mud off his shoe in a large pool that flows through the house, but he loses his shoe. After many failures, he is reunited with his shoe. 

Bakshi has awkward interactions with everyone at the party, including Clutterbucks dog Cookie. He meets famous Western movie actor "Wyoming Bill" Kelso (Denny Miller), who gives Bakshi an autograph. Bakshi later accidentally shoots Kelso with a toy gun, but Kelso does not see who did it. Bakshi feeds a caged macaw bird food from a container marked "Birdie Num Num" and accidentally drops the food on the floor. Bakshi at various times during the film activates a panel of electronics that control the intercom, a fountain replica of the Manneken Pis (soaking a guest), and a retractable bar (while Clutterbuck is sitting at it). After Kelso hurts Bakshis hand while shaking it, Bakshi sticks his hand into a bowl of crushed ice containing caviar. While waiting to wash his hand in the bathroom, he meets aspiring actress Michèle Monet (Longet), who came with producer C.S. Divot (Gavin MacLeod). Bakshi shakes Divots hand, and Divot then shakes hands with other guests, passing around the fishy odor, even back to Bakshi after he has washed his hand.

At dinner, Bakshis place setting right by the kitchen door has a very low chair that puts his chin near the table. An increasingly drunk waiter named Levinson (Steven Franken) tries to serve dinner and fights with the other staff. During the main course, Bakshis roast Cornish game hen accidentally catapults off his fork and becomes impaled on a guests tiara. Bakshi asks Levinson to retrieve his meal, but the womans wig comes off along with her tiara, as she obliviously engages in conversation. Levinson ends up brawling with other waiting staff, and dinner is disrupted.

Bakshi apologizes to his hosts, then needs to go to the bathroom. He wanders through the house, opening doors and barging in on various servants and guests in embarrassing situations. He ends up in the back yard, where he accidentally sets off the irrigation sprinkler|sprinklers. At Divots insistence, Monet gives an impromptu guitar performance of "Nothing to Lose," to impress the guests. Bakshi goes upstairs, where he saves Monet from Divots unwanted sexual advances by dislodging Divots toupee with the toy gun. Bakshi finally finds a bathroom, but he breaks the toilet, drops a painting in it, gets toilet paper everywhere, and floods the bathroom. To avoid being discovered Bakshi sneaks out on the roof and falls into the pool. Monet leaps in to save him, and hes coerced to drink alcohol to warm up. Bakshi is unaccustomed to alcohol, and he struggles to put on a dry red terry toweling jumpsuit. He finds Monet crying in the next room and consoles her. Divot bursts in and demands Monet leave with him. Monet says no, and Divot cancels her screen test for him the next day. Bakshi convinces her to stay and have a good time with him. They return to the party in borrowed clothes as a Russian dance troupe arrives. The party gets wilder, and Bakshi offers to retract the bar to make room for dancing. He accidentally opens a retractable floor with a pool underneath, causing guests to fall in the pool. Levinson makes more floors retract, and more guests fall in. Clutterbucks daughter arrives with friends and a baby elephant painted with hippie slogans. Bakshi takes offense and asks them to wash the elephant. The entire house is soon filled with soap bubbles from the cleaning. 
 Morgan three-wheeler car. Outside her apartment, Bakshi and Monet appear on the verge of admitting that they have fallen for each other. Bakshi gives Monet the hat as a keepsake, and she says he can come get it any time. Bakshi suggests he could come by next week, and she readily agrees. Bakshi smiles and drives off as his car Back-fire|backfires.

==Cast==
* Peter Sellers as Hrundi V. Bakshi
* Claudine Longet as Michele Monet
* Natalia Borisova as Ballerina
* Jean Carson as Nanny
* Marge Champion as Rosalind Dunphy
* Al Checco as Bernard Stein Corinne Cole as Janice Kane
* Dick Crockett as Wells
* Frances Davis as The Maid
* Danielle De Metz as  Stella DAngelo Herb Ellis as Director
* Paul Ferrara as Ronnie Smith
* Steve Franken as Levinson
* Kathe Green as Molly Clutterbuck
* Allen Jung as Cook
* Sharron Kimberly as Princess Helena
* James Lanphier as Harry
* Buddy Lester as Davey Kane
* Stephen Liss as Geoffrey Clutterbuck
* Gavin MacLeod as C.S. Divot
* Jerry Martin as Bradford
* Fay McKenzie as Alice Clutterbuck
* J. Edward McKinley as Fred Clutterbuck
* Denny Miller as Wyoming Bill Kelso
* Elyane Nadeau as Wiggy
* Thomas W. Quine as Congressman Dunphy
* Timothy Scott as Gore Pontoon
* Ken Wales as Assistant Director
* Carol Wayne as June Warren
* Donald R. Frost as Drummer
* Helen Kleeb as Secretary
* George Winters as Cliff Hanger
* Linda Gaye Scott as Starlet

Vin Scully is uncredited onscreen, but his voice can be heard announcing a Los Angeles Dodgers game on the kitchen radio. Koseluk, Chris (April 16, 2008). The voice of generations. The Hollywood Reporter 

==Production==
The Party was the only non-Pink Panther collaboration  between Sellers and Edwards. Producer Walter Mirisch knew that Sellers and Edwards were considered liabilities; in his autobiography, Mirisch wrote, "Blake had achieved a reputation as a very expensive director, particularly after The Great Race."     Sellers had played another Indian man in his hit film The Millionairess, and a similar klutz as Inspector Clouseau.

The films interiors were shot on a set, at the MGM lot. The original script was only 63 pages in length.  Edwards later said it was the shortest script he ever shot from, and the majority of the content in the film was improvised on set.

The film draws much inspiration from the works of   

==Release==
The film was released on April 4, 1968, the same day as the assassination of Dr. Martin Luther King, Jr..

==Cultural influence in India== Indian Prime Minister Indira Gandhi was very fond of repeating Bakshis line, "In India we dont  think  who we are, we  know  who we are!", the characters reply to a hostile "who do you think you are?"          

==Soundtrack== Nothing to It Had Pink Panther film. The compact disc was originally released on August 20, 1995 by BMG Victor.

Track listing
#"The Party"   2:14
#"Brunette in Yellow" 2:56
#"Nothing to Lose  " 3:18
#"Chicken Little Was Right" 2:54
#"Candleleight On Crystal" 3:05
#"Birdie Num-Num" 2:21
#"Nothing To Lose  " 2:25
#"The Happy Pipers" 2:17
#"Party Poop" 2:34
#"Elegant" 4:44
#"Wiggy" 3:02
#"The Party  " 3:12

==References==
 

==External links==
 
*  
*  
*  


 

 
 
 
 
 
 
 
 