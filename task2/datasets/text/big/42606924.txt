Picket 43
{{Infobox film
| name           = Picket 43
| image          = Picket43.jpeg
| alt            =
| caption        = Theatrical poster
| director       = Major Ravi
| producer       = OGEE
| writer         = Major Ravi Prithviraj Javed Jaffrey
| narrator       = Mohanlal
| music          = Songs:  
| lyrics         = Rajeev Nair
| cinematography = Jomon T. John
| editing        =Ranjan Abraham 
| studio         =Film Brewery 
| distributor    = Murali Films
| released       =  
| runtime        =Murali Films 
| country        = India Malayalam
| budget         =
| Gross          =   
}}
 Prithviraj and Javed Jaffrey in the lead roles. It was filmed by cinematographer Jomon T. John and most portions of the film were shot in Kashmir. The film dealt with the story of an Indian army soldier guarding a picket alone in Kashmir, and his friendship with a Pakistani soldier. The film released on January 23, 2015 to mixed reviews from critics and audience alike. 

==Plot==
Hareendranath Nair (played by Prithviraj) is an Indian soldier guarding an Indian picket in Kashmir.  He happens to meet a Pakistani soldier Mushraff (played by Javed Jaffrey) on one fine day. They immediately relate to each other and despite of being from different countries they seem to understand each other better than anyone else. They eventually become close friends and start to enhance the morale of each other. However, their friendship raises suspicions among others. They even put into question the genuineness of their patriotism which break their heart. They find themselves in nether world and the story proceeds to tell the hardship they have to go through for their friendship.  It is not just with the Pakistani soldier he shares special bond, he also considers a dog by the name Baccardi as his companion. He even does not permit anyone to call him dog.  He confides in the dog his inner thoughts and emotions which lifts his spirit slightly even at the most difficult times.

Overall, instead of focusing on devastating wars and the like, the movie deals more with the psychological trauma a soldier has to go through in order to safeguard his motherland.  It also throws light on the fact that friendship is not based on certain rules or conventions and that the key factor for a true friendship is to have a better understanding and compassion for each other. It also explains how a faithful dog can turn out to be your confidante when you feel alone and dejected.

==Cast== Prithviraj as Havildar Harindran
* Javed Jaffrey as Mushraff
* Renji Panicker as C.O Vinay Chandran
* Sudheer Karamana as Vaasu
* Kannan Nair
* Haresh Peradi as Lakshmis father
*angana roy as Hanara
* Anu Mohan as Dineshan
* Anshu Sharma as Lakshmi
*Meghanathan as Thampi
*Shobha Mohan as Haris mother

==Casting==
Initially Ravi developed the film with Mohanlal in mind for the lead role as a reboot to the Major Mahadevan film series. But when approached, he was reluctant to take the role, because, Lal found the character not matching with his age, and said it suits more for a younger actor, he also doubted how people accept a Havildar Mahadevan who was in much higher ranks in earlier films. And when asked he suggested Prithviraj Sukumaran|Prithvirajs name.  

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 