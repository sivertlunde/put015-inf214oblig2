Necromania
{{Infobox film
| name = Necromania
| image =
| image_size =
| caption =
| director = Ed Wood
| producer = Ed Wood
| writer = Ed Wood
| narrator =
| starring = Maria Arnold Rene Bond
| music =
| cinematography =
| editing = Ed Wood
| distributor = Stacey Distributors
| released =
| runtime = 51 min. (R-rated) 54 min. (X-rated)
| country = United States English
| budget = $5,000
}}
Necromania (sometimes subtitled A Tale of Weird Love) is a pornographic film by Edward D. Wood, Jr., released in 1971.

==Production and rediscovery==
Ed Wood produced, wrote, and directed the film under the pseudonym "Don Miller". Craig (2009), pp. 242–251  The title seems to imply necrophilia, but the content implies an obsession with Death (personification)|Death.  The film was based on the novel The Only House (1970), also written by Wood. Rob Craig observes that certain elements of the original story were "slavishly" adapted, while others were altered or removed in their entirety.  For example, in the novel the rituals of sex magic are depicted in detail, and the Carpenters are not lovers posing as a married couple. They are in fact married. 

The film was shot on a budget of 7,000 dollars.  According to Charles Anderson, a Wood collaborator, the director himself played a role in the film. Anderson recalled this role to be a wizard or an evil doctor. However, no such role appears in the finished film. Craig suspects it was included in a deleted scene. 

The film was an early entry to the new subgenre of  , and The Opening of Misty Beethoven (1976) by Radley Metzger. As a narrative-driven film, Rob Craig argues that Necromania can also be considered part of the Golden Age of Porn, along with these films. 
 Something Weird Fleshbot Films in 2005. Opening titles indicate "Produced & directed by Don Miller. Our cast wish to remain anonymous."

==Plot==
Before the credits, the film opens to an image seen through a prism. It depicts a group of naked, writhing bodies in the process of group sex. The prism replicates the image, so several versions are seen in a single frame.  The credits are followed by a scene opening in a suburban area of California. A car is seen driving around, the passengers presumably looking for something. They stop before an old mansion, then the camera shifts to the image of door knocker depicting a lions head. The young couple knocks first, then enters through the unlocked door. They bicker over the decision to enter unannounced. The young man then jokes about the creepy location, saying that "Any minute, I expect Bela Lugosi as Dracula" to appear. 
 necromancer Madame Heles (pronounced "heals") for a witchcraft solution to Dannys erectile dysfunction. Tanya leads them to a room prepared for their stay. A dildo serves as the ringer of the room.  When left alone, the Carpenters resume bickering over their sexual dysfunction. They fail to notice feminine eyes watching them through the holes in a nearby painting—Tanyas eyes. 

Tanya ends her surveillance and returns to the room with the coffin. She picks up a skull and uses it to rub her body. Besides achieving sexual stimulation, this is implied to be a ritual of sex magic. Speaking to the coffin, Tanya informs someone that their suspicions were correct. The Carpenters are not married. The significance of this information is not explained.  Tanya leaves the room and encounters a man called Carl, who demands to have sex with her. Claiming that he paid plenty to be the first to have her. Tanya makes clear that she does not have to service him. But out of pity for his need, she chooses to do so anyway. An explicit sex scene follows. 
 stuffed wolf lesbian sex between the two young women. 

In the bedroom, Danny wakes up from nap to find himself alone and his penis at rest. He decides to head out to search for Shirley. Elsewhere, Barb and Shirley have moved their lovemaking to another bedroom. Danny instead meets Tanya who leads him to yet another bedroom and seduces him. Two parallel sex scenes follow. The lesbian one is depicted as mutually satisfying, while the heterosexual only manages to benefit the male partner.  Following that, Tanya leads Danny to a window. Once again group sex is seen through a prism. Tanya explains that not all people react to "the treatment" successfully. The people depicted through the window are those who will never find satisfaction in their sexual lives, as some want too much and other too little. Suddenly self-conscious, Danny realizes that his own reaction to the treatment was not the proper one.  Tanya assures him that he is not like them, since they are lost forever. They can never return to a world which will reject them. 

Next, Tanya and Barb lead their lovers to the room with the coffin. Danny and Shirley seem hostile to each other, implying that their relationship is doomed.  Tanya and Barn kneel before the coffin and then strip each other. They engage in sex before their audience. In reaction Shirley swoons, while Danny groans in displeasure. The sexual ritual summons Madame Heles from her coffin.  Heles asks about the progress of her two newest students. Barb praises the learning of Shirley in sex, in response, Heles proclaims that Shirley will henceforth live for sex alone. Barb explains that Shirley has graduated. 

As Shirley walks away with Barb, Danny is left behind. Tanya declares that they still have somework to do on him. In response, Heles proclaims that he needs her personal sex teachings. While she waits in her coffin, Barb and Karl enter the room. They help Tanya restrain Danny and strip off his clothes. They force the young man to enter the coffin of Heles and then depart. At first Danny screams, but then he is seen enjoying his healing session with the attractive Heles. The film ends. 

==Cast==
* Maria Arnold as Madame Heles
* Rene Bond as Shirley
* Ric Lutze as Danny

==Analysis==
Wood probably included the reference to Bela Lugosi as a tribute to an old friend. 

The front door is decorated with the image of a trident. Rob Craig suggests that it can also be seen as the pitchfork of a devil. 

The spying eyes, seen through a painting are part of a trope derived from films featuring haunted houses. 

Craig sees the group sex sessions seen through the prism as a depiction of the then-ongoing sexual revolution. 

==Behind the scenes==
A coffin owned by The Amazing Criswell is seen in the film, the second of Woods films (after Night of the Ghouls) in which such a coffin appears.  Criswells family was in the mortician business. The coffin used in Necromania, however, looks antique. According to cinematographer Ted Gorley, this was the result of a misunderstanding. Criswell had meant to donate his own coffin, but the crew of the film borrowed the wrong coffin. The one used in the film was a relic dating to the presidency of Abraham Lincoln (1861–1865). 
 Vampira on TV and in Plan 9 from Outer Space, tells how she declined Woods request for her to do a nude scene sitting up in a coffin in the role of Madame Heles. 

==Rediscovery==
The film magazine Cult Movies (issue #36) printed a detailed article about the rediscovery of Woods Necromania and The Only House in Town. The piece was written by Rudolph Grey, author of the Wood biography Nightmare of Ecstasy.

==References==
 

== Sources ==
*  
* Rudolph Grey, Nightmare of Ecstasy: The Life and Art of Edward D. Wood, Jr. (1992) ISBN 978-0-922915-24-8
* The Haunted World of Edward D. Wood, Jr. (1996), documentary film directed by Brett Thompson

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 