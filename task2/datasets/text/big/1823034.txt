The Women on the Roof
{{Infobox film
| name           = The Women on the Roof
| image          =
| caption        =
| director       = Carl-Gustav Nykvist
| producer       = Waldemar Bergendahl
| writer         = Carl-Gustav Nykvist Lasse Summanen
| starring       = Amanda Ooms Helena Bergström
| music          =
| cinematography = Ulf Brantås
| editing        = Lasse Summanen
| distributor    =
| released       =  
| runtime        = 89 minutes
| country        = Sweden
| language       = Swedish
| budget         =
}}
 Best Actor.   

==Plot==
An innocent and beautiful girl, Linnea (Amanda Ooms) rents an apartment in Stockholm just before World War I. As she works for an old man who owns a photography studio she meets Anna, a photographer (Helena Bergstrom) with whom she develops a complex friendship. Anna’s circus-performer boyfriend and European politics complicate Linnea’s routine.

==Cast==
* Amanda Ooms - Linnea
* Helena Bergström - Anna
* Stellan Skarsgård - Willy
* Percy Brandt - Fischer
* Lars Ori Bäckström - Holger
* Katarina Olsson - Gerda
* Leif Andrée - Oskar
* Stig Ossian Ericson - Vicar Johan Bergenstråhle - Photographer Halling

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 

 