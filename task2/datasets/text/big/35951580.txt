Playing with Fire (1921 film)
For 1921 Universal film of the same name, see Playing with Fire (1921 Universal film)
{{Infobox film
| name           = Playing with Fire
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene   Georg Kroll
| producer       = Erich Pommer
| writer         = Alexander Engel   Julius Horst
| narrator       =  Hans Junkermann
| music          = 
| editing        = 
| cinematography = Fritz Arno Wagner
| studio         = Decla-Bioscop
| distributor    = Decla-Bioscop 
| released       =  
| runtime        = 
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
}} German silent silent comedy film|comedy-drama film directed by Georg Kroll and Robert Wiene and starring Diana Karenne, Vasilij Vronski, Ossip Runitsch and Anton Edthofer. A method actress likes living out the roles she is playing in real life. To prepare for her new play, she enters the criminal underworld and ends up being implicated in a burgalry of a Duke who is one of her suitors.  The film received a generally positive reception from critics, although some were doubtful about the blending of farce and tragedy. 

==Cast==
* Diana Karenne   
* Vasilij Vronski 
* Ossip Runitsch
* Anton Edthofer    Hans Junkermann   
* Emil Biron   
* Viktor Blum   
* Leonhard Haskel   
* Emil Heyse   
* Max Kronert   
* Karl Platen      
* Lucia Tosti   
* Otto Treptow

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 