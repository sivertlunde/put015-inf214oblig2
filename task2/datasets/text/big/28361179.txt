Kassbach – Ein Porträt
 
{{Infobox film
| name           = Kassbach – Ein Porträt
| image          = 
| caption        = 
| director       = Peter Patzak
| producer       = Peter Patzak
| writer         = Peter Patzak Helmut Zenker
| narrator       = 
| starring       = Walter Kohut
| music          = 
| cinematography = Dietrich Lohmann
| editing        = Trude Gruber
| distributor    = 
| released       =  
| runtime        = 110 minutes
| country        = Austria
| language       = German
| budget         = 
}}

Kassbach – Ein Porträt is a 1979 Austrian drama film directed by Peter Patzak. It was entered into the 29th Berlin International Film Festival.   

==Cast==
* Walter Kohut - Karl Kassbach
* Immy Schell - Kassbachs wife
* Konrad Becker - Kassbachs son
* Walter Davy - Head of the Initiative
* Franz Buchrieser - Kassbachs friend
* Hanno Pöschl - Kassbachs friend
* Heinz Petters - Post office clerk
* Ulrich Baumgartner - Anwerber
* Erni Mangold - Liesis mother
* Maria Engelstorfer - Kassbachs mother
* Heinrich Strobele - Kassbachs colleague

==References==
 

==External links==
* 

 
 
 
 
 
 
 