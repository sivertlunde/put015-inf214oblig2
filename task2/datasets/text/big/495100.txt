Dororo
 
{{Infobox animanga/Header
| name            = Dororo
| image           =  
| caption         = Cover of Dororo volume 4 from the Osamu Tezuka Manga Complete Works edition.
| ja_kanji        = どろろ
| ja_romaji       = 
| genre           = Adventure, Historical, Supernatural
}}
{{Infobox animanga/Print
| type            = manga
| author          = Osamu Tezuka
| publisher       = Shogakukan
| demographic     = Shōnen manga|Shōnen
| magazine        = Weekly Shōnen Sunday
| first           = 27 August 1967
| last            = 22 July 1968
| volumes         = 4
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = tv series
| director        = Gisaburō Sugii
| producer        = 
| writer          = 
| music           = 
| studio          = Mushi Productions
| network         = Fuji TV
| first           = April 6, 1969
| last            = September 28, 1969
| episodes        = 26
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = live film
| director        = Akihiko Shiota
| producer        = 
| writer          = 
| music           = 
| studio          = 
| released        = 2007
| runtime         = 
}}
 
 manga creator Osamu Tezuka in the late 1960s. The anime television series (1969) based on the manga consists of 26 half-hour episodes. It was made into a Dororo (film)|live-action film in 2007.
 goblins was popular among kids. Dororo was serialized in Weekly Shōnen Sunday for three years.

Tezukas childhood memory of his friends pronouncing   as dororo inspired the title of this work.  In the live action movie series, the name is explained to be a southern term for Hyakkimaru, meaning "Little Monster."

The anime series bears the distinction of being the first entry in what is now known as the "World Masterpiece Theater" series.

==Story==
Dororo is a thriller manga, which revolves around a ronin during the Sengoku period. He was born malformed, limbless and without facial features or internal organs. This was the result of his birth father Daimyo Daigo Kagemitsu forging a pact with 48 sealed demons so that he might rule the world. In return, he promised the demons could each obtain a piece of his unborn childs body. This enabled them to roam free and commit atrocities along the countryside.

After his mother was forced to set him adrift on the river, lest he be killed by his father, the infant was subsequently found and raised by Dr. Honma, a medicine man who used healing magic and alchemical methods to give the child prostheses crafted from the remains of children who had died in the war. The boy became nearly invincible against any mortal blow as a result of the prostheses and healing magic. Grafted into his left arm was a very special blade that a travelling storyteller presented to Dr. Honma, believing it was fated to be within his possession given that ever since the boy had been discovered, the doctor had been visited by goblins. As revealed in a short tale about the blades origin, the blade had been forged out of vengeance to kill goblins as well as other supernatural entities.

After the doctor was forced to send him on his way because he was attracting demons, the young man learned from a ghostly voice of the curse that had be set upon him at birth and that by killing the demons responsible he could reclaim the stolen pieces of his body and thus regain his humanity. Across his travels, he earned the name   among other names for his inhuman nature. On one such hunt of a demon, Hyakkimaru came across a young orphan thief named Dororo who thereafter travels by his side through the war-torn countryside. When Hyakkimaru met Dororo, he had already killed 16 demons.

Throughout their journey, Hyakkimaru killed 6 more demons, bringing the total to 22. Along the way, Hyakkimaru learns that Dororo was hiding a big secret. Dororos father, Bandit Hibukuro, hid money he saved up on his raids on Bone Cape to later be distributed to the people squeezed dry by the samurai. Itachi, a bandit who betrayed Hibukuro and sided with the authorities, crippled Hibukuro. Hibukuro escaped with limping legs, along with his wife and young Dororo. Hibukuro dies trying to let his family escape. Fearing that she will die, Dororos mother prayed to Buddha and, with her blood, drew the map that will lead him to Bone Cape. Three days later, she froze to death.
 boatman ferry them to the Cape but unbeknownst to them, he had two demon sharks with him. One of the sharks ate half of Itachis bandits while the other shark left with the boatman, leaving them stranded with a shark. With half of Itachis bandits dead from a demon shark, Dororo and the remaining bandits managed to kill the shark. The boatman and the second shark caught up with Dororo and the bandits. Dororo was able to separate the boatman and the shark. Hyakkimaru caught up with struggle and used his sword to stab the shark in one of its eyes. It escaped. They held the boatman as prisoner. Then they landed on Bone Cape.

As time flew by and night fell, the bandits were getting thirsty. The boatman told them of a spring not too far from their camp. The rest of the bandits — except for Itachi, Dororo, the boatman, and Hyakkimaru — left to drink from the well. Later, when Dororo went to check on them, he found corpses and blood leading to the half-blind shark. Dororo telepathically told Hyakkimaru of the deaths and he came, ready to kill the shark. Hyakkimaru killed the shark and the boatman and left them drift at sea. Hyakkimaru got his real voice back.

With the demons out of the way, Itachi went to search for the money but only found a letter, stating that Hibukuro thought Itachi might come looking for the treasure so he hid it somewhere else. As the sun rises, the Magistrate under the pretense of getting rid of the bandits but in actually, came for the treasure. Hyakkimaru, Dororo, and Itachi were able to kill them. Itachi was left for dead, and Hyakkimaru and Dororo continued on their journey.
 archers shoot and killed many. The remaining slaves hid in a tunnel they built under the fort.

Hyakkimaru forcefully separated Dororo and ran in the fort. Dororo was caught by the slaves and joined them in their ambush. Daigos soldiers caught Dororo. Daigo told Hyakkimaru to kill him and Daigo will gain his trust. Hyakkimaru acted as he is about to kill Dororo but turned around and threw his sword into the dark. The thing he stabbed was the physical manifestation of the 48 demons. Hyakkimaru was able to kill it but some of the demons managed to escape. With the slaves entering through the tunnel, they were able to open up the big entrance doors and attacked Daigos soldiers. Daigo, too weak from more than half of the demons dead, left with his wife to someplace far away.

Hyakkimaru parted with Dororo. He wanted Dororo, who was actually a female disguised as a male, to grow up to be a beautiful lady. Hyakkimaru also wanted Dororo to fight alongside the farmers against those in power because Dororos father was a farmer. Hyakkimaru gave his sword, the one that Dororo wanted throughout the series, to Dororo. Hyakkimaru will continue his journey alone and they will meet again when Hyakkimarus body is whole. They parted, with Dororo crying at the doors. It wasnt until 50 years later when the last of the 48 demons was slain.

==Characters==
{|class="wikitable"
! Character Name
! Japanese voice actor (Anime)
! Japanese voice actor (VG)
! English voice actor (VG)
|- Chris Murphy
|-
| Dororo (どろろ) || Minori Matsushima || Ikue Ohtani || Bret Walter
|-
| Daigo Kagemitsu (醍醐景光) || Gorō Naya || Akio Ōtsuka || Kevin Blackton
|- Kevin Miller
|- Adam Harrington
|-
| Biwa-hōshi (琵琶法師) || Junpei Takiguchi || ||
|-
| Mio (みお) || Reiko Mutō || Yuki Makishima || Evelyn Huynh
|}

==Adaptations==

===Film===
 
A life action film directed by Akihiko Shiota was released in 2007.

===Anime===
Unlike the manga, the anime version has a conclusive ending.  Anime Sols is currently crowd-funding the official streaming of the show. The first half of the show has reached its goal, and now the funding has continued for the second half.   However, Anime Sols has folded, and Discotek Media has picked up the slack, with an announcement of the show on DVD.

===Video game===
Developer Sega made a Dororo-based video game for the PlayStation 2 console in 2004. It was released in the United States and Europe under the title Blood Will Tell. The games artwork was done by renowned manga artist Hiroaki Samura. Dororo was not very successful commercially or critically. In fact, Dororo only had an average of 69% at Game Rankings, but it developed a cult following.

As a side note, Dororo is most often construed as a man, or young boy, often being called as such, but, in the final chapter, you see that he is in fact a she, after Hyakkimaru makes a comment one chapter before about how she will grow into a fine young woman. She does, when she is seen five years later.

===English translation===
In 2008, Vertical Inc. released an English translation of Dororo in three volumes.  In 2009, it won the Eisner Award in the "Best U.S. Edition of International Material—Japan" division. 

===Spin-offs===
In 2012, a manga crossover one-shot was created featuring Dororo and Dororon Enma-kun. In 2013, it was expanded into a full series. 

==See also==
 
*List of Osamu Tezuka anime
*List of Osamu Tezuka manga
*Osamu Tezuka
*Osamu Tezukas Star System

==References==
 

==External links==
*   &mdash; Official site for Tezuka Osamus works
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 