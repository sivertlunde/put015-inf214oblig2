The Johnsons
{{Infobox film
| name           = De Johnsons
| image          = The Johnsons.jpg
| image_size     =
| caption        = 
| director       = Rudolf van den Berg
| producer       = 
| writer         = Roy Frumkes, Rocco Simonelli
| narrator       = 
| starring       = Monique van de Ven, Esmée de la Bretonière, Kenneth Herdigein
| music          =   
| cinematography = 
| editing        = 
| distributor    = 
| released       =14 February 1992
| runtime        =103 minutes
| country        = Netherlands Dutch
| budget         = 
}} 1992 Netherlands|Dutch horror film thriller directed by Rudolf van den Berg. It also known under the title De Blaauw Sens Johnand Xianjing. The film was nominated for a Fantasy Film Award at the film festival in Oporto. It was the last Dutch horror film in the twentieth century.

==Plot ==
Victoria Lucas (played by Monique van de Ven) is a single mother with her teenage Emalee (played by Esmee de la Bretonière) in an apartment. Emalee is not normal, having come to life through test tube fertilization, under the assistance of Dr. Johnson. The doctor has not only made Emalee, but in secret also used the eggs of Victoria to make seven boys.

When Emalee is 14 years, her mother takes her on a camping holiday in the Biesbosch. From her 14th birthday, Emalee begins to suffer from nightmares, about seven men that are identical to fertilize her.

==Cast==
*Monique van de Ven as Victoria Lucas
*Esmée de la Bretonière as Emalee Lucas
*Kenneth Herdigein as Professor Keller
*Rik van Uffelen as de Graaf
*Otto Sterman as vader Keller
*Olga Zuiderhoek as Angela
*Nelly Frijda as Tante van Peter
*Miguel Stigter as Johnson 1, Bossie
*Diederik van Nederveen as Johnson 2, Dakkie
*Erik van Wilsum as Johnson 3, Tellie
*Marcel Colin as Johnson 4, Droppie
*Kees Hulst as Jansma
*Nathan Moll as Johnson 5, Kniffie
*Jan-Mark Wams as Johnson 6, Koppie
*Michel Bonset as Johnson 7, Kurkie

==Overview==
The films title is a reference to the surnames of the seven brothers, all born with the help of the American doctor Johnson. Originally, the idea of the movie "First Blood" called, referring to the first time that the character Emalee is unwell during the film.

== External links ==
*  

 
 
 
 
 
 
 


 
 