Ariyappedatha Rahasyam
{{Infobox film 
| name           = Ariyappedatha Rahasyam
| image          =
| caption        =
| director       = P. Venu
| producer       = Koshi Ninan John Philip Koshi Philip Raji George
| writer         =
| screenplay     = P. Venu
| starring       = Prem Nazir Jayan Jayabharathi Jose Prakash
| music          = M. K. Arjunan
| cinematography = Sivan
| editing        = K Narayanan
| studio         = Sargadhara Cine Arts
| distributor    = Sargadhara Cine Arts
| released       =  
| country        = India Malayalam
}}
 1981 Cinema Indian Malayalam Malayalam film,  directed by P. Venu and produced by Koshi Ninan, John Philip, Koshi Philip and Raji George. The film stars Prem Nazir, Jayan, Jayabharathi and Jose Prakash in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
*Prem Nazir as Vijayan
*Jayan as Raghu
*Jayabharathi as Geetha
*Jose Prakash as Sreedharan thampi Sadhana
*Prameela as Santha Janardhanan as Pappan
*Mala Aravindan as Gopi
*N. Govindankutty as Gopi
*Alummoodan as Andrews
* Kanakadurga as Sarojam
* Poojappura Ravi as Paramu Sadhana as Reetha

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaanana poykayil || K. J. Yesudas, Vani Jairam || P. Bhaskaran || 
|-
| 2 || Navarathna vilpanakkaari || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Vaasara kshethrathil nadathurannu || S Janaki || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 