D.O.A.: A Rite of Passage
{{Infobox film
| name           = D.O.A.
| image_size     =
| image	         = D.O.A.- A Rite of Passage FilmPoster.jpeg
| caption        =
| director       = Lech Kowalski
| producer       = Tom Forcade,  Lech Kowalski,  Mike...
| writer         = Lech Kowalski, Chris Salewicz
| narrator       = Generation X, The Rich Kids, Joe Strummer, Nancy Spungen
| music          = Sex Pistols
| cinematography =
| editing        = Val Kuklowsky
| distributor    =
| released       = 1980
| runtime        = 95 min.
| country        = United States
| language       = English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
}} 1980 rockumentary Generation X (with Billy Idol), The Rich Kids, the X-Ray Spex, and Sham 69, with additional music from The Clash, Iggy Pop, and Augustus Pablo. {{cite web
 | url        = http://www.imdb.com/title/tt0082226/maindetails 
 | title      = D.O.A. 
 | accessdate = 2008-02-22 
 | publisher  = Internet Movie Database 
}}  {{cite web
|url=http://www.dvdmaniacs.net/Reviews/A-D/doa.html 
|title=D. O. A. 
|accessdate=2008-02-22 
|last=Jane 
|first=Ian 
|date=2005-01-08 
|work=Reviews 
|publisher=DVD Maniacs  archiveurl = archivedate = 2008-01-30}} 

==Plot==
The film centers around the Sex Pistols 1978 tour of the United States which ended with the group breaking up. The tour was the only one the group played in the U.S. Film director Lech Kowalski followed them with handheld cameras through the clubs and bars of their seven-city Southern tour.  Mixing this with footage of other contemporary bands, trends in the fashion capitals, and punks of all shapes and colors, Kowalski created a grainy, stained snapshot of a movement at its peak, {{cite web
 | url        = http://www.tvguide.com/movies/doa/review/132669 
 | title      = D.O.A.: Review 
 | accessdate = 2008-02-22 
 | work       = Movies 
 | publisher  = TVGuide.com 
}}  showing how certain authority figures saw the movement as a threat.  

It features interview footage (including the famous interview of Sid Vicious and Nancy Spungen in bed), and behind the scenes shots from the tour as well as interviews with audience members who had strong and widely varied reactions to the group. {{cite web
|url=http://www.allmovie.com/cg/avg.dll?p=avg&sql=1:11918~T1 
|title=D.O.A.: A Right of Passage 
|accessdate=2008-02-22 
|last=Unterberger 
|first=Richie 
|work=Review 
|publisher=allmovie}}    
 Generation X with Billy Idol.   
 indie film was shot mostly in bars and clubs on 16mm film, and documented early years of punk from both in front of and behind the stage. 
 1981 film 2000 rockumentary film about the Sex Pistols directed by Julien Temple, and in the 2002 television series Hollywood Rocks the Movies: The 1970s. 

==DVD release== Region 0.

==Cast (in alphabetical order)==
 
* Stiv Bators as himself (The Dead Boys)
* Terry Chimes as himself (The Clash)
* The Clash as themselves
* Paul Cook as himself (Sex Pistols)
* The Dead Boys as themselves Generation X as themselves
* Jonathan Guinness as himself
* Topper Headon as himself (The Clash)
* Billy Idol as himself (Generation X)
* Tony James as himself (Generation X) Mick Jones as himself (The Clash) Steve Jones as himself (Sex Pistols)
* John Lydon as himself (Johnny Rotten)
* Glen Matlock as himself (The Rich Kids)
* Gene October as himself (Generation X)
 
* Augustus Pablo as himself
* Bernard Brooke Partridge as himself - Council Member
* Rich Kids as themselves
* Heidi Robinson as herself - Tour Manager
* Sex Pistols as themselves
* Sham 69 as themselves
* Paul Simonon as himself (The Clash)
* Nancy Spungen as herself
* Joe Strummer as himself (The Clash)
* Terry Sylvester as himself
* Terry and the Idiots as themselves
* Sid Vicious as himself
* Mary Whitehouse as herself - Anti-Smut Crusader
* X-Ray Spex as themselves
 

==Songs performed==
The musical performances/tracks contained in the documentary are as follows:

# "Nightclubbing"  Written by Iggy Pop and David Bowie;  performed by Iggy Pop Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols
# "Oh Bondage Up Yours"  Written by Poly Styrene;  performed by X-Ray Spex God Save the Queen" Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols
# "Pretty Vacant"  Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by The Rich Kids
# "Liar" Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols
# "Police and Thieves" Written by Lee "Scratch" Perry and Junior Murvin;  performed by The Clash (CBS Records) Generation X (Chrysalis Records)
# "I Wanna Be Me" Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols Lust for Life"  Written by Iggy Pop and David Bowie;  performed by Iggy Pop
# "All This And More" Performed by The Dead Boys (Sire Records);  recorded live by Joe Sutherland
# "Pretty Vacant" Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols
# "No Fun" – Sex Pistols
# "New York" Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols Roundhouse Studios
# "Borstal Breakout" Performed by Sham 69;  recorded live at Roundhouse Studios
# "Holidays in the Sun" – Sex Pistols
# "Holidays in the Sun" Written by Paul Cook, Steve Jones, John Lydon and Sid Vicious;  performed by The Sex Pistols
# "E.M.I." Written by Paul Cook, Steve Jones, Glen Matlock and John Lydon;  performed by the Sex Pistols
# "Bodies (Sex Pistols song)|Bodies" Written by Paul Cook, Steve Jones, John Lydon and Sid Vicious;  performed by the Sex Pistols
# "A. P. Special" Written and performed by Augustus Pablo

==References==
 

==External links==
*   at  
*   at  
*  
*   at  
*   at  

 

 
 
 
 