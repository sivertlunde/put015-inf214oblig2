List of Kannada films of 2005
{{multiple issues|
 
 
}}

 

==Commercially successful films of year 2005==

* Aakash (film)|Aakash -Blockbuster

* Ayya (2005 Kannada film)|Ayya - Hit

* Amrithadhare - Blockbuster

* Namma Basava - Super Hit

* Anna Thangi - Super Hit
 Auto Shankar - Hit

* Deadly Soma - Super Hit

* Gowramma - Super Hit

* Shastri - Hit

* Jogi (film)|Jogi - Industry Record

* Nenapirali - Super Hit

* Rama Shama Bhama - Super Hit

* Rishi (2005 film)|Rishi - Super Hit

* Rakshasa (film)|Rakshasa - Hit

==List of released films in 2005==

=== January–June ===
{| class="wikitable"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening
! style="width:13%;"|Title
! Director !! Cast !! Music Director !! Genre !! Notes 
|- January!
| rowspan="7"   style="text-align:center; background:#ffa07a; textcolor:#000;"| J A N
| style="text-align:center;"| 7th
| Nanage Neenu Ninage Naanu || Anitha Subramanya || Rakesh  || Praveen D Rao || Romance || 
|-
| rowspan="2" style="text-align:center;"| 14th
| Maharaja (2005 film)|Maharaja || Om Sai Prakash || Sudeep, Nikita Thukral, Ashok (Kannada actor)|Ashok|| S. A. Rajkumar || Action || 
|- Meena || Sri Sailam || Drama ||
|-
| rowspan="3" style="text-align:center;"| 21st Prakash || Shivarajkumar, Vijay Raghavendra, Radhika Kumaraswamy, Sindhu Tolani || Gurukiran || Comedy / Drama ||  
|-
| Zabardast || Ram Bharath Devan || Bhushan Kumar, Rashmi Kulkarni ||  Gandharva || Romance  ||
|-
| Olave || P H Vishwanath || Vishal Hegde, Sandhya || B R Hemanth || Romance ||
|-
| style="text-align:center;"| 28th
| Surya The Great || Rushi || Ajay Rao, Shivani || Samson || Action || 
|- February!
| rowspan="5"   style="text-align:center; background:thistle; textcolor:#000;"| F E B
| style="text-align:center;"| 4th
| Yeshwanth || Dayal Padmanabhan || Sri Murali, Rakshita, Ramesh Bhat || Mani Sharma || Romance || 
|-
| rowspan="2" style="text-align:center;"| 11th
| Ayya (2005 Kannada film)|Ayya || Om Prakash Rao|| Darshan (actor)|Darshan, Rakshita || V. Ravichandran || Action ||
|-
| Encounter Dayanayak || D. Rajendra Babu || Sachin, Spoorthi || R. P. Patnaik || Action / Crime ||
|-
| rowspan="2" style="text-align:center;"| 18th
| Shadyantra || Rajesh Murthy || Harish Raj, Namratha  || Prakash Sontakki || Drama ||
|-
| Udees || P N Satya || Mayoor Patel, Sonali Joshi || Venkat Narayan || Action || 
|- March!
| rowspan="9"   style="text-align:center; background:#98fb98; textcolor:#000;"| M A R
| rowspan="5" style="text-align:center;"| 4th
| Beru (film)|Beru || P. Sheshadri || Suchendra Prasad, Neetha, H G Dattatreya, T. N. Seetharam|| Praveen Kiran || Drama / Documentary || 
|-
| Aadi (2005 film)|Aadi || M S Ramesh || Auditya, Ramya, Avinash || Gurukiran || Action || 
|- Bhavana || Hamsalekha || Drama || Guinness record film
|-
| Hendathiyobbalu Maneyolagiddare || V Umakanth || Krishne Gowda, Sharan (actor)|Sharan, Namratha, Doddanna  || Abhi || Comedy || 
|-
| Gadipaar || S S David || Charanraj, Vinod Alva || M S Maruthi || Action ||
|-
| rowspan="2" style="text-align:center;"| 18th
| Moorkha || A N Jayaramaiah || Kashinath (actor)|Kashinath, Namratha, Bhavya, Bank Janardhan, Karibasavaiah || V. Manohar || Comedy ||
|-
| Inspector Jhansi || H Vasu || Prema (actress)|Prema, Anand  || Dilip Sen - Sameer Sen || Action || 
|-
| rowspan="2" style="text-align:center;"| 25th
|  Ranachandi || B R Keshav || Prema (actress)|Prema, Naveen Mayur, Shobharaj || M S Maruthi || Action || 
|-
| Rakshasa (film)|Rakshasa || Sadhu Kokila || Shivrajkumar, Gajala, Ruthika, Kishore (actor)|Kishore, Avinash, Rangayana Raghu || Sadhu Kokila || Action ||
|- April!
| rowspan="6"   style="text-align:center; background:#ffdead; textcolor:#000;"| A P R
| rowspan="2" style="text-align:center;"| 1st
| Jootata || N S Shankar || Sameer Dattani|Dhyan, Richa Pallod, Akash, Rangayana Raghu, Sanket Kashi || R. P. Patnaik || Comedy || Remake of Malayalam film Poochakkoru Mookkuthi  
|- Koti || Action ||  
|-
| style="text-align:center;"| 8th
| Preethisu || Siddaraju || Vishal Hegde, Ashwini || Teja || Romance || 
|-
| style="text-align:center;"| 15th
| Navabharathi || K Ganesh || Sourav, Usha Kiran || Raj Bhaskar || Romance / Drama || 
|-
| style="text-align:center;"| 22nd
| Varsha  || S. Narayan || Vishnuvardhan (actor)|Vishnuvardhan, Ramesh Aravind, Manya (actress)|Manya, Komal Kumar, Anu Prabhakar, Doddanna || S. A. Rajkumar || Drama || Remake of Malayalam film Hitler (1996 film)|Hitler  
|- 
| style="text-align:center;"| 29th Kishore || R. P. Patnaik || Action / Romance ||
|- May!
| rowspan="4"   style="text-align:center; background:#edafaf; textcolor:#000;"| M A Y
| style="text-align:center;"| 6th
| Karnana Sampathu || Shantaram Kanagal || Ambarish, Tara (Kannada actress)|Tara, Prakash Rai || Guru || Action || Re-released
|-
| style="text-align:center;"| 13th
| Gunna (film)|Gunna || Dwarki  || Mayoor Patel, Chaitra Hallikeri, Rangayana Raghu, Sudeep || Mahesh || Action ||
|-
| style="text-align:center;"| 20th
| Mr. Bakra || Vasanth || Jaggesh, Rohini, Srinivasa Murthy, Mukhyamantri Chandru || V. Manohar || Comedy ||  
|- 
| style="text-align:center;"| 27th
| Aham Premasmi || V. Ravichandran || V. Ravichandran, Balaji, Aarti Chabria || V. Ravichandran || Romance || 
|- June!
| rowspan="8"   style="text-align:center; background:#d9c1b0; textcolor:#000;"| J U N
| style="text-align:center;"| 3rd
| Gowramma || Naganna || Upendra, Ramya, Ramesh Bhat, Srinivasa Murthy|| S. A. Rajkumar || Drama || Remake of Telugu film Nuvvu Naaku Nachav
|-
| style="text-align:center;"| 10th
| Shastri || P N Satya || Darshan (actor)|Darshan, Manya (actress)|Manya, Bullet Prakash || Sadhu Kokila || Action || 
|-
| rowspan="3" style="text-align:center;"| 17th
| Nan Love Madthiya || Venkatswamy || Dileep Raj, Deepa, Ananth Nag || Hamsalekha || Romance ||
|-
| Magic Ajji || Dinesh Babu || Kushboo, Sudharani, Ramesh Bhat, Master Tejas || Aadhi || Fantasy / Horror ||  
|-
| Yaake || V Rajendra Kumar || Raghav, Divya Gowda || M N Krupakar || Mystery ||
|-
| rowspan="3" style="text-align:center;"| 24th Chakri || Comedy ||
|-
| Ugra Narasimha || Surya || Mohan Shankar, Madhu Sharma, Sharath Lohitashwa || Rajesh Ramanath ||  Action ||  
|-
| O Gulabiye || Narendra Babu || Vivek Beedu, Mallika, Suhasini || M N Krupakar || Romance ||
|}

===July – December===
{| class="wikitable"
|-  style="background:#b0e0e6; text-align:center;"
! colspan="2" | Opening
! style="width:13%;"| Title
! Director !! Cast !! Music Director !! Genre !! Notes  
|- July!
| rowspan="5"   style="text-align:center; background:#d0f0c0; textcolor:#000;"| J U L
| rowspan="2" style="text-align:center;"| 1st
| Sye (kannada film)|Sye || Arun Prasad || Sudeep, Kanika Subramaniam, Avinash, Pasupathy || Gurukiran || Action || 
|-
| Hudgeeru Saar Hudgeeru  || Lingaraj Kaggal || Mohan Shankar, Sangeetha Shetty || S P Chandrakanth || Comedy || 
|- 
| style="text-align:center;"| 8th
| Masaala || Dayal Padmanabhan || Sunil Raoh, Radhika Kumaraswamy, Vishal Hegde, Ramesh Bhat || Sadhu Kokila|| Romance / Comedy|| 
|-
| style="text-align:center;"| 22nd
| Siddu || Mahesh Sukhadhare || Sri Murali, Deepu, Tara Venu|Tara, Sihi Kahi Chandru || R. P. Patnaik || Action ||  
|-
| style="text-align:center;"| 29th
| Valmiki || M S Ramesh || Shivrajkumar, Hrishitaa Bhatt, Lakshmisri, Avinash, Srinivasa Murthy || Gurukiran || Action || 
|- August!
| rowspan="8"   style="text-align:center; background:#c2b280; textcolor:#000;"| A U G
| style="text-align:center;"| 4th
| News (film)|News || M K Maheshwar || Upendra, Renuka Menon, Reemma Sen, Nasser, Avinash || Gurukiran || Action ||  
|-
| style="text-align:center;"| 5th
| Mental Manja || Sai Sagar || Arjun, Sheetal Ramachandran, Srinath, Girija Lokesh || Sai Sagar || Drama ||
|-
| rowspan="3" style="text-align:center;"| 12th
| Thunta || Om Prakash Rao || Balaji, Ektha Khosla || V. Ravichandran || Romance ||
|-
| Siri Chandana || Rajkishore || Suryavardhan, Chethan || S. A. Rajkumar || Drama ||
|-
| Abhinandane || N Ramachandra Rao || Ramkumar, Sudharani, Anu, Yeshwanth || K. Kalyan || Drama ||
|-
| rowspan="2" style="text-align:center;"| 19th Prem || Shivrajkumar, Jennifer Kotwal, Arundhati Nag, Ramesh Bhat, Sanket Kashi || Gurukiran || Action / Drama || 
|-
| Green Signal || Eeshwar Balegundi || Ajay Rao, Ashitha, Vishal Hegde, Ashwini || Venkat Narayan || Romance ||
|-
| style="text-align:center;"| 26th
| Deadly Soma || Ravi Shrivatsa || Auditya, Rakshita, Tara Venu|Tara, Devaraj, Avinash || Sadhu Kokila || Action ||  
|- September!
| rowspan="9" style="text-align:center; background:wheat; textcolor:#000;"| S E P
| style="text-align:center;"| 2nd
| Live Band || B R Keshav || Nagesh Mayya, Sneha  || M N Krupakar || Adult drama ||
|-
| rowspan="3" style="text-align:center;"| 9th Auto Shankar || D. Rajendra Babu || Upendra, Shilpa Shetty, Radhika Kumaraswamy, Sudharani || Gurukiran || Action ||
|-
| Namma Basava || Veera Shankar || Puneeth Rajkumar, Gowri Munjal, Sudharani, Tara Venu|Tara, Kota Srinivasa Rao || Gurukiran || Action ||
|-
| Yauvana || Jayasimha Musuri || Nithin, Lakshmi Musuri || Gopi Krishna || Adult romance ||
|-
| rowspan="2" style="text-align:center;"| 16th
| Amrithadhare || Nagathihalli Chandrashekar || Sameer Dattani|Dhyan, Ramya, Bhavya, Avinash, Amitabh Bachchan || Mano Murthy || Drama / Romance ||
|-
| Hasina (film)|Haseena || Girish Kasaravalli || Tara Venu|Tara, Chandrahasa Alva || Issac Thomas Kottukapally || Drama || Based on novel by Banu Mushtaq
|-
| style="text-align:center;"| 23rd
| Boyfriend || D R Jana || Dileep Raj, Rathi (actress)|Rathi, Abhinayasri  || K M Indra || Romance / Drama || 
|-
| rowspan="2" style="text-align:center;"| 30th
| Swamy (film)|Swamy || M S Ramesh || Darshan (actor)|Darshan, Gayatri Jayaraman, Avinash, Rangayana Raghu || Gurukiran || Action ||
|- Konkani film
|- October!
| rowspan="3"   style="text-align:center; background:#ffb7c5; textcolor:#000;"| O C T
| style="text-align:center;"| 7th Tara || M N Krupakar || Action ||
|-
| style="text-align:center;"| 21st
| Shambhu || Dwarki || Sri Murali, Manya (actress)|Manya, Avinash, Rangayana Raghu || Ramesh Krishnan || Romance ||  
|-
| style="text-align:center;"| 24th
| Hai Chinnu || Shivaji Rao Ghorpade || Nithin, Mohini Patel || Gopi Krishna || Romance||
|- November!
| rowspan="5"   style="text-align:center; background:#ace1af; textcolor:#000;"| N O V
| style="text-align:center;"| 4th
| Anna Thangi || Om Sai Prakash || Shivrajkumar, Radhika Kumaraswamy, Sudharani, Deepu, Vishal Hegde, Doddanna || Hamsalekha || Family Drama || 
|-
| rowspan="2" style="text-align:center;"| 18th
| Nammanna || N Shankar || Sudeep, Anjala Zhaveri, Asha Saini, Ashish Vidyarthi, Kota Srinivasa Rao || Gurukiran || Action ||  
|-
| Lathi Charge || B Ramamurthy || Thriller Manju, Mohan Shankar || Gopi Krishna || Action||
|-
| rowspan="2" style="text-align:center;"| 26th
| Love Story || Bharati Kannan || Mayoor Patel, Tanu Roy, Komal Kumar, Nizhalgal Ravi, Pramila Joshai || S. A. Rajkumar || Romance || Remake of Hindi film Ek Duuje Ke Liye
|-
| Good Bad Ugly || Tiger Prabhakar || Tiger Prabhakar, Nisha || Tiger Prabhakar || Adult romance ||
|- December!
| rowspan="6"   style="text-align:center; background:#b8860b; textcolor:#000;"| D E C
| style="text-align:center;"| 2nd Prem Kumar, Varsha, Vidya Venkatesh, Naveen Krishna || Hamsalekha || Romance ||
|-
| style="text-align:center;"| 9th Sathi Leelavathi  
|-
| style="text-align:center;"| 16th
| Sakha Sakhi || Dayal Padmanabhan || Sunil Raoh, Chaya Singh, Doddanna, Vinayak Joshi, Sadhu Kokila || Sadhu Kokila || Comedy / Romance || Remake of Tamil film Thiruda Thirudi
|-
| style="text-align:center;"| 23rd Deva || Drama || Remake of Tamil film Ramanaa (film)|Ramanaa
|-
| rowspan="2" style="text-align:center;"| 30th
| Dr. B R Ambedkar || Sharan Kumar Kabbur || Vishnukanth, Tara Venu|Tara, Bhavya || Abhimann Roy || Biopic ||
|-
| Pandu Ranga Vittala || Dinesh Babu || V. Ravichandran, Prema (actress)|Prema, Rambha (actress)|Rambha, Shruti (actress)|Shruti, Komal Kumar || V. Ravichandran || Comedy ||
|}

==References==
 

==External links==
*   at the Internet Movie Database

 
 

 
 
 
 
 