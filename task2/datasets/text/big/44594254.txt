Jaana (film)
{{Infobox film
| name = Jaana
| image = 
| caption =
| director = Shivamani
| writer = Shivamani Kasthuri  Shruti
| producer = Gayathri Sa Ra Govindu
| music = Hamsalekha
| cinematography = R. Madhusudhan
| editing = Suresh Urs
| studio = Amrutheshwari Chithra
| released = 1994
| runtime = 154 minutes
| language = Kannada
| country = India
| budget =
}} romantic drama drama film Kasthuri and Shruti among others.  The film was a musical blockbuster hit upon release with the songs composed and written by Hamsalekha being received well.

== Cast ==
* V. Ravichandran  Kasthuri  Shruti
* B. C. Patil
* Anjana
* B. V. Radha
* Anandraj
* Umashri
* Bank Janardhan
* Ashalatha
* Kishori Ballal
* Shankar Ashwath

== Soundtrack ==
The music was composed and lyrics were written by Hamsalekha.  All the seven tracks composed for the film became popular with "Prema Lokada Parijathave" and "Premane Nanna Prana" being received well.

{{track listing
| headline = Track listing
| extra_column = Singer(s)
| lyrics_credits = yes
| collapsed = no
| title1 =  Premane Nanna Prana
| extra1 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics1 = Hamsalekha
| length1 = 
| title2 = One By Two
| extra2 = S. P. Balasubrahmanyam, Lata Hamsalekha
| lyrics2 = Hamsalekha
| length2 = 
| title3 = Premalokada Parijathave
| extra3 = S. P. Balasubrahmanyam
| lyrics3 = Hamsalekha
| length3 = 
| title4 = Dham Dham Endide
| extra4 = S. P. Balasubrahmanyam, Lata Hamsalekha
| lyrics4 = Hamsalekha
| length4 = 
| title5 = Ay Hudugi Yaake
| extra5 = S. P. Balasubrahmanyam, K. S. Chithra
| lyrics5 = Hamsalekha
| length5 = 
|}}

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 


 

 