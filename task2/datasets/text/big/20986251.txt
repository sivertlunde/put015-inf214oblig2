Fog Warning
 
 
{{Infobox film
| name           = Fog Warning
| image          = 
| caption        = 
| director       = Christopher Ward
| producer       = Marty Lang Jeffrey Marvin
| writer         = Christopher Ward
| starring       = Michael Barra Elise Rovinsky Cuyle Carvin Alice Snow Johnson
| music          = Jeff Preston
| cinematography = Alan McIntyre Smith
| editing        = Richard Byard
| distributor    = iTunes
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Fog Warning is a 2008 independent physiological thriller horror film written and directed by Christopher Ward in New Haven, Connecticut.    

==Plot==
When a series of gruesome murders start plaguing a small New England town, people suspect its a vampire.  Ronny (Michael Barra), manager of a local comic book store and, decides to kidnap Anna (Elise Rovinsky), a woman who he believes is with Satan. He locks her up in the attic of the historical home. Hes joined by two thugs, Karl (Cuyle Carvin) and Eddie (Joe Kathrein), who enjoy tormenting the woman until she confesses that shes the vampire. All they want is a confession Ronny can record to sell to the media, however the captive alarms them with odd behavior. Their dreams of becoming rich and famous turn into a violent nightmare.

==Cast==
*Michael Barra as Ronny
*Elise Rovinsky as Anna 
*Cuyle Carvin as Karl  
*Alice Snow Johnson as Linda
*Joe Kathrein as Eddie
*Madeline Reed as Detective Powell
*Marty Lang as Detective
*Jacqueline Shea as Trudy Trippy Miller
;Additional cast 
*Ashley Bates as Woman in Car,
*David Michaels as Art Gallery Waiter
*Gary Ploski as Gas Station Attendant
*Lou Ursone as Wino

==Reception==
Although the film has not had its theatrical release, it has been shown at film festivals  and has received a mostly positive response. Nic Brown of B Movie Man wrote, "Writer/director Christopher Ward has brings an interesting twist to the vampire tale...", and "If you are looking for an intelligent thriller that will keep you guessing, then check out Christopher Ward’s FOG WARNING",  while after its screening the Fright Night Film Festival, while praising Christopher for being a 2001 Emmy winner.

==References==
*SortEnd Magazine, August 20, 2007, by Marty Lang, "Real Meanings of "Indie" Film" 
 

==External links==
*  
*  
*  
*  

 

 
 
 
 