Joan of Arc (1948 film)
{{Infobox film
| name           = Joan of Arc
| image          = Joan of arc (1948 film poster).jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = Original theatrical release poster
| film name      = 
| director       = Victor Fleming
| producer       = Walter Wanger
| writer         = 
| screenplay     = Maxwell Anderson Andrew Solt
| story          = 
| based on       =  
| narrator       = 
| starring       = Ingrid Bergman
| music          = Hugo Friedhofer
| cinematography = Joseph Valentine Frank Sullivan
| studio         = Sierra Pictures RKO Radio Pictures   Balboa Film Distributors  
| released       =    |ref2= }}
| runtime        = 145 minutes   100 minutes  
| country        = United States
| language       = English
| budget         = $4,650,506 Matthew Bernstein, Walter Wagner: Hollywood Independent, Minnesota Press, 2000 p444 
| gross          = $5,768,142 
}} epic List historical drama French religious icon and war heroine. It was produced by Walter Wanger. It is based on Maxwell Andersons successful Broadway play Joan of Lorraine, which also starred Bergman, and was adapted for the screen by Anderson himself, in collaboration with Andrew Solt. It is the only film of an Anderson play for which the author himself wrote the film script (at least partially). 

Bergman had been lobbying to play Joan for many years, and this film was considered a dream project for her. It received mixed reviews and lower-than-expected box office, though it clearly was not a "financial disaster" as is often claimed. Donald Spoto, in a biography of Ingrid Bergman, even claims that "the critics denunciations notwithstanding, the film earned back its investment with a sturdy profit". 

The movie is considered by some to mark the start of a low period in the actresss career that would last until she made Anastasia (1956 film)|Anastasia in 1956. In April 1949, five months after the release of the film, and before it had gone out on general release, the revelation of Bergmans extramarital relationship with Italian director Roberto Rossellini brought her American screen career to a temporary halt. The nearly two-and-a-half hour film was subsequently drastically edited for its general release, and was not restored to its original length for nearly fifty years.
 Academy Award nominations for their performances. The film was director Victor Flemings last project — he died only two months after its release.

In Michael Sragows 2008 biography of the director, he claims that Fleming, who was, according to Sragrow, romantically involved with Ingrid Bergman at the time, was deeply unhappy with the finished product, and even wept upon seeing it for the first time.  Sragrow speculates that the disappointment of the failed relationship and the failure of the film may have led to Flemings fatal heart attack, but there is no real evidence to support this. While contemporary critics may have agreed with Flemings assessment of Joan of Arc, more recent reviewers of the restored complete version on DVD have not.    

==Plot== burnt at the stake at the hands of the English and the Burgundians.

== Cast ==
 
 
At Domrémy, Joans Birthplace in Lorraine (duchy)| Lorraine, December 1428
* Ingrid Bergman as Jeanne dArc
* Selena Royle as Isabelle dArc, her mother
* Robert Barrat as Jacques dArc, her father James Lydon as Pierre dArc, her younger brother
* Rand Brooks as Jean dArc, her older brother
* Roman Bohnen as Durand Laxart, her uncle
At Vaucouleurs, February 1429
* Irene Rich as Catherine le Royer, her friend
* Nestor Paiva as Henri le Royer, Catherines husband
* Richard Derr as Jean de Metz, a knight
* Ray Teal as Bertrand de Poulengy, a squire
* David Bond as Jeun Fournier, Cure of Vaucouleurs
* George Zucco as Constable of Clervaux
* George Coulouris as Sir Robert de Baudricourt, Governor of Vaucouleurs
The Court of Charles VII at Chinon, March 1429 John Emery as Jean, Duke dAlencon, cousin of Charles
* Gene Lockhart as Georges de la Trémoille, the Dauphins chief counselor 
* Nicholas Joy as Regnault de Chartres, Archbishop of Rheims and Chancellor of France
* Richard Ney as Charles de Bourbon, Duke de Clermont
* Vincent Donohue as Alain Chartier, court poet Charles VII, later King of France Battle of Orléans, May 1429 Leif Erickson as Dunois, Bastard of Orleans John Ireland as Jean de la Boussac (St. Severe), a Captain Henry Brandon as Gilles de Rais, a Captain
* Morris Ankrum as Poton de Xaintrailles, a Captain
* Tom Brown Henry as Raoul de Gaucourt, a Captain
* Gregg Barton as Louis dCulan, a Captain
* Ethan Laidlaw as Jean dAulon, her squire
* Hurd Hatfield as Father Pasquerel, her Chaplain
* Ward Bond as La Hire
 
The Enemy
* Frederick Worlock as John of Lancaster, Duke of Bedford, Englands Regent
* Dennis Hoey as Sir William Glasdale
* Colin Keith-Johnston as Philip the Good, Duke of Burgundy
* Mary Currier as Jeane, Countess of Luxembourg
* Ray Roberts as Lionel of Wandomme, a Burgundian Captain
* J. Carrol Naish as John, Count of Luxembourg, her Captor
The Trial at Rouen, February 21st to May 30th, 1431
* Francis L. Sullivan as Bishop Pierre Cauchon of Beauvais, the trial conductor
* Shepperd Strudwick as Father Jean Massieu, her Bailiff
* Taylor Holmes as the Bishop of Avranches
* Alan Napier as Earl of Warwick
* Philip Bourneuf as Jean dEstivet, a Prosecutor
* Aubrey Mather as Jean de La Fontaine
* Herbert Rudley as Thomas de Courcelles, a Prosecutor
* Frank Puglia as Nicolas de Houppeville, a Judge
* William Conrad as Guillaume Erard, a Prosecutor
* John Parrish as Jean Beaupere, a Judge
* Victor Wood as Nicolas Midi, a Judge
* Houseley Stevenson as The Cardinal of Winchester
* Jeff Corey as her Prison Guard Bill Kennedy as Thierache, her Executioner
* Cecil Kellaway as Jean Le Maistre, Inquisitor of Rouen
 

==Academy Award wins and nominations== Best Actress (nomination) — Ingrid Bergman    Best Supporting Actor (nomination) — José Ferrer Best Costume Design (color) (won) — Barbara Karinska, Dorothy Jeakins Best Cinematography (color) (won) — Joseph Valentine, William Skall, Winton Hoch Best Film Frank Sullivan Best Art Richard Day, Edwin Casey Roberts, Joseph Kish Best Score, Dramatic or Comedy Picture (nomination) — Hugo Friedhofer
* Honorary Award - Walter Wanger "for distinguished service to the industry in adding to its moral stature in the world community by his production of the picture Joan of Arc." (Wanger refused the award in protest of the films absence in the Best Picture category.)

==Production==
Joan of Arc was made in 1947–1948 by an independent company, Sierra Pictures, created especially for this production, and not to be confused with the production company with the same name that made mostly silent films. 

Filming began 16 September 1947 Nat Segaloff, Final Cuts: The Last Films of 50 Great Directors, Bear Manor Media 2013 p 98-99  and was done primarily at Hal Roach Studios, with location scenes shot in the Los Angeles area. 

The movie was first released in November 1948 by RKO. When the film was shortened for its general release in 1950, 45 minutes being cut out; it was distributed, not by RKO, but by a company called Balboa Film Distributors, the same company which re-released Alfred Hitchcocks Under Capricorn, also starring Ingrid Bergman.

The 1948 Sierra Pictures never produced another film after Joan of Arc.

==Reception== Swedish actress Jennifer Jones The Song The Wizard Alice in Wonderland (1933).

Several critics of the time criticized the film for being slow and talky,   as does contemporary critic Leonard Maltin, who has not yet reviewed the full-length version; he has said that there is "not enough spectacle to balance the talky sequences". 

The film recorded a loss of $2,480,436. 

==Versions==
The movie was first released in November 1948 by RKO. When the film was shortened for its general release in September 1950, it was distributed not by RKO, but by a company called Balboa Film Distributors, the same company which re-released Alfred Hitchcocks Under Capricorn, also starring Ingrid Bergman. The complete 145 minute version of Joan of Arc remained unseen in the U.S. for about forty-nine years. Although the complete Technicolor negatives remained in storage in Hollywood, the original soundtrack was thought to be lost. The movie was restored in 1998 after an uncut print in mint condition was found in Europe, containing the only known copy of the complete soundtrack. When it finally appeared on DVD, the restored complete version was hailed by online movie critics as being much superior to the edited version. It was released on DVD in 2004.
 WTCG and on cable several times. Although the complete, unedited version of the film was scheduled to be shown on American television for the first time on February 13, 2011, by Turner Classic Movies, with a broadcast window of 2-1/2 hours, it was pulled and the 100 minute edited version was presented later, on Sunday, February 27. However, the full-length version was finally shown on Turner Classic Movies on March 13, 2011. This marked the first time that the complete unedited version had ever been shown on American television. It has now been shown several times by TCM and appears to have supplanted the edited version.

===Differences between complete and edited versions=== roadshow version of the film and the edited general release version. 
*One that is immediately noticeable is that there is actually a snippet from Joans trial during the opening narration in the edited version, whereas in the full-length version, the events of Joans life are shown in chronological order. The narration is more detailed in the edited version than in the complete version, with much of it used to cover the breaks in continuity caused by the severe editing.

*The edited version omits crucial scenes that are important to a psychological understanding of the narrative, such as the mention of a dream that Joans father has which foretells of Joans campaign against the English. When Joan hears of the dream, she becomes convinced that she has been divinely ordered to drive the English out of France.

*Most of the first ten minutes of the film, a section showing Joan praying in the Domrémy shrine, followed by a family dinner and conversation which leads to the mention of the dream, are not in the edited version.

*In the complete 145-minute version, the narration is heard only at the beginning of the film, and there are no sudden breaks in continuity.

*Entire characters, such as Joans father (played by Robert Barrat) and Father Pasquerel (played by Hurd Hatfield) are partially or totally omitted from the edited version.
 Gone with the Wind. More than thirty of the actors are listed.

The edited version might be considered more cinematic through its use of maps and voice-over narration to explain the political situation in France. (In the full-length version, Joans family discusses the political situation during dinner.) The full-length version, although not presented as a play-within-a-play, as the stage version was, nevertheless resembles a stage-to-film adaptation, makes great use of Maxwell Andersons original dialogue, and may seem, to some, stagy in its method of presentation, despite having a realistic depiction of the Siege of Orléans.

==See also==
*The Passion of Joan of Arc, an earlier 1928 film by Carl Theodor Dreyer
*List of historical drama films

==References==
 

==External links==
 
*  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 