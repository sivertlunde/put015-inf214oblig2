Train of Events
 
 
{{Infobox film
  | name = Train of Events
  | image = Train of Events.jpg   
  | caption = VHS cover
  | director = Sidney Cole Charles Crichton Basil Dearden
  | producer = Michael Balcon Michael Relph
  | writer = Jack Warner  Peter Finch  Valerie Hobson
  | music =
  | cinematography =
  | editing =
  | distributor = Ealing Studios
  | released = 1949
  | runtime = 90 min.
  | country = United Kingdom
  | language = English
  | budget =
  | preceded_by =
  | followed_by =
  }}
 British film made by Ealing Studios directed by Sidney Cole, Charles Crichton and Basil Dearden.

A portmanteau work, it tells the various stories of the passengers who are on a train which crashes into a stalled petrol tanker at a level (grade) crossing.

==Plot==
The film opens with a long shot of a Liverpool-bound train waiting to depart from Euston station in London. The train leaves with various characters on board.

After dark, the train is still travelling north at speed when a light being waved by the trackside is seen by the driver. He applies the brakes, but a road tanker stalled across a level crossing is looming up just ahead. Plainly, there is not enough room to stop. Just as the collision is about to occur there is a fade out, which is succeeded by a general view of the railway locomotive sheds at Euston, three days earlier.

Several personal stories are then told in a series of flashbacks which make up the train of events referred to in the title.

Peter Finch plays Philip, an actor on board the train who has a dark secret. He has been visited by his estranged wife, and in a tense scene set in his lodgings we learn that she has been unfaithful while he was serving in the Army. She jeers at him, and he is roused to one supreme effort of revenge, strangling her while a gramophone plays These Foolish Things. The theatre party to which he belongs is travelling on the train, en route to a tour of Canada. Also on board is a costume hamper, which contains the body of his wife. He is hoping to "lose" it somehow on the Transatlantic crossing, but two suspicious detectives have been tracking him, and are on board the train too.

Also among those troubled on this journey are a desperate couple, Richard and Ella (Laurence Payne and Joan Dowling). He is a former prisoner of war on the run, who hates the idea of returning to Germany. They have endured a miserable life of subterfuge in a succession of seedy lodgings, and Ella is hoping that they can start again on the other side of the Atlantic. However, Ella has stolen money from her landladys cashbox to pay for the journey, and there was only enough for one of them to emigrate. Selflessly, she intends that it will be him.

A lighter note is sounded by the tale of composer Raymond Hillary (John Clements), who is travelling to a performance away from London with his star pianist, the temperamental Irina (played by Irina Baranova). Although married, he has a string of dalliances behind him, and Irina is the latest of these.
 Jack Warner). He is facing his own crisis, because he is a candidate for a job in management at the locomotive sheds. This would take him off the footplate and allow him to work office hours for the first time in his career, which is the heartfelt wish of his wife Emily (Gladys Henson). However, to protect his daughters future husband when he was accidentally absent, Jim illicitly worked the younger mans shift, and this innocent deception could cost him the promotion.

The scene returns to the train, which is now roaring through the dark of the evening. Again, there is the light by the track, and the tanker just ahead. This time, we see the whole collision.

The derailed and damaged train is lying in ruins. Jim Hardcastle groggily recovers consciousness in a pile of coal from the overturned tender, and shocked passengers wander about. One of them is Richard, but Ella cannot wander. She is on a stretcher and evidently badly hurt, and dies before she can be taken away for treatment. Richard runs from the scene (and the attending police), unaware of the steamship ticket in Ellas handbag, which blows away across the tracks.

Philip, meanwhile, seems unhurt and tries to make a dash for freedom. But as he tries to evade the detectives he runs dangerously close to the wreckage, just in time to be caught as the end of an unstable coach collapses on top of him.

Irina and Raymond are only bruised, and their company is able to continue with their performance, albeit in bandages.

There is a happy ending for driver Jim. The final scene shows him waving goodbye to his wife, as he prepares to cycle across to the locomotive sheds on his first day in that nine-to-six job.

==Cast==

==="The Engine Driver"=== Jack Warner as Jim Hardcastle
* Gladys Henson as Mrs Hardcastle
* Susan Shaw as Doris Hardcastle
* Patric Doonan as Ron Stacey
* Miles Malleson as Timekeeper
* Philip Dale as Hardcastles fireman
* Leslie Phillips as Staceys Fireman

==="The Prisoner-of-War"===
* Joan Dowling as Ella
* Laurence Payne as Richard
* Olga Lindo as Mrs. Bailey

==="The Composer"===
* Valerie Hobson as Stella
* John Clements as Raymond Hillary
* Irina Baronova as Irina
* John Gregson as Malcolm
* Gwen Cherrell as Charmian
* Jacqueline Byrne as TV Announcer

==="The Actor"===
* Peter Finch as Philip
* Mary Morris as Louise
* Laurence Naismith as Joe Hunt
* Doris Yorke as Mrs Hunt
* Michael Hordern as Plainclothesman Charles Morgan as Plainclothesman
* Guy Verney as Producer
* Mark Dignam as Bolingbroke
* Philip Ashley as Actor
* Bryan Coleman as Actor
* Henry Hewitt as Actor
* Lyndon Brook as Actor

== Production information ==
Jack Warner was permanently injured while making this film. He had insisted on learning how a steam engine is driven to get his posture right, but fell into a locomotive turntable pit and injured his back. He had a slight limp ever afterwards as a result, which became noticeably worse as he aged.

One quirk of the film is that the number of a locomotive featured in one of the early scenes is painted out (presumably to avoid worrying passengers who might fear that it really would be involved in an accident) but is still clearly readable because the numbers themselves were made from raised metal.

The locomotives used in the film included two LMS Class 3F "Jinty" 0-6-0Ts Nos. 47327 and 47675, and LMS Royal Scot Class No. 46126 Royal Army Service Corps. Two Royal Scots, No. 46100 Royal Scot and No. 46115 Scots Guardsman, and several Jinties are preserved today, including No. 47327 at the Midland Railway - Butterley.

==External links==
* 

 
 

 
 
 
 
 
 
 
 
 