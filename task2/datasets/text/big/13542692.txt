La gran familia
{{Infobox Film
| name           = La gran familia
| image          = La gran familia.jpg
| image_size     = 
| caption        = Spanish film poster
| director       = Fernando Palacios
| producer       = Pedro Masó	
| writer         = Rafael J. Salvia Pedro Masó Antonio Vich
| narrator       = 
| starring       = Alberto Closas  Amparo Soler Leal José Isbert  José Luis López Vázquez  María José Alfonso  Jaime Blanch  Pedro Mari Sánchez  Maribel Martín  Paco Valladares  Julia Gutiérrez Caba  María Isbert  Jesús Álvarez (actor)|Jesús Álvarez  Luis Barbero  Jesús Guzmán (actor)  Pedro Sempson  Valentín Tornos  Félix Acaso  José María Caffarel  Laly Soldevila  José María Prada  Luis Morris
| music          = Adolfo Waitzman
| cinematography = Víctor Benínetz Juan Mariné          
| editing        = Pedro del Rey  
| distributor    = 
| released       = 20 December 1962 Spain
| runtime        = 104 minutes
| country        =   Spanish
| budget         = 
}}
 1962 Spain|Spanish classic comedy film directed by Fernando Palacios.

It was so successful that it inspired both a sequel, La familia y... uno más (1965) ; La familia, bien, gracias (1979) and a TV movie, La familia... 30 años después (1999) (TV)

==Plot==
Carlos (Alberto Closas) has to work to feed his wife (Amparo Soler Leal), and his 15 children and the grandpa (José Isbert). The children dream with having a television. The summer holidays is another trouble for the big family.

==Cast==
*Alberto Closas
*Amparo Soler Leal
*José Isbert
*José Luis López Vázquez
*María José Alfonso
*Jaime Blanch
*Pedro Mari Sánchez
*Maribel Martín
*Paco Valladares
*Julia Gutiérrez Caba
*María Isbert
*Jesús Álvarez (actor)|Jesús Álvarez
*Luis Barbero
*Jesús Guzmán (actor)
*Pedro Sempson
*Valentín Tornos
*Félix Acaso
*José María Caffarel
*Laly Soldevila
*José María Prada
*Luis Morris

==External links==
*  

 
 
 
 
 
 
 
 


 
 