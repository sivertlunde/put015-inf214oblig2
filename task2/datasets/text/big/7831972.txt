The Chimp
 
 
{{Infobox film
  | name = The Chimp
  | image = Laurel and Hardy The Chimp.jpg
  | caption = Promotional shot for The Chimp
  | director =  James Parrott
  | producer = Hal Roach
  | writer = H.M. Walker 
  | starring = Stan Laurel   Oliver Hardy	 
  | music = Marvin Hatley   Leroy Shield
  | cinematography = Walter Lundin	
  | editing = Richard C. Currier
  | distributor = Metro-Goldwyn-Mayer
  | released = May 21, 1932
  | runtime = 25 13"
  | language = English
  | country = United States
  | budget = 
}}
The Chimp is a Laurel and Hardy short film made in 1932. It was directed by James Parrott, produced by Hal Roach and distributed by Metro-Goldwyn-Mayer.

== Plot ==
The comedy duo are working at the circus. They first appear in the Pantomime horse and then as assistants to Destructo, a strongman. The circus goes bankrupt after the Big Top is destroyed when Laurel and Hardy cause Destructos cannonball-catching act to go wrong. The circus cant pay them their wages so Oliver is given a gorilla called Ethel and Stanley a Flea Circus as a pay off. Despite the films title Ethel isnt a chimpanzee. She is dressed in a ballet tutu and hat. Stanley and Oliver need to find a room to stay in overnight and they go to a guest house. The landlord gives them a room but refuses to let Ethel stay. A lion named MGM after the MGM lion has escaped from the circus. Stanley and Oliver try to smuggle Ethel into their room whilst avoiding MGM. They decide to leave Ethel outside for the night and go to bed. They sleep in the same bed as in all Laurel and Hardy films. Stanley falls out of bed and decides to sleep in the spare bed. Ethel climbs in through the window and gets into bed with Oliver. Ethel steals the blanket from Oliver so he decides to use the spare bed as well as Stanley. They both begin to itch and find that the flea circus has escaped into the bed. Another guest at the house puts some music on and Ethel begins to dance. The landlord has a wife called Ethel and when Oliver tells the gorilla to stop dancing the landlord thinks his wife is in the room with Stanley and Oliver. The landlord confronts Stanley and Oliver with a pistol. At the end the gorilla gets hold of the pistol and begins to shoot, scaring everyone out of the room.

== Cast ==
* Stanley Laurel (Himself)
* Oliver Hardy (Himself)
* Tiny Sandford (Destructo)
* Billy Gilbert (Landlord)
* Charles Gemora (Ethel the gorilla, Opening Announcer)
* Jimmy Finlayson (Ringmaster)
* Martha Sleeper (Ethel, landlords wife)
* Belle Hare (Laid-off Circus Performer) George Miller (Circus owner)
* Bobby Burns (Tenant) Jack Hill (Audience)
* Lois Laurel (Audience)
* Baldwin Cooke (minor role)	
* Dorothy Layton (minor role)

== The Sons of the Desert ==
Chapters — called Tents — of The Sons of the Desert, the international Laurel and Hardy Appreciation Society, all take their names from L&H films. The Chimp Tent is in Cincinnati, Ohio. 

==References==
 

== External links ==
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 