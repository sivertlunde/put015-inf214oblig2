The Foxy Merkins
{{Infobox film
| name           = The Foxy Merkins
| image          = The Foxy Merkins poster.jpg
| alt            = 
| caption        = Film poster
| director       = Madeleine Olnek
| producer       = Madeleine Olnek Laura Terruso
| writer        = Madeleine Olnek Lisa Haas Jackie Monahan
| screenplay     = 	
| starring       = Lisa Haas Jackie Monahan Alex Karpovsky Susan Ziegler Sally Sockwell Deb Margolin
| music          = Dan Bartfield
| cinematography = Anna Stypko
| editing        = Curtis Grout
| studio         = 
| distributor    = 
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English 
}}
The Foxy Merkins is an American comedy film directed by Madeleine Olnek   and the lowest budget narrative feature in the Sundance 2014 film festival. The film had its world premiere at Sundance NEXT Weekend on August 9, 2013  and later screened at 2014 Sundance Film Festival on January 20, 2014.    It had its international premiere at the Moscow International Film Festival on June 25, 2014,  selected for screening despite the hostile climate towards LGBT people in Russia.

The film had its theatrical release December 5, 2014 in Brooklyn, NY, presented by the Independent Film Project (IFP).

==Plot==
Margaret is a down-on-her-luck hooker in training, who meets up with Jo, a beautiful and self-assured grifter from a wealthy family and an expert on servicing women, even as she considers herself proudly heterosexual.

==Cast==
*Lisa Haas as Margaret
*Jackie Monahan as Jo
*Alex Karpovsky as The Mumbling Erotic Accessory Salesman/CNN Executive
*Susan Ziegler as Kinky Republican Lady
*Sally Sockwell as Eloise
*Deb Margolin as The MFA Drama Student
*Dennis Davis as Margaret’s brother
*Diane Ciesla as Jo’s mother
*Rae C Wright as the jittery cancelling businesswoman

==Reception==
The Foxy Merkins received generally positive reviews from critics. Mark Adams, chief film critic of Screen International, wrote, "The film, though clearly micro-budget and at times acted with enthusiasm rather than precision, is a gentle charmer and punctuated with some delightful dialogue and set-piece sequences." Dennis Harvey of Variety (magazine)|Variety, said in his review that "This equally absurdist exercise gets off to a good start portraying an unlikely Manhattan prostitution trade in homeless but well-educated females serving closeted lesbian socialites."  Emma Myers an “aspiring writer”  at Sundance as part of a critic training program at Indiewire graded the film C- and wrote, ""The Foxy Merkins" has all the right ingredients to please Olneks niche audience. Unfortunately, after a truly hilarious and fresh first act, the film can no longer sustain its premise.”  In contrast, Peter Knegt, a senior editor at Indiewire, praised the film as “Downright hilarious,” naming it one of "10 Great LGBT Films From This Years Sundance Film Festival" and also one of "8 Queer Women Films to Watch in 2014."  Stephen Holden from the NY Times wrote about The Foxy Merkins “One of its charms is its cheerful demolition of female stereotypes, gay and straight” and called it “sly” “funny” and “sweet.” 

Justin Lowe in his review for The Hollywood Reporter said of star and co-writer Lisa Haas, "Haas, who also played the lead in Codependent, reprises a similar, wide-eyed role, but is so effective at drawing out some of the film’s more ridiculously awkward moments that it’s hard to imagine any established actress outdoing her."." 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 