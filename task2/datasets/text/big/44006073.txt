Themmadi Velappan
{{Infobox film
| name = Themmadi Velappan
| image =
| image_size =
| caption = Hariharan
| producer = GP Balan
| writer = S. L. Puram Sadanandan
| screenplay = S. L. Puram Sadanandan Madhu Jayabharathi KPAC Lalitha
| music = M. S. Viswanathan
| cinematography = TN Krishnankutty Nair
| editing = VP Krishnan
| studio = Chanthamani Films
| distributor = Chanthamani Films
| released =  
| country = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, Hariharan and produced by GP Balan. The film stars Prem Nazir, Madhu (actor)|Madhu, Jayabharathi and KPAC Lalitha in lead roles. The film had musical score by M. S. Viswanathan.   
 
==Cast==
  
*Prem Nazir  Madhu 
*Jayabharathi 
*KPAC Lalitha 
*Jose Prakash 
*Pattom Sadan 
*T. R. Omana 
*Bahadoor 
*Kanakadurga 
*Kunjava
*Nellikode Bhaskaran 
*P. K. Abraham 
*Paravoor Bharathan  Sudheer  Master Raghu as Young Velappan
 

==Soundtrack==
The music was composed by M. S. Viswanathan. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Dharmasamaram Vijayichu || K. J. Yesudas, Chorus || Mankombu Gopalakrishnan || 
|- 
| 2 || Indradhanussu Kondu || K. J. Yesudas || Mankombu Gopalakrishnan || 
|- 
| 3 || Thrisanku Swargathe || K. J. Yesudas, Chorus || Mankombu Gopalakrishnan || 
|-  Susheela || Mankombu Gopalakrishnan || 
|}

==References==
 

==External links==
*  

 
 
 


 