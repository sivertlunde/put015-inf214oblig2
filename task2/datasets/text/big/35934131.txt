Lehmann's Honeymoon
{{Infobox film
| name           = Lehmanns Honeymoon
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Wiene
| producer       = Oskar Messter
| writer         = Arthur Bergen   Robert Wiene   
| narrator       = 
| starring       = Guido Herzfeld   Christel Lorenz   Arnold Rieck
| music          =  Giuseppe Becce   
| editing        = 
| cinematography = 
| studio         = Messter Film
| distributor    = 
| released       =  
| runtime        = 
| country        = German Empire Silent  German intertitles
| budget         = 
| gross          = 
}}
 silent comedy film directed by Robert Wiene and starring Guido Herzfeld, Christel Lorenz and Arnold Rieck. In order to persuade a daydreaming Professor of Greek History to marry his cousin, his family dress themselves up as Ancient Greeks. 

==Cast==
* Guido Herzfeld  
* Christel Lorenz   
* Arnold Rieck   
* Hella Thornegg

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 

 
 
 
 
 
 
 
 


 
 