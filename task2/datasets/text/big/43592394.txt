Ata Pata Laapata
 
{{Infobox film
| name           = Ata Pata Laapata
| image          = Ata Pata Laapata.jpg
| caption        = Movie poster for Ata Pata Laapata
| director       = Rajpal Yadav
| producer       = Radha Yadav
| writer         = Mohd. Saleem
| starring       = Rajpal Yadav, Asrani, Om Puri, Ashutosh Rana, Manoj Joshi, Govind Namdeo, Dara Singh, Vikram Gokhale, Vijay Raaz, and Satyadev Dubey
| music          = Original Songs: Sujeet Choubey, Amod Bhatt, Rajpal Yadav, Sukhwinder Singh Background Score: Aadesh Shrivastav
| cinematography = Arvind K
| editing        = Aseem Sinha
| released       =  
| runtime        = 128 minutes
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}}
 musical political satire film from first-time director Rajpal Yadav, who is also the main protagonist in the film. Other than Yadav, the film stars the ensemble cast of Asrani, Om Puri, Ashutosh Rana, Manoj Joshi, Govind Namdeo, Dara Singh, Vikram Gokhale, Vijay Raaz, and Satyadev Dubey. The film was produced by Yadavs wife Radha Yadav. The film was a commercial failure and panned for its weak script.

==Plot==
Manav Chaturvedi (Rajpal Yadav) files a police complaint of robbery where the entire house has gone missing. Chaturvedi is suspected of abolishing his own house in order to claim the insurance money. The case comes to the medias attention and soon becomes a topic of everyones interests. Due to media pressure, authorities are forced to acknowledge the robbery. The film focuses on the loopholes of governance, bureaucracy and the attitude of people in power.

==Cast==
* Rajpal Yadav as Manav Chaturvedi and narrator.
* Amit Behl as Lawyer
* Asrani as Session Court Lawyer
* Om Puri
* Ashutosh Rana as Satyaprakash Chaube
* Manoj Joshi as Jagrut Jaganath BMC Commissioner
* Dara Singh as S P Shastri
* Vikram Gokhale as Superintendent of police (India)|S.P.
* Vijay Raaz as Munshiji
* Satyadev Dubey as Pagla Baba

==Production== Sameer and sung by Sukhwinder Singh. 

===Casting===
The film had a huge star cast of about 175 actors, the majority of whom had no prior acting experience in films. The ensemble cast included veteran Dara Singh and Satyadev Dubey, for both of whom this was their last film.     Both died before the release of the film. 

==Release==
The music of the film was launched by Amitabh Bachchan on 22 September 2012.  The film was originally set to release in October 2012, but its release was stayed by Delhi High Court after a local businessman MG Agarwal filed a cheating case against Rajpal Yadav. Agarwal alleged that Yadav had taken Rs. 5 crore as loan from him for the film and signed agreement with Agarwal owned Murli Projects for the music and overall production of the movie. He alleged that Yadav went ahead and independently released the music and all the cheques Yadav gave to Agarwal bounced.  The film was finally released on 2 November 2012. 

==Reception==
Film received average rating or 2.8 out of 5 on Rotten Tomatoes. The Times of India criticized the film for weak script, repetitiveness, preachiness and lack of clarity. Songs were criticized for acting only as fillers without adding anything to the story.  Koimoi.com praised the film for an experimental theatre play-like story telling, casting, and performances of Ashutosh Rana and Rajpal Yadav. But it criticized the film for lack of in-synch dialogues and abrupt ending.  Navbharat Times criticized the movie for being noisy, weak script and inability of Yadav to make good use of the huge star cast.  Commercially the film failed at the box office. 

==References==
 

==External links==
*  

 
 
 
 