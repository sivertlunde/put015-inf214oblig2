The Boys of Baraka
 
 
{{Infobox film name            = The Boys of Baraka image           = Baraka poster.jpg director        = Heidi Ewing Rachel Grady starring        = Devon Brown Richard Keyser music           = J.J. McGeehan cinematography  = Marco Franzoni Tony Hardmon editing         = Enat Sidi producer        = Heidi Ewing Rachel Grady distributor     = ITVS released        =   runtime         = 84 min. language        = English Creative Consultant = Sam Pollard
}}
The Boys of Baraka is a 2005 documentary film produced and directed by filmmakers Heidi Ewing and Rachel Grady. Twenty at-risk boys from Baltimore attend the seventh and eighth grades at a boarding school in Kenya. The documentary follows them in Kenya and in Baltimore, before and after attending the Baraka School in Kenya. It also mentions that 61% of African Americans in Baltimore do not graduate High School.
 South by Southwest Film Festival in 2005 winning a Special Jury Prize. The film also won the Audience Award Best Feature Film at the 2005 Silverdocs Festival at the AFI Silver Theater in Silver Spring, Maryland. The film also won the Gold Hugo at The Chicago Film Festival, won the NAACP Image Award and was nominated for an Emmy.

==Film Description==
The Boys of Baraka reveals the human faces of a tragic statistic – 61 percent of Baltimores African-American boys fail to graduate from high school; 50 percent of them go straight on to jail. Behind these figures lies the grimmer realities of streets ruled by drug dealers, families fractured by addiction and prison and a public school system seemingly surrendered to uncontrolable chaos. As simply portrayed in Heidi Ewing and Rachel Gradys award-winning documentary, which has its national broadcast premiere on public televisions POV, a generation of inner-city children faces dilemmas that would undo most adults. In this case, they are told early on that they face three stark "dress" options by their 18th birthdays – prison orange, a suit in a box, or a high school cap and gown.

The four young boys featured in The Boys of Baraka, despite individual talents and considerable personal charms, cannot escape the common fate expressed by those dress options. But fate, as documented in this film, comes to them with a remarkable and fickle twist – an experimental boarding school in rural Kenya.
 
The Boys of Baraka won an NAACP Image Award for Outstanding Independent or Foreign Film, as well as Best Documentary Awards at the Chicago and Newport film festivals, a special Jury Award at South by Southwest (SXSW), and Audience Awards at the Woodstock and SILVERDOCS film festivals.
 
Devon Brown, Montrey Moore, Richard Keyser, Jr., and Richards younger brother, Romesh Vance, are just at that age – 12 and 13 years old – when boys start to become men. On the harsh streets of a city like Baltimore, Maryland, where the four boys live, that passage presents choices that are far more make-or-break, even life-or-death, than anything faced by their counterparts in the suburbs or the middle-class districts of the city. Will they want to become part of the lure of the drug trade? Will they, against the odds, continue their education? Or one day will they simply, whether the intended target or not, stop a bullet? Given the odds against them, do these boys have the power to make a choice? What choice will they make? The choice is theirs.
 
Richard speaks with charming bluster about being strong, but is troubled by his absent fathers imprisonment and about his little brothers prospects growing up in the projects. Romesh is already suspicious and downbeat. Montrey is compulsively mischievous and contentious, fighting with other boys and earning multiple suspensions from school. Devon seems to have found a way: preaching in the local church with precocious confidence while coping with his mothers repeated bouts with addiction and prison.
 
The Boys of Baraka shows that despite the most difficult circumstances, the boys can draw on the traditional strengths of the black community, church, and family. The latter may present them with a mixed legacy; the extended family pulls together to make up for members succumbing to the social blights of poverty and the drug culture. But the enthusiastic support of family and community are critical when a rare opportunity comes to the boys to join 16 other inner-city black youths in attending an experimental boarding school. Their families know instinctively that virtually any educational opportunity besides the Baltimore public schools will offer their children a lifeline — even if it is located in the rural bush land of Kenya in east Africa.
 
Founded by the private Abell Foundation in 1996, the Baraka School — "baraka" means "blessing" in Kiswahili, the native spoken language of eastern Africa — was designed to give "at-risk" African-American boys from Baltimore a chance to learn academically and grow personally in an environment far removed from their troubled neighbourhoods. Without television, Game Boys and fast food, and exposed to the hardworking and socially rich life of rural Africans, the boys are given a more disciplined structure and the kind of educational attention (a five-to-one student-teacher ratio) normally reserved for better-heeled private schools.
 
The boys themselves understand that this is a chance for them to change their lives, but its difficult to imagine 12-year-olds making a leap to rural Africa without the presence of their families and friends. In The Boys of Baraka, the filmmakers have crafted the vérité tale of the Baraka class, including Devon, Montrey, Richard and Romesh, that left for Kenya in September 2002 for the first of two years of schooling, corresponding to the seventh and eighth grades. The over-riding goal is to have the kids gain educational confidence and direction and to go on to at least graduate from high school. The Baraka School had a good record in the effort. In contrast to Baltimores public schools, three out of four Baraka students were graduating high school.
 
Africa is at first both wonderful and disorienting. The boys revel in a chance to be children, discovering lizards or playing pranks without fear of gunfire. Then homesickness and discontent with the schools discipline take hold. Romesh even sets off in a futile effort to drag his pack to the distant airport. But then a transformation begins to take place.
 
By the time they return to Baltimore for summer vacation, they share a new enthusiasm for education and a greater confidence in their abilities. Its a striking flowering of hope, not only for the boys but also for their families. Then unexpected news comes for Devon, Montrey, Richard and Romesh and their families. How each of the boys responds to this dramatic twist of fate may be the most surprising thing about The Boys of Baraka.
 
"It was both exhilarating and sobering to follow these kids through a couple of years of grappling with one of the best breaks they may ever have," says co-director/producer Heidi Ewing. "The film zeroes in on kids that society has given up on — boys with every disadvantage — but who refuse to be throw-aways," says Rachel Grady, her directing/producing partner.
 
The Boys of Baraka is a co-production of the Independent Television Service (ITVS), produced in association with American Documentary | POV

As of 2013 Devon Brown is pursuing a kickstarter business called Taharka Brothers Ice Cream in Baltimore.

==External links==
*  
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 