Trees Cry for Rain
{{Infobox film
| name           = Trees Cry for Rain: A Sephardic Journey
| image          = Treescryforrainhomeimage-1-.jpg
| image_size     = 150px
| caption        =
| director       = Bonnie Burt
| producer       =
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1989
| runtime        = 33 min.
| country        = English
| budget         =
| preceded_by    =
| followed_by    =
| website        =
}}
 documentary that profiles Rachel Amado Bortnick, exploring her Turkey|Turkish-Jewish Cultural heritage|heritage.

==Summary==
Less than 25,000   took effect.

Jews were executed or forced to convert under Christian rule in Spain before being forced out of the country. Many fled to nearby Turkey, where, for centuries, they were able to maintain their traditions, passing them down through the generations. Trees Cry for Rain shares an intimate interview with Rachael Amado Bortnick, a native of Turkey who left as a young adult, complemented by detailed paintings and illustrations of Turkey, to bring the once-rich culture of Turkey’s Sephardic Jewry to life. Their unique foods, songs, and traditions are explored, and the importance of keeping their unique Judeo-Spanish language alive is impressed upon the viewer.
 Ladino language is in danger of being lost forever. “My generation was the last to speak the language fluently,” Rachael explains. But, thankfully, the culture’s beautiful music has brought about a renewed interest in the unique tongue. Sephardic singer, Judy Frankel practices her pronunciations with Rachael, while she sings, “Trees cry for rain and the mountains for wind; So my eyes weep for you, my dear sweetheart.”

Rachael listens intently and confesses that the song’s refrain reminds her of her own situation. “I turn and I say what will become of me? I will die in a strange land.”

==See also==
Other documentaries about Diaspora Jews:
*Trip to Jewish Cuba
*Reconstruction (documentary)|Reconstruction
*Next Year in Argentina
*Balancing Acts
*Yearning to Belong
*Thunder in Guyana

==References==
* *{{Citation
  | title =Trees Cry for Rain
  | date =
  | year = 2008
  | url =http://www.tjctv.com/movies/trees-cry-for-rain/
  | accessdate = September 9 }}

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 

 
 