Very Important Person (film)
 
 
{{Infobox film
|  name           = Very Important Person
| image          = Very Important Person 1961 cinema poster.jpg
| caption        = Original British cinema poster
|  director       = Ken Annakin
|  producer       = Leslie Parkyn Julian Wintle Jack Davies Henry Blyth
| based on       =  
|  starring       = James Robertson Justice Stanley Baxter Leslie Phillips
|  music          = Reg Owen
|  cinematography = Ernest Steward
|  editing        = Ralph Sheldon Independent Artists
|  distributor    = Rank Organisation (UK) Union Film Distributors (US)
|  released       =  
|  country        = United Kingdom
|  runtime        = 98 min.
|  language       = English
| budget         =
}} Jack Davies and Henry Blyth.

The film contains performances from several well-known British comedy and character actors, including James Robertson Justice, Stanley Baxter as both a dour Scottish prisoner and the camp Kommandant, Eric Sykes as a sports fanatic, John Le Mesurier as the Escape officer, Leslie Phillips and Richard Wattis as the emotional Entertainments officers, desperately trying to coax quality performances out of would-be entertainers.
 Leicester Square Theatre in Londons West End and went on General Release in late May on Ranks second string National circuit.

==Plot==
Sir Ernest Pease (James Robertson Justice) is a brilliant but acerbic scientist working on aircraft research during World War II. He needs to take a trip on a bomber to observe the results of his work. At first the plan is to fly in an RAF plane disguised as an RAF officer, but when he is told to shave off his beard he refuses and gets to go as a Royal Navy officer, as beards are allowed in the RN.

Because it is vital that nobody knows who he is, Pease goes on the trip as Lieutenant Farrow, a public relations officer.
 Germany and RAF officers.

His excellent command of the German language causes him to be suspected of being a German agent, but when his real identity and importance becomes known to the Senior British Officer (Norman Bird), orders are given that the men in his hut co-operate to help him escape.

Pease initially views his somewhat happy-go-lucky fellow prisoners, especially Jimmy Cooper (Leslie Phillips), Everett (Stanley Baxter) and "Bonzo" Baines (Jeremy Lloyd) with disdain, but comes to understand and appreciate their optimistic attitudes under the prison system they find themselves in, even if he remains as pompous and arrogant as ever.

Pease/Farrow concocts a plan whereby he is believed to have escaped "through the wire". In fact, he plans to go into hiding and later walk out of the camp, disguised as one of three visiting Swiss Red Cross observers. Crucial to the escape plan is that Everett looks exactly like the camp Commandant – also played by Baxter – (even though he describes him as "hideously ugly"). He must pretend to be the Commandant if Pease/Farrow is to escape.

Whilst the prisoners busy themselves with organising camp concerts and sports, the plan goes ahead. But it nearly comes unstuck at the last moment, when one of the prisoners, "Grassy" Green (John Forrest) is revealed as a real Luftwaffe officer and spy. He is "dealt with", and Pease, Cooper and Baines calmly walk out of the camp and eventually make their way back home.
 This Is Your Life during which he is re-united with the other ex-POWs and even gets to meet the former Commandant  , who is now the sports/entertainments officer of a holiday camp.

==Cast==
 
* James Robertson Justice – Sir Ernest Pease; aka: Lt. Farrow RN
* Leslie Phillips – Jimmy Cooper
* Stanley Baxter – Jock Everett/Kommandant Stamfel
* Eric Sykes – Willoughby, Sports Officer
* Richard Wattis – Woodcock, Entertainments Officer
* Godfrey Winn – Himself
* Colin Gordon – Briggs
* John Le Mesurier – Piggott, Escape Officer
* Norman Bird – Travers, Senior British Officer
* Jeremy Lloyd – Bonzo Baines John Forrest – Grassy Green
* Jean Cadell – Lady Telling Story on TV show. Opening Scene Peter Myers – Shaw Ronnie Stevens – Hankley
* Ronald Leigh-Hunt – Clynes
* Steve Plytas – Luftwaffe officer
* John Ringham – Plum Pouding
* Joseph Furst – Luftwaffe interrogator
* Norman Shelley – Fred Whittaker
* Brian Oulton – 1st Scientist in corridor
* Frederick Piper – 2nd Scientist in corridor
* Joan Haythorne – Miss Rogers
 
==Inspiration==
The escape plan, to walk out of the camp dressed as Red Cross observers, actually happened. It was briefly mentioned in Paul Brickhills book The Great Escape.
 John Foley, which has erroneously caused John Foley to sometimes be credited as author of the novel, which the film is based upon. However, it was the other way around: his novel is based on the film.  

==Quotes==
Sir Ernest Pease to the others in his hut: "Cooking requires no intelligence. Were it otherwise women would be no good at it."

==References==
 

== External links ==
*  
*   at BFI Explore Film
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 