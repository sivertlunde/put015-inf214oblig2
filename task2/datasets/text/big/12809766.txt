Hwang Jin Yi (film)
{{Infobox film
| name           = Hwang Jin Yi
| image          = Hwang Jin Yi.jpg
| caption        = Poster to Hwang Jin Yi (2007)
| director       = Jang Yoon-hyun
| producer       = Lee Chun-yeon   Jo Seong-won   Hwang Gyeong-seong
| writer         = Kim Hyun-jung
| based on       =  
| starring       = Song Hye-kyo Yoo Ji-tae
| cinematography = Choi Young-taek
| editing        = Nam Na-yeong
| distributor    = Cine-2000
| music          = Won Il
| released       =  
| runtime        = 141 minutes
| country        = South Korea
| language       = Korean
| budget         =  
| admissions     = 
| gross          =   
}} South Korean biographical drama 2002 novel Hwang Jin-yi, the most famous courtesan (or "gisaeng") in Korean history, starring Song Hye-kyo in the title role.  

==Plot==
Raised as an aristocrat in 16th century  , she decides to give up her virginity to a man of her own choosing, and spends her first night with Nomi, a long-time family servant, whom she is aware loves her deeply. As a gisaeng, Jin-yi becomes celebrated for her legendary beauty, wit, and talents in singing, dancing and poetry. But although she is surrounded by an entourage of noblemen showering her with gifts and admiration, she lives a solitary life of tragic isolation. Jin-yis only solace is the game she and the local governor play on her noble clients, tricking them into exposing their hypocrisies. But when a bandit leader matching Nomis description is spotted in the region, Jin-yi begins to question the life she endures.

==Cast== Hwang Jin-yi
* Yoo Ji-tae - Nomi
* Ryu Seung-ryong - Hee-yeol
* Youn Yuh-jung - old woman
* Oh Tae-kyung - Gwiddongi Jeong Yu-mi - Yi-geum
* Ye Soo-jung - Hwang Jin-yis mother
* Jo Seung-yeon - Byeok Kye-soo Kim Eung-soo - Seo Gyung-deok
* Kim Boo-seon - Jang-deok
* Song Min-ji - Mae-hyang
* Park Cheol-ho - nobleman Hwang
* Lee Kwang-hee - servant of nobleman Hwang
* Kim Yoo-jung - young Hwang Jin-yi Lee Hyun-woo - young Nomi
* Kim Hyun-ah - gisaeng
* Tae-won - nobleman Kim
* Park No-shik - Choi Joo-boo
* Jo Kyung-hoon - public officer
* Choi Ji-na - Hyun-geum
* Bae Yong-geun - nobleman
* Min Bok-gi - Ho-jang
* Lee Mi-so - Hwang Jin-yis female servant

==Box office==
Hwang Jin Yi was highly anticipated due to the star power of Song Hye-kyo, and many expected the film to be a box office hit. It received 1,270,644 admissions nationwide;  however, due to its   budget, it was not enough for the producers to turn a profit. 

==Awards and nominations==
;2007 Chunsa Film Art Awards
* Best Lighting - Im Jae-young
* Technical Award - Jung Ku-ho

;2007 Blue Dragon Film Awards 
* Nomination - Best Actress - Song Hye-kyo
* Nomination - Best Cinematography - Choi Young-taek
* Nomination - Best Art Direction - Kim Jin-cheol
* Nomination - Special Prize

;2007 Korean Film Awards
* Best New Actress - Song Hye-kyo
* Nomination - Best Cinematography - Choi Young-taek
* Nomination - Best Art Direction - Kim Jin-cheol, Jung Ku-ho
* Nomination - Best Music - Won Il
* Nomination - Best New Actor - Ryu Seung-ryong

;2008 Grand Bell Awards
* Best Costumes - Jeong Jeong-eun
* Best Music - Won Il
* Nomination - Best Art Direction - Kim Jin-cheol, Jung Ku-ho

==See also==
* Hwang Jini (TV series)
* Eoudong

== References ==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 
 
 