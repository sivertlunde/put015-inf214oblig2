Daughter of Shanghai
{{Infobox film
| name           = Daughter of Shanghai
| image          = Daughter of shanghai.jpg
| caption        = Philip Ahn and Anna May Wong
| director       = Robert Florey
| producer       = Harold Hurley (uncredited) Edward T. Lowe Jr. (uncredited)
| writer         = William J. Hurlbut (uncredited)   Gladys Unger   Garnett Weston 
| starring       = Anna May Wong Philip Ahn Charles Bickford Buster Crabbe Cecil Cunningham J. Carrol Naish
| music          =
| cinematography = Charles Schoenbaum
| editing        = Ellsworth Hoagland
| distributor    = Paramount Pictures
| released       = December 17, 1937
| runtime        = 63 minutes
| country        = United States
| language       = English
| budget         =
}}
Daughter of Shanghai is a 1937 American motion picture directed by Robert Florey, written by Gladys Unger and Garnett Weston, and starring Anna May Wong and Philip Ahn. The film was unusual in that Asian American actors played the lead roles. It was also one of the first films in which Anthony Quinn appeared.

==Plot==
Lan Ying Lin and government agent Kim Lee battle alien smugglers.

==Cast==
*Anna May Wong as Lan Ying Lin
*Philip Ahn as Kim Lee
*Charles Bickford as Otto Hartman
*Buster Crabbe as Andrew Sleete
*Cecil Cunningham as Mrs. Mary Hunt
*J. Carrol Naish as Frank Barden
*Evelyn Brent as Olga Derey
*Anthony Quinn as Harry Morgan
*John Patterson as James Lang
*Fred Kohler as Captain Gulner

==Analysis==
Daughter of Shanghai is unique among 1930s Hollywood features for its portrayal of an Asian-focused theme with two prominent Asian-American performers as leads. This was truly unusual in a time when white actors typically played Asian characters in the cinema. At best, Hollywood assigned some Asian roles to Asian performers and some to whites stars in the same film, with results that seem discordant today even if widely accepted at the time. Daughter of Shanghai was prepared as a vehicle for Anna May Wong, the first Asian-American woman to become a star of the Hollywood cinema. Appearing in some 60 movies during her life, she was a top billed player for over twenty years, working not only in Hollywood, but also in England and Germany. In addition, she was a star of the stage and a frequent guest performer on radio, and would headline the first American television series concentrating on an Asian character, The Gallery of Madame Liu-Tsong (DuMont, 1951).

==Reception==
On its release, on the 17th of December 1937, The New York Times gave the film a generally positive review, commenting of its B-movie origins, "An unusually competent cast saves the film from the worst consequences of certain inevitable banalities.  ... combine with effective sets to reduce the natural odds against any pictures in the Daughter of Shanghai tradition."  In 2006, the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". In a press release, the Library of Congress said: 

 
"B-movies|B-films during the studio era often resonate decades later because they explore issues and themes not found in higher-budget pictures. Robert Florey, widely acclaimed as the best director working in major studio B-films during this period, crafted an intriguing, taut thriller. Anna May Wong overcame Hollywood’s practice at the time of casting white actors to play Asian roles and became its first, and a leading, Asian-American movie star in the 1920s through the late 1930s. Daughter of Shanghai was more truly Wong’s personal vehicle than any of her other films. In the story she uncovers the smuggling of illegal aliens through San Francisco’s Chinatown, cooperating with costar Philip Ahn as the first Asian G-man of the American cinema."    

==References==
 

===Bibliography===
 
* Taves, Brian. Robert Florey, The French Expressionist. Lanham, Maryland: Scarecrow Press, 1986. ISBN 0-8108-1929-5.
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 