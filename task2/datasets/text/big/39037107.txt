Noobz
{{Infobox film
| name           = Noobz
| image          = Noobz-Movie-Poster.jpg
| alt            = 
| caption        = 
| director       = Blake Freeman
| producer       = 
| writer         = Blake Freeman Marvin Willson
| starring       = Blake Freeman Moises Arias Jason Mewes Jon Gries Matt Shively
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 97 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Noobz is a 2012 American comedy film.
==Plot== Xbox were spent wisely. When Cody (Blake Freeman) lost his job, he turned to video games for pleasure. Meanwhile, things go from bad to worse when Codys wife grows weary of being married to a gamer, and walks out on him. But when Codys pal Andy (Jason Mewes) rallies their old gang the Reign Clan to compete in the Cyberbowl Video Game Championship, it appears to be the perfect opportunity to put their gaming skills to use, and win a big cash prize in the process. With the old pals Oliver (Matt Shively) and "Hollywood" (Moises Arias) in tow, Cody and Andy set their sights on Los Angeles. Unfortunately for the Reign Clan, the forces of the universe seem to be working against them. Now, as the contest draws near and Andy finally gets some quality face time with gamer goddess Rickie (Zelda Williams), the Reign Clan find that their enemies on the virtual battlefield are no match for the real-life foes who are determined to see them fail.

==Cast==
*Blake Freeman as Cody
*Moises Arias as Hollywood
*Jason Mewes as Andy 
*Jon Gries as Greg Lipstein 
*Matt Shively as Oliver 
*Casper Van Dien as Casper Van Dien
*Sabrina Carpenter as Brittney
*Jesse Heiman as Computer Guy
*Lin Shaye as Mrs. Theodore 
*Carly Craig as Melissa 
*Mindy Sterling as Mrs. Robinson 
*Richard Speight Jr. as Jeff
*Brien Perry as Mr. Perry
*Bill Bellamy as Brian Bankrupt Simmons
*Zelda Williams as Rickie
*Chenese Lewis as Milkshake

== External links ==
*  

 
 
 
 
 
 


 