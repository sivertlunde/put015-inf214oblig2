Mummy Punjabi
{{Infobox film
| name           = Mummy Punjabi : Superman Ki Bhi Maa!!
| image          = Mummy punjabi film poster.jpg
| alt            =  
| caption        = 
| director       = Pammi Somal
| producer       = Pammi Somal 
Sonny Somal
| writer         = Pammi Somal
| screenplay     = 
| starring       = Kiron Kher  Kanwaljit Singh 
Jackie Shroff
| music          = Aadesh Shrivastava
| cinematography = Vivek
| editing        = Pammi Somal
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi 
Punjabi 
English
| budget         = 
| gross          = 
}} Kanwaljit Singh and Jackie Shroff in lead roles.

==Synopsis==
Mummy Punjabi is a New Age Mother India, with todays grandeur, intensity, humour & emotions. A complete family entertainer, wherein a Punjabi urban mother balances tradition with her own value system. 

==Cast==
* Kirron Kher as Baby Kaur  Kanwaljit Singh as Rajinder Singh 
* Jackie Shroff as Kanwal Sandhu 
* Anju Mahendru as Kanwals Sister 
* Divya Dutta  as Muniya - Maidservant 
* Viraf Patel as Dr. Arjun R. Arora
* Sachin Sharma as Karan R. Arora 
* Simran Vaid as Simran R. Arora 
* Supriya shukla as Bittu 
* Urwashi Gandhi as Jeena W. Puri 
* Michael Joseph as Sid 
* Freddy Daruwala as Sexy Sam 
* Kashif Khan as DJ Abraham
* Gurdas Maan as Dancer / Singer (as Gurdas Maan) 
* Satish Kaushik as Rajeev Bhalla 
* Rohit Roy as Salman Khan / Rahul Sharma

==Reception==
Mummy Punjabi received generally negative reviews from critics. Nikhat Kazmi of Times of India gave the film a 2 star rating out of 5 saying, "The problem with Mummy Punjabi lies in the fact that it is neither a comedy nor a serious film, just something in-between. Wonder who is the target audience!". 

Daily Bhaskar gave the film a single star out of five and said, "Nothing sums up ‘Mummy Punjabi,’ better than a brain-numbing movie with a mayhem stirring family drama. Add to this, outrageously bad acting by the supporting cast. This ones not recommended at all." 
==References==
 

==External links==
*  
*  

 
 
 
 