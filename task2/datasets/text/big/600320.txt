Scream 2
{{Infobox film
|name=Scream 2
|image_size=220px
|image=Scream 2.jpg
|caption=Theatrical release poster Kevin Williamson Jada Pinkett Liev Schreiber 
|director=Wes Craven
|producer=Wes Craven Cathy Konrad Marianne Maddalena
|cinematography=Peter Deming
|music=Marco Beltrami
|editing=Patrick Lussier
|studio=Konrad Pictures Craven-Maddalena Films
|distributor=Dimension Films United States
|released=   
|runtime=120 minutes
|language=English
|budget=$24 million
|gross=$172,363,301}} Kevin Williamson copycat killer using the guise of Ghostface (Scream)|Ghostface. Sidney is accompanied by film-geek Randy Meeks (Kennedy), retired deputy sheriff Dewey Riley (Arquette) and news reporter Gale Weathers (Cox). Like its predecessor, Scream 2 combines the violence of the slasher genre with elements of comedy and "whodunit" mystery while satirizing the cliché of film sequels. The film was followed by two sequels, Scream 3 (2000) and Scream 4 (2011).

Williamson provided a five-page outline for a sequel to Scream when auctioning his original script, hoping to entice bidders with the potential of buying a franchise. Following a successful test screening of Scream and the films financial and critical success, Dimension Films moved forward with the sequel while Scream was still in theaters, with the principal cast all returning to star, Craven to direct and Beltrami to provide music.
 leaking onto the Internet, revealing the identity of the killers. Combined with the films rushed schedule, the script was rewritten often with pages sometimes being completed on the day of filming. Despite these issues, Scream 2 was released to significant financial and critical acclaim, earning $172 million, several awards and nominations with some critics claiming that the film  had surpassed the original.

Beltrami received positive critical reception for his Scream 2 score for evolving the musical themes of the characters created in Scream although some critics claimed that the most memorable pieces from the film were created by composers Danny Elfman and Hans Zimmer, whose pieces were controversially used in the film, replacing Beltramis own work. The soundtrack received negative feedback from reviewers but achieved moderate sales success, reaching #50 on the Billboard 200|Billboard 200.

==Plot== Maureen Evans Debbie Salt, descend on Windsor College, where Sidney Prescott now studies alongside fellow Woodsboro survivor List of Scream characters#Randy Meeks|Randy, her boyfriend List of Scream characters#Derek|Derek, Dereks best friend List of Scream characters#Mickey|Mickey, and her best friend List of Scream characters#Hallie|Hallie. Sidney receives prank calls but is oblivious to the recent killings until someone instructs her to watch the news.
 Dewey Riley Gale Weathers Cotton Weary, who is attempting to gain fame from his exoneration for the murder of Sidneys mother. Sidney angrily hits Gale.

Later that night, Sidney unwillingly goes to a party with Hallie. At a sorority house, Ghostface kills fellow student List of Scream characters#Cici Cooper|Cici. He then crashes the party and attempts to murder Sidney, though Derek intervenes. The killer injures Derek, but Dewey and the police arrive, causing the killer to flee. The next day, Gale discusses the case with the police. Upon realizing that Cicis real name is Casey, she concludes that the killer is a copycat who targets students who share the same names as the Woodsboro murder victims . While Gale is talking to Dewey and Randy on the campus lawn, she receives a call from Ghostface hinting that he is watching them. They search for him, but Randy, who tries to keep the killer on the phone, is dragged into Gales broadcast van and stabbed to death. Dewey and Gale review the tape of Ghostface killing Randy, hoping to find some clues, but the killer attacks them and seemingly kills Dewey. Gale hides and eventually escapes. In the wake of the escalating murders, two officers prepare to take Sidney and Hallie to safety, but the killer ambushes them and kills the two officers. In the ensuing struggle, Ghostface is knocked unconscious. Sidney insists on unmasking him, while Hallie insists they escape. Before they can see his face, Ghostface wakes up and kills Hallie. Sidney flees.
 Billy Loomiss mother, seeking revenge for her sons death. Mrs. Loomis betrays Mickey and shoots him. Mickey shoots Gale before he collapses. Sidney and Mrs. Loomis fight until Cotton intervenes and shoots Mrs. Loomis. As they wait to see if Mrs Loomis is really dead, they find Gale still alive. Mickey suddenly leaps to his feet and is shot to death by Sidney and Gale. Sidney then turns and shoots Mrs. Loomis in the head to make sure she is dead. When the police arrive, Gale finds Dewey to be injured but alive and accompanies him to the hospital. Sidney instructs the press to direct questions to Cotton, rewarding him with the fame he has been chasing while removing the attention from herself.

==Production==

===Writing=== copycat Ghostface Ghostface killer Cotton Weary, leaked onto Randy Meeks and List of Scream characters#Joel|Joel. 

To avoid another such incident and prevent sensitive plot details being revealed through other means, the actors were not given the last pages of the script until weeks before shooting and the pages that revealed the killers identity were only provided on the day the scene was shot to the actors involved.  The short production schedule on Scream 2 and his work on other projects meant that Williamsons final script used for the film was detailed in some areas but lacking in others, with Wes Craven forced to write and develop certain scenes as they were being filmed. 

===Development===
Williamson had been contracted for two potential sequels to Scream when selling the script for the original, based on five-page proposals he attached to the script, hoping to entice prospective buyers with the fact that they were not just buying a film but a franchise,   and after a successful test screening for the original, at which Miramax executives were present, Craven was also given a contract to direct the two future films.  Dimension Films considered pursuing development of a sequel in January 1997 after Scream proceeded to gross more than $50 million in the first month of its release, with production being greenlit in March 1997 and an increased budget of $24 million over Scream s $15 million. 

The production of the film suffered a significant setback when the script was leaked, revealing plot details including the identity of the killers, resulting in the script being modified to change many details.  In an interview, Craven commented on the rushed six-month production schedule, with the film being expected ready for release on December 12, less than a year after the release of Scream, and Williamson forced to rewrite his script, pages for scenes would often only be ready on the day of filming and others lacked significant detail that forced Craven to develop them as the scenes took place.  Various titles were considered for the sequel at different points in the films production, including Scream Again, Scream Louder and Scream: The Sequel before the studio decided to simply use Scream 2. 

===Casting=== Gale Weathers, Dewey Riley, Randy Meeks and Liev Schreiber as the man exonerated for the murder of Sidneys mother.  Roger L. Jackson also returned to voice the character of Ghostface (Scream)|Ghostface.

Having finalized the returning principal cast from Scream, the production approached their casting for Scream 2 in a similar manner, seeking established and popular actors, largely sourcing from TV shows of the time. In interviews, the production staff of the film remarked that they found approaching and securing the talent they wanted significantly easier than it had been for Scream, considering the financial and critical success of the film but also believing the prior involvement of Drew Barrymore had lent the horror genre an element of credibility which made serious actors eager to become involved. 
 Cici Cooper, Mickey and Debbie Salt, Billy Loomis Buffy the Vampire Slayer and had only recently finished work on another Williamson-penned film, I Know What You Did Last Summer (1997). Despite the hectic scheduling, Gellar admitted in an interview that she agreed to perform in Scream 2 without having read the script because of the success of Scream.  Metcalf had only just finished her nine-year run on the popular sitcom Roseanne (TV series)|Roseanne when she began work on Scream 2 and Craven was praising of her ability to portray the deranged Mrs. Loomis.  To obtain the role of Derek, OConnell and other candidates had to audition by performing a scene from the film where the character sings "I Think I Love You".  Olyphants involvement as Mickey was his first leading role in a feature film. Despite scheduling difficulties, Craven took their desire to participate in the film despite their workload as a compliment to the films quality. 
 Jada Pinkett Maureen Evans Tatum Riley David Warner Casey respectively. Spelling was cast based on a sarcastic remark by Campbells character in Scream that she would be played by Spelling in a movie based on her life. Craven remarked that she was a "good sport" about the joke and happy to take part. 

===Filming=== Extras present Cici Cooper UCLA in Los Angeles were used to represent the fictional Windsor College which appears in the film. 
 filmsets and strict restriction on what personnel could be present during filming and have access to the script, with all present required to sign non-disclosure agreements. The script itself was reprinted on specialty paper to prevent photocopying and was often destroyed after use. 

===Post-production=== MPAA to receive an R-rating to help the film remain commercially viable,  sending eight different cuts and requiring the direct intervention of Dimension Films founder Bob Weinstein to eventually get the necessary rating to release the film without significant cuts. 
 character being Randy Meeks death that showed his throat being slashed.  Cravens reasoning was that the parts of the film they wished to keep would be more acceptable when viewed with the enhanced violence and so the MPAA would force them to remove the footage they already did not want to keep while passing the content they did want. However, the MPAA granted Scream 2 an R-rating for the more violent cut as they believed the underlying message of the film was significant enough to warrant the violence. 

===Music=== score for play and Broken Arrow placeholder for Beltramis incomplete score during a test screening. The test audience reaction to it influenced the studio keep the Zimmer piece, reducing "Deweys Theme", which Beltrami had composed to fill its place, to minor use during more serious scenes involving the character.  

==Release== Hollywood    followed by a general release on December 12, 1997, less than a year after the release of Scream.  After the unexpected success of Scream, by late 1997, Scream 2 was considered such a potential box office success that the James Bond film Tomorrow Never Dies and James Camerons future hit Titanic were moved from their release date of December 12 to December 19 so as to not face the film as competition. 

===Box office=== slasher genre, blockbuster films Men in Black. 

With $33 million, Scream 2 broke December opening weekend records for its box-office takings and held the record until December 15, 2000, being replaced by What Women Want (2000).  

{|class="wikitable" width=99% border="1"
!rowspan="2" align="center"|Release date (United States)
!rowspan="2" align="center"|Budget (estimated)
!colspan="3" align="center"|Box office revenue
!colspan="3" align="center" text="wrap"|Box office ranking
|-
!align="center"|United States
!align="center"|Foreign
!align="center"|Worldwide
!align="center"|Release year
!align="center"|All time U.S.
!align="center"|All time worldwide
|- December 12, 1997
|align="center"|$24,000,000 
|$101,363,301 
|$71,000,000 
|$172,363,301 
|align="center"|#21 
|align="center"|#499 
|align="center"|#579 
|-
|colspan="9"| 
Note(s)
*Box office ranking accurate as of October 2012.
 
|}

===Critical reception=== Time Out London was also mixed in its response, calling the film superior to most other horror films but poor in comparison to Scream,  while Variety (magazine)|Variety, scathing of Scream before its release, positively received the sequel, saying "  ponder whether any sequel ever topped the original. Scream 2 is certainly worthy of being part of that debate."  Empire (magazine)|Empires Kim Newman echoed this sentiment saying "Some great comic - and terrifying - moments, but suffers for not being as original as the original" remarking that though the film had the same elements that made Scream a success, they were no longer surprising for having been in Scream. 

There was however criticism over the death of the character   and violence over the original, saying that the films killer could literally be anyone as the audience is never provided with enough information about the new characters to form an opinion about them.  Ebert also commented upon this, saying "there is no way to guess whos doing the killing, and everyone who seems suspicious is (almost) sure to be innocent."  According to the review aggregate site Rotten Tomatoes, Scream 2 has an 81% average approval rating from 73 critics and is "Certified Fresh", indicating a majority positive reception. 

{|class="wikitable" width=50% border="1"
!align="center"|Rotten Tomatoes
!align="center"|Metacritic
|-
|81% (73 reviews)  63 (22 reviews) 
|-
|}

===Accolades=== Best Female Best Supporting Best Horror Film for Scream 2 itself.

{|class="wikitable"
|-
!Year!!Award!!Category!!Work!!Result
|- 1998
|rowspan="3"|Saturn Award Saturn Award Best Actress Neve Campbell
| 
|- Saturn Award Best Horror Film
|
| 
|- Saturn Award Best Supporting Actress Courteney Cox
| 
|- MTV Movie Award MTV Movie Best Female Performance Neve Campbell
| 
|- Razzie Award Razzie Award Worst New Star Tori Spelling
| 
|}

===Home media===
Scream 2 was released in US territories on VHS on December 1, 1998  by Buena Vista Home Entertainment. Scream 2 was released on DVD for the first time in the US on July 22, 1998  with a Collectors Edition following on August 7, 2001. The Collectors Edition featured additional material including outtakes, deleted scenes, the films theatrical trailer, music videos of songs featured in the film and directors commentary. Both releases were undertaken by Buena Vista Home Entertainment. Following the release of then series finale Scream 3, the three films were collected in "The Ultimate Scream Collection" by Dimension Films on September 26, 2000, in a boxset containing "Behind the Scream", a short documentary about the production of the three films and additional material including screentests of actors involved in the films, outtakes and deleted scenes.

Scream 2 remained unreleased in foreign territories including Europe and Japan until 2001 where it was released simultaneously with Scream and Scream 3 on February 26 by Buena Vista Home Entertainment.  Each film contained the additional content found in the Collectors Edition version of their US release including deleted scenes, outtakes, theatrical trailers, music videos and commentary from each respective films crew.    Additionally, the three films were collected together in a single pack, again released on February 26 and released as "Scream Trilogy". 

Scream 2 was released individually and in a collection with Scream and Scream 3 on Blu-ray Disc on March 29, 2011, two weeks prior to the theatrical release of Scream 4, by Lionsgate Home Entertainment. In addition to the films, each release contained audio commentary, theatrical trailers and behind-the-scenes footage. 

==Soundtrack==
{{Infobox album
|Name=Scream 2: Music from the Dimension Motion Picture
|Type=Soundtrack
|Longtype=
|Artist=Various Artists
|Cover=Scream 2 Soundtrack Cover.jpg Cover size=
|Caption=
|Released=November 18, 1997
|Recorded= 1970-1997 Rock
|Length=1:02:09
|Language=
|Label=Capitol Records
|Producer= Compiled by=
|Chronology= Last album=  (1996) This album=Scream 2: Music from the Dimension Motion Picture (1997) Next album=  (2000)
|Misc=}}
The Scream 2 original soundtrack was released November 18, 1997 by the   by the Recording Industry Association of America, signifying that the album achieved sales in excess of 500,000 units. 
{{Track listing
|collapsed=no
|headline=Scream 2: Music from the Dimension Motion Picture
|writing_credits=yes
|extra_column=Artist
|total_length=1:02:09
|title1=Scream
|writer1=Master P
|extra1=Master P, Silkk The Shocker
|length1=3:30
|title2=Suburban Life
|writer2=Kottonmouth Kings and AK Brothers
|extra2=Kottonmouth Kings
|length2=3:34 title3 = Rivers writer3 = Sugar Ray and McG extra3 = Sugar Ray length3 = 2:50 title4 = Shes Always in My Hair writer4 = Prince
|extra4 = DAngelo length4 = 6:19 title5 = Help Myself writer5 = David J Matthews extra5 = Dave Matthews Band length5 = 4:31 title6 = She Said writer6 = Ed Roland extra6 = Collective Soul length6 = 4:51 title7 = Right Place, Wrong Time writer7 = Malcolm "Mac" Rebennack aka Dr. John extra7 = The Jon Spencer Blues Explosion length7 = 3:16 title8 = Dear Lover writer8 = Dave Grohl, Pat Smear and Nate Mendel extra8 = Foo Fighters length8 = 4:33 title9 = Eyes of Sand writer9 = Tonic
|extra9 = Tonic length9 = 4:16 title10 = The Swing writer10 = Everclear
|extra10 Everclear
|length10 = 2:59 title11 = I Think I Love You writer11 = Tony Romeo extra11 = Less Than Jake length11 = 2:03 title12 = Your Lucky Day in Hell writer12 = E and Mark Goldenberg extra12 = Eels
|length12 = 4:26 title13 = Red Right Hand writer13 = Mick Harvey, Nick Cave, and Thomas Wydler extra13 = Nick Cave and the Bad Seeds length13 = 8:23 title14 = One More Chance writer14 = Kelly and T Smoov extra14 = Kelly length14 = 4:14 title15 = The Race writer15 = David Arquette, Gabe Cowan, and Sammy Music extra15 = Ear2000
|length15=2:03}}

===Score=== Filmtracks as "a frenzied, choral-enhanced three minutes".   The length of the released score was considered disappointing with some reviews considering the track omissions the result of the high fees required to be paid to composers in order to release their music commercially. 
 Broken Arrow, particularly the tracks "Brothers" and "Secure", featuring guitar work by Duane Eddy, would become a component of the theme tune of the character Dewey Riley. Filmtracks was complimentary to the score, appreciating Beltramis evolution of his work in Scream but remarked that the most memorable music of the film would be from other composers, Elfmans contribution "Cassandra Aria" and the samples from Zimmers score, finding the replacement of Beltramis work for the Dewey character with Zimmers work "effective". Ultimately, the album was called unsatisfying without Elfmans piece and it was awarded 2 stars out of 5.  The music guide AllMusic was more complimentary of Beltramis contribution, saying the score "perfectly captured the post-modern, hip scare-ride of the Scream movies", and awarded it 3 stars out of 5. 
{{Track listing
| collapsed       = no
| headline= Scream/Scream 2 Original Score
| writing_credits = no
| extra_column = Artist
| total_length = 29:46
| title1 = Sidneys Lament  
| writer1 =
| extra1 = Marco Beltrami
| length1 = 1:37
| title2 = Altered Ego  
| writer2 =
| extra2 = Marco Beltrami
| length2 = 2:47
| title3 = Trouble in Woodsboro  
| writer3 =
| extra3 = Marco Beltrami
| length3 = 1:49
| title4 = A Cruel World  
| writer4 =
| extra4 = Marco Beltrami
| length4 = 1:53
| title5 = Chasing Sidney  
| writer5 =
| extra5 = Marco Beltrami
| length5 = 1:27
| title6 = NC-17  
| writer6 =
| extra6 = Marco Beltrami
| length6 = 3:03
| title7 = Stage Fright Requiem  
| writer7 =
| extra7 = Marco Beltrami
| length7 = 2:07
| title8 = Love Turns Sour  
| writer8 =
| extra8 = Marco Beltrami
| length8 = 4:44
| title9 = Cici Creepies  
| writer9 =
| extra9 = Marco Beltrami
| length9 = 1:13
| title10 = Deputy for a Friend  
| writer10 =
| extra10 = Marco Beltrami
| length10 = 2:17
| title11 = Hollow Parting  
| writer11 =
| extra11 = Marco Beltrami
| length11 = 1:47
| title12 = Dewpoint and Stabbed  
| writer12 =
| extra12 = Marco Beltrami
| length12 = 2:15
| title13 = Hairtrigger Lunatic  
| writer13 =
| extra13 = Marco Beltrami
| length13 = 1:11
| title14 = Sundown Search  
| writer14 =
| extra14 = Marco Beltrami
| length14 = 0:50
| title15 = Its Over, Sid  
| writer15 =
| extra15 = Marco Beltrami
| length15 = 0:46
}}

==References==
{{Reflist|2|refs=

   

   

   

   

   

   

   

   

   

   

   

   

   

   

   

 

   

   

   

   

   

   

   

   

   

   

   

   

   

   
 date = March 1998|accessdate= April 16, 2011}} 

   

   

   

   

   

   

   

   

   

   

   

   

   }}

==External links==
 
* 
* 
* 
* 
* 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 