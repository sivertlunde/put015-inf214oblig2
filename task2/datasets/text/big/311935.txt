The Good Son (film)
 
{{Infobox film
| name = The Good Son
| image = goodson.jpg
| alt = 
| caption = Theatrical release poster
| director = Joseph Ruben
| producer = Joseph Ruben Mary Ann Page
| writer = Ian McEwan
| starring = Macaulay Culkin Elijah Wood
| music = Elmer Bernstein
| cinematography = John Lindley George Bowers
| distributor = 20th Century Fox
| released =  
| runtime = 87 minutes
| country = United States
| language = English
| budget = $17 million
| gross = $60.6 million
}}

The Good Son is a 1993 American psychological thriller film directed by Joseph Ruben and written by English novelist Ian McEwan. The film stars Macaulay Culkin and Elijah Wood.

==Plot==
 David Morse), to stay with his uncle Wallace (Daniel Hugh Kelly) and aunt Susan (Wendy Crewson) in Maine. Mark is re-introduced to his extended family, including his cousins Connie (Quinn Culkin) and Henry (Macaulay Culkin). Mark and Henry get along at first, and Henry seems to be nice and well-mannered. In discussing the death of Marks mother and that of Henrys baby brother Richard, however, Henry expresses an abnormal fascination with death, making Mark uneasy.

Henry continues to display increasingly psychopathic behavior, which Mark is unable to tell Wallace and Susan about due to Henrys dark threats. Later, Henry insinuates he will try to kill his sister. Terrified that something might happen to Connie, Mark spends the night in her room. The next morning, Mark awakens to find Henry has taken Connie ice skating. At the pond, Henry purposely throws his sister toward thin ice. The ice collapses and Connie nearly drowns before she is rescued and taken to the hospital. Susan becomes suspicious of Henry when he visits Connies room, planning to smother her, but Susan, who has been sitting in the dark out of view, interrupts him.

Susan finds a rubber duck Henry has hidden; it had once belonged to Richard and was with him in the bathtub the night he drowned, after which it went missing. When Susan confronts Henry, he coldly reminds her the toy had belonged to him before it had been Richards and asks for it back. Susan refuses, and Henry tries to take it from her. After a violent tug-of-war, Henry takes the toy and escapes to the cemetery where he throws it down a well, indicating that Richard meant nothing to him.

As Susan and Mark grow closer, Henry insinuates he will kill Susan rather than let Mark continue to develop a relationship with her. When a struggle breaks out between the two boys, Wallace locks Mark in the den. Henry asks a suspicious Susan to go for a walk with him, while Mark escapes the den and chases after them. Susan firmly asks if Henry killed his brother. Henry indirectly replies, "What if I did?"

Horrified by what her son has become, Susan tells Henry that he needs help, but Henry flees into the woods. Susan gives chase, and upon arriving at a cliff, Henry shoves her over the edge. As Susan dangles precariously, Henry picks up a large rock he intends to throw down at her, but Mark intervenes and tackles his cousin, and they fight while Susan climbs back up. In the ensuing brawl, the boys roll off the cliff, but are caught by Susan. She arduously hangs onto both boys, each with one hand. Henry holds on with both hands, but Marks one-handed grip begins to slip. With only enough strength to save one of them, Susan notes her disturbed sons icy-calm demeanor, confident that his mother will save him. However, Susan, now aware of her sons deeply evil nature, allows Henry to slip from her grasp and he falls to his death. Susan pulls Mark up from the ledge, and they look down upon Henrys corpse on the rocks below, before it is washed away into the sea. Susan and Mark share an emotional embrace.

When Mark returns to Arizona, he reflects upon Susans choice to save him instead of Henry and wonders if she would make the same choice again, but knows it is something he will never ask her.

==Cast==
*Macaulay Culkin as Henry Evans, 13 years old.
*Elijah Wood as Mark Evans, 12 years old.
*Wendy Crewson as Susan Evans, 36 years old. David Morse as Jack Evans, 39 years old.
*Daniel Hugh Kelly as Wallace Evans, 45 years old.
*Jacqueline Brookes as Alice Davenport, 63 years old.
* Quinn Culkin as Connie Evans, 8 years old.
* Ashley Crow as Janice Evans, 33 years old.

==Critical reception==
The film received a mostly negative response from critics, with a 24% overall score on Rotten Tomatoes.  Roger Ebert, who deemed the film inappropriate for children, awarded it just half a star, calling the project a "creepy, unpleasant experience".  He and Gene Siskel later gave it "Two Thumbs Down": 

 }}
 Saturn Award Best Performance MTV Movie Best Villain. 

===Box office===
The Good Son earned United States dollar|US$44,789,789 at the North American box office revenues, and another $15,823,219 in other territories, for a total worldwide box office take of $60,613,008.  

==Development==

Following the completion of his novel The Child in Time, English novelist Ian McEwan was invited by 20th Century Fox to write a screenplay "about evil - possible concerning children."  McEwan recalled, "The idea was to make a low budget, high class movie, not something that Fox would naturally make a lot of money on."
 Brian Gilbert was attached as director, but it collapsed thereafter.
 The Silence of the Lambs, which respectively demonstrated the appeal of both a movie about kids and of an "extreme thriller," Fox itself chose to revisit the project, which they now saw as viable. Director Michael Lehmann (Heathers) became attached, Laurence Mark was appointed as a co-producer, and McEwan was called in for rewrites. Mary Steenburgen was cast as the mother with two unknowns (including Jesse Bradford as the bad seed) starring as the boys. McEwan was optimistic about the project and by November 1991, sets were being built in Maine for a production that would cost approximately $12 million.

This progress was suddenly interrupted when  . Fox agreed enthusiastically due to Culkins bankability. As the movie was originally scheduled to shoot at the same time as Home Alone 2, the start date for The Good Son was pushed back for a year, making Steenburgen no longer available but enabling Elijah Woods involvement.

Director Lehmann and producer Mark conflicted with the imposition, leading both to leave the project. The demanding Culkin would go on to insist that his daughter Quinn receive a role in the film and vetted replacement director Joseph Ruben (Sleeping with the Enemy). Furthermore, the budget had risen to an estimated $20 million.

McEwan found himself performing further rewrites that continued to simplify the story to satisfy Rubens comparatively mainstream tastes, and was ultimately unceremoniously removed from the project altogether when another screenwriter was commissioned. Despite this, McEwan was awarded sole writing credit in arbitration when he contested a shared credit. 

==Novel==

A tie-in novel was published alongside the movies release in 1993, written by Todd Strasser. The novel elaborates on the movie, detailing how Henry was born a antisocial personality disorder|sociopath, rather than being some personification of evil.

In the novel, Henrys mother Susan eventually discovers that Henry is unable to understand emotions like love and sorrow, and that pleasure derived from selfish actions and the torment of others are the few things he truly feels. The book also concludes differently from the movie, ending with Mark returning to Uncle Wallaces home in Maine one year later. Mark and Susan visit Henrys grave, which includes an epitaph: "Without Darkness There Can be No Light".

Also in the novelization, Wallace and Susans date is cut short because Susan gets a feeling something is wrong at the house.

==References==
 

==External links==
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 