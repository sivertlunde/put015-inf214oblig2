Caravaggio (1986 film)
 
 
{{Infobox film
| name           = Caravaggio
| image          = Caravaggio poster.jpg
| image_size     =
| caption        =
| director       = Derek Jarman
| producer       = Sarah Radclyffe
| writer         = Derek Jarman Suso Cecchi dAmico Nicholas Ward-Jackson (story)
| starring       = Nigel Terry Sean Bean Tilda Swinton
| music          = Simon Fisher-Turner
| cinematography = Gabriel Beristain
| editing        = George Akers
| distributor    = Cinevista (USA) Umbrella Entertainment (AUS)
| released       = United States: 29 August 1986
| runtime        = 93 minutes
| country        = United Kingdom
| language       = English
| budget         =£450,000 

}}
 Michelangelo Merisi da Caravaggio.

==Plot== Spencer Leigh) (who was given by his family to the artist as a boy) by his side. Caravaggio thinks back to his life as a teenage street ruffian (Dexter Fletcher) who hustles and paints. While taken ill and in the care of priests, young Caravaggio catches the eye of Cardinal Del Monte (Michael Gough). Del Monte nurtures Caravaggios artistic and intellectual development but also appears to molest him.
 article on the painter for examples). He is depicted as frequently brawling, gambling, getting drunk and is implied to sleep with both male and female models, including the male Jerusaleme and the female contortionist Pipo (Dawn Archibald). In the art world, Caravaggio is regarded as vulgar and entitled due to his Vatican connections.

One day, Ranuccio (Sean Bean), a street fighter for pay, catches Caravaggios eye as a subject and potential lover. Ranuccio also introduces Caravaggio to his girlfriend Lena (Tilda Swinton), who also becomes an object of attraction and a model to the artist. When both Ranuccio and Lena are separately caught kissing Caravaggio, each displays jealousy over the artists attentions. One day, Lena announces she is pregnant (although she does not state who the father is) and will become a mistress to the wealthy Scipione Borghese (Robbie Coltrane). Soon, she is found murdered by drowning. As the weeping Ranuccio looks on, Caravaggio and Jerusaleme clean Lenas body. Caravaggio is shown painting Lena after she dies and mournfully writhing with her nude body. Ranuccio is arrested for Lenas murder, although he claims to be innocent. Caravaggio pulls strings and goes to the Pope himself to free Ranuccio. When Ranuccio is freed, he tells Caravaggio he killed Lena so they could be together. In response, Caravaggio cuts Ranuccios throat, killing him. Back on his deathbed, Caravaggio is shown having visions of himself as a boy and trying to refuse the last rites offered him by the priests.

In keeping with Caravaggios use of contemporary dress for his Biblical figures, Jarman intentionally includes several anachronisms in the film that do not fit with Caravaggios life in the 16th century. In one scene, Caravaggio is in a bar lit with electric lights. Another character is seen using an electronic calculator. Car horns are heard honking outside of Caravaggios studio and in one scene Caravaggio is seen leaning on a green truck. Cigarette smoking, a motorbike, and the use of a manual typewriter also feature in the film.

==Details and awards==
Caravaggio was Jarmans first project with Tilda Swinton and was also her first film role. The cook Jennifer Paterson was an extra. The production designer was Christopher Hobbs who was also responsible for the copies of Caravaggio paintings seen in the film. The film was entered into the 36th Berlin International Film Festival where it won the Silver Bear for an outstanding single achievement.   

==Home media==
Caravaggio was released on DVD by Umbrella Entertainment in July 2008. The DVD is compatible with all region codes and includes special features such as the trailer, a gallery of production designs and storyboards, feature commentary by Gabriel Berestain, an interview with Christopher Hobbs titled Italy of the Memory, and interviews with Tilda Swinton, Derek Jarman, Nigel Terry.   

==Cast==
 
* Noam Almaz as Boy Caravaggio
* Dawn Archibald as Pipo
* Sean Bean as Ranuccio
* Jack Birkett as The Pope
* Sadie Corre as Princess Collona
* Una Brandon-Jones as Weeping Woman
* Imogen Claire as Lady with the Jewels
* Robbie Coltrane as Scipione Borghese
* Garry Cooper as Davide
* Lol Coxhill as Old Priest
* Nigel Davenport as Giustiniani
* Vernon Dobtcheff as Art Lover
* Terry Downes as Bodyguard
* Dexter Fletcher as Young Caravaggio
* Michael Gough as Cardinal Del Monte
* Jonathan Hyde as Baglione Spencer Leigh as Jerusaleme
* Emile Nicolaou as Young Jerusaleme
* Gene October as Model Peeling Fruit
* Cindy Oswin as Lady Elizabeth John Rogan as Vatican Official
* Zohra Sehgal as Jerualemes Grandmother
* Tilda Swinton as Lena
* Lucien Taylor as Boy with Guitar
* Nigel Terry as Caravaggio
* Simon Fisher Turner as Fra Fillipo (as Simon Turner)
 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 