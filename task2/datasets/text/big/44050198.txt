Kondaveeti Simham
{{Infobox film
| name = Kondaveeti Simham
| image = Kondaveeti Simham.jpg
| image_size =
| caption =
| director = K. Raghavendra Rao
| producer = M Arjuna Raju K Sivaramaraju Nirvahana Kumarji
| writer = Satyanand (dialogues)
| story = J. Mahendran Jayanthi Mohan Babu
| music = K. Chakravarthi
| cinematography = K S Prakash
| editing = Kotagiri Venkateswara Rao
| studio = Roja Movies
| distributor = Roja Movies
| released =  
| country = India Telugu
}}
 Telugu Action action drama Jayanthi and Mohan Babu in the lead roles. The film had musical score by K. Chakravarthi.    The film was a remake of Tamil film Thanga Pathakkam.

==Cast==
 
*N. T. Rama Rao as Ranjit Kumar
*Sridevi as Devi Jayanthi as Annapurna
*Mohan Babu as Ravi Geetha as Ravis wife
*Rao Gopal Rao as Devis father
*Kaikala Satyanarayana as Sher Khan
*Allu Rama Lingaiah as China Yuddham
*Nagesh
*Chalapathi Rao
*Jaggarao
*Suthi Veerabhadra Rao
*P. J. Sarma
*Chidatala Apparao
*Subhashini
*Pushpalatha
*Jhansi Sri Lakshmi Girija Rani
*Master Hari
*Neelaveni
*Thyagaraju Kanta Rao in Guest Appearance
*Mukkamala in Guest Appearance
 

==Soundtrack==
The music was composed by K. Chakravarthy and lyrics was written by Veturi Sundararamamurthy.  
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|-  Susheela || Veturi Sundararamamurthy || 04.19 
|-  Susheela || Veturi Sundararamamurthy || 04.01 
|-  Susheela || Veturi Sundararamamurthy || 04.10 
|-  Susheela || Veturi Sundararamamurthy || 03.22 
|-  Susheela || Veturi Sundararamamurthy || 04.21 
|}

==References==
 

==External links==
* 
*  

 
 
 
 
 
 
 

 