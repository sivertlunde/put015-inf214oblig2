Season in Cairo
Season German musical musical comedy film directed by Reinhold Schünzel and starring Renate Müller, Willy Fritsch and Gustav Waldau.  A French-language version Idylle au Caire was released, also featuring Müller.

==Cast==
* Renate Müller - Stefanie von Weidling-Weidling, Tochter 
* Willy Fritsch - Tobby Blackwell, Sohn 
* Gustav Waldau - Leopold Graf von Weidling-Weidling 
* Leopoldine Konstantin - Ellinor Blackwell 
* Anton Pointner - Giacomo Ottaviani 
* Jakob Tiedtke - Exzellenz Ismael Pascha 
* Angelo Ferrari - 1st Gigolo 
* Kurt Hagen - 2nd Gigolo 
* Erik Ode - 3rd Gigolo

==References==
 

==Bibliography==
* Bergfelder, Tim & Bock, Hans-Michael. The Concise Cinegraph: Encyclopedia of German. Berghahn Books, 2009.
* Elsaesser, Thomas. Weimar Cinema and After: Germanys Historical Imaginary. Routledge, 2000.
* Hake, Sabine. Popular cinema of the Third Reich. University of Texas Press, 2001.

==External links==
* 

 

 
 
 
 
 
 
 


 