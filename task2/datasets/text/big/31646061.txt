Arirang (2011 film)
{{Infobox film 
| name           = Arirang
| image          = Arirang.png
| caption        = Festival poster
| film name =  |rr=Arirang|mr=Arirang}}
| studio         = Kim Ki-duk Film
| distributor    = 
| released       =  
| runtime        = 100 minutes
| country        = South Korea
| language       = Korean
| budget         = 
}} the same title. In a heavily Line break (poetry)|line-broken text released about the film, Kim writes that "Through Arirang I understand human beings, thank the nature, and accept my life as it is now."    Kim produced the film entirely on his own.  It premiered in the Un Certain Regard section of the 2011 Cannes Film Festival, and won the top award for best film.

==Release==
The film premiered on 13 May at the 2011 Cannes Film Festival where it was screened in Un Certain Regard.    Kim only did one interview at the festival, where he said that he felt better than when he made the film, and that it had helped him cure himself. At one occasion during the interview, Kim suddenly started to sob and sang "Arirang". 

===Reception===
After attending the festival screening in Cannes,  , Dan Fainaru called it "the ultimate film dauteur", and wrote: "Doing it justice in a short review is almost impossible, not because Kim Ki-duk is providing revolutionary insights into his line of work, but because he raises numerous issues that are too often ignored as irrelevant or pedantic by professionals who should know better."  Peter Bradshaw wrote: "It is the most extravagantly self-indulgent piece of pure loopiness imaginable – but gripping as well. A piece of experimentalism at odds with convention."    	

The film won the Prize of Un Certain Regard,  the top award for best film in the section. The win was shared with the German film Stopped on Track, directed by Andreas Dresen. 

==References==
 

==Publications==
MARTONOVA, (2012) A. To feel HAN (  by Kim Ki-duk) // Kino, No.3, Sofia:p.49-47, ISSN 0861-4393   

== External links ==
*  
 
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 