Give Out, Sisters
{{Infobox film
| name           = Give Out, Sisters
| image          =
| caption        =
| director       = Edward F. Cline
| producer       = 
| writer         = 
| starring       = 
| music          =
| cinematography = 
| editing        = 
| distributor    = Universal Pictures
| released       =  
| runtime        = 65 min.
| country        = United States
| language       = English 
| budget         =
}}

Give Out, Sisters is a 1942 American film starring The Andrews Sisters.

The film co-stars Dan Dailey, and the teenage couple of the time, Donald OConnor and Peggy Ryan. Dailey and OConnor went on to be in the 1954 film Theres No Business Like Show Business.

==Cast==

* The Andrews Sisters as Themselves
* Grace McDonald as Gracie Waverly
* Dan Dailey as Bob Edwards  Charles Butterworth as Prof. Woof
* Walter Catlett as Gribble
* William Frawley as Harrison
* Donald OConnor as Don
* Peggy Ryan as Peggy
* Edith Barrett as Agatha Waverly
* Marie Blake as Biandina Waverly
* Fay Helm as Susan Waverly
* Emmett Vogan as Batterman
* Leonard Carey as Jamison - The Waverly Butler Richard Davies as Kendall
* Irving Bacon as Dr. Howard
* The Jivin Jacks and Jills as Dancers
* Leon Belasco as Waiter
* Robert Emmett Keane as Lawyer Peabody
* Lorin Raker as Dr. Bradshaw
* Jason Robards Sr. as Drunk
* Duke York as Louie
* Alphonse Martell as Headwaiter
* Emmett Smith as Porter
* Fred Snowflake Toones as Nightclub Valet

==External links==
*  

 

 
 
 
 
 
 


 