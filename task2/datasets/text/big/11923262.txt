Brain Dead (1990 film)
 
{{Infobox Film
  | name = Brain Dead
  | image = Brain Dead (1990 film).jpg
  | caption = Movie poster
  | director = Adam Simon
  | producer = Julie Corman (producer)  Lynn Whitney (associate producer)
  | writer = Charles Beaumont Adam Simon 
  | starring = Bill Pullman Bill Paxton Bud Cort George Kennedy
  | music = Peter Rotter
  | cinematography = Ronn Schmidt    
  | editing = Carol Oblath    
  | released = 19 January 1990
  | runtime = 85 minutes
  | country = United States English
  | budget = $ 2.000.000 (IMDB estimate) gross          =$1.6 million 
  }}

Brain Dead is a 1990 Horror film|horror/psychological thriller starring Bill Pullman, Bill Paxton and George Kennedy and written by Charles Beaumont.

==Plot==
Dr. Rex Martin is a top neurosurgeon, who is active in studying brain malfunctions that cause mental illnesses. High school friend Jim Reston, a successful businessman at Eunice, requires Martins aid in reaching the mind of John Halsey, a former genius mathematician who once worked for the company and is now a paranoid psychotic at a nearby insane asylum|asylum. Dr. Martins surgery is intended to successfully alter the patients mental attitude, either unlocking the corporate secrets within Halseys brain or else leaving Halsey unable to accidentally share them with anyone else. As Martin begins the surgical procedure, he starts to experience the same paranoid dreams as Halsey. The episodes grow in intensity until it becomes unclear whether Martin is a doctor imagining hes the patient, or a mental patient who succumbed to the delusion that he was a brain surgeon. Martin floats further and further from reality, caught between his loyalty to his business colleagues and his own humanity.

==Cast==
*Bill Pullman as Dr. Rex Martin 
*Bill Paxton as Jim Reston 
*Bud Cort as John Halsey
*Nicholas Pryor as Man in Bloody White Suit/Ramsen/Ed Conklin 
*Patricia Charbonneau as  Dana Martin 
*George Kennedy as  Vance 
*Brian Brophy as  Ellis 
*David Sinaiko as  Berkovitch 
*Lee Arenberg  as Tramp
*Andy Wood as  Brain Surgeon 
*Maud Winchester as  Crazy Anna 
*Lisa Moncure as  Board Member 
*Jon Kellam as  Board Member 
*Willie Garson as  Board Member 
*John Paxton as  Board Member 
*David Sklare as  Board Member

==References==
 
==External links==
* 
*  
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 