The Gay Cavalier (film)
 

{{Infobox film
| name           = The Gay Cavalier
| image          = The Gay Cavalier (film).jpg
| director       = William Nigh
| produced by    =
| cinematography = Harry Neumann
| music          = Edward J. Kay
| editing        =
| distributor    = Monogram Pictures Corporation
| writers        = O. Henry Charles Belden Tristram Coffin Martin Garralaga Nacho Galindo Ramsay Ames
| released       =  
| runtime        = 65 min.
| country        = United States English
}} 1946 black Western adventure Tristram Coffin. It is based on a story by the author O. Henry.

== Plot ==

Roland plays The Cisco Kid, who sets out on a double mission. He must prevent a girl from marrying a wealthy suitor in order to save her familys hacienda, thus forsaking her true love in doing so; and also apprehend the outlaws who robbed a stagecoach carrying gold to a local Mission (Christian)|mission. He eventually finds that the wealthy suitor is behind the gold robbery, a revelation that makes his task much easier.

== Cast ==

* Gilbert Roland as Cisco Kid
* Martin Garralaga as Don Felipe Geralda
* Nacho Galindo as "Baby"
* Ramsay Ames as Pepita Geralda
* Helen Gerald as Angela Geralda Tristram Coffin as Lawton
* Gil Frye as Juan (as Drew Allen)
* Iris Flores as Fishermans wife
* John Merton as Lewis Frank LaRue as Graham

== External links ==
*   at the Internet Movie Database

 

 
 
 
 
 
 
 


 