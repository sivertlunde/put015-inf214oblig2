Napoleon Bunny-Part
{{multiple issues|
 
 
 
 
}}
{{Infobox Hollywood cartoon|
| cartoon_name = Napoleon Bunny-Part
| series = Merrie Melodies (Bugs Bunny)
| director = Friz Freleng
| story_artist = Warren Foster Art Davis
| layout_artist = Hawley Pratt
| background_artist = Irv Wyner
| voice_actor = Mel Blanc
| musician = Carl Stalling
| distributor = Warner Bros. Pictures
| release_date = June 16, 1956
| color_process = Technicolor
| runtime = 7:08
| movie_language = English
| preceded_by = Rabbitson Crusoe
| followed_by = Barbary Coast Bunny
}}
 
Napoleon Bunny-Part is a Warner Bros. animated cartoon of the Merrie Melodies series, directed by Friz Freleng.

==Plot== Napoleon Bonaparte, Mugsy then Josephine and asks Napoleon to dance. Napoleon sees Bugs tail through the disguise and chases Bugs, who slides down a staircase. The guard sees Bugs and points his bayonet, intending to stab Bugs as he slides down. Bugs hops off of the staircase and Napoleon is stabbed instead offscreen-he comes back up the staircase screaming in pain. Mugsy then gets the "point" again.

Napoleon ends up catching Bugs by holding him at gunpoint and orders him into a guillotine. Bugs runs off with Napoleon in pursuit, running down the guillotine platform and then back up and through the guillotine which is still raised. Napoleon follows Bugs but as he runs through the guillotine the blade falls and slices the back of his uniform along with the hair from the back of his head. Napoleon demands an explanation from the hooded executioner, who is revealed to be Bugs himself. Bugs then tries to elude Napoleon by hiding in a cannon, but is found when Mugsy is ordered to sound the alarm and fires the cannon.

Napoleon resumes his chase of Bugs, but two men in white coats show up and one says to the other: "Hey Pierre, heres another Napoleon", and Pierre replies: "thats the twelfth one today." "But I am Napoleon", the little  Commander wails as hes dragged away by them to the nearest insane asylum or "maison didiot ("Sure, you are", one of the men nods sarcastically). Napoleon screams and threatens the men in white coats with death: "I will have you executed for this!!!"

Bugs then looks at the camera and says: "Imagine that guy thinking hes Napoleon...  when I really am!". He pulls out a flute, playing "La Marseillaise", which becomes "Yankee Doodle" as he marches into the distance.

==Availability==
Napoleon Bunny-Part is available, uncensored and uncut, on the Looney Tunes Superstars DVD. However, it was cropped to widescreen. It is also available on The Best of Bugs Bunny DVD, also cropped to widescreen.

 
{{succession box |
before= Rabbitson Crusoe | Bugs Bunny Cartoons |
years= 1956 |
after= Barbary Coast Bunny|}}
 

 
 
 
 
 
 
 