Hou De Jarasa Ushir
{{Infobox film
| name           = Hou De Jarasa Ushir
| image          = Marathi_Movie,_Hou_De_Jarasa_Ushir_Movie_Poster.jpg
| alt            =  
| caption        = Movie poster
| director       = Wasim Maner
| producer       = Tahir Maner
| distributor    = Abhijit Gholap
| writer         = Wasim Maner
| screenplay     = Wasim Maner, Rahul Ovhal
| starring       = Sadashiv Amrapurkar Aishwarya Narkar Sharvari Jamenis Chinmay Mandlekar Raj Ranaware
| music          = Nakul Jogdeo
| lyrics         = Rahul Ovhal
| cinematography = Wasim Maner, Meethil Momaya
| editing        = Prashant Pandav
| studio         = Biroba Films
| released       =  
| runtime        = 
| country        = India
| language       = Marathi
| budget         = 
| gross          = 
}}
 Marathi film directed by Wasim Maner, Produced by Tahir Maner under Biroba Films and distributed by Abhijit Gholap of Devisha Films]. Hou De Jarasa Ushir is the first Marathi film to be selected among top 282 films for a nomination in Best Film Category for Academy Award for Best Picture|Oscars.  The films screenplay has already been preserved by Academy of Motion Picture Arts and Sciences library for research purposes.

== Cast ==

* Sadashiv Amrapurkar as Fakir Baba
* Aishwarya Narkar as Rukmini Pawar
* Sharvari Jamenis as Sunaina Pandit
* Chinmay Mandlekar as Sarjerao Patil
* Aditi Sarangdhar as Monali Mohite
* Jayawant Wadkar as Marutrao Mohite
* Raj Ranaware as Malhar Gaikwad 
* Abhyangh Kuvalekar as Shantanu Jahagirdar
* Anil Nagarkar as Shabbir Tamboli
* Priya Shinde as Swapna
* Vishwas Sakat as Bhavadya
* Akshay Waghmare as Mohanya
* Rajesh More as Dattu Pawar
* Manoj Narule as Sunil Pawar
* Sanyogita Bhave as Sneha Gosavi
* Mahesh Ghag as Shankar

== Plot ==

Hou De Jarasa Ushir (Let There Be A Little Delay) is the story of three people working at a software company. Every morning, they travel to work together in the same cab. HDJU paints a single day in their life where the journey from their homes to the office makes them to look at the world quite differently.

== Release ==

Hou De Jarasa Ushir is released on January 18, 2013 all over Maharashtra.  It was released in California United States on November 23, 2012.

== Music ==

Music of Hou De Jarasa Ushir is composed by Nakul Shirish Jogdeo and lyrics are written by Rahul Gautam Ovhal. 
{{track listing
| headline      = Track listing
| music_credits = no
| extra_column    = Singer(s)
| total_length  = 
| title1  = Piraticha-Tujhya Majhya Piraticha
| note1   = A bubbly romantic farmer-song
| extra1  = Chandan Kamble, Sonia Mundhe
| length1 = 02:48
| title2  = Kashas Ghai
| note2   = A light romantic song
| length2 = 03:33
| extra2  = Hamsika Iyer, Gandhar Sangoram
| title3  = Bolawanarya Akashache
| note3   = A soft rock song
| extra3  = Mandar Phatak
| length3 = 04:03
| title4  = Dhampak Dhampak
| length4 = 03:29
| note4   = A rap song Sadashiv Amarapurkar
| title5  = Vithoo Aai
| note5   = A bhajan
| length5 = 04:25
| extra5  = Rucha Bondre
| title6  = Lagnala Jayacha
| note6   = A comedy ‘band-baja’ song
| length6 = 03:00
| extra6  = Raj Ranaware, Vishas Sakat
| title7  = Hou De Jarasa Ushir 
| note7   = A Sufi song
| length7 = 07:14
| extra7  = Anand Shinde
}}

<!-- 
IMDBs not a reliable source

  -->
 
 
 
 
 
 
 

== References ==

 

== External links ==

*  

 
 
 

 
 