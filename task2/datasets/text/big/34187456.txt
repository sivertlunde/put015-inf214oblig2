Lightning Bolt (film)
{{Infobox film
| name           = Operazione Goldman/Lightning Bolt
| image          = Opgoldpos.jpg
| image_size     =
| caption        = Original German film poster
| director       = Antonio Margheriti
| producer       =
| writer         = 
| narrator       =
| starring       = Anthony Eisley
| music          = Riz Ortolani
| cinematography = Riccardo Pallottini
| editor         =
| distributor    = Woolner Brothers
| released       = 1965
| runtime        = 90 minutes
| country        = Italy Italian
| budget         =
}}
 Spanish and Italian international 1965 Eurospy spy-fi film directed by Antonio Margheriti originally titled to coincide with the release of Goldfinger (film)|Goldfinger was not released in the US until two years later.  It was retitled to reflect the release of the next James Bond film  Thunderball (film)|Thunderball with the tagline "It strikes like a ball of thunder!"

==Plot== US space program rocket launches.  Sennet and his superior Captain Flanagan discover a beer baron named Rehte is destroying the rockets through laser beams fired at the rockets from Rehtes beer trucks parked outside the installation. Rehtes lair is a Dr. No (film)|Dr. No type underwater city off the coast of Cape Canaveral that Sennet infiltrates and destroys. 

==Production==
Sent to Italy by the Woolner Brothers, former Hawaiian Eye actor Anthony Eisley was told by director Antonio Margheriti that he looked "too Italian". Margheriti insisted Eisleys dark hair be dyed blonde; it came out red.  Eisley recalled working on the film in Rome and Lazio where the production designer recreated Florida was one of the most fun experiences that he ever had.   Other locations included an actual brewery and an underwater silo was a former Olympic swimming pool in Rome. 

To look like an American made film, director Margheriti was billed as "Anthony Dawson" whilst Wandisa Guida was billed as "Wandisa Leigh". Eisley recalled the original negative of the film was lost and for its American release they had to reassable the film from various prints that gave it a bad quality.   Eisley provided narration to give the print a more coherent story.

==Cast==
 
Anthony Eisley  ...  Lt. Harry Sennet   
Wandisa Guida  ...  Kary  
Folco Lulli  ...  Rehte   
Diana Lorys  ...  Capt. Patricia Flanagan   
Luisa Rivelli  ...  Ursula Parker

==Quotes==
I never liked your beer anyway – Goldman to Rethe

==Notes==
 

==External links==
*  

 
 
 
 
 
 

 

 