The Garden (1995 film)
 
{{Infobox film
| name           = The Garden (Záhrada) 
| director       = Martin Šulík
| writer         = Marek Leščák Ondrej Šulaj Martin Šulík
| starring       = Marián Labuda Roman Luknár Zuzana Šulajová Jana Švandová Katarína Vrzalová
| music          = Vladimír Godár
| cinematography = Martin Štrba
| distributor    = Action Gitanes
| released       =  
| runtime        = 99 minutes
| country        = Slovakia France
| language       = Slovak
}}
The Garden (original title Záhrada) is a 1995 Slovak feature film in cooperation with French Artcam International directed by Martin Šulík. The film is a poetic tale of Jakub, a man in his early 30s. It reflects the relationships and desires of Jakub in the idyllic surroundings of his grandfathers garden. 

==Plot==
Jakub is a daydreamer in his early 30s and is still living with his father. The father is tired of Jakubs idleness and throws him out of the flat. He tells Jakub to sell the old garden of his grandfather and to buy his own flat. Jakub retires into the neglected garden and retreats into the worn garden house from the world and from his problems. The garden turns out to be a magical place full of surprises and mysteries. Jakub stumbles across his grandfather’s journal which is written in reverse script. He finds a map leading him to an old bottle of slivovice hidden years ago. Now curious Jakub must solve the mysteries of his grandfather’s garden. He meets Helena, a young girl Jakub’s grandfather taught to also write backwards. Strange things happen in the garden, and some of them do indeed seem like miracles. Amid the eccentric events, young Helena teaches Jakub to appreciate the delicate mysteries of life. In the end Jakub loses all his possessions but finds his peace.

==Cast==
{| class="wikitable"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Marián Labuda || Jakubs father
|- 
| Roman Luknár || Jakub
|-
| Zuzana Šulajová || Helena
|-
| Jana Švandová || Tereza
|-
| Katarína Vrzalová || Helenas mother
|}

==Reception==
The Garden is one of the most recognised films of post communist Slovakia. It is especially popular in Slovakia and the Czech Republic. It won the special prize of the jury of the Karlovy Vary Film Festival in 1995 and several Czech Lions in 1996.

==External links==
* 

 

 
 
 
 
 
 
 

 