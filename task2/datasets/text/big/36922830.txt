Yugo & Lala
{{Infobox film
| name           = Yugo & Lala
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Wang Yunfei
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = Sebastien Pan
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =   
| runtime        = 87 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = Chinese yuan|¥24 million (China)   
}}

Yugo & Lala is a 2012 Chinese computer animated film directed by Wang Yunfei.  

==Synopsis==

Precocious ten-year-old Yugo, a village girl who is also a student of martial arts, meets a white "liger", a half lion, half tiger creature born from a magic stone. It introduces itself as Lala. Shocked that she can understand the animals language, Yugo chases it up a mountain where a waiting Cloud Whale swallows them both whole. Inside its giant stomach, there are many different creatures all excitedly being transported to Animal Paradise, a fantastical kingdom that welcomes only 99 chosen ones every ten years. However, human beings are forbidden in the kingdom, any that remain for more than three days will be transformed into an animal forever. To return home, Yugo seeks the help of the seemingly ferocious Uncle Bear, a black bear with a sharp tongue but a gentle heart. As time is running out for Yugo, she and Lala uncover Tiger Generals devious plot to open a magical portal into Lalas world and turn every human into an animal. As Yugos neighbours are transformed into animals one-by-one, she desperately seeks a way to destroy the gateway between the two worlds... only to discover that her new friend Lala is the magical portal.

==References==
 


==External links==
* 

 
 
 
 
 
 

 
 