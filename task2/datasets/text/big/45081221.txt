Chakra Theertha
{{Infobox film 
| name           = Chakra Theertha
| image          =  
| caption        = 
| director       = Peketi Sivaram
| producer       = 
| writer         = Tha Ra Su (Based on Novel)
| screenplay     = Tha Ra Su Rajkumar Udaykumar Jayanthi Balakrishna Balakrishna
| music          = T. G. Lingappa
| cinematography = R Chittibabu
| editing        = P Bhakthavathsalam
| studio         = Sri Bhagavathi Productions
| distributor    = Sri Bhagavathi Productions
| released       =  
| runtime        = 151 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, Jayanthi and Balakrishna in lead roles. The film had musical score by T. G. Lingappa. 

==Cast==
  Rajkumar
*Udaykumar Jayanthi
*Balakrishna Balakrishna
*B. M. Venkatesh
*Ganapathi Bhat
*Dr Sheshagiri Rao
*Rajanand
*Ramaraje Urs
*Master Prabhakar
*B. Jayashree
*Shanthaladevi
*Baby Raji
*Madhavi
 

==Soundtrack==
The music was composed by TG. Lingappa. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Janaki || R. N. Jayagopal || 03.29
|-
| 2 || Hagalu Hariyithu || P. Nageswara Rao ||  || 03.16
|-
| 3 || O Beli Leso || P. Nageswara Rao || Ta. Ra. Su || 03.17
|-
| 4 || Kuniyonu Bara Kuniyonu || L. R. Eswari || DR. Bendre || 03.30
|- Susheela || R. N. Jayagopal || 04.38
|- Janaki || R. N. Jayagopal || 04.08
|-
| 7 || Odi Baa || Sumithra, Lata Mangeshkar || R. N. Jayagopal || 03.27
|-
| 8 || Vidhiyu Thaleda Kopa || PB. Srinivas || R. N. Jayagopal || 03.31
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 