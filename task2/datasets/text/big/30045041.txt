Silent Hill: Revelation
 
{{Infobox film
| name = Silent Hill: Revelation
| image = Silenthill3Dposter.jpg
| image_size = 215px
| alt = 
| caption = Theatrical release poster
| director = Michael J. Bassett
| producer = Samuel Hadida Don Carmody
| writer = Michael J. Bassett
| based on =  
| starring = Adelaide Clemens Kit Harington Deborah Kara Unger Martin Donovan Malcolm McDowell Carrie-Anne Moss Sean Bean
| music = Jeff Danna Akira Yamaoka 
| cinematography = Maxime Alexandre   
| editing = Michele Conroy 
| studio = Davis Films Lionsgate  Universal Pictures  
| released =  
| runtime = 94 minutes   
| country = Canada France  United States
| language = English
| budget = $20 million
| gross = $52.3 million 	
}} 3D horror Silent Hill. Heather Mason alternate dimension Silent Hill.
 audio mixing took place in France. Revelation grossed $52,370,559,       but has received overwhelmingly negative reviews by film critics and fans alike. 

==Plot==
 

Sharon Da Silva (Adelaide Clemens) and her adoptive father Christopher (Sean Bean), currently hiding behind aliases of Heather and Harry Mason, have spent the past few years moving from town to town and assuming different identities. Heather believes that they are on the run from the police because Harry killed a man in self-defense and that her adoptive mother Rose (Radha Mitchell) died in a car crash. In fact, Harry has been protecting her from the Order, a cult of Silent Hill, a town haunted by shifting dimensions. Rose was able to free Heather (then Sharon) from one of the alternate dimensions using one half of a talisman called the Seal of Metatron, but Rose remained trapped in Silent Hill.
 Vincent Cooper Douglas Cartland (Martin Donovan) regarding her identity. Heather warns Harry, but he is abducted by the Order and taken to Silent Hill. Unaware of this, Heather goes to a mall to wait for Harry, but enters the malls Otherworld. Douglas explains he was hired by the Order to find Heather, but decides to help her when he discovered who his clients were. A monster, the Missionary (Liise Keeling), kills Douglas, but returning to the real world, Heather flees, leaving her a suspect to Douglass murder.
 Claudia Wolf (Carrie-Anne Moss), and was sent by her to convince Heather to willingly come to Silent Hill. He reveals that Heather is a part of Alessa Gillespie (Erin Pitt), a girl who was burned 38 years ago by the Order, leading her to create the towns shifting dimensions. A shift to the Otherworld occurs, and Vincent is dragged away by the Missionary after telling Heather to find his grandfather Leonard (Malcolm McDowell), who possesses the other half of the Seal of Metatron.

Heather ventures into one of the alternate dimensions to find Leonard. She encounters Alessa’s mother Dahlia (Deborah Kara Unger), who reveals that Claudia intends to complete a purpose intended for Alessa at her burning. Heather finds Leonard who after informing her that the Seal of Metatron will reveal "the true nature of things," fuses Heathers half of the amulet with the one he possesses and becomes a monster. After knocking Heather out and taking her away, she awakens and retrieves the amulet from his body, which kills him. As she runs away, she encounters Pyramid Head (Roberto Campanella) and hides from him while noticing Vincent being dragged off by the Order after being deemed insane by Claudia for betraying them.

Heather saves Vincent and they go to Lakeside Amusement Park, where the Orders sanctuary is hidden. Dark Alessa (Erin Pitt and Adelaide Clemens), the manifestation of Alessa’s wrath, confronts Heather who embraces her counterpart, absorbing her, and making Alessa complete once again. Heather confronts Claudia, who is holding Harry and Vincent hostage. Claudia explains that Alessas destiny was to be the incubator for a deity worshiped by the Order, who will punish all sinners upon its birth and Heathers destiny will be complete. Realizing Leonards words, Heather gives Claudia the Seal of Metatron, revealing her to be the Missionary. Heather summons Pyramid Head, who kills the Missionary, allowing Heather to rescue Vincent and Harry.

As the fog fades from the town, Harry decides to stay in Silent Hill to find and free Rose, leaving Heather in Vincents care. They manage to hitch a ride on a truck driven by Travis Grady (Peter Outerbridge) away from the place. Travis mentions to Heather (now referring to herself as Sharon) and Vincent that they were lucky he was there since he hasnt been passing that road for a while. A couple of police cars followed by a prisoner transport enter the area of Silent Hill which is consumed by the fog. In a post-credits scene, Pyramid Head is seen walking through an unknown area in Silent Hill.

==Cast==
  Heather Mason / Sharon Da Silva, the troubled adoptive daughter of Rose and Chris Da Silva who has been running away from Silent Hill with Chris for six years, and as Dark Alessa, in teenaged form, the tormented daughter of Dahlia Gillespie who was severely burned by the cult and exacted revenge on them. Vincent Cooper, a classmate of Heather and secretly a member of the Order and the son of Claudia Wolf, sent to take Heather to Silent Hill.
* Sean Bean as Harry Mason / Christopher Da Silva,  the adoptive father of Sharon who has been keeping Heathers true identity and memories a secret out of protection for six years. Claudia Wolf, the priestess of the Order of Valtiel, sister of Christabella and Dahlia, daughter of Leonard, and the main antagonist of the film who holds Christopher captive.
* Malcolm McDowell as Leonard Wolf, the grandfather of Vincent and father of Claudia who was chained down and abandoned by the cult. Douglas Cartland,  a detective hired by Claudia to spy on Sharon and Vincent to capture them.
* Heather Marks as Suki, a girl who took a wrong turn and got lost in the fog.
* Roberto Campanella as Pyramid Head,  a humanoid monster wearing a triangular shaped helmet who protects Sharon and Alessa.
* Radha Mitchell as Rose Da Silva, the adoptive mother of Sharon and wife to Chris who is trapped in the fog world after the events of the first film.
* Deborah Kara Unger as Dahlia Gillespie, the ragged and depressed biological mother of Alessa Gillespie who wanders through Silent Hills alternate dimensions.
* Erin Pitt as Young Alessa Gillespie, the tormented daughter of Dahlia Gillespie who was severely burned by the cult and exacted revenge on them, and as the younger Sharon Da Silva, along with Dark Alessa who is seen frequently haunting Heather.
*   who makes a cameo near the end of the film.

==Production==
 
===Development=== Silent Hill, Christophe Gans, its director, reported that a sequel "is officially ordered and is already well underway". However, he then declined to direct a sequel, stating he "had other projects in mind." Michael J. Bassett then took over writing and directing duties.  Silent Hill s writer Roger Avary had planned to return to the sequel, before his arrest in 2008 for vehicular manslaughter.    Michael J. Bassett later replaced Gans and Avary as writer and director. 

In 2009, video game artist Masahiro Ito, who participated in the development of multiple installments of the Silent Hill series of video games, was asked to design the creatures and the look of the "Otherworld" dimension featured in the film, but declined the offer because of other obligations.  Jeff Danna and Akira Yamaoka composed the films soundtrack.   

===Casting=== IMDb was also cited as a requirement.  Australian actress Adelaide Clemens was eventually cast in the role. Radha Mitchell, Sean Bean, Deborah Kara Unger, and Roberto Campanella were all contacted to reprise their previous roles which they accepted.

===Filming and audio mixing=== wrapped in Galt from March 21 to March 26  and scenes set at Silent Hills Lakeside Amusement Park were filmed at the Cherry Beach park on April 7, 2011. {{cite web|url=http://torontoist.com/2011/04/scene_foggy_film_shoot_in_the_portlands/|title=Scene: Foggy Film Shoot in the Port Lands
|date=April 8, 2011|work=  took place in Paris, France, and was handled by a team of six people. 

===Release=== San Diego Comic-Con International 2012 and a trailer was published on July 27, 2012.  The film was not released theatrically in Australia;  it was released direct-to-video on March 6, 2013. 

==Reception==

===Box office===
Silent Hill: Revelation opened at #5 at the box office, taking in an opening weekend gross of $8 million, and has internationally grossed a total of $52,302,796, as of March 27, 2013,   making it a moderate box office success from its $20 million budget.

===Critical reception===
 
Silent Hill: Revelation was panned by film critics, and currently holds a 5% approval rating on review aggregate   said that Silent Hill: Revelation 3D is a "cheaper, cheesier sequel thats worse than its predecessor on every level (save being a half-hour shorter) and takes no special advantage of the stereoscopic process." 

==Sequel==
Michael J. Bassett stated that if he is to make a sequel, instead of adapting from an existing game, he would prefer to use the stories in the graphic-novel adaptations. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 