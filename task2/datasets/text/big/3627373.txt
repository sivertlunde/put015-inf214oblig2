Waiting Alone
{{Infobox film
| name           = Waiting Alone
| image          =Waiting Alone.jpg
| image_size     = 
| caption        = 
| director       = Dayyan Eng 
| producer       = Dayyan Eng 
| writer         = Dayyan Eng 
| narrator       =  Xia Yu Li Bingbing  Gong Beibi  Yuan Quan
| music          = 
| cinematography = Toby Oliver
| editing        = Dayyan Eng
| distributor    = 
| released       =  
| runtime        = 107 minutes
| country        = China
| language       = Mandarin
| budget         = 
}} Xia Yu, Gong Beibi and Li Bingbing. It also features cameos of some of Hong Kongs best known actors, including Chow Yun-fat.

Excellent reviews and strong word-of-mouth made this independent film a hit in China where it was embraced by young audiences. In 2005, Waiting Alone was nominated for three Chinese academy awards (Golden Rooster Awards) including Best Picture; the first time a nomination was awarded to a foreign director in this category. Waiting Alone was acquired for international distribution in 2006 by Arclight Films.

==Cast== Xia Yu
* Li Bingbing
* Gong Beibi
* Dayyan Eng
* Yuan Quan
* Chow Yun-fat

== Awards ==
*Best Picture Nomination - 2005 Golden Rooster Awards
*Best Actress Nomination - 2005 Golden Rooster Awards
*Best Art Direction Nomination - 2005 Golden Rooster Awards
*Best First Feature Award - 2005 Beijing Film Festival
*Best Actor Award - 2005 Beijing Film Festival
* Best New Director Nomination - 6th Chinese Film Media Awards
*Official Selection - 17th Tokyo International Film Festival
*Official Selection - 2006 Thessaloniki International Film Festival
*Official Selection - 2006 Hawaii International Film Festival

== External links ==
*  
*  
*   
* 
* 
*   
*   
*   

 
 
 
 
 


 
 