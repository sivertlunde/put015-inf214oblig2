Where Have All the Flowers Gone? (film)
{{Infobox film
| name           = Where Have All the Flowers Gone?
| image          = Where Have All the Flowers Gone? (film).jpg
| alt            = 
| caption        = 
| director       = Arturo Perez Jr.
| producer       = 
| writer         = Arturo Perez Jr.
| starring       = Joel Sadler, Billy Troy, Arturo Perez Jr.
| music          = 
| cinematography = Arturo Perez Jr.
| editing        = Arturo Perez Jr.
| studio         = 
| distributor    = 
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English, Spanish
| budget         = 
| gross          = 
}}
Where Have All the Flowers Gone? is a 2008 documentary film directed by Arturo Perez Jr. and produced by Arturo Perez Jr., Joel Sadler and Billy Troy. Perez, Sadler, and Troy travel to San Francisco to recapture the Summer of Love more than 40 years previously. The film premiered at the Wine Country Film Festival on August 4, 2008 and at San Francisco State University on September 26, 2008.

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 

 