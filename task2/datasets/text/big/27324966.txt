Carbon Nation
{{Infobox film
| name           = Carbon Nation
| image          = Carbon Nation.jpg
| alt            =
| caption        =
| director       = Peter Byck
| producer       = Peter Byck Artemis Joukowsky Craig Sieben Chrisna Van Zyl Karen Weigert
| narrator       = Bill Kurtis starring = Bob Fox Thomas Friedman Eban Goodstein Gary Hirshberg Sadhu Aufochs Johnston Amory B. Lovins Joel Makower Edward Mazria Arthur H. Rosenfeld John Rowe (CEO) Exelon
| music          = Robert Hawes Matt Heck
| cinematography =
| editing        = Peter Byck Patrick Coleman Duncan
| studio         = Earth School Education Foundation
| distributor    =
| released       =  
| runtime        = 86 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Carbon Nation is a 2010 documentary film by Peter Byck about technological- and community-based energy solutions to the growing worldwide carbon footprint.  The film is narrated by Bill Kurtis.  ASIN: B0055T46LA (Rental) and B0055T46G0 (Purchase).
 16 terawatts of energy the world consumes can be met while reducing or eliminating carbon-based sources.  It contains optimistic interviews with experts in various fields, business CEOs, and sustainable energy supporters to present a compelling case for change while having a neutral, matter-of-fact explanation.   

Among those interviewed are Richard Branson, former CIA Director R. James Woolsey, Earth Day founder Denis Hayes and environmental advocate Van Jones. 

Much of the content of the film consists of interviews, some are listed above.  The list of interviewees also includes 
* Lester R. Brown President, Earth Policy Institute	
* Sean Casten President & CEO, Recycled Energy Development
* Ralph Cavanagh Lead Attorney, NRDC  Bob Fox, Partner Cook+Fox Architects
* Thomas Friedman, Author & NY Times Columnist
* Eban Goodstein Economic Professor, Lewis and Clark College
* Gary Hirshberg Chairman, President, and CEO of Stonyfield Farm
* Sadhu Aufochs Johnston Chief Environmental Officer, City of Chicago
* Amory B. Lovins Chairman & Chief Scientist, Rocky Mountain Institute
* Joel Makower Executive Director, GreenBiz.com
* Edward Mazria Executive Director, Solar Richmond
* Arthur H. Rosenfeld Commissioner, California Energy Commission
* John Rowe (CEO) Exelon Chairman & CEO, Exelon Corporation

==See also==
*An Inconvenient Truth
*Renewable energy commercialization
*Community wind energy
*1BOG, a San Francisco-based solar financing activism company featured in the film

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 


 