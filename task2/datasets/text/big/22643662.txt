Pygmalion (1937 film)
{{Infobox film
| name           = Pygmalion
| image          = 
| image_size     = 
| caption        =  Ludwig Berger
| producer       =  Ludwig Berger, Johan De Meester 
| narrator       = 
| starring       = 
| music          =    Akos Farkas
| editing        = 
| distributor    = 
| released       = February 26, 1937
| runtime        = 95 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
}} 1937 Netherlands|Dutch Ludwig Berger.

==Plot==
The film takes place in Amsterdam. Elisa "Lizzie" Dolittle (Doeluttel) is a poor but cheeky flower seller from the lower class, with a flat Amsterdam accent. When Professor Higgins, a linguist and speech teacher, accidentally bumps into Lizzie, he is shocked by her voice and manner of articulation. He looks down on her and even says that she has no right to exist if she cannot talk like a decent young lady. The next day, Lizzie visits him at his house on the Prinsengracht, seeking diction lessons so that she can work as a saleswoman in a store. Higgins is convinced that the girl has no chance of success, but when Colonel Pickering wagers that he is unable to make her into a proper lady, Higgins cannot resist a challenge and bets Pickering that within three months he will succeed.

His housekeeper, Mrs. Mills (Snijders) is not happy with her arrival and protests that it is impossible to accommodate Lizzie for three months.  Nevertheless, she is given a room in the house and immediately begins the first lessons of training to be a lady.  She struggles against the training, however, making it clear to Higgins that his task will be a difficult one. He almost gives up hope, until Lizzies father pays a visit and Higgins realizes that he desperately longs for his daughter to change. After a bath, Lizzie gets a new wardrobe and her diction lessons begin. She soon finds out that this is more difficult than she thought, and can barely endure Higgins strict teaching methods.

Nevertheless, Lizzie begins to make progress. She learns how to articulate precisely and successfully loses her Amsterdam dialect. Higgins decides that the time has come for her to drink tea with his mother to put his teaching to the test. Lizzie knows how to speak with a flawless accent, but shocks the guests with her talking points. Not much later, she is invited to a ball. Just before her departure, Mrs. Mills advises her that the less she says, the better things will be for her.  At the ball there are rumors that she is of noble descent.

Lizzie successfully comes across as a proper lady at the ball, and Higgins wins the bet. As her business with Higgins comes to an end, she feels he has only used her to win a bet and cannot wait to throw her on the street again. She is furious and tells him that she wished he had never taken her to his house. Higgins feels hurt and angry. They quarrel, after which Lizzie leaves the house unnoticed. Once back in her simple home, she feels that she no longer belongs. She leaves and seeks refuge with Higgins mother, who receives her with open arms. Higgins has now realized Lizzie is missing, and begins a search for her.

When he finds Lizzie at his mothers house, his concern quickly turns to relief. Lizzie is then surprised by the arrival of her father, who has left behind his life as a drunkard in favor of a new career as a public speaker. Later she tries to explain her quarrel with Higgins, but it once again results in a shouting match. Their anger quickly turns into love.

==Cast==
*Lily Bouwmeester as Elisa Doeluttel
*Johan De Meester as Higgins
*Emma Morel as Mrs. Higgins
*Eduard Verkade as Pickering
*Matthieu van Eysden as Doeluttel
*Elly Van Stekelenburg as Mrs. Doeluttel
*Nel Oosthout as Juffrouw Snijders
*Sara Heyblom as Mevrouw van Heteren-Hill
*Wim Kan as Mevrouw van Heteren-Hills zoon
*Tous Sigma as Mevrouw van Heteren-Hills dochter

==Production== German version from 1935 in film|1935, to which Shaw gave very little praise. The star of the Dutch version was Lily Bouwmeester. The name of the main character was actually Eliza Doolittle, but this was changed to the Dutch version Elisa "Liesje" Doeluttel. Bouwmeester was formerly known as a stage actress and had not been active in the film industry since 1921. When her name first came up in connection with the role, the idea was not seriously considered, but she did end up being awarded an audition. The director was impressed and Bouwmeester was cast on site.   BOUWMEESTER, Lily Gertrude Henrietta Maria (1901-1993) 
 British film Ludwig Berger departed for America to find his luck. He wanted the first director to make an American version of the film, but Shaw refused an offer to sell the film rights to Hollywood for $200,000. 

Today the film is seen as one of the three most successful films from the period before the Second World.  On January 25, 2007 the film was released on DVD by the Dutch Filmmuseum. 

==References==
 

==External links==
*  

 
 

 
 
 
 
 