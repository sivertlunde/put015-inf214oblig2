California (1927 film)
{{Infobox film
| name           = California
| image          = 
| alt            = 
| caption        = 
| director       = W. S. Van Dyke
| producer       = Erich Pommer
| screenplay     = Marian Ainslee Ruth Cummings Frank Davis 
| story          = Peter B. Kyne
| starring       = Tim McCoy Dorothy Sebastian Marc McDermott Frank Currier Fred Warren
| music          = 
| cinematography = Clyde De Vinna
| editing        = Basil Wrangell
| studio         = Metro-Goldwyn-Mayer
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Western silent film directed by W. S. Van Dyke and written by Marian Ainslee, Ruth Cummings and Frank Davis. The film stars Tim McCoy, Dorothy Sebastian, Marc McDermott, Frank Currier and Fred Warren. The film was released on May 7, 1927, by Metro-Goldwyn-Mayer.  

==Plot==
 

== Cast ==
*Tim McCoy as Capt. Archibald Gillespie
*Dorothy Sebastian as Carlotta del Rey
*Marc McDermott as Drachano
*Frank Currier as Don Carlos del Rey
*Fred Warren as Kit Carson
*Lillian Leighton as Duenna
*Edwin Terry as Brig. Gen. Stephen W. Kearny

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 


 
 