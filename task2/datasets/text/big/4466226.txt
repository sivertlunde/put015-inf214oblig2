The Saddest Music in the World
{{Infobox film
| name           = The Saddest Music in the World
| image          = Saddestmusicposter.jpg
| image_size     =
| director       = Guy Maddin
| producer       = Niv Fichman Daniel Iron Jody Shapiro Executive Producer Atom Egoyan
| writer         = Screenplay: Guy Maddin George Toles David Fox Ross McMillan Louis Negin
| music          = Christopher Dedrick
| cinematography = Luc Montpellier
| editing        = David Wharnsby
| distributor    = IFC Films (US theatrical) MGM (North America DVD)
| released       = September 07, 2003 (Canada) October 25, 2003 (UK) February 14, 2004 (US)
| runtime        = 99 minutes
| country        = Canada
| language       = English CAD $3.8 million (estimated) 
| gross          =
}}

The Saddest Music in the World is a 2003 Canadian film directed by Guy Maddin, budgeted at $3.8-million (a large budget relative to the average Canadian film)  and shot over 24 days.  The film was Maddins first collaboration with Isabella Rossellini, who subsequently appeared in a number of Maddins films, and co-created a film with him about her father Roberto Rossellini.
 expressionist art design. A few scenes are filmed in colour, in a manner that imitates early Technicolor|two-strip Technicolor.

==Plot== David Fox), representing Canada, and his brother Roderick (Ross McMillan), representing Serbia as "Gavrilo the Great" (even though he is also Canadian).

It is revealed that Fyodor is in love with Helen, who he had once hoped to marry. However, Helen and Chester had an affair, and an accident involving the three occurred when Fyodor stepped out in front of Chesters car as Helen was performing oral sex on Chester. Helens legs were both amputated as a result, and Fyodor became an alcoholic, while Chester left for Broadway. With Chester returned, and his relationship with Helen renewed, Fyodor swears off drink and fashions prosthetic legs filled with beer in an attempt to earn Helens love.

Roderick meanwhile discovers that Chesters girlfriend Narcissa is his missing wife, who has forgotten both their marriage and their son after the boys death (Roderick carries the boys heart in a jar, preserved in his own tears). Helen rigs the contest to favour Chester/America, and Fyodor/Canada quickly loses after singing "Red Maples Leaves," although Roderick/Serbia advances. Although Roderick and Narcissa have sex, she still doesnt remember their marriage, and he accidentally breaks the jar containing his sons heart (which is pierced by a glass shard), and although Helen loves her new glass beer legs, she still hates Fyodor. Fyodor then drinks a legs worth of beer and falls through the concert hall rooftop to his death.

Helen appears in Chesters final performance, but her legs leak and explode when Roderick plays. Roderick then changes his tune to play "The Song Is You," which he had sworn not to perform until reunited with his wife. The song recovers Narcissas memory and Chester, meanwhile, is stabbed to death by Helen (using a long shard from her glass legs). Chester refuses to let this sadden him, and staggers away, accidentally setting the building on fire with his victory cigar. Chester dies playing "The Song Is You" on the piano as the building burns.

==Cast==
Mark McKinney as Chester Kent 
Isabella Rossellini as Lady Helen Port-Huntley 
Maria de Medeiros as Narcissa  David Fox as Fyodor Kent 
Ross McMillan as Roderick Kent / Gravillo the Great 
Louis Negin as Blind Seer 
Darcy Fehr as Teddy 
Claude Dorge as Duncan Elksworth 
Talia Pura as Mary 
Jeff Sutton as Young Chester 
Graeme Valentin as Young Roderick 
Maggie Nagle as Chesters Mother

==Release==
The Saddest Music in the World was released theatrically in Canada on September 7, 2003, to the UK on October 25, 2003, and to the US on February 14, 2004. TVAFilms released it to home video on DVD in 2004, with a directors commentary by Maddin and Mark McKinney, and three short films: "A Trip to the Orphanage," "Sombra Dolorosa," and "Sissy Boy Slap Party." 

==Awards and honors==
Directors Guild of Canada:
*Win: Outstanding Achievement in Production Design, Feature Film - Matthew Davies
*Nominated: Outstanding Achievement in Direction, Feature Film - Guy Maddin
*Nominated: Outstanding Achievement in Picture Editing, Feature Film - David Wharnsby

Genie Awards
*Win: Best Achievement in Costume Design - Meg McMillan
*Win: Best Achievement in Editing - David Wharnsby
*Win: Best Achievement in Music, Original Score - Christopher Dedrick
*Nominated: Best Achievement in Direction - Guy Maddin

U.S. Comedy Arts Festival
*Win: Film Discovery Jury Award, Best Director - Guy Maddin

==Critical reception==
The Saddest Music in the World was well received by critics, with a 78/100 rating by review aggregator Metacritic, based on 33 critics.  Review aggregator Rotten Tomatoes similarly reported a 78% approval rating based on 101 reviews. 

The Christian Science Monitor drew attention to "Maddins unique style ...   carries old-movie nostalgia past the breaking point, making the picture look and sound like a long-ago production thats been stored under somebodys bed for the past few decades, and now reaches the screen replete with often-spliced frames and a fuzz-filled sound track. This is no mere gimmick but a core ingredient of Maddins aesthetic, which bestows affection and regard on everything we overlook and undervalue in our daily lives." 

Roger Ebert gave the film 3 1/2 out of 4 stars, calling it "entirely original" and noting that "You have never seen a film like this before, unless you have seen other films by Guy Maddin." 

== References ==
 

==External links==
*  
*  
*  
*  
*  
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 