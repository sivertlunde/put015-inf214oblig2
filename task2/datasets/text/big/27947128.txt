Dobří holubi se vracejí
 
{{Infobox film
| name           = Dobří holubi se vracejí
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Dusan Klein
| producer       = 
| writer         = 
| narrator       = 
| starring       = Milan Knazko
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 1988
| runtime        = 109 minutes
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}} Czech drama film/dark comedy from the Alcohol Treatment Centre. It was directed by Dusan Klein, released in 1987.

==Cast==
* Milan Knazko - Milos Lexa
* Rudolf Hrusínský - Masák
* Alicja Jachiewicz - Sona Landova
* Vladimír Mensík - Honzícek
* Pavel Zednícek - Mutzlinger
* Jirí Hálek - Ctvrtecka
* Marián Labuda - Dorenda
* Zdenek Rehor - Zlámal
* Radan Rusev - Janosik
* Rudolf Hrušínský ml. - Obuli
* Jan Preucil - Doctor Vonka
* Josef Somr - senior doctor Dorotka
* Evelyna Steimarová - Irena
* Petr Cepek - Uncle Karel
* Oldrich Navrátil - Doctor Krízek
* Josef Somr - Dobrotka
* Eva Vejmelková

==External links==
*  

 
 
 
 
 


 
 