Senior Year (film)
{{Infobox film
| name         = Senior Year
| image        = Senioryearlowres.jpg
| alt          =
| caption      = Theatrical poster for the commercial screening of Senior Year
| director     = Jerrold Tarog
| producer     = Franco Alido
| writer       = Jerrold Tarog 
| starring     = 
| music        = Jerrold Tarog, Johnoy Danao
|cinematography= Mackie Galvez
| editing      = Jerrold Tarog 
| distributor  = Digitank Studios, Inc and Metric Films
| released     =  
| runtime      = 
| country      = Philippines
| language     = Filipino
| budget       = 
| gross        =
}}
Senior Year is a 2010 Philippine film directed by Jerrold Tarog, which tells the story of ten students of a private Catholic high school in Manila, as they go through the many transitions that come with their final year in high school.      The film features real high school students in its major roles, with more seasoned adult actors playing secondary roles, often taking their cue from the younger performers. 

==Plot==
The promotional material for Senior Year avoids giving the audience specific plot points, and describes the film simply as: 

:"...a glimpse into the lives of ten students at St. Fredericks Academy as they struggle through the final months before graduation. Hearts are broken and healed, friendships are formed and lost. Childish ways are thrown out in exchange for seeds of maturity in what may be the beginning of a bumpy ride towards the chaos of adulthood."

==Cast==
(in alphabetical order within each category)

===Students===
* Aaron John Tan Balana - Henry
* Sheila Marie Bulanhagui - Stephanie
* Franzes Bunda - Bunda
* Nikita Conwi - Solenn
* Rossanne de Boda - Sofia
* Mary Amyrose Lojo - Bridget
* Daniel Lumain - Briggs
* Eric Marquez - Chito
* Daniel Clavecilla Medrana - Carlo
* Celina Peñaflorida - Mitch

===Teachers===
* Ramon Bautista
* LJ Moreno
* Che Ramos

===Bookend characters===
* Ina Feleo
* RJ Ledesma
* Arnold Reyes
* Dimples Romana

==Production==

===Casting and Screenplay===
To cast the film, Tarog sent surveys to about 300 students about their life experiences. The ten students with the most interesting stories were selected to be part of the cast. Tarog then put these students through a two-month workshop, and then he started writing the screenplay for Senior Year.   

"Kung anuman yung kwento ng buhay nila, dun ako na-inspire na magsulat ng script" (Whatever their own life stories were, thats where I drew the inspiration for writing the script.), Tarog would later explain. 

== Recognition == Department of Education.       Aside from marking the film in the public eye as "excellent", an "A" rating from the Cinema Evaluation Board entitles a film to a 100% tax rebate on its box office sales. 

The risky move of featuring real high school students in the major roles of the film paid off, with critics praising the young actors for their exceptional performances.  The film was nominated for national awards even before its scheduled regular screenings.

The Entertainment Press Society (Enpress)s 8th Golden Screen Awards (GSA), which opens the awards season for films in the Philippines, saw Senior Year nominated for six awards, including Best Motion Picture Drama, Best Director, Best Story, and Best Original Screenplay. 

The Philippine Movie Press Club (PMPC)s Star Awards for Movies saw Senior Year nominated for six awards, including Digital Movie Production Designer of the Year for Benjamin Padero, and five nominations for Tarog: Digital Movie Director of the Year, Digital Movie Screenplay of the Year, Digital Movie Editor of the Year, Digital Movie Musical Scorer of the Year,and Digital Movie Sound Engineer of the Year.

== Promotion ==

=== Faculty: A Prequel to Senior Year ===
In 2010, as Senior Year was being made, the ABS CBN News Channel tapped Tarog to be part of AmBisyon 2010, a series in which directors would use short films to narratively present the various problems that would face whoever won the Presidency in the Philippine Presidential Election of 2010. Tarogs entry, covering the area of "education", was the short film "Faculty", subtitled "A prequel to Senior Year." The Ms. Joan character from Senior Year makes her first appearance in this short, which explains why the character, who used to be a College teacher, had become a high school teacher by the time she was shown in Senior Year. Faculty became a viral hit on various social networks, and the popularity of the short film became part of the marketing for its longer, more fleshed-out sequel. 

=== Digitank Studios, 2010 Metro Manila Filmfest, and SM Cinemas ===
During the development of the film, Tarog, who up to this point had been known primarily as an independent film director, told producer Franco Alido that he would "do my best to come up with a film that he can show to everybody. But he has to do his best to show it to everybody." He further explained that "Any film that has commercial aspirations is nothing without the marketing that comes after it. People have to know the film is out there, that it wants to be seen. And thats exponentially more difficult for an independent film without big stars, huge billboards and full support from the major TV networks."    

Alido and his Digitank Studios staff proceeded to do everything they could to promote the film, lugging promotional material and viewing equipment to different SM branches and eventually getting the film played in twelve SM Cinema Branches beginning March 9, while a nonconventional promotional campaign used Facebook and YouTube to build hype for the film, often taking advantage of the fact that the prequel short film, "Faculty", had become a viral favorite on social networking sites. As of the day before the regular screening, the campaign had attracted more than twelve thousand to the movies official Facebook page. 
 
The film was first screened as an independent film during the 2010 Metro Manila Film Festival, which began a special category for indie films that year.   

It was later picked up for distribution in SM Cinemas  and had its wider-audience premiere on March 2, 2011, and began its regular screening run on March 9. 

==== SM Cinema extension campaign goes viral ====
On the evening of the films release, Tarog thanked those who had already seen the film and also encouraged those who hadnt seen it yet to watch the film as soon as possible.   

:"From a purely business standpoint, the hard truth is that some SM branches will pull out the film or slide it to a limited time slot due to slow ticket sales on a Wednesday afternoon (precisely the time when nobody goes to watch a film... So listen...tomorrow might be your last chance to see SENIOR YEAR. So if you have time tomorrow, do catch it! Most branches within Metro Manila are still relatively "safe" so we expect weekend screenings. But the earlier you catch the film, the better."

On March 10, 2011, with viewers asking on the various social networks for the film to be extended, the films marketing team replied "We notice some of your pleas are addressed to SM Cinema. Well, you can tell it to them directly. Just Like their FB page and post on their Wall. It could work." 

Fans immediately started posting requests on the official SM Cinema wall so that the film would be extended, with the page soon becoming flooded with Senior Year requests. Theater patrons from areas where the SM Cinema branch wasnt showing Senior Year, such as Baguio, Laguna, and Batangas, also requested that the film be shown in their local SM Cinemas. 

The campaign worked, and the film was screened in SM Cinemas for an entire week, an unusual achievement for a Filipino independent film. In a message to viewers after the screenings, Tarog noted:   

:Its   a big achievement to have an independent film of this type (no major stars, no TV advertisements) to be allowed a weeklong run in a major mall chain. The theatrical run was an experiment for us. Without TV exposure, we didnt really raise our expectations at the box office but, with the advance screenings and word-of-mouth, we DID hope that enough people would see the film to make an impact on the Filipino audience.

:Apparently it has. The viral campaign to extend SENIOR YEAR by flooding SM Cinemas Facebook Wall was probably the main reason why SM allowed the film to stay in the theaters in the first place.

:It was all about trying something new and seeing if audiences would respond. On that count, we succeeded, all thanks to you, our audience, who helped us fight for the right to be seen and heard. All we need now is more faith from distributors and sponsors to give SENIOR YEAR and other independently-produced, audience-friendly films the proper media mileage.

In addition, Tarog noted: 

:I am confident thats all it takes. The audience and the market exists. The Filipino moviegoer is smart and intelligent. After all, arent we sons and daughters of people who sparked intelligent revolutions? I dont think its a big stretch to imagine that we deserve entertainment that levels with us.

==See also==
* Jerrold Tarog
* Confessional (film)
* Mangatyanan
* Cinema of the Philippines

== References ==
 

==External links==
*  
*  

 
 
 
 