Micki & Maude
{{Infobox film
| image = Micki and maude film poster.jpg
| image_size =
| alt = The groom in the middle. The bride with white flowers at left; the other bride with yellow flower at right.
| caption = Theatrical release poster
| director = Blake Edwards Tony Adams
| writer = Jonathan Reynolds
| starring       = {{plainlist|
* Dudley Moore
* Amy Irving
* Ann Reinking
* Richard Mulligan
* George Gaynes
* Wallace Shawn
}}
| music = Lee Holdridge
| cinematography = Harry Stradling Jr.
| editing = Ralph E. Winters
| distributor = Columbia Pictures
| released =  
| runtime = 118 minutes
| country = United States
| language = English
| budget =
| gross = $26,200,000
}}
Micki & Maude is a 1984 comedy film directed by Blake Edwards and starring  Dudley Moore. It co-stars Tony-award winning actress and dancer Ann Reinking as Micki and Amy Irving as Maude.

With the exception of appearances as herself, as in the documentary Mad Hot Ballroom in 2005, this has been Reinkings last film role as of 2012.
 The Princess Bride, the two do not appear together onscreen.

==Plot==
Rob Salinger (Dudley Moore) is an overworked television reporter. He is happily married to Micki (Ann Reinking), a lawyer who is a candidate to become a judge.  Rob wants a child badly, but Micki decides to postpone having children in order to better pursue her new career opportunity. On an assignment, Rob interviews a young musician named Maude Guillory (Amy Irving) and is smitten with her. They begin seeing one another and, when she becomes pregnant, Maude and her professional wrestler father begin to plan her wedding.

Prepared to face the music, confess to Micki and agree to a divorce, Rob is stunned when she reveals that she, too, is going to give birth. Rob becomes a Bigamy|bigamist. With his television boss Leo (Richard Mulligan) covering for him, he sees one wife during the daytime and the other at night, using work as an excuse. He gets away with it until the fates collide: Micki and Maude going into labor at the same hospital on the same floor at the same time.

The two women end up becoming friends, but they ban Rob from their lives as well as the lives of his new children after realizing he had been dishonest with them. Rob follows them around, spying on both families from a distance. Eventually Rob reconciles with both Micki and Maude, though it is not clear if the two women are aware he has reconciled with the other. The film ends with both women pursuing their careers, Micki is now a judge and Maude is a lead cellist in a symphony. The film closes with a shot of Rob in a park years later, with two babies and his six other children he has had over the years with Micki and Maude.

==Cast==
* Ann Reinking as Micki
* Amy Irving as Maude Guillory
* Dudley Moore as Rob Salinger
* Richard Mulligan as Leo
* George Gaynes as Dr. Glztszki
* Wallace Shawn as Dr. Fibel
* H.B. Haggerty as Barkhas Guillory
* John Pleshette as Hap Ludlow Andre Rousimmoff as Wrestler
* Lu Leonard as Nurse Verbeck
* Roger Rose as the Newscaster

==Awards and nominations==
In 1985, Moore won the Golden Globe award for Best Performance by an Actor in a Motion Picture - Comedy/Musical. The film was also Golden Globe-nominated for Best Motion Picture - Comedy/Musical.

==External links==
* 
* 
* 
* 

 

 
 
 
 
 
 
 
 