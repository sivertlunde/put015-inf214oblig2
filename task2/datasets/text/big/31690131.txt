Damarukam (film)
 
 
{{Infobox film
| name           = Damarukam
| image          = Damarukam Wallpaper.jpg
| caption        =
| writer         = Srinivasa Reddy   
| story          = Veligonda Srinivas 
| producer       = Venkat   K. Achi Reddy   
| director       = Srinivasa Reddy Nagarjuna Akkineni Anushka Shetty
| music          = Devi Sri Prasad
| cinematography = Chota K. Naidu
| editing        = Gautham Raju
| studio         = R. R. Movie Makers
| distributor    = Bellamkonda Suresh  
| released       =  
| runtime        = 157 min
| country        =   India
| language       = Telugu
| budget         =   
| gross          =  
}}  
 Telugu socio Nagarjuna Akkineni, Anushka Shetty play the lead roles and music composed by Devi Sri Prasad. The film recorded as Utter flop at box office.

The film was released on 23 November 2012. Chota K. Naidu won Filmfare Award for Best Cinematographer - South.
The film was later dubbed into Hindi as Shiva - The Superhero 2.

==Plot==
The protagonist of the movie is born with a divine grace of Lord Shiva (Prakash Raj) and the boys parents are advised to name him as Mallikarjuna as he is destined to accomplish a great work. During his childhood, Mallikarjuna faces a great tragedy as his parents and grandparents are killed by a huge feline creature when they are returning from Kashi. This incident leaves his younger sister, Sailu (Abhinaya (actress)|Abhinaya) paralyzed from the waist and hence renders her lame (Sailu had a talent for dancing before this incident). This disaster makes Malli to become an atheist.
 Ravi Shankar). We see him performing a severe penance to obtain the boon of Lord Shiva. Lord Shiva blesses the demon that he will not interfere in the latters attempt to sacrifice a virgin girl in order to obtain complete rulership over the 3 worlds. The demon is advised by his aged friend Maayi (Jeeva (Telugu actor)|Jeeva) that the sacrifice must be made in a month having 2 solar eclipses, in such a way that he has to marry the girl and sacrifice her before the solar eclipse ends. This will render the demon matchless and a master over all the 5 elements.

The story then shifts to Malli (Akkineni Nagarjuna|Nagarjuna), now an adult whose motive is to help people and take care of his bedridden sister. He falls in love with Dr. Maheshwari (Anushka Shetty) on a visit to the hospital where his sister is being treated. There are many scenes where the atheist Malli forces the people of his colony who include Rudraksha (Brahmanandam), Ringu Raja (Raghu Babu), President (Krishna Bhagavaan) and others, not to worship, on account of his childhood tragedy. Then, on one occasion, Malli questions Lord Shiva for his condition and drives away in his car, only to meet with an accident. Lord Shiva comes and saves him. Lord Shiva calls himself as Saambayya, befriends Malli and goes to his home.

The demon chooses Dr. Maheshwari as his target for the sacrifice and prevents the Kaapaalikas (who arrive to the temple) from having access to Mahi, on the first solar eclipse day. Malli rescues Mahi from the Kaapaalikas and Mahis parents entrust Malli to take care of Mahi. Then, Mahis parents decide to get Mahi married to a relative, Rahul (Ganesh Venkatraman), who is to arrive from US. This advice is given by saint known to Mahis family. On his arrival to the airport, Andhakaasura kills Rahul in the washroom and takes his form. The demon, now in Rahuls form, enters Mahis home, and kills 3 dogs who guard Mahi. The demon also paralyzes Mahis father, when he comes to know the truth about him. He also tires to kill Malli and Mahi, on their way to a temple, but Lord Shiva intervenes by sending his divine vehicle, Nandi who saves Malli. This infuriates the demon, who feels that Lord Shiva has not kept his promise. He challenges the Lord, to which he replies that it was his duty to protect his devotee. The demon then decides to oppose Lord Shiva, so he kills the saint known to Mahis family, impersonates him and convinces Malli, Mahi and others that Saambayya is not a good person by faking his death at the hands to Saambayya. This makes Malli angry on Saambayya and he returns the ring of friendship given to him by Saambayya.

Finally, Malli convinces Mahis family that Rahul is actually Andhakaasura. Then, the demon carries Mahi to the spot of sacrifice and prepares for the ritual. Meanwhile, Malli is advised by the Kaapalika chief (Avinash) to worship Lord Shiva, he tells Malli about his purpose of birth to destroy the demon and the means to do it, as he was the reason of the tragedy in Mallis Childhood and Lord Shiva saved him as Saambayya. Malli confronts the demon, is initially defeated, but he heads skyward to get a blessing from Lord Shiva and finally destroys Andhakaasura by piercing the Trident given by Lord Shiva.

==Cast==
{{columns-list|3| Nagarjuna Akkineni as Mallikarjuna (Malli)
* Anushka Shetty as Maheshwari (Mahi)
* Prakash Raj as Lord Shiva / Sambayya Ravi Shankar as Andhakasura Rakshasha
* Ganesh Venkatraman as Rahul
* Brahmanandam as Rudraksha
* M. S. Narayana as Vankara Satyam Devan as Vishwanadham, Maheshwaris father
* Avinash
* Brahmaji as Vishwanadhams brother
* Sravan
* Sameer as Mallikarjunas father
* Giri Babu as Mallikarjunas grand father
* Raghu Babu as Goke Ring Raju
* Krishna Bhagavan as Colony President Prithviraj as Ad film director
* Kamal 
* Prabhu 
* Ramaraju 
* Vijaya Rangaraju 
* Thagubothu Ramesh as "full bottle" Vasu 
* Duvvasi Mohan Jeeva as Maayi Charmy as a item number Pragati as Rajeshwari, Maheshwaris mother Abhinaya as Shailu, Mallikarjunas sister
* Rajitha Kavitha
* Apoorva
* Kalpana Priya as Mallikarjunas mother
* Satya Krishnan
* Lakshmi
* Geetanjali as Mallikarjunas grand mother
* Aruna 
* Amurtha Gowri 
* Sruthi 
* Archana 
* Anitha
* Nandini
* Nansy
* Master Gaurav as Young Malli
}}

==Soundtrack==
{{Infobox album  
| Name        = Damarukam
| Type        = Soundtrack
| Recorded    = 2011-2012 Recorded and Mixed at "Brindavan - The Garden of Music" Hyderabad
| Artist      = Devi Sri Prasad
| Cover       =
| Caption     = Album Cover Telugu 
| Label       = Aditya Music
| Music Director = Devi Sri Prasad
| Length      = 34:32
| Lyrics      = Jonnavittula, Ramajogayya Sastry, Bhaskarabhatla, Chandrabose (lyricist)|Chandrabose, Sahithi
| Last album  = Julai|Julayi (2012)
| This album  = Damarukam (2012)
| Next album  = Sarocharu (2012)
}}

Music composed by Devi Sri Prasad. All songs are blockbusters. Music released on ADITYA Music Company. 

The audio release was held on 10 September 2012 at Shilpakala Vedika in Hyderabad. Devi Sri Prasad scored the music and there are 10 songs for which lyricists Jonnavithhula Ramalingeswara Rao|Jonnavittula, Chandra Bose, Sahithi and Bhasakara Bhatla have penned the lyrics.  entertainment.in.msn.com gave a moderate review stating "Devi Sri Prasads 50th album is unique from his usual run-of-the-mill compositions, but not his best. As a fan of the composer, the listener will be satisfied, but as a music lover, one doubts if this album would live up to listeners expectations."  telugu.way2movies.com gave a positive review stating "Damarukam has the mix of devotional tracks on Lord Siva along with the romantic duets and a mass track. The devotion numbers are highly appealing and catch your attention while the duets are likeable with the repeated listen. Sakkubai will be instantly liked by masses. DSP scores well with his 50th album Damarukam."  Musicperk.com rated the album 8.5/10 quoting "A must-listen album by DSP. Not a song to be left out".  

{{Tracklist
| collapsed       =
| headline        = Tracklist
| extra_column    = Artist(s)
| total_length    = 34:32
| writing_credits =
| lyrics_credits  = yes
| music_credits   =
| title1          = Omkaram
| note1           =
| lyrics1         = Jonnavittula
| extra1          = Venkat Sai
| length1         = 01:04
| title2          = Aruna Dhavala
| note2           =
| lyrics2         = Jonnavittula Karthik
| length2         = 01:05
| title3          = Nestama Nestama
| note3           =
| lyrics3 = Bhaskarabhatla	
| extra3 = Sri Krishna & Harini 	
| length3 = 04:56	
| title4 = Reppalapai	
| note4 =
| lyrics4 = Ramajogayya Sastry	 Hariharan & Chitra	
| length4 = 03:42	
| title5 = Dheemtana
| note5 =
| lyrics5 = Karunakar & Jonnavittula (Mantras)
| extra5 = Shankar Mahadevan
| length5 = 02:08
| title6 = Sakkubaai Garam Chaai
| note6 =	
| lyrics6 = Ramajogayya Sastry	
| extra6 = Suchith Suresan & Mamta Sharma	
| length6 = 03:42	
| title7 = Laali Laali	
| note7 =	 Chandrabose
| extra7 = Gopika Poornima	
| length7 = 04:47	
| title8 = Bhoonabhontaalake	
| note8 =	
| lyrics8 = Jonnavittula	
| extra8 = M. L. R. Karthikeyan
| length8 = 02:02
| title9 = Kanyakumari	
| note9 =	
| lyrics9 = Sahiti	 Sunita 	
| length9 = 04:31	
| title10 = Shiva Shiva Shankara	
| note10 =	
| lyrics10 = Jonnavittula 	
| extra10 = Shankar Mahadevan
| length10 = 04:52	
}}

==Production==

===Casting=== Nagarjuna and Pradeep Rawat, Venu Madhav, Jeeva are Charmy in the place of Lakshmi Rai for the introduction song in this movie.  Nagarjuna had shown his six-pack abs for the first time in the movie. 

===Filming=== Nagarjuna would romance Anushka Shetty to the choreography of Raju Sundaram.   On 29 September 2012, The theatrical trailer of this socio-fantasy film has released with the movie Rebel, which graced the theatres that day. According to the producer Dr Venkat, the trailer has received a huge response from the film goers.  This movie is the first Telugu film ever with over 70 minutes of special effects. It is learnt that the movie is loaded with some of most breathtaking visual effects ever for an Indian movie and is going to set a new benchmark as far as VFx standards go in India. A team of visual effects experts at Firefly Creative Studio have been working on the movie for over a year now. Firefly, the company which worked on special effects for films like Anji (film)|Anji, Arundhati (2009 film)|Arundathi, Magadheera & Anaganaga O Dheerudu.  Vyshnavi Films purchased the overseas theatrical rights.

===Release===
After much delays the film was released on 23 November 2012  

==Critical reception==
idlebrain.com jeevi gave a rating of 3/5 stating "First half of the film is good, Plus points of the film are Nagarjuna and the grandeur".  DNA India gave the film 2.5 and Times of India gave 3.5 ratings. 

==Box office==
The film opened to very less occupancy as Nagarjuna Akkineni|Nagarjunas ruling in the box-office had slowly reduced by the years due to the entry of new stars and a new generation of films in Telugu Cinema|Tollywood. Hence the film ended up as a Utter flop.

==Awards==
{| class="wikitable"
|-
! Ceremony
! Category
! Nominee
! Result
|- 2nd South Indian International Movie Awards SIIMA Award Best Cinematographer Chota K. Naidu
| 
|- SIIMA Award Best Actor in a Supporting Role Prakash Raj
| 
|-
|}

==References==
 

==External links==
*  

 
 
 
 