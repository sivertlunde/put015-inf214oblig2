Anhonee (1952 film)
 
{{Infobox film
| name           = Anhonee
| image          = Anhonee1952.jpg
| image_size     = 
| caption        = 
| director       = K. A. Abbas
| producer       = 
| writer         =  
| narrator       = 
| starring       =   Roshan
| cinematography = Ramachandra
| editing        = 
| distributor    = 
| released       = 1952
| runtime        = 155 mins
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Roshan while lyrics were written by Ali Sardar Jafri. Nargis was highly appreciated for enacting a dual role and her performance garnered critical acclaim.  Abbas attempted to explore two concepts—Geneticsm and Determinism, a theme which he experimented in Awaara|Aawara (1951).   

==Plot==
 
The film deals with the story of two sisters—Mohini and Roop—both played by Nargis—the legal one raised by a courtesan and the illegal child. As time passes by, Roop falls in love with Rajkumar Saxena (played by Raj Kapoor), an advocate—who is a tenant—comes to pay the house rent to her father, but instead meets Roop. Soon they involve in a deeper romantic relationship, and Roop convinces her father for their marriage.

As the family plan to organise a party to formally announce the wedding, Rajkumar runs into Mohini, Roops twin sister and comes to know that Mohini is the real daughter. Unable to bear this, Mohini gets into an unpleasant situation, and gets angry over Rajkumar. In the meanwhile, Roop comes to know about the truth, and tries to save Mohini by deciding to swap the position of both of them. During this the marriage happens where Rajkumar unknowingly weds Mohini. When Roops father comes to know about the reality he dies.

==Cast==
* Nargis as Roop H. Singh, Mohinibai (Dual roles)
* Raj Kapoor as Advocate Rajkumar Saxena
* Achla Sachdev as Champa
* Om Prakash as Shyam Sundar Laddan Agha as Vidyasagar David Abraham as Munshiji
* Jankidas
* Salma	as Salma Mirzah
* Shaukat Hashmi
* Moti Bina
* Habib Alkaf

==Production== Hindi film. 

==References==
 

==External links==
 

 

 
 
 
 
 
 