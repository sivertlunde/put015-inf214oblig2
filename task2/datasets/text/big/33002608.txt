Maha Shaktishaali
 
{{Infobox film
| name           =Maha Shaktishaali
| image          = 
| image_size     = 
| caption        = 
| director       =
| producer       =
| writer         =
| narrator       = 
| starring       =Dharmendra Ayesha Jhulka
| music          = Anand-Milind
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1994
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}

Maha Shaktishaali  is a 1994 Bollywood film starring Dharmendra and Ayesha Jhulka.

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Chori Chori Lag Gayee"
| Suresh Wadkar, Sadhana Sargam
|- 
| 2
| "Main Hosh Mein Nahin Hoon"
| Suresh Wadkar
|-
| 3
| "Kamli Han Tere Pyar Mein"
| Mohammad Aziz, Sadhana Sargam
|-
| 4
| "Main Hosh Mein Nahin Hoon"
| Kavita Krishnamurthy
|-
| 5
| "Daud Ke Aaya Main"
| Mohammad Aziz, Mangal Singh
|-
| 6
| "Mohabbat Mein Itne Karib Aagaye"
| Udit Narayan, Sadhana Sargam
|-
| 7
| "Chal Meri Chhappan Chhuri"
| Amit Kumar 
|}

==External links==
*  

 
 
 


 