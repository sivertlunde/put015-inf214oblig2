Enoch Arden (1915 film)
 
{{Infobox film
| name           = Enoch Arden
| image          = Enoch Arden (1915) - Wedding.jpg
| caption        = Film still of the wedding of Enoch and Annie
| director       = Christy Cabanne
| producer       =
| writer         = D. W. Griffith
| based on       =  
| starring       = Alfred Paget Lillian Gish
| cinematography = William Fildew
| editing        =
| distributor    =
| released       =  
| runtime        = 40 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}}
 short drama film directed by  Christy Cabanne. It is based on the poem "Enoch Arden" by Alfred Tennyson, 1st Baron Tennyson|Tennyson.    A print of the film exists at the George Eastman House Motion Picture Collection. 

==Plot==
Based on a summary in a film magazine,  Enoch, Annie, and Walter grow up as friends. Later, Annie decides to marry Enoch, but Walter, though bitter about the decision, remains their friend. Enoch and Annie have two children. Then business takes Enoch on a sailing voyage, which he states will take less than one year, and he asks Walter to look over his family while he is gone. Enoch does not return, and Walter dutifully cares after Enochs wife and children. After ten years word comes of a wreck seen in the Pacific, and everyone believes Enoch has died. Walter and Annie then marry. One night a stranger comes to the house and through a window sees Walter, Annie, and the children happy. The stranger, who is Enoch, finds an old woman who tells him what happened. Enoch tells her to keep his secret, and then leaves. He later dies with a smile on his face.

==Cast==
* Alfred Paget as Enoch Arden
* Lillian Gish as Annie Lee
* Wallace Reid as Walter Fenn
* D. W. Griffith as Mr. Ray
* Mildred Harris as A Child
* Betty Marsh

==References==
 

==External links==
 
* 

 
 
 
 
 
 
 
 
 

 