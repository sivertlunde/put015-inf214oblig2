Noise and Resistance
{{Infobox film
| name           = Noise and Resistance
| image          = 
| caption        = 
| director       =  
| producer       =  
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| music          =  
| cinematography =  
| editing        =  
| studio         = 
| distributor    = Neue Visionen
| released       =  
| runtime        = 87 minutes
| country        = Germany
| language       = English, German, Spanish
| budget         = 
| gross          = 
}} punk music scene. The directors enter the centres of a vivid and vibrant, a rebellious and self-conscious scene. Be it squatters in Barcelona, anti-fascists in Moscow, Dutch trade unionists, the activists of Englands Crass collective, queer trailer park inhabitants in Berlin, or Swedish girl punk bands, their music always expresses a collective self-assertion, a No! set to music whose slogan : Do it yourself! has become a strident 21st century "International".
The film was mainly self-produced by the two directors. It had its international premiere at the 
Crossing Europe film Festival in Linz, Austria.

==Filming locations==
The documentary was shot on various locations, including Barcelona-district Gràcia; the wagon fort Schwarzer Kanal in Berlin; as well as various other locations in Britain, Russia and Sweden.

==External links==
* 
* 
*  

 
 
 
 

 