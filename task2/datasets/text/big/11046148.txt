Kit Kittredge: An American Girl
{{Infobox film
| name = Kit Kittredge: An American Girl
| image = kitposter.jpg
| caption = Theatrical release poster
| director = Patricia Rozema
| producer = Julia Roberts (executive)
| writer = Ann Peacock
| based on =  
| starring = Abigail Breslin Chris ODonnell Julia Ormond Max Thieriot Jane Krakowski Wallace Shawn Glenne Headly Joan Cusack Stanley Tucci
| music = Joseph Vitarelli David Boyd
| editing = Julie Rogers Picturehouse Films American Girl Movies
| distributor = Warner Bros. Pictures
| released =  
| runtime = 100 min.
| country = United States
| language = English
| budget = $10 million
| gross = $17,657,973
}} American Girl character Kit Kittredge, who lives in Cincinnati, Ohio during the Great Depression.  The film is the first in the American Girl films|American Girl film series to have a theatrical release; the first three were television movies. Julia Roberts served as one of the executive producers here (as she did with the TV movies).

== Plot == magician Mr. mobile library driver Miss Bond.

Locally there have been reports of muggings and robberies supposedly committed by hobos. Kit investigates and meets young Will and Countee, who live in a hobo jungle near the Ohio River and Erie Lackawanna Railway. Kit writes a story about the camp and tries to sell it to Mr. Gibson, the mean editor of the Cincinnati newspaper, but he has no interest in the subject. She adopts a dog, her mother buys chickens, and Kit sells their eggs.

Then a locked box containing her mothers treasures is stolen, and a footprint with a star matching the one on Wills boot is discovered, making him the prime suspect. The sheriff goes to find Will and Countee. However, Will and Countee have left the hobo jungle. Kit, Stirling and Ruthie then set out to investigate on the incidents and clear Wills name. It then turns out that Mr. Berk, along with his assistant Frederich, were actually the ones behind the robberies, framing Will and the rest of the hobos for the crime. Kit then becomes a local hero. They found out that Countee has been pretending to be a boy. On Thanksgiving the hobos bring food to Kits mother and Kits father returns home. Mr. Gibson arrives to show Kit that she is in print in Cincinnatis major daily newspaper.

== Cast ==
* Abigail Breslin as Margaret Mildred Kit Kittredge
* Chris ODonnell as Jack Kittredge
* Julia Ormond as Margaret Kittredge
* Max Thieriot as Will Shepherd
* Joan Cusack as Miss Lucinda Bond
* Jane Krakowski as Miss May Dooley
* Madison Davenport as Ruthie Smithens
* Zach Mills as Stirling Howard IV
* Austin MacDonald as Roger
* Willow Smith as Countee (Constance)
* Wallace Shawn as Mr. Gibson
* Glenne Headly as Mrs. Howard
* Stanley Tucci as Mr. Jefferson Jasper Rene Berk
* Colin Mochrie as Mr. Pennington
* Kenneth Welsh as Uncle Hendrick
* Brieanne Jansen as Frances Stone
* Erin Hilgartner as Florence Stone
* Martin Roach as Hobo Doctor
* Elisabeth Perez as Classmate #1
* Jordan Rackley as Classmate #2
* Dylan Scott Smith as Frederich

== Development ==
 ), reflecting their familys status before financial problems stated plaguing the Kittredges.]]
Ideas of a possible feature film revolving around the character had been discussed by the company for several years,    although American Girl president Ellen L. Brothers stated that "it was all brand new to us", and had to explore the feasibility of a live-action film by producing made-for-television adaptations of American Girl characters.    After the successes of   and succeeding TV movies, several options were considered on making the transition to a theatrical feature.

Production of the film, which involved finalizing the script and cast, film preparations and principal photography, took up roughly four months due to limitations on part of Abigail Breslins availability before starting another production. Camera angles were also put into consideration, with the film crew being careful not to shoot areas or objects on the set location that would be out of place in the 1930s setting. A multiple-camera setup was also used to speed up the process. Filming was done in and around Toronto, and in Tottenham, Ontario in the summer of 2006.   
 distressed using sandpaper and trisodium phosphate, fading the colors of the clothes to give a more aged, worn out feel.

Among some of the vehicles used in the film, several 1934  s were also used. The car lot was portrayed as a Chrysler-Plymouth-Dodge dealership in Cincinnati in 1934. An antique Peter Whit Toronto Transit Commission street car stood in for a City of Cincinnati one.

== Critical reception ==
The film received generally favorable reviews from critics. As of April 29, 2011, the review aggregator Rotten Tomatoes reported that 78% of critics gave the film positive reviews, based on 99 reviews, and gave it a Golden Tomato for Best Kids Film of 2008.  Metacritic reported the film had an average score of 63 out of 100, based on 27 reviews. 

Roger Ebert of the Chicago Sun-Times said, "It has a great look, engaging performances, real substance and even a few whispers of political ideas",  and in the New York Times, Jeannette Catsoulis said, "this classy, heart-on-its-sleeve movie is packed with laudable life lessons."  Elizabeth Weitzman of the New York Daily News called it "resolutely old-fashioned" and thought "the script feels a little stiff and moralistic at times," but added, "its hard to fault a film with such an intelligent, good-hearted heroine." 
  Megan Basham of World Magazine said, "Even if young fans cant relate the struggles in the movie to their own life, Kit still offers more than the shows and movies typically aimed at the tween girl market. Besides the simple educational value of giving them a picture to connect with their history lessons, the film also focuses on more significant themes than the materialism and prettiness championed."

== Home video release ==
Kit Kittredge: An American Girl was released on DVD and Blu-ray Disc on October 28, 2008, the only special feature being American Girl trailers.

There are also enhanced DVD-ROM features including an HBO First Look and deleted scenes.

As part of a 4 disc set with 3 other American Girl films, it includes a deleted scene, an HBO look, a featurette on the casting, and a look at the 1930s.

== References ==
 

== External links ==
 
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 