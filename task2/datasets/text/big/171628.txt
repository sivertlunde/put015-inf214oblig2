I Am Sam
{{Infobox film
| name=I Am Sam
| image          = ImAmSamSeanMichelle.jpg
| caption        = Theatrical release poster Jessie Nelson
| producer       = Jessie Nelson Richard Solomon Edward Zwick Marshall Herskovitz
| writer         = Kristine Johnson Jessie Nelson
| starring       = Sean Penn Michelle Pfeiffer Dianne Wiest Dakota Fanning Richard Schiff Loretta Devine Laura Dern John Powell
| cinematography = Elliot Davis
| editing        = Richard Chew The Bedford Falls Company
| distributor    = New Line Cinema
| released       =  
| runtime        = 134 minutes
| country        = United States
| language       = English
| budget         = $22 million 
| gross          = $97,818,139 
}} Jessie Nelson, and starring Sean Penn as a father with a developmental disability, Dakota Fanning as his inquisitive seven-year-old daughter, and Michelle Pfeiffer as his lawyer. Dianne Wiest, Loretta Devine, Richard Schiff and Laura Dern appear in supporting roles. 
Jessie Nelson and Kristine Johnson, who co-wrote the screenplay, researched the issues facing adults with developmental disabilities by visiting the non-profit organization L.A. GOAL (Greater Opportunities for the Advanced Living). They subsequently cast two actors with disabilities, Brad Silverman and Joe Rosenberg, in key roles. 

For his role as Sam, Penn was nominated for the Academy Award for Best Actor at the 74th Academy Awards in 2002.

The movies title is derived from the opening lines "I am Sam / Sam I am" of the book Green Eggs and Ham, which is read in the movie.

==Plot== agoraphobic neighbor Annie (Dianne Wiest) who takes care of Lucy when Sam cannot. Though Sam provides a loving and caring environment for precocious Lucy, she soon surpasses his mental ability. Other children tease her for having a "retard" as a father, and she becomes too embarrassed to accept that she is more intellectually advanced than Sam. 

On the advice of his friends, Sam approaches a high-powered lawyer, Rita Harrison (Michelle Pfeiffer), whose brusque manner, fast-paced schedule and difficult personal life have earned her a reputation as cold and unfeeling. In an attempt to prove to others that she is not heartless, Rita surprisingly agrees to take on Sams case pro bono. As they work together to secure Sams parental rights, Sam unwittingly helps Rita with her family problems, including encouraging her to leave her philandering husband and repairing her fractious relationship with her son.  She and Sam have an emotional moment together when they reveal that they never feel good enough.

At the trial, Sam breaks down after opposing counsel convinces him that he is not capable of being a father. After the trial, Lucy resides in a foster home with Randy Carpenter (Laura Dern), but tries to convince Sam to help her run away, and continually escapes in the middle of the night to go to Sams apartment, whereupon he immediately returns her. Ultimately, the foster family who planned to adopt Lucy decide to return her to Sam, with an arrangement that Randy will help him raise her.

The final scene depicts a soccer game, which Sam referees and in which Lucy participates as a player. In attendance are the foster family, Sams friendship group, and a newly-single Rita with her son.

==Cast==
* Sean Penn as Samuel John "Sam" Dawson
* Michelle Pfeiffer as Rita Harrison Williams
* Dakota Fanning as Lucy Diamond Dawson
* Dianne Wiest as Annie Cassell
* Loretta Devine as Margaret Calgrove
* Richard Schiff as Mr. Turner
* Laura Dern as Miranda "Randy" Carpenter
* Marin Hinkle as Patricia
* Stanley DeSantis as Robert
* Doug Hutchison as Ifty
* Rosalind Chao as Lily
* Ken Jenkins as Judge Philip McNeily
* Wendy Phillips as Miss Wright
* Scott Paulin as Duncan Rhodes
* Kimberly Scott as Gertie
* Michael B. Silver as Dr. Jaslow
* Eileen Ryan as Estelle Dawson
* Mary Steenburgen as Dr. Blake
* Elle Fanning as "Lucy" (aged 2)

==Critical reception==
The film received mixed-to-negative reviews from critics. I Am Sam holds a rating of 34% on Rotten Tomatoes,    and a score of 28 on Metacritic.   

The   wrote that "every device of the movies art is designed to convince us Lucy must stay with Sam, but common sense makes it impossible to go the distance with the premise."    Roger Ebert also criticized the morality tale character of the movie, saying that "you cant have heroes and villains when the wrong side is making the best sense." 

On the other hand, the  , however, found Michelle Pfeiffer to be the standout: "Pfeiffer, enormously likable in the role, almost saves the movie." 

Nick Rogers condemned the film: "Sean Penn gives the most professionally shameful, cruelly wrongheaded performance ever nominated for a Best Actor Oscar, while Jessie Nelson Kristine Johnsons script lives down to Penns ugly ineptitude with idiotic catchphrases and product placement."  Ron Wells added: "This movie is every bit as painful as it sounds." 

==Awards and nominations==
Sean Penn was nominated for the Academy Award for Best Actor (the Oscar), the Screen Actors Guild Award for Outstanding Performance by a Male Actor in a Leading Role, the Broadcast Film Critics Association Award for Best Actor and the Satellite Award for Best Actor - Motion Picture Drama.   
 Las Vegas Satellite Special Young Artist Award for Best Performance in a Feature Film - Young Actress Age Ten or Under. She was also nominated for the Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Supporting Role. 

The soundtrack was nominated for the Grammy Award for Best Compilation Soundtrack Album for a Motion Picture, Television or Other Visual Media. 

The film won the inaugural Stanley Kramer Award from the Producers Guild of America, and was nominated for the Humanitas Prize and the Japan Academy Prize for Outstanding Foreign Language Film. 

{|class="wikitable" style="font-size: 100%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;"| Awarding Body
! style="background:#B0C4DE;"| Award
! style="background:#B0C4DE;"| Nominee
! style="background:#B0C4DE;"| Result
|-
| Academy Awards Best Actor
| Sean Penn
|  
|-
| rowspan="2"| Broadcast Film Critics Association Best Actor
| Sean Penn
|  
|- Best Young Performer
| Dakota Fanning
|  
|-
| Grammy Awards Best Compilation Soundtrack Album for a Motion Picture, Television or Other Visual Media
| 
|  
|-
| Humanitas Prize
| 
| 
|  
|-
| Japanese Academy Awards Outstanding Foreign Language Film
| 
|  
|-
| Las Vegas Film Critics Society Youth in Film
| Dakota Fanning
|  
|-
| Producers Guild of America
| Stanley Kramer Award
| Jessie Nelson Edward Zwick Marshall Herskovitz Richard Solomon
|  
|-
| Phoenix Film Critics Society
| Phoenix Film Critics Society Award for Best Youth Actress
| Dakota Fanning
|  
|-
| rowspan="2"| Satellite Awards Best Actor
| Sean Penn
|  
|-
| Special Achievement Award for Outstanding New Talent
| Dakota Fanning
|  
|-
| rowspan="2"| Screen Actors Guild Outstanding Performance by a Male Actor in a Leading Role
| Sean Penn
|  
|- Outstanding Performance by a Female Actor in a Supporting Role
| Dakota Fanning
|  
|-
| rowspan="2"| Young Artist Awards
| Best Family Feature Film - Drama
|
|  
|-
| Best Performance in a Feature Film - Young Actress Age Ten or Under
| Dakota Fanning
|  
|}

==Soundtrack==
  The Vines and Ben Folds, to cover the songs for the soundtrack. Penns brother, Michael Penn, is also featured on a duet with his wife Aimee Mann.

As the movie was shot and produced to the original Beatles music, the artists had to record their covers to the same musical timing (tempo) as The Beatles original pieces had.

==Remakes==
{| class="wikitable" style="width:50%; text-align:center;"
|- style="background:#ccc;"
| I Am Sam (2001) (Cinema of United States|English) || Main Aisa Hi Hoon (2005) (Bollywood|Hindi) || Deiva Thirumagal (2011) (Cinema of Tamil Nadu|Tamil) 
|-
| Sean Penn || Ajay Devgn || Vikram Kennedy 
|-
| Michelle Pfeiffer || Sushmita Sen || Anushka Shetty 
|}

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 