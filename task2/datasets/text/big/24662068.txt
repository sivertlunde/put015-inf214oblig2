Innocents of Paris
{{Infobox film
| name           = Innocents of Paris
| image          = 
| image_size     = 
| alt            = 
| caption        =  Richard Wallace
| producer       = Jesse L. Lasky
| writer         = C.E. Andrews (play "Flea Market") Ethel Doherty (screenplay) Ernest Vajda (screenplay)
| narrator       =  Russell Simpson
| music          = John Leipold Richard A. Whiting Leo Robin
| cinematography = Charles Lang
| editing        = George M. Arthur
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =   	
| runtime        = 78 minutes
| country        = USA
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
 Richard Wallace and is based on the play Flea Market, the film was the first musical production by Paramount Pictures.

==Cast==
*Maurice Chevalier - Maurice Marney
*Sylvia Beecher - Louise Leval Russell Simpson - Emile Leval
*George Fawcett - Monsieur Marny
*John Miljan - Monsieur Renard
*Margaret Livingston - Madame Renard
*David Durand - Jo-Jo
*Jack Luden - Jules Johnnie Morris - Musician

==Soundtrack==
*"Its A Habit Of Mine"
:Words by Leo Robin
:Music by Richard A. Whiting
:Copyright 1929 by Famous Music Corp.
*"Wait Til You See Ma Cherie"
:Words by Leo Robin
:Music by Richard A. Whiting
:Copyright 1929 by Famous Music Corp.
*"On Top Of The World, Alone"
:Words by Leo Robin
:Music by Richard A. Whiting
:Copyright 1929 by Famous Music Corp.
*"Louise" 
:Words by Leo Robin
:Music by Richard A. Whiting
:Sung by Maurice Chevalier
:Copyright 1929 by Famous Music Corp.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 
* 

 

 
 
 
 