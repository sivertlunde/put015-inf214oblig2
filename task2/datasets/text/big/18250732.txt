The Rocky Road
{{Infobox film
| name           = The Rocky Road
| image          = 
| caption        = 
| director       = D. W. Griffith
| producer       = 
| writer         = D. W. Griffith
| narrator       = 
| starring       = Frank Powell
| music          = 
| cinematography = G. W. Bitzer Arthur Marvin
| editing        = 
| distributor    = Biograph Company
| released       =  
| runtime        = 11 minutes
| country        = United States 
| language       = Silent with English intertitles
| budget         = 
}}
 short silent silent drama film directed by D. W. Griffith and starring Frank Powell. Prints of the film survive in the film archives of the Library of Congress and the Museum of Modern Art.   

==Cast==
* Frank Powell as Ben
* Stephanie Longfellow as Bens Wife George Nichols as The Farmer
* Linda Arvidson
* Kate Bruce as The Maid Charles Craig as A Farmhand / At Church
* Adele DeGarde
* Gladys Egan as At Church Frank Evans as In Bar
* Edith Haldeman as The Daughter, as a Child James Kirkwood as The Best Man
* Henry Lehrman as Outside Bar
* Marion Leonard
* Wilfred Lucas
* W. Chrystie Miller as The Minister
* Owen Moore
* Anthony OSullivan as In Bar
* Harry Solter
* Blanche Sweet as The Daughter, at Eighteen
* J. Waltham as In Bar Dorothy West as At Church (unconfirmed)

==See also==
* D. W. Griffith filmography
* Blanche Sweet filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 