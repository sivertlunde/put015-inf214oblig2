Mr. Saturday Night
 

{{Infobox film
| name = Mr. Saturday Night
| image = Mryshdfbn.jpg
| director = Billy Crystal
| writer = Billy Crystal Babaloo Mandel Lowell Ganz
| starring = Billy Crystal David Paymer Julie Warner Helen Hunt Ron Silver Jerry Lewis
| producer = Billy Crystal
| studio = Castle Rock Entertainment New Line Cinema
| distributor = Columbia Pictures
| released =  
| runtime = 119 minutes
| country = United States
| language = English
| budget =
| gross = $13,351,357
}}
Mr. Saturday Night is a 1992 film that marks the directorial debut of its star, Billy Crystal.

It focuses on the rise and fall of Buddy Young Jr., a stand-up comedian. Crystal produced and co-wrote the screenplay with the writing duo Babaloo Mandel and Lowell Ganz.  It was filmed from 1991 to 1992 and released on September 23, 1992, by Columbia Pictures.
 Best Supporting Actor.

The opening title sequence was designed by Elaine and Saul Bass.

==Plot==

Mr. Saturday Night details how stand-up comedian Buddy Young Jr. became a television star, with the help of his brother and manager, Stan, but alienated many of those closest to him once his career began to fade.
 the Catskills, where he meets his future wife, Elaine.

Buddys fame grows, as does his ego. He hits the big time with his own Saturday night television show. But over the warnings of his brother, Buddy uses offensive material on the air, costing him his show and beginning his career slide.

As an older man, long past his prime, Buddy is estranged from Stan as well as from his daughter, Susan. A chance at redemption comes when a young agent named Annie Wells finds him work and even gets Buddy a shot at a role in a top directors new film. Buddy nevertheless gives into his own self-destructive nature, continuing to take its toll on the comics relationships with his family.

==Cast==

* Billy Crystal as Buddy Young, Jr.
* David Paymer as Stan Young
* Julie Warner as Elaine Young
* Helen Hunt as Annie Wells
* Jerry Orbach as Phil Gussman
* Ron Silver as Larry Meyerson
* Mary Mara as Susan Young
* Jerry Lewis as Himself

==Reception==
The film received mixed reviews from critics. It holds a 56% rating on Rotten Tomatoes based on 25 reviews with the consensus stating: "Billy Crystals flawed directorial debut cant seem to decide whether it wants the viewer to love its protagonist or hate him, but it features fine work from Crystal and his co-stars."

===Box office ===
The film performed poorly at the U.S. box office.   According to boxofficemojo.com it grossed less than $14 million.

==Awards== Best Supporting Best Actor in a Motion Picture - Musical or Comedy.

During the Academy Awards the next year, Crystal hosted. When announcing the nominees for Best Film of 1992 during the opening, Crystal added his movie to the list, immediately afterwards adding the line, "I just wanted to see how it feels."

==Cameos==
Comedians Jerry Lewis, Carl Ballantine, Slappy White, and Jackie Gayle appear in the New York Friars Club scene.

==Home media==
  
The film was released twice on DVD, the first time on December 8, 1998 by PolyGram Video, and again on June 4, 2002 by MGM Home Entertainment.

==See also==
* List of American films of 1992

== References ==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 