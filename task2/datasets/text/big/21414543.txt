Snow White and the Seven Dwarfs (1955 film)
{{Infobox film
| name           = Schneewittchen
| image          = Schneewittchen55.jpg
| image_size     =
| caption        = Title card
| director       = Erich Kobler
| producer       = Hubert Schonger
| writer         = Konrad Lustig Walter Oehmichen
| narrator       = Paul Tripp  (1965-English)  Adele "Addi" Adametz Niels Clausnitzer Dietrich Thoms Renate Eichholz Zita Hitz Erwin Platzer
| music          = Franz Miller Carl Stueber Anne Delugg  (US version)  Milton Delugg  (US version) 
| cinematography = Wolf Swan 
| editing        = Horst Rossberger (as Horst Roßberger)
| distributor    = Jugendfilm-Verleih  (1955) (West Germany) (theatrical)  Childhood Productions  (USA, English-dubbed) (theatrical) 
| released       = November 27, 1955  (West Germany)  October 15, 1962  (Denmark)  November 1965  (USA, English-dubbed) 
| runtime        = 76 min.
| country        = West Germany German
| budget         =
| preceded_by    =
| followed_by    =
}}

Snow White and the Seven Dwarfs (USA: Snow White,  ) is a 1955 German film, directed by Erich Kobler, based on the story of Schneewittchen by the Brothers Grimm.

== Cast ==
* Elke Arendt – Snow White Adele "Addi" Adametz – The Evil Queen
* Niels Clausnitzer – Prince Edelmunt
* Dietrich Thoms – The Huntsman
* Renate Eichholz – The Good Queen
* Zita Hitz – Francisca, the Chambermaid
* Erwin Platzer – The Little Moor

==Production==
Neuschwanstein Castle, in Bavaria, Germany was used as a film set.

The Seven Dwarfs were all played by children from childrens dance group led by Suse Böhm.

== DVD release ==
In 2007, Schneewittchen was released on DVD in Germany.  The film was also part of five DVD boxset, which contained other classic live-action German fairytale films made in the 1950s.

In 2003, the American English-dubbed DVD version was released officially in the United States by Catcom Home Video/Krb Music and then re-issued in 2007. PR Studios then re-issued the DVD in 2008.

== External links ==
*  
*  
*  
*  

 
 

 
 
 
 
 


 