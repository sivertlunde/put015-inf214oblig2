Toto and Marcellino
{{Infobox film
 | name =Toto and Marcellino
 | image = Toto and Marcellino.jpg
 | director = Antonio Musu
 | writer = Pasquale Festa Campanile   Massimo Franciosa
 | starring = Totò Pablito Calvo
 | music = Carlo Rustichelli
 | cinematography = Renato Del Frate
 | editing = Otello Colangeli
 | released = 1958
 | country = Italy
 | language = Italian 
 | runtime = 98 min
 }}
Toto and Marcellino ( ,  ) is a  1958 Italian-French comedy film directed by Antonio Musu.          

== Plot ==
The small Marcellino sees his mother die, and follows her to the funeral. A poor street musician, affectionately called by his fellow citizens "the Professor", sees the little Marcellino follow the coffin of his mother, and decides to adopt him, pretending to his uncle. The two live happily, but one day the real uncle of Marcellino, a greedy and cruel man, discovers the truth about hhis grandson, and sends Marcellino to the reformatory. Later the uncle forces Marcellino to live with him for doing so begging in the street, but the Professor runs to save his darling.

== Cast ==

*Totò: The Professor 
*Pablito Calvo: Marcellino Merini
*Fanfulla: Uncle Alvaro Merini
*Jone Salinas: Ardea
*Memmo Carotenuto: Zeffirino
*Wandisa Guida: The School Teacher
*Nanda Primavera: Rosina

==References==
 

==External links==
* 

 
  
  
 
  
 
 
 

 
 
 