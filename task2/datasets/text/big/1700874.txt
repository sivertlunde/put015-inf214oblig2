Lotna
{{Infobox film
  | name =Lotna
  | image =Lotna poster.gif
  | caption =
  | director = Andrzej Wajda
  | producer = 
  | writer = Andrzej Wajda and Wojciech Żukrowski
  | starring =Jerzy Pichelski Adam Pawlikowski Jerzy Moes Mieczysław Loza Bożena Kurowska Bronisław Dardziński
  | cinematography =
  | music =
  | editing =
  | studio =  
  | distributor = Film Polski 1959
  | runtime = 90 min
  | country = Poland Polish
  | budget =
}} Polish war 1959 and directed by Andrzej Wajda.

==Overview== Soviets during the Katyn massacre.
 Romantic tradition in culture, a tradition that had a huge influence in the course of Polish history and the formation of Polish literature. Lotna is Wajdas meditation on the historical breaking point that was 1939, as well as a reflection on the ending of an entire era for literature and culture in Poland and in Europe as a whole. Writing of the film, Wajda states that it "held great hopes for him, perhaps more than any other." Sadly, Wajda came to think of Lotna "a failure as a film."
 Polish horsemen suicidally charge a unit of German tanks, an event that never actually happened.

==Synopsis== mare that belonged to a wealthy nobleman, is given to Captain Chodakiewicz (Jerzy Pichelski), the commander of a Polish Cavalry Squadron (army)|squadron, and immediately becomes a bone of contention for everyone in the unit.

Lieutenant Wodnicki (Adam Pawlikowski), Cadet Grabowski (Jerzy Moes) and Sergeant Major Laton (Mieczysław Loza) jealously scheme among themselves to get their hands on the horse. However, the war takes the lives of Captain Chodakiewicz and Cadet Grabowski, and Lotna falls is passed to Wodnicki. Laton feels he should get the animal and so he steals Lotna and flees amidst the abandoned supply wagons and equipment of the retreating Polish Army.

== Cast ==
* Jerzy Pichelski as Captain Chodakiewicz
* Adam Pawlikowski as Lieutenant Wodnicki
* Jerzy Moes as Cadet Grabowski
* Mieczysław Loza as Sergeant Major Laton
* Bożena Kurowska as Ewa
* Bronisław Dardziński as nobleman

== See also ==
* Cinema of Poland
* List of Polish language films
* Karol Rómmel

==External links==
*  
*  

 

 
 
 
 
 
 
 