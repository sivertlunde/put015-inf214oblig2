Jewelled Nights
 
 
{{Infobox film
| name           = Jewelled Nights
| image          = 
| image_size     =
| caption        = 
| director       = Louise Lovely Wilton Welch
| producer       = Wilton Welch
| writer         = Louise Lovely Wilton Welch
| based on       = novel by Marie Bjelke-Petersen
| narrator       =
| starring       = Louise Lovely Gordon Collingridge Godfrey Cass 
| music          =
| cinematography = Walter Sully Tasman Higgins
| editing        = Louise Lovely Wilton Welch
| studio    = Louise Lovely Productions
| distributor    = Hellmrich and Conrad
| released       =  
| runtime        = 10,000 feet
| country        = Australia
| language       = Silent film  English intertitles
| budget         = ₤8,016   accessed 6 December 2011 
| gross          = ₤5,000 
| preceded_by    = 
| followed_by    = 
}}

Jewelled Nights is a 1925 Australian silent film directed by the film star Louise Lovely in collaboration with her husband Wilton Welch. Only part of the film survives today.

==Synopsis==
After her fathers death, socialite Elaine Fleetwood promises to marry a man she does not love. However, she leaves him at the altar during a wedding ceremony, cuts her hair and decides to disguise herself as a boy and go prospecting in north west Tasmania. She meets a handsome miner who figures out she is a woman, saves her from a villain and marries her.

==Cast==
*Louise Lovely as  Elaine Fleetwood             
*Gordon Collingridge as Larry Salarno
*Godfrey Cass as Tiger Sam
*Arthur Styan as Crank Ned
*Grafton Williams as Red Roof
*John Dobbie as Tiny Tim
*Charles Brown as Gus
*Harry Halley as wowser
*Leslie Woods as Robert Milton
*Robert Morgan as Sir John Fleetwood
*Clifford Miller as Richard Fleetwood
*George Bryant as Dr Mason
*Reg Leslie as Frank Reid
*Frank Dunn as Dr Hughes
*Katrina Barry as Lady Fleetwood
*Lucille de Rago as Netta
*Joy Law as Nora Foster
*Jean Foulis as Yvette

==Production==
The film was made by Lovely on her return from Hollywood in 1923. She and her husband helped set up a company, Louise Lovely Productions, worth ₤30,000. Among her backers were several businessmen who worked with Arthur Shirleys Pyramid Pictures. 

They constructed an elaborate ₤3,000 studio which contained several large sets, and shot on location at Flemington Racecourse and in Tasmania. This caused to budget to spiral. The sets were used to film a storm in the Tasmanian rain forest.   

==Reception==
The film was well attended at first, being seen by an estimated 350,000 people in Melbourne and 9,000 in Hobart.  However it was unable to recover its costs. Lovely partly attributed to this to the amount of money taken by distributors and exhibitors – she claimed that in one week in Melbourne the film took ₤1,565 out of which the producers received £382. 

Lovely subsequently retired from filming and divorced Welch. Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 129.  

==Reconstruction==
Only two minutes of the film were thought to have survived, along with stills taken during shooting. However using photographic reconstruction, newly found footage, animation and a copy of the original novel annotated by Lovely, archivists have manage to reconstruct 20 minutes of the film. This plays daily at the Gaiety Theatre in Zeehan, near where the movie was shot.  

==References==
 

==External links==
* 
*  at National Film and Sound Archive

 
 
 
 
 
 