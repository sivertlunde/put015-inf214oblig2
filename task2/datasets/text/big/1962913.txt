The Three Musketeers (1993 film)
{{Infobox film
| name = The Three Musketeers
| image = Threemusketeers1993.jpg
| caption = Promotional film poster
| director = Stephen Herek
| producer = Roger Birnbaum Joe Roth Jon Avnet
| screenplay = David Loughery
| based on =  | }}
| starring = Charlie Sheen Kiefer Sutherland Chris ODonnell Oliver Platt Tim Curry Rebecca De Mornay
| music = Michael Kamen
| cinematography = Dean Semler
| editing = John F. Link
| studio = Walt Disney Pictures Caravan Pictures Buena Vista Pictures
| released =  
| runtime = 105 minutes
| country =   United States   Austria 
| language = English
| budget = $30 million
| gross = $53,898,845
}}

The Three Musketeers is a 1993 Austrian-American action-adventure film from Walt Disney Pictures and Caravan Pictures, directed by Stephen Herek from a screenplay by David Loughery and starring Charlie Sheen, Kiefer Sutherland, Chris ODonnell, Oliver Platt, Tim Curry and Rebecca De Mornay. 

The film is loosely based on the novel The Three Musketeers (Les Trois Mousquetaires) by Alexandre Dumas, père. It recounts the adventures of dArtagnan on his quest to join the three title characters in becoming a musketeer. However this adaptation simplifies and alters the story.

==Plot summary==
In 1625 France, following in his late fathers footsteps, DArtagnan (  (Kiefer Sutherland), Porthos (Oliver Platt), and Aramis (Charlie Sheen).

Upon reaching Paris, the headstrong dArtagnan has a series of chance physical encounters with these same three musketeers, resulting in dArtagnan accepting a duel with each one that very day. dArtagnan arrives at the Ruins for his first duel and, much to his surprise, Athos, Porthos and Aramis reveal themselves as musketeers. But before the duels can commence, the captain of the cardinals guard appears with orders to arrest the resistant musketeers. Although dArtagnan himself is not under arrest, he joins the three musketeers in the ensuing skirmish, implicating himself with them. Displeased (but still impressed) by dArtagnans involvement, the three musketeers leave dArtagnan behind. More of the Cardinals guards, led by Rochefort, arrive and dArtagnan is captured.

During an escape attempt, dArtagnan is able to eavesdrop on a conversation between Cardinal Richelieu and Milady de Winter (Rebecca De Mornay), as the cardinal asks that she deliver a signed treaty to the Duke of Buckingham of England. Before he can get a view of the cardinals spy, dArtagnan is caught at the doorway by Rochefort, interrogated by the Cardinal, and ultimately sent for execution the next morning. At the execution, dArtagnan is saved by Porthos and Aramis, and the three make a getaway in the Cardinals personal coach, driven by Athos. While dArtagnan reveals Richelieus plans, the three musketeers decide to intercept Richelieus spy to prove that the Cardinal is guilty of treason.

That night dArtagnan and the three musketeers stop at an inn to rest. Athos tells the story of a count that fell in love with a beautiful woman, but upon discovering that she was branded for execution, betrayed her by giving her up to the authorities. The party decides to split up during a skirmish. Athos sends dArtagnan to ride ahead and intercept the cardinals spy and the treaty, but dArtagnan passes out from exhaustion in the middle of the road. When he wakes up, he finds he is stripped of his weapons and clothes and Milady de Winter is there to rouse and seduce him. Not knowing who the spy is, dArtagnan tells her of his plans, whereupon she tries to kill him. Instead, dArtagnan convinces her to keep him alive. When Milady de Winters party tries to escape by boat to England, they find that the crew has been killed by Porthos and Aramis, and another skirmish ensues. Milady attempts to run away, but is confronted by the newly arrived Athos, who recognizes her and calls her Sabine. He is astonished to see her, as he thought she was dead. It is revealed that he was the count of his story and that Sabine was the wife he betrayed. Milady de Winter is then apprehended by her former brother-in-law, as Sabine is responsible for killing her husband, Lord de Winter, and sentenced to death by execution.

The three musketeers retrieve the treaty and learn that the Cardinal is planning something on King Louis birthday, though it does not specify what in the treaty. Athos attempts to learn what it is by visiting Sabine in her cell. She asks if he can stop her execution tomorrow. Athos cannot and Sabine does not reveal what the Cardinals plan is. During the execution, just as Sabine is lowering her head for the executioner, Athos stops him and begs forgiveness from Sabine for his betrayal. She accepts and whispers to Athos Richelieus plans to assassinate King Louis before jumping off a cliff to her death. After learning of the Cardinals plan, the three musketeers set out to re-band the rest of the musketeers, in secret, for the kings birthday celebration. Richelieu and Rochefort hire a sharpshooter to assassinate the king. During the assembly, dArtagnan is able to stop the sniper from killing the king, but the shot narrowly misses its target and the Cardinal blames the musketeers in the crowd for the attempted assassination.

Athos, Porthos and Aramis drop their cloaks to show their musketeer tunics and face the Cardinals guards. Meanwhile, men from the crowd rush to their sides and reveal that they are musketeers. A battle between the musketeers and the Cardinals guards engulfs the palace. Richelieu takes the king and queen as hostages and tries to take them to the dungeon below. Aramis confronts the Cardinal to stop him, but Richelieu shoots him in the chest with a pistol and makes his way into the passage to the dungeon. Athos duels Rochefort and dArtagnan interrupts the battle to fight Rochefort himself. During dArtagnans duel, Rochefort reveals that he was the one that murdered dArtagnans father, and dArtagnan, from anger, renews his efforts to kill him. Rochefort fights back and is able to disarm dArtagnan. Just as Rochefort is about to deal dArtagnan the final blow, dArtagnans sword is jettisoned back to him and Rochefort is killed before he can strike. dArtagnan has finally avenged his fathers death. Constance, on the stairs, slides the sword away and puts her hands in his, smiling.

Athos joins Porthos, who is at the unconscious Aramis side, and as they search for his wound, Aramis suddenly wakes, and it is revealed that the bullet was stopped by the huge cross that Aramis wears. They follow Richelieu into the dungeons and split up to stop him from killing the king and queen. In the dungeon, Porthos is confronted by the brutal jailer but after a brief fight, manages to defeat him. Afterwards, Athos and Porthos just miss the Cardinal as his boat starts on the underground river. Athos says that they have proof of Richelieus treason, but Richelieu does not seem to care. The boatman then casts off his cloak and the Cardinal is astonished to see that it is Aramis. Aramis attempts to apprehend the Cardinal, but King Louis stops him and punches Richelieu, knocking him in to the river. It is the last time Richelieu appears in the film.

The musketeers are reinstated by the king. Accompanied by Athos, Aramis, and Porthos, dArtagnan is honored in a ceremony. King Louis offers him anything he wants, and all dArtagnan wants - at Athos request - is to serve Louis as a musketeer. King Louis does so. Constance, who has remained by the queens side, runs to him and gives him a passionate kiss, impressing both Aramis and Porthos. Outside Musketeer Headquarters, Gérard and his brothers once again challenge dArtagnan to an immediate duel. dArtagnan tells his new friends that he will take care of this problem and Porthos stops him from continuing, stating that in addition to protecting King and country, musketeers protect each other. dArtagnan calls out, "All for one..." and the rest of the musketeers shout out, "One for all!" The scene ends with Gérard and his brothers being chased by the entire division of musketeers.

==Cast==
*Charlie Sheen as Aramis Athos
*Chris ODonnell as Charles de Batz-Castelmore dArtagnan|DArtagnan
*Oliver Platt as Porthos Cardinal Richelieu Milady
*Gabrielle Anne
*Michael Captain Rochefort
*Paul McGann as Girard and Jussac
*Julie Delpy as Constance King Louis Christopher Adamson as Henri
*Philip Tan as Parker
*Erwin Leder as a Peasant
*Axel Anselm as a Musketeer
*Bruno Thost as Seneschal #1
*Oliver Hoppa as Seneschal #2
*Emma Moore as a Damsel
*Herbert Fux as an Innkeeper
*Nichola Cordey as a Barmaid
*Sebastian Eckhardt as Armand de Winter Bob Anderson (Uncredited) as The Kings Fencing Instructor

== Production ==
Charlie Sheen was originally sought for the role of Porthos before he was cast as Aramis. Kiefer Sutherland, Chris ODonnell and Oliver Platt all endured six weeks of fencing and riding lessons. Sheen missed this training as he was still filming Hot Shots! Part Deux. Brad Pitt and Stephen Dorff turned down the role of DArtagnan, which ultimately went to ODonnell. William Baldwin, Jean-Claude Van Damme, Al Pacino, Johnny Depp, Cary Elwes and Gary Oldman were also sought out by Disney for parts in the film. Winona Ryder was considered for the role of Milady de Winter, but dropped out and Rebecca De Mornay was cast. The Three Musketeers was mostly shot in Perchtoldsdorf, Austria, where De Mornay attended high school and college. A rival TriStar version was also in development at the same time as this film, with Depp and director Jeremiah S. Chechik attached. Ultimately, it fell through. Oliver Platt had also been approached to play Porthos in that version as well.
   

=== Filming locations ===
Filming locations included Charlestown, Cornwall|Charlestown, Cornwall, UK, and Castle Landsee (Burgenland); Burg Liechtenstein, Maria Enzersdorf, Hinterbrühl, Korneuburg (Lower Austria); and Vienna (particularly Hofburg) in Austria. Some sequences were shot in Cornwall, UK. A small woods called Golitha Falls was used in one sequence when the musketeers are being pursued by guards. The small harbour village of Charlestown is home to the galleon that was used in a night-shoot. 

== Reception == Young Guns fame. Janet Maslin of the New York Times described the movie as "Conceived frankly as a product, complete with hit-to-be theme song over the closing credits, this adventure film cares less about storytelling than about keeping the Musketeers feathered hats on straight whenever they go galloping." 
 IMDb website it holds a more favorable 6.3/10 and on Amazon.com it currently holds a rating of 4.3/5.
 Worst Supporting Actor for his work in the film, but lost to Woody Harrelson for Indecent Proposal.

=== Box office ===
The film grossed $11.5 million for the Friday to Sunday weekend, placing it at number 1 at the box office. 

== Soundtrack == All for Love" with Robert John "Mutt" Lange and Michael Kamen  for the movies end credits, performing it with Rod Stewart and Sting (musician)|Sting. As Janet Maslin predicted, the song was a big hit (reaching #1 in North America and several other territories). Kamen also composed the movies score,  conducting the Greater Los Angeles All Star Orchestra.

The soundtrack album was released on compact disc and cassette by Hollywood Records in North America and A&M Records (the label to which both Adams and Sting were signed at the time) elsewhere.

# All For Love - Bryan Adams, Rod Stewart & Sting (4:45) 
# The Cavern Of Cardinal Richelieu (Overture & Passacaille) (2:58) 
# DArtagnan (Galliard & Air) (3:19) 
# Athos, Porthos And Aramis (Courante) (5:24) 
# Sword Fight (Bransle) (3:20) 
# King Louis XIII, Queen Anne And Constance/Lady In Waiting (Gavotte) (5:05) 
# The Cardinals Coach (Estampie) (4:45) 
# Cannonballs (Rigadoon) (3:29) 
# MLady DeWinter (Lament) (4:16) 
# The Fourth Musketeer (Concert Royaux) (5:19)

==References==
 

== External links ==
 
*   
* 
* 
* 
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 