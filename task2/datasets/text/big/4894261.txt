Ue o Muite Arukō: Sakamoto Kyu Monogatari
 
{{Infobox film
 | name = Ue o Muite Arukō
 | image = Ue o Muite Arukō DVD cover.jpg
 | caption = DVD cover
 | script = Nobuo Yamada Tatsuya Yamaguchi Rie Tomosaka
 | released =  
 | runtime = 127 min.
 | language = Japanese
}}

  is a biography based on the Japanese singer Kyu Sakamotos life. The movie was first broadcast on TV Tokyo on August 21, 2005 as a commemoration of the 20th anniversary of Sakamotos death.

==Plot summary==
The movie follows Sakamotos life from being 3 years old to a teenager and finally a family father. Flight 123 plane crash in 1985. It also brings up some more obscure aspects such as surviving a car accident, how he met his wife Yukiko Kashiwagi as well as his time in the Japanese band Paradise King.

==Cast== Tatsuya Yamaguchi - Kyu Sakamoto (Adult)
* Watanabe Takuto - Kyu Sakamoto (Boy) 
* Rie Tomosaka - Yukiko Kashiwagi
* Ikko Furuya - Yutaka Sakamoto (Kyus father)
* Kumiko Okae - Iku Sakamoto (Kyus mother)
* Akira Saitō - Seri Oshima (Kyus grandmother)

==See also==
*Kyu Sakamoto
*Yukiko Kashiwagi
*Hanako Oshima

==External links==
* 

 
 


 
 