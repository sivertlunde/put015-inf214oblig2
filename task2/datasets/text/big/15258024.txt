Dark Waters (1944 film)
 Dark Waters}}
{{Infobox film
| name           = Dark Waters
| image          = Darkwaters1944.jpg
| image_size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = André De Toth
| producer       = Benedict Bogeaus Joan Harrison Arthur Horman John Huston
| based on       =   Thomas Mitchell Fay Bainter Elisha Cook, Jr.
| music          = Miklós Rózsa cinematography   = John J. Mescall Archie Stout
| editing        = James Smith
| distributor    = United Artists
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Gothic film Thomas Mitchell. 

==Plot==
A shaken survivor of a ship sunk by a submarine travels to her aunt and uncles Louisiana plantation to recuperate, but her relatives have other ideas.

==Cast==
* Merle Oberon as Leslie Calvin
* Franchot Tone as Dr. George Grover Thomas Mitchell as Mr. Sydney
* Fay Bainter as Aunt Emily
* Elisha Cook, Jr. as Cleeve
* John Qualen as Uncle Norbert Rex Ingram as Pearson Jackson
* Nina Mae McKinney as Florella

==Reception==
===Critical response===
Slant Magazines film critic, Glenn Heath Jr., liked the film writing, "Mood dictates narrative in Andre de Toths Dark Waters, a hallucinatory jigsaw puzzle set in the deep swamps of 1940s Louisiana that becomes a perfect breeding ground for noirish shadows and deceptive wordplay ... Dark Waters ends with multiple dead bodies sinking into the bayou and Leslie directly confronting what one character calls her "persuasion complex." The bravura finale through the oozing locale is a stunner, and despite some surface romance that feels a bit forced, the film stays true to its mystically dark mood, a slithering distant cousin to Tourneurs I Walked with a Zombie. 

==References==
 

==External links==
*  
*  
*  
*   informational page and DVD review at DVD Beaver (includes images)
*   film at Hulu (free and complete) 

 

 
 
 
 
 
 
 
 
 