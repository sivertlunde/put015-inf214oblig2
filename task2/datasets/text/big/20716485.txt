The Battle of the Sexes (1928 film)
:For Griffiths earlier version, see The Battle of the Sexes (1914 film).
{{Infobox film
| name           = The Battle of the Sexes
| image          = Battle of the Sexes poster.jpg
| image_size     =
| caption        = theatrical poster
| director       = D. W. Griffith
| producer       = Joseph M. Schenck
| writer         = Daniel Carson Goodman (story) Gerrit J. Lloyd (adaptation & titles)
| starring       = Jean Hersholt Phyllis Haver Belle Bennett Don Alvarado Sally ONeil
| music          = Hugo Riesenfeld Nathaniel Shilkret  Shilkret, Nathaniel, ed. Shell, Niel and Barbara Shilkret, Nathaniel Shilkret: Sixty Years in the Music Business, Scarecrow Press, Lanham, Maryland, 2005, pp. 227, 273 and 287. ISBN 0-8108-5128-8 
| cinematography = Karl Struss G.W. Bitzer
| editing        = James Smith
| distributor    = United Artists
| released       =  
| runtime        = 88 minutes
| country        = United States Silent English English intertitles
| budget         =
}} an earlier film he directed in 1914, which starred Lillian Gish.  Both films are based on the short story "The Single Standard" by Daniel Carson Goodman; the story was adapted for this production by Gerrit J. Lloyd.
 sound version Movietone sound-on-film system. In 2004, the film was released on DVD by Image Entertainment. The theme song of the motion picture, "Just a Sweetheart", by Dave Dryer, Josef Pasternack and Nathaniel Shilkret (recorded versions of which are available, for example, on a commercially issued Paul Whiteman CD Shilkret, Nathaniel, ed. Shell, Niel and Barbara Shilkret, Nathaniel Shilkret: Sixty Years in the Music Business, Scarecrow Press, Lanham, Maryland, 2005. ISBN 0-8108-5128-8 ) was omitted from the DVD.

==Plot==
Marie Skinner (Phyllis Haver) is a gold digger with her hooks out for devoted middle-aged family man J.C. Hudson (Jean Hersholt), a portly real estate tycoon, who falls for her when she contrives to meet him.  When his wife (Belle Bennett) and grown children, Ruth (Sally ONeil) and Billy (William Bakewell) discover him dancing with Marie at a nightclub, J.C. leaves home the next day.  Ruth seeks out Marie to shoot her, but is interrupted by Maries boyfriend, jazz hound Babe Winsor (Don Alvarado), who takes a shine to her.  When Judson walks in on them he condemns her licentiousness, but is forced to face his double standard when he witnesses a violent argument between Marie and Babe. Full of contrition, J.C. returns to home and hearth and the bosom of his loving family.   

==Cast==
*Jean Hersholt as William Judson, the Father
*Phyllis Haver as Marie Skinner 
*Belle Bennett as Mrs. Judson, the Mother
*Sally ONeil as Ruth Judson, the Daughter
*Don Alvarado as Babe Winsor 
*William Bakewell as Billy Judson, the Son
*John Batten as A friend of the Judsons

==Notes==
 

==External links==
*   
*  
*  
*  
*   in the New York Times

 

 
 
 
 
 
 
 
 
 
 
 