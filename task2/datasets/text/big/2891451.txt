Love in Magic
{{Infobox film 
| name         = Love in Magic 
| image        = Love in Magic film poster.jpg
| caption      = Theatrical poster
| film name    = {{Film name
| hangul       =  
| hanja        =  
| rr           = Yeonaesulsa
| mr           = Yŏnaesulsa}}
| director     = Cheon Se-hwan
| writer       = Kim Kyu-won 
| starring     = Park Jin-hee   Yeon Jung-hoon
| music        = Park Jeong-weon
| cinematography = Hwang Chul-hyun
| editing      = Nam Na-yeong   Heo Sun-mi
| producer     = Lee Hyo-seung   Lee Geun-du   Jo Yun-ho
| distributor  = CJ Entertainment
| released     =  
| runtime      = 106 minutes
| country      = South Korea
| language     = Korean
| budget       = 
}}
Love in Magic ( ) is a 2005 South Korean romantic comedy film.

== Plot == magician Woo Ji-hoon who one day discovers a hidden camera film on the Internet that shows him having sex with one of his former girlfriends, Koo Hee-won in a motel. Ji-hoon first tracks down Hee-won, who is working as a teacher at a local school. They decide that instead of going to the police, theyd be better off trying to track the films makers themselves and get the film taken offline without making a fuss, since both of their careers could suffer. Ji-hoon and Hee-won start spending their evenings going through all the motels they visited while going out, and slowly rediscover their feelings for each other.

== Cast ==
* Park Jin-hee - Koo Hee-won
* Yeon Jung-hoon - Woo Ji-hoon Jo Mi-ryung - Lee Seon-hee Ha Dong-hoon (aka Haha) - Park Dong-sun
* Oh Yoon-ah - Kim Hyun-joo Kim Ji-seok - Yoon Woo-suk
* Choi Seong-guk - Han Joon-seok
* Choi Won-young - Han Joon-seok
* Park Yong-soo - Koo Hee-wons father
* Lee Kyung-jin - Koo Hee-wons mother
* Kim Ha-eun - Koo Mi-young
* Kang Jae-seop - Mi-youngs boyfriend
* Kim Ji-young - sexy woman at the bar
* Kim Young-hoon - Yoon Woo-suks classmate
* Choi Eun-joo	
* Choi Yeo-jin
* Shim Hyung-tak
* Jung Ui-gap

== External links ==
*    
*  
*  
*  

 
 
 
 


 