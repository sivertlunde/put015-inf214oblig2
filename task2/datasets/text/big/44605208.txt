God Said Ha!
 
{{Infobox film
| name           = God Said Ha!
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = Julia Sweeney
| producer       = Quentin Tarantino
| writer         = Julia Sweeney
| screenplay     = 
| story          = 
| based on       =  
| starring       = Julia Sweeney Quentin Tarantino
| narrator       = 
| music          = Anthony Marinelli
| cinematography = John Hora
| editing        = Fabienne Rawley
| studio         =       
| distributor    = 
| released       =  
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
God Said Ha! is a 1998 film that was written and directed by Julia Sweeney, who was also the movies star. It had its world premiere on 14 March 1998 at the South by Southwest Film Festival and focuses on Sweeneys recollections of when her brother was diagnosed with cancer.

==Synopsis== one woman stage show of the same name where Sweeney discusses her memories of her brother Michael getting diagnosed with lymphoma and her own personal experiences when she discovered that she also had cancer.   

==Cast==
*Julia Sweeney as Herself
*Quentin Tarantino as Himself

==Reception==
Critical reception for God Said Ha! has been predominantly positive and the film currently holds a rating of 86% on Rotten Tomatoes (based on 22 reviews), with the consensus "God Said, Ha! plumbs poignant depths, but Julia Sweeneys sharp, graceful wit makes this one-woman monologue a wise, big-hearted burst of uplifting -- and perhaps therapeutic -- entertainment." 

===Awards===
*Golden Space Needle for Best Film at the Seattle International Film Festival (1998, won) 
*Audience Award at the New York Comedy Festival (1998, won) 

==References==
 

==External links==
*  

 
 
 


 