The Mountain of the Cannibal God
{{Infobox film 
| name           = The Mountain of the Cannibal God
| image          = Mountain_of_cannibal_god_poster3.jpg
| image_size     =
| caption        = 
| director       = Sergio Martino
| producer       = Luciano Martino
| writer         = Cesare Frugoni Sergio Martino
| narrator       = 
| starring       = Ursula Andress Stacy Keach Claudio Cassinelli Antonio Marsina
| music          = 
| cinematography = Giancarlo Ferrando
| editing        = Augenio Alabiso
| distributor    = New Line Cinema 
| released       = 1978
| runtime        = 99 min.
| country        = Italy English
| budget         = 
| preceded_by    = 
| followed_by    = 
}} English dialogue 2001 for its graphic violence and considered a "video nasty".

==Plot==
Susan Stevenson (Ursula Andress) is trying to find her missing anthropologist husband Henry in the jungles of New Guinea. She and her brother Arthur enlist the services of Professor Edward Foster (Stacy Keach), who thinks her husband might have headed for the mountain Ra Ra Me, which is located just off the coast on the island of Roka.

The locals believe that the mountain is cursed, and the authorities wont allow expeditions there. So they surreptitiously head on into the jungle to see if thats where he went. They eventually make it to the island, and after a few run-ins against some unfriendly anacondas, alligators and tarantulas, they meet another jungle explorer named Manolo (Claudio Cassinelli) whos been staying at a nearby mission camp, and agrees to join them in their expedition. 

Matters become complicated when it then turns out that each of them has their own private reasons for coming to the island, and finding Susans husband wasnt part of any of them. Susan and Arthur have secretly been looking for uranium deposits, and then Foster reveals that he has only come there because he had been on the island a few years previously and was taken captive by a tribe of primitive cannibals; he has only returned to see if they still exist, and wipe them out. However, Foster later dies when climbing up a waterfall.
 honey torture, but she is instead turned into a goddess. Manolo is tied up and tortured, while the others are turned into the dish-of-the-day. Manolo and Susan eventually escape after enduring the ordeal.

==Violence==
The uncut European print shows scene of gratuitous animal violence. This includes a monitor lizard being gutted, and a live monkey being devoured by a python. Director Sergio Martino admits he only tacked these on at the distributors insistence. In addition the extended version of the film (credited to the "private collection of the director") features explicit shots of a native girl masturbating, and a simulated sex scene between a tribesman and a wild pig.

== Critical reception ==

AllRovi|Allmovies review of the film was negative, calling it "a graphic and unpleasant film, with all the noxious trademarks intact: gratuitous violence, real-life atrocities committed against live animals and an uncomfortably imperialist attitude towards underprivileged peoples." 

== References ==

 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 