One Hundred Mornings
{{Infobox film
| name           = One Hundred Mornings
| image          = 
| alt            = 
| caption        = 
| director       = Conor Horgan
| producer       = Katie Holly
| screenplay     = Conor Horgan Alex Reid Rory Keenan Kelly Campbell Chris White
| cinematography = Suzie Lavelle
| released       =  
| runtime        = 83 minutes
| country        = Ireland
| language       = English
}}
One Hundred Mornings is a 2009 post-apocalyptic Irish drama film written and directed by Conor Horgan. It was one of three films funded by the Irish Film Board’s Catalyst Project, designed to give up-and-coming filmmakers the opportunity to produce a low-budget feature film. 

Filmed over twenty days in County Wicklow, Ireland, for a total budget of €275,000, it was writer/director Conor Horgans first feature.  The film was produced by Katie Holly for Blinder Films.

==Plot==

Set in a world upended by a complete breakdown of society, two couples hide out in a lakeside cabin hoping to survive the crisis. As resources run low and external threats increase, they forge an uneasy alliance with their self-sufficient hippie neighbour. With no news from the outside world they can’t know how long they must endure living in such close quarters, and with such limited supplies.

Unspoken animosity fills the air, and a suspected affair is driving a wedge between them all. Poorly equipped to cope in a world without technology and saddled with completely conflicting worldviews, everything begins to disintegrate. Finally, each of them faces a critical decision they never thought they’d have to make.

==Release==
One Hundred Mornings premiered at Galway Film Fleadh in July 2009 and received its American premiere at the 2010 Slamdance Film Festival. Its U.S. theatrical release was on 25 March 2011. It is being released in Ireland on the 6 May 2011.

The New York Times commented: "Positioned on the cusp of a dying civilization, "One Hundred Mornings" shows people still bound by rules — like deference to a couple of essentially useless local cops — while coming to terms with an unspeakable future. Packing reams of information into a minimalist screenplay (as an added novelty, the female characters are, if anything, more complex than their male counterparts), the film slowly subordinates sex, death and basic decency to the terrors of a dwindling food supply." 

==Awards==
* Workbook Project Discovery and Distribution Award  Slamdance Film Festival 2010
* Irish Film and Television Award for Cinematography 
*Vortex Sci Fi and Fantasy Award, Rhode Island International Film Festival 2010
*Best Director Award, San Francisco Irish Film Festival, 2010

==References==
 

== External links ==
*  
*  
*  
*  
*  

 
 
 
 
 