The Gaucho
 
 
{{Infobox film
| name           = The Gaucho 
| image          = The-gaucho-1927.jpg 
| caption        = Theatrical release poster
| director       = F. Richard Jones 
| writer         = Douglas Fairbanks 
| starring       = Douglas Fairbanks Lupe Vélez
| producer       = Douglas Fairbanks
| music          = 
| cinematography = 
| editing        = 
| distributor    = United Artists  
| released       =  
| runtime        = 115 minutes Silent English intertitles
| budget         = 
| gross          = $1.4 million {{cite book
 | last = Balio
 | first = Tino
 | authorlink =
 | coauthors =
 | title = United Artists: The Company Built by the Stars
 | date = 2009
 | publisher = University of Wisconsin Press
 | location = Madison, Wisconsin
 | p=93}} 
}}

The Gaucho (the official full title of the film is Douglas Fairbanks as The Gaucho   ) is a 1927 silent film starring Douglas Fairbanks and Lupe Vélez set in Argentina. The lavish adventure extravaganza, filmed at the height of Fairbanks box office clout, was directed by F. Richard Jones with a running time of 115 minutes.

Fairbanks biographer Jeffrey Vance considers the film "a near masterwork" and "an anomaly among his   works.",  Vance also considers it a "daring departure, the film is an effort of unanticipated darkness in tone, setting, and character. The spirit of adolescent boyish adventure, the omnipresent characteristic of his prior films, is noticeably absent. It has been replaced by a spiritual fervor and an element of seething sexuality the likes of which has never been seen before in one of his productions.” 
 
==Cast==
*Douglas Fairbanks - The Gaucho
*Lupe Vélez - The Mountain Girl
*Joan Barclay (as Geraine Greear) - The Girl of the Shrine (younger)
*Eve Southern - The Girl of the Shrine
*Gustav von Seyffertitz - Ruiz, The Usurper Charles Stevens - The Gauchos First Lieutenant
*Nigel De Brulier - The Padre
*Albert MacQuarrie - Victim of the Black Doom
*Mary Pickford - Virgin Mary (cameo)

==Legacy==

A new preservation print of The Gaucho, created by the Museum of Modern Art, was first shown at the Academy of Motion Picture Arts and Sciences in 2008.  It has subsequently been screened at MoMA (2008),  and the San Francisco Silent Film Festival (2009)  to promote the new book Douglas Fairbanks with author Jeffrey Vance introducing the screenings.

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 
 
 


 