By the Sad Sea Waves
 
{{Infobox film
| name           = By the Sad Sea Waves
| image          = By the Sad Sea Waves 1917 HAROLD LLOYD BEBE DANIELS SNUB POLLARD Alfred J Goulding.webm
| caption        = Full film
| director       = Alfred J. Goulding
| producer       = Hal Roach
| writer         = H. M. Walker
| starring       = Harold Lloyd
| cinematography = Walter Lundin
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States  Silent English intertitles
| budget         = 
}}
 short comedy film featuring Harold Lloyd.   

==Cast==
* Harold Lloyd 
* Snub Pollard 
* Bebe Daniels  Frank Alexander as Bather
* William Blaisdell
* Sammy Brooks
* Rudolph Bylek
* Billy Fay (as William Fay)
* Florence Gibson
* Clyde E. Hopkins
* Oscar Larson
* Gus Leonard
* Vivian Marshall
* Belle Mitchell
* Fred C. Newmeyer
* Cora Nye
* Evelyn Page
* Hazel Powell
* Hazel Redmond
* Zetta Robson
* David Voorhees
* Dorothea Wolbert (as Dorothy Wolbert)

==See also==
* List of American films of 1917
* Harold Lloyd filmography

==References==
 

==External links==
* 
*   on YouTube

 
 
 
 
 
 
 
 
 


 