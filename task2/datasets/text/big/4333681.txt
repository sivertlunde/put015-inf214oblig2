A Bucket of Blood
 
{{Infobox film
| name           = A Bucket of Blood
| image          = Bucket of blood affiche.jpg
| caption        = Theatrical release poster
| director       = Roger Corman
| producer       = Roger Corman
| writer         = Charles B. Griffith John Brinkley Fred Katz
| cinematography = Jacques R. Marquette
| editing        = Anthony Carras
| distributor    = American International Pictures
| released       =   }}
| runtime        = 66 minutes
| country        = United States English
| budget         = $50,000   
}} American comedy comedy horror dark comic Bohemian café who is acclaimed as a brilliant sculptor when he accidentally kills his landladys cat and covers its body in clay to hide the evidence.  When he is pressured to create similar work, he becomes murderous. Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 35 
 Beat milieu of 1950s Southern California, Corman creates an entirely different mood from the earlier film. 
 1995 as Showtime network. The Howling and Shake, Rattle and Rock! (1994 film)|Shake, Rattle and Rock!, which credit otherwise unrelated characters played by Miller under the character name.  

==Plot==
One night after hearing the words of Maxwell H. Brock (Julian Burton), a poet who performs at The Yellow Door cafe, the dimwitted, impressionable, busboy Walter Paisley (Dick Miller) returns home to attempt to create a sculpture of the face of the hostess Carla (Barboura Morris). He stops when he hears the meowing of Frankie, the cat owned by his inquisitive landlady, Mrs. Surchart (Myrtle Vail), who has somehow gotten himself stuck in Walters wall. Walter attempts to get Frankie out using a knife, but accidentally kills the cat when he sticks the knife into his wall. Instead of giving Frankie a proper burial, Walter covers the cat in clay, leaving the knife stuck in it.
  beatniks in the café. An adoring fan, Naolia (Jhean Burton), gives him a vial of heroin to remember her by. Naively ignorant of its function, he takes it home and is followed by Lou Raby (Bert Convy), an undercover cop, who attempts to take him into custody for narcotics possession. In a blind panic, thinking Lou is about to shoot him, Walter hits him with the frying pan he is holding, killing Lou instantly.

Meanwhile, Walters boss discovers the secret behind Walters "Dead Cat" piece when he sees fur sticking out of it. The next morning, Walter tells the café-goers that he has a new piece, which he calls "Murdered Man".  Both Leonard and Carla come with Walter as he unveils his latest work and are simultaneously amazed and appalled. Carla critiques it as "hideous and eloquent" and deserving of a public exhibition. Leonard is aghast at the idea, but realizes the potential for wealth if he plays his cards right.

The next night, Walter is treated like a king by almost everyone, except for a blonde model named Alice (Judy Bamber), who is widely disliked by her peers. Walter later follows her home and confronts her, explaining that he wants her to model. At Walters apartment, Alice strips nude and poses in a chair, where Walter proceeds to strangle her with her scarf. Walter creates a statue of Alice which, once unveiled, so impresses Brock that he throws a party at the Yellow Door in Walters honor. Costumed as a carnival fool, Walter is wined and dined to excess.

After the party, Walter later stumbles towards his apartment. Still drunk, he beheads a factory worker with his own buzz-saw to create a bust. When he shows the head to Leonard, his boss realizes that he must stop Walters murderous rampage and promises Walter a show to offload his latest "sculptures". At the exhibit, Walter proposes to Carla, but she rejects him. Walter is distraught and now offers to sculpt her, and she happily agrees to after the reception. Back at the exhibit, however, she finds part of the clay on one figure has worn away, revealing a human finger. When she tells Walter that there is a body in one of the sculptures, he tells her that he "made them immortal", and that he can make her immortal too. She flees the exhibit, and he chases after her. Meanwhile, the others at the exhibit learn Walters secret as well, and chase after them. Walter and Carla wind up at a lumber yard where Walter, haunted by the voices of Lou and Alice, stops chasing Carla, and runs home. With discovery and retribution closing in on him, Walter vows to "hide where theyll never find me". The police, Carla, Leonard and Maxwell break down Walters apartment door only to find that Walter has hanged himself. Looking askance at the hanging corpse, Maxwell proclaims that this could be "his greatest work" and that he would probably have named it "Hanged Man".

==Production and release history==
In the middle of 1959, American International Pictures approached Roger Corman to direct a horror film, but only gave Corman a $50,000 budget, and a five-day shooting schedule, plus left over sets from Diary of a High School Bride (1959). Mark McGee, Faster and Furiouser: The Revised and Fattened Fable of American International Pictures, McFarland, 1996 p145  
 satirical black comedy horror film about the beatnik culture.    Charles Griffith later claimed Corman was very uneasy at the idea of making a comedy "because you have to be good. We dont have the time or money to be good so we stick to action." Beverly Gray, Roger Corman: Blood Sucking Vampires, Flesh Eating Cockroaches and Driller Killers AZ Ferris Publications 2014 p 48  

Griffith says he talked Corman around by pointing out the film was made for such a little amount of money over such a short schedule, he could not fail to make money. 

Corman says that genesis of the film was an evening he and Griffith "spent drifting around the beatnik coffeehouses, observing the scene and tossing ideas and reactions back and forth until we had the basic story."   accessed 20 April 2014  The director says by the end of the evening they developed the films plot structure,  partially basing the story upon Mystery of the Wax Museum.  

Griffith says Corman was uneasy how to direct comedy and Griffith, whose parents were in vaudeville, advised him that the key was to ensure the actors played everything straight. 

The film was shot under the title The Living Dead    and filming started 11 May 1950. FILMLAND EVENTS: Ilona Massey Signed for Airplane Drama
Los Angeles Times (1923-Current File)   05 May 1959: A13.  

According to actor Antony Carbone, "  had a kind of spirit of having fun, and I think   realized that while making the film. And I feel it helped him in other films he made, like  —he carried that Bucket of Blood idea into that next film."  

Actor Dick Miller was unhappy with the films low production values. Miller is quoted by Beverly Gray as stating that:
 If theyd had more money to put into the production so we didnt have to use mannequins for the statues, if we didnt have to shoot the last scene with me hanging with just some gray makeup on because they didnt have time to put the plaster on me, this could have been a very classic little film. The story was good, the acting was good, the humor in it was good, the timing was right, everything about it was right—but they didnt have any money for production values, and it suffered.  

==Release== 1958 book with the same title. The films poster consists of a series of comic strip panels humorously hinting at the films horror content.

According to Tim Dirks, the film was one of a wave of "cheap teen movies" released for the Drive-in theater|drive-in market. They consisted of "exploitative, cheap fare created especially for them   in a newly-established teen/drive-in genre."  


When Corman found that the film "worked well," he continued to direct two more comedic films scripted by Griffith,  The Little Shop of Horrors, a farce with a similar plot to Bucket of Blood and using the same sets,   and Creature from the Haunted Sea, a parody of the monster movie genre.

The film is in the public domain  and has been widely distributed on home video from various companies. The films negative was acquired by MGM Home Entertainment upon the companys purchase of Orion Pictures, which had owned the AIP catalog. MGM released A Bucket of Blood on VHS and DVD in 2000.   MGM re-released the film as part of a box set with seven other Corman productions in 2007. However, the box set featured the same menus and transfer as MGMs previous edition of the film. 

==Remakes== 1995 remake, A Bucket of Blood was produced by Chicagos Annoyance Theatre in 2009. http://www.theannoyance.com  It opened Sept. 26, and closed Oct. 31, 2009, garnering exceptional reviews,  including a recommendation from the Chicago Reader.   The musical was directed by Ray Mees, with music by Chuck Malone. The cast included James Stanton as Walter Paisley, Sam Locke as Leonard, Peter Robards as Maxwell, Jen Spyra as Carla, Colleen Breen as Naolia, Maari Suorsa as Alice, Tyler Patocka as William and Peter Kremidas as Lee. 

==See also==
* List of films in the public domain

==References==
 

==External links==
 
* 
* 
* 
*   at Google Videos
*  at Rotten Tomatoes
*  on Livestream
* 

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 