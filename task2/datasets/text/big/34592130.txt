Cruel Passion
{{Infobox film
| name = Cruel Passion
| image =Cruel Passion UK 2012 DVD cover.JPG
| caption =2012 UK DVD cover
| director = Chris Boger
| producer =
| writer = Marquis de Sade (novel) Ian Cullen Martin Potter Lydia Lisle Katherine Kath
| music =
| cinematography =
| editing =
| distributor =
| released = 1977
| runtime =
| country = United Kingdom
| language = English
| budget =
}}
 Martin Potter, Lydia Lisle, and Katherine Kath. It was directed by Chris Boger. 

==Plot==
Justine is a young virgin thrown out of a French orphanage and into the depraved world of prostitution. She slips into a life of debauchery, torture, whipping, slavery and salaciousness while her brazen, flirtatious and liberated sister Juliette ironically receives nothing but happiness and reward for her wanton behaviour. As told by the Marquis de Sade.

==Cast and characters==
*Koo Stark as Justine Jerome Martin Potter as Lord Carlisle
*Lydia Lisle as Juliette Jerome
*Katherine Kath as Madame Laronde
*Hope Jackman as Mrs Bonny
*Barry McGinn as George
*Louis Ife as Pastor John
*Maggie Petersen as Mother Superior
*David Masterman as Archer
*Ian McKay as Brough
*Ann Michelle as Pauline

==DVD==
The DVD is now available uncut in the United Kingdom as "Cruel Passion" and in the United States as  "Marquis de Sades Justine" 

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 
 