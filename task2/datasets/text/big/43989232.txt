Vinayapoorvam Vidhyaadharan
{{Infobox film
| name = Vinayapoorvam Vidyaadharan
| image =
| caption =
| director = KB Madhu
| producer = PG Mohan
| writer = KB Madhu
| screenplay = P Suresh Kumar Sukanya Sukumari Madhupal
| music = Kaithapram
| cinematography = MJ Radhakrishnan
| editing = G Murali
| studio = Film Folks
| distributor = Film Folks
| released =  
| country = India Malayalam
}}
 2000 Cinema Indian Malayalam Malayalam film, directed by KB Madhu and produced by PG Mohan. The film stars Jagathy Sreekumar, Sukanya (actress)|Sukanya, Sukumari and Madhupal in lead roles. The film had musical score by Kaithapram.    

==Cast==
  
*Jagathy Sreekumar  Sukanya 
*Sukumari 
*Madhupal 
*Rajan P Dev 
*Aloor Elsy 
*Harishree Ashokan 
*Indrans 
*Jagadish 
*Jolly Easow
*Kalabhavan Rahman 
*Kochu Preman 
*Manju Pillai 
*N. F. Varghese 
*Kozhikode Narayanan Nair 
*Ponnamma Babu 
*Salim Kumar 
*Salu Koottanad
*Spadikam George 
*Valsala Menon 
 

==Soundtrack==
The music was composed by Kaithapram. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Enniyaal Theeraatha   || K. J. Yesudas || Kaithapram || 
|- 
| 2 || Enniyaal Theeratha   || KS Chithra || Kaithapram || 
|- 
| 3 || Kaattu Valli || K. J. Yesudas, Swarnalatha || Kaithapram || 
|- 
| 4 || Kaattu Valli Oonjaalaadaam   || K. J. Yesudas || Kaithapram || 
|- 
| 5 || Paadanariyilla || KS Chithra || Kaithapram || 
|- 
| 6 || Paadanariyilla (M) || K. J. Yesudas || Kaithapram || 
|- 
| 7 || Ponnumkudathinu || K. J. Yesudas || Kaithapram || 
|- 
| 8 || Tholil Maarappu || K. J. Yesudas || Kaithapram || 
|}

==References==
 

==External links==
*  

 
 
 


 