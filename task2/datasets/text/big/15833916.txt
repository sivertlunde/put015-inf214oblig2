Unborn but Forgotten
{{Infobox film name           = Unborn But Forgotten  image          = Unborn But Forgotten film poster.jpg director       = Im Chang-jae producer       = Yu Hee-suk writer         = Han Hyun-geun starring       = Lee Eun-ju Jung Joon-ho music          = Kim Jeong-a cinematography = Park Hui-ju editing        = Kyung Min-ho distributor    = CJ Entertainment released       =   runtime        = 95 minutes country        = South Korea language       = Korean gross          =    . Box Office Mojo. Retrieved 19 February 2008. 
| film name      = {{Film name hangul         =   rr             = Hayanbang mr             = Hayanbang}}
}} 2002 South The Ring and FeardotCom.

== Plot ==
TV producer Han Su-jin investigates a string of mysterious deaths involving pregnant women after they have visited a website. Out of curiosity Han visits the website; later she starts to have bizarre hallucinations, suggesting that her death may be imminent. Han hires a detective named Choi to help her solve the mystery. 

== Cast ==
* Lee Eun-ju as Han Su-jin
* Jung Joon-ho as Lee Seok
* Kye Seong-yong
* Myeong Ji-yeon
* Kim Ki-hyeon
* Jo Seon-mook
* Kim Jae-rok
* Lee Kan-hee as Hospital director
* Kim Kyeong-ik

== Music ==
The song "Awen" written and performed by Irish duo RUA (Liz Madden and Gloria Mulhall) was used as the theme song for the main character. This song was taken from their first album RUA released by Celtic Collections.

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 

 
 