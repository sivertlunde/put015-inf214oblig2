800 Two Lap Runners
{{Infobox film
| name =800 Two Lap Runners
| image = 
| image_size = 
| caption = 
| director = Ryūichi Hiroki
| producer = Isao Yoshiwara
| screenplay = Masato Kato
| based on = novel by Makoto Kawashima
| narrator = 
| starring = Shunsuke Matsuoka Eugene Nomura
| music = Motohiro Tomita
| cinematography = Naoki Kayano
| editing = 
| distributor = Herald Ace ( )
| released =  
| runtime = 110 min.
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

800 Two Lap Runners is a 1994 Japanese film directed by Ryūichi Hiroki starring Shunsuke Matsuoka and Eugene Nomura. For director Hiroki, the film marks a transition from his early work in pink film to mainstream cinema.   

==Plot==
This coming-of-age story revolves around two young long distance runners, Kenji Hirose and Ryuji Nakazawa. Kenji, haunted by memories of his dead friend Aihara with whom he had a brief homosexual affair, is now dating Aiharas former girlfriend Kyoko, but Kyoko is more interested in Kenji than he is in her. Ryuji, Kenjis friend and track rival, is pursuing hurdler Shoko but she in turn is after Kenji. Ryuji does have his own admirer, Nao, Kenjis younger sister, but when they do get together, Nao resembles her brother too much for Ryuji to go through with the lovemaking.

==Cast==
*   as Kenji Hirose
*   as Ryuji Nakazawa
*   as Shoko Ida
*   as Kyoko Yamaguchi
*   as Nao Hirose
*   as Aihara
*   as Tomomi Ogawa
*   as Shunichi Saitō

==Release== Pioneer LDC on May 24, 2002. 

==Reception==
Kinema Junpo magazine placed the film at number 7 in its list of the ten best Japanese films of 1994.   At the 16th Yokohama Film Festival held in January 1995, 800 Two Lap Runners was listed as the fourth best film of 1994  and actor Shunsuke Matsuoka was given one of the Best New Talent Awards.  The film also took two awards at the 49th annual Mainichi Film Awards, Eugene Nomura for Best New Talent and Naoki Kayano for Cinematography. 

==References==
 

==External links==
*  
*  

 

 
 
 
 

 