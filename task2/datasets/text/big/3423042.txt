Rain Without Thunder
{{Infobox Film
 | name = Rain Without Thunder
 | image = RainWIthoutThunderPoster.jpg
 | caption = Rain Without Thunder Theatrical Poster
 | director = Gary O. Bennett
 | producer = {{plainlist|
* Gary Sorensen
* Nanette Sorensen
}}
 | writer = Gary O. Bennett
 | starring = {{Plainlist|
* Betty Buckley
* Jeff Daniels
}}
 | music = {{plainlist|
* Allen Lynch
* Randall Lynch
}}
 | cinematography = Karl Kases
 | editing = {{plainlist|
* Mallory Gottlieb
* Suzanne Pillsbury
}}
 | distributor = Orion Classics
 | released =   }}
 | runtime = 85 minutes
 | language = English
 | country = United States
 | budget =
 | gross = $5000 (US) 
 }} Gary O. Bennett and starring Betty Buckley and Jeff Daniels. The film is set fifty years in the future from the time of production. Although the Planned Parenthood v. Casey case is never mentioned by name, the decision took place in the same year as the film was made and many characteristics of the society portrayed are clearly a reaction to the growing possibilities of restricting abortion rights at the time of production.  The film is presented as a documentary about the Goldring case, a mother and daughter imprisoned at the Walker Point Center for seeking an abortion outside of the United States. Although Beverly (Betty Buckley) and Allison (Ali Thomas) are the main focus, the journalist (Carolyn McCormick) also interviews numerous people with varying viewpoints discussing the ramifications of the Goldring case and abortion in general in 2042 society.

== Plot ==

Note: This plot summary is a linear account of the Goldrings story. The film itself does not reveal all plot details in order.

Allison Goldring, an upper-class, white college student, becomes pregnant with her boyfriend Jeremy Tanner (Steve Zahn). After discussing her options with both Tanner and her family, she makes the decision to travel abroad to terminate the pregnancy, as abortion is prosecuted as "fetal murder" in the United States. According to Allison and her mother Beverley, everyone – including Tanner – supported her decision. Tanner later denies this, though the film makes his denial seem improbable. Allisons father and grandmother are interviewed and openly support both Allison and Beverly.  Allisons father says that he originally intended to go along with them and that the choice to prosecute Beverly is arbitrary; ultimately, Beverly is perceived to have a greater influence on Allison.

Later interviews give further background on the society: civil liberties are slowly and methodically curtailed over time in order fight "hypercrime".  In the early twenty-first century, restrictions on warrants are loosened, and several states pass laws criminalizing abortion.  At first, only abortionists are targeted by the laws, and complacent feminists dismiss the idea that the situation will get worse.  When the Roman Catholic Church accepts barrier contraception, feminism becomes further weakened, and a wave of pro-life legislation is passed, culminating in a new amendment to the United States Constitution that defines personhood at conception.  Following that, laws are enacted that target women seeking abortions, and feminism becomes not only politically incorrect but also subject to historical revisionism that denies its impact.

The state of New York has recently passed a law that classifies going abroad to seek a termination as "fetal kidnapping". Beverly admits to being aware of the change but assumed it would be some time before it would be enforced. It is not clear how aware Allison and Jeremy were of the legal change. The law is a reaction to a lawsuit aimed at overturning fetal murder statutes because they are enforced almost exclusively against poor minority women. Examples of such women are interviewed at Walker Point (Ming-Na and Bahni Turpin). One had used some abortifacient called a "baby bomb". She was arrested as she bled out after improperly administering the drug. The other was initially arrested on suspicion of having a termination, but is convicted of using  an IUD, which is also illegal. Her descriptions of how she obtained the "uudee" suggest that she was also in a potentially dangerous medical situation. 

African American district attorney Andrea Murdoch (Iona Morris) discovers what the Goldrings have done and prosecutes them under the new law, in large part because they are exactly the type of women targeted by the law. The criminal procedures show that doctor-patient confidentiality is no longer guaranteed. Murdochs motivations are questioned by Jonathan Garson (Jeff Daniels), the Goldrings attorney, who suggests she is seeking higher office, although he doesnt question her ethics. Murdochs own statements suggest that she is angered by the racial and class disparities in enforcement, but she does not question the propriety of fetal murder law. 

During the trial, Allison decides to take the stand and confesses to what she did. She does not express remorse at the time nor does she express any regret later. She says that she felt relieved to get everything out. Beverly and Garson are frustrated by her decision, since it condemns both Allison and Beverly to prison. At the end of the film, the Swedish clinic checks their pathology reports on Allison and determines that the fetus had been dead for almost three weeks prior to the procedure. The Goldrings are released, but Murdoch declares her intention to prosecute them on attempted fetal kidnapping, on the grounds that they had intended to commit the crime even if they had not be able to commit it.

== Cast ==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|- 
| Betty Buckley || Beverly Goldring
|- 
| Katharine Crost || Walker Point Guard #1
|- 
| Jeff Daniels || Jonathan Garson
|- 
| Frederic Forrest || Warden
|- 
| Graham Greene || Author on History
|- 
| Linda Hunt || Atwood Society Director
|- 
| Robert Earl Jones || Old Lawyer
|- 
| Carolyn McCormick || Reporter
|- 
| Ming-Na Wen || Uudie Prisoner (as Ming-Na Wen)
|- 
| Iona Morris || Andrea Murdoch
|- 
| Austin Pendleton || Catholic Priest
|- 
| Ethan Phillips || Gynecologist
|- 
| Ali Thomas || Allison Goldring
|- 
| Steve Zahn || Jeremy Tanner
|- 
| Alyssa Rallo Bennett || Max Sinclair
|- 
| Heather Lilly || Micka Goldring
|}

== Production ==
Writer and director Gary Bennett began writing Rain Without Thunder in 1988 after he became frustrated with pro-choice acquaintances who voted for conservative Republicans due to their economic policies.  He wanted to avoid overt science fiction themes to keep the film relevant, and he included arguments for the opposing side to avoid creating a one-sided message. 

Rain Without Thunder was budgeted at under two million dollars. 

== Release ==
Orion Classics released Rain Without Thunder on February 5, 1993, in Los Angeles.   It was released on video in 1995.   

== Reception ==
Rotten Tomatoes, a review aggregator, reports that 50% of six surveyed critics gave the film a positive review; the average rating was 5.7/10.   Roger Ebert rated the film 1/5 stars and called it "the longest 85-minute movie I have ever seen."   Janet Maslin of The New York Times called it "a dreary, nondramatic tract" with "a drab parade of earnest talking heads".   Hal Hinson of The Washington Post described it as a "brilliant, haunting mock-documentary" that invokes Nineteen Eighty-Four.   Desmond Ryan of the Philadelphia Inquirer wrote that the film "reduces an intensely emotional and politically volatile issue to a sterile debate" by an "endless succession of talking heads".   Melissa Pierson of Entertainment Weekly rated it C- and called it a "dreary fantasy" that is too unpleasant to sell even to those who agree with its message.   Johanna Steinmetz of the Chicago Tribune wrote, "This movie is rain without thunder, not to mention drama without tension, acting without reacting, and thought without passion. It is entirely talking heads, if nothing else a provocative test of the mediums preference for action."   Michael Wilmington of the Los Angeles Times wrote the film is "thought-provoking" yet spends all its time preaching to the converted, explaining in-universe history to people who already know it, and setting up questionable class prejudice.   David Sterritt of The Christian Science Monitor called it a thoughtful, balanced film that is too ambitious.   Jay Boyar of the Orlando Sentinel called it "intellectually stimulating" but lacking in action and faith in its ability to convince viewers without manipulation. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 