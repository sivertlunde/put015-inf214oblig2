For Heaven's Sake (1926 film)
 
{{Infobox film
| name           = For Heavens Sake
| image          = Poster - For Heavens Sake 02.jpg
| caption        = Theatrical release Poster Sam Taylor
| producer       = Harold Lloyd
| writer         = Jim Mason (as James Mason) Paul Weigel
| music          =
| cinematography = Walter Lundin
| editing        = Allen McNeil
| distributor    = Paramount Pictures
| released       =  
| runtime        = 58 minutes
| country        = United States
| language       = Silent film  English intertitles
| budget         =
}} 
For Heavens Sake is a 1926 comedy silent film starring Harold Lloyd. Commercially, it was one of Lloyds most successful films and the 12th highest-grossing film of the silent era, pulling in United States dollar|$2,600,000.

== Plot ==
J. Harold Manners (Lloyd) is a millionaire playboy who accidentally finds himself in the poor part of town one day.

There, he unknowingly gives a man enough money to start a religious mission.  Once he hears that a mission was started in his name, he goes there to tell them to take his name off since (as he believes) he didnt have anything to do with it.  Once there, however, he falls for the Downtown Girl, Hope (Ralston), who works in Brother Pauls (Weigel) rebuilt mission. In order to build up attendance, and win Hopes attention, Harold runs through town causing trouble, and winds up with a crowd chasing him right into the mission. He eventually wins the girl and they marry, but not without some interference from his high-brow friends.

== Production ==

In the late 1920s, Lloyd alternated between making what he called "gag pictures" and "character pictures".  This was a "gag picture".

This was the first Lloyd film distributed by Paramount Pictures, and was a difficult production for Lloyd and his film company.  Numerous scenes were filmed and later cut from the released version.  Some of the cut elements, especially an underworld theme, were incorporated into Lloyds 1928 film Speedy (film)|Speedy.  Lloyd was somewhat disappointed in the final product, and considered shelving the picture.  However, it grossed over 2 million dollars upon release.

==References==
 

== External links ==
*  
*  
*  
*  

 
 
 
 
 
 
 