A Son of Erin
{{infobox film
| name           = A Son of Erin
| image          =
| caption        =
| director       = Julia Crawford Ivers
| producer       = Pallas Pictures
| writer         = Julia Crawford Ivers(story, screenplay)
| starring       = Dustin Farnum Winifred Kingston
| cinematography = J. O. Taylor
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes; 5 reels
| country        = United States
| language       = Silent(English intertitles)
}}
A Son of Erin is an extant 1916 silent film comedy-drama directed by Julia Crawford Ivers. It was produced by Pallas Pictures and distributed by Famous Players-Lasky and Paramount Pictures. Dustin Farnum and Winifred Kingston(real life husband and wife) star.   

==Cast==
*Dustin Farnum - Dennis OHarhar
*Winifred Kingston - Katie OGrady
*Tom Bates - Patrick OGrady
*Jack Livingston - Brian Trelawney
*Wilfred McDonald - Terence
*Wallace Pyke - Dan OKeefe
*Lee Willard - George Harding
*Mabel Wiles - Florence Harding
*Hugo B. Koch - John D. Haynes (as Hugh B. Koch)

==References==
 

==External links==
* 
* 

 
 
 
 
 
 

 