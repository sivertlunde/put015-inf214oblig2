Made (2001 film)
{{Infobox film
| name = Made
| image = Made film.jpg
| imagesize = 200px
| caption = Theatrical release poster
| director = Jon Favreau
| producer = Jon Favreau Vince Vaughn Peter Billingsley
| writer = Jon Favreau
| starring = Jon Favreau Vince Vaughn Peter Falk Sean Combs
| music = John OBrien Lyle Workman
| cinematography = Christopher Doyle
| editing = Curtiss Clayton
| distributor = Artisan Entertainment
| released = July 13, 2001
| runtime = 94 minutes
| country = United States
| language = English
| budget = $5 million
| gross = $5,480,653 	 
}}

Made is a 2001 American comedy/crime film written and directed by Jon Favreau. It stars Favreau, Vince Vaughn, Peter Falk, and Sean Combs. It was Favreaus directorial debut.

==Plot==
Bobby has ties to the local mafia boss, Max, but works as an honest mason for Maxs construction projects. He fights in amateur boxing matches on the side, but his career is lackluster (five wins, five losses, one draw). Struggling to support his stripper girlfriend Jessica and her daughter Chloe, Bobby decides to do a mafia job for Max. Against his better judgment, he brings along his neer-do-well friend Ricky.
 New York East Coast Westie contacts.

Ricky grows suspicious of Ruiz, and insists that they bring a gun to their meeting with the Westies. Bobby adamantly refuses. On the day of the meet, Ricky has disappeared, but Jimmy insists that Bobby carry on with the meeting. As Bobby begins to grow suspicious of Jimmy, he meets with the Welshman and the Westies. The Westies double-cross Bobby and the Welshman, but Ricky arrives from a side entrance with a gun. A Westie recognizes Rickys weapon as a starter pistol and a fight breaks out. Jimmy arrives with a real pistol and sends the boys away while he deals with the Westies.

Back in Los Angeles, Bobby severs all business ties with Max. Arriving home, he discovers Jessica in bed with a client and snorting cocaine. Bobby tries to convince Jessica to clean up her act for Chloes sake, but Jessica refuses. Instead, she asks that Bobby take custody of Chloe and leave. In an epilogue set at Chuck E. Cheeses, we learn that Bobby and Ricky are now raising Chloe together, although the two friends still bicker constantly.

==Cast==
*Jon Favreau as Bobby Ricigliano
*Vince Vaughn as Ricky Slade
*Peter Falk as Max
*Famke Janssen as Jessica
*P. Diddy as Ruiz
*Faizon Love as Horrace
*Vincent Pastore as Jimmy
*David OHara as The Welshman
*Makenzie Vega as Chloe
*Sam Rockwell as Hotel Clerk (uncredited)

==Movie connections==
 
Because the film is written by Jon Favreau and stars Favreau and Vince Vaughn, it is commonly seen as a follow-up to Swingers (1996 film)|Swingers. The license plate of Jimmys Limo, "DBLDN11," is a reference to a blackjack strategy articulated in Swingers that one should "always double down on an 11."

During Dustin Diamonds cameo, Ricky refers to him as "Saved By the Bell#Samuel "Screech" Powers|Screech," referencing Diamonds character on Saved By the Bell.

==Reception==
The critical reception of the film was positive, receiving a 71% "Fresh" rating at Rotten Tomatoes. The sites consensus reads "Not as good as Swingers, but its still witty and goofy enough for some laughs." {{cite web
  | title = Made (2001)
  | publisher = Rotten Tomatoes
  | url = http://www.rottentomatoes.com/m/made
  | accessdate = 2007-03-21 }}  The film received a limited release in the United States and had almost no release overseas. It had a world box office gross of $5.3 million. {{cite web
  | title = MADE
  | publisher = Box Office Mojo
  | url = http://www.boxofficemojo.com/movies/?id=made.htm
  | accessdate = 2007-03-21 }} 

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 