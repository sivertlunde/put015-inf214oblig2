The White Moll
{{infobox film
| title          = The White Moll
| image          = Pearl White in The White Moll by Harry Millarde.png
| imagesize      =
| caption        = Lobby poster
| director       = Harry Millarde Anthony J. Merlo (second director) William Fox
| writer         = Frank L. Packard (novel) E. Lloyd Sheldon (scenario)
| starring       = Pearl White
| cinematography = Eddie Wynard
| editing        =
| distributor    = Fox Film Corporation
| released       = July 24, 1920
| runtime        = 5-8 reels
| country        = United States Silent (English intertitles)
}}
 lost  1920 American silent feature length crime drama film directed by Harry Millarde and starring Pearl White. It was produced and distributed by the Fox Film Corporation.  

It marked Pearl Whites return to feature films and her first film for Fox Film Corporation.

==Cast==
*Pearl White - Rhoda, The White Moll
*Richard Travers - The Adventurer, The Pug (as Richard C. Travers)
*Jack Baston - The Dangler (J. Thornton Baston)
*Walter P. Lewis - The Sparrow (Walter Lewis)
*Eva Gordon - Gypsy Nan
*John Woodford - Father Michael
*George Pauncefort - Rhodas Father
*Charles Slattery - Detective Henry
*John P. Wade - The Rich Man
*William Harvey - Skinny
*Blanche Davenport- ?unconfirmed role

==References==
 

==External links==
 
*  
* 

 
 
 
 
 
 
 


 
 