Naalu Pennungal
{{Infobox film
| name           = Naalu Pennungal
| image          = Naalu Pennungal.jpg
| caption        = Screenshot of the movie
| director       = Adoor Gopalakrishnan
| producer       = Adoor Gopalakrishnan Benzy Martin
| writer         = Thakazhi Sivasankara Pillai Adoor Gopalakrishnan
| starring       = Padmapriya Nandita Das Kavya Madhavan Geetu Mohandas Manju Pillai
| music          = Isaac Thomas
| cinematography = M. J. Radhakrishnan
| editing        = Ajith
| distributor    = Emil & Eric Digital Films Pvt.Ltd.
| released       =  
| runtime        = 105 minutes
| country        = India
| language       = Malayalam
| budget         =
}}
Naalu Pennungal (Four Women, Malayalam: നാല് പെണ്ണുങ്ങള്‍) is a 2007 Malayalam film produced and directed by Adoor Gopalakrishnan based on four short stories written by Thakazhi Sivasankara Pillai. The film stars Padmapriya, Geethu Mohandas, Manju Pillai, and Nandita Das in the major roles, and KPAC Lalitha, Mukesh (actor)|Mukesh, Manoj K. Jayan, Sona Nair, Sreejith Ravi, Ravi Vallathol, Nandulal, Remya Nabeeshan, P. Sreekumar,  M. R. Gopakumar, and Kavya Madhavan in supporting roles. 

The movie chronicles a journey of womanhood across assorted backdrops with a classic amalgamation of source matters and techniques that splendidly spans times and frames. The movie has four distinct parts - each adapted from separate short stories by Thakazhi Sivasankara Pillai. Each of the parts narrate the stories of women from different strata of the society. Though the stories are not explicitly connected in narration, a pattern emerges in the flow of the movie - both in the chronological setting and the stature of the women.

Adoor Gopalakrishnan won the National Film Award for Best Direction for the film. 

==Plot==

Naalu Pennungal is the story of four women from Kuttanad in Alappuzha district in Kerala. The stories are set in the years between the 1940s to the 1960s.

The Prostitute

The first story profiled is the story of a street prostitute Kunjipennu (Padmapriya) and Pappukutty (Sreejith Ravi) who decide to start a life as husband and wife. They have bound themselves in matrimony that does not have any legal sanction. When the law catches up with them, they do not have anything that can be an evidence of their commitment towards each other. The story ends in the court scene where the helpless couple are punished for the crime of prostituition.

The Virgin

Kumari (Geethu Mohandas)- literally, a virgin girl- is a farm worker who shouldered the responsibility of running her household at a very early age. Her father (M. R. Gopakumar), having realized her advancing age, accepts a suitable marriage proposal for her. After the wedding, her husband, Narayanan (Nandhu), behaves strangely, as he evades any kind of contact with her including verbal conversation and sexual activity. The husbands mysterious behaviour is accentuated by his glut. After a few days, the couple make the customary visit to Kumaris house. Narayanan, leaves Kumari in her house after the visit and never returns to take her back. As days pass, rumours spread that she has been abandoned by him because of her infidelity. Her father unable to bear the shame of it, picks up a fight with the neighbour who had brought the marriage proposal. Kumari, who had maintained silence so far, emerges from the house and declares that the marriage never happened.

The Housewife

Chinnu Amma (Manju Pillai) is a childless housewife. She lives a fairly contented life with her loving husband (Murali (Malayalam actor)|Murali). Her husband works in a nearby town and she spends her time alone at home. One day, she is visited by Nara Pillai (Mukesh (Malayalam actor)|Mukesh), a former classmate. Nara Pillai had run away to Tamil Nadu, long ago and now visits his village rarely.  He is said to have made good in Tamil Nadu and speaks Malayalam with a heavy Tamil accent even when he is back in Kerala. Through their conversations we realize that they both had an amorous encounter in their childhood, from which Chinnu Amma escaped narrowly due to her fear of getting pregnant out of wed-lock. Nara Pillai assumes that she is vulnerable and attempts to talk her into bed, promising her a healthy off spring.Chinnu Ammas mind vacillates, but in the end of the movie she boldly declines the offer.

The Spinster

The last one, is about an upper middleclass girl (Nandita Das). She has a quiet life in a family composed of her widowed mother (Lalitha), an elder brother and two sisters. The story begins with a marriage proposal for her, which fails as the groom (Ravi Vallathol) prefers her younger sister (Kavya Madhavan). She silently witnesses the marriage. As years pass on, her elder brother (Ashokan (actor)|Ashokan) also gets married. As she walks towards middle age, her youngest sister (Remya Nambeesan) also gets married. Once the mother passes away, she is forced to move in with her younger sisters family. Things seem to go well initially, with her getting close to her nieces. Her sisters husband apologizes to her, for being the reason for her failed marriage proposal. But, soon problems surface as her sister gets jealous of her and starts imagining a non-existing affair with her husband and her. She goes back home alone, refusing to live with either her brother or the youngest sister. She finally has broken the shackles of others controlling her life and has decided to live on her own.

==Production==
The project to make films on Thakazhi Sivasankara Pillais stories came when Doordarshan in their Timeless Classics series wanted to compile works of writers in different languages who have produced classic literature. 
   This was the first film in the project and was followed by Oru Pennum Randaanum in the next year. Both the films have a four-chapter-structure based on independent short stories by Thakazhi Sivasankara Pillai. Both the films also share the same geographical and temporal setting.

Adoor selected the stories on which the movie is based, from the 300-400 stories that Pillai wrote. They were originally written over a long period of time. Thee director stated that, he chose these particular stories, because of their current relevance. He has claimed that unlike his previous films, this one is less complex. 

==Reception==
The film premiered in the masters section of the Toronto International Film Festival in September,2007. The films has so far been screened at more than twenty festivals     The Miami International Film Festival selected Adoor Gopalakrishnans Naalu Pennungal. Besides Adoor, Geethu Mohandas who featured in the story Kanyaka and Martin Sebastian, the co-producer of the film attended the film festival.

The film was also screened at the following festivals.
* 51st London film festival. 
* Vienna Film Festival. 
* Seattle International Film Festival. {{cite web|url=http://www.siff.net/festival/film/detail.aspx?id=27199&FID=64
|title=Films and Events- Naalu Pennungal|accessdate=2009-05-28|publisher= Seattle International Film Festival}} 

==Legacy== Marathi filmmaker Sachin Kundalkar said that his third film Gandha (film)|Gandha (Smell), an ensemble of three stories interconnected by the theme of smell, was inspired by Naalu Pennungal. 

==Notes==
 

==External links==
*  
*  

 

 
 
 
 
 