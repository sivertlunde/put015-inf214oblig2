Happy (2011 film)
 
{{Infobox film
| name           = Happy
| image          = Happy film.jpg
| caption        = Promotional poster
| director       = Roko Belic
| producer       = Tom Shadyac Frances Reid  Eiji Han Shimizu   Roko Belic
| writer         = Roko Belic
| music          = Mark Adler
| cinematography = Roko Belic   Adrian Belic
| editing        = Vivien Hillgrove
| studio         = Wadi Rum Productions
| released       =  
| runtime        = 73 minutes
| country        = United States
| language       = English
| budget         = $700,000
}}
Happy is a 2011 feature documentary film directed, written, and co-produced by Academy Award nominated film-maker Roko Belic.  It explores human happiness through interviews with people from all walks of life in 14 different countries, weaving in the newest findings of positive psychology.

Director Roko Belic was originally inspired to create the film after producer/director Tom Shadyac (Liar, Liar, Patch Adams, Bruce Almighty) showed him an article in the New York Times entitled "A New Measure of Well Being From a Happy Little Kingdom".     The article ranks the United States as the 23rd happiest country in the world.  Shadyac then suggested that Belic make a documentary about happiness.  Belic spent several years interviewing hundreds of people, ranging from leading happiness researchers to a rickshaw driver in Kolkatta, a family living in a cohousing community in Denmark, a woman who was run over by a truck, a Cajun fisherman, and more.

==Production==
Roko and his brother Adrian Belic shot the film on three Sony Z1U HDV video cameras.  They interviewed a number of top psychologists around the world, including Ed Diener, a professor of psychology at the University of Illinois, Richard Davidson, a professor at the University of Wisconsins Lab of Affective Neuroscience, and Sonja Lyubomirsky, professor at the University of California, Riverside and author of The How of Happiness.   

==Post-production== Blue Velvet, The Unbearable Lightness of Being—edited the film.  Belic received the majority of the budget from Tom Shadyac to complete principal photography and post-production.  The filmmakers then turned to crowdsource fundraising website Kickstarter.com to raise the finishing funds for the film.  The Kickstarter campaign succeeded in raising $36,000 out of a requested $33,000 in July 2010.   

==See also==
Project Happiness

==References==
 
 

==External links==
*  
*  

 
 
 
 
 
 
 
 