I Don't Want to Be Born
{{Infobox film
| name           = I Dont Want to Be Born
| image          = IDWTBB.jpg
| image_size     =
| caption        = UK release poster
| director       = Peter Sasdy
| producer       = Nato de Angeles
| writer         = Stanley Price
| starring       = Joan Collins Ralph Bates Eileen Atkins
| music          = Ron Grainer
| cinematography = Kenneth Talbot
| editing        = Keith Palmer
| distributor    = Rank Organisation
| released       = 1975
| runtime        = 95 minutes
| country        = United Kingdom English
}}

I Dont Want to Be Born (U.S. The Devil Within Her; also known as The Monster) is a 1975 British horror film, directed by Peter Sasdy and starring Joan Collins, Ralph Bates, Eileen Atkins and Donald Pleasence, which tapped into the 1970s fad for devil-child horror films.  The film was originally marketed as a straight-faced and serious product, and as such was comprehensively mauled by critics of the time.  However it later gained a reputation as a cult film favourite due to its perceived shortcomings, absurdities and unintentional camp comedy appeal.

== Plot ==
Lucy (Collins) is working as a dancer in a sleazy strip joint.  Her stage act includes a routine with a dwarf named Hercules (George Claydon).  One night after the show, she invites Hercules into her dressing-room for a drink.  He declines the drink but starts to rub Lucys neck and shoulders.  Lucy feels uncomfortable but tries to pretend nothing is happening, until Hercules makes a sudden lunge for her breasts, causing her to scream out in shock.  This alerts the stage manager Tommy (John Steiner), who rushes into the dressing-room and sends Hercules unceremoniously on his way, then proceeds to make love to Lucy.  Later as Lucy leaves the club she is confronted by the spurned and humiliated Hercules, who curses her with the words "You will have a baby...a monster! An evil monster conceived inside your womb! As big as I am small and possessed by the devil himself!"

Months pass and Lucy has left her stripping days behind, having moved up in the world via marriage to the wealthy Italian Gino Carlesi (Bates) and now comfortably settled in a grand Kensington townhouse.  Lucy goes into hospital to give birth to the baby she is expecting.  It proves to be a protracted, dangerous and painful delivery as the baby is a hefty 12-pounder.  The newborn infant is handed to Lucy, and seconds later she is sporting a slashed and bleeding cheek.  "He scratched me! With his sharp nails!" she exclaims in horror to her obstetrician Dr. Finch (Pleasence), who calmly explains that the baby must have been alarmed at being held too tightly.

Lucy and Gino bring the baby home and are welcomed by their efficient, no-nonsense housekeeper Mrs. Hyde (Hilary Mason).  Things get off to a bad start when Mrs. Hyde goes to chuck the babys chin, only to regret it.  "The little devil bit me!" she says as she displays her crushed finger.  She takes an instant dislike to the child, and is later rewarded with a dead mouse in her cup of tea.  Lucys attempts at maternal bonding are fraught with problems.  She is visited by her friend Mandy (Caroline Munro) and is voicing her concerns when they are interrupted by a series of crashings from upstairs.  To their horror, they find the baby in his cot but the nursery practically demolished.

Ginos sister Albana (Atkins), a nun, arrives from her convent in Italy to visit her new nephew.  Immediately aware that all is not well, she invites Gino to pray with her for the baby, which results in agonising screams from the nursery.  Dr. Finch is consulted, and agrees to carry out a series of tests.  Lucy meanwhile finds the burdens of motherhood too much to bear alone, and employs a nurse (Janet Key) to look after the baby.  After a near-miss when the nurses head is pulled underwater while bathing the baby, matters take a deadly turn when she takes him for a walk in the park.  Reaching out from his pram, he pushes her with such force that she falls, cracks her head on a lakeside rock, falls unconscious into the water and is drowned.

Lucy pays a visit Tommy at the strip club.  She intimates that, given the timing of the birth, there is a chance the baby could be his.  "Just ’cause you’ve got some freaky offspring you wanna pin it on me?" he asks.  However his curiosity is aroused and he asks to see the "spooky kid".  Once at the house he leans over to peer into the babys cot, only to reel back with a smashed and bloody nose for his trouble.  This temporarily pushes the baby up in Lucys estimation and she gazes lovingly at him, until the face in the cot turns into that of Hercules.

One evening Gino plans a romantic night-in to take Lucys mind off her woes.  At the end of a successful evening, he goes to check on the baby, only to find the nursery empty, the window open and odd noises coming from the garden.  Going out to investigate, he looks up into a tree, whereupon a noose is thrust around his neck and he is hauled into the air and hanged.  His body is stuffed down a drain.  The following day Lucy criss-crosses London in a frenzy trying to find her missing husband.  Dr. Finch is summoned and pays an evening call to check on the baby and the distraught Lucy.  After administering a powerful sedative to Lucy, he too hears strange noises.  He unwisely steps out into the garden, and is decapitated with a spade(while still standing up).  The trail of death continues as Lucy stumbles through the house in a groggy haze, pleading for her life to no avail as she is stabbed through the heart with a pair of scissors.

Finally galvanised into action, Albana decides that she must perform an exorcism on the baby.  Brandishing a crucifix at him and incanting in Latin, she bravely persists as the room shakes and the baby tears at her vestments.  Meanwhile at the strip club Hercules is on stage, and begins to stagger around in pain.  Albana finally touches the crucifix to the babys head, and his demons are cast out at the same time as Hercules falls over dead in front of a stunned audience.

== Cast ==
 
 
* Joan Collins as Lucy Carlesi
* Ralph Bates as Gino Carlesi
* Eileen Atkins as Sister Albana
* Donald Pleasence as Dr. Finch
* Hilary Mason as Mrs. Hyde
* Caroline Munro as Mandy Gregory
* John Steiner as Tommy Morris
 
* George Claydon as Hercules
* Janet Key as Jill Fletcher
* Derek Benfield as Police Inspector
* Stanley Lebor as Police Sergeant
* Judy Buxton as Sheila
* Andrew Secombe as Delivery Boy
* Floella Benjamin as Nurse
 

== Reputation == The Exorcist...and Hammer and Swinging London horrors. Give it a wide berth."   An American reviewer found the film "sensational and vulgar" and "an unholy cross" between The Exorcist and Rosemarys Baby (film)|Rosemarys Baby, adding "why decent actors like Eileen Atkins and Donald Pleasence allowed themselves to be corralled into a mess like this is enigmatic." 

From the 1980s, the film became a regular feature on late-night TV in the UK and U.S., attracting ironic appreciation from connoisseurs of the "Cult film#So-bad-theyre-good cult films and camp classics|so-bad-its-good" school of films.  The factors which were originally ridiculed now became selling points to this audience, including the incoherent, implausible and ill-explained plot; wild overacting and a general air of absurd hysteria; laughably overwrought, clumsy and stilted dialogue, the bizarre accents adopted by Bates and Atkins; the inclusion of a lengthy, completely gratuitous and archetypally-1970s sex scene between Collins and Bates, and the spectacle of several well-respected actors of the time making fools of themselves.  The tongue-in-cheek British Horror Films website sums up the films appeal as: "There are some films that just defy description.  I Dont Want to Be Born ranks very highly among them.  The film is...an out-and-out classic.  And it was apparently made completely straight-faced.  Quite how unbelievable that statement is can only be appreciated when the film is watched."

== References ==
 

== External links ==
*  
*  
*   at British Horror Films
*   at Cool Cinema Trash

 

 
 
 
 
 
 
 
 