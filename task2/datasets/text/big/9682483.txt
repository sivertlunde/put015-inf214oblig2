The Offence
 
 
{{Infobox film
| name = The Offence
| image =Poster of the movie The Offence.jpg
| caption =
| director = Sidney Lumet
| producer = Denis ODell	 John Hopkins
| starring =Sean Connery Trevor Howard Vivien Merchant Ian Bannen Peter Bowles Derek Newark Ronald Radd
| music =Harrison Birtwistle
| cinematography = Gerry Fisher
| editing = John Victor-Smith
| country = UK
| distributor = United Artists (theatrical release)
| released = 1973 Christopher Bray: Sean Connery: The Measure of a Man, Faber and Faber, London 2010, ISBN 9780-571-23807-1, p. 174–180. 
| runtime = 112 min.
| language = English
| budget = $900,000 
}}
 John Hopkins. It stars Sean Connery as police detective Johnson, who kills suspected child molester Kenneth Baxter (Ian Bannen) while interrogating him. The film explores Johnsons varied, often aggressive attempts at rationalizing what he did, revealing his true motives for killing the suspect in a series of flashbacks. Trevor Howard and Vivien Merchant appear in major supporting roles.

==Plot summary==
Detective-Sergeant Johnson (Connery) has been a police officer for 20 years, and is deeply affected by the murders, rapes, and other violent crimes he has investigated.

His anger surfaces while interrogating Kenneth Baxter (Bannen), who is suspected of raping a young girl; by the end of the interrogation, Johnson has beaten him to death. Johnson is suspended and returns home for the night, and gets into a violent argument with his wife, Maureen (Vivien Merchant). 
 flashbacks show the events during the night when Johnson killed Baxter. 

The flashbacks portray Baxter &mdash; whose guilt or innocence is left ambiguous &mdash; taunting Johnson during the interrogation, insinuating that he secretly wants to commit the sort of sex crimes he investigates. Johnson at first flies into a rage and strikes Baxter, but he eventually admits that he does indeed harbour obsessive fantasies of murder and rape, and that he is losing his mind under the strain. He then tearfully begs Baxter to help him. When Baxter recoils from him in disgust, Johnson snaps and kills him. 

The film ends with another flashback, this time of Johnson attacking the police officers who pulled him off Baxter, and muttering, "God...my God..." as he realizes what he has done.

==Cast==
*Sean Connery as Detective Sergeant Johnson
*Trevor Howard as Lieutenant Cartwright, Detective Superintendent
*Vivien Merchant as Maureen Johnson
*Ian Bannen as Kenneth Baxter
*Peter Bowles as Detective Inspector Cameron
*Derek Newark as Frank Jessard
*Ronald Radd as Lawson
*John Hallam as Panton Richard Moore as Garrett
*Anthony Sagar as Hill
*Maxine Gordon as Janie Edmonds, The Raped Girl
*Hilda Fenemore as Woman on the common  
*Rhoda Lewis as Woman at the school  
*Cynthia Lund as Child at the school  
*Howard Goorney as Lambert

==Background== Diamonds Are Bob Simmons, who had designed similar action scenes for the Bond films.  It is the composer Sir Harrison Birtwistles only film score.

United Artists finally released The Offence in 1973. It was a commercial failure and did not yield a profit for nine years,  even going unreleased in several markets, including France, where it did not premiere until 2007. United Artists pulled out of the deal and the next project, a film version of Macbeth that Connery was to direct, was scotched by Roman Polanskis Macbeth (1971 film)|adaptation.

==Reception==
 

 

 "The Offence is an important film for study because its release coincides with the rediscovery of family violence and child sexual abuse in the 1970s." 
—Adrian Schober, The Journal of Popular Film and Television 

==DVD releases==
In 2004, MGM UK released a DVD of the film which contained no extras or trailers. Simultaneous releases from MGM were made in other PAL format countries, such as Germany and Australia. On 20 October 2008, the film was again released on DVD in the UK by Optimum Releasing, again without extras or trailers. A French Region 2 DVD, preserving the films original ratio of 1:1.66, became available in 2009. In April 2010, MGM put the film out on a U.S. DVD-R "on demand" for the first time. It is available as an exclusive from Amazon.com and contains no extras.

== References ==
 

==External links==
*  
*   for The Offence

 
 

 
 
 
 
 
 
 
 

 