Kids Don't Tell
{{Infobox film
| name = Kids Dont Tell
| image = Jobethscreenshot.jpg
| caption = JoBeth Williams & Michael Ontkean in a scene from Kids Dont Tell
| director = Sam OSteen
| writer   = Maurice Hurley
| starring = Michael Ontkean JoBeth Williams Ari Meyers Leo Rossi Joaquin Phoenix
| cinematography = Michael D. Margulies
| editing = Randy Roberts
| producer = Barry Greenfield
| music = Fred Karlin
| distributor = CBS Entertainment Productions
| released =  
| runtime = 101 minutes
| language = English
}} Postcards from the Edge), was broadcast on March 5, 1985. 

==Plot==
Driven by a duty to his young daughter (Ari Meyers), filmmaker John Ryan (Michael Ontkean) agrees to produce a documentary on the sexual abuse of children in the American status quo.  However, his loving wife Claudia (JoBeth Williams) becomes increasingly despondent and troubled as the filmmaker immerses himself further into the project.  Ryan obtains participation from a host of experts in the field, including a Los Angeles police detective (Leo Rossi), who provides powerful insight into how the legal system fails, and a habitual molester (Jordan Charney) who tells of his technique for choosing and assaulting his victims. 

==Cast==
*Michael Ontkean ...  John Ryan 
*JoBeth Williams ...  Claudia Ryan 
*Leo Rossi ...  Detective Rastelli 
*Ari Meyers ...  Nicky Ryan 
*John Sanderford ...  Eli Davis 
*Jordan Charney ...  Tatum 
*Joaquin Phoenix ...  Frankie
*Robin Gammell ...  Dr. Houghton 
*Shelley Morrison ...  Carol 
*Jean Bruce Scott ...  Clare 
*Matthew Faison ...  Evan Harris 
*David S. Aaron ...  Waiter 
*Roger Askin ...  Man at Wedding 
*Judith Barsi ...  Jennifer Ryan 
*Gary Bayer ...  Speaker at Meeting 
*Earl Billings ...  Terry 
*Dennis Bowen ...  Ted 
*Sally Brown ...  Jill 
*Victor Campos ...  Dale 
*Diane Cary ...  Sandra Luce (as Diane Civita) 
*E.M. Fredric ...  Cocktail Waitress 
*Kristin Gamboa ...  Linda 
*Mari Gorman ...  Macy 
*Nancy Lee Grahn ...  Puppet Lady 
*Natalie Gregory ...  Krista Mueller
*David Kaufman ...  Kenny 
*Charles Lanyer ...  Scotty 
*Michael Laskin ...  Man at Park 
*Christopher Lofton ...  Charles 
*Oceana Marr ...  Mildred  Bob Minor ...  Pool Player 
*Milton Murrill ...  Detective Timmy 
*John Napierala ...  David 
*David Lloyd Nelson ...  Lyle 
*Greg Nourse ...  Bartender 
*Steve Pershing ...  Officer at Desk (as Stephen Pershing) 
*Branscombe Richmond ...  Pool Player 
*Jack Thibeau ...  Donny 
*Cori Wellins ...  Lisa 
*Jaleel White ...  Christofer 
*Barbra Rae ... Daphne

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 