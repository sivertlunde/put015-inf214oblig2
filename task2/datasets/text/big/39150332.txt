Lullaby (2014 film)
{{Infobox film
| name           = Lullaby
| image          = Lullaby2014Poster.jpg
| border         = yes
| caption        = Promotional poster
| director       = Andrew Levitas
| producer       = Cary Brokaw
| writer         = Andrew Levitas
| starring       = {{Plainlist|
* Amy Adams
* Garrett Hedlund
* Jessica Brown Findlay
* Terrence Howard
* Richard Jenkins
* Daniel Sunjata
* Jennifer Hudson
* Anne Archer}}
| music          =
| cinematography = Florian Ballhaus
| editing        = Julie Monroe
| studio         = {{Plainlist|
* Avenue Pictures 
* Ananta Productions 
* Metalwork Pictures 
* Media House Capital 
* Lullaby New York}}
| distributor    = ARC Entertainment
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
| gross          =
}}

Lullaby is a 2014 American drama film written and directed by Andrew Levitas, and starring Amy Adams, Garrett Hedlund, Jessica Brown Findlay, Terrence Howard, Richard Jenkins, Daniel Sunjata, Jennifer Hudson, and Anne Archer.       The movie explores all the right to die issues of a cancer stricken father who has decided to stop all medication and turn off the life support machines.  One sees how the Jewish family members respond to each other and how the father says goodbye to his wife, son, and daughter.  

==Cast==
 
* Amy Adams as Emily
* Garrett Hedlund as Jonathan
* Jessica Brown Findlay as Karen
* Terrence Howard as Dr. Crier
* Richard Jenkins as Robert
* Daniel Sunjata as Officer Ramirez
* Jennifer Hudson as Nurse Carrie
* Anne Archer as Rachel
* Frankie Shaw as Janice
* Darren Le Gallo as Ethan
* Jessica Barden as Meredith
* Maddie Corman as Beth
* Anne Vyalitsyna as Brooke
* Sterling Jerins as Young Karen
* Robert Bogue as Steven Lavipour
* Zac Ballard as Nicholas
 

==Production==
On February 6, 2014, ARC Entertainment announced that they have acquired all the North American distribution rights to the film. 

==References==
 

==External links==
*  

 
 
 

 