The Last Fort
{{Infobox film
| name           = The Last Fort
| image          = 
| image_size     = 
| caption        = 
| director       = Curtis Bernhardt Gustav Schwab Hans Wilhelm
| narrator       = 
| starring       = Rolla Norman   Maria Paudler   Albert Steinrück   Fritz Odemar
| music          = 
| editing        =
| cinematography = Artur von Schwertführer   Fritz Arno Wagner
| studio         = Nero Film
| distributor    = 
| released       = 1928
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
|  silent war film  directed by Curtis Bernhardt and starring Rolla Norman, Maria Paudler and Albert Steinrück.

==Cast==
* Rolla Norman as Major Leblanc
* Maria Paudler as Yvonne, seine Tochter
* Albert Steinrück as Lensky, Kommandant
* Fritz Odemar as Lieutenant Brand
* Heinrich George as Croff
* Alexander Granach as Gestino
* Alfred Gördel as Capitän Andrieux

==References==
 

==Bibliography==
* Koepnick, Lutz Peter. The Dark Mirror: German Cinema Between Hitler and Hollywood. University of California Press, 2002.
* Prawer, S.S. Between Two Worlds: The Jewish Presence in German and Austrian Film, 1910-1933. Berghahn Books, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 