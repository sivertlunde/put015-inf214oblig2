The Maze (1953 film)
{{Infobox film
| name           = The Maze
| image          = TheMaze1953.jpg
| director       = William Cameron Menzies
| producer       = Richard Heermance  (producer) Victor Heerman  (executive producer)
| writer         = Daniel Ullman  (screenplay)  Maurice Sandoz  (short story)
| music          = Marlin Skiles Richard Carlson Veronica Hurst Katherine Emery
| cinematography = Harry Neumann
| editing        = John Fuller Allied Artists Pictures Corporation
| released       = 26 July 1953 
| runtime        =  80 minutes
| country        = United States
| language       = English
}}
 1953 atmospheric Richard Carlson and Hillary Brooke. It was directed by William Cameron Menzies and distributed by Allied Artists Pictures. This was to be the second 3-D film designed and directed by William Cameron Menzies, who was known as a director with a very "dimensional" style (e.g. many shots are focused in layers). This would be his final film as production designer and director.

==Plot==
 Richard Carlson) abruptly breaks off his engagement to pretty Kitty (Veronica Hurst) after receiving word of his uncles death.  He inherits a mysterious castle in the Scottish highlands and moves there to live with the castle servants. Kitty refuses to accept the broken engagement and travels with her aunt (Katherine Emery) to the castle.  When they arrive, they discover that Gerald has suddenly aged and his manner has changed significantly.

After a series of mysterious events occur in both the castle and the hedge maze outside, they invite a group of friends, including a doctor, to the castle in the hopes that they can help Gerald with whatever ails him.  Although the friends are equally concerned by Geralds behavior, they are at a loss to its cause.  One night, Kitty and her aunt steal a key to their bedroom door (which is always locked from the outside) and sneak out into the mysterious maze.  There they discover Gerald and his servants tending to a frog-like monster.  The monster panics upon seeing the strangers and runs back to the castle, hurling itself from a top-story balcony.

At the end, Gerald explains that the amphibious creature was the actual master of the castle and that he and his ancestors were merely its servants.  The death of the creature releases him from his obligation and he is able to return to a normal life.

==Cast== Richard Carlson as Gerald MacTeam
*Veronica Hurst as Kitty Murray
*Katherine Emery as Edith Murray
*Michael Pate as William John Dodsworth as Dr. Bert Dilling
*Hillary Brooke as Peggy Lord
*Stanley Fraser as Robert
*Lilian Bond as Margaret Dilling (as Lillian Bond)
*Owen McGiveney as Simon
*Robin Hughes as Richard Roblar

 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 


 
 