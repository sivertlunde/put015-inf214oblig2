Not Guilty (1919 film)
{{Infobox film
| name           = Not Guilty 
| image          =
| caption        =
| director       = Arrigo Bocchi
| producer       = 
| writer         =  Kenelm Foss  Charles Vane   Hayford Hobbs
| music          =
| cinematography = Randal Terraneau
| editing        = 
| studio         = Windsor Films 
| distributor    = Walturdaw
| released       = March 1919
| runtime        = 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent comedy comedy crime Charles Vane and Hayford Hobbs.  A barrister takes on his own case when he has to defend himself in court for tricking a millionaire out of money for a good cause.

==Cast==
*  Kenelm Foss as Sir Graham Carfax   Charles Vane as Andrew McTaggart  
* Hayford Hobbs as Donald McTaggart  
* Olive Atkinson as Minnie Day  
* Barbara Everest as Hetty Challis  
* Bert Wynne as Tom Dent  
* Evelyn Harding as Matron  
* Philip Hewland as Dillingham  

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918-1929. George Allen & Unwin, 1971.

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 

 