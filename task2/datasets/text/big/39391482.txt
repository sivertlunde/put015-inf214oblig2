Ang Kwento Ni Mabuti
{{Infobox film
| name = Ang Kwento Ni Mabuti
| image = File:Ang Kwento ni Mabuti poster.jpg
| caption = Official Poster
| director = Mes De Guzman
| starring = Nora Aunor Sue Prado Mara Lopez Arnold Reyes
| line producers = Rhea De Guzman
| writer = Mes De Guzman
| cinematography = Albert Banzon
| art director = Cesar Hernando
| editing =
| studio = Cinelarga Productions
| distributor = Cinelarga Productions
| released =  
| country = Philippines
| language = Ilokano language|Ilocano, English (subtitle)
| budget   =
| gross =
}} Filipino drama film and the official entry to the first CineFilipino Film Festival. Not to be confused with the Filipino literary classic by Genoveva Edroza-Matute, De Guzman’s film is actually a morality tale about Mabuti, a faith healer who maintains a positive outlook in life despite her poverty. But that was until she found herself in a moral dilemma after accidentally finding a bag containing P5 million in cash that could bring an end to her family’s problems. 

The film was the first collaboration of the award winning actress Nora Aunor and the award winning director Mes de Guzman. The film was shot entirely in Aritao, Nueva Viscaya. The dialogue of the film is entirely in Ilokano language|Ilocano, to give the film its authenticity with English subtitles. 

The film was also top-grosser of the first CineFilipino Film Festival. 

==Synopsis==

Mabuti de la Cruz, is a 58 year old healer who lives in poverty-stricken Sitio Kasinggan in Nueva Vizcaya with her surly mother, Guyang (Josephina Estabillo), luckless son Ompong (Arnold Reyes), loveless daughter Angge (Mara Lopez), and her four fatherless granddaughters.   Although she came from the impoverished family, Mabuti still look up life in a positive way. Life is hard, but that doesn’t stop the cash-strapped but cheerful grandmother from basking in the simple pleasures of barrio life. Life was always simple for Mabuti until an unexpected twist changed her perception and found herself in a moral dilemma.

One day, Mabuti received a letter demanding payment for an overdue loan compels her to take the five-hour trip to the big city to ask for financial reprieve. Mabuti needed to mortgage their land when Ompong was going abroad to work. On her way back home, she met a friendly stranger named Nelia on the bus who entrusts a bag to her and runs off, only to be killed by the military.   Mabuti was so confused of what happened but kept the bag that was entrusted to her. On the way home, she decided to take a peek on what was inside of the bag and to her surprised she saw bundles of money, that she has never seen before. Mabuti suffers a crisis of conscience after she comes upon a bag of money—millions of pesos, at that. She is torn between surrendering it to the authorities and using it to solve the problems that her family faces. 

==Cast==
* Nora Aunor as Mabuti de la Cruz
* Sue Prado as Nelia
* Mara Lopez as Lucia de la Cruz
* Arnold Reyes as Ompong de la Cruz.
* Ama Quiambao
* Josephina Estabillo as Guyang

==Review==
*in Ang Kwento Ni Mabuti that the actress redeems her stature and the command of her vast talent. The performance is shorn of affectation and is nearly flawless, from the subtle movement of neck and eye to wailing by her mothers death bed. At this point, melodrama flirts with melancholy, tragedy with the realism of soap opera, an uncanny liaison that takes us to the most fraught of ties, the most alienating of emotions, and an emergent tone and terrain of affection. Mabuti succeeds at opening up spaces to ponder the fraught issues of faith and responsibility well-served by the restrained performances of its supporting cast. 
- Jojo Devera, Film Critic 

*You don’t want to miss the CineFilipino entry, “Ang Kwento ni Mabuti,” if only for Nora Aunor, whose peerless, award-worthy portrayal and magnetic presence give director Mes de Guzman’s visually and thematically compelling drama a spare but forceful elegance—and its reason for being! - Rito P. Asilo, Philippine Daily Inquirer, Sept 21, 2013

*Director Mes de Guzman has succeeded to create a charming fairy tale with modern-day characters and situations. There is a whimsical element about the interplay of harsh realities of live with fate and destiny that will make the audience smile. - Fred Hawson, Film blogger

==List of Festival Competed or Exhibited==
* 1st CineFilipino film Festival, Manila, Philippines 
* The 20th Festival International des Cinémas dAsie | Festival du Film Asiatique de Vesoul "Cinemas dAsie" (11-18 Feb 2014), France 
* 6Th Cinema Rehiyon, Cagayan de Oro, Philippines (February 18–22, 2014)s 

==Awards and Recognition==

{| width="90%" class="wikitable sortable"
|-
! colspan="4" align="center" | 1st Cine-Filipino Film Festival
|-
! width="10%"| Year
! width="35%"| Category
! width="40%"| Nominee
! width="15%"| Result
|-
| rowspan = 3 align="center"| 2013
| align="center"| Best Picture
| align="center"|
|  
|-
| align="center"| Best Director
| rowspan = 2 align="center"| Mes de Guzman
|  
|-
| align="center"| Best Screenplay
|  
|-
|}

{| width="90%" class="wikitable sortable"
|-
! colspan="4" align="center" | Young Critics Circle
|-
! width="10%"| Year
! width="35%"| Category
! width="40%"| Nominee
! width="15%"| Result
|- 2013
| align="center"| Best film
| align="center"| Mes de Guzman
|  
|-
| align="center"| Best Performer
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Screenplay
| align="center"| Mes de Guzman
|  
|-
| align="center"| Best in Cinematography and Visual Design
| align="center"| Albert Banzon (cinematography), Cesar Hernando and Mes de Guzman (production design)
| 
|-
|}
{| width="90%" class="wikitable sortable"
|-
! colspan="4" align="center" | 15th Gawad Pasado
|-
! width="10%"| Year
! width="35%"| Category
! width="40%"| Nominee
! width="15%"| Result
|-
| rowspan="4" | 2014 
| align="center"| Pinakapasadong Pelikula 
| align="center"| 
|  
|-
| align="center"| Pinakapasadong Aktres
| align="center"| Nora Aunor
|  
|-
| align="center"| Pinakapasadong Istorya
| align="center"| Mes de Guzman
|  
|-
| align="center"| Pinakapasadong Sinematograpiya
| align="center"| Albert Banson
|  
|-
|}

{| width="90%" class="wikitable sortable"
|-
! colspan="4" align="center" | 37th Gawad Urian
|-
! width="10%"| Year
! width="35%"| Category
! width="40%"| Nominee
! width="15%"| Result
|-
| rowspan="4" | 2014 
| align="center"| Best Picture
| align="center"| 
|  
|-
| align="center"| Best Director
| align="center"| Mes de Guzman
|  
|-
| align="center"| Best Actress
| align="center"| Nora Aunor
|  
|-
| align="center"| Best Sound
| align="center"|
|  
|-
|}

==References==
 

 
 
 