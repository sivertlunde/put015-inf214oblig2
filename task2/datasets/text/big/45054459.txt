Monsieur (1990 film)
{{Infobox film
| name           = Monsieur
| image          = 
| caption        =
| director       = Jean-Philippe Toussaint
| producer       = {{plainlist|
* Pascal Judelewicz
* Anne-Dominique Toussaint
}}
| screenplay     = Jean-Philippe Toussaint
| based on       =  
| starring       = {{plainlist|
* Dominic Gould
* Wojciech Pszoniak
* Eva Ionesco
}}
| music          = Olivier Lartigue
| cinematography = Jean-Francois Robin
| editing        = Sylvie Pontoizeau
| studio         = 
| distributor    = 
| released       =  
| runtime        = 90 minutes
| country        = Belgium France
| language       = French
| budget         = 
| gross          = 
}}
Monsieur is a 1990 comedy film based on the 1986 novel of the same name by Jean-Philippe Toussaint. It was directed by the novels author and produced by Pascal Judelewicz and Anne-Dominique Toussaint. The film starred Dominic Gould, Wojciech Pszoniak, and Eva Ionesco. Monsieur was screened at the 1990 Toronto International Film Festival. It received the André Cavens Award for Best Film given by the Belgian Film Critics Association (UCC). 

== Cast ==
* Dominic Gould as Monsieur
* Wojciech Pszoniak as Kaltz
* Eva Ionesco as Mrs. Pons-Romanov
* Aziliz Juhel as Anne Bruckhardt
* Jacques Lippe as Parrain
* Jany de Stoppani as Mrs. Parrain
* Alexandra Stewart as Mrs. Dubois-Lacour
* Alexandre von Sivers as Leguen
* Aurelle Doazan as Laurence

== References ==
 

== External links ==
*  

 

 
 
 
 
 
 
 

 
 