Nabi (film)
 
{{Infobox film
| name           = Nabi
| image          = Nabi film poster.jpg
| caption        = Theatrical poster
| director       = Moon Seung-wook
| producer       = Park Ji-yeong Kim Chang-hyo
| writer         = Moon Seung-wook Jeong Hye-ryeon
| starring       = Kim Ho-jung Kang Hye-jung Jang Hyun-sung
| music          = Jeong Hoon-young
| cinematography = Kwon Hyuk-jun
| editing        = Kim Deok-yeong Lee Jang-uk
| distributor    = Buena Vista International
| released       =  
| runtime        = 116 minutes
| country        = South Korea
| language       = Korean
| budget         = United States dollar|US$380,000
| gross          = 
| film name      = {{Film name
 | hangul         =  
 | rr             = Nabi
 | mr             = Nabi}}
}} 2001 South Korean film. Directed by Moon Seung-wook, Nabi was shot on digital video and transferred onto 35mm film, filmed on a low budget of United States dollar|$380,000. It marked the feature film debut of Kang Hye-jung, who won Best Actress at the 5th Puchon International Fantastic Film Festival for her role as Yuki. Nabi also starred Kim Ho-jung, who won the Bronze Leopard for Best Actress at the 54th Locarno International Film Festival.
 German woman of Korean descent, seeks the virus in order to erase painful memories and, along the way, develops a close bond with her driver and her teenage guide.

== Plot ==
The film is set in an unnamed Korean city of the near future, a city plagued with acid rain, lead poisoning, and the "oblivion virus". People come from all over the world on guided tours of the city deliberately seeking the virus. Victims of lead poisoning are quarantined in sanatoriums for the protection of tourists, and forced abortions are carried out to prevent the births of deformed babies.

Anna Kim (Kim Ho-jung) is one such tourist who arrives in the city having booked a tour with the Butterfly Travel Agency. Anna wishes to become infected with the virus in order to forget the painful memories of her abortion. Her "virus guide", Yuki (Kang Hye-jung), is seven months pregnant and unwell, but needs the money to support herself and her unborn child. K (Jang Hyun-sung), their driver, is new to the agency and starting his first assignment. An orphan with no memory of his real family, he keeps an old photograph of himself as a child on the dashboard of his taxi; he picks up other passengers in the hopes that someone will recognize him, even though this is against agency policy.

Ater meeting Yuki and K at the airport, Anna is taken straight to a number of virus exposure sites, but their early attempts at finding the virus are cut short by acid rain storms. Finding out about Yuki’s pregnancy and poor health, Anna requests a new guide. K, suspicious of Yuki’s behaviour, reports her to the authorities as a suspected lead poisoning victim. Nevertheless, after Yuki treats Anna for exposure to acid rain, a bond starts to develop between them, and the three spend some time together. Anna cooks a meal for Yuki, and in return Yuki reveals her collection of personal items from previous clients, memories she is safeguarding should those people ever wish to remember their past again. After Anna leaves, Yuki is taken away by the city authorities.

Continuing without Yuki as a guide, K continues to drive Anna around the city. At first Anna is frustrated by Ks efforts to learn about his own past, but they begin to understand each other as they spend more time together. One night, they are involved in a road accident and their taxi veers over the side of a bridge; Anna rescues K from the water below and manages to resuscitate him, though she later requires treatment herself.
 water breaks. Knowing that she will not survive the birth, Anna urges Yuki to put her own health first, though Yuki is adamant that she will have the baby. Unable to reach the hospital in time, Anna fulfils Yuki’s wish to have a water birth, and, assisted by K, takes her down to the beach where she delivers the baby in the sea. Yuki later dies in hospital.

As Anna and K search through Yuki’s belongings, they discover an old passport with Anna’s photo in it, suggesting that this is not the first time she has been to the city in search of the virus. Having no memory of such a visit, Anna goes to the Butterfly Travel Agency headquarters where she demands answers, but as she has signed a waiver they refuse to divulge any information. Later, she tells K how she wanted to adopt Yuki’s son in order to make a fresh start, but that she knows he needs the child more than she does.

Three years later, K is still working as a driver for the agency. No longer searching for answers to his past, he now keeps a photograph of Anna in his taxi, along with one of him and his adopted son.

== Production, style and themes ==
Produced on a budget of United States dollar|$380,000,    Nabi was shot on digital video and later transferred to 35mm film.  Filming took place on location in Busan, South Korea, as well as Kobe and Osaka, Japan.   
 aqua colours.]] Tarkovsky and Krzysztof Kieślowski|Kieślowski, the latter a former teacher of Moons.  Water is used throughout the film in various forms—rain, showers, swimming pools, and the ocean—resulting in a continuous stream of blue and Aqua (color)|aqua-tinted images, described by Shelly Kracier of Senses of Cinema as being "subtly shaded, eerily translucent, and suffocatingly dreamlike...   an underwater world in which the characters seem suspended, floating in voids of their own, unmoored by their own particular estrangements with their pasts". 

The premise of Nabi has been compared to Song Il-gons Flower Island, another South Korean film released in 2001. Both films feature three central characters, each with differing goals but sharing an emotional pain that allows them to develop a strong bond, and both films use a plot device—Nabis "oblivion virus" and the magical Flower Island—promising take away painful memories.  Lisa Roogen-Runge of Senses of Cinema found the "oblivion virus" similar to the magic wine used by Wong Kar-wai in his 1994 film Ashes of Time, and also noted the physical scars bore by each of the three main characters in Nabi, in each case connected to their childhood or relationship with children. 

== Release ==
Nabi was released theatrically in South Korea on 13 October 2001, and received a total of 5,700 admissions in Seoul.  It was also screened at the following film festivals:

* 5th Puchon International Fantastic Film Festival (12–20 July 2001)   
* 54th Locarno International Film Festival (2–12 August 2001) 
* 26th Toronto International Film Festival (6–15 September 2001) 
* 20th Vancouver International Film Festival (27 September–12 October 2001) 
* 14th Tokyo International Film Festival (27 October–4 November 2001) 
* 45th London International Film Festival (7–22 November 2001) 
* 2nd Asiatica Film Mediale (28 November–9 December 2001) 
* 20th San Francisco International Asian American Film Festival (7–17 March 2002) 
* 12th Festival Black Movie (15–24 March 2002) 
* 11th Brisbane International Film Festival (9–21 July 2002) 
* 16th Wine Country International Film Festival (18 July–11 August 2002)   
* 51st Melbourne International Film Festival (23 July–11 August 2002) 
* 4th Silver Lake Film Festival (10–18 September 2003)  Era New Horizons International Film Festival (22 July–1 August 2004) 

Nabi was released on DVD in South Korea on 7 September 2002. 

== Critical reception ==
 , held in 2001.]]
In a report of the 26th Toronto International Film Festival for   found the film to be "worthy of cult status... properly weird and dripping with atmosphere", and despite being "sometimes sluggish", it was nevertheless "typical of the inventiveness in genre filmmaking that has characterized recent Korean filmmaking."    Darcy Paquet of Koreanfilm.org praised both the director and two lead actresses, describing Nabi as a "beautiful and strange film   offers its viewers a memorable experience". 

Lisa Roosen-Runge was less glowing in her report of the 20th Vancouver International Film Festival for Senses of Cinema, saying, "I admit that the revelation about Annas expired passport did not make any sense to me, and I was confused by the extra non-Yuki character played by the same actress. I also did not find the epilogue completely necessary. Still,   is recommended as an unusual film despite the minor caveats."    Comments made in the San Francisco Bay Guardian were somewhat critical of Moon Seung-wooks direction, saying, "some of his favored technical tactics (in particular, camera work that lurches during moments of emotional upheaval) become heavy-handed", though Kim Ho-jungs performance was singled out as the films "chief strength". 

== Awards and nominations ==
{| class="wikitable"
|-
! Year !! Awards group !! Award category—Recipient !! Result !! Ref.
|-
| rowspan=3 | 2001
| 5th Puchon International Fantastic Film Festival
| Best Actress—Kang Hye-jung
| style="background: #ddffdd" | Won
| 
|-
| rowspan=2 | 54th Locarno International Film Festival
| Bronze Leopard for Best Actress—Kim Ho-jung
| rowspan=2 style="background: #ddffdd" | Won
| 
|-
| Young Critics Award
| 
|-
| rowspan=3 | 2002
| rowspan=3 | 16th Wine Country International Film Festival
| Best Sound Design
| rowspan=3 style="background: #ddffdd" | Won
| rowspan=3 | 
|-
| Best Cinematography
|-
| Gaia Film Award for Environmental Awareness
|}

== References ==
 

== External links ==
*   at the Korean Movie Database
*  
*  

 
 
 
 
 
 