Gharsallah, la semence de Dieu
{{Multiple issues|
 
 
}}

{{Infobox film
| name           = Gharsallah, la semence de Dieu
| image          = 
| caption        = 
| director       = Kamel Laaridhi
| producer       = Gsara
| writer         = 
| starring       = 
| distributor    = 
| released       = 2007
| runtime        = 56 minutes
| country        = Belgium Tunisia
| language       = 
| budget         = 
| gross          = 
| screenplay     = Kamel Laaridhi
| cinematography = Kamel Laaridhi
| editing        = Nadia Touijer
| music          = 
}}

Gharsallah, la semence de Dieu is a 2007 documentary film.

== Synopsis ==
At the beginning of the 19th century, a man called Gharsallah dies and is buried in a mausoleum in the village of Dhibet, in the center of Tunisia. The film tries to evoke how this Gharsallah affected other lives: Was he Saint? A madman? Possessed? Unjust? The story of a man that marked everything around him, even dreams.

== References ==
 

 
 
 
 
 
 


 
 
 