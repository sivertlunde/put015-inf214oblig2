The Devils (film)
 
 
 
{{Infobox film
| name           = The Devils
| image          = Thedevils1971poster.png
| image_size     =
| alt            =
| caption        = Official 1971 poster promoting R-rated cut of the film as it was released in Australia
| director       = Ken Russell
| producer       = Ken Russell Robert H. Solo
| screenplay     = Ken Russell
| based on       =    
| starring       = Oliver Reed Vanessa Redgrave
| music          = Peter Maxwell Davies David Watkin
| distributor    = Warner Bros.
| released       =  
| runtime        = 117 minutes  
| country        = United Kingdom
| language       = English
}} historical Drama drama horror The Devils possessions in sexually repressed nun who finds herself inadvertently responsible for the accusations.

The film faced harsh reaction from national film rating systems due to its disturbingly violent, sexual, and religious content, and originally received an X rating in both Britain and the United States. It was banned in several countries, and eventually heavily edited for release in others. The film has never received a release in its original, uncut form in various countries, and is largely unavailable in the home video market.

==Plot==
:Note: This plot is for the unedited version of the film. Some scenes described below are omitted from other versions.
In 17th Century France, Cardinal Richelieu is influencing Louis XIII in an attempt to gain further power. He convinces Louis that the fortifications of cities throughout France should be demolished to prevent Protestants from uprising. Louis agrees, but forbids Richelieu from carrying out demolitions in the town of Loudun, having made a promise to its Governor not to damage the town.
 Ursuline convent), sacrilegious bitch," among other things.

Baron Jean de Laubardemont arrives with orders to demolish the city, overriding Grandiers orders to stop. Grandier summons the towns soldiers and forces Laubardemont to back down pending the arrival of an order for the demolition from King Louis. Grandier departs Loudun to visit the King. In the meantime, Sister Jeanne is informed by Father Mignon that he is to be their new confessor. She informs him of Grandiers marriage and affairs, and also inadvertently accuses Grandier of witchcraft and of possessing her, information that Mignon relays to Laubardemont. In the process, the information is pared down to just the claim that Grandier has bewitched the convent and has dealt with the Devil. With Grandier away from Loudon, Laubardemont and Mignon decide to find evidence against him.

Laubardemont summons the lunatic inquisitor Father Pierre Barre, a "professional witch-hunter," whose interrogations actually involve depraved acts of "exorcism", including the forced administration of enemas to his victims. Sister Jeanne claims that Grandier has bewitched her, and the other nuns do the same. A public exorcism erupts in the town, in which the nuns remove their clothes and enter a state of "religious" frenzy. Duke Henri de Condé (actually King Louis in disguise) arrives, claiming to be carrying a holy relic which can exorcise the "devils" possessing the nuns. Father Barre then proceeds to use the relic in "exorcising" the nuns, who then appear as though they have been cured – until Condé/Louis reveals the case allegedly containing the relic to be empty. Despite proof to the contrary, both the possessions and the exorcisms continue unabated, eventually descending into a massive orgy in the church in which the disrobed nuns remove the crucifix from above the high altar and sexually assault it.

In the midst of the chaos, Grandier and Madeleine return and are immediately arrested. After being given a ridiculous show trial, Grandier is shaven and tortured – although at his execution, he eventually manages to convince Mignon that he is innocent. The judges, clearly under orders from Laubardemont, sentence Grandier to death by burning at the stake. Laubardemont has also obtained permission to destroy the citys fortifications. Despite pressure on Grandier to confess to the trumped-up charges, he refuses, and is then taken to be burnt at the stake. His executioner promises to strangle him rather than let him suffer the agonising death by fire that he would otherwise experience, but the overzealous Barre starts the fire himself, and Mignon, now visibly panic-stricken about the possibility of Grandiers innocence, pulls the noose tight before it can be used to strangle the priest. As Grandier burns, Laubardemont gives the order for explosive charges to be set off and the city walls are blown up, causing the revelling townspeople to flee.

After the execution, Barre leaves Loudun to continue his witch-hunting activities elsewhere in the southwest of France. Laubardemont informs Sister Jeanne that Mignon has been put away in an asylum for claiming that Grandier was innocent (the explanation given is that he is demented), and that "with no signed confession to prove otherwise, everyone has the same opinion". He gives her Grandiers charred femur and leaves. Sister Jeanne, now completely broken, masturbates pathetically with the bone. Madeleine, having been released, is seen walking over the rubble of Louduns walls and away from the ruined city as the film ends.

==Cast==
 
* Oliver Reed as Father Urbain Grandier
* Vanessa Redgrave as Sister Jeanne of the Angels
* Dudley Sutton as the Baron de Laubardemont
* Max Adrian as Ibert
* Gemma Jones as Madeleine De Brou
* Murray Melvin as Father Mignon
* Michael Gothard as Father Pierre Barre
* Georgina Hale as Philippe Trincant Brian Murphy as Adam
* John Woodvine as Louis Trincant
* Christopher Logue as Cardinal Richelieu
* Kenneth Colley as Legrand
* Graham Armitage as Louis XIII of France
* Andrew Faulds as Rangier
* Judith Paris as Sister Judith
* Catherine Willmer as Sister Catherine

==Reaction==
  The Boy Friend. Film
historian Joel W. Finler described The Devils as Russells "most brilliant cinematic achievement, but widely regarded
as his most distasteful and offensive work". Joel W. Finler,  The Movie Directors Story. London : Octopus Books, 1985.
ISBN 0706422880  (p. 240)  In 2002, when 100 film makers and critics were asked to cite what they considered to be the ten most important films ever made, The Devils featured in the lists submitted by critic Mark Kermode and director Alex Cox. 

==Censorship==
This was a highly controversial film with a rough history of censorship issues; its commentary on religious institutions such as the Catholic Church and organised religion in general stirred up controversy from censorship and ratings boards around the world. This, combined with its graphic depictions of violence, accentuated the films uncompromising subject matter.   
 British Board Festival of Light. 
 masturbating with the charred femur of Grandier at the end of the film. However, even in its released form, the film was considerably stronger in detail than most films released prior to that point.
 R rating.

==Modern reissues==
All of this material  was presumed lost or destroyed until critic Mark Kermode found the complete "Rape of Christ" sequence and several other deleted scenes (including the fuller version of Sister Jeannes masturbation scene as well as additional sequences of naked nuns lounging around the convent and a bawdy dance performed by travelling players mimicking the bizarre events whilst Grandier is being led to his death) in 2002. The artist Adam Chodzko made a video work in which he traced and interviewed many of the actresses who had played the nuns during the orgy scene. Although some material may have been lost forever, the NFT was able to show The Devils in the fullest possible state in 2004. This uncut directors version premiered at the Brussels International Festival of Fantasy Film in March 2006.

On 25 April 2007, The Devils was shown for a second time in its fullest possible state to a group of students and staff at the University of Southampton, followed by a question and answer session with the director, moderated by Mark Kermode. It was the first significant event to take place during Russells tenure as a visiting fellow at the University of Southampton in the English and film departments, April 2007 – March 2008.

On 19 July 2010, the film was screened for the Fantasia Film Festival in Montreal at the Hall theatre, preceded by a Q & A session with director Ken Russell. It was screened again on 28 July.

On the evening of 20 August 2010, the American Cinematheque in Los Angeles hosted Ken Russell at the Aero Theatre in Santa Monica with screenings of The Devils (108-minute version) and Altered States with Charles Haid and Stuart Baird in attendance. There was a discussion between films with director Russell and actor Charles Haid, and moderated by Mick Garris.

On 29 August 2010, The Devils was again shown at the Bloor Street Cinema in Toronto, Ontario, preceded by a question and answer session with director Ken Russell.

An NTSC-format DVD edition of the R-rated version on the Angel Digital label appeared in 2005, with the so-called "Rape of Christ" scene and other censored footage restored, and featuring a documentary by Mark Kermode about the film, as well as interviews with Russell, some of the surviving cast members, and a member of the BBFC who participated in the original censorship of the film. The consensus among those who have purchased this edition is that video quality of the DVD is only to the standard of a recording from television and may be a recording from the UK Channel 4 screening of 25 November 2002. 

DVDActive.com announced on 28 February 2008 that The Devils would finally be released on DVD by Warner Home Video in the US on 20 May 2008, in the UK theatrical (111 min) version, but without additional material. However, a day later, a DVDActive forum post asserted that the release had been dropped from Warners schedule.  Warner Bros released The Devils on DVD in Spain in the summer of 2010. However, it was the heavily cut US version. Despite this, the US R-rated version surfaced again on 31 December 2010 as a Euro Cult DVD, with the so-called Rape of Christ scene and some censored footage restored, and with the accompaniment of the above-mentioned interviews as well as commentary by the censors and the US trailer. This claimed-to-be uncut NTSC Euro Cult DVD as available in the US during 2011 actually plays for under 109 minutes even though the packaging claims 111 minutes; it does not include all the missing footage. The quality is acceptable but definition is poor, as in a transfer from domestic videotape. Most of these DVD releases present the film in a cropped to 1.78:1 from its original 2.45:1 Panavision ratio.

In June 2010, Warner Bros. released The Devils in a 108-minute version for purchase and rental through the iTunes Store,  but the title was removed without explanation after about three days  and remained unavailable until reappearing in April 2011. 

In April 2011, Londons East End Film Festival screened the full uncut version, which was claimed to be only the third time this version has been shown in the UK. Ken Russell and other cast were in attendance to take part in a Q&A afterwards. 

The British Film Institute released the UK theatrical version (111 minutes, or 107 minutes on PAL systems) on DVD on 19 March 2012. BFI licensed the film from Warner Brothers but was not permitted to include the additional Kermode-found footage from 2004 nor issue the film on Blu-ray.  The BFI release also includes the Kermode documentary on the history of the film entitled Hell on Earth: The Desecration & Resurrection of The Devils, as well as a vintage documentary shot during the production entitled Directing Devils. Additionally, the release includes an early Ken Russell short film entitled Amelia and the Angel, which was made shortly after Russell converted to Catholicism.  

Photographs from the lost Spike Milligan scene, together with Murray Melvins memories of that days filming, are included in Paul Suttons book, Six English Filmmakers (2014, ISBN  978-0957246256).

==See also==
* Mother Joan of the Angels – A 1961 Polish film also based on the Loudun possessions.
* The Crucible, ostensibly about Salem witch trials, but actually a political satire that shares several plot parallels with The Devils.
* The Devils of Loudun (opera) by Krzysztof Penderecki, 1968 and 1969; revisions 1972 and 1975.

==References==
 

==External links==
*  
*  
*  
*   

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 