Plaster Caster
 
 
{{Infobox Film
| name           = Plaster Caster
| image          = Plaster Caster.jpg
| caption        = DVD cover for the film.
| director       = Jessica Everleth
| producer       = 
| writer         = 
| narrator       = 
| starring       = Cynthia Plaster Caster
| music          = 
| cinematography = Jeff Economy & Ken Heinemann
| editing        = Brian R. Johnson
| distributor    = Xenon Pictures
| released       = 2001
| runtime        = 
| country        = USA
| language       = English
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 documentary about Cynthia Plaster Caster, the legendary groupie who became famous for making plaster casts of rock stars penises, including Jimi Hendrixs.

== Synopsis ==

The film explores how Cynthia developed her unique pursuit, follows the ups and downs of casting sessions with a shy guitarist and an extroverted glam rocker, and goes along for the ride as Cynthia prepares for her first gallery show in New York City.

== Interview Subjects ==

* Jello Biafra (The Dead Kennedys)
* Eric Burdon (The Animals)
* Pete Shelley (The Buzzcocks)
* Jon Langford (Mekons) Wayne Kramer (MC-5)
* Paul Barker (Ministry (band)|Ministry) Chris Connelly (The Revolting Cocks)
* Ian Svenonius (The Make-Up, Nation of Ulysses)
* Momus
* Camille Paglia
* Ed Paschke

== External links ==
*  
*  
*  
*  


 
 
 
 
 