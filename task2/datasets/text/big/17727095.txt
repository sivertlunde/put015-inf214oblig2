The Girl of the Golden West (1938 film)
 
{{Infobox film
| name           = The Girl of the Golden West
| image          = The-girl-of-the-golden-west-1938.jpg
| image_size     = 
| caption        = 
| director       = Robert Z. Leonard
| producer       = Robert Z. Leonard William Anthony McGuire
| writer         = David Belasco (play) Isabel Dawn Boyce DeGaw
| starring       = Jeanette MacDonald Nelson Eddy Walter Pidgeon
| music          = Herbert Stothart
| cinematography = Oliver T. Marsh
| editing        = W. Donn Hayes
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 121 minutes
| country        = United States
| language       = English
| budget         = $1,680,000 Turk, Edward Baron "Hollywood Diva: A Biography of Jeanette MacDonald" (University of California Press, 1998)   . 
| gross          = $1,597,000 (Domestic earnings)  $1,285,000 (Foreign earnings) 
}}
 musical western film. It was adapted from the play of the same name by David Belasco, better known for providing the plot of the opera La fanciulla del West by Giacomo Puccini. A frontier woman falls in love with an outlaw. 

==Cast==
*Jeanette MacDonald as Mary Robbins
*Nelson Eddy as Ramerez
*Walter Pidgeon as Sheriff Jack Rance
*Leo Carrillo as Mosquito
*Buddy Ebsen as Alabama
*Leonard Penn as Pedro
*Priscilla Lawson as Nina Martinez
*Bob Murphy as Sonora Slim
*Olin Howland as Trinidad Joe
*Cliff Edwards as Minstrel Joe
*Billy Bevan as Nick
*Brandon Tynan as The Professor
*H.B. Warner as Father Sienna
*Monty Woolley as The Governor
*Charley Grapewin as Uncle Davy (in prologue)
*Noah Beery, Sr. as The General - in prologue (as Noah Beery Sr.)
*Bill Cody, Jr. as Gringo (young Ramirez; in prologue)
*Jeanne Ellis as Young Mary Robbins (in prologue)
*Ynez Seabury as Wowkle

==Soundtrack==
* Sun-Up to Sun Down; Played during the opening credits
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Jeanne Ellis and the pioneers in the prologue

* Shadows On The Moon
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Jeanne Ellis at a campfire in the prologue
** Reprised by Jeanette MacDonald
** Whistled and hummed by Nelson Eddy

* Soldiers Of Fortune
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Noah Beery and his men in the prologue, Bill Cody, Jr. (dubbed by Raymond Chace) in the prologue
** Reprised by Nelson Eddy and his men

* The Wind In The Trees
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Jeanette MacDonald
** Played on a fife by Buddy Ebsen

* Gwine to Rune All Night (1850); (De Camptown Races)
** Written by Stephen Foster
** Played as background music in the saloon

* Polly Wolly Doodle
** Composer unknown
** Played as background music in the saloon

* Liebestraum (Dream of Love)
** Music by Franz Liszt
** Lyrics by Gus Kahn
** Played on piano by Brandon Tynan
** Sung by Jeanette MacDonald

* Ave Maria
** Music by Charles Gounod
** Adopted from the First Prelude in The Well-Tempered Clavier
** By Johann Sebastian Bach
** Played on an organ by H.B. Warner
** Sung by Jeanette MacDonald and chorus

* Señorita
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Nelson Eddy and party guests
** Reprised by Jeanette MacDonald and Nelson Eddy

* Mariache
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Additional lyrics by Carlos Ruffino
** Translation for Spanish lyrics by Zacharias Yaconelli (uncredited)
** Sung by Jeanette MacDonald, Nelson Eddy and chorus
** Danced to by the party guests

* The West Aint Wild Anymore
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Buddy Ebsen

* Who Are We To Say
** Music by Sigmund Romberg
** Lyrics by Gus Kahn
** Sung by Nelson Eddy
** Hummed by Jeanette MacDonald
** Reprised on piano by Brandon Tynan and sung by Jeanette MacDonald

* The Wedding March
** from A Midsummer Nights Dream, Op.61
** Written by Felix Mendelssohn-Bartholdy
** Played on a banjo and hummed by Cliff Edwards

==Box Office==
According to MGM records the film earned $2,882,000 resulting in a profit of $243,000. 


==See also== The Girl of the Golden West (1915) The Girl of the Golden West (1923) The Girl of the Golden West (1930)

==References==
 

==External links==
* 
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 