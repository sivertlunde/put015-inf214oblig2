Abbott and Costello Meet Captain Kidd
{{Infobox film
| name           = Abbott and Costello Meet Captain Kidd
| image          = a&cmeetcaptainkidd.jpg
| caption        = Theatrical release poster
| director       = Charles Lamont
| producer       = Alex Gottlieb John Grant Leif Erickson Fran Warren
| music          = Raoul Kraushaar
| cinematography = Stanley Cortez
| editing        = Edward Mann Woodley Productions
| distributor    = Warner Brothers
| released       = December 27, 1952
| runtime        = 70 min.
| country        = United States
| language       = English
| budget         = $701,688 
| gross          = $2 million (US) 
}}
 Captain Kidd. It was the second film in SuperCinecolor, a three-color version of the two-color process Cinecolor. 

==Plot==
Oliver "Puddin Head" Johnson (Lou Costello) and Rocky Stonebridge (Bud Abbott) are on their way to Deaths Head Tavern, where they work. They encounter Lady Jane (Fran Warren), who asks them to bring a love note to the tavern singer, Bruce Martingale (Bill Shirley).

At the tavern, the notorious Captain Kidd (Charles Laughton) is dining with Captain Bonney (Hillary Brooke), a female pirate. She accuses Kidd of raiding ships in her territory and is asking for restitution. Kidd informs Bonney that he has hidden the amassed treasure on Skull Island, and that only he has the map to its exact location. He agrees to take her, with her ship in tow, to the island so that she can receive her share. During the discussion, Oliver happens to be waiting on them, and inadvertently switches the map for the love note that he was carrying. Rocky discovers the mistake and goes to Captain Kidd, demanding a share of the treasure and a place on the voyage in exchange for the map. Kidd ostensibly agrees, but intends to kill Oliver and Rocky once he gets the map.

The voyage begins (with the addition of Bruce, who has been shanghaied), and Kidd unsuccessfully attempts to regain the map throughout the entire voyage. (Meanwhile, Bonney mistakenly believes that Oliver wrote the love note and has now fallen for him—further complicating the whole situation).  During the voyage, Kidd raids another ship, which happens to have Lady Jane on board, and she is kidnapped.

The two ships finally arrive at Skull Island; Oliver and Rocky begin to dig up the treasure, when Kidd arrogantly declares his plans to dispose of them along with Captain Bonney. They alert Bonney to Kidds true intentions, and her crew attacks. The treasure is recovered, and Bonneys crew wins the fight, with Kidd becoming her prisoner.

==Cast==
* Bud Abbott as Rocky Stonebridge  
* Lou Costello as Captain Puddin head Feathergill  
* Charles Laughton as Capt. William Kidd  
* Hillary Brooke as Capt. Bonney  
* Bill Shirley as Bruce Martingale   Leif Erickson as Morgan  
* Fran Warren as Lady Jane

==Production== Universal would Jack and the Beanstalk, using Costellos company, Exclusive Productions).  Produced during a slump in Charles Laughtons career, the accomplished actor signed on to do the film for a mere $25,000. 

Shortly after filming was completed, on April 6, Abbott and Costello hosted an episode of the Colgate Comedy Hour and brought Laughton along as a guest. Later that year, the three of them filmed a two-minute commercial for Christmas Seals.

==Re-release==
It was re-released in 1960 by RKO Pictures.

==Routines== Who Done It?, is used again here.  (This time, Captain Kidd demonstrates to Oliver how handcuffs should be worn by putting them on himself).

==DVD release== Rio Rita, were released on DVD on April 1, 2011 by Warner Bros. on the WB Archive Collection.

==References==
 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 