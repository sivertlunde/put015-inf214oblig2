Odakkuzhal
{{Infobox film
| name           = Odakkuzhal
| image          =
| caption        =
| director       = P. N. Menon (director)|P. N. Menon
| producer       = MP Navakumar
| writer         = N. P. Chellappan Nair Sherif (dialogues)
| screenplay     = P. N. Menon (director)|P. N. Menon
| starring       = Sheela Jose Prakash P. J. Antony Alummoodan
| music          = M. K. Arjunan Ashok Kumar
| editing        = Ravi
| studio         = Ratnagiri
| distributor    = Ratnagiri
| released       =  
| country        = India Malayalam
}}
 1975 Cinema Indian Malayalam Malayalam film,  directed by P. N. Menon (director)|P. N. Menon and produced by MP Navakumar. The film stars Sheela, Jose Prakash, P. J. Antony and Alummoodan in lead roles. The film had musical score by M. K. Arjunan.   

==Cast==
 
*Sheela
*Jose Prakash
*P. J. Antony
*Alummoodan
*Bahadoor
*Janardanan
*MG Soman
*Rani Chandra
*Shekhar
 

==Soundtrack==
The music was composed by M. K. Arjunan and lyrics was written by Vayalar.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Dukhadevathe Unaroo || S Janaki || Vayalar ||
|-
| 2 || Manassum Maamsavum || K. J. Yesudas || Vayalar ||
|-
| 3 || Naalillam Nalla Nadumuttam || P Jayachandran || Vayalar ||
|-
| 4 || Varnangal Vividha Vividha Varnangal || K. J. Yesudas || Vayalar ||
|}

==References==
 

==External links==
*  

 
 
 


 