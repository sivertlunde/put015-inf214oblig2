Peg of Old Drury
{{Infobox film
| name           = Peg of Old Drury
| image          = "Peg_of_Old_Drury"_(1935).jpg
| image size     = 
| border         = 
| alt            = 
| caption        = Anna Neagle and Jack Hawkins
| director       = Herbert Wilcox
| producer       = Herbert Wilcox
| writer         = 
| screenplay     = Miles Malleson
| story          = 
| based on       = the play Masks and Faces by Charles Reade and Tom Taylor
| narrator       = 
| starring       = Anna Neagle Cedric Hardwicke Margaretta Scott
| music          = 
| cinematography = Freddie Young
| editing        =  Merrill G. White
| studio         = Herbert Wilcox Productions  for British & Dominions Film Corporation
| distributor    = United Artists Corporation  (UK)
| released       =  28 August 1935	(London)  (UK)
| runtime        = 75 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}} British historical Shakespearian performance, Richard III and As You Like It. 

The film was voted the third best British movie of 1936. 

==Cast==
* Anna Neagle ...  Peg Woffington 
* Cedric Hardwicke ...  David Garrick 
* Margaretta Scott ...  Kitty Clive 
* Maire ONeill ...  Mrs. Woffington - Pegs Mother 
* Arthur Sinclair ...  Mr. Woofington - Pegs Father 
* Dorothy Robinson ...  Mrs. Margaret Dalloway 
* Polly Emery ...  Martha the Maid 
* Aubrey Fitzgerald ...  Digby 
* Jack Hawkins ...  Michael OTaffe  Robert Atkins ...  Dr. Samuel Johnson
* Hay Petrie ...  Mr. Rich 
* George Barrett ...  Tom - Stage Doorkeeper 
* Stuart Robertson ...  Singer 
* Leslie French ...  Alexander Pope 
* Tom Heslewood ...  William Pitt
* Christopher Steele ...  Oliver Goldsmith 
* Eliot Makeham ...  Dr. Bowdler 
* Sara Allgood ...  Irish Woman on Boat

==Critical reception==
The New York Times wrote, "with superb acting, photography that is effective and unusual, yet not bizarre, and direction that is gentleness and good taste itself, Peg of Old Drury is one of the finest cinema production ever to come out of England, or of anywhere else, for that matter";  {{cite web|url=http://www.nytimes.com/movie/review?res=9E01E5DF143CE53ABC4B52DFB266838D629EDE|title=Movie Review - Jonson are Malleson in his first screenwriting assignment."  

==References==
 

==External links==
* 
 

 
 
 
 
 
 
 
 
 
 


 
 