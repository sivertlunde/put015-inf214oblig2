Town Without Pity
{{Infobox film
| name           = Town Without Pity
| image          = Town Without Pity.jpg
| image_size     = 
| alt            = 
| caption        = Original German theatrical release poster
| director       = Gottfried Reinhardt
| producer       = Eberhard Meichsner Gottfried Reinhardt
| screenplay     = George Hurdalek Jan Lustig Silvia Reinhardt Dalton Trumbo
| based on       =  
| starring       = Kirk Douglas Christine Kaufmann E. G. Marshall
| music          = Dimitri Tiomkin
| cinematography = Kurt Hasse
| editing        = Werner Preuss The Mirisch Corporation
| distributor    = United Artists
| released       =  
| runtime        = 105 minutes
| country        = United States Switzerland West Germany
| language       = English German
}}

 
 The Mirisch Corporation, the film stars Kirk Douglas, Christine Kaufmann, and E. G. Marshall.

The film was based on the 1960 novel Das Urteil (The Verdict) by German writer Gregor Dorfmeister, who wrote under the pen name Manfred Gregor. The film was rewritten at Kirk Douglas suggestion by Dalton Trumbo without credit. 

==Plot== occupied Germany Robert Blake). When Frank hears her screams for help, he runs to her, but is knocked out. After the four men are done, the guilt-ridden Larkin lingers behind; he covers the girl with his shirt before leaving with the others. 

The soldiers are quickly apprehended. To appease the anger and outrage of the Germans, General Stafford orders that their court martial be held in public in the local high school gymnasium. The prosecutor, Colonel Jerome Pakenham (E. G. Marshall), seeks the death penalty. Major Steve Garrett (Kirk Douglas) is assigned to defend them. After interviewing his clients, Garrett tries to plea bargain for long sentences at hard labor in an attempt to save their lives, but Pakenham has such a strong case, he turns it down. Garrett starts investigating, questioning the residents. He is followed by Inge Koerner (Barbara Rütting), a hostile German reporter from what Garrett considers to be a scandal-seeking newspaper. 
 Hans Nielsen), to withdraw her from the trial before it is too late, stating that he will have to break her down on the stand to save his clients. He advises Herr Steinof to take his family and leave town, but Steinhof refuses. 

With no choice, Garrett shows that Karin is not as innocent as she first appeared, nor is she well liked. He also catches both her and Frank in pointless little lies to destroy their credibility. As his cross examination of Karin continues, the girl eventually collapses under the strain. Her father withdraws her from the trial, which ensures that the defendants cannot be executed. Three are sentenced to long terms at hard labor; Larkin is given a shorter sentence of six years. The damage has been done, however; the townsfolk turn against Karin.

Though Frank attacks him with a whip, Garrett tells him to take Karin and leave town forever. The young man takes his advice, but to raise money, he forges his mothers check. Determined to keep her son under her control, she sends the police after the couple. While Frank argues with the policemen, Karin runs away. Koerner later informs Garrett that Karin committed suicide, drowning herself in the river near where she had been violated. Shaken by the tragic ongoings, Garrett decides to leave the town without saying too much to the others, finding that, as the last line of the title song goes, "It isnt very pretty what a town without pity can do."

==Cast==
* Kirk Douglas as Major Garrett
* Barbara Rütting as Inge Koerner  
* Christine Kaufmann as Karin Steinhof
* E. G. Marshall as Colonel Pakenham Hans Nielsen as Karl Steinhof
* Ingrid van Bergen as Trude, a prostitute who provides Garrett with information  Robert Blake as Corporal Jim Larkin
* Richard Jaeckel as Corporal Birdwell Scott
* Frank Sutton as Sergeant Chuck Snyder
* Karin Hardt as Frau Steinhof, Karins mother
* Fred Dur as Gerichtsoffizier
* Gerhart Lippert as Frank Borgmann 
* Mal Sondock as Private Joey Haines
* Alan Gifford as General Stafford
* Max Haufler as Doctor Urban, called to testify about Larkin
* Rose Renée Roth as Frau Kulig, a town gossip
* Eleonore von Hoogstraten as Frau Borgmann, Franks mother
* Egon von Jordan as Bürgermeister
* Philo Hauser as Herr Schmidt

==Production== Town Without Gene Pitney. The song became an Academy Award nominee and Pitneys first Top 40 single. 

Filming took place in the towns of Bamberg and Forchheim in Bavaria, Germany, with some scenes shot in a studio in Vienna, Austria. 

==See also==
* Trial movies

==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 