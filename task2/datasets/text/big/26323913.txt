Saturn Returns (film)
{{Infobox film
| name           = Saturn Returns
| image          = Saturn_Returns_Film.jpg
| image_size     = 
| caption        = 
| director       = Lior Shamriz
| producer       = Lior Shamriz, Imri Kahn
| writer         = Imri Kahn, Lior Shamriz
| narrator       = 
| starring       = Chloe Griffin, Tal Meiri, Joshua Bogle
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = Israel Germany
| language       = English French German Hebrew
| budget         = $2500
| preceded_by    = 
| followed_by    = 
}}
Saturn Returns is a 2009 film directed by Lior Shamriz, starring Chloe Griffin, Tal Meiri and Joshua Bogle. It was written and produced by Lior Shamriz and Imri Kahn.

== Cast ==
{| class="wikitable"
|- bgcolor="#efefef"
! Actor !! Role
|- Chloe Griffin|| Lucy
|- Tal Meiri|| Galia
|- Joshua Bogle|| Derek
|- Heinz Emigholz||Edgar Schmidt
|- Julien Binet||Jeremy
|- Martin Deckert|| Linda
|}

The cast also includes Namosh, Susanne Sachsse, Imri Kahn and Lior Shamriz.

== Plot ==
The film title references to the astrological phenomenon Saturn Return that occurs at the ages of 27-30, 58-60, and finally from 86-88, coinciding with the time it takes the planet Saturn to make one orbit around the sun.
 expat in punk hedonism, roams the streets with her best friend, Derek. Together they use the city like a playground, a stage, and a never ending party. Into their lives enters Galia, a young Israeli woman who is presumably carrying the promise of a better, cleaner way of living.

A tribute to punk underground films turns into a melodrama, mirroring Lucy and Galia’s modulating states of mind. Their look into each other’s life and culture becomes an investigation of empty facades.

==Production==
According to the filmmakers, the shooting of the film took place in two days during December 2007 in Israel and twelve days in February 2008 in Berlin, with professional and non-professional actors and a budget of about €2,000.   The film was shot and edited by the director, with only one more crew member on set, usually the script co-author, Imri Kahn. All public spaces (including streets, museums, shops, bars) were shot “as is”, with no interventions and no extras. The film is set within a social scene of the Kreuzberg neighborhood in Berlin, where Chloe Griffin (Lucy), Joshua Bogle (Derek) and most of the cast live.  The film was constructed by both improvised and Screenplay|pre-scripted scenes, as required by the nature of each scene. In an interview to Spanish press, Shamriz said:  "I consider myself an anti-auteur. I find it important that the cast and the crew contribute their ideas to the film".
 stateless Israeli director Lior Shamriz".  It was later nominated to the German Max Ophüls Prize  and won the Best Motion Picture prize at the Achtung Berlin Film Festival ("New Berlin Film Award"). 

The filmmakers wrote about their film: "One of the objectives of the film was to create a modulating film language that adapts to the states of mind of the characters. The production adjusted itself to every part of the film, sometimes imitating documentary approaches and sometimes setting a framework where scripted dialogue was enacted within a pre-planned shooting." 

In early 2010, the director of the film published three experimental films referencing "Saturn Returns" and nicknamed "Saturn Returns Three Satellite Films".  Return Return, the first of the three, premiered at the 60th Berlin Film Festival. 

==See also== Lior Shamriz filmography

==References==
 

== External links ==
* 
* 

 
 
 
 
 
 
 
 
 
 
 