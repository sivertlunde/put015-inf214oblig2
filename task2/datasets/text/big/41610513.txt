Big Match (film)
{{Infobox film name           = Big Match 

 image          =  director       = Choi Ho producer       = Park Sang-hyun writer         = Choi Ho starring  Lee Sung-min   BoA music          =   cinematography =    editing        =  studio         = BK Pictures distributor    = United International Pictures released       =   runtime        = 112 minutes country        = South Korea language       = Korean
}}
 Lee Sung-min and BoA.  

==Plot== UFC mixed martial arts star nicknamed "Zombie." When his older brother and coach Young-ho suddenly disappears, the police arrests him as the prime murder suspect. Then Ik-ho receives a phone call from Ace, a genius mastermind who designs elaborate games for Koreas wealthiest citizens to bet astronomical sums of money on, conducted with real people in real time (media)|real-time using high-tech gadgets and CCTV cameras. To save his brothers life and his own, Ik-ho is forced to join Aces game and follow instructions from a woman of mystery named Soo-kyung. Using his wits and skills against multiple adversaries, including cops and low-rent gangsters, Ik-ho completes seemingly impossible missions. Then he reaches the final round, during which he must find his brother in a huge soccer stadium, with a bomb strapped to his ankle and time ticking.

==Cast==
*Lee Jung-jae as Choi Ik-ho   
*Shin Ha-kyun as Ace Lee Sung-min as Choi Young-ho
*BoA as Soo-kyung    
*Kim Eui-sung as Detective Do
*Bae Sung-woo as Axe, gang member
*Son Ho-jun as Jae-yeol, president of Ik-hos fan club
*Choi Woo-shik as Guru, hacker
*Kim Yoon-seong as Department head Jo
*Park Doo-sik as Detective Nam
*Ra Mi-ran as Young-hos wife

==References==
 

==External links==
*   
* 
* 
* 

 
 
 
 

 