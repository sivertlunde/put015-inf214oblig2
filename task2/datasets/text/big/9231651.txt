Gentleman Jim (film)
{{Infobox film
| name           = Gentleman Jim
| image          = File:Gentleman Jim - Poster.jpg
| image_size     =
| caption        = 1942 Theatrical Poster
| director       = Raoul Walsh
| producer       = Robert Buckner
| writer         = Vincent Lawrence Horace McCoy based on = autobiography The Roar of the Crowd: The Rise and Fall of a Champion by Jim Corbett
| starring       = Errol Flynn Alexis Smith
| music          = Heinz Roemheld
| cinematography = Sidney Hickox
| editing        = Jack Killifer
| distributor    = Warner Bros. Pictures
| released       =   1948 (France)
| runtime        = 104 minutes
| country        = United States English
| budget         = gross = 1,255,311 admissions (France) 
}}
 Alan Hale, William Frawley, and Ward Bond. The movie was based upon Corbetts autobiography, The Roar of the Crowd, and directed by Raoul Walsh. The role was one of Flynns favorites. Tony Thomas, Rudy Behlmer * Clifford McCarty, The Films of Errol Flynn, Citadel Press, 1969 p 116-117 

==Plot== Rhys Williams) Alan Hale), has excellent fighting skills; Geary likes his protégés seemingly-polished manner.

However, Corbetts arrogance irritates many of the upper class, particularly Victoria Ware (Alexis Smith). They clash frequently, but Corbett is attracted to her, and his limitless self-confidence and charm eventually overcome her distaste for him.

Meanwhile, Corbett becomes a professional prizefighter. He acquires a manager, Billy Delaney (William Frawley), and introduces a new, more sophisticated style of boxing, emphasizing footwork over the unscientific brawling epitomized by world champion John L. Sullivan (Ward Bond). After winning several matches, Corbett finally gets the opportunity to take on the great man. Corbetts method of boxing baffles Sullivan, and Corbett wins not only the title, but also Victoria.

Corbett is crowned as the new Heavyweight Champion. His victory party is unexpectedly interrupted by the defeated Sullivan, who has come to personally present the championship belt to Corbett.

==Cast==
* Errol Flynn as James J. Corbett
* Alexis Smith as Victoria Ware
* Jack Carson as Walter Lowrie Alan Hale as Pat Corbett John Loder as Carlton De Witt
* William Frawley as Bill Delaney
* Minor Watson as Buck Ware
* Ward Bond as John L. Sullivan
* Madeleine LeBeau as Anna Held Rhys Williams Harry Watson
* Arthur Shields as Father Burke
* Dorothy Vaughan as Ma Corbett

==Production==
===Development===
In July 1941 it was announced that Warner Bros had purchased the rights to make a film of Corbetts life from his widow, Vera. Errol Flynn was intended to star. SCREEN NEWS HERE AND IN HOLLYWOOD: Ernst Lubitsch Signs Ginger Rogers to Star in His First Production for Fox NEW FILM AT MUSIC HALL  Tom, Dick and Harry to Open Today -- Arnold Pressburger to Produce Saxophone
By DOUGLAS W. CHURCHILLBy Telephone to THE NEW YORK TIMES.. New York Times (1923-Current file)   17 July 1941: 23.   Aeneas MacKenzie and Wally Kline were signed to write the screenplay. Of Local Origin
New York Times (1923-Current file)   29 July 1941: 18.   Ann Sheridan was announced as female co-star. JIMMIE FIDLER IN HOLLYWOOD
Los Angeles Times (1923-Current File)   04 Sep 1941: A10.  

Filming was to start in January 1942, after which Flynn was to make The Sea Devil, a remake of The Sea Beast (Warners version of Moby Dick). SCREEN NEWS HERE AND IN HOLLYWOOD: Castings for There Goes Lona Henry and Bombardier Are Announced by RKO NEW YORK TOWN OPENS Comedy Drama at Paramount Today -- Board of Review Conference Tomorrow
By Telephone to THE NEW YORK TIMES.. New York Times (1923-Current file)   12 Nov 1941: 31.   Bill Morrow and Ed Beloin, gag writers for Jack Benny, did some work on the script. Kay Kyser Financial Record Now Soaring: Peter Pan Moved Ahead Joan Merrill Reoptioned Gilbert Play Purchased Tim Holt Build-up Seen Legion Nixes Garbo
Schallert, Edwin. Los Angeles Times (1923-Current File)   25 Nov 1941: A10.  Phil Silvers was announced in the support cast. Banquetless Academy Pushes Award Plans: Paramount After Hayes Laraine Day, Ayres Duo Silvers in Flynn Opus Carey Wins Two Roles Glover Cast With Kyser
Schallert, Edwin. Los Angeles Times (1923-Current File)   20 Dec 1941: 9.  

Flynn was very keen to make the movie and undertook extensive boxing training, working with Buster Wiles and Mushy Callahan. Town Called Hollywood
Scheuer, Philip K. Los Angeles Times (1923-Current File)   07 June 1942: C3.  
However when the US officially entered World War Two it was decided to postpone the movie so Flynn could be rushed into Desperate Journey. SCREEN NEWS HERE AND IN HOLLYWOOD: Errol Flynn to Be Starred in Desperate Journey, Soon to Go Before the Camera NEW GARBO FILM TODAY  Two-Faced Woman Arrives at the Capitol and Louisiana Purchase at Paramount
By Telephone to THE NEW YORK TIMES.. New York Times (1923-Current file)   31 Dec 1941: 22.   While he did this Horace McCoy rewrote the script. The Yearling, Abandoned Last Summer, Will Be Salvaged -- Roddy MacDowall Sought: NEW FILM DUE AT STRAND Wild Bill Hickok Rides Will Open Today -- Two Revivals at the Irving Place
By Telephone to THE NEW YORK TIMES.. New York Times (1923-Current file)   06 Feb 1942: 23.   Vincent Lawrence then wrote some drafts and Robert Buckner was assigned to produce. DRAMA: Pola Negri May Play in Story of Valentino
Schallert, Edwin. Los Angeles Times (1923-Current File)   16 Apr 1942: A10.   There was some doubt the film would even be made if Flynn enlisted in the army but they rejected his application on the grounds of ill-health. Filming began in May 1942.

===Shooting===
In his somewhat unreliable autobiography My Wicked, Wicked Ways, Flynn details how he suffered a mild heart attack while making this movie. He collapsed on set on 15 July 1942, while filming a boxing scene with Ward Bond.  Filming had to be shut down why he recovered; he returned a week later. 

Flynn took the role seriously, and was rarely doubled during the boxing sequences.  Alexis Smith recounted in the biography The Two Lives of Errol Flynn by Michael Freedland how she took the star aside and told him, "Its so silly, working all day and then playing all night and dissipating yourself. Dont you want to live a long life? Errol was his usually apparently unconcerned self: Im only interested in this half, he told her. I dont care for the future." 

==Reception==
This was the third Errol Flynn movie to gross at least $2 million for Warner Bros. in 1942, according to Variety. 

==See also==
* List of American films of 1942

==References==
 	

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 