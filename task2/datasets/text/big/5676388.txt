Unstoppable (2004 film)
{{Infobox film
| name = Unstoppable image = Unstoppable dvd movie cover snipes.jpg
| image_size = 215px
| caption = DVD Cover David Carson
| producer = Boaz Davidson Tom Jacobson Bob Misiorowski Jim Wedaa
| writer = Tom Vaughan Stuart Wilson Kim Coates
| music = Louis Febre
| cinematography = Ward Russell
| editing = David Jakubovic Alain Jakubowicz Jeremy Presner Millennium Films Columbia TriStar Home Entertainment
| released =  
| runtime = 96 minutes
| language = English
| country = United States
| budget = $20 Million
}} David Carson, Stuart Wilson and Kim Coates. The film was released in the United States on October 30, 2004.

==Plot==
A former CIA agent and ex-Special Forces member Dean Cage (Wesley Snipes), is in a rehab program, haunted by a botched mission in Bosnia which resulted in the execution of his best friend Scott (Cristian Solimeno). While in a restaurant waiting for his girlfriend, Detective Amy Knight (Jacqueline Obradors), who happens to be Scotts sister, he is mistakenly believed to be a CIA agent involved with a stolen military experimental truth serum. He is abducted by the real thieves and injected with the drug, which makes him relive the moments from the failed Bosnian mission and not able to discern who are his allies and who are his enemies. Amy has six hours to find the antidote and save Deans life.

==Cast==
* Wesley Snipes as Dean Cage
* Jacqueline Obradors as Detective Amy Knight Stuart Wilson as Sullivan
* Kim Coates as Peterson Mark A. Sheppard as Leitch
* Adewale Akinnuoye-Agbaje as Agent Junod
* Vincent Riotta as Detective Jay Miller David Schofield as Dr. Collins
* Nicholas Aaron as McNab
* Kim Thomson as Agent Kennedy
* Jo Stone-Fewings as Agent Gabriel
* Cristian Solimeno as Scott Knight
* Gary Oliver as Sullivans Driver
* Andrew Pleavin as Cherney

==Production==
It is set and filmed at Baltimore Maryland and Los Angeles, California in 42 days on March 2 and April 13, 2003.

==Home media== Region 1 Columbia TriStar Home Entertainment.

==Music==
The credit song is "Move" from Leroy and Denoyd. 

==External links==
*  
 

 

 
 
 
 
 
 
  
 
 
 
 

 