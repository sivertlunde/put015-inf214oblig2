The Invader (1935 film)
{{Infobox film
| name           = The Invader
| image          =
| image_size     =
| caption        =
| director       = Adrian Brunel 
| producer       = Harold Richman   Sam Spiegel
| writer         = Edwin Greenfield
| narrator       =
| starring       = Buster Keaton   Lupita Tovar   Lyn Harding   Esme Percy John Greenwood
| cinematography = Eugen Schüfftan
| editing        = Daniel Birt
| studio         = British & Continental
| distributor    = MGM 
| released       = 1935
| runtime        = 61 minutes
| country        = United Kingdom English
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}
The Invader is a 1935 British comedy film directed by Adrian Brunel and starring Buster Keaton, Lupita Tovar and Lyn Harding. The film follows the same plot as its remake Pest from the West (1939), with a millionaire setting out to win a local girl in Mexico. 

The film was produced as a quota quickie at Isleworth Studios by British & Continental as part of a contract to supply films for MGM to meet its annual quota set by the British government. The film is also known under the alternative title An Old Spanish Custom,

==Cast==
* Buster Keaton as Leander Proudfoot 
* Lupita Tovar as Lupita Melez 
* Lyn Harding as Gonzalo Gonzalez 
* Esme Percy as Jose 
* Andreas Malandrinos as Carlos the barman 
* Clifford Heatherley as Cheeseman 
* Hilda Moreno as Carmita 
* Webster Booth as Cantina Singer

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 


 