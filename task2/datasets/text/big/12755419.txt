Self Medicated
 
 
{{Infobox film name =Self Medicated image =Self-Medicated Final Poster.jpg caption =Theatrical Poster director =Monty Lapica producer =Thomas Thomas Bell Monty Lapica writer =Monty Lapica starring =Diane Michael Bowen music =Anthony Marinelli cinematography =Denis Maloney editing =Timothy Kendall distributor =THINKFilm released =  runtime =107 minutes country =United States language =English
}} kidnapped at age 17 by a private company and forcibly committed to a locked-down psychiatric institute.

==Plot==
On the edges of Las Vegas, 17-year-old Andrew’s life is spiraling out of control. Unable to cope with the loss of his father, Andrew’s descent into drugs and violence is gaining momentum, and the once promising young man is now headed for self-destruction (in what Variety calls “an especially blistering performance by Monty Lapica”).
 
Andrew’s mother (Golden Globe nominee Diane Venora), helpless to control her son and fighting an addiction of her own, refuses to watch idly as her only child destroys himself. As a last resort, she hires a private company to forcibly kidnap and confine him in a locked-down and corrupt psychiatric hospital. As Andrew is subjected to the secret physical and emotional abuses of the program something inside him is re-awakened. He must somehow get free to save what’s left of his life, but to do that, he knows he must first face his own demons head-on.

==Reception==
The film has toured the festival circuit extensively, collecting 39 awards.
  
Reviews have been generally favorable. The Hollywood Reporter said there was "a raw, unmannered intensity in Lapicas performance sets the requisite tone for the entire production. Self- Medicated is just what the doctor ordered." {{cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =Hollywood Reporter
  | work =
  | publisher =
  | date =
  | url =http://www.hollywoodreporter.com/hr/search/article_display.jsp?vnu_content_id=1000979485
  | format =
  | doi =
  | accessdate =  }}, and Daily Variety said the film was "a searing portrait of an out-of-control youth . . . strong acting from all quarters and an especially blistering performance from Lapica.  Packs a startling punch!"   {{cite news
  | last =Chang
  | first =Justin
  | authorlink =
  | coauthors =
  | title =Daily Variety
  | work =
  | publisher =
  | date =2005-07-04
  | url =http://www.variety.com/review/VE1117927558.html?categoryid=31&cs=1&p=0
  | format =
  | doi =
  | accessdate =  }}

== Release == Las Vegas.

== Awards ==
* Rome Independent Film Festival – Grand Jury Prize
* Australian International Film Festival – Best Feature Film/Best Actress (Diane Venora) Angel Award Winner
* 2006 PRISM Award Winner
* WorldFest International Film Festival – Gold Remi Award Sundance Channel Audience Award/Best Ensemble Acting
* Berkeley Film Festival – Best Feature
* Memphis International Film Festival – Best Feature
* Tahoe/Reno International Film Festival – Best Spotlight Feature
* Newport Beach International Film Festival – Best First Feature
* San Luis Obispo International Film Festival – Best Feature
* Zion International Film Festival – Grand Jury Prize
* Santa Fe Film Festival – Best of the Southwest
* Tiburon International Film Festival – Best Actor (Monty Lapica)
* DIY Film Festival – Best Feature/Best Director
* George Lindsey Film Festival – Best Feature
* Cosmos International Film Festival – Best Feature
* Flint International Film Festival – Competition Jury Prize
* Twin Rivers Film Festival – Feature Film Award
* Zoie Film Festival – Best Picture
* Big Island Film Festival – Best Feature/Up-And-Coming Filmmaker Award Fuji Best Feature Shot on Film
* Lake Arrowhead Film Festival – Best Feature
* Trenton International Film Festival – Best Screenplay
* BridgeFest Film Festival Canada – Best Feature
* Staten Island Film Festival – Best New Filmmaker
* Estes Park Film Festival – Best Feature/Audience Award
* Tony Bennett / Mike Agassi Foundation – Inspiration Award
* VisionFest Film Festival – Best Feature/Best Actor (Monty Lapica)/Best Editing
* Charlotte Film Festival – Audience Award

== Cast ==
* Monty Lapica as Andrew Eriksen
* Diane Venora as Louise Eriksen Michael Bowen as Dan Jones
* Greg Germann as Keith McCauley

== See also ==
* Teen escort company
* World Wide Association of Specialty Programs and Schools

== External links ==
*  
*  

 
 
 
 
 