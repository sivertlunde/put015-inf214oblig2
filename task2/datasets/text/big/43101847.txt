Caught in Trap
 
{{Infobox film
| name           = Caught in Trap
| image          = CaughtinTrap.jpg
| alt            = 
| caption        = Theatrical poster
| film name      = {{Film name
 | traditional    = 破局
 | simplified     = 破局
 | pinyin         = Pò Jú
 | jyutping       = Po3 Geok6 }}
| director       = Lee Tso Nam
| producer       = Han Xiaoli Zhang Zhenxin
| writer         = Wang Min Zhou Lisong Zhou Zechun
| starring       = Bowie Lam Che Xiao Lee Qiang Guo Wei
| narrator       = 
| music          = Derek Zhao
| cinematography = 
| editing        = 
| studio         = China Film Co. Ltd. Wuhan Zuoyou Entertainment Production The Society of Prosecutors of HuBei Province
| distributor    = China Film Co. Ltd.
| released       =  
| runtime        = 90 minutes
| country        = China
| language       = Mandarin
| budget         = 
| gross          = 
}}
 crime Thriller thriller film directed by Lee Tso Nam and starring Bowie Lam, Che Xiao, Lee Qiang and Guo Wei. The film was released throughout China on 11 July 2014. 

==Plot==
In a village, due to the occurrence of forced land acquisition, villager Chang Ching accidentally killed a demolition personnel during an armed group fight and was arrested by the police, making the villagers dissatisfied. This incident also reflected illegal collisions between government officials and businesspeople, with the villagers wanting to punish corrupt officials.  

Newcomer Zhang Yun (Che Xiao) of the City Procuratorate, while reviewing Changs case of intentional injury, discovers many contradictions from the confiscated items, and collaborates with the head of the  Anti-malfeasance Bureau Gu Changfeng (Bowie Lam), who is in charge of investigating the corruption behind the case. Starting from the incident where suspect Ma Yicheng dies in a traffic accident, and discovers that demolition company owner Yang Shenqiang and "Jin Hua Yu Ting" boss Jin Yuting (Lee Qiang) has been illegally colluding, transferring and selling land with lnad planning and real estate management officials. To avoid legal action, Jin Yuting and others attempts to bribe Gu with money and blackmailing him by kidnapping with young daughter. Gu denies Jin and does not back off from the case, and also gets help from Yun, whom have secretly admired him for a long time.

Eventually, Chang Chings intentional injury case is identified as self-defense with the strict control by Yun. Chang was released without charge and unscrupulous businessmen and corrupt officials were all arrested. At this time, Jin plans to abscond with money abroad and Yun, while attempting to apprehend Jin, was injured when Mas wife ignited gasoline on her.

==Cast==
*Bowie Lam as Gu Changfeng (顧長風)
*Che Xiao as Zhang Yun (張芸)
*Lee Qiang as Jin Yuting (金玉庭)
*Guo Wei

==Production==
Star Bowie Lam reveals that since the film was planned and produced by real-life veteran prosecutors, the cases and the characters encounters in the film were based on real-life events. Also with the help of The Society of Prosecutors of HuBei Province, scenes depicting prisoner interrogations were filmed on location in real offices of prosecutors. Lam states, Our film comes from the reality of life, and seeking realistic themes and shooting styles, which are able to make the story to life and believable. To some extent, Caught in Trap can be considered as a docudrama. {{cite web|url=http://news.mtime.com/2014/06/19/1528540.html|title=
《破局》发剧场版海报剧照
林保怡演绎检察官 联手车晓反腐}} }}

==References==
 

 
 
 
 
 
 
 
 
 