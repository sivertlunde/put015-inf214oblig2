Dream Kitchen
{{Infobox film
| name           = Dream Kitchen
| image          =
| image_size     =
| caption        = 
| director       = Barry Dignam
| producer       = Conor McKiernan Katherina Fay Barry Dignam
| writer         = Barry Dignam  (adapted from an original stage scene by Kevin McCarthy)
| narrator       =
| starring       = {{plainlist |
* Andrew Lovern
* Frank Coughlan
* Caroline Rothwell
* Sarah Pilkington
* Loclann Aiken
}} Orchestra of the Opera House, Rome
| cinematography = David OSullivan
| editing        = Michelle Spilanne DLIADT
| distributor    =
| released       =   
| runtime        = 8 minutes
| country        = Ireland
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}
Dream Kitchen is a 1999 Irish short film directed by Barry Dignam in which a young man fantasises about coming out to his parents. 

==Plot==
A young man arrives home from school and meets his father fixing the car. The two barely exchange words except for the boys father asking him to hand him a pair of Locking pliers|Vise-Grips. When he goes inside he sees his pregnant sister smoking and his mother in the dark and untidy kitchen drinking vodka and coke. 

As he sits at the table he begins to daydream of being in a clean stylish kitchen with his well dressed mother. Speaking in faux-Elizabethan English to one another he tells his mother that he is gay and she is delighted with the news. She calls in his father who is wearing clean white overalls and when he is told of his sons secret he too is overjoyed. Just then the young mans sister enters and again reacts happily to the news of her brothers homosexuality and hugs him. 

The doorbell then rings and the son reveals to his family a glowingly handsome young man, Andrew; his boyfriend. The family welcome him and invites him to a celebratory feast.
 
The young man awakes from his daydream and is back in the grey kitchen. He tries to initiate a conversation with his mother about a television programme they watched the night before. His father enters and sits at the table still grubby from fixing the car. He asks what they are talking about, to which the mother says "...the programme about those fairy (gay slang)|fairies", to which he replies "Bloody perverts!" and rants that the programme is "rubbish" and "dangerous". The parents then ask why the son brought up the programme; just then the doorbell rings and he rushes out.

When he answers the door it is revealed that the handsome boyfriend from the fantasy was real, and the young man grabs his coat and smiles to the camera before they drive off together.

==Cast==
*Andrew Lovern as Son
*Frank Coughlan as Da
*Caroline Rothwell as Ma
*Sarah Pilkington as Daughter
*Loclann Aiken as Andrew

==Awards and accolades==
{|class="wikitable" border="1" align="centre"
|-
! Film Festival !! Category !! Year !! Outcome
|- Galway Film Best Irish Short Film|| 1999 || 
|- Berlin International Golden Berlin Bear|| 2000 || 
|- Rose Film Best Short Film|| 2000 || 
|- New York Audience Award for Best Short Film|| 2001 || 
|}

==References==
 

==External links==
* 
*  on Vimeo — the full short film

 
 
 


 
 