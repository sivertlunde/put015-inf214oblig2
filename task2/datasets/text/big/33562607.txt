My Tehran for Sale
 
 
{{Infobox film name      = My Tehran for Sale image     = My Tehran for Sale movie.jpg
}}
My Tehran for Sale ( , produced by Cyanfilms and starring Marzieh Vafamehr, Amir Chegini and Asha Mehrabi.

Officially selected by Toronto, Rotterdam, Pusan and several other international film festivals, this art-house debut film explores the contemporary Tehran and its underground art scene, focusing on the life of a young actress who has been banned from her theater work. Struggling to pursue her passion in art as well as her secret lifestyle in a socially oppressed environment, Marzieh gets involved in some subsequent and unexpected events leading her to a decision-making dilemma regarding her survival and identity. My Tehran for Sale has been compared with Cristian Mungius film 4 Months, 3 Weeks and 2 Days by Parviz Jahed, an Iranian critic and film scholar. {{cite web
| url=http://www.mardomak.org/story/60593
| script-title=fa:مردمک  - مهمانی به صرف شام و شلاق
| author=Parviz Jahed
| date=2011-02-25
| language=Persian}} 

Unlike already acclaimed Iranian exotic cinema which is mostly known in the West by its portrayal of rural life, nature landscapes, as well as socioeconomically deprived children struggling for survival in the outskirts of Tehran, the writer/director of My Tehran for Sale attempts to depict the middle-class people and the uncertainties in their every day urban life. My Tehran for Sale has a poetic language to address critical issues such as double life of young people, oppression of women, HIV, secret abortions, underground art, massive emigration, crisis of identity, people smuggling and also asylum seeker detention centers.

Some underground music are employed throughout the film presenting  a number of emerging alternative singers such as Mohsen Namjoo. The borders between documentary and fiction are seemingly dissolved in many scenes using a poetic language with a non-linear narrative and an open ending.

My Tehran For Sale is the winner of Independent Spirit Inside Film Awards 2009. {{cite news
| url=http://www.smh.com.au/news/entertainment/film/2009-inside-film-award-winners/2009/11/19/1258219913042.html
| title=2009 Inside Film award winners
| date=2009-11-19
| work=The Sydney Morning Herald}}  It won the jury award for best feature Film at the TriMedia Film Festival in 2010. {{cite web
| url=http://trimediafestival.org/2010-festival/2010-awards/the-2010-awards/
| title=The 2010 Awards
| author=Levi Brown
| publisher=trimediafestival.org
| archiveurl=http://wayback.archive.org/web/20120422091542/http://trimediafestival.org/2010-festival/2010-awards/the-2010-awards/
| archivedate=2012-04-22}}  Apart from various festivals and universities the film was screened at Museum of Modern Art in New York as well as Cinémathèque Française in Paris.

The film has been an official selection to Toronto International Film Festival (Sep 2009), {{cite web
| url=http://tiff.net/press/pressreleases/2009/wide-selection-of-festival-titles-available-for-acquisition
| title=2009 Press Releases: Wide Selection Of Festival Titles Available For Acquisition
| publisher=Toronto International Film Festival
| year=2009}}    Copenhagen International Film Festival 2010, Guadalajara International Film Festival Mexico, Sydney Travelling Film Festival, Las Palmas de Gran Canaria International Film Festival, Human Rights Arts and Film Festival 2010, Focus on Asia Fukuoka International Film Festival 2010, Global Lens USA 2010, Dialogue of Cultures Film Festival New York 2011 and SVAW Film Fesival Melbourne 2011, 
{{cite web
| url=http://www.amnesty.org.au/vic/event/27127/
| title=Stop Violence Against Women Film Festival
| date=November 2011
| publisher=Amnesty International Australia}}  etc.

Controversy surrounded the film when unauthorized copies of the film hit the black market in Tehran extensively. In July 2011, Iranian authorities arrested Vafamehr, reportedly for acting in the film without proper Islamic hijab and with a shaved head. She was sentenced to one year in prison and 90 lashes, however due to international pressures and various campaigns, an appeals court later reduced her sentence to only three months imprisonment. She was released in October 2011. {{cite news
| url=http://www.reuters.com/article/2011/10/28/idUS302409815520111028
| title=Iranian Actress Freed From Prison on Reduced Sentence
| last=Kenneally
| first=Tim
| date=2011-10-28
| publisher=Reuters
| accessdate=2013-01-11}} 

The irony of this incident is that the plot of the film is as follows:
"Marzieh is a young female actress living in Tehran. The authorities ban her theater work and, like so many young people in Iran, she is forced to lead a secret life in order to express herself artistically. At an underground rave, she meets Iranian born Saman, now an Australian citizen, who offers her a way out of her country and the possibility of living without fear."  

==References==
 

 
 
 
 
 