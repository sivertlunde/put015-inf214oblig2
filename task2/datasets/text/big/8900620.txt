Rio Conchos (film)
{{Infobox film
| name           = Rio Conchos
| image          = Poster of Rio Conchos (1964 film).jpg
| image_size     =
| alt            =
| caption        = Theatrical release poster
| producer       = David Weisbart Gordon Douglas
| writer         = Joseph Landon Clair Huffaker (novel)
| narrator       =
| starring       = Richard Boone Stuart Whitman Tony Franciosa Edmond OBrien Jim Brown
| music          = Jerry Goldsmith
| cinematography = Joseph MacDonald
| editing        = Joseph Silver
| studio         =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 107 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2,500,000 (US/ Canada) 
}}
 Western starring Richard Boone, Stuart Whitman, Tony Franciosa, Edmond OBrien, and in his motion picture debut, Jim Brown, based on Clair Huffakers novel of the same name.
 The Comancheros The Searchers.  The main female role, played by Wende Wagner in a black wig, has no English dialogue. Rio Conchos was filmed in Moab, Utah though the Conchos River and most of the action of the film takes place in Mexico.

Jerry Goldsmiths complete soundtrack was given a limited release on CD in January 2000 by Film Score Monthly that featured a tie-in title song by Johnny Desmond. 

On June 21, 2011, Shout! Factory released the film on DVD as part of a double feature with Take a Hard Ride (1975). 

==Plot== Confederate Army officer (Richard Boone) named Jim Lassiter, who has been out for revenge against Apache Indians who massacred his family, recovers a stolen U.S. Army repeating rifle from some Apaches he has killed; as the Apache have proven formidable with lesser weaponry, there is cause for concern should they become equipped with such superior firepower.

The U.S. Army arrests him, then offers Lassiter his freedom if he leads a small, clandestine scouting unit into Mexico consisting of an Army captain (Stuart Whitman), a Buffalo Soldier sergeant (Jim Brown), a knife-wielding Mexican prisoner (Tony Franciosa), and later an Apache woman warrior (Wende Wagner).

After blasting their way through bandits and Apaches, they discover Colonel Pardee, another former rebel soldier (Edmond OBrien), has set up a new Confederate headquarters, and is selling guns to the Apaches, including the ones who slaughtered Lassiters family.

The woman, who is called Sally, saves his life, so Lassiter puts aside his hatred. He and Franklyn sacrifice themselves to save Sally and the Army captain Haven while holding off Pardee and his men.

==Cast==
* Richard Boone as Maj. James Jim Lassiter
* Stuart Whitman as Capt. Haven
* Tony Franciosa as Juan Luis Rodriguez aka Juan Luis Martinez
* Edmond OBrien as Col. Theron Gray Fox Pardee
* Jim Brown as Sgt. Franklyn
* Wende Wagner as Sally (Apache girl)
* Warner Anderson as Col. Wagner
* Rodolfo Acosta as Bloodshirt (Apache chief)
* Barry Kelley as Croupier at Presidio
* Vito Scotti as Bandit chief
* House Peters, Jr. as Maj. Johnson
* Kevin Hagen as Major Johnson aka Blondebeard
* Robert Adler as Pardee Soldier (uncredited)
* Timothy Carey as Chico (cantina owner) (uncredited)
* Abel Fernandez as Mexican Guard (uncredited)
* Mickey Simpson as Bartender who refuses to serve Franklyn (uncredited)

==Billing== Key Largo, for which Humphrey Bogart had been listed first but Edward G. Robinson was placed in the middle of the three above-the-title leads with his name elevated higher than the other two (the third name being Lauren Bacalls).  In the case of Rio Conchos, Whitman was billed as Bogart had been, with Boone in Robinsons middle slot and Franciosa in Bacalls spot, with his name listed third going left to right and at the same height as Whitmans. Quite differently, however, Boone was billed before Whitman in the movie itself, with each name appearing onscreen one at a time.

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 