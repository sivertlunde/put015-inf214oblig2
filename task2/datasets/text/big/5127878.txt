Popcorn (2007 film)
{{Infobox film 
| name = Popcorn 
| image = Popcornmp.jpg
| caption = Promotional movie poster for the film 
| writer = Darren Fisher  Jack Ryder Jodi Albert Kate Maberly 
| director = Darren Fisher 
| fight director = David Goodall 
| producer = Daniel M. San, Rebecca Knapp 
| distributor = Moviehouse Entertainment
| released =  
| runtime = 90 minutes 
| country = United Kingdom
| language = English
 }}
Popcorn is a 2007 British teen comedy film written and directed by Darren Fisher. It was filmed in 2005 at one of Londons largest multiplex cinemas (Odeon Greenwich).

==Plot== Jack Ryder) takes a job at his local movie house where she works, only to learn his first day is her last. After his initial efforts to woo her fail, he resorts to drastic measures by enlisting the help of the chief projectionist, a man who no longer knows the difference between the real and the film worlds.

==Cast== Jack Ryder as Danny 
* Jodi Albert as Suki 
* Luke de Woolfson as Zak 
* Colette Brown as Florence 
* Andrew Lee Potts as Kris 
* Kate Maberly as Annie
* Laura Aikman as Jeannie
* Layke Anderson as Cool Guy 
* Sophie Anderton as Female Killer 
* Kacey Barnfield as Yukino Girl 
* Chike Chan as Lo
* Charlie Clements as  Horror Customer #2  Andrew Dunn as Max
* Charlotte Bellis Ferreira as Laura
* Oliver Ford as Annoying Customer
* Harrison Foss as Screamer #2 
* Gemma Gregory as Fit Girl 
* Ophelia Lovibond as Katerina
* Sonny Muslim as Screamer #1 
* Kavi Shastri as Carl
* Kate Loustau as Romantic Screen Woman  Lee Williams as Emil 
* Anthony OSullivan as Poster Boy #3 David Goodall as Violent Customer 
* Michael Luxton as Police Constable #1 
* Sarah McVicar as Inspector Davis 
* Luce Norris as Kissing Girl #1
* Tim Robinson  as Romantic Screen Man

==Soundtrack==
 Mohair
#Burn Hardwire
#Undertow Wade
#Love Is A Wonderful Thing - Performed by Sarah Lundbeck China Doll
#Time To Get It On - Performed by Mista Groove featuring Joneice Jamieson
#Round And Round - Performed by LJ Mohair
#Hypnotised - Performed by Solasso featuring Foster Child
#Number One Or Zero - Performed by Toffee

==Reviews==
A majority of the reviews received by this film have not been positive according to online sources. Matthew Leyland from BBC gives it two out of five stars, saying that "few of the jokes hit the target" and the "leads were dull." He acknowledges the films ambition, which uses "slapstick, sight gags, sound gags (movie dialogue finishing characters sentences) and manga-esque inserts." According to rottentomatoes.com, 34% of the audience liked the film, giving it an average rating of 2.8 out of five. Anna Smith from Empire Magazine also gave the film a negative review, rating it one out of five. She states that Popcorn is "a well-meaning British comedy that fails to deliver the laughs – or the romance." Paul Newmans blog review pointed to a positive aspect of the film, in that it doesnt follow the typical Hollywood format, but "feels more like an indy comedy."

==Manga style artwork==
The film makes use of manga-style artwork and comic strips by United Kingdom resident Original English-language manga artist Sonia Leong of Sweatdrop Studios. {{cite web
 | first = Paul
 | title = Sonia Leongs working on Popcorn; Hollywood & hentai
 | publisher = Anime UK News
 | date = January 30, 2007
 | url = http://forums.animeuknews.net/viewtopic.php?t=5941
 | accessdate = 2007-03-17 }}  The artwork is used as both posters in one of the characters rooms, and as illustrations in the movie plot themselves.

==References==
 
*   Popcorn. Rotten Tomatoes. Web. 15 April 2012.
* Smith, Anna.   Empireonline.com. Empire Magazine. Web. 15 April 2012.
* Leyland, Matthew.   BBC News. BBC, 23 Feb. 2007. Web. 15 April 2012.

==External links==
*  
* http://www.empireonline.com/reviews/review.asp?FID=134796
* http://www.bbc.co.uk/films/2007/02/26/popcorn_2007_review.shtml
*  

 
 
 
 
 
 