Make-Up (1937 film)
{{Infobox film
| name =  Make-Up
| image =
| image_size =
| caption =
| director = Alfred Zeisler
| producer = K.C. Alexander   C.M. Origo
| writer =  Hans Mahner-Mons  (novel)   Jeffrey Dell   Reginald Long   Ludwig von Wohl
| narrator =
| starring = Nils Asther   June Clyde   Judy Kelly   Lawrence Grossmith
| music =   Eric Cross   Philip Tannura
| editing = George Grace
| studio = Standard International 
| distributor = Associated British Film Distributors
| released = 29 February 1937
| runtime = 70 minutes
| country = United Kingdom English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} independent production company at Shepperton Studios. 

==Cast==
* Nils Asther as Bux  
* June Clyde as Joy  
* Judy Kelly as Marien Hutton 
* Lawrence Grossmith as Sir Edward Hutton  
* Jill Craigie as Tania  
* Kenne Duncan as Lorenzo  
*   Lawrence Anderson (actor)|  Lawrence Anderson as Goro
* Roddy Hughes as Mr. Greenswarter 
* Johnnie Schofield as Publicity Man   John Turnbull as Karo 
* Norma Varden as Hostess 
* Billy Wells as Ringmaster

==References==
 

==Bibliography==
* Low, Rachael. Filmmaking in 1930s Britain. George Allen & Unwin, 1985.
* Wood, Linda. British Films, 1927-1939. British Film Institute, 1986.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 