Klopka
{{Infobox film
| name           = Клопка   Klopka   The Trap
| image          = Klopka1.jpg
| alt            =  
| caption        = 
| director       = Srdan Golubović
| producer       = Jelena Mitrovic, Natasa Ninkovic, Alexander Ris, Laszlo Kantor
| writer         = Melina Pota Koljević Srđan Koljević Nenad Teofilović
| screenplay     = 
| story          = 
| based on       =  
| starring       = Nebojša Glogovac  Nataša Ninković Miki Manojlović Anica Dobra Bogdan Diklić Vuk Kostić Vojin Ćetković
| music          = Mario Schneider
| cinematography = Aleksandar Ilic
| editing        = Marko Glusac Dejan Urosevic
| studio         = 
| distributor    = New Yorker Video
| released       =    
| runtime        = 106 minutes
| country        = Serbia
| language       = Serbian
| budget         = 
| gross          = 
}}
Klopka ( : Клопка, Klopka) is a 2007 psychological thriller directed by Srdan Golubović, based on the novel of the same name, written by Nenad Teofilović.

The film is a neo-noir piece that explores the age old question of how far a parent is willing to go to help an ailing child. Simultaneously, it also deals with the issues and challenges faced by the people living in post-Slobodan Milošević|Milošević Serbian society.

==Plot==
 

===Intro===
The film opens with Mladen Pavlović (Nebojša Glogovac), sporting bumps and bruises on his face, nervously smoking a cigarette while talking to unrevealed individual(s). Among other things, he says that he is trying to "do this one thing right, after a series of wrongs that never should have happened".

The movie occasionally returns to the scene of Mladen talking to the unseen individual(s) and discussing different details following key plot points or displaying inner torment over the unfolding story.

===Story===
Mladen is a young professional residing in Belgrade where he works as construction engineer in a decrepit state-owned company thats undergoing the process of privatization. He drives a beat-up Renault 4 and rents an apartment with his wife Marija (Nataša Ninković) who teaches English in a primary school. Together theyre raising their only child—an 8-year-old boy named Nemanja (Marko Djurovic). Despite their limited means, theyre still managing to make ends meet and provide for their son. They arrange and lead a fairly normal and happy family life—cheering Nemanja on at swim meets and taking him to the local playground where Mladen becomes acquainted with their blonde neighbour (Anica Dobra) who also brings her daughter to play there.

However, everything drastically changes one day when Nemanja is rushed to the hospital following a collapse at gym class in school. After undergoing emergency advanced life support|reanimation, he is diagnosed with a heart muscle condition that requires immediate surgery since the next inflammation that could come at any time might be fatal. They are further informed by Dr. Lukić (Bogdan Diklić), that the procedure is only performed at a clinic in Berlin, Germany, costs  26,000 and is not covered by domestic health insurance plans.

Faced with this shocking development and the knowledge that they have nowhere near the money that is required for the surgery, Mladen and Marija look into different ways of coming up with the funds. Mladen applies for a bank loan, but gets flatly rejected due to not owning property and being employed at a bankrupt company. Marija submits an ad in the paper, asking for charitable donations for their sons surgery. When informed about it, Mladen confronts her on the issue as he is vehemently opposed to what she did. However, suspecting misplaced ego might be the source of his ire, she simply states that its not beneath her pride to ask for a handout in this situation. Although they quickly make up, it is obvious that the situation is starting to put a lot of strain on their marriage.
 Hotel Moskva. At the meeting, the dapper, a well-spoken middle-aged man (played by Miki Manojlović) says hes willing to pay €30,000, explaining that that should cover Nemanjas surgery and three plane tickets to Germany. Furthermore, he says that all he wants in return is for Mladen to murder someone, seeing him as the perfect candidate to carry out the crime, because he does not have any prior record and because he is an honest, hard-working man whom no one will suspect. Counting on Mladens dismayed initial reaction, the man tells him to think it over and says hell contact him in two days.

Coming back home, Marija is eager to hear how the meeting went, however Mladen doesnt mention the shocking offer he received, simply dismissing the man he met with as "some nutcase". Although very much tempted, at this stage Mladen hopes that he never gets a call from him again, and in search of money even looks up an old colleague from his university days (Vojin Ćetković). The friend quickly rejects Mladen, telling him hes doesnt have the money right now.

Two days later, as he announced, the mysterious man calls from a moving car, inquiring about the decision. A torn Mladen tells him unconvincingly that its a no-go. Sensing doubts in Mladens voice, the man tells him to give it more thought and informs him that everything needed to carry out the murder will be in a plastic bag placed in an electrical closet underneath Brankos Bridge, and tells him to pick it up at 7am the next morning. Lying in bed that night, tormented, Mladen attempts to get some input and advice from Marija by saying hes got something important to tell her while looking ready to finally clue her in on whats going on. However, following his long-winded introduction, she decisively interrupts him, thinking hes about to go into a whiny tale of whats bothering him in general. She further tells him to concentrate on Nemanja rather than on himself.

That morning, Mladen shows up at the bridge and finds the plastic bag containing a loaded hand-gun, a letter with instructions and a message that promises a cash advance will be in his building mail box the next day. The letter also gives Mladen a contact name Miloš Ilić along with a PO box number. While Mladen is reading the letter in his Renault, a white BMW is seen leaving the scene.
 Toyota Land Cruiser 100 series SUV to go to work in the morning. Along with a well dressed man, Mladen also spots a blonde woman and a little girl running to embrace Ivković - obviously his wife and daughter. Upon having a closer look, Mladen realizes that it is the blonde woman from the playground. While the little girl is the one that Nemanja plays and goes to school with. Deeply conflicted, Mladen goes to work where he takes out his frustration on the office equipment.

Soon, Nemanja has another heart episode and is rushed to the hospital once again, but this time the doctor wants him to stay for observation, repeating that the surgery needs to be done as soon as possible. The new development puts even more strain on Mladen and Marija, as they ponder their future course of action while taking turns being with Nemanja at the hospital during the day. Both realize that something needs to be done fast. Mladen is mulling over the preparations for the murder, none of which can be discussed with her, while Marija is growing impatient with what looks to her like his lack of action and answers.

Finally, late one night while Marija is at the hospital watching over Nemanja, Mladen drinks a glass of water, prepares the hand- gun, and goes out into the night. At the same time while he is waiting in front of Ivkovićs apartment, Nemanja gets another attack and a horrified Marija calls the nurse for help. Meanwhile, Ivkovićs SUV pulls up, he exits, Mladen approaches and following a short verbal exchange fires a succession of bullets at Ivković, killing him instantly. Simultaneously, Nemanja is fighting for his life as his pulse drops, but the doctors somehow manage to stabilize his condition. After the murder, a distraught Mladen is back home where he hides the gun and compulsively washes his clothes clean of powder residue. Emotionally drained, Marija also arrives home and gets very angry to see Mladen after the rough night she endured. Perplexed and completely disconnected from each other, theyre barely on speaking terms. She finally implores him to say and do something in order to start dealing with this situation, but distracted and overwrought with guilt over what he did, he just tells her to move away from him. From that night they start sleeping in separate beds.

During Petars funeral, Mladen watches the procession from a distance and sees the man who hired him giving condolences to Petars widow Jelena. Petars brother (played by Vuk Kostić) gives an emotional and impassioned speech vowing to find the killers and get revenge.

Mladen next wants to collect the rest of the money agreed upon after the murder, but has troubles reaching the man that had promised it to him. He also sends a letter to the contact PO box, but finds out that it doesnt even exist. Mladen realizes he was used by the man, about whom he only knows one thing: that he knew the late Ivković. To that end, Mladen starts following Ivkovićs widow, hoping to get some leads. However, he only finds her collapsing on a park bench while taking her daughter out to play. Mladen takes her to the hospital, and, after coming to, she tells him that she took a little more sedatives than usual and that she didnt want to kill herself despite the fact that even the doctors dont believe her. She finds comfort in talking to someone who is not from her late husbands "business" milieu. She tells him she is aware that shes receiving phony condolences from many of her late husbands friends and is convinced the murderer came from there. She also lets on that she knows her husband to have been involved in all kinds of dodgy stuff by saying that all of them knew his one side, but adds that he was the love of her life and was wonderful to her and her two daughters. Deeply conflicted and torn, Mladen is visibly troubled with the fact that hes listening and comforting the woman whose husband he just killed. While shes thanking him for all hes done for her, his conscience cant take it anymore and he excuses himself and quickly leaves.
 Mercedes with a large rock. After getting beaten, he is taken to a police-station where he spends the night in custody. The next day, when questioned about the incident with the youngsters, he suddenly admits to killing Petar Ivković and tells the inspector (Milorad Mandić) every single detail of how it went down. The Inspector, however, claims to not believe a word of it and sends Mladen home while admonishing him for "wasting valuable police time on nonsense".

Mladen comes home where Marija has had just about enough of his mysterious and days-long disappearances, which she sees as his failure to deal with the situation properly and even abandonment of his family. She confronts him verbally for withdrawing inwards, calling him a "good-for-nothing weakling". In the middle of her rant, he cant take it any longer and slaps her across the face.

Later, Mladen gets a call from the man who hired him, who threatens that he will kill him and Nemanja if he keeps talking about his deed. After hearing a gypsy speaking on the other end of the phone, Mladen realizes where his antagonist is and rushes into his car and starts following the mans silver BMW. After following the man to his home, a massive compound on the outskirts of Belgrade, Mladen realizes what needs to be done. He returns to his small apartment and takes his hand-gun, loads it and heads to the compound. Mladen creeps inside and deliberately activates the BMWs security system. When the man comes outside to see why it is chirping, Mladen appears, pointing a pistol at him and asking for his money. The man confesses that he is indebted to over  500,000 and begs of Mladen to kill him, claiming that he would rather get shot in the forehead than chopped to pieces by the thugs that he is indebted to. Mladen leaves him and sits by the swimming-pool, where he gets a call from his wife, who says that somebody put   30,000 in their bank account.

Afterwards, Mladen goes to Petars wife and tells her that he killed her husband (this is revealed to be the scene from the beginning of the film in which he is seen speaking to an unidentified individual). He offers her the opportunity to kill him with the same gun he used to kill Petar. She declines and instructs him to leave, which Mladen does, leaving the gun behind on the table. As he leaves he sees Petars brother approach the home with Petars daughter, who waves to Mladen. Mladen waves back and gets in his car and stops at a red light, remaining still even once the light turns green (anticipating that Petars widow will inform Petars brother of the story, and that Petars brother will make good on his vow to avenge Petars death). Eventually a black SUV pulls up and shoots Mladen in his car (the shooter presumed to be Petars brother). Whether Mladen is dead or just injured is left ambiguous as the film ends.

==Themes and motifs==
Underneath its simple noir narrative, some of the movies key points lie in spotlighting what the author sees to be the staples of post-Milošević Serbian society such as the huge gap between the rich and the poor, an entire nouveau riche class that managed to gain enormous wealth through shady means in the years since the collapse of communism, a middle class that is teetering on the edge of poverty, patriotism being used to cover up criminal activities, workers and clerks at the mercy of ruthless foreign-based businesses that negotiated their entry into the shattered Serbian economy from an overwhelming position of strength, etc. 

*When Mladen applies for a loan at a foreign-owned bank, he is flatly rejected and the news is broken to him by a smiling bank employee. When asked by an angry Mladen why he is so cheerful, the clerk suddenly turns embarrassed and informs him somewhat dejectedly that the bank penalises its employees if theyre not smiling while providing financial services.
*The mysterious man who offers the money for Nemanjas surgery, on condition that Mladen kill Petar Ivković, introduces himself as representing "those of us who love this country, who build and create" - a clear dig by the filmmakers at the usage of patriotism in Serbia in order to initiate and carry out criminal endeavours through state security echelons, shady businessmen, and common criminals.
*In search of the money required for sons surgery, Mladen looks up an old colleague from university days who is now doing well financially. Before rejecting Mladen, the friend sings praises of the business hes currently involved with – designing kitschy suburban palaces for the nouveau riche despite not even being an architect.
*After she puts an ad in the paper asking for donations towards Nemanjas surgery, Marija is faced with the petulant female student who after not heeding the teachers request to put away her handheld during class, flippantly asks Marija in front of everybody to give her private English lessons. The young students reasoning is that Marija "could use the money considering her home situation". Deeply shaken, Marija throws the student out of the class, but eventually swallows her pride and after Nemanjas third inflammation goes to the students lavish home in order to give her private lessons in search of supplemental income or even hoping to get a loan from her rich parents, only to be shocked and sickened by the fact that the students father owns a picture frame that costs €30,000. All of this leads to a long tirade when she comes home to Mladen that "her sons life is worth less than those peoples picture frame".

==Reception==

===2007 Berlinale===
Despite being shown outside of the main program, Klopka garnered quite a buzz at the 57th Berlin International Film Festival, culminating in Hollywood Reporter including it in the list of 10 most important films of that years festival.  Varietys writer Deborah Young also wrote a positive piece. 

===Serbia===
In its home country, Klopkas reviews were mixed. Popboks webmagazine calls it a movie whose great potential wasnt properly utilized. Its reviewer Miloš Cvetković praises the depiction of the father characters introspectiveness and emotional withdrawal following the murder, but criticizes the filmmakers tendency to raise this familys personal drama into a general societal symbolic realm without first establishing them enough as specific people with specific individual identites. Because of that, Cvetković says, it feels like the point of no return is reached far too soon without properly exploring other options and as such feels like it wasnt the absolute last resort for the father. 

The movie became the official Serbian entry for the Best Foreign Movie category at the 2008 Academy Awards. It made the shortlist of 9 films,  but failed to get into the final five.

===Other festivals===
Success at Berlinale opened the doors of various festivals across the globe. After showing at the 2008 Portland International Film Festival, the movie was reviewed by website Cinematical. 

==Cast==

* Nebojša Glogovac ... Mladen Pavlović
* Nataša Ninković ... Marija Pavlović
* Marko Đurović ... Nemanja Pavlović
* Miki Manojlović ... Kosta Antić
* Anica Dobra ... Jelena Ivković
* Bogdan Diklić ... Dr. Lukić
* Vuk Kostić ... Petars Brother
* Vojin Ćetković ... Vlada, Mladens friend from the university
* Dejan Čukić ... Petar Ivković
* Nebojša Iliš ... Smiling bank clerk
* Boris Isaković ... Moma
* Ivana Jovanović ... Momas wife
* Milorad Mandić ... Inspektor
* Ana Marković ... Post office clerk
* Mladen Nelević ... Radnik

==Hollywood remake==
Ever since its favourable showing at the 2007 Berlinale, as well as affirmative notices in Hollywood Reporter and Variety that followed, the sale of Klopka rights to a Hollywood studio was mentioned by various press outlets.  In April 2008, Hollywood production company Alcon Entertainment closely affiliated with Warner Bros. bought an option to remake the movie within 18 months of the purchase. 

Finally, in early November 2008, it was announced that after buying the rights to Klopka, Alcon Entertainment gave a green light for the remake to be directed by Ericson Core based on an adapted screenplay yet to be written by Matthew Aldrich.  
 Toronto International Film Festival, it was announced that the films rights have been purchased by the production company Emmett/Furla/Oasis Films|Emmett/Furla Films headed by Randall Emmett and George Furla. The movie also got a working title Fair Trade with the producers reportedly offering the main role to Liam Neeson. The main plotline was also modified for the American audience. Warren is down on his luck and works as a limo driver, despite having a masters degree. When his son has a seizure and needs surgery, he learns that the insurance company won’t cover it. With no money, a mysterious man approaches with a deal: he’ll pay for the child’s surgery if the driver kills a mobster. Warren at first refuses, but reconsiders once he realizes his child’s life depends on it. 

== See also ==
*List of most expensive non-english language films

==References==
 

==External links==
*  
*   at - Filmovi 24/7

 
 
 
 
 
 
 
 