Hit So Hard
{{Infobox film
| name           = Hit So Hard
| image          = hitsohardposter.jpg
| alt            = Patty Schemel is the subject of this documentary
| caption        = Festival poster
| director       = P. David Ebersole
| producer       = Todd Hughes   Christina Soletti 
| editing        = P. David Ebersole
| music          = Roddy Bottum
| cinematography = John Tanzer   Larra Anderson  Mark Putnam
| studio         = The Ebersole Hughes Company   Tight Ship Productions
| distributor    = Well Go USA   Variance Films
| released       =  
| runtime        = 103 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}

Hit So Hard is a 2011 American documentary film directed by P. David Ebersole. The film details the life and near death story of Patty Schemel, drummer of the seminal 90s alternative rock band Hole (band)|Hole, and charts her early life, music career, and spiral into crack cocaine addiction.  The film weaves together Hi8 video footage Schemel recorded while on Holes 1994-95 world tour with contemporary interviews with her, bandmates Courtney Love, Eric Erlandson, and Melissa Auf der Maur, as well as her family members. The film also features interviews with other female drummers and musicians, including Nina Gordon, Kate Schellenbach, Gina Schock, Debbi Peterson, and Phranc. 

The films score is written by Roddy Bottum of Faith No More. It was produced by Todd Hughes and Christina Soletti and was released theatrically in North America in 2012 by Well Go USA via Variance Films.  It also screened a series of film festivals, including South by Southwest, the Marché du Film at Cannes Film Festival|Cannes, the Seattle Gay and Lesbian Film Festival, The Portland Queer Documentary Film Festival, and Outfest. The title is a reference to a song on Holes 1998 album Celebrity Skin.

==Background==
Hit So Hard was conceived after Schemel discovered a cache of Hi8 video footage which she had recorded herself during her world tour with Hole promoting their second album, Live Through This (1994). In the process of digitizing the footage to preserve it from deterioration, Schemel and Ebersole sought to utilize the footage in order to tell Schemels story. 

==Synopsis==
The film begins with discussion amidst Holes 1994 and 1995 world tour, then works backwards to Schemels childhood growing up in Marysville, Washington, and details her coming out to her family as a lesbian, as well as her immersion in Seattles music scenes, where she would eventually cross paths with Kurt Cobain. Through contemporary interviews with Schemels bandmates Courtney Love, Eric Erlandson, and Melissa Auf der Maur, her beginnings in Hole are detailed, including her audition with Love and Erlandson in Los Angeles amidst the Rodney King riots, as well as her time living with Love and husband Kurt Cobain, and the songwriting process between Love, Schemel, and Erlandson. Additional commentary from fellow female drummers, musicians, peers, and friends of Schemels are provided throughout. After the death of Holes bassist Kristen Pfaff in 1994 (only two months after the suicide of Kurt Cobain), the band embarked on a world tour with Auf der Maur as Pfaffs replacement, and Schemel, along with Love, began heavily using heroin. Schemels drug use leads to a breakup with her girlfriend, who acted as Loves personal assistant on the tour, and Schemel reflects on her time in a rehabilitation facility she checked into with Love after the conclusion of the tour in 1995.
 dog walking business. The film concludes with Schemel teaching drum lessons in her spare time.

==Cast==
 
* Patty Schemel 
* Courtney Love  (of Hole (band)|Hole) 
* Eric Erlandson  (of Hole) 
* Melissa Auf der Maur  (of Hole)  Veruca Salt) 
* Kate Schellenbach  (of Luscious Jackson) 
* Gina Schock  (of The Go-Gos) 
* Alice de Buhr  (of Fanny (band)|Fanny) 
* Debbi Peterson  (of The Bangles) 
* Izzy  (of Care Bears on Fire) 
* Phranc
* Roddy Bottum  (of Faith No More)  Dallas Taylor
* Sarah Vowell
* Chris Whitemyer
* Larry Schemel  (of Midnight Movies) 
* Terry Schemel 
* Joe Mama-Nitzberg
* Kristen Pfaff  (archival footage) 
* Kurt Cobain  (archival footage) 
* Evan Dando  (archival footage) 
* Dexter Holland  (archival footage) 
* Leslie Mah  (archival footage) 
* Sheri Ozeki  (archival footage) 
* Fred Schneider  (archival footage) 
* Jenny Shimizu  (archival footage) 
* Pat Smear  (archival footage) 
* Metallica  (archival footage) 
 

==Release==
*  Hit So Hard opened at New York Citys Cinema Village on April 13, 2012 before playing in theaters in a limited release around the United States and Canada, via WellGo USA and Variance Films  .  King Records : it opened in Tokyo at Theater N on April 28, 2012.
*  Hit So Hard was released theatrically in the UK via   on November 16, 2012.

==Accolades==
* 2011: Opening Night Film at Beat Film Festival in Moscow
* 2011: Won Jury Award for Best Documentary (Honorable Mention) at Frameline Film Festival
* 2011: Documentary Centerpiece Gala at Outfest
* 2011: Closing Night Film at Birmingham Shout at the Sidewalk Moving Picture Festival
* 2011: Won Audience Award - Sight & Soundtrack at Philadelphia Film Festival
*2012: Nominated for Outstanding Documentary for the 24th GLAAD Media Awards


==References==
 

==External links==
 
*  
*  

 

 
 
 
 
 
 
 