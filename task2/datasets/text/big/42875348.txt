The Initiation of Sarah
{{Infobox television film
| name           = The Initiation of Sarah
| image          = File:The Initiation of Sarah.jpg
| alt            = 
| caption        = DVD cover Robert Day
| producer       = Chuck Fries
| screenplay     = Don Ingalls Carol Saraceno Kenette Gfeller Tom Holland Carol Saraceno
| starring       = Kay Lenz, Shelley Winters, Tony Bill, Morgan Fairchild Johnny Harris
| cinematography = Ric Waite
| editing        = Anthony DiMarco
| studio         = Charles Fries Productions, Stonehenge Productions ABC
| first_aired    =  
| distributor    = Shout! Factory 
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} Robert Day. ABC on February 6, 1978 and starred Kay Lenz as a shy, withdrawn young woman that discovers that she has psychic powers after joining a sorority. The film achieved some controversy upon its initial airing on television as part of the films plot involved Morgan Fairchild wearing a wet t-shirt after being thrown into a fountain, something that had not been previously shown in a made for TV movie. 

The film was later The Initiation of Sarah (2006 film)|re-made in 2006 for ABC Family and Fairchild returned to portray the mother of the titular Sarah and her sister, who was re-named Lindsay.    Fairchild initially regretted portraying Jennifer in the 1978 film but later changed her mind after producer Chuck Fries stated that while it was easy to find an ingenue (stock character)|ingenue, it was difficult to find someone who could play a convincing "bitch".  

==Synopsis==
The film opens with Sarah (Kay Lenz), a painfully shy young woman, and her beautiful sister Patty (Morgan Brittany) playing on the beach at sunset. Patty wanders off to play in the surf with a young man, only for him to try to force himself on her. After a moment the man recoils and it is implied that Sarah has used telekinesis to save Patty. The film then cuts to the two sisters driving to college. On the way there Patty and Sarah discuss plans for the two of them to join Alpha Nu Sigma (ANS) as their mother was a member of the sorority. Sarah points out that she (Sarah) is adopted and that only Patty is their mothers only biological child, as she was adopted shortly after she was born.

Once at the campus the two girls attend a rush week party at ANS. Patty is instantly welcomed into the sororitys clique while Sarah is directed to the nearby Phi Epsilon Delta (PED) house, as they do not view her as a potential candidate. The sisters both go to the PED house, where they are met with a general lack of enthusiasm from almost all of the girls other than Mouse (Tisa Farrow), an awkward and shy young woman who is instantly drawn to Sarah. After rush week is over Patty is overjoyed to get into ANS, but is somewhat hurt when she discovers that not only did Sarah not get in, but that ANS president Jennifer (Morgan Fairchild) insists that Patty not speak to Sarah, as she was accepted into PED. This causes friction between the two sisters, as Sarah is frustrated at Pattys reluctant willingness to follow Jennifers orders.

As the semester progresses Sarah begins to strike up a relationship of sorts with Paul (Tony Bill), a teaching assistant for her psychology classes, and becomes somewhat closer to Mouse. Shes unnerved by the PEDs den mother Mrs. Hunter (Shelley Winters), who recognizes that Sarah has special powers and insists that she use them to lead PED to glory against the ANS. Patty continually tries to find ways to talk to Sarah but is repeatedly unsuccessful and Jennifers taunting ultimately leads Sarah to use her powers to push Jennifer into a fountain. Feeling vindicated by the encounter, Sarah begins to open up to the idea of her powers but does not fully embrace them until Jennifer orchestrates a cruel prank against Sarah where she is pelted with rotten food, eggs, and mud. While the prank is successful in humiliating Sarah, it finally pushes Patty to leave Jennifers sorority. Angry, Sarah rebuffs Pauls attempts to persuade her that Mrs. Hunter is evil and that she should leave the sorority, ultimately deciding to hold an initiation ceremony with Mrs. Hunter.

On the night of the ceremony Sarah and the other PEDs eagerly follow Mrs. Hunters lead. Sarah notes that Mouse is not at the ceremony but is pacified by Mrs. Hunters assurances that Mouse will show up at the final portion of the nights events. As the group is led through the ceremony, the ANS are also going through their own initiation rites. At Mrs. Hunters prompting Sarah uses her powers to disrupt the ceremony and cause Jennifers face to permanently warp into a horrific visage. However when Sarah discovers that the ceremony will end with Mouse getting sacrificed, she uses her powers to stop the ceremony but ends up burning herself and Mrs. Hunter alive. The film ends with Patty deciding to join PED at Mouses insistence and the two sorrowfully gaze at a picture of Patty and Sarah in happier times.

==Cast==
*Kay Lenz as Sarah Goodwin
*Shelley Winters as Mrs. Erica Hunter
*Tony Bill as Paul Yates Kathryn Grant as Mrs. Goodwin
*Morgan Fairchild as Jennifer Lawrence
*Morgan Brittany as Patty Goodwin
*Robert Hays as Scott Rafferty
*Tisa Farrow as "Mouse"
*Elizabeth Stack as  ONeil
*Deborah Ryan as Bobbie Adams
*Nora Heflin as Barbara
*Talia Balsam as Allison
*Michael Talbott as Freddie
*Jennifer Gay as Kathy Anderson
*Susan Duvall as Regina Wilson

==Reception==
Critical reception for the DVD release of The Initiation of Sarah has been mixed to positive.  Most of the reviews shared the opinion that while the film was cheesy, they enjoyed the acting.   Multiple reviewers also noted its similarity to the 1976 film Carrie (1976 film)|Carrie, which also dealt with the subject of a female outcast that discovers telekinetic powers, with comparing it unfavorably to the earlier film while still maintaining that The Initiation of Sarah was overall enjoyable.  

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 