La Traviata (1983 film)
{{Infobox film
| name           = La Traviata
| image          = LaTraviataPoster.jpg
| image_size     = 200px
| caption        = Film poster
| director       = Franco Zeffirelli
| based on       =  
| writer         = Franco Zeffirelli Based on the libretto by Francesco Maria Piave
| starring       = Plácido Domingo Teresa Stratas
| producer       = Tarak Ben Ammar
| music          = Giuseppe Verdi 
| cinematography =  Peter Taylor Universal Classics
| released       =  
| country        = Italy
| runtime        = 105 minutes Italian
}}

La Traviata is a 1982 Italian film written, designed, and directed by Franco Zeffirelli. It is based on the opera La traviata with music by Giuseppe Verdi and libretto by Francesco Maria Piave. The film actually premiered in Italy in 1982, then went into general release there in 1983. It opened in theatres in the U.S. on April 22, 1983.

==Plot== flashback we are transported to a lavish party she is hosting to celebrate her recovery from an illness. One of her guests, Count Gastone, has brought with him his friend, the young nobleman Alfredo Germont, who has long adored Violetta from afar. She becomes dizzy and retires to her bedroom to recover; he follows her and declares his love. At first Violetta rejects him, telling him love means nothing to her, but she is touched by his concern and offers him a camellia, telling him to return it when it has wilted. He promises to see her the next day.

Three months pass, and Violetta and Alfredo are living together in a peaceful country house outside Paris. She has fallen deeply in love with him and has abandoned her former life. When Alfredo learns from the maid Annina her mistress has ordered her to sell the horses, carriages, and everything owned by Violetta in order to finance their lavish lifestyle, Alfredo leaves for Paris immediately to handle matters himself.

During his absence, his father Giorgio arrives and requests that, for the sake of his family, Violetta end her relationship with his son. Violettas unsavory reputation has threatened his daughters engagement. After declaring her love for Alfredo, she reluctantly agrees to Giorgios demand. He kisses her forehead in a gesture of gratitude for her sacrifice and leaves.

Violetta is in the midst of writing a letter of farewell to Alfredo when he enters. She tells him repeatedly of her unconditional love before rushing out and handing the letter to her servant to give to Alfredo. Giorgio returns and attempts to comfort his son, who suspects Barone Douphol is the real reason Violetta left. When he finds an invitation from Flora Bervoix on the desk, he decides to confront Violetta at the party.

Violetta arrives with Baron Douphol, who challenges Alfredo at the gambling table. Alfredo wins large sums of money, and when everyone goes into the dining room for supper, Violetta - fearful the Barons anger at losing will lead him to challenge Alfredo to a duel - asks Alfredo to leave. He misunderstands her apprehension and demands she admit she loves Douphol. When she does, an infuriated Alfredo humiliates and denounces her in front of the other guests, then throws his winnings at her for the "services" she performed for him while they lived together.
Giorgio, having arrived in search of his son, witnesses the scene and castigates Alfredo for his boorish behavior. Flora and the ladies try to escort Violetta from the room but, before departing, she turns to him and tells him he cannot fathom the love she has for him within her heart.

Back in Violettas home, Dr. Grenvil advises Annina her mistress tuberculosis has worsened and she doesnt have long to live. Giorgio sends Violetta a letter telling her he has informed Alfredo of the sacrifice she made for him and his sister, and that he is sending his son to see her. Alfredo arrives and suggests they leave Paris, but Violetta knows the end is near. Giorgio rushes in and begs her forgiveness. Violetta presents Alfredo with a locket with her picture and asks him to remember her, while at the same time urging him to marry if he falls in love again. "How strange," she mumurs, "the spasms have stopped." With a peaceful smile on her face, she lifts herself from the bed and reaches to the daylight. "I am returning to life!," she cries, then falls to the floor.

==Production notes== extras are Charles Anthony, Ferruccio Furlanetto, and Russell Christopher.

The film grossed $3,594,000 in the US. 

==Principal cast==
*Teresa Stratas ..... Violetta Valery 
*Plácido Domingo ..... Alfredo Germont 
*Cornell MacNeil ..... Giorgio Germont 
*Allan Monk ..... Baron Douphol 
*Pina Cei ..... Annina 
*Axelle Gall ..... Flora Bervoix 
*Maurizio Barbacini ..... Gastone

==Principal production credits== Producer ..... Tarak Ben Ammar
*Cinematography ..... Ennio Guarnieri Production Design ..... Franco Zeffirelli, Gianni Quaranta
*Costume Design ..... Piero Tosi Musical Director ..... James Levine
*Choreography ..... Alberto Testa

==Critical reception==
In his review in the New York Times, Vincent Canby called the film a "triumph" and "dazzling" and added, "  never has the manner of something scaled down or souped up for a mass audience, though I suspect it will be immensely popular anyway. Verdis genius will out, especially when presented with the talent, intelligence and style that have gone into this production . . . Miss Stratas not only sings magnificently but she also looks the role . . .   is a screen presence as riveting to watch as to listen to. Its an acting performance of breathtaking intensity . . . La Traviata benefits from Mr. Zeffirellis talents as a designer as much as from his gifts as a director. The physical production is lush without being fussy. Nor is it ever overwhelming. This possibly is because at key moments we are always aware of details that, however realistic, remind us that what we are witnessing is not life but a grand theatrical experience. Its not to be missed."  

==Awards and nominations==
*Academy Award for Best Art Direction (Franco Zeffirelli, Gianni Quaranta - nominees) Academy Award for Best Costume Design (nominee)
*Golden Globe Award for Best Foreign Language Film (nominee) BAFTA Award for Best Foreign Language Film (nominee)
*BAFTA Award for Best Costume Design (winner)
*BAFTA Award for Best Production Design (winner)
*Nastro dArgento for Best Costume Design (winner)
*Nastro dArgento for Best Production Design (winner)
*Nastro dArgento for Best Cinematography (winner)

==References==

 

==External links==
 
* 
*   at  

 
 

 
 
 
 
 
 
 
 
 