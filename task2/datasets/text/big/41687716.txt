Tough Assignment
{{Infobox film
| name =  Tough Assignment 
| image =
| image_size =
| caption =
| director = William Beaudine
| producer = Carl K. Hittleman 
| writer = Miltan Luban    Carl K. Hittleman 
| narrator = Steve Brodie   Marc Lawrence
| music = Albert Glasser  
| cinematography = Benjamin H. Kline 
| editing = Harry W. Gerstad  
| studio = Donald Barry Productions 
| distributor = Lippert Pictures
| released = November 15, 1949
| runtime = 64 minutes
| country = United States English 
| budget =
| gross =
| preceded_by =
| followed_by =
| website =
}} Steve Brodie. It is regarded as a film noir.  A Los Angeles reporter and his photographer wife investigate a criminal gang trying to establish a cartel over beef supplies to the citys butcher shops.

==Cast==
* Don Barry as Dan Reilly
* Marjorie Steele as Margie Reilly  Steve Brodie as Boss Morgan 
* Marc Lawrence as Vince 
* Ben Welden as Sniffy 
* Sid Melton as Herman 
* John L. Cason as Joe Frank Richards as Steve
* Fred Kohler Jr. as Grant, Head Rancher  Michael Whalen as Hutchison 
* Edit Angold as Mrs. Schultz 
* Leander De Cordova as Mr. Schultz 
* Stanley Andrews as Chief Investigator Patterson 
* Stanley Price as Al Foster 
* Iris Adrian as Gloria Hugh Simpson as Ted 
* Gayle Kellogg as Jack Lowery

==References==
 

==Bibliography==
* Spicer, Andrew. Historical Dictionary of Film Noir. Scarecrow Press, 2010.
* Marshall, Wendy L. William Beaudine: From Silents to Television. Scarecrow Press, 2005.

==External links==
* 

 

 
 
 
 
 
 
 
 
 

 