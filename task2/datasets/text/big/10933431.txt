Subha Sankalpam
{{Infobox film 
  | name = Subhasankalpam
  | image = Subhasankalpamfilm.png
  | caption = Promotional Poster
  | director = K. Viswanath
  | producer = S. P. Balasubrahmanyam
  | writer = Gollapudi Maruti Rao (dialogues)
  | screenplay = K. Viswanath
  | story = K. Viswanath
  | starring =Kamal Haasan Aamani Priya Raman
  | music = M. M. Keeravani
  | cinematography = P. C. Sreeram
  | editing = G. G. Krishna Rao
  | studio = Sri Kodandapani Film Circuit
  | distributor =
  | released = 13 July 1995
  | runtime =
  | language =  Telugu
  | country = India
  | budget =
  | preceded_by =
  | followed_by =
    }}
 1995  Indian feature directed by K. Vishwanath and produced by noted singer, S. P. Balasubrahmanyam. It features Kamal Haasan, Aamani and Priya Raman in the leading roles, with Vishwanath also playing a pivotal role. P. C. Sreeram handled the camera while M. M. Keeravani composed the music for the film, which became a blockbuster.

==Cast==
*Kamal Haasan as  Dasu
*K. Vishwanath as  Raayudu
*Aamani as  Ganga
*Priya Raman as  Sandhya
*Kota Srinivasa Rao Marutirao Gollapudi as  Chennakesava Rao Rallapalli as  Bheema Raju
*Sakshi Ranga Rao Mallikharjunarao
*Srilakshmi
*Vaishnavi

==Production==
Kamal Haasan, co-producer for the film, persuaded and succeeded in getting K. Vishwanath to make his debut as an actor for the film. Kamal Haasan revealed that the character required a renowned person to play the man who he bowed to, and if it was any other person, scenes would have had to be used to establish his importance. This film was produced by acclaimed singer S.P.Balasubrahmanyam. 

==Awards ==
* Filmfare Best Film Award (Telugu) - S. P. Balasubrahmanyam
* Filmfare Best Director Award (Telugu) - K. Viswanath
* Filmfare Best Music Director Award (Telugu) - M.M. Keeravani

==Release==
The film won five Nandi Awards in 1995 with Aamani winning the Nandi Award for Best Actress for her performance, while S. P. Sailaja won the Nandi Award for Best Female Playback Singer.  Furthermore Vishwanth won the Nandi Award for Best Character Actor, Krishna Rao won the Nandi Award for Best Editor and Vaisnavi secured the Nandi Award for Best Co-actress. 

The film was later dubbed into Tamil language|Tamil-language as Paasavalai.

==References==
 

==External links==
*  

 

  
 
 
 
 
 
 


 