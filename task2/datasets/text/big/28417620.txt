Innocents with Dirty Hands
{{Infobox film
| name           = Innocents with Dirty Hands
| image          = Les innocents aux mains sales.jpg
| image_size     = 
| alt            = 
| caption        = Film poster
| director       = Claude Chabrol
| producer       = André Génovès
| writer         = Claude Chabrol
| based on  =   Richard Neely
| narrator       = 
| starring       = Romy Schneider Rod Steiger
| music          = Pierre Jansen
| cinematography = Jean Rabier
| editing        = Jacques Gaillard
| studio         = Jupiter Generale Cinematografica Les Films de la Boétie Terra-Filmkunst
| distributor    = New Line Cinema
| released       = 26 March 1975 (France) 3 November 1976 (USA)
| runtime        = 121 minutes
| country        = France Italy West Germany
| language       =
| budget         = 
| gross          =
| preceded_by    = 
| followed_by    = 
}} 1975 film written and directed by Claude Chabrol. Its title in French is Les innocents aux mains sales. The film was based on the novel The Damned Innocents by Richard Neely. The film had 553,910 admissions in France.   

==Plot==
Louis, a rich man who lives quietly with his beautiful young wife Julie at St Tropez, has a cardiac probem and an alcohol problem. She sleeps in another room and when she meets Jeff, a writer, starts an affair with him. The two decide that she will kill her husband and that Jeff, after dumping the body off a boat, will then lie low in Italy. To her dismay, Jeff dispppears and with him all Louis money. She is left without husband, lover or assets, under police surveillance. Then Louis reappears, alive, fit, alcohol-free and ready to forgive her …..

==Principal cast==
{| class="wikitable" style="width:50%;"
|- "
! Actor !! Role
|-
| Romy Schneider || Julie Wormser
|-
| Rod Steiger|| Louis Wormser
|-
| Jean Rochefort || Maître Albert Légal
|-
| François Maistre || Commissaire Lamy
|-
| Hans Christian Blech || Le juge 
|-
| François Perrot || Georges Thorent
|-
| Paolo Giusti || Jeff Marle 
|-
| Henri Attal || Police Officer #1
|-
| Dominique Zardi || Police Officer #2
|}

==Critical reception==
Vincent Canby of The New York Times did not care for the film:
 

==References==
 

== External links ==
* 
* 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 