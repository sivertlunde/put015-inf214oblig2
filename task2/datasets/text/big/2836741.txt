Queen at Wembley
 
 
{{Infobox film
| name           = Queen at Wembley
| image          =
| caption        = Gavin Taylor
| producer       = Simon Lupton Rhys Thomas Roger Taylor John Deacon Spike Edney Queen
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 300 min
| language       = English
| budget         =
}}
 Original Wembley Magic Tour. It was first released in December 1990 as an edited VHS (missing 9 songs), then as an audio CD in 1992, followed by a DVD release as Queen: Live at Wembley Stadium (in its entirety) to coincide with the CD rerelease in 2003. The DVD has gone five times platinum in the United States, four times platinum in the United Kingdom, and achieved multi platinum status around the world. On 5 September 2011, the 25th Anniversary Edition of the concert was released as a standard 2-DVD set and a deluxe 2-DVD and 2-CD set. Eagle Rock Entertainment announced that the 25th Anniversary Edition would be released in the USA and Canada on 12 March 2013.  
A special edition of DAT tape can be found. Recorded and distributed by W.L.S.T. in Europe under license from EMI / Parlophone. The track list is the same on both CDs. W.L.S.T. used sony pdp 125 and registered 500 tapes.

==Disc one==
# One Vision
# Tie Your Mother Down
# In the Lap of the Gods...Revisited
# Seven Seas of Rhye Tear It Up A Kind of Magic
# Under Pressure
# Another One Bites the Dust
# Who Wants to Live Forever
# I Want to Break Free
# Impromptu Brighton Rock Solo
# Now Im Here Love of My Life
# Is This the World We Created?
# (Youre So Square) Baby I Dont Care
# Hello Mary Lou Tutti Frutti
# Gimme Some Lovin
# Bohemian Rhapsody
# Hammer to Fall
# Crazy Little Thing Called Love
# Big Spender
# Radio Ga Ga
# We Will Rock You
# Friends Will Be Friends
# We Are the Champions God Save the Queen

==Disc two==
 

===Road to Wembley===
* Brian May and Roger Taylor interview (2003) (28 mins)
* Gavin Taylor (Director) and Gerry Stickles (Tour Manager) interview (19 mins)
* A Beautiful Day – Rudi Dolezals backstage documentary about the whole day (30 mins)
* Tribute to the Wembley Towers – including timelapse demolition footage set to These Are the Days of Our Lives (Instrumental) (5 mins)

===Unseen Magic===
* Features Friday Concert Medley – highlights package of the previous nights show (28 mins):
# A Kind of Magic
# Another One Bites The Dust
# Tutti Frutti
# Crazy Little Thing Called Love
# We Are The Champions (ending)
# God Save The Queen
* Rehearsals (10 mins)
* Picture Gallery (5 mins) (The background music for this extra is the full, unreleased original version of "A Kind of Magic" used in the Highlander film)

===Queen Cams===
4 tracks presented in multi-angle Brian, Roger, John and Freddie cams:
# One Vision
# Under Pressure
# Now Im Here
# We Are the Champions

==Audio==
DTS Surround Sound & PCM Stereo

==The Original Concert==
The original concert started at 4.00pm with tickets costing £14.50, Four bands performed in the following order:
# INXS
# The Alarm Status Quo
# Queen (band)|Queen

==Charts and certifications==
 
 

=== Charts ===
{|class="wikitable sortable" border="1" Chart (2003) Peak position
|- Australian Top 40 DVDs Chart  1
|- Austrian Top 10 DVDs Chart  5
|- Norwegian Music DVDs Chart  3
|-
|}
{|class="wikitable sortable" border="1" Chart (2005) Peak position
|- Hungarian Top 20 DVDs  7
|-
|}
 

=== Certifications ===
 
 
 
 
 
 
 
 
 
 
 
 
 

==References==
 

==External links==
*  

 

 
 
 

 