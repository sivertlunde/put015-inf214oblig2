Minoru: Memory of Exile
{{Infobox film
| name           = Minoru: Memory of Exile
| image          = 
| image_size     = 
| alt            = 
| caption        = 
| director       = Michael Fukushima
| producer       = William Pettigrew
| writer         = 
| narrator       = Minoru Fukushima
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = National Film Board of Canada
| distributor    = 
| released       = 1992
| runtime        = 18 min 45 s  
| country        = Canada
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Minoru: Memory of Exile is a 1992 animated documentary about the Japanese Canadian internment by Michael Fukushima. The film recreates the experiences of the filmmakers father, Minoru, who as a child was sent along with his family and thousands of other Japanese Canadians to internment camps in the interior of British Columbia. 

The film explores the narrow range of options available to internees after the war, with Minuros father choosing the option of repatriation to Japan, even though his children were born in Canada. The film concludes with a description of the Canadian governments redress of 1988, which took place one year after the filmmakers mothers death.   

Produced by  .   

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 


 
 