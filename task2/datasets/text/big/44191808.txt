Ambadi Thannilorunni
{{Infobox film
| name           = Ambadi Thannilorunni
| image          =
| caption        =
| director       = Alleppey Ranganath
| producer       = CS Abraham
| writer         = Alleppey Ranganath
| screenplay     = Alleppey Ranganath
| starring       = Jagathy Sreekumar Anand Vinayan CS Abraham
| music          = Alleppey Ranganath
| cinematography = Saloo George
| editing        = K Rajagopal
| studio         = Shiny Films
| distributor    = Shiny Films
| released       =  
| country        = India Malayalam
}}
 1986 Cinema Indian Malayalam Malayalam film, directed by Alleppey Ranganath and produced by CS Abraham. The film stars Jagathy Sreekumar, Anand, Vinayan and CS Abraham in lead roles. The film had musical score by Alleppey Ranganath.   

==Cast==
*Jagathy Sreekumar
*Anand
*Vinayan
*CS Abraham
*MG Soman
*Minumohan
*Santhakumari
*Soumini

==Soundtrack==
The music was composed by Alleppey Ranganath and lyrics was written by Muttar Sasikumar. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Aalaapanam || KS Chithra || Muttar Sasikumar || 
|-
| 2 || Akkuthikkuthaanavarambe || K. J. Yesudas, Ashalatha, Ampalappuzha Jyothy || Muttar Sasikumar || 
|-
| 3 || Sandhye Shaarada Sandhye || K. J. Yesudas || Muttar Sasikumar || 
|}

==References==
 

==External links==
*  

 
 
 

 