You Too Brutus
 
{{Infobox film
| name           = You Too Brutus
| image          = You Too Brutus film poster.jpg
| caption        = Theatrical poster
| director       = Roopesh Peethambaran
| producer       = Sheikh Afsal
| writer         = Roopesh Peethambaran Sreenivasan Asif Ali Tovino Thomas Ahmed Sidhique Anu Mohan Honey Rose Rachana Narayanankutty Ena Saha
| music          = Roby Abraham
| cinematography = Swaroop Philip
| editing        = Abhinav Sunder Nayak 
| studio         = Round Up Cinema
| distributor    =Popcorn Entertainments (Asia Pacific) 
| released       =   (India)
| runtime        = 101 minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          = screenplay = Roopesh Peethambaran Dialogues: Mathukkutty and Roopesh Peethambaran Dialogues (Hindi): Sony Chandy}} Malayalam comedy Hindi dialogues were written by Sony Chandy. The film is produced by Sheikh Afsal under the banner of Round Up Cinema. It features an ensemble cast including Asif Ali, Sreenivasan (actor)|Sreenivasan, Rachana Narayanankutty, Anu Mohan, Honey Rose, Ahmed Sidhique, Tovino Thomas and Ena Saha. 

The film released in March 20, 2015 to generally positive reviews.

==Cast== Sreenivasan as Hari
* Tovino Thomas as Tovino
* Asif Ali as Abhi
* Rachana Narayanankutty as Aparna
* Anu Mohan as Vicky
* Honey Rose as Shirly
* Ahmed Sidhique as Arun
* Ena Saha as Diya Muktha
* Suddhy Kopa
* Delna Davis
* Molly Kannamaly

==Critical reception==
The Times of India gave the film 3 stars out of 5 and wrote, "With its metro-friendly content, the movie cant be expected to go down well every section of the audience, but it can surely strike rich with the young crowd, thanks to the relatability quotient".  Indiaglitz.com wrote, "You Too Brutus stands out as one movie that will satisfy the new generation film goers. With no characteristic story telling paths here, the regulars may find the presentations shallow and desperate".  Sify wrote, "At 101 minutes, You Too Brutus is an okay film, if you are not at the cinemas vying to watch something new. There is not much in store that you havent seen in those so-called new-generation movies that were essentially inspired from the Latin American movies". 

Rediff gave the film 2 stars out of 5 and wrote, "The message from You Too Brutus is that human relationships are fragile but the way it is conveyed leaves us with a bad taste in the mouth".  Nowrunning.com, too, gave 2 out of 5 and wrote, "You Too Brutus is a bewildering film that fails to create something memorable or meaningful, and at best is a plodding assemblage of characters who thrash about in a clumsy gloop. And it never manages to go quite far either, in any direction whatsoever". 

==References==
 

==External links==
*  

 
 
 
 