Universal Soldier (1971 film)
 
{{Infobox film
| name           = Universal Soldier
| image          = UniversalSoldier1971.jpg
| image_size     =
| caption        =
| director       = Cy Endfield
| producer       = Donald Factor Frederick Schwartz
| writer         = Cy Endfield additional dialogue: Kevin Duggan Kenneth Fueurman George Lazenby
| based on = a story by Derek Marlowe Joe Massot
| narrator       =
| starring       = George Lazenby Ben Carruthers Robin Hunter Edward Judd Alan Barnes Cy Endfield Germaine Greer Rudolph Walker Chrissie Townson
| music          = Phillip Goodhand-Tait
| cinematography = Tony Imi
| editing        =
| studio = Appaloosa Pictures Ionian
| distributor    = Hemdale Film Corporation
| released       = May 1972 (London)  July 1972 (Dublin)  1974 (Australia) 
| runtime        = 94 min.
| country        = United Kingdom
| language       = English
| budget         = £300,000 
| preceded_by    =
| followed_by    =
}}
Universal Soldier is a 1971 film starring George Lazenby as a mercenary. It was the final film of veteran director Cy Endfield, who also has an acting role in it.

==Plot==
Ryker (Lazenby), a former mercenary, comes out of retirement to take part in the overthrow of an African dictator. He travels to London to meet former war comrade Jesse Jones (Ben Carruthers), and his associates Freddy Bradshaw (Robin Hunter) and Temple Smith (Alan Barnes). After helping fellow mercenaries test and ship weapons to South Africa, Ryker begins to have ethical concerns about his involvement. He eventually distances himself from the others, and rents a flat in London. He falls into hippie culture, and begins dating a girl named Chrissie (Chrissie Townson).

Jesse tracks down Ryker. Explaining that the operation is not producing the profits he expected, he tries to convince Ryker to return. Ryker declines, but develops a plan with Jesse to thwart the operation and take the money for themselves. They succeed and escape with Bradshaws car. A weapons dealer named Rawlings (Edward Judd) pursues them.

Jesse discovers that their "take" is somewhat less than the amount of cash they supposedly embezzled. Ryker reveals that his real plan was to sabotage the gun running operation, not to take all the money. Jesse assaults Ryker; Ryker, now a pacifist, refuses to defend himself. Ryker is eventually forced to break Jesses ankle to end his assault. As Ryker bundles Jesse into a car to seek medical treatment, Rawlings shoots them down with rifle fire.

==Cast==
*George Lazenby as Ryker
*Ben Carruthers as Jesse
*Robin Hunter as Bradshaw
*Rudolph Walker as Mbote
*Cy Endfield as Derek Bowden
*Alan Barnes as Temple Smith
*Guy Deghy as Timmerman
*Edward Judd as Rawlings
*Germaine Greer as Clara Bowden
*Ronan ORahilly as Gered
*Kevin Duggan as Hippie

==Production== On Her Majestys Secret Service but decided not to repeat the role. He spent over a year deciding what film to make next. Lazenby:
 I went to Cy Endfield and told him I wanted to make the kind of film I could believe in. He came back two days later with a conventional script about gun-runners – just a joke to both of us. But with his 28 years experience in the business he knew we had to have that kind of script to raise the backing.  
The movie was announced in November 1970.  Lazenby described the movie at the time as "anti-guns and anti-Bond... a   with no plot. It is really just a series of happenings which keep the audience entertained. This is the kind of film which is coming out in Europe now." 
He later elaborated:
 I believe that there is a true revolution going on in the youth of today which is far more than just a fad or a passing fashion. The kids are beginning to learn that the worlds economy is dependent on the arms race, and they dont like it. They feel the people going on and on making guns could be building new irrigation schemes to make the deserts productive, or making agricultural equipment to help feed all the hungry. People would be far happier employed this way than they are in wars and they would be saving lives instead of taking them. If anyone told me today there was an advancing army just outside Botany Bay, I wouldnt want to pick up a gun and fire away at it. Id be keener to swim out and have a discussion about it.  
Lazenby did not take a fee for his performance; indeed, he invested his own money to make the movie happen. 

===Shooting===
Lazenby said the film was made with an improvisational style:
 We wrote it as we went along, watching just what each member of the cast could best do, and adapting accordingly. We tell the actors the idea of a scene, but let them speak it in their own words. There are no elocution experts around our outfit and no one talks in that foreign Hollywood language, which has nothing to do with how people speak in the street. This way the characters dont sound like actors. It will be a strange experience, seeing my film, but I hope it will be entertaining as well as having something to say. Perhaps there will be many young people who will say we havent gone far enough out of the old technique. Well, if I could put on the screen what is in my head, I would never get it down at all.  
"We did it as we went along", Lazenby said later."There was a script but we didn’t follow it – we were all smoking wacky tobacco."   accessed 15 October 2012 

Germaine Greer appeared as Clara Bowden, wife of Derek Bowden, played by the films director Cy Endfield. "She is a most interesting person with a lot of fresh ideas", said Lazenby. "Working with people like her is a far greater experience than working with run- of-the-mill actors who think of nothing but their own careers." 

After finishing the main shoot, Lazenby returned to Australia for a brief visit where he told a reporter:
 No one yet knows exactly how the film will end. When I get back from Australia we shall edit, fit the music, and have every – thing but the last shot in the can. Then about three days before the film is shown, we want to stage a real demonstration in the street, mingle the actors with it, Cy will shoot it, and this will be the final scene in the film. By doing it this way, it will be absolutely up to date. It must be current if it is to have the impact we want. Then the audience will really be involved with present history.    
Lazenby was optimistic he could use this method for other movies:
 The nine months I spent on the Bond film I learnt about the old-fashioned way of doing things-and a lot about what not to do. Now Im trying to learn the newer, freer approach working with Cy Endfield. I feel once Ive learnt from him something about putting a movie together I shall be able to go on and develop some of my own ideas. Im lucky now, because I know enough of the tricks of the trade to feel I shall never have to starve and this gives me a chance to work on my ideals.  

Lazneby said Endfield wanted to end the film with Lazenbys character being killed and the actor disagreed. "I didn’t want him to die because he’d done the right thing. So I refused to film it. They had to freeze frame it instead." 

Lazenby claimed that Jimi Hendrix was supposed to do the music for the movie. "Then he died", he said.  

Lazenby later admitted he "didnt know what he was doing" when making the film and that the investors had to take over before shooting finished.  He and three other men associated with the film were each sued for £10,000 by Ionian Finance, who had leant money for the movie. 

===Reception===

===Box Office===
The film flopped at the box office. 

"I had a piece of the action and I never got a cent", says Lazenby. "I’ve only ever seen it once." 

===Critical===
Dilys Powell of the Sunday Times reviewed the movie and said "I am glad to recommend... this story... a notable performance in the lead role by George Lazenby, relaxed, reflective but solidly resolved, very different from that misfire a year or two ago as James Bond." 

===Legacy===
Lazenby subsequently fired ORahilly. After this, ORahillys career as a film producer failed and he went back into radio production.  Lazenbys acting career in the UK fizzled due to the films box office failure and he went to Italy, then to Hong Kong, and then to Australia to make more films, before going to Hollywood in the late 1970s. He married the woman who plays his love interest in the film, Chrissie Townsend.

Lazenby has since called the movie "silly". 

"On Universal Soldier I was involved in the production, the writing and even a bit in direction", said in 1973. "I dont think Im a good enough actor to get fragmented like that on a job. Now I can give my full concentration to acting I hope it will be good and lead to other roles." 

==References==
 

==External links==
*  

 

 
 
 