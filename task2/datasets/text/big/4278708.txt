Zamaana Deewana
{{Infobox film
| name = Zamaana Deewana ज़माना दीवाना  
| image = Zamaana_Deewana.jpg|
| director = Ramesh Sippy
| producer = G. P. Sippy
| music = Nadeem Shravan
| starring = Shahrukh Khan Raveena Tandon Jeetendra Shatrughan Sinha Anupam Kher Prem Chopra
| distributor =  Ramesh Sippy Enterprises Sippy Films Eros Entertainment
| released = 28 July 1995
| runtime = 180 min.
| language = Hindi
| budget =
| country = India
}}

Zamaana Deewana ( ,  }},   movie which was directed by Ramesh Sippy and released in India in 1995. It stars Shahrukh Khan and Raveena Tandon as the leading pair, with Shatrughan Sinha Jeetendra playing their parents, respectively. Anupam Kher and Prem Chopra make supporting roles as well. The film was declared a flop at the Indian box office.

== Synopsis==
Suraj (Shatrughan Sinha) and Lala (Jeetendra) were good friends before falling prey to the vicious tricks of Sundar (Tinnu Anand), so much so that Lala believes his wife is dead and Suraj is to blame for that.
They become greatest enemies and create chaos in the city by way of gang wars. Asst. Commissioner of Police (Prem Chopra) has 2 criminal psychologists, KD (Anupam Kher), and Shalini (Kiran Juneja) weave a plot to bring Rahul (Shahrukh Khan), the lively and spirited son of Suraj, close to Priya (Raveena Tandon), the ever-so graceful and elegant daughter of Lala, hoping to bring the two sides together.

== Cast ==
*Shahrukh Khan ... Rahul Singh
*Raveena Tandon ...  Priya Malhotra
*Jeetendra ... Madanlal Malhotra
*Shatrughan Sinha ... Suraj Pratap Singh
*Anupam Kher ...  Kamdev Singh (KD)
*Prem Chopra ... Asst. Commissioner of Police
*Tinnu Anand ... Sundar
*Kiran Juneja ... Shalini Srivastav
*Neelima Azeem ... Nisha
*Beena ... Sarita Malhotra
*Aashif Sheikh ... Bobby

==Soundtrack==

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- style="background:#ccc; text-align:center;"
!#!! Title !! Singer(s)

|-
| 1
| "Zamaana Deewana Ho Gaya"
| Vinod Rathod, Sapna Mukherjee
|-
| 2
| "Neend Kise Chain Kahan"
| Kumar Sanu, Alka Yagnik
|-
| 3
| "For Ever N Ever"
| Kumar Sanu, Alka Yagnik
|-
| 4
| "O Rabba"
| Udit Narayan, Sapna Awasthi
|-
| 5
| "Zamaane Ko Ab Tak Nahi" 
| Abhijeet Bhattacharya|Abhijeet, Alka Yagnik
|-
| 6
| "Soch Liya Maine Aye Mere Dilbar"
| Vinod Rathod, Alka Yagnik
|-
| 7
| "For Ever N Ever (Sad)"
| Kumar Sanu, Alka Yagnik
|-
| 8
| "Rok Sake To Rok "
| Vinod Rathod
|-
| 9
| "Parody"
| Alisha Chinai, Bali Brahmabhatt
|}

==External links==
* 

 
 
 
 
 

 