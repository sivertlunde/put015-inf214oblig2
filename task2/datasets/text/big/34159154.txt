Outcast (1937 film)
:for others with the same name, see Outcast (disambiguation)

{{Infobox film
| name           = Outcast
| image          =
| caption        =
| director       = Robert Florey
| producer       = Emanuel Cohen
| writer         = 
| starring       = 
| music          = Ernst Toch
| cinematography = Rudolph Maté
| editing        = Ray Curtiss
| distributor    = Paramount Pictures
| released       = 5 February 1937
| runtime        = 
| country        = United States
| language       = English 
| budget         =
}}

Outcast is a 1937 American film directed by Robert Florey.  

Warren William plays a Baltimore doctor accused of murder.  Although acquitted, he becomes a pariah and his practice is ruined, so he transplants himself to a small Wisconsin town.  Confiding with a sympathetic retired lawyer (Lewis Stone), the doctor just begins to build back his practice, his self-respect, and a relationship with a local girl (Karen Morley) when his past follow him in the form of the avenging sister of the murder victim.  

Unusually for Florey, this was an independent production (Emanuel Cohen Productions, billed as "Major Pictures Corporation") released through Paramount.  

== Cast ==

* Warren William as Dr. Wendell Phillips / Phil Jones
* Karen Morley as Margaret Stevens
* Lewis Stone as Anthony Abbott
* Jackie Moran as Freddie Simmerson John Wray as Hank Simmerson
* Esther Dale as Hattie Simmerson
* Christian Rub as Olaf - the Valet
* Virginia Sale as Jessica Tuite
* Ruth Robinson as Mrs. Scutter
* Murray Kinnell as Anthony Tony Stevens Harry Woods as Grant - Head Lyncher
* Richard Carle as Mooney

== External links ==

*  
*  
 
 
 
 
 
 
 