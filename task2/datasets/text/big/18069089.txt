The Meeting Point
 
{{Infobox film
| name           = Sabirni centar (The Meeting Point)
| image          = Goran Marković (screenplay)
| starring       = Rade Marković Bogdan Diklić Dragan Nikolić Olivera Marković Bata Stojković Mirjana Karanović Anica Dobra Radmila Živković
| producer       = Aleksandar Stojanović Goran Marković
| cinematography = Tomislav Pinter
| editing        = Snežana Ivanović
| distributor    =
| released       = SFR Yugoslavia|Yugoslavia: 19 July 1989
| runtime        = 98 mins
| language       = Serbo-Croatian language|Serbo-Croatian
| budget         =
| music          = Zoran Simjanović
}}
 Yugoslavian fantasy Goran Marković, starring Rade Marković, Bogdan Diklić, Dragan Nikolić, Mirjana Karanović and Anica Dobra. It is based on Dušan Kovačevićs play of the same title translated in the U.S. as The Gathering Place. It is published by Samuel French.

Movie scenes of The Meeting Point filmed in Gamzigrad, and several actors of them actually went to Tunisia where they filmed scenes from the desert and the ruins of a Roman city Dougga. Catacombs from Labyrinth are made in a studio in Košutnjak.

== Plot == archaeological team, digging in a remote village and led by an old professor, unearths an old Roman artifact, a gravestone bearing some mysterious inscriptions. After realizing that they have stumbled upon something precious, the professor collapses with a heart attack. Seemingly dead for people around him, he finds himself in a sort of afterlife state and realizes that the stone marked a passage into the classical underworld so he starts mingling with the antique spirits of the dead. The spirits themselves appear just as silly and petty as the peasants from the village above them, and in their desire to see what happened to their descendants, they find themselves surprised by the modern world of the living.

== Awards ==
At the 1989 Pula Film Festival (the Yugoslavian version of the Academy Awards), the film won the Big Golden Arena awards for Best Film, Best Screenplay (Dušan Kovačević) and  Best Actress in a Supporting Role (Radmila Živković).

== External links ==
*  

 
 

 
 
 
 
 
 
 
 
 


 
 