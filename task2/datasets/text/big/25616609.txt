A Woman of the World
{{Infobox film
| name           = A Woman of the World
| image          = Pola Negri Tobacco Card.jpg
| caption        = Pola Negri Mal St. Clair
| producer       = Adolph Zukor Jesse L. Lasky
| writer         = Pierre Collings Carl Van Vechten (novel The Tattooed Countess) Morris Ryskind (intertitles)
| starring       = Pola Negri Holmes Herbert Charles Emmett Mack Chester Conklin Dot Farley
| cinematography = Bert Glennon
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        = 70+ minutes
| country        = United States Silent (English intertitles)
| budget         =
}} silent drama Mal St. Clair, produced by Famous Players-Lasky, and distributed by Paramount Pictures.   

One of Pola Negris best Paramounts and today available films. 

==Synopsis==
A European Countess visits relatives in the United States where her continental freedoms clash with the morals of a small American town. She smokes a cigarette in public and wears flashy makeup. A crusading District Attorney stops at nothing to have her leave town and expose her for her loose ways only to fall in love with her himself.

==Cast==
* Pola Negri - Countess Elenora
* Holmes Herbert - Richard Granger
* Charles Emmett Mack - Gareth Johns
* Chester Conklin - Sam Poore
* Dot Farley - Mrs. Baerbauer
* Blanche Mehaffey - Lennie Porter
* Guy Oliver - Judge Porter
* Lucille Ward - Lou Poore
* Dorothea Wolbert - Annie

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 


 