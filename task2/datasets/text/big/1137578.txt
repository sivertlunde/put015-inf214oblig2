Bride and Prejudice
 
 
{{Infobox film
| name           = Bride and Prejudice
| image          = Bride-and-prejudice.jpeg
| caption        = Original film poster
| director       = Gurinder Chadha
| producer       = Gurinder Chadha Deepak Nayar
| writer         = Paul Mayeda Berges Gurinder Chadha Jane Austen (novel)
| starring       = {{Plainlist |
* Aishwarya Rai
* Martin Henderson
* Daniel Gillies
* Nadira Babbar
* Anupam Kher
* Naveen Andrews
* Namrata Shirodkar
* Sonali Kulkarni
}}
| music          = Anu Malik
| cinematography = Santosh Sivan
| editing        = Justin Krish
| distributor    = Miramax Films (USA) Pathé|Pathé Distribution (UK)
| released       =          
| runtime        = 111 minutes
| country        = United Kingdom United States India
| language       = English Hindi (Some dialogue)
| budget         = $7 million 
| gross          = $24,716,440 
}} Punjabi dialogue. The film released in the
United States on 11 February 2005 and was well received by film critics. 

==Plot==
The plot of the movie is based on Austens novel Pride and Prejudice. Some character names remain the same, while others are changed slightly, using localized names with similar pronunciation (such as Lalita for Elizabeth). Set in Amritsar, the story follows Lalita Bakshi, a young woman living with her doting father and helping him run the family farming enterprise; her mother, who is determined to marry off her daughters to respectable and wealthy men; and her three sisters, Jaya, Maya, and Lakhi. At a friends wedding, Lalita meets Will Darcy, a handsome and wealthy American working in the family hotel business, who has arrived in Amritsar with his long-time friend, the barrister Balraj, and Balrajs sister Kiran.

Events in the movie roughly parallel those in the novel, punctuated by "Bollywood" style song and dance numbers: Darcy resists his attraction to Lalita, who considers him conceited, arrogant, and intolerant toward India and Indian culture. At various parties and gatherings, Mrs Bakshis mindless chatter, Mayas kitschy dancing, and Lakhis uninhibited flirting astonish Darcy and his friends, and mortify Jaya and Lalita.  Balraj and Jaya fall quickly in love, but misunderstandings and interference by others drag out their courtship. Lalita meets and is attracted to "Johnny Wickham", Darcys former friend, and he validates her low opinion of Darcy.  Mr Kholi, a rich, crass, clumsy, and ostentatious Americanized relative, proposes to Lalita; after she turns him down, her best friend Chandra agrees to marry him, much to Lalitas confusion and consternation.

When the youngest sister Lakhi runs off with Wickham, Darcy and Lalita find the couple and separate them before he can ruin her life, as he almost did to Wills young sister Georgie.

Ultimately, back in India for Jaya and Balrajs wedding, Darcy surprises and wins over Lalita by joining in the traditional drumming, showing that he is learning to appreciate Indian culture. The film ends with the double wedding of Jaya to Balraj and Lalita to Darcy, with the two couples riding on elephants down the streets of Amritsar.

==Cast==

Names in parentheses are the characters in the original Austen novel.

* Aishwarya Rai as Lalita Bakshi (Elizabeth Bennet)
* Martin Henderson as William "Will" Darcy (Fitzwilliam Darcy)
* Daniel Gillies as Johnny Wickham (Mr. George Wickham)
* Naveen Andrews as Mr. Balraj (Mr. Bingley)
* Anupam Kher as Mr. Bakshi (Mr. Bennet (Pride and Prejudice)|Mr. Bennet)
* Nadira Babbar as Mrs. Bakshi (Mrs. Bennet)
* Namrata Shirodkar as Jaya Bakshi (Jane Bennet)
* Indira Varma as Kiran Balraj (Caroline Bingley)
* Sonali Kulkarni as Chandra Lamba (Charlotte Lucas)
* Nitin Ganatra as Mr. Kholi (Mr. Collins)
* Meghna Kothari as Maya Bakshi (Mary Bennet)
* Peeya Rai Chowdhary as Lakhi Bakshi (Lydia Bennet)
* Alexis Bledel as Georgina "Georgie" Darcy (Georgiana Darcy)
* Marsha Mason as Catherine Darcy (Lady Catherine de Bourgh)
* Harvey Virdi as Mrs. Lamba (Lady Lucas)
* Georgina Chapman as Anne de Bourgh

==Soundtrack==
 
{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Title !! Singers
|-
| Balle Balle
| Sonu Nigam, Gayatri Iyer
|-
| Tumse Kahen Ya, Hum Na Kahen
| Udit Narayan, Alka Yagnik
|-
| No Life Without Wife
| Gayatri Iyer, Nisha Mascarenhas, Sowmya Rao
|-
| veer ji veyon chaliya Jassi Sidhu
|-
| Lo Shaadi Aayi
| Alka Yagnik, Kunal Ganjawala, Anu Malik
|-
| Tumse Kahen Ya, Hum Na Kahen (sad)
| Alka Yagnik
|-
| Dola Dola
| Gayatri Iyer
|-
| Tumse Kahen Ya, Hum Na Kahen
| Instrumental
|}

==Production==
 Little Venice, Golden Temple Santa Monica Beach.
 Ashanti sings "Take Me to Love" and "Touch My Body" in the film. According to director Gurinder Chadha in "making-of" extras on the DVD release, Ashantis appearance in the film is an homage to the tradition of a celebrity making a cameo appearance to sing an "item number, a song that has no direct involvement in the plot in Bollywood films.

==Critical reception==
The film opened to generally positive reviews. Review aggregation website Rotten Tomatoes gives the film a score of 79% based on reviews from 33 critics, with a rating average of 6.6 out of 10.   

Stella Papamichael at the BBC noted that "Swapping corsets for saris, and polite pianoforte for the bhangra beat, director Gurinder Chadha reinvigorates Jane Austens Pride And Prejudice with fun and flamboyance". Stella Papamichael, October 2004, BBC http://www.bbc.co.uk/films/2004/09/16/bride_and_prejudice_2004_review.shtml 

 
{| style="float: middle;" border="1"
|-
! Professional Reviews
|-
| BBC   
|-
| Rotten Tomatoes   
|-
| Hollywood.com   
|-
| USA Today  
|-
| Rolling Stone   
|-
| reelviews.net   
|- The New York Times   
|- ABC Australia   
|-
|}

==Box office==
The film grossed $24,716,440, against a production budget of $7 million.   

==References==
 

==External links==
 
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 