Phoring
{{Infobox film
| name           = Phoring
| image          = Phoring_Title_Art.jpg
| alt            = Film title written in bengali script
| caption        = Film Title Art
| director       = Indranil Roychowdhury
| producer       = Sugata Bal (Das) Anasua Roychowdhury Pinaki Shoukalin Ghosh
| writer         = Indranil Roychowdhury Sugata Sinha
| based on       =  
| narrator       = 
| starring       = Akash Adhikari Sohini Sarkar Sourav Basak Sankar Debnath Senjuti Roy Mukherji Dwijen Bandopadhyay Ritwick Chakraborty
| music          = Prabuddha Banerjee
| cinematography  = Indranil Mukherjee
| Sound          = Partha Ray Barman
| editing        = Sumit Ghosh
| studio         = G B C Enterprise
| presented by    = Chitrabeekshan Audio Visual Pvt. Ltd.
| released       = September 27, 2013 
| runtime        = 2hr 07min
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}} Bengali feature drama film directed by Indranil Roychowdhury.  It was produced by Chitrabeekshan Audio Visual PVT LTD., and is 126 minutes long. The film won the Prasad Labs post-production completion award for 2012. 

==Plot outline==

An adolescent boy (Phoring, for "Dragonfly") grows up in a back-of-beyond township in North Bengal. Surrounded by the lush Dooars countryside, the town barely survives the shutdown of a factory. Maladjusted and a born loser, Phoring fights the voices in his head which he calls God. A new teacher (Doel) arrives in school, and opens up his mind to things unknown.  Just when Phoring starts to believe that this is not a dream, Doel disappears abruptly leaving behind a trail of doubt and suspicion. Phoring decides to go looking for her in Kolkata.

Awards and Screenings:
Vincent Ward Prize. Asian New Talents Competition. Shanghai International Film Festival.2014
Best Project: Work In Progress Lab. Filmbazar 2012, Goa
Selection, Indian Panorama, 2013
Also Screened at: Indian Film Festival Los Angeles and Indian Film Festival Stuttgart, Chiocago South Asian Film Festival, Indian Film Festival, Prague. all in 2014. 

Filmfare Awards East:
Best Cinematography: Indraneel Mukherjee
Best Background score: Prabuddha Banerjee
Best Debut Actor: Sohini Sarkar
Best Debut Director: Indranil Roychowdhury

Zee Bangla Gaurav Samman::

Rituporno Ghosh Memorial Award: Akash Adhikary and Indranil Roychowdhury
*Best Actress (Jury) - Sohini Sarkar

==References==

{{reflist|refs=
 
   "Phoring, Chavunilum win India bazaar awards", Variety.com,
   26 November 2012, webpage:
    .
 
}}

==External links==
 
*  

 
 
 
 


 