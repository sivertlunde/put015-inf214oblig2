Mumbai Police (film)
 
{{Infobox film
| name           = Mumbai Police
| image          = Mumbai_Police_(film).jpg
| caption        = 
| director       = Rosshan Andrrews
| producer       = Nisad Haneefa
| writer         = Bobby-Sanjay Prithviraj Jayasurya Rahman
| music          = Gopi Sunder
| cinematography = R. Diwakaran
| editing        = Mahesh Narayanan
| studio         = Nisad Haneefa Productions
| distributor    = Central Pictures & Tricolor Entertainments
| online media partner = PrimeGlitz Media
| released        =  
| country        = India
| language       = Malayalam
| runtime        = 145 minutes
| budget         =
| gross          =
}} Malayalam Crime Rahman in the lead roles. Supporting roles are played by Kunjan, Aparna Nair and debutant Hima Davis. The film is produced by Nisad Haneefa and co-produced by Nivas Haneef and Niyas Haneefa. The background score was composed by Gopi Sunder and the cinematography was handled by G. Diwakaran.

The Khaleej Times identified Mumbai Police as one of the "remarkable box-office success " of 2013. 

==Plot==

The movie opens with ACP Antony Moses aka Rascal Moses (Prithviraj Sukumaran|Prithviraj) getting into an accident which causes him, a partial memory loss. Before the accident, he had solved the murder case of his friend, Assistant Commissioner Aaryan John Jacob (Jayasurya), who was killed during a gallantry awards ceremony at police/parade grounds possibly by a Long range rifle. Antony gets into the accident while talking to his senior officer and brother-in-law Farhan Ashraf (Rahman (actor)|Rahman),Commissioner of police. He tells him that the case is solved. But, before giving the name of the murderer, accident occurs. Farhan tells Antony, about his past and the tragedy occurred to their friend, Aaryan. He reassigns Antony Moses, back to the case. Antony re investigates the case during which he finds out the murderer.

Antony Moses at first struggles with the day to day happenings of his increasingly confusing life he lived which he has no memory of and confronts many problems such as assaults by thugs at his residence, complaints of other residents of his flat and a history as an alcoholic and womaniser. He has no clue on how to lead his life and is irritated as he feels like a puppet. He slowly regains confidence and gets back on duty partly due to motivation by Farhan. He investigates various links of the crime by utilizing clever ideas and implementing it with finesse. As the murder weapon is suggested to be a most modern sniper rifle, he uses the assistance of a professional sharpshooter (Riyaz Khan) in identifying the modus operandi of the criminal, where the sharpshooter suggests the possibility of the rifle capable of being remote controlled and thereby an absence of a real shooter is suggested.

The story takes a twist as Antony Moses discovers darker sides of his own personality and the strength and integrity of the friendship he shared  with the "Mumbai Police" group, which is concreted by a personal video taken by the fiancee of Aaryan, just before the award ceremony,in which Aaryan rehearses his speech for the ceremony and gives a heartwarming speech regarding his friendship with Antony. This video causes a visible change in him and he delves deeper into the investigation with more personal desire for the truth.

It is then revealed in a shocking scene that Antony is in fact secretly gay, and had a seemingly sexual relationship with another gay man who happens to be a pilot for an international airline. The memory impaired Antony rebuffs his sexual advances and breaks down knowing the truth about his sexuality and violent personality with criminal tendencies which was a mask to his sexual orientation (which would be regarded as being especially shameful in the police force). It is then shown in a flashback that Aaryan was a witness to one of his sexual affairs and threatened Antony and his lover that he will do what is necessary according to protocol and breaks their friendship. A distraught Antony then plans an elaborate scheme to murder Aaryan at the awards ceremony before he supposedly discloses the shameful truth about Antony.

After the murder, Antony takes up the investigation himself and gradually sees the video clip that showed the integrity of Aaryan and their friendship. It was after this incident that he realizes the futility of his actions and calls up Farhan and had actually already revealed/confessed the fact that he in fact is the murderer. But Farhan being a brilliant police officer had actually used the memory impaired Antony to uncover the proper evidence (Why and How?). The ending scene shows Farhan with a heavy heart filling up the last blank page of the case diary, Antony had submitted and calling up his superiors to report the arrest of Antony as Antony sits before him in silence, after saying emotionally that he truly misses "Mumbai Police" (a nickname for Antony Moses, Fahran and Aryan Jacob who always got into trouble together).

==Cast==
*Prithviraj Sukumaran as ACP Antony Moses, Ernakulam Assistant Commissioner
*Jayasurya as ACP Aaryan John Jacob, Mattancherry Assistant Commissioner Rahman as CP Farhan Ashraf, Commissioner of police
*Hima Davis as Rebecca, the love interest of ACP Aryan Jacob
*Aparna Nair as Rakhee Menon
*Deepa Rahul Ishwar as Annie Farhan, Fahrans wife and Antonys sister
*Kunjan as Sudhakaran
*Rohith Vijayan as Vishnu
*Mukundan as Captain Srinivas
*Harish Uthaman as Roy
*Captain Raju as Inspector General
*Amal
*Chaali Paala 
*Nihal Pillai (cameo) as Gay Friend of ACP Antony Moses
*Riyaz Khan (cameo) as Professional Shooter
*Shweta Menon (cameo) as Doctor
*Mariya Roy (cameo)


The shoot started on January 19, 2013 at Kochi.  . Mediasplash.in (2013-01-05). Retrieved on 2013-08-19. 

==Release==
It was released on 3 May 2013.

===Reception===
Sify|Sify.com and Indiaglitz.com  called Mumbai Police one of the best recent Malayalam films.    Paresh of Rediff.com called the film a " good suspense thriller which will make history for its interesting story."  Aswin of The Times of India says that the film " arrests and engages the audience with bold portrayals from a well-chosen cast." 

Veeyen of Nowrunning.com praised the writing and said the " dark lined elegance that the movie sports, makes it a gritty, knotty thriller - a hardboiled treat." 

Shekhar of Oneindia.in praised the narrative and said the story has unexpected surprises. 

==References==
 

==External links==
*  
*  
 
 
 