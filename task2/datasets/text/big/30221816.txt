The Girl with a Hatbox
{{Infobox film
| name           = The Girl with a Hatbox
| image          = Anna sten.jpg
| image_size     = 220px
| caption        = Anna Sten in the film
| director       = Boris Barnet
| producer       =
| writer         = Vadim Shershenevich Valentin Turkin Vladimir Mikhailov Vladimir Fogel
| cinematography =
| editing        =
| distributor    =
| studio         = Gorky Film Studio|Mezhrabpom-Russ
| released       =  	
| runtime        = 67 minutes
| country        = Soviet Union Russian intertitles)
| budget         =
}}
 Vladimir Mikhailov and Vladimir Fogel.

==Plot==


Natasha and her grandfather live in a cottage near Moscow, making hats for Madame Irène. Madame and her husband have told the housing committee that Natasha rents a room from them; this fiddle gives Madames lazy husband a room for lounging. The local railroad clerk, Fogelev, loves Natasha but she takes a shine to Ilya, a clumsy student who sleeps in the train station. To help Ilya, Natasha marries him and takes him to Madames to live in the room the house committee thinks is hers. Meanwhile, Madames husband pays Natasha with a lottery ticket he thinks is a loser, and when it comes up big, just as Ilya and Natasha are falling in love, everything gets complicated...

==Cast==
*Anna Sten as Natasha Vladimir Mikhailov as her grandfather
*Vladimir Fogel as Fogelev
*Ivan Koval-Samborsky as Ilya Snegiryov
*Serafima Birman as Madame Irène
*Pavel Pol as Irènes Husband
*Eva Milyutina as Marfusha

==See also==
*The Three Million Trial
*The House on Trubnaya

==External links==
* 

 
 
 
 
 
 

 