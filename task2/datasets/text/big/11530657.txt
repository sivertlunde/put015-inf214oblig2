The Heartbreak Kid (2007 film)
{{Infobox film
| name           = The Heartbreak Kid
| image          = Heartbreak kid 2007.jpg
| border         = yes
| caption        = Theatrical release poster
| alt            = A woman in the passenger seat making faces at a man driving the car. Peter Farrelly Bobby Farrelly
| producer       = Ted Field Bradley Thomas
| screenplay     = Scot Armstrong Leslie Dixon Peter Farrelly Bobby Farrelly Kevin Barnett
| based on       =     Scott Wilson Danny McBride
| music          = Bill Ryan Brendan Ryan
| cinematography = Matthew F. Leonetti
| editing        = Sam Seig DreamWorks Pictures Davis Entertainment Conundrum Entertainment Radar Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = English
| budget         = $45 million 
| gross          = $127,766,650
}}
 film of Scott Wilson and Danny McBride. The screenplay for the 2007 film was written by Leslie Dixon, Scot Armstrong, the Farrelly brothers and Kevin Barnett.

==Plot== single and indecisive about starting a relationship. While walking down the street he witnesses a purse snatcher victimizing Lila (Malin Åkerman). After failing to retrieve the purse, they exchange pleasantries and eventually date. They quickly become serious, as they seem perfect for each other. Their relationship becomes threatened, however, when Lila is offered a job in Holland.  When Eddie probes further, it is revealed that it is company policy to not relocate married employees.  Pushed by the urging of his father (Jerry Stiller) and best friend (Rob Corddry), Eddie proposes to Lila and they get married.
 deviated septum (which, as she casually mentions later, is the result of her past cocaine usage).
 Los Cabos, Danny McBride), who suspects Eddie of hiding something. To cover his long absences from his bride, Eddie makes up a story about a business with a major supplier to his store, whom he is trying to butter up for better credit terms.

Eddie decides to break up with Lila. In the chaos that ensues, she and Miranda both abandon him. Eddies passport is destroyed by Lila in the mayhem. He is forced to cross the U.S. border illegally with help provided from his friend Uncle Tito (Carlos Mencia), but is repeatedly caught by border patrols while others escape by the hundreds. After an extensive effort to get into the country, he goes to Oxford, Mississippi only to find out that Miranda already married her previous boyfriend. He promises her aunt to leave Miranda alone, only to sneak into Mirandas house late at night. He wakes her up and talks to her without waking up her husband. Alas, her cousin breaks into the house with a baseball bat and beats Eddie up until Eddies father makes his presence known too. Eddie agrees to leave if Miranda just says if she really loves her new husband. Miranda says yes. Eddie gives up on her although he doesnt know she looks at him from her balcony when he walks away.

Eighteen months later, Eddie has moved to Mexico. Miranda comes there on vacation, informing him that she made a mistake, has split with her husband and wants to be with him. Eddie is very pleased, except he once again hides the fact that he has a new wife, Consuelo (Eva Longoria), and he is thrust right back into the same situation.

==Cast==
* Ben Stiller as Edward "Eddie" Cantrow
* Malin Åkerman as Lila Cantrow
* Michelle Monaghan as Miranda
* Jerry Stiller as Doc Cantrow
* Rob Corddry as Mac
* Carlos Mencia as Uncle Tito
* Danny McBride as Martin
* Ali Hillis as Jodi Scott Wilson as Boo
* Kayla Kleevage as Docs Vegas Companion
* Stephanie Courtney as Gayla
* Polly Holliday as Beryll
* Eva Longoria as Consuelo Cantrow

==Promotion== Comic Con Convention in San Diego, California, a sex scene from the film was cut due to backlash for the nudity in prior films at the convention such as 300 (film)|300 and Borat. {{cite web
|title=Malaysia Sun
|work=Stillers S and M sex scene axed
| url=http://story.malaysiasun.com/index.php/ct/9/cid/f825b92e19df636a/id/269270/cs/1
| accessdate=September 3, 2007
}} 

==Reception==

===Box office===
The film grossed $14,022,105 in 3,219 theaters in its opening weekend, putting it in second place at the box office in the North America. The film eventually grossed a total of $127,766,650 worldwide, which includes $36,787,257 in North America and $90,979,393 in other territories. 

===Critical response===
The Heartbreak Kid has received mostly negative reviews from film critics. Review aggregation website Rotten Tomatoes gives the film a score of 29% based on 156 reviews.  On Metacritic, the film had an average score of 46 out of 100, based on 30 reviews, which indicates "mixed or average reviews". 

Peter Travers (of Rolling Stone) declared the film the years Worst Remake on his list of the Worst Movies of 2007.  In a clip for the 2008 MTV Movie Awards, while arguing, Robert Downey Jr. takes a jab at Stiller about the weak reception for the film. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 