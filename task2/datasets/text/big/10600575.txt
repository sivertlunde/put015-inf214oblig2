The Brave Archer
 
 
 Brave Archer}}
{{Infobox film name = The Brave Archer image = The Brave Archer.jpg caption = DVD cover art traditional = 射鵰英雄傳 simplified = 射雕英雄传 pinyin = Shè Diāo Yīngxióng Chuán jyutping = Se 6  diu 1  jing 1 hung 4  cyun 4 }} director = Chang Cheh producer = Runme Shaw story = Louis Cha screenplay = Ni Kuang starring = Alexander Fu Tanny Tien music = Frankie Chan cinematography = Kung Mu-to editing = Chiang Hsing-lung studio = Shaw Brothers Studio distributor = Shaw Brothers Studio released =   runtime = 117 minutes country = Hong Kong language = Mandarin budget =  gross = 
}} Louis Chas novel The Legend of the Condor Heroes. The film was produced by the Shaw Brothers Studio and directed by Chang Cheh, starring Alexander Fu and Tanny Tien in the lead roles. The film is the first part of a trilogy and was followed by The Brave Archer 2 (1978) and The Brave Archer 3 (1981). The trilogy has two unofficial sequels, The Brave Archer and His Mate (1982) and Little Dragon Maiden (1983).

==Cast==
* Alexander Fu as Guo Jing
* Tanny Tien as Huang Rong
* Ku Feng as Hong Qigong
* Philip Kwok as Zhou Botong
* Wang Lung-wei as Ouyang Feng
* Ku Kuan-chung as Huang Yaoshi
* Ti Lung as Duan Zhixing Danny Lee as Ouyang Ke
* Lee I-min as Yang Kang
* Kara Hui as Mu Nianci
* Yu Hoi-lun as Mei Chaofeng
* Wang Chiang-liang as Chen Xuanfeng
* Yu Wing as Wanyan Honglie
* Dick Wei as Yang Tiexin
* Lau Wai-ling as Bao Xiruo
* Bruce Tong as Guo Xiaotian
* Chu Jing as Li Ping
* Fan Mei-sheng as Liang Ziweng
* Chan Shen as Lingzhi Shangren
* Suen Shu-pei as Sha Tongtian
* Tsai Hung as Ke Zhene
* Lam Fai-wong as Zhu Cong
* Lo Mang as Han Baoju
* Jamie Luk as Nan Xiren
* Lu Feng as Zhang Asheng
* Chiu Chung-hing as Quan Jinfa
* Chow Kit as Han Xiaoying
* Lee Siu-wa as Wang Chuyi
* Yeung Hung as Qiu Chuji
* Yip Tin-hang as Lu Chengfeng
* Chiang Sheng
* Wong Lik
* Wang Han-chen
* Cheung Chok-chow
* Lau Fong-sai
* Chan Si-kai
* Tsang Choh-lam
* Suen San-cheung
* Kwan Yan
* Siu Yuk-lung
* Robert Tai
* Yu Tai-ping
* Fung King-man
* Wai Pak
* Chow Kin-ping
* Chan Hung

==External links==
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 


 