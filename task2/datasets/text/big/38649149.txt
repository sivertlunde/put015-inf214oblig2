Asai Man Piyabanna
{{Infobox film
| name           = ආසයි මං පියාඹන්න Asai Man Piyabanna
| image          = AsaiManPiyabanna.jpg Udayakantha Warnasuriya
| producer       = Dhammika Siriwardana
| writer         = Udayakantha Warnasuriya
| starring       = Roshan Ranawana Pooja Umashankar Jayanath Gunawardhane
| editing        = Ajith Ramanayake
| released       = 01 November 2007
| runtime        = Sinhala
| music          = Rohana Weerasinghe
| awards         =
| budget         =
}}
 2007 Sinhala Sinhala Romantic/Drama film written and directed by Udayakantha Warnasuriya. Asai Man Piyambanna is the Sri Lankan version of the Hindi musical drama romance film Taal (film)|Taal. The film features Roshan Ranawana and Pooja Umashankar in the leading roles while Gayathri Dias, Shiran Silva, Nalin Pradeep Udawela and Sanath Gunatilaka also play key supporting roles. Produced by Dhammika Siriwardana, the film had music scored by Rohana Weerasinghe. It released in November 2007 and was a big hit in Sri Lankan Film History in that year. It also received good reviews from Critics. 
 

==Plot==
 Taal (1999) Hindi movie starring Anil Kapoor and Akshay Khanna.

==Cast==
 
*Roshan Ranawana
*Pooja Umashankar as Ranmalee/Maleesha
*Gayathri Dias
*Shiran Silva
*Nalin Pradeep Udawela
*Sanath Gunatilaka
*Upeksha Swarnamali
*Umali Thilakaratne
 

==Production==

 

==Release==
The film earned positive reviews at the box office and went on to become a huge commercial success. With this movie Pooja proved that She was the Most Successful Actress of Sri Lankan Movies and became a household name in Sri Lanka. The Songs of this film were most Melodious and beautifully picturised in the pristine locales of Sri Lanka and became hugely popular among the Sinhala Movie lovers.

==Soundtrack==

Samanal Hanguman: Taal verrsion (Taal se Taal Mila)

Pethu Pem Pathum

Pehasara

Kasthuri Suwada: Taal Version (Kahin Aag Lage)

==References==
 

==External links==

 
 
 