The Vulture (1937 film)
{{Infobox film
| name           = The Vulture
| image          =
| caption        =
| director       = Ralph Ince
| producer       = Irving Asher Brock Williams
| starring       = Claude Hulbert Hal Walters Lesley Brook
| music          =
| cinematography = Basil Emmott
| editing        = Warner Brothers-First First National Productions
| released       =  
| runtime        = 67 minutes
| country        = United Kingdom
| language       = English
}} The Viper, although this was much less successful.  
 75 Most missing British feature films. 

==Plot==
Hopeless but eager would-be private detective Cedric Gull (Hulbert) has just obtained a diploma from a backstreet School of Detection and is keen to put his new qualification to good use.  Fortuitously, he happens to stumble across a crime scene at the office of a diamond merchant, who has just been robbed and assaulted and is being tended by his secretary Sylvia (Brook).  The police arrive on the scene, but despite Cedrics proud boasts about his sleuthing qualifications, they decline his kind offers of help.

Striking out on his own, Cedric becomes convinced that the robbery was the work of a notorious gang of East End Chinese jewel thieves led by a mysterious and sinister individual known as The Vulture.  He takes on board his ex-con sidekick Stiffy (Walters) and the pair set off in pursuit of the criminals.  Their plans come unstuck when their inept bungling lands them both in prison.  However the police, aware of their interest in the case, agree to allow them out to act as decoys.  Cedric learns that Sylvia has been abducted by the criminals.  He decides to disguise himself as Chinese and try to infiltrate their hideout and rescue Sylvia.  After a good deal of hapless buffoonery and narrow escapes from sticky situations, he and Stiffy finally succeed in freeing Sylvia, unmasking the thieves and uncovering the identity of the elusive Vulture.

==Cast==
* Claude Hulbert as Cedric Gull
* Hal Walters as Stiffy Mason
* Lesley Brook as Sylvia George Merritt as Spicer Arthur Hardy as Li-Fu
* Frederick Burtwell as Jenkinson
* Archibald Batty as McBride
* George Carr as Charlie Yen

==Reception==
As well as turning out to be a box-office hit, The Vulture received a generally favourable reception from critics.    was prepared to concede that it was "an amusing story...adequately directed and the fun well-timed" and that Hulbert "makes the most of his opportunities".

==References==
 

== External links ==
*  , with extensive notes
*  

 
 
 
 
 
 
 
 