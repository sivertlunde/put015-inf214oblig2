Seasons of Our Love
{{Infobox film
| name           = Seasons of Our Love
| image          =Seasons of Our Love.jpg
| image_size     =
| caption        = 
| director       = Florestano Vancini
| producer       = 
| writer         = Elio Bartolini, Florestano Vancini
| narrator       = 
| starring       = 
| music          =   Carlo Rustichelli
| cinematography =    Dario Di Palma
| editing       =  Nino Baragli
| distributor    = 
| released       = 25 March 1966
| runtime        = 93 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}
 1966 cinema Italian drama film directed by Florestano Vancini. It was entered into the 16th Berlin International Film Festival.   

==Cast==
*Enrico Maria Salerno	... 	Vittorio Borghi
*Anouk Aimée	... 	Francesca
*Jacqueline Sassard	... 	Elena
*Gastone Moschin	... 	Tancredi
*Valeria Valeri	... 	Milena Borghi, moglie di Vittorio
*Gian Maria Volonté	... 	Leonardo Varzi
*Checco Rissone	... 	Olindo Civenini
*Daniele Vargas	... 	Il conte
*Elena Ballesio		
*Pietro Tordi	... 	Mario Borghi
*Massimo Giuliani	... 	Leonardo Varzi da ragazzo
*Albano Bissoni	... 	Un partigiano alla cascina
*Umberto Bertagna	... 	Un partigiano alla cascina
*Luciano Damiani		
*Isa Mancini

==References==
 

== External links ==
*  

 
 
 
 
 
 
 