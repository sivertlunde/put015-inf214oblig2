Man at Sea
 
{{Infobox film
| name           = Man at Sea
| image          = 
| caption        = 
| director       = Constantine Giannaris
| producer       = Alexander Emmert
| writer         = Constantine Giannaris
| starring       = Antonis Karistinos
| music          = 
| cinematography = Yorgos Argiroiliopoulos
| editing        = 
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Greece
| language       = Greek
| budget         = 
}}

Man at Sea ( , Transliteration|translit.&nbsp;Anthropos sti thalassa) is a 2011 Greek drama film directed by Constantine Giannaris.    

==Plot==
Alex, the captain of a Greek  , Captain Alex comes across a boat filled with adolescent refugees from Iran, Iraq, and Afghanistan.    He takes pity on the refugees and allows them on his ship. He plans to drop the refugees off at a port, but local authorities refuse to take them, forcing the refugees to stay on the boat. Their residence angers the ships owners, and gradually the "Sea Voyager" becomes a claustrophobic war zone between the refugees and the ships owners.   

==Cast==
* Antonis Karistinos as Alex
* Theodora Tzimou as Katia
* Konstadinos Avarikiotis as Andreas
* Konstadinos Siradakis as Pantelis
* Stathis Papadopoulos as Yuri
* Thanasis Tatavlalis as Petros
* Nikos Tsourakis as Samir
* Stathis Apostolou as Johnny
* Chalil Ali Zada as Rafik
* Rahim Rahimi as Kamal

==Reception==
"Man at Sea" was featured in the Panorama section of the 2011 Berlin Film Festival. 

It was observed, "If Man at Sea isn’t the director’s best work – although it certainly is his most ambitious – it’s because of his inability to orchestrate the internal rhythms of the conflict."  Movies Ltd. listed the diverse sociological issues that the movie deals with: "Illegal immigration, family loss, financial crisis, illegality" ( ).  Boyd van Hoeij wrote, "Giannaris’s latest plays more like Around the World in 80 Plot Twists."   

==References==
 

==External links==
*  

 
 
 
 
 
 