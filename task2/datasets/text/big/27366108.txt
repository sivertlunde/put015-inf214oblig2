Kushti (film)
 
{{Infobox film
| name           = Kushti
| image          = Kushti2.jpg
| caption        = Theatrical Poster.
| writer         = 
| starring       =  
| director       = T.K. Rajeev Kumar
| producer       = Girish Jain
| distributor    = Venus Records & Tapes
| editing        = 
| cinematography = 
| released       =  
| runtime        = 
| language       = Hindi
| music          =  
| country        = India
}}
Kushti (  comedy film directed by T.K. Rajeev Kumar. The film stars Rajpal Yadav in the lead role along with the WWE wrestler The Great Khali (Dalap Singh Rana) making his Bollywood debut. The film also stars Manoj Joshi, Om Puri and Sharat Saxena in supporting roles. The movie is inspired by the 1985 Malayalam film Mutharamkunnu P.O.. 

== Plot ==

The film Kushti is the story of a small village in Northern India where wrestling is a popular sport and an important wrestling match is held every year. Every year rivals Avtar Singh (Sharat Saxena) and Jiten Singh (Om Puri) try to beat each other in the wrestling match and gain the trusteeship of the village. Chander (Rajpal Yadav) plays the role of a village simpleton and a post-master.

It filled with misunderstandings and misconceptions, of hidden identities and secret love-affairs and the outcome is simply hilarious. The movie begins to take a turn when a certain someone delivers a secret package in the wrong hands. Especially someone, who is bound to take advantage and manipulate the real owner of the package.

Avtar Singh has a young and a beautiful daughter named Laadli (Nargis Bagheri|Nargis), with who Chander is madly in love. To get her fathers approval for their marriage he was to first prove his prowess by wrestling with the 7 and half feet tall "Rama Krishna" (Dalip Singh Rana). The condition set by Avtar Singh is that Chander has to defeat "Khali" in the wrestling match!!

== Cast ==

*Rajpal Yadav...... Chander
*Dalip Singh Rana....... Ramakrishna
*Nargis Bagheri...... Laadli
*Sharat Saxena...... Avtar Singh
*Asrani
*Manoj Joshi
*Om Puri...... Jiten Singh
*Jagadeesh......Puncher Pappu
*Asif Basra

== Soundtrack ==
The music of Kushti is composed by Srinivas, Tauseef Akhtar, AD Boyz and Hriju Roy. The lyrics were done by Sameer. The movie has 5 original songs. 

{| class="wikitable sortable"
|-
! Song !! Singer(s) !!   Length
|-
| "Rang Rasiya" || Anuradha Sriram, Tauseef Akhtar ||   4:37 mins  
|-
| "Dangal || Srinivas", Shreya Ghoshal ||   4:58 mins
|-
| "Kar Kar Kushti Kar" || AD Boyz ||   3:09 mins                     
|-
| "Hanuman Chalisa" || Ravi Choudhary ||   3:41 mins
|-
| "Dekho Mere Gaon Mein" || Abhijeet Bhattacharya ||   3:38 mins
|}

==Critical Reception==

Kushti received mixed reviews, but mostly negative. It was a poor show. The total grossing came up to about 3.50 score, and was held by many complaints for refunds. The song "Rang Rasiya" was praised a lot, which helped the film from going down even lower in business. On the whole, Kushti was declared a flop.

== References ==
 

==External links==
* 
* 
* 

 
 
 
 