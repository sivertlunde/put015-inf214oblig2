Gen (film)
{{Infobox film
| name           = Gen
| image          = GenFilmPoster.jpg
| image size     =
| alt            = 
| caption        = Film poster
| director       = Togan Gökbakar
| producer       = Murat Toktamisoglu Kemal Kaplanoglu
| writer         = Güray Ölgü Murat Toktamisoglu Alper Mestçi
| narrator       = 
| starring       = Doga Rutkay  Mahmut Gökgöz  Cemil Büyükdögerli
| music          = Taner Onat  Serkan Sönmezocak
| cinematography = Veli Kuzlu
| editing        = Ilker Canikligil
| studio         = Dada Film  Tiglon
| distributor    = Epic Pictures Group
| released       =  
| runtime        = 85 mins 
| country        = Turkey
| language       = Turkish
| budget         = 
| gross          = 
| preceded by    = 
| followed by    = 
}}

Gen is a 2006 Turkish horror film directed by Togan Gökbakar.

==Production==
The film was shot on location in Istanbul, Turkey.   

Karl T. Hirsch produced an English language dub of the film.

==Plot and cast==
===Plot ===
A newly appointed doctor witnesses a series of murders in a hospital which no one can reach due to heavy snow. Everybody is suspicious of each other and searching for the killer, moreover, due to the heavy snow, no one can reach the hospital and telephone lines are jammed. In three days, the hospital, which has been quiet and peaceful over the years, faces a terror that turns nightmares into reality.

{| width="75%" | 
| valign="top" width=50% |

===Original cast===
*Doga Rutkay as Dr. Deniz
*Mahmut Gökgöz as Head Physician Dr. Metin
*Cemil Büyükdögerli as Officer Cemil
*Mutlu Güney as Dr. Aykut
*Yurdaer Okur as Dr. Ragip
*Haldun Boysan as Officer Halil
*Zeliha Güney as Head Nurse Ipek
*Şahan Gökbakar as Separated Patient
| valign="top" width=50% |

===English language dub cast===
*Noel Thurman as Dr. Deniz
*Michael Roemer as Head Physician Dr. Metin
*Devin Reeve as Officer Cemil
*Steve Grabowsky as Dr. Aykut
*Ali MacLean as Head Nurse Ipek
*Julie Fitzgerald as Handan
*J. Lauren Proctor as Radio Voice
|}

==External links==
*  
*  

==References==
 

 

 
 
 
 
 
 

 
 