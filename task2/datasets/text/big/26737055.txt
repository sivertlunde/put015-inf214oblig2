Sorceress (1982 film)
 
 
{{Infobox film
| name           = Sorceress
| image          = Sorceress (1982 film).jpg
| image_size     = 
| caption        = 
| director       = Jack Hill (as "Brian Stuart")
| producer       = Jack Hill
| writer         = Jack Hill (uncredited) Jim Wynorski
| starring       = Leigh Harris
| music          = 
| cinematography = Alex Phillips Jr
| editing        = Larry Bock Barry Zetlin
| studio         = New World Pictures Conacine
| distributor    = New World Pictures
| released       = March 1982
| runtime        = 83 min.
| country        = United States
| language       = English
| budget         = $500,000 
| gross          = $4 million Christopher T Koetting, Mind Warp!: The Fantastic True Story of Roger Cormans New World Pictures, Hemlock Books. 2009 p 218 
}} Mexican Sword sword and sorcery film directed by Jack Hill and starring Leigh Harris and Lynette Harris. 

== Plot ==
 
To maintain his powers, the evil wizard Traigon must sacrifice his firstborn child to the god Caligara. He is married to a wife, however, who has other ideas; after giving birth to twin daughters, she flees from him, taking their daughters with her. Before she dies, she hands the girls over to a warrior named Krona, who promises to raise them both as great soldiers; this he does.

Twenty years pass, and when Traigon returns, he resumes hunting down his now-adult daughters(the Harris sisters), still intending to sacrifice them to Caligara. The twins have to enlist the help of a barbarian by the name of Erlik and a Viking named Baldar in their struggle to defeat their own father.

== Cast ==
* Leigh Harris ... Mira
* Lynette Harris ... Mara Bob Nelson ... Erlick
* David Millbern ... Pando
* Bruno Rey ... Valdar
* Ana De Sade ... Delissia
* Roberto Ballesteros ... Traigon
* Douglas Sanders ... Hunnu Tony Stevens ... Khrakannon
* Martin LaSalle ... Krona

== Production ==
Jack Hill said he was inspired to do a film about twin girls inspired by The Corsican Brothers. The movie was shot in Mexico in October of 1981. Production was extremely difficult, plagued by rain, fire, and low budget. Hill claimed that Roger Corman never delivered the budget he promised, forcing him to compromise on both special effects and music. Hill took his name off the film, which turned out to be a big hit. It became the last time he and Corman ever worked together. 

Leigh and Lynette Harris had both appeared in Playboy.

== References ==
 

== External links ==
*  
*   at Trailers From Hell

 

 
 
 
 
 

 