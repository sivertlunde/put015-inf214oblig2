Games Gamblers Play
 
 
{{Infobox film
| name           = Games Gamblers Play
| image          = Gui ma shuang xing.jpg
| caption        = One of theatrical posters
| film name = {{Film name| traditional    = 鬼馬雙星
| simplified     = 鬼马双星
| pinyin         = Gúi mǎ shuāng xīng
| jyutping       = Gwai2 maa2 seong1 sing1}}
| director       = Michael Hui
| producer       = Raymond Chow
| writer         = Michael Hui Lau Tin Chi Thomas Tang Sam Hui James Wong Roy Chiao
| music          = Joseph Koo Sam Hui
| cinematography = Ho Lan Shan Danny Lee Yau-Tong
| editing        = Peter Cheung Golden Harvest Media Asia
| released       =  
| runtime        = 107 minutes
| country        = Hong Kong
| language       = Cantonese
| budget         = 
| gross          = 
}} 1974 Cinema Hong Kong film directed by Michael Hui with action direction by Sammo Hung.

==Cast==
*Chan Lap Ban
*Cheng Kwan-Min
*Cheung Wah
*Chiao Roy
*Fung Ging Man
*Ho Pak-Kwong
*Michael Hui
*Ricky Hui Sam Hui
*Benz Hui
*Sammo Hung
*Law Lan
*Lee Kwan
*Lisa Lui
*Dean Shek Betty Ting Pei James Wong
*Xie Yang

==Partial soundtrack== Cantonese album by Hong Kong actor and singer Samuel Hui, released in 1974 by Polydor Records. The first two tracks are heard in the film and also released in the separate 7" vinyl.

{{track listing
|headline=Side one
|all_writing=Samuel Hui, unless otherwise
|music_credits=yes
|lyrics_credits=yes
|extra_column=English translation

|title1=雙星情歌
|note1=seung sing ching gaw
|extra1=Love Theme from (gwai ma) seung sing
|length1=2:58

|title2=鬼馬雙星
|note2=gwai ma seung sing
|extra2=Ghost Horse, Double Star
|length2=2:25

|title3=甜蜜伴侶
|note3=tim maat boon leui
|extra3=Sweet Companionship
|lyrics3=Ricky Hui
|music3=Ricky Hui
|length3=2:43

|title4=無情夜冷風
|lyrics4=Ricky Hui
|music4=Ricky Hui
|length4=2:38
|note4=mou ching ye lang fung
|extra4=Merciless Breezy Night

|title5=一水隔天涯 (a) / 愛你三百六十五年 (b) Huis combined rendition of both songs is heard in the 1974 film Naughty! Naughty! (綽頭狀元). 
|note5=yat seui gak tin ngai / ai ni san bai liu shiwu nian
|extra5=Water over the Horizon / Love You for 365 Years
|lyrics5=Wong Jim Original lyrics of "yat seui gak tin ngai" were written by Joh Kei (左几) and are heard in the 1966 film of the same Chinese name. "Ai ni san bai liu shiwu nian" is a Mandarin rendition of the 1969 Japanese song, "Anata ga i nakute mo" (あなたがいなくても), sung by Mieko Hirota and lyricized by Rei Nakanishi, and was previously recorded by other singers. 
|music5=Yu Lawn 于粦 (a) / Shin Kawaguchi   (b)
|length5=3:45
}}

{{track listing
|headline=Side two
|music_credits=yes
|lyrics_credits=yes
|extra_column=English translation

|title6=春夢良宵 Rendition of the 1972 song "Id Love You to Want Me" by Lobo. 
|note6=cheun mung leung siu
|extra6=Good Night Dream Lobo
|length6=2:27

|title7=制水歌 Rendition of the 1972 song "Mother and Child Reunion" by Paul Simon 
|note7=chi seui goh
|extra7=Song of a Regulated Water
|music7=Paul Simon
|length7=2:50
 Just a Little" by The Beau Brummels 
|note8=dang yook yan Ron Elliott, Bob Durand
|length8=2:35

|title9=鐵塔凌雲
|note9=teet taap ling waan
|lyrics9=Michael Hui
|length9=3:05 Lingyun Tower

|title10=夜雨聲
|note10=ye yu sing
|lyrics10=Ricky Hui
|music10=Ricky Hui
|extra10=Sounds of the Night Rain
|length10=2:16

|title11=雙星情歌音樂
|extra11=Love Theme from (gwai maa) seung sing (instrumental)
|length11=2:58
}}

;Notes
 

==External links==
* 

 

 
 
 
 
 
 
 

 