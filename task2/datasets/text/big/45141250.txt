Nakkare Ade Swarga
{{Infobox film 
| name           = Nakkare Ade Swarga
| image          =  
| caption        = 
| director       = M. R. Vittal
| producer       = Srikanth Nahatha Srikanth Patel
| writer         = 
| screenplay     = M. R. Vittal Jayanthi Shailashree Jr Revathi Thara
| music          = M. Ranga Rao
| cinematography = S V Srikanth D V Rajaram
| editing        = SPN Krishna T P Velayudham
| studio         = Srikanth &amp; Srikanth Enterprises
| distributor    = Srikanth &amp; Srikanth Enterprises
| released       =  
| runtime        = 137 min
| country        = India Kannada
}}
 1967 Cinema Indian Kannada Kannada film, directed by M. R. Vittal and produced by Srikanth Nahatha and Srikanth Patel. The film stars Jayanthi (actress)|Jayanthi, Shailashree, Jr Revathi and Thara in lead roles. The film had musical score by M. Ranga Rao.  

==Cast==
  Jayanthi
*Shailashree
*Jr Revathi
*Thara
*Indrani
*Kamalamma Narasimharaju
*Arunkumar
*Ranga
*Dikki Madhavarao
*R. Nagendra Rao
*Samapth in Guest Appearance
*Srirangamurthy
*Rajendra Prasad
*M. R. Srinivasan
*Somashekar
*AVK Murthy
*Guruswamy
*Raju
*Babu
 

==Soundtrack==
The music was composed by M. Ranga Rao. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|- Susheela || Vijaya Narasimha || 03.46
|-
| 2 || Baalkondu Bhaava Geethe || PB. Srinivas || R. N. Jayagopal || 03.17
|- Susheela || R. N. Jayagopal || 03.03
|- Susheela || R. N. Jayagopal || 03.25
|-
| 5 || Nagabeku Nagisabeku || PB. Srinivas || R. N. Jayagopal || 03.11
|}

==References==
 

==External links==
*  

 
 
 
 


 