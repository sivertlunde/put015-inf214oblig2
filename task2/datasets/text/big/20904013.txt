Amaanat
 
 
{{Infobox film
| name           = Amaanat
| image          = Amaanat77.jpg
| image_size     = 
| caption        = 
| director       = Shatrujit Paul
| producer       = Shatrujit Paul
| writer         = Shatrujit Paul
| narrator       =  Asit Sen, Aruna Irani
| music          = Ravi
| lyrics         = Sahir Ludhianvi  
| cinematography = 
| editing        = 
| distributor    = 
| released       = 1977
| runtime        = 
| country        = India Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
}}
 1977 Bollywood Asit Sen, Aruna Irani.

==Story==
Suresh is a painter who lives a contented life with wife, son and his work.One day a lady asks him to make a portrait of her for which she will give him Rupees 2,000. He agrees and next day he goes to the ladys house.She seduces him and her husband catches her with Suresh. A fight ensues between the husband and wife with the wife killing the husband. She puts the blame on Suresh. Suresh manages to escape and leaves the place abandoning his wife and child.

While on the train a man asks Suresh to take care of his wife and baby girl who are travelling alone for first time. Suresh agrees. The train meets with an accident and the lady dies. Suresh takes the baby entrusted in his care and starts looking after her as his daughter. 

Sureshs wife has been left to fend for herself and her son (Deepak). She starts working as a maid to earn a livelihood. Her employer offers to adopt the son and provide him with an education and clothes to wear and food to eat. She agrees to leave her son in his care so as to give him a good life.

Several years pass and Deepak falls in love with Sureshs adopted daughter Suchitra. After several eventful incidents in their lives, Deepak gets his love Suchitra, and Suresh gets justice and is reunited with his wife and son.

==Cast==
*Manoj Kumar ...  Deepak
*Sadhana Shivdasani ...  Suchitra (as Sadhana)
*Balraj Sahni ...  Suresh Mehmood ...  Mahesh
*Rehman (actor)|Rehman...  Amar
*Dheeraj Kumar ...  Mohan (as Dhiraj)
*Shashikala ...  Sonia
*Aruna Irani ...  Flora DCosta
*Krishan Dhawan ...  Mohans Father Asit Sen ...  Mr. DCosta
*Brahm Bhardwaj ...  Mohans Grandfather (as Braham Bhardwaj)
*Praveen Paul ...  Mohans Mother
*Mukri ...  Mr. Lobo
*Devyani
*Achala Sachdev ...  Shanti
*Music Director ...  Ravi

==Soundtrack==

The music of the film was composed by Ravi. Mohammad Rafi sang the most memorable songs Door Rehkar Na Karo Baat Qareeb Aajao, Matlab Nikal Gaya to Pehchante Nahin, Teri Jawani Tapta Mahina. 


==External links==
*  

 
 
 


 