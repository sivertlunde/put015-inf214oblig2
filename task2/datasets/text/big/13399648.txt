The Left-Handed Man
 
{{Infobox film
| name           = The Left-Handed Man
| image          =
| caption        =
| director       = D. W. Griffith
| producer       =
| writer         = Frank E. Woods
| starring       = Lillian Gish
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        = 10 minutes
| country        = United States
| language       = Silent with English intertitles
| budget         =
}} short drama film directed by  D. W. Griffith. Prints of the film survive at the film archive of the Museum of Modern Art.   

==Cast==
* Lillian Gish as The Old Soldiers Daughter Charles West as The Old Soldiers Daughters Sweetheart Harry Carey as The Thief
* Kathleen Butler as In Court
* William J. Butler as In Court
* William A. Carroll as Extra
* William Elmer as Policeman (as Billy Elmer)
* Frank Evans as Policeman Charles Gorman as In Bar James Kirkwood as The Old Soldier (unconfirmed) Joseph McDermott as Policeman
* Alfred Paget as Policeman

==See also==
* Harry Carey filmography
* D. W. Griffith filmography
* Lillian Gish filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 

 