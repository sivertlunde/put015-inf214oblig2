Bestiaire
 
{{Infobox film
| name           = Bestiaire
| image          = Bestiaire_Poster.jpg
| caption        = 
| director       = Denis Côté
| producer       = Sylvain Corbeil Denis Côté
| writer         = Denis Côté
| starring       = 
| music          = 
| cinematography = Vincent Biron
| editing        = Nicolas Roy
| studio         = Metafilms Le Fresnoy Studio National des Arts Contemporains
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = Canada France
| language       = 
| budget         = 
| gross          = 
}}
Bestiaire is a 2012 Canadian-French experimental film|avant-garde nature documentary film directed by Denis Côté.  The film centers on how humans and animals observe each other.  It has received mostly positive critical reception.

It was filmed at Parc Safari in Hemmingford, Quebec (township)|Hemmingford, Quebec, and had its world premiere at the Sundance Film Festival. It was also shown at the Berlin Film Festival.

==External links==
* 
* 
* 
* 

 
 
 
 
 
 
 
 
 
 


 