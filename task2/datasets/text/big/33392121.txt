Too Tough to Care
 
{{Infobox film
| name           = Too Tough to Care
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =  Dave Parker
| producer       = Marin County Medical Society
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Sid Davis Productions
| released       = 1964 
| runtime        = 18 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Too Tough to Care is an 18-minute educational film produced in 1964 to undermine teenage resistance to anti-smoking education.    Unlike other films in its genre, Too Tough to Care used satire and humor to illustrate the misleading claims of cigarette advertising. The film garnered attention in both the mainstream press as well as academic journals.

==Synopsis==
Farley, an advertising copy writer for the fictional Finster Cigarette Company, is dismayed by the medical establishments successful campaign to link smoking with lung cancer. It dawns on Farley that one effective way to counter this campaign would be to promote the concept of being "too tough to care" about the hazards of smoking. So he launches an advertising campaign, complete with a compelling jingle, showing men such as dynamite workers and gas workers lighting up Finster cigarettes in situations that would be dangerous, suggesting that they are too tough to care about consequences.

The campaign is successful. Farley offers other executives a Finster cigarette to celebrate, but the offer is declined. Farley is the only one who smokes.

==Production== Sid Davis Dave Parker, the production company as Lawren Productions, and the sponsor as the American Heart Association. 

==Reception==
The Los Angeles Times called the film "a refreshing departure from conventional educational films."  An article in the academic journal American Biology Teacher cited the film with the comment "In contrast to Smoking and You, this film uses the humorous, overly dramatic approach."  A paper in The American Journal of Public Health used the film in an experimental program for research on school-based intervention of teenage smoking behavior. 

==References==
 

 
 
 
 
 
 