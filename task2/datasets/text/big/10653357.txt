Cthulhu (2000 film)
 
 
 
{{Infobox film
| name           = Cthulhu
| image          =
| caption        =
| director       = Damian Heffernan
| producer       = Kevin Dunn, Damian Heffernan
| writer         = 
| narrator       = Paul Douglas Melissa Georgiou Malcolm Miller
| music          = Jason Sims
| cinematography = Carl Looper
| editing        =
| distributor    =
| released       = 
| runtime        = 75 min
| country        = Australia English
| budget         =
| preceded by    =
| followed by    =
}}Cthulhu is an  " and The Shadow Over Innsmouth.

Filmed on location in Canberra during the winter of 1996 with additional scenes shot in early 1997, the film was finished digitally in 1998. Most of the filming was completed in the first two week shoot. A large number of the interior locations were shot in the (now demolished) Royal Canberra Hospital which was de-commissioned in 1991 and remained vacant for many years before the building was imploded in July 1997.

Produced on a very low budget the movie was actually shot on 16mm film and transferred to Digital Betacam for finishing. The film also features an entirely original soundtrack by Australian musician Jason Sims.

A rough cut, dual head print was screened for Miramax in the Australian Film, Television and Radio Schools theatre in the hopes of obtaining finishing funds but was not ultimately purchased. The Producers abandoned hope of a theatrical release and raised additional funds to finish on video for a DVD release.
 Channel 9 for screening in Australia as part of their Australian content quota obligations. The film was also purchased by Trend Films in Italy for screening via their Satellite Television network.

The film was most recently screened at as part of a collection of Canberra feature film, known as Local Feats.

==Critical reaction==

In their book  , Andrew Migliore and John Strysik write: "Even though the seams show, the plot creaks, and the acting clips in and out of reality, the makers of Cthulhu really do mean it, and sometimes meaning it is enough. In the end its an earnest effort at weaving together different Lovecraftian motifs into a cohesive movie." 

==Cast== Paul Douglas - Inspector LeGrasse
*Melissa Georgiou - Asenath Waite	
*Malcolm Miller - Professor Armitage 
*James Payne  - Edward Derby
*Adam Somes - Dan Upton

==References==
 

==External links==
* 
*   News of the DVD release
* 
* 
* 

 
 

 
 
 
 
 
 
 


 
 