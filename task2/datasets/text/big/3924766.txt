Atragon
{{Infobox film
| name           = Atragon
| image          = Atragon 1963.jpg
| image_size     =
| caption        =
| director       = Ishirō Honda
| producer       = Tomoyuki Tanaka
| writer         = Shunrō Oshikawa (Kaitei Gunkan|novel) Shigeru Komatsuzaki (Kaitei Okoku|story) Shinichi Sekizawa
| narrator       =
| starring       = Jun Tazaki Yoko Fujiyama|Yōko Fujiyama Tadao Takashima
| music          = Akira Ifukube
| cinematography = Hajime Koizumi
| editing        = Ryohei Fujii Toho Studios AIP (U.S.)
| released       = December 22, 1963 (Japan) March 11, 1965 (U.S.)
| runtime        = 96 min. (Japan) 88 mins (US) Gary A. Smith, The American International Pictures Video Guide, McFarland 2009 p 16 
| country        = Japan
| language       = Japanese English (Dubbed)
| budget         =
| gross          =
}}
 science fiction illustrated story Kaitei Okoku ("The Undersea Kingdom") by illustrator Shigeru Komatsuzaki, serialized in a monthly magazine for boys. Komatsuzaki also served as an uncredited visual designer, as he had on The Mysterians (1957) and Battle in Outer Space (1959). visualizing the titular super weapon, among others.

The film was one of several tokusatsu collaborations of director Ishirō Honda, screenwriter Shinichi Sekizawa, and special effects director Eiji Tsuburaya. It features Jun Tazaki, an authority figure regular to tokusatsu, in his largest genre role as the conflicted Captain Jinguji of the super submarine, 轟天号 Gotengo (or Roaring Heaven). While the name of the ship is recited as "Gotengō" in Japanese, it should be rendered as "Goten" in English; as the suffix, 号 (gō), simply denotes the object as a ship. For the English-language United States|U.S. version, released in 1965 by American International Pictures, the supersub itself was dubbed Atragon, which had been shortened from Tohos own foreign sales title, Atoragon. Confusion over the actual Japanese title of the film by non-Japanese speakers, has led many to assume the original title, 海底軍艦 (Kaitei Gunkan), to be "Undersea Battleship"; unfortunately, the Japanese term for "Battleship", 戦艦 (Senkan), is nowhere to be found in the title. Since 軍艦 (Gunkan) should be correctly rendered as "Warship", therefore, the film should be correctly transliterated as Undersea Warship.

As was the case in several other 1960s   and  , as well as the video game  .

There is also an anime version, a two episode OVA named Super Atragon based on the same novels made in 1995 by Phoenix Entertainment. A dub of the OVA was made by ADV Films.

==Synopsis== Mu reappears Captain has created the greatest warship ever seen, and possibly the surface worlds only defense.
 Mu Empire. Foiled by the ensuing photographers, he flees into the ocean.
 geothermal "sun") UN realizes tropical island inhabited only by Jingujis forces and enclosing a vast underground dock.

 
Eventually Captain Jinguji (Tazaki) greets the visitors, though he is cold toward his daughter and infuriated by Kusumis appeal. He built Atragon, he explains, as a means to restore the Japanese Empire after its defeat in World War II, and insists that it be used for no other purpose. Makoto runs off in anger, later to be consoled by Susumu. Atragons test run is a success, the heavily armored submarine even elevating out of the water and flying about the island. When the Captain approaches Makoto that evening they exchange harsh words; again Susumu reproaches the Captain for his selfish refusal to come to the worlds aid. After Makoto and Shindo are kidnapped by the reporter (a disguised Mu agent) and the base crippled by a bomb, Jinguji consents to Kusumis request and prepares Atragon for war against Mu.
 Absolute Zero Cannon". Jinguji offers to hear peace terms, but the proud Empress refuses. The Captain then advances Atragon into the heart of the Empire (power room) and freezes its geothermal machinery. This results in a cataclysmic explosion visible even to those on deck of the surfaced submarine. Her empire dying, the Mu Empress abandons the Atragon and, Jinguji and company looking on, swims into the conflagration.

==Production and distribution==
  theatrical poster for the 1965 U.S release of Atragon.]]
  glances up at the Manda prop during filming of the Manda vs Atragon scene]]
 The Last War (1961).

Kaitei Gunkan became Tohos top box office earner during its month-long run in Japanese theaters and is a popular feature on TV and at film festivals. In fact, it was so popular that it was re-released in 1968 as the support feature for Hondas Destroy All Monsters. It was also the 1964 Japanese entry at the Trieste Science Fiction Film Festival. 
 dubbing by contraction of international version. This alternate dubbed version syncs up perfectly with the Japanese video, but fans generally consider these international dubs to be inferior. Its enduring popularity in Japan is evident in the number of plastic model kits, garage kits, and adult-targeted toys based on the Goten continually on the market.

==Themes== revolted in order to prepare a counterattack, the Empress acts differently on her nationalistic resolve by abandoning the Atragon to die with her people.

==Cultural references==
Space ships resembling those seen in the movie appeared in the video game  . The games cutscenes suggest that these ships are piloted by evil aliens from Planet X, who declare war on Earth in the year 2XXX. Other enemies within the game resemble the creatures from Matango.

There is a Doom Metal band from Edinburgh, Scotland who take their name from the film.

==Cast==
{| class="wikitable"
! Actor
! Role
|- 
| Tadao Takashima || Susumu Hatanaka, Photographer
|-
| Yoko Fujiyama|Yōko Fujiyama || Makoto Jinguji, daughter of Captain Jinguji
|-
| Yu Fujiki || Yoshito Nishibe, Assistant Photographer
|-
| Ken Uehara || Rear Admiral Kusumi (Ret.), Kokoku Shipping Company
|-
| Jun Tazaki || Captain Hachiro Jinguji, Imperial Japanese Navy
|-
| Kenji Sahara || Umino, Journalist/Mu Agent
|-
| Hiroshi Koizumi || Detective Ito, Tokyo Metropolitan Police
|-
| Yoshifumi Tajima || Seaman Saburo Amano
|- Hiroshi Hasegawa || Lieutenant Junior-grade Fuji
|-
| Akihiko Hirata || Mu Agent #23
|-
| Tetsuko Kobayashi || Empress of Mu
|-
| Hideyo Amamoto || High Priest of Mu
|-
| Minoru Takada || Commander, Japan Defense Agency
|-
| Susumu Fujita || Officer A, Japan Defense Agency
|-
| Mitsuo Tsuda || Officer B, Japan Defense Agency
|-
| Shin Otomo || Officer C, Japan Defense Agency
|-
| Ikio Sawamura || Taxi Driver
|-
| Akemi Kita || Rimako, the Bikini Model
|-
| Hisaya Ito || Shindo, Kidnapped Civil Engineer
|-
| Nadao Kirino || Kidnapped Civil Engineer
|-
| Tetsu Nakamura || Merchant Ship Captain
|-
| Yutaka Nakayama || Lookout on Cargo Ship
|-
| Wataru Omae || Radar Operations Officer
|-
| Shoichi Hirose || Mu Empire Subject
|-
| Katsumi Tezuka || Mu Empire Subject
|-
| Koji Uno || Constable, Oshima Island
|-
| Yukihiko Gondo || Bus Passenger, Mt. Mihara
|-
| Yutaka Oka || Bus Passenger, Mt. Mihara
|-
| Yasuzo Ogawa || Mu Empire Subject, Attached to High Priest
|}

==Production credits==

*Executive Producer - Tomoyuki Tanaka
*Screenplay - Shinichi Sekizawa (Based on Kaitei Gunkan by Shunro Oshikawa)
*Director - Ishirō Honda
*Visual Effects Director - Eiji Tsuburaya
*Cinematographer - Hajime Koizumi
*Production Designer - Takeo Kita
*Assistant Director - Koji Kajita
*Sound Recordist - Masanao Uehara
*Lighting - Shoshichi Kojima
*Music - Akira Ifukube
*Sound Editor - Hisashi Shimonga
*Assistant Director - Koji Kajita
*Film Editor - Ryohei Fujii
*Sound Effects - Minoru Kaneyama
*Film Development - Far East Laboratories
*Production Manager - Shigeru Nakamura
*Visual Effects Photography - Sadamasa Arikawa and Sokei Tomioka
*Optical Photography - Yukio Manoda and Yoshiyuki Tokumasa Akira Watanabe
*Conceptual Designer - Shigeru Komatsuzaki (uncredited)
*Composites - Hiroshi Mukoyama
*Assistant Visual Effects Director - Teruyoshi Nakano
*Visual Effects Production Manager - Tadashi Koike

==References==
 

==Bibliography==
* Godziszewski, Ed (1995). "Atragon: A Toho Classic Revisited".   #21: 18–33.
*  
*  
* Ragone, August (2007).   San Francisco, California: Chronicle Books. ISBN 978-0-8118-6078-9.

==External links==
*  
*  , anime version
*  , anime version
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 