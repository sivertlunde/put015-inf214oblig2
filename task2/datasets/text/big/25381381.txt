Manthiri Kumari
 
{{Infobox film
| name = Manthiri Kumari மந்திரி குமாரி
| image = Manthiri kumari.jpg
| caption  = Theatrical poster
| director = Ellis R. Dungan
| writer =  M. Karunanidhi
| starring = M. G. Ramachandran M. N. Nambiar S. A. Natarajan G. Sakunthala
| producer = T. R. Sundaram
| music = G. Ramanathan
| studio = Modern Theatres
| distributor = Modern Theatres
| cinematography = J. G. Vijayam
| editing = L. Balu
| released =  24 June 1950
| runtime = 163 mins
| country = India Tamil
| budget =
}}

Manthiri Kumari ( ) is a 1950   sang for very first time for MGR in this film. 

==Production==
Manthiri Kumari (lit. The ministers daughter) was the film version of a play written by M. Karunanidhi and based an incident that occurs in the Tamil epic poem Kundalakesi (One of the five Great Tamil epics). T. R. Sundaram of Modern Theatres had previously produced a Dungan directed film Ponmudi (1950 film)|Ponmudi (1950) in which Karunanidhi had worked as a script writer. Sundaram decided to make a film based on the play and hired Dungan to direct it (the credits show Sundaram and Dungan as co-directors of the film). M. G. Ramachandran (MGR) who had played the supporting roles in many of Dungans earlier films had recently achieved success as a hero in Rajakumaari (1947) and Marudhanaattu Ilavarasi (1950). Karunanidhi recommended that his friend MGR be made hero for the new film. Sundaram agreed with a caveat - MGRs double chin had to be hidden behind a beard. G. Ramanathan was hired to compose the music. The lyrics for the songs were written by A. Marudhakaasi and Ka. Mu. Sherriff. 

==Plot==
The king of Mullai nadu is dominated by his Raja guru (head priest) (M. N. Nambiar). The guru wants his son Parthiban (S. A. Natarajan) to be appointed as the General of the army. But the king appoints Veera Mohan (MGR) instead. The enraged Parthiban becomes a bandit and starts raiding the country side. He wants to marry the princess Jeevarekha (G. Shakuntala) who is in love with Veera Mohan. Parthiban sends a message to Jeevarekha to meet him secretly. The message is delivered by mistake to the ministers daughter Amudhavalli (Madhuri Devi) and she goes to meet Parthiban. Parthiban and Amudhavalli fall in love. Meanwhile, the king sends his general Veeramohan to capture the bandits plaguing the country side. Veeramohan captures Parthiban and produces him in the royal court. The Raja guru is enraged and tries to get his son off by various means. He demands a trial for his son in front of the Goddess. During the trial, Amudhavalli hides behind the Goddess statue and pronounces Parthiban as innocent. She blames Veeramohan for the banditry. The king believing that the Goddess had spoken releases Parthiban and exiles Veeramohan. Parthiban and Amudhavalli are happily married. But goaded by his father Parthiban wants to take over the kingdom by marrying the princess. He decides to kill Amudhavalli. He tricks her into going with him to a cliff edge and tells her of his intention to kill her. Amudavalli begs him for a chance to worship him by going around him three times before she meets her death. Parthiban grants her last wish. While going around him she pushes him to his death from behind. Shocked by her actions and her husbands betrayal, she confesses her sins and becomes a Buddhist nun. The raja guru is jailed and Veera mohan is reunited with the princess.

==Cast==
{| class="wikitable" width="50%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| M. G. Ramachandran || General Veera Mohan
|-
| S. A. Natrajan || Parthiban
|-
| M. N. Nambiar || The Rajaguru
|-
| G. Shakuntala || Princess Jeevarekha
|-
| Madhuri Devi || Amudhavalli
|-
| T. P. Muthulakshmi || 
|-
| A. Karunanidhi || 
|-
| K. S. Angamuthu || 
|-
| Sivasuriyan || 
|-
| K. V. Seenivasan || 
|-
| K. K. Soundar || 
|}

==Crew==
*Producer: T. R. Sundaram, Modern Theatres
*Director: Ellis R. Dungan  
*Screenplay: M. Karunanidhi
*Dialogue:M. Karunanidhi 
*Music: G. Ramanathan 
*Lyrics: Ka. Mu. Sheriff, A. Marudhakasi
*Cinematography: J.G.Vijayam
*Sound Recording: K. B. S. Mani, Padmanaban, Raju 
*Assistant Director: T. P. S. Mani & K. Somu  Ragini

==Reception==
The film was released in June 1950 and became a box office hit. Though MGR was the hero, it was S. A. Natarajans role which received the most acclaim. Karunanidhis fiery dialogues became famous and stirred controversy.   

==Soundtrack== Playback singers are Thiruchi Loganathan, T. M. Soundararajan, M. L. Vasanthakumari, P. Leela, N. Lalitha & U. R. Chandra.

The song Vaarai Nee Vaarai sung by Trichy Loganathan and Jikki was the most popular song of the film. List of songs in the film: 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Vaarai Nee Vaarai || Thiruchi Loganathan & Jikki || || 02:52
|- 
| 2 || Isai Tamizhe Inidhaana || M. L. Vasanthakumari || || 03:32
|- 
| 3 || Ulavum Thendral || Thiruchi Loganathan & Jikki || || 03:17
|- 
| 4 || Kaadhal Baliyaagi || M. L. Vasanthakumari || || 02:45
|- 
| 5 || Ubakaram Seibavarukke.... Annam Itta Vittile || T. M. Soundararajan || A. Maruthakasi || 02:07
|- 
| 6 || Manam Pola Vaazhvu Peruvome || M. L. Vasanthakumari & Jikki || || 02:38
|- 
| 7 || Kannadichi Yaarai Neeyum || A. P. Komala || || 03:29
|- 
| 8 || Porakka Poguthu || A. Karunanidhi & T. P. Muthulakshmi || || 01:18
|- 
| 9 || Aahaahaahaa Vaazhvile || M. L. Vasanthakumari || || 03:07
|- 
| 10 || O Raja O Rani Indha Ezhaiyeliya || P. Leela, N. Lalitha and U. R. Chandra || || 05:21
|- 
| 11 || Anthisaayura Neram Mandhaarai Chedi Oram ||  || || 02:20
|- 
| 12 || Pengalinaal || Jikki || || 01:54
|- 
| 13 || Ennum Pozhuthil Inbam || M. L. Vasanthakumari || || 01:45
|- 
| 14 || En Erumai Kannukutti ||  || || 02:53
|- 
| 15 || Aadhavan Udhitthu Tamarai Malarndhadhu ||  || || 04:31
|}

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 