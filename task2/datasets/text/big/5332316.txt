Feed (film)
 
{{Infobox film
| name           = Feed
| image          = Feedposter.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Brett Leonard Melissa Beauford
| writer         = Kieran Galvin Alex OLoughlin Patrick Thompson Jack Thompson
| music          = Gregg Leonard Geoff Michael
| cinematography = Steve Arnold
| editing        = Mark Bennett
| studio         = Honour Bright Films Becker Films International
| distributor    = Force Entertainment   TLA Releasing  
| released       =  
| runtime        = 101 minutes
| country        = Australia
| language       = English German
| budget         = 
}} thriller film Rotenburg Cannibal".

==Plot==
Australian cop Phillip (Patrick Thompson) works as a Cybercrime investigator for Interpol. Phillip finds himself shaken after investigating a case in Hamburg, Germany, in which a man consents to have his penis cut off and eaten by his lover. Phillips own relationship is troubled due to his frequent travel and difficulties with romantic intimacy, and he finds himself unable to respond positively to his beautiful girlfriends sexual overtures. The two have rough sex that gets out of hand, and she leaves him after writing "pig" on his chest with lipstick.

Meanwhile, Phillip has been working with his partner, Nigel (Matthew Le Nevez), to investigate a fetish website that features morbidly obese women being held captive and fed fattening food. The websites intricate encryption suggests that the webmaster is concealing a deeper perversion, and, despite the objections of his superiors, Phillip travels to Toledo, Ohio, to investigate the webmaster and determine the whereabouts of "Lucy," a former site favorite. In Ohio, the sites sadistic webmaster, Michael Carter (Alex OLoughlin), holds Deidre (Gabby Millgate) captive in a ramshackle cottage in the woods. After questioning a local priest, Michaels adoptive sister, and his thin, attractive wife, Phillip manages to track Michael to the cottage, where the latter is preparing to feed Deidre a thick slurry of eggs and weight gain powder. Phillip learns that Michael developed a sexual fascination with obese women due to his troubled relationship with his overweight, immobile mother, who died when he was a child. He also uncovers the twist in Michaels fetish website: not only are paying site members able to watch him feed and fornicate with obese women, but they can place bets on when each woman will die, using posted statistics on their body proportions, blood pressure, and other medical indicators. 

In the cottage, Phillip finds Lucys decaying remains and then confronts Michael; Michael reveals that he killed his mother and fed Lucy until she died. The slurry-like preparation he was attempting to feed Deidre through a tube contains some of the fat he had carved from Lucys body. After a struggle, Phillip shoots Deidre, who maintains her love for Michael even as Philip tells her about his deceptions, and two shots can be heard off screen.

The final scene reveals Phillip living in suburban bliss with Michaels overweight adoptive sister. He takes some sandwiches she has packed for him and drives to the cottage in the woods, where he eats them with gusto, pausing to tantalize a wheelchair-bound Michael with one. Michael, starving and emaciated, begs Phillip to "Feed me."

==Cast==
* Alex OLoughlin as Michael Carter
* Patrick Thompson as Phillip Jackson
* Gabby Millgate as Deidre Jack Thompson as Richard
* Rose Ashton as Abbey
* Matthew Le Nevez as Nigel David Field as Father Turner

==Reception==
Feed currently holds a 40% rotten rating on review aggregate website Rotten Tomatoes. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 