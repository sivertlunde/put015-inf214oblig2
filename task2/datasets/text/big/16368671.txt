La Damigella di Bard (film)
{{Infobox film
| name           =La Damigella di Bard
| image          =
| image_size     =
| caption        =
| director       = Mario Mattoli
| producer       =
| writer         = Salvator Gotta (play), Mario Mattoli (screenplay)
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       = 1936
| runtime        = 75 minutes
| country        = Italy
| language       = Italian
}}
 1936 cinema Italian comedy film directed by Mario Mattoli.

==Cast==
*Emma Gramatica as  Maria Clotilde di Bard
*Luigi Cimara as  Il marchese Luciano di Pombia
*Carlo Tamberlani as  Ferdinandi di Bard
*Cesare Bettarini as  Franco Toscani
*Amelia Chellini as  Signora Ponzetti
*Luigi Pavese as Avv. Palmieri
*Achille Majeroni as  Il padre di Maria (as Achile Majeroni)
*Olga Pescatori as  Denise
*Mirella Pardi as Renata
*Armando Migliari as  Amilcare Pacotti
*Vasco Creti as Papa Ponzetti
*Romolo Costa as  Il conte Amedeo
*Rina Valli as  Orsolina
*Mario Brizzolari as Lufficiale giudiziario
*Eugenio Duse as  Filippo Carli

==External links==
*  

 

 
 
 
 
 
 
 
 

 
 