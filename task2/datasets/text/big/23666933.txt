Born of Unknown Father
{{Infobox film
| name           = Born of Unknown Father
| image          =
| image_size     =
| caption        =
| director       =Maurice Cloche
| producer       =
| writer         = Maurice Cloche
| narrator       =
| starring       =
| music          =Wal Berg
| cinematography =Claude Renoir
| editor       =
| distributor    =
| released       = 1950
| runtime        = 75 minutes
| country        =France
| language       = French
| budget         =
}}
Born of Unknown Father ( ) is a 1950 French film directed by Maurice Cloche.

==Cast==
*Yvonne Dany
*Irasema Dilián	... 	Rose Dormoy
*Gabrielle Dorziat	... 	Mme. Mussot
*Irene Genna	... 	Antoinette
*Gilbert Gil	... 	Raymond Denis
*Jean-Pierre Kérien	... 	Claude Nogent
*Claude Le Lorrain
*Charles Lemontier	... 	Le président
*Héléna Manson	... 	Mme Denis
*Renzo Merusi	... 	Pierre Neville
*Gaby Morlay	... 	Mme. Nogent
*Van Mullen	... 	Maître Mussot
*Sandro Ruffini	... 	Henri Mussot
*Nicole Stéphane	... 	Jacqueline Mussot
*Max Tréjean
*Janine Viénot	... 	La surveillante de lAssistance Publique

==External links==
*  

 
 
 
 

 