Story of a Prostitute
{{Infobox film
| name           = Story of a Prostitute
| image          = Story of a Prostitute poster.jpg
| caption        =
| director       = Seijun Suzuki
| producer       = Kaneo Iwai
| writer         = Hajime Takaiwa Taijiro Tamura (novel)
| starring       = Tamio Kawachi Yumiko Nogawa
| music          = Naozumi Yamamoto
| cinematography = Kazue Nagatsuka
| editing        = Akira Suzuki
| distributor    = Nikkatsu
| released       = February 28, 1965
| runtime        = 96 minutes
| country        = Japan Japanese
| budget         =
| gross          =
}} 1965 Cinema Japanese film directed by Seijun Suzuki. It is based on a story by Taijiro Tamura who, like Suzuki, had served as a soldier in the war.

==Synopsis==
Disappointed by the marriage of her lover to a woman he does not love, prostitute Harumi drifts from the city to a remote Japanese outpost in Manchuria to work in a "comfort house," or brothel, during the Sino-Japanese war. The commanding adjutant there takes an immediate liking to the new girl, but she is at first fascinated, and comes to love, Mikami, the officers aide. At first he is haughty and indifferent to the girl, which enrages her, but they are drawn together eventually. Abused and manipulated by the adjutant, she grows to hate the officer and seeks solace in Mikamis arms. They carry on a clandestine affair, which is a dangerous breach of code for both of them.

Tragedy strikes when the Chinese attack the outpost, and Mikami is severely wounded in a trench. Harumi runs to him and they are both captured by the enemy while he is unconscious. The Chinese dress his wounds and he is given the opportunity to withdraw with them; but as a Japanese soldier, he is bound by a code not to be captured at all, and only Harumis intervention prevents him from killing himself. Once again in the custody of the Japanese, Harumi is sent back to the brothel, and Mikami is to be court-martialed and executed in disgrace. During another attack on the outpost, he escapes with Harumis aid, but instead of fleeing with her, he intends to blow himself up to clear his and his battalions honor. She leaps on his body and they die together.

==Cast==
* Yumiko Nogawa as Harumi
* Tamio Kawaji as Pvt. Shinkichi Mikami
* Isao Tamagawa as Lt. Narita
* Shoichi Ozawa as Sgt. Akiyama

==Production==
In 1950, Tamuras story was made into a more romantic film, Escape at Dawn, co-written by Akira Kurosawa and directed by Senkichi Taniguchi. For the Nikkatsu adaptation, Suzuki drew upon his firsthand experience at the wartime front to portray the conditions and behavior in a more realistic light. What was presented in the film and the actual conditions "probably arent that different," Suzuki said in a 2005 interview. Most Japanese war movies portrayed the era with healthy doses of tragedy, but Suzuki infused an air of ludicrousness in his film. His own opinion of the wartime military experience was that beside the brutality, it was "extremely comical and absurd." {{cite video
 | people = Seijun Suzuki|Suzuki, Seijun; Takeo Kimura; Tadao Sato date = 2005
 | title = Story of a Prostitute (Interviews)
 | url = http://www.criterion.com/asp/release.asp?id=299
 | accessdate = 2007-01-01
 | medium = DVD
 | publisher = The Criterion Collection
}} 

Suzuki could not film on location in China, so studio sets and lookalike locations in Japan were used. 

==Reception==
At this time, Suzuki was at regular odds with Nikkatsu, his studio. He would be given a project and expected to direct it in workmanlike fashion, but he consistently introduced stylistic nuances and levels of realism that the studio resented. 

Suzuki didnt at first get critical attention as a "B-movie director." By 1965, some critics began to take note of his style, but this film had very mixed reviews, mostly disappointing. In this case, the first version of Tamuras story was chaste (the girls were "entertainers," not "comfort women") and romantically inclined, and Suzuki portrayed a far more realistic portrait of the ugly side of Japanese military life. Critic Tadao Sato noted that most films portray the very meditative and serene traditions that Japanese are known for, but Suzuki also dramatizes "another tradition that is the complete opposite ... very disorderly and grotesque." This and the realistic onscreen depiction of sexuality and sadism didnt win over audiences. 

 

==Release==

===Home media===
Story of a Prostitute has been released on DVD as part of The Criterion Collection.

==References==
 

==External links==
*  
*  
*  
*     at the Japanese Movie Database

 

 
 
 
 
 
 
 
 
 
 
 
 