The Idol (1923 film)
{{Infobox film
| name           = The Idol
| image          =
| image_size     = 
| caption        = 
| director       = Wiktor Bieganski
| producer       = Józef Szwajcer
| writer         = Gustaw Kamienski
| narrator       = 
| starring       = Ryszard Sobiszewski   Norbert Wicki   Antoni Piekarski   Maria Lubowiecka
| music          = 
| editing        = 
| cinematography = Stanislaw Sebel
| studio         = Polfilma 
| distributor    = 
| released       = 18 May 1923
| runtime        = 
| country        = Poland
| language       = Silent   Polish intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} Polish silent silent drama film directed by Wiktor Bieganski and starring Ryszard Sobiszewski, Norbert Wicki and Antoni Piekarski. The film was shot and set in the Tatra Mountains. 

==Cast==
*  Ryszard Sobiszewski as Jan Zadroga, painter 
* Norbert Wicki as Wolski, sculptor 
* Antoni Piekarski as Wezyk 
* Maria Lubowiecka as Stasia Wezykówna 
* Jerzy Starczewski as Jan Zadroga, young 
* Leon Trystan as Wolski, young 

==References==
 

==Bibliography==
* Haltof, Marek. Polish National Cinema. Berghahn Books, 2002.

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 

 