Island Wives
{{infobox film
| name           = Island Wives
| image          = Island Wives (1922) - 2.jpg
| caption        = Corinne Griffith
| director       = Webster Campbell
| producer       = Vitagraph Company of America
| writer         = Bob Dexter(story) William B. Courtney
| starring       = Corinne Griffith
| music          =
| cinematography = Arthur Ross
| editing        =
| distributor    = Vitagraph company of America
| released       = March 12, 1922
| runtime        = 5 reels; 4,994 feet
| country        = USA
| language       = Silent ...English titles
}} lost  1922 silent film adventure drama directed by Webster Campbell and starring Corinne Griffith. The Vitagraph Company of America produced and distributed. Some of the film was shot in Florida. 

The famous writer John Galsworthy played a role in this film.


==Cast==
*Corinne Griffith - Elsa Melton
*Charles Trowbridge - Jimmy
*Rockliffe Fellowes - Hitchens
*Ivan Christy - McMasters
*Edna Hibbard - Piala
*Norman Rankow - Bibo
*Peggy Parr - McMasters Native Wife
*J. Barney Sherry - Yacht Captain
*John Galsworthy - Lester
*Mrs. Trowbridge - Mrs. Lester

==References==
 


==External links==
* 
* 
* 

 
 
 
 

 