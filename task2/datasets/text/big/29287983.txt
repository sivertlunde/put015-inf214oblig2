The Thirteenth Hour
 
{{Infobox film
| name           = The Thirteenth Hour
| image          = The Thirteenth Hour - 1927.jpg
| image size     = 
| alt            = 
| caption        = 1927 lobby card Chester M. Franklin
| producer       = 
| writer         = Edward T. Lowe Jr.
| screenplay     = Chester M. Franklin Douglas Furber
| story          = Chester M. Franklin Douglas Furber Intertitles: Wellyn Totman
| based on       =  
| narrator       = 
| starring       = Lionel Barrymore Jacqueline Gadsden Charles Delaney
| music          = 
| cinematography = Max Fabian
| editing        = Dan Sharits
| studio         = 
| distributor    = Metro Goldwyn Mayer
| released       =  
| runtime        = 6 reels, 5,252 feet
| country        = United States English intertitles)
}}
 1927 silent film mystery produced and distributed by Metro Goldwyn Mayer and directed by Chester Franklin.  The film stars Lionel Barrymore in a role where, as noted criminologist Professor Leroy, he dons a weird series of disguises to hide a dark secret.     This was the first film where Barrymore was cast opposite talented dogs,  and the first where he was cast as a serial killer. 

A print of this film survives in 16mm. 

==Plot==
Junior detective Gray (Charles Delaney) discovers that the eccentric criminologist Professor Leroy (Lionel Barrymore) is both a crook and a murderer.

==Cast==
* Lionel Barrymore as Professor Leroy
* Jacqueline Gadsden as Mary Lyle (as Jacqueline Gadsdon)
* Charles Delaney as Matt Gray
* Fred Kelsey as Detective Shaw
* Polly Moran as Polly
* Napoleon the Dog  as the dog Sojin Kamiyama

==See also==
*Lionel Barrymore filmography

==References==
 

 {{cite news
|url=http://books.google.com/books?id=MIoQAAAAIAAJ&q=%22The+Thirteenth+Hour%22,+%22Lionel+Barrymore+%22&dq=%22The+Thirteenth+Hour%22,+%22Lionel+Barrymore+%22&hl=en&ei=gFXwTPHZNIeosAOfysGmCw&sa=X&oi=book_result&ct=result&resnum=10&ved=0CFcQ6AEwCQ
|title=The Thirteenth Hour
|year=1927
|work=Time Magazine
|pages=Volume 10
|accessdate=27 November 2010}} 

 {{cite book
|title=The Educational screen, Volume 7
|url=http://books.google.com/books?id=e1VOAAAAYAAJ&q=%22The+Thirteenth+Hour%22,+%22Lionel+Barrymore+%22&dq=%22The+Thirteenth+Hour%22,+%22Lionel+Barrymore+%22&hl=en&ei=PG3wTJzPN4LQsAOQz8CoCw&sa=X&oi=book_result&ct=result&resnum=3&ved=0CDIQ6AEwAjgK
|year=1928
|publisher=Educational Screen, Inc.
|page=16}} 

 {{cite book
|author=Lionel Barrymore, Cameron Shipp
|title=We Barrymores
|edition=illustrated
|year=1974
|publisher=Greenwood Press
|isbn=9780837175508
|page=258}} 

 {{cite book
|last=Rigby
|first=Jonathan 
|title=American Gothic: Sixty Years of Horror Cinema
|url=http://books.google.com/books?ei=gFXwTPHZNIeosAOfysGmCw&ct=result&id=TqUqAQAAIAAJ&dq=%22The+Thirteenth+Hour%22%2C+%22Lionel+Barrymore+%22&q=%22The+Thirteenth+Hour%22
|edition=illustrated
|year=2007
|publisher=Reynolds & Hearn
|isbn=9781905287253
|page=53}} 

 {{cite book
|last=Pitts
|first=Michael R. 
|title=Famous movie detectives III, Volume 3
|url=http://books.google.com/books?id=xSmWVsDh8WEC&pg=PA285&dq=%22The+Thirteenth+Hour%22,+%22Lionel+Barrymore+%22&hl=en&ei=gFXwTPHZNIeosAOfysGmCw&sa=X&oi=book_result&ct=result&resnum=4&ved=0CDkQ6AEwAw#v=onepage&q=%22The%20Thirteenth%20Hour%22%2C%20%22Lionel%20Barrymore%20%22&f=false
|edition=illustrated
|series=G - Reference, Information and Interdisciplinary Subjects Series
|year=2004
|publisher=Scarecrow Press
|page=285}} 

 {{cite book
|last=Everson
|first=William K.
|title=The detective in film
|url=http://books.google.com/books?ei=PG3wTJzPN4LQsAOQz8CoCw&ct=result&id=J88dAAAAMAAJ&dq=%22The+Thirteenth+Hour%22%2C+%22Lionel+Barrymore+%22&q=%22The+Thirteenth+Hour%22
|edition=illustrated
|year=1972
|publisher=Citadel Press
|isbn=9780806502984
|pages=29, 35, 217}} 

 

==External links==
*  at the Internet Movie Database
* 
* 

 
 
 
 
 
 
 
 