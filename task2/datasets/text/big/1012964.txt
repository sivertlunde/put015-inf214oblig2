Blood Circus (film)
 
 science fiction movie, with a Professional wrestling|professional-wrestling theme, produced in 1985.   The movie was produced by Baltimore-native Santo Victor Rigatuso, also called Robert "Bob" Harris, who promoted it through infomercials for his mail-order "Santo Gold" jewelry business.

==Plot== Aliens from the planet Zoran are sent to Earth to fight against professional wrestlers from the United States and the Soviet Union, who prove actually to be Cannibals|man-eaters who devour their opponents upon defeating them.

==Production==
Filming for Blood Circus began in 1985. After spending two years editing it, Rigatuso could not find a distributor for the film. He ended up renting several theaters in the Baltimore area to show his film. It was shown for only a week, and took in far less than the $2 million it cost to make the movie. The movie was never shown to the public after it ended its initial run, and the original copy of the film was believed to have been lost.  Clips of Blood Circus can still be seen in portions of "Santo Gold" infomercials circulating on the internet.
 World Wrestling Douglas “Ox” Baker, an experienced actor in his own right.
 Baltimore Civic Center, where Rigatuso, playing a character called Santo Gold, performs a song before the climactic wrestling match. The song lyrics have nothing to do with the movie; instead, the song promotes Rigatusos "Santo Gold" jewelry.
 poem about Blood Circus on each side, as well as a coupon for a free diamond ring from Rigatusos "Santo Gold" infomercials.

In 2008, the Santo Gold website  announced that the 35mm negatives of Blood Circus had finally been found, and that producers were being sought for its release.

==References==
 

==External links==
* 
* 
*YouTube -  
 

 
 
 
 
 
 


 