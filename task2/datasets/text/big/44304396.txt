The Narrow Road
{{Infobox film
| name           = The Narrow Road
| image          =
| caption        =
| director       = D. W. Griffith
| producer       = Biograph Company
| writer         = George Hennessy
| starring       = Elmer Booth Mary Pickford
| music          =
| cinematography = Billy Bitzer
| editing        =
| distributor    = General Film Company
| released       = August 1, 1912
| runtime        = 17 minutes
| country        = USA
| language       = Silent..English titlecards
}}
The Narrow Road is a 1912 short silent film directed by D. W. Griffith and produced and distributed by the Biograph Company. 

A short Biograph film preserved from the Library of Congress paperprint collection. 

==Cast==
*Elmer Booth - Jim Holcomb
*Mary Pickford - Mrs. Jim Holcomb
*Charles Hill Mailes - The Counterfeiter
*Jack Pickford -

other cast
*Christy Cabanne - A Tramp (as W. Christy Cabanne)
*Max Davidson - A Tramp Frank Evans - A Prison Guard Charles Gorman - A Detective
*Grace Henderson - 
*Harry Hyde -
*J. Jiquel Lanoe - A Prisoner/The Foreman
*Adolph Lestina - A Bartender
*Alfred Paget - A Detective
*W. C. Robinson - A Prison Guard

==References==
 

==External links==
* 
* 
*  available for free download at  

 

 
 
 
 
 
 
 
 
 


 