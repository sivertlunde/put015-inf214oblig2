Chandrawal
{{Infobox film
| name           = Chandrawal
| image          = File:Chandrawal film Poster.jpg
| image size     =
| alt            =
| caption        =
| director       = Jayant Prabhakar
| producer       = Usha Sharma
| writer         = Devi Shankar Prabhakar
| screenplay     = Devi shankar prabhakar
| story          =
| based on       =
| narrator       =
| starring       =Jagat Jakhar Usha Sharma
| music          = J. P. Kaushik
| cinematography =
| editing        =
| studio         = Prabhakar Films
| distributor    =
| released       = March 1984
| runtime        =
| country        = India Haryanvi
| budget         =
| gross          =
}}
 Haryanvi film.  It is the third List of Haryanvi-language films|Haryanavi-language film to be released and the first financially successful one.  Written by Late. Shri Devi Shankar Prabhakar,produce by usha sharma and directed by Jayant Prabhaker, the films music was created by J. P. Kaushik. Starring dancer Usha Sharma, the film tells the story of a tragic love affair between Chandrawal, a girl of the Gadia Lohar community, and Suraj, a haryanavi Jat Boy. It was released in March 1984 and became very successful, spurred on by a popular soundtrack.  A sequel is in development and will be produced by Usha Sharma and directed by Nishant Prabhakar.

==Plot== Chittoor with the pledge that they would never settle down at a permanent place under roof till they again hoist Maharanas flag at Chitoor fort, and since then the tribe has been roaming about as gypsies, doing Lohars jobs for the people with great honesty and hard work. They never allow their girls to marry the boys of other castes. During their unending journey, Jodha Sardar was camping near Ram Garh village and here the young Suraj first saw Chanrdrawal dancing at a marriage ceremony in the camp. Both the young hearts fell in love at first sight. The love affair becomes an open secret soon and Ranjit Chaudhary the father of Suraj who is also the village Chaudhary becomes very angry. On the other hand Jodha Sardar also reacts very strongly and decides to leave the village at once as he does not like that Chandrawal and Suraj are given any chance to meet. He, before leaving the haveli of Chaudhary Ranjit, warns Suraj that if ultimately with the help of his friends he succeeds in finding out the place of his camp. On seeing Chandrawal after a long time, Suraj becomes so emotional that he runs towards the Dera crying - Chandra. Chandrawal also sees him and immediately rushes towards him without caring for the warnings of her grandfather that he would cut her into pieces with a long knife. At the same time Suraj sees Jodha Sardar throwing a knife to kill Chandrawal. Suraj at once turns himself to the side and takes the fatal attack on Chandrawal upon himself. Realising that Suraj is dead, Chandrawal snatches the knife out of the body of her lover and sacrifices herself there and then. 

==Cast==
* Jagat Jakhar - Suraj
* Usha Sharma - Chandrawal
* Nasib Singh Kundu - Roondah
* Daryav Singh Malik - Khoondah
* Manphool Chand Dangi - Jodha Sardar

==Box office== Western U.P., Delhi and parts of Rajasthan,    grossing more than Hindi films like Sholay and Bobby (1973 film)|Bobby in these areas.  Chandrawal celebrated its Silver Jubilee at Gagan Cinema in Faridabad, recovering its entire cost at that one theatre alone.  While the movie was in theatres, the sight of hundreds of villagers coming in large-groups boarding rural tractor-trolleys and trucks to watch Chandrawal at theatre, was commonplace. This movie was popular among all age groups. This movie was particularly instrumental in making the Haryanvi folksongs and music a popular choice in mainstream Indian music industry especially Indian pop|Indipop. Chandrawal has set a world record when the Producers honoured a dozen of cine goers of Haryana and Western U.P. who witnessed Chandrawal more than 200 times. 

==Soundtrack==
* "Jija tu kala main gori ghani"
* "Gaade Aali Gajban Chori"
* "Main suraj tu chandrawal"
* "Nain katore kajal dore"
* "Mera Nau Dandi Ka Beezna"
* "Mera Chundar manga de ho"

==References==
 

==External links==
*  at the IMDB

 
 
 
 