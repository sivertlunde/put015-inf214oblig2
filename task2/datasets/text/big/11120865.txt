Butterfly and Sword
 
 
{{Infobox film name = Butterfly and Sword image = Butterfly-sword-poster.jpg alt =  caption = Film poster traditional = 新流星蝴蝶劍 simplified = 新流星蝴蝶剑 pinyin = Xīn Líuxīng Húdié Jiàn jyutping = San1 Lau4 Sing1 Wu4 Dip6 Gim3}} director = Michael Mak producer = Chu Yen-ping screenplay = John Chong story = Gu Long starring = Tony Leung Michelle Yeoh Jimmy Lin Joey Wong Donnie Yen music = Stephen Shing Chris Babida cinematography = Chan Wing-shu editing = Ma Chung-yiu Wong Jing-cheung Mui Tung-lit studio = Chang-Hong Channel Film & Video Co. distributor = Regal Films Distribution Tai Seng Video Marketing released =   runtime = 88 minutes country = Hong Kong language = Cantonese budget =  gross =
}} Tony Leung, Michelle Yeoh, Jimmy Lin, Joey Wong and Donnie Yen.

==Plot==
Butterfly has nothing to do with the martial arts world. Her father was once a renowned member of the martial arts world, but she is just another young girl deeply in love with Meng Sing Wan. They live happily in a small hut next to a river, where he spends time trying to catch fish, and writing poetry. From time to time, Sing has to go away on business to earn money, thats what he tells her anyway, but the horrifying truth is that he is an assassin. 

He is a member of the Happy Forest, a group of assassins led by Sister Ko, but he is tired of the never-ending life of killing for a living. Yip is another member of the group, and is Sings best friend. The both of them, along with Sister Ko, and a girl called Ho Ching grew up together, forming the best of Happy Forest. Yip is in love with Sister Ko, but is afraid to tell her. However, Ko only has eyes for Sing, but Sing regards her only as an older sister. 

Ko is given a mission by the Grand Eunuch Tsao, who instructs her to steal a letter from the hands of Master Suen from the Elites Villa sect, who was given to him by Grand Eunuch Li, Tsaos adversary in court. Ko tells Sing to fake his own death, then enter Suens service as a lone swordsman. Sing does this and more. He impresses Suen with his skills and soon has his trust. At this time however, he encounters Suens girl, who looks remarkably like the Ho Ching that once disappeared many years ago. Suen sees Sings interest in his woman, and so, gives her to him. 

But on the night of the wedding, Ho Ching tries to steal the message, but fail and dies. Sing is devastated by her death, and he goes back to Happy Forest, confronting Ko as to why he wasnt told that Ho Ching was alive all this time, and was being an undercover. Ko is upset by Sings outburst, only wanting what is best for all of them. Ko and Yip forces into Suens Elites Villa, and with Sing already inside, they are able to defeat Suen and take the message. When Ko and Sing deliver the message to Eunuch Tsao, it is revealed that Eunuch Li is really Eunuch Tsao, and that he hatched up the plan to destroy the people of the martial arts world. In the end, Tsao is defeated by the teamwork of Ko, Sing, and a young prince.

==Cast== Tony Leung as Meng Sing Wan
*Michelle Yeoh as Lady Ko
*Jimmy Lin as Prince Cha
*Joey Wong as Butterfly
*Donnie Yen as Yip Cheung
*Tok Chung-wa as Lui Heung Chuen
*Elvis Tsui as Lord Suen Yuk Pa
*Yip Chuen-chan as Miu Siu Siu / Ho Ching
*Lee Ka-ting as Lui Chung Yuen
*Chang Kuo-chu as Eunuch Tsao / Li Shu Tin
*Wong Chung-kui as eunuch
*Cho Boon-feng as bald fighter
*Choi Hin-cheung as Lord Suens man
*Lee Wai
*Wong Yiu
*Lam Gwong-chun

==External links==
* 
* 
* 

 
 
 
 