Legion of the Dead (film)
 
{{Infobox Film
| name           = Legion of the Dead
| image          = Legion of the Dead DVD cover.jpg
| image_size     =
| caption        = DVD cover
| director       = Paul Bales
| producer       = David Michael Latt David Rimawi Paul Bales
| writer         = Paul Bales
| narrator       = Chad Collins
| music          =
| cinematography =
| editing        =
| distributor    = The Asylum
| released       = August 9, 2005
| runtime        = 85 mins.
| country        = United States
| language       = English
| budget         = $500.000 
| gross          =
| preceded_by    =
| followed_by    =
}}

Legion of the Dead is a 2005 American horror film produced by The Asylum.
It Stars Courtney Clonch, and Bruce Boxleitner.

The film is a   (2001) and   (2008).
 Hammer horror film Blood from the Mummys Tomb, which also featured an evil Egyptian queen as the films antagonist.

==Plot==
In Ancient Egypt, there once reigned the malevolent Queen Aneh-Tet (Claudia Lynx), who ruled Egypt with an iron fist, terrorising the populace and slaughtering her enemies. Upon her death, her body was cursed, and buried in a secret location.

In the modern day, an archaeological team uncovers Aneh-Tets sarcophagus, and accidentally unleashes her evil onto the world. Among the team of experts opposing Aneh-Tet is Molly (Courtney Clonch), a young woman who finds herself pitted against Aneh-Tets evil in the final battle against the forces of darkness.

==Cast==
* Courtney Clonch – Molly
* Claudia Lynx – Aneh-Tet
* Bruce Boxleitner – Sheriff Jones
* Zach Galligan – Dr. Swatek Chad Collins – Carter
* Rhett Giles – Dr. Ari Ben-David
* Andrew Lauer – Sam Weaver
* Laura Vandervoort – Shayla
* Emily Falkenstein – Kevyn
* Amanda Ward – Kara
* Chase Hoyt – Justin
* Aaron David Thompson – Axel
* Amy Clover – Linda
* Patrick Thomassie – Santos
* Jared Michaels – Petrie
* Terry Shusta – Security Guard
* Heather Ashley Chase – Hotel Manager 

==See also==
*Allan Quatermain and the Temple of Skulls – Another film by The Asylum with a similar setting

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 