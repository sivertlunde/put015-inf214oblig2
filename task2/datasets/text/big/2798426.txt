Auto Focus
 
{{Infobox film
| name           = Auto Focus
| image          = AutoFocus.jpg
| caption        = Theatrical release poster 
| director       = Paul Schrader Brian Oliver Todd Rosken
| writer         = Michael Gerbosi
| based on       = The Murder of Bob Crane by Robert Graysmith
| starring       = Greg Kinnear Willem Dafoe
| music          = Angelo Badalamenti Fred Murphy
| editing        = Kristina Boden
| studio         = Propaganda Films Good Machine
| distributor    = Sony Pictures Classics
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English
| budget         = $7 million
| gross          = $2,704,951
}}
Auto Focus is a 2002 American biographical film directed by Paul Schrader, starring Greg Kinnear and Willem Dafoe. The screenplay by Michael Gerbosi is based on Robert Graysmiths book The Murder of Bob Crane (1993).  
 sitcom about a prisoner of war camp during World War II, and his dramatic descent into the underbelly of Hollywood after the series was cancelled.

==Plot== home video market.
 sex addict obsessed with women and with recording his encounters using video and photographic equipment, usually with Carpenter participating. Auto Focus depicts Cranes life from his sitcom success through his post-Hogans Heroes efforts to sustain a viable career &mdash; mostly in dinner theatre &mdash; until his murder in 1978.
 acquitted of the crime, he remains the subject of suspicion even after his death in 1998. 

==Cast==
*Greg Kinnear as Bob Crane
*Willem Dafoe as John Henry Carpenter
*Rita Wilson as Anne Crane Patricia Olson/Patricia Crane/Sigrid Valdis
*Ron Leibman as Lenny
*Michael E. Rodgers as Richard Dawson
*Kurt Fuller as Werner Klemperer
*Christopher Neiman as Robert Clary
*Ed Begley, Jr. as Mel Rosen
*Roderick L. McCarthy as Bartender
*John Kapelos as Bruno Gerussi
*Lyle Kanouse as John Banner

==Production==
The film premiered at the Toronto Film Festival and was shown at the San Sebastián Film Festival, the Helsinki International Film Festival, the Chicago International Film Festival, the New Orleans Film Festival, and the Bergen International Film Festival before going into limited release on eleven screens in the US, earning $123,761 on its opening weekend. It grossed $2,063,196 in the US and $641,755 in foreign markets for a total worldwide box office of $2,704,951. 

The DVD release includes a 50-minute Documentary film|documentary, Murder in Scottsdale, which delves into the initial murder investigation and the reopening of the case some 15 years later. 

==Critical reception==
The film met with a largely positive reception from critics.  A.O. Scott of the New York Times said the film "gets to you like a low-grade fever, a malaise with no known antidote. When it was over, I wasnt sure if I needed a drink, a shower or a lifelong vow of chastity ... there is   severe, powerful moralism lurking beneath the films dispassionate matter-of-factness. Mr. Schrader is indifferent to the sinner, but he cannot contain his loathing of the sin, which is not so much sex as the fascination with images ... To argue that images can corrupt the flesh and hollow out the soul is, for a filmmaker, an obviously contradictory exercise, but not necessarily a hypocritical one. There is plenty of nudity in Auto Focus, but you can always glimpse the abyss behind the undulating bodies, and the director leads you from easy titillation to suffocating dread, pausing only briefly and cautiously to consider the possibility of pleasure." 

Roger Ebert of the Chicago Sun-Times gave the film four stars calling it "a hypnotic portrait ... pitch-perfect in its decor, music, clothes, cars, language and values ... Greg Kinnear gives a creepy, brilliant performance as a man lacking in all insight ... Crane was not a complex man, but that should not blind us to the subtlety and complexity of Kinnears performance." 

Edward Guthmann of the San Francisco Chronicle called it "a compelling, sympathetic portrait ... Kinnear undercuts the seaminess of the Crane story, and shows us a man with more dimension and complexity than his behavior might suggest." 

Peter Travers of Rolling Stone awarded it 3½ out of 4 stars and added, "Schrader, the writer of Taxi Driver and the director of American Gigolo, is a poet of male sexual pathology. Shot through with profane laughs and stinging drama, Auto Focus ranks with his best films." 

Todd McCarthy of Variety (magazine)|Variety called it "one of director Paul Schraders best films, and like Boogie Nights ranks as a shrewd exposé of recent Hollywoods slimy underside ... Schrader directs with a very smooth hand, providing a good-natured and frequently amusing spin to eventually grim material that aptly reflects the protagonists almost unfailing good humor ... Pic overall has an excellent L.A. period feel without getting elaborate about it, and musical contributions by Angelo Badalamenti and a host of pop tunes are tops." 

The film has an 72% approval rating on review-aggregation website Rotten Tomatoes, based on 158 reviews. 

==Criticism by Scotty Crane==
Bob Cranes son, Scotty, bitterly attacked the film as being inaccurate. In an October 2002 piece he wrote on the movie, Scotty said that his father was not a regular church-goer and had only been to church three times in the last dozen years of his life, which included his own funeral. There is no evidence that Crane engaged in Sadomasochism|S&M, and director Paul Schrader told Scotty that the S&M scene was based on Schraders own personal experience. Scotty claims that his father and John Carpenter did not become close friends who socialized together until 1975, and that Crane was a sex addict long before he became a star, recording his sexual encounters at least as early as 1956. 

Scotty and his mother had shopped a rival script for a Bob Crane movie biography. The spec script, alternately titled "F-Stop" and "Take Off Your Clothes and Smile", was written up in Variety (magazine)|Variety by venerable columnist Army Archerd, but after Auto-Focus was announced, interest in Scottys script ceased. 

==Awards and nominations== Chicago Film Mystic River. 

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 