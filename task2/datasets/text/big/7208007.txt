Crimen Ferpecto
{{Infobox film
| name           = Crimen Ferpecto
| image          = Crimenferpecto.jpg
| caption        =
| director       = Álex de la Iglesia
| producer       = Roberto di Girolamo Gustavo Ferrada Álex de la Iglesia
| writer         = Álex de la Iglesia Jorge Guerricaechevarría Javier Gutiérrez Kira Miróbr> Rosario Pardo Gracia Olayo 
| music          = Roque Baños
| cinematography = José L. Moreno
| editing        = Alejandro Lázaro
| distributor    =
| released       =  
| runtime        = 106 minutes
| country        = Spain
| language       = Spanish
| budget         =
| gross          =
}}
Crimen Ferpecto, released in the United States as The Perfect Crime, is a 2004 Spanish black comedy film co-written, produced and directed by Álex de la Iglesia.

==Plot==
Rafael is a womens clothing clerk at a large Madrid department store, Yeyos.  His department is filled with beautiful, comely young women whom Rafael routinely seduces.  When Rafael vies for a management position with Don Antonio, a mens clothing clerk whom Rafael despises, a fluke causes Don Antonio to win the promotion.  He fires Rafael and a fight ensues in which Rafael accidentally kills Don Antonio.  Lourdes, an ugly and unassuming clerk at the store, witnesses the outcome of the fight, helps Rafael incinerate the body, and provides an alibi for the police.  Rafael wins his coveted promotion, but at a terrible cost: Lourdes blackmails Rafael into an unwanted relationship. He is forced to fire his many former lovers, to marry Lourdes (she proposes on a live reality TV show) and to support clown-like womens clothing of her design. Rafael becomes so depressed he begins to hallucinate, seeing the ghost of Don Antonio who suggests Rafael should kill Lourdes. As the police are also pressing him again, he causes a fire in the department store and fakes his death, in front of his wife and a police officer. Five years later, he (with a false identity) has a small business selling ties and socks, but Lourdes clown-like clothes are a success and she becomes a millionaire.

==Reception==
The film received very positive reviews from critics, 85% of critics gave the film a positive review based on 53 reviews and an average score of 7.1/10 according to Rotten Tomatoes.

==Trivia==
* When the film was released in the United States in 2005, the film was retitled El Crimen Perfecto, which translates to "The Perfect Crime".  The original title was a deliberate error, as the film explains.

* The film was released on DVD in the United States in March 2007 under the name "The Perfect Crime", and in Croatia it was titled Ševac i njegov pjevac which literally means "A fucker and his rooster".

== External links ==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 
 