A Pork Chop for Larry
A Pork Chop for Larry is an American short film directed by David Diliberto. It premiered at the Santa Barbara Film Festival in 2000.  The film was shot entirely in Wilmington, North Carolina.  

== Cast ==
* Amy Ryan
* Jonathan Wilson
* Tammy Arnold
* Warren Hendon
* Alex Halpern

== Production ==
The film was written and directed by David Diliberto. It was produced by David Diliberto, Diana Weymar and James Shaughnessy. Mark Gilmer was the cinematographer and Louise Byrne was the music composer.

== Summary ==

The movie revolves around a carnival worker who wants to leave her husband.  Every time she tries to leave him, her car gets stolen.  Her only recourse is to kill him.  She decides to poison him.

 
 
 


 