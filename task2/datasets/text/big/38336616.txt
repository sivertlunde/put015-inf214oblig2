Diana and Destiny
{{Infobox film
| name           = Diana and Destiny
| image          =
| caption        =
| director       = Floyd Martin Thornton 
| producer       = Floyd Martin Thornton
| writer         = Charles Garvice (novel) 
| starring       = Evelyn Boucher   Wyndham Guise   Roy Travers   Frank Petley
| music          = 
| cinematography = 
| editing        = 
| studio         = Windsor Films
| distributor    = Gaumont British Distributors
| released       = October 1916
| runtime        = 5 reels 
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         =
| preceded_by    =
| followed_by    =
}} silent drama film directed by Floyd Martin Thornton and starring Evelyn Boucher, Wyndham Guise and Roy Travers. It was made at Catford Studios, and based on a novel by Charles Garvice.

==Cast==
* Evelyn Boucher as Diana
* Wyndham Guise as William Bourne
* Roy Travers
* Frank Petley
* Harry Royston
* Harry Agar Lyons Ernie Collins
* Greta Wood

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1914-1918. Routledge, 2005.

 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 