Faasle
{{Infobox film
| name           = Faasle
| image          =Faasle.jpg
| image_size     = 
| caption        = 
| director       = Yash Chopra
| producer       = Yash Chopra
| writer         = 
| narrator       =  Farha Rohan Kapoor Shahryar (lyrics)
| cinematography = 
| editing        =  Shahryar
| distributor    = 
| released       = September 27, 1985
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| preceded_by    = 
| followed_by    = 
}} 1985 Bollywood film produced and directed by Yash Chopra.
 Raj Kiran, Shahryar .This film was panned by the critics for bad storyline and editing.   

==Plot==
Vikram (Sunil Dutt) is a proud and wealthy man and adores his motherless daughter Chandni (Farah). Widowed early, he brought up his daughter Chandni (Farah) and son Sanjay (Farooq Shaikh) sacrificing his own personal happiness, choosing a secret relationship over marriage, with Maya (Rekha).

When Vijay (Rohan Kapoor) comes into Chandnis life and steals her heart - they realize the course of true love never runs smooth and her fathers disapproval and her impending arranged marriage threatens the love between Chandni and Vijay.

Faasle is a story about the blind sense of duty inculcated by the older generation and the self-confidence and arrogance of the youth, it is a story of everyone doing what they believe to be right, all in the name of Love.

==Cast==
*Sunil Dutt  as  Vikram
*Rekha  as  Maya
*Farooq Shaikh  as  Sanjay  
*Deepti Naval  as  Sheetal   Raj Kiran  as  Shivraj Gupta
*Rohan Kapoor  as  Vijay
*Farha Naaz  as  Chandni  
*Daljeet Kaur  as  Shivrajs sister-in-law
*Sushma Seth  as  Shivrajs eldest Sister-in-law Javed Khan as  Nandu
*Alok Nath  as  Shivrajs Brother on wheelchair

==Reception==
This film was a massive flop at the box office.The film was panned by critics for weak storyline and slow pace of the film.

==Music==
{{Track listing
| headline        = Songs
| extra_column    = Playback
| all_lyrics      =  Shahryar (lyrics) 
| lyrics_credits  =

| title1          = Chandni Tu Hai Kahan
| lyrics1         = 
| extra1          = Lata Mangeshkar, Kishore Kumar

| title2          = Faasle Hain Bohat
| lyrics2         = 
| extra2          = Asha Bhosle

| title3          = Hum Chup Hain
| lyrics3         = 
| extra3          = Kishore Kumar, Lata Mangeshkar

| title4          = Hum Chup Hain
| note4           = Sad
| lyrics4         = 
| extra4          = Lata Mangeshkar

| title5          = In Aankhon Ke Zeenon Se
| lyrics5         = 
| extra5          = Lata Mangeshkar, Kishore Kumar

| title6          = Janam Janam
| lyrics6         = 
| extra6          = Lata Mangeshkar, Kishore Kumar

| title7          = More Banna Dulhan Leke Aaya
| lyrics7         = 
| extra7          = Pamela Chopra, Shobha Gurtu

| title8          = Sun Le Ye Saara Zamaana
| lyrics8         = 
| extra8          = Lata Mangeshkar

| title9          = Ye Qaafile Yaadon Ke
| lyrics9         = 
| extra9          = Asha Bhosle
}}

==References==
 

==External links==
*  

 

 
 
 
 
 


 