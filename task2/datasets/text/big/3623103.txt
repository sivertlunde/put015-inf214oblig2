Halloween (2007 film)
{{Infobox film
| name             = Halloween 
| image            = Halloween2007.jpg
| image_size       = 215px
| alt              =
| caption          = Theatrical release poster
| director         = Rob Zombie
| producer         = Malek Akkad Rob Zombie Andy Gould 
| writer           = Rob Zombie
| based on         =   William Forsythe 
| music            = Tyler Bates
| cinematography   = Phil Parmet
| editing          = Glenn Garland
| studio           = Dimension Films Nightfall Productions Spectacle Entertainment Group Trancas International Films Screen Gems
| distributor      = The Weinstein Company Metro-Goldwyn-Mayer   Paramount Pictures  
| released         =  
| runtime          = 109 minutes
| country          = United States
| language         = English
| budget           = $15 million    
| gross            = $80.2 million
}} reimagining of 1978 horror reboot of Michael Myers,  Malcolm McDowell as Samuel Loomis|Dr. Sam Loomis, and Scout Taylor-Compton as Laurie Strode; Daeg Faerch portrays a ten-year-old Michael Myers. Rob Zombies "reimagining" follows the premise of John Carpenters original, with Michael Myers stalking Laurie Strode and her friends on Halloween night. Zombies film goes deeper into the characters psyche, trying to answer the question of what drove him to kill people, whereas in Carpenters original film Michael did not have an explicit reason for killing.
 Halloween II, in 2009.

==Plot==
  Michael Myers Judith (Hanna Ronnie (William William Forsythe), Steve (Adam Weisman). Only his baby sister, Angel Myers, is spared. After one of the longest trials in the state’s history, Michael is found guilty of first degree murder and sent to Smiths Grove — Warren County Sanitarium under the care of child psychologist Dr. Samuel Loomis (Malcolm McDowell).
 Deborah (Sheri Moon Zombie), visits him regularly. While incarcerated at Smiths Grove, Michael becomes fixated on his papier-mâché masks, closing himself off from everyone, even his mother. When Michael kills a nurse as Deborah is leaving from one of her visits, she can no longer handle the situation and commits suicide. For the next fifteen years, Michael (Tyler Mane) continues making his masks and not speaking to anyone. Dr. Loomis, having continued to treat Michael over the years, attempts to move on with his life and closes Michaels case. Later, while being prepared for transfer to maximum security, Michael escapes Smiths Grove, killing the sanitarium employees and a truck driver for his overalls, and makes his way back to Haddonfield. On Halloween, Michael arrives at his old home, now abandoned, and recovers the kitchen knife and Halloween mask he stored under the floorboards the night he killed his sister.
 Annie Brackett Lynda Van Tommy (Skyler Bob (Nick Mason (Pat Cynthia (Dee Sheriff Brackett (Brad Dourif) that Michael has returned to Haddonfield. Brackett and Dr. Loomis head to the Strode home, with Brackett explaining along the way that Laurie is actually Michaels baby sister.
 Lindsey Wallace (Jenny Gregg Stewart), a girl Annie is supposed to be watching, so she can have sex with her boyfriend Paul (Max Van Ville). Annie and Paul return to the Wallace home and during sex, Michael kills Paul and attacks Annie. Bringing Lindsey home, Laurie finds Annie on the floor, bloodied but alive, and calls the police. She is attacked by Michael, who chases her back to the Doyle home. Sheriff Brackett and Loomis hear the call announced over the radio and head toward the Wallace residence. Meanwhile, Michael kidnaps Laurie and takes her back to his home. Michael approaches Laurie and tries to show her that she is his younger sister, presenting a photo of the siblings with their mother. Unable to understand, Laurie grabs Michaels knife and stabs him before escaping the house; Michael chases her, but is repeatedly shot by Dr. Loomis. Laurie and Loomis are just about to leave when Michael grabs Laurie and heads back to the house. Loomis intervenes and tries to reason with Michael, but Michael attacks him by squeezing Loomiss skull with his hands. Laurie takes Loomiss gun and runs upstairs; she is chased by Michael, who, after cornering her on a balcony, charges her head-on, knocking both of them over the railing. Laurie finds herself on top of a bleeding Michael. Aiming Loomis gun at his face, she repeatedly pulls the trigger until the gun finally goes off just as Michaels hand grips Lauries wrist.

==Production== Dimension announced that Rob Zombie, director of House of 1000 Corpses and The Devils Rejects, would be creating the next installment in the Halloween film series.    The plan was for Zombie to hold many positions in the production; he would write, direct, produce, and serve as music supervisor.  Bob Weinstein approached him about making the film. Zombie, who was a fan of the original Halloween (1978 film)|Halloween and a friend of John Carpenter, jumped at the chance to make a Halloween film for Dimension Studios.  Before Dimension went public with the news, Zombie felt obligated to inform John Carpenter, out of respect, of the plans to remake his film.    Carpenters request was for Zombie to "make it his own".  During a June 16, 2006 interview, Zombie announced that his film would combine the elements of prequel and remake with the original concept. He insisted that there would be considerable original content in the new film, as opposed to mere rehashed material.    The BBC reported that the new film would disregard the numerous sequels that followed Halloween.   
 .]]
Zombies intention was to reinvent Michael Myers because, in his opinion, the character, along with Freddy Krueger, Jason Voorhees and Pinhead (Hellraiser)|Pinhead, has become more familiar to audiences, and as a result, less scary.     The idea behind the new film was to delve deeper into Michael Myers back story. A deeper back story would add "new life" to the character, as Zombie put it.  Michaels mask will be given its own story, to provide an explanation as to why he wears it, instead of having the character simply steal a random mask from a hardware store, as in the original film.    Zombie explained that he wanted Michael to be true to what a psychopath really is, and wanted the mask to be a way for Michael to hide. He wants the young Michael to have charisma, which would be projected onto the adult Michael. Zombie has decided that Michaels motives for returning to Haddonfield should be more ambiguous. As Zombie explains, "was he trying to kill Laurie, or just find her because he loves her?" 

Moreover, Michael would not be able to drive in the new film, unlike his 1978 counterpart who stole Loomis car so that he could drive back to Haddonfield.  Zombie also wants the Dr. Loomis character to be more intertwined with that of Michael Myers; Zombie said that the characters role in the original was "showing up merely to say something dramatic".  Although Zombie has added more history to the Michael Myers character, hence creating more original content for the film, he chose to keep the characters trademark mask and Carpenters theme song intact for his version (despite an apparent misinterpretation in an interview suggesting the theme would be ditched).  Production officially began on January 29, 2007.    Shortly before production began, Zombie reported that he had seen the first production of Michaels signature mask. Zombie commented, "It looks perfect, exactly like the original. Not since 1978 has The Shape looked so good".  Filming occurred in the same neighborhood that Carpenter used for the original Halloween. 

On December 19, 2006, Zombie announced to Bloody-Disgusting that Daeg Faerch would play the part of ten-year-old Michael Myers.  On December 22, 2006, Malcolm McDowell was officially announced to be playing Dr. Loomis.  McDowell stated that he wanted a tremendous ego in Loomis, who is out to get a new book from the ordeal.  On December 24, 2006, Zombie announced that Tyler Mane, who had previously worked with Zombie on The Devils Rejects, would portray the adult Michael Myers.  Mane stated that it was very difficult to act only with his eyes.    Scout Taylor-Compton endured a long audition process, but as director Zombie explains, "Scout was my first choice. There was just something about her; she had a genuine quality. She didnt seem actor-y."    She was one of the final people to be cast for a lead role after Faerch, Mane, McDowell, Forsythe, and Harris.  A contest was held for a walk on role in the film, at the time called Halloween 9; it was won by Heather Bowen.  She played a news reporter who covered Michaels arrest but her scene was cut from the film and does not appear in the deleted scenes. 

==Release==
Approximately four days before the theatrical release of the film, a  , to the leaking of a workprint version.  Dark Horizons webmaster Garth Franklin noted that watching the workprint allows a viewer to see what was changed after test screenings of the film in June 2007. For example, one particular scene—the rape of one of the Smiths Grove female inmates—was replaced in the final version.  Halloween was officially released on August 31, 2007 to 3,472 theaters in North America,    giving it the widest release of any of the previous Halloween films.   

==Reception==

===Box office===
On its opening day, Halloween grossed $10,896,610,    and immediately surpassed the opening weekend grosses for   (1982) at $6,333,259,   (1988) at $6,831,250,   (1989) at $5,093,428, and   (1995) at $7,308,529.  From September 1–2, Halloween earned $8,554,661 and $6,911,096, respectively, for a 3-day opening weekend total of $26,362,367. The film would earn an additional $4,229,392 on Labor Day for a 4-day holiday weekend gross of $30,591,759,  making it the highest ever for that holiday.    As a result, the 2007 film would immediately surpass the total box office gross for Halloween II (1981) at $25,533,818, Halloween III (1982) at $14,400,000, Halloween 4 (1988) at $17,768,757, Halloween 5 (1989) at $11,642,254, The Curse of Michael Myers (1995) at $15,116,634, and   (2002) with $30,354,442. 

Following its first Friday after its opening weekend, Halloween saw a 71.6% drop in attendance, earning $3,093,679.  The film, which earned the #1 spot at the box office in its opening weekend,  earned only $9,513,770 in its second weekend—a 63.9% decrease—but still claimed the #2 spot at the box office just behind  .  The film continued to appear in the weekend top ten going into its third weekend, when it earned $4,867,522 to take sixth place.  It was not until the films fourth weekend that it fell out of the top ten and into twelfth place with $2,189,266.  Halloween would fail to regain a top ten spot at the box office for the remainder of its theatrical run. 
 When a Black Christmas Prom Night Friday the 13th (2009) leading the group with $60 million.  Halloween is also ranked eleventh overall when comparing it to all of the horror remakes,  as well as eighth place for all slasher films in general, in unadjusted dollars. 
 Michael Clayton and Mr. Woodcock in foreign markets on the weekend of September 29, 2007. Halloween led the trio with a total of $1.3 million in 372 theaters – Michael Clayton and Mr. Woodcock took in $1.2 million from 295 screens and $1 million from 238 screens, respectively.  By November 1, 2007, Halloween had taken in an additional $7 million in foreign markets.  Ultimately, the film would earn approximately $21,981,879 overseas.  By the end of the films theatrical run, the film had taken a worldwide total of $80,253,908.  Comparing this film to the rest of the films in the Halloween film series, Zombies remake is the highest grossing film in unadjusted US dollars.  When adjusting for the 2009 inflation,  Zombies Halloween—which adjusts to $60.4 million domestically—is fourth, behind Carpenters Halloween at $166.9 million, Halloween H20 at $73.8 million, and Halloween II at $66.7 million. 

===Critical response=== weighted average score out of 100 to reviews from mainstream critics, calculated a 47 out of 100 from the 18 reviews it collected.  CinemaScore polls reported that the average grade cinemagoers gave the film was "B-minus" on an A+ to F scale; it also reported that 62% of the audience was male, with 57% being 25 years or older. 

  was perfectly cast as Loomis.]]
Peter Hartlaub, of the San Francisco Chronicle, felt Zombie was successful in both "  his own spin on Halloween, while at the same time paying tribute to Carpenters film"; he thought Zombie managed to make Michael Myers almost "sympathetic" as a child, but that the last third of the film felt more like a montage of scenes with Halloween slipping into "slasher-film logic".  Nathan Lee of The Village Voice disagreed in part with Harlaub, feeling that Halloween may have placed too much emphasis on providing sympathy for Michael Myers, but that it succeeded in "  Carpenters vision without rooting out its fear".  The film critic Matthew Turner believed the first half of the film, which featured the prequel elements of Michael as a child, were better played than the remake elements of the second half. In short, Turner stated that performances from the cast were "superb", with Malcolm McDowell being perfectly cast as Dr. Loomis, but that the film lacked the scare value of Carpenter’s original.  Jamie Russell from the BBC agreed that the first half of the film worked better than the last half; she stated that Zombie’s expanded backstory on Michael was "surprisingly effective"—also agreeing that McDowell was perfectly cast as Loomis—but that Zombie failed to deliver the "supernatural dread" that Carpenter created for Michael in his 1978 original. 
 New York Time Out London, felt Zombie added "surprising realism" to the development of Michael Myers’ psychopathic actions, but agreed with Newman that the director replaced the original film’s "suspense and playfulness" with a convincing display of "black-blooded brutality". 
 torture porn" horror films.  Bill Gibron, of PopMatters, believes that audiences and critics cannot compare Carpenters film to Zombies remake; where Carpenter focused more on the citizens of Haddonfield—with Michael acting as a true "boogeyman"—Zombie focuses more on Michael himself, successfully forcing the audience to experience all of the elements that Michael went through that would result in his "desire for death". 

Halloween won the Rondo Hatton Classic Horror Award for Best Film of 2007, drawing in 550 votes, the most ever in the history of the award.  The film also won the Best Remake Award at the 2008 Spike TV Scream Awards.  Dan Mathews, vice president of PETA, sent Rob Zombie a thank-you letter for what he perceived as Zombie sending a message to audiences when he depicted the young Michael Myers torturing animals, something he felt demonstrated that people who commit acts of cruelty to animals are likely to move on to humans. Mathews went on to say, "Hopefully, with the attention focused by your movie on the link between cruelty to animals and human violence, more people will recognize the warning signs among people they know and deal with them more forcefully. We wish you continued success!" 
 

==Home media== God of Tom Sawyer", Alice Coopers "Only Women Bleed", Peter Framptons "Baby, I Love Your Way", Nazareth (band)|Nazareths "Love Hurts", Bachman–Turner Overdrives "Let It Ride", Misfits (band)|Misfits "Halloween II", and an Iggy Pop live version of the The Stooges "1969" among others. 

On December 18, 2007, the film was released on DVD in the United States; both the theatrical (109 minutes) and an unrated directors cut (121 minutes) were released as two-disc special editions containing identical bonus features.  The film was released on DVD in the UK on April 28, 2008, known as the "Uncut" edition.  On October 7, 2008, a three-disc set was released.    This Collectors Edition of Halloween features the same bonus features as the previous unrated edition, but includes Rob Zombies four-and-a-half hour "making-of" documentary similar to the "30 Days in Hell" documentary for Zombies The Devils Rejects. 

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 