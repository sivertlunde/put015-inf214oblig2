Sorrow : Every Good Feeds On Evil
 
  

 
 
{{Infobox film
| name           = Sorrow The Movie
| image          = Sorrow the movie direct Millie Loredo.jpg
| caption        = Horror release poster
| director       = Millie Loredo
| producer       = {{Plain list |
Millie Loredo
}}
| writer         = Millie Loredo
| starring       = {{Plain list |
* Vannessa Vasquez
* Melissa Mars
* Eric Martinez
* Mary Etuk
* Andrew Sensenig
* Brenden Whitney
* Donny Boaz
 
}}
| music          = Giona Ostinelli
| cinematography = {{Plain list |
Dustin Hethcock
Millie Loredo
}}
| editing        = {{Plain list |
Kevin Ray
}}

| distributor    = BrinkVision
| country        = United States
| language       = English

}}

Sorrow The Movie is an independent film written and directed by Millie Loredo. The star cast of the film includes Vannessa Vasquez,  Melissa Mars, Andrew Sensenig, Eric Martinez, Mary Etuk and Donny Boaz. The movie is about a young woman named Mila who is kidnapped by a dangerous serial killer couple, Hersey Igor and Dale Rogers. From the physical pain to the mental torments she is subjected to during her captivity, Mila experiences the most harrowing situation of her life. Her bold resistance and quick wit push her to escape and take revenge on her kidnappers.

The movie is based on a nightmare of director Millie Loredo.  She gave words to her nightmare on pen and paper. She had initially written the screenplay for a short film, which was shot in the outskirts of Moab, Utah. After seeing the completed short (edited by Nick Gregorio), director Millie Loredo saw an opportunity to further explore the characters, so she extended the short film’s screenplay into a feature film. The rest of the feature film was shot in Texas and Utah.

==Plot==
Detective Farrell and his partner Detective Ana Salinas are called to a crime scene in the bucolic city of Fieldhouse, Texas. This day proves to be unexpectedly disturbing, every ominous turn bringing more mystery to the case. Two dead bodies are found at the crime scene, an abandoned, run-down house with walls splattered in blood and torture devices scattered throughout. An unidentified woman is also found at the crime scene with a gunshot wound to the shoulder, but she manages to flee before the police reach her for questioning.

The investigation reveals that the mysterious woman is Mila, a forensic psychologist kidnapped by the serial killers that were found dead at the crime scene. While she was being held captive there, Mila faced unspeakable terror at the hands of the serial killers. She witnessed the torture and killing of several other women, but her resilience and will to live push her to escape from her captors. Mila finds help and begins plotting her own revenge amidst a broken criminal system.

==Cast==
* Vannessa Vasquez as Mila Sweeney

* Melissa Mars as Detective Salinas

* Donny Boaz as Detectivee Farrell

* Andrew Sensenig as Chief of Police Gonzales

* Eric Martinez as Dale Rogers

* Mary Etuk as Hersey Igor

==Production==
Sorrow’s first premier was held on April 21, 2015 in Houston, Texas. It was then screened in select theaters in New York, Utah, North Carolina and Georgia. Streaming of the film is available on Amazon Prime, will be available on the Hulu Network in mid-May 2015. The DVD and Blu-ray is available in select stores.

===Filming===
Sorrow was initially conceived as a short film shot in Utah in 2011. Several scenes were shot at Mount Peale with an elevation of 12,271 feet. Most parts of the feature film scenes were shot in Houston and others in Utah. The house in the film where the serial killers tortured and killed women is a famous historic house in the Heights. The police station scenes were shot at the Richmond Police Department in Richmond, Texas.

===Casting===
Casting for the short film version of Sorrow took place in Utah in 2011, where Loredo cast Vannessa Vasquez as Mila Sweeney, Eric Martinez as Dale Rogers, Mary Etuk as Hersey Igor and Brenden Whitney as Gambit. The four of them continued their work in the feature film. The casting for the rest of the characters in the feature was carried out in August 2012 in Texas. The rest of the cast, including Andrew Sensenig, Donny Boaz, John Mastrangelo, Jill Adler, Alethea Bailey and Heather Williams, were added to the project in Houston.

===Style===
According to Loredo, the movie is inspired by classic thrillers like Reservoir Dogs, I Spit on Your Grave, The Devil’s Rejects, Natural Born Killers and The Last House on the Left. Loredo envisioned the film to be full of creepy scenes and unexpected twists, but still retaining the realistic elements of life that make the story believable. 

==Distribution==
Brink Vision took on the film in December 2014. The company distributes films in Canada and U.S. Breaking Glass Pictures is the foreign distributors for the film.

==Release==
Sorrow will premiere at River Oaks Theatre in Houston, Texas April 21, 2015. It will then be screened in select theaters in Utah, Georgia, North Carolina and New York. Amazon Prime has chosen to stream the film online beginning April 2015, and Hulu Network will stream the film beginning mid-May. Sorrow will be sold on DVD  in select retail stores, and it will also be available on Blu-ray in July.

==References==
{{reflist|refs=
   
   
}}

==External links==
* 
* 

 

 
 
 
 
 
 