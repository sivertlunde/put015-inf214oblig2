Bone Eater
{{Infobox television film
| image          =  
| alt            = 
| caption        = DVD cover
| genre          = Science fiction horror
| director       = Jim Wynorski (as Bob Robertson) 
| producer       = Paul Hertzberg
| writer         = Jim Wynorski
| starring       = Bruce Boxleitner Michael Horse Clara Bryant
| music          = Chuck Cirino
| cinematography = Andrea V. Rossotto
| editing        = Marcus Manton
| studio         = CineTel Films
| distributor    = Syfy
| network        = Syfy
| released       =   }}
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $700,000  
| gross          = 
}} Native American monster from destroying his town.  It premiered on Syfy and was later released on DVD.

== Plot ==
An unscrupulous land developer, Big Jim Burns, ignores the protests of Native Americans as he violates their ancestral burial grounds.  The local sheriff, Steve Evans, is caught in the middle of the conflict, as he is half Native American and half Caucasian.  When one of Burns construction crews unearths an ancient relic, they unleash a giant skeletal monster that proceeds to kill everyone in its path.  Evans must deal with his rebellious daughter, unhelpful bureaucrats, and Johnny Black Hawk, a Native American who agitates for violence.  After consulting with the local chief, Storm Cloud, Evans learns he must locate the relic and use it in ritual combat against the monster.  Once the sheriff acquires the relic, Johnny Black Hawk attempts to take it from Evans and use it to get vengeance on the town; Evans is forced to kill him in self-defense.  After donning war paint, Evans goes on to fight and ultimately defeat the Bone Eater.

== Cast ==
* Bruce Boxleitner as Sheriff Steve Evans
* Michael Horse as Chief Storm Cloud
* Adoni Maropis as Johnny Black Hawk
* Clara Bryant as Kelly Evans
* Gil Gerard as Big Jim Burns
* Jennifer Lee Wiggins as Kaya
* Walter Koenig as Coogan
* William Katt as Doctor Boombas
* Veronica Hamel as Commissioner Hayes

== Release ==
CineTel Films announced Bone Eater in 2006.   It premiered on Syfy on April 4, 2008, and Lionsgate released it on DVD on July 8, 2008. 

== Reception ==
Scott Foy of Dread Central rated it 2/5 stars and wrote that the film would not "scare anyone except maybe the smallest of children".  Foy criticized the story as "pointless and derivative" and the effects as cartoonish, though he stated that the monster gives Bone Eater "moments of wacky charm".     David Johnson of DVD Verdict called it "astonishingly stupid" and described the title monster as "one of the most ridiculous CGI contraptions Ive seen."   Justin Felix of DVD Talk rated the film 1/5 stars and stated "it has a its so bad you have to see it to believe it vibe."   Bloody Disgusting rated the film 2.5/5 stars and called it "the ultimate Saturday morning mindwipe." 

== See also ==
* List of Sci Fi Pictures original films

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 