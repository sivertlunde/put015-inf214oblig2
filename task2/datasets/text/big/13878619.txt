Kancil's Tale of Freedom
{{Infobox film
| name           = Dongeng Kancil untuk Kemerdekaan
| image          = Jalan malioboro - Jogjakarta.JPG
| image_size     =
| caption        = The film follows four children on Malioboro Street
| director       = Garin Nugroho
| producer       = 
| writer         = 
| narrator       = 
| starring       = Children: Kancil, Topo, Sugeng and Hatta 
| music          = 
| cinematography =         
| editing        = 
| distributor    = 
| released       = 1995
| runtime        = 55 minutes
| country        = Indonesia
| language       = Indonesian
| budget         = 
}} 1995 Indonesian documentary film directed by Garin Nugroho.

==Plot==
The film follows the daily lives of four street kids in Yogyakarta Kancil, Topo, Sugeng and Hatta around the legendary Malioboro street, which was once the battleground against the Japanese and the Dutch. The film commemorates the 50th anniversary of Sukarno driving through the street in the aftermath and waving the Flag of Indonesia as he became the first President proclaiming "My nationalism is humanity". 

The film investigates modern life on the street and whether early promises made by Sukarno lived up to expectations.

==References==
 

==External links==
*  

 

 
 
 
 
 
 
 
 


 
 