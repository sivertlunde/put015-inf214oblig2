Conversations with Other Women
{{Infobox film
| name           = Conversations with Other Women
| image          = Conversations with Other Women.jpg
| caption        = Theatrical release poster
| writer         = Gabrielle Zevin
| director       = Hans Canosa
| producer       = Ram Bergman Bill McCutchen Kerry Barden
| screenplay     = Gabrielle Zevin
| music          = Starr Parodi Jeff Eden Fair
| starring       = Helena Bonham Carter Aaron Eckhart Nora Zehetner Olivia Wilde
| cinematography = Steve Yedlin
| editing        = Hans Canosa
| released       =  
| runtime        = 84 minutes
| country        = United States
| budget         = 
| gross          = $973,525   
}}
 romantic drama film directed by Hans Canosa, written by Gabrielle Zevin, starring Aaron Eckhart and Helena Bonham Carter. The film won Best Actress for Bonham Carter at the 2005 Tokyo International Film Festival.

==Plot== Bonham Carter) of about the same age, and offers her a glass of champagne. As conversation ensues, they begin to flirt. Witty small talk about such topics as the wedding party and their own past relationships gradually reveals to the viewing film audience that they are not strangers, but in fact that they share an intimate past. A series of flashback scenes shows much younger versions of the two of them as lovers.

Despite having significant others (22-year-old Sarah the dancer and Geoffrey the cardiologist, both absent) the couple choose to go upstairs to her hotel room together. However, their decision to sleep together is one which is clearly complex and fraught with emotional baggage for each of them. Again with flashbacks, a series of vignettes juxtaposes their earlier selves against the older, perhaps wiser couple in the hotel room. 

The two reminisce and reassess their feelings for each other. He appears to have ambiguous feelings about the direction of his life, while she seems more adjusted to her life choices. The emotional fulfillment the two experienced in their youth has the appearance of causing them to reflect on their current lives in comparison to the choices and options they had while much younger.

She must catch a transatlantic flight home to London in the morning, so the two leave the hotel together in the early morning. As they return to their separate lives, each speculates with their cab driver on the future and the difficulty of being happy.

==Release information==

===Theatrical release===
Conversations, Canosas directorial debut, premiered at the 2005 Telluride Film Festival. The film subsequently played at the Tokyo International Film Festival, Seminci Valladolid International Film Festival, the US Comedy Arts Festival, South by Southwest Film Festival, Seattle International Film Festival, Los Angeles Film Festival, Rio de Janeiro International Film Festival, Hamburg Film Festival, São Paulo International Film Festival, and the Muestra Internacional de Cine.

The films international theatrical premiere was on June 7, 2006 in France. Released by distributor MK2 Diffusion under the title Conversations avec une Femme, the film played theatrically for five months to both box office success and critical acclaim.

Released on August 11, 2006 in the United States by Fabrication Films, the film played in fourteen cities, garnering modest theatrical box office and critical acclaim.

===DVD release===
The original split-screen Region 1 DVD version was released in the United States on January 9, 2007 by Arts Alliance America. A single frame, full screen DVD version, created for 4x3 broadcast television release, was subsequently released on October 9, 2007. The single frame cut only retains three split-screen sequences: the opening titles, the sex scene, and the closing taxicab sequence.

International DVD releases include MK2 in France, Shochiku in Japan, Revelation Films in the United Kingdom, TVA Films in Canada, Dendy Films in Australia, Filmes Unimundos in Portugal, D Productions in Turkey, Civite Films in Spain, Global in Russia, J-Bics in Thailand, Paradiso Home Entertainment in Belgium, Luxembourg and the Netherlands, Cathay-Keris Films in Singapore and Malaysia, Atlantic Film in Sweden, NoShame Films in Italy, Prooptiki Bulgaria in Bulgaria, Croatia, Romania, Serbia and Montenegro and Slovenia, Prooptiki in Greece, Shapira Films in Israel, Solopan in Poland, VideoFilmes in Brazil, and With Cinema in South Korea.

==Awards==
{| class="wikitable"
|-
! Result !! Award !! Recipient !! Festival/Ceremony !! Year
|-
|   || Special Jury Prize || Hans Canosa ||rowspan="3"| Tokyo International Film Festival ||rowspan="3"| 2005
|-
|   || Best Actress || Helena Bonham Carter
|-
|   || Tokyo Grand Prix ||
|-
|   || Best Actress || Helena Bonham Carter || Evening Standard British Film Awards || 2007
|-
|   || Producers Award (also for Brick (film)|Brick)  || Ram Bergman || Independent Spirit Awards || 2005
|-
|   ||nowrap="nowrap"| Best First Screenplay Award || Gabrielle Zevin || Independent Spirit Awards || 2006
|-
|   || Golden Spike || || Seminci Valladolid International Film Festival || 2005
|-
|}

==Production==
Eckhart and Bonham Carter shot 82 pages of dialogue in only 12 days of principal photography.

To facilitate the split screen presentation of the film, two cameras (one on each actor) were used throughout principal photography.

For the sex scene, the director asked the actors to stay in bed while the crew quickly changed camera positions to get all of the coverage. The entire scene, including 10 camera setups and a complex dolly shot, was completed in 45 minutes.

To facilitate a sense of realism, both actors provided elements of their own costumes. Eckhart wore his own Armani suit and Calvin Klein underwear as part of his costume, while Bonham Carter wore her own Prada shoes.

The hotel room, the interior of the elevator and the interior of the cab(s) in the final shot were shot on a sound stage in Culver City, California.
 The Fisher Wild at Heart and Bugsy.

Many scenes were shot in the Los Angeles Herald-Examiner building, which has been used almost exclusively as a film location since the notorious Los Angeles newspaper, once owned by William Randolph Hearst, closed down in 1989.

==Post-production==
Though an editor was initially hired to cut the movie, he quit after putting together an initial assembly, citing difficulties editing the dual-frame split screen presentation in which the movie is presented. The director, who had never cut a film before, elected to learn and use the editing software himself, and acted as editor.

The final shot in the movie was the only one captured with a single camera. Eckhart and Bonham Carter were filmed in the back of one taxi on set. In post production, the shot was digitally divided in two; digital movement was added for each car and two separate background plates were composited to create the illusion of different taxi interiors.
 producer Kwesi Shake to complete the necessary shots.

Three apparent B-roll shots of the supporting characters in a ballroom full of dancers were actually created using visual effects. When the line producer asked the director the minimum number of extras needed for these shots during principal photography, the director requested 50 extras. When only seven extras showed up on the ballroom shoot days, an alternate solution became necessary. The visual effects supervisor found takes which included empty sections of the ballroom. Taking several high resolution stills from those takes, he created three background plates. During a day of additional photography, both the supporting characters who would appear in the foreground and pairs of dancers who would appear in the middle ground were shot against a greenscreen. The visual effects supervisor then composited up to a dozen elements to create shots which appear to contain the bride, her bridesmaids and the young man and young woman characters in the midst of a ballroom full of dancing couples.
 rotoscoped the resolution images of feet were on foot fetish websites. Thus the replacement feet in those five shots are "pornographic feet".

A single-frame 4:3 version of the movie was produced for television.

==Soundtrack==
Although the film contains no traditional film score|score, music plays for almost 40 percent of the running time.

Three songs from the 2003 album Quelquun ma dit by Carla Bruni complement the tone of other sequences in the film. The song "Jen connais" accompanies the opening title cards and the juxtaposed narrative images, and then recurs in the final scene through the end credits. The song "Le plus beau du quartier" plays over the scene in which the woman asks the man to help her undress. The song "Lexcessive" serves as accompaniment to the transition from the hotel room to the roof.

The sex scene is played to the song "Ripchord" from the 2004 album More Adventurous by the Los Angeles-based rock band Rilo Kiley.

The scenes in the wedding reception are accompanied by "wedding band" music composed by Starr Parodi and Jeff Eden Fair.

==Split screen== split screen. silent epic, Napoléon (1927 film)|Napoléon. The term "split screen" was coined to describe the many uses of the technique in films of the 1960s. More recent uses of split screen include Mike Figgis 2000 film Timecode (film)|Timecode and the Fox TV series 24 (TV series)|24.
 Pillow Talk starring Doris Day and Rock Hudson. Another common use of the technique is to show two separate but converging spaces (such as contrasting shots of predator and prey) to create tension or suspense. The filmmaker most associated with the latter use is Brian De Palma.

Conversations innovation in split screen is the juxtaposition of shot and reverse shot of two actors in the same take, captured with two cameras, for the entire movie. The film represents a new kind of viewing experience that enlists the audience as a perceptual editor. The filmmakers allow the viewer to choose how they watch the film, following either character or both simultaneously. Seeing both characters act and react in real time lets the audience follow the emotional experience of the characters without interruption.

At a panel on acting at the Telluride Film Festival, the actors spoke of the challenge of working in a two-camera system. Unlike traditionally shot and cut films, the actors knew that all moments of a take could end up on screen and thus acted through every take.  The actors were constantly in the moment. The resulting film presents the actors work in the way musicians play in a duet , with action, dialogue and reaction running on both sides of the frame in real time. The movie presents two remarkable achievements in screen acting.

The shot/reverse shot function of split screen comprises most of the running time of the film, but the filmmakers also use split screen for other spatial, temporal and emotional effects. Conversations split screen sometimes shows flashbacks of the recent or distant past juxtaposed with the present; moments imagined or hoped by the characters juxtaposed with present reality; present experience fractured into more than one emotion for a given line or action, showing an actor performing the same moment in different ways; and present and near future actions juxtaposed to accelerate the narrative in temporal overlap.

Film critic David Thomson found fault with "Conversations with Other Women" use of split screen, contending that the device detracted from the movies focus; if Canosa and Zevin had "excise  the last remaining split-screen stuff, they’d have a film with a kind of modest, forlorn greatness."  

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 
 
 
 