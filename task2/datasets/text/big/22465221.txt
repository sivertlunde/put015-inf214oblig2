Belle (1973 film)
{{Infobox film
| name           = Belle
| image          =
| image size     =
| caption        =
| director       = André Delvaux
| producer       = Jean-Claude Batz Albina du Boisrouvray
| writer         = André Delvaux Monique Rysselinck
| narrator       =
| starring       = Jean-Luc Bideau
| music          = Frédéric Devreese
| cinematography = Ghislain Cloquet Charles Van Damme
| editing        = Emmanuelle Dupuis Pierre Joassin
| distributor    =
| released       = 1973
| runtime        = 96 minutes
| country        = Belgium France
| language       = French
| budget         =
}}

Belle is a 1973 Belgian-French drama film directed by André Delvaux. It was entered into the 1973 Cannes Film Festival.    

==Plot==
The aging romancier Mathieu Grégoire falls in love with a female stranger who doesnt even understand his language. Their uncommon relationship upsets his family.

==Cast==
* Jean-Luc Bideau - Mathieu Grégoire
* Danièle Delorme - Jeanne
* Adriana Bogdan - Belle
* Roger Coggio - Victor
* René Hainaux - the deputy
* Stéphane Excoffier - Marie
* John Dobrynine - John
* Valerio Popesco - the stranger
* François Beukelaers - the false stranger
* André Blavier - Vincent
* Marc Audier - the café owner
* Arlette Emmery - the presenter
* Suzanne Gohy - the mother
* Yvette Merlin - the boss at the Joités

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 