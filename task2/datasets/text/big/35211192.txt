Gojoe
{{Infobox film
| name           = Gojoe: Spirit War Chronicle
| image          = Gojoe.jpg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Sogo Ishii
| producer       = Takenori Sento
| writer         = Goro Nakajima Sogo Ishii
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Tadanobu Asano Masatoshi Nagase Daisuke Ryu Jun Kunimura
| music          = Hiroyuki Onogawa
| cinematography = Makoto Watanabe
| editing        = Shuichi Kasesu
| studio         = Toho
| distributor    = 
| released       =  
| runtime        = 138 minutes
| country        = Japan
| language       = 
| budget         = 
| gross          = 
}}

  is a 2000 Japanese jidaigeki action film directed by Sogo Ishii. In several English-speaking countries, it was released as Gojoe.  The film is a martial arts film set in twelfth century Japan about a battle between the Minamoto and Taira clans.

==Production== The Ring horror film series, wanted to create a box office hit by making an action film in the jidaigeki genre.   

==Release==
The film was first shown in North America as part of the 2000 Toronto Film Festival.  The film was also shown at the 2001 Sitges Film Festival.  The film was set for an October 7, 2001 theatrical release in Japan. 

==Awards==
Actor Tadanobu Asano won the award for Best Supporting Actor at the Hochi Film Awards in 2000 for his role in this film and in Taboo (1999 film)|Taboo. 

==Reception==
Variety (magazine)|Variety gave the film a mixed review, stating that "Despite an impressive opening and suitably titanic finale, yarn about various warriors battling one another during the "Dark Ages" is way overlong at almost 2-1/2 hours and soon palls with its endless succession of sylvan swordplay."    Shichinin no Samurai." Kurosawas masterpiece delivers the essence of battle with gut-wrenching authority.  Film4 gave the film a positive review, stating that "What it lacks in humour (and there is absolutely none to be found in it), Gojoe more than makes up for in its sheer, unrelenting intensity, something which few other directors would be able to sustain over so long a duration." 

==Notes==
 

==External links==
*  

 

 
 
 
 
 
 
 
 
 