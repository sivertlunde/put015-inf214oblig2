Pattabhishekam (1999 film)
{{Infobox film
| name           = Pattabhishekam (1999)
| image          =
| image_size     =
| caption        =
| director       = Anil & Babu
| producer       =Ramesh, Sherif for Rajsagar Films 
| writer         =
| screenplay     =Rajan Kiriath and Vinu Kiriath
| starring       = Jayaram, Jagathy, Harisri Ashokan, Mohini, Indrans, Zeenath, Srilatha etc
| music          = Berny-Ignatius
| cinematography =Vipin Mohan
| editing        =P.C.Mohan
| studio         =
| distributor    =
| released       =  
| country        = India Malayalam
}}
 1999 comedy-drama Indian Malayalam Malayalam film,  directed by  Anil-Babu. The film stars Jayaram, Mohini (Tamil actress)|Mohini, Jagathy Sreekumar and Harishree Ashokan in lead roles. The film had musical score by Berny-Ignatius. 

==Cast==
*Jayaram Mohini
*Jagathy Sreekumar
*Harishree Ashokan
*Jagannatha Varma
* Kozhikode Narayanan Nair
*Bobby Kottarakkara
*Augustine
* Kottayam Nazeer
* Zeenath

==Soundtrack==
The music was composed by Beny Ignatious. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Eazhamkooli || M. G. Sreekumar || Bichu Thirumala || 03.59
|-
| 2 || Girijapathisukha || K. J. Yesudas, Sujatha || Bichu Thirumala || 04.22
|-
| 3 || Poochapoochapenne || M. G. Sreekumar || Bichu Thirumala || 04.01
|-
| 4 || Poovukal Peyyum || K. J. Yesudas, Sujatha || Bichu Thirumala || 04.23
|-
| 5 || Shankhumvenchamaram || K. J. Yesudas || Bichu Thirumala || 04.13
|}

==References==
 

==External links==
*  
*  

 
 
 
 


 
 