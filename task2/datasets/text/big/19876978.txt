Meera (1945 film)
{{Infobox film name           = Meera image          = Meera 1945 Tamil.jpg caption        = Theatrical poster director       = Ellis R. Dungan writer         = starring       = M. S. Subbulakshmi Chittor V. Nagaiah Kumari Kamala  T. S. Baliah producer       = Kalki Sadasivam|T. Sadasivam
 | studio        = Chandraprabha Cinetone Films  distributor    = music          = S. V. Venkatraman cinematography = Jithen Banerjee P. S. Selvaraj editing        = R. Rajagopal country       = India released       =  3 November 1945 5 December 1947  (Hindi version)   runtime        = 113 minutes language  Tamil
}}
  1945 Tamil language|Tamil-language film starring M. S. Subbulakshmi, Kumari Kamala, T. S. Baliah and Chittoor V. Nagaiah based on the life of the devotional singer and dancer Meera. M. G. Ramachandran done a minor role. The film was directed by American film director Ellis R. Dungan.   

==Plot== AD during the reign of Emperor Akbar, young Meera (Radha Viswanathan), inﬂuenced by the Andal-Krishna story, is deeply in love with Krishna, so much so that she considers Krishna to be her husband after she garlands him on an auspicious day as advised by her mother. As Meera grows into a young woman (M. S. Subbulakshmi), her devotion to Krishna grows.

Much against her wishes, Meera is married to Rana (Chittor V. Nagaiah), the king of Mewar. But even after marriage, her love for Krishna remains unchanged. She follows her own ideals and way of living which are not acceptable to Rana and his family, especially his brother Vikraman (T. S. Balaiah) and sister Udra Devi. Meera requests Rana to construct a temple for Krishna in Chittoor (capital of Mewar). Out of love for her, Rana agrees and constructs a temple. An overjoyed Meera remains in the temple most of the time, singing in praise of Krishna along with other devotees and avoids staying at the palace.
On Vijayadasami day, Rana expects Meera to be with him at the royal assembly, when other kings come to offer their respects. But on her way to the assembly, Meera hears Krishnas ﬂute playing, returns to the temple and remains there. The king gets angry when he realises that Meera has gone back to the temple again, thus ignoring her duties as a wife. To get rid of Meera, Vikraman gives a poisoned drink through Udra, but Meera is saved by Krishna and the poison does not affect her. Instead, Krishnas idol at the Dwarakadheesh Temple (the origin of Krishna) becomes deep blue (the poison effect), the temple doors close spontaneously, and remain closed.
 pearl necklace as a gift, which Meera puts on Krishnas idol. Rana gets angry when he comes to know of these developments and her disinterest to fulﬁll her duties as a wife and queen; he orders the demolition of the temple using cannons so that she will come out. Vikraman goes to the temple and orders Meera and the other devotees to come out before the demolition begins. However, Meera refuses, stays back in the temple and continues her bhajans.

Meanwhile, Rana comes to know from his sister Udra about Vikramans failed attempt to kill Meera by poison. Shocked when he realises Meeras real identity (she is one with Krishna), he rushes to meet her in the temple which is about to be demolished. When a cannon is ﬁred, Rana stops it and gets injured. When Meera hears Krishna calling her, she admits to Rana that she has failed in her duties as a wife. She explains that her heart is with Krishna and seeks Rana’s permission to leave palace life and her desire to visit Krishnas temple at Dwarka|Dwaraka. Rana realises her devotion and allows her to go. Once Meera leaves, the Mewar kingdom suffers serious drought and the subjects plead with Rana to bring Meera back, so Rana goes in search of her. Meera ﬁrst goes to Brindavanam and meets the sage (Serukalathur Sama) who originally predicted about her devotion. Together, they leave for Dwaraka, the birthplace of Krishna; on reaching the temple, she starts singing in praise of Krishna. Rana, who has followed her, also reaches the temple. The doors of the temple, which were closed till then, open. Krishna appears and invites Meera inside. Meera runs towards the god and falls dead while her soul merges with him. Rana comes rushing in only to ﬁnd Meera’s corpse. Meera’s devotion to Krishna is ﬁnally rewarded and she is united with him.

== Cast ==
* T. S. Balaiah as Vikraman
* Kumari Kamala as Krishna
* Chittor V. Nagaiah as Rana, the king of Mewar.
* M. S. Subbulakshmi as Meera
* Radha Viswanathan|Radha, Young Meera
* M. G. Ramachandran

== Production ==
===Development===
Sadasivam dreamt of taking M. S. Subbulakshmis music even to the common man. Nothing but cinema would be able to achieve this goal, he knew. So he started looking for a good story. He had several discussions with intellectual friends like Kalki, and was of the opinion that if Subbulakshmi was to act in a film, it could not be a mass entertainer, but would need to carry a universal and uplifting message for the masses. And what better rasa (theology)|rasa than bhakti to convey lofty ideas! After much deliberation, Subbulakshmi herself chose the story of Meera. 

Meera was launched in 1943, and while Subbulakshmi was cast as the titular character, her stepdaughter Radha Viswanathan was recruited to play the younger Meera.  To prepare for the part of Meera, Subbulakshmi stated that she would go to all the places where Meera had wandered in search of the elusive Krishna, and would worship at all the holy temples that Meera worshipped at.    Kannada singer-actor Honnappa Bhagavathar was the first choice to play Meeras husband King Rana which he accepted, but was later replaced by Chittor V. Nagaiah, much to Bhagavathars annoyance. Nagaiah was the own choice of the films director Ellis R. Dungan. An undisclosed person suggested P. U. Chinnappa, but Dungan rejected him as he felt the actor was "uncouth and did not have the regal presence" needed for the role. According to Dungan, Nagaiah "proved the right choice for a Rajput king". 

N. S. Krishnan and T. A. Madhuram were supposed to have acted in Meera. However, Krishnan was arrested in December 1944 as a suspect in the Lakshmikantham Murder Case, precluding him from acting in the film. Others added to the cast were T. S. Durairaj, K. R. Chellam, K. Sarangapani, T. S. Balaiah, Serukalathur Sama, T. S. Durairaj, "Appa" K. Duraiswami, and M. G. Ramachandran in a minor role.     Kumari Kamala was chosen to act as Krishna, an irony as the character is male while Kamala is female.  She also appeared as Krishna in some "cut-away shots" in the scene of the song "Kaatriniley varum geetham" that was choreographed by Ramaiah Pillai. 

===Filming===
Dungan, being a well-trained technician in Hollywood, brought in many creative and technical innovations that were never seen before in Indian cinema of that period. Dungan shot a scene that "created history in south Indian film technique". The toddler Meera (Radha), changes into a young woman (Subbulakshmi), and the transition is made with the song "Nanda balaa enn manaalaa" sung by Radha and Subbulakshmi. When the changeover takes place, there is a 45-second, fast-paced musical interlude by the background orchestra as bridge as part of the song. Normally such background musical interludes are recorded along with the song in a recording studio long before the shooting of the film commences. But Dungan did not do so. 

He shot the scene first and the changeover sequence consisted of a number of shots of the statue of Krishna, lighted candles with flames flickering, flowers on trays, prayer offerings, Krishnas flute in the statue, and then a cut to a close-up of Subbulakshmi singing with heavy feeling and emotion, "Hey! Murali... Mohana..." The shots were static, and also on fast trolley in close-up. (There were no zoom lenses in India before 1945) Dungan edited them all himself into a speedily cut fast-paced sequence first. Then, music director S. V. Venkataraman composed the background music, in rhythm with the shots in a recording theatre. It was the first time such a technique was used in Indian cinema. 

Dungan had studied the history and culture of the Rajputs, even the colour of the bangles worn by Rajput maidens on their wedding day. He declared that, to be "credible and historically accurate", they would have to shoot the film on actual locations in Rajasthan, including the sacred spots of Brindavan and Dwaraka. A 20-member crew including Subbulakshmi, Radha, Dungan and cameraman Jiten landed first in Brindavan, young Krishnas playground. On the first day of shoot, Subbulakshmi led a group of bhajan singers through the streets singing Brindavan ki mangal lila. 

During the shooting, Subbulakshmi had a providential escape from drowning. The scene to be shot was one in which Meera would cross the Yamuna in a boat, the boat would capsize and she would be saved by Krishna who would appear in the guise of a boatman. While the shooting was in progress, Subbulakshmi accidentally hurt her head and fell unconscious. The crew rescued her, but only after a few anxious moments.  Next stop was Jaipur. Over there, shooting took place at the old springtime mansion at the Purana Ghat. The famous scene shot there was of Meera sitting by the marble pool and strolling in the garden, singing of the magic flute which had enchanted her from childhood. 

Shooting also took place at Dwaraka, the native town of Krishnas birth.    As Dungan was not a Hindu, he was not allowed to enter the Krishna temple. Therefore, he disguised himself as a Kashmiri pundit, wearing a turban and fake beard, and he went in unquestioned.  Dungan shot the indoor part in Newtone Studio and went all the way to Rajasthan for the outdoor shoot to impart realism to Meera. There were some problems getting permission to shoot but Dungan solved them all by the sheer impact of his personality coupled with the fact that he was an American. 

==Release==
===Reception===
 
"Meera" was released on Deepavali Day in 1945. Two years later, the Hindi version came out in 1947, and with it MS became a national celebrity. The film had an on-screen introduction by the noted politician and poet, Sarojini Naidu, who described MS as "The Nightingale of India". The film was seen by Pandit Jawaharlal Nehru, the Mountbattens and other leaders who became her ardent fans and friends. She went on to conquer new areas around the world and became an international celebrity.  The film was screened at various film festivals such as Prague Film Festival, Venice Film Festival and Toronto Film Festival. 

===Reviews===
The Free Press Journal said, "Meera transports us into a different world of bhakti, piety and melody. It shatters the misguided belief that film music is inferior. Subbulakshmi follows no stereotyped techniques in acting. She is just Meera."   IBN Live included the film in its list of 100 greatest Indian films of all time.   

==References==
 

==Further reading==
* http://www.hindu.com/fr/2004/09/17/stories/2004091702890600.htm
* http://www.hindu.com/fr/2004/12/17/stories/2004121700410500.htm
* http://www.hindu.com/thehindu/fr/2002/02/01/stories/2002020100850300.htm
*  

==External links==
*  
*  
*  

 
 
 
 
 
 
 