Springsteen & I
{{Infobox film
| name           = Springsteen & I
| image          = Springsteenandi.jpg
| image_size     = 
| alt            =
| caption        = Official poster featuring the images of over 350 fans 
| director       = Baillie Walsh
| producer       = Ridley Scott
| starring       = Bruce Springsteen, E Street Band
| music          = Bruce Springsteen
| cinematography = 
| editing        = 
| studio         = Black Dog Films
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Springsteen & I is a 2013 documentary-biographical film directed by Baillie Walsh documenting the life and career of Bruce Springsteen through the eyes and insights of his fans throughout the world. 

==Background== Life in a Day. Walsh will use only fan submitted video and photographs where fans will provide their most personal insights, abstractions and reflections on how Bruce Springsteen and his music has affected their lives.

Walsh stated, "We are searching for a wide variety of creative interpretations, captured in the most visually exciting way you can think of, whether youve been a hardcore Tramp since 73 or have heard one of his songs for the first time today! If you have a parent, a sibling, a neighbor or a colleague who has an interesting tale, we want to know about them. If you cant use a camera or are not sure how to capture your story then get in touch and we will link you up with someone who can!"
 
The production team will edit the footage to feature length and are therefore requesting that submissions be raw footage rather than edited material. As well as moving image accounts on camera, the filmmakers are looking for fans to provide photographs, old footage or audio narration to help them build up a picture of what the acclaimed songwriter and musician means to them. Movie and image files will be able to be quickly uploaded through the film’s website springsteenandi.com by following simple online instructions. Ideally, the higher quality the footage the better, but the producers stress that any equipment, from smartphones to hi-def cameras are accepted – as long as they are all under 5 minutes in length. The documentary will feature unseen live performances and some of Springsteens most loved songs. Submissions for the film were accepted from November 15, 2012 through November 29, 2012. Walsh then edited the footage together for the films 2013 release.
 Wrecking Ball at the age of 60, that’s an amazing achievement." Walsh said that Springsteen saw the film and really enjoyed the humor in it. The people in the film were really eloquent. His management informed Walsh that Springsteen really enjoyed the film. 

An interactive poster for the film was released on June 7, 2013 featuring the images of over 350 fans.

==Release==
The film was released on July 22, 2013 simultaneously via a worldwide cinema broadcast in over 50 countries and in over 2000 movie theaters. A trailer was released on May 23, 2013.  while an extended trailer was released in July 2013. Tickets for the film went on sale across the world on June 7, 2013 while tickets for the United States went on sale June 14, 2013.
 Showtime  and was released on DVD and Blu-Ray on October 29, 2013. 

==Bonus theatrical footage==
Following the films credits, fans who attend the theatrical release will be treated to a special six song, 45 minute live performance from Springsteens set at the 2012 "Hard Rock Calling" festival which featured Paul McCartney. Following the live footage there will be an epilogue featuring some of the films more stand-out fans who were able to give an update on their lives and who were lucky enough to meet Springsteen backstage after one of his shows. The bonus footage was included in the DVD and blu-ray release.

==Reception==

Critical reviews for the film have been mostly positive Randy Lewis of the Los Angeles Times said "When fans describe the passion of his performances or the connection of his music and lyrics to episodes in their lives, its clearly heartfelt, but not significantly different than what fans of Bon Jovi or Billy Joel or Justin Bieber might say about their favorite artists. If Springsteen and I doesnt conclusively answer the question of what distinguishes him from the thousands of others who have traveled the path of rock n roll before and since he came along, it does give the faithful, and even the curious, a lot to bond over."  Austin OConnor of AARP called the film "A love letter to The Boss" and that "Even the biggest Springsteen hater might be converted to one of the faithful.   Its a quirky little film, at turns charming and creepy but ultimately redeeming, about people who are extremely devoted to the 63-year-old rock star." OConnor also discussed the films flaws by saying "But there are missteps. Some of the glorified YouTube videos seem to straddle the line between fan and stalker. One woman who sits far too close to her webcam gushes so rapturously about Springsteen’s effects on her you wonder if a restraining order might be required. Another guy talking about Springsteen while driving his car suddenly breaks down in tears." 

==References==
 

==External links==
 
*  
*  
*  

 

 
 
 
 
 
 
 
 
 