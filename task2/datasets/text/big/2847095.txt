Time Stands Still (film)
{{Infobox film
| name     = Time Stands Still
| image    =
| writer   = Péter Gothár Géza Bereményi
| starring = Anikó Iván István Znamenák Péter Gálfy Lajos Őze
| director = Péter Gothár
| released =  
| runtime  = 103 minutes
| country  = Hungary
| language = Hungarian
| budget   =
}} uprising of Best Foreign Language Film at the 55th Academy Awards, but was not accepted as a nominee. 

==Cast==
* Anikó Iván as Szukics Magda
* István Znamenák as Dini
* Péter Gálfy as Wilman Péter, "Vilma"
* Henrik Pauer as Gábor
* Sándor Söth as Pierre
* Ágnes Kakassy as Anya
* Lajos Öze as Bodor
* Pál Hetényi as Apa 

==See also==
* List of submissions to the 55th Academy Awards for Best Foreign Language Film
* List of Hungarian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*  

 
 
 
 
 
 
 