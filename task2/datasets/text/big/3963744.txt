Le Jour Se Lève
{{Infobox film
| name           = Le jour se lève
| image          = Le jour se leve.jpg
| caption        = Film poster
| director       = Marcel Carné
| producer       = Robert and Raymond Hakim
| writer         = Jacques Prévert Jacques Viot
| starring       = Jean Gabin Jules Berry Arletty
| music          = Maurice Jaubert
| cinematography = Philippe Agostini André Bac Albert Viguier Curt Courant
| editing        = René Le Hénaff
| distributor    = AFE
| released       =  
| runtime        = 93 min.
| country        = France French
| budget         =
}}
Le jour se lève (or Daybreak) is a 1939 French film directed by Marcel Carné and written by Jacques Prévert, based on a story by Jacques Viot. It is considered one of the principal examples of the French film movement known as poetic realism.

In 1952, it was included in the first Sight and Sound top ten greatest films list.

==Synopsis==
The film begins with foundry worker François (Jean Gabin) shooting and killing Valentin (Jules Berry).  François then locks himself in his room in a guest house at the top of many flights of stairs. He is soon besieged by the police, who fail in an attempt to shoot their way into the room, as François barricades himself in.

In a series of flashbacks punctuated by glimpses of the present, it is revealed that François had become involved with both the naive young floral shop worker Françoise (Jacqueline Laurent), and the more experienced Clara (Arletty), who until she met François had been the assistant in Valentins performing dog act. It becomes clear that the manipulative Valentin, an older man, had himself been involved with both women, and he becomes jealous of François (at one point, mendaciously telling François that he, Valentin, was Françoises father, although both she and François had grown up in orphanages). Finally Valentin confronts François in his room, bringing with him the gun with which François eventually shoots him.

As we return to the present, François continues to chain-smoke nervously in his room.  Françoise, having learned of his plight, has become delirious and is being tended to by Clara in her room at a nearby hotel. Then, two policemen climb over the roof of Françoiss building, preparing to throw tear gas grenades through the window of Françoiss room.  Before they can do so, François, consumed with despair, shoots himself in the heart. The film ends with tear gas clouds filling the room around his lifeless body, as the alarm clock, which he set earlier at the beginning of his meeting with Valentin, starts to ring, announcing the morning.

==Cast==
* Jean Gabin as François
* Jacqueline Laurent as Françoise
* Jules Berry as M. Valentin
* Arletty as Clara
* Arthur Devère as Mr. Gerbois
* Bernard Blier as Gaston
* Marcel Pérès as Paulo
* Germaine Lix as La chanteuse
* Georges Douking as blind man

==Distribution==
Le Jour se lève was released in France in June 1939 and shown in the US the following year. In France, however, the film was banned in 1940 by the Vichy government on the grounds it was demoralizing.  After the wars end, the film was shown again to wide acclaim.
 The Long Les Enfants du paradis as one of the finest achievements of the partnership of Carné and Prévert. 

==See also==
*List of rediscovered films

==References==
 

==External links==
*  TCM Movie Database

 
 

 
 
 
 
 
 
 
 
 