What's Love Got to Do with It (film)
{{Infobox film
| name           = Whats Love Got to Do with It
| image          = Whats love got to do with it poster.jpg
| border         = 
| caption        = Theatrical release poster Brian Gibson
| based on       =  
| screenplay     = Kate Lanier
| producer       = Doug Chapin  Barry Krost
| starring       = Angela Bassett  Laurence Fishburne
| studio         = Touchstone Pictures Buena Vista Pictures
| released       =    
| runtime        = 118 minutes
| country        = United States
| language       = English
| budget         = $15 million
| gross          = $39,100,956
}} biographical film Brian Gibson, loosely based on the life of Tina Turner. It stars Angela Bassett as Tina Turner and Laurence Fishburne as Ike Turner.
 soundtrack featured the hit song "I Dont Wanna Fight", which went to number one in seven countries. In the United States, the film grossed almost $50 million and around $20 million in rentals. In the United Kingdom, it grossed nearly £10 million.

==Storyline==
Born and raised in the small Tennessee town of Nutbush, Tennessee|Nutbush, Anna Mae Bullock (Tinas original name) grows up in an unhappy family, with her parents later leaving and abandoning her and taking only her sister. Following her grandmothers death, she relocates to St. Louis, reuniting with her mother and close sister Alline. Anna Mae pursues a chance to be a professional singer after seeing charismatic bandleader Ike Turner perform one night. Later she wins her spot in Turners band after singing onstage and he begins mentoring her. In time, an unexpected romance develops between the two after she moves into Ikes home. Shortly afterwards, they marry and begin having musical success together as Ike and Tina Turner.
 defend herself, eventually leaving Ike after they arrive at a hotel.
 Roger Davies, who eventually helps her realize her dreams of rock stardom. Despite Ikes attempts to win her back, Tina eventually prevails and finds solo success, accomplishing her dreams without Ike.

==Cast==
*Angela Bassett as Anna Mae Bullock / Tina Turner
*RaeVen Larrymore Kelly as Anna Mae Bullock (Young)
*Cora Lee Day as Grandma Georgiana
*Khandi Alexander as Darlene
*Laurence Fishburne as Ike Turner
*Jenifer Lewis as Zelma Bullock
*Phyllis Yvonne Stickney as Alline Bullock
*Penny Johnson Jerald as Lorraine Turner
*Vanessa Bell Calloway as Jackie
*Chi McBride as Fross
*Sherman Augustus as Reggie
*Terrence Riggins as Spider
*Bo Kane as Dance Show Host
*Terrence Evans as Bus Driver
*Rob LaBelle as Phil Spector Roger Davies
*Richard T. Jones as Ike Turner, Jr.
*Shavar Ross as Michael Turner
*Damon Hines as Ronnie Turner
*Suli McCulloghh as Craig Turner

==Differences from noted events==
 
Though the film took its story from Tina Turners autobiography, I, Tina, it was determined to be only loosely based from the book, meaning certain events in the film were "fictionalized for dramatic purposes".  Examples of the fictionalized scenes include:

*In the film, Tinas constant friend during the film, "Jackie" (played by Vanessa Bell Calloway), was put in the story. Tina Turner never had a friend named Jackie. The same friend is not only a member of The Ikettes but is also shown as the person who visits Tina in the hospital after her attempted suicide and later converts Tina to Nichiren Buddhism and chants "Nam Myoho Renge Kyo" in front of the "Gohonzon". The "friend" apparently was a combination of friends from Turners past, including members of the Ikettes and friends from her and Ikes entourage.

*When Anna Mae Bullock sees Ike Turner and his Kings of Rhythm perform, Ike is shown as the front man. While the real-life Ike Turner was a band leader and organizer, he often had other performers, particularly male singers, fronting the band. Turner had stage fright. The original vocalist behind "Rocket 88", sung by Lawrence Fishburne as Ike in the movie, was Jackie Brenston.

*During Anna Maes first performance with Ike and the Kings of Rhythm, Ike is shown playing guitar. But Tina and Ike stated Tina sung after being given a microphone by the bands drummer and singing while Ike was playing piano during an intermission, rather than a live performance as indicated in the film though she later did front the band the night she began singing with them.

*A reenactment shows Tina recording her first song, which is called "Tinas Wish", on the films soundtrack. However, its actually a re-recording Tina did of an old Ike and Tina song, "Make Me Over", from the Ike and Tina album, Nutbush City Limits, released in 1973. Tinas first recording (as "Little Ann") was actually singing background on one of Ikes songs, "Box Top".

*During the scene where Ike and Tina are scheduled to perform at the Apollo Theater in 1960, the bill has Martha and the Vandellas and Otis Redding as fellow headliners. In reality, Martha and the Vandellas hadnt formed yet, with several of them performing in another different vocal group called the Del-Phis, while Redding had yet to record his first single and was fronting several bands.

*The film shows Tina giving birth to Ikes son who is named Craig but in reality, Craig Turner was Tinas child with another man, Raymond Hill. Ike and Tina did not have a child until 1960, when son Ronnie Turner was born and Ronnie would be their only child; Tina was pregnant with Ikes child in 1968 but allegedly had an abortion.

*Like Vanessa Bell Calloways character "Jackie", there was no "Fross", "Reggie" or "Spider" in Ike and Tinas private circle.

*While Tina mentions that there were accounts of Ike being abusive to her, there was no incident where Ike pushed cake in Tinas face leading to a food fight in the diner. The reenactment is a dramatization of an incident Tina Turner herself accounted in her autobiography saying that while waiting for food, a waitress gave her cake. Though Tina said she hadnt ordered it, Ike told her to eat it.

*In one scene, Tina calls her mother and tells her that shes heading over to her house in St. Louis to hide from Ike Turner. After Ike locates where Tina is, he berates her for trying to escape and makes it appear as if Ike had gotten word about what Tina was doing from her mother but in her memoirs, Tina Turner said that Ike knew where to seek her out.

*When Ike and Tina are performing "Proud Mary", the timeline has it around 1968, with them opening for The Rolling Stones. Creedence Clearwater Revivals original version of "Proud Mary" was not released until January 1969, and Ike and Tina did not include the song in their act until 1970. Also, Ike and Tina opened for the Stones twice, in 1966 on their UK tour and in 1969 on their US tour.

*The film alleges that Ike and Tina did not have a hit after "A Fool in Love" was released but, in truth, the duo had several hit singles between the release of "A Fool in Love" and "River Deep - Mountain High", including "Its Gonna Work Out Fine", "I Idolize You", "Tra La La" and "Poor Fool".

*The films most infamous scene showcases "Ike" committing what looks like an act of rape on "Tina" after beating her up in the studio. Both Turners denied the rape scene reenacted in the film but Tina writes in her memoirs that after Ike would physically assault her, he would ask for sex stating Ike "instilled fear" in her.
 drugged state where her Ikettes noticed that she had attempted suicide, with the timeline being given as 1974. In real life, the incident occurred in 1969 before a show in Los Angeles and occurred shortly after Tina learned one of her friends and a member of the Ikettes was pregnant with Ike Turners child.

*When Tina Turner performs at the Ritz (rock club)|Ritz, the film has the timeline at 1983 and the host announces her arrival as her "debut solo performance", but Tina had first performed at the theater in 1981. Also proven to be inaccurate was Ike Turners visit to the theater where he failed at silencing Tina with his gun. In truth, Ike Turner was never at the Ritz. In fact, Ike Turner had not been seen in public for years following his and Tinas divorce in 1978 and had remained in Los Angeles.

*Though the film depicts Tina Turner as having addressed the courtroom to retain her stage name, in an interview with Oprah Winfrey in 1996, she denies speaking up but said her lawyer had brought it up in court after Tina advised her lawyer to drop a potential financial support suit against Ike as their divorce court dragged on for a year.
 Love Explosion. It also concluded that Ike Turner was still serving prison for drug possession; he had actually been released from jail after being granted parole in 1991.

==Awards and nominations== NAACP Image the Academy Best Actor Best Actress in a Leading Role. The film won an American Choreography Award for one of its dance sequences.

* Academy Awards
** Nominated — Academy Award for Best Actress (Angela Bassett) 
** Nominated — Academy Award for Best Actor (Laurence Fishburne)

* Golden Globes
** Won — Golden Globe Award for Best Actress – Motion Picture Musical or Comedy (Angela Bassett)

==Production==
 
 , nominated for an Academy Award for her portrayal of Tina Turner]]
Halle Berry, Whitney Houston, Robin Givens, Pam Grier, Vanessa L. Williams and Janet Jackson were all considered for the role of Tina Turner.  It was Whitney Houston who was actually offered/received the role, but had to decline due to imminent maternity. Jenifer Lewis, who plays Tinas mother in this film, originally auditioned to play Tina Turner. Lewis is only one year older than Bassett.

Laurence Fishburne was offered the role of Ike Turner five times and turned it down each time. When he found out that Angela Bassett was cast as Tina Turner, he changed his mind. 

All the Ike and Tina Turner songs used in the film were newly re-recorded versions featuring Tina Turner covering her own songs. On "Proud Mary", Laurence Fishburne sings Ike Turners parts. For Tina Turners solo recordings the original masters were used, including the Phil Spector-produced "River Deep - Mountain High".

In his autobiography Taking Back My Name, Ike Turner claims the movie damaged his reputation immensely and attacks many of the scenes for being either not strictly accurate or completely fabricated.

Bassett was injured while filming the first spousal abuse sequence. She fell off the back of a high-rise sofa, put her hands out to reduce the impact and suffered a hairline fracture of her right hand. She only tried the stunt fall once, and footage leading up to the mishap appears in the film.

Actress Vanessa Bell Calloway, who plays the fictional character of Jackie, was wary of chanting the Buddhist words because of her strong Christian faith. Director Brian Gibson allowed her to form the words with her lips silently during filming and added the words with a voice double in post-production. She also appeared alongside Tina in the music video of "Whats Love Got to Do with It".

==Reception==

The movie gained a mostly positive reception from critics.     It currently holds a 96% "fresh" rating on Rotten Tomatoes based on 49 reviews with the consensus: "With a fascinating real-life story and powerhouse performances from Angela Bassett and Laurence Fishburne, Whats Love Got to Do with It is a cant miss biopic." Ike Turner complained that Fishburnes portrayal of him was inaccurate. 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 