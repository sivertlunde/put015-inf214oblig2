Lego: The Adventures of Clutch Powers
{{multiple issues|
 
 
 
 
 
 
}}
{{Infobox film
| name           = Lego: The Adventures of Clutch Powers
| image          = Lego - The Adventures of Clutch Powers Coverart.png
| alt            = 
| caption        = 
| director       = Howard E. Baker
| producer       = Kristy Scanlan  | writer         = Tom Rogers
| starring       = Ryan McPartlin Yvonne Strahovski Jeff Bennett Roger Rose Paul Michael Glaser Christopher Emerson Gregg Berger Alex Desert Eric and David Wurst
| cinematography = 
| editing        = Michael D. Black The LEGO Group
| distributor    = Universal Studios Home Entertainment
| released       =  
| runtime        = 82 minutes
| country        = United Kingdom/United States English
| Followed by    =  
}} CGI animated comedy film|comedy-adventure film. The film is based on the toy series of the same name, Lego. Its the first movie in the Clutch Powers series.

The film stars Ryan McPartlin, Yvonne Strahovski, Roger Rose, Jeff Bennett, Paul Michael Glaser, Gregg Berger, Christopher Emerson and Alex Désert.

==Plot==
 
The film starts with a man named  ), a firefighter, weapons specialist, and demolition expert; Peg Mooring (Yvonne Strahovski) a biologist, and Bernie von Beam (Jeff Bennett) an engineer. Playwell informs of an incident in the Space Police prison planet when chaos occurs regarding to the imprisonment of the three most wanted criminals in the Lego galaxy. Clutch and his team investigate the situation at once, despite the teams lack of cooperation and Clutchs intent of working alone. They soon arrive at the planet.

They see that someone is stuck in one of the prisoners cells, but they are attacked by an unseen Wizard (fantasy)|wizard, who was one of the criminals leading the other two. The criminals then disappear and Clutch and his friends break the person out of the cell, revealing himself to be the Watch Commander. Clutch and his team attempt to go after the criminals, though the Watch Commander comforts them by stating that he has pulled off the spark plugs of all ships to ensure that the criminals will never escape from the planet. Unfortunately, the criminals have taken the teams ship (due to the team having left it open after squabbling with each other) and destroy the other ships, inciting the Watch Commander to berate the team for their arguing and their lack of cooperation. Feeling guilty, Clutch decides to take action and builds a new ship for the team to move on, declaring that a team arguing with each other while being ordered to do their tasks is the sole reason why he always works alone. During the trip back, the team is informed by Playwell that the symbol refers to Omega, one of the two criminals led by the evil wizard, and that the criminals have gone their separate ways following the escape. Playwell also informs that the evil wizard who planned the escape is none other than Mallock the Malign, infamous for terrorizing a medieval planet named Ashlar. The planets ruler, the late King Revet, sacrificed his own life to ensure his kingdoms safety and Mallocks previous imprisonment. He left his son Prince Varen with his powerful golden sword to rule, despite Prince Varens incapability of preparing in battle. Playwell then orders Clutch and the team to travel there to capture Mallock. The team manages to get to Ashlar, but their ship accidentally knocks down Lego-Henge (a spoof of Stonehenge). They get off and take refuge in a medieval camp with extra parts, but not after witnessing a group of armed skeletons marching down the forests, realizing that Mallock has gained a stranglehold of Ashlar around his fortress with a skeleton army acting as his own forces. As Peg, Brick, and Bernie build a battle chariot, Clutch leaves to get Prince Varen to convince him to fight against Mallock. Meanwhile in the fortress, Mallock learns of the teams presence and orders his two skeleton henchmen Skelly and Bones to go after Clutch so they can find the prince and the golden sword to finalize his takeover of Ashlar. Back in the forest, Clutch ends up in a bridge but a troll named Hogar refuses to let him across until Clutch solves three riddles. Though Clutch gets them all right, Skelly and Bones catch up with him after a failed attempt to trap him, demanding Hogar to surrender Clutch to them at once. Hogar attempts to ward off the skeletons by hypnotizing them, but they dont fall for it, prompting an annoyed Hogar to swallow Clutch and runs away to a secret doorway that is painted like an ending forest to escape the skeletons.

Having escaped Skelly and Bones, Hogar vomits out Clutch and shows him the way to the kingdom of Prince Varen, telling him about the history of his father and the golden sword. Hogar also tells Clutch that he was the only troll that was on the kingdoms team and that he was assigned to protect Varen. Back at the medieval camp, Bernie develops a crush on Peg, who leaves to look for a relaxing land, but later befriends a dragon. Back in the kingdom, Clutch finds Prince Varen and he helps him wield a sword but he fails and gets angry at Clutch. Clutch protests that he was trying to help him defeat the wizards forces, but Varen rejects him, confessing that hes scared of Mallock. Enraged, Clutch goes back to the camp and sees that a dwarf will help them so they set out to stop Mallock on their own.

Eventually, Prince Varen, Hogar, and their knights find out that Clutch was telling the truth and decide to help them in their battle against Mallock. Varen and Clutch stow away in the back of Mallocks castle by disguising Peg as Varen. Unfortunately, Mallock plays wise to their moves and traps them in a bone cage outside a lava river and the growing skeleton army drives the knights away to retreat. Back in the castle, Mallock makes a screen in front of the clouds to show them that Clutch and Varen will be killed unless the golden sword is surrendered to him immediately. Having no choice, Hogar brings the golden swords case to Mallock so he can free them, only to find thats it empty at the last minute. Enraged, Mallock traps Hogar in lightning and projects him back to the edge of the wall surrounding Varens castle, leaving him trapped. Varen and Clutch send a message to the team so they can rely on themselves. Bernie gets fueled up by hope and they rebuild their broken chariot (which resembles the high-speed attack jeep used by Dr. Infernos henchmen from the Lego Agents theme) They reunite with the knights and they attack the skeleton army. Back in the castle, Peg gets the dragon to cut down the cage, freeing Clutch and Varen, who then retrieve the golden sword that Varen dropped earlier, since Mallock didnt bother to search for it. After much convincing from Clutch, Varen finally decides to face his fear of Mallock to ensure his kingdoms safety. Around the same time, Mallock attempts to lure Clutch into giving the sword to him by promising to lead him to his father, but Clutch refuses.

As Clutch watches Varen using the golden sword to fight against Mallock, Brick uses a jet pack and lands on Mallocks fortress, where he battles Skelly and Bones. Despite Brick being outnumbered, Bernie helps him by throwing a disc at the skeletons and they get knocked apart. Back inside the fortress, Mallock taunts Varen of retaining his title as Prince of Ashlar as he uses his staff to attack Varen. However, Varen finally takes enough power from Mallocks staff into his sword, allowing the sword to unleash its full power, much to Mallocks shock. Taking the opportunity, Varen uses the sword to trap Mallock with a glowing chain, declaring Mallock under arrest and himself the new King of Ashlar. Following Mallocks defeat, Hogar is freed from his restrains and the skeleton army disappears into puffs of smoke, leaving Clutchs team and the knights to celebrate their victory. With peace declared open in Ashlar, Varen is then officially declared the new King, and when Clutch and his team pack up to transport the chained Mallock back to Lego City, Varen thanks him for being loyal and for being part of the team. The team then return to Lego City, allowing the authorities to send the imprisoned Mallock in a cage back to the prison planet. Congratulating the team for their efforts, Playwell tells them that Omega and the other unnamed criminal are still on the loose, and they have gotten reports of the unnamed criminals location in another planet. Encouraged with a new-found belief of teamwork, the team head onward to a new adventure to capture the criminal.

==Cast==
* Ryan McPartlin as Clutch Powers. He is the leader of the group. He usually would rather be alone than being with a group, due to believing that a team would always have conflicts within themselves while being tasked of certain jobs, which is shown much throughout the film. He also lost his father three years ago.
* Yvonne Strahovski as Peg Mooring. She is the smartest of the group. Shes also the love interest of Bernie.
* Roger Rose as Brick Masterson, the strongest of the group. He usually is known for breaking things and sometimes, hes reckless.
* Jeff Bennett as Bernie von Beam, the "smartypants" of the group. Hes very timid most of the time. Bennet also voices Arthur Artie Fol, a Lego scientist who declares himself the biggest fan of Clutch.
* Paul Michael Glaser as Kjeld Playwell, the boss of Clutch Powers and the head of the Lego Corporation.
* Gregg Berger as Watch Commander, the senior officer and warden of the Space Prison Planet. Berger also voices Rock Powers, the father of Clutch who disappeared three years prior to the films events.
* Stephen Cox as Mallock the Malign, an evil wizard, the powerful of the three most wanted criminals in the Lego galaxy. He is determined to possess the Golden Sword of Ashlar to finalize his fight over Prince Varen and his domination over Ashlar.
* Alex Désert as Skelly, one of Mallocks skeleton henchmen. He is shown to be somewhat intelligent in nature, and is best friends with his cohort Bones. Richard Doyle as Hogar the Troll, a protector of Prince Valen and a close friend of Varens father.
* Christopher Emerson as Prince Varen, the sole, yet afraid ruler of Ashlar. He wants to prove himself of bravery to become the new King, though he has trouble due to his fear of Mallock.
* Chris Hardwick as Bones, Skellys best friend, one of Mallocks skeleton henchmen. He is shown to wield a helmet, which distinguishes him from Skelly (who is wearing nothing). He is also shown to be very dumb in nature, contrary to Skellys somewhat intelligent nature.
* John Di Crosta as Lofar the Dwarf
* Scott Weil as Villagers and Knights
* Matt Kasanoff as Cub Scout 1#
* Jake Barton as Cub Scout 2#
* Cody Taylor as Cub Scout 3#

==Reception==
 
At the meta-critic site Rotten Tomatoes, there is no established score by official critics.,  although users gave it a rating of 63%.

==See also==
*  
*  
*  
*  

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 