Premji Rise of A Warrior
 
 
{{Infobox film
| name           = Premji – Rise of A Warrior
 
| caption        = Official poster
| director       = Vijaygiri Bava
| producer       = Twinkle Vijaygiri
| writer        = Vijaygiri  Gireesh Parmar
| screenplay     = Vijaygiri
| starring       = {{Plainlist|
* Abhimanyu Singh
* Happy Bhavsar
* Mehul Solanki
* Aarohi Patel
* Malhar Pandya 
* Namrata Pathak
* Maulik Nayak
* Vishal Vaishya
}}
| music          = Kedar Upadhyay–Bhargav Purohit
| lyrics         = Bhargav Purohit Milind Gadhavi
| cinematography = Pruthvish Mistry  Nilesh Goswami  Pratik Raj
| art director         = Ghanshaym Patel
| production manager         = Mayursinh Solanki Shaswat Shah
| editing        = Chirag Vaishnav Parth Desai
| studio         = Vijaygiri Films
| released       = In Post production
| country        = India
| language       = Gujarati
}}
 Gujarati film directed by Vijaygiri Bava & produced by Twinkle Vijaygiri. It also stars the popular hindi cinema actor, Abhimanyu Singh.

The movie is about a boy named Premji played by Mehul Solanki who hails from a  Kutchi village .His journey from a small village to a upbeat city like Ahmedabad . He comes with a tragic past and his struggle to know who he really is , is the crux of this story .His fight with his inner self and with the society is constant and in his journey his soul mate, Pavan played by Aarohi Patel and his best friends Mukesh (Maulik Nayak), Chitra (Namrata Pathak) and Roy (Malhar Pandya) supports him and joins the fight against the evil . The movie is set to release in june/july . 

==Cast==
* Abhimanyu Singh as Meghji (Premji’s Father)
* Happy Bhavsar as Kuwar (Premji’s Mother)
* Mehul Solanki as Premji
* Aarohi Patel as Pavan 
* Malhar Pandya as Roy
* Namrata Pathak as Chitra
* Maulik Nayak as Mukesh
* Vishal Vaishya as Raghnath Malan

==Production==

===Development===
After directing various short films including the short film Amdavadi Mijaj,  Director Vijaygiri is now debuting with Premji – Rise of A Warrior. The concept of a boy who is struggling with himself for his own identity clicked in the director’s mind long time ago and he gradually developed the story. Dialogues of the film are written by Gireesh Parmar & Vijaygiri. The Cast includes veteran Gujarati theater artists like Happy Bhavsar, Maulik Nayak, Mehul Solanki, Namrata Pathak & Vishal Vaishya along with hindi cinema actor like Abhimanyu Singh.

===Filming=== Diu & Kutch. It was shot at more than 20 locations in 25 days.  Talking about filming the locations, VijayGiri said, "We have tried to cover the college life of Ahmedabad but the emphasis was given more to the Kutch locations as per the scripts requirement. Also, we have tried to capture Diu the way you might have never seen before."

Music for the film is composed by Kedar Upadhyay–Bhargav Purohit. The duo is well known for composing music in various stage plays. Director Vijaygiri says, "Premjis music is completely theme based music. As the story will progress, you will be able to find lots of variation in music like Kutch folk song, ritual song etc." 

==Release==

Currently in Post Production.

==References==
 

==External links==
*   on Facebook

L8

 
 
 
 
 
 
 
 