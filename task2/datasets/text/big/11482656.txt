Winners of the Wilderness
{{Infobox film
| name           = Winners of the Wilderness
| image          = Winners11janx.jpg
| image size     = 190px
| caption        = Original Film Poster
| director       = W.S. Van Dyke
| producer       =
| writer         = Story:   Titles: Marian Ainslee
| narrator       =
| starring       = Tim McCoy Joan Crawford Edward Connelly Roy DArcy Ernest Ian Torrence
| music =
| cinematography = Clyde De Vinna
| editing        = Conrad A. Nervig
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 68 min.
| country        = US
| language       = English
| budget         =
| gross          =
}}

Winners of the Wilderness (1927) is a MGM silent film, directed by W.S. Van Dyke, and starring Tim McCoy and Joan Crawford.  In this costume drama, set during the French-Indian War,  Rene Contrecouer (Crawford), the daughter of a French general falls for a soldier of fortune (McCoy). This movie was photographed mostly in black and white, but one scene was in color by Technicolor.  

==Cast==
*Tim McCoy – Colonel Sir Dennis OHara
*Joan Crawford – Rene Contrecoeur
*Edward Connelly – General Contrecoeur
*Roy DArcy – Captain Dumas
*Louise Lorraine – Mimi Edward Hearn – General George Washington Tom OBrien – Timothy
*Will Walling – General Edward Braddock
*Frank Currier – Governor de Vaudreuil
*Lionel Belmore – Governor Dinwiddie of Virginia
*Chief John Big Tree – Chief Pontiac

==Crew== David Townsend - Set Designer

==References==
 

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 
 


 
 