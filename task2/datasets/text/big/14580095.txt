Wild Boys of the Road
{{Infobox film
| name           = Wild Boys of the Road
| producer       = Robert Presnell Sr.
| image         = Wild Boys of the Road FilmPoster.jpeg
| director       = William Wellman
| story          = Daniel Ahern
| screenplay     = Earl Baldwin  Grant Mitchell
| music          = Bernhard Kaun (uncredited)
| cinematography = Arthur L. Todd Thomas Pratt
| released       = October 7, 1933
| runtime        = 67 minutes
| language       = English
| studio         = First National Pictures
| distributor    = a Warner Bros.-First National Picture
}}
 American film telling the story of several teens forced into becoming hobos. The film was directed by William Wellman from a screenplay by Earl Baldwin based on the story "Desperate Youth" by Daniel Ahern. The film stars Frankie Darro. In 2013 the film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".   

==Plot== Grant Mitchell) about getting him a job, only discover that his father has himself just lost his own. Eddie sells his beloved car and gives the money to his father, but when his father remains unemployed, the bills keep piling up, and the family is threatened with eviction. Eddie and Tommy decide to leave home to ease the burden on their families.
They board a freight train, where they meet Sally (Dorothy Coonan), another teenager, who is hoping her aunt in Chicago can put her up for a while. More and more teens hop aboard the train. 

When they reach Chicago, they are met by the police. Most of the transients are sent to detention, but Sally has a letter from her aunt, so they let her through. She claims her companions are her cousins; the kindly policeman is skeptical, but lets them go. Sallys Aunt Carrie (Minna Gombell) welcomes all three into her apartment. However, before they even have a chance to eat, the place is raided by the police. The trio hastily depart and continue heading east. 
 prosthetic leg for Tommy.
 New York Municipal Dump. Eddie finally lands a job, but needs to find $3 to pay for a coat he has to have. They panhandle to raise the money. When two men offer Eddie $5 to deliver a note to a movie theater cashier across the street, he jumps at the chance. The note turns out to be a demand for money. Eddie is arrested, and the other two are taken in as well when they protest. The judge (Robert Barrat) cannot get any information out of them, particularly about their parents. However, Eddies embittered speech moves him. He promises to get Eddies job back for him and dismisses the charges.

==Cast==
*Frankie Darro as Eddie Smith
*Edwin Phillips as Tommy Gordon
*Rochelle Hudson as Grace
*Dorothy Coonan as Sally 
*Sterling Holloway as Ollie, another hobo
*Arthur Hohl as Dr. Heckel, who amputates Tommys leg
*Ann Hovey as Lola
*Minna Gombell as Aunt Carrie Grant Mitchell as Mr. Smith
*Claire McDowell as Mrs. Smith
*Robert Barrat as Judge White

==See also== Miss Nobody (1926) - directed by Lambert Hillyer
*Beggars of Life (1928) - directed by William Wellman

==External links==
*  
*  
*  

==References==
 

 

 
 
 
 
 
 
 
 
 
 
 
 