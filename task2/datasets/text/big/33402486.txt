Let Me Explain, Dear
{{Infobox film
| name = Let Me Explain, Dear
| image = "Let_Me_Explain,_Dear".jpg
| alt = 
| caption = Original poster ad Frank Miller John Maxwell
| writer = Gene Gerrard Frank Miller
| based on = the play by Walter Ellis
| starring = Gene Gerrard Viola Lyel Claude Hulbert
| music =   Idris Lewis
| cinematography = Walter J. Harvey Horace Wheddon
| editing = Bert Bates
| studio = British International Pictures
| distributor = Wardour Films
| released = 1932
| runtime = 75 minutes
| country = United Kingdom
| language = English
| budget =
| gross =
}} British comedy Frank Miller and starring Gerrard, Viola Lyel and Claude Hulbert. It was adapted from the play A Little Bit of Fluff by Walter Ellis.  It was made by British International Pictures. A man tries to fake an accident in order to claim insurance money, but things soon go awry.

==Cast==
* Gene Gerrard - George Hunter 
* Viola Lyel - Angela Hunter 
* Claude Hulbert - Cyril Merryweather  Jane Carr - Mamie 
* Amy Veness - Aunt Fanny  Henry B. Longhurst - Dr. Coote 
* Hal Gordon - Parrott 
* C. Denier Warren - Jeweller 
* Reginald Bach - Taxi Driver

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 