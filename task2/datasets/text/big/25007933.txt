A Thousand Months
{{Infobox film
| name           = A Thousand Months
| image          = AThousandMonths2003Poster.jpg
| caption        = French poster
| director       = Faouzi Bensaïdi
| producer       = Bénédicte Bellocq Mustapha El Orch
| writer         = Faouzi Bensaïdi Emmanuelle Sardou
| screenplay     = 
| story          = 
| based on       =  
| starring       = Fouad Labied
| music          = 
| cinematography = Antoine Héberlé
| editing        = Sandrine Deegen
| studio         = 
| distributor    = 
| released       =  
| runtime        = 124 minutes
| country        = France Morocco
| language       = French
| budget         = 
| gross          = 
}}

A Thousand Months ( ) is a 2003 French-Moroccan drama film directed by Faouzi Bensaïdi. It was screened in the Un Certain Regard section at the 2003 Cannes Film Festival.   

==Cast==
* Fouad Labied as Mehdi
* Nezha Rahile as Amina
* Mohamed Majd as Grandfather
* Mohammed Afifi as Houcine
* Abdelati Lambarki as Caid
* Mohamed Bastaoui as Caids Brother
* Brahim Khai as The moqadam
* Abdellah Chicha as Abdelhadi
* Mohamed Choubi as Marzouk, primary school teacher
* Hajar Masdouki as Saadia
* Meryem Massaia as Malika
* Nabila Baraka as Lalla hnia
* Mohammed Talibi as The Kaid
* Faouzi Bensaïdi as Samir
* Rachid Bencheikh as The shepherd

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 