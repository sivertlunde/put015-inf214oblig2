Tracks (1977 film)
{{Infobox film
| name           = Tracks
| image          = Tracks film.jpg
| image_size     =
| caption        = DVD cover
| director       = Henry Jaglom
| producer       = Howard Zuker
| writer         = Henry Jaglom
| narrator       =
| starring       = Dennis Hopper Taryn Power Dean Stockwell
| music          =
| cinematography = Paul Glickman
| editing        = George Folsey Jr.
| distributor    = Trio
| released       =  
| runtime        = 90 min.
| country        = United States
| language       = English
| budget         =
}} 1977 Cinema American drama film written and directed by Henry Jaglom.  The film stars Dennis Hopper, Taryn Power and Dean Stockwell. The story involves a returned Vietnam veteran escorting a fellow soldiers coffin across the United States for burial.

==Plot==
 flashbacks to combat.

==Cast==
*Dennis Hopper as 1st Sgt. Jack Falen
*Taryn Power as Stephanie
*Dean Stockwell as Mark
*Topo Swope as Chloe
*Alfred Ryder as The Man
*Zack Norman as Gene
*Michael Emil as Emile
*Barbara Flood as The Lady
*Frank McRae as Train coachman

==External links==
* 
*  
 
 

 
 
 
 
 
 


 