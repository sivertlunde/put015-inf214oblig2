Mr. and Mrs. Ramachari
 
 
{{Infobox film
| name           = Mr. and Mrs. Ramachari
| image          = 2014_Kannada_film_Mr._and_Mrs._Ramachari_poster.jpg
| caption        = Film poster
| director       = Santhosh Ananddram
| producer       = Jayanna Bhogendra
| writer         = Santhosh Ananddram
| screenplay     = Santhosh Ananddram Vijay Naagendra Yash Radhika Pandit
| music          = V. Harikrishna
| cinematography = Vaidy S.
| editing        = K. M. Prakash
| studio         = Jayanna Combines
| distributor    = 
| released       =  
| runtime        = 156 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 romance drama Yash and Radhika Pandit in the lead roles. The supporting cast features Srinath, Achyuth Kumar and Malavika Avinash.
 Kannada cinema. 

==Plot summary== Vishnuvardhan and Ramachari, a character the latter portrayed in Naagarahaavu (1972). His love for the film is of such a grade that he gets a Ramachari tattoo on his chest. Due to his waywardness, his relationship with his father (Achyuth Kumar) has been damaged to an extent where the father and son cannot stand each other. His father favours his elder son Hari (Vishal Hegde), who is docile and studious, all in all, an ideal son. 

Ramachari hardly attends college and mostly hangs out with his friends Dattu (Ashok Sharma) and Chikkappa, this becoming reason for resentment of Dattus mother (Malavika Avinash). In the college one day, he encounters Divya (Radhika Pandit), a damsel in distress, her distress of being ragging by seniors. Divya, however is Dattuss sister, unbeknown to Ramachari. Like in all cliches, it is love at first sight for him. Divya too, falls for him. He prefers calling her Margaret (based on actress Shubhas character in Naagarahaavu; Vishnuvardhans love interest). The new-found love is sweet for Divya and Ramachari and they make a hot-headed decision to get married. But, they face problems with their relationship, which leads to a painful breakup. 

Ramachari agrees to marry a girl of his fathers choice to save his honor, and on hearing this, Divya attempts suicide. Later, Divya agrees to marry Aakash (Sameer Dattani), a boy of her mothers choice. Both marriages are set on the same date and in the same town of Chitradurga. Both the weddings are managed by a wedding planner, Manche Gowda (Sadhu Kokila), who mixes up the photos of bride and groom, leading to confusing situations between either parties. On the day of the wedding, Ramachari gets his fiance married to her boyfriend Farhan and performs a monologue by  Vishnuvardhans statue at Chitradurga fort. When Divya learns of this, she breaks her engagement and goes on to unite with him.

==Cast== Yash as Ramachari
* Radhika Pandit as Divya
* Srinath as Venkatesh
* Achyuth Kumar as Shankar
* Malavika Avinash as Sudha
* Vishal Hegde as Hari
* Ashok Sharma as Dattu
* Sadhu Kokila as Manche Gowda
* Honnavalli Krishna
* Aruna Balaraj
* Srinivas Prabhu
* Girish
* Rockline Sudhakar
* Mysore Naidu
* Sameer Dattani (credited as Dhyan) as Akash, in a cameo appearance

==Production==

===Casting=== Yash and Radhika Pandit.  The film was their third together, after Moggina Manasu (2008) and Drama (film)|Drama (2012). Sameer Dattani, in August 2014, said that he would be making a cameo appearance in the film. 

===Filming===
The filming of Mr. and Mrs. Ramachari began in April 2014 in Bangalore.  However, another report said, the film was shot in Goa in its first schedule before filming in Bangalore and Mysore.  With the lead pair of Yash and Pandit playing the roles of college students in the film, the college scenes were filmed in Maharajas College, Mysore. Team went on to shoot the climax portion in Chitradurga.  Filming of a song sequence was underway with an allocated budget of   in October 2014. Part of a five-day schedule, filming resumed in Nelamangala, Bangalore Rural district, after having completed the first part at Minerva Mills in Bangalore. This was when heavy rains destroyed the set put up over a period of 20 days, costing the producer a loss of  .   Another song sequence for the film was then shot in Switzerland. 
 Vishnuvardhan in his 1972 film Naagarahaavu. Yash, in the film, sports a tattoo of that character on his chest, played by Vishnuvardhan, and plays the role of a fan of the character.    Speaking on this, the films director said, "Just like the character of Ramachari in Naagarahaavu, our hero is a rebel and he is short-tempered. Moreover, he is a great fan of the Ramachari character itself and is inspired in life by it. He tries to lead a life similar to the character."  Yash got the portrait of Ramachari tattooed on his chest on all days of filming, that took over an hour, done by Mayur, a  Bangalore-based tattoo artist. 

===Marketing===
The films first look revealing the logo and the crew was released on 8 September 2014.  The first promos of Mr. and Mrs. Ramachari were released on 15 September. To mark the occasion of late actor Vishnuvardhans birth anniversary, the first teaser of the film was released on 17 September.  The first look poster of the films lead actors was released on 19 September.  It featured Yash holding Pandit by his left arm and a snake by his right.  The official film trailer was released on YouTube on 15 December 2014.

==Soundtrack==
{{Infobox album
| Name        = Mr. and Mrs. Ramachari
| Type        = Soundtrack
| Artist      = V. Harikrishna
| Cover       = Mr. and Mrs. Ramachari album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = November 2014
| Recorded    = 2014 Feature film soundtrack
| Length      = 23:32
| Label       = D Beats
| Producer    = V. Harikrishna
| Last album  = 
| This album  = 
| Next album  = 
}}

V. Harikrishna composed the background score for the film and its soundtracks. The album consists of six soundtracks.  The track "Annthamma" was sung by the male lead in the film Yash (actor)|Yash, who made his debut as a playback singer with the track.  The track had been recorded in the voices of the composer V. Harikrishna and Vijay Prakash, before it was decided that Yash record it. 

{{tracklist
| headline = Tracklist
| extra_column = Singer(s)
| total_length = 23:32
| lyrics_credits = yes
| title1 = Yaaralli
| lyrics1 = Santhosh Anandaram Ranjith
| length1 = 3:38
| title2 = Mr & Mrs Ramachari
| lyrics2 = A. P. Arjun Tippu
| length2 = 3:56
| title3 = Upavasa
| lyrics3 = Ghouse Peer
| extra3 = Sonu Nigam, Shreya Ghoshal
| length3 = 4:05
| title4 = Yenappa
| lyrics4 = Ghouse Peer
| extra4 = Tippu
| length4 = 3:30
| title5 = Kaarmoda
| lyrics5 = Ghouse Peer
| extra5 = Rajesh Krishnan
| length5 = 4:10
| title6 = Annthamma
| lyrics6 = Yogaraj Bhat Yash
| length6 = 4:13
}}

===Reception===
The album was received well by listeners of which over 15,000 was sold by mid-February 2015. It was also among the most downloaded albums on the internet at around 3 lakhs. 

==Release and reception==
The film was given the "U" certificate by the Regional Censor Board, without censoring any part of it.  It was released theatrically on 25 December 2014 across Karnataka. Following good response at the domestic box-office, it was screened in California, Germany and Dublin.   

===Critical reception===
The film released theatrically on 25 December 2014, in over 200 theatres across Karnataka. Upon release, it opened to generally positive response from critics. G. S. Kumar of The Times of India reviewed the film gave the film a 3.5/5 rating and wrote that the film had a "neat plot, interspersed with lively sequences." Of the acting performances, he wrote, "Yash is brilliant, be it his expressions, dialogue delivery or style. Radhika Pandit delivers a winsome performance. Srinath, Achyuth Kumar, Malavika and Dhyan essay their roles with ease. Ashok Sharma shows promise of a good actor." He added crediting the films music and cinematography.  Writing for Bangalore Mirror, Shyam Prasad S. called the film "a visually appealing mass entertainer". He wrote praising the films screenplay and the performances of all the actors, and its cinematography.  A. Sharadhaa of The New Indian Express called the film "a family entertainer" and praised the films screenplay, dialogues and the performances of Yash, Radhika Pandit, Achuyth Kumar, Malavika Avinash and Srinath. He concluded crediting the music, cinematography and editing in making the film "that works on many levels and is cheerful, romantic and emotional".  S. Viswanath of Deccan Herald gave the film a rating of three out of five, and said the film was "Preachy and predictable". He wrote, "If only subtlety and sanity scored over predictable commercial claptrap, this Ramachari could have also risen to cult status of S R Puttanna Kanagal’s Naagarhaavu’s own eponymous hero." and added that it "condescends to cliched contrivances". 

===Box office=== Hindi film, PK (film)|PK.  However, it collected   on the day of its release, 25 December.  At the end of its fifth day from release, it had collected  , making it one of the biggest openings for a Jayanna Combines film or a Yash film.  Following a good run, the collections totalled to   at the end of 15 days.    It reportedly grossed   by the end of its 50 days from release.  It went on to complete its 100-day run on 3 April. 

==References==
 

==External links==
*  

 
 
 
 
 
 
 