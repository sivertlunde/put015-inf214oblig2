Hands Across the Table
{{Infobox film
| name           = Hands Across the Table
| image          = Hands Across the Table poster.jpg
| image_size     =
| caption        = Trade advertisement
| director       = Mitchell Leisen
| producer       = E. Lloyd Sheldon
| screenplay     = Norman Krasna
| story          = Viña Delmar
| narrator       =
| starring       = {{plainlist|
* Carole Lombard
* Fred MacMurray
}}
| music          =
| cinematography =
| editing        =
| distributor    = Paramount Pictures
| released       =  
| runtime        =
| country        = United States
| language       = English
| budget         =
}} Swing High, Swing Low, and 1937s True Confession.

==Plot==
Brought up in poverty, hotel manicurist Regi Allen wants to marry a rich husband. Her new client, wheelchair-using hotel guest Allen Macklyn is immediately attracted to her and becomes her confidant. Despite his obvious wealth, Regi does not view him as a potential husband, and has no qualms about telling him about her goal in life. 

Exiting his penthouse suite, she encounters a man playing hop-scotch in the hallway, and declines his invitation to join him.  He makes an appointment for a manicure as Theodore Drew III, scion of a socially prominent family. Unaware that the Drews were bankrupted by the Great Depression, she accepts his invitation to dinner.

They have a good time, but Ted drinks too much and tells Regi that he is engaged to Vivian Snowden, heiress to a pineapple fortune. When Regi is unable to wake him from his drunken slumber, she lets him sleep on her sofa. He explains to her that he was supposed to sail to Bermuda last night (a trip paid for by his future father-in-law) and that he has nowhere to stay and no money.  Regi reluctantly lets him live in her apartment until his boat returns from Bermuda, at which time he can return to sponging off of Vivian.  Ted and Regi confess to each other that they intend to marry for money.

Ted and Regi play fun pranks on each other.  In the first one, Ted frightens away Regis date (an uncredited by pretending to be her abusive husband.  Later, in order to convince Vivian that he is in Bermuda, Ted persuades Regi to telephone Vivian while posing as a Bermuda telephone operator. When Regi repeatedly interrupts in a nasally voice, Ted hangs up to avoid laughing in his fiancees hearing. However, this backfires, as Vivian discovers that the call came from New York when she tries to reconnect. She hires private investigators to find out what is going on.

In the course of their stay together, Ted and Regi fall in love.  On their last night before the boat returns, they admit their mutual love, but Regi ends the relationship, insisting that Ted would resent having given up his chance to be wealthy if he were to marry her. Early the next morning, Ted leaves without saying goodbye.

Vivian has a nasty confrontation with Regi at the hotel. After Regi leaves and Ted shows up, Vivian makes it clear that she knows what happened, but is still willing to go through with the marriage. Ted, however, asks to be released from their engagement. Meanwhile, Regi goes to her regular appointment in Allens suite, but breaks down in tears. Allen had intended to propose to her, but he secretly puts away his engagement ring after she confesses she has fallen in love despite herself. When Ted finds her there, she agrees to marry him.  On a bus, Regi and Ted discuss what they should do first:  eat lunch, get married, or find a job for Ted. They toss a coin to decide; Ted jokingly says he will get a job if it lands on its side. Sure enough, it does when it gets wedged in a manhole cover.

==Cast==
* Carole Lombard as Regi Allen
* Fred MacMurray as Theodore "Ted" Drew III
* Ralph Bellamy as Allen Macklyn
* Astrid Allwyn as Vivian Snowden
* Ruth Donnelly as Laura, Regis boss and friend
* Marie Prevost as Nona, Regis friend and a strong believer in numerology

Lombard had originally wanted Cary Grant in the role of Theodore Drew III, but scheduling conflicts made him impossible to get.  0/Hands-Across-the-Table.html|title=Hands Across the Table|last=Tatara|first=Paul|work=Turner Classic Movies|accessdate=April 8, 2015}} 

==Production==
  for the film]]
The film was intended primarily as a vehicle to promote Lombards comedic acting abilities. 

MacMurray was not known for his comedic acting abilities and found it difficult to be humorous enough for the role. Director Mitchell Leisen and Lombard both worked extremely hard to get the performance they wanted out of him. Lombard at one point sat on MacMurrays chest, pounding on him with her fists and yelling, "Now Uncle Fred, you be funny or Ill pluck your eyebrows out!"  Lombard and MacMurray were unable to create the chemistry that they had with various other on-screen partners. Leisen said, "The main problem with Fred in those days was that he didnt project much sex, aside from being very good looking. In the scene where he says Arent you going to kiss me good-night? Carole was supposed to walk in and kiss him, then walk out of the frame. Well, she came out past the camera, just looked at me and shrugged her shoulders, as if to say, So what? Poor Fred!"  Lombard and MacMurray liked each other immensely, Lombard going to parties at the MacMurrays house and vice versa. With Leisens direction, they were able to project their genial relationship onto the screen. Of the scene in which MacMurray calls his fiancee and Lombard continuously interrupts stating "Bermuda calling," Leisen said, "When they finished the take, Carole and Fred collapsed on the floor in laughter; they laughed until they couldnt laugh any more. It wasnt in the script, but I made sure the cameras kept turning and I used it in the picture. It is so hard to make actors laugh naturally – I wasnt about to throw that bit out." 

==Critical reception==
The New York Times reviewer Andre Sennwald called it "an uproariously funny romantic comedy, with a brilliant screen play", with "some of the best dialogue that has come out of Hollywood in many months". 

==References==
 

 

==External links==
*  
*  
*  

 
 

 
 
 
 
 
 
 
 