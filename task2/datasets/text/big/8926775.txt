The Confessions of Amans (film)
{{Infobox film
| name           = The Confessions of Amans
| image          =
| caption        =
| writer         = Gregory Nava Anna Thomas
| starring       = William Bryan
| director       = Gregory Nava
| cinematography = Gregory Nava
| editing        = Gregory Nava
| music          =
| producer       = Anna Thomas Gregory Nava
| distributor    = American Film Institute Bauer International
| released       =  
| runtime        = 88 minutes
| country        = United States
| language       = English
| budget         = $24,000
|}} American 16mm wed wife Anna Thomas. 

The picture was partly funded by the American Film Institute.

==Plot== itinerant student of philosophy hired by an uneducated Lord to tutor his wife.

Soon, the newly employed teacher tragically falls in love with his student.

==Background== English stage Samuel Bronstons El Cid said Vincent Canby. Film locations include castles of ancient Segovia, Spain.

==Cast==
* William Bryan as Amans
* Michael St. John as Absalom
* Susannah MacMillan as Anne
* Leon Liberman as Arnolfo
* Feliciano Ituero Bravo as Nicholas
* Stephen Bateman as Landlord

==Distribution==
The Confessions of Amans was first presented in 1976 at the Chicago International Film Festival.  Later in a limited release the film opened in New York, New York on November 17, 1977.

==Reception==
===Critical response===
Vincent Canby, film critic for The New York Times liked the film and wrote, "The Confessions of Amans was a very beautiful film, though not an especially pretty one, a chilly, tightly disciplined tale of the tragic love affair of a young philosophy tutor and the wife of the lord of the manor. Like the great Robert Bresson, Mr. Nava appeared to be less interested in the heat of the passion of the lovers than in the succession of moral choices their passion represented."  

In addition, an unsigned film review written for The New York Times wrote, "  is a beautiful, muted film of the kind that takes some getting used to. People seldom raise their voices or lose control of themselves. Passion is expressed discreetly in glances or in the holding of hands." 

===Awards===
Wins
* Chicago International Film Festival: Gold Hugo Award, Best First Feature Award, 1976.

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 