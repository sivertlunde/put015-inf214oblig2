Easy Virtue (2008 film)
 
{{Infobox film
| name           = Easy Virtue
| image          = EasyVirtueposer30052.jpg
| image_size     = 215px
| alt            =
| caption        = Theatrical release poster
| director       = Stephan Elliott
| producer       = Barnaby Thompson Joseph Abrams James D. Stern
| writer         = Stephan Elliott Sheridan Jobbins
| based on       =   Ben Barnes
| music          = Marius de Vries
| cinematography = Martin Kenzie
| editing        = Sue Blainey
| studio         = Odyssey Entertainment BBC Films
| distributor    = Ealing Studios Pathé
| released       =  
| runtime        = 97 minutes 
| country        = United Kingdom
| language       = English French
| budget         = 
| gross          = $US18,463,793 
}}
 play of Easy Virtue Ben Barnes, Colin Firth and Kristin Scott Thomas. The score contains many Coward and jazz-age songs, some of which are sung or partially sung by the cast.

Easy Virtue is a social comedy in which a glamorous American widow, Larita, impetuously marries a young Englishman, John Whittaker, in the South of France. When they return to England to meet his parents, his mother takes an immediate and strong dislike to the new daughter-in-law, while his father, Jim, finds a kindred spirit. Family tensions escalate.

The film was screened at the Toronto Film Festival and London Film Festival  prior to its 7 November release by Pathé in the UK. Subsequently, the film was also screened at the Rio International Film Festival,  Middle East International Film Festival in Abu Dhabi,  and the Rome Film Festival.  It closed the Adelaide Film Festival prior to the Australian theatrical release on 12 March 2009.  In May 2009 it was released in the US. 

In the United States, the film enjoyed some commercial success. Sony Pictures Classics paid an estimated $US1 million to acquire the films distribution rights in the United States, Latin America and South Africa.  The film went on to gross $US2.5 million in limited theatrical release in the United States. 

==Plot== Ben Barnes) in Monaco. They marry, and he takes his bride home to meet his family at their dauntingly large rural mansion, where seven generations of Whittakers have been gentleman farmers. There she meets her icy cold mother-in-law, Veronica (Kristin Scott Thomas) and disheveled, sad-eyed father-in-law, Major Jim Whittaker (Colin Firth).

Veronica, already predisposed to dislike her new daughter-in-law ("Johns floozy") is further disappointed to find that she is a brash American who, like the Major, speaks fluent French. Larita also meets Johns former girlfriend and neighbour Sarah Hurst (Charlotte Riley), who is gracious about the marriage. Larita remains calm in the face of her new mother-in-laws disdain, even being so bold as to reveal having been previously married.
 Flintham Hall, Nottinghamshire) so that they can find a home of their own. Larita is bored and miserable in the countryside and hates blood sports like hunting, and indeed any of the entertainment country English people seem to enjoy. She reads Lady Chatterleys Lover, by D.H. Lawrence, which shocks her husbands female relatives, and she doesnt want to play tennis. She also dislikes Veronicas stuffy decor, her constant entertaining of her friends and the overcooked food. Worse still, she suffers from hay fever. She tries to get along with her mother-in-law, but Veronica refuses to accept her and naturally resents her attempts to bring some American traditions into the situation.

Larita makes some inadvertent gaffes, accidentally killing the family chihuahua and giving some joking advice to the younger daughter, Hilda (Kimberley Nixon), that unfortunately results in embarrassment to, and enmity from, the daughters. Sarah comes to the Whittakers parties, and to play tennis, accompanied by her brother Philip (Christian Brassington), on whom Hilda has a crush. Philip, however, is infatuated with Larita, which further angers Hilda. To Veronicas horror, she and her hunting party discover John and Larita making love in an outbuilding. Larita finds herself increasingly isolated and demoralized by her mother-in-laws derision, verbal attacks and dirty tricks. Laritas only sympathetic friends are the Major and the servants, whom she treats better than Veronica does. Larita retreats to the Majors workshop to help him work on his motorcycle. Still troubled from having lost all his men in the war, Jim Whittaker has lost interest in his home, and any love between him and his wife has long since gone.

The Whittaker estate has fallen on hard times. John, once in the shadow of his dragon-lady mother, loses his independence and seems immature to her as he is drawn into family life. In addition, Johns affection for Larita seems to be waning, as he complains about his wife to Sarah, who finds his overture inappropriate.
 helped him die using poison. John withdraws from Larita, while the Major scolds his daughters for their cruelty. At Veronicas next big party, John refuses to dance with Larita, so the Major dances a tango with her. She determines to leave the marriage, and on her way out of the mansion, she apologizes to Sarah for having interrupted her relationship with John. She hopes that Sarah will take John back. Veronica and her daughters confront Larita one last time, and an argument ensues in which Veronica and Larita trade barbs, and Larita advises the daughters to leave and see the world while they still can. Larita finally leaves after casually destroying a priceless bust, but the Major goes with her. Furber (Kris Marshall), the family butler, wishes them well.

==Cast==
* Jessica Biel as Larita Whittaker, a glamorous American widow and race car driver  Ben Barnes as John Whittaker
* Colin Firth as Major Jim Whittaker
* Kristin Scott Thomas as Mrs. Veronica Whittaker
* Kimberley Nixon as Hilda Whittaker
* Katherine Parkinson as Marion Whittaker
* Kris Marshall as Furber the butler
* Christian Brassington as Philip Hurst
* Charlotte Riley as Sarah Hurst Jim McManus as Jackson
* Pip Torrens as Lord Hurst
* Georgie Glen as Mrs. Langridon
* Stephan Elliott and Sheridan Jobbins (uncredited) as Grumpy party guests John Warburton as Mr. Gribble

==Production==
 

===Writing===
Cowards play was adapted to the screen by Stephan Elliott and Sheridan Jobbins but hardly any feature of the original play remains besides the main characters, and even they do not greatly resemble Cowards cast.

In his autobiography, Present Indicative published in 1937, Coward describes his object in the play as being "to compare the declasse woman of to-day with the more flamboyant demi-mondaine of the 1890s." He goes on to say, "The line that was intended to establish the play on a basis of comedy rather than tragedy, comes at the end of the second act when Larita, the heroine, irritated beyond endurance by the smug attitude of her in-laws, argues them out of the room." 

Although the play was made into a silent film in 1928 directed by Alfred Hitchcock and starring Isabel Jeans and Franklin Dyall, this film is not mentioned in Cowards autobiography. The 2008 version of the film is favourably reviewed by the Noël Coward Society.  

===Directing===
Filmmaker Stephan Elliott instructed Kristin Scott Thomas to play Mrs. Whittaker as a "mustache-twirling... Disney witch."  Initially, the actress responded by suggesting this is the worst direction she had ever received, but later embraced her characters wickedness and somewhat haggard appearance and unflattering wardrobe. 

=== Shooting === location at:
* Flintham Hall, Flintham, Nottinghamshire (Whittaker estate) 
* Wimpole Hall, Cambridgeshire (Hurst estate) 
* Englefield House, Berkshire (Monte Carlo) 

===Music===
  Ben Barnes sings several songs on the sound track, including "Room With a View", while Colin Firth makes a guest appearance on the closing track.
 When The Car Wash Sex Bomb were also included. 

A soundtrack album was released on 3 November 2008 in stores and on iTunes. 

==Reception==
 

===Critical response===
Review aggregation website Rotten Tomatoes gives the film a score of 53% based on 120 reviews.  Metacritic gives the film a score of 58% based on reviews from 28 critics. 

Colm Andrew of the Manx Independent gave the film 7/10 and said it was "a frothy affair but the source material is good - the script is workmanlike but at least it doesnt try to be clever and the quality of the acting makes sure the lines resonate soundly".  Some critics felt that the movie’s insistent jazz-age lilt is ultimately at odds with a play written in 1924 that attacks the hypocrisy, smugness and benighted values of the English landed gentry between the wars. The screenplay includes scattered Coward bons mots, but the witticisms don’t come as thick or as fast as in his later plays.   

Broadsheet and national British reviews were generally mixed or negative. Film Fours praised the casting of Biel and noted that, though Firths and Thomass casting was "hardly radical thinking, both offer something different from their previous period work".  Philip French of the Observer wrote that  the film was "well enough designed and photographed, but witless, anachronistic, cloth-eared, lacking in both style and period sense",  while Peter Bradshaw of the Guardian attacked its script for "undermin  the material by slipping arch modern phrases and gags into everyones mouths".  The critic of the Times awarded it only one star out of five,  whilst Nicholas Barber of the Independent wrote that "every one of Elliotts straining efforts to turn Easy Virtue into a zany, risqué farce only makes it seems stuffier and starchier".   Stella Papamichael of digital spy added that British jazz renditions of Car Wash and Sex Bomb used in the film were totally distracting.   

===Box office===
The film earned $US18,463,608 worldwide.   
The film earned $US2,656,784 in Canada and the United States, Sony Pictures Classics paid an estimated $US1 million for distribution rights in the United States, Latin America and South Africa.    

==References==
 

*  

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 