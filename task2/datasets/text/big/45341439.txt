The Flag of His Country
 
{{Infobox film
| name           = The Flag of His Country
| image          = July 2 1910 The Moving Picture World Thanhouser ad.jpg
| caption        = An image still of the film.
| director       = 
| producer       = Thanhouser Company
| writer         = 
| narrator       =
| starring       =
| music          =
| cinematography =
| editing        =
| distributor    =
| released       =  
| runtime        =
| country        = United States English inter-titles
}} silent short short drama Union and Confederacy in the American Civil War. The family is reunited thirty year later at a Grand Army of the Republic reunion through the actions of their grandchild. Little is known about the production and cast of the film, but the role of granddaughter was played by Marie Eline. Released on July 1, 1910, the film took place within living memory of the war and a reviewer in the The Moving Picture World specifically noted that the film would not offend its audience members. The film is presumed lost film|lost.

== Plot == Civil War. Union army, GAR post. Mrs. North, who believed that her husband was killed in the war, comes  orth with her brother and her little grandchild to attend the reunion of the Confederate veterans and the GAR. While out working with her grandmother, the little girl is lost, and is picked up by North on his way to attend the reunion. Through the police the grandmother and uncle are notified of the whereabouts of the child, and when they come to claim her at the GAR post, there are mutual recognitions, and Mr. and Mrs. North are reunited. There is but one flag now, the blue and the gray clasp hands beneath its folds."   

== Production ==
The writer of the scenario is unknown, but it was most likely Lloyd Lonergan. Lonergan was an experienced newspaperman employed by The New York Evening World while writing scripts for the Thanhouser productions. He was the most important script writer for Thanhouser, averaging 200 scripts a year from 1910 to 1915.  The film director is unknown, but it may have been Barry ONeil. Bowers does not attribute a cameraman for this production, but two possible candidates exist. Blair Smith was the first cameraman of the Thanhouser company, but he was soon joined by Carl Louis Gregory who had years of experience as a still and motion picture photographer. The role of the cameraman was uncredited in 1910 productions.    The only credit known for the cast is that of Marie Eline in the role of the granddaughter.  Other members cast may have included the other leading players of the Thanhouser productions, Anna Rosemond, Frank H. Crane and Violet Heming.    Bowers states that most of the credits are fragmentary for 1910 Thanhouser productions.   

== Release == living memory. The movement and influence of members in the Grand Army of the Republic was strong and memberships still exceeded 200,000 persons in 1910.  A review by The New York Dramatic Mirror stated, "This is a sure enough classy picture, full of human feeling and good acting. It is one of the kind that is giving the Thanhouser people the fine reputation they are gaining. ... The picture is strongly effective."  One advertisement billed the film as a patriotic masterpiece and would have musical accompaniment by the vaudeville musician Etta Hyland. 

== References ==
 

 
 
 
 
 
 
 
 
 