High Plains Doctor: Healing on the Tibetan Plateau
 
{{Infobox film
 
| director = Michael Oved Dayan
| producer = Michael Oved Dayan
| cinematography = Michael Oved Dayan
| music          = {{Plainlist |
* Isaac Sobol
* Roman Elinson
}}
| studio =  
| distributor = {{Plainlist |
* documentary (TV channel)
}}
| released =  
| runtime        = 67 minutes
| country = Canada
| language = English
}}
 Michael Dayan, documentary channel in Canada, it focuses on documenting the life journey of Isaac Sobol, who recounts his professional experiences and personal insights as Chief Medical Officer of Nunavut and professor of Aboriginal Peoples Health. 
 earthquake shortly after filming. The film is the only known moving picture documentation of the town before its destruction,  providing a rare document into a way of life that is disappearing. The film documents Sobol’s tenth and final medical mission to Yushu. 

==Locations==
*Yushu Tibetan Autonomous Prefecture|Yushu, Tibet
*Iqaluit, Nunavut, Canada

==Releases==
The Canadian premiere was on CBC televisions documentary channel  on May 7, 2012. The first charity (or fundraiser) pre-release screening was at the Park Theatre in Vancouver BC Canada on April 15, 2012. 
The United States premiere will be at the 2013 Santa Barbara International Film Festival  January 24 - February 3, 2013. The film is entered in the Social Justice Competition.

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 
 

 
 