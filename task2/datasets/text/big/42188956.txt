Between Valleys
{{Infobox film
| name           = Between Valleys
| image          = Between Valleys Poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Philippe Barcinski
| producer       = 
| writer         = Philippe Barcinski  Fabiana Werneck Barcinski
| starring       = Melissa Vettore Ângelo Antônio Daniel Hendler
| music          = 
| cinematography = Walter Carvalho
| editing        = 
| studio         = Pacto Audiovisual  Aurora Filmes   Cordón Films   Augenschein Filmproduktion
| distributor    = Imovision (Brazil)
| released       =  
| runtime        = 80&nbsp;minutes
| country        = Brazil  Germany   Uruguay
| language       = Portuguese
| budget         = 
| gross          = 
}}
Between Valleys ( ) is a Brazilian drama film in co-production with Germany and Uruguay.    It premiered at the Festival do Rio in September 2012. It was also screened at the Festlatino - "Latin American Film Festival of São Paulo" and at the Seattle International Film Festival. A premiere took place in the city of Paulínia, where some scenes were filmed in April 2013.

==Plot==
Vicente is an economist, father of Caio and married with Marina, a dedicated dentist. He leads an ordinary life both at home and work. However, a loss followed by another ultimately leads him to a life completely miserable. He changes his name and starts living in a garbage dump.   

==Cast==
*Ângelo Antônio
*Daniel Hendler
*Melissa Vettore
*Inês Peixoto
*Matheus Restiffe
*Edmilson Cordeiro

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 

 