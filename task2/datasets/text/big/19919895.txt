Cold Prey 2
{{Infobox film
| name           = Cold Prey 2: Resurrection
| image          = Fritt_Vilt_II.JPG
| caption        = Poster of "Fritt Vilt II"
| director       = Mats Stenberg
| producer       = Martin Sundland  Kristian Sinkerud
| writer         = Thomas Moldestad  Roar Uthaug  Martin Sundland
| starring       = Ingrid Bolsø Berdal  Marthe Snorresdotter Rovik  Kim Wifladt  Fridtjov Såheim Johanna Mørck  Mats Eldøen  Rune Melby
| music          = Magnus Beite
| cinematography = Anders Flatland
| editing        = Jon Endre Mørk
| distributor    = Nordisk Film
| released       =  
| runtime        = 86 minutes
| country        = Norway
| language       = Norwegian
| budget         = 
| gross          =
}}

Cold Prey 2: Resurrection  ( ) is a 2008 Norwegian slasher film, directed by Mats Stenberg. It is the sequel to the highly successful Cold Prey (Fritt Vilt), and premiered in Norway on 10 October 2008. It was directed by Roar Uthaug and starred Ingrid Bolsø Berdal in the leading role. The film picks up where the first left off. The female protagonist is picked up in the wilderness and brought to a hospital, but soon her nightmare starts all over again. Reviewers, though not overwhelmed, declared it a surprisingly good sequel to the original. Its opening weekend was the best for any Norwegian movie in history.

==Plot==

Jannicke (Berdal), the only survivor from the massacre depicted in the previous movie, is picked up in Jotunheimen and brought to a hospital in Otta, Norway|Otta. At the hospital, she receives good care, but seems distant from her caretakers.

While she stays in the hospital, she meets a boy named Daniel and the two have a pleasant conversation. Meanwhile in the rest of the hospital police officers Einar and Kim discuss Jannickes situation and investigate her last known location at the hotel where they discover several bodies nearby including the mountain mans and&nbsp; &nbsp;has it sent back to the hospital.

At the hospital, officer Sverre flirts with the local nurse Audhild while nurse Camilla tries to uncover Jannickes story. It is also revealed that Jannicke has been suffering nightmares from her previous encounter with the mountain man and even goes as far as attacking the mountain mans body in the morgue, much to the surprise of her caretakers.

They decide to drug her to keep her calm after her emotional outburst, much to the protest of nurse Camilla. During this time, the police investigation continues as they begin to discover more bodies.

However, at the hospital the mountain man shows sign of life and is frantically resuscitated, much to the horror of Jannicke who violently attempts to stop them but is tranquillized in the process. The doctors manage to successfully bring the mountain man back to life and leave him under watch by Sverre. Eventually the mountain man wakes up, murders a distracted Sverre, and breaks free.

He then proceeds to go in a killing spree across the hospital while a recently awoken Jannicke escapes the hospital in the aftermath with Daniel and Camilla while locking the mountain man in the basement.

They proceed to rendezvous with officers Kim, Ole, Einar and Johan outside. After being told to stay outside by Einar, who has been investigating the mountain mans background, Jannicke watches nervously as he enters the hospital with Ole and Kim accompanying while Daniel is taken away by his mother.

Inside, the officers fail to find the mountain man and decide to pull back from the hospital but are side tracked by Johans radio message revealing the location of Sverres body.

After finding the body, the officers are attacked and all but Ole are killed in the ambush. After watching gunshots within the hospital, Jannicke offers to enter the hospital with officer Johan. However, this plan is interrupted when the mountain man attacks, wounding her and killing officer Johan as well as frightening Camilla.

The remaining two are then saved by a severely wounded Ole who dies of his wounds distracting the mountain man allowing the survivors to escape. Enraged, Jannicke travels to the hotel where the first movie occurred and falls asleep waiting for the mountain man.

The mountain man eventually appears and seems to have the upper hand over Jannicke but is defeated when Camilla arrives. Not satisfied with the mountain man being stabbed through the chest by his own pickaxe, Jannicke ensures the mountain mans death with a shotgun blast to the head.

==Cast==

{| class="wikitable"
|-
! Actor
! Character
|-
| Ingrid Bolsø Berdal
| Jannicke
|-
| Marthe Snorresdotter Rovik
| Camilla
|-
| Kim Wifladt
| Ole
|-
| Fridtjov Såheim
| Herman
|-
| Johanna Mørck
| Audhild
|-
| Mats Eldøen
| Sverre
|-
| Rune Melby
| The Mountain Man
|}

==Production==
The first of the two Cold Prey-movies was extremely well received, and seen by 260,000 – a high number for Norway.    One reviewer even described it as "the best slasher flick" of 2006.      For this reason the expectations on the sequel were high, and Ingrid Bolsø Berdal (whose character was the only surviving member of the original cast   ) emphasised the determination of everyone involved to make good film, not simply cash in on a mediocre sequel.    Though she enjoyed the role, Berdal insisted that this would be the last instalment.   

Norsk Filmfond gave financial support to the project 4 February 2008,    and the new cast was announced 20 February.    Mats Stenberg took over as director after Roar Uthaug, who had directed the first movie.  Filming started in late February.   

The shooting was eventful, with Berdal at one point suffering from food poisoning; having to perform scenes in between throwing up.    There were also lighter moment, with team members hiding in various rooms of the hotel where they were staying, doing their best to scare the others by jumping out wearing masks.  While the outdoor scenes were filmed in the wild scenery of Jotunheimen and the village Otta, the scenes in the hospital were shot in Åslund hospital in Ås, Akershus|Ås. 

==Reception==
Norwegian reviewers were remarkably unanimous in their assessment of the film. All of the three major national newspapers gave it four out of six points. The consensus was that, even though the sequel did not quite live up to the original, it was still enjoyable and not disappointing. Morten Ståle Nilsen, writing for Verdens Gang, commented on the beautiful natural setting, and commended Berdal for managing to look both battered and sexy at the same time.    Dagbladet  s Vegard Larsen declared himself surprised that the sequel turned out as well as it did. Like Nilsen, he compared Berdal to Ellen Ripley, Sigourney Weavers character in the Alien (franchise)|Alien-films.  Per Haddal in Aftenposten concluded that the film failed to scare its audience as much as it intended, but he nevertheless complimented the solid handiwork.   

The movie was seen by 101,564 people on its opening weekend, which was a new Norwegian record for a domestic movie. The previous record – of 70,952 – was set by Mors Elling in 2003.    The box-office success was largely the result of a massive marketing campaign in the weeks leading up to the movies release.   

==Home media==
Shout! Factory announced that Cold Prey 2 will be released on DVD on 23 April 2013. 

==References==
 

==External links==
*  
*  
*  
*   at the Norwegian Film Institute (English)

 
 
 
 
 
 