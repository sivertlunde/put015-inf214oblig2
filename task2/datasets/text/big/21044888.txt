The Story on Page One (film)
 
{{Infobox film
| name           = The Story On Page One
| image          = The Story on Page One FilmPoster.jpeg
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Clifford Odets
| producer       = Jerry Wald
| writer         = Clifford Odets
| starring       = Rita Hayworth Anthony Franciosa
| music          = Elmer Bernstein
| cinematography = James Wong Howe
| editing        = Hugh S. Fowler
| studio         = 
| distributor    = 20th Century Fox
| released       = December 1959 (premiere)  
| runtime        = 123 mins. 118 mins. (FMC Library Print)
| country        = United States
| language       = English
| budget         = $1,748,000 
| gross          =
}}

The Story on Page One is a 1959 American drama film written and directed by Clifford Odets, and starring Rita Hayworth, Anthony Franciosa, and Gig Young. Shot in CinemaScope, the film was distributed by 20th Century Fox. 

==Plot==
After her marriage turns loveless and dull, Josephine "Jo" Morris (Hayworth) finds comfort and company with Larry Ellis (Young), a widower. The two soon begin an affair. After Larry accidentally kills Jos husband during a struggle over a gun, the two are tried for his murder.

Victor Santini is hired to defend Jo by her mother. The lawyer hears her story, of how Mike Morris, her husband, drank and became abusive to her, and how Larrys domineering mother, Mrs. Ellis, resented the affair and demanded that Jo stop seeing her son.

In court, prosecuting attorney Phil Stanley stresses how Jo first told police a prowler had killed her husband until a cufflink belonging to Larry was found at the scene of the crime. Santini uses all his skills to gain a verdict of not guilty, after which, as Jo and Larry are about to leave the courtroom together, Mrs. Ellis calls her son a murderer.

==Cast==
{| class="wikitable" width="60%"
|- bgcolor="#CCCCCC"
! Actor !! Role
|-
| Rita Hayworth || Josephine Brown Morris
|-
| Anthony Franciosa || Victor Santini
|-
| Gig Young || Larry Ellis
|-
| Mildred Dunnock || Mrs. Ellis
|-
| Hugh Griffith || Judge Edgar Neilsen
|-
| Sanford Meisner || Phil Stanley
|-
| Robert Burton || District Attorney Nordeau
|-
| Alfred Ryder || Lt. Mike Morris
|-
| Raymond Greenleaf || Judge Carey
|-
| Katherine Squire || Mrs. Hattie Brown
|-
| Myrna Fahey || Alice
|-
| Leo Penn || Morrie Goetz
|-
| Sheridan Comerate ||  Officer Francis Morris
|-
| Dana Andrews || Det. Lt. Mark McPherson (voice)
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 

 