The Power of Darkness (1924 film)
{{Infobox film
| name           = The Power of Darkness
| image          = 
| image_size     = 
| caption        = 
| director       = Conrad Wiene 
| producer       = 
| writer         = Leo Tolstoy (play)   Robert Wiene
| narrator       = 
| starring       = Petr Sharov   Mariya Germanova   Maria Kryshanovskaya
| music          = Willy Schmidt-Gentner
| editing        = 
| cinematography = Willy Goldberger   Ernst Lüttgens
| studio         = Neumann-Filmproduktion
| distributor    = Bayerische Film
| released       = 16 June 1924
| runtime        = 72 minutes
| country        = Germany Silent  German intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} German silent silent drama film directed by Conrad Wiene and starring Petr Sharov, Mariya Germanova and Maria Kryshanovskaya. It is an adaptation of Leo Tolstoys play The Power of Darkness. 

==Cast==
* Petr Sharov 
* Mariya Germanova - Anisia 
* Maria Kryshanovskaya - Aniuta 
* Alexander Wiruboff - Nikita  Pavel Pavlov - Akim 
* Maria Egorowa - Akulina 
* Sergej Kommissarov   
* Nikolai Massalitinov   Vera Orlova   
* Vera Pawlowa   
* George Seroff

==References==
 

==Bibliography==
* Jung, Uli & Schatzberg, Walter. Beyond Caligari: The Films of Robert Wiene. Berghahn Books, 1999.

==External links==
* 

 
 
 
 
 
 
 
 
 


 
 