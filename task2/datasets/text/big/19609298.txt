A Aa E Ee (2009 Tamil film)
{{Infobox film
| name = A Aa E Ee
| image =
| image_size =
| caption =
| director = Sabapathy Dekshinamurthy|D. Sabapathy
| producer = AVM.K. Shanmugam
| writer = Krishna Vamsi V. Seenivasan Prabhu Navdeep Monica Saranya Livingston
| music = Vijay Antony
| cinematography = Aruldas
| editing = K. Shankar K. Thangavel Kumaran
| distributor = AVM Productions
| released =  
| runtime =
| country = India
| language = Tamil
| budget =
| gross =
}} Monica and Manorama and Tamil film production company, AVM Productions, the film released on 9 January 2009.  The film is a remake of Telugu hit Chandamama (2007 film)|Chandamama with Navdeep enacting the same role.

==Plot==
Subramaniam (Prabhu), an Ayurvedic physician is a much respected man in his village. His only daughter Anita (Monica (actress)|Monica) returns home from Chennai after completing a course in fashion designing. Having lost her mother at a very young age, Anita is close to her father and they share a special bond. Soon Anita’s marriage is fixed with Ilango (Arvind), son of Vedachalam (Haneefa).

While Vedachalam is a good-for-nothing chap, his son is a perfect match. Illango falls in love with Anita. But Anita tells him of her past: her lover Akash (Navdeep) who had jilted her after a one night stand. Illango, though shattered meets Akash and tries to unite him with Anita.

In this attempt, he finds himself falling in love with Anita’s cousin, Eshwari (Saranya Mohan). Now the story is about uniting the two pairs of lovers. Anita does not want to hurt her father with the knowledge of her past.

==Soundtrack==
* "A Aa E Ee" - Rahul Nambiar, Dinesh
* "Dingi Tappu" - Megha, Sheeba, Vinaya, Maya, Ramya, Vijay Antony
* "Kanni Vedi" - Sangeetha Rajeshwaran, Vijay Antony
* "Mena Minuki" - Vijay Antony, Suchitra, Suchitra Raman, Sangeetha Rajeshwaran
* "Natta Nada" - Sangeetha Rajeshwaran, Karthik (singer)|Karthik, Christopher
* "Tappo Tappo" - Bakshi, Sulabha

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 