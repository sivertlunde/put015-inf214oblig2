Hands Across the Sea (film)
 
{{Infobox film
| name           = Hands Across the Sea
| image          = 
| image_size     =
| caption        = 
| director       = Gaston Mervale
| producer       = 
| writer         = 
| based on   = play by Henry Pettitt
| narrator       = Louise Carbasse
| music          =
| cinematography = 
| editing        = 
| studio   = Australian Life Biograph Company
| distributor    = 
| released       = 8 February 1912 (Adelaide)
| runtime        = 3.500 feet 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

Hands Across the Sea is a 1912 Australian silent film directed by Gaston Mervale starring Louise Lovely.

==Plot==
Jack Dudley, an English farmer, is married to Lilian, who is desired by the evil Robert Stilwood. While Jack and Lilian are honeymooning in Paris, Stilwood frames Dudley for murder and he is sentenced to imprisonment in a French penal colony in New Caledonia. He escapes and is recused by a British man-o-war. 

==Production==
The film was based on a popular English play by Henry Pettitt. 

==Release==
An American film with the same title was released in Australia around the same time. 

==References==
 

==External links==
* 

 
 
 
 
 


 