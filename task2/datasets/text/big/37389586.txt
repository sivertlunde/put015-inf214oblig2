The Delta (film)
{{Infobox film
|name=The Delta
|image=The Delta film poster.jpg
|caption=Theatrical release poster
|country= USA
|released=  
|runtime= 85 min   
|director= Ira Sachs
|screenplay= Ira Sachs, Mauricio Zacharias
|distributor= Strand Releasing 
}} American dramatic LGBT film which premiered at the Toronto International Film Festival on September 9, 1996.  The 85 minute film was shot with 16mm film.  The film was directed by Ira Sachs.

It won the "Outstanding Emerging Talent" award at Outfest in 1997,    and was also nominated for the "Producers Award" (for producer Margot Bridger) at the 1997 and 1998 Independent Spirit Awards. 

==Plot==
The Delta tells the story of 18-year-old Lincoln Bloom (Shayne Gray), who, after leading a relatively straight teenage life, is slowly drawn into the LGBT world and discovers he is Homosexual|gay. After visiting various shady establishments (including gay bars, video arcades, etc.) and engaging in sexual acts with "men he didnt know," Lincoln meets Minh Nguyen (Thang Chan), a Vietnamese immigrant, and travels down river with him in a cabin cruiser. In a doomed relationship, the two bond with each other, Lincoln neglecting to tell his girlfriend and leading two parallel lives. At the end of the film, Minh and Lincoln take a boat out on the Mississippi River to set off fireworks and this is where the film dramatically concludes.

The movie was filmed entirely in Memphis, Tennessee, Sachss hometown,  and includes scenes of Memphis locations.  

==Cast==
* Shayne Gray as Lincoln Bloom
* Thang Chan as Minh Nguyen (John)
* Rachel Zan Huss as Monica Rachel
* Colonious David as Ricky Little
* Charles J. Ingram as David Bloom

==Reception==
The film received mostly positive reviews, with IndieWire calling it the "essence of independent film."  Los Angeles Timess Kevin Thomas called it "...so evocative that it is poetic in its impact."  However, it was also criticized by The New York Times    and others for leaving viewers with too many questions and moving too abruptly from plotline to plotline.

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 