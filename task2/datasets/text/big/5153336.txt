Far Side of the Moon (film)
 
{{Infobox film
| name = Far Side of the Moon
| image = tlareleasingfarsideofthemoon.jpg
| caption =
| director = Robert Lepage
| producer = Bob Krupinski Mario St-Laurent
| writer = Robert Lepage
| narrator =
| starring = Robert Lepage Anne-Marie Cadieux Marco Poulin Céline Bonnier Lorraine Côté
| music = Benoît Jutras
| cinematography = Ronald Plante
| editing = Philippe Gagnon
| distributor = TLA Releasing
| released =  
| runtime = 105 mins
| country = Canada French
| budget =
}}

Far Side of the Moon, in original French language|French, La face cachée de la lune, is a 2003 film by Robert Lepage. The film is based on the play by the same name by Lepage and Adam Nashman. The film is set in the context of the Soviet Union|USSR-United States Space Race of the 1960s. The leading characters are the brothers André and Philippe, both played by Robert Lepage.  The film was Canada|Canadas official nomination for the Academy Award for Best Foreign Language Film|foreign-language Oscar.

==Awards and Nominations==
*2004: International Press Award (FIPRESCI) to Robert Lepage at Berlin International Film Festival
*2004: Genie Awards for "Best Screen Adaptation" to Robert Lepage 
*2004: Jutra Award for Best Make-up to Brigitte Bilodeau
*2004 : Award for "Best French language film" at Festival international francophone de Namur
;Nominations
*2004: Annual Award at Valladolid International Film Festival to Robert Lepage
*2004: Genie Award for Best Director to Robert Lepage
*2004: Genie Award for Best Film to Bob Krupinski and Mario St-Laurent
*2004: Genie Award for Best Actor in a Main Role to Robert Lepage

==External links==
* 
* 

 
 
 
 
 
 


 
 