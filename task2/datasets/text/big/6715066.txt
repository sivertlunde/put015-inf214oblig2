Last Embrace
 
 
{{Infobox film
| name           = Last Embrace
| image          = Last embrace.jpg
| image_size     =
| caption        = Movie poster
| director       = Jonathan Demme Michael Taylor Dan Wigutow
| writer         = Murray Teigh Bloom (novel) David Shaber (screenplay)
| narrator       =
| starring       = Roy Scheider Janet Margolin
| music          = Miklós Rózsa
| cinematography = Tak Fujimoto
| editing        = Barry Malkin
| distributor    = United Artists
| released       =  
| runtime        = 102 mins
| country        = United States English
| budget         =
| gross          =  $1,537,125 
}} thriller film directed by Jonathan Demme. Based on the novel The 13th Man by Murray Teigh Bloom, it stars Roy Scheider, Janet Margolin and Christopher Walken, telling the story of a woman who takes the role of the biblical avenger Goel Hadam, killing the descendants of the Lower East Side Zwi Migdal who enslaved her grandmother.

==Plot summary==
The film opens in a Mexican cantina across the border from El Paso, where government agent Harry Hannan is canoodling with his wife. Harry spots an informant that he was supposed to meet in a few days. Realizing he is about to be attacked, he shoves his wife to the ground and starts shooting at the informants companions who return fire and flee the restaurant. Harrys wife dies in the attack, and he suffers a nervous breakdown. He spends 161 days in a Connecticut sanitarium before being released.

On his way back to New York City, Harry stumbles and nearly falls into the path of an express train. He goes to the makeup counter at Macys to retrieve his next assignment, but the assignment slip inside the lipstick case he is given is blank. He accosts his contact who assures him that the agency probably just does not have any work for him at the moment.

When Harry returns to his apartment, he finds it is occupied by a doctoral student named Ellie Fabian. She explains that she had a perfectly legal sublet arranged while she is in the last semester of her studies at Princeton University|Princeton. Ellie claims that the housing office said the Hannans would be gone indefinitely. She gives Harry a note that was slipped under the door, but it contains only a few Hebrew characters that Harry cannot read.

Paranoid that he is being targeted by his own agency, Harry visits his supervisor Eckart, who assures Harry that the agency has higher priorities. Eckart insists that Harry is not ready to return to the field, but that he is perfectly safe. After their conversation, Harry notices that he is being surveilled. He loses the tail and rushes to the American Museum of Natural History, where Ellie is carbon dating a skull. She is astonished that he found her, but he rattles off facts about her life, including that she is conducting a study of prostitution.

He gleaned everything from his brief encounter with Ellie in his apartment. He gives her some money and urges her to stay in a hotel, because he fears she will be accidentally targeted by whomever is after him. Ellie stays in the apartment despite Harrys request. When Harry wakes from a nightmare, he tells Ellie about the death of his wife. He takes a prescription pill, but immediately spits it out, realizing that it is cyanide.

Harry takes the Hebrew note to a local rabbi who can only partially decode it. The rabbi informs the agency that Harry has visited him, and Eckhart orders Harrys murder. Ellie suggests that they take it to her friend at Princeton who specializes in Hebrew studies. On the train, Ellie tells Harry about her grandmother, and confesses that she inherited her wild streak. Harry notices an old man looking at him, and the camera reveals that another agent is also on the train.

At Princeton, Ellie introduces Harry to Richard Peabody, who is very possessive of Ellie. He decodes the note for Harry and explains that it means " ." Peabody has accumulated several of the notes over the years, and they were all attached to very peculiar murders. Harry is the first one to have received the note and lived.

The next day, Harry is lured to a trap by the other agent. He manages to kill the agent during a shootout in a bell tower. There, he encounters the old man from the train, Sam Urdeil. Sam explains that he is part of a committee investigating the blood murders. Together, they investigate the various clues, and they piece together that Harrys grandfather owned a brothel on the Lower East Side of Manhattan.
 white slavery like her grandmother. He drives her up to Niagara Falls, where they have an emotional confrontation. She tries to kill him but confesses that she loves him. He is conflicted, but he tells her that he will turn her in. Ellie runs from him, and the film ends with a prolonged chase through the hydroelectric power plant. It ends above the falls, where Harry grabs Ellie, but she struggles and ends up plunging to her death.

==Cast==
* Roy Scheider as Harry Hannan
* Janet Margolin as Ellie Fabian
* Christopher Walken as Eckart
* Sam Levene as Sam Urdeil John Glover as Richard Peabody
* Marcia Rodd as Adrian Charles Napier as Quittle
* David Margulies as Rabbi Drexel

==Reaction==

Vincent Canby in a May 4, 1979 New York Times review of Last Embrace wrote of Scheider: "No other leading actor can create so much tension out of such modest material."

==References==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 