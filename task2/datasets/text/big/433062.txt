Broken (1993 film)
{{Infobox film
| name = Broken
| image = brokenmovie.jpg
| caption = Unofficial DVD cover included with the DVD-version leak
| director = Peter Christopherson
| producer = Trent Reznor
| writer = Trent Reznor
| starring = Trent Reznor Bob Flanagan
| music = Trent Reznor
| cinematography =
| editing =
| distributor =
| released = 1993, 2006 (bootlegged/leaked)
| runtime = 20:10
| country = United States English
| budget =
| gross =
| preceded by =
| followed by =
}} horror Musical musical short EP Broken framing sequence, concluding with an otherwise unreleased video for the EPs final song "Gave Up," setting the conclusion of the films frame story to the song.  Due to its extremely graphic content, the Broken movie was never officially released, but was leaked as a bootleg which became heavily traded on VHS in the 1990s, and more recently via the Internet.
 Happiness in Slavery look like a Disney movie."  While his comments about the movie have been cryptic at best, he makes no secret of the films existence.

==Synopsis==

===Plot===
 
The film begins with a scene of a person being executed by hanging. The trap door opens and the person drops with a maniacal smile on his face.
 cleave gagged, while being forced to watch a television. The first video, "Pinion," begins to play.

 
The music video for "Pinion" begins in a bathroom. The camera zooms in on a toilet flushing, and a network of pipes is shown leading to a contraption with a large gear system and a pressure gauge on it. As the camera zooms out, a tight, plastic bodybag-like suit suspended in a padded cell with six rods by the side is shown, with the end of the pipes attached to the mouth portion with water gushing in, presumably to drown the person inside. An alternative interpretation that the person bound inside is being fed the "waste of the world" coming through the pipes.   

Cutting back to the amateur video, the killer, wearing some sort of leather mask, drags the victims head back and forces him to drink some sort of liquid from a jerrycan. The middle of the video for "Wish" is interrupted by amateur footage again with the victim chained to a table with a large wad of dark substance on his face. As the killer is immediately followed to be seen putting his pants on, it could be assumed that it is feces. After the video ends, the killer repeatedly rewinds to the part of the video where Reznor screams "fist fuck" and begins to rub his fist.

The video for "Help Me I Am in Hell" shows a middle-age bald man in a room filled with flies. He ignores them while eating some sort of steak and drinking wine. On one occasion it is clearly shown that flies also enter his mouth when he eats. The video cuts away several times to show the same man in bondage gear.
 identify the source of the leak.

  near the far wall]]
Following the video, the victim is shown still tied to a table as the killer proceeds to rip his teeth out. The video for "Happiness in Slavery" shows up, which was banned by music video channels worldwide.  It features a man (played by performance artist Bob Flanagan) stripping off his clothes and settling down on a machine that tortures, rapes and later kills him.

The film ends with a video for "Gave Up" - this is different from the one on Closure (Nine Inch Nails VHS)|Closure, as it is not an actual Nine Inch Nails video, but the music of NIN dubbed over the storyline of the movie. At this point, the victim is suspended from the ceiling, and is repeatedly attacked by the perpetrator first with a blade, then a blowtorch, after which the killer slices off the victims penis with a straight razor. The camera-work here closely resembles that of an amateur snuff film, while there is interspersed footage of the police searching through the basement, and finding remains of previous victims. (At one point, a sign saying "TRESPASSERS WILL BE EATEN" is shown.) Finally, the film cuts back to the victim strapped on a table, as the murderer hacks his limbs off with a chainsaw, proceeds to presumably rape him, and finally slices his chest open to eat his heart.

The movie then cuts back to the execution scene, showing as the killer is dropped through the trap door, with a crazed smile on his face, falling through a seemingly immensely long tunnel, until the rope suddenly tightens. The movie ends with the inverted version of the Broken album cover, with the background black, and a mirrored "n" character filled with the original orange background texture. The severed head of the killer flying across the screen is also shown after 30 seconds of no audio and a black screen.

In versions of the tape labeled as "Techno-necrophilia", the movie is followed by a segment of Robert "Budd" Dwyers televised suicide. It does not appear on the "leaked" DVD version - it is possible that it was merely added by a third party while the tape was circulating.

==Cast==
Much of the cast, aside from   and Serge Becker, while "Happiness in Slavery" is credited to Jon Reiss. 

==Availability== so he MPEG and AVI formats and distributed extensively through peer-to-peer networks and Nine Inch Nails fan websites. These are generally not of the highest quality, as they are not first-generation copies.

The music videos (without the interstitial footage between songs) for "Pinion", "Wish", "Help Me I Am in Hell", and "Happiness in Slavery" were made officially available on Closure and on the official Nine Inch Nails website.   

On Fixed (EP)|Fixed (the remix CD for the Broken EP), remixers employed a heavily processed sample of Flanagans screaming from the "Happiness in Slavery" video as part of the rhythm in the tracks "Screaming Slave" and a remix of the "Happiness in Slavery" song (although the sample is very obscure on the latter).

On the Collected (Nine Inch Nails DVD)|Collected DVD that was sent out in promotion of the album With Teeth, portions of the movie are shown for a few seconds in crystal-clear quality.
 BitTorrent as a remastered DVD image. The remaster, titled "Broken 2.0," was primarily sourced from a low-generation copy of the tape that was anonymously sent to a long-time member of the online NIN community; however, much of the music video content was replaced with higher-resolution footage sourced from the VHS release of Closure (video)|Closure. Like all of the copies that existed online prior to that point, this version does not have video footage during "Help Me I Am in Hell", but a blank screen instead. Menu screens, a chapter index and the option to listen to the audio tracks from the CD (without added sound effects) are all distinct features of "Broken 2.0."

On December 30, 2006, an unofficial version of the film was released on a DVD disc image and distributed via BitTorrent at The Pirate Bay by the same anonymous user called "seed0" who uploaded the leaked DVD version of Closure. The DVD image represents a significant upgrade in visual and audio quality from "Broken 2.0.", and includes the oft-missing video for "Help Me I Am in Hell." Fans have speculated that this version of the film has been sourced directly from the master tapes, and that Reznor himself may have been the source of this leak along with the Closure DVD leak, as implied by a post on his official blog: "12/21/06 : Happy Holidays!  This one is a guilt-free download. (shhhh - I didnt say that out loud). If you know what Im talking about, cool." {{cite web
| url = http://www.nin-thespiral.com
| title = The Spiral
| publisher = Nine Inch Nails
| format = registration required
| accessdate = 2006-12-21}} 

On May 6, 2013, the entire video was made available for streaming on Vimeo via Nine Inch Nails Tumblr account. It was removed by Vimeo almost immediately, and the Tumblr post was updated with an allusion to The Pirate Bay.  In July 2014, the full video returned to Vimeo, and is available for viewing in HD.  

== Footnotes ==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 