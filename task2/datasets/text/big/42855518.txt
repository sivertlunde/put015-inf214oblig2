One Small Hitch
 
 
{{Infobox film
| name           = One Small Hitch
| director       = John Burgess
| producer       = {{plainlist|
* John Burgess
* Brett Henenberg
}}
| writer         = Dode B. Levenson
| cinematography = Tari Segal
| editing        = Ryan Koscielniak
| distributor    = Freestyle Releasing
| released       =  
| runtime        = 105 minutes
| starring       = {{plainlist|
* Shane McRae
* Aubrey Dollar
* Daniel J. Travanti
* Janet Ulrich Brooks
* Ron Dean
* Mary Jo Faraci
* Robert Belushi
* Rebecca Spence
* Heidi Johnanningmeier
}}
| country        = United States
| language       = English
}}
One Small Hitch is a 2012 romantic comedy film directed by John Burgess and written by Dode B. Levenson. Shane McRae stars as Josh Shiffman, who learns that his father is dying and his only regret is that he will not get to meet the woman who will one day become his sons wife. Desperate to fulfill his dads final wish, Josh claims that he already met that woman, Molly Mahoney, who is played by Aubrey Dollar.

== Plot ==
Molly Mahoney flies home to Chicago for her mothers wedding and brings a "mystery man" with her, but when she discovers this Mystery Man has a wife, she dumps him at the curbside check-in. She ends up crying on the shoulder of childhood friend Josh Shiffman, who is flying home for the same wedding. Emotionally drained, Molly passes out in Joshs arms just as he gets a phone call from his parents. Josh learns that his father is dying and his only regret is that he will not get to meet the woman who will one day become his sons wife.

Desperate to fulfill his dads final wish, Josh claims he has already met that woman, close family friend Molly Mahoney. When Josh sheepishly confesses his ruse in mid-flight, Molly is furious and makes him promise to right the wrong the moment they land. Only when they arrive, its too late. Word has leaked out, and they are swarmed at the airport by both families with well wishes and congratulatory banners. Mollys family is delighted that her "mystery man" turns out to be Josh, and Joshs dad is thrilled that his playboy son is finally settling down with a nice girl theyve known all their lives.

Josh convinces Molly to play fiancée, but as they plan their phony wedding, the two of them start to fall in love for real. This turns their dating lives, and familys interactions upside down.

== Cast ==
* Shane McRae as Josh Shiffman
* Aubrey Dollar as Molly Mahoney
* Daniel J. Travanti as Max Shiffman
* Janet Ulrich Brooks as Frida Shiffman
* Ron Dean as Art Burke
* Mary Jo Faraci as Doreen Mahoney
* Robert Belushi as Sean Mahoney
* Rebecca Spence as Carla Mahoney
* Heidi Johanningmeier as Giselle Brousard

==Production==
 
Filming took place in Chicago, Illinois, and Los Angeles, California.  As a Chicago native, director John Burgess, having attended film school at USC, wanted to go back to his hometown to make his first feature film.  To put together the cast, he called upon two veteran casting directors: Monika Mikkelsen in Los Angeles, and Claire Simon in Chicago.  To keep costs down, the production cast locally in both cities except for the lead roles of Josh and Molly.  Mikkelsen had cast Shane McRae in previous projects and pitched him to play Josh Shiffman.  Reels had been sent from various talent agencies and John Burgess came across Aubrey Dollar for Molly Mahoney.  Both actors lived in New York and were flown to Chicago and Los Angeles for filming.

The production wanted an established actor with a familiar face to anchor the story and play the dying father.  Daniel J. Travanti, a local Chicagoan who had received Emmy and Golden Globes awards, was offered and accepted the role.  Janet Ulrich Brooks was then cast as Joshs mom, Frida Shiffman, and Mary Jo Faraci as Mollys mom, Doreen Mahoney, who with their matching red hair could easily be mistaken for the real life mother of Aubrey.  Ron Dean, a staple in the Chicago acting community from such films as The Breakfast Club, The Fugitive, and The Dark Knight, accepted the role of Art Burke, Mollys step dad to be, and the parents were all cast.  Robert Belushi, well known in Chicagos theater and sketch comedy scene at Second City and Improv Olympic, was cast to play the best friend and over-protective brother, Sean Mahoney, while Rebecca Spence, another staple in the Chicago acting community was subsequently cast as his wife, Molly’s sister-in-law, Carla Mahoney.  Lastly, to round out the main cast a femme fatale was needed to throw a monkey wrench into Josh and Molly getting together too quickly.  A local actress, Heidi Johanningmeier, who actually first came in to read for the role of Molly, was instead offered the role of Giselle Brousard.

The department heads were filled with local stars from Chicago’s independent filmmaking scene.  Tari Segal was hired to shoot the film.  She had just wrapped shooting Phedon Popamichaels feature film, Lost Angeles, whom she had interned for a few years earlier on the film he was shooting, Sideways, while she was still attending Columbia College Chicago.  Adri Siriwatt was hired as the production designer.  Adri came in for the interview covered in paint from a production that she was working on and the producers could tell she wasnt afraid to do whatever it took to get the job done, even if it meant getting her hands dirty.  Aly Barohn was hired as the costume designer.  The producers first meeting with Aly was very similar: she had a quirky style and impressive knowledge of current fashion trends that were going on in Venice Beach and Hollywood out on the West Coast, all the way to the Midwest styles that were dominating every age group in the city of Chicago.

Principal photography began in Chicago the last week in October, and many of the crew dressed up in costume on Halloween while filming overnight at OHare International Airport.  The Windy City portion of the shoot was wrapped up just before Thanksgiving, and then the production was off to Los Angeles for scenes in Hollywood, Venice Beach, and Burbank.  When shooting was wrapped in California, John Burgess headed back to Chicago to begin post-production.  Ryan Koscielniak, the editor of Johns USC graduate thesis film, The Powder Puff Principle, had set up shop in Evanston, IL.  The production couldnt afford to hire Ryan full time, so they started meeting on nights and weekends.  The process took longer, but it enabled the production to keep costs down to save what money was left for color timing, sound mixing, and music licensing.  The production utilized Illinois 30% film tax credit for finishing funds and completed all post-production activities locally at the famed Chicago Recording Company and Filmworkers Club.

== Release ==
One Small Hitch premiered at the California Independent Film Festival on November 8, 2012. 

It will be released theatrically in the United States on February 6, 2015. 

== Reception ==
The Radio Times rated it 2/5 stars and called it a "by-the-numbers romantic comedy". 

=== Awards ===
 
* Best Picture Comedy – California Independent Film Festival (2012) 
* Best Production Design – Hollywood Reel Independent Film Festival (2012) 
* Best Picture Comedy – Cinequest (2013) 
* Best Picture Comedy – Sedona International Film Festival (2013) 
* Best Feature Film – Omaha Film Festival (2013) 
* Audience Award – Bahamas International Film Festival (2013)
* Best Ensemble Cast – Chicago Comedy Film Festival (2013)
* Best Screenplay – L.A. Comedy Film Festival (2013) 
* Best Director – Laugh Or Die Film Festival (2013) 
* Best Director – Stony Brook Film Festival (2013) 
* Rising Star – Naples International Film Festival (2013) 
* Award of Merit – The Indie Fest (2014) 
* Audience Award – Durango Film Festival (2014) 
* Special Jury Commendation – Durango Film Festival (2014) 
* Award of Excellence – Accolade Competition (2014) 

== Soundtrack ==
#"White Dress" – Ben Rector
#"Cold Shoulders" – Gold Motel
#"Spirit Of Waste" – Goodbye Satellite
#"Little Horn" – Suns 
#"The Joker" – Ives The Band
#"Hearts Dont Beat Right" – New Cassettes
#"Take It Easy" – Francis 
#"Hymn 101" – Joe Pug
#"421" – The Wildbirds
#"Hiding – This Is Me Smiling
#"Falling Apart" – The Sleeptalkers
#"Beam Me Up" – Go Back To The Zoo
#"Puppet" – Brian Lee
#"Stay" – Dot Dot Dot
#"Chupacabra" – Flatbed Orange
#"Now The Rabbit Has The Gun" – Now The Rabbit Has The Gun
#"For Your Love" – Marching Band
#"Dope Fiend" – Jaime Wyatt
#"Right Or Reason" – The Blissters
#"Closer" – Sabrosa Purr
#"Both Young & Wild" – Aktar Aktar Capital Cities
#"Poison & Wine" – The Civil Wars 
#"One Day" – Erin Martin
#"Terrified" – Kevin Andrew Prchal
#"Stockholm" – Brian McSweeney
#"Jag Alskar Dig" – Volcanoes Make Islands
#"Under Your Wings Ill Hide" – Immanu El
#"This Is For You" – David Dunn

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 


 