Crime on Their Hands
{{Infobox Film |
  | name           = Crime on Their Hands |
  | image          = CrimeonTheirHandsTITLE.jpg|
  | caption        = |
  | director       = Edward Bernds
  | writer         = Elwood Ullman| Kenneth MacDonald Charles C. Wilson Cy Schindell
  | cinematography = Henry Freulich | 
  | editing        = Henry DeMond |
  | producer       = Hugh McCollum |
  | distributor    = Columbia Pictures |
  | released       = December 9, 1948 |
  | runtime        = 17 44"
  | country        = United States
  | language       = English
}}
Crime on Their Hands, released in 1948, is the 112th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot== Kenneth MacDonald). With dreams of becoming genuine reporters, the trio head for Squid McGuffys cafe asking for the whereabouts of Dapper. They manage to convince everyone at the restaurant that they are actually police.

While searching several rooms above the cafe, the Stooges stumble on Dappers moll (Christine McIntyre), who hastily hides the Punjab diamond in a candy dish. The boys refuse to leave, suspecting Dapper will eventually show his face. While killing time, Shemp starts to flirt with the moll, and manages to swallow the ice along with some mints from the candy dish. The gal nearly has a nervous breakdown but quickly discovers the Stooges are nothing more than reporters. She calls in Dapper and his henchman Muscles (Cy Schindell) and frantically try to pry the diamond out of frazzled Shemp.

After all else fails, Dapper decides to cut him open. Moe and Larry are locked in a closet by Muscles while Shemp is tied down on a close by desk-turned-operating table. As luck would have it, there happen to be a bag of tools in the closet, which Moe and Larry use to saw their way out of the closet, and right into a gorillas cage on the other side of the wall. The gorilla knocks Moe, Larry, Dapper, and Muscles cold. The beast, however, befriends Shemp, and helps him cough up the diamond.

==Production notes== Hot Ice.  It is a partial remake of the 1942 Andy Clyde short All Work and No Pay. 
 jungle rot while on Guadalcanal, which eventually developed into terminal cancer. Knowing he was dying, Schindell worked constantly during his illness to assure his family would be financially secure after his death. Excessive makeup was used during the filming of Crime on Their Hands to mask Schindells cancer-ridden face.   

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 
 