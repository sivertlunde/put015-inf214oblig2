Darna, Kuno?
{{Infobox film
| name           = Darna, Kuno?
| image          = Darna Kuno Poster 1979.jpg
| caption        = Theatrical poster
| director       = Luciano B. Carlos
| producer       = Lily Monteverde
| writer         = Toto Belano
| narrator       = 
| starring       = Dolphy   Lotis Key   Brenda Del Rio
| music          = Ernani Cuenco  Levi Celerio
| cinematography = Claro Gonzales   Ricardo Jacinto 
| editing        = Rogelio Salvador  
| distributor    = Regal Films, Inc.
| released       =  
| runtime        = 
| country        = Philippines
| language       = Tagalog
| budget         = 
| gross          = 
}} spoof of comic book character and female superhero Darna.  The role of Darna Kuno was played by Filipino actor, Dolphy.   The film was released on 30 March 1979.   

==Plot==
Dolphy plays Dondoy, a humble tricycle driver who was the ardent admirer of Annabel (Key). At first, Dondoy was often ridiculed by Annabels sister Estelga (Delgado) and her mother (de Villa), who later became kind as the story progresses. The real Darna (Brenda Del Rio) in the Darna, Kuno? storyline became pregnant.  Based on the story, Darna was impregnated by the Japanese anime robots Voltes V and Mazinger Z. Because of the pregnancy someone had to take Darna’s place temporarily as the superheroine until she had given birth.  The original Darna lent her magical stone (the stone that transforms Narda, Darnas alter-ego, into Darna when swallowed and the name Darna is shouted aloud) to Dolphys character Dondoy who will become the male Darna dressed up in the female Darna costume.  Dolphy as Darna or more specifically the Darna Pretender or replacement had to fight tikbalangs, aswangs, and other enemies. Annabel became the other Darna Kuno in the story, as she stole the magical stone from Dolphy after finding out Darna Kuno’s secret identity.  Both male and female Darna Pretenders fought and defeated alien invaders.  The true Darna returned to retrieve her magical stone carrying her baby who was already wearing a Darna costume.   

==Cast==
*Dolphy as Dondoy/Darna Kuno 1
*Lotis Key as Anabelle/Darna Kuno 2
*Marissa Delgado as Estelga
*Tita de Villa as Anabel and Estelgas mother
*Romy Nario
*Tonio Gutierrez
*Dijay Dadivas
*Karlo Vero

===Special Guest Appearances===
*Brenda del Rio as the real Darna San Pedro
*Sandy Garcia
*Christopher de Leon as Chris, Anabelles cousin
*Bella Flores as the chief manananggal
*Celia Rodriguez as the organ player who stole the brides
*Charo Valdez
*Rio Locsin
*Lily Miraflor
*Ruel Vernal as the spaceship alien
*Bata Batuta Characters
**Alice Kamatis
**Kardong Kayod
**Tak-talaok

==See also==
*Zsazsa Zaturnnah

==References==
 

==External links==
*  poster

 
 
 
 
 
 
 
 