Je vous trouve très beau
 
{{Infobox film
| name           = You Are So Beautiful 
| image          = 
| caption        = 
| director       = Isabelle Mergault
| producer       = Jean-Louis Livi
| starring       = Michel Blanc Medeea Marinescu Wladimir Yordanoff Éva Darlan Valérie Bonneton
| screenplay     = Isabelle Mergault
| music          = Bob Lenox Alain Wisniak
| cinematography = Laurent Fleutot
| editing        = Colette Beltran Marie-Josèphe Yoyotte
| distributor    = Gaumont Film Company
| released       =     
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = $6,000,000
| gross          = $51,475,263  
}} French comedy 2006 by the Gaumont company. It is written and directed by Isabelle Mergault and includes a cast of Michel Blanc, Medeea Marinescu, Wladimir Yordanoff, Benoît Turjman, Éva Darlan and Elisabeth Commelin.

==Plot==
Aymé played by Blanc, a recently widowed farmer, is eager to find a new wife to help him run his farm.  Desperate, he seeks the aid of a local matchmaker who suggest that he go to Romania to find a new wife. There he meets Elena played by Marinescu.

==Cast==
* Michel Blanc as Aymé Pigrenet
* Medeea Marinescu as Elena
* Wladimir Yordanoff as Roland Blanchot
* Benoît Turjman as Antoine
* Éva Darlan as Mme Marais
* Elisabeth Commelin as Françoise
* Valérie Bonneton as Maître Labaume
* Julien Cafaro as Thierry
* Valentin Traversi as Jean-Paul
* Raphaël Dufour as Nicolas

==Release==
The film premiered at the "Sarlat Film Festival" on 7 November 2005. It was also screened at the Marrakech International Film Festival on 13 November 2005. On 14 June 2006, the film was projected at the Seattle International Film Festival. 

==Accolades==
{| class="wikitable" style="width:99%;"
|-
! Year !! Award !! Category !! Recipient !! Result
|-
|rowspan="5"| 2007
|rowspan="3"| César Awards Best Original Screenplay
| Isabelle Mergault
|  
|- Best Actor
| Michel Blanc
|  
|- Best First Feature Film
| Isabelle Mergault
|  
|-
| Lumières Award Best Actor
| Michel Blanc
|  
|-
| Love is Folly International Film Festival
| Best Actor
| Michel Blanc
|  
|}

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 


 
 