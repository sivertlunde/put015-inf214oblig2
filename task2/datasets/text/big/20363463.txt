Virgil (film)
{{Infobox film
| name           = Virgil
| image          = Mabrouk-el-mechri-virgil.jpg
| caption        = Theatrical release poster
| writer         = Mabrouk El Mechri
| starring       = Jalil Lespert   Léa Drucker   Jean-Pierre Cassel   Philippe Nahon   Patrick Floersheim   Karim Belkhadra   Sami Zitouni   Jean-Marie Frin   Tomer Sisley
| director       = Mabrouk El Mechri
| producer       = Sidonie Dumas
| music          = Frédéric Verrières
| cinematography = Mabrouk El Mechri
| editing        =
| distributor    = Gaumont Columbia Tristar Films
| released       =  
| runtime        = 93 minutes
| country        = France
| language       = French
| budget         =
| gross          =
}}
 2005 comedy first feature film. It had its first public screening in movie theaters starting September 2005. The film is about the life of a French boxer in the French suburbs.

==Synopsis==
Every week, Virgil dreams of his father Ernest telling him about his earlier life as a boxer.
And every week he dreams of a crossing glance from a young woman named Margot. This week, his father announces that he will finally come to see his son fight on the boxing ring. One problem: Virgil has not been boxing for the last three years.

==Cast==
* Jalil Lespert as Virgil
* Léa Drucker as Margot
* Jean-Pierre Cassel as Ernest
* Philippe Nahon as Louis
* Patrick Floersheim as Dunlopillo
* Karim Belkhadra as Sid
* Sami Zitouni as Kader
* Jean-Marie Frin as Mario Taliori
* Tomer Sisley as Dino Taliori
* Jean-Luc Abel as Marcel
* Michel Trillot as character in the shower
* Marc Duret as character in the kitchen
* Philippe Manesse as character at the hospital
* Antoine Chain as character at the prisoners visiting hall
* Ouassini Embarek as Greek client
* Nasser Zerkoune as Ernest at 22 years

==External links==
* 

 
 
 
 

 

 
 