El Monstruo resucitado
{{multiple issues|
 
 
}}

{{Infobox film
| name           =El Monstruo Resucitado
| image          = 
| image size     =
| caption        =
| director       =Chano Urueta
| producer       = Sergio Kogan	Abel Salazar
| writer         =  Arduino Maiuri (story), Chano Urueta
| narrator       = Miroslava Carlos Navarro José María Linares-Rivas Fernando Wagner Alberto Mariscal Stefan Berne
| music          = Raúl Lavista
| cinematography = Víctor Herrera
| editing        = Jorge Bustos	
| studio         = Internacional Cinematográfica
| distributor    = Azteca Films Inc.
| released       = 1953
| runtime        = 85 min
| country        = Mexico Spanish
| budget         =
| gross          =
| preceded by    =
| followed by    =
}}

El Monstruo Resucitado ("The Revived Monster") is a 1953 Mexican horror film directed by Chano Urueta.

==Plot==
 

==Cast== Miroslava	 ...	Nora
* 	 Carlos Navarro	 ...	Ariel / Serguei Rostov
* 	 José María Linares-Rivas	 ...	Hermann Ling
* 	 Fernando Wagner	 ...	Gherásimos
* 	 Alberto Mariscal	 ...	Mischa
* 	 Stefan Berne	 ...	Crommer

==Production==
  Frankenstein and horror and Fantasy films in Mexican Cinema, El Monstruo being one of the many films that were spawned by Ladróns and El Vampiros critical and financial success.      

==Release==
 

==Reception==
 
The film received mixed to positive reviews upon its release, it currently has a 5.5/10 on IMDb.  It has been considered by some to be one of the best horror films in Mexican Cinema  with some critics praising its atmosphere   
Glenn Erickson of DVD Talk.com gave the film a positive review  stating that the "camera direction kept pace with the theatrical delirium of the performances by evoking the expressionist angles and lighting of Universal films". Erickson also praised the films cinematography, atmosphere, art direction, and designs. 

==Legacy==
 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 
 


 