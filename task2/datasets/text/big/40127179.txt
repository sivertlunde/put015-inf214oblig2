The Dark Wind (1991 film)
{{Infobox film
| name = The Dark Wind
| image = DarkWindFilm.jpg
| caption = Theatrical release poster
| director = Errol Morris
| producer = Robert Redford Patrick Markey
| writer = Eric Bergren Neal Jimenez
| starring = Lou Diamond Phillips Fred Ward John Karlen Gary Farmer
| music = Michel Colombier
| cinematography = Stefan Czapsky
| editing = Freeman Davies Susan Crutcher Le Studio Canal+ Seven Arts Pictures (through New Line Cinema) Artisan Entertainment
| released =  
| runtime = 111 min.
| country = United States English Navajo Navajo Hopi Hopi
| budget =
| gross =
}}
The Dark Wind is a 1991 theatrical mystery film based on The Dark Wind by Tony Hillerman, one of a series of mysteries set against contemporary Navajo life in the Southwest. It stars Lou Diamond Phillips as Jim Chee and Fred Ward as Joe Leaphorn.

==Synopsis==
As Officer Jim Chee watches a windmill, trying to catch the vandal repeatedly sabotaging it, a small plane crashes nearby.  Thus begins a tangled story involving not only the vandalism and the crash, but murder, drug smuggling, and burglary.  Officer Chee is suspected by the FBI when drugs known to have been on the plane are missing. 

==References==
 


 
 
 
 

 