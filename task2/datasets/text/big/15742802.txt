Children of Hiroshima
{{Infobox film
| film name =  
| name           = Children of Hiroshima
| image          = Children of Hiroshima 1952 film.jpg
| caption        = Film poster
| director       = Kaneto Shindo
| producer       = 
| writer         = Kaneto Shindo
| starring       = Nobuko Otowa
| music          = Akira Ifukube
| cinematography = Takeo Itō
| editing        = Zenju Imaizumi
| distributor    = 
| released       =  
| runtime        = 97 Minutes 
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
}}
 , (lit. Children of the Atomic Bomb) also released as Atom-Bombed Children in Hiroshima,  is a 1952 Japanese film directed by Kaneto Shindo. It was entered into the 1953 Cannes Film Festival.   

==Plot==
Takako (Nobuko Otowa) is a teacher on an island in the inland sea off the coast of Hiroshima after world war II. During her summer holiday, she goes back to Hiroshima to visit the graves of her parents and younger sister, who were killed in the bomb attack. She sees a beggar and realizes he is a man called Iwakichi (Osamu Takizawa) who used to work for her parents, now burned on the face and partially blind. She visits him at his home and asks about his family. His grandson, Tarō, is now in an institution. She visits the institution and finds the children barely have enough to eat. Takako offers to take Iwakichi and his grandson back to the island, but he refuses, running away.

Takako then goes on to visit Natsue, who was another teacher at a kindergarten where she used to teach, and is now a midwife. Natsue has been rendered sterile by the atom bomb blast and she is discussing adopting a child. Natsue and Takako visit the site of the kindergarten, which is now destroyed, and Takako decides to visit the students of the kindergarten.

The father of the first student she visits, Sanpei, has suddenly been taken ill from a radiation-related illness and dies just before she arrives. Another one of the students is terminally ill and dying in a church where many people with bomb-related injuries are gathered.

After staying the night in Natsus house, she then goes to visit another student, Heita. His sister (Miwa Satō), who has an injured leg, is just about to get married, and Takako dines with her. She talks to Heitas older brother (Jūkichi Uno) about the people who died or were injured in the war.

She returns to Iwakichis house and asks him again to let her take Tarō back to the island. At first he refuses, then his wife persuades him to let Takako take Tarō. However, Tarō refuses to leave his grandfather. The grandfather sets Tarō down for a meal, buys him new shoes, and sends him to Takako with a letter. Then he sets his house on fire. He survives the fire but is badly burned and eventually dies. Tarō and Takako go back to the island, carrying Iwakichis ashes.

==Cast==
* Nobuko Otowa as Takako Ishikawa
* Osamu Takizawa as Iwakichi
* Miwa Saitō as Natsue Morikawa
* Tsuneko Yamanaka
* Hideji Ōtaki  Takashi Itō
* Chikako Hosokawa as Setsu, Takakos mother
* Masao Shimizu as Toshiaki, Takakos father
* Yuriko Hanabusa as Oine
* Tanie Kitabayashi as Otoyo
* Tsutomu Shimomoto as Natsues husband
* Eijirō Tōno
* Taiji Tonoyama as Owner of a ship
* Jūkichi Uno as Kōji

==References==
 

==External links==
 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 