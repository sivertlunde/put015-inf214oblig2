Just Tell Me What You Want
{{Infobox film
| name           = Just Tell Me What You Want
| image          = Just_tell_me_what_you_want_poster.jpg
| image size     = 225px
| alt            = 
| caption        = Theatrical release poster
| director       = Sidney Lumet
| producer       = Burtt Harris
| writer         = Jay Presson Allen
| narrator       =  Alan King Ali MacGraw
| music          = Charles Strouse
| cinematography = Oswald Morris
| editing        = Jack Fitzstephens
| studio         =  Warner Bros. Pictures
| released       =  
| runtime        = 112 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $2,086,905   
}}
 American comedy Alan King, and was also the final film of screen legend Myrna Loy.
 David di Donatello Award for Best Screenplay of a Foreign Film.

==Plot==

Max Herschel, the married, wealthy, vulgar, egotism|egotistical, middle-aged head of a corporate empire, is satisfied with the somewhat casual love/hate relationship he shares with his mistress and protegee, television producer "Bones" Burton, just as it is, but she wants a more serious commitment.

The young woman attempts to extricate herself from the affair -- or perhaps force her lover into taking the next, more permanent step -- by dating a younger man, off-off-Broadway playwright Steven Routledge. Max, however, is not a man to accept defeat in any of his endeavors, and he retaliates with a vengeance.

The two engage in an escalating battle of wits, with Max discovering money cant resolve everything when he is outsmarted by business rival Seymour Berger and his grandson Mike. It leads to a comic fight between Max and Bones at New Yorks Bergdorf Goodman.

==Cast== Alan King ..... Max Herschel
*Ali MacGraw ..... Bones Burton 
*Peter Weller ..... Steven Routledge 
*Keenan Wynn ..... Seymour Berger  Tony Roberts .....  Mike Berger 
*Myrna Loy ..... Stella Liberti 
*Dina Merrill ..... Connie Herschel 
*Judy Kaye ..... Baby 
*Joseph Maher .....  Dr. Coleson

==Production credits== Original Music ..... Charles Strouse    
*Cinematography ..... Oswald Morris Production Design ..... Tony Walton
*Art Direction ..... John Jay Moore    
*Set Decoration ..... Robert Drumheller, Justin Scoppa, Jr.
*Costume Design ..... Gloria Gresham, Tony Walton

==Production==

The film features the last motion picture performance of Myrna Loy, who plays Herschels confidential secretary.  

Interiors of the Warner Bros. release were filmed at the Kaufman Astoria Studios in Queens, New York, with Manhattan exteriors (and the scene inside Bergdorf Goodman) shot on location.

==Critical reception==
In his review in Time (magazine)|Time, Frank Rich stated, "After a brisk 20 minutes, the movie loses its assurance and sense of purpose. Indeed, Jay Presson Allens screenplay reels around like a drunken sailor. From moment to moment, Just Tell Me is a somber and confusingly plotted story of corporate power struggles, a syrupy account of a love triangle, and a sloppy satire of show business . . . There are still some bright moments, but they are separated by flaccid, poorly connected scenes that go nowhere at great length. The few high points belong to King and MacGraw.   is too much of a pussycat to convey the heros toughness, but he delivers Allens best sallies with crackling speed . . . Though MacGraw is no comedian, she is animated and playful for the first time in memory." 

==References==
 

==External links==
 
*  
*  
*  
*  

 

 
 
 
 
 
 