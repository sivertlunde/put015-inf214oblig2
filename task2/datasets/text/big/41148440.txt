The Battle Between the Burps and Farts
{{Infobox film
| name           = The Battle Between the Burps and Farts
| image          = 
| caption        = 
| director       = Aiden Dillard
| producer       = 
| writer         = Aiden Dillard
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = Troma Entertainment   
| released       = 
| runtime        = 31 minutes   
| country        = United States
| language       = English
| budget         =  
| gross          = 
}}
The Battle Between the Burps and Farts is a 31-minute long  short film directed, written and produced by Aiden Dillard and distributed by Troma Entertainment.  It was made as an expansion of a student film he made in his junior year    which was, according to Dillard, he made the film in the wake of 9/11, calling it "his attempt to show the terrorists that they will never destroy the rotten, smelly core of the Big Apple". 

==Screening==
The Battle Between the Burps and Farts was first screened in New York City and upon its première, was responsible for inciting many a fist fight amongst audience members.  After it was submitted to the TromaDance festival, it became the first and so far only film to be booed by everybody in attendance.  Despite this, Troma Entertainment executive Lloyd Kaufman remained optimistic about it, calling it a "high point of TromaDance 2004"    and on the back of this films screening, Troma released Meat Weed Madness which included this film as an extra on the DVD. 

==Plot==
The film tells the story of a girl, Princess Martha Stew-Burp,  who is able to channel flatulence at will.  After she is kidnapped by a wizard, she is taught to channel her wind burping|orally, but after being raped by the leader of the Burps, regains her ability to channel her wind anally. 

==Critical reception==
Bill Gibron of DVD Talk called its universal condemnation "not hard to see why"  and called it "excruciating to sit through. Its loud, abrasive and more or less pointless. Still, it does offer a window into Dillards designs as a filmmaker". 
==References==
 

 
 