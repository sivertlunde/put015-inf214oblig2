Echelon Conspiracy
 
{{Infobox film
| name           = Echelon Conspiracy
| image          = Echelon conspiracy poster.jpg
| alt            =  
| caption        =Promotional film poster
| director       = Greg Marcks
| producer       = John Corser Alexander Leyviman Steve Richards Roee Sharon
| screenplay  = Kevin Elders Michael Nitsberg
| story           = Michael Nitsberg
| starring       = Shane West Edward Burns Ving Rhames Jonathan Pryce Martin Sheen
| music          = Bobby Tahouri
| cinematography = Lorenzo Senatore
| editing        = Joseph Gutowski James Herbert
| studio         = Dark Castle Entertainment
| distributor    = After Dark Films
| released       =  
| runtime        = 105 minutes
| country        = United States
| language       = English Russian
| budget         = 
| gross          = $2,186,782 
}}
Echelon Conspiracy (formerly titled The Gift) is a 2009 American science fiction action thriller film directed by Greg Marcks and starring Shane West, Edward Burns, and Ving Rhames.

==Plot==
A young American computer engineer (Shane West as Max) acquires a mobile phone that receives strange text messages. First they encourage him to miss his flight which he learns crashed soon after takeoff. Then the messages direct him to buy a certain stock, which he then learns increases by 313%. Next, he is directed to a hotel/casino in Prague to gamble. He first wins one-hundred thousand Euro on a slot machine and bets the entire amount on a hand of blackjack, which he wins. Both bets are made on the basis of further text messages he receives on his new phone. Max then has an altercation with a beautiful woman (Tamara Feldman) and her jealous boyfriend in the hotel corridor, where he is knocked-out and his mysterious phone is apparently scanned. Max wakes up with the woman, Kamila, smiling above him.  He immediately asks her out for a drink.

To further his new-found career in gambling, Max enlists the aid of a Russian cabbie/apparent e-gadget enthusiast, Yuri (Sergey Gubanov), who outfits him with a text-to-voice earpiece to wirelessly receive his anonymous and lucrative text messages. His lucky streak continues when he hits the 3 million Euro jackpot on a slot machine but is forced to run away when casino security led by John Reed (Edward Burns) attempts to detain him.  This chase is ended by the intervention of FBI Agent Dave Grant (Ving Rhames) who handcuffs Max and interrogates him for information about the phone. Max is frightened, but unable to provide any information.

At this point Agent Grant contacts Raymond Burke (Martin Sheen) of the National Security Agency|NSA, who is apparently monitoring Max because he is receiving messages from an omniscient communication surveillance computer system known as Echelon (signals intelligence)|Echelon. These messages have been responsible for the deaths of several Americans, most recently an IT specialist working in the The Pentagon|Pentagon. Burke recently lost a battle to pass a bill in Congress that would allow Echelon to be upgraded by being uploaded into personal computers worldwide. Burke eventually decides that Max knows too much and must be eliminated, however, Reed and the beautiful woman from the hotel – now revealed as Reeds associate – come to Maxs aid and spirit him away to Moscow. There Max re-connects with the techie Yuri and tries to get his help in discovering who is sending the messages. Yuri believes that the messages are coming directly from the computer itself, and that the system has achieved some kind of autonomous self-awareness. Max and Reed object to his idea, but they are forced to flee when more armed men arrive at Yuris apartment. A car chase through Moscow ensues.

The chase ends with Reed outmaneuvering and eventually blowing-up the pursuing cars, which happen to be led by Agent Grant. Grant escapes injury, but Max seizes the moment to inflict some payback for their previous encounter. Grant asks Max to help him stop Echelon, as he has now also begun to receive threatening texts.  Max receives another text, instructing him to return to Omaha, Nebraska, where he first worked as a computer security engineer. Max, Grant and Reed all fly home on a military aircraft.    
Arriving in Omaha, the group finds a sealed-up, bunker-like structure with a cache of servers and a high-end computer system that Max helped install years earlier. The property is revealed to belong to another victim of Echelons messages, the same person whose credit card was used to send Max the phone. Max starts up the bunkers computer and is instructed via text to fire up the servers and connect them to the network.  Echelon begins downloading itself into the bunkers computers and begins a countdown to replicate itself across the world wide computer network. Agent Grant calls Burke at the NSA to inform him, but Burke is content to let the Echelon replicate itself worldwide, in the interests of US national security. Meanwhile, Max is failing to stop Echelons replication countdown, until he takes an idea from the 1968 Star Trek episode The Ultimate Computer. Max asks the computer what its primary purpose is and it replies to defend the US, as defined by the Constitution. Max asks the computer to search for threats to the US Constitution, and it comes back with a plethora of articles concerning the recent attempts to secure Congressional approval to upgrade Echelon, which is characterized as a grave threat to personal freedoms. At the point when the download is complete, Echelon "learns" that it is itself the threat and executes its own shutdown.
 Security Service.  In a conversation (in Russian) with his apparent commanding officer he is commended for his actions. Yuri replies that they will soon try to start it again but, for today, they have helped the Americans to make the right decision; "I would like to believe", he adds cryptically, and at that moment turns off his mobile phone.

==Cast==
* Shane West as Max Peterson
* Edward Burns as John Reed
* Ving Rhames as Agent Dave Grant
* Martin Sheen as Raymond Burke
* Jonathan Pryce as Mueller
* Tamara Feldman as Kamila
* Sergey Gubanov as Yuri Malinin
* Gosha Kutsenko as Russian General
* Steven Elder as Charles

==Reception==
Echelon Conspiracy received overwhelmingly negative reviews. Review website Rotten Tomatoes reports that 0% of film critics gave it a positive review, based on 12 reviews. Audiences also disliked the film, with only 30% of viewers liking the film. 

==See also==
*List of films featuring surveillance

==References==
 

==External links==
*  
*  
*  
*  
*   by Duane Byrge at The Hollywood Reporter (May 16, 2008)

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 