History of Fear
 
{{Infobox film
| name           = History of Fear
| image          = 
| caption        = 
| director       = Benjamín Naishtat
| producer       = 
| writer         = Benjamín Naishtat
| starring       = Jonathan Da Rosa
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = Argentina
| language       = Spanish
| budget         = 
}}

History of Fear ( ) is a 2014 Argentine drama film directed by Benjamín Naishtat. The film had its premiere in the competition section of the 64th Berlin International Film Festival.   

==Cast==
* Jonathan Da Rosa as Pola
* Tatiana Giménez as Tati
* Mirella Pascual as Teresa
* Claudia Cantero as Edith
* Francisco Lumerman as Camilo
* César Bordón as Carlos
* Valeria Lois as Beatriz
* Elsa Bois as Amalia
* Edgardo Castro as Marcelo 

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 