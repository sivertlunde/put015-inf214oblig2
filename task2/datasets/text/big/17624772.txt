Everybody's Fine (2009 film)
{{Infobox film
| name           = Everybodys Fine
| image          = Everybodys fine.jpg Kirk Jones
| producer       = {{Plain list |
* Gianni Nunnari
* Ted Field
* Vittorio Cecchi Gori
* Glynis Murray
}}
| writer         = Kirk Jones
| starring       = {{Plain list |
* Robert De Niro
* Drew Barrymore
* Kate Beckinsale
* Sam Rockwell
* Katherine Moennig
}}
| music          = Dario Marianelli
| cinematography = Henry Braham
| editing        = Andrew Mondshein
| distributor    = Miramax Films
| released       =  
| runtime        = 99 minutes 
| country        = United States
| language       = English
| budget         = $21 million   
| gross          = $15,986,449 
}} Kirk Jones, Italian film Stanno tutti bene. In Brazil, Russia and Japan, the film was released Direct-to-video|direct-to-DVD.

==Plot==
Frank Goode (Robert De Niro), a recently widowed retiree, is getting ready for his children, David (Austin Lysy), Rosie (Drew Barrymore), Amy (Kate Beckinsale) and Robert (Sam Rockwell) to come visit him. One by one though, each of his children call to cancel on him at the last minute. Feeling a bit down by the rejections, Frank decides to head out on a cross-country trip, visiting each of his kids, despite warnings against travel from his doctor. He is chronically ill with cardiac and respiratory problems from his life work making PVC-covered power lines. He deceives his children about his health, telling them that he is fine.

As Frank travels to each of his childrens homes, beginning with his son David who is absent from his New York apartment, his other son and daughters divert his surprise visits and make excuses for not allowing him a lengthy visit. He begins to suspect that something is amiss. 

Frank heads home to Elmira, New York, by plane and suffers a heart attack. The heart attack renders him in a dream-like state where he reflects on his visits to his children. While reflecting upon his visits, Frank realizes that each of his children is hiding a secret: Amys husband is leaving her for another woman and Amy has found a new boyfriend; Robert is not a musical conductor nor is he going on tour to Europe; Rosie has mothered a child and is bisexual; David has gone missing. He awakes to find his children at the hospital to comfort him. His children finally tell him the big secret that David has died of a drug overdose. Upon his release from the hospital, Frank visits his wifes grave and talks to her. He tells her all about the kids and how theyre all doing fine. The final scene depicts Frank as he loves and accepts his children (including their secrets).

==Cast==
* Robert De Niro as Frank Goode   
* Drew Barrymore as Rosie Goode 
** Mackenzie Milone as the young Rosie
* Kate Beckinsale as Amy Goode 
** Lily Mo Sheen as the young Amy
* Sam Rockwell as Robert Goode 
** Seamus Davey-Fitzpatrick as the young Robert
* Austin Lysy as David Goode
** Chandler Frantz as the young David
* Katherine Moennig as Jilly, Rosies partner
* Melissa Leo as Colleen, the truck driver
* Lucian Maisel as Jack, Amys son
* Damian Young as Jeff, Amys estranged husband
* James Frain as Tom, Amys current partner
* Sonja Stuart as Jean Goode
** Mimi Lieber provides Jean Goodes voice

==Production==
Filming took place in Connecticut  and New York City, including several scenes filmed at Yale University in New Haven, Connecticut. Scenes set in a concert hall were filmed at Yale Universitys Woolsey Hall, and featured the New Haven Symphony Orchestra.

==Reception==
===Critical reaction=== rating average weighted average score out of 100 to reviews from film critics, the film is considered to have "mixed or average reviews", with a rating score of 47 based on 25 reviews. {{cite web | url= http://www.metacritic.com/film/titles/everybodysfine?q=Everybody%27s%20Fine | title= Everybodys Fine (2009): Reviews
| work= Metacritic | publisher= CBS Interactive | accessdate= December 25, 2009 }}  

Michael Medved gave Everybodys Fine two stars (out of four), calling the film "...bleak, deeply depressive, and utterly depressing..." But he also added that "DeNiros acting is intense and moving as always." 

Overall, the critics consensus praises Robert De Niro for having "intensity and presence that shines through even when hes not playing Travis Bickle/Jake LaMotta types, "but the movie becomes overly sentimental, and the supporting players arent given three-dimensional characters to play." 

===Box office===
The film "unspooled in 10th   with $4 million."  As of December 6, the film has grossed $4,027,000.    It closed on December 24, 2009 after a brief three-week run.

===Awards===
   GLAAD Media Award for Outstanding Film – Wide Release. Drew Barrymore will also receive the Vanguard Award at the 21st GLAAD Media Awards ceremony, in part due to her performance in the film.  

==Soundtrack== Golden Globe nomination for Best Song.

==Home media==
Everybodys Fine was released on DVD February 23, 2010.

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 