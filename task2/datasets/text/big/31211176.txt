The Temporary Widow
 
 
{{Infobox film
| name           = The Temporary Widow
| director       = Gustav Ucicky
| producer       = Erich Pommer Günther Stapenhorst
| writer         = Karl Hartl Walter Reisch Benn Levy
| starring       = Lilian Harvey Laurence Olivier Athole Stewart
| music          = Robert Stolz
| cinematography = Carl Hoffmann 
| editing        = 
| distributor    = Universum Film AG
| released       = 15 November 1930
| runtime        = 84 minutes Germany
| English
| budget         = 
}} German comedy Austrian director Gustav Ucicky, starring Laurence Olivier in his first film role, Lilian Harvey and Athole Stewart. 

Kitty Kellermann is put on trial for murdering her husband, a failed painter. When her counsel resigns from his mandate, the mysterious Peter Bille steps in, though it becomes apparent that he actually is not an advocate but Kittys lover and moreover confesses the murder. The widow has to admit that the pictures by her deceased spouse sell much better, only for him to suddenly appear alive.
 in 1953 (starring Curt Goetz himself) and in 1966.

==Cast==
* Lilian Harvey - Kitty Kellermann
* Laurence Olivier - Peter Bille
* Athole Stewart - President of the Court of Justice
* Gillian Dean - Witness Anny Sedal Frank Stanmore - Witness Kulicke
* Felix Aylmer - Public Prosecutor
* Frederick Lloyd - Counsel for the Defense
* Henry Caine - Councillor Lindberg

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 