Aane Pataaki
{{Infobox film
| name           = Aane Pataaki
| image          = 
| caption        = 
| director       = Chandrashekar Bandiyappa
| writers        = Chandrashekar Bandiyappa
| producer       = L. P. Suresh Babu
| starring       = Srujan Lokesh   Parvati Nirban   Jai Jagadish   Vijayalakshmi Singh   Sadhu Kokila   Rangayana Raghu
| cinematography = J. S. Wali
| editing        = Lingaraju
| music          = Dharma Vish
| released       =  
| runtime        = 130 minutes 
| language       = Kannada
| country        = India
| budget         = Rs. 1cr
| gross          = Rs. 2.1cr
}} Kannada comedy film directed by Chandrashekar Bandiyappa. He makes his directorial debut, after working as an assistant to major directors. The film stars Srujan Lokesh in lead. 

==Plot==
The movie is about a wannabe actor who comes to the city aspiring to become a hero. However because of his innocence and naivety he is often teased and made fun. He is invited to a party at the house of a movie producer because of an error made by the producers secretary. There he falls in love with a "Star Guest". There are rumors that the love story is based on a popular celebrity couple of the Kannada industry. 

== Cast ==
* Srujan Lokesh as Besagarahalli Bhyregowda
* Parvathi Nirban as Rakshitha
* Bhasker as Adi
* Rangayana Raghu as Govindanna
* Sadhu Kokila as Chinnadappa
* Jai Jagadish as Jagadish
* Vijayalakshmi Singh as Vijayalakshmi
* Rockline Sudhakar
* Somanna Jadar
* Cable Anand
* Akul

==Background and development==
Director Chandrashekar Bandiyappa had earlier worked as an assistant director under Rajendrasingh Babu and associate director under S.Narayan.  After working for more than 4 years in the film industry he tumbled upon an exciting action script. He spoke to many producers but could not convince any since the cost of the movie was very high and it was his first venture as a Director. So he decided to make "Aane Pataaki" which is a romantic comedy and the cost of making is comparatively less.

==Soundtrack==
{{Infobox album
|  Name        = Aane Pataaki
|  Type        = soundtrack
|  Artist      = Dharma Vish
|  Cover       =
|  Released    =  
|  Recorded    =  Feature film soundtrack
|  Length      = 
|  Label       = SounDesigns
|  Producer    = Dharma Vish
|  Reviews     = 
}}

The songs for the film were composed by  Dharma Vish. Although he has worked as an arranger and music producer for over 300 films over the last 15 years, this film is his debut as a music composer. 
{| class="wikitable"
|-
! SL. No
! Song
! Artist
! Lyricist
|-
| 1
| "Aane Pataaki"
| Dharma Vish
| Nagendra Prasad
|-
| 2
| "Itta Itta Baa"
| Vijay Prakash, Sunitha
| Somanna Jadar
|-
| 3
| "8ne Taragathi"
| Sangeetha Rajeev
| Arasu Anthaare
|-
| 4
| "One One Za"
| Queeny Fernandez
| Nagendra Prasad
|}

==References==
 

 
 
 
 
 


 