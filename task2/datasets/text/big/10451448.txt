Wartime Nutrition
 
{{Infobox Film
| name           = Wartime Nutrition
| image          = 
| image_size     = 
| caption        = 
| director       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| released       =  
| runtime        = 10 min.
| country        = United States
| language       = English
| budget         = 
}} short film. Allied propaganda United States home front to eat healthier and follow the wartime  ration guidelines. 
 British have years of rationing and their food supply being attacked. It shows the reductions in particular foods and the need to find replacement, particularly in "victory gardens" that can be grown in at home, or even by soldiers. Back in the states many people starve because they dont eat the right kind of food, but the same stuff every day (a sign in the background advertises a sirloin steak, for 35 cents) and a woman orders her usual coffee and donuts for breakfast.
 Surgeon General different methods of improving nutrition and their importance for the war industry worker are examined.

==External links==
*  

 
 

 