Especially on Sunday
{{Infobox film
 | name = Especially on Sunday
 | image = Especially on Sunday.jpg
 | caption = DVD cover
 | border = yes
 | director = Giuseppe Tornatore  Marco Tullio Giordana Giuseppe Bertolucci  Francesco Barilli
 | writer = Tonino Guerra
 | starring =
 | music = Ennio Morricone  Andrea Morricone
 | cinematography =
 | editing =
 | producer =
 | studio = Basic Cinematografica Titanus Paradis Films Rai Due Intermédias
 | distributor = Miramax Films (USA) Bac Films (France) Dusk Motion Pictures (Belgium)
 | released = September 26, 1991  (Italy)  August 13, 1993  (United States)
 | runtime =
 | awards =
 | country = Italy
 | language = Italian
 | budget =
 }}
La domenica specialmente (internationally released as Especially on Sunday) is a  , Marco Tullio Giordana, Giuseppe Bertolucci and Francesco Barilli.   

== Cast ==

=== La neve sul fuoco ===
*Maddalena Fellini: Caterina
*Chiara Caselli: the spouse
*Andrea Prodan: Marco
*Ivano Marescotti: Don Vincenzo
(directed by Marco Tullio Giordana)

=== Il cane blu ===
*Philippe Noiret: Amleto
*Nicola Di Pinto: pastore
(directed by Giuseppe Tornatore)

=== La domenica specialmente ===
*Bruno Ganz: Vittorio
*Ornella Muti: Anna
*Nicoletta Braschi: Nicoletta
(directed by Giuseppe Bertolucci)

=== Le chiese di legno ===
*Jean-Hugues Anglade: Motociclista degli uccellini 
*Sergio Bini Bustric 
(directed by Francesco Barilli)

==References==
 

==External links==
* 
 
 

 
 
 
 
 
 
 
 
 


 
 