Mothra (film)
 

{{multiple issues|
 
 
 
}}
{{Infobox film
| name           = Mothra
| image          = Mothra.jpg
| image_size     = 
| caption        = 
| director       = Ishirō Honda
| producer       = Tomoyuki Tanaka
| writer         = Shinichi Sekizawa
| narrator       = 
| starring       = Frankie Sakai Kyoko Kagawa Hiroshi Koizumi
| music          = Yuji Koseki
| cinematography = Hajime Koizumi
| editing        = Kazuji Taira Toho Studios
| distributor    = Toho (Japan) Columbia Pictures (US)
| released       =  
 
| runtime        = 100 min.
| country        = Japan Japanese English English
| budget         = 
| gross          = 
}}
 Toho Studios, academic roles The Last War.
 her own trilogy in the 1990s.

==Plot==
An expedition to an irradiated island brings civilization in contact with a primitive native culture. When one sensationalist entrepreneur tries to exploit the islanders, their ancient deity arises in retaliation.
 Infant Island, Rolisican atomic tests, the Daini-Genyou-Maru is caught and run aground in the turbulence of a typhoon. A rescue party following the storm finds four sailors alive and strangely unafflicted with radiation sickness, which they attribute to the juice provided them by island indigenous peoples|natives. The story is broken by tenacious reporter Zenichiro (also known as "Bulldog" or "Zen-chan") Fukuda (Sakai) and photographer Michi Hanamura (Kagawa), who infiltrate the hospital examining the survivors.
 capitalist Clark anthropologist Shinichi mutated flora, Emi and Yumi Itō), who save Chujo from being eaten by a vampire plant. The "Shobijin" (small beauties), as Fukuda dubs them, wish their island to be spared further atomic testing. Acknowledging this message, the team returns and conceals these events from the public.
 henchmen and holding the girls against their will; Nelson denies the charge and files a libel suit against the paper. Meanwhile, the island egg hatches to reveal a gigantic caterpillar, which begins swimming the Pacific Ocean toward Japan. The caterpillar destroys a cruise ship and survives a napalm attack on a beeline path for Tokyo. The Rolisican Embassy, however, defends Nelsons property rights over the girls, ignoring any connection to the monster.
 cocoon in New Kirk City for Nelson as Mothra lays waste to the metropolis. Ultimately Nelson is killed in a shootout with police, and the girls are assigned to Chūjōs care. Church bells begin to ring, and sunlight illuminates the cross atop the steeple with radiant beams, reminding Chūjō and Hanamura of Mothras unique symbol and of the girls voices. Chūjō hits upon a novel way to attract Mothra to an airport runway. The girls are returned amid salutations of "sayōnara", and Mothra flies back to Infant Island.

==Production==
Based on the novel The Luminous Fairies and Mothra, this adaptation was penned by Shinichi Sekizawa, who submitted his first draft proposal in 1960. Vaguely titled The Giant Monster Thing, this early concept featured a race of tiny women alongside the movies giant monster. A name for the fairies was not specified in this first proposal, as apparently even at this early stage Sekizawa was contemplating a name change from their original title of the "Airenas" from the book.

In an unusual turn of events, another draft proposal was submitted shortly after. Re-titled Giant Monster Mothra, this second proposal fleshed out the concept more, giving the small women the name of the "Pichi Fairies" while citing that five would be seen during the course of the film. The size of the Pichi Fairies was stated as being 60 centimeters, showing a strange level of detail in this respect for a draft proposal. For reference, that would have been roughly twice the size of the final "Shobijin." The idea was seen as a winner, as a more fleshed out screenplay was finally commissioned.

In 1960, the first script for the production was completed, and was accompanied by a storyboard to help relate the concept. The storyboard, infamous for showing a rather frightening interpretation of Mothra, displayed a number of different concepts compared to the final film, including Mothra squaring off with a squadron of jets and also building a cocoon on the Diet Building, a concept that would eventually return in Godzilla vs. Mothra (1992). A lot of this was about to change, though, with the signing of two popular singers to the project.

On January 14, 1961, the second draft of Giant Monster Mothra was submitted. Since the first draft, the singing duet The Peanuts (Emi and Yumi Ito) had been attached to the production. Seeing a very lucrative idea in the making, the fairy role was altered to just that of the singing duo, while their name was also changed to the Shobijin. The concept would be altered even further by Sekizawa, as a third draft of the script was submitted on February 5 of that same year. Production on the movie was now in full swing, although the name Giant Monster Mothra hanged around for quite a while, even being used on advance posters before it was shortened to simply Mothra (1961) for its final release.

The original climax for Mothra was entirely different from what ended up in the final print. Originally, Nelson and his group had planned to kidnap Shinji Chujo for a longer period of time, rather than leaving him tied up as they fled the country. Instead, they took the small boy on a private plane as they attempted to flee, only to accidentally crash near a mountainside. Meanwhile, the films heroes are in close pursuit, having brought along the authorities to help save the boy. All of this takes place while Mothra is still in its cocoon, meaning that Fukuda and the others werent originally planned to witness the birth of Mothras adult form. Instead they give chase to Nelson as Mothra breaks free after the Atomic Heat Ray Gun attack. She then immediately flies to the mountainside, kills Nelson and goes back to Infant Island with the Shobijin. 

Its hard to say how much of the original climax was finished before being cut, but there are a number of black and white production stills showing that the mountain chase sequence was filmed, at least in part; however, no photos have been seen of the story boarded sequence relating to the plane crash that would have occurred just before it.

==U.S. release==
  theatrical poster for the 1962 U.S release of Mothra.]]
Mothra was released in the United States in May 10, 1962 on a double-bill with The Three Stooges in Orbit. Film critic A. H. Weiler in The New York Times gave the film a generally positive review, singling out the color and special effects for praise. "Theres that color, as pretty as can be, that now and then smites the eye with some genuinely artistic panoramas and décor designs."

Some plot points, also, were favorably mentioned: "Fantastic though the plot may be, there are some genuinely penetrating moments, such as the contrast of the approaching terror and those patient, silvery-voiced little dolls, serenely awaiting rescue. Several of the special effects shots are brilliant, such as the sight of a giant cocoon nestling against a large citys power station tower" (in reality, it was Tokyo Tower, a landmark broadcasting tower). The movie was edited also removing about 10 minutes from the original Japanese version for its release in America at 90 minutes.

==Cast==
* Frankie Sakai - Zenichirou Zen-Chan Fukuda, Nittou Journalist
* Kyoko Kagawa - Michi Hanamura, Nittou Photographer 
* Hiroshi Koizumi - Dr. Shinichi Chujo 
* Ken Uehara - Dr. Harada 
* Jerry Itou - Clark Nelson 
* The Peanuts, Yumi Itou, Emi Itou - The Shoubijin 
* Takashi Shimura - Sadakatsu Amano, Nittou Editor 
* Masamitsu Tayama - Shinji Tyuujou  Tetsu Nakamura - Nelsons Henchman 
* Ousmane Yusef - Danny, Nelsons Henchman 
* Akira Wakamatsu - Nelsons Henchman 
* Hiroshi Takagi - Nelsons Henchman 
* Toshio Miura - Nelsons Henchman 
* Audie Wyatt - Dr. Raaff, Captain of Infant Island investigating group
* Akihiko Hirata - Director General of National Nucleus Center 
* Hisaya Itou - Commander of Defence Force A 
* Yoshihumi Tajima - Commander of Defence Force B 
* Kenji Sahara - Helicopter pilot A 
* Kenzou Echigo - Helicopter pilot B 
* Seizaburou Kawazu - Secretary of Defense 
* Haruo Nakajima - Mothra Larva (Head) 
* Katsumi Tezuka/Other - Mothra Larva (Body)

==Additional credits==
*Eiji Tsuburaya- special effects
*Teruyoshi Nakano- assistant special effects director
*Akira Watanabe-  Special effects art director
*Yukio Manoda- special optical photography
*Teisho Arikawa- effects photography
*Hiroshi Mukoyama- matte shots
*Samaji Nonagase- chief assistant director

==DVD release==
Columbia TriStar (Sony)
*Released: August 18, 2009 (as part of the Icons of Sci-Fi: Toho Collection, along with The H-Man and Battle in Outer Space)

Mill Creek Entertainment
*Released: February 25, 2014 as part of the "Sci-Fi Creature Classics", along with   widescreen though the DVD case mislabels it as being in letterbox.) 

==References==
*Kalat, David. A Critical History and Filmography of Tohos Godzilla Series, McFarland, 1997. ISBN 0-7864-0300-4
*Weiler, A. H. (1962).  . New York Times. Retrieved October 25, 2013.
 

==External links==
* 
* 
* 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 