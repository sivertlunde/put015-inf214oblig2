The Stoning of Soraya M.
 
{{Infobox film
| name           = The Stoning of Soraya M.
| film name      =  
| image          = The Stoning of Soraya M. US Poster.jpg
| alt            = 
| caption        = US theatrical release poster
| director       = Cyrus Nowrasteh John Shepherd Todd Burns Diane Hendricks    
| writer         = Betsy Giffen Nowrasteh Cyrus Nowrasteh
| based on       =   Jim Caviezel Parviz Sayyad Vida Ghahremani Navid Negahban
| music          = John Debney
| cinematography = Joel Ransom
| editing        = David Handman Geoffrey Rowland
| studio         = Mpower Pictures
| distributor    = Roadside Attractions 
| released       =  
| runtime        = 116 minutes
| country        = United States
| language       = Persian English
| budget         = 
| gross          = $1,090,260   
}} 1990 book La Femme Lapidée.
 world premiere at the 2008 Toronto International Film Festival, where it won the Directors Choice Award. It was also the second runner-up for the Cadillac Peoples Choice Award. The book has been banned in Iran. 

==Plot== a journalist is approached by Zahra, a woman with a harrowing tale to tell about her niece, Soraya, and the bloody circumstances of Sorayas death, by stoning, the previous day. The two sit down as Zahra recounts the story to Freidoune, who records the conversation with his tape recorder. The journalist must escape with his life to tell the story to the rest of the world.
 marry a 14-year-old girl.       Ali is able to convince the mullah by making threats to tell the rest of the village about his past as a convict.

Alis marriage to the teenager is conditional on Alis ability to save the girls father, a doctor who has been sentenced to death for an unspecified crime. The mullah proposes that Soraya becomes his temporary wife, referred to in Iran as Sigeh, in exchange for protection and monetary support for Soraya and her two daughters. Zahra barges in and encourages Soraya to refuse the offer. Soraya has two sons whom Ali wants, and who have both turned against her. Some days following the incident, a woman dies. The mullah, the villages mayor, and Ali ask Zahra to persuade Soraya to care for the widower. Zahra suggests that Soraya may do the job if she is paid.

Soraya starts working for the widower, and Ali plans to use the unusual circumstance to spread lies that Soraya is being unfaithful to him so that she will be stoned and he can remarry. Ali also knows if Soraya were dead, he would not have to pay child support. Ali and the mullah start a rumor about Sorayas infidelity so they can charge her with adultery. One day while Zahra is walking in town, she realizes that a rumor has spread that her niece is being unfaithful to her husband.

Ali and the mullah need one more "witness" to Sorayas "infidelity" to be able to formally charge her. They visit the widower at home and, using threats, manipulate the widower into agreeing to back up their story. Soon after, Ali drags Soraya through the streets, beating her and publicly declaring that she has been unfaithful. Zahra intervenes and takes her niece, Ali, and the Mayor of the village to her house to talk privately at her house. They bring the widower to the home and after he lies and says that they had engaged in adultery, a trial is pursued. Only men, including Sorayas father, are allowed while Soraya is confined with some women in Zahras house. She is quickly convicted. Zahra tries to flee with her and after realizing she cannot, goes to plead with the mayor for Sorayas life, even offering to switch places with Soraya. The conviction is upheld though, and as they are preparing for the stoning, the Mayor prays to Allah for a sign if they are not doing the right thing. 

Before the actual stoning can begin, a traveling carnival van comes through and tries to perform their act. They are shooed away, where they wait by the sidelines as the stoning begins. Sorayas father disowns her as he is given the first stone to throw but he misses her repeatedly. A woman in the crowd pleads to the mayor that the stones missing are a sign Soraya is innocent, but none of the men listen. Ali takes up stones and throws them himself. Her two sons are also forced to throw stones.   The widower is given two stones to throw but instead walks away in tears. The crowd finally joins in and Soraya is stoned to death.
 Revolutionary Guard to stop him at gunpoint. They spill out the belongings of his bag, seize his tape recorder, and destroy all of the tapes. But as the journalist prepares to drive away, Zahra appears out of an alley with the true tape in her hand. Men attempt to run after the car before the journalist is able to drive away. Zahra screams that the God that she loves is great and now the whole world will know of the injustice that has happened.  

==Cast==
* Mozhan Marnò as Soraya M.
* Shohreh Aghdashloo as Zahra Jim Caviezel as Freidoune Sahebjam
* Parviz Sayyad as Hashem
* Vida Ghahremani as Mrs. Massoud
* Navid Negahban as Ali
* Vachik Mangassarian as Morteza Ramazani, Sorayas father
* Bita Sheibani as Leila

==Book==
The international bestseller book tells the story of one of the victims of stonings in modern Iran.         

Soraya Manutchehris husband Ghorban-Ali was an ambitious man, prone to fits of rage. He wanted a way out of his marriage in order to marry a 14-year-old girl but did not want to support two families or return Sorayas dowry.  When Soraya began cooking for a local widower he found a way to achieve his goal. Abetted by venal and corrupt village authorities, who also turned her father against her, he accused his wife of adultery. She was convicted, buried up to her waist, and stoned to death.   

===Author===
The son of a former Iranian ambassador, French-Iranian journalist and war correspondent Freidoune Sahebjam has also reported on the crimes of the Iranian government against the Baháí Faith|Baháí community in Iran. 
He was traveling through Iran, when he came upon Sorayas village, where he learned from her aunt about Soraya and her cruel fate. 

Credibility of Freidoune Sahebjams story and film plot itself is disputed by many Iranian critics,   expert Elise Auerbach from Amnesty International,  and film critics like Richard Nilsen from The Arizona Republic  and Wesley Morris from Boston Globe. 

==Reception==
The Stoning of Soraya M. received generally mixed reviews; it currently holds a 57% rating on Rotten Tomatoes with a consensus that states: "The Stoning of Soraya M. nearly transcends its deficiencies through the sheer strength of its subject material, but ultimately drowns out its message with an inappropriately heavy-handed approach."

===Box office and financing===
The film opened at #32 at the U.S. box office in 27 theaters, grossing $115,053 in the opening weekend. As of August 30, its domestic grossing is $636,246.  It grossed $1,090,260 worldwide. 
 Todd Burns.  Additional financing came from Blackwater founder Erik Prince.  

==Accolades==
{| class="wikitable" style="font-size:90%;" ;"
|- style="text-align:center;"
! style="background:#cce;"| Award
! style="background:#cce;"| Category
! style="background:#cce;"| Recipients and nominees
! style="background:#cce;"| Outcome
|-
|rowspan="2"| Flanders International Film Festival
| Canvas Audience Award
| -
|  
|-
| Gran Prix - Best Film
| Cyrus Nowrasteh
|  
|-
| Heartland Film Festival
| Heartland Truly Moving Picture Award
| Cyrus Nowrasteh
|  
|-
| Los Angeles Film Festival
| Audience Award for Best Narrative Feature
| Cyrus Nowrasteh
|  
|- Satellite Awards Best Motion Picture, Drama
| -
|  
|- Best Actress in a Motion Picture, Drama
| Shohreh Aghdashloo
|  
|- Best Actress in a Supporting Role
| Mozhan Marnò
|  
|- Toronto International Film Festival
| Runner Up Audience Choice Award
| Cyrus Nowrasteh
|  
|}

==See also==
* Human rights in the Islamic Republic of Iran
* Violence against women
* 2008 in film
* Cinema of the United States
* Rajm

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 