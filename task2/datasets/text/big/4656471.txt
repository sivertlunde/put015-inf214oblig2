The Mad Adventures of Rabbi Jacob
{{Infobox film
| name           = The Mad Adventures of Rabbi Jacob
| image          = The Mad Adventures of Rabbi Jacob.jpg
| border         = yes
| caption        = Poster for the French release
| director       = Gérard Oury
| producer       = Bertrand Javal
| writer         = Gérard Oury, Danièle Thompson (screenplay)
| starring       = Louis de Funès Suzy Delair Marcel Dalio Claude Giraud Claude Piéplu Renzo Montagnani Henri Guybet Miou-Miou
| music          = Vladimir Cosma
| cinematography = Henri Decaë
| editing        = Albert Jurgenson
| distributor    =
| released       = 18 October 1973
| runtime        = 100 minutes
| country        = France Italy 
| language       = French
| budget         =
}}
The Mad Adventures of Rabbi Jacob ( ) is a 1973 French-Italian comedy film directed by Gérard Oury, starring Louis de Funès and Claude Giraud.

==Plot==
Rabbi Jacob (Marcel Dalio) is one of the most loved rabbis of New York. One day, the French side of his family, the Schmolls, invite him to celebrate the bar mitzvah of the young David. Rabbi Jacob boards a plane to leave America for his birthland of France after more than 30 years of American life. His young friend Rabbi Samuel comes with him.

In Normandy, the rich businessman Victor Pivert (  has just begun, Pivert fires him, much to Salomons content.

Arab revolutionist leader Mohamed Larbi Slimane (Claude Giraud) is kidnapped by killers who are working for his countrys government. The team, led by Colonel Farès, takes him by night to an empty bubble gum factory... the same place where Victor Pivert goes to find assistance. Pivert involuntarily helps Slimane to flee, leaving two killers corpses behind them. The police, alerted by Salomon, find the bodies and accuse Pivert of the crime.

The next day, Slimane forces Pivert to go to Orly airport to catch a plane to Slimanes country (if the revolution succeeds, he will become Prime Minister). However, they are followed by a number of people: the jealous Germaine, Piverts wife, who thinks her husband is going to leave her for another woman; Farès and the killers; and the police commissioner Andréani (Claude Piéplu), a zealous and overly suspicious cop who imagines that Pivert is the new Al Capone. Farès and his cohorts manage to kidnap Germaine, and they use her own dentist equipment to interrogate her.

Trying to conceal his and Piverts identities, Slimane attacks two rabbis in the toilets, stealing their clothes and shaving their beards and their payot. The disguises are perfect, and they are mistaken for Rabbi Jacob and Rabbi Samuel by the Schmoll family. The only one who recognizes Pivert (and Slimane) behind the disguise is Salomon, his former driver, who just happens to be a Schmoll nephew. But Pivert and Slimane are able to keep their identity secret and even manage to hold a sermon in Hebrew language|Hebrew, thanks to the polylingual Slimane (who is deeply gutted, of course).

After a few misunderstandings, Commissioner Andréani and his two inspectors are mistaken by the Jews for terrorists, attempting to kill Rabbi Jacob. The real Rabbi Jacob arrives at Orly, where no one is waiting for him any more. He is mistaken for Victor Pivert by the police, then by Farès and his killers (both times in a painful way for his long beard).

There is a chaotic, but sweeping happy ending:

*the revolution is a success, and Slimane becomes President of the Republic
*Piverts daughter falls in love with Slimane and escapes her dull fiance near the altar to go with him
*Pivert learns tolerance towards other religions and cultures, and also Salomon and Slimane make peace with their respective Arab and Jewish colleagues
*the Schmolls finally find the real Rabbi Jacob
*the Piverts and the Schmolls go together feasting and celebrating

==Cast==
* Louis de Funès - Victor Pivert
* Suzy Delair - Germaine Pivert
* Marcel Dalio - Rabbi Jacob
* Claude Giraud - Mohamed Larbi Slimane
* Renzo Montagnani - Colonel Farès
* Janet Brandt - Tzipé Schmoll, la grand-mère
* André Falcon - Le ministre
* Xavier Gélin - Alexandre, le fils du général
* Miou-Miou - Antoinette Pivert
* Henri Guybet - Salomon	
 

==Notes==
 

==External links==
* 
*  

 

 
 
 
 
 
 
 
 
 
 
 
 