The Golden Compass (film)
 
{{Infobox film
| name           = The Golden Compass
| image          = The Golden Compass.jpg
| caption        = Theatrical poster
| alt            = 
| director       = Chris Weitz
| producer       = Bill Carraro Deborah Forte
| based on       =  
| screenplay     = Chris Weitz
| starring       = {{Plain list |
* Nicole Kidman 
* Sam Elliott 
* Eva Green 
* Dakota Blue Richards 
* Ian McKellen 
* Ian McShane 
* Freddie Highmore 
* Kathy Bates 
* Kristin Scott Thomas 
* Daniel Craig
}}
| music          = Alexandre Desplat
| cinematography = Henry Braham
| editing        = {{Plain list | 
* Anne V. Coates
* Peter Honess
* Kevin Tent
}}
| studio         = {{Plain list | 
* New Line Cinema
* Ingenious Film Partners
* Scholastic Productions
* Depth of Field
* Rhythm and Hues
}}
| distributor    = New Line Cinema (US) Entertainment Film Distributors (UK)
| released       =  
| runtime        = 113 minutes 
| country        = United States United Kingdom
| language       = English
| budget         = $180 million 
| gross          = $372,234,864 
}} Northern Lights, Philip Pullmans trilogy His Dark Materials. Directed by Chris Weitz, it stars Dakota Blue Richards, Daniel Craig, Eva Green, Tom Courtenay, Christopher Lee, Nicole Kidman and Sam Elliot. The project was announced in February 2002, following the success of recent adaptations of other fantasy epics, but troubles over the script and the selection of a director caused significant delays. At US$180 million, it was one of New Line Cinemas most expensive projects ever,    and its middling success in the US contributed to New Lines February 2008 restructuring.   
 parallel universe Gyptian children are disappearing at the hands of a group the children call the Gobblers. When Lyra discovers that Mrs. Coulter is running the Gobblers, she flees. Rescued by the Gyptians, Lyra joins them on a trip to the far north, to the land of the armored polar bears, in search of the missing children.
 secularist organisations average score of 51, based on 33 reviews. 

==Plot== Lord Asriel, who instructs her to remain in hiding. Lyra watches Asriel give a presentation regarding Dust, a particle that the Magisterium has forbidden the mention of. The college gives Asriel a grant to fund a northern expedition.

At dinner, Lyra meets Marisa Coulter|Mrs. Coulter, who insists on taking Lyra north as her assistant. Before Lyra leaves, the Master of the college entrusts her with the only remaining alethiometer, a compass-like artifact that reveals the truth. The Magisterium has destroyed all the others. He instructs her to keep it secret, especially from Mrs. Coulter.
 Roger and Gyptian friend Billy have been taken by the Gobblers.

Lyra and Pan walk in on Mrs. Coulters dæmon attempting to steal the alethiometer. They escape into the streets. The "Gobblers" pursue her, but she is saved by some Gyptians. Aboard a Gyptian boat heading north to rescue their children, Lyra shows the alethiometer to a Gyptian wise man, Farder Coram. On deck that night Serafina Pekkala, the witch queen, tells Lyra that the missing children are in a place called Bolvangar. Mrs.Coulter sends two mechanical spy flies after Lyra and Pan; one is batted away but the other is caught and sealed in a tin can by Farder Coram, who explains that the spy fly has a sting with a sleeping poison.
 armoured bear. Iorek Byrnison has been tricked out of his armour by the local townspeople. Using the alethiometer Lyra tells Iorek where to find his armour. Armoured again, the fearsome Iorek and his friend Lee Scoresby join the trek northward.
 Samoyeds who capture Lyra. Taken to the armoured bear king Ragnar Sturlusson, Lyra tricks him into fighting Iorek one on one. At first, Ragnar seems to have the upper hand in the fight, but Iorek eventually tricks his rival and kills him. He then becomes the new king. Iorek carries Lyra near to a thin ice bridge near Bolvangar. Reaching the station, Lyra is taken to eat with the missing children. While hiding again Lyra discovers that the Magisterium scientists, under the guidance of Mrs. Coulter, are performing experiments to sever the bond between a child and their dæmon. Caught spying, Lyra and Pan are thrown in the intercision chamber, and end up unconscious from the energy force that tries to cut them. On seeing Lyra in the guillotine, Mrs. Coulter rescues her and takes her to her quarters.

When Lyra wakes up she is comforted by a distraught Mrs. Coulter, who explains the dæmon cutting to Lyra and also tells Lyra that she is her mother. Lyra then guesses that Lord Asriel is her father. When Mrs. Coulter asks for the alethiometer, Lyra gives her the can containing the spy fly. The spy fly stings Mrs. Coulter, knocking her and her dæmon out. Lyra runs to the room with the intercision machine. The growing chain reaction builds as Lyra yanks a control box loose and hurls it into the intercision machine, causing it to explode. This sets off a series of explosions that tear the facility apart.

Outside, the children are attacked by Tartar mercenaries and their wolf dæmons. The battle is joined by Iorek, the Gyptians, and a band of flying witches led by Serafina Pekkala. The Tartars are defeated and the children are rescued. Rather than returning south, Lyra, Roger and Iorek fly north with Lee Scoresby in search of Lord Asriel.

Unaware that he is in mortal danger, Lord Asriel has set up a laboratory to investigate the glowing Dust from another world.

==Cast==
  (Dakota Blue Richards|Richards) with Pantalaimon (cat form, voiced by Freddie Highmore) and Mrs. Coulter (Nicole Kidman|Kidman) with her "Golden Monkey" dæmon. Scene filmed in the grand hall of Hedsor House, Buckinghamshire, UK.]]
* Dakota Blue Richards as Lyra Belacqua, who embarks on a voyage to battle the forces of evil and rescue her best friend. New Line Cinema announced 12-year-old Richards casting in June 2006. She had attended an open audition after watching a stage production of His Dark Materials,    and was picked from 10,000 girls who auditioned, for what was her first acting role. 
* Freddie Highmore as the voice of Pantalaimon, Lyras Dæmon (His Dark Materials)|dæmon. Pan was originally to be voiced by an older actor, but they called in Highmore instead, as it would be more of an intimate relationship if Pan and Lyra were the same age, and also would underscore the contrast between Lyras relationship with him versus her relationships with older male characters such as Lord Asriel, Lee Scoresby, and Iorek.
* Nicole Kidman as Mrs. Coulter, an influential woman who takes an interest in Lyra (and later admits that she is Lyras mother). Kidman was author Philip Pullmans preferred choice for the role ten years before production of the film,  and despite initially rejecting the offer to star as she did not want to play a villain, she signed on after receiving a personal letter from Pullman. 
* Daniel Craig as Lord Asriel, Lyras strict and mysterious adventurer uncle (later revealed to be her father). In July 2006, it was reported that Paul Bettany was in talks to play the role.  Texan aeronaut who comes to Lyras aid. Pullman has singled out Elliotts performance as one the film got "just right".  Sir Ian McKellen as the voice of Iorek Byrnison, a panserbjørn (armored bear) who becomes Lyras friend and comrade. Nonso Anozie had recorded lines for the part of Iorek Byrnison, but was replaced by McKellen at a late stage as New Line wanted a bigger name in the role.    New Line president of production Toby Emmerich admitted he "never thought Anozie sounded like Iorek" and while he initially trusted director Weitzs casting decision, he "never stopped thinking that this guy didnt sound right." The recasting was against Weitzs wishes, though he later said "if youre going to have anyone recast in your movie, youre happy its Ian McKellen." 
* Eva Green as Serafina Pekkala, a witch queen. In an interview, Green has described her character and her looks: "I wanted to look a bit like from the Waterhouse painting. You know, quite pre-Raphaelite. And also shes barefoot, quite ethereal, and the materials quite translucent and see-through, because witches dont feel the cold. If they were going to wear heavy clothing it would prevent them from sensing the world around them. Theyre very close to nature."  Ragnar Sturlusson, king of the panserbjørner. Ragnars name in the book was Iofur Raknison, but the name was changed to prevent confusion between him and Iorek.  However, in the German-language version of the film, the dialogue retains the name Iofur Raknison, whilst the subtitles reflect the change.
* Kathy Bates as the voice of List of His Dark Materials characters#Lee Scoresby|Hester, Lee Scoresbys jackrabbit dæmon.
* Kristin Scott Thomas as the voice of Stelmaria, Lord Asriels dæmon.
* Ben Walker as Roger Parslow, Lyras best friend, who is kidnapped and taken north. Jim Carter as John Faa, the king of the Gyptians. Sir Tom Courtenay as Farder Coram, Gyptian second-in-command and advisor to John Faa. Sir Christopher Lee as the Magisteriums first high councilor. Lees casting was also at New Lines behest, rather than that of Chris Weitz. 
* Edward de Souza as the Magisteriums second high councilor.
* Simon McBurney as Fra Pavel Jack Shepherd as master of Jordan College.
* Magda Szubanski as Mrs. Lonsdale Sir Derek Jacobi as the Magisterial emissary. Clare Higgins Ma Costa, Gyptian family that aids Lyra. Billy Costa, son of Ma Costa, and Lyras friend.

==Production==

===Development===
{| class="toccolours" style="float: right; margin-left: 1em; margin-right: 2em; font-size: 85%; background:#c6dbf7; color:black; width:30em; max-width: 40%;" cellspacing="5"
| style="text-align: left;" | "Peters operation was so impressive that, well, I realized the distance between me and Peter Jackson... At that moment, I realized the sheer scope of the endeavor. And I thought, You know what? I cant do this."
|-
| style="text-align: left;" |&nbsp;— Director Chris Weitz on his initial departure from the project 
|}
 The Lord King Kong in order to gather information on directing a blockbuster film, and to receive advice on dealing with New Line Cinema, for whom Jackson had worked on Lord of the Rings. After a subsequent interview in which Weitz said the novels attacks on organised religion would have to be softened, he was criticised by some fans,  and on December 15, 2004, Weitz announced his resignation as director of the trilogy, citing the enormous technical challenges of the epic.  He later indicated that he had envisioned the possibility of being denounced by both the books fans and its detractors, as well as a studio hoping for another Lord of the Rings. 

On August 9, 2005, it was announced that British director Anand Tucker would take over from Weitz. Tucker felt the film would thematically be about Lyra "looking for a family",  and Pullman agreed: "He has plenty of very good ideas, and he isnt daunted by the technical challenges. But the best thing from the point of view of all who care about the story is his awareness that it isnt about computer graphics; it isnt about fantastic adventures in amazing-looking worlds; its about Lyra."    Tucker resigned on May 8, 2006, citing creative disagreements with New Line, and Weitz returned to direct.  Weitz said "Im both the first and third director on the film&nbsp;..but I did a lot of growing in the interim." 

According to producer Deborah Forte, Tucker wanted to make a smaller, less exciting film than New Line wanted. New Line production president Toby Emmerich said of Weitzs return: "I think Chris realized that if he didn’t come back in and step up, maybe the movie wasn’t going to get made&nbsp;... We really didn’t have a Plan B at that point."  Weitz was attracted back to the project after receiving a letter from Pullman asking him to reconsider . Since his departure, blueprints, production design and visual effects strategies had been put into position, and while Weitz admitted that his fears did not vanish, the project suddenly seemed feasible for the director. 

===Filming===
  and her dæmon" (Dakota Blue Richards|Richards, right, with Freddie Highmore|Highmore, as Pantalaimon) record dialogue in post-production.]] Old Royal The Historic Dockyard Chatham  and Hedsor House in Buckinghamshire.

===Design===
Production Designer Dennis Gassner says of his work on the film: “The whole project is about translation&nbsp;– translation from something you would understand into something that is in a different vernacular. So, it’s a new signature, looking into another world that seems familiar but is still unique. There’s a term I use&nbsp;– called cludging&nbsp;– it’s taking one element and combining it with another element to make something new. It’s a hybrid or amalgamation, and that’s what this movie is about from a design perspective. It’s about amalgamating ideas and concepts and theoretical and physical environments.” 

Rhythm and Hues Studios created the main dæmons, and Framestore CFC created all the bears.  British company Cinesite created the secondary dæmons. 

===Music===
Alexandre Desplat composed the soundtrack to the film. Music icon Kate Bush recorded the track Lyra which plays over the end credits. 

==Differences from the novel==
Numerous scenes from the novel did not feature in the film or were markedly changed. On December 7, 2007, New York Magazine reviewed draft scripts from both Stoppard and Weitz; both were significantly longer than the final version, and Weitzs draft (which, unlike Stoppards, did not feature significant additions to the source material) was pronounced the best of the three. The magazine concluded that instead of a "likely three hours of running time" that included such scenes as Mrs. Coulters London party and Lyras meeting with a witch representative, the studio had opted for a "failed" length of under two hours in order to maximise revenue.   

On October 9, 2007, Weitz revealed that the final three chapters from Northern Lights had been moved to the films potential sequel, The Subtle Knife, in order to provide "the most promising conclusion to the first film and the best possible beginning to the second",    though he also said less than a month later that there had been "tremendous marketing pressure" to create "an upbeat ending".  (The   audience that the film had been "recut by  , and my experience with it ended being quite a terrible one”;  he also told Time.com that he had felt that by "being faithful to the book I was working at odds with the studio".    In 2011, Pullman told an audience at the British Humanist Association annual conference that he was also disappointed by this decision, and hoped that a directors cut of the film would be released some day including the footage cut by New Line.

In the book the Jordan College Master reluctantly poisons Lord Asriels wine; in the film a visiting Magisterium official undertakes (more willingly) this action. The alethiometer is mentioned multiple times throughout the film as a "golden compass".

Tasha Robinson of The A.V. Club argued that through the use of a spoken introduction and other exposition-filled dialogue, the film fails by "baldly revealing up front everything that the novel is trying to get you to wonder about and to explore slowly".  Youyoung Lee wrote in a December 2007 Entertainment Weekly that the film "leaves out the gore", such as the books ritualistic heart-eating that concludes the bear fight, "to create family-friendlier fare".  Lee also said that the film "downplays the Magisteriums religious nature", but Robinson argued that the depiction of the Church in the film is as "a hierarchical organization of formally robed, iconography-heavy priests who dictate and define morality for their followers, are based out of cathedrals, and decry teachings counter to theirs as heresy....doing ugly things to children under cover of secrecy". Robinson rhetorically then asks, "Who are most people going to think of besides the Catholic Church?"  The film does show more prominently scenes from the perspective of Magisterium officials than the novel. The novel never explicitly mentions the Magisteriums intentions except through the rumours and gossip of others, and through comments made by the character of Mrs. Coulter.

Although the character of Mrs. Coulter has black hair in the novel, Pullman responded to the blonde Kidmans portrayal by saying "I was clearly wrong. You sometimes are wrong about your characters. Shes blonde. She has to be."   

==Controversies==
 . ]]

Several key themes of the novels, such as the rejection of religion and the abuse of power in a fictionalised version of the Christian Church|Church, were diluted in the adaptation. Director Weitz said "in the books the Magisterium is a version of the Catholic Church gone wildly astray from its roots," but that the organization portrayed in his film would not directly match that of Pullmans books. Instead, the Magisterium represents all dogmatic organizations.  Weitz said that New Line Cinema had feared the storys anti-religious themes would make the film financially unviable in the U.S., and so religion and God ("the Authority" in the books) would not be referenced directly.

Attempting to reassure fans of the novels, Weitz said that religion would instead appear in euphemistic terms, yet the decision was criticised by some fans,  anti-censorship groups, and the National Secular Society (of which Pullman is an honorary associate), which said "they are taking the heart out of it, losing the point of it, castrating it..."    and "this is part of a long-term problem over freedom of speech." The Atlantic Monthly said also "With $180 million at stake, the studio opted to kidnap the book’s body and leave behind its soul."    The changes from the novel have been present since Tom Stoppards rejected version of the script,  and Pullman expected the film to be "faithful,"    although he also said, "They do know where to put the theology and that’s off the film."  A Christianity Today review of the film noted that "Magisterium does refer, in the real world, to the teaching authority of the Roman Catholic Church, and the film   peppered with religiously significant words like oblation and heresy", adding that when one character smashes through the wall of a Magisterium building, the damaged exterior is "decorated with   Byzantine icons."   
 Catholic League called for a boycott of the film.    League president William A. Donohue said he would not ordinarily object to the film, but that while the religious elements are diluted from the source material, the film will encourage children to read the novels, which he says denigrate Christianity and promote atheism for children.    He cited Pullman telling the Washington Post in 2001 that he is trying to undermine the basis of Christian belief.  The League hoped that "the film   to meet box office expectations and that   books attract few buyers,"  declaring the boycott campaign a success after a North American opening weekend which was lower than anticipated.  One week after the films release, Roger Ebert said of the campaign, "any bad buzz on a family film can be mortal, and that seems to have been the case this time." 
 The Christian Roman Catholic Church in England & Wales.  Some religious scholars have challenged the view that the story carries atheistic themes,   while in November 2007, a review of the film by the director and staff reviewer of the United States Conference of Catholic Bishops Office for Film and Broadcasting appeared on the website of the Catholic News Service and in Catholic newspapers across the country. The review suggested that instead of a boycott, it may be appropriate for Catholic parents to "talk through any thorny philosophical issues" with their children.  However, on December 10, 2007 the review was removed from the website at the USCCBs request. {{cite web | url=http://newshub.cnslis.com/2007/12/10/usccb-withdraws-review-of-the-golden-compass/ | title= Vatican newspaper, LOsservatore Romano, published an editorial in which it denounced the film as godless. 

Pullman said of Donohues call for a boycott, "Why dont we trust readers? Why dont we trust filmgoers? Oh, it causes me to shake my head with sorrow that such nitwits could be loose in the world."    In a discussion with Donohue on CBSs Early Show, Ellen Johnson, president of American Atheists, said that rather than promote atheism, the film would encourage children to question authority, saying that would not be a bad thing for children to learn.  Director Weitz says that he believes His Dark Materials is "not an atheistic work, but a highly spiritual and reverent piece of writing",    and Nicole Kidman defended her decision to star in the film, saying that "I wouldnt be able to do this film if I thought it were at all anti-Catholic".   
Some commentators indicated that they believed both sides criticism would prove ultimately impotent and that the negative publicity would prove a boon for the films box office.    Nonetheless, the trilogy has not been continued, prompting actor Sam Elliot to blame censorship and the Catholic church. 

==Reception==

===Box office===
The North American opening weekend return was "a little disappointing" for New Line Cinema,  earning US$25.8 million with total domestic box office of $70 million compared to an estimated $180 million production budget.    Despite this, the films loss rebounded as its performance outside the United States was described as "stellar" by Variety (magazine)|Variety,  and as "astonishing" by New Line.  In the United Kingdom, the film grossed $53,198,635 and became the second highest grossing non-sequel of 2007 there (behind The Simpsons Movie). In Japan, the film was officially released in March 2008 on 700 screens, ultimately grossing $33,501,399; but previews of the film between February 23–24, 2008 earned $2.5 million. By July 6, 2008, it had earned $302,127,136 internationally, totaling $372,234,864 worldwide.  Overseas rights to the film were sold to fund the $180 million production budget for the film, so most of these profits did not go to New Line.  This has been cited as a potential "last straw" in Time Warners decision to merge New Line Cinema into Warner Bros Pictures. 

===Critical response===
  average score of 51, based on 33 reviews. 

Manohla Dargis of The New York Times said that the film "crams so many events, characters,   twists and turns, sumptuously appointed rooms and ethereally strange vistas   that   risks losing you in the whirl" and that while The Golden Compass is "an honorable work," it is "hampered by its fealty to the book and its madly rushed pace."  James Berardinelli of ReelReviews gave the film 2½ stars out of 4, calling it "adequate but not inspired" and criticising the first hour for its rushed pace and sketchily-developed characters.  James Christopher of The Times was disappointed, praising the "marvellous" special effects and casting, but saying that the "books weave a magic the film simply cannot match" and citing a "lack of genuine drama." 
 The Chronicles of Narnia or the Harry Potter film series|Potter films," saying that it "creates villains that are more complex and poses more intriguing questions. As a visual experience, it is superb. As an escapist fantasy, it is challenging   I think   is a wonderfully good-looking movie, with exciting passages and a captivating heroine." 

Pullman himself was described by a Times interviewer as sounding "ambivalent" and "guarded" about the film, saying in March 2008: "A lot of things about it were good... Nothings perfect. Nothing can bring out all thats in the book. There are always compromises". He hoped, however, that the rest of the trilogy would be adapted with the same cast and crew.  In July 2009, after this possibility had been exhausted, Weitz told Time.com that he thought the films special effects ended up being its "most successful element." 

At the British Humanist Association conference in 2011, Pullman disclosed that there were not going to be any more films with the same cast and crew. On reflection, while praising the portrayals of his characters by Nicole Kidman and Dakota Blue Richards, Pullman said that he was disappointed with the end film and New Lines re-editing of it.

===Accolades===
  Special Visual Best Visual Effects notably beating what many considered to be the front runner, Michael Bays Transformers (film)|Transformers, which had swept the Visual Effects Society awards prior.  It was also nominated for two Critics Choice Movie Awards|Critics Choice Awards in 2007 ("Best Family Film," and "Best Young Actress" for Dakota Blue Richards ), five Satellite Awards, and the Hugo Award for Best Dramatic Presentation, Long Form. The Golden Compass was nominated for the National Movie Award for Best Family movie but lost to Disney/Pixars WALL-E.

==Home media==
The film was released on DVD and Blu-ray Disc formats in the United Kingdom on April 28, 2008 and the United States on April 29, 2008. The movie is closed-captioned by the National Captioning Institute on its first Home Video release.  The extra material on the single-disc DVD consists of previews of upcoming New Line Cinema films. The two-disc edition includes a commentary from writer/director Chris Weitz, eleven "making-of" featurettes, a photo gallery, and theatrical and teaser trailers. The Blu-ray disc features the same extras from the two-disc DVD edition.  Exclusive to Blu-ray Disc is Visual Commentary Picture-in-Picture feature which enables users to view behind the scene feature while watching the movie.

Shortly before the films release, Weitz suggested that an extended cut of the film could be released on DVD, saying "Id really love to do a fuller cut of the film"; he further speculated that such a version "could probably end up at two and a half hours."  This proposed cut would presumably not include the original ending: MTV reported in December 2007 that Weitz hoped to include that material at the beginning of a possible The Subtle Knife adaptation, and that a Compass Directors Cut might feature "a moment" of it as a "teaser".    Cast members Craig and Green have echoed this hope for such a DVD
cut; so far, however, no official announcement has been made. 

==Video game==
 
The video game for this film was released in November 2007 in Europe and December 2007 in North America and Australia for the Personal computer|PC, Wii, PlayStation 2, PlayStation 3, PlayStation Portable, Nintendo DS, and the Xbox 360. It was developed by Shiny Entertainment and published by Sega. 

Players take control of the characters Lyra Belacqua and Iorek Byrnison in Lyras attempt to save her friend Roger from the General Oblation Board. As this game does not fully take into account the changes made by the final version of the film, a small amount of footage from the films deleted ending can be viewed near the end of the game, and the order in which Lyra travels to Bolvangar and Svalbard follows the book and not the film.

==Sequels==
At the time of The Golden Compass theatrical release, Chris Weitz pledged to "protect   integrity" of the prospective sequels by being "much less compromising" in the book-to-film adaptation process.    New Line Cinema commissioned Hossein Amini to write a screenplay based on the second book in the trilogy, The Subtle Knife, potentially for release in 2010 or 2011, with the third book of the trilogy, The Amber Spyglass, to follow. However, New Line president Toby Emmerich stressed that production of the second and third films was dependent on the financial success of The Golden Compass.  When The Golden Compass did not meet expectations at the United States box office, the likelihood of a sequel was downplayed by New Line. According to studio co-head Michael Lynne, "The jury is still very much out on the movie, and while its performed very strongly overseas well look at it early 2008 and see where were going with a sequel." 
 uncertain economic climate may have contributed to their cancellation. {{cite news | url=http://www.independent.co.uk/arts-entertainment/films/news/can-fantasy-epics-survive-the-credit-crunch-chronicles-1213848.html | title=
Can fantasy epics survive the Credit Crunch Chronicles? | author=Guy Adams |work=The Independent  | date=December 28, 2008 | accessdate=January 1, 2009 | location=London}} 

By October 2008, the two planned sequels were officially placed on hold, according to New Line Cinema, because of financial concerns during the global recession.  Sam Elliott, however, stated, "The Catholic Church ... lambasted them, and I think it scared New Line off."  In 2011, Philip Pullman remarked at the British Humanist Association annual conference that there were not going to be any more films from the series made with the same cast. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  
*   by The Onion A.V. Club
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 