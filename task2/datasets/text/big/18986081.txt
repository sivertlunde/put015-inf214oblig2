Madame Butterfly (1915 film)
 

{{Infobox film
 | name = Madame Butterfly
 | image = Madame Butterfly 1915 poster.jpg
 | caption = Theatrical poster
 | director = Sidney Olcott
 | writer = John Luther Long (novel)
 | starring = Mary Pickford
 | producer =
 | distributor = Paramount Pictures
 | budget =
 | released = November 7, 1915
 | country = United States English intertitles
 | runtime =
 }}
  1915 silent film directed by Sidney Olcott. The film is based upon a John Luther Long novel and the opera Madama Butterfly.

==Production==
Reportedly, leading actress Mary Pickford fought constantly with Sidney Olcott about the character. Olcott wanted Pickford to be more reserved and thought she was "too Americanized to play a Japanese". 

==Plot==
The film takes place in Japan in 1904. Lieutenant Pinkerton (Marshall Neilan) marries Cho-Cho-San Butterfly (Mary Pickford), a 15-year-old Japanese geisha. Cho-Cho-San is lucky with her new husband and takes the marriage very seriously. Pinkterton, however, regards it as entertainment. He is not in love with her and plans to break off the wedding in a month. The American Consul (William T. Carleton)  begs him to break off the wedding as soon as possible, to avoid hurting her feelings. The lieutenant laughs him off.

After Pinkerton forces Cho-Cho-San to end their wedding reception early, her disapproving family disowns her. When Pinkerton is ordered to return to America, he promises Cho-Cho-San he will return before he leaves. Three years go by. Cho-Cho-San, now a mother, still believes Pinkerton will return someday, while he is engaged to an American woman. He sends her a letter to announce he will marry another woman, but Cho-Cho-San cant read.

Meanwhile, The Prince of Japan (David Burton) takes interest in Cho-Cho-San, but she refuses his company and claims she is still waiting for her husband. Sometime later, Pinkerton returns to Japan but he hands the American Consul some money as compensation for Cho-Cho-San and leaves again. When Cho-Cho-San comes to ask about her husband, she runs into Pinkertons new American wife. The American woman asks Cho-Cho-San to give them her child, as he will be given better opportunities and prosperity under their parenting. Cho-Cho-San is crushed but complies and hands over her child. She kills herself in the final scene by walking into a river and drowning.

==Cast==

* Mary Pickford - Cho-Cho-San
* Marshall Neilan - Lieutenant Pinkerton
* Olive West - Suzuki
* Jane Hall - Adelaide
* Lawrence Wood - Cho-Cho-Sans father
* Caroline Harris - Cho-Cho-Sans mother
* M.W. Rale - The Nakodo
* William T. Carleton - The American Consul
* David Burton - The Prince
* Cesare Gravina - The Soothsayer
* Frank Dekum - Naval officer

==References==
 

==External links==
* 
* 
*     website dedicated to  Sidney Olcott

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 