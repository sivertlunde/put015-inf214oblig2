Prem Nagar (1974 film)
::This article is about the film, for other uses see Prem Nagar (disambiguation).
{{Infobox Film
| name           = Prem Nagar
| image          = Prem Nagar 1974 Film Poster.JPG
| caption        = Film poster
| director       = K.S. Prakash Rao
| producer       = D. Rama Naidu
| writer         = Inder Raj Anand Koduri Kausalya Devi K.S. Prakash Rao Raj Baldev Raj
| narrator       = 
| starring       = Rajesh Khanna Hema Malini Prem Chopra Asrani Nazir Hussain
| music          = Sachin Dev Burman
| cinematography = A. Vincent
| editing        = K.A. Marthand J. Narasimha Rao
| studio         = Suresh Productions Vauhini Studios Vijaya Studios
| distributor    = Vijay and Suresh Combines Video Sound
| released       = 24 May 1974
| runtime        =  158 minutes
| country        = India
| language       = Hindi
| budget         = 
}}

Premnagar or Prem Nagar (City of Love) is a 1974 Hindi drama film produced by D. Ramanaidu and directed by K.S. Prakash Rao. It stars Rajesh Khanna, Hema Malini, Aruna Irani, Ashok Kumar, Kamini Kaushal, Prem Chopra, Asrani and Jagdeep. The music is by Sachin Dev Burman. Box Office India declared the film a hit.  
 Prem Nagar released in 1971. In Tamil the pair was played by Sivaji Ganesan and Vanisri and in Telugu Akkineni Nageswara Rao and Vanisree as well. It ran for nearly 750 days in the theatres. Though the movie is remake of original Telugu version, some few scenes which were not in Telugu but in Tamil version was remade in Hindi also.

== Plot ==
Karan Singh lives a wealthy lifestyle in a palace along with his widowed mother, an elder brother, Shamsher, his wife and daughter, Meena. His mother, Rani Maa, spent most of her time playing cards and left his upbringing to his nanny. As a result, Karan ended up believing her to be his real mother. Now matured, a womanizer and alcoholic, he comes to the rescue of a former air hostess, Lata, who is being molested by her boss, hires her as his secretary, and permits her parents, sister and brother, to move into one of his cottages. Lata attempts to change his bad habits, initially meets with opposition, but eventually succeeds; they fall in love. Karan even builds a palace and names it Prem Nagar. Their idyllic romance is shattered when Lata is accused of interfering in palace affairs and stealing a valuable necklace—with her mother testifying that she found it on Latas purse and, as a result, a much-humiliated Lata quits and her family moves out.

In the meantime Karan found out the truth that Lata was victim of his mothers conspiracy to push her out of his life to maintain family prestige, as he was supposed to date a girl of his status and background. Karan confronts his mother and elder brother and leaves home in frustration. He also clears the matter with Lata and asks for pardon for his family and himself. Deeply hurt, Lata tells him that it is not his familys accusation but his distrust hurt her most and she just wants to go away for sake of her self-esteem. All efforts of Karan to reconcile failed and he went into deep agony. Later during an medical test it is revealed that Karans health is seriously affected as he suddenly stopped his long tern alcohol addiction and doctor advised him to continue it in lesser quantity as medicine, but Karan cannot obliged due to his promise to Lata to never touch wine again.  As result his health deterioted further, Lata shows up who overheard doctors advice and give him a glass of wine, Karan refused as he thinks he has no reason to live on, Lata left the palace helpless. Lata decided to marry someone else so that Karan can forget her and return to normal life. In wedding day, Karan visits her for last time with tearful eyes and they separated their ways, overlooked by her to be mother in law. Taking it as issue of her character, they cancelled the marriage. Lata realised that her ego destroying many lives, as she about to abandon her wedding sari, Karan mother shows up and accepted her as daughter in law. In palace, Karan devastated by her loss about to consume poison when she intervened and asked him for pardon and they lovingly hug.

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Yeh Thandi Hawayen"
| Kishore Kumar, Asha Bhosle
|-
| 2
| "Kiska Mahal Hai Kiska Yeh Ghar Hai"
| Kishore Kumar, Lata Mangeshkar
|-
| 3
| "Yeh Lal Rang Kab Mujhe Chhodega"
| Kishore Kumar
|-
| 4
| "Yeh Kaisa Sur Mandir"
| Lata Mangeshkar
|-
| 5
| "Jaa Jaa Jaa"
| Kishore Kumar
|-
| 6
| "Ek Maumma Hai"
| Kishore Kumar, Rajesh Khanna
|-
| 7
| "Bye Bye Miss Good Night"
| Kishore Kumar
|-
| 8
| "Pyase Do Badan Pyasi Raat Mein"
| Asha Bhosle
|}
==Awards and nominations==
*Filmfare Best Cinematographer Award &mdash; A. Vincent
*Filmfare Nomination for Best Actor &mdash; Rajesh Khanna
*Filmfare Nomination for Best Actress &mdash; Hema Malini
*Filmfare Nomination for Best Music &mdash; Sachin Dev Burman 

==References==
 

== External links ==
*  

 
 
 
 
 

 