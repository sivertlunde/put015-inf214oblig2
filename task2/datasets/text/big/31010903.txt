Walking My Baby Back Home (film)
 
{{Infobox Film
| name           = Walking My Baby Back Home
| image_size     = 
| image	=	Walking My Baby Back Home FilmPoster.jpeg
| caption        = 
| director       = Lloyd Bacon
| producer       = Ted Richmond Leonard Goldstein (co-producer)
| writer         = Oscar Brodney Don McGuire
| narrator       = 
| starring       = Donald OConnor Janet Leigh Buddy Hackett
| music          = Henry Mancini (uncredited)
| cinematography = 
| editing        = 
| studio         = Universal-International
| distributor    = 
| released       = December 6, 1953
| runtime        = 95 minutes
| country        = United States
| language       = English
}}

Walking My Baby Back Home is a 1953 American musical comedy film starring Donald OConnor, Janet Leigh, and Buddy Hackett. This was Hacketts film debut.
 Forgotten Lady where Janet Leigh plays an aging film star who nostalgically watches the film.

==Cast==

*Donald OConnor as Clarence "Jigger" Miller
*Janet Leigh as Chris Hall
*Buddy Hackett as Blimp Edwards
*Lori Nelson as Claire Millard
*Scatman Crothers as "Smiley" Gordon
*Kathleen Lockhart as Mrs. Millard
*George Cleveland as Col. Dan Wallace John Hubbard as Rodney Millard
*Norman Abbott as Doc
*Phil Garris as Hank
*Walter Kingsford as Uncle Henry Hall
*Sidney Miller as Walter Thomas
*The Modernaires as themselves
*The Sportsmen Quartet as themselves (credited as The Sportsmen)

==External links==
* 
* 
* 

 

 
 
 
 


 