The Lady Has Plans
{{multiple issues|
 
 
}}
{{Infobox film
| name           = The Lady Has Plans
| image          = 
| image_size     = 
| border         = 
| caption        = 
| director       = Sidney Lanfield
| producer       = Fred Kohlmar
| screenplay     = Harry Tugend
| starring       = {{Plainlist|
* Ray Milland
* Paulette Goddard
}}
| music          = {{Plainlist|
* Leigh Harline
* Leo Shuken
}}
| cinematography = Charles Lang William Shea
| studio         = Paramount Pictures
| distributor    = Paramount Pictures
| released       =  
| runtime        = 77 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
 American Thriller thriller comedy film starring Ray Milland and Paulette Goddard.  , imdb. 

== Cast ==
* Ray Milland as Kenneth Clarence Harper
* Paulette Goddard as Sidney Royce
* Roland Young as Ronald Dean
* Albert Dekker as Baron Von Kemp 
* Margaret Hayes as Rita Lenox
* Cecil Kellaway as Peter Miles
* Addison Richards as Paul Baker
* Edward Norris as Frank Richards
* Charles Arnt as Pooly

== Plot ==
A gang of criminals steal some top secret plans and tattoo them in invisible ink on the back of a beautiful young girl, planning to sell them to the highest bidder in Lisbon.

== Adaptations ==
The Lady Has Plans was adapted for The Lux Radio Theatre, with William Powell and Rita Hayworth replacing Milland and Goddard in the title roles. It was aired on April 26, 1943.  , The Lady Has Plans by Lux Radio Theatre. 

== References ==
 

 

 