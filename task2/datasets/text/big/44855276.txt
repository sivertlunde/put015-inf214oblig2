Spotkanie w "Bajce"
{{Infobox film
| name           = Spotkanie w "Bajce" 
| image          = 
| caption        = 
| director       = Jan Rybkowski
| producer       = Film Polski
| writer         = Jan Rybkowski Michael Tonecki
| starring       = Aleksandra Slaska Gustaw Holoubek Andrzej Lapicki
| music          = Wojciech Kilar
| cinematography = Mieczyslaw Jahoda
| editing        = 
| distributor    = 
| released       =  
| runtime        = 72 minutes
| country        = Poland
| language       = Polish
| budget         = 
}} Polish psychological drama film directed by Jan Rybkowski.    Also known as Meeting in the Fable,  it was produced by Film Polski, and starred Aleksandra Slaska, Gustaw Holoubek, Andrzej Lapicki and Teresa Izewska.    The writers Jan Rybkowski and Michal Tonecki developed it from a story Tonecki had originally written as a radio play. 

Set in post-war Poland, the story is a marital triangle involving Victor a famous pianist, Teresa, and her husband Doctor Paul from whom she is separated. The three meet after fifteen years in a café called Fable (Bajce). The film was entered in the 3rd International Film Festival of India held in Delhi, India, from January 8-21 1965.   

==Cast==
* Aleksandra Slaska as Teresa
* Gustaw Holoubek as Paul
* Andrzej Lapicki as Victor
* Teresa Izewska as Eve
* Maria Wachowiak - waitress
* Mieczyslaw Pawlikowski - President of MRN
* Mieczyslaw Fogiel - MRN employee
* Beata Barszczewska as Christie, daughter Teresa
* Magdalena Zawadzka - the daughter of the President of the MRN
* Stanislaw Niwiński - truck driver

==References==
 

==External links==
*  

 
 
 
 
 
 
 
 