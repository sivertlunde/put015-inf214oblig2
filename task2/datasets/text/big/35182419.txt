Wonderful Radio
 
{{Infobox film
| name           = Wonderful Radio
| image          = Wonderful Radio-poster.jpg
| caption        = Promotional poster for Wonderful Radio
| director       = Kwon Chil-in
| producer       = Jung Kyoung-il
| writer         = Lee Jae-ik
| starring       = Lee Min-jung Lee Jung-jin
| film name      = {{Film name
| hangul         =    
| rr             = Wondeopul Radio
| mr             = Wŏndŏp‘ul Radio}}
| music          = 
| cinematography = Choi Ju-young
| editing        = Shin Min-kyung
| distributor    = Showbox/Mediaplex
| released       =  
| runtime        = 120 minutes
| country        = South Korea
| language       = Korean
| budget         = 
| gross          =   
}}
 
 South Korean DJ struggling to boost listener ratings. It was released in theaters on January 5, 2012 by distributor Showbox/Mediaplex, and ran for 120 minutes.

==Plot== SBS station Power FM while also trying to restart her career as a solo singer-composer under her goofy manager Cha Dae-geun (Lee Kwang-soo). However, the ratings have been dropping, so when the shows producer leaves to have a baby, station manager Im (Kim Byung-ok) assigns the ambitious Lee Jae-hyeok (Lee Jung-jin) to her job with a brief to bring in some fresh ideas. Jae-hyeok is appalled by Jin-ahs casual, diva-ish work attitude and the two dont hit it off. When he challenges her to come up with a new segment for the show, she finally gets an idea from her restaurateur mother (Kim Hae-sook) in which members of the public come into the studio and sing a song to their loved ones. She calls the segment "The Song I Sing to You". The initial broadcast, involving a serving soldier, goes disastrously, but subsequent attempts, involving a brattish schoolgirl (Jo Jung-eun) and a taxi driver (Jung Man-shik), are a big success. Jin-ah and Jae-hyeok start to bond. However, In-seok (Kim Jung-tae), the manager of TV drama actress Yoon Mi-ra (Seo Young) and a major supplier of talent to the radio station, is plotting to bring Jin-ah down and replace her with Mi-ra, who still hates Jin-ah for leaving Purple at the height of its fame and causing the groups break-up.   

==Cast==
* Lee Min-jung - Shin Jin-ah
* Lee Jung-jin - Lee Jae-hyeok
* Lee Kwang-soo - Cha Dae-geun, Jin-ahs manager
* Kim Hae-sook - Mrs. Lee, Jin-ahs mother
* Kim Jung-tae - In-seok, head of Mi-ras agency
* Kim Byung-ok - Director Im, Power FM station manager
* Han Yeo-woon - Ji In-yeong
* Seo Young - Yoon Mi-ra Jeong Yu-mi - Nan-seol, show scriptwriter
* Jung Man-sik - Ji-hyeong, taxi driver
* Jo Jung-eun - Yoon Da-hee, schoolgirl
* Kim Tae-won - Music bar owner (cameo)
* Lee Seung-hwan - Dream Factory owner (cameo)
* Dal Shabet - Cobi Girls (cameo)
* Cul Two - guys in the elevator (cameo)
* Jung Yup - (cameo) Kim Jong-kook - taxi passenger (cameo) Gary - taxi passenger (cameo)

==Production==
The director of Wonderful Radio is Kwon Chil-in, who previously directed Singles (2003 film)|Singles (2003), Hellcats (film)|Hellcats (2008) and Loveholic (2010). The film’s script was written by Lee Jae-ik, producer of SBS Power FM’s "Cultwo Show," who incorporated his own experiences in the movie, thereby giving viewers a realistic glimpse at the behind-the-scenes workings of a radio program. 

Lee Min-jung said she enjoyed playing both a singer and radio DJ, basing her performance on popular idol groups in her schooldays such as S.E.S. (band)|S.E.S. and Fin.K.L.     She also said she drew inspiration from her own childhood experience as an avid fan of radio shows, when she would laugh and cry at hearing peoples stories broadcast over the airwaves.  

==Soundtrack==
The instrumental score was composed by Jung Soo-min and Hwang Seong-je. Along with various K-pop artists, the films lead actors Lee Min-jung and Lee Jung-jin also contributed tracks.

# Wonderful Radio (Inst.) - Jung Soo-min
# Again - Lee Min-jung
# 참쓰다 "Write the truth" - Lee Min-jung
# Youre My Angel - Lee Min-jung, Seo Young, Ahn Mi-na
# Get Out Boy - Cobi Girls (Dal Shabet)
# Thats When I Feel Love - Seo Mi-rae
# Black Star - Yoo Keun-ho
# Stay - Kim Kyu-won
# Blind By Love - Kim Ho-yeon
# 사랑의 시작은 고백에서부터 "Love begins with a confession" - Lee Jung-jin
# The Moment (Inst.) - Hwang Seong-je
# 위로 "Comfort" (Inst.) - Hwang Seong-je
# 지각 "Late" - Hwang Seong-je
# 4% (Inst.) - Hwang Seong-je
# 글래머 인영 "Glamorous In-yeong" (Inst.)
# Reminiscence (Inst.) - Jung Soo-min
# 계절이 바뀌며... "Changing seasons" (Inst.) - Hwang Seong-je
# 아직도 기다려요 "Still wating" (Inst.) - Jung Soo-min
# 1부 Opening (Inst.) - Hwang Seong-je
# Ending Signal (Inst.) - Hwang Seong-je
# 전화통화 "Phone call" (Inst.) - Jung Soo-min
# 마지막 콘서트 "Last concert" (Inst.) - Hwang Seong-je
# 로비에서 "In the lobby" (Inst.) - Hwang Seong-je, Lee Seung-hwan, Jung Soo-min
# Namaste (Inst.) - Jung Soo-min
# 그대에게 부르는 노래 "The song I sing to you" (Inst.) - Jung Soo-min
# 미라라... "Mirara" (Inst.) - Jung Soo-min
# 2부 Opening (Inst.) - Jung Soo-min
# 뚬바뚬바 메들리 "Ttumba Ttumba medley" - Kim Kyu-won
# Island - Lee Do-woo

==Reception== Dancing Queen and Unbowed to achieve one million admissions in 2012.  It ranked second and grossed   in its first week of release  and grossed a total of   domestically after three weeks of screening. 
 48th Baeksang Arts Awards in 2012.

==References==
 

==External links==
*    
*  
*  
*  

 
 
 
 
 