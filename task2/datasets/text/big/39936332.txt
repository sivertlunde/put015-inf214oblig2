Jingles the Clown
 
{{Infobox film
| name           = Jingles the Clown
| image          = JinglesTheClown.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = DVD released by R-Squared Films
| film name      = 
| director       = Tommy Brunswick
| producer       = Todd Brunwick   Tommy Brunswick
| writer         = Todd Brunswick
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = April Canning   Tevis Marcum   Virginia Bryant   Jim Lewis   Nigora Mirkhanova   Tim Kay   Marisa Stober   DaVaughn Lucas   Darrell M. Stavros    And John Anton 
| music          = Doug Kolbicz   James Souva   Michael Kudrieko
| cinematography = Michael Kudreiko
| editing        = Todd Brunswick
| studio         = T & T Productions   Big Bite Entertainment   Atomic Devil Entertainment
| distributor    = R-Squared Films
| released       =  
| runtime        = 83 minutes
| country        = United States
| language       = English
| budget         = $150,000
| gross          = 
}}
 reboot of the 2006 film Mr. Jingles, also created by the Brunswicks.

== Plot ==

Mr. Jingles, a serial killer and the co-host of a childrens television series, abducts the Nelson family, and forces nine-year-old Angela to watch as he films himself murdering her parents, and sister. The police arrive in time to save Angela, and they arrest Jingles, though the sheriff is so disgusted by the clowns actions that he executes Jingles. The seemingly dead Jingles is thrown into the trunk of a police cruiser, but as Angela is led away, he winks at her. The next day, the deputies who were going to dispose of Jingles are found butchered, and Jingles is assumed to have drowned in a pit while making his escape.
 pilot episode of the show, which will have Angela as a special guest. Two nights before Haunted Maniacs is set to shoot, Jingles kills a pair of teenagers trespassing on his property. The crew of Haunted Maniacs reaches the Jingles estate, and despite Angelas anxiety and finding evidence of the young couple murdered there earlier, set up as planned.

The crew explores the Jingles mansion, and discusses Jingless history, revealing he was the result of an incestuous relationship between his grandfather and mother, who murdered her sexually abusive parents with a cleaver. Guy, the psychic, goes off alone to smoke, and is killed by Jingles, an event which is felt by Marco, the Mediumship|medium. Marco warns the others about what he sensed, and as they go to leave, they run into David Hess, the caretaker. David tells the crew about the missing couple, the owners of the bloody objects they stumbled upon hours ago. The vehicles all fail to start, so David and Jimmy head to Davids house to call 911.

The ghost hunting equipment goes haywire, and carnival music emanates from the stables, so J.B. and Sam go to investigate, finding an old stereo, and David and Jimmys mutilated remains. The two get the others, and they all try to make a run for it, but retreat back to the manor when they spot Guys body on the path, tied to a cross and set ablaze by Jingles, who taunts Angela. Jingles sneaks into the house, and performs a dark ritual in the attic, while downstairs the ghost of Angelas sister, Amy, manifests. Amy explains that Jingles is powered by the souls of his victims being channeled through something, and is unstoppable as long as he remains on his property. Amy goes on to state that while she and the spirits of the others killed by Jingles can hold him off temporarily to allow the Haunted Maniacs crew to escape, Angela must remain behind.

Jingles fatally stabs Marco, electrocutes Sam as she tries to help Marco, and kills Tom with a pickaxe. The others remember what Amy said about Jingles receiving his power from something, and conclude that if they find and destroy the conduit, Jingles will become killable. Angela, J.B., and Mia go to look through Jingless belongings in the attic, while Miranda stays behind, and is beat to death with a mallet by Jingles. The remaining three reach the attic, where Jingles snaps J.B.s neck, and captures Angela and Mia. Jingles decapitates Mia, reveals to Angela that he was having an affair with Angelas mother and is her biological father, and forces Angela to remember that she sacrificed Amy to him, shoving her older sister into Jingless arms to save herself the night the rest of the Nelson family was slaughtered.
 CSI walks past with it.

== Cast ==

* John Anton as Mr. Jingles/Charles David Tanner
* April Canning as Angela Nelson
* Tevis R. Marcum as J.B.
* Virginia Negron Bryant as Miranda
* Jim Lewis as Tom Reeser
* Nigora Mirkhanova as Mia
* Tim Kay as Guy
* Marisa Ruby as Sam
* DaVaughn Lucas as Marco
* Darrell M. Stavros as Jimmy
* Bill Moore as David Hess
* Raffaele Recchia as Sheriff Miller
* Jason Hughley as Deputy Lewis
* Johnny Foz as Deputy Randy
* Ken Svoboda as Deputy Joe
* Dwayne Roszkowski as Mitchel Nelson
* Anna Recchia as Mary Nelson
* Alyssa J. Stavros as Amy Nelson
* Mallory Castor as Young Angela Nelson
* Courtney Benjamin as Deputy Connelly
* Monique Recchia-Castria as Deputy #1
* Rex Recchia as Deputy #2
* Tommy Brunswick as CSI Tech
* Adam Lorenz as Doug
* Shauna Bryn as Jenny
* Dayna Recchia as 911 Operator
* Raglan Brunswick as Tom Reesers Older Son
* Magnus Brunswick as Tom Reesers Younger Son
==Production==
 

==Release==
 
== Reception ==
  cheesy cornball fest" by Film Bizarro, which criticized the plot, lighting, and acting, but offered mild praise to the gore and violence.  A two out of four was awarded by Gut Munchers, which stated that while the film had a decent beginning, nice cinematography, and a slightly creepy (if over the top) villain, it suffered from bad Computer-generated imagery|CGI, uninspired kills, and an overall weak story. 

 
== References ==

 

== External links ==

*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 