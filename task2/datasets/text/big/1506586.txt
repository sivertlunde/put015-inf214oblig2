Body and Soul (1947 film)
{{Infobox film
| name           = Body and Soul
| image          = Body and Soul 1947 movie poster.jpg
| alt            =
| caption        = Theatrical release poster
| director       = Robert Rossen
| producer       = Bob Roberts
| screenplay     = Abraham Polonsky
| starring       = John Garfield Lilli Palmer Hazel Brooks Anne Revere William Conrad
| music          = Hugo Friedhofer
| cinematography = James Wong Howe Francis Lyon (sup) Robert Parrish
| studio         = The Enterprise Studios
| distributor    = United Artists
| released       =  
| runtime        = 104 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Body and Soul is a 1947 American film noir directed by Robert Rossen, and features John Garfield, Lilli Palmer, Hazel Brooks, Anne Revere and William Conrad.   The film, written by Abraham Polonsky, is considered the first great film about boxing; its also a cautionary tale about the lure of money—and how it can derail even a strong common man in his pursuit of success.

==Plot==
Charley Davis, against the wishes of his mother, becomes a boxer.  As he becomes more successful the fighter becomes surrounded by shady characters, including an unethical promoter named Roberts, who tempt the man with a number of vices.  Charley finds himself faced with increasingly difficult choices.

==Cast==
* John Garfield as Charlie Davis
* Lilli Palmer as Peg Born
* Hazel Brooks as Alice
* Anne Revere as Anna Davis
* William Conrad as Quinn
* Joseph Pevney as Shorty Polaski
* Lloyd Gough as Roberts
* Canada Lee as Ben Chaplin Art Smith as David Davis

==Reception==

===Critical response===
When the film was released, critic Bosley Crowther, praised the film, writing, ""Body and Soul has up and done it, with interest and excitement to spare, and we heartily recommend it in its present exhibition at the Globe ... Still   written his story with such flavor and such slashing fidelity to the cold and greedy nature of the fight game, and Robert Rossen has directed it with such an honest regard for human feelings and with such a searching and seeing camera, that any possible resemblance to other fight yarns, living or dead, may be gratefully allowed." 
 sociopolitical point of view and praised Garfields work.  He, wrote, "Robert Rossens Body and Soul becomes more than a boxing and film noir tale, as screenwriter Abraham Polonsky makes this into a socialist morality drama where the pursuit of money becomes the focus that derails the common man in his quest for success ... Garfield is seen as a victim of the ruthless capitalistic system that fixes everything including athletic events, as the little guy is always at the mercy of the big operator. Its the kind of liberalism that was common in the dramas made in the 1930s. Its more a film about corruption and the presence of violence everywhere in America rather than a straight boxing film ... Body and Soul viewed at this late date lacks much relevancy and now only seems gripping because of Garfields gritty performance, and not because of the intense script that once made waves in powerful circles. 

TV Guides review notes "The fight sequences, in particular, brought a kind of realism to the genre that had never before existed (James Wong Howe wore skates and rolled around the ring shooting the fight scenes with a hand-held camera). A knockout on all levels." 

===Awards=== Best Actor Best Writing, Original Screenplay. The film was voted as the Greatest Boxing Movie Ever in 2014 by the Houston Boxing Hall Of Fame.

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 