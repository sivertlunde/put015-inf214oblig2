The House (1997 film)
{{Infobox film
| name           = The House
| image          = 
| image_size     = 
| caption        = 
| director       = Šarūnas Bartas
| producer       = Paulo Branco
| writer         = Šarūnas Bartas Yekaterina Golubeva
| starring       = Valeria Bruni Tedeschi
| music          = 
| cinematography = Šarūnas Bartas Rimvydas Leipus
| editing        = Mingilié Murmulaitiné
| distributor    = 
| released       = 1 October 1997
| runtime        = 120 minutes
| country        = Lithuania France
| language       = Lithuanian, French
| budget         = 
}}

The House ( ) is a 1997 French-Lithuanian drama film directed by Šarūnas Bartas. It was screened in the Un Certain Regard section at the 1997 Cannes Film Festival.   

==Cast==
* Valeria Bruni Tedeschi
* Leos Carax
* Micaela Cardoso
* Oksana Chernych
* Alex Descas
* Egle Kuckaite
* Jean-Louis Loca
* Viktorija Nareiko
* Francisco Nascimento
* Eugenia Sulgaite
* Leonardas Zelcius
* Marija Olšauskaitė

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 