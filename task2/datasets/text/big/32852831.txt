Parlami d'amore
{{Infobox film
| name           = Parlami damore
| image          = Parlamidamoreposter.jpg
| caption        = Promotional poster
| director       = Silvio Muccino
| producer       = Marco Chimenz Giovanni Stabilini Riccardo Tozzi Francesca Longardi	
| writer         = Silvio Muccino Carla Vangelista
| starring       = Silvio Muccino Aitana Sánchez-Gijón Carolina Crescentini Andrea Guerra 	
| cinematography = Arnaldo Catinari		
| editing        = Patrizio Marone	 			
| distributor    = 
| released       = 14 February 2008 (Italy)
| runtime        = 115 minutes
| country        = Italy Spain
| language       = Italian
| budget         =
| gross          =
}}
Parlami damore (English: Speak to me of love) is a 2008 Italo-Spanish film directed by Silvio Muccino. The film is based on Muccinos novel of the same name that he co-wrote with Carla Vangelista. The film was nominated for 8 David di Donatello awards. The film was released in Italy on 14 February 2008.

==Plot==
Distracted by taking her antidepressants, Nicole (Sanchez-Gijon) causes an accident with Sasha (Muccino), whos just left a communal home for the families of drug addicts. Despite the circumstances of their meeting, the pair become friends and Sasha seeks Nicoles help in pursuing his love interest, Bendetta (Crescentini). Despite her beauty, Bendetta is the spoiled daughter of Sashas patron, Riccardo (Colangeli)
She appears to spend much of her time abusing alcohol, surrounded by like-minded rich youth that also delve into cocaine.

Sasha is then approached by a former acquaintance, former addict, Fabrizio (Mazzotta) who is looking for money to participate in an upcoming poker game. Sasha decides to get involved in the game himself. He scores big and adapts to the gamblers lifestyle, gaining respect from Bendettas spoiled friends. Meanwhile Nicoles life continues to unravel, racked by feelings of guilt over the death of her former lover ten years earlier. She even visits the forgiving mother (Chaplin) of her former lover. She also looks for escape from her passionless marriage and looks towards Sasha. Weissberg, Jay. Talk to Me About Love. Variety. April 7, 2008 - April 13, 2008 

==Cast==
*Silvio Muccino as Sasha
*Aitana Sánchez-Gijón as Nicole
*Carolina Crescentini as Benedetta
*Geraldine Chaplin as Amelie
*Giorgio Colangeli as Riccardo
*Niccolò Senni as Giacomo
*Flavio Parenti as Tancredi
*Max Mazzotta as Fabrizio
*Andrea Renzi as Lorenzo
*Giorgio Sgobbi as Architetto

==Reception==
The film debuted at no. 1 on the Italian box office.  It had grossed over $11. 5 million by the end of March 2008. 

===Awards===
*David di Donatello for David of the Youth
*Nastro dArgento for Best Cinematography

===Nominations===
*David di Donatello for Best Cinematography
*David di Donatello for Best Costumes
*David di Donatello for Best Score
*David di Donatello for Best New Director
*David di Donatello for Best Supporting Actress
*Nastro dArgento for Best New Director
*Nastro dArgento for Best Producer Nastro dArgento Best Production Design

==References==
 

==External links==
*  

 
 
 
 
 
 