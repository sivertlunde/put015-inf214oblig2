Kingsman: The Secret Service
 
 
{{Infobox film
| name = Kingsman: The Secret Service
| image = Kingsman The Secret Service poster.jpg
| alt =  
| caption = Theatrical release poster
| director = Matthew Vaughn
| producer = {{Plain list |
* Adam Bohling
* David Reid
* Matthew Vaughn
}}
| screenplay =  {{Plain list |
* Jane Goldman
* Matthew Vaughn
}}
| based on =  
| starring = {{Plain list |  
* Colin Firth
* Samuel L. Jackson
* Mark Strong
* Taron Egerton
* Michael Caine
}}
| music = {{Plain list|
* Henry Jackman
* Matthew Margeson
}}
| cinematography = George Richmond
| editing = {{Plain list|
* Eddie Hamilton Jon Harris
}}
| studio = Marv Films Cloudy Productions
| distributor = 20th Century Fox
| released =  
| runtime = 129 minutes  
| country = {{Plain list|
* United Kingdom
* United States 
}}
| language = English
| budget = $81 million   
| gross = $401.4 million 
}} spy Action action comedy The Secret Service, created by Dave Gibbons and Mark Millar. The screenplay was written by Vaughn and Jane Goldman. The film stars Colin Firth, Samuel L. Jackson, Mark Strong, Taron Egerton and Michael Caine. It follows the recruitment and training of a potential secret agent, Gary "Eggsy" Unwin, into a secret spy organisation. Eggsy joins a mission to tackle a global threat from Richmond Valentine, a wealthy Eco-terrorism|eco-terrorist.

Kingsman: The Secret Service premiered at Butt-Numb-A-Thon on 13 December 2014, and was theatrically released in the United Kingdom on 29 January 2015. The film received positive critical reviews, and has grossed over $401 million worldwide, becoming Vaughns most successful movie to date. 

==Plot==
During a raid in the Middle East in 1997, a probationary secret agent sacrifices himself to save his team. Feeling guilt over the loss of his comrade, Harry Hart, code-named "Galahad," personally delivers a bravery medal to the agents widow, Michelle Unwin, and his young son, Gary "Eggsy", saying that if they ever need help, they should call the phone number on the back of the medal and deliver a coded message.
 heads of SIM cards, granting free cellular and Internet access.

In London, Eggsy is now an unemployed young adult living with his mother, infant half-sister, and abusive stepfather Dean. Despite being intelligent and capable, he left training for the Royal Marines and lives an aimless life. After being arrested for stealing a car and taking it for a joyride, Eggsy calls the number on the back of the medal. Hart arranges for his release and tells Eggsy about the existence of Kingsman, a secret intelligence agency for which both he and Eggsys late father worked. In the wake of Lancelots death, the agency opens a vacancy for a new agent, and Eggsy agrees to apply. He joins a group of other candidates, including a woman named Roxy. Training is overseen by "Merlin," a senior Kingsman that acts as teacher, quartermaster, pilot and Ops coordinator. The candidates are eliminated one by one until Eggsy and Roxy emerge as the final two. However, Eggsy fails his final test, and Roxy becomes the new Lancelot.
 chip implanted in Arnolds head explodes, killing him; Hart is injured during his escape from unknown assailants. Undeterred, Hart poses as a billionaire and dines with Valentine to try to discern his plans.

Hart tracks Valentine to an obscure hate group church in Kentucky, where Valentine and Gazelle are conducting a test. They broadcast a signal to his SIM cards, causing everyone in the church, including Hart, to become uncontrollably violent. A mass brawl breaks out, with Hart the sole survivor, while Eggsy, Merlin and Arthur—the Kingsmans leader—watch via video link. Valentine approaches Hart and kills him without revealing his plan.

Eggsy returns to the Kingsman headquarters, where he discovers that Arthur is one of Valentine’s converts. Arthur explains Valentines views: humanity is akin to a virus, and global warming is the Earths equivalent of a fever. Valentine intends to kill the virus before it kills the host, broadcasting his signal worldwide to cause a massive culling of the human race. Only a select few that Valentine has deemed worthy of living — those who have sided with him and thus received the protective microchips in their heads, and the VIPs he kidnapped — will be spared.

After avoiding Arthur’s attempt to kill him — killing the group leader as he does so — Eggsy teams up with Roxy and Merlin. Roxy pilots a high-altitude balloon vehicle into the stratosphere to disrupt Valentines satellite network with a missile, while Eggsy and Merlin directly assault Valentine’s mountain bunker. Roxy knocks out the satellite and Eggsy fights his way through Valentine’s security forces, while Merlin detonates the security chips, killing all who were part of Valentines plan. Eggsy then fights and defeats Gazelle before killing Valentine and saving the world.  In gratitude for saving the world, the princess of Sweden engages in anal sex with Eggsy.

In a mid-credits scene, Eggsy, now a full Kingsman, reaches out to his mother, offering her a nicer house and a chance to get away from Dean. In a callback to when Galahad recruited Eggsy, Dean and his subordinates move to attack, and Eggsy quotes his old mentor by saying "manners maketh man" before moving to defeat them and rescue his mother.

==Cast==
 
* Colin Firth as Galahad (Harry Hart), a veteran Kingsman agent.   
* Samuel L. Jackson as Richmond Valentine, a billionaire philanthropist who speaks with a lisp and has a weak stomach for blood and violence.
* Mark Strong as Merlin, a senior Kingsman agent and trainer.
* Taron Egerton as Gary "Eggsy" Unwin, a poor Londoner who is chosen by Galahad as his Kingsman candidate. Alex Nikolov portrays the young Eggsy.
* Sophie Cookson as Roxy Morton, a trainee who befriends Eggsy and later becomes the new Lancelot.
* Jack Davenport as Lancelot, a Kingsman who is killed while trying to rescue Professor Arnold.
* Mark Hamill as Professor James Arnold, an academic from Imperial College London, and an expert in climate change. prosthetic legs.
* Edward Holcroft as Charlie Hesketh, Arthurs Kingsman candidate and Eggsy and Roxys primary rival during the training program.
* Michael Caine as Arthur (Chester King), the leader of the Kingsman organisation.
 Geoff Bell plays Dean, her abusive second husband. Jonno Davies portrays Lee Unwin, Eggsys father and a former Kingsman candidate who sacrificed himself. Hanna Alström portrays Princess Tilde, and Björn Flomberg plays the Scandinavian Prime Minister. Nicholas Banks, Jack Cutmore-Scott, Nicholas Agnew, Rowan Polonski and Tom Prior portray, respectively, Digby Barker, Rufus Saville, Nathaniel, Piers and Hugo Higins, the other five Kingsman candidates. Fiona Hampton plays Amelia, a Kingsman agent who pretends to be a candidate in order to set up the first test.

==Production==
The film was announced in late October 2012, after Vaughn dropped out of directing   to adapt the Mark Millar comic book The Secret Service. In March 2013 20th Century Fox confirmed the film and set 14 November 2014 as the release date for the film worldwide, with production to begin in the following August.    Colin Firth joined the cast to lead the film on 29 April 2013.  In June 2013 it was reported that Leonardo DiCaprio was in talks to play a villain.  In September 2013, Vaughn cast Sophie Cookson for the female lead, preferring a newcomer over more obvious candidates like Emma Watson and Bella Heathcote.  Mark Hamill was cast in a cameo role as Professor James Arnold, a reference to his character in the source comic book being named "Mark Hamill". 

===Filming===
Principal photography began 6 October 2013 in Deepcut, Surrey,    on a budget reported to be one-third of the $200 million budget of Skyfall. 

The Alexandra Road Estate in Camden  was used for Eggsys home area, and some scenes were filmed at Imperial College London. The Black Prince Pub in Kennington, South London was used for various fight scenes and the car chase.

While rumours of several celebrity cameo parts were published, including Adele,  Elton John,     Lady Gaga   and David Beckham,  none of these rumours proved to be true.

===Music===
In May 2014, it was reported that Henry Jackman and Matthew Margeson would be composing the music for the film,  while in July it was announced that Gary Barlow would be writing the music for the film; a song from Take Thats seventh studio album III (Take That album)|III, "Get Ready for It", played during the end credits. 

{{Infobox album
| Name = Kingsman: The Secret Service (Original Motion Picture Score)
| Longtype =
| Type = Soundtrack
| Artist = Henry Jackman and Matthew Margeson
| Cover = Kingsman OST.jpg
| Border =
| Alt =
| Caption =
| Released =  
| Recorded = 2014
| Genre = Film score
| Length = 57:27
| Label = La-La Land Records|La-La Land 
| Producer = 
| Chronology = 
| Misc = {{Extra chronology
 | Artist     = Henry Jackman
 | Type       = soundtrack The Interview (2014)
 | This album = Kingsman: The Secret Service (2015)
 | Next album = Pixels (2015 film)|Pixels (2015)
}}}}

{{Track listing
| total_length = 57:27
| all_music    = Henry Jackman and Matthew Margeson
| music_credits = no
| writing_credits  = yes
| headline     =
| title1       = Manners Maketh Man
| length1      = 1:38
| title2       = The Medallion
| length2      = 2:14
| title3       = Valentine
| length3      = 2:25
| title4       = To Become a Kingsman
| length4      = 4:19
| title5       = Pick a Puppy
| length5      = 2:13
| title6       = Drinks with Valentine
| length6      = 2:40
| title7       = Skydiving
| length7      = 3:37
| title8       = Shame We Had to Grow Up
| length8      = 1:56
| title9       = Kentucky Christians*
| length9      = 2:37
| title10      = Curious Scars and Implants
| length10     = 3:09
| title11      = Toast to a Kingsman
| length11     = 1:56
| title12      = An 1815 Napoleonic Brandy
| length12     = 4:23
| title13      = Eat, Drink, and Paaaaarty
| length13     = 1:54
| title14      = Calculated Infiltration
| length14     = 7:54
| title15      = Out of Options*
| length15     = 1:48
| title16      = Hand on the Machine
| length16     = 2:22
| title17      = Finale
| length17     = 3:56
| title18      = Original Valentine Ideas (Demo Suite)*
| length18     = 6:25
}}

*Album exclusive tracks

Songs not included in the soundtrack, but featured in the film and trailers include the following: 
 Money for Nothing"
* Dizzee Rascal – "Bonkers (song)|Bonkers" Give It Up"
* Lynyrd Skynyrd – "Free Bird"
* Iggy Azalea (featuring Ellie Goulding) – "Heavy Crown" John Newman) – "Feel the Love"
* Bryan Ferry – "Slave to Love"
* Take That – "Get Ready for It" Pomp & Circumstance"
* Kula Shaker - "Hush" Ging Gang Gong De Do Gong De Laga Raga"

==Release and reception==
The films premiere was held in London, with director Vaughn and stars Firth, Egerton, and Strong attending, and Take That performing the films theme live.  A regional premiere was held in Glasgow at exactly the same time as the London event, and live footage was streamed from the premiere to Glasgow.  Mark Millar also hosted a charity screening of the film ahead of its release in Glasgow to raise money for his old school, St. Bartholomews. 

The film opened in the United Kingdom on 29 January 2015.    In the United States 20th Century Fox planned to release the film on 14 November 2014,  but later delayed it to 6 March 2015.  It was later moved up to 24 October 2014,  before being delayed again to 13 February 2015. 

The film was released in most of Latin America with the action scene set in the church removed. The scene, considered vital by the director and film critics, was excised almost completely, leaving only the set-up and immediate consequences.   

===Marketing===
The   suiting, while everything from the ties and shirts to eyewear, umbrellas, shoes and watches were designed by heritage brands such as Cutler and Gross, George Cleverley, Mackintosh and Bremont Watch Company|Bremont. The collaboration is the first of its kind, making Kingsman: The Secret Service the first film from which customers can buy all of the outfits they see.  

The film includes significant product placement for Adidas Originals. 

 

===Box office===
 
  Kingsman: The Secret Service has earned a gross of $126.5 million in North America and  $274.5 million in other territories for a worldwide total of $400.9 million, against a budget of $81&nbsp;million. 
 Big Hero Fifty Shades of Grey.    In its fourth weekend, it expanded to a total of 54 countries and grossed $33.4 million from 5,940 screens.    Its biggest opener outside of North America was in China where it earned $27.9 million.  Other high openings occurred in Korea ($5.3 million)  Russia and the CIS ($3.6 million),  Taiwan ($3.4 million),  and France ($3.3 million). 
 Fifty Shades Presidents Day weekend it grossed $41,761,527.    

===Critical response===
The review aggregator website Rotten Tomatoes sampled 183 critics and judged 74% of the reviews positive, with an average rating of 6.6/10, calling the film "stylish, subversive, and above all fun".  On Metacritic, the film has a critics score of 58 out of 100, based on reviews from 39 critics, indicating mixed reviews. 

Peter Travers of Rolling Stone said of the film, "This slam-bang action movie about British secret agents is deliriously shaken, not stirred...Even when it stops making sense, Kingsman is unstoppable fun."  Anthony Lane of The New Yorker stated, "Few recent movies have fetched quite as far as “Kingsman,” and countless viewers will relish the brazen zest of its invention." However, Lane was critical of the films use of stereotypes.  Manohla Dargis of The New York Times enjoyed the film, but criticised Vaughns use of violence as a cinematic tool, calling it "narrative overkill". 

Jason Ward of The Guardian wrote that " verything about Kingsman exists to disguise the fact that it is solidly conservative". His examples include " he depiction of Valentines plan as a throwback to a less serious era of spy movies   is revealed as a feint, with the ulterior motive of undermining environmentalists".  Jordan Hoffman, writing for the same paper, said of the film, "The spirit of 007 is all over this movie, but Vaughns script&nbsp;... has a licence to poke fun.&nbsp;... no one involved in the production can believe theyre getting away with making such a batshit Bond." Comparing the film to those of Christopher Nolan, Hoffman said, "Despite the presence of grandfatherly Michael Caine, Kingsmans tone is about as far from the Christopher Nolan-style superhero film as you can get. Verisimilitude is frequently traded in for a rich laugh." 
 James Bond films. 

==Sequel==
Millar and Vaughn have stated that a sequel was possible if the film performs well at the box office, and Vaughn has expressed interest in directing the sequel.    Vaughn also noted that he hopes to have Firth back in the sequel, while Strong is interested in returning as well.   Fox announced a sequel is in the works, but its unclear if Vaughn will return to direct. 

==References==
 

==External links==
*  
*  
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 