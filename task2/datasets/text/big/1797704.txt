Electronic Labyrinth: THX 1138 4EB
{{Infobox film name          = Electronic Labyrinth: THX 1138 4EB    image         = Thx1138_labyrinth.gif caption       = THX escaping the electronic labyrinth  director      = George Lucas writer        = George Lucas starring      = Dan Natchsheim producer      =  music         = The Yardbirds, "Still Im Sad" (opening credits)
|cinematography= F. E. Zip Zimmerman release       =  distributor   =  runtime       = 15 min. language  English
}}
 science fiction film school. The short was reworked as the 1971 theatrical feature THX-1138.

In 2010, this film was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant". 

==Plot==
In an underground city in a dystopian future, the protagonist, whose name is "THX 1138 4EB", is shown running through passageways and enclosed spaces. It is soon discovered that THX is escaping his community. The government uses computers and cameras to track down THX and attempt to stop him; however, they fail. He escapes by breaking through a door and runs off into the sunset. The government sends their condolences to YYO 7117, THXs mate, claiming that THX has destroyed himself.

The USC program guide accompanying the film describes it as a "nightmare impression of a world in which a man is trying to escape a computerized world which constantly tracks his movements". 

==Production== Matthew Robbins and Walter Murch had a similar idea which Robbins developed into a short treatment,    but Robbins and Murch lost interest in the idea, whereas Lucas was keen to persist.  
 US Navy, whereby Navy filmmakers attended USC for additional study. Pollock, Dale, Skywalking: The Life and Films of George Lucas,  Harmony Books, New York, 1983, ISBN 0-517-54677-9.  Fensch, Thomas, Films on the Campus,  A.S. Barnes & Co, New York, 1970, ISBN 0-498-07428-5.  Teaching the class was not popular amongst USC staff, as the Navy filmmakers often had rigid, preconceived ideas about filmmaking, and sometimes misbehaved in class.   But the Navy paid for unlimited color film, and lab processing costs, for their students.   Lucas offered to teach the class, and was allowed the opportunity.  

The Navy men formed the crew of the film, and some appeared in the cast.   Because of the Navy connection, Lucas was able to access filming locations which would not otherwise have been available to him:  the USC computer center, a parking lot at University of California, Los Angeles|UCLA, the Los Angeles International Airport,   and the Van Nuys Airport.  Much of the filming was done at night,   with some at weekends. 

The film was completed in 12 weeks, with Lucas editing it on the Moviola at the home of Verna Fields, where he was working during the day editing United States Information Agency films under Fields supervision. 

==Reception==
In January 1968, the film won first prize in the category of Dramatic films at the third National Student Film Festival held at the   film critic Charles Champlin, and Ned Tanen, then a Universal Studios production executive, who was later involved with Lucas American Graffiti. 

==Feature film==
 
In 1971, Lucas re-worked the short as a theatrical feature, THX 1138. The last act of the feature-length THX 1138 roughly corresponds with the events in this film. In the final scene, people are warned not to exit the underground city through the door,  which allegedly leads to death. In truth, the exit leads to their freedom. Cosmetically, there are similarities in coloring and appearance, although one noticeable difference is the actors did not have to shave their heads for the short film, unlike the later feature version.

==Versions==
The film became widely available on the bonus disk of the 2004 George Lucas Directors Cut DVD release of THX 1138.  The film also exists as a 16mm reference print and on videocassette with a run time of 15 minutes. 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 