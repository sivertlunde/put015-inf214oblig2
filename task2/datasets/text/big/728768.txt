Tokyo Babylon
{{Infobox animanga/Header
| name            = 
| image           =  
| caption         = The first volume of Tokyo Babylon as released by Tokyopop.
| ja_kanji        = 東京BABYLON
| ja_romaji       = 
| genre           = Occult detective, shōnen-ai
}}
{{Infobox animanga/Print
| type            = manga Clamp
| publisher       = Shinshokan
| publisher_en    =  
| demographic     = Shōjo manga|Shōjo Wings
| first           = 1990
| last            = 1993
| volumes         = 7
| volume_list     = 
}}
{{Infobox animanga/Video
| type            = ova
| director        = Koichi Chigira
| producer        = 
| writer          = 
| music           =  Madhouse
| licensee        =  
| first           = October 21, 1992
| last            = March 21, 1994
| runtime         = 
| episodes        = 2
| episode_list    = 
}}
{{Infobox animanga/Video
| type            = live film
| title           = Tokyo Babylon/1999 - The Movie George Iida
| producer        = 
| music           = Kuniaki Haishima
| studio          = PDS
| licensee        = 
| released        = August 21, 1993
| runtime         = 100 minutes
}}
 

 , also known as Tokyo Babylon: A Save Tokyo City Story, is a Shōjo manga|shōjo manga series created by Clamp (manga artists)|Clamp, with story by Nanase Ohkawa and art by Mokona. The series follows Subaru Sumeragi, the head of the Sumeragi clan, and his sister Hokuto as they work to protect Tokyo from a myriad of supernatural perils. The series is based on a dōjinshi Clamp wrote but decided to add dark social themes in the serialization as a result of the chapters length. They were published by Shinshokan in Japan from 1990 to 1993 and collected total of seven tankōbon volumes. The English-language version of the manga was first distributed by Tokyopop and is now in possession of Dark Horse Comics.

Between 1992 and 1994 Tokyo Babylon was adapted into a two-part original video animation series by the studio Madhouse (company)|Madhouse. PDS also produced a live-action feature film sequel, Tokyo Babylon 1999, which was released on August 21, 1993. The series has been well received for its focus on occultism and social themes, the latter which were reflected most notably in Subarus character development. The manga is also notorious for its open and tragic ending which is followed in Clamps manga X (manga)|X.

== Plot ==
The plot is told in a series of substories, published monthly or as 2-3 per volume. While it begins with a monster of the week approach, with somewhat independent running chapters, the plot gradually becomes continuous and backstory is introduced. It focuses on the development of the characters and the relationships between them.

Sixteen-year-old Subaru Sumeragi, the main character, is a very powerful magician, the thirteenth head of the foremost family of onmyōji in Japan, which has served the Emperor for centuries. As such, he is called upon to solve various occult mysteries, or stumbles himself on people whom his kind nature compels him to help. These occurrences form the main plot of most chapters. He lives in Tokyo with his twin sister Hokuto, an exuberant girl whose chief occupations are to design eccentric clothing for herself and her brother, and to egg on their mutual friend Seishirō Sakurazuka, a kindly, 25-year-old veterinarian, when he declares his love for Subaru.

There are early hints that Seishirō may not be all that he appears. Hokuto jokes about him being a member of the family of Sakurazukamori, a clan of assassins who use onmyōjitsu to kill, and are pronounced to be the Sumeragis opposite. Also, Subaru has dreams about having met someone under a cherry tree blooming out of season when he was but a small child, but he cannot quite recall what was being said. This person was actually Seishirō, upon whom Subaru stumbled when he had just performed a kill. According to the rules of the Sakurazukamori, Seishirō should have killed him, but impressed with the childs purity, he made a bet with him instead: He would meet Subaru again, and would then spend one year with him, protecting him and trying to love him. If, at the end of that year, he felt something for Subaru which distinguished him from a thing he could easily destroy, as he could not with any other person, then he would not kill him. To recognize him, he marked him with inverted pentagrams on both hands, the sign of the Sakurazukamoris prey. These marks are for most of the story hidden beneath the gloves Subaru continually wears on the direction of his grandmother, the previous family head, who recognized the marks and so hopes to conceal them with her magic.

Things come to a head when Seishirō loses an eye protecting Subaru, and Subaru realizes that he is in love with Seishirō. But the year is over, and Seishirō declares himself the winner of the bet. He breaks Subarus arm and tortures him, but does not succeed in killing him, as Subarus grandmother breaks his spell - an action which leaves her crippled. Subarus shock and heartbreak leave him catatonia|catatonic. Hokuto, feeling guilty for her promotion of Seishirō, whom she knew to be dangerous, but also believed to be the only one who might touch Subarus heart, leaves to find Seishirō and confronts him, asking to be killed by him and casting a spell with her death. Subaru, seeing her in a dream, is shocked out of his catatonia by her action. He vows to find Seishirō and take revenge for his sisters death.

== Characters ==
 

  is the 13th Head of the Sumeragi clan, a family of onmyōji. He tends to be rather shy, but is extremely kindhearted. He constantly wears gloves over his hands for reasons known only to his grandmother. A shadowy childhood moment may provide the answer.   

  is Subarus eccentric twin sister. She is quick-witted, bold and impulsive, the polar opposite of Subaru. While she lacks most of Subarus strong spiritualistic abilities, she is still able to cast spells unique to her. She always means well and acts in Subarus best interests. Hokuto is the designer of the outrageous outfits she and Subaru often don.  
 

  is a veterinarian who claims to be romantically interested in Subaru. He is secretely the Sakurazukamori, a murderous onmyōji who first met the teenager seven years ago.     

==Production==
  influenced the mangas themes.]] Babylon resembled Tokyo according to Clamp. The one-shots characters had several changes when starting the series most notably Seishirō.   

Back when Tokyo Babylon started serialization, Clamp was also writing RG Veda on the monthly magazine Wings. While the authors found this complicated, the series quarterly publication of sixty pages resulted in the authors making several changes; The length of each chapter made the authors write a darker story in contrast to the original plans for a soft one. Based on their own experience in Tokyo, Clamp incorporated dark social themes making the series realistic despite its focus on occultism. Ohkawa believes their young age when writing the manga also influenced most of the series negative messages. Although when the series started the ending was already planned, it was not until the Tokyo Tower chapter that Clamp set the general atmosphere with the pilot being perceived as a comedy. The next chapter involving Subarus past meeting with a man later revealed as Seishirō then set the events from the series future. 

In the making of the art, colored weft and light colors were used for the main illustrations. This proved to be difficult to the authors. Ohkawa believes the art in general was influenced by the years in which the manga was published. This is reflected in how Clamps artwork changed, the clothing and most notably Hokutos dresses.  In retrospective, Clamp found this work was completely different from RG Veda as it was completely written by them and was also influenced by their way of living. 

==Themes==
The series focuses on several social themes such as faith, organs donation and the treatment to middle age men. However, the main philosophy seen across the story is the individualism, the stance which emphasizes the moral worth of an individual. Subaru and Hokuto believe that nobody can fully understand the another persons suffering. Despite his belief in this philosophy, Subaru is a resigned individual who worries more about others than himself.    This is because Subaru believes he can hurt others with the idea that he will never understand other people. The same problem comes from the fact Subaru and Hokuto are identical twins and as a result try to live in order to see each other as different people.    Across the plot, Subaru develops as an individual as his actions are deemed as arrogant by Seishiro for taking the guilt.  Additionally, he starts taking actions that he believes are immoral despite his reasons for them such as lying to a mother about her daughters wish to stop her from taking revenge. 

==Media==

===Manga===
The Tokyo Babylon manga was published by Shinshokan in the South magazine from 1990 to 1993 on a quarterly basis. Its chapters were collected in a total of seven tankōbon volumes. The series has also been rereleased in two different formats; five volumes in Tankōbon#bunkoban|bunkoban format by Shinshokan and three volumes in Tankōbon#aizōban|aizōban format by Kadokawa Shoten.  Tokyopop announced they licensed the series for a North American release in September 2003.  They published the series in North America between May 11, 2004 and May 10, 2005. The manga was rereleased in omnibus format by Dark Horse Comics in 2011. 

{{Graphic novel list/header
 | Language       = Japanese
 | SecondLanguage = North American
 | Width          = 80%
}}
{{Graphic novel list
 | VolumeNumber    = 01
 | OriginalRelDate = April 10, 1991  
 | OriginalISBN    = 4-403-61250-4
 | LicensedRelDate = May 11, 2004 
 | LicensedISBN    = 978-1-59182-871-6
 | ChapterList     = 
* Vol 0: "T·Y·O"
* Vol 1: "Babel"
* Vol 1.5: "Destiny"
}}
{{Graphic novel list
 | VolumeNumber    = 02
 | OriginalRelDate = November 10, 1991 
 | OriginalISBN    = 4-403-61268-7
 | LicensedRelDate = July 6, 2004 
 | LicensedISBN    = 978-1-59182-872-3
 | ChapterList     = 
* Vol 2: "Dream"
* Annex: "Smile"
}}
{{Graphic novel list
 | VolumeNumber    = 03
 | OriginalRelDate = January 25, 1992 
 | OriginalISBN    = 4-403-61274-1
 | LicensedRelDate = September 7, 2004 
 | LicensedISBN    = 978-1-59182-873-0
 | ChapterList     = 
* Vol 3: "Call.A"
* Vol 3: "Call.B"
}}
{{Graphic novel list
 | VolumeNumber    = 04
 | OriginalRelDate = July 10, 1992  
 | OriginalISBN    = 4-403-61282-2
 | LicensedRelDate = November 9, 2004 
 | LicensedISBN    = 978-1-59182-874-7
 | ChapterList     = 
* Vol 4: "Crime"
* Vol 5: "Save.A"
* Vol 5: "Save.B"
}}
{{Graphic novel list
 | VolumeNumber    = 05
 | OriginalRelDate = April 5, 1993 
 | OriginalISBN    = 4-403-61304-7
 | LicensedRelDate = January 11, 2004 
 | LicensedISBN    = 978-1-59532-049-0
 | ChapterList     = 
* Vol 6: "Old"
* Vol 7: "Box"
* Vol 8: "Rebirth"
}}
{{Graphic novel list
 | VolumeNumber    = 06
 | OriginalRelDate = August 25, 1993 
 | OriginalISBN    = 4-403-61319-5
 | LicensedRelDate = March 8, 2005 
 | LicensedISBN    = 978-1-59532-050-6
 | ChapterList     = 
* Vol 9: "News"
* Vol 10: "Pair"
}}
{{Graphic novel list
 | VolumeNumber    = 07
 | OriginalRelDate = March 25, 1994   
 | OriginalISBN    = 4-403-61339-X
 | LicensedRelDate = May 10, 2005    
 | LicensedISBN    = 978-1-59532-051-3
 | ChapterList     = 
* Vol 11: "End"
* Annex: "Secret"
* Annex: "Start"
}}
 

===Original video animation===
The original video animations (OVA) series are two episodes with original stories animated by Madhouse. They are directed by Koichi Chigira, with characters designs by Kumiko Takahashi. The first episode has Subaru investigating the meaning behind several accidents in the construction of a building while in the second he meets another onmyoji who is helping the police in finding a serial killer. Producer Yumiko Masujima remembers how difficult it was to recreate the mangas atmosphere in the OVAs.    
While originally released in VHS, the OVAs were rereleased in Japan in DVD format on June 21, 2000.  The OVAs were distributed by U.S. Manga Corps and Manga Entertainment for Australia, UK and France.
 US Manga Corps released the two OVA episodes on April 4 and July 11, 1995. 

=== Feature film === George Iida requested the help from the OVA producer Yumiko Masujima in casting the main characters.  The film is famous for being the first live-action production based on a Clamp series and Iida expressed satisfaction in response to positive fanresponse. 

The plot has Subaru Sumeragi investigating the death of a former enemy of the Sumeragi clan, Kaneyama. Before his death, Kaneyama had undertaken a new project: teaching a group of seven teenage girls how to use dark magic to take revenge on those they deem "guilty," beginning with an abusive teacher. One of the girls, Kurumi, starts to feel remorse; however, she is convinced by the others to continue with their plans. When Subaru attempts to stop them, they declare him to be their enemy and attack him. Subaru learns that his former friend-turned-enemy, assassin Seishirō Sakurazuka, was the one who killed Kaneyama, and he has now turned his sights on the girls.

The girls grow increasingly sick as a result of using the spells, and Subaru tries once more to save them. Seishirō appears, claiming that none of the girls can be saved. Subaru and Seishirō fight, only to ultimately be stopped by the appearance of the ghost of Hokuto Sumeragi, who asks them to stop for her sake. Seishirō leaves, and the fight is unresolved. At the end, Kurumi says that, hopefully, all the other girls will be able to give up their vengeful plans and become good again. Subaru adds finally that he wishes for the same thing.

===Art book===
An artbook entitled Tokyo Babylon Photographs was released on April, 1996. (Kadokawa Shoten: ISBN 4-403-65008-2)

==Reception and legacy==
The Tokyo Babylon manga has been popular with its English releases often appearing in Diamond Comic Distributorss lists of best selling comics.    It received generally positive critical response with the writers from Manga Bookshelf referring to it as one of Clamps best works based on the social themes reflected on the main cast whose traits, while found stereotypical at first, are developed across the story until its tragic ending.  Besides its focus on supernatural events, the series is famous for the shonen ai seen in the interactions between Subaru and Seishiro despite being initially taking comically.   The artwork has also been praised for remaining appealing despite the English volumes being released over a decade after their original release. Most of the reviewers compliments aimed to the landscapes and the atmosphere.      

When the series was reaching its climax, Wings editor Miki Ishikawa remembers receiving several letters in which fans requested for a happy ending. Once reaching the end, more letters expressed sadness and shock for the tragic ending and wanted more explications regarding Hokutos fate. In regards to this character, Ishikawa considered her popularity an exception considering most readers tend to be more attracted by the series male characters.  The writers from Manga Book Shelf noted that the series open ending has also been a subject of criticism within fans. 

The OVAs were the subject of positive response thanks to its animation which has been referred to as "stunning."       They have been recommended to X (manga)|X fans for Subarus appearances as well as the female demographic.    Although the story has been labeled as "s nothing complex, nor is it ingenious," it has been praised for its focus on horror even though the violent imagery may bother viewers.  

Subaru and Seishirō return in Clamps apocalyptic manga X. The story, set nine years after the end of Tokyo Babylon, has the two onmyōji on opposite sides during  .

==See also== protagonist of the novel even has a cameo appearance at the beginning of the Tokyo Babylon manga as Subarus "helpful ambassador". Tokyo Babylon book 1, vol. 1. (English translation by TOKYOPOP) 

==References==
 

== External links ==
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 