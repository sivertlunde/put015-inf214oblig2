BoBoiBoy: The Movie
 
{{Infobox film
| name = BoBoiBoy: The Movie
| image = 
| caption = 
| genre = superhero, action, adventure, sci-fiction
| director = Nizam Razak
| producer = 
| writer = 
| starring = Nur Fathiah Diaz Anas Abdul Aziz Dzubir Mohamed Zakaria Azman Zulkiply Nur Sarah Alisya Yap Ee Jean
| music = 
| studio = Animonsta Studios
| distributor = 
| released =  
| runtime = 
| country = Malaysia
| language = Malay
| budget = 
| gross = 
}}
BoBoiBoy: The Movie is the upcoming Malaysian animation movie that is expected to be available in cinemas on end 2015.  This is the first film of Animonsta Studios and BoBoiBoy.

== Synopsis ==
A group of alien treasure hunters named The Tengkotak has arrived on earth and kidnapped Ochobot in order to use him to locate an ancient and powerful “Sfera Kuasa” hiding on earth. BoBoiBoy and his super friends must now race against time to save Ochobot and uncover the secrets behind the “Sfera Kuasa”. Their journey will take them on an adventure filled with action, comedy, and beautiful locales.

== Characters ==
* BoBoiBoy (Nur Fathiah Diaz)
* Yaya (Nur Sarah Alisya)
* Ying (Yap Ee Jean)
* Gopal (Dzubir Mohamed Zakaria)
* Fang (Wong Wai Kay)
* Ochobot (Mohd Fathi Diaz)
* Klamkabot
* Bora Ra (Azman Zulkiply)
* Gaga Naz
* Yoyo Oo (Anas Abdul Aziz)
* Kiki Ta
* Cici Ko
* Papa Zola (Nizam Razak)
* Tok Aba (Anas Abdul Aziz)
* Adu Du (Anas Abdul Aziz)
* Probe (Anas Abdul Aziz)
Nicki

== References ==
 

== External links ==
* 
* 

 

 
 
 
 
 
 