Dangerous Curve Ahead
{{Infobox film
 | name = Dangerous Curve Ahead
 | image = Dangerous Curve Ahead (1921) - 1.jpg
 | caption = Helene Chadwick and Richard Dix
 | director = E. Mason Hopper William J. Reiter (Assistant Director)
 | producer = 
 | writer = Rupert Hughes (Story) Julien Josephson
 | starring = Helene Chadwick Richard Dix
 | music = 
 | cinematography = John J. Mescall
 | editing =
 | distributor = Goldwyn Pictures Corporation
 | released = 
 | runtime = 50&nbsp;minutes Silent (English intertitles)
 | country = United States
 | budget = 
}}
  silent comedy comedy starring Helene Chadwick and Richard Dix.    The film is considered to be lost films|lost.   

==Cast==

*Helene Chadwick as Phoebe Mabee
*Richard Dix as Harley Jones Maurice Lefty Flynn as Anson Newton
*James Neill as Mr. Mabee
*Edythe Chapman as Mrs. Mabee
*Kate Lester as Mrs. Noxon Newton Hall as Phoebes son

==See also==
*List of lost silent films (1920–24)
*List of lost films

==References==
 

== External links ==
 
* 
* 

 
 
 
 
 
 
 
 
 
 


 