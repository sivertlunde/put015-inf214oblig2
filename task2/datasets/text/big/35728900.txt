Public Opinion (1916 film)
{{infobox film
| name           = Public Opinion
| image          =
| imagesize      =
| caption        =
| director       = Frank Reicher Roscoe Smith(asst dir.)
| producer       = Jesse Lasky Margaret Turnbull(original story & scenario)
| starring       = Blanche Sweet
| music          =
| cinematography = Dent Gilbert
| editing        =
| distributor    = Paramount Pictures
| released       = August 20, 1916
| runtime        = 50 minutes;5 reels
| country        = United States
| language       = Silent film(English intertitles)
}} Margaret Turnbull provided the original screen story and scenario. One of Sweets very few Paramount Pictures to survive and preserved by the Library of Congress.    

==Cast==
*Blanche Sweet - Hazel Gray
*Earle Foxe - Dr. Henry Morgan
*Edythe Chapman - Mrs. Carson Morgan Tom Forman - Phillip Carson
*Elliott Dexter - Gordon Graham
*Raymond Hatton - Smith
*R. Henry Grey - (*billed Robert Henry Gray)

==See also==
*Blanche Sweet filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 


 