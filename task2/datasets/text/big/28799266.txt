I'll Give a Million (1936 film)
 
 
{{Infobox film
| name           = Ill Give a Million
| image          = 
| caption        = 
| director       = Mario Camerini
| producer       = 
| writer         = Mario Camerini Giaci Mondaini Ivo Perilli Cesare Zavattini
| starring       = Vittorio De Sica
| music          = 
| cinematography = Otello Martelli
| editing        = Fernando Tropea
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = Italy
| language       = Italian
| budget         = 
}}

Ill Give a Million ( ) is a 1936 Italian comedy film directed by Mario Camerini and starring Vittorio De Sica. It is based on the first screenplay by Cesare Zavattini which tells the sory of a disillusioned millionaire who, tired of the attempts of greedy friends and relatives to sponge off of him, becomes a bum in order to find a decent human being.   

==Plot==
A millionaire is about to commit suicide by leaping off of his yacht into the sea. He is just about to go when he spies a bum attempting suicide himself. The rich man saves the bum and tells him of his 
frustration with his friends and relatives who are always seeking a handout. He then claims that he will give a million francs to the first person to treat him kindly without thinking about his wealth. The next day, the tramp awakens to find the millionaire gone, and that his own tattered rags have been replaced by 
the finest clothing, along with a large roll of francs. The bum then begins circulating the millionaires story around the town. As a result, people all over the country begin treating the homeless with kindness and respect. Eventually the disguised millionaire meets and marries a circus performer and donates his million francs to the whole community.

==Cast==
* Vittorio De Sica - Gold
* Assia Noris - Anna
* Luigi Almirante - Blim
* Mario Gallina - Cavalier Primerose
* Franco Coop - Il banditore
* Gemma Bolognesi - Maria
* Cesare Zoppetti - Falso milionario

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 