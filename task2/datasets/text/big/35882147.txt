Eskimotive
{{Infobox Hollywood cartoon|
| cartoon_name = Eskimotive
| series = Felix the Cat
| image =
| caption = Pat Sullivan
| story_artist =
| animator = Otto Messmer
| voice_actor =
| musician =
| producer = Jacques Kopfstein
| studio = Pat Sullivan Studios
| distributor = The Bijou Films Inc. Educational Pictures
| release_date = April 19, 1928
| color_process = Black and white
| runtime = 7:32 English
| preceded_by = Felix the Cat in Sure-Locked Home
| followed_by = Arabiantics
}}
 Pat Sullivan Studios, starring Felix the Cat.

==Plot summary==
Felix and a little black kitten are outdoors blowing Soap bubble|bubbles. After blowing a few, Felix is asked by the kitten to make one to ride in. Felix then makes the bubble, and the kitten starts to float away in it. Much to their surprise, the bubble is too difficult to burst, and Felix goes on to chase his airborne companion for hundreds of miles.

Finally the bubble reaches the Arctic and settles to the surface. Felix at last opens the sphere and frees the kitten. While they are traveling home, darkness falls, and the two cats are separated without realizing. To deal with the situation, Felix blows lantern bubbles to brighten the place. The Arctic became a beautiful lighted paradise, but a local bear does not appreciate it and therefore goes to confront Felix. To defend himself against the fearsome bruin, Felix shields himself in a bubble. The bear then pushes Felixs bubble into a hole in the ice cap where it sinks into the water. Once more, Felix feels he is untouchable until an eel is able to penetrate his protective orb. Although other eels pack themselves into the bubble, Felix is able to slip out. He then carries the eel-filled bubble all the way home.

Upon reaching a seafood store in a city, Felix sells the eels he has, receiving a sackful of cash. While celebrating his earnings, he suddenly remembers about his little buddy who is still missing. Lost in some wilderness, the kitten is weeping and does not know what to do. Felix gives a loud call, and the little cat, though faraway, is able to hear it. The kitten eventually reaches the city and embraces Felix.

==External links==
*  at the Big Cartoon Database

 
 
 
 
 
 
 
 
 
 


 