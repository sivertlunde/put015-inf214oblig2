The Prisoner of Second Avenue
{{Infobox film
| name          = The Prisoner of Second Avenue
| image         = Prisonersecondave.jpeg
| image_size  =
| caption        = Theatrical release poster (film)
| director       = Melvin Frank
| producer     = Melvin Frank
| writer          = Neil Simon|
| starring       = Jack Lemmon Anne Bancroft Gene Saks
| music          = Marvin Hamlisch Philip Lathrop
| distributor   = Warner Bros. Pictures
| released      =  
| runtime       = 98 minutes
| country        = United States
| language      = English
}} American black play by Neil Simon, later made into a film released in 1975.

==Productions==
The Prisoner of Second Avenue premiered on   and directed by Mike Nichols, the cast featured Peter Falk and Lee Grant starring as Mel and Edna Edison, and Vincent Gardenia as Mels brother Harry.    Internet Broadway Database, accessed April 11, 2012  

The production received 1972 Tony Award nominations for Best Play, for Mike Nichols for Best Director, Play, and Vincent Gardenia for Supporting Actor, Play.  

Clive Barnes, in The New York Times, wrote that "it is, I think, the most honestly amusing comedy that Mr. Simon has so far given us."  Walter Kerr, in The New York Times wrote: "He   has made a magnificent effort to part company with the mechanical, and his over-all success stands as handsome proof that humor and honesty can be got into bed together." 
  West End at the Vaudeville Theatre, produced by Old Vic Company/Old Vic Productions and Sonia Friedman Productions, opening on June 30, 2010 in previews. Directed by Terry Johnson, the cast starred Jeff Goldblum and Mercedes Ruehl. This marked Ruehls London stage debut. 

==Film==
The film version of The Prisoner of Second Avenue stars Jack Lemmon, Anne Bancroft and Gene Saks.  It was produced and directed by Melvin Frank from a screenplay by Simon.  The music is by Marvin Hamlisch. Weiler, A.H.  The New York Times, March 15, 1975 

The New York Times reviewer wrote: "Mr. Simon is serious about a theme that isnt earth-shaking and he understandably cloaks its gravity with genuine chuckles that pop up mostly as radio news bulletins such as the flash that a Polish freighter has just run into the Statue of Liberty. And, with a cast whose members appreciate what theyre saying and doing, the gnawing problems of Second Avenue become a pleasure." 

==Plot== Second Avenue on the Upper East Side of Manhattan, New York City.  Mel Edison, the main character, has just lost his job after many years and now has to cope with being unemployed at middle age. The action occurs during an intense summer heat wave and a prolonged garbage strike, which just exacerbates Edisons plight to no end as he and his wife Edna deal with noisy neighbors, loud sounds emanating from Manhattan streets up to their apartment and even a robbery of their apartment during broad daylight.  Mel eventually suffers a nervous breakdown from the whole affair, and it is up to the loving care of his brother Harry, his sisters and Edna to bring Mel back to a firm reality.

==References==
 

==External links==
*  at the Internet Broadway Database
*  at the Internet Movie Database

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 