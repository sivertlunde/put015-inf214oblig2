Jellyfish (film)
{{Infobox film
| name           = Jellyfish
| image          = Jellyfish poster.png
| caption        = theatrical release poster
| director       = Etgar Keret Shira Geffen
| producer       = Yael Fogiel Amir Harel Ayelet Kit
| writer         = Shira Geffen 
| starring       = Sarah Adler Nikol Leidman Gera Sandler Noa Knoller Ma-nenita De Latorre Zaharira Harifai
| music          = Christopher Bowen
| cinematography = Antoine Héberlé
| editing        = François Gédigier
| distributor    = Pyramide Distribution 
| released       =  
| runtime        = 78 minutes
| country        = Israel France
| language       = Hebrew English Tagalog German
| budget         = 
}}
Jellyfish ( ; Meduzot) is a 2007 Israeli film based on a story by Shira Geffen and directed by her husband, Etgar Keret. The film tells the story of three women in Tel Aviv whose intersecting lives paint a pessimistic portrait of Israeli secular life. Batya, a waitress at weddings, comes across a mute child who seemingly emerges from the sea. Keren, a bride whose wedding Batya worked at, breaks her leg climbing out of bathroom stall and ruins her dream honeymoon in the process. And Joy, a Filipina domestic, attends to her employer with whom she struggles to communicate. Poetic imagery draws connections between the lives of these women, all of whom find solace in the sea.

Jellyfish was the winner of the 2007 Camera dOr at the Cannes Film Festival, the Official Selection at the 2008 Toronto Film Festival, and the Official Selection at the 2008 Telluride Film Festival. The film stars Sarah Adler and Gera Sandler. The DVD release date was September 30, 2008.

==Themes==
The film begins and ends with the little girl in the presence of its main character, Batya at the sea- when first it appears, coming out, and ending with her following the tot into it, almost drowning. Instead her friend Malka pulls her out. She is revived, appearing relieved to find that she still has her friend.

==Critical reception==
The film received generally positive reviews from Western critics. The review aggregator Rotten Tomatoes reported that 88% of critics gave the film positive reviews, based on 49 reviews. 
Metacritic reported the film had an average score of 68 out of 100, based on 11 reviews. 

==References==
 

== External links ==
*  
*  
*  
*  
*   - Israel Movie Website entry (Hebrew)
*   - Israeli Film Fund website entry (Hebrew)
*   - Zeitgeist Films official site
*   - budget entry

 

 
 
 
 
 