The Conrad Boys
{{Infobox film
| name           = The Conrad Boys
| image          = Conradboys.jpg
| caption        = Movie Cover
| director       = Justin Lo
| producer       = Justin Lo &Jose Ramirez
| writer         = Justin Lo
| starring       = Justin Lo, Nick Bartzen, Boo Boo Stewart, Barry Shay
| music          =
| cinematography =
| editing        =
| studio         = Newport Films
| distributor    = 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}} 
The Conrad Boys is a 2006 drama film starring Justin Lo, Nick Bartzen, Boo Boo Stewart and Barry Shay. The film was written and directed by Justin Lo.This film was produced by Justin Lo and Jose Ramizer.     

==Cast==
* Justin Lo as Charlie Conrad
* BooBoo Stewart as Ben Conrad
* Nick Bartzen as Jordan Rivers
* Barry Shay as Doug Conrad
* Nancy Hancock as Tori Marshall
* Katelyn Ann Clark as Louise Denver
* Dorian Frankel as Evelyn Bridge
* Lauren Xerxes as Suzie Conrad
* Bruce Blauer as Vince Miller
* Shane Arenal as Andy Calhoun
* Bart Shattuck as Attorney Mark Poland
* Connie Schiro as Principal Brower
* Kari McDermott as Paula
* Keegan Bell as PJ
* Wesley Stiller as Keaton

==Release==
The Conrad Boys was released on April 24, 2006 at the Newport Beach International Film Festival  and was released into the theaters in United States on June 6, 2006 and was released to DVD on August 15, 2006.   

==See also==
*List of lesbian, gay, bisexual, or transgender-related films by storyline

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 


 