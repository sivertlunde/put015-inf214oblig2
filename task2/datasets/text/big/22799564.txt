Ordinary People (2009 film)
{{Infobox film
| name           = Ordinary People
| image          = 
| caption        = 
| alt            = 
| director       = Vladimir Perišić
| producer       = Anthony Doncque Pierre-Alain Meier Miroslav Mogorovich Nadezda Perišić Vladimir Perišić Miléna Poylo Gilles Sacuto Szabolcs Tolnai
| writer         = Vladimir Perisic  Alice Winocour Miroslav Stevanović
| music          = 
| cinematography = Simon Beaufils
| editing        = Martial Salomon
| studio         = TS Productions
| distributor    = Pyramide Distribution
| released       =  
| runtime        = 80 minutes
| country        = France, Serbia, Netherlands, Switzerland
| language       = Serbian
| budget         = 
| gross = 
}}
Ordinary People is a 2009 French and Serbian film directed by Vladimir Perišić, starring Relja Popović, Boris Isaković, and Miroslav Stevanivić. Director Vladimir Perišić tells the story of a soldier faced with mission of executing prisoners in an unspecified place and time. It is Perišićs first feature film. The film has won several awards from various film festivals.

==Plot==
The film follows a hot summer day in the life of Dzoni (Relja Popović), a twenty-year-old recruit, who is sent to an abandoned farm on an unknown mission. The soldiers wait to battle unnamed terrorists, but instead, a bus full of prisoners arrives at the barracks. The commander orders the soldiers to execute the prisoners. At first, Dzoni is shocked by the cruel killings, but as more prisoners arrive, he begins to enjoy the executions.

==Cast==
*Relja Popović as Dzoni
*Boris Isaković as Kouki Miroslav Stevanović as Ivan
*Miroslav Isaković as Micha

==Release==
The film was released on 26 August 2009 in France.

===Film Festivals===
The film was featured in the 2009 Sarajevo Film Festival where it won the Heart of Sarajevo Award for Best Film. Relja Popović won the Best Actor award.  The film also won the Cineuropa Prize for Best European film in the Miami International Film Festival in 2010.  The film received a special mention in the 2010 Trieste Film Festival.  The film was shown in the 2009 International Critics Week in Cannes, France. 

==Themes==
The film deals with the banality of violence. The soldiers are ordered to kill the prisoners, but they are never given a reason. They become used to the killings, losing their morality and conscience.       The juxtaposition of the idyllic countryside setting and the senseless murders give the movie a sense of absurdity.    The jury from the 2009 Sarajevo Film Festival emphasized "the universal pattern of abuse of male youth through the military structure".   

==Development==
The director lived in Belgrade during the Yugoslav Wars, where "the violence was omnipresent in our lives but indirectly".  The film, however, does not directly state the setting, leaving the audience to decide where the film takes place.  Perišić stated that the film portrays how brutal acts, like those in the Yugoslav Wars, were not committed by "monsters" but rather "ordinary men".   The director describes the film as "some kind of laboratory experiment". The film did not hire professional actors, and the script was written as the filming occurred. Filming took place in order, which is unusual for most movies.  

==Reception== existentialist novels.   

==Notes==
 

== References ==
 

== External links ==
*  
*  
*  

 
 
 
 
 
 
 