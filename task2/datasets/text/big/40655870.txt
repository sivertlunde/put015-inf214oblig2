Música de siempre
 
Música de siempre is a 1958 film produced by the A.N.D.A. that provided work for hundreds of union members at a time when the Regent of Mexico City Ernesto P. Uruchurtu cracked down on the entertainment business. The film is a combination of acts strung together with a story of a theater janitor, played by the comedian Manuel Valdes, who tries to persuade two film producers to make a movie about music. Among the entertainers he suggests for the movie is Libertad Lamarque, Toña la Negra, Edith Piaf, and nine-year-old Angélica María, along with Altia Michel, German Valdes, Agustín Lara, Alejandro Algara and Tongolele.
Música de siempre was one of the first Mexican movies filmed in color. The lavish colorful sets and costumes created a spectacular that was not only entertaining, but is a historical record of some of Mexicos most famous entertainers.

==References==

 

==External links==
*  

 
 
 
 
 

 