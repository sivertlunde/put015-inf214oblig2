Iti Srikanta
{{Infobox film
| name           = Iti Srikanta
| image          = Iti_Srikanta_DVD_cover.png
| image_size     = 200px
| border         = 
| alt            = DVD cover of the film
| caption        = DVD cover of the film
| director       = Anjan Das
| producer       = Shantasree Sarkar
| writer         = 
| screenplay     = Rajarshi Roy Shantasree Sarkar
| story          = 
| based on       =  
| narrator       = 
| starring       = Adil Hussain Soha Ali Khan Reema Sen
| music          = Bikram Ghosh
| cinematography = Shirsha Roy
| editing        = Sanjib Datta
| studio         = 
| distributor    = 
| released       =  2004
| runtime        = 
| country        = India
| language       = Bengali
| budget         = 
| gross          = 
}}
 2004 Bengali Bengali period drama film directed by Anjan Das. This film is based on novelist Sarat Chandra Chattopadhyays novel, Srikanta (1917)   In an interview in 2005, Das, director of the film, told about his depiction of the character Srikanta– "Several films have been made on Srikanta but all these had mainly dealt with specific chapters from Srikanta, the novel. I wanted to present the complete man — his inner conflict and turmoil over the two women in his life, torn between a baiji and a vaishnavi".     He also felt, the crisis felt by Srikanta as shown in Sarat Chandra Chattopadhyays novel are still relevant.
 2004 Anandalok Awards, Anjan Das won the award for Best Director. 
{{cite news
|last=
|first=|date=8 November 2004
|title=Star-struck night on stage and floor
|url=http://www.telegraphindia.com/1041108/asp/calcutta/story_3979680.asp
|location=Calcutta, India
|newspaper=The Telegraph 
|accessdate=19 November 2008
}}
 

== Plot ==
The film is a love triangle between a young man Srikanta (Adil Hussain) and two women in his life, Rajalakshmi (Reema Sen) a rich courtesan  and Kamalata (Soha Ali Khan), a vaishnavite living in an ashram. 

== Cast ==
* Adil Hussain as Srikanta
* Soha Ali Khan as Kamallata
* Reema Sen as Rajlakshmi
* Pijush Ganguly as Gahar
* Aparajita Ghosh Das as Padma

== References ==
 

== External links ==
*  

 
 
 
 
 