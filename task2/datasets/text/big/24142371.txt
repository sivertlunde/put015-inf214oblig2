The Girl from Calgary
{{Infobox film
| name           = The Girl from Calgary
| image          =
| image_size     =
| caption        =
| director       = Phil Whitman
| producer       = I. E. Chadwick
| writer         = Lee Chadwick (continuity) Lee Chadwick (dialogue) Leon DUsseau (story) Sid Schlager (story)
| starring       = See below
| music          = Albert Hay Malotte
| cinematography = Harry Neumann
| editing        = Carl Pierson
| studio         = Chadwick Productions
| distributor    = Monogram Studios
| released       = 24 October 1932
| runtime        = 64 minutes
| country        = United States
| language       = English}}

The Girl from Calgary is a 1932 American musical comedy film directed by Phil Whitman.

==Plot summary==
A French-Canadian girl is a champion bronc rider and is also a nightclub singer. An ambitious young man sees her act one night and is struck by her talent, realizing that she is good enough to become a Broadway star. 

He convinces her to accompany him to New York, where she indeed does become a Broadway star. However, the young man finds himself being squeezed out by greedy Broadway producers who see the talented young girl as their own personal gold mine.

==Cast==
*Fifi DOrsay as Fifi Follette Paul Kelly as Larry Boyd
*Robert Warwick as Bill Webster
*Edwin Maxwell as Earl Darrell
*Astrid Allwyn as Mazie Williams
*Edward Fetherston as Monte Cooper

==Production background==
* The first reel, with an elaborate musical number, is taken from The Great Gabbo (1929) which had one sequence filmed in Multicolor.
* When originally released, the first reel of The Girl From Calgary, approximately seven minutes including the title credits, was in 2-strip Magnacolor. Reviewers at the time commented on the poor quality of the color, registration problems, and lack of focus. In surviving prints, this sequence is in black-and-white, with a replaced title card that includes a 1951 copyright statement.

==External links==
* 
* 
* 

 

 
 
 
 
 
 
 
 
 
 

 
 