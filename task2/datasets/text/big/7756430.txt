Les Ordres
{{Infobox film 
| name = Les Ordres
| image = 
| caption = 
| starring = Jean Lapointe  Hélène Loiselle  Guy Provost  Claude Gauthier  Louise Forestier
| director = Michel Brault
| writer = Michel Brault
| producer = Gui Caron  Bernard Lalonde
| cinematography = Michel Brault  François Protat 
| editing        = Yves Dion
| released =  
| runtime        = 109 minutes
| country =  Canada
| language = French
| music          = 
}}

Orders (original title: Les Ordres, known in the United States as: Orderers) is a 1974 Quebec historical drama film about the incarceration of innocent civilians during the 1970 October Crisis and the War Measures Act enacted by the Canadian government of Pierre Trudeau. It is the second film by director Michel Brault. It features entertainer and Senator Jean Lapointe.

The film tells the story of five of those incarcerated civilians. It is scripted but is inspired by a number of interviews with actual prisoners made during the events and its style is heavily inspired by the Quebec school of Cinéma vérité. It is a docufiction.
 Best Foreign Language Film at the 48th Academy Awards, but was not accepted as a nominee.  The film was selected to be screened in the Cannes Classics section of the 2015 Cannes Film Festival.   

== Cast ==
* Jean Lapointe - Clermont Boudreau
* Hélène Loiselle - Marie Boudreau
* Guy Provost - Dr. Jean-Marie Beauchemin
* Claude Gauthier - Richard Lavoie
* Louise Forestier - Claudette Dusseault

== Awards == Best Director    (Michel Brault, tied with Costa Gavras for Section spéciale)
* 1975 Cannes Film Festival, nominated for Golden Palm (Michel Brault)
* 1975 Canadian Film Awards, won for Best Feature Film (Claude Godbout, Guy Dufaux and Bernard Lalonde)
* 1975 Canadian Film Awards, won for Film of the Year (Guy Dufaux, Claude Godbout and Bernard Lalonde)
* 1975 Canadian Film Awards, won for Best Original Script (Michel Brault)
* 1975 Canadian Film Awards, won for Best Direction (Michel Brault)
* The Toronto International Film Festival ranked it in the Top 10 Canadian Films of All Time four times, in 1984, 1993, 2004 and 2015. 

== See also ==
* Docufiction
* List of docufiction films
----
*List of Quebec films
*Cinema of Quebec
*Culture of Quebec
*Quebec independence movement
*History of Quebec
*List of submissions to the 48th Academy Awards for Best Foreign Language Film
*List of Canadian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
* 
* 
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 