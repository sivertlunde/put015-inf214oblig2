La Cambrure
{{Infobox film
| name           = La Cambrure  (aka: The Curve) 
| image          = 
| alt            = 
| caption        = 
| film name      = 
| director       = Edwige Shaki
| producer       = Françoise Etchegaray
| writer         = Edwige Shaki
| screenplay     = 
| story          = 
| based on       =  
| starring       =  
| narrator       = 
| music          = 
| cinematography = Diane Baratier
| editing        = Éric Rohmer Compagnie Eric Rohmer (CER)
| distributor    = 
| released       = May, 1999  {Cannes Film Festival}  
| runtime        = 16 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}
 French short directed by Edwige Shaki who also wrote the scenario. Éric Rohmer was a technical advisor and editor for the movie. Despite being directed by Shaki, some of Rohmers trademarks, extensive dialog and beautiful young actors, including Shaki herself, are present. 
This short film was the first digital cinema production to be presented in a commercial theater, at the Cannes Film Festival in May 1999.    The short was produced in preparation for Rohmers feature film  The Lady and the Duke, in which Shaki also appeared. 

==Summary==
As an art student, Roman (François Rauscher) falls in love with an art model (Edwige Shaki) who reminds him of a woman once sculpted by his father.

==Cast==
* Edwige Shaki as Eva
* François Rauscher as Roman
* André Del Debbio as Le sculpteur

==DVD release== Criterion Collection fetishisation of the female body.

==Reception==
Tim Lucas of Sight & Sound described the short as "a delightful exploration of arts role in sexual aesthetics and the objectification of desire". 

==References==
 

==External links==
* 

 
 
 
 


 