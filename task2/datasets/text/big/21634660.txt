Papa, Can You Hear Me Sing
{{Infobox film name = Papa, Can You Hear Me Sing image = Papa, Can You Hear Me Sing.jpg alt =  caption =  traditional =  simplified =  pinyin = }} director = Yu Kanping producer =  writer =   starring =  music = Su Rui cinematography =  editing =  studio =  distributor =  released =   runtime =  country = Taiwan language = Mandarin budget =  gross = 
}}
 
Papa, Can You Hear Me Sing ( ) is a 1983 Taiwanese musical film directed by Yu Kanping (虞戡平) starring Sun Yueh (孫越) and Linda Liu (劉瑞琪).  This film was released 8 times in Taiwan and 11 times in Hong Kong and won 4 Golden Horse Awards.  The theme song Any Empty Wine Bottles For Sale ( ) by Su Rui is also famous.

Da Cuo Cue (搭错车)(2005) is a popular 22 episodes TV series produced in Mainland China rewritting the film plot starring Li Xuejian (李雪健) and Li Lin (李琳). 

==Plot==
The movie centres around the lives of a speech-impaired army veteran and his adopted daughter. He works as a bottle recycler either buying used bottles or picking up discarded bottles with his tricycle wagon. He lives within a shanty ghetto part of the city with his woman companion who is dependent on him bringing back a bottle of saki every evening. He is affectionately known as "Uncle" in the ghetto.

One morning, on one of his daily collection trips, he chanced upon an abandoned baby girl in a basket with an attached note that says "Please give baby Mei a good home." He brings baby Mei home to raise as his own.

However, his companion is visibly upset with the presence of baby Mei and with the attention he lavishes on baby Mei. The next evening, rather than his usual spending a portion of his daily proceeds on a bottle of saki, he decides to buy a can of powdered condensed milk for his adopted baby.

On reaching home, his enthusiasm is dashed along with the can of condensed milk his companion throws on the floor when his companion discovers she would have no saki but milk for the evening. His companion grows violent and bruises his eye. The next evening, on his return, he enthusiastically brings home a bottle of saki, but his home is silent. His companion had decided to leave him and had left baby Mei with a neighbour.

Meis adopted father dotes on her and makes her the centre of his life. Her neighbourhood is also very protective of her. Along with the joys and travails of the shanty neighbourhood Mei shared with her father, she grows up into a beautiful young woman.

Mei meets a singer-song writer and they traverse the bar scene as a singing couple. They are talent-spotted by a record producer-manager who is looking out for new blood to replace his aging artiste. However the record producer only wants Mei, not her composer boyfriend.

Mei signs a contract with the producer as her manager. Her manager reinvents Meis image by masking her native and Taiwanese Hokkien linguistic origins but portraying her as a product of a rich and respectable family who has since emigrated to the United States but that Mei had decided to stay behind to pursue her singing career. In doing that, her manager decided to sever her ties to her family, ghetto and native origins.

Meis popularity explodes while her boyfriend singer-song writer languishes along with her father and friends. Her neighbours are upset that she has abandoned her father in her pursuit of fame and fortune. She goes on a regional concert tour. On her return, her neighbours and dad shows up at a publicity party her manager threw for her. Her manager denies her relationship with her father at the publicity party. She shuns her father and friends.

One evening, on an attempt to visit her father, she realises she had not even known that her shanty ghetto had been forcibly demolished by city authorities and that she is unaware of the location to which the city had relocated her neighbourhood. Coincidentally, his fathers former girlfriend comes asking her about the whereabouts of a friend she has missed for twenty years but both Mei and her fathers former girlfriend is unaware of their linked relationships.

Her father grows depressed and reflects on the years of attention he doted on her daughter. He collapses from depression and hypertension while watching her concert on TV until his neighbours. He is brought to the hospital dying and his widowed neighbour, with whom he has grown intimate, rushes to the concert to get Meis attention to inform her of her dying father. Mei rushes but reaches the hospital too late. She breaks down in tears.

On the aftermath of her fathers demise, an abruptly dejected Mei receives a set of song scripts from her estranged composer boyfriend. Her boyfriend had embedded native lyrics into the song which would remind her of her native origins and the devotion  her father had for her, rather than the faux origins her manager had projected. The native words are her fathers bottle-recycling call "Any bottles to be sold (for recycling)?" She takes up the challenge to sing the song which forms the musical crescendo of the movie.

== Social-political backdrop of the plot == cultural tensions between citizens of mainland Chinese and native Taiwanese origins. 
{{cite web
| url = http://www.atimes.com/atimes/China/FC20Ad04.html
| title = Whos the real Taiwanese
| author = Wei Yun
| publisher = Asia Times Online
| date = 2004-03-20
}}
  
{{cite web
| url = http://articles.latimes.com/1995-12-10/news/mn-12530_1_lee-teng-hui
| title = Taiwans Native-Son President Epitomizes Power Shift on Island - Asia: Lee Teng-hui appears to have a lock on nations first direct vote for its leader. The mainland elite are in decline
| author = RONE TEMPEST
| publisher = LA Times
| date = 1995-12-19
}}
  
{{cite web
| url = http://www.ksc.kwansei.ac.jp/~jed/MultilingMulticult/Taiwan/TaiwanLgNames.html
| title = The politics of language names in Taiwan
| author = J. DeChicchis
| work = Studies in Language and Culture
| date = 1995
}}
  It also depicts, for the first time in Taiwanese cinema, the controversial process of demolition-and-relocation. 
{{cite web
| url = http://www.amazon.com/Painting-City-Red-Contract-Asia-Pacific/dp/0822347237
| title = Painting the City Red: Chinese Cinema and the Urban Contract
| author = Yomi Braester
| date = 2010
| pages = 195-200
}} 
 

==Broadcasting== STAR TV Mandarin Movies Channel (臺灣衛星電視華語電影台) channel 4 on 10 June 1994 and was watched by 10 million viewers. Channel 8 English subtitles.
* Hong Kong: The film premiered on channel 10 on 10 November 1993 with Cantonese dubbing and Chinese subtitles. Indonesian dubbing. STAR Space Channel (星空衛視) on 21 June 2003 with Chinese dubbed captionings and Chinese-English subtitle. TV3 on Malay subtitles.
* Star Chinese Movies Legend digitally retrieved the movie on HD and it is currently available on demand on Mio TV.

==References==
 

 