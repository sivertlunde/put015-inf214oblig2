Heaven's Lost Property the Movie: The Angeloid of Clockwork
{{Infobox film
| name           = Heavens Lost Property the Movie: The Angeloid of Clockwork
| image          = Sora no Otoshimono movie.jpg
| alt            = 
| caption        = Japanese theatrical release poster featuring, from left to right, Astraea, Ikaros, and Nymph with Hiyori Kazane in the background
| director       = Hisashi Saitō (Chief director) Tetsuya Yanagisawa
| producer       = 
| screenplay     = Yūko Kakihara
| based on       =   Mina Soichiro Hoshi Ayahi Takagaki Tatsuhisa Suzuki AIC A.S.T.A
| music          = Motoyoshi Iwasaki
| cinematography = 
| editing        = Takashi Sakurai
| distributor    = Japan:   
| released       =  
| runtime        = 97 Minutes
| country        = Japan
| language       = Japanese
|}}
 |Gekijōban Sora no Otoshimono: Tokei-jikake no Enjeroido}} is a 2011 Japanese anime fantasy film based on the manga and anime series Heavens Lost Property by Suu Minazuki. It was released on June 25, 2011 in Japan under the directorship of Hisashi Saitō. The film covers events involving Hiyori Kazane.  The DVD version was released on February 24, 2012 in Japan  and on February 26, 2013 in North America. {{cite web
| url = http://www.youtube.com/watch?v=rbtbag9nasI
| title = Heavens Lost Property: The Angeloid of Clockwork Movie - Available 2.26.13 - Trailer
| author = Funimation
| authorlink = Funimation
| date = January 3, 2013
| format = YouTube
}} 

==Synopsis== Tomoki and Sohara grown Daedalus who Hiyori Kazane, a resident of Sorami City and the films main heroine, introduces herself and explains how she met and fell in love with Tomoki, while also narrating about how she took part in several events from the two previous anime seasons, all the while observing Tomoki and his friends, and even becoming acquainted with most of them.
 Sugata is Mikako mentions Astraea believe Master of the Synapse appears and taunts Sugata with his forgotten memories and mentions that Hiyori is awakening. Tomoki, unaware of Hiyoris fate, waits for her but when Nymph and Ikaros arrive, the former lies to him and tells him Hiyori will meet him tomorrow. To make up for her lying, Nymph spends the night with Tomoki and asks for the imprinting, but Tomoki explains that he wants her and the other Angeloids to be free. Nymph says she wants to be imprinted because she is in love with Tomoki, but before she can convey her feelings Tomoki experiences severe headaches from the memory wipe, so Nymph holds him, knowing that by tomorrow Tomoki will have forgotten Hiyori.

The next morning, Tomoki still clearly remembers Hiyori, while everyone else has forgotten her. Distraught, Tomoki runs over town searching for her, while in Synapse Hiyori is grief-stricken that her time on Earth has ended, so the Master of the Synapse offers her one more chance to see Tomoki. Tomoki confronts Ikaros and Nymph on what is happening, but Nymph explains they cannot tell him anything for fear of Tomoki learning that Hiyori is not the only part of his reality that isnt real. Tomoki collapses from the severe headache pain, so the two Angeloids including Astraea look after him. Nymph picks up something on her radar, so the three go to investigate, and find Hiyori. The reunion is cut short when, she is transformed into an Angeloid and is ordered by the Master of the Synapse to kill the three. Hiyori uses her ability of time manipulation to attack both Sorami City and the Angeloids, and easily overpowers the three. Nymph determines the only way to stop Hiyori is to kill her, though Ikaros objects, but Nymph reluctantly prepares to do so, until she is stopped by Tomoki, who swears to save Hiyori. The Master of the Synapse orders Hiyori to kill Tomoki, but Sugata arrives to help, while the three Angeloids stall Hiyori. They manage to destroy the structure that binds her, but it does not free her. Nymph also realizes that the dimensional mechanism system that produces Hiyoris time altering powers has reached the breaking point causing a process that will result in the area around Hiyori, Sorami City, to be blown away. Unable to stop the process, Ikaros traps Hiyori within her Aegis to contain the blast, but also traps herself in since she does not want Hiyori to be alone in her final moments. Tomoki however glides to them and is let into the Aegis, where he is able to bring Hiyori back to her senses. Hiyori begs for them to get away as it wont be long before she self-destructs, so she uses her hacking abilities to dispel the Aegis, and kisses Tomoki before leaving. She thanks him and remembers how they first met as children, and that it was because of him that she came to love Sorami City. She notes that while her time on Earth was short, she enjoyed her time with everyone and thanks her friends and Tomoki, and reaffirms her love for him before being destroyed. Tomoki breaks down in tears, and the film ends with him swearing that he will never forget Hiyori.

Near the end of the credits, Sohara is seen viewing the scrapbook, filled with pictures of the group having fun. As Sohara leaves, a picture where Hiyori had faded away due to the Synapses deletion, is seen where Hiyori slowly fades back in, revealing that she has somehow been revived.

In a post-credits scene, Tomoki stands at the spot overlooking the city where he and Hiyori first met and behind him a ray of light shines down with feathers falling from the sky, when he looks he smiles happily, and as the screen fades out, the sound of Hiyoris bell ornaments are heard, that she was somehow revived.

==Songs==
;Opening theme
*"Second"
**Lyrics: Seiji Miura
**Composition: Asu (of The New Classics)
**Arrangement: Atsushi Umebari
**Artist: Blue Drops (Hitomi Yoshida and Ikaros (Saori Hayami))

;Ending theme 
* 
**Lyrics: Natsumi Tadano
**Composition: Katomi Yazu
**Arrangement: Kenta Miyahara
**Artist: Blue Drops

;Insert songs 
* 
**Lyrics: Suu Minazuki
**Composition: Seiji Miura
**Arrangement: Seiji Miura
**Artist: Takayuki Miyauchi, Tomoki Sakurai (Soichiro Hoshi) and Sohara Mitsuki (Mina (voice actress)|Mina)

* 
**Lyrics: Seiji Miura
**Composition: Motoyoshi Iwasaki
**Arrangement: Motoyoshi Iwasaki
**Artist: Hiyori Kazane (Yōko Hikasa)

==Reception==
 

==References==
 

==External links==
*  
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 
 