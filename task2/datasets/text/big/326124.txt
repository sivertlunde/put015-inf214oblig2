Irma Vep
 
 
{{Infobox film
| name           = Irma Vep
| image          = irmavep.jpg
| caption        =
| director       = Olivier Assayas
| producer       = Georges Benayoun
| eproducer      = Françoise Guglielmi
| aproducer      =
| writer         = Olivier Assayas
| starring       = Maggie Cheung Jean-Pierre Léaud Nathalie Richard
| music          =
| cinematography = Eric Gautier
| editing        = Luc Barnier
| distributor    = Dacia Films
| released       = November 13, 1996
| runtime        = 97 minutes
| country        = France
| awards         =
| language       = French/English
| budget         =
| preceded_by    =
| followed_by    =
}}
 silent film serial Les vampires. Taking place as it does largely through the eyes of a foreigner (Cheung), it is also a meditation on the state of the French film industry at that time.

The film was screened in the Un Certain Regard section at the 1996 Cannes Film Festival.   

== Plot ==
Cheung is employed to play the Story within a story|film-within-the-films heroine, Irma Vep (an anagram for vampire), a burglar, who spends most of the film dressed in a tight, black, latex rubber catsuit, defending her directors odd choices to hostile crew members and journalists.  As the film progresses, the plot mirrors the disorientation felt by the films director.  Cheung the character is in many ways seen by other characters as an exotic sex object dressed in a latex catsuit; both the director and Cheungs costume designer Zoe (Nathalie Richard) have crushes on her.

The film makes reference to iconic figures in French film history: Louis Feuillade, Musidora, Arletty, François Truffaut, the Groupe Chris Marker#SLON and ISKRA (1967–1974)|SLON, Alain Delon, and Catherine Deneuve.  Thematically, the film questions the place of French cinema today.  It is not a “mourning for cinema with the romantic nostalgia” but “more like the Mexican Day of the Dead: remembrance as an act of celebration,”  so that “It is less a film about re-presenting the past, than it is a film about addressing the present, specifically the place of France within the global economy.” 

== Cast ==
 
 
* Maggie Cheung as Herself
* Jean-Pierre Léaud as René Vidal
* Nathalie Richard as Zoé
* Antoine Basler as Journalist
* Nathalie Boutefeu as Laure
* Alex Descas as Desormeaux
* Dominique Faysse as Maïté
* Arsinée Khanjian as Laméricaine
* Bernard Nissile as Markus
* Olivier Torres as Ferdinand and Moreno
* Bulle Ogier as Mireille
* Lou Castel as José Mirano
* Jacques Fieschi as Roland
* Estelle Larrivaz as La standardiste
* Balthazar Clémenti as Robert, assistant

== Production ==
The idea for the film was born out of an attempted collaboration between Assayas, Claire Denis, and Atom Egoyan, who wanted to experiment with the situation of a foreigner in Paris.  In the 1915 original serial, written and directed by Louis Feuillade, Irma Vep was played by French silent film actress Musidora (1889–1957). Much of the film depicts set-related incidents that echo scenes in François Truffaut|Truffauts La nuit americaine (English title: Day for night), to which Irma Vep owes a large thematic debt.

However, Assayas has publicly stated that although he considers La nuit americaine a great film, it is more about the fantasy of filmmaking than the reality.  Assayas credits Rainer Werner Fassbinders Beware of a Holy Whore as a much greater inspiration.

Assayas married Cheung in 1998. They divorced in 2001.  They again collaborated in 2004 on the film Clean (film)|Clean.

==References in other media==
 White Rings video for their single IxC999.

==See also==
* Louis Feuillade, director of the original serial film Les vampires (1915–1916)
* The Mystery of Irma Vep, a 1984 play for two characters by Charles Ludlam
* Lettrism, inspiration for some sequences of the film

== References ==
 

== External links ==
*  
*  
*  
*  

 

 
 
 
 
 
 