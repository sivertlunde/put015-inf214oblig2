The Book of Mormon Movie
 
{{Infobox film
| name = The Book of Mormon Movie, Volume 1: The Journey
| image = DVD cover of the movie The Book of Mormon Movie, Vol. 1.jpg
| alt = 
| caption = DVD cover
| director = Gary Rogers
| producer = Gary Rogers
| writer = {{Plainlist|
* Gary Rogers
* Craig Clyde}}
| starring = {{Plainlist|
* Noah Danby
* Bryce Chamberlain
* Jan Broberg Felt
* Cragun Foulger
* Mark Gollaher
* Kirby Heyborne
* Bruce Newbold
* Bern Kubiak}}
| music = Robert C. Bowden
| cinematography = Neal Brown
| editing = {{Plainlist|
* Ira Baker
* David Hales}}
| studio = Mormon Movies Halestone Distribution
| released =  
| runtime = 119 minutes
| country = United States
| language = English
| budget = $1.5 million 
| gross = $1.7 million 
}} adventure drama limited theatrical release on September 12, 2003.

==Plot==
The movie is based on the first two books of the Book of Mormon. It is the story of  , Lemuel (Book of Mormon)|Lemuel, Sam (Book of Mormon)|Sam, and Nephi, son of Lehi|Nephi. Lehi leaves Jerusalem in 600 B.C. after having prophesied concerning the destruction of Jerusalem and people wanted to kill him. He journeys into the wilderness with his family. He sends Nephi and his brothers back to Jerusalem after the brass plates and the family of Ishmael. The sons and daughters of Lehi marry the sons and daughters of Ishmael. They take their families and continue into the wilderness. Ishmael dies in the wilderness. They come to the sea. Nephis brothers rebel against him; he confounds them, and builds a ship. They cross the sea to the promised land in the Americas. Lehi dies in the promised land. Nephis brethren rebel against him again. The plot concludes with Nephi departing again into the wilderness.

==Cast==
  Nephi
* Lehi
* Jan Broberg Felt as Sariah Lemuel
* Mark Gollaher as Laman Sam
* Moroni
* Bern Kubiak as Jesus Christ
* Jacque Gray as Nephis wife
* Ron Frederickson as Ishmael
* Todd Davis as Zoram
* Michael Flynn as Laban
* Richard J. Clifford as Lucan
* Brad Johnson as Jonathan
 

==Production==
===Development=== The Ten Commandments. He envisioned The Book of Mormon as one long historical epic. His plan was to make nine films that cover the entire story of the book.

The films length is two hours, and it was revealed on the DVD commentary that the first cut of the film was two hours and forty minutes.

===Casting===
Noah Danby was cast as Nephi because of his strong resemblance to the art of Arnold Friberg.  He had never read the Book of Mormon prior to his casting. Danby is a devout Lutheran, and while at first he didnt feel comfortable in making the film due to religious differences, he has said in an interview for the Hollywood Reporter that he took the role to gain experience as an actor.

===Filming===
The desert scenes were filmed in Utah in the spring, and it was very cold. The "great and spacious building" was a five-foot miniature. The boat does not appear in the theatrical version of the scene in which the family arrives in the promised land. It was digitally added to that scene for the DVD version.

The costume designer used Mormon art and illustrations to guide her selections. 

The film was mentioned in Paul C. Gutjahrs 2012 book The Book of Mormon: A Biography. 

==Release== Nephi with blood splatter on his face after beheading Laban (Book of Mormon)|Laban. This image was removed for home media releases, and the film received a PG rating on DVD.

===Box office===
Produced for $1.5 million, Book of Mormon opened in 29 theaters on September 12, 2003 and made $114,573 in its first weekend, ranking number 41 in the domestic box office.  The film played for 35 weeks before closing on May 13, 2004, its widest release being 38 theaters, and it had grossed $1,680,020.   

It is the fourth highest-grossing film in the history of LDS cinema. 

===Critical reception===
The film was widely panned by Mormon  and non-Mormon critics.  Variety (magazine)|Variety described it as " ell meaning but often as tediously earnest as a Sunday sermon".  In the Bloggernacle, A Motley Vision gave it a grade of C–. 

Review aggregator website Rotten Tomatoes scored 17% of 6 critics giving the film a positive review. 

==Soundtrack==
 .]]
{{Infobox album
| Name        = Book Of Mormon Movie, Volume 1: The Journey
| Type        = Soundtrack
| Artist      = Robert C. Bowden
| Cover       = 
| Released    = 2003
| Recorded    = 
| Genre       = 
| Length      = 01:06:08
| Label       = Mormon Movies, L.L.C. 
| Producer    = 
| Reviews     = 
}}
 
# "Prologue/Joseph Meets Moroni" (01:48)
# "Main Theme" (02:31)
# "Playing Ball" (00:19)
# "I Nephi" (01:48)
# "Lucan Gets Laban" (01:39)
# "We Shall Never See This House Again" (01:32)
# "Leaving Jerusalem" (01:34)
# "In the Presence of Diety" (02:26)
# "Brothers Return From Brass Plates" (01:38)
# "Lamans Chase" (00:48)
# "Nephi Sneaking Into Jerusalem" (02:15)
# "Beheading of Laban" (02:28)
# "Returned to the Tent of My Father" (00:38)
# "Return for Ishmaels Family" (01:15)
# "Love Theme" (03:37)
# "Nephis Vision" (03:26)
# "Wedding & Celebration" (02:55)
# "Wandering in the Desert" (02:36)
# "Ishmaels Death/Bountiful" (03:03)
# "Enticing" (01:35)
# "Storm at Sea" (03:00)
# "The Promised Land" (03:42)
# "Lehis Death" (03:33)
# "Attack at Night" (00:48)
# "I Miss My Brothers" (02:25)
# "Sams Journey" (00:53)
# "Lamanites" (02:29)
# "End Theme" (05:37)
# "Forever Will Be" (03:50)
 

==Sequel==
A  . 

==References==
 

==External links==
 
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 