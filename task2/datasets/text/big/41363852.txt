A Broadway Butterfly
{{Infobox film
| name           = A Broadway Butterfly
| image          = 
| caption        = 
| director       = William Beaudine
| producer       = Warner Brothers
| writer         = Pearl Keating Darryl F. Zanuck
| starring       = Dorothy Devore
| cinematography = Ray June
| editing        = 
| distributor    = Warner Brothers
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = Silent
| budget         = 
}}
 silent comedy film directed by William Beaudine.      A lost film with no known prints. 

==Cast==
* Dorothy Devore as Irene Astaire
* Louise Fazenda as Cookie Dale
* Willard Louis as Charles Gay John Roche as Crane Wilder
* Cullen Landis as Ronald Steel
* Lilyan Tashman as Thelma Perry
* Wilfred Lucas as Stage Manager
* Eugenia Gilbert as Riding Mistress (as Eugenie Gilbert)
* Margaret Seddon as Mrs. Steel

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 