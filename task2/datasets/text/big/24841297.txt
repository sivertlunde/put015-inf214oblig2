The Next Three Days
{{Infobox film
| name           = The Next Three Days
| image          = The Next Three Days Poster.jpg
| caption        = Theatrical release poster
| director       = Paul Haggis
| producer       = {{Plain list | 
* Michael Nozik
* Olivier Delbosc
* Paul Haggis
* Marc Missonnier
}}
| screenplay     = Paul Haggis
| based on       =  
| starring       = {{Plain list | 
* Russell Crowe
* Elizabeth Banks
* Brian Dennehy
* Olivia Wilde
* Liam Neeson Daniel Stern
}}
| music          = Danny Elfman
| cinematography = Stéphane Fontaine
| editing        = Jo Francis
| studio         = Highway 61 Films Lionsgate
| released       =  
| runtime        = 133 minutes
| country        = United States
| language       = English
| budget         = $30 million  
| gross          = $67,448,651 
}} vigilante Thriller thriller film directed by Paul Haggis and starring Russell Crowe and Elizabeth Banks. It was released in the United States on November 19, 2010 and was filmed on location in Pittsburgh.    It is a remake of the 2008 French film Pour Elle (Anything for Her) by Fred Cavayé and Guillaume Lemans.    

==Plot==
Lara Brennan ( ), a professor at a community college, becomes obsessed with the idea of breaking her out of jail, while their son Luke    ceases to acknowledge her during their prison visits, saddening Lara. One day, she attempts suicide, and tells John that she cannot survive a life in prison. John promises that this will not be her life.

John, intent on breaking her out, consults Damon Pennington (Liam Neeson), a former convict who escaped prison seven times and wrote an autobiography about it. Damon, although reluctant to help since he has turned away from his life of crime, advises John to study the prison where his wife is, warns him that the initial escape will be easy compared with evading the police after that, and explains what to do:

* He must obtain the false passports, new social security numbers, a handgun, and save up as much cash as he can.
* Since the 9/11 attacks, town security has been incredibly increased, and after breaking her out of prison and the first call, he has only 15 minutes before the city center is closed up, also counting 35 minutes for all interstates, secondary roads, train stations, docks and airports being closed.
* After a successful escape, they must escape to a country that does not extradite American citizens back stateside and with a relatively low number of American citizens.
* He must also question his resolve, and his willingness to make ruthless ethical compromises in a split-second (killing an innocent, leaving his son unprotected and alone, etc.)
* If the plan fails, they have to surrender or they will be shot on sight.

John then starts his preparation. He buys a handgun, learns to use it and obtains fake IDs. He also buys and carefully studies the map of Pittsburgh. To obtain the money, he sells most of his furniture as well as some personal belongings. At first, he attempts to break Lara out from the prison in which she is held but abandons the plan when he is almost caught testing a self-made "bump key" on an elevator.

When John is told that Lara will be transferred in 72 hours to another prison facility in the north he becomes desperate and is forced to come up with an emergency plan. Unable to get the money from his house in time he considers robbing a bank but hesitates at the last minute. Instead, John tails a local drug dealer to a drug lord, then robs him. Following clues left behind at the drug lords house, the police tracks down Johns car, get to his empty house and conclude that he is planning to break his wife out.

The following morning, John tears the map down and stuffs the pieces into several garbage bags, putting one in the trash outside his home, and the rest in a dumpster some blocks away, then leaves his son Luke at a classmates house who has a birthday party. John plants falsified blood work indicating that Lara is in a state of hyperkalemia, so she is transferred to the hospital. He gets to the hospital and helps her escape although she is doubtful and reluctant, motivated only by the idea of her son being raised without either parent.

John and Lara leave the hospital, narrowly escaping the police and make it out of the city center in less than 15 minutes; they then drive to Lukes friends house to pick him up. Finding out his son is at the zoo for the birthday party, John drives there to retrieve him, by which point the police have already established roadblocks on all interstate routes to look for "a couple and a child". John improvises by picking up an elderly couple, and the five drive through the check point without incident.

John drops off the couple and heads to the airport. Meanwhile, the police question Johns parents, but they refuse to say anything. The police examine the map fragments found in Johns garbage bag to figure out his destination, but are misled by the photos and delay the wrong flight.

A detective returns to the crime scene where Laras boss was killed. He remembers Lara saying a button popped off as the mugger bumped into her, and deduces that it must have fallen in the storm drain. He searches the storm drain but is unable to find the button. It turns out the button was there, buried under grime, and the detective just missed it. At the end of the film, the family arrives at a hotel in Caracas, Venezuela. As Lara lies down next to him, Luke kisses his mother on the forehead and falls asleep. John takes a picture of their sleeping faces as the movie ends.

==Cast==
* Russell Crowe as John Brennan
* Elizabeth Banks as Lara Brennan
* Brian Dennehy as George Brennan
* Lennie James as Lieutenant Nabulsi
* Olivia Wilde as Nicole
* Ty Simpkins as Luke
* Helen Carey as Grace Brennan
* Liam Neeson as Damon Pennington Daniel Stern as Meyer Fisk
* Kevin Corrigan as Alex
* Jason Beghe as Detective Quinnan
* Aisha Hinds as Detective Collero
* Tyrone Giordano as Mike
* Jonathan Tucker as David
* Allan Steele as Sergeant Harris Robert Diggs as Mouss
* Moran Atias as Erit
* Michael Buie as Mick Brennan
* Trudie Styler as Dr. Byrdie Lifson
* Tyler M Green and Toby J. Green as 3-year-old Luke
* Kaitlyn Wylde  as Julie

==Production==

===Pour Elle===
 
The Next Three Days is a remake of the 2008 French film Pour  Elle (Anything for Her) by Fred Cavayé.    
 French Film Festival in 2010.  Cavayé explained the plot and motivation for making the film, "We wanted to make a real human story about an ordinary man doing an extraordinary thing because hes faced with a miscarriage of justice. The film also talks about courage- saying how you show courage depending on the situation. In France, for example, there were good people who did not go into the Resistance against the Germans." 

Cavayé told The Age regarding the remake of the film by Haggis, he is eager "to be a spectator of my own film".  The director commented on the news his film would be remade by Haggis, "Its a strange feeling. I wrote this story in my very small apartment in Paris. When I saw my name next to Russell Crowe on the net, it was amazing."   

===Filming===
In October 2009, Haggis and his staff were in the principal photography stage of production filming in Pittsburgh, Pennsylvania.   On October 4, 2009, filming of the movie was ongoing and was set to complete on December 12, 2009.   
On December 14, 2009, the Pittsburgh Post-Gazette reported that filming of The Next Three Days was going to wrap that day, after 52 days of shooting. 

== Reception ==

===Release ===
In October 2009, the film was originally scheduled to be released in 2011,  by March 2010, the Australian media company Village Roadshow was set to release the film in Australia in November 2010.  It was released in the United States on November 19, 2010. 

=== Critical response ===
  was nominated for an Irish Film and Television Award for Best International Actor for his role as John Brennan. ]]
 
The Next Three Days received mixed reviews. Rotten Tomatoes gives the film a score of 52% based on review from 161 critics, with an average score of 5.9/10. The critical consensus is: "Russell Crowe and Elizabeth Banks give it their all, but their solid performances arent quite enough to compensate for The Next Three Days  uneven pace and implausible plot." {{cite web
| url= http://www.rottentomatoes.com/m/next_three_days/
| title= The Next Three Days Movie Reviews, Pictures
| publisher= Flixster
| work = Rotten Tomatoes
| accessdate= 2011-02-15
}}
 

Roger Ebert awarded the film two and a half out of four stars and said, "The Next Three Days is not a bad movie; its just somewhat of a waste of the talent involved." {{cite news
| url= http://rogerebert.suntimes.com/apps/pbcs.dll/article?AID=/20101117/REVIEWS/101119966
| title= The Next Three Days :: rogerebert.com :: Reviews
| last= Ebert |first= Roger
| authorlink = Roger Ebert
| date= November 17, 2010
| work= Chicago Sun-Times
| accessdate= November 20, 2010
}}
 

=== Box office ===
The film opened at #5 with a weekend gross of $6,542,779 from 2,564 theaters for an average of $2,552 per theater. It closed on January 6, 2011, having earned $21,148,651 domestically. The film grossed a further $46,300,000 overseas, for a worldwide total gross of $67,448,651, making it a modest success against its $30 million budget. {{cite web
| url= http://www.boxofficemojo.com/movies/?id=nextthreedays.htm
| title= The Next Three Days
| work= Box Office Mojo
| publisher= IMDb
| accessdate= 2011-02-07
}}
 

==See also==

*List of films featuring diabetes

==References==
 

==External links==
 
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 