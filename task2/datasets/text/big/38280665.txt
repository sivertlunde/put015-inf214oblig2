A Soul's Awakening
 
 
 {{Infobox film
| name           = A Souls Awakening
| image          =
| caption        =
| director       = W.P. Kellino 
| producer       = 
| writer         = Frank Powell  David Hawthorne   Flora le Breton   Ethel Oliver   Maurice Thompson
| music          = 
| cinematography = 
| editing        = 
| studio         = Gaumont British Picture Corporation
| distributor    = Gaumont British Distributors 
| released       = July 1922
| runtime        = 6,000 feet  
| country        = United Kingdom 
| awards         =
| language       = Silent   English intertitles
| budget         = 
| preceded_by    =
| followed_by    =
}} silent drama David Hawthorne, Flora le Breton and Ethel Oliver. It was made at Lime Grove Studios in Shepherds Bush. It is also known by the alternative title What Love Can Do.

==Cast== David Hawthorne as Ben Rackstraw 
* Flora le Breton as Maggie Rackstraw 
* Ethel Oliver as Sal Lee 
* Maurice Thompson as Jim Rackstraw 
* Sylvia Caine as Cynthia Dare 
* Philip Desborough as Cecil Wayne 
* Tom Morris as Mike Nolan

==References==
 

==Bibliography==
* Low, Rachael. History of the British Film, 1918–1929. George Allen & Unwin, 1971.

 
 
 
 
 
 
 
 
 
 
 