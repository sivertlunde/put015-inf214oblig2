Le Quattro Volte
{{Infobox film
| name           = Le Quattro Volte
| image          = Le Quattro Volte.jpg
| director       = Michelangelo Frammartino
| producer       = Philippe Bober Marta Donzelli Elda Guidinetti Gabriella Manfré Susanne Marian Gregorio Paonessa Andres Pfäffli
| writer         = Michelangelo Frammartino
| starring       = Giuseppe Fuda Bruno Timpano Nazareno Timpano Artemio Vellone
| music          = Paolo Benvenuti
| cinematography = Andrea Locatelli
| editing        = Benni Atria Maurizio Grillo
| studio         = Invisibile Film Ventura Film Vivo Film Essential Filmproduktion GmbH Caravan Pass Altamarea Film Ministero per i Beni e le Attività Culturali Eurimages Council of Europe Calabria Film Commission Torino Film Lab Medienboard Berlin-Brandenburg Regione Calabria ZDF Enterprises ARTE RSI-Radiotelevisione Svizzera
| distributor    = Cinecittà Luce
| released       =  
| runtime        = 88 minutes
| country        = Italy Germany Switzerland
| language       = Italian
| budget         = 
| gross          = $255,391 
}}
Le Quattro Volte ( ) is an Italian film, made in 2010, about life in the remote mountain town of Caulonia, in southern Italy.  Le Quattro Volte, Michelangelo Frammartino, 88 mins, U. Jonathan Romney. The Independent on Sunday. 29 May 2011.   

==Plot==
The film comprises four phases or turns following Pythagoras.     The turning of the phases occurs in Calabria where Pythagoras had his sect in Crotone. Pythagoras claimed he had lived four lives and this with his notion of metempsychosis is the structure of the film showing one phase and then turning into another phase.  A famous anecdote is that Pythagoras heard the cry of his dead friend in the bark of a dog. 

* The first turn is the human realm and is about an old goatherd who is quite sick and who takes medicine made from the dust from the church floor in water at night. This phase includes a long 8-minute shot of the procession of the villagers culminating in the dog and truck episode so the goats occupy the village. 
* The second turn is the animal realm and is a study of a young goat, from its birth onwards.  
* The third turn is the plant realm and is a study of a fir tree. Eventually the tree is chopped down to be displayed in the town square and an evocation of cultural memory.
*  The fourth turn shows the mineral realm as the tree is made into charcoal for the townspeoples fires. 
This phase, as charcoal is not a mineral in any modern definitions, points to a remembering of bio-cultural processes.

The fire and smoke point to carbon at the heart of the homes in the village delivered by the truck evoking human reason as the final understanding of the interaction of these turns and the true place of the human in the scheme of things.

==Production==
There is virtually no dialogue in the film. The film was written and directed by Michelangelo Frammartino  and stars Giuseppe Fuda, Bruno Timpano, Nazareno Timpano and Artemio Vellone. 

==Reception== average score of 80, based on 16 reviews, which indicates "Generally favorable reviews".   

Jonathan Romney, writing in The Independent on Sunday, described Le Quattro Volte as "both magnificent and magnificently economical", remarking "I like to think that its possible for cinema to make profound cosmological statements without having to go all Cecil B. DeMille".  Romney finds the film "the freshest and the deepest film Ive encountered in a while", and "one of those rare films that anyone could enjoy, whether or not they normally care for slow Italian art cinema". 

==Accolades==

{| class="wikitable"
|-
! Awards Group !! Category !! Recipient !! Result
|- AFM International Independent Film Festival ||!f Inspired Award|||| 
|}

==See also==
*Slow cinema

==References==
 

==External links==
*  
*  
*  

 
 
 