Bhola Bhala
{{Infobox film
| name           = Bhola Bhala
| image          = Bhola Bhala.jpg
| image_size     =
| caption        = 
| director       = Satpal
| producer       = V. Gopi Krishna
| writer         =
| narrator       = 
| starring       = Rajesh Khanna Rekha
| music          = Rahul Dev Burman
| cinematography = V. Subbarao
| editing        = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}
Bhola Bhala is a 1978 Bollywood action film directed by Satpal. The film stars Rajesh Khanna, Rekha and Moushumi Chatterjee.

==Cast==
*Rajesh Khanna ...  Ram Kumar Verma / Nathu Nathiya Singh 
*Rekha ...  Champa 
*Moushumi Chatterjee ...  Renu (as Moushumi Chatterji)  Joginder Shelly ...  Sambha 
*Satyendra Kapoor ...  Mr. Kapoor (as Satyan Kappu) 
*Sulochana Latkar ...  Renus Mother (as Sulochana) 
*Pinchoo Kapoor ...  General Manager of Jeevan Beema Co. 
*V. Gopal ...  Khalifa 
*Master Chicoo  (as Master Chikku) 
*Sudhir Thakkar ...  D.I.G. 
*Kumari Naaz ...  Sarla (as Naaz)  Mumtaz Begum  ...  Rams Mother 
*Jagdeep ...  Tamanchewala

==Soundtrack==
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1 
| "Dheere Dheere Naach"
| Kishore Kumar
|-
| 2
| "Waqt Waqt Ki Baat"
| Kishore Kumar
|-
| 3
| "Kaali Kaali Raton Mein"
| Asha Bhosle
|-
| 4
| "Jhuk Gayi Ankhen"
| Kishore Kumar, Lata Mangeshkar
|-
| 5
| "Kal Ki Fikar"
| Asha Bhosle
|}

==External links==
*  

 
 
 
 
 