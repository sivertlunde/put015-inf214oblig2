Flash Gordon's Trip to Mars
 
{{Infobox film
| name           = Flash Gordons Trip to Mars
| image          = Flashgordontriptomars.jpg
| image_size     = 
| caption        = 
| director       = Ford Beebe Robert F. Hill Frederick Stephani (uncredited)
| writer         = Ray Trampe Norman S. Hall Wyndham Gittens Herbert Dalmas Alex Raymond (based on the comic strip by)
| starring       = Buster Crabbe Jean Rogers Charles B. Middleton Frank Shannon Beatrice Roberts
| cinematography = Jerome Ash Universal Pictures
| released       =  
| runtime        = 15 chapters (299 min)
| country        = United States
| language       = English
}}

Flash Gordons Trip to Mars is a 1938 serial film of 15 episodes, based on the comic strip Flash Gordon. It is the second of three Flash Gordon serials made between 1936 and 1940.
 Richard Alexander as Prince Barin.
 Donald Kerr as Happy Hapgood, C. Montague Shaw as the Clay King, and Wheeler Oakman as Mings chief henchman.

==Plot==
When a mysterious beam of light starts disrupting and destroying the Earths atmosphere, Flash Gordon (Larry "Buster" Crabbe), Dr. Zarkov (Frank Shannon), and Dale Arden (Jean Rogers) - accidentally accompanied by wisecracking reporter Happy Hapgood (Donald Kerr) - swing into action in Zarkovs rocketship, believing that it could be coming from the planet Mongo. Once in space, however, they discover that the ray is originating from Mars.

Journeying to the fourth planet, they discover that their old enemy from Mongo, Ming the Merciless (Charles B. Middleton), whom they had believed dead, is still alive, and has formed an alliance with Azura (Beatrice Roberts), the Witch Queen of Mars. From Azuras planet, and under her protection, he is operating a gigantic Nitron ray that is destroying Earths atmosphere. Azuras powers include the ability to transmute people into figures of living clay, condemned to live in darkened caves, and she is hated and feared by most of the population. Conversely, the Clay People, led by their King (C. Montague Shaw), know the secret of how to eliminate Azuras power, but lack the means of escaping the caves to which their ruined bodies restrict them, in order to battle her.

Gordon and his party would seem to hold the answer to their problem, except that the Clay People dont trust them at first, and end up holding Dale Arden hostage. Ultimately the Earth visitors and the Clay People become allies in the tandem quest to defeat Azura and stop Ming from destroying the Earth. Flash, Dale, Zarkov, and Hapgood do battle against Azuras magic and her Martian space-force, Mings super-scientific weaponry, the treacherous Forest People, and other dangers on the Red Planet. Finally, they win by the classic strategy of divide-and-conquer, showing Azura that Ming has been plotting behind her back to take power from her.

Azuras alliance with Ming is broken, at the cost of the Queens own life, but the Clay People are freed from their curse. And the evil emperor of Mongo, his Nitron ray destroyed and his escape cut off on all sides by the now hostile Martian forces, is finally destroyed by the accidental result of his own machinations and treachery.

==Cast==
* Buster Crabbe as Flash Gordon
* Jean Rogers as Dale Arden
* Charles B. Middleton as Ming the Merciless. Ming is portrayed more in the manner of the Devil in this serial (as opposed to Fu Manchu of the first serial). {{cite book
 | last = Harmon
 | first = Jim
 |author2=Donald F. Glut 
 | authorlink = Jim Harmon
 | title = The Great Movie Serials: Their Sound and Fury
 | publisher = Routledge
 | isbn = 978-0-7130-0097-9
 | chapter = 2. "We Come from Earth, Dont You Understand?"
 | page = 39 
 | date = 1973
 }} 
* Frank Shannon as Hans Zarkov|Dr. Alexis Zarkov
* Beatrice Roberts as Queen Azura
* Donald Kerr as Happy Hapgood Richard Alexander as Prince Barin
* C. Montague Shaw as Clay King
* Wheeler Oakman as Tarnak
* Kenne Duncan as Airdrome captain
* Warner Richmond as Zandar

==Production== War of the Worlds broadcast.  According to Stedman, this serial preceded that broadcast, which made Universal hastily release a feature version of the serial as Mars Attacks the World to capitalise on the publicity. The film was a box office success. {{cite book
 | last = Stedman
 | first = Raymond William
 | title = Serials: Suspense and Drama By Installment
 | publisher = University of Oklahoma Press
 | isbn = 978-0-8061-0927-5
 | chapter = 4. Perilous Saturdays 
 | page = 105
 | date = 1971
 }} 

Flash Gordons Trip to Mars was less expensive than the first Flash Gordon serial. 

===Mars Attacks the World=== Universal Pictures Mercury Theatre The War Broadway theater as a major premiere event. The original title for this feature had been Rocket Ship, which was subsequently added to reissues of the first Flash Gordon serials feature version, shown under its source serials title in 1936.

===Television broadcasting===
In the 1950s, the three serials were broadcast on American television. To avoid confusion with a made-for-TV Flash Gordon series airing at the same time, they were retitled, becoming respectively Space Soldiers, Space Soldiers Trip to Mars, and Space Soldiers Conquer the Universe.

In the mid-1970s, they were shown by PBS stations in the USA, and by the BBC in the United Kingdom (in the UK they aired as Flash Gordon serials, under their original titles), bringing Flash Gordon to a new generation, two years before Star Wars and Close Encounters of the Third Kind re-ignited interest in science fiction.

In the UK, boosted by their having been part of George Lucass inspiration behind Star Wars, the serials continued to enjoy re-runs on the BBC, sporadically, mostly at Christmas time, into the mid-1980s.

==Critical reception== Time magazine declared the serial to be "a Grade A cinemedition of the famed King Features strip." {{cite journal
| last        = 
| first       = 
| date         =  March 28, 1938 |  title       = Also Showing
| journal     = Time magazine
| volume      = 
| issue       = 
| publisher   = 
| location    = 
| url         = http://www.time.com/time/magazine/article/0,9171,759387,00.html
| accessdate  = 2009-03-23
| quote=
}} 

==Chapter titles==
# New Worlds to Conquer
# The Living Dead
# Queen of Magic
# Ancient Enemies
# The Boomerang
# Tree-men of Mars
# The Prisoner of Mongo
# The Black Sapphire of Kalu
# Symbol of Death
# Incense of Forgetfulness
# Human Bait
# Ming the Merciless
# The Miracle of Magic
# A Beast at Bay
# An Eye for an Eye
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | chapter = Filmography
 | page = 220
 | date = 1984
 }} 

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 