Dead Body (film)
 Dead body}}
{{Infobox film
| name           = Dead Body
| image          = 
| alt            =  
| caption        = 
| director       = Kaz Rahman
| producer       = Kaz Rahman Li-Yun Chu
| writer         = Kaz Rahman Husain Naqvi Ted Hurban Katia Ustinova E. F. Morrill
| music          = Marcus Quin Hédi Hurban
| cinematography = Yuan-Chung Hsu
| editing        = Li-Yun Chu
| studio         = 
| distributor    = 
| released       =   
| runtime        = 40 minutes 
| country        = Canada USA
| language       = English
| budget         = 
| gross          = 
}}Dead Body is a 2002 short film by Kaz Rahman.

==Synopsis==
Hassan, a funeral poet, is going through writers block. Sitting at his desk in a small cramped room with his girlfriend lying on a mattress he leaves for some air- instead he drifts and deviates into much more including an endless procession of chewing gum, a rotund Hungarian delivery man and a sexy Russian woman. Hassans journey has a metaphysical aspect to it and life in this surreal world is focused on the strange work, anxiety and humour that hovers over death. 

==Cast== Husain Naqvi as Hassan
* Ted Hurban as Laszlo
* Katia Ustinova as Sveta
* Jean Burns as Girlfriend
* E. F. Morrill as Dead Body

==References==
 

==External links==
*  

 
 
 
 


 