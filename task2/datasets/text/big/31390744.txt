Ponnar Shankar (film)
 
{{Infobox film
| name           = Ponnar Shankar
| image          = Ponnar-Shankar-movie-poster.jpg
| alt            =
| caption        = Release Poster
| director       = Thiagarajan
| producer       = Thiagarajan
| writer = M. Karunanidhi
| based on       =  
| starring       =  
| music          = Ilaiyaraaja
| cinematography = Shaji Kumar
| editing        = Don Max
| studio         = Lakshmi Shanthi Movies
| distributor    =
| released       =  
| runtime        =
| country        = India
| language       = Tamil
| budget         =
| gross          =
}} Prashanth in Sneha and Kushboo amongst others. The background score and soundtrack of the film, composed by Ilaiyaraaja, was released on 27&nbsp;March&nbsp;2011. 

Filming, although significantly delayed, began in August 2009 at Valluvar Kottam in Chennai, where an ancient township film set with temples, forts, a lake and palaces had been built. The films producers encountered difficulties in making prompt payments to its crew that consisted of more than 1000 members during the filming in the forests of Pollachi. The filming then shifted to Guindy National Park in Chennai, with the battle sequences shot in Kerala and Karnataka.

Prior to its release, the film drew criticism from the Kongu Nadu Munnetra Kazhagam (KMK) Party with an unsuccessful petition filed at Madras High Court seeking a ban on the film, claiming that it portrayed the traditional deities of the gounder community in a bad light. The film was released on April&nbsp;9,&nbsp;2011   to a mixed response from critics. The film was generally praised for its cinematography and art direction but was criticised for having a weak plot and character deficiencies.

== Plot == gounder custom). Periyamalaikozhundhu disowns his daughter for choosing Nellaiyankondan and banishes them from his kingdom.

Many years later, Mayavar (Nasser), a chieftain of a village in Periyamalaikozhundhus kingdom, encounters twin brothers Ponnar and Sankar (both played by Prashanth), who are being tutored in martial arts by Rakki Annan (Raj Kiran). Ponnar and Sankar are valorous and compassionate to the sufferings of the poor. One day, the twins save two sisters, Muthayi (Pooja Chopra) and Pavalayi (Dhivya Parameshwaran), from death. The girls are later revealed to be daughters of Chinna Malaikozhundhu. Impressed with Ponnar and Sankars valour, Chinna Malaikozhundhu agrees to marry his daughters off to them.

A flashback reveals that Ponnar and Sankar are actually Thamarais sons and that Rakki, who was then a servant of Nellaiyankondan, had saved the twins as small children from Mandhiyappan. Mandhiyappan had attempted to avenge himself on Thamarai for rejecting his hand in marriage. He learns that the twins are still alive and hatches a conspiracy to kill them. He tricks Kali Mannan (Napoleon) and Thamarais father into helping him. This leads to an intense battle which ends with good prevailing over evil.

== Cast ==
  Prashanth as Ponnar and Shankar, the twin brothers
* Pooja Chopra as Muthaayi, Shankars love interest
* Divya Parameshwaran as Pavalaayi, Ponnars love interest
* Jayaram as Nellian Kodan, Ponnar and Shankars father Sneha as Arukkaani Thangam, Ponnar and Shankars sister
* Prakash Raj as Manthiappan, the rival king who schemes to bring down Ponnar and Shankar Prabhu as the Chola King who is an ally to Ponnar and Shankar
* Kushboo as Thamarai Nachiyar, Ponnar and Shankars mother
* Nassar as Mayavar, village chieftain in Thamarais dads kingdom
* Rajkiran as Raaki Annan, Nellians servant Napoleon as Thalaiyoor Kaali, an ally to Mandhiyappan Vijayakumar as Periyamalai Kozhundu, Thamarais father
* Ponvannan as Chinnamalai Kozhundu, Thamarais brother
* Bharat Dabholkar as Thirumalai
* Riyaz Khan as Vaiyamperumal, Chinnamalais son
* Lakshmi Ramakrishnan as Silambayi, Chinnamalais wife Seetha as Azhagu Nachiyar
* Sukumari as Vaanayi Ponnambalam as Thalapathi Raj Kapoor
* Santhana Bharathi
* Delhi Ganesh Bhanu in a special appearance
 

== Historical background == Chera chieftains, folk tale of these brothers, also known as Annamar Swamigal, has been passed down through many generations by means of village songs (known as Gramiya Padalgal) and traditional street theatre, known as Therukkoothu. Absence of reliable written historical records makes it difficult to ascertain the credibility of any of the above claims, along with the widely held legend which was popularised by a late 1980s M Karunanidhis novel on the subject.     According to the legend, the brothers were born to Periyakaralanaka Mannudaiyak Kounden and Thamaraiyal of Manamadurai, belonging to the Kongu Vellalar community. They had attained the kingdom with help from the then king of Urayur, which was the capital of early Cholas. Thalaiyur Kali, a neighbouring country king belonging to the rival Vettuva Gounder caste, hatched a conspiracy against the brothers which led to a war and the subsequent death of Shankar who was tricked into proving his innocence by falling over a sword. Grieving the loss of his brother, Ponnar entered the battlefield, killing Kali Mannan, before proceeding to kill himself . The brothers were then brought to life by their sister Arukaani with help from the Goddess Periyakandiamman.  The brothers, however, were told by the Gods that their earthly duties were over which led to their leaving the mortal world.  Arukaani followed suit by throwing herself along with her royal ornaments into a well in Valanadu, a village in Tiruchirappalli|Trichy.   
 Tamil historical tutelar deities by people belonging to Kongu Vellalar community as a tribute to the brave way the brothers defended their kingdom    

== Production ==

=== Development ===
 ]]
In early 2007, Thiagarajan approached M. Karunanidhi with the intention of making a film on his adaptation of the Ponnar Shankar epic that he had written in the late 1970s. The latter accepted Thiagarajans offer and approved of his decision to cast his son, Prashanth Thyagarajan|Prashanth, in the dual lead roles.     In July&nbsp;2008, the project was officially announced in a press meet.  The film subsequently began pre-production, with Thyagarajan opting to produce the film under his Sri Lakshmi Shanthi Films banner.  The preliminary stages of the production took more than a year, with story-boarding, costume designs and colossal set designs all being finalized.  Prashanth revealed that extensive research about the time period alone took a year and a half. 
 Miss India junior artists and 1000 artists from the annual Chennai Sangamam had reportedly taken part in the film.  For music, Thiagarajan approached A. R. Rahman in 2008 who declined the offer,  and Ilaiyaraaja eventually replaced him. Muthuraj was selected as the art director for the film and Shaji Kumar as cinematographer while Prashanth took care of visual effects. 

The film ran into production trouble in mid 2010 and was close to being shelved, with sources reporting that Thiagarajan sold the film to Santiago Martin to revive the project.   The reports proved to be untrue, and Thiagarajan maintained that he was still the producer of the film. The film had also begun to surpass its budget, due to the casual nature of shooting and difficulties in creating historical sets.    The first shots from the film and the video for three songs were unveiled publicly in March&nbsp;2011, with the publicity campaign initiated after this. 

=== Filming === Napoleon as Sneha was roped in to play Arukaani, sister of the warrior twins, and her scenes were filmed. 

The climax scenes of the film were shot in the deserts of Rajasthan in February&nbsp;2011.  The final phase of filming was going on in the forests of Pollachi with elephants and horses resembling the region in the 1500s, when the production team couldnt make prompt payments onsite to the onsite crew that consisted of more than 1000 members. The displeasure shown by the crew threatened to disrupt the filming process.  Karunanidhi, the writer, visited the sets of the film in a Vadapalani studio in February&nbsp;2011 to peruse over the scenes that had been shot at the time,  and the filming then moved on to Guindy National Park in Chennai.   The four battle sequences in the movie were shot in Perambalur, near Trichy and Warangal in Andhra Pradesh. The scene in Trichy took 28 days to complete, with 30,000 junior artists used as soldiers along with 3000 horses and another battle scene was shot underwater.   A few scenes in the film were reshot after the criticism it received at its premiere that Prashanths characters showed no emotions at the death of their parents. 

== Soundtrack ==
{{Infobox album
| Name = Ponnar Shankar
| Type = soundtrack
| Artist = Ilaiyaraaja
| Cover = Ponnar Shankar Audio Cover.jpg
| Caption = Front Cover
| Released =  
| Recorded =
| Genre = Film soundtrack
| Length = 38:34
| Label = Saga Music
| Producer = Ilaiyaraaja
| Last album = Azhagarsamiyin Kudhirai (2011)
| This album = Ponnar Shankar (2011)
| Next album = Sengathu Bhoomiyilae (2011)
}} folk genre. The song Kannai Padithaen received a special mention in The Hindu newspaper where it was cited as "simply melodious and rings in ones ears for long".  The soundtrack received a negative review by Rediff, which stated that the "tunes were all generic" and that the operatic background score did not gel well with the movies subject. It summed that "Ilaiyaraja had just skimmed the bare bones of his usually fulfilling musical compositions".  There was also a review in Thinakaran on the inappropriateness of the films tracks as the reviewer felt they were ill-suited to the films historical themes. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration
|- 1 || Sathyan Mahalingam || 4:14
|- 2 || Annanmaar Kathai || Madhu Balakrishnan, Hemambika, Anitha || 9:04
|- 3 || Bavaani Varugira || Shreya Ghoshal || 7:36
|- 4 || Kannai Padithaen || Sriram Parthasarathy, Shreya Ghoshal || 6:07
|- 5 || Thedi Vantha Devathai || Kunal Ganjawala, Shreya Ghoshal || 5:09
|- 6 || Malar Villile || Darshana KT|Darshana, Shreya Ghoshal || 6:24
|}

== Release == 2011 Tamil gounder community in a bad light, showing them dancing with semi-nude women.  Leading figures from the party wrote to the Central Board of Film Certification (CBFC) to ban the film,  holding the promotional material used for the movie as evidence. When they received no response from CBFC, a petition was filed at Madras High Court, seeking its ban under Article 226 of the Indian constitution. The petition was dismissed, given that Article 226 can only be used by a court on a body that falls within its jurisdiction.    

The film was released on April&nbsp;9,&nbsp;2011, four days before the election.  It received a lukewarm response at the box office, opening at 14 theatres in Chennai where it collected   in the first three days.   After a two-week run it collected   from Chennai, and was declared "average" by trade pundits. 

=== Reception ===
The film received a mixed to positive reception from critics. The film was praised for its technical merit and art direction, Shaji Kumars cinematography, Ilayarajas re-recording, crisp editing and predominantly colloquial dialogues (unusual for M. Karunanidhi|Kalaignars movies), yet was criticised for its weak storyline and continuity problem.     The Times of India praised the "terrific" art direction and Shaji Kumars cinematography but stated that there were far "too many gaps in the story to ensure that the movie never rises above its mediocrity and ends up being an epic disappointment", giving it two out of five stars.    Thinakaran and Nakkeeran (magazine)|Nakkheeeran compared the grandeur of the film to that of S. Shankar, noted for making high budget films in Tamil.     Eelam Press gave it a positive review, lauding on all aspects of the film, particularly the films war scenes, highlighting that by using contemporary Tamil, instead of its ancient counterpart in its dialogues, the audience could better relate to it. 

Malathi Rangarajans review  (The Hindu)  was fair, labelling the film an "alluring canvas", praising the opulent sets,  locations, rich costumes and imposing cast, yet noted that it was "too short to negate the many loopholes".    Divya Parameshwaran was appreciated in her debut role, while Pooja Chopra was credited as being "attractive too, but   expressions needed some honing".  Though Prashanths stunt acts were appreciated, Rangarajan pointed out his character flaws, such as minimal expressions which were "barely a smirk" in the initial scenes, and the distinct lack of distinguishable traits between the twin characters he depicted.  He also highlighted the lack of logic in Periyamalai Gounders (Vijayakumar (actor)|Vijayakumar) animosity towards his daughter Thamarai (Kushboo) and the cheerful appearance of Thamarai when she must have been sad and heartbroken in the wedding scene with Prakashraj.  Ananda Vikatan|Vikatan stated that the only good aspects of the movie were its expensive sets and its avoidance of boring archaic Tamil dialogues, common in other period films in Tamil, giving it a rating of 41 points out of 100.  The film was later dubbed and released as Prashanth Veer in Hindi and then as Rajakottai Rahasyam in Telugu in 2012 and 2013 respectively.

=== Historical inaccuracies === British constructed literary devices like hyperbole|exaggeration. He further stated that Ponnar and Shankar remained celibate throughout their lives while in the movie they were shown romancing with immodestly dressed women. They married only to avenge the ill treatment meted out to their mother and promptly sent their newly wed wives to prison denying coital relations with them, he added. Also, in the movie the titular characters were not shown committing suicide as in Sakthikanal and Karunanidhis version, but rather were depicted emerging victorious from the war alive. 

== References ==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 
 