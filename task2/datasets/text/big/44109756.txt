Jaalakam
{{Infobox film 
| name           = Jaalakam
| image          =
| caption        = Harikumar
| producer       = M Chandrika
| writer         = Balachandran Chullikkad
| screenplay     = Balachandran Chullikkad Innocent
| music          = MG Radhakrishnan
| cinematography = KG Jayan
| editing        = G Murali
| studio         = Ammu Arts
| distributor    = Ammu Arts
| released       =  
| country        = India Malayalam
}}
 1987 Cinema Indian Malayalam Malayalam film, Harikumar and Innocent in lead roles. The film had musical score by MG Radhakrishnan.   

==Cast==
 
*Sukumari
*Srividya
*Jagathy Sreekumar Innocent
*KPAC Lalitha Ashokan
*Murali Murali
*Babu Namboothiri
*Jagadish
*MG Soman Parvathy
*Rajam K Nair
*Sreenath
 

==Soundtrack==
The music was composed by MG Radhakrishnan and lyrics was written by ONV Kurup. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Oru Dalam Maathram || K. J. Yesudas || ONV Kurup || 
|-
| 2 || Unni Urangaariraro || KS Chithra || ONV Kurup || 
|}

==References==
 

==External links==
*  

 
 
 

 