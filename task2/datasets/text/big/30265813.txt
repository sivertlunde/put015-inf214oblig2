The Impossible Itself
{{multiple issues|
 
 
}}

{{Infobox film
| name           = The Impossible Itself
| image          = 
| alt            = 
| caption        = 
| director       = Jacob Adams
| producer       = Jacob Adams Alan Mandell
| writer         = 
| narrator       = 
| music          = Art Pepper Laurie Pepper
| cinematography = Jacob Adams
| editing        = Jacob Adams Tony Noel
| studio         = 
| distributor    = 
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
The Impossible Itself is a 2010 documentary film produced and directed by Jacob Adams to cover the 1957 San Francisco Actors Workshop production of the Samuel Beckett stage play Waiting For Godot that was taken to San Quentin Prison and performed before its inmates, with an examination of an earlier incarnation of Godot as performed by inmates at the Luttringhausen Prison in Germany in 1953.

==Background==
Adams was 19 when he began working to raise the money to create the film. During filming, Adams travelled to Germany in 2000 and interviewed former Prison Pastor Hans Freitag about the performances. Freitag stated on camera that inmates were allowed to leave the prison to perform Godot in a Jewish Cultural Building in Frankfurt and provided documentary evidence to support his claim. Adams himself later cross-checked the inmate names with names collected in the Holocaust Registry of Nazi officers and found two highly likely matches. The film posits this irony.
 Swedish theatre director Jan Jonson in 2001 when Adams had travelled to NYC to meet Jonson for an interview. Jonson failed to show and the travel expenditures cost the project $2,000. Production was then suspended until 2006 before continuing with funds provided through the directors student loans. In total, it took 9 years to complete all the interviews and editing. The films original version was completed in 2008.

==Synopsis==
The film documents the 1957 San Francisco Actors Workshop production of the Samuel Becketts play Waiting For Godot which was performed live before inmates at San Quentin Prison. The film also examines a 1953 performance of Godot by inmates at the Luttringhausen Prison in Germany, providing new scholarship material on those performances.

==Cast==
The documentary features interviews with former S.F. Actors Workshop members Herbert Blau, Alan Mandell, Eugene Roche, Robert Symonds, Robin Wagner, Joseph Miksak, Tony Miksak, and David Irving as well as former prison inmates Rick Cluchey, Ed Reed, Professor John Irwin and Prison Recreation Supervisor Clem Swagerty.

==Release==
The documentary qualified for Academy Award consideration as a documentary short subject in 2008 but failed to gain a nomination.

After 2008, the film was lengthened and distribution began in 2010, finding a home in such universities as Stanford, Duke, Cal-Berkeley|Berkeley, University of North Carolina at Chapel Hill|UNC, University of Southern California|USC, Kansas State and many others. The film is currently being used as a source on three dissertations.
 KQED programmer Scott Dwyer as "too academic". It was supported by BBC programmer Roger Thompson, but voted against by their committee. It was presented at an Honorarium through Professor David Lloyd of USC.

The film was turned down by the following: Sundance, Cannes, Slamdance, Austin, RiverRun, many others.

==External links==
*  
*  

 
 
 
 
 
 
 
 
 