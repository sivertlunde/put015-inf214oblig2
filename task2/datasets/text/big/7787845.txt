Living Out Loud
 

 
{{Infobox film
| name           = Living Out Loud
| image          = LivingOutLoudPoster.jpg
| caption        = Film poster
| director       = Richard LaGravenese
| producer       = Danny DeVito Michael Shamberg Stacey Sher
| writer         = Richard LaGravenese
| starring       = Holly Hunter Danny DeVito Queen Latifah Martin Donovan Elias Koteas Richard Schiff Mariangela Pino Suzanne Shepherd
| music          = George Fenton
| cinematography = John Bailey
| production design = Nelson Coates Jon Gregory Lynzee Klingman
| studio         = Jersey Films
| distributor    = New Line Cinema Toronto Film Hamptons Film Festival) October 30, 1998
| runtime        = 100 minutes
| country        = USA English
| budget         = $20 million
| gross          = $15,610,299
}}
 1998 comedy-drama film written and directed by Richard LaGravenese and set in New York City, starring Holly Hunter, Danny DeVito, Queen Latifah, Martin Donovan, and Elias Koteas. 

==Plot==
 
Judith Moore had what she thought was a perfect marriage, with both she and her husband studying to be doctors. But after she puts her studies on hold to find a job and support them, many years pass until suddenly he leaves Judith to be with another doctor. Depressed, she holes up in her apartment, where the middle-aged Pat Francato serves as a building superintendent and elevator operator. He is as lonely as she is, beset with gambling problems, and Judith and Pat make a connection. Yet what he wishes to pursue as a romantic relationship, Judith sees only as a friendship. Her friend Liz Bailey, who sings at a nightclub, makes attempts to improve Judiths love life as well as her own.

==Cast==
* Holly Hunter as Judith Moore
* Danny DeVito as Pat Francato
* Queen Latifah as Liz Bailey
* Martin Donovan as Robert Nelson
* Elias Koteas as The Kisser
* Richard Schiff as Philly Francato
* Mariangela Pino as Donna
* Suzanne Shepherd as Mary
* Eddie Cibrian as The Massuer

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 