Up the Creek (1958 film)
 
 
{{Infobox film
| name           =  Up the Creek
| image          =  "Up_the_Creek"_(1958).jpeg
| image_size     = 
| caption        = 
| director       = Val Guest
| producer       = Henry Halstead
| writer         = Val Guest Len Heath John Warren
| narrator       = 
| starring       = David Tomlinson  Peter Sellers  Wilfrid Hyde-White
| music          = Tony Fones Tony Lowry Arthur Grant Moray Grant
| editing        = Helen Wiggins
| studio         = Byron Film Production
| distributor    = Warner Bros
| released       = 13 May 1958 London UK
| runtime        = 83 minutes
| country        =   English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        =
}} 1958 British David Lodge and Lionel Jeffries. 

==Plot synopsis==
Lieutenant Humphrey Fairweather, a well-meaning but accident-prone naval officer with a passion for rockets, is posted where he can (so the navy hopes) cause no further damage. He is given command of a   Royal Navy vessel, HMS Berkeley, which has had no Commanding Officer for several years. She is moored at a wharf on the Suffolk coast near the (fictional) village of Meadows End.

He discovers that the ship is woefully under strength and is forced to contend with the  schemes of his bosn, Chief Petty Officer Dogerty (Sellers). He and the crew are running several profitable businesses, including a same-day laundry, selling naval rum and cigarettes to the local pub, the Pig and Whistle, and making pies and pastries for sale to the villagers. They also keep pigs and hens.

After the naive Fairweather is innocently drawn into the enterprises, he is politely blackmailed into covering for them. But when an Admiral makes a surprise inspection, the story eventually comes out. Whilst angrily haranguing them, Admiral Foley accidentally launches Fairweathers experimental rocket, and the ship is sunk.
 Woomera to continue his rocketry research, accompanied by Susanne, the attractive French girl he has met at the pub. The ships crew are posted to another ship, HMS Incorruptible.
 Weymouth harbour.

According to an interview with Val Guest (included on the DVD issue of the film), Up the Creek was the first starring film role for Sellers, at the time known only for radio and short television sketches. Guest was only able to obtain his services by also including established comedy film star, David Tomlinson.

A sequel Further Up the Creek was released later in the same year, with Frankie Howerd replacing Peter Sellers.

==Cast==
* David Tomlinson as Lieutenant Fairweather
* Peter Sellers as Chief Petty Officer Doherty
* Wilfrid Hyde-White as Admiral Foley
* Vera Day as Lily
* Liliane Sottane as Susanne Tom Gill as Flag Lieutenant
* Michael Goodliffe as Nelson
* Reginald Beckwith as Publican of Pig and Whistle
* Lionel Murton as Perkins
* John Warren as Cooky
* Lionel Jeffries as Steady Barker
* Howard Williams as Bunts
* Peter Collingwood as Chippie
* Barry Lowe as Webster
* Edwin Richfield as Bennett David Lodge as Scouse

==Critical reception==
*The New York Times called the film, "an amiable jest that is diverting and spasmodically amusing, if not precisely unuproarious." 
*TV Guide said, "it is a surprise that UP THE CREEK is as fresh and amusing as it is... Sellers, in one of his earliest roles, steals the show."  

==References==
 

 
 
 
 
 
 

 
 