Dick Deadeye, or Duty Done
{{Infobox film
| image=
| image size=
| caption=
| director=Bill Melendez
| producer=
| writer= Robin Miller (story) Leo Rost (story) Gene Thompson (additional material) Victor Spinetti (additional material)
| narrator=
| starring=Victor Spinetti
| music= Arthur Sullivan
| cinematography =
| editing= Roger Donley Steven Cuitlahuac Melendez Babette Monteil
| studio=Bill Melendez Productions
| distributor=
| released=1975
| runtime=81  min.
| country=   UK
| language=English
| budget=
| gross=
}}
Dick Deadeye, or Duty Done is a 1975  British animated film musical, based on the comic operas of Gilbert and Sullivan. Smith, Winfield. "Dick Deadeye, or Duty Done", review in Issue 30 (May 1991), Precious Nonsense, The Midwestern Gilbert & Sullivan Society 

The comically convoluted plot is a pastiche of many in the Gilbert and Sullivan canon, particularly Trial by Jury, The Sorcerer, H.M.S. Pinafore, The Pirates of Penzance, Patience (opera)|Patience, Iolanthe and The Mikado, in which the principal character, Able Seaman Dick Deadeye, is sent by Queen Victoria on a quest to recover the "Ultimate Secret" from the Sorcerer, who has stolen it. The music is borrowed from many Savoy operas, with the orchestrations being updated in a contemporary (for 1975) popular style.

Animation was by Bill Melendez, who produced the Charlie Brown television cartoon, based on character drawings by veteran cartoonist Ronald Searle.  The films release was accompanied by the original release of a deluxe-jacketed LP soundtrack recording and a colourful storybook by Jeremy Hornsby, with colourful Searle-inspired art. 

==Synopsis==
Queen Victoria sends Dick Deadeye (Victor Spinetti), a sailor, to recover the "Ultimate Secret" from two thieves, the Sorcerer (Peter Reeves) and his reptilian henchman, the Shameleon.  They are trying to sell it to the Pirate King (George Cooper, sung Ian Samwell).  At a military parade ("Entrance of the Peers"), Dick sees the Sorcerer speaking with the Pirate King.  There, Nanki (John Newton, sung Casey Kelly) sees his evil twin brother Poo (same), who is picking pockets. Dick goes for backup to headquarters, the Hexagon ("Heres a how-dee-do"), finding the Captain (Peter Reeves) and the Major-General (Francis Ghent, sung John Baldry; "I am the very model of a modern major general").

Dick then goes to the pirates lair, "The Queens Nose" ("Oh, better far to live and die").  There, Rose Maybud (Julia McKenzie, sung Liza Strike), the barmaid, wants to find "a man of pure evil", so she can reform him. She and Dick have a moment ("Prithee, pretty maiden"), but she loses interest, since he is good. The Sorcerer and his sidekick arrive ("My name is John Wellington Wells").  The Pirate King finalizes the deal to buy the Secret for a modest sum of pirate booty, and the Sorcerer goes to get it, followed by Dick.

Dick arrives at the Sorcerers shop and asks for a potion to make him handsome ("Sprites of earth and air") and makes a grab for the Secret, but it falls through the window into the basket of Little Buttercup (Miriam Karlin), a buxom seller of ribbons, laces and marine supplies.  Poo steals the basket, and everyone chases him, including three policemen ("A policemans lot is not a happy one"), but they mistake Nanki for Poo and arrest him.  The Judge at his trial (Barry Cryer, "All hail great Judge"; "Now, Jurymen, hear my advice") flirts with Little Buttercup ("Im called Little Buttercup"), ignores Nankis evidence ("A wandering minstrel I" and "I swear to tell the truth" based on "When I go out of door"), sentences Nanki to 200 years in the Tower of London ("A Judge is he, and a good judge too") and leaves with Little Buttercup.

At the Tower, Nanki muses on his lot and lost love ("Farewell my love").  The spirit of Yum-Yum (Linda Lewis, sung Beth Porter) is trapped in Nankis shamisen ("Just as the moon must have the sun", based on "The sun whose rays") and needs Nanki "to make me a whole woman".  Poo is willing to return the Secret to the Sorcerer in exchange for learning his tricks, but the pirates drag them to "The Queens Neck".  The Secret in code, and the Sorcerer must decipher it. Since Poo is "the most evil man in the world", Rose decides her love for him and intends to reform him.  Meanwhile, at the Hexagon, Dick sees the Rear-Admiral (Francis Ghent, sung John Baldry; "I am the monarch of the sea") and his sisters and his cousins and his aunts.  He give the Captain ("He remains an Englishman") command of the Pinafore and allows Dick to recruit a crew from the prisoners, and at the Tower, Nanki sings for the prisoners ("The flowers that bloom in the spring": Prisoner sung by Ian Samwell), who nearly all enlist.

The Sorcerer and Poo are on the pirate ship ("Pour, oh pour the pirate sherry"). Rose Maybud, disguised as a pirate, sneaks aboard.  Meanwhile, the Captain, accompanied by the Judge, Major-General, Rear-Admiral takes command of the Pinafore ("When I was a lad"; "We sail the ocean blue"; and "I am the Captain of the Pinafore").  They pursue the pirate ship, assisted by two giant cherubs ("Go, ye heroes"); meanwhile Poo overfeeds the Sorcerer who gets seasick and hands over the Secret.  When the two crews meet, they greet each other warmly. The Pirate King fights the Captain, but the Kings trousers fall, and the strawberry birthmark on his backside is seen. Little Buttercup has a secret ("A many years ago"); as a wetnurse, she mixed up the infant Pirate King and the Captain (recognizable by the birthmark).

Poo sails to the nearby island of Utopia with the Secret, now pursued by everyone ("With cat-like tread").  He meets the Regent of Utopia, the Princess Zara (Miriam Karlin) and her court of show-girls.  They all sing ("Hail, hail, and how-dee-do", based on "With cat-like tread"; "Land of sand and sea and sun", in the style of The Beach Boys).  Zara has Poo arrested.  The Sorcerer regains the Secret and is chased by Dick and the Captain.  They find that the Secret is in mirror-writing, and decipher it: "Its love that makes the world go round" ("If you go in").  Nanki and Poo combine into a person composed of both good and evil.  The shamisen releases the spirit of Yum Yum (the Queen of Utopia).  The Sorcerer falls in love with Little Buttercup, while Rose Maybud and Dick are reunited.  Once the Secret is out, war and crime cease around the world ("Heres a how-dee-do") and all live happily ever after ("Entrance of the Peers", reprise).

==Musical numbers==
*"Entrance of the Peers", from Iolanthe
*"Heres a how-dee-do", from The Mikado
*"I am the very model of a modern major general", from The Pirates of Penzance
*"Oh, better far to live and die", from Pirates
*"Prithee, pretty maiden", from Patience (opera)|Patience
*"My name is John Wellington Wells", from The Sorcerer
*"Sprites of earth and air", from The Sorcerer
*"A policemans lot is not a happy one", from Pirates
*"All hail great Judge", from Trial by Jury
*"Now, Jurymen, hear my advice", from Trial
*"Im called Little Buttercup", from H.M.S. Pinafore
*"A wandering minstrel I", from The Mikado
*"I swear to tell the truth", based on "When I go out of door", from Patience
*"A Judge is he, and a good judge too", from Trial
*"Farewell my love", from Pinafore
*"Just as the moon must have the sun", based on "The sun whose rays", from The Mikado. 
*"I am the monarch of the sea", from Pinafore
*"He remains an Englishman", from Pinafore
*"The flowers that bloom in the spring", from The Mikado
*"Pour, oh pour the pirate sherry", from Pirates
*"When I was a lad", from Pinafore
*"We sail the ocean blue", from Pinafore
*"I am the Captain of the Pinafore", from Pinafore
*"Go, ye heroes", from "Pirates"
*"A many years ago", from Pinafore
*"With cat-like tread", from Pirates
*"Hail, hail, and how-dee-do", based on "Come friends who plough the sea", from Pirates, combined with "Land of sand and sea and sun", in the style of The Beach Boys
*"Its love that makes the world go round", part of "If you go in", from Iolanthe
*"Heres a how-dee-do" (reprise), from The Mikado
*"Entrance of the Peers" (reprise)

==Voice cast==
The following is the voice cast of the film, together with role played, and opera in which the role appears: 
*Victor Spinetti – Dick Deadeye (H.M.S. Pinafore)
*Peter Reeves – Sorcerer / Captain of the Pinafore (The Sorcerer / Pinafore)
*George A. Cooper – The Pirate King (The Pirates of Penzance)
*Miriam Karlin – Little Buttercup / Utopian Maiden (Pinafore / Utopia, Limited)
*John Newton – Nanki Poo (The Mikado)
*Linda Lewis – Yum-Yum (The Mikado)
*Julia McKenzie – Rose Maybud (Ruddigore)
*Francis Ghent – Monarch of The Sea / Major General (Pinafore / Pirates)
*Barry Cryer – Judge (Trial by Jury)
*Beth Porter – Yum-Yum (The Mikado)
*Long John Baldry – Monarch of The Sea / Major General (singing voice)
*Liza Strike – Rose Maybud (singing voice)
*Ian Samwell – The Pirate King / The Prisoner (singing voice)
*Casey Kelly – Nanki Poo (singing voice)

==References==
 

==External links==
*  
* 


 
 
 
 
 