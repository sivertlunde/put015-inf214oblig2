February 15, 1839
 
:This article is about the Quebec film. For the date, see February 15 and 1839.
{{Infobox film
| name = 15 février 1839
| starring = Luc Picard Sylvie Drapeau Frédéric Gilles Julien Poulin Denis Trudel
| director = Pierre Falardeau
| writer = Pierre Falardeau
| producer = René Chenier  Marc Daigle Bernadette Payeur 
| cinematography = Alain Dostie
| editing = Claude Palardy
| released =  
| runtime = 120 minutes
| distributor = Christal Films
| studio = Téléfilm Canada Cinépix Film
| country  = Canada
| language = French
| music = Jean St-Jacques
| gross = $248,093 
}}

February 15, 1839 (original title: 15 février 1839) is a 2001 Quebec historical drama film. Directed by Pierre Falardeau, it is about the incarceration at the Pied-du-Courant Prison and the execution by hanging there of Patriote participants of the Lower Canada Rebellion. Those rebels sought to make Lower Canada, now Quebec, a republic independent from the British Empire.

It features as characters the historical figures François-Marie-Thomas Chevalier de Lorimier, his wife Henriette and Charles Hindelang.
 Lionsgate on January 26, 2001.

== Cast ==
*Luc Picard - François-Marie-Thomas Chevalier de Lorimier
*Sylvie Drapeau - Henriette De Lorimier
*Frédéric Gilles - Charles Hindelang
*Denis Trudel - Jacques Yelle
*Julien Poulin - Curé Marier
*Yvon Barrette - Osias Primeau

== See also ==
*Patriote movement
*Quebec nationalism
*Quebec independence movement
*History of Quebec
*Timeline of Quebec history

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 


 
 