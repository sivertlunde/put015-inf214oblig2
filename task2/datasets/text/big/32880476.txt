The Martyrdom of Nurse Cavell
 
{{Infobox film
| name           = The Martyrdom of Nurse Cavell
| image          =Nurse_Cavell.jpg
| image_size     =
| caption        =Still from the film John Gavin C. Post Mason
| producer       = John Gavin
| writer         = Agnes Gavin 
| narrator       =
| starring       =  Vera Pearce
| music          =
| cinematography = Lacey Percival
| editing        =
| studio         = Australian Famous Feature Company
| distributor    =
| released       = 1916
| runtime        = 4,000 feet
| country        = Australia
| language       = Silent film
| budget         = £450    Andrew Pike and Ross Cooper, Australian Film 1900–1977: A Guide to Feature Film Production, Melbourne: Oxford University Press, 1998, 59.  or ₤1,400   accessed 6 December 2011 
| gross         = £25,000 (est.)  
}}

The Martyrdom of Nurse Cavell is a 1916 Australian silent film about the execution of nurse Edith Cavell during World War I.

Although one of the most popular Australian silent movies ever made, it is considered a lost film.

==Synopsis==
The story is told in four parts. The film starts at the English home of Edith Cavell before the war, then jumps forward six years to a Belgium hospital, where Cavell is working. The war is about to start and Dr Schultz suggests Nurse Cavell return home but she refuses, saying her place is with the sick. She gets an invitation to the wedding of two friends, Lt Renard and Yvonne Loudet. Herr Cries is also invited; he pretends to be a medical student but is in fact a foreign spy and is a rejected suitor of Yvonne. He forces himself on her  but Lt Renard knocks him out and Cries departs, swearing vengeance. The wedding ends when everyone gets news that war has been declared and Renard goes to military headquarters.

Four months later Brussels has been occupied by the Germans and Cavell is tending wounded British, German and Belgium soldiers. Lt Renard has been captured and imprisoned by the Germans. He makes an escape with the help of friends and visits his wife and parents. Yvonne asks Nurse Cavell to help them escape the country. She advises her to send her husband to the Cafe Française and give the password "Liberty" to Monsieur Fouchard, the proprietor, in exchange for false passports.

Renard succeeds but Herr Cries and Captain Hoffberg follow him home. Hoffberg murders Renards father, causing the mother to die of shock. He then tries to rape Yvonne but Renard intervenes. A struggle ensues with Yvonne saving her husbands life and the two of them escaping.

Cries and Hoffberg report the escape to Baron von Bissell, Military Governor of Brussels, and report their suspicions about Nurse Cavell. Searching the hospital, Cries finds a letter from England incriminating Nurse Cavell for assisting another prisoner of war to escape. She is captured by the Germans and refused legal advice, being secretly tried and sentenced to death.

The American Ambassador pleads for her life and the Reverend Gerard demands the right of see her and administer communion. The German officer Von Bissell grants a permit. She is sentenced and shot at 2am, her last words being: "Tell my friends I give my life willingly for my country. I have no fear or shrinking. I have seen death so often, it is not fearful or strange to me."  

==Cast==
*Vera Pearce as Edith Cavell John Gavin as Captain von Hoffberg, a German cavalry officer
*C. Post Mason as Georges Renard, a Belgian officer
*Harrington Reynolds as Reverend Thomas Gerard
*Percy Walshe as Baron Von Bissell, the German military Govenor at Brussels Charles Villiers as Herr Cries, a German spy
*George Portus as Dr Schultz
*Roland Stavey as American Ambassador James Martin as Monsieur Renard
*Robert Floyd as Monsieur Fouchard of the Cafe Française George Farrell as disabled soldier
*Ethel Bashford as Yvonne Loudet, Lt Reynards sweetheart
*Clare Stephenson as Madame Renard
*Nellie Power as Nurse Marcheau

==Production==
John Gavin got the idea to make the film after reading a newspaper story about Cavells death. Agnes Gavin wrote the script overnight and finance was obtained from two leading distributors, J. D. Williams and the partnership of Stanley Crick and Jones.

Filming took three weeks in and around Sydney, with locations at Watsons Bay  and interiors at Darlinghurst Gaol (standing in for German Headquarters ) and the Rushcutters Bay Studio, finishing in early January 1916. It was the first film made on this topic in the world. 

The movie was co-directed with an American, C. Post Mason, who had been working for Australasian Films and also appeared in the movie. Eleven stone Mason had a fight scene with 18 stone Gavin, which Masons character was required to win; in order to make this believable the script was rewritten to have Ethel Bashfords character come in and smash Gavin over the head with a vase.  (Post moved to New York to promote the movie in North America. He decided to stay there and died during the 1918 flu pandemic.) 

Vera Pearce was appearing in the Tivoli Follies at the time. Harrington Reynolds had just appeared on stage in The Rosary. The Motion Picture News wrote during production that "I am off the opinion that it will be one of the usual war dramas. If so, it seems a pity to waste such talent as that possessed by Miss Pearce and Harrington Reynolds." 
 William Hughes sent a letter to Mason prior to the films release, stating that:
 I shall certainly be pleased to see the Photo-play dealing with the Martyrdom and execution of Nurse Cavell, which you propose to produce shortly; but Im very much afraid that I shall not be able to do so, seeing that in the course of a few days I shall be leaving Australian for London and will be absent for some little time. I wish the venture success, and hope it may be the medium of impressing on people the dreadful inhumanity of our enemy."  

==Reception==
The film was given a preview in front of several notables in January 1916, including the Governor General and acting Prime Minister. 

Reviews were generally good  and the film was very successful at the box office, achieving release in the US, South Africa, Canada,  India  and the UK.

By April 1917 it had made £10,000. 

==References==
 

==External links==
* 
*  at the National Film and Sound Archive

 

 
 
 
 
 
 
 