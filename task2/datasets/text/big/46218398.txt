L'Auberge rouge (film)
{{Infobox film
| name           = LAuberge rouge
| image          = 
| caption        = 
| director       = Gérard Krawczyk
| producer       = Christian Fechner Alexandra Fechner Hervé Truffaut Jean Louis Nieuwbourg
| writer         = Christian Clavier Michel Delgado
| based on       =  
| starring       = Josiane Balasko Gérard Jugnot Christian Clavier
| music          = Alexandre Azaria
| cinematography = Gérard Sterin
| editing        = Nicolas Trembasiewicz
| distributor    = Warner Bros. Les Films Christian Fechner
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = $21,400,000
| gross          = $6,509,058 
}}
LAuberge rouge (The Red Inn)  is a 2007 French comedy crime film directed by Gérard Krawczyk. 

==Plot==
In 1830, the sinister Crouteux Inn is set in wild and disturbing mountains ariegeois country. The establishment is run by Martin and Rose, a couple of innkeepers who regularly murdered by Violet - their deaf-mute adopted son - solo travelers to rob. One stormy night, a stagecoach in difficulty refuge in the inn. Among the travelers, the good father has been responsible Carnus a novice it must lead to a monastery lost in the mountains. Martin innkeeper learns that the route of a new road will divert travelers from its establishment. Fearing to be ruined, he decided to remove and jettison all travelers, despite opposition from his wife to the murder of the priest.

==Cast==
* Christian Clavier as Pierre Martin
* Josiane Balasko as Rose Martin
* Gérard Jugnot as Father Carnus
* Jean-Baptiste Maunier as Octave
* Sylvie Joly as Countess of Marcillac
* Anne Girouard as Marie-Odile de Marcillac
* Urbain Cancelier as Philippe de Marcillac
* François-Xavier Demaison as Simon Barbeuf
* Jean-Christophe Bouvet as Lawyer Rouget
* Laurent Gamelon as The woodcutter
* Christian Bujeau as The captain
* Juliette Lamboley as Mathilde
* Fred Epaud as Violet
* Jan Rouiller as Duflot

==References==
 

==External links==
* 

 
 
 
 
 
 

 
 