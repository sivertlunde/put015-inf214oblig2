Women of All Nations
 
{{Infobox film
| name           = Women of All Nations
| image          = Film_Poster_-_Women_of_All_Nations.jpg
| alt            =
| caption        = Women of All Nations poster
| director       = Raoul Walsh William Fox
| writer         = Barry Conners
| starring       = Victor McLaglen Edmund Lowe Greta Nissen El Brendel
| music          = Carli Elinor
| cinematography = Lucien Andriot Jack Dennis
| distributor    = 20th Century-Fox
| released       =  
| runtime        = 72 minutes
| country        = United States
| language       = English
| budget         =
| gross          = 
}}
 American military comedy directed What Price Glory?, with McLaglen and Lowe reprising their roles.

==Plot==
The film opens at the end of World War I, with lifelong rivals Captain Jim Flagg and Sergeant Harry Quirt in the trenches.  After the war ends, both men re-enlist, and the film follows their adventures through the Philippines, Asia, and the Panama Canal.

After a stint in the brig, Flagg is given command of a recruiting office in Brooklyn, New York, where he works with Olsen, who has a persistent sneezing problem, much to the annoyance of Flagg.  Quirt, meanwhile has been discharged.  When a local “joint” is raided by the police, Flagg discovers it is being run by Quirt, and he forces Quirt to re-enlist, or be turned over to the police.  Flagg has been highly unsuccessful as a recruiter, managing only a single other recruit, Izzy Kaplan, who Flagg promised to look after to his father.

They are sent to Sweden, where a love triangle develops between Flagg, Quirt and a dancer they meet in a café, Elsa.  Elsa’s boyfriend, Olaf, eventually intervenes, and the three Marines leave Sweden, bound to Nicaragua, to help out in earthquake rescue and relief efforts.  During the efforts, Izzy is killed, and Flagg digs out a buried marine, discovering it is Quirt.

Their final mission takes them to the Mid-East, where they find Elsa as a favorite in a harem.  She had arrived there from Paris, where she had met Prince Hassan, in whose harem she now finds herself.  Quirt, Flagg and Olsen rescue Elsa from the Harem, sneaking her out in an enclosed harem chair.  As they argue about who will end up with Elsa, they hear Olsens sneeze from within the chair, and they understand that Olsen will wind up with Elsa, much to the chagrin of the other two Marines.

==Cast==

*Victor McLaglen as Captain Jim Flagg
*Edmund Lowe as Sergeant Harry Quirt
*Greta Nissen as Elsa
*El Brendel as Olsen
*Fifi DOrsay as Fifi
*Marjorie White as Margie
*Jesse De Vorska as Izzy Kaplan
*Marion Lessing as Gretchen
*T. Roy Barnes as Captain of the Marines
*Ruth Warren as Ruth
*Bela Lugosi as Prince Hassan
*Joyce Compton as Kiki

==Reception==
Mordaunt Hall, of the New York Times, gave the film a favorable review. 

In his autobiography, Walsh did not have too much positive to say about the film, calling it "...a turkey because it could not be anything else.  A third McLaglen-Lowe film was too much for the public; I had been afraid of this from the start. It should have been called `The League of Nations, because it flopped just as hard." 

==Notes==
Humphrey Bogart was cast in the role of Stone in the film, but in the final edited version, none of his scenes made it to the screen.   
 Hot Pepper, was directed by John Blystone and released in 1933, with McLaglen and Lowe once again reprising their roles. 

==References==
 

==External links==
*  
*  

 
 
 