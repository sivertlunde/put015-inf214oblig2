The Silver Brumby (1993 film)
 
{{Infobox film
| name           = The Silver Brumby
| image size     = 
| image	=	The Silver Brumby FilmPoster.jpeg
| border         = 
| alt            = 
| caption        = 
| director       = John Tatoulis
| producer       = Colin South
| writer         = Elyne Mitchell Jon Stephens
| screenplay     = 
| story          = 
| based on       = Elyne Mitchell
| narrator       = Caroline Goodall
| starring       = Russell Crowe Caroline Goodall Amiel Daemion Johnny Raaen Buddy Tyson
| music          = Tassos Ioannides
| cinematography = Mark Gilfedder 
| editing        = Peter Burgess
| studio         = Film Victoria Film Finance Corporation Australia
| distributor    = Roadshow Entertainment Barnholtz Entertainment Media World Features Paramount Pictures  20th Century Fox
| released       =  
| runtime        = 93 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = $1,532,649AUD ($35,114USD) 
}}

The Silver Brumby is a 1993 Australian drama film|drama-family film, directed by John Tatoulis, and starring actor Caroline Goodall, Russell Crowe and Amiel Daemion. It was based on the Silver Brumby series of novels by Elyne Mitchell.

==Plot==
 
A mother tells her daughter a fable about the prince of the brumbies, brumby being a term for the feral horses of Australia, who must find its place among its kind, while one man makes it his mission to capture it and tame it.

==Cast==
* Caroline Goodall ... Elyne Mitchell
* Amiel Daemion ... Indi Mitchell
* Russell Crowe ... The Man
* Johnny Raaen ... Jock
* Buddy Tyson ... Darcy

==Production==
John Tatoulis says he was attracted the spirituality of the original move. 

==Home Media==
On this VHS in 1993 and in on this DVD in 2004 of Roadshow Entertainment 

==Awards==

===Won===
*Australasian Performing Rights Association 1994:
**APRA Music Award - Best Film Score: Tassos Ioannides
*Chicago International Childrens Film Festival 1994:
**Childrens Jury Award - Feature Film
*Cinekid 1994:
**Audience Award - John Tatoulis

===Nominations===
*Australian Film Institute 1993:
**AFI Award - Best Screenplay: John Tatoulis

==References==
 

==External links==
 

 
 
 
 
 


 