SistaGod
{{Infobox Film
| name           = SistaGod
| image          = 
| image_size     = 
| caption        = 
| director       = Robert Yao Ramesar
| producer       = Robert Yao Ramesar
| writer         = Robert Yao  Ramesar
| narrator       = Evelyn Caesar Munroe
| starring       = Evelyn Caesar Munroe Michael Cherrie Nicole Minerve Indigo Minerve Matthew McHugh Yashmin Campagne
| music          = Anderson Cave Ella Andall 
| cinematography = Edmund I. Attong
| editing        = Debra Lezama
| distributor    = 
| released       =  
| runtime        = 72 min.
| country        = Trinidad and Tobago
| language       = English
| budget         = 
}}
SistaGod is a 2006 Trinidadian fantasy drama, the first in a trilogy  by director Robert Yao Ramesar.  The film stars Evelyn Caesar Munroe,  who also serves as the narrator of the story. She plays the role of Mari (the Sista God), who undergoes a transition from a girl who cheats death to a harbinger of death itself.
 Orisha songs sung by female calypsonian Ella Andall. Despite the genre, there are practically no visual effects and hardly any dialogue in this movie, but instead there is a greater emphasis on imagery. Trinidadian Carnival plays an integral part in the film, with the use of traditional characters that are still popular today.

== Plot ==
 
The film begins during the early 1990s. A white American soldier, serving in the Gulf War as a sniper, is washed ashore on Trinidadian soil. He is shell-shocked from the war, with the mindset of a 12-year-old child. He is taken care of by an Afro-Trinidadian nurse, who helps rehabilitate him. Eventually, they fall in love.

Mari, the daughter of the soldier and the nurse, was conceived in a cemetery. Because the girl is dark-skinned, the soldier denies that she is his child, and leaves the mother. The nurse goes insane and is taken to an asylum, where she spends day after day staring at photos of her and the soldier. Mari is taken in by “Nan”, her adopted Hindu grandmother, in St. Joseph, Trinidad. They live near the cemetery where she claims she hears the souls of the dead trying to resurface.

One day, out of curiosity, Mari picks a bunch of poisoned berries from a tree and eats them. She does not die, but her tongue is darkened black. Nan creates an antidote for the poison, using medicinal herbs.

Her mother is eventually released from the asylum and takes Mari to church one day. When reaching the church, they view a woman and her three children. The woman, in Mari’s opinion, looks like Miss Universe. The three children (two boys and a girl) are her half-brothers and half-sister respectively. The two women stare at each other, frozen in the middle of the street. Mari’s mother walks off, leaving the family to walk the other way.

At age eighteen, while Mari is sleeping one night, he has a premonition of the future. After this dream, she realizes that she is the New Messiah. Her presence on Earth will herald an event known as the “Apocalypso”, after which “everyone will disappear”.

Believing that Mari is possessed by a spirit, Nan hires a popular televangelist to exorcise her. Mari remains on her bed as the televangelist begins. As he speaks to her to be released from the spirit, she suddenly goes into convulsions and light illuminates from her mouth. The televangelist ultimately gives up, and assumes that Nan’s house is possessed and is causing her adopted granddaughter to act strange. The decision is made to burn the house down, forcing Nan to stay by her sister’s house. The loss of her home affects Nan, who dies a few days later. She is buried in a funeral pyre, using scraps from her house.

The televangelist returns to complete the demolition of Nan’s house.  He tries to console Mari by giving her an iguana that he caught.  She eventually frees the iguana, after which it is run over by a vehicle. She glances into the car, and sees the face of her father. Her father had returned to Trinidad years ago to open a pub, with its walls painted with images from the Gulf War.

Days later, Mari becomes pregnant. The mother, after hearing this shocking news, threatens to fling herself off the top of a waterfall located near her hometown. Instead, she designs a Baby Doll costume for her daughter to wear. The Baby Doll, in Carnival mythology, is a teenage girl dressed in all white who wears a white mask to hide her shame for being pregnant at such an early age. Now in costume, Mari has adopted a new persona - SistaGod. 

One day, Mari has a dream in which she is wearing the Baby Doll costume (which she wears throughout the rest of the film) and “floating through the trees”. Her adopted grandmother floats with her, followed by her father, and the televangelist who has his arms outstretched with a Bible in his left hand. All four of them are now aligned, still floating. The dream ends, and Mari is shown walking away from the waterfall. Her mother arrives afterwards, walking towards it. She capsizes her head face down in the pool, with her hands outstretched, and drowns herself. The next day, Mari rushes to her father’s home to tell him about her mother’s death. Days later, he visits her grave. Mari arrives in the night, leaving candles to illuminate the tombstone. She then remembers her mother telling her that she will return to Earth in fire.

The next morning, a Carnival procession begins. This is the day before the Apocalypso – the end of humanity. All the participants are dressed as traditional Carnival characters such as Burrokeets, Bats, Midnight Robbers, Bookmen, Dame Lorraines and the bizarre Blue Devils. As a child, Mari could not take part in Carnival, but can only watch in fear and awe. On this day, she is simply observing the festivities. The celebrations become more intense and more frightening as the Blue Devils enter. Realizing that the end is near, Mari asks a nearby Bookman(who is holding a book containing names of souls going to Hell) if there is any room there for the rest of humanity. He tells her that Hell is currently overpopulated.  Night creeps in and the Blue Devils continue their ranting and raving. The Apocalypso then begins.

The next morning, Mari is seen standing near a wall. The entire wall is scribbled with names. She is the human survivor of the Apocalypso and begins to walk through the empty streets. She assumes that the gods are the only other survivors, as she passes a gigantic statue of the Hindu monkey-god Hanuman.

She continues to walk aimlessly until she sits down in front of a tree. She is in pain, as her water just broke. She gives birth, as is symbolized by a light (similar to the one that illuminated her mouth during the exorcism) between her legs. The screen fades to black, with the words “The End of the Beginning” being shown. These words fade, after which we hear the crying of Mari’s newborn child.

== References ==
 

== External links ==
*  
*   at Answers.com

 
 
 