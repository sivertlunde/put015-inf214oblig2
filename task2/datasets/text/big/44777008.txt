Crazy Cukkad Family
{{Infobox film
| name = Crazy Cukkad Family
| image = Crazy Cukkad Family.jpg
| caption = First Look Poster
| director = Ritesh Menon
| producer = Prakash Jha
| writer =  Suhaas Shetty, Kushal Punjabi
| music = Songs:  Sidhartha-Suhaas   Lyrics: Kumaar 
| cinematography = Sojan Narayanan
| editing =  Shakti Hasija
| marketing = Bhavesh Thakkar Swanand Kirkire|Shilpa Zachary Coffin|Nora Kiran Karmarkar| Pravina Deshpande| Jugnu Ishiqui}}
| studio = Prakash Jha Productions
| distributor = Prakash Jha Productions
| released =  
| country = India
| language = Hindi
}}
Crazy Cukkad Family is a 2015 bollywood, comedy film directed by Ritesh Menon and produced by Prakash Jha.  The film stars Swanand Kirkire,   Shilpa Shukla, Zachary Coffin, Nora Fatehi, Kushal Punjabi Jugnu Ishiqui, Jagat Singh and Anushka Sen. Upon release the movie received mixed to positive responses. rajkumar hirani, sudhir mishra and gauri shinde praised the performances and the story.


==Plot==
This roller coaster ride begins with the Wealthy Mr. Beri slipping into his third Coma. His four estranged children have to make it back home to be there while their father “hopefully” breathes his last, leaving behind his huge estate in the mountains.

Pawan beri (Swanand Kirkire) the oldest child of Mr. & Mrs Beri is a hustler who is in big trouble with a local mafia don turned Politician. Rude brash and arrogant, he looks at everything from his own crooked view. Archana Beri- (Shilpa Shukla) is the wannabe socialite who wanted to participate in the Miss India contest but she got married against her will at a young age now she is desperate to enter the Mrs. India contest. she is bitter towards the entire world including her family. She bullies her meek husband Digvijay (Ninad Kamat) who in secret has a dual personality. Aman Beri (Kushal Punjabi) is the New York based son who returns with his American wife, Amy (Nora Fatehi) who meets the family for the first time. He pretends to be a top fashion photographer, but in reality he is struggling as an unemployed light boy. Abhay Beri/ Chotu (Siddharth Sharma) is the youngest in the family. Not much is known about his present status. Years ago he was sent to New Zealand to study, but never returned... well until now!

Along with the four siblings are a bunch of other unique and nutty characters like a village item girl, an extra slow family lawyer and 3 goofy investigators. The chaos begins when they discover that to open the will of their father they need to get ‘Chotu’ married. Set in a lush green, picturesque hill station, this story is about the dysfunctional Beri family. A mad- caper but true to life, hilarious but emotional, fast paced yet heartwarming story of a family estranged for years, brought together by greed but eventually finding each other, finding the meaning in being a family.

==Cast==
* Swanand Kirkire as Pawan beri 
* Shilpa Shukla as Archana Berry
* Kushal Punjabi as Aman Beri
* Siddharth Sharma as Abhay Beri/ Chotu
* Pravina Deshpande
* Kiran Karmarkar
* Yousuf Hussain
* Ninad Kamat
* Nora Fatehi
* Anushka Sen
* Jagat Singh as Lakhan
* Zachary Coffin
* Jugnu Ishiqui as Cherry
* Anushka Sen


==Reception==
The film started slowly but went on to do decent business at the box office. The movie was appreciated by the LGBT community for breaking the stereotypical  portrayal of the community in hindi films.


==References==
 

==External links==
*  