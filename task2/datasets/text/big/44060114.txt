Ee Ganam Marakkumo
{{Infobox film
| name           = Ee Ganam Marakkumo
| image          =
| caption        =
| director       = N Sankaran Nair
| producer       = Adoor Manikantan Adoor Padmakumar
| writer         = Venu Nagavally Aniyan Alanchery
| screenplay     = Venu Nagavally Aniyan Alanchery
| starring       = Prem Nazir Sukumari Jagathy Sreekumar Adoor Bhasi
| music          = Salil Chowdhary Ashok Kumar
| editing        = MS Mani
| studio         = Padmamani Productions
| distributor    = Padmamani Productions
| released       =  
| country        = India Malayalam
}}
 1978 Cinema Indian Malayalam Malayalam film,  directed by N Sankaran Nair and produced by Adoor Manikantan and Adoor Padmakumar. The film stars Prem Nazir, Sukumari, Jagathy Sreekumar and Adoor Bhasi in lead roles. The film had musical score by Salil Chowdhary.   

==Cast==
 
*Prem Nazir
*Sukumari
*Jagathy Sreekumar
*Adoor Bhasi
*Thikkurissi Sukumaran Nair
*Jose Prakash
*Sreelatha Namboothiri
*Bahadoor
*Jalaja
*K. P. Ummer
*Lalu Alex Meena
*Rugmini Roy
 

==Soundtrack==
The music was composed by Salil Chowdhary and lyrics was written by ONV Kurup.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Ee Kaikalil || S Janaki || ONV Kurup ||
|-
| 2 || Kalakalam Kaayalolangal || K. J. Yesudas || ONV Kurup ||
|-
| 3 || Kurumozhi Mullappoove || K. J. Yesudas, Vani Jairam || ONV Kurup ||
|-
| 4 || Onappoove || K. J. Yesudas || ONV Kurup ||
|-
| 5 || Raakkuyile Urangoo || K. J. Yesudas, Sabitha Chowdhary || ONV Kurup ||
|}

==References==
 

==External links==
*  

 
 
 


 