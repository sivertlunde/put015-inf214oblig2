One Life (film)
{{Infobox film
| name           = One Life
| image          = one_life_film_poster.jpg
| caption        = Film poster Michael Gunton Martha Holmes
| producer       = Martin Pope, Michael Rose
| writer         = Michael Gunton Martha Holmes
| narrator       = Daniel Craig
| music          = George Fenton
| cinematography =
| editing        =
| studio         = BBC Earth IM Global Magic Light Pictures
| distributor    =
| released       =  
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         =
| gross          =
}} Michael Gunton Martha Holmes. The film is narrated by the British actor Daniel Craig.

==Release==
One Life premiered on 22 July 2011 in the United Kingdom. The film had a limited release in the United States in February 2013.

===Reception===
Rotten Tomatoes reported that 95% of critics gave the film positive reviews based on 20 reviews. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 
 

 
 