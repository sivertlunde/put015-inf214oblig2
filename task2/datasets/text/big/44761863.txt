Werewolf Rising
{{Infobox film
| name           = Werewolf Rising
| image          = 
| alt            =
| caption        = 
| film name      = 
| director       = BC Furtney
| producer       = Jesse Baget Jennifer Furtney
| writer         = BC Furtney
| screenplay     = 
| story          = 
| based on       =  
| starring       = Brian Berry Melissa Carnell Matt Copko Bill Oberst Jr.
| narrator       = 
| music          = 
| cinematography = Ernesto Galan
| editing        = Thomas A. Marino
| production companies = Ruthless Pictures
| distributor    = RLJ Entertainment (Australia)
| released       =    
| runtime        = 78 minutes
| country        = United States
| language       = English
| budget         = 
| gross          =  
}}
Werewolf Rising is a 2014 horror film that was written and directed by BC Furtney.  The film was released to DVD in the United Kingdom on 22 September 2014 and was later released in the United States on 14 October. The movie stars Bill Oberst Jr. and Melissa Carnell, who portrays a young woman that finds herself the unwitting prey of a werewolf. 

==Synopsis==
The film begins with a car approaching a clearing at dusk. Inside the vehicle,a woman is taken hostage by a man named Rhett wielding a knife. He forces her out of the car and attempts to rape her, but is interrupted by a creature spying on them from the clearing. It attacks the pair, and while the woman is killed, a lacerated Rhett is able to escape, now cursed with the creatures bloodline. 

Hoping to overcome her alcoholism by sequestering herself, Emma (Melissa Carnell) has traveled to the old family homestead maintained by her fathers friend Wayne (Brian Berry). Through a phone conversation it is revealed that it has been twenty years since her last visit home and that she has been plagued with nightmares consisting of her lost in the woods for most of her life, with the addition of a man stalking her once she began her quest to sober up. 

Once there, Emma reminisces through her fathers old things and spends the evening watching old home videos. She drifts into sleep and she begins to have several gruesome nightmares involving a bloodstained man stalking her through the woods. She wakes from her nightmare  and heads to the fridge to calm herself. She finds two beers and proceeds to dump the alcohol, but is interrupted by a man approaching her as she does so. He introduces himself as Johnny Lee (Julia Jacome), Waynes nephew. At first Emma is unsure of who he is but warms up to Johnnys attempts to make idle conversation. They part ways and it is revealed that Johnny Lee sleeps in a shack in the middle of the woods. Once there, a howling wolf is heard in the distance.

The following day Wayne visits Emma. Emma mentions Johnny Lees visit Waynes and his tone changes. He reveals to her that Johnny Lee is an escaped convict with a history of violence and drug abuse. Wayne gives Emma a handgun for her protection and changes the locks for good measure. The two arrange to have dinner the following evening.

The next day, Emma proceeds to walk the woods and arrives at a small lake. Once there, Johnny Lee reveals himself to her on the other side of lake by casting a rock in her direction. They spend the day traversing the wilderness on Emmas ATV, andJohnny Lee reveals his living conditions to Emma. He quickly teaches her how to handle the ATV once she turns down his offer to continue their evening. Emma offers her hospitality in a flirtatious manner to Johnny and they decide to meet the following evening.

In the evening, Johnny, alone in his shack, hears the distant calls of a wolf. Then there is a quick bang to his door, followed by a werewolf breaking down the door and attacking him.  Johnny is taken down by the creature, in the struggle Johnny uses a large wooden splinter to defend himself.

Back at Emmas place, Wayne and Emma are eating dinner dinner. While Emma seeks to maintain a friendly atmosphere with him, Wayne attempts to court Emma with flattery and flowers. As the night progresses Wayne has become intoxicated and attempts to push Emma into a corner and initiate a sexual encounter. Emma refuses and forces Wayne to leave. Emma composes herself and heads to Johnny Lees place. Once there she finds his door in pieces and Johnny Lee with deep lacerations. She rushes him to her place for medical attention, while the creature watches in the brush. She pleads with Johnny Lee to let her help him and professes that she knows everything about him and does not care but only wishes to be with him. Johnny Lee bleeds out on her kitchen floor and grows silent. Emma believing him dead grieves on the front porch. The rustling of the brush near her porch and deep animal growls frighten her and runs inside in terror only to find Johnny Lee missing. She finds him in her bathroom and is surprised that hes still alive. She cleans his wounds and offers her help but he refuses. He reveals to Emma that a large wolf attacked him. 

The following day, Emma removes the blood stains from her floors and tends to Johnny. He recovers quickly and becomes increasingly hungry. That evening his behavior changes and he begins to pine for meat. She offers to make him soup and she quickly fetchers her handgun. Johnny Lee heads outside and becomes unruly, berating Emmas offers for soup and her insistence that he come inside. He runs through the woods and becomes animalistic; Emma finds him but becomes frightened by his guttural howls. Emma quickly runs home and composes herself and attempts to look for Johnny. She drives down the paths near the lake searching for Johnny and finds no one. The creature arrives to her location and Emma rushes home, where it begins to torment her inside the house by banging and scratching the windows. As she tries to leave for her vehicle the creature reveals itself her and she is forced back inside the cabin. Across town, Wayne is drinking in the local bar explaining why he did what he did. Emma locks herself in the closet and stumbles upon a bottle of vodka, which she quickly chugs. She makes her way out into the living room and passes out. Wayne, in a drunken stupor, heads to her cabin to apologize to Emma. Meanwhile, in another part of town, a nude Johhny Lee walks into the local laundry-mat and collects someones clothes and walks out.

Wayne, upon finding Emma in her living room, carries her to her bedroom and collects his handgun from her. Johnny Lee arrives to find Wayne stepping out of Emmas door and questions him for his reasoning on being there. Johnny Lee begins to taunt Wayne on his taste in younger women. Wayne defends himself by bringing up Johnny Lees past and drug history. The confrontation escalates and Wayne shoots him, alerting Emma to the situation. As shes attempting to help Johnny, Wayne also shoots her in shoulder. Meanwhile, Johnny begins to turn and aims to attack Wayne, but Wayne quickly guns him down. Behind him, Rhett makes his presence known. He reveals to Wayne that he was the other escape convict who was with Johnny and that he is aware of who he is. Wayne shoots Rhett and attempts to strangle Emma as she begins to stir but is interrupted by the appearance of the werewolf. The beast drags Wayne to the clearing and kills him while Emma crawls to safety. The beast then turns its attention to Emma who is able to defend herself with a kitchen knife.

She runs from the cabin with the beast in hot pursuit, only to be tormented as it delights in chasing her. She stumbles upon a clearing with a woman in red standing next to a campfire. She attempts to warn the woman of the creature, but she chastises her and reveals her name, that she has been writing to him for years while he was in prison and that the only reason he is here is because she led him there. The werewolf enters the clearing and Beatrix disrobes for him, only for the wolf to kill her and grapple with Emma. As she screams, the creature shifts back to human form. Rhett knows about Emmas dream and tells her that he "knows" her but didnt know her father. He goes on to tell her that she has something special inside her, like her father before her, and advises her to give in to it. As she writhes on the ground, Rhett tells her she has something beautiful inside her and Emma begins to transform

In a post credit scene, Emma makes her way to a neighbors home, pleads for help as she writhes in pain. An older woman approaches and ask if she can help...as the screen fades to black, Emmas face can be seen as she roars going in for the kill.

==Cast==
*Melissa Carnell as Emma
*Brian Berry as Wayne Dobbs
*Matt Copko as Johnny Lee
*Taylor Horneman as Werewolf
*Danielle Lozeau as Christina
*Irena Murphy as Beatrix
*Bill Oberst Jr. as Rhett
*Julia Jacome
*Teagan Grinwis
*Jessie Lewis
*Myles Adkin

==Reception==
Critical reception for Werewolf Rising has been predominantly negative.   Reviewers for Bloody Disgusting heavily panned the film and both criticized the movies special effects, while also agreeing that Oberst Jr. was the "only bright spot in the film".   Starburst magazine|Starburst also criticized the film, negatively comparing the plot premise to Juan Martínez Morenos 2012 film Game of Werewolves. 

In contrast, Aint It Cool News praised Werewolf Rising and wrote that "while there are some trips along the way in terms of acting and effects, WEREWOLF RISING gets points for trying something new." 

==References==
 

==External links==
*  

 
 
 