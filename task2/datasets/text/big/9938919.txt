The Lawton Story
{{Infobox film
| name           = The Prince of Peace
| image          = princeofpeaceposter.jpg
| caption        = The Prince of Peace film poster, circa 1950.
| director       = William Beaudine Harold Daniels
| producer       = Kroger Babb J. S. Jossey
| writer         = Mildred Horn Milton Raison Rev. A. Mark Wallock
| starring       = Ginger Prince Forrest Taylor Millard Coody Lee White Henry Sharp
| editing        = Dick Currier
| distributor    = Hygienic Productions Modern Film Distributors
| released       =  
| runtime        = 
| country        = United States English
| budget         = 
}}
 exploitation rounds presented by exploitation pioneer Kroger Babb.  The film was based on a passion play created in 1948 in Lawton, Oklahoma.  Filmed in Cinecolor,  it was presented in various forms in the years following its debut.  The film also served as the debut film of child actress Ginger Prince, who was touted as her generations Shirley Temple. Feaster, 112. 

==Plot and production== mortgage lender, to see the performance of a passion play in Lawton. The uncle is moved by the performance and changes his greedy and sinful ways.   The scenes with Prince, filmed over a six-day period by William Beaudine in Lawton,  were interspersed with scenes from nearly four hours of footage of the real-life residents of Lawton in their annual performance of the crucifixion of Jesus Christ. 

It was marketed in a manner similar to other roadshow-style film productions, such as Mom and Dad. Promoters of the film often sold Bibles following showings to capitalize on the religious element, often with a lecture during intermission.   Kroger Babb had no issue with his attempts at making money off the religious topic, saying that "Its no sin to make a profit." 

Babb attempted to introduce Prince in this film as a replacement for aging child star Shirley Temple.  A native of Atlanta, Georgia, Prince was given four musical numbers in the production, and featured prominently in the films advertising and promotion, which referred to the girl as "42 inches and 42 pounds of Southern Charm" and, in reference to a sensational bathing scene with Prince, "soap washes off dirt, but only God can wash away your sins." 

==Reception== English to redubbed many times,  but eventually opened in Lawton to a respectable crowd, and, while it failed to be a hit, the films run in New York City was so successful that the New York Daily News called it "the Miracle of Broadway theatre|Broadway." 

Other reviews were not as glowing, however.  Variety (magazine)|Variety, in a review, specifically criticized Princes performance in the film, saying the movie would have been better "had not producers seen fit to drag in a crass, commercial showcasing of a precocious moppet, apparently in an attempt to strike a broader popular market." 

==People in the movie==
* Ginger Prince - Ginger
* Forrest Taylor - Mark Wallock (Gingers grandfather)
* Millard Coody - Millard Coody/Jesus
* Ferris Taylor - Uncle Jonathan Wallock
* Gwynne Shipman (credited as Gwyn Shipman) - Jane (as Gwyn Shipman)
* Darlene Bridges - Darlene Bridges/Virgin Mary
* Maude Eburne - Henrietta
* Willa Pearl Curtis - Willa Pearl
* Raymond Largay (credited as Ray Largay) - Dr. Martin (as Ray Largay)
* A.S. Fischer - A.S. Fisher/Simon Peter
* Hazel Lee Becker - Herself/Mary Magdalene
* William Ruhl - Mr. Nelson
* Russ Whiteman - Mr. Cole
* Knox Manning - Narrator

==Works cited==
*  
*  
*  
*  
*  
* Variety (magazine)|Variety review, April 6, 1949.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 