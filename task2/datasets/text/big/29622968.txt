The Clutching Hand
The Clutching Hand (in full, The Amazing Exploits of the Clutching Hand) is a 1936 15-episode serial produced by the Weiss Brothers based on the final Craig Kennedy novel of the 1934 same name by Arthur B. Reeve.  A 70 minute feature film using a condensed version of the serial was also released in the same year. 

In it, the famous detective (portrayed by Jack Mulhall, who had portrayed the Black Ace in the serial The Mystery Squadron three years before) is assigned to solve the disappearance of Dr. Paul Gironda (Robert Frazer), a scientist who has developed a formula for synthesizing gold but vanishes before he has a chance to reveal it to his board of directors.  It was the last Craig Kennedy serial and the only one to be filmed as a talkie.  One of the criminals, Hobart, is played by Charles Locher, who is better known nowadays as Jon Hall, and it appears that Gironda is being held prisoner by Craig Kennedys old foe, the Clutching Hand (a faceless presence apparently played by Bud Geary, an actor who was frequently cast in such roles, and voiced by Robert Frazer).

==Production==

The serial was based very loosely on Arthur Reeves final novel and in it, the Clutching Hand has a new secret identity - Dr. Paul Gironda, the very man whom Kennedy is looking for.  Having almost exhausted the estate of his young ward Vera, he has come up with the idea of using a fake synthetic gold formula to recoup his losses on the stock market, but just before he disappears, one of his assistants steals the notebook containing the formula.  Unable to find it, he fakes his disappearance and goes into hiding, assuming the identity of Craig Kennedys foe, the Clutching Hand, who was seemingly disposed of in The Exploits of Elaine years before.  The fact that the Clutching Hand had a totally different identity in that serial is enough for me to go into detail about his new identity here. Supposedly holding Gironda captive in an induced coma, the Hand demands the missing pages of the gold formula from his wife and "daughter."  At the same time as Gironda vanishes, Mitchell - Veras real father and Mrs. Girondas first husband - is released from prison after serving a full sentence on a blackmail charge, forcing the Hand to try to dispose of Mitchell before Vera can learn of her true identity.

==Notes==

Chapter Thirteen is titled "The Mystic Menace," and yet nothing mystical happens - it is a pretty standard serial chapter.  This is due to the fact that the serial makers - the Weiss Brothers, in this case - could not alter the chapter titles for some reason.  This was apparently the brothers final serial and their last Craig Kennedy adaptation, however in 1952 the Weiss Brothers produced a television show Craig Kennedy, Criminologist.

==Notes==
 

==External links==

*  
* The serial itself can be found at CamelotBroadcasting.com
 
 
 
 
 
*   complete Series on YouTube (public domain)
 