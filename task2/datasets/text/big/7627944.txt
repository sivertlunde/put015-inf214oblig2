The Boy Friends
 
  American comedy short films released between 1930 and 1932.  The series consisted of fifteen films and was spun off from the long running Our Gang film series (also known as The Little Rascals).  

==Overview== short subjects produced by Hal Roach through Metro-Goldwyn-Mayer.  They featured teenaged actors including some, particularly Mickey Daniels and Mary Kornman, who as children had starred in the early silent Our Gang shorts. Grady Sutton was also a lead player in the series. Daniels and Sutton appeared in every short, while Kornman appeared in all but one (1932s Youre Telling Me).

Roach released three The Boy Friends films in 1930: Doctors Orders, Ladies Last, and Bigger and Better.  The series added seven installments in 1931: Blood and Thunder, High Gear, Love Fever, Air Tight, Call a Cop, Mama Loves Papa, and The Kickoff.  Finally, 1932 saw the release of the last five films: Love Pains, The Knockout, Too Many Women, Wild Babies, and Youre Telling Me.  At least some of the films in the series took place at Elmira College, an actual school located in Roachs home town of Elmira, New York.

==Characters==
*Mickey (Mickey Daniels): The main character. Appeared in all entries.
*Mary (Mary Kornman): Mickeys girl friend. Appeared in all entries except Youre Telling Me.
*Grady "Alabam" Sutton (Grady Sutton): Mickeys best friend and sidekick. Appeared in all entries. David Sharpe): Mickey and Gradys friend. He was their rival in his first short. Last appeared in Call a Cop!
*Gertie (Gertrude Messinger): Daves love interest. Last appeared in The Kick-Off.
*Dorothy (Dorothy Granger): Alabams love interest. Last appeared in Love Fever.
*Betty ( ): Alabams love interest. Appeared in Air-Tight, Mama Loves Papa, The Kick-Off!, Love Pains, The Knockout, Youre Telling Me and Wild Babies. Jacqueline Wells): Appeared in The Knockout and Youre Telling Me.

==Filmography==
===1930===
*Doctors Orders
*Bigger and Better
*Ladies Last

===1931===
*Blood and Thunder
*High Gear
*Love Fever
*Air-Tight
*Call a Cop!
*Mama Loves Papa
*The Kick-Off!

===1932===
*Love Pains
*The Knock-Out
*Youre Telling Me
*Wild Babies
*Too Many Women

 
 
 
 
 
 
 
 
 
 
 
 
 