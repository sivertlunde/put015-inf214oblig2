Death of a Salesman (1985 film)
{{Infobox film
| name           = Death of a Salesman
| image          = Death of a salesman dvd.jpg
| alt            = 
| caption        = DVD cover
| film name      = 
| director       = Volker Schlöndorff
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Dustin Hoffman Kate Reid John Malkovich
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 136 minutes 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 play Death of the same name by Arthur Miller. It stars Dustin Hoffman, Kate Reid, John Malkovich, Stephen Lang and Charles Durning.  The film follows the script of the 1949 play almost exactly. The film earned 10 Emmy nominations at the 38th Primetime Emmy Awards ceremony and 4 Golden Globe nominations at the 43rd Golden Globe Awards ceremony, winning 3 and 1, respectively.                

== Plot ==

Willy Loman (Dustin Hoffman) returns home exhausted after a canceled business trip. Worried over Willys state of mind and recent car crash, his wife Linda (Kate Reid) suggests that he ask his boss Howard Wagner (Jon Polito) to allow him to work in his home city so he will not have to travel.  Willy complains to Linda that their son Biff (John Malkovich), who is visiting, has yet to make good on his life.  Despite Biffs promise as an athlete in high school, he flunked senior year math and never went to college.

Biff and his brother Happy (Stephen Lang), who is also visiting, reminisce about their childhood together.  They discuss their fathers mental degeneration, which they have witnessed by his constant vacillations and talking to himself.  When Willy walks in, angry that the two boys have never amounted to anything, Biff and Happy tell Willy that Biff plans to make a business proposition the next day in an effort to pacify their father.

The next day, Willy goes to ask Howard for a job in town while Biff goes to make a business proposition, but neither are successful.  Willy gets angry and ends up getting fired when Howard tells him that he needs a rest and can no longer represent the company.  Biff waits hours to see a former employer who does not remember him and turns him down.  Biff impulsively steals a fountain pen.  Willy then goes to the office of his neighbor Charley (Charles Durning), where he runs into Charleys son Bernard (David S. Chandler) (now a successful lawyer).  Bernard tells him that Biff originally wanted to do well in summer school, but something happened in Boston when Biff went to visit Willy that changed his mind.
 flashback of what happened in Boston the day Biff came to see him.  Willy had been in a hotel on a sales trip with a young woman named Miss Francis (Kathryn Rossetter) when Biff arrived.  From that moment, Biffs view of his father changed and set Biff adrift.

Biff leaves the restaurant in frustration, followed by Happy and two girls (Linda Kozlowski and Karen Needle) that Happy has picked up.  They leave a confused and upset Willy behind in the restaurant.  When they later return home, their mother angrily confronts them for abandoning their father while Willy remains talking to himself outside.  Biff goes outside to try to reconcile with Willy.  The discussion quickly escalates into another argument, at which point Biff forcefully tries to convey to his father that he is not meant for anything great, that he is simply ordinary, insisting that they both are.  The feud culminates with Biff hugging Willy and crying as he tries to get him to let go of the unrealistic dreams that he still carries for Biff and wants instead for Willy to accept him for who he really is.  He tells his father he loves him.

Rather than listen to what Biff actually says, Willy realizes that his son has forgiven him and thinks that Biff will now pursue a career as a businessman.  Willy kills himself by intentionally crashing his car so that Biff can use the life insurance money to start his business.  However, at the funeral, Biff retains his belief that he does not want to become a businessman.  Happy, on the other hand, chooses to follow in his fathers footsteps.

== Cast ==
 William "Willy" Loman (Dustin Hoffman): The salesman.  He is 60 years old and very unstable, tending to imagine events from the past as if they are real.  He vacillates between different perceptions of his life.  Willy seems childlike and relies on others for support.  His first name, Willy, reflects this childlike aspect as well as sounding like the question "Will he?" His last name gives the feel of Willy being a "low man," someone low on the social ladder and unlikely to succeed.
* Linda Loman (Kate Reid): Willys wife.  Linda mostly just smiles and nods when Willy talks unrealistically about hopes for the future, although she seems to have a good knowledge of what is really going on.  She berates her sons, particularly Happy, for not helping Willy more, and supports Willy lovingly, despite the fact that Willy sometimes ignores her opinion over that of others.  She is the first to realize Willy is contemplating suicide at the beginning of the play and urges Biff to make something of himself, while expecting Happy to help Biff do so.
* Biff Loman (John Malkovich): Willys older son.  Biff was a football star with lots of potential in high school, but failed math his senior year and dropped out of summer school due to seeing Willy with another woman while visiting him in Boston.  He goes between going home to try to fulfill Willys dream for him to be a businessman and ignoring his father and going out West to be a farmhand where he is happiest.  He likes being outdoors and working with his hands yet wants to do something worthwhile, so Willy will be proud.  Biff steals because he wants evidence of success, even if it is false evidence, but overall Biff remains a realist, and informs Willy that he is just a normal guy and will not be a great man.
* Harold "Happy" Loman (Stephen Lang): Willys younger son.  He has lived in the shadow of his older brother Biff most of his life and seems to be almost ignored, but he still tries to be supportive towards his family.  He has a very restless lifestyle as a womanizer and dreams of moving beyond his current job as an assistant to the assistant buyer at the local store but is unfortunately willing to cheat a little in order to do so, by taking bribes.  He is always looking for approval from his parents but rarely gets any, and he even goes as far as to make things up just for attention, such as telling his parents he is going to get married.  He tries often to keep his familys perceptions of each other positive or "happy" by defending each of them during their many arguments but still has the most turbulent relationship with Linda, who looks down on him for his lifestyle and apparent cheapness, despite him giving them money.
* Charley (Charles Durning): Willys wisecracking yet understanding neighbor.  He pities Willy and frequently lends him money and comes over to play cards with Willy, although Willy often treats him poorly.  Willy is jealous of him because his son is more successful than Willys.  Charley offers Willy a job many times when visiting him, yet Willy declines every time, even after he loses his job as a salesman.
* Bernard (David S. Chandler): Charleys son.  In Willys flashbacks, he is a nerd, and Willy forces him to give Biff test answers.  He worships Biff and does anything for him.  Later, he is a very successful lawyer, married, and expecting a second son.
* Ben Loman (  success story, and is shown coming by the Lomans house while on business trips to share stories or to hear about their lives.
* Woman from Boston (Kathryn Rossetter, as Kathy Rossetter): A woman with whom Willy cheated on Linda.
* Howard Wagner (Jon Polito): Willys boss.  He was named by Willy, and yet he sees Willy as a liability for the company and lets him go, ignoring all the years that Willy has given to the company.  Howard is extremely proud of his wealth, which is manifested in his recording machine, and his family.
* Jenny (Anne McIntosh): Charleys secretary.
* Stanley (Tom Signorelli): A waiter at the restaurant who seems to be friends or acquainted with Happy.
* Miss Forsythe (Linda Kozlowski): A call girl (prostitute) whom Happy picks up at the restaurant.  She is very pretty and claims that she was on several magazine covers.  Happy lies to her, making himself and Biff look like they are important and successful (Happy claims that he attended West Point and that Biff is a star football player).
* Letta (Karen Needle): Miss Forsythes friend; also a call girl.
* Waiter (Michael Quinlan)

==Reception==
The film received critical acclaim. On review aggregate website Rotten Tomatoes, the film holds an overall 100% approval rating based on 8 reviews, with a rating average of 8.4 out of 10. 

== Style ==

The film is mostly told from the point of view of the protagonist, Willy Loman|Willy, and the previous parts of Willys life are revealed in the Flashback (narrative)|analepsis, sometimes during a present day scene.  It does this by having a scene begin in the present time, and adding characters onto the screen whom only Willy can see and hear, representing characters and conversations from other times and places.

Many dramatic techniques are also used to represent these time shifts.  For example, leaves often appear around the current setting (representing the leaves of the two elm trees which were situated next to the house, prior to the development of the apartment blocks).  Biff and Happy are dressed in high school football sweaters and are accompanied with the "gay music of the boys".  The characters will also be allowed to pass through the walls that are impassable in the present, as told in Arthur Miller|Millers original stage directions in the opening of ACT 1, "Whenever the action is in the present the actors observe the imaginary wall-lines, entering the house only through its door at the left.  But in the scenes of the past these boundaries are broken and characters enter or leave a room by stepping through a wall onto the fore-stage."

However some of these time shifts/imaginings occur when there are present characters on-screen.  For example, during a conversation between Willy and his neighbor Charley, Willys brother Ben comes on screen and begins talking to Willy while Charley speaks to Willy.  When Willy begins talking to his brother, the other characters do not understand to whom he is talking, and some of them even begin to suspect that he has "lost it."  However, at times it breaks away from Willys point of view and focuses on the other characters: Linda, Biff, and Happy.  During these parts of the film, the time and place stay constant without any abrupt flashbacks that usually happen while the play takes Willys point of view.

The films structure resembles a stream of consciousness account.  Willy drifts between his living room, downstage, to the apron and flashbacks of an idyllic past, and also to fantasized conversations with Ben.  When we are in the present the characters abide by the rules of the set, entering only through the stage door to the left.  However, when we visit Willys "past" these rules are removed, with characters openly moving through walls.  Whereas the term "flashback" as a form of cinematography for these scenes is often heard, Miller himself rather speaks of "mobile concurrences."  In fact, flashbacks would show an objective image of the past.  Millers mobile concurrences, however, rather show highly subjective memories.  Furthermore, as Willys mental state deteriorates, the boundaries between past and present are destroyed, and the two start to exist in parallel.

== Awards and nominations ==

* Won: 1986 Golden Globe Award for Best Performance by an Actor in a Mini-Series or Motion Picture Made for Television (Dustin Hoffman)
* Won: 1986 Primetime Emmy Award for Outstanding Art Direction for a Miniseries or a Special (Robert J. Franco, John Kasarda, and Tony Walton)
* Won: 1986 Primetime Emmy Award for Outstanding Lead Actor - Miniseries or a Movie (Dustin Hoffman)
* Won: 1986 Primetime Emmy Award for Outstanding Supporting Actor - Miniseries or a Movie (John Malkovich)
* Nominated: 1986 Golden Globe Award for Best Miniseries or Television Film
* Nominated: 1986 Golden Globe Award for Best Supporting Actor – Series, Miniseries or Television Film (John Malkovich)
* Nominated: 1986 Golden Globe Award for Best Supporting Actress – Series, Miniseries or Television Film (Kate Reid)
* Nominated: 1986 Primetime Emmy Award for Outstanding Directing for a Miniseries, Movie or a Dramatic Special (Volker Schlöndorff)
* Nominated: 1986 Primetime Emmy Award for Outstanding Made for Television Movie
* Nominated: 1986 Primetime Emmy Award for Outstanding Achievement in Music Composition for a Miniseries or a Special (Dramatic Underscore) (Alex North)
* Nominated: 1986 Primetime Emmy Award for Outstanding Costume Design for a Miniseries or a Special (Ruth Morley)
* Nominated: 1986 Primetime Emmy Award for Outstanding Drama/Comedy Special (Robert F. Colesberry, Dustin Hoffman, and Arthur Miller)
* Nominated: 1986 Primetime Emmy Award for Outstanding Sound Mixing for a Miniseries or a Special (Tom Fleischman)
* Nominated: 1986 Primetime Emmy Award for Outstanding Supporting Actor - Miniseries or a Movie (Charles Durning)

==References==
 

== External links ==

*  
*  
*  
*  

 

 

 

 
 
 
 
 
 
 
 