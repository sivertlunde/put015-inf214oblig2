The Winning Ticket
{{Infobox film
| name           = The Winning Tickey
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Charles Reisner Jack Cummings
| screenplay     = Ralph Spence Richard Schayer
| story          = Robert Pirosh George Seaton
| starring       = Leo Carrillo Louise Fazenda Ted Healy
| music          = Herbert Stothart Charles Maxwell (uncredited) Charles Clarke
| editing        = Hugh Wynn
| studio         = 
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 70 mins.
| country        = United States
| language       = English
}}

The Winning Ticket is a 1935 American comedy film directed by Charles Reisner. Released by Metro-Goldwyn-Mayer, the film stars Leo Carrillo, Louise Fazenda, and Ted Healy. 

==Overview==
Poor Italian-American barber Joe Thomasello (Leo Carrillo) purchases a sweepstakes ticket at the urging of his brother-in-law Eddie Dugan (Ted Healy). Joes wife Nora (Louise Fazenda) is against gambling. The ticket turns out to be a $150,000 first prize winner. Joe remembers that he gave the ticket to his lawyer Tony (Luis Alberni) to hold. The three men realize that Joes baby, Mickey (Roland Fitzpatrick) was the last one seen with the ticket. When they ask the baby where the ticket is, the baby points to a loose board on the floor. The three men tear up the floor and dig a hole looking for the lost ticket to no avail. Tony, who has huge debts decides that the best thing to do is go to Ireland by boat and try to convince the sweepstakes officials that Joe was the actual sweepstakes winner. 

Eddie has a fear of ships and is tricked by the other men into making the voyage. All the men are eventually kicked off the ship when Eddie is accused of being a stowaway. After this, the men witness baby Mickey stuffing paper into the mouth of one of his fathers many ceramic parrots, which leads them to believe the baby had put the winning ticket into one of the birds. Mr. Powers (Purnell Pratt) has the men arrested after they destroy some of his ceramic birds. Nora, with baby Mickey visit Joe in jail and they present him with his guitar to help him pass the time. Nora and Joe end up in an argument and the guitar gets broken over Joes head which exposes the winning ticket which was put into the guitar by Mickey earlier.

==Cast==
* Leo Carrillo as Joe Tomasello
* Louise Fazenda as Nora Tomasello
* Ted Healy as Eddie Dugan
* Irene Hervey as Mary Tomasello James Ellison as Jimmy
* Luis Alberni as Tony
* Purnell Pratt as Mr. Powers
* Akim Tamiroff as Giuseppe
* Betty Jane Graham as Noreen Tomasello Billy Watson as Joey Tomasello Jr.
* John Indrisano as Lefty Costello
* Roland Fitzpatrick as Mickey Tomasello
* Clara Blandick as Aunt Maggie
* James P. Burtis as Newsreel Man (uncredited)
* Charles Dunbar as Still Cameraman (uncredited)
* Sam Flint as Captain (uncredited)
* George Guhl as Turnkey (uncredited)
* Sherry Hall as Officer (uncredited)
* Buddy Harris as Valet (uncredited) Al Hill as Bookie (uncredited)
* Wilbur Mack as Banker (uncredited)
* Wally Maher as Sound Man (uncredited)
* Jane Meredith as Traveler (uncredited)
* Frank Moran as Bartender (uncredited)
* Milton Owen as Purser (uncredited)
* Lee Phelps as Bookmaker (uncredited)
* C. Montague Shaw as President of Insurance Company (uncredited)
* William Stack as Jeffries (uncredited)
* Larry Steers as Traveler (uncredited) Clarence Wilson as Dolan (uncredited)

==Crew==
* Cedric Gibbons - Art Director David Townsend - Associate Art Director
* Edwin B. Willis - Associate Art Director
* William Axt - Composer: stock music (uncredited)
* Douglas Shearer - Recording Director
* Charles Reisner - Co-Producer

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 
 