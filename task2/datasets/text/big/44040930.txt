Anaavaranam
{{Infobox film
| name = Anaavaranam
| image =
| caption =
| director = A. Vincent
| producer = KJ Joseph
| writer = Thoppil Bhasi
| screenplay = Thoppil Bhasi
| starring = M Chandran Nair Sathar Janardanan Rani Chandra
| music = G. Devarajan
| cinematography = Soorya Prakash
| editing = G Venkittaraman
| studio = Cherupushpam Films
| distributor = Cherupushpam Films
| released =  
| country = India Malayalam
}}
 1976 Cinema Indian Malayalam Malayalam film, directed by A. Vincent and produced by KJ Joseph. The film stars M Chandran Nair, Sathar, Janardanan and Rani Chandra in lead roles. The film had musical score by G. Devarajan.   

==Cast==
  
*M Chandran Nair 
*Sathar
*Janardanan 
*Rani Chandra 
*Shylaja
*Usharani 
 

==Soundtrack==
The music was composed by G. Devarajan and lyrics was written by Vayalar. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Nanma Niranjoru || P. Leela, P. Madhuri || Vayalar || 
|- 
| 2 || Pachakarpooramalayil || P Susheela || Vayalar || 
|- 
| 3 || Saraswatheeyaamam Kazhinju || K. J. Yesudas || Vayalar || 
|- 
| 4 || Thevi Thiru Thevi || P. Madhuri || Vayalar || 
|- 
| 5 || Thinthinathim || K. J. Yesudas, P. Madhuri || Vayalar || 
|}

==References==
 

==External links==
*  

 
 
 


 