The Mystery at Castle House
{{Infobox film
| name           = The Mystery at Castle House
| image          = 
| image size     =
| caption        = 
| director       = Peter Maxwell
| producer       = 
| writer         = 
| based on = 
| narrator       =
| starring       = Aileen Britton Henri Szeps Simone Buchanan Ray Meagher
| music          = 
| cinematography = 
| editing        = 
| studio = 
| distributor    = 
| released       = 1982
| runtime        = 
| country        = Australia English
| budget         =
| gross = 
| preceded by    =
| followed by    =
}}
Mystery at Castle House is a 1982 Australian film for children.  Ed. Scott Murray, Australia on the Small Screen 1970-1995, Oxford Uni Press, 1996 p109 

==Plot==
Three free spirited children named Spider, Kate, and Ben explore Castle House that has been abandoned for 15 years. The Castle is shrouded in intrigue as the children discover a secret tunnel that leads to clues to help them solve the mystery of Castle House.

==References==
 

==External links==
*  at IMDB

 
 
 
 


 
 