Dead Letters (film)
{{multiple issues|
 
 
}}
 

{{Infobox Film
| name           =  Dead Letters
| image_size     = 
| image	=	Dead Letters FilmPoster.jpeg
| caption        = A poster with the films original title: Cold Ones
| director       =  Garrett Clancy
| producer       =  Garrett Clancy Josephina Sykes Patricia Anne Isgate
| writer         =  Garrett Clancy
| narrator       = Geoffrey Lewis Kim Darby Joe Unger
| music          =  David Baerwald
| cinematography =  Scott Spears Richard Casey David H. Lloyd 
| studio         =  Brightway Productions Inc.
| distributor    =  Leo Films
| released       =  
| runtime        = 90 min.
| country        = United States English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Geoffrey Lewis, Kim Darby, Joe Unger and Duane Whitaker. It was originally entitled Cold Ones, and was retitled by Distributor, Leo Films, for its 2009 DVD release. It won Honorable Mention in the feature film category at the 2007 Buffalo Niagara Film Festival.  The films score was composed by David Baerwald who also has several songs on the soundtrack, including "Hi Ho" with vocals by Terra Naomi. Dead Letters (as Cold Ones) also played at the 2007 Real to Reel Film Festival  in North Carolina, and both The California Independent  and the Kern Projections Film Festivals.

==References==
 

== External links ==
*  
*  
*  

 
 
 
 
 


 