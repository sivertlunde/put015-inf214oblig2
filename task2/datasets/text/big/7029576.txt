Four Eyed Monsters
 
{{Multiple issues|
 
 
 
}}

{{Infobox film
| name           = Four Eyed Monsters
| image          = Four.Eyed.Monsters.Logo.png
| caption        = 
| director       = Susan Buice Arin Crumley
| writer         = Susan Buice Arin Crumley
| starring       = Susan Buice Arin Crumley
| music          = Andrew A. Peterson
| cinematography = 
| editing        = Susan Buice Arin Crumley
| distributor    = self-distributed
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = $927
}} Creative Commons Attribution-Sharealike license allowing audience members to redistribute or even alter as they wish.  Since then a number of screenings have been utilized taking advantage of this open license. 

The film was shot on MiniDV using the Panasonic AG-DVX100 mostly in Brooklyn and Manhattan, New York, with supplementary shooting in Framingham, Massachusetts and Johnson, Vermont. It was edited on Apple Inc.|Apples Final Cut Pro editing software. It debuted on the festival circuit in January 2005 at the Slamdance Film Festival where it was well received. At first the filmmakers hoped to obtain a conventional deal for theatrical distribution on the basis of its success at Slamdance, but nothing was forthcoming.  This is when they decided to release what eventually grew into a two season 13 episode online series about the journey of creating the film.  This attention eventually grabbed the attention of IFC TV who went on to air the film, release in iTunes and sell DVDs through Borders bookstore.  IFC then stopped selling the film and for a period of time it was only available in Canada through Films We Like and UK via DOG WOOF.  IN 2009 the film was relaunched on YouTube and in 2010 released on VODO. 

==Plot==
 
A shy videographer (Arin) and an uninspired artist working as a waitress (Susan) meet on the Internet and spark a relationship. Fed up with the usual dating game, the two decide to not communicate verbally, only through artistic means to see if they can make it work.

==Video podcast== Four Eyed IFC as a featured web series.

==Awards==
 
* Best New Directors - 2005 Brooklyn International Film Festival
* Chameleon Award - 2005 Brooklyn International Film Festival
* Student Jury Award - 2005 Newport International Film Festival South by South West Film Festival
* Special Mention - 2005 Sidewalk Moving Picture Festival
* Nominated for Best Cinematography - 2007 Independent Spirit Awards
* Nominated for John Cassavetes Award - 2007 Independent Spirit Awards Sundance Channel still under negotiation.

==References==
 

==External links==
*  
*  
*  
 

 
 
 
 
 