The Singing Vagabond
{{Infobox film
| name           = The Singing Vagabond
| image          = The_Singing_Vagabond_Poster.jpg
| caption        = Theatrical release poster
| director       = Carl Pierson
| producer       = Nat Levine (uncredited)
| screenplay     = {{Plainlist| Oliver Drake
* Betty Burbridge
}}
| story          = {{Plainlist|
* Oliver Drake
* William Witney (uncredited)
}}
| starring       = {{Plainlist|
* Gene Autry
* Ann Rutherford
* Smiley Burnette
}}
| cinematography = {{Plainlist|
* William Nobles
* Edgar Lyons (uncredited)
}}
| editing        = Lester Orlebeck
| studio         = Republic Pictures
| distributor    = Republic Pictures
| released       =  
| runtime        = 56 minutes
| country        = United States
| language       = English
}} Western film Oliver Drake and Betty Burbridge, the film is about a cowboy who rides to the rescue when badguys kidnap a beautiful woman.   

==Plot==
At a St. Louis opera house in 1860, a singer in blackface named Jerry Barton, known as "King of the Minstrels", comes backstage and asks his sweetheart, Lettie Morgan (Ann Rutherford), to elope. Letties Aunt Hortense, fearing that Barton is a fortune hunter, tells Lettie she is not the heiress she thought she was and that she has been living off her aunts charity. With no fortune to hunt, Barton informs Lettie that an artist cannot be burdened with the responsibility of a wife.

Outside the opera house, Lettie meets a chorus girl named Honey (Barbara Pepper), who is preparing to leave with her theatrical troupe in a caravan heading West. When the troupes producer mistakes Lettie for the star, she joins the group as "Mary Varden". The troupes wagon train is escorted by Captain Tex Autry (Gene Autry) of the U.S. Cavalry and his singing plainsmen. The troupe misses the wagon train, however, and must travel alone. 

On their way to San Francisco, the caravan is ambushed by a gang of thieves. Tex and his men arrive on the scene and following a gunfight, the gang is chased off. After Tex saves Lettie from a runaway wagon, he comments on the foolishness of risking his mens lives for a bunch of "crazy showgirls". Angered by his insolence, Lettie decides to walk rather than ride with Tex. Eventually she gets tired and asks Tex if she can ride with him. The troupe arrives safely at Fort Henry, which is run by Colonel Seward (Frank LaRue). 

An Indian named Young Deer warns Tex that Chief White Eagle is preparing to attack the army. When horses are stolen from the fort by a renegade named Buck LaCrosse (Warner Richmond), Tex saves the horses. Utah Joe (Allan Sears), who is in league with Chief White Eagle, falsely accuses Tex of complicity with the Indians, and Tex is soon arrested for treason. Aunt Hortense arrives with Judge Forsythe Lane (Niles Welch), who hopes to marry Lettie and use her money to run for president. Aunt Hortense encourages her to marry Lane. Lettie appeals to Lane on Texs behalf, hinting that she will marry him if he will save Tex. As the wagon train prepares to leave, Lettie sadly says goodbye to Tex, and Lane promises to join Lettie after the trial. However, Lane double-crosses Lettie and helps to secure Texs conviction. 

Following his conviction and death sentence, Tex escapes with the help of his friends. Suspicious that Utah Joe has promised to supply Chief White Eagle with ammunition, Tex orders his friend Frog (Smiley Burnette) to join the wagon train to spy on Utah Joe. While the caravan camps, Frog tells Lettie that Lane encouraged Texs conviction. Honey, who has fallen in love with Frog, tells Lettie that Lane was probably jealous of Letties feelings for Tex, but Lettie denies loving the soldier. When Utah Joe announces plans to take a new route through Kern Valley, Frog warns that the valley is filled with renegades and unfriendly Indians. 

Later that night Tex arrives and overhears Utah Joe direct Chief White Eagle to the wagons stocked with gunpowder. Tex pulls his gun on them, and a fight ensues, during which LaCrosse arrives on the scene. As Frog and Tex try to fight off the renegades, the soldiers ride up. Chief White Eagle is shot during the scuffle, but Utah Joe escapes. LaCrosse is arrested and, under the threat of a firing squad, confesses that Utah Joe instigated the horse stealing at the fort, while he let loose the clever black stallion who opened the corral gate. LaCrosse also warns the caravan that Utah Joe is leading them into an ambush. 

As the caravan prepares to leave, Utah Joe, now dressed as an Indian, sends a smoke signal, and the Indians approach. Frog is grazed by a bullet and inadvertently becomes attached to the underside of the runaway powder wagon. Tex manages to save him, and igniting the wagon, sends it into a throng of Indians. The Indians retreat once the plainsmen arrive, and as the wagon train departs, Tex and Lettie kiss, with Honey nursing Frog behind the embracing couple.

==Cast==
* Gene Autry as Capt. Tex Autry
* Ann Rutherford as Lettie Morgan
* Smiley Burnette as Frog
* Barbara Pepper as Honey
* Champion as Champion
* Niles Welch as Judge Forsythe Lane
* Grace Goodall as Aunt Hortense
* Allan Sears as Utah Joe
* Warner Richmond as Buck LaCrosse
* Henry Roquemore as Otto Speth, the Show Troupe Leader
* Frank LaRue as Col. Seward
* Chief John Big Tree as Chief White Eagle (uncredited)
* Chief Thundercloud as Young Deer (uncredited)   

==Production==
===Stuntwork===
* Cliff Lyons
* George Montgomery 

===Filming locations===
* Kernville, California, USA   

===Soundtrack===
* "Lousiana Belle" (Stephen Foster) by minstrels in blackface at a show
* "Gwine to Rune All Night (De Camptown Races)" (Stephen Foster) by Robinson Neeman and danced by an unidentified black child
* "Old Folks at Home (Swanee River)" (Stephen Foster) by two unidentified men
* "Singing Vagabonds" (Herbert Myers, Oliver Drake) by Gene Autry and his singing plainsmen
* "Honeymoon Trail" (Herbert Myers, Oliver Drake) by Gene Autry, Ann Rutherford, Smiley Burnette, Barbara Pepper, and Showgirls
* "Friends Of The Prairie, Farewell" (Smiley Burnette) by Gene Autry, Ann Rutherford, and Showgirls
* "Wagon Train" (Gene Autry, Smiley Burnette) by most of the cast   

==References==
;Citations
 
;Bibliography
 
*  
*  
*   

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 