Nana 2
{{Infobox film| name = NANA2
| image = nana2movieposter.jpg
| image size = 190px
| caption = Theatrical release poster 
| producer = Osamu Kubota, Toshiaki Nakazawa
| director = Kentarō Ōtani    |
| writer = Ai Yazawa, Kentarō Ōtani, Taeko Asano
| starring = Mika Nakashima  Yui Ichikawa Hiroki Narimiya Kanata Hongo Yuna Ito and more|
| music = 
| cinematography = Kazuhiro Suzuki
| editing = 
| distributor = Toho| Toho Movie Production
| released =  
| runtime = 115 min  Japanese 
| gross          = 
}}
 the manga by Ai Yazawa, directed by Kentarô Ôtani. Production for the movie began in mid-September and only one and a half month shootings finished the movie in time for the December 9, 2006 release.

==Synopsis==
Taking place shortly after the end of the first film, Nana 2 focuses more on Komatsu Nana (Hachi/Hachiko) and her love life. Romance develops between Hachi and TRAPNESTs bassist Takumi as well as with the Black Stones guitarist Nobu. Meanwhile, Nana works hard for her band while trying to find happiness. Both girls struggle through life, and try to keep their friendship from falling apart. More information on the story at Nana (manga)|Nana.

==Cast==
*Nana Komatsu : Yui Ichikawa
*Nana Osaki : Mika Nakashima
*Ren Honjo : Nobuo Kyo 
*Takumi Ichinose : Tetsuji Tamayama
*Shinichi Okazaki a.k.a. Shin: Kanata Hongo
*Nobuo Terashima : Hiroki Narimiya
*Reira Serizawa : Yuna Ito

==Casting issues==
There were several casting issues in the production of Nana 2.  Aoi Miyazaki had declined to reprise the role of Komatsu Nana.  She was replaced with actress Yui Ichikawa. Ryuhei Matsuda had declined to play the role of Ren and was replaced with Nobuo Kyō. Also, Kenichi Matsuyama, who portrayed Shinichi Okazaki, had been replaced by Hongo Kanata.

==Theatrical release==

Even though both theme songs were released ahead (Hitoiro of NANA starring MIKA NAKASHIMA on November 29, 2006 and Truth of REIRA starring YUNA ITO on December 6, 2006), the movie could only hit the #4 spot on the Movie Charts, from its release December 9, 2006 onward, and overall had very weak and low ratings. Many fans claimed that the exchange of main cast members led to the disappointing statistics.

On December 18, NANA2 made its international debut in New York, USA. New York - Tokyo brought the main actresses Mika Nakashima and Yui Ichikawa to the limited seated IFC Cinema. The movie was well received by fans.

==Theme songs==
Nana 2 once again featured the two artists Mika Nakashima and Yuna Ito, releasing songs under the names Nana starring Mika Nakashima and Reira starring Yuna Ito, respectively.
 The End, which managed to rank at #2 on the Oricon charts.

Reira, starring Yuna Itos "Truth (Yuna Ito song)|Truth" had no more luck than Nakashimas new single, ranking in only at the tenth place on the Oricon charts. The music video of the single was filmed in Scotland and used as scenes of the film. Neither of the singles were as record-setting as their predecessors.

==External links==
*   (Japanese)
*   (Italian)
*  

 

 
 
 
 
 
 
 
 
 
 

 