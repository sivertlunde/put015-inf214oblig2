Stranded: Náufragos
{{Infobox film
| name           = Stranded
| image          = Stranded_imp.jpg
| alt            =
| caption        = Theatrical release poster
| director       = María Lidón
| producer       = José Magán
| writer         = Juan Miguel Aguilera
| starring       = Vincent Gallo Maria de Medeiros Joaquim de Almeida Maria Lidón  Daniel Aser  Johnny Ramone  José Sancho  Paul Gibert
| music          = Javier Navarrete
| cinematography =
| editing        = Luis de la Madrid
| studio         = Universal Pictures, Niggeman IndieFilms S.L
| released       =  
| runtime        = 95 minutes
| country        = Spain
| language       = English Spanish
| budget         = $6,230,811 (810 million pesetas) 
  
{{cite web
    | url = http://www.hiperespacio.info/Artics/naufragos/naufragos.htm
    | title = NAUFRAGOS - En el espacio, nadie puede oir tus gritos.
    | language = Spanish
    | trans_title = SHIPWRECKED - In space, nobody can hear you scream.
    | work = Hiperespacio.info
    | last = Mayorga
    | first = Luis F.
    | accessdate = 2010-08-14
    | quote = Se han invertido 810 millones de pesetas de presupuesto en esta película, de los que unos 100 se están invirtiendo en efectos especiales.
  }} 
| gross          =
}}

Stranded is a 2001 film about a fictional first manned mission to Mars. It stars Vincent Gallo and Maria de Medeiros, and was directed by Spanish filmmaker and actress María Lidón (credited in the English version of the movie as "Luna"), with screenplay by Spanish science fiction author Juan Miguel Aguilera. Lidón won the "Grand Prize of European Fantasy Film in Silver", and Gallo and de Medeiros were named best actors at the 2002 Fantafestival in Rome.   

==Plot==
The film is set in 2020 and begins as the Ares spacecraft enters orbit around Mars. Andre Vishniac commands an international crew of seven astronauts. They try to land, but the small landing craft crashes as a result of an altimeter error. Vishniac is immediately killed, and five other crew members are stranded inside the toppled landing craft, unable to return to the waiting Ares mothercraft. With no spare landing craft, Lowell, the pilot of the Ares, returns to Earth with it.

It will take 26 months for Lowell to send a rescue ship from Earth, but the stranded landing crew have supplies for less than a year and need to find ways to extend the life support system. The main problem is the thermoelectric power generator, since air and water recycling require electrical power. Since the lander is damaged beyond repair, it no longer requires fuel, and Sagan (the mission geologist) proposes to use what is left to power an improvised silicate reactor to produce methane and water vapor from the martian soil. They try to build it, but the landing engines and propellant tanks prove to be too damaged to salvage.

The landing crew tries to find ways to save electrical power, but even draconian measures will only extend the life of the generator to fourteen months. The required lifetime can be attained only if the crew is reduced to two astronauts. Sanchez, the new commander, decides that she, Sagan and Rodrigo should abandon the craft, since their specialist skills are less important to the survival of the mission. They don their spacesuits and walk to the edge of the Valles Marineris valley, not far from the crash site. Recent radar scans from the orbiter reveal a maze-like structure hidden below a thick cloud of water vapor in the Valles Marineris. The structure appears artificial and is virtually identical to another one near the martian south pole. The three astronauts try to reach the bottom of the valley before they run out of oxygen.

Sagan dies of asphyxiation, but Sanchez and Rodrigo stumble upon an artificial tunnel near the bottom of the valley, containing the mummified bodies of humanoid alien beings. They find that the air pressure and oxygen content in some of the tunnels is just like that on Earth. Rodrigo dies when accidentally entering a tunnel with no atmosphere, but Sanchez manages to contact the two astronauts still at the lander, who have meanwhile discovered that they are gradually losing air pressure and have realised that there must be an air leak somewhere. Sanchez urges them to join her, as there is enough oxygen and water in the valley for them to survive until the rescue ship arrives.

==Cast==
  as TV newscaster George Collins, and Jose Sancho (lower right) plays mission commander Andre Vishniac.]]
{| class="wikitable"
|-
! Actor !! Role
|-
| Vincent Gallo
| Luca Baglioni (Mars lander mission specialist, technical systems engineer)
|-
| Maria de Medeiros
| Jenny Johnson (Mars lander mission specialist, doctor)
|- Joaquim de Almeida
| Fidel Rodrigo (Mars lander mission specialist, astrobiologist)
|-
| Maria Lidón
| Susana Sánchez (Mars lander pilot)
|-
| José Sancho
| Andre Vishniac (Mars lander commander)
|- Danel Aser
| Herbert Sagan (Mars lander mission specialist, geologist)
|-
| Johnny Ramone
| Lowell (Mars orbiter pilot)
|}

==Filming locations==
The scenes set on the surface of Mars were filmed on Lanzarote, and the interior scenes were filmed at Panavision Studios in Hollywood. To save costs, they used the Space Shuttle interior set previously used for Space Cowboys.

==Weird Science==
  type cabin, and the orbiting mothership special-effects model consists of tankage from European Ariane 5 and Ariane 4 rockets. The red arrow indicates the landing site at approx. -10° latitude, 70° longitude.]]

* In the movie, the astronauts stand on the edge of Valles Marineris and look at the other edge of the valley 300 kilometers away. However, as the diameter of Mars is only 6700&nbsp;km, the other side at that distance would be hidden below the horizon. 
* In the opening scene, a presumably Earth-based newscaster has a real-time conversation with lander commander Vishniac. However, radio waves take between four and twenty minutes to reach Mars from Earth&nbsp;– depending on the planets relative positions&nbsp;– so such a conversation would be impossible. 

== References ==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 