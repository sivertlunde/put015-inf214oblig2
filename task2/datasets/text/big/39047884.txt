Rossini! Rossini!
{{Infobox film
| name = Rossini! Rossini!
| image = Rossini! Rossini!.jpg
| caption =
| director = Mario Monicelli
| writer = Mario Monicelli, Suso Cecchi dAmico, Nicola Badalucco, Bruno Cagli
| starring =Sergio Castellitto 
| music = Giovanni Pergolesi, Gioachino Rossini
| cinematography = Franco Di Giacomo
| editing = Ruggero Mastroianni
| producer =
| distributor =
| released = 1991
| runtime = 130 min
| awards =
| country = Italy
| language = Italian
| budget =
}} 1991 Italian Best Costumes.   

== Plot ==
In 1868 the Italian composer Gioachino Rossini is already famous all over the country. However, his last opera The Barber of Seville is not understood and even booed by the audience at La Scala for the indecency of the sets and love situations. Also disappointed by the replicas at the Teatro San Carlo in Naples, then Rossini decides to move to Paris, where he is hailed as a genius.

== Cast ==
*Sergio Castellitto as Young Rossini  
*Philippe Noiret as  Old Rossini  
*Giorgio Gaber as  Domenico Barbaja
*Jacqueline Bisset as  Isabella Colbran
*Assumpta Serna as  Maria Marcolini
*Sabine Azéma as  Olympe Pélissier
*Galeazzo Benti as  La Rochefoucauld
*Feodor Chaliapin Jr. as Baron Rothschild
*Claudio Gora as Dr. Bardos  
*Silvia Cohen as Marietta Alboni  
*Pia Velsi as Adina 
*Maurizio Mattioli as Mimì  
*Vittorio Gassman as  Ludwig van Beethoven (Cameo appearance|cameo)

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 
 


 
 