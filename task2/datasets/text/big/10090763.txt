Buck Rogers (serial)
{{Infobox film name           = Buck Rogers image          = Buckrogersserial.jpg image size     = caption        = director       = Ford Beebe  Saul A. Goodkind producer       = Barney A. Sarecky writer         = Norman S. Hall  Ray Trampe  Dick Calkins   Based on Buck Rogers created by Philip Francis Nowlan  starring       = Buster Crabbe  Constance Moore  Jackie Moran  Anthony Warde  C. Montague Shaw  Jack Mulhall cinematography = Jerome Ash music          =  editing        = Joseph Gluck  Louis Sackin  Alvin Todd distributor  Universal Pictures released       = February 2, 1939 runtime        = 12 chapters (237 min) country        =   language  English
}}
 Universal Serial serial film title character Flash Gordon serials and would return for a third in 1940) as the eponymous hero, Constance Moore, Jackie Moran and Anthony Warde. It was based on the Buck Rogers character created by Philip Francis Nowlan, which had appeared in magazines and comic strips since 1928.

==Plot== dirigible flight William Gould), resist the criminal rulers of Earth.
 Henry Brandon). The serial then becomes a back-and-forth struggle between Buck and Kane to secure the military support of Saturn for the struggles on Earth.

==Production notes==
This 12-part Buck Rogers movie serial was launched in 1939. It starred Buster Crabbe, who had previously played the role of Flash Gordon in two movie serials prior to Buck Rogers. Constance Moore played Lieutenant Wilma Deering, the only woman in the film, and Jackie Moran was Buddy Wade, a character who did not appear in other versions of the Buck Rogers franchise, but who was clearly modeled on the Sunday strip character Buddy Deering. Anthony Warde played "Killer Kane", Buck Rogers enemy; this was the only time that Warde, who usually portrayed evil underlings in serials, played a lead villain. Korean-American actor Philson Ahn, younger brother of noted actor Philip Ahn, played Prince Tallen, a Saturnian native who befriends Buck Rogers.
 David Sharpe, who appeared in over 4,500 films over the course of a seven-decade career, also appeared in the Buck Rogers serial in several roles: as one of Kanes pilots, a Hidden City sentry, and a Saturnian lieutenant.

The serial had a small budget and saved money on special effects by re-using material from other stories: background shots from the futuristic musical Just Imagine (1930), as the city of the future, the garishly stenciled walls from the Azura palace set in Flash Gordons Trip to Mars, and even the studded leather belt that Crabbe wore in Flash Gordons Trip to Mars, turned up as part of Bucks uniform.

In 1953, the 1939 movie serial was edited into a feature film entitled Planet Outlaws. Then it was edited again to feature length and titled Destination Saturn for syndication to television, in 1965. Finally, the serial was edited once again into a feature film format in the late 1970s, this version simply entitled Buck Rogers with the theatrical poster advertising, "Star Wars owes it all to Buck Rogers", and later was sold on videotape in the early 1990s by VCI Entertainment under the catalogue title of Planet Outlaws (which title, to make it appear legitimate, was also superimposed onto the first shot of film following the main titles). VCI released all twelve installments on DVD in September 2000. In November 2009, VCI released a special 70th anniversary edition on DVD, with extras including "The History of Buck Rogers" by Clifford Weimer, a photo gallery and the 1935 Buck Rogers short feature originally shown at the 1933-34 Worlds Fair.

==Cast==
* Buster Crabbe as Buck Rogers
* Constance Moore as Wilma Deering. This was the only serial appearance for Moore, a singer on radio and the New York stage. {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 92
 | chapter = 5. A Cheer for the Champions (The Heroes and Heroines)
 }} 
* Jackie Moran as George "Buddy" Wade
* Anthony Warde as "Killer" Kane
* C. Montague Shaw as Doctor Huer
* Jack Mulhall as Captain Rankin
* Guy Usher as Aldar William Gould as Air Marshal Kragg
* Philson Ahn as Prince Tallen Henry Brandon as Captain Laska

==Chapter titles==
# Tomorrows World
# Tragedy on Saturn
# The Enemys Stronghold
# The Sky Patrol
# The Phantom Plane
# The Unknown Command
# Primitive Urge
# Revolt of the Zuggs
# Bodies Without Minds
# Broken Barriers
# A Prince in Bondage
# War of the Planets
 Source:  {{cite book
 | last = Cline
 | first = William C.
 | title = In the Nick of Time
 | year = 1984
 | publisher = McFarland & Company, Inc.
 | isbn = 0-7864-0471-X
 | pages = 223–224
 | chapter = Filmography
 }} 

==See also==
* List of film serials by year
* List of film serials by studio

==References==
 

==External links==
*    
*  
*  
*  
*  

 
{{Succession box Universal Serial Serial
| before=Scouts to the Rescue (1939 in film|1939)
| years=Buck Rogers (1939 in film|1939) The Oregon Trail (1939 in film|1939)}}
 

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 