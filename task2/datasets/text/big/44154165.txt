Ee Yugam
{{Infobox film
| name           = Ee Yugam
| image          =
| caption        =
| director       = NP Suresh
| producer       = Purushan Alappuzha
| writer         = Purushan Alappuzha Alappuzha Karthikeyan (dialogues)
| screenplay     = Alappuzha Karthikeyan Hari Rohini Rohini
| music          = A. T. Ummer
| cinematography = Rajan
| editing        = NP Suresh
| studio         = Sreedevi Movies
| distributor    = Sreedevi Movies
| released       =  
| country        = India Malayalam
}}
 1983 Cinema Indian Malayalam Malayalam film, Hari and Rohini in lead roles. The film had musical score by A. T. Ummer.   

==Cast==
 
*Prem Nazir as Dr Prasad
*Srividya as Aparna Hari
*Rohini Rohini
*Kalaranjini
*Sukumaran
*Balan K Nair
*Janardanan
*Kundara Johny as Vaasu
*MG Soman
*Mala Aravindan Meena
*Ranipadmini as Madhavi 
*Sabitha Anand
*Sathyakala as Prema 
*Shanavas as Prasad
*Usharani as  Ammu
*Kaduvakulam Antony as Pappan
 

==Soundtrack==
The music was composed by A. T. Ummer and lyrics was written by Koorkkancheri Sugathan and Poovachal Khader. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kanna Nin Kilikkonchal   || S Janaki || Poovachal Khader, Koorkkancheri Sugathan || 
|-
| 2 || Maanathin manimuttathu || S Janaki, Jolly Abraham || Poovachal Khader || 
|-
| 3 || Manjaadikutti malavedathi || Krishnachandran || Poovachal Khader || 
|}

==References==
 

==External links==
*  

 
 
 

 