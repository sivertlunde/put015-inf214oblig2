Babysitter Wanted
{{Infobox film
| name = Babysitter Wanted
| image = Babysitterwanted.jpg
| image_size     = 215px
| caption = Promotional poster
| director = Michael Manasseri Jonas Barnes
| producer = Kimberley Kates Michael Manasseri
| writer = Jonas Barnes Sarah Thompson Kristen Dalton
| music = Kurt Oldman
| cinematography = Alex Vendler
| editing = Stephen Eckelberry
| studio = Big Screen Entertainment Group
| distributor = Big Screen Entertainment Group
| released =   
| runtime = 93 minutes
| country = United States
| language = English
| budget = $8 million
| gross = $36,345, 678
}}
Babysitter Wanted is a 2008 American horror film directed by Jonas Barnes & Michael Manasseri being written by Jonas Barnes.

==Plot==
A new life is starting for Angie, who’s leaving the embrace of her religious mother for college studies. She applies for a babysitting job after realizing she will need to find a job to support herself in college. She finds herself on a remote farm before the Stanton (Bruce Thomas and Kristen Dalton) family and their adorable little boy, Sam.  But Angie’s first night of work might just be her last.

Alone in the vast and sprawling house, anonymous phone calls begin to shatter her peace of mind.  The calls soon turn into a face to face confrontation with a hulking beast of a man who breaks through the front door.  Angie must fight for her life to protect herself and Sam.  Angie then realizes that Sam is a born child of the devil, and the man who broke in was a priest that discovered the familys horrible secret. Sam
s mother and father return home and they reveal they are just as evil, hiding their possessed childs secret with a hat to hide his horns and feeding him babysitters they reel in, and Angie was one of the babysitters.

They hold Angie hostage, and she manages to fight back for a while, and when the police arrive, she, with the help of an officer she called earlier, they kill the family, but Sam appears, kills the officer, and chases Angie into the barn where she seemingly defeats him as well. Angie wakes up in a hospital and the police inform her they didnt find a little boy, much to Angies horror. It is then shown that Sam is alive, now with another family, with the mother putting up flyers for a needed babysitter; just another one to reel in...

==Release==
On February 23, 2009 had a limited theatrical release.  The DVD and Blu-ray release is set for May 25, 2010. 

==Production== Sarah Thompson, Matt Dallas, Bill Moseley, Bruce Thomas, Nana Visitor, Monty Bane and introduced Kai Caster. It was produced by Kimberley Kates and Stephen Eckelberry for Big Screen Entertainment Group.  
The Director of Photography was Alex Vendler.
 Lake Shastina, Weed and Yreka, California. 

==Soundtrack==
The score was composed by Swiss Musician Kurt Oldman. 

==Awards==
Winner of Best Feature at Weekend of Fear Festival in Nuremberg, Germany.  It was also accepted into competition at Fantasporto, Sitges, Hollywood Film Festival, Malaga Film Festival, Montreals Fantasiafest. 

==Reception==
Sarah Thompsons role as Angie Albright was awarded number 30 on Total Films list of 50 Most Bad-Ass Female Horror Leads. 

==References==
 

==External links==
*  
*  

 
 
 
 
 
 
 
 