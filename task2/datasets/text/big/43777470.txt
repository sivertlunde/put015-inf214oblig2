Their Big Moment
{{Infobox film
| name = Their Big Moment
| image = 
| alt = 
| caption = 
| film name =  Charles Kerr (assistant)
| producer = Pandro S. Berman Cliff Reid
| writer = Arthur Caesar Marion Dix
| screenplay = 
| story = 
| based on =     
| starring = ZaSu Pitts Slim Summerville
| narrator = 
| music = Max Steiner
| cinematography = Harold Wenstrom William Hamilton RKO Radio Pictures
| distributor = 
| released =      }}
| runtime = 
| country = 
| language = 
| budget = 
| gross =        
}}

Their Big Moment is a 1934 American mystery film directed by James Cruze, from a screenplay by Arthur Caesar and Marion Dix.  The film starred ZaSu Pitts and Slim Summerville.

==References==
 

 