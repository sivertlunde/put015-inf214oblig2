Flaming Hearts
 
{{Infobox film
| name           = Flaming Hearts
| image          = 
| caption        = 
| director       = Walter Bockmayer Rolf Bührmann
| producer       = Walter Bockmayer
| writer         = Walter Bockmayer Rolf Bührmann
| starring       = Peter Kern
| music          = Michael Rother
| cinematography = Horst Knechtel
| editing        = Ila von Hasperg
| distributor    = 
| released       =  
| runtime        = 95 minutes
| country        = West Germany
| language       = German
| budget         = 
}}

Flaming Hearts ( ) is a 1978 West German drama film directed by Walter Bockmayer and Rolf Bührmann. It was entered into the 28th Berlin International Film Festival.    Michael Rother produced the music for the film which was released the previous year as his debut solo album Flammende Herzen.

==Plot==
Peter Huber (Peter Kern), the proprietor of a Bavarian corner newsstand, wins a free trip to New York City in a magazine contest, he is overjoyed. Filled with romantic ideas from the movies, his actual encounter with the gritty realities of the Big Apple are sobering. Nonetheless, he is in for the adventure of his life. First, he meets Karola Faber (Barbara Valentin), the German wife of a U.S. G.I. who has found life in the States not all it’s cracked up to be: she has left her husband and makes her living through prostitution. Peter and Karola visit the local German émigré community’s Oktoberfest, and win the festival’s King and Queen crown. Their prize is a cow, which accompanies them on their further journeys in New York City.

==Cast==
* Peter Kern – Peter Huber
* Barbara Valentin – Karola Faber
* Enzi Fuchs – Anna Schlätel
* Katja Rupé – Magda Weberscheid
* Anneliese Geisler – Frau Geisler Peter Geisler – Herr Geisler
* Rolf Bührmann – Conferencier
* Armin Meier – Fahrer
* Ila von Hasperg – Verkäuferin
* Evelyn Künneke – Sängerin im Lokal

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 