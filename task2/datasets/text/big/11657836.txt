Tera Mera Saath Rahen
{{Infobox film
| name           = Tera Mera Saath Rahen
| image          = Tera_Mera_Saath_Rahen.jpg
| image size     =
| caption        = 
| director       = Mahesh Manjreker
| producer       = N.R. Pachisia Sunil Saini
| story          = Deepak Kulkarni Mahesh Manjrekar
| narrator       =
| starring       = Ajay Devgn Sonali Bendre
| music          = Anand Raj Anand
| cinematography = Vijay Arora
| editing        = V.N. Mayekar
| distributor    =
| released       = 7 November 2001
| runtime        =
| country        = India Hindi
}}
 2001 Hindi Hindiromantic drama film directed by Mahesh Manjreker.

==Plot summary==
“Haathon Ki Lakeeron,” the first of the four versions of the title song, opens up the drama.

Raj Dixit (Ajay Devgan) is a respectable average man who lives with his younger brother Rahul (Dushyant Wagh). The obstacle in Rahul’s life is not his physical handicap, cerebral palsy, but his imperative need for Raj. His dependency is like a flower without sunlight, without it he would simply whither away. When the normal youngster’s activities are for Rahul impossible without Raj, we come to see that their bond is improbably one of the closest. Raj seems quite solemn with a humane personality.

Khanna (Prem Chopra) takes advantage of Rajs quiescent personality and asks him to marry his niece, Madhuri (Sonali Bendre). It doesn’t take long before Raj accepts the offer and develops a relationship with Madhuri. However, during “Pehli Nazar” and “Tadapati Hai, Tarsati Hai,” two romantic well-pictured numbers, a dilemma is evident. Raj explains to Madhuri that their relationship has little room for growth, as lovers anyways. After all if the sun started shining other places what would happen to the plants? Raj tries to present this to a reluctant Madhuri who later offers to send Rahul off to a school where his needs and dependency could be well suited. He decides, after some though, to try it out.

We see who is the dependent one here and who has the handicap. There’s more to the story: that angle is Suman (Namrata Shirodkar). Suman and her parents are Rajs neighbors, somewhat of surrogate parents to him at times. Suman is the fourth angle of the quadrangle.

==Cast==
*Ajay Devgan ...  Raj Dixit
*Sonali Bendre ...  Madhuri
*Namrata Shirodkar ...  Suman Gupta
*Prem Chopra ...  Mr. Khanna
* Anand Abhyankar
* Kishore Nandalaskar
* Shivaji Satam
* Reema lagu
* Sayaji Shinde
* Swapnil Kamble ... Mr. Amit Chauhan

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Pehli Nazar"
| Udit Narayan, Alka Yagnik
|- 
| 2
| "Jumbo Jet"
| Atul Kale
|- 
| 3
| "Dil Wahi Bekaraar Hota Hai"
| Udit Narayan, Alka Yagnik
|- 
| 4
| "Main Sochon"
| Hariharan (singer)|Hariharan, Alka Yagnik
|- 
| 5
| "Dum Dum Diga Diga"
| Atul Kale, Bela Shinde
|- 
| 6
| "Tera Mera Saath Rahen"
| Udit Narayan, Alka Yagnik
|- 
| 7
| "Hathon Ki Lakeeron Mein"
| Udit Narayan, Alka Yagnik
|- 
| 8
| "Haqh Jata De"
| Sukhwinder Singh, Hema Sardesai
|- 
| 9
| "Tadpati Hai Tarsati Hai"
| Udit Narayan, Alka Yagnik
|-
| 10
| "Tujh Se Bichad Ke"
| Udit Narayan
|}

==External links==
* 

 
 
 
 
 


 