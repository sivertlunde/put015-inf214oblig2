The Barons
{{Infobox film
| name           = The Barons
| image          = 
| caption        = 
| director       = Nabil Ben Yadir
| producer       = 
| writer         = Nabil Ben Yadir Laurent Brandenbourger Sébastien Fernandez
| based on       = 
| starring       =  Imhotep
| cinematography = Danny Elsen
| editing        = Damien Keyeux
| studio         = 
| distributor    = 
| released       =  
| runtime        = 111 minutes
| country        = Belgium France
| language       = French Flemish Arabic
| budget         = 
| gross          = 
}}
The Barons ( ) is a 2009 Belgian comedy film directed by Nabil Ben Yadir. It was written by Yadir, Laurent Brandenbourger and Sébastien Fernandez. It premiered on September 2, 2009, at the Festival International du Film Francophone de Namur. The film was nominated for six Magritte Awards, winning Best Supporting Actor for Jan Decleir. 

==Cast==
* Nader Boussandel as Hassan
* Mourade Zeguendi as Mounir
* Mounir Ait Hamou as Aziz
* Julien Courbey as Franck Tabla
* Jan Decleir as Lucien
* Fellag as R.G.
* Édouard Baer as Jacques
* Amelle Chahbi as Malika
* Melissa Djaouzi as Milouda
* Salah Eddine Ben Moussa as Kader
* Jean-Luc Couchard as Ozgür
* Virginie Efira as lartiste

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 


 