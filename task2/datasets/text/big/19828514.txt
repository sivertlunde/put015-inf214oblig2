Little Miss Pinkerton
{{Infobox film
| name           = Little Miss Pinkerton
| image          =
| image_size     =
| caption        =
| director       = Herbert Glazer
| producer       = MGM
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau MGM
| released       =  
| runtime        = 10 40"
| country        = United States English
| budget         =
}} short comedy film directed by Herbert Glazer.  It was the 216th Our Gang short (217th episode, 128th talking short, 129th talking episode, and 48th MGM produced episode) that was released.

==Synopsis==
The janitor of the Greenpoint department store is murdered during a robbery, while Mickey, Froggy, Buckwheat, and Janet witness the crime. The thieves take the boys hostage, but Janet escapes and heads for the police. Alas, no grownup will believe her story, so Janet enlists the aid of the other gang members to rescue the boys and capture the crooks.   

==Notes== Edward Cahn returns for the next episode, Three Smart Guys, and after that another director takes over, where under his direction, the series finally ends. This was the 3rd MGM Our Gang short to lose money during its initial release. It lost approximately $900 after print and advertising expenses were factored into the budget.

==Cast==
===The Gang===
* Janet Burston as Janet Robert Blake as Mickey
* Billy Laughlin as Froggy
* Billie Thomas as Buckwheat

===Additional cast===
* Robert Ferrero as Paper boy
* Mark Daniels as Photographer
* Robert Emmet OConnor as Sgt. OToole
* Dick Rich as Pete
* Norman Willis as Joe

==See also==
* Our Gang filmography

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 


 