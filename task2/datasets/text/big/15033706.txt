Phoebe in Wonderland
 
{{Infobox Film
| name           = Phoebe in Wonderland
| image          = Phoebe in Wonderland.jpg
| caption        = Theatrical release poster
| director       = Daniel Barnz
| producer       =
| writer         = Daniel Barnz
| starring       = Elle Fanning Felicity Huffman Patricia Clarkson Bill Pullman Campbell Scott Peter Gerety Bailee Madison
| music          = Christophe Beck
| cinematography =
| editing        =
| studio         = Red Envelope Entertainment
| distributor    = THINKFilm
| released       =  
| runtime        = 96 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
Phoebe in Wonderland is a 2008 independent film directed by Daniel Barnz.

It was screened in the Dramatic Competition at the 2008 Sundance Film Festival,    and received a limited theatrical release on March 6, 2009.

==Plot==
A 9-year-old girl, Phoebe (Elle Fanning), has apparent Tourette syndrome.  While she deals with being odd and insecure, her mother (Felicity Huffman) and father (Bill Pullman) are dealing with complexities in their relationship with each other and their challenging child. Her younger sister (Bailee Madison) feels neglected as Phoebe gains more attention.

Phoebe seeks a role in her schools play, Alice In Wonderland, directed by her schools off-beat drama teacher, Miss Dodger (Patricia Clarkson). Phoebe flourishes on stage, relaxing and feeling normal, but her impulsive speech and behavior persist off stage. Her parents hire a therapist for her, but after he proposes medication, Phoebes mother fires him. She does not want to accept that there is anything wrong with Phoebe; when the principal questions if Phoebe behaves oddly outside of the classroom, her mother denies it even though she has many times witnessed her daughters self-destructive rituals at home. When Phoebe is taken out of the play due to her classroom behavior, her dreams are shattered. Her mother, desperate to help her daughter feel normal, works with the drama teacher to bring Phoebe back on stage.

Although Phoebe is put back into the play, her challenges continue as she is driven to behavior she doesnt understand. She hurts herself jumping off the catwalk onto the stage, and the drama teacher is fired. Phoebes fellow-actors descend into chaos, but Phoebe alone clings to a sense of purpose. She urges her classmates to continue their rehearsals on their own, and they do. Her mother, who has resisted efforts to label Phoebe, tells Phoebe that she has Tourette syndrome, and Phoebe helps her classmates understand her by explaining the condition to them.

==Cast==
* Elle Fanning as Phoebe Lichten, a 9-year-old girl with Tourette syndrome. Red Queen in Phoebes fantasy scenes.
* Patricia Clarkson as Miss Dodger, the drama teacher at Phoebes school. Queen of Hearts in Phoebes fantasy scenes.
* Bailee Madison as Olivia Lichten, Phoebes younger sister. Close at first, the sisters draw apart as Phoebes symptoms progress.
* Campbell Scott as Principal Davis, the head of Phoebes school. Does not understand either Phoebe or Miss Dodger. Scott also appears as the Mad Hatter in Phoebes fantasy scenes.
* Ian Coletti as Jamie Madison, Phoebes only friend.
* Peter Gerety as Dr. Miles, Phoebes therapist. Later dismissed by Hillary. Gerety also appears as Humpty Dumpty in Phoebes fantasy scenes. Knave of Hearts in Phoebes fantasy scenes.
* Caitlin Sanchez as Monica, one of Phoebes classmates. Plays the White Queen in the play at Phoebes school.
* Maddie Corman as First Teacher/White Rabbit King of Hearts

==Production==
 
*The film was submitted for the Sundance Film Festival of 2008.
*This movie is Elle Fannings first lead role in a feature film.
*The first draft of the script was written a year before Fanning was born.
*Shown at the RiverRun International Film Festival in 2008

==Awards and nominations==
On January 14, 2010, the film was nominated for Outstanding Film – Wide Release – at the 21st GLAAD Media Awards.   

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 