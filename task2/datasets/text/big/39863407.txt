Comedian Paulus Singing
{{Infobox film
| name           = Paulus chantant
| image          =
| border         =
| alt            =
| caption        =
| director       = Georges Méliès
| producer       =
| based on       =  
| starring       = Paulus
| cinematography = 
| studio         = Star Film Company
| released       =  
| runtime        = Five films of   each   
| country        = France
| language       = Silent
| budget         =
| gross          =
}}
{{multiple image
| align     = right

| image1    = Paulus 01.jpg
| width1    =  
| alt1      = Paulus
| caption1  = Paulus

| image2    = George Melies.jpg
| width2    =  
| alt2      = Georges Méliès
| caption2  = Méliès
}}
Comedian Paulus Singing ( ) was a series of five French short silent films made in 1897 by Georges Méliès, starring the popular café-concert singer Paulus (real name Jean-Paul Habans, 1845–1908). The films were designed for a café-concert stunt in which Paulus would sing behind the screen as the films were projected, giving the illusion of a sound film.    All five films are currently presumed Lost film|lost.   

==Summary==
The English and French release titles, as well as the Star Film Company catalog numbers, for the films were as follows: 
*Comedian Paulus Singing "Derrière lOmnibus" (Paulus chantant: Derrière lOmnibus, 88)
*Comedian Paulus Singing "Coquin de Printemps" (Paulus chantant: Coquin de Printemps, 89)
*Comedian Paulus Singing "Duelliste Marsellais" (Paulus chantant: Duelliste marseillais, 90)
*Paulus chantant: Père la Victoire (no English title or catalogue number)
*Paulus chantant: En revenant dla revue (no English title or catalogue number)

==Production and release== Boulangist entertainer, approached Méliès with the concept for the stunt. Frazer 1979, p. 37  The series was filmed at Mélièss theater of illusions, the Théâtre Robert-Houdin in Paris.  In order to provide enough light for his film camera, Méliès included fifteen arc lamps and fifteen mercury-vapor lamps in his setup, making the Comedian Paulus Singing series the first known use of artificial light in a motion picture. 

Three of the films (Derrière lOmnibus, Coquin de Printemps, and Duelliste Marsellais) were sold through the catalogs of Mélièss studio, the Star Film Company, for both French and American film markets.  The other two, which featured Paulus singing hymns of praise to the controversial political figure Georges Ernest Boulanger, were filmed for the café-concert performance but not included in the Star Film catalogs,  as Méliès himself was a fervent anti-Boulangist. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 

 
 
 