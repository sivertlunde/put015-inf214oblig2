Grassroots (film)
{{Infobox film
| name           = Grassroots
| image          = Grassroots 2012.jpg
| director       = Stephen Gyllenhaal Matthew Brady Brent Stiefel Peggy Case
| writer         =  Justin Rhodes Stephen Gyllenhaal
| based on       =  
| starring       = Jason Biggs Joel David Moore Lauren Ambrose Cedric The Entertainer
| studio         = MRB Productions Votiv Films
| music          = Nick Urata
| editing        = Neil Mandelberg Sean Porter
| distributor    = Samuel Goldwyn Films
| released       =   (Seattle International Film Festival)
| country        = United States
| language       = English
}} Phil Campbell. 

Shot in Seattle, the film revolves around a grassroots campaign for Seattle City Council and explores what happens when a dedicated activist tries to realize a vision by seeking political office.

==Plot== Phil Campbell (Jason Biggs), a journalist who has just lost his job and gets roped into leading Grant Cogswell | Grant Cogswells political campaign. Grant, played by Joel David Moore, is Phils enthusiastic friend whose passion for the monorail inspires him to run for Seattle City Council.  Grant is running against Richard McIver, played by Cedric the Entertainer, and although McIver has more money and more supporters, Grants blind passion paired with Phils strategy makes Grant a contender.  

== Cast ==
 ]] Phil Campbell
* Joel David Moore as Grant Cogswell 
* Lauren Ambrose as Emily Bowen
* Cobie Smulders as Clair (a.k.a. "Monorail Girl") Tom Arnold as Tommy
* Emily Bergl as Theresa Glendon
* Todd Stashwick as Nick Ricochet
* Cedric the Entertainer as Richard McIver
* DC Pierson as Wayne Christopher McDonald as Jim Tripp

==References==
 

==External links==
 
*  
*  

 

 
 
 