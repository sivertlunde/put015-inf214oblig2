The Wild Goose on the Wing
 

 
  Chin Han, Tse Ling-ling, and Ma Yung-lin (馬永霖). It was produced by Chiung Yao (瓊瑤), who wrote the novel of the same Chinese name. It was adapted into the screenplay by Qiao Ye (喬野) and directed by Lily Liu (劉立立).

==Music==
Yan er zai lin shao (雁兒在林梢) is a 1979 album by Feng Fei-fei, released by Kolin Records (歌林唱片). It contains three songs that are heard in the film. A few songs, like "Naihe" (奈何), have been later sung by other singers, like Teresa Teng, Sylvia Chang, and Faye Wong.

;Side A
# "The Wild Goose in the Forest" (雁兒在林梢 "Yan er zai lin shao") — lyrics by Chiung Yao, composed by Xin Yi (欣逸)
#: Main theme song of this film
# "You Want to Stay Behind" (你可願留下 "Ni ke yuan liuxia") — lyrics by Chiung Yao, composed by Xin Yi
#: Also called "Ask the Wild Goose" (問雁兒 "Wen yan er"); sub-theme song of this film
# "The Grassland Is Green and Greener" (草原青又青 "Caoyuan qing you qing") — written by Wang Zhi Yuan (王智遠)
# "Dang na qin shengxiang qi" (當那琴聲響起) — lyrics by Chen Xi (晨曦), composed by Gu Yue ( )
#: Sub-theme song of the film Return of Monsoon (一片深情 Yipian shen qing)
# "Autumn Lake" (秋湖 "Qiu hu") — lyrics by Zhuang Nu (莊奴)
#: Mandarin rendition of the Japanese song

;Side B
# "When Does a Person Know" (何時才知道 "Heshi cai zhidao") — written by Jiang Rong Yi (蔣榮伊)
# "Arriving to Dusk" (又是黃昏到 "You shi huanghun dao") — lyrics by Chiung Yao, composed by Xin Yi
#: Sub-theme of this film
# "If My Heart Speaks..." (心裡想說的話 "Xinli xiang shuo dehua") — lyrics by Chen Xi, composed by Kuranosuke Hamaguchi (浜口庫之助)
#: Mandarin rendition of the Japanese song, "Mō koi na no ka" (もう恋なのか), by Akira Nishikino (錦野旦)
# "Wu se bin fen" (五色繽紛) — written by Yue Xun (岳勳)
# "Regret Leaving Old Love" (舊情依依 "Jiuqing yiyi") — written by Xiao Yan 
# "Naihe" (奈何) — written by Li Da Tao (李達濤)

==External links==
*  
*   at Wakaie.Freehostia.com

 
 
 
 


 