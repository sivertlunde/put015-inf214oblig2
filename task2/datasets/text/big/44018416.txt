Train Busters
{{Infobox film
| name           =Train Busters
| image          =Screen shot Train Busters Title.png
| image size     =150px
| caption        =Title card
| director       =Sydney Newman 
| producer       = 
| writer         =
| narrator       =
| starring       =
| music          =Lucio Agostini
| cinematography =RCAF Overseas Film Unit
| editing        =
| studio         = National Film Board of Canada 
| distributor    = Columbia Pictures of Canada
| released       =  
| runtime        = 13 minutes
| country        = Canada
| language       = English
| budget         =
| gross          =
}}
Train Busters (aka R.C.A.F. Train-Busters; its alternate French language version was known as  La guerre des airs ) is a 13-minute 1943 Canadian  , 1944. Retrieved: October 3, 2014. 

==Synopsis== ground attack fighters were extremely successful, with fighter-bombers destroying munition trains.

==Production==
  is filmed; note the lack of a spinner that characterized the Hurricane fighters in Canada.]] Ministry of Information as well as other sources, including pre-war German films.   
 Bomber and Fighter Commands, by the RCAF in 1943, including:
* de Havilland Mosquito medium bomber/fighter-bomber
* Handley Page Halifax heavy bomber
* Hawker Hurricane fighter/fighter-bomber North American Mustang I reconnaissance/fighter-bomber
* Short Stirling heavy bomber
* Supermarine Spitfire fighter, fighter-bomber
* Vickers Wellington medium bomber

==Reception==
As part of the NFBs newsreel programs, Train Busters was produced for both the military and the theatrical market. As R.C.A.F. Train-Busters, the film was issued to military bases on May 25, 1943, as Newsreel of the Week: Issue No. 8, part of the Canadian Army newsreel series.  

Each film in NFBs Canada Carries On series was shown over a six-month period as part of the shorts or newsreel segments in approximately 800 theatres across Canada. The NFB had an arrangement with Famous Players theatres to ensure that Canadians from coast to coast could see them, with further distribution by Columbia Pictures. Ellis and McLane  2005, p. 122.  

After the six-month theatrical tour ended, individual films were made available on 16&nbsp;mm to schools, libraries, churches and factories, extending the life of these films for another year or two. They were also made available to film libraries operated by university and provincial authorities. Although available from the National Film Board either online or as a DVD, Train Busters is now largely forgotten. Ohayon, Albert.   National Film Board of Canada, July 13, 2009. Retrieved: October 3, 2014. 

Historian Malek Khouri analyzed the role of the NFB wartime documentaries, with Train Busters characterized as an example of a propaganda film. "During the early years of the NFB, its creative output was largely informed by the turbulent political and social climate the world was facing. World War II, Communism, unemployment, the role of labour unions, and working conditions were all subjects featured by the NFB during the period from 1939 to 1946". In Filming Politics, Khouri described a "new-found fascination" with technological advances in winning the war, especially through the use of air power.  

==References==
===Notes===
 
===Citations===
 
===Bibliography===
 
* Ellis, Jack C. and Betsy A. McLane. New History of Documentary Film. London: Continuum International Publishing Group, 2005. ISBN 0-8264-1750-7.
* Khouri, Malek. Filming Politics: Communism and the Portrayal of the Working Class at the National Film Board of Canada, 1939-46. Calgary, Alberta, Canada: University of Calgary Press, 2007. ISBN 978-1-55238-199-1.
 

==External links==
*  
*  .

 
 
 
 
 
 
 
 
 
 
 
 