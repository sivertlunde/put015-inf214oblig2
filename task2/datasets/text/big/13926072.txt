The Void (film)
{{Infobox Film  name        = The Void  image       = caption     =  writer      =     starring    = Amanda Tapping Adrian Paul Malcolm McDowell  director    =    producer    =   Kira Domaschuk Douglas Schwartz Harold Lee Tichenor David Willson music       = Ross Vannelli distributor = Lions Gate Films released    = United States March 19, 2002 runtime     = 90 minutes gross       =  language  English
|budget      =
|}}
 2001 Cinema American direct-to-DVD thriller film which follows a scientist who has discovered that a man whos been attempting to solve an energy crisis has inadvertently created a black hole which, unless stopped, will swallow the world.

The film features Amanda Tapping as Prof. Eva Soderstrom, Adrian Paul as Prof. Steven Price, and Malcolm McDowell as Dr. Thomas Abernathy. Principal photography was completed in British Columbia, Canada.

==Synopsis==
Professor Eva Soderstrom (Tapping) discovers that Dr. Thomas Abernathy (McDowell), the man responsible for starting a scientific experiment which claimed the life of her father some years before, has plans to restart the experiment on a bigger scale. With only 24 hours, Eva must stop the experiment or the Earth will be destroyed.

==Cast==
*Amanda Tapping as Professor Eva Soderstrom
*Adrian Paul as Professor Steven Price
*Malcolm McDowell as Dr. Thomas Abernathy
*Andrew McIlroy as Oscar
*Kirsten Robek as Christine Marshall
*Michael Rivkin as Dr. Jason Lazarus

==External links==
*  

 
 
 
 
 
 
 


 