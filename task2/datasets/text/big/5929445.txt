Swamp Thing (film)
 
{{Infobox film
| name           = Swamp Thing
| image          = Swampthing.jpg
| caption        = Theatrical release poster
| image_size     = 225px
| alt            = 
| director       = Wes Craven
| producer       = Benjamin Melniker Michael E. Uslan
| screenplay     = Wes Craven
| based on       =  
| starring       = Louis Jourdan Adrienne Barbeau Ray Wise David Hess Mimi Craven Dick Durock Nicholas Worth
| music          = Harry Manfredini
| cinematography = Robbie Greenberg
| editing        = Richard Bracken
| studio         = Swampfilms
| distributor    = Embassy Pictures  (Sony Pictures Entertainment) 
| released       =  
| runtime        = 91 minutes
| country        = United States
| language       = English
| budget         = $3 million
}} science fiction character of the same name created by Len Wein and Bernie Wrightson. It tells the story of scientist Alec Holland (Ray Wise) who becomes transformed into the monster Swamp Thing (Dick Durock) through laboratory sabotage orchestrated by the evil Anton Arcane (Louis Jourdan). Later, he helps out a woman named Alice (Adrienne Barbeau) and battles the man responsible for it all, the ruthless Arcane. 

== Plot ==
  hybrid capable mutated plant creature. As the Swamp Thing, Holland battles Arcanes forces to protect Cable, and eventually takes on Arcane himself, also mutated by the Holland formula.

== Cast == Doctor Alec Holland Alice Cable
* Louis Jourdan as Anton Arcane|Dr. Anton Arcane
* Dick Durock as Swamp Thing
* David Hess as Ferret
* Nicholas Worth as Bruno Don Knight as Harry Ritter
* Al Ruban as Charlie Ben Bates as Arcane Monster
* Nannette Brown as Dr. Linda Holland
* Reggie Batts as Jude
* Mimi Craven as Arcanes Secretary (as Mimi Meyer)
* Karen Price as Karen
* Bill Erickson as Young Agent
* Dov Gottesfeld as Commando
* Tommy Madden as Little Bruno
* Garry Westcott as Louis Jordans Stand In & Stunts


== Production ==
 
  Johns Island. The character of Alice Cable is a combination of two characters from the Swamp Thing comics, Matthew Cable and Abby Arcane (who in the comics married both Cable and Holland).

== Reception ==
Swamp Thing received generally average to positive reviews from critics, with the movie review aggregator website Rotten Tomatoes giving the film a score of 64% based on 33 reviews.   Roger Ebert gave the film three out of a possible four stars, saying "Theres beauty in this movie, if you know where to look for it."   
 The Last The Hills Have Eyes (1977), Craven shows a close connection between the landscape and his characters.  The film was adapted in comic form as Swamp Thing Annual #1.

PopMatters journalist J.C. Maçek III wrote "As much fun as this film can be (and it often is), its equally often difficult to ignore that Swamp Thing ultimately is, at core, a rubber-suit monster movie."   

== Home media and controversy == MGM released Blockbuster Video store for her children and reported this discrepancy.  MGM recalled the disc and reissued it in August of 2005, with the US theatrical cut as originally intended. 

Swamp Thing was released in a   anamorphic widescreen format, along with bonus content including interviews with Adrienne Barbeau, Len Wein, and Reggie Batts, as well as commentary tracks with Wes Craven and makeup artist Bill Munn.  

== Legacy ==
=== Sequel ===
A low-budget sequel entitled The Return of Swamp Thing was released in 1989. 

=== Reboot ===
In 2009, Joel Silver announced plans to produce a reboot of the Swamp Thing film franchise from a story written by Akiva Goldsman.  In April 2010, Vincenzo Natali was confirmed to direct,  but on May 12, 2010, Vincenzo Natali decided to delay the Swamp Thing reboot to pursue other projects. 

== References ==
 

== External links ==
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 