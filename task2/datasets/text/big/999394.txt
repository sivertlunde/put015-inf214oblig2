Pirates of the Caribbean: Dead Man's Chest
 
 
{{Infobox film
| name           = Pirates of the Caribbean:  Dead Mans Chest
| image          = Pirates of the caribbean 2 poster b.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Gore Verbinski
| producer       = Jerry Bruckheimer
| writer         = {{Plainlist| Ted Elliott
* Terry Rossio}}
| based on       = {{Plainlist| Pirates of the Caribbean
* List of Pirates of the Caribbean characters|Characters:
* Ted Elliott
* Terry Rossio
* Stuart Beattie
* Jay Wolpert}}
| starring       = {{Plainlist|
* Johnny Depp
* Orlando Bloom
* Keira Knightley
* Stellan Skarsgård
* Bill Nighy
* Jack Davenport
* Kevin R. McNally
* Jonathan Pryce}}
| music          = Hans Zimmer
| cinematography = Dariusz Wolski
| editing        = {{Plainlist|
* Stephen E. Rivkin Craig Wood}}
| studio         = {{Plainlist|
* Walt Disney Pictures
* Jerry Bruckheimer Films}} Buena Vista Pictures
| released       =  
| runtime        = 150 minutes  
| country        = United States
| language       = English
| budget         = $225 million   
| gross          = $1.066 billion 
}} Ted Elliott Lord Cutler Captain Jack Davy Jones (Bill Nighy) is due.

Two   that would span both films. Filming took place from February to September 2005 in  .
 11th highest-grossing Walt Disney The Avengers Best Art Sound Editing, Sound Mixing, and won the Academy Award for Visual Effects.

==Plot==
  Letters of Governor Swann, Elizabeths father, tries to flee Port Royal with her, but he is arrested by Becketts aide List of Pirates of the Caribbean characters#Mercer|Mercer. Beckett reluctantly allows Elizabeth to go after Will with the Letters of Marque, which she does by stowing away on a Scottish merchant vessel, the Edinburgh Trader.
 Davy Jones Flying Dutchman — and gives him a jar of dirt as a defense against Jones, who cannot step on land but once every 10 years.
 Tortuga to recruit new crewmates, meeting Elizabeth and Norrington, who has become a drunkard. Both of them join the Black Pearl. Jack realizes that if Beckett gains Jones’ heart, he can rule the seas. Norrington eavesdrops, planning to get the heart in order to regain his life and honour. Will hitches a ride on the Edinburgh Trader, but Jones discovers the theft of the key and summons the kraken to destroy the Scottish ship, realising that Jack is after his chest.
 Isla Cruces, Davy Jones Locker along with the Pearl.
 Captain Barbossa.

==Cast==
  Captain Jack Sparrow: Captain of the Black Pearl. He is hunted by the Kraken because of his unpaid blood debt to Davy Jones. He is also searching for the Dead Mans Chest to free himself from Jones servitude.
* Orlando Bloom as Will Turner: A blacksmith-turned-pirate who is trying to retrieve Jacks compass for Lord Beckett in order to secure freedom for himself and Elizabeth.
* Keira Knightley as Elizabeth Swann: Governor Swanns daughter and Wills fiancée, who is arrested on her wedding day for helping Captain Jack Sparrow escape. Escaping jail with help from her father, she meets up with Jack in Tortuga and joins his crew to search for both Will and the chest.
*  . Davy Jones was once a human being who was unable to bear the pain of losing his true love. He carved out his heart and put it into the Dead Mans Chest, then buried it in a secret location. He has become a bizarre creature – part octopus, part lobster, part man. Jones collects the souls of dead or dying sailors to serve aboard his ship for one hundred years.
*   in the pursuit of Jack Sparrow and his crew. Fallen on hard times and into alcoholism, he joins the Black Pearls crew and seeks to regain his honor and Naval career.
*  s crew. When they went to give mutiny to Jack, he disagreed. Thrown overboard after refusing to take part in the mutiny against Jack led by Barbossa, he spent years bound to a cannon beneath the crushing ocean, though before this, he sent one piece of the Aztec Gold to his son, Will, saying they all deserved to be cursed. Found by Davy Jones, he swore to servitude aboard the Flying Dutchman crew and escaped death. This story was told by Pintel to Will and Jacks crew in the first movie.
*  .
*  . He adores his daughter but puts little faith in Will – not considering him the best match for Elizabeth.
*  , he was imprisoned after the Aztec curse was broken, but escaped to rejoin Jack Sparrows Black Pearl crew.
* Mackenzie Crook as Pintel and Ragetti|Ragetti: Pintels inseparable crewmate. He has a wooden eye, and despite being illiterate, has begun "reading" the Bible, with the excuse that "you get credit for trying."
*  , he travels to Port Royal to capture and recruit Jack Sparrow as a privateer. What he really desires is Davy Jones heart, with which he can rule the seas with Jones commanded servitude.
*   priestess whom Jack Sparrow bartered with for his magic compass. She explains the legend of Davy Jones, in addition to owning a similar locket to his.
* David Bailie as Cotton: A sailor on the Black Pearl who lost his tongue and trained his parrot Tiki Macaw to talk for him.
* Martin Klebba as Marty: A short sailor on the "Black Pearl". David Schofield as Mercer: Lord Becketts right-hand-man. Captain Hector Barbossa: The former captain of the Black Pearl is resurrected during this film; however, he does not appear until the final scene. Having met his demise in the previous installment, Barbossa is resurrected by Tia Dalma and agrees to rescue Jack Sparrow in order to save the Black Pearl. For this role, Rush was uncredited to keep his return a surprise and the DVD commentary said that not even the cast of the movie knew that Rush confirmed desire to reprise his character more often and that the expressions on the characters faces when seeing him for the first time in three years were real.
*Peter Donald Badalamenti II as Penrod and Davy Jones crew 
 

==Production==
===Development=== shot back-to-back, Ted Elliott Indiana Jones James Bond Davy Jones, Flying Dutchman East India Trading Company, who for them represented a counterpoint to the themes of personal freedom represented by pirates. 

Planning began in June 2004, and production was much larger than The Curse of the Black Pearl, which was only shot on location in Saint Vincent (island)|St. Vincent.  This time, the sequels would require fully working ships, with a working Black Pearl built over the body of an oil tanker in Bayou La Batre, Alabama. By November, the script was still unfinished as the writers did not want director Gore Verbinski and producer Jerry Bruckheimer to compromise what they had written, so Verbinski worked with James Byrkit to storyboard major sequences without need of a script, while Elliott and Rossio wrote a "preparatory" script for the crew to use before they finished the script they were happy with. By January 2005, with rising costs and no script, Disney threatened to cancel the film, but changed their minds. The writers would accompany the crew on location, feeling that the lateness of their rewrites would improve the spontaneity of the casts performances. 

===Filming===
 .]] Walt Disney replica of HMS Bounty Mutiny on the Bounty.  

On April 18, 2005,  the crew began shooting at Dominica, a location Verbinski had selected as he felt it fitted the sense of remoteness he was looking for.  However, this was also a problem; the Dominican government were completely unprepared for the scale of a Hollywood production, as while the 500-strong crew occupying around 90% of the roads on the island they had trouble moving around on the underdeveloped surfaces. The weather also alternated between torrential rainstorms and hot temperatures, the latter of which was made worse for the cast who had to wear period clothing. At Dominica, the sequences involving the Pelegosto and the forest segment of the battle on Isla Cruces were shot. Verbinski preferred to use practical props for the giant wheel and bone cage sequences, feeling long close-up shots would help further suspend the audiences disbelief.  Dominica was also used for Tia Dalmas shack. Filming on the island concluded on May 26, 2005. 

The crew moved to a small island called White Cay in the Bahamas for the beginning and end of the Isla Cruces battle,  before production took a break until August, where in Los Angeles the interiors of the Flying Dutchman were shot.  On September 18, 2005,  the crew moved to Grand Bahama Island to shoot ship exteriors, including the working Black Pearl and Flying Dutchman. Filming there was a tumultuous period, starting with the fact that the tank had not actually been finished. The hurricane season caused many pauses in shooting, and Hurricane Wilma damaged many of the accessways and pumps, though no one was hurt nor were any of the ships destroyed.  Filming completed on September 10, 2005. 

===Special effects===
 
The  , while veterans had full-blown undersea creature attributes. Verbinski wanted to keep them realistic, rejecting a character with a turtle shell, and the animators watched various David Attenborough documentaries to study the movement of sea anemones and mussels.  All of the crew are Computer animation|computer-generated, with the exception of Stellan Skarsgård, who played Bootstrap Bill Turner|"Bootstrap" Bill Turner. Initially his prosthetics would be augmented with CGI but that was abandoned.  Skarsgård spent four hours in the make-up chair and was dubbed "Bouillabaisse" on set. 
 Davy Jones had originally been designed with chin growths, before the designers made the move to full-blown tentacles;  the skin of the character incorporates the texture of a coffee-stained Styrofoam cup among other elements. To portray Jones on set, Bill Nighy wore a motion capture tracksuit that meant the animators at Industrial Light & Magic did not have to reshoot the scene in the studio without him or on the motion capture stage. Nighy wore make-up around his eyes and mouth to splice into the computer-generated shots, but the images of his eyes and mouth were not used. Nighy only wore a prosthetic once, with blue-colored tentacles for when Will Turner (Orlando Bloom) steals the key to the Dead Mans Chest from under his "beard" as he sleeps. To create the CG version of the character, the model was closely based on a full-body scan of Nighy, with Jones reflecting his high cheekbones. Animators studied every frame of Nighys performance: the actor himself had blessed them by making his performance more quirky than expected, providing endless fun for them. His performance also meant new controls had to be stored. Finally, Jones tentacles are mostly a simulation, though at times they were hand-animated when they act as limbs for the character. 

The   stage. The scene where the Kraken spits at   thrown at Johnny Depp. 

==Release==
 
Pirates of the Caribbean: Dead Mans Chest premiered at Disneyland in California on June 24, 2006. It was the first Disney film to use the new computer-generated Walt Disney Pictures production logo, which took a year for the studio to design.    Weta Digital was responsible for the logos final animated rendering and Mark Mancina was hired to score a new composition of "When You Wish Upon A Star". 

===Home media===
The film became available on DVD on December 5, 2006 for Region 1 and sold 9,498,304 units in its first week of sales (equivalent to $174,039,324). In total it sold 16,694,937 units, earning $320,871,909. It was the best-selling DVD of 2006 in terms of units sold and second in terms of sales revenue behind  . 
 gag reel, with the double-disc featuring a video of the film premiere and a number of documentaries, including a full-length documentary entitled "According to the Plan" and eight featurettes. The film was released on Blu-ray Disc on May 22, 2007.  The film had its UK Television premiere on Boxing Day 2008 on BBC One at 20:30. It was seen by 6.8 million viewers according to overnight figures. 

==Reception==
===Critical reception=== Flying Dutchman and its cliffhanger.  The completely Computer animation|computer-generated Davy Jones turned out to be so realistic that some reviewers mistakenly identified Nighy as wearing prosthetic makeup.  

The New York Times gave a positive review praising director Gore Verbinski saying "You put down your money – still less than $10 in most cities – and in return you get two and a half hours of spirited swashbuckling, and Gore Verbinski has an appropriate sense of mischief, as a well as a gift, nearly equaling those of Peter Jackson and Steven Spielberg, for integrating CGI seamlessly into his cinematic compositions."  Empire magazine gave the film 3 stars saying "Depp is once again an unmitigated joy as Captain Sparrow, delivering another eye-darting, word-slurring turn with some wonderful slapstick flourishes. Indeed, Rossio and Elliot smartly exploit these in some wonderful action set-pieces." "We don’t get the predictable ‘all friends together on the same quest’ structure, and there’s a surfeit of surprises, crosses and double-crosses and cheeky character beats which stay true to the original’s anti-heroic sense of fun. After all, Jack Sparrow is a pirate, a bad guy in a hero’s hat, a man driven by self-gain over concern for the greater good, who will run away from a fight and cheat his ‘friends’ without a second’s thought." 
Lord McLovin of MovieWeb said "The second tale of Captain Jack Sparrow is another epic adventure!" 

On the other hand, critic Paul Arendt of the BBC negatively compared it to The Matrix Reloaded, as a complex film that merely led onto the next film.  Richard George felt a "better construct of Dead Mans Chest and At Worlds End would have been to take 90 minutes of Chest, mix it with all of End and then cut that film in two."  Alex Billington felt the third film "almost makes the second film in the series obsolete or dulls it down enough that we can accept it in our trilogy DVD collections without ever watching it." 

===Box office=== eleventh highest-grossing second instalment in a franchise.    It is the third film in history to reach the $1-billion-mark worldwide, and it reached the mark in record time (63 days),  a record that has since been surpassed by many films, of which the first was Avatar (2009 film)|Avatar (in January 2010). 
 The Dark eleventh highest-grossing film, although, adjusted for inflation, the film ranks List of highest-grossing films in Canada and the United States#Adjusted for ticket-price inflation|forty-sixth. It is also the highest-grossing 2006 film,  the highest grossing Pirates of the Caribbean film,  and the second highest-grossing Disney film. 

Outside North America, it is the twenty-first highest-grossing film,  the third highest-grossing Pirates film, the eighth highest-grossing Disney film  and the highest-grossing film of 2006.  It set opening-weekend records in Russia and the CIS, Ukraine, Finland, Malaysia, Singapore,  Greece  and Italy.   It was on top of the box office outside North America for 9 consecutive weekends and 10 in total.  It was the highest-grossing film of 2006 in Australia,  Bulgaria,  Germany,  Japan,  the Netherlands,  New Zealand,  Spain,  Sweden  and Thailand. 

===Accolades===
  Charles Gibson, Allen Hall Best Visual Best Art Sound Editing, Sound Mixing.   

The film also won a BAFTA and Satellite award for Best Visual Effects,  and six awards from the Visual Effects Society. 

Other awards won by the film include  , Choice Drama/Action Adventure Movie, Actor for Johnny Depp at the 2006 Teen Choice Awards; Favorite Movie, Movie Drama, Male Actor for Depp and On-Screen Couple for Depp and Keira Knightley at the 33rd Peoples Choice Awards; Best Movie and Performance for Depp at the 2007 MTV Movie Awards and Best Special Effects at the Saturn Awards, and Favorite Movie at the 2007 Kids Choice Awards. 

==Video game==
 
A video game adaptation of the film was developed by Griptonite Games and Amaze Entertainment and released by Buena Vista Games in June–August 2006 for the PlayStation Portable, Nintendo DS and Game Boy Advance.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   at the IMSDb 
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 