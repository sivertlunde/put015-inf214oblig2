Tora-san Makes Excuses
{{Infobox film
| name = Tora-san Makes Excuses
| image = Tora-San Makes Excuses.jpg
| caption = Theatrical poster
| director = Yoji Yamada
| producer = Kiyoshi Shimizu Hiro Fukazawa
| writer = Yoji Yamada Yoshitaka Asama
| starring = Kiyoshi Atsumi Kumiko Goto
| music = Naozumi Yamamoto
| cinematography = Tetsuo Takaba Mitsumi Hanada
| editing = Iwao Ishii
| distributor = Shochiku
| released =  
| runtime = 101 minutes
| country = Japan
| language = Japanese
| budget = 
| gross = 
}}

  is a 1992 Japanese comedy film directed by Yoji Yamada. It stars Kiyoshi Atsumi as Torajirō Kuruma (Tora-san), and Kumiko Goto as his love interest or "Madonna".  Tora-San Makes Excuses is the forty-fifth entry in the popular, long-running Otoko wa Tsurai yo series.

==Cast==
* Kiyoshi Atsumi as Torajirō 
* Chieko Baisho as Sakura
* Jun Fubuki as Choko
* Kumiko Goto as Isumi Oikawa
* Hidetaka Yoshioka as Mitsuo Suwa
* Masami Shimojō as Kuruma Tatsuzō
* Chieko Misaki as Tsune Kuruma (Torajirōs aunt)
* Gin Maeda as Hiroshi Suwa
* Hisao Dazai as Boss (Umetarō Katsura)
* Gajirō Satō as Genkō
* Chishū Ryū as Gozen-sama
* Mari Natsuki as Ayako Oikawa

==Critical appraisal==
The German-language site molodezhnaja gives Tora-san Makes Excuses three and a half out of five stars.   

==Availability==
Tora-san Makes Excuses was released theatrically on December 26, 1992.  In Japan, the film was released on videotape in 1993 and 1996, and in DVD format in 2002 and 2008. 

==References==
 

==Bibliography==

===English===
*  
*  

===Japanese===
*  
*  
*  
*  

==External links==
*   at www.tora-san.jp (Official site)

 
 

 
 
 
 
 
 
 
 
 

 