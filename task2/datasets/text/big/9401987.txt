Twist Around the Clock
{{Infobox Film |
| name     = Twist Around the Clock|
| image         = File:Twist_around_the_clock_poster.jpg
| caption       = Theatrical Poster.
| director       = Oscar Rudolph |
| writer         = Robert E. Kent |
| starring       = Chubby Checker |
| producer       = Sam Katzman |
| music          = Freddy Karger |
| cinematography = Gordon Avil |
| editing        = Jerome Thoms |
| production_company = Four-Leaf Productions |
| distributor    = Columbia Pictures |
| released       =   |
| runtime        = 86 minutes |
| language       = English |
}}
 Rock Around the Clock. Like Rock Around the Clock, which was followed by a sequel titled, Dont Knock the Rock, this film was followed by a sequel titled, Dont Knock the Twist.

==Synopsis==
A struggling manager visits a hayseed town and discovers a new dance craze, Twist (dance)|"the Twist" and hopes to turn it into an overnight nationwide sensation.


"Twist Around the Clock only cost $250,000 to make, but in less than six months it grossed six million - so of course Im gonna make more Twist movies !"  Sam Katzman

==Singers/groups featured==
# Chubby Checker Dion
# The Marcels
# Vicki Spencer
# Clay Cole
# The Jay Birds

==External links==
*  

 
 


 