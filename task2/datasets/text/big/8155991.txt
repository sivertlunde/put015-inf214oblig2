One Too Many
 

{{Infobox film
| name           = One Too Many
| image          = onetoomanydvd.jpg
| caption        = Is There One In Your House?
| director       = Erle C. Kenton
| producer       = Kroger Babb
| writer         = Kroger Babb (story) Malcolm Stuart Boylan (screenplay)
| narrator       = Richard Travis Rhys Williams William Tracy
| music          =
| cinematography =
| editing        =
| distributor    = Hallmark Productions
| released       =  
| runtime        =
| country        = United States English
| budget         =
}}
One Too Many, also known as Killer With a Label, Mixed-Up Women, and The Important Story of Alcoholism, was an exploitation film produced by Kroger Babb in 1950 in film|1950.  It told the story of Helen Mason (Ruth Warrick), who is slowly revealed during the course of the film to be an alcoholism|alcoholic, destroying her career as a concert pianist and her family in the process.

The film contained musical numbers as filler (media)|filler, and was presented as an exploitation film by Babb.  The subject matter, however, was not easily exploitable and the film failed to do well.  Regardless, the film was redistributed many times over the years, and most recently released on DVD by Retroflicks.

==External links and references==
*  
*  

 

 
 
 
 
 
 
 


 
 