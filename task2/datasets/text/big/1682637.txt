Teacher's Pet (1930 film)
{{Infobox film
| name           = Teachers Pet
| image          =
| caption        =
| director       = Robert F. McGowan
| producer       = Robert F. McGowan Hal Roach
| writer         = Robert F. McGowan H. M. Walker
| narrator       = Matthew Beard Buddy McDonald
| music          = Leroy Shield Marvin Hatley
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| distributor    = Metro-Goldwyn-Mayer
| released       =  
| runtime        = 20 minutes 
| country        = United States
| language       = English
| budget         =
}}
Teachers Pet is a 1930 two-reel comedy short; part of the Our Gang (Little Rascals) series. It was produced by Hal Roach, directed by Robert F. McGowan, and originally released to theatres by Metro-Goldwyn-Mayer on October 11, 1930.    It was the 101st  Our Gang short to be released.

==Plot== Hercules — come in and tell Miss Crabtree that they need to be excused to go home ..."and then, were all goin swimmin!"

Jackie Cooper|Jack, the mastermind of the operation, asks Wheezer if he remembers what to say. Wheezer says "Mama wants Jack home right away; shes gonna shoot Papa!"  Jackie says "No, thats too strong, just say "important business."

However, the plan falls apart when Jackie takes a ride to school from a beautiful young lady (June Marlowe) with a shiny Roadster (automobile)|roadster. Unbeknownst to Jackie, his benefactor is actually Miss Crabtree, and he tells her everything about the plan to harass the new teacher. She drops Jack off a mile from the school, and Jack tells her "yknow, youre almost as pretty as Miss McGillicuddy...all except in your nose."
 Gordon Douglas) bring in a wealth of cake and ice cream as a first-day treat for the class. After being shooed outside, the kids all turn on Jack, with an angry Farina remarking "yeah, my pal," making a throat-cutting gesture to accent his anger. Farina, Chubby, and Buddy decide to go back inside, apologize, and hope that they can "get in on that ice cream". Jack decides that he "cant ever go back; Im too ashamed", and sits under a tree in the schoolyard and begins sobbing. After accepting the other three pranksters apologies and giving them their treats, Miss Crabtree goes outside looking for Jack, and, upon finding him, quietly presents him with a plate of cake and a bowl of ice cream, showing that she forgives him. Amidst tears, Jack looks up at Miss Crabtree and tells her "gee, youre pretty, Miss Crabtree. Youre even prettier than Miss McGillicuddy," and solemnly tries to eat his dessert.

==Notability== Matthew Beard, here identified as "Hercules", and later called "Stymie".

The short was also the first in the series to use the now-popular Our Gang theme song, "Good Old Days", composed by Leroy Shield and featuring a notable saxophone solo. Teachers Pet was also the first of three Our Gang films in what is now considered the "Jackie/Miss Crabtree trilogy", which were the first sound Our Gang films to successfully balance comedy with drama and emotion. All three of the films (the other two are Schools Out (1930 film)|Schools Out and Love Business) are opened not with text title cards, but with live-action title cards recited to the audience by two twin girls, Betty Jean and Beverly McCrane:

:(in unison) "Dear Ladies and Gentlemen; Hal Roach presents, for your entertainment and approval, His Rascals, in their latest Our Gang comedy entitled Teachers Pet.
:(first twin) Direction by Robert McGowan.... photography by Art Lloyd... edited by Richard Currier... recording by Elmer Raguse... dialogue by H.M. Walker. We thank you!

==Release and legacy== Paramount feature The Champ; by the spring of 1931, Hal Roach had sold his contract to MGM so that Cooper could make features full-time. Miss Crabtree would go on to appear in five more Our Gang shorts, and remains today not only the most popular adult character from Our Gang, but also one of the most popular fictional schoolteachers of all time.
 Academy Award for Short Subjects (One-Reel).

The television syndication print distributed by King World was edited in 1971 to remove scenes considered in bad taste. These scenes were reinstated in the early 1990s.

==Cast==
===The Gang===
* Jackie Cooper as Jackie Matthew Beard as Hercules
* Norman Chaney as Chubby
* Dorothy DeBorba as Dorothy
* Allen Hoskins as Farina
* Bobby Hutchins as Wheezer
* Mary Ann Jackson as Mary Ann
* Buddy McDonald as Buddy

===Additional cast===
* Artye Folz as Third pea-shooter victim
* Donald Haines as Don
* June Marlowe as Miss June Crabtree
* Baldwin Cooke as First caterer
* William Courtright as Old man talking with Miss Crabtree (in his last film role) Gordon Douglas as Second caterer
* Beverly and Betty Mae Crane as the Title readers
* Mildred Kornman as Classroom extra
* Bobby Mallon as Classroom extra
* Billy Seay as Classroom extra
* Barbara Roach as Classroom extra

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
*  at TheLuckyCorner.com

 
 
 
 
 
 
 
 
 
 