Lillian Russell (film)
{{Infobox film
| name           = Lillian Russell
| image          = Lillianrussell1940.jpg
| alt            =
| image_size     =
| caption        = Theatrical release poster
| director       = Irving Cummings
| producer       = Darryl F. Zanuck
| writer         = William Anthony McGuire Edward Arnold
| music          =
| cinematography =
| editing        =
| distributor    = 20th Century Fox
| released       =  
| runtime        = 127 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}} singer and Edward Arnold as Diamond Jim Brady.
 Richard Day and Joseph C. Wright were nominated for an Academy Award for Best Art Direction, Black-and-White.    

==Plot==
Helen Leonard (Faye) has a beautiful voice. As she grows up, she trains to become an opera singer. Her instructor, however, informs her that her voice is pleasing, but not suitable for grand opera. Returning home one day, she and her grandmother (Westley) are saved by a handsome young man, newspaperman Alexander Moore (Fonda). Meanwhile, Helens mother, Cynthia (Peterson), has political aspirations, but only receives a handful of votes for mayor.

While singing one evening, Helen is overheard by vaudeville impresario Tony Pastor (Carrillo), who hires her to sing at his theater. She is given a new name, Lillian Russell, and quickly rises to fame as the toast of New York. As the years pass, Lillian becomes one of the most revered stars in America. She has many suitors, including financier Diamond Jim Brady (Arnold), Jesse Lewisohn (William), and composer Edward Solomon (Ameche). She eventually marries Edward and they move to London, where Gilbert and Sullivan are writing an operetta especially for her.

Alexander Moore returns and makes a contract with Lillian to write stories about her rise to fame. But tragedy soon strikes when Edward dies one evening while composing a song for her. Lillian cancels the interviews and makes an appearance in the show, singing the song her husband composed for her, "Blue Lovebird."

Lillian returns to America and is, by this time, the greatest stage attraction of the century. Alexander comes to see Lillian after a new show and the two are happily reunited.

==Cast==
* Alice Faye as Helen Leonard / Lillian Russell
* Don Ameche as Edward Solomon Alexander Moore Edward Arnold as Diamond Jim Brady
* Warren William as The Famous J.L. - Jesse Lewisohn
* Leo Carrillo as Tony Pastor
* Helen Westley as Grandma Leonard
* Dorothy Peterson as Cynthia Leonard
* Ernest Truex as Charles Leonard William S. Gilbert
* Claud Allister as Arthur Sullivan (as Claude Allister) Weber and Fields as Themselves (their comedy duo)
* Eddie Foy, Jr. as Eddie Foy

==Songs== After the Ball". Several new songs were also written for the film, including "Adored One" and most notably, "Blue Lovebird", composed by Gus Kahn and Bronislau Kaper.

==See also==
* Diamond Jim, a 1935 film starring Edward Arnold, again as Jim Brady

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 