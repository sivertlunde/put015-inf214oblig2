Bahurani
 
{{Infobox film
| name = Bahurani
| image =Bahurani.jpg
| caption = DVD cover
| director = T. Prakash Rao
| producer =
| writer = Inder Raj Anand
| starring =Guru Dutt Mala Sinha Feroz Khan
| music = C. Ramchandra
| cinematography =
| editing =
| distributor =Meena Movies
| released =
| runtime = 132 minutes
| country = India
| language = Hindi
| budget =
| gross =
}} Indian Hindi film directed by T. Prakash Rao. The film stars Guru Dutt, Mala Sinha and Feroz Khan in lead roles.

==Plot==
The Zamindar (Nazir Hussain) has two sons - Raghu, by his first wife, and Vikram, by his second wife. Raghu (Guru Dutt) is a simple-minded and innocent young man. Vikram (Feroz Khan) is cruel, domineering, selfish and greedy, and he maltreats everyone, from servants to his own brother Raghu. Vikrams vicious mother (Lalita Pawar) does the same.

After Vikram has a feud with a tough and smart village girl named Padma (Mala Sinha), who is the first person to ever confront him, Zamindar gets an idea of marrying Vikram and Padma. Vikram refuses, and after a series of incidents, Padma ends up getting married to Raghu instead. When she understands how her husband has been treated over the years, she vows to set things right, and in the process falls in love with him. Inspired by her love, fearlessness and no-nonsense attitude, Raghu begins to find the courage to resist his oppressors.

==Cast==
*Guru Dutt ... Raghu
*Mala Sinha ... Padma
*Feroz Khan ... Vikram (Raghus stepbrother)
*Shyama
*Lalita Pawar ... Rajeshwari (Raghus stepmother) Agha
*Badri Prasad
*Mukri
*Nasir Huussain ... Zamindar

==Awards==
*Nominated, Filmfare Best Actress Award - Mala Sinha

==Music==
The soundtrack of the film contains two songs composed by C. Ramchandra with lyrics authored by Sahir Ludhianvi

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Song !! Singer(s)
|-
|1.
| "Balma Anari Man Bhaye"
| Lata Mangeshkar
|-
|1.
| "Umr Hui Tumse Mile Phir Bhi"
| Hemant Kumar, Lata Mangeshkar
|}

==External links==
* 

 
 
 