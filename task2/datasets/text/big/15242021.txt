Floating Clouds
{{Infobox film
| name           = Floating Clouds
| image          = Ukigumo poster 2.jpg
| caption        = Japanese film poster
| director       = Mikio Naruse Kihachi Okamoto (assistant director)
| producer       = Sanezumi Fujimoto
| based on       =  
| screenplay      = Yōko Mizuki Masayuki Mori Mariko Okada
| music          = Ichirō Saitō
| cinematography = Masao Tamai
| editing        = Hideshi Ohi
| studio =  Toho
| distributor    = 
| released       =  
| runtime        = 123 minutes
| country        = Japan
| awards         = 
| language       = Japanese
| budget         = 
}} Masayuki Mori and Hideko Takamine.]] Japanese film Fumiko Hayashi, written just before she died in 1951. The novel is set after World War II and contains the common post-war theme of wandering; the female main character struggles to find where she belongs in post-war Japan, and ends up floating endlessly until her death at the novels end.

The film is Naruses most popular film in Japan,  and was in 1995 named the third best film in Japanese film history.

==Story==
The film follows Yukiko Koda, a woman who has just returned to Japan from French Indochina, where she has been working as a secretary. Yukiko seeks out Kengo, with whom she had an affair in Da Lat during the war. They renew their affair, but Kengo tells Yukiko he is unable to leave his wife. Brightly lit flashback of their time in Indochina contrasts with the sombre tones of the films present.

==Cast==
 
* Hideko Takamine as Yukiko Koda Masayuki Mori as Kengo Tomioka
* Mariko Okada as Sei Mukai
* Chieko Nakakita as Kuniko Tomioka
* Daisuke Katō as Seikichi Mukai
* Isao Yamagata as Sugio Iba
* Mayuri Mokusho as Nomiya no musume
* Noriko Sengoku as Yakushima no okaasen
* Fuyuki Murakami as Futsuin no shikensho-chou
* Heihachiro Okawa as Isha
* Nobuo Kaneko as Futsuin no shoin-Suitou
* Roy James as American soldier
* Akira Tani as Shinja
 

== Awards ==
* 1956 - Blue Ribbon Awards for best film (Mikio Naruse) Masayuki Mori), for best actress (Hideko Takamine), for best director (Mikio Naruse) and for best film (Mikio Naruse)
* 1956 - Mainichi Film Concours for best actress (Hideko Takamine), for best director (Mikio Naruse), for best film (Mikio Naruse) and for best sound recording (Hisashi Shimonaga)
* 1995 - Named the third best film in Japanese film history, in Kinema Junpos 1995 "All Time Best 100" list.

==Impact==
Adrian Martin, editor of on-line film journal Rouge (film journal)|Rouge  has remarked upon Naruses cinema of walking.  Bertrand Tavernier, speaking of Naruses Sound of the Mountain  described how the director minutely describes each journey and that " such comings and goings represent uncertain yet reassuring transitions: they are a way of taking stock, of defining a feeling".  So in Floating Clouds the walks down streets " are journeys of the everyday, where time is measured out of footfalls, - and where even the most melodramatic blow or the most ecstatic moment of pleasure cannot truly take the characters out of the unromantic, unsentimental forward progression of their existences."

The Australian scholar Freda Freiberg has remarked on the terrain of the film : " The frustrations and moroseness of the lovers in Floating Clouds are directly linked to and embedded in the depressed and demoralised social and economic conditions of early post-war Japan; the bombed-out cities, the shortage of food and housing, the ignominy of national defeat and foreign occupation, the economic temptation of prostitution with American military personnel."  

== References ==
 

==External links==
*  
*  

 
{{Navboxes title = Awards list =
 
 
 
}}

 
 
 
 
 
 
 
 
 