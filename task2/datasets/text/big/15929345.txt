Mahanagaramlo Mayagadu
{{Infobox film
| name           = Mahanagaramlo Mayagadu
| image          = Mahanagaramlo Mayagadu.jpg
| caption        =
| director       = Vijaya Bapineedu
| producer       =
| writer         =
| starring       = Chiranjeevi Vijayashanti Rao Gopal Rao Allu Ramalingaiah
| music          = Chellapilla Satyam
| cinematography =
| editing        =
| distributor    =
| released       = June 28, 1984
| runtime        =
| country        = India Telugu
| budget         =
}} Telugu film directed by Vijaya Bapineedu. The film stars Chiranjeevi and Vijayashanti in lead roles as well as Rao Gopal Rao and Allu Rama Lingaiah.

==Plot==
Chiranjeevi plays the role of Raja, whose main aims are to make money by hook or by crook, and get his sister married. His father commits suicide after being accused of theft and his mother dies of disease. Unable to bear poverty in his village, he leaves his sister at his friend’s place and ends up in city, using the name Mayagadu, who makes money by every possible means. He ends up in a police colony, in Sub-inspector Allu’s house as a preacher. Vijayasanthi, who
knows his real identity, tries in every possible way to get him out of that place and finally succeeds.
But later when she learns Raja’s bitter past, she decides to help him.
Giribabu, who is son-in-law of SP, hates his wife and has an affair with Chanchala (Jayamalini), who is dancer at a club. This club is managed by Jyothilaxmi, who is Nutan Prasad’s wife but lives with club’s owner. Giribabu plans to kill his wife but instead unknowingly ties up his lover in a bag and throws her in a river. Raja’s brother-in-law notices this and saves her, but is framed in her murder case and jailed. Later Raja realizes his mistake and frees him from police custody. On his way to find out the real culprit, he realizes that Chanchala is alive and that the club owners killed another dancer and framed him to save Giribabu.
Meanwhile Raja’s sister arrives in the city in search of her husband and brother, and stays at SP’s house. Raja deceives SP and brings her out and with the help of Nutan Prasad, who is the nephew of SP, unfolds the details of the case and frees his brother-in-law.

==Cast==
Chiranjeevi  		
Vijayshanti  		
Allu Ramalingaiah  		
Raogopalrao  		
Sangeetha  		
Nutan Prasad  		
Giribabu  		
Nirmalamma  		
Jayamalini  		
Mouli

==Sound Track==
Hari Katha  
Mahanagaramlo Mayagadu  
Vuduku Vuduku  
Yedhava Vatti Yedhava

==External links==
* 

 
 
 


 