Looking for a True Fiancee
{{Infobox film
| name           = Looking for a True Fiancee
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        = 
| director       = Yuki Iwata 
| producer       = 
| writer         = Yuki Iwata
| screenplay     = 
| story          = 
| based on       =  
| narrator       =  Yoko Maki Chizuru Ikewaki
| music          = Mino Kabasawa
| cinematography = Shuhei Onaga
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 108 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2011 Japanese film directed by Yuki Iwata and based on a novel by Takami Itō. 

==Cast==
* Takayuki Yamada as Teruhiko Katayama
* Manami Konishi as Chie Sumitomo Yoko Maki as Shiozaki Megumi
* Chizuru Ikewaki as Wakako Suzuki
* Fumi Nikaido

==Reception==
Russell Edwards of Variety (magazine)|Variety criticized the film, saying: "Iwata lacks the timing needed to make this romantic comedy work." 

Mark Schilling of The Japan Times said, "Iwata evidences a surreal visual flair in the early scenes especially, as in the dreamy shot of revolving shadows resolving into spinning ice skaters to injured Teruhikos woozy eyes." 

==References==
 

==External links==
*    
*  

 
 
 


 