The Strait Story
{{Infobox film
| name = The Strait Story
| image =
| caption =
| director = Yu-shan Huang 黃玉珊
| writer = Yu-shan Huang (黃玉珊)
| starring = Freddy Lim Yuki Hsu Jun-Ning How-Jie Ho Hong-Shiang Lin Tsai-Yi Huang 羅浥薇
| producer =
| distributor =
| budget =
| released = October 21, 2005 ( Taiwan )
| runtime = 105 min. Taiwanese Mandarin Mandarin Japanese Japanese
| jianti =
| fanti =
| pinyin =
}}
 Taiwanese film directed by Yu-shan Huang.

==Plot summary==

The movie tells the story of a young but noteworthy Taiwanese sculptor and painter, Ching-cheng Huang(黃清埕).  He has already completed his studies in Tokyo and even had a few successful exhibitions. It is war, he plans to see his parents in Taiwan and then wants to go on to Peking, in order to deepen his knowledge of the metier. But as the artist is returning from Japan with his fiancée, a young pianist, both -  together with hundreds of others - lose their lives on board the passenger liner Takachiho Maru that is tragically torpedoed by the American submarine   in March, 1943, while on its way from Kobe to Keelung (Taiwan).

In flashbacks, we do not only see Huang in his Tokyo studio, or attending an exhibition of his works, or on the steamship mentioned, looking forward to an encounter with his father, mother, and elder brother. We also see Huang as a boy and adolescent, growing up on the island of Penghu, situated in the Taiwan Straits. It is here that we encounter him as a dreaming but also rebellious youth, in love with a girl from a poor family that he could never have married. We also learn about his father, a businessman who wanted his son to follow in his footsteps as a pharmacist  and therefore did not support him anymore when Ching-cheng decided to become an artist.

The narration switches between present and past. An important strand of the narration is focused on the restoration of lost works created by the artist that were discovered recently - a difficult task that is carefully accomplished by a young, physically handicapped (or ill?) art restorer.

The movie is also focused on the young art restorer’s search for traces of Huang (his grave, for instance), and on her attempt to reconstruct aspects of his biography. 
The film is a celebration of regional South Taiwan culture, of the land, its people and their contribution to the common Chinese heritage. Incidentally, Ching-cheng Huang is a relative of the filmmaker Yu-shan Huang.

== Cinematic approach ==

Though not radically innovative, the film cannot be considered ordinary entertainment devoid of aesthetic qualities. On the contrary, it is fairly close to the art film genre. As the Taiwanese critic Shen-chon Lai points out,  The film’s language that is artfully employed displays great similarities with the stream of consciousness technique of contemporary literature and relies on such artistic devices as ‘point of view’. Lai  notes both its realism, for Taiwans cultural history is fully reflected, and the fact that the reality that is reconstructed is … melted in a poetic atmosphere.   The critic Lingzhen Wang observes that The Strait Story, just like Huang’s subsequent feature film, Song of Chatian Mountain, combine(s) historical materials, including those censored in the past, personal memories, and a distinctive documentary style. 

==References==
 

==Further reading==
*Kate E. Taylor (ed.), dekalog 4: On East Asian Filmmakers. Brighton, UK (Wallflower Press) 2011
*Yingjin Zhang (ed.), A Companion to Chinese Cinema. Malden (Wiley-Blackwell) 2012

==External links==
*  
*  
* Yushan Huang’s blog. 

 
 
 
 