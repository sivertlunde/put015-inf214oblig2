Söndrige Sondre
 
  Swedish documentary Daniel Halasz. 
 

The film follows Sondre Kvevik, who got diagnosed with schizophrenia as a teenager, and spent almost his entire adult life locked up in a mental institution legally forced to take anti-psychotic drugs.

In the documentary, filmmaker Daniel Halasz who is Sondres little brother, investigates if the treatment Sondre receives is necessary, if the grounds for his incarceration are justified, and the effects anti-psychotic drugs has on his beloved and helpless brother.

The film premiered at the Gothenburg International Film Festival in 2004. Shortly afterwards, it was purchased by Swedish TV Channel and public broadcaster SVT where it aired primetime on Saturday 20 November 2004. Directly after the TV broadcast, a panel of psychologists and psychiatrists had a live debate about Sondres case and the Swedish mental health care system in general.

The documentary was well received by both critics and the public.  Its publicity eventually got Sondres doctors to review his case and in 2006 he was released from the hospital and was no longer required to take any sort of medication.

==References==
 

 
 
 
 
 
 
 

 
 