Lovin' the Ladies
{{Infobox film
| name           = Lovin the Ladies
| image          =
| caption        = Charles Kerr 
| producer       = William LeBaron 
| writer         = J. Walter Ruben 
| based on       =    Richard Dix Lois Wilson Allen Kearns 
| music          =
| cinematography = Edward Cronjager 
| editing        = George Marsh   
| studio         = RKO Radio Pictures 
| distributor    =
| released       =   }}
| runtime        = 65-68 minutes
| country        = United States
| language       = English
| budget         = $207,000 Richard Jewel, RKO Film Grosses: 1931-1951, Historical Journal of Film Radio and Television, Vol 14 No 1, 1994 p56 
| gross          = $428,000 
}} Richard Dix, Lois Wilson and Allen Kearns. It made a profit of $65,000. 

The films screenplay was adapted by J. Walter Ruben, based on the 1919 Broadway play, I Love You, which was written by William Le Baron, who produced this film. A younger Richard Dix starred in the play, which was produced by legendary movie cowboy G. M. Anderson. 

==Plot==
Peter Darby is an electrician sent on a call to the home of wealthy Jimmy Farnsworth.  While there, Farnsworth is telling his friend, George Van Horne, that any two people can fall in love, under the right circumstances.  When George expresses his skepticism, Jimmy bets him $5,000 that he can prove his contention.  George agrees, on the condition that he can choose the two people, to which Jimmy also agrees.  For the woman, much to the chagrin of Jimmy, George selects Betty Duncan, a bored socialite acquaintance of the two, who seems much more interested in solitary pursuits than men.  When George is seemingly having difficulty deciding on the man, his eyes alight on Peter, who he selects.

Jimmy, eager to win the bet as well as prove his theory, is not content with simply allowing nature to take its course.  He approaches Peter and gets him to agree to woo Betty, posing as a member of the upper classes, in exchange for $2,500 and Jimmys financing of the wooing. When Jimmy takes Peter to be fitted with new clothes suitable for Jimmys high society circle, Peter meets Joan Bently, a woman whom Jimmy has repeatedly asked to marry him without success. Mistaking her for the target of the bet, Peter becomes excited, but Jimmy fervently corrects him.

Under the pretense of being Jimmys friend, Peter reluctantly sets about romancing Betty at Jimmys estate. Jimmys schemes to help the two to warm up, as he provides flowers, a violinist, and other mood enhancers, such as scenting the parlor with perfume, and leaving a collection of Shelleys poems out. However, Betty is more interested in Brooks, Jimmys butler. As time goes on, Peter gives in to his strong attraction to Joan. At first believing he is just like all the other blase wealthy idlers of her acquaintance, she warms to him when he reveals his zest for life. Encouraged, he reveals his humble status and manages to persuade her to leave with him the next morning. However, when Jimmy tells her that Peter has been romancing Betty, she thinks he is just a lying womanizer. Peter forces Jimmy to admit in front of everyone what had really gone on, then leaves. He is delighted when he finds Joan waiting for him in the taxi.

==Cast== Richard Dix as Peter Darby Lois Wilson as Joan Bently
*Allen Kearns as Jimmy Farnsworth
*Rita La Roy as Louise Endicott (as Rita LaRoy)
*Anthony Bushell as Brooks, Jimmys butler
*Renée Macready as Betty Duncan
*Virginia Sale as Marie, Joans maid
*Henry Armetta as Signore Sagatelli
*Ernest Hilliard as Davison, the tailor shop owner
*Selmer Jackson as George Van Horne

==Reception==
===Box Office===
The film made a modest profit of $65,000. 
===Critical===
In his review of the film, the New York Times critic, Mordaunt Hall, gave the film a positive review, praising the acting of the players, and stating, "... what this talking film lacks in spontaneity it atones for in farcical situations. Its idea is unfailing as a comedy-froth, but pleasant enough and nicely acted by goodlooking players." 

==References==
 

==External links==
* 
* 
* 

 
 
 
 
 
 
 
 