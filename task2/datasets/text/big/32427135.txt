The Flowers of War
{{Infobox film
| name           =  The Flowers of War 
| image          = The Flowers of War english poster.jpg
| alt            =  
| caption        = Theatrical release poster
| director       = Zhang Yimou
| producer       = William Kong   David Linde   Zhang Weiping   Zhang Yimou  Brandt Andersen
| screenplay     = Liu Heng
| based on       =  
| starring       = Christian Bale   Ni Ni   Zhang Xinyi   Tong Dawei   Atsuro Watabe   Shigeo Kobayashi   Cao Kefan
| music          = Qigang Chen
| cinematography = Zhao Xiaoding
| editing        = Peicong Meng
| studio         = EDKO Film   Beijing New Picture Film   New Picture Company
| distributor    = EDKO Film   Wrekin Hill Entertainment   Row 1 Productions
| released       =  
| runtime        = 146 minutes
| country        = China
| language       = Mandarin   English   Japanese
| budget         = $94&nbsp;million   
| gross          = $95,311,434      
}} historical drama war film directed by Zhang Yimou, starring Christian Bale, Ni Ni, Zhang Xinyi, Tong Dawei, Atsuro Watabe, Shigeo Kobayashi and Cao Kefan.          The film is based on a novella by Geling Yan, 13 Flowers of Nanjing, inspired by the diary of Minnie Vautrin.  The story is set in Nanjing|Nanking, China, during the 1937 Rape of Nanking in the Second Sino-Japanese War. A group of escapees, finding sanctuary in a church compound, try to survive the plight and persecution brought on by the violent invasion of the city.      
 Best Foreign Language Film at the 84th Academy Awards,          but did not make the final shortlist.    It also received a nomination for the 69th Golden Globe Awards.    The 6th Asian Film Awards presented The Flowers of War with several individual nominations, including Best Film.      
 distribution rights were acquired by Wrekin Hill Entertainment, in association with Row 1 Productions, leading to an Oscar-qualifying limited release in New York, Los Angeles and San Francisco in late December 2011, with general release in January 2012.         

==Plot==
In 1937, Japan invades China, beginning the Second Sino-Japanese War. The Japanese Imperial Army overruns Chinas capital city, Nanking, in December and carries out the systematic and brutal Nanking massacre.

As the invading Japanese overpower the Chinese army, desperate schoolgirls flee to the nominally protective walls of their convent at a Western-run Roman Catholic cathedral. Here, John Miller (Christian Bale|Bale), an American mortician on a task to bury the head priest, joins the group of innocent schoolgirls.  He finds a boy there, George, an orphan who was raised by the dead priest, and taught English. The boy is the same age as the schoolgirls. Soon a group of flamboyant prostitutes arrive at the cathedral, seeking refuge by hiding in the cellar. Pretending to be a priest, Miller tries to keep everyone safe while trying to repair the convents truck to use for an escape.

After an incident when rogue Japanese forces assault the cathedral (who are then killed by the dying effort of a lone Chinese Major), Japanese Colonel Hasegawa promises to protect the convent by placing guards outside the gate, and requests that the schoolgirls sing a chorale for him. Several days later, he hands Miller an official invitation for the schoolgirls to sing at the Japanese Armys victory celebration. Fearing for the safety of the virginal schoolgirls, Miller declines. Hasegawa informs him that it is an order and that the girls are going to be picked up the next day. Before they leave, the Japanese soldiers count the schoolgirls and erroneously include one of the prostitutes (who has strayed from the cellar), totalling 13.

When the de facto leader of the schoolgirls, Shu Juan (Xinyi), convinces them that they are better off committing suicide by jumping off the cathedral tower, they are saved at the last moment when the de facto leader of the prostitutes, Yu Mo (Ni Ni|Ni), convinces her group to protect the schoolgirls by taking their place at the Japanese party. As there are only 12 prostitutes, George, the dead priests adoptive son, volunteers as well. Miller initially opposes their self-sacrificing decision, but ultimately assists in disguising them, using his skills as a mortician to adjust their makeup and cut their hair to appear like schoolgirls. The prostitutes also create knives out of broken windows and hide them in their cloaks.

The next day, the "13 Flowers of Nanking" are led away by the unsuspecting Japanese soldiers. After they depart, Miller hides the schoolgirls on the truck he repaired and, using a single-person permit provided by the father of a schoolgirl, drives out of Nanking. In the last scene, the truck is seen driving on a deserted highway heading west, away from the advancing Japanese army. The fate of the 13 Flowers remains unknown, apparently martyring themselves for the students freedom.

==Cast==
* Christian Bale as John Miller
* Ni Ni as Yu Mo
* Zhang Xinyi as Shu
* Tong Dawei as Major Li
* Atsuro Watabe as Colonel Hasegawa
* Shigeo Kobayashi as Lieutenant Kato
* Cao Kefan as Mr. Meng
* Huang Tianyuan as George Chen
* Han Xiting as Yi
* Zhang Doudou as Ling
* Yuan Yangchunzi as Mosquito
* Sun Jia as Hua
* Li Yuemin as Dou
* Bai Xue as Lan
* Takashi Yamanaka as Lieutenant Asakura Paul Schneider as Terry

==Production==
In December 2010, it was announced that the film would be made, and pre-production started the same month. They began shooting on location in Nanking|Nanjing, China, on January 10, 2011.    The dialogue of the film was shot about 40% in English and the rest in Mandarin Chinese (particularly in the Nanjing dialect, distinct from Standard Chinese) and Japanese,       with an estimated production budget of $94 million,  which makes it the most expensive film in Chinese history.   

  |source=Zhang Yimou on the films message.|width=35%|align=right}}
 Zhang said that he tried to portray the Japanese invaders with multiple layers. Regarding Colonel Hasegawas sympathetic features, he explained that "in 1937, the militaristic notion among Japanese armies was very prevalent, and officers were not allowed to sing a homesick folk song, but we still wanted to endow this character with something special."    The director articulated that his biggest, though challenging, accomplishment in the film was the creation of John Miller, saying that "this kind of character, a foreigner, a drifter, a thug almost, becomes a hero and saves the lives of Chinese people. That has never ever happened in Chinese filmmaking, and I think it will never happen again in the future." Filming completed within 6 months.    One challenging aspect was what Zhang called the "very slow pace" of negotiation with the Chinese censorship authorities during the editing process.   

==Marketing==
On September 9, 2011, the film was retitled The Flowers of War, after a 20-minute screening for prominent United States|U.S. film distributors and the media at the Toronto International Film Festival.     Zhang stated that the story in The Flowers of War differs from many other Chinese films on this subject as it is told from the perspectives of women.    In October 2011, the first trailer was released, making way for an American trailer to be revealed.            

==Release== producer of head of Film Bureau of SARFT, hoping it would mediate the dispute.    At the order of SARFT, both sides were to reach a compromise, which was achieved after four hours of negotiation.      

===Box office===

====China====
The Flowers of War was released in China just days after the 74th anniversary of the   ($145.5 million) and Avatar (2009 film)|Avatar ($204 million).       After five weeks of release the movie earned $93 million.    The film reportedly earned $95 million in China.   

===Critical reaction===
The movie received mixed reviews. Rotten Tomatoes reported a 41% critical approval rating with an average of 5.5/10.    Metacritic, another review aggregator, assigned the film an average score of 46 (out of 100) based on 22 reviews from mainstream critics.   
 Boxoffice Magazine gave it 4 stars of 5, and said "The Flowers of War is ultimately an inspiring, stirring and unforgettable human drama in the face of a horrifying war. It is highly recommended."    Variety (magazine)|Variety gave a generally positive review, describing the film as "a uniquely harrowing account of the rape of Nanjing," and defined it as "a work of often garish dramatic flourishes yet undeniable emotional power, finding humor and heartbreak in a tale of unlikely heroism in close quarters."   

Most negative feedback from critics were similar to that from Toronto Star, which gave the film 2.5/4, and said that "the drama is often weakened by the penchant for creating spectacles."    Roger Ebert, who gave the film 2 out of 4 stars, took issue with making the story about a white American, "Can you think of any reason the character John Miller is needed to tell his story? Was any consideration given to the possibility of a Chinese priest? Would that be asking for too much?"   

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|- Asian Film Awards   
| Best Composer
| Qigang Chen
|  
|-
| Best Costume Designer
| William Chang
|  
|-
| Best Director
| Zhang Yimou
|  
|-
| Best Film
| The Flowers of War
|  
|-
| Best Newcomer
| Ni Ni
|  
|-
| Best Screenwriter
| Geling Yan and Liu Heng
|  
|-
| Golden Globe Awards   
| Best Foreign Language Film
| rowspan="2"|The Flowers of War
|  
|-
| Hong Kong Film Award 
| Best Film of Mainland and Taiwan
|  
|- The Golden Reel Awards   
| Best Sound Editing – Foreign Feature
| Row 1 Entertainment
|  
|-
|}

===Home media===
The Flowers of War was released on Blu-ray Disc and DVD on June 10, 2012.      

==See also==
 
* List of submissions to the 84th Academy Awards for Best Foreign Language Film
* List of Chinese submissions for the Academy Award for Best Foreign Language Film
* Golden Globe Award for Best Foreign Language Film

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 