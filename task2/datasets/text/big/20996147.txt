L'Amore (film)
  
{{Infobox film
| name            = LAmore
| image           = 
| caption         = 
| director        = Roberto Rossellini
| producer        = Roberto Rossellini
| writer          = Roberto Rossellini Federico Fellini Jean Cocteau
| starring        = Anna Magnani Federico Fellini  Renzo Rossellini
| cinematography  = 
| editing         = 
| studio          = Finecine
| distributor     = Joseph Burstyn (US)
| released        = August 1948 (Venice Film Festival) February 1950 (US)
| runtime         = 70 minutes
|}}
LAmore (1948 in film|1948) is an Italian anthology film directed by Roberto Rossellini starring Anna Magnani and Federico Fellini. 

The film opened to considerable controversy in the United States, which led to a lengthy legal dispute, Joseph Burstyn, Inc. v. Wilson, that ended up in the Supreme Court of the United States that ruled in 1952 that film as a form of expression was protected under the First Amendment to the United States Constitution.

==Production background==
The film has two parts: "Il Miracolo" ("The Miracle") and "Una Voce Umana" ("The Human Voice"). The latter is based on a French play The Human Voice (La Voix humaine, 1930) by Jean Cocteau. Rossellini and Fellini co-wrote "Il Miracolo", and Rossellini adapted Cocteaus play. Magnani appears in both segments.
 best actress for her performance in the film.
 First Amendment freedom of speech issues.

=="The Miracle"==
Fellini and Rossellini co-wrote the script for "The Miracle".
 impregnates "Nanni" (Anna Magnani), a disturbed peasant who believes herself to be the Virgin Mary.

=="The Human Voice"==
"The Human Voice" is based on Cocteaus play about a woman desperately trying to salvage a relationship over the telephone.

==Controversy==
"The Miracle" part of the film was embroiled in a major controversy when U.S. distributor Joseph Burstyn exhibited the film, as Ways of Love with English subtitles, in New York City in November 1950. In December, Ways of Love was voted Best Foreign Language Film of 1950 by the New York Film Critics Circle.

The film was condemned by the National Legion of Decency in 1951 as "anti-Roman Catholic|Catholic" and "sacrilegious".

The New York State Board of Regents, in charge of film censorship for New York State, revoked the license to show the film on February 16, 1951. This led to a lawsuit finally decided by the United States Supreme Court in 1952 in Joseph Burstyn, Inc. v. Wilson, a case popularly known as the "Miracle Decision", which declared the film was a form of artistic expression protected by the freedom of speech guarantee of First Amendment to the United States Constitution.

Due to legal complications over the rights to Cocteaus play, the film was not exhibited for many years, until a restored print was shown at the Roxie Cinema in San Francisco in 1978.

==See also==
*Mutual Film Corporation v. Industrial Commission of Ohio (1915) U.S. Supreme Court case
*Film censorship in the United States
*Whirlpool of Desire (1939) film distributed by Burstyn and Arthur Mayer

==References==
* 
==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 