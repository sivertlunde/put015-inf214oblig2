The Strike (1947 film)
{{Infobox film
| name           = The Strike
| image          = 
| image_size     = 
| caption        = 
| director       = Karel Steklý
| producer       = 
| writer         = Marie Majerová Karel Steklý
| narrator       = 
| starring       = Josef Bek
| music          = Emil František Burian|E. F. Burian
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 77 minutes
| country        = Czechoslovakia
| language       = Czech
| budget         = 
| gross          = 
}}
The Strike ( ) is a 1947 Czechoslovakian film about striking miners directed by Karel Steklý.    It is based on the novel of the same title by Marie Majerová.

==Cast==
* Josef Bek as Karel Hampl
* L. Bohác as Hudec
* Josef Dekoj as Cerný
* Nadezda Gajerová as Ruzena (as N. Mauerová)
* Vera Kalendová as Cerná
* Bedrich Karen as Bacher
* Lída Matousková
* Marie Vásová as Hudcová

==References==
 

==External links==
*  

 
{{succession box Grand International Prize of Venice
| years=1947
| before=n/a
| after=Hamlet (1948 film)|Hamlet
}}
 

 

 
 
 
 
 
 

 
 