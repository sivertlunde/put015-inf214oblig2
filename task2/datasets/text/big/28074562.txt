Judge and the Forest
 
{{Infobox Film
| name           = Judge and the Forest
| image          = 
| image size     = 
| caption        = 
| director       = Rangel Vulchanov
| producer       = 
| writer         = Rangel Vulchanov
| narrator       = 
| starring       = Lyubomir Bachvarov
| music          = 
| cinematography = Viktor Chichov
| editing        = 
| distributor    = 
| released       = 12 December 1975
| runtime        = 102 minutes
| country        = Bulgaria
| language       = Bulgarian
| budget         = 
| preceded by    = 
| followed by    = 
}}

Judge and the Forest ( , Transliteration|translit.&nbsp;Sledovatelyat i gorata) is a 1975 Bulgarian drama film directed by Rangel Vulchanov. It was entered into the 26th Berlin International Film Festival.   

==Cast==
* Lyubomir Bachvarov - Sledovatelyat Nikolov
* Sonya Bozhkova - Elena
* Alexander Pritup - Mihaylov
* Georgi Kishkilov - Sledovatelyat Naydenov
* Penka Tsitselkova - Studentkata
* Tzvetana Golanova - S.N.Popova
* Georgi Rusev - Stoyan Dimitar Angelov - Prokopiev
* Dimiter Milushev - Bonev
* Emil Dzamdzijew - Mladezhat na garata

==References==
 

==External links==
* 

 
 
 
 
 
 