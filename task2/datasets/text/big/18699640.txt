The Halliday Brand
{{Infobox film
| name           = The Halliday Brand
| image          = HallidaybrandOS.gif
| caption        = Theatrical poster Joseph H. Lewis
| producer       = Robert Eggenweiler Collier Young
| writer         = George W. George George F. Slavin
| starring       = Joseph Cotten
| music          = 
| cinematography = Ray Rennahan
| editing        = Michael Luciano Stuart OBrien
| distributor    = 
| released       =  
| runtime        = 79 minutes
| country        = United States 
| language       = English
| budget         = 
}}
 Western film Joseph H. Lewis and starring Joseph Cotten.    

==Plot==
Daniel and Martha Halliday, son and daughter of the powerful Big Dan, strenuously object to his bigotry and violence. When he leads the lynching of her half-breed beau, Jivaro Burris, it is the last straw.

Dan becomes estranged from his father and closer to his rival, Jivaros father, while also developing a romantic attachment to Aleta Burris, the dead mans sister. It leads to a showdown between the two sides.

==Cast==
* Joseph Cotten as Daniel Halliday
* Viveca Lindfors as Aleta Burris
* Betsy Blair as Martha Halliday
* Ward Bond as Big Dan Halliday Bill Williams as Clay Halliday
* Jay C. Flippen as Chad Burris
* Christopher Dark as Jivaro Burris
* Jeanette Nolan as Nante
* Peter Ortiz as Manuel

==References==
 

==External links==
* 

 

 
 
 
 
 
 
 
 
 