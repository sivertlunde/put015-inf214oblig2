The Amsterdam Kill
{{Infobox film
| name           = The Amsterdam Kill
| image          = The_Amsterdam_Kill.jpg
| image_size     = 
| caption        = 
| director       = Robert Clouse
| producer       = Raymond Chow
| writer         = 
| narrator       = 
| starring       = Robert Mitchum
| music          = 
| cinematography = 
| editing        =  Golden Harvest
| distributor    = Hong Kong:  
| released       = Denmark:   Hong Kong:   USA:  
| runtime        = 90 minutes
| country        = Hong Kong United States
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} Richard Egan.  

==Plot==
Former DEA Agent Quinlan, removed from the force some years earlier for stealing confiscated drug money, is hired by Chung Wei, a leader in the Amsterdam drug cartel, who wants out of the business. Quinlans job is to use Chungs information to tip DEA agents to drug busts, thereby destroying the cartel. But when the first two "tips" go awry, resulting in murdered DEA officers, the feds must decide whether to trust Quinlan further... 

==Cast==
*Robert Mitchum as Quinlan Richard Egan as Ridgeway
*Leslie Nielsen as Riley Knight
*Bradford Dillman as Odums
*Keye Luke as Chung Wei Mars
*Lam Ching-ying
*Yuen Biao
*Yuen Wah

==References==
 

==External links==
* 
* 

 

 

 
 
 

 