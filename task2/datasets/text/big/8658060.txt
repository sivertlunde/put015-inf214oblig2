Nashville Rebel (film)
{{Infobox Film
| name           = Nashville Rebel
| image          = Nahville Rebel 1965 poster.jpg
| image_size     = 
| caption        = Promotional poster for "Nashville Rebel"
| director       = Jay J. Sheridan
| producer       = Fred A. Niles
| writer         = Ira Kerns Jay J. Sheridan
| narrator       = 
| starring       = {{Plainlist | 
* Waylon Jennings
* Mary Frann
* Gordon Oas-Heim
* CeCe Whitney
}}
| music          = Robert Blanford
| cinematography = 
| editing        = 
| distributor    = American International Pictures 1966
| runtime        = 95 minutes
| country        = U.S.A. English
| budget         = 
}} Nashville Rebel" anthology.

==Plot==
 
Arlin Grove (Jennings) has just finished a hitch in the Army and finds hes stranded in the small town of Morgans Corner after being robbed by drunken rednecks. Grove is taken in by pretty Molly Morgan (Mary Frann) and her father, and it doesnt take long for Molly to become infatuated with the rugged stranger while nursing him back to health. 

Arlin and Molly soon marry, and after playing a few songs at a local honky-tonk, Grove becomes a professional musician when hes offered 75 dollars a week for a standing Saturday night gig. Word about Grove begins to spread, and entertainment lawyer Wesley Lang (Gordon Oas-Heim) offers to take over his management and take him to the big time. Langs paramour Margo (CeCe Whitney) helps give Arlins act some polish, and before long the singer is knocking em dead on the country circuit, and even playing the Grand Ole Opry. 

Lang takes it upon himself to break up Arlin and Mollys marriage, convinced it would be better for Groves career if he were single, and Molly, now expecting a baby, is left heartbroken. Arlin soon finds himself of the other side of Langs machinations when the manager wrongly suspects his new client is having an affair with Margo; Lang sabotages Grove with a booking at a ritzy supper club, and thinking his career is over, Grove turns to the bottle. An apologetic Margo consoles Arlin, and helps him get back to Molly. (Special thanks to Mark Deming, AMG)

==Aftermath==
While the film was a modest success, this would be the only movie which featured Waylon Jennings as a lead actor. Waylon himself stated in his autobiography (1994, Warner Books) that his performances were not as good as they could have been, and cautioned fans against seeing the film. Eventually, Waylon stopped playing "Green River" (released as a single by RCA) in his live performances.

Several years later, the films title would prove prophetic: Waylon (with others) was openly rebelling against Nashvilles producer-oriented approach to recording. Ironically, the film was pulled from distribution in the mid-1970s, and despite the upsurge in Waylons popularity in the late 1970a and early 1980s, "Nashville Rebel" would neither be seen on TV nor be officially released on video until the mid-1990s.

==Ownership==
Upon release in 1966, "Nashville Rebel" was billed as a co-production between Fred A. Niles Productions and Nashville-based Show Biz, Inc., and distributed by American International Pictures. At some point, ownership of the film fell solely into the hands of Show Biz. Unfortunately, the assets of Show Biz lay dormant throughout the mid-1980s until the mid-1990s.

In the early 1990s, the video and film assets of Show Biz, Inc. were purchased from Norman Lear by Willie Nelson, who then created Act IV Productions, its sole purpose being management of the Show Biz library. Not long afterwards, Waylon himself became the owner to the rights of his first feature film.  
 soundtrack LP.

==Aspect ratio problems==
When preparing the film for release on VHS and DVD, an anamorphic projection print was used rather than the original Techniscope negatives. As a result, the home video releases are indeed presented in a 1.78:1 aspect ratio. However, since the film was shot in Techniscope (a method often used by budget filmmakers), the correct exhibition ratio is 2.35:1. The 1.78:1 prints used are vertically compressed, intended to be projected with a special lens in order to achieve the proper aspect ratio. When viewed on a widescreen TV, the picture settings can be adjusted to the proper ratio.

==External links==
*  
*  

 
 
 