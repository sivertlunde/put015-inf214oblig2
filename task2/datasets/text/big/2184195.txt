Return to the 36th Chamber
 
 
 
{{Infobox film
| name           = Return to the 36th Chamber
| image          = Return 36th Chamber movie poster.jpg
| caption        = The Hong Kong film poster.
| director       = Lau Kar-Leung
| producer       = Mona Fong Run Run Shaw
| writer         = Ni Kuang
| narrator       =
| starring       = Gordon Liu
| music          = Eddie Wang
| cinematography = Peter Ngor
| editing        =
| distributor    = Shaw Brothers Studio
| released       =  
| runtime        = 99 min.
| country        = Hong Kong
| language       = Cantonese
| budget         =
}} martial arts Shaolin monk San Te, but in Return, he portrays an imposter monk.

== Plot ==

The story opens at a fabric dyeing mill. The quality of the dyes has noticeably worsened, and the factory owner, Wang, and his subordinate chief, Boss Wa, decide to hire some Manchu overseers to improve the work. Wang decides to cut the workers salary to pay the mercenaries, and when the workers protest they are viciously thrashed.
 con man and the foremans younger brother who is posing as a monk. He offers to help, but since he cannot actually do kung fu, he and the foremans assistant, Ah Chao, devise a plan to trick the Manchu into reinstating the full salary pay, with Jen-chieh posing as the Shaolins head abbot, San Te.

At first the scheme works, but Wang quickly works out that this man is an impostor, and has him driven away and his Manchu hirelings retaliate against the workers. Feeling guilty about what has happened, Jen-chieh leaves and heads for the Shaolin temple. His first attempts to enter by stealth are thwarted by the vigilant monks and his own bumbling, but eventually he manages to sneak his way in, just to run into the Abbot San Te himself. He, too, realizes that Jen-chieh is not what he claims to be, but he announces that he wants to give him a chance: Jen-chieh is to build a set of gantries all around the temple and renovate the entire complex.

Somewhat reluctantly, Jen-chieh goes to work, but he is constantly distracted by the monks practicing martial arts in the training courtyard, which he can look into from his high vantage point. Eventually, he begins to train in kung fu by himself, using the conditions of his assignment to improvise training facilities. However, this causes him to lag behind in his work, and it takes him more than a year to finish the gantry. As soon as Jen-chieh announces that he is finished, the abbot wants him to dismantle the structure and leave the monastery. Rebelling against this decision, Jen-chieh lands in the training courtyard and while trying to evade the abbot chasing him, he inadvertently manages to pass all hazards set in the yard with ease. The abbot finally corners him and forces him to leave, but with a strange smile playing around his lips.

Jen-chieh returns to town to find that the conditions of the workers have worsened. Their salary has been cut by nearly half, and any who have protested had been laid off immediately. When some of the ex-workers attack him, Jen-chieh instinctively and to his own surprise fights them off with his newly acquired kung fu skills. The next morning, Jen-chieh appears at the dye mill and thrashes the overseers, using bamboo fibers (like he used when building the gantry) to tie them up and incapacitate them and introducing his style as "scaffolding kung fu".

Wang and his bodyguards appear at the scene. Jen-chieh lures them out of the city to a mansion under construction, where he uses the building equipment and the tight quarters to his advantage. Finally overpowering Wang, he forces him to pay his workers their full wages again. Wang admits defeat, and Jen-chieh continues his training on the half-finished grounds.

==Cast==
* Gordon Liu as Chu Jen-chieh
* Kara Hui as Hsiao Hung Hsiao Ho as Ah Chao
* Wong Ching-ho as Uncle Li
* Wa Lun as Chou Sheng
* Lee King-chue as Abbot San Te
* Chan Si-gaai as Ah Fen
* Kwan Yung-moon as Chuan Min
* Yeung Jing-jing as Hsiao Ting
* Johnny Wang Lung-wei as Huang Kao-feng
* Kong Do as Chief Ma
* Chang Yi-tao as Yuen Li-hou

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 