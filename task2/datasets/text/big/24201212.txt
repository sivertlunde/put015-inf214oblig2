Between the Devil and the Deep Blue Sea (film)
 
{{Infobox film
| name           = Between the Devil and the Deep Blue Sea
| image          = Between the Devil and the Deep Blue Sea (film).jpg
| caption        = Film poster
| director       = Marion Hänsel
| producer       = Marion Hänsel Jessinta Liu
| writer         = Marion Hänsel Nikos Kavvadias Louis Grospierre
| starring       = Stephen Rea
| music          =
| cinematography = Bernard Lutic
| editing        = Susana Rossberg
| distributor    =
| released       =  
| runtime        = 92 minutes
| country        = Belgium France
| language       = Dutch English
| budget         =
}}

Between the Devil and the Deep Blue Sea is a 1995 Belgian-French drama film directed by Marion Hänsel. It was entered into the 1995 Cannes Film Festival.    The plot is based on the short story titled "Li", by the Greek poet and sailor Nikos Kavvadias.

==Plot==
Nikos (Stephen Rea), a sailor, learns that the company that runs his ship has gone bankrupt. For the few weeks it will take to sell the ship he is on, the ship remains off the coast of Hong Kong. It is boarded by a young beggar girl Li, (Ling Chu) who offers to take care of him in exchange for food for her and her baby brother. Though he claims to have no use for her, Nikos reluctantly agrees to the deal.

Nikos struggles with an opium addiction and regret over abandoning his girlfriend and their child. He warms to Li and her brother, treating them as surrogate children. When his final paycheck comes through he decides to return to Europe but not before bringing Li ashore for a day where he meets both her mother and father. 
 Shantung silk, the symbol Li had previously told Nikos represented luck.

==Cast==
* Stephen Rea as Nikos
* Ling Chu as Li
* Adrian Brine as Captain
* Maka Kotto as African sailor
* Mischa Aznavour as Young sailor
* Koon-Lan Law as Lis mother
* Jane Birkin as The Woman Nikos once loved (voice)
* Chan Chan Man as Baby Zheng, Lis little brother
* Chow Jo as Grand-Father
* Wong Wong as Grand-Mother
* Fow Tse as Stepfather
* Cheung Chi Ho as Sailor Chong Ching as Sailor

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 