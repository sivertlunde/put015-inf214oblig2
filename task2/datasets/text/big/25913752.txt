Water for Elephants (film)
 
 
{{Infobox film
| name           = Water for Elephants
| image          = Water for Elephants Poster.jpg
| image_size     = 215px
| alt            = 
| caption        = Theatrical release poster
| director       = Francis Lawrence
| producer       = Gil Netter Erwin Stoff Andrew R. Tennenbaum
| screenplay     = Richard LaGravenese
| based on       =  
| starring       = Reese Witherspoon Robert Pattinson Christoph Waltz Hal Holbrook 
| music          = James Newton Howard
| cinematography = Rodrigo Prieto
| editing        = Alan Edward Bell Fox 2000 Pictures Dune Entertainment Ingenious Media
| distributor    = 20th Century Fox
| released       =  
| runtime        = 120 minutes  
| country        = United States
| language       = English Polish
| budget         = $38 million 
| gross          = $117,094,902 
}} 2006 Water novel of the same name. It stars Reese Witherspoon, Robert Pattinson, and Christoph Waltz.  

The film was released in the United States and Canada on April 22, 2011, and received mixed to positive reviews from film critics; it garnered a "Fresh" rating on Rotten Tomatoes based upon aggregated reviews,  and a rating of "mixed or average reviews" at Metacritic. 

==Plot==
The film opens in present day, when the proprietor of a small traveling circus (portrayed in the movie by Circus Vargas) encounters an elderly man, Jacob, who has apparently become detached from his nursing home group, which attended the circus earlier that day. They strike up a conversation and Jacob reveals that he had a career in the circus business and was present during one of the most infamous circus disasters of all time and the circus owner asks him to share his story.
 flashback to the era of the Great Depression. 23-year-old Polish American Jacob is a Cornell veterinary medicine student on the brink of a promising career as a veterinarian. Unfortunately, during his last final exam, he is informed that his parents were killed in a car crash. His father has left huge debts, and the bank was foreclosing on Jacobs home. Feeling there is no point in returning to school, and having no home to go to, he jumps onto a passing train where he meets Camel.

In the morning, Jacob discovers that he jumped on the Benzini Brothers Circus train. He sees a beautiful woman, Marlena Rosenbluth, and meets August, the circus owner, head animal trainer, and Marlenas husband. Jacob reveals he studied veterinary science and has noticed a problem with Silver, the star horse in the show. August agrees to hire Jacob as a vet for the circus animals after Jacob tells August that Silver has laminitis, is in terrible pain and will soon be unable to walk, never mind perform.

August instructs Jacob to "fix" Silver and keep him performing as long as possible. But Jacob cannot bear to see Silvers suffering and takes it upon himself to put Silver down. August is furious with Jacobs decision to euthanize Silver against orders. To show Jacob who is boss, he threatens to throw him from the moving train — telling him that an animals suffering is nothing compared to a mans, and that Jacob must carry out all of Augusts future orders if he wishes to keep his job.

August eventually procures Rosie the elephant as Silvers replacement. He is initially thrilled and invites Jacob to his car for dinner and cocktails with him and Marlena. Jacob — clearly attracted to Marlena — watches uncomfortably as the married couple flirt and dance in front of him, but later in the evening becomes clear that their relationship is complicated. August is possessive, jealous and sometimes rough with Marlena.

In the next few weeks, August becomes frustrated when Rosie the elephant seems impossible to train. August is brutal with Rosie, beating her when she fails to follow orders. After one such beating that August gave to Rosie when she ran away after fleeing from the event and dropping Marlena, Jacob realizes that the elephant was trained in Polish and only understands Polish commands. After that, Rosie performs beautifully and the circus enjoys a short period of success. While working closely together to train Rosie, Jacob and Marlena fall in love. After August discovers this, he cruelly taunts the two. Later that night, Marlena discovers that August plans to throw Jacob from the train and they run away together, hiding in a local hotel. Soon after consummating their relationship, they are ambushed by Augusts henchmen who drag Marlena away and beat up Jacob.

Jacob returns to the circus to find Marlena. After Marlena finds Jacob she tells him that his friends Walter (Kinko) and Camel were thrown from the train and killed. Several circus employees have become fed up with Augusts murderous cruelty and unleash their revenge by unlocking all the animals cages while the big top tent is jam-packed with an audience enjoying Marlena and Rosies performance.  Jacob attempts to find Marlena in the chaos and August attacks him. When Marlena tries to stop August from beating Jacob, he turns his fury on her and attempts to choke her, while one of Augusts henchmen continues beating Jacob. Two circus workers save Jacob just in time. Lying on the ground, bloodied and beaten, he looks up and sees Rosie hit August on the back of the head with a metal spike, killing him.

Back in the present, Jacob explains what happened afterward: he returned to Cornell, took his last exam, and finished his degree. He and Marlena took several of the horses and Rosie and got jobs with Ringling Brothers — he as a vet and she continuing to perform with the animals. They married and had five children and kept Rosie until her death many years later. Eventually, he took a job as a vet at the Albany Zoo and Marlena died peacefully in her bed at an old age. He convinces the circus owner to whom he is telling his story to hire him in the ticket booth. The circus owner agrees and Jacob states that he has finally come home.

==Cast==
* Reese Witherspoon as Marlena Rosenbluth
* Robert Pattinson as Jacob Jankowski
* Hal Holbrook as older Jacob Jankowski
* Christoph Waltz as August Rosenbluth Tai as Rosie, the elephant
* James Frain as Rosies caretaker Paul Schneider as Charlie OBrien
* Ken Foree as Earl
* Tim Guinee as Diamond Joe
* Mark Povinelli as Kinko/Walter Scott MacDonald as Blackie Jim Norton as Camel
* Richard Brake as Grady
* Sam Anderson as Mr. Hyde
* John Aylward as Mr. Ervin
* Uggie as Queenie, the terrier

==Production==

===Filming=== Fillmore in Kensington and Chickamauga in Vanity Fair in which he was her estranged son. Reshoots for the film were scheduled for mid January 2011. 

The stampede scenes were digitally composed. 

==The Loco Star of the Movie== McCloud River Railroad No. 18. McCloud No. 18 was built in 1914, and it was the true star of the film.   

==Release==

===Critical response===
Water for Elephants has received mixed to positive reviews; review aggregate  , which assigns a  . Retrieved April 26, 2011. 

  of The Hollywood Reporter gave the film a positive review. He stated: "The Reese Witherspoon-Robert Pattinson film will please fans of Sara Gruen’s best seller, but it lacks the vital spark that would have made the drama truly compelling on the screen." 

Kenneth Turan of the Los Angeles Times gave the film a positive review, stating that "despite the stars lack of romantic chemistry, theres much to enjoy in this cinematic retelling of Sara Gruens big top bestseller, starting with the spectacular circus setting." 

 , film critic for ReelViews,wrote: "Theres an old-fashioned vibe to Water for Elephants; its the kind of movie Hollywood once turned out with regularity but rarely does anymore." 

Some critics, however, praised the films cast.   of Rolling Stone also said that Pattinson and Witherspoon "smoldered" under the "golden gaze of Rodrigo Prietos camera." 

===Box office===
Water for Elephants was released in theaters on April 22, 2011. In the United States and Canada, Water for Elephants was released theatrically in 2,817 conventional theaters. The film grossed $6,924,487 during its opening day on April 22, 2011, with midnight screenings in 2,817 locations.  . Box Office Mojo. Retrieved 2011-09-27.  Overall the film made $16,842,353 and debuted at #3 on its opening weekend.  On its second weekend, it dropped to #4 and grossed $9,342,413 - $3,313 per theater.  By its third weekend it dropped down to #6 and made $6,069,603 - $2,322 per theater.  As of September 27, 2011 its final gross is $58,709,717 in the United States and $58,385,185 overseas, for a total of $117,094,902. 

===Accolades===
{| class="wikitable"
! Group !! Category !! Recipient !! Result
|-
| NewNowNext Awards
| Next Must See Movie
|
|  
|-
| rowspan="3" | 2011 Teen Choice Awards  Choice Movie – Drama
|
|  
|-
| Choice Movie Actor – Drama
| Robert Pattinson
|  
|-
| Choice Movie Actress – Drama
| Reese Witherspoon
|  
|-
| rowspan="4" | 38th Peoples Choice Awards
| Favorite Drama Movie
|
|  
|-
| Favorite Book Adaption
|
|  
|-
| Favorite Movie Actor
| Robert Pattinson
|  
|-
| Favorite Movie Actress
| Reese Witherspoon
|  
|-
| rowspan="3" | Satellite Awards Best Original Score
| James Newton Howard
|  
|- Best Art Direction and Production Design
| Jack Fisk
|  
|- Best Costume Design
| Jacqueline West
|  
|}

===Home media===
The film was released on Blu-ray Disc|Blu-ray and DVD on November 1, 2011 in two physical packages: a 1-disc DVD and 2-disc Blu-ray/Digital Copy combo pack.       As of Nov. 6, it has been on Direct TV Cinema.

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 