Sold (2014 film)
{{infobox film
| name           = Sold
| image          = Sold_2014_movie_poster.jpg
| imagesize      =
| caption        = Theatrical release poster
| director       = Jeffrey D. Brown
| producer       = Emma Thompson, Jane Charles, Katie Mustard
| based on       =  
| writer         = Joseph Kwong 
| starring       = Niyar Saikia Gillian Anderson David Arquette Seema Biswas Sushmita Mukherjee Parambrata Chatterjee
| music          = John McDowell
| cinematography = Seamus Tierney
| studio         = 
| distributor    = 
| released       = March 7, 2014
| runtime        = 
| country        = United States
| language       = English
}}
 Patricia McCormicks novel Sold (McCormick novel)|Sold.  Sold illustrates the story of Lakshmi who journeys from a rural village in Nepal to a brothel called Happiness House in Kolkata, India.  The film is executive produced by Emma Thompson, Jane Charles, and Katie Mustard. The script was written by Joseph Kwong and Jeffrey D. Brown. 

==Plot==
 
A girl risks everything for freedom after being trafficked from her mountain village in Nepal to a brothel in India.

==Cast==
* Niyar Saikia - Lakshmi
* Gillian Anderson - Sophia
* David Arquette - Sam
* Seema Biswas - Amma
* Sushmita Mukherjee - Mumtaz
* Parambrata Chatterjee - Vikram
* Ankur Vikal - Varun
* Neerja Naik - Anita
* Tillotama Shome - Bimla

==Awards==
{| class="wikitable" width=
! colspan="5" style="background: LightSteelBlue;" | Awards
|- style="background:#ccc; text-align:center;"
! Year
! Award
! Category
! Recipients and nominees
! Result
|- 2015
|Sonoma International Film Festival World Cinema Audience Award Jeffrey D Brown
| 
|- 2014
|London Indian Film Festival  Pure Heaven Audience Award Jeffrey D Brown
| 
|- 2014
|Albuquerque Film and Media Experience  Best Narrative Feature Jeffery Dean Brown and Jane Charles
| 
|- 2014
|River to River. Florence Indian Film Festival Audience Award Jeffery Dean Brown and Jane Charles
| 
|-
|}

==References==
 

==External links==
*  
* 

 
 
 
 
 
 
 