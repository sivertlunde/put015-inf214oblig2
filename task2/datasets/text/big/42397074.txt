Drive, She Said
Drive, Josh Hamilton. IMDB,  . Retrieved 2014-04-04. 

==Plot Summary==

Nadine (Moira Kelly), a bank teller, is taken hostage by Tass (Josh Hamilton), who has robbed the bank to pay for medical care for his ailing mother. The film focuses on the developing relationship between Nadine and Tass, and Nadines changing views in relation to her conventional life and relationships. The police and Nadines longtime boyfriend, fellow bank employee Jonathan (Sebastian Spence) search for and successfully find her, though also find that she has been permanently affected by her time with Tass.

==Critical Reception==
 Double Happiness, which starred Sandra Oh. Drive, She Said received mixed reviews. Judy Gerstel "Sophomore effort crashes", Toronto Star, September 4, 1998;  
 , September 4, 1998. Source:  ; retrieved 2014-04-13.   Writing in Variety (magazine)|Variety, Derek Elley described the film as "a meet-cute road movie that starts in high gear but soon takes too many left turns for its own good. Mina Shum’s second feature, after her well-remarked, Chinese-themed low-budgeter Double Happiness is too mild a confection to motor on to much theatrical business." 

==Release History==
The film, produced by Stephen Hegyes had a limited release. It premiered at the Toronto International Film Festival in 1997, where audience reaction was positive, but did not generate firm distribution interest.     It was then shown at the Popcorn Festival in Sweden, in 1998.   Also in 1998, the film was invited to the competition section of the Delle Donne International Film Festival, in Turin, Italy. 

==Cast ==
*Moira Kelly  ... Nadine Ship  
*Sebastian Spence  ... Jonathan Evans   Josh Hamilton  ... Tass Richards   Jim Byrnes  ... Dr. Glen Green  
*Lori Ann Triolo  ... Jo (as Lori Triolo)  
*Peter Stebbings  ... Detective Eddie  
*David Hurtubise  ... Ben Polstein  
*Hiromoto Ida  ... Sloan  
*Mina Shum  ... Chen  
*John B. Destry  ... Bob The Guard (as John Destry)  
*Hrothgar Mathews  ... Ernie  
*Carrie Cain-Sparks  ... The Waitress (as Carrie Cain Sparks)  
*Mike Crestejo  ... Bike Cop   Amanda Leary  ... Bank Heistess  
*Allan Franz  ... Medic  
*Tom Scholte  ... Arnold The Gas Guy  
*Tom Shorthouse  ... Kindly Older Gent   Tong Lung  ... Counter Person  
*Alex Diakun  ... The Prophecy  
*Carla Stewart  ... Tasss Mom  
*Harry Kalinski  ... Merlin The Driver (as Harry Kalensky)  
*Micki Maunsell  ... Cranky Lady

==References==
 

 
 
 
 
 