Walter Makes a Movie
 

Walter makes a Movie was directed by Tom Seymour and Walter Forde in 1922. Forde plays a petty thief who steals from film star Pauline Highbrow (Pauline Peters). Pursued by the police he ends up in the Star Film Companys studio during the shooting of her latest melodrama.

This film is preserved in the National Film Archive. A version from a 1935 9.5mm home movie release is available to watch on YouTube channel British Silents.

Further information on Forde and his silent movies can be found in Chapter Five of ebook British Silent Film Comedies available from Amazon.

 
 


 