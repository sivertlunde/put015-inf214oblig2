Le Capitaine Fracasse (1943 film)
Le Capitaine Fracasse (Captain Fracasse) is a black and white film by Abel Gance released in 1943. It is an adaptation of the novel Le Capitaine Fracasse by Théophile Gautier.  The scenario and dialogue is by Abel Gance and Claude Vermorel and the music composed by Arthur Honegger. Honeggers score for the film (H. 166 in his catalogue of works) consists of around 50 minutes of music for chorus and large orchestra. 
 1961 colour version directed by Pierre Gaspard-Huit    both with the same title.

== Cast ==
* Fernand Gravey as the Baron de Sigognac (later Capitaine Fracasse)
* Assia Noris as Isabelle
* Jean Weber as the Duc de Vallombreuse
* Alice Tissot as Dame Léonarde
* Vina Bovy as Séraphine
* Maurice Escande as the Marquis de Bruyères 
* Roland Toutain as Scapin 
* Lucien Nat as Agostin 
* Mona Goya as the Marquise de Bruyères 
* Paul Œttly as Matamore 
* Jacques François as the Chevalier de Vidalenc, friend of Vallombreuse
* Mary Lou as Yolande de Foix
* Jean Fleur as Blazius
* Nino Costantini as Léandre
* Josette France as Zerbine
* Roger Blin as Fagotin
* Paul Mondollot as Pierre

== Synopsis ==
Set in the mid 17th century, the young Baron de Sigognac lives alone, ruined, in his castle. A troupe of travelling players stop in the castle ward and are offered hospitality by Sigognac, who remarks the beauty of the actress Isabelle. He decides to go with the troupe on their journeying.

The old leader of the troupe, Matamore dies and Sigognac replaces him using the pseudonym Capitaine Fracasse. He is enthusiastically received but has a rival for the attentions of Isabelle in the Duc de Vallombreuse. They fight a duel in a cemetery in Poitiers.

Afterwards, Vallombreuse manages to abduct Isabelle and imprison her in his castle. Sigognac-Fracasse comes to the rescue and another duel ensues. Then it is revealed that Isabelle is in fact the rich descendant and the sister of Vallombreuse. Sigognac finally marries her.

==References==
 

 

 
 
 
 
 
 
 
 

 