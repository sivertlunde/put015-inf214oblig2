A Face in the Crowd (film)
{{Infobox film
| name           = A Face in the Crowd
| image          = Afaceinthecrowdposter.jpg
| caption        =
| director       = Elia Kazan
| producer       = Elia Kazan
| based on       =  
| starring       = Andy Griffith Patricia Neal Anthony Franciosa Walter Matthau Lee Remick
| music          = Tom Glazer
| cinematography = Gayne Rescher Harry Stradling Sr.
| editing        = Gene Milford
| distributor    = Warner Bros.
| released       =  
| runtime        = 125 minutes
| country        = United States
| language       = English
| budget         =
}}

A Face in the Crowd is a 1957 film starring Andy Griffith, Patricia Neal and Walter Matthau, directed by Elia Kazan.  The screenplay was written by Budd Schulberg, based on his short story "Your Arkansas Traveler", part of his 1953 short story collection, Some Faces in the Crowd.

The story centers on a drifter named Larry "Lonesome" Rhodes (Griffith, in a role starkly different from the amiable Andy Taylor (The Andy Griffith Show)|"Sheriff Andy Taylor" persona), who is discovered by the producer (Neal) of a small-market radio program in rural northeast Arkansas. Rhodes ultimately rises to great fame and influence on national television.

The film launched Griffith into stardom, but earned mixed reviews upon its original release. Later decades have seen reappraisals of the movie, and in 2008 A Face in the Crowd was selected for preservation in the United States National Film Registry by the Library of Congress as being "culturally, historically, or aesthetically significant".

==Plot==
In late 1950s United States|America, a drunken drifter, Larry Rhodes (Andy Griffith), is plucked out of a rural Arkansas jail by Marcia Jeffries (Patricia Neal) to sing on a radio show at station KGRK.  His raw voice, folksy humor and personal charm bring about a strong local following, and he lands a television show in Memphis, Tennessee under the stage name "Lonesome" Rhodes, given to him on a whim by Jeffries.

With the support of the shows staff writer Mel Miller (Walter Matthau) and Jeffries, the charismatic Rhodes ad libs his way to Memphis area popularity. When he pokes fun at his sponsor, a mattress company, they initially pull their ads—but when his adoring audience revolts, burning mattresses in the street, the sponsor discovers that Rhodes irreverent pitches actually increased sales by 55%, and returns to the air with a new awareness of his power of persuasion.  Rhodes also begins an affair with Jeffries.

An ambitious office worker at the mattress company, Joey DePalma (Anthony Franciosa), puts together a deal for Rhodes to star in his own show in New York City. The sponsor is Vitajex, an energy supplement which he ingeniously pitches as a yellow pill which will make men energetic and sexually powerful. Rhodes fame, influence and ego balloon. Behind the scenes, he berates his staff and betrays Jeffries by eloping with a 17-year-old drum majorette (Lee Remick). The onetime drifter and his new bride move into a luxury penthouse, while a furious Jeffries demands more money and credit for her role in Rhodes success.

The sponsors CEO introduces Rhodes to a senator named Fuller whose presidential campaign is faltering. Under Rhodes tutelage as media coach, the senator gains the lead in national polls.  But Rhodes life begins to unravel as his amoral dealings with the people closest to him have placed his career trajectory on a collision course with their festering wounds. A woman (Kay Medford) turns up claiming to be his legitimate wife. He also goes home early to find his agent and young wife ending a tryst.  He returns to Marcia Jeffries to proclaim that with the election victory assured, he will soon serve on the Presidents cabinet as "Secretary For National Morale", as a part of his organization called "Fighters for Fuller". He also expects Jeffries to resume her romance with him. She runs away.

Miller tells Jeffries he has written an exposé about Rhodes, entitled "Demagogue in Denim", and he has just found a publisher. Ultimately, Rhodes ascent into fame and arrogance begins to turn on him. DePalma threatens to reveal Rhodes own secrets if the affair with the young wife is made public, claiming that he and Rhodes are now part of the same corruption. Rhodes is stuck with his business partner, but cruelly dumps his cheating wife.

The final blow is delivered by the one who has loved Rhodes the most and been most injured by his selfishness: Marcia Jeffries. At the end of one of Rhodes shows, the engineer cuts the microphone and leaves Jeffries alone in the control booth while the shows credits roll. Millions of viewers watch (in what initially is silence) their hero Rhodes smiling and seeming to chat amiably with the rest of the cast. In truth, he is on a vitriolic rant about the stupidity of his audience. In the broadcast booth, Jeffries reactivates his microphone, sending his words and laughter over the air live. A sequence of television viewers is shown to react to Rhodes description of them all as "idiots, morons, and guinea pigs".

Still unaware that his words have gone out over the air waves (with thousands of angry calls to local stations and the network headquarters), he departs the penthouse studio in a jovial mood and prophetically tells the elevator operator that he is going "all the way down". As the elevator numbers go down to 0, the ratings of the show go down as well, due to Rhodes insults.

Rhodes arrives at his penthouse, where he was to meet with the nations business and political elite.  Instead he finds an empty space, except for a group of black butlers and servants, by whom, in desperation, he demands to be loved. When they fail to respond, Rhodes dismisses all of them.  Rhodes calls the studio and Jeffries listens to him rant as he threatens to jump to his death from the penthouse. Jeffries, who has been silent, suddenly screams at Rhodes, telling him to jump and to get out of her, and everybodys, life. Miller asks her angrily why she did not tell Rhodes the whole truth.

Jeffries and Miller go to the penthouse and Rhodes is drunk and disconnected from reality. He shouts folksy platitudes and sings at the top of his lungs while his longtime flunky Beanie (Rod Brasfield) works an applause machine—Rhodes own invention—to replace the cheers, applause, and laughter of the audience that has abandoned him. When he vows to get revenge on the TV studios engineer, Jeffries admits it was she who betrayed him. She demands he never call her again, and Miller tells Rhodes that life as he knew it is over.

Miller bemoans the fact that Rhodes is not really destroyed at all. Both the publics and the networks need for Rhodes, will, "after a reasonable cooling off period" of remorse and contrition, he predicts, return Rhodes to the public eye, but never to his previous height of power and success. Rhodes ends up screaming from the window of his penthouse for Marcia Jeffries to come back as she leaves in a taxi with Miller, while a Coca-Cola sign continuously flashes off and on.

==Cast==
* Andy Griffith as Larry "Lonesome" Rhodes
* Patricia Neal as Marcia Jeffries
* Anthony Franciosa as Joey DePalma
* Walter Matthau as Mel Miller
* Lee Remick as Betty Lou Fleckum
* Percy Waram as Gen. Haynesworth Paul McGrath as Macey
* Rod Brasfield as Beanie
* Marshall Neilan as Senator Worthington Fuller
* Alexander Kirkland as Jim Collier Charles Irving as Luffler Howard Smith as J.B. Jeffries
* Kay Medford as First Mrs. Rhodes
* Big Jeff Bess as Sheriff Big Jeff Bess Henry Sharp as Abe Steiner
* Cara Williams as Nurse Mike Wallace, Earl Wilson, and Walter Winchell in cameo appearances as themselves

==Real-life inspirations==
  

Aspects of the Lonesome Rhodes character were likely inspired by 1940s and 1950s CBS radio-TV star Arthur Godfrey.  The scene where Rhodes spoofs his sponsor on TV in Memphis echoes Godfreys reputation for kidding his own advertisers. Godfrey claimed he would not advertise products he did not believe in, and routinely ridiculed both the sponsors stodgy ad copy and occasionally, the companies executives. The more Godfrey did this, the more sales increased. Godfreys immense popularity began to deflate following his 1953 on-air firing of singer Julius LaRosa, which opened the gradual exposure of his less lovable, often controlling off-camera personality. Though he remained on radio, TV and even films for several years afterward, Godfreys mass appeal and popularity had passed its apex, and were never the same. At one point in the film, Rhodes telegraphs Jeffries that he is going to miss a broadcast and requests that Godfrey fill in for him.
 Red Scare", although Faulk was never really a national figure. 

Screenwriter Schulberg himself claimed to have based a significant part of the characters facade on that of Will Rogers, adding a distinctively un-Rogers-like level of amorality and cruelty. In Richard Schickels 2005 biography of director Elia Kazan, Schulberg explained that he had met Will Rogers, Jr. during the latters run for Congress and discussed his famous father. The younger Rogers supposedly told Schulberg that his father socialized with the very establishment types he mocked in his public pronouncements, adding that his father was actually a political reactionary in private life.  

The films scene in which Marcia turns up the volume on the studio, with Rhodes unaware that his voice can now be heard by his viewers, was inspired by the debunked urban legend of radios "Uncle Don" show where he thought that he was off the air.  His words: "This is Uncle Don, saying good night. Were off. Good, that will hold those little bastards."
 Mike Wallace, Earl Wilson, and Walter Winchell.

Two cast members had genuine ties to the country music field. Rod Brasfield was a popular Grand Ole Opry comedian in the 1950s, known for his own performances and onstage comic banter with legendary Opry comic Minnie Pearl. Big Jeff Bess, who portrayed the Sheriff, was a Nashville-based country music performer on radio station WLAC there, leading a group called "Big Jeff and His Radio Playboys", which recorded for Dot Records and included guitarist Grady Martin.  Bess was, for a time, the husband of Tootsie Bess, longtime owner of Nashvilles famous downtown bar Tootsies Orchid Lounge, a hangout for country entertainers.

==Production==
Most of the films interiors were shot in New York at the Biograph Studios in the Bronx.  This was preceded by location shooting in Memphis and in Piggott, Arkansas—where Rhodes meets Betty Lou.

Contemporary newspapers reported an early 1956 trip by Kazan to Sarasota, Florida to confer with Schulberg.  Late in April, columnist Walter Winchell noted that Andy Griffith would depart the cast of his Broadway show No Time for Sergeants at the end of July, vacation for a month, and then begin shooting with Kazan. Kazan and Schulberg spent much of July and August 1956 in Memphis and in Arkansas,  and Patricia Neals involvement would be announced by early August. Both Griffith and Lee Remick made their film debuts in Face. 

The most involved location shoot was in Piggot, Arkansas (the fair and baton-twirling competition scenes). Five thousand extras were sought, to be fed and paid $1 hourly for a mid-August days work. Sixty baton twirlers were rounded up from NE Arkansas and SE Missouri, and musicians from six different high school bands were assembled.  Remick reported spending two weeks in Piggott living with teen twirler Amanda Robinson and her family, working on her twirling and local accent.  At the Piggott location shoot some 380 dogs were assembled from Missouri and Arkansas  for the scene following Rhodes first mass-action call on his audience: to take their dogs to the home of a local sheriff who was running for higher office – Rhodes opining that people should first find out if a candidate is worthy of the office of "dog catcher".

Shooting in New York included 61 sets at Biograph Studios as well as some exteriors. Anthony Franciosa, eager to work with Kazan, had turned down a more lucrative offer to appear in MGMs The Vintage.  Writer Schulberg remained involved throughout: "I went on a trip in 1955 to scout a location in Arkansas, and Ive been on the set every day since shooting started in August  ." 

In stage performance, Griffith noted, he woud work gradually up to his most intense moments, but needed to conjure that up spontaneously when shooting such scenes for Kazan. In some instances, he asked to have a few discarded chairs available to destroy, in order to work up his rage before filming. 

==Critical reception==
Upon its original release, A Face in the Crowd earned somewhat mixed reviews, one of them from Bosley Crowther of The New York Times. Though he applauded Griffiths performance ("Mr. Griffith plays him with thunderous vigor ..." ), at the same time, he felt that the character overpowered the rest of the cast and the story. "As a consequence, the dominance of the hero and his monstrous momentum ... eventually become a bit monotonous when they are not truly opposed."  Crowther found Rhodes "highly entertaining and well worth pondering when he is on the rise", but considered the ending "inane". 

One critic who had only praise for the movie was Francois Truffaut; in his review in Cahiers du Cinéma, he called the film "a great and beautiful work whose importance transcends the dimensions of a cinema review". 

Over the decades critical opinion of the film has warmed considerably. As of mid-2011, A Face in the Crowd had a 91% "fresh" rating on Rotten Tomatoes, based on 23 reviews. 

==See also==
*The Great Man

==References==
{{Reflist|refs=
   
 Richard Schickel (2005). Elia Kazan: A Biography. New York: HarperCollins Publishers. ISBN 978-0-06-019579-3. 
 Leonard Lyons, "The Lyons Den" (syndicated column), Long Beach, CA: Independent, April 12, 1956. 
 "Arkansas Movie Sets Production for Near Future", Kingsport, TN: Kingsport Times, July 9, 1956. 
 "Want to be in Movies? Extras Needed in Piggott", Blytheville, AR: Blytheville Courier News, August 8, 1956. 
 Erskine Johnson, "Hollywood", Ardmore, OK: Daily Ardmoreite, April 21, 1957. 
 "Dogs in Demand", Aiken, SC: Aiken Standard and Review, August 20, 1956. 
 Bob Thomas, "Hard Knocks and Actors Studio", Long Beach, CA: Press Telegram, December 27, 1956. 
 Rob Burton (AP), "Novelist Stays for Face Film", Amarillo Globe-Times, December 31, 1956. 
 Hal Boyle, "He Fights Furniture Before Acting as If in a Rage", Lumberton, NC: The Robesonian, November 16, 1956. 
   
   
   
   
}}

==External links==
 
*  
*  
*  
*  
*  
* Archive of American Television interview of Andy Griffith about the film:  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 