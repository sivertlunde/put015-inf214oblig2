Tropical Fish (film)
{{Infobox film| name = Tropical Fish 熱帶魚 Rèdài Yú
| image = Tropical Fish (film).png
| caption = Taiwanese DVD cover
| director = Chen Yu-hsun
| writer = Chen Yu-hsun
| producer = Chung Hu-pin
| Associate Producer = Wang Shau-di
| starring = Lin Cheng-sheng Lin Chia-hung Wen Ying
| music = Wu Bai & China Blue
| cinematography =
| editing =
| studio = Central Pictures Company
| released = 1995
| runtime = 108 minutes Taiwan
| language = Taiwanese Hokkien|Taiwanese, Mandarin Chinese
}}
 
  1995 Taiwanese comedy-drama film written and directed by Chen Yu-hsun.  It is an example of New Taiwanese Cinema. 

==Plot==
 
The movie tells the story of Liu Chih-chiang (劉志強, played by Lin Chia-hung  ), a disaffected Taipei high school student who is kidnapped and taken by Ah Ching (阿慶, played by the filmmaker Lin Cheng-sheng) to rural Chiayi County in southern Taiwan.

==Locations==
Chih-chiangs school scenes were filmed at Bailing Senior High School (百齡高中) in Shilin District|Shilin, Taipei, while several other scenes were filmed in Daan District, Taipei|Daan District.   The kidnappers hold Chih-chiang in Dongshi, Chiayi|Dongshi, a coastal Chiayi County township noted for its oyster production.  The end of the film shows the business district of Dunhua Road in Taipei.

==Music==
In addition to the music score, two songs by Wu Bai & China Blue, "This Continuous Sinking" (繼續墮落) (from their 1994 album Wanderers Love Song) and "Go to the Graveyard" (墓仔埔也敢去) are featured in the movie.  Different (live music|live) versions of both songs were released on Wu Bai & China Blues 1995 live album 伍佰的LIVE.

==Awards==
At Taiwans 1995 Golden Horse Film Festival, Chen Yu-hsun won the award for Best Screenplay Originally Written for the Screen and Wen Ying (文英) won Best Supporting Actress for her role as Ah Yi (阿姨).   Chen Yu-hsun was also nominated for a Golden Leopard for the film at the 1995 Locarno International Film Festival. 

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 