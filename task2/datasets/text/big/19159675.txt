You Know What Sailors Are (1954 film)
{{Infobox film
| name           = You Know What Sailors Are
| image size     = 
| image	=	You Know What Sailors Are FilmPoster.jpeg
| alt            = 
| caption        = 
| director       = Ken Annakin
| producer       = Peter Rogers Julian Wintle
| writer         = Roger Hyams (novel), Peter Rogers (writer)
| screenplay     = 
| story          = 
| narrator       = 
| starring       = 
| music          = Malcolm Arnold
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       = 4 November 1954  
| runtime        = 89 minutes
| country        = United Kingdom
| language       = English
}}
 1954 United British comedy film directed by Ken Annakin from a screenplay by Peter Rogers. It starred Donald Sinden, Michael Hordern, Bill Kerr, Dora Bryan and Akim Tamiroff.

== Cast ==
* Akim Tamiroff as President of Agraria
* Donald Sinden as Lt. Sylvester Green Sarah Lawson as Betty
* Naunton Wayne as Captain Owbridge
* Bill Kerr as Lt. Smart
* Dora Bryan as Gladys Martin Miller as Prof. Hyman Pfumbaum
* Michael Shepley as Admiral
* Michael Hordern as Captain Hamilton
* Ferdy Mayne as Stanislaus Voritz of Smorznigov
* Bryan Coleman as Lt. Comdr. Voles
* Cyril Chamberlain as Stores Officer
* Hal Osmond as Stores Petty Officer
* Peter Arne as Ahmed
* Shirley Lorimer as Jasmin

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 


 
 