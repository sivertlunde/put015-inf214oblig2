Bloodstained Memoirs
{{Infobox film
| name = Bloodstained Memoirs
| image = Bloodstained_Memoirs_Poster.jpg
| caption = Promotional Poster for Bloodstained Memoirs
| producer = David Sinnott Christian Cage
| music = Tigers and Monkeys
| released =  
| country = United States
| language = English
}} documentary released online via an official stream in 2009. It is compiled by interviews featuring wrestlers synonymous with different eras in wrestling (1970s to 2000s) and different wrestling regions (United States, Canada, Japan and Mexico). Production was filmed in the United Kingdom|UK, U.S., France, Japan and Italy. When interviewed for Fighting Spirit magazine|Fighting Spirit magazine, director David Sinnott stated, "I dont intend this film to be negative. Its too easy to sell people bad news."   

In October 2011, the documentary was made available on DVD.      

==Background information== ITV Celebrity Wrestling Coach Joe Legend. Two of these dramatic scenes were released for the first time on the DVD release. The drama concept was dropped in favour of extended interviews.   

==Cast== Christian Cage World Wrestling shoot style interviews conducted with Último Dragón and Muto in the Western world. The interview questions are voiced over by British wrestling commentator Dean Ayass.

==Leaked YouTube video==
In March 2007, footage of the film featuring four minutes of interviews with Piper and Greenwald appeared on YouTube, claiming that it was hacked footage from the former official website. It is debated whether this was the true case or whether the video was put up for publicity. Over 160,000 people watched the video, and the news story was picked up by a number of publications, including Power Slam magazine which adds cause to the debate that it was in fact a planted video. In time the video even made its way to the official website.    Several topics were discussed in the video, including the death of Bruiser Brody.   

The video was taken down in late May 2009 for unknown reasons. The YouTube video proved to contain no footage which was used in the actual production itself.

==Legal issues==
David Sinnott, executive producer of Bloodstained Memoirs, released a statement on July 30, 2008 through the official Bloodstained Memoirs MySpace page stating that documentary was facing a delay because post production services delivered to the documentary had been below standard. The company responsible for the post production services, Online Post Production Ltd., were taken to court as a result of this. The company was owned by Hilary Lawson, Vice Chair of the Forum for European Philosophy at the London School of Economics and Political Science and author of Epistemic closure|Closure.      

On October 24, it was revealed that the producers of Bloodstained Memoirs won the case in court for an undisclosed financial sum, and that they would be moving on to another post production facility, stating that a release date would not be far away.   

==Website release== Dixie Carter had given the producers footage to use from TNAs video library.
 The Sun teasing the phrase: "The Wait Is Almost Over."

Following several more trailers, Bloodstained Memoirs became available to stream on November 23, 2009, via a subscription on the official website.      

==Reception== Empire magazine The Sun remarked that it was "excellent." 
     Inside Pulse summarised that "what is put together here is pretty phenomenal" and Film Monthly summarised, "If wrestling is an art, then it’s fun to hear the process from the artists."       

Several wrestling personalities who were not associated with the production also spoke out publicly to compliment it, such as Paul Bearer, Sonjay Dutt, Missy Hyatt and Lanny Poffo. Something which in itself was noted by sources such as The Miami Herald.   

The film, however, has not escaped criticism. Fighting Spirit magazine|Fighting Spirit magazine called it, “Something of a missed opportunity.” Style Weekly proclaimed, “interviews tend to jump into nitty-gritty wrestling knowledge without setting up scenarios so that casual fans will get lost in the details.”    
 application was released in September 2011 featuring unreleased footage of the documentary.      

A Kindle book titled "The Making of Bloodstained Memoirs" was released in May 2012. The book is authored by the films director and looks at the making of the production. The book reached #1 in both the American and the United Kingdom Amazon stores within several categories including Wrestling, Film & Television and Direction & Production.      

==DVD release==
In early October 2011, a new trailer was put online with the news that Bloodstained Memoirs was being released for the first time ever on DVD. The DVD was released on October 17, 2011, reaching the top 10 of the Amazon.com sports chart.   

A Special Edition DVD was released on October 16, 2012. This version includes footage of a match between Foley and Snuka, as well as additional interviews with Christy Hemme, Juventud Guerrera, and Tigers and Monkeys (the band that contributed to the films soundtrack). In addition, an extended interview with Piper is also included, plus a discussion on the film by Aftermath Wrestling hosts Arda Ocal and Jim Korderas. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 
 