Money Talks (1972 film)
{{Infobox film
| name           = Money Talks
| image          = Money Talks (1972 film) poster.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Allen Funt
| producer       = Allen Funt 
| writer         =
| starring       = 
| music          = Mark Barkan
| cinematography = Gil Geller 
| editing        = Jan Welt 
| studio         = Allen Funt Productions
| distributor    = United Artists
| released       =  
| runtime        = 81 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}

Money Talks is a 1972 American documentary film directed by Allen Funt. The film was released on August 30, 1972, by United Artists.  {{cite web|url=http://www.nytimes.com/movie/review?res=9F06E3D9143BE43BBC4953DFBF668389669EDE|title=Movie Review -
  Money Talks - Screen: Money Talks:Film by Allen Funt Is in Familiar Pattern |work=The New York Times|accessdate=31 October 2014}} 

==Plot==
 

== Cast ==	 
*Muhammad Ali as Himself
*David McHarris as Himself / Tap dance
*Marian Mercer as Herself / Waitress
*Henny Youngman as Himself
*Jack London as Himself / Money eater
*Joseph R. Sicari as Himself / Dog owner 
*Jackie Bright as Himself / Wheeler dealer
*Ann Myles as Herself
*Guy King as Himself / Rest room attendant
*Peter Hock as Himself
*Karen Fund	as Herself / Money dropper
*Erin Peeters as Herself / Panhandler
*Tony Bell as Himself / Boy tipper
*Ira Kosloff as Himself
*Robbie Manning as Himself
*Juliet Funt as Herself
*Lulu-May Brown as Herself
*Joya Gingold as Herself
*Rubin Pochtar as Himself
*Elizabeth Fisher as Herself
*Jack Avidon as Himself
*Norman Hilliard as Himself
*Eva Barthfeld as Herself
*Michael C. Roberts as Himself
*Arthur Miller as Himself
*John L. Smith as Himself
*Cristobal Correa as Himself 

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 