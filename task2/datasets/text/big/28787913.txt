Runners (film)
{{Infobox film
| name           = Runners
| image          = 
| image_size     = 
| caption        = 
| director       = Charles Sturridge
| writer         = Stephen Poliakoff
| narrator       = 
| starring       = Kate Hardie
| producer       = Barry Hanson
| music          = George Fenton
| cinematography = Howard Atherton
| editing        = 
| studio         = 
| distributor    = 
| released       = August 1983
| runtime        = 107 min.
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
| website        = 
| amg_id         = 
}}
Runners is a 1983 film written by Stephen Poliakoff, and directed by Charles Sturridge. It stars Kate Hardie and James Fox.  

==Synopsis==

An English father heads for London in search of his missing teenage daughter.

==Cast==
*Kate Hardie as Rachel Lindsay
*James Fox as Tom Lindsay
*Jane Asher as Helen Eileen OBrien as Gillian Lindsay
*Ruti Simon as Lucy Lindsay

==References==
 

==External links==
* 

 

 
 
 
 
 
 

 