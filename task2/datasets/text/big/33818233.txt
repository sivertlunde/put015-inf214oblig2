Weekender (film)
 
 
{{Infobox film
| name           = Weekender
| image          = 
| alt            = 
| caption        = 
| director       = Karl Golden
| producer       = Ian Brady Stephen Salter Robert Walak
| writer         = Chris Coghill Jack OConnell Zawe Ashton
| cinematography = John Conroy
| music          = James Edward Barker
| studio         = Benchmark Films
| distributor    = Momentum Pictures
| released       =  
| runtime        = 90 minutes  
| country        = United Kingdom
| language       = English
}} Jack OConnell, Sam Hazeldine, and  Zawe Ashton.

==Plot==
1990: The rave scene has arrived from Ibiza and warehouse parties are exploding across the United Kingdom, bringing phenomenal wealth to the organisers. In Manchester, best mates Matt and Dylan are in their early twenties and long to be more than just punters. As the government moves to outlaw the scene, its now or never and they quickly rise through the ranks to join the promoting elite. They are taken on a wild journey from the exclusive VIP rooms of London clubs to the outrageous parties in Ibiza super-villas and the hedonism of Amsterdam. Its everything they dreamed of and more. But as their success continues to grow, they attract a more dark and sinister world. Matt and Dylan start to drift apart as they are forced to question the dreams they set out to achieve and their once solid friendship.

==Cast== Jack OConnell as Dylan
* Zawe Ashton as Sarah
* Stephen Wight as Gary Mac
* Henry Lloyd-Hughes as Matt
* Sam Hazeldine as Maurice
* Emily Barclay as Claire
* Dean Andrews as Sargent Thompson
* Ben Batt as John Anderson
* Fergus ODonnell as Tommy
* Richard Riddell as Craig
* Craig Izzard as Doorman
* Perry Fitzpatrick as Chris
* Tom Meeten as Captain Acid

==Reception==
Weekender received generally negative reviews, currently holding a 9% rating on review aggregator website Rotten Tomatoes based on 11 reviews.  On Metacritic, based on 5 critics, the film has a 35/100 rating, signifying "generally unfavorable reviews". 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 
 
 
 