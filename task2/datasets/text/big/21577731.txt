H (1990 film)
{{Infobox Film
| name = H
| director = Darrell Wasyk
| producer = Darrell Wasyk Stavrose C. Stavrides
| writer = Darrell Wasyk
| starring = Pascale Montpetit Martin Neufeld Ingrid Veninger Bruce Beaton
| music = Rob Carroll
| cinematography = Gerald Packer
| editing = Tom McMurty
| distributor =   Alliance Releasing International sales Domino Film and Television International Ltd.
| released = December, 1990
| runtime = 97 min
| country = Canada
| language = English
| budget = $25,000
}}
H is a 1990 Canadian film written, directed, and produced by Darrell Wasyk.

==Plot summary==
H is about two heroin addicts, Michele, Pascale Montpetit and Snake, Martin Neufeld, who struggle to withdraw from the drug. They do it “cold turkey”. Snake nails the apartment door shut: they are determined to come clean. Michele awakens to discover she has been “betrayed” by her lover and is trapped. Barricaded in their apartment, they become each other’s hostage. As the days go by, their resolve ebbs and flows as they dig deeper and deeper into themselves. The hook to heroin addiction is the false sense of immunity to the state at large but as withdrawal starts, this cocooned existence abruptly unravels. Reality sharpens, intensifies, and re-awakens. Sensations flare, as Michele and Snake scrape at the walls, the camera becomes a monitor, never once leaving the apartment. When almost totally withdrawn, Snake discovers a forgotten stash. They are faced with the ultimate question: “Do we really have control over our addictions?”

==Cast==
* Pascale Montpetit - Michele
* Martin Neufeld - Snake
* Bruce Beaton - Franco
* Ingrid Veninger - Kathleen

==Production==
The movie was shot in Toronto, Ontario on a meagre budget of only $25,000. There are only two principal characters in one location.

==Festivals==
*Berlin International Film Festival October 1990.
*Montreal World Film Festival August 23-September 3, 1990.
*Toronto International Film Festival September 11, 1990.
*Vancouver International Film Festival October 1990.
*Independent Feature Film Market, New York City October 2–12, 1990.
*Palm Springs International Film Festival January 8–13, 1991.
*Cannes Film Festival (market) May 9–20, 1991.
*Locarno International Film Festival (competition) August 7–16, 1991.

==Awards==

At the 1990 Toronto International Film Festival, then called the Festival of Festivals, it won the Toronto-City Award for Best Canadian Feature Film,  and at Vancouver International Film Festival it won the Best Canadian Screenplay Award.  At the 1991 Locarno International Film Festival  the film won two prizes, the Leopardo di Bronzo, Terzo premio del Festival, and the 1st prize offered by the Department for Education, Culture & Sport. At the 12th Genie Awards Pascale Montpetit won a Best Actress for her portrayal of Michele. It was also nominated for Best Director and Best Original Screenplay.

==References==
 

==External links==
* 
* 
* 
* 
* 
* 

 
 
 