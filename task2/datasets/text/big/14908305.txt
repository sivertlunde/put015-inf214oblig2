De Scheepsjongens van Bontekoe (film)
{{Infobox Film
| name           = De Scheepsjongens van Bontekoe
| image          = 
| image_size     = 
| caption        = 
| director       = Steven de Jong
| producer       = 
| writer         = 
| narrator       = 
| starring       = 
| music          = 
| cinematography = 
| editing        = 
| distributor    = 
| released       = 11 November 2007
| runtime        = 
| country        = 
| language       = 
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}}

De Scheepsjongens van Bontekoe (  family film, directed by Steven de Jong.

The film received a Golden Film for  .

The film is based on the book Java Ho!.

==Plot / differences with the book==
In the film three of the boys play the central roles: Hajo, Rolf, and Padde, while Harmen is here an adult. In the film after the ship wreck the four plus Hein Rol and two other men are in a lifeboat. They do not make a sail but row. They assume that Willem Bontekoe is dead. One of the men dies. The remaining six meet locals and trade with them. Rol and another man try to steal from the locals, after which the six are attacked. The three boys together with local girl Dolimah move on. After some time Dolimah returns, and the three reach Java. However, the danger is not over because the authorities want to kill Padde because the ship wreck was his fault. Bontekoe shows up, he has survived too, and saves Padde by lying that it is not him. They get a job on Bontekoes new ship on its voyage home. Hajo proudly brings his earnings to his poor mother.

==External links==
* 

 
 
 
 