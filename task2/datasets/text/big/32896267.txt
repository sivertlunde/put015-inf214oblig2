The Pioneers (1926 film)
 
{{Infobox film
| name           = The Pioneers
| image          = 
| image_size     =
| caption        = 
| director       = Raymond Longford
| producer       = Raymond Longford
| writer         = Lottie Lyell
| based on    = novel by Katharine Susannah Prichard
| narrator       =
| starring       = William Thornton Virginia Beresford
| music          =
| cinematography = Arthur Higgins
| editing        = Raymond Longford Arthur Higgins
| studio         = Australasian Films A Master Picture
| distributor    = Union Theatres
| released       = 5 June 1926 (Sydney) 
| runtime        = 8,000 feet (approx two hours) "Raymond Longford", Cinema Papers, January 1974 p51 
| country        = Australia
| language       = Silent film  English intertitles
| budget         = 
| preceded_by    = 
| followed_by    = 
}}

The Pioneers is a 1926 Australian silent film directed by Raymond Longford. The script had been written by Lottie Lyell but she had died by the time filming started. It was considered a lost film but some surviving footage from it has recently emerged. 

==Synopsis==
The story of a Scottish settler and his wife, Donald and Mary Cameron, who live in the Gippsland bush, with their son David. They adopt the daughter of an ex-convict and raise him as their own. The daughter and David Cameron fall in love, but she marries another man. 

==Cast==
*Virginia Beresford 
*William Thornton as David Cameron
*Robert Purdie as Donald Cameron
*Connie Martyn as Mary Cameron
*Augustus Neville
*George Chalmers
*W. Dummitt
*Big Bill Wilson
*Sydney Hackett 
*Phyllis Culbert

==Production==
Katharine Susannah Prichards novel had won a ₤1,000 prize in 1915 and had previously been filmed by Franklyn Barrett in The Pioneers (1916 film)|1916. 

It was directed by Raymond Longford who in September 1925 had accepted a position of director of productions at Australasian Films. He worked on several films for them but the association ended badly. The director complained that the cast of The Pioneers was forced upon him.

Filming took place on location near Gosford and at Australasians studios in Bondi Junction in early 1926.  During the shooting of one sequence, William Thornton was thrown from his horse and was seriously injured. Because they were so far from a town, first aid was performed by Longford himself, who had had medical training. Longford sewed four stitches into Thorntons head. 

==Reception==
The critic for the Sydney Morning Herald wrote that: The Bushwhackers". Its photography and settings are equal to the best American, and a vein of natural sincerity runs right through its acting. The story, too, is more definite. In fact, from the state of having practically no story at all, Mr. Longford has run to the other extreme and tried to bring in too much story, so that after one has been looking at the picture for nearly two hours new issues are still coming in, which would need still another half hour for their adequate solution... Mr. Longford himself seems to have realised that his spectators patience must be at an end here; for he has suddenly brought the play to a close and left all sorts of important things unexplained... If only The Pioneers could be wound up about half-way or two-thirds of the way through, so as to obviate all this trite melodrama, which has been put in obviously as a sap to the populace, it would stand as a landmark In the history of Australian motion pictures.  
Table Talk said the film "presents a vivid story of the old Colonial days in Victoria." 
 
==Proposed remake==
In 1932 Cinesound Productions announced plans to make a sound version of the novel but no film resulted. 

==References==
 

==External links==
* 
*  at National Film and Sound Archive
*  at AustLit
* 

 

 
 
 
 
 
 
 