Step Brothers (film)
{{Infobox film
| name           = Step Brothers
| image          = StepbrothersMP08.jpg
| caption        = Promotional poster
| director       = Adam McKay
| producer       = {{Plainlist |
* Jimmy Miller
* Judd Apatow}} 
| screenplay     = {{Plainlist |
* Will Ferrell
* Adam McKay
}}
| story          = {{Plainlist |
* Will Ferrell
* Adam McKay
* John C. Reilly
}}
| starring       = {{Plainlist |
* Will Ferrell
* John C. Reilly
* Richard Jenkins
* Mary Steenburgen Adam Scott
* Kathryn Hahn
}}
| music          = Jon Brion Oliver Wood
| editing        = Brent White
| studio         = {{Plainlist |
* Relativity Media The Apatow Company
* Mosaic Media Group
* Gary Sanchez Productions}}
| distributor    = Columbia Pictures
| released       =  
| runtime        = 98 minutes 105 minutes (unrated version)
| country        = United States
| language       = English Spanish
| budget         = $65 million 
| gross          = $128.1 million 
}}  buddy slapstick comedy film starring Will Ferrell and John C. Reilly.  The screenplay was written by Will Ferrell and Adam McKay, from a story written by them with Reilly. It was produced by Jimmy Miller and Judd Apatow, and directed by McKay.

The film was released on July 25, 2008, two years after the same group of men wrote, produced, and starred in another comedy,  .

==Plot==
39-year-old Brennan Huff (Will Ferrell) and 40-year-old Dale Doback (John C. Reilly) are two unemployed, middle-aged, spoiled, offensive, and self-centered men who still live with (and are reliant on) their parents. They have no intention of moving out or finding jobs and behave childishly. When Brennans mother Nancy (Mary Steenburgen) and Dales father Robert (Richard Jenkins) marry, Brennan and Dale are forced to live with each other as step brothers.
 Adam Scott), an arrogant and rude helicopter leasing agent, comes to visit with his oddly perfect family, he mocks their unsuccessful lives and entices Dale to punch him in the face, to which Dale unexpectedly complies. Brennan is awed that Dale was able to stand up to Derek since Derek embarrassed Brennan at a talent show years prior. Meanwhile, Dereks wife Alice (Kathryn Hahn), who is also resentful of Derek, finds Dales courage a turn on. Brennan and Dale use this incident to discover their many shared interests and develop a strong friendship.
 Klansman and Brennan pretending to be a corpse after dying of the alleged asbestos fibers in the house.

Dale and Brennan, in their attempts to start an entertainment company, "Prestige Worldwide", release their first music video for their song "Boats N Hoes" in front of their family at Dereks birthday party, which shows them accidentally wrecking Roberts boat. Angered to his limits, Robert refuses the investment and spanks Brennan after a heated verbal exchange between the two. Robert later walks out on Christmas Eve to go to the Cheesecake Factory for a drink and, upon his return later that evening, proclaims to Nancy that it was the "happiest he had been in months". The following day, at Christmas dinner, he announces his intention to divorce Nancy, causing Brennan and Dale to break down. 

Upon discovering that each blames the other for the divorce, the boys fight again but decide to go their separate ways. Brennan starts working for Dereks helicopter leasing firm and Dale works for a catering company.
 Por ti Volare." Derek (like the rest of the audience) is so moved by the performance that he and Brennan make amends.

Six months later, Robert and Nancy are reunited and move back into their old home, while Brennan and Dale form a successful entertainment company that runs karaoke events.

After the credits, Brennan and Dale confront Gardocki and his classmates, fighting and effortlessly beating many of the kids and causing the remainder to run away. Brennan and Dale then walk away, with the two lightly arguing about the latters drum set. 

==Cast==
 
* Will Ferrell as Brennan Huff
* John C. Reilly as Dale Doback
* Richard Jenkins as Robert Doback
* Mary Steenburgen as Nancy Huff-Doback Adam Scott as Derek Huff
* Kathryn Hahn as Alice Huff, Dereks wife
* Andrea Savage as Denise
* Rob Riggle as Randy
* Logan Manus as Chris Gardocki
* Lurie Poston as Tommy
* Elizabeth Yozamp as Tiffany
* Ken Jeong as Employment Agent
* Wayne Federman as Don
* Abigail Wagner as Erica
* Carli Coleman and Brandon T. Webb as the first homebuyers.
* Phil LaMarr as the Second Homebuyer Matt Walsh as Drunk Corporate Guy
* Seth Rogen as a sporting goods manager
* Gillian Vigman as Pam
* Horatio Sanz as the lead singer of "Uptown Girl", an 80s Billy Joel cover band
* Brady Scanlon as a "Boats N Hoes" music video extra.
 

==Critical reception==
Step Brothers received mixed reviews from critics. On Rotten Tomatoes, the film has a rating of 55%, based on 184 reviews, with an average rating of 5.5/10. The sites critical consensus reads, "The relentless immaturity of the humor is not a total handicap for this film, which features the consistently well-matched talents of Will Ferrell and John C. Reilly."  At the website Metacritic, which utilizes a normalized rating system, the film has a score of 51 out of 100, based on 33 critics, indicating "mixed or average reviews". 

Roger Ebert gave the film 1 1/2 out of 4 stars and stated, "When did comedies get so mean? Step Brothers has a premise that might have produced a good time at the movies, but when I left, I felt a little unclean". 

==Home media release== commentary track mostly in song, accompanied by Jon Brion; the track covers "the movie-making process   their characters’ offscreen lives" in remarks that range "from the inspired to the irritatingly prolonged, but when Ferrell and Reilly really get into a good groove, they’re actually funnier than the main feature." 

==Rap album==
Adam McKay announced on Twitter that production of a Step Brothers rap album featuring John C. Reilly and Will Ferrell had begun,  but later said that the rap album fell apart and will not be released. 

==Possible sequel==
Will Ferrell and John C. Reilly have talked about a sequel. Reilly had the idea.  

Adam McKay was also interviewed about the possible sequel. “We’re kicking around the idea of Step Brothers 2," he said. "We feel like there’s way more fat to be mined there. While it isn’t quite the legend that   is, it has built kind of a nice following. We think it could be a pretty fun one.” He added that Ferrell and Reillys characters would be mature and have jobs. "One of them’s married and has a kid. They’re still kind of goofballs but they’ve taken three or four steps. Then we have an idea for something happens that knocks him back to square one, and one of the brothers, John C. Reilly sort of instigates it, like ‘we can’t take this anymore.’ And things go really bad, their lives kind of fall apart. They have to pull it back together is sort of the basic structure." McKay has also said that ideas that were not used in the first film may be used in the sequel.  
 Empire in February 2014 and appeared to rule out a sequel to Anchorman 2 or Step Brothers saying "No, that’s the last sequel we’re gonna do. There’s nothing more fun to me than new characters and a new world. And now we’re releasing this alt version, we’re totally satisfied. No Anchorman 3." 

However, in an interview with Collider  http://collider.com/step-brothers-2-adam-mckay-the-big-short/#WCDPGyePVXmlYcJC.99
  posted on 21st October 2014, Adam McKay indicated the door was still open for a Step Brothers sequel at some point, while making clear it wasnt a short term development priority. 
 The Big Short is an adaptation but do some different stuff.  But who knows?  2-3 years, 3-4 years.  I mean the funny thing with Step Brothers is if those guys are in their 50’s it still works, so we could easily return to that, but for now no sequels.”

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*   ScoringSessions.com

 
 

 
 
 
 
 
 
 
 
 
 
 
 