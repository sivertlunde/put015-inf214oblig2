Special Delivery (1955 film)
 

{{Infobox film
| name           = Special Delivery
| image          = 
| image_size     = 
| caption        = 
| alt            =
| director       = John Brahm
| producer       =  .  Retrieved 14 October 2012.  Oskar Kalbus  (supervising producer)   Charles Münzel  (executive producer)   Stuart Schulberg  (producer)   Dwight Taylor  Géza von Radványi  (idea)  
| narrator       =  Bob Cunningham
| music          = Bernhard Kaun 	
| cinematography = Joseph C. Brun 
| editing        = Georges Klotz 	 
| studio         = Trans-Rhein Film 
| distributor    =  .  Retrieved 14 October 2012. 
Columbia Pictures  (United States)  
| released       = April 1955  ( . Retrieved 14 October 2012. 
| runtime        = 86 minutes 
| country        = West Germany  German  (filmed simultaneously in both languages)  
| budget         = 
| gross          = 
}}

Special Delivery (also known as Vom Himmel gefallen (in  .  Retrieved 14 October 2012.  directed by  .  Retrieved 14 October 2012. 

==Plot==
 
The film is set in a fictional Iron Curtain country. 

==Cast==
* Joseph Cotten as John Adams  
* Eva Bartok as Sonja Bob Cunningham as Captain Heinikan 
* René Deltgen as Kovak
* Gert Fröbe as Olaf

==Production==
 
It was filmed simultaneously in English- and German-language versions. 
 

==See also==
 
* 1955 in film
* List of comedy films of the 1950s
* List of German films 1945–1959
 

==References==
 

==External links==
*  
*  

 

 
 
 
 
 
 
 
 
 
 


 
 