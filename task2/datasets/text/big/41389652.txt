Aadi (2005 film)
{{Infobox film
| name = Aadi
| image =
| caption = 
| director = M. S. Ramesh
| writer = M. S. Ramesh
| producer = Jai Jagadish   Vijayalakshmi Singh
| starring = Auditya, Ramya, Avinash
| music = Gurukiran
| cinematography = Dasari Seenu
| editing = S Manohar
| studio  = Lakshmi Entertainers Pvt Ltd.
| released =  March 4, 2005
| runtime = 147 min
| language = Kannada  
| country = India
}}

Aadi is a Kannada romance film released in 2005 written and directed M. S. Ramesh. It was produced by Jai Jagadish and Vijayalakshmi Singh under Sri Lakshmi Entertainers Pvt Ltd banner. Auditya and Ramya appeared in lead roles. 

==Cast==
*Auditya as Aadi
*Ramya as Aishwarya
*Srinagar Kitty
*Avinash
*Srinivasa Murthy
*Rangayana Raghu
*Shobhraj

==Release==
The film was released on March 4, 2006 all over Karnataka. The film performed moderately well at the box-office.

==Soundtrack==
All the songs are composed and scored by Gurukiran.

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) !! Lyricist
|- Kaviraj
|- Nanditha || V. Manohar
|- Nanditha || Kaviraj
|- Kaviraj
|-
| 5 || "Enu Majaano" || Gurukiran || Bhangi Ranga
|-
|}

==References==
 

== External links ==
*Movie full info|http://kannadamoviesinfo.wordpress.com/2013/10/16/aadi-2005/

 
 
 
 
 
 


 