Before Dawn (film)
{{Infobox film
| name           = Before Dawn
| image          = "Before_Dawn"_(1933).jpg
| alt            = 
| caption        = 
| director       = Irving Pichel
| producer       = Merian C. Cooper
| screenplay     = Garrett Fort
| based on        = short story Death Watch by Edgar Wallace Dorothy Wilson Dudley Digges Gertrude Hoffman
| music          = Max Steiner
| cinematography = Lucien N. Andriot William Hamilton
| studio         = RKO Pictures
| distributor    = RKO Pictures
| released       =  
| runtime        = 60 minutes
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
 Dorothy Wilson, Dudley Digges Gertrude Hoffman. The film was released on August 4, 1933, by RKO Pictures.   

==Plot==
  Dying gangster Joe Valerie (Frank Reicher) reveals the hiding place of a million dollars in loot to his physician Dr. Cornelius (Warner Oland). The sinister Austrian doctor has designs on the money, but must first outwit detective Dwight Wilson (Stuart Erwin) and clairvoyant Patricia Merrick (Dorothy Wilson). The setting is an eerie, possibly haunted house in small town America, where the stash is hidden, and the bodies begin piling up.

== Cast ==
*Stuart Erwin as Dwight Wilson Dorothy Wilson as Patricia
*Warner Oland as Dr. Paul Cornelius Dudley Digges as Horace Merrick Gertrude Hoffman as Mattie
*Oscar Apfel as OHara
*Frank Reicher as Joe Valerie
*Jane Darwell as Mrs. Marble

==Critical reception==
TV Guide wrote, "very fast-paced whodunit thanks to Pichels taut direction, with the added stylistic flair of a glowing deathmask which floats through the mansions hallways. Penned by Wallace during his stay in Hollywood" ;  and The New York Times wrote, "like most of the British authors works, this particular ruddy specimen is blessed with vigor and imagination...There are enough killings and accidental deaths to satisfy the most ardent enthusiasts of such thrillers."  {{cite web|url=http://www.nytimes.com/movie/review?res=980DE3DE123FE63ABC4F52DFB6678388629EDE|title=Movie Review -
   - Dudley Digges and Warner Oland in a Pictorial Melodrama by the Late Edgar Wallace. - NYTimes.com|work=nytimes.com}} 

== References ==
 

== External links ==
*  

 
 

 
 
 
 
 
 
 
 


 