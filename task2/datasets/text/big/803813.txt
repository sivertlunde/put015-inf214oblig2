In the Bleak Midwinter (film)
 
 
{{Infobox film
| name           = In the Bleak Midwinter
| image          = 
| caption        =
| director       = Kenneth Branagh David Barron
| writer         = Kenneth Branagh
| starring       = Michael Maloney Richard Briers Hetta Charnley Joan Collins Nicholas Farrell Mark Hadfield Julia Sawalha
| music          = Jimmy Yuill
| cinematography = Roger Lanser
| editing        = Neil Farrell
| studio         = Castle Rock Entertainment
| distributor    = Sony Pictures Classics
| released       = 10 September 1995
| runtime        = 99 minutes
| country        = United Kingdom
| awards         =
| language       = English
| budget         =
| preceded_by    =
| followed_by    =
}}

In the Bleak Midwinter (also known as A Midwinters Tale) is a 1995 British romantic comedy written and directed by Kenneth Branagh. Many of the roles in the film were written for specific actors. This was the first film directed by Branagh in which he did not appear.

The film begins with a monologue by out-of-work actor Joe Harper (Michael Maloney) about his slow decline into depression. In an attempt to beat his depression, Joe volunteers to help try to save his sisters local church from land developers for the community by putting on a Christmas production of Hamlet, somewhat against the advice of his agent Margaretta. As the cast he assembles are still available even at Christmas and are prepared to do it on a profit sharing basis (that is, they may not get paid anything), he cannot expect – and does not get – the cream of the cream. But although they all bring their own problems and foibles along, something bigger starts to emerge in the perhaps aptly named village of Hope. This film encapsulates the hilarious and heartbreaking struggle of actor versus situation versus life, and often versus each other. It was shot in black and white for artistic effect. Branagh.  . INTERVIEW WITH KENNETH BRANAGH. 18 June 2006. 
 film version of Hamlet: Briers played Polonius, Farrell played Horatio, and Maloney played Laertes.

==Synopsis==
The story is set in Hope, Derbyshire, England. Its Christmastime, and tis the season to put on Shakespeares most cheery seasonal play: Hamlet. Joe Harper takes the project on as a Final Stand of sorts under the encouragement of his catty agent, advertising in the local newspaper for actors at a low price. From the rabble of actors both strange and untalented, he chooses the following:

Prince Hamlet|Hamlet: Joe Harper himself.

Ophelia: Nina Raymond, a shortsighted well-meaning space cadet who has been using low-calorie mayonnaise for lotion because she cant make out the label.

King Claudius: Henry Wakefield, a long-suffering and cynical old Englishman with a sharp tongue and a list of prejudices five miles (8&nbsp;km) long, forced to room with Queen Gertrudes player.
 Queen Gertrude: Terry DuBois, a flamboyantly gay theatrical actor with a preoccupation for costume and an estranged son.

  actor who foists his values on everyone else and attempts to distinguish his many characters with a variety of outrageous accents.

Horatio (Hamlet)|Horatio, Bernardo (Hamlet)|Bernardo: Not-so-subtle Carnforth Greville, an alcoholic in denial and a kindly bachelor with no memory for lines whatsoever.

Polonius, Marcellus (Hamlet)|Marcellus, and the First Gravedigger: Vernon Spatch, the ambitious techies techie, self-imposed marketing director, and unofficial documentarian of the proceedings. 

Set Designer: Fadge, just Fadge, specialising in new-age art and obscure statements about "air," "space," and "fog." Close friends call her "Fa".

==Cast==
*Michael Maloney as Joe Harper (Hamlet)
*Richard Briers as Henry Wakefield (Claudius, the Ghost, and the Player King)
*Hetta Charnley as Molly
*Joan Collins as Margaretta DArcy
*Nicholas Farrell as Tom Newman (Laertes, Fortinbras, and messengers)
*Mark Hadfield as Vernon Spatch (Polonius, Marcellus, and First Gravedigger)
*Gerard Horan as Carnforth Greville (Rosencrantz, Guildenstern, Horatio, and Barnardo)
*Celia Imrie as Fadge
*Jennifer Saunders as Nancy Crawford
*Julia Sawalha as Nina Raymond (Ophelia)
*John Sessions as Terry Du Bois (Queen Gertrude) Ann Davies as Mrs. Branch
*James D. White as Tim
*Robert Hines as Mortimer
*Allie Byrne as Tap Dancer
*Adrian Scarborough as Young Actor
*Brian Pettifer as Ventriloquist
*Patrick Doyle as Scotsman
*Shaun Prendergast as Mule Train Man
*Carol Starks as Audience Member
*Edward Jewesbury as Ninas Father
*Katy Carmichael as Mad Puppet Woman (uncredited)

==Reception==

===Critical response===
The film was well received by the majority of critics and currently holds an 89% "Fresh" rating on Rotten Tomatoes. 

Roger Ebert, noted critic of the Chicago Sun-Times, gave the film three out of four stars, praising the performances and Branaghs screenplay.  Online critic James Berardinelli highly praised the film, giving it three-and-a-half out of four stars, stating, "No current film maker appears to love and understand Shakespeare as well as Branagh, and never has his affection for the Bard been more apparent than here. This picture succeeds as a comedy, a satire, and even, to a certain extent, as a mild melodrama about choosing between a paycheck and the nourishment of the soul."  Berardinelli concluded, "Anyone who wondered about Branaghs future following Frankenstein (1994 film)|Frankenstein can set their concerns to rest. Hes back on top in the independent arena with A Midwinters Tale, a film that offers ninety-eight minutes of pure fun re-interpreting the phrase the plays the thing." 

===Accolades===
{| class="wikitable plainrowheaders" style="font-size: 95%;"
|- Award
! Category
! Recipients and nominees Result
|-
| rowspan=2 | Venice Film Festival
| Golden Osella
| Kenneth Branagh  (tied with Abolfazl Jalili for Det Means Girl) 
|  
|-
| Golden Lion
| Kenneth Branagh
|  
|-
|}

==References==
 

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 