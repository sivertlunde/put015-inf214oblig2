No Nukes (film)
{{Infobox Film
| name           = No Nukes
| image_size     = 
| image	=	No Nukes FilmPoster.jpeg
| caption        = 
| director       = Daniel Goldberg   Anthony Potenza
| producer       = Julian Schlossberg John Hall   James Taylor   Carly Simon   The Doobie Brothers   Bruce Springsteen & The E Street Band
| editing        = Neil L. Kaufman
| cinematography = Haskell Wexler
| distributor    = 
| released       = July 1980
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}} 1980 documentary documentary and John Hall being the key organizers of the event and guiding forces behind the film.  Also included were scenes of the organizers getting the event together, expounding upon the dangers of nuclear power, and staging an anti-nuclear rally at Battery Park in New York City.

==History== The River" Spinal Tap-like reputation for rock star verbal blundering.

No Nukes was released for home consumption on VHS and LaserDisc along the way, but as of 2012 not on DVD.  Two of Springsteens three numbers are available on his 2001 The Complete Video Anthology / 1978-2000 DVD, however.

The No Nukes (album)|No Nukes live album was also released in May 1980 from this event, although it contained varying musical contents from the film (generally, the artists biggest hits make it into the film but not the album, while some artists are on the album but not in the film).

==Performers and songs==
Those who performed in the film in order of appearance, 

at Madison Square Garden:
* "Mockingbird (1963 song)|Mockingbird" - James Taylor and Carly Simon
* "Runaway (Del Shannon song)|Runaway" - Bonnie Raitt The Times John Hall (portion)
* " " – Crosby, Stills & Nash (portions rehearsing in a quiet corner, then on-stage) Running on Empty" – Jackson Browne Before the Deluge" – Jackson Browne (portion)
* "Dependin On You" - The Doobie Brothers
* "What a Fool Believes" - The Doobie Brothers
* "Barrel of Pain" – Graham Nash
* "Your Smiling Face" – James Taylor
* "Stand and Fight" – James Taylor (portion, more played over end credits) We Almost Lost Detroit" - Gil Scott-Heron Our House" – Graham Nash (portions with his family, then on-stage) The River" – Bruce Springsteen & The E Street Band Thunder Road" – Bruce Springsteen & The E Street Band
* "Quarter to Three" – Bruce Springsteen & The E Street Band John Hall, Graham Nash, others

at Battery Park:
* "No More Nukes" – Joy Ryder/Avis Davis Band John Hall with Jackson Browne, Carly Simon, Graham Nash, Stephen Stills, Bonnie Raitt, others Get Together" – Jesse Colin Young with the previous assortment

Other famous personalities and celebrities are seen during the film, including Jane Fonda, Chaka Khan, Maggie Kuhn of the Gray Panthers, Ray Parker Jr., Ralph Nader, Steven Tyler of Aerosmith, Nicolette Larson, Phoebe Snow, and ubiquitous backup singer Rosemary Butler.

==External links==
* 
*  

 
 
 
 
 
 
 
 
 
 