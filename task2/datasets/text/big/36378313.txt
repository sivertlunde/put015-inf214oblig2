Dead Sushi
{{Infobox film
| name           = Dead Sushi
| image          = Dead-sushi.jpg
| alt            = 
| caption        = 
| director       = Noboru Iguchi
| producer       = Motohisa Nagata Yoichi Sakai Mana Fukui
| writer         = Noboru Iguchi
| starring       = Rina Takeda Shigeru Matsuzaki Kentaro Shimazu Asami Sugiura Demo Tanaka
| music          = 
| cinematography = 
| editing        = 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 91 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
  is a 2012 Japanese comedy horror film directed by Noboru Iguchi. The film involves Keiko (Rina Takeda) who is the daughter of a famous sushi chef. She leaves home for an inn where she is bullied by the president of Komatsu Pharmaceuticals. A Komatsu researcher arrives intent on revenge by creating a serum that turns fish on rice into killer sushi. Keiko teams up with the former sushi chef Sawada to fight off the creatures.

==Production==
Director and writer Noboru Iguchi decided to work on a film after the film Piranha 3D (2010) was popular in Japan. Iguchi was also influenced by the film Attack of the Killer Tomatoes by the idea of food attacking people.    The film contains a number of action sequences which Iguchi said he wanted to be more comedic than his previous films.  Yoshihiro Nishimura contributed to the special effects in the film. 

Dead Sushi is the first film to be released from Walker Pictures, the new producing and distribution offshoot of the Japanese talent agency Office Walker. 

==Cast==
*Rina Takeda as Keiko
*Shigeru Matsuzaki
*Kentaro Shimazu as Yamada 
*Asami Sugiura as Yumi Hanamaki 
*Demo Tanaka 
* Takamasa Suga as Nosaka 
* Takashi Nishina as Mr. Hanamaki 
* Yui Murata as Miss Enomoto

==Release==
Dead Sushi had its world premiere at the Fantasia Festival on July 22, 2012.    

==Reviews==
In a review for Dead Sushi and  , horror magazine Fangoria gave the film a 2 and a half rating out of four, stating that the film "kind of defy ratings; obviously they’re not high art, and the serious critic in me believes the score below is appropriate for these slight, outrageous confections. Yet they’re also diverting, raucous fun when seen in the company of like-minded viewers, so that rating can be augmented by half a skull (or more) if you’re able to watch these movies with a group".  Variety (magazine)|Variety gave the film a mixed review, noting that "the comic dialogue lacks zing and most perfs are pitched too high even for this type of nonsense, Takeda tones it down nicely and impressively kicks the butts of sleazy corporate types and seafood. Effects are super-cheesy; other tech work is passable." 

==References==
 

==External links==
*   
* 

 

 
 
 
 
 
 