Daddy's Gone A-Hunting (1969 film)
{{Infobox film
| name           = Daddys Gone A-Hunting
| image          =
| image_size     =
| caption        =
| director       = Mark Robson
| screenplay  = Larry Cohen Lorenzo Semple, Jr.
| narrator       = Paul Burke Scott Hylands
| music          = John Williams
| cinematography = Ernest Laszlo
| editing        = Dorothy Spencer
| distributor    = National General Pictures
| released       =  
| runtime        = 108 minutes
| country        = United States
| language       = English
| budget         =
| gross          = $2.9 million (US/ Canada rentals) 
| website        =
| amg_id         =
}} Paul Burke, and Scott Hylands.
 1925 silent film of the same name.

==Plot==
Cathy Palmer (White), a young British woman, comes to San Francisco to live. There she meets Kenneth Daly (Hylands), a relationship develops and she becomes pregnant, but when Cathy sees another side of Kenneths personality, she elects to break off their engagement and abort the pregnancy.

Sometime later, Cathy meets and marries Jack Byrnes (Burke), who has political ambitions. Kenneth, however, continues to be disturbed by the way Cathy ended their romance, and soon comes back into her life. After Cathy gives birth to Jacks baby, Kenneth demands that she kill the child as retribution for the one she aborted earlier.

==Cast==
* Carol White as Cathy Palmer Paul Burke as Jack Byrnes
* Mala Powers as Meg Stone
* Scott Hylands as Kenneth Daly
* James Sikking as Joe Menchell
* Walter Brooke as Jerry Wolfe
* Matilda Calnan as Ilsa Gene Lyons as Dr. Blanker
* Dennis Patrick as Dr. Parkington Barry Cahill as FBI Agent Crosley

==References==
 
==External links==
* 

 

 
 
 
 
 
 
 
 
 
 
 
 