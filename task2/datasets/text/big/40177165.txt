The Lion Men
 
 
 
{{Infobox film
| name           = The Lion Men 狮神决战
| image          = TheLionMenPoster.jpg
| border         = Yes
| alt            = 
| caption        = Theatrical release poster
| director       = Jack Neo
| producer       = 
| writer         = 
| starring       = Chen Tianwen Eva Cheng Tosh Zhang Wang Wei Liang
| music          = Tosh Zhang
| cinematography =
| editing        = Yim Mun Chong
| studio         = J Team
| distributor    = Golden Village Pictures SB
| released       =   
| runtime        = 130 minutes
| country        = Singapore Malaysia   
| language       = English Mandarin Hokkien
| budget         = 
| gross          = 
}}

The Lion Men ( ) is a Singapore action film directed by Jack Neo and starring Wang Wei Liang, Tosh Zhang, Noah Yap, Charlie Goh, and Maxi Lim. The main plot revolves around three lion dance groups pitting themselves against each other.

==Cast==
* Tosh Zhang   as Shishen/Supreme
* Wang Weiliang   as Mikey
* Eva Cheng as Xiao Yu
* Noah Yap 
* Charlie Goh 
* Maxi Lim 
* Chen Tianwen  as Master He

Ah Boys to Men star Ridhwan Azman was originally in talks to join The Lion Men, but he had to drop out because of his clashing army enlistment. {{cite news|url=http://www.zaobao.com.sg/culture/entertainment/stars/story20130805-237245|script-title=zh:梁导贺岁片《狮神决战》　陈志伟当舞狮顾问 
舞狮非‘歹仔’ 形象潮流化|date=5 August 2013|newspaper=Zaobao|language=Chinese}} 

==Plot==
Shi Shen (Tosh Zhang) is the top performer in the Tiger Crane Lion Dance Association, but feels restricted by Master Hes (Chen Tianwen) traditional mindset. He decides to take a group of disciples and forms his own lion dance troupe, which fuses hip hop and rock with lion dance movements. 

A major Lion Dance Competition is coming up and Mikey (Wang Weiliang) is groomed to be Shi Shens successor. However, he has a huge fear of heights! The situation worsens when both Mikey and Shi Shen fall for the Tiger Crane masters daughter (Eva Cheng).

==Production==
Development of The Lion Men was first announced in June 2013 as an untitled project hailed as the "breakthrough action movie" of director Jack Neo.    In August 2013, the films title was revealed during its opening ceremony, with Neo adding that he had been thinking of such a film premise for "quite some time ago". The budget of the film is estimated to be around $4 million. To prepare for the film, the stars had to undergo real life lion dancing training sessions.    

==Release==
The Lion Men was released on 30 January 2014, the day before Chinese New Year. 

==References==
 

 

 
 
 
 