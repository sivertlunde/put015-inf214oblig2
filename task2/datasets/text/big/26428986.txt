Little Big Top
{{Infobox film
| name           = Little Big Top
| image          =File:Littlebigtopposter.jpg
| caption        = Theatrical poster
| director       = Ward Roberts
| producer       = Christina Mauro Jessica Petelle-Slagle
| writer         = Ward Roberts
| narrator       = 
| starring       = Sid Haig Richard Riehle Hollis Resnik Mel England Jacob Zachar
| music          = Thomas Gustin
| cinematography = Jim Timperman
| editing        = Jonathan Del Gatto
| studio         = Fly High Films
| distributor    = Moving Pictures Film and Television  (U.S. theatrical)  Morningstar Entertainment  (DVD) 
| released       = October 21, 2006 (U.S. premiere—Heartland Film Festival)
| runtime        = 89 min.
| country        = United States
| language       = English
| budget          = United States dollar|$240,000 (estimated)   
| gross           = $7,072 (United States)  
| followed_by    =
}}
Little Big Top is 2006 American comedy film written and directed by Ward Roberts and starring Sid Haig, Richard Riehle, Hollis Resnik, Mel England, and Jacob Zachar.  The film tells the story of an aging, unemployed clown who returns to his small hometown, content to spend to rest of his days in a drunken stupor. But his passion for clowning is reawakened by the local amateur circus. 
 Ringling Brothers, Barnum and 8th Heartland Film Festival held in Indianapolis. It was released on DVD in November 2008 by Morningstar Entertainment. 
==Reception==
Joe Leydon of Variety (magazine)|Variety wrote that the film was "a lightly likable trifle that benefits greatly from the offbeat casting of vet heavy Sid Haig" and even though it "predictably evolves into a seriocomic tale of personal redemption through clowning around", "Haig maintains just enough irascibility to keep things interesting".  
== References ==
 
== External links ==
*  
*  
*  
*  

 
 
 
 
 
 