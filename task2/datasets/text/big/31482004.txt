Kottaram Veettile Apputtan
{{Infobox film
| name           = Kottaram Veetile Apputtan
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Rajasenan
| producer       = Kalligoor Sasi PM Basheer 
| writer         = Mani Shornur Rajan Kizhekkanda (dialogues) Shruti
| music          = Berny-Ignatius
| cinematography = K. P. Nambiathiri
| editing        = G. Murali
| studio         = United Vision
| distributor    = Seven Star
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Shruti in Rajasekhar and in Tamil as Sound Party with Sathyaraj. The film was dubbed to Tamil as "Aranmanaikaran".

==Plot==
The film tells the story of a rich orphan - Apputtan, a generous and liberal man. He helps settle the financial woes of people in the village. He is attracted to a girl whose education he has been sponsoring. But her response to his love after becoming a doctor at his expense, was something Apputtan had least expected. As Apputtan has studied only up to high school, she treats him like an illiterate. The film ends well with the girl developing interest towards Apputtan. 

==Cast==
* Jayaram as Apputtan Shruti 
* Jagathy Sreekumar
* Narendra Prasad
* Kalabhavan Mani
* Rajan P. Dev
* Cherthala Lalitha
* Darshana

== Soundtrack ==
The films soundtrack contains 6 songs, all composed by Berny-Ignatius. Lyrics by S. Ramesan Nair, Panthalam Sudhakaran, Chittoor Gopi.

{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Aavanipponnoonjal Aadikkam"
| M. G. Sreekumar
|-
| 2
| "Aavanipponnoonjaal Aadumpol"
| K. S. Chithra
|-
| 3
| "Ambottee"
| M. G. Sreekumar, Kalabhavan Mani, Jagathy Sreekumar 
|-
| 4
| "Ente Mounaraagaminnu"
| K. J. Yesudas, K. S. Chithra
|-
| 5
| "Karalinte Novarinjal"
| K. J. Yesudas
|-
| 6
| "Naalukettin Akathalathil"
| M. G. Sreekumar
|}

==References==
 

==External links==
*  
*   at the Malayalam Movie Database

 
 
 
 