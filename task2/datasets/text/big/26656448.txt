The Donner Party (2009 film)
 
  period drama drama film written and directed by T.J. Martin, and distributed by First Look International. It is based on the true story of the Donner Party, an 1840s westward traveling group of settlers headed for California. Becoming snowbound in the Sierra Nevada mountains, with food increasingly scarce, a small group calling themselves "The Forlorn Hope" turned to cannibalism. The Forlorn was the working title for the film. 

==Production==
As well as being Martins directorial debut, it was production company Anacapa Entertainments first feature film.  It premiered at the  . Retrieved March 13, 2010.  Originally, the work had a higher budget, with greater use of child stars and a longer shooting schedule. The original production company pulled funding for the project, so several changes were made. The title change was a marketing choice by the distributor. 

The soundtrack was done by the Aspiro Choir under Mary Amond OBrian.

==Cast==
* Crispin Glover&nbsp;– William Foster
* Clayne Crawford&nbsp;– William Eddy
* Michele Santopietro&nbsp;– Amanda McCutchen
* Mark Boone Junior&nbsp;– Franklin Graves
* Christian Kane&nbsp;– Charles Stanton

and

* Crispian Belfrage&nbsp;– Patrick Dolan Catherine Black&nbsp;– Ann Fosdick
* Jamie Anne Allman&nbsp;– Eleanor Eddy  
* Jack Kyle&nbsp;– Milt McCutchen
* Cary Wayne Moore&nbsp;– Jay Fosdick
* Alison Haislip&nbsp;– Mary Graves
* Mara LaFontaine&nbsp;– Sarah Foster
* John A. Lorenz&nbsp;– Luis

==See also==
 
* Cannibalism in popular culture

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 