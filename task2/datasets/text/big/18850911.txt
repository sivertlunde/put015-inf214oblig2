Mickey's Circus
Mickeys Frank Thomas, Al Eugster & Shamus Culhane.

Although the film is called Mickeys Circus, the film mostly features Donald Duck; however Mickey Mouse does feature in the beginning and end. Also featured are a trio of trained Sea Lions, a young Sea Lion pup, and an audience full of children.

==Plot== sea lions. Children from all over come to see the circus. After Mickey introduces the performers, the show starts. Donald enters the ring only to be stampeded by the sea lions and a young sea lion pup (Salty the Seal). The first act is juggling, in which the sea lions and Donald juggle balls that land on their noses. When the stunt is finished, the sea lions beg to be fed, throwing their balls at Donald and pointing to their mouths. Before Donald can feed the last sea lion, Salty intervenes and steals the fish. The resulting tug-of-war ends in the sea lions fighting over the fish. The next act is playing the organ pipe, but the sea lion cant do it right. Salty interferes and plays the tune "Yankee Doodle" instead, but the crowd cheers anyway. Salty distracts Donald to the point where Donald loses his temper, attacks Salty, and gets stuck in a drum. Donald regains control by driving away the sea lions with his pistol.
 voltage to high, electrocuting Mickey and Donald and splitting the bike (and rope) in half, causing Mickey and Donald to fall. The two land in the seal tank, only to be hit by a fish thrown by Salty, and the film ends with the sea lions beating up Mickey and Donald as they fight over the fish in the tank.

== Release ==

3 May 2004: Released on DVD as part of a two disk collection of early Mickey Mouse shorts -  , part of the Walt Disney Treasures line.

==Trivia==

*This is Salty the Seals first appearance.

== See also ==
* List of Disney animated shorts
* Mickey Mouse and Donald Duck

== External links ==
* 
*  

 

 
 
 
 
 
 
 

 