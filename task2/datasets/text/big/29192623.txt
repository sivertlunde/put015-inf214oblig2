Violent Story
{{Multiple issues|
 
 
 
}}

Violent Story (2009) is a crime/drama film.  The feature film was written and directed by Rodrigo Cipriano for Bobbing Skull Films.  The film stars Gabriel Valentin Ochoa, Hank Vasquez and Israel Ochoa.

==Plot==
The film follows Lorenzo Madero, a low level thug and ex boxer who collects taxes for a loan shark.  Lorenzo decides to clean up his act and leave his violent ways after he slices open a watermelon and finds the image of the Virgin Mary Virgen de Guadalupe outlined in the melons seeds.  His employers think that he will turn them all into the police based on his new way of life so they set off to hunt him down. Lorenzo finds that in order to reach his goal of a peaceful existence he will have to kill for it. Mykel Board of Maximum Rock N Roll called the film "Great!" and praised Gabriel Valentin Ochoas performance saying "He was great in it". 

==References==
*Internet Movie Database 

 