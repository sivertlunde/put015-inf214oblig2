Ram Lakshman
{{Infobox film
| name           = Ram Lakshman
| image          = Raamlakshmanfilm.png
| image_size     =
| caption        = LP Vinyl Records Cover
| director       = R.Thyagarajan
| producer       = Devar Films
| writer         =
| narrator       = Asokan M. Raveendran Sivachandran Suruli Rajan Thengai Srinivasan V.Gopalakrishnan
| music          = Ilayaraaja
| cinematography = V.Ramamoorthy
| editing        = M.G.Balurao
| distributor    =
| released       =   28 Feb 1981
| runtime        =
| country        = India Tamil
| budget         =
| gross          =
| preceded_by    =
| followed_by    =
| website        =
}}

Ram Lakshman is a Tamil language film starring Kamal Haasan in the lead role of the protagonist. A song from this film, Nandhaan Ungappanda,  was selected to be played at the opening ceremony of the London 2012 Olympics.  The star power of Kamalhassan made this average movie a Superhit at the box office.

==Summary==

Ram (Kamal Haasan) and his pet elephant Laxman have grown up together and are very emotionally attached to one another. They share such a strong bond that Ram refuses to marry his sweetheart Meena (Sripriya) when her father refuses to sanction their marriage unless Ram leaves Laxman. Meanwhile, in a very cunningly planned plot, an employee of Meenas father kills him and implicates Ram and Laxman for the murder. How the two friends stand by each other in the hour of need and how they prove themselves innocent forms the rest of the plot.

==Cast==

*Kamal Haasan
*Sripriya Asokan
*M. N. Nambiar
*Master Bittu Raveendran
*Sivachandran
*Suruli Rajan
*Thengai Srinivasan
*V.Gopalakrishnan

==External links==
*  

==References==
  

 
 
 
 
 
 


 