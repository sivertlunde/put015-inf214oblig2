Deportee (film)
{{Infobox Film
| name           = Deportee
| image          = 
| image_size     = 
| caption        = 
| director       = Sharron Miller
| producer       = Sharron Miller
| writer         = Sharron Miller
| narrator       = 
| starring       = Andrew Stevens Leslie Paxton Sam Gilman
| music          = Evan Tonsing Song "Flying Upside Down in My Plane" by Steve Ripley
| cinematography = Michael Scott
| editing        = Sharron Miller
| released       = 1976
| runtime        = 24 minutes
| country        = United States
| language       = English
}}

:For the Woody Guthrie song, see "Deportee (Plane Wreck at Los Gatos)".
 dramatic short film written, produced and directed by Sharron Miller.  It stars Andrew Stevens, Leslie Paxton, and Sam Gilman.  

==Plot==
Deportee tells the story of a young man and his alcoholic father who live in a skid-row hotel while trying to make ends meet.  The father longs for the farm they left behind when they came to the city seeking a better life.  The young man falls in love with a beautiful but troubled older woman who lives down the hall.  Their bittersweet romance proves to be his painful rite-of-passage into adulthood.

==External links==
*   

 
 
 
 
 
 
 
 
 