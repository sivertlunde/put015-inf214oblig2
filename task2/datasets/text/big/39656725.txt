The Gentleman Without a Residence
{{Infobox film
| name           = The Gentleman Without a Residence
| image          = 
| image_size     = 
| caption        = 
| director       = Heinrich Bolten-Baeckers 
| producer       = Heinrich Bolten-Baeckers
| writer         = Heinrich Bolten-Baeckers
| narrator       = 
| starring       = Georg Alexander   Georg John   Paul Otto   Julius Brandt
| music          = 
| editing        =
| cinematography = Hermann Boettger   Albert Schattmann
| studio         = BB-Film-Fabrikation   UFA
| released       = 11 November 1925
| runtime        = 
| country        = Germany
| language       = Silent   German intertitles
| budget         =  
| gross          =
| preceded_by    = 
| followed_by    = 
}} German silent silent comedy UFA alongside its more prestigious art films.  The films art direction was by Erich Czerwonski.

==Cast==
* Georg Alexander as Alfred 
* Georg John as Fürst 
* Paul Otto as Professor
* Julius Brandt as Droschkenkutscher  Richard Ludwig as Rechtsanwalt 
* Heinrich Gotho as zweiter Professor 
* Margarete Lanner as Frau des Professors

==References==
 

==Bibliography==
* Kreimeier, Klaus. The Ufa Story: A History of Germanys Greatest Film Company, 1918-1945. University of California Press, 1999.

==External links==
* 

 
 
 
 
 
 
 
 


 
 