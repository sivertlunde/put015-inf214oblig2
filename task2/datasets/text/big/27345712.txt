Cochin Express
{{Infobox film 
| name           = Cochin Express
| image          =
| caption        =
| director       = M. Krishnan Nair (director)|M. Krishnan Nair
| producer       = TE Vasudevan
| writer         = V Devan S. L. Puram Sadanandan (dialogues)
| screenplay     =
| starring       = Prem Nazir Sheela Adoor Bhasi Sankaradi
| music          = V. Dakshinamoorthy
| cinematography =
| editing        =
| studio         = Jaya Maruthi
| distributor    = Jaya Maruthi
| released       =  
| country        = India Malayalam
}}
 1967 Cinema Indian Malayalam Malayalam thriller film, directed by M. Krishnan Nair (director)|M. Krishnan Nair and produced by TE Vasudevan. The film stars Prem Nazir, Sheela, Adoor Bhasi and Sankaradi in lead roles. The film had musical score by V. Dakshinamoorthy.   

==Plot== Madras to Cochin. Adoor Bhasi is Unni Kannan Nair who was a passenger in the same compartment where the murder happened and also he is the only person who saw a lady passenger that is suspected behind the murder. 

==Cast==
*Prem Nazir
*Sheela
*Adoor Bhasi
*Sankaradi
*Devakibhai GK Pillai
*Kottarakkara Sreedharan Nair
*Lakshmi
*Sakunthala
*Vijayalalitha

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chandamulloru Penmani || K. J. Yesudas, LR Eeswari || Sreekumaran Thampi || 
|-
| 2 || Ethu Raavilennnariyilla || P. Leela || Sreekumaran Thampi || 
|-
| 3 || Innunammal ramikkuka || V. Dakshinamoorthy, LR Eeswari || Sreekumaran Thampi || 
|-
| 4 || Irathedi Piriyum || S Janaki, Chorus, Uthaman || Sreekumaran Thampi || 
|-
| 5 || Kadhayonnu Kettu || S Janaki || Sreekumaran Thampi || 
|-
| 6 || Kannukal Thudichappol || P. Leela || Sreekumaran Thampi || 
|}

==References==
 

==External links==
*  

 
 
 
 
 
 

 