Love Me or Leave Me (film)
{{Infobox film
| name           = Love Me or Leave Me
| image          = Love me or leave me.jpg
| caption        = Theatrical Poster
| director       = Charles Vidor
| producer       = Joe Pasternak
| writer         = Daniel Fuchs Isobel Lennart Cameron Mitchell
| music          = Chilton Price
| cinematography = Arthur E. Arling
| editing        = Ralph E. Winters
| distributor    = Metro-Goldwyn-Mayer
| released       = May 26, 1955 (New York City|NYC)   June 10 (US wide)
| runtime        = 122 minutes
| country        = United States
| language       = English
| budget         = $2,762,000  . 
| gross          = $5,632,000   
}} biographical romance romantic musical musical drama Martin "Moe Cameron Mitchell as pianist/arranger Myrl Alderman, her second husband. It was written by Daniel Fuchs and Isobel Lennart.  It was directed by Charles Vidor.

==Production== MGM to cast Doris Day, who was excited to play opposite Cagney. 

==Reception== Academy Award Best Writing, Best Actor Best Music, Best Music, Best Sound, Best Writing, Screenplay.   

Variety (magazine)|Variety called the film "a rich canvas of the Roaring 20s, with gutsy and excellent performances."   Posted: Sat., Jan. 1, 1955 

==Songs== Never Look Back", by Chilton Price, and, "Ill Never Stop Loving You (1955 song)|Ill Never Stop Loving You", by Nicholas Brodzsky and Sammy Cahn. 

The songs as they appear in the film (all sung by Doris Day except as shown):
*Ten Cents a Dance
*Im Sitting on Top of the World (sung by Claude Stroud)
*It All Depends On You You Made Me Love You
*Stay On the Right Side Sister Everybody Loves My Baby (But My Baby Loves Nobody But Me) Mean To Me
*Sam, the Old Accordion Man
*Shaking the Blues Away (sung by Doris Day, danced by Doris Day and Chorus)
*Ill Never Stop Loving You (1955 song)|Ill Never Stop Loving You Never Look Back
*Five Foot Two, Eyes of Blue
*At Sundown My Blue Heaven Love Me or Leave Me

==Cast==
* Doris Day as Ruth Etting
* James Cagney as Martin Snyder Cameron Mitchell as Johnny Alderman Robert Keith as Bernard V. Loomis
* Tom Tully as Frobisher
* Harry Bellaver as Georgie
* Richard Gaines as Paul Hunter
* Peter Leeds as Fred Taylor
* Claude Stroud as Eddie Fulton
* Audrey Young as Jingle Girl
* John Harding as Greg Trent

==Reception==
According to MGM records the film earned $4,035,000 in the US and Canada and $1,597,000 elsewhere, resulting in a profit of $595,000. 
Love Me or Leave Me was the eighth ranked movie in 1955.

==References==
 

== External links ==
 
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 