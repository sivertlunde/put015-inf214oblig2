The Golden Head
{{Infobox film
| name           = The Golden Head
| image          = 
| image_size     = 
| caption        =  James Hill
| producer       = Alexander Paal   William R. Forman
| writer         = Iván Boldizsár   Stanley Goulder   Roger Windle Pilkington
| music          =  Szabolcs Fényes
| editing        = 
| cinematography = 
| narrator       = 
| starring       = George Sanders   Buddy Hackett  Jess Conrad   Lorraine Power
| distributor    = Hungarofilm
| released       = 10 December 1964
| runtime        = 105 minutes (Hungary)
| country        = United States   Hungary
| language       = English   Hungarian
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
 Hungarian comedy James Hill and starring George Sanders, Buddy Hackett, Jess Conrad, Lorraine Power and Robert Coote. 

==Synopsis==
The children of a British policemen holidaying in Hungary track down a priceless art treasure which has recently been stolen.

==Cast==
* George Sanders - Basil Palmer
* Buddy Hackett - Lionel Pack
* Jess Conrad - Michael Stevenson
* Lorraine Power - Milly Stevenson
* Robert Coote - Braithwaite
* Denis Gilmore - Harold Stevenson
* Cecília Esztergályos - Anne
* Douglas Wilmer - Detective Inspector Stevenson
* Sándor Pécsi - Priest
* Zoltán Makláry - Old Man

==Production==
Its Hungarian title is Az aranyfej. It was shot on location in Hungary and was loosely based on the novel by Nepomuk of the River by Roger Windle Pilkington. Lionel Jeffries and Hayley Mills were originally attached to the project. James Hill was the original director, but was replaced by Richard Thorpe during shooting. 

==References==
 

==External links==
* 

 


 
 
 
 
 
 
 
 
 


 