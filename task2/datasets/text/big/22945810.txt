Companys, procés a Catalunya
 
{{Infobox film
| name           = Companys, procés a Catalunya
| image          =
| caption        =
| director       = Josep Maria Forn
| producer       =
| writer         = Josep Maria Forn Antoni Freixas
| narrator       =
| starring       = Luis Iriondo
| music          =
| cinematography = Cecilio Paniagua
| editing        = Emilio Rodríguez
| distributor    =
| released       =  
| runtime        =
| country        = Spain
| language       = Spanish
| budget         =
}}

Companys, procés a Catalunya ( ) is a 1979 Spanish drama film directed by Josep Maria Forn. It competed in the Un Certain Regard section at the 1979 Cannes Film Festival.   

==Cast==
* Luis Iriondo - Lluís Companys
* Marta Angelat - Ángela
* Montserrat Carulla - Ramona Companys
* Xabier Elorriaga - Fortuny
* Pau Garsaball - Miquel
* Agustín González - Miembro del Tribunal
* Alfred Lucchetti - Juez instructor
* Marta May - Carmen Ballester
* Biel Moll - Irraca Pastor
* Ovidi Montllor - Jordi
* Conrado Tortosa Pipper - García
* Rafael Anglada - Martí

==References==
 

==External links==
* 

 
 
 
 
 
 


 