The Kingdom of the Fairies
 
{{Infobox film
| name           = Le Royaume des fées
| image          = Royaume des fees.jpg
| caption        = A frame from the film
| director       = Georges Méliès
| producer       = Georges Méliès
| writer         = Georges Méliès
| starring       = Bleuette Bernon Georges Méliès
| cinematography = 
| editing        = 
| distributor    = 
| released       =  
| runtime        = 320 meters   
| country        = France 
| language       = Silent
| budget         = 
}}

The Kingdom of the Fairies ( ),   initially released in the United States as Fairyland, or the Kingdom of the Fairies and in Great Britain as The Wonders of the Deep, or Kingdom of the Fairies, is a 1903 French silent film directed by Georges Méliès.  The film is freely adapted from Biche au Bois, a popular stage pantomime that had originated at the Théâtre de la Porte Saint-Martin in 1845.    Prints of the film survive in the film archives of the British Film Institute and the Library of Congress.   

==Cast==
* Georges Méliès as Prince Bel-Azor   
* Marguerite Thévenard as Princess Azurine 
* Bleuette Bernon as the fairy Aurora 

==Release==
The Kingdom of the Fairies was released by Mélièss Star Film Company and is numbered 483–498 in its catalogues.  (In Mélièss numbering system, films were listed and numbered according to their order of production, and each catalogue number denotes about 20 meters of film.)  The film was registered for American copyright at the Library of Congress on 3 September 1903. 
 The Barber of Seville), some prints were individually Film colorization|hand-colored and sold at a higher price. 

==Reception==
The Kingdom of the Fairies, like Mélièss similarly spectacular films A Trip to the Moon (1902) and The Impossible Voyage (1904), was one of the most popular films of the first few years of the twentieth century.  When Thomas L. Tally debuted the film at his Lyric Theater in Los Angeles in 1903 (billing it as "Better than A Trip to the Moon"), the Los Angeles Times called the film "an interesting exhibit of the limits to which moving picture making can be carried in the hands of experts equipped with time and money to carry out their devices." 

The film theorist Jean Mitry called it "undoubtedly Mélièss best film, and in any case the most intensely poetic." 

==References==
 

==External links==
* 
* 

 

 
 
 
 
 
 
 
 
 