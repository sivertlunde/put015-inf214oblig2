Phir Se...
{{infobox film
| name = Phir Se...
| image = Phir Se... Film First Look.jpg
| caption  = 
| director = Kunal Kohli Ajay Bhuyan Shiv Kumar
| writer = 
| starring = Kunal Kohli Jennifer Winget 
| music = Jeet Ganguly Background Score Sandeep Shirodka
| cinematography = Maneesh Chandra Bhatt
| editing = Dhawal Ashok Darji
| studio = Bombay Film Company Ltd.
| distributor = The Bombay Film Company Ltd.(United Kingdom|UK)
| released = Scheduled for  
| country = India
| runtime = 
| language = Hindi
| budget = 
| gross  = 
}}
Phir Se (English: Once Again) is an upcoming Indian Romance film directed by Kunal Kohli and Ajay Bhuyan. The film stars Kunal Kohli and Jennifer Winget and deals with a husband-wife relationship. Rajit Kapoor, Dalip Tahil, Kanwaljeet Singh and Sushmita Mukherjee portray supporting roles. This is the first film of the director Kunal Kohli as an actor and also the debut film of Jennifer Winget in bollywood.   Phir Se is scheduled for release on May 8, 2015.    The tag line of the film is "Im not 40 Im 18 with 22 years experience"  The trailer of the film was released on 7 April 2015.

== Plot ==
Phir Se is going to be based on a separated couple living in London, trying to come to terms with the consequences of their split.

== Cast ==
* Kunal Kohli as Jay Khanna   
* Jennifer Winget as Kaajal Kapoor 
* Rajit Kapoor 
* Dalip Tahil 
* Kanwaljeet Singh 
* Sushmita Mukherjee 

== Production ==

===Filming===
The shooting of the film Phir Se is going to be held completely in London. 

===Casting===
 Saraswatichandra a daily soap produced by Sanjay Leela Bhansali. Kunal Kohli tweeted the poster of his film on Twitter on 13 October 2014. It was first time he revealed something about the film.  The first look of the film was also reaveled then.

== Soundtrack ==
{{Infobox album
| Name = Phir Se...
| Type = Soundtrack
| Artist = Jeet Ganguly
| Released =  
| Length =   Feature Film Soundtrack
| Label = T-Series
| Last Album = }}

The Soundtrack of the album is composed by Jeet Gannguli, Lyrics are penned by Rashmi Singh. Singers include Arijit Singh, Shreya Ghoshal, Palak Muchhal, Mohammed Irfan, Mohit Chauhan and Nikhil DSouza.  The first song of the film is "Rozana". This song is being gifted to Kunal Kohli by T-Series manager Bhushan Kumar as media news. 
===Track listing===

{{track listing
| extra_column=Singer(s)
| total_length =  
| title1= Rozana
| extra1= Mohit Chauhan, Shreya Ghoshal
| length1=
| title2= Title Song 1
| extra2= Nikhil DSouza, Shreya Ghoshal
| length2=
| title3= Title Song 2
| extra3= Shruti Pathak
| length3=
| title4= Ye Dil Jo Hai
| extra4= Mohit Chauhan, Palak Muchhal
| length4= 
}}

== Marketing ==

The trailer was released through the movies official facebook, twitter accounts and T-series YouTube channel on 7th April 2015. The 155 second clip featured glimpses of the critically acclaimed director Kunal Kohli making his acting debut in bollywood with the daily soap actress Jenifer Winget. Personalities like Karan Johar, Rishi Kapoor, Gautam Singhania, Farhan Akhtar, Uday Chopra among many other tweeted about the movie and its trailer release on twitter.

== References ==
 

== External links ==
* 
* 
* 
* 

 
 
 
 
 
 
 