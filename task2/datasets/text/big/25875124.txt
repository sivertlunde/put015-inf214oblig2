Xagoroloi Bohudoor
{{Infobox film
| name           = Xagoroloi Bohudoor (Its a Long Way to the Sea)
| image          = sbd_screenshot.jpg
| caption        = A screenshot
| director       = Jahnu Barua 
| producer       = Jahnu Barua Sailadhar Baruah Gayatri Barua
| writer         = Jahnu Barua
| starring       = Bishnu Kharghoria Arun Nath Kashmiri Saikia Baruah Sushanta Baruah
| music          = Satya Baruah
| cinematography = P. Ranjan
| editing        = Heu-en Baruah
| distributor    = Dolphin Films Pvt. Ltd
| released       = 1995
| runtime        = 106 minutes
| country        = India
| language       = Assamese
}}
Xagoroloi Bohudoor (  film directed by Jahnu Barua. The film was released in 1995.

==Plot summary==
The story revolves around a boatman who earns his living by sailing boat in the nearby ghats. Problem arises when the government decides to construct a bridge on it which will deprive his earnings. His son who lives in city wants his father only to take care of their property. 

==Cast==
*Bishnu Kharghoria as Powal, the old man
*Arun Nath as Homanta, Powal’s son
*Kashmiri Saikia as Runumi
*Miren as Land agent
*Shusanta Barooah as Kkhuman, the boy
*Baruah as daughter-in-law
*Jatin Bora

==Awards==
*National Film Award for Best Feature Film in Assamese (1995)
*GETZ Prize( 31st Chicago International Film Festival )
*Pri Do Public Award (Best Film: Nantes Film festival, France)

==References==
 

==External links==
* 
 

 
 
 
 


 