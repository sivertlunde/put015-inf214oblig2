Prince of Shadows
{{Infobox film
| name           = Prince of Shadows
| image          = 
| caption        = 
| director       = Pilar Miró
| producer       = Andrés Vicente Gómez
| writer         = Pilar Miró Antonio Muñoz Molina
| starring       = Terence Stamp
| music          = 
| cinematography = Javier Aguirresarobe
| editing        = José Luis Matesanz
| distributor    = 
| released       =  
| runtime        = 109 minutes
| country        = Spain
| language       = Spanish
| budget         = 
}}

Prince of Shadows ( ) is a 1991 Spanish crime film directed by Pilar Miró. It was entered into the 42nd Berlin International Film Festival where it won the Silver Bear for an outstanding artistic contribution.   

==Cast==
* Terence Stamp as Darman
* Patsy Kensit as Rebeca
* José Luis Gómez as Ugarte / Valdivia
* Geraldine James as Rebeca Osorio
* Simón Andreu as Andrade
* Aleksander Bardini as Bernal (as Aleksander Bardin)
* John McEnery as Walter
* Jorge de Juan as Luque
* Pedro Díez del Corral as Policeman
* Carlos Hipólito as Boite Owner
* Francisco Casares as Train Steward
* Queta Claver as Dancer Boite
* Felipe Vélez as Policeman
* William Job as Howard
* Magdalena Wójcik as Polish Tango Dancer

==References==
 

==External links==
* 

 
 
 
 
 
 
 


 
 