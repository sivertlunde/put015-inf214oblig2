Baby Blues (1941 film)
{{Infobox film
| name           = Baby Blues
| image          =
| image_size     =
| caption        = Edward Cahn
| producer       =
| writer         = Hal Law Robert A. McGowan
| narrator       =
| starring       =
| music          =
| cinematography = Jackson Rose
| editing        = Leon Borgeau
| distributor    = Metro-Goldwyn-Mayer
| released       = February 15, 1941
| runtime        = 9 18"
| country        = United States English
| budget         =
}}
 short comedy Edward Cahn.  It was the 196th Our Gang short (197th episode, 108th talking short, 109th talking episode, and 28th MGM produced episode) that was released.

==Plot==
When Mickey reads something in a jokebook stating that one out of every four children born is Chinese, he begins to worry that his new baby brother or sister will be Chinese as well. The Gang put Mickey at ease by telling him that it will not be so bad to have a Chinese sibling. The gang then introduce Mickey to Spankys friend Lee Wong. Once he has learned that people are people no matter what their ethnic background, Mickey is happy—until he discovers that his much-anticipated "kid brother" is not only a girl, but twins to boot.   

==Cast==
===The Gang=== Mickey Gubitosi as Mickey Gubitosi
* Billy Laughlin as Froggy
* George McFarland as Spanky
* Billie Thomas as Buckwheat

===Additional cast===
* Edward Soo Hoo as Lee Wong
* Janet Burston as Mamie Gubitosi
* Freddie Chapman as Bully, tough kid
* Vincent Graeff as Tough kid
* James Gubitosi as Tough kid
* Ruth Tobey as Gladys Gubitosi
* Hank Mann as Zoo assistant
* Margaret Bert as Receptionist William Edmunds as Mickeys father
* Eddie Lee as Lee Wongs father
* Jenny Lee as Lee Wongs mother

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 