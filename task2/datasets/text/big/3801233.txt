Remote Control (1992 film)
{{Infobox film
| name           = Remote Control
| image          = Remote Control (1992 movie poster).jpg
| caption        = 
| director       = Óskar Jónasson
| producer       = Jón Ólafsson
| writer         = Óskar Jónasson
| narrator       = 
| starring       = Björn Jörundur Friðbjörnsson
| music          = Björk Sigurjón Kjartansson	
| cinematography = 
| editing        = Valdís Óskarsdóttir
| distributor    =  1992
| runtime        = 78 minutes
| country        = Iceland
| language       = Icelandic
| budget         = 
| preceded_by    = 
| followed_by    = 
}}
Remote Control ( ic film from 1992 directed by Óskar Jónasson. The plot is a farce, revolving around the young car mechanic Axel and his adventure in the Reykjavík underworld which starts when his mother insists that he must recover the remote control to her TV. It was screened in the Un Certain Regard section at the 1993 Cannes Film Festival.   

The film stars Björn Jörundur Friðbjörnsson as Axel, and features the Icelandic metal band HAM (band)|HAM.

==Cast==
* Björn Jörundur Friðbjörnsson - Axel
* Þórarinn Eyfjörð - Flosi
* Thorarinn Oskar Thorarinsson - Vigfús
* Helga Braga Jónsdóttir - Símastúlka
* Þóra Friðriksdóttir - Mamma
* Margrét Hugrún Gústavsdóttir - Mæja
* Sigurjón Kjartansson - Orri
* Soley Eliasdottir - Unnur
* Óttarr Proppé - Hrólfur
* Ari Matthíasson - Þorbjörn
* Erling Jóhannesson - Arnar
* Pétur Eggerz - Sveinn
* Helgi Björnsson - Moli
* Ólafur Guðmundsson - Árni
* Björn Karlsson - Höddi Feiti
* Rósa Guðný Þórsdóttir - Sonja 
* Guðný Helgadóttir - Katrín 
* Árni Pétur Guðjónsson - Viggó 
* Hjálmar Hjálmarsson - Hjörtur 
* Jóhann G. Jóhannsson - Garðar 
* Gígja Hilmarsdóttir - Daughter of Moli and Sonja 
* Eggert Þorleifsson - Aggi Flinki 
* Stefan St. Sigurjonsson - Brjánsi Sýra 
* Þröstur Guðbjartsson - Elli 
* Steinunn Ólafsdóttir - Afgreiðslustúlka 
* Skúli Gautason - Grímur 
* Björgúlfur Egilsson - Rótari 
* Björn Blöndal - Trommari 
* Flosi Þorgeirsson - Gítarleikari 
* Baldur Maríusson - Veiðimaður 
* Sólveig Stefánsdóttir - Barn 
* Ingólfur Þór Guðmundsson -  Barn 
* Þórir Steingrímsson - Hallvarður 
* Þröstur Leó Gunnarsson - Áslákur 

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 