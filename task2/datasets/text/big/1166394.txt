The War Lord
{{Infobox film
| name           = The War Lord
| image          = Warlordposter.jpeg
| caption        = film poster by Howard Terpning
| director       = Franklin J. Schaffner
| producer       = Walter Seltzer John Collier Millard Kaufman
| based on       =    Maurice Evans Guy Stockwell
| music          = Jerome Moross Hans J. Salter
| cinematography = Russell Metty
| editing        = Folmar Blangsted
| distributor    = Universal Pictures
| released       =   
| runtime        = 123 min
| country        = United States
| language       = English
| budget         = $ 3,500,000
| gross          =
}}
:See The Warlord (comics) for the comic book.
 Maurice Evans, Niall MacGinnis, Henry Wilcoxon and James Farentino, with Jon Alderson, Allen Jaffe, Sammy Ross, and Woodrow Parfrey. Schaffner would later reteam with Heston and Evans in 1968 for Planet of the Apes. 

Until this film, most Hollywood representations of feudal life were glamorized. The War Lord attempts to portray the 11th Century in a more accurate fashion as dirty, violent and ruled by brute force. The social stratification imposed by feudalism governed every human relationship, with power devolving from the duke, to the knight, to the men at arms, the church and the peasantry at the very bottom.

==Plot==
Chrysagon de la Cruex (Heston) is a Norman knight charged with defending a Druid village.  At the heart of the story is a doomed romance which defies the social norms and sparks a growing confrontation with Chrysagons brother, Draco (Stockwell).

Chrysagon encounters Bronwyn (Forsyth), his future love, as she is harassed by his own men.  Gradually he finds himself falling for the girl hes rescued.  Bronwyns father, the village chief, Odins, later asks Chrysagons permission for Bronwyn to marry Marc, her betrothed.  Chrysagon approves, but soon regrets the decision.  He wants Bronwyn for himself.

He later learns of "Droit du seigneur", a right which permits the Lord of the Domain to sleep with any virgin woman on her wedding night.  But custom demands Bronwyn be given up by dawn.  The following day, Bronwyn is not returned and Marc demands justice.  What the village doesnt realize is that shes chosen to stay of her own free will.
 Frisian raiders Norman coast.

==Main cast==
* Charlton Heston as Chrysagon
* Rosemary Forsyth as Bronwyn
* Richard Boone as Bors
* Guy Stockwell as Draco Maurice Evans as Priest
* James Farentino as Marc
* Henry Wilcoxon as Frisian Prince
* Niall MacGinnis as Odins

==See also==
*Middle Ages in film

==External links==
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 