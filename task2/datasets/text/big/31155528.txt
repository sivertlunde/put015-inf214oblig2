Tharavattamma
{{Infobox film
| name           = Tharavattamma
| image          =
| caption        =
| director       = P. Bhaskaran
| producer       = N Vasudevan
| writer         = Dada Mirasi Parappurathu (dialogues)
| screenplay     = P. Bhaskaran Sathyan Sheela Sukumari Thikkurissi Sukumaran Nair
| music          = MS Baburaj
| cinematography = EN Balakrishnan
| editing        = K Narayanan K Sankunni
| studio         = Madras Movies
| distributor    = Madras Movies
| released       =  
| country        = India Malayalam
}}
 1966 Cinema Indian Malayalam Malayalam film,  directed by P. Bhaskaran and produced by N Vasudevan. The film stars Sathyan (actor)|Sathyan, Sheela, Sukumari and Thikkurissi Sukumaran Nair in lead roles. The film had musical score by MS Baburaj.   

==Cast==
  Sathyan
*Sheela
*Sukumari
*Thikkurissi Sukumaran Nair
*P. J. Antony
*Prathapachandran
*Vasanthi
*Abbas
*Aranmula Ponnamma
*B. S. Saroja GK Pillai
*K. P. Ummer
*PR Varalekshmi
 

==Soundtrack==
The music was composed by MS Baburaj and lyrics was written by P. Bhaskaran.

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Chettathiyamma || Renuka || P. Bhaskaran || 
|-
| 2 || Kanniyil Pirannaalum || K. J. Yesudas || P. Bhaskaran || 
|-
| 3 || Mannerinjaal Ponnuvilayum || K. J. Yesudas, Kamala, Renuka, Natarajan, Ramani, Moitheen, Radha || P. Bhaskaran || 
|-
| 4 || Mattoru Seethaye || Kamukara || P. Bhaskaran || 
|-
| 5 || Oru Kochu Swapnathin || S Janaki || P. Bhaskaran || 
|-
| 6 || Pandu Nammal || S Janaki, B Vasantha || P. Bhaskaran || 
|-
| 7 || Udalukalariyaathe || B Vasantha, KP Udayabhanu || P. Bhaskaran || 
|}

==References==
 

==External links==
*  

 
 
 


 