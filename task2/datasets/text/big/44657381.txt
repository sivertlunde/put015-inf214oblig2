Case No. 18/9
{{Infobox film
| name           = Case No.18/9
| image          = 2013 Kannada film Case No. 18 9 poster.jpg
| caption        = Film poster
| director       = Mahesh Rao
| producer       = Praveen Kumar Shetty Shivanand Shetty V. K. Mohan Kanthi Shetty
| writer         = Balaji Sakthivel
| screenplay     = Mahesh Rao
| based on       =  
| starring       = Niranjan Shetty Sindhu Lokanath
| narrator       = 
| music          = Arjun Janya cinematography = SabhaKumar
| editing        = Deepu S. Kumar
| studio         = Kranthi Creations
| distributor    = 
| released       =  
| runtime        = 134 minutes
| country        = India Kannada
| budget         = 
| gross          = 
}}
 Tamil film Vazhakku Enn 18/9 (2012). The film stars Niranjan Shetty and Sindhu Lokanath in the lead roles. The supporting cast features Abhishek, Shwetha Pandit and Rangayana Raghu.

==Cast==
 
* Niranjan Shetty as Mahadeva
* Sindhu Lokanath as Lakshmi
* Abhishek as Sandy
* Shwetha Pandit as Kavya
* Rangayana Raghu as Revanna
* Girija Lokesh
* Kari Subbu
* Karthik Sharma
* Mico Nagaraj
* Marina Thara
* Rekha V. Kumar
* Koli Ramya
* Apoorva
* Kuri Prathap
* Victory Vasu
* Girish
* Girish Jatthi
* Gowda Kondajji
* Shailashree
* Joseph
* Master Goutham
* Manjula
* Vijay Simha
* Harshika Poonacha in a cameo appearance
 

==Production and marketing== Tamil film Vazhakku Enn 18/9. Niranjan Shetty and Sindhu Lokanath were signed to play the lead roles.  Shwetha Pandit was signed to play the role of a school-going girl in the film, in October 2012.  Reports came out in March 2013, that Harshika Poonacha would be making a cameo appearance in the film, in a item number. The song was filmed at a pub in Bangalore at an estimated cost of  . 

The trailer of the film featuring sequences of action and romance, was released on YouTube on April 30, 2013.  Prior to 2 weeks of the films release, its satellite rights were secured by the TV channel Suvarna, for over  . 

==Soundtrack==
{{Infobox album
| Name        = Case No. 18/9
| Type        = Soundtrack
| Artist      = Arjun Janya
| Cover       = 2013 Kannada film Case No. 18 9 album cover.jpg
| Border      = yes
| Caption     = Soundtrack cover
| Released    = 29 March 2013
| Recorded    =  Feature film soundtrack
| Length      = 27:39
| Label       = Jhankar Music
| Producer    = 
| Last album  = 
| This album  = 
| Next album  = 
}}

The music for the soundtrack was composed by Arjun Janya, also writing the lyrics for the track "Yakbekithappa Mobile Phonu". The other tracks had lyrics were written by Ghouse Peer, V. Nagendra Prasad, Mahesh Rao, Vikas Chandra and Hrudaya Shiva. The album consists of seven soundtracks. 

{{tracklist
| headline = Tracklist
| extra_column = Artist(s)
| total_length = 27:39
| lyrics_credits = yes
| title1 = Ninthalle Ninthukolle
| lyrics1 = Ghouse Peer
| extra1 = Vijay Prakash
| length1 = 4:33
| title2 = Nodkond Nodkond
| lyrics2 = V. Nagendra Prasad Tippu
| length2 = 4:45
| title3 = Party Suru - Virus Mix
| lyrics3 = Mahesh Rao, Vikas Chandra
| extra3 = Arjun Janya
| length3 = 4:16
| title4 = Yakbekithappa Mobile Phonu
| lyrics4 = Arjun Janya
| extra4 = Arjun Janya
| length4 = 4:03
| title5 = Ee Beyuva Hrudhayada
| lyrics5 = Hrudaya Shiva
| extra5 = Sri Loki
| length5 = 3:05
| title6 = Party Suru
| lyrics6 = Mahesh Rao, Vikas Chandra
| extra6 = Arjun Janya
| length6 = 4:55
| title7 = Theme Music - Case No 18/9
| lyrics7 = 
| extra7 = Arjun Janya
| length7 = 2:02
}}

==Critical reception==
Upon theatrical release, the film received generally positive reviews from critics. The performances Niranjan Shetty, Sindhu Lokanath, Shwetha Pandit and Rangayana Raghu, and the films direction, screenplay and narration received praise from critics.

G. S. Kumar of The Times of India reviewed the film giving it a rating of 3.5/5 and wrote, "Armed with an excellent script, director Mahesh Rao has handled the sequences very well with good narration". He concluded crediting the performances of all the lead characters, and the cinematography and music.  Bangalore Mirror, in its review called the film "a laudable effort" and praises the performances of Niranjan Shetty, Sindhu Lokanath, Shwetha Pandit and Rangayana Raghu, and credits the screenplay and music.  Writing for Deccan Herald, B. S. Srivani gave the film a 3-star rating and wrote, "Case no. 18/9 stands out for several reasons. Sabha Kumar’s camerawork is excellent, Arjun Janya revives spirits with his score and Deepu S Kumar is so mindful of his scissors, the impact created by the secondary leads is maximised, leading to a rather tame climax, which appears so mainly because it comes out so naturally!" and concluded writing, "Nirranjan’s lament, Sindhu’s shining eyes, Abhishek’s sneer, Shwetha’s body language and Raghu’s quiet competence linger long after the case is closed."  Sify.com gave the film an average rating and said the film "original essence of the movie (Vazhakku Enn 18/9) has been retained from the start till the end." It also added that the performances of Raghu stand out and credited the directors "technical brilliance, grip over narration and impressive film-making." 

==Award nominations==
;3rd South Indian International Movie Awards
* Best Actress in a Supporting Role (Kannada) – Sindhu Lokanath
* Best Male Debutant (Kannada) – Niranjan Shetty

==References==
 

==External links==
*  
*  

 
 
 
 