Just Around the Corner
{{Infobox film
| name           = Just Around the Corner
| image          =
| image_size     =
| caption        =
| director       = Irving Cummings
| producer       = Darryl F. Zanuck
| writer         = Ethel Hill J. P. McEvoy Darrell Ware
| story          = Paul Girard Smith
| starring       = Shirley Temple Charles Farrell Claude Gillingwater Joan Davis
| music          = Harold Spina
| cinematography = Arthur C. Miller Walter Thompson
| distributor    = 20th Century Fox
| released       =  
| runtime        = 70 minutes
| country        = United States
| language       = English
}}
Just Around the Corner is a 1938 American musical comedy film directed by Irving Cummings. The screenplay by Ethel Hill, Darrell Ware, and J. P. McEvoy was based on the novel Lucky Penny by Paul Girard Smith.  The film focuses on the tribulations of little Penny Hale (Temple) and her architect father (Farrell) after he is forced by circumstances to accept a job as janitor.  The film was the fourth and last cinematic song and dance pairing of Shirley Temple and Bill Robinson.   It is available on DVD and videocassette. The musical score includes the popular standard "I Love to Walk in the Rain" which can be viewed on YouTube.

==Cast==
*Shirley Temple as Penny Hale
*Charles Farrell as Jeff Hale, her father
*Claude Gillingwater as Samuel Henshaw
*Joan Davis as Kitty
*Bert Lahr as Gus
*Bill Robinson as Corporal Jones
*Franklin Pangborn as Waters
*Cora Witherspoon as Aunt Julia Ramsby
*Benny Bartlett as Milton Ramsby
*Marilyn Knowlden as Gwendolyn

==Production==

It was during the making of this film that the relationship between the Temples and 20th Century Fox head Darryl Zanuck took an irreparable turn. Temples mother Gertrude was not happy with the script or the cast of the movie and had a strained meeting with Zanuck to express her frustrations. Dissatisfied with Zanucks response, she went straight to studio chairman Joseph Schenck, who supported Zanuck and refused to take the matter up with him. Direct communication eventually broke down between Zanuck and the Temples. It was the beginning of a chain of events that would eventually lead to Temples parents opting out of her contract in 1940. 

For the casting of the movie, Zanuck brought in Charles Farrell for what was hoped to be a comeback role for him. The director tracked down Farrell at a racquetball club, catching him completely by surprise with the movie offer. The comeback attempt never materialized, however, as his movie career would be over by the end of the decade.  Temples dog Ching-Ching II was brought in as an extra in the movie for $5. When the script called for her to bathe the dog in one of the scenes, she managed to negotiate an extra $2.50. 

==See also==
* Shirley Temple filmography

==References==
 
*  

==External links==
*  
*  
*  

 

 
 
 
 
 
 
 
 


 