Heavenly Daze
{{Infobox Film
| name           = Heavenly Daze
| image          = HeavenlyDazeTITLE.jpg
| image_size     = 
| caption        = 
| director       = Jules White 
| writer         = Zion Myers
| starring       = Moe Howard Larry Fine Shemp Howard Vernon Dent Sam McDaniel Victor Travers Symona Boniface Marti Sheldon Judy Malcolm  
| producer       = Jules White 
| distributor    = Columbia Pictures 
| cinematography = Allen Siegler   Edwin Bryant
| released       =   
| runtime        = 16 47"
| language       = English
| country        = United States
}}

Heavenly Daze is the 109th short subject starring American slapstick comedy team the Three Stooges. The trio made a total of 190 shorts for Columbia Pictures between 1934 and 1959.

==Plot==
Shemp has died and is in heaven, talking with his Uncle Mortimer (Moe, dressed to resemble Moses). Mortimer is on the phone checking to see if Shemp will remain in Heaven or Hell, and it does not look good. Furthermore, it comes to light that cousins Moe and Larry have also not been on their best behavior. Seeing this as an opportunity for his deceased nephew, Uncle Mortimer gives Shemp the chance to reform Moe and Larry from their evil ways so he can gain entry to Heaven. The catch is that Shemp cannot be seen nor heard by anyone on Earth, much to his delight.

Back on Earth, Moe and Larry are crying their eyes out while attending the reading of Shemps will with their attorney, I. Fleecem (Vernon Dent). Seems that Shemp has left behind a grand total of $140, which is $10 less than Fleecems fee of $150 (a rip-off as Fleecem himself admits any other lawyer would take the case for $20). The boys grumble about not having much dough, but invisible Shemp swipes the money back from Fleecem, putting it in Moe and Larrys pockets. When they realize money has magically reappeared, they get spooked, and then remember Shemp saying he would come back to haunt them. They brush this off, but do not completely clear their heads of Shemps ghostly presence.

Afterwards, Moe and Larry rent a luxury apartment, complete with butler Spiffingham (Sam McDaniel), and rent tuxedos. The boys have a grand scheme that involves the conning of wealthy couple the DePuysters (Victor Travers and Symona Boniface) into buying a fountain pen that will write under whipped cream.

Shemp enters the luxurious apartment and terrorises Spiffingham the Butler into leaving. He then smacks Moe and Larry to let them know he is there. Though their butler has run off, Moe and Larry remain, but frightened. The DePuysters show up, and promptly receive dollops of cream in their face when Shemp turns the mixer to "high." After the mixer catches fire, Shemp begins yelling. A few moments later, he awakes, realizing this was all a dream, but then, to his horror, he discovers that the bed is on fire from a cigarette he was smoking. After putting out the fire, Shemp tells his cosuins he dreamt they invented a fountain pen that wirtes under whipped cream Moe hits him with a cream pie-and Larry gives him a pen and tells Shemp to write himself a letter ("Dear Ma...").

==Production notes==
  A Matter Wonder Man, Down to Earth. The Tom and Jerry series also reworked parts of Heavenly Daze the following year in Heavenly Puss.

Larry asks why anyone would want a fountain pen that would write under whipped cream. Moe responds that people might be in a desert where they would not be able to write under water. This refers to the first ball point pen being introduced by Milton Reynolds in 1945 that was a bestseller. It was sold for $10 with the slogan "It writes under water." 

A gag in the films script called for a fountain pen to be thrown into the middle of Larrys forehead. The pen was to be thrown on a wire and into a small hole in a tin plate fastened to Larrys head. However, due to a miscalculation on the part of the special effects department, the sharp pen point punctured Larrys skin, leaving a gash in his forehead. Moe later chased director Jules White around the set because White had promised that the gag would be harmless.   

Heavenly Daze was reworked in 1955 as Bedlam In Paradise, using ample stock footage.    The title is a play on "heavenly days," a popular catch-phrase from radio program "Fibber McGee and Molly." 

==References==
 

== External links ==
*  
*  

 

 
 
 
 
 
 
 