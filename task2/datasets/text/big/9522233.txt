Kannamoochi Yenada
{{Infobox film
| name = Kannamoochi Yenada
| image = Kannamoochi yenada.jpg
| director = V. Priya
| writer = V. Priya
| producer = Raadhika Sarathkumar Zarine Screwvala P. S. Saminathan Prithviraj Sandhya Sandhya Sathyaraj Raadhika Sarathkumar Sripriya Manobala
| cinematography = Preetha Jayaraman UTV Pyramid Saimira UTV Pyramid Saimira
| music = Yuvan Shankar Raja
| editing = MS. Surya Tamil
| country = India
| released =  
| runtime = 133 minutes
}} Tamil comedy Sandhya and Guess Who, UTV and Pyramid Saimira.  It released on 8 November 2007 during Diwali|Deepavali.

==Plot==
Harish Venkatraman (Prithviraj Sukumaran), a software architect by profession who lives in Malaysia, is the millionaire nephew of Maheswaran Iyer (Radharavi). Having lost his parents early in life, he runs the business of his uncle with impressive results. He stumbles upon the psychiatry student Deva (Sandhya), in the most cinematic manner, and as script would have it, falls for her that very instant. Deva is the daughter of Commissioner Arumugam (Sathyaraj) and Dhamayanthi (Raadhika Sarathkumar) in Chennai.

As the plot progresses, Deva is being summoned by her parents for their silver jubilee wedding anniversary back home. Notwithstanding the fact that he has a flourishing business to attend to and much to the wrath of his uncle - who arranges for his wedding with his business partner’s daughter - Harish takes the next flight to Chennai to accompany Deva. He is subjected to a warm welcome by Radhika and cold shoulder by Arumugam. To make matters worse, Harishs uncles (Radharavi) vicious character assassination – in the name of a complaint he sends to the Police Commissioner’s office – doesn’t help Harish in the task of gaining enough confidence among his girlfriend’s parents. Will Harish ever win his love back, now that his chance of impressing Deva’s parents are doomed, forms the rest of the story.

==Cast== Prithviraj as Harish Venkatraman Sandhya as Devasena Arumugam Gounder
*Sathyaraj as Arumugam Gounder
*Raadhika Sarathkumar as Dhamayanthi Arumugam Gounder
*Sripriya as Seetha
*Radharavi as Maheswaran Iyer
*Manobala as Senthilkannu
*Vietnam Veedu Sundaram as Arumugam Gounders father
*Mahanadhi Shankar as Vellaichamy (cameo role)
*Mayilsamy in a cameo role

==Soundtrack==
{{Infobox album
| Name = Kannamoochi Yenada
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Kannamoochi Yaenada.jpg
| Released = 23 August 2007 (India)
| Recorded = 2007 Feature film soundtrack
| Length =
| Label = Big Music
| Producer = Yuvan Shankar Raja
| Last album  = Thottal Poo Malarum (2007)
| This album  = Kannamoochi Yenada (2007)
| Next album  = Kattradhu Thamizh (2007)
}}{{Album reviews
| rev1 = Behindwoods
| rev1Score =    
}}
The music was scored by Yuvan Shankar Raja, teaming up with director V. Priya again after Kanda Naal Mudhal (2005). The soundtrack was released on 23 August 2007 by Vivek Oberoi.  It features 5 tracks, including a retune of the yesteryear hit song "Andru Vandhadhum" from the M. G. Ramachandran|M. G. R.-starrer Periya Idathu Penn, being just the second song to be retuned after A. R. Rahman|A. R. Rahmans "Thottal Poo Malarum" from the film New (film)|New. Noticeably, singer Shankar Mahadevan lent his voice for three songs. Lyrics were provided by Thamarai.

Yuvan Shankar Raja won accolades for creating an "interesting" and "thoroughly enjoyable" album, which was cited to be "an absolute treat to listen to".      Behindwoods, which described the song "Kannamoochi" as "this year’s one of the most enjoyable songs" and the song "Sanjaram" as a "masterpiece", said, that the Yuvan-Priya duo had even bettered their previous venture Kanda Naal Mudhal, giving the album four out of five stars. 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Notes
|- 1 || "Megam Megam" || Haricharan, Shweta Mohan || 5:05 ||
|- 2 || "Kannammoochi Aattam" || Palghat Sriram, Saindhavi, Prasanna, Dr. Narayanan || 3:55 || Incorporates elements of the Carnatic traditional wedding song "Kannoonjal Aadi"
|- 3 || "Andru Vandhadhum" ||  , written by Kannadasan
|- 4 || "Putham Pudhu" || Shankar Mahadevan, Vijay Yesudas || 3:56 ||
|- 5 || "Sanjaram Seiyyum" || Shankar Mahadevan, Madhushree || 4:54 ||
|}

==References==
 

==External links==
*  

 
 
 
 
 
 