Devudu Chesina Manushulu (2012 film)
 
{{Infobox film
| name           = Devudu Chesina Manushulu
| image          = Devudu chesina manushulu poster.jpg
| caption        = Film poster
| director       = Puri Jagannadhg
| producer       = B. V. S. N. Prasad
| writer         = Puri Jagannadh
| screenplay     = Puri Jagannadh
| starring       = Ravi Teja Ileana DCruz
| music          = Raghu Kunche
| cinematography = Shyam K. Naidu
| editing        = S. R. Shekhar
| studio         = Sri Venkateswara Cine Chitra Reliance Entertainment
| distributor    = Reliance Entertainmnent Hari Venkateswara Pictures  (Overseas)  
| released       =   }}
| runtime        = 124 minutes
| country        = India
| language       = Telugu
| budget         =  
| gross          =  (20 Days) http://www.superwoods.com/news-id-dcm-ravi-teja-04-09-122921.htm 
}}
 Tollywood comedy Business Man handled the editing of the film. 

The film was released on 15 August 2012 and met with predominantly negative reviews from critics. Gemini TV bought the telecast rights for the film, telecasting it for the first time on television on 23 October 2012. The film was later dubbed in Hindi under the title "Dadagiri" by GoldMinesTeleFilms. This film was an average grosser at the box office.

==Plot==
The movie opens with Lord Vishnu (Bramhanandam) trying to pacify Lakshmi Devi (Kovai Sarala), who is quite disappointed after listening to words of Narada that Lord Vishnu didnt gave any present to Lakshmi Devi on the eve of her Birthday (Akshaya Tritiya). Lord Vishnu shows a love story between 2 orphans Ravi Teja (Ravi Teja), a settlement broker in Hyderabad and Ileana (Ileana DCruz), a taxi driver living in Bangkok.

Panileni Papayya (M. S. Narayana) sits at a shop, eats a banana & throws the peel on the road. Using the peel, Lakshmi Devi creates a small problem which leads C.I. Subbaraju (Subbaraju) killing Sandy, a trusted goon of the absent-minded Don Prakash Raj (Prakash Raj) of Bangkok. Thus to settle the problem between the two, Ravi Teja goes to Bangkok. At the airport, a taxi driver Goli (Ali (actor)|Ali) steals the bag of Ravi Teja which has his belongings including passport. He chases him in Ileanas cab but his efforts are in vain. Feeling bad upon learning he is an orphan like her, Ileana invites Ravi Teja to her home. Next day he finds Goli, who is a die-hard devotee of Lakshmi Devi and meets Prakash Raj through him and settles the problem. Prakash Raj, who lusts after Ileana, asdks Ravi Teja to send Ileana for a night. He bashes his entire team and runs to Ileana who is already in love with him. Both quarrel and get separated. Unhappy with the turn of events, Lakshmi Devi goes into the past and makes Papayya throw the Banana peel into a dust bin instead on the road.

Due to this the story turns like this: Sandy is caught red-handed by Subbaraju and Sandy is sent to jail. As Sandy is a trusted goon and has all the information regarding his activities, Prakash Raj is furious and wants Sandy released. Ravi Teja is appointed by Prakash Rajs henchmen to strike a deal between Subbaraju and Prakash Raj. Both Ravi Teja and Subbaraju go to Bangkok.

At the airport he sees Ileana for the first time and falls in love with her, due to which he refuses to sit in the taxi of Goli and sits in her taxi. Meanwhile Prakash Rajs men chase them in Golis Taxi. During the chase, Subbaraju, Ravi Teja and Ileana are safe but their taxi is destroyed. After a long argument, Ravi Teja promises to buy a taxi when the deal is over and stay at Ileanas house as Subbaraju is injured. Meanwhile Ravi Teja flirts with Ileana which infuriates her. Both go to Prakash Raj and settle the problem for  2 crore. Prakash Raj sees his sister in Ileana and is ready to do anything for her. Ileana accepts him as her brother. Meanwhile Ravi Teja proposes to Ileana to which she accepts happily.

At the time of closing of deal, Prakash Raj brings the  20&nbsp;million but Subbaraju comes with Police. Meanwhile Ravi Teja & Ileana quarrel but both hug each other, expressing their love. Finally after a fight sequence, Ravi Teja & Ileana are shown to live a happy life & Prakash Raj is a member of their family who would return to their home after six months of imprisonment. The whole story is seen by Lakshmi Devi and Lord Vishnu is successful in making her happy.

==Cast==
  was selected as lead heroine marking her third collaboration with Ravi Teja and director Puri Jagannadh.]]
* Ravi Teja as Ravi Teja
* Ileana DCruz as Ileana
* Prakash Raj as Prakash Raj
* Bramhanandam as Lord Vishnu
* M. S. Narayana as Papayya Subbaraju as Subbaraju Ali as Goli
* Kovai Sarala as Goddess Lakshmi
* Fish Venkat as Venkat
* Jyothirana as Jyothi
* Gabriela Bertante in Item number

==Crew== Vijay
* Choreography: Ajay Sai
* Co-Director: Vijay Ram Prasad
* Co-Producer: Reliance Entertainment, Bhogavalli Bapineedu

==Production== NTR and Ghattamaneni Krishna|Krishna. Filming began March 2012 and was completed in a single schedule.  The film was officially announced on January 27, 2012 and was launched on February 17, 2012.  The first look of the film was released by Puri Jagannadh through Twitter. 

===Filming===
Filming began on March 2, 2012 in Ramoji Film City, Hyderabad.  Major part of the filming was done in a single schedule which was finished on May 15, 2012.  On May 30, 2012 it was announced that filming was completed and that the film would be released in July 2012.  Unit members reveal that Puri catered exactly 35 days for regular shooting. This took place in the Annapurna Studio. This was followed by a set of 14 days catered exclusively for the canning of the songs and another 10 days were catered to shoot the action sequences. That way, the entire film got over in just two months. 

==Release==
The film was released worldwide on August 15, 2012. 

===Critical reception===
Devudu Chesina Manushulu received mixed-to-negative reviews from critics. Mahesh Koneru of 123telugu.com gave a review of 2.25/5 and commented that "Devudu Chesina Manushulu is a surprisingly poor offering from Ravi Teja and Puri Jagan. The movie fails to entertain on any level. Strong and hard hitting dialogues are expected in a Puri Jagan movie and he disappoints this time. Except for a few moments of humour and one or two nice lines, there is nothing to rave about in this movie. Perhaps, some respect for the intelligence of the viewers should have been shown."  Shekhar of OneIndia Entertainment gave a review stating "Watch the film only if you are a hardcore fan of Ravi Teja. The film can be watched for Puri Jagannaths punch dialogues and Ileanas glamour. After a long time, you feel that you spent your valuable time and money unnecessarily on a Ravi Tejas film."  Supergood Movies gave a rating of 2.5/5 stating "Devudu Chesina Manushulu has some good comedy scenes and was targeted to be a mass entertainer. But it missed the target due to poor script".  Apherald gave an average review of rating 3/5. 

===Box office===
The film has collected  55&nbsp;million on its first day.  The film was declared an average grosser. Devudu Chesina Manushulu collected   gross in its lifetime &   nett to the producer. 

==Soundtrack==
  Sony Music label in market and launch of the audio was held at a private auditorium in Hyderabad on same day.  The soundtrack of the film was composed by Raghu Kunche. The album consists of six songs. The lyrics for all songs were penned by Bhaskarabhatla.

==References==
 

==External links==
*  

 

 
 
 
 
 