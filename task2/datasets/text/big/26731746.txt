Gulmohar (2008 film)
 
{{Infobox film
| name           = Gulmohar
| image          = Gulmohar (film).jpg
| caption        = Poster
| director       = Jayaraj
| producer       = P. J. Mathew
| writer         = Deedi Damodaran Ranjith  Siddique  Neenu Mathews Johnson
| cinematography = M. J. Radhakrishnan
| editing        = Vijai Sankar
| distributor    = Orient Films Release 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         =
| gross          =
}}
Gulmohar is a 2008 Malayalam film written by Deedi Damodaran and directed by Jayaraj, starring Ranjith (director)|Ranjith, Siddique (actor)|Siddique, Augustine, and Neenu Mathew.

== Plot ==
Induchoodan is a guest lecturer in a college. He is a revolutionary who dreams equality and justice and  is willing to take up arms against injustice. Induchoodan and his group of six start an operation named Operation April aimed at eliminating those who exploit the tribal people and rape tribal women. They plan to kill Chacko Muthallali, an estate owner who has bad taste towards women.

Induchoodan gets caught and is sentenced to imprisonment. After six years he comes out of jail. The movie talks about the rest of the life of Induchoodan.

== Cast ==
* Ranjith	 ...	Induchoodan (as Ranjith) Siddique	 ...	Harikrishnan
* Jagadish	 ...	Samuel (as Jagadeesh) Augustine	 ...	Appuettan
* Kollam Thulasi	 ...	DySP Sivan
* Sudheesh	 ...	Anwar
* Nishanth Sagar	 ...	Kuriakose
* Meghanadhan	 ...	Rasheed
* Subair	 ...	Inspector Alex
* Jayakrishnan	 ...	Unnikrishnan
* I. M. Vijayan	 ...	Kariyathan
* Rajamani	 ...	Chacko
* Arun	 ...	Harikrishnan
* Vijayan V. Nair	 ...	Dasettan (as Vijayan)
* Ibrahim Vengara	 ...	Pothuval
* Sabitha Jayaraaj	 ...	Sunila Teacher (as Sabitha Jayaraj)
* Meenu Mathews	 ...	Gayathri (as Meenu Mathew)
* Ambika Mohan	 ...	Lakshmi
* Kaviyoor Ponnamma	 ...	Devaki
* Surabhi	 ...	Nirmala
* Meera Vasudev	 ...	Chithra
* Zeenath	 ...	Paulachans Wife

== External links ==
*  
* http://www.indiaglitz.com/channels/malayalam/preview/10115.html
* http://www.cinefundas.com/2008/10/20/gulmohar-malayalam-movie-review
* http://popcorn.oneindia.in/title/501/gulmohar.html
* http://www.nowrunning.com/movie/5146/malayalam/gulmohar/preview.htm

 
 
 


 