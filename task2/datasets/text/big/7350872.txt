Let George Do It!
 
{{Infobox film
| name = Let George Do It!
| image = "Let_George_Do_It!"_(1940).jpg
| caption = 
| director = Marcel Varnel 
| producer = Michael Balcon Basil Dearden 
| writer = Basil Dearden John Dighton Angus MacPhail Austin Melford  George Formby Phyllis Calvert Garry Marsh
| music = Ernest Irving Eddie Latta 
| cinematography = Ronald Neame 
| editing = Ray Pitt
| studio  = Ealing Studios ABFD
| released = 17 August 1940 (UK)
| runtime = 82 minutes
| country = United Kingdom
| language = English
| budget = 
| preceded_by = 
| followed_by = 
}}
 comedy musical musical war Second World War. 

==Plot== British undercover agent and thinks he is one too. The duo manage to find and break a code that the Nazis are using to sink Allied shipping. A noted sequence was a dream where George had been given a truth drug by the Nazi conductor Mendez, in which he gives Hitler a right hook.

==Cast== George Formby as George Hepplewhite
* Phyllis Calvert as Mary Wilson
* Garry Marsh as Mendez
* Romney Brent as Slim Selwyn
* Bernard Lee as Oscar
* Coral Browne as Iris
* Helena Pickard as Oscars wife
* Percy Walsh as Schwartz
* Ronald Shiner as the Clarinetist Ben Williams as Radio Operator on SS Macaulay (uncredited) Jack Hobbs as Conway

==Critical reception==
Allmovie noted the film as, "one of the best and most successful of the George Formby vehicles." 

==References==
 

==External links==
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
 
 