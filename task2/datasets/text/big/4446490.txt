It Takes Two (1995 film)
{{Infobox film
| name           = It Takes Two
| image          = It takes two.jpg
| caption        = Theatrical release poster
| director       = Andy Tennant
| producer       = Mel Efros Keith Samples
| writer         = Deborah Dean Davis
| starring       = Kirstie Alley Steve Guttenberg Mary-Kate Olsen Ashley Olsen
| music          = Ray Foote Sherman Foote
| cinematography = Kenneth D. Zunder
| editing        = Roger Bondelli
| distributor    = Warner Bros. Family Entertainment (North America) Rysher Entertainment (International)
| released       =  
| runtime        = 101 minutes
| country        = United States
| language       = English
| gross          = $19,474,589
}}
 It Takes Two", by Marvin Gaye and Kim Weston, which was featured in the closing credits. 

It Takes Two boasts some similarities to Mark Twains The Prince and the Pauper, which was pointed out in Leonard Maltins review of the film. 
== Plot ==
Two unrelated young girls who happen to look identical suddenly meet. Amanda Lemmon (Mary-Kate Olsen) is an orphan, and she is about to be adopted by the Butkises, a family whom she doesnt like. She actually wants her child-loving social worker, Diane Barrows (Kirstie Alley), to adopt her instead. Diane would like to do so, but authorities will not let her because of her low salary. Alyssa Callaway (Ashley Olsen) is coming home from her boarding schools piano recital competition, only to find that her wealthy father, Roger (Steve Guttenberg), is about to marry Clarice Kensington (Jane Sibbett) a socialite who threatens to send her soon-to-be stepdaughter to boarding school in Tibet. 

The girls switch places and find out that Roger and Diane would fit together perfectly. So they arrange "chance" meetings with the desired result: they fall in love with each other. After some turbulence, Alyssa (who poses as Amanda) ends up being adopted by the Butkises. She and Diane (while looking for Alyssa) find out the only reason they have adopted so many kids was for them to work in their salvage yard. 

When Clarice secretly spies on Roger and Diane, she decides to move up the wedding from the next month to the next day. Roughly two hours before it, Amanda, who poses as Alyssa, proves to the family butler, Vincenzo (Philip Bosco), who she really is. He summons to have the real Alyssa picked up from the Butkises salvage yard to stall the wedding. Once she and Diane show up, Roger stops it and tells Clarice that he fell in love with Diane. Furious, she slaps him and prepares to do the same to both Amanda and Alyssa but is stopped by both Vincenzo and Diane. She storms out, embarrassed. Alyssa embarrasses her even more by stepping on her gown, causing the skirt to rip off, revealing her white underpants and stockings in front of everybody, including people with cameras. She then runs off crying, desperately trying to hide her white knickers, shouting for her father, whom just laughs at her. Roger and Diane both find out in the end that it was Amanda and Alyssa that arranged all the meetings between both of them the entire time but it ends happily.

== Characters ==
* Amanda Lemmon (Mary-Kate Olsen) is an orphan. She loves to play stickball. She has the idea for herself and Alyssa to switch places for a couple of days.
* Alyssa Callaway (Ashley Olsen) is a rich girl and the only child of Roger Callaway, the owner of Camp Callaway, the summer camp for the orphans. She is a prize-winning pianist and an equestrian. She thinks her fathers fiancee, Clarice Kensington, seems nice until she overhears her saying rude things about the house and her mother. She attends Camp Callaway after switching places with Amanda.
* Roger Callaway (Steve Guttenberg) is a very wealthy widower. He owns Camp Callaway, which he founded with his late wife, and currently resides in a large vacation home across the lake from it. He begins to have doubts about marrying Clarice after he meets Diane and they click.
* Diane Barrows (Kirstie Alley) is a social worker who takes care of the orphans. She especially loves Amanda and would like to adopt her but does not make enough money to be allowed to do so. Alyssa also especially likes her. She also wants to find love and thinks she might have a chance after meeting Roger.
* Clarice Kensington ( , the opposite of Diane. She hates kids and baseball and only wants to marry Roger for his money. She also convinces him that Alyssa is too spoiled and gets away with bad behavior. The main antagonist, she has her underwear exposed at the end of the film.
* Vincenzo (Philip Bosco) is Rogers butler, best friend, and right-hand man as well as a father figure to Alyssa since the day she was born.
* Harry (Ernie Grunwald) and Fanny Butkis (Ellen-Ray Henessy) are Amandas potential adoptive parents. Although she wants to be adopted, she dislikes them, having heard that they "collect kids" and will "take anybody".  They have several adopted children and a biological son, Harry, Jr. (Dov Tiefenbach).

== Other characters ==
 
 
* Carmen (Michelle Grisom), Amandas closest friend at the orphanage
* Frankie (Desmond Robertson), Amandas friend at the orphanage, who makes fun of her for being adopted by the Butkises
* Tiny (Tiny Mills)
* Patty (Shanelle Henry)
* Anthony (Anthony Aiello)
* Wanda (La Tonya Borsay)
* Michelle (Michelle Lonsdale-Smith)
* Jerry (Sean Orr)
* Emily (Elizabeth Walsh)
* Blue Team Kid (Michael Vollans)
* Bernard Louffier (Paul OSullivan)
* Mr. Kensington (Lawrence Dane)
 
* St. Barts Priest (Gerrard Parkes)
* Muffy Bilderberg (Gina Clayton)
* Craig Bilderberg (Doug OKeefe)
* Waiter at Party (Mark Huisman)
* Miss Van Dyke (Marilyn Boyle)
* Brenda Butkis (Annick Obonsawin)
* Billy Butkis (Austin Pool)
* Airport Tractor Driver (Philip Williams)
* Butkises Neighbor (Vito Rezza)
 

== Awards and nominations == Nickelodeon Blimp Award for Favorite Movie Actress (Mary-Kate and Ashley Olsen) 
* Nominated - Nickelodeon Blimp Award for Favorite Movie Actress (Kirstie Alley)
* Nominated -  , 2012. Web. February 11, 2012  . 
* Nominated - Young Artist Award for Best Performance by an Actress Under Ten (Mary-Kate Olsen) 

== Reception ==
Kevin Thomas from Los Angeles Times called It Takes Two "a predictable but fun romp."  Roger Ebert called it "harmless and fitfully amusing" with "numbingly predictable" plot and praiseworthy performances and rated it two out of four stars. 

The website Parent Previews graded this movie an overall B as a family-friendly film with "only a couple of bad words and a bit of child intimidation from the bad guys," and Rod Gustafson from that website called it "predictable" with a "happy ending" that children can enjoy. 

== References ==
 

== External links ==
 
*  
*  
*  
*  

 

 
 
 
 
 
 
 
 
 
 
 