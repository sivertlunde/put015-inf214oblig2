Sagan om Karl-Bertil Jonssons julafton
{{Infobox film
| name = Sagan om Karl-Bertil Jonssons julafton
| image = Karlbertil.png
| alt =
| caption =
| director = Per Åhlin
| producer =
| writer = Tage Danielsson
| starring = Tage Danielsson Per Andrén Toivo Pawlo Marianne Stjernqvist Åke Fridell Catrin Westerlund
| music =
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime = 23 minutes
| country = Sweden
| language = Swedish
| budget =
| gross =
}} FST in Finland.

The film was dubbed to English into 1987, with Bernard Cribbins as the voice-over.

==Cast==
* Tage Danielsson as Narrator
* Per Andrén as Karl-Bertil Jonsson
* Toivo Pawlo as Tyko Jonsson
* Marianne Stjernqvist as Mrs Jonsson
* Åke Fridell as H.K. Bergdahl
* Catrin Westerlund as Mrs Bergdahl

==Criticism== SVT started broadcasting it annually, with critics arguing that it is too political and that stealing isnt really on par with traditional Christmas spirit. Danielssons response was that it is only a fairy tale and shouldnt be overanalyzed.   

==References==
 

== External links ==
*  
*  

 
 
 
 
 
 
 
 
 


 
 