No Way to Forget
 
{{Infobox Film
| name           = No Way to Forget
| image          = 
| image_size     = 
| caption        = 
| director       = Richard Frankland
| producer       = John Foss
| writer         = Richard Frankland
| narrator       = 
| starring       = David Ngoombujarra Christina Saunders Shane Franzis
| music          = 
| cinematography = 
| editing        = 
| distributor    = Golden Seahorse Productions
| released       = 1996
| runtime        = 12 minutes
| country        = Australia
| language       = English
| budget         = 
| gross          = 
| preceded_by    = 
| followed_by    = 
}} SBS TV.  It screened at the 1996 Cannes Film Festival in the category of Un Certain Regard.    

No Way to Forget is based on Franklands experiences while working as a Field Officer for the Royal Commission into Aboriginal Deaths in Custody. 

==Awards== AFI Award]] (1996) - Best Short Film
• AFI Award (1996) - Best Screenplay in a Short Film 
• St Kilda Film Festival (2000) - Best New Director
• Cannes International Film Festival (1996) - Un Certain Regard

==References==
 

==External links===
* 

 
 
 
 
 
 
 
 


 