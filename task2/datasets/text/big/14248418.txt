Krishna (1996 film)
{{Infobox film
| name           = Krishna
| image          = Krishna1996film.jpg
| image_size     = 
| caption        = 
| director       = S. Deepak
| producer       = Dhirajlal Shah
| writer         = 
| screenplay     = Tinu Anand	
| narrator       = 
| starring       = Sunil Shetty Karishma Kapoor Om Puri Shakti Kapoor Tinu Anand
| music          = Anu Malik
| Lyricist       =   Teja Sameer Reddy
| editing        = V. N. Mayekar
| distributor    = Asian
| released       = August 2, 1996
| runtime        = 164 mins
| country        = India Hindi
| budget         = 
}}
Krishna is a 1996 Indian movie from S. Deepak starring Sunil Shetty, Karishma Kapoor, Om Puri, Shakti Kapoor, Tinu Anand. The film was a huge success at the box office and was declared a HIT.

==Plot==

Krishna (Sunil Shetty) is a new name in the world of crime, whose exploits attract the attention of Raja (Shakti Kapoor), an heir to a dead underworld don. Raja is just reduced to remain at the mercy of Bhujang Rao aka Bhau (Mohan Joshi), another don. Raja loathes Bhujang & his attitude & extends a hand to Krishna, who accepts the offer. Bhau tries to convince Krishna of a brighter future if he joins Bhaus gang, but Krishna remains with Raja. Meanwhile, Rashmi (Karisma Kapoor), a club dancer, is lamenting the loss of Sunil, her love. Sudhir, who was Krishnas lookalike, was a pilot who died during his training session. Rashmi refuses to believe so, hoping that Sunil will return.

During a gig, Rashmi runs into Krishna & is thrilled to see him. Krishna tells her firmly that he is not Sunil. Only when she sees that a tattoo bearing Rashmis name, which was supposed to be on Sunils arm, is missing from Krishnas arm, she leaves broken hearted & dejected. However, some goons try to rape her & she is saved by Krishna. Krishna reveals that he is indeed Sunil & goes on to tell the truth. He reveals that his instructor was actually acting as a link for some criminals. He forced Sunil to land on an abandoned place to meet the criminals.

One of the criminals was Cobra (Tinu Anand), an infamous name in underworld. The deal went wrong & Sunils instructor was killed. Sunil survived somehow & nobody had seen his face. He was wrongfully arrested as a suspect & minister Amar Prabhakar (Om Puri) bailed him out. Amar told him that if he wants to survive, he has to take down all the people who could pose a possible threat. So, Sunil got booked under another name - Krishna. Meanwhile, Cobra returns to India again. It is revealed that both Bhau & Raja are on his payroll. Cobra decides to make Raja the de facto ruler of his operations in India. Raja tells him about Krishna.

Cobra & Sunil meet, where Cobra suspects that he has seen Krishna somewhere else. Sunil knows that Cobra may recognize him, but now he has come too far. Later, Raja drops by a brothel, where Sunil is surprised to see his sister perform. His sister is shocked to see him too & commits suicide on the spot. Sunil learns from another dancer that most of the dancers have same story to share - a man came in their life pretending to be a well to do person, married them & sold them, where they were raped before ending up here. He realizes that the man whom he gave his sister to is responsible for destruction of so many girls lives.

The dancer also tells him that he keeps a record, mostly videotapes of the people he has sold the girls to, in order to blackmail them. In a fit of rage, Sunil accosts the man, but he dies. Sunil receives another blow when he learns that his mother has died. Amar keeps the truths under wraps & gives Sunils mother the funeral she deserved. Sunil also learns that Amars own daughter has been kidnapped & that Cobra has the tape which may uncover the identity of his sisters rapist. On learning of Cobras another deal with Bhau & Raja, Sunil decides to end it all. In a bloody aftermath, the criminals are killed.

Sunil finds the tape, whereby he receives a shocker that his sister was raped by none other than Amar. Here, it is revealed that Amars daughter is not kidnapped; it was only a ploy to garner public sympathy for next elections. However, the Commissioner (Kulbhushan Kharbanda) learns of this fraud & Amar has him captured. Next day, Amar is giving a speech when an enraged Sunil turns up. He makes his way to Amar. The Commissioner, who has somehow succeeded in escaping, manages to save Sunil from being taken down. He pleads Sunil to let the police take it over. Amar tries to run away, taking advantage of the situation, but Sunil removes a fake Sudarshan Chakra from an idol of lord Krishna & beheads Amar with it. With Amar dead, Sunil surrenders.

==Cast==

*Sunil Shetty as Sunil / Krishna
*Karisma Kapoor as Rashmi
*Om Puri as Amar Prabhakar
*Shakti Kapooras Raja
*Mohan Joshi as Bhujang Rao aka Bhau
*Tinu Anand as Cobra
*Kulbhushan Kharbanda as Commissioner
*Rohini Hattangadi as Sunils mother
* Lalit Tiwari as Sunils Instructor

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Jhanjariya (Male)" Abhijeet
|- 
| 2
| "Jhanjariya (Female)"
| Alka Yagnik
|- 
| 3
| "Koi Kaise Mohabbat"
| Kumar Sanu, Sadhana Sargam
|-
| 4
| "Main is Kadar Mere Mehboob"
| Vinod Rathod, Alka Yagnik
|-
| 5
| "Darwale Pe Tere Baarat"
| Abhijeet
|-
| 6
| "Main Kya Thi Kya Se Kya Ho Gayi"
| Bela Sulakhe
|-
| 7
| "Masti Masti"
| Anu Malik
|-
| 8
| "Darwaje Pe Tere Baraat (Sad)"
| Abhijeet
|}

==External links==
*  On VIDUBA
*  

 
 