Africadalli Sheela
{{Infobox film
| name = Africadalli Sheela
| image =
| caption = 
| director = Dwarakish
| writer = Dwarakish
| producer = Dwarakish
| starring = Charan Raj   Sahila 
| music = Bappi Lahiri   Vijayanand ( )
| cinematography = R. Deviprasad
| editing = Goutham Raju
| studio  = Dwarakish Chitra
| released =  1986
| runtime = 105 min
| language = Kannada  
| country = India
}}
 Kannada fantasy adventure film written, directed and produced by Dwarakish. Made on the similar lines as the Hollywood film Sheena (film)|Sheena, the film was extensively shot at the forest ranges in African continent. This film was the first Indian film to have shot in the African forest ranges. 

The film featured Charan Raj and Sahila in the lead roles along with Dwarakish, Srinivasa Murthy and Kalyan Kumar in supporting roles.  The music was composed by Bappi Lahiri to the lyrics of Chi. Udaya Shankar and R. N. Jayagopal. Dwarakish remade the film in Tamil as Kizhakku Africavil Sheela which starred Suresh (actor)|Suresh, Nizhalgal Ravi and Sahila reprising her character while she went on to reprise her character in its Hindi remake titled Sheela starring Nana Patekar.  

==Cast==
*Charan Raj as Shankar
*Sahila as Sheela
*Srinivasa Murthy as Ramu
*Dwarakish
*Kalyan Kumar as Rao Bahaddur
*Disco Shanti
*Thoogudeepa Srinivas as Raacha Kanchana
*Sudheer

==Soundtrack==
All the songs are composed and scored by Bappi Lahiri.  This film marked the entry of singer K. S. Chithra to the Kannada cinema.

{|class="wikitable"
! Sl No !! Song Title !! Singer(s) !! Lyricist
|-
| 1 || "Sheela Sheela" || Bappi Lahiri, Nazia Hassan || Chi. Udaya Shankar
|-
| 2 || "Thakka Dhimi Thana" || Manjula Gururaj || Chi. Udaya Shankar
|-
| 3 || "Hey Hennu Illade" || Manjula Gururaj || R. N. Jayagopal
|-
| 4 || "Thampada Tholu" || K. J. Yesudas, Manjula Gururaj  || R. N. Jayagopal
|-
| 5 || "Sheela O My Sheela" || K. J. Yesudas, K. S. Chithra || Chi. Udaya Shankar
|}

==References==
 

== External links ==
* 
* 

 

 
 
 
 
 
 
 
 


 