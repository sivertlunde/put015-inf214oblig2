Aitraaz
 
 
{{Infobox film
| name           = Aitraaz
| image          = Aitraaz.jpg
| alt            = The poster features three people, one man and two women, with green and black themed colour in the background. Text at the bottom of the poster reveals the tagline of the film while the text at the bottom of the poster reveals the title, name of the director, name of the producer, name of the distributor, the release date and the rest of the credits.
| caption        = Theatrical release poster
| director       = Abbas-Mustan
| producer       = Subhash Ghai
| writer         = Shiraz Ahmed Shyam Goel
| story          = Shiraz Ahmed Shyam Goel
| screenplay     = Shiraz Ahmed Shyam Goel
| based on       =  
| starring       = Akshay Kumar Kareena Kapoor Priyanka Chopra Amrish Puri Paresh Rawal Annu Kapoor
| music          = Himesh Reshammiya
| cinematography = Ravi Yadav
| editing        = Hussain A. Burmawala
| studio         = Mukta Arts
| distributor    = Mukta Arts
| released       = 12 November 2004
| country        = India
| language       = Hindi
| runtime        = 159 minutes 
| gross          =   
}}
 Hindi romantic thriller film produced by Subhash Ghai for Mukta Arts and directed by Abbas-Mustan. It tells the story of a man accused of sexual harassment by his female superior. The film stars Akshay Kumar, Kareena Kapoor and Priyanka Chopra and was the third film collaboration between Kumar and Kapoor, and Kumar and Chopra. Aitraaz features Amrish Puri, Paresh Rawal and Annu Kapoor in supporting roles. Its score was composed by Himesh Reshammiya, with lyrics by Sameer. The film is inspired by the 1994 American film Disclosure (film)|Disclosure.
 Best Supporting Best Performance Kannada as Shrimathi in 2011.

==Plot==
Raj Malhotra (Akshay Kumar) is employed by a telecommunications company, Air Voice. Priya Saxena (Kareena Kapoor), who is looking for a full-time job, goes to Rajs house for an interview, mistaking him for barrister Ram Chautrani (Annu Kapoor), a neighbour and Rajs friend. Raj and Priya fall in love, marry and are expecting their first child.

Raj expects to be promoted to CEO when the companys chairman (Amrish Puri) arrives with his new wife, Sonia Roy (Priyanka Chopra) to announce the promotions. Sonia Roy is named the companys new chairman; after a discussion with her husband, she announces the promotions. The CEO position goes to Rajs friend Rakesh (Vivek Shauq), and Raj is named as one of the board of directors. At a party, Raj, accompanied by Priya, learns about his new boss, Sonia Roy. Priya is surprised that Sonia is the wife of the former chairman (and half his age). Raj and his colleagues talk about Sonia Roy and the age difference between her and her husband, and Raj jokes that his magnetic personality was responsible for his promotion.

A flashback explores Rajs previous relationship with Sonia. Five years earlier, Raj and Sonia (then a model) meet at a beach in Cape Town. They fall in love and move in together; Sonia becomes pregnant with Rajs child,which makes him happy. But Sonia refuses Rajs marriage proposal and says she is going to terminate the pregnancy. She wants wealth, fame, power and status, and a child would be in the way; their relationship ends. 

On the next day, Rakesh tells Raj about a defect in the companys new mobile handset: a call goes to two people simultaneously—the intended recipient and another person on the phones contact list. Raj needs Sonias permission to halt production, and she invites him to her house to discuss the matter.

Sonia aggressively tries to pursue Raj, who resists. Although he repeatedly rejects her advances, Sonia continues trying to seduce him. For a moment it seems that Raj has given in to Sonias advances but after looking at his locket which has Priyas portrait in it, Raj realises what he is doing and tries to leave. As Raj leaves, she threatens to punish him for spurning her. The next day, he learns that Sonia has told her husband that Raj harassed her sexually. Since he has admitted finding Sonia attractive, his claim of innocence is not believed, and the company pressures him into a resignation.

Raj asks Ram Chauthrani to take his case; Chauthrani tells him not to resign, and to keep going to work. The case goes to court; Sonia and Roy engage a lawyer, Patel (Paresh Rawal). Although the bulk of the evidence is at first against Raj, his friend returns from Bangkok and gives him a tape which recorded Rajs encounter at Sonias house. After the tape is proven genuine, Chauthrani is struck by a car driven by someone hired by Sonia and the tape is substituted.

When Priya asks Raj why he called their bank manager from Sonias house, he replies that he had called Rakesh; the call went through to the bank manager as well. Priya (also a lawyer) continues the case after Chauthranis injury. She uses Rakeshs phone in evidence against Sonia, exposing her earlier relationship with Raj. It is revealed that Sonia married Roy for money, power and status; when he could not satisfy her sexually, she tried to resume her relationship with Raj. Priya wins the case and Roy leaves Sonia. Guilt-stricken and humiliated, Sonia commits suicide by jumping from a building.

==Cast==
* Akshay Kumar as Raj Malhotra
* Kareena Kapoor as Priya Saxena Malhotra
* Priyanka Chopra as Sonia Roy
* Amrish Puri as Ranjit Roy
* Anu Kapoor as Barrister Ram Chauthrani
* Paresh Rawal as Advocate Patel
* Vivek Shauq as Rakesh Sharma
* Preeti Puri as Jenny (Rajs secretary)
* Upasna Singh as Kanchan
* Dinesh Lamba as Choitranis assistant

==Production== his sexual-assault case in the newspapers.    According to the directors, they were fascinated by the possibility of the situation in reverse.  About the films unusual title, they said the word "aitraaz" was colloquial and suited the subject. 

{{multiple image
 
| align     = left
| direction = vertical
| footer    =  Kumar (top), Kapoor (middle) and Chopra (bottom): The lead cast of the film
| width     =
 
| image1    = AkshayKumar.jpg
| width1    = 150
| alt1      = Man looking forward with a smile, wearing a white shirt
| caption1  =
 
| image2    = Kareena kapoor vaio launch.jpg
| width2    = 150
| alt2      =
| alt2      = Young woman looking left with a gentle smile, wearing a black dress
| caption2  =
 
| image3    = Priyanka Chopra at the launch of Barfi! promo 12.jpg
| width3    = 150
| alt3      = Young woman looking right with a gentle smile, wearing a green dress
| caption3  =
}}

In early 2004 the media reported that Akshay Kumar, Kareena Kapoor and Priyanka Chopra were cast in lead roles, making it the third film collaboration between Kumar and Chopra after highly successful films Andaaz (2003) and Mujhse Shaadi Karogi (2004).    Kumar was cast as a working man accused of rape at his workplace; Kapoor is his supportive wife, who goes to extremes to defend him.  According to the directors, Kumar was cast against type; an action star, they wanted him to underplay his character.  Abbas-Mustan, known for stylish thrillers and intriguing antagonists,  cast Chopra in her first negative role.  She plays a woman, married to a business magnate more than twice her age (played by Amrish Puri), who falsely accuses her ex-lover (Kumar) of raping her to seek revenge. Chopra was initially apprehensive about such a bold character, due to the controversial nature of sexual harassment.    Abbas-Mastan and Subhash Ghai (the films producer) convinced her to accept the role, assuring her that the film would not damage her career.    

According to Kumar, Raj (accused of rape by his female boss) is realistic and could be described as a "new-age  , Kapoor remarked that "every Indian woman could identify with her character" of Priya.    She said her role (a woman whose husband is accused of rape by his former girlfriend) is supportive; she stands by him, as any Indian woman would. 

Chopra described her character (Sonia, an ambitious woman who accuses her employee of sexual harassment) as "charming and focused", commenting that her "philosophy is that she has to achieve her goals at any cost. She knows one thing: that nothing can come in between her desires and herself."  Chopra did not identify with the character, considering it a "man-eater role" because of her conservative real-life upbringing.  Chopra found it challenging to play such an "extremely negative character", and had to mentally prepare herself for an hour before each scene to get inside her character.  She improvised gestures and expressions; beyond actions and dialogue, her attitude was aggressive and bold. 

The film was shot in Cape Town, Goa, Pune and Mumbai.  During filming of the sexual-harassment scene, Chopra wept; it took the directors several hours to remind her she was only playing a character, and additional filming was postponed. 

==Soundtrack==
{{Infobox album  
| Name       = Aitraaz
| Type       = Soundtrack
| Artist     = Himesh Reshammiya
| Cover      = Aitraaz_albumcover.jpg
| Border     = yes
| Alt        = The poster showing a women standing in orange dress, with white themed colour in the background
| Caption    = Soundtrack cover
| Released   = 24 September 2004
| Recorded   = 2004 Feature film soundtrack
| Length     = Hindi
| Sony Music
| Producer   =
| Last album = Dil Ne Jise Apna Kahaa (2004)
| This album = Aitraaz (2004)
| Next album = Dil Maange More  (2004)
}}
 Sony Music,  topping charts on a number of platforms in India.    The video of the title track with Kumar and Chopra was shot in one take with a steadycam. 

The soundtrack was generally well received by music critics, who praised its lyrics and vocals. Planet Bollywood gave a rating of 7 out of 10, calling it a "good album".  Joginder Tuteja of Bollywood Hungama rated the album 3 out of 5, praising "I Want To Make Love To You" (all three versions): "Sunidhi Chauhan is excellent in this wonderfully-composed track that shocks everyone with the intensity of the lyrics and the music". He concluded, "Except for two or three average songs here and there, the majority of songs in Aitraaz do keep you engaged". 

{{track listing
| headline       = Track listing
| extra_column   = Singer(s)
| lyrics_credits = yes
| title1         = Aankhen Bandh Karke
| extra1         = Udit Narayan, Alka Yagnik
| music1         = Himesh Reshammiya
| lyrics1        = Sameer
| length1        = 4:41
| title2         = Tala Tum Tala Tum
| extra2         = Alka Yagnik, Jayesh Gandhi, Udit Narayan
| music2         = Himesh Reshammiya
| lyrics2        = Sameer
| length2        = 6:58
| title3         = Woh Tassavvur
| extra3         = Udit Narayan, Alka Yagnik
| music3         = Himesh Reshammiya
| lyrics3        = Sameer
| length3        = 5:24
| title4         = Nazar Aa Raha Hai
| extra4         = Udit Narayan, Alka Yagnik
| music4         = Himesh Reshammiya
| lyrics4        = Sameer
| length4        = 5:07
| title5         = Gela Gela Gela
| extra5         = Adnan Sami, Sunidhi Chauhan
| music5         = Himesh Reshammiya
| lyrics5        = Sameer
| length5        = 4:42
| title6         = Aitraaz – I Want to Make Love to You
| extra6         = Sunidhi Chauhan
| music6         = Himesh Reshammiya
| lyrics6        = Sameer
| length6        = 5:11
| title7         = Yeh Dil Tumpe Aa Gaya
| extra7         = KK (singer)|KK, Alisha Chinai
| music7         = Himesh Reshammiya
| lyrics7        = Sameer
| length7        = 5:23
| title8         = Aitraaz – I Want to Make Love to You (Male)
| extra8         = Kunal Ganjawala
| music8         = Himesh Reshammiya
| lyrics8        = Sameer
| length8        = 5:10
| title9         = Aankhen Bandh Karke (Close Your Eyes Mix)
| extra9         = Udit Narayan, Alka Yagnik
| music9         = Himesh Reshammiya
| lyrics9        = Sameer
| length9        = 5:00
| title10        = Gela Gela Gela (The Dance on the Beach Mix)
| extra10        = Adnan Sami, Sunidhi Chauhan
| music10        = Himesh Reshammiya
| lyrics10       = Sameer
| length10       = 4:00
| title11        = Woh Tassavvur (Love is Forever Mix)
| extra11        = Udit Narayan, Alka Yagnik
| music11        = Himesh Reshammiya
| lyrics11       = Sameer
| length11       = 4:00
| title12        = Tala Tum Tala Tum (The Cyclonic Dance Mix)
| extra12        = Alka Yagnik, Jayesh Gandhi, Udit Narayan
| music12        = Himesh Reshammiya
| lyrics12       = Sameer
| length12       = 5:00
| title13        = Nazar Aa Raha Hai
| extra13        = Udit Narayan, Alka Yagnik
| music13        = Himesh Reshammiya
| lyrics13       = Sameer
| length13       = 4:00
| title14        = Yeh Dil Tumpe Aa Gaya (The Slip and Slide Mix)
| extra14        = KK, Alisha Chinai
| music14        = Himesh Reshammiya
| lyrics14       = Sameer
| length14       = 5:00
| title15        = Aitraaz – I Want to Make Love to You (The Passion Mix)
| extra15        = Sunidhi Chauhan
| music15        = Himesh Reshammiya
| lyrics15       = Sameer
| length15       = 4:00
}}

==Marketing and release==
The first-look poster of the film, with the tagline "In the world of women, you either play by their rules or else...", was received positively by critics;  the films trailers were also well received. They and the films music aided its marketing. 

Aitraaz was released worldwide on Diwali, 12 November 2004, to positive reviews and moderate box-office success.  It became the eleventh-highest-grossing film of the year in India, and a commercial success.    Kannada as Shrimathi, starring Upendra, Priyanka Trivedi and Celina Jaitley. 

==Critical reception==
Aitraaz received generally positive reviews from critics, who praised its direction, cinematography, dialogue, music and performances, particularly Chopras. The BBC noted the films bold theme, good music and performances and remarked that "Abbas-Mustaan have done a good job in Indianising the whole concept". It also praised other aspects of the film, describing it as "a gripping edge of the seat drama that keeps viewers glued to their seats".  India Today film critic Anupama Chopra gave it a positive review, remarking that "Aitraaz has no pretensions. Its good timepass."  

 , on the other hand, was particularly impressed with the court scene which he considered to be "splendid", and rated the film 3 out of 5. He considered Chopras performance to be a triumph, remarking: "A star is born! As the predatory social-climbing seductress who can go to any length to satiate her lust for life, Priyanka Chopra rocks the scene like never before." However, Jha believed that Kareena was miscast and seemed a little awkward in a non-glamorous role, but "comes into her own in the climactic courtroom sequence where she dons the lawyers coat to bail her husband out".   Taran Adarsh of Bollywood Hungama rated the film 3.5 out of 5, describing it as "a well-crafted thriller" and complimenting the directors opting for "a theme that has been untouched on the Indian screen so far" and the films "dramatic moments".    Like Jha, he believed that the film belonged entirely to Priyanka Chopra, and was impressed with her understanding of the character, and the way she drew the hatred of the audience.  He also complimented the performances by Kapoor and Kumar. 

Sudhish Kamath of The Hindu criticised the films second half, commenting that "though first half of the movie is well-paced, the second half sags with the songs and twists forced into the plot to buy time", but stated that it was "passable with its slick production, a few funny lines, glam quotient and star appeal." 

==Awards and nominations== Best Sound Best Sound Re-Recording.
{| class="wikitable" style="font-size: 95%;"
|-
! Award
! Category
! Recipients and nominees
! Outcome
|-
| Apsara Film & Television Producers Guild Awards  Best Actress in a Supporting Role
| Priyanka Chopra
| 
|-
| Bengal Film Journalists Association Awards 
| Best Actress
| Priyanka Chopra
| 
|-
| rowspan="2" | Filmfare Awards   Best Supporting Actress
| rowspan="2" | Priyanka Chopra
| 
|- Best Performance in a Negative Role
| 
|-
| Global Indian Film Awards  Best Villain Female
| Priyanka Chopra
| 
|-
| rowspan="10" | International Indian Film Academy Awards  Best Actress
| Kareena Kapoor
| 
|- Best Supporting Actor
| Paresh Rawal
| 
|- Best Music Director
| Himesh Reshamiya
| 
|- Best Lyricist
| Sameer
| 
|- Best Male Playback
| Udit Narayan
| 
|- Best Female Playback
| Alka Yagnik
| 
|- Best Story
| Shiraz Ahmed and Shyam Goel
| 
|- IIFA Award Best Editing
| Hussain A. Burmawala
| 
|- Best Sound Recording
| Rakesh Rajan
| 
|- Best Sound Re-Recording
| Anup Dev
| 
|-
| rowspan="2" | Screen Awards   Best Villain
| Priyanka Chopra
| 
|- Jodi No. 1
| Akshay Kumar and Priyanka Chopra
| 
|-
| rowspan="3" | Zee Cine Awards  Best Actor in a Negative Role
| Priyanka Chopra
| 
|- Best Music Director
| Himesh Reshamiya
| 
|- Best Lyricist
| Sameer
| 
|-
|}

==References==
;Notes
 

;Citations
 

==External links==
*  
*  

 
 
 
 

 
 
 
 
 
 