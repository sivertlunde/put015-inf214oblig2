High Society (2014 film)
 
{{Infobox film
| name           = High Society
| image          = 
| caption        =
| director       = Julie Lopes-Curval
| producer       = Fabienne Vornier Francis Boespflug Stéphane Parthenay
| writer         = Sophie Hiet Julie Lopes-Curval 
| starring       = Ana Girardot Bastien Bouillon Baptiste Lecaplain
| music          = Sébastien Schuller 
| cinematography = Céline Bozon
| editing        = Muriel Breton
| studio         = Pyramide Productions
| distributor    = Pyramide Distribution
| released       =  
| runtime        = 95 minutes
| country        = France
| language       = French
| budget         = 
| gross          = 
}}

High Society ( ) is a 2014 French romantic drama film written and directed by Julie Lopes-Curval. The film stars Ana Girardot, Bastien Bouillon and Baptiste Lecaplain.  It was selected to be screened in the Contemporary World Cinema section at the 2014 Toronto International Film Festival.    In January 2015, the film received two nominations at the 20th Lumières Awards. 

== Cast ==
*Ana Girardot as Alice 
*Bastien Bouillon as Antoine 
*Baptiste Lecaplain as Kevin
*Aurélia Petit as Agnès  Sergi López as Harold 
*India Hair as Manon 
*Stéphane Bissot as Christiane  
*Jean-Noël Brouté as Monsieur Jacquard  
*Michèle Gleizer as Arlette
*David Houri as Rodolphe
*Blanche Cluzet as Catherine
*Cécile Bernot as Déborah
*Lawrence Valin as Martin

== References ==
 

== External links ==
*  

 
 
 
 
 
 
 

 