Irish Destiny
{{Infobox film
| name = Irish Destiny
| image =
| alt =
| caption =
| director = George Dewhurst
| producer =
| writer = Isaac Eppel
| screenplay =
| story =
| based on =
| starring =
| cinematography =
| editing =
| studio =
| distributor =
| released =  
| runtime = Ireland
| language =
| budget =
| gross =
}}

Irish Destiny is a 1926 film made in Ireland, directed by George Dewhurst and written by Isaac Eppel to mark the tenth anniversary of the Easter Rising.

The film was considered lost for many years until in 1991 a single surviving nitrate print was located by the Irish Film Institute in the United States Library of Congress. The institutes archive had the film transferred to safety stock and restored. The institute then commissioned Mícheál Ó Súilleabháin to write a new score for the film.

Irish Destiny is the first fiction film that deals with the Irish War of Independence, and the first and only film written and produced by Isaac (Jack) Eppel, a Dublin GP and pharmacist who also enjoyed a career as theater impresario and cinema owner.

==External links==
* 
* 

 

 
 
   
 
 
 
 


 
 