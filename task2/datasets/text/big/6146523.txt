Santosham (2002 film)
{{Infobox film
| name           = Santosham
| image          = Santosham.jpg
| caption        = DVD cover
| writer         = Dasharath  
| screenplay     = Gopimohan
| producer       = Dr.K.L.Narayana Dasharath
| Nagarjuna Akkineni Prabhu Deva Shriya Saran Gracy Singh
| music          = R. P. Patnaik
| cinematography = S. Gopal Reddy
| editing        = K. V. Krishna Reddy
| studio         = Sri Durga Arts
| distributor    = Annapurna Studios Supreme Audio EVP International  
| released       =  
| runtime        = 150 minutes
| country        = India
| language       = Telugu
| budget         =
}}
 Nagarjuna Akkineni Tamil Santhosha Vanille and was also dubbed in Hindi as Pehli Naazar ka Pehla Pyaar. The film was remade in Bengali as Bondhon starring Jeet and Koyel and in Kannada as Ugadi (film)|Ugadi with V. Ravichandran. The film recorded as Super Hit at box-office.

==Plot==

Karthik (Akkineni Nagarjuna|Nagarjuna) is a rich architect in Ooty. He falls in love with Padhu (Gracy Singh). Padmavathi takes her own sweet time to accept Karthiks love. Padmavathi has a cute cousin sister Bhanu (Shriya). Bhanu encourages their love and gives courage to Padhu. When Padhu reveals about her love, her father Rama Chandraiah (K. Viswanath) resists and asks her to marry the guy he has chosen for her. Padhu flees from home and marries Karthik. Padmavathi is very eager to get back to her parents and get their blessings. Karthik and Padmavathi migrate to New Zealand and they give birth to a kid named Lucky. Padhu dies in an accident. Rama Chandraiah has a very big joint family. A few of the family members opine that inviting Karthik and his kid for a marriage to be happened at their place would give an opportunity to get a closer look at Lucky. When Karthik visits Rama Chandraiahs place he gets mixed responses from different people in the house. Rama Chandraiah doesnt like Karthik and others like him. Over a period of time, Karthik impresses them all and when he was about to go back to New Zealand, Rama Chandraiah expresses his repentance for whatever happened and says that he is accepting Karthik as his son-in-law. Bhanu has fallen in love with Karthik. She meets Karthik, a widower now. She still has feelings for him. But at the same time, there is Pawan (Prabhu Deva) - a childhood friend of Bhanu - who is deeply in love with her. Karthik returns to New Zealand. Bhanu along with Pawan visits Karthik. During that period Bhanu gets closer to Karthik. When Karthik realizes the intentions of Bhanu, he fixes Pawan as the fiancée for her. Now Bhanu is about to get married to Pawan. Bhanu loves Karthik, but Karthik hesitates to repeat the history by marrying a girl against the wishes of her family. Just as Karthik is about to leave for good with his son, Rama Chandraiah stops him from leaving and pleads him to marry Bhanu. Also, Pawan comes and pleads Karthik to marry Bhanu as he can tell that Bhanu is not happy about the marriage. The film has a happy ending with Karthik and Bhanu marrying.

==Cast==
  was selected as lead heroine marking her first collaboration with Nagarjuna.]]
{{columns-list|3| Nagarjuna Akkineni as Karthik
*Shriya Saran as Bhanu
*Gracy Singh as Padmavathi / Paddu
*Prabhu Deva as Pavan
*K. Viswanath as Rama Chandraiah 
*Kota Srinivasa Rao as Himsaraju / Vikram 
*Brahmanandam 
*Sunil as Kranthi Kumar / Seetaiah Chandra Mohan as Chandram Paruchuri Venkateswara Rao as Venkat Rao 
*Tanikella Bharani  Pruthvi Raj as Sriram 
*L. B. Sriram Melkote 
*Ahuti Prasad as Prasad
*Benerjee as Suri
*Giridhar 
*Aajaam Sumitra as Lakshmi
*Sudha as Savitri 
*Sophiya Haque as in item song
*Preethi Nigam as Rani
*Madhu Mani as Malika
*Indu Anand 
*Anitha Chowdary as Anitha
*Lahari as Durga
*Swapna 
*Devisri 
*Saraswatamma 
*Master Akshay as Lucky
}}

==Soundtrack==
{{Infobox album
| Name        = Santosham
| Tagline     = 
| Type        = film
| Artist      = R. P. Patnaik
| Cover       = 
| Released    = 2002
| Recorded    = 
| Genre       = Soundtrack
| Length      = 29:59
| Label       = Supreme Music
| Producer    = R. P. Patnaik
| Reviews     =
| Last album  = Nuvvu Leka Nenu Lenu   (2002)
| This album  = Santosham   (2002)
| Next album  = Jayam   (2003)
}}

The music was composed by R. P. Patnaik. All songs are blockbusters. Music released on SUPREME Music Company.
{{Track listing
| collapsed =
| headline =
| extra_column = Singer(s)
| total_length = 29:59
| all_writing =
| all_lyrics =
| all_music =
| writing_credits =
| lyrics_credits = yes
| music_credits =

| title1  = Nuvvante Nakishtamani Sirivennela Sitarama Sastry Usha
| length1 = 5:01

| title2  = Dhimdhinaktari Naktithom
| lyrics2 = Kulasekhar
| extra2  = Shankar Mahadevan
| length2 = 4:22

| title3  = So Much To Say
| lyrics3 = Chandra Siddhartha
| extra3  = Rajesh
| length3 = 1:57

| title4  = Devude Digivachina
| lyrics4 = Kulasekhar Usha
| length4 = 4:00

| title5  = Ne Tolisariga  Sirivennela Sitarama Sastry Usha
| length5 = 4:51

| title6  = Mehbooba Mehbooa 
| lyrics6 = Vishwa 
| extra6  = Mano (singer)|Mano,Bhargavi,Vishwa
| length6 = 4:07

| title7  = Emaindho Emo Naalo 
| lyrics7 = Kulasekhar
| extra7  = R. P. Patnaik
| length7 = 1:15

| title8  = Diri Diri Diridee
| lyrics8 = Kulasekhar  Usha
| length8 = 4:21 
}}

==Awards== Best Actor Best Film categories. Filmfare Best Film award in Telugu, for 2002 Filmfare Best Music Director Award

== Crew ==
* Gopimohan ... Screenplay
* Peter Hein ... Stunts

==External links==
*  

 
 
{{succession box
| title=Filmfare Best Film Award (Telugu)
| years=2002
| before=Nuvvu Nenu
| after=Okkadu}}
 

 
 

 
 
 
 
 
 