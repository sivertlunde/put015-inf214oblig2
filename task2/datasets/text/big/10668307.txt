Necromancy (film)
 
{{Infobox film
| name           = Necromancy
| image          = Necromancy.jpg
| image_size     =
| caption        =
| director       = Bert I. Gordon
| producer       = Sidney L. Caplan Gail March Jeffrey M. Sneller
| writer         = Bert I. Gordon Gail March
| narrator       =
| starring       = Orson Welles Pamela Franklin Lee Purcell
| music          = Fred Karger Robert J. Walsh
| cinematography = Winton C. Hoch
| editing        = John B. Woelz
| distributor    = Cinerama Releasing Corporation
| released       =  
| runtime        = 83 min.
| country        = United States English
| budget         =
| gross          =
}}
Necromancy (AKA A Life for a Life, The Toy Factory and The Witching) is a 1972 horror film directed by Bert I. Gordon and starring Orson Welles and Pamela Franklin.

==Plot==
A strange and sinister man, Mr. Cato (Orson Welles), wields extraordinary power in the small town of Lilith. The townsfolk indulge in weird rituals in their pursuit of necromancy, bring the dead back to life. Against this disturbing background a beautiful young girl, Lori (Pamela Franklin), becomes the human catalyst. She is married to one of Catos workers and holds the key between life and death: what Cato and his followers have in mind is using Lori to bring back Catos dead son.

==Cast==
*Orson Welles as Mr. Cato
*Pamela Franklin	as Lori Brandon
*Lee Purcell as Priscilla
*Michael Ontkean as Frank Brandon
*Harvey Jason as Dr. Jay
*Lisa James as Georgette
*Susan Bernard as Nancy (billed as Sue Bernard)

==Release==
The film was released theatrically in the United States by Cinerama Releasing Corporation in 1972.

The film was released on VHS in the U.S. as The Witching by both Paragon Video Productions and Magnum Entertainment in the 1983.

The film was released on DVD in the United Kingdom by Pegasus Entertainment in 2002.  It has still not been officially released on DVD in the U.S.

== External links ==
*  
*  

 

 
 
 
 
 
 


 