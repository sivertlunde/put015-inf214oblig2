Noise (2007 American film)
{{Infobox Film
| name           = Noise
| image          = 
| image_size     = 
| caption        = 
| director       = Henry Bean Seven Arts Pictures Fuller Films
| writer         = Henry Bean
| narrator       = 
| starring       = Tim Robbins Bridget Moynahan  and William Hurt
| music          = Phillip Johnston
| cinematography = Andrij Parekh
| editing        = Julie Carr
| distributor    = THINKFilm
| released       = October 22, 2007
| runtime        = 88 min.
| country        =  English Russian Russian
| budget         = 
| gross          =  $16,934 
| preceded_by    = 
| followed_by    = 
}}
 comedy drama film written and directed by Henry Bean. It stars Tim Robbins and Bridget Moynahan. Robbins plays a successful lawyer in Manhattan named David Owen who is bothered by all the noise in the city, and who resorts to vandalism to put a stop to it, adopting the identity of "The Rectifier". His acts of vandalism provoke the mayor of the city, played by William Hurt.

The film premiered October 22, 2007 at the Rome Film Festival. It was later shown at the AFI Film Festival on November 6, 2007. It opened in limited release in the United States on May 9, 2008.   

==Plot==
David Owen, a New York lawyer, is constantly plagued by noise, particularly car alarms, but also burglar alarms and backup beepers. Despite putting up with the noise for years, including the formative years of his daughter, he finally breaks into a car to shut off its alarm, and is arrested by police. Owen and his family move to the countryside, but it only replaces one set of noise problems with another. Despite attempting to legally deal with the constant barrage of noise, he makes no progress, and continues to resort to vandalism, resulting in higher and higher court penalties and straining his relationship with his wife, who finally asks him to leave.

While living on his own, Owen continues to destroy alarms, and progressively damages cars based on the number of violations. During this, he meets and begins a relationship with Ekaterina, a Russian student. After confronting him with the fact that he is "The Rectifier", a vigilante personality, complete with monikered stickers, that Owen has adopted. After falling in with him, Ekaterina convinces Owen to start a ballot initiative. While initially slow, the measure garners explosive popularity. Despite this, it is shut down by Mayor Schneer, who disdains the Rectifier and believes supporting the measure would encourage vigilantism.

After being shut down through a loophole that prevents the ballot measure from appearing, Owen outfits a car with numerous alarms and much louder horns. He then parks in front of city hall and sets off his alarms, citing the various laws that so frustrated Owen initially when city hall security demands he turn them off. After enraging a nearby tenant, Judge Kornreich, his vehicle is attacked and the judge is mistakenly arrested as The Rectifier. Kornreich sues Owen for pain and suffering damages, and Owen goes to court. During his cross-examination of Kornreich, Owen intentionally alludes that his actions could be considered assault and Battery (crime)|battery, establishing a legal precedent. Owen is found guilty, though Kornreich obviously intends to be lenient on him. Having won a legal victory, Owen reunites with his wife. Walking out of the courthouse, an activist approaches Owen about blowing up a billboard TV that has been causing accidents and is an eyesore of the cityscape. A dual ending is provided, one in which Owen flees with the activist, and one where he politely declines to accompany her and gets into a cab with his wife.

==Cast==
*Tim Robbins as David Owen
*Bridget Moynahan as Helen Owen
*William Hurt as Mayor Schneer
*Margarita Levieva as Ekaterina Filippovna
*María Ballesteros as Gruska
*William Baldwin as Mayors Chief of Staff
*Lou Carbonneau as Dante Moretti Chuck Cooper as Judge Gibson
*Stephen Adly Guirgis as Anthony J. Corpitani
*Helen Hanft as Forceful Juror
*Peter Hoffman as Judge Kornreich
*Aaron Lohr as Rowdy Drinker
*David Margulies as Heart Attack Man
*Keir ODonnell as Experienced Car Thief

==Production== The Believer." 

==Critical reception==
The film received mixed reviews from critics. The New York Times film critic Stephen Holden called Noise "a one-trick film that rapidly wears out its welcome." Holden said the film "is shallow and loud" and "lurches unsteadily between drama and comedy." Holden wrote "Its performances are discordant" and said "you have to wonder to what degree Mr. Robbins, an actor famous for his political activism, is here simply because he shares Mr. Beans outrage."   
 David Denby New York." Denby said "Surely a more glamorous actor would have given Noise a lighter, more satirical spin, but Robbins makes David a lost, unhappy man who needs to save himself, and thats the right way to go&mdash;it turns a one-note story into a universal fable." Denby praised the performance of Margarita Levieva as Ekatrina. Denby wrote "What saves Noise&mdash;and makes it almost continuously entertaining&mdash;is Beans sense of the city as a gathering place of clever, chattering people."   

==Box office performance==
The film opened in limited release in the United States on May 9, 2008 and grossed $3,697 in 2 theaters its opening weekend. 

==References==
 

==External links==
* 
* 
* 
* 
* 

 
 
 
 
 