Les Amants du Pont-Neuf
 
{{Infobox film
| name           = Les Amants du Pont-Neuf
| image          = amantsdupontneuf.jpg Gaumont 1991
| director       = Leos Carax
| producer       = Christian Fechner
| writer         = Leos Carax
| starring       = Juliette Binoche Denis Lavant
| music          = Les Rita Mitsouko David Bowie Arvo Pärt
| cinematography = Jean-Yves Escoffier
| editing        = Nelly Quettier Gaumont (France)
| released       =  
| runtime        = 125 minutes
| country        = France
| language       = French
| budget         =
}}
Les Amants du Pont-Neuf ( ) is a 1991 French film directed by Leos Carax, starring Juliette Binoche and Denis Lavant. The title refers to the Pont Neuf bridge in Paris. The DVD of the film is released under its French title in the UK, as The Lovers on the Bridge in North America,  and, in a mistranslation of the original title, as Lovers on the Ninth Bridge (instead of "Lovers on the New Bridge") in Australia.

== Plot == vagrants Alex relationship and a disease which is slowly destroying her sight. The film portrays their harsh existence living on the bridge with Hans (Klaus Michael Grüber), an older vagrant. As her vision deteriorates Michèle becomes increasingly dependent on Alex. When a possible treatment becomes available, Michèles family use street posters and radio appeals to trace her. Fearing that she will leave him if she receives the treatment, Alex tries to keep Michèle from becoming aware of her familys attempts to find her. The streets, skies and waterways of Paris are used as a backdrop for the story in a series of set-pieces set during the French Bicentennial celebrations in 1989.

==Production== Boy Meets Mauvais Sang had been considerably larger and more costly, albeit more successful at the boxoffice.
 department of Hérault in southern France. Construction began.

At this time the mayor of Paris gave the authorisation for filming in Paris on the Pont Neuf - between 28 July and 18 August. While tying his shoe on set, lead actor Denis Lavant injured the tendon in his thumb so badly that filming could not be completed in the given time. The insurers were called - Carax was pressured to recast the male lead. However the director insisted that he could not recast or change his approach to the film to allow for Lavants injury. Immediately the solution was clear - the Lansargues model was to be extended for daytime use. At this point only a few minutes of footage had been shot.

The producers secured a further 9 million to develop the set. This extra budget seemed too low given the extent of work required at the Lansargues site. It soon became obvious that more money would be required before shooting could resume. Without this funding the producers were obliged to accept an insurance payment that would settle outstanding debts but not allow any further work. Production was shut down. A frustrated cast and crew took time off to relax and it was not until the Cannes festival in 1989 that Dominique Vignier, together with the Swiss millionaire Francis von Buren, agreed to undertake the funding based on the few short rushes filmed a year before.

In the documentary Von Buren clearly states that he was deceived about the upcoming costs he would be forced to take on. The number of 30 million francs, given for the total cost of construction and to finish the film, was not nearly enough; the final number from this stage would be closer to 70 million francs. He withdrew his funding late in 1989 with an estimated loss of 10 million francs. Yet again, production stopped. From October 1989 to June 1990 the only person at the site in Lansargues was the guard - and a number of storms followed over that winter causing massive water damage to the uncompleted set.

==Release==
At the Cannes festival in 1990 Christian Fechner allocated 70 million francs in order to finish the film. Unable to find financing partners, he provided his own money, purchasing both rights and debts of the picture. The picture was finished on 22 December 1990 and premiered out of competition at the 1991 Cannes Film Festival before arriving in French theatres on 17 October 1991. The film had 867,197 Admissions in France where it was the 34th highest earning film of 1991.    Caraxs most successful film to date.

==References==
 

==External links==
*  
*  

 

 

 
 
 
 
 
 
 