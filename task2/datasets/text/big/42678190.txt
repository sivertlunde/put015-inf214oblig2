App (film)
{{Infobox film
| name           = APP
| image          = File:APP FINAL US Poster 250x352.jpg
| alt            = 
| caption        = 
| film name      = 
| director       = Bobby Boermans
| producer       = 
| writer         = Robert Arthur Jansen
| screenplay     = 
| story          = 
| based on       =  
| narrator       = 
| starring       = Hannah Hoekstra, Isis Cabolet, Robert de Hoog
| music          = Herman Witkam
| cinematography = Ezra Reverda
| editing        = Brian Ent
| studio         = 2CFilm
| distributor    = 
| released       =  
| runtime        = 75 minutes
| country        = Netherlands
| language       = Dutch
| budget         = 
| gross          = 
}} VOD and digital release.

==Synopsis==
After Anna (Hannah Hoekstra) blacks out after binge drinking at a party, she wakes to find herself with a hangover and a mysterious app called IRIS downloaded onto her phone. At first the app seems harmless, but soon IRIS begins tampering with Annas life in increasingly destructive ways and there seems to be nothing she can do to remove the app from her phone. As Anna tries desperately to rid herself of IRIS and discover why shes being targeted, she finds that shes in the middle of something much larger than she initially expected.

==Cast==
*Hannah Hoekstra as Anna Rijnders
*Isis Cabolet as Sophie Welts
*Robert de Hoog as Tim Maas
*Alex Hendrickx as Stijn Rijnders
*Matthijs van de Sande Bakhuyzen as Daan Thijsse
*Patrick Martens as Henry
*Harry van Rijthoven as Dr. Carlo
*Liza Sips as Liesbeth
*Jeroen Spitzenberger as Jerry
*Mark van Eeuwen as Sim

==Production and second screen usage==
Filming for APP began in December 2012 and was shot with the intent to utilize second screen technology to show a parallel storyline via viewers mobile devices while they were viewing the film.    After the film was released to theaters, viewers with Android or iPhone devices could text “IRIS” to 97-000, upon which they would be sent the app.   Viewers could also download the app from the Google and Apple stores. While viewing the film the app would expand on the movies story line, giving more information about the events and characters as well as showing elements such as different camera angles for specific scenes and newspaper articles.   FirstShowing.net questioned the need for the app, as 2CFilm stated that it was not necessary to understand the movie and FirstShowing.net wondered if it was more of a gimmick than something that would truly add to the viewing experience. 

==Reception==
Critical reception for APP has been mostly positive.  Bloody Disgusting and Dread Central both praised the film overall and cited that the use of the second screen to enhance the film was an interesting touch.   IndieWire gave a mostly favorable review, stating that they enjoyed the app but that the movie itself "fizzles in its closing moments, in which a rushed explanation and clichéd rooftop showdown bring the conflict to a tidy finale".  In contrast, Shock Till You Drop gave the movie a score of 5 out of 10 and commented that "While it was a decent attempt, App ends up being another mediocre techno-thriller that bogs itself down with an outlandish premise and an extreme amount of nonsense dialogue." 

==References==
 

==External links==
*  
*  
*  
*  

 
 
 