White Bird in a Blizzard
 
{{Infobox film
| name           = White Bird in a Blizzard 
| image          = WBIB poster.jpg
| alt            = 
| caption        = Film poster
| director       = Gregg Araki
| producer       = {{plainlist|
* Gregg Araki
* Pascal Caucheteux
* Sebastien Lemercier
}}
| screenplay     = Gregg Araki
| based on       =  
| starring       = {{plainlist|
* Shailene Woodley
* Eva Green
* Christopher Meloni
* Shiloh Fernandez
* Gabourey Sidibe
* Thomas Jane
* Angela Bassett
|}}
| music          = {{plainlist|
* Harold Budd
* Robin Guthrie
}}
| cinematography = Sandra Valde-Hansen 
| editing        = Gregg Araki
| studio         = {{plainlist|
* Why Not Productions
* Desperate Pictures Wild Bunch
* Orange Studio
}}
| distributor    = {{plainlist|
* Bac Films
* Magnolia Pictures
}}
| released       =  
| runtime        = 91 minutes  
| country        = France United States
| language       = English
| budget         =
| gross          = $378,300  
}} thriller film premiered at limited theatrical release on October 24, 2014. 
==Plot== flashbacks of Eves past life and the present day.

In the flashbacks, Eve was a wild girl who gradually changed into a domesticated housewife after her marriage to Brock (Christopher Meloni), an ordinary man who leads an uneventful life. While Kat explores her blossoming sexuality with her handsome but dim-witted neighbor and schoolmate, Phil (Shiloh Fernandez), Eve struggles to deal with aging and quenching her youthful wildness. She tries to be sexy when Brock is away, even luring Phils attention. After Eve disappeared, Kat deals with her abandonment without much issue, occasionally releasing her own wild side, seducing the detective (Thomas Jane) investigating her mothers disappearance. The film then jumps forward three years to the spring of 1991. On a break from college, Kat returns home and seems unfazed to learn that her father is in a relationship with a co-worker.

The older detective that Kat has been having an affair with informs her that Brock might have killed Eve after catching Eve cheating. Kat dismisses this theory, just like she did three years ago, but after mentioning the topic to her friends Beth (Gabourey Sidibe) and Mickey (Mark Indelicato) they tell her that they suggested this same theory to her and she dismissed them as well. Kat suspects Phil of sleeping with Eve and confronts him the night before she is to return to college, but Phil angrily rebuffs it and tells her that Brock knows where Eve is. 

Kat begins to unpack Brocks suspiciously locked freezer in their basement, but is stopped when he walks in on her. She questions Brock about her mothers disappearance, asking if he does in fact know where Eve is, but he denies having any knowledge of her whereabouts. Believing her father, Kat bids her father goodbye and tearfully boards her flight, returning to college. It is revealed that this was the last time Kat saw Brock, as he went out to a bar shortly thereafter and drunkenly admitted to murdering Eve. He is soon arrested and later hangs himself with a sheet in his jail cell as well as showing that Brock moved Eves body from the freezer the night before Kat unpacked it.

The film ends with a flashback of Eves death; she came home from shopping the afternoon of her disappearance to find Brock and Phil in bed together. Phil dashed out of the room and Eve began laughing hysterically at Brock, incredulous, and he responded by wrapping his fingers around her throat, asking her repeatedly to stop, to which she kept on laughing, and he strangled her to death.

==Cast==
* Shailene Woodley as Katrina "Kat" Connors
** Ava Acres as 8-year-old Kat
* Eva Green as Eve Connors
* Christopher Meloni as Brock Connors
* Shiloh Fernandez as Phil
* Gabourey Sidibe as Beth
* Thomas Jane as Detective Scieziesciez
* Angela Bassett as Dr. Thaler
* Dale Dickey as Mrs. Hillman
* Mark Indelicato as Mickey
* Sheryl Lee as May
* Jacob Artist as Oliver

==Reception==
  and Shiloh Fernandez in Paris at the films French premiere.]]

===Box office===
The film opened in the United States in a limited release on October 24, 2014 in 4 theaters and grossed $6,302 with an average of  $1,576 per theater and ranking #80 at the box office. After 7 weeks in theaters the film earned $33,821 domestically and $344,479 internationally for a total of $378,300.  

===Critical response===
On    based on 27 critics indicating "mixed or average reviews". 

Pop Insomniacs said, "Weve seen versions of this story several times, but never quite mangled together like this before, which is precisely why I was so captivated, uncomfortable and surprised by this movie".  Kansas City Star reporter Jocelyn Noveck said, "It all comes down to a doozy of a plot twist, and its enjoyably shocking. But at the end youre still left shaking your head, feeling lost, wishing there was something tangible to hold on to — perhaps a bit like being trapped in a snow globe... Two stars out of four". 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 