Cloaca (film)
{{Infobox film
| name = Cloaca
| image = Film poster Cloaca.jpg
| caption = Film poster
| director = Willem van de Sande Bakhuyzen
| producer =
| writer = Maria Goos
| narrator =
| starring = Peter Blok Pierre Bokma Gijs Scholten van Aschat Jaap Spijkers Caro Lenssen Elsie de Brauw Marleen Stolz
| music =
| cinematography =
| editing =
| distributor =
| released =  
| runtime =
| country = Netherlands Dutch
| budget =
}}
Cloaca is a 2003 Dutch film, directed by   (Tom), Pierre Bokma (Pieter), Gijs Scholten van Aschat (Joep), and Jaap Spijkers (Maarten). Female roles were played by Caro Lenssen (Laura), Elsie de Brauw (Conny), and Marleen Stolz (Russian prostitute). Eric Schneider acted in a supporting role as Brest, Pieters bosss boss.

Blok, Bokma, Goos, Scholten van Aschat, and Van de Sande Bakhuyzen know each other from the Toneelacademie Maastricht.

The film was conceived as a made-for-TV film (it was co-financed by the AVRO) but later got a cinema release. It won the public prize for Dutch film at a festival in 2003, and was released on DVD in 2005.

==Plot==
The career of homosexual art historian Pieter, who has been working as an archivist at a municipality, has been going nowhere for 20 years: he is ignored by his colleagues, and for many years now, as a sort of revenge, he has been taking a painting from the depot in the cellar on his birthday. Brest, his bosss boss, takes Vermeulen out for a meal. Pieter expects good news, but instead hears he must return the paintings: in any case the eight paintings by Van Goppel (who died recently) which all of a sudden have become worth a lot of money. This produces a major problem, because Pieter has sold four of the Van Goppels in order to pay for his apartment.

If anyone can help him, it would be his old student friends Joep and Tom. Joep, a politician who is slated to become the next Minister for Foreign Affairs, has been turned out of his house by his wife Conny because he proved to have an extramarital affair with Jennie. He thought he would be able to stay at Toms, but Tom went insane a couple of months earlier as a result of cocaine abuse. As a lawyer, Tom wants to take the case of Pieter himself. Joep cannot help with Pieters problem now, because hes on the verge of being appointed. The rancid Maarten comes also; his pretentious stage play "The Wheel of Ixion" is about to have its opening night, in which Joeps almost 18-year-old daughter is to play Laura, a role where she stands naked on the stage. Joep is with him there and does not know that Maarten had sex with Laura.

The friends celebrate Joeps birthday by hiring a prostitute for him, but that leads to tears from Joep and a brawl with Maarten who also wanted to use her services. The friends go to the première of Maartens play. Joeps mistrust about his daughter acting in the play disappears. He gets a chance afterwards of a reconciliation with his wife, but bungles it totally. Tom goes crazy and goes into the city to obtain cocaine. Meanwhile Pieter tries to convince his colleagues to attest that the Van Goppels had been given to him. When the friends return to Pieter, they look back on their past.

Just when everything seems about to end well for Pieter, Joep withdraws from the matter because he has become Minister for Culture. Tom breaks down now entirely and runs away. Maarten, who still has not confessed to Joep what he did with his daughter, continues to wait until Pieter comes home and gives him the bad news. Pieter cuts one of his arteries open in the bathroom.

==External links==
*  

 
 
 
 