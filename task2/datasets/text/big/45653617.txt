Sazaa (2011 film)
 
{{Infobox film
| name           = Sazaa
| image          = SazaaTheMoviePoster.jpg
| caption        = Official film poster
| director       = Hussain Munavvaru 
| producer       = Mahumaa Hussain Rasheed (Farivaa Films)
| writer         = Mahdi Ahmed
| screenplay     = Mariyam Moosa
| starring       = Niuma Mohamed Lufshaan Shakeeb Ismail Rasheed 
| music          = Mohamed Ikram
| cinematography = 
| editing        = Ali Musthafa Mohamed Niyaz
| studio         = Farivaa Films KID Production
| distributor    = 
| released       =  
| runtime        = 140 minutes
| country        = Maldives
| language       = Dhivehi MVR 450,000 
| gross          = 
}}

Sazaa ( ,   film, starring Niuma Mohamed and Lufshaan Shakeeb in the lead roles while Ismail Rasheed in the supporting role. 

==Plot==
Reema (Niuma Mohamed), a happy-go-lucky girl while in her island for holidays meets Zaid (Lufshaan Shakeeb), a land surveyor from Male. For them, its hate at first sight which as predicted turns in to love. 

But Reemas life is thrown in to unpredictability and turmoil when connection to Zaid is mysteriously lost after he leaves to Male’ promising to return within few days. Days turn to weeks and weeks to months. While Reema waits desperately, she sees her life changing unexpectedly as if fate has other plans for her. 

==Cast==
* Niuma Mohamed as Reema
* Lufshaan Shakeeb as Zaid
* Ismail Rasheed as Ahammaa

==Soundtrack==
The soundtrack for the film includes six songs (including 1 promotional song) with no copy musics included.

{{Infobox album |  
 Name = Sazaa|
 Type = Album |
 Artist = Ibrahim Zaid Ali |
 Cover = |
 Released = |
 Recorded = | Feature film soundtrack |
 Length = 37:10 |
 Label = |
 Producer = Farivaa Films KID Production |
 Reviews = |
 Last album =  |
 This album =  |
 Next album = |
}}
{{Track listing
| headline        = Track listing
| extra_column    = Singer(s)
| total_length    =  
| lyrics_credits  = no
| music_credits   = no
| title1          = Sazaa
| extra1          = Raafiyath Rameeza
| music1          = Ibrahim Zaid Ali
| lyrics1         = Mohamed Abdul Ghanee
| length1         = 4:11
| title2          = Lafzu Thakey
| extra2          = Mumtaaz Moosa, Shaufa
| music2          = Ibrahim Zaid Ali
| lyrics2         = Mohamed Abdul Ghanee
| length2         = 4:33
| title3          = Hindhukolheh
| extra3          = Ibrahim Zaid Ali, Mariyam Ashfa
| music3          = Ibrahim Zaid Ali
| lyrics3         = Mohamed Abdul Ghanee
| length3         = 3:56
| title4          = Furathama Nazaru
| extra4          = Ibrahim Zaid Ali
| music4          = Ibrahim Zaid Ali
| lyrics4         = Mohamed Abdul Ghanee
| length4         = 3:51
| title5          = Keehvehey
| extra5          = Mohamed Abdul Ghanee
| music5          = Ibrahim Zaid Ali
| lyrics5         = Mohamed Abdul Ghanee
| length5         = 4:36
| title6          = Fisve Kakoo Jehi (Promotional Song)
| extra6          = Ahmed Yafiu, Raafiyath Rameeza
| music6          = n/a
| lyrics6         = n/a
| length6         = 4:13
}}

==Awards & Nominations==

===Maldives Film Awards===
Won {{cite web
| title      = MFA list 2012
| url        = http://www.haveeru.com.mv/dhivehi/entertainment/123762
| publisher  =  
| language   = Divehi| accessdate = 12 March 2015}} 
* 2012 Maldives Film Award for Best Original Screenplay - Mahdi Ahmed

==References==
 

==External links==
*  
*  

 
 
 

 