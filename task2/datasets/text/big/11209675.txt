Wrath of Gods
{{Infobox film
| name           = Wrath Of Gods
| image          = WrathOfGodsCover.jpg
| caption        = 
| producer       = Jon Gustafsson Karolina Lewicka
| writer         = Jon Gustafsson Karolina Lewicka
| starring       = Gerard Butler Stellan Skarsgård Sarah Polley Ingvar E. Sigurdsson Sturla Gunnarsson Stellan Skarsgård
| music          = Hilmar Örn Hilmarsson
| cinematography = Jon Gustafsson
| editing        = 
| distributor    = Artio Distribution
| released       =  
| runtime        = 72 minutes
| country        = Canada Iceland
| language       = English
| budget         = 
| gross          = 
}} Martin Delaney co-produced the film. The music was composed by Hilmar Örn Hilmarsson.

==Cast==
* Gerard Butler
* Sturla Gunnarsson
* Wendy Ord
* Paul Stephens
* Eric Jordan Martin Delaney
* Michael Cowan
* Stellan Skarsgård
* Sarah Polley
* Ingvar Sigurdsson
* Tony Curran
* Jon Gustafsson

==Awards==
* Audience Award - Best Documentary Feature - Oxford International Film Festival 2007
* Jury Award - Achievement in Filmmaking - Stony Brook Film Festival 2007
* Best Documentary Feature shot in digital - Napa Sonoma WineCountry Film Festival 2007
* Bronze Remi Award - film & video production - Entertainment WorldFest 2007
* Premio Especial do Juri - MOFF - Santarém, Portugal 2007
* Grand Jury Prize - Red Rock Film Festival, Utah 2007

==Festivals==
* Oxford International Film Festival
* RiverRun International Film Festival
* Reykjavik International Film Festival
* Waterfront Film Festival
* Stony Brook Film Festival
* Napa Sonoma WineCountry Film Festival
* Revelation Perth International Film Festival
* NSI Film Exchange Canadian Film Festival
* MOFF - Santarém, Portugal
* Docudays, Beirut, Lebanon
* Fort Lauderdale International Film Festival
* Red Rock Film Festival

==External links==
*  
*  

 

 
 
 
 
 
 
 