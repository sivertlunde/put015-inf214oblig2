Horsemen (film)
{{Infobox film
| name           = Horsemen
| image          = Horsemenposter.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Jonas Åkerlund Brad Fuller Andrew Form
| writer         = David Callaham
| starring       = Dennis Quaid Zhang Ziyi Lou Taylor Pucci Clifton Collins Jr. Patrick Fugit Peter Stormare 
| music          = Jan A.P. Kaczmarek
| cinematography = Eric Broms
| editing        = Jim May Todd E. Miller Radar Pictures
| distributor    = Lionsgate Films
| released       =  
| runtime        = 90 minutes
| country        = United States
| language       = English 
| budget         = 
| gross          = $2,405,815 
}}
Horsemen is a 2009 American  , Winnipeg and Chicago, and was released on March 6, 2009.

==Plot==
An older man and his dog are out hunting, when they discover a serving tray on a stand in the middle of a frozen lake. He notices the words "Come and See" painted on several trees as he lifts the lid.

Detective Aidan Breslin (Dennis Quaid) is a widower who has become emotionally distanced from his two sons, Alex (Lou Taylor Pucci) and Sean (Liam James), following the death of his wife three years prior to the films events. Due to his former dental forensics expertise, Aidan receives a call regarding the contents of the tray, where it is revealed that it contained human teeth. After analyzing the teeth, Breslin is able to match them to a man who had been reported missing. Aidan is once again called to investigate the bizarre murder of a woman named Mrs. Spitz, who was discovered hanging in her bedroom, eerily displayed and strung up on a series of hooks connected to a rig; the words "Come and See" displayed prominently on the walls. According to her autopsy report, she drowned in her own blood due to a precise stabbing. They also discover that Mrs. Spitz had been pregnant and the fetus was removed. Aidan speculates that there were four attackers and that they had used a camera to record the murder. After leaving the crime-scene, Breslin comforts one of the Spitzs three daughters, their adopted Asian daughter Kristen (Zhang Ziyi), reassuring her that he will do his best to solve the crime.

The next murder shares the same Modus operandi|M.O. with the Spitz murder; the similar hook contraption leads Breslin to a local tattoo parlor. There, he is informed that the owner constructed four devices in total. Yet another murder occurs, but this time no hook rig was involved and the message only appears on three of the rooms walls. While Aidan reviews the evidence at his home, Sean stumbles on one of the photographs. Seans insights point Breslin to the Bible, where he discovers that these killings are patterned after the Four Horsemen of the Apocalypse, with each room corresponding to a Horseman; the "Come and See" message is a quote from the Book of Revelation  , which pertains to the lifting of the veil and the coming of the Apocalypse.  When Kristen contacts Aidan unexpectedly, he goes to meet with her, and during their conversation Kristen produces the missing fetus, confessing to the murder. During her interrogation, Aidan discovers the darker side of Kristens personality, one similar to the Horseman War.

Later, Breslin goes over to Kristens house where he and his partner, Stingray (Clifton Collins, Jr.), discover the video recording from the murders as well as some polaroids of Kristen posing sexually with Mr. Spitz (Peter Stormare). Mr. Spitz unsuccessfully assaults Breslin. Breslin confronts Mr. Spitz with the photos of him and Kristen. Stingray and Breslin arrest Mr. Spitz and his children are placed in protective custody. When Breslin confronts Kristen in jail, she claims to have been sexually abused for years and the murder of Mrs. Spitz served as punishment for her abuse. Following the interview, Breslin has Mr. Spitz arrested. A microchip discovered in the unrigged victims stomach leads Breslin to an apartment associated with the four horsemen. The apartment is booby-trapped and is engulfed in flames by magnesium ribbons in the computers.
 gay young homophobic brother, Taylor (Eric Balfour). Cory puts what appears to be a sedative in Taylors drink and hands it to him after he has heard his response to the issue. As Taylor drunkenly stumbles out to his car, Cory is accosted by a thief in the parking lot. In response, Cory stabs the man, who survives the attack and later provides the police with Corys description. The precision of the stab wound, and that Cory whispered "and Hell follows me", leads Aidan to assume that it was done by a Horseman (Death). Elsewhere, Taylor awakens to find himself hooked onto a rig with his eyes fixed open. Cory appears wielding a bone saw, then proceeds to try to cut out his own heart, killing himself in the process.

Breslin, convinced that there may be another victim, asks Kristen if she represented the Horseman War and that the unrigged victim was Famine. She confirms this, but refuses to relinquish any information about their leader. Breslin comes to the conclusion that due to the nature of the first murder, he was meant to be assigned to the case all along, and becomes concerned that his family will be targeted next. Breslin asks Stingray to go on ahead and check his house. At the Breslin home, Stingray is attacked and knocked out. Aiden arrives later and enters Alexs room for the first time in three years. To his horror, everything in the room is white or painted white: the color of the Horseman Conquest, their leader. A clue points him to the Metropolitan Theatre, where Aidan first met his wife. When he arrives, he is rendered unconscious from an injection into his neck by an unidentifiable assailant; when he comes to, he finds himself handcuffed to a seat as Alex dangles over the stage on the final hook rig. Alex starts bleeding to death, as he gives Breslin a speech regarding the Horsemens emotional detachment from their families. As Alex succumbs to his injuries, Breslin rips his handcuffs off the seating and fires his gun to detach the rigging from the ceiling. Alex awakens as his father holds him.

At the Breslin home, Sean wakes up from a bad dream as Breslin comforts him. When he asks where Alex is, Breslin reassures him that Alex will be okay, his fate ultimately unknown.

==Cast==
* Dennis Quaid as Aidan Breslin, a widower detective trying to solve the "Horsemen" case   
* Zhang Ziyi as Kristen Spitz; War (Red Horse)
* Patrick Fugit as Corey Kurth; Death (Pale/Green Horse)
* Lou Taylor Pucci as Alex Breslin; Conquest (White Horse)
* Daryl Dorge as Garrison Jacobs; Famine/Pestilence (Black Horse)
* Clifton Collins, Jr.  as Stingray
* Peter Stormare as Mr. Spitz
* Eric Balfour as Taylor
* Chelcie Ross as Police Chief Krupa
* Liam James as Sean Breslin
* Deborah Odell as Ms. Bradshaw
* David Dastmalchian as Terrence
* Barry Shabaka Henley as Tuck
* Paul Dooley as Father Whiteleather
* Neal McDonough (deleted scenes)  as Police Chief Krupa

==Reception==
Horsemen was released to mixed reviews. Review aggregation website Rotten Tomatoes gives the film a rating of 40% based on 5 reviews. 

==References==
 

==External links==
*  
*  
*  
*  
*  

 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 