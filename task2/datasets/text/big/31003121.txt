Temptation (1946 film)
{{Infobox film
| name           = Temptation
| image          = Temptation Poster.jpg
| image size     = 
| alt            = 
| caption        = Theatrical release poster
| director       = Irving Pichel
| producer       = Edward Small
| screenplay     = Robert Thoeren
| based on       =    
| narrator       = 
| starring       = Merle Oberon George Brent
| music          = Daniele Amfitheatrof
| cinematography = Lucien Ballard
| editing        = Ernest J. Nims
| studio         = International Pictures
| distributor    = Universal Pictures
| released       =  
| runtime        = 98 minutes
| country        = United States
| language       = English
| budget         = $1.6 million 
}}
Temptation is a 1946 American drama film noir directed by Irving Pichel.  The drama features Merle Oberon and George Brent.  The film was based on Robert Smythe Hichenss novel Bella Donna. 
 Bella Donna, starring Pola Negri, which was directed by George Fitzmaurice  is also based on that novel. Other versions were filmed in 1918 and 1935.

==Plot==
Victorian melodrama, set in Egypt, where a down-on-her-luck courtesan (Oberon) snares a loving but naive Egyptologist (Brent), marries him and moves to Egypt.  She quickly becomes bored and embarks on an affair with Mamhoud Baroudi (Korvin).  She falls in love with him, and together, they plot to do away with her husband.

==Cast==
* Merle Oberon as Ruby
* George Brent as Nigel Armine
* Charles Korvin as Mahoud Baroudi
* Paul Lukas as Sir Meyer Isaacson
* Lenore Ulric as Marie
* Arnold Moss as Ahmed Effendi
* Robert Capa as Hamza
* Aubrey Mather as Dr. Harding
* Ludwig Stössel as Dr. Mueller
* André Charlot as Prof. Dupont
* Suzanne Cloutier as Yvonne Dupont
* Gloria Lloyd as Jean McCormick

==Production==
The movie was the first producer Edward Small had made away from United Artists for a number of years. MICHAEL TODD SET TO FILM GREAT SON: Ferber Novel Will Be His First Universal Picture--Hellinger Acquires Criss-Cross "Variety Girl" Planned
Special to THE NEW YORK TIMES.. New York Times (1923-Current file)   04 June 1946: 33.   He bought the rights to the novel in 1941 intending to make it as a vehicle for Ilona Massey who had just made International Lady for him.   He intended to make the film for United Artists but other projects took priority. Over a five year period there were several near-starts and postponements; he almost managed to begin production in February 1946 but there was not enough studio space. Smalls deal with United Artists expired so the film became the first shot at Universal by International under the new United World Pictures arrangement. THE HOLLYWOOD WIRE: In the Clear MORE HOLLYWOOD ITEMS By Meets Girl By FRED STANLEY. New York Times (1923-Current file)   05 May 1946: X1 

==Reception==
Film critic Bosley Crowther panned the film when it was released, writing "True enough, Miss Oberon looks lovely, Mr. Korvin behaves with bold sang-froid and George Brent is sufficiently unimpressive to seem a husband that a dame would double-cross. Paul Lucas, Arnold Moss and Lenore Ulric also act as though they thought they had a script. But the whole thing is as claptrap in its nature as it was when Pola Negri played it back in 1923." 

Variety (magazine)|Variety magazine panned the screenplay, writing "Production is well-stacked with solid values in every department except for the screenplay, which falls short in its attempt to stretch an unsubstantial story line over so long a running time." 

==References==
 

==External links==
*  
*  
*  

 
 

 

 
 
 
 
 
 
 
 
 