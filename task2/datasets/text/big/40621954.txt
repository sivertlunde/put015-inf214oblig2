Enslavement: The True Story of Fanny Kemble
{{Infobox television film name = Enslavement: The True Story of Fanny Kemble image =   image_size =  caption =  genre = Drama director = James Keach producer =  screenplay = Christopher Lofton based on =  starring = Jane Seymour Keith Carradine James Keach music = Charles Bernstein cinematography = Roland Smith  editing = Heidi Scharfe costume design = Cheri Ingle studio = Catfish Productions Hallmark Entertainment Distribution Company  budget =  country = United States language = English  network = Showtime
|released =   runtime = 115 mins 
}} Jane Seymour Confederacy during Colin Fox also appearing.
 Showtime on Charles Bernstein earned a Primetime Emmy Award nomination for "Outstanding Music Composition for a Miniseries, Movie, or a Special".

==Plot summary== treatment of Confederacy during the American Civil War.

==Cast==
  Jane Seymour as Fanny Kemble
* Keith Carradine as Pierce Butler
* James Keach as Dr. Huston
* Adewale Akinnuoye-Agbaje as Joe Colin Fox as John Quincy Adams
* Francois Klanfer as Daniel Webster
* Eugene Byrd as Jack
* Sharon Washington as Psyche
* Peter Mensah as Quaka
* Gerard Parkes as Charles Kemble
* Bernard Brown as Sam Swift
* Rick Demas as Brutus
* Arlene Duncan as Harriet
* Lili Francks as Aunt Jerusalem
* Janet-Laine Green as Elizabeth Sedgwick 
* Catherine Hayos as Julia
* Kayla Perlmutter as Young Sarah
* Brett Porter as Owen Parker
* Jackie Richardson as Daphnie
* Richard Yearwood as Habersham
 
 
 

==Production==
 

In 2000, it was reported that  s as well as the lack of familiarity Americans had with Kembles life.   

One of their goals was to depict several different types of enslavement.   Seymour described her character as "a strong, willful woman   fought against being enslaved by the man she fell in love with and married. When she saw slavery on his plantation, she fought against that, too."      Actor Keith Carradine, one of Keachs good friends, was cast as Fannys husband.  Keach also directed, yet another such collaboration between him and his wife.  The film was shot mostly in Canada. 
 

==Reception== New York Daily News, David Bianculli blamed the screenwriter for the films faults, explaining that "Lofton writes dialogue that sounds like unvarnished tracts from an authors message manifesto – and in Seymours Kemble, he has created a character so noble and modern-thinking she makes Dr. Quinn, Medicine Woman look like a backwoods savage."  Tom Jicha of the Sun-Sentinel faulted the film for being riddled with "cliched scenes of plantation atrocities" that "frequently cross the line between realism and titillation," such as an early scene in which a black couple are attacked for having sex. 

The historical accuracy of the film has also been a source for complaint. In his book Echoes of War: A Thousand Years of Military History in Popular Culture,  Michael C. C. Adams cited Enslavement as an example of a movie that claims historical accuracy while "gratuitously and radically distorting" the truth.  The Seattle Times negatively reviewed the film for "exaggerating and sensationalizing" Kembles life,  while Michael Kilian of the Chicago Tribune criticized the portrayal of John Quincy Adams as a racist. Kilian did however commend it for depicting the "horrors of slave life on a sea island plantation   every bit as compelling as those contained in the journal Kemble kept and later published in England during the Civil War – a book that helped turn British public opinion against recognition of the South." 
 Charles Bernstein 2001 Black Reel Awards. 

==See also==
 
* List of films featuring slavery

== References ==
 

== External links ==
*  

 
 
 