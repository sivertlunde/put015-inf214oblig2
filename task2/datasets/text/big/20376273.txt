Smile, Darn Ya, Smile!
 
{{Infobox Hollywood cartoon
| cartoon_name = Smile, Darn Ya, Smile!
| series = Foxy (Merrie Melodies)|Foxy/Merrie Melodies
| image = Smile, Darn Ya, Smile!.jpg
| caption = The opening scene
| director = Rudolf Ising (uncredited)
| story_artist =
| animator = Friz Freleng Carman Maxwell
| layout_artist =
| background_artist =
| narrator =
| voice_actor =
| musician = Frank Marsales Abe Lyman
| producer = Leon Schlesinger Harman and Ising
| studio = Warner Bros.
| distributor = Warner Bros. The Vitaphone Corporation
| release_date = September 5, 1931
| color_process = Black-and-white
| runtime = 7 minutes
| preceded_by = Lady, Play Your Mandolin! One More Time English
}}
 One More Time (October 3, 1931). This short is a remake of Trolley Troubles, a Disney short featuring Oswald the Lucky Rabbit in whose creation Hugh Harman had once been involved.

==Synopsis== trolley Railroad engineer whose hippo who cow who wont get off the track. A group of nearby hobos sing the title song while Foxy tries to move the cow; he finally runs the car underneath the cow and goes on his way.

The trolley then goes down a hill and runs out of control; Foxy tries to stop it, but the brakes dont work. Finally, the trolley runs off of a cliff, throwing Foxy right into the camera... and then he falls from bed, waking up from what has turned out to be just a nightmare. The radio by his bed is playing the title song, and the annoyed Foxy smashes the radio with a bedpost upon hearing it.

==Notes==
* Foxys name was mentioned for the first time in this cartoon.
* The song in the short was made by Billy Cotton (the song was named after the short).
* The title song was featured in Prosperity Blues, a 1932 Krazy Kat cartoon.
* The title song was also featured twice in the 1988 movie Who Framed Roger Rabbit; first when Eddie drives into Toontown, and again at the end of the film. The song can now be heard as part of the background area music in the Esplanade at the Disneyland Resort as well as in the Mickeys Toontown area of Disneyland Park. played backwards: "Susie heard one of those Atlantic bells! Whataya think?" Arrow collars cough drops ("Sniff Brothers Cough Drops") and Fisk Tires ("Risk Tires").
* A portion of this cartoon appeared on a couple episodes of Pee-wees Playhouse.
* The songs title is referenced in the chorus of "Wheres the Fun in That?", a song   episode "Emperor Joker!" colorized in the 1990s by re-drawing the animation and re-painting the backgrounds.  
* The song was performed by Christoph Waltz when he hosted Saturday Night Live on February 16, 2013.
* Same tunnel gag from Mickeys Choo Choo (1929).
* That was the first pre-1948 WB short sold to Associated Artists Productions in 1956.

==Availability==
* Looney Tunes Golden Collection: Volume 6
* Return of the 30s Characters

==External links==
*  
*   at the Big Cartoon DataBase

 
 
 
 
 
 
 
 
 
 
 
 
 