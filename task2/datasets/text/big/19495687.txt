Mosekongen
 
{{Infobox film
| name           = Mosekongen
| image          = Mosekongen.jpg
| caption        = Film poster
| director       = Jon Iversen Alice OFredericks
| producer       = Henning Karmark
| writer         = Morten Korch Svend Rindom
| starring       = Johannes Meyer
| music          = Sven Gyldmark
| cinematography = Rudolf Frederiksen
| editing        = Marie Ejlersen
| distributor    = 
| released       =  
| runtime        = 92 minutes
| country        = Denmark 
| language       = Danish
| budget         = 
}}

Mosekongen is a 1950 Danish family film directed by Jon Iversen and Alice OFredericks.

==Cast==
* Johannes Meyer - Claus Munk
* Poul Reichhardt - Jørgen Munk
* Tove Maës - Hanne
* Peter Malberg - Sofus Fusser Hansen
* Grete Frische - Norma William Rosenberg - Erik Jelling
* Signi Grenness - Grete Sander
* Asbjørn Andersen - Julius Sander
* Grethe Holmer - Ellen Madsen
* Randi Michelsen - Abelone Madsen
* Axel Frische - Jesper Madsen
* Agnes Rehni - Fru Karen Winge
* Poul Müller - Ejendomsmægler Søren Just
* Ib Schønberg - Martin Hald
* Helga Frier - Johanne Hald
* Sigurd Langberg - Tjener Rasmussen Henry Nielsen - Ole Post
* Jørn Jeppesen - Redaktør Juul
* Ruth Brejnholm - Stuepigen Clara
* Christian Møller - Redaktør Andreasen
* Edith Hermansen
* Anna Henriques-Nielsen - Marianne Wævers
* Carl Heger - Ole
* Aage Foss - Gamle Søren

==External links==
* 

 

 
 
 
 
 


 