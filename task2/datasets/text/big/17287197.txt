Night Caller from Outer Space
 
 
{{Infobox film
| name           = Night Caller from Outer Space
| image          = "Night_Caller_from_Outer_Space"_(1965).jpg
| image_size     = 
| caption        = 
| director       = John Gilling
| producer       = Ronald Liles
| writer         = Frank Crisp (novel) Jim OConnolly (screenplay) John Saxon Maurice Denham Patricia Haines Alfred Burke Warren Mitchell
| music          = Johnny Gregory
| cinematography = Stephen Dade
| editing        = Philip Barnikel
| studio         = Armitage Film Productions Ltd.
| distributor    = Butchers Film Service (UK)
| released       = 1965
| runtime        = 85 minutes
| country        = United Kingdom
| language       = English
| budget         = 
| gross          = 
}}
Night Caller from Outer Space, also known as simply The Night Caller or Blood Beast from Outer Space, is a British 1965 science fiction film directed by John Gilling.  It is based on Frank Crisps novel The Night Callers.

==Synopsis==
Scientist Jack Costain (John Saxon) and his aides investigates a meteorite in the British countryside, discovering that it is an alien device from Ganymede (moon)|Ganymede, a moon of Jupiter. They capture a tall alien and take it to the lab, only to have it escape. Shortly thereafter, teenage girls begin disappearing after answering an advertisement for Bikini Girl magazine. It turns out the aliens want Earth women for breeding purposes... {{cite book
  | first=Steven | last=Puchalski | year=2002
  | title=Slimetime: a guide to sleazy, mindless movies
  | page=207 | edition=2nd | isbn=1-900486-21-0
  | publisher=Headpress/Critical Vision }}  {{cite web
  | first=Mark | last=Deming | work=The New York Times
  | title=Blood Beast From Outer Space (1966)
  | url=http://movies.nytimes.com/movie/35202/Blood-Beast-From-Outer-Space/overview | accessdate=2009-09-19 }} 

==Cast== John Saxon as Dr. Jack Costain 
* Maurice Denham as Dr. Morley 
* Patricia Haines as Ann Barlow 
* Alfred Burke as Detective Supt. Hartley 
* Warren Mitchell as Reg Lilburn 
* Stanley Meadows as Det. Tom Grant 
* Aubrey Morris as Thorburn 
* Ballard Berkeley as Cmdr. Savage 
* Marianne Stone as Madge Lilburn 
* Geoffrey Lumsden as Colonel Davy Jack Watson as Sergeant Hawkins  

==Reception==
Leonard Maltin called it a "well-done sci-fi thriller" and rated it as two and a half stars. {{cite book
  | first=Leonard | last=Maltin | year=2008
  | title=Leonard Maltins 2009 Movie Guide
  | page=980 | publisher=Penguin Group
  | isbn=0-452-28978-5 }}  Author Steve Puchalski said, "this alleged film is dry and slow paced, uninvolving and uninspiring". 
UK prints of the film feature Alan Havens version of the hit instrumental "Image" as its theme.

==References==
 

== External links ==
*  

 

 
 
 
 
 
 
 
 