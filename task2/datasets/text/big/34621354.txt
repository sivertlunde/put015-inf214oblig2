Sathyathinte Nizhalil
{{Infobox film
| name           = Sathyathinte Nizhalil
| image          =
| caption        =  Babu Nandancode
| producer       = Prema Shanmugham
| writer         = 
| screenplay     = Sreekumaran Thampi
| story          = P. Shanmugham
| based on       =  
| narrator       = 
| starring       =  
| music          = V Dakshinamoorthy
| cinematography = P. S. Nivas
| editing        = Sasikumar
| studio         = Hariram Movies
| distributor    = 
| released       =  
| runtime        = 
| country        = India
| language       = Malayalam
| budget         = 
| gross          = 
}}
 Babu Nandancode.  The story was written by P. Shanmugham with dialog and screen Play by Sreekumaran Thampi.  Playback singers were Ambili (singer)|Ambili, K. J. Yesudas, and P. Susheela, and actors were Sudheer (Malayalam actor)|Sudheer, K. P. Ummer, Ushanandini, Bahadoor, Kuthiravattam Pappu|Pappu, Thikkurisi Sukumaran Nair, Philomina, Janardanan, and Kunchan (actor)|Kunchan.
 Sudheer won Kerala State Government for his role in the film.

==Plot==

 

==Cast== Sudheer
* Bahadoor
* Janardanan Kunchan
* Thikkurisi Sukumaran Nair Pappu
* Philomina
* Usha Nandhini
* Usharani

==Soundtrack==
The music was composed by V. Dakshinamoorthy and lyrics was written by Sreekumaran Thampi. 

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCF" align="center"
| No. || Song || Singers ||Lyrics || Length (m:ss)
|-
| 1 || Kaaladevathathanna Veena || P Susheela || Sreekumaran Thampi ||
|-
| 2 || Njanuminnoru Dushyanthanaayi || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 3 || Swargathilulloru || K. J. Yesudas || Sreekumaran Thampi ||
|-
| 4 || Swarnamalli Pushpavanathil || K. J. Yesudas, Ambili || Sreekumaran Thampi ||
|}

==References==
 

==External links==
*   at the Internet Movie Database

****

 
 
 
 


 