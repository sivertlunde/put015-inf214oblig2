Kanamachi (film)
{{Infobox film
| name           = Kanamachi
| image          = Kanamachi 2013 Bengali film poster.jpg
| alt            = 
| caption        = 
| director       = Raj Chakraborty
| producer       = Ashok Dhanuka
| writer         = Abhimanyu Mukherjee
| story          = K.V. Anand
| starring       = Ankush Hazra Srabanti Chatterjee Abir Chatterjee Sayani Ghosh Rajatava Dutta
| music          = Indradeep Dasgupta Rishi Chanda
| cinematography = Subhankar Bhar Yuvraj
| editing        = Rabi Ranjan Moitra 
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = India Bengali
| budget         = 
| gross          = 
}} Bengali political Ko .    The music directors of the film are Indradeep Dasgupta and Rishi Chanda.   

== Plot ==

The film begins with Abir (Ankush Hazra), a young, energetic photojournalist working for the private newspaper Anandabazar Patrika (ABP), witnessing Naxalites robbing a local bank. He chases them and manages to click photos of the robbers. When he tries to escape, he is stopped by a young woman Nayana (Srabanti Malakar) who assumes he is the culprit and helps the robbers to take his camera. However, Abir manages to secure the memory card of the camera. The cops catch him, and to prove that he is a press photographer he shows the photos to them. The cops identify everyone in the photos except the gang leader (whose face is covered by a mask). At his office, Abir again meets Nayana, who has newly joined as Article Editor. Payel (Sayani Ghosh) also works in his office and has feelings for Abir. However, Abir does not reciprocate her feelings and treats her only as a friend.

Settled in her new job, Nayana writes a cover story about the leader of the opposition Tarakeshwar Dutta (Supriyo Dutta), describing his attempt to marry a minor. Enraged by this, Tarakeshwar barges into the newspaper office and shouts at Nayana. The audio recording of her interview disappears (courtesy Tarakeshwar), and she is fired from her job for falsified reporting. Abir later risks his life to capture the pictures of Tarakeshwar who actually engages in child marriage in a temple at midnight. Later the story with the photos is printed in their newspaper and this makes Nayana to fall in love with him, which Abir reciprocates. Payel is initially jealous of the love between Abir and Nayana, but later accepts it and gets over Abir.

Meanwhile, Abhimanyu (Abir Chatterjee), a recent graduate and engineer, strives hard to provide education and spread awareness to the poor people. He and his team of graduates try to contest against more experienced politicians who capture the people by giving freebies and money. No one cares about Abhimanyu and his team of graduates, who promise a healthy government to people. Their party name is Barno Porichoy (Grammar Introduction). One day, Abhimanyu gets attacked by some miscreants on the streets and he rushes towards the ABP office to get help. The attackers are thrown away by the office staffs and security. Abhimanyu tells the staff that those miscreants are from the ruling party and they have been sent by the Chief Minister Surojit Sarkar (Rajatava Dutta). Abir and Nayana find out about Abhimanyus election campaign, and they and the entire ABP team extend their support to the campaign, covering it extensively, much to the chagrin of Tarakeshwar and the Chief Minister.

One night, Abhimanyus party organises an election campaign meeting. Abir, photographing the event, receives a text message from Payel that states there is a bomb underneath the stage where Abhimanyu is speaking. Abir manages to save Abhimanyu just before the bomb explodes. Later, Abir finds Payel fatally wounded near the blast site. Payel struggles to tell something to Abir and Nayana before she dies. Abir, through a video clip recorded by another photographer, finds out that Payel was intentionally killed by someone. He later notices a resemblance between the leader of the bank robber and this unknown killer, concluding that the Naxalite leader killed Payel.

A few days later, Nayana notices that Abhimanyus photo in their newspaper was cut from a college class photo, where Abir is also present. Confronted by Nayana, Abir tells her that he and Abhimanyu studied in the same college and were best friends. He also tells her that he is happy for Abhimanyus success and is wholeheartedly supporting his election campaign.

In the election, Abhimanyus party wins by a huge majority and Abhimanyu becomes the Chief Minister. He orders the release of 10 naxals on Republic Day, citing humanity. Shocked on hearing this news, Abir rushes to the secretariat to meet Abhimanyu. In Abhimanyus office, Abir notices that the Naxalite leader who killed Payel is sitting there, talking to Abhimanyu. Abir airs his grievances to Abhimanyu, who ignores him. Abir then follows the leader to his hideout. At the same time, Abhimanyu orders the Kolkata Police to go kill the Naxals at their hideout and the Police Commandos surround the perimeter of that place and start firing at the Naxalites. The Naxalite leader gets shot during the shooting and his group take hold of some people (including children) as hostage. Abir, already at the hideout, confronts the Naxalite leader, whose name is Shankar and finds out from him that Abhimanyu had made a deal with the Naxalites to help him win the election. They orchestrated events such as the burning of a hut and saving the woman in that hut (who was also a Naxalite) in order to win peoples sympathy. In the same vein, they had planted a bomb on the stage where Abhimanyu was speaking during his meeting. Payel had found out the truth about Abhimanyu, but was fatally assaulted by him so that she does not reveal his character and intentions to anybody. Abir realises that Payel had tried to warn him and Nayana about Abhimanyus true character before dying and also that now Abhimanyu is double-crossing the Naxalites and is planning to kill them as a show of achievement. Abir records the confession of Shankar using his mobile phone camera and sends it to Nayana, who plans to publish this story in their newspaper. Abhimanyu then arrives outside the hideout and asks the police personnel to let him go inside alone and try to reason with the Naxalites. Abhimanyu goes inside and asks Shankar to kill Abir. Shankar refuses as Abir has saved his life by getting a Doctor for him to cure his wounds. Abhimanyu then kills Shankar. He also tries to kill Abir. However, Abir triggers a bomb there which explodes, killing Abhimanyu, while Abir manages to escape.

Meanwhile, Abhimanyus party members arrive at the hideout on hearing the news that Abhimanyu had died. Seeing their innocence, Abir forces Nayana not to reveal the truth about Abhimanyu because his party members would get into serious trouble and lose their seats just for supporting Abhimanyu. They did not know about Abhimanyus true character and intentions and supported him, believing his false claims of a healthy government. Instead Nayana publishes an article saying that Abhimanyu had sacrificed his life fighting the Naxalites and died as a martyr.

The film ends with Abir and Nayana submitting their resignations to the Chief Editor for falsified reporting, but he rejects their resignations and tells them to cover the upcoming election.

== Cast ==
* Ankush Hazra  as Abir
* Abir Chatterjee as Abhimanyu
* Srabanti Chatterjee as Nayana
* Sayani Ghosh as Payel
* Rajatava Dutta as Chief Minister Surojit Sarkar
* Supriyo Dutta as Tarakeshwar Dutta (Leader of the Opposition)
* Shankar Chakraborty as Chief Editor
* Shantilal Mukherjee as the Naxalite leader
* Aritra Dutta Banik as Renukas friend
* Biswajit Chakraborty(cameo) as Abirs father

==Soundtrack==
{{Infobox album  
| Name       = Kanamachi
| Type       = soundtrack
| Artist     = Indradeep Dasgupta Rishi Chanda
| Cover      = Kanamachi poster.jpg
| Alt        = 
| Released   =  
| Recorded   =  Feature film soundtrack
| Length     =  
| Label      = Eskay Movies
| Producer   = {{flatlist|
* Ashok Dhanuka
* Himangshu Dhanuka
}}
}}

Music director Indradeep Dasgupta and Rishi Chanda was roped as the music directors. The grand music launch of this film had taken place in the Mirchi Republic Of The Rocks, a music concert at Nicco Park.

{{Track listing
| collapsed       = 
| headline        = Kanamachi Album Songs
| extra_column    = Artist(s)
| total_length    =  
| all_music       = Indradeep Dasgupta, Rishi Chanda
| title1          = Mon Baawre
| extra1          = Arijit Singh, Timir Biswas, Anweshaa and Ujjaini Mukherjee
| length1         = 4:46
| title2          = Beporowa Mone
| extra2          = Kunal Ganjawala
| length2         = 4:18
| title3          = Dil Dorodiya
| extra3          = Zubeen Garg, June Banerjee
| length3         = 3:18
| title4          = Kanamachi Title Track
| extra4          = Kunal Ganjawala, Rishi Chanda
| length4         = 4:28
| title5          =Kanamachi Mashup
| extra5          =Several Artists
| length5         = 4:15
}}

==Critical Response==
Jaya Biswas of The Times of India reviewed "Abhimanyu is definitely the surprise package. He does a brilliant job — totally breaking away from his Bomkesh avatar.
Srabanti as Nayona makes for a pretty picture with her colour-coordinated specs matching her dresses. Sayani as the effervescent and chirpy friend Payel shows sparks of a fine actress. Credit goes to Raj for bringing out the Dev in Ankush, who looks and acts like him. Barring a few over-the-top antics, Ankush is not bad. Kanamachi may have its takers, especially in the single screens. For all you know, it might just work for its special effects!" 
Writing for The Daily Star, Zia Nazmul Islam told "Minus the song-dance items and distracting change in focus at times, Kanamachi is an entertaining movie with a strong message." 

== See also ==
* Tor Naam
*Game

== References ==
 
 

 
 