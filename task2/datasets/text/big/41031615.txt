Ajah Berdosa
{{Infobox film
| name      = Ajah Berdosa
| image     = Ajah Berdosa ad.jpg
| image size   = 
| border     = 
| alt      = 
| caption    = Newspaper ad
| director    =Wu Tsun
| producer    =
| writer     = 
| starring    = {{Plainlist|
*M Arief
* S Waldy
* Elly Joenara
*Soetijem
}}
| music     = 
| cinematography = Chok Chin Hsien
| editing    =  Star Film
| distributor  = 
| released    =  
| runtime    = 
| country    = Dutch East Indies Malay
| budget     = 
| gross     = 
}} Star Film. Starring M. Arief, S Waldy, Elly Joenara, and Soetijem, it follows a villager named Mardiman over a period of several years, in which he loses everything owing to his infatuation with a "modern" woman.

==Plot==
Through diligence and hard work, a young villager named Mardiman, has developed a successful career as a scribe. His wife, Warsiah, and her family have been unable to help him advance his career, as none of them have any influence. Not long after Warsiah gives birth to a son, Mardiman is promoted and asked to work in the city. There he becomes infatuated with a sophisticated city woman, one who is more "modern" than Warsiah.

Mardiman leaves his wife and sick son so that he can be with this new woman, but soon spends all of his money trying to please her and treat her to a high-class lifestyle. Unable to support himself, yet unwilling to abandon his infatuation and return to the village, Mardiman begins to embezzle money from his office. After he is caught, he is imprisoned and loses everything he has in the city.

After being released from prison, Mardiman is unable to find honest employment. He finally begins working for an underground Arak (drink)|arak syndicate. When the police raid the facility, Mardiman is severely injured. He is sent to a hospital, where he is treated by Warsiah&nbsp;– who has become a nurse&nbsp;– and their son, now a doctor. Mardiman soon repents his sins. .}}

==Production== Star Film, native journalist who had joined the company in 1941 for Wus directorial debut, Lintah Darat.   The black-and-white film, set and filmed around Batavia (now Jakarta),  starred M. Arief, S Waldy, Elly Joenara, and Soetijem. 

==Reception==
Ajah Berdosa was released in 1941,  and by early January 1942 was already being screened in Surabaya.  Targeted at all ages, it was advertised as "an extremely simple and touching story".   An anonymous reviewer for the Soerabaijasch Handelsblad recommended the film due to the quality of its acting and technical aspects, citing it as having the "momentum and dynamics"  necessary for success. 

The film is likely lost film|lost. The American visual anthropologist Karl G. Heider writes that all Indonesian films from before 1950 are lost.  However, JB Kristantos Katalog Film Indonesia (Indonesian Film Catalogue) records several as having survived at Sinematek Indonesias archives, and Biran writes that several Japanese propaganda films have survived at the Netherlands Government Information Service. 

==Explanatory notes==
 

==References==
 

==Works cited==
 
* {{cite web
  | title = Ajah Berdosa
  | language = Indonesian
  | url = http://filmindonesia.or.id/movie/title/lf-a011-41-072767_ajah-berdosa
  | work = filmindonesia.or.id
  | publisher = Konfiden Foundation
  | location = Jakarta
  | accessdate = 26 July 2012
  | archiveurl = http://www.webcitation.org/69QxtZAv4
  | archivedate = 26 July 2012
  | ref =  
  }}
*{{cite book
 |title= 
 |trans_title=History of Film 1900–1950: Making Films in Java
 |language=Indonesian
 |last=Biran
 |first=Misbach Yusa
 |author-link=Misbach Yusa Biran
 |location=Jakarta
 |publisher=Komunitas Bamboo working with the Jakarta Art Council
 |year=2009
 |isbn=978-979-3731-58-2
 |ref=harv
}}
*{{cite book
 |url=http://books.google.ca/books?id=m4DVrBo91lEC
 |title=Indonesian Cinema: National Culture on Screen
 |isbn=978-0-8248-1367-3
 |author1=Heider
 |first1=Karl G
 |year=1991
 |publisher=University of Hawaii Press
 |location=Honolulu
 |ref=harv
}}
*{{cite news
 |title=Nieuwe Inheemsche Filmproductie Ajah Berdosa
 |trans_title=New Native Film Production, Ajah Berdosa
 |language=Dutch
 |work=Bataviaasch Nieuwsblad
 |location=Batavia
 |page=3
 |date=10 January 1942
 |url=http://kranten.kb.nl/view/article/id/ddd%3A110612835%3Ampeg21%3Ap003%3Aa0093
 |ref= 
}}
*{{cite news
 |title=Sampoerna-theatre: Ajah Berdosa
 |language=Dutch
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |page=6
 |date=9 January 1942
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122023%3Ampeg21%3Ap006%3Aa0097
 |ref= 
}}
*{{cite news
 |title=(untitled)
 |language=Dutch
 |work=Soerabaijasch Handelsblad
 |location=Surabaya
 |page=7
 |date=7 January 1942
 |url=http://kranten.kb.nl/view/article/id/ddd%3A011122019%3Ampeg21%3Ap007%3Aa0121
 |ref= 
}}
 

 
 
 