French Roast
 
{{Infobox film name           = French Roast image          = French Roast poster.jpg caption        = Poster for French Roast alt            = Poster for French Roast director       = Fabrice O. Joubert producer       = Bibo Bergeron writer         = Fabrice O. Joubert starring       = music          = Olivier Liboutry cinematography = editing        = distributor    = released       =   runtime        = 8 minutes, 15 seconds country        = France budget         = preceded_by    =
}}
French Roast is a 2008 French computer-animated short created by Fabrice O. Joubert. The short received ANIMA award and was nominated for Academy Award for Best Animated Short Film in 2009.

French Roast is the first short film by Fabrice O. Joubert,  an animator, who worked from 1997-2006 at DreamWorks Animation and later worked as the animation director of A Monster in Paris (2011). 

==Plot==
In a fancy Parisian Café c. 1960, an uptight businessman discovers he forgot to bring his wallet and bides his time by ordering more coffee.

==Release==
French Roast was first released in France on October 30, 2008 at the Festival Voix dEtoiles. It was later released in the Czech Republic on May 3, 2009 at the AniFest Film Festival; Canada on February 19, 2010 in Waterloo, Ontario; and in the USA on February 19, 2010, limited release.

==Accolades==
Fabrice Joubert was presented with the ANIMA for Best Animation at the 2009 Córdoba International Animation Festival.  Fabrice Joubert has been nominated for Academy Award for Best Animated Short Film in 2009. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 
 

 
 