Good Cheer
 
{{Infobox film
| name           = Good Cheer
| image          = 
| caption        = 
| director       = Robert F. McGowan
| producer       = Hal Roach F. Richard Jones
| writer         = Hal Roach H. M. Walker
| starring       = 
| cinematography = 
| editing        = Richard C. Currier
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States  Silent English intertitles
| budget         = 
}}
 short silent silent comedy film directed by Robert F. McGowan.       It was the 46th Our Gang short subject released.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Mickey Daniels as Mickey
* Johnny Downs as Johnny
* Allen Hoskins as Farina
* Mary Kornman as Mary
* Jay R. Smith as J.R.
* Jannie Hoskins as Arnica
* David Durand as Marys little brother
* Pal the Dog as Himself
* Dinah the Mule as Herself

===Additional cast===
* Jack Ackroyd as Crooked Santa
* Chet Brandenburg as Pedestrian / Crooked Santa
* Ed Brandenburg as Store window assistant
* Richard Daniels as Old man Jack Gavin as Crooked Santa Charlie Hall as Motorist / Crooked Santa
* Al Hallett as Crooked Santa Jack Hill - Pedestrian
* Wallace Howe as Crooked Santa
* Sam Lufkin as Inebriated Santa Claus
* Jules Mendel as Crooked Santa
* Gene Morgan as First officer
* William Orlamond as Crooked Santa
* Tonnage Martin Wolfkeil as Store window Santa
* Noah Young as Second officer

==See also==
* Our Gang filmography

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 
 
 
 
 
 