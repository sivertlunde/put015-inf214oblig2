Good Gracious, Annabelle
{{infobox film
| title          = Good Gracious, Annabelle
| image          = Good Gracious, Annabelle (1919) - Ad 1.jpg
| image_size     =
| caption        = Ad for film
| director       = George Melford
| producer       = Adolph Zukor Jesse Lasky
| writer         = 
| based on       =  
| cinematography = Paul Perry
| editing        =
| studio         = Famous Players-Lasky
| distributor    = Paramount Pictures
| released       =  
| runtime        = 50 minutes
| country        = United States Silent (English intertitles)
}}
Good Gracious, Annabelle is a lost  1919 American silent society comedy film starring Billie Burke. It is based on a Broadway play, Good Gracious, Annabelle by Clare Kummer. This film was produced by Famous Players-Lasky with distribution by Paramount Pictures.   

==Plot==
As described in film magazines,     Annabelle Leigh (Burke) lives extravagantly on a quarterly allowance that she spends monthly, until she is tricked out of two shares of a mining stock by crude, western miner John Rawson (Rawlinson), who compelled her to marry him after the death of her father in a squabble over the stock. The marriage is little more than form and rater than keeping her in a lonesome cabin where she cries perpetually, her magnanimous husband sets her free to go to New York City where she lives in an extravagant style. During a struggle for possession of her stock certificates with financier George Wimbledon (Kent), she takes a violent fancy towards a mysterious millionaire whom she meets during a party at Kents Long Island estate. She tells him that she is resorting to all of the tricks she plays simply to save her husband, whose interests are threatened. The mysterious millionaire turns out to be that husband, who has shaved off his beard and wins her this time through love.

==Cast==
*Billie Burke - Annabelle Leigh
*Herbert Rawlinson - John Rawson
*Gilbert Douglas - Harry Murchison
*Crauford Kent - George Wimbledon
*Frank Losee - William Gosling
*Leslie Casey - Wilbur Jennings
*Gordon Dana - Alfred Weatherby
*Delle Duncan - Ethel Deane
*Olga Downs - Gwendolyn Morley
*Thomas Braidon - James Ludgate
*Billie Wilson - Lottie

==Later adaptation==
The play was also the basis of the 1931 romantic comedy Annabelles Affairs.

==References==
 

==External links==
 
* 
* 

 
 
 
 
 
 
 
 


 