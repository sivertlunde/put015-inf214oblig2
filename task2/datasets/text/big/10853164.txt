Bhookailas (1958 Telugu film)
{{Infobox film
  | name = Bhookailas (1958)
  | image =Bhookailas58.jpg
  | caption =
  | director = K. Shankar
  | producer = A.V. Meiyappan
  | writer = Samudrala Raghavacharya Jamuna S.V. Ranga Rao
  | music = R. Sudarsanam, R. Govardhanam
  | cinematography = Madhava Bulbule
  | editing = K. Narayanan and K. Shankar
  | distributor = AVM Productions
  | released = 20 March 1958
  | runtime = 174 minutes.
  | language =  Telugu
  | budget =
  | preceded_by =
  | followed_by =
    }} Gokarna Kshetram in Karnataka. The film was released in Tamil as Bhaktha Ravana.

==Plot==
Rakshas King Ravana decides to invade Amaravathi, the capital of the heavenly kingdom of Indra. Scared of Ravanas plans, Indra asks Narada for help. Narada informs Indra that Ravanas strength comes from the worship performed by Kaikasi, Ravanas mother. He then suggests that Indra sabotage Ravanas mothers worship of saikatha lingam, a sand sculpture representation of Lord Siva. Ravana decides to perform penance and bring Lord Sivas Atma Lingam for his mother to worship. Hearing of Ravanas plans from Narada, Goddess Parvathi, Lord Sivas consort, appeals to Lord Vishnu. When Lord Siva appears to Ravana to grant his wish, Lord Vishnu manipulates Ravanas mind and makes him wish for Goddess Parvathi. As Ravana proceeds home with Goddess Parvathi, Narada meets him midway and tells him that his companion is fake Parvathi. Dejected by the subterfuge, Ravana returns Goddess Parvathi to Lord Siva. During his return journey, he meets Mandodari, the young princess of Pathala, and believing her to be the real Goddess Parvathi, marries her. Eventually, he realizes what happened and appeals to Lord Siva for forgiveness by presenting his severed head. Lord Siva presents Ravana with Atma Lingam and warns him that, if the Atma Lingam ever touches the earth, it can never be moved again. Narada instigates Lord Vinayaka to trick Ravana into grounding the Atma Lingam at what later became known as Gokarna Kshethram in Karnataka.

==Cast==
{| class="wikitable"
|-
! Actor !! Character
|-
| Nandamuri Taraka Rama Rao || Ravana
|-
| Akkineni Nageswara Rao || Narada
|- Jamuna || Mandodari
|-
| S. V. Ranga Rao || Mayasura
|- Siva
|-
| B. Saroja Devi || Goddess Parvati
|-
| Hemalatha || Kaikesi
|- Helen || Apsara
|-
| Vijaya Nirmala || Goddess Sita
|-
| Mahankali Venkaiah || Kumbhakarna
|}

==Crew==
* Director: K. Shankar	 	
* Writer: Samudrala Raghavacharya	 	
* Producer: A.V. Meiyappan
* Production Company: A.V.M. Productions	
* Music Directors: R. Goverdhanam and R. Sudarsanam	 	
* Cinematography: Madhav Bulbule	 	
* Film Editing: K. Narayanan and K. Shankar	 	
* Art Direction: A. Balu
* Choreographer: Dandayudhapani Pillai	 	
* Lyrics: Samudrala Raghavacharya
* Playback singers: A. P. Komala, M. L. Vasantha Kumari, Ghantasala Venkateswara Rao, P. Susheela and T. S. Bhagavathi

==Songs==
*Andamulu Chinde Avani Idena
*Deva Deva Dhavalachala - (Lyrics: Samudrala; Singer: Ghantasala (singer)|Ghantasala)
*Deva mahadeva mamu brovumu siva - (Lyrics: Samudrala; Singer: M. L. Vasantha Kumari)
*Munneeta Pavalinchu - (Lyrics: Samudrala; Singer: M. L. Vasantha Kumari)
*Naa Nomu Phalinchenugaa (Lyrics: Samudrala; Singer: P. Susheela)
*Narayana Hari Namo Namo - (Lyrics: Samudrala;Singer: Ghantasala)
*Neela Kandharaa - (Lyrics: Samudrala; Singer: Ghantasala)
*Premaleevidhamaa (Lyrics: Samudrala; Singers: Ghantasala and P. Susheela)
*Raamuni Avathaaram - (Lyrics: Samudrala; Singer: Ghantasala)
*Sundaranga Andukora
*Tagunaa Varameeya - (Lyrics: Samudrala; Singer: Ghantasala)
*Teeyani Talapula Teevelu Saage (Lyrics: Samudrala; Singer: P. Susheela)

==External links==
*  
*  
*  

 
 
 
 
 
 