Ladies and Gentlemen, The Fabulous Stains
 
{{Infobox film
| name = Ladies and Gentlemen, The Fabulous Stains
| image = Stainsposter.jpg
| caption = Original film poster
| director = Lou Adler
| producer = Joe Roth
| writer = Rob Morton (Pseudonym of Nancy Dowd) Steve Jones Paul Cook Paul Simonon Fee Waybill Barry Ford Black Randy Elizabeth Daily Brent Spiner
| cinematography = Bruce Surtees
| editing = Tom Benko
| distributor = Paramount Pictures
| released =  
| runtime = 87 min.
| country = United States
| language = English
}}
 punk band.

==Plot== garage band "The Stains", which consists of her, her sister Tracy, and their cousin Jessica.

Emboldened by appearing on television, Corinne attends a concert put on by small-time promoter Lawnboy, featuring the washed-up metal band the Metal Corpses and their opening act, an up-and-coming punk band called the Looters. Eager to end hostilities between the jaded Metal Corpses and the hedonistic Looters, Lawnboy signs the Stains without having heard them perform. Corinne and the Stains join the bands on tour, witnessing firsthand the bands animosity towards one another, largely the result of the conflict between the aging Lou, the frontman for the Metal Corpses, and Billy, the Looters volatile lead singer.

At their first show, the Stains prove to be completely inept as a band: Neither Jessica nor Tracy can play instruments, and Corinne sings in an off-key monotone. The audience reacts angrily, prompting Corinne to lash out at them for a variety of real and perceived faults. After the show, the Metal Corpses guitar player is found dead in the bathroom; the Metal Corpses decide to leave the tour, with Lawnboy making the Looters the new headliners with the Stains as their opening act. A dissatisfied Billy asks Lawnboy to replace the Stains as soon as possible.

At their next show, Corinne debuts a new, more extreme punk look, with hair dyed to resemble a skunk and a see-through blouse worn over a pair of bikini briefs. Claiming that she "never puts out," she goes on another tirade, garnering media attention. While male journalists focus on Corinnes antisocial attitude and the bands lack of talent, female journalists perceive Corinnes rants as calls for female empowerment and  hail the Stains as a new voice of feminism. Almost literally overnight, the Stains become a national sensation, with girls all over the country emulating Corinne in every way possible, from dying their hair to running away from home.
 plagiarizes Billys song, which skyrockets the band to even further stardom. With Robells encouragement, Corinne signs a new contract, cutting Lawnboy out of any royalties and making the Stains the new headliners of the tour.
 corporate sell outs; when the Stains come onstage, the fans riot, and Corinne is attacked by a girl with a tube of hair dye. The tour becomes a financial disaster and Robell cancels the Stains contract. Corinne responds by attacking him with a bottle opener and robbing him of the money he has in his wallet; Corinne then presents it to Lawnboy as an apology.

The next morning, Corinne appears on television, where a journalist chastises her for having been a poor role model to her fans. Billy apologies for ruining Corinnes career and asks her to come back as the Looters opening act. Corinne refuses; as she wanders the streets, she overhears a radio broadcast identifying the Stains first song as a hit record. Some time in the future, the Stains make their MTV debut, having become a successful act on Lawnboys new record label.

== Cast == Steve Jones and Paul Cook, along with Paul Simonon from The Clash, and Vince Welnick and Fee Waybill from The Tubes. Los Angeles punk icon Black Randy from Black Randy and the Metrosquad makes an appearance as himself and as "Mexican Randy".

There are brief appearances by Elizabeth Daily as a hotel maid, and Brent Spiner. Musician Barry Ford plays the busdriver named Lawnboy. Ford wrote the majority of the soundtrack including the title song "All Washed Up"; which features Jones, Cook, and Simonon.

*Peter Donat as Harley Dennis
*Diane Lane as Corinne Burns
*Marin Kanter as Tracy Burns
*Laura Dern as Jessica McNeil
*Christine Lahti as Aunt Linda
*Janet Wright as Aunt Lindas Friend - Brenda
*Mia Bendixsen as Pregnant Girl in Ladies Room
*Stuart Ferguson as Gold Key D.J.
*Ray Winstone as Billy - Vocals for The Looters
*Paul Simonon as Johnny - Bassist for The Looters Steve Jones as Steve - Guitarist for The Looters
*Paul Cook as Danny - Drummer for The Looters
*Fee Waybill as Lou Corpse - Vocals for The Metal Corpses
*Vince Welnick as Jerry Jervey - Guitarist for The Metal Corpses
*Vern Willis as Corpses Bass

== Production == Coming Home. Walt Disney Studios. Punk rock journalist Caroline Coon was a technical advisor on the film.

== Release ==
The film was never given wide release.  A screening in  . Retrieved on July 4, 2009.  in Chicago in April 1985,  in Atlanta and Los Angeles in July 1985, and at the Theater of the Living Arts in Philadelphia from Friday, August 23 – Saturday, August 24, 1985. 

== Release ==

=== Home media ===
Rhino Home Video released a DVD on September 16, 2008.

== Reception == Night Flight, Los Angeles local cable Z Channel and Showtime (TV network)|Showtime. 
 Nuart Cinema in Los Angeles lobbied for the sole remaining print to be kept in circulation, and the Nuart has screened the film multiple times since 2001.  In 1998, the film showed at the Chicago Underground Film Festival and in 2005 at the Sydney Film Festival. In recent years, a private screening was held at the Yale University Department of Anthology in December 2006. The film enjoyed a two day showing at Alamo Draft House - Ritz in Austin, Texas (December 1 and 8, 2008). The film recently aired on Turner Classic Movies as part of their Friday night feature, TCM Underground, on January 31, 2009. It was also shown as one of the films in the outdoor Top Down film series run by the Northwest Film Center in Portland, Oregon on August 6, 2009.   Most recently, it was screened at the premiere of the Rocksploitation midnight movie series on July 10, 2010 at the Bridge Theater in San Francisco. 

=== Influence ===
Fans of the film include musician/actor Courtney Love,  musician/actor Jon Bon Jovi  (who dated Stains star Diane Lane in the 1980s),  Paul and Ariel Awesome of the punk fanzine Maximumrocknroll,  Beat the Geeks "Movie Geek" Marc Edward Heuck,  writer/podcaster Jake Fogelnest, {{Cite news| last = Rabin| first = Nathan| title = Jake Fogelnest wants everyone to watch Ladies And Gentlemen, The Fabulous Stains| work = The Dissolve: Compulsory Viewing| accessdate = 2014-08-01| date = 2013-08-20| url = http://thedissolve.com/features/compulsory-viewing/96-jake-fogelnest-in-on-a-quest-to-make-people-watch-/ Sam Green, IFC television show Split Screen. In 2008, Heuck recorded a historical commentary track for the Rhino DVD, which was not included on the finished DVD; it is available for free download. 

==References==
 

==External links==
*  
*  
*  
*  
*   at Internet Archive
*  
*  
*  

 
 
 
 
 
 
 
 
 
 
 
 
 