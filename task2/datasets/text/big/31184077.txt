Thacholi Ambu
{{Infobox film
| name           = Thacholi Ambu
| image          = 
| image size     = 
| alt            = 
| caption        = 
| director       = Navodaya Appachan
| producer       = Navodaya Appachan
| writer         = N. Govindan Kutty
| based on       = Vadakkan Pattukal
| starring       = Prem Nazir Sivaji Ganesan Jayan
| music          = K. Raghavan
| cinematography = U. Rajagopal
| editing        = T. R. Sekhar
| studio         = Navodaya Studio
| distributor    = 
| released       =  
| runtime        = 2 Hours 35 Minutes
| country        = India
| language       = Malayalam
| budget         = 
| gross          =
}} Ravikumar and Jayan. Directed and produced by Navodaya Appachan, it was the first cinemascope film in Malayalam.    Prem Nazir played the title role in the film.

==Cast==
* Prem Nazir as Thacholi Ambu,Othenans Nephew Thacholi Othenakkurup
* Jayan as Bappu and Kutty(Bappus Son)
* K. P. Ummer as Kathiroor Gurukkal
* Balan K. Nair as Mayin Kuttiyil,Bappus Father
* N. Govindan Kutty as Parunthunkalkotta Panicker,Othenans Brother-in-law
* M. N. Nambiar as Ittiri,Parunthunkalkotta Panickers Son
* G. K. Pillai (actor)|G. K. Pillai as Payyamvelli Chanthu,Othenans Friend Ravikumar as Bapputty,Bappus another Son
* K. R. Vijaya as Kunjitheyi,Othenans Wife
* Unnimary as Kanni,Othenans Daughter
* Vijaya Lalitha as 
*Alummoodan
* Ushakumari as
*Meenakumari as Ambus mother and Othenans Sister

==Soundtrack== 
The music was composed by K. Raghavan and lyrics was written by Yusufali Kechery. 
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;" 
|- bgcolor="#CCCCCF" align="center" 
| No. || Song || Singers ||Lyrics || Length (m:ss) 
|- 
| 1 || Anuraagakkalariyil || K. J. Yesudas || Yusufali Kechery || 
|- 
| 2 || Makaramaasa Pournamiyil || P Susheela, Chorus || Yusufali Kechery || 
|- 
| 3 || Naadapuram Palliyile || Vani Jairam || Yusufali Kechery || 
|- 
| 4 || Naanam Kunungikale || S Janaki, P Susheela || Yusufali Kechery || 
|- 
| 5 || Ponniyam Parunthunkal || P Susheela, Chorus || Yusufali Kechery || 
|- 
| 6 || Ponniyam Poonkanni || P Susheela || Yusufali Kechery || 
|- 
| 7 || Thacholi Veettile || P Susheela || Yusufali Kechery || 
|}

==References==
 

==External links==
*   at the Malayalam Movie Database
*  

 
 
 
 


 