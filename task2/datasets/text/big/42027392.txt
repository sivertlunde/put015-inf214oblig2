The Seven Swans
{{infobox film
| title          = The Seven Swans
| image          =
| imagesize      =
| caption        =
| director       = J. Searle Dawley
| producer       = Adolph Zukor
| writer         = Hans Christian Andersen(story) J. Searle Dawley(scenario)
| starring       = Marguerite Clark Richard Barthelmess
| music          =
| cinematography = H. Lyman Broening
| editing        =
| distributor    = Paramount Pictures
| released       = December 31, 1917
| runtime        =
| country        = USA
| language       = Silent film..(English intertitles)

}} lost  1917 silent film fantasy starring Marguerite Clark. Famous Players Film Company produced and J. Searle Dawley directed. 

==Plot==
Based on a story by Hans Christian Andersen, Clark stars as Princess Tweedledee who later falls in love with Prince Charming. An evil witch, yearning to take over a kingdom, turns the Princesss brothers into swans. Moon Fairies vow to turn her brothers back to humans if she knits them seven robes and not speak to another human for a specified amount of time.

==Cast==
*Marguerite Clark - Princess Tweedledee
*William E. Danforth - The King
*Augusta Anderson - The Wicked Queen
*Edwin Denison - The Lord High Chancellor
*Daisy Belmore - The Witch
*Richard Barthelmess - Prince Charming
*Richard Allen - Princess Tweedeldees Brother
*Jere Austin - Tweedeldees Brother
*Joseph Sterling - Tweedeldees Brother
*Frederick Merrick - Tweedeldees Brother
*Lee F. Daly - Tweedeldees Brother
*Stanley King - Tweedeldees Brother
*Gordon Dana - Tweedeldees Brother

==References==
 

==External links==
* 
* 

 
 
 
 
 
 
 

 
 