Seeing the World
 
{{Infobox film
| name           = Seeing the World
| image          =
| caption        =
| director       = Robert A. McGowan Robert F. McGowan
| producer       = F. Richard Jones Hal Roach
| writer         = Hal Roach H. M. Walker
| starring       = Joe Cobb Jackie Condon Jean Darling Johnny Downs Jay R. Smith Allen "Farina" Hoskins
| cinematography = Art Lloyd
| editing        = Richard C. Currier
| studio         = Hal Roach Studios
| distributor    = Pathé|Pathé Exchange
| released       =  
| runtime        = 20 minutes
| country        = United States
| language       = Silent film English intertitles
| budget         =
}}

Seeing the World, also known as A Roamin Holiday, is a 1927 silent Our Gang film featuring Stan Laurel, directed by Robert F. McGowan and Robert A. McGowan.       It was the 57th Our Gang short subject released.

==Plot== James Finlayson) is trying to win a trip to Europe. He does win, but the gang accompanies him as well, which causes his trip to become a nightmare. The group treks through Venice, Rome, Pompeii, Naples, and London. Finally, the entourage ends up in Paris, where Farina manages to falls off the side of the Eiffel Tower. Finlayson tries desperately tries to rescue Farina, leading him to wake up from what was apparently a daydream caused by the gang tossing sleeping pills into his water.

==Cast==
===The Gang===
* Joe Cobb as Joe
* Jackie Condon as Jackie
* Johnny Downs as Johnny
* Allen Hoskins as Farina
* Scooter Lowry as Skooter
* Jay R. Smith as Jay
* Peggy Eames as Peggy

===Additional cast=== James Finlayson as James Finlayson, The schoolteacher
* Jean Darling as Extra at pier
* Ed Brandenburg as Window washer
* Frank Butler as English pedestrian
* Dorothy Darling as Extra at pier Charlie Hall as English chauffeur
* Ham Kinsey as Ships official
* Stan Laurel as English pedestrian
* Charles McMurphy as Ships official
* Charley Young as Extra at pier
* Edward VIII|David, Prince of Wales as Himself (stock footage)
* Gaston Doumergue as Himself (stock footage)
* Yusef of Morocco as Himself (stock footage)

==See also==
* Our Gang filmography
* Stan Laurel filmography

==References==
 

==External links==
* 

 
 
 
 
 
 
 
 
 
 
 
 