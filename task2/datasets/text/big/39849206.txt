Ramanujan (film)
{{Infobox film
| name           = Ramanujan
| image          = Ramanujan film.jpg
| alt            =
| caption        =
| director       = Gnana Rajasekaran
| producer       = Srivatsan Nadathur Sushant Desai Sharanyan Nadathur Sindhu Rajasekaran  	
| writer         = Gnana Rajasekaran Abbas Suhasini Maniratnam Kevin McGowan Bhama Michael Lieber
| music          = Ramesh Vinayakam
| cinematography = Sunny Joseph
| editing        = B. Lenin
| released       =  
| runtime        =
| country        = India
| language       = Tamil
}} back to Tamil and English languages. South Indian British film, Tamil film actors Gemini Ganesan and Savitri (actress)|Savitri, as the main protagonist.

== Plot ==
Set in the early 1900s, the film traces the life of the prodigious math genius Srinivasa Ramanujan from the time he was a young Tamil Brahmin to his years in England, where he attended Cambridge University during World War I. The film follows his relationships with his mother Komalatammal, his wife Janaki, and his collaborator Professor G. H. Hardy. The film will also showcase how Indian society viewed a mathematician of such great stature.   

== Cast ==
* Abhinay Vaddi as Srinivasa Ramanujan 
* Suhasini Maniratnam as Komalatammal ( Srinivasa Ramanujans Mother  )
* Bhama as Janaki ( Srinivasa Ramanujans Wife  )
* Kevin McGowan as G. H. Hardy Abbas as Prasanta Chandra Mahalanobis
* Anmol as Young Srinivasa Ramanujan
* Michael Lieber as John Edensor Littlewood Richard Walsh as Francis Spring
* Sharath Babu as Diwan Bahadur R. Ramachandra Rao I. C. S.
* Radha Ravi as Prof. Singaravelu Mudaliar
* Madhan Bob as Prof. Krishna Shastri
* Y. G. Mahendran as S. Narayana Iyer
* Manobala as Krishna Rao
* Nizhalgal Ravi as Srinivasa Raghavan
* Satish Kumar as Anandhu
* Thalaivasal Vijay as Sathiyapriya Rayar
* Manibharathi as Krishnan
* Delhi Ganesh Raja Krishnamoorthy as Seshu Iyer
* T. P. Gajendran as T. Namberumal Chetty
* Mohan V. Ram
* Cloudia Swann as Ms. Bourne
* Mike Parish as Doctor Charles
* Harsh Naik as Chatterjee
* Lizzie Bourne as Ms. Gertrude Hardy

==Crew==
* Music  Ramesh Vinayakam
* Production Designer: P. Krishnamurthy
* Costume Designer: Sakunthala Rajasekaran
*Sound Designer: Laxmi Narayan
* Assistant Scriptwriters: Roxane de Rouen & Sindhu Rajasekaran
* Production Managers: V. Vishwanathan & K.S. Venkatesh Vaali

==Production==

=== Casting ===
{{quote box
|quote="We had very few photos of Ramanujan and his family. So, we had to purely depend on whatever information was available on the social setup of Iyengars and other castes of that period. We had to design costumes keeping in mind the three phases of his life and those surrounding him — the Kumbakonam phase, his life in Chennai and then, in London,"
|source= — Shakuntala, on designing the looks for Ramanujan  
|align=right
|width=50%
}} Abbas and Richard Walsh Malayalam works with directors Adoor Gopalakrishnan and Shaji N. Karun, was signed as cinematographer.

=== Filming ===
 
The film has been shot in the five main locations of Ramanujans life, Kumbakonam, Namakkal, Chennai, London and Cambridge.   The first two schedules were shot in India while the third was done in England, where they took the permission of Cambridge University to shoot.  The task to create a script in multiple languages was described by Sindhu Rajasekaran as "quite a chore. Roxane de Rouen and I are working with Gnana Rajasekaran, the director of ‘Ramanujan,’ to make the characters speak words that make them real. No, you wouldn’t find roadside urchins who speak in Queen’s English here; people would speak what comes to them naturally: Tamil, English, Indian English, even Tamenglish. What a delight it is to live in this world where languages are not borders, but an element to experiment with…" 

==Music==
{{quote box
|quote="My music tries to delve into the unfathomable mysterious and mystical mind of Ramanujan in its own world, which invariably found bliss in the triumph of originating newer and newer theorems. The music represents the tribulations of Ramanujans life torn between hope and despair, and the loneliness that came along with the genius".
|source= — Vinayakam, on composing the music for Ramanujan  
|align=right
|width=50%
}}
 pitch (Shruti (music)|shruthi) was less than what she usually sings in but came to know that, in the days of Ramanujan, people only sang in that pitch. 

The soundtrack album of Ramanujan was released at the Suryan FM radio station in Chennai on June 13, 2014.  The album, which features eight tracks, including four songs and four instrumentals, was praised by critics. Indiaglitz in its review wrote, "Ramesh Vinayakam was always there and thereabouts with his wonderful albums in the past. With this album he has struck the right chords towards his name once again. This must be the break hes been looking for years". musicaloud.com gave it a score of 9 out of 10 and wrote, "Ramesh Vinayakam expertly draws from Carnatic and Western classical styles to produce one of the finest period film soundtracks ever".  Behindwoods.com gave it 3 stars out of 5 and wrote, "Ramanujan enthralls the listener by transporting them to a bygone era of classical music".  Ramanujan was also named by Deccan Music  and Milliblog  as the best Tamil music album of the year 2014. 

{{Track listing extra_column = Singer(s) music_credits = no lyrics_credits = yes

|title1= Narayana
|extra1= Karthik Suresh, Vani Jayaram Vaali
|length1= 4:33
|title2= Thuli Thilyay
|extra2= Ramesh Vinayakam, Vinaya
|lyrics2= Na. Muthukumar
|length2= 4:58
|title3= Vinkadantha
|extra3= Unnikrishnan
|lyrics3= Thirumalisai Alvar
|length3= 3:38
|title4= Thuli Thilyay
|extra4= Ramesh Vinayakam, Kaushiki Chakrabarty
|lyrics4= Na. Muthukumar
|length4= 4:58
|title5= Mystic Mind
|extra5= Instrumental
|lyrics5= —
|length5= 3:00
|title6= Ramanujan (Theme)
|extra6= Instrumental
|lyrics6= —
|length6= 3:16
|title7= English Notes
|extra7= Instrumental
|lyrics7= —
|length7= 6:58
|title8= One To Zero
|extra8= Instrumental
|lyrics8= —
|length8= 3:16
}}

==Reception==
The film received mixed reviews from critics, who generally praised the acting but criticized the writing. The Deccan Chronicle called Ramanujan "a brilliant piece on canvas with edifying moments and relevance to modern age" and went on to add that it was "not to be missed", giving it 3.5/5 stars.  S. Saraswathi of Rediff wrote, "Ramanujan is a brilliant film, a must watch" and gave the film 4/5.  Gautaman Bhaskaran of The Hindustan Times gave the film 3/5 stars and wrote, "The movie is a poignant look at the way a prodigy struggled and suffered in a penurious family, a mastermind whose mathematical wizardry invited ridicule and revulsion in far lesser mortals. Rajasekaran, who also scripted the film, takes us through a linear narrative to tell us about the intelligence of boy Ramanujan as he completely foxes his school-master with a little insight into the importance of zero, and later about his frustration when he hits a wall in his quest to sink into, and shine, with numbers".  Sify wrote, "Making biopics is indeed very challenging and the director has been successful to a very large extent to bring out each and every character. Gnana Rajasekaran has done a well-researched biopic on Ramanujam...it is a film that is definitely worth viewing".   Indo-Asian News Service|IANS gave it 3/5 and wrote, "Gnana Rajasekaran certainly knows the art and succeeds narrating an inspiring tale, but his work doesn’t resonate deep within. This is so because the director merely recreates several important episodes from Ramanujan’s life on the screen while ignoring the need to build a screenplay to keep the viewers hooked".  Bharath Vijayakumar of Moviecrow rated 3/5 stars and said,  "Ramanujan is a noble effort and a fascinating insight about the life and times of this Maths Wizard who lived all his life in unison with his true love.". 

In contrast, The New Indian Express wrote, "The director’s persistent effort to bring on celluloid lives of eminent people is laudable. But a movie is not only about the theme, but also about how it is presented on screen. And the presentation of the life and journey of the mathematical genius is disappointing and uninspiring".  Baradwaj Rangan of The Hindu wrote, "The film runs nearly three hours and it’s puzzling why it needed to. There appears to have been no effort to streamline the events of Ramanujan’s life. The writing, too, fails to make Ramanujan interesting", going on to add "The great man certainly deserved a better movie".  S. Rajasekar from Cinemalead wrote "On the whole, if you want to know about Ramanujan, go and see this Gnana Rajasekaran’s Ramanujan but if you really want to have a perfect biopic watching experience, I would say the director has missed few things in making Ramanujan as a cult classic." giving it 3/5.   Behindwoods gave it 2.5/5 stars and wrote, "Rajasekaran should be credited for showcasing the life, tribulations and accomplishments of an unsung Math genius on the silver screen thereby making the legend be known to a wider audience", but called it "A sincerely made biopic, which falls short of inspiring".  M. Suganth of the Times Of India gave 2.5/5 and wrote, "For a film that is about a man with astounding talent, the filmmaking is largely unimaginative. The staging is somewhat old-fashioned (read dated), the pacing staid and the film often slips into the kind of melodrama that you nowadays find in TV serials.".  Daily India gave 2.5/5 and stated, "In overall, Ramanujan is a cleanly made and its a very rare kind of movie in biopics genre. Editing and cinematography is worth praising. Abhinay Vaddi, the grandson of veteran Tamil film actor Gemini Ganesan done a good job and made justice to their roles.".  Indiaglitz gave 2.25/5 and wrote, "An honest attempt to drive through the life of a genius gets stuck at lot of bumpers.". 

== References ==
 

==External links==
*  

 
 
 
 
 
 
 