Vallavan
 
 
{{Infobox film
| name = Vallavan
| image = Vallavan cover art.jpg
| caption = Release poster
| director = Silambarasan
| writer = Silambarasan Balakumaran
| starring = Silambarasan Nayantara Reema Sen
| producer = P. L. Thenappan
| music  = Yuvan Shankar Raja
| cinematography = Priyan   S.Murthy Anthony
| studio = Sri Raj Lakshmi Films
| released =  
| runtime = 185 minutes Tamil
| country = India
| gross = 
}} 2006 Tamil Sandhya whilst Sathyan and The Notebook Telugu and released as Vallabha.

==Plot==
Vallavan (Silambarasan) is a happy-go-lucky college student with a set of friends. Bala (Santhanam (actor)|Santhanam) & Suji (Sandhya (actress)|Sandhya) are two of Vallavans friends. When theyre at the temple, he sees Swapna (Nayantara). He immediately falls in love with her.. While Vallavan’s at the college, Swapna shows up. Shes revealed to be a teacher, who’s three years older than Vallavan. He decides to woo her by turning into Pallavan, an ugly duckling with buck teeth and glasses. Pallavan makes Swapna fall in love with him for his heart, even though he is ugly. The pair end up having sex. Vallavan is unable to focus on anything because he is having sex with Swapna. But when Swapna comes to know that Pallavan is younger than her (from one of Vallavans enemies in school) and that too a student, she dumps him and decides to marry another guy. Meanwhile a sequence of events take place, much to the chagrin of Vallavan who is highly insulted by them. As Vallavan walks in the streets wondering how things could get so messy, he recalls his school life.

When he was in higher secondary school, Vallavan met Geetha (Reema Sen). A hysteric Geetha ensures that Vallavan is crazy for her. Coming to know of her true nature, he ends the affair with her. But she is not ready to let him go so easily, so he resolves to teach her a lesson. Geetha returns for payback. She tells him that she was the spoilsport for the current mess in his life. In a climax displaying her disorder, she kidnaps Suchitra and Vallavan arrives and frees her. The film fast-forwards to 3 years later. Geetha is released from a mental asylum. The moment she steps out, she exhibits her fiery psychotic expression. Vallavan is waiting for her and they communicate telepathically while Vallavan says that he will destroy her.

==Cast==
 
*Silambarasan as Vallavan/Pallavan
*Nayantara as Swapna
*Reema Sen as Geetha Sandhya as Suchitra Santhanam as Bala
*S. V. Shekhar as Suchitra and Balas father
*Premji Amaren as Swapnas friend Sathyan as Vallavans friend
*Balakumaran - (Cameo appearance)
*T. Rajendar - (Special appearance)
*Kanal Kannan - (Cameo appearance)
 

==Soundtrack==
{{Infobox album
| Name = Vallavan
| Type = soundtrack
| Artist = Yuvan Shankar Raja
| Cover = Vallavan ~ Front.png
| Released = 1 June 2006
| Recorded = 2005 - 2006 Feature film soundtrack
| Length = 41:45 22:40 (2nd release)
| Label = Ayngaran Music An Ak Audio Hit Music
| Producer = Yuvan Shankar Raja
| Reviews =
| Last album  = Azhagai Irukkirai Bayamai Irukkirathu (2006)
| This album  = Vallavan (2006)
| Next album  = Kedi (2006 film)|Kedi (2006)
}}

The music was scored by noted musician and Silambarasans friend, Yuvan Shankar Raja, coming together again after churning out the successful album of Manmadhan (film)|Manmadhan (2004). The soundtrack was released on 1 June 2006 and features 9 tracks, including 7 songs and two Instrumental pieces. As it was the case in Manmadhan, a second Soundtrack was released afterwards with bit songs that feature in the film but not on the first soundtrack along with pieces from the film score. The lyrics were penned by Vaali (poet)|Kavignar Vaali, Thamarai, film director Perarasu and Silambarasan himself. Karunakaran wrote the lyrics of the bit song "Kadhal Vandhale", released in the second edition.

Yuvan Shankar Raja won accolades for his soundtrack, whereas especially the songs "Loosu Penne" and "Yammaadi Aathaadi" were huge hits, topping the charts for the following weeks, with the latter even becoming the anthem of the season.   His film score, too, was lauded, called as the films highlight and "backbone of the film", while he himself was hailed as the films "real hero". 

{| class="wikitable"
|-  style="background:#ccc; text-align:center;"
! Track !! Song !! Singer(s)!! Duration (min:sec) !! Lyricist!! Notes
|- 1 || "Vallava Ennai" ||   ||
|- 2 || "Hooray Hooray Hip" ||   ||
|- 3 || "Kadhal Vanthirichu" ||  
|- 4 || "Loosu Penne" || Silambarasan & Blaaze || 6:52 || Silambarasan ||
|- 5 || "Yammaadi Aathaadi" ||   ||
|- 6 || "Podu Attam Podu" || Vijay Yesudas || 5:38 || Vaali ||
|- 7 || "He Knows What To Do (Vallavan Theme)" || Tanvi Shah (Humming) || 2:00 || - ||
|- 8 || "Success of Love" || Instrumental || 1:05 || - ||
|- 9 || "Loosu Penne (Club Mix)" || Silambarasan & Premji Amaren || 4:02 || Silambarasan ||
|}

{{tracklist
| headline     = Bonus tracks (Second release)
| extra_column = Singer(s)
| all_music    = Yuvan Shankar Raja
| total_length = 22:40
| title10       = Yammaadi Aathaadi (2nd Version)
| extra10       = Vijaya T. Rajendar, Silambarasan, Suchitra & Mahathi
| length10      = 5:24
| title11       = Hip Hip Hurrey (2nd Version) Ranjith & Karthik
| length11      = 5:38
| title12       = Theme Music (1)
| extra12       = Instrumental
| length12      = 1:38
| title13       = Theme Music (2)
| extra13       = Instrumental
| length13      = 0:47
| title14       = Loose Penne (Music)
| extra14       = Instrumental
| length14      = 0:50
| title15       = Kadhal Vandhale
| note15        = Lyrics written by Karunakaran
| extra15       = Yuvan Shankar Raja
| length15      = 2:26
| title16       = Folk Bit
| extra16       = Instrumental
| length16      = 0:35
| title17       = Victory of Love
| extra17       = Silambarasan
| length17      = 0:56
| title18       = Valla Valla Vallavan
| extra18       = Blaaze
| length18      = 1:10
| title19       = Kadhal Vandhale (Music)
| extra19       = Instrumental
| length19      = 1:31
| title20       = Ilamai Idho + Pothuvaga (Remix)
| extra20       = Silambarasan
| length20      = 1:45
}}

==Release==

===Critical reception===
 , though the maker has lent it his trademark frills". 

Sify gave 2.5/5 stars to Vallavan and said:
"If you are looking for some wholesome entertainment, then Vallavan is worth your time and money". 

Behindwoods.com commented: "On a positive note, the glamorous quotient of three heroines along with music and the youth element can play to Vallavan’s advantage". 

Oneindia wrote: "Yuvans music and Santhanams comedy provide the much-needed relief as the films dark and tension filled screenplay roils throughout with murky games of passion and revenge." 

===Box office===
Vallavan received mixed reviews from all sources. The film also featured, prior to release, in "most awaited" list from entertainment sites. The film got a grand release for Diwali 2006 along with other films like Ajith Kumar|Ajiths Varalaru, Jiivas E (film)|E, Arya (actor)|Aryas Vattaram and Sarath Kumars Thalaimagan. Due to the pre-release hype it created, Vallavan received a very good opening, but collections started declining and the film ended up as an average grosser.

===Controversies===
While filming for Vallavan, Nayantara was romantically linked with the films director and co-actor Silambarasan. She initially denied the reports. In November 2006, however, she confirmed that she and Silambarasan had broken up, going on to add that she will not work with him again.  intimate photos of him and Nayantara on the internet.

==References==
 

==External links==
*  

 
 
 
 
 
 