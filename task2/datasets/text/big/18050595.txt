We Live Again
{{Infobox film
| name           = We Live Again
| image          = We Live Again poster.jpg
| image_size     =
| caption        = Italian film poster
| director       = Rouben Mamoulian
| producer       = Samuel Goldwyn
| writer         = Novel:   Screenplay:   Talbot Jennings Willard Mack Edgar G. Ulmer Thornton Wilder
| narrator       =
| starring       = Anna Sten Fredric March Alfred Newman
| cinematography = Gregg Toland
| editing        = Otho Lovering
| studio         = Samuel Goldwyn Productions
| distributor    = United Artists
| released       = November 1, 1934
| runtime        = 85 minutes
| country        = United States
| language       = English
| budget         =
| gross          =
}}
We Live Again (1934) is a film adaptation of Leo Tolstoys 1899 novel Resurrection (novel)|Resurrection (Voskraeseniye), starring Anna Sten and Fredric March.  Directed by Rouben Mamoulian, the screenplay was written by Maxwell Anderson with contributions from numerous writers, including Preston Sturges and Thornton Wilder.

Producer   have been made since then.

==Plot==
Russian Prince Dmitri Nekhlyudov (Fredric March) seduces innocent young Katusha Maslova (Anna Sten), a servant to his aunts.  After they spend the night together in the greenhouse, Dmitri leaves the next morning, outraging Katusha by not leaving a note for her, only money. When she becomes pregnant, she is fired, and when the baby is born, it dies and is buried unbaptized.  Katusha then goes to Moscow, where she falls into a life of prostitution, poverty and degradation.

Dmitri, now engaged to Missy (Jane Baxter), the daughter of the wealthy judge, Prince Kortchagin (C. Aubrey Smith), is called for jury duty in Kotchagins court for a murder trial. The case is about a merchant who has been killed, and Dmitri is astonished to see that Katusha is one of the defendants. The jury finds that she is guilty of "giving the powder to the merchant Smerkov without intent to rob", but because they neglected to say without intent to kill, even though the jury intended to free her, the judge sentences her to five years hard labor in Siberia.

Feeling guilty about abandoning Katusha years before, and wanting to redeem her and himself as well, the once-callous nobleman attempts to get her released from prison.  He fails in his efforts, so he returns to the prison to ask Katusha to marry him.  When he doesnt show up on the day the prisoners are to be transported, Katusha gives up hope, but then he appears on the border of Siberia where the prisoners are being processed: he has divided his land among his servants and wants to "live again" with her forgiveness, help and love.   

==Cast==
*Anna Sten as Katusha Maslova
*Fredric March as Prince Dmitri Nekhlyudov
*Jane Baxter as Missy Kortchagin
*C. Aubrey Smith as Prince Kortchagin
*Sam Jaffe as Gregory Simonson
*Ethel Griffies as Aunt Marie
*Gwendolyn Logan as Aunt Sophia
*Jessie Ralph as Matrona Pavlovna
*Leonid Kinskey as Simon Kartinkin Dale Fuller as Botchkova
*Morgan Wallace as The Colonel
*Crauford Kent as Schonbock
*Fritzi Ridgeway as The Redhead

Cast notes:
*Samuel Goldwyn had introduced Anna Sten, who he hoped would become the "new Greta Garbo|Garbo", earlier in 1934 in the film Nana (1934 film)|Nana, then showcased her in this film, and tried again in 1935 with The Wedding Night.  None of the three films was a box office success, and Goldwyn released "The Passionate Peasant" from her contract.
*This was the first Hollywood film for English actress Jane Baxter. TCM   

==Production==
Unlike many films made in the 1930s, We Live Again, which had the working title of "Resurrection", met with the approval of the censors at the Hays Office.  Joseph Breen wrote to Will H. Hays: "Though dealing with a sex affair and its attendant consequences, the story has been handled with such fine emphasis on the moral values of repentance and retribution, as to emerge with a definite spiritual quality. We feel that this picture could, in fact, serve as a model for the proper treatment of the element of illicit sex in pictures." 

The film was in production from 12 June to 2 August 1934.  The New York opening took place during the week of 1 November of that year, with the general American release on 16 November. 
 John Boles in 1931. The story has not been made into a theatrical film version in English since We Live Again.

==Reception==
The film was a box office disappointment. THE YEAR IN HOLLYWOOD: 1984 May Be Remembered as the Beginning of the Sweetness-and-Light Era
By DOUGLAS W. CHURCHILL.HOLLYWOOD.. New York Times (1923-Current file)   30 Dec 1934: X5.  
==References==
 

==External links==
*  
*  
*  
*  

 
 
 
 

 
 
 
 
 
 
 
 
 
 
 