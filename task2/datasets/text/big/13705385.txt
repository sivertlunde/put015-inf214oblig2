Absolument fabuleux
 
{{Infobox film
| name           = Absolument fabuleux
| caption        = 
| image	         = Absolument fabuleux FilmPoster.jpeg
| director       = Gabriel Aghion
| producer       = Pascal Houzelot Pascal Houzelot
| writer         = Jennifer Saunders Dawn French Gabriel Aghion François-Olivier Rousseau Rémi Waterhouse Pierre Palmade
| screenplay     = 
| story          = 
| based on       =  
| starring       = Josiane Balasko Nathalie Baye Marie Gillain Vincent Elbaz
| music          = Nicolas Neidhardt
| cinematography = François Catonné
| editing        = Maryline Monthieux
| studio         = 
| distributor    = 
| released       =  
| runtime        = 105 minutes
| country        = France
| language       = French
| budget         = $11,220,000
| gross          = $8,222,364 
}}

Absolument fabuleux is a French comedy film, released in 2001. Written and directed by Gabriel Aghion, the film was an adaptation of the British television comedy series Absolutely Fabulous. 

Aghions stated reason for making the film was to increase awareness of the series in France, where it was not widely known. For the screenplay, Aghion simply translated a number of scenes from the original series, and tied them together into a coherent screenplay.
 Lulu in the original series, French singer Chantal Goya appeared as herself. Jennifer Saunders also had a cameo appearance in the film as a spectator sitting next to Catherine Deneuve at a fashion show.

The Patsy role in the film was originally offered to Amanda Lear, who declined by saying that shed "already lived it".

The film performed poorly at the French box office and was panned by most French critics,  who argued that it failed to translate the typically British humour of the original TV series.

==Plot==
Patsy, who has a strong penchant for alcohol, especially for a famous brand of champagne, and Eddie, obsessed with her curves and perpetually electrified by cocaine it soothes by countless different sleeping pills, have once been set May 68 but already on the barricades they were at a place that was not really theirs. Today, both reached fifty, rabid feminists continue to say no to anything, they say, degrades the weaker sex: cooking, living alongside a man raising a child. But feminism May 68 and the "seventies" is no longer for them a vague nostalgia that is lost in their nebulous mind. Eddie ended up getting married and became the mother of a beautiful girl Bachelor ... then divorced but still lives off her ex-husband under the same roof, the only client of his last living "public relations," being a perpetually Chantal Goya tour ass. Patsy, she preferred to remain single and independent but spent his life living muse to countless men hook succeeding one after the other over the years, and both, real "fashion victims", today totally dissolute life completely "crazy" resolutely eccentric, to the discouragement of the family of Eddie. The life of Eddie and Patsy is only champagne at any time of day or night, invaded atmosphere of tobacco smoke joints, cocaine, luxury toilets ... and parasitism by attending fashion stars and the show. Eddie would lead Safrane, his daughter, in the tornado of his "values" in light years of work and family life. But this is still a virgin at 17 and prepares the Polytechnic entrance exam. The husband, played by Yves Renier, no longer supports permanent pranks, alcoholism, grotesque and pathological irresponsibility of his ex-wife who also literally invades the house.

==Cast==
* Josiane Balasko as Edith "Eddie" Mousson
* Nathalie Baye as Patricia "Patsy" Laroche
* Marie Gillain as Safrane Vaudoye
* Vincent Elbaz as Jonathan
* Claude Gensac as Mamie Mousson
* Yves Rénier as Alain Vaudoye
* Saïd Taghmaoui as Manu
* Armelle as Cerise
* Tomer Sisley as Kevin

Cameo :
* Chantal Goya as Herself
* Stéphane Bern as Himself
* Christophe Robin as Himself
* Jean-Paul Gaultier as Himself
* Catherine Deneuve as Herself
* Jennifer Saunders as Herself
* Brigitte Fontaine as Herself
* Estelle Lefébure as Herself
* Claire Chazal as Herself

==References==
 

==External links==
* 

 

 
 
 
 
 
 


 
 