Rock: Wanko no Shima
{{Infobox film
| name           = Rock ~Wanko no Shima~
| image          = Rock ~Wanko no Shima~ movie poster.jpg
| caption        = Film Poster
| director       = Isamu Nakae
| producer       = 
| writer         = 
| screenplay     = 
| story          = 
| based on       =  
| starring       = Ryuta Sato, Kumiko Aso
| music          = 
| cinematography = 
| editing        = 
| studio         = Fuji TV
| distributor    = Toho
| released       =  
| runtime        = 
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}

  is a 2011 Japanese film directed by Isamu Nakae and is scheduled to be released in Japanese cinemas on 23 July 2011. It is based on a true story from the small island of Miyakejima.   

==Cast==
* Ryuta Sato 
* Kumiko Aso
* Yoshinori Okada
* Shuuji Kashiwabara
* Koki Sahara
* Taiki Nakahayashi
* Jiro Sato
* Ken Mitsuishi
* Hitomi Sato
* Okayamahajime
* Shisho Nakamaru
* Mieko Harada
* Mitsuko Baisho
* Karen Miyama

==Filming==

===Post Production===
This films official trailer was first uploaded to this films official page on April 16, 2011.   

==References==
 

==External links==
*    
*  

 
 

 