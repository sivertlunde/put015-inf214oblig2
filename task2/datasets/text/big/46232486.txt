Fixed (film)
{{Infobox film
| name           = Fixed
| image          = Amara_Harnisch_in_Fixed_(film)_03.jpg
| alt            = 
| caption        = Amara Harnisch in Fixed
| directors       = Codey Wilson and Burleigh Smith
| producer       = Rob Viney Burleigh Smith
| writer         = 
| starring       = Amara Harnisch Paul Treasure Alitia Harnisch
| cinematography = Michael Titter
| editing        = Ross Farnsworth
| studio         = Wavebreaker SAE Institute
| distributor    = Wavebreaker Tropfest
| released       =  
| runtime        = 7 minutes
| country        = Australia
| language       = English
}}
Fixed is a 2014 short comedy film directed by Codey Wilson and Burleigh Smith.  It features Amara Harnisch as Jemimah, a little girl who wants her dog to have puppies.  The film premiered at Tropfest, the worlds largest short film festival.  

==Plot==
Jemimah is five years old and desperate for her dog, Tilly, to have puppies.  When she learns her parents plan to have Tilly desexed, Jemimah embarks on a quest to get Tilly pregnant.

==Production==
The film started as a student production at SAE Institute, Perth, Western Australia. 

Several scenes in the screenplay did not make the final cut, including one scene where Jemimah dresses Tilly in her older sisters fishnet stockings and bra and tries to prostitute her on a street corner. 

==Festival Screenings==
Fixed premiered at Tropfest 2014, where it was the only Western Australian finalist. 

The film screened at the 46th Annual Nashville Film Festival and the 48th Annual Humboldt Film Festival in April, 2015. 
 Cayman Islands Davis Feminist The European Riverside International Sedona International Film Festival, the SouthSide Film Festival (Pennsylvania) and the World Music & Independent Film Festival (Virginia).

The film was awarded Best Comedy at the Poppy Jasper Film Festival and Audience Favourite at the Pacific Coast International Short Film Festival 2015.

==Reception==
Reviewing the film at the Speechless Film Festival, Minnesota, Judge Donna Casella wrote "How could you not love this story? This little girl has such depth to her performance. That face speaks volumes." 

"Ostensibly a light and fluffy story," wrote Douglas Sutherland-Bruce, "There is an underlying theme of personal freedom and choice subtly explored. Very highly recommended." 

==References==
 

==External links==
*  
*  production company website
*  creative media school
*  the worlds largest short film festival
*  at Tropfests YouTube Channel

 
 