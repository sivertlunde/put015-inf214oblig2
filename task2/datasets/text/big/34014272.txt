Nerd Wars!
{{Infobox film
| name           = Nerd Wars!
| image          =
| alt            = 
| caption        = Nerd Wars! Release Poster
| director       = Willie Peña
| producer       = SAG New Media
| writer         = Willie Peña
| starring       = Michael M. Peña, Nathaniel A. Peña, Alfonso Freeman, Lia Marie Johnson
| music          = 
| cinematography = 
| editing        = Willie Peña
| studio         = 
| distributor    = 
| released       =  
| runtime        = 
| country        = United States
| language       = English
| budget         = 
| gross          = 
}}
Nerd Wars! is a 2011 American short film written, directed and edited by Willie Peña and starring Michael M. Peña, Nathaniel A. Peña, Alfonso Freeman, and Lia Marie Johnson. The comedy tells the story of the rivalry between class nerd Alfonso, played by Michael M. Peña, and Marty Baboor, a newly arrived "super-nerd" student from India played by Nathaniel A. Peña.
This short film, which was shot in High Definition Video, was produced under a SAG New Media Contract with a budget of less than 10,000 dollars.  It has an anti-bullying theme.

==Plot==
Alfonso is the resident sixth-grade nerd who is quite happy with his status as an intellectual genius. His day is ruined, however, when a new kid arrives to class…Marty, the super-nerd from India.    Marty takes no prisoners, and Alfonso quickly finds himself outmatched by the new kid. The rivalry continues in gym class with a brutal game of dodge ball and finally ends up in an epic after-school battle. Romantic interest is provided by the militant Russian nerd Ivanka, played by Danika Yarosh,  and Doris, played by Lia Marie Johnson, who firmly believes she is a witch with magic powers.   

==Cast==
* Michael Manuel Peña as Alfonso
* Nathaniel A. Pena as Marty Baboor
* Alfonso Freeman as Mr. Chen
* Lia Marie Johnson as Doris
* Everhet Kokason as Boris
* Danika Yarosh as Ivanka
* Milton Greenberg as Angel
* Jemal Draco as Raheem
* Kent A. Gallegos as Coach
* Deborah Vieths as Cafeteria Lady
* Allie Costa as Natalie Grace
* Jessica Gwennap as Sally
* Marisa Imbroane as Suzie
* Chad Pawlak as Ryan
* Ryan Snyder as Chad
* Devin Knight as Janitor
* Keira Peña as Lily
* Sancho Martin as B-Boy Alfonso
* Jason Sensation Thomas as B-Boy Marty
* Willie Peña as Dad (cameo)

==Production==
Willie Peña wrote the film after watching his two sons, who play the leads, pretend to be nerds engaged in a fistfight while on a photo shoot. Principal photography took place on four Saturdays in May, 2011. Editing took approximately five months, as everything from sound to visual effects was handled by the first-time director who had no prior experience and had to teach himself the software and art of video editing. 

==Screenings and reception==
Nerd Wars! was accepted as part of the WorldkidsFoundation Lessons in the Dark film screenings for schoolchildren in Delhi and Mumbai, India and will be part of the organizations monthly showings at the National Center for the Performing Arts in Mumbai in 2012. 

==References==
 

==External links==
*  
*  
*  

 
 
 
 