City of Play
City of Play is a 1929 British drama film directed by Denison Clift and starring Chili Bouchier, Patrick Aherne and Lawson Butt.  It was made by Gainsborough Pictures and produced by Michael Balcon. It was made partly in sound film|sound. 

==Cast==
* Chili Bouchier - Ariel
* Patrick Aherne - Richard von Rolf
* Lawson Butt - Tambourini
* James Carew - Gen. von Rolf
* Harold Huth - Arezzi
* Andrews Engelmann - Colonel von Lessing
* Leila Dresner - Zelah
* Olaf Hytten - Schulz

==References==
 

==Bibliography==
* Low, Rachel. The History of British Film: Volume IV, 1918–1929. Routledge, 1997.

==External links==
* 

 

 
 
 
 
 
 


 