Aas Paas
{{Infobox film
| name           =  Aas Paas
| image          = Aas Paas.jpg
| caption        = Promotional Poster
| director       = J. Om Prakash
| producer       = Jagdish Kumar
| writer         =
| starring       =  Dharmendra Hema Malini
| music          = Laxmikant-Pyarelal
| cinematography =
| editing        =
| distributor    = Shemaroo Video Pvt. Ltd
| released       =  16 January 1981
| runtime        =
| country        = India
| language       = Hindi
| Budget         =
| preceded by    =
| followed by    =
| awards         =
| Gross          =
}} 1981 Bollywood romantic film directed by J. Om Prakash and starring Dharmendra, Hema Malini and Prem Chopra. The music was by Laxmikant-Pyarelal.  The film begins with a dedication to the late singer Mohammed Rafi, announcing that his last recorded song was for this film. 

Director-producer J. Om Prakash included in Aas Paas (1981) yet another brief "Lucky Mascot"  screen appearance of his  grandson -- future Hindi film superstar actor Hrithik Roshan, but then aged only 7 -- as the (uncredited) boy dancing in the song "Shehar Main Charchi Hai" who winks at Hema Malini and passes her a love note from Dharmendra.

==Plot==
Arun meets Seema by accident and both are attracted to each other. But Seema has a questionable background, which do create doubts in the minds of Arun and his mother. Arun is then involved in an accident, and everyone believes he is dead. Seema is devastated, and takes to alcohol, singing and dancing in a bar, and is raped by a man. Then Arun returns, and Seema is overjoyed to see him, but is hesitant to tell him about what had occurred. Arun takes Seema to his home, and introduces them to his family, namely his mom, his sister, Priti, and his brother-in-law, Prem. Seema is stunned and shocked when she sees Prem, for she recognizes him as the man who had raped her. Now Seema must decide to keep this terrible secret to herself and get married to Arun, or simply disappear from his life altogether

==Cast==
*Dharmendra as  Arun Choudhury
*Hema Malini as  Seema
*Prem Chopra as Prem
*Nadira
*Om Prakash as Seemas Father
*Nirupa Roy as  Aruns mom
*Aruna Irani as Rama
*G. Asrani as Jaikishen
*Rajendra Nath as  Seth Ramiklal
*Indrani Mukherjee as  Priti
*Hrithik Roshan as (uncredited) boy dancing in song "Shehar Main Charchi Hai"

==Soundtrack==
{| border="10" cellpadding="10" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer(s)
|-
| 1
| "Dariya Mein Phenk Do Chabi"
| Kishore Kumar, Lata Mangeshkar
|-
| 2
| "Shaher Mein Charcha Hai"
| Mohammed Rafi, Lata Mangeshkar
|-
| 3
| "Bhare Bazaar Mein"
| Lata Mangeshkar
|-
| 4
| "Main Phool Bechti Hoon"
| Lata Mangeshkar
|-
| 5
| "Tu Kahin Aas Paas Hai Dost"
| Mohammed Rafi
|- 
| 6
| "Ham Ko Bhi Gham Ne Mara"
| Lata Mangeshkar
|-
| 7
| "Tere Long Da Piya Lashkara"
| Mohammed Rafi, Lata Mangeshkar
|}
==References==
 

==External links==
*  

 
 
 
 
 

 