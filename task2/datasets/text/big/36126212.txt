À propos de Sarajevo
{{Infobox film name        = À propos de Sarajevo image       = caption     = director    = Haris Pašović writer      = Haris Pašović producer    = Space Productions  Haris Pašović  starring    = studio      = Space Productions distributor = Gramofon released    =   runtime     = 30 minutes country     = Bosnia and Herzegovina language  Bosnian
|budget      = gross       =
}}
À propos de Sarajevo is a name of a documentary written and directed by Haris Pašović.  It is the story about a Sarajevo festival led by Edin Zubcevic and the city of Sarajevo which, despite four years of the brutal siege of Sarajevo, still nourishes multiculturalism and love of jazz music.  The documentary is 30 minutes long and features E.S.T., Denis Baptist, Bojan Zulfikarpašić Trio, Dhafer Yousuf, Anuar Brahem and several other European bands.  

The documentary was screened at Sarajevo Film Festival (2005) and Bangkok International Film Festival (2006). 

==References==
 

 
 
 
 
 