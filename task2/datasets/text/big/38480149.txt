Neighboring Sounds
{{Infobox film
| name           = Neighboring Sounds
| image          = Neighbouring Sounds.jpg
| alt            = 
| caption        = Theatrical release poster
| director       = Kleber Mendonça Filho
| producer       = Emilie Lesclaux
| screenplay     = Kleber Mendonça Filho
| starring       = Irandhir Santos  Gustavo Jahn Maeve Jinkings
| music          = DJ Dolores
| cinematography = Pedro Sotero  Fabricio Tadeu
| editing        = Kleber Mendonça Filho  João Maria
| studio         = CinemaScópio
| distributor    = Vitrine Filmes
| released       =  
| runtime        = 131 minutes
| country        = Brazil
| language       = Portuguese
| budget         = BRL|R$1,860,000
| gross          = R$968,981 
}}

Neighboring Sounds (  drama film directed and written by Kleber Mendonça Filho, produced by Emilie Lesclaux and starring Irandhir Santos, Gustavo Jahn and Maeve Jinkings.   

The film entered the list of top films of 2012 made by film critic Anthony Oliver Scott from The New York Times.    The film was screened at the International Film Festival Rotterdam and was released nationally on January 4, 2013. The film was selected as the Brazilian submission in the List of submissions to the 86th Academy Awards for Best Foreign Language Film|foreign-language race for the 86th Academy Awards,     but it was not nominated.

==Plot==
Life in a middle-class neighbourhood in present day Recife takes an unexpected turn after the arrival of an independent private security firm. The presence of these men brings a sense of safety, but also a good deal of anxiety, to a culture which runs on fear. Meanwhile, Bia (Maeve Jinkings), married and a mother of two, must find a way to deal with the constant barking and howling of her neighbour’s dog.   

==Cast==
 
* Irandhir Santos as Clodoaldo
* Gustavo Jahn as João
* Maeve Jinkings as Bia
* W.J. Solha as Francisco
* Irma Brown as Sofia
* Lula Terra as Anco
* Yuri Holanda as Dinho
* Clébia Souza as Luciene
* Albert Tenório as Ronaldo
* Nivaldo Nascimento as Fernando
* Felipe Bandeira as Nelson
* Clara Pinheiro de Oliveira as Fernanda
* Sebastião Formiga as Claudio
* Mauricéia Conceição as Mariá
 

==Production== Rotterdam Film Festival, where the film made its world premiere. The screenplay was also awarded in bidding by Petrobras and the Government of Pernambuco.   

==Critical reception== Brazilian films in 2012, Neighbouring Sounds received overwhelmingly positive reviews; film review aggregator Rotten Tomatoes reports that 92% of critics gave the film a positive review based on 36 reviews, with an average score of 7.8/10.  On Metacritic, which assigns a normalised rating out of 100 based on reviews from critics, the film has a score of 77 (citing "generally favourable reviews") based on 9 reviews. 

Critic A. O. Scott from The New York Times names as one of the worlds top 10 movies made in 2012.  Caetano Veloso, in his column in the newspaper O Globo, called it "one of the best films made recently in the world."    Robert Abele from Los Angeles Times emphasizes it as "remarkable" and "breathtaking".    Tom Dawson of Total Film gave it four of five stars, saying that Filho "reveals a society haunted by both its past and by the threat of future violence",    while David Parkinson from Empire (magazine)|Empire also praises the director, calling his film "a hugely impressive debut feature".   

President of Brazil Dilma Rousseff, in her Twitter account, said to be happy with the films submission to the 86th Academy Awards, stating its a "beautiful film".  The president also recommended Neighbouring Sounds to her followers, considering the film "a chronicle of todays Recife". 

==Accolades==

{| class="wikitable"
|-
! Awards Group !! Category !! Recipient !! Result
|- 2012 BFI Sutherland Award|||| 
|- New Talent Kleber Mendonça Filho|| 
|- Festival de 2012 Festival de Gramado Brazilian Competition
|
| 
|- Best Director Kleber Mendonça Filho
| 
|- Best Sound Kleber Mendonça Filho & Pablo Lamar
| 
|- Mar del 2012 Mar Best Film|||| 
|- Films from 2012 Films FIPRESCI Prize|||| 
|- Festival do 2012 Festival do Rio Best Film
|
| 
|- Best Screenplay Kleber Mendonça Filho
| 
|- International Film 2012 International Film Festival Rotterdam Tiger Competition
|
| 
|- Tiger Award
|
| 
|- Sydney Film 2012 Sydney Official Competition Award|||| 
|- 2012 São Best Brazilian Film|||| 
|- Lleida Latin-American 2013 Lleida Latin-American Film Festival Special Jury Award
|
| 
|- Best Screenplay Kleber Mendonça Filho
| 
|- AFM International Independent Film Festival ||!f Inspired Award|||| 
|}

==See also==
* List of submissions to the 86th Academy Awards for Best Foreign Language Film
* List of Brazilian submissions for the Academy Award for Best Foreign Language Film

==References==
 

==External links==
*   
*  
*  
*  

 
 
 
 
 
 