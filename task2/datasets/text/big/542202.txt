Strictly Ballroom
 
 
{{Infobox film
| name = Strictly Ballroom
| image =  
| caption = Australian theatrical release poster
| director = Baz Luhrmann Antoinette Albert
| screenplay = Baz Luhrmann Craig Pearce Andrew Bovell
| based on =   Bill Hunter Gia Carides Peter Whitford Barry Otto
| music = David Hirschfelder
| cinematography = Steve Mason
| production designer = Catherine Martin
| editing = Jill Bilcock
| studio   = M&A Productions
| distributor = 
| released =  
| runtime = 94 minutes
| country = Australia
| language = English
| budget = AUD 3 million
| gross = AUD 80 million     Linked 2014-07-10 
}}
Strictly Ballroom is a 1992 Australian romantic comedy film directed and co-written by Baz Luhrmann. The film, which was Luhrmanns first, is the first in his The Red Curtain Trilogy of theatre-motif-related films; the other two are Romeo + Juliet and Moulin Rouge!.
 Wharf Theatre, where it was seen by Australian music executive Ted Albert and his wife Antoinette. They both loved it, and when Ted Albert soon after set up the film production company M&A Productions with ex-Film Australia producer Tristram Miall, they offered Luhrman to transform his play into a film. Jane Albert, House of Hits: the great untold story of Australias first family of music (Hardie Grant Books, Prahran, Vic, 2010), pp. 316–331  He agreed on the condition that he would also get to direct it.   Linked 2014-07-10 

==Plot==
Strictly Ballroom tells the story of an Australian ballroom dancer, Scott Hastings (Paul Mercurio), and his struggle to establish his personal style of dance in his way to win the Pan-Pacific Grand Prix Dancing Championship. Scotts steps are not strictly ballroom. Scott comes from a family with a history of ballroom dancing and has been training since childhood. Scotts mother Shirley teaches ballroom dancing, and his father Doug meekly handles maintenance chores at the dance studio.

After losing a competition to a rival pair, because Scott started dancing his own steps, his dancing partner Liz Holt (Gia Carides) leaves him for the rival male, Ken Railings, after his partner Pam Short breaks both her legs in a car accident. With only weeks before the next Pan-Pacific competition, try-outs begin to find Scott a new dance partner but, unknown to his parents, Scott secretly begins rehearsing with frumpy outsider Fran (Tara Morice), a beginner dancer at his parents studio.

Scott is initially sceptical, but when Fran introduces pasodoble steps into their routine, Scott realises her potential. He walks her home one night and finds her Spanish family living in a tiny home next to the railway tracks, where Frans family show him the authentic Spanish pasodoble style. As their rehearsals progress, Fran grows more attractive and self-confident. Few days before the Pan-Pacifics, Frans family decide they are ready to dance pasodoble. Scott and Fran are walking together and talking until they kiss.

When Scott returns to the dance studio, he finds Barry Fife, the conniving president of the Australian Dancing Federation who proceeds to tell him "the truth" about his parents, Doug and Shirley — they too were ballroom dancing champions, especially his father, until they lost the Pan-Pacific Grand Prix because of Dougs self-obsession and unorthodox dance steps. According to Barry losing the contest left Doug a broken man, sustained by the hope that one day his son would learn from his fathers mistakes and win the Grand Prix. Scott is convinced to dance with Liz instead of Fran so he can win "for his fathers sake". However, this is later revealed as a lie, part of Barrys plot to fix the competition so Scott and Liz will lose. Scott starts training with Liz, while a heart-broken Fran goes back to the beginners class.

At the Pan-Pacific Grand Prix, Scotts friend Wayne Burns and his partner Vanessa Cronin overhear Fife talking to Ken and his partner Tina Sparkle saying that they will win no matter what. Wayne tells Les Kendall (Scotts coach, Doug and Shirleys friend and one of the judges) who then confronts Fife about it. Meanwhile, Doug finally manages to pull Scott aside and tells the real story — Doug never danced at the competition because Barry convinced Shirley the only way to win was to dance the conventional steps with Les, but Shirley and Les lost the contest anyway.  After hearing his fathers revelation, Scott finds Fran and asks her to dance with him. Fife tries to cut the music and stop them from dancing but Scotts sister Kylie and her partner Luke (from the childrens division) interfere until Fifes loyal companion Charm Leachman cuts the music. Fife then disqualifies them, but Doug, along with Frans family, clap out a beat which encourages Scott and Fran to "dance from the heart", drawing cheers from the crowd and tears of joy from Doug. Finally, Liz, having had a change of heart, turns on Barry and Leachman and restores the music.  The couples spirited dancing brings down the house. Doug asks Shirley to dance with him and the whole audience joins them on the stage. As the performance finishes, Scott and Fran kiss, the competition forgotten, as it was never about winning or losing.

==Main cast==
 
* Paul Mercurio as Scott Hastings
* Tara Morice as Fran Bill Hunter as Barry Fife
* Pat Thomson as Shirley Hastings
* Gia Carides as Liz Holt
* Peter Whitford as Les Kendall
* Barry Otto as Doug Hastings
* John Hannan as Ken Railings
* Sonia Kruger as Tina Sparkle
* Kris McQuade as Charm Leachman
* Pip Mushin as Wayne Burns
* Leonie Page as Vanessa Cronin
* Antonio Vargas as Rico, Frans father
* Armonia Benedito as Ya Ya, Frans grandmother
* Jack Webster as Terry
* Lauren Hewett as Kylie Hastings
* Steve Grace as Luke, Kylies dance partner
* Kerry Shrimpton as Pam Short
* Todd McKenney as Nathan Starkey
 

==Style== David and Goliath, Cinderella and The Ugly Duckling.

==Production history==
The film version of Strictly Ballroom was developed from an original short play of the same name. It drew on Luhrmanns own life experience—he had studied ballroom dancing as a child and his mother worked as a ballroom dance teacher in his teens.  While studying at NIDA in the early 1980s, Luhrmann and a group of fellow students devised a short comedy-drama set in the cutthroat world of competitive ballroom dancing.  This original 1984 NIDA production was a critical success and, after graduating, Luhrmann was invited to re-stage the play for the Czechoslovakian Youth Drama Festival in Bratislava in 1986. He invited his school friend Craig Pearce to help him rewrite and expand the script. With its themes of artistic repression and underdogs battling against the odds, the play was a success at the festival, winning both the best director and best production awards. 
 Catherine Martin Brisbane Expo in 1988 before opening at the Wharf Studios on 24 September 1988. 

During its successful run at the Wharf, the play was seen by an influential Australian music executive. Ted Albert was a leading record producer and music publisher, best known in Australia as the discoverer and original producer of 1960s pop sensations The Easybeats. By the time he saw Strictly Ballroom, Albert was the managing director of his family-owned music publishing company Albert Music (formerly J. Albert & Sons) and its subsidiary, the highly successful record label Albert Productions, which scored a string of hits in the 1970s and 1980s with acts including John Paul Young and AC/DC.
 BHP steelworks in the industrial city of Newcastle, New South Wales|Newcastle. Luhrmann balked at the move towards naturalism and eventually, with Alberts agreement, the director brought in his old friend Craig Pearce, who was able to translate Luhrmanns theatrical vision into a workable screenplay. 
 Bill Hunter, and although co-star Paul Mercurio was well known as a dancer through his work with the Sydney Dance Company, Strictly Ballroom was his first acting role. With the original budget set at over AUD 5 million, government film funding bodies were reluctant to back such a left-field project with few major names in the credits. The script was then pared back and the subplot dropped, but when Miall approached the Film Finance Corporation, he was told that they would not back such a high-budget film (in Australian terms) with a first-time director. He was told to replace Luhrmann, but he refused, promising to make further cuts. Miall and Albert then pared the budget down to AUD 3.3 million and the FFA then agreed to provide around 65%, on condition that the producers were able to raise the remaining AUD 1 million and secure a local distributor. They sent Luhrmann to the Cannes Film Festival in hopes of finding an overseas distributor, but this came to nothing. After returning to Australia, Miall and Luhrmann had a fortuitous meeting with Andrew Pike, head of the Canberra-based independent distribution company Ronin Films. Intrigued by Luhrmanns colourful pitch which involved sketches, set miniatures and pieces of costume, Pike agreed to back a limited local release, although he later admitted that, had he seen only the script, he would probably have turned it down. 

Although the FFC funding was now in the pipeline, the production faced its most serious challenge when, on 11 November 1990, Ted Albert died suddenly from a heart attack (the film is dedicated to him). This threw the entire project into doubt, but Ted Alberts widow Popsy decided that it should go to completion in honour of her husband, so she took over as executive producer, with Miall as producer. With her blessing, Teds family company Albert Music invested AUD 1 million, with the remaining AUD 300,000 sourced from private investors. Even after completion, the team were greeted with stiff resistance from exhibitors: Luhrmann recalled that one exhibitor walked out before the film had even finished, declaring that Luhrmann was ruined and that he would never work again. 

The film was accepted for the Cannes Film Festival, but another tragedy struck just before its first screening—actress Pat Thomson, who played Scotts mother, was diagnosed with cancer and she died in April 1992, only one month before its Cannes world premiere in May. Strictly Ballroom had its first public screening at midnight in the Un Certain Regard programme and proved to be an instant hit—the cast and crew received a fifteen-minute standing ovation, which was repeated the following night; it became one of the major hits of the festival, winning the Prix De Jeunesse and triggering a bidding war among international distributors. 

It was a huge success when released in Australia in August, and it swept the field at the 1992 AFI awards, gaining 13 nominations and winning in eight major categories. It was also a major success at the 1993 BAFTA awards, gaining eight nominations and winning three awards for Best Costume Design, Best Original Film Score and Best Production Design. Other major accolades included a 1994 Golden Globe nomination for Best Picture, Newcomer of the Year at the 1993 London Critics Circle Film Awards, the Peoples Choice award at the 1992 Toronto International Film Festival and Most Popular Film at the 1992 Vancouver International Film Festival. 

==Box office==
Strictly Ballroom grossed AUD 21,760,400 at the box office in Australia,  and a further USD 11,738,022 in the United States.  Worldwide, it eventually took AUD 80 million at the box office,  making it one of the most successful Australian films of all time.

==Awards== AFI Award for Best Achievement in Costume Design, Best Achievement in Editing, Best Achievement in Production Design, Best Actor in Supporting Role (Barry Otto), Best Actress in Supporting Role (Pat Thomson), Best Director, Best Film, Best Screenplay AFI Award for Best Achievement in Cinematography, Best Achievement in Sound, Best Actor in Lead Role (Paul Mercurio), Best Actress in Lead Role (Tara Morice), Best Actress in Supporting Role (Gia Carides)
* 1992 - Won Cannes Film Festival: Award Of The Youth for Foreign Film
* 1993 - Won BAFTA Film Award for Best Costume Design, Best Original Film Score, Best Production Design
* 1993 - Nominated BAFTA Film Award for Best Actress (Tara Morice), Best Editing, Best Film, Best Adapted Screenplay, Best Sound
* 1993 - Nominated Golden Globe Award for Best Motion Picture – Musical or Comedy
* 1993 - Won  )
* 1994 - Nominated Bogota Film Festival: Golden Precolumbian Circle Award for Best Film
* 2013 - Nominated 20/20 Award for Best Supporting Actress Pat Thompson
* 2013 - Nominated 20/20 Award for Best Original Screenplay Baz Luhrman and Craig Pearce
* 2013 - Nominated 20/20 Award for Best Film Editing Jill Bilcock
* 2013 - Nominated 20/20 Award for Best Original Score David Hirschfelder Catherine Martin
* 2013 - Nominated 20/20 Award for Best Costume Design Angus Strathie

==Music==
 
Among the songs featured on the soundtrack are:
* "The Blue Danube" by Johann Strauss II Love is in the Air" and "Standing In The Rain" by John Paul Young. The films version of "Love is in the Air" re-entered the Australian charts and became a Top 5 hit, peaking at #4 on the national chart in October 1992. 
* A cover version of John Paul Youngs "Yesterdays Hero" by Ignatius Jones
* "Perhaps, Perhaps, Perhaps" by Doris Day Time After Mark Williams and Tara Morice
 Time After Time were played in the 1984 and 1986 Strictly Ballroom stage productions.

==Musical== Sydney Lyric theatre. It was initially scheduled to open in September 2013,  but in fact premiered on 12 April 2014.  The production moved to Her Majestys Theatre in Melbourne in January 2015. 

==See also==
* Cinema of Australia
* List of films set in Sydney

==References==
 

==External links==
 
*  
*  
*  
*  
*  
*  
*  

 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 