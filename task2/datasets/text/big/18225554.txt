It's Alive (2008 film)
 
 
{{Infobox film| name =Its Alive
  | image = Aliveunrdvd.jpg
  | caption = Unrated DVD cover
  | director = Josef Rusnak
  | producer = Mark Damon
  | writer = 2007 Screenplay:   James Murray
  | cinematography =
  | music = Nicholas Pike
  | editing = James Herbert Patrick McMahon
  | distributor =
  | released = October 6, 2009 (USA)
  | runtime =
  | language = English
  | budget =
  | gross =
    | followed_by =
}}
 horror film of the same name.

== Plot == James Murray). After discovering the baby has doubled in size in just a month, doctors decide to extract the baby by Caesarian section. After the doctor cuts the umbilical cord, the newborn goes on a rampage and kills the surgical team in the operating room. It afterwards crawls onto its mothers belly and falls asleep. Lenore and the baby are found on the operating table, the room covered in blood. Lenore has no memory of what happened.
 
After questioning by the police, Lenore is allowed to take the baby home. Authorities arrange for a psychologist to help her regain her memory of the delivery. Soon, baby Daniel bites Lenore when she’s feeding him, revealing his taste for blood.
 
Daniel begins to attack small animals and progresses to killing adult humans. Lenore refuses to accept that her baby is a cannibalistic killer. Frank comes home from work to find Lenore sitting in the babys room, but Daniel is not in his crib. He searches for Daniel and accidentally locks himself in the basement. The police find Frank, but Daniel kills one of the police officers. Frank captures Daniel, but is unable to bring himself to kill the baby. The baby attacks him.

Lenore finds Frank injured, and she brings the baby into the house. She sets fire to the house, killing herself and the baby. Frank and his brother watch the house burn.

== Cast ==
* Bijou Phillips as Lenore Harker James Murray as Frank Davis
* Raphaël Coleman as Chris Davis
* Owen Teale as Sgt. Perkins
* Skye Bennett as Nicole
* Ty Glaser as Marnie
* Arkie Reece as Perry
* Todd Jensen as Dr. Orbinson

==Release==
The film was officially released Direct-to-video|straight-to-DVD on October 6, 2009. It is available in both rated and unrated editions.  The film had theatrical release in the Philippines in August 2009. 

==Reception==
 
Larry Cohen, the director of the 1974 original, was interviewed on December 21, 2009 regarding the remake and gave it a negative review, saying "Its a terrible picture. Its just beyond awful," and "I would advise anybody who likes my film to cross the street and avoid seeing the new enchilada." 

==References==
 

== External links ==
* 

 

 
 
 
 
 
 