Toofan Aur Bijlee
{{Infobox film
| name           = Toofan Aur Bijlee
| image          = 
| image size     = 
| border         = 
| alt            = 
| caption        =
| director       = Homi Wadia
| producer       = Homi Wadia
| writer         = 
| screenplay     = 
| story          = 
| based on       = 
| narrator       =  Bhagwan
| Chitragupta
| cinematography = 
| editing        = 
| studio         = Basant Studios
| distributor    = 
| released       = 1975
| runtime        = 155 min
| country        = India
| language       = Hindi
| budget         = 
| gross          = 
}} 1975 Hindi Chitragupta with lyrics written by Kafil Azar.    The film starred Arvind Kumar, Zaheera, Bhagwan (actor)|Bhagwan, Imtiaz Khan, Randhawa, Mohan Choti and Tun Tun. 

The story is about twin sisters Madhuri and Sheela, both played by Zaheera, whose parents and younger brother have been killed by villains. The story follows Madhuri’s revenge against them, aided by a young man called Azad.

==Plot==
Detective Rana is after the bad guys Jugal, Zulfi and Laloo. However they murder him along with his wife and son. Out of his two daughters Madhuri escapes and is saved by a circus man Zorawar. She has promised her mother to avenge her familys death before running away from the villains. The other daughter Sheela, Madhuri’s look-alike, is kidnapped by the crooks and grows up to be a night club dancer, having lost her memory. Madhuri, with the help of her friends and her dog Tiger, finds the night club. She takes Sheela’s place. Azad, a young man whom Madhuri meets and falls in love with helps her fight the gangsters. Sheela also recovers her memory and falls in love with Azad. Finally, there is a chase scene with fights on top of trains and shootout in which Sheela is killed. Azad turns out to be a police detective and Madhuri and he get together.
 
==Cast==
* Zaheera
* Arvind Kumar
* Imtiaz Bhagwan
* Randhawa
* Mohan Choti
* Tun Tun
* Habib
* Babu Raje

==Music==
The film’s music was composed by Chitragupta with lyrics by Kafil Azar. The singing artists were Mohammed Rafi, Mahendra Kapoor, Asha Bhosle, Ranu Mukherjee and Aparna Mayekar. 

===Songlist===
{| border="4" cellpadding="7" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#CCCCCC" align="center"
! # !! Title !! Singer
|-
| 1
| Yeh Dil Tera Hi Tha
| Mahendra Kapoor, Asha Bhosle, Ranu Mukherjee
|-
| 2
| Galyaat Saankli Sonyachi Hi Pori Konachi
| Mahendra Kapoor, Asha Bhosle
|-
| 3
| Meri Jaan Baaton Hi Baaton Mein Bol Zara
| Mohammed Rafi
|-
| 4
| Zulfi Main Tere Pyar Mein Diwani Ho Gayi
| Asha Bhosle
|-
| 5
| Main Hoon Toofan Main Hoon Bijli
| Asha Bhosle, Aparna Mayekar
|}

   
==References==
 

==External links==
* 

 

 
 
 
 
 
 
 