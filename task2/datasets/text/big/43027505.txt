Gabriela (2001 film)
{{Infobox film
| name           = Gabriela
| director       = Vincent Jay Miller
| writer         = Vincent Jay Miller
| starring       = Jaime Gomez Seidy Lopez
| producer       = Vincent J. Francillon
| music          = Craig Stuart Garfinkle Leo Marchildon 
| released       =  
| runtime        = 93 minutes
| country        = United States
| language       = English Spanish
}}
Gabriela is a 2001 American romance film, starring Seidy Lopez in the title role alongside Jaime Gomez as her admirer Mike. The film has been cited as an inspiration behind the Premiere Weekend Club, which supports Latino film-making.

== Plot ==
Gabriela, engaged to be married to Patrick, feels more of a sense of duty than anticipation towards their planned wedding. On meeting her at the clinic where he works, Mike falls in love with Gabriela, and turns to his friend Douglas and his brother for advice. When he asks Gabriela to break off her engagement, she refuses and takes Patrick to Mexico to meet her family, leaving Mike trying to find a way to save his hopes of marrying her.

== Cast ==
* Seidy Lopez - Gabriela
* Jaime Gomez - Mike
* Zach Galligan - Patrick
* Troy Winbush - Douglas
* Lupe Ontiveros - Grandma Josie
* Stacy Haiduk - Ilona
* Evelina Fernandez - Sofia
* Frank Medrano - Manny
* Sal Lopez - Policeman
* Danny De La Paz - Policeman
* Steve Valentine - Steven
* Liz Torres - Julia
* Lamont Bentley - Nick
* Patrick Rowe - Gary
* Teddy Lane Jr. - Rex
* Colette OConnell - Ryan
* Patrick M.J. Finerty - Adrian
* Yennifer Behrens - Jesica

== Soundtrack ==
The films soundtrack was written by Craig Stuart Garfinkle and Leo Marchildon and released on Power Point Records. The track Gabriela Tribute is performed by Latin Jazz musician Poncho Sanchez and his band. 

== Reception ==
The film received mixed reviews. Marc Salov, writing for the Austin Chronicle, likened Gabriela to a "melodramatic telenovela" and gave it one star out of five.  Robert Koehler of Variety agreed, declaring that the film has "hardly enough substance to carry a 30-minute TV episode".  However, epinions awarded Gabriela four stars out of five, noting that it is "not the same old love story". 

Similarly, Linda Pliagas of Latin Style wrote "Gabriela is amazing. Seidy Lopez and Jaime Gomez set the screen on fire."   and NuVisions wrote "Gabriela is a classic Hollywood love story with cutting edge wit. Its the most relevant Indie film since Sex, Lies and Videotape.". 

== References ==
 

== External links ==
* http://www.imdb.com/title/tt0189541/
* http://www.movieweb.com/movie/gabriela/reviews/critic
* http://variety.com/2001/film/reviews/gabriela-1200467313/
* http://www.austinchronicle.com/calendar/film/2001-05-11/141148/
* http://www.cdbaby.com/Artist/Cosimo

 
 